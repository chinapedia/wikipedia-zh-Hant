《**史瑞克**》（），於2001年上映，為[美國](../Page/美國.md "wikilink")[好萊塢導演](../Page/好萊塢.md "wikilink")[安德魯·亞當森](../Page/安德魯·亞當森.md "wikilink")、執導的童話故事作品，製作公司則為[夢工廠](../Page/夢工廠.md "wikilink")。\[1\]該作品改編自知名童畫書作家的同名繪本，影片[笑點在於](../Page/笑點.md "wikilink")「嘲諷所有的經典故事，並顛覆了一般人對故事的刻版印象。」\[2\]

參與配音的好萊塢演員有[麥克·邁爾斯](../Page/麥克·邁爾斯.md "wikilink")、[約翰·李斯高](../Page/約翰·李斯高.md "wikilink")、[艾迪·墨菲](../Page/艾迪·墨菲.md "wikilink")、[卡麥蓉·狄亞](../Page/卡麥蓉·狄亞.md "wikilink")。本片獲得了二億六千七百萬元票房，突破《[獅子王](../Page/獅子王.md "wikilink")》，成為當時有史以來賣座最高的動畫片。此紀錄直到2003年的《[海底總動員](../Page/海底總動員.md "wikilink")》才被打破\[3\]。之後亦有續集《[史瑞克2](../Page/史瑞克2.md "wikilink")》、《[史瑞克三世](../Page/史瑞克三世.md "wikilink")》以及《[史瑞克快樂4神仙](../Page/史瑞克快樂4神仙.md "wikilink")》的製作。

## 劇情概要

遙遠的沼澤裡住著一个被人稱為怪物且力大无比的史瑞克，他喜欢独居生活，虽然脾气暴躁但却心地善良。為了趕走被邪惡且身材矮小的法克大人驅離到他家附近森林裡的一大堆童話故事人物而前往杜洛城找法克大人談判，並挑選了喋喋不休、不肯閉嘴的驢子成为伙伴。其後法克表示史瑞克說將一座塔裡的美丽的費歐娜公主救出來以後，法克就還他寧靜的沼澤，而娶到費歐娜公主的法克就能当上国王。

史瑞克來到位於火山口的城堡後救出了公主並打败了火龙，而火龙与驴子互相产生了情愫。在救出公主回来的途中，他和費歐娜產生情愫。但驢子首先發現了美麗公主的秘密：原来費歐娜公主在她小的時候曾被一个恐怖的咒语所诅咒，每到晚上咒语生效，她会变成一个怪物的模样，直到天亮才能恢复原样。国王和王后知道后向神仙教母求助，被教母告知需要把公主锁在高塔上，等待一位勇敢的白马王子来救她并给她一吻，她才能破除咒语永远恢复原样。

就在驴子知道这个秘密的晚上，史瑞克决定向費歐娜表白爱意，不料由于偷听到二人之间的谈话，使他误会认为費歐娜嫌弃他的怪样，于是放弃了表白。

当史瑞克把費歐娜交给法克后，他回到了宁静的沼澤老家，而驴子把真相告知他后，他决定夺回公主。在小龍女（火龙）的帮助下，史瑞克最终战胜法克，重新夺回了費歐娜公主的芳心。但是当他亲吻費歐娜后，費歐娜并没有变回原来的美丽长相，而是永远停留在怪物的样子状态(因為費歐娜的原貌就是怪物)，但这并不影响史瑞克对她的爱。从此两人幸福地生活在了一起，故事結束。

## 角色

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>美國</p></td>
<td><p>台灣</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥克·邁爾斯.md" title="wikilink">麥克·邁爾斯</a></p></td>
<td><p><a href="../Page/李立群.md" title="wikilink">李立群</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾迪·墨菲.md" title="wikilink">艾迪·墨菲</a></p></td>
<td><p><a href="../Page/唐從聖.md" title="wikilink">唐從聖</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡麥蓉·狄亞兹.md" title="wikilink">卡麥蓉·狄亞兹</a></p></td>
<td><p><a href="../Page/蔣篤慧.md" title="wikilink">蔣篤慧</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/约翰·利思戈.md" title="wikilink">约翰·利思戈</a></p></td>
<td><p><a href="../Page/符爽.md" title="wikilink">符爽</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凡松·卡塞.md" title="wikilink">凡松·卡塞</a></p></td>
<td><p><a href="../Page/黃韋皓.md" title="wikilink">黃韋皓</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/劉傑_(臺灣).md" title="wikilink">劉傑</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/克里斯·米勒.md" title="wikilink">克里斯·米勒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克里斯·米勒.md" title="wikilink">克里斯·米勒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/于正昇.md" title="wikilink">于正昇</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西蒙·J·史密斯.md" title="wikilink">西蒙·J·史密斯</a><br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安德魯·亞當森.md" title="wikilink">安德魯·亞當森</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>巴比·布拉克</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>麥克·葛拉斯索</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 發行

電影於2001年5月16日在美國上映。

### 評價

[爛番茄根據](../Page/爛番茄.md "wikilink")202條評論，給出88%的新鮮度，平均得分7.8／10\[4\]。在[Metacritic上](../Page/Metacritic.md "wikilink")，電影獲得了84分\[5\]。

## 其它媒体

### 书籍

  - 本片原著：

<!-- end list -->

  - Steig, William (1990). [史瑞克！](../Page/史瑞克！.md "wikilink"), Sunburst
    Paperback. ISBN 0-374-46623-8

### 電視遊戲

Several [電視遊戲](../Page/電視遊戲.md "wikilink") adaptations of Shrek have
been published on various [遊戲機](../Page/遊戲機.md "wikilink") platforms.

  - [史瑞克 (電視遊戲)](../Page/史瑞克_\(電視遊戲\).md "wikilink")
  - [史瑞克2 (電視遊戲)](../Page/史瑞克2_\(電視遊戲\).md "wikilink")
  - [史瑞克：Smash and Crash](../Page/史瑞克：Smash_and_Crash.md "wikilink")
  - [史瑞克：Hassle at the
    Castle](../Page/史瑞克：Hassle_at_the_Castle.md "wikilink")
  - [史瑞克：Super Slam](../Page/史瑞克：Super_Slam.md "wikilink")
  - [史瑞克：Extra Large](../Page/史瑞克：Extra_Large.md "wikilink")
  - [史瑞克：超級派對](../Page/史瑞克：超級派對.md "wikilink")
  - [史瑞克3 (電視遊戲)](../Page/史瑞克3_\(電視遊戲\).md "wikilink")
  - [史瑞克4 (電視遊戲)](../Page/史瑞克4_\(電視遊戲\).md "wikilink")

### 漫画

  - 2003年[黑馬漫畫出版了](../Page/黑馬漫畫.md "wikilink")3期史瑞克迷你系列[漫画书](../Page/漫画书.md "wikilink")，这个系列收入到中。
    \[6\]

## 參看

  - [史瑞克2](../Page/史瑞克2.md "wikilink")
  - [史瑞克三世](../Page/史瑞克三世.md "wikilink")
  - [史瑞克快樂4神仙](../Page/史瑞克快樂4神仙.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [梦工厂-史瑞克](http://www.dreamworksfansite.com/shrek/)

  - [Tom Hester -史瑞克角色设计](http://www.HesterStudios.com)

  - [William Steig网站-本片原著故事](http://www.williamsteig.com/shrek.htm)

  - [官方網站](http://www.shrek.com)

  -
  -
  -
  -
  -
  - {{@movies|fsen30126029}}

  -
  -
  -
[Category:2001年動畫電影](../Category/2001年動畫電影.md "wikilink")
[Category:书籍改编电影](../Category/书籍改编电影.md "wikilink")
[Category:美国喜剧片](../Category/美国喜剧片.md "wikilink")
[Category:美國奇幻電影](../Category/美國奇幻電影.md "wikilink")
[Category:美国冒险片](../Category/美国冒险片.md "wikilink")
[Category:美國惡搞片](../Category/美國惡搞片.md "wikilink")
[Category:美国動畫電影](../Category/美国動畫電影.md "wikilink")
[Category:史瑞克](../Category/史瑞克.md "wikilink")
[Category:具有超人类力气的虚构角色](../Category/具有超人类力气的虚构角色.md "wikilink")
[Category:2000年代冒险片](../Category/2000年代冒险片.md "wikilink")
[Category:2000年代奇幻片](../Category/2000年代奇幻片.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:導演處女作](../Category/導演處女作.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:安妮奖最佳动画片](../Category/安妮奖最佳动画片.md "wikilink")
[Category:東森電視外購動畫](../Category/東森電視外購動畫.md "wikilink")

1.  [星光大道，電影快報](http://movie.starblvd.com/cgi-bin/movie/euccns?/film/2001/Shrek/Shrek.html)

2.  夢工廠；傑佛瑞凱森柏格
3.  [新浪娛樂](http://ent.sina.com.cn/m/f/2003-06-10/1117155519.html)
4.
5.
6.  <http://www.darkhorse.com/profile/profile.php?sku=12-541>