<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Slytherin.jpg" title="fig:史萊哲林徽">史萊哲林徽</a><br />
</p>
<center>
<p><small>史萊哲林</p></td>
</tr>
<tr class="odd">
<td><p><strong>-{斯萊特林}-</strong><br />
<strong>-{史萊哲林}-</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>學院創立</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>學院<a href="../Page/图腾.md" title="wikilink">圖騰</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong>學院院長</strong></p></td>
</tr>
<tr class="odd">
<td><p>學院顏色</p></td>
</tr>
<tr class="even">
<td><p><strong>選擇<a href="../Page/学生.md" title="wikilink">學生方式</a></strong></p></td>
</tr>
</tbody>
</table>

**史萊哲林**（），-{zh-hans:台湾译名; zh-hant:大陸譯名;}-為-{zh-hans:**史莱哲林**;
zh-hant:**斯萊特林**;}-，是奇幻小說《[哈利·波特](../Page/哈利·波特.md "wikilink")》系列中，[霍格華茲魔法與巫術學院的四大學院之一](../Page/霍格華茲魔法與巫術學院.md "wikilink")\[1\]\[2\]，象徵[四大元素中的](../Page/四大元素.md "wikilink")[水](../Page/水.md "wikilink")。學院圖騰是[蛇](../Page/蛇.md "wikilink")\[3\]。

創立者是[薩拉札·史萊哲林](../Page/薩拉札·史萊哲林.md "wikilink")\[4\]，擇生條件是純種，精明，意志堅强，有野心。此學院培養多著名[巫師](../Page/巫師_\(哈利波特\).md "wikilink")，但許多黑巫師也出自于此，如：[佛地魔](../Page/佛地魔.md "wikilink")。雖然選擇學生方式為血統純正，但有時侯會收取極少量[混血學生](../Page/混血.md "wikilink")（但從來不收[麻瓜後代](../Page/麻瓜.md "wikilink")），例如[石內卜以及佛地魔](../Page/石內卜.md "wikilink")，小說的主角[哈利·波特也差點被分到史萊哲林](../Page/哈利·波特_\(角色\).md "wikilink")。而[跩哥·馬份亦屬於該學院](../Page/跩哥·馬份.md "wikilink")。

## 位置

位於[地窖](../Page/地窖.md "wikilink")，要在一個空白的石牆說出通關密語才能入內。它有一個低矮的天花板和綠色火炬。房間部份位於湖底，所以光線永遠是綠色的。據說從裏面往外看，能看見黑湖裡的生物（因為地窖在黑湖底下）。

## 物品

薩拉札·史萊哲林的遺物是一個小金匣，屬於現存的史萊哲林血統繼承者魔佛羅·剛特。最後被佛地魔用作[分靈體](../Page/分靈體.md "wikilink")，

在小說以及同名電影《哈利波特》中，[榮恩·衛斯理用](../Page/榮恩·衛斯理.md "wikilink")[葛來分多寶劍將其劈碎](../Page/葛來分多.md "wikilink")。

## 後裔

在原著小說中 ，有出現已知的薩拉札·史萊哲林之後裔為剛特家族 ，也就是佛地魔的母系家族 。

## 代表人物

  - [薩拉札·史萊哲林](../Page/薩拉札·史萊哲林.md "wikilink")

## 其他資訊

2007年，羅琳在接受訪問時表示，後來的史萊哲林學院已不再像以往那樣崇尚純種，但它的壞聲譽仍然存在，因此哈利·波特的兒子[阿不思·波特還是不想被分到史萊哲林](../Page/阿不思·波特.md "wikilink")\[5\]。

## 參考資料

<references />

[category:哈利波特](../Page/category:哈利波特.md "wikilink")
[category:霍格華茲](../Page/category:霍格華茲.md "wikilink")

[en:Hogwarts\#Houses](../Page/en:Hogwarts#Houses.md "wikilink")

1.
2.
3.
4.
5.