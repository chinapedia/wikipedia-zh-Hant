**葉姓**是一个[中文姓氏](../Page/中文姓氏.md "wikilink")，名列[百家姓第](../Page/百家姓.md "wikilink")257位。在[簡體字中因為把](../Page/簡體字.md "wikilink")“-{葉}-”簡化為“-{叶}-”，致使與[-{zh-tw:正體字;zh-hk:繁體字;zh-hans:繁体字}-中的](../Page/正體字.md "wikilink")-{叶}-（讀作“協”，與協相通，有協同、-{叶}-韻之意）重疊，使兩者容易混淆。例如：日本藝人[-{叶}-氏姐妹花](../Page/叶姊妹.md "wikilink")、[-{叶}-恭子](../Page/叶恭子.md "wikilink")、[-{叶}-美香及漫畫家](../Page/叶美香.md "wikilink")[-{叶}-恭弘中的](../Page/叶恭弘.md "wikilink")-{叶}-並非指[華人常見的](../Page/華人.md "wikilink")-{葉}-姓。宋人在《[新唐書](../Page/新唐書.md "wikilink")》編纂時誤將葉姓的前身沈尹氏與姬姓沈氏混為一談，造成民間有沈葉同宗之訛傳。\[1\]

葉姓於[宋朝为著姓](../Page/宋朝.md "wikilink")。[香港拼音則為](../Page/香港政府粵語拼音.md "wikilink")“Yip”或“Ip”。

## 來源

关于叶氏的来龙去脉，《通志氏族略》记述得最为清楚。《风俗通》：“[楚](../Page/楚國.md "wikilink")[沈尹戌生诸梁](../Page/沈尹戌.md "wikilink")，食采于叶，因氏焉。”葉姓出自[芈姓](../Page/芈姓.md "wikilink")[沈尹氏](../Page/沈尹氏.md "wikilink")，为帝[颛顼的后裔](../Page/颛顼.md "wikilink")。传说，帝颛顼的后裔[陆终娶方氏女为妻](../Page/陆终.md "wikilink")，生下六个儿子，其中第六个儿子叫[季连](../Page/季连.md "wikilink")，赐姓芈。季连的后裔[鬻熊很有学问](../Page/鬻熊.md "wikilink")，作过文王的老师。后成王便追封其曾孙[熊绎](../Page/熊绎.md "wikilink")[荆山](../Page/荆山.md "wikilink")（今[湖北西部](../Page/湖北.md "wikilink")）一带建立[荆国](../Page/荆国.md "wikilink")，定都[丹阳](../Page/丹阳_\(楚国\).md "wikilink")（今河南[淅川](../Page/淅川.md "wikilink")），后迁都[子郢](../Page/子郢.md "wikilink")（今湖北[江陵](../Page/江陵.md "wikilink")），改国号为[楚](../Page/楚国.md "wikilink")。

[春秋时](../Page/春秋時期.md "wikilink")，[楚庄王有一曾孙叫戌](../Page/楚庄王.md "wikilink")，沈尹戌后来任楚国左[司马](../Page/司马.md "wikilink")，他为人正直，疾恶如仇，深得楚人敬重。[楚昭王十八年战死沙场](../Page/楚昭王.md "wikilink")，昭王封他的儿子[沈诸梁](../Page/沈诸梁.md "wikilink")（字子高）于叶（今河南[叶县南旧城](../Page/叶县.md "wikilink")），称为叶公。叶公曾平定白公胜的叛乱以复惠王，有功于楚，得封[南阳](../Page/南阳.md "wikilink")，更获赐为公，后委其事于子，而退休于叶。其后人便以邑地为姓氏，称为叶氏。

《通志‧氏族略‧以邑為氏》載：「葉氏，舊音攝，後世與木葉同音」。\[2\][沈諸梁被稱為葉姓族人祖先](../Page/沈諸梁.md "wikilink")，又名葉公。該葉（「攝」）氏以[葉縣爲名](../Page/葉縣.md "wikilink")，亦影響「葉」字於部份地區讀法。\[3\]葉（「攝」）氏最早於1012年出現於高麗王朝。\[4\]

## 歷史名人

  - [葉溫裕](../Page/葉溫裕.md "wikilink")：唐朝吏部尚書
  - [葉夢鼎](../Page/葉夢鼎.md "wikilink")：南宋丞相
  - [葉紹翁](../Page/葉紹翁.md "wikilink")：南宋詩人
  - [葉衡](../Page/葉衡.md "wikilink")：宋朝丞相
  - [葉適](../Page/葉適.md "wikilink")：宋朝理學家
  - [葉伯巨](../Page/葉伯巨.md "wikilink")：明洪武，山西汾州府平遙縣訓導
  - [葉向高](../Page/葉向高.md "wikilink")：明朝首輔
  - [葉名琛](../Page/葉名琛.md "wikilink")：晚清政治人物
  - [葉天士](../Page/葉天士.md "wikilink")：清朝醫學家
  - [葉頌清](../Page/葉頌清.md "wikilink")：辛亥革命志士
  - [葉問](../Page/葉問.md "wikilink")：清代武術家

## 現代人物

### 中國大陸

  - [葉挺](../Page/葉挺.md "wikilink")：中國人民解放軍創始人
  - [葉劍英](../Page/葉劍英.md "wikilink")：中華人民共和國開國領導人、中國共產黨革命家、政治家、軍事家、中國人民解放軍的創建者之一
  - [葉選平](../Page/葉選平.md "wikilink")：葉劍英之子，中國人民政治協商會議全國委員會副主席
  - [葉飛](../Page/葉飛.md "wikilink")：中國共產黨、中華人民共和國政治人物
  - [葉聖陶](../Page/葉聖陶.md "wikilink")：中國作家、教育家
  - [葉千華](../Page/葉千華.md "wikilink")：中國诗人、编辑家、学者、德教专家
  - [葉嘉莹](../Page/葉嘉莹.md "wikilink")：中国诗人、学者、教授
  - [葉永烈](../Page/葉永烈.md "wikilink")：中國作家、教授
  - [葉盛兰](../Page/葉盛兰.md "wikilink")：中國京剧表演艺术家
  - [葉少兰](../Page/葉少兰.md "wikilink")：中國京剧演员、戏剧表演艺术家
  - [葉迎春](../Page/葉迎春.md "wikilink")：中國播音员、主持人
  - [葉君健](../Page/葉君健.md "wikilink")：中國作家、文学翻译家
  - [葉一茜](../Page/葉一茜.md "wikilink")：中國演员、歌手
  - [葉小纲](../Page/葉小纲.md "wikilink")：中國作曲家、音乐家
  - [葉小文](../Page/葉小文.md "wikilink")：中國宗教人士
  - [葉笃正](../Page/葉笃正.md "wikilink")：中國气象学家

### 港澳地區

  - [葉競生](../Page/葉競生.md "wikilink")：香港藝人八両金本名
  - [葉蘊儀](../Page/葉蘊儀.md "wikilink")：香港女藝人，1980年代末的當紅玉女明星
  - [葉德嫻](../Page/葉德嫻.md "wikilink")：香港女演員
  - [葉玉卿](../Page/葉玉卿.md "wikilink")：香港女演員
  - [葉振聲](../Page/葉振聲.md "wikilink")：香港男演員、配音員
  - [葉曉欣](../Page/葉曉欣.md "wikilink")：香港配音員
  - [葉特生](../Page/葉特生.md "wikilink")：1980年代《香港早晨》節目主持人，現移民國外
  - [葉澍堃](../Page/葉澍堃.md "wikilink")：香港前經濟發展及勞工局局長
  - [葉漢](../Page/葉漢.md "wikilink")：港澳地區著名企業家
  - [葉詠詩](../Page/葉詠詩.md "wikilink")：香港指揮家
  - [葉麗儀](../Page/葉麗儀.md "wikilink")：香港歌手
  - [葉子楣](../Page/葉子楣.md "wikilink")：香港女演員，豔星
  - [葉蒨文](../Page/葉蒨文.md "wikilink")：香港紅極一時的天后級歌手
  - [葉璇](../Page/葉璇.md "wikilink")：香港女演員，内地女製片人，大學教授
  - [葉翠翠](../Page/葉翠翠.md "wikilink")：香港小姐冠軍
  - [葉文輝](../Page/葉文輝.md "wikilink")：[新城電台DJ](../Page/新城電台.md "wikilink")
  - [葉雅媛](../Page/葉雅媛.md "wikilink")：[香港資深傳媒人](../Page/香港.md "wikilink")
  - [葉問](../Page/葉問.md "wikilink")：詠春拳宗師
  - [葉念琛](../Page/葉念琛.md "wikilink")：香港電影導演、演員及影評人
  - [葉繼歡](../Page/葉繼歡.md "wikilink")：臭名昭著香港罪犯，曾牽涉多宗持械行劫案，服刑期間患末期[肺癌病逝](../Page/肺癌.md "wikilink")
  - [葉維義](../Page/葉維義.md "wikilink")：香港商人、前行政會議成員
  - [葉紹德](../Page/葉紹德.md "wikilink")：著名粵劇編劇家
  - [葉鴻輝](../Page/葉鴻輝.md "wikilink")：香港足球運動員，在2009年東亞運動會中代表香港隊獲得金牌
  - [葉世榮](../Page/葉世榮.md "wikilink")：前香港殿堂級搖滾樂隊[Beyond的鼓手](../Page/Beyond.md "wikilink")
  - [葉楚航](../Page/葉楚航.md "wikilink")：2012-2013年香港冠軍練馬師
  - [葉凱茵](../Page/葉凱茵.md "wikilink")：香港女演員
  - [葉鎮輝](../Page/葉鎮輝.md "wikilink")：香港無綫電視資深知名監製

### 臺灣

  - [葉公超](../Page/葉公超.md "wikilink")：中華民國外交部長
  - [葉瑋庭](../Page/葉瑋庭.md "wikilink")：[臺灣著名華語流行音樂女歌手](../Page/臺灣.md "wikilink")。
  - [葉金川](../Page/葉金川.md "wikilink")：[臺灣中華民國紅十字會總會副會長](../Page/臺灣.md "wikilink")，前任中華民國行政院衛生署署長。
  - [葉啟田](../Page/葉啟田.md "wikilink")：[臺灣著名台語男歌手](../Page/臺灣.md "wikilink")，有寶島歌王之稱
  - [葉佳修](../Page/葉佳修.md "wikilink")：[臺灣校園民歌作曲填詞人](../Page/臺灣.md "wikilink")
  - [葉慶炳](../Page/葉慶炳.md "wikilink")：曾任[臺灣大學中文系主任](../Page/臺灣.md "wikilink")
  - [葉秉桓](../Page/葉秉桓.md "wikilink")：[臺灣著名華語流行音樂男歌手](../Page/臺灣.md "wikilink")。
  - [葉璦菱](../Page/葉璦菱.md "wikilink")：[臺灣女歌手](../Page/臺灣.md "wikilink")、節目主持人，本名葉美娟。
  - [葉華成](../Page/葉華成.md "wikilink")：金門高粱酒鼻祖，[金城酒廠创立者](../Page/金城酒廠.md "wikilink")。
  - [葉舒華](../Page/葉舒華.md "wikilink")：韓國女子團體[(G)I-DLE成員](../Page/\(G\)I-DLE.md "wikilink")。
  - [方季惟](../Page/方季惟.md "wikilink")：[臺灣女歌手](../Page/臺灣.md "wikilink")、女演員，"永遠軍中情人"，本名葉純華。

### 世界各地

  - [葉亞來](../Page/葉亞來.md "wikilink")：吉隆坡第三任[甲必丹](../Page/甲必丹.md "wikilink")，是吉隆坡的開埠功臣
  - [葉永烜](../Page/葉永烜.md "wikilink")：德籍華人天文學家

## 註釋

[\*](../Category/葉姓.md "wikilink") [Y葉](../Category/漢字姓氏.md "wikilink")
[Category:越南姓氏](../Category/越南姓氏.md "wikilink")
[Category:朝鮮語姓氏](../Category/朝鮮語姓氏.md "wikilink")

1.  \[[http://www.bsm.org.cn/show_article.php?id=866\#_ednref40\]此段文字舛誤甚多，如它認為沈尹戌等人名中沈是氏稱、尹戊是名，與](http://www.bsm.org.cn/show_article.php?id=866#_ednref40%5D此段文字舛誤甚多，如它認為沈尹戌等人名中沈是氏稱、尹戊是名，與)《[左傳](../Page/左傳.md "wikilink")》和出土文獻多處以沈尹為氏不符；姬姓沈氏奔楚的敍述，與典籍記載出入較大，亦不可信\[20\]。這條來源不明、存在杜撰痕跡的材料，鄭樵斥其為“野書之言，無足取也
2.  [百家姓—–葉氏的起源](http://www.epochtimes.com/b5/2/9/26/n233010.htm)
3.  [Yip
    Surname](http://www.cantonese.sheik.co.uk/phorum/read.php?1,66445,66448#msg-66448)
4.  [족보소개‧섭(葉)씨(族譜紹介‧葉氏)](http://translate.google.ca/translate?hl=en&sl=ko&tl=zh-TW&u=http://web.archive.org/web/20041019184211/http://www.giljae.com/jokbo_new/index_sa_seop.php3)
    註：譯文以「Seo先生」及「攝氏」稱「섭(葉)」，本文按原文更正。