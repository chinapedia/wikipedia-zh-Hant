[Ye_Jiangchuan.jpg](https://zh.wikipedia.org/wiki/File:Ye_Jiangchuan.jpg "fig:Ye_Jiangchuan.jpg")
**叶江川**（），前[中国](../Page/中国.md "wikilink")[国际象棋棋手](../Page/国际象棋.md "wikilink")，现任中国国际象棋国家队总教练。出生于[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[无锡市](../Page/无锡市.md "wikilink")，后迁居至[山西省](../Page/山西省.md "wikilink")[太原市](../Page/太原市.md "wikilink")。1974年开始学习[中国象棋](../Page/中国象棋.md "wikilink")。1977年底跟随[王品璋改学国际象棋](../Page/王品璋.md "wikilink")。后入选[山西省国际象棋代表队](../Page/山西省.md "wikilink")。1986年底，叶江川进入国际象棋中国国家集训队。

1981年叶江川首次参加[全国国际象棋个人赛](../Page/全国国际象棋锦标赛.md "wikilink")，并获得冠军。此后他还获得1984年、1986年、1987年、1989年、1994年和1996年六届全国国际象棋个人赛冠军。

1993年，叶江川被[世界国际象棋联合会](../Page/世界国际象棋联合会.md "wikilink")（FIDE）批准为[国际特级大师](../Page/国际特级大师.md "wikilink")。他也是中国第一个[国际象棋等级分超过](../Page/国际象棋等级分.md "wikilink")2600分的棋手。2000年世界排名第17，创中国棋手的最高纪录。

叶江川曾与[国际象棋的两位世界冠军](../Page/国际象棋.md "wikilink")[卡斯帕罗夫和](../Page/卡斯帕罗夫.md "wikilink")[卡尔波夫战平](../Page/卡尔波夫.md "wikilink")，并战胜过两位世界亚军：[瑞士的](../Page/瑞士.md "wikilink")[科尔奇诺依和](../Page/科尔奇诺依.md "wikilink")[英国的](../Page/英国.md "wikilink")[肖特](../Page/肖特.md "wikilink")。

叶江川的棋艺特点是力求主动，善于扭杀，战术手段多变，算度精深。

1988年底起，他开始担任[谢军的教练](../Page/谢军_\(棋手\).md "wikilink")。为谢军多次获得世界国际象棋女子冠军做出了不可磨灭的贡献，也为中国女子的国际象棋水平的飞速发展和提高贡献了他自己的力量。

[category:无锡人](../Page/category:无锡人.md "wikilink")

[Category:中國國際象棋棋手](../Category/中國國際象棋棋手.md "wikilink")
[Category:中國之最](../Category/中國之最.md "wikilink")
[Category:国际象棋特级大师](../Category/国际象棋特级大师.md "wikilink")
[J](../Category/叶姓.md "wikilink")
[Category:中国国际象棋协会秘书长](../Category/中国国际象棋协会秘书长.md "wikilink")