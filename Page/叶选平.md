[谢非](../Page/谢非.md "wikilink") | predecessor2 =
[梁灵光](../Page/梁灵光.md "wikilink") | successor2 =
[朱森林](../Page/朱森林_\(广东省长\).md "wikilink") | office3 =
[广州市人民政府](../Page/广州市人民政府.md "wikilink")[市长](../Page/广州市市长列表.md "wikilink")
| period3 = 1983年7月 - 1985年8月 | 1blankname3 = 市委书记 | 1namedata3 =
[许士杰](../Page/许士杰.md "wikilink") | predecessor3 =
[梁灵光](../Page/梁灵光.md "wikilink") | successor3 =
[朱森林](../Page/朱森林_\(广东省长\).md "wikilink") | Sex = 男 | different
name = | nationality = | party =  | Native place =
[广东省](../Page/广东省.md "wikilink")[梅县](../Page/梅县.md "wikilink")
| date of birth =  | place of birth
=[广东省](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")
| date of death = | place of death = | parents =
[叶剑英](../Page/叶剑英.md "wikilink")
冯华 | spouse = 吴小兰 | children = 叶新福 | relatives = 叶楚梅
[叶选宁](../Page/叶选宁.md "wikilink")（弟） | educate =

  - 延安自然科学院机械系

| past =

  - <span style="color: blue;">广州市市长
    (1983年-1985年)</span>
  - <span style="color: blue;">广东省省长
    (1985年-1991年)</span>
  - <span style="color: blue;">全国政协副主席
    (1991年4月-2003年3月)</span>

| work = }}

**叶选平**（
）\[1\]，[广东](../Page/广东.md "wikilink")[梅县人](../Page/梅县.md "wikilink")，[中华人民共和国元帅](../Page/中华人民共和国元帅.md "wikilink")[叶剑英的长子](../Page/叶剑英.md "wikilink")；[延安自然科学院机械系毕业](../Page/延安自然科学院.md "wikilink")。1945年9月，加入[中国共产党](../Page/中国共产党.md "wikilink")；是中共第十二届候补中央委员，第十三届、十四届中央委员；曾任[广东省省长](../Page/广东省省长.md "wikilink")、[广州市市长等职](../Page/广州市市长.md "wikilink")。1991年4月，被增选为第七届[全国政协副主席](../Page/全国政协副主席.md "wikilink")；后连任第八届、第九届全国政协副主席（排名第一）。

## 早年经历

叶选平1924年12月20日出生在[广州市](../Page/广州市.md "wikilink")，后在家乡梅县雁洋下虎村生活；曾就读于[广东省](../Page/广东省.md "wikilink")[中山市](../Page/中山市.md "wikilink")[中山纪念中学](../Page/中山纪念中学.md "wikilink")。1939年，赴[重庆](../Page/重庆.md "wikilink")。[皖南事变发生后](../Page/皖南事变.md "wikilink")，经中共组织安排转往[延安](../Page/延安.md "wikilink")，和父亲叶剑英团聚，并进入[延安自然科学院机械系学习](../Page/延安自然科学院.md "wikilink")，是该校第一批学生。1945年毕业后，被分配到延安兵工厂工作，成为一名机械师，并于该年9月加入[中国共产党](../Page/中国共产党.md "wikilink")。[国共第二次内战时期](../Page/国共第二次内战.md "wikilink")，曾在[晋绥边区第一机械厂工作](../Page/晋绥边区.md "wikilink")。

## 留苏工程师

[中华人民共和国建国后](../Page/中华人民共和国.md "wikilink")，叶选平先后在[哈尔滨工业大学和](../Page/哈尔滨工业大学.md "wikilink")[清华大学进行过短期学习](../Page/清华大学.md "wikilink")；1950年，调入当时中国最大的机床厂—[原沈阳第一机床厂工作](../Page/沈阳机床集团有限责任公司.md "wikilink")。1952年，被派往[苏联学习进修机床专业](../Page/苏联.md "wikilink")；1954年回国后，先后在[沈阳和](../Page/沈阳.md "wikilink")[北京担任两家机床厂的总工程师](../Page/北京.md "wikilink")。曾历任沈阳第一机床厂副厂长、兼总工程师，沈阳市机械局副总工程师，北京第一机床厂生产技术副厂长、兼总工程师等职。

## 仕途

1973年，49岁的叶选平出任北京市机械局领导小组副组长、党委常委，开始步入政界。[文化大革命结束后](../Page/文化大革命.md "wikilink")，于1977年进入[中共中央党校学习](../Page/中共中央党校.md "wikilink")；1978年，出任[国家科委三局局长](../Page/国家科委.md "wikilink")。1979年，叶选平调往[广东省工作](../Page/广东省.md "wikilink")，出任副省长、兼省科委主任；1983年，任[中共广州市委副书记](../Page/中共广州市委.md "wikilink")、市长；1985年，晋升[广东省省长](../Page/广东省省长.md "wikilink")，成为[改革开放初期](../Page/改革开放.md "wikilink")，广东省的主要领导之一。1988年1月12日，在广东省七届人大第一次会议上，叶选平连任广东省省长；在750张有效票选票中，获得了746张\[2\]。

在中共大力提倡干部年轻化的政策下，叶选平在67岁时，才卸任省长职务；并于1991年4月，在第七届[全国政协第四次会议上](../Page/全国政协.md "wikilink")，增选为[全国政协副主席](../Page/全国政协副主席.md "wikilink")。此后，他分别于1993年和1998年连任第八届、第九届全国政协副主席（排名第一位）。

## 家庭

父亲是[中华人民共和国元帅](../Page/中华人民共和国元帅.md "wikilink")[叶剑英](../Page/叶剑英.md "wikilink")，生母是冯华。

妻子[吴小兰](../Page/吴小兰.md "wikilink")，延安“五老”之一的[吴玉章的外甥女](../Page/吴玉章.md "wikilink")，是一位机械专家，曾任[中国机械进出口公司副总经理](../Page/中国机械进出口公司.md "wikilink")、[深圳市副市长](../Page/深圳市.md "wikilink")、深圳市人大常委会副主任。\[3\]

儿子[叶新福](../Page/叶新福.md "wikilink")，1992年在香港创立[万信证券](../Page/万信证券.md "wikilink")。\[4\]现任广东南湖高尔夫球会董事长，另一子姓名职业不详。

孙子[叶仲豪](../Page/叶仲豪.md "wikilink")。广东云浮市高新区主任。\[5\]
另一名为叶丁。[1](http://politics.caijing.com.cn/20140812/3652749.shtml)

## 参考文献

## 外部链接

{{-}}

[Category:中共廣東省委副書記](../Category/中共廣東省委副書記.md "wikilink")
[Category:第七届全国政协副主席](../Category/第七届全国政协副主席.md "wikilink")
[Category:第八届全国政协副主席](../Category/第八届全国政协副主席.md "wikilink")
[Category:第九届全国政协副主席](../Category/第九届全国政协副主席.md "wikilink")
[Category:中國共產黨第十三屆中央委員會委員](../Category/中國共產黨第十三屆中央委員會委員.md "wikilink")
[Category:中國共產黨第十四屆中央委員會委員](../Category/中國共產黨第十四屆中央委員會委員.md "wikilink")
[Category:中國共產黨第十二屆中央委員會候补委員](../Category/中國共產黨第十二屆中央委員會候补委員.md "wikilink")
[Category:第五届全国人大代表](../Category/第五届全国人大代表.md "wikilink")
[Category:第六届全国人大代表](../Category/第六届全国人大代表.md "wikilink")
[Category:中國政治人物第二代](../Category/中國政治人物第二代.md "wikilink")
[Category:北京理工大学校友](../Category/北京理工大学校友.md "wikilink")
[Category:叶剑英家族](../Category/叶剑英家族.md "wikilink")
[Category:广东客家人](../Category/广东客家人.md "wikilink")
[Category:梅縣區人](../Category/梅縣區人.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")
[Xuan选](../Category/叶姓.md "wikilink")

1.  [深圳市梅縣商會《恭贺叶选平首长生日快乐》](http://www.szmxsh.org/newsView.aspx?cid=35&id=42)
2.
3.
4.  [联合早报：国叶拟收购宝雅科技借壳上市](http://www.zaobao.com/zaobao/pages/hkjj0305.html)
5.