**司法管轄權**（jurisdiction），或稱為**審判權**，是指[法院或](../Page/法院.md "wikilink")[司法機構對](../Page/司法.md "wikilink")[訴訟進行聆訊和審判的權力](../Page/訴訟.md "wikilink")。在大部份地區，不同法院的司法管轄權是不同的，通常以區域和類別劃分。例如[區域法院只能聆訊在該地區發生的訴訟](../Page/區域法院.md "wikilink")，錢債法院只能聆訊和金錢糾紛有關的訴訟，軍事法庭只能聆訊和[軍人有關的訴訟等](../Page/軍人.md "wikilink")。司法管轄權亦和法院級別有關；例如上級法院可聆訊不服下級法院判決的上訴案件，反之則不可以。

## 管辖权的分类

  - 属人管辖权

凡具有某国国籍或护照的公民，某国对其即有司法管辖权。

  - 属地管辖权

凡在某国的国境之内，该国即有司法管辖权。

  - 保护管辖权

凡刑事案件中被害人为某国公民的，该国即有司法管辖权。实际操作上，在国外对本国公民实施犯罪，而又未能引渡的，则无法对其进行司法管辖。

  - 普遍管辖权

对国际社会上一切犯罪皆有司法管辖权。普遍管辖一般比较难执行，但随着国际犯罪的增多，各国往往通过缔结共同的国际条约来打击这些犯罪，缔结或参与国际条约的国家，对条约中所规定的犯罪即可行使普遍管辖权。

## 地区事例

### 香港

在[香港](../Page/香港.md "wikilink")，根據《[基本法](../Page/香港基本法.md "wikilink")》，法院（包括最高級原訟法院——[香港高等法院](../Page/香港高等法院.md "wikilink")）對[中華人民共和國](../Page/中華人民共和國.md "wikilink")[外交和](../Page/外交.md "wikilink")[國防等](../Page/國防.md "wikilink")[國家行為無司法管轄權](../Page/國家行為.md "wikilink")。

### [台灣詐騙集團的司法管辖权争议](../Page/台灣詐騙集團.md "wikilink")

在2011年至2016年期间，部分台湾人在第三方境内使用电话诈骗中国大陆居民，导致被当地警方或联合中国大陆警方抓捕，因为司法管辖权问题，或者当地警方遣返至中国大陆或台湾；或被中国大陆警方引渡或押回中国大陆受审；或被台湾警方押回台湾受审或释放。该争议由于[2016年肯亞遣送台灣人至中國大陸地區事件而再次受到两岸媒体和行政机构的关注](../Page/2016年台灣詐騙集團成員涉嫌電信詐騙案.md "wikilink")。

## 参考文献

## 外部連結

  - [Cornell.edu](https://www.law.cornell.edu/topics/jurisdiction.html)
  - [Supreme Court
    Decision](https://web.archive.org/web/20120424110014/http://www.cdi.org/news/law/gtmo-sct-decision.cfm)
    on the Guantánamo Bay jurisdiction
  - [Jurisdiction As
    Property](https://web.archive.org/web/20091001030343/http://szabo.best.vwh.net/JurisdictionAsProperty.pdf)
    – franchise jurisdiction

[ka:გერმანიის
მართლმსაჯულება](../Page/ka:გერმანიის_მართლმსაჯულება.md "wikilink")

[Category:司法](../Category/司法.md "wikilink")