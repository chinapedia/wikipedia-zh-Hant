**司馬中原**（），本名**吳延玫**，原籍[江蘇](../Page/江蘇省_\(中華民國\).md "wikilink")[淮陰](../Page/淮陰市.md "wikilink")，生於[南京](../Page/南京市_\(中華民國\).md "wikilink")，是[台灣](../Page/台灣.md "wikilink")[作家](../Page/作家.md "wikilink")、[繪本作家](../Page/繪本.md "wikilink")\[1\]。

## 簡介

司馬中原本籍江蘇[淮陰](../Page/淮陰.md "wikilink")。1948年即以15歲年紀參與[國共內戰](../Page/國共內戰.md "wikilink")，未受任何正統[學校教育](../Page/學校教育.md "wikilink")。後隨[中華民國國軍遷徙至](../Page/中華民國國軍.md "wikilink")[台灣的他](../Page/台灣.md "wikilink")，因[自學後的文筆卓越](../Page/自學.md "wikilink")，於是擔任師旅新聞官等文職宣傳工作，與[段彩華](../Page/段彩華.md "wikilink")、[朱西甯號稱](../Page/朱西甯.md "wikilink")“軍中三劍客”。1962年以[上尉軍銜退役](../Page/上尉.md "wikilink")。退役後專事寫作，寫作範圍相當寬廣。

司馬中原是1950年代[文學代表作家之一](../Page/文學.md "wikilink")，其鄉野、懷舊、[武俠](../Page/武俠.md "wikilink")、《[聊齋](../Page/聊齋誌異.md "wikilink")》式[鬼怪通俗](../Page/鬼怪.md "wikilink")[小說均頗有特殊之處](../Page/小說.md "wikilink")。其主要作品有《狂風沙》、《荒原》、《失去監獄的囚犯》、《月光河》、《駝鈴》、《雲上的聲音》、《路客與刀客》、《大漠英雄傳》、《鄉野奇談》、《鬼話》、《醫院鬼話》、《春遲》等；其中，《春遲》獲得第22屆[國家文藝獎](../Page/國家文藝獎.md "wikilink")。另外，描寫[斑鳩的散文](../Page/斑鳩.md "wikilink")《火鷓鴣鳥》（選自散文集《雲上的聲音》）則被[國立編譯館選入](../Page/國立編譯館.md "wikilink")[國民中學](../Page/國民中學.md "wikilink")[國文](../Page/國文.md "wikilink")[教科書](../Page/教科書.md "wikilink")，被視為「以文字描繪[聲音](../Page/聲音.md "wikilink")」的極佳示範文學作品。

司馬中原現任[中華語文著作權仲介協會](../Page/中華語文著作權仲介協會.md "wikilink")[董事長](../Page/董事長.md "wikilink")，並在大學內兼任講師。

## 主持節目

司馬中原曾經在[衛視中文台主持兩個](../Page/衛視中文台.md "wikilink")[靈異節目](../Page/靈異節目.md "wikilink")：《今夜鬼未眠》（與[郝劭文主持](../Page/郝劭文.md "wikilink")）、《驚夜嚇嚇叫》（[類戲劇](../Page/類戲劇.md "wikilink")），也曾主持[中國廣播公司深夜節目](../Page/中國廣播公司.md "wikilink")《午夜奇譚》。在《午夜奇譚》中，司馬中原把自己親耳聽過或親眼見過的一些光怪陸離、鄉野奇譚說給聽眾聽，是當時熱門的廣播節目之一。

## 戲仿

藝人[郭子乾在](../Page/郭子乾.md "wikilink")[中華電視公司](../Page/中華電視公司.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[綜藝萬花筒](../Page/綜藝萬花筒.md "wikilink")》扮演「司馬湯圓」[戲仿司馬中原](../Page/戲仿.md "wikilink")，一句[台詞](../Page/台詞.md "wikilink")「中國人怕鬼，西洋人也怕鬼，全世界的人都怕鬼。恐怖喔！恐怖到了極點喔！」相當膾炙人口。

## 作品

  - 鄉野傳說系列

<!-- end list -->

  - 《路客與刀客》
  - 《紅絲鳳》
  - 《天網》
  - 《荒鄉異聞》
  - 《十八里旱湖》

<!-- end list -->

  - 秉燭夜談系列

<!-- end list -->

  - 《遇邪記》
  - 《復仇》
  - 《呆虎傳》
  - 《野狼噑月》
  - 《闖將》
  - 《挑燈練膽》

<!-- end list -->

  - 其他

<!-- end list -->

  - 《石鼓莊》
  - 《狂風沙》
  - 《青春行》
  - 《煙雲》
  - 《綠楊村》
  - 《啼明鳥》
  - 《荒原》
  - 《割緣》
  - 《巫蠱》
  - 《靈河》
  - 《鄉思井》
  - 《失去監獄的囚犯》
  - 《孽種》
  - 《湘東野話》
  - 《月光河》
  - 《駝鈴》
  - 《雲上的聲音》
  - 《大漠英雄傳》
  - 《鄉野奇談》
  - 《鬼話》
  - 《醫院鬼話》
  - 《春遲》

## 演出

  - 2013年[賀歲片](../Page/賀歲片.md "wikilink")《[變身](../Page/變身_\(台灣電影\).md "wikilink")》正式預告片擔任旁白
  - 2013年8月[全聯福利中心](../Page/全聯福利中心.md "wikilink")「Smart[中元節](../Page/中元節.md "wikilink")」電視廣告代言

## 軼聞

據司馬中原表示，他轉世時沒喝[孟婆湯](../Page/孟婆湯.md "wikilink")，因此帶[前世記憶](../Page/前世.md "wikilink")[投胎](../Page/投胎.md "wikilink")\[2\]。

## 參考文獻

  - [應鳳凰](../Page/應鳳凰.md "wikilink")，《[台灣文學花園](../Page/台灣文學花園.md "wikilink")》，2003，[台北](../Page/台北.md "wikilink")，[玉山社](../Page/玉山社.md "wikilink")

## 外部連結

  - [作家正名》軍中作家封號
    司馬中原不在乎](http://mag.udn.com/mag/reading/storypage.jsp?f_ART_ID=194152)

[Category:台灣作家](../Category/台灣作家.md "wikilink")
[Category:台灣廣播主持人](../Category/台灣廣播主持人.md "wikilink")
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:國家文藝獎得主](../Category/國家文藝獎得主.md "wikilink")
[Category:台灣戰後南京移民](../Category/台灣戰後南京移民.md "wikilink")
[Category:台灣戰後江蘇移民](../Category/台灣戰後江蘇移民.md "wikilink")
[Category:臺灣繪本作家](../Category/臺灣繪本作家.md "wikilink")
[Category:淮安人](../Category/淮安人.md "wikilink")
[Category:南京人](../Category/南京人.md "wikilink")
[Category:吳姓](../Category/吳姓.md "wikilink")

1.  [童話列車，從台灣駛來，帶你進入奇妙的童話世界！](http://m.sohu.com/n/441484355/?_trans_=000115_3w)
2.