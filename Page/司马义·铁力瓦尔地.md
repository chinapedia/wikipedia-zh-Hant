**司马义·铁力瓦尔地**（，[拉丁维文](../Page/拉丁维文.md "wikilink")：Isma'il
Tiliwaldi，），男，[维吾尔族](../Page/维吾尔族.md "wikilink")。[中国](../Page/中华人民共和国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[喀什地区](../Page/喀什.md "wikilink")[疏附人](../Page/疏附县.md "wikilink")。1973年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，1967年9月参加工作，[新疆大学数学专业毕业](../Page/新疆大学.md "wikilink")，大学学历。曾任[新疆维吾尔自治区人民政府主席](../Page/新疆维吾尔自治区人民政府.md "wikilink")、第十一届[全国人大常委会副委员长](../Page/全国人大常委会副委员长.md "wikilink")。中国共产党第十六屆候補中央委員，第十七屆中央委員。

## 生平

1962年，铁力瓦尔地进入[新疆大学](../Page/新疆大学.md "wikilink")[数学系学习](../Page/数学.md "wikilink")，毕业后[下放至](../Page/下放.md "wikilink")[疏附县站敏公社接受再教育](../Page/疏附县.md "wikilink")。1968年在拖拉机站、塔什米力克公社担任翻译。1973年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，并进入疏附县县委任组织部干事，1976年升任喀什地委组织部干事。1980年，任喀什政府人事处副处长，后升任处长。1983年升任[喀什地区副](../Page/喀什地区.md "wikilink")[专员](../Page/专员.md "wikilink")，1990年升任喀什地委副书记、行署专员。

1993年，铁力瓦尔地任[新疆维吾尔自治区人民政府秘书长](../Page/新疆维吾尔自治区人民政府.md "wikilink")，1995年调任[新疆生产建设兵团党委常委](../Page/新疆生产建设兵团.md "wikilink")、副政治委员。1998年升任中共新疆自治区党委常委、[政法委副书记](../Page/政法委.md "wikilink")，2000年任自治区党委副书记。2003年1月至2007年12月，任新疆自治区政府主席。2008年3月當選為第十一屆[全國人大常委會副委員長](../Page/全國人大常委會副委員長.md "wikilink")，2013年3月卸任。

## 参考文献

{{-}}

[Category:中华人民共和国領導人](../Category/中华人民共和国領導人.md "wikilink")
[Category:维吾尔族全国人大常委会副委员长](../Category/维吾尔族全国人大常委会副委员长.md "wikilink")
[Category:新疆维吾尔自治区人民政府主席](../Category/新疆维吾尔自治区人民政府主席.md "wikilink")
[Category:中华人民共和国时期维吾尔族中国共产党党员](../Category/中华人民共和国时期维吾尔族中国共产党党员.md "wikilink")
[Category:新疆大学校友](../Category/新疆大学校友.md "wikilink")
[Category:疏附人](../Category/疏附人.md "wikilink")
[Category:维吾尔族人](../Category/维吾尔族人.md "wikilink")