**合浦**（[邮政式拼音](../Page/邮政式拼音.md "wikilink")：），舊稱**[廉州](../Page/廉州.md "wikilink")**（[邮政式拼音](../Page/邮政式拼音.md "wikilink")：），是[中国](../Page/中国.md "wikilink")[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[北海市所辖的一个](../Page/北海市.md "wikilink")[县](../Page/县.md "wikilink")，位于广西壮族自治区东南部。东南部与[广东省](../Page/广东省.md "wikilink")[廉江接壤](../Page/廉江.md "wikilink")，东北部与[博白县相邻](../Page/博白县.md "wikilink")，南部东西临海，中部与[北海市毗邻](../Page/北海市.md "wikilink")，西接[钦州市](../Page/钦州市.md "wikilink")，北部与[浦北县交界](../Page/浦北县.md "wikilink")。总面积2380[平方公里](../Page/平方公里.md "wikilink")，人口930,914（2003年），以[汉族为主](../Page/汉族.md "wikilink")，有少量[壮](../Page/壮族.md "wikilink")、[苗等](../Page/苗族.md "wikilink")[少数民族](../Page/少数民族.md "wikilink")\[1\]。

“合浦”意为江河汇集入海之处。而[成語](../Page/成語.md "wikilink")「珠还合浦」中「合浦」也是出自此縣。

## 历史

早在五千年前的[新石器时代](../Page/新石器.md "wikilink")，合浦地区就有人类活动，在西沙坡新石器遗址曾发掘出肩[石斧](../Page/石斧.md "wikilink")、石锛、石刀、夹砂红褐陶和灰褐陶等原始人类用具。

[秦统一中国前](../Page/秦.md "wikilink")，合浦称为“[百越之地](../Page/百越.md "wikilink")”。

[秦始皇三十三年](../Page/秦始皇.md "wikilink")（前214年），秦军统一[岭南](../Page/岭南.md "wikilink")，置南海、桂林、象郡。合浦属象郡辖地。

[西汉](../Page/西汉.md "wikilink")[元鼎六年](../Page/元鼎.md "wikilink")（前111年），[汉武帝平](../Page/汉武帝.md "wikilink")[南越](../Page/南越.md "wikilink")，划出南海、象郡交界处设置[合浦郡](../Page/合浦郡.md "wikilink")，郡治[徐闻](../Page/徐闻.md "wikilink")（今[广东省](../Page/广东省.md "wikilink")[海康县地域](../Page/海康县.md "wikilink")），同时设合浦县。

[三国](../Page/三国.md "wikilink")[吴](../Page/吴.md "wikilink")[黄武七年](../Page/黄武.md "wikilink")（228年）合浦郡改称珠官郡，不久复称合浦郡。

[隋煬帝](../Page/隋煬帝.md "wikilink")[大業元年](../Page/大業.md "wikilink")（605年）設置[祿州](../Page/祿州.md "wikilink")，又併入[合州](../Page/合州.md "wikilink")。

[唐](../Page/唐.md "wikilink")[贞观八年](../Page/贞观.md "wikilink")（634年）合浦称[廉州](../Page/廉州_\(唐朝\).md "wikilink")。

[元](../Page/元.md "wikilink")[至元十七年](../Page/至元.md "wikilink")（1280年）改设[廉州路](../Page/廉州路.md "wikilink")。

[明](../Page/明.md "wikilink")[洪武元年](../Page/洪武.md "wikilink")（1368年）至[清设](../Page/清.md "wikilink")[廉州府](../Page/廉州府.md "wikilink")，隶属广东省。

[中华民国时期](../Page/中华民国.md "wikilink")（1911年－1949年），合浦县先后隶属于广东省钦廉道、南区[绥靖公署和第八区行政督察专员公署](../Page/绥靖公署.md "wikilink")。

[中华人民共和国时期](../Page/中华人民共和国.md "wikilink")：1949年12月中國人民解放軍進駐合浦县，仍属广东省。1951年5月10日将合浦县下辖的北海东镇、西镇、高德乡、[涠洲乡划出设北海市](../Page/涠洲.md "wikilink")。1952年划归广西。1955年划归广东。1965年再次划归广西，隶属于钦州专区。1987年7月合浦县改由北海市管辖。1988年3月被[国务院批准为沿海开放县](../Page/国务院.md "wikilink")。1994年12月17日，合浦县的福成镇划归北海市[银海区管辖](../Page/银海区.md "wikilink")；南康镇、营盘镇划归北海市[铁山港区管辖](../Page/铁山港区.md "wikilink")。

## 自然环境

### 地理

西部为[南流江](../Page/南流江.md "wikilink")[冲积平原](../Page/冲积平原.md "wikilink")，同时也是广西壮族自治区最大的[三角洲](../Page/三角洲.md "wikilink")。北部和中部为[丘陵地带](../Page/丘陵.md "wikilink")。东部包括[铁山港和周边的滨海](../Page/铁山港.md "wikilink")[平原](../Page/平原.md "wikilink")。[海拔](../Page/海拔.md "wikilink")100米以下的平原、[台地和低丘陵地占全县总面积的](../Page/台地.md "wikilink")92％。

### 气候

合浦县位于[北回归线以南](../Page/北回归线.md "wikilink")，属[亚热带](../Page/亚热带.md "wikilink")[季风型海洋性气候区](../Page/季风.md "wikilink")，日照较强，热量充足，雨量充沛，夏热冬暖，无[霜期长](../Page/霜.md "wikilink")。气候受季风环流控制，雨热同季。冬干夏湿，夏无酷暑，冬无严寒，盛行[风向有明显的季节性转换](../Page/风向.md "wikilink")。在沿海乡镇还有昼夜交替的海陆风出现。

1955-2007年统计，年平均日照总时数为1927.1小时，年均[气温](../Page/气温.md "wikilink")23.0℃，极端最高气温37.7℃，极端最低气温-0.8℃。年均[降雨量](../Page/降雨量.md "wikilink")1500－1800毫米，[相对湿度](../Page/相对湿度.md "wikilink")75%以上。县境全年的风向北风最多，出现频率达20.7%\[2\]。

### 动植物

合浦县境内有多种类型野生生态环境：山地、滩涂、浅海、丘陵、平原等，拥有众多珍稀野生动植物，包括：

  - [儒艮](../Page/儒艮.md "wikilink")
  - [中华白海豚](../Page/中华白海豚.md "wikilink")
  - [文昌鱼](../Page/文昌鱼.md "wikilink")
  - [星格沙虫](../Page/沙蚕.md "wikilink")（俗称沙虫）
  - [绿海龟](../Page/绿海龟.md "wikilink")
  - [斑海馬](../Page/斑海馬.md "wikilink")\[3\]
  - 普通野生[稻](../Page/稻.md "wikilink")\[4\]

目前合浦县境内设有两个[国家级自然保护区](../Page/国家级自然保护区.md "wikilink")：

  - [合浦营盘港-英罗港儒艮国家级自然保护区](../Page/合浦营盘港-英罗港儒艮国家级自然保护区.md "wikilink")
  - [山口红树林生态国家级自然保护区](../Page/山口红树林生态国家级自然保护区.md "wikilink")

## 行政区划

合浦縣下辖有14个鎮、1个鄉：

  - 鎮：廉州鎮、党江鎮、沙岗鎮、西场鎮、乌家鎮、星岛湖鎮、石湾鎮、石康鎮、常乐鎮、闸口鎮、公館鎮、白沙鎮、山口鎮、沙田鎮。

<!-- end list -->

  - 鄉：曲樟鄉。

## 教育

### 民國

在廣東省第八行政區時期，全縣中小學教育發達，省立廉州中學和第一（北海）中學早年畢業學生可以到省城、上海、南京、北平等等地升學。其中中學設立大致如下\[5\]：

**公立學校**

  - 省立廉州中學

<!-- end list -->

  - 縣立第一（北海）中學

<!-- end list -->

  - 縣立第二（福旺）中學

<!-- end list -->

  - 縣立第三（南康）中學

<!-- end list -->

  - 縣立第四（張黃）中學

<!-- end list -->

  - 縣立第五（公館）中學
  - 縣立第六（石康）中學
  - 县立第七(浦北寨圩) 中学

**私立學校**\[6\]

  - 私立正诚初级中学(浦北小江)<small>1944年改县立第四中学</small>
  - 中华圣公会圣三一中学(北海)
  - 私立文德初级中学(浦北平睦) 。
  - 私立屯英初中(浦北旧州)
  - 私立乾体中学(乾江)
  - 私立旭初初级中学(北海)
  - 私立润珠初级中学(西场镇)
  - 私立东坡初级中学(廉州)
  - 私立太邱中学(廉州)
  - 私立高梁初级中学(北海)

<!-- end list -->

  - 私立海門中學

## 交通

经过合浦县境内的主要交通线：

  - [南北高铁](../Page/南北高铁.md "wikilink")（[南宁](../Page/南宁.md "wikilink")－[北海](../Page/北海市.md "wikilink")）。
  - [325国道](../Page/325国道.md "wikilink")（[广州](../Page/广州.md "wikilink")－[南宁](../Page/南宁.md "wikilink")）。
  - [209国道](../Page/209国道.md "wikilink")（[呼和浩特](../Page/呼和浩特.md "wikilink")－[北海](../Page/北海市.md "wikilink")）。
  - [兰海高速公路](../Page/兰海高速公路.md "wikilink")（\[<http://www.china-highway.com/bencandy.php?fid=165&id=22915>**G75**\]，[兰州](../Page/兰州.md "wikilink")－[海口](../Page/海口.md "wikilink")）。
  - [玉铁高速公路](../Page/玉铁高速公路.md "wikilink")（[玉林](../Page/玉林.md "wikilink")－[铁山港](../Page/铁山港.md "wikilink")）
  - [钦北铁路](../Page/钦北铁路.md "wikilink")（[钦州](../Page/钦州.md "wikilink")－[北海](../Page/北海市.md "wikilink")）。
  - [合湛铁路](../Page/合湛铁路.md "wikilink")（合浦－[湛江](../Page/湛江.md "wikilink")，规划中）。
  - [玉合铁路](../Page/玉合铁路.md "wikilink")（[玉林](../Page/玉林.md "wikilink")－合浦，规划中）。

## 名胜古迹

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")
      - [合浦汉墓群](../Page/合浦汉墓群.md "wikilink")
      - [大士阁](../Page/大士阁.md "wikilink")

<!-- end list -->

  - 其他重要文物古迹
      - [大浪古城遗址](../Page/大浪古城遗址.md "wikilink")
      - [文昌塔](../Page/文昌塔.md "wikilink")
      - [东坡亭](../Page/东坡亭.md "wikilink")

## 合浦珍珠

合浦自[汉代起即为重要的天然](../Page/汉代.md "wikilink")[珍珠产地](../Page/珍珠.md "wikilink")，历朝历代皆专设官职，驻合浦监督采珠[进贡](../Page/进贡.md "wikilink")。合浦东南部北部湾海域所产珍珠尺寸较大，色泽艳丽，是珍珠中的上品，称为“南珠”，合浦因此得名“南珠之乡”。

[明朝人屈大均著](../Page/明朝.md "wikilink")《广东新语》载\[7\]：

合浦海域采集到的珍珠多出自马氏珍珠贝\[8\]，这种贝多见于开敞的内海湾，在水温15―25℃间，水质清澈，水流平缓的泥砂底质上生活。

采集珍珠贝是十分危险的，[清代张英等奉敕撰的](../Page/清代.md "wikilink")《[渊鉴类函](../Page/渊鉴类函.md "wikilink")》记载\[9\]：

近代，随着[人工养殖珍珠技术的发展](../Page/人工养殖.md "wikilink")，合浦海域的天然珍珠采集业逐渐衰落。1958年，合浦营盘镇建立了中国第一个人工养殖海水珍珠基地，同年马氏珍珠贝人工插核育珠获得成功。但是随着[日本](../Page/日本.md "wikilink")、[浙江等地的人工珍珠培育技术的不断进步](../Page/浙江.md "wikilink")，合浦南珠的一家独大已成为历史。近年来珍珠的功效得到了进一步挖掘，推出了珍珠粉、珍珠贝壳粉等产品。

### 珠还合浦

[南朝宋](../Page/南朝宋.md "wikilink")[范晔著](../Page/范晔.md "wikilink")《[后汉书](../Page/后汉书.md "wikilink")·循吏传·孟尝》载\[10\]：

[孟尝合理开采珍珠的故事演化为](../Page/孟尝_\(东汉\).md "wikilink")[成语](../Page/成语.md "wikilink")“[珠还合浦](../Page/珠还合浦.md "wikilink")”，比喻失物复得或人去复还。

## 著名人物

  - [陈铭枢](../Page/陈铭枢.md "wikilink")：[国民革命军](../Page/国民革命军.md "wikilink")[十九路军总指挥](../Page/十九路军.md "wikilink")
  - [梅艳芳](../Page/梅艳芳.md "wikilink")：[香港歌手及演员](../Page/香港.md "wikilink")

## 参考资料

## 外部链接

  - [合浦县人民政府公众信息网](http://www.hepu.gov.cn/)

[合浦县](../Category/合浦县.md "wikilink") [县](../Category/北海区县.md "wikilink")

1.  合浦县人民政府2004年统计公报
    [1](http://www.hepu.gov.cn/home_webviewcontent.asp?menuid=15&itemid=159)
2.  合浦县气象局:合浦气候特征
    [2](http://www.hepu.gov.cn/home_webmenupage.asp?menuid=7)
3.  韩秋影等. 人类活动对广西合浦海草床服务功能价值的影响. 生态学杂志.2007, 26(4): 544-548
    [3](http://ledweb.scsio.ac.cn/download/lunwen2007/lunwen200775.pdf)
4.  陈成斌等. 合浦县野生稻资源现状调查及保护对策. 植物遗传资源学报 2005: 6(4)
5.
6.
7.  《广东新语·卷十五·货语》[4](http://www.guoxue123.com/biji/qing/gdxy/015.htm)
8.  廖国一. 环北部湾沿岸历代珍珠的采捞及其对海洋生态环境的影响. 广西民族研究. 2001(1)
9.  《渊鉴类函卷三十三·地部十一》
    [5](http://dcc.ndhu.edu.tw/dilcis/query/text/r-033-1.html)
10. 《后汉书·孟尝传》