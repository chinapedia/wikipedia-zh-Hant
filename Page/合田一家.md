[合田一家.png](https://zh.wikipedia.org/wiki/File:合田一家.png "fig:合田一家.png")
**合田一家**，本部設立於[山口縣](../Page/山口縣.md "wikilink")[下關市竹崎町](../Page/下關市.md "wikilink")3-13-6，[日本的](../Page/日本.md "wikilink")[指定暴力團](../Page/指定暴力團.md "wikilink")([黑社會組織](../Page/黑社會.md "wikilink"))。組織構正式成員在2018年12月時約有70人。合田一家的勢力範圍擴及三個縣。

## 簡介

1948年時，[合田幸一受到當時下關地區賭博系組織名門](../Page/合田幸一.md "wikilink")．二代目[籠寅組組長](../Page/籠寅組.md "wikilink")．[保良寅之助的幫助](../Page/保良寅之助.md "wikilink")，於[下關市創立合田組](../Page/下關市.md "wikilink")。後來在1968年時，合田幸一繼承籠寅組的勢力，並改稱為「**合田一家**」直到現在\[1\]。

合田一家積極的擴展自己的勢力版圖，特別是與山口縣的反合田系勢力的對立抗爭最為激烈。1987年5月，[山中大康繼承五代目之後](../Page/山中大康.md "wikilink")，吸收了山口縣反合田勢力的其他黑社會團體，可說是統括整個山口縣的勢力範為直到現在。

1992年7月，[山口縣公安委員會將合田一家列為](../Page/山口縣.md "wikilink")[指定暴力團](../Page/指定暴力團.md "wikilink")。

2009年9月，6代目溫井完治總長引退\[2\]。11月20日、由原若頭[末廣
誠繼承](../Page/末廣_誠.md "wikilink")7代目總長\[3\]，後見人為[山口組若頭高山清司](../Page/山口組.md "wikilink")。

## 歷代總長

  - 籠寅組初代：保良淺之助
  - 籠寅組二代目：保良寅之助
  - 初代：[合田幸一](../Page/合田幸一.md "wikilink")
  - 二代目：[濱部一郎](../Page/濱部一郎.md "wikilink")
  - 三代目：[濱崎 彰](../Page/濱崎_彰.md "wikilink")
  - 四代目：[川崎友治](../Page/川崎友治.md "wikilink")
  - 五代目：[山中大康](../Page/山中大康.md "wikilink")(三代目[小櫻組組長](../Page/小櫻組.md "wikilink"))
  - 六代目：[溫井完治](../Page/溫井完治.md "wikilink")(二代目濱部組組長)
  - 七代目：[末廣 誠](../Page/末廣_誠.md "wikilink")(本名：金教煥；四代目小櫻組組長)

## 最高幹部

  - 總長・[末廣 誠](../Page/末廣_誠.md "wikilink")(本名：金教煥；四代目小櫻組組長)
  - 若頭・新井鐘吉(五代目小櫻組組長)
  - 本部長・片山洋二朗（片山組組長)
  - 幹事長・皿田正信（二代目岡村組組長)
  - 組織委員長・橋本鋭二(橋本組組長)
  - 渉外委員長・那須三四志（三四志組組長)

### 下部組織

#### [山口縣](../Page/山口縣.md "wikilink")[下關市](../Page/下關市.md "wikilink")

  - 四代目小櫻組
  - 二代目阿部組
  - 二代目中川組
  - 吳石組
  - 下川組
  - 岡吉組
  - 田中(久)組

#### [山口縣](../Page/山口縣.md "wikilink")[宇部市](../Page/宇部市.md "wikilink")

  - 二代目一松組

#### [山口縣](../Page/山口縣.md "wikilink")[山陽小野田市](../Page/山陽小野田市.md "wikilink")

  - 五代目甲斐組

#### [山口縣](../Page/山口縣.md "wikilink")[長門市](../Page/長門市.md "wikilink")

  - 二代目田中組

#### 山口縣[岩國市](../Page/岩國市.md "wikilink")

  - 六代目上田組
  - 三代目温井組
  - 可部組

#### [山口縣](../Page/山口縣.md "wikilink")[柳井市](../Page/柳井市.md "wikilink")

  - 岡村組

#### [山口縣](../Page/山口縣.md "wikilink")[周南市](../Page/周南市.md "wikilink")

  - 椎木組
  - 濱高組
  - 高橋組

#### [山口縣](../Page/山口縣.md "wikilink")[萩市](../Page/萩市.md "wikilink")

  - 五代目竹内組

#### [山口縣](../Page/山口縣.md "wikilink")[山口市](../Page/山口市.md "wikilink")

  - 三代目松正會

#### [山口縣](../Page/山口縣.md "wikilink")[防府市](../Page/防府市.md "wikilink")

  - 二代目藏重組
  - 岡健組

\[4\]

## 資料來源

[Category:指定暴力團](../Category/指定暴力團.md "wikilink")
[Category:中國地區暴力團](../Category/中國地區暴力團.md "wikilink")
[Category:關西二十日會](../Category/關西二十日會.md "wikilink")
[Category:五社會](../Category/五社會.md "wikilink")
[Category:下關市](../Category/下關市.md "wikilink")
[Category:西日本二十日會](../Category/西日本二十日會.md "wikilink")

1.  [独立組織の現状 七代目合田一家
    通信時報](http://ch.nicovideo.jp/tsushinjihou/blomaga/ar207090)
2.
3.  [合田一家七代目繼承盃儀式
    実話時報2009/12月號](http://auctions.c.yimg.jp/img361.auctions.yahoo.co.jp/users/0/6/2/4/konata6438-img450x600-1339315508vsp9vj57553.jpg)

4.  \[<http://www.police.pref.yamaguchi.jp/0360/jyousei/bunpuzu.pdf#search='%E4%B8%83%E4%BB%A3%E7%9B%AE%E5%90%88%E7%94%B0%E4%B8%80%E5%AE%B6++%E5%B1%B1%E5%8F%A3%E7%9C%8C+%E5%BA%83%E5%B3%B6%E7%9C%8C>'
    山口縣警察 - 合田一家\]