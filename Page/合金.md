[Steel_wire_rope.png](https://zh.wikipedia.org/wiki/File:Steel_wire_rope.png "fig:Steel_wire_rope.png")是以[铁为主要组成元素的合金](../Page/铁.md "wikilink")，其中[碳的质量占](../Page/碳.md "wikilink")0.02%到2.04%。\]\]
[HK_Arena_Sunday_AsiaWorld_Expo_Table_n_Chairs_1a.jpg](https://zh.wikipedia.org/wiki/File:HK_Arena_Sunday_AsiaWorld_Expo_Table_n_Chairs_1a.jpg "fig:HK_Arena_Sunday_AsiaWorld_Expo_Table_n_Chairs_1a.jpg")[椅](../Page/椅.md "wikilink")\]\]
**合金**，就是两种或两种以上[化学物质](../Page/化学物质.md "wikilink")（至少有一组分为[金属](../Page/金属.md "wikilink")）混合而成具有金属特性的物质，一般由各组分熔合成均匀的[液体](../Page/液体.md "wikilink")，再经[冷凝而得](../Page/冷凝.md "wikilink")。

合金至少是以下三種中的一種：元素形成的單一相固態[溶液](../Page/溶液.md "wikilink")，許多金屬相形成的[混合物](../Page/混合物.md "wikilink")，金屬形成的[金屬互化物](../Page/金屬互化物.md "wikilink")。固態溶液的合金其有單一[相](../Page/相.md "wikilink")，部份為溶液的合金則是有二相或二相以上，其分佈可能是勻相，也可能不是勻相，依材料冷卻過程的溫度變化而定。金屬互化物一般會有一種合金或純金屬包在另一種純金屬內。

由於合金一些特性比純金屬元素要好，因此會用在特定的應用中。合金的例子包括[鋼](../Page/鋼.md "wikilink")、[銲料](../Page/銲料.md "wikilink")、[黃銅](../Page/黃銅.md "wikilink")、、[磷青銅及](../Page/磷青銅.md "wikilink")[汞齊等](../Page/汞齊.md "wikilink")。

合金的成份一般是以質量比例來計算。合金依其原子組成的方式，可以區分為替代合金或间质合金，又可以進一步區分為勻相（只有一相）、非勻相（不止一相）及[金屬互化物](../Page/金屬互化物.md "wikilink")（兩相之間沒有明顯的邊界）。

## 概述

合金的生成常会改变元素[单质的性质](../Page/单质.md "wikilink")，例如，钢的强度大于其主要组成元素[铁](../Page/铁.md "wikilink")。合金的[物理性质](../Page/物理性质.md "wikilink")，例如[密度](../Page/密度.md "wikilink")、反应性、[杨氏模量](../Page/杨氏模量.md "wikilink")、[导电性和](../Page/导电性.md "wikilink")[导热性可能与合金的组成元素有类似之处](../Page/导热性.md "wikilink")，但是合金的[抗拉强度](../Page/抗拉强度.md "wikilink")\[1\]和[抗剪强度却通常与组成元素的性质有很大不同](../Page/抗剪强度.md "wikilink")。这是由于合金与单质中的原子排列有很大差异。例如合金的熔点通常比组成合金的金属熔点要低是因为各种金属原子半径有所差异，较不易形成稳定的晶格。

少量的某种元素可能会对合金的性质造成很大的影响。例如，[铁磁性合金中的杂质会使合金的性质发生变化](../Page/铁磁性.md "wikilink")。\[2\]\[3\]

不同于纯净金属的是，多数合金没有固定的熔点，温度处在熔化温度范围间时，混合物为固液并存状态。因此可以说，合金的熔点比组分金属低。参见[低共熔混合物](../Page/低共熔混合物.md "wikilink")。

常见的合金中，[黄铜是铜和锌的合金](../Page/黄铜.md "wikilink")；[青铜是锡和铜的合金](../Page/青铜.md "wikilink")，常用于雕象、装饰品和教堂鐘;
[鋼則為](../Page/鋼.md "wikilink")[铁和](../Page/铁.md "wikilink")[碳的合金](../Page/碳.md "wikilink")。一些國家的[貨幣都會使用合金](../Page/貨幣.md "wikilink")（如鎳合金）。

合金是一种[溶液](../Page/溶液.md "wikilink")，固態的溶液特稱為[固溶体](../Page/固溶体.md "wikilink")
。如钢中，铁是[溶剂](../Page/溶剂.md "wikilink")，碳是[溶质](../Page/溶质.md "wikilink")。

## 参见

  - [合金列表](../Page/合金列表.md "wikilink")
  - [金属互化物](../Page/金属互化物.md "wikilink")
  - [热处理](../Page/热处理.md "wikilink")
  - [理想溶液](../Page/理想溶液.md "wikilink")
  - [高熵合金](../Page/高熵合金.md "wikilink")

## 参考资料

<div class="references-small">

<references/>

</div>

[\*](../Category/合金.md "wikilink") [A](../Category/冶金.md "wikilink")

1.  Adelbert Phillo Mills, (1922) *Materials of Construction: Their
    Manufacture and Properties*, John Wiley & sons, inc, 489 pages,
    originally published by the University of Wisconsin, Madison
2.  [C. Michael Hogan, (1969) *Density of States of an Insulating
    Ferromagnetic Alloy* Phys. Rev. 188, 870 - 874, \[Issue 2 – December
    1969](http://prola.aps.org/abstract/PR/v188/i2/p870_1)
3.  \[<http://prola.aps.org/abstract/PRA/v32/i4/p2530_1>. X. Y. Zhang
    and H. Suhl（1985）Phys. Rev. A 32, 2530 - 2533（1985）\[Issue 4 –
    October 1985\]