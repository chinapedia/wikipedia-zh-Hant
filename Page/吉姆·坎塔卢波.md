**吉姆·坎塔卢波**（Jim
Cantalupo，），[美国](../Page/美国.md "wikilink")[麦当劳公司董事长兼](../Page/麦当劳.md "wikilink")[首席执行官](../Page/首席执行官.md "wikilink")。

## 生平

坎塔卢波1974年加入麦当劳，1975年任公司副总裁，1987年主管公司海外业务，1991年担任总裁兼首席执行官。2003年，坎塔卢波主持制定了“[反肥胖战略](../Page/反肥胖战略.md "wikilink")”，提倡健康的饮食和运动。

2004年因心臟病逝世於[佛羅里達州](../Page/佛羅里達州.md "wikilink")[奧蘭多](../Page/奧蘭多.md "wikilink")，他逝世後2日，[日本麦当劳创业者](../Page/日本.md "wikilink")[藤田田亦因心臟病](../Page/藤田田.md "wikilink")（[心衰竭](../Page/心衰竭.md "wikilink")）病逝[東京](../Page/東京.md "wikilink")。

## 外部链接

  - [McDonald's CEO Cantalupo dies suddenly; Bell
    succeeds](http://money.cnn.com/2004/04/19/news/fortune500/mcdonalds_ceo/)
    - CNN/Money
  - [Cantalupo's career at
    McDonald's](https://web.archive.org/web/20040422161602/http://money.cnn.com/2004/04/19/news/fortune500/mcd_chronology.reut/)
    - Reuters
  - [Jim Cantalupo
    Biography](http://www.referenceforbusiness.com/biography/A-E/Cantalupo-Jim-1943-2004.html)

[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:意大利裔美国人](../Category/意大利裔美国人.md "wikilink")
[Category:爱尔兰裔美国人](../Category/爱尔兰裔美国人.md "wikilink")
[Category:美国会计师](../Category/美国会计师.md "wikilink")
[Category:麦当劳人物](../Category/麦当劳人物.md "wikilink")
[Category:伊利諾大學厄巴納－香檳分校校友](../Category/伊利諾大學厄巴納－香檳分校校友.md "wikilink")
[Category:伊利諾州人](../Category/伊利諾州人.md "wikilink")
[Category:西尔斯控股人物](../Category/西尔斯控股人物.md "wikilink")