<table>
<thead>
<tr class="header">
<th><p>吉扎克州<br />
Jizzax viloyati</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Jizzax_Viloyati_in_Uzbekistan.svg" title="fig:Jizzax_Viloyati_in_Uzbekistan.svg">Jizzax_Viloyati_in_Uzbekistan.svg</a></p></td>
</tr>
<tr class="even">
<td><p>基本統計資料</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/首府.md" title="wikilink">首府</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面積.md" title="wikilink">面積</a> :</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>(2005):<br />
 • <a href="../Page/人口密度.md" title="wikilink">密度</a> :</p></td>
</tr>
<tr class="even">
<td><p>主要<a href="../Page/民族.md" title="wikilink">民族</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ISO_3166-2.md" title="wikilink">行政區代碼</a>:</p></td>
</tr>
</tbody>
</table>

**吉扎克州**（[烏茲別克語](../Page/烏茲別克語.md "wikilink")：Jizzax
viloyati）是[烏茲別克十二個州份之一](../Page/烏茲別克.md "wikilink")。它涵蓋20,500平方公里，有人口910,500。吉扎克州州下轄11個縣，[首府設於](../Page/首府.md "wikilink")[吉扎克市](../Page/吉扎克.md "wikilink")。

## 地理位置

吉扎克州州位於[烏茲別克中部](../Page/烏茲別克.md "wikilink")。它與以下州份或國家相連（從北開始逆時針）：[哈薩克](../Page/哈薩克.md "wikilink")、[撒馬爾罕州](../Page/撒馬爾罕州.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")、[錫爾河州](../Page/錫爾河州.md "wikilink")。公路全長2,500公里。

## 經濟

## 參考

  - Umid World

[Category:烏茲別克行政區劃](../Category/烏茲別克行政區劃.md "wikilink")