**吉普森**（；全稱：**吉普森品牌公司**；前稱：**吉普森吉他公司**、**吉普森曼陀林吉他公司**），是[美國著名](../Page/美國.md "wikilink")[吉他製造商](../Page/吉他.md "wikilink")。

## 歷史

  - 1902年，[歐維爾·吉普森創立吉普森](../Page/歐維爾·吉普森.md "wikilink")。
  - 1944年，被[芝加哥樂器收購吉普森](../Page/芝加哥樂器.md "wikilink")。
  - 1969年，[厄瓜多爾收購吉普森](../Page/厄瓜多爾_\(公司\).md "wikilink")，該公司更名為[諾林](../Page/諾林.md "wikilink")。
  - 1986年，諾林被其現有業主收購，吉布森變成私人控股公司，並由Henry Juszkiewicz和David H.
    Berryman所有。
  - 2018年，吉普森申請了第11章破產保護，並宣布通過關閉無利可圖的消費電子部門來實現盈利能力重組。

## 產品

吉普森擁有[Epiphone](../Page/Epiphone.md "wikilink")、[Kramer](../Page/Kramer.md "wikilink")、[Maestro](../Page/Maestro.md "wikilink")、[Steinberger](../Page/Steinberger.md "wikilink")、[Tobias](../Page/Tobias.md "wikilink")、[Kalamazoo](../Page/Kalamazoo.md "wikilink")、[Dobro](../Page/Dobro.md "wikilink")、[Slingerland](../Page/Slingerland.md "wikilink")、[Valley
Arts以及](../Page/Valley_Arts.md "wikilink")[Baldwin](../Page/Baldwin.md "wikilink")（[Chickering](../Page/Chickering.md "wikilink")、[Hamilton和](../Page/Hamilton.md "wikilink")[Wurlitzer](../Page/Wurlitzer.md "wikilink")）等品牌。[吉普森創新提供](../Page/吉普森創新.md "wikilink")[飛利浦](../Page/飛利浦.md "wikilink")、[蒂雅克](../Page/蒂雅克.md "wikilink")（[Esoteric](../Page/Esoteric.md "wikilink")）、[安橋](../Page/安橋_\(公司\).md "wikilink")（[先鋒](../Page/先鋒_\(公司\).md "wikilink")）、[史雲威格和](../Page/史雲威格.md "wikilink")[Stanton的消費級電子產品](../Page/Stanton.md "wikilink")，以及[KRK
Systems的專業音響設備](../Page/KRK_Systems.md "wikilink")、[鮑德溫鋼琴的鋼琴和](../Page/鮑德溫鋼琴.md "wikilink")[Cakewalk的音樂軟件](../Page/Cakewalk.md "wikilink")。

### 工廠

所有吉普森的樂器是美國製造

| 地址                                          | 介紹                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 114 So. Burdick, Kalamazoo, MI.             | This was the "business location" of "O. H. Gibson, Manufacturer, Musical Instruments."\[1\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 104 East Main, Kalamazoo, MI                | This was Orville Gibson’s residence, and he built instruments on the 2nd floor of this location.\[2\]\[3\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 114 East Main, Kalamazoo, MI                | The "Gibson Mandolin-Guitar Manufacturing Co, Ltd." was established in 1902.\[4\] This building, said to be infested with cockroaches, was probably the former Witmer Bakery.\[5\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 114 East Exchange Place, Kalamazoo, MI      | Located quite close to the previous location, in Kalamazoo’s business district.\[6\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 521–523 East Harrison Court, Kalamazoo, MI  | Located about .5 miles from previous location. The building was next to the Michigan Central Railroad, and stood for many decades, until it came down in the late 20th century.\[7\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 225 Parsons St, Kalamazoo, MI, 49007        | Also located next to railroad tracks, this facility had major expansions in 1945, 1950, and 1960.\[8\] Various brands were produced there, including Gibson, Epiphone, (1957–1970)\[9\]\[10\] and Kalamazoo. During the depression of the 1930s, children's toys were produced there, and during WW2 it produced materials to support the war effort in addition to producing guitars.\[11\] Between 1974 and 1984 Gibson moved its manufacturing out of this facility to Tennessee. Most of this move happened in 1974, leaving only acoustic and some semi-acoustic production for this plant.\[12\] In 1985, Heritage Guitars began production, renting part of this facility.\[13\] |
| 641 Massman Drive, Nashville, TN, 37210     | This is Gibson's facility for production of their main solid body models, such as the Les Paul and the SG.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 145 Lt. George W. Lee Av, Memphis, TN 38103 | This is Gibson's facility for production of their semi-hollowbody electric guitars. This facility shares the same building as Gibson's Retail Shop and Beale Street "Showcase" location.\[14\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| 1894 Orville Way, Bozeman, MT, 59715        | This facility is dedicated to acoustic guitar production.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

## 參考文獻

## 外部連結

  - [吉普森全球](https://www.gibson.com/)

  - [吉普森中國大陸](https://web.archive.org/web/20180507221426/https://www.gibson.cn/)

[Category:美國公司](../Category/美國公司.md "wikilink")
[Category:電子樂器製造公司](../Category/電子樂器製造公司.md "wikilink")
[Category:吉他製造公司](../Category/吉他製造公司.md "wikilink")
[Category:電貝斯製造公司](../Category/電貝斯製造公司.md "wikilink")

1.

2.
3.

4.
5.

6.

7.

8.

9.

10.

11.

12.

13.

14.