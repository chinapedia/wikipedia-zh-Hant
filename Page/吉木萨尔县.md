**吉木萨尔县**（）是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[昌吉回族自治州所辖的一个](../Page/昌吉回族自治州.md "wikilink")[县](../Page/县.md "wikilink")。总面积为8149平方公里，2002年人口为13万人。

## 历史文化

吉木萨尔是[蒙古语](../Page/蒙古语.md "wikilink")，意为沙砾滩河。吉木萨尔县位于[阜康市之东](../Page/阜康市.md "wikilink")，[奇台县之西](../Page/奇台县.md "wikilink")。[清](../Page/清代.md "wikilink")[光绪二十八年](../Page/光绪.md "wikilink")（1902年）建县，称**孚远县**。1952年改名为吉木萨尔县。境内有[车师后国](../Page/车师.md "wikilink")、[別失八里古城遗址](../Page/別失八里.md "wikilink")。

唐代诗人岑参曾赋诗《白雪歌送武判官归京》中“輪臺東門送君去，去時雪滿天山路”的“輪臺”即今新疆维吾尔自治区昌吉回族自治州吉木萨尔县城北11公里处的“北庭故城遗址”，系国务院第三批公布的全国重点文物保护单位。\[1\]

## 行政区划

下辖6个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 参考文献

## 外部链接

  - [吉木萨尔县政府网站](http://www.jmser.gov.cn/)

## 参见

  - [庭州](../Page/庭州.md "wikilink")

{{-}}

[吉木萨尔县](../Category/吉木萨尔县.md "wikilink")
[县](../Category/昌吉县市.md "wikilink")
[昌吉](../Category/新疆县份.md "wikilink")

1.  <http://webapp.ts.cn/?src=http://www.ts.cn/culture/content/2013-10/22/content_8827453.htm>
    天山网《八月梨花何处开？——谈岑参诗中的“轮台”》