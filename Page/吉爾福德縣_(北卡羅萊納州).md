**吉爾福德县**（）是[美國](../Page/美國.md "wikilink")[北卡羅萊納州北部的一個縣](../Page/北卡羅萊納州.md "wikilink")。面積1,703平方公里。根据[美國2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口488,406人。縣治[格林斯伯勒](../Page/格林斯伯勒_\(北卡羅來納州\).md "wikilink")（Greensboro）和[海因波特](../Page/海波因特_\(北卡罗来纳州\).md "wikilink")（High
Point）。

成立於1770年12月5日。縣名紀念英國貴族法蘭西斯·諾斯，吉爾福德伯爵 （Francis North, 1<sup>st</sup>Earl
of
Guilford，[英國首相](../Page/英國首相.md "wikilink")[腓特烈·諾斯，諾斯勳爵之父](../Page/腓特烈·諾斯，諾斯勳爵.md "wikilink")）。\[1\]
[Map_of_Guilford_County_North_Carolina_With_Municipal_and_Township_Labels.PNG](https://zh.wikipedia.org/wiki/File:Map_of_Guilford_County_North_Carolina_With_Municipal_and_Township_Labels.PNG "fig:Map_of_Guilford_County_North_Carolina_With_Municipal_and_Township_Labels.PNG")

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/北卡羅萊納州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.