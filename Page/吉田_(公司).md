**株式會社吉田**（）是一個[日本的](../Page/日本.md "wikilink")[皮革製品商](../Page/皮革.md "wikilink")。

## 歷史

1935年4月1日，吉田鞄製作所由吉田吉蔵在[東京](../Page/東京.md "wikilink")[神田須田町創立](../Page/神田須田町.md "wikilink")。

1951年，改組為吉田株式會社。

1962年，發表品牌「[Porter](../Page/Porter.md "wikilink")」，俗稱「Porter包」的Porter系列成為「[吉田包](../Page/吉田包.md "wikilink")」的代名詞。

1983年，由吉田克幸所主導設計的「Porter Tanker」上市。

其後，與[-{zh-hans:索尼;
zh-hant:新力;}-的電腦品牌](../Page/Sony.md "wikilink")[VAIO展開合作](../Page/VAIO.md "wikilink")，陸續推出專為VAIO設計的電腦背包。

## 商標爭議

1999年，位於臺灣[臺中的尚立國際股份有限公司](../Page/台中.md "wikilink")（Porter International
Company Limited，簡稱尚立國際或Porter
International）引進其品牌PORTER；當時尚立國際自吉田株式會社取得原料及材質與設計之使用授權，並在臺灣當地自行製造，其產品之商標維持為「PORTER.TOKYO.JAPAN」，但會於內部標示註明是「MADE
IN TAIWAN」。

2000年後，[木村拓哉在](../Page/木村拓哉.md "wikilink")[戀愛世代](../Page/戀愛世代.md "wikilink")、[美麗人生等](../Page/美麗人生_\(日本電視劇\).md "wikilink")[日劇中使用](../Page/日劇.md "wikilink")「Porter
Tanker」系列的商品，帶動了Porter系列的熱賣。

2001年，尚立國際搶先將Porter系列之商標在全球各地註冊（除吉田株式會社已先行註冊之日本與香港地區），取得國際商標、設計製造及經營之權利，開始以「Porter
International」品牌行銷於各地；但是吉田株式會社不願承認「PORTER
INTERNATIONAL」之相關品牌，並開始與對方進行長期訴訟。

2007年底，與尚立國際達成和解，其社長表示：「經濟發展全球化是當今各國企業發展的根本趨勢，這是眾所皆知的事實。對於PORTER
INTERNATIONAL 及 YOSHIDA
KABAN而言，因應全球化發展的市場規律，及順應時代潮流，為此我們進行了一連串的會商，雙方秉持信賴並以共榮共存為基本精神，從此進行更密切的相互交流及互換資訊，更加提升全球競爭力。依此關係，我們宣布與尚立公司展開合作並指定PORTER
INTERNATIONAL為YOSHIDA KABAN的臺灣獨家代理商。」

## 外部链接

  - [吉田株式會社](http://www.yoshidakaban.com)
  - [尚立國際股份有限公司](http://www.ll-porter.com)

[J](../Category/日本製造公司.md "wikilink")
[Category:時尚品牌](../Category/時尚品牌.md "wikilink")
[Category:1935年日本建立](../Category/1935年日本建立.md "wikilink")