**吉田秋生**
，[日本漫畫家](../Page/日本漫畫家.md "wikilink")。[東京都](../Page/東京都.md "wikilink")[澀谷區出身](../Page/澀谷區.md "wikilink")，女性，於[武藏野美術大學畢業](../Page/武藏野美術大學.md "wikilink")。代表作有『[BANANA
FISH](../Page/Banana_Fish.md "wikilink")』、『[YASHA
-夜叉-](../Page/YASHA_-夜叉-.md "wikilink")』、『[吉祥天女](../Page/吉祥天女.md "wikilink")』、『[櫻園](../Page/櫻園.md "wikilink")』等。

## 概要

作品多為女性向，1977年以『有些不可思議的房客』出道。1983年以『河よりも長くゆるやかに』與『吉祥天女』獲第29屆[小學館漫畫賞](../Page/小學館漫畫賞.md "wikilink"),2001年又以『YASHA-夜叉-』榮獲第47屆[小學館漫畫賞](../Page/小學館漫畫賞.md "wikilink")。

## 作品

  - 有些不可思議的房客（）（1977）

  - （1977）

  - 加州物語（）（1978-1984）

  - （1983-1985）

  - [吉祥天女](../Page/吉祥天女_\(漫画\).md "wikilink")（）（1983-1984）

  - [BANANA FISH
    戰慄殺機](../Page/Banana_Fish.md "wikilink")（バナナフィッシュ）全19卷（1985-1994）

  - [櫻園](../Page/櫻園.md "wikilink")（）（1985-1986）

  - （1988-1994）

  - （）（1995-1996）

  - （）（1996-2002）

  - 沈睡的夏娃（）（2003-2005）

  - [海街日記](../Page/海街日記.md "wikilink")（）（2007-）

## 作品映像化

  - 1990年《[櫻園](../Page/櫻園.md "wikilink")》（櫻の園）被導演中原俊搬上銀幕。
  - 1991年「[世界奇妙物語](../Page/世界奇妙物語.md "wikilink")」將《ざしきわらし》搬上螢幕。（演出：永島敏行／沢田和美／栗田陽子／光石研、ほか。）
  - 2000年4月由[朝日電視台製作的](../Page/朝日電視台.md "wikilink")《》電視劇開始播出。（[伊藤英明主演](../Page/伊藤英明.md "wikilink")。）
  - 2003年《》（ラヴァーズ・キス）電影化。（及川中導演，[平山綾](../Page/平山綾.md "wikilink")、[成宮寬貴主演](../Page/成宮寬貴.md "wikilink")。）
  - 2006年4月由[朝日電視台製作的](../Page/朝日電視台.md "wikilink")《吉祥天女》電視劇開始播出。（[岩田小百合主演](../Page/岩田小百合.md "wikilink")。）
  - 2007年《吉祥天女》電影版上映。（及川中導演，[鈴木杏主演](../Page/鈴木杏.md "wikilink")。）
  - 2014年东宝宣布拍摄《[海街日記](../Page/海街日記.md "wikilink")》电影版，于2015年夏季上映。（[是枝裕和导演](../Page/是枝裕和.md "wikilink")，四女分別由[綾瀨遙](../Page/綾瀨遙.md "wikilink")、[長澤雅美](../Page/長澤雅美.md "wikilink")、[夏帆及](../Page/夏帆.md "wikilink")[廣瀨鈴出演](../Page/廣瀨鈴.md "wikilink")）
  - 2018年7月《[:Banana
    Fish](../Page/:Banana_Fish.md "wikilink")》公布動畫化，由MAPPA公司所製，為吉田秋生老師出道40週年紀念企劃。

## 參考文獻

  - 《少女魂　現在映少女完全&集》，[白泉社](../Page/白泉社.md "wikilink")，2000年12月 ISBN
    4-592-73178-6－來自少女漫畫評論家·藤本由香里的刊登專訪

## 外部連結

  - [吉田秋生
    ファンのページ](http://www.ne.jp/asahi/okumura/callenreese/yoshida/yoshidaindex.html)

  - [小學館漫畫@flowers](https://web.archive.org/web/20070404225236/http://flowers.shogakukan.co.jp/interviwe/interviwe_10.html)
    －2002年6月的刊登專訪

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:武藏野美術大學校友](../Category/武藏野美術大學校友.md "wikilink")