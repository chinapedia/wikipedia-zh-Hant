**吉野家**（）是源自[日本的跨國日式](../Page/日本.md "wikilink")[快餐](../Page/快餐.md "wikilink")[連鎖店](../Page/連鎖店.md "wikilink")，以製售[丼物為主力](../Page/丼物.md "wikilink")，分店遍及日本、[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞及](../Page/馬來西亞.md "wikilink")[美國](../Page/美國.md "wikilink")[加州等地](../Page/加州.md "wikilink")。吉野家於1899年成立，時至今天，全球分店超過1,700間。其名稱中的[日本漢字](../Page/日本漢字.md "wikilink")「吉」字，正式寫法為上「土」下「口」的「」。吉野家早年在日本成功連鎖化，當時在日本的電視廣告中強調「快速！便宜！好吃！」的牛肉飯，成功抓住以男性為主的消費者\[1\]。目前日本國內店鋪總數在同型餐廳中居於第二，僅次於[食其家](../Page/食其家.md "wikilink")。

2001年年底，由於日本出現[狂牛症個案](../Page/牛海綿狀腦病.md "wikilink")，[牛丼的銷量開始遭到打擊](../Page/牛丼.md "wikilink")。2003年，美國華盛頓發現狂牛症，而美國也是牛丼的牛肉主要來源，吉野家遂停止入口美國牛肉，同時於2004年2月11日停止在日本售賣牛丼，直至2006年12月1日始重新販售。吉野家馬上舉行為期一天的「牛肉飯復活大節」，日本全國各地分店門口出現[人龍](../Page/排隊.md "wikilink")\[2\]。

## 歷史

  - 1899年：個人商店“吉野家”於日本[東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京都\).md "wikilink")[日本橋的魚市場成立](../Page/日本橋_\(東京\).md "wikilink")。
  - 1926年：遷移到日本東京都中央區[築地](../Page/築地.md "wikilink")。
  - 1958年12月27日：為使得牛肉飯餐廳企業化，“吉野家株式會社”成立。
  - 1971年：開設首間24小時營業分店（新橋店）。
  - 1973年：公司在科羅拉多州註冊，成立吉野家（美國）公司。
  - 1975年：開設首間[美國分店](../Page/美國.md "wikilink")，北美第一家吉野家在[科羅拉多州大城](../Page/科羅拉多州.md "wikilink")[丹佛營業](../Page/丹佛.md "wikilink")。
  - 1979年：在[加州](../Page/加州.md "wikilink")[帕薩迪納開設首間](../Page/帕薩迪納.md "wikilink")[洛杉磯分店](../Page/洛杉磯.md "wikilink")，亦是現在著名的洛杉磯吉野家1號店。
  - 1980年：申請“會社更生法”（支持經營困難的公司重建的一個法律，相當於美國[破產法第](../Page/破產法.md "wikilink")11章），公司破產。
  - 1983年：在Saison Group（西武流通集團）旗下重建。
  - 1985年：洛杉磯市的分店達到20家，而在此年實現了全美的店鋪超過200家的計劃。
  - 1987年：償還了由於公司倒閉而產生的100億[日圓重建債務](../Page/日圓.md "wikilink")。
  - 1988年：與 **D\&C**
    合併成立**吉野家D\&C株式會社**。同年於[台北館前路上開設](../Page/台北市.md "wikilink")[台灣首家分店](../Page/台灣.md "wikilink")，開始進軍海外市場。
  - 1990年：於日本[JASDAQ市場上市](../Page/JASDAQ.md "wikilink")。
  - 1991年：於[香港開設首家分店](../Page/香港.md "wikilink")。
  - 1992年：於[北京開設首家](../Page/北京.md "wikilink")[中国內地分店](../Page/中国內地.md "wikilink")。
  - 1992年：於[新加坡開設首家分店](../Page/新加坡.md "wikilink")。
  - 2000年代初：吉野家在日本曾與[麥當勞等其他速食連鎖店爆發](../Page/麥當勞.md "wikilink")[減價戰](../Page/恶性竞争.md "wikilink")，當時日本所賣之牛丼，其價錢低至280[日圓](../Page/日圓.md "wikilink")。
  - 2000年：於[東京證券交易所上市](../Page/東京證券交易所.md "wikilink")。
  - 2001年：於[菲律賓開設首家分店](../Page/菲律賓.md "wikilink")。
  - 2002年：於[紐約](../Page/紐約.md "wikilink")、[上海開設首家分店](../Page/上海.md "wikilink")。
  - 2004年：於[馬來西亞](../Page/馬來西亞.md "wikilink")、[深圳與](../Page/深圳.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")[悉尼開設首家分店](../Page/悉尼.md "wikilink")。
  - 2008年：於[香港](../Page/香港.md "wikilink")[西環](../Page/西環.md "wikilink")、[荃灣](../Page/荃灣.md "wikilink")、[慈雲山](../Page/慈雲山.md "wikilink")、[大角咀](../Page/大角咀.md "wikilink")、[油塘開設至第](../Page/油塘.md "wikilink")39分店。
  - 2009年：[香港吉野家推行外賣速遞服務](../Page/香港.md "wikilink")，在[西環分店率先推行](../Page/西環.md "wikilink")。
  - 2010年：[香港吉野家正式接受](../Page/香港.md "wikilink")[八達通付款](../Page/八達通.md "wikilink")。
  - 2010年：於[廣州開設首家分店](../Page/廣州.md "wikilink")。
  - 2019年：日本吉野家15億日元赤字，評論指牛肉飯360円薄利多銷模式已達極限。\[3\]

<File:The> head store of YOSHINOYA,
TSUKIJI.JPG|位於[東京](../Page/東京.md "wikilink")[築地市場內的吉野家](../Page/築地市場.md "wikilink")1號店
<File:Yoshinoya> in Sapporo APIA
2014.jpg|[札幌APIA地下街的吉野家分店](../Page/札幌.md "wikilink")
<File:YoshinoyaBeijingRestaurant.JPG>|[北京市分店](../Page/北京市.md "wikilink")
<File:HK> Central Connaught Road night Yoshinoya restaurant light boxes
Travel
Expert.JPG|thumb|[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[干諾道中分店](../Page/干諾道中.md "wikilink")
<File:Yoshinoya> Guanqian Restaurant
20160505.jpg|台北市[館前店](../Page/館前路.md "wikilink")
<File:Yoshinoya> in Pasar Festival Kuningan
Jakarta.JPG|印尼[雅加達店出售的牛肉飯](../Page/雅加達.md "wikilink")
<File:Buta-don> 001.jpg|吉野家的牛肉饭
[File:咖喱牛肉饭.jpg|香港吉野家的咖喱野菜牛肉饭](File:咖喱牛肉饭.jpg%7C香港吉野家的咖喱野菜牛肉饭)
<File:HKU> Yoshinoya Outdoor Delivery Motorbike 1.JPG|香港吉野家的外賣電單車

## 資料來源

## 外部連結

  - [吉野家（日本）](http://www.yoshinoya.com/)

  - [吉野家（美國）](https://www.yoshinoyaamerica.com/)

  - [吉野家（北京）](http://www.4008-197-197.com/)

  - [吉野家东北（辽宁、内蒙）](http://www.hophingfood.com.cn/)

  - [吉野家（上海）](http://www.yoshinoyashanghai.com/)

  - [吉野家（香港）](http://www.yoshinoya-hk.com/)

  - [吉野家（台灣）](https://web.archive.org/web/20120314195627/http://yoshinoya.com.tw/)

  -
  -
  - [台灣公司資料](http://company.g0v.ronny.tw/id/22662664)

[Category:1899年日本建立](../Category/1899年日本建立.md "wikilink")
[Category:1899年成立的公司](../Category/1899年成立的公司.md "wikilink")
[Category:1988年成立的公司](../Category/1988年成立的公司.md "wikilink")
[Category:1988年台灣建立](../Category/1988年台灣建立.md "wikilink") [Category:北區
(東京都)公司](../Category/北區_\(東京都\)公司.md "wikilink")
[Category:再次上市公司企業](../Category/再次上市公司企業.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:日本連鎖速食店](../Category/日本連鎖速食店.md "wikilink")
[Category:日本餐飲公司](../Category/日本餐飲公司.md "wikilink")
[Category:牛肉料理](../Category/牛肉料理.md "wikilink")

1.
2.

3.  \[<https://headlines.yahoo.co.jp/article?a=20190116-00555157-shincho-bus_all&p=1>"「吉野家」15億円の赤字は“牛丼並盛360円”ビジネスの限界「すき家」も「松屋」も逆風"\]