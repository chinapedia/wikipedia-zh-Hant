**吉隆坡中央車站**\[1\]（），简称**KL
Sentral**，是[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡的重要](../Page/吉隆坡.md "wikilink")[公共交通樞紐](../Page/公共交通.md "wikilink")、未來[马新高速铁路的起始站以及](../Page/马新高速铁路.md "wikilink")[泛亞鐵路的主要站點之一](../Page/泛亞鐵路.md "wikilink")，於2001年投入運營，以取代原有的[吉隆坡火车站](../Page/吉隆坡火车站.md "wikilink")，而後者則改向[马来西亚铁道局的](../Page/马来西亚铁道局.md "wikilink")[城际铁路提供服務](../Page/城际铁路.md "wikilink")。值得注意的是，KL
Sentral在中文分成两种，即综合车站和商业中心地带，其中各条线路的综合车站处被称为“吉隆坡中央车站”，而铁路车站和周边的酒店、商场、办公楼等聚集的商业中心地带则为“吉隆坡中环”\[2\]。

## 中央车站

中央車站是大多数吉隆坡的铁路枢纽，以便能让乘客转搭另一条火车线。以下是吉隆坡的铁路系统把中央車站成为他们其中一个停留站。中央車站有各种设施如餐厅，便利店等。目前，中央車站一年有能力服务5千万人次，而到2020年，中央車站能够容纳1亿人次。

### 线路

  - [机场快铁程搭机场快铁到](../Page/吉隆坡機場快鐵.md "wikilink")[吉隆坡国际机场只需](../Page/吉隆坡国际机场.md "wikilink")28分钟。
  - [机场支线程搭机场地铁到](../Page/吉隆坡机场支线.md "wikilink")[吉隆坡国际机场只需](../Page/吉隆坡国际机场.md "wikilink")35分钟，路经3个站\[3\]。
  - [KTM通勤铁路巴生谷一带通勤火车服务](../Page/KTM通勤铁路.md "wikilink")
      - [芙蓉线](../Page/芙蓉线.md "wikilink")
      - [巴生港线](../Page/巴生港线.md "wikilink")
  - [格拉那再也綫吉隆坡轻快铁服务](../Page/格拉那再也綫.md "wikilink")
  - [吉隆坡单轨火车离中央車站大概](../Page/吉隆坡单轨火车.md "wikilink")140米，但需要跃过一条马路。2014年两者之间的新购物广场（Nu
    Sentral）营业后，乘客可从广场之间不必日晒雨淋快速通过换乘。

### 结构

中央車站分为三层楼。

  - **第一层**：吉隆坡城市综合铁路系统的乘客使用
  - **第二层**：马来西亚铁道局国内与国外铁路航班
  - **第三层**：**吉隆坡市内空港（KL
    CAT）**[机场快铁与](../Page/吉隆坡機場快鐵.md "wikilink")[机场支线乘客使用](../Page/吉隆坡机场支线.md "wikilink")

## 吉隆坡市内空港

吉隆坡市内空港是[吉隆坡国际机场其中一部分](../Page/吉隆坡国际机场.md "wikilink")。此空港已被[国际航空运输协会承认](../Page/国际航空运输协会.md "wikilink")，并携带XKL代码。吉隆坡市内空港目前只为6家航空公司提供行李Check
In
服务，即[马来西亚航空](../Page/马来西亚航空.md "wikilink")、[国泰航空](../Page/国泰航空.md "wikilink")、[国泰港龍航空](../Page/国泰港龍航空.md "wikilink")、[文莱皇家航空](../Page/文莱皇家航空.md "wikilink")、[阿联酋航空以及](../Page/阿联酋航空.md "wikilink")[阿提哈德航空](../Page/阿提哈德航空.md "wikilink")。管理机构也有意推出行李Check
Out 服务。乘客须在航班起飞2小时前Check In 行李。吉隆坡市内空港有各种各样的设施，如亚航售票处、便利店、咖啡座等。

## 中央車站巴士枢纽

在吉隆坡快捷巴士将吉隆坡改革后，中央車站就成为了吉隆坡快捷巴士，市内线的其中一个主要枢纽。除此之外，它也是吉隆坡快捷巴士第五区、第六区的巴士终站。巴士来往[吉隆坡市中心与](../Page/吉隆坡.md "wikilink")[吉隆坡国际机场也把它成为它们的枢纽与终点站](../Page/吉隆坡国际机场.md "wikilink")。

## 注释

<references/>

## 参见

  - [吉隆坡火车总站](../Page/吉隆坡火车总站.md "wikilink")
  - [巴生谷車站列表](../Page/巴生谷車站列表.md "wikilink")
  - [吉隆坡快捷通](../Page/吉隆坡快捷通.md "wikilink")

## 外部链接

  - [吉隆坡中央车站](http://www.klsentral.com.my/)
  - [KL
    CAT](http://www.lcct.com.my/others/places/kuala-lumpur-city-air-terminal)
  - [快捷通巴士最新路线](https://www.myrapid.com.my/traveling-with-us/how-to-travel-with-us/rapid-kl/bus)
  - [MRT网上的车站资料](http://mrt.com.my/komuter/KL_Sentral.htm)

[Category:吉隆坡交通](../Category/吉隆坡交通.md "wikilink")
[Category:芙蓉线车站](../Category/芙蓉线车站.md "wikilink")
[Category:巴生港线车站](../Category/巴生港线车站.md "wikilink")
[Category:格拉那再也线车站](../Category/格拉那再也线车站.md "wikilink")
[Category:吉隆坡机场快铁车站](../Category/吉隆坡机场快铁车站.md "wikilink")
[Category:吉隆坡机场支线车站](../Category/吉隆坡机场支线车站.md "wikilink")
[Category:吉隆坡单轨列车车站](../Category/吉隆坡单轨列车车站.md "wikilink")
[Category:天空花园线车站](../Category/天空花园线车站.md "wikilink")
[Category:西海岸线车站](../Category/西海岸线车站.md "wikilink")
[Category:2001年啟用的鐵路車站](../Category/2001年啟用的鐵路車站.md "wikilink")
[Category:吉隆坡聯邦直轄區鐵路車站](../Category/吉隆坡聯邦直轄區鐵路車站.md "wikilink")
[Category:多媒体超级走廊](../Category/多媒体超级走廊.md "wikilink")

1.
2.
3.  机场快铁和机场地铁共用轨道