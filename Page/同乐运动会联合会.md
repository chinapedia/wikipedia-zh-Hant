**同乐运动会联合会**（又译为**同志运动会联合会**，，缩写为FGG），是主办[同乐运动会](../Page/同乐运动会.md "wikilink")（）的官方机构。同乐运动会是由在1982年创办的\[1\]。他之前曾亲历过同性恋运动员所遭受的偏见以及他们不能够公开身份去参加各种体育活动。同乐运动会联合会每四年举办世界上最大的[LGBT文化和体育活动](../Page/LGBT.md "wikilink")。第十届同乐运动会在2018年8月4日法国巴黎开幕。

## 参见

  - [同乐运动会](../Page/同乐运动会.md "wikilink")（）

  - / [世界同志运动会](../Page/世界同志运动会.md "wikilink")（）

  - [欧洲同性恋体育联合会](../Page/欧洲同性恋体育联合会.md "wikilink") / （）

## 参考文献

## 外部链接

  - [同乐运动会联合会官方网站](https://gaygames.org/)

[Category:国际体育组织](../Category/国际体育组织.md "wikilink")
[Category:LGBT組織](../Category/LGBT組織.md "wikilink")

1.