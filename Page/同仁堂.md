**同仁堂**（原名**同仁堂藥室**、**同仁堂藥鋪**）是[乐显扬在](../Page/乐显扬.md "wikilink")[中國](../Page/中國.md "wikilink")[北京發迹](../Page/北京.md "wikilink")，於1669年（[清](../Page/清朝.md "wikilink")[康熙八年](../Page/康熙.md "wikilink")）創建的一家[藥店](../Page/藥店.md "wikilink")，被授予[中国国家级非物质文化遗产](../Page/中国国家级非物质文化遗产.md "wikilink")**同仁堂中医药文化**」號。該藥店現時已經[企業化為](../Page/私有化.md "wikilink")**北京同仁堂（集團）有限責任公司**，旗下擁有北京同仁堂股份有限公司（）、[北京同仁堂科技发展股份有限公司](../Page/北京同仁堂科技发展股份有限公司.md "wikilink")（）及北京同仁堂國藥有限公司（，负责境外业务）三家[上市公司](../Page/上市公司.md "wikilink")。

## 歷史

[Tongrentang_herbal_store.JPG](https://zh.wikipedia.org/wiki/File:Tongrentang_herbal_store.JPG "fig:Tongrentang_herbal_store.JPG")同仁堂总店的招牌，为著名[书法家](../Page/书法家.md "wikilink")[启功所題](../Page/启功.md "wikilink")。\]\]
[樂氏祖籍](../Page/樂姓.md "wikilink")[浙江](../Page/浙江.md "wikilink")[宁波](../Page/宁波.md "wikilink")，[明朝](../Page/明朝.md "wikilink")[永乐年间迁至](../Page/永乐.md "wikilink")[北京](../Page/北京.md "wikilink")，最初以搖串鈴走街串巷行醫、賣小藥維持生活。其后世[乐显扬当了](../Page/乐显扬.md "wikilink")[太医院](../Page/太医院.md "wikilink")[吏目](../Page/吏目.md "wikilink")，康熙八年（1669年）创办「同仁堂藥室」，以「製藥一絲不苟，賣藥貨真價實」為宗旨，藥方來自民間驗方、宮廷秘方。其子[乐凤鸣接续祖业](../Page/乐凤鸣.md "wikilink")，1702年迁舖至[前门](../Page/前门.md "wikilink")[大栅栏路南](../Page/大栅栏.md "wikilink")，总结前人制药经验，完成《[乐氏世代祖传丸散膏丹下料配方](../Page/乐氏世代祖传丸散膏丹下料配方.md "wikilink")》一书，明确提出了「遵肘后，辨地产，炮制虽繁必不敢省人工，品味虽贵必不敢减物力」的训条，树立「修合无人见，存心有天知」的意识。

1723年，[清朝](../Page/清朝.md "wikilink")[雍正帝钦定同仁堂供奉清宫](../Page/雍正帝.md "wikilink")[御药房](../Page/御药房.md "wikilink")。其后同仁堂独办官药188年，历经八代[皇帝](../Page/皇帝.md "wikilink")。自製名藥有[安宮牛黃丸](../Page/安宮牛黃丸.md "wikilink")、[牛黃清心丸](../Page/牛黃清心丸.md "wikilink")、[烏雞白鳳丸等](../Page/烏雞白鳳丸.md "wikilink")。

1907年，同仁堂樂氏第十二代子孙[樂达聪在](../Page/樂达聪.md "wikilink")[濟南](../Page/濟南.md "wikilink")[魏家莊創建](../Page/魏家莊.md "wikilink")[宏濟堂](../Page/宏濟堂.md "wikilink")，與[北京](../Page/北京.md "wikilink")「同仁堂」、[杭州](../Page/杭州.md "wikilink")「[胡慶餘堂](../Page/胡慶餘堂.md "wikilink")」並譽為[中國](../Page/中國.md "wikilink")「三大名藥店」。

1949年后，乐氏二房第十二代子孙乐崇辉携祖传秘方去台，于1953年在台北与部分北平同仁堂员工开设[台湾同仁堂](../Page/台湾同仁堂.md "wikilink")。\[1\]

1954年，同仁堂实行[公私合营](../Page/公私合营.md "wikilink")，[总经理为第十三代子孙](../Page/总经理.md "wikilink")[乐松生](../Page/乐松生.md "wikilink")。

1955年，[乐松生受到](../Page/乐松生.md "wikilink")[毛泽东和](../Page/毛泽东.md "wikilink")[周恩来的接见](../Page/周恩来.md "wikilink")。

1957年，同仁堂成立“同仁堂中药提炼厂”。

[文化大革命期間](../Page/文化大革命.md "wikilink")，乐松生遭红卫兵迫害打死，同仁堂中藥提煉廠亦因受[紅衛兵思想而影響經營並停業一段時間](../Page/紅衛兵.md "wikilink")，直至1979年獲[國務院批准重開](../Page/國務院.md "wikilink")。

1991年，同仁堂晋升为[国家一级企业](../Page/国家一级企业.md "wikilink")。

1998年，[中國全国总工会授予同仁堂](../Page/中國全国总工会.md "wikilink")“全国五一奖状”。

1997年，同仁堂在[上海](../Page/上海.md "wikilink")[證券交易所上市](../Page/上海證券交易所.md "wikilink")。

1999年，同仁堂成立“同仁堂发展委员会”。

2000年10月，同仁堂分拆[同仁堂科技在](../Page/同仁堂科技.md "wikilink")[香港創業板上市](../Page/香港創業板.md "wikilink")。

2011年，台灣[上櫃公司](../Page/上櫃.md "wikilink")[眾星國際代銷同仁堂中药酒](../Page/眾星國際.md "wikilink")。

2016年1月26日，同仁堂党委书记、董事长[梅群表示在](../Page/梅群.md "wikilink")[十三五期间](../Page/十三五.md "wikilink")，同仁堂所有制药工厂将迁往[河北和](../Page/河北.md "wikilink")[安徽](../Page/安徽.md "wikilink")[亳州](../Page/亳州.md "wikilink")，而北京只保留同仁堂总部和少数项目\[2\]。

2018年12月，北京同仁堂旗下蜂業公司[代工廠商](../Page/代工.md "wikilink")[盐城金蜂食品科技有限公司被曝回收过期蜂蜜进原料库](../Page/盐城金蜂食品科技有限公司.md "wikilink")\[3\]\[4\]。同仁堂股价也受到波及而下跌\[5\]。后江苏省盐城市滨海县市场监管局和北京市大兴区食药局分别对同仁堂罚款14,088,266.1元和111,740.88元。公司[管理階層](../Page/管理階層.md "wikilink")，包含董事、總經理及副總經理皆遭到免職處分\[6\]\[7\]。2019年2月，[国家市场监督管理总局宣布撤销北京同仁堂的](../Page/国家市场监督管理总局.md "wikilink")[中国质量奖称号](../Page/中国质量奖.md "wikilink")，并收回证书和奖杯\[8\]。

## 境外分店

（参考资料：\[9\]）

  - ：香港岛13家（4家隶属于同仁堂國藥）、九龙23家（5家隶属于同仁堂國藥）、新界23家（7家隶属于同仁堂國藥）

  - ：4家（[新馬路](../Page/新馬路.md "wikilink")、[紅街市](../Page/紅街市.md "wikilink")、[大三巴](../Page/大三巴.md "wikilink")、[百利寶](../Page/澳门.md "wikilink")）

  - ：[曼谷分店](../Page/曼谷.md "wikilink")

  - ：3家（[吉隆坡](../Page/吉隆坡.md "wikilink")、[槟城](../Page/槟城.md "wikilink")、[八打靈](../Page/马来西亚.md "wikilink")）

  - ：[雅加达分店](../Page/雅加达.md "wikilink")

  - ：5家（[牛車水橋南路](../Page/牛車水.md "wikilink")、亞歷山大醫院、[心理衛生學院](../Page/心理衛生學院.md "wikilink")、克羅士街上段、仁慈社區醫院）

  - ：[金边分店](../Page/金边.md "wikilink")

  - ：汶萊分店

  - ：[迪拜分店](../Page/迪拜.md "wikilink")

  - ：1家（內湖明水路，由总公司持有）

  - ：[馬尼拉分店](../Page/馬尼拉.md "wikilink")（由总公司持有）

  - ：2家（[伦敦](../Page/伦敦.md "wikilink")、[利茲](../Page/利茲.md "wikilink")，其中伦敦分店由总公司持有）

  - ：波蘭分店

  - ：荷蘭分店

  - ：6家（[悉尼](../Page/悉尼.md "wikilink")、[布里斯本](../Page/布里斯本.md "wikilink")、[卡市](../Page/雪梨市.md "wikilink")、[車士活](../Page/車士活.md "wikilink")、[墨爾本](../Page/墨爾本.md "wikilink")、[博士山](../Page/博士山.md "wikilink")）

  - ：5家（[列治文](../Page/列治文.md "wikilink")、[溫哥華甘比](../Page/溫哥華.md "wikilink")、溫哥華格兰维尔、[多倫多](../Page/多倫多.md "wikilink")、[密西沙加](../Page/密西沙加.md "wikilink")）

  - ：7家([奥克兰](../Page/奥克兰.md "wikilink")、[基督城](../Page/基督城.md "wikilink")，由总公司持有）

## 相关影视作品

  - 《[同仁堂传说](../Page/同仁堂传说.md "wikilink")》 ：大陆六集电视连续剧
  - 《[大清藥王](../Page/大清藥王.md "wikilink")》：大陆电视连续剧，剧中角色「樂阔海」原型为同仁堂第八代传人[樂兴](../Page/樂兴.md "wikilink")，而角色「樂宏达」原型则是同仁堂第十代传人、中兴同仁堂家业的[樂平泉](../Page/樂平泉.md "wikilink")。
  - 《[大宅门](../Page/大宅门.md "wikilink")》：剧中主角“[白景琦](../Page/白景琦.md "wikilink")”原型为[樂达聪](../Page/樂达聪.md "wikilink")，而该剧[导演](../Page/导演.md "wikilink")[郭宝昌就是樂达聪的](../Page/郭宝昌.md "wikilink")[养子](../Page/养子.md "wikilink")。
  - 《[风雨同仁堂](../Page/风雨同仁堂.md "wikilink")》：大陆八集[京剧连续剧](../Page/京剧.md "wikilink")，以[八国联军侵华为背景](../Page/八国联军侵华.md "wikilink")。剧中主角“乐徐氏”原型即为[樂平泉的继配夫人](../Page/樂平泉.md "wikilink")[许叶芬](../Page/许叶芬.md "wikilink")。
  - 《[戊子风雪同仁堂](../Page/戊子风雪同仁堂.md "wikilink")》：大陆三十集电视连续剧，以1949年前后为故事背景。

## 照片集

<File:Beijing> Qianmen
Tongrentang.JPG|北京[前门大街同仁堂](../Page/前门大街.md "wikilink")
<File:Tongrentang> Wangfujing.JPG|北京[东华门同仁堂](../Page/东华门.md "wikilink")
<File:HK> Central QRC 33 Melbourne Plaza Tong Ren
Tang.JPG|[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[皇后大道中分店](../Page/皇后大道中.md "wikilink")
<File:Tong> Ren Tang
Toronto.JPG|同仁堂[多伦多STEELES](../Page/多伦多.md "wikilink")
AVE 分店 <File:Gilman> Street Tram Stop with Beijing Tong Ren Tang
AD.jpg|[香港電車車站的特色廣告](../Page/香港電車.md "wikilink")

## 参见

  - [同仁堂乐氏世系图](../Page/同仁堂乐氏世系图.md "wikilink")

## 参考文献

## 外部链接

  - [北京同仁堂](http://www.tongrentang.com)
  - [北京同仁堂香港藥業管理有限公司](http://tongrentang.com.hk)
  - [北京同仁堂國藥有限公司](http://www.tongrentangcm.com)
  - [同仁堂崇文門藥店](http://www.trt-shop.com)
  - [泉昌有限公司　同仁堂香港總代理](http://www.chuanchiong.com.hk)
  - [同仁堂：三百年沉浮成名店](https://web.archive.org/web/20050302072403/http://fzwb.ynet.com/article.jsp?oid=4677099)
  - [新西兰奥克兰同仁堂](http://www.kannz.com/tong-ren-tang-auckland)
  - [五子衍宗丸 Wu Zi Yan Zong
    Wan](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00131)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [十全大補丸 Shi Quan Da Bu
    Wan](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00108)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [杞菊地黃丸 Di Huang
    Wan](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00112)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [板藍根顆粒 Ban Lan Gen Ke
    Li](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00124)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [祛班增白面膜 Qu Ban Zeng
    Bai](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00105)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [金匱腎氣丸 Jin Gui Shen Qi
    Wan](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00107)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [銀翹解毒片 Yin Qiao Jie Du
    Pian](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00128)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

{{-}}

[Category:总部位于北京市的中华人民共和国国有企业](../Category/总部位于北京市的中华人民共和国国有企业.md "wikilink")
[同仁堂](../Category/同仁堂.md "wikilink")

1.
2.
3.
4.  [同仁堂被曝回收过期蜂蜜
    涉事工厂是当地重大项目](https://news.sina.com.cn/s/2018-12-16/doc-ihmutuec9770542.shtml)
5.  [同仁堂股价下跌
    因其被曝大量回收过期蜂蜜](http://365jia.cn/news/2018-12-17/E84A0AADDD30C7DA.html)
6.
7.  [同仁堂“蜂蜜事件”处理结果公布：罚款1408万余元、问责多名高管](http://www.xinhuanet.com/politics/2019-02/12/c_1124106061.htm)
8.
9.  [门市网络](http://www.tongrentangcm.com/c/content/content_c.asp?content_id=68)