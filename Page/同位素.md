[Discovery_of_neon_isotopes.JPG](https://zh.wikipedia.org/wiki/File:Discovery_of_neon_isotopes.JPG "fig:Discovery_of_neon_isotopes.JPG")同位素：[氖](../Page/氖.md "wikilink")-20（neon-20）和[氖](../Page/氖.md "wikilink")-22（neon-22）。\]\]

**同位素**（）是某種特定化學[元素之下的不同種類](../Page/元素.md "wikilink")，同一種元素下的所有同位素都具有相同，[質子數目相同](../Page/質子.md "wikilink")，但[中子數目卻不同](../Page/中子.md "wikilink")。這些同位素在[化學元素週期表中佔有同一個位置](../Page/化學元素週期表.md "wikilink")，因此得名。

例如[氫元素中](../Page/氫.md "wikilink")[氘和](../Page/氘.md "wikilink")[氚](../Page/氚.md "wikilink")，它們[原子核中都有](../Page/原子核.md "wikilink")1個質子，但是它們的原子核中分別有0個中子、1個中子及2個中子，所以它們互為同位素。

## 歷史

[弗雷德里克·索迪藉由](../Page/弗雷德里克·索迪.md "wikilink")[衰變鏈分析](../Page/衰變鏈.md "wikilink")，在1912年證實同位素存在\[1\]<ref>Others
had also suggested the possibility of isotopes; e.g.,

  - Strömholm, Daniel and Svedberg, Theodor (1909) "Untersuchungen über
    die Chemie der radioactiven Grundstoffe II."（Investigations into the
    chemistry of the radioactive elements, part 2）, *Zeitschrift für
    anorganischen Chemie*, **63**: 197–206; see especially page 206.
  - Alexander Thomas Cameron, *Radiochemistry*（London, England: J. M.
    Dent & Sons, 1910）, p. 141.（Cameron also anticipated the
    displacement law.）</ref>。

[约瑟夫·汤姆孙在](../Page/约瑟夫·汤姆孙.md "wikilink")1913年首次發現穩定元素同位素的證據\[2\]\[3\]。[弗朗西斯·阿斯顿通过实验](../Page/弗朗西斯·阿斯顿.md "wikilink")，证明了[氖的两种](../Page/氖.md "wikilink")**同位素**的存在。

## 用途

可用於部分身體結構的檢驗，及檢測歷史物品的歷史年分。

## 參考文獻

## 参看

  - [同位素列表](../Page/同位素列表.md "wikilink")
  - [元素](../Page/元素.md "wikilink")
  - [核素](../Page/核素.md "wikilink")
  - [核素列表](../Page/核素列表.md "wikilink")
  - [化學元素](../Page/化學元素.md "wikilink")
  - [同中子素](../Page/同中子素.md "wikilink")
  - [同量素](../Page/同量素.md "wikilink")
  - [放射性碳定年法](../Page/放射性碳定年法.md "wikilink")（碳十四定年法）

[Category:复合粒子](../Category/复合粒子.md "wikilink")
[Category:同位素](../Category/同位素.md "wikilink")

1.  Choppin, G.; Liljenzin, J. O. and Rydberg, J.　（1995）*Radiochemistry
    and Nuclear Chemistry*（2nd ed.）Butterworth-Heinemann, pp. 3–5
2.
3.