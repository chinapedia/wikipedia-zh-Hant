《****》（**同心**）是[中国](../Page/中国.md "wikilink")[歌手](../Page/歌手.md "wikilink")[阿兰·达瓦卓玛以](../Page/阿兰·达瓦卓玛.md "wikilink")**alan**之名在[日本的第二张](../Page/日本.md "wikilink")[单曲](../Page/单曲.md "wikilink")，歌曲的主旨是：节约[地球的](../Page/地球.md "wikilink")[水资源](../Page/水资源.md "wikilink")。该单曲后来还被翻唱为[中文版本](../Page/中文.md "wikilink")《一个》收录于alan在[中国发行的首张](../Page/中国.md "wikilink")[EP](../Page/EP.md "wikilink")《[心·战～RED
CLIFF～](../Page/心·战～RED_CLIFF～.md "wikilink")》里。

C/W曲《》后来被重新翻唱为[中文版本](../Page/中文.md "wikilink")《迷失的祝福》收录于alan在[中国发行的首张](../Page/中国.md "wikilink")[EP](../Page/EP.md "wikilink")《心·战～RED
CLIFF～》里。

## 收录内容

### CD

1.  （同心）

      - 作词：[吉元由美](../Page/吉元由美.md "wikilink")　作曲：[菊池一仁](../Page/菊池一仁.md "wikilink")　编曲：[中野雄太](../Page/中野雄太.md "wikilink")

2.    - 作词：[夏川サファリ](../Page/夏川サファリ.md "wikilink")　作曲：菊池一仁　编曲：[ats-](../Page/ats-.md "wikilink")

3.    - 作词：[御徒町凧](../Page/御徒町凧.md "wikilink")　作曲：菊池一仁　编曲：[京田誠一](../Page/京田誠一.md "wikilink")

4.  sign

      - 作曲：菊池一仁　编曲：菊池一仁、[tasuku](../Page/tasuku.md "wikilink")

5.  (Instrumental)

6.  (Instrumental)

7.  (Instrumental)

### DVD

1.  (Music video)

2.  (Image clip)

## 脚注

<references/>

[Category:Alan的歌曲](../Category/Alan的歌曲.md "wikilink")
[Category:2008年单曲](../Category/2008年单曲.md "wikilink")