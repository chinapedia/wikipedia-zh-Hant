**同源异形基因**（）是生物體中一類專門調控生物形體的[基因](../Page/基因.md "wikilink")，一旦這些基因發生[突變](../Page/突變.md "wikilink")，就會使身體的一部分變形。其作用機制，主要是調控其他有關於[細胞分裂](../Page/細胞分裂.md "wikilink")、[紡錘體方向](../Page/紡錘體.md "wikilink")，以及硬毛、[附肢等部位](../Page/附肢.md "wikilink")[發育的基因](../Page/發育.md "wikilink")。Hox基因屬於[同源框家族的其中一員](../Page/同源框.md "wikilink")，在大多數Hox基因中，會含有一段約180個[核苷酸的同源異型盒](../Page/核苷酸.md "wikilink")，可以轉錄出含有約60個[氨基酸序列](../Page/氨基酸.md "wikilink")，稱為同源异形域。

Hox基因的特色之一，是其排列順序与其作用順序、作用位置相关，例如位在較靠近3'端（DNA的其中一端）的基因，作用的位置較靠近頭部。而且[動物界中的成員](../Page/動物界.md "wikilink")，皆擁有類似的Hox基因排列方式、產物與作用方式。Hox基因對於動物型態的影響，能使[演化生物學研究得到關於型態轉變的線索](../Page/演化生物學.md "wikilink")。

## 調控型態的方式

[IC_Gomphidae_wing.jpg](https://zh.wikipedia.org/wiki/File:IC_Gomphidae_wing.jpg "fig:IC_Gomphidae_wing.jpg")[Inachis_io_beentree_brok_2005.jpg](https://zh.wikipedia.org/wiki/File:Inachis_io_beentree_brok_2005.jpg "fig:Inachis_io_beentree_brok_2005.jpg")

各種生物的Hox基因之表現方式類似，靠近3'端的基因會表現於動物的身體前部，靠近5'端則表現於身體的後部。除了這種規律之外，Hox基因又會因為一些原因，而在不同狀態下有不同的表現。

當Hox基因下游調控的基因改變時，會使個體產生變異，例如果蠅的平衡桿（halteres）與[蝴蝶後翅的差別](../Page/蝴蝶.md "wikilink")，就是源於兩者的Ubx基因受到的不同調控而產生。在演化的過程中，[蜻蜓的Ubx下游基因調控階層](../Page/蜻蜓.md "wikilink")（regulatory
hierachy）與原始祖先較為相似，而[鱗翅目](../Page/鱗翅目.md "wikilink")（如蝴蝶）與[雙翅目](../Page/雙翅目.md "wikilink")（如果蠅），則改變了一些對下游基因的調控方式。蜻蜓、蝴蝶與果蠅的調控階層比較如下表，紅色字表示受到活化的基因，灰色字表示此類昆蟲並不活化此基因：

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>昆蟲種類</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>上游Hox基因</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>調控階層</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p>蜻蜓（蜻蛉目）</p></td>
<td><div style="text-align: center;">
<p><em>Ubx</em></p>
</div></td>
<td><p><span style="color: gray;">a1 → a2 → a3</span><br />
<span style="color: gray;">b1 → b2 → b3</span><br />
<span style="color: red;">c1 → c2 → c3</span><br />
<span style="color: gray;">d1 → d2 → d3</span></p></td>
</tr>
<tr class="odd">
<td><p>蝴蝶（鱗翅目）</p></td>
<td><div style="text-align: center;">
<p><em>Ubx</em></p>
</div></td>
<td><p><span style="color: gray;">a1 → a2</span><span style="color: red;"> → a3</span><br />
<span style="color: gray;">b1</span><span style="color: red;"> → b2 → b3</span><br />
<span style="color: gray;">c1 → c2</span><span style="color: red;"> → c3</span><br />
<span style="color: red;">e1 → e2 → e3</span></p></td>
</tr>
<tr class="even">
<td><p>果蠅（雙翅目）</p></td>
<td><div style="text-align: center;">
<p><em>Ubx</em></p>
</div></td>
<td><p><span style="color: red;">a1 → a2 → a3</span><br />
<span style="color: red;">b1 → b2 → b3</span><br />
<span style="color: red;">c2 → c3</span><span style="color: gray;"> → c4</span><br />
<span style="color: red;">d1 → d2 → d3</span></p></td>
</tr>
</tbody>
</table>

除了調控方式的差異之外，當Hox基因本身改變，個體發育也會受到影響，例如Ubx基因發生變化時，會影響自身性質，進而影響與其他基因的交互作用，使動物產生不同外型。若是改變Hox基因在某一區域的表現狀況，則會使得此區域的發育產生變化，例如當Ubx與Abd-A所表現的蛋白質沒有作用時，會使Dll誘導前肢發育。不同區域的Hox基因表現變化，也會導致不同區域發展出不同的構造，例如Ubx與Abd-A在不同部位的交互作用方式差異，是造成不同附肢形成的原因。此外，基因的表現量也會影響發育，例如Hox基因-{}-數目不同的昆蟲與脊椎動物之間的差異。

## 各物種的Hox基因

### 果蠅

[Hoxgenesoffruitfly.svg](https://zh.wikipedia.org/wiki/File:Hoxgenesoffruitfly.svg "fig:Hoxgenesoffruitfly.svg")的8個Hox基因。左邊的5個屬於ANT-C；右邊的3個屬於BX-C。基因與果蠅身體的顏色，表示基因所影響的身體部位。\]\]

[果蠅](../Page/果蠅.md "wikilink")（這裡指[黑腹果蠅](../Page/黑腹果蠅.md "wikilink")）的Hox基因皆位在其[3號染色體右臂上](../Page/3號染色體_\(果蠅\).md "wikilink")，可分為兩個群集。一群稱作雙胸複合群（Bithorax
complex，BX-C），此群集中的*Ubx*基因若突變，會使第3胸節（T3）變成第2胸節（T2），而且由於正常狀態下的[翅膀是長在T](../Page/翅膀.md "wikilink")2之上，因此這樣的突變同時會使果蠅長出兩對翅膀，並使原本長在T3上的平衡棒（haltere；[雙翅目昆蟲特有的構造](../Page/雙翅目.md "wikilink")，源自其中一對翅膀）消失。此突變最早是[卡耳文·布里吉斯](../Page/卡耳文·布里吉斯.md "wikilink")（Calvin
Bridges）於1915年發現。

另一群基因則稱為觸足複合群（Antennapedia
complex，ANT-C），此基因群集中的*Antp*基因發生突變時，會使長在頭上的[觸角替換成第二对](../Page/觸角.md "wikilink")[腿](../Page/腿.md "wikilink")。另外*pb*基因的突變，則是會使頭上的[口器變成腿](../Page/口器.md "wikilink")。

兩群基因一開始的研究成果，皆是由觀察它們對[成蟲的影響而得](../Page/成蟲.md "wikilink")。較晚的研究則發現，對果蠅的[幼蟲](../Page/幼蟲.md "wikilink")（[蛆](../Page/蛆.md "wikilink")）來說，BX-C影響了T3到A8部分的發育（果蠅分節：頭、T1\~T3、A1\~A8、性器），其中主要的影響位置是T3到A1。ANT-C則是影響[頭部以及T](../Page/頭.md "wikilink")1到T2部分。

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>群集名稱</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>位置</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>基因</strong>（<strong>名稱意義</strong>）</p>
</div></td>
<td><div style="text-align: center;">
<p><strong>影響位置</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p>ANT-C</p></td>
<td><p>靠近3'端</p></td>
<td><p><em>lab</em>（labial；唇）<br />
<em>pb</em>（proboscipedia；口器足）<br />
<em>Dfd</em>（Deformed；畸形）<br />
<em>Scr</em>（Sex combs reduced；<a href="../Page/性梳.md" title="wikilink">性梳減少</a>）<br />
<em>Antp</em>（Antennapedia；觸足）</p></td>
<td><p>身體前部</p></td>
</tr>
<tr class="odd">
<td><p>BX-C</p></td>
<td><p>靠近5'端</p></td>
<td><p><em>Ubx</em>（Ultrabithorax）<br />
<em>AbdA</em>（Abdominal A）<br />
<em>AbdB</em>（Abdominal B）</p></td>
<td><p>身體後部</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 人類

[人類的Hox基因可分成](../Page/人類.md "wikilink")4個基因群集，分別位在不同的染色體上，這些染色體分別是7號、17號、12號與2號。人類的Hox基因在書寫時以全大寫表達，例如HOXA、HOXB、HOXC與HOXD。此外，這些基因可分成13個平行同源家族（paralogous
family），以數字表示，例如HOXA4，HOXB4，HOXC4與HOXD4。這些家族成員的DNA序列與轉錄出來的[蛋白質序列類似](../Page/蛋白質.md "wikilink")。

其中HOXD13的突變會造成[多指症](../Page/多指症.md "wikilink")（synpolydactyly）；而HOXA13的突變則會導致[手腳生殖器症](../Page/手腳生殖器症.md "wikilink")（hand-foot-genital
syndrome，HGFS）。另外HOXB群集中的8個成員會影響[紅血球的發育](../Page/紅血球.md "wikilink")，其中HOXB4與HOXB7又會影響[T細胞與](../Page/T細胞.md "wikilink")[B細胞](../Page/B細胞.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>位置</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>基因群集</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>影響頭部</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>影響頸部</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>影響軀幹</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>影響四肢</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/7號染色體_(人類).md" title="wikilink">7號染色體</a><br />
<em>7p15 ~ p14</em></p></td>
<td><p>HOXA</p></td>
<td><p><br />
<br />
<br />
</p></td>
<td><p><br />
</p></td>
<td></td>
<td><p><br />
<br />
<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/17號染色體_(人類).md" title="wikilink">17號染色體</a><br />
17q21 ~ q22</p></td>
<td><p>HOXB</p></td>
<td><p><br />
<br />
<br />
</p></td>
<td><p><br />
</p></td>
<td><p><br />
</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/12號染色體_(人類).md" title="wikilink">12號染色體</a><br />
12q12 ~ q13</p></td>
<td><p>HOXC</p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td><p><br />
<br />
<br />
<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2號染色體_(人類).md" title="wikilink">2號染色體</a><br />
2q31 ~ q32</p></td>
<td><p>HOXD</p></td>
<td><p><br />
<br />
</p></td>
<td></td>
<td></td>
<td><p><br />
<br />
<br />
<br />
</p></td>
</tr>
</tbody>
</table>

### 老鼠

[老鼠的Hox基因特性與人類相似](../Page/老鼠.md "wikilink")，分別位在6號、11號、15號與2號染色體上，書寫時只有首字母大寫，例如Hoxa-11與Hoxd-11。

第3平行同源家族，也就是Hoxa-3、Hoxb-3與Hoxd-3基因，主要作用是產生頸部的[脊椎骨](../Page/脊椎骨.md "wikilink")。另外Hoxa-11或是Hoxd-11其中一的基因發生突變時，會使[橈骨](../Page/橈骨.md "wikilink")（radius）或[尺骨](../Page/尺骨.md "wikilink")（ulna）產生輕微缺陷。若是兩個基因同時突變，則會使兩塊骨頭皆退化。

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>位置</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>基因群集</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>基因</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/6號染色體_(老鼠).md" title="wikilink">6號染色體</a></p></td>
<td><p>Hoxa</p></td>
<td><p>Hoxa-1<br />
Hoxa-2<br />
Hoxa-3<br />
Hoxa-4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/11號染色體_(老鼠).md" title="wikilink">11號染色體</a></p></td>
<td><p>Hoxb</p></td>
<td><p>Hoxb-1<br />
Hoxb-2<br />
Hoxb-3<br />
Hoxb-4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/15號染色體_(老鼠).md" title="wikilink">15號染色體</a></p></td>
<td><p>Hoxc</p></td>
<td><p>Hoxc-4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2號染色體_(老鼠).md" title="wikilink">2號染色體</a></p></td>
<td><p>Hoxd</p></td>
<td><p>Hoxd-1<br />
Hoxd-3<br />
Hoxd-4</p></td>
</tr>
</tbody>
</table>

## 參考文獻

  - Daniel Hartl & Elizabath W. Jones. *Genetic*. 6th ed. Jones and
    Bartlett Publishers. ISBN 0-7637-1511-5

  - 大石正道。《圖解人類基因組的構造》。林碧清譯。世茂出版社。2002年12月。ISBN 957-776-432-0

  - Sean B. Carrol。《蝴蝶斑馬與胚胎：探索演化發生學之美》（*Endless Forms Most Beautiful*:
    *The New Science of Evo Devo*）。王惟芬譯。商周出版。2006年。ISBN
    978-986-124-774-8

  - 生命经纬 -
    [ANT-C复合座位的结构和功能](http://www.biox.cn/content/20050707/25092.htm)

  - Brian E. Staveley - [Evolution and
    Development](http://www.mun.ca/biology/desmid/brian/BIOL3530/DB_Ch15/BIOL2900_EvoDevo.html)

  - 清華大學 -
    [Organogenesis](https://web.archive.org/web/20070525022249/http://www.dls.ym.edu.tw/lesson3/org2.htm)

  - 郭曉卉 -
    [同位序列基因與細胞分化及器官發育](https://web.archive.org/web/20060409172002/http://science.scu.edu.tw/micro/800/learn/01class_gene/same_serial.htm)

  - 清華大學 - [Developmental
    Biology](https://web.archive.org/web/20070525022348/http://www.dls.ym.edu.tw/lesson3/plant.htm)

  - Leland H. Hartwell. *et al.* *Genetics - From gene to genome*.
    McGraw-Hill. ISBN 0-07-540923-2

## 外部連結

  - [生物通 - 新發現
    Hox基因成簇是多餘的？](http://www.ebiotrade.com/newsf/2005-5/20055995128.htm)

  - [科景 -
    生物：一個就夠了](https://web.archive.org/web/20070927173051/http://www.sciscape.org/news_detail.php?news_id=611)

  - [科景 -
    生物：重造熱血奔騰的斑馬魚](https://web.archive.org/web/20071016210629/http://sciscape.org/news_detail.php?news_id=1278)

  - [Society for Neuroscience - Hox Genes and Birth
    Defects](http://www.sfn.org/index.cfm?pagename=brainBriefings_hoxGenesAndBirthDefects)

  - [A brief overview of Hox
    genes](https://web.archive.org/web/20070421111012/http://pharyngula.org/index/weblog/comments/a_brief_overview_of_hox_genes/)

## 參見

  - [同源框](../Page/同源框.md "wikilink")
  - [發育生物學](../Page/發育生物學.md "wikilink")
  - [演化發育生物學](../Page/演化發育生物學.md "wikilink")
  - [動物胚胎發生](../Page/動物胚胎發生.md "wikilink")
  - [同源基因](../Page/同源基因.md "wikilink")（Homeotic genes）

[Category:发育基因与蛋白质](../Category/发育基因与蛋白质.md "wikilink")