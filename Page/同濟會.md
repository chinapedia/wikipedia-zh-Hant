**國際同濟會**（Kiwanis
International）是一個以「關懷兒童，無遠弗屆」為任務目標的[服務性組織](../Page/服務性組織.md "wikilink")，1915年1月21日創建於[美國](../Page/美國.md "wikilink")[密西根州的](../Page/密西根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")。現在，國際同濟會的總部在[印第安納州的](../Page/印第安納州.md "wikilink")[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")。

「Kiwanis」源於底特律地區的一種印地安語中的「Nunc Kee-wanis」，意思是「我們交易」（we trade）或「祝你愉快」（we
have a good time）。

該會原本的宗旨在於促進成員間的[商業交流和幫助窮困](../Page/商業.md "wikilink")。關於應定位為一個[社交性組織還是服務性組織的討論](../Page/社交性組織.md "wikilink")，在1919年獲得解決，國際同濟會選擇以服務為重心。在1987年以前，這個組織只有男性參加，然而在該年開始加入女性成員後，女性成員的比例日漸提高。

身為全球第三大的服務性組織，在2005年，同濟會在93個國家設有分會，成員總數達到275,000人。成員的平均年齡是57歲。所屬13,000個單位的平均規模為34人。

## 參考資料

  - 維基百科編者，2006，〈Kiwanis〉，《Wikipedia, The Free Encyclopedia》，2006年2月17日
    01:02，http://en.wikipedia.org/w/index.php?title=Kiwanis\&oldid=39733316.

[Category:美國組織](../Category/美國組織.md "wikilink")
[Category:1915年建立](../Category/1915年建立.md "wikilink")
[Category:兄弟会服务组织](../Category/兄弟会服务组织.md "wikilink")