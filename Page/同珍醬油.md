[HK_TungChun_FanlingFactory.JPG](https://zh.wikipedia.org/wiki/File:HK_TungChun_FanlingFactory.JPG "fig:HK_TungChun_FanlingFactory.JPG")[軍地同珍醬油廠](../Page/軍地.md "wikilink")\]\]
[HKBPE_香港工展會_CWB_Victoria_Park_HK_Brands_and_Products_Expo_booth_Tung_Chun_night_Dec-2015_DSC.JPG](https://zh.wikipedia.org/wiki/File:HKBPE_香港工展會_CWB_Victoria_Park_HK_Brands_and_Products_Expo_booth_Tung_Chun_night_Dec-2015_DSC.JPG "fig:HKBPE_香港工展會_CWB_Victoria_Park_HK_Brands_and_Products_Expo_booth_Tung_Chun_night_Dec-2015_DSC.JPG")
**同珍醬油罐頭有限公司**，簡稱**同珍醬油**（**Tung Chun Soy
Sauce**），是[香港製造的](../Page/香港.md "wikilink")[品牌](../Page/品牌.md "wikilink")[老字號](../Page/老字號.md "wikilink")，屬[香港品牌產品](../Page/香港品牌產品.md "wikilink")，1876年創業於[廣東省](../Page/廣東省.md "wikilink")[東莞](../Page/東莞.md "wikilink")[石排镇](../Page/石排镇.md "wikilink")，於19世紀末從東莞到香港落地生根，1919年在香港註冊，與[李錦記](../Page/李錦記.md "wikilink")、[淘大和](../Page/淘大食品.md "wikilink")[八珍並稱香港四大醬園家族](../Page/八珍甜醋.md "wikilink")。

同珍醬油在[香港設有](../Page/香港.md "wikilink")[葵涌昌榮路](../Page/葵涌.md "wikilink")（35萬[平方呎](../Page/平方呎.md "wikilink")）及[粉嶺](../Page/粉嶺.md "wikilink")[軍地](../Page/軍地.md "wikilink")（12萬平方呎）兩處廠房。現任同珍集團董事經理[王賜豪是家族第三代經營者](../Page/王賜豪.md "wikilink")\[1\]，於1998年接任，他是一名耳鼻喉專科醫生，身兼廣東省東莞市政協常委。

「同珍」的[醬油於](../Page/醬油.md "wikilink")2003年獲得「[香港Q嘜](../Page/香港工業總會#香港Q嘜.md "wikilink")」優質產品認證。

## 地產業務

同珍的業務亦已由醬油發展至[地產項目](../Page/地產.md "wikilink")，亦將其位於葵涌的廠房有意發展為服務式住宅，但因[補地價金額與政府未能達成協議而擱置](../Page/補地價.md "wikilink")\[2\]。

## 相關

  - [樂善堂王仲銘中學](../Page/樂善堂王仲銘中學.md "wikilink")，同珍醬油第二代[王仲銘曾任](../Page/王仲銘.md "wikilink")[九龍樂善堂主席](../Page/九龍樂善堂.md "wikilink")；
  - [保良局王賜豪（田心谷）小學](../Page/保良局王賜豪（田心谷）小學.md "wikilink")，同珍醬油第三代王賜豪曾任癸未年[保良局](../Page/保良局.md "wikilink")（2003年）主席；
  - [旺角](../Page/旺角.md "wikilink")[同珍商業中心](../Page/同珍商業中心.md "wikilink")，同珍醬油公司物業。

## 赞助

2010年[農曆新年](../Page/農曆新年.md "wikilink")，同珍醬油與香港東莞同鄉總會（主席同為王賜豪）聯合赞助在[維港的賀歲煙花](../Page/香港賀歲煙花匯演.md "wikilink")，名為「萬彩煙花耀維港」。

## 註腳

## 外部連結

  - [同珍集團網頁](http://www.tungchun.hk/)

[Category:1876年成立的公司](../Category/1876年成立的公司.md "wikilink")
[Category:香港家族式企業](../Category/香港家族式企業.md "wikilink")
[Category:香港食品製造商](../Category/香港食品製造商.md "wikilink")
[Category:香港名牌](../Category/香港名牌.md "wikilink")
[Category:同珍集團](../Category/同珍集團.md "wikilink")

1.
2.