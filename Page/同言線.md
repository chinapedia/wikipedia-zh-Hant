[Faroe_islands_isoglosses.png](https://zh.wikipedia.org/wiki/File:Faroe_islands_isoglosses.png "fig:Faroe_islands_isoglosses.png")同言線.\]\]
[German_dialectal_map.PNG](https://zh.wikipedia.org/wiki/File:German_dialectal_map.PNG "fig:German_dialectal_map.PNG")
subdivides into [Upper German](../Page/Upper_German.md "wikilink")
(green) and [Central German](../Page/Central_German.md "wikilink")
(blue), and is distinguished from [Low
Franconian](../Page/Low_Franconian.md "wikilink") and [Low
German](../Page/Low_German.md "wikilink") (yellow). The main isoglosses,
the [Benrath](../Page/Benrath_line.md "wikilink") and [Speyer
lines](../Page/Speyer_line.md "wikilink"), are marked in black.\]\]

'''同言線 **（isogloss、heterogloss），也稱作**等語線
'''，是指某一個特定的[語言學特徵的分佈地理界線](../Page/語言學.md "wikilink")
，譬如，某個[元音的發音](../Page/元音.md "wikilink")、某個[詞彙的意義](../Page/詞彙.md "wikilink")、或是某種[句法](../Page/句法.md "wikilink")。[方言界線的劃分一般就是許多條大致重疊的同言線](../Page/方言.md "wikilink")
。

## 參考文獻

  -
  -
## 外部連結

  - [江苏境内长江两岸江淮官话与吴语边界的同言线](https://web.archive.org/web/20160304084316/http://www.xici.net/d140721570.htm)
  - [An example of an isogloss in Southern
    England](https://web.archive.org/web/20060924104632/http://www.phon.ucl.ac.uk/home/johnm/sid/isogloss.htm).
  - *[Beyond the Isogloss: The Isograph in Dialect
    Topography](http://www.chass.utoronto.ca/canengglobal/abstracts/tony_pi.pdf):*
    A discussion of the shortcomings and oversimplifications of using
    isoglosses.
  - *[On Some Acoustic Correlates of
    Isoglossy](http://specgram.com/LP/20.rankin.isoglossy.html):* A
    humorous analysis of Russian isoglossy.

[Category:语言学](../Category/语言学.md "wikilink")