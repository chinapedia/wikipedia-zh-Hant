在[数学特别是](../Page/数学.md "wikilink")[抽象代数中](../Page/抽象代数.md "wikilink")，**同餘关系**或简称**[同餘](../Page/同餘.md "wikilink")**是相容于某个代数运算的[等价关系](../Page/等价关系.md "wikilink")。

## 模算术

元型例子是[模算术](../Page/模算术.md "wikilink")：对于一个正[整数](../Page/整数.md "wikilink")*n*，两个整数*a*和*b*被称为**同餘模*n***，如果*a* − *b*整除于*n*（还有一个等价的条件是它们除以*n*得出同样的餘数）。

例如，5和11同餘模3：

  -
    11 ≡ 5 (mod 3)

因为11 − 5得出6，它整除于3。或者等价的说，这两个数除以3得到相同的餘数：

  -
    11 = 3×3 + 2
    5 = 1×3 + 2

如果\(a_1 \equiv b_1 \pmod n\)并且\(a_2 \equiv b_2 \pmod n\)，则\(a_1+a_2 \equiv b_1+b_2 \pmod n\)并且\(a_1a_2 \equiv b_1b_2 \pmod n\)。这把同餘(mod
*n*)变成了在所有整数的环上的一个等价。

## 线性代数

两个实数[矩阵](../Page/矩阵.md "wikilink")*A*和*B*被称为[合同的](../Page/合同矩陣.md "wikilink")，如果存在[可逆实数矩阵](../Page/可逆矩阵.md "wikilink")*P*使得

\[P^\top A P = B\]。

对称矩阵有实数[特征值](../Page/特征值.md "wikilink")。对称矩阵的“惯性”是由正特征值的数目、零特征值的数目和负特征值的数目组成的三元组。[Sylvester惯性定律声称两个对称实数矩阵是合同的](../Page/Sylvester惯性定律.md "wikilink")，当且仅当它们有相同的惯性。所以，全等变换可以改变矩阵的特征值但不能改变特征值的符号。

对于复数矩阵，必须区分“<sup>T</sup>合同”（*A*和*B*是<sup>T</sup>合同，如果有可逆矩阵*P*使得*P*<sup>T</sup>*AP*
= *B*）和“\*合同”（*A*和*B*是\*合同，如果有可逆矩阵*P*使得*P*\**AP* = *B*）。

## 泛代数

想法是推广到[泛代数中](../Page/泛代数.md "wikilink")：代数*A*上的同餘关系是[直积](../Page/直积.md "wikilink")*A*×*A*的[子集](../Page/子集.md "wikilink")，它既是在*A*上的[等价关系又是](../Page/等价关系.md "wikilink")*A*×*A*的[子代数](../Page/子代数.md "wikilink")。

[同态的](../Page/同态.md "wikilink")[核总是同餘](../Page/核_\(代数\).md "wikilink")。实际上，所有同餘引起自核。对于给定在*A*上的同餘\~，[等价类的集合](../Page/等价类.md "wikilink")*A*/\~可以自然的方式给出自代数的结构[商代数](../Page/商代数.md "wikilink")。映射所有*A*的元素到它的等价类的函数是同态，这个同态的核是\~。

在一个代数上的所有同餘关系的[格是](../Page/格_\(数学\).md "wikilink")[代数格](../Page/代数格.md "wikilink")。

## 群的同餘、正规子群和理想

在[群的特殊情况下](../Page/群.md "wikilink")，同餘关系可以用基本术语描述为：如果*G*是群（带有[单位元](../Page/单位元.md "wikilink")*e*）并且\~是在*G*上的[二元关系](../Page/二元关系.md "wikilink")，则\~是同餘只要：

1.  给定*G*的[任何元素](../Page/全称量词.md "wikilink")*a*，*a* \~
    *a*（**[自反关系](../Page/自反关系.md "wikilink")**）。
2.  给定*G*任何的元素*a*和*b*，[如果](../Page/实质蕴涵.md "wikilink")*a* \~ *b*，则*b* \~
    *a*（**[对称关系](../Page/对称关系.md "wikilink")**）。
3.  给定*G*的任何元素*a*,*b*和*c*，如果*a* \~ *b*
    [并且](../Page/逻辑合取.md "wikilink")*b* \~ *c*，则*a* \~
    *c*（**[传递关系](../Page/传递关系.md "wikilink")**）。
4.  给定*G*的任何元素*a*,*a*',*b*和''b' *，如果*a'' \~ ''a' *并且*b'' \~ ''b' *，则*a''
    \* *b* \~ ''a' '' \* ''b' ''。
5.  给定*G*的任何元素*a*和''a' *，如果*a'' \~ ''a' *，则*a''<sup>−1</sup> \~ ''a'
    ''<sup>−1</sup>（这个条件可以从其他四个条件证明，所以严格上是冗餘的）。

条件1, 2和3声称\~是[等价关系](../Page/等价关系.md "wikilink")。

同餘\~完全确定自*G*的同餘于单位元的那些元素的集合{*a* ∈ *G* : *a* \~
*e*}，而这个集合是[正规子群](../Page/正规子群.md "wikilink")。特别是，*a*
\~ *b*当且仅当*b*<sup>−1</sup> \* *a* \~
*e*。所以替代谈论在群上同餘，人们通常以正规子群的方式谈论它们；事实上，所有同餘都唯一的对应于*G*的某个正规子群。

### 环理想和一般情况的核

类似的技巧允许谈论环中的核为[理想来替代同餘关系](../Page/理想_\(环论\).md "wikilink")，在[模理论中为](../Page/模.md "wikilink")[子模来替代同餘关系](../Page/子模.md "wikilink")。

这个技巧不适用于[幺半群](../Page/幺半群.md "wikilink")，所以同餘关系的研究在幺半群理论扮演更中心的角色。

## 参见

  - [合同](../Page/合同_\(數學\).md "wikilink")
  - [等價關係](../Page/等價關係.md "wikilink")
  - [模](../Page/模.md "wikilink")
  - [模算數](../Page/模算數.md "wikilink")

## 引用

  - Horn and Johnson, *Matrix Analysis,* Cambridge University Press,
    1985. ISBN 0-521-38632-2.（Section 4.5 discusses congruency of
    matrices.）

[T](../Category/代数.md "wikilink") [T](../Category/抽象代数.md "wikilink")
[Category:同余](../Category/同余.md "wikilink")
[Category:数学关系](../Category/数学关系.md "wikilink")