**名間交流道**為[台灣](../Page/台灣.md "wikilink")[國道三號的交流道](../Page/福爾摩沙高速公路.md "wikilink")，位於台灣[南投縣](../Page/南投縣.md "wikilink")[名間鄉](../Page/名間鄉.md "wikilink")，指標為237k，該交流道因鄰近[日月潭](../Page/日月潭.md "wikilink")（北上）、[集集等](../Page/集集鎮.md "wikilink")[南投縣重要景點](../Page/南投縣.md "wikilink")，所以導致[假日常常](../Page/假日.md "wikilink")[塞車](../Page/塞車.md "wikilink")，與[竹山交流道雷同](../Page/竹山交流道.md "wikilink")
\[1\]。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW3.svg" title="fig:TW_PHW3.svg">TW_PHW3.svg</a></p></td>
<td><p><strong>名間</strong> <strong>集集</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近交流道

## 參考資料

[Category:南投縣交流道](../Category/南投縣交流道.md "wikilink")
[Category:名間鄉](../Category/名間鄉.md "wikilink")

1.  [〈中部〉名間交流道匝道
    初三到初五雙向封閉](http://news.ltn.com.tw/news/local/paper/1176054)