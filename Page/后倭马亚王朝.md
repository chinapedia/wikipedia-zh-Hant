**[後伍麥亞王朝](../Page/後伍麥亞王朝.md "wikilink")**（即中国古代文献中的**白衣[大食](../Page/阿拉伯帝国.md "wikilink")**），或稱**[後奧米雅王朝](../Page/後奧米雅王朝.md "wikilink")**，是[穆斯林在](../Page/穆斯林.md "wikilink")[伊比利亚半岛建立的](../Page/伊比利亚半岛.md "wikilink")[伊斯兰教政权](../Page/伊斯兰教.md "wikilink")，首都是[西班牙的](../Page/西班牙.md "wikilink")[科尔多瓦](../Page/科尔多巴_\(西班牙\).md "wikilink")。由于王朝的建立者是[阿拉伯帝国被推翻了的](../Page/阿拉伯帝国.md "wikilink")[倭马亚王朝的后裔](../Page/倭马亚王朝.md "wikilink")，故名。在[中世纪](../Page/中世纪.md "wikilink")，科尔多瓦的历代[埃米尔和](../Page/埃米尔.md "wikilink")[哈里发控制了伊比利亚半岛上的大部分土地](../Page/哈里发.md "wikilink")。后来这个国家分裂为许多独立的封建领地。

[公元1000年的后倭马亚王朝.svg](https://zh.wikipedia.org/wiki/File:公元1000年的后倭马亚王朝.svg "fig:公元1000年的后倭马亚王朝.svg")

## 历代君主

### [埃米爾](../Page/埃米爾.md "wikilink")

  - [阿卜杜拉赫曼一世](../Page/阿卜杜拉赫曼一世.md "wikilink")，756年-788年
  - [希沙姆一世](../Page/希沙姆一世.md "wikilink")，788年-796年
  - [哈卡姆一世](../Page/哈卡姆一世.md "wikilink")，796年-822年
  - [阿卜杜拉赫曼二世](../Page/阿卜杜拉赫曼二世.md "wikilink")，822年-852年
  - [穆罕默德一世](../Page/穆罕默德一世_\(科尔多瓦\).md "wikilink")，852年-886年
  - [蒙齐尔](../Page/蒙齐尔.md "wikilink")，886年-888年
  - [阿卜杜拉·伊本·穆罕默德](../Page/阿卜杜拉·伊本·穆罕默德.md "wikilink")，888年-912年
  - [阿卜杜拉赫曼三世](../Page/阿卜杜拉赫曼三世.md "wikilink")，912年-929年

### 哈里发

  - [阿卜杜拉赫曼三世](../Page/阿卜杜拉赫曼三世.md "wikilink")，929年-961年
  - [哈卡姆二世](../Page/哈卡姆二世.md "wikilink")，961年-976年
  - [希沙姆二世](../Page/希沙姆二世.md "wikilink")，976年-1008年
  - [穆罕默德二世](../Page/穆罕默德二世_\(科尔多瓦\).md "wikilink")，1008年-1009年
  - [苏莱曼](../Page/苏莱曼_\(科尔多瓦\).md "wikilink")，1009年-1010年
  - [希沙姆二世](../Page/希沙姆二世.md "wikilink")（复位），1010年-1012年
  - [苏莱曼](../Page/苏莱曼_\(科尔多瓦\).md "wikilink")（复位），1012年-1017年
  - [阿卜杜拉赫曼四世](../Page/阿卜杜拉赫曼四世.md "wikilink")，1021年-1022年
  - [阿卜杜拉赫曼五世](../Page/阿卜杜拉赫曼五世.md "wikilink")，1022年-1023年
  - [穆罕默德三世](../Page/穆罕默德三世_\(科尔多瓦\).md "wikilink")，1023年-1024年
  - [希沙姆三世](../Page/希沙姆三世.md "wikilink")，1027年-1031年

## 附註

## 参见

  - [西班牙君主列表](../Page/西班牙君主列表.md "wikilink")
  - [奧米亞王朝](../Page/奧米亞王朝.md "wikilink")

{{-}}

[Category:已不存在的歐洲君主國](../Category/已不存在的歐洲君主國.md "wikilink")
[Category:阿拉伯帝国](../Category/阿拉伯帝国.md "wikilink")
[Category:中世紀西班牙](../Category/中世紀西班牙.md "wikilink")
[Category:穆斯林王朝](../Category/穆斯林王朝.md "wikilink")
[Category:哈里发国](../Category/哈里发国.md "wikilink")