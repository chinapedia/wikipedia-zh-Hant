**M63**或**梅西爾63**，也稱為**NGC
5055**或很少使用的**向日葵星系**\[1\]，是在[獵犬座的一個](../Page/獵犬座.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")。M63最初是由法國天文學家[皮埃爾·梅尚發現的](../Page/皮埃爾·梅尚.md "wikilink")，然後得到他的同事梅西爾在1779年6月14日予以證實\[2\]。這個星系被收錄為[梅西爾天體](../Page/梅西爾天體.md "wikilink")，序號為M63。在19世紀中葉，愛爾蘭天文學家[羅斯勳爵確定這星系的螺旋結構](../Page/威廉·帕森思，第三代羅斯伯爵.md "wikilink")，使其成為最早被確定結構的星系之一\[3\]。

這個星系的[型態分類是SAbc](../Page/星系型態分類.md "wikilink")\[4\]，表示它是螺旋星系，中心沒有[棒狀結構並且](../Page/棒旋星系.md "wikilink")[螺旋臂的結構鬆散](../Page/螺旋星系#螺旋臂.md "wikilink")。這種在可見光中缺乏連續的大規模螺旋臂的螺旋星系稱為"[絮結螺旋星系](../Page/絮結螺旋星系.md "wikilink")"。然而，以[近紅外線觀測時](../Page/近紅外線.md "wikilink")，有一個非常明顯的對稱雙臂結構。每個臂環繞星系150°，並從核心延伸到\[5\]。

M63是一個具有[低電離星系核的](../Page/低電離星系核.md "wikilink")[活躍星系](../Page/活躍星系.md "wikilink")。這顯示核心有未能解析的瀰漫[發射源被包覆著](../Page/譜線.md "wikilink")。後者沿著110°的[位置角延伸](../Page/位置角.md "wikilink")，在幾乎相同的方向上還有著[軟X射線和](../Page/X射線.md "wikilink")[Hα的發射](../Page/Hα.md "wikilink")\[6\]。不確定核心是否存在著[超大質量黑洞](../Page/超大質量黑洞.md "wikilink")；如果有，則估計質量為\[7\]。

觀察波長[21公分的無線電波顯示M](../Page/氫線.md "wikilink")63的氣態盤向外延伸到，遠遠超過明亮的星系盤面。這些氣體顯示一種對稱的型式，從半徑處開始有著明顯的扭曲。這種形式表明星系的[暗物質暈相對於內部區域被偏移](../Page/暗物質.md "wikilink")。翹曲的原因尚不清楚，但位置角指向較小的伴星系UGC8313\[8\]。

基於亮度的測量，M63的距離是\[9\]。相對於[本星系群的](../Page/本星系群.md "wikilink")[徑向速度估計距離是](../Page/徑向速度.md "wikilink")\[10\]。基於[塔利-費舍爾關係估算的距離範圍是](../Page/塔利-費舍爾關係.md "wikilink")。[紅巨星分支技術給出的距離是](../Page/紅巨星分支技術.md "wikilink")\[11\]。M63是[M51星系群的一部份](../Page/M51星系群.md "wikilink")，這一組星系群還包括M51\]\]（漩渦星系）\[12\]。

在1971年，在M63的螺旋臂上出現一顆[視星等](../Page/視星等.md "wikilink")11.8的超新星：SN 1971
I。它在1971年5月24日被發現，5月26日左右達到峰值光度\[13\]。的光譜與[I型超新星一致](../Page/超新星#I型超新星.md "wikilink")。然而，光譜的行為顯得異常\[14\]。

## 圖集

<File:Messier> 63 GALEX
WikiSky.jpg|thumb|巡天的[星系演化探測器拍攝的M](../Page/星系演化探測器.md "wikilink")63。創建者：[NASA](../Page/NASA.md "wikilink")
/ [WikiSky](../Page/WikiSky.md "wikilink") Image:A galactic
sunflower.jpg|哈伯太空望遠鏡拍攝的M63螺旋星系的螺旋臂\[15\]。 Image:Messier
63.jpg|[亞利桑那州](../Page/亞利桑那州.md "wikilink")[萊蒙山天文台的](../Page/萊蒙山天文台.md "wikilink")24英吋望遠鏡拍攝的M63。
Image:M63 3.6 8.0 24 microns
spitzer.png|[史匹哲太空望遠鏡以紅外線拍攝的M](../Page/史匹哲太空望遠鏡.md "wikilink")63。

## 參考資料

## 外部連結

  -
  - [Sunflower Galaxy @ SEDS Messier
    pages](http://messier.seds.org/m/m063.html)

  - [Sunflower Galaxy (M63) at Constellation
    Guide](http://www.constellation-guide.com/sunflower-galaxy-messier-63/)

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[Category:無棒螺旋星系](../Category/無棒螺旋星系.md "wikilink")
[Category:LINER星系](../Category/LINER星系.md "wikilink")
[Category:M51星系群](../Category/M51星系群.md "wikilink")
[Category:獵犬座](../Category/獵犬座.md "wikilink")
[063](../Category/梅西耶天體.md "wikilink")
[Category:NGC天體](../Category/NGC天體.md "wikilink")
[08334](../Category/UGC天體.md "wikilink")
[46153](../Category/PGC天體.md "wikilink")
[Category:1779年發現的天體](../Category/1779年發現的天體.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.