**向榮**（） 字**欣然**，汉族\[1\]
，[四川省](../Page/四川省.md "wikilink")[夔州府](../Page/夔州府.md "wikilink")[大宁縣](../Page/巫溪县.md "wikilink")（今重慶市萬州區巫溪縣）人。[清朝軍事人物](../Page/清朝.md "wikilink")，曾參與
[太平天國之戰](../Page/太平天國.md "wikilink")，有《向荣奏稿》传世。

## 簡介

向荣为[陕甘总督](../Page/陕甘总督.md "wikilink")[杨遇春所提攜](../Page/杨遇春.md "wikilink")。随[杨遇春镇压](../Page/杨遇春.md "wikilink")[河南](../Page/河南.md "wikilink")[滑县](../Page/滑县.md "wikilink")[天理教](../Page/天理教.md "wikilink")[李文成和新疆回民](../Page/李文成.md "wikilink")[张格尔叛乱](../Page/张格尔.md "wikilink")。

[道光十三年](../Page/道光.md "wikilink")（1833年），[直隶](../Page/直隶.md "wikilink")[总督](../Page/总督.md "wikilink")[琦善知向榮才](../Page/琦善.md "wikilink")，迁[开州协](../Page/开州.md "wikilink")[副将](../Page/副将.md "wikilink")。海疆戒严，率兵驻防[山海关](../Page/山海关.md "wikilink")。二十七年（1847年），擢升为四川[提督](../Page/提督.md "wikilink")。三十年（1850年），改任[湖南提督](../Page/湖南提督.md "wikilink")、[陕西固原提督](../Page/陕西固原提督.md "wikilink")、[廣西提督](../Page/廣西提督.md "wikilink")；清[咸豐時](../Page/咸豐.md "wikilink")[欽差大臣](../Page/欽差大臣.md "wikilink")。

咸丰三年（1853年）二月建立[江南大營圍困](../Page/江南大營.md "wikilink")[天京](../Page/天京.md "wikilink")，向荣以孝陵卫的[江南大营为中心](../Page/江南大营.md "wikilink")，在尧化门、黄马群、孝陵卫、高桥门、秣陵关、溧水一帶建立防線。當時大營纪律败坏，甚至拦路行劫，杀人越货，“各兵勇与本地居民结为婚姻，生有子女，各怀室家之念”\[2\]。

咸丰六年（1856年）四月，[石達開经](../Page/石達開.md "wikilink")[皖南入](../Page/皖.md "wikilink")[江苏](../Page/江苏.md "wikilink")，採声东击西策略，先遣一部占领溧水，向荣中计，连续派军争夺溧水，造成江南大營兵力空虛。[秦日纲部於](../Page/秦日纲.md "wikilink")5月26日至瓜洲，并自瓜洲渡江，攻打黄泥洲。向荣急令[張國樑由溧水星夜赶回](../Page/張國樑.md "wikilink")。[吉尔杭阿率军来救](../Page/吉尔杭阿.md "wikilink")，被包围于烟礅山。6月8日，张国梁在丹徒遭到大敗\[3\]。太平軍大破[江南大營](../Page/江南大營戰役.md "wikilink")，6月19日破曉，石达开、秦日纲二部人马即猛攻仙鹤门，“抄断大营后路”\[4\]，6月20日，石达开自统大军猛攻青马群，“各营火药、铅弹俱已打完”\[5\]，7月初，向榮退守[丹陽](../Page/丹陽.md "wikilink")\[6\]，與[秦日綱激戰月餘](../Page/秦日綱.md "wikilink")，太平軍始终未能突破丹阳城防。咸丰六年六月初一日（7月2日），向榮被革去湖北提督职，留任钦差大臣，继续督办军务\[7\]。咸丰六年七月初九日（8月9日），向榮以年老多病，腿足患疾，又遭此惨败，忧忿而死\[8\]。又一說咸丰六年七月（8月9日）向榮在营帐自盡身亡\[9\]。卒諡忠武。

向榮死後，由江南[提督](../Page/提督.md "wikilink")[和春升任接掌欽差大臣](../Page/和春.md "wikilink")。

## 注釋

{{-}}

[X向](../Category/清朝四川提督.md "wikilink")
[Category:巫溪人](../Category/巫溪人.md "wikilink")
[R荣](../Category/向姓.md "wikilink")
[Category:清朝自杀人物](../Category/清朝自杀人物.md "wikilink")
[Category:清朝廣西提督](../Category/清朝廣西提督.md "wikilink")
[Category:清朝湖北提督](../Category/清朝湖北提督.md "wikilink")
[Category:清朝湖南提督](../Category/清朝湖南提督.md "wikilink")
[Category:清朝陕西提督](../Category/清朝陕西提督.md "wikilink")
[Category:諡忠武](../Category/諡忠武.md "wikilink")

1.
2.  [张集馨](../Page/张集馨.md "wikilink")：《道咸宦海见闻录》
3.  《李秀成书供原稿》
4.  《向荣奏稿》，见丛刊《太平天国》（八），623页。
5.  《向荣奏稿》，见丛刊《太平天国》（八），639页。
6.  《向荣奏稿》，见丛刊《太平天国》（八），640页。
7.  《清文宗实录》卷二○○，咸丰六年六月丙戌。
8.  《向荣奏稿》，见丛刊《太平天国》（八），673页。
9.  《李秀成书供原稿》，見於罗尔纲《太平天国史》