**呂怡萱**（**Ivy
Lu**，），[南非](../Page/南非.md "wikilink")[華裔](../Page/華裔.md "wikilink")，在2005年[南非](../Page/南非.md "wikilink")[約翰尼斯堡華裔小姐競選亞軍](../Page/約翰尼斯堡.md "wikilink")\[1\]，隨後參選2007年[國際中華小姐競選得亞軍](../Page/國際中華小姐.md "wikilink")，姐姐[呂怡慧是](../Page/呂怡慧.md "wikilink")2005年[南非](../Page/南非.md "wikilink")[約翰尼斯堡華裔小姐競選冠軍及](../Page/約翰尼斯堡.md "wikilink")2006年[國際華裔小姐競選冠軍](../Page/國際華裔小姐競選.md "wikilink")。

## 獎項

  - 2005年：[約翰尼斯堡華裔小姐亞軍](../Page/國際中華小姐.md "wikilink")
  - 2007年：[國際中華小姐競選亞軍](../Page/國際中華小姐競選.md "wikilink")

## 曾參與的節目

### 2007年

  - 2月17日：[無綫電視](../Page/無綫電視.md "wikilink") - [美女廚房
    (第一輯)第十九集](../Page/美女廚房_\(第一輯\).md "wikilink")《美女廚房團年飯》

## 參考

## 外部連結

  - [2007年度國際中華小姐選舉官方網頁：呂怡萱個人資料](https://web.archive.org/web/20071217175901/http://tvcity.tvb.com/special/mcip2007/profile/profile20.html)

[L呂](../Category/台灣裔南非人.md "wikilink")
[Category:南非華人](../Category/南非華人.md "wikilink")
[Y怡](../Category/呂姓.md "wikilink")
[Category:金山大学校友](../Category/金山大学校友.md "wikilink")

1.  [呂怡慧拿下南非華裔小姐后冠](http://www.epochtimes.com/b5/5/1/30/n798259.htm)《大紀元》2005年1月30日