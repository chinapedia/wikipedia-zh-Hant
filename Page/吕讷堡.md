**呂訥堡**（；官方稱呼：Hansestadt Lüneburg ；[低地德語](../Page/低地德語.md "wikilink")：
Lümborg；[波拉布語](../Page/波拉布語.md "wikilink")：Glain）是位於[德国](../Page/德国.md "wikilink")[下萨克森州北部的一座](../Page/下萨克森州.md "wikilink")[漢薩同盟城市](../Page/漢薩同盟.md "wikilink")，距離最近的另一最漢薩同盟城市為西北方處的[漢堡](../Page/漢堡.md "wikilink")，它實際也是[漢堡大都會區的一部分](../Page/漢堡大都會區.md "wikilink")，人口在
72 000左右。加上週圍的[阿登多夫和](../Page/阿登多夫.md "wikilink")[巴爾多維克](../Page/巴爾多維克.md "wikilink")，人口則為103 000。2007年獲得頭銜“[Hansestadt](../Page/漢薩同盟.md "wikilink")”。該城有同名大學[呂訥堡大學](../Page/呂訥堡大學.md "wikilink")，學生數量達到城市人口的十分之一。\[1\]


## 参见

  - 吕讷堡市内“[绊脚石](../Page/绊脚石_\(艺术项目\).md "wikilink")”列表（）

## 參考文獻

## 外部連結

  - [Official website](http://www.lueneburg.de/)
  - [Official
    website](https://web.archive.org/web/20090410040817/http://www.lueneburg.de/en/desktopdefault.aspx/tabid-358)

  - [Leuphana University](http://www.leuphana.de/)

[L](../Category/下萨克森州市镇.md "wikilink")
[L](../Category/汉萨同盟.md "wikilink")

1.