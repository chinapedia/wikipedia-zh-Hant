**君特·馮·克魯格**（，）是一位[德國陸軍將領](../Page/納粹德國陸軍.md "wikilink")，最高軍階為[元帥](../Page/德國陸軍元帥列表.md "wikilink")，在[第二次世界大戰參加了許多重大戰役](../Page/第二次世界大戰.md "wikilink")，擔任指揮要職，最終因涉及政變陰謀而服毒自殺身亡。

[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，他是參謀部的軍官，曾於1916年參加[凡爾登戰役](../Page/凡爾登戰役.md "wikilink")。

1936年，他被晉用為陸軍[上將](../Page/上將.md "wikilink")。1937年奉命指揮第6軍團；該軍團後來改編為[第4軍團](../Page/德國第4集团军.md "wikilink")。1939年他指揮該軍團攻入[波蘭](../Page/波蘭.md "wikilink")。儘管他反對進攻西線，但他仍在[法國戰役中率領第](../Page/法國戰役_\(二戰\).md "wikilink")4軍團進攻[阿登](../Page/阿登.md "wikilink")。1940年7月被晉升為[陸軍元帥後](../Page/陸軍元帥.md "wikilink")，他在1941年6月22日开始的[巴巴羅薩行動中繼續指揮第](../Page/巴巴羅薩行動.md "wikilink")4軍團；在這段時間他與[古德里安因進攻戰術上的分岐及古德里安經常違反他的命令而雙方關係惡劣](../Page/海因茨·古德里安.md "wikilink")。

當[馮·博克在](../Page/費多爾·馮·博克.md "wikilink")1941年12月被解除[中央集團軍群的指揮權後](../Page/中央集團軍.md "wikilink")，克魯格被晉升為集團軍群司令官直至1943年10月受傷為止。克魯格經常乘坐飛機，來巡視麾下師團及有時在空中乘坐飛機以解除厭倦\[1\]
— 一個非常规的方法。

1943年10月27日，克魯格因其座駕在[明斯克](../Page/明斯克.md "wikilink")-[斯摩棱斯克公路上發生意外翻車而受重傷](../Page/斯摩棱斯克.md "wikilink")，他不能履行指揮職責；直至1944年7月，他受命替代[馮·倫德施泰特為](../Page/格特·馮·倫德施泰特.md "wikilink")[德國西歐戰場總司令](../Page/西方戰線.md "wikilink")。

德國抵抗運動領導人[特萊斯科夫將軍是中央集團軍群的參謀長](../Page/曼凱爾·馮·特萊斯科夫.md "wikilink")，克魯格亦間接參與了抵抗運動。他知道特萊斯科夫計劃在希特勒巡視中央集團軍群司令部時，要射殺希特勒，所以事先通知了他的前任下屬[馮·波斯拉格](../Page/格奧爾格·馮·波斯拉格.md "wikilink")，而波斯拉格當時即是特萊斯科夫的下屬。在最後時刻，克魯格最後放棄了特萊斯科夫的刺殺計劃，波斯拉格其後推測這是因為[希姆萊將陪同希特勒前往開會](../Page/海因里希·希姆萊.md "wikilink")；克魯格擔心希姆萊也會被殺，而一旦希姆萊被除去，則將引爆[武裝親衛隊及德意志國防軍陆军之間的一場慘烈的內戰](../Page/武裝親衛隊.md "wikilink")\[2\]。

當[施陶芬貝格在](../Page/克勞斯·馮·施陶芬貝格.md "wikilink")[7月20日密謀案中企圖行刺希特勒時](../Page/7月20日密謀案.md "wikilink")，克魯格正身任德國西歐戰場司令在其位於[拉羅什蓋恩城堡的司令部中](../Page/拉羅什蓋恩城堡.md "wikilink")。法國佔領軍司令[馮·史圖爾普納格上將](../Page/卡爾-海因利希·馮·史圖爾普納格.md "wikilink")，及其同事[馮·塞奧特少將](../Page/凱撒·馮·塞奧特.md "wikilink")
— 施陶芬貝格的姪兒 —
前往拜訪克魯格；史圖爾普納格來訪前，甫命令逮捕德軍駐防在[巴黎的武裝親衛隊單位](../Page/巴黎.md "wikilink")。由於克魯格當時已知希特勒仍然健在，所以拒絕提供任何協助，他說“可以，只要那头猪已经死了！”\[3\]。在施陶芬貝格的計劃失敗後，他被希特勒召回[柏林](../Page/柏林.md "wikilink")，但是認為希特勒會把他視為背叛者來懲罰，於是在返回德國途中服食[氰化物自殺](../Page/氰化物.md "wikilink")。他遺留一封信給希特勒，信中建議希特勒應該尋求和平及有需要時可以結束為鞏固權力而推展的鬥爭，希特勒將信交給[約德爾並說](../Page/阿爾弗莱德·約德爾.md "wikilink")：“這是很有力的證據，說明克魯格應該被處死。”\[4\]

## 註釋

## 參考文獻

  - Hoffman, Peter, (tr. Richard Barry) *The History of the German
    Resistance, 1939–1945*, Cambridge, MA: MIT Press, 1977. ISBN
    0773515313
  - [Shirer, William L.](../Page/William_L._Shirer.md "wikilink") *[The
    Rise and Fall of the Third
    Reich](../Page/The_Rise_and_Fall_of_the_Third_Reich.md "wikilink")*,
    New York: [Simon and
    Schuster](../Page/Simon_and_Schuster.md "wikilink"), 1990. ISBN
    0671728687
  - [Knopp, Guido](../Page/Guido_Knopp.md "wikilink") *Die Wehrmacht:
    Eine Bilanz*, [C. Bertelsmann
    Verlag](../Page/Bertelsmann.md "wikilink"), München, 2007. ISBN
    978-3-570-00975-8

## 外部連結

{{-}}

[Category:橡葉騎士佩寶劍鐵十字勳章人物](../Category/橡葉騎士佩寶劍鐵十字勳章人物.md "wikilink")
[Category:納粹德國元帥](../Category/納粹德國元帥.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:德國軍事人物](../Category/德國軍事人物.md "wikilink")
[Category:德國第一次世界大戰人物](../Category/德國第一次世界大戰人物.md "wikilink")
[Category:德國貴族](../Category/德國貴族.md "wikilink")
[Category:德國自殺者](../Category/德國自殺者.md "wikilink")
[Category:波森人](../Category/波森人.md "wikilink")
[Category:服毒自殺者](../Category/服毒自殺者.md "wikilink")

1.  *The History of the German Resistance, 1939–1945*, p. 276
2.  *Die Wehrmacht: Eine Bilanz*, p. 226.
3.  *Die Wehrmacht: Eine Bilanz*, p. 251.
4.  *Rise and Fall of the Third Reich*, pp. 1076–77