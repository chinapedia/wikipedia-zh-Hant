**啟功**（），[愛新覺羅氏](../Page/愛新覺羅.md "wikilink")，[字](../Page/表字.md "wikilink")**元白**（或作**元伯**），[滿族](../Page/满族.md "wikilink")[正藍旗人](../Page/正藍旗.md "wikilink")。[清世宗嫡派後裔](../Page/清世宗.md "wikilink")，幼孤，自称学习上得益于姑姑甚多。

是一位[中國的](../Page/中國.md "wikilink")[古典文獻學家](../Page/古典文献学.md "wikilink")。他曾任職[輔仁大學附屬中學](../Page/北平輔仁大學附屬中學.md "wikilink")[美術教師](../Page/美術.md "wikilink")、[輔仁大學中文系教師](../Page/輔仁大學.md "wikilink")、[北京師範大學](../Page/北京师范大学.md "wikilink")[中文系教授](../Page/中文學系.md "wikilink")、[中國人民政治協商會議全國委員會常務委員](../Page/中国人民政治协商会议.md "wikilink")、[中央文史研究館館長](../Page/中央文史研究馆.md "wikilink")、[九三學社](../Page/九三学社.md "wikilink")[顧問](../Page/顧問.md "wikilink")、[國家文物鑑定委員會主任委員](../Page/國家文物鑑定委員會.md "wikilink")、[中國書法家協會名譽主席](../Page/中国书法家协会.md "wikilink")、[中國佛教協會顧問](../Page/中国佛教协会.md "wikilink")、[故宮博物院顧問](../Page/故宫博物院.md "wikilink")、[國家博物館顧問](../Page/国家博物馆.md "wikilink")、[西泠印社社長等職務](../Page/西泠印社.md "wikilink")。

此外，他還在[業餘從事](../Page/業餘愛好者.md "wikilink")[書法](../Page/書法.md "wikilink")、[中國畫和](../Page/中國畫.md "wikilink")[詩詞創作](../Page/詩詞.md "wikilink")，並參與故宮博物院、[國家文物局等機構組織的](../Page/國家文物局.md "wikilink")[文物鑑定](../Page/文物鑑定.md "wikilink")。今日，他的字跡被[方正公司製成電腦漢字字體的](../Page/方正集团.md "wikilink")[方正啟體](../Page/方正啟體.md "wikilink")。

## 生平

[Qi_Gong_1923.jpg](https://zh.wikipedia.org/wiki/File:Qi_Gong_1923.jpg "fig:Qi_Gong_1923.jpg")

启功为清朝近支宗室后裔，属[正蓝旗](../Page/八旗制度.md "wikilink")，是[雍正帝九世孙](../Page/雍正帝.md "wikilink")，和恭亲王[弘昼八世孙](../Page/弘昼.md "wikilink")。曾祖父[溥良為](../Page/溥良.md "wikilink")[光绪六年](../Page/光绪.md "wikilink")（1880年）庚辰科進士，1908年秋末，曾作为礼部尚书目睹光绪被害的下毒过程。祖父[毓隆為](../Page/毓隆.md "wikilink")[光緒二十年](../Page/光緒.md "wikilink")（1894年）甲午恩科進士，父親[恆同封](../Page/恆同.md "wikilink")[奉恩將軍](../Page/奉恩將軍.md "wikilink")\[1\]。清朝宗室虽为[爱新觉罗氏](../Page/爱新觉罗.md "wikilink")，但启功明确表示不再以「爱新觉罗」或「金」为姓氏，而以「启」为姓。\[2\]

启功自汇文中学肄业后，师从[贾尔鲁](../Page/贾尔鲁.md "wikilink")（羲民）和[吴熙曾](../Page/吴熙曾.md "wikilink")（镜汀）学习书法[丹青](../Page/丹青.md "wikilink")，从[戴绥之](../Page/戴绥之.md "wikilink")（姜福）学习[古典文学](../Page/古典文学.md "wikilink")。曾受业于元史学家[陈垣](../Page/陈垣.md "wikilink")，专门从事中国文学史、[中国美术史](../Page/中国美术史.md "wikilink")、中国历代[散文](../Page/散文.md "wikilink")、历代诗选和唐宋词等课程的教学与研究。

因身為清朝宗室後裔，1957年[反右運動中](../Page/反右運動.md "wikilink")，為劃為右派。[文化大革命期間](../Page/文化大革命.md "wikilink")，列為「牛鬼蛇神」之中，遭受不斷審查與批判。

2005年2月9日下午6點，因突发[脑血栓形成住進](../Page/脑血栓形成.md "wikilink")[北京](../Page/北京.md "wikilink")[北大医院](../Page/北大医院.md "wikilink")，昏迷至6月30日2时25分，因脑血管、心血管病併發症逝世。

中华书局“灿然书屋”牌匾、人民教育出版社《中日交流标准日本语》题名为其挥毫。

[Qi_Gong_1930.jpg](https://zh.wikipedia.org/wiki/File:Qi_Gong_1930.jpg "fig:Qi_Gong_1930.jpg")

## 家族

<center>

</center>

## 主要学术成就

[_Qi_Gong_1955.jpg](https://zh.wikipedia.org/wiki/File:_Qi_Gong_1955.jpg "fig:_Qi_Gong_1955.jpg")

啟功除了是[古典文献学家](../Page/古典文献学.md "wikilink")，亦从事书画创作、古書畫[鑒定](../Page/鑒定.md "wikilink")、[碑帖研究](../Page/碑帖.md "wikilink")。在碑帖之學上，啟功開拓了新的研究方法，啟功嘗作詩論曰：「買櫝還珠事不同，拓碑多半為書工。滔滔駢散終何用，幾見藏家誦一通。」一改以往名家學者，如[葉昌熾](../Page/葉昌熾.md "wikilink")、[翁方綱等研究歷代碑帖只重形式](../Page/翁方綱.md "wikilink")，不重內容；只知書法，而略其辭章之習。

除研究方法開拓新途外，啟功更對《[孝女曹娥碑](../Page/孝女曹娥碑.md "wikilink")》的真偽作出一硾定音之論，判定歷代相傳的《曹娥碑》殊非[王羲之真跡](../Page/王羲之.md "wikilink")。期間，雖有部分學者提出異議，如香港學者[陳勝長曾撰](../Page/陳勝長.md "wikilink")〈絹本《孝女曹娥碑》墨跡考辨〉與之辯論，惟啟功以其獨特的研究方法與深厚學養，對陳氏之立論作出有力反駁，並深責陳氏之說乃「一派胡言」，終使《孝女曹娥碑》的真偽得以辨明。詳細論述請參考啟功《論書絕句》、《古代字體論稿》、《論書札記》等書。

## 著作

  - 《古代字体论稿》
  - 《诗文声律论稿》
  - 《启功丛稿》
  - 《启功韵语》
  - 《启功絮语》
  - 《启功赘语》
  - 《汉语现象论丛》
  - 《论书绝句》
  - 《论书札记》
  - 《说八股》
  - 《启功书画留影册》

## 相關影視作品

  - 2015年中國大陸電影《[启功 (电影)](../Page/启功_\(电影\).md "wikilink")》\[3\]

## 参考文献

### 引用

### 来源

  - 《啟功年表》，2005年7月2日《海南日報》
  - [中央文史研究馆](../Page/中央文史研究馆.md "wikilink")
    编：《馆员传略》（[中华书局](../Page/中华书局.md "wikilink")）
  - 《启功口述历史》（[北京师范大学出版社](../Page/北京师范大学出版社.md "wikilink")）

## 外部連結

  - [离我们远去的大师](http://www.hao565.cn/content.asp?id=506)
  - [启功书画网](http://www.qgsh.net)
  - [中国书法家协会](http://www.ccagov.com.cn/)

{{-}}

{{Authority control}

[Category:北京人](../Category/北京人.md "wikilink")
[Category:愛新覺羅氏](../Category/愛新覺羅氏.md "wikilink")
[Category:中国书法家协会会员](../Category/中国书法家协会会员.md "wikilink")
[Category:中国书法家协会主席](../Category/中国书法家协会主席.md "wikilink")
[Category:中華人民共和國書法家](../Category/中華人民共和國書法家.md "wikilink")
[Category:中华人民共和国画家](../Category/中华人民共和国画家.md "wikilink")
[Category:輔仁大學教授](../Category/輔仁大學教授.md "wikilink")
[Category:中央文史研究馆馆长](../Category/中央文史研究馆馆长.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[Category:清朝宗室后裔](../Category/清朝宗室后裔.md "wikilink")
[Category:北京师范大学校友](../Category/北京师范大学校友.md "wikilink")
[Category:鉴定家](../Category/鉴定家.md "wikilink")
[Category:右派分子](../Category/右派分子.md "wikilink")
[Category:中国艺术史学家](../Category/中国艺术史学家.md "wikilink")
[Category:葬于北京市八宝山革命公墓](../Category/葬于北京市八宝山革命公墓.md "wikilink")
[Category:中国书法家协会副主席](../Category/中国书法家协会副主席.md "wikilink")

1.  《爱新觉罗宗譜》
2.
3.