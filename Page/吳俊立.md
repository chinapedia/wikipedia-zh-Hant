**吳俊立**，[臺灣政治人物](../Page/臺灣.md "wikilink")，[中國國民黨籍](../Page/中國國民黨.md "wikilink")。

## 經歷

高雄應用科技大學畢業\[1\]。從商轉政，參選議員，一參選議員就獲得第一高票\[2\]，榮耀同時當選副議長，擔任副議長期間，謹守分際協助議長折衝協調解決各種棘手問題，深獲議員們肯定；所以，當前任議長病故，在補選時獲得支持順利當選議長，才三十七歲，從政資歷不到一年，他就創下全國最年輕議長的紀錄。

\[3\]2015年3月3日，由最高法院判決貪污罪無罪定讞。\[4\]

2016年2月3日，警方調查發現花東玉石博物館用旗下花蓮、台東縣關係企業員工、親朋好友及虛擬身分證字號合計三百五十組（其中虛擬字號就有六十四組）再使用電腦掃票程式，一年來已經成功搶票七萬八千多張，專供中國大陸籍遊客使用，希望換取旅行團把該館排入行程，吸引陸客登門消費。\[5\]經向藝品店求證，發現報導與業者所稱事實有很大出入：業者所訂購的車票，是政府為因應陸客為避開蘇花公路，所開放花蓮新城至蘇澳的「區間車」與「莒光號」列車，在新聞稿中有鐵路警察所呈現的車票為證，並不是花東縣民返鄉買不到的自強號、太魯閣號、普悠瑪號車票。

## 政治

### 2005年台東縣長選舉

| 2005年[臺東縣縣長選舉結果](../Page/臺東縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### [2012年中華民國立法委員選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")

| 2012年[臺東縣選舉區](../Page/臺東縣選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| --------------------------------------------------------------------------------- |
| 號次                                                                                |
| 1                                                                                 |
| 2                                                                                 |
| 3                                                                                 |
| 4                                                                                 |
| 5                                                                                 |
| **選舉人數**                                                                          |
| **投票數**                                                                           |
| **有效票**                                                                           |
| **無效票**                                                                           |
| **投票率**                                                                           |

## 參考文獻

|- |colspan="3"
style="text-align:center;"|**[Seal_of_Taitung_County.svg](https://zh.wikipedia.org/wiki/File:Seal_of_Taitung_County.svg "fig:Seal_of_Taitung_County.svg")
[台東縣議會](../Page/台東縣議會.md "wikilink")** |-     |- |colspan="3"
style="text-align:center;"|**[Seal_of_Taitung_County.svg](https://zh.wikipedia.org/wiki/File:Seal_of_Taitung_County.svg "fig:Seal_of_Taitung_County.svg")[台東縣政府](../Page/台東縣政府.md "wikilink")**
|-

[Category:臺東縣縣長](../Category/臺東縣縣長.md "wikilink")
[Category:臺東縣議長](../Category/臺東縣議長.md "wikilink")
[Category:臺東縣議員](../Category/臺東縣議員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:臺東人](../Category/臺東人.md "wikilink")
[J俊](../Category/吳姓.md "wikilink")
[Category:臺灣貪污犯](../Category/臺灣貪污犯.md "wikilink")
[Category:任內離職的臺灣地方首長](../Category/任內離職的臺灣地方首長.md "wikilink")

1.
2.
3.  [吳俊立賄選案
    更一審仍判二年](http://www.libertytimes.com.tw/2009/new/aug/5/today-p5.htm)

4.   台灣英文新聞|last=台灣英文新聞|newspaper=台灣英文新聞|accessdate=2018-12-02}}
5.