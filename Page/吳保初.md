**吳保初**（），[字](../Page/表字.md "wikilink")**彥復**，又字**君遂**，[號](../Page/號.md "wikilink")**嬰公**、**北山**，[安徽](../Page/安徽.md "wikilink")[廬江人](../Page/廬江.md "wikilink")，曾任[主事](../Page/主事.md "wikilink")，支持[戊戌維新](../Page/戊戌維新.md "wikilink")，[維新四公子之一](../Page/維新四公子.md "wikilink")。女兒[吳若男](../Page/吳若男.md "wikilink")，女婿[章士釗](../Page/章士釗.md "wikilink")。

父親[吳長慶為](../Page/吳長慶.md "wikilink")[淮軍名將](../Page/淮軍.md "wikilink")，曾任[浙江](../Page/浙江.md "wikilink")[提督](../Page/提督.md "wikilink")。

## 簡介

光绪十年（1884），长庆病重，保初驰往待疾，事闻于[朝廷](../Page/朝廷.md "wikilink")，特旨褒嘉，且授[主事](../Page/主事.md "wikilink")。服丧期满入[京師](../Page/京師.md "wikilink")，分[兵部学习](../Page/兵部.md "wikilink")。光绪二十一年（1895）补授[刑部山东司](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")，旋派充贵州司主稿、秋审处帮办，任职期间不畏权势，力平董裕氏冤狱。

光绪二十三年（1897）鉴于[甲午戰爭之败](../Page/甲午戰爭.md "wikilink")，保初乃上《陈时事疏》，直“以亡国之说，告之于皇上”。冀其“怵危亡”而“谋富强”，被[刑部尚书](../Page/刑部尚书.md "wikilink")[刚毅压下未报](../Page/刚毅.md "wikilink")，保初乃愤然引疾南归，然而他的奏疏在上海报纸登了出来，以致出了大名。

他未去职以前早有[维新思想](../Page/维新.md "wikilink")，[梁启超初入京才](../Page/梁启超.md "wikilink")24岁，保初即视为“奇士”，并向文渊阁大学士[孙家鼐力荐](../Page/孙家鼐.md "wikilink")，梁因此得入自强书局。[戊戌变法前后](../Page/戊戌变法.md "wikilink")，他著文痛论阻挠新法之害。变法失败后，[谭嗣同等](../Page/谭嗣同.md "wikilink")[戊戌六君子被](../Page/戊戌六君子.md "wikilink")[斬殺](../Page/斬殺.md "wikilink")，他时已南归，又写《哭六君子》诗并“为亡人讼冤”。

辛丑以后他又入京上疏归政权给[光绪帝](../Page/光绪帝.md "wikilink")，他同时写信给[袁世凯劝其](../Page/袁世凯.md "wikilink")“行[桓文之事](../Page/尊王攘夷.md "wikilink")”，主旨在于支持[光绪帝实行变法](../Page/光绪帝.md "wikilink")。袁曾在长庆[幕府](../Page/幕府.md "wikilink")，与保初有兄弟之交，袁显贵以后，保初勉以“君王神武丁多故，好建奇功答圣时”，但袁不采纳，赠以重金，保初亦斥而不受。

光绪三十一年（1905）东渡[日本](../Page/日本.md "wikilink")，舟过[玄海滩时](../Page/玄海滩.md "wikilink")，有“舟人那识伤心地，遥指前程是马关”（《乙巳游日本绝句》）之句，赋诗寄慨，忧伤国事。他又在[苏报案中](../Page/苏报案.md "wikilink")，保护过入狱革命派的[章太炎](../Page/章太炎.md "wikilink")，于是[革命党也认为朋友](../Page/革命党.md "wikilink")。他既维新又革命，还和先维新后[保皇黨的](../Page/保皇黨.md "wikilink")[康南海](../Page/康南海.md "wikilink")、[梁啟超保持联系](../Page/梁啟超.md "wikilink")。等到党禁和缓，保初始悄然回[天津](../Page/天津.md "wikilink")，不久即患[中风](../Page/中风.md "wikilink")，手足偏废，对于国事就少过问了。

宣统三年春南归[上海](../Page/上海.md "wikilink")，卧床两载，民国2年（1913）春病逝，葬沪[静安寺侧](../Page/静安寺.md "wikilink")。

## 外部連結

  - [从沈曾植为吴保初墓志书丹说开去](http://www.guoxue.com/master/shenzengzhi/szz08.htm)，戴家妙
  - [《乙巳游日本绝句》
    鉴赏](https://web.archive.org/web/20071019113139/http://www.zhsc.net/Article/scjs/sgjs/200504/20050424164443.html)

[W吳](../Category/戊戌变法.md "wikilink") [W吳](../Category/廬江人.md "wikilink")
[B保](../Category/吳姓.md "wikilink")