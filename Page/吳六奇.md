**吳六奇**，字**鑒伯**，別字**葛如**，[廣東省](../Page/廣東省.md "wikilink")[潮州](../Page/潮州.md "wikilink")[豐順縣人](../Page/豐順縣.md "wikilink")。

家世[廣東](../Page/廣東.md "wikilink")[海陽人](../Page/海陽.md "wikilink")，因好賭，家道中落，明末[行乞於](../Page/行乞.md "wikilink")[吳越之間](../Page/吳越.md "wikilink")。後投靠[南明桂王](../Page/南明.md "wikilink")[朱由榔](../Page/朱由榔.md "wikilink")。

清兵攻克[潮州時投降](../Page/潮州.md "wikilink")[平南王](../Page/平南王.md "wikilink")。拜潮州總兵，以功升為廣東水陸師提督。[王士禎](../Page/王士禎.md "wikilink")《吳順恪六奇別傳》記載六奇微時，[海寧](../Page/海寧.md "wikilink")[孝廉](../Page/孝廉.md "wikilink")[查繼佐](../Page/查繼佐.md "wikilink")（查史璜）曾接濟過他，贈以「海內奇男子」五個字。

康熙二年（1663年），[莊廷鑨明史案起](../Page/莊廷鑨明史案.md "wikilink")，繼佐名列參校中，幾招不測，六奇力為奏辨，終免一死。著有《忠孝堂文集》。王士禛《[香祖笔记](../Page/香祖笔记.md "wikilink")》卷七載：“六奇後卒官，贈[少師](../Page/少師.md "wikilink")，兼太子太師，諡**順恪**。”榮祿大夫，以別字行\[1\]。

## 筆記小說上的記載

### 聊齋誌異

《[聊齋誌異](../Page/聊齋誌異.md "wikilink")》〈大力將軍〉篇稱吳六奇為吳六一：『後十餘年，查猶子令於閩，有吳將軍六一者，忽來通謁\[2\]。』

根據此故事，查繼佐在一廟內看見吳六奇單手可以升起廟內的大鍾，並取出藏在鍾內的剩飯，驚為奇人，認為他在這個亂世應該報效社會，使其異能得以發揮。爾後，中國改朝換代，而吳六奇亦在新的滿清政府當官，衣錦榮歸後向查繼佐道謝。

### 鹿鼎記

《[鹿鼎記](../Page/鹿鼎記.md "wikilink")》指吳六奇為天地會人，純為虛構。

### 觚賸

《[觚賸](../Page/觚賸.md "wikilink")》〈雪遘〉篇稱吳六奇當上水陸提督後，款待查繼佐，並送一座名為英石峰的奇石給查繼佐\[3\]，此石改名為皱云峰，後世譽為[江南三大名石](../Page/江南三大名石.md "wikilink")\[4\]。

## 附註

## 參看

  - 《[康熙大帝](../Page/康熙大帝.md "wikilink")》：以吳六奇為原形的[吳六一在此歷史小說與電視劇出現](../Page/吳六一.md "wikilink")，並且擒住鰲拜，人物劇情純屬虛構。史實上吳六奇早已去世，而且也非九門提督。

## 外部連結

  - [丰顺县人文景观②——丰良镇少师第（大衙）](http://bbs.meizhou.cn/thread-51123-1-1.html)

[鹿](../Page/category:金庸筆下真實人物.md "wikilink")

[Category:清朝廣東陸路提督](../Category/清朝廣東陸路提督.md "wikilink")
[Category:清朝廣東水師提督](../Category/清朝廣東水師提督.md "wikilink")
[Category:鹿鼎記角色](../Category/鹿鼎記角色.md "wikilink")
[Category:豐順人](../Category/豐順人.md "wikilink")
[L六](../Category/吳姓.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝三孤](../Category/清朝三孤.md "wikilink")
[Category:諡順恪](../Category/諡順恪.md "wikilink")
[Category:潮州鎮總兵](../Category/潮州鎮總兵.md "wikilink")

1.  <http://cul.china.com.cn/lishi/2012-09/23/content_5359281.htm>
2.  [《聊齋志異》卷六
    大力將軍](http://yipip.com/yipbook/book/1sbce1ev/chapter/33331/)
3.  [《觚剩（續編）》正文·卷七·粵觚上](http://big5.dushu.com/showbook/101490/1048075.html)

4.  [江南三大名石](http://www.gss.org.cn/news/shangshiwenhua/2009/10/091041737211365.html)