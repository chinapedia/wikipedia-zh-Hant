**吳安琪**（），現任TVBS新聞台主播&假日《12、13午間新聞》主播。

## 學歷

  - [美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[雪城大學廣電碩士](../Page/雪城大學.md "wikilink")(SYRACUSE
    U. TV、RADIO & FILM NEWHOUSE SCHOOL.)
  - [台灣大學農業經濟學系](../Page/台灣大學.md "wikilink")

## 經歷

  - [台北市政府市場管理處](../Page/台北市政府.md "wikilink")
  - 華衛電視台記者
  - [東森新聞台](../Page/東森新聞台.md "wikilink")[記者](../Page/記者.md "wikilink")、[主播](../Page/主播.md "wikilink")

## 特殊經歷

  - 音樂伴唱帶〈[孤女的願望](../Page/孤女的願望.md "wikilink")〉女主角
  - 第一屆網路票選主播金鐘獎：最佳風雲女主持人

## 主播過或主持過的節目

|             |                                              |                                                                  |        |
| ----------- | -------------------------------------------- | ---------------------------------------------------------------- | ------ |
| **時間**      | **頻道**                                       | **節目**                                                           | **備註** |
| 時間待查        | [TVBS](../Page/TVBS.md "wikilink")           | 《[無線午報](../Page/無線午報.md "wikilink")》                             |        |
| 時間待查        | [TVBS-N](../Page/TVBS-N.md "wikilink")       | 《早上7點整點新聞》                                                       |        |
| 時間待查        | [TVBS-N](../Page/TVBS-N.md "wikilink")       | 周末《[Good morning 魔力新聞](../Page/Good_morning_魔力新聞.md "wikilink")》 |        |
| 時間待查-至今     | [TVBS-N](../Page/TVBS-N.md "wikilink")       | 《12、13午間新聞》                                                      |        |
| 時間待查—至今     | [TVBS-NEWS](../Page/TVBS-NEWS.md "wikilink") | 《整點新聞》                                                           |        |
| 2004年—2006年 | [TVBS](../Page/TVBS.md "wikilink")           | 《[一步一腳印，發現新台灣](../Page/一步一腳印，發現新台灣.md "wikilink")》               | 代班主持人  |

## 參考資料

## 外部連結

  - TVBS官方網站[吳安琪](http://www.tvbs.com.tw/anchor/wu-an-chi)簡介

[Category:雪城大學校友](../Category/雪城大學校友.md "wikilink")
[W](../Category/國立臺灣大學校友.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:台灣電視主播](../Category/台灣電視主播.md "wikilink")
[Category:台灣新聞節目主持人](../Category/台灣新聞節目主持人.md "wikilink")
[Category:東森主播](../Category/東森主播.md "wikilink")
[Category:TVBS主播](../Category/TVBS主播.md "wikilink")