**吳成典**（），[新黨](../Page/新黨.md "wikilink")[金門縣政治人物](../Page/金門縣.md "wikilink")，曾任[金門縣副縣長](../Page/金門縣縣長.md "wikilink")、[立法委員](../Page/立法委員.md "wikilink")、[國大代表](../Page/國大代表.md "wikilink")，[2009年金門縣長選舉落敗後擔任](../Page/2009年中華民國縣市長暨縣市議員選舉.md "wikilink")[新黨副秘書長](../Page/新黨.md "wikilink")。原籍金門縣[烈嶼鄉上庫村](../Page/烈嶼鄉.md "wikilink")，其堂弟為偶像團體[飛輪海成員](../Page/飛輪海.md "wikilink")[吳尊](../Page/吳尊.md "wikilink")\[1\]\[2\]。

## 生平

成長在[金門](../Page/金門.md "wikilink")，[大學後前往](../Page/大學.md "wikilink")[美國攻讀](../Page/美國.md "wikilink")[化學](../Page/化學.md "wikilink")[博士](../Page/博士.md "wikilink")，返國後擔任[工研院材料所主任](../Page/工研院.md "wikilink")，引進並參與台灣第一片[CD-R光碟的研發](../Page/CD-R光碟.md "wikilink")。1992年返回故鄉參選[國民大會代表](../Page/中華民國國民大會.md "wikilink")，踏上從政之路。

[2001年中華民國立法委員選舉擊敗尋求連任的國民黨立委](../Page/2001年中華民國立法委員選舉.md "wikilink")[陳清寶](../Page/陳清寶.md "wikilink")，而新黨的[李炷烽也順利入主縣府](../Page/李炷烽.md "wikilink")，吳成典曾短暫擔任其副縣長後就職立委；三年後連任成功，是[兩岸](../Page/兩岸.md "wikilink")[小三通的推手](../Page/小三通.md "wikilink")，這也是新黨於2002年至2008年的唯一一席立委。

[2008年中華民國立法委員選舉首度接受國新共推挑戰三連霸立委](../Page/2008年中華民國立法委員選舉.md "wikilink")，但以74票些微差距敗給曾任金湖鎮長、後競選縣長落敗的[陳福海](../Page/陳福海.md "wikilink")。同年受助理費案的影響遭[金門地檢署起訴求刑](../Page/金門地檢署.md "wikilink")15年，一、二審皆獲無罪，2012年三審確定無罪定讞。

在其連任失利後獲金門縣長李炷烽任命為縣府顧問；隔年9月19日，吳成典成立個人網路部落格，並正式宣布脫離國民黨、參選第五屆金門縣縣長選舉，但在金門傳統陳、李兩大宗族的夾擊下，最終吳成典以些微差距輸給國民黨提名、新黨出身的[李沃士](../Page/李沃士.md "wikilink")。縣長落選後，曾獲新黨任命為副秘書長。

2012年及2016年皆獲新黨徵召，挑戰回鍋[金門縣選舉區立委](../Page/金門縣選舉區.md "wikilink")，但分別敗於獲國民黨提名的前新黨金門縣議員[楊應雄及前國代](../Page/楊應雄.md "wikilink")[楊肅元之子](../Page/楊肅元.md "wikilink")、前金門縣觀光處長[楊鎮浯](../Page/楊鎮浯.md "wikilink")，未能重返國會。

2016年5月，獲金門縣長陳福海任命為副縣長，並於6月中旬正式就任。

## 選舉

[2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")

  - 總選舉人數：63,451
  - 總票數（投票率）：27,007（42.56%）
  - 有效票（比率）：26,564（98.36%）
  - 無效票（比率）：443（1.64%）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/陳福海.md" title="wikilink">陳福海</a></p></td>
<td></td>
<td><p>9,912</p></td>
<td><p>37.31%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/李沃士.md" title="wikilink">李沃士</a></p></td>
<td></td>
<td><p>5,274</p></td>
<td><p>19.85%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/高絃騰.md" title="wikilink">高絃騰</a></p></td>
<td><p><a href="../Page/公民黨.md" title="wikilink">公民黨</a></p></td>
<td><p>39</p></td>
<td><p>0.15%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/胡偉生.md" title="wikilink">胡偉生</a></p></td>
<td></td>
<td><p>1,070</p></td>
<td><p>4.03%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/唐惠霈.md" title="wikilink">唐惠霈</a></p></td>
<td></td>
<td><p>431</p></td>
<td><p>1.62%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><strong>吳成典</strong></p></td>
<td><p><span style="font-size:smaller;">（背書）</span></p></td>
<td><p>9,838</p></td>
<td><p>37.04%</p></td>
<td></td>
</tr>
</tbody>
</table>

[2009年中華民國縣市長暨縣市議員選舉](../Page/2009年中華民國縣市長暨縣市議員選舉.md "wikilink")
[Kinmen_magistrate_election_2009.png](https://zh.wikipedia.org/wiki/File:Kinmen_magistrate_election_2009.png "fig:Kinmen_magistrate_election_2009.png")

| 2009年[金門縣縣長選舉結果](../Page/金門縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| 4                                             |
| 5                                             |
| 6                                             |
| 7                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

[2012年中華民國立法委員選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")

  - 總選舉人數：81,783
  - 總票數（投票率）：38,756（47.39%）
  - 有效票（比率）：37,729（97.35%）
  - 無效票（比率）：1,027（2.65%）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong><a href="../Page/陳福海.md" title="wikilink">陳福海</a></strong></p></td>
<td></td>
<td><p>12,128</p></td>
<td><p>32.15%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong>吳成典</strong></p></td>
<td></td>
<td><p>10,678</p></td>
<td><p>28.30%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/許乃權.md" title="wikilink">許乃權</a></p></td>
<td></td>
<td><p>2,260</p></td>
<td><p>5.99%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/高絃騰.md" title="wikilink">高絃騰</a></p></td>
<td><p><a href="../Page/健保免費連線.md" title="wikilink">健保免費連線</a></p></td>
<td><p>228</p></td>
<td><p>0.60%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/楊應雄.md" title="wikilink">楊應雄</a></p></td>
<td></td>
<td><p>12,435</p></td>
<td><p>32.96%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
</tbody>
</table>

[2016年中華民國立法委員選舉](../Page/2016年中華民國立法委員選舉.md "wikilink")

  - 選舉人數：109,478
  - 投票數（投票率）：37,014（33.81%）
  - 有效票（比率）：36,267（97.98%）
  - 無效票（比率）：747（2.02%）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>性別</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/洪志恒.md" title="wikilink">洪志-{恒}-</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>647</p></td>
<td><p>1.78%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/陳滄江.md" title="wikilink">陳滄江</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>8,367</p></td>
<td><p>23.07%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/高丹樺.md" title="wikilink">高丹樺</a></p></td>
<td><p>女</p></td>
<td></td>
<td><p>666</p></td>
<td><p>1.84%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/張中法.md" title="wikilink">張中法</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>670</p></td>
<td><p>1.85%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/楊鎮浯.md" title="wikilink">楊鎮浯</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>16,350</p></td>
<td><p>45.08%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/陳仲立.md" title="wikilink">陳仲立</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>49</p></td>
<td><p>0.14%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/陳德輝.md" title="wikilink">陳德輝</a></p></td>
<td><p>男</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_square_yellow_bg.png" title="fig:Green_square_yellow_bg.png">Green_square_yellow_bg.png</a> <a href="../Page/台灣工黨.md" title="wikilink">台灣工黨</a></p></td>
<td><p>113</p></td>
<td><p>0.31%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><strong>吳成典</strong></p></td>
<td><p>男</p></td>
<td></td>
<td><p>9,405</p></td>
<td><p>25.93%</p></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [吳成典官方網站](https://web.archive.org/web/20161014132120/http://www.kinmenupgrade.com/)
  - [吳成典個人臉書](https://www.facebook.com/home.php#!/profile.php?id=100000369140193&sk=wall)
  - [吳成典臉書粉絲專頁](https://www.facebook.com/home.php#!/pages/%E5%90%B3%E6%88%90%E5%85%B8/164513070265007)
  - [吳成典個人部落格](http://blog.yam.com/changekm)
  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00026&stage=6)
  - [線上大國民 Mighty People Online - 立法委員資料 –
    吳成典](https://web.archive.org/web/20071224151725/http://www.mightypeople.org/congress/congress.php?id=356)

## 參考資料

<div class="references-small">

<references />

</div>

  - [吳成典個人部落格](http://blog.yam.com/changekm)

[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:凱斯西儲大學校友](../Category/凱斯西儲大學校友.md "wikilink")
[Category:新黨黨員](../Category/新黨黨員.md "wikilink")
[Category:金門縣副縣長](../Category/金門縣副縣長.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:金門人](../Category/金門人.md "wikilink")
[C](../Category/吳姓.md "wikilink")

1.
2.