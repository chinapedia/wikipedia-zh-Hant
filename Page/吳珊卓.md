**吳珊卓**（，），又譯**桑得拉·奧**，生於[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")，是一位[韓國裔](../Page/韓國.md "wikilink")[加拿大籍的](../Page/加拿大.md "wikilink")[女演員](../Page/演員.md "wikilink")。

## 生平

曾在1994以《[雙喜](../Page/双喜_\(1994年电影\).md "wikilink")》，1998以《[昨夜](../Page/昨夜.md "wikilink")》，兩度獲得[加拿大](../Page/加拿大.md "wikilink")[吉尼獎影后](../Page/吉尼獎.md "wikilink")，也曾獲[艾美獎提名](../Page/艾美獎.md "wikilink")，並獲得[金球獎](../Page/金球獎.md "wikilink")。她知名的角色除上述兩部封后之作外，還有在影集《[實習醫生](../Page/實習醫生_\(電視劇\).md "wikilink")》中的克莉絲汀娜·楊（Cristina
Yang），以及在《[托斯卡尼艷陽下](../Page/托斯卡尼艷陽下.md "wikilink")》、《[寻找新方向](../Page/酒佬日記.md "wikilink")》等電影中的角色。在2005年，吳珊卓被《[時人](../Page/人物_\(杂志\).md "wikilink")》杂志選為世界上最美麗的50人之一
[1](http://people.aol.com/people/article/0,26334,1046248_1054061,00.html)。

吳珊卓曾與獨立製片導演[亞歷山大·潘恩於](../Page/亞歷山大·潘恩.md "wikilink")2003結婚， 於2006年離異。

## 主要作品

  - 《[殺死伊芙](../Page/殺死伊芙.md "wikilink")》（*Killing Eve*，2018年）-
    飾演：伊芙·波拉斯特里（Eve Polastri）
  - 《[蕾蒙娜和姐姐](../Page/蕾蒙娜和姐姐.md "wikilink")》（''Ramona and Beezus
    ''，[2010年](../Page/2010年电影.md "wikilink")）- 飾演：Mrs. Meacham
  - 《[心顫頻率](../Page/心顫頻率.md "wikilink")》（*The Night
    Listener*，[2006年](../Page/2006年电影.md "wikilink")）- 飾演：Anna
  - 《[愛之針](../Page/愛之針.md "wikilink")》（*3
    Needles*，[2005年](../Page/2005年电影.md "wikilink")）- 飾演：Mary
  - 《蛋糕》（*Cake*，[2005年](../Page/2005年电影.md "wikilink")）- 飾演：Lulu
  - 《[實習醫生](../Page/實習醫生_\(電視劇\).md "wikilink")》（電視影集，2005年）-
    飾演：克莉絲汀娜·楊（Cristina Yang）
  - 《[水果硬糖](../Page/水果硬糖.md "wikilink")》（*Hard
    Candy*，[2005年](../Page/2005年电影.md "wikilink")）- 飾演：Judy Tokuda
  - 《[花木蘭2](../Page/花木蘭2.md "wikilink")》（[2004年](../Page/2004年电影.md "wikilink")）-
    配音：Ting Ting
  - 《[尋找新方向](../Page/酒佬日記.md "wikilink")》（[2004年](../Page/2004年电影.md "wikilink")）-
    飾演：Stephanie
  - 《*Rick*》（[2003年](../Page/2003年电影.md "wikilink")）- 飾演：Michelle
  - 《[托斯卡尼艷陽下](../Page/托斯卡尼艷陽下.md "wikilink")（*Under the Tuscan
    Sun*，[2003年](../Page/2003年电影.md "wikilink")）- 飾演：Patti
  - 《[福祿壽](../Page/福祿壽.md "wikilink")》（[2002年](../Page/2002年电影.md "wikilink")）-
    飾演：Kin Ho Lum
  - 《[大謊言家](../Page/大謊言家.md "wikilink")》（*Big Fat
    Liar*，[2002年](../Page/2002年电影.md "wikilink")）- 飾演：Mrs. Phyllis
    Caldwell
  - 《[麻雀變公主](../Page/麻雀變公主.md "wikilink")》（[2001年](../Page/2001年电影.md "wikilink")）-
    飾演：Vice Principal Gupta
  - 《[昨夜](../Page/昨夜.md "wikilink")》（''Last Night
    ''，[1998年](../Page/1998年电影.md "wikilink")）- 飾演：Sandra
  - 《[豆豆秀](../Page/戇豆先生_\(電影\).md "wikilink")》（*Bean*，[1997年](../Page/1997年电影.md "wikilink")）-
    飾演：伯尼斯·希米爾（Bernice Schimmel）
  - 《[雙喜](../Page/双喜_\(1994年电影\).md "wikilink")》（*Double
    Happiness*，[1994年](../Page/1994年电影.md "wikilink")）- 飾演：Jade Li
    (導演為港裔加籍女導沈小艾)

她也在許多[電視影集中客串演出](../Page/影集.md "wikilink")，例如《[六呎風雲](../Page/六呎風雲.md "wikilink")》等。

## 獲得獎項

**[金球獎](../Page/金球獎.md "wikilink")**

  - 最佳電視女配角︰吳珊卓（2006年）
  - 最佳電視女主角（劇情）︰吳珊卓（2019年）

**[螢幕演員公會獎](../Page/螢幕演員公會獎.md "wikilink")**（**Screen Actors Guild**）

  - 電視影集最佳女演員：吳珊卓（2006年）

## 外部連結

  -
[O](../Category/加拿大电影演员.md "wikilink")
[O](../Category/加拿大電視演員.md "wikilink")
[O](../Category/韓裔加拿大人.md "wikilink")
[O](../Category/安大略省人.md "wikilink")
[吳珊卓](../Category/演員小作品.md "wikilink")
[Category:美国演员工会奖最佳电影群体演出奖得主](../Category/美国演员工会奖最佳电影群体演出奖得主.md "wikilink")
[Category:金球奖获得者](../Category/金球奖获得者.md "wikilink")
[Category:美國演員工會獎得主](../Category/美國演員工會獎得主.md "wikilink")