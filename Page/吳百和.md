**吳百和**（），[台灣職業足球員](../Page/台灣.md "wikilink")，職司[中場](../Page/中場.md "wikilink")，曾為[中華台北足球代表隊及](../Page/中華台北足球代表隊.md "wikilink")[中華台北奧運足球隊成員](../Page/中華台北奧運足球隊.md "wikilink")。

## 球員生涯

吳百和高中時就讀於台灣高中足球名校[北門高中](../Page/北門高中.md "wikilink")，更曾在[2006年台灣高中足球聯賽獲得](../Page/2006年台灣高中足球聯賽.md "wikilink")[金靴獎](../Page/金靴獎.md "wikilink")\[1\]。

## 經歷

  - [台北縣清水高中](../Page/台北縣清水高中.md "wikilink")
  - [北門高中](../Page/北門高中.md "wikilink")
  - [輔仁大學](../Page/輔仁大學.md "wikilink")

## 榮譽

### 個人榮譽

  - [2006年台灣高中足球聯賽](../Page/2006年台灣高中足球聯賽.md "wikilink")：[金靴獎](../Page/金靴獎.md "wikilink")
  - [2013年聯發科技全國城市足球聯賽](../Page/2013年聯發科技全國城市足球聯賽.md "wikilink") ：最佳球員獎

## 外部連結

  - [吳百和](http://www.national-football-teams.com/v2/player.php?id=24634)在national-football-teams.com的球員資料

[P](../Category/吳姓.md "wikilink")
[Category:台灣足球運動員](../Category/台灣足球運動員.md "wikilink")
[W](../Category/輔仁大學校友.md "wikilink")
[Category:2013年東亞盃中華台北男子足球代表隊選手](../Category/2013年東亞盃中華台北男子足球代表隊選手.md "wikilink")

1.