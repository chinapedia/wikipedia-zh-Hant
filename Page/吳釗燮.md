**吳釗燮**\[1\]（），生於[臺灣](../Page/臺灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[大城鄉](../Page/大城鄉.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")、[学者和](../Page/学者.md "wikilink")[外交官](../Page/外交官.md "wikilink")。現任[中華民國外交部部長](../Page/中華民國外交部.md "wikilink")，曾任[中華民國總統府秘書長](../Page/中華民國總統府秘書長.md "wikilink")、[中華民國總統府](../Page/中華民國總統府.md "wikilink")[副秘書長](../Page/秘書長.md "wikilink")、[國家安全會議秘書長](../Page/國家安全會議秘書長.md "wikilink")、[民主進步黨秘書長](../Page/民主進步黨秘書長.md "wikilink")、[行政院大陸委員會主任委員](../Page/行政院大陸委員會.md "wikilink")、[駐美代表](../Page/中華民國駐美國大使列表.md "wikilink")（[代表级](../Page/代表.md "wikilink")），為首任非[中國國民黨籍的駐美代表](../Page/中國國民黨.md "wikilink")。

## 學歷

吳釗燮曾就讀於[臺灣省立臺中第一高級中學](../Page/臺灣省立臺中第一高級中學.md "wikilink")，1978年畢業於[國立政治大學](../Page/國立政治大學.md "wikilink")[政治學系](../Page/政治學系.md "wikilink")，1982年取得美國[密蘇里大學](../Page/密蘇里大學.md "wikilink")[政治學](../Page/政治學.md "wikilink")[碩士](../Page/碩士.md "wikilink")，1989年取得[俄亥俄州立大學政治學](../Page/俄亥俄州立大學.md "wikilink")[博士](../Page/博士.md "wikilink")。

## 經歷

### 學歷

吳釗燮從省立臺中一中、國立政治大學政治學系(1978年)畢業後，於1982年取得美國密蘇里大學政治學碩士學位，1989年取得美國俄亥俄州立大學政治學博士學位。之後吳釗燮即返回台灣，在[國立政治大學國際關係研究中心擔任專任研究員](../Page/國立政治大學國際關係研究中心.md "wikilink")，也在國立政治大學選舉研究中心兼任副研究員、研究員及政治系擔任兼任副教授、教授。1999年至2002年，他在國立政治大學國際關係研究中心担任副主任。

### 總统府副秘書長

2002年，獲[陳水扁主政下的執政黨政府任名為](../Page/陳水扁政府.md "wikilink")[總统府副秘書長](../Page/中華民國總统府.md "wikilink")。2002年7月23日，諾魯與中華民國中止外交關係後，總统府副秘書長吳釗燮曾説：「我們應該要用**辦喜事**的心情面對這件事。」

2004年5月20日，吳釗燮卸下總统府副秘書長的職務，被重新任命為行政院[陸委會主任委員](../Page/大陸委員會.md "wikilink")。

### 駐美代表

2004年7月10日，吳釗燮轉任中華民國駐美國代表。

2007年4月，吳釗燮出任[駐美代表](../Page/中華民國駐美國大使列表.md "wikilink")，成為中華民國首位有[民主進步黨籍身份的駐美代表](../Page/民主進步黨.md "wikilink")。在吳蒞任後不久，即在[雙橡園創下一晚同時宴請](../Page/雙橡園.md "wikilink")[馬紹爾](../Page/馬紹爾.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[吉里巴斯和](../Page/吉里巴斯.md "wikilink")[諾魯等四位](../Page/諾魯.md "wikilink")[大洋洲國家總統的紀錄](../Page/大洋洲.md "wikilink")\[2\]，其後他也曾在雙橡園宴請[薩爾瓦多總統](../Page/薩爾瓦多總統.md "wikilink")[埃利亞斯·安東尼奧·薩卡及隨行要員](../Page/埃利亞斯·安東尼奧·薩卡.md "wikilink")。吳釗燮擔任駐美代表期間，在美國公開演講超過九十次，接受媒體專訪也近九十次。

2008年8月4日，吳釗燮從駐美代表處離任，返回政大國際關係研究中心擔任研究員。

### 民進黨政策委員會執行長

2012年[蘇貞昌出任民進黨主席之後](../Page/蘇貞昌.md "wikilink")，吳釗燮獲邀出任民進黨政策委員會執行長，同年底兼任民主進步黨的駐美代表。

2012年10月30日，吳釗燮承認過去民進黨對於[轉型正義處理方式讓支持者喪失信心](../Page/轉型正義.md "wikilink")，所以現在必須分門別類去處理，糾正過去的錯誤\[3\]。

### 民進黨秘書長

2014年[蔡英文回任民進黨主席之後](../Page/蔡英文.md "wikilink")，同年5月28日，吳釗燮獲邀擔任民主進步黨秘書長，仍兼任民主進步黨的駐美代表。

2015年6月3日，[TVBS新聞獨家報導](../Page/TVBS.md "wikilink")，蔡英文秘密拜訪[美國貿易代表署](../Page/美國貿易代表署.md "wikilink")，蔡英文與[立法委員](../Page/立法委員.md "wikilink")[蕭美琴看到記者時臉色難看](../Page/蕭美琴.md "wikilink")，吳釗燮一度伸手阻擋記者拍攝\[4\]。

### 國家安全會議秘書長

2016年5月20日，[\[蔡英文主政下的民進黨政府正式上任後](../Page/蔡英文政府.md "wikilink")，吳釗燮被任命為[國家安全會議秘書長](../Page/中華民國國家安全會議.md "wikilink")。

### 總統府秘書長

2017年5月18日，總統府發布人事異動，吳釗燮轉任[總統府秘書長](../Page/總統府秘書長.md "wikilink")。

### 外交部部長

2018年2月23日，[行政院發布人事異動](../Page/行政院.md "wikilink")，吳釗燮轉任[外交部部長](../Page/中華民國外交部.md "wikilink")。2月26日，吳釗燮就任為外交部部長。

2018年5月1日，[多明尼加宣佈與中華民國斷交](../Page/多明尼加.md "wikilink")\[5\]。5月24日，[布吉納法索也宣佈與中華民國斷交](../Page/布吉納法索.md "wikilink")，同日，吳釗燮向總統蔡英文口頭請辭，但經總統慰留後決定留任。

2018年8月21日，[薩爾瓦多宣佈與中華民國斷交](../Page/薩爾瓦多.md "wikilink")，是吳釗燮上任外交部部長半年來第三個斷交國，邦交國只剩17個\[6\]。

## 媒體評論

《[中國時報](../Page/中國時報.md "wikilink")》長年駐美的特派員[傅建中](../Page/傅建中.md "wikilink")，對吳釗燮評論頗佳。\[7\]
《[三立新聞台](../Page/三立新聞台.md "wikilink")》2019年1月14日接受[鄭弘儀等媒體當場問到關於](../Page/鄭弘儀.md "wikilink")[赵怡翔的事情](../Page/赵怡翔.md "wikilink")。吳釗燮力挺，稱希望給年輕人一個機會，但事實上過去擔任[民進黨政策會執行長時](../Page/民進黨.md "wikilink")，吳釗燮曾表示台美關係複雜，包含政治、經濟、文化等不同層面，也必須面對美國行政、國會部門以及僑界等，如果完全沒有這方面經驗，是相當令人擔心的。\[8\]

## 家庭

吳釗燮之父為吳溫培，叔父為[吳澧培](../Page/吳澧培.md "wikilink")（美國[銀行家](../Page/銀行家.md "wikilink")，知名[台獨大老](../Page/台獨.md "wikilink")），二叔[吴澍培是著名的](../Page/吴澍培.md "wikilink")[统派人物](../Page/统派.md "wikilink")，不過各兄弟成就不小，均在政府與商業有影響力。

2016年8月30日，其叔父吳澧培接受[台北之音訪問時批評](../Page/台北之音.md "wikilink")，[行政院院長](../Page/行政院院長.md "wikilink")[林全與](../Page/林全.md "wikilink")[外交部部長](../Page/中華民國外交部.md "wikilink")[李大維不適任](../Page/李大維_\(外交官\).md "wikilink")，總統[蔡英文不能再用林全](../Page/蔡英文.md "wikilink")，「林全不下台，蔡英文沒有明天」；他曾和侄子吳釗燮說李大維不適任外交部部長，吳釗燮卻為李大維辯護「除了我本人，他是最適合的人」，吳釗燮不認為民進黨有人適任外交部部長，之後他就和吳釗燮「完全不講話了」\[9\]。

台灣人公共事務會政策協調員[吳廸為其兒子](../Page/吳廸.md "wikilink")。

## 註解

## 参考资料

  - [華夏經緯網](http://big5.huaxia.com/tw/sdbd/rw/00207517.html)

  - [星期人物：吳釗燮
    承接重擔樂觀看未來](https://web.archive.org/web/20070330035954/http://www.libertytimes.com.tw/2004/new/jun/20/today-fo2.htm)
    ，《[自由時報](../Page/自由時報.md "wikilink")》，2004年6月20日。

|- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-       |- |colspan="3"
style="text-align:center;"|**[ROC_Office_of_the_President_Emblem.svg](https://zh.wikipedia.org/wiki/File:ROC_Office_of_the_President_Emblem.svg "fig:ROC_Office_of_the_President_Emblem.svg")[總統府](../Page/總統府.md "wikilink")**
|-    |-         |- |colspan="3" style="text-align:center;"| |-


[Z釗](../Category/吳姓.md "wikilink")
[Category:大城鄉人](../Category/大城鄉人.md "wikilink")
[Category:中華民國總統府秘書長](../Category/中華民國總統府秘書長.md "wikilink")
[Category:中華民國國家安全會議秘書長](../Category/中華民國國家安全會議秘書長.md "wikilink")
[Category:民主進步黨秘書長](../Category/民主進步黨秘書長.md "wikilink")
[Category:中華民國行政院大陸委員會主任委員](../Category/中華民國行政院大陸委員會主任委員.md "wikilink")
[Category:中華民國駐美國代表](../Category/中華民國駐美國代表.md "wikilink")
[Category:台灣政治學家](../Category/台灣政治學家.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:國立臺中第一高級中學校友](../Category/國立臺中第一高級中學校友.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:密蘇里大學校友](../Category/密蘇里大學校友.md "wikilink")
[Category:俄亥俄州立大學校友](../Category/俄亥俄州立大學校友.md "wikilink")

1.
2.  [頭一遭
    雙橡園夜宴四友邦元首](http://www.libertytimes.com.tw/2007/new/may/11/today-fo1.htm)

3.
4.
5.
6.
7.
8.
9.