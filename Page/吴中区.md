**吴中区**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[苏州市所辖的一个](../Page/苏州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")；其位于苏州南城，于2001年由原吴县市撤市分设成区，是[吴文化的发祥地](../Page/吴文化.md "wikilink"),同時也是《[孙子兵法](../Page/孙子兵法.md "wikilink")》的诞生地。全区陆地面积745平方公里，[太湖水域面积约](../Page/太湖.md "wikilink")1486平方公里。

## 历史

[秦始皇二十六年](../Page/秦始皇.md "wikilink")（公元前221年），置吴县，为[会稽郡治所](../Page/会稽郡.md "wikilink")。后历为[吴郡](../Page/吴郡.md "wikilink")、[吴州](../Page/吴州.md "wikilink")、[苏州](../Page/苏州_\(隋朝\).md "wikilink")、[苏州府治所](../Page/苏州府.md "wikilink")。1912年1月1日至1912年9月24日，为[江苏省](../Page/江蘇省_\(中華民國\).md "wikilink")[省会](../Page/省会.md "wikilink")。1928年城区划出设苏州市，1930年5月16日，撤苏州市，仍并入吴县。1949年，吴县划出城区建苏州市，市、县分治。同年，属[苏州专区](../Page/苏州专区.md "wikilink")。1970年，属[苏州地区](../Page/苏州地区.md "wikilink")。1983年3月，实行市管县新体制，撤销苏州地区行政公署，吴县隶属江苏省苏州市。1995年6月撤消吴县，设[吴县市](../Page/吴县市.md "wikilink")（[县级市](../Page/县级市.md "wikilink")），2000年12月撤消吴县市，改设苏州市**吴中区**和[相城区](../Page/相城区.md "wikilink")。\[1\]

## 地理位置

吴中区是中国历史文化名城[苏州的地理中心](../Page/苏州市.md "wikilink")，北与苏州古城、苏州工业园区、苏州高新区接壤，南临苏州[吴江区](../Page/吴江区.md "wikilink")，西衔太湖，与无锡市、[浙江省湖州市隔湖相望](../Page/浙江省.md "wikilink")。地理坐标为东经119°55´-120°54´,北纬30°56´-31°21´。全境东西长，南北宽。

## 行政区划

吴中区辖7个街道：[长桥街道](../Page/长桥街道_\(苏州市\).md "wikilink")、[城南街道](../Page/城南街道_\(苏州市\).md "wikilink")、[越溪街道](../Page/越溪街道.md "wikilink")、[横泾街道](../Page/横泾街道.md "wikilink")、[香山街道](../Page/香山街道_\(苏州市\).md "wikilink")、[郭巷街道](../Page/郭巷街道.md "wikilink")、[太湖街道](../Page/太湖街道_\(苏州市\).md "wikilink")，和7个镇：[甪直镇](../Page/甪直镇.md "wikilink")、[光福镇](../Page/光福镇.md "wikilink")、[木渎镇](../Page/木渎镇.md "wikilink")、[胥口镇](../Page/胥口镇_\(苏州市\).md "wikilink")、[东山镇](../Page/东山镇_\(苏州市\).md "wikilink")、[金庭镇](../Page/金庭镇.md "wikilink")（原西山镇）、[临湖镇](../Page/临湖镇_\(苏州市\).md "wikilink")（原[渡村镇和浦庄镇](../Page/渡村镇.md "wikilink")）。

## 旅遊景點

[甪直镇](../Page/甪直镇.md "wikilink")、[宝带桥](../Page/宝带桥.md "wikilink")、旺山生态农庄、淞南生态农庄、东村。

## 江苏吴中经济开发区

苏州吴中经济开发区\[2\]是1993年11月经江苏省政府批准的首批省级经济开发区之一。目前，规划控制面积123.91平方公里，并通过ISO14001环境管理体系、ISO9001质量管理体系认证，及获得省级电子信息产业基地、省级国际服务外包示范区、省级软件和信息服务产业园和中国可再生能源学会光伏产业示范基地授牌。当前已进驻中外企业3800家，分别来自20多个国家地区及国内10多个省市，形成了以电子信息、精密机械、生物医药、精细化工、新能源新材料等为特色的高新技术产业集聚。
2009年，实现地区生产总值190亿元，工业总产值510亿元，成为苏州南部先进制造业的集聚区和现代化的工业城区。2012年12月升级为国家级开发区。

## 友好城市

  - [里薩](../Page/里薩.md "wikilink")

  - [罗托路亚](../Page/罗托路亚.md "wikilink")

## 参考文献

## 外部链接

  - [苏州市吴中区政府网站](http://www.szwz.gov.cn/)

[category:中国国家级生态市区县](../Page/category:中国国家级生态市区县.md "wikilink")

[吴中区](../Category/吴中区.md "wikilink") [区](../Category/苏州区市.md "wikilink")
[苏州市](../Category/江苏市辖区.md "wikilink")

1.  [吴县的历史沿革](http://da.szwz.gov.cn/Show.asp?id=275)
2.  [吴中经济开发区](http://rightsite.asia/zh-hans/gongyeyuanqu/jiangsuwuzhongjingjikaifaqu/)