**吴伟业**（），[字](../Page/表字.md "wikilink")**駿公**，[號](../Page/號.md "wikilink")**梅村**，[祖籍](../Page/祖籍.md "wikilink")[直隸](../Page/南直隸.md "wikilink")[苏州府](../Page/苏州府.md "wikilink")[崑山縣](../Page/崑山縣.md "wikilink")（今[江苏省](../Page/江苏省.md "wikilink")[昆山市](../Page/昆山市.md "wikilink")），祖父始遷居[太倉州](../Page/太倉州.md "wikilink")（今江苏省苏州市[太仓市](../Page/太仓市.md "wikilink")），[明末](../Page/明.md "wikilink")[清初著名](../Page/清.md "wikilink")[诗人](../Page/诗人.md "wikilink")、政治人物，長於七言歌行，初學“長慶體”，後自成新吟，后人称之为“梅村體”。与[錢謙益](../Page/錢謙益.md "wikilink")、[龔鼎孳并稱為江左三大家](../Page/龔鼎孳.md "wikilink")。

## 生平

明神宗萬曆三十七年，五月二十日生於江南太倉的一書香家庭。幼時受學於同鄉[張溥](../Page/張溥.md "wikilink")，文名卓著，積極參加[復社](../Page/復社.md "wikilink")，与[钱谦益](../Page/钱谦益.md "wikilink")、[龚鼎孳并称为](../Page/龚鼎孳.md "wikilink")“江左三大家”，和當時的[陳維崧](../Page/陳維崧.md "wikilink")、[吳兆騫](../Page/吳兆騫.md "wikilink")、[嚴繩孫等才子有交往](../Page/嚴繩孫.md "wikilink")，且與著名歌妓[卞玉京有過一段姻緣](../Page/卞玉京.md "wikilink")。與[陳之遴爲兒女親家](../Page/陳之遴.md "wikilink")。

[崇禎四年](../Page/崇禎.md "wikilink")（1631年）參加[會試](../Page/會試.md "wikilink")，[溫體仁黨人誣陷](../Page/溫體仁.md "wikilink")[周延儒](../Page/周延儒.md "wikilink")，伟业被控與延儒勾結[作弊](../Page/作弊.md "wikilink")，[崇禎帝調閱試卷](../Page/崇禎帝.md "wikilink")，御覽之後，在吳的試卷上批“正大博雅，足式詭靡”，[殿試高中一甲第二名進士](../Page/殿試.md "wikilink")[榜眼](../Page/榜眼.md "wikilink")，授[翰林院](../Page/翰林院.md "wikilink")[編修](../Page/編修.md "wikilink")，“賜馳節還里門”，娶先室程氏。\[1\]
崇禎十一年（1638年），崇祯帝临场视学，觀看[皇子就學情況](../Page/皇子.md "wikilink")，亲问《[尚书](../Page/尚书.md "wikilink")》大义，讲毕，获赐“龙团月片，甘瓜脆李”。歷官[南京](../Page/南京.md "wikilink")[国子监司業](../Page/国子监.md "wikilink")、左[庶子](../Page/庶子.md "wikilink")。

崇禎十七年（1644年）[甲申之變](../Page/甲申之變.md "wikilink")，明朝滅亡，[清兵入關後](../Page/清兵入關.md "wikilink")，短暫出仕[弘光帝](../Page/弘光帝.md "wikilink")[朝廷](../Page/朝廷.md "wikilink")，任[詹事府少](../Page/詹事府.md "wikilink")[詹事](../Page/詹事.md "wikilink")，不久請假歸鄉。清[順治二年](../Page/順治.md "wikilink")（1645年），清軍南下，吳偉業攜家眷逃難，先後投奔到同宗繇青房及公益兄弟家，之後再逃往礬清湖親戚家，隐居不出。
[吳偉業.jpg](https://zh.wikipedia.org/wiki/File:吳偉業.jpg "fig:吳偉業.jpg")
[順治九年](../Page/順治.md "wikilink")（1652年），[兩江總督](../Page/兩江總督.md "wikilink")[馬國柱向朝廷舉薦任官](../Page/馬國柱.md "wikilink")，吳偉業得知後致信馬國柱，以身體有疾為由拒絕了。\[2\]但弘文院大學士[陳名夏](../Page/陳名夏.md "wikilink")、親家禮部尚書[陳之遴仍極力勸說出仕](../Page/陳之遴.md "wikilink")。十年（1653年），[吏部左侍郎](../Page/吏部左侍郎.md "wikilink")[孫承澤再次向順治帝舉薦](../Page/孫承澤.md "wikilink")，稱吳偉業為東南最有才能名士之一\[3\]，吳偉業表面上不置可否，實際表明了如果清政府給予高官則將會接受。\[4\]陳名夏、[馮銓](../Page/馮銓.md "wikilink")、[成克鞏](../Page/成克鞏.md "wikilink")、[張端](../Page/張端_\(崇禎進士\).md "wikilink")、[呂宮聯名舉薦了吳偉業](../Page/呂宮.md "wikilink")、[楊廷鑒](../Page/楊廷鑒.md "wikilink")、[宋之盛](../Page/宋之盛.md "wikilink")；十一年（1654年）年初，吳偉業接受了清朝[內翰林秘書院](../Page/內翰林秘書院.md "wikilink")[侍講官職](../Page/侍講.md "wikilink")。\[5\]五月，吳偉業出發前往北京，充任《大清太祖高皇帝聖訓》、《大清太宗文皇帝聖訓》纂修官。\[6\]順治十三年（1656年），官[國子監](../Page/國子監.md "wikilink")[祭酒](../Page/祭酒.md "wikilink")，次年，因母喪（实为[继母丧](../Page/继母.md "wikilink")）[丁憂歸鄉](../Page/丁憂.md "wikilink")。

康熙十年，十二月二十四日逝，葬予蘇州玄墓山之北。临殁顾言，“吾一生遭际，万事忧危，死后殓以[僧装](../Page/僧.md "wikilink")，葬我邓尉灵岩之侧，坟前立一圆石，题曰：‘诗人吴梅村之墓’，勿起祠堂，勿乞铭。”闻其言者皆悲之。\[7\]

## 著作

吳偉業著有《[梅村家藏稿](../Page/梅村家藏稿.md "wikilink")》、《[梅村诗馀](../Page/梅村诗馀.md "wikilink")》，[傳奇](../Page/傳奇.md "wikilink")《秣陵春》，[雜劇](../Page/雜劇.md "wikilink")《[通天台](../Page/通天台.md "wikilink")》、《[臨春閣](../Page/臨春閣.md "wikilink")》，史料《[綏寇紀略](../Page/綏寇紀略.md "wikilink")》等。

其詩情深文麗，宮商和諧，敷衍成長篇七言，蔚然可觀，如《洛陽行》詠[福王](../Page/福王.md "wikilink")[朱由崧](../Page/朱由崧.md "wikilink")，《松山哀》詠[洪承疇](../Page/洪承疇.md "wikilink")，《蕭史青門曲》詠寧德公主，《圓圓曲》詠[吳三桂](../Page/吳三桂.md "wikilink")，《雁門尚書行》詠[孫傳庭](../Page/孫傳庭.md "wikilink")，《臨江參軍》詠[楊廷麟](../Page/楊廷麟.md "wikilink")、[盧象昇](../Page/盧象昇.md "wikilink")，《永和宮詞》詠田貴妃，《鴛湖曲》詠[吳昌時](../Page/吳昌時.md "wikilink")，《楚兩生行》詠[柳敬亭](../Page/柳敬亭.md "wikilink")、[蘇昆生](../Page/蘇昆生.md "wikilink")，《詠拙政園山茶花》詠[陳之遴](../Page/陳之遴.md "wikilink")，《聽女道士卞玉京彈琴歌》詠[卞玉京事](../Page/卞玉京.md "wikilink")，《贈寇白門六首》詠[寇白門事](../Page/寇湄.md "wikilink")……。今存1000多首，《[四庫全書總目](../Page/四庫全書總目.md "wikilink")》評論說：“其少作大抵才華艷發，吐納風流，有藻思綺合、清麗芊眠之致。及乎遭逢喪亂，閱歷興亡，激楚蒼涼，風骨彌為遒上。”

其寫，諷刺[吳三桂為](../Page/吳三桂.md "wikilink")[陳圓圓而降清](../Page/陳圓圓.md "wikilink")，傳誦一時。事實上，吳三桂引[清兵入關並非純粹為了](../Page/清兵入關.md "wikilink")「紅顏」，而是一種[政治](../Page/政治.md "wikilink")[投機](../Page/投機.md "wikilink")。據稱當時有人自稱奉平西王之命，賄吳偉業以重金，求自毀《圓圓曲》詩稿，吳偉業一口回絕。\[8\]

## 评价

[梅村公祠壁记碑_0779.jpg](https://zh.wikipedia.org/wiki/File:梅村公祠壁记碑_0779.jpg "fig:梅村公祠壁记碑_0779.jpg")吴伟业在清朝被称为「本朝词家之领袖」\[9\]。陈廷焯评论说：「吴梅村词，虽非专长，然其高处，有令人不可捉摸者，此亦身世之感使然。」又说：「梅村高者，有与[老坡神似处](../Page/蘇東坡.md "wikilink")。」（《白雨斋词话》）[康熙帝亲制御诗](../Page/康熙帝.md "wikilink")《题〈吴伟业集〉》：「梅村一卷足风流，往复搜寻未肯休。秋水精神香雪句，西昆幽思杜陵愁。裁成蜀锦应惭丽，细比春蚕好更抽。寒夜短檠相对处，几多诗兴为君收。」对吴伟业诗歌给予恰当中肯的高度评价，肯定了吴伟业诗歌地位。

關於出仕清朝，清人[方浚师所著](../Page/方浚师.md "wikilink")《[蕉轩随录](../Page/蕉轩随录.md "wikilink")》認為“第梅村受知于庄烈帝，南宫首策，莲烛赐婚，不十年累迁至宫詹学士，负海内重名久矣。当都城失守，帝殉[社稷时](../Page/社稷.md "wikilink")，不能与陈卧子，黄蕴生诸贤致命遂志，又不能与顾亭林，纪伯紫诸子自放山林之间，委蛇伏游，遂事二朝，则不若尚书（王阮亭）之峻整，随园之清高远矣。向使梅村能取义成仁，或隐身岩穴间，其节概文章，皆足以为后学标准，而天下所推为一代冠冕者，亦将不在阮亭而在梅村，岂不尤可惜哉？”。民国[蒋芷侪](../Page/蒋芷侪.md "wikilink")《[都门识小录](../Page/都门识小录.md "wikilink")》載：“昔吴梅村[宫詹](../Page/詹事.md "wikilink")，尝于席上观[伶人演](../Page/伶人.md "wikilink")《[烂柯山](../Page/烂柯山.md "wikilink")》（即《[朱买臣休妻](../Page/朱买臣.md "wikilink")》），某伶于科白时，大声对梅村曰：‘姓朱的有甚亏负于你？’梅村为之面赤。”

## 注釋

<div class="references-small">

<references />

</div>

## 參考書目

  - 顾伊人：《吴梅村先生行状》

## 外部連結

  - 孫康宜：〈[吳梅村的藝術超越觀](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/12/12-99-101.pdf)〉。
  - 王璦玲：〈[「磈壘怎消醫怎識，惟將痛苦付汍瀾」──吳偉業、黃周星劇作中之「存在」焦慮與自我救贖](http://www.litphil.sinica.edu.tw/home/publish/PDF/Bulletin/39/39-41-102.pdf)〉。
  - [江苏太仓吴梅村遗迹查考](http://www.1644wmc.com/List.asp?ID=351)

{{-}}

[Category:明朝榜眼](../Category/明朝榜眼.md "wikilink")
[Category:明朝翰林院编修](../Category/明朝翰林院编修.md "wikilink")
[Category:明朝國子監司業](../Category/明朝國子監司業.md "wikilink")
[Category:明朝復社人物](../Category/明朝復社人物.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝國子監祭酒](../Category/清朝國子監祭酒.md "wikilink")
[Category:明朝作家](../Category/明朝作家.md "wikilink")
[Category:清朝作家](../Category/清朝作家.md "wikilink")
[Category:明朝诗人](../Category/明朝诗人.md "wikilink")
[Category:清朝诗人](../Category/清朝诗人.md "wikilink")
[Category:太倉人](../Category/太倉人.md "wikilink")
[W](../Category/吳姓.md "wikilink")

1.  《[与子疏](../Page/与子疏.md "wikilink")》：“不意年逾二十，遂掇大魁，福过其分，实切悚栗。时有攻宜兴座主（周延儒），借吾作射的者，故榜下即多危疑，赖[烈皇帝保全](../Page/崇禎帝.md "wikilink")。并给假归娶先室程氏。”
2.  馬導源，《吳梅村年譜》（上海：商務印書館，1935年），頁55。
3.  馬導源，《吳梅村年譜》（上海：商務印書館，1935年），頁56-57。
4.  魏斐德著，陳蘇鎮、薄小瑩等譯，《洪業：清朝開國史（增訂版）》（北京：新星出版社，2017年），頁611。
5.  魏斐德著，陳蘇鎮、薄小瑩等譯，《洪業：清朝開國史（增訂版）》（北京：新星出版社，2017年），頁645，注釋113。
6.  馬導源，《吳梅村年譜》（上海：商務印書館，1935年），頁56-60。
7.  《[清史稿](../Page/清史稿.md "wikilink")·列传二百七十一·文苑一》：吴伟业，字骏公，太仓人。明崇祯四年进士，授编修。充东宫讲读官，再迁左庶子。弘光时，授少詹事，乞假归。顺治九年，用两江总督马国柱荐，诏至京。侍郎孙承泽、大学士冯铨相继论荐，授秘书院侍讲，充修太祖、太宗圣训纂修官。十三年，迁祭酒。丁母忧归。
    伟业学问博赡，或从质经史疑义及朝章国故，无不洞悉原委。诗文工丽，蔚为一时之冠，不自标榜。性至孝，生际鼎革，有亲在，不能不依违顾恋，俯仰身世，每自伤也。临殁，顾言：“吾一生遭际，万事忧危，无一时一境不历艰苦。死后敛以僧装，葬我邓尉、灵岩之侧。坟前立一圆石，题曰‘诗人吴梅村之墓’。勿起祠堂，勿乞铭。”闻其言者皆悲之。著有《春秋地理志》、《氏族志》，《绥寇纪略》及《梅村集》。
8.  梅村于顺治十八年所著《清凉山赞佛诗》四首被[程穆衡指为影射](../Page/程穆衡.md "wikilink")[顺治帝](../Page/顺治帝.md "wikilink")[出家于](../Page/出家.md "wikilink")[五台山](../Page/五台山.md "wikilink")。近人[邓之诚等称梅村为妄作](../Page/邓之诚.md "wikilink")。
9.  [张德瀛](../Page/张德瀛.md "wikilink")：《词征》卷六。