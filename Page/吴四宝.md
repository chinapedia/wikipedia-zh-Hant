**吴四宝**（），又名**吴世宝**、**吴云甫**，是20世纪上半叶[中国](../Page/中国.md "wikilink")[上海著名](../Page/上海.md "wikilink")[黑社会人物](../Page/黑社会.md "wikilink")，[極司非爾路76號特務](../Page/極司非爾路76號.md "wikilink")。

## 生平

吴四宝原籍[江苏省](../Page/江苏省.md "wikilink")[南通](../Page/南通.md "wikilink")，未接受过教育。父亲在[上海公共租界的成都路开](../Page/上海公共租界.md "wikilink")[老虎灶卖开水](../Page/老虎灶.md "wikilink")，父亲去世后，随姊夫在[上海跑马厅牵马](../Page/上海跑马厅.md "wikilink")。二十几岁时身量高大，担任
汽车司机，娶妻生子，并加入[青帮](../Page/青帮.md "wikilink")，人称“马立司小四宝”，获得在租界佩枪的执照。后因杀死妻子的情夫，带着女儿到[山东参加](../Page/山东.md "wikilink")[张宗昌的部队](../Page/张宗昌.md "wikilink")，后来又加入[国民革命军](../Page/国民革命军.md "wikilink")[白崇禧的部队参加](../Page/白崇禧.md "wikilink")[北伐](../Page/北伐.md "wikilink")。6年后，39岁的四宝带女儿回上海，不久与[佘爱珍结婚](../Page/佘爱珍.md "wikilink")。吴四宝结婚后，住在[上海法租界巨籁达路](../Page/上海法租界.md "wikilink")（现[巨鹿路](../Page/巨鹿路.md "wikilink")）同福里，对佘爱珍言听计从。当时租界的探长为吴四宝未结的案件前来敲诈，佘爱珍运用其智慧，运动当事人撤销此案\[1\]。

吴四宝、佘爱珍夫妇通过佘爱珍的义父[季云卿结识](../Page/季云卿.md "wikilink")[李士群](../Page/李士群.md "wikilink")。1939年，吴四宝带领大批徒众参加[76号特工总部](../Page/国民党中央执行委员会特务委员会特工总部.md "wikilink")，担任特工总部警卫总队副总队长。李、吴均住在[愚园路](../Page/愚园路.md "wikilink")749弄。

[上海孤岛时期](../Page/孤岛时期.md "wikilink")，吴四宝不仅公开收受[沪西越界筑路地带各赌窟和贩毒机关送来的保护费](../Page/上海公共租界越界筑路.md "wikilink")，同时仍纵容部众从事抢劫汽车、绑架、敲诈勒索等不法活动。

1941年春某日，在沪西越界筑路与公共租界交界的[极司非尔路](../Page/极司非尔路.md "wikilink")（今[万航渡路](../Page/万航渡路.md "wikilink")）与愚园路口，[佘爱珍的保镖因违反不允许携带武器进入租界的禁令](../Page/佘爱珍.md "wikilink")，与英籍巡长發生冲突，引发枪战，多人死伤，只有佘爱珍幸免于难。此后连续发生多起76号特工暗杀公共租界巡捕的事件。

1942年春，吴四宝为上海日本宪兵队以“破坏和运”逮捕，为李士群保释出来，次日去[苏州](../Page/苏州.md "wikilink")，第三天在苏州暴毙。一般认为吴四宝是临行前吃了日本宪兵队的面条中毒而死，也有人认为是被李士群毒死。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:與大日本帝國合作的中國人](../Category/與大日本帝國合作的中國人.md "wikilink")
[Category:中國刺客](../Category/中國刺客.md "wikilink")
[Category:中华民国大陆时期被毒死人物](../Category/中华民国大陆时期被毒死人物.md "wikilink")
[Category:青幫人物](../Category/青幫人物.md "wikilink")
[Category:南通人](../Category/南通人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[S四](../Category/吳姓.md "wikilink")

1.  胡兰成：《今生今世》