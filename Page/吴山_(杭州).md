[吴山天风.jpg](https://zh.wikipedia.org/wiki/File:吴山天风.jpg "fig:吴山天风.jpg")
**吴山**又名**城隍山**或**胥山**\[1\]，是[西湖南山延伸进入](../Page/西湖.md "wikilink")[杭州城区的尾部](../Page/杭州.md "wikilink")，原为纪念春秋时期吴国的爱国名士伍子胥，其上有子胥祠城隍庙，称胥山，后讹为吴山，由紫阳、云居、金地、清平、宝莲、七宝、石佛、宝月、骆驼、峨眉等十几个山头形成西南—东北走向的弧形丘冈，总称[吴山](../Page/吴山.md "wikilink")\[2\]。附近有[吴山广场](../Page/吴山广场.md "wikilink")。

[吴山江湖汇观亭.jpg](https://zh.wikipedia.org/wiki/File:吴山江湖汇观亭.jpg "fig:吴山江湖汇观亭.jpg")
**吴山天风**是[西湖新十景之一](../Page/西湖.md "wikilink")，吴山高100米，是市区之内游人最便捷的登山游乐之地。\[3\]

## 参考资料

[Category:杭州山峰](../Category/杭州山峰.md "wikilink")
[Category:上城区](../Category/上城区.md "wikilink")

1.
2.
3.