**吴法宪**（），[江西](../Page/江西.md "wikilink")[永丰人](../Page/永丰.md "wikilink")。1930年参加[中国工农红军](../Page/中国工农红军.md "wikilink")，同年加入[中国共产主义青年团](../Page/中国共产主义青年团.md "wikilink")，1932年转为[中国共产党党员](../Page/中国共产党.md "wikilink")。1955年被授予[中国人民解放军中将军衔](../Page/中国人民解放军中将.md "wikilink")。

隸屬[林彪的](../Page/林彪.md "wikilink")[四野系統](../Page/中国人民解放军第四野战军.md "wikilink")，1949年是空軍負責人，1965年接任[空軍司令員](../Page/中国人民解放军空军司令员.md "wikilink")，[文革時升](../Page/文革.md "wikilink")「[軍委辦事組](../Page/軍委辦事組.md "wikilink")」副組長和副總參謀長，與[周恩來關係密切](../Page/周恩來.md "wikilink")。他是“[林彪、江青反革命集团案主犯](../Page/林彪、江青反革命集团案.md "wikilink")”之一，1981年被判处有期徒刑17年，剥夺政治权利5年，八個月後保外就醫。2004年10月17日在[山东](../Page/山东.md "wikilink")[济南逝世](../Page/济南.md "wikilink")，享年89岁。

吴法宪在回忆录中表示，在[朝鮮戰爭爆發前的](../Page/朝鮮戰爭.md "wikilink")1950年5月，[中共中央軍委決定要](../Page/中共中央軍委.md "wikilink")[黃永勝](../Page/黃永勝.md "wikilink")、吴法宪等人準備帶領13兵團的四個軍參加朝鮮戰爭。\[1\]

## 简历

  - 1940年5月任[八路军第五纵队政治部主任](../Page/八路军.md "wikilink")
  - 1941年任[新四军第三师政治部主任](../Page/新四军.md "wikilink")
  - 1946年任[辽西军区副政委](../Page/辽西.md "wikilink")，[东北民主联军第二纵队政委](../Page/东北民主联军.md "wikilink")
  - 1949年任[第四野战军三十九军政委](../Page/第四野战军.md "wikilink")、第四野战军第十四兵团副政委兼政治部主任
  - 1950年任[中国人民解放军空军副政治委员兼政治部主任](../Page/中国人民解放军空军.md "wikilink")
  - 1957年任空军政治委员
  - 1965年任空军司令员
  - 1967年任中国人民解放军副总参谋长兼空军司令员
  - 1969年当选中共九届中央政治局委员
  - 1973年被开除党籍，撤销党内外一切职务。

## 著作

  -
## 参考文献

<references />

## 参见

  - [林彪、江青反革命集团案](../Page/林彪、江青反革命集团案.md "wikilink")

{{-}}        中国人民解放军第三十九军政治委员

[Category:中国人民解放军空军中将](../Category/中国人民解放军空军中将.md "wikilink")
[Category:林彪、江青反革命集团案主犯](../Category/林彪、江青反革命集团案主犯.md "wikilink")
[Category:文革时期人物](../Category/文革时期人物.md "wikilink")
[F法](../Category/吳姓.md "wikilink")
[Category:永丰人](../Category/永丰人.md "wikilink")
[Category:中国共产党第九届中央政治局委员](../Category/中国共产党第九届中央政治局委员.md "wikilink")

1.