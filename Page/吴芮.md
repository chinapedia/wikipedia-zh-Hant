**吳芮**（），[中國](../Page/中國.md "wikilink")[西漢](../Page/西漢.md "wikilink")[長沙國第一代王](../Page/長沙國.md "wikilink")。[秦朝時任番陽令](../Page/秦朝.md "wikilink")，號曰番君。[戰國末期](../Page/戰國_\(中國\).md "wikilink")，父[吳申曾任](../Page/吳申.md "wikilink")[楚國大司馬](../Page/楚國.md "wikilink")。秦二世元年(前209年)，**吳芮**率部將[梅鋗及女婿](../Page/梅鋗_\(秦\).md "wikilink")[英布響應](../Page/英布.md "wikilink")[陳勝的](../Page/陳勝.md "wikilink")[大澤之變](../Page/大澤之變.md "wikilink")。前206年，[項羽攻入](../Page/項羽.md "wikilink")[咸陽](../Page/咸陽.md "wikilink")，因其率[百越人民起兵](../Page/百越.md "wikilink")，又跟隨大軍入[函谷關](../Page/函谷關.md "wikilink")，封其為[衡山王](../Page/衡山王.md "wikilink")，都邾(今[湖北](../Page/湖北.md "wikilink")[黃岡](../Page/黃岡.md "wikilink"))。不久受[項羽密令與](../Page/項羽.md "wikilink")[臨江王](../Page/臨江王.md "wikilink")[共敖](../Page/共敖.md "wikilink")，[九江王](../Page/九江王.md "wikilink")[英布擊殺](../Page/英布.md "wikilink")[義帝於郴州](../Page/楚義帝.md "wikilink")（今[湖南](../Page/湖南.md "wikilink")[郴州](../Page/郴州.md "wikilink")）。

[漢高祖五年](../Page/漢高祖.md "wikilink")（前202年）二月，徙封長沙王，都臨湘（今[湖南](../Page/湖南.md "wikilink")[長沙](../Page/長沙.md "wikilink")）。領[長沙郡](../Page/長沙郡.md "wikilink")（今[湖南省](../Page/湖南省.md "wikilink")）、[豫章郡](../Page/豫章郡.md "wikilink")（今[江西省](../Page/江西省.md "wikilink")）、[象郡](../Page/象郡.md "wikilink")、[桂林郡](../Page/桂林郡.md "wikilink")（今[廣西壮族自治区](../Page/廣西壮族自治区.md "wikilink")）及[南海郡](../Page/南海郡.md "wikilink")（今[廣東省](../Page/廣東省.md "wikilink")）五郡。但當時象郡、桂林郡及南海郡已被[南越王](../Page/南越王.md "wikilink")[趙佗所佔](../Page/趙佗.md "wikilink")，豫章郡又早已封給[淮南王](../Page/淮南王.md "wikilink")[英布](../Page/英布.md "wikilink")，長沙國實際上版圖只有長沙一郡。

[漢高祖五年](../Page/漢高祖.md "wikilink")（前202年）七月，奉命率兵定閩，病死於途中，諡**文**，長子[吳臣襲位](../Page/吳臣_\(西漢\).md "wikilink")。

三国时期，处于[孙吴控制区的吴芮的坟墓被挖开](../Page/孙吴.md "wikilink")，发现吴芮尸体和衣服不朽。同时坟墓内的砖块被用于兴建[孙坚庙](../Page/孙坚.md "wikilink")。\[1\]

## 世系图

<center>

</center>

[W吴](../Category/长沙历史.md "wikilink")
[W吴](../Category/秦朝民變領袖.md "wikilink")
[W吴](../Category/楚漢戰爭人物.md "wikilink")
[W吴](../Category/西漢政治人物.md "wikilink")
[W吴](../Category/余干人.md "wikilink")
[R芮](../Category/吴姓.md "wikilink")

1.  《三国志》卷28引《世语》：黄初末，吴人发长沙王吴芮冢，以其砖于临湘为孙坚立庙。芮容貌如生，衣服不朽。