[Confucius_Tang_Dynasty.jpg](https://zh.wikipedia.org/wiki/File:Confucius_Tang_Dynasty.jpg "fig:Confucius_Tang_Dynasty.jpg")像，（传）吴道子绘\]\]
**吴道子**又称**吳道元**（\[1\]），字**道子**，後改名為**道玄**，画史尊称“吴生”。阳翟（今[河南](../Page/河南.md "wikilink")[禹县](../Page/禹县.md "wikilink")）人。[中国](../Page/中国.md "wikilink")[唐代著名](../Page/唐代.md "wikilink")[画家](../Page/画家.md "wikilink")，被称为“百代画圣”。

## 生平

出生年份没有记载
。吴道子幼年家境贫寒，初为民间画工，相传曾随[张旭](../Page/张旭.md "wikilink")、[贺知章学习书法](../Page/贺知章.md "wikilink")，年轻时即有画名。后来作了[山东](../Page/山东.md "wikilink")[兖州](../Page/兖州.md "wikilink")[瑕丘](../Page/瑕丘县.md "wikilink")[县尉](../Page/县尉.md "wikilink")，不久即辞职。漫游[洛阳](../Page/洛阳.md "wikilink")，从事壁画创作。时曾为将军[裴旻作画](../Page/裴旻.md "wikilink")，被当时人将[张旭](../Page/张旭.md "wikilink")[草书](../Page/草书.md "wikilink")、[裴旻舞剑](../Page/裴旻.md "wikilink")、吴道子作画同时称为“三绝”。

[开元年间以善画被](../Page/开元.md "wikilink")[唐玄宗召入宫中](../Page/唐玄宗.md "wikilink")，历任[供奉](../Page/供奉.md "wikilink")、[中书省](../Page/中书省.md "wikilink")[内教博士](../Page/内教博士.md "wikilink")，改为道玄，后官[宁王友](../Page/宁王.md "wikilink")（官名），此后一直为宫廷作画。通过观赏[公孙大娘舞剑](../Page/公孙大娘.md "wikilink")，体会用笔之道。吴道子擅佛教、道教人物画，还有神鬼、人物、山水、鸟兽、草木、楼阁等，尤精于[佛](../Page/佛.md "wikilink")[道](../Page/道.md "wikilink")、人物，长于[壁画创作](../Page/壁画.md "wikilink")，据载他曾于[长安](../Page/长安.md "wikilink")、洛阳两地寺观中绘制壁画多达300余堵，奇踪怪状，无有雷同，其中尤以繪于長安[景云寺的](../Page/景云寺.md "wikilink")《地狱变相》闻名于时\[2\]。

## 风格

吴道子的绘画具有独特风格。早年笔法较细，风格稠密；中年变为遒劲，[宋代画家](../Page/宋代.md "wikilink")[米芾形容吴道子笔下圆润似](../Page/米芾.md "wikilink")“莼菜条”，滑溜细腻，波浪起伏，点划之间，时见缺落，有笔不周而意周之妙。后人把他和[张僧繇并称](../Page/张僧繇.md "wikilink")“疏体”。吴道子所画人物衣褶，飘飘欲举，线条遒劲，潇洒秀逸，当时人将他和[北朝](../Page/北朝.md "wikilink")[齐时代著名画家](../Page/齐.md "wikilink")[曹仲达相提并论](../Page/曹仲达.md "wikilink")，称他们为“吴带当风，曹衣出水”，因为曹仲达画中的人物，衣服常紧裹在躯体上；而吴道子笔下的人物，大袖飘飘，线条流畅。他利用线条的宽窄变化表现物体的凹凸，使线描的画面具有立体感。他喜用焦墨勾线，略施淡彩，自然超出绢素，称为“吴装”。作画线条简练，笔才一二，象已应焉。其山水画作蜀道之景，自为一家；又传曾于大同殿壁画[嘉陵江三百余里山水](../Page/嘉陵江.md "wikilink")，一日而毕。吴道子的绘画对后世影响极大，他被人们尊为“画圣”，被民间画工尊为祖师。[苏轼曾称赞他说](../Page/苏轼.md "wikilink")“画至吴道子，古今之变、天下之能事毕矣”、“出新意于法度之中，寄好理于豪放之外”。

## 作品

吴道子在史籍中富有盛名，但因为他大量的创作是壁画，所以很少有传世作品保留下来，无真迹传世，传至今日的《送子天王图》可能为[宋代摹本](../Page/宋代.md "wikilink")，但已经可以窥见吴道子笔法的精髓。另外还流传有《宝积宾伽罗佛像》、《道子墨宝》等摹本，[敦煌](../Page/敦煌.md "wikilink")[莫高窟第](../Page/莫高窟.md "wikilink")103窟的[维摩经变图](../Page/维摩经.md "wikilink")，亦被认为是他的画风。[徐悲鸿曾经获得一幅残卷](../Page/徐悲鸿.md "wikilink")，经他和[张大千鉴定](../Page/张大千.md "wikilink")，认为是吴道子真迹，可能是一幅壁画的草图，暂根据画中人物取名为《[八十七神仙卷](../Page/八十七神仙卷.md "wikilink")》，此幅画气势磅礴，人物闲适秀丽，是中国古代画的精品。

## 参考文献

## 外部链接

[Category:唐朝畫家](../Category/唐朝畫家.md "wikilink")
[Category:唐朝县尉](../Category/唐朝县尉.md "wikilink")
[Category:唐朝中书省官员](../Category/唐朝中书省官员.md "wikilink")
[Category:禹州人](../Category/禹州人.md "wikilink")
[D道元](../Category/吴姓.md "wikilink")
[Category:河南画家](../Category/河南画家.md "wikilink")

1.  王伯敏，1982，《中國繪畫史》，人民美術出版社

2.