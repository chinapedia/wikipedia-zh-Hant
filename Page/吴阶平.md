**吴阶平**（），原名**吴泰然**，[号](../Page/号.md "wikilink")**阶平**\[1\]，[江苏](../Page/江苏.md "wikilink")[常州](../Page/常州.md "wikilink")[武进人](../Page/武进.md "wikilink")，中国[泌尿外科](../Page/泌尿外科.md "wikilink")[医学家](../Page/医学.md "wikilink")，[中国科学院和](../Page/中国科学院.md "wikilink")[中国工程院双料](../Page/中国工程院.md "wikilink")[院士](../Page/院士.md "wikilink")。

## 生平

吴阶平自小于[天津长大](../Page/天津.md "wikilink")。1942年毕业于[北京协和医学院](../Page/北京协和医学院.md "wikilink")，获得[医学博士学位](../Page/医学博士.md "wikilink")。1947年赴[美国](../Page/美国.md "wikilink")[芝加哥大学留学](../Page/芝加哥大学.md "wikilink")，次年回国。[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，他就职于[北京医学院](../Page/北京医学院.md "wikilink")，曾率医疗队参加[朝鲜战争](../Page/朝鲜战争.md "wikilink")。1956年，吴加入[中国共产党](../Page/中国共产党.md "wikilink")。

从1954年起，吴阶平先后奉命为[金日成](../Page/金日成.md "wikilink")、[苏加诺](../Page/苏加诺.md "wikilink")、[胡志明](../Page/胡志明.md "wikilink")、[费迪南德·马科斯等人提供过医疗服务](../Page/费迪南德·马科斯.md "wikilink")。1967年底开始任[江青的保健医生](../Page/江青.md "wikilink")，次年又服务于[林彪](../Page/林彪.md "wikilink")。1972年，吴阶平出任[周恩来医疗小组组长](../Page/周恩来.md "wikilink")，为其诊治[膀胱癌](../Page/膀胱癌.md "wikilink")。

1980年，吴阶平当选[中国科学院](../Page/中国科学院.md "wikilink")[学部委员](../Page/学部委员.md "wikilink")\[2\]。1989年，被选为[九三学社中央副主席](../Page/九三学社.md "wikilink")，1992年当选主席，1990年在[第七届全国人民代表大会第三次会议上被補選為](../Page/第七届全国人民代表大会第三次会议.md "wikilink")[全國人大常委会委员](../Page/全國人大常委会.md "wikilink")，1993年至2003年任[全国人大常委会副委员长](../Page/全国人大常委会副委员长.md "wikilink")。

2000年2月28日，[吴阶平医学基金会在北京成立](../Page/吴阶平医学基金会.md "wikilink")，为[中华人民共和国卫生部直属的行业基金会](../Page/中华人民共和国卫生部.md "wikilink")\[3\]。

2011年3月2日21时18分在北京逝世\[4\]。

## 家庭

其兄[吴瑞萍](../Page/吴瑞萍.md "wikilink")、弟[吴蔚然亦是著名医学家](../Page/吴蔚然.md "wikilink")。

## 参考文献

## 外部链接

  - [吴阶平医学基金会](http://www.wjpmf.org/)
  - [北京大学·吴阶平](https://web.archive.org/web/20070706234500/http://www.pku.edu.cn/about/yuanshi/wjp.htm)
  - [九三学社主席传记·吴阶平](https://web.archive.org/web/20070929000406/http://www.jq93.cn/Article/lilun/lishi/200611/40.html)
  - [杏林绛帐
    圣手慈心——《吴阶平传》书眉札记·徐唐龄](https://web.archive.org/web/20070927051616/http://www.jsxx.org/93/html/mzykx/democracy00_4/37.htm)

{{-}}

[Category:中华人民共和国党和国家领导人
(中国科学院院士)](../Category/中华人民共和国党和国家领导人_\(中国科学院院士\).md "wikilink")
[Category:中华人民共和国党和国家领导人
(中国工程院院士)](../Category/中华人民共和国党和国家领导人_\(中国工程院院士\).md "wikilink")
[Category:中华人民共和国领导人](../Category/中华人民共和国领导人.md "wikilink")
[Category:中华医学会会长](../Category/中华医学会会长.md "wikilink")
[Category:中国科学院生命科学和医学学部院士](../Category/中国科学院生命科学和医学学部院士.md "wikilink")
[Category:中国工程院医药卫生学部院士](../Category/中国工程院医药卫生学部院士.md "wikilink")
[Category:中华人民共和国泌尿外科医生](../Category/中华人民共和国泌尿外科医生.md "wikilink")
[Category:中華人民共和國醫學家](../Category/中華人民共和國醫學家.md "wikilink")
[Category:第九届全国人大常委会副委员长](../Category/第九届全国人大常委会副委员长.md "wikilink")
[Category:第八届全国人大常委会副委员长](../Category/第八届全国人大常委会副委员长.md "wikilink")
[Category:第七屆全國人大代表](../Category/第七屆全國人大代表.md "wikilink")
[Category:第五届全国政协委员](../Category/第五届全国政协委员.md "wikilink")
[Category:第六届全国政协委员](../Category/第六届全国政协委员.md "wikilink")
[Category:世界科学院院士](../Category/世界科学院院士.md "wikilink")
[Category:九三學社社員](../Category/九三學社社員.md "wikilink")
[Category:中國共產黨黨員](../Category/中國共產黨黨員.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:北京協和醫學院校友](../Category/北京協和醫學院校友.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:九三学社中央委员会主席](../Category/九三学社中央委员会主席.md "wikilink")
[Category:武進人](../Category/武進人.md "wikilink")
[Je阶平](../Category/吳姓.md "wikilink")
[Category:中华人民共和国党和国家领导人的专职保健医生](../Category/中华人民共和国党和国家领导人的专职保健医生.md "wikilink")

1.
2.
3.
4.