**吸虫**（学名：）是[寄生虫的一种](../Page/寄生虫.md "wikilink")，为[扁形动物门](../Page/扁形动物门.md "wikilink")[吸虫纲动物的总称](../Page/吸虫纲.md "wikilink")，也称为**瓜仁虫**。一些吸虫也被称为**二口虫**（拉丁語：Distoma）。其名由来是因为它的口吸盘和腹吸盘都被认为是口而致，[拉丁语中](../Page/拉丁语.md "wikilink")，Di代表“二”，stoma代表“口”。

## 概述

吸蟲是非常有名的一種[寄生蟲](../Page/寄生蟲.md "wikilink")，大部分為[雌雄同體](../Page/雌雄同體.md "wikilink")，可自體受精；生活史中可能有一個或多個[中間宿主](../Page/中間宿主.md "wikilink")，以[魚類](../Page/魚類.md "wikilink")、[人體等多種](../Page/人體.md "wikilink")[脊椎動物為最終宿主](../Page/脊椎動物.md "wikilink")；成蟲產卵發育成[纖毛幼蟲](../Page/纖毛幼蟲.md "wikilink")（Miracidium，亦作[毛蚴](../Page/毛蚴.md "wikilink")），纖毛幼蟲入侵中間宿主後，增殖並發育成[尾動幼蟲](../Page/尾動幼蟲.md "wikilink")(cercaride，亦作[尾蚴](../Page/尾蚴.md "wikilink"))，再被最終宿主食入前會發育成[曩狀幼蟲](../Page/曩狀幼蟲.md "wikilink")（metacerariae，亦作[曩蚴](../Page/曩蚴.md "wikilink")）。吸蟲的成蟲有口吸盤及腹吸盤，其卵有[卵蓋](../Page/卵蓋.md "wikilink")（operculum），蟲體在人體中最久可活30年，由生食淡水魚而感染。貓、狗為一種名為保蟲的吸蟲宿主，成蟲寄生於[膽道](../Page/膽管.md "wikilink")，大量感染時可在膽囊中發現蟲體。曩狀幼蟲存在於[淡水魚時](../Page/淡水魚.md "wikilink")，醃漬、煙燻或脫水皆不能將其殺死，輕微感染時無症狀，重度感染會造成膽道阻塞、[膽囊炎](../Page/膽囊炎.md "wikilink")、[結石](../Page/結石.md "wikilink")、[肝膿痬或](../Page/肝膿痬.md "wikilink")[急性胰臟炎等](../Page/胰臟炎.md "wikilink")，蟲卵約於感染後1個月後可於糞便中發現。

## 分類

吸蟲綱物種原來分為[单殖目](../Page/单殖目.md "wikilink")（Monogena）、[複殖目](../Page/複殖目.md "wikilink")（Digenea）、[盾腹目](../Page/盾腹目.md "wikilink")（Aspidogastrea，亦作[盾盘目或](../Page/盾盘目.md "wikilink")[楯盤目](../Page/楯盤目.md "wikilink")）三個目。單殖、複殖所指的是物種的雄性生殖系统的[输精管的数目](../Page/输精管.md "wikilink")（单殖1个、複殖2个）；而盾腹目的命名則沿於其特征，在其[腹口吸盘周围有一個盾牌狀的衬垫](../Page/腹口.md "wikilink")。當時的單殖目可再細分為三個亞目，但現時單殖目已被提升至綱級，成為[單殖綱](../Page/單殖綱.md "wikilink")。餘下的兩個目合計的物種，推算約
18,000\[1\]到 24,000\[2\]個物種。有文獻把這兩個目提升到亞綱的地位。

複殖目又名[二生目](../Page/二生目.md "wikilink")，是吸蟲綱絕大多數物種所屬，之下再分為[腹口亞目](../Page/腹口亞目.md "wikilink")（Gasterostomata，寄生於魚腸內）及[前口亞目](../Page/前口亞目.md "wikilink")（Prosostomata，有59科），其物種泛稱複殖吸蟲（digenetic
trematode）是以寄生人體為對象的吸蟲。複殖吸蟲的基本結構和發育過程大致上相同，種類相當繁多且形態各自相差頗大，生活史有性世代（寄生於軟體動物，如螺螄、蚌類的階段）與無性世代（寄生於脊椎動物體內階段），基本生活型態是以卵生且多種幼蟲（[毛蚴](../Page/毛蚴.md "wikilink")、[胞蚴](../Page/胞蚴.md "wikilink")、[雷蚴](../Page/雷蚴.md "wikilink")、[尾蚴](../Page/尾蚴.md "wikilink")、[囊蚴](../Page/囊蚴.md "wikilink")）之後在成長為成蟲。這些吸虫中以人类为宿主的种类，有肺吸虫（[卫氏肺吸虫](../Page/卫氏肺吸虫.md "wikilink")）、肝吸虫（[中華肝吸蟲](../Page/中華肝吸蟲.md "wikilink")，[牛羊肝吸蟲](../Page/牛羊肝吸蟲.md "wikilink")）、[肠道吸虫](../Page/肠道吸虫.md "wikilink")、[横川吸虫](../Page/横川吸虫.md "wikilink")、[血吸虫](../Page/血吸虫.md "wikilink")（[日本血吸虫](../Page/日本血吸虫.md "wikilink")）、[布氏薑片蟲](../Page/布氏薑片蟲.md "wikilink")（Fasciolopsis
buski）等。

楯盤目只有一科四屬，直接發育，無需中間宿主。今提升至[亞綱級](../Page/亞綱.md "wikilink")，是為[楯吸蟲亞綱或](../Page/楯吸蟲亞綱.md "wikilink")[楯盤亞綱](../Page/楯盤亞綱.md "wikilink")。

## 構造

體壁組織可分為體被與肌肉；可自我更新；具有吸收營養、感覺等功能。

## 参考文献

## 参见

  - [血吸虫病](../Page/血吸虫病.md "wikilink")

{{-}}

[吸虫纲](../Category/吸虫纲.md "wikilink")
[Category:寄生蟲](../Category/寄生蟲.md "wikilink")
[Category:病原微生物](../Category/病原微生物.md "wikilink")

1.
2.