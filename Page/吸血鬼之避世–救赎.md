《**吸血鬼之避世救赎**》("**Vampire: The Masquerade –
Redemption**")是一款由[Nihilistic制作](../Page/:en:Nihilistic.md "wikilink")，在1999年由[动视在电脑上发行的一款](../Page/动视.md "wikilink")[PC](../Page/PC.md "wikilink")[角色扮演游戏](../Page/電子角色扮演遊戲.md "wikilink")。故事以[桌上角色扮演游戏](../Page/桌上角色扮演游戏.md "wikilink")《[吸血鬼之避世](../Page/吸血鬼之避世.md "wikilink")》为背景设定，以法国[十字军战士Christof](../Page/十字军.md "wikilink")
Romuald的冒险为开端，讲述他变成一个吸血鬼的过程和他与修女Anezka的故事。故事线横跨千年，包括古代部分和现代部分，且2部分的玩法差异很大。场景包括[中世纪的](../Page/中世纪.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")、[维也纳直到现代的](../Page/维也纳.md "wikilink")[伦敦与](../Page/伦敦.md "wikilink")[纽约等地](../Page/纽约.md "wikilink")。

这部游戏采用了当时较先进的图形处理技术，包括对室内多点光源的多重阴影效果的完美展现。游戏的音乐也是一大特色，随着故事的发展和场景的变化，背景音乐时而悠扬抒情，时而壮阔大气，时而富有节奏感。

## 情节概要

## 评论情况

对于《救赎》的评论有褒有贬。\[1\]尽管其娱乐性受到好评，但对程序bug和设计缺陷的抱怨也不少。

## 所获奖项

  - [E3](../Page/E3.md "wikilink")1999年：最佳RPG

## 参考资料

## 延伸阅读

  - Green, Jeff (April 1999) "Vampire The Masquerade: Redemption"
    *Computer Gaming World* Issue \#177, p. 110, a software review

## 另见

  - 《[吸血鬼之避世–血族](../Page/吸血鬼之避世–血族.md "wikilink")》

## 外部链接

  -
  -
[Category:1999年电子游戏](../Category/1999年电子游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:吸血鬼之避世](../Category/吸血鬼之避世.md "wikilink")
[Category:动视游戏](../Category/动视游戏.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")

1.  [Vampire: The Masquerade - Redemption (pc: 2000):
    Reviews](http://www.metacritic.com/games/platforms/pc/vampiremasqueraderedemp)