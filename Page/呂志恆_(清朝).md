**呂志恒**（），[字](../Page/表字.md "wikilink")**立吾**，[江蘇](../Page/江蘇.md "wikilink")[陽湖人](../Page/陽湖.md "wikilink")，[清朝政治人物](../Page/清朝.md "wikilink")，[監生出身](../Page/監生.md "wikilink")。

## 生平

呂志恒本為[監生](../Page/監生.md "wikilink")，後捐得[縣丞](../Page/縣丞.md "wikilink")。[嘉慶二十二年](../Page/嘉慶.md "wikilink")（1817年）七月，署[臺灣府](../Page/臺灣府.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")。歷任[臺灣府北路](../Page/臺灣府.md "wikilink")[理番鹿仔港海防捕盜同知](../Page/理番.md "wikilink")、[臺灣府撫民理番](../Page/臺灣府.md "wikilink")[海防糧補](../Page/海防.md "wikilink")[通判](../Page/通判.md "wikilink")、[臺灣府海防兼南路理番同知](../Page/臺灣府海防兼南路理番同知.md "wikilink")、[福州海防](../Page/福州.md "wikilink")[同知等](../Page/同知.md "wikilink")。[道光二年](../Page/道光.md "wikilink")（1822年）接替[张缙云任](../Page/张缙云.md "wikilink")[龙岩](../Page/龙岩.md "wikilink")[知州一职](../Page/知州.md "wikilink")，[道光三年](../Page/道光.md "wikilink")（1823年）由[陈士荣接任](../Page/陈士荣.md "wikilink")\[1\]。[道光十一年](../Page/道光.md "wikilink")（1831年）因功陞任[臺灣府知府一職](../Page/臺灣府知府.md "wikilink")。翌年，農人[張丙起事](../Page/張丙.md "wikilink")，他於[台南率軍前往](../Page/臺南市.md "wikilink")[嘉義解圍](../Page/嘉義市.md "wikilink")，不久於兩軍交戰中陣亡\[2\]。

## 參考文獻

{{-}}

[Category:清朝彰化縣知縣](../Category/清朝彰化縣知縣.md "wikilink")
[Category:臺灣府知府](../Category/臺灣府知府.md "wikilink")
[Category:臺灣府海防兼南路理番同知](../Category/臺灣府海防兼南路理番同知.md "wikilink")
[Category:清朝龍岩直隸州知州](../Category/清朝龍岩直隸州知州.md "wikilink")
[Category:清朝通判](../Category/清朝通判.md "wikilink")
[Category:清朝战争身亡者](../Category/清朝战争身亡者.md "wikilink")
[Category:常州人](../Category/常州人.md "wikilink")
[Z](../Category/毗陵呂氏.md "wikilink")

1.
2.  劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。