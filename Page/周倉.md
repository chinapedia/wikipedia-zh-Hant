[GuanYu-ZhouCang.jpg](https://zh.wikipedia.org/wiki/File:GuanYu-ZhouCang.jpg "fig:GuanYu-ZhouCang.jpg")
[Zhou_Cang_in_Beijing_opera.JPG](https://zh.wikipedia.org/wiki/File:Zhou_Cang_in_Beijing_opera.JPG "fig:Zhou_Cang_in_Beijing_opera.JPG")中周仓的扮相\]\]

**周倉**，字**元福**，是《[三國演義](../Page/三國演義.md "wikilink")》中的[人物](../Page/人物.md "wikilink")，在《[山西通志](../Page/山西通志.md "wikilink")》中也有记载，關西人，本為[黃巾賊首領](../Page/黃巾賊.md "wikilink")[張寶的麾下之將](../Page/張寶.md "wikilink")，在[黃巾軍潰敗之後落草為寇](../Page/黃巾軍.md "wikilink")。[漢壽亭侯](../Page/漢壽亭侯.md "wikilink")[關羽](../Page/關羽.md "wikilink")[千里走單騎尋找](../Page/過五關斬六將.md "wikilink")[劉備之時](../Page/劉備.md "wikilink")，經過[裴元紹的介紹而結識周倉](../Page/裴元紹.md "wikilink")。周倉在隨[黃巾軍征戰之時即聽聞過](../Page/黃巾軍.md "wikilink")[關羽的大名](../Page/關羽.md "wikilink")，當即表示要追隨[關羽](../Page/關羽.md "wikilink")。

## 歷史原型

在《[三國志](../Page/三國志.md "wikilink")‧魯肅傳》中[孫吳大臣](../Page/孫吳.md "wikilink")[魯肅在和](../Page/魯肅.md "wikilink")[蜀漢戍守](../Page/蜀漢.md "wikilink")[荊州的關羽一次會晤中](../Page/荊州.md "wikilink")，指責關羽不講信義，不還荊州。但是話還沒說完，席間某人突然反駁說：「所謂土地，只要給有德行的人擁有就夠了，哪裡有本來屬於誰的道理！」這段突如其來的發言引來了魯肅嚴厲地喝斥。接著，關羽拿起武器對那人說：「這是國家大事，你這人懂些什麼！」說完便使了眼色教那人離開。\[1\]

後世的[說書人](../Page/說書.md "wikilink")、[戲曲家將這段歷史資料加以描寫](../Page/戲曲.md "wikilink")，給席間出言反駁魯肅的關羽[幕僚起了一個名字](../Page/幕僚部門.md "wikilink")：周倉。藉此人的心直口快，為觀眾抒發孫吳處處計較[蜀漢的不平之氣](../Page/蜀漢.md "wikilink")，也同時對比出關羽的沉穩和大局之識。儘管[正史從來沒有提及那位幕僚的名稱](../Page/正史.md "wikilink")，戲曲家們仍然樂於把他喚作周倉。

## 民間影響

在[民間信仰中](../Page/民間信仰.md "wikilink")，信眾稱周倉為周倉將軍、周大將軍、周恩師等名號，常配祀於[關聖帝君之右側](../Page/關聖帝君.md "wikilink")（左側為[關平太子](../Page/關平.md "wikilink")）作為其從祀神。其形象多為黑面長鬚、執持[關刀而立](../Page/關刀.md "wikilink")。

以周倉為主神之廟宇少見，有[新北市](../Page/新北市.md "wikilink")[萬里區野柳仁和宮](../Page/萬里區.md "wikilink")、國聖仁和宮\[2\]、[臺南市](../Page/臺南市.md "wikilink")[鹽水區鎮南宮](../Page/鹽水區.md "wikilink")\[3\]等。

非為主神但獨立奉祀（非作為關帝挟祀神）者，有[馬祖](../Page/馬祖.md "wikilink")[北竿鄉白沙境平水尊王廟](../Page/北竿鄉.md "wikilink")\[4\]。

## 藝術形象

### 三國演義

《三國演義》中[關羽辭行](../Page/關羽.md "wikilink")[曹操](../Page/曹操.md "wikilink")、[過五關斬六將](../Page/過五關斬六將.md "wikilink")，在臥牛山與周倉初遇。周倉原在黄巾軍張寶麾下，黃巾軍遭滅後嘯聚山林、築寨為王，因久慕關羽盛名，遂放火燒寨、遣散部眾以示其誠，隻身跟隨關羽。

關羽鎮守[荊州對抗](../Page/荊州_\(古代\).md "wikilink")[曹魏名將](../Page/曹魏.md "wikilink")[于禁與](../Page/于禁.md "wikilink")[白馬將軍](../Page/白馬將軍.md "wikilink")[龐德時](../Page/龐德.md "wikilink")，使用水計大敗魏軍，期間周倉與龐德在水中激鬥一場，最終龐德因為不識水性而敗於周倉之手。

周倉在得知君主關羽父子退走麥城被[孫權斬首後](../Page/孫權.md "wikilink")，也[自刎殉主](../Page/自刎.md "wikilink")。

### 影視

  - 1994年《[三國演義](../Page/三國演義_\(電視劇\).md "wikilink")》（[中國中央電視台電視劇集](../Page/中國中央電視台.md "wikilink")）：劉潤成
  - 2010年《[三國](../Page/三国_\(电视剧\).md "wikilink")》（[中國大陸電視劇](../Page/中國大陸.md "wikilink")）：張驕陽

### 動漫遊戲

  - [三國志](../Page/三國志_\(動畫電影\).md "wikilink")
  - [三國演義](../Page/三國演義_\(動畫\).md "wikilink")
  - 《[火鳳燎原](../Page/火鳳燎原.md "wikilink")》（[陳某](../Page/陳某.md "wikilink")）
  - 《[無雙☆群星大會串](../Page/無雙☆群星大會串.md "wikilink")》
  - 《[真·三國無雙8](../Page/真·三國無雙8.md "wikilink")》（[光榮特庫摩](../Page/光榮特庫摩.md "wikilink")，[澤城千春配音](../Page/澤城千春.md "wikilink")）

## 腳註

## 参考资料

  - 《原來三國是這樣》盧盛江著

[C](../Category/周姓.md "wikilink")
[Category:關羽](../Category/關羽.md "wikilink")
[Category:三國演義登場人物](../Category/三國演義登場人物.md "wikilink")
[Category:三国演义虛構角色](../Category/三国演义虛構角色.md "wikilink")

1.  見《三國志》[吳書‧周瑜魯肅呂蒙傳](../Page/:s:三國志/卷54.md "wikilink")，原文：「肅邀羽相見，各駐兵馬百步上，但諸將軍單刀俱會。肅因責數羽曰：“國家區區本以土地借卿家者，卿家軍敗遠來，無以為資故也。今已得益州，既無奉還之意，但求三郡，又不從命。”語未究竟，坐有一人曰：“夫土地者，惟德所在耳，何常之有！”肅厲聲呵之，辭色甚切。羽操刀起謂曰：“此自國家事，是人何知！”目使之去。」
2.  [萬里仁和宮](http://nrch.culture.tw/twpedia.aspx?id=27327)
3.  [臺南市 / 鹽水區 /
    鎮南宮](http://crgis.rchss.sinica.edu.tw/temples/TainanCity/yanshuei/1102006-ZNG)
4.  [白沙境平水尊王廟](http://crgis.rchss.sinica.edu.tw/temples/LienchiangCounty/beigan/90070264006-PSZWM)