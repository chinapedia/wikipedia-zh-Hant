**周僖王**，又作**周-{釐}-王**（），[姓](../Page/姓.md "wikilink")**[姬](../Page/姬姓.md "wikilink")**，名**胡齐**，[東周第四代君主](../Page/東周.md "wikilink")，在位5年，\[1\][號](../Page/諡號.md "wikilink")-{僖}-。

僖王爲[周莊王長子](../Page/周莊王.md "wikilink")，莊王雖偏爱姚姬生的少子[王子頹](../Page/王子頹.md "wikilink")，但未能廢長立幼。前681年，僖王即位。

前679年，[曲沃武公滅](../Page/曲沃武公.md "wikilink")[晉後](../Page/晉國.md "wikilink")，把晉的寶器獻給僖王，僖王封曲沃武公為晉的國君，並列為諸侯。前678年，遭晉軍攻打並殺害夷邑大夫詭諸，執政大臣周公忌父逃奔[虢國](../Page/虢國.md "wikilink")。\[2\]

前677年，周僖王崩。

在位期间执政为[虢公醜](../Page/虢公醜.md "wikilink")、[周公忌父](../Page/周公忌父.md "wikilink")。

## 子女

  - [周惠王](../Page/周惠王.md "wikilink")（？－前653年或前652），東周第五代君主，諡號惠王。
  - [姬虎](../Page/王叔文公.md "wikilink")（？－前624年），[王叔国始封者](../Page/王叔国.md "wikilink")，諡號文公。

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 周僖王                            | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前681年                          | 前680年                          | 前679年                          | 前678年                          | 前677年                          |
| [干支](../Page/干支.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") |

</div>

</div>

## 参考资料

  -
[Category:周朝君主](../Category/周朝君主.md "wikilink")
[周](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")

1.
2.  《左傳》莊公十六年