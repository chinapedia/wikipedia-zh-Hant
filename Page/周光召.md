**周光召**（），[湖南](../Page/湖南.md "wikilink")[长沙人](../Page/长沙.md "wikilink")。理论物理、粒子物理学家，[中国科学院院士](../Page/中国科学院院士.md "wikilink")，曾任第九屆[全国人大常委会副委员长](../Page/全国人大常委会.md "wikilink")、[中国科学院院长](../Page/中国科学院.md "wikilink")。是[中国共产党](../Page/中国共产党.md "wikilink")[第十二届](../Page/中国共产党第十二届中央委员会委员列表.md "wikilink")、[第十三届](../Page/中国共产党第十三届中央委员会委员列表.md "wikilink")、[第十四届](../Page/中国共产党第十四届中央委员会委员列表.md "wikilink")、[第十五届中央委员](../Page/中国共产党第十五届中央委员会委员列表.md "wikilink")。

## 生平

1942年，周光召进入重庆南开中学学习。家庭的熏陶和老师的教育不断开拓着他的视野，养成了他独立思考而又踏实进取的精神。在数学老师唐秀颖先生的影响下，他特别喜爱数学课，独辟蹊径地解开一个又一个数学难题。
1946年初，周光召回到长沙，同年秋季考入[清华大学先修班](../Page/清华大学.md "wikilink")。1947年转入清华大学物理系。

1951年毕业于[清华大学物理系](../Page/清华大学.md "wikilink")，从师于著名理论物理学家彭桓武教授，进行基本粒子物理的研究，1952年加入中国共产党。1954年毕业于[北京大学研究生院](../Page/北京大学.md "wikilink")。1957年赴[苏联](../Page/苏联.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")[杜布纳联合原子核研究所工作](../Page/杜布纳联合原子核研究所.md "wikilink")，周光召两次获得联合原子核研究所的科研奖金。1958年在国际上首先提出粒子的螺旋态振幅，并且建立了相应的数学方法。他是世界公认的赝矢量流部分守恒定理的奠基人之一。参加领导了爆炸物理、辐射流力学、高温高压物理、计算力学等研究工作。在中国第一颗原子弹和氢弹的理论设计中作出贡献。

1961年2月回国，历任[二机部第九研究院理论研究所副所长](../Page/二机部.md "wikilink")、所长，二机部九局总工程师，[中国科学院理论物理研究所研究员](../Page/中国科学院.md "wikilink")、副所长、所长。1960年代初开始核武器的理论研究工作，领导并参与了爆炸物理、辐射流体力学、高温高压物理、二维流体力学、中子物理等多个领域的研究工作，取得了许多具有实际价值的重要成果，为核武器的理论设计奠定了基础。1964年获得[国家自然科学奖一等奖](../Page/国家自然科学奖.md "wikilink")。

1979年8月，周光召重返理论物理研究领域，任中国科学院理论物理研究所研究员，在基本粒子和统计物理等方面均作出了相当的贡献。1984年4月任[中国科学院副院长](../Page/中国科学院.md "wikilink")。1985年获两项国家科技进步奖特等奖，1987年获中国科学院重大科技成果奖一等奖。1987年任[美国国家科学院外籍院士](../Page/美国国家科学院.md "wikilink")，1987年1月到1997年7月任[中国科学院院长](../Page/中国科学院.md "wikilink")。1992年当选为中国科学院学部委员会执行主席。1996年6月到2006年5月任[中国科学技术协会主席](../Page/中国科学技术协会.md "wikilink")，1998年3月到2003年3月任第九届[全国人大常委会副委员长](../Page/全国人大常委会.md "wikilink")。1998年12月获香港浸会大学荣誉理学博士学位。1999年9月与[钱学森](../Page/钱学森.md "wikilink")、[钱三強](../Page/钱三強.md "wikilink")、[邓稼先等共](../Page/邓稼先.md "wikilink")23人一起获得[两弹一星功勋奖章](../Page/两弹一星.md "wikilink")。

## 参考文献

## 外部链接

  - [周光召简历](http://news.xinhuanet.com/ziliao/2002-01/23/content_250843.htm)
  - [周光召：今天的科技界民主氛围太少](https://web.archive.org/web/20080923063037/http://www.sciencenet.cn/htmlnews/2007126114243337196043.html)

{{-}}

[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:中华人民共和国理论物理学家](../Category/中华人民共和国理论物理学家.md "wikilink")
[Category:中华人民共和国核物理学家](../Category/中华人民共和国核物理学家.md "wikilink")
[Category:中华人民共和国党和国家领导人
(中国科学院院士)](../Category/中华人民共和国党和国家领导人_\(中国科学院院士\).md "wikilink")
[Category:第九届全国人大常委会副委员长](../Category/第九届全国人大常委会副委员长.md "wikilink")
[Category:中华人民共和国时期中国共产党党员](../Category/中华人民共和国时期中国共产党党员.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:重庆市南开中学校友](../Category/重庆市南开中学校友.md "wikilink")
[Category:香港城市大學榮譽博士](../Category/香港城市大學榮譽博士.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:中国科学技术协会主席](../Category/中国科学技术协会主席.md "wikilink")
[Category:湖南科学家](../Category/湖南科学家.md "wikilink")
[Category:俄罗斯科学院外籍院士](../Category/俄罗斯科学院外籍院士.md "wikilink")
[Category:苏联科学院外籍院士](../Category/苏联科学院外籍院士.md "wikilink")
[Category:宁乡县人](../Category/宁乡县人.md "wikilink")
[G](../Category/周姓.md "wikilink")