**周兴铭**（），[浙江](../Page/浙江.md "wikilink")[余姚人](../Page/余姚.md "wikilink")，生于上海，中国计算机科学家。

## 生平

1956年8月考入[军事工程学院](../Page/军事工程学院.md "wikilink")。1961年毕业，并留校任教。1970年随学校南迁到长沙工作。

1986年任教授。1987—1993年任国防科技大学计算机研究所总工程师，1994—1996年任国防科技大学研究生院副院长，1997年任并行与分布处理国防科技重点实验室学术委员会主任，国防科技大学计算机学院顾问。

现任同济大学软件学院院长。

中国著名计算机专家和系统结构专家，[中国科学院](../Page/中国科学院.md "wikilink")[院士](../Page/院士.md "wikilink")，中国[银河系列超级计算机主要研制者之一](../Page/银河系列超级计算机.md "wikilink")，“银河—Ⅱ”总设计师，人称“中国巨型计算机之父”。\[1\]

## 参考文献

## 外部链接

  - [银河闪耀中国星——记计算机专家周兴铭](http://www.lovenudt.com/detail.asp?fileid=121)

[Category:中国计算机科学家](../Category/中国计算机科学家.md "wikilink")
[Category:中国人民解放军少将](../Category/中国人民解放军少将.md "wikilink")
[Category:国防科学技术大学教授](../Category/国防科学技术大学教授.md "wikilink")
[Category:同济大学教授](../Category/同济大学教授.md "wikilink")
[Category:中国计算机学会会士](../Category/中国计算机学会会士.md "wikilink")
[Category:余姚人](../Category/余姚人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Xing兴](../Category/周姓.md "wikilink")
[Category:CCF终身成就奖获得者](../Category/CCF终身成就奖获得者.md "wikilink")

1.  [周兴铭院士具体介绍](http://www.lovenudt.com/detail.asp?fileid=120)