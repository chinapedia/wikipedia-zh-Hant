**周匡王**（），[姓](../Page/姓.md "wikilink")**[姬](../Page/姬姓.md "wikilink")**，名**班**，[中国](../Page/中国.md "wikilink")[东周第](../Page/东周.md "wikilink")8代君主，前612年至前607年在位，共6年。匡王是[周頃王之子](../Page/周頃王.md "wikilink")。前607年十月，周王班崩，[諡](../Page/諡號.md "wikilink")“匡”，其弟王子瑜繼位，即[周定王](../Page/周定王.md "wikilink")。

在位期间执政为[周公閱](../Page/周公閱.md "wikilink")、[王孫蘇](../Page/王孫蘇.md "wikilink")、[召桓公](../Page/召桓公.md "wikilink")、[毛伯衛](../Page/毛伯衛.md "wikilink")。

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 周匡王                            | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前612年                          | 前611年                          | 前610年                          | 前609年                          | 前608年                          | 前607年                          |
| [干支](../Page/干支.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") |

</div>

</div>

## 參見

  -
[Category:周朝君主](../Category/周朝君主.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")
[周](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")