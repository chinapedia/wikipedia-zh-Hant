**周埈年**爵士（Sir **Tsun-nin
Chau**，），[香港富商及](../Page/香港.md "wikilink")[華人領袖](../Page/華人.md "wikilink")，曾為執業大律師，[第二次世界大戰前歷任過](../Page/第二次世界大戰.md "wikilink")[潔淨局局紳及](../Page/潔淨局.md "wikilink")[立法局非官守議員等職](../Page/立法局.md "wikilink")；戰後先後獲港府委任為立法局及[行政局的](../Page/行政局.md "wikilink")[首席非官守議員](../Page/首席非官守議員.md "wikilink")。周埈年亦曾任[廣東信託銀行董事長](../Page/廣東信託銀行.md "wikilink")，惟該行在1965年因[股災倒閉](../Page/1965年香港股災.md "wikilink")，引起經濟動盪。

## 生平

### 早年生涯

周埈年祖籍[廣東](../Page/廣東.md "wikilink")[東莞](../Page/東莞.md "wikilink")，1893年10月22日生於[香港](../Page/香港.md "wikilink")，乃曾任[立法局非官守議員的富商](../Page/立法局.md "wikilink")[周少岐](../Page/周少岐.md "wikilink")[太平紳士與庶室葉氏所出](../Page/太平紳士.md "wikilink")。周埈年雖然為周少歧的第七子，並有「七哥」之稱，但事實上他的六名長兄皆死於夭折。周埈年有一堂弟[周錫年爵士](../Page/周錫年.md "wikilink")，日後亦成為香港的行政、立法兩局非官守議員。

周埈年早年入讀[聖士提反書院](../Page/聖士提反書院.md "wikilink")，1910年畢業後；前往[英國](../Page/英國.md "wikilink")[牛津大學](../Page/牛津大學.md "wikilink")[王后學院修讀](../Page/牛津大學王后學院.md "wikilink")[法律](../Page/法律.md "wikilink")，終在1914年畢業，並且在[英國](../Page/英國.md "wikilink")[中院考取得執業](../Page/中院.md "wikilink")[大律師資格](../Page/大律師.md "wikilink")，及後又在牛津取得[文學碩士學位](../Page/文學碩士.md "wikilink")。

在1914年返港後，周埈年曾經執業數月，但不久就放棄大律師的工作，改為打理家族業務，開始從事[金融及](../Page/金融.md "wikilink")[保險工作](../Page/保險.md "wikilink")，很快就成為了華人社會的僑領之一。周埈年初年出任過安全火燭保險公司及香港九龍置業按揭公司的[總經理](../Page/總經理.md "wikilink")、以及裕安[輪船公司監督](../Page/輪船.md "wikilink")，後來又出任[油蔴地小輪公司](../Page/油蔴地小輪.md "wikilink")、[中華娛樂置業公司](../Page/中華娛樂置業公司.md "wikilink")、第一人壽保險公司及[廣東信託銀行等等的](../Page/廣東信託銀行.md "wikilink")[董事長](../Page/董事長.md "wikilink")；另外也是[香港電燈公司](../Page/香港電燈公司.md "wikilink")、[電話公司](../Page/香港電話有限公司.md "wikilink")、[中華百貨公司及](../Page/中華百貨.md "wikilink")[香港船廠公司等的](../Page/香港船廠公司.md "wikilink")[董事](../Page/董事.md "wikilink")。

### 公職生涯

作為華人領袖，周埈年在1922年獲港府委任為非官守[太平紳士](../Page/太平紳士.md "wikilink")，其後又於1929年獲委任為[潔淨局](../Page/潔淨局.md "wikilink")（[市政局前身](../Page/市政局.md "wikilink")）局紳。在1931年，周埈年獲港督[貝璐爵士委為立法局非官守議員](../Page/貝璐.md "wikilink")，並在同年12月10日正式宣誓，以填補[周壽臣爵士退休後遺下的空缺](../Page/周壽臣.md "wikilink")，成為[香港歷史上首位繼](../Page/香港歷史.md "wikilink")[父親](../Page/父親.md "wikilink")（即周少岐）成為立法局議員的人士。及至1937年1月，周埈年進一步取代[曹善允博士](../Page/曹善允.md "wikilink")，累遷成為立法局首席華人非官守議員。到1939年12月，由於已屆任期極限，周埈年退出立法局，而首席華人非官守議員一銜則由好友[羅文錦爵士接替](../Page/羅文錦.md "wikilink")。

香港在[第二次世界大戰期間曾經淪陷](../Page/第二次世界大戰.md "wikilink")，並經歷了三年零八個月的[日治時期](../Page/香港日治時期.md "wikilink")。日治時間完結後，香港自1945年9月至1946年4月一度由英國的臨時軍政府作出管理。臨時軍政府由於人手短缺，以及為安撫華人，周埈年在1945年接受軍政府臨時委任為[副華民政務司](../Page/華民政務司.md "wikilink")，並在[九龍辦公](../Page/九龍.md "wikilink")，以協助維持[重光後的政治局面](../Page/香港重光.md "wikilink")。

在1946年5月1日民政恢復後，周埈年被[港督](../Page/港督.md "wikilink")[楊慕琦爵士重新召到立法局供職](../Page/楊慕琦.md "wikilink")，重新擔任首席華人非官守議員，同年還加入[行政局成為首席華人非官守議員](../Page/行政局.md "wikilink")，使他一時成為戰後華人領袖之首。至1950年，他成為立法局首席非官守議員，並於1953年再一次從立法局榮休，由其堂弟[周錫年爵士接任首席非官守議員之位](../Page/周錫年.md "wikilink")。最後在1959年8月，他繼而退任行政局首席非官守議員之職。為了表彰其在行政局的貢獻，[英女皇](../Page/英女皇.md "wikilink")[伊利沙伯二世在他退出行政局後特地下諭旨](../Page/伊利沙伯二世.md "wikilink")，御准周埈年可繼續使用行政局議員尊享的「閣下」（The
Honourable）頭銜。這是繼[周壽臣爵士後](../Page/周壽臣.md "wikilink")，再次有華人行政局議員享有如此禮遇。

除了在兩局供職以外，周埈年亦曾任大量不同的公職，當中包括[東華三院永遠顧問](../Page/東華三院.md "wikilink")、[保良局永遠總理](../Page/保良局.md "wikilink")、[中華總商會名譽會董及顧問](../Page/中華總商會.md "wikilink")、[工展會名譽會長](../Page/工展會.md "wikilink")、[南華體育會名譽會長](../Page/南華體育會.md "wikilink")、[那打素醫院董事](../Page/那打素醫院.md "wikilink")、[香港聖約翰救傷會董事](../Page/香港聖約翰救傷會.md "wikilink")、保護兒童會值理、東莞工商總會主席及[童軍總會香港分會副會長等職](../Page/香港童軍.md "wikilink")。此外，他也曾是[聖士提反書院校董及校友會主席](../Page/聖士提反書院.md "wikilink")、亦是東莞義學校長。而早在戰前的時候，周埈年已自1931年起出任[香港大學校董會成員](../Page/香港大學.md "wikilink")，他後在1961年港大金禧時獲頒榮譽法學博士學位。

為答謝其長年對社會事務的貢獻，周埈年在1938年元旦獲英皇[喬治六世授予](../Page/喬治六世.md "wikilink")[CBE勳銜](../Page/CBE.md "wikilink")；後在1956年5月，他復於[英女皇壽辰榮譽名單中獲勳為](../Page/英女皇壽辰.md "wikilink")[爵士](../Page/爵士.md "wikilink")。

### 晚年

在1965年2月6日，[廣東信託銀行發生擠提事件](../Page/廣東信託銀行.md "wikilink")，兩日後廣東信託銀行全線停業，並波及多間華資銀行爆發擠提，導致[1965年香港股災](../Page/1965年香港股災.md "wikilink")。事件中，廣東信託銀行宣告倒閉，而[恆生指數也因這次股災急挫四分之一](../Page/恆生指數.md "wikilink")。周埈年雖身為廣東信託銀行董事長，不過為了自己聲譽，他在股災爆發後立即發表聲明，指到該行一切業務皆由經理部管理，並澄清自己沒有動用銀行的分毫款項，因此並不涉及擠提事件。

周埈年在1968年突然因[血栓塞性脈管炎而被送院進行手術](../Page/血栓塞性脈管炎.md "wikilink")，但未幾因情況轉好而返家休養，此後亦減少出席公開場合。至1970年12月12日，周埈年因為舊病復發而被送入[養和醫院](../Page/養和醫院.md "wikilink")，期間每下愈況，最終在1971年1月27日中午12時05分逝世，終年77歲。在他過世那天，正是[農曆](../Page/農曆.md "wikilink")[大年初一](../Page/大年初一.md "wikilink")。

周埈年身後，其遺體在同年1月31日於[香港殯儀館舉行](../Page/香港殯儀館.md "wikilink")[大殮](../Page/大殮.md "wikilink")，並由生前六位好友[關祖堯爵士](../Page/關祖堯.md "wikilink")、[羅理基爵士](../Page/羅理基.md "wikilink")、[周錫年爵士](../Page/周錫年.md "wikilink")、[簡悅強議員](../Page/簡悅強.md "wikilink")、莫應基及盧義明扶靈。遺體隨後安葬於[香港仔華人永遠墳場](../Page/香港仔華人永遠墳場.md "wikilink")。

## 家庭

周埈年有一[元配蘇氏](../Page/元配.md "wikilink")，後來又在1931年迎娶梁彥玲（Elaine
Leung）為妻，梁彥玲婚後冠夫姓為周梁彥玲。周埈年與周梁彥玲共有四子一女，四名兒子依次分別叫長子[周湛霖](../Page/周湛霖.md "wikilink")、次子[周湛燊](../Page/周湛燊.md "wikilink")、三子[周湛樵及幼子](../Page/周湛樵.md "wikilink")[周湛煌](../Page/周湛煌.md "wikilink")。周梁彥玲曾在1947年12月11日獲委為非官守[太平紳士](../Page/太平紳士.md "wikilink")，她在1984年2月12日於[養和醫院逝世](../Page/養和醫院.md "wikilink")，[積閨享壽](../Page/積閨.md "wikilink")77歲。

## 榮譽

### 勳銜

[Memorial_of_Dr._Sun_Yat-sen.jpg](https://zh.wikipedia.org/wiki/File:Memorial_of_Dr._Sun_Yat-sen.jpg "fig:Memorial_of_Dr._Sun_Yat-sen.jpg")的[孫逸仙博士紀念碑](../Page/中山公園與青山紅樓.md "wikilink")，碑正下方有周埈年爵士所撰370餘字的碑文。\]\]

  - [太平紳士](../Page/太平紳士.md "wikilink") （1922年11月23日）
  - [英帝國司令勳章](../Page/英帝國司令勳章.md "wikilink")（C.B.E.）（1938年元旦授勳名單）
  - [下級勳位爵士](../Page/下級勳位爵士.md "wikilink") （K.t.）（1956年女皇壽辰授勳名單）

### 榮譽學位

  - **榮譽法學博士**
      - [香港大學](../Page/香港大學.md "wikilink") （1961年）

## 相關條目

  - [周永泰家族](../Page/周永泰家族.md "wikilink")
  - [周少岐](../Page/周少岐.md "wikilink")
  - [周錫年](../Page/周錫年.md "wikilink")

## 參考資料

<div class="references-small">

  - *Who Was Who*, London: A & C Black, 1996.
  - *[OFFICIAL REPORT OF
    PROCEEDINGS](http://www.legco.gov.hk/1931/h311210.pdf)*, Hong Kong:
    HONG KONG LEGISLATIVE COUNCIL, 10 December, 1931.
  - *[OFFICIAL REPORT OF
    PROCEEDINGS](http://www.legco.gov.hk/1937/h370203.pdf)*, Hong Kong:
    HONG KONG LEGISLATIVE COUNCIL, 3 February, 1937.
  - *[OFFICIAL REPORT OF
    PROCEEDINGS](http://www.legco.gov.hk/1939/h391207.pdf)*, Hong Kong:
    HONG KONG LEGISLATIVE COUNCIL, 7 December, 1939.
  - 鄧志清著，《港澳聞人錄》，香港：Hong Kong Associated Press，1957年。
  - "[Sir Tsun-nin
    Chau](http://www3.hku.hk/eroonweb/hongrads/person_c.php?id=18)",
    *Honorary Graduates*, Hong Kong: University of Hong Kong, 1961.
  - 岑維休主編，《香港年鑑》，香港：華僑日報，1969年。
  - 〈周埈年爵士退休後仍保留議員稱號〉，《[工商日報](../Page/工商日報.md "wikilink")》第六頁，1959年8月8日。
  - 〈周埈年爵士年初一病逝〉，《工商日報》第五頁，1971年1月29日。
  - 〈周埈年爵士定今午舉殯〉，《工商日報》第五頁，1971年1月31日。
  - 〈周埈年爵士舉殯哀榮〉，《工商日報》第五頁，1971年2月1日。
  - 〈已故周埈年爵士之夫人，周母梁太夫人舉殯哀榮〉，《[華僑日報](../Page/華僑日報.md "wikilink")》第六章第四頁，1984年2月16日。

</div>

## 外部連結

  - [香港大學贊辭](http://www3.hku.hk/eroonweb/hongrads/person_c.php?id=18)，1961年

[Category:前香港立法局議員](../Category/前香港立法局議員.md "wikilink")
[Category:前香港行政局議員](../Category/前香港行政局議員.md "wikilink")
[Category:香港法律界人士](../Category/香港法律界人士.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[周](../Category/香港東莞人.md "wikilink")
[T](../Category/周姓.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:牛津大學王后學院校友](../Category/牛津大學王后學院校友.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:聖士提反書院校友](../Category/聖士提反書院校友.md "wikilink")