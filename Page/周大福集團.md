**周大福集團**是香港商人[鄭裕彤家族擁有的商業集團](../Page/鄭裕彤.md "wikilink")，旗下主要上市公司包括[新世界發展](../Page/新世界發展.md "wikilink")（）、[周大福珠寶集團](../Page/周大福珠寶集團.md "wikilink")（）、[新世界百貨中國](../Page/新世界百貨中國.md "wikilink")（）和[新創建](../Page/新創建.md "wikilink")。

## 周大福企業

[Chow_Tai_Fook_in_Aon_China_Building_201705.jpg](https://zh.wikipedia.org/wiki/File:Chow_Tai_Fook_in_Aon_China_Building_201705.jpg "fig:Chow_Tai_Fook_in_Aon_China_Building_201705.jpg")\]\]
[Chow_Tai_Fook_concept_store_in_YOHO_Mall_2017.jpg](https://zh.wikipedia.org/wiki/File:Chow_Tai_Fook_concept_store_in_YOHO_Mall_2017.jpg "fig:Chow_Tai_Fook_concept_store_in_YOHO_Mall_2017.jpg")周大福體驗店以禮物盒為主題，由One
Plus設計\]\]
[HK_TST_Nathan_Road_Park_Lane_Shopper's_Boulevard_周大福_Chow_Tai_Fook.JPG](https://zh.wikipedia.org/wiki/File:HK_TST_Nathan_Road_Park_Lane_Shopper's_Boulevard_周大福_Chow_Tai_Fook.JPG "fig:HK_TST_Nathan_Road_Park_Lane_Shopper's_Boulevard_周大福_Chow_Tai_Fook.JPG")的周大福分店\]\]
[HK_Kennedy_Town_Bus_Terminus_Chow_Tai_Fook_Group_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Kennedy_Town_Bus_Terminus_Chow_Tai_Fook_Group_a.jpg "fig:HK_Kennedy_Town_Bus_Terminus_Chow_Tai_Fook_Group_a.jpg")及[城巴車身上的周大福珠寶首飾廣告](../Page/城巴.md "wikilink")\]\]
[Chow_Tai_Fook_advertisement_on_a_Austal_catamaran_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Chow_Tai_Fook_advertisement_on_a_Austal_catamaran_\(Hong_Kong\).jpg "fig:Chow_Tai_Fook_advertisement_on_a_Austal_catamaran_(Hong_Kong).jpg")「新輪捌拾貳」號，後轉售予「[噴射飛航](../Page/噴射飛航.md "wikilink")」。\]\]
現時持有在香港上市的[新世界發展有限公司](../Page/新世界發展.md "wikilink")44.24%控制性股權，股價市值超過30億美元；集團控制及管理在香港、中國及世界各地不同性質的業務，包括有酒店、房地產、設施管理、建築、百貨零售、公共交通、電訊、道路網、自來水、電力及金融服務業，控制公司包括[新世界第一巴士](../Page/新世界第一巴士.md "wikilink")、[城巴](../Page/城巴.md "wikilink")、[新創建及](../Page/新創建.md "wikilink")[協興建築等](../Page/協興建築.md "wikilink")。

2011年5月9日，以平均價每股4.81元購入佐丹奴2.18億股，斥資10.5億元購入[佐丹奴](../Page/佐丹奴.md "wikilink")14.58%股權，成為最大單一股東。

2011年7月20日，入股倫敦社會保障房管理公司Pinnacle 42.67%股權。

2012年5月27日周大福總投資逾100億人民幣在珠江新城興建[廣州周大福金融中心](../Page/廣州周大福金融中心.md "wikilink")（廣州東塔），項目總投資逾100億（人民幣，下同），樓高約530米，建成後將是華南第一高樓，地皮於2008年由周大福集團以15.5億投得，擬建設施包括五星級酒店、國際化公寓、國際標準甲級寫字樓和商場。

2012年1月底，周大福珠寶向CTF
Holding發行約1.04億股股份，涉及金額約104.25億元，完成後已繳股本金額增至120億元，但股權架構不變。

2014年10月31日，周大福企業，於場外以每股均價0.09美元，買入4.96億股[綠森集團股份](../Page/綠森集團.md "wikilink")，涉資逾4,465萬美元（約3.5億元），周大福企業持好倉62.82%，成為綠森大股東。

2014年1月15日周大福企業與非洲銀行Investec合營的飛機出租財團以5.07億美元（約39.54億港元）作價，向科威特廉航Jazeera
Airways購入15架空中巴士A320客機。

2015年1月27日，[新世界發展以](../Page/新世界發展.md "wikilink")17.79億現金向大股東周大福企業購入其持有的北角吉祥大廈40%業權，令新世界持有吉祥大廈的業權升至90%，另外10%權益由該公司若干附屬公司的主要股東Good
Step持有，該地盤位於[英皇道](../Page/英皇道.md "wikilink")704及706號、健康東街14號與禮信街1號，以及英皇道708及710號與禮信街3及5號，整個項目包含14幢舊樓合共504個業權，地盤總面積約32,500方呎，若以商業發展最高地積比15倍計，可建樓面面積約487,500方呎。

2015年12月29日周大福企業於場外以11.99元認購[銀河證券](../Page/銀河證券.md "wikilink")1.98億股，涉資23.7億元，持股量5.36%。

2015年12月29日[恆大向周大福企業以及](../Page/恆大.md "wikilink")[新世界中國](../Page/新世界中國.md "wikilink")，收購位於四川、青島、上海及北京等6個地產項目，涉資達204億元人民幣，包括131億元向周大福企業收購四個地產項目。

2017年3月15日周大福企業以40億澳元（31億美元／約238.9億港元）收購西[澳州最大的電力零售商Alinta](../Page/澳州.md "wikilink")
Energy Holdings Ltd.100%股權。

2017年3月15日周大福企業全購維多利亞教育機構（VEO）（維多利亞幼稚園）8所分校，包括下康、中康、上康、海峰園、海怡、寶翠、何文田及君匯港校舍，並由明年1月31日起接手經營。

2018年4月17日周大福企業與遠東發展分別出資逾2.45億澳元（約14.78億港元），各認購澳洲博彩營運商Star Entertainment
45825萬股，成為該公司具重要性的股東，兩間企業的持股量分別為4.99%

## 周大福控股旗下投資

  - [新世界發展](../Page/新世界發展.md "wikilink")44.41%

  - [周大福珠寶集團](../Page/周大福珠寶集團.md "wikilink")89.3%

  - [周大福能源公司](../Page/周大福能源公司.md "wikilink")100%

  -
  - [Star Entertainment](../Page/Star_Entertainment.md "wikilink")4.99%

  - [中国平安保险](../Page/中国平安保险.md "wikilink")3.4% 194.48億港元

  - [佐丹奴](../Page/佐丹奴.md "wikilink")25.01%

  - [綜合環保](../Page/綜合環保.md "wikilink")46.93%

  - [新昌營造](../Page/新昌營造.md "wikilink")7.71%

  - [聯合醫務](../Page/聯合醫務.md "wikilink")20%

  - [新時代能源](../Page/新時代能源.md "wikilink")29.18%

  - [盛京銀行](../Page/盛京銀行.md "wikilink")3.41%

  - [澳門旅遊娛樂](../Page/澳門旅遊娛樂.md "wikilink")9.6%

  - [澳博控股](../Page/澳博控股.md "wikilink")0.10%

  - [銀基集團](../Page/銀基集團.md "wikilink")8.07%

  - [恒實礦業](../Page/恒實礦業.md "wikilink")

  - 維多利亞教育機構（維多利亞幼稚園）

  - 聯合醫務15%

  - [環球醫療](../Page/環球醫療.md "wikilink")5.7%

  - [耀才證券](../Page/耀才證券.md "wikilink")8.92%

  - [上海四季酒店](../Page/上海四季酒店.md "wikilink")77%11.64億港元

  - 青島唐島灣項目50%11.83億人民幣

  - 上海青浦土地F及G地塊90%24.32億人民幣

  - [武漢周大福金融中心](../Page/武漢周大福金融中心.md "wikilink")

  - [廣州周大福金融中心](../Page/廣州周大福金融中心.md "wikilink")

  - [天津周大福金融中心](../Page/天津周大福金融中心.md "wikilink")

  - [千禧新世界香港酒店](../Page/千禧新世界香港酒店.md "wikilink")

  - 澳洲昆士蘭布里斯班Queen's Wharf Brisbane娛樂城及綜合度假村開發項目25%

  - 前海（香港）全球商品購物中心

  - [周大福商業中心](../Page/周大福商業中心.md "wikilink")（潮流特區）

  - 巴哈馬Baha Mar度假村

  - 澳洲布里斯本皇后碼頭綜合度假村

  - 會安南綜合度假村

## 周大福珠寶

[ChowTaiFook_logo.svg](https://zh.wikipedia.org/wiki/File:ChowTaiFook_logo.svg "fig:ChowTaiFook_logo.svg")
主要經營[周大福珠寶集團之](../Page/周大福珠寶集團.md "wikilink")[珠寶](../Page/珠寶.md "wikilink")[首飾零售及製作業務](../Page/首飾.md "wikilink")，在大中華區珠寶首飾行業裡，每年銷售額佔市場第一位。周大福同時亦為全球最大鑽石胚供應商
De Beers
Group的指定交易商之一，獲直接配售天然鑽石原胚。周大福於[順德及香港亦設有首飾加工廠房](../Page/順德.md "wikilink")，員工3000多人，每年生產首飾超過250萬件。

周大福是香港的一間著名的[珠寶](../Page/珠寶.md "wikilink")[零售商](../Page/零售.md "wikilink")，由[周至元](../Page/周至元_\(香港企業家\).md "wikilink")1929年在[廣州](../Page/廣州.md "wikilink")[河南](../Page/河南_\(廣州\).md "wikilink")[洪德路創立](../Page/洪德路.md "wikilink")，1938年及1939年分別在[澳門及](../Page/澳門.md "wikilink")[香港開業](../Page/香港.md "wikilink")，並於1947年由[女婿鄭裕彤接手經營至今](../Page/女婿.md "wikilink")，目前共有逾2,500間店舖。

周大福是中國大陸及港澳地區的最大珠寶零售商，2010年在內地的市場占有率為12.6%，港澳地區為20.1%。60%是自營店舖，餘下近30%是特許經營，20%是合營。

2011年11月25日，周大福珠寶集團有限公司於香港交易所上市。

2014年6月18日，周大福珠寶集團宣布，將以現金總代價1.5億美元，收購美國著名鑽飾公司。

2017年11月7日，周大福官方微博宣布中國女演員[趙麗穎出任該集團的首位形象大使](../Page/趙麗穎.md "wikilink")。

## 店舖分佈

截至2018年9月30日，周大福於中國內地開設2,682個零售點，香港、澳門以及其他市場開設140個零售點。

## 爭議

  - 周大福於[柬埔寨拍廣告](../Page/柬埔寨.md "wikilink")，用貧窮國度的不幸兒童來反襯新人幸福，被市民狠批\[1\]。
  - 周大福珠寶公司公關副總監在社交網站上發表侮辱女性言論，引來市民嚴厲抗議，更有市民表示要罷買其公司的產品\[2\]。

## 參見

  - [新世界發展](../Page/新世界發展.md "wikilink")

## 注釋

## 參考文獻

## 外部連結

  - [周大福珠寶集團網站](http://corporate.chowtaifook.com/tc/global/home.php)

  - [周大福珠寶時尚網站](http://www.ctflife.com)

  - [周大福（香港）網絡旗艦店](https://www.ctfeshop.com.hk/)

  -
  -
  -
  -
[Category:中国珠宝公司](../Category/中国珠宝公司.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:香港綜合企業公司](../Category/香港綜合企業公司.md "wikilink")
[Category:周大福集團](../Category/周大福集團.md "wikilink")
[Category:1929年成立的公司](../Category/1929年成立的公司.md "wikilink")
[Category:廣州歷史](../Category/廣州歷史.md "wikilink")
[Category:香港家族式企業](../Category/香港家族式企業.md "wikilink")
[Category:鄭裕彤家族](../Category/鄭裕彤家族.md "wikilink")

1.
2.