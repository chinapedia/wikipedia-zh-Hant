**周宜霈**（），舊藝名**大牙**，畢業於[華岡藝校表演藝術科](../Page/華岡藝校.md "wikilink")。原為饒舌團體「地下國度」的成員，因為參加電視節目《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》中表現出色，被選為[黑Girl第一代成員之一](../Page/黑Girl.md "wikilink")。她同時是[黑Girl隊長](../Page/黑Girl.md "wikilink")，外號「性感美眉」。曾是臺灣[Channel
V娛樂台的VJ](../Page/Channel_V.md "wikilink")、《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》助教，是節目中唯一能升為VJ的美眉，並且是第一位奪得三次優勝的美眉。

2010年3月17日，**大牙**因為加盟[傳奇星娛樂並宣佈退出](../Page/傳奇星娛樂.md "wikilink")**黑Girl**。2014年約滿後轉到[重感情創意工作室](../Page/重感情創意工作室.md "wikilink")。2014年起，**大牙**開始以本名**周宜霈**出席綜藝節目，2015年演藝重心轉為戲劇演出，代表作為[1989一念間](../Page/1989一念間.md "wikilink")。

## 个人背景

### 学历

  - 畢業於[華岡藝校表演藝術科](../Page/華岡藝校.md "wikilink")

## 经历

### 2010年：合约期满，退出团体黑Girl、加入新公司

3月17日，**大牙**因為加盟[傳奇星娛樂並宣佈退出](../Page/傳奇星娛樂.md "wikilink")**黑Girl**。

### 2014年：合约期满，加入新公司

約滿後轉到[重感情創意工作室](../Page/重感情創意工作室.md "wikilink")。2014年起，**大牙**開始以本名**周宜霈**出席綜藝節目。

### 2017年：出演电视剧《一家人》

以演藝重心轉為戲劇演出，代表作為《[一家人](../Page/一家人.md "wikilink")》。

### 2018年：出演电视剧《金家好媳婦》

劇中飾演用盡心機搶人男友的**趙映心**。

### 2019年：出演电视剧《炮仔聲》

劇中飾演碩士畢業的吳家女兒**吳家琇**。

## 音樂作品

### 創作

|            |                                                  |                           |      |
| ---------- | ------------------------------------------------ | ------------------------- | ---- |
| 發行日期       | 專輯                                               | 作品                        | 負責項目 |
| 2008年8月29日 | [黑Girl首張同名專輯](../Page/黑Girl首張同名專輯.md "wikilink") | 哈庫吶瑪塔塔 Hakuna Matata\[1\] | 填詞   |
| 2010年6月29日 | 未收錄                                              | 雨天 Rainy Day\[2\]         |      |
| 2010年7月5日  | 一個人 Alone\[3\]                                   |                           |      |
| 2010年7月6日  | 而，我們 Now, Us\[4\]                                |                           |      |
| 2011年5月17日 | 敲敲門 Knock On The Door\[5\]                       |                           |      |

## 影視作品

### 电视剧

|                                      |                                                                     |                                             |         |                                                                 |
| ------------------------------------ | ------------------------------------------------------------------- | ------------------------------------------- | ------- | --------------------------------------------------------------- |
| 首播日期                                 | 播出頻道                                                                | 劇名                                          | 飾演      | 集數                                                              |
| 2006年                                | [衛視中文台](../Page/衛視中文台.md "wikilink")                                | [天使情人](../Page/天使情人.md "wikilink")          | 小君      | 第1、26集                                                          |
| 2007年                                | [民視](../Page/民視.md "wikilink")、[衛視中文台](../Page/衛視中文台.md "wikilink") | [黑糖瑪奇朵](../Page/黑糖瑪奇朵.md "wikilink")        | 大牙（羅莉凱） | 全部                                                              |
| 2007年10月6日                           | woo.com (網路劇)                                                       | [黑糖來了](../Page/黑糖來了.md "wikilink")          | 大牙      |                                                                 |
| 2007                                 | [公視](../Page/公視.md "wikilink")                                      | [我在墾丁\*天氣晴](../Page/我在墾丁*天氣晴.md "wikilink") | 小雁      | 第1、2、6集                                                         |
| 2008年                                | [衛視中文台](../Page/衛視中文台.md "wikilink")                                | [黑糖群俠傳](../Page/黑糖群俠傳.md "wikilink")        | 邪教護法    | 第13集                                                            |
| 2011年                                | [臺視](../Page/臺視.md "wikilink")、[三立都會台](../Page/三立都會台.md "wikilink") | [勇士們](../Page/勇士們_\(電視劇\).md "wikilink")    | 王靜雯     | 第10\~15集                                                        |
| 2015年                                | [中視](../Page/中視.md "wikilink")                                      | [金魚媽媽](../Page/金魚媽媽.md "wikilink")          | 葉凱琪     | 全劇                                                              |
| 2015年                                | [臺視](../Page/臺視.md "wikilink")、[三立都會台](../Page/三立都會台.md "wikilink") | [他看她的第2眼](../Page/他看她的第2眼.md "wikilink")    | 吳智琳     | 全劇                                                              |
| 2015年                                | [愛奇藝](../Page/愛奇藝.md "wikilink")                                    | [星空中的潘朵拉](../Page/星空中的潘朵拉.md "wikilink")    | 杨欣      | 网剧                                                              |
| 2016年                                | [三立都會台](../Page/三立都會台.md "wikilink")                                | [1989一念間](../Page/1989一念間.md "wikilink")    | 胡麗菁     | 第2\~4集，6\~9集，11\~15集，18-19集                                     |
| [飛魚高校生](../Page/飛魚高校生.md "wikilink") | 黑茉莉                                                                 | 第3\~5集，10\~12集，14\~17集                      |         |                                                                 |
| 2017年                                | [三立台灣台](../Page/三立台灣台.md "wikilink")                                | [一家人](../Page/一家人.md "wikilink")            | 呂媜兒     | 第63集初登場、第64集、第68集、第70集、第72集、第73集、第75集、第78集、第79集、第80集、第81集至第150集 |
| [公視](../Page/公視.md "wikilink")       | \-{R|[乒乓](../Page/乒乓_\(電視電影\).md "wikilink")}-                      | 毒女                                          |         |                                                                 |
| 2018年                                | [三立台灣台](../Page/三立台灣台.md "wikilink")                                | [金家好媳婦](../Page/金家好媳婦.md "wikilink")        | 趙映心     | 女配角                                                             |
| 2019年                                | [三立台灣台](../Page/三立台灣台.md "wikilink")                                | [天之蕉子](../Page/天之蕉子.md "wikilink")          | 柯美娟     | 第二女主角                                                           |
|                                      |                                                                     |                                             |         |                                                                 |

### 音乐录影带(MV)

[潘瑋柏](../Page/潘瑋柏.md "wikilink")：

  - 2004年《Do That to Me One More Time》

Zayin：

  - 2006年《宇宙》

## 配音作品

  - 《[快樂腳](../Page/快樂腳.md "wikilink")》（聲音演出－葛莉亞，2007）
  - 《[-{zh:辛普森一家;zh-hans:辛普森一家;zh-hant:辛普森一家;zh-cn:辛普森一家;zh-tw:辛普森家庭;zh-hk:阿森一族;zh-mo:阿森一族;}-電影版](../Page/辛普森一家.md "wikilink")》（聲音演出-霸子，2007）

## 书籍作品

### 寫真書

|            |                                                            |      |                       |
| ---------- | ---------------------------------------------------------- | ---- | --------------------- |
| 發行日期       | 作品                                                         | 出版社  | 作者                    |
| 2013年1月23日 | [偷窺24hr.×周宜霈大牙寫真集](../Page/偷窺24hr.×周宜霈大牙寫真集.md "wikilink") | 推守文化 | 大牙(周宜霈)/著；Leaf Yeh/攝影 |

## 节目主持

  - [中視](../Page/中視.md "wikilink")
      - 《金曲超級星-愛秀大明星》（已停播）

<!-- end list -->

  - [三立都會台](../Page/三立都會台.md "wikilink")
      - 《[愛玩客](../Page/愛玩客.md "wikilink")》（與[小鐘搭檔主持](../Page/小鐘.md "wikilink")）（已退出）

<!-- end list -->

  - [中天綜合台](../Page/中天綜合台.md "wikilink")
      - 《[台灣腳逛大陸](../Page/台灣腳逛大陸.md "wikilink")》

<!-- end list -->

  - [TVBS-G](../Page/TVBS-G.md "wikilink")
      - 《娛樂新聞 ─ 美眉ㄅㄠˋㄅㄠˋ》

<!-- end list -->

  - [Channel V](../Page/Channel_V.md "wikilink")
      - 《[我愛黑澀棒棒堂](../Page/我愛黑澀棒棒堂.md "wikilink")》
      - 《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》
      - 《[音樂飆榜](../Page/音樂飆榜_\(台灣\).md "wikilink")》
      - 《[VJ普普風](../Page/VJ普普風.md "wikilink")》
      - 《〔V〕好機會》

<!-- end list -->

  - [四川衛視](../Page/四川衛視.md "wikilink")
      - 《給力男子漢》

<!-- end list -->

  - [中華電信MOD](../Page/中華電信MOD.md "wikilink")
      - 《潮流瘋》

<!-- end list -->

  - 香港 [now寬頻電視](../Page/now寬頻電視.md "wikilink")
      - 《18 22》（與
        [黑人](../Page/陳建州.md "wikilink")、[Apple搭檔主持](../Page/黃暐婷.md "wikilink")）

## 節目出演

  - [歡樂智多星](../Page/歡樂智多星.md "wikilink")
    ([衛視中文台](../Page/衛視中文台.md "wikilink"))
  - [冠軍任務](../Page/冠軍任務.md "wikilink")
    ([衛視中文台](../Page/衛視中文台.md "wikilink"))
  - [綜藝大熱門](../Page/綜藝大熱門.md "wikilink")
    ([三立都會台](../Page/三立都會台.md "wikilink"))
  - [綜藝玩很大](../Page/綜藝玩很大.md "wikilink")
    ([中國電視公司](../Page/中國電視公司.md "wikilink").[三立都會台](../Page/三立都會台.md "wikilink"))
  - [小明星大跟班](../Page/小明星大跟班.md "wikilink")
    ([中天綜合台](../Page/中天綜合台.md "wikilink").[中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [飢餓遊戲](../Page/飢餓遊戲_\(電視節目\).md "wikilink")
    ([中國電視公司](../Page/中國電視公司.md "wikilink"))
  - [麻辣天后傳](../Page/麻辣天后傳.md "wikilink")
    ([中天綜合台](../Page/中天綜合台.md "wikilink"))
  - [天天樂財神](../Page/天天樂財神.md "wikilink")
    ([中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [碰碰發財星](../Page/碰碰發財星.md "wikilink")
    ([中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [真的？假的](../Page/真的？假的.md "wikilink")
    ([中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [天才衝衝衝](../Page/天才衝衝衝.md "wikilink")
    ([中華電視公司](../Page/中華電視公司.md "wikilink"))
  - [一袋女王](../Page/一袋女王.md "wikilink")
    ([衛視中文台](../Page/衛視中文台.md "wikilink"))

## 广告代言

### 廣告

  - 芬達凍感10下
  - FIFA ONLINE2
  - Edwin牛仔褲
  - 新蜀山劍俠Online
  - Hi-Chew「嗨啾」軟糖
  - 「潘朵拉的甜蜜衣櫥」少女服飾
  - 爭鮮迴轉壽司
  - KnightsBridge少女服飾
  - Yuskin化妝品
  - 婦潔《 VIGILL 除毛系列》

### 活動

  - [The Body
    Shop](../Page/The_Body_Shop.md "wikilink")[美體小舖義賣活動](../Page/美體小舖.md "wikilink")(2007/9/1)
  - 新萃研agnes b. Lolita彩妝新品上市發表會

## 参考文献

## 外部連結

  -
  -
  -
[en:Hey Girl (group)\#Tina Chou (Da Ya) (Old
Leader)](../Page/en:Hey_Girl_\(group\)#Tina_Chou_\(Da_Ya\)_\(Old_Leader\).md "wikilink")

[C周](../Category/台灣綜藝節目主持人.md "wikilink")
[C周](../Category/台灣女歌手.md "wikilink")
[C周](../Category/台灣電影女演員.md "wikilink")
[C周](../Category/台灣電视女演員.md "wikilink")
[C周](../Category/我愛黑澀會.md "wikilink")
[Y宜](../Category/周姓.md "wikilink")
[C周](../Category/臺北市私立華岡藝術學校校友.md "wikilink")
[C周](../Category/黑Girl.md "wikilink")

1.
2.
3.
4.
5.