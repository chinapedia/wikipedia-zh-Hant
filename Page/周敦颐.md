[庐山白鹿洞书院周敦颐铜像.JPG](https://zh.wikipedia.org/wiki/File:庐山白鹿洞书院周敦颐铜像.JPG "fig:庐山白鹿洞书院周敦颐铜像.JPG")周敦颐铜像\]\]
**周敦颐**（），又名**周元皓**，原名**敦实**，字**茂叔**，號**濂溪**，又稱**濂溪先生**。\[1\]道州营道县（今[湖南](../Page/湖南.md "wikilink")[道县](../Page/道县.md "wikilink")）人，北宋官員、學者，是[北宋](../Page/北宋.md "wikilink")[宋明理学理論基礎的創始人之一](../Page/宋明理学.md "wikilink")。\[2\]

## 生平

[Zhou_Dunyi_and_lotus.JPG](https://zh.wikipedia.org/wiki/File:Zhou_Dunyi_and_lotus.JPG "fig:Zhou_Dunyi_and_lotus.JPG")
周敦颐生於一[官僚](../Page/官僚.md "wikilink")[地主家庭](../Page/地主.md "wikilink")，年幼喪父，北宋[天圣三年](../Page/天圣.md "wikilink")（1025），8岁时，母親帶他投靠[衡阳](../Page/衡阳.md "wikilink")[舅父](../Page/舅父.md "wikilink")[鄭向](../Page/鄭向.md "wikilink")(郑家故宅后改为濂溪周氏宗祠，在今[南华大学附一医院处](../Page/南华大学.md "wikilink"))，宋仁宗天圣九年（1031年），周敦颐随其母到京师开封。
1037年郑向调任[两浙转运使](../Page/两浙.md "wikilink")，周敦颐同母随迁[润州丹徒县](../Page/润州.md "wikilink")(今江苏[镇江市丹徒区](../Page/镇江.md "wikilink"))。鄭向見他聰穎好學，便栽培他唸書，敦颐知識廣泛，博取眾家之長，融會貫通，成一家之言。由于任[宋仁宗朝中的龙图阁学士的舅舅郑向的推荐](../Page/宋仁宗.md "wikilink")，做了[分宁县](../Page/分宁县.md "wikilink")（修水）的主簿，后调任到[南安军担任司理参军](../Page/南安.md "wikilink")。

宋仁宗庆历六年（1046年），[大理寺丞程珦在南安认识了周敦颐](../Page/大理寺.md "wikilink")，见他“气貌非常人”，与之交谈，更知其“为学知道”，同他结为朋友，随即将两个儿子[程颢](../Page/程颢.md "wikilink")、[程颐送至南安拜敦颐为师受业](../Page/程颐.md "wikilink")。后移[桂阳令](../Page/桂阳.md "wikilink")，徙知[南昌](../Page/南昌.md "wikilink")，历[合州判官](../Page/合州.md "wikilink")、[虔州通判](../Page/虔州.md "wikilink")。[熙宁初知](../Page/熙宁.md "wikilink")[郴州](../Page/郴州.md "wikilink")，擢广东转运判官，提点刑狱。后知[南康军](../Page/南康军.md "wikilink")，治所在今[星子县](../Page/星子县.md "wikilink")。筑室[庐山莲花峰下](../Page/庐山.md "wikilink")，前有溪，取营道故居濂溪以名之，遂定居于此，并将原在故里的母亲郑木君墓迁葬于庐山清泉社三起山。周敦颐卒，亦附葬于母亲墓旁。

[熙宁六年六月初七](../Page/熙宁.md "wikilink")（1073年7月24日星期日）病逝，死後神宗賜[諡](../Page/諡.md "wikilink")「元」，人稱「元公」。弟子[程颢](../Page/程颢.md "wikilink")、[程颐继承和完善了他的思想](../Page/程颐.md "wikilink")。后来经过著名学者[朱熹的进一步的发展](../Page/朱熹.md "wikilink")，成为[理学](../Page/宋明理學.md "wikilink")。轍是[孔子](../Page/孔子.md "wikilink")、[孟子之后](../Page/孟子.md "wikilink")[儒学最重要的发展](../Page/儒学.md "wikilink")，在中国思想史上的影响深远。《[宋元學案](../Page/宋元學案.md "wikilink")》中对于周敦颐的地位有这样的论述："孔孟而后，[汉儒止有传经之学](../Page/汉儒.md "wikilink")。性道微言之绝久矣。元公崛起，[二程嗣之](../Page/二程.md "wikilink")，又复横渠諸大儒辈出，圣学大昌。"
《[宋史](../Page/宋史.md "wikilink")·[道学传](../Page/道学.md "wikilink")》:“[两汉而下](../Page/两汉.md "wikilink")，[儒学几至大坏](../Page/儒学.md "wikilink")。千有余载，至宋中叶，周敦颐出于舂陵，乃得圣贤不传之学，作《[太极图说](../Page/太极图说.md "wikilink")》、《[通书](../Page/通书.md "wikilink")》，推明[阴阳](../Page/阴阳.md "wikilink")[五行之理](../Page/五行.md "wikilink")，明于天而性于人者，了若指掌。”

[二程兄弟](../Page/二程.md "wikilink")、[張橫渠](../Page/張橫渠.md "wikilink")、[邵雍](../Page/邵雍.md "wikilink")、[司馬光五人](../Page/司馬光.md "wikilink")，再加上周敦頤，被[朱熹稱為](../Page/朱熹.md "wikilink")「[北宋六先生](../Page/北宋六先生.md "wikilink")」\[3\]。

## 著作

[Shou_Toni.jpg](https://zh.wikipedia.org/wiki/File:Shou_Toni.jpg "fig:Shou_Toni.jpg")
[Zhou_Dunyi.JPG](https://zh.wikipedia.org/wiki/File:Zhou_Dunyi.JPG "fig:Zhou_Dunyi.JPG")
1072年，在[江西](../Page/江西.md "wikilink")[庐山](../Page/庐山.md "wikilink")[莲花洞创办了](../Page/莲花洞.md "wikilink")[濂溪书院](../Page/濂溪书院.md "wikilink")，并自号“濂溪先生”。著名[散文作品](../Page/散文.md "wikilink")《[爱莲说](../Page/爱莲说.md "wikilink")》作於此時，表明了敦颐对[莲花的赞赏](../Page/莲花.md "wikilink")。是为千古名句。

周敦颐的主要著作是《[通書](../Page/通書.md "wikilink")》、《[太極圖說](../Page/太極圖說.md "wikilink")》。其著作《[太极圖说](../Page/太极圖说.md "wikilink")》，提出[宇宙生成论体系](../Page/宇宙.md "wikilink")，继承了《[易传](../Page/易传.md "wikilink")》和部分[道家](../Page/道家.md "wikilink")、[道教](../Page/道教.md "wikilink")、[禪門思想](../Page/禪門.md "wikilink")，提出一个简单而有系统的[宇宙构成论](../Page/宇宙.md "wikilink")，用圖形以資推演，可達雅俗共享之效，融會[三教於](../Page/三教.md "wikilink")[儒家](../Page/儒家.md "wikilink")。

敦颐首次將「[無極](../Page/無極.md "wikilink")」一詞引入儒家理論，说“无极而太极”，“太极”一动一静，产生[阴阳万物](../Page/阴阳.md "wikilink")。“万物生生而变化无穷焉，惟人也得其秀而最灵”\[4\]。。

而後世有論者如[毛奇齡等謂此圖實出於](../Page/毛奇齡.md "wikilink")[道教](../Page/道教.md "wikilink")，[清初](../Page/清.md "wikilink")[黃宗炎在](../Page/黃宗炎.md "wikilink")《太極圖說辨》中便說：“圖學從來，出於圖南（[陳希夷](../Page/陳希夷.md "wikilink")）。”。载希夷曾将“无极图”传给[种放](../Page/种放.md "wikilink")，种放以传[穆修](../Page/穆修.md "wikilink")，后穆修将“太极图”传敦颐。周敦颐與[道教關係深遠](../Page/道教.md "wikilink")，亦精研[禪理](../Page/禪.md "wikilink")，故此圖說亦有[道教與](../Page/道教.md "wikilink")[釋教的影子](../Page/釋教.md "wikilink")。

## 后人

[中華人民共和國總理](../Page/中華人民共和國總理.md "wikilink")[周恩来](../Page/周恩来.md "wikilink")、知名作家[鲁迅](../Page/鲁迅.md "wikilink")（周树人）與[周作人](../Page/周作人.md "wikilink")、[周建人三兄弟](../Page/周建人.md "wikilink")、[中華民國內政部警務委員](../Page/中華民國內政部.md "wikilink")[周代殷均为周敦颐后人](../Page/周代殷.md "wikilink")。\[5\]

籍貫[东莞市的富商](../Page/东莞市.md "wikilink")[周永泰是第](../Page/周永泰.md "wikilink")20世孙。\[6\]

[广州市白云区龙归镇南村是周敦颐第](../Page/广州市.md "wikilink")9代后人的聚居地，现在已经到第31代。而同属[广州市的](../Page/广州市.md "wikilink")[黄埔区](../Page/黄埔区.md "wikilink")[茅岗村则是](../Page/鱼珠街道.md "wikilink")**周敦颐**第5代后人周伯进于[南宋年间的迁居地](../Page/南宋.md "wikilink")，现在已经到第27代。此外[广东](../Page/广东.md "wikilink")[新会](../Page/新会.md "wikilink")、[开平](../Page/开平.md "wikilink")、[順德](../Page/順德.md "wikilink")、[潮阳](../Page/潮阳.md "wikilink")，[广西](../Page/广西.md "wikilink")[桂林](../Page/桂林.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[诸暨](../Page/诸暨.md "wikilink")、[金华等地都有周敦颐后人分布](../Page/金华.md "wikilink")。

[灵川县青狮潭镇江头村是周敦颐后人聚居地](../Page/灵川县.md "wikilink")，历代在全国各地做官的多人，[进士](../Page/进士.md "wikilink")、[举人不计其数](../Page/举人.md "wikilink")，至今该村仍留下來重教育人的历史痕迹，现江头村已列为国家级文物保护单位，桂林玖爱莲文化传媒有限公司计划将该村打造成为一个宣传周敦颐理学思想文化教育基地，修复古代建筑设施。

## 参考文献

## 外部链接

  - 黃崇修：〈[周敦頤《太極圖說》定靜工夫新詮釋](http://thinkphil.nccu.edu.tw/files/archive/196_479b5e02.pdf)〉。
  - 張祥龍：〈[周敦頤的《太極圖說》、《易》象數及西方有關學說](http://homepage.ntu.edu.tw/~bcla/e_book/62/08.pdf)〉。

## 参见

  - [湖湘文化](../Page/湖湘文化.md "wikilink")
  - [關學](../Page/關學.md "wikilink")
  - [洛學](../Page/洛學.md "wikilink")
  - [閩學](../Page/閩學.md "wikilink")

{{-}}

[Z周](../Category/宋朝理學家.md "wikilink")
[Z周](../Category/中国哲学家.md "wikilink")
[Z周](../Category/道县人.md "wikilink")
[Z周](../Category/東廡先賢先儒.md "wikilink")
[D敦](../Category/周姓.md "wikilink")
[Category:11世紀哲學家](../Category/11世紀哲學家.md "wikilink")
[Z周](../Category/諡元.md "wikilink")

1.

2.
3.  《六先生畫像贊》

4.  《[太極圖說](../Page/太極圖說.md "wikilink")》

5.  《越城周氏支谱》清光绪三年修木活字本，上海图书馆藏

6.  《石龍周氏支谱》