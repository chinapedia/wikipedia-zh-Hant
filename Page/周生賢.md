**周生賢**（），[寧夏](../Page/寧夏.md "wikilink")[同心人](../Page/同心.md "wikilink")，[高级经济师](../Page/高级经济师.md "wikilink")，曾任[中华人民共和国环境保护部正式建部后的首任部长](../Page/中华人民共和国环境保护部.md "wikilink")。中共第十六届中央候补委员，十七届、十八届中央委员。

## 生平

1968年7月，周生贤毕业于吴忠师范学校。1972年加入[中国共产党](../Page/中国共产党.md "wikilink")。

歷任宁夏[同心县韦州中学教员](../Page/同心县.md "wikilink")，同心县副县长，中共同心县委书记，宁夏[西吉县委书记](../Page/西吉县.md "wikilink")，宁夏回族自治区人民政府副秘书长、秘书长，寧夏回族自治區人民政府副主席、區委常委、國家林業局副局長、黨組副書記、局長、黨組書記。2005年12月任[国家環保總局局長](../Page/国家環保總局.md "wikilink")\[1\]。2008年3月出任[環境保護部部長兼黨組書記](../Page/環境保護部.md "wikilink")。2015年1月28日，被免去环保部党组书记职务\[2\]。2015年2月27日，被免去环保部部长职务\[3\]。次日，全国政协十二届全国委员会常务委员会第九次会议决定增补周生贤为全国政协第十二届全国委员会委员、全国政协人口资源环境委员会副主任\[4\]。

## 家族

  - 儿子：周少舟，1972年10月生，曾担任[国家林业局经济发展研究中心体制改革室副主任](../Page/国家林业局.md "wikilink")、主任，中共浙江临安市委副书记（挂职），[国家林业局经济发展研究中心副主任](../Page/国家林业局.md "wikilink")、高级工程师，[国家林业局驻武汉森林资源监督专员办事处专员](../Page/国家林业局.md "wikilink")。
  - 女儿：周颖瑞，任职于[国家林业局](../Page/国家林业局.md "wikilink")。
  - 女婿：何乐观，任职于[国家林业局](../Page/国家林业局.md "wikilink")。
  - 外甥女：白洁，其名下的绿动元公司代理审批企业环保证书。

## 参考文献

  - [新华网：周生贤简历](http://news.xinhuanet.com/ziliao/2002-03/05/content_300398.htm)

{{-}}

[Z](../Category/中华人民共和国环境保护部部长.md "wikilink")
[Z](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Z](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Z](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Z周](../Category/同心人.md "wikilink") [S生](../Category/周姓.md "wikilink")
[Category:国家林业局局长](../Category/国家林业局局长.md "wikilink")

1.
2.
3.
4.