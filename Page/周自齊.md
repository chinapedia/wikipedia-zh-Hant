[Zhou_Ziqi.jpg](https://zh.wikipedia.org/wiki/File:Zhou_Ziqi.jpg "fig:Zhou_Ziqi.jpg")

**周自齊**（），字**子廙**，祖籍[山東省](../Page/山東省.md "wikilink")[單縣](../Page/單縣.md "wikilink")，中國近代外交家、政治家、教育家、經濟學家、實業家。屬於[交通系](../Page/交通系.md "wikilink")。

## 生平

[清](../Page/清.md "wikilink")[同治八年](../Page/同治.md "wikilink")（1869年）10月14日出生于广东潮州\[1\]。其父周鎬秀，曾任廣東候補巡檢，早逝。伯父周京秀（号绍堂）将其帶大，早年入广州[同文馆和京师](../Page/同文馆.md "wikilink")[同文馆学习](../Page/同文馆.md "wikilink")，后到美国[哥伦比亚大学留学](../Page/哥伦比亚大学.md "wikilink")。1896年任驻美公使馆秘书。1899年任[纽约领事](../Page/纽约.md "wikilink")。1900年任驻[古巴代办](../Page/古巴.md "wikilink")。1903年任驻[旧金山总领事](../Page/旧金山.md "wikilink")。次年任驻美公使馆一等秘书。1908年任驻美公使馆代办，游美學生監督。1909年初，署（“代理”之意）外务部[左参议](../Page/左参议.md "wikilink")，5月补任左参议，8月署[右丞](../Page/右丞.md "wikilink")，不久改为署[左丞](../Page/左丞.md "wikilink")。8月25日，兼任[学部丞参上行走](../Page/学部.md "wikilink")、[游美学务处总办](../Page/游美学务处.md "wikilink")，主持筹建了游美学务处肄业馆（后改名为[清華學堂](../Page/清華學堂.md "wikilink")，即清华大学前身）。1911年4月清华学堂正式开学，周自齐任监督\[2\]。

1911年，任[袁世凱內閣财政次长](../Page/袁世凱.md "wikilink")，1912年改任山东都督兼民政长。1913年任[交通部總長](../Page/交通部.md "wikilink")、代理陆军部总长。1914年任税务处督办兼[中国银行总裁](../Page/中国银行.md "wikilink")。1915年任农商总长，支持[袁世凯称帝](../Page/袁世凯.md "wikilink")，任大典筹备处委员。1916年6月列为帝制祸首“[十三太保](../Page/十三太保.md "wikilink")”之一被[黎元洪通缉而亡命日本](../Page/黎元洪.md "wikilink")。次年回国。1918年特赦后，任参议院副议长、总统府高等顾问。10月，与[熊希龄等](../Page/熊希龄.md "wikilink")24人通电发起“和平期成会”。12月任北京总统府财政委员会委员长。1919年任币制总裁。1920年任[北京政府财政总长](../Page/北京政府.md "wikilink")。次年，以中国代表团顾问身份出席[华盛顿会议](../Page/华盛顿会议.md "wikilink")。

1922年任[徐世昌大总统下的國務院總理兼教育總長](../Page/徐世昌.md "wikilink")，6月2日徐世昌辭職，將印信交由國務院攝行大總統職權\[3\]，周自齊等通電表示，將大總統職權交還國會，以國民資格維持一切，聽侯接收\[4\]。是日，周自齐等內閣成員发布国务院令，宣告国务院依法摄行[大总统职权](../Page/中华民国大总统.md "wikilink")\[5\]，并于6月2日至6月11日发布大总统令一百一十余道，任免政府官员，维持政府正常运转\[6\]\[7\]，直到6月11日[黎元洪復職](../Page/黎元洪.md "wikilink")。后不满[直系军阀](../Page/直系军阀.md "wikilink")，退出政界，出游欧美考察实业，回国后拟筹办孔雀电影制片公司。1923年10月21日，周自齐在北京病逝\[8\]。葬于今[北京市](../Page/北京市.md "wikilink")[门头沟区](../Page/门头沟区.md "wikilink")[周自齐墓](../Page/周自齐墓.md "wikilink")。

## 参考文献

<table>
<thead>
<tr class="header">
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>（<a href="../Page/北京政府.md" title="wikilink">北京政府</a>）         </p></td>
</tr>
</tbody>
</table>

[Z周](../Category/中華民國大陸時期政治人物.md "wikilink")
[Z周](../Category/中华民国山东省省长.md "wikilink")
[Category:中華民國教育總長](../Category/中華民國教育總長.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:中華民國農商總長](../Category/中華民國農商總長.md "wikilink")
[Category:中華民國交通總長](../Category/中華民國交通總長.md "wikilink")
[Category:中華民國陸軍總長](../Category/中華民國陸軍總長.md "wikilink")
[Category:周姓](../Category/周姓.md "wikilink")
[Category:单县人](../Category/单县人.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:出使美国钦差大臣](../Category/出使美国钦差大臣.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中華民國大總統](../Category/中華民國大總統.md "wikilink")

1.  [周纶岐《周自齐哀启》](http://blog.sina.com.cn/s/blog_ec5d43bc0102vmwt.html)
2.  <http://www.tsinghua.edu.cn>
3.  政府公報 2245號
4.  吳宗慈，「中華民國憲法史後編」頁5
5.  政府公报2245号
6.  政府公报2246-2255号
7.  颜惠庆：《颜惠庆自传：一位民国元老的历史记忆》,页160-161
8.  [周纶岐《周自齐讣告》](http://blog.sina.com.cn/s/blog_ec5d43bc0102vldu.html)