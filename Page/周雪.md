**周雪**（，），[加拿大](../Page/加拿大.md "wikilink")[華裔](../Page/華裔.md "wikilink")，籍貫[中國](../Page/中國.md "wikilink")[北京市](../Page/北京市.md "wikilink")，擅長大提琴演奏\[1\]。2001年參選[溫哥華華裔小姐競選奪魁](../Page/溫哥華華裔小姐.md "wikilink")，隨後參選2002年[國際華裔小姐競選亦奪冠](../Page/國際華裔小姐競選.md "wikilink")，賽后並沒有進入娛樂圈\[2\]。

## 獎項

  - 2001年[溫哥華華裔小姐](../Page/溫哥華華裔小姐.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")
  - 2002年[國際華裔小姐](../Page/國際華裔小姐.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")

## 参考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [2002年國際華裔小姐競選官方網頁：周雪個人資料](https://web.archive.org/web/20070128110307/http://tvcity.tvb.com/special/mcip2002/profile/profile_07.html)

[Z周](../Category/國際中華小姐.md "wikilink")
[Z周](../Category/北京人.md "wikilink")
[X雪](../Category/周姓.md "wikilink")
[Z周](../Category/不列顛哥倫比亞大學校友.md "wikilink")

1.  [周雪大提琴技驚全場奪華裔小姐冠軍](http://wwww.epochtimes.com/b5/2/1/29/n167148.htm)
    大紀元新聞網，2002年1月29日
2.  [华姐周雪弃娱乐圈返加拿大升学](http://gzdaily.dayoo.com/gb/content/2002-02/21/content_377199.htm)
     大洋網，2002年2月21日