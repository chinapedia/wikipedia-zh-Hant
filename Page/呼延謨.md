**呼延謨**，[前趙時](../Page/前趙.md "wikilink")[劉曜手下大將](../Page/劉曜.md "wikilink")，任鎮東將軍。

## 生平

劉曜與[石勒交戰的時候](../Page/石勒.md "wikilink")，呼延謨奉令部率[荊州](../Page/荊州.md "wikilink")、[司州的軍隊](../Page/司州.md "wikilink")，從[崤山](../Page/崤山.md "wikilink")、[澠池一帶向東方行軍](../Page/澠池.md "wikilink")，與前趙另一將領[劉岳遙為呼應](../Page/劉岳.md "wikilink")。然而劉、呼二人相繼敗於[石虎之手](../Page/石虎.md "wikilink")，呼延謨更於陣上被石虎所殺。

呼延谟的儿子呼延琮。

## 軼話

《[晉書](../Page/晉書.md "wikilink")‧列女傳》提及一名居於[陝縣之婦人](../Page/陝縣.md "wikilink")，性格貞烈，服事叔姑既孝且謹，但卻被叔父之女誣告殺害叔姑，受冤而死。其怨氣令屍骸不腐、當地多年沒有下雨。後來劉曜命呼延謨擔任當地[太守](../Page/太守.md "wikilink")，呼延謨知道此事後，處決誣告貞婦的女人，為貞婦雪冤，又設祭其墓，追[諡](../Page/諡號.md "wikilink")「孝烈貞婦」，陝縣一帶才再次下雨。

[category:五胡十六國人物](../Page/category:五胡十六國人物.md "wikilink")
[category:前趙官員](../Page/category:前趙官員.md "wikilink")

[五胡十六國](../Category/呼延姓.md "wikilink")