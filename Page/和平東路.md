**和平東路**，為[台北市的重要幹道之一](../Page/臺北市.md "wikilink")，為東西向道路，共有四段。以[羅斯福路為和平東西路分界](../Page/羅斯福路_\(台北市\).md "wikilink")，以東為和平東路，以西則為和平西路。東接[木柵路四段](../Page/木柵路.md "wikilink")，西接[和平西路](../Page/和平西路.md "wikilink")。

## 行經行政區域

（由西至東）

  - [大安區](../Page/大安區_\(臺北市\).md "wikilink")
  - [信義區](../Page/信義區_\(臺北市\).md "wikilink")
  - [文山區](../Page/文山區.md "wikilink")

## 歷史

和平東路闢建於[日治時代](../Page/台灣日治時期.md "wikilink")，最初名為「六張犁道路」，此路段同時還闢有運送煤礦用的台車軌道，可將[六張犁的煤礦送至](../Page/六張犁.md "wikilink")[古亭站](../Page/古亭站.md "wikilink")。\[1\]\[2\]國民政府來台後，於1948年改稱為「和平東路」，並停用了台車軌道；原本的和平東路包括現在的羅斯福路至基隆路之間路段及崇德街；1970年，在現今的基隆路以東闢建新的道路取代原本的和平東路，成為「和平東路三段」，以東之舊有和平東路則被改稱為「崇德街」。2012年3月，接續原和平東路三段終點（捷運[麟光站](../Page/麟光站.md "wikilink")）之原臥龍街後半段，改編為「和平東路三段」；2014年5月，透過[莊敬隧道連結之](../Page/莊敬隧道.md "wikilink")[文山區軍功路更名為](../Page/文山區.md "wikilink")「和平東路四段」，並重編開段之門牌順序。

## 分段

和平東路目前共分四段。

  - 一段：西於[羅斯福路與](../Page/羅斯福路_\(台北市\).md "wikilink")[和平西路一段相接](../Page/和平西路.md "wikilink")，東於[新生南路接和平東路二段](../Page/新生南路.md "wikilink")。
  - 二段：西於新生南路與和平東路一段相接，東於[臥龍街與和平東路三段相接](../Page/臥龍街.md "wikilink")。
  - 三段：西於臥龍街與和平東路二段相接，東接[莊敬隧道](../Page/莊敬隧道.md "wikilink")。\[3\]
  - 四段：西於莊敬隧道與和平東路三段相接，東止於木柵路四段。\[4\]

## 沿線設施（由西至東）

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li>一段
<ul>
<li><a href="../Page/台北捷運.md" title="wikilink">捷運</a><a href="../Page/古亭站.md" title="wikilink">古亭站</a>(5號出口)</li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統捷運古亭站</a>(2號出口)</li>
<li><a href="../Page/台灣電力公司.md" title="wikilink">台灣電力公司核能火力發電工程處</a>(39號)</li>
<li><a href="../Page/法務部行政執行署.md" title="wikilink">法務部行政執行署</a>(115號)</li>
<li><a href="../Page/臺北市政府警察局.md" title="wikilink">臺北市政府警察局大安分局和平東路派出所</a>(143號)</li>
<li><a href="../Page/中華郵政.md" title="wikilink">中華郵政台北青田郵局</a>(台北7支)(155號)</li>
<li><a href="../Page/國立台灣師範大學.md" title="wikilink">國立台灣師範大學</a>(162號)</li>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統</a>-臺灣師範大學（圖書館）站</li>
<li><a href="../Page/國家教育研究院.md" title="wikilink">國家教育研究院臺北院區</a>(179號)</li>
</ul></li>
</ul>
<ul>
<li>二段
<ul>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統新生和平路口站</a></li>
<li><a href="../Page/大安森林公園.md" title="wikilink">大安森林公園</a></li>
<li><a href="../Page/臺北靈糧堂.md" title="wikilink">臺北靈糧堂宣教大樓</a>(24號)</li>
<li><a href="../Page/國立臺北教育大學附設實驗國民小學.md" title="wikilink">國立臺北教育大學附設實驗國民小學</a>(94號)</li>
<li><a href="../Page/科技大樓.md" title="wikilink">科技大樓</a>(106號)
<ul>
<li><a href="../Page/中華民國科技部.md" title="wikilink">科技部</a></li>
<li><a href="../Page/國家實驗研究院.md" title="wikilink">國家實驗研究院</a>(3樓)</li>
<li><a href="../Page/資訊工業策進會.md" title="wikilink">資訊工業策進會</a>(11樓)</li>
<li><a href="../Page/國家圖書館_(中華民國).md" title="wikilink">國家圖書館附設資訊圖書館</a>(13樓)</li>
<li><a href="../Page/國家實驗研究院科技政策研究與資訊中心.md" title="wikilink">國家實驗研究院科技政策研究與資訊中心</a>(1樓、14樓、15樓)</li>
</ul></li>
<li><a href="https://zh.wikipedia.org/wiki/File:NTUELogo.jpg" title="fig:NTUELogo.jpg">NTUELogo.jpg</a><a href="../Page/國立臺北教育大學.md" title="wikilink">國立臺北教育大學</a>(134號)</li>
<li><a href="../Page/教廷駐華大使館.md" title="wikilink">教廷駐華大使館</a>(265巷7之1號)</li>
<li>成功國宅(311巷)</li>
<li><a href="../Page/臺北市立圖書館.md" title="wikilink">臺北市立圖書館成功民眾閱覽室</a>(311巷29號4樓)</li>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統和平建國路口站</a></li>
</ul></li>
<li>三段
<ul>
<li><a href="../Page/臺北高等行政法院.md" title="wikilink">臺北高等行政法院</a>(1巷1號)</li>
<li><a href="../Page/交通部台灣區國道新建工程局.md" title="wikilink">交通部台灣區國道新建工程局</a>(1巷1號5樓)</li>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統成功國宅</a>(1巷)</li>
<li>臺灣電力公司北區活動中心(60號)
<ul>
<li>台電會館-和平會館</li>
</ul></li>
<li><a href="../Page/中影股份有限公司.md" title="wikilink">中影股份有限公司</a><a href="../Page/梅花數位影院.md" title="wikilink">梅花數位影院</a>(63號2F)</li>
<li><a href="../Page/捷運六張犁站.md" title="wikilink">捷運六張犁站</a>(168號)</li>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統</a><a href="../Page/捷運六張犁站.md" title="wikilink">捷運六張犁站</a>(168號)</li>
<li>中華郵政台北六張犁郵局(台北57支)(208號)</li>
<li><a href="../Page/台灣昆蟲館.md" title="wikilink">台灣昆蟲館</a>(406巷8號)</li>
<li><a href="../Page/捷運麟光站.md" title="wikilink">捷運麟光站</a>(410號)</li>
<li><a href="../Page/臺北市公共自行車租賃系統.md" title="wikilink">臺北市公共自行車租賃系統</a><a href="../Page/捷運麟光站.md" title="wikilink">捷運麟光站</a>(2號出口)(410號)</li>
<li>大我山莊(557號)</li>
<li>大我新舍(599號)
<ul>
<li><a href="../Page/臺北榮民總醫院.md" title="wikilink">臺北榮民總醫院大我新舍門診部</a>(門牌為臥龍街)</li>
</ul></li>
<li><a href="../Page/天德堂總堂.md" title="wikilink">天德堂總堂</a>(596號)</li>
<li>慈恩園(631巷49號)</li>
<li><a href="../Page/石泉巖.md" title="wikilink">六張犁石泉巖清水祖師廟</a>(631巷50號)</li>
<li>台北港口奉聖宮(631巷52號)</li>
</ul></li>
</ul>
<ul>
<li>四段
<ul>
<li><a href="../Page/中華信義會.md" title="wikilink">中國基督教信義會施恩堂</a>(370號)</li>
</ul></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")

## 參考資料

<references/>

## 外部連結

[H和](../Category/台北市街道.md "wikilink")

1.
2.
3.
4.