[Hoping_river.JPG](https://zh.wikipedia.org/wiki/File:Hoping_river.JPG "fig:Hoping_river.JPG")
[Taiwan_formosa_vintage_history_other_places_bridges_suspension_taipics045.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_formosa_vintage_history_other_places_bridges_suspension_taipics045.jpg "fig:Taiwan_formosa_vintage_history_other_places_bridges_suspension_taipics045.jpg")）是以鐵線橋橫跨大濁水溪，上題「大濁水鐵線橋（蘇澳郡蕃地）」等字樣。\]\]

**和平溪**又名**大濁水溪**，位於[台灣東部](../Page/台灣.md "wikilink")，屬於[中央管河川](../Page/中央管河川.md "wikilink")，是[花蓮縣與](../Page/花蓮縣.md "wikilink")[宜蘭縣的界溪](../Page/宜蘭縣.md "wikilink")，也是[北臺灣與](../Page/北臺灣.md "wikilink")[東臺灣的界溪](../Page/東臺灣.md "wikilink")。

## 簡介

和平溪發源於[南湖北山東側](../Page/南湖北山.md "wikilink")，總長約有50.73公里\[1\]。流域面積約為561.60平方公里，主要支流有[和平南溪與](../Page/和平南溪.md "wikilink")[和平北溪](../Page/和平北溪.md "wikilink")\[2\]，流經[南澳鄉與](../Page/南澳鄉.md "wikilink")[秀林鄉](../Page/秀林鄉.md "wikilink")，出海口分為兩處。下游建有[大濁水橋](../Page/大濁水橋.md "wikilink")。

## 特色

和平溪的溪流主線在短短四、五十公里的距離內下降近三千公尺，在上游造成劇烈的[侵蝕作用及崩塌](../Page/侵蝕作用.md "wikilink")，再加上此區在冬季是[東北季風的迎風面](../Page/東北季風.md "wikilink")，在夏季又常有[颱風侵襲](../Page/颱風.md "wikilink")，因此，雨量豐富，故河水的搬運作用極強，由於河水的沙石含量極大，造成溪水混濁，故又有人將其稱為**大濁水溪**。

由於和平溪主幹水系並不長，而高低落差大，因此，河水停留在水域內的時間並不長，造成豐水期與枯水期的極大差異，甚至經常見底，是個典型的「荒溪型」河川。

和平溪最有名的就是出海口的大扇型沙洲，其位於和平溪下游出海口，當河水運砂量遠大於海水沿岸流的搬運力量時，就會堆積出大型的河口三角洲，和平溪因主流和山脈逼近海洋的關係，所以砂礫從上游衝擊的力道極強，因此到了出海口形成圓弧狀突出外形的海口沖積平原，使海岸線呈現外凸的弧形，為標準的圓弧狀[三角洲](../Page/三角洲.md "wikilink")（arcuate
delta），整個三角洲沖積扇的扇頂位於[宜蘭縣](../Page/宜蘭縣.md "wikilink")[南澳鄉澳花村](../Page/南澳鄉.md "wikilink")，扇面向東橫跨宜蘭、花蓮兩縣至出海口。

## 水系主要河川

  - **和平溪**
      - [澳花溪](../Page/澳花溪.md "wikilink")
      - [和平南溪](../Page/和平南溪.md "wikilink") （大濁水南溪）
          - [闊闊庫溪](../Page/闊闊庫溪.md "wikilink")
      - [和平北溪](../Page/和平北溪.md "wikilink") （大濁水北溪）
          - [布蕭丸溪](../Page/布蕭丸溪.md "wikilink")
          - [莫很溪](../Page/莫很溪.md "wikilink")
          - [次考干溪](../Page/次考干溪.md "wikilink")
          - [拉巴丸溪](../Page/拉巴丸溪.md "wikilink")

## 主要橋樑

以下由溪口至源頭列出主流上之主要橋樑：

  - [台鐵和平溪橋](../Page/台鐵和平溪橋.md "wikilink")（[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")[台鐵](../Page/台鐵.md "wikilink")[北迴線](../Page/北迴線.md "wikilink")）
  - [大濁水橋](../Page/大濁水橋.md "wikilink")（[TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[省道](../Page/臺灣省道.md "wikilink")[台9線](../Page/台9線.md "wikilink")／[蘇花公路](../Page/蘇花公路.md "wikilink")）
  - [和平溪橋](../Page/和平溪橋.md "wikilink")（省道台9線／[蘇花公路改善計畫](../Page/蘇花公路改善計畫.md "wikilink")；施工中）

## 附近設施

  - （[TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[省道](../Page/臺灣省道.md "wikilink")[台9線](../Page/台9線.md "wikilink")／[蘇花公路](../Page/蘇花公路.md "wikilink")）目前為此區域內對外的主要聯絡公路。
  - 和平水泥專業區：此區位於和平溪南岸，[花蓮縣](../Page/花蓮縣.md "wikilink")[秀林鄉境內](../Page/秀林鄉.md "wikilink")，由[台灣水泥公司投資興建](../Page/台灣水泥.md "wikilink")，包含台泥公司的水泥廠、採礦場、水泥專用港及[和平發電廠等](../Page/和平發電廠.md "wikilink")。
  - 台鐵[北迴線](../Page/北迴線.md "wikilink")：在和平溪出口北側，宜蘭縣境內設有[漢本車站](../Page/漢本車站.md "wikilink")，和平溪南側花蓮縣境內有[和平車站](../Page/和平車站.md "wikilink")，為此區域內對外的主要聯絡車站。
  - [南溪壩](../Page/南溪壩.md "wikilink")：位於和平溪支流和平南溪上，主配合為[台灣電力公司東部地區](../Page/台灣電力公司.md "wikilink")[碧海電廠運作使用](../Page/碧海電廠.md "wikilink")。

## 相關條目

  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[和平溪水系](../Page/category:和平溪水系.md "wikilink")
[category:花蓮縣河川](../Page/category:花蓮縣河川.md "wikilink")
[category:宜蘭縣河川](../Page/category:宜蘭縣河川.md "wikilink")

1.
2.  [和平溪流域圖及支流分布表](http://web.thu.edu.tw/deborah/www/index2/stream/hopin/hopin.htm)