[Cenotaph,_Hong_Kong_1.jpg](https://zh.wikipedia.org/wiki/File:Cenotaph,_Hong_Kong_1.jpg "fig:Cenotaph,_Hong_Kong_1.jpg")[皇后像廣場的](../Page/皇后像廣場.md "wikilink")[和平紀念碑](../Page/和平紀念碑_\(香港\).md "wikilink")\]\]
[香港的](../Page/香港.md "wikilink")**和平紀念日**（**Remembrance
Day**）原是紀念[第一次世界大戰結束的](../Page/第一次世界大戰.md "wikilink")[紀念日](../Page/紀念日.md "wikilink")，後來擴展至悼念[香港保衛戰中的捐軀者及在](../Page/香港保衛戰.md "wikilink")[第二次世界大戰中的罹難者](../Page/第二次世界大戰.md "wikilink")\[1\]。紀念日原定於每年的11月11日，現在定為每年11月第二個星期日，當日在[中環的](../Page/中環.md "wikilink")[和平紀念碑會舉行悼念活動](../Page/和平紀念碑_\(香港\).md "wikilink")。

## 起源

自[1919年起](../Page/1919年.md "wikilink")，[英屬香港政府將每年的](../Page/英屬香港.md "wikilink")[11月11日訂為](../Page/11月11日.md "wikilink")「和平紀念日」（即《[康邊停戰協定](../Page/康邊停戰協定.md "wikilink")》簽訂日）\[2\]。[香港總督](../Page/香港總督.md "wikilink")、政府官員及立法局議員，每年均會於[和平紀念碑所在的](../Page/和平紀念碑_\(香港\).md "wikilink")[中環](../Page/中環.md "wikilink")[皇后像廣場出席大型悼念儀式](../Page/皇后像廣場.md "wikilink")。

[第二次世界大戰於](../Page/第二次世界大戰.md "wikilink")1945年8月結束，相對於一次大戰的主要戰場在歐洲，香港在二戰期間經歷[香港保衛戰的戰火洗禮及承受嚴重傷亡](../Page/香港保衛戰.md "wikilink")，所以紀念日有所調動。香港政府將紀念一次大戰結束的「和平紀念日」改定為最接近11月11日的每年11月第二個星期日，從此香港紀念一戰結束的日子與世界上多數地區的11月11日並非每年都相同。政府其後於1946年將每年8月30日定為[香港重光紀念日紀念](../Page/香港重光紀念日.md "wikilink")[香港日佔時期結束及](../Page/香港日佔時期.md "wikilink")[香港重光](../Page/香港重光.md "wikilink")，並列入[公眾假期](../Page/香港公眾假期.md "wikilink")。當時紀念一戰結束的儀式在和平紀念日舉行；紀念二戰結束的儀式則在香港重光紀念日舉行，而兩者都在中環皇后像廣場舉行官方悼念儀式\[3\]。

## 主權移交後

1997年[香港主權移交後](../Page/香港主權移交.md "wikilink")，[香港重光紀念日的公眾假期曾一度由](../Page/香港重光紀念日.md "wikilink")[抗日戰爭勝利紀念日取代](../Page/抗日戰爭勝利紀念日.md "wikilink")，但該公眾假期後來又於1999年由[佛誕取代](../Page/佛誕.md "wikilink")；紀念二戰結束的儀式則合併到和平紀念日舉行\[4\]。

1998年9月9日，立法會通過的《1998年假期（修訂）條例草案》\[5\]，包含廢除[抗日戰爭勝利紀念日公眾假期](../Page/:抗日戰爭勝利紀念日.md "wikilink")（1997年7月1日前為[香港重光紀念日公眾假期](../Page/香港重光紀念日.md "wikilink")）\[6\]。

現在香港和平紀念日定為每年11月第二個星期日\[7\]。

## 參見

  - [虞美人花](../Page/虞美人_\(花卉\).md "wikilink")：代表和平紀念日的花朵\[8\]
  - [香港重光紀念日](../Page/香港重光紀念日.md "wikilink")：紀念[香港日佔時期結束的紀念日](../Page/香港日佔時期.md "wikilink")
  - [國殤紀念日](../Page/國殤紀念日.md "wikilink")：[英國](../Page/英國.md "wikilink")、[北美及](../Page/北美.md "wikilink")[英聯邦國家紀念](../Page/英聯邦.md "wikilink")[第一次世界大戰結束的紀念日](../Page/第一次世界大戰.md "wikilink")，定於每年11月11日

## 參考資料

[Category:香港節日](../Category/香港節日.md "wikilink")
[Category:第一次世界大戰紀念日](../Category/第一次世界大戰紀念日.md "wikilink")
[Category:第二次世界大戰紀念日](../Category/第二次世界大戰紀念日.md "wikilink")
[Category:11月節日](../Category/11月節日.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.