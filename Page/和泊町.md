**和泊町**（，）是[鹿兒島縣轄下位於](../Page/鹿兒島縣.md "wikilink")[沖永良部島東部的一個](../Page/沖永良部島.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。屬[大島郡](../Page/大島郡_\(鹿兒島縣\).md "wikilink")。

## 歷史

沖永良部島在1266年至1609年期間屬於[琉球國](../Page/琉球國.md "wikilink")，因此與現在的[沖繩地區交流頻繁](../Page/沖繩.md "wikilink")，文化受到琉球的影響很深。\[1\]

1609年至1871年期間，為日本[薩摩藩的直屬領](../Page/薩摩藩.md "wikilink")。

### 沿革

  - 1908年4月1日：奄美群島實施[島嶼町村制](../Page/島嶼町村制.md "wikilink")，設置和泊村。
  - 1941年5月1日：改制為和泊町。

## 交通

### 機場

  - [沖永良部機場](../Page/沖永良部機場.md "wikilink")

## 姊妹市・友好都市

### 日本

  - 花都市交流

<!-- end list -->

  - [中富良野町](../Page/中富良野町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[空知郡](../Page/空知郡.md "wikilink"))
  - [砺波市](../Page/砺波市.md "wikilink")（[富山縣](../Page/富山縣.md "wikilink")）
  - [下田市](../Page/下田市.md "wikilink")（[靜岡縣](../Page/靜岡縣.md "wikilink")）
  - [武生市](../Page/武生市.md "wikilink")（[福井縣](../Page/福井縣.md "wikilink")）：已於2005年10月1日合併為[越前市](../Page/越前市.md "wikilink")
  - [長井市](../Page/長井市.md "wikilink")（[山形縣](../Page/山形縣.md "wikilink")）
  - [大野町](../Page/大野町.md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")[揖斐郡](../Page/揖斐郡.md "wikilink")）
  - [須賀川市](../Page/須賀川市.md "wikilink")（[福島縣](../Page/福島縣.md "wikilink")）
  - [寶塚市](../Page/寶塚市.md "wikilink")（[兵庫縣](../Page/兵庫縣.md "wikilink")）
  - [久留米市](../Page/久留米市.md "wikilink")（[福岡縣](../Page/福岡縣.md "wikilink")）

## 本地出身之名人

  - [大山百合香](../Page/大山百合香.md "wikilink")：[歌手](../Page/歌手.md "wikilink")

## 參考資料

## 外部連結

  - [和泊町商工會](http://wadomari.kashoren.or.jp/)

<!-- end list -->

1.