**和硕亲王**（），简称[亲王](../Page/亲王.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[宗室和](../Page/宗室.md "wikilink")[蒙古外藩中内扎萨克蒙古爵位的第一等爵位](../Page/外藩蒙古.md "wikilink")。宗室唯皇子、皇兄弟可以获得此爵位。在外扎萨克蒙古中為第二等爵，僅次於[汗](../Page/可汗.md "wikilink")。清太宗仿明建制时于亲王前加号和硕，意思為方城或王國，亲王以下爵仅加号多罗或固山（旗），以示此亲王即旧和硕贝勒，地位尊崇。

[世袭罔替的](../Page/世袭罔替.md "wikilink")[王爵](../Page/王爵.md "wikilink")，俗稱[鐵帽子王](../Page/鐵帽子王.md "wikilink")，從[清兵入關至](../Page/清兵入關.md "wikilink")[宣統遜位](../Page/宣統遜位.md "wikilink")，共有十二家，即[礼](../Page/礼亲王.md "wikilink")、[睿](../Page/睿亲王.md "wikilink")、[豫](../Page/豫亲王.md "wikilink")、[肃](../Page/肃亲王.md "wikilink")、[简](../Page/简亲王.md "wikilink")、[庄](../Page/庄亲王.md "wikilink")、[怡](../Page/怡亲王.md "wikilink")、[恭](../Page/恭亲王.md "wikilink")、[醇](../Page/醇亲王.md "wikilink")、[庆这十位亲王和](../Page/庆亲王.md "wikilink")[顺承郡王](../Page/顺承郡王.md "wikilink")、[克勤郡王](../Page/克勤郡王.md "wikilink")，[亲王和](../Page/亲王.md "wikilink")[郡王世袭罔替的本质](../Page/郡王.md "wikilink")，都是一样的，但是后代的袭爵待遇不一样。

## 宗室世爵

[清朝](../Page/清朝.md "wikilink")[宗室从高到低共十二级](../Page/宗室.md "wikilink")：

1.  [和硕亲王](../Page/和硕亲王.md "wikilink")
2.  [多罗郡王](../Page/多罗郡王.md "wikilink")
3.  [多罗贝勒](../Page/多罗贝勒.md "wikilink")
4.  [固山贝子](../Page/固山贝子.md "wikilink")
5.  [奉恩鎮國公](../Page/奉恩镇国公.md "wikilink")
6.  [奉恩辅国公](../Page/奉恩辅国公.md "wikilink")
7.  [不入八分镇国公](../Page/不入八分镇国公.md "wikilink")
8.  [不入八分辅国公](../Page/不入八分辅国公.md "wikilink")
9.  [镇国将军](../Page/镇国将军.md "wikilink")，分一至三等镇国将军，正一品；
10. [辅国将军](../Page/辅国将军.md "wikilink")，分一等辅国将军兼一云骑尉及一至三等辅国将军，正二品；
11. [奉国将军](../Page/奉国将军.md "wikilink")，分一等奉国将军兼一云骑尉及一至三等奉国将军，正三品；
12. [奉恩将军](../Page/奉恩将军.md "wikilink")，分奉恩将军兼一云骑尉及奉恩将军，正四品。

奉恩辅国公之前等称[入八分公](../Page/入八分公.md "wikilink")，与之后的不入八分镇国公、不入八分辅国公等爵位不同。冊封[嫡子共二級](../Page/嫡子.md "wikilink")，分別為[世子](../Page/世子.md "wikilink")（[亲王嫡子](../Page/亲王.md "wikilink")）、與[长子](../Page/长子.md "wikilink")（[郡王嫡子](../Page/郡王.md "wikilink")）。没有爵位的宗室成员统称为**“闲散宗室”**，可穿着四品武官官服。

除了清初受封的八大[鐵帽子王礼亲王](../Page/鐵帽子王.md "wikilink")[代善](../Page/代善.md "wikilink")、郑亲王[济尔哈朗](../Page/济尔哈朗.md "wikilink")、睿亲王[多尔衮](../Page/多尔衮.md "wikilink")、豫亲王[多铎](../Page/多铎.md "wikilink")、肃亲王[豪格](../Page/豪格.md "wikilink")、承泽亲王[硕塞](../Page/硕塞.md "wikilink")、克勤郡王[岳托](../Page/岳托.md "wikilink")、和顺承郡王[勒克德浑](../Page/勒克德浑.md "wikilink")、以及後來的雍正时怡亲王[胤祥](../Page/胤祥.md "wikilink")、同治时恭亲王[奕訢](../Page/奕訢.md "wikilink")、光绪时醇亲王[奕譞](../Page/奕譞.md "wikilink")、庆亲王[奕劻共十二个王可以](../Page/奕劻.md "wikilink")[世袭罔替外](../Page/世袭罔替.md "wikilink")，其余宗亲世爵只能遞降[世袭](../Page/世袭.md "wikilink")。

宗室爵俸禄：

1.  亲王岁俸银10000两，禄米10000[斛](../Page/斛.md "wikilink")；
2.  世子岁俸银6000两，禄米6000斛；
3.  郡王岁俸银5000两，禄米5000斛；
4.  长子岁俸银3000两，禄米3000斛；
5.  贝勒岁俸银2500两，禄米2500斛；
6.  贝子岁俸银1300两，禄米1300斛；
7.  镇国公岁俸银700两，禄米700斛；
8.  辅国公岁俸银500两，禄米500斛；
9.  一等镇国将军岁俸银410两，禄米410斛；
10. 二等镇国将军岁俸银385两，禄米385斛；
11. 三等镇国将军岁俸银360两，禄米360斛；
12. 一等辅国将军兼一云骑尉岁俸银335两，禄米335斛；
13. 一等辅国将军岁俸银310两，禄米310斛；
14. 二等辅国将军岁俸银285两，禄米285斛；
15. 三等辅国将军岁俸银260两，禄米260斛；
16. 一等奉国将军兼一云骑尉岁俸银235两，禄米235斛；
17. 一等奉国将军岁俸银210两，禄米210斛；
18. 二等奉国将军岁俸银185两，禄米185斛；
19. 三等奉国将军岁俸银160两，禄米160斛；
20. 奉恩将军兼一云骑尉岁俸银135两，禄米135斛；
21. 奉恩将军岁俸银110两，禄米110斛。

## 注释

## 参考文献

## 参见

  - [入八分公](../Page/入八分公.md "wikilink")
  - [清朝亲王列表](../Page/清朝亲王列表.md "wikilink")
  - [中国爵位](../Page/中国爵位.md "wikilink")

{{-}}

[清朝親王](../Category/清朝親王.md "wikilink")