**咪唑**（），即1,3-[二氮唑](../Page/二唑.md "wikilink")，是一个五元[杂环](../Page/杂环.md "wikilink")[芳香性](../Page/芳香性.md "wikilink")[有机化合物](../Page/有机化合物.md "wikilink")，化学式。它也是一个[生物碱](../Page/生物碱.md "wikilink")。白色或浅黄色固体结晶，可溶于水、[氯仿](../Page/氯仿.md "wikilink")、[醇](../Page/醇.md "wikilink")、[醚](../Page/醚.md "wikilink")，具有[酸性](../Page/酸性.md "wikilink")，也具有[碱性](../Page/碱性.md "wikilink")。氢原子在两个氮原子之间移动，因此存在两个[互变异构体](../Page/互变异构.md "wikilink")。

咪唑环结构在生物分子中广泛存在，例如[组氨酸和对应的](../Page/组氨酸.md "wikilink")[荷尔蒙](../Page/荷尔蒙.md "wikilink")[组胺](../Page/组胺.md "wikilink")。很多药物也包含有咪唑环，例如[硝基咪唑和咪唑类](../Page/硝基咪唑.md "wikilink")[抗真菌药物](../Page/抗真菌药物.md "wikilink")。\[1\]\[2\]\[3\]\[4\]\[5\]

## 发现

早在1840年代，多种咪唑的衍生物就已经被发现。1858年，Heinrich
Debus首次合成了咪唑。他合成咪唑的方法是用[乙二醛和](../Page/乙二醛.md "wikilink")[甲醛在](../Page/甲醛.md "wikilink")[氨中进行反应](../Page/氨.md "wikilink")，所以此法又称狄博斯法。\[6\]
这一合成方法虽然效率很低，但目前仍用于合成C取代的咪唑，也是目前工业上合成咪唑的常用方法之一。

<center>

[ImidazoleSynthesisDebus.png](https://zh.wikipedia.org/wiki/File:ImidazoleSynthesisDebus.png "fig:ImidazoleSynthesisDebus.png")

</center>

在一个微波改性反应中[1,2-二苯基乙二酮](../Page/二苯基乙二酮.md "wikilink")、[苯甲醛和](../Page/苯甲醛.md "wikilink")[氨在](../Page/氨.md "wikilink")[冰醋酸中生成](../Page/冰醋酸.md "wikilink")2,4,5-三苯基咪唑（又称洛粉碱）。\[7\]

## 结构与性质

咪唑为平面五元[环状化合物](../Page/环状化合物.md "wikilink")，易溶于水（以无限比例）和其它极性溶剂。咪唑的两个氮原子间存在永久偶极，极性很强，[偶极矩为](../Page/偶极矩.md "wikilink")3.61D，并且分子间存在[氢键缔合](../Page/氢键.md "wikilink")，导致了咪唑具有反常高的沸点（256℃）。分子中存在一个6电子[共轭](../Page/共轭.md "wikilink")[大π键](../Page/大π键.md "wikilink")，故具有典型的[芳香性](../Page/芳香性.md "wikilink")。与氢以σ键相连的氮原子提供一对电子，环内其余四个原子各提供一个电子成键。

1N上有氢的咪唑环中，氢原子可以在两个氮原子间迁移，存在两个[互变异构体](../Page/互变异构.md "wikilink")，C-4和C-5是等同的。这两个互变异构体无法分离，当有取代基时，常以“4(5)-取代咪唑”（如4(5)-甲基咪唑）来命名。

### 两性化合物

咪唑具有[两性](../Page/两性.md "wikilink")，即同时表现出酸性与碱性。作为一种酸，咪唑的pK<sub>a</sub>是14.5，它的酸性比[羧酸](../Page/羧酸.md "wikilink")、[酚类与](../Page/酚类.md "wikilink")[酰亚胺弱](../Page/酰亚胺.md "wikilink")，但稍微比[醇强](../Page/醇.md "wikilink")。可离解质子在N-1上。作为碱，咪唑的[共轭酸的pK](../Page/共轭酸.md "wikilink")<sub>a</sub>（上文提到的为pK<sub>BH<sup>+</sup></sub>以避免混淆）大约是7，使咪唑碱性比[吡啶强约六十倍以上](../Page/吡啶.md "wikilink")，可以和无机酸生成稳定且易溶于水的盐。体现碱性的原子为N-3。

咪唑的一些共振结构如下所示：

[ImidazoleResonance.png](https://zh.wikipedia.org/wiki/File:ImidazoleResonance.png "fig:ImidazoleResonance.png")

### 反应

咪唑比其他1,3-二唑更容易发生[亲电芳香取代反应](../Page/亲电芳香取代反应.md "wikilink")，并且反应主要在C-4和C-5上进行。这是因为亲电试剂进攻C-2时，有特别不稳定的[极限式](../Page/共振论.md "wikilink")，生成的中间体将正电荷分布在氮原子上。例如，咪唑与发烟硝酸/浓硫酸作用，可以很快生成产率很高的4(5)-硝基咪唑；而4,5-二甲基咪唑在剧烈条件下硝化，仍然不能发生反应。

[ImidazoleNitration.png](https://zh.wikipedia.org/wiki/File:ImidazoleNitration.png "fig:ImidazoleNitration.png")

咪唑N-3上的电子云密度较大，所以[烷基化反应一般都先在这个氮原子上发生](../Page/烷基化.md "wikilink")。一烷基化的产物通过互变异构，又可以产生一个类似于[吡啶中的氮原子](../Page/吡啶.md "wikilink")，因此可以进一步反应，生成二烷基化的产物咪唑鎓盐。

咪唑的[酰基化反应一般也在N](../Page/酰基化.md "wikilink")-3上发生，但由于酰基是[吸电子基](../Page/吸电子基.md "wikilink")，故反应能控制在一元酰基化阶段，产物是*N*-酰基咪唑。

咪唑的活泼氢可以分解[格氏试剂](../Page/格氏试剂.md "wikilink")，生成咪唑的*N*-镁盐，经异构化后得到C-2取代的咪唑。后者用[碘甲烷处理](../Page/碘甲烷.md "wikilink")，可以生成1,2-二甲基咪唑。

咪唑可与[亲双烯体发生加成](../Page/亲双烯体.md "wikilink")，先生成3-鎓盐两性离子，然后与另一分子亲双烯体亲核加成，生成C-2环化的产物。例如1-甲基-2-乙基咪唑与两分子[丁炔二酸二甲酯反应后](../Page/丁炔二酸二甲酯.md "wikilink")，得到8a-乙基-1-甲基-1,8a-二氢咪唑并\[1,2-a\]吡啶-5,6,7,8-四羧酸四甲酯。

## 咪唑盐

[Imidazolium_salt.svg](https://zh.wikipedia.org/wiki/File:Imidazolium_salt.svg "fig:Imidazolium_salt.svg")
咪唑环上的氮原子被质子化或取代时可以形成咪唑鎓盐（如氯化咪唑）。这些盐是咪唑[离子液体和很多](../Page/离子液体.md "wikilink")[稳定卡宾的前体](../Page/稳定卡宾.md "wikilink")。

咪唑也可形成[阴离子与金属成盐](../Page/阴离子.md "wikilink")，如[咪唑钠](../Page/咪唑钠.md "wikilink")、咪唑钾，以及不溶性的咪唑银盐。咪唑钾可由咪唑与[氢氧化钾反应得到](../Page/氢氧化钾.md "wikilink")，它与卤代烃反应，生成*N*-烷基咪唑，后者在加热时可以异构化生成2-烷基咪唑。

## 制备

除了狄博斯法外，咪唑还可用多种方法合成。如果在反应中引入其它[官能团](../Page/官能团.md "wikilink")，这些方法也可以应用于咪唑[衍生物的合成](../Page/衍生物.md "wikilink")。在文献中，这些方法通常按照合成咪唑环时新形成的化学键数分类。例如，狄博斯方法在合成咪唑环时形成了（1,2）、（3,4和（1,5）三个键，所以这种方法是一个三键合成。以下列出几个有代表性的合成路线：

  - 单键合成反应

咪唑环的（1,5）或（3,4）键可在α-氨基二乙[缩醛与](../Page/缩醛.md "wikilink")[亚氨酸酯作用生成](../Page/亚氨酸酯.md "wikilink")[脒再进一步生成咪唑的反应中生成](../Page/脒.md "wikilink")。

[ImidazoleOneBondMethod.png](https://zh.wikipedia.org/wiki/File:ImidazoleOneBondMethod.png "fig:ImidazoleOneBondMethod.png")

  - 双键合成反应

高温加热1,2-脂肪二胺与乙醇、乙醛或羧酸混合物可生成咪唑环的（1,2）和（2,3）键。此反应需要脱氢催化剂如[铂或](../Page/铂.md "wikilink")[氧化铝的催化](../Page/氧化铝.md "wikilink")。

[ImidazoleRCOOHMethod.png](https://zh.wikipedia.org/wiki/File:ImidazoleRCOOHMethod.png "fig:ImidazoleRCOOHMethod.png")

工业上用[邻苯二胺与](../Page/邻苯二胺.md "wikilink")[甲酸反应生成](../Page/甲酸.md "wikilink")[苯并咪唑](../Page/苯并咪唑.md "wikilink")，用[双氧水处理](../Page/双氧水.md "wikilink")，开环转化为咪唑-4,5-二羧酸，最后与[氧化铜混合加热](../Page/氧化铜.md "wikilink")，发生脱羧生成咪唑。或者也可用[酒石酸与](../Page/酒石酸.md "wikilink")[发烟硝酸和](../Page/发烟硝酸.md "wikilink")[浓硫酸混合搅拌后](../Page/浓硫酸.md "wikilink")，得到酒石酸二[硝酸酯](../Page/硝酸酯.md "wikilink")，然后与[氨水](../Page/氨水.md "wikilink")、[甲醛反应](../Page/甲醛.md "wikilink")，生成的[二羰基丁二酸未经分离便转化为咪唑](../Page/二羰基丁二酸.md "wikilink")-4,5-二羧酸，最后脱羧生成咪唑。

加热*N*-取代α-氨基酮与[甲酰胺可形成咪唑环的](../Page/甲酰胺.md "wikilink")（1,2）和（3,4）键。产物是一个1,4-取代咪唑，如果R<sub>1</sub>
= R = H，则产物是咪唑。

[ImidazoleAminoketone.png](https://zh.wikipedia.org/wiki/File:ImidazoleAminoketone.png "fig:ImidazoleAminoketone.png")

此外，咪唑还可由链中含有氮原子的1,4-[二羰基化合物](../Page/二羰基化合物.md "wikilink")，在与铵盐加热时环化得到。原料二酮可通过α-[卤代酮](../Page/卤代酮.md "wikilink")[发生氨解得到α](../Page/Gabriel伯胺合成.md "wikilink")-氨基酮，然后再与[酸酐或](../Page/酸酐.md "wikilink")[酰氯反应制得](../Page/酰氯.md "wikilink")。

[ImidazoleDicarbonyl.png](https://zh.wikipedia.org/wiki/File:ImidazoleDicarbonyl.png "fig:ImidazoleDicarbonyl.png")

  - 四键合成反应

这是一个常见的生成取代咪唑的方法，由狄博斯法改良而来，[产率一般较高](../Page/产率.md "wikilink")。原料为取代[乙二醛](../Page/乙二醛.md "wikilink")、[醛](../Page/醛.md "wikilink")、[胺和](../Page/胺.md "wikilink")[氨或](../Page/氨.md "wikilink")[铵盐](../Page/铵盐.md "wikilink")。\[8\]

[ImidazoleFourBondMethod.png](https://zh.wikipedia.org/wiki/File:ImidazoleFourBondMethod.png "fig:ImidazoleFourBondMethod.png")

  - 由其他杂环化合物生成

咪唑可由1-[乙烯基四唑的](../Page/乙烯基四唑.md "wikilink")[光解反应合成](../Page/光解.md "wikilink")。如果1-乙烯基四唑可以大量地由[有机锡化合物](../Page/有机锡化合物.md "wikilink")（如2-三丁基锡基四氮唑）制得，反应收率较高。

[ImidazoleHeterocycle.png](https://zh.wikipedia.org/wiki/File:ImidazoleHeterocycle.png "fig:ImidazoleHeterocycle.png")

咪唑也可以由[气相反应制得](../Page/气相反应.md "wikilink")。[甲酰胺](../Page/甲酰胺.md "wikilink")、[乙二胺和](../Page/乙二胺.md "wikilink")[氢气在载](../Page/氢气.md "wikilink")[铂](../Page/铂.md "wikilink")[氧化铝催化下加热至](../Page/氧化铝.md "wikilink")340到480℃可生成纯度非常高的咪唑。

## 生物学意义和应用

在许多重要的生物分子中含有咪唑[官能团](../Page/官能团.md "wikilink")。最常见的是含有咪唑[侧链的](../Page/侧链.md "wikilink")[组氨酸](../Page/组氨酸.md "wikilink")。组氨酸出现在许多[蛋白质和](../Page/蛋白质.md "wikilink")[酶中](../Page/酶.md "wikilink")，也是[血红蛋白的重要结构组成](../Page/血红蛋白.md "wikilink")，对其配位能力具有重要意义。蛋白质中的His残基可以扮演酸碱两性基团，因此常是酶中发生酸碱催化机理的部分（如[碳酸酐酶](../Page/碳酸酐酶.md "wikilink")）。组氨酸可发生[脱羧生成](../Page/脱羧.md "wikilink")[组胺](../Page/组胺.md "wikilink")，这也是一种常见的[生物分子](../Page/生物分子.md "wikilink")，具有降低血压，收缩子宫等功能。细胞释放的组胺常是导致[荨麻疹的原因之一](../Page/荨麻疹.md "wikilink")。组氨酸生成组胺的反应如下所示：

[Histidine_decarboxylase.svg](https://zh.wikipedia.org/wiki/File:Histidine_decarboxylase.svg "fig:Histidine_decarboxylase.svg")

咪唑一个用途是在[金属螯合亲和层析](../Page/金属螯合亲和层析.md "wikilink")(IMAC)中用于[His标签蛋白的纯化](../Page/His标签蛋白.md "wikilink")。标签蛋白与[层析柱表面珠孔内的](../Page/层析柱.md "wikilink")[镍离子介质发生结合](../Page/镍.md "wikilink")，过量的咪唑通过层析柱，将与镍配位的标签蛋白洗脱下来，得到高纯度的目标蛋白。

咪唑是许多药品的重要组成部分。许多[杀菌剂和抗](../Page/杀菌剂.md "wikilink")[真菌](../Page/真菌.md "wikilink")、抗[原虫和抗](../Page/原虫.md "wikilink")[高血压的药物中含有合成咪唑](../Page/高血压.md "wikilink")。咪唑是[茶叶和](../Page/茶叶.md "wikilink")[咖啡豆中含有的](../Page/咖啡豆.md "wikilink")[茶碱分子的组成部分](../Page/茶碱.md "wikilink")，具有刺激[中枢神经系统的作用](../Page/中枢神经系统.md "wikilink")。通过干扰[DNA的活动抑制](../Page/DNA.md "wikilink")[白血病的抗癌药物](../Page/白血病.md "wikilink")[巯嘌呤中也含有咪唑](../Page/巯嘌呤.md "wikilink")。

## 工业应用

工业上咪唑被广泛用作某些[过渡金属](../Page/过渡金属.md "wikilink")（如[铜](../Page/铜.md "wikilink")）的[缓蚀剂](../Page/缓蚀剂.md "wikilink")。

许多工业上的重要化合物含有咪唑衍生物。耐高温的[聚苯并咪唑材料](../Page/聚苯并咪唑.md "wikilink")（PBI）含有[苯并咪唑](../Page/苯并咪唑.md "wikilink")（咪唑与苯环稠合）和另一个苯环相连的结构，PBI可用作阻燃剂。咪唑的各种化合物也常见于摄影和电子产品中。

## 参见

  - [4-甲基咪唑](../Page/4-甲基咪唑.md "wikilink")
  - 其他[二氮唑](../Page/二唑.md "wikilink")：[吡唑](../Page/吡唑.md "wikilink")（1,2-二氮唑）；
  - 其他1,3-[二唑](../Page/二唑.md "wikilink")：[噁唑](../Page/噁唑.md "wikilink")、[噻唑](../Page/噻唑.md "wikilink")；
  - [咪唑啉](../Page/咪唑啉.md "wikilink")（[二氢咪唑](../Page/二氢咪唑.md "wikilink")）、[苯并咪唑](../Page/苯并咪唑.md "wikilink")；
  - [吡咯](../Page/吡咯.md "wikilink")、[吡啶](../Page/吡啶.md "wikilink")

## 参考文献

[\*](../Category/咪唑.md "wikilink")
[Category:胺](../Category/胺.md "wikilink")
[Category:芳香环](../Category/芳香环.md "wikilink")

1.  Katritzky; Rees. *Comprehensive Heterocyclic Chemistry.* Vol. 5,
    p.469-498, (**1984**).
2.  Grimmett, M. Ross. *Imidazole and Benzimidazole Synthesis.* Academic
    Press, (**1997**).
3.  Brown, E. G. *Ring Nitrogen and Key Biomolecules.* Kluwer Academic
    Press, (**1998**).
4.  Pozharskii, A. F., *et al.* *Heterocycles in Life and Society.* John
    Wiley & Sons, (**1997**).
5.   ISBN 978-0-582-01421-3
6.  Heinrich Debus (1858). "Ueber die Einwirkung des Ammoniaks auf
    Glyoxal". *Annalen der Chemie und Pharmacie* **107** (2): 199 – 208.
    [doi](../Page/doi.md "wikilink"):[10.1002/jlac.18581070209](http://dx.doi.org/10.1002%2Fjlac.18581070209).
7.
8.  [US6,177,575](http://patft.uspto.gov/netacgi/nph-Parser?patentnumber=6,177,575)
    ([PDF
    version](http://www.pat2pdf.org/pat2pdf/foo.pl?number=6,177,575))
    (2001-01-23) Anthony J. Arduengo, III, *Process for Manufacture of
    Imidazoles.*