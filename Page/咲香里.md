**咲香-{里}-**，[日本女性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。[千葉縣出身](../Page/千葉縣.md "wikilink")。現在住[東京都](../Page/東京都.md "wikilink")。

## 概要

咲香-{里}-早年以「」筆名在[小學館的](../Page/小學館.md "wikilink")[少女漫畫雜誌](../Page/少女漫畫.md "wikilink")《別冊少女Comic》（）出道，曾任[森川讓次助手](../Page/森川讓次.md "wikilink")2年；後因畫[遊戲軟體的漫畫版而轉向畫](../Page/遊戲軟體.md "wikilink")[成人漫畫](../Page/成人漫畫.md "wikilink")，擅長[百合題材](../Page/百合_\(同人\).md "wikilink")。

1999年，咲香-{里}-在[講談社的](../Page/講談社.md "wikilink")[青年漫畫雜誌](../Page/青年漫畫.md "wikilink")《[Young
Magazine
Uppers](../Page/Young_Magazine_Uppers.md "wikilink")》（）連載《[思春期少女](../Page/思春期少女.md "wikilink")》（），正式轉向畫青年漫畫；當時咲香-{里}-很喜愛[羽球](../Page/羽球.md "wikilink")，故在《思春期少女》連載完畢後同樣在《Young
Magazine
Uppers》連載羽球漫畫《[大和羽球男](../Page/大和羽球男.md "wikilink")》（）。2004年10月19日，《Young
Magazine
Uppers》在發行2004年第21期以後停刊，《大和羽球男》同時連載完畢。2006年至2010年，咲香-{里}-在講談社的[少年漫畫雜誌](../Page/少年漫畫.md "wikilink")《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》連載另一部羽球漫畫《[羽球小子](../Page/羽球小子.md "wikilink")》（）。

## 作品

  - 以「」筆名

<!-- end list -->

  - 《[思春期少女](../Page/思春期少女.md "wikilink")》（），全11冊，講談社發行，[尖端出版代理台灣中文版](../Page/尖端出版.md "wikilink")
  - 《[大和羽球男](../Page/大和羽球男.md "wikilink")》（），全4冊，講談社發行，尖端出版代理台灣中文版
  - 《[羽球小子](../Page/羽球小子.md "wikilink")》（），全18冊，講談社發行，[東立出版社代理台灣中文版](../Page/東立出版社.md "wikilink")
  - 《少女的季節》（），全1冊，[笠倉出版社發行](../Page/笠倉出版社.md "wikilink")
  - 《太陽會落下》（），全3冊，笠倉出版社發行
  - 《》，全3冊，[富士美出版發行](../Page/富士美出版.md "wikilink")
  - 《》，全1冊，笠倉出版社發行

<!-- end list -->

  - 以「」筆名

<!-- end list -->

  - 《[復活邪神](../Page/復活邪神.md "wikilink")》（），全3冊，[德間書店Intermedia發行](../Page/德間書店.md "wikilink")

## 外部連結

  - [](https://web.archive.org/web/20080315191157/http://hw001.gate01.com/bad/)（作者公式網站）

  - \[<http://blog.livedoor.jp/kaorisaki228/>  -
    作者本人的舊[部落格](../Page/部落格.md "wikilink")

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")