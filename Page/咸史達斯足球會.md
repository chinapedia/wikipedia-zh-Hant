**咸史達斯足球會**（，-{zh-hant:大陸譯作**哈姆斯塔德**;
zh-hans:香港译作**咸史達斯**}-，[澳門譯作](../Page/澳門.md "wikilink")**咸史泰斯**），是一支位於[哈爾姆斯塔德的](../Page/哈爾姆斯塔德.md "wikilink")[瑞典足球會](../Page/瑞典.md "wikilink")。咸史達斯於1914年3月6日成立，贏過四次本土聯賽冠軍，和一次本土杯賽冠軍。即使由於咸史達斯的所在地規模較小，資源不多，但咸史達斯也是一支成功的球會。他們有一個連續教了七年的教練，在[瑞典足球超級聯賽角逐](../Page/瑞典足球超級聯賽.md "wikilink")。

咸史達斯在1995年開始獲得注目。當年的[歐洲杯賽冠軍杯](../Page/歐洲杯賽冠軍杯.md "wikilink")，咸史達斯首回合在舊烏利維球場以3-0擊敗意大利球會[帕爾馬](../Page/帕尔玛足球俱乐部.md "wikilink")，但次會合在[帕爾馬市被](../Page/帕尔马_\(意大利\).md "wikilink")[帕爾馬反勝](../Page/帕尔玛足球俱乐部.md "wikilink")4-0,結果被淘汰。

## 著名球員

  - [阿歷山達臣](../Page/阿歷山達臣.md "wikilink")
  - [Rutger Backe](../Page/Rutger_Backe.md "wikilink")
  - [Dusan Djuric](../Page/Dusan_Djuric.md "wikilink")
  - [Tommy Jönsson](../Page/Tommy_Jönsson.md "wikilink")
  - [Mats Lilienberg](../Page/Mats_Lilienberg.md "wikilink")
  - [弗雷德里克·永贝里](../Page/弗雷德里克·永贝里.md "wikilink")
  - [Mikael Nilsson](../Page/Mikael_Nilsson_\(born_1978\).md "wikilink")
  - [羅辛保](../Page/羅辛保.md "wikilink")
  - [Stefan Selakovic](../Page/Stefan_Selakovic.md "wikilink")
  - [米高·史雲遜](../Page/米高·史雲遜.md "wikilink")

## 成就

  - **[瑞典足球冠军](../Page/瑞典足球冠军.md "wikilink"):**
      - **冠軍 (4):** 1976, 1979, 1997, 2000

<!-- end list -->

  - **[瑞典足球超級聯賽](../Page/瑞典足球超級聯賽.md "wikilink"):**
      - **冠軍 (4):** 1976, 1979, 1997, 2000
      - **亞軍 (2):** 1954-55, 2004

<!-- end list -->

  - **[瑞典盃](../Page/瑞典盃.md "wikilink"):**
      - **冠軍 (1):** 1994–95

## 外部連結

  - [Halmstads BK](http://www.hbk.se/) - 官方網站
  - [Kvastarna](https://web.archive.org/web/20060513170638/http://www.svenskidrott.se/n/kvastarna/)
    - 官方球迷會網站
  - [Himlen är blå](http://www.svenskafans.com/fotboll/hbk/) - 球迷網站

[Category:瑞典足球俱樂部](../Category/瑞典足球俱樂部.md "wikilink")