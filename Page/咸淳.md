**咸淳**（1265年－1274年）是[宋度宗赵禥的](../Page/宋度宗.md "wikilink")[年号](../Page/年号.md "wikilink")。[南宋使用这个年号共](../Page/南宋.md "wikilink")10年。

咸淳十年七月[宋恭帝即位沿用](../Page/宋恭帝.md "wikilink")\[1\]。

## 纪年

| 咸淳                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1265年                          | 1266年                          | 1267年                          | 1268年                          | 1269年                          | 1270年                          | 1271年                          | 1272年                          | 1273年                          | 1274年                          |
| [干支](../Page/干支纪年.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [紹隆](../Page/紹隆.md "wikilink")（1258年至1278年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳聖宗陳晃之年號](../Page/陳聖宗.md "wikilink")
      - [至元](../Page/至元_\(元世祖\).md "wikilink")（1264年八月—1294年十二月）：元—元世祖忽必烈之年號
      - [文永](../Page/文永.md "wikilink")（1264年二月廿八至1275年四月廿五）：日本龜山天皇與[後宇多天皇年號](../Page/後宇多天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:南宋年号](../Category/南宋年号.md "wikilink")
[Category:13世纪中国年号](../Category/13世纪中国年号.md "wikilink")
[Category:1260年代中国政治](../Category/1260年代中国政治.md "wikilink")
[Category:1270年代中国政治](../Category/1270年代中国政治.md "wikilink")

1.