**咸鏡南道**（）是[日本統治時代朝鮮的行政區劃](../Page/朝鮮日佔時期.md "wikilink")，包括了現[朝鮮民主主義人民共和國的](../Page/朝鮮民主主義人民共和國.md "wikilink")[咸鏡南道還有](../Page/咸鏡南道.md "wikilink")[江原道跟](../Page/江原道.md "wikilink")[兩江道的一部份](../Page/兩江道.md "wikilink")

## 人口

昭和11年（1936年）現住戶口調査

  - 總人口　1,602,178人
      - 日本人　51,052人
      - 朝鮮人　1,544,883人
      - 其他　6,243人

## 行政區劃

昭和11年（1936年）

### 府

  - [元山府](../Page/元山市.md "wikilink")

  - [咸興府](../Page/咸興市.md "wikilink")

  -
### 郡

  - [咸州郡](../Page/咸州郡.md "wikilink")
      - 興南邑、州北面、岐谷面、下岐川面、上岐川面、下朝陽面、上朝陽面、川西面、川原面、州西面、朱地面、連浦面、三平面、雲南面、西湖面、西退潮面、東川面、徳山面
  - [定平郡](../Page/定平郡.md "wikilink")
      - 府内面、広徳面、長原面、春柳面、文山面、帰林面、宣徳面、朱伊面、高山面
  - [永興郡](../Page/永興郡.md "wikilink")
      - 洪仁面、福興面、順寧面、憶岐面、鎮坪面、虎島面、古寧面、仁興面、徳興面、長興面、宣興面、耀徳面、横川面
  - [高原郡](../Page/高原郡.md "wikilink")
      - 下鉢面、上山面、郡内面、水洞面、山谷面、雲谷面
  - [文川郡](../Page/文川市.md "wikilink")
      - 文川面、明亀面、都草面、雲林面
  - [徳源郡](../Page/元山市.md "wikilink")
      - 府内面、北城面、赤田面、県面、豊上面、豊下面
  - [安邊郡](../Page/安邊郡.md "wikilink")
      - 鶴城面、安道面、培花面、瑞谷面、文山面、衛益面、新茅面
  - [洪原郡](../Page/洪原郡.md "wikilink")
      - 洪原面、景雲面、龍源面、龍浦面、普賢面、雲鶴面、甫青面
  - [北青郡](../Page/北青郡.md "wikilink")
      - 北青邑、新北青面、居山面、新昌面、俗厚面、陽化面、新浦面、厚昌面、佳会面、上車書面、下車書面、徳城面、星岱面、泥谷面
  - [利原郡](../Page/利原郡.md "wikilink")
      - 東面、西面、南面
  - [端川郡](../Page/端川市.md "wikilink")
      - 波道面、福貴面、何多面、新満面、水下面、利中面、広泉面、南斗日面、北斗日面
  - [新興郡](../Page/新興郡.md "wikilink")
      - 加平面、元平面、東古川面、西古川面、永高面、上元川面、下元川面、東上面
  - [長津郡](../Page/長津郡.md "wikilink")
      - 郡内面、上南面、中南面、西閑面、新南面、北面、東下面
  - [豐山郡](../Page/金亨權郡.md "wikilink")
      - 里仁面、熊耳面、安水面、安山面、天南面
  - [三水郡](../Page/三水郡.md "wikilink")
      - 三南面、襟水面、別東面、好仁面、江鎮面、自西面、三西面、館興面
  - [甲山郡](../Page/甲山郡.md "wikilink")
      - 恵山邑、長平面、山南面、鎮東面、会麟面、同仁面、雲興面、普天面

[分類:1910年建立的行政區劃](../Page/分類:1910年建立的行政區劃.md "wikilink")
[分類:1945年廢除的行政區劃](../Page/分類:1945年廢除的行政區劃.md "wikilink")

[Category:朝鮮日治時期](../Category/朝鮮日治時期.md "wikilink")