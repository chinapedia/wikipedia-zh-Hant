**咸鏡道**（）是昔日[朝鮮八道之一](../Page/朝鮮八道.md "wikilink")，位於[朝鮮半島東北部](../Page/朝鮮半島.md "wikilink")，道府位於[咸興](../Page/咸興.md "wikilink")。

## 沿革

始於1413年，稱**永吉道** （）。1416年，稱**咸吉道** （）1470年改稱**永安道**
（），1509年改稱現名。道名由咸興（）和[鏡城](../Page/鏡城郡.md "wikilink")（）合成。這些地方之前土著是[女真人](../Page/女真人.md "wikilink")。[朝鲜世宗派](../Page/朝鲜世宗.md "wikilink")[金宗瑞发兵此地使豆满江成为两国疆界](../Page/金宗瑞.md "wikilink")。

在燕山君時代曾改名永安道。

1895年，朝鮮行[二十三府制](../Page/二十三府制.md "wikilink")。本道被分為三府，即南部的咸興府、西北部的甲山府（）和東北部的鏡城府。翌年即廢，咸興府改制為[咸鏡南道](../Page/咸鏡南道.md "wikilink")，甲山、鏡城兩府合併為[咸鏡北道](../Page/咸鏡北道.md "wikilink")。兩道目前皆為[朝鮮民主主義人民共和國所轄](../Page/朝鮮民主主義人民共和國.md "wikilink")。

## 地理

本道傳統上分為[關南地方](../Page/關南地方.md "wikilink")、[關北地方兩部分](../Page/關北地方.md "wikilink")。東為[日本海](../Page/日本海.md "wikilink")，西為[平安道](../Page/平安道.md "wikilink")，南為[平安道](../Page/平安道.md "wikilink")，北為[中國](../Page/中國.md "wikilink")
（後來還有[俄羅斯](../Page/俄羅斯.md "wikilink")）。

[Category:朝鲜王朝的道](../Category/朝鲜王朝的道.md "wikilink")
[Category:咸镜南道历史](../Category/咸镜南道历史.md "wikilink")
[Category:咸镜北道历史](../Category/咸镜北道历史.md "wikilink")
[Category:1500年代建立的行政区划](../Category/1500年代建立的行政区划.md "wikilink")
[Category:1895年废除的行政区划](../Category/1895年废除的行政区划.md "wikilink")