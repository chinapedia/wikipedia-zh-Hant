**咸阳市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[陕西省下辖的](../Page/陕西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于[关中平原中部](../Page/关中平原.md "wikilink")、[渭河沿岸](../Page/渭河.md "wikilink")；距[西安](../Page/西安.md "wikilink")25千米；[黄河的第一大支流](../Page/黄河.md "wikilink")——[渭河流经市区](../Page/渭河.md "wikilink")。咸阳辖区面积10196平方千米，总人口504万人\[1\]，下辖3个[市辖区](../Page/市辖区.md "wikilink")、2个[县级市和](../Page/县级市.md "wikilink")9个[县](../Page/县.md "wikilink")，拥有一个[国家级高新技术产业开发区和一个国家高新技术创业服务中心](../Page/国家级高新技术产业开发区.md "wikilink")；中国第七个[国家级新区](../Page/国家级新区.md "wikilink")、首个以创新城市发展方式为主题的国家级新区[西咸新区的大部分](../Page/西咸新区.md "wikilink")（包括秦汉新城、沣西新城、[空港新城和泾河新城的全部以及沣东新城的部分区域](../Page/空港新城.md "wikilink")）均位于咸阳市境内；中国目前唯一的农业高新技术产业开发区也于1997年在咸阳市[杨陵区成立](../Page/杨陵区.md "wikilink")。

咸陽是中国著名古都，是中国历史上第一个中央集权王朝——[秦朝的都城所在地](../Page/秦朝.md "wikilink")，在[周朝](../Page/周朝.md "wikilink")、[西汉和](../Page/西汉.md "wikilink")[唐朝均为京畿重地](../Page/唐朝.md "wikilink")。有两千多年的建城史的咸阳，是中国国家级历史文化名城、中国优秀旅游城市、中国甲级对外开放城市、全国双拥模范城、国家卫生城市；2004年，被评为首届中国魅力城市；2005年当选中国十佳宜居地级市。2014年被授予国家园林城市。\[2\]

咸阳市位于[关中-天水经济区核心区域](../Page/关中-天水经济区.md "wikilink")，是[国家发改委确定的西安](../Page/国家发改委.md "wikilink")（咸阳）大都市的两大组成部分之一。

## 历史

据[李吉甫](../Page/李吉甫.md "wikilink")《元和郡县志》解释：“山南曰‘阳’，水北也称‘阳’”。而咸阳正是地处[九嵕山之南](../Page/九嵕山.md "wikilink")，[渭水之北](../Page/渭水.md "wikilink")，山水俱阳，“咸”即为“皆”之意，故名“咸阳”。另据《[史记](../Page/史记.md "wikilink")》和秦都咸阳出土的陶文，以为[商鞅在此曾置](../Page/商鞅.md "wikilink")“咸亭”、“阳里”，[秦孝公将两名合一](../Page/秦孝公.md "wikilink")，为“咸阳”。

咸阳因历史悠久，[文物](../Page/文物.md "wikilink")[遗址众多被人们称为](../Page/遗址.md "wikilink")“天然[历史](../Page/历史.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")”。它的[历史可以上溯到](../Page/历史.md "wikilink")4000多年以前的[新石器时代](../Page/新石器时代.md "wikilink")，曾是11个王朝的[京畿属地](../Page/京畿.md "wikilink")，历史文物方迹遍布全市。

前350年，[秦孝公由](../Page/秦孝公.md "wikilink")[栎阳迁都咸阳](../Page/栎阳.md "wikilink")，在此营筑翼阙及宫殿。[秦始皇仿建六国宫殿](../Page/秦始皇.md "wikilink")，使[咸阳城成为规模恢宏的帝都](../Page/咸阳城.md "wikilink")。后置扶风郡。西汉建立（公元前206年）之时，[咸阳先后改名为](../Page/咸陽縣.md "wikilink")[新城和](../Page/新城县.md "wikilink")[渭城](../Page/渭城县.md "wikilink")。在今咸阳原上，因[西汉](../Page/西汉.md "wikilink")[五陵置有](../Page/五陵.md "wikilink")[陵邑](../Page/陵邑.md "wikilink")，故有“五陵原”之称。[西晋析置](../Page/西晋.md "wikilink")[始平郡](../Page/始平郡.md "wikilink")。[前秦置](../Page/前秦.md "wikilink")[咸阳郡](../Page/咸阳郡.md "wikilink")。[北魏](../Page/北魏.md "wikilink")，并咸阳县于泾阳县。[隋朝并入](../Page/隋朝.md "wikilink")[京兆郡](../Page/京兆郡.md "wikilink")。[唐朝重置咸阳县](../Page/唐朝.md "wikilink")，建[咸阳城](../Page/唐咸阳城.md "wikilink")。[武则天因其母](../Page/武则天.md "wikilink")[杨氏的顺陵在咸阳北原](../Page/孝明高皇后.md "wikilink")，曾改咸阳县为“赤县”。唐神龙元年（公元705年），升为次[畿](../Page/畿.md "wikilink")。[五代](../Page/五代.md "wikilink")、[宋](../Page/北宋.md "wikilink")、[金皆称](../Page/金朝.md "wikilink")“咸阳”。[元朝初时](../Page/元朝.md "wikilink")，一度将咸阳并入兴平，不久又恢复咸阳县制。[明洪武四年](../Page/明朝.md "wikilink")（1371年），将[县城迁到渭水驿](../Page/明咸阳城.md "wikilink")，即现在“秦都区”所在地。[明](../Page/明朝.md "wikilink")、[清均称](../Page/清朝.md "wikilink")“咸阳”，属[西安府管辖](../Page/西安府.md "wikilink")。

[中华民国时期](../Page/中华民国.md "wikilink")，咸阳县先是归[关中道管辖](../Page/关中道.md "wikilink")，不久废道直属陕西省政府。1937年后，属咸阳第十行政督察专员公署。1949年5月18日，[解放军进驻咸阳县](../Page/中国人民解放军.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，设咸阳分区，初辖咸阳、兴平、武功、长安、户县、周至六县\[3\]。1950年，根据[陕西省人民政府成立后发布的区域划分命令](../Page/陕西省人民政府.md "wikilink")，[咸阳专区辖咸阳](../Page/咸阳专区.md "wikilink")、[三原](../Page/三原.md "wikilink")、泾阳、淳化、[栒邑](../Page/旬邑.md "wikilink")、[醴泉](../Page/礼泉县.md "wikilink")、兴平、[盩厔](../Page/周至.md "wikilink")、[鄠县](../Page/户县.md "wikilink")、耀县、铜川、富平、高陵13县。1952年，设县级咸阳市。1953年1月，咸阳分区撤销；下辖咸阳市、咸阳县、鄠县、铜川由省政府直辖，其他郊县分划[渭南](../Page/渭南.md "wikilink")、[宝鸡专署](../Page/宝鸡.md "wikilink")。

1958年，咸阳市、县合并，成立新的咸阳市（县级市）。1961年10月，复设咸阳专区；下辖咸阳市（县级市）和三原、泾阳、淳化、栒邑、长武、邠县、醴泉、永寿、乾县、兴平、盩厔、鄠县、高陵13县。1966年6月，咸阳市（县级市）划归西安市管辖；至1971年，重新划归咸阳地区。

1983年10月5日，经[中国国务院批准](../Page/中华人民共和国国务院.md "wikilink")，[咸阳地区改为](../Page/咸阳地区.md "wikilink")**咸阳市**（省辖市），原咸阳市（县级市）改为秦都区，[周至](../Page/周至.md "wikilink")、[户县](../Page/户县.md "wikilink")、[高陵划归](../Page/高陵.md "wikilink")[西安市](../Page/西安市.md "wikilink")，原属[宝鸡市的](../Page/宝鸡市.md "wikilink")[武功县和](../Page/武功县.md "wikilink")[杨陵区划归新设的咸阳市](../Page/杨陵区.md "wikilink")（省辖市）。1984年5月，咸阳地区改为[省辖市后](../Page/省辖市.md "wikilink")，原县级市更名“秦都区”。

2007年7月，所属[杨陵区划归](../Page/杨陵区.md "wikilink")[国家杨凌农业高新技术产业示范区管辖](../Page/杨凌农业高新技术产业示范区.md "wikilink")。但由于农业高新区的特殊性，[国家杨凌农业高新技术产业示范区并未被](../Page/杨凌农业高新技术产业示范区.md "wikilink")[民政部列为行政区域](../Page/民政部.md "wikilink")，其下辖的县级[杨陵区在名义上仍为咸阳市市辖区](../Page/杨陵区.md "wikilink")。

2017年1月，咸阳市所辖的秦都、渭城、泾阳、兴平4个县市区中的15个乡镇街道、60.5万人口交由[西安市代管](../Page/西安市.md "wikilink")。\[4\]

2018年5月，经国务院批准，同意撤销彬县，设立县级[彬州市](../Page/彬州市.md "wikilink")，由陕西省直辖，咸阳市代管。\[5\]

## 地理

咸阳市地处陕西省[关中盆地的中部](../Page/关中盆地.md "wikilink")，地势北高南低，呈阶梯状，高差明显，界限清晰，黄土高原、平原居主导地位。全市最高点位于东北部的石门山峰海拔1826米，最低处在东南部的三原县大程镇清河出境地，海拔366米。北部是黄土高原南缘的一部分，大体以[泾河为界](../Page/泾河.md "wikilink")，西南部是黄土丘陵沟壑区，东北部是残原黄土沟壑和土石低中山，南部为[渭河盆地](../Page/渭河盆地.md "wikilink")，属[关中平原的一部分](../Page/关中平原.md "wikilink")，地势平坦，水土流失轻。盆地又可分为[泾渭冲积平原和黄土台原](../Page/泾渭冲积平原.md "wikilink")，从北向南呈阶梯状分布。

境内水资源主要由河川径流和地下水组成。河流水系属黄河流域渭河水系，渭河干流从南缘流过，在市境汇入的主要支流有漆水河、新河、沣河、泾河、石川河，其中泾河最大，形成了泾河、渭河两大水系。地下水资源南富北贫，由于连续干旱，地表水供给不足，地下水开采过度，致使有的河流和池塘干涸，地下水位持续下降。过境客水较多，但利用难度较大。地表水与地下水分布极不平衡。

## 政治

### 现任领导

<table>
<caption>咸阳市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党咸阳市委员会.md" title="wikilink">中国共产党<br />
咸阳市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/咸阳市人民代表大会.md" title="wikilink">咸阳市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/咸阳市人民政府.md" title="wikilink">咸阳市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议咸阳市委员会.md" title="wikilink">中国人民政治协商会议<br />
咸阳市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/岳亮.md" title="wikilink">岳亮</a>[6]</p></td>
<td><p><a href="../Page/卫华.md" title="wikilink">卫华</a>（女）[7]</p></td>
<td><p><a href="../Page/贺书田.md" title="wikilink">贺书田</a>[8]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/邯郸市.md" title="wikilink">邯郸市</a></p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/许昌市.md" title="wikilink">许昌市</a></p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/丹凤县.md" title="wikilink">丹凤县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年4月</p></td>
<td><p>2016年2月</p></td>
<td><p>2013年4月</p></td>
<td><p>2015年8月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市共辖3个[市辖区](../Page/市辖区.md "wikilink")、9个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管2个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[秦都区](../Page/秦都区.md "wikilink")、[渭城区](../Page/渭城区.md "wikilink")、[杨陵区](../Page/杨陵区.md "wikilink")
  - 县级市：[兴平市](../Page/兴平市.md "wikilink")、[彬州市](../Page/彬州市.md "wikilink")
  - 县：[三原县](../Page/三原县.md "wikilink")、[泾阳县](../Page/泾阳县.md "wikilink")、[乾县](../Page/乾县.md "wikilink")、[礼泉县](../Page/礼泉县.md "wikilink")、[永寿县](../Page/永寿县.md "wikilink")、[长武县](../Page/长武县.md "wikilink")、[旬邑县](../Page/旬邑县.md "wikilink")、[淳化县](../Page/淳化县.md "wikilink")、[武功县](../Page/武功县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>咸阳市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[9]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>610400</p></td>
</tr>
<tr class="odd">
<td><p>610402</p></td>
</tr>
<tr class="even">
<td><p>610403</p></td>
</tr>
<tr class="odd">
<td><p>610404</p></td>
</tr>
<tr class="even">
<td><p>610422</p></td>
</tr>
<tr class="odd">
<td><p>610423</p></td>
</tr>
<tr class="even">
<td><p>610424</p></td>
</tr>
<tr class="odd">
<td><p>610425</p></td>
</tr>
<tr class="even">
<td><p>610426</p></td>
</tr>
<tr class="odd">
<td><p>610428</p></td>
</tr>
<tr class="even">
<td><p>610429</p></td>
</tr>
<tr class="odd">
<td><p>610430</p></td>
</tr>
<tr class="even">
<td><p>610431</p></td>
</tr>
<tr class="odd">
<td><p>610481</p></td>
</tr>
<tr class="even">
<td><p>610482</p></td>
</tr>
<tr class="odd">
<td><p>注：杨陵区实际上由<a href="../Page/杨凌农业高新技术产业示范区.md" title="wikilink">杨凌农业高新技术产业示范区管辖</a>，于省内其数据可单列，国家行政区划和统计标准仍将其数据划入咸阳市；秦都区数字包含咸阳高新技术产业园区所辖渭滨街道。渭城区北杜镇、底张街道、周陵街道、渭城街道、窑店街道、正阳街道，秦都区钓台街道、沣东街道、双照街道，兴平市南位镇，泾阳县泾干镇、崇文镇、太平镇、高庄镇、永乐镇实际由<a href="../Page/西安市.md" title="wikilink">西安市代管</a>。[10]</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>咸阳市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[11]（2010年11月）</p></th>
<th><p>户籍人口[12]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>咸阳市</p></td>
<td><p>5096001</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>秦都区</p></td>
<td><p>507093</p></td>
<td><p>9.95</p></td>
</tr>
<tr class="even">
<td><p>杨陵区</p></td>
<td><p>201172</p></td>
<td><p>3.95</p></td>
</tr>
<tr class="odd">
<td><p>渭城区</p></td>
<td><p>438327</p></td>
<td><p>8.60</p></td>
</tr>
<tr class="even">
<td><p>三原县</p></td>
<td><p>403524</p></td>
<td><p>7.92</p></td>
</tr>
<tr class="odd">
<td><p>泾阳县</p></td>
<td><p>487749</p></td>
<td><p>9.57</p></td>
</tr>
<tr class="even">
<td><p>-{乾}-　县</p></td>
<td><p>527088</p></td>
<td><p>10.34</p></td>
</tr>
<tr class="odd">
<td><p>礼泉县</p></td>
<td><p>447771</p></td>
<td><p>8.79</p></td>
</tr>
<tr class="even">
<td><p>永寿县</p></td>
<td><p>184642</p></td>
<td><p>3.62</p></td>
</tr>
<tr class="odd">
<td><p>彬　县</p></td>
<td><p>323256</p></td>
<td><p>6.34</p></td>
</tr>
<tr class="even">
<td><p>长武县</p></td>
<td><p>167570</p></td>
<td><p>3.29</p></td>
</tr>
<tr class="odd">
<td><p>旬邑县</p></td>
<td><p>261566</p></td>
<td><p>5.13</p></td>
</tr>
<tr class="even">
<td><p>淳化县</p></td>
<td><p>193377</p></td>
<td><p>3.79</p></td>
</tr>
<tr class="odd">
<td><p>武功县</p></td>
<td><p>411312</p></td>
<td><p>8.07</p></td>
</tr>
<tr class="even">
<td><p>兴平市</p></td>
<td><p>541554</p></td>
<td><p>10.63</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")4894834人\[13\]（不含杨陵区，下同），同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加56080人，增长1.16%。年平均增长率为0.12%。其中，男性人口为2512545人，占51.33%；女性人口为
2382289人，占48.67%。总人口性别比（以女性为100）为105.47。0－14岁人口为746446人，占15.25%；15－64岁人口为3728386人，占76.17%；65岁及以上人口为420002人，占8.58%。

据人口变动抽样调查推算，2013年末全市常住人口为494.22万人。

2014年末，咸阳市常住人口495.68万人，比上年末增加1.46万人。全年出生人口5.03万人，人口出生率10.14‰；死亡人口3.05万人，人口死亡率6.16‰；人口增长率3.98‰，比上年下降0.04个千分点。从城乡结构看，城镇常住人口236.59万人，比上年末增加6.53万人；乡村常住人口259.09万人，减少5.07万人，城镇人口占总人口比重为47.73%。\[14\]

2016年末，咸阳市常住人口498.66万人，比上年末增加1.42万人。全年出生人口5.2万人，人口出生率10.43‰；死亡人口3.05万人，人口死亡率6.12‰；人口增长率4.31‰，比上年提高0.36个千分点。从城乡结构看，城镇常住人口253.52万人，比上年末增加9.38万人；乡村常住人口245.14万人，减少7.96万人；城镇人口占总人口比重为50.84%，比上年提高1.74个百分点。

## 经济

咸阳是[陕西省工业中心之一](../Page/陕西省.md "wikilink")，以纺织、电子为中心，兼有电力、建材、橡胶、机械等工业部门。棉织、毛纺、化纤产品畅销国内外。咸阳是陕西省重要的粮棉产区，向国家提供的粮、肉、蛋、菜人均数量居全省之冠。著名的企业有：[彩虹集团](../Page/彩虹集团.md "wikilink")（中国最大的显象管生产企业）、西北橡胶总厂（中国大型综合工业橡胶和军用橡胶制品厂，品牌“双西”，始建于1959年），陕西棉纺织系统工厂（包括[西北国棉一厂（现已破产）](../Page/西北国棉一厂（现已破产）.md "wikilink")\[15\]、二厂、七厂和八厂）。

咸阳又以自己的[工业特色被誉称为](../Page/工业.md "wikilink")“[纺织](../Page/纺织.md "wikilink")[电子城](../Page/电子.md "wikilink")”。纺织工业成龙配套，从棉纺、毛纺到[化纤](../Page/化纤.md "wikilink")，从[纱](../Page/纱.md "wikilink")、[布](../Page/布.md "wikilink")、针织到[印染](../Page/印染.md "wikilink")，从纺织大机到[器材](../Page/器材.md "wikilink")、[配件制造](../Page/配件.md "wikilink")，形成了比较完整的纺织工业体系。电子工业由只生产军品，发展到“军民结合”，由只生产元部件，发展到组装整机。闻名全国的陕西彩色显像管总厂就设在这裡。

## 交通

[Xian_Xianyang_International_Airport_Tower.JPG](https://zh.wikipedia.org/wiki/File:Xian_Xianyang_International_Airport_Tower.JPG "fig:Xian_Xianyang_International_Airport_Tower.JPG")

  - 公路：[福银高速](../Page/福银高速.md "wikilink")、[咸旬高速](../Page/咸旬高速.md "wikilink")、[连霍高速](../Page/连霍高速.md "wikilink")、[西安咸阳国际机场专用高速公路](../Page/西安咸阳国际机场专用高速公路.md "wikilink")、[西安绕城高速公路](../Page/西安绕城高速公路.md "wikilink")、[312国道](../Page/312国道.md "wikilink")
  - 铁路：[陇海铁路](../Page/陇海铁路.md "wikilink")、[咸铜铁路](../Page/咸铜铁路.md "wikilink")、[西宝客运专线](../Page/西宝客运专线.md "wikilink")
  - [西安咸阳国际机场是西北最大的国际航空港](../Page/西安咸阳国际机场.md "wikilink")，国内外航线有40多条。
  - 公共交通：咸阳市公共交通系统较为发达，市内拥有公交线路30余条，大多实行一元通票制，不仅可通达市内大部分区域，同时更有多条公交线路于省城西安无缝对接，成为国内城际公交互通的先行者。
  - 轨道交通：[西安地铁1号线西延段终点设在咸阳市](../Page/西安地铁1号线.md "wikilink")[秦都区扶苏路的](../Page/秦都区.md "wikilink")[咸阳森林公园](../Page/咸阳森林公园.md "wikilink")。
  - 参见[咸阳公交线路](../Page/咸阳公交线路.md "wikilink")

## 文物与旅游

咸阳是中国历史上第一个封建帝国秦的首都，又是汉、唐等十余个王朝的京畿重地，是闻名世界的古[丝绸之路的第一站](../Page/丝绸之路.md "wikilink")。悠久的历史给咸阳留下了极为丰富的历史文化遗产，古遗址众多，内涵丰富；古墓葬分布广泛，最为集中。古建筑主要有旬邑泰塔、彬县开元寺塔、泾阳崇文塔、三原城隍庙等。石刻主要有彬县大佛寺石窟等。据统计，全市有各类文物点5313处，其中古遗址1037处，古墓葬1135处，古建筑247处。

咸阳市旅游资源丰富，著名景点星罗棋布，是[陕西省乃至中国的文物大市](../Page/陕西省.md "wikilink")。咸阳號稱[周朝](../Page/周朝.md "wikilink")、[秦朝](../Page/秦朝.md "wikilink")、[漢朝](../Page/漢朝.md "wikilink")、[唐朝文物宝库](../Page/唐朝.md "wikilink")，拥有[世界文化遗产](../Page/世界文化遗产.md "wikilink")1处、[国家考古遗址公园](../Page/国家考古遗址公园.md "wikilink")2处、[国家一级博物馆](../Page/国家一级博物馆.md "wikilink")1座、[国家森林公园](../Page/国家森林公园.md "wikilink")1处，另有古邰国、秦[咸阳宫](../Page/咸阳宫.md "wikilink")、[郑国渠等重要古代遗址](../Page/郑国渠.md "wikilink")9处，规模宏大的漢唐帝王陵群21处，千佛塔、秦、漢[兵马俑](../Page/汉兵马俑.md "wikilink")、[漢茂陵](../Page/漢茂陵.md "wikilink")、[咸阳博物馆](../Page/咸阳博物馆.md "wikilink")、[漢陽陵等](../Page/漢陽陵.md "wikilink")。[東漢断代史家](../Page/東漢.md "wikilink")[班固](../Page/班固.md "wikilink")、投筆從戎再通西域的[班超](../Page/班超.md "wikilink")、曹大家[班昭兄妹](../Page/班昭.md "wikilink")（皆為[班彪之子](../Page/班彪.md "wikilink")、扶風安陵人）的故郷在今日咸陽。

咸阳市区境内的亦有以北平街为主要入口之一的明清老城遗址区，老城内存有全国重点文物保护单位——咸阳文庙、安国寺、圣母宫等古建筑和咸阳市内唯一的明城墙遗址[凤凰台](../Page/凤凰台.md "wikilink")。

咸阳湖，是西北地区水域面积最大的人工湖，著名的[关中八景之一咸阳古渡遗址就坐落在咸阳湖畔](../Page/关中八景.md "wikilink")。东起渭城区古渡公园，西至秦都区咸通南路；南邻咸阳湖南苑广场，并于西咸新区的主干道——世纪大道相接，北抵咸阳市区最大的广场——统一广场。
统一广场是2008年北京奥运会火炬接力咸阳站的起点。

主要旅游景点：

  - [世界文化遗产](../Page/世界文化遗产.md "wikilink"):[彬县大佛寺](../Page/彬县大佛寺.md "wikilink")
  - [秦咸阳城遗址](../Page/秦咸阳城遗址.md "wikilink")
  - [大佛寺石窟](../Page/大佛寺石窟.md "wikilink")
  - [昭仁寺大殿](../Page/昭仁寺大殿.md "wikilink")
  - [西漢帝陵](../Page/西漢帝陵.md "wikilink")
      - [漢长陵](../Page/漢长陵.md "wikilink")
      - [漢陽陵](../Page/漢陽陵.md "wikilink")
      - [漢茂陵](../Page/漢茂陵.md "wikilink")
          - [霍去病墓](../Page/霍去病墓.md "wikilink")
  - [隋泰陵](../Page/隋泰陵.md "wikilink")
  - [乾陵](../Page/乾陵.md "wikilink")
  - [昭陵](../Page/昭陵.md "wikilink")
  - [三原城隍庙](../Page/三原城隍庙.md "wikilink")
  - [郑国渠首遗址](../Page/郑国渠首遗址.md "wikilink")
  - [甘泉宫遗址](../Page/甘泉宫遗址.md "wikilink")
  - [咸阳文庙](../Page/咸阳文庙.md "wikilink")
  - [泾阳崇文塔](../Page/泾阳崇文塔.md "wikilink")
  - [安吴青训班](../Page/安吴青训班.md "wikilink")
  - [泰塔](../Page/泰塔.md "wikilink")
  - [北杜铁塔](../Page/北杜铁塔.md "wikilink")
  - [沙河古桥遗址](../Page/沙河古桥遗址.md "wikilink")
  - [秦直道起点遗址](../Page/秦直道起点遗址.md "wikilink")
  - [咸阳钟楼](../Page/咸阳钟楼.md "wikilink")
  - [咸阳古渡遗址](../Page/咸阳古渡遗址.md "wikilink")
  - [咸阳湖风景区](../Page/咸阳湖风景区.md "wikilink")
  - [石门国家森林公园](../Page/石门国家森林公园.md "wikilink")
  - [马栏革命旧址](../Page/马栏革命旧址.md "wikilink")

<File:Xian> Xianyang Airport 9547.JPG|咸阳市境内的西安咸阳国际机场
[File:茂陵博物馆额匾.JPG|茂陵博物馆](File:茂陵博物馆额匾.JPG%7C茂陵博物馆)

## 宗教

明代以前，境内汉族多信佛教或道教，回族信仰伊斯兰教。明末清初，天主教传入境内。1840年鸦片战争后，天主教和基督新教遍及各县，相继成为境内信徒较多、影响较大的宗教。

“[文化大革命](../Page/文化大革命.md "wikilink")”中，境内所有宗教被迫停止活动。[中共十一届三中全会以后](../Page/中共十一届三中全会.md "wikilink")，归还了教堂、寺观等宗教财产，恢复了宗教活动。

## 语言

咸阳市方言大部分属北方官话区中原方言关中片，而杨陵区五泉、武功县游凤部分村庄、彬县永乐、长武巨家、路家等地属中原方言秦胧片。

## 教育科研资源

咸阳市内教育科研资源丰富，是陕西省内仅次于省会西安的科教大市，咸阳高等院校和科研单位之多全国地级市中也实属罕见。咸阳市拥有1所[985工程和](../Page/985工程.md "wikilink")[211工程院校](../Page/211工程.md "wikilink")[西北农林科技大学和](../Page/西北农林科技大学.md "wikilink")1所全国重点大学、[中西部高校基础能力建设工程高校](../Page/中西部高校基础能力建设工程.md "wikilink")[陕西科技大学和其他多所高等学校](../Page/陕西科技大学.md "wikilink")。咸阳市辖多所省级知名中学。同时，咸阳市是西北地区重要的军工科研基地之一，多家兵工集团坐落在咸阳。

### 大学

  - [西北农林科技大学](../Page/西北农林科技大学.md "wikilink")
  - 空军工程大学导弹学院（提前批次）
  - [陕西科技大学](../Page/陕西科技大学.md "wikilink")
  - 陕西中医药大学
  - 西藏民族大学
  - 咸阳师范学院
  - 陕西工业职业技术学院
  - 陕西邮电职业技术学院
  - 陕西财经职业技术学院
  - 陕西能源职业技术学院
  - 咸阳职业技术学院
  - 陕西科技大学镐京学院
  - 陕西服装工程学院
  - 陕西国际商贸学院

### 著名中学

  - 咸阳中学
  - 渭城中学
  - 彩虹中学
  - 咸阳市实验中学
  - 育才中学
  - 咸阳道北中学
  - 咸阳师范学院附属中学
  - 西北工业大学启迪中学
  - 周陵中学
  - 秦都中学
  - 陕西科技大学附中
  - 清华附中秦汉中学
  - 咸阳市高新一中

### 科研单位

  - 中国兵器工业集团第202研究所（西北机电研究院）
  - 核工业203研究院
  - 西北橡胶研究院
  - 陕西机械研究院
  - 陕西农业机械研究院
  - 陕西中医研究院
  - 陕西纺织器材研究院

## 对外交流

自1984年建市以来，咸阳市已同5个国家的7个城市建立了友好关系。\[16\]

  - [宇治市](../Page/宇治市.md "wikilink")（1986年7月24日缔结）

  - [成田市](../Page/成田市.md "wikilink")（1988年9月14日缔结）

  - [明尼苏达州](../Page/明尼苏达州.md "wikilink")[罗切斯特市](../Page/罗切斯特市.md "wikilink")（1995年1月18日缔结）

  - [莫兰德市](../Page/莫兰德市.md "wikilink")（1997年7月31日缔结）

  - [勒芒市](../Page/勒芒.md "wikilink")（2001年9月12日缔结）

  - [义城郡](../Page/义城郡.md "wikilink")（2003年10月17日缔结）

  - [纽约州](../Page/纽约州.md "wikilink")[罗切斯特市](../Page/罗切斯特市.md "wikilink")（2006年1月6日缔结）

## 外部链接

  - [咸阳网](http://www.xy029.net)

<!-- end list -->

  - [咸阳政府网站](http://www.xianyang.gov.cn)

## 参考文献

{{-}}

[咸阳](../Category/咸阳.md "wikilink")
[Category:陕西地级市](../Category/陕西地级市.md "wikilink")
[陕](../Category/中国大城市.md "wikilink")
[Category:关中城市群](../Category/关中城市群.md "wikilink")
[陕](../Category/国家历史文化名城.md "wikilink")
[陕](../Category/国家卫生城市.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.
2.
3.  [咸阳地情网-古今大事](http://www.xydqw.com/Article_Class2.asp?ClassID=130)
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16. [陕西省外事办公室](http://www.sxfao.gov.cn/nry.jsp?urltype=news.NewsContentUrl&wbnewsid=90451&wbtreeid=124)