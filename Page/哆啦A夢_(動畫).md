《**哆啦A夢**》是一部改编自[藤子·F·不二雄的](../Page/藤子·F·不二雄.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")《[哆啦A夢](../Page/哆啦A夢.md "wikilink")》的國民級[日本动画](../Page/日本动画.md "wikilink")，於[日本電視台](../Page/日本電視台.md "wikilink")（1973年4月1日
- 9月30日）及[朝日電視台](../Page/朝日電視台.md "wikilink")（1979年4月2日至今尚播放中）首播。

## 日本電視台版本（1973年）

《[哆啦A夢](../Page/哆啦A夢.md "wikilink")》在[朝日電視台熱播了三十年](../Page/朝日電視台.md "wikilink")，鮮少有人知道《哆啦A夢》最初是在[日本電視台播出的](../Page/日本電視台.md "wikilink")。不過由於只播出半年，再加上後續幾乎沒有重播，因此被稱為「幻之版」「舊哆啦」或「日本電視台版哆啦」。
日本電視台版的《哆啦A夢》，於1973年4月1日至9月30日，每周日下午七點播出。但是，由於敵對節目太過出色，因此哆啦A夢的收視率一直不見起色。製作團隊於是將原作中不曾出現的機器鴨ガチャ子變成真正班底，還將哆啦A夢的配音員換人，希望能拉抬收視。收視拉抬是拉抬了，雖然還曾經有再繼續播出一季的計畫，不過，因為製作哆啦A夢動畫的[日本電視台動畫公司財務出現問題](../Page/日本電視台動畫公司.md "wikilink")，社長落跑，最後一集播出時，該公司[辦公室已經被完全淨空](../Page/辦公室.md "wikilink")，可見其窘迫程度。因此《哆啦A夢》在日本電視台的播出也就匆匆了結。1973年9月30日，第26集第52回的《再见，哆啦A梦之巻》，是《哆啦A夢》在日本電視台最後的播出。當時，最後的哆啦A夢還穿插了黃色小鳥飛走的圖案，暗示著離別。而原本在片尾的圖卡「期待下周」（的播出）也改成了「期待下次」。

日本電視台版《哆啦A夢》的特點有：

  - 作品標題是紅色的，片頭曲為音頭風格。
  - 哆啦A夢手腳較長，身高很高。
  - [胖虎的配音員](../Page/胖虎.md "wikilink")，是在朝日電視台的版本中擔任[小夫的](../Page/小夫.md "wikilink")[肝付兼太](../Page/肝付兼太.md "wikilink")。
  - 原作幾乎不登場的虛幻的登場人物是機器鴨「ガチャ子」，在靜香家居住。靜香家裡還有「ボタ子」，還有沒出現在原作中的家務助理。
  - 胖虎的媽媽去世了。
  - [大雄等人就讀](../Page/野比大雄.md "wikilink")「下町小学校」。
  - 哆啦A夢拿出秘密道具（当时称“秘密兵器”）的時候，會先說江戶口音「a～rayo」。
  - 哆啦A夢的眼皮是藍色的。
  - “秘密道具”被称作“秘密兵器”。

雖然比方賽璐璐原畫、分鏡稿等眾多資料都因為沒有存放的地方而被燒毀，而電視台存放的膠卷也不知所終，但製作團隊們或許認為，舊版《哆啦A夢》尚未到亡佚的地步，還有重新在電視上播出的一天。“旧哆啦A夢”在1973年到1979年之间被各个电视台重复播放，直到1979年[大山版动画播映开始後停止重播](../Page/哆啦A夢_\(電視動畫\)#大山羨代時期（1979年～2005年）.md "wikilink")。并在1980年以后没有再播过。

## 朝日電視台版本（1979年至今）

[日本電視台忽然宣告停止播放六年後](../Page/日本電視台.md "wikilink")，1979年4月2日終於在[朝日電視台重新再度於](../Page/朝日電視台.md "wikilink")-{zh-cn:小屏幕;zh-hant:螢光幕}-上播出《哆啦A夢》。初期是每週一到週五以十分鐘的小節目由朝日電視台（播映區域只在[關東](../Page/關東地方.md "wikilink")）開始放送。六天後（4月8日）才正式順次由朝日電視台全國[聯播網各成員電視台播放](../Page/聯播網.md "wikilink")，時間是在每週日上午8時30分以每次30分鐘的動畫節目播放。而這個版本又因製作群、[聲優和畫風的不同](../Page/聲優.md "wikilink")，分為兩個時期，詳細請參見[大山羨代時期和](../Page/哆啦A夢_\(電視動畫\)#大山羨代時期（1979年～2005年）.md "wikilink")[水田山葵時期的介紹](../Page/哆啦A夢_\(電視動畫\)#水田山葵時期（2005年至今）.md "wikilink")。水田山葵版由[楠葉宏三總監督執導](../Page/楠葉宏三.md "wikilink")，製作公司是由[東映動畫所獨立出來的](../Page/東映動畫.md "wikilink")「[SHIN-EI動畫](../Page/SHIN-EI動畫.md "wikilink")」（/，Shinei）（新鋭動畫株式會社）所擔任。

### 大山羨代時期（1979年～2005年）

### 水田山葵時期（2005年至今）

## 工作成員

在這裡所記載的人員皆是依據當時工作人員的敘述內容為證而編輯的，所以會和一部分的網站或是書籍內容有些微誤差。\[1\]

### 日本電視台參與人員

#### 工作人員

  - 企劃：藤井賢祐、佐佐木一雄
  - 監製：佐佐木一雄
  - 總監製：上梨滿雄
  - 擔當演出：岡迫和之、腰繁男、矢澤則夫、石黑昇…等人
  - 腳本：山崎晴哉、鈴木良武、井上知士、吉原幸榮…等人
  - 繪圖攝影指導：生頼昭憲、奥田誠二、棚橋一徳、矢沢則夫…等人
  - 作畫監督：鈴木滿、村田四郎、宇田川一彦、生頼昭憲、白川忠志…等人
  - 美術監督：鈴木森繁、川本征平
  - 文藝：徳丸正夫
  - 音樂：[越部信義](../Page/越部信義.md "wikilink")
  - 製作主任：下崎闊
  - 製作：日本電視動畫製作公司

#### 主要角色配音

  - [哆啦A夢](../Page/哆啦A夢_\(漫畫角色\).md "wikilink")：[富田耕生](../Page/富田耕生.md "wikilink")（第1話～第13話）→[野澤雅子](../Page/野澤雅子.md "wikilink")（第14話以後）
  - [野比大雄](../Page/野比大雄.md "wikilink")：[太田淑子](../Page/太田淑子.md "wikilink")
  - [源靜香](../Page/源靜香.md "wikilink")：[惠比壽真沙子](../Page/惠比壽真沙子.md "wikilink")
  - [剛田武](../Page/剛田武.md "wikilink")：[肝付兼太](../Page/肝付兼太.md "wikilink")
  - [骨川小夫](../Page/骨川小夫.md "wikilink")：[八代駿](../Page/八代駿.md "wikilink")
  - 大雄的媽媽（[野比玉子](../Page/野比玉子.md "wikilink")）：[小原乃梨子](../Page/小原乃梨子.md "wikilink")
  - 大雄的爸爸（[野比伸助](../Page/野比伸助.md "wikilink")）：[村越伊知郎](../Page/村越伊知郎.md "wikilink")
  - 小夫的媽媽：[高橋和枝](../Page/高橋和枝.md "wikilink")
  - 小鬼Q太郎：[堀絢子](../Page/堀絢子.md "wikilink")
  - [世修](../Page/世修.md "wikilink")：[山本圭子](../Page/山本圭子.md "wikilink")
  - 大雄的老師：[加藤修](../Page/加藤修.md "wikilink")→[雨森雅司](../Page/雨森雅司.md "wikilink")
  - Botako（）：[野澤雅子](../Page/野澤雅子.md "wikilink")
  - Debuko（）時常與靜香一起玩的胖妹：[塚瀨紀子](../Page/塚瀨紀子.md "wikilink")
  - Jyamako（）：[吉田理保子](../Page/吉田理保子.md "wikilink")

#### 其他配音（角色不明）

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/大竹宏.md" title="wikilink">大竹宏</a></li>
<li><a href="../Page/兼本新吾.md" title="wikilink">兼本新吾</a></li>
<li><a href="../Page/田中亮一.md" title="wikilink">田中亮一</a></li>
<li><a href="../Page/野村道子.md" title="wikilink">野村道子</a></li>
<li><a href="../Page/水鳥鐵夫.md" title="wikilink">水鳥鐵夫</a></li>
<li><a href="../Page/山下啓介.md" title="wikilink">山下啓介</a></li>
<li><a href="../Page/岡本敏明.md" title="wikilink">岡本敏明</a></li>
<li><a href="../Page/槐柳二.md" title="wikilink">槐柳二</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/辻村真人.md" title="wikilink">辻村真人</a></li>
<li></li>
<li><a href="../Page/峰恵研.md" title="wikilink">峰恵研</a></li>
<li><a href="../Page/山田俊司.md" title="wikilink">山田俊司</a></li>
<li><a href="../Page/神谷明.md" title="wikilink">神谷明</a></li>
<li><a href="../Page/北川米彥.md" title="wikilink">北川國彥</a></li>
<li><a href="../Page/中西妙子.md" title="wikilink">中西妙子</a></li>
<li><a href="../Page/松金米子.md" title="wikilink">松金米子</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/八奈見乘兒.md" title="wikilink">八奈見乘兒</a></li>
<li><a href="../Page/山丘陽人.md" title="wikilink">山丘陽人</a></li>
<li><a href="../Page/加茂嘉久.md" title="wikilink">加茂嘉久</a></li>
<li><a href="../Page/田村錦人.md" title="wikilink">田村錦人</a></li>
<li><a href="../Page/永井一郎.md" title="wikilink">永井一郎</a></li>
<li><a href="../Page/丸山裕子.md" title="wikilink">丸山裕子</a></li>
<li><a href="../Page/矢田耕司.md" title="wikilink">矢田耕司</a></li>
<li><a href="../Page/渡邊典子.md" title="wikilink">渡邊典子</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 主題曲與插曲

### 日本電視台播放曲

  - [片頭曲](../Page/主題曲.md "wikilink")
    「」（哆啦A夢）
    歌曲：、，作詞：藤子不二雄，作曲、編曲：越部信義
    與新鋭動畫版本的主題曲有很大的差異，帶著有點憂鬱的演歌曲風讓人感到很奇妙的印象，尤其加上原作者自己作詞，更是飄著一股很奇特的氣氛。可以由這裡得知，當時原作者將哆啦A夢把持著什麼樣的感覺。
  - 片尾曲
    「」（哆啦A夢倫巴）
    歌曲：，作詞：横山陽一，作曲、編曲：越部信義
    這是一首委外作詞的主題曲，輕快節奏拉丁風的歌曲。感覺比片頭曲有更多世界觀的奇妙表現。
  - 插曲
    「」（忧郁的哆啦A梦）
    歌：富田耕生，作詞・横山陽一，作曲・越部信義
    「」
    歌：[哥倫比亞搖籠會](../Page/音羽搖籠會.md "wikilink")、劇團NLT，作詞：藤子不二雄，作曲：越部信義

### 朝日電視台播放曲

#### 第1期

  - 片頭曲

<!-- end list -->

1.  「」（[哆啦A夢之歌](../Page/哆啦A夢之歌.md "wikilink")）
      -
        歌曲：[大杉久美子](../Page/大杉久美子.md "wikilink")，作詞：楠部工，作曲、編曲：[菊池俊輔](../Page/菊池俊輔.md "wikilink")
        播放日期：1979年4月2日－1992年10月2日
2.  「」（哆啦A夢之歌）
      -
        歌曲：[山野智子](../Page/山野智子.md "wikilink")，作詞：楠部工，作曲、編曲：菊池俊輔
        播放日期：1992年10月9日－2002年9月20日
3.  「」（哆啦A夢之歌）
      -
        歌曲：東京布丁，作詞：楠部工，作曲：菊池俊輔，編曲：岩戶崇
        播放日期：2002年10月4日－2003年4月11日
4.  「」（哆啦A夢之歌）
      -
        歌曲：[渡邊美里](../Page/渡邊美里.md "wikilink")，作詞：楠部工，作曲：菊池俊輔，編曲：金子隆博
        播放日期：2003年4月18日－2004年4月23日
5.  「」（哆啦A夢之歌）
      -
        歌曲：AJI，作詞：楠部工，作曲：菊池俊輔，編曲：橘哲夫
        播放日期：2004年4月30日－2005年3月18日

週一至週五的10分鐘節目：

1.  「」（哆啦A夢之歌）
      -
        歌曲：大杉久美子，作詞：楠部工，作曲、編曲：菊池俊輔
        播放日期：1979年4月2日－1979年9月29日
2.  「」（[我是哆啦A夢](../Page/我是哆啦A夢.md "wikilink")）
      -
        歌曲：[大山羨代](../Page/大山羨代.md "wikilink")、，作詞：[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")，作曲、編曲：菊池俊輔
        播放日期：1979年10月1日－1981年9月26日

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「」（藍色的天空是口袋喲）
      -
        歌曲：大杉久美子
        播放日期：1979年4月8日－1981年9月27日
2.  「」
      -
        歌曲：大山羨代
        播放日期：1981年10月2日－1984年3月30日
3.  「」
      -
        歌曲：大山羨代
        播放日期：1983年11月18日－1983年12月30日
4.  「」（我們是地球人）
      -
        歌曲：[堀江美都子](../Page/堀江美都子.md "wikilink")
        播放日期：1984年4月6日－1988年4月8日
5.  「」
      -
        歌曲：堀江美都子
        播放日期：1988年4月15日－1992年10月2日
6.  「」
      -
        歌曲：西脇唯
        播放日期：1992年10月9日－1995年4月7日
7.  「」（[我是哆啦A夢2112](../Page/我是哆啦A夢.md "wikilink")）
      -
        歌曲：大山羨代、，作詞：藤子·F·不二雄，作曲、編曲：菊池俊輔
        播放日期：1995年4月14日－2002年9月20日
8.  「」
      -
        歌曲：，作詞：北川悠仁，作曲：北川悠仁，編曲：、寺岡呼人
        播放日期：2002年10月4日－2003年4月11日
9.  「」（蒲公英之詩）
      -
        歌曲：THE ALFEE
        播放日期：2003年4月18日－2003年10月4日
10. 「」
      -
        歌曲：[島谷瞳](../Page/島谷瞳.md "wikilink")，作詞：小幡英之，作曲：宮崎步，編曲：宗像仁志
        播放日期：2003年10月10日 - 2004年5月28日
11. 「」（啊～真好呀！）
      -
        歌曲：[W](../Page/W_\(Double_U\).md "wikilink")
        播放日期：2004年6月4日－2005年3月18日

#### 第2期

  - 主題曲

電影推廣期間會改為電影的主題曲。

1.  「」（哆啦A夢之歌）
      -
        歌曲：[女子十二樂坊](../Page/女子十二樂坊.md "wikilink")，作詞：楠部工，作曲：菊池俊輔，編曲：西脇辰彌，中樂編曲：梁劍峰
        播放日期：2005年4月15日 - 2005年10月21日
2.  「」（[擁抱吧](../Page/给我抱抱.md "wikilink")）
      -
        歌曲：[夏川里美](../Page/夏川里美.md "wikilink")，作詞：[阿木燿子](../Page/阿木燿子.md "wikilink")，作曲：[宇崎龍童](../Page/宇崎龍童.md "wikilink")，編曲：[京田誠一](../Page/京田誠一.md "wikilink")
        播放日期：2005年10月28日 - 2007年4月20日
3.  「」（[實現夢想的哆啦A梦](../Page/實現夢想的哆啦A梦.md "wikilink")）
      -
        歌曲：mao，作詞、作曲：黑須克彥，編曲：大久保薫
        播放日期：2007年5月11日 - 2014年4月10日，2015年5月8日 -（2008年7月11日 - 8月8日暫停）
4.  「」（實現夢想的哆啦A梦）
      -
        歌曲：水田山葵、大原惠、嘉數由美、木村昴、關智一，作詞、作曲：黑須克彥，編曲：大久保薫
        播放日期：2014年4月17日 - 2014年12月5日

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「」
      -
        歌曲：[水田山葵](../Page/水田山葵.md "wikilink")，作詞：，作曲、編曲：[澤田完](../Page/澤田完.md "wikilink")
        播放日期：2005年8月5日 - 10月21日、2007年6月 -、2008年7月11日- 8月8日、2013年8月9日-？

## 各話列表

## 日本以外地区

《哆啦A梦》这部动画片在日本以外的地区正在播放，但主要集中在[東亞及](../Page/東亞.md "wikilink")[東南亞地區](../Page/東南亞.md "wikilink")，更获得相当程度的好评。具体在各地播放电视台，可参见文章开头表格之叙述。

### 中國大陆

[广东电视台於](../Page/广东电视台.md "wikilink")1989年在中國大陆地區以中文（普通话）首播，後来各个电视台曾多次更改翻译、配音版本。

期初，广东电视台的1989年译制版名为《叮当》，此版本曾于1989-1991年间在多家电视台播出，此版片头画面使用出现红色的气球的版本，片头曲用大杉久美子演唱版，片尾画面截取原版的一帧并加中文字，片尾曲用《蓝色的天空是口袋》。

1991年2月9日，[中央电视台译制版更名为](../Page/中央电视台.md "wikilink")《机器猫》，在[二套首播](../Page/中国中央电视台财经频道.md "wikilink")，[一套重播](../Page/中国中央电视台综合频道.md "wikilink")，此版片头画面使用出现红色的气球的版本，片头曲用大杉久美子演唱版，片尾画面使用原版并加中文字，片尾曲用《蓝色的天空是口袋》。\[2\]

之后的2000年代央视又改为《哆-{}-啦A梦》，此版并无片头和片尾。

之后可能还有一版配音，不过无相关资料。

2001/2002年广电总局审查版本（上海配音版）的人物名称翻译除去世修被译作阿忙外与现在基本一致。此版的第一季与第二季片头使用出现哆啦A梦表情包的版本，片头曲则使用陈慧琳演唱的国语版本，没有片尾
，而早期集数片尾后的几秒钟画面在该版本中也全部遭到删除。此版的第三季片头画面使用与现在的台湾配音版（第2作）片头一致的版本，片头曲则使用山野智子演唱版本，依旧没有片尾。第一季和第二季亦曾在多家电视台播出，而中国大陆视频网站中的正版哆啦A梦前五百多集亦使用该配音版本的第一季和第二季（后面的集数则使用台湾配音版本）。

同时，如[南方电视台少儿频道](../Page/南方电视台.md "wikilink")、[江苏卫视等](../Page/江苏卫视.md "wikilink")[电视台也会在一些学生假期播放哆啦A梦第](../Page/电视台.md "wikilink")2作的2001/2002广电总局审查版本，如江苏卫视最近一次播出是在2009年[国际劳动节假期期间](../Page/国际劳动节.md "wikilink")。

[北京电视台卡酷少儿频道等儿童频道曾长期循环播出哆啦A梦第](../Page/北京电视台卡酷少儿频道.md "wikilink")2作的2001/2002广电总局审查版本，第一季&第二季，但目前已停播。
该版本由[佳韵社代理](../Page/佳韵社.md "wikilink")，第一季（1979） 152集 \* 24分钟 ；第二季（1979）
641集 \* 24分钟；第三季（特别篇） 52集 \* 24分钟。

2006年代理哆啦A梦中国大陆地区业务[艾影](../Page/艾影.md "wikilink")（上海）商贸有限公司正式成立。艾影(上海)商贸有限公司经授权在中国大陆地区独家享有哆啦A梦形象的版权。

目前哆啦A梦第3作没有中国大陆播出过。

### 香港

香港[無綫電視](../Page/電視廣播有限公司.md "wikilink")（TVB）[翡翠台在](../Page/翡翠台.md "wikilink")1981年8月（一說1982年2月）正式以[粵語播出](../Page/粵語.md "wikilink")，是多啦A夢首個海外播放地\[3\]。播放初期稱為**《叮噹》**，及至1999年6月30日將其改稱為**《多-{啦}-A夢》**。

與日本不同的是，TVB播放多啦A夢並無長期固定時段，而是向日本購入一定集數後，就每星期播放1至5集，有時在兒童節目（先後有《[430-{穿}-梭機](../Page/430穿梭機.md "wikilink")》、《[閃電傳真機](../Page/閃電傳真機.md "wikilink")》、《[至Net小人類](../Page/至Net小人類.md "wikilink")》）內播出，有時以獨立節目形式播出，多是約16:00～17:30的兒童節目時段；播畢購入集數後即停播，若干年後購入新集數又再續播，如是者斷斷續續持續多年，與日本自1981年至今逾36年，均固定星期五19:00～19:30（除了曾短期稍為提早至18:50～19:20）播出的做法大相徑庭。

香港版動畫更創造了一首與日本版旋律一樣的主題曲，初時名為《叮噹》，由兒童合唱團所唱，而其時亦有一首歌曲《畫叮噹》，也是由日本版旋律編上中文歌詞，因為歌詞第一句是「圓圈加一點」，所以也有不少人也稱它為《圓圈加一點》；1990年代初於閃電傳真機時段內播出時，歌曲曾由[陳松齡](../Page/陳松齡.md "wikilink")（現改名[陳松伶](../Page/陳松伶.md "wikilink")）主唱，旋律及歌詞重新編製，但並不流行；及後叮噹正名為多啦A夢，歌曲亦改名為《多啦A夢》，由[陳慧琳主唱](../Page/陳慧琳.md "wikilink")，而當中旋律不變，而這首歌曲也廣為香港兒童傳誦。至2009年1月26日開始，翡翠台開始播放水田版多啦A夢，而主題曲改自《哆啦A夢
為我實現夢想》的粵語版《夢》，由[廖碧兒主唱](../Page/廖碧兒.md "wikilink")。到2013年3月11日，新一輯的《多啦A夢》主題曲改由歌手[謝安琪主唱](../Page/謝安琪.md "wikilink")，但沿用《夢》的歌詞，同日開始於第二聲道提供日文原曲。

由2005年1月3日起連續七年逢星期一17:15（或17:20)於翡翠台播映（當中包括首播及重播、大山版及水田版），但至2012年1月30日於翡翠台播出水田版第235話後就暫停播映新集數，同年起4月2日逢星期一至五18:25～19:00於[J2重播水田版第二輯](../Page/J2.md "wikilink")，6月14日起再度重播水田版第一輯，為首度於翡翠台以外的頻道播出。

第3輯（第231話開始）《多啦A夢》於2013年2月18日起再次於翡翠台播出，是多年來聲演小夫的香港資深配音員[黃鳳英離開無綫電視的首部作品](../Page/黃鳳英.md "wikilink")\[4\]
。另一方面，由於數碼[翡翠台於](../Page/翡翠台.md "wikilink")2013年3月18日改以[高清格式廣播](../Page/高清.md "wikilink")，故即日起，接收[數碼頻道之香港觀眾能以高清畫面收看本作](../Page/香港數碼地面電視廣播.md "wikilink")。2014年10月16日起於[J2重播本輯](../Page/J2.md "wikilink")。

第4輯（第321話開始）《多啦A夢》於2015年7月7日起再次於翡翠台播出，是多年來聲演多啦A夢的香港資深配音員[林保全離世後和胖虎配音員](../Page/林保全.md "wikilink")[陳卓智離開無綫電視的首部作品](../Page/陳卓智.md "wikilink")，播映時間為逢星期二、三17:20；此外，由第321話起，[無綫電視於官方網站中的](../Page/無綫電視.md "wikilink")[myTV提供粵語配音版本供香港市民重溫](../Page/myTV.md "wikilink")，限期為7天（第363及364話誤設為14天）。此輯最後一集（第412話）於2015年12月30日播映。2017年8月5日起於翡翠台重播，每週六日播映。

第5輯（第413話開始）《哆啦A夢》於2016年7月4日起再次於翡翠台播出，亦是多年來聲演老師的香港資深配音員[陳曙光退休後的首部作品](../Page/陳曙光.md "wikilink")。此輯最後一集（第516話）於2017年7月3日播映。

第6輯（第517話開始）《哆啦A夢》於2018年7月2日起再次於翡翠台播出。

<table>
<thead>
<tr class="header">
<th></th>
<th style="text-align: center;"><p>配音員</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>初期（1980年代）</p></td>
<td style="text-align: center;"><p>中期（1990年代）</p></td>
</tr>
<tr class="even">
<td><p><strong>多啦A夢</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/林保全.md" title="wikilink">林保全</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>大雄</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/曾慶珏.md" title="wikilink">曾慶珏</a></p></td>
</tr>
<tr class="even">
<td><p><strong>静香</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/孫明貞.md" title="wikilink">孫明貞</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>胖虎</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/馮志潤.md" title="wikilink">馮志潤</a></p></td>
</tr>
<tr class="even">
<td><p><strong>小夫</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/方煥蘭.md" title="wikilink">方煥蘭</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>野比玉子（大雄媽媽）</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/區瑞華.md" title="wikilink">區瑞華</a></p></td>
</tr>
<tr class="even">
<td><p><strong>多啦美</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/周婉侶.md" title="wikilink">周婉侶</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>出木杉</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/黃麗芳.md" title="wikilink">黃麗芳</a></p></td>
</tr>
<tr class="even">
<td><p><strong>舍華斯（小雄）</strong></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p><strong>迷你多啦</strong></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><center>
<p><strong>其他角色</strong></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>老師</p></td>
<td style="text-align: center;"><p><a href="../Page/周鸞.md" title="wikilink">周鸞</a>→<a href="../Page/陳曙光.md" title="wikilink">陳曙光</a>→<a href="../Page/馮志輝.md" title="wikilink">馮志輝</a>，<a href="../Page/黃健強.md" title="wikilink">黃健強</a>〔代配〕、<a href="../Page/陳永信.md" title="wikilink">陳永信</a>〔代配〕、<a href="../Page/馮志輝.md" title="wikilink">馮志輝</a>（代配），<a href="../Page/陳永信.md" title="wikilink">陳永信</a>(電影-<a href="../Page/大雄與白金迷宮.md" title="wikilink">大雄的迷宮之旅</a>、<a href="../Page/大雄與夢幻三劍士.md" title="wikilink">大雄與夢幻三劍士</a>)，<a href="../Page/黃文偉_(配音員).md" title="wikilink">黃文偉</a> (3D電影-<a href="../Page/STAND_BY_ME_哆啦A夢.md" title="wikilink">STAND BY ME 哆啦A夢</a>)</p></td>
</tr>
<tr class="even">
<td><p>野比大助（大雄爸爸）</p></td>
<td style="text-align: center;"><p><a href="../Page/梁曉峰.md" title="wikilink">梁曉峰</a>→<a href="../Page/張炳強.md" title="wikilink">張炳強</a>→<a href="../Page/梁耀昌.md" title="wikilink">梁耀昌</a>→<a href="../Page/潘文柏.md" title="wikilink">潘文柏</a>，<a href="../Page/盧國權.md" title="wikilink">盧國權</a>〔代配〕、<a href="../Page/招世亮.md" title="wikilink">招世亮</a>〔代配〕，<a href="../Page/張炳強.md" title="wikilink">張炳強</a>（電影版）李建良（3D電影-<a href="../Page/STAND_BY_ME_哆啦A夢.md" title="wikilink">STAND BY ME 哆啦A夢</a>）</p></td>
</tr>
<tr class="odd">
<td><p>技子</p></td>
<td style="text-align: center;"><p><a href="../Page/雷碧娜.md" title="wikilink">雷碧娜</a></p></td>
</tr>
<tr class="even">
<td><p>胖虎媽媽</p></td>
<td style="text-align: center;"><p><a href="../Page/沈小蘭.md" title="wikilink">沈小蘭</a></p></td>
</tr>
<tr class="odd">
<td><p>小夫媽媽</p></td>
<td style="text-align: center;"><p><a href="../Page/雷碧娜.md" title="wikilink">雷碧娜</a>、<a href="../Page/黃玉娟.md" title="wikilink">黃玉娟</a>〔代配〕</p></td>
</tr>
<tr class="even">
<td><p>小夫爸爸</p></td>
<td style="text-align: center;"><p><a href="../Page/源家祥.md" title="wikilink">源家祥</a>（至2008年去世）→<a href="../Page/黃子敬.md" title="wikilink">黃子敬</a></p></td>
</tr>
<tr class="odd">
<td><p>小夫表哥</p></td>
<td style="text-align: center;"><p><a href="../Page/梁偉德.md" title="wikilink">梁偉德</a>→<a href="../Page/李致林.md" title="wikilink">李致林</a></p></td>
</tr>
<tr class="even">
<td><p>靜香媽媽</p></td>
<td style="text-align: center;"><p><a href="../Page/蔡惠萍.md" title="wikilink">蔡惠萍</a></p></td>
</tr>
</tbody>
</table>

#### 播放曲

  - 片頭曲

<!-- end list -->

1.  「叮噹」
      -
        歌曲：[小太陽兒童合唱團](../Page/小太陽兒童合唱團.md "wikilink")，作詞：鄭國江，作曲：[菊池俊輔](../Page/菊池俊輔.md "wikilink")，編曲：？
        播放日期：？－1992年
2.  「叮噹」
      -
        歌曲：[陳松伶](../Page/陳松伶.md "wikilink")，作詞：鄭國江，作曲、編曲：[黃思源](../Page/黃思源.md "wikilink")
        播放日期：1992年－？
3.  「叮噹」
      -
        歌曲：[龔珮嫻](../Page/龔珮嫻.md "wikilink")，作詞：不詳，作曲、編曲：不詳
        播放日期：1998年－1999年
4.  「多啦A夢」
      -
        歌曲：[陳慧琳](../Page/陳慧琳.md "wikilink")，作詞：鄭國江、[甄健強](../Page/甄健強.md "wikilink")，作曲：菊池俊輔，編曲：[伍樂城](../Page/伍樂城.md "wikilink")
        播放日期：1999年－2009年1月19日
5.  「夢」
      -
        歌曲：[廖碧兒](../Page/廖碧兒.md "wikilink")，作詞：余皓文，作曲：黑須克彥，編曲：[葉肇中](../Page/葉肇中.md "wikilink")
        播放日期：2009年1月26日－2013年3月4日
6.  「夢」
      -
        歌曲：[謝安琪](../Page/謝安琪.md "wikilink")，作詞：余皓文，作曲：黑須克彥，編曲：[葉肇中](../Page/葉肇中.md "wikilink")
        播放日期：2013年3月11日－

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「畫叮噹」
      -
        歌曲：小太陽兒童合唱團，作詞：鄭國江，作曲：菊池俊輔，編曲：？
        播放日期：80年代
2.  「只要有夢想」
      -
        歌曲：陳慧琳，作詞：歐湛江，作曲：菊池俊輔，編曲：Raymond Ng
        播放日期：90年代中後期
3.  「夢」
      -
        歌曲：兒童合唱，作詞：[余皓文](../Page/余皓文.md "wikilink")，作曲：黑須克彥，編曲：葉肇中
        播放日期：2009年1月26日－

### 臺灣

1994年，[中國電視公司先取得在](../Page/中國電視公司.md "wikilink")[臺灣的公開播映權](../Page/臺灣.md "wikilink")。1996年，跳槽至[台灣電視公司](../Page/台灣電視公司.md "wikilink")，同時少部分內容在[衛視中文台晚間時段播映](../Page/衛視中文台.md "wikilink")。1997年，再次跳槽至[中華電視公司](../Page/中華電視公司.md "wikilink")，1999年9月20日起開播，才正式將公開播映權易主。目前《哆啦A夢》在華視及[八大綜合台播出](../Page/八大綜合台.md "wikilink")，八大綜合台播出的是華視[配音版](../Page/配音.md "wikilink")。華視播映時間為每週一至週五18：28至18：58，以[雙語](../Page/雙語.md "wikilink")（主聲道為[國語配音](../Page/國語.md "wikilink")，副聲道為[日語原音](../Page/日語.md "wikilink")）發音的形式播出，然後在2006年中旬\[5\]以後已經改為[國語發音的形式播出](../Page/國語.md "wikilink")。八大綜合台播映時間為周一至周五早上5：30、早上7：30、17：00、周一至周四19：00和周六日的早上8：30及19：00，以單一[國語的形式播出](../Page/國語.md "wikilink")。（以上皆為[臺灣時間](../Page/國家標準時間.md "wikilink")。）

但在2009年新版播映後，原來的舊版動畫開始呈現出相同內容定期連環重播的傾向。2016年8月下旬根據一位提供華視播映記錄的忠實觀眾所述，其中一日播出的兩個內容〈阿拉丁神燈〉與〈神奇天線〉在新版開始播映之後至今為止已經重播約4次\[6\]，而且也有少部分觀眾已經發現到華視與八大電視台在平常日播出的內容多數是日本從1980年代初期到1995年初期的內容（自2003年6月6日到2005年3月8日舊版完結的內容僅限於華視播映；八大版本按目前播映情形僅至於2003年5月30日內容），其中在日本於1993年首播的部分內容有跟1988年的混合著播出。

此後對於以上重播次數過高此事華視節目部門曾出面表示過：「《哆啦A夢》舊有集數（也就是所謂的大山版）共計684集，其中有52集過於老舊，尚有632集可輪番播出；《新哆啦A夢》華視目前(2016年)購播至208集，後續仍以每年購買52集左右之新集數交替播出。」\[7\]。

2009年1月19日起，[華視開始播出水田版](../Page/華視.md "wikilink")《哆啦A夢》在台灣稱它為《新哆啦A夢》。當時華視的宣傳片以
「哆啦A夢變瘦」、「大雄變高」
作賣點誤導觀眾，事實上是水田版動畫是16:9畫面製作，但台灣觀眾當時看的大多還是4:3畫面的電視機，故畫面有所變形。華視目前固定在於每年暑假期間播出全新一輯的《新哆啦A夢》，隔年寒假期間重播去年暑假播過的集數（2013年與2016年寒假例外），新版集數播映完後便播回大山版《哆啦A夢》(2018年5月3日止)，2018年5月3日起，華視在大山版播映到4200集後原本的舊版不再播映，同年9月3日八大電視台節目表改點後大山版在台灣正式走入歷史。

2009年8月3日起，由於版權問題，[中華電信MOD用戶無法看到華視播映的新版](../Page/中華電信MOD.md "wikilink")《哆啦A夢》。

2010年2月1日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共26集。

2011年11月28日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共52集。

2013年8月19日起，華視HD頻道播出的《新哆啦A夢》為HD版本，有接收無線數位電視訊號的觀眾，亦可收看到HD版本的《新哆啦A夢》。

2013年9月9日，華視《新哆啦A夢》主題曲錯字「描繪者」已修正為「描繪-{zh-hant:著;zh-hans:着}-」，此問題已經存在四年之多。

2014年7月14日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共26集。

2015年7月13日，華視再次播出全新一輯的《新哆啦A夢》，但主題曲錯字又出現，本次播出共26集。

2015年10月1日起，由於數位無線[華視主頻升規為HD規格播出](../Page/華視主頻.md "wikilink")，因此同年11月24日起，[華視綜合娛樂台](../Page/華視綜合娛樂台.md "wikilink")（原華視HD頻道）不再播出新舊版《哆啦A夢》。

2016年7月11日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共26集。

2017年7月3日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共52集，2015年復現的主題曲錯字在此輯已修正。

2018年5月2日，華視播出大山版《哆啦A夢》已播出4200集，日後，《新哆啦A夢》之新集數播畢後，以重播《新哆啦A夢》之舊集數取代之，然而不再播出大山版《哆啦A夢》。

2018年7月16日，華視再次播出全新一輯的《新哆啦A夢》，本次播出共52集。

由於臺灣的配音員資訊並不多，無法完全得知各期的配音員。

|          |                                    配音員                                    |
| -------- | :-----------------------------------------------------------------------: |
| 舊版       |                                    新版                                     |
| **哆啦A夢** |                     [陳美貞](../Page/陳美貞.md "wikilink")                      |
| **大雄**   |                     [楊凱凱](../Page/楊凱凱.md "wikilink")                      |
| **静香**   |                     [許淑嬪](../Page/許淑嬪.md "wikilink")                      |
| **胖虎**   | [-{于}-正昇](../Page/于正昇.md "wikilink")／[-{于}-正昌](../Page/于正昌.md "wikilink") |
| **小夫**   |                  [劉傑](../Page/劉傑_\(配音員\).md "wikilink")                   |
| **大雄媽媽** |                     [許淑嬪](../Page/許淑嬪.md "wikilink")                      |
| **大雄爸爸** |                      [劉傑](../Page/劉傑.md "wikilink")                       |
| **小雄**   |                     [楊凱凱](../Page/楊凱凱.md "wikilink")                      |
| **靜香媽媽** |                     [陳美貞](../Page/陳美貞.md "wikilink")                      |
| **胖虎媽媽** |                     [楊凱凱](../Page/楊凱凱.md "wikilink")                      |
| **胖虎妹妹** |                     [陳美貞](../Page/陳美貞.md "wikilink")                      |
| **小夫媽媽** |                     [陳美貞](../Page/陳美貞.md "wikilink")                      |
| **小夫堂哥** | [-{于}-正昇](../Page/于正昇.md "wikilink")／[-{于}-正昌](../Page/于正昌.md "wikilink") |
| **哆啦美**  |                     [許淑嬪](../Page/許淑嬪.md "wikilink")                      |
| **出木杉**  | [-{于}-正昇](../Page/于正昇.md "wikilink")／[-{于}-正昌](../Page/于正昌.md "wikilink") |
| **老師**   |                      [劉傑](../Page/劉傑.md "wikilink")                       |
| **雷公**   |   [于正昇](../Page/于正昇.md "wikilink")／[-{于}-正昌](../Page/于正昌.md "wikilink")   |

## 收視率情況

在日本，《哆啦A夢》已經成為高收視率的保證。根據統計\[8\]，《哆啦A夢》的電視動畫曾經於1983年2月11日創下31.2%的高收視，位居所有動畫中的第九位。而在平日，《哆啦A夢》也長期穩居動畫節目收視前五位\[9\]。在臺灣，《哆啦A夢》的收視也是同時段第一名，《哆啦A夢》曾經在2000年以將近7%的收視拿下全國各類節目收視總冠軍，到現在仍舊沒有動畫可以打破這個紀錄（因為節目總類增多使收視率分散）\[10\]。

## 作品變遷

### 日本

### 香港

### 臺灣

### 新加坡

## 参考文献

## 外部連結

  - [舊版哆啦A夢的其中一部分](https://www.youtube.com/watch?v=Ji92uSqPHqk)

  - [](http://hanaballoon.com/ntvdora/)（哆啦A夢房間內舊版哆啦A夢動畫大研究）

  - [](https://web.archive.org/web/20070603173245/http://kiiboo.hp.infoseek.co.jp/)
    （記載有關當時新聞與宣傳資料的公開網站）

  - [](https://web.archive.org/web/20070226174949/http://www.tv-asahi.co.jp/doraemon_25/)（～2005年3月的朝日電視台哆啦A夢網站）

  - [](http://www.tv-asahi.co.jp/doraemon/)（2005年4月～的朝日電視台哆啦A夢網站）

  - [](https://web.archive.org/web/20070209164536/http://blog.tv-asahi.co.jp/wasadora93/)（哆啦A夢聲優水田的博客網站）

  - [](http://www.shin-ei-animation.jp/modules/products/index.php?id=5)（新鋭動畫哆啦A夢的網站）

  - [](http://hanaballoon.com/dorachan/data/anime/)（參考用的個人網站）

  - [哆啦A夢-華視](http://shows.cts.com.tw/shows_prog/cartoon/12.html#.Vtr2D_l95hE)（中華電視公司的哆啦A夢網站）

  - [哆啦A夢動畫－哆啦A夢中文網](http://chinesedora.com/cartoon/)

  - [TVB《多啦A夢》水田版第4輯官方網站](http://programme.tvb.com/animation/newdoraemon4)

  - [myTV《多啦A夢》水田版第4輯節目重溫](http://mytv.tvb.com/tc/cat_animation/newdoraemon4)

### 哆啦A夢(1973)

  -
  -
### 哆啦A夢(1979)

  -
  -
  -
### 哆啦A夢(2005)

  -

<references />

[多啦A夢動畫](../Category/多啦A夢動畫.md "wikilink")
[Category:1973年日本電視動畫](../Category/1973年日本電視動畫.md "wikilink")
[Category:1979年日本電視動畫](../Category/1979年日本電視動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:台視外購動畫](../Category/台視外購動畫.md "wikilink")
[Category:華視外購動畫](../Category/華視外購動畫.md "wikilink")
[Category:八大電視外購動畫](../Category/八大電視外購動畫.md "wikilink")
[Category:朝日電視台動畫](../Category/朝日電視台動畫.md "wikilink")
[Category:漫畫改編動畫](../Category/漫畫改編動畫.md "wikilink")
[Category:東森電視外購動畫](../Category/東森電視外購動畫.md "wikilink")
[Category:時間旅行動畫](../Category/時間旅行動畫.md "wikilink")
[Category:自主機器人題材動畫](../Category/自主機器人題材動畫.md "wikilink")
[Category:藤子·F·不二雄](../Category/藤子·F·不二雄.md "wikilink")
[Category:幼稚園 (雜誌)](../Category/幼稚園_\(雜誌\).md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:機器人主角題材作品](../Category/機器人主角題材作品.md "wikilink")
[Category:小學背景漫畫](../Category/小學背景漫畫.md "wikilink")
[Category:猫主角故事語](../Category/猫主角故事語.md "wikilink")

1.  [](http://mcsammy.fc2web.com/)（這是當時參予這個作品的工作人員網站。如需要資料必須先申請註冊）；[](http://kiokunokasabuta.web.fc2.com/)
     （有本作品的考察資料以及真佐美的訪問記事）
2.
3.
4.  [《多啦A夢》（第231集開始）2013年2月18日起翡翠台播映](http://talk.doracity.com/viewtopic.php?f=2&t=16948)，叮噹小城，2012年12月19日
5.  具體的日期不詳，大約在該年四月初到六月底前後。
6.
7.
8.  參考：[1](http://www.videor.co.jp/data/ratedata/junre/03anime.htm)。
9.  參考：[2](http://www.videor.co.jp/data/ratedata/top10.htm#anime)。
10. 參考：。