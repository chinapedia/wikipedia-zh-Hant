**哈利路亞**（ <sup>*[現代發音](../Page/现代希伯来语.md "wikilink")
／[提比里安发音](../Page/提比里安发音.md "wikilink") *</sup>，，，
<sup>*
*</sup>），又譯為**阿肋路亞**（[天主教會使用](../Page/天主教會.md "wikilink")）、**哈雷路亞**、**阿利路亞**等\[1\]，是希伯來語
「」（）\[2\]和「」（）\[3\]\[4\]\[5\]兩字[單詞組成的](../Page/單詞.md "wikilink")[詞語](../Page/詞語.md "wikilink")，意為「（讓我們）**[讚美](../Page/讚美.md "wikilink")[耶和华](../Page/耶和华.md "wikilink")**」或「**讚美主**」之意。「YAH或Jah--亞
」[希伯來人對](../Page/希伯來人.md "wikilink")[神的稱呼](../Page/神.md "wikilink")「[YHVH或YHWH](../Page/四字神名.md "wikilink")」的縮寫。這個辭彙主要出現在《[詩篇](../Page/詩篇.md "wikilink")》，在[猶太教作爲](../Page/猶太教.md "wikilink")[讚美詩](../Page/讚美詩.md "wikilink")（）禱文的一部分，後來也由[基督教使用](../Page/基督教.md "wikilink")，成為基督教[禮拜儀式的禱文之一](../Page/禮拜.md "wikilink")。

## 希伯来文和希腊文圣经原文

這個詞在[希伯來《聖經》中](../Page/希伯來聖經.md "wikilink")，主要在《[詩篇](../Page/詩篇.md "wikilink")》中\[6\]出現過24次\[7\]，多次在詩篇中出現，並在[希臘語版的](../Page/希臘語.md "wikilink")《[啓示錄](../Page/啓示錄.md "wikilink")》中出現四次\[8\]。

對於大部分[基督徒](../Page/基督徒.md "wikilink")，哈利路亞是對[耶和華的最熱情讚頌](../Page/耶和華.md "wikilink")。很多基督教派別不會在[大齋期時間內說或唱](../Page/大齋期.md "wikilink")《哈利路亞》以及《[榮歸主頌](../Page/榮歸主頌.md "wikilink")》，取而代之的是[大齋期頌揚](../Page/大齋期頌揚.md "wikilink")（Lenten
acclamation）。

Jah作为Jehevah的缩写也出现在很多常见的圣经名字中，这些名字在当今也十分频繁的使用。例如，

  - 約亞拿——“耶和華大有恩典”
  - [約珥](../Page/約珥.md "wikilink")——“耶和華是神”
  - [約翰](../Page/約翰.md "wikilink")——“耶和華大有恩惠”
  - [約拿單](../Page/約拿單.md "wikilink")——“蒙耶和華所賜”
  - [約瑟](../Page/約瑟.md "wikilink")——“願耶和華加給他”
  - [約書亞](../Page/約書亞.md "wikilink")——“耶和華是拯救”

## 不同德语译本中的形式

### Alleluja

  - Allioli译本, 1937年
  - Rösch译本, Bott编撰, 1967年

### Hallelujah

  - 圣经[达秘译本](../Page/达秘译本.md "wikilink");
  - 统一译本（die Einheitsübersetzung）, 1980年;
  - 旧约新约[好消息译本](../Page/好消息译本.md "wikilink")（Die Gute Nachricht des
    Alten und Neuen Testaments）, 1982;
  - [马丁·路德译本](../Page/马丁·路德.md "wikilink"), 1984年修订;
  - Hermann Menge译本, 1951;
  - Franz Eugen Schlachter译本, 1951年;
  - 苏黎世圣经译本（„Zürcher Bibel“）, 2007年;
  - die Übersetzung Hoffnung für Alle, 国际圣经协会，1996年。

### Preiset Jah

  - [圣经新世界译本](../Page/圣经新世界译本.md "wikilink"), 1989年

## 大众文化

  - 18世纪作曲家[韩德尔的著名](../Page/韩德尔.md "wikilink")[清唱剧](../Page/神劇.md "wikilink")《[彌賽亞](../Page/彌賽亞_\(韓德爾\).md "wikilink")》中有一段[合唱“哈利路亚”](../Page/哈利路亚大合唱.md "wikilink")，非常雄壮悦耳。
  - 一首著名的美国爱国歌曲，[《共和国之战歌》](../Page/共和國戰歌.md "wikilink")（The Battle Hymn
    of the Republic），又称《荣哉！哈利路亚》（Glory，Hallelujah）。
  - 加拿大創作歌手[李歐納·柯恩於](../Page/里奧納德·科恩.md "wikilink")1984年發行的專輯《Various
    Positions》其中一首歌名為《哈利路亞》（Hallelujah）。此歌後來被[傑夫·巴克利](../Page/傑夫·巴克利.md "wikilink")、[凱蒂蓮](../Page/凱蒂蓮.md "wikilink")、[邦喬飛等多位歌手翻唱](../Page/邦喬飛.md "wikilink")，並且是電影《[史瑞克](../Page/史瑞克.md "wikilink")》和[正負2度C的插曲之一](../Page/正負2度C.md "wikilink")。

## 参考文献

## 参见

  - [耶和华](../Page/耶和华.md "wikilink")
  - [雅威](../Page/雅威.md "wikilink")
  - [诗篇](../Page/诗篇.md "wikilink")
  - [哈利路亚大合唱](../Page/哈利路亚大合唱.md "wikilink")
  - [大讚辭](../Page/大讚辭.md "wikilink")

[Category:基督教禮儀](../Category/基督教禮儀.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")
[Category:圣经短语](../Category/圣经短语.md "wikilink")
[Category:希伯来圣经中的希伯来语单词和短语](../Category/希伯来圣经中的希伯来语单词和短语.md "wikilink")
[Category:詩篇](../Category/詩篇.md "wikilink")

1.  通常來說，[華語圈的](../Page/華語圈.md "wikilink")[基督教宗派都會直接唸原文而非譯文](../Page/基督教宗派.md "wikilink")。不同的是[新教起頭是](../Page/新教.md "wikilink")「哈」的音，[天主教則是](../Page/天主教.md "wikilink")「阿」的音。
2.  Page H. Kelley, *[Biblical
    Hebrew](../Page/Biblical_Hebrew.md "wikilink"), an Introductory
    Grammar*, page 169. Ethics & Public Policy Center, 1959. ISBN
    978-0-8028-0598-0.
3.  [Hallelujah, also spelled
    Alleluia](http://www.britannica.com/EBchecked/topic/252791/hallelujah),
    *Encyclopædia Britannica*
4.  Brown-Driver-Briggs (Hebrew and English Lexicon, page 238)
5.  page 403, note on line 1 of Psalm 113,
6.  参看诗篇113篇-118篇
7.  分别出现在诗篇104,35; 105,45; 106,148; 111,1; 112,1; 113,1.9; 115,18;
    116,19; 117,2; 135,1.3.21; 146,1.10; 147,1.20; 148,1.14; 149,1.9;
    150,1.6
8.  参看启示录19章1-3节