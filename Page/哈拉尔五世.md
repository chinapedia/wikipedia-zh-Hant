**哈拉尔五世**（**Harald V**，），[挪威國王](../Page/挪威.md "wikilink")，1991年1月17日登基。

他是当时挪威王储奥拉夫王子（后来的国王[奥拉夫五世](../Page/奥拉夫五世.md "wikilink")）和王储妃[玛塔公主的独生子](../Page/玛塔公主_\(瑞典\).md "wikilink")，生于[奥斯陆附近的斯考古姆](../Page/奥斯陆.md "wikilink")（Skaugum）乡村王储行宫中。

哈拉尔是自[奥拉夫四世](../Page/奥拉夫四世.md "wikilink")1370年出生以来第一位在挪威本土出生的王储。由于他是[英国女王](../Page/英国女王.md "wikilink")[维多利亚的后裔](../Page/维多利亚_\(英国女王\).md "wikilink")，哈拉尔五世对英国王位也有继承权。

哈拉尔国王有两个姐姐，分別為生于1930年的[拉格茜尔公主](../Page/拉格茜尔公主.md "wikilink")（Princess
Ragnhild, Mrs.
Lorentzen），2012年9月16日去世；生于1932年的[阿斯特丽公主](../Page/阿斯特丽公主.md "wikilink")（Princess
Astrid, Mrs. Ferner）现住在[奥斯陆](../Page/奥斯陆.md "wikilink")。

哈拉尔信奉[挪威信義宗](../Page/挪威信義宗.md "wikilink")，1968年8月29日和[宋雅·哈拉尔森小姐在](../Page/宋雅王后.md "wikilink")[奥斯陆大教堂结婚](../Page/奥斯陆大教堂.md "wikilink")，生有[玛塔·路易斯公主](../Page/玛塔·路易斯公主.md "wikilink")（Princess
Märtha Louise）和[哈康王储](../Page/哈康王储.md "wikilink")（Crown Prince
Haakon）两个孩子。

## 祖先

<center>

</center>

[Category:挪威君主](../Category/挪威君主.md "wikilink")
[Category:格呂克斯堡王朝](../Category/格呂克斯堡王朝.md "wikilink")
[Category:奧斯陸大學校友](../Category/奧斯陸大學校友.md "wikilink")
[Category:牛津大学贝利奥尔学院校友](../Category/牛津大学贝利奥尔学院校友.md "wikilink")
[Category:现任君主](../Category/现任君主.md "wikilink")
[Category:現任國家領導人](../Category/現任國家領導人.md "wikilink")
[Category:新教君主](../Category/新教君主.md "wikilink")
[Category:德國裔挪威人](../Category/德國裔挪威人.md "wikilink")
[Category:比利時利奧波德大綬章勳章持有人](../Category/比利時利奧波德大綬章勳章持有人.md "wikilink")
[Category:法國榮譽軍團大十字勳章持有人](../Category/法國榮譽軍團大十字勳章持有人.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")