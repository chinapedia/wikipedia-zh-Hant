[Glucose_Haworth.png](https://zh.wikipedia.org/wiki/File:Glucose_Haworth.png "fig:Glucose_Haworth.png")的哈沃斯透视式。\]\]
[Glucose_Fisher_to_Haworth.gif](https://zh.wikipedia.org/wiki/File:Glucose_Fisher_to_Haworth.gif "fig:Glucose_Fisher_to_Haworth.gif")向哈沃斯透视式的转变过程，以葡萄糖为例。\]\]
**哈沃斯投影式\[1\]**（），是表示[单糖](../Page/单糖.md "wikilink")、[双糖或](../Page/双糖.md "wikilink")[多糖所含单糖环形结构的一种常用方法](../Page/多糖.md "wikilink")，名称来源于英国化学家[沃尔特·霍沃思](../Page/沃尔特·霍沃思.md "wikilink")。

哈沃斯透视式有以下特征：

  - 一般成环的原子只有[氧和](../Page/氧.md "wikilink")[碳](../Page/碳.md "wikilink")，氧原子会标注出来，碳原子一般省略，以折点表示。
  - 1号碳称为[异头碳](../Page/端基差向异构体.md "wikilink")，该[半缩醛](../Page/半缩醛.md "wikilink")/[酮碳原子是原非手性的](../Page/半缩酮.md "wikilink")[羰基碳](../Page/羰基.md "wikilink")。由异头碳可衍生出两种异构体，称为[端基差向异构体](../Page/端基差向异构体.md "wikilink")，半缩醛[羟基与](../Page/羟基.md "wikilink")5号碳[羟甲基不在环平面一侧的定为α](../Page/羟甲基.md "wikilink")-异构体，反之定为β-异构体。
  - 连到碳原子上的[氢原子可以写出](../Page/氢.md "wikilink")，也可省略，如右图。
  - 粗线表示原子更接近观察者。在右侧示例中，2、3号原子（以及它们的对应羟基）最接近观察者，而1、4号原子相距观察者更远，剩余的5号等原子最远。

## 参见

  - [费歇尔投影式](../Page/费歇尔投影式.md "wikilink")
  - [纽曼式](../Page/纽曼式.md "wikilink") - [锯架式](../Page/锯架式.md "wikilink")
    - [键线式](../Page/键线式.md "wikilink")

## 参考资料

## 參考文獻

  - [International Union of Pure and Applied
    Chemistry](../Page/IUPAC.md "wikilink"). "[Haworth
    representation](http://goldbook.iupac.org/H02749.html)". *Compendium
    of Chemical Terminology* Internet edition.

[Category:糖化学](../Category/糖化学.md "wikilink")
[Category:糖类](../Category/糖类.md "wikilink")
[Category:立體化學](../Category/立體化學.md "wikilink")

1.  [1](http://term.gov.cn/pages/homepage/result2.jsp?id=333352&subid=10001798&subject=%E7%B3%96%E7%B1%BB&subsys=%E7%94%9F%E7%89%A9%E5%8C%96%E5%AD%A6%E4%B8%8E%E5%88%86%E5%AD%90%E7%94%9F%E7%89%A9%E5%AD%A6)