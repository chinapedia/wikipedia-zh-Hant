**哈特·克萊恩**（Harold Hart Crane，）是二十世紀美國詩人。

## 生平

受到[托馬斯·斯特爾那斯·埃利奧特的啟蒙與刺激](../Page/托馬斯·斯特爾那斯·埃利奧特.md "wikilink")，克萊恩開始寫詩，形式上雖依循傳統，但在文句遣詞上常採古語，晦澀難读；並試圖超越埃利奧特詩中常出現的諷世的絕望。詩作雖然常被批評晦澀難懂及故弄玄虛，克萊恩同時被公認為是當時最具影響力的詩人之一。

## 参见

  - [著名同性恋和双性恋者](../Page/著名同性恋和双性恋人士列表.md "wikilink")
  - [LGBT自杀人士列表](../Page/LGBT自杀人士列表.md "wikilink")

[Category:美國詩人](../Category/美國詩人.md "wikilink")
[Category:美國自殺者](../Category/美國自殺者.md "wikilink")
[Category:自殺作家](../Category/自殺作家.md "wikilink")
[Category:男同性戀作家](../Category/男同性戀作家.md "wikilink")
[Category:美國LGBT作家](../Category/美國LGBT作家.md "wikilink")
[Category:自杀诗人](../Category/自杀诗人.md "wikilink")