**哈維蘭彗星型客機**（）是由[英國](../Page/英國.md "wikilink")[哈維蘭公司研發](../Page/德哈维兰.md "wikilink")，於1949年出廠。這種[民航機](../Page/民航機.md "wikilink")，是全球首款以[噴射引擎為動力的民用飛機](../Page/噴射引擎.md "wikilink")，以0.5毫米的[鋁製蒙皮包覆](../Page/鋁.md "wikilink")，且可飛行至10000米之高空。

這種飛機的研發，使英國成為航空界的翹楚。也因當時二戰的結束，興起的旅遊風氣，彗星型客機也在當時大放異彩。

## 歷史

英國海外航空781號班機由彗星型客機執行由羅馬至倫敦航線。1954年1月10日，飛機在[地中海上空發生爆炸解體](../Page/地中海.md "wikilink")，機上29名乘客（包括10名小孩）及6名機員無人生還。同年四月八日，事故再度發生。

1955年2月公佈的調查報告中，詳細指出彗星式客機意外原因，並提出若干改善飛機設計的建議，防止日後重蹈覆轍。其後彗星機生產商迪哈維蘭跟進，於同年年尾推出其哈維蘭彗星三型，1958年再推出哈維蘭彗星四型。雖然從此沒再發生意外，可是，在連串空難的陰霾下，彗星式改良型飛機得不到客戶垂青，令[德哈维兰陷入財政困難](../Page/德哈维兰.md "wikilink")。其後，彗星飛機只能獲得小部份客戶，如[馬來亞航空](../Page/馬來亞航空.md "wikilink")（新加坡航空前身）、[英國皇家空軍小部份訂單](../Page/英國皇家空軍.md "wikilink")。

針對哈維蘭彗星型設計問題，波音於1958年正式投入服務的[波音707客機](../Page/波音707.md "wikilink")，比彗星式來得更先進。它的後掠翼、更為流線型的機身設計，比彗星式更能承受空氣阻力設計。而更大的載客量、更遠航程、更低營運成本，都令哈維蘭彗星型望塵莫及。最終令哈維蘭彗星型於1964年停產，服役中的哈維蘭彗星型，更於60年代中期陸續退下航線。

## 結構

  - 機組人員：4（包括3名駕駛員和1名空乘人員）
  - 乘客：56-109
  - 長度：34m
  - 翼展：25m
  - 高度：9m
  - 淨重：34200公斤
  - 荷重：39270公斤

## 機型

### 民用機

|      |                                                                                                    |
| ---- | -------------------------------------------------------------------------------------------------- |
| 彗星-1 | 使用於1949-1954年，由於空難事故，導致停航                                                                          |
| 彗星-2 | 啟用於1953年八月，有更大的機翼，和更多儲油量                                                                           |
| 彗星-3 | 類似於彗星二                                                                                             |
| 彗星-4 | 彗星-3的升級版本，也是最大的彗星型客機                                                                               |
| 彗星-5 | 類似[波音707和](../Page/波音707.md "wikilink")[DC-8](../Page/DC-8.md "wikilink")，但未能銷售成功，大多數的航空公司都選擇波音707 |

### 軍用機

“**猎人**”[霍克薛利猎迷](../Page/霍克薛利猎迷.md "wikilink")[反潜巡逻机](../Page/反潜巡逻机.md "wikilink")：1967年服役，共生產49架。

## 飛行事件

  - [英國海外航空781號班機空難](../Page/英國海外航空781號班機空難.md "wikilink")－1954年，一架[英國海外航空彗星](../Page/英國海外航空.md "wikilink")-1型（comet1）客機由[羅馬飛往](../Page/羅馬.md "wikilink")[倫敦時墜毀於](../Page/倫敦.md "wikilink")[地中海](../Page/地中海.md "wikilink")，導致該經營公司停飛該機型兩個月。
  - [南非航空201號班機空難](../Page/南非航空201號班機空難.md "wikilink")－由英國首相[邱吉爾和該公司總裁保證不會出事後數週](../Page/邱吉爾.md "wikilink")，南非航空彗星型由羅馬飛往[開羅的班機又墜毀於地中海](../Page/開羅.md "wikilink")。

經由英國官方研究發現，由於飛機內部有加壓空氣的系統，使乘客不至於感到不適，但卻導致飛機鋁製蒙皮的[金屬疲勞](../Page/金屬疲勞.md "wikilink")，並於飛機的天窗（用以發射無線電）的[鉚釘處裂開](../Page/鉚釘.md "wikilink")，導致飛機解體。這使哈維蘭公司經營不振，最終被其他公司合併。

## 参见

  - [客机列表](../Page/客机列表.md "wikilink")
  - [民航机列表](../Page/民航机列表.md "wikilink")

## 外部連結

  - ["The Comet Emerges" a 1949 *Flight* article on the unveiling of the
    Comet
    prototype](http://www.flightglobal.com/pdfarchive/view/1949/1949%20-%201324.html)
  - ["Comet in the Sky" a 1949 *Flight* article on the Comet's maiden
    flight](http://www.flightglobal.com/pdfarchive/view/1949/1949%20-%201356.html)
  - [The de Havilland Comet in RCAF
    Service](https://web.archive.org/web/20101206071542/http://www.airforce.forces.gc.ca/v2/hst/page-eng.asp?id=621)
  - ["The Tale of the Comet" a 1952 *Flight* article on the design and
    development of the
    Comet](http://www.flightglobal.com/pdfarchive/view/1952/1952%20-%201127.html)
  - ["The Comet Accidents: History of Events," a 1954 *Flight* article
    of Sir Lionel Heald's summary of the
    enquiry](http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%202971.html)
  - ["Report of the Comet Inquiry", a 1955 *Flight* article on the
    publishing of the enquiry into the Comet
    design](http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%200193.html)

{{-}}

[Category:民航飛機](../Category/民航飛機.md "wikilink")
[Category:英国民航机](../Category/英国民航机.md "wikilink")