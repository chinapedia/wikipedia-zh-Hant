[Location_of_Harlem.svg](https://zh.wikipedia.org/wiki/File:Location_of_Harlem.svg "fig:Location_of_Harlem.svg")

**哈莱姆**（），又译**哈林**，是[美国](../Page/美国.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")[曼哈顿的一个](../Page/曼哈顿.md "wikilink")[社区](../Page/社区.md "wikilink")，原名來自一個[荷蘭的](../Page/荷蘭.md "wikilink")[村莊](../Page/哈勒姆.md "wikilink")。曾经长期是20世纪[美国黑人文化与商业中心](../Page/美国黑人.md "wikilink")，也是[犯罪与](../Page/犯罪.md "wikilink")[贫困的主要中心](../Page/贫困.md "wikilink")，目前正在经历一场社会和经济复兴。1873年，哈莱姆区納入紐約市的部分。

## 位置與邊界

哈林位處美國[曼哈頓北端](../Page/曼哈頓.md "wikilink")，與[布朗克斯接壤](../Page/布朗克斯.md "wikilink")。在曼哈頓以[中央公園為邊界](../Page/中央公园園\(紐約市\).md "wikilink")。一般說來哈林在普遍看法，與官方劃定有點不同──後者看只要是黑人聚居的，周遭地區都是哈林範圍。

## 历史

在歐洲人到來以前，哈林居民都是一些美洲土著為主。十七世紀[荷蘭人佔領今日](../Page/荷蘭人.md "wikilink")[曼哈頓地區](../Page/曼哈頓.md "wikilink")，並把整個曼哈頓重新劃分，其中哈林就以荷蘭城市[哈勒姆](../Page/哈勒姆.md "wikilink")
( 荷兰语：**Haarlem** )
來命名。後來[英國人起兵](../Page/英國.md "wikilink")，佔領了整個紐約市，包括哈林。

在美國剛立國時，哈林還只是一個村莊，並未像曼哈頓南端般被發展成商業區。後來鐵路的興建和其他道路網絡之改進，吸引了一些人投資購買哈林土地，以搏取升值。而輸水管等基礎設施也在此時興建。越來越多人居住於哈林，以求方便到曼哈頓上班。而有史以來黑人遷入之記載，則是1900年代之事。

隨著[大蕭條和](../Page/大蕭條.md "wikilink")[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")，紐約市產業空洞化使工作崗位有所減少，哈林犯罪和貧困率也隨之增加。直到1990年代紐約市長[魯迪·朱利安尼大力撲滅](../Page/魯迪·朱利安尼.md "wikilink")，哈林治安才有所改善。

## 文化

[Harlem_-_W125st_-_Madison_Avenue.jpg](https://zh.wikipedia.org/wiki/File:Harlem_-_W125st_-_Madison_Avenue.jpg "fig:Harlem_-_W125st_-_Madison_Avenue.jpg")与[麦迪逊大道之间的](../Page/麦迪逊大道.md "wikilink")125街\]\]
在20和30年代，中和西哈林是[哈萊姆文藝復興的中心點](../Page/哈萊姆文藝復興.md "wikilink")。這是美國黑人社會中史無前例的藝術勃發期。

哈林出了許多名音樂家和作家，此外社區裡還有眾多戲劇公司及劇場。其中最出名的便是阿波羅劇院。阿波羅劇院於1934年1月26日開幕於125街，此前是個舞廳。
位於雷諾克斯大街的Savoy舞廳，是著名的搖擺舞場地，並是爵士名曲“ Stompin' at the
Savoy”的靈感來源。在20和30年代，哈林有超過125個營業中的娛樂場所，包括地下酒吧、咖啡館、西餐廳、牛排館、歌舞廳和交誼廳。
第133街，又名“搖擺街”，在禁酒時代以爵士表演與地下酒吧聞名，並因為在大街上常見的“跨種族交融”而被稱為叢林巷 。
一些爵士場館，包括因[艾靈頓公爵表演而出名的Cotton](../Page/艾靈頓公爵.md "wikilink")
Club，都僅限白人入場。其他的包括萬麗宴會廳和薩沃伊宴會廳是不分種族開放的。

1936年，[奧森•威爾斯改編的](../Page/奧森•威爾斯.md "wikilink")[馬克白在哈林的拉斐特劇院開演](../Page/馬克白.md "wikilink")。此齣[莎士比亞劇全由黑人出演](../Page/莎士比亞.md "wikilink")。
許多19世紀末和20世紀初的大劇院被拆除或改建成教堂。直至2006年哈林缺乏任何永久性的表演空間。

James Baldwin的短篇小说《桑尼的布鲁斯》讲的就是一对来自哈林的黑人兄弟的故事

哈林也是美國最大的非洲裔節遊行慶祝的所在。

許多著名[嘻哈音樂家亦出身哈林](../Page/嘻哈音樂.md "wikilink")，如[吐派克•夏庫爾](../Page/吐派克•夏庫爾.md "wikilink")，A$AP
Rocky，[尚恩•庫姆斯和](../Page/尚恩•庫姆斯.md "wikilink")[阿澤莉亞•班克斯](../Page/阿澤莉亞•班克斯.md "wikilink")。哈林也是許多流行嘻哈舞蹈的發源地。

哈林目前正在經歷一個美食新餐飲的復興。

## 宗教

哈林有約四百家[基督教教堂](../Page/基督教.md "wikilink")，分別屬[衛理宗](../Page/衛理宗.md "wikilink")、[浸信會](../Page/浸信會.md "wikilink")、[聖公會](../Page/聖公會.md "wikilink")、[羅馬天主教等等](../Page/羅馬天主教.md "wikilink")。大多教堂都是小型教會，往往都是在一個狹小的空間聚會，成員不超過50人。但是區內也有一些少數的例子，甚至擁有物業資產。

哈林的[清真寺包括了馬爾科姆](../Page/清真寺.md "wikilink")·沙巴茲清真寺（Malcolm Shabazz
Mosque）、回教兄弟會清真寺（the Mosque of Islamic Brotherhood），和馬斯積阿克薩清真寺（Masjid
Aqsa）。

## 犯罪

自1940年起統計數字顯示，哈林每年發生約100宗謀殺案，但強姦案則較罕見。在1950年代，幾乎所有白人已搬離了哈林；到1960年代，一些生活有所改進的黑人也不再居於哈林。與此同時哈林居民吸毒的比率，比紐約市其他區域高約10-12倍，在全市30,000名吸毒者中，有15,000-20,000人是哈林居民。而在1953年至1962年間，哈林青少年犯罪比率，比全市還高出50%。

## 交通

哈林有地鐵連接至紐約其他區域。

## 地标

  - [125街](../Page/125街.md "wikilink")
  - [115街高架橋](../Page/115街.md "wikilink")
  - [紐約市立學院](../Page/紐約市立學院.md "wikilink")
  - [阿比西尼亞浸信會堂](../Page/阿比西尼亞浸信會堂.md "wikilink")
  - 中央公園北端
  - 格拉咸法院
  - 舒堪堡黑人文化研究中心
  - 哈林藝術學院
  - 哈林兒童區
  - 胡珀噴泉
  - 特利莎酒店
  - 紐約市博物館
  - 洛克公園

## 有关影視作品

  - 《[天生不是寶貝](../Page/天生不是寶貝.md "wikilink")》
  - 《[盧克·凱奇 (電視劇)](../Page/盧克·凱奇_\(電視劇\).md "wikilink")》

## 参考文献

<div class="references-small">

<references />

</div>

  - *WPA Guide to New York City* 1939
  - "Harlem: The Making of a Ghetto. Negro New York, 1890-1930". Gilbert
    Osofsky, 1963
  - TIME Magaine, vol. 84, No.5, July 31, 1964. "Harlem: No Place Like
    Home"
  - Newsweek, August 3, 1964,. "Harlem: Hatred in the Streets"
  - *Harlem Stirs*, John O. Killens, Fred Halstead, 1966
  - Francis A. J. Ianni, *Black Mafia: Ethnic Succession in Organized
    Crime,* 1974
  - "Crack's Decline: Some Surprises from U.S. Cities", National
    Institute of Justice Research in Brief, July 1997

## 外部链接

  - [New York and Harlem Railroad and the Harlem Valley
    line.](https://web.archive.org/web/20060827193219/http://www.southeastmuseum.org/html/history.html)
  - [Harlem and the
    Heights](http://www.nyc-architecture.com/HAR/HAR.htm) - New York
    Architecture Images
  - [13 Gigapixel panorama](http://www.harlem-13-gigapixels.com)

[H](../Category/曼哈頓境內地區.md "wikilink")