**哈那提·斯拉木**（[哈萨克语](../Page/哈萨克语.md "wikilink")：Qanat
Ïsläm，），出生于[中國](../Page/中華人民共和國.md "wikilink")[新疆](../Page/新疆维吾尔自治区.md "wikilink")[阿勒泰地区](../Page/阿勒泰地区.md "wikilink")[哈巴河县](../Page/哈巴河县.md "wikilink")\[1\]，[哈萨克族](../Page/哈萨克族.md "wikilink")，是男子[拳擊運動員](../Page/拳擊.md "wikilink")。

## 生涯

哈那提·斯拉木的父亲是一名教师，二哥曾是新疆拳击队队员。受二哥影响热爱拳击，於1997年進入新疆競技學枚的拳擊隊，阿不力克木‧阿不都熱希提為他的教練。4年後，他被教練提拔至新疆拳擊隊中。2004年，他取得加入國家隊的機會，教練依然是阿不力克木‧阿不都熱希提。

2000年，哈那提·斯拉木於新疆錦標賽贏得69公斤級小項的[冠軍](../Page/冠軍.md "wikilink")。3年後的全國錦標賽，他取得一個[亞軍](../Page/亞軍.md "wikilink")，同年的全國冠軍賽中，他成為冠軍得主。2004年，他在[廣州贏得了](../Page/廣州.md "wikilink")[雅典奧運亞洲區選拔賽的](../Page/2004年夏季奧林匹克運動會.md "wikilink")69公斤級小項冠軍以及參與奧運的資格。在這年的8月，他在[拳擊項目的](../Page/2004年夏季奧林匹克運動會拳擊比賽.md "wikilink")69公斤級小項中，他在32強擊敗[乌干达對手](../Page/乌干达.md "wikilink")，晉身16強，但最終以10點之差不敵[亞美尼亞對手出局](../Page/亞美尼亞.md "wikilink")。

2005年10月，代表新疆的哈那提·斯拉木於在[十運會拳擊項目的](../Page/十運會.md "wikilink")69公斤級小項決賽以40:27大勝[重慶對手](../Page/重慶.md "wikilink")，為新疆隊摘下當屆[全運會的第一面](../Page/全運會.md "wikilink")[金牌](../Page/金牌.md "wikilink")\[2\]。翌年12月的[亞洲運動會](../Page/2006年亞洲運動會.md "wikilink")，他在69公斤級小項的4強賽以30:37不敵[哈薩克對手](../Page/哈薩克.md "wikilink")，取得一面[銅牌](../Page/銅牌.md "wikilink")。

2007年4月，哈那提·斯拉木在[成都舉行的全國錦標賽中贏得冠軍](../Page/成都.md "wikilink")。兩個月於[蒙古舉行的亞洲錦標賽中](../Page/蒙古.md "wikilink")，斯拉木在4強出局，取得銅牌。

[2008年北京奧運](../Page/2008年北京奧運.md "wikilink")69公斤級小項四強，他不敵古巴選手，得到銅牌。

2011年，哈那提放弃[中国国籍](../Page/中华人民共和国国籍.md "wikilink")，入籍[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")\[3\]\[4\]。2012年转为职业选手，并在美国[佛罗里达训练](../Page/佛罗里达.md "wikilink")。

## 資料來源

[Category:中國拳擊運動員](../Category/中國拳擊運動員.md "wikilink")
[Category:中国奥运拳击运动员](../Category/中国奥运拳击运动员.md "wikilink")
[Category:新疆籍运动员](../Category/新疆籍运动员.md "wikilink")
[Category:哈萨克斯坦拳击运动员](../Category/哈萨克斯坦拳击运动员.md "wikilink")
[Category:哈萨克族人](../Category/哈萨克族人.md "wikilink")
[Category:哈巴河人](../Category/哈巴河人.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奥林匹克运动会拳击奖牌得主](../Category/奥林匹克运动会拳击奖牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會拳擊運動員](../Category/2008年夏季奧林匹克運動會拳擊運動員.md "wikilink")
[Category:归化哈萨克斯坦公民的中国人](../Category/归化哈萨克斯坦公民的中国人.md "wikilink")

1.
2.  [哈那提·斯拉木表現神勇
    新疆團奪得十運會首金](http://www.xj.chinanews.com.cn/newsshow.asp?id=22708&ntitle=0dd72c6ea81cf63e94015e3c8f0f4f29)
3.
4.