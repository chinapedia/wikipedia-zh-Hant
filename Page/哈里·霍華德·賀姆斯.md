**哈里·霍華德·福爾摩斯**（[英文](../Page/英文.md "wikilink")：Henry Howard
Holmes，），出生名**赫爾曼·韋伯斯特·穆德吉特**（Herman Webster
Mudgett），暱稱**H·H·福爾摩斯**（H. H. Holmes），美國連環殺手，有兩百件失蹤事件與他有關。

## 殺人方法

自稱為藥劑師的他在芝加哥舉行[芝加哥哥倫布紀念博覽會期間興建了一間名為](../Page/芝加哥哥倫布紀念博覽會.md "wikilink")「博覽會旅館」，其低廉的房租吸引了許多年輕女性入住，但這座建築物充滿了密道與小隔間，等到這些人發覺有問題後，他們早已在這迷失了方向。而福爾摩斯會把受害者困在這裡進行折磨，並且把他們殘酷地殺害。而他殺害的受害者中大部份是金髮的女性。

## 逮捕和死亡

自博覽會結束之後，福爾摩斯結束了旅館的生意，但沒有結束其罪行。終於在1895年被警方逮捕，而在隔年5月7日福爾摩斯就被處以絞刑。

## 影響

福爾摩斯的事跡更吸引到美國作家艾瑞克·拉森，最終將福爾摩斯的事跡寫為《The Devil in the White
City》的小說，中文版本為《[白城魔鬼：奇蹟與謀殺交織的博覽會](../Page/白城魔鬼：奇蹟與謀殺交織的博覽會.md "wikilink")》。

## 參見

  - 書籍：《白城魔鬼：奇蹟與謀殺交織的博覽會》作者：艾瑞克·拉森，ISBN：9789866858147
  - 電影：H.H. Holmes: America's First Serial Killer，導演： John Borowski
    [1](http://www.hhholmesthefilm.com/)

[Category:連環殺手](../Category/連環殺手.md "wikilink")
[Category:被處決的美國人](../Category/被處決的美國人.md "wikilink")
[Category:美國死刑案件](../Category/美國死刑案件.md "wikilink")
[Category:美國殺童犯](../Category/美國殺童犯.md "wikilink")
[Category:被判謀殺罪的美國人](../Category/被判謀殺罪的美國人.md "wikilink")