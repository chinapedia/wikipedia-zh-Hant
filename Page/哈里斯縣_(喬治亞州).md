**哈里斯縣**（Harris County,
Georgia）是位於[美國](../Page/美國.md "wikilink")[喬治亞州西部的一個縣](../Page/喬治亞州.md "wikilink")，西鄰[亞拉巴馬州](../Page/亞拉巴馬州.md "wikilink")。面積1,225平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口23,695人。縣治[漢彌爾頓](../Page/漢彌爾頓_\(喬治亞州\).md "wikilink")。

成立於1827年12月14日。縣名紀念律師、[沙瓦納市長](../Page/沙瓦納.md "wikilink")，查爾斯·哈里斯
(Charles Harris)。

[H](../Category/佐治亚州行政区划.md "wikilink")