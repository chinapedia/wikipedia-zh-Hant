[Map_of_Harris_County_-_Northeast_One-fourth.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Harris_County_-_Northeast_One-fourth.jpg "fig:Map_of_Harris_County_-_Northeast_One-fourth.jpg")

**哈里斯縣**（）是位於[美國](../Page/美國.md "wikilink")[德克薩斯州東南部的一個縣](../Page/德克薩斯州.md "wikilink")，面積達4,604平方公里。根據[美國人口調查局於](../Page/美國人口調查局.md "wikilink")2005年估計，該縣共有人口3,693,050人，是德州中人口最多及美國人口第三多的縣。

哈里斯縣是位於大[休斯顿都會區中](../Page/休斯顿.md "wikilink")，而大休斯顿都會區是美國第五大都會區\[1\]。根據[美國2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，哈里斯縣擁有約410萬人口\[2\]，使哈里斯縣成為德州人口最多的縣份，以及美國人口第三多的縣份。哈里斯縣的縣治為德州最大城市[休斯敦](../Page/休斯敦.md "wikilink")。

## 歷史

哈里斯縣成立於1836年3月17日，而縣政府則成立於1837年12月。哈里斯縣原名**哈里斯堡縣**，是德克薩斯州最早的二十三個縣之一。哈里斯縣於1839年改為現名，是為了紀念首批白人定居者之一約翰·理查森·哈里斯。\[3\]\[4\]

## 地理

根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，哈里斯縣總面積為。其中有，即97.25%為陸地；，即2.75%為水域\[5\]。其陸地面積比[羅得島州要大](../Page/羅得島州.md "wikilink")。

### 毗邻县

因哈里斯縣位於德州內，所以其所有毗邻县均為德州的縣份。

  - [蒙哥馬利縣](../Page/蒙哥馬利縣_\(德克薩斯州\).md "wikilink")：北方
  - [利柏提縣](../Page/利柏提縣_\(德克薩斯州\).md "wikilink")：東北方
  - [钱伯斯县](../Page/钱伯斯县_\(得克萨斯州\).md "wikilink")：东方
  - [加尔维斯顿县](../Page/加尔维斯顿县.md "wikilink")：東南方
  - [布拉佐里亞縣](../Page/布拉佐里亞縣.md "wikilink")：南方
  - [本德堡縣](../Page/本德堡縣_\(德克薩斯州\).md "wikilink")：西南方
  - [沃勒縣](../Page/沃勒縣_\(德克薩斯州\).md "wikilink")：西北方

## 人口

[Panoramic_Houston_skyline.jpg](https://zh.wikipedia.org/wiki/File:Panoramic_Houston_skyline.jpg "fig:Panoramic_Houston_skyline.jpg")的天際線\]\]

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，哈里斯縣擁有人口4,092,459人。其人口是由56.6%[白人](../Page/歐裔美國人.md "wikilink")、18.9%[黑人](../Page/非裔美國人.md "wikilink")、0.7%[土著](../Page/美國土著.md "wikilink")、6.2%[亞洲人](../Page/亞裔美國人.md "wikilink")（2.0%[越南人](../Page/越南人.md "wikilink")、1.2%[印度人](../Page/印度人.md "wikilink")、1.1%[華人](../Page/華人.md "wikilink")、0.6%[菲律賓人](../Page/菲律賓人.md "wikilink")、0.3%[韓國人](../Page/韓國人.md "wikilink")、0.1%[日本人](../Page/日本人.md "wikilink")、1.0%其他）、0.1%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、14.3%其他[種族以及](../Page/種族.md "wikilink")3.2%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。非[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人的白人佔有總人口的](../Page/拉丁美洲人.md "wikilink")33.0%\[6\]。另一方面，[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人則佔人口](../Page/拉丁美洲人.md "wikilink")40.8%。\[7\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，哈里斯縣擁有3,400,578居民、1,205,516住戶和834,217家庭。\[8\]。其[人口密度為每平方英里](../Page/人口密度.md "wikilink")1,967居民（每平方公里759居民）。哈里斯縣擁有1,298,130間房屋单位，其密度為每平方英里751間（每平方公里290間）。\[9\]哈里斯縣的人口是由58.73%[白人](../Page/歐裔美國人.md "wikilink")、18.49%[黑人](../Page/非裔美國人.md "wikilink")、0.45%[土著](../Page/美國土著.md "wikilink")、5.14%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.06%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、14.18%其他[種族和](../Page/種族.md "wikilink")2.96%[混血兒組成](../Page/混血兒.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")32.93%。在[白人當中](../Page/歐裔美國人.md "wikilink")，有7.2%為[德國人和](../Page/德國人.md "wikilink")5.3%為[英國人](../Page/英國人.md "wikilink")。於3,400,578居民當中，有63.8%母語為[英語](../Page/英語.md "wikilink")、28.8%為[西班牙語以及](../Page/西班牙語.md "wikilink")1.6%為[越南語](../Page/越南語.md "wikilink")。\[10\]

在1,205,516住户中，有37.7%擁有一個或以上的兒童（18歲以下）、50.6%為夫妻、13.7%為單親家庭、而有30.8%為非家庭。其中25.1%住户為獨居，5.3%為同居長者。平均每戶有2.79人，而平均每個家庭則有3.38人。在3,400,578居民中，有29.00%為18歲以下、10.3%為18至24歲、33.4%為25至44歲、19.8%為45至64歲以及7.4%為65歲以上。人口的年齡中位數為31歲，每100個女性就有99.2個男性。而每100個成年女性（18歲以上）則有97.0個成年男性。\[11\]

哈里斯縣的住戶收入中位數為$42,598，而家庭收入中位數則為$49,004。男性的收入中位數為$37,36，而女性的收入中位數則為$28,941。洛杉矶县的[人均收入為](../Page/人均收入.md "wikilink")$21,435。約12.10%家庭和14.97%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")。\[12\]

根據慈善組織「处境危险的儿童」統計顯示，哈里斯縣20.8%的兒童生活在貧窮線下，每1000個兒童就有6.5個未滿一歲已死亡，以及38%學生在高中時期輟學。\[13\]

哈里斯縣和一些德克薩斯州縣份是美國其中一些物业税率最高的縣份。於2007年，哈里斯縣在全國物业税率最高的縣份中排第22，而這個統計只包含擁有人口逾65,000人的縣份。\[14\]

## 照片

[File:HarrisCountyCivilCourthouseTexas.JPG|哈里斯县民事法院](File:HarrisCountyCivilCourthouseTexas.JPG%7C哈里斯县民事法院)
[File:Oldharriscountycourthouse.JPG|旧哈里斯县法院大楼](File:Oldharriscountycourthouse.JPG%7C旧哈里斯县法院大楼)
<File:Harris> County Criminal Courts Building.jpg|哈里斯县刑事法院大楼
<File:Firefighters> and Carriages.jpg|在圣哈辛托街的消防员（攝於1914年）
[File:OldHarrisCountyCourthouse.png|哈里斯县法院（攝於1913年](File:OldHarrisCountyCourthouse.png%7C哈里斯县法院（攝於1913年)）

## 参考文献

## 外部链接

  - [哈里斯縣](http://www.co.harris.tx.us/)
  - [選民登記記錄](http://www.tax.co.harris.tx.us/Voter/voterintro_ch.aspx)

[H](../Category/得克萨斯州行政区划.md "wikilink")

1.

2.

3.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.

4.

5.

6.

7.  ["2010 Census P.L. 94-171 Summary File Data". United States Census
    Bureau.](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/)

8.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

9.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

10.

11.

12.
13. *Houston Chronicle*. 2007, February 22. "Raise the alarm; Two
    surveys point to the abysmal and deteriorating state of American
    children's well-being." p. B10.

14.