**哈里特·伊丽莎白·比彻·斯托**（**Harriet Elizabeth Beecher
Stowe**，），[美国作家](../Page/美国.md "wikilink")、废奴主义者，最著名的作品《[汤姆叔叔的小屋](../Page/汤姆叔叔的小屋.md "wikilink")》成为美国[南北战争的导火线之一](../Page/南北战争.md "wikilink")。她的一生以写作为生，发表了多部作品。

## 生平

斯托夫人的父亲里曼·比彻（Lyman
Beecher）是著名的[公理会牧师和废奴主义者](../Page/公理会.md "wikilink")，共有8个孩子。

她四歲時母親羅珊娜（Roxana
Foote）去世，由长姊教導，在哈特福德长大，后来随父移居[俄亥俄州](../Page/俄亥俄州.md "wikilink")[辛辛那提](../Page/辛辛那提.md "wikilink")，一个废奴情绪强烈的州。成为教师的她，积極参加[文学界和](../Page/文学.md "wikilink")[教育界的活动](../Page/教育.md "wikilink")。

1836年和牧师兼神学院教授斯托（Calvin Ellis
Stowe）结婚，丈夫鼓励她继续写作，但丈夫体弱多病，因此生活贫寒；他们共生有7个孩子，但大都早夭。

[辛辛那提和蓄奴州](../Page/辛辛那提.md "wikilink")[肯塔基州只有一河之隔](../Page/肯塔基州.md "wikilink")，他们在那里生活了18年，经常接触逃亡奴隶。她自己也到过南方，亲眼目到黑奴的悲惨生活。他们的家后来成为帮助南方奴隶逃亡的中转站之一。1850年，由于丈夫工作变迁，他们搬到[缅因州](../Page/缅因州.md "wikilink")，她从1851年到1852年为[华盛顿特区的报纸](../Page/华盛顿特区.md "wikilink")《民族时代》撰写连载小说《汤姆叔叔的小屋—卑贱者的生活》，揭露南方黑奴受到非人的待遇，因此受到南方奴隶主的痛恨，卻在北方受到热烈的欢迎——成本印刷出书时，首天就卖出三千本，第一年卖出30万册，翻译成超过40种文字，后来改编成剧本，每次上演场场爆满，大大促进了北方的废奴情绪。1853年她发表了《汤姆叔叔的小屋题解》，列举了大量文件和证据证实《汤姆叔叔的小屋》中的描写是真实的。同年她去[欧洲旅行](../Page/欧洲.md "wikilink")，在[英国受到热烈赞扬](../Page/英国.md "wikilink")。

1856年她发表《德雷德，阴沉地大沼泽地的故事》，进一步揭露蓄奴制的社会堕落现象。

1859年她发表小说《牧师的求婚》。1869年，《老镇居民》都是描写她熟悉的新英格兰生活。

1869年，她经过对[历史资料的研究](../Page/历史.md "wikilink")，发表了《[拜伦生活真相](../Page/拜伦.md "wikilink")》，揭露拜伦和其姊姊有过[乱伦的恋爱关系](../Page/乱伦.md "wikilink")。因為诗人拜伦是英国人心中的偶像，这篇文章在英国引起哗然，英国人开始攻击她。

1896年，她在[哈特福德去世](../Page/哈特福德.md "wikilink")，終年85岁。与丈夫合葬于[安多佛](../Page/安多佛.md "wikilink")[菲利普斯学院校园内](../Page/菲利普斯学院.md "wikilink")。

[Category:聖公宗聖人](../Category/聖公宗聖人.md "wikilink")
[Category:美國小說家](../Category/美國小說家.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:女性小說家](../Category/女性小說家.md "wikilink")
[Category:美国废奴主义者](../Category/美国废奴主义者.md "wikilink")
[Category:美國新教徒](../Category/美國新教徒.md "wikilink")
[Category:威爾斯裔美國人](../Category/威爾斯裔美國人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:康乃狄克州人](../Category/康乃狄克州人.md "wikilink")