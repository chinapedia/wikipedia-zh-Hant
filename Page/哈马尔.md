**哈马尔**（
）是[挪威](../Page/挪威.md "wikilink")[海德马克](../Page/海德马克.md "wikilink")[行政区的小城](../Page/行政区.md "wikilink")，位于[米约萨湖的东岸](../Page/米约萨湖.md "wikilink")。

## 历史

哈马尔旧城在1152年成立，当时被划为挪威五个[主教管区之一](../Page/主教管区.md "wikilink")，一直到1536年北欧[宗教改革](../Page/宗教改革.md "wikilink")，仍保持其在挪威重要宗教与政治中心的位置，但因挪威人在改革期间普遍从[新教的](../Page/新教.md "wikilink")[路德教会而丢失了主教管区的地位](../Page/路德教会.md "wikilink")。后来哈马尔大教堂1657年在[瑞挪战争被摧毁](../Page/北欧七年之战.md "wikilink")，这两个事件引起了哈马尔渐渐失去其以往的重要性。

现代的哈马尔1849年由[奥斯卡一世建立](../Page/奥斯卡一世.md "wikilink")。市区有[步行街](../Page/步行街.md "wikilink")，从围绕着西边大广场的[图书馆](../Page/图书馆.md "wikilink")，[电影院和](../Page/电影院.md "wikilink")[市场沿到东边的东广场](../Page/市场.md "wikilink")。哈马尔也是重要的[铁路交通中心](../Page/铁路.md "wikilink")，也有[挪威铁路博物馆](../Page/挪威铁路博物馆.md "wikilink")。

## 人口

历史人口统计：

  - 1855年: 1 025人
  - 1865年: 1 868人
  - 1875年: 2 050人
  - 1885年: 3 773人
  - 1895年: 4 777人
  - 1900年: 6 043人
  - 1950年: 11 507人
  - 1960年: 13 489人
  - 1970年: 15 417人
  - 1975年: 16 261人
  - 1980年: 15 917人
  - 1990年: 16 129人
  - 1992年: 26 000人
  - 2000年: 26 545人
  - 2005年: 27 439人

## 文化

[thumb](../Page/图像:Domkirkeruinene-Hamar.jpg.md "wikilink")
[thumb](../Page/图像:17th-may-hamar.jpg.md "wikilink")

每年在大教堂遗址处举行的“[哈马尔国际音乐节](../Page/哈马尔国际音乐节.md "wikilink")”吸引许多摇滚及流行音乐爱好者，一般都会有多位国内国外的国际级歌手与乐队参加。

有时候城内也举行“中世纪节”（），以历史上的传统服装游行于哈马尔旧城，此类活动多以教育及文化目的为由，但仍有人为了对历史的兴趣或上街的乐趣而参加其内。

哈马尔大众媒体机关包括[哈马尔劳工邮报](../Page/哈马尔劳工邮报.md "wikilink")（）与[哈马尔日报](../Page/哈马尔日报.md "wikilink")（）等报纸。

## 教育

[thumb](../Page/图像:Hedmark_University_College_Hamar.JPG.md "wikilink")

海德马克大学在哈马尔有分校，“”（海德马克大学，哈马尔学院）有该校的教师培训（）中心等学科门类。

## 体育

[thumb](../Page/图像:Vikingskipet-Hamar.jpg.md "wikilink")
[thumb球队](../Page/图像:Ham-Kam_players_thanking_supporters.jpg.md "wikilink")\]\]

哈马尔是挪威的滑冰胜地。来自哈马尔的多位滑冰选手历来为滑冰赛中的冠军。1994年奥运会而建的[奥运](../Page/奥运.md "wikilink")[速滑赛场](../Page/速度滑冰.md "wikilink")（又称“[维京船](../Page/維京人.md "wikilink")”，因其外形似维京时代的战船而得名）坐落于哈马尔城内。

与许多挪威人一样，哈马尔人是热衷球迷，[咸卡](../Page/咸卡.md "wikilink")[足球队也来自哈马尔](../Page/足球.md "wikilink")。

[Category:挪威城市](../Category/挪威城市.md "wikilink")
[Category:海德馬克郡自治區](../Category/海德馬克郡自治區.md "wikilink")