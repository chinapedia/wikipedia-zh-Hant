**哌啶**、**六氢吡啶**是一个[杂环化合物](../Page/杂环化合物.md "wikilink")，分子式为(CH<sub>2</sub>)<sub>5</sub>NH。它是一个仲[胺](../Page/胺.md "wikilink")，可看作[环己烷一个碳被](../Page/环己烷.md "wikilink")[氮替代后形成的化合物](../Page/氮.md "wikilink")，即氮杂环己烷。室温下为无色发烟液体，有类似[氨](../Page/氨.md "wikilink")、[胡椒和](../Page/胡椒.md "wikilink")[人類](../Page/人類.md "wikilink")[精液的刺激性气味](../Page/精液.md "wikilink")，广泛应用在[有机合成](../Page/有机合成.md "wikilink")，尤其是[药物合成中](../Page/药物.md "wikilink")。亦可用于[DNA测序](../Page/DNA测序.md "wikilink")。

## 存在及生产

哌啶环存在于很多[生物碱中](../Page/生物碱.md "wikilink")，如[奎宁和](../Page/奎宁.md "wikilink")[胡椒碱](../Page/胡椒碱.md "wikilink")。工业上，它由[吡啶](../Page/吡啶.md "wikilink")[氢化制备](../Page/氢化.md "wikilink")，用[二硫化钼作催化剂](../Page/二硫化钼.md "wikilink")：\[1\]

  -
    [Pyridine_hydrogenation.png](https://zh.wikipedia.org/wiki/File:Pyridine_hydrogenation.png "fig:Pyridine_hydrogenation.png")

用[钠](../Page/钠.md "wikilink")-[乙醇溶液也可将哌啶还原为吡啶](../Page/乙醇.md "wikilink")。\[2\]

## 有机化学

哌啶是很常用的仲胺，用于将[酮转化为](../Page/酮.md "wikilink")[烯胺](../Page/烯胺.md "wikilink")。\[3\]

哌啶与[次氯酸钙反应生成氯代胺C](../Page/次氯酸钙.md "wikilink")<sub>5</sub>H<sub>10</sub>NCl，该化合物发生[脱卤化氢反应得到环状的](../Page/脱卤化氢反应.md "wikilink")[亚胺](../Page/亚胺.md "wikilink")。\[4\]

## 参见

  - [哌嗪](../Page/哌嗪.md "wikilink")
  - [吡啶](../Page/吡啶.md "wikilink")
  - [硫酸二甲酯](../Page/硫酸二甲酯.md "wikilink")

## 参考资料

<div class="references-small">

<references/>

</div>

[\*](../Category/哌啶.md "wikilink")
[Category:胺类溶剂](../Category/胺类溶剂.md "wikilink")
[Category:易制毒化学品](../Category/易制毒化学品.md "wikilink")

1.  Karsten Eller, Erhard Henkes, Roland Rossbacher, Hartmut Höke
    “Amines, Aliphatic” Ullmann's Encyclopedia of Industrial Chemistry
    2002 Wiley-VCH.
2.
3.
4.