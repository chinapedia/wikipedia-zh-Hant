[Gottfried_Böhm_2015.jpg](https://zh.wikipedia.org/wiki/File:Gottfried_Böhm_2015.jpg "fig:Gottfried_Böhm_2015.jpg")
[Aachen_Hubertuskirche.jpg](https://zh.wikipedia.org/wiki/File:Aachen_Hubertuskirche.jpg "fig:Aachen_Hubertuskirche.jpg")，
胡貝圖斯教堂（Hubertuskirche）\]\]
**哥-{特}-佛伊德·波姆**（****，或譯**哥-{特}-佛萊德·波**；），[德國](../Page/德國.md "wikilink")[建築師](../Page/建築師.md "wikilink")，曾於1986年獲頒[普利兹克奖](../Page/普利兹克奖.md "wikilink")。

## 生平

波姆1920年1月23日誕生於德國[奧芬巴赫一個建築世家](../Page/美因河畔奥芬巴赫.md "wikilink")。其父多明尼庫司·波姆（）以興築德國境內數座[教堂而聞名](../Page/教堂.md "wikilink")，而其祖父亦為建築師。

1946年，哥-{特}-佛伊德·波姆自[慕尼黑工業大學建築設計畢業後](../Page/慕尼黑工业大学.md "wikilink")，在鄰近的藝術協會研習[雕塑](../Page/雕塑.md "wikilink")。1947年，哥-{特}-佛伊德開始為父親工作，並在父親於1955年去世後繼承他的事務所。這段時期，波姆亦在魯道夫·史瓦茲（）麾下與「[科隆重建協會](../Page/科隆.md "wikilink")」共事。1951年，波姆前往[紐約](../Page/紐約.md "wikilink")，在卡耶堂·鮑曼（）的事務所工作六個月，並遇見兩位最鼓舞他的人：德國建築師[密斯·凡·德羅與](../Page/密斯·凡·德羅.md "wikilink")[沃爾特·格羅佩斯](../Page/沃爾特·格羅佩斯.md "wikilink")。

此後數十年，波姆在德國境內興建許多建築物，包括教堂、[博物館](../Page/博物館.md "wikilink")、[市民中心](../Page/市民中心.md "wikilink")、[辦公大樓](../Page/辦公大樓.md "wikilink")、[住宅及](../Page/住宅.md "wikilink")[公寓](../Page/公寓.md "wikilink")。波姆被認為是帶[表现主义與](../Page/表现主义.md "wikilink")[後包豪斯風格的建築師](../Page/包豪斯.md "wikilink")，但他認為自己是一名「連接」過去與未來、創意世界與物理世界、建物與城市景觀的一個建築師。在這個信念下，波姆認為建築物的色彩、形式及物料與它的設定息息相關。

波姆早期的作品多使用已鑄型的[混凝土](../Page/混凝土.md "wikilink")，近年由於[科技進展](../Page/科技.md "wikilink")，他開始採用[鋼鐵與](../Page/钢.md "wikilink")[玻璃在他的建築設計之中](../Page/玻璃.md "wikilink")。他的計畫案明顯注重[都市規劃](../Page/城市规划.md "wikilink")，展現了他對於「連接」的注重。

波姆曾獲得多個建築獎項，包括1986年的[普利兹克奖](../Page/普利兹克奖.md "wikilink")。

## 部份建築作品

  - [菁寮天主堂](../Page/菁寮天主堂.md "wikilink")，興建於1960年：位於[臺灣](../Page/台灣.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")[後壁區菁寮的天主堂](../Page/後壁區.md "wikilink")，為哥-{特}-佛伊德·波姆在成名前進行草圖設計。它是波姆首件座落在德國以外的作品，亦為臺灣第一件由普利兹克獎得主設計的作品。\[1\]
  - [科隆宗教大樓](../Page/科隆.md "wikilink")（教堂、圖書館與青年中心），1968年。\[2\]
  - [盧森堡](../Page/盧森堡.md "wikilink")[德意志銀行](../Page/德意志銀行.md "wikilink")
  - [乌尔姆公共圖書館](../Page/乌尔姆.md "wikilink")，2004年

## 參考資料

<div style="font-size:small">

<references />

</div>

## 外部連結

  - [普利茲克獎網頁哥-{特}-佛伊德·波姆](https://web.archive.org/web/20060910224144/http://www.pritzkerprize.com/boehm.htm)

[Category:德国建筑师](../Category/德国建筑师.md "wikilink")
[Category:普利兹克奖得主](../Category/普利兹克奖得主.md "wikilink")
[Category:慕尼黑工業大學校友](../Category/慕尼黑工業大學校友.md "wikilink")
[Category:黑森人](../Category/黑森人.md "wikilink")

1.  東森新聞Ettoday旅遊玩家，《[來去台南／普立茲獎得主設計的天主堂　偷偷藏在台南後壁](http://www.ettoday.com/2004/09/14/11059-1685794.htm)》
2.  圖片見於<http://www.epdlp.com/edificio.php?id=75>