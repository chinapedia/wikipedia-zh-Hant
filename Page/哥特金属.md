**哥-{}-德金属**（**Gothic
Metal**），[台灣](../Page/台灣.md "wikilink")、[香港等地譯作](../Page/香港.md "wikilink")**哥-{}-德金屬**，[中國大陸則譯作](../Page/中國大陸.md "wikilink")-{哥特金屬}-，是[重金属音乐的一个分支](../Page/重金属音乐.md "wikilink")，融合了[哥德摇滚的黑暗抑郁与重金属的侵略性](../Page/哥德摇滚.md "wikilink")。哥德金属诞生于20世纪90年代初的欧洲，由发展而来。歌德金属音乐按照所融合的重金属乐的不同类型，也有不同分类。歌词上，既有[哥德小说式的](../Page/哥德小说.md "wikilink")，也有描述个人经历的，极富戏剧性并充满悲伤与感悟。

歌德金属的先驱，包括来自英格兰北部的，，，来自美国的，来自瑞典的和来自荷兰的。挪威乐队开创了“美女与野兽”的风格，将侵略性的男声和女声[清嗓结合](../Page/清嗓.md "wikilink")，这种鲜明的对比从此被诸多歌德金属乐队采用。90年代中期，和[Cradle
of
Filth在](../Page/Cradle_of_Filth.md "wikilink")[黑金属中加入了哥特元素](../Page/黑金属.md "wikilink")。90年代末，在和[Within
Temptation的带动下](../Page/Within_Temptation.md "wikilink")，歌德金属出现了向[交响金属发展的方向](../Page/交响金属.md "wikilink")。

进入21世纪，歌德金属在欧洲已进入主流，尤其是在芬兰，诸如、、[HIM](../Page/HIM_\(乐团\).md "wikilink")、、、等乐队均创造了排行榜热门歌曲和畅销专辑。尽管如此，在美国，只有很少乐队，比如[HIM](../Page/HIM_\(乐团\).md "wikilink")、[Lacuna
Coil](../Page/Lacuna_Coil.md "wikilink")、[Evanescence](../Page/Evanescence.md "wikilink")、[Cradle
of Filth取得了商业成功](../Page/Cradle_of_Filth.md "wikilink")。
在中国，歌德金属同其他重型音乐一样主要靠地下传播，其中[Lacrimosa是最受中国乐迷欢迎的乐队](../Page/Lacrimosa_\(樂團\).md "wikilink")，而[Evanescence是唯一一支在](../Page/Evanescence.md "wikilink")[中国大陆有主流发行渠道的乐队](../Page/中国大陆.md "wikilink")。中国本土的哥特金属也相当弱势，仅有[Dengel](../Page/Dengel.md "wikilink")、[寂静的幽怨等为数不多的几支乐队选择了这一乐风](../Page/寂静的幽怨.md "wikilink")。此外，香港本地都有哥德金屬的樂隊存在，分別有[古幽靈](../Page/古幽靈.md "wikilink"),[Chimeras和](../Page/Chimeras.md "wikilink")[Amnesia](../Page/Amnesia.md "wikilink")。

## 词源

“哥特”一词引入重金属音乐领域可追溯到1991年，的专辑《Gothic》的发行。从此，乐迷之间便经常争论“哪些乐队是真正的哥特，哪些乐队不是”。一些音乐家也加入了这一行列，[After
Forever](../Page/After_Forever.md "wikilink")、[HIM](../Page/HIM_\(乐团\).md "wikilink")、[Nightwish的成员都曾表示自己的音乐不应被贴上](../Page/Nightwish.md "wikilink")“哥特”的标签。

## 特征

### 音乐

哥特金属在音乐上的基本特点是其黑暗氛围。“黑暗”一词常被用来形容哥特音乐，类似的词还有“深邃”、“忧郁”、“浪漫”、“激情”、“剧烈”等。哥特金属可描述为“哥特摇滚的黑暗忧郁与重金属乐的结合”。[Allmusic则将该音乐风格定义为](../Page/Allmusic.md "wikilink")“哥特摇滚的苍凉冰冷氛围与重金属侵略性的吉他轰鸣”间的融合。[Allmusic还进一步指出](../Page/Allmusic.md "wikilink")“纯正的哥特金属都受到哥特摇滚的直接影响——飘渺的合成器之声以及鬼魅的音律架构，在重要性上不亚于吉他riff”。

哥特金属因乐队追寻的方向不同而呈现出多样性，既有“缓慢极具压迫性的”，又有“交响般恢弘壮阔的”。具有[厄运金属背景的Paradise](../Page/厄运金属.md "wikilink")
Lost、My Dying Bride开创了这一风格，接过他们旗帜的有Ava Inferi、Draconian。Cradle of Filth、
Theatres des
Vampire以及Moonspell领军哥特[黑金属](../Page/黑金属.md "wikilink")，后起之秀有Graveworm、Trail
of Tears等。Tritania、Within
Temptation将[交响金属引入哥特领域](../Page/交响金属.md "wikilink")，Epica、Sirenia、After
Forever同属此列。其他方向如与[旋律死亡金属结合的Crematory](../Page/旋律死亡金属.md "wikilink")、Dark
the
Suns，与[民谣金属结合的Midnattsol](../Page/民谣金属.md "wikilink")，与[工业金属结合的Deathstars](../Page/工业金属.md "wikilink")、Gothminister，与[另类金属结合的Katatonia等](../Page/另类金属.md "wikilink")。

### 唱腔

哥特金属所运用的唱腔也多种多样。男声方面，既有Dani Filth（[Cradle of
Filth](../Page/Cradle_of_Filth.md "wikilink")）、 Morten
Veland（Sirenia）这样的黑嗓和死嗓，也有Østen
Bergøy（Tritania）这样的[假声男高音](../Page/假声男高音.md "wikilink")，以及Peter
Steele（Type O
Negative）这样的[男低音](../Page/男低音.md "wikilink")。女声方面，同样有如Cadaveria这样的嘶吼与咆哮，也有Lullacry主唱Tanja
Lainio似的流行女声，以及Tritania的Vibeke
Stene这样的[歌剧](../Page/歌剧.md "wikilink")[女高音](../Page/女高音.md "wikilink")。在哥特金属领域的女主唱数量，要远远多于别的金属乐类型中的女性，但这并不意味着“女声金属”和“哥特金属”间可以划等号。[Theatre
of Tragedy以及](../Page/Theatre_of_Tragedy.md "wikilink")[Leaves'
Eyes的女主唱Liv](../Page/Leaves'_Eyes.md "wikilink")
Kristine就谈到“哥特”的标签常常被误用，“并不是每个有女主唱的乐队都是哥特乐队”。不过，哥特金属的确是金属乐中女性乐迷最多的一类。

### 歌词

哥特金属的歌词以“史诗”、“戏剧化”的特点著称。根植于[厄运金属的三支英国的奠基乐队](../Page/厄运金属.md "wikilink")，歌词充满着悲伤与抑郁。My
Dying
Bride的音乐被形容为“诉说着种种欺诈与罪孽的迷人歌词，将背叛与痛苦一滴一滴浸入心口”。Anathema的歌词着重于[自杀和无意义的人生](../Page/自杀.md "wikilink")，Paradise
Lost则从未让他们的“抑郁之地”失落。

[哥特小说是一种融合了](../Page/哥特小说.md "wikilink")[恐怖小说与](../Page/恐怖小说.md "wikilink")[骑士文学特点的文学类型](../Page/骑士文学.md "wikilink")，它成为了诸如Cadaveria、Cradle
of Filth、Moonspell、Theatres des
Vampires、Xandria此类哥特金属乐队创作的灵感源泉。Allmusic的评论家Eduardo
Rivadavia将戏剧性的哀伤之美视作这一类型的必备要素。在My Dying
Bride的歌词中，“死亡、苦痛、失去爱人”这样的主题从不同角度反复的被诠释。“失去爱人”这个常见的哥特主题，在Theatre
of Tragedy、Leaves' Eyes等乐队的作品中也有体现。

基于个人情感经历的歌词在许多哥特金属乐队中同样常见，如Anathema、Elis、Tiamat、Midnattsol、The Old Dead
Tree。Graveworm的歌词也从早期的奇幻故事转至现在的个人情感经历，因为他们发觉这更适合他们的音乐。意大利乐队Lacuna
Coil同样不采用任何奇幻内容或是现实中不存在的东西， 女主唱Cristina
Scabbia表示这是为了让人们更好的建立与歌词间的联系。类似的，
Lullacry也在歌词中描写“爱、恨、激情、痛苦”等主题，因为这样的歌词更容易打动人心。

## 发展历史

哥特金属的出现要追溯到20世纪90年代初，英格兰北部的Paradise Lost、My Dying
Bride与Anathema掀起的潮流。这三支乐队同时也催生了[死亡厄运金属](../Page/死亡厄运金属.md "wikilink")，哥特金属因此与[死亡厄运金属有着密切的联系](../Page/死亡厄运金属.md "wikilink")。三支乐队均签约于Peaceville
Records唱片公司，被称为“ Peaceville三巨头”。来自哈利法克斯的Paradise Lost，1990年的首张专辑《Lost
Paradise》对[死亡厄运金属做了很好的定义](../Page/死亡厄运金属.md "wikilink")，1991年的次张专辑《Gothic》加入了零星的键盘与女声点缀，在氛围上制造出些许差异，形成了哥特金属。Paradise
Lost的前五张专辑，被形容为“一只深爱着Sisters Of
Mercy的乐队演奏着黑暗时期Metallica的作品”，他们“最早播下了哥特的种子，才有了近年来其他乐队的收割”。同样来自哈利法克斯的My
Dying Bride在1993年的专辑《Turn Loose the
Swans》中，加入了小提琴，这一创造性的做法进一步增添了暗色浪漫，也启迪了诸多后来者。来自[利物浦的Anathema](../Page/利物浦.md "wikilink")，在发出了[死亡厄运金属之声后](../Page/死亡厄运金属.md "wikilink")，走得更远。从1995年的第二张专辑《The
Silent
Enigma》开始，Anathema进行了音乐上的探索，不再局限于传统[死亡厄运金属领域](../Page/死亡厄运金属.md "wikilink")。评论家甚至拿他们和Pink
Floyd做比较。到了1996年的第三张专辑《Eternity》，Anathema甚至转向用[清嗓演唱](../Page/清嗓.md "wikilink")。他们这种营造悲伤大气氛围又不失哥特背景的主流摇滚姿态，对后来的主流哥特金属乐队，产生了深远影响。

美国的Type O Negative、瑞典的Tiamat、荷兰的The Gathering则是哥特金属在英国以外地区的先行者。Type O
Negative的音乐源于[死亡金属和](../Page/死亡金属.md "wikilink")[鞭笞金属](../Page/鞭笞金属.md "wikilink")。在1993年的专辑《Bloody
Kisses》中，他们引入了一些[哥特摇滚的元素](../Page/哥特摇滚.md "wikilink")，歌词中也充斥着“性、死亡、基督、吸血鬼”等内容。哥特金属从此在北美地区受到了关注。Tiamat早在1988年就开始了活动，那时他们做的是纯正的[死亡金属](../Page/死亡金属.md "wikilink")。在1992年的专辑《Clouds》中，Tiamat降低了音乐的速度性而加强了旋律与氛围。到了1994年的《Wildhoney》，他们的转变更为明显与彻底：插入的[原声吉他](../Page/吉他#.E6.9C.A8.E5.90.89.E4.BB.96.md "wikilink")、呢喃耳语、天使般的合唱与失真的吉他、咆哮的死嗓形成强烈对比冲击，所营造的哥特氛围深深影响了后来的北欧乐队。The
Gathering则是第一支配备了女主唱的哥特金属乐队，以Bart
Smits的死嗓为主，辅以女主唱的吟唱，形成了具有黑暗根源的中板哥特厄运金属。然而早期几个女主唱均无法达到乐队期望制造的效果，直到第三张专辑《Mandylion》中Anneke
van Giersbergen的出现。Anneke用她的声音将The
Gathering带向了巅峰，也启迪了一大批荷兰以及欧洲其他地区女声哥特金属乐队。

葡萄牙的Moonspell和英国的Cradle of
Filth最早将[黑金属与哥特金属进行了融合](../Page/黑金属.md "wikilink")。Moonspell的早期作品以葡萄牙的乡野传说为背景，将浓郁的民族元素与极端金属结合，以Riff和键盘制造出独特忧郁氛围的恶魔之声。首张专辑《Wolfheart》出现了狼人、吸血鬼等主题的歌曲，次张专辑《Irreligious》进一步强化了哥特意味。凭着这两张专辑，Moonspell奠定了在欧洲哥特金属浪潮中的地位。Cradle
of Filth则从成立之初便做着具有独特扭曲美感的哥特黑金属音乐。从1994年的首张专辑《The Principle of Evil Made
Flesh》开始，由键盘演绎的前奏、不时插入的歌剧女声以及Dani
Filth扭曲的嗓音和用黑与血编织的浪漫诗句就成了乐队的标志。1996年的第二张专辑《Dusk...
and Her Embrace》更是一张“令人毛骨悚然的哥特史诗”，其中《Gothic Romance》一曲成为哥特黑金属的代表作。

“美女与野兽”是指天籁般的女声与侵略性的死嗓对比产生的美学体验。这种技巧虽然Paradise Lost和The
Gathering早已采用，但直到1995年挪威乐队Theatre of
Tragedy才推出了完全“美女与野兽”式的全长专辑《Theatre of
Tragedy》。随后他们又推出了《Velvet Darkness They
Fear》和《Aégis》，赞誉不绝。在《Aégis》中他们进行了一些新鲜的尝试，最显著的便是Raymond
Rohonyi放弃了死嗓而代之以念白与呢喃耳语，这造就了另一种对比的美学体验。90年代末，越来越多的乐队开始采用侵略性男声与天籁女声的技巧，仅挪威就又涌现出Tristania、Trail
of Tears、The Sins of Thy
Beloved三支乐队。Tristania除了“美女与野兽”之外，还加入了男声清嗓，进一步丰富了作品带来的听觉体验。此外，Tristania还加入了管风琴、小提琴等交响元素，引领了此后的交响哥特金属浪潮。在“美女与野兽”的推动下，涌现了一大批极具代表性的女性金属乐主唱。

尝试在哥特金属中加入交响元素的不只是Tristania，来自荷兰的Within
Temptation也为交响哥特的普及与推广做出了巨大贡献。成立于1996年的Within
Temptation，最初做的是Sharon den Adel与Robert
Westerholt构成的“美女与野兽”式哥特金属。在2000年的第二张专辑《Mother
Earth》中，他们去除掉了死嗓，仅保留Sharon的天籁之音，同时加强了交响氛围，制造出极为优美动听的声音。2004年，他们凭《The
Silent
Force》将交响哥特金属推至一个新高度。这张专辑找来了整支交响乐团以及80人的唱诗班进行录制，充分展现了交响哥特金属的气势磅礴与华美壮丽。自此，不只是欧洲，整个世界都被这种重型吉他与交响女声的结合所征服。另一支荷兰乐队After
Forever及其关联乐队Epica，不仅延续了Tritania式的交响化“美女与野兽”，还加入了前卫式的复杂编曲架构，奉献出一场场史诗般的交响盛宴。这些出色的交响哥特女声，成为了荷兰的骄傲。

哥特金属的风行使得一些原本做其他风格音乐的乐队也加入了进来。瑞士的Lacrimosa，最初做的是暗潮音乐。随着女声兼键盘手 Anne
Nurmi的加入，他们尝试在原有风格的基础上引入了重型吉他以及古典乐器，形成了独有的兼具暗潮感和古典氛围的交响哥特金属。[交响金属名团](../Page/交响金属.md "wikilink")[Therion](../Page/Therion.md "wikilink")，也在2007年推出了名为《Gothic
Kabbalah》的专辑，展露出他们想和哥特金属接轨的想法。

## 混淆問題及分類方法

雖然此類金屬較其他類型金屬音樂的爭議性少，但明顯地，議論主要圍繞著以形象上演繹還是側重音樂上的演繹。

雖然哥特金属與其他金屬音樂有相似的地方，難以確認。但一致性的定律仍然存在。

1.  [歌詞集中在](../Page/歌詞.md "wikilink")[宗教和](../Page/宗教.md "wikilink")[神](../Page/神.md "wikilink")、[地獄與](../Page/地獄.md "wikilink")[天堂](../Page/天堂.md "wikilink")、[愛](../Page/愛.md "wikilink")、[驚慌](../Page/驚慌.md "wikilink")、[絕望](../Page/絕望.md "wikilink")、
    [喪親](../Page/喪親.md "wikilink")、[空虛和](../Page/空虛.md "wikilink")[死亡](../Page/死亡.md "wikilink")。大部分的歌詞內容早在20世紀前定位了。
2.  主音多數有[男主音和](../Page/男主音.md "wikilink")[女主音](../Page/女主音.md "wikilink")，甚至兩者皆備。男主音帶有深厚的聲線或[死腔](../Page/死腔.md "wikilink")（[咆哮](../Page/咆哮.md "wikilink")、[呻吟](../Page/呻吟.md "wikilink")、[吱吱聲](../Page/吱吱聲.md "wikilink")），女主音傾向於高音及似[歌劇中的女歌手](../Page/歌劇.md "wikilink")，同時衍生了[劇院金屬](../Page/劇院金屬.md "wikilink")。
3.  樂隊隊員身穿黑色緊身或[闊袍衣衫](../Page/闊袍.md "wikilink")，亦有成員面上化上[濃妝](../Page/濃妝.md "wikilink")，如塗上深刻的[眼影](../Page/眼影.md "wikilink")。
4.  變化大的吉他聲及鼓聲往往獨立於歌唱之外，成為伴奏。
5.  有時哥特金属會與[力量金屬和](../Page/力量金屬.md "wikilink")[旋律金屬混為一談](../Page/旋律金屬.md "wikilink")。

### 哥特金属樂團列表

  - [After Forever](../Page/After_Forever.md "wikilink")
  - [Anathema](../Page/Anathema.md "wikilink")
  - [Atargatis](../Page/Atargatis.md "wikilink")
  - [Ava Inferi](../Page/Ava_Inferi.md "wikilink")
  - [Beseech](../Page/Beseech.md "wikilink")
  - [Cadaveria](../Page/Cadaveria.md "wikilink")
  - [Cradle of Filth](../Page/Cradle_of_Filth.md "wikilink")
  - [Crematory](../Page/Crematory.md "wikilink")
  - [Crescent Lament](../Page/Crescent_Lament.md "wikilink")
  - [Darkseed](../Page/Darkseed.md "wikilink")
  - [Deathstars](../Page/Deathstars.md "wikilink")
  - [Delain](../Page/Delain.md "wikilink")
  - [Delight](../Page/Delight.md "wikilink")
  - [Dimmu Borgir](../Page/Dimmu_Borgir.md "wikilink")
  - [Draconian](../Page/Draconian.md "wikilink")
  - [Drastique](../Page/Drastique.md "wikilink")
  - [Dreams of Sanity](../Page/Dreams_of_Sanity.md "wikilink")
  - [Elis](../Page/Elis.md "wikilink")
  - [Entwine](../Page/Entwine.md "wikilink")
  - [Epica](../Page/Epica.md "wikilink")
  - [Estatic Fear](../Page/Estatic_Fear.md "wikilink")
  - [The Eternal](../Page/The_Eternal.md "wikilink")
  - [Eternal Tears of
    Sorrow](../Page/Eternal_Tears_of_Sorrow.md "wikilink")
  - [Evanescence](../Page/Evanescence.md "wikilink")
  - [Flowing Tears](../Page/Flowing_Tears.md "wikilink")
  - [For My Pain...](../Page/For_My_Pain....md "wikilink")
  - [Forever Slave](../Page/Forever_Slave.md "wikilink")
  - [The Gathering](../Page/The_Gathering.md "wikilink")
  - [Gothminister](../Page/Gothminister.md "wikilink")
  - [Graveworm](../Page/Graveworm.md "wikilink")
  - [HIM](../Page/HIM_\(乐团\).md "wikilink")
  - [Katatonia](../Page/Katatonia.md "wikilink")
  - [Krypteria](../Page/Krypteria.md "wikilink")
  - [Lacrimas Profundere](../Page/Lacrimas_Profundere.md "wikilink")
  - [安魂彌撒樂團](../Page/安魂彌撒樂團.md "wikilink")（Lacrimosa）
  - [時空飛鷹樂團](../Page/時空飛鷹樂團.md "wikilink")（Lacuna Coil）
  - [Lake of Tears](../Page/Lake_of_Tears.md "wikilink")
  - [L'Âme Immortelle](../Page/L'Âme_Immortelle.md "wikilink")
  - [Leaves' Eyes](../Page/Leaves'_Eyes.md "wikilink")
  - [Letzte Instanz](../Page/Letzte_Instanz.md "wikilink")
  - [Lullacry](../Page/Lullacry.md "wikilink")
  - [Madder Mortem](../Page/Madder_Mortem.md "wikilink")
  - [Midnattsol](../Page/Midnattsol.md "wikilink")
  - [Moi Dix Mois](../Page/Moi_Dix_Mois.md "wikilink")
  - [Moonspell](../Page/Moonspell.md "wikilink")
  - [Mortal Love](../Page/Mortal_Love.md "wikilink")
  - [Mortemia](../Page/Mortemia.md "wikilink")
  - [My Dying Bride](../Page/My_Dying_Bride.md "wikilink")
  - [Nemesea](../Page/Nemesea.md "wikilink")
  - [Nightfall](../Page/Nightfall.md "wikilink")
  - [Nightwish](../Page/Nightwish.md "wikilink")
  - [Paradise Lost (乐团)](../Page/Paradise_Lost_\(乐团\).md "wikilink")
  - [Poisonblack](../Page/Poisonblack.md "wikilink")
  - [Sentenced](../Page/Sentenced.md "wikilink")
  - [Silentium](../Page/Silentium.md "wikilink")
  - [Seraphim](../Page/Seraphim.md "wikilink")
  - [The Sins of Thy
    Beloved](../Page/The_Sins_of_Thy_Beloved.md "wikilink")
  - [Sirenia](../Page/Sirenia.md "wikilink")
  - [Stream of Passion](../Page/Stream_of_Passion.md "wikilink")
  - [Theatre of Tragedy](../Page/Theatre_of_Tragedy.md "wikilink")
  - [Theatres des Vampires](../Page/Theatres_des_Vampires.md "wikilink")
  - [聖獸樂團](../Page/聖獸樂團.md "wikilink")（Therion）
  - [Tiamat](../Page/Tiamat.md "wikilink")
  - [To/Die/For](../Page/To/Die/For.md "wikilink")
  - [Trail of Tears](../Page/Trail_of_Tears.md "wikilink")
  - [Tristania](../Page/Tristania.md "wikilink")
  - [Type O Negative](../Page/Type_O_Negative.md "wikilink")
  - [Unsun](../Page/Unsun.md "wikilink")
  - [Virgin Black](../Page/Virgin_Black.md "wikilink")
  - [The Vision Bleak](../Page/The_Vision_Bleak.md "wikilink")
  - [We Are the Fallen](../Page/We_Are_the_Fallen.md "wikilink")
  - [Within Temptation](../Page/Within_Temptation.md "wikilink")
  - [Xandria](../Page/Xandria.md "wikilink")
  - [The 3rd and the
    Mortal](../Page/The_3rd_and_the_Mortal.md "wikilink")
  - [The 69 Eyes](../Page/The_69_Eyes.md "wikilink")

## 參看

現時所謂[流行的哥特樂團](../Page/流行.md "wikilink")，均注入了以下各種特色來演繹。

1.  [劇院金屬](../Page/劇院金屬.md "wikilink")（Opera Metal）
2.  [交響金屬](../Page/劇院金屬.md "wikilink")（Symphonic Metal）
3.  [死亡旋律金屬](../Page/死亡旋律金屬.md "wikilink")（Death Melodic Metal）
4.  [死亡金屬](../Page/死亡金屬.md "wikilink")（Death Metal）
5.  [厄运金屬](../Page/末日金屬.md "wikilink")（Doom Metal）

## 外部連結

  - [Metal
    Observer](http://www.metal-observer.com/articles.php?lid=1&sid=5&a=ls&s=3)哥特金属樂團介紹
  - [Goth Metal
    World](https://web.archive.org/web/20070928073033/http://www.gothmetal.net/link/category/)哥特金属樂團介紹\[2\]

[Category:重金屬音樂](../Category/重金屬音樂.md "wikilink")
[Category:哥德式文化](../Category/哥德式文化.md "wikilink")