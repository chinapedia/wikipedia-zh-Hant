**唇形科**（学名：），過去也稱作**唇形花科**（）是[双子叶植物纲中的一个](../Page/双子叶植物纲.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，大约有236个[属](../Page/属.md "wikilink")7200余[种](../Page/种.md "wikilink")\[1\]，广泛分布于全球，是干旱地区的主要植被。唇形科的[模式屬為](../Page/模式屬.md "wikilink")[野芝麻屬](../Page/野芝麻屬.md "wikilink")（*Lamium*）。

唇形科的植物一般都含有挥发性芳香油，可以提取香精，药用或[烹饪做调料](../Page/烹饪.md "wikilink")。许多品种被人工种植，不仅可以应用，有许多品种作为容易成活的鲜花种植。例如[薄荷](../Page/薄荷.md "wikilink")、[迷迭香](../Page/迷迭香.md "wikilink")、[披薩草](../Page/披薩草.md "wikilink")、[罗勒](../Page/罗勒.md "wikilink")、[藥用鼠尾草](../Page/藥用鼠尾草.md "wikilink")、[马郁兰](../Page/马郁兰.md "wikilink")、[薰衣草](../Page/薰衣草.md "wikilink")、[紫苏](../Page/紫苏.md "wikilink")、[百里香](../Page/百里香.md "wikilink")、[丹参](../Page/丹参.md "wikilink")、[益母草](../Page/益母草.md "wikilink")、[一串红等](../Page/一串红.md "wikilink")。本科还包含多种中药，如[黄芩](../Page/黄芩.md "wikilink")、[夏枯草](../Page/夏枯草.md "wikilink")、[藿香](../Page/藿香屬.md "wikilink")、[廣藿香](../Page/廣藿香.md "wikilink")。这些植物一般都很容易用插枝繁殖。

唇形科植物由于其[花瓣分为上下两部分](../Page/花.md "wikilink")，类似「唇」形，花萼五裂，花瓣有的五裂，有的合瓣形成二唇或合成花冠筒而得名。

## 形态

[Leucas_aspera_at_Gandipet,_Hyderabad,_AP_W_IMG_9054.jpg](https://zh.wikipedia.org/wiki/File:Leucas_aspera_at_Gandipet,_Hyderabad,_AP_W_IMG_9054.jpg "fig:Leucas_aspera_at_Gandipet,_Hyderabad,_AP_W_IMG_9054.jpg")的花\]\]

  - 多年生或一年生[草本](../Page/草本.md "wikilink")。具有腺体，或各种单毛、具节毛或星状毛。
  - [茎直立或匍匐状](../Page/茎.md "wikilink")，常为4棱形；枝条对生，稀有轮生。
  - 叶通常为单叶，全缘、浅裂或深裂，稀为复叶，大多对生，稀轮生或部分互生。
  - 聚伞花序，3至更多花在节上形成轮伞花序，单歧聚伞花序，再组成顶生、腋生的总状、穗状或圆锥状复合花序，稀为头状花序。苞叶常在茎上向上过渡成苞片。花常为两性，两侧对称；钟状、管状或杯状[花萼](../Page/花萼.md "wikilink")，通常5萼齿，具5、10、15脉，外面通常被毛或腺体，内面有或无毛环（果盖）；管状花冠多为二唇形，裂片5；通常4雄蕊，通常前对长、后对较短，生于花冠管上，花药2室、纵裂，肉质花盘下位；雌蕊由2心皮形成，[子房上位](../Page/子房.md "wikilink")，花通常生于子房基部，柱头常2裂。
  - 4枚小[坚果或核果状](../Page/坚果.md "wikilink")；种子单生，直立，稀横生而皱曲，一般无[胚乳](../Page/胚乳.md "wikilink")。胚具有微肉质子叶。扁球形至长球形花粉常具3～6沟。

## 属

  - *[Acanthomintha](../Page/Acanthomintha.md "wikilink")*
  - [鳞果草属](../Page/鳞果草属.md "wikilink") *Achyrospermum*
  - [圣蓟属](../Page/圣蓟属.md "wikilink") *Acinos*
  - *[Acrotome](../Page/Acrotome.md "wikilink")*
  - *[Acrymia](../Page/Acrymia.md "wikilink")*
  - *[Aeollanthus](../Page/Aeollanthus.md "wikilink")*
  - [藿香属](../Page/藿香属.md "wikilink") *Agastache*
  - [筋骨草属](../Page/筋骨草属.md "wikilink") *Ajuga*
  - *[Ajugoides](../Page/Ajugoides.md "wikilink")*
  - [菱叶元宝草属](../Page/菱叶元宝草属.md "wikilink") *Alajja*
  - *[Alvesia](../Page/Alvesia.md "wikilink")*
  - [水棘针属](../Page/水棘针属.md "wikilink") *Amethystea*
  - [排香草属](../Page/排香草属.md "wikilink") *Anisochilus*
  - [广防风属](../Page/广防风属.md "wikilink") *Anisomeles*
  - *[Antonina](../Page/Antonina.md "wikilink")*
  - *[Asterohyptis](../Page/Asterohyptis.md "wikilink")*
  - *[Ballota](../Page/Ballota.md "wikilink")*
  - [小冠熏属](../Page/小冠熏属.md "wikilink") *Basilicum*
  - *[Becium](../Page/Becium.md "wikilink")*
  - *[Benguellia](../Page/Benguellia.md "wikilink")*
  - *[Blephilia](../Page/Blephilia.md "wikilink")*
  - [毛药花属](../Page/毛药花属.md "wikilink") *Bostrychanthera*
  - *[Bovonia](../Page/Bovonia.md "wikilink")*
  - *[Brazoria](../Page/Brazoria.md "wikilink")*
  - [二芒草属](../Page/二芒草属.md "wikilink") *Bystropogon*
  - [新风轮菜属](../Page/新风轮菜属.md "wikilink") *Calamintha*
  - *[Capitanopsis](../Page/Capitanopsis.md "wikilink")*
  - *[Capitanya](../Page/Capitanya.md "wikilink")*
  - [心叶石蚕属](../Page/心叶石蚕属.md "wikilink") *Cardioteucris*
  - *[Catoferia](../Page/Catoferia.md "wikilink")*
  - [柠檬香属](../Page/柠檬香属.md "wikilink") *Cedronella*
  - [鬃尾草属](../Page/鬃尾草属.md "wikilink") *Chaiturus*
  - [矮刺苏属](../Page/矮刺苏属.md "wikilink") *Chamaesphacos*
  - *[Chaunostoma](../Page/Chaunostoma.md "wikilink")*
  - [铃子香属](../Page/铃子香属.md "wikilink") *Chelonopsis*
  - *[Cleonia](../Page/Cleonia.md "wikilink")*
  - [肾茶属](../Page/肾茶属.md "wikilink") *Clerodendranthus*
  - [风轮菜属](../Page/风轮菜属.md "wikilink") *Clinopodium*
  - [羽萼木属](../Page/羽萼木属.md "wikilink") *Colebrookia*
  - [鞘蕊花属](../Page/鞘蕊花属.md "wikilink") *Coleus*
  - *[Collinsonia](../Page/Collinsonia.md "wikilink")*
  - [火把花属](../Page/火把花属.md "wikilink") *Colquhounia*
  - [绵穗苏属](../Page/绵穗苏属.md "wikilink") *Comanthosphace*
  - *[Conradina](../Page/Conradina.md "wikilink")*
  - [簇序草属](../Page/簇序草属.md "wikilink") *Craniotome*
  - *[Cuminia](../Page/Cuminia.md "wikilink")*
  - *[Cunila](../Page/Cunila.md "wikilink")*
  - *[Cyclotrichium](../Page/Cyclotrichium.md "wikilink")*
  - [歧伞花属](../Page/歧伞花属.md "wikilink") *Cymaria*
  - *[Dauphinea](../Page/Dauphinea.md "wikilink")*
  - *[Dicerandra](../Page/Dicerandra.md "wikilink")*
  - *[Dorystaechas](../Page/Dorystaechas.md "wikilink")*
  - [青兰属](../Page/青兰属.md "wikilink") *Dracocephalum*
  - *[Drepanocaryum](../Page/Drepanocaryum.md "wikilink")*
  - [水蜡烛属](../Page/水蜡烛属.md "wikilink") *Dysophylla*
  - *[Eichlerago](../Page/Eichlerago.md "wikilink")*
  - [香薷属](../Page/香薷属.md "wikilink") *Elsholtzia*
  - *[Endostemon](../Page/Endostemon.md "wikilink")*
  - *[Englerastrum](../Page/Englerastrum.md "wikilink")*
  - [沙穗属](../Page/沙穗属.md "wikilink") *Eremostachys*
  - *[Eriope](../Page/Eriope.md "wikilink")*
  - [绵参属](../Page/绵参属.md "wikilink") *Eriiophyton*
  - *[Eriopidion](../Page/Eriopidion.md "wikilink")*
  - *[Eriothymus](../Page/Eriothymus.md "wikilink")*
  - *[Erythrochlamys](../Page/Erythrochlamys.md "wikilink")*
  - [宽管花属](../Page/宽管花属.md "wikilink") *Eurysolen*
  - *[Fuerstia](../Page/Fuerstia.md "wikilink")*
  - [小野芝麻属](../Page/小野芝麻属.md "wikilink") *Galeobdolon*
  - [鼬瓣花属](../Page/鼬瓣花属.md "wikilink") *Galeopsis*
  - [活血丹属](../Page/活血丹属.md "wikilink") *Glechoma*
  - *[Glechon](../Page/Glechon.md "wikilink")*
  - [锥花属](../Page/锥花属.md "wikilink") *Gomphostemma*
  - *[Gontscharovia](../Page/Gontscharovia.md "wikilink")*
  - [四轮香属](../Page/四轮香属.md "wikilink") *Hanceola*
  - *[Haplostachys](../Page/Haplostachys.md "wikilink")*
  - *[Haumaniastrum](../Page/Haumaniastrum.md "wikilink")*
  - *[Hedeoma](../Page/Hedeoma.md "wikilink")*
  - *[Hemiandra](../Page/Hemiandra.md "wikilink")*
  - *[Hemigenia](../Page/Hemigenia.md "wikilink")*
  - *[Hemizygia](../Page/Hemizygia.md "wikilink")*
  - *[Hesperozygis](../Page/Hesperozygis.md "wikilink")*
  - [异野芝麻属](../Page/异野芝麻属.md "wikilink") *Heterolamium*
  - *[Hoehnea](../Page/Hoehnea.md "wikilink")*
  - [全唇花属](../Page/全唇花属.md "wikilink") *Holocheila*
  - *[Holostylon](../Page/Holostylon.md "wikilink")*
  - *[Horminum](../Page/Horminum.md "wikilink")*
  - *[Hoslundia](../Page/Hoslundia.md "wikilink")*
  - *[Hymenocrater](../Page/Hymenocrater.md "wikilink")*
  - *[Hypenia](../Page/Hypenia.md "wikilink")*
  - *[Hypogomphia](../Page/Hypogomphia.md "wikilink")*
  - *[Hyptidendron](../Page/Hyptidendron.md "wikilink")*
  - [山香属](../Page/山香属.md "wikilink") *Hyptis*
  - [神香草属](../Page/神香草属.md "wikilink") *Hyssopus*
  - *[Isodictyophorus](../Page/Isodictyophorus.md "wikilink")*
  - [香茶菜属](../Page/香茶菜属.md "wikilink") *Isodon*
  - *[Isoleucas](../Page/Isoleucas.md "wikilink")*
  - [香简草属](../Page/香简草属.md "wikilink") *Keiskea*
  - [动蕊花属](../Page/动蕊花属.md "wikilink") *Kinostemon*
  - *[Kudrjaschevia](../Page/Kudrjaschevia.md "wikilink")*
  - *[Kurzamra](../Page/Kurzamra.md "wikilink")*
  - [兔唇花属](../Page/兔唇花属.md "wikilink") *Lagochilus*
  - [夏至草属](../Page/夏至草属.md "wikilink") *Lagopsis*
  - [扁柄草属](../Page/扁柄草属.md "wikilink") *Lallemantia*
  - *[Lamiastrum](../Page/Lamiastrum.md "wikilink")*
  - [独一味属](../Page/独一味属.md "wikilink") *Lamiophlomis*
  - [野芝麻属](../Page/野芝麻属.md "wikilink") *Lamium*
  - [熏衣草属](../Page/薰衣草屬.md "wikilink") *Lavandula*
  - *[Leocus](../Page/Leocus.md "wikilink")*
  - *[Leonotis](../Page/Leonotis.md "wikilink")*
  - [益母草属](../Page/益母草属.md "wikilink") *Leonurus*
  - *[Lepechinia](../Page/Lepechinia.md "wikilink")*
  - [绣球防风属](../Page/绣球防风属.md "wikilink") *Leucas*
  - [米团花属](../Page/米团花属.md "wikilink") *Leucosceptrum*
  - [扭藿香属](../Page/扭藿香属.md "wikilink") *Lophanthus*
  - [斜萼草属](../Page/斜萼草属.md "wikilink") *Loxocalyx*
  - [地笋属](../Page/地笋属.md "wikilink") *Lycopus*
  - *[Macbridea](../Page/Macbridea.md "wikilink")*
  - [扭连钱属](../Page/扭连钱属.md "wikilink") *Marmoritis*
  - [欧夏至草属](../Page/欧夏至草属.md "wikilink") *Marrubium*
  - *[Marsypianthes](../Page/Marsypianthes.md "wikilink")*
  - [龙头草属](../Page/龙头草属.md "wikilink") *Meehania*
  - [蜜蜂花属](../Page/蜜蜂花属.md "wikilink") *Melissa*
  - *[Melittis](../Page/Melittis.md "wikilink")*
  - [薄荷属](../Page/薄荷属.md "wikilink")*Mentha*
  - *[Meriandra](../Page/Meriandra.md "wikilink")*
  - [箭叶水苏属](../Page/箭叶水苏属.md "wikilink") *Metastachydium*
  - *[Microcorys](../Page/Microcorys.md "wikilink")*
  - [姜味草属](../Page/姜味草属.md "wikilink") *Micromeria*
  - [冠唇花属](../Page/冠唇花属.md "wikilink") *Microtoena*
  - *[Minthostachys](../Page/Minthostachys.md "wikilink")*
  - *[Moluccella](../Page/Moluccella.md "wikilink")*
  - [美国薄荷属](../Page/美國薄荷屬.md "wikilink") *Monarda*
  - *[Monardella](../Page/Monardella.md "wikilink")*
  - [石荠苧属](../Page/石荠苧属.md "wikilink") *Mosla*
  - *[Neoeplingia](../Page/Neoeplingia.md "wikilink")*
  - *[Neohyptis](../Page/Neohyptis.md "wikilink")*
  - [荆芥属](../Page/荆芥属.md "wikilink") *Nepeta*
  - *[Neustruevia](../Page/Neustruevia.md "wikilink")*
  - [钩萼属](../Page/钩萼属.md "wikilink") *Notochaete*
  - [罗勒属](../Page/罗勒属.md "wikilink") *Ocimum*
  - [喜雨草属](../Page/喜雨草属.md "wikilink") *Ombrocharis*
  - [牛至属](../Page/牛至属.md "wikilink") *Origanum*
  - [鸡脚参属](../Page/鸡脚参属.md "wikilink") *Orthosiphon*
  - *[Otostegia](../Page/Otostegia.md "wikilink")*
  - [脓疮草属](../Page/脓疮草属.md "wikilink") *Panzerina*
  - *[Paraeremostachys](../Page/Paraeremostachys.md "wikilink")*
  - [假野芝麻属](../Page/假野芝麻属.md "wikilink") *Paralamium*
  - [假糙苏属](../Page/假糙苏属.md "wikilink") *Paraphlomis*
  - *[Peltodon](../Page/Peltodon.md "wikilink")*
  - *[Pentapleura](../Page/Pentapleura.md "wikilink")*
  - [紫苏属](../Page/紫苏属.md "wikilink") *Perilla*
  - [分药花属](../Page/分药花属.md "wikilink") *Perovskia*
  - *[Perrierastrum](../Page/Perrierastrum.md "wikilink")*
  - [糙苏属](../Page/糙苏属.md "wikilink") *Phlomis*
  - *[Phlomoides](../Page/Phlomoides.md "wikilink")*
  - *[Phyllostegia](../Page/Phyllostegia.md "wikilink")*
  - *[Physostegia](../Page/Physostegia.md "wikilink")*
  - *[Piloblephis](../Page/Piloblephis.md "wikilink")*
  - *[Pitardia](../Page/Pitardia.md "wikilink")*
  - [仙草属](../Page/仙草属.md "wikilink") *Platostoma*
  - [香茶属](../Page/香茶属.md "wikilink") *Plectranthus*
  - *[Pogogyne](../Page/Pogogyne.md "wikilink")*
  - [刺蕊草属](../Page/刺蕊草属.md "wikilink") *Pogostemon*
  - *[Poliomintha](../Page/Poliomintha.md "wikilink")*
  - *[Prasium](../Page/Prasium.md "wikilink")*
  - *[Prostanthera](../Page/Prostanthera.md "wikilink")*
  - [夏枯草属](../Page/夏枯草属.md "wikilink") *Prunella*
  - *[Prunella](../Page/Self-heal.md "wikilink")*
  - *[Pseuderemostachys](../Page/Pseuderemostachys.md "wikilink")*
  - *[Puntia](../Page/Puntia.md "wikilink")*
  - *[Pycnanthemum](../Page/Pycnanthemum.md "wikilink")*
  - *[Pycnostachys](../Page/Pycnostachys.md "wikilink")*
  - *[Renschia](../Page/Renschia.md "wikilink")*
  - *[Rhabdocaulon](../Page/Rhabdocaulon.md "wikilink")*
  - *[Raphidion](../Page/Raphidion.md "wikilink")*
  - *[Rhododon](../Page/Rhododon.md "wikilink")*
  - [迷迭香属](../Page/迷迭香属.md "wikilink") *Rosmarinus*
  - [钩子木属](../Page/钩子木属.md "wikilink") *Rostrinucula*
  - *[Roylea](../Page/Roylea.md "wikilink")*
  - [掌叶石蚕属](../Page/掌叶石蚕属.md "wikilink") *Rubiteucris*
  - *[Sabaudia](../Page/Sabaudia.md "wikilink")*
  - *[Saccocalyx](../Page/Saccocalyx.md "wikilink")*
  - [鼠尾草属](../Page/鼠尾草属.md "wikilink")*Salvia*
  - *[Satureja](../Page/Satureja.md "wikilink")*
  - [裂叶荆芥属](../Page/裂叶荆芥属.md "wikilink") *Schizonepeta*
  - [四棱草属](../Page/四棱草属.md "wikilink") *Schnabelia*
  - [黄芩属](../Page/黄芩属.md "wikilink") *Scutellaria*
  - [毒马草属](../Page/毒马草属.md "wikilink") *Sideritis*
  - [筒冠花属](../Page/筒冠花属.md "wikilink") *Siphocranion*
  - [子宫草属](../Page/子宫草属.md "wikilink") *Skapanthus*
  - [鞘蕊花属](../Page/鞘蕊花属.md "wikilink") *Solenostemon*
  - [假水苏属](../Page/假水苏属.md "wikilink") *Stachyopsis*
  - [水苏属](../Page/水苏属.md "wikilink") *Stachys*
  - *[Stenogyne](../Page/Stenogyne.md "wikilink")*
  - *[Sulaimania](../Page/Sulaimania.md "wikilink")*
  - [台钱草属](../Page/台钱草属.md "wikilink") *Suzukia*
  - *[Symphostemon](../Page/Symphostemon.md "wikilink")*
  - *[Synandra](../Page/Synandra.md "wikilink")*
  - *[Syncolostemon](../Page/Syncolostemon.md "wikilink")*
  - *[Tetradenia](../Page/Tetradenia.md "wikilink")*
  - [香科属](../Page/香科属.md "wikilink") *Teucrium*
  - *[Thorncroftia](../Page/Thorncroftia.md "wikilink")*
  - *[Thuspeinanta](../Page/Thuspeinanta.md "wikilink")*
  - *[Thymbra](../Page/Thymbra.md "wikilink")*
  - [百里香属](../Page/百里香属.md "wikilink") *Thymus*
  - *[Tinnea](../Page/Tinnea.md "wikilink")*
  - *[Trichostema](../Page/Trichostema.md "wikilink")*
  - *[Warnockia](../Page/Warnockia.md "wikilink")*
  - [保亭花属](../Page/保亭花属.md "wikilink") *Wenchengia*
  - *[Westringia](../Page/Westringia.md "wikilink")*
  - *[Wiedemannia](../Page/Wiedemannia.md "wikilink")*
  - *[Wrixonia](../Page/Wrixonia.md "wikilink")*
  - *[Zataria](../Page/Zataria.md "wikilink")*
  - *[Zhumeria](../Page/Zhumeria.md "wikilink")*
  - [新塔花属](../Page/新塔花属.md "wikilink") *Ziziphora*

## 系統分類學

本分類樹主要依據李波等人於2016年提出的論文為架構\[2\]\[3\]：

## 参考文献

## 外部链接

  - [唇形科](https://web.archive.org/web/20050525101953/http://delta-intkey.com/angio/www/labiatae.htm)
  - \[<http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin='Lamiaceae>'
    唇形科 Lamiaceae\] 藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/唇形科.md "wikilink")
[Category:唇形目](../Category/唇形目.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  Raymond M. Harley, Sandy Atkins, Andrey L. Budantsev, Philip D.
    Cantino, Barry J. Conn, Renée J. Grayer, Madeline M. Harley, Rogier
    P.J. de Kok, Tatyana V. Krestovskaja, Ramón Morales, Alan J. Paton,
    and P. Olof Ryding. 2004. "Labiatae" pages 167-275. In: Klaus
    Kubitzki (editor) and Joachim W. Kadereit (volume editor). *The
    Families and Genera of Vascular Plants* volume VII. Springer-Verlag:
    Berlin; Heidelberg, Germany. ISBN 978-3-540-40593-1
2.
3.