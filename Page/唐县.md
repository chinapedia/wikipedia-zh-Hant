**唐县**，[中国的一个县](../Page/中华人民共和国.md "wikilink")，地处[河北省](../Page/河北省.md "wikilink")，隶属于[保定市](../Page/保定市.md "wikilink")。唐县历史悠久，早在六七千年以前就有人类聚居活动。境内地貌丰富，多[山地](../Page/山地.md "wikilink")，海拔在52－1869.8米之间，素有“七山一水二分田”之称，属于温带半干旱、半湿润的大陆性季风气候。唐县毗邻京津，交通便利，物产丰富，资源充足。[石灰石](../Page/石灰石.md "wikilink")、[石英石](../Page/石英石.md "wikilink")、[花岗岩](../Page/花岗岩.md "wikilink")、[大理石](../Page/大理石.md "wikilink")、[金矿](../Page/金矿.md "wikilink")、[铁矿等矿藏丰富](../Page/铁矿.md "wikilink")。粮食作物主要有[小麦](../Page/小麦.md "wikilink")、[水稻](../Page/水稻.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[谷子等](../Page/谷子.md "wikilink")。

## 历史沿革

唐县古属[冀州](../Page/冀州.md "wikilink")，为尧帝受封故土，所谓“帝喾氏没，帝尧氏作，始封于唐”\[1\]。[西汉置唐县](../Page/西汉.md "wikilink"),属[中山郡](../Page/中山郡.md "wikilink")，后[中山郡改为](../Page/中山郡.md "wikilink")[中山国](../Page/中山郡.md "wikilink")，唐县仍属之；[东汉](../Page/东汉.md "wikilink")、[曹魏](../Page/曹魏.md "wikilink")、[西晋皆因袭之](../Page/西晋.md "wikilink")；[北齐](../Page/北齐.md "wikilink")[天保七年](../Page/天保.md "wikilink")（公元556年）省入[安喜县](../Page/安喜县.md "wikilink")；[隋](../Page/隋朝.md "wikilink")[开皇六年复置县](../Page/开皇.md "wikilink")\[2\]，属[博陵郡](../Page/博陵郡.md "wikilink")；[唐](../Page/唐朝.md "wikilink")[宋属](../Page/宋朝.md "wikilink")[定州](../Page/定州.md "wikilink")；[金属](../Page/金朝.md "wikilink")[中山府](../Page/中山府.md "wikilink")；[元属](../Page/元朝.md "wikilink")[保定路](../Page/保定路.md "wikilink")；[明](../Page/明朝.md "wikilink")[清皆属](../Page/清朝.md "wikilink")[保定府](../Page/保定府.md "wikilink")。现属[河北省](../Page/河北省.md "wikilink")[保定市](../Page/保定市.md "wikilink")。

## 行政区划

下辖9个[镇](../Page/行政建制镇.md "wikilink")、11个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 名人

  - [李汉](../Page/李汉.md "wikilink")
  - [宋子贤](../Page/宋子贤.md "wikilink")

## 外部链接

  - [中国唐县](http://www.tangxian.gov.cn/)
  - [唐县论坛](http://www.txsns.com/)

## 参考文献

[唐县](../Page/category:唐县.md "wikilink")
[县](../Page/category:保定区县市.md "wikilink")
[保定](../Page/category:河北省县份.md "wikilink")
[冀](../Page/分类:国家级贫困县.md "wikilink")

1.  史记
2.  [隋书.地理志](http://www.guoxue.com/shibu/24shi/oldtangsu/jts_043.htm)