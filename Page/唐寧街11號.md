**唐寧街11號**（**11 Downing
Street**）理論上是第二財政大臣的[官邸](../Page/官邸.md "wikilink")，但此職早已由[財政大臣所兼任](../Page/財政大臣_\(英國\).md "wikilink")。

它是現任[保守黨財政大臣](../Page/保守黨_\(英國\).md "wikilink")[夏文達的官邸](../Page/夏文達.md "wikilink")。

## 歷史

唐寧街11號與著名的[唐寧街10號相連](../Page/唐寧街10號.md "wikilink")，經過多年改建，兩者的內部也完全打通。唐寧街11號的右邊就是[唐寧街12號](../Page/唐寧街12號.md "wikilink")，是[黨鞭的官邸](../Page/黨鞭.md "wikilink")。

[Category:伦敦官方办公楼](../Category/伦敦官方办公楼.md "wikilink")
[Category:英国官邸](../Category/英国官邸.md "wikilink")
[\*](../Category/英國財政大臣.md "wikilink")
[Category:倫敦旅遊景點](../Category/倫敦旅遊景點.md "wikilink")