**劉稹之叛**，因其发生于唐武宗会昌年间，史稱**会昌伐叛**，是指[唐朝](../Page/唐朝.md "wikilink")[會昌三年](../Page/会昌_\(唐朝\).md "wikilink")（843年）至四年(844年)，唐廷平定[昭義節度使](../Page/昭義節度使.md "wikilink")[劉稹叛乱之戰](../Page/劉稹.md "wikilink")。

會昌三年（843年）四月，[昭義節度使](../Page/昭義節度使.md "wikilink")[劉從諫病卒](../Page/劉從諫.md "wikilink")，其侄也是养子劉稹用[昭義](../Page/昭義.md "wikilink")（[山西](../Page/山西.md "wikilink")[長治](../Page/長治市.md "wikilink")）兵馬使郭誼的建議，秘不發喪，自領軍務。[监军](../Page/监军.md "wikilink")[崔士康奏称刘从谏病重](../Page/崔士康.md "wikilink")，请求朝廷任命劉稹为[留后](../Page/留后.md "wikilink")。

劉稹因其父刘从谏上書言宦官[仇士良等之惡](../Page/仇士良.md "wikilink")，与朝廷交恶，不敢歸朝。[宰相](../Page/宰相.md "wikilink")[李德裕認為昭义邻近](../Page/李德裕.md "wikilink")[帝都](../Page/帝都.md "wikilink")，不可割据一方，故朝廷不准，五月十三日，唐朝廷下诏令削夺刘从谏及刘稹官爵。武宗會昌四年（844年），李德裕用[成德](../Page/成德.md "wikilink")、[魏博](../Page/魏博.md "wikilink")、[河中等鎮兵力進攻昭義](../Page/河中.md "wikilink")，命[王元逵为泽潞北面招讨使](../Page/王元逵.md "wikilink")，[何弘敬为南面诏讨使](../Page/何弘敬.md "wikilink")，与[河中节度使](../Page/河中.md "wikilink")[陈夷行](../Page/陈夷行.md "wikilink")、[河东节度使](../Page/河东.md "wikilink")[刘沔](../Page/刘沔.md "wikilink")、[河阳节度使](../Page/河阳.md "wikilink")[王茂元合力出擊](../Page/王茂元.md "wikilink")。劉稹命薛茂卿抵抗。

期间，因刘沔与[卢龙节度使](../Page/卢龙节度使.md "wikilink")[张仲武不合](../Page/张仲武.md "wikilink")，武宗调任前宰相[李石为河东节度使](../Page/李石_\(唐朝\).md "wikilink")。刘稹遣军将贾群来见李石，呈上李石堂兄[洺州刺史](../Page/洺州.md "wikilink")[李恬的信说](../Page/李恬.md "wikilink")：“刘稹愿举族归命于相公，奉刘从谏的丧事归葬东都。”李石囚禁贾群，上报书信。李德裕以李石名义回信要刘稹面缚来降。不久河东都将[杨弁趁府库空虚之机煽动士兵兵变驱逐李石](../Page/杨弁.md "wikilink")，与刘稹结盟。朝臣们与[忠武节度使](../Page/忠武节度使.md "wikilink")[王宰建议停止讨伐刘稹](../Page/王宰_\(节度使\).md "wikilink")，接受其投降，但李德裕仍坚持原立场。刘稹为了表明自己并非叛者，反而攻打杨弁。杨弁被平定后，朝廷仍然讨伐刘稹。

是年七月十八日，天德防御使[石雄入潞州](../Page/石雄.md "wikilink")，有七千人隨行，過烏嶺（在[山西](../Page/山西.md "wikilink")[翼城縣境](../Page/翼城.md "wikilink")），破昭義軍五寨。王元逵攻宣务栅（今河北[隆尧西北](../Page/隆尧.md "wikilink")），在尧山（今河北[完县西北](../Page/完县.md "wikilink")）擊敗劉軍。八月十八日，薛茂卿攻陷科斗店。劉稹軍心漸怠，权实握在王协、李士贵手中，將士愈覺離心，大將[高文端向唐軍投降](../Page/高文端.md "wikilink")，[邢州](../Page/邢州.md "wikilink")（今河北邢台）刺史崔嘏归降，洺州（今河北[永年东南](../Page/永年.md "wikilink")）、[磁州](../Page/磁州.md "wikilink")（今河北磁县）相繼倒戈。

劉稹部下[董可武](../Page/董可武.md "wikilink")、[郭誼](../Page/郭誼.md "wikilink")、[王協等人](../Page/王協.md "wikilink")，把劉稹騙入別院殺害，事後[族滅其家](../Page/族滅.md "wikilink")，[郭谊](../Page/郭谊.md "wikilink")、[王协等人出降](../Page/王协.md "wikilink")。[石雄宣佈](../Page/石雄.md "wikilink")[澤潞平定](../Page/澤潞.md "wikilink")，将郭谊、王协等人連同劉稹的首級傳送長安。

[Category:唐朝戰爭](../Category/唐朝戰爭.md "wikilink")
[澤潞](../Category/藩鎮割據.md "wikilink")
[Category:山西历次战争与战役](../Category/山西历次战争与战役.md "wikilink")
[Category:843年](../Category/843年.md "wikilink")
[Category:844年](../Category/844年.md "wikilink")