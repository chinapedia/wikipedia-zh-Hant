**唐才常**（），字**伯平**，號**絨丞**，又字**佛塵**，[湖南](../Page/湖南.md "wikilink")[瀏陽人](../Page/瀏陽.md "wikilink")。生於清[同治六年](../Page/同治.md "wikilink")（1867年），與[譚嗣同同鄉](../Page/譚嗣同.md "wikilink")，同师于[欧阳中鹄](../Page/欧阳中鹄.md "wikilink")，并称为“浏阳二杰”，因發動[唐才常起義](../Page/唐才常起義.md "wikilink")，被[張之洞逮捕](../Page/張之洞.md "wikilink")，[斬首](../Page/斬首.md "wikilink")。

## 簡介

早年就讀於長沙[校經書院](../Page/校經書院.md "wikilink")、[岳麓書院及武昌](../Page/岳麓書院.md "wikilink")[兩湖書院](../Page/兩湖書院.md "wikilink")。

光绪二十三年（1897年），与[熊希龄](../Page/熊希龄.md "wikilink")、谭嗣同、[蒋德钧](../Page/蒋德钧.md "wikilink")、[陈三立等创湖南](../Page/陈三立.md "wikilink")[时务学堂](../Page/时务学堂.md "wikilink")。二十四年（1898年）正月与谭嗣同等发起创立南学会，担任议事会友。八月应谭嗣同电召赴[京師](../Page/京師.md "wikilink")，行至[汉口](../Page/汉口.md "wikilink")，闻[政变发生](../Page/戊戌政變.md "wikilink")，嗣同等就义。遂南返，旋往[上海](../Page/上海.md "wikilink")，历游[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、日本等地。光绪二十五年（1899年）二月回到上海，主编《亚东时报》。秋，再赴[香港](../Page/香港.md "wikilink")、[南洋](../Page/南洋.md "wikilink")、[日本](../Page/日本.md "wikilink")，與[康有为](../Page/康有为.md "wikilink")、[梁启超](../Page/梁启超.md "wikilink")、[孙中山等人接触](../Page/孙中山.md "wikilink")，图谋武装[勤王](../Page/勤王.md "wikilink")。

光緒二十六年（1900年）[義和團事起](../Page/義和團.md "wikilink")，唐在上海與沈藎、畢永年等组织“正气会”，對外託名“東文譯社”，后改名为“自立会”，自任总司令；七月一日，在上海[愚園邀集維新人物](../Page/愚園.md "wikilink")，召開“[中國國會](../Page/中國國會.md "wikilink")”（又名中國議會），由葉浩吾主席，宣布：「不認通匪矯詔之偽政府」「保全中國自主」等事項；並由[容閎以英文起草對外宣言](../Page/容閎.md "wikilink")，謂「決定不認滿洲政府有統治清國之權」。會中推舉容閎、[嚴復為正副會長](../Page/嚴復.md "wikilink")，才常任總幹事，參加者八十餘人，[孫寶瑄](../Page/孫寶瑄.md "wikilink")、[汪康年](../Page/汪康年.md "wikilink")、[章太炎](../Page/章太炎.md "wikilink")、[畢永年等與焉](../Page/畢永年.md "wikilink")。會後決定[自立軍分七路大舉](../Page/自立軍.md "wikilink")，定七月十五日在[汉口](../Page/汉口.md "wikilink")、[汉阳](../Page/汉阳.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[江西](../Page/江西.md "wikilink")、[湖南等地同时起事](../Page/湖南.md "wikilink")。因保皇會匯款不至，擬延於二十九日發難。自立军前军统领[秦力山在安徽](../Page/秦力山.md "wikilink")[淮南大通](../Page/淮南.md "wikilink")，未得情报，仍按期起事，迅遭失败。

清政府闻风声，沿[长江](../Page/长江.md "wikilink")[戒严](../Page/戒严.md "wikilink")。[湖广总督](../Page/湖广总督.md "wikilink")[張之洞對於唐才常的活動早有所聞](../Page/張之洞.md "wikilink")。唐肄業於兩湖書院，算是他的[門生](../Page/門生.md "wikilink")。後有剃頭匠向都司告密，張之洞派兵搜索。七月二十七日晚，汉口总机关被破获，唐与[林圭](../Page/林圭.md "wikilink")、[傅慈祥](../Page/傅慈祥.md "wikilink")、[田邦璿等](../Page/田邦璿.md "wikilink")12人被捕。張之洞特派[鄭孝胥去審問](../Page/鄭孝胥.md "wikilink")，才常說：「此才常所為，勤王事，酬死友，今請速殺！」並獄中題詩“賸好頭顱酬故友，無損面目見群魔”。於七月二十八日夜二更於武昌大朝街滋陽湖畔[斬首](../Page/斬首.md "wikilink")，臨刑前有詩“七尺微軀酬故友，一腔熱血濺荒丘。”同難者共十一人，懸才常首級於漢陽門。

遗稿有《觉颠冥斋内言》4卷，另有部分文稿收入《浏阳二杰遗文》。后有新编《唐才常集》。

## 其它

  - 次子[唐有壬](../Page/唐有壬.md "wikilink")，曾任国民党民国政府外交部常务次长。

[T](../Page/category:長沙人.md "wikilink")

[T](../Category/清朝被處決者.md "wikilink")
[T](../Category/中国革命家.md "wikilink")
[C](../Category/唐姓.md "wikilink")