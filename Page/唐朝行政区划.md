**[唐朝行政区划](../Page/唐朝.md "wikilink")**，是[中国行政区划史中比较重要的转折时期](../Page/中国行政区划.md "wikilink")。在较长时期内，[唐朝采用了](../Page/唐朝.md "wikilink")“道州县”三级制，但是“道”的实际权限相互间差异很大，变革繁多，而且其基础往往是[节度使的实际权力膨胀](../Page/节度使.md "wikilink")，所以这一体系常被称为“虚三级”。而且唐朝疆域经历了显著的扩张到鼎盛到缩减的过程，后期逐渐趋向[五代十国的分裂状态](../Page/五代十国.md "wikilink")，行政区划的记载已经非常不完整。但是唐初始设的“道”的概念，仍然影响了之后的[宋朝](../Page/宋朝.md "wikilink")，并成为“路”的原形。

## 概述

从[汉朝开始逐渐成形的](../Page/汉朝.md "wikilink")[州](../Page/州.md "wikilink")[郡](../Page/郡.md "wikilink")[县三级制](../Page/县.md "wikilink")，经历了[东晋](../Page/东晋.md "wikilink")[十六国](../Page/十六国.md "wikilink")、[南北朝](../Page/南北朝.md "wikilink")，到[隋朝时已经名存实亡](../Page/隋朝.md "wikilink")。[隋先是废除了](../Page/隋.md "wikilink")[郡](../Page/郡.md "wikilink")，但不久又将州改称郡，恢复[秦朝的郡县两级制](../Page/秦朝.md "wikilink")。[唐高祖建国后](../Page/唐高祖.md "wikilink")，将郡改称州，长官复称[汉朝的](../Page/汉朝.md "wikilink")[刺史](../Page/刺史.md "wikilink")，成为一级行政区划，下领县，实行州县两级制。但此时天下已经不是[秦朝建国时的三十六郡的规模](../Page/秦朝.md "wikilink")，州数激增到了三百以上，中央政府管理非常不便。

[唐太宗](../Page/唐太宗.md "wikilink")[贞观元年](../Page/贞观.md "wikilink")（627年），将天下按照山川形势、交通便利分为[十个道](../Page/貞觀十道.md "wikilink")，按需要设监察性的官吏协助中央监管州级行政区。[唐玄宗](../Page/唐玄宗.md "wikilink")[开元二十一年](../Page/开元.md "wikilink")，进一步分成了[十五个道](../Page/開元十五道.md "wikilink")，各道置[采访使](../Page/采访使.md "wikilink")，仿照[西汉的刺史制度](../Page/西汉.md "wikilink")，只起监察性的作用。但是不久之后，即发生了[天宝之乱](../Page/天宝之乱.md "wikilink")，原本只用于边境地区的[节度使制度被广泛用于全国](../Page/节度使.md "wikilink")，节度使一般被授予州刺史，于是同时掌握本州甚至邻州的军政权力，而且其辖区也称为“道”，或称“[藩镇](../Page/藩镇.md "wikilink")”，成为有实际权力的一级非正式行政区划。到[唐宪宗](../Page/唐宪宗.md "wikilink")[元和年间](../Page/元和.md "wikilink")，天下政区已经基本被各节度使、观察使、经略使、防御使瓜分，[贞观](../Page/贞观.md "wikilink")、[开元年间的十道](../Page/开元.md "wikilink")、十五道完全失去了实际意义。

唐朝境内外战争频繁，除州、府、县的常规区划外，还有诸多关、军、监等以军事为主的建制。到唐朝正式灭亡时，天下已经出现了四十多个道和藩镇，其中大多数是节度使管辖。藩镇割据最终导致了[唐王朝的灭亡](../Page/唐朝.md "wikilink")。

## 名称及级别

[唐高祖改隋末的郡为州](../Page/唐高祖.md "wikilink")，[唐玄宗又改州为郡](../Page/唐玄宗.md "wikilink")，到其子[唐肃宗继位后复改郡为州](../Page/唐肃宗.md "wikilink")，所以唐朝总体是以州为主。不过[北宋](../Page/北宋.md "wikilink")[欧阳修等编著的](../Page/欧阳修.md "wikilink")《[新唐书](../Page/新唐书.md "wikilink")》也同时叙述唐玄宗时期的郡，而《[旧唐书](../Page/旧唐书.md "wikilink")》则以州制。但是无论如何，州的概念从此与[汉](../Page/汉朝.md "wikilink")[晋](../Page/晋朝.md "wikilink")[三国时期有了根本性的区别](../Page/三国.md "wikilink")，相对[东汉末年有的州辖一百多个县的情况](../Page/东汉.md "wikilink")，唐朝州的范围大幅缩减到几个或十几个县的范围。此外，许多原来汉晋时的州治所在县，则沿用了原来州的名称，反而其本来的县名逐渐不为人所熟知，如[彭城成为了](../Page/彭城.md "wikilink")[徐州](../Page/徐州.md "wikilink")、[信都成了](../Page/信都.md "wikilink")[冀州](../Page/冀州.md "wikilink")、[昌邑成了](../Page/昌邑.md "wikilink")[兖州](../Page/兖州.md "wikilink")、[江陵成了](../Page/江陵.md "wikilink")[荆州等等](../Page/荆州.md "wikilink")。同时，许多从秦汉朝甚至[春秋战国时代便长期使用的郡名地名](../Page/春秋战国.md "wikilink")，也从此成为历史不再使用，如[琅玡](../Page/琅玡.md "wikilink")、[弘农](../Page/弘农.md "wikilink")、[上党等](../Page/上党.md "wikilink")。

唐朝还将部分比较重要的州命名为府，以示与一般州的区别。同时，与府并列的还有都督府与都护府，但是都督府由于都督权力太大，在设立后又撤除。唐朝还按照经济、地理等因素，将所有的州（府、郡）和县分级，其中州最多分为辅、雄、望、紧、上、中、下，共七等，县则分为京（赤）、畿（望）、上、中、中下、下。各级行政区按照级别的不同，其官吏级别、人数等建制都有所不同。这种按情况将部分次级行政区特别处理的做法，直到今天的[中华人民共和国仍然非常常见](../Page/中华人民共和国.md "wikilink")，如副省级市、省直辖县级市等。

## 地方长官

唐朝的道由于始终以监察为目的，并无长期设置的长官，实际到后期被[节度使等掌控](../Page/节度使.md "wikilink")。

各地的府中，西都（京兆）、东都（河南）、北都（太原）仿古制设“牧”，一般由[亲王担任](../Page/亲王.md "wikilink")，常常不实际管理政务，也经常缺位。西都、东都、北都、凤翔、成都、河中、江陵、兴元、兴德九府各设“[尹](../Page/尹.md "wikilink")”一名，是常置最高长官，其下有“少尹”二名为副。[唐高宗](../Page/唐高宗.md "wikilink")[永徽年间](../Page/永徽.md "wikilink")，尹改名“长史”。之后，西都、东都、北都长史又复称尹，在皇帝不在该都的时候还称“留守”，合称三都留守。

各州置[刺史](../Page/刺史.md "wikilink")，为最高行政长官，但是按照州级别的不同其品级也不同，自从三品到正四品下不等。刺史下有[别驾](../Page/别驾.md "wikilink")、[长史为辅](../Page/长史.md "wikilink")，别驾曾一律改称长史，但后又复置，常由王子担任。

各县设[县令](../Page/县令.md "wikilink")，品级按县的级别从正五品上到从七品下不等，下有[县丞](../Page/县丞.md "wikilink")、[主簿](../Page/主簿.md "wikilink")、[县尉等辅官](../Page/县尉.md "wikilink")。

## 道州列表

|                                        |
| :------------------------------------: |
|               **唐朝道州列表**               |
|                   道                    |
|    [京畿道](../Page/京畿.md "wikilink")     |
|     [雍州](../Page/雍州.md "wikilink")     |
|     [华州](../Page/华州.md "wikilink")     |
|     [同州](../Page/同州.md "wikilink")     |
|    [商州](../Page/商洛市.md "wikilink")     |
|     [岐州](../Page/岐州.md "wikilink")     |
|     [邠州](../Page/邠州.md "wikilink")     |
|    [关内道](../Page/关内道.md "wikilink")    |
|     [泾州](../Page/泾州.md "wikilink")     |
|     [原州](../Page/原州.md "wikilink")     |
|     [渭州](../Page/渭州.md "wikilink")     |
|     [武州](../Page/武州.md "wikilink")     |
|     [甯州](../Page/甯州.md "wikilink")     |
|     [庆州](../Page/庆州.md "wikilink")     |
|     [鄜州](../Page/鄜州.md "wikilink")     |
|     [坊州](../Page/坊州.md "wikilink")     |
|     [丹州](../Page/丹州.md "wikilink")     |
|     [延州](../Page/延州.md "wikilink")     |
|     [灵州](../Page/灵州.md "wikilink")     |
|     [威州](../Page/威州.md "wikilink")     |
|     [会州](../Page/会州.md "wikilink")     |
|     [盐州](../Page/盐州.md "wikilink")     |
|     [夏州](../Page/夏州.md "wikilink")     |
|     [绥州](../Page/绥州.md "wikilink")     |
|     [银州](../Page/银州.md "wikilink")     |
|     [宥州](../Page/宥州.md "wikilink")     |
|     [麟州](../Page/麟州.md "wikilink")     |
|     [胜州](../Page/胜州.md "wikilink")     |
|     [丰州](../Page/丰州.md "wikilink")     |
| [单于大都护府](../Page/单于大都护府.md "wikilink") |
| [安北大都护府](../Page/安北大都护府.md "wikilink") |
| [镇北大都护府](../Page/镇北大都护府.md "wikilink") |
|    [都畿道](../Page/都畿道.md "wikilink")    |
|     [洛州](../Page/洛州.md "wikilink")     |
|     [汝州](../Page/汝州.md "wikilink")     |
|    [河南道](../Page/河南道.md "wikilink")    |
|     [虢州](../Page/虢州.md "wikilink")     |
|     [滑州](../Page/滑州.md "wikilink")     |
| [郑州](../Page/郑州_\(古代\).md "wikilink")  |
|     [潁州](../Page/潁州.md "wikilink")     |
|     [许州](../Page/许州.md "wikilink")     |
|     [陈州](../Page/陈州.md "wikilink")     |
|     [蔡州](../Page/蔡州.md "wikilink")     |
|     [汴州](../Page/汴州.md "wikilink")     |
|     [宋州](../Page/宋州.md "wikilink")     |
|     [亳州](../Page/亳州.md "wikilink")     |
|     [徐州](../Page/徐州.md "wikilink")     |
|     [泗州](../Page/泗州.md "wikilink")     |
|     [濠州](../Page/濠州.md "wikilink")     |
|     [宿州](../Page/宿州.md "wikilink")     |
|     [郓州](../Page/郓州.md "wikilink")     |
|     [齐州](../Page/齐州.md "wikilink")     |
|     [曹州](../Page/曹州.md "wikilink")     |
|     [濮州](../Page/濮州.md "wikilink")     |
|     [青州](../Page/青州.md "wikilink")     |
|     [淄州](../Page/淄州.md "wikilink")     |
|     [登州](../Page/登州.md "wikilink")     |
|     [莱州](../Page/莱州.md "wikilink")     |
|     [棣州](../Page/棣州.md "wikilink")     |
|     [兖州](../Page/兖州.md "wikilink")     |
|     [海州](../Page/海州.md "wikilink")     |
|     [沂州](../Page/沂州.md "wikilink")     |
|     [密州](../Page/密州.md "wikilink")     |
|    [河东道](../Page/河东道.md "wikilink")    |
|     [晋州](../Page/晋州.md "wikilink")     |
|     [绛州](../Page/绛州.md "wikilink")     |
|     [慈州](../Page/慈州.md "wikilink")     |
|     [隰州](../Page/隰州.md "wikilink")     |
|     [并州](../Page/并州.md "wikilink")     |
|     [汾州](../Page/汾州.md "wikilink")     |
|     [沁州](../Page/沁州.md "wikilink")     |
|     [辽州](../Page/辽州.md "wikilink")     |
|     [岚州](../Page/岚州.md "wikilink")     |
|     [宪州](../Page/宪州.md "wikilink")     |
|     [石州](../Page/石州.md "wikilink")     |
|     [忻州](../Page/忻州.md "wikilink")     |
|     [代州](../Page/代州.md "wikilink")     |
|     [云州](../Page/云州.md "wikilink")     |
|     [朔州](../Page/朔州.md "wikilink")     |
|     [蔚州](../Page/蔚州.md "wikilink")     |
|     [武州](../Page/武州.md "wikilink")     |
|     [新州](../Page/新州.md "wikilink")     |
|     [潞州](../Page/潞州.md "wikilink")     |
|     [泽州](../Page/泽州.md "wikilink")     |
|    [河北道](../Page/河北道.md "wikilink")    |
|     [怀州](../Page/怀州.md "wikilink")     |
|     [魏州](../Page/魏州.md "wikilink")     |
|     [博州](../Page/博州.md "wikilink")     |
|     [相州](../Page/相州.md "wikilink")     |
|     [卫州](../Page/卫州.md "wikilink")     |
|     [贝州](../Page/贝州.md "wikilink")     |
|     [澶州](../Page/澶州.md "wikilink")     |
|     [邢州](../Page/邢州.md "wikilink")     |
|     [洺州](../Page/洺州.md "wikilink")     |
|     [惠州](../Page/惠州.md "wikilink")     |
|     [镇州](../Page/镇州.md "wikilink")     |
|     [冀州](../Page/冀州.md "wikilink")     |
|     [深州](../Page/深州.md "wikilink")     |
|     [赵州](../Page/赵州.md "wikilink")     |
|     [沧州](../Page/沧州.md "wikilink")     |
|     [景州](../Page/景州.md "wikilink")     |
|     [德州](../Page/德州.md "wikilink")     |
|     [定州](../Page/定州.md "wikilink")     |
|     [易州](../Page/易州.md "wikilink")     |
|     [幽州](../Page/幽州.md "wikilink")     |
|     [涿州](../Page/涿州.md "wikilink")     |
|     [瀛洲](../Page/瀛洲.md "wikilink")     |
|     [莫州](../Page/莫州.md "wikilink")     |
|     [平州](../Page/平州.md "wikilink")     |
|     [妫州](../Page/妫州.md "wikilink")     |
|     [檀州](../Page/檀州.md "wikilink")     |
|     [蓟州](../Page/蓟州.md "wikilink")     |
|     [营州](../Page/营州.md "wikilink")     |
|  [安东都护府](../Page/安东都护府.md "wikilink")  |
|   [山南东道](../Page/山南东道.md "wikilink")   |
|     [峡州](../Page/峡州.md "wikilink")     |
|     [归州](../Page/归州.md "wikilink")     |
|     [夔州](../Page/夔州.md "wikilink")     |
|     [澧州](../Page/澧州.md "wikilink")     |
|     [朗州](../Page/朗州.md "wikilink")     |
|     [忠州](../Page/忠州.md "wikilink")     |
|     [涪州](../Page/涪州.md "wikilink")     |
|     [万州](../Page/万州.md "wikilink")     |
|     [襄州](../Page/襄州.md "wikilink")     |
|     [泌州](../Page/泌州.md "wikilink")     |
|     [隋州](../Page/隋州.md "wikilink")     |
|     [邓州](../Page/邓州.md "wikilink")     |
|     [均州](../Page/均州.md "wikilink")     |
|     [房州](../Page/房州.md "wikilink")     |
|     [复州](../Page/复州.md "wikilink")     |
|     [郢州](../Page/郢州.md "wikilink")     |
| [金州](../Page/金州_\(西魏\).md "wikilink")  |
|   [山南西道](../Page/山南西道.md "wikilink")   |
|     [洋州](../Page/洋州.md "wikilink")     |
|     [利州](../Page/利州.md "wikilink")     |
|     [凤州](../Page/凤州.md "wikilink")     |
|     [兴州](../Page/兴州.md "wikilink")     |
|     [成州](../Page/成州.md "wikilink")     |
|     [文州](../Page/文州.md "wikilink")     |
|     [扶州](../Page/扶州.md "wikilink")     |
|     [集州](../Page/集州.md "wikilink")     |
|     [壁州](../Page/壁州.md "wikilink")     |
|     [巴州](../Page/巴州.md "wikilink")     |
|     [蓬州](../Page/蓬州.md "wikilink")     |
|     [通州](../Page/通州.md "wikilink")     |
|     [开州](../Page/开州.md "wikilink")     |
|     [阆州](../Page/阆州.md "wikilink")     |
|     [果州](../Page/果州.md "wikilink")     |
|     [渠州](../Page/渠州.md "wikilink")     |
|    [陇右道](../Page/陇右道.md "wikilink")    |
|     [河州](../Page/河州.md "wikilink")     |
|     [渭州](../Page/渭州.md "wikilink")     |
|     [鄯州](../Page/鄯州.md "wikilink")     |
|     [兰州](../Page/兰州.md "wikilink")     |
|     [临州](../Page/临州.md "wikilink")     |
|     [阶州](../Page/阶州.md "wikilink")     |
|     [洮州](../Page/洮州.md "wikilink")     |
|     [岷州](../Page/岷州.md "wikilink")     |
|     [廓州](../Page/廓州.md "wikilink")     |
|     [叠州](../Page/叠州.md "wikilink")     |
|     [宕州](../Page/宕州.md "wikilink")     |
|     [凉州](../Page/凉州.md "wikilink")     |
|     [沙州](../Page/沙州.md "wikilink")     |
|     [瓜州](../Page/瓜州.md "wikilink")     |
|     [甘州](../Page/甘州.md "wikilink")     |
|     [肃州](../Page/肃州.md "wikilink")     |
|     [伊州](../Page/伊州.md "wikilink")     |
|     [西州](../Page/西州.md "wikilink")     |
|     [庭州](../Page/庭州.md "wikilink")     |
| [安西大都护府](../Page/安西大都护府.md "wikilink") |
|    [淮南道](../Page/淮南道.md "wikilink")    |
|     [楚州](../Page/楚州.md "wikilink")     |
|     [滁州](../Page/滁州.md "wikilink")     |
|     [和州](../Page/和州.md "wikilink")     |
|     [寿州](../Page/寿州.md "wikilink")     |
|     [庐州](../Page/庐州.md "wikilink")     |
|     [舒州](../Page/舒州.md "wikilink")     |
|     [光州](../Page/光州.md "wikilink")     |
|     [蕲州](../Page/蕲州.md "wikilink")     |
|     [安州](../Page/安州.md "wikilink")     |
|     [黄州](../Page/黄州.md "wikilink")     |
|     [申州](../Page/申州.md "wikilink")     |
|   [江南东道](../Page/江南东道.md "wikilink")   |
| [升州](../Page/升州_\(唐朝\).md "wikilink")  |
|     [常州](../Page/常州.md "wikilink")     |
|     [苏州](../Page/苏州.md "wikilink")     |
|     [湖州](../Page/湖州.md "wikilink")     |
|     [杭州](../Page/杭州.md "wikilink")     |
|     [睦州](../Page/睦州.md "wikilink")     |
|     [越州](../Page/越州.md "wikilink")     |
|     [明州](../Page/明州.md "wikilink")     |
|     [衢州](../Page/衢州.md "wikilink")     |
|     [处州](../Page/处州.md "wikilink")     |
|     [婺州](../Page/婺州.md "wikilink")     |
|     [温州](../Page/温州.md "wikilink")     |
|     [台州](../Page/台州.md "wikilink")     |
|     [福州](../Page/福州.md "wikilink")     |
|     [建州](../Page/建州.md "wikilink")     |
|     [泉州](../Page/泉州.md "wikilink")     |
|     [汀州](../Page/汀州.md "wikilink")     |
|     [漳州](../Page/漳州.md "wikilink")     |
|   [江南西道](../Page/江南西道.md "wikilink")   |
|     [歙州](../Page/歙州.md "wikilink")     |
|     [池州](../Page/池州.md "wikilink")     |
|     [洪州](../Page/洪州.md "wikilink")     |
| [江州](../Page/江州_\(隋朝\).md "wikilink")  |
|     [鄂州](../Page/鄂州.md "wikilink")     |
|     [岳州](../Page/岳州.md "wikilink")     |
|     [饶州](../Page/饶州.md "wikilink")     |
|     [虔州](../Page/虔州.md "wikilink")     |
|     [吉州](../Page/吉州.md "wikilink")     |
|     [袁州](../Page/袁州.md "wikilink")     |
|     [信州](../Page/信州.md "wikilink")     |
|     [抚州](../Page/抚州.md "wikilink")     |
|     [潭州](../Page/潭州.md "wikilink")     |
|     [衡州](../Page/衡州.md "wikilink")     |
|     [永州](../Page/永州.md "wikilink")     |
|     [道州](../Page/道州.md "wikilink")     |
|     [郴州](../Page/郴州.md "wikilink")     |
|     [邵州](../Page/邵州.md "wikilink")     |
|    [黔中道](../Page/黔中道.md "wikilink")    |
|     [辰州](../Page/辰州.md "wikilink")     |
|     [锦州](../Page/锦州.md "wikilink")     |
|     [施州](../Page/施州.md "wikilink")     |
|     [叙州](../Page/叙州.md "wikilink")     |
|     [奖州](../Page/奖州.md "wikilink")     |
|     [夷州](../Page/夷州.md "wikilink")     |
|     [播州](../Page/播州.md "wikilink")     |
|     [思州](../Page/思州.md "wikilink")     |
|     [费州](../Page/费州.md "wikilink")     |
| [南州](../Page/南州_\(黔中道\).md "wikilink") |
| [溪州](../Page/溪州_\(武周\).md "wikilink")  |
|     [溱州](../Page/溱州.md "wikilink")     |
|    [剑南道](../Page/剑南道.md "wikilink")    |
|     [彭州](../Page/彭州.md "wikilink")     |
|     [蜀州](../Page/蜀州.md "wikilink")     |
|     [汉州](../Page/汉州.md "wikilink")     |
|     [嘉州](../Page/嘉州.md "wikilink")     |
|     [眉州](../Page/眉州.md "wikilink")     |
|     [邛州](../Page/邛州.md "wikilink")     |
|     [简州](../Page/简州.md "wikilink")     |
|     [资州](../Page/资州.md "wikilink")     |
|     [巂州](../Page/巂州.md "wikilink")     |
|     [雅州](../Page/雅州.md "wikilink")     |
|     [黎州](../Page/黎州.md "wikilink")     |
|     [茂州](../Page/茂州.md "wikilink")     |
|     [翼州](../Page/翼州.md "wikilink")     |
|     [维州](../Page/维州.md "wikilink")     |
|     [戎州](../Page/戎州.md "wikilink")     |
|     [姚州](../Page/姚州.md "wikilink")     |
|     [松州](../Page/松州.md "wikilink")     |
|     [当州](../Page/当州.md "wikilink")     |
|     [悉州](../Page/悉州.md "wikilink")     |
|     [静州](../Page/静州.md "wikilink")     |
|     [柘州](../Page/柘州.md "wikilink")     |
|     [恭州](../Page/恭州.md "wikilink")     |
|     [保州](../Page/保州.md "wikilink")     |
|     [真州](../Page/真州.md "wikilink")     |
|     [霸州](../Page/霸州.md "wikilink")     |
|     [乾州](../Page/乾州.md "wikilink")     |
|     [梓州](../Page/梓州.md "wikilink")     |
|     [遂州](../Page/遂州.md "wikilink")     |
|     [绵州](../Page/绵州.md "wikilink")     |
| [剑州](../Page/剑州_\(先天\).md "wikilink")  |
|     [合州](../Page/合州.md "wikilink")     |
|     [龙州](../Page/龙州.md "wikilink")     |
|     [普州](../Page/普州.md "wikilink")     |
|     [渝州](../Page/渝州.md "wikilink")     |
|     [陵州](../Page/陵州.md "wikilink")     |
|     [荣州](../Page/荣州.md "wikilink")     |
|     [昌州](../Page/昌州.md "wikilink")     |
|     [泸州](../Page/泸州.md "wikilink")     |
|  [保宁都护府](../Page/保宁都护府.md "wikilink")  |
|    [岭南道](../Page/岭南道.md "wikilink")    |
|     [冈州](../Page/冈州.md "wikilink")     |
|     [韶州](../Page/韶州.md "wikilink")     |
|     [循州](../Page/循州.md "wikilink")     |
|     [潮州](../Page/潮州.md "wikilink")     |
|     [康州](../Page/康州.md "wikilink")     |
|     [泷州](../Page/泷州.md "wikilink")     |
|     [端州](../Page/端州.md "wikilink")     |
|     [新州](../Page/新州.md "wikilink")     |
|     [封州](../Page/封州.md "wikilink")     |
|     [潘州](../Page/潘州.md "wikilink")     |
|     [春州](../Page/春州.md "wikilink")     |
|     [勤州](../Page/勤州.md "wikilink")     |
|     [罗州](../Page/罗州.md "wikilink")     |
|     [辩州](../Page/辩州.md "wikilink")     |
|     [高州](../Page/高州.md "wikilink")     |
|     [恩州](../Page/恩州.md "wikilink")     |
|     [雷州](../Page/雷州.md "wikilink")     |
|     [崖州](../Page/崖州.md "wikilink")     |
|     [琼州](../Page/琼州.md "wikilink")     |
|     [振州](../Page/振州.md "wikilink")     |
|     [儋州](../Page/儋州.md "wikilink")     |
|    [万安州](../Page/万安州.md "wikilink")    |
|     [邕州](../Page/邕州.md "wikilink")     |
|     [澄州](../Page/澄州.md "wikilink")     |
|     [宾州](../Page/宾州.md "wikilink")     |
|     [横州](../Page/横州.md "wikilink")     |
|     [浔州](../Page/浔州.md "wikilink")     |
|     [峦州](../Page/峦州.md "wikilink")     |
|     [钦州](../Page/钦州.md "wikilink")     |
|     [贵州](../Page/贵州.md "wikilink")     |
|     [龚州](../Page/龚州.md "wikilink")     |
|     [象州](../Page/象州.md "wikilink")     |
|     [藤州](../Page/藤州.md "wikilink")     |
|     [岩州](../Page/岩州.md "wikilink")     |
|     [宜州](../Page/宜州.md "wikilink")     |
|     [瀼州](../Page/瀼州.md "wikilink")     |
|     [笼州](../Page/笼州.md "wikilink")     |
|     [田州](../Page/田州.md "wikilink")     |
|     [环州](../Page/环州.md "wikilink")     |
|     [桂州](../Page/桂州.md "wikilink")     |
|     [梧州](../Page/梧州.md "wikilink")     |
|     [贺州](../Page/贺州.md "wikilink")     |
|     [连州](../Page/连州.md "wikilink")     |
|     [柳州](../Page/柳州.md "wikilink")     |
|     [富州](../Page/富州.md "wikilink")     |
|     [昭州](../Page/昭州.md "wikilink")     |
|     [蒙州](../Page/蒙州.md "wikilink")     |
|     [严州](../Page/严州.md "wikilink")     |
|     [融州](../Page/融州.md "wikilink")     |
|    [思唐州](../Page/思唐州.md "wikilink")    |
|     [古州](../Page/古州.md "wikilink")     |
|     [容州](../Page/容州.md "wikilink")     |
|     [牢州](../Page/牢州.md "wikilink")     |
|     [白州](../Page/白州.md "wikilink")     |
|     [顺州](../Page/顺州.md "wikilink")     |
|     [绣州](../Page/绣州.md "wikilink")     |
|    [郁林州](../Page/郁林州.md "wikilink")    |
|     [党州](../Page/党州.md "wikilink")     |
|     [窦州](../Page/窦州.md "wikilink")     |
|     [禺州](../Page/禺州.md "wikilink")     |
|     [廉州](../Page/廉州.md "wikilink")     |
|     [义州](../Page/义州.md "wikilink")     |
|     [交州](../Page/交州.md "wikilink")     |
|     [陆州](../Page/陆州.md "wikilink")     |
|     [峰州](../Page/峰州.md "wikilink")     |
|     [爱州](../Page/爱州.md "wikilink")     |
|     [驩州](../Page/驩州.md "wikilink")     |
|     [长州](../Page/长州.md "wikilink")     |
|    [福禄州](../Page/福禄州.md "wikilink")    |
|     [汤州](../Page/汤州.md "wikilink")     |
| [芝州](../Page/芝州_\(唐朝\).md "wikilink")  |
|    [武峨州](../Page/武峨州.md "wikilink")    |
|     [演州](../Page/演州.md "wikilink")     |
|    [武安州](../Page/武安州.md "wikilink")    |

## 注释

## 参考文献

  - 《[旧唐书](../Page/旧唐书.md "wikilink")》
  - 《[新唐书](../Page/新唐书.md "wikilink")》
  - 《[中国历史地图集](../Page/中国历史地图集.md "wikilink")》

{{-}}

[Category:中国各朝代行政区划](../Category/中国各朝代行政区划.md "wikilink")
[唐朝行政区划](../Category/唐朝行政区划.md "wikilink")