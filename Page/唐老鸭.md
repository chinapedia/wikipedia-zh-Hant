**唐老鴨**（英語：Donald
Duck）是[迪士尼所創的經典](../Page/迪士尼.md "wikilink")[動畫人物之一](../Page/動畫.md "wikilink")。牠首次出現於1934年、官方生日是1934年6月9日。牠有橙黃色的嘴、腳和蹼，品種為北京鴨\[1\]，身上穿的是Fauntleroy水手裝，但卻沒有穿褲子，愛吃胡萝卜。女友為[黛絲鴨](../Page/黛絲鴨.md "wikilink")（英語：Daisy
Duck）。

唐老鴨的全名是**Donald Fauntleroy Duck**（唐納德·方特洛伊·達克）。此名字出現於1942年的動畫*Donald
Gets Drafted*，印在牠的水手帽上。在迪士尼的網頁裡，亦有提及牠的全名。\[2\]

唐老鴨經常性格暴躁、沮喪時容易放棄，但大部分時間都很隨和、容易滿足，講話口齒不清。

## 配音員

自1934年唐老鴨於[迪士尼動畫亮相開始](../Page/迪士尼.md "wikilink")，一直擔任唐老鴨專屬配音員，直至逝世。1985年Nash因[白血病辭世後](../Page/白血病.md "wikilink")，改由擔任配音。

中國大陸的華語版本早期曾由[張涵予及](../Page/張涵予.md "wikilink")[李揚擔任配音](../Page/李揚.md "wikilink")。

台灣版由[謝文德](../Page/謝文德.md "wikilink")、[蔡閨](../Page/蔡閨.md "wikilink")、[陳志邦擔任配音](../Page/陳志邦.md "wikilink")。

日本版由[山寺宏一擔任配音](../Page/山寺宏一.md "wikilink")。

## 動畫中的唐老鴨

### 早期

1934年6月9日，唐老鴨首次亮相於《糊塗交響樂》系列卡通的中的《聰明的小母雞》。

1936年，唐老鴨於被重新設計，變得稍加豐滿、圓渾。牠亦開始有單獨演出，第一部片是1937年1月9日由Ben
Sharpsteen繪製的卡通**。牠的女友[黛絲鴨](../Page/黛絲鴨.md "wikilink")(Daisy
Duck)亦在本片以Donna Duck之名首次出現。

唐老鴨的三個姪兒則於1年後，即1938年4月15日，在*Donald's Nephews*首次在大銀幕上亮相。此影片由傑克·金（Jack
King）執導。

除了有自己擔當主演的單元外，在1935年播出的米奇的修車站中與[米老鼠及](../Page/米老鼠.md "wikilink")[高飛狗第一次做三人搭配演出](../Page/高飛狗.md "wikilink")，後續到1937年的兩年間三人經常一起演出，到1938年後開始跟[高飛狗做撘檔演出](../Page/高飛狗.md "wikilink")，或是和[奇奇與蒂蒂做對手戲](../Page/奇奇與蒂蒂.md "wikilink")，直到[1999年的米老鼠新傳中三人才繼續合作演出](../Page/1999.md "wikilink")。

## 漫畫中的唐老鴨

唐老鴨的漫畫週刊及月刊在許多[歐洲國家大受歡迎](../Page/歐洲.md "wikilink")，首先風行於[挪威和](../Page/挪威.md "wikilink")[芬蘭](../Page/芬蘭.md "wikilink")，其後有[丹麥](../Page/丹麥.md "wikilink")、
[德國](../Page/德國.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[荷蘭及](../Page/荷蘭.md "wikilink")[瑞典](../Page/瑞典.md "wikilink")。它們大多數由[華特迪士尼公司位於義大利的分公司及位於丹麥](../Page/華特迪士尼公司.md "wikilink")、挪威、芬蘭和瑞典的[Egmont生產和出版](../Page/Egmont_\(media_group\).md "wikilink")。

下列地方亦有唐老鴨漫畫連載——
[澳大利亞](../Page/澳大利亞.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[保加利亞](../Page/保加利亞.md "wikilink")、[中國](../Page/中國.md "wikilink")（内地和[香港](../Page/香港.md "wikilink")）、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[捷克共和國](../Page/捷克共和國.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")（包括[法羅群島](../Page/法羅群島.md "wikilink")）、[埃及](../Page/埃及.md "wikilink")、[愛沙尼亞](../Page/愛沙尼亞.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[法國](../Page/法國.md "wikilink")、[德國](../Page/德國.md "wikilink")、[希臘](../Page/希臘.md "wikilink")、[蓋亞那](../Page/蓋亞那.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[印度](../Page/印度.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[拉脫維亞](../Page/拉脫維亞.md "wikilink")、[立陶宛](../Page/立陶宛.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")、[斯洛維尼亞共和國](../Page/斯洛維尼亞.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[英國](../Page/英國.md "wikilink")、[美國和前](../Page/美利堅合眾國.md "wikilink")[南斯拉夫](../Page/南斯拉夫.md "wikilink")。

### 早期發展

1937年，一位名為Mondadori的義大利出版商首度為唐老鴨打造個人漫畫。該漫畫有18頁、由撰寫。唐老鴨成為主角，並第一次以冒險者之姿態登場，有別於以往單純的喜劇角色。同時，[英格蘭的](../Page/英格蘭.md "wikilink")[Fleetway也開始出版以唐老鴨為主角的漫畫故事](../Page/Fleetway.md "wikilink")。

### 在Taliaferro筆下的發展

從1938年2月2日開始，美國的漫畫家[查爾斯·阿弗雷德·塔里亞菲羅和](../Page/查爾斯·阿弗雷德·塔里亞菲羅.md "wikilink")每日發表一篇的唐老鴨短篇漫畫。塔里亞菲羅負責作畫，Karp則負責故事。

翌年，他們更發表星期日短篇漫畫，為唐老鴨的世界創作更多角色。他養了一隻[聖班納](../Page/聖班納_\(狗\).md "wikilink")，名叫[Bolivar](../Page/Bolivar_\(dog\).md "wikilink")。鴨家族亦開始成長，多了堂叔[史高治和祖母](../Page/史高治.md "wikilink")[Elvira
Coot](../Page/Elvira_Coot.md "wikilink")。唐老鴨早期的墨西哥女朋友名叫[Donna](../Page/Donna_Duck.md "wikilink")，於1940年由[黛絲鴨所取代](../Page/黛絲鴨.md "wikilink")。

### 在Barks筆下的發展

1942年，開始發展關於唐老鴨和迪士尼其他角色的原創漫畫。新發行商除了[卡尔·巴克斯和](../Page/卡尔·巴克斯.md "wikilink")外，。Barks在很多故事中都重覆用了[尋寶這個主題](../Page/尋寶.md "wikilink")。

Barks很快就負擔起唐老鴨漫畫的主要發展工作，包括寫故事和繪畫。在他筆下，漫畫和動畫中的唐老鴨的性格分別愈來愈大，變得愈來愈愛冒險、更雄辯，亦沒有那麼情緒化。

Barks給了唐老鴨一架電單車，出現於1985年出版的[A 1934 Belchfire
Runabout其中一個故事](../Page/A_1934_Belchfire_Runabout.md "wikilink")。\[3\]

## 褲子問題

[芬蘭曾一度禁止唐老鴨](../Page/芬蘭.md "wikilink")，有說是因為他沒穿褲子\[4\]。

## 野心

雖然早期較不明顯，但唐老鴨有著想搶下[米老鼠在迪士尼中作為吉祥物的野心](../Page/米老鼠.md "wikilink")，尤其在[米老鼠新傳的片頭曲結束前最為明顯](../Page/米老鼠新傳.md "wikilink")，甚至有些單元中會見[米老鼠消失後自己會多受歡迎的妄想](../Page/米老鼠.md "wikilink")，這點與[樂一通的](../Page/樂一通.md "wikilink")[達菲鴨極為相像](../Page/達菲鴨.md "wikilink")。

## 家族

## 迪士尼以外

1947年[俄勒岡大學](../Page/俄勒岡大學.md "wikilink")**（英文：University of
Oregon）**運動總監Leo
Harris取得[華特·迪士尼本人的同意](../Page/華特·迪士尼.md "wikilink")，正式讓唐老鴨成為學校的吉祥物。1974年，迪士尼公司正式授權俄勒岡大學，可於與運動相關的地點或事件使用唐老鴨圖像。1991年，此授權更延至學校相關商品。

1984年，唐老鴨50歲生日紀念，俄勒岡大學頒發榮譽學位給唐老鴨，近4000位影迷來到當地機場接機與參與儀式，數千名當地居民連署贈送賀卡歡迎，這些文件目前都存放於迪士尼公司。

唐老鴨的名字和圖像也出現於其他商業產品，例如1940年由Citrus World出品的唐老鴨的橙汁。

## 唐老鴨曾現身的作品

### 電影

  - [致候吾友](../Page/致候吾友.md "wikilink")-*[致候吾友](../Page/致候吾友.md "wikilink")*
    (1942年)
  - [三騎士](../Page/三騎士.md "wikilink")-*[西班牙三紳士](../Page/西班牙三紳士.md "wikilink")*
    (1944年)
  - ** (1983年)
  - [夢城兔福星](../Page/夢城兔福星.md "wikilink")-*[谁陷害了兔子罗杰](../Page/谁陷害了兔子罗杰.md "wikilink")*
    (1988年)
  - [乞丐王子](../Page/乞丐王子.md "wikilink")-*[乞丐王子](../Page/乞丐王子.md "wikilink")*
    (1990年)
  - ** (1995年)
  - [幻想曲2000](../Page/幻想曲2000.md "wikilink")-*[幻想曲2000](../Page/幻想曲2000.md "wikilink")*
    (1999年)
  - ** (1999年)
  - ** (2001年)
  - ** (2003年)
  - ** (2004年)
  - *[三劍客](../Page/米奇·唐老鴨·高飛三劍客.md "wikilink")* (2004年)

### 電視

  - 1987-1990： [唐老鴨俱樂部](../Page/唐老鴨俱樂部.md "wikilink")（*DuckTales*）
  - 1993-1995： *Bonkers*
  - 1996-1997： [唐老鴨新傳](../Page/唐老鴨新傳.md "wikilink")（*Quack Pack*）
  - 1999-2000： [米老鼠新傳](../Page/米老鼠新傳.md "wikilink")
  - 2001-2003： [米老鼠群星會](../Page/米老鼠群星會.md "wikilink")
  - 2006－： [米奇妙妙屋](../Page/米奇妙妙屋.md "wikilink")

### 電玩遊戲

  - ** (1988年)
  - *[唐老鸭梦冒险](../Page/唐老鸭梦冒险.md "wikilink")* (1989年) (唐老鴨只是一個
    [NPC](../Page/非玩家角色.md "wikilink"))
  - ** (1991年)
  - ** (1991年)
  - ** (1992年)
  - *[唐老鸭梦冒险2](../Page/唐老鸭梦冒险2.md "wikilink")* (1993年) (唐老鴨只是一個
    [NPC](../Page/非玩家角色.md "wikilink"))
  - ** (1993年)
  - ** (1996年)
  - *[Disney's Donald Duck: Goin'
    Quackers](../Page/Disney's_Donald_Duck:_Goin'_Quackers.md "wikilink")*
    (2000年)
  - [王國之心](../Page/王國之心.md "wikilink")-*[王國之心系列](../Page/王國之心系列.md "wikilink")*
    (2002年)
  - ** (2002年)
  - ** (2002年)
  - [王國之心 記憶之鍊](../Page/王國之心_記憶之鍊.md "wikilink") (2004年)
  - [王國之心2](../Page/王國之心2.md "wikilink")-''\*[多種語言網頁](http://www.kingdom-hearts.com/language.html)
    \*[日文網頁](http://www.square-enix.co.jp/kingdom2/) (2005年)

### 美國漫畫

  - *[Walt Disney's Comics and
    Stories](../Page/Walt_Disney's_Comics_and_Stories.md "wikilink")*
  - *Donald Duck*
  - *[Uncle Scrooge](../Page/Uncle_Scrooge.md "wikilink")*
  - *[Uncle Scrooge
    Adventures](../Page/Uncle_Scrooge_Adventures.md "wikilink")*
  - *Donald Duck Adventures*
  - *Mickey and Donald*
  - *DuckTales*
  - *Donald and Mickey*
  - *Donald Duck and Mickey Mouse*
  - *Walt Disney Giant*
  - *Walt Disney's Comics and Stories Penny Pincher*
  - *Uncle Scrooge and Donald Duck*
  - *The Adventurous Uncle Scrooge McDuck*
  - *Kingdom Hearts*

### 著名畫師

  - [Carl Barks](../Page/Carl_Barks.md "wikilink")
  - [Luciano Bottaro](../Page/Luciano_Bottaro.md "wikilink")
  - [Giovan Battista Carpi](../Page/Giovan_Battista_Carpi.md "wikilink")
  - [Giorgio Cavazzano](../Page/Giorgio_Cavazzano.md "wikilink")
  - [William Van Horn](../Page/William_Van_Horn.md "wikilink")
  - [Daan Jippes](../Page/Daan_Jippes.md "wikilink")
  - [Don Rosa](../Page/Don_Rosa.md "wikilink")
  - [Marco Rota](../Page/Marco_Rota.md "wikilink")
  - [Romano Scarpa](../Page/Romano_Scarpa.md "wikilink")
  - [Tony Strobl](../Page/Tony_Strobl.md "wikilink")
  - [Al Taliaferro](../Page/Al_Taliaferro.md "wikilink")

## 延伸閱讀

  - Ariel Dorfman, Armand Mattelart, David Kunzle (trans.), *How to Read
    Donald Duck: Imperialist Ideology in the Disney Comic* ISBN
    0-88477-023-0 (Anti-Donald Duck Marxist Critique)
  - Walt Disney Productions, *Walt Disney's Donald Duck: 50 Years of
    Happy Frustration*, Courage Books, May 1990 ASIN: 0894715305

## 参考文献

## 外部連結

  - [Donald Duck comics](http://www.donaldduckcomics.com)
  - [All the
    Ducks](http://duckman.pettho.com/characters/characters.html)
  - [Donald's profile in the
    Inducks](http://coa.inducks.org/character.php/x/DD)
  - [Paperinik's profile in the
    Inducks](http://coa.inducks.org/character.php/x/PK)
  - [Donald's profile in the Disney
    HooZoo](https://web.archive.org/web/20051216010859/http://users.cwnet.com/xephyr/rich/dzone/hoozoo/donald.html)
  - [Toonopedia: Donald Duck](http://www.toonopedia.com/donald.htm)
  - [Donald Duck shorts
    film](https://web.archive.org/web/20051214100257/http://disneyshorts.toonzone.net/characters/donald.html)
  - [Donald Duck advertising
    pinbacks](http://www.marklansdown.com/pinbacks/pages/donaldduck.html)
  - [問雅虎\! - 2005年10月5日 -
    為何唐老鴨不穿褲子?(英文)](http://ask.yahoo.com/20051005.html)
  - [迪士尼所有動畫電影年表](https://web.archive.org/web/20051215113717/http://disney.wretch.cc/disney.htm)
  - [迪士尼中文官方网站：米奇和好友](https://web.archive.org/web/20090221062039/http://d.dolmagic.cn/characters/mickey/)
  - [迪士尼中文官方网站：唐老鸭](https://web.archive.org/web/20090409160613/http://d.dolmagic.cn/story/character_detail.jsp?id=12)

[\*](../Category/唐老鸭.md "wikilink")
[Category:1934年面世的虛構角色](../Category/1934年面世的虛構角色.md "wikilink")
[Category:动物超级英雄](../Category/动物超级英雄.md "wikilink")
[Category:迪士尼漫画角色](../Category/迪士尼漫画角色.md "wikilink")
[Category:唐老鸭世界角色](../Category/唐老鸭世界角色.md "wikilink")
[Category:虚构鸭](../Category/虚构鸭.md "wikilink")
[Category:拟人化武术家](../Category/拟人化武术家.md "wikilink")
[Category:虚构士兵](../Category/虚构士兵.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:迪士尼漫畫作品](../Category/迪士尼漫畫作品.md "wikilink")

1.
2.  <http://disney.go.com/vault/archives/characterstandard/donald/donald.html>
3.  <http://coa.inducks.org/story.php/0/CB+OIL+127>
4.  [1](http://www.snopes.com/disney/films/finland.htm)