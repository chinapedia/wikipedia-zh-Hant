**唐隆**（710年六月初四日—七月二十日）是唐少帝[李重茂的年号](../Page/李重茂.md "wikilink")。

[唐玄宗李隆基執政期的](../Page/唐玄宗.md "wikilink")[开元](../Page/开元.md "wikilink")、[天宝年間](../Page/天寶_\(唐朝\).md "wikilink")，因[避讳而改称](../Page/避讳.md "wikilink")**唐元**、**唐興**、**唐安**\[1\]。

## 大事记

  - 景龍四年六月甲申改元唐隆\[2\]\[3\]。
  - 唐隆元年七月己巳改元景雲\[4\]\[5\]。

## 出生

## 逝世

  - 景龍四年-----[唐中宗李顯](../Page/唐中宗.md "wikilink")
  - 死於[唐隆之變者](../Page/唐隆之變.md "wikilink")－韋璿、韋播、高嵩、[韋皇后](../Page/韋皇后.md "wikilink")、[安樂公主](../Page/安樂公主.md "wikilink")、[武延秀](../Page/武延秀.md "wikilink")、[上官婉兒](../Page/上官婉兒.md "wikilink")、[韋溫](../Page/韋溫.md "wikilink")、內將軍賀婁氏（女）、趙履溫、[韋巨源](../Page/韋巨源.md "wikilink")、馬秦客、楊均、葉靜能等。

## 纪年

| 唐隆                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 710年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [中元克復](../Page/中元克復.md "wikilink")（710年七月—八月）：譙王[李重福之年號](../Page/李重福.md "wikilink")
      - [和銅](../Page/和銅.md "wikilink")（708年[正月十一](../Page/正月十一.md "wikilink")—715年[九月初二](../Page/九月初二.md "wikilink")）：[奈良時代](../Page/奈良時代.md "wikilink")[元明天皇之年號](../Page/元明天皇.md "wikilink")

## 参考文献

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:710年代中国政治](../Category/710年代中国政治.md "wikilink")
[Category:710年](../Category/710年.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年, 第101页。ISBN 7101025129
2.  [:s:舊唐書/卷7](../Page/:s:舊唐書/卷7.md "wikilink")
    六月...甲申，發喪於太極殿，宣遺制。皇太后臨朝，大赦天下，改元為唐隆。
3.  [:s:新唐書/卷005](../Page/:s:新唐書/卷005.md "wikilink")
    景雲元年六月...甲申，乃發喪。又矯遺詔，自立為皇太后。皇太子即皇帝位，以睿宗參謀政事，大赦，改元曰唐隆。
4.  [:s:舊唐書/卷7](../Page/:s:舊唐書/卷7.md "wikilink")
    秋七月...己巳，冊平王為皇太子。大赦天下，改元為景雲。
5.  [:s:新唐書/卷005](../Page/:s:新唐書/卷005.md "wikilink")
    景雲元年...七月...己巳，大赦，改元