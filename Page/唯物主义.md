**唯物论**（），[哲學理論](../Page/哲學.md "wikilink")，認為世界的基本成份為[物質](../Page/物質.md "wikilink")，所有的事物
(包含心靈及意識) 都是物質交互作用的結果。

物質形式與過程是人類認識世界的主要途徑\[1\]，持着「只有事实上的物质才是真实存在的实体」的这一种观点，并且被认为是[物理主義的一种形式](../Page/物理主義.md "wikilink")。该理论的基础是，所有的[实体](../Page/实体.md "wikilink")（和[概念](../Page/概念.md "wikilink")）都是物质的一种构成或者表达，并且，所有的现象（包括[意识](../Page/意识.md "wikilink")）都是物质相互作用的结果，在意识与物质之间，物质决定了意识，而意识则是客观世界在人脑中的生理反应，也就是有机物出于对物质的反应。因此，[物质是唯一事实上存在的实体](../Page/物质.md "wikilink")。作为一个理论体系，唯物主义属于[一元](../Page/一元論.md "wikilink")[本体论](../Page/本体论.md "wikilink")。但其本身又不同于以[二元论或多元论为基础的](../Page/二元论.md "wikilink")[本体论](../Page/本体论.md "wikilink")。

唯物主义是现实世界的一种解释。[恩格斯及](../Page/恩格斯.md "wikilink")[馬克思主義哲學認為唯物主义和](../Page/馬克思主義哲學.md "wikilink")[唯心主义是对立的](../Page/唯心主义.md "wikilink")\[2\]。

## 概述

唯物主义在哲学上屬於[一元的](../Page/一元论.md "wikilink")[本体论](../Page/本体论_\(哲学\).md "wikilink")，因此就和[二元論及](../Page/二元論.md "wikilink")[多元論產生的本体论不同](../Page/多元論.md "wikilink")。以對現象實質的解釋來說，唯物主义和[唯心主義](../Page/唯心主義.md "wikilink")、及不同。

雖然有許多的哲學流派，有些流派之間可能只有短微的差異\[3\]\[4\]\[5\]，不過許多哲學可以分為以下兩類，這兩類在其定義上是互斥的，分別是[唯心主義及唯物主义](../Page/唯心主義.md "wikilink")。這兩類哲學的基本主張和[現實的本質有關](../Page/現實.md "wikilink")，而其主要差異出現在對以下二個基本問題的回答上：「現實包括什麼？」以及「現實如何產生的？」。對唯心主義而言，心、靈或意念是主要的，物質是次要的，對唯物主义而言，物質是主要的，心、靈或意念是次要的，是物質的產物\[6\]。

历史上，源于[笛卡尔的传统观点认为](../Page/笛卡尔.md "wikilink")，意识是非物质的存在。而唯物主义者的观点则是从根本上反对这一信条的。尽管如此，唯物主义本身并没有讨论如何来描述所谓的“物质实体”。在实践中，唯物主义通常等同于[物理主义或其他理论的一个种类](../Page/物理主义.md "wikilink")。

唯物主义通常被认为与[简化论的方法论原理有关](../Page/简化论.md "wikilink")，根据简化论，被一个特定描述层次所区分出来的事物或者现象，如果它是真的（正确的），那它也必须可以在其他描述层次中被解释——特别是在一个普通或简化了的层次中可以被解释。而非简化唯物主义就明确地反对这一种概念或说法，但无论是简化唯物主义还是非简化唯物主义，都通常将[物质的构成作为所有存在事物](../Page/物质.md "wikilink")、性质、无法解释现象的物质基础。[杰瑞·福多通过描述经验主义和某些特殊学科](../Page/杰瑞·福多.md "wikilink")（如[心理学和](../Page/心理学.md "wikilink")[地质学](../Page/地质学.md "wikilink")）是无法直接从基本物理中观察出，从而有力地证明了这个观点。并且，很多有关的文献都是基于这些观点之间的联系而发展起来的。

现代的唯物主义哲学家们扩充了唯物主义中关于物质的定义，其中包括符合科学并能探测得到的实体，如[能量](../Page/能量.md "wikilink")、[力](../Page/力.md "wikilink")、甚至[空间曲率等](../Page/曲率#空间的曲率.md "wikilink")。尽管如此，像这样的哲学家，依然认为概念中的“物质”是非常难以定义的。\[7\]特别地，唯物主义是[二元论](../Page/二元论.md "wikilink")、[现象论](../Page/现象论.md "wikilink")、[理想主义](../Page/理想主义.md "wikilink")、[活力论以及](../Page/活力论.md "wikilink")的相反对照面。在某种程度上，唯物主义的物质性，可以和[决定论的概念产生联系](../Page/决定论.md "wikilink")。[启蒙时代的一些思想家認同决定论](../Page/启蒙时代.md "wikilink")。

唯物主义通常会被唯心主义思想家批评和反对。他们认为唯物主义是一种令精神上空洞的哲学观。而马克思则将唯物主义作为一种非形而上学的“[唯物史观](../Page/唯物史观.md "wikilink")”，相对地，他将其专注于人类活动（包括实践和劳动）和由此产生的[制度生成](../Page/制度.md "wikilink")、复制和摧毁。（请参照“[唯物史观](../Page/唯物史观.md "wikilink")”）

## 對物質的定義

正如其他科學及哲學中的基本概念一様，針對物質的本質及其定義也已出現許多的論點\[8\]。物質是由單一成份（）組成？物質是否是一種單一的成份，但會以許多不同的形式出現（）\[9\]？或是由幾種不同的，不會改變的成份組成（[原子論](../Page/原子論.md "wikilink")）？\[10\]物質是否有其固有性質（）\[11\]\[12\]或者是否其中少了固有性質（）？

傳統對物質的觀點是有形體的「東西」，但在十九世紀提出[场的概念後](../Page/场_\(物理\).md "wikilink")，上述觀點受到了挑戰。[狭义相对论證明物質及能量](../Page/狭义相对论.md "wikilink")（包括將能量分布在空間中的場）是可以互相轉換的。因此產生了一種[本體論的觀點](../Page/本體論.md "wikilink")，認為能量是，物質只是它的一種形態。但另一方面，[粒子物理的](../Page/粒子物理.md "wikilink")[標準模型用](../Page/標準模型.md "wikilink")[量子场论來描述所有的交互作用力](../Page/量子场论.md "wikilink")，因此又可以說這些場才是第一元素，能量只是場的一個性質而已。

依照主流的宇宙模型[ΛCDM模型](../Page/ΛCDM模型.md "wikilink")，宇宙中的能量中只有5%是由粒子物理中標準模型提到的粒子所組成，宇宙主要是由[暗物质及](../Page/暗物质.md "wikilink")[暗能量組成](../Page/暗能量.md "wikilink")，不過科學家對暗物质及暗能量的了解及共識還不多\[13\]。

在量子力學出現之後，有些科學家認為只是改變了物質的概念而已，但也有科學家認為物質概念已不再存在。例如[维尔纳·海森堡提到](../Page/维尔纳·海森堡.md "wikilink")：「唯物主義的本體論是以一個幻想為前提：認為我們週圍世界的現實及存在的方式可以外推到原子層次。但這個外推是錯的，原子不是（一般認知的）東西。」

對應新的科學發現，物質的概念也隨之改變
。[诺姆·乔姆斯基認為任何一種](../Page/诺姆·乔姆斯基.md "wikilink")都可以視為是物質，只要有人定義有這種性質的東西是物質即可\[14\]。

## 腳註

**a.**
不過，要定義一個分類，而不提出此分類和哪些分類是相對的，有些研究者認為就算有可能以此方式定義，定義上也相當困難\[15\]\[16\]

## 參考資料

## 相关主题

  - [本原](../Page/本原.md "wikilink")

  - [荀子](../Page/荀子.md "wikilink"):類似西方樸素唯物論

  - [無神論](../Page/無神論.md "wikilink")

  - [佛教](../Page/佛教.md "wikilink")

  - [顺世论](../Page/顺世论.md "wikilink")

  - [耆那教](../Page/耆那教.md "wikilink")

  -
  -
  -
  - [辩证唯物主义](../Page/辩证唯物主义.md "wikilink")

  - [二元論](../Page/二元論.md "wikilink")

  -
  -
<!-- end list -->

  - [存在](../Page/存在.md "wikilink")

  -
  -
  - [历史唯物主义](../Page/历史唯物主义.md "wikilink")

  - [人文主義](../Page/人文主義.md "wikilink")

<!-- end list -->

  - [唯心主义](../Page/唯心主义.md "wikilink")
  - [主观唯心主义](../Page/主观唯心主义.md "wikilink")
  - [中觀派](../Page/中觀派.md "wikilink")．大乘佛教的一個主要派系
  - [唯物女性主義](../Page/唯物女性主義.md "wikilink")

<!-- end list -->

  -
  - [物質](../Page/物質.md "wikilink")

<!-- end list -->

  - [形而上自然主義](../Page/形而上自然主義.md "wikilink")

  -
  - [自然主义](../Page/自然主义.md "wikilink")

  - [後物質主義](../Page/後物質主義.md "wikilink")

  - [物理主义](../Page/物理主义.md "wikilink")

  - [精神哲学](../Page/精神哲学.md "wikilink")

  -
  -
  -
  - [上座部佛教](../Page/上座部佛教.md "wikilink")

  -
## 延伸閱讀

  - Buchner, L. (1920). *\[books.google.com/books?id=tw8OuwAACAAJ Force
    and Matter\]*. New York, Peter Eckler Publishing Co.

  - Churchland, Paul (1981). *[Eliminative Materialism and the
    Propositional Attitudes](http://www.jstor.org/stable/2025900)*. The
    Philosophy of Science. Boyd, Richard; P. Gasper; J. D. Trout.
    Cambridge, Massachusetts, MIT Press.

  -
  -
  - Fodor, J.A. (1974). Special Sciences, *Synthese*, Vol.28.

  - Gunasekara, Victor A. (2001). "[Buddhism and the Modern
    World](http://www.buddhismtoday.com/english/buddha/Teachings/basicteaching11.htm)".
    ''Basic Buddhism: A Modern Introduction to the Buddha's Teaching".
    18 January 2008

  - Kim, J. (1994) [Multiple Realization and the Metaphysics of
    Reduction](http://www.jstor.org/stable/2107741), *Philosophy and
    Phenomenological Research*, Vol. 52.

  - La Mettrie, La Mettrie, Julien Offray de (1748). *L'Homme Machine*
    (*Man a Machine*)

  - Lange, Friedrich A.,(1925) *[The History of
    Materialism](http://www.worldcat.org/title/history-of-materialism/oclc/703434926)*.
    New York, Harcourt, Brace, & Co.

  -
  - Alternative ISBN 978-0-14-013069-0

  - Schopenhauer, Arthur (1969). *The World as Will and Representation*.
    New York, Dover Publications, Inc.

  - Seidner, Stanley S. (June 10, 2009). ["A Trojan Horse:
    Logotherapeutic Transcendence and its Secular Implications for
    Theology"](http://docs.google.com/gview?a=v&q=cache:FrKYAo88ckkJ:www.materdei.ie/media/conferences/a-secular-age-parallel-sessions-timetable.pdf+%22Stan+Seidner%22&hl=en&gl=us).
    *Mater Dei Institute*

  -
  - Vitzthum, Richard C. (1995) *[Materialism: An Affirmative History
    and
    Definition](https://books.google.com/books/about/Materialism.html?id=odjWAAAAMAAJ)*.
    Amherst, New York, Prometheus Books.

## 外部連結

  - Stanford Encyclopedia:
      - [Physicalism](http://plato.stanford.edu/entries/physicalism/)
      - [Eliminative
        Materialism](http://plato.stanford.edu/entries/materialism-eliminative)
  - [Philosophical Materialism (by Richard C.
    Vitzthum)](http://www.infidels.org/library/modern/richard_vitzthum/materialism.html)
    from infidels.org
  - [Dictionary of the Philosophy of Mind on
    Materialism](https://sites.google.com/site/minddict/materialism)
    from the [University of
    Waterloo](../Page/University_of_Waterloo.md "wikilink")

[W](../Category/形上學.md "wikilink") [W](../Category/主义.md "wikilink")
[W](../Category/哲学基本问题.md "wikilink")
[Category:形而上学理论](../Category/形而上学理论.md "wikilink")

1.  [藝術與建築索引典—唯物論（哲學運動）](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300311113)
     於2011 年3 月14 日查閱

2.  [馬克思新唯物主義理論概述](http://theory.people.com.cn/BIG5/179412/184669/13615645.html)

3.   Alternative ISBN 978-0-02-894950-5

4.   Alternative ISBN 978-0-14-013069-0

5.

6.
7.  Mary Midgley *The Myths We Live By*.

8.

9.  ["Hylomorphism"](http://www.britannica.com/ebc/article-9041771)
    *Concise Britannica*

10. ["Atomism: Antiquity to the Seventeenth
    Century"](http://etext.lib.virginia.edu/cgi-local/DHI/dhi.cgi?id=dv1-21)
     *Dictionary of the History of Ideas*
    ["Atomism in the Seventeenth
    Century"](http://etext.lib.virginia.edu/cgi-local/DHI/dhi.cgi?id=dv1-22)
     *Dictionary of the History of Ideas*
    [Article by a philosopher who opposes
    atomism](http://people.umass.edu/schaffer/papers/Fundamental.pdf)
    [Information on Buddhist
    atomism](http://www.abstractatom.com/buddhist_atomism_and_the_r_theory_of_time.htm)

    [Article on traditional Greek
    atomism](http://plato.stanford.edu/entries/democritus/)
    ["Atomism from the 17th to the 20th
    Century"](http://plato.stanford.edu/entries/atomism-modern/)
    *Stanford Encyclopedia of Philosophy*

11.

12.

13. Bernard Sadoulet "Particle Dark Matter in the Universe: At the Brink
    of Discovery?" *Science* 5 January 2007: Vol. 315. no. 5808, pp. 61
    - 63

14. Chomsky, Noam (2000) *New Horizons in the Study of Language and
    Mind*

15.
16.