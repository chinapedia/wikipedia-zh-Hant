[Tibetan_writer-_blogger_Tsering_Woeser_on_26_March_2009_from_Voice_of_America's_Chinese_service.jpg](https://zh.wikipedia.org/wiki/File:Tibetan_writer-_blogger_Tsering_Woeser_on_26_March_2009_from_Voice_of_America's_Chinese_service.jpg "fig:Tibetan_writer-_blogger_Tsering_Woeser_on_26_March_2009_from_Voice_of_America's_Chinese_service.jpg")
**唯色**（， ），全名**茨仁唯色**（，其本人使用 Tsering Woeser
的拉丁字母转写，意为“永恒的光辉”），汉名**程文萨**\[1\]，[中国](../Page/中国.md "wikilink")[藏族女诗人](../Page/藏族.md "wikilink")、作家。生于[拉萨](../Page/拉萨.md "wikilink")，籍贯[四川](../Page/四川.md "wikilink")[甘孜](../Page/甘孜藏族自治州.md "wikilink")[德格](../Page/德格县.md "wikilink")。

## 家庭

唯色1966年出生於[文化大革命中的](../Page/文化大革命.md "wikilink")[拉薩](../Page/拉薩.md "wikilink")，从小在四川藏区和汉区长大。唯色的祖父是[江津汉人](../Page/江津区.md "wikilink")，祖母是[康区](../Page/康区.md "wikilink")[德格的藏人](../Page/德格.md "wikilink")。唯色的父亲泽仁多吉（汉名[程宽德](../Page/程宽德.md "wikilink")）通[藏](../Page/藏語.md "wikilink")、[漢雙語](../Page/漢語.md "wikilink")，1950年初，[毛泽东派军入藏](../Page/西藏和平解放.md "wikilink")，经过其家乡[德格](../Page/德格.md "wikilink")（今[四川省](../Page/四川省.md "wikilink")[甘孜藏族自治州](../Page/甘孜藏族自治州.md "wikilink")）時，13岁的泽仁多吉被他的父親送去入伍
，随[解放军入藏](../Page/解放军.md "wikilink")，1950年代中期是少尉，文革时在[西藏军区政治部是副团职军官](../Page/西藏军区.md "wikilink")，病故前是拉萨军分区副司令。母亲是[日喀则地区的藏人](../Page/日喀则.md "wikilink")，外祖父曾是[噶厦](../Page/噶厦.md "wikilink")[噶伦](../Page/噶伦.md "wikilink")、[昌都总管](../Page/昌都总管.md "wikilink")[拉鲁·次旺多吉的管家](../Page/拉鲁·次旺多吉.md "wikilink")\[2\]\[3\]。

## 經歷

1988年7月，唯色從[成都](../Page/成都.md "wikilink")[西南民族学院汉语文系畢業](../Page/西南民族学院.md "wikilink")，当过《[甘孜州报](../Page/甘孜州.md "wikilink")》记者。1990年回到西藏，長期擔任拉萨《[西藏文学](../Page/西藏文学_\(杂志\).md "wikilink")》杂志编辑。

2003年1月，唯色撰写的散文集《[西藏笔记](../Page/西藏笔记.md "wikilink")》由[花城出版社出版](../Page/花城出版社.md "wikilink")。几个月后，该书被当局查禁。唯色而后没有接受编辑部的[思想教育](../Page/思想教育.md "wikilink")，不愿做检讨而离开拉萨，并拒绝接受杂志社编辑部其他工作委派，杂志社编辑部的上级主管单位西藏自治区文联将其作离职处理，并发表《西藏文联关于唯色新作〈西藏笔记〉的审读意见及其处理建议》对其进行批判。其后唯色即脱离国家[事业单位体制](../Page/事业单位.md "wikilink")，成为自由撰稿人。

2004年底，与中国作家[王力雄结婚](../Page/王力雄.md "wikilink")。

2006年，唯色将其父在文革期间拍摄的拉萨照片，与自己对这些照片的背景进行的采访结果，整理成《[杀劫](../Page/杀劫.md "wikilink")》一书，在台湾出版，同时出版的还有收录西藏文革口述历史的《[西藏记忆](../Page/西藏记忆.md "wikilink")》，而這兩本互為表裡的書提供了前所未见的研究中国统治下西藏文革期间情况的影像和第一手素材。作者本人就前一书名解释道：“‘杀劫’是藏语‘革命’的发音，而‘文化’藏语的发音与汉语的‘人类’发音相近。”

2010年11月，唯色的博客、推特及Gmail邮箱密码，星期二凌晨遭黑客攻击。博客被删除，推特页面被恶意更改。而网友认为，攻击者的背后可能是中國政府。湖北网络作家刘逸明认为，“官方一般情况下，都是先来软的或者就是先来硬的，一般就是软硬兼施。如果说软硬兼施都不行了，我就耍流氓，偷偷的搞破坏，让你觉得干扰太严重了，你受不了了。”\[4\]

## 获奖

  - 2005年、2009年，两度获[人权观察颁发的](../Page/人权观察.md "wikilink")[赫尔曼-哈米特奖](../Page/赫尔曼-哈米特奖.md "wikilink")。
  - 2007年12月，获[印度的](../Page/印度.md "wikilink")[西藏记者协会颁发的](../Page/西藏记者协会.md "wikilink")“无畏言论者”奖和[挪威作家联盟颁发的](../Page/挪威.md "wikilink")“自由表达奖”。\[5\]
  - 2010年3月20日，唯色获[独立中文笔会第五届林昭纪念奖](../Page/独立中文笔会.md "wikilink")，但她因无法得到出境手续，而未能前往[香港领奖](../Page/香港.md "wikilink")。\[6\]
  - 2010年5月10日，唯色获[国际妇女传媒基金会](../Page/国际妇女传媒基金会.md "wikilink")（International
    Women's Media Foundation，IWMF）2010年度新闻勇气奖。\[7\]
  - 2011年，获荷兰克劳斯亲王基金会颁发的[克劳斯亲王奖](../Page/克劳斯亲王奖.md "wikilink")。\[8\]
  - 2013年3月，獲美國國務院頒發的[國際婦女勇氣獎](../Page/國際婦女勇氣獎.md "wikilink")。

## 作品

  - 诗集《[西藏在上](../Page/西藏在上.md "wikilink")》，1999年，[青海人民出版社](../Page/青海人民出版社.md "wikilink")

  - 散文集《[西藏笔记](../Page/西藏笔记.md "wikilink")》（後改名為《[名為西藏的詩](../Page/名為西藏的詩.md "wikilink")》於台灣出版），2003年，广州[花城出版社](../Page/花城出版社.md "wikilink")

  - 《[西藏：绛红色的地图](../Page/绛红色的地图.md "wikilink")》，2003年，台湾[时英文化](../Page/时英文化.md "wikilink")

  - 《[绛红色的地图](../Page/绛红色的地图.md "wikilink")》，2004年2月，[中国旅游出版社](../Page/中国旅游出版社.md "wikilink")

  - 台湾大块文化2006年出版，2016年出版新版。\[9\]被翻译为日文版（2009年）、藏文版（2009年）。\[10\]

  -
  - 访谈集《[西藏记忆](../Page/西藏记忆.md "wikilink")》，2006年1月，台湾大块文化。Forbidden
    Memory: Tibet During the Cultural Revolution

  -
  - 《[看不見的西藏](../Page/看不見的西藏.md "wikilink")》，2008年1月，台湾大块文化

  -
  -
  -
## 參考文獻

<references />

## 外部链接

  - 唯色的[個人博客](http://woeser.middle-way.net/)/[Twitter](https://twitter.com/degewa)
  - [关于《被绑架的“民族英雄”》的回复](http://www.cdd.cn/homepage/article_show.asp?id=48579)，唯色。
  - [西藏面对的两种帝国主义——透视唯色事件](http://www.zgzc.org/2005/citizenship/00105.htm)，[王力雄](../Page/王力雄.md "wikilink")
  - [访西藏作家唯色
    其人其书其博客](http://www.voanews.com/chinese/archive/2006-07/w2006-07-31-voa1.cfm)，[美国之音](../Page/美国之音.md "wikilink")，2006年7月31日
  - [专访茨仁唯色](https://web.archive.org/web/20061002223704/http://www.rfa.org/mandarin/shenrubaodao/2006/07/31/woser/)，[自由亚洲电台](../Page/自由亚洲电台.md "wikilink")，2006年7月31日
  - [唯色的信仰和中共的无神论](https://web.archive.org/web/20060831095025/http://www.boxun.com/hero/liuxb/241_1.shtml)，[刘晓波](../Page/刘晓波.md "wikilink")
  - [火車來了，鐵龍來了](https://web.archive.org/web/20070927211449/http://www.open.com.hk/2006_8p29.htm)，唯色，2006年7月21日
  - [长诗：西藏的秘密](http://www.peacehall.com/news/gb/pubvp/2005/05/200505080321.shtml)，唯色
  - [唯色：怎能打开我的西藏？](http://www.epochtimes.com/gb/4/12/16/n748958.htm)，茉莉
  - [囊帕拉：雪红雪白](https://web.archive.org/web/20070213081545/http://minzhuzhongguo.org/Article/ShowArticle.asp?ArticleID=339)，唯色

[Category:中華人民共和國詩人](../Category/中華人民共和國詩人.md "wikilink")
[Category:中華人民共和國女性作家](../Category/中華人民共和國女性作家.md "wikilink")
[Category:中華人民共和國持不同政見者](../Category/中華人民共和國持不同政見者.md "wikilink")
[Category:零八宪章签署者](../Category/零八宪章签署者.md "wikilink")
[Category:藏族作家](../Category/藏族作家.md "wikilink")
[Category:藏族學者](../Category/藏族學者.md "wikilink")
[Category:藏學家](../Category/藏學家.md "wikilink")
[Category:西南民族大學校友](../Category/西南民族大學校友.md "wikilink")
[Category:拉萨人](../Category/拉萨人.md "wikilink")
[Category:甘孜人](../Category/甘孜人.md "wikilink")
[Category:独立中文笔会林昭纪念奖得主](../Category/独立中文笔会林昭纪念奖得主.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [Claudia Duque of Colombia, Tsering Woeser, a Tibetan living in
    China, and Vicky Ntetema of Tanzania will Receive 2010 Courage in
    Journalism Awards from the International Women’s Media
    Foundation](http://www.iwmf.org/article.aspx?id=1212&c=press)
    ，IWMF，2010年5月10日
8.
9.
10. 《杀劫》资料补充：当时在西藏也有红红火火的文革运动，但文献和相片方面却少得可怜；2006年1月在台湾出版的《杀劫》一书，藏人作者唯色根据父亲在文革期间拍摄的数百张珍贵照片中，花费六年在西藏访问遍查，并从访者眼中认出不少相中批斗与被批斗的人物详情和去向，并有少量官方资料，包括当时《[西藏日报](../Page/西藏日报.md "wikilink")》的叙述；另外在另一书《[西藏记忆](../Page/西藏记忆.md "wikilink")》中亦有不少文字描述（访谈参予文革者23位），对文革期间对西藏的受害情况较好的补白，惊人地是其中不少受访者是当年的红卫兵或是积极份子，由他们述说西藏文革可能更具说服力，并补充了另一种观点。