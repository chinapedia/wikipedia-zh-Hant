**商州**，[中国古代的](../Page/中国.md "wikilink")[州](../Page/州.md "wikilink")。

[北周](../Page/北周.md "wikilink")[宣政元年](../Page/宣政.md "wikilink")（578年）改[洛州置](../Page/洛州_\(北魏\).md "wikilink")，[治所在](../Page/治所.md "wikilink")[上洛县](../Page/上洛县.md "wikilink")（今[陕西省](../Page/陕西省.md "wikilink")[商洛市](../Page/商洛市.md "wikilink")[商州区](../Page/商州区.md "wikilink")）。[隋朝](../Page/隋朝.md "wikilink")[大业三年](../Page/大业_\(年号\).md "wikilink")（607年）改为[上洛郡](../Page/上洛郡.md "wikilink")。[唐朝](../Page/唐朝.md "wikilink")[武德元年](../Page/武德.md "wikilink")（618年）复为商州。[天宝](../Page/天宝_\(唐朝\).md "wikilink")、[至德时复为上洛郡](../Page/至德_\(唐朝\).md "wikilink")。[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年）复为商州。唐朝时，辖境约当今陕西省[秦岭以南](../Page/秦岭.md "wikilink")，[柞水](../Page/柞水.md "wikilink")、镇安以东，商南以西和[湖北省](../Page/湖北省.md "wikilink")[郧西县](../Page/郧西县.md "wikilink")[上津镇地](../Page/上津镇.md "wikilink")。

[北宋时](../Page/北宋.md "wikilink")，属[陕西路](../Page/陕西路.md "wikilink")，后属[永兴军路](../Page/永兴军路.md "wikilink")。[崇宁时户七万三千一百二十九](../Page/崇宁.md "wikilink")，口一十六万二千五百三十四。贡麝香、枳壳实。下领五县：上洛县、[商洛县](../Page/商洛县.md "wikilink")、[洛南县](../Page/洛南县.md "wikilink")、[丰阳县](../Page/丰阳县.md "wikilink")、[上津县](../Page/上津县.md "wikilink")\[1\]。

[明朝](../Page/明朝.md "wikilink")[洪武年间降为县](../Page/洪武.md "wikilink")，[成化时复升州](../Page/成化.md "wikilink")，同时以商县的层峰驿置[商南县](../Page/商南县.md "wikilink")。下领四县：商南县、[雒南县](../Page/雒南县.md "wikilink")、[山阳县](../Page/山阳县.md "wikilink")、[镇安县](../Page/镇安县.md "wikilink")，属[西安府](../Page/西安府.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正三年](../Page/雍正.md "wikilink")（1725年），升为[商州直隸州](../Page/商州直隸州.md "wikilink")。[辛亥革命后](../Page/辛亥革命.md "wikilink")，于1913年废为[商县](../Page/商县.md "wikilink")。

## 注释

## 参考资料

  - 《[明史](../Page/明史.md "wikilink")·志第十八·地理三》

[Category:北周的州](../Category/北周的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:五代十国的州](../Category/五代十国的州.md "wikilink")
[Category:北宋的州](../Category/北宋的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:明朝的州](../Category/明朝的州.md "wikilink")
[Category:清朝的州](../Category/清朝的州.md "wikilink")
[Category:陕西的州](../Category/陕西的州.md "wikilink")
[Category:湖北的州](../Category/湖北的州.md "wikilink")
[Category:商洛行政区划史](../Category/商洛行政区划史.md "wikilink")
[Category:十堰行政区划史](../Category/十堰行政区划史.md "wikilink")
[Category:578年建立的行政区划](../Category/578年建立的行政区划.md "wikilink")
[Category:1725年废除的行政区划](../Category/1725年废除的行政区划.md "wikilink")

1.  《[宋史](../Page/宋史.md "wikilink")·志第四十·地理三》