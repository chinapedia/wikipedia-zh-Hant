<noinclude></noinclude>

<table>
<caption>商朝（约前17世纪─约前11世纪）</caption>
<thead>
<tr class="header">
<th><p>先商时期（约前21世纪─约前17世纪）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/谥号.md" title="wikilink">谥号</a><br />
<small>*殷墟甲骨<br />
*传世文献</small></p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>商</p></td>
</tr>
<tr class="even">
<td><p>—</p>
<hr />
<p>玄王、素王？</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p>
<hr />
<p>武王</p>
<hr />
<p>文武帝乙</p></td>
</tr>
<tr class="odd">
<td><p>亳</p></td>
</tr>
<tr class="even">
<td><p>早商时期（约前17世纪─约前14世纪）</p></td>
</tr>
<tr class="odd">
<td><p>—</p>
<hr />
<p>武王</p>
<hr />
<p>文武帝乙</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p>
<hr />
<p>明王</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p>
<hr />
<p>明王</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>奄</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>晚商时期（约前14世纪─约前11世纪）</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>文武帝<sup><a href="../Page/#簠.md" title="wikilink">[簠 143</a>]<a href="../Page/#续.md" title="wikilink">[续 2.71</a>]<a href="../Page/#前.md" title="wikilink">[前 1.22.2</a>]<a href="../Page/#前.md" title="wikilink">[前 4.17.4</a>]<a href="../Page/#粹.md" title="wikilink">[粹 3621</a>]</sup></p>
<hr />
<p>—</p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>—</p>
<hr />
<p>纣王</p></td>
</tr>
<tr class="odd">
<td><p>朝歌</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>【注】结合古今本《<a href="../Page/竹書紀年.md" title="wikilink">竹書紀年</a>》体系推算，由于年代久远，记录匮乏，各家推算不一，本表年代仅供参考。</p></td>
</tr>
</tbody>
</table>


{|class="wikitable" style="margin:auto;" |+ 附：有待考证的先公 |- \!
style="background: \#EFEFEF;" | 庙号
<small>\*殷墟甲骨 \! style="background: \#EFEFEF;" | 传世文献见名 \!
style="background: \#EFEFEF;" | 出土甲骨见名 \! style="background: \#EFEFEF;"
| 配偶名 |- | align=center | 高且河？（高祖河？）<sup>[\[合集
32028](../Page/#合集.md "wikilink")\]</sup>、河高且？（河高祖？）、河□高且？（河□高祖？）、河宗？<sup>[\[合集
13532](../Page/#合集.md "wikilink")\]</sup> | align=center | 河伯氏？ |
[河](../Page/河_\(商族先公\).md "wikilink") | 河母、河妾？<sup>[\[后
上6.3](../Page/#后.md "wikilink")\]</sup> |- | align=center |
岳宗？<sup>[\[合集 30298](../Page/#合集.md "wikilink")\]</sup> |
align=center | （未见） | [岳](../Page/岳_\(商族先公\).md "wikilink") |
align=center | （不详） |- | align=center | — | align=center | （未见） | 土 |
align=center | （不详） |- | align=center | — | align=center | （未见） |
[炘](../Page/炘.md "wikilink") | align=center | （不详） |- | align=center |
— | align=center | （未见） | [𠣬](../Page/𠣬.md "wikilink") | align=center |
（不详） |- | align=center | — | align=center | （未见） |
[娥](../Page/娥.md "wikilink") | align=center | （不详） |- | align=center
| — | align=center | [昏](../Page/昏_\(商朝\).md "wikilink")
昏微 | align=center | （未见） | align=center | （不详） |}<noinclude>
__NOTOC__

## 注释

## 参考资料

## 参考书目

## 参阅

  - [商朝](../Page/商朝.md "wikilink")
  - [商朝君主世系图](../Page/商朝君主世系图.md "wikilink")

{{-}}    [商朝君主](../Page/分类:商朝君主.md "wikilink")
[商](../Page/分类:中國君主列表.md "wikilink")

</noinclude>

[Category:先秦人列表](../Category/先秦人列表.md "wikilink")