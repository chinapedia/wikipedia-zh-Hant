[商朝的国家政体是以](../Page/商朝.md "wikilink")[商王为核心的家族制度](../Page/商朝君主列表.md "wikilink")，辅之以其他联盟部族、臣服部族首领等。这个政体的中坚分子是商王的亲族。在这个时期，国家事务与商王的私人事务是难以严格区分的。商朝的官僚体系大致可以分为所谓的[内廷官和](../Page/内廷官.md "wikilink")[外廷官](../Page/外廷官.md "wikilink")，而这些官员都是中央官员〈处理商王私人领地事务的[宰也可以视为地方官员](../Page/宰.md "wikilink")〉。商朝施行[分封制度](../Page/分封制度.md "wikilink")，地方由[诸侯统治](../Page/诸侯.md "wikilink")，诸侯内部的官吏设置等，目前发现的[甲骨文中还没有这方面的记录](../Page/甲骨文.md "wikilink")。

## 中央

### 内廷官

商王的内廷官，主要有[宰](../Page/宰.md "wikilink")、[臣两类](../Page/臣.md "wikilink")。这些官员主要处理商王的私人事务，包括其私人领地、以及王官内部的管理等。[宰的地位非常高](../Page/宰.md "wikilink")，特定的情况下作为商王的代表直接处理政务。《[史记](../Page/史记.md "wikilink")·殷本纪》记载，殷高宗[武丁](../Page/武丁.md "wikilink")，“三年不言，政事决于[冢宰](../Page/冢宰.md "wikilink")”。[臣的种类很多](../Page/臣.md "wikilink")，有负责农耕的、有负责内廷供奉的、有负责祭祀的、有负责警卫的、也有管理商王私人军队的。臣的级别也很不相同，有些是商王的亲族、有些是来自臣服的部族、甚至有些属于奴隶的身份。不过，作为臣的[奴隶很多成为显赫的大臣](../Page/奴隶.md "wikilink")，如[商汤时的](../Page/商汤.md "wikilink")[伊尹](../Page/伊尹.md "wikilink")，[武丁时的](../Page/武丁.md "wikilink")[傅说](../Page/傅说.md "wikilink")，都从奴隶晋升为[宰相](../Page/宰相.md "wikilink")。

### 外廷官

外廷官即处理国家事务的官员，以[尹为长](../Page/尹.md "wikilink")，另有[卜](../Page/卜.md "wikilink")，[作册](../Page/作册.md "wikilink")，[亚服等各种名目](../Page/亚服.md "wikilink")。尹相当于后世的宰相，其中最著名的是[伊尹](../Page/伊尹.md "wikilink")。伊尹，姓伊名挚，尹是官名。伊挚曾“为[有莘氏](../Page/有莘氏.md "wikilink")[媵臣](../Page/媵臣.md "wikilink")〈陪嫁的奴隶〉”，得到[商汤的赏识](../Page/商汤.md "wikilink")，“汤举任以国政”。《[史记](../Page/史记.md "wikilink")·殷本纪》记载，商汤死后，继承王位的[外丙](../Page/外丙.md "wikilink")、[仲壬相继死去](../Page/仲壬.md "wikilink")。伊尹立汤的[嫡长孙](../Page/嫡长孙.md "wikilink")[太甲为王](../Page/太甲.md "wikilink")。太甲即位后，违背商汤所立的法度，暴虐无道，于是伊尹把他放逐于[桐](../Page/桐.md "wikilink")〈今[河南](../Page/河南.md "wikilink")[虞城东北](../Page/虞城.md "wikilink")〉，由伊尹代王施政。三年以后，太甲认识了自己的过错并有所悔改，伊尹才把他迎接回来并“授之政”。伊尹的儿子[伊陟](../Page/伊陟.md "wikilink")，也身居高官，“帝[太戊立](../Page/太戊.md "wikilink")[伊陟为相](../Page/伊陟.md "wikilink")”。

[卜](../Page/卜.md "wikilink")、[作册都是宗教官员](../Page/作册.md "wikilink")，相当于[罗马时代的](../Page/罗马.md "wikilink")[祭司](../Page/祭司.md "wikilink")，但是地位远不如罗马祭司高。其中卜作为大祭司，权力比较大，作册的职务与后世的[史相当](../Page/史.md "wikilink")，是负责记录的官吏。[亚服是武官](../Page/亚服.md "wikilink")，统帅军队，有[亚](../Page/亚.md "wikilink")，[多亚](../Page/多亚.md "wikilink")，[大亚之分](../Page/大亚.md "wikilink")，但是具体职务与等级区别不详。商朝的武官还有[射](../Page/射.md "wikilink")〈统帅弓箭手〉、[犬](../Page/犬_\(商朝官名\).md "wikilink")〈负责商王的田猎〉、[戍](../Page/戍.md "wikilink")〈要地驻屯军长官〉等其他称号。

## 地方諸侯

商朝的地方诸侯有[侯](../Page/侯.md "wikilink")、[伯](../Page/伯.md "wikilink")、[男](../Page/男.md "wikilink")、[甸不同的称号](../Page/甸.md "wikilink")。侯、伯是较大的诸侯，男、甸较小或较远。与后世不同的是，商朝保留了前代母系社会的一些特点，女性也可以被封为诸侯并统帅军队。甲骨文中就有“辛巳卜，贞登[妇好三千](../Page/妇好.md "wikilink")，登旅万，呼伐□”说在[辛巳这一天卜问](../Page/辛巳.md "wikilink")，以命令妇好这个人统帅一万多人的军队去讨伐某某地方。这里的妇好，就是商王[武丁的妻子](../Page/武丁.md "wikilink")。

[Category:商朝官制](../Category/商朝官制.md "wikilink")