是依據台灣作家[瓊瑤所撰寫的小說](../Page/瓊瑤.md "wikilink")《[啞妻](../Page/六個夢.md "wikilink")》為基礎改編的電影作品。

劇中由於女主角為聾啞人士，因此導演也運用大量的內心戲獨白做處理，使得影片洋溢著濃厚的文藝氣息，而人物的性格衝突和情感的細緻描繪，也拓寬了原著的深度和格局。

## 劇情簡介

**柳靜言**與其啞妻**方依依**兩人感情甚深，但所生的女兒**雪兒**卻和自己一樣是聾啞人士，當再懷第二胎時，柳靜言強迫她墮胎，但方依依極力抗拒，於是柳靜言憤而拋妻出走。十年後，方依依病危，其女雪兒寫信求父親柳靜言回家，當他趕回家時，啞妻已撒手人寰。

## 人物角色

| 角色名稱   | 演員                               | 備註 |
| ------ | -------------------------------- | -- |
| 方依依    | [王莫愁](../Page/王莫愁.md "wikilink") |    |
| 柳靜言    | [柯俊雄](../Page/柯俊雄.md "wikilink") |    |
|        | [林璣](../Page/林璣.md "wikilink")   |    |
|        | [蔡慧華](../Page/蔡慧華.md "wikilink") |    |
| 丁珮     | [唐美麗](../Page/唐美麗.md "wikilink") |    |
|        | [潘琪](../Page/潘琪.md "wikilink")   |    |
| 雪兒（童年） | [伊娜](../Page/伊娜.md "wikilink")   |    |
|        |                                  |    |

## 獎項

### 榮獲

  - [金馬獎](../Page/金馬獎.md "wikilink")

:\*「優等劇情片獎」

:\*「最佳音樂獎」：[左宏元](../Page/左宏元.md "wikilink")

:\*「最佳錄音獎」：[洪瑞庭](../Page/洪瑞庭.md "wikilink")

:\*「特別演技獎」：[王莫愁](../Page/王莫愁.md "wikilink")

  - [亞太影展](../Page/亞太影展.md "wikilink")

:\*「最佳編劇獎」：[劉藝](../Page/劉藝_\(導演\).md "wikilink")

:\*「最佳攝影獎」：[賴成英](../Page/賴成英.md "wikilink")

:\*「特別演技獎」：[王莫愁](../Page/王莫愁.md "wikilink")

## 相關條目

  - [六個夢](../Page/六個夢.md "wikilink")

## 參考資料

  - [瓊瑤作品集─啞妻](http://www.millionbook.net/yq/q/qiongyao/lgm/002.htm)
  - [中文電影資料庫─啞女情深](http://www.dianying.com/ft/title/ynq1965)
  - [第四屆金馬獎得獎名單](https://web.archive.org/web/20070926231114/http://student.wtuc.edu.tw/~1093211020/12.htm)

## 外部連結

  - {{@movies|fUcmb4042162}}

  -
  -
  -
  -
  -
  -
  - [啞女情深（The Silent
    Wife）](https://web.archive.org/web/20041121215332/http://cinema.nccu.edu.tw/cinemaV2/film_show.htm?SID=2162)，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉

[5](../Category/1960年代台灣電影作品.md "wikilink")
[Category:台灣劇情片](../Category/台灣劇情片.md "wikilink")
[Category:瓊瑤小說改編電影](../Category/瓊瑤小說改編電影.md "wikilink")
[Category:身心障礙題材作品](../Category/身心障礙題材作品.md "wikilink")
[Category:李行電影](../Category/李行電影.md "wikilink")
[Category:中影公司電影](../Category/中影公司電影.md "wikilink")