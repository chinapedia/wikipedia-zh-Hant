[Entrance_and_exit_A_of_Kai_Tak_Station_under_construction_in_October_2017.jpg](https://zh.wikipedia.org/wiki/File:Entrance_and_exit_A_of_Kai_Tak_Station_under_construction_in_October_2017.jpg "fig:Entrance_and_exit_A_of_Kai_Tak_Station_under_construction_in_October_2017.jpg")
[Entrance_and_exit_B_of_Kai_Tak_Station_under_construction_in_October_2017.jpg](https://zh.wikipedia.org/wiki/File:Entrance_and_exit_B_of_Kai_Tak_Station_under_construction_in_October_2017.jpg "fig:Entrance_and_exit_B_of_Kai_Tak_Station_under_construction_in_October_2017.jpg")
[Entrance_and_exit_D_of_Kai_Tak_Station_under_construction_in_October_2017.jpg](https://zh.wikipedia.org/wiki/File:Entrance_and_exit_D_of_Kai_Tak_Station_under_construction_in_October_2017.jpg "fig:Entrance_and_exit_D_of_Kai_Tak_Station_under_construction_in_October_2017.jpg")
**啟德站**（）是[香港](../Page/香港.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[沙田至中環綫一個已完成土木工程並有待通車啟用的車站](../Page/沙田至中環綫.md "wikilink")，車站位於[啟德新發展區西北部地底](../Page/啟德新發展區.md "wikilink")，即[協調道及](../Page/協調道.md "wikilink")[啟德明渠旁邊](../Page/啟德明渠.md "wikilink")\[1\]，為一座深兩層的地底車站。2015年7月16日舉行平頂儀式，並於2018年11月尾完工交付，並預計於2019年11月啟用\[2\]。

由於沙中綫將原訂設於[大磡村舊址的鑽石山列車停放處改設於](../Page/大磡村.md "wikilink")[紅磡貨場](../Page/紅磡貨場.md "wikilink")，故港鐵將於本站加設[側線供停放列車](../Page/側線.md "wikilink")。\[3\]另外，根據環評報告附圖所示，本站以北將有一條[渡線連接兩條軌道](../Page/渡線.md "wikilink")\[4\]。

## 結構

### 樓層

地下一層為車站大堂，地下二層為一個島式月台及側線\[5\]。車站將會設有3個出入口，連接區內基礎設施。

<table>
<tbody>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>地面</p></td>
<td><p>出口</p></td>
</tr>
<tr class="even">
<td><p><strong>C</strong></p></td>
<td><p>大堂</p></td>
<td><p>客務中心、車站商店</p></td>
</tr>
<tr class="odd">
<td><p><strong>P</strong></p></td>
<td><p>屯馬綫側線</p></td>
<td><p>供列車停泊，不對外開放[6]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>屯馬綫往<a href="../Page/屯門站_(西鐵綫).md" title="wikilink">屯門</a><small>（<a href="../Page/宋皇臺站.md" title="wikilink">宋皇臺</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>島式月台，開左邊門</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 出入口

啟德站將設有3個出入口\[7\]\[8\]，並預留1個出入口通往車站上蓋及地下街\[9\]：

<table>
<thead>
<tr class="header">
<th><p>出口編號</p></th>
<th><p>預計可前往目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>車站東北面（<a href="../Page/啟晴邨.md" title="wikilink">啟晴邨</a>、<a href="../Page/德朗邨.md" title="wikilink">德朗邨</a>、<a href="../Page/晴朗商場.md" title="wikilink">晴朗商場</a>、<a href="../Page/啟朗苑.md" title="wikilink">啟朗苑</a>、<a href="../Page/煥然壹居.md" title="wikilink">煥然壹居</a>、<a href="../Page/啟德一號.md" title="wikilink">啟德一號</a>、<a href="../Page/啟德大道公園.md" title="wikilink">啟德大道公園</a>、學校新校舍（<a href="../Page/保良局何壽南小學.md" title="wikilink">保良局何壽南小學</a>、<a href="../Page/聖公會聖十架小學.md" title="wikilink">聖公會聖十架小學</a>、<a href="../Page/文理書院（九龍）.md" title="wikilink">文理書院（九龍）</a>）、<a href="../Page/麗晶花園.md" title="wikilink">麗晶花園</a>）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>車站西北面（<a href="../Page/太子道東.md" title="wikilink">太子道東</a>、<a href="../Page/采頤花園.md" title="wikilink">采頤花園</a>、<a href="../Page/景泰苑.md" title="wikilink">景泰苑</a>、<a href="../Page/友邦九龍金融中心.md" title="wikilink">友邦九龍金融中心</a>、<a href="../Page/東啟德遊樂場.md" title="wikilink">東啟德遊樂場</a>、<a href="../Page/彩虹邨.md" title="wikilink">彩虹邨</a>）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>車站西南面（<a href="../Page/啟德坊.md" title="wikilink">啟德坊</a>、<a href="../Page/都會公園_(香港).md" title="wikilink">都會公園</a>、<a href="../Page/啟德體育園區.md" title="wikilink">啟德體育園區</a>、<a href="../Page/工業貿易大樓.md" title="wikilink">工業貿易大樓</a>、<a href="../Page/譽·港灣.md" title="wikilink">譽·港灣</a>、<a href="../Page/Mikiki.md" title="wikilink">Mikiki</a>、<a href="../Page/新蒲崗.md" title="wikilink">新蒲崗</a>、<a href="../Page/啟德郵輪碼頭.md" title="wikilink">啟德郵輪碼頭</a>）</p></td>
</tr>
<tr class="even">
<td><p><br />
（預留）</p></td>
<td><p>車站廣場、地下購物街、<a href="../Page/國際展貿中心.md" title="wikilink">國際展貿中心</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 接駁交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [觀塘綫](../Page/觀塘綫.md "wikilink")：[彩虹站](../Page/彩虹站.md "wikilink")、[鑽石山站](../Page/鑽石山站.md "wikilink")、[黃大仙站](../Page/黃大仙站.md "wikilink")
  - [城巴20線](../Page/城巴20線.md "wikilink")、[城巴22線](../Page/城巴22線.md "wikilink")、[過海隧道巴士608線等多線巴士及小巴](../Page/過海隧道巴士608線.md "wikilink")

## 工程圖片

Construction site of Kai Tak Station.JPG|2013年8月 Kai Tak Station under
construction in March 2015.jpg|2015年3月 Kai Tak Station under
construction in July 2015.jpg|2015年7月 Kai Tak Station under construction
in February 2017.jpg|2017年2月

## 未來發展

### 環保連接系統

政府建議於本站設置[環保連接系統與](../Page/環保連接系統.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[屯馬綫之轉乘站](../Page/屯馬綫.md "wikilink")，有關之鐵路預計於2023年通車。

## 參考資料

[Category:啟德發展計劃](../Category/啟德發展計劃.md "wikilink")
[Category:建在填海/填塘地的港鐵車站](../Category/建在填海/填塘地的港鐵車站.md "wikilink")
[Category:沙田至中環綫](../Category/沙田至中環綫.md "wikilink")
[Category:2019年啟用的鐵路車站](../Category/2019年啟用的鐵路車站.md "wikilink")
[Category:九龍城區鐵路車站](../Category/九龍城區鐵路車站.md "wikilink")

1.  [九龍城區區議會沙中綫區議會諮詢文件](http://www.mtr.com.hk/chi/projects/images/KCDC%20Paper_final.pdf)，[港鐵公司](../Page/港鐵公司.md "wikilink")，2009年6月1日

2.
3.

4.
5.
6.

7.

8.

9.