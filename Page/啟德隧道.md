[Kai_Tak_Tunnel_Kowloon_Bay_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Kai_Tak_Tunnel_Kowloon_Bay_Entrance.jpg "fig:Kai_Tak_Tunnel_Kowloon_Bay_Entrance.jpg")
**啟德隧道**（，簡稱**啟隧**、**機隧**，因前稱為**機場隧道**，故[啟德機場停用後亦被稱為](../Page/啟德機場.md "wikilink")**舊機場隧道**）是[香港一條雙程雙線分隔管道行車](../Page/香港.md "wikilink")[隧道](../Page/隧道.md "wikilink")，位於前啟德機場[跑道](../Page/跑道.md "wikilink")（今[啟德體育園工地](../Page/啟德體育園.md "wikilink")，附帶地役權）下方，全長1.26公里，車速限制為50公里，連接[馬頭角及](../Page/馬頭角.md "wikilink")[九龍灣](../Page/九龍灣.md "wikilink")。啟德隧道的一大特色，就是東行管道內有支路通往[宋皇臺道](../Page/宋皇臺道.md "wikilink")。[隧道交通督導員獲授權在隧道範圍內執行香港法例](../Page/隧道交通督導員.md "wikilink")368章《行車隧道（政府）條例》。

## 歷史

隧道於1976年完工\[1\]，舊稱**機場隧道**（簡稱**機隧**），是香港第一條免費行車隧道；由於隧道上面是機場跑道，在施工上有困難，因此要到1982年才開放使用。其工程為全球首起在營運機場地下施工興建道路的工程。

## 近況

2006年5月4日機場隧道更名為啟德隧道，以紀念前啟德機場及避免市民將之與現今位於[大嶼山](../Page/大嶼山.md "wikilink")[赤鱲角的](../Page/赤鱲角.md "wikilink")[香港國際機場](../Page/香港國際機場.md "wikilink")\[2\]混淆。在機場搬遷後至隧道名稱更名前，的士台和電台的交通報導將之稱為「舊機隧」。

啟德隧道是連接[東九龍和](../Page/東九龍.md "wikilink")[西九龍的其中一條要道](../Page/西九龍.md "wikilink")：屬[5號幹線一部份](../Page/香港5號幹線.md "wikilink")（另一條[東西九龍要道為](../Page/九龍.md "wikilink")[7號幹線](../Page/香港7號幹線.md "wikilink")），因此交通流量非常大，在2012年平均每日有54,825架車輛使用\[3\]。

2012年4月23日越運亨（香港）有限公司取得啟德隧道為期6年的營運管理合約，於2012年8月1日起正式生效。\[4\]

## 隧道設計

啟德隧道的特色就是東行管道內有分叉路通往[宋皇臺道](../Page/宋皇臺道.md "wikilink")。雖然在[土瓜灣](../Page/土瓜灣.md "wikilink")[九龍城道同興花園對出有一條支路可通往隧道](../Page/九龍城道.md "wikilink")，但該路段只供緊急車輛行駛。因此車輛由土瓜灣往[觀塘方向](../Page/觀塘.md "wikilink")，只能取道[太子道東或](../Page/太子道東.md "wikilink")[承啟道前往](../Page/承啟道.md "wikilink")。另外在西行隧道出口亦有分叉路通往新山道，有不少巴士路線用此分叉路。另一方面隧道中段有一隻接近八十度的急彎使隧道由東北偏北／西南偏南走向轉為正東／正西走向。這隧道中有支路進出及急彎的設計在香港很罕見。

## 出口

\! colspan="4" style="border-bottom: 5px solid \#E28D0E;" |
[<File:AB-Tunnel.svg>](https://zh.wikipedia.org/wiki/File:AB-Tunnel.svg "fig:File:AB-Tunnel.svg")
[啟德隧道](../Page/啟德隧道.md "wikilink") |- style="text-align:center;" \!
colspan=2 | 西行方向 \!\! colspan=2 | 東行方向 |- style="text-align:center;" |
width=30 | 編號 || width=300 | 前往地區 || width=300 | 前往地區 || width=30 | 編號
|- style="text-align:center;" \! colspan=4 |
[啟福道](../Page/啟福道.md "wikilink") |- | || ||
[九龍城](../Page/九龍城.md "wikilink")（出口在啟德隧道內） || 2A |- \!
colspan=4 |  [東九龍走廊](../Page/東九龍走廊.md "wikilink")

## 途經隧道的公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表** (往土瓜灣方向途經)

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

</div>

</div>

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表** (往九龍灣方向途經)

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

</div>

</div>

## 軼事

2010年4月11日下午1時許，一部[靈車由](../Page/靈車.md "wikilink")[觀塘繞道向](../Page/觀塘繞道.md "wikilink")[尖沙咀行駛](../Page/尖沙咀.md "wikilink")，當駛至啟德隧道近[土瓜灣出口](../Page/土瓜灣.md "wikilink")，懷疑「入錯線」，由快線切到慢線，引起尾隨[客貨車司機不滿](../Page/客貨車.md "wikilink")，兩車行駛50米後，靈車突然打[死火燈停下](../Page/死火燈.md "wikilink")，而客貨車也將車駛到路中央。

當時客貨車上兩男子跳下，其中年約20歲的跟車青年打開後座車門，取出兩支類似木棍物體，靈車亦有五男一女走出，一男子也拿出懷疑抬[棺材的兩米長木棍](../Page/棺材.md "wikilink")，站在一旁戒備，雙方似乎一觸即發，隨時大打出手。

經過約一分半鐘的理論和對峙，靈車一方的六名男女首先返回車上，令事件未有鬧大，但雙方開車仍未駛出隧道口，涉嫌堵塞交通的靈車未有被截停，而尾隨的客貨車被高級[交通督導員陸偉明先生截停](../Page/交通督導員.md "wikilink")，司機接受初步調查。有尾隨司機以Car
Camera（汽車鏡頭）拍攝該事件的過程，早前被放上互聯網在片段曝光後，警方表示關注事件及正進行調查。根據《行車隧道（政府）條例》，在隧道內違例切[雙白線](../Page/雙白線.md "wikilink")，[隧道交通督導員可檢控不遵守道路標記司機](../Page/隧道交通督導員.md "wikilink")/人士；根據交通法例，在隧道內違例切雙白線，可以被控以不小心駕駛並罰款450港元和扣3至5分。

## 圖片集

<File:Kai> Tak Tunnel01.jpg|啟德隧道[馬頭角入口](../Page/馬頭角.md "wikilink")
[File:KaiTakTunnelEntrance.jpg|啟德隧道](File:KaiTakTunnelEntrance.jpg%7C啟德隧道)[九龍灣入口](../Page/九龍灣.md "wikilink")（現時名牌）
<File:Tk> tunnel
klnb.JPG|啟德隧道[九龍灣入口](../Page/九龍灣.md "wikilink")（名牌仍為機場隧道）
<File:Kai> Tak Tunnel Ma Tau Kok Entrance 2018.jpg|馬頭角入口（2018）

## 參考資料

## 相關條目

  - [承啟道](../Page/承啟道.md "wikilink")（2010年代開通而提供同樣功能的地面路線）
  - [復興北路車行地下道](../Page/復興北路車行地下道.md "wikilink")（[台北松山機場下方](../Page/台北松山機場.md "wikilink")）

[Category:香港公路隧道](../Category/香港公路隧道.md "wikilink")
[Category:九龍灣](../Category/九龍灣.md "wikilink")
[Category:土瓜灣](../Category/土瓜灣.md "wikilink")

1.  [問啟德（舊機場）隧道](http://www.hkitalk.net/HKiTalk2/viewthread.php?tid=97440&page=1)
2.  [機場隧道易名為啟德隧道](http://www.info.gov.hk/gia/general/200603/02/P200603020139.htm)
3.  [運輸署：隧道汽車流量、青嶼幹線汽車流量及汽車渡輪服務](http://www.td.gov.hk/filemanager/en/content_4609/table31s.pdf)
4.  [越秀發展成功取得香港獅子山隧道及啟德隧道營運管理合約](http://www.yuexiu.com/newscenter/xsqy/qtqy/16rpb4b5n549k.xhtml)