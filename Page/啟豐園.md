[Richland_Garden_(revised).jpg](https://zh.wikipedia.org/wiki/File:Richland_Garden_\(revised\).jpg "fig:Richland_Garden_(revised).jpg")
[Richland_Garden_shopping_mall.JPG](https://zh.wikipedia.org/wiki/File:Richland_Garden_shopping_mall.JPG "fig:Richland_Garden_shopping_mall.JPG")
**啟豐園**（）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[湖翠路](../Page/湖翠路.md "wikilink")138號的私人屋苑，屬[南豐發展的物業](../Page/南豐發展.md "wikilink")。屋苑1992年入伙，共4座樓宇，共896個單位，另設有兩層商場及一層停車場，商場以小商户為主。部分單位擁有全海景，景觀包括[龍珠島](../Page/龍珠島.md "wikilink")、[大嶼山](../Page/大嶼山.md "wikilink")[東涌及](../Page/東涌_\(香港\).md "wikilink")[香港國際機場等](../Page/香港國際機場.md "wikilink")。

## 歷史

本屋苑興建前屬[蝴蝶邨巴士總站](../Page/蝴蝶邨.md "wikilink")，屯門碼頭巴士總站於1988年啟用後興建本屋苑，於1992年入伙。

## 住宅

與鄰近[私人屋苑](../Page/私人屋苑.md "wikilink")[邁亞美海灣的樓宇設計不同](../Page/邁亞美海灣.md "wikilink")，啟豐園採用傳統井字式設計，各單位的座向並不相同，加上座與座之間的距離不是太遠，故此不是所有單位都有全海景。

所有座宇的A和B單位都有全海景。第一座的C和D單位擁有向西南全海景；第四座的G和H單位則擁有向東南全海景。以上兩款被稱為單邊單位。

因為座向的明顯分別，所以各單位的呎價差別頗大。一般來說，同一款間格的單邊單位比非單邊的呎價約貴20%。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

屯門碼頭：設有來往東涌、沙螺灣及大澳的渡輪服務。

### 輕鐵服務

|                                         |                                      |   |                                         |                                                                                                                                      |
| --------------------------------------- | ------------------------------------ | - | --------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| [507](../Page/香港輕鐵507線.md "wikilink")   | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [田景站](../Page/田景站.md "wikilink")        | （經市中心站）                                                                                                                              |
| [610](../Page/香港輕鐵610線.md "wikilink")   | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [元朗站](../Page/元朗站_\(輕鐵\).md "wikilink") | （經[大興](../Page/大興.md "wikilink")、[澤豐](../Page/澤豐站.md "wikilink")）                                                                    |
| [614](../Page/香港輕鐵614線.md "wikilink")   | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [元朗站](../Page/元朗站_\(輕鐵\).md "wikilink") | （經[景峰](../Page/景峰站.md "wikilink")、[杯渡](../Page/杯渡站.md "wikilink")、[市中心](../Page/市中心站.md "wikilink")、[安定](../Page/安定站.md "wikilink")） |
| [614P](../Page/香港輕鐵614P線.md "wikilink") | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [兆康站](../Page/兆康站_\(輕鐵\).md "wikilink") | （經[景峰](../Page/景峰站.md "wikilink")、[杯渡](../Page/杯渡站.md "wikilink")、[市中心](../Page/市中心站.md "wikilink")、[安定](../Page/安定站.md "wikilink")） |
| [615](../Page/香港輕鐵615線.md "wikilink")   | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [元朗站](../Page/元朗站_\(輕鐵\).md "wikilink") | （經[良景](../Page/良景站.md "wikilink")、[建生](../Page/建生站.md "wikilink")）                                                                   |
| [615P](../Page/香港輕鐵615P線.md "wikilink") | [屯門碼頭站](../Page/屯門碼頭站.md "wikilink") | ↔ | [兆康站](../Page/兆康站_\(輕鐵\).md "wikilink") |                                                                                                                                      |
|                                         |                                      |   |                                         |                                                                                                                                      |

</div>

</div>

## 屋苑周邊

  - [屯門碼頭站](../Page/屯門碼頭站.md "wikilink")
  - [屯門碼頭](../Page/屯門碼頭.md "wikilink")
  - [湖景邨](../Page/湖景邨.md "wikilink")
  - [蝴蝶邨](../Page/蝴蝶邨.md "wikilink")
  - [海翠花園](../Page/海翠花園.md "wikilink")
  - [美樂花園](../Page/美樂花園.md "wikilink")

## 參見

  - [啟豐園屋苑資料 (資料由祥益地產提供)](http://www.manyw.com/info/?RICHLANDGARDEN)
  - [佳定集團-啟豐園](http://www.savillsguardian.com.hk/cn/customer/property/homepage.asp?propertyID=200)
  - [WikiMapia: Richland
    Garden](http://wikimapia.org/#lat=22.372916&lon=113.964508&z=17&l=0&m=a&v=2&show=/48236/)
  - [啟豐園成交紀錄－美聯集團](http://proptx.midland.com.hk/propTx/index.jsp?lang=chi&estateId=E00428)

[Category:屯門區私人屋苑](../Category/屯門區私人屋苑.md "wikilink")
[Category:南豐發展物業](../Category/南豐發展物業.md "wikilink")
[Category:屯門區](../Category/屯門區.md "wikilink")
[Category:屯門碼頭區](../Category/屯門碼頭區.md "wikilink")
[Category:蝴蝶灣](../Category/蝴蝶灣.md "wikilink")