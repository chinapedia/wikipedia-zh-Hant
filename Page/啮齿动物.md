**嚙齒動物**也称作**啮形大目**（[学名](../Page/学名.md "wikilink")：），是咬食性[哺乳动物的总称](../Page/哺乳动物.md "wikilink")，包含了[嚙齒目和](../Page/嚙齒目.md "wikilink")[兔形目等](../Page/兔形目.md "wikilink")[演化支](../Page/演化支.md "wikilink")。其中嚙齒目的代表动物有[松鼠](../Page/松鼠.md "wikilink")、[河狸](../Page/河狸.md "wikilink")、[鼠](../Page/鼠.md "wikilink")、[豪猪等](../Page/豪猪.md "wikilink")，兔形目的代表动物有[家兔](../Page/家兔.md "wikilink")、[野兔和](../Page/野兔.md "wikilink")[鼠兔等](../Page/鼠兔.md "wikilink")。\[1\]

## 物种

[缩略图](https://zh.wikipedia.org/wiki/File:Mammalia-percent-schinese.png "fig:缩略图")
對嚙齒動物是一個[單系群的假定](../Page/單系群.md "wikilink")，長久以來都存在著很大的爭議，儘管最近的形態學研究強烈地支持嚙齒動物是單系群的\[2\]\[3\]。新的[化石發現填補了嚙齒目和兔形目之間的裂縫](../Page/化石.md "wikilink")\[4\]\[5\]。[細胞核DNA的資料支持嚙齒動物是](../Page/細胞核.md "wikilink")[靈長動物的旁系群](../Page/靈長動物.md "wikilink")，且兩者組成[靈長總目](../Page/靈長總目.md "wikilink")\[6\]\[7\]，但有些從細胞核和[粒線體DNA得出的基因資料卻對此論點較少支持](../Page/粒線體DNA.md "wikilink")\[8\]。一份對[逆轉位子存在](../Page/逆轉位子.md "wikilink")/消失的研究則無疑地支持了嚙齒動物的假定\[9\]。根據分子學的研究成果，嚙齒動物在靈長總目內的亲缘關係可能是：

世界上啮齿动物的种类或个体数量可能比其他所有哺乳动物加起来还要多，啮齿动物包括：[海狸](../Page/海狸.md "wikilink")、[花栗鼠](../Page/花栗鼠.md "wikilink")、[老鼠](../Page/老鼠.md "wikilink")、[豪猪以及](../Page/豪猪.md "wikilink")[松鼠等](../Page/松鼠.md "wikilink")\[10\]。[水豚是世界上最大的啮齿类生物](../Page/水豚.md "wikilink")，原產於[南美洲的稀樹草原和叢林中](../Page/南美洲.md "wikilink")\[11\]\[12\]。

## 特征

[兔形目与](../Page/兔形目.md "wikilink")[啮齿目一样都有可以不断生长的](../Page/啮齿目.md "wikilink")[门齿](../Page/门齿.md "wikilink")，没有[犬齿](../Page/犬齿.md "wikilink")，其生活习性也和典型的啮齿目比较相似\[13\]。不同的是[囓齒目上下顎各只有一對門齒](../Page/囓齒目.md "wikilink")，而[兔形目则有](../Page/兔形目.md "wikilink")2對上門齒\[14\]。

## 参见

  - [靈長總目](../Page/靈長總目.md "wikilink")
  - [齧齒目](../Page/齧齒目.md "wikilink")
  - [兔形目](../Page/兔形目.md "wikilink")

## 參考資料

## 外部链接

汪诚信.《[啮齿动物名称的改变与跟进](http://www.bmsw.net.cn/UserFiles/File/%E5%95%AE%E9%BD%BF%E5%8A%A8%E7%89%A9%E5%90%8D%E7%A7%B0%E7%9A%84%E6%94%B9%E5%8F%98%E4%B8%8E%E8%B7%9F%E8%BF%9B.pdf)》《中国媒介生物学及控制杂志》2008年8月

[Category:靈長總目](../Category/靈長總目.md "wikilink")
[Category:齧齒動物](../Category/齧齒動物.md "wikilink")

1.

2.  Meng, J., and A.R. Wyss, 2001. The morphology of *Tribosphenomys*
    (Rodentiaformes, Mammalia): phylogenetic implications for basal
    Glires. *Journal of Mammalian Evolution* 8(1):1-71.

3.  \*Meng J., Y. Hu, C. Li, 2003. The osteology of *Rhombomylus*
    (Mammalia, Glires): implications for phylogeny and evolution of
    Glires. *Bulletin of the American Museum of Natural History*
    275:1-247. [1](http://hdl.handle.net/2246/442)

4.
5.  Asher R.J., J. Meng, J.R. Wible, M.C. McKenna, G.W. Rougier, D.
    Dashzeveg, and M.J. Novacek, 2005. Stem Lagomorpha and the antiquity
    of Glires. *Science* 307:1091-1094.
    [Abstract](http://www.sciencemag.org/cgi/content/abstract/307/5712/1091)

6.  Madsen O., M. Scally, C. J. Douady, D. J. Kao, R. W. DeBry, R.
    Adkins, H. M. Amrine, M. J. Stanhope, W. W. de Jong, M. S. Springer,
    2001 Parallel adaptive radiations in two major clades of placental
    mammals. *Nature* 409:610-614.
    [2](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=11214318&dopt=Abstract)

7.  Murphy W. J., E. Eizirik, W. E. Johnson, Y. P. Zhang, O. A. Ryder,
    S. J. O'Brien, 2001a. Molecular phylogenetics and the origins of
    placental mammals. *Nature* 409:614-618.
    [3](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=11214319&dopt=Abstract)

8.  Ulfur Arnason, et al. Mammalian mitogenomic relationships and the
    root of the eutherian tree. *Proceedings of the National Academy of
    Science* 99: 8151-8156.
    [4](http://www.pnas.org/cgi/content/full/99/12/8151)

9.  Jan Ole Kriegs, Gennady Churakov, Jerzy Jurka, Jürgen Brosius, and
    Jürgen Schmitz (2007) Evolutionary history of 7SL RNA-derived SINEs
    in Supraprimates. Trends in Genetics 23 (4): 158-161
    [5](http://dx.doi.org/10.1016/j.tig.2007.02.002) (PDF version )

10.

11.

12.

13.

14.