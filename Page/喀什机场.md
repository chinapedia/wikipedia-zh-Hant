**喀什机场**（；）是一座中國民用機場，位於[新疆維吾爾自治區](../Page/新疆維吾爾自治區.md "wikilink")[喀什市](../Page/喀什市.md "wikilink")。

喀什机场于1953年建成，先后于1974年和1986年进行过两次改扩建工程。原[飞行区等级为](../Page/飞行区等级.md "wikilink")4D级，1条2800×45×0.24米的水泥[跑道](../Page/跑道.md "wikilink")，[候机楼](../Page/候机楼.md "wikilink")4,856平方米。1997年至1999年进行了飞行区改扩建工程，投资2亿元新建一条长3,200米的标准跑道，飞行区等级升级为4E级。

1993年4月23日，经[国务院批准开放喀什机场](../Page/国务院.md "wikilink")[口岸](../Page/口岸.md "wikilink")。国际联检楼[建筑面积](../Page/建筑面积.md "wikilink")3,100平方米，于2001年8月完工，并于2002年6月5日正式启用。

2004年，喀什－[巴基斯坦](../Page/巴基斯坦.md "wikilink")[伊斯兰堡的国际](../Page/伊斯兰堡.md "wikilink")[航线正式开通](../Page/航线.md "wikilink")。

## 航空公司与航点

2017-2018冬春航季

## 参考资料

  - 新疆外经贸：[喀什机场口岸](http://www.xjftec.gov.cn/Family/zhengwugongkai/kouan/40289298127e71bc01127e89bb010b38.html?1357883469)

## 相关条目

  - [中国大陆机场列表](../Page/中国大陆机场列表.md "wikilink")

[Category:新疆機場](../Category/新疆機場.md "wikilink")
[Category:喀什市建筑](../Category/喀什市建筑.md "wikilink")