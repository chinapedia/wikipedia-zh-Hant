**喀山汗國**（；）是[金帳汗國瓦解後產生的](../Page/金帳汗國.md "wikilink")[汗国](../Page/汗国.md "wikilink")（1441—1552年），由[拔都的兄弟](../Page/拔都.md "wikilink")[禿花帖木兒後人](../Page/禿花帖木兒.md "wikilink")[兀魯·穆罕默德和兒子](../Page/兀魯·穆罕默德.md "wikilink")[馬赫第提克住在](../Page/馬赫第提克.md "wikilink")[喀山建立的獨立汗國](../Page/喀山.md "wikilink")，範圍大約是[伏尔加保加利亚區域](../Page/伏尔加保加利亚.md "wikilink")、現今的[楚瓦什共和國](../Page/楚瓦什共和國.md "wikilink")、[馬里埃爾共和國](../Page/馬里埃爾共和國.md "wikilink")、[韃靼斯坦共和國](../Page/韃靼斯坦共和國.md "wikilink")、[烏德穆爾特共和國和](../Page/烏德穆爾特共和國.md "wikilink")[巴什科爾托斯坦共和國](../Page/巴什科爾托斯坦共和國.md "wikilink")。

## 历史

在[馬赫第提克在位期間](../Page/馬赫第提克.md "wikilink")，汗國正式立國。1452年，他的一位兄弟[卡西姆逃亡](../Page/卡西姆.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")。[莫斯科公国把一塊](../Page/莫斯科公国.md "wikilink")[奥卡河畔的](../Page/奥卡河.md "wikilink")、以他的名字命名土地封給他，名為[卡西莫夫](../Page/卡西莫夫.md "wikilink")。其屬下一部分逃亡至[克里米亞汗國](../Page/克里米亞汗國.md "wikilink")。

馬赫第提克在1452年冬季，進攻[莫斯科公國](../Page/莫斯科公國.md "wikilink")。他的继承者喀山汗[易卜拉欣甚至](../Page/易卜拉欣.md "wikilink")1468年征服了[维亚特卡](../Page/维亚特卡.md "wikilink")，但是，他不久就被迫与俄国人媾和，并交还了他所俘获的人。易卜拉欣的两个儿子[伊尔哈姆和](../Page/伊尔哈姆.md "wikilink")[穆罕默德·阿明爭立](../Page/穆罕默德·阿明.md "wikilink")。阿明逃亡俄國，俄國派軍護送，使他即位。但他在1505年反叛，次年他打败了一支莫斯科公国的军队。

莫斯科為了控制[伏爾加河](../Page/伏爾加河.md "wikilink")，向喀山进攻。1516年阿明死後，喀山汗國絕嗣。接著克里米亞[格來王朝和莫斯科公國的](../Page/格來王朝.md "wikilink")[瓦西里三世輪流強加自己的人選於喀山](../Page/瓦西里三世.md "wikilink")。1545—1552年，罗斯军队为阻止蒙古鞑靼人的侵袭而对喀山汗国进行征战，叫“[喀山远征](../Page/喀山远征.md "wikilink")”。经过数次的征战，1552年10月2日[伊凡四世对喀山实施围攻和强击](../Page/伊凡四世.md "wikilink")，结果喀山汗国被消灭，伏尔加河中游地区并入俄国。俄罗斯的象征[圣瓦西里大教堂即为纪念](../Page/圣瓦西里大教堂.md "wikilink")[伊凡四世攻陷喀山汗国所建](../Page/伊凡四世.md "wikilink")。

## 经济和人口

居民是操[突厥語的](../Page/突厥語.md "wikilink")[巴什基爾人](../Page/巴什基爾人.md "wikilink")、[楚瓦什人](../Page/楚瓦什人.md "wikilink")，[诺盖人和](../Page/诺盖人.md "wikilink")[烏拉爾語系的](../Page/烏拉爾語系.md "wikilink")[摩爾多瓦人](../Page/摩爾多瓦人.md "wikilink")、[馬里人](../Page/馬里人.md "wikilink")（舊稱**切列米斯人**）、[烏德穆爾特人](../Page/烏德穆爾特人.md "wikilink")，以及一些[科米人部落](../Page/科米人.md "wikilink")。最主要是[保加尔人](../Page/保加尔人.md "wikilink")。

汗国居民主要從事农业(黑麥、大麥和燕麥)，也有手工业（生產粘土製品，木材和金屬手工，皮革，盔甲，犁和珠寶）和養蜂。汗国治下的城市贸易商多与高加索、中亚、莫斯科公国贸易，到十六世纪俄罗斯是最重要贸易夥伴，主要市場是喀山和伏爾加河上的大[巴扎](../Page/巴扎.md "wikilink")。

## 政治

國家最高統治者是可汗，他的行为决定是基於一个内阁会议的協商。貴族組成[伯克](../Page/伯克.md "wikilink")，[埃米爾](../Page/埃米爾.md "wikilink")，[米儿咱](../Page/米儿咱.md "wikilink")，神職人員也有相當大影響力，有經學院。軍隊二萬至六萬人。由當地[保加爾人組成貴族](../Page/保加爾人.md "wikilink")，但汗的[保鑣由](../Page/保鑣.md "wikilink")[欽察人与诺蓋人出任](../Page/欽察人.md "wikilink")，也有韃靼人，他們是由俄羅斯的精英、[突厥语族人](../Page/突厥语族.md "wikilink")、蒙古人结合而成，由四大[貴族](../Page/貴族.md "wikilink")（阿尔根、八林、钦察、Shirin）主持汗國。该国以伊斯兰教为国教。多数人是平民。战俘多卖给[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")、[布哈拉汗国和](../Page/布哈拉汗国.md "wikilink")[希瓦汗国](../Page/希瓦汗国.md "wikilink")，非穆斯林要交[吉茲亞税](../Page/吉茲亞税.md "wikilink")。

縱觀其歷史，汗國容易出現內亂和爭位。汗國115年更換了19次可汗，一些多次登基。汗常常被當地貴族選出。

## 文化

俄罗斯消息指出，至少有五种语言被用在喀山汗国。首要是[鞑靼语](../Page/鞑靼语.md "wikilink")，其中包括喀山鞑靼人中的方言（其前身为穆斯林保加尔人）和西部方言的mishars。其书面形式（旧鞑靼语）。[楚瓦什语言是保加尔语言的一个分支](../Page/楚瓦什语.md "wikilink")，该保加尔语言，也强烈地影响了鞑靼语的中部方言。其他三組，大概是[马里语](../Page/马里语.md "wikilink")，[摩爾達維亞语和](../Page/摩爾達維亞语.md "wikilink")[巴什基尔语](../Page/巴什基尔语.md "wikilink")。

## 資料來源

**維爾納茨基**:蒙古與俄羅斯（2）

[Category:欧洲汗国](../Category/欧洲汗国.md "wikilink")
[Category:俄罗斯历史政权](../Category/俄罗斯历史政权.md "wikilink")