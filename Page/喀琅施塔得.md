[Spb_kronshtadt.svg](https://zh.wikipedia.org/wiki/File:Spb_kronshtadt.svg "fig:Spb_kronshtadt.svg")
**-{zh-cn:喀琅施塔得;zh-hk:克隆施塔特;zh-tw:喀琅施塔得}-**（，），又譯作**王冠城**，是[俄罗斯的一个港口城市](../Page/俄罗斯.md "wikilink")，位于[芬蘭灣的口上的](../Page/芬蘭灣.md "wikilink")[科特林岛上](../Page/科特林岛.md "wikilink")，[圣彼得堡以西约](../Page/圣彼得堡.md "wikilink")30千米。行政上它属于圣彼得堡，也是圣彼得堡的主要港口。1921年3月这里爆发了[王冠城叛乱](../Page/王冠城叛乱.md "wikilink")。

过去王冠城是俄罗斯海军指挥所和[波罗的海舰队的基地](../Page/波罗的海舰队.md "wikilink")。今天城市的历史中心以及其要塞是[世界遗产](../Page/世界遗产.md "wikilink")[圣彼得堡历史中心及相关建筑群的一部分](../Page/圣彼得堡历史中心及相关建筑群.md "wikilink")。

## 历史

[Kronstadt_Peter_the_Great_monument.jpg](https://zh.wikipedia.org/wiki/File:Kronstadt_Peter_the_Great_monument.jpg "fig:Kronstadt_Peter_the_Great_monument.jpg")

1703年[彼得大帝从](../Page/彼得大帝.md "wikilink")[瑞典手中夺得科特林岛后建立王冠城](../Page/瑞典.md "wikilink")。1704年5月18日第一座要塞开始启用。

王冠城的要塞在很短的时间里就建成了。芬兰湾本身不很深，在冬季它完全被冻结。工人用马将上千载满石头的橡木箱从冰上运到当地，沉入冰洞里，由此建造了数个人工岛屿，然后在这些岛上建造了堡垒。这样完全封锁了通向圣彼得堡的水道。只有两条很窄的水路依然可以行船，而它们的两侧则有非常坚固的堡垒守卫。

这样一共形成的五个堡垒是芬兰湾口的主要防御设施。在[克里米亚战争中它们成功地击退了](../Page/克里米亚战争.md "wikilink")[英国和](../Page/英国.md "wikilink")[法国的联合舰队](../Page/法国.md "wikilink")。19世纪里王冠城的要塞再次被扩大和加固。从1856年至1871年四个新的炮台建成，它们控制着芬兰湾的主入口，另外七个炮台则控制着比较浅的北航道。所有这些堡垒全部非常低，由极厚的装甲保护，装备着重[克虏伯炮塔](../Page/克虏伯.md "wikilink")。

### 王冠城起义

1921年一群王冠城的[水手](../Page/水手.md "wikilink")、[士兵及其](../Page/士兵.md "wikilink")[平民支持者](../Page/平民.md "wikilink")，发动反对王冠城[布尔什维克政府的起义](../Page/布尔什维克.md "wikilink")，他们要求[言论自由](../Page/言论自由.md "wikilink")、停止將人关入[集中营](../Page/集中营.md "wikilink")、改变苏联战争政策、停止[共产党对](../Page/共产党.md "wikilink")[苏维埃的控制](../Page/苏维埃.md "wikilink")，以及私人允许擁有更多的财产。苏联战争部长和[苏联红军总指挥](../Page/苏联红军.md "wikilink")[列夫·托洛茨基与起义者短期谈判后派军进入王冠城血腥镇压了起义](../Page/列夫·托洛茨基.md "wikilink")。这是1991年苏联解体前在苏联本土上最后一次发生反对[共产党的大规模武装抗争](../Page/共产党.md "wikilink")。

### 第二次世界大战

在1930年代後期，克隆施塔特過著堅固城市的生活，是波羅的海艦隊的基地。在此期間，克隆施塔特是蘇聯海軍的重要訓練中心。在克隆施塔特船舶修理廠（海軍廠）大修和修理水面艦艇和波羅的海艦隊的潛艇。

在[第二次世界大战中克隆施塔特受到](../Page/第二次世界大战.md "wikilink")[纳粹德国空军多次的强烈轰炸](../Page/纳粹德国空军.md "wikilink")。1941年8月，德國空軍開始定期轟炸克隆施塔特。引人注目的轟炸是驾驶[Ju
87俯衝轟炸機的王牌飞行员](../Page/Ju_87俯衝轟炸機.md "wikilink")[汉斯·乌尔里希·鲁德尔击沉了](../Page/汉斯·乌尔里希·鲁德尔.md "wikilink")[彼得罗巴甫洛夫斯克号战列舰](../Page/彼得罗巴甫洛夫斯克号战列舰.md "wikilink")。

到1941年底，紅軍共進行了82次海軍作戰。希特勒感到憤怒，因為蘇聯潛艇經常擾亂從瑞典到德國的戰略物資的軍事供應。德國人試圖用反潛網和地雷完全封鎖芬蘭灣的出口。儘管做出了這些努力，蘇聯潛艇艇員繼續攻擊德國船隻。1942年，潛艇與偵察機合作搜尋軍事目標。29艘德國船隻沉沒。

## 市容

[Kronstadt_Bypass_canal.jpg](https://zh.wikipedia.org/wiki/File:Kronstadt_Bypass_canal.jpg "fig:Kronstadt_Bypass_canal.jpg")
王冠城建在平地上，因此历史上多次被淹，其中最著名的是在1824年。市的南侧有三个港口：最大的是西边的商港。中间的港口曾短时被用来做修补船只用。东边的军港被俄罗斯海军使用。两条运河连接西港和中港，穿越市区。在这两条运河之间曾经树立着一座意大利式宫廷。后来被一座飞行学校占领。
[Kronstadt_Naval_Cathedral.jpg](https://zh.wikipedia.org/wiki/File:Kronstadt_Naval_Cathedral.jpg "fig:Kronstadt_Naval_Cathedral.jpg")
[Naval_Cathedral_of_St_Nicholas_in_Kronstadt_02.jpg](https://zh.wikipedia.org/wiki/File:Naval_Cathedral_of_St_Nicholas_in_Kronstadt_02.jpg "fig:Naval_Cathedral_of_St_Nicholas_in_Kronstadt_02.jpg")

今天市内最显眼的建筑是从1908年至1913年建造的[海军大教堂](../Page/海军大教堂.md "wikilink")。这座教堂被看作是新拜占庭式建筑的顶峰。王冠城的老圣安德鲁大教堂于1932年被共产党下令摧毁。
[Kronstadt_lighthouse.jpg](https://zh.wikipedia.org/wiki/File:Kronstadt_lighthouse.jpg "fig:Kronstadt_lighthouse.jpg")\]\]

其它公共建筑还有海军医院、英国海员医院、民用医院、海军部、捣药库、仓库、海军工程学校和英国教堂。每年王冠城的港口从12月至4月有140至160天受冰封。市内相当多居民是水手。

王冠城的要塞被看作是世界上防卫最好的滨海要塞，从外部它从未被攻克过。今天王冠城依然保留了一些过去人工造成的堡垒岛屿。过去在芬兰湾的南北岸之间曾经有过42个这样的堡垒排成一列。市内也有一些堡垒。

通过建造圣彼得堡大坝一些这些碉堡被拆除。这个大坝将连接王冠城与大陆。

## 外部連結

  - [Kronstadt history](http://www.kronstadt.ru/)
  - [Kronstadt web site](http://www.kotlin.ru/)
  - [Map of
    Kronstadt](http://www.around.spb.ru/maps/suburb/kronstadt_gen.gif)
  - [Kronstadt: Virtual Excursion](http://www.kronshtadt.info/)
  - [International Kronstadt Development
    Fund](http://www.kronfund.ru/eng/)

[К](../Category/聖彼得堡下轄市鎮.md "wikilink")