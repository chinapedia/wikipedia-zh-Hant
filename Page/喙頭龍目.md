**喙頭龍目**（Rhynchosauria）是一群[雙孔類](../Page/雙孔類.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，生存在[三疊紀](../Page/三疊紀.md "wikilink")，跟[主龍類有接近親緣關係](../Page/主龍類.md "wikilink")。牠們是[草食性動物](../Page/草食性.md "wikilink")，擁有矮胖結實的身體與強壯的喙狀嘴。喙頭龍類曾經非常興盛，在某些化石挖掘現場，有40%\~60%的化石屬於喙頭龍目。

## 敘述

[Hyperodapedon_huxleyi_maxilla.jpg](https://zh.wikipedia.org/wiki/File:Hyperodapedon_huxleyi_maxilla.jpg "fig:Hyperodapedon_huxleyi_maxilla.jpg")的上頜、上顎牙齒\]\]
早期原始物種的體型小，在外形上類似[蜥蜴](../Page/蜥蜴.md "wikilink")，例如：[中鱷](../Page/中鱷.md "wikilink")（*Mesosuchus*）、*Howesia*。早期原始物種的頭骨，類似早期[雙孔類的](../Page/雙孔類.md "wikilink")[楊氏蜥](../Page/楊氏蜥.md "wikilink")，除了喙狀嘴與少數其他特徵。

較晚的進階型物種，體型為中到大型，身長可達約2公尺。頭骨較短、寬廣、呈三角形，頭骨的寬度比長度還長，例如最先進的[異平齒龍](../Page/異平齒龍.md "wikilink")（=[舟爪龍](../Page/舟爪龍.md "wikilink")*Scaphonyx*），臉頰較深，[前上頜骨往前](../Page/前上頜骨.md "wikilink")、往下伸長，形成上喙。寬廣的頭骨能容納強壯的下頜肌肉。下頜也很深，當嘴部閉合時，下頜與[上頜骨緊緊地閉合](../Page/上頜骨.md "wikilink")，類似小刀折回它的刀柄。這種剪刀狀的動作可讓喙頭龍切斷堅硬的植物。

喙頭龍類的牙齒也很奇特，上頜的牙齒轉變成寬廣的齒板。後肢上有巨大的腳爪，被推測是用來挖掘根部與塊莖。

如同這時期的其他動物，喙頭龍類也是散佈於全球的，在[盤古大陸各地都有發現化石](../Page/盤古大陸.md "wikilink")。這些繁盛的動物在[卡尼階之末忽然消失](../Page/卡尼階.md "wikilink")，也許是因為牠們賴以為食的[二叉羊齒](../Page/二叉羊齒.md "wikilink")（*Dicrodium*）絕種。

### 種系發生學

以下[演化樹是根據](../Page/演化樹.md "wikilink")[麥克斯·郎格](../Page/麥克斯·郎格.md "wikilink")（Max
C. Langer）等人的2000年喙頭龍類研究\[1\]︰

[Mesosuchus_BW.jpg](https://zh.wikipedia.org/wiki/File:Mesosuchus_BW.jpg "fig:Mesosuchus_BW.jpg")（*Mesosuchus*），生存於早[三疊紀的](../Page/三疊紀.md "wikilink")[南非](../Page/南非.md "wikilink")\]\]
[Paradapedon_1DB.jpg](https://zh.wikipedia.org/wiki/File:Paradapedon_1DB.jpg "fig:Paradapedon_1DB.jpg")[印度](../Page/印度.md "wikilink")\]\]
以下演化樹來自於2008年的喙頭龍類研究\[2\]︰

以下演化樹則來自於麥克斯·郎格的2010年研究\[3\]︰

## 參考資料

  - Michael J. Benton (2000), *Vertebrate Paleontology*, 2nd ed.
    Blackwell Science Ltd
  - Robert L. Carroll (1988), *Vertebrate Paleontology and Evolution*,
    W.H. Freeman & Co.
  - David Dilkes 1998. The Early Triassic rhynchosaur *Mesosuchus
    browni* and the interrelationships of basal archosauromorph
    reptiles. *Philosophical Transactions of the Royal
    Society|Philosophical Transactions of the Royal Society of London:
    Biological Sciences*, 353:501-541.

## 外部連結

  - [Dinosaurs of Rio grande do
    Sul.](https://web.archive.org/web/20080127175240/http://www.royalsul.com.br/paleo/galeria.asp)

[\*](../Category/主龍形下綱.md "wikilink")

1.
2.
3.