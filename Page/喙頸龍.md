**喙頸龍**（屬名：*Rhamphinion*）意為「喙狀嘴的頸背」，喙狀嘴是[喙嘴翼龍科屬名中常見的詞源](../Page/喙嘴翼龍科.md "wikilink")，而頸背指的是喙頸龍的化石來自於頭顱骨後部。喙頸龍是種[翼龍類](../Page/翼龍類.md "wikilink")，發現於[美國](../Page/美國.md "wikilink")[亞利桑那州東北部的](../Page/亞利桑那州.md "wikilink")[卡岩塔組](../Page/卡岩塔組.md "wikilink")，該地年代為下[侏儸紀的](../Page/侏儸紀.md "wikilink")[錫內穆階到中](../Page/錫內穆階.md "wikilink")[普連斯巴奇階](../Page/普連斯巴奇階.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**詹氏喙頸龍**（*R.
jenkinsi*），是在1984年由[凱文·帕迪恩](../Page/凱文·帕迪恩.md "wikilink")（Kevin
Padian）敘述、命名

牠們的[正模標本](../Page/正模標本.md "wikilink")（編號MNA V
4500）為一個部份頭顱骨，包含[枕骨](../Page/枕骨.md "wikilink")，一個部份左[顴骨](../Page/顴骨.md "wikilink")，一個破碎的下頜，上有兩顆牙齒與第三顆牙齒的痕跡，以及其他碎片。喙頸龍一度是[西半球已知最古老的翼龍類標本](../Page/西半球.md "wikilink")。喙頸龍的命名者凱文·帕迪恩並沒有將喙頸龍歸類於[翼龍目的任何一科或亞目](../Page/翼龍目.md "wikilink")，但他注意到喙頸龍的顴骨並不類似[翼手龍亞目的顴骨](../Page/翼手龍亞目.md "wikilink")，所以應該屬於[喙嘴翼龍亞目](../Page/喙嘴翼龍亞目.md "wikilink")。在同一地點所發現的另一個其他翼龍類的翼部骨頭，也可能屬於喙嘴翼龍亞目，而翼展約1.5公尺寬\[1\]。[彼得·沃爾赫費爾](../Page/彼得·沃爾赫費爾.md "wikilink")（Peter
Wellnhofer）認為牠們非常有可能屬於喙嘴翼龍亞目\[2\]，但[大衛·安文](../Page/大衛·安文.md "wikilink")（David
Unwin）則在「The Pterosaurs: From Deep
Time」一書中保持更質疑的態度，認為這些破碎骨頭僅是一個關係未定的可能有效種\[3\]。

## 參考資料

## 外部連結

  - [喙頸龍](https://web.archive.org/web/20070930154836/http://www.pterosaur.co.uk/species/LJP/unclass/Rhamphinion.htm)
    翼龍資料庫網站
  - [喙頸龍簡介](https://web.archive.org/web/20070923150337/http://archosauria.org/pterosauria/taxonomy/genera/rhamphinion.html)

[Category:喙嘴翼龍亞目](../Category/喙嘴翼龍亞目.md "wikilink")
[Category:侏羅紀翼龍類](../Category/侏羅紀翼龍類.md "wikilink")

1.  Padian, K. (1984). [Pterosaur remains from the Kayenta Formation
    (?early Jurassic) of
    Arizona](http://palaeontology.palass-pubs.org/pdf/Vol%2027/Pages%20407-413.pdf)
    . *Palaeontology* **27**(2):407-413. \[if you get an I/O error
    message, push "OK" and it should work\]
2.
3.  Unwin, D.M. (2006). *The Pterosaurs: From Deep Time*. Pi Press:New
    York, p. 273. ISBN 978-0-13-146308-0.