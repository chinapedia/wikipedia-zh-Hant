**喜納昌吉**（）是[日本](../Page/日本.md "wikilink")[沖繩民謠歌手](../Page/沖繩.md "wikilink")、反战主义政治家，他是[琉球國](../Page/琉球國.md "wikilink")[三司官](../Page/三司官.md "wikilink")[楊太鶴](../Page/楊太鶴.md "wikilink")（山内親方昌信）的後人，父親是有「沖繩民謠第一人」之稱的[喜納昌永](../Page/喜納昌永.md "wikilink")。

作品《花》（「[花〜すべての人の心に花を〜](../Page/花〜すべての人の心に花を〜.md "wikilink")」）为他的代表作，在日本国内外为多人所翻唱。1993年亦經[台灣著名作詞人](../Page/台灣.md "wikilink")[厲曼婷改編为歌曲](../Page/厲曼婷.md "wikilink")《[花心](../Page/花心_\(周華健专辑\).md "wikilink")》，由[周華健主唱](../Page/周華健.md "wikilink")，此曲後來在两岸三地受到許多歌迷歡迎。[粵語版](../Page/粵語.md "wikilink")《幸運就是遇到妳》由[馬浚偉主唱](../Page/馬浚偉.md "wikilink")。

在2004年當選日本[參議員](../Page/參議院_\(日本\).md "wikilink")。

2014年9月，喜納昌吉宣佈參選11月舉行的沖繩縣知事選舉，並退出[民主黨](../Page/民主黨_\(日本\).md "wikilink")。最終落敗。

[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:比例區選出日本參議院議員](../Category/比例區選出日本參議院議員.md "wikilink")
[Category:沖繩縣出身人物](../Category/沖繩縣出身人物.md "wikilink") [Category:日本參議院議員
2004–2010](../Category/日本參議院議員_2004–2010.md "wikilink")
[Shoukichi](../Category/楊氏喜納家.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[Category:沖繩國際大學校友](../Category/沖繩國際大學校友.md "wikilink")