**喬治·布拉克**（**Georges Braque**
，），為[法國](../Page/法國.md "wikilink")[立體主義](../Page/立體主義.md "wikilink")[畫家與](../Page/畫家.md "wikilink")[雕塑家](../Page/雕塑家.md "wikilink")。布拉克於1882年出生於法國[阿讓特伊](../Page/阿讓特伊.md "wikilink")，1963年卒於法國巴黎。他與[巴伯羅·畢卡索在](../Page/巴伯羅·畢卡索.md "wikilink")20世紀初所創立的[立體主義運動](../Page/立體主義.md "wikilink")，深深影響了後來[美術史的發展](../Page/美術史.md "wikilink")，「立體主義」一名也由其作品而來。

乔治的童年在法国北部度过，他的父亲是一个油漆工人，受他的父亲的影响，乔治从小就对涂画有所兴趣，他于1907年与毕加索相遇，然后他们一起在法国南部作画。因天性安靜，他的名聲很大程度上被畢加索所掩蓋了。\[1\]

## 野獸派時期

[Georges_Braque,_1906,_L'Olivier_près_de_l'Estaque_(The_Olive_tree_near_l'Estaque).jpg](https://zh.wikipedia.org/wiki/File:Georges_Braque,_1906,_L'Olivier_près_de_l'Estaque_\(The_Olive_tree_near_l'Estaque\).jpg "fig:Georges_Braque,_1906,_L'Olivier_près_de_l'Estaque_(The_Olive_tree_near_l'Estaque).jpg")
[Georges_Braque,_1908,_Maisons_et_arbre,_oil_on_canvas,_40.5_x_32.5_cm,_Lille_Métropole_Museum_of_Modern,_Contemporary_and_Outsider_Art.jpg](https://zh.wikipedia.org/wiki/File:Georges_Braque,_1908,_Maisons_et_arbre,_oil_on_canvas,_40.5_x_32.5_cm,_Lille_Métropole_Museum_of_Modern,_Contemporary_and_Outsider_Art.jpg "fig:Georges_Braque,_1908,_Maisons_et_arbre,_oil_on_canvas,_40.5_x_32.5_cm,_Lille_Métropole_Museum_of_Modern,_Contemporary_and_Outsider_Art.jpg")
布拉克早期從事[印象派繪畫](../Page/印象派.md "wikilink")，但在1905年看到[野獸派的作品之後便開始轉型](../Page/野獸派.md "wikilink")。當時他主要與[劳尔·杜飞及](../Page/劳尔·杜飞.md "wikilink")[奥东·弗里茨走得比較近](../Page/奥东·弗里茨.md "wikilink")。1906年布拉克與弗里茨前往[艾斯塔克](../Page/艾斯塔克.md "wikilink")、[安特衛普旅遊](../Page/安特衛普.md "wikilink")。\[2\]
1907年5月布拉克在[獨立者沙龍展出了多幅野獸派作品](../Page/獨立者沙龍.md "wikilink")。同年，由於[塞尚的影響](../Page/塞尚.md "wikilink")，他的作品風格再次發生轉變。

## 立體主義

法國藝術評論家[路易·沃克塞爾在](../Page/路易·沃克塞爾.md "wikilink")1908年見到布拉克的作品《埃斯塔克的房子》時就曾說那幅畫“充滿了小立體方塊”，不過立體主義一詞正式出現是1911年，而布拉克與畢加索一樣最初並不喜歡這個稱呼。\[3\]

1909年開始他開始與[畢加索合作](../Page/畢加索.md "wikilink")，主要研究塞尚的作畫理念，但與畢加索的研究方向並不相同。\[4\]\[5\]
不過可以說，立體主義是畢加索和布拉克兩人共同在巴黎[蒙馬特發明出來的](../Page/蒙馬特.md "wikilink")。\[6\]

1911年夏立體主義的發展到了一個關鍵點\[7\]，當時兩人在[塞雷繪製的作品及其相似](../Page/塞雷_\(东比利牛斯省\).md "wikilink")，很難以分辨。1912年他們開始嘗試[拼貼畫](../Page/拼貼畫.md "wikilink")
，布拉克發明了所謂的纸拼贴法（papier colle）。\[8\]
畢加索和他的合作關係一直持續到1914年[一戰爆發](../Page/一戰.md "wikilink")，布拉克被征入法軍。1915年5月布拉克在[卡朗西的一場戰鬥中嚴重受傷導致](../Page/卡朗西.md "wikilink")[頭部穿孔](../Page/頭部穿孔.md "wikilink")，並短暫失明。\[9\]

## 晚期作品

1916年末布拉克開始重新作畫，他的作品風格也更加突出。這一時期他繪製了許多[靜物畫](../Page/靜物畫.md "wikilink")。\[10\]
除去繪畫外，他還創作了許多的雕塑及其他藝术作品。\[11\] 1963年布拉克在巴黎逝世。

## 參见

  - [立體主義](../Page/立體主義.md "wikilink")

## 参考文献

## 外部連結

[Category:法國藝術家](../Category/法國藝術家.md "wikilink")
[Category:法國畫家](../Category/法國畫家.md "wikilink")
[Category:法國雕塑家](../Category/法國雕塑家.md "wikilink")
[Category:野獸派畫家](../Category/野獸派畫家.md "wikilink")
[Category:立體主義藝術家](../Category/立體主義藝術家.md "wikilink")

1.  [Lewis Kachur, From Grove Art Online, MoMA, 2009 Oxford University
    Press](http://www.moma.org/collection/artist.php?artist_id=744)

2.
3.  [Ernst Gombrich](../Page/Ernst_Gombrich.md "wikilink") (1960) *Art
    and Illusion*, as quoted in [Marshall
    McLuhan](../Page/Marshall_McLuhan.md "wikilink") (1964)
    *[Understanding Media](../Page/Understanding_Media.md "wikilink")*,
    p.12

4.  Fry 1966, p. 71.

5.

6.  Picasso, P., Rubin, W. S., & Fluegel, J. (1980). *Pablo Picasso, a
    retrospective*. New York: Museum of Modern Art. ISBN 0-87070-528-8
    p. 99,

7.  [Solomon_R._Guggenheim_Museum](http://www.guggenheim.org/new-york/collections/collection-online/show-full/piece/?search=Pablo%20Picasso&page=1&f=People&cr=5)


8.  Cooper, Philip. *Cubism*. London: Phaidon, 1995, p. 14. ISBN
    0714832502

9.  Oxford Art Online, "Georges Braque"

10. [Allen Memorial Art
    Museum](http://oberlin.edu/amam/documents/16_Braque_BlueGuitar.pdf)

11.