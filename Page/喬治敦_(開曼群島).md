**喬治敦**位於[大開曼島](../Page/大開曼.md "wikilink")，是英屬[開曼群島的](../Page/開曼群島.md "wikilink")[首都](../Page/首都.md "wikilink")，人口27,704是開曼群島[金融業的重鎮](../Page/金融業.md "wikilink")，超過600家以上的銀行皆設置於此，而英屬開曼群島目前在世界上是僅次於[美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")、[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")、[日本](../Page/日本.md "wikilink")[東京和](../Page/東京.md "wikilink")[香港的第五大金融中心](../Page/香港.md "wikilink")。

除了金融機構，喬治敦也有一個貨運碼頭、以及幾處[購物中心](../Page/購物中心.md "wikilink")。此外喬治敦近郊建有[歐文·羅伯茨國際機場](../Page/歐文·羅伯茨國際機場.md "wikilink")。

## 參考文獻

## 外部連結

  - [Photos of George
    Town](https://web.archive.org/web/20110417145255/http://opengalleries.org/cayman_islands/george_town/George_Town.html)
  - [Webcam in George
    Town](https://web.archive.org/web/20130204020528/http://www.webviewcams.com/america/cayman-islands/george-town)

[Category:開曼群島](../Category/開曼群島.md "wikilink")
[Category:属地首府](../Category/属地首府.md "wikilink")