**喬爾語**（Ch’ol）屬於[馬雅語系的語言](../Page/馬雅語系.md "wikilink")，為[墨西哥](../Page/墨西哥.md "wikilink")[喬爾族人的語言](../Page/喬爾族.md "wikilink")。

喬爾語可追溯回[古典馬雅語](../Page/古典馬雅語.md "wikilink")，與使用於[瓜地馬拉的](../Page/瓜地馬拉.md "wikilink")[奇奧蒂語有很近的關係](../Page/奇奧蒂語.md "wikilink")。

## 音韵

以下为乔尔语的音韵。\[1\]

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/唇音.md" title="wikilink">唇音</a></p></th>
<th><p><a href="../Page/齿龈音.md" title="wikilink">齿龈音</a></p></th>
<th><p><a href="../Page/硬腭音.md" title="wikilink">硬腭音</a></p></th>
<th><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a></p></th>
<th><p><a href="../Page/聲門音.md" title="wikilink">聲門音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/边音.md" title="wikilink">边音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/颤音_(语音学).md" title="wikilink">颤音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半元音.md" title="wikilink">半元音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|                                  | [前元音](../Page/前元音.md "wikilink") | [央元音](../Page/央元音.md "wikilink") | [後元音](../Page/後元音.md "wikilink") |
| -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| [閉元音](../Page/閉元音.md "wikilink") | i                                | ɨ                                | u                                |
| [中元音](../Page/中元音.md "wikilink") | e                                |                                  | o                                |
| [開元音](../Page/開元音.md "wikilink") |                                  | a                                |                                  |

## 参考来源

## 參考文獻

  -

[Category:馬雅語族](../Category/馬雅語族.md "wikilink")
[Category:墨西哥語言](../Category/墨西哥語言.md "wikilink")

1.  Vázquez Álvarez, Juan Jesús. *A Grammar of Chol, a Mayan Language*.
    Austin, Texas: University of Texas at Austin, 2011; p.35