**單純狸藻**（[學名](../Page/學名.md "wikilink")：***Utricularia
simplex***）為[狸藻屬](../Page/狸藻屬.md "wikilink")[多年生極小型](../Page/多年生植物.md "wikilink")[陸生](../Page/陸生植物.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")。其[种加词](../Page/种加词.md "wikilink")“*simplex*”意为“简单、单一”。单纯狸藻为[西澳大利亚的](../Page/西澳大利亚.md "wikilink")[特有種](../Page/特有種.md "wikilink")。其陆生于[海拔近](../Page/海拔.md "wikilink")[海平面的](../Page/海平面.md "wikilink")[荒原或沼泽的](../Page/荒原.md "wikilink")[泥炭质土壤中](../Page/泥炭.md "wikilink")。1810年，[羅伯特·布朗最先发表了单纯狸藻的描述](../Page/羅伯特·布朗.md "wikilink")。\[1\]

## 参考文献

## 外部連結

  - 食虫植物照片搜寻引擎中[单纯狸藻的照片](http://www.cpphotofinder.com/utricularia-simplex-276.html)

[Utricularia simplex](../Category/澳大利亞食蟲植物.md "wikilink")
[simplex](../Category/狸藻屬.md "wikilink")
[simplex](../Category/澳洲狸藻组.md "wikilink")

1.  Taylor, Peter. (1989). *[The genus Utricularia - a taxonomic
    monograph](../Page/狸藻属——分类学专著.md "wikilink")*. Kew Bulletin
    Additional Series XIV: London.