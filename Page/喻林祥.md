**喻林祥**（），[中國](../Page/中國.md "wikilink")[湖北](../Page/湖北.md "wikilink")[应城人](../Page/应城.md "wikilink")，[中国人民解放军和](../Page/中国人民解放军.md "wikilink")[中国人民武装警察部队高级将领](../Page/中国人民武装警察部队.md "wikilink")。1966年4月加入[中国共产党](../Page/中国共产党.md "wikilink")。2006年6月24日晋升为[中国人民解放军上将军衔](../Page/中国人民解放军上将.md "wikilink")。

入伍前在湖北省应城县做邮电局工人，1963年入伍，在[中国人民解放军第一军当兵](../Page/中国人民解放军第一军.md "wikilink")，逐步晋升为政治军官。曾任[第一集团军政治部副主任](../Page/第一集团军.md "wikilink")、第一集团军第二师政治委员。1993年任武警政治部副主任，1996年任武警北京第一总队政委。1997年任解放军[总政治部组织部部长](../Page/总政治部组织部.md "wikilink")。2000年10月转任[兰州军区副政委](../Page/兰州军区.md "wikilink")，兼任[新疆军区政委](../Page/新疆军区.md "wikilink")。2004年12月任[兰州军区政治委员](../Page/兰州军区.md "wikilink")。2007年任武警部队政治委员。2010年7月退役。2010年10月任[全国人大华侨委员会副主任委员](../Page/全国人大华侨委员会.md "wikilink")\[1\]。2013年3月任12届[全国政协港澳台侨委员会副主任](../Page/全国政协港澳台侨委员会.md "wikilink")\[2\]。

## 履历

  - [中国人民解放军第1集团军](../Page/中国人民解放军第1集团军.md "wikilink")[政治部副主任](../Page/政治部.md "wikilink")、第2师[政治委员](../Page/政治委员.md "wikilink")
  - 武警部队政治部副主任
  - 武警北京市第一总队政治委员
  - 武警部队政治部副主任兼北京市第一总队政委
  - 解放军总政治部组织部部长
  - [兰州军区副司令員](../Page/兰州军区.md "wikilink")、[新疆军区政治委员](../Page/新疆军区.md "wikilink")
  - 2004年12月任兰州军区政委。
  - 2007年9月任[武警部队政委](../Page/武警部队.md "wikilink")。
  - 中共第十七届中央委员

## 参考文献

{{-}}

[Category:中国人民解放军上将](../Category/中国人民解放军上将.md "wikilink")
[Category:中国人民武装警察部队上将](../Category/中国人民武装警察部队上将.md "wikilink")
[Category:中国人民武装警察部队政治委员](../Category/中国人民武装警察部队政治委员.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:应城人](../Category/应城人.md "wikilink")
[L林](../Category/喻姓.md "wikilink")

1.  [喻林祥被任命为全国人大华侨委副主任委员](http://www.npc.gov.cn/npc/zhibo/zzzb18/2010-10/28/content_1602108.htm)
2.  [中国人民政治协商会议第十二届全国委员会各专门委员会主任、副主任名单](http://news.xinhuanet.com/2013lh/2013-03/13/c_115015504_3.htm)