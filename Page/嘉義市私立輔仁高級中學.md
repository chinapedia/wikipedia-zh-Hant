**嘉義市天主教輔仁中學**（簡稱**輔仁中學**或**輔中**）是一所由[天主教](../Page/天主教.md "wikilink")[聖言會興辦的](../Page/聖言會.md "wikilink")[高級中學](../Page/高級中學.md "wikilink")。位於[台灣](../Page/台灣.md "wikilink")[嘉義市](../Page/嘉義市.md "wikilink")[東區](../Page/東區_\(嘉義市\).md "wikilink")，前身為1931年[德國](../Page/德國.md "wikilink")[聖言會在](../Page/聖言會.md "wikilink")[北平創立的](../Page/北平.md "wikilink")**[北平輔仁大學附屬中學](../Page/北平輔仁大學附屬中學.md "wikilink")**（[輔大附中](../Page/輔大附中.md "wikilink")）\[1\]。現與[輔仁大學已無從屬關係](../Page/輔仁大學.md "wikilink")。

原本為單純的男性學校，現為[男女共學的高級中學](../Page/男女共學.md "wikilink")。\[2\]

## 公車資訊

| 路線編號                            | 營運業者                                 | 乘車站名     | 行先                                                                                  | 備考 |
| ------------------------------- | ------------------------------------ | -------- | ----------------------------------------------------------------------------------- | -- |
| <font color=red>**7210**</font> | [嘉義客運](../Page/嘉義客運.md "wikilink")   | **輔仁中學** | <font color=green>嘉義</font><font color=black>—</font><font color=purple>白河</font>   |    |
| <font color=red>**7214**</font> | [嘉義客運](../Page/嘉義客運.md "wikilink")   | **輔仁中學** | <font color=green>嘉義</font><font color =black>—</font><font color=purple>關子嶺</font> |    |
| <font color=red>**7215**</font> | [嘉義客運](../Page/嘉義客運.md "wikilink")   | **輔仁中學** | <font color=green>嘉義</font><font color=black>—</font><font color=green>澐水</font>    |    |
| <font color=red>**7216**</font> | [嘉義客運](../Page/嘉義客運.md "wikilink")   | **輔仁中學** | <font color=green>嘉義</font><font color>—</font><font color=green>觸口</font>          |    |
| <font color=red>**7301**</font> | [嘉義縣公車](../Page/嘉義縣公車.md "wikilink") | **輔仁中學** | <font color=green>嘉義</font><font color>—</font><font color=green>嘉義農場</font>        |    |
| <font color=red>**7302**</font> | [嘉義縣公車](../Page/嘉義縣公車.md "wikilink") | **輔仁中學** | <font color=green>嘉義</font><font color>—</font><font color=green>奮起湖</font>         |    |
| <font color=red>**7314**</font> | [嘉義縣公車](../Page/嘉義縣公車.md "wikilink") | **輔仁中學** | <font color=green>嘉義</font><font color>—</font><font color=green>達邦</font>          |    |
| <font color=red>**7322**</font> | [嘉義縣公車](../Page/嘉義縣公車.md "wikilink") | **輔仁中學** | <font color=green>嘉義</font><font color>—</font><font color=green>阿里山</font>         |    |

## 歷史沿革

1931年，[天主教](../Page/天主教.md "wikilink")[聖言會創辦](../Page/聖言會.md "wikilink")[北平輔仁大學附屬中學](../Page/北平輔仁大學附屬中學.md "wikilink")。

1960年，配合[輔仁大學](../Page/輔仁大學.md "wikilink")[在臺復校](../Page/在臺復校.md "wikilink")，[中學也列入復校的考量](../Page/中學.md "wikilink")。1961年，教會任派當時人在[菲律賓傳教的朱秉文神父返國籌辦](../Page/菲律賓.md "wikilink")，擇定校地於嘉義市郊吳鳳路南端[八掌溪畔](../Page/八掌溪.md "wikilink")，即現址一帶。1962年5月，奉臺灣省教育廳核准立案招生，校名為「臺灣省嘉義縣私立輔仁初級中學」，由朱秉文神父擔任第一任校長。同年7月，招收新生入學。1965年，該學年度第一學期起試辦高中為期一年。1966年，該學年度第一學期正式設置高中部，並更改校名為「臺灣省嘉義縣私立輔仁中學」。1979年，依照高級中學法規定於8月1日起改為「臺灣省嘉義縣私立輔仁高級中學」。1982年7月1日，由於嘉義市升格為[省轄市](../Page/省轄市.md "wikilink")，更改校名為「臺灣省嘉義市私立輔仁高級中學」\[3\]。

## 歷任校長

1.  [朱秉文](../Page/朱秉文.md "wikilink") 神父
2.  [洪山川](../Page/洪山川.md "wikilink") 神父
3.  [陳憲一](../Page/陳憲一.md "wikilink") 校長
4.  [張日亮神父](../Page/張日亮.md "wikilink")
5.  [李國榮](../Page/李國榮.md "wikilink") 校長

## 知名校友

  - [汪明輝](../Page/汪明輝.md "wikilink")(行政院原住民委員會副主委)
  - [陳立恆](../Page/陳立恆.md "wikilink")(法蘭瓷總裁)
  - [徐世昌](../Page/徐世昌.md "wikilink")(華碩共同創辦人暨策略長)
  - [李威震](../Page/李威震.md "wikilink")(長庚醫院器官移植中心主任)
  - [沈玉琳](../Page/沈玉琳.md "wikilink")（藝人）
  - [蔡阿嘎本名蔡緯嘉](../Page/蔡阿嘎.md "wikilink")(網路名人)
  - [李泳龍](../Page/李泳龍.md "wikilink")(長榮大學現任校長)
  - [任賢齊](../Page/任賢齊.md "wikilink")(歌手)

## 男籃

籃球隊成立於2015年

## 女籃

籃球隊成立於2015年，由總教練前[達欣工程籃球隊黃玟穎領軍](../Page/達欣工程籃球隊.md "wikilink")，在成立首年挑戰甲級。

## 參見

  - [東區](../Page/東區_\(嘉義市\).md "wikilink")
  - [輔仁大學](../Page/輔仁大學.md "wikilink")

## 參考資料

## 外部連結

  - [天主教輔仁中學](http://www.fjsh.cy.edu.tw)

[Category:嘉義市中學](../Category/嘉義市中學.md "wikilink")
[Category:台灣私立高級中學](../Category/台灣私立高級中學.md "wikilink")
[Category:台灣天主教會學校](../Category/台灣天主教會學校.md "wikilink")
[Category:1931年創建的教育機構](../Category/1931年創建的教育機構.md "wikilink")
[Category:1962年創建的教育機構](../Category/1962年創建的教育機構.md "wikilink")
[Category:東區 (嘉義市)](../Category/東區_\(嘉義市\).md "wikilink")

1.

2.  [輔仁中學同學會暌違40年再聚首](http://news.ltn.com.tw/news/local/paper/967766),自由時報

3.