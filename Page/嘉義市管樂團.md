**嘉義市管樂團（Chia Yi Wind
Orchestra）**成立於1994年1月，以推廣本土音樂為目標，經常蒐集本土歌謠作家之旋律，以管樂團編制改寫配器，巡迴全國為民眾服務，每場演出均受到各地民眾熱烈迴響與讚許。

## 演出

  - 每年固定有兩次定期音樂會，一次於夏季，一次於[嘉義市管樂節](../Page/嘉義市管樂節.md "wikilink")。
  - 「藝術歸鄉」活動，巡迴[嘉義市各社區及校園](../Page/嘉義市.md "wikilink")，將音樂的歡樂帶給每一位市民。

### 海外演出

  - 1996年 [香港](../Page/香港.md "wikilink")
  - 1998年 [香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")
  - 1999年 [日本](../Page/日本.md "wikilink")
  - 2004年 [中國大陸](../Page/中國大陸.md "wikilink")
  - 2005年 [菲律賓](../Page/菲律賓.md "wikilink")
  - 2006年 [韓國](../Page/韓國.md "wikilink")
  - 2010年 [韓國](../Page/韓國.md "wikilink")

## 音樂總監

  - [曾膺安](../Page/曾膺安.md "wikilink") 1994年-迄今

## 團員

2016年6月22日

<table style="width:35%;">
<colgroup>
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/長笛.md" title="wikilink">長笛</a></dt>

</dl>
<p><a href="../Page/殷瑞霙.md" title="wikilink">殷瑞霙</a>、<a href="../Page/林珍葉.md" title="wikilink">林珍葉</a>、<a href="../Page/張仁傑.md" title="wikilink">張仁傑</a>、<a href="../Page/毛嘉婧.md" title="wikilink">毛嘉婧</a>、<a href="../Page/吳牧萱.md" title="wikilink">吳牧萱</a></p>
<dl>
<dt><a href="../Page/雙簧管.md" title="wikilink">雙簧管</a></dt>

</dl>
<p><a href="../Page/黃綉清.md" title="wikilink">黃綉清</a>、<a href="../Page/古書存.md" title="wikilink">古書存</a></p>
<dl>
<dt><a href="../Page/豎笛.md" title="wikilink">豎笛</a></dt>

</dl>
<p><a href="../Page/李吉峰.md" title="wikilink">李吉峰</a>、<a href="../Page/蕭芳照.md" title="wikilink">蕭芳照</a>、<a href="../Page/林慧婷.md" title="wikilink">林慧婷</a>、<a href="../Page/曹淑芬.md" title="wikilink">曹淑芬</a>、<a href="../Page/黃信夫.md" title="wikilink">黃信夫</a>、<a href="../Page/楊佩珊.md" title="wikilink">楊佩珊</a>、<a href="../Page/林蘊秋.md" title="wikilink">林蘊秋</a>、<a href="../Page/黃馨嬋.md" title="wikilink">黃馨嬋</a>、<a href="../Page/劉宏輝.md" title="wikilink">劉宏輝</a>、<a href="../Page/翁怡欣.md" title="wikilink">翁怡欣</a>、<a href="../Page/蕭伊婷.md" title="wikilink">蕭伊婷</a></p>
<dl>
<dt><a href="../Page/薩克管.md" title="wikilink">薩克管</a></dt>

</dl>
<p><a href="../Page/鄭鈞元.md" title="wikilink">鄭鈞元</a>、<a href="../Page/賴姿瑜.md" title="wikilink">賴姿瑜</a>、<a href="../Page/黃玫瑄.md" title="wikilink">黃玫瑄</a>、<a href="../Page/黃菡婗.md" title="wikilink">黃菡婗</a>、<a href="../Page/吳欣玫.md" title="wikilink">吳欣玫</a>、<a href="../Page/翁孟筠.md" title="wikilink">翁孟筠</a></p>
<dl>
<dt><a href="../Page/法國號.md" title="wikilink">法國號</a></dt>

</dl>
<p><a href="../Page/王怡文.md" title="wikilink">王怡文</a>、<a href="../Page/鄭雋澔.md" title="wikilink">鄭雋澔</a>、<a href="../Page/丁靜秀.md" title="wikilink">丁靜秀</a></p>
<dl>
<dt><a href="../Page/小號.md" title="wikilink">小號</a></dt>

</dl>
<p><a href="../Page/楊千逸.md" title="wikilink">楊千逸</a>、<a href="../Page/黃朝偉.md" title="wikilink">黃朝偉</a>、<a href="../Page/吳冠霖.md" title="wikilink">吳冠霖</a>、<a href="../Page/楊秉驊.md" title="wikilink">楊秉驊</a>、<a href="../Page/裴一中.md" title="wikilink">裴一中</a>、<a href="../Page/施文源.md" title="wikilink">施文源</a>、<a href="../Page/賴忠瑩.md" title="wikilink">賴忠瑩</a>、<a href="../Page/林志學.md" title="wikilink">林志學</a></p>
<dl>
<dt><a href="../Page/長號.md" title="wikilink">長號</a></dt>

</dl>
<p><a href="../Page/陳建州.md" title="wikilink">陳建州</a>、<a href="../Page/林中伍.md" title="wikilink">林中伍</a>、<a href="../Page/邱偉倫.md" title="wikilink">邱偉倫</a>、<a href="../Page/簡晟軒.md" title="wikilink">簡晟軒</a>、<a href="../Page/姚威旭.md" title="wikilink">姚威旭</a>、<a href="../Page/張建昇.md" title="wikilink">張建昇</a></p>
<dl>
<dt><a href="../Page/上低音號.md" title="wikilink">上低音號</a></dt>

</dl>
<p><a href="../Page/蕭學銓.md" title="wikilink">蕭學銓</a>、<a href="../Page/陳佩欣.md" title="wikilink">陳佩欣</a></p>
<dl>
<dt><a href="../Page/低音號.md" title="wikilink">低音號</a></dt>

</dl>
<p><a href="../Page/翁啟榮.md" title="wikilink">翁啟榮</a>、<a href="../Page/詹金諭.md" title="wikilink">詹金諭</a>、<a href="../Page/楊士進.md" title="wikilink">楊士進</a></p>
<dl>
<dt><a href="../Page/打擊樂器.md" title="wikilink">打擊樂器</a></dt>

</dl>
<p><a href="../Page/陳郁毓.md" title="wikilink">陳郁毓</a>、<a href="../Page/陳正宜.md" title="wikilink">陳正宜</a>、<a href="../Page/錢威穎.md" title="wikilink">錢威穎</a>、<a href="../Page/李孟萱.md" title="wikilink">李孟萱</a>、<a href="../Page/劉乃諠.md" title="wikilink">劉乃諠</a>、<a href="../Page/倪詩婷.md" title="wikilink">倪詩婷</a>、<a href="../Page/徐慶獻.md" title="wikilink">徐慶獻</a>、<a href="../Page/李瑾佑.md" title="wikilink">李瑾佑</a>、<a href="../Page/鄧杰翔.md" title="wikilink">鄧杰翔</a></p></td>
</tr>
</tbody>
</table>

樂團細節請拜訪官網：http://www.cywo.org.tw

## 姊妹團

  - 韓國[原州市](../Page/原州.md "wikilink")[Appassionata管樂團](../Page/Appassionata管樂團.md "wikilink")

[Category:管樂團](../Category/管樂團.md "wikilink")
[Category:台灣管樂團](../Category/台灣管樂團.md "wikilink")