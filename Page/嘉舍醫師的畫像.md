《**嘉舍醫師的畫像**》（[荷蘭文](../Page/荷蘭文.md "wikilink")：）是[荷蘭](../Page/荷蘭.md "wikilink")[後期印象派畫家](../Page/後期印象派.md "wikilink")[文森·梵谷](../Page/文森·梵谷.md "wikilink")（Vincent
van
Gogh）最著名的作品之一，繪於1890年，當時他已住進[精神病院接受](../Page/精神病院.md "wikilink")[保羅·嘉舍](../Page/保羅·嘉舍.md "wikilink")（Paul
Gachet）醫師的治療。1990年5月15日，此畫以8250萬[美金創下有史以來藝術品](../Page/美金.md "wikilink")[拍賣](../Page/拍賣.md "wikilink")[最高價格](../Page/最昂贵画作列表.md "wikilink")。此畫有两个版本，第一个版本，现私人收藏；第二个版本的目前存放在[巴黎的](../Page/巴黎.md "wikilink")[奧塞美術館中](../Page/奧塞美術館.md "wikilink")\[1\]。

## 参考文献

## 外部連結

  - [*U.S. News and World Report*: Van Gogh's vanishing
    act](http://www.usnews.com/usnews/doubleissue/mysteries/portrait.htm)

[梵](../Category/1890年畫作.md "wikilink")
[Category:梵高的繪畫作品](../Category/梵高的繪畫作品.md "wikilink")
[Category:梵高肖像画](../Category/梵高肖像画.md "wikilink")

1.  Saltzman, Cynthia: Portrait of Dr. Gachet. The Story of a van Gogh
    Masterpiece: Money, Politics, Collectors, Greed, and Loss, . ISBN
    0-670-86223-1