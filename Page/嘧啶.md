**嘧啶**（****，音同「密定」，英語：**Pyrimidine**）為1,3-二氮杂苯，是一种[杂环化合物](../Page/杂环化合物.md "wikilink")。嘧啶由2个[氮原子取代](../Page/氮.md "wikilink")[苯分子间位上的](../Page/苯.md "wikilink")2个[碳形成](../Page/碳.md "wikilink")，是一种[二嗪](../Page/二嗪.md "wikilink")。和[吡啶一样](../Page/吡啶.md "wikilink")，嘧啶保留了[芳香性](../Page/芳香性.md "wikilink")。

## 嘧啶与核酸

形成[DNA和](../Page/DNA.md "wikilink")[RNA的五种](../Page/RNA.md "wikilink")[碱基中](../Page/碱基.md "wikilink")，有三种是嘧啶的衍生物：[胞嘧啶](../Page/胞嘧啶.md "wikilink")（**C**ytosine）、[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")（**T**hymine）、[尿嘧啶](../Page/尿嘧啶.md "wikilink")（**U**racil）。

[File:Cytosin.svg|胞嘧啶](File:Cytosin.svg%7C胞嘧啶)
[File:Thymin.svg|胸腺嘧啶](File:Thymin.svg%7C胸腺嘧啶)
[File:Uracil.svg|尿嘧啶](File:Uracil.svg%7C尿嘧啶)

其中胸腺嘧啶只能出现在脱氧核糖核酸中，尿嘧啶只能出现在核糖核酸中，而胞嘧啶两者均可。在[碱基互补配对时](../Page/碱基互补配对.md "wikilink")，胸腺嘧啶或尿嘧啶与[腺嘌呤以](../Page/腺嘌呤.md "wikilink")2个[氢键结合](../Page/氢键.md "wikilink")，胞嘧啶与[鸟嘌呤以](../Page/鸟嘌呤.md "wikilink")3个氢键结合。

## 相关条目

  - [杂环化合物](../Page/杂环化合物.md "wikilink")
  - [芳香性](../Page/芳香性.md "wikilink")
  - [吡啶](../Page/吡啶.md "wikilink")
  - [哒嗪](../Page/哒嗪.md "wikilink")（1,2-二氮杂苯） -
    [吡嗪](../Page/吡嗪.md "wikilink")（1,4-二氮杂苯）
  - [DNA](../Page/DNA.md "wikilink") - [RNA](../Page/RNA.md "wikilink")
  - [嘌呤](../Page/嘌呤.md "wikilink")

[Category:生物分子](../Category/生物分子.md "wikilink")
[嘧啶](../Category/嘧啶.md "wikilink")
[Category:芳香族碱](../Category/芳香族碱.md "wikilink")