[DNA_UV_mutation.svg](https://zh.wikipedia.org/wiki/File:DNA_UV_mutation.svg "fig:DNA_UV_mutation.svg")

**嘧啶[二聚体](../Page/二聚体.md "wikilink")**（pyrimidine
dimer，简称PD），是[DNA或](../Page/DNA.md "wikilink")[RNA中的相邻](../Page/RNA.md "wikilink")[碱基](../Page/碱基.md "wikilink")，如[胞嘧啶及](../Page/胞嘧啶.md "wikilink")[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")，在[紫外线的诱导下進行](../Page/紫外线.md "wikilink")[光化学合成](../Page/光化学合成.md "wikilink")，於C=C碳雙鍵生成[共价键而形成的一种化合物](../Page/共价键.md "wikilink")，是[突变产生的原因之一](../Page/突变.md "wikilink")。
\[1\]\[2\]\[3\]在[双链RNA中](../Page/核糖核酸#双链RNA.md "wikilink")，紫外線也可能導致[脲嘧啶二聚體生成](../Page/脲嘧啶.md "wikilink")。紫外線二聚體的常見例子包括[环丁烷嘧啶二聚体及](../Page/环丁烷.md "wikilink")6-4光产物。它們改變了DNA原有結構，令[聚合酶無法正常運作](../Page/聚合酶.md "wikilink")，DNA無法複製。嘧啶二聚体可通過[光致活作用或](../Page/光致活作用.md "wikilink")[核苷酸切除修复的作用來修復](../Page/核苷酸切除修复.md "wikilink")。如果最終無法修復，可引致[突變](../Page/突變.md "wikilink")。

## 二聚体的種類

[Thymine_photodimer.svg](https://zh.wikipedia.org/wiki/File:Thymine_photodimer.svg "fig:Thymine_photodimer.svg")光化二聚體。|
[胸腺嘧啶光化二聚體的例子包括](../Page/胸腺嘧啶.md "wikilink"):6-4光产物（左）及[環丁烷](../Page/環丁烷.md "wikilink")（右）\]\]
[環丁烷嘧啶二聚体](../Page/環丁烷.md "wikilink")(CPD)由嘧啶的C=C雙鍵的[偶联反应所生成](../Page/偶联反应.md "wikilink")，共有四個環。
\[4\]\[5\]\[6\] 它們影響了[DNA复制時的鹼基配對](../Page/DNA复制.md "wikilink")，導致突變。

6-4光产物又稱6,4嘧啶-嘧啶酮。它的生成率只有[環丁烷嘧啶二聚体的三分之一](../Page/環丁烷.md "wikilink")，但有更大可能引發突變。\[7\][孢子光產物溶酶為修複](../Page/孢子光產物溶酶.md "wikilink")[胸腺嘧啶二聚體的另一途徑](../Page/胸腺嘧啶.md "wikilink")。\[8\]

## 誘變

跨損傷[聚合酶常令嘧啶二聚体轉化為突變](../Page/聚合酶.md "wikilink")。這種反應於[原核生物](../Page/原核生物.md "wikilink")（[SOS反应](../Page/SOS反应.md "wikilink")）及真核生物中都存在。雖然胸腺嘧啶二聚體是在紫外線損傷中較為常見，但因跨損傷聚合酶較常以[腺嘌呤修復DNA](../Page/腺嘌呤.md "wikilink")，故胸腺嘧啶二聚體通常能正確修複。反之，環丁烷型嘧啶二聚体中的[胞嘧啶則易受](../Page/胞嘧啶.md "wikilink")[脫氨作用攻擊](../Page/脫氨作用.md "wikilink")，轉化為胸腺嘧啶。\[9\]

## DNA修復

[melanoma.jpg](https://zh.wikipedia.org/wiki/File:melanoma.jpg "fig:melanoma.jpg")，一種皮膚癌\]\]
嘧啶二聚體會導致DNA的局部構象改變，故可被修復酶察覺。\[10\]在絕大部分的生物中，損害亦可借由光致活作用修複，但諸如人類的[胎盤動物則無此機制](../Page/胎盤動物.md "wikilink")。\[11\]光致活作用中，[光裂合酶將環丁烷嘧啶二聚体直接以](../Page/光裂合酶.md "wikilink")[光化学作用還原](../Page/光化学.md "wikilink")。修復酶察覺損傷後，會吸收波長\>300 nm的光線（即荧光及紫外光），令光化学作用產生，將二聚体還原。\[12\]

[核苷酸切除修复則是更通用的DNA維修方式](../Page/核苷酸切除修复.md "wikilink")。在這過程中，環丁烷嘧啶二聚体被切除，並合成新的DNA來填補該區域。\[13\][著色性乾皮症正是因患者無法進行核苷酸切除修复而導致的遺傳病](../Page/著色性乾皮症.md "wikilink")，患者皮膚細胞被紫外光破壞後無法修復，導致皮膚變色及誘發癌變。人類中，沒有修復的嘧啶二聚體可引致黑色素瘤。\[14\]

## 参考文献

[M](../Page/category:分子生物学.md "wikilink")

[Category:DNA](../Category/DNA.md "wikilink")
[Category:突變](../Category/突變.md "wikilink")
[Category:二聚体](../Category/二聚体.md "wikilink")
[Category:衰老](../Category/衰老.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12. Friedberg, Errol C. (23 January 2003) "DNA Damage and Repair".
    *Nature* 421, 436-439.

13.
14.