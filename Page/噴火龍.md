**噴火龍**是[神奇寶貝系列登場的](../Page/神奇寶貝.md "wikilink")種虛構[角色](../Page/角色.md "wikilink")（[怪獸](../Page/怪物.md "wikilink")）中的一種。

雖然噴火龍的中文名字中有龍，但噴火龍並不是龍屬性的神奇寶貝（而是火屬性與飛行屬性），由噴火龍的日文名（リザードン）與英文名（Charizard）可以發現這隻神奇寶貝的名稱來自於「蜥蜴」，也只有中文版命名將其稱為龍。

## 特徵

噴火龍擁有橙色的身軀，腹部和腳底是奶油黃，雙翼的正面是湖水色的藍綠而背面是橙色，眼睛亦是湖水色的藍綠。

噴火龍的頭部有兩隻向後延伸的角，雙爪和腳上都有3根指甲。牠還擁有一雙寬大的翅膀和長長的尾巴，尾巴末端像小火龍和火恐龍一樣燃燒著象徵健康和生命的火焰，在生命力下降時火焰會減弱。
噴火龍本身並不帶有龍屬性，而是火屬性和飛行屬性的，能學習許多火屬性和飛行屬性的技能。但噴火龍本身具有西方龍的外表，所以牠能學會許多龍屬性技能。

噴火龍的競爭心十分強烈，會花大量的時間鍛煉自己、尋找對手。牠十分高傲，除非訓練師命令或是挑釁，否則絕不會在一個較弱的對手面前吐出火焰。根據圖鑒的記錄，在噴火龍憤怒的時候，牠尾巴的火焰會變成藍白色；在經歷艱苦的戰鬥之後，牠的火焰溫度會升高；噴火龍擅長火焰和飛行，牠能飛到一千四百公尺高，吐出的火焰足以熔化岩石。不過噴火龍的火焰有時會引發森林大火。

## [遊戲中的噴火龍](../Page/神奇寶貝遊戲列表.md "wikilink")

起初出現於精靈寶可夢紅／綠版，噴火龍為火屬性及飛行屬性，是火焰[寶可夢](../Page/精靈寶可夢.md "wikilink")，普通版編號為006，從[火恐龍進化而成](../Page/火恐龍.md "wikilink")，特性為[猛火](../Page/猛火.md "wikilink")，夢特性為太陽之力。

噴火龍在自[精靈寶可夢X/精靈寶可夢Y之後](../Page/精靈寶可夢_X·Y.md "wikilink")，可以使用超級石進行超級進化，成為超級噴火龍X（屬性：火屬性、龍屬性）/超級噴火龍Y（屬性：火屬性、飛行屬性）。

噴火龍的特徵是擁有火紅色（亦可以說是橙色）的身軀、奶油黃的腹部及腳底、湖水色的藍綠雙翼及長長的帶著火焰的尾巴。

## 動畫中的噴火龍

動畫版中，主角小智有一隻噴火龍，因為等級過高而拒絕服從命令一陣子，但是在之後成為隊伍中最可靠的夥伴之一。

[ca:Línia evolutiva de
Charmander\#Charizard](../Page/ca:Línia_evolutiva_de_Charmander#Charizard.md "wikilink")
[cs:Seznam pokémonů
(1-20)\#Charizard](../Page/cs:Seznam_pokémonů_\(1-20\)#Charizard.md "wikilink")
[da:Pokémon
(1-20)\#Charizard](../Page/da:Pokémon_\(1-20\)#Charizard.md "wikilink")
[fr:Salamèche et ses
évolutions\#Dracaufeu](../Page/fr:Salamèche_et_ses_évolutions#Dracaufeu.md "wikilink")
[pl:Lista Pokémonów
(1-20)\#Charizard](../Page/pl:Lista_Pokémonów_\(1-20\)#Charizard.md "wikilink")
[pt:Família de
Charmander\#Charizard](../Page/pt:Família_de_Charmander#Charizard.md "wikilink")

[Category:第一世代寶可夢](../Category/第一世代寶可夢.md "wikilink")
[Category:火屬性寶可夢](../Category/火屬性寶可夢.md "wikilink")
[Category:飛行屬性寶可夢](../Category/飛行屬性寶可夢.md "wikilink")
[Category:虛構蜥蜴](../Category/虛構蜥蜴.md "wikilink")
[Category:1996年推出的電子遊戲角色](../Category/1996年推出的電子遊戲角色.md "wikilink")