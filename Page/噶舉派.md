**噶举派**（）[藏传佛教的一个派别](../Page/藏传佛教.md "wikilink")，藏语“噶举”中的“噶”字本意指口，而“举”字则意为传。故“噶举派”一词可理解为口传宗派。另外，由于噶举派僧人的僧裙中加有白色条纹，又俗称“**白教**”。噶举派是[后弘期](../Page/后弘期.md "wikilink")[西藏](../Page/西藏.md "wikilink")[佛教的重要宗派之一](../Page/佛教.md "wikilink")，由[马尔巴创立](../Page/马尔巴.md "wikilink")，承传至今。是西藏历史上最早实行[活佛](../Page/活佛.md "wikilink")（朱沽或祖古）[转世制度的派别](../Page/转世.md "wikilink")，最早的[再來人](../Page/再來人.md "wikilink")（活佛）即[噶玛噶举派的](../Page/噶玛噶举派.md "wikilink")[噶玛巴](../Page/噶玛巴.md "wikilink")。

其在西藏以不重著述而重视实际的修行最具特色，强调刻苦的修行，造就了如[密勒日巴等众多实践苦行修炼的高僧](../Page/密勒日巴.md "wikilink")。

噶举派所最核心的修法是《[那洛六法](../Page/那洛六法.md "wikilink")》与显、密两种[大手印的教授](../Page/大手印.md "wikilink")，其中尤以大手印教授最为著名。

[Marpa_painting_Holy_isle.jpg](https://zh.wikipedia.org/wiki/File:Marpa_painting_Holy_isle.jpg "fig:Marpa_painting_Holy_isle.jpg")\]\]

## 概述

噶舉派開宗立派于11世纪，相当于[中国的](../Page/中国.md "wikilink")[北宋年間](../Page/北宋.md "wikilink")。西藏被[蒙古帝国攻佔](../Page/蒙古帝国.md "wikilink")，后隶属于[元朝](../Page/元朝.md "wikilink")，明初白教盛极一時，全盤掌管了西藏政教大權，但歷代[大寶法王均向中國朝廷進貢](../Page/大寶法王.md "wikilink")，避免與實力龐大的[中國對抗](../Page/中國.md "wikilink")。

而就在明朝永樂年間，[青海藏人](../Page/青海.md "wikilink")[宗喀巴](../Page/宗喀巴.md "wikilink")，他進入[雪山](../Page/雪山.md "wikilink")[閉關修行](../Page/閉關.md "wikilink")，以[噶當派教義為主](../Page/噶當派.md "wikilink")，融合各宗各派，破關而出即創立[格魯派](../Page/格魯派.md "wikilink")，喇嘛均戴黃帽，故俗稱黃教。宗喀巴圓寂，座前兩大弟子[根敦朱巴与](../Page/根敦朱巴.md "wikilink")[克主傑的法脈逐漸興盛](../Page/克主傑.md "wikilink")，[根敦朱巴的繼承人號稱](../Page/根敦朱巴.md "wikilink")[達賴](../Page/達賴.md "wikilink")，統治[前藏](../Page/前藏.md "wikilink")；[克主傑的繼承人則號稱](../Page/克主傑.md "wikilink")[班禪](../Page/班禪.md "wikilink")，統治[後藏](../Page/後藏.md "wikilink")。白教執掌西藏三百年間，與[藏巴汗等三大勢力迫害格魯派](../Page/藏巴汗.md "wikilink")，但最終還是未能阻止格魯派的興起，最後格魯派取代噶舉派統治。

黃教空前鼎盛，成為第一大教派，從白教手中接管了全藏的政教事務。18世纪以后聯合傳統盟友[蒙古人尊](../Page/蒙古人.md "wikilink")[清朝皇帝爲共同宗主](../Page/清朝.md "wikilink")，與征服漢地十八省的[滿洲人建立](../Page/滿洲人.md "wikilink")[君合國關係](../Page/君合國.md "wikilink")。

## 噶舉傳承

此賢劫開始到現在已經有四位佛陀出世。第一尊佛是[拘留孫佛](../Page/拘留孫佛.md "wikilink")。第四尊就是歷史上記載的[佛陀](../Page/佛陀.md "wikilink")－釋迦牟尼佛。他三轉法輪並以八萬四千法門教導眾生，這些法門仍被延續至今。寺院[噶瑪三乘法輪中心的創立便是要弘揚這些教法](../Page/噶瑪三乘法輪中心.md "wikilink")，利益所有的眾生。

佛陀的教法被分為各種不同的「乘」，或說是修習的層次，有分為三乘的(依次为[小乘佛教](../Page/小乘佛教.md "wikilink")、[大乘佛教](../Page/大乘佛教.md "wikilink")、[金刚乘佛教](../Page/金刚乘.md "wikilink"))，也有分為九乘的。這其中，金剛乘是最殊勝的，已如陽光似的光輝燦爛。印度這個國家，由於金剛乘教法的普遍宏傳，已出現了很多的修行者及[成就者](../Page/成就者.md "wikilink")。其中較負盛名的有南贍部洲的六大莊嚴、二勝士，八十四位神通士等。

西藏這個地方是一處被[觀世音菩薩所調伏的](../Page/觀世音菩薩.md "wikilink")[道場](../Page/道場.md "wikilink")，因此透過他大願和大悲的力量，而以譯師、法王、成就者、學者等等[化身示現於西藏](../Page/化身.md "wikilink")。所有這些化身之事業，就是完全地將存在印度的佛法傳入西藏，並將佛法譯成西藏的語言。

在那个時代，很多的喇嘛上師參與宏法，因此傳遍了西藏各地，八大佛法傳統就誕生了。這八大派的修行傳承，都是一樣的，它們都包含了完整的佛法。現存有四個主要的傳承：分別是**噶舉派**、[寧瑪派](../Page/寧瑪派.md "wikilink")、[薩迦派和](../Page/薩迦派.md "wikilink")[格魯派](../Page/格魯派.md "wikilink")。
\[1\]

在這些傳承派別之中，最具代表性的傳承大概是[噶瑪巴了](../Page/噶瑪巴.md "wikilink")。這個傳承就是眾所週知的噶舉派，其意味著「訓誡的傳承」或「口耳傳承」。噶舉派起源於當釋迦牟尼佛以報身佛的型式－金剛持或[多-{杰}-羌](../Page/多杰羌佛.md "wikilink")，來傳達密續金剛乘法。因此，噶舉派的傳統一直保持著金剛持的口傳傳承，這個密續傳承從末間斷過。

這個傳承以各種方式流傳至今，其中之一是[遠傳派的傳承](../Page/遠傳派.md "wikilink")，由金剛持傳給了羅佐寧青（寶意菩薩），寶意菩薩再傳給印度的大成就者薩拉哈，薩拉哈傳給首席學者[龍樹菩薩](../Page/龍樹菩薩.md "wikilink")，龍樹菩薩再傳給大成就者沙瓦利巴，沙瓦利巴傳給大成就者[梅紀巴](../Page/梅紀巴.md "wikilink")，梅紀巴再傳到[馬爾巴譯師](../Page/馬爾巴.md "wikilink")。

近傳派的傳承由大成就者[帝洛巴開始](../Page/帝洛巴.md "wikilink")，在他的修行過程中受到許多偉大的成就者、瑜伽士和上師的指導。據說他曾在西[印度一處叫索瑪普利的地方](../Page/印度.md "wikilink")，絲毫不動地修行十二年。經過這段時間的修行，他獲得了成就，能夠面對面地從金剛持那裡接受教導、口傳和灌頂。

帝理巴（988－1069年）就是大家所熟悉的[帝洛巴](../Page/帝洛巴.md "wikilink")。追溯噶举传承，他是第一位人身导师，被尊为[印度噶举传承的创始者](../Page/印度.md "wikilink")。帝洛巴从禅观中直接得到[金刚总持及许多本尊的授记](../Page/金刚总持.md "wikilink")，在未证得大成就之前，也曾经在坟场不动坐禅十二年,他以榨[芝麻及零工为生](../Page/芝麻.md "wikilink")，也因此而得名。

[帝洛巴的继承者是](../Page/帝洛巴.md "wikilink")[那洛巴大学者](../Page/那洛巴.md "wikilink")（1016－1100年），他是印度最重要的大成就者之一。那洛巴将「[六瑜伽](../Page/六瑜伽.md "wikilink")」及「[大手印](../Page/大手印.md "wikilink")」传给了他的伟大弟子[马尔巴](../Page/马尔巴.md "wikilink")（1012－1096年），而后者成为噶举派在[西藏的第一位导师](../Page/西藏.md "wikilink")，其影响力远在未遇到[那洛巴时已被](../Page/那洛巴.md "wikilink")[帝洛巴所预言](../Page/帝洛巴.md "wikilink")。为求对教义的体解，[马尔巴到曼卡尔区的沐估隆镇跟随卓弥罗札娃](../Page/马尔巴.md "wikilink")（922－1073年）学习[梵文](../Page/梵文.md "wikilink")，但后来因为学费太昂贵而离开，并决定到印度。在他三游印度之中，接收了[那洛巴及其他禅修者完整的](../Page/那洛巴.md "wikilink")「[大手印](../Page/大手印.md "wikilink")」教义，之后[马尔巴](../Page/马尔巴.md "wikilink")、卓弥罗札娃及窝在复兴藏传佛教运动中扮演了极重要的角色。

早期的噶举传承称为「马尔巴噶举」，取自[马尔巴的名字](../Page/马尔巴.md "wikilink")。另一噶举传承则是[西藏中部](../Page/西藏.md "wikilink")「香」郡「昌」地发起的，其创始人为穹波纳玖叔林宫波（1002－1096年），他是[那洛巴及那洛巴妹妹](../Page/那洛巴.md "wikilink")[尼估玛的弟子](../Page/尼估玛.md "wikilink")。这一派系称为「[香巴噶举](../Page/香巴噶举.md "wikilink")」，乃取自发源地为名。值得注意的是这两派是噶举传承最早创立的派别。

马尔巴主要的弟子[密勒日巴](../Page/密勒日巴.md "wikilink")（1052－1135年）是一位著名的「西藏瑜伽士」。密勒日巴在接收其上师严厉的教导下，那种坚韧不拔的精神至今仍是无数修行者的借镜，而他即兴所作的道歌「[密勒日巴一万偈](../Page/密勒日巴一万偈.md "wikilink")」更是许多读者自我鼓励及激励的泉源。

[密勒日巴有许多弟子](../Page/密勒日巴.md "wikilink")，其中特别杰出的有两位，他们是[惹琼巴](../Page/惹琼巴.md "wikilink")（1083－1161年）及[冈波巴](../Page/冈波巴.md "wikilink")（1079－1153年）。这两位大师开创了噶举派完整的体制。[惹琼巴创立了](../Page/惹琼巴.md "wikilink")「惹琼噶举」而冈波巴创立了「达波噶举」。冈波巴是达波区人氏（派别之名即是源自于此），是一位著名的[医生](../Page/医生.md "wikilink")，所以也被称为「达波拉杰」意即「来自达波的医生」，而他的全名是「达波达沃匈努」。冈波巴也是「[解脱庄严宝缦](../Page/解脱庄严宝缦.md "wikilink")」的作者，为噶举传承兴起于雪域西藏中最有作为的人物。[帝洛巴](../Page/帝洛巴.md "wikilink")、[那洛巴](../Page/那洛巴.md "wikilink")、[马尔巴](../Page/马尔巴.md "wikilink")、[密勒日巴及](../Page/密勒日巴.md "wikilink")[冈波巴在噶举传承中被尊为](../Page/冈波巴.md "wikilink")「五祖」。

## 噶舉支派

较早的噶举派不应与后期兴起的[噶举四大八小派有所混淆](../Page/噶举四大八小派.md "wikilink")。噶举传承之派系远超于[噶举四大八小派](../Page/噶举四大八小派.md "wikilink")，故「噶举四大八小派」不能作为噶举传承派系唯一的考量。除此之外，噶举传承中尚有[达波](../Page/达波.md "wikilink")、[香巴](../Page/香巴.md "wikilink")、[竹巴](../Page/竹巴.md "wikilink")、[惹琼等等其他分支](../Page/惹琼.md "wikilink")。

噶举传承的「四大派」是由[岡波巴的四大弟子所创立](../Page/岡波巴.md "wikilink")，包括噶玛噶举、采巴噶举、巴融噶举、帕竹噶举。然而[达波噶举却保存著自承的](../Page/达波噶举.md "wikilink")[方丈系统而自成一派](../Page/方丈.md "wikilink")，这与其他十二派是有所不同的。噶举传承的「八小派」是由帕莫竹巴（1100－1170）的主要弟子所创立，包括[直贡噶举](../Page/直贡噶举.md "wikilink")、達隆噶举、[竹巴噶举](../Page/竹巴噶举.md "wikilink")、雅桑噶举、措普噶举、休色噶举、耶巴噶举和玛仓噶举
。[帕木竹巴在西藏推翻了](../Page/帕木竹巴.md "wikilink")[萨迦政权](../Page/萨迦.md "wikilink")，他也跟随了[萨虔昆噶宁波](../Page/萨虔昆噶宁波.md "wikilink")（1092－1158）参学了十二年并获得萨迦派「[道果](../Page/道果.md "wikilink")」的教义。帕莫竹巴对十二支派的影响在于他将「道果」教义融入噶举派「丹雅卓」的教义内。

## 注释

## 相关内容

  - [佛教宗派](../Page/佛教宗派.md "wikilink")
  - [佛教历史](../Page/佛教历史.md "wikilink")
  - [藏传佛教](../Page/藏传佛教.md "wikilink")
  - [帝洛巴](../Page/帝洛巴.md "wikilink")
  - [马尔巴](../Page/马尔巴.md "wikilink")
  - [噶瑪巴](../Page/噶瑪巴.md "wikilink")

## 参见

  - [佛教](../Page/佛教.md "wikilink")
  - [活佛](../Page/活佛.md "wikilink")
  - [转世](../Page/转世.md "wikilink")

[噶舉派](../Category/噶舉派.md "wikilink")

1.  [台灣佛學研究所資料](http://www.buddhanet.com.tw/zfrop/tibet/ggab-33.htm)。