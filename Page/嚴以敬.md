[Fung_Ying_Sin_Koon,_Entrance_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Fung_Ying_Sin_Koon,_Entrance_\(Hong_Kong\).jpg "fig:Fung_Ying_Sin_Koon,_Entrance_(Hong_Kong).jpg")近正門圍牆上有嚴以敬（阿虫）的作品「道法自然得自在」\]\]
**嚴以敬**（\[1\]），[筆名](../Page/筆名.md "wikilink")**-{阿虫}-**，出生於[廣東省](../Page/廣東省_\(中華民國\).md "wikilink")[廣州市](../Page/廣州市_\(中華民國\).md "wikilink")，[香港](../Page/香港.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。2018年8月11日因突發性的[心臟衰竭於](../Page/心臟衰竭.md "wikilink")[美國](../Page/美國.md "wikilink")[洛杉磯家中離世](../Page/洛杉磯.md "wikilink")，享年85歲。\[2\]

## 生平

嚴以敬在[廣州出生並讀小學](../Page/廣州.md "wikilink")，後移居[香港](../Page/香港.md "wikilink")，全家人就住[深水埗一個牀位](../Page/深水埗.md "wikilink")，由於生活貧困，讀了兩年中學就開始出來做工，曾做過學徒、[小販和](../Page/小販.md "wikilink")[礦工](../Page/礦工.md "wikilink")。他無聊時便拿起紙筆畫下來，漸漸培養出繪畫的興趣。到19歲時開始習畫，原為[政治漫畫家](../Page/政治.md "wikilink")，自1984年起他對政治漫畫感到厭倦，移民美國後改以-{阿虫}-為名發表畫作，創作一系列的生活[水墨畫小品](../Page/水墨畫.md "wikilink")，作品刊載於《[快報](../Page/快報.md "wikilink")》及《[天天日報](../Page/天天日報.md "wikilink")》；自嘲「不能成龍，只好成-{虫}-」。\[3\]

2002年，嚴以敬曾於中環[閣麟街某大廈的地下經營畫室](../Page/閣麟街.md "wikilink")，佔地600平方呎，到2008年因無法負擔租金而結業。\[4\]\[5\]

## 參考來源

## 外部連結

  - [聯絡阿虫有關原作及授權事務](http://www.artware.com.hk/)

[Y嚴](../Category/中国画家.md "wikilink") [Y嚴](../Category/广东人.md "wikilink")
[嚴](../Category/香港漫畫家.md "wikilink")
[Category:严姓](../Category/严姓.md "wikilink")

1.
2.

3.

4.

5.