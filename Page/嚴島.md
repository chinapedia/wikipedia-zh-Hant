[Grand_torii_of_Itsukushima_Shrine_3.jpg](https://zh.wikipedia.org/wiki/File:Grand_torii_of_Itsukushima_Shrine_3.jpg "fig:Grand_torii_of_Itsukushima_Shrine_3.jpg")的大鳥居\]\]

**嚴島**（）是位於[日本](../Page/日本.md "wikilink")[瀨戶內海](../Page/瀨戶內海.md "wikilink")西部的[島嶼](../Page/島嶼.md "wikilink")，行政區劃屬於[廣島縣](../Page/廣島縣.md "wikilink")[廿日市市](../Page/廿日市市.md "wikilink")，全島面積約為30[平方公里](../Page/平方公里.md "wikilink")，常住人口約1,600人\[1\]。

嚴島也被稱做**宮島**，為[日本三景之一](../Page/日本三景.md "wikilink")，被稱為**安藝之宮島**；島上最主要的景點是以海上[鳥居聞名的](../Page/鳥居.md "wikilink")[嚴島神社及後方的](../Page/嚴島神社.md "wikilink")[彌山原始林區](../Page/彌山_\(廣島縣\).md "wikilink")，已被列入[世界遺產](../Page/世界遺產.md "wikilink")。現每年有超過300萬人次的觀光客來到島上，在2011年[貓途鷹的日本](../Page/貓途鷹.md "wikilink")「外國人人氣觀光景點」排名第一。

## 歷史

島上的[嚴島神社約建於](../Page/嚴島神社.md "wikilink")6世紀末，12世紀[平安時代末期曾受到](../Page/平安時代.md "wikilink")[平清盛之庇護](../Page/平清盛.md "wikilink")，在[江戶時代中期已成為全日本的知名觀光景點](../Page/江戶時代.md "wikilink")。其主要建物都已被列為日本國家重要文化財產，並收藏有多件來自過去皇族、貴族、武将、商人奉納的美術工藝品或武具。

戰國時代由於位於以[安藝國為根據地的](../Page/安藝國.md "wikilink")[毛利氏及以](../Page/毛利氏.md "wikilink")[周防國為根據地的](../Page/周防國.md "wikilink")[大內氏勢力之間](../Page/大內氏.md "wikilink")，雙方於1555年在此發生[嚴島之戰](../Page/嚴島之戰.md "wikilink")，最終毛利氏在此取得勝利，並進一步成為統治中部地方多達10國的大名。

1587年[豐臣秀吉統一日本後](../Page/豐臣秀吉.md "wikilink")，在此建立大規模的大經堂，其本堂用了多達857張榻榻米，也因此被稱為「」，現被作為嚴島神社的[攝末社豐國神社本殿](../Page/攝末社.md "wikilink")。

[明治維新後](../Page/明治維新.md "wikilink")，曾任日本第一任內閣總理大臣的[伊藤博文曾說](../Page/伊藤博文.md "wikilink")「日本三景之一的真正價值要從彌山山點眺望」，為了方便前往島上的參拜客和觀光客，也因此在明治時代後期便有可以登上彌山的登山道路，1900年前往嚴島的定期渡輪也開始營運。

全島區域在1934年被劃入[瀨戶內海國立公園](../Page/瀨戶內海國立公園.md "wikilink")，成為自然公園法規定的特別保護區。1952年被日本政府列為[日本文化遺產](../Page/日本文化遺產.md "wikilink")、特別史跡和特別名勝，島上的原始林也被列為國家的自然保護植物。

1889年實施[町村制時](../Page/町村制.md "wikilink")，島上區域設立[嚴島町](../Page/宮島町.md "wikilink")，1950年更名為「宮島町」，2005年被併入[廿日市市](../Page/廿日市市.md "wikilink")。

## 自然景觀

[Miyajima13.jpg](https://zh.wikipedia.org/wiki/File:Miyajima13.jpg "fig:Miyajima13.jpg")
[ItsukushimaDeer7389.jpg](https://zh.wikipedia.org/wiki/File:ItsukushimaDeer7389.jpg "fig:ItsukushimaDeer7389.jpg")

由於嚴島被視為女神所居住的島嶼，嚴島上從過去就是禁止砍伐[樹木](../Page/樹木.md "wikilink")，因此島上至今得以維持完整的[森林環境](../Page/森林.md "wikilink")。島上有許多[楓樹](../Page/楓樹.md "wikilink")，在秋天會佈滿美麗的紅葉，可以搭乘[纜車俯視](../Page/纜車.md "wikilink")[紅葉谷](../Page/紅葉谷.md "wikilink")，同時前往[標高](../Page/標高.md "wikilink")535[公尺的](../Page/公尺.md "wikilink")[彌山山頂](../Page/彌山.md "wikilink")。

島上有許多野生的[鹿和](../Page/鹿.md "wikilink")[猿猴](../Page/猿猴.md "wikilink")，在過去的文獻中島上就一直有關於鹿的記載，這些鹿被認為是神的使者，目前島上的鹿估計約有600隻；但在江戶時代以前的紀錄中並沒有猴子的存在，推測現在的猿猴群是由人帶到島上繁衍而成。

在海邊的沙灘上，可以在退潮時看見許多[寄居蟹](../Page/寄居蟹.md "wikilink")。

## 觀光

[宫岛.JPG](https://zh.wikipedia.org/wiki/File:宫岛.JPG "fig:宫岛.JPG")

嚴島除了嚴島神社外，還有以[大聖院為首的許多佛閣](../Page/大聖院.md "wikilink")。另外有[嚴島神社寶物館](../Page/嚴島神社寶物館.md "wikilink")、[宮島水族館](../Page/宮島水族館.md "wikilink")、[宮島歷史民俗資料館](../Page/宮島歷史民俗資料館.md "wikilink")、[廣島大學理學研究所附屬自然植物實驗所](../Page/廣島大學.md "wikilink")、[宮島町傳統產業會館等文化設施供遊客參觀](../Page/宮島町傳統產業會館.md "wikilink")。每年到訪嚴島的遊客高達三百萬人，因此嚴島有不少旅館，這些旅館主要集中在宮島棧橋至嚴島神社的道路旁。

島上的主要特產為[紅葉饅頭及](../Page/紅葉饅頭.md "wikilink")[杓文字等](../Page/杓文字.md "wikilink")。在每年的8月14日舉行水上[煙火大會](../Page/煙火大會.md "wikilink")。

### 嚴島神社

由於地理位置獨特及秀麗景致，嚴島自古就被認為是女神的御神體，嚴島之名即來自市杵島姫命（）。嚴島神社的創建時間並沒有明確的記載，但一般認為是在[推古天皇即位當年](../Page/推古天皇.md "wikilink")（593年）由安藝國的有力豪族[佐伯鞍職所創建](../Page/佐伯鞍職.md "wikilink")。\[2\]。在[平安時代](../Page/平安時代.md "wikilink")，由於[平氏和](../Page/平氏.md "wikilink")[源氏對神社的支持](../Page/源氏.md "wikilink")，使許多[京都的皇親貴族前來參拜](../Page/京都.md "wikilink")，並引進[平安文化](../Page/平安文化.md "wikilink")。[江戶時代以後](../Page/江戶時代.md "wikilink")，在[明治政府與](../Page/明治.md "wikilink")[廣島藩的支持及保護下](../Page/廣島藩.md "wikilink")，前來島上神社的香客絡繹不絕。今日的嚴島神社是日本最重要的觀光景點之一。

## 文化與習俗

[Tsukushima_roast_oyster.JPG](https://zh.wikipedia.org/wiki/File:Tsukushima_roast_oyster.JPG "fig:Tsukushima_roast_oyster.JPG")

基於信仰的理由，嚴島擁有許多獨特的習俗，這些習俗流傳至今。

嚴島自古被視為女神的御神體，為清淨之神域，因此對血、死亡等穢有所避忌。例如：

  - 島上的死者均埋葬於對岸的[赤崎](../Page/赤崎.md "wikilink")。
  - 一直到今日島上仍沒有建築任何的[墓地](../Page/墳墓.md "wikilink")。
  - 島上的女性在快要[分娩時](../Page/分娩.md "wikilink")，會去到本州的對岸分娩。分娩後的一百天才會回到島上。
  - 女性經期時要到特設的町內小屋暫時隔離。

除此之外，島上嚴禁農耕及織布的行動。而島上的商家及居民，則有去大鳥居所在的海濱取水，清潔屋門的習慣。另外，由於島上的鹿被視為女神的使者，因此島上嚴禁傷害鹿。島內亦嚴禁飼養犬隻，從國內其他地方來的犬隻則要被送到本州的對岸放生。

## 交通

[宫岛渡轮.JPG](https://zh.wikipedia.org/wiki/File:宫岛渡轮.JPG "fig:宫岛渡轮.JPG")

搭乘[廣島電鐵](../Page/廣島電鐵.md "wikilink")到終點，或是搭乘[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[山陽本線到](../Page/山陽本線.md "wikilink")[宮島口車站](../Page/宮島口車站.md "wikilink")，再由搭乘廣電經營的或是西日本旅客鐵道經營的[宮島連絡船渡輪](../Page/宮島連絡船.md "wikilink")，約10到15分鐘可到達[宮島棧橋](../Page/宮島棧橋.md "wikilink")；在前往宮島港時，船隻會行駛靠近嚴島神社的路線，滿潮時還可靠近大鳥居。

島內由於道路非常窄，加上有大量遊客和鹿，而主要景點嚴島神社也在港口的步行範圍內，島上住宿點也會提供接送服務，因此甚少有遊客自行駕車渡海前往。

在連續假日、新年參拜、水上煙火大會期間以及秋末的旅遊旺季中，宮島口周邊常因大量遊客的湧入而造成交通阻塞，因此有提供廣島市和廿日市市之間的接駁車協助乘客往返。

## 圖片集

[File:MiyajimaSenjokaku7474.jpg|嚴島神社](File:MiyajimaSenjokaku7474.jpg%7C嚴島神社)
末社豐國神社本殿「千畳閣」 [File:日本严岛神社古塔.JPG|严岛五重塔](File:日本严岛神社古塔.JPG%7C严岛五重塔)
[File:MiyajimaRestroomDeer.jpg|島上公共廁所及鹿](File:MiyajimaRestroomDeer.jpg%7C島上公共廁所及鹿)
[File:MiyajimaKatsuyamajoMarker7451.jpg|勝山城遺跡石碑](File:MiyajimaKatsuyamajoMarker7451.jpg%7C勝山城遺跡石碑)

## 參考資料

## 相關條目

  - [日本三景](../Page/日本三景.md "wikilink")

  -
  -
  - [宮島航路](../Page/宮島航路.md "wikilink")

## 外部連結

  - [宮島觀光官方網頁](http://www.miyajima-wch.jp/)

  - [宮島觀光協會](http://www.miyajima.or.jp/)

[Category:特別史跡](../Category/特別史跡.md "wikilink")
[Category:拉姆薩公約登錄地](../Category/拉姆薩公約登錄地.md "wikilink")
[Category:安藝國](../Category/安藝國.md "wikilink")
[Category:嚴島](../Category/嚴島.md "wikilink")
[Category:平成百景](../Category/平成百景.md "wikilink")
[Category:日本世界遺產](../Category/日本世界遺產.md "wikilink")
[Category:廣島縣史跡](../Category/廣島縣史跡.md "wikilink")
[Category:靈場巡禮](../Category/靈場巡禮.md "wikilink")
[Category:神佛習合](../Category/神佛習合.md "wikilink")

1.
2.  宮島民俗歷史資料館 - 宮島的略年表
    <http://www.hiroshima-cdas.or.jp/home/okazaki/jpn/nenpyo/nenpyo.html>