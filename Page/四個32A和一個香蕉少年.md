，乃1996年上映的[香港電影](../Page/香港電影.md "wikilink")。

## 內容

成年後的Minnie（[李麗珍飾](../Page/李麗珍.md "wikilink")）在其婚禮上，與自小已是好友的Pat（[莫文蔚飾](../Page/莫文蔚.md "wikilink")）、天文臺（[李蕙敏飾](../Page/李蕙敏.md "wikilink")）及高妹（[王馨平飾](../Page/王馨平.md "wikilink")）再次眾首，一同回味過去的青蔥歲月……

## 幕後人員

  - 導演：[曾志偉](../Page/曾志偉.md "wikilink")
  - 編劇：[阮世生](../Page/阮世生.md "wikilink")、[黃婉妍](../Page/黃婉妍.md "wikilink")、[林愛華](../Page/林愛華.md "wikilink")
  - 攝影：[陳兆君](../Page/陳兆君.md "wikilink")

## 演員

  - [李麗珍](../Page/李麗珍.md "wikilink") ......Minnie
  - [王馨平](../Page/王馨平.md "wikilink") ......高妹
  - [李蕙敏](../Page/李蕙敏.md "wikilink") ......天文臺
  - [莫文蔚](../Page/莫文蔚.md "wikilink") ......Pat
  - [江麗娜](../Page/江麗娜.md "wikilink") ......少女Minnie
  - [文頌嫻](../Page/文頌嫻.md "wikilink") ......少女Pat
  - [蔡詩敏](../Page/蔡詩敏.md "wikilink") ......少女天文臺
  - [何嘉莉](../Page/何嘉莉.md "wikilink") ......少女高妹
  - [張智霖](../Page/張智霖.md "wikilink") ......陳老師
  - [黃寶龍](../Page/黃寶龍.md "wikilink") ......Mickey
  - [滕麗名](../Page/滕麗名.md "wikilink") ......Jackie
  - [顧紀筠](../Page/顧紀筠.md "wikilink") ......Minnie媽
  - [曾志偉](../Page/曾志偉.md "wikilink") ......Minnie爸
  - [何超儀](../Page/何超儀.md "wikilink") ......Minnie大姊
  - [徐濠縈](../Page/徐濠縈.md "wikilink") ......Minnie二姊
  - [吳家樂](../Page/吳家樂.md "wikilink") ......Andy Wong
  - [白嘉倩](../Page/白嘉倩.md "wikilink") ...... Miss Lee
  - [鄧兆尊](../Page/鄧兆尊.md "wikilink")
  - [陳小春](../Page/陳小春.md "wikilink") ......Super
  - [錢嘉樂](../Page/錢嘉樂.md "wikilink")
  - [鄧一君](../Page/鄧一君.md "wikilink") ......高妹前男友
  - [張崇德](../Page/張崇德.md "wikilink")
  - [潘芳芳](../Page/潘芳芳.md "wikilink") ......婦科醫生
  - [上山安娜](../Page/上山安娜.md "wikilink")
  - [張之亮](../Page/張之亮.md "wikilink") .......Minnie 之丈夫
  - 河國榮 ....... George Hamcatcher (Minnie 的大姐夫)

## 主題曲

  - 〈[第一次我想醉](http://www.youtube.com/watch?v=rbS_Ve3LeXY)〉：由[容祖兒唱](../Page/容祖兒.md "wikilink")

## 外部連結

  - {{@movies|fthk60186543}}

  -
  -
  -
  -
[6](../Category/1990年代香港電影作品.md "wikilink")
[Category:香港劇情片](../Category/香港劇情片.md "wikilink")
[Category:阮世生编剧作品](../Category/阮世生编剧作品.md "wikilink")