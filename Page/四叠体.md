**四叠体**（）是[中脑的一个解剖结构](../Page/中脑.md "wikilink")。其[拉丁文学名的字面意思为](../Page/拉丁文.md "wikilink")“四个双生子形的物体”，形容中脑背面的四个圆形隆起：两侧的[下丘](../Page/下丘.md "wikilink")（Inferior
colliculi，单数Inferior
colliculus）和两侧的[上丘](../Page/上丘.md "wikilink")（Superior
colliculi，单数Superior
colliculus）。其中，下丘是[听觉通路的一部分](../Page/听觉通路.md "wikilink")，上丘主司与视觉相关的[眼动功能](../Page/眼动.md "wikilink")。上丘与下丘间有密切的连接，主要负责基于视觉和听觉信息的空间定位\[1\]。

## 更多图像

[`File:Gray678.png|脑的主要神经核团的示意图。其中Corpora`](File:Gray678.png%7C脑的主要神经核团的示意图。其中Corpora)` quadrigemina所示为四叠体。`
[`File:Gray710.png|中脑的`](File:Gray710.png%7C中脑的)[`冠状切面`](../Page/冠状切面.md "wikilink")`。`
[`File:Gray713.png|内，外侧`](File:Gray713.png%7C内，外侧)[`丘系和四叠体的位置关系`](../Page/丘系.md "wikilink")`。其中蓝色所示为`[`内侧丘系`](../Page/内侧丘系.md "wikilink")`，红色所示为`[`外侧丘系`](../Page/外侧丘系.md "wikilink")`。`
<File:Gray716.png>`|`[`脑室与四叠体的位置关系`](../Page/脑室.md "wikilink")`。`
[`File:Gray720.png|从脑的中矢面看四叠体`](File:Gray720.png%7C从脑的中矢面看四叠体)`。`
<File:Gray792.png>`|`[`后脑和`](../Page/后脑.md "wikilink")[`中脑的上部`](../Page/中脑.md "wikilink")`，在体背面观。`

## 参考文献

## 外部連結

  - [SUNY
    Niagara](https://web.archive.org/web/20050114130829/http://www.sunyniagara.cc.ny.us/val/corporaquad.html)
  - [Diagram](https://archive.is/20070317210005/http://faculty.tcc.fl.edu/scma/aplab/Practical%20Three/BrainmodelOne.htm)

[Category:脑干](../Category/脑干.md "wikilink")
[Category:神经解剖学](../Category/神经解剖学.md "wikilink")

1.  Marieb, Elaine N. Essentials of Human Anatomy & Physiology. 6th ed.
    San Francisco: Daryl Fox, 2000. 210.