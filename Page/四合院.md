[Xsgl2.jpg](https://zh.wikipedia.org/wiki/File:Xsgl2.jpg "fig:Xsgl2.jpg")\]\]
[Siheyuan_model.jpg](https://zh.wikipedia.org/wiki/File:Siheyuan_model.jpg "fig:Siheyuan_model.jpg")

**四合院**，又称**四合房**\[1\]，是一种[合院式](../Page/合院.md "wikilink")[中国传统建筑](../Page/中国传统建筑.md "wikilink")，其格局为一个院子四面建有[房屋](../Page/住宅.md "wikilink")，通常由[正房](../Page/正房.md "wikilink")、东西[厢房和](../Page/厢房.md "wikilink")[倒座房组成](../Page/倒座房.md "wikilink")，从四面将[庭院合围在中间](../Page/花園.md "wikilink")，故名四合院。\[2\]

## 樣式

四合院就是三合院前面有加門房的屋舍來封閉。若呈「口」字形的稱為一進院落；「日」字形的稱為二進院落；「目」字形的稱為三進院落\[3\]。一般而言，大宅院中，第一進為門屋，第二進是廳堂，第三進或後進為私室或閨房，是婦女或眷屬的活動空間，一般人不得隨意進入，故古人有詩云：“庭院深深幾許也”。\[4\]庭院越深，越不得窺其堂奧。

四合院至少有3000多年的历史，在中国各地有多种类型，其中以[北京四合院为典型](../Page/北京四合院.md "wikilink")。四合院通常为大家庭所居住，提供了对外界比较隐密的庭院[空间](../Page/空间.md "wikilink")，其建筑和格局体现了中国传统的尊卑等级思想以及[阴阳五行学说](../Page/阴阳五行.md "wikilink")。

## 現狀

在现代，随着家庭结构和社会观念的变迁，传统四合院的宜居性受到了挑战。而在[城市规划过程中](../Page/城市规划.md "wikilink")，传统四合院也面临着保护和发展的矛盾，一些四合院被列为了中國文物保护单位，同时也有一些被拆除。

## 历史

四合院历史悠久，早在3000多年前的中国[西周时期就有四合院的出现](../Page/西周.md "wikilink")。[陕西](../Page/陕西省.md "wikilink")[岐山凤雏村](../Page/岐山县.md "wikilink")[周原遗址出土的两进院落建筑遗迹](../Page/周原.md "wikilink")，是中国已知最早、最严整的四合院实例。\[5\]

[汉代四合院建筑有了更新的发展](../Page/汉朝.md "wikilink")，受到[风水学说的影响](../Page/風水.md "wikilink")，四合院从选址到布局，有了一整套阴阳五行的说法。[唐代四合院上承两汉](../Page/唐朝.md "wikilink")，下启宋元，其格局是前窄后方。\[6\]

然而，古代盛行的四合院是[廊院式院落](../Page/廊院.md "wikilink")，即院子[中轴线为主体建筑](../Page/中轴线.md "wikilink")，周围为[回廊链接](../Page/回廊.md "wikilink")，或左右有屋，而非四面建房。\[7\]
[晚唐出现具有](../Page/唐朝.md "wikilink")[廊庑的四合院](../Page/廊庑.md "wikilink")，逐渐取代了廊院，[宋朝以后](../Page/宋朝.md "wikilink")，廊院逐渐减少，到明清逐渐绝迹。\[8\]

[元明清时期四合院逐渐成熟](../Page/元明清历史年表.md "wikilink")。元世祖[忽必烈](../Page/忽必烈.md "wikilink")“诏旧城居民之过京城老，以赀高（有钱人）及居职（在朝廷供职）者为先，乃定制以地八亩为一分”，分给前往[大都的富商](../Page/元大都.md "wikilink")、官员建造住宅，由此开始了[北京传统四合院住宅大规模形成时期](../Page/北京市.md "wikilink")。\[9\]
1970年代初，北京[后英房胡同出土的](../Page/后英房胡同.md "wikilink")[元代四合院遗址](../Page/元朝.md "wikilink")，可视为[北京四合院的雏形](../Page/北京四合院.md "wikilink")。\[10\]后经[明](../Page/明朝.md "wikilink")、[清完善](../Page/清朝.md "wikilink")，逐渐形成[北京特有的四合院建筑风格](../Page/北京市.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，北京的很多四合院沦为了[大杂院](../Page/大杂院.md "wikilink")，\[11\]而[改革开放后随着城市改造的开展](../Page/改革开放.md "wikilink")，很多传统四合院被拆毁，如1998年拆除[康有为的](../Page/康有为.md "wikilink")[粤东新馆](../Page/粤东新馆.md "wikilink")，2000年拆除[赵紫宸故居](../Page/赵紫宸.md "wikilink")，2004年拆除[孟端胡同](../Page/孟端胡同.md "wikilink")45号的清代[果郡王府](../Page/果郡王府.md "wikilink")，2005年拆除[曹雪芹故居](../Page/曹雪芹.md "wikilink")，2006年拆除[唐绍仪故居](../Page/唐绍仪.md "wikilink")。\[12\]与此同时，一些四合院被列入北京市和各区县级的保护院落。

[Siheyuanxianzhuang2.JPG](https://zh.wikipedia.org/wiki/File:Siheyuanxianzhuang2.JPG "fig:Siheyuanxianzhuang2.JPG")现存的四合院院门\]\]
在古代，四合院基本上满足了一家人生活的需要，那时两进四合院以及更大的四合院通常是官宦和[士绅的住所](../Page/士绅.md "wikilink")。而到了现代，一方面上下水、[暖气等卫生设施沒有進入四合院](../Page/暖气.md "wikilink")，淪為大雜院的四合院也未繼續改進以適應[汽车](../Page/汽车.md "wikilink")、[空调等设备的需要](../Page/空氣調節.md "wikilink")，另一方面，像四世同堂那样的大家庭也比较少见，有钱的人家通常愿意在交通方便的郊外购置[别墅](../Page/别墅.md "wikilink")，而不是生活在人口密度较高的[市区](../Page/市区.md "wikilink")。因此作为民居的四合院是否还存在价值，也是近代以来，一个争论的问题。

随着老城区保护的开展，对原有四合院进行了改造，例如1990年由[清华大学](../Page/清华大学.md "wikilink")[吴良镛教授主持的对北京](../Page/吴良镛.md "wikilink")[菊儿胡同四合院的危改项目](../Page/菊儿胡同.md "wikilink")，在保留院落结构的基础上，将原有四合院的平房改为楼房，增加了厨房、[卫生间等设施](../Page/卫生间.md "wikilink")。\[13\]该改造工程获得了[联合国的](../Page/联合国.md "wikilink")[世界人居奖](../Page/世界人居奖.md "wikilink")。\[14\]北京[南池子危改中](../Page/南池子.md "wikilink")，也将部分四合院房屋改成两层，并修建了地下车库。\[15\]2006年，北京市公布了《[北京四合院建筑要素图](../Page/北京四合院建筑要素图.md "wikilink")》，作为对四合院保护、修缮、翻建、改建的参考依据。\[16\]

## 各地四合院

四合院在中国的[华北地区和](../Page/华北地区.md "wikilink")[西北地区建造得较多](../Page/中国西北地区.md "wikilink")，以[北京的四合院为代表](../Page/北京市.md "wikilink")。

### 北京四合院

[Menlian.JPG](https://zh.wikipedia.org/wiki/File:Menlian.JPG "fig:Menlian.JPG")\]\]
北京是四合院最常见也最有特色的城市。北京四合院的特点是，絕大多數爲單層建築，\[17\]当中围成的院落接近正方形，四面各房屋独立，以[廊相连](../Page/廊.md "wikilink")，\[18\]院门多开在东南方位。\[19\]

从空中俯瞰北京城，可以看到一片灰瓦的房屋围着一个四方的院子。院子里绿葱葱的树木给灰色的房屋做点缀，也给四合院院里的人们提供了树荫。北京的四合院现在已经和北京[胡同一起](../Page/胡同.md "wikilink")，成为北京传统文化和民俗的代表，成为北京城市建筑形象的标志。

### 其他地方的四合院

除北京外，中国北方其他地方也以四合院为主要的民居形式。由于[气候](../Page/氣候.md "wikilink")、建筑[材料](../Page/材料.md "wikilink")、[文化传统等因素的影响](../Page/文化.md "wikilink")，不同地区的四合院也有不同的特色。

著名的[山西省](../Page/山西省.md "wikilink")[平遥的](../Page/平遥县.md "wikilink")[乔家大院就是山西风格的四合院](../Page/乔家大院.md "wikilink")。更广义地说，[黄土高原上由](../Page/黄土高原.md "wikilink")[窑洞围合而成下沉式](../Page/窑洞.md "wikilink")“[地坑窑](../Page/地坑窑.md "wikilink")”的院落也是四合院的一种。另外，[云南省](../Page/云南省.md "wikilink")[大理](../Page/大理.md "wikilink")[白族的](../Page/白族.md "wikilink")“[三坊一明壁](../Page/三坊一明壁.md "wikilink")”式民居，也是由正房、厢房和三坊房屋及明壁围成的四合院。\[20\]

不同之处是，[山西南部以及](../Page/山西省.md "wikilink")[陕西](../Page/陕西省.md "wikilink")、[河南等地](../Page/河南省.md "wikilink")，由于夏季有严重的[西晒](../Page/西晒.md "wikilink")，因此为减少日晒而把院子南北向加长，不像北京四合院为接近正方形，而是长方形。而[安徽](../Page/安徽省.md "wikilink")[徽州的四合院则南北窄而东西长](../Page/徽州.md "wikilink")，以接受更多日照。\[21\]西北[甘肃](../Page/甘肃省.md "wikilink")、[青海风沙大](../Page/青海省.md "wikilink")，因此加高了院墙，称为[庄窠](../Page/庄窠.md "wikilink")。南方的庭院则较小，四周以楼围成，中央称为[天井](../Page/天井.md "wikilink")。

此外，云贵的“[一颗印](../Page/一颗印.md "wikilink")”和[东北的大院与四合院也有相似之处](../Page/中国东北地区.md "wikilink")，不过又有着本质的不同。东北土地辽阔而气候寒冷，因此院子面积很大，院内空地多，以便接纳更多阳光。\[22\]

## 总体格局

[Siheyuan_pmt.JPG](https://zh.wikipedia.org/wiki/File:Siheyuan_pmt.JPG "fig:Siheyuan_pmt.JPG")
正式的四合院，一户一宅，平面格局可大可小。房屋主人可以根据土地面积的大小、家中人数的多少来建造，小到可以只有一进，大可以到三进或四进，还可以建成两个四合院宽的带[跨院的](../Page/跨院.md "wikilink")。小者，房间为13间；一院或二院者，房间为25间到40间。\[23\]厢房的后墙为院墙，拐角处再砌砖墙。大四合院从外边用墙包围，墙壁高大，不开窗户，以显示其隐秘性。从制式上来说，许多[王府和](../Page/王府.md "wikilink")[寺庙也是按照四合院的布局进行设计和建造的](../Page/寺庙.md "wikilink")。

最小的一进院，进了街门直接就是院子，以中轴线贯穿，房屋都是单层，由[倒座房](../Page/倒座房.md "wikilink")、正房、厢房围成院落，其中北房为正房，东西两个方向的房屋为厢房，南房门向北开，故称为“倒座房”。四合院中植花果树木，以供观赏。

两进四合院分为前院和后院，后院又叫做内宅。前院由门楼、倒座房组成，连接前后院的一般为[垂花门](../Page/垂花门.md "wikilink")，一些相对朴素的住宅则用[月亮门](../Page/月亮门.md "wikilink")，后院由东西厢房、正房、[游廊组成](../Page/廊.md "wikilink")。也有的两进四合院，例如北京[茅盾故居](../Page/茅盾故居.md "wikilink")，并没有垂花门隔出前院，而是在正房后加后院，建专供女眷居住的[后罩房](../Page/后罩房.md "wikilink")。

完整的四合院为三进院落，第一进院是垂花门之前由倒座房所居的窄院，第二进院是厢房、正房、游廊组成，正房和厢房旁还可加[耳房](../Page/耳房.md "wikilink")，第三进院为正房后的后罩房，在正房东侧耳房开一道门，连通第二和第三进院。在整个院落中，老人住北房（上房），中间为大客厅（[中堂间](../Page/中堂.md "wikilink")），长子住东厢，次子住西厢，佣人住倒座房，女儿住后院，互不影响。这其中也有反映“男外女内”的中国传统文化[哲学的影响](../Page/哲学.md "wikilink")。

四、五进院的组合方式较多，通常为“前堂后寝”式。第一进院与三进院相同，第二进院是对外使用的厅房和东西厢房，之后再设一道垂花门，在厅房和这道垂花门之间形成第三进院，垂花门之后为正房和厢房所在的第四进院，是主院。如果后面还有后罩房，就构成了第五进院。还有的在倒座房北侧再建一排南房，而组成四进或五进院的。\[24\]

北京四合院最大的进深为两个[胡同之间的距离](../Page/胡同.md "wikilink")，约77米左右，\[25\]一些比较奢华的院落甚至还有[花园和](../Page/花園.md "wikilink")[假山](../Page/假山.md "wikilink")。规格高一些的四合院还设有[厕所](../Page/廁所.md "wikilink")，这些内设的厕所一般都被安排到西南角，按风水的说法，西南为“五鬼之地”，建厕所可以用秽物将白虎镇住，\[26\]从实用的角度看，厕所建在西南方适应了西北-东南风向，可防臭味在院内扩散，\[27\]但大多数居民如厕大多在胡同里的“官厕”即现在说的[公共厕所](../Page/公共厕所.md "wikilink")。

## 单体建筑

四合院是由许多单体建筑组合而成，包括大门、正房、厢房等等。

### 大门

[大門又叫街门](../Page/大門.md "wikilink")、宅门，是北京四合院与外界沟通的通道，一般都修筑在整个院落的东南侧，以取“紫气东来”之意，\[28\]也有说法是占据[八卦中的](../Page/八卦.md "wikilink")[巽位](../Page/周易下經三十四卦列表#.E5.B7.BD.md "wikilink")，即风位，是和风、润风吹进的位置，以引进东南风，挡住冬天的西北风，是吉祥之位，\[29\]体现“[坎宅巽门](../Page/坎宅巽门.md "wikilink")”的原则。\[30\]

#### 大门的形制

根据主人的地位等级不同四合院的街门分为[王府大门](../Page/王府大门.md "wikilink")、[广亮大门](../Page/广亮大门.md "wikilink")、[金柱大门](../Page/金柱大门.md "wikilink")、[蛮子门](../Page/蛮子门.md "wikilink")、[如意门](../Page/如意门.md "wikilink")、[墙垣式门](../Page/墙垣式门.md "wikilink")（門樓）等几种不同的形制，\[31\]随着西洋式建筑[圆明园的修建](../Page/圆明园.md "wikilink")，在民间也出现了大量的中西合壁式的门楼，被百姓形象地称为“圆明园式”门楼。

王府大门顾名思义是用于王府，通常是三间房开门一间，大门上有[门钉](../Page/门钉.md "wikilink")。按照不同的等级最多有63颗门钉。广亮大门和金柱大门都是有官的人家使用。广亮大门通常宽一开间，门设于[中柱的位置](../Page/柱#.E4.B8.AD.E6.9F.B1.md "wikilink")；金柱大门规格上略小，门设于[金柱的位置](../Page/柱#.E9.87.91.E6.9F.B1.md "wikilink")。蛮子门和如意门是一般人家使用的。蛮子门的门更为靠前，设于[檐柱位置](../Page/柱#.E6.AA.90.E6.9F.B1.md "wikilink")；如意门通常在檐柱的位置砌一堵墙，门很小。如意门上面的门头通常是[砖雕](../Page/砖雕.md "wikilink")，也有用瓦片堆成链形，[砂锅形图案](../Page/砂锅.md "wikilink")。最簡單的大門是直接在牆上用磚砌築一個入口門洞，安裝門扇，沒有內部空間，稱為門樓。

<File:Wangfudamen.JPG>|[王府大门](../Page/王府大门.md "wikilink")（大门五间，启门三，醇王府）
<File:Guangliangdamen.JPG>|[广亮大门](../Page/广亮大门.md "wikilink")（门框设于[中柱位置](../Page/柱#.E4.B8.AD.E6.9F.B1.md "wikilink")）
<File:Jinzhudamen.JPG>|[金柱大门](../Page/金柱大门.md "wikilink")（门框前移，设于[金柱位置](../Page/柱#.E9.87.91.E6.9F.B1.md "wikilink")）
<File:Manzimen.JPG>|[蛮子门](../Page/蛮子门.md "wikilink")（门框进一步前移，设于[檐柱位置](../Page/柱#.E6.AA.90.E6.9F.B1.md "wikilink")）
<File:Ruyimen.JPG>|[如意门](../Page/如意门.md "wikilink")（在檐柱位置砌一堵墙，墙上开一小门）
<File:Zhongxihebimen.JPG>|[中西合璧门](../Page/中西合璧.md "wikilink")（[南锣鼓巷](../Page/南锣鼓巷.md "wikilink")）
<File:Hutong_Jiemen_(Snowyowls).jpg>|[清水脊街门](../Page/清水脊.md "wikilink")

#### 大门的构件

[Qingshuiji.JPG](https://zh.wikipedia.org/wiki/File:Qingshuiji.JPG "fig:Qingshuiji.JPG")[清水脊](../Page/清水脊.md "wikilink")\]\]
门楼的屋顶通常铺瓦有[筒瓦和](../Page/筒瓦.md "wikilink")[仰合瓦两种方式](../Page/仰合瓦.md "wikilink")。屋顶有[清水脊和](../Page/清水脊.md "wikilink")[卷棚](../Page/卷棚顶.md "wikilink")（又称[元宝脊](../Page/卷棚顶.md "wikilink")）等几种。清水脊通常在两边有两块向上的瓦，称为[蝎子尾](../Page/清水脊.md "wikilink")，[鸱尾](../Page/螭吻.md "wikilink")，朝天笏，在蝎子尾下面有花砖。房檐位于[墀头的位置通常装有](../Page/墀头.md "wikilink")[博风起到保护墀头墙的作用](../Page/博风板.md "wikilink")，博风也可以有[砖雕](../Page/砖雕.md "wikilink")。

[雀替是位于檐柱和檐枋之间的木构件](../Page/雀替.md "wikilink")，在力学上有一定的作用，但雀替和[三幅云更多地是官品的象征](../Page/三幅云.md "wikilink")。只有王府大门、广亮大门和金柱大门才有雀替，而蛮子门和如意门由于大门设在[檐柱上](../Page/柱#.E6.AA.90.E6.9F.B1.md "wikilink")，因此没有雀替的位置。

[门簪是把楹固定在门中门槛上的木结构](../Page/门簪.md "wikilink")，大门用四颗门簪，小门用两颗门簪。门簪露于门外的部分用门簪帽来装饰，门簪帽一般是带有曲线的六角形。门簪帽上雕通常刻有迹象的图样和文字。

大门的[门板有很多种](../Page/门板.md "wikilink")，在门板上的通常还雕刻有[门联](../Page/门联.md "wikilink")，比较讲究一些的街门都雕刻有门心，一般比较常见的联句是“忠厚传家久，诗书济世长”之类。\[32\]门上在中间装有[门钹](../Page/门环.md "wikilink")，用于叩门，门下方有[护门铁](../Page/护门铁.md "wikilink")。

[Shangmashi.JPG](https://zh.wikipedia.org/wiki/File:Shangmashi.JPG "fig:Shangmashi.JPG")\]\]
[墀头是指山墙突出檐柱以外的部分](../Page/墀头.md "wikilink")，\[33\]墀头墙上的[盘头](../Page/盘头.md "wikilink")（又称为戗檐和拔檐）以及如意门的门头的砖雕都是精美的工艺品。墀头墙丛上往下是戗檐，两层拔檐，莲花墩，贴花等部件。如意门门头从上往下是栏板和望柱、冰盘檐、[枭混](../Page/枭混.md "wikilink")、[连珠](../Page/连珠纹.md "wikilink")、挂落板等部件。

[门墩是指](../Page/门墩.md "wikilink")[门枕石位于门外部的部分](../Page/门墩.md "wikilink")，通常有箱形和抱鼓形（[抱鼓石](../Page/抱鼓石.md "wikilink")）两种，门枕石的内部有一石窝用于插入门枢。门墩是门楼中比较有特色的一个组成部件，通常由[须弥座](../Page/须弥座.md "wikilink")，抱鼓或箱形，以及[兽吻或](../Page/兽吻.md "wikilink")[狮子](../Page/狮.md "wikilink")（有说是[狻猊](../Page/狻猊.md "wikilink")）几部分组成。根据门楼的形制不同门墩的形制也有差异，出现在门墩上的雕刻是研究中国古代社会文化重要的资料，也是精美的[石刻艺术品](../Page/石刻.md "wikilink")。

在门旁通常还有[上马石和](../Page/上马石.md "wikilink")[拴马桩等设施](../Page/拴马桩.md "wikilink")，有的还有[泰山石敢当](../Page/石敢当.md "wikilink")。

### 影壁

[Mdgj_yingbi.JPG](https://zh.wikipedia.org/wiki/File:Mdgj_yingbi.JPG "fig:Mdgj_yingbi.JPG")的内[影壁](../Page/影壁.md "wikilink")\]\]
影壁在四合院中通常用于遮挡视线、美化实现和突出大门的作用，通常是由砖砌成，由座、身、顶三部分组成，座有[须弥座](../Page/须弥座.md "wikilink")，也有简单的没有座。四合院的大门有时候向后退后几步在大门左右修建八字墙，称为燕翅影壁，撇子影壁。四合院大门内一般也有一个影壁，一般镶嵌在东厢房[盝顶的山墙上](../Page/盝顶.md "wikilink")，也有规格高一些的院落，采用独立影壁。影壁也有置于四合院大门外的街道中正对大门的位置。

影壁墙身的中心区域称为影壁心，通常由45度角斜放的方砖贴砌而成，简单一点的影壁可能没有什么装饰，但也必须磨砖对缝非常整齐，豪华的影壁通常装饰有很多吉祥图样的砖雕。影壁墙上的砖雕主要有中心区域的中央和四角，在与屋顶相交的地方也有[混枭和](../Page/混枭.md "wikilink")[连珠](../Page/连珠纹.md "wikilink")。中心方砖上面一边雕刻有中心花、岔角在影壁墙的中央还镶嵌有福寿字的砖匾或者是带有吉祥意味的砖雕。

影壁墙的顶一般和屋顶一样，虽然是砖砌，影壁的顶也用砖雕出椽子，并在上设清水脊或卷棚脊的屋顶。

### 倒座房

倒座房是整个四合院中最南端的一排房子，其[檐墙临](../Page/檐墙.md "wikilink")[胡同](../Page/胡同.md "wikilink")，一般不开[窗](../Page/窗.md "wikilink")。\[34\]由于门窗都向北，因此采光不好。其最东为[私塾](../Page/私塾.md "wikilink")，最西为[厕所](../Page/廁所.md "wikilink")，其间的房子一般为[仆人居住](../Page/僕人.md "wikilink")。\[35\]

南侧的街门、倒座房、北侧的垂花门和游廊共同围成四合院的前院，前院是主人会客办公的场所，通过垂花门之后才是内宅，即四合院的生活区。

### 屏门

[Gmrgj_1.JPG](https://zh.wikipedia.org/wiki/File:Gmrgj_1.JPG "fig:Gmrgj_1.JPG")的[垂花门](../Page/垂花门.md "wikilink")，其后绿色的为[屏门](../Page/屏门.md "wikilink")\]\]
进入大门后影壁的左右两侧分别有一道屏门，向东通向“塾”的小院，向西通往有倒座房的外院。在内外院之间的垂花门内也有屏门。\[36\]

### 垂花门

垂花门又称二门，开在内外院之间的隔墙上，位于院落的中轴线上。旧时说的大户人家的闺女“大门不出，二门不迈”，就是指不迈垂花门。\[37\]垂花门的外檐柱不是从地上立起的，而是悬在中柱的横木上，称为垂柱，垂柱的下端有一垂珠，通常彩绘为花瓣的形式，因此称为垂花门。垂花门是四合院中装饰富丽的建筑。

垂花门的屋顶通常是[卷棚式](../Page/卷棚顶.md "wikilink")，或一殿一卷式，即门外为清水脊式，门内为卷棚式。垂花门的门有两道，一道是在中柱位置上，白天开启，夜间关闭；另一道是屏门，在内檐柱位置上，平时关闭，起到隔绝内院视线的作用。垂花门进入内院是通过垂花门两侧内檐柱与中柱之间空间进出，通常与[抄手游廊相衔接](../Page/抄手游廊.md "wikilink")。

### 正房

[Siheyuan_zhengfangchenshe.JPG](https://zh.wikipedia.org/wiki/File:Siheyuan_zhengfangchenshe.JPG "fig:Siheyuan_zhengfangchenshe.JPG")
四合院的[正房一般三间](../Page/正房.md "wikilink")，大四合院的正房可以为五至七间，坐北朝南，是一家之主的居所。正房的明间（即中间一间）称为[堂屋](../Page/堂屋.md "wikilink")，也称为中堂，三开间的正房堂屋两侧是[卧室和](../Page/卧室.md "wikilink")[书房](../Page/书房.md "wikilink")，正房的特点是冬天太阳能够照进屋里，冬暖夏凉。通常在明间正中排放一[八仙桌](../Page/八仙桌.md "wikilink")，桌子两旁设两把椅子，在墙上挂着一幅画和两副[条幅](../Page/条幅.md "wikilink")，或挂四幅[中堂画](../Page/中堂画.md "wikilink")。

### 厢房

东西厢房是子孙們的住房，也常是三间。以东厢房为尊西厢房为卑，北京四合院东厢房一般住长子长媳，\[38\]因此在建筑上东西厢房的高度有着细微的差别，东厢房略高西厢房略低，但由于差别非常细微因此很难用肉眼看出来。例如[石家庄四合院的东厢房比西厢房高二寸](../Page/石家庄市.md "wikilink")。\[39\]然而，在中国华北地区，东厢房夏季[西晒](../Page/西晒.md "wikilink")，冬季直接受到西北冷风吹袭，所以不宜居住，[陕西四合院东厢房多被富户用来存储粮物](../Page/陕西省.md "wikilink")，或作[厨房](../Page/厨房.md "wikilink")、[马厩](../Page/马厩.md "wikilink")。\[40\]

### 耳房

[Luding_paishuikong.JPG](https://zh.wikipedia.org/wiki/File:Luding_paishuikong.JPG "fig:Luding_paishuikong.JPG")上的排水孔\]\]
正房两侧的两间房间高度低于堂屋，且布局颇似人的双耳，故而被称作耳房。\[41\]如果院子狭长，厢房通常也会有耳房，通常是平顶的，因此厢房的耳房被称为[盝顶](../Page/盝顶.md "wikilink")。\[42\]

### 后罩房

后罩房通常是最裏一进院子的，靠近院落边界的房子，通常主人的女儿居住。后罩房和正房朝向一致，坐北朝南，其间数一般是和[倒座房相同](../Page/倒座房.md "wikilink")，以尽量添满[住宅基地的宽度](../Page/宅基地.md "wikilink")。\[43\]后罩房的等级低于正房和[厢房](../Page/厢房.md "wikilink")，其房屋尺度及品质相比而言都稍差。\[44\]

### 群房

群房，又称裙房，通常在院子的东侧或西侧的一排房子，作为[厨房和仆人住宅](../Page/厨房.md "wikilink")。\[45\]

### 廊

[Pashanlang.JPG](https://zh.wikipedia.org/wiki/File:Pashanlang.JPG "fig:Pashanlang.JPG")\]\]
四合院里的廊是有顶的建筑，用于下雨雪时行走，分为[檐廊和](../Page/檐廊.md "wikilink")[游廊两种](../Page/廊.md "wikilink")，前者是指正房和厢房前面有顶的走廊，顶通常是屋檐延长出来的；后者是指沿墙的廊（[抄手游廊](../Page/抄手游廊.md "wikilink")）和连接正房与厢房的走廊（[穿山游廊](../Page/穿山游廊.md "wikilink")）。有的檐廊和抄手游廊用窗户封起来，成为室内环境，称为[暖廊](../Page/暖廊.md "wikilink")。

《[红楼梦](../Page/红楼梦.md "wikilink")》中有在[林黛玉初进贾府时](../Page/林黛玉.md "wikilink")，有如下描述：

“众婆子步下围随至一[垂花门前落下](../Page/垂花门.md "wikilink")。众小厮退出，众婆子上来打起轿帘，扶黛玉下轿。林黛玉扶着婆子的手，进了垂花门，两边是抄手游廊，当中是穿堂，当地放着一个[紫檀架子](../Page/紫檀.md "wikilink")[大理石的大](../Page/大理岩.md "wikilink")[插屏](../Page/插屏.md "wikilink")。转过插屏，小小的三间厅，厅后就是后面的正房大院。正面五间上房，皆雕梁画栋，两边穿山游廊厢房，挂着各色[鹦鹉](../Page/鹦形目.md "wikilink")，[画眉等鸟雀](../Page/画眉.md "wikilink")。”\[46\]

### 庭院

[Bjlaosheguju.jpg](https://zh.wikipedia.org/wiki/File:Bjlaosheguju.jpg "fig:Bjlaosheguju.jpg")正房前庭院，内种两棵[柿子树](../Page/柿.md "wikilink")\]\]
四合院除了内宅、外宅的主要院落之外，还会形成一些小的院子，如正房两旁耳房前的小院，以及外院两侧被屏门隔开的小院。

内宅的院落中有正南北十字形的甬道，老北京的住户大多会在院子里栽上树，除了[松树](../Page/松科.md "wikilink")、[柏树和](../Page/侧柏.md "wikilink")[杨树等因为多种在坟地而不能栽种外](../Page/杨树.md "wikilink")，其他各种树木都有种植，过去北京有[民谚](../Page/谚语.md "wikilink")：“[桑松柏](../Page/桑树.md "wikilink")[梨](../Page/梨.md "wikilink")[槐](../Page/槐树.md "wikilink")，不进府王宅”，说的就是在庭院种树的禁忌。\[47\]比较常见的树木有[枣树](../Page/枣.md "wikilink")、[柿树等](../Page/柿.md "wikilink")，花木主要有[牡丹](../Page/牡丹.md "wikilink")、[芍药](../Page/芍药.md "wikilink")、[玉兰](../Page/玉兰.md "wikilink")、[丁香](../Page/丁香.md "wikilink")、[海棠](../Page/海棠.md "wikilink")、[紫藤](../Page/紫藤.md "wikilink")、[石榴等](../Page/石榴.md "wikilink")，在四合院内种枣树、石榴树寓意“早”生贵子、多子多孙，种柿树表示事事如意，种丁香、海棠，表示主人有身份和有一定的文化修养。\[48\]

此外民间还有养鱼的习俗，多用直径为60到70厘米的[鱼缸养着各色的](../Page/水族箱.md "wikilink")[鱼并种着](../Page/鱼.md "wikilink")[荷花](../Page/莲.md "wikilink")，冬天鱼缸还能用来存放食品。\[49\]

[夏天时分](../Page/夏季.md "wikilink")，天气炎热，还可在庭院中搭设[天棚遮阳](../Page/天棚.md "wikilink")，老北京有“天棚鱼缸石榴树，先生肥狗胖丫头”的[俗语](../Page/俗語.md "wikilink")，就是家住北京的[书吏人家的生活写照](../Page/书吏.md "wikilink")。\[50\]

## 裝修

除梁柱等支持建築整體的關鍵部件外，起輔助修飾作用的構件稱作裝修。按室內外的不同，分為外檐裝修和內檐裝修。

### 外檐裝修

包括吊掛楣子、坐凳欄杆、隔扇門、支摘窗等。

### 內檐裝修

包括天花、隔斷。其中隔斷又有板壁、花罩、博古架、碧紗櫥四種形式。

## 著名四合院

### 高级四合院

  - [崇礼住宅](../Page/崇礼住宅.md "wikilink")
  - [张之洞住宅](../Page/张之洞住宅.md "wikilink")
  - [婉容故居](../Page/婉容故居.md "wikilink")
  - [乔家大院](../Page/乔家大院.md "wikilink")，位于[山西省](../Page/山西省.md "wikilink")[祁县乔家堡村](../Page/祁县.md "wikilink")

### 名人故居

  - [阅微草堂](../Page/纪晓岚故居.md "wikilink")
  - [茅盾故居](../Page/茅盾故居.md "wikilink")，位于[北京市](../Page/北京市.md "wikilink")[交道口](../Page/交道口.md "wikilink")[后圆恩寺胡同](../Page/后圆恩寺.md "wikilink")13号。
  - [鲁迅故居](../Page/北京鲁迅故居.md "wikilink")，位于北京市[阜成门内西三条](../Page/阜成门.md "wikilink")21号[鲁迅纪念馆院内](../Page/鲁迅纪念馆.md "wikilink")。
  - [老舍故居](../Page/北京老舍故居.md "wikilink")，位于北京市[东城区](../Page/东城区_\(北京市\).md "wikilink")[灯市口西街](../Page/灯市口.md "wikilink")[丰富胡同](../Page/丰富胡同.md "wikilink")19号。
  - [郭沫若故居](../Page/郭沫若故居.md "wikilink")，位于北京市[什刹海](../Page/什刹海.md "wikilink")[前海西街](../Page/前海灣.md "wikilink")。
  - [梅兰芳故居](../Page/北京梅兰芳故居.md "wikilink")，位于北京市[西城区](../Page/西城区.md "wikilink")[护国寺街九号](../Page/護國寺.md "wikilink")

### 四合院酒店

  - [竹園賓館](../Page/竹園賓館.md "wikilink")
  - [呂松園賓館](../Page/呂松園賓館.md "wikilink")
  - [北京閲微庄四合院賓館](../Page/北京閲微庄四合院賓館.md "wikilink")
  - [好園賓館](../Page/好園賓館.md "wikilink")
  - [中堂客桟](../Page/中堂客桟.md "wikilink")

## 参考文献

## 外部链接

  - [當我們遇上奧運-第三集：北京四合院賓館副總經理張燕](http://www.rthk.org.hk/asx/rthk/tv/OlympicRendezvous/20080413.asx)（[香港電台](../Page/香港電台.md "wikilink")）
  - [應用「五行八卦」老北京四合院蘊藏中華古老智慧](http://www.epochtimes.com/b5/14/8/24/n4232219.htm)

## 参见

  - [三合院](../Page/三合院.md "wikilink")

{{-}}

[Category:中国民居](../Category/中国民居.md "wikilink")
[四合院](../Category/四合院.md "wikilink")
[Category:中國傳統建築](../Category/中國傳統建築.md "wikilink")
[Category:中式建築](../Category/中式建築.md "wikilink")

1.  老北京人称四合院为四合房。见[宫阙依旧在今夕是何年](http://www.rpccn.com/news/62/2006518105413.htm)
    ，《[中华建筑报](../Page/中华建筑报.md "wikilink")》2006年5月18日。

2.  [四合院的传统文化](http://www.oldbeijing.net/Article/200704/10727.shtml)，老北京网2007年4月4日，文章来源于[荆楚网](../Page/荆楚网.md "wikilink")。

3.

4.  [欧阳修](../Page/欧阳修.md "wikilink")，《蝶態花》。

5.  [合院鼻祖——陕西歧山凤雏村西周遗址](http://www.xtour.cn/2004-11/2004114232818.htm)，陕西旅游资料网2004年11月4日。

6.  高巍，《漫话北京城》2003年5月第1版，第170,171页。

7.  高巍等，《四合院》，[学苑出版社](../Page/学苑出版社.md "wikilink")2007年第一版，第3页。ISBN
    7-5077-1827-1

8.  [中国古建独特的群体组合形式](http://www.klfcn.com/index.asp?xAction=xReadNews&NewsID=1341)
    。

9.  [老北京四合院的历史文化](http://xiuxian.ynet.com/article.jsp?oid=6913355)
    ，北青网，2005年11月1日。

10. 高巍等，《四合院》，[学苑出版社](../Page/学苑出版社.md "wikilink")2007年第一版，第3-4页。ISBN
    7-5077-1827-1

11. [四合院已变大杂院北京拟迁出住户以保护四合院](http://news.tom.com/2006-01-11/000N/11484253.html)，[Tom.com](../Page/TOM集團.md "wikilink")2006年1月11日。

12. [北京胡同迅速消亡：梅兰芳故居被大楼埋葬](http://history.163.com/06/0607/13/2J15AHS500011247.html)
    ，[网易](../Page/网易.md "wikilink")2006年6月7日。

13. 孔繁峙，[危房改造方式与传统四合院保护](http://www.bjww.gov.cn/2004/12-7/3717.shtml)，北京文博2004年12月7日。

14. [伤心四合院——拆掉“第二座城墙”](http://www.jianzhuw.com/article/jzarticle/jianzhulishi/20070722/1185113294247.html)
    ，[中国建筑网](../Page/中国建筑网.md "wikilink")2007年7月22日。

15. [北京：四合院地下有车库](http://news.xinhuanet.com/house/2003-08/18/content_1030858.htm)，[新华网](../Page/新华网.md "wikilink")2003年8月18日，文章来源于《[北京青年报](../Page/北京青年报.md "wikilink")》。

16. [《北京四合院建筑要素图》正式对外公布](http://www.bj.xinhuanet.com/bjpd_bjzq/2006-06/25/content_7345599.htm)
    ，[新华网](../Page/新华网.md "wikilink")2006年6月25日。

17. [试论北京四合院的建筑特色](http://www.artcn.cn/Article/hysj/LLYJ/200707/13559.html)
    ，艺术中国网2007年7月15日。

18. [老北京四合院的历史文化](http://xiuxian.ynet.com/article.jsp?oid=6913355&pageno=2)
    ，[北青网](../Page/北青网.md "wikilink")2005年11月1日。

19. [北京四合院的特点](http://house.qianlong.com/31278/2006/06/19/2530@3248375.htm)
    ，[千龙网](../Page/千龙网.md "wikilink")2006年6月19日。

20. 高巍等，《四合院》，[学苑出版社](../Page/学苑出版社.md "wikilink")2007年第一版，第6页。ISBN
    7-5077-1827-1

21.
22. 萧默，[北京四合院](http://www.china-culture.com.cn/ww/gs/1.htm)，节选自《中国建筑艺术史》第九章《明清建筑（二）》。

23. [张驭寰](../Page/张驭寰.md "wikilink")，《中国古建筑百问》，[档案出版社](../Page/档案出版社.md "wikilink")2000年3月版，ISBN
    7-80019-754-9，第113页（“九/（四）：北京[合院有什么特点](../Page/合院.md "wikilink")，其他地方的合院是否与北京合院相同？”；该节全文转载于北京胡同网2006年7月1日：“[北京胡同和四合院](http://www.hottoo.com/Article/shy/200607/20060701140906.html)
    ”）。

24. [四、五进院](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E5%9B%9B%E3%80%81%E4%BA%94%E8%BF%9B%E9%99%A2.html)
    ，中国文化网。

25. [元大都东西通道一般彼此间距](../Page/元大都.md "wikilink")77米，见[西城的胡同西城的胡同](http://www.bjxch.gov.cn/pub/xch_zhuzhan/B/B7/B7-2/B7-2a/B7-2a6/t20040301_532790.html)
    ，[西城区人民政府网站](../Page/西城区.md "wikilink")。

26. [求解四合院方程](http://culture.qianlong.com/6931/2006/10/19/1400@3468896.htm)
    ，[千龙网](../Page/千龙网.md "wikilink")，2006年10月19日。

27. [老舍茶馆与民俗文化](http://www.laosheteahouse.com/newsLetters/detail.asp?iNewID=571&iNoID=226&LetterNO=219)，《老舍月报》。

28. [恬静舒适的北京四合院](http://www.china.com.cn/chinese/zhuanti/184794.htm)，[中国网](../Page/中国网.md "wikilink")，2002年8月1日。

29. [平安四合院](http://wtyl.beijing.cn/lbjwh/gpshy/n214041311.shtml)
    ，[北京网](../Page/北京网.md "wikilink")2007年8月24日。

30. [大同四合院](http://www.tydao.com/datong/minshu/qita/4hy.htm)，[太原道](../Page/太原道.md "wikilink")2000年3月31日。

31. [四合院的宅门](http://www.beijingww.com/221/2007/07/09/61@28866.htm)，[北京文网](../Page/北京文网.md "wikilink")2007年7月9日。

32. 舒了，“[漫话胡同门联](http://www.bj.xinhuanet.com/bjpd_sdwm/2006-04/18/content_6769267.htm)
    ”，《[北京晚报](../Page/北京晚报.md "wikilink")》2006年4月18日。

33. [梁思成](../Page/梁思成.md "wikilink")，《[清式营造则例](../Page/清式营造则例.md "wikilink")》，[中国营造学社](../Page/中国营造学社.md "wikilink")1934年版。

34. [倒座](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E5%80%92%E5%BA%A7.html)
    ，

35. [北京四合院-房间](http://www.jixuelu.com/yandu/bjshy/fangjian/fangjian.htm)。

36. [旧京宅门的屏门](http://www.oldbeijing.net/Article/200501/20050111000727.shtml)
    ，老北京网，2005年1月11日。

37. [北京宅门中的垂花门](http://www.oldbeijing.net/Article/200501/5974.shtml)，老北京网，2005年1月8日。

38. [舒适温馨四合院](http://www.oldbeijing.net/Article/200412/5169.shtml)，老北京网，2004年12月14日，文章源自[北京电视台](../Page/北京电视台.md "wikilink")。

39. [石家庄人的房屋样式](http://www.hbyzly.com/yy/33j.asp?id=177)
    ，燕赵旅游网2006年7月16日。

40. 福客民俗网：[词条：陕西院落](http://dict.folkw.com/Dict.asp?id=3258) 。

41. [耳房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E8%80%B3%E6%88%BF.html)
    ，中国文化网。

42. [厢房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E5%8E%A2%E6%88%BF.html)
    ，中国文化网。

43. [四合院-三进院](http://heritage.news.tom.com/Archive/2000/12/15-61060.html)
    ，[Tom.com](../Page/TOM集團.md "wikilink")，2003年6月1日。

44. [后罩房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E5%90%8E%E7%BD%A9%E6%88%BF.html)
    ，中国文化网。

45. [群房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E7%BE%A4%E6%88%BF.html)
    ，中国文化网。

46. 《[红楼梦](../Page/红楼梦.md "wikilink")》第三回：“[贾雨村夤缘复旧职](../Page/贾雨村.md "wikilink")　[林黛玉抛父进京都](../Page/林黛玉.md "wikilink")”。

47. [“改造四合院：不事张扬融入现代气息”](http://news.xinhuanet.com/house/2007-05/25/content_6150044.htm)，《[新京报](../Page/新京报.md "wikilink")》2007年5月25日。

48. [北京四合院里的花木柿树事事如意秋柿橙红](http://house.qianlong.com/31278/2006/07/11/2530@3297412.htm)，[千龙网](../Page/千龙网.md "wikilink")2006年7月11日。

49. 老杨，[天棚、鱼缸](http://www.llaile.com/shownews.asp?id=103)
    ，《[老来乐](../Page/老来乐.md "wikilink")》杂志

50. [朱家溍](../Page/朱家溍.md "wikilink")，[老北京俗语正误](http://www.oldbeijing.net/Article/200404/20040419134749.shtml)
    ，《[人民日报海外版](../Page/人民日报.md "wikilink")》2000年11月9日第七版，老北京网转载。