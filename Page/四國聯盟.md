<table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><div style="text-align: center;">
<p><big><strong>四国联盟</strong></big></p>
</div>
<p><a href="https://zh.wikipedia.org/wiki/File:G4_Nations.svg" title="fig:G4_Nations.svg">G4_Nations.svg</a></p>
<ul>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

**四國聯盟**（）是指由[印度](../Page/印度.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[德國及](../Page/德國.md "wikilink")[日本四国組成的聯盟](../Page/日本.md "wikilink")，希望通过相互支持加入[联合国安全理事会](../Page/联合国安全理事会.md "wikilink")，並成為[聯合國安理會常任理事國](../Page/聯合國安理會常任理事國.md "wikilink")。

## 背景

联合国安理会目前有五个常任理事国，并且在决议表决时都拥有否决权，分別為：[美国](../Page/美国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[中國](../Page/中華人民共和國.md "wikilink")。

另外，联合国安理会还有10个非常任理事国席位，每个非常任理事国的每届任期为两年。据统计，四国联盟成员国在1987年至2010年的24年之间，当选联合国安理会非常任理事国的次数分别为：[印度二次](../Page/印度.md "wikilink")、[巴西五次](../Page/巴西.md "wikilink")、[日本五次](../Page/日本.md "wikilink")、[德国三次](../Page/德国.md "wikilink")。\[1\]

虽然大多数国家都已原则同意，联合国需要进行必要的改革，其中也包括扩大安全理事会的席位数量，但由于方向性分歧，很少有国家愿意详细讨论具体的改革时间和进程。

## 第一次結盟

2004年印度\[2\]、巴西、德國及日本四國聯合組成“四國聯盟”，藉由[安理會改革的機會爭取成為](../Page/安理會.md "wikilink")[常任理事國](../Page/常任理事國.md "wikilink")。

## 各國加入受阻

當四國欲加入的意願傳出後，許多國家聞訊認為本國權益受損，由[意大利起頭開始了集體反對的行動](../Page/意大利.md "wikilink")，由[韓國](../Page/韓國.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[巴基斯坦和](../Page/巴基斯坦.md "wikilink")[加拿大等約](../Page/加拿大.md "wikilink")50個國家於1995年組成的[咖啡俱乐部反對擴大聯合國安理會](../Page/咖啡俱乐部.md "wikilink")。

這些國家除了自身考慮外，常任國也不滿意增加過多成員，例如[美國反對日本](../Page/美國.md "wikilink")、德國、印度及巴西皆擁有[否決權](../Page/聯合國安全理事會否決權.md "wikilink")\[3\]。

### 印度

主要受到[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[美國和](../Page/美國.md "wikilink")[中國反對](../Page/中國.md "wikilink")。巴基斯坦外交部长[卡姆兰·尼亚兹说](../Page/卡姆兰·尼亚兹.md "wikilink")，由于印度过去与巴基斯坦的[战争](../Page/印巴战争.md "wikilink")，以及违反了[联合国要求在有争议的](../Page/联合国.md "wikilink")[克什米尔地区举行全民公决的决议](../Page/克什米尔.md "wikilink")，巴基斯坦反对印度成为联合国安理会常任理事国\[4\]。

### 巴西

受到[墨西哥](../Page/墨西哥.md "wikilink")、[阿根廷反對](../Page/阿根廷.md "wikilink")，認為會提高了該國在[拉丁美洲的競爭力及影響力](../Page/拉丁美洲.md "wikilink")。但巴西認為作為[發展中國家及拉丁美洲的最大經濟體](../Page/發展中國家.md "wikilink")，加入成為安理會常任理事國及擁有否決權後，必定會增強發展中國家的話語權，並可平衡中國作為現時唯一代表發展中國家否決權的地位。但美国不願意也不希望巴西拥有過多在[拉丁美洲的話語權](../Page/拉丁美洲.md "wikilink")，因而也不做支持立場。

### 德國

受到[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[美國](../Page/美國.md "wikilink")、[義大利反對](../Page/義大利.md "wikilink")。義大利連同西班牙等歐洲各國，皆反對歐洲鄰國的影響力擴張。而美國也時常因為利益因素與向來堅持己見的反對者德國發生摩擦，此外，日本和德国都是二战的战败国，如果让这两个战败国“入常”，单独撇下昔日的盟友意大利，意大利心理不平衡。此外，意大利、西班牙和北欧国家都是有分量的国家，它们表示自己國家也能要求分享新增设的常任理事国席位。雖然過去的歷史問題因素等，德國縱使早已與歐洲各國達成和解，但德國與英國、法國一樣同樣是[歐盟及](../Page/歐盟.md "wikilink")[北大西洋公約組織的成員國](../Page/北大西洋公約組織.md "wikilink")，假設只有德國加入，若把[俄羅斯也算為歐洲國家](../Page/俄羅斯.md "wikilink")，一旦德國成為安理會常任理事國及擁有否決權，歐洲國家在安理會的否決權便佔有過高比重，那意味著歐洲日後必須有其中一個國家（英國、法國）放棄否決權。

\===日國的主要原因。此外，[美國在日本的影響力](../Page/美國.md "wikilink")，日本成為安理會常任理事國及擁有否決權后，是否會有獨立于美國的外交政策成為疑問，因此众多的亚洲国家雖然不表反對但也沒有實質支持行動。

## 結果

最後，四國為了拉攏利亚和埃及等国围绕这两个席位争得热火朝天，加上美国反对让政局不稳定的非洲国家拥有否决权，故難以獲非洲國家支持。新增常任理事國的新方案未獲通過。

## 参见

  - [團結謀共識](../Page/團結謀共識.md "wikilink")
  - [BRICS](../Page/BRICS.md "wikilink")

## 参考文献

[Category:联合国改革](../Category/联合国改革.md "wikilink")
[Category:国际组织](../Category/国际组织.md "wikilink")
[Category:日德關係](../Category/日德關係.md "wikilink")
[Category:日本－巴西關係](../Category/日本－巴西關係.md "wikilink")
[Category:巴西－印度關係](../Category/巴西－印度關係.md "wikilink")
[Category:印度－日本關係](../Category/印度－日本關係.md "wikilink")

1.  [Membership of the Security
    Council](http://www.un.org/sc/members.asp)
2.
3.  [US backs Japan's UNSC bid despite setback to
    momentum](http://www.chinadaily.com.cn/english/doc/2005-04/18/content_435251.htm),
    [人民日报](../Page/人民日报.md "wikilink"), 2005年4月19日
4.  [巴基斯坦反对印度成为常任理事国
    公开力挺日本](http://news.21cn.com/world/guojisaomiao/2005/02/24/1998516.shtml)
    ，[21CN](../Page/21CN.md "wikilink")，2005年2月24日