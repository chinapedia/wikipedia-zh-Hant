[Si_Da_Kou.jpg](https://zh.wikipedia.org/wiki/File:Si_Da_Kou.jpg "fig:Si_Da_Kou.jpg")（並不是四大寇之一）。\]\]
[HK_Dr_Sun_Yat_Sen_KomTongHall_4_desperados.JPG](https://zh.wikipedia.org/wiki/File:HK_Dr_Sun_Yat_Sen_KomTongHall_4_desperados.JPG "fig:HK_Dr_Sun_Yat_Sen_KomTongHall_4_desperados.JPG")
**四大寇**，是[清末](../Page/清朝.md "wikilink")[民初](../Page/中華民國.md "wikilink")[革命家](../Page/革命家.md "wikilink")[孫中山](../Page/孫中山.md "wikilink")（1866年－1925年）、[陳少白](../Page/陳少白.md "wikilink")（1869年－1934年）、[尢列](../Page/尢列.md "wikilink")（1866年－1936年）、[楊鶴齡](../Page/楊鶴齡.md "wikilink")（1868年－1934年）四人的稱號。

清末時期，他們常在[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[歌賦街二十四號的楊鶴齡祖產商店](../Page/歌賦街.md "wikilink")[楊耀記處會面](../Page/楊耀記.md "wikilink")，並議論[中國時政](../Page/中國.md "wikilink")，大談反清逐滿及[太平天國遺事](../Page/太平天國.md "wikilink")，倡言革命，鼓吹共和。其所言無忌，聞者動容，孫、陳、尢、楊四人故被[滿清政府稱為](../Page/清朝政府.md "wikilink")「四大寇」。

民國十年（西元1921年），孫中山建立[廣州軍政府時](../Page/廣州軍政府.md "wikilink")，常與白、列、鶴齡三人在[廣州觀音山](../Page/廣州.md "wikilink")（今[越秀山](../Page/越秀山.md "wikilink")）[文瀾閣會面](../Page/文瀾閣_\(越秀山\).md "wikilink")；孫中山亦修治文瀾閣並題曰「四寇樓」，以誌昔日在楊耀記時的生活。

## 图片

<File:Yang> Heling.jpg|楊鶴齡 <File:Sun> Yat Sen 1900.png|孫中山 <File:Chen>
Shaobai.jpg|陳少白 <File:You> Lie.jpg|尢列

## 其他

另外[三国和](../Page/三国.md "wikilink")[宋代时也有四大寇之说](../Page/宋代.md "wikilink")。三国时西凉军[董卓势力衍生出来的四位军阀](../Page/董卓.md "wikilink")：[李傕](../Page/李傕.md "wikilink")、[郭汜](../Page/郭汜.md "wikilink")、[张济](../Page/张济.md "wikilink")、[樊稠](../Page/樊稠.md "wikilink")，李傕等人挟持[汉献帝](../Page/汉献帝.md "wikilink")，把持朝政，后被[曹操派人击败](../Page/曹操.md "wikilink")。

《[水浒传](../Page/水浒传.md "wikilink")》中[北宋末年的农民起义领袖](../Page/北宋.md "wikilink")：山东[宋江](../Page/宋江.md "wikilink")、江南[方腊](../Page/方腊.md "wikilink")、淮西[王庆](../Page/王庆.md "wikilink")、河北[田虎](../Page/田虎.md "wikilink")。

日本漫畫[海賊王的四大](../Page/海賊王.md "wikilink")[海盜也常被稱為](../Page/海盜.md "wikilink")[四皇](../Page/四皇.md "wikilink")。

## 参考文献

  - [四大寇之陳少白](http://www.southcn.com/news/gdnews/sh/hrjnh/fyrw/200409020620.htm)

{{-}}

[Category:孫中山](../Category/孫中山.md "wikilink")
[Category:清朝政治人物并称](../Category/清朝政治人物并称.md "wikilink")
[Category:中國革命家](../Category/中國革命家.md "wikilink") [Category:中国四大
(人物)](../Category/中国四大_\(人物\).md "wikilink")