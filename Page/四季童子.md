**四季童子**（，）是日本的女性[插圖畫家](../Page/插圖畫家.md "wikilink")。居住於[愛知縣](../Page/愛知縣.md "wikilink")。

主要擔任作品《[驚爆危機](../Page/驚爆危機.md "wikilink")》、《》及《》的[插圖而廣為人知](../Page/插圖.md "wikilink")。

另外在[TRPG雜誌中](../Page/テーブルトークRPG.md "wikilink")『』擔當著隔號封面的工作。

2008年，《[驚爆危機](../Page/驚爆危機.md "wikilink")》插圖的女主角千鳥要列入『[這本輕小說真厲害！](../Page/這本輕小說真厲害！.md "wikilink")』年度女性角色前十名。

## 作品列表

### 畫集

  - 四季童子畫集―Colorful wind ISBN 4829191260

### 插圖、插畫

  - [驚爆危機](../Page/驚爆危機.md "wikilink")，原名《》（著，[富士見Fantasia文庫](../Page/富士見Fantasia文庫.md "wikilink")）

  - ，原名《》（著，[角川Sneaker文庫](../Page/角川Sneaker文庫.md "wikilink")）

  - ，原名《》（著，[HJ文庫](../Page/HJ文庫.md "wikilink")）

  - 龍捲風，原名《》（著，HJ文庫）

  - [Monster Collection](../Page/Monster_Collection.md "wikilink")・ノベル
    イエル編、エルリク編、マリア編（著，富士見Fantasia文庫）

  - 變換多姿的少女，原名《》（著，富士見Fantasia文庫）

  - （著，角川Sneaker文庫）

  - （著，）

  - [Battle
    Spirits](../Page/Battle_Spirits.md "wikilink")，原名《》（魔女ナージャ・ナイフ投げのジャグリーン・エンジェルボイス）

  - ，原名《》（異海の剣豪 リングレイ）

  - （著，Hero文庫）

  - （著，Hero文庫）

  - （著，[GA文庫](../Page/GA文庫.md "wikilink")）

  - [七星的刻印使](../Page/七星的刻印使.md "wikilink")，原名《》（[涼暮皐著](../Page/涼暮皐.md "wikilink")，HJ文庫）

### 遊戲

  - （系列之一）

  -   -
      -
      -
  -
  - [聖火降魔錄 覺醒](../Page/聖火降魔錄_覺醒.md "wikilink") （追加コンテンツ用描き下ろし）

## 外部連結

  - [風の色](http://shikidoji.blog9.fc2.com/) - 四季童子的部落格

  -
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")