[Sichuan_pickles_in_Beijing_(20161122163831).jpg](https://zh.wikipedia.org/wiki/File:Sichuan_pickles_in_Beijing_\(20161122163831\).jpg "fig:Sichuan_pickles_in_Beijing_(20161122163831).jpg")
**四川泡菜**（有的地方叫“泡咸菜”或“泡酸菜”），
是[中國](../Page/中國.md "wikilink")[重庆和](../Page/重庆.md "wikilink")[四川一带家喻戶曉的一種](../Page/四川.md "wikilink")[佐餐食品](../Page/佐餐食品.md "wikilink")。在川渝的筵席、宴會中，在品嘗各味佳餚之餘，最後上幾色泡菜（稱為「隨飯菜」），以調節口味，也有醒酒解膩的特殊效果。

製作泡菜用料考究，它是用[川鹽](../Page/川鹽.md "wikilink")、[料酒](../Page/料酒.md "wikilink")、[白酒](../Page/白酒.md "wikilink")、[紅糖及多種香料製成鹽水](../Page/紅糖.md "wikilink")，用特製的土陶泡菜壇作盛器，然後將四季可取的根、莖、瓜、果、葉菜（如各種[蘿蔔](../Page/蘿蔔.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[子薑](../Page/子薑.md "wikilink")、[苦瓜](../Page/苦瓜.md "wikilink")、[茄子](../Page/茄子.md "wikilink")、[豇豆](../Page/豇豆.md "wikilink")、[蒜薹](../Page/蒜薹.md "wikilink")，[蓮白](../Page/莲花白.md "wikilink")、[青菜等](../Page/青菜.md "wikilink")）洗淨投入，
蓋嚴密封，經一定時間的乳酸發酵後，而成菜的。

四川泡菜易於貯存，取食方便，既可直接入饌，又可作輔料。如泡菜魚、酸菜雞豆花湯，以及魚香昧菜肴必須的泡生薑，泡辣椒等，均能增加菜肴的風味特色。

## 原料

泡菜的原料是蔬菜的根、莖、葉、果，但並非所有的蔬菜都可作泡菜原料。泡菜原料應選擇色澤鮮豔、質地脆嫩、不乾縮、不皺皮、無腐爛、無蟲傷和損傷者。

  - 春季蔬菜

<!-- end list -->

  -
    如[青菜](../Page/青菜.md "wikilink")、[萵苣](../Page/萵苣.md "wikilink")、[蒜薹](../Page/蒜薹.md "wikilink")、[蓮白的葉和莖](../Page/莲花白.md "wikilink")、[竹筍](../Page/竹筍.md "wikilink")、[白菜苔等](../Page/白菜苔.md "wikilink")；

<!-- end list -->

  - 夏季蔬菜

<!-- end list -->

  -
    如大小[辣椒](../Page/辣椒.md "wikilink")、[豇豆](../Page/豇豆.md "wikilink")、[四季豆](../Page/四季豆.md "wikilink")、[青黃豆](../Page/青黃豆.md "wikilink")、[豌豆](../Page/豌豆.md "wikilink")、[蠶豆](../Page/蠶豆.md "wikilink")、[大蒜](../Page/大蒜.md "wikilink")、[苦瓜](../Page/苦瓜.md "wikilink")、[黃瓜](../Page/黃瓜.md "wikilink")、[苤蘭](../Page/苤蘭.md "wikilink")、[茄子](../Page/茄子.md "wikilink")、[洋蔥等](../Page/洋蔥.md "wikilink")；

<!-- end list -->

  - 秋季蔬菜

<!-- end list -->

  -
    如[子薑](../Page/子薑.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[青椒](../Page/青椒.md "wikilink")、[菱白](../Page/菱白.md "wikilink")、[藕](../Page/藕.md "wikilink")、[甜椒](../Page/甜椒.md "wikilink")、[韭菜花](../Page/韭菜花.md "wikilink")、[刀豆](../Page/刀豆.md "wikilink")、[土瓜](../Page/土瓜.md "wikilink")、[草石蠶等](../Page/草石蠶.md "wikilink")；

<!-- end list -->

  - 冬季蔬菜

<!-- end list -->

  -
    如各種[蘿蔔](../Page/蘿蔔.md "wikilink")、[胡蘿蔔](../Page/胡蘿蔔.md "wikilink")、[青菜頭](../Page/青菜頭.md "wikilink")、[冬筍](../Page/冬筍.md "wikilink")、[花菜](../Page/花菜.md "wikilink")、[芹黃](../Page/芹黃.md "wikilink")、[茨菇](../Page/茨菇.md "wikilink")、[土耳瓜](../Page/土耳瓜.md "wikilink")、[大白菜](../Page/大白菜.md "wikilink")、[紅苕等](../Page/紅苕.md "wikilink")。

## 参见

  - [泡菜](../Page/泡菜.md "wikilink")
  - [韓式泡菜](../Page/韓式泡菜.md "wikilink")
  - [酸菜](../Page/酸菜.md "wikilink")

## 外部链接

  - [做四川泡菜的视频](http://www.spuweb.cn/component/option,com_seyret/Itemid,43/task,videodirectlink/id,538/)
  - [四川泡菜做法](http://www.spuweb.cn/content/view/3040/45/)

[Category:中国腌制蔬菜](../Category/中国腌制蔬菜.md "wikilink")
[Category:四川饮食](../Category/四川饮食.md "wikilink")
[Category:重庆饮食](../Category/重庆饮食.md "wikilink")