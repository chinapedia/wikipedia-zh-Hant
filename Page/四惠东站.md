**四惠东站**位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[1号线與](../Page/北京地铁1号线.md "wikilink")[八通线的换乘車站](../Page/北京地铁八通线.md "wikilink")，同时也是1号线东端的终点站。此站位於東四環外，編號為125和BT02。

## 位置

此站位于[朝阳区](../Page/朝阳区.md "wikilink")，[东四环外](../Page/北京四环路.md "wikilink")，[京通快速路北侧](../Page/京通快速路.md "wikilink")。[双沙铁路](../Page/双沙铁路.md "wikilink")（东北环线）在该站东侧上跨1号线四惠东折返线及八通线的路轨。

## 车站结构

四惠東站一层为站台层，二层为站厅层。1號線與八通線可經站台轉乘。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地上二层</strong></p></td>
<td><p>站厅</p></td>
<td><p>出入口、票务服务、自动售票机、一卡通充值</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地上一层</strong></p></td>
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>落客月台，往折返线方向</p></td>
<td><p>东 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>列车往<a href="../Page/土桥站_(北京).md" title="wikilink">土桥方向</a> <small>（<a href="../Page/高碑店站_(北京市).md" title="wikilink">高碑店</a>）</small></p></td>
<td><p>东 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 出口

此站一共有2个出口：A南、B北。

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>建国路、天洋·运河壹号、中国紫檀博物馆</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>康家沟、通惠家园</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 临近车站

[Category:北京市朝阳区地铁车站](../Category/北京市朝阳区地铁车站.md "wikilink")
[Category:1999年啟用的鐵路車站](../Category/1999年啟用的鐵路車站.md "wikilink")
[Category:2003年启用的铁路车站](../Category/2003年启用的铁路车站.md "wikilink")