**四氯乙烯**，又称全氯乙烯，是一种有机化学品，被广泛用于[干洗和](../Page/干洗.md "wikilink")[金属除油](../Page/金属.md "wikilink")，也被用来制造其他化学品和消費品。

室温下是不易燃的液体。容易挥发，有刺激的甜味。很多人在空气含有百万分之一四氯乙烯的时候就可以闻到。

1821年[麥可·法拉第第一次加热](../Page/麥可·法拉第.md "wikilink")[六氯乙烷使之分解为四氯乙烯和氯气](../Page/六氯乙烷.md "wikilink")。

## 用途

四氯乙烯對有機物是絕佳的溶劑。此外它具有[揮發性](../Page/揮發性.md "wikilink")、高度穩定、不可燃等特點。因為這些原因，它被廣泛使用在乾洗上。它在參雜其他[有機氯化合物後](../Page/有機氯化合物.md "wikilink")，也用來為金屬除油。

## 健康與安全

四氯乙烯的毒性介於中與低之間。即使四氯乙烯在乾洗與去油被廣泛使用，人類受傷的報告並不常見。\[1\]

## 参考文献

[Category:氯代烯烃](../Category/氯代烯烃.md "wikilink")
[Category:卤代烃类溶剂](../Category/卤代烃类溶剂.md "wikilink")
[Category:空气污染物](../Category/空气污染物.md "wikilink")
[Category:IARC第2A类致癌物质](../Category/IARC第2A类致癌物质.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")
[Category:全氯化合物](../Category/全氯化合物.md "wikilink")

1.