**四氯化硅**是化学式为SiCl<sub>4</sub>的[无机化合物](../Page/无机化合物.md "wikilink")，1823年由[永斯·贝采利乌斯首次发现](../Page/永斯·贝采利乌斯.md "wikilink")。

## 化学

四氯化硅室温下为无色粘稠液体，可通过[硅与](../Page/硅.md "wikilink")[氯气反应制备](../Page/氯气.md "wikilink")：

  -
    Si + 2 Cl<sub>2</sub> → SiCl<sub>4</sub>

与[四氯化碳不同](../Page/四氯化碳.md "wikilink")，四氯化硅与水迅速反应生成[二氧化硅和](../Page/二氧化硅.md "wikilink")[氯化氢](../Page/氯化氢.md "wikilink")。该水解速率差异归咎于硅原子更大的[原子半径](../Page/原子半径.md "wikilink")、硅原子存在的3d轨道以及Si-Cl键较低的键能。

  -
    SiCl<sub>4</sub> + 2 H<sub>2</sub>O → SiO<sub>2</sub> + 4 HCl

与[甲醇或](../Page/甲醇.md "wikilink")[乙醇反应则得到](../Page/乙醇.md "wikilink")和[硅酸四乙酯](../Page/硅酸四乙酯.md "wikilink")：

  -
    SiCl<sub>4</sub> + 4 ROH → Si(OR)<sub>4</sub> + 4 HCl

高温下与硅发生[归中反应](../Page/归中反应.md "wikilink")：

  -
    Si + SiCl<sub>4</sub> → Si<sub>2</sub>Cl<sub>6</sub> +
    [同系物](../Page/同系物.md "wikilink")

## 用途

四氯化硅沸点较低，可用于多次[分馏生产高纯](../Page/分馏.md "wikilink")[硅](../Page/硅.md "wikilink")；也可用于生产高纯的合成[石英玻璃](../Page/石英玻璃.md "wikilink")（熔融石英）。它可被氢气[还原为硅](../Page/还原.md "wikilink")，也可[水解为](../Page/水解.md "wikilink")[二氧化硅](../Page/二氧化硅.md "wikilink")。大量的高纯硅应用在[半导体工业和](../Page/半导体.md "wikilink")[光電池中](../Page/太阳能电池.md "wikilink")。

## 参考

## 参考资料

  -
## 外部链接

  - [MSDS—牛津大学](https://web.archive.org/web/20060517230517/http://physchem.ox.ac.uk/MSDS/SI/silicon_tetrachloride.html)

[Category:卤化硅](../Category/卤化硅.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")