**四溴双酚A**為白色粉末，用作[塑料制品添加剂](../Page/塑料.md "wikilink")，是常用的[溴化阻燃劑之一](../Page/溴化阻燃劑.md "wikilink")。

## 合成

四溴双酚A是[双酚A的](../Page/双酚A.md "wikilink")[衍生物](../Page/衍生物.md "wikilink")，製造四溴双酚A時，双酚A也是原料之一。大部份商業上使用的四溴双酚A純度不高，是由許多溴化程度不同的化合物混合而成。不過這不影響商業上的使用，因為這物質大多數是作為[阻燃劑](../Page/阻燃劑.md "wikilink")，而其關鍵是其平均的含溴量。因此商業上使用的四溴双酚A不需純化，即可作為一種低價、有效的阻燃劑。

## 参考资料

[Category:酚](../Category/酚.md "wikilink")
[Category:有机溴化合物](../Category/有机溴化合物.md "wikilink")
[Category:持久性有機污染物](../Category/持久性有機污染物.md "wikilink")