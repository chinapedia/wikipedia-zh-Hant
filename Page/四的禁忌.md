[ShanghaiMissingFloors.jpg](https://zh.wikipedia.org/wiki/File:ShanghaiMissingFloors.jpg "fig:ShanghaiMissingFloors.jpg")一棟大廈內電梯的按鈕面板，可以發現[4](../Page/4.md "wikilink")、[13](../Page/13.md "wikilink")、[14樓都被省略](../Page/14.md "wikilink")\]\]
**四的禁忌**，指基於對「[四](../Page/四.md "wikilink")」這個[數字的发音联想的](../Page/數字.md "wikilink")[迷信之現象](../Page/迷信.md "wikilink")，而厭惡、排斥、迴避带「[四](../Page/四.md "wikilink")」的事物。常出現於[漢字文化圈內的](../Page/漢字文化圈.md "wikilink")[中國大陸](../Page/中華人民共和國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[日本](../Page/日本.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[越南以及](../Page/越南.md "wikilink")[東南亞的](../Page/東南亞.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")。

## 原因

|                                  | 讀音              |
| -------------------------------- | --------------- |
| 四                                | 死               |
| [官話](../Page/官話.md "wikilink")   | sì              |
| [吳語](../Page/吳語.md "wikilink")   | sy<sup>3</sup>  |
| [客家話](../Page/客家話.md "wikilink") | si<sup>4</sup>  |
| [粵語](../Page/粵語.md "wikilink")   | sei<sup>3</sup> |
| [越南语](../Page/越南语.md "wikilink") | tứ              |
| [閩南語](../Page/閩南語.md "wikilink") | sì,sù           |
| [日語](../Page/日語.md "wikilink")   | shi             |
| [朝鮮語](../Page/朝鮮語.md "wikilink") | sa              |
|                                  |                 |

[漢字文化圈對](../Page/漢字文化圈.md "wikilink")「四」字的厭惡起因於其[發音容易讓人聯想到](../Page/發音.md "wikilink")[死](../Page/死.md "wikilink")。因為在[漢語的大部份](../Page/漢語.md "wikilink")[方言中](../Page/方言.md "wikilink")，「四」字的發音和「死」字仅仅在[声调上有区别](../Page/汉语#汉语声调.md "wikilink")。而在[日語和](../Page/日文汉字.md "wikilink")[朝鮮語中汉字的读音没有声调](../Page/朝鮮漢字.md "wikilink")，导致「四」和「死」兩字的發音更是完全相同。故長期以來民間常視「四」為不吉利的[數字](../Page/數字.md "wikilink")，並且在一些敏感的場合中刻意迴避。

## 现象

最常見的迴避措施之一為跳過含有「[4](../Page/4.md "wikilink")」的數字。在前述地区中，樓層編號常常跳過尾數是「4」的數字，例如將實際的4樓編為[5樓](../Page/5.md "wikilink")，形成從[3樓上去後就變成](../Page/3.md "wikilink")5樓的現象。見證生死的[醫院對這項禁忌尤為敏感](../Page/醫院.md "wikilink")，樓層、看診序號與病房編號都盡可能避免含有「4」，這種現象在其[電梯內最容易觀察到](../Page/升降机.md "wikilink")。近年，[香港部份住宅樓宇連](../Page/香港.md "wikilink")40至49樓也全部跳過，即39樓的樓上已經是50樓，如[凱旋門](../Page/凱旋門_\(香港\).md "wikilink")\[1\]、[萬景峯](../Page/萬景峯.md "wikilink")\[2\]等，但也有人認為「消失的樓層」反而帶來另一種恐怖感，所以有些措施是改以[拉丁字母](../Page/拉丁字母.md "wikilink")「[F](../Page/F.md "wikilink")」來代替「4」（因為「F」為「4」的[英語](../Page/英語.md "wikilink")「Four」的首字母），有些則改以「3A」這種附屬的標記形式來取代。某些东西方文化融汇的地方，如香港或[新加坡](../Page/新加坡.md "wikilink")，某些建筑物内可能会出现13、14和其他带4的楼层全部“不翼而飞”的现象。在[房地產上](../Page/房地產.md "wikilink")，4樓有時變成同一建築物中市場價值較低的樓層，又或者業主不願低估賣出而將其轉為出租物件，衍生4樓住戶搬遷率較高的現象。

除了樓層編號之外，在[台灣有許多](../Page/台灣.md "wikilink")[餐廳](../Page/餐馆.md "wikilink")、[飯店在詢問客人的人數時](../Page/酒店.md "wikilink")，會技巧性地略過「四（位）」，改稱「三加一（位）」。在某些地区，[車牌編號也已開始跳過](../Page/車輛號牌.md "wikilink")「4」，如台灣（詳見[台灣汽車號牌中的介紹](../Page/台灣汽車號牌#忌諱.md "wikilink")）和中國海南[海口](../Page/海口.md "wikilink")\[3\]。甚至在公共交通领域，[成都地铁](../Page/成都地铁.md "wikilink")[4号线列车编号也没有](../Page/成都地铁4号线.md "wikilink")444号。

[北伐战争时期](../Page/北伐战争.md "wikilink")，[國民革命軍第四軍](../Page/國民革命軍第四軍.md "wikilink")（粤军）是头等主力部队。受此影响，朱毛红军也自称为[中国工农红军第四军](../Page/中国工农红军第四军.md "wikilink")。鄂豫皖红军的主力部队也自称为红四军，后扩编为[红四方面军](../Page/红四方面军.md "wikilink")。[第二次国共内战后期](../Page/第二次国共内战.md "wikilink")，解放军的五个[野战军以东北的](../Page/野战军.md "wikilink")[第四野战军最为强大](../Page/第四野战军.md "wikilink")。

不過基於宣揚[科學](../Page/科學.md "wikilink")[文明的](../Page/文明.md "wikilink")[意識型態或實務上的](../Page/意識型態.md "wikilink")[技術問題等原因](../Page/技術.md "wikilink")，有更多時候是無法採用跳過「4」的方法來編號，例如[地址](../Page/地址.md "wikilink")、[車牌](../Page/車輛號牌.md "wikilink")、[電話號碼等等](../Page/電話.md "wikilink")。

## 反例

對「四」的避諱不是絕對的，實際上有很多的不避諱的例子或者反例\[4\]。

### 傳統文化

中国傳統认为[偶数代表吉祥](../Page/偶数.md "wikilink")，而四属于偶数，因此并不代表凶兆。另外在道教文化中，「四」代表[四象](../Page/四象.md "wikilink")，有圆满之意。又如年分[四季](../Page/四季.md "wikilink")，地分[四方](../Page/四方位.md "wikilink")，城市有[四郊](../Page/四郊.md "wikilink")，盖房要[四至](../Page/四至.md "wikilink")。送礼时应送四、六或八样，婚嫁时应送[四彩礼](../Page/四彩礼.md "wikilink")，以示吉祥\[5\]。[風水上](../Page/風水.md "wikilink")「四」代表四綠[文昌星](../Page/文昌星.md "wikilink")，有利[學生和](../Page/學生.md "wikilink")[文職工作者](../Page/白領.md "wikilink")。

現時也有繼承四字稱號，有[四大天王](../Page/四大天王.md "wikilink")、[四大金剛](../Page/四大金剛.md "wikilink")、[四大美人等](../Page/四大美人.md "wikilink")。

[麻將裏更是與](../Page/麻將.md "wikilink")「四」字離不開，一些高難度的和牌牌型役名也帶有「四」字，諸如[大四喜](../Page/四喜和#大四喜.md "wikilink")、[四槓子](../Page/四槓子.md "wikilink")、[四暗刻等](../Page/四暗刻.md "wikilink")；由於它們可以為玩家取得十分高的翻數（點數），故縱使名稱是有「四」字，不少雀士都很想甚至渴望能和出這些牌型。

### 現代文化

在西方音樂領域中，第四階的[唱名為Fa](../Page/唱名.md "wikilink")，[簡譜記作](../Page/簡譜.md "wikilink")4，其[普通話讀音為](../Page/普通話.md "wikilink")「[發](../Page/發.md "wikilink")」。

其他反例便是[棒球打者中的第四棒](../Page/棒球.md "wikilink")。因為在棒球戰術的需要下，第四棒通常由隊中最擅於長打的人擔任，即該隊的打擊王牌，所以第四棒的位置是棒球打者所追求的榮耀之一。

[冰壶比赛](../Page/冰壶.md "wikilink")，最后一位出场的选手被称为“四垒”（Skip），是队伍中的王牌。如[中国女子冰壶队的](../Page/中国女子冰壶队.md "wikilink")[王冰玉](../Page/王冰玉.md "wikilink")。

國際[籃球比賽或](../Page/籃球.md "wikilink")**職業籃球比賽以外**的籃球賽中，球衣號碼一般沒有一至三號，而是由四號作開始，而且不成文規定下，四號都是作為該球隊的隊長。

## 西方

西方世界稱東方的這種禁忌為「恐四症」（），是「Tetra」（四）與「Phobia」（[恐懼症](../Page/恐懼症.md "wikilink")）的[合字](../Page/合字.md "wikilink")。不過這種禁忌並不像恐懼症一樣是一種[精神疾病](../Page/精神疾病.md "wikilink")，而是一種迷信，是[文化與](../Page/文化.md "wikilink")[教育下的結果](../Page/教育.md "wikilink")。另外，「恐四症」一詞並非西方對東方文化的揶揄或嘲諷，與恐四症類似的，西方也有對「[13](../Page/13.md "wikilink")」這個數字的忌諱現象，稱為「[十三恐懼症](../Page/十三恐懼症.md "wikilink")」（英文為Triskaidekaphobia）。

## 参见

  - [迷信](../Page/迷信.md "wikilink")
  - [4](../Page/4.md "wikilink")
  - [13號星期五](../Page/13號星期五.md "wikilink")
  - [恐十三症](../Page/恐十三症.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[T](../Category/迷信.md "wikilink") [T](../Category/東亞文化圈.md "wikilink")
[Category:漢語同音詞](../Category/漢語同音詞.md "wikilink")

1.  [凱旋門摩天閣30至59樓的平面圖，可見34樓、40至49樓、53、54及58樓都被跳過](http://www.thearch.com.hk/pdf/sky/sky_30th.pdf)

2.  [萬景峯平面圖附註](http://www.visioncity.com.hk/notes_on_floor_plan_chi.html)

3.  [海口私家车号牌统统不带“4”部分车主遗憾](http://society.people.com.cn/GB/1062/6081377.html)
4.  [浅谈英汉数字的文化差异与翻译](http://www.lwlm.com/html/2008-06/48262.htm)
5.  [婚礼习俗演进及变异](http://www.mlovem.com/hqzs-zy.asp?anclassid=20&nclassid=68&id=778)