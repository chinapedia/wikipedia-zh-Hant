**回良玉**（），[回族](../Page/回族.md "wikilink")，[吉林](../Page/吉林省.md "wikilink")[榆树人](../Page/榆树市.md "wikilink")；[中国共产党和](../Page/中国共产党.md "wikilink")[中华人民共和国前](../Page/中华人民共和国.md "wikilink")[领导人之一](../Page/党和国家领导人.md "wikilink")，是农业领域工作的主要领导人。1966年4月加入[中国共产党](../Page/中国共产党.md "wikilink")，是[中共第十四屆中央候補委員](../Page/中国共产党第十四届中央委员会候补委员列表.md "wikilink")，[十五](../Page/中国共产党第十五届中央委员会委员列表.md "wikilink")、[十六](../Page/中国共产党第十六届中央委员会委员列表.md "wikilink")、[十七屆中央委員](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")，十六、十七屆[中共中央政治局委员](../Page/中国共产党中央政治局.md "wikilink")。曾任两届[中华人民共和国国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")。

## 学历

早年从吉林省农业学校毕业后，在1984年至1987年间，由[中共吉林省委党校函授班进修后](../Page/中国共产党吉林省委员会.md "wikilink")，获得[大专文凭](../Page/大专.md "wikilink")。但是，回良玉长期在农业领域工作；从一名普通县农业局干部，一步一步地积累自己的政治资历；被认为是中共高层少见的农业专家和理论人才。同时，作为中共少数民族干部，也是他获得提拔的因素之一。

## 从政经历

1964年从吉林省农业学校毕业后，回良玉被分配到家乡[榆树县农业局工作](../Page/榆树市.md "wikilink")。[文化大革命开始后](../Page/文化大革命.md "wikilink")，他曾被下放到[五七干校劳动](../Page/五七干校.md "wikilink")。1969年恢复工作后，就一直在家乡任职；历任榆树县[革命委员会政治部干事](../Page/革命委员会_\(文革\).md "wikilink")，县委组织部副部长，于家公社党委书记，[榆树县委副书记等职](../Page/榆树市.md "wikilink")。1977年文革刚结束，回良玉就得到了一次个人政治飞跃，被拔攫为吉林省农业局副局长。由一个县（处）级干部，直接被提升为副厅（局）级。此后，他又历任中共[吉林省](../Page/吉林省.md "wikilink")[白城地委副书记](../Page/白城市.md "wikilink")、[白城地区行署专员](../Page/白城地区.md "wikilink")，[中共吉林省委常委](../Page/中国共产党吉林省委员会.md "wikilink")、省委农村政策研究室主任、省委农工部部长；1987年升任[吉林省人民政府副省长](../Page/吉林省人民政府.md "wikilink")。

1990年10月，回良玉受推荐进入[中共中央政策研究室](../Page/中共中央政策研究室.md "wikilink")，任副主任、负责农业理论研究。由于中共党内缺乏懂农业的官员，在[中共十四大召开之前](../Page/中国共产党第十四次全国代表大会.md "wikilink")，回良玉被安排到地方历练。此后，他历任[中共湖北省委副书记](../Page/中国共产党湖北省委员会.md "wikilink")、湖北省政协主席，农业大省[安徽省的省长](../Page/安徽省.md "wikilink")，后又晋升为[中共安徽省委书记](../Page/中国共产党安徽省委员会.md "wikilink")；1999年又调任经济大省[中共江苏省委书记](../Page/中国共产党江苏省委员会.md "wikilink")；并在[中共十六届一中全会上](../Page/中国共产党第十六届中央委员会.md "wikilink")，进入[中共中央政治局](../Page/中国共产党中央政治局.md "wikilink")。

2003年3月，回良玉出任负责农业的[国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")，同时任[国家防汛抗旱总指挥部的总指挥长](../Page/国家防汛抗旱总指挥部.md "wikilink")、[国务院抗震救灾总指挥部总指挥](../Page/国务院抗震救灾总指挥部.md "wikilink")；并于2008年连任，是两届[温家宝政府中](../Page/温家宝.md "wikilink")，唯一连任的[副总理](../Page/中华人民共和国国务院副总理.md "wikilink")。

## 参考文献

  - [新华网：回良玉简历](http://news.xinhuanet.com/ziliao/2002-02/22/content_285979.htm)
  - 《第四代》作者：宗海仁，香港明镜出版社，2002年11月版

{{-}}

[Category:中国共产党第十六届中央政治局委员](../Category/中国共产党第十六届中央政治局委员.md "wikilink")
[Category:中国共产党第十七届中央政治局委员](../Category/中国共产党第十七届中央政治局委员.md "wikilink")
[Category:第11届国务院副总理](../Category/第11届国务院副总理.md "wikilink")
[Category:第10届国务院副总理](../Category/第10届国务院副总理.md "wikilink")
[Category:第八届全国人大代表](../Category/第八届全国人大代表.md "wikilink")
[Category:湖北省全國人民代表大會代表](../Category/湖北省全國人民代表大會代表.md "wikilink")
[Category:回族全國人大代表](../Category/回族全國人大代表.md "wikilink")
[Category:湖北省政協主席](../Category/湖北省政協主席.md "wikilink")
[Category:中华人民共和国吉林省副省长](../Category/中华人民共和国吉林省副省长.md "wikilink")
[Category:中共江苏省委书记](../Category/中共江苏省委书记.md "wikilink")
[Category:中共安徽省委书记](../Category/中共安徽省委书记.md "wikilink")
[Category:中共湖北省委副書記](../Category/中共湖北省委副書記.md "wikilink")
[Category:中共吉林省委常委](../Category/中共吉林省委常委.md "wikilink")
[Category:中华人民共和国回族政治人物](../Category/中华人民共和国回族政治人物.md "wikilink")
[Category:中华人民共和国时期回族中国共产党党员](../Category/中华人民共和国时期回族中国共产党党员.md "wikilink")
[Category:榆树人](../Category/榆树人.md "wikilink")
[Category:回族人](../Category/回族人.md "wikilink")
[L](../Category/回姓.md "wikilink")
[Category:中华人民共和国回族党和国家领导人](../Category/中华人民共和国回族党和国家领导人.md "wikilink")
[Category:中国共产党第十四届中央委员会候补委员](../Category/中国共产党第十四届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中共中央政策研究室副主任](../Category/中共中央政策研究室副主任.md "wikilink")