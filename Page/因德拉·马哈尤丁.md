**因德拉·布特拉·马哈尤丁**（**Indra Putra
Mahayuddin**，）生于[怡保](../Page/怡保.md "wikilink")，[马来西亚职业足球运动员](../Page/马来西亚.md "wikilink")，可以胜任前场的多个位置，包括[前鋒和](../Page/前锋_\(足球\).md "wikilink")[边锋](../Page/中场.md "wikilink")。

## 足球生涯

马哈尤丁十多岁的时候在[霹雳足球协会的球校学习](../Page/霹雳足球协会.md "wikilink")，2002年进球球队一线帮助霹雳足协夺得了马来西亚联赛的冠军，2004年以较高的转会费转会去了[彭亨足球协会](../Page/彭亨足球协会.md "wikilink")，帮助球队夺取了新创立的马来西亚超级联赛冠军。

这位左脚将在2003年[越南](../Page/越南.md "wikilink")[河内的](../Page/河内.md "wikilink")[东南亚运动会上代表](../Page/东南亚运动会.md "wikilink")[马来西亚国家足球队出场](../Page/马来西亚国家足球队.md "wikilink")，之前还参加过2002年的东南亚老虎杯比赛，帮助马来西亚队取得了赛事的第四名。霹雳队主教练表示马哈尤丁是马来西亚这一年龄段最出色的足球运动员之一。在2004年[马来西亚超级联赛中](../Page/马来西亚超级联赛.md "wikilink")21场比赛射入15球，成为联赛的最佳射手。

[马来西亚国家足球队主教练](../Page/马来西亚国家足球队.md "wikilink")[诺里赞·巴卡尔将马哈尤丁招入了](../Page/诺里赞·巴卡尔.md "wikilink")2007年7月由[马来西亚与](../Page/马来西亚.md "wikilink")[泰国](../Page/泰国.md "wikilink")、[越南](../Page/越南.md "wikilink")、[印尼合办的](../Page/印尼.md "wikilink")[亚洲杯足球赛中](../Page/亚洲杯.md "wikilink")。
[2007年亚洲杯首场对阵](../Page/2007年亞洲盃足球賽.md "wikilink")[中国队的比赛中马哈尤丁射入一球](../Page/中国国家足球队.md "wikilink")，但是最终球队以1-5负于对手。

## 荣誉

  - [马来西亚超级联赛](../Page/马来西亚超级联赛.md "wikilink"): 2002, 2003 and 2004
  - [马来西亚足协杯](../Page/马来西亚足协杯.md "wikilink"): 2004 和2005/06

## 外部链接

  - [Indra Putra Mahayuddin -
    goalzz.com](http://www.goalzz.com/main.aspx?player=16015)

[Category:马来西亚足球运动员](../Category/马来西亚足球运动员.md "wikilink")
[Category:馬來西亞馬來人](../Category/馬來西亞馬來人.md "wikilink")
[Category:怡保人](../Category/怡保人.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")