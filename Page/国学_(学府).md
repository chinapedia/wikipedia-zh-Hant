**國學**指[國家學府](../Page/國家.md "wikilink")，即[古代中國和周邊地區如](../Page/古代中國.md "wikilink")[朝鮮](../Page/朝鮮.md "wikilink")、[越南](../Page/越南.md "wikilink")、[日本](../Page/日本.md "wikilink")、[琉球等國的中央学府](../Page/琉球.md "wikilink")，为官学体系的最高学府。為古之大學。

## 中國

[虞之](../Page/虞.md "wikilink")[上庠](../Page/上庠.md "wikilink")，[夏之](../Page/夏.md "wikilink")[東序](../Page/東序.md "wikilink")，[殷之](../Page/殷.md "wikilink")[瞽宗](../Page/瞽宗.md "wikilink")，[周之](../Page/周.md "wikilink")[辟雍](../Page/辟雍.md "wikilink")，[漢後之](../Page/漢.md "wikilink")[太學](../Page/太學.md "wikilink")，[隋後之](../Page/隋.md "wikilink")[國子監](../Page/國子監.md "wikilink")，皆為國學。

國學先[秦上古之大學](../Page/秦.md "wikilink")，一般貴族子弟才能入學；庶民子弟入讀的學校則稱為小學，如[舜帝時之下庠](../Page/舜帝.md "wikilink")、夏朝之西序、[商朝之左學](../Page/商朝.md "wikilink")、周朝之虞庠。到了秦漢，[封建制度瓦解](../Page/封建制度.md "wikilink")，漢朝建立[太學以後](../Page/太學.md "wikilink")，平民子弟方可入讀大學。

## 参考

  - [太学](../Page/太学.md "wikilink")
  - [国子监](../Page/国子监.md "wikilink")
  - [成均館](../Page/成均館.md "wikilink")
  - [大學寮](../Page/大學寮.md "wikilink")
  - [府学](../Page/府学.md "wikilink")
  - [县学](../Page/县学.md "wikilink")
  - [藩學](../Page/藩學.md "wikilink")
  - [乡学](../Page/乡学.md "wikilink")
  - [社学](../Page/社学.md "wikilink")

[國學_(學府)](../Category/國學_\(學府\).md "wikilink")