[政治上的國家](../Page/政治.md "wikilink")[分裂是指](../Page/分裂.md "wikilink")[國家的](../Page/國家.md "wikilink")[主權受到另一](../Page/主權.md "wikilink")[政治實體干擾而無法於部份](../Page/政治實體.md "wikilink")[地區正常運作](../Page/地區.md "wikilink")。此一情況往往會受到各種[地方](../Page/地方.md "wikilink")[分裂主義](../Page/分裂主義.md "wikilink")[壓力或](../Page/壓力.md "wikilink")[強權](../Page/強權.md "wikilink")[國家壓力而促使其地區實行](../Page/國家.md "wikilink")[獨立](../Page/獨立.md "wikilink")。政治上的分裂是相對於「[政治統一](../Page/政治統一.md "wikilink")」而言。

## 分裂形式

  - 中央政權非[和平的](../Page/和平.md "wikilink")[政黨輪替](../Page/政黨輪替.md "wikilink")，新舊執政勢力以[武力對抗](../Page/武力.md "wikilink")，終使一方更改[國號](../Page/國號.md "wikilink")，導致国家分裂。
  - [地方政權或地方](../Page/地方政權.md "wikilink")[軍隊脫離中央政權控制](../Page/軍隊.md "wikilink")。（[軍事政變](../Page/軍事政變.md "wikilink")）
  - [種族或](../Page/種族.md "wikilink")[宗教之間的不協調](../Page/宗教.md "wikilink")，導致的武力對峙，甚至[武裝衝突](../Page/武裝衝突.md "wikilink")。其中可能包含不被承认及[非法](../Page/非法.md "wikilink")[組織和](../Page/组织_\(社会学\).md "wikilink")[恐怖主義](../Page/恐怖主義.md "wikilink")。
  - [聯邦制國家的省級或市級政府要求脫離中央政府](../Page/聯邦制.md "wikilink")。
  - 社會形態轉制所引發的[矛盾](../Page/矛盾.md "wikilink")，導致各[省](../Page/省.md "wikilink")[市](../Page/市.md "wikilink")（[州份或](../Page/州份.md "wikilink")[联邦主体](../Page/联邦主体.md "wikilink")）獨立[自治](../Page/自治.md "wikilink")，最終各自獨立。
  - 外国的[军事](../Page/军事.md "wikilink")[侵略強行剝離](../Page/侵略.md "wikilink")。
  - 外国势力的影响，导致政局不稳，使部分地区从[国际上独立](../Page/国际.md "wikilink")。
  - 外国介入地區戰爭，導致分裂的國家和政權，在冷戰時多次出現，導致部分國家至今仍然分裂。

## 分裂的例子

### 遠古至18世紀

  - [古埃及](../Page/古埃及.md "wikilink")[古王國時期後期](../Page/古王國時期.md "wikilink")（[埃及第七王朝](../Page/埃及第七王朝.md "wikilink")）地方[領袖紛紛脫離](../Page/領袖.md "wikilink")[中央政府](../Page/中央政府.md "wikilink")（約前2180年）
  - [以色列王國分裂成](../Page/以色列王國.md "wikilink")[北國以色列與南國](../Page/以色列王國_\(後期\).md "wikilink")[猶大王國](../Page/猶大王國.md "wikilink")（前930年代）
  - [中國古代的](../Page/中國.md "wikilink")[春秋時期的](../Page/春秋時期.md "wikilink")[楚國自立為王](../Page/楚國.md "wikilink")，與[周室分庭抗禮](../Page/周朝.md "wikilink")（前710年代）
  - 中國古代的[戰國時期的](../Page/戰國時期.md "wikilink")[韩](../Page/韩国_\(战国\).md "wikilink")、[赵](../Page/赵国.md "wikilink")、[魏](../Page/魏国_\(战国\).md "wikilink")（[三晉](../Page/三晉.md "wikilink")）取代[晉國](../Page/晉國.md "wikilink")，史稱「[三家分晉](../Page/三家分晉.md "wikilink")」（前400年代）
  - 中國古代的[秦朝滅亡後](../Page/秦朝.md "wikilink")[楚漢相爭及](../Page/楚漢相爭.md "wikilink")[南越国](../Page/南越国.md "wikilink")（前206年－前111年）
  - 中國古代的[新朝滅亡後群雄並立](../Page/新朝.md "wikilink")（20年－36年）
  - [南北匈奴分立](../Page/匈奴#分裂時期.md "wikilink")（40年代）
  - 中國古代的九真、日南郡脫離[東漢建立](../Page/東漢.md "wikilink")[占城](../Page/占城.md "wikilink")
  - 中國古代的東漢末年群雄割據與[三國時代的](../Page/三國.md "wikilink")[曹魏](../Page/曹魏.md "wikilink")、[蜀漢](../Page/蜀漢.md "wikilink")、[東吳三国鼎立](../Page/東吳.md "wikilink")（180年代至280年代）
  - [歐洲古代的](../Page/歐洲.md "wikilink")[羅馬帝國分裂成](../Page/羅馬帝國.md "wikilink")[東羅馬帝國](../Page/東羅馬帝國.md "wikilink")（拜占庭帝國）與[西羅馬帝國](../Page/西羅馬帝國.md "wikilink")（280年代）
  - 中國古代的[晉朝的](../Page/晉朝.md "wikilink")[五胡十六國](../Page/五胡十六國.md "wikilink")（310年代至430年代）
  - 中國古代的[北朝](../Page/北朝.md "wikilink")，[北魏分裂成](../Page/北魏.md "wikilink")[東魏與](../Page/東魏.md "wikilink")[西魏](../Page/西魏.md "wikilink")（530年代）
  - 中國古代的[南朝的](../Page/南朝.md "wikilink")[陳朝與](../Page/陳朝.md "wikilink")[西梁](../Page/西梁.md "wikilink")（550年代）
  - 中国古代的[五代十国](../Page/五代十国.md "wikilink")（897年代-979年代）
  - [法蘭克王國按](../Page/法蘭克王國.md "wikilink")《[凡爾登條約](../Page/凡爾登條約.md "wikilink")》被瓜分為[東法蘭克王國](../Page/東法蘭克王國.md "wikilink")、[西法蘭克王國及](../Page/西法蘭克王國.md "wikilink")[中法蘭克王國](../Page/中法蘭克王國.md "wikilink")（840年代）
  - [朝鮮古代](../Page/朝鮮.md "wikilink")，[後百濟和](../Page/後百濟.md "wikilink")[後高句麗從](../Page/後高句麗.md "wikilink")[統一新羅分裂出去](../Page/統一新羅.md "wikilink")，[朝鮮半島進入](../Page/朝鮮半島.md "wikilink")[後三國時代](../Page/後三國時代.md "wikilink")（890年代至900年代）
  - 中國古代的[唐朝後期至](../Page/唐朝.md "wikilink")[五代十國時期藩鎮獨立及](../Page/五代十國.md "wikilink")[安南的](../Page/越南.md "wikilink")[丁朝](../Page/丁朝.md "wikilink")（850年代至980年年代）
  - [亞洲古代的](../Page/亞洲.md "wikilink")[大蒙古國與](../Page/大蒙古國.md "wikilink")[四大汗國](../Page/四大汗國.md "wikilink")
  - [卡爾馬聯合解体](../Page/卡爾馬聯合.md "wikilink")（1520年代）
  - 越南古代的[莫朝與](../Page/莫朝.md "wikilink")[後黎朝及後黎朝的](../Page/後黎朝.md "wikilink")[鄭阮紛爭](../Page/鄭阮紛爭.md "wikilink")（1533年－1592年）及（1627年－1673年）
  - [瀾滄王國分裂為](../Page/瀾滄王國.md "wikilink")[占巴塞王國](../Page/占巴塞王國.md "wikilink")、[琅勃拉邦王國](../Page/琅勃拉邦王國.md "wikilink")、[萬象王國三個小國](../Page/萬象王國.md "wikilink")（1690年代）

### 19世紀

  - [烏拉圭脫離](../Page/烏拉圭.md "wikilink")[巴西帝國的統治獨立](../Page/巴西帝國.md "wikilink")（1820年代）
  - [荷蘭](../Page/荷蘭.md "wikilink")、[比利時與](../Page/比利時.md "wikilink")[盧森堡](../Page/盧森堡.md "wikilink")（1830年代）
  - [大哥倫比亞共和國解體](../Page/大哥倫比亞共和國.md "wikilink")（1830年代）
  - [德克薩斯共和國自](../Page/德克薩斯共和國.md "wikilink")[墨西哥獨立出來](../Page/墨西哥.md "wikilink")（1830年代）
  - [中國近代的](../Page/中國近代.md "wikilink")[清朝與](../Page/清朝.md "wikilink")[太平天國](../Page/太平天國.md "wikilink")（1850年代）
  - [美國](../Page/美國.md "wikilink")[南方七州脫離聯邦](../Page/南方.md "wikilink")，成立[美利堅邦聯](../Page/美利堅邦聯.md "wikilink")（1860年代）

### 20世紀

  - [瑞典-挪威聯合解體](../Page/瑞典-挪威聯合解體.md "wikilink")（1900年代）
  - [西烏克蘭人民共和國脫離](../Page/西烏克蘭人民共和國.md "wikilink")[奧匈帝國](../Page/奧匈帝國.md "wikilink")（1910年代）
  - [漢志王國獨立](../Page/漢志王國.md "wikilink")（1910年代）
  - [奧匈帝國的解體](../Page/奧匈帝國#奧匈帝国的解体.md "wikilink")（1910年代至1920年代）
  - [奧斯曼帝國的分裂](../Page/奧斯曼帝國的分裂.md "wikilink")（1910年代至1920年代）
  - [愛爾蘭自由邦退出](../Page/愛爾蘭自由邦.md "wikilink")[大不列顛及愛爾蘭聯合王國](../Page/大不列顛及愛爾蘭聯合王國.md "wikilink")（1920年代）
  - [蒙古国与](../Page/蒙古国.md "wikilink")[中華民國](../Page/中華民國_\(大陸時期\).md "wikilink")（1920年代）
  - [義大利](../Page/義大利.md "wikilink")[北部的](../Page/倫巴底.md "wikilink")[義大利社會共和國與南部的](../Page/義大利社會共和國.md "wikilink")[義大利王國](../Page/義大利王國.md "wikilink")（1940年代）
  - [巴以衝突導致的](../Page/巴以衝突.md "wikilink")[以色列國和](../Page/以色列國.md "wikilink")[巴勒斯坦國脫離](../Page/巴勒斯坦國.md "wikilink")[英屬巴勒斯坦託管地分治](../Page/英屬巴勒斯坦託管地.md "wikilink")（1940年代）
  - [印巴分治導致分裂獨立的](../Page/印巴分治.md "wikilink")[印度](../Page/印度.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[孟加拉國](../Page/孟加拉國.md "wikilink")（1940年代及1970年代）
  - [大韓民國](../Page/大韓民國.md "wikilink")（南韓）與[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")（北韓）（1940年代）
  - [东德與](../Page/东德.md "wikilink")[西德](../Page/西德.md "wikilink")（1940年代）
  - [南越與](../Page/南越.md "wikilink")[北越](../Page/北越.md "wikilink")（1940年代）
  - [中華民國與](../Page/中華民國.md "wikilink")[中华人民共和国在](../Page/中华人民共和国.md "wikilink")[國共內戰後](../Page/國共內戰.md "wikilink")[兩岸分治](../Page/兩岸分治.md "wikilink")(1949年)
  - [馬里聯邦解體](../Page/馬里聯邦.md "wikilink")（1960年代）
  - [加丹加國與](../Page/加丹加國.md "wikilink")[剛果](../Page/剛果.md "wikilink")（1960年）\[1\]
  - [阿拉伯聯合共和國解散](../Page/阿拉伯聯合共和國.md "wikilink")（1960年代）
  - [馬來西亞與](../Page/馬來西亞.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")（1960年代）
  - [賽普勒斯與](../Page/賽普勒斯.md "wikilink")[北賽普勒斯](../Page/北賽普勒斯.md "wikilink")（1970年代）（未得到廣泛承認，因[塞浦路斯問題分裂](../Page/塞浦路斯問題.md "wikilink")）
  - [巴布亞新畿內亞脫離](../Page/巴布亞新畿內亞.md "wikilink")[澳洲獨立](../Page/澳洲.md "wikilink")（1970年代）
  - [納米比亞從](../Page/納米比亞.md "wikilink")[南非獨立](../Page/南非.md "wikilink")（1990年代）
  - [索馬利蘭從](../Page/索馬利蘭.md "wikilink")[索馬里獨立](../Page/索馬里.md "wikilink")（1990年代）
  - [苏联解体](../Page/苏联解体.md "wikilink")（1990年代）
  - [塞內岡比亞解散](../Page/塞內岡比亞.md "wikilink")（1990年代）
  - [喬治亞與](../Page/喬治亞.md "wikilink")[南奧塞提亞](../Page/南奧塞提亞.md "wikilink")（1990年代）（未得到廣泛承認）
  - [德涅斯特河沿岸從](../Page/德涅斯特河沿岸.md "wikilink")[摩爾多瓦](../Page/摩爾多瓦.md "wikilink")（1990年代）（未獲普遍承認）
  - [捷克斯洛伐克解體](../Page/天鵝絨分離.md "wikilink")（1990年代）
  - [南斯拉夫解體](../Page/南斯拉夫解體.md "wikilink")（1990年代至2000年代）
      - 塞爾維亞克拉伊納自治區脫離[克羅埃西亞獨立](../Page/克羅埃西亞.md "wikilink")（[塞爾維亞克拉伊納共和國](../Page/塞爾維亞克拉伊納共和國.md "wikilink")）
      - [塞爾維亞與](../Page/塞爾維亞.md "wikilink")[蒙特內哥羅](../Page/蒙特內哥羅.md "wikilink")（2006年）

### 21世紀

  - [印度尼西亞共和國與](../Page/印度尼西亞共和國.md "wikilink")[東帝汶民主共和國](../Page/東帝汶民主共和國.md "wikilink")（2002年）
  - [塞爾維亞和黑山解體](../Page/塞爾維亞和黑山.md "wikilink")，[黑山獨立](../Page/黑山.md "wikilink")（2006年）
  - [塞爾維亞共和國與](../Page/塞爾維亞共和國.md "wikilink")[科索沃共和國](../Page/科索沃共和國.md "wikilink")（2008年）
  - [蘇丹共和國與](../Page/蘇丹共和國.md "wikilink")[南蘇丹共和國](../Page/南蘇丹共和國.md "wikilink")（2011年）
  - [馬里共和國與](../Page/馬里共和國.md "wikilink")[阿扎瓦德](../Page/阿扎瓦德.md "wikilink")
    （2012年，隨著[馬里北部衝突結束其分裂而恢復統一](../Page/馬里北部衝突.md "wikilink")）
  - [烏克蘭与](../Page/烏克蘭.md "wikilink")[克里米亚共和國](../Page/克里米亚共和國.md "wikilink")（2014年）
  - [烏克蘭與](../Page/烏克蘭.md "wikilink")[顿涅茨克人民共和国](../Page/顿涅茨克人民共和国.md "wikilink")（2014年）
  - [烏克蘭與](../Page/烏克蘭.md "wikilink")[卢甘斯克人民共和国](../Page/卢甘斯克人民共和国.md "wikilink")（2014年）
  - [西班牙王国與](../Page/西班牙王国.md "wikilink")[加泰罗尼亚共和国](../Page/加泰罗尼亚共和国_\(2017年\).md "wikilink")（2017年）

## 目前的分裂勢力或思想

### 英國

  - [蘇格蘭獨立運動](../Page/蘇格蘭獨立運動.md "wikilink")

      - [蘇格蘭民族黨](../Page/蘇格蘭民族黨.md "wikilink")

  - [威爾斯獨立運動](../Page/威爾斯獨立運動.md "wikilink")

      - [威爾斯黨](../Page/威爾斯黨.md "wikilink")

  -
  - [康沃爾郡分離主義](../Page/康沃爾郡#分離主義與自治要求.md "wikilink")

  - **[新芬黨](../Page/新芬黨.md "wikilink")（謀求[北愛爾蘭與](../Page/北愛爾蘭.md "wikilink")[愛爾蘭共和國統一](../Page/愛爾蘭共和國.md "wikilink")）（曾经暴力谋求独立）**

### 西班牙

  - [加泰隆尼亞獨立運動](../Page/加泰隆尼亞獨立運動.md "wikilink")
  - [巴斯克地區分離主義](../Page/巴斯克地區.md "wikilink")
  - [納瓦拉民族主義](../Page/納瓦拉.md "wikilink")
  - **[埃塔](../Page/埃塔.md "wikilink")（暴力谋求独立）**
  - [休达和](../Page/休达.md "wikilink")[梅利利亚](../Page/梅利利亚.md "wikilink")

### 意大利

  - [威尼斯獨立運動](../Page/威尼斯獨立運動.md "wikilink")
  - [西西里岛独立运动](../Page/西西里岛.md "wikilink")

### 德國

  - [巴伐利亞](../Page/巴伐利亞#独立.md "wikilink")
      - [拜仁黨](../Page/拜仁黨.md "wikilink")

### 法國

  - [科西嘉民族解放陣線](../Page/科西嘉民族解放陣線.md "wikilink")

### 比利時

  - [荷語文化區獨立思想](../Page/荷語文化區.md "wikilink")\[2\]\[3\]\[4\]\[5\]\[6\]
      - [新佛蘭芒人聯盟](../Page/新佛蘭芒人聯盟.md "wikilink")

### 科索沃

  - [科索沃解放軍](../Page/科索沃解放軍.md "wikilink")（2000年解散）

### 俄罗斯

  - **[车臣独立运动](../Page/车臣.md "wikilink")（暴力谋求独立）**
  - [图瓦独立运动](../Page/图瓦.md "wikilink")
  - [鞑靼斯坦独立运动](../Page/鞑靼斯坦.md "wikilink")
  - [马里埃尔独立运动](../Page/马里埃尔.md "wikilink")
  - [薩哈共和國獨立運動](../Page/薩哈共和國.md "wikilink")
  - [布里亞特共和國獨立運動](../Page/布里亞特共和國.md "wikilink")

### 加拿大

  - [魁北克獨立運動](../Page/魁北克獨立運動.md "wikilink")
      - [魁北克解放陣線](../Page/魁北克解放陣線.md "wikilink")
      - [魁人政團](../Page/魁人政團.md "wikilink")

### 美國

  - [德克薩斯獨立運動](../Page/德克薩斯獨立運動.md "wikilink")

  - [波多黎各獨立黨](../Page/波多黎各獨立黨.md "wikilink")

  - [夏威夷獨立運動](../Page/夏威夷獨立運動.md "wikilink")

  -
### 加拿大和美國

  - [卡斯卡迪亞獨立運動](../Page/卡斯卡迪亞獨立運動.md "wikilink")
      -
### 中華人民共和國

  - **[西藏獨立運動](../Page/西藏獨立運動.md "wikilink")**
  - **[新疆獨立運動](../Page/新疆獨立運動.md "wikilink")**
  - [香港獨立運動](../Page/香港獨立運動.md "wikilink")
  - [澳門獨立運動](../Page/澳門獨立運動.md "wikilink")
  - [內蒙古獨立運動](../Page/內蒙古獨立運動.md "wikilink")
  - [湖南獨立運動](../Page/湖南獨立運動.md "wikilink")
  - [廣東獨立運動](../Page/廣東獨立運動.md "wikilink")

### 中華民國

  - [台湾独立运动](../Page/台湾独立运动.md "wikilink")

### 日本

  - [琉球獨立運動](../Page/琉球獨立運動.md "wikilink")
  - [与那国岛独立运动](../Page/与那国岛.md "wikilink")
  - [北海道](../Page/北海道.md "wikilink")[獨立運動](../Page/獨立運動.md "wikilink")

### 菲律賓

  - **[阿布沙耶夫](../Page/阿布沙耶夫.md "wikilink")（暴力谋求独立）**
  - **[摩洛民族解放陣線](../Page/摩洛人#獨立運動.md "wikilink")（暴力谋求独立）**
  - **[摩洛伊斯蘭解放陣線](../Page/摩洛人#獨立運動.md "wikilink")（暴力谋求独立）**

### 印度

  - [印度分離主義運動](../Page/印度分離主義運動.md "wikilink")
      - [阿薩姆聯合解放陣線](../Page/阿薩姆聯合解放陣線.md "wikilink")
      - [波多民族民主陣線](../Page/波多民族民主陣線.md "wikilink")
      - [波多解放猛虎力量](../Page/波多解放猛虎力量.md "wikilink")
      - [廓爾喀民族解放陣線](../Page/廓爾喀民族解放陣線.md "wikilink")
      - [特里普拉民族解放陣線](../Page/特里普拉民族解放陣線.md "wikilink")

### 印度尼西亚

  - **[自由亞齊運動](../Page/自由亞齊運動.md "wikilink")（曾经暴力谋求独立）**
  - [自由巴布亞運動](../Page/自由巴布亞運動.md "wikilink")

### 斯里蘭卡

  - **[泰米爾伊拉姆猛虎解放組織](../Page/泰米爾伊拉姆猛虎解放組織.md "wikilink")（曾经暴力谋求独立）**

### 坦桑尼亞

  - [桑給巴爾獨立思想](../Page/桑給巴爾#獨立運動.md "wikilink")

### 缅甸

  - **[克钦独立军](../Page/克钦独立军.md "wikilink")（暴力谋求独立）**

### 伊拉克

  - [庫爾德斯坦独立运动](../Page/伊拉克庫爾德斯坦.md "wikilink")

## 参看

  - [獨立](../Page/獨立.md "wikilink")
  - [獨立運動列表](../Page/獨立運動列表.md "wikilink")
  - [分離主義](../Page/分離主義.md "wikilink")
  - [分裂国家列表](../Page/分裂国家列表.md "wikilink")
  - [殘存國家](../Page/殘存國家.md "wikilink")
  - [私人國家](../Page/私人國家.md "wikilink")

## 參考文獻

[Category:分离主义](../Category/分离主义.md "wikilink")

1.  [加丹加省从刚果分裂，内战爆发](http://www.wst.net.cn/history/7.15/10.htm)
2.  [比利時主張分裂政黨成議會最大黨](http://atchinese.com/index.php?option=com_content&view=article&id=66980:2010-06-14-06-20-12&catid=22&Itemid=31)

3.  [主張分裂政黨大選獲勝　比利時恐釀國家分裂](http://www.chinareviewagency.net/doc/1013/5/3/5/101353569.html?coluid=70&kindid=1850&docid=101353569&mdate=0614110310)
4.  [分離黨贏取大選
    比利時解體隱患](http://orientaldaily.on.cc/cnt/china_world/20100615/00192_001.html)
5.  [獨立黨勝選 比利時面臨分裂](http://hk.epochtimes.com/10/6/15/119647.htm)
6.  [主張分裂政黨獲勝比利時恐解體](http://www.job853.com/MacauNews/news_list_show_macao.aspx?id=13815)