**国家歷史文化名城**由[中华人民共和国国务院确定及公布](../Page/中华人民共和国国务院.md "wikilink")，是於1982年根据[北京大学](../Page/北京大学.md "wikilink")[侯仁之](../Page/侯仁之.md "wikilink")、建设部[郑孝燮和](../Page/郑孝燮.md "wikilink")[故宫博物院](../Page/故宫博物院.md "wikilink")[单士元提议而建立的一种](../Page/单士元.md "wikilink")[文物保护机制](../Page/文物.md "wikilink")。被列入名单的均为保存文物特别丰富、具有重大历史价值或者纪念意义、而且正在延续使用的[城市](../Page/城市.md "wikilink")。目前中国公布了135个国家历史文化名城，其中琼山市已划为[海口市](../Page/海口市.md "wikilink")[琼山区](../Page/琼山区.md "wikilink")，虽然根据国务院相关批复文件“海口历史文化名城”不是“琼山历史文化名城”的扩展，但是住房城乡建设部、国家文物局在做统计报告及海口市政府在编制城市总体规划或者历史文化名城保护规划时通常会把二者合并为一处，故实际上共有134个国家历史文化名城。

## 历史

  - 第一批：1982年2月8日公布，共24处
  - 第二批：1986年12月8日公布，共38处
  - 第三批：1994年1月4日公布，共37处
  - 增补a：2001年8月10日公布，共1处
  - 增补b：2001年12月17日公布，共1处
  - 增补c：2004年10月1日公布，共1处
  - 增补d：2005年4月14日公布，共1处
  - 增补e：2007年3月9日公布，共1处
  - 增补f：2007年3月13日公布，共1处
  - 增补g：2007年3月18日公布，共2处
  - 增补h：2007年4月27日公布，共1处
  - 增补i：2007年5月6日公布，共1处
  - 增补j：2007年9月15日公布，共1处
  - 增补k：2009年1月2日公布，共1处
  - 增补l：2010年11月9日公布，共1处
  - 增补m：2011年1月24日公布，共2处
  - 增补n：2011年3月12日公布，共1处
  - 增补o：2011年3月14日公布，共1处
  - 增补p：2011年5月1日公布，共1处
  - 增补q：2011年11月2日公布，共1处
  - 增补r：2012年3月15日公布，共1处
  - 增补s：2012年6月28日公布，共1处
  - 增补t：2013年2月10日公布，共1处
  - 增补u：2013年5月18日公布，共1处
  - 增补v：2013年7月28日公布，共1处
  - 增补w：2013年11月18日公布，共1处
  - 增补x：2014年7月14日公布，共1处
  - 增补y：2014年8月6日公布，共1处
  - 增补z：2015年6月1日公布，共1处
  - 增补aa：2015年8月11日公布，共1处
  - 增补bb：2015年10月3日公布，共1处
  - 增补cc：2016年4月22日公布，共1处
  - 增补dd：2016年11月22日公布，共1处
  - 增补ee：2016年12月16日公布，共1处
  - 增补ff：2017年7月16日公布，共1处
  - 增补gg：2017年10月15日公布，共1处
  - 增补hh：2018年5月2日公布，共1处

## 分类

[中国的历史文化名城按照各个](../Page/中国.md "wikilink")[城市的特点主要分为七类即](../Page/城市.md "wikilink")

  - 古都型：以都城时代的[历史遗存物](../Page/历史.md "wikilink")、古都的风貌为特点，如[北京](../Page/北京.md "wikilink")、[洛陽](../Page/洛陽.md "wikilink")、[西安等](../Page/西安.md "wikilink")；
  - [传统风貌型](../Page/传统.md "wikilink")：保留一个或几个历史时期积淀的有完整[建筑群的](../Page/建筑.md "wikilink")[城市](../Page/城市.md "wikilink")，如[平遥](../Page/平遥.md "wikilink")、[韩城等](../Page/韩城.md "wikilink")；
  - [风景名胜型](../Page/风景.md "wikilink")：由建筑与山水环境的叠加而显示出鲜明个性特征的城市，如[桂林](../Page/桂林.md "wikilink")、[苏州等](../Page/苏州.md "wikilink")；
  - 地方及[民族特色型](../Page/民族.md "wikilink")：由地域特色或独自的个性特征、民族风情、地方文化构成城市风貌主体的城市，如[丽江](../Page/丽江.md "wikilink")、[拉萨等](../Page/拉萨.md "wikilink")；
  - 近现代史迹型：反映历史上某一事件或某个阶段的建筑物或建筑群为其显著特色的城市，如[上海](../Page/上海.md "wikilink")、[遵义等](../Page/遵义.md "wikilink")；
  - 特殊职能型：城市中的某种职能在历史上占有极突出的地位，如“盐都”[自贡](../Page/自贡.md "wikilink")、“瓷都”[景德镇等](../Page/景德镇.md "wikilink")；
  - 一般史迹型：以分散在全城各处的[文物古迹为历史传统体现主要方式的城市](../Page/文物.md "wikilink")，如[长沙](../Page/长沙.md "wikilink")、[济南等](../Page/济南.md "wikilink")。

## 名单

\[1\]

| 序号  | 名称                                   | 公布日期              |
| --- | ------------------------------------ | ----------------- |
| 1   | [北京](../Page/北京.md "wikilink")       | 1982年2月8日         |
| 2   | [承德](../Page/承德.md "wikilink")       |                   |
| 3   | [大同](../Page/大同市.md "wikilink")      |                   |
| 4   | [南京](../Page/南京.md "wikilink")       |                   |
| 5   | [苏州](../Page/苏州.md "wikilink")       |                   |
| 6   | [扬州](../Page/扬州.md "wikilink")       |                   |
| 7   | [杭州](../Page/杭州.md "wikilink")       |                   |
| 8   | [绍兴](../Page/绍兴.md "wikilink")       |                   |
| 9   | [泉州](../Page/泉州.md "wikilink")       |                   |
| 10  | [景德镇](../Page/景德镇.md "wikilink")     |                   |
| 11  | [曲阜](../Page/曲阜.md "wikilink")       |                   |
| 12  | [洛阳](../Page/洛阳.md "wikilink")       |                   |
| 13  | [开封](../Page/开封.md "wikilink")       |                   |
| 14  | [江陵](../Page/荆州区.md "wikilink")      |                   |
| 15  | [长沙](../Page/长沙.md "wikilink")       |                   |
| 16  | [广州](../Page/广州.md "wikilink")       |                   |
| 17  | [桂林](../Page/桂林.md "wikilink")       |                   |
| 18  | [成都](../Page/成都.md "wikilink")       |                   |
| 19  | [遵义](../Page/遵义.md "wikilink")       |                   |
| 20  | [昆明](../Page/昆明.md "wikilink")       |                   |
| 21  | [大理](../Page/大理市.md "wikilink")      |                   |
| 22  | [拉萨](../Page/拉萨.md "wikilink")       |                   |
| 23  | [西安](../Page/西安.md "wikilink")       |                   |
| 24  | [延安](../Page/延安.md "wikilink")       |                   |
| 25  | [上海](../Page/上海.md "wikilink")       | 1986年12月8日        |
| 26  | [天津](../Page/天津.md "wikilink")       |                   |
| 27  | [沈阳](../Page/沈阳.md "wikilink")       |                   |
| 28  | [武汉](../Page/武汉.md "wikilink")       |                   |
| 29  | [南昌](../Page/南昌.md "wikilink")       |                   |
| 30  | [重庆](../Page/重庆.md "wikilink")       |                   |
| 31  | [保定](../Page/保定.md "wikilink")       |                   |
| 32  | [平遥](../Page/平遥.md "wikilink")       |                   |
| 33  | [呼和浩特](../Page/呼和浩特.md "wikilink")   |                   |
| 34  | [镇江](../Page/镇江.md "wikilink")       |                   |
| 35  | [常熟](../Page/常熟.md "wikilink")       |                   |
| 36  | [徐州](../Page/徐州.md "wikilink")       |                   |
| 37  | [淮安](../Page/淮安区.md "wikilink")      |                   |
| 38  | [宁波](../Page/宁波.md "wikilink")       |                   |
| 39  | [歙县](../Page/歙县.md "wikilink")       |                   |
| 40  | [寿县](../Page/寿县.md "wikilink")       |                   |
| 41  | [亳州](../Page/亳州.md "wikilink")       |                   |
| 42  | [福州](../Page/福州.md "wikilink")       |                   |
| 43  | [漳州](../Page/漳州.md "wikilink")       |                   |
| 44  | [济南](../Page/济南.md "wikilink")       |                   |
| 45  | [安阳](../Page/安阳.md "wikilink")       |                   |
| 46  | [南阳](../Page/南阳.md "wikilink")       |                   |
| 47  | [商丘（县）](../Page/商丘县.md "wikilink")   |                   |
| 48  | [襄阳](../Page/襄阳.md "wikilink")       |                   |
| 49  | [潮州](../Page/潮州.md "wikilink")       |                   |
| 50  | [阆中](../Page/阆中.md "wikilink")       |                   |
| 51  | [宜宾](../Page/宜宾市.md "wikilink")      |                   |
| 52  | [自贡](../Page/自贡.md "wikilink")       |                   |
| 53  | [镇远](../Page/镇远.md "wikilink")       |                   |
| 54  | [丽江](../Page/丽江.md "wikilink")       |                   |
| 55  | [日喀则](../Page/日喀则.md "wikilink")     |                   |
| 56  | [韩城](../Page/韩城.md "wikilink")       |                   |
| 57  | [榆林](../Page/榆林.md "wikilink")       |                   |
| 58  | [武威](../Page/武威.md "wikilink")       |                   |
| 59  | [张掖](../Page/张掖.md "wikilink")       |                   |
| 60  | [敦煌](../Page/敦煌.md "wikilink")       |                   |
| 61  | [银川](../Page/银川.md "wikilink")       |                   |
| 62  | [喀什](../Page/喀什市.md "wikilink")      |                   |
| 63  | [正定](../Page/正定.md "wikilink")       | 1994年1月4日         |
| 64  | [邯郸](../Page/邯郸.md "wikilink")       |                   |
| 65  | [新绛](../Page/新绛.md "wikilink")       |                   |
| 66  | [代县](../Page/代县.md "wikilink")       |                   |
| 67  | [祁县](../Page/祁县.md "wikilink")       |                   |
| 68  | [哈尔滨](../Page/哈尔滨.md "wikilink")     |                   |
| 69  | [吉林](../Page/吉林市.md "wikilink")      |                   |
| 70  | [集安](../Page/集安.md "wikilink")       |                   |
| 71  | [衢州](../Page/衢州.md "wikilink")       |                   |
| 72  | [临海](../Page/临海.md "wikilink")       |                   |
| 73  | [长汀](../Page/长汀.md "wikilink")       |                   |
| 74  | [赣州](../Page/赣州.md "wikilink")       |                   |
| 75  | [青岛](../Page/青岛.md "wikilink")       |                   |
| 76  | [聊城](../Page/聊城.md "wikilink")       |                   |
| 77  | [邹城](../Page/邹城.md "wikilink")       |                   |
| 78  | [临淄](../Page/临淄.md "wikilink")       |                   |
| 79  | [郑州](../Page/郑州.md "wikilink")       |                   |
| 80  | [浚县](../Page/浚县.md "wikilink")       |                   |
| 81  | [随州](../Page/随州.md "wikilink")       |                   |
| 82  | [钟祥](../Page/钟祥.md "wikilink")       |                   |
| 83  | [岳阳](../Page/岳阳.md "wikilink")       |                   |
| 84  | [肇庆](../Page/肇庆.md "wikilink")       |                   |
| 85  | [佛山](../Page/佛山.md "wikilink")       |                   |
| 86  | [梅州](../Page/梅州.md "wikilink")       |                   |
| 87  | [海康](../Page/海康.md "wikilink")       |                   |
| 88  | [柳州](../Page/柳州.md "wikilink")       |                   |
| 89  | [琼山](../Page/琼山.md "wikilink")       |                   |
| 90  | [乐山](../Page/乐山.md "wikilink")       |                   |
| 91  | [都江堰](../Page/都江堰市.md "wikilink")    |                   |
| 92  | [泸州](../Page/泸州.md "wikilink")       |                   |
| 93  | [建水](../Page/建水.md "wikilink")       |                   |
| 94  | [巍山](../Page/巍山.md "wikilink")       |                   |
| 95  | [江孜](../Page/江孜.md "wikilink")       |                   |
| 96  | [咸阳](../Page/咸阳.md "wikilink")       |                   |
| 97  | [汉中](../Page/汉中.md "wikilink")       |                   |
| 98  | [天水](../Page/天水.md "wikilink")       |                   |
| 99  | [同仁](../Page/同仁.md "wikilink")       |                   |
| 100 | [山海关区](../Page/山海关区.md "wikilink")   | 2001年8月10日\[2\]   |
| 101 | [凤凰县](../Page/凤凰县.md "wikilink")     | 2001年12月17日\[3\]  |
| 102 | [濮阳市](../Page/濮阳市.md "wikilink")     | 2004年10月1日\[4\]   |
| 103 | [安庆市](../Page/安庆市.md "wikilink")     | 2005年4月14日\[5\]   |
| 104 | [泰安市](../Page/泰安市.md "wikilink")     | 2007年3月9日\[6\]    |
| 105 | [海口市](../Page/海口市.md "wikilink")     | 2007年3月13日\[7\]   |
| 106 | [金华市](../Page/金华市.md "wikilink")     | 2007年3月18日\[8\]   |
| 107 | [绩溪县](../Page/绩溪县.md "wikilink")     | 2007年3月18日\[9\]   |
| 108 | [吐鲁番市](../Page/吐鲁番市.md "wikilink")   | 2007年4月27日\[10\]  |
| 109 | [特克斯县](../Page/特克斯县.md "wikilink")   | 2007年5月6日\[11\]   |
| 110 | [无锡市](../Page/无锡市.md "wikilink")     | 2007年9月15日\[12\]  |
| 111 | [南通市](../Page/南通市.md "wikilink")     | 2009年1月2日\[13\]   |
| 112 | [北海市](../Page/北海市.md "wikilink")     | 2010年11月9日\[14\]  |
| 113 | [宜兴市](../Page/宜兴市.md "wikilink")     | 2011年1月24日\[15\]  |
| 114 | [嘉兴市](../Page/嘉兴市.md "wikilink")     | 2011年1月24日\[16\]  |
| 115 | [中山市](../Page/中山市.md "wikilink")     | 2011年3月12日\[17\]  |
| 116 | [太原市](../Page/太原市.md "wikilink")     | 2011年3月14日\[18\]  |
| 117 | [蓬莱市](../Page/蓬莱市.md "wikilink")     | 2011年5月1日\[19\]   |
| 118 | [会理县](../Page/会理县.md "wikilink")     | 2011年11月2日\[20\]  |
| 119 | [库车县](../Page/库车县.md "wikilink")     | 2012年3月15日\[21\]  |
| 120 | [伊宁市](../Page/伊宁市.md "wikilink")     | 2012年6月28日\[22\]  |
| 121 | [泰州市](../Page/泰州市.md "wikilink")     | 2013年2月10日\[23\]  |
| 122 | [会泽县](../Page/会泽县.md "wikilink")     | 2013年5月18日\[24\]  |
| 123 | [烟台市](../Page/烟台市.md "wikilink")     | 2013年7月28日\[25\]  |
| 124 | [青州市](../Page/青州市.md "wikilink")     | 2013年11月18日\[26\] |
| 125 | [湖州市](../Page/湖州市.md "wikilink")     | 2014年7月14日\[27\]  |
| 126 | [齐齐哈尔市](../Page/齐齐哈尔市.md "wikilink") | 2014年8月6日\[28\]   |
| 127 | [常州市](../Page/常州市.md "wikilink")     | 2015年6月1日\[29\]   |
| 128 | [瑞金市](../Page/瑞金市.md "wikilink")     | 2015年8月11日\[30\]  |
| 129 | [惠州市](../Page/惠州市.md "wikilink")     | 2015年10月3日\[31\]  |
| 130 | [温州市](../Page/温州市.md "wikilink")     | 2016年4月22日\[32\]  |
| 131 | [高邮市](../Page/高邮市.md "wikilink")     | 2016年11月22日\[33\] |
| 132 | [永州市](../Page/永州市.md "wikilink")     | 2016年12月16日\[34\] |
| 133 | [龙泉市](../Page/龙泉市.md "wikilink")     | 2017年7月16日\[35\]  |
| 134 | [长春市](../Page/长春市.md "wikilink")     | 2017年10月15日\[36\] |
| 135 | [蔚县](../Page/蔚县.md "wikilink")       | 2018年5月2日\[37\]   |

## 列表

括号内数字为城市数。

  - [北京](../Page/#北京.md "wikilink")（1）
    [天津](../Page/#天津.md "wikilink")（1）
    [河北](../Page/#河北.md "wikilink")（6）
    [山西](../Page/#山西.md "wikilink")（6）
    [内蒙古](../Page/#内蒙古.md "wikilink")（1）
  - [辽宁](../Page/#辽宁.md "wikilink")（1）
    [吉林](../Page/#吉林.md "wikilink")（3）
    [黑龙江](../Page/#黑龙江.md "wikilink")（2）
  - [上海](../Page/#上海.md "wikilink")（1）
    [江苏](../Page/#江苏.md "wikilink")（13）
    [浙江](../Page/#浙江.md "wikilink")（10）
    [安徽](../Page/#安徽.md "wikilink")（5）
    [福建](../Page/#福建.md "wikilink")（4）
    [江西](../Page/#江西.md "wikilink")（4）
    [山东](../Page/#山东.md "wikilink")（10）
  - [河南](../Page/#河南.md "wikilink")（8）
    [湖北](../Page/#湖北.md "wikilink")（5）
    [湖南](../Page/#湖南.md "wikilink")（4）[广东](../Page/#广东.md "wikilink")（7）
    [广西](../Page/#广西.md "wikilink")（3）
    [海南](../Page/#海南.md "wikilink")（2）
  - [重庆](../Page/#重庆.md "wikilink")（1）
    [四川](../Page/#四川.md "wikilink")（8）
    [贵州](../Page/#贵州.md "wikilink")（2）
    [云南](../Page/#云南.md "wikilink")（6）
    [西藏](../Page/#西藏.md "wikilink")（3）
  - [陕西](../Page/#陕西.md "wikilink")（6）
    [甘肃](../Page/#甘肃.md "wikilink")（4）
    [青海](../Page/#青海.md "wikilink")（1）
    [宁夏](../Page/#宁夏.md "wikilink")（1）
    [新疆](../Page/#新疆.md "wikilink")（5）

下文括号内数字为入选批次。

### [北京](../Page/北京.md "wikilink")

  - [北京](../Page/北京.md "wikilink")（1）

### [天津](../Page/天津.md "wikilink")

  - [天津](../Page/天津.md "wikilink")（2）

### [河北](../Page/河北.md "wikilink")

  - [承德](../Page/承德.md "wikilink")（1）{{\!}}
    [保定](../Page/保定.md "wikilink")（2）{{\!}}
    [正定](../Page/正定.md "wikilink")（3）{{\!}}
    [邯郸](../Page/邯郸.md "wikilink")（3）{{\!}}
    [山海关区](../Page/山海关区.md "wikilink")（秦皇岛市）（a）{{\!}}
    [蔚县](../Page/蔚县.md "wikilink")（hh）

### [山西](../Page/山西.md "wikilink")

  - [大同](../Page/大同市.md "wikilink")（1）{{\!}}
    [平遥](../Page/平遥.md "wikilink")（2）{{\!}}
    [新绛](../Page/新绛.md "wikilink")（3）{{\!}}
    [代县](../Page/代县.md "wikilink")（3）{{\!}}
    [祁县](../Page/祁县.md "wikilink")（3）{{\!}}
    [太原](../Page/太原.md "wikilink")（o）

### [内蒙古](../Page/内蒙古.md "wikilink")

  - [呼和浩特](../Page/呼和浩特.md "wikilink")（2）

### [辽宁](../Page/辽宁.md "wikilink")

  - [沈阳](../Page/沈阳.md "wikilink")（2）

### [吉林](../Page/吉林.md "wikilink")

  - [吉林](../Page/吉林市.md "wikilink")（3）{{\!}}[集安](../Page/集安.md "wikilink")（3）{{\!}}[长春](../Page/长春.md "wikilink")（gg）

### [黑龙江](../Page/黑龙江省.md "wikilink")

  - [哈尔滨](../Page/哈尔滨.md "wikilink")（3）{{\!}}[齐齐哈尔](../Page/齐齐哈尔.md "wikilink")（y）

### [上海](../Page/上海.md "wikilink")

  - [上海](../Page/上海.md "wikilink")（2）

### [江苏](../Page/江苏.md "wikilink")

  - [南京](../Page/南京.md "wikilink")（1） {{\!}}
    [苏州](../Page/苏州.md "wikilink")（1）{{\!}}
    [扬州](../Page/扬州.md "wikilink")（1）{{\!}}
    [镇江](../Page/镇江.md "wikilink")（2）{{\!}}
    [常熟](../Page/常熟.md "wikilink")（2）{{\!}}
    [徐州](../Page/徐州.md "wikilink")（2）{{\!}}
    [淮安区](../Page/淮安区.md "wikilink")（淮安市）（2）{{\!}}
    [无锡](../Page/无锡.md "wikilink")（j）{{\!}}
    [南通](../Page/南通.md "wikilink")（k）{{\!}}
    [宜兴](../Page/宜兴.md "wikilink")（m）{{\!}}
    [泰州](../Page/泰州.md "wikilink")（t）{{\!}}
    [常州](../Page/常州.md "wikilink")（z）{{\!}}
    [高邮](../Page/高邮.md "wikilink")（dd）

### [浙江](../Page/浙江.md "wikilink")

  - [杭州](../Page/杭州.md "wikilink")（1） {{\!}}
    [绍兴](../Page/绍兴.md "wikilink")（1） {{\!}}
    [宁波](../Page/宁波.md "wikilink")（2） {{\!}}
    [衢州](../Page/衢州.md "wikilink")（3） {{\!}}
    [临海](../Page/临海.md "wikilink")（3） {{\!}}
    [金华](../Page/金华.md "wikilink")（g）{{\!}}
    [嘉兴](../Page/嘉兴市.md "wikilink")（m）{{\!}}
    [湖州](../Page/湖州市.md "wikilink")（x）{{\!}}
    [温州](../Page/温州市.md "wikilink")（cc）{{\!}}
    [龙泉](../Page/龙泉市.md "wikilink")（ff）

### [安徽](../Page/安徽.md "wikilink")

  - [歙县](../Page/歙县.md "wikilink")（2） {{\!}}
    [寿县](../Page/寿县.md "wikilink")（2） {{\!}}
    [亳州](../Page/亳州.md "wikilink")（2） {{\!}}
    [安庆](../Page/安庆.md "wikilink")（d） {{\!}}
    [绩溪](../Page/绩溪.md "wikilink")（g）

### [福建](../Page/福建.md "wikilink")

  - [泉州](../Page/泉州.md "wikilink")（1） {{\!}}
    [福州](../Page/福州.md "wikilink")（2） {{\!}}
    [漳州](../Page/漳州.md "wikilink")（2） {{\!}}
    [长汀](../Page/长汀.md "wikilink")（3）

### [江西](../Page/江西.md "wikilink")

  - [景德镇](../Page/景德镇.md "wikilink")（1） {{\!}}
    [南昌](../Page/南昌.md "wikilink")（2） {{\!}}
    [赣州](../Page/赣州.md "wikilink")（3） {{\!}}
    [瑞金](../Page/瑞金.md "wikilink")（aa）

### [山东](../Page/山东.md "wikilink")

  - [曲阜](../Page/曲阜.md "wikilink")（1） {{\!}}
    [济南](../Page/济南.md "wikilink")（2） {{\!}}
    [青岛](../Page/青岛.md "wikilink")（3） {{\!}}
    [聊城](../Page/聊城.md "wikilink")（3） {{\!}}
    [邹城](../Page/邹城.md "wikilink")（3） {{\!}}
    [临淄区](../Page/临淄区.md "wikilink")（淄博市）（3） {{\!}}
    [泰安](../Page/泰安.md "wikilink")（e）{{\!}}
    [蓬莱](../Page/蓬莱市.md "wikilink")（p）{{\!}}
    [烟台](../Page/烟台.md "wikilink")（v）{{\!}}
    [青州](../Page/青州.md "wikilink")（w）

### [河南](../Page/河南.md "wikilink")

  - [洛阳](../Page/洛阳.md "wikilink")（1） {{\!}}
    [开封](../Page/开封.md "wikilink")（1） {{\!}}
    [安阳](../Page/安阳.md "wikilink")（2） {{\!}}
    [南阳](../Page/南阳.md "wikilink")（2） {{\!}}
    [睢阳区](../Page/睢阳区.md "wikilink")（商丘市）（2） {{\!}}
    [郑州](../Page/郑州.md "wikilink")（3） {{\!}}
    [浚县](../Page/浚县.md "wikilink")（3） {{\!}}
    [濮阳](../Page/濮阳.md "wikilink")（c）

### [湖北](../Page/湖北.md "wikilink")

  - “江陵”（主体在今荆州市[荆州区](../Page/荆州区.md "wikilink")）（1） {{\!}}
    [武汉](../Page/武汉.md "wikilink")（2） {{\!}}
    [襄阳](../Page/襄阳.md "wikilink")（2） {{\!}}
    [随州](../Page/随州.md "wikilink")（3） {{\!}}
    [钟祥](../Page/钟祥.md "wikilink")（3）

### [湖南](../Page/湖南.md "wikilink")

  - [长沙](../Page/长沙.md "wikilink")（1） {{\!}}
    [岳阳](../Page/岳阳.md "wikilink")（3） {{\!}}
    [凤凰](../Page/凤凰县.md "wikilink")（b）{{\!}}
    [永州](../Page/永州.md "wikilink")（ee）

### [广东](../Page/广东.md "wikilink")

  - [广州](../Page/广州.md "wikilink")（1） {{\!}}
    [潮州](../Page/潮州.md "wikilink")（2） {{\!}}
    [肇庆](../Page/肇庆.md "wikilink")（3） {{\!}}
    [佛山](../Page/佛山.md "wikilink")（3） {{\!}}
    [梅州](../Page/梅州.md "wikilink")（3） {{\!}}
    [雷州](../Page/雷州.md "wikilink")（3） {{\!}}
    [中山](../Page/中山市.md "wikilink")（n）{{\!}}
    [惠州](../Page/惠州.md "wikilink")（bb）

### [广西](../Page/广西.md "wikilink")

  - [桂林](../Page/桂林.md "wikilink")（1） {{\!}}
    [柳州](../Page/柳州.md "wikilink")（3） {{\!}}
    [北海](../Page/北海市.md "wikilink")（l）

### [海南](../Page/海南.md "wikilink")

  - [琼山](../Page/琼山.md "wikilink")（3） {{\!}}
    [海口](../Page/海口.md "wikilink")（f）

### [重庆](../Page/重庆.md "wikilink")

  - [重庆](../Page/重庆.md "wikilink")（2）

### [四川](../Page/四川.md "wikilink")

  - [成都](../Page/成都.md "wikilink")（1） {{\!}}
    [阆中](../Page/阆中.md "wikilink")（2） {{\!}}
    [宜宾](../Page/宜宾.md "wikilink")（2） {{\!}}
    [自贡](../Page/自贡.md "wikilink")（2） {{\!}}
    [乐山](../Page/乐山.md "wikilink")（3） {{\!}}
    [都江堰](../Page/都江堰市.md "wikilink")（3） {{\!}}
    [泸州](../Page/泸州.md "wikilink")（3） {{\!}}
    [会理](../Page/会理.md "wikilink")（q）

### [贵州](../Page/贵州.md "wikilink")

  - [遵义](../Page/遵义.md "wikilink")（1） {{\!}}
    [镇远](../Page/镇远.md "wikilink")（2）

### [云南](../Page/云南.md "wikilink")

  - [昆明](../Page/昆明.md "wikilink")（1） {{\!}}
    [大理](../Page/大理.md "wikilink")（1） {{\!}}
    [丽江](../Page/丽江.md "wikilink")（2） {{\!}}
    [建水](../Page/建水.md "wikilink")（3） {{\!}}
    [巍山](../Page/巍山.md "wikilink")（3）{{\!}}
    [会泽](../Page/会泽.md "wikilink")（u）

### [西藏](../Page/西藏.md "wikilink")

  - [拉萨](../Page/拉萨.md "wikilink")（1） {{\!}}
    [日喀则](../Page/日喀则.md "wikilink")（2） {{\!}}
    [江孜](../Page/江孜.md "wikilink")（3）

### [陕西](../Page/陕西.md "wikilink")

  - [西安](../Page/西安.md "wikilink")（1） {{\!}}
    [延安](../Page/延安.md "wikilink")（1） {{\!}}
    [韩城](../Page/韩城.md "wikilink")（2） {{\!}}
    [榆林](../Page/榆林.md "wikilink")（2） {{\!}}
    [咸阳](../Page/咸阳.md "wikilink")（3） {{\!}}
    [汉中](../Page/汉中.md "wikilink")（3）

### [甘肃](../Page/甘肃.md "wikilink")

  - [武威](../Page/武威.md "wikilink")（2） {{\!}}
    [张掖](../Page/张掖.md "wikilink")（2） {{\!}}
    [敦煌](../Page/敦煌.md "wikilink")（2） {{\!}}
    [天水](../Page/天水.md "wikilink")（3）

### [青海](../Page/青海.md "wikilink")

  - [同仁](../Page/同仁.md "wikilink")（3）

### [宁夏](../Page/宁夏.md "wikilink")

  - [银川](../Page/银川.md "wikilink")（2）

### [新疆](../Page/新疆.md "wikilink")

  - [喀什](../Page/喀什市.md "wikilink")（2） {{\!}}
    [高昌区](../Page/高昌区.md "wikilink")（吐鲁番市）（h）{{\!}}
    [特克斯](../Page/特克斯.md "wikilink")（i）{{\!}}
    [库车](../Page/库车.md "wikilink")（r）{{\!}}
    [伊宁](../Page/伊宁市.md "wikilink")（s）

## 参考文献

## 参见

  - [中华人民共和国文物保护制度](../Page/中华人民共和国文物保护制度.md "wikilink")
  - [中国历史文化街区](../Page/中国历史文化街区.md "wikilink")
  - [中国历史文化名镇](../Page/中国历史文化名镇.md "wikilink")
  - [中国历史文化名村](../Page/中国历史文化名村.md "wikilink")

{{-}}

[国家历史文化名城](../Category/国家历史文化名城.md "wikilink")
[Category:中华人民共和国国家级事物](../Category/中华人民共和国国家级事物.md "wikilink")
[Category:中华人民共和国城市列表](../Category/中华人民共和国城市列表.md "wikilink")

1.
2.  [国务院关于同意将秦皇岛市山海关区列为国家历史文化名城的批复](http://www.gov.cn/gongbao/content/2001/content_61006.htm)
3.  [国务院关于同意将湖南省凤凰县列为国家历史文化名城的批复](http://www.gov.cn/gongbao/content/2002/content_61851.htm)
4.  [国务院关于同意将河南省濮阳市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2005-08/26/content_26268.htm)
5.  [国务院关于同意将安徽省安庆市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2008-03/28/content_5673.htm)
6.  [国务院关于同意将山东省泰安市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-03/16/content_552987.htm)
7.  [国务院关于同意将海南省海口市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-03/16/content_552994.htm)
8.  [国务院关于同意将浙江省金华市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-03/23/content_559385.htm)
9.  [国务院关于同意将安徽省绩溪县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-03/23/content_559400.htm)
10. [国务院关于同意将新疆维吾尔自治区吐鲁番市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-04/30/content_603304.htm)
11. [国务院关于同意将新疆维吾尔自治区特克斯县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-05/11/content_611302.htm)
12. [国务院关于同意将江苏省无锡市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-09/21/content_757421.htm)
13. [国务院关于同意将江苏省南通市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2009-01/07/content_1198333.htm)
14. [国务院关于同意将广西壮族自治区北海市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2010-11/12/content_1744022.htm)
15. [国务院关于同意将江苏省宜兴市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-01/27/content_1793808.htm)
16. [国务院关于同意将浙江省嘉兴市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-01/27/content_1793810.htm)
17. [国务院关于同意将广东省中山市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-03/17/content_1826261.htm)
18. [国务院关于同意将山西省太原市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-03/17/content_1826264.htm)
19. [国务院关于同意将山东省蓬莱市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-05/05/content_1858124.htm)
20. [国务院关于同意将四川省会理县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2011-11/08/content_1988306.htm)
21. [国务院关于同意将新疆维吾尔自治区库车县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2012-03/20/content_2095526.htm)
22. [国务院关于同意将新疆维吾尔自治区伊宁市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2012-07/03/content_2175888.htm)
23. [国务院关于同意将江苏省泰州市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2013-02/18/content_2334038.htm)
24. [国务院关于同意将云南省会泽县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2013-05/22/content_2408675.htm)
25. [国务院关于同意将山东省烟台市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2013-08/02/content_2459966.htm)
26. [国务院关于同意将山东省青州市列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2013-11/22/content_2532638.htm)
27. [国务院关于同意将浙江省湖州市列为国家历史文化名城的批复](http://www.gov.cn/gongbao/content/2014/content_2729569.htm)
28. [国务院关于同意将黑龙江省齐齐哈尔市列为国家历史文化名城的批复](http://www.gov.cn/gongbao/content/2014/content_2739850.htm)
29. [国务院关于同意将江苏省常州市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2015-06/11/content_9837.htm)
30. [国务院关于同意将江西省瑞金市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2015-08/19/content_10104.htm)
31. [国务院关于同意将广东省惠州市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2015-10/18/content_10244.htm)
32. [国务院关于同意将浙江省温州市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2016-05/04/content_5070199.htm)
33. [国务院关于同意将江苏省高邮市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2016-11/28/content_5138722.htm)
34. [国务院关于同意将湖南省永州市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2016-12/26/content_5152877.htm)
35. [国务院关于同意将浙江省龙泉市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2017-07/21/content_5212296.htm)
36. [国务院关于同意将吉林省长春市列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2017-10/26/content_5234622.htm)
37. [国务院关于同意将河北省蔚县列为国家历史文化名城的批复](http://www.gov.cn/zhengce/content/2018-05/10/content_5289771.htm)