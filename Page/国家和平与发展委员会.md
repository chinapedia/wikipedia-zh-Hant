**国家和平与发展委员会**（[緬甸語](../Page/緬甸語.md "wikilink")：နိုင်ငံတော်အေးချမ်းသာယာရေးနှင့်ဖွံ့ဖြိုးရေးကောင်စီ）為[缅甸](../Page/缅甸.md "wikilink")[軍政府時期的最高权力机构](../Page/軍政府.md "wikilink")，成立於1988年9月18日，當時名為**国家恢复法律和秩序委员会**，由蘇貌擔任主席至1992年由副主席[丹瑞繼任主席](../Page/丹瑞.md "wikilink")，1997年11月15日更名為国家和平与发展委员会。

## 歷史

委員會由13位緬甸民族團結黨成員组成，成員主要是[軍人](../Page/緬甸軍事.md "wikilink")，緬甸軍政府的最高權力機構。

軍政府機構在2011年3月30日解散，由[登盛領導的](../Page/登盛.md "wikilink")[文官](../Page/文官.md "wikilink")[官僚](../Page/官僚.md "wikilink")[政府所取代](../Page/政府.md "wikilink")。

## 领袖

  - 主席——[丹瑞](../Page/丹瑞.md "wikilink")（Than Shwe）大将，軍政府最高領導人
  - 副主席——（Maung Aye）副大将（二级大将）
  - 第一秘书长——[丁昂敏烏](../Page/丁昂敏烏.md "wikilink")（Tin Aung Myint Oo）中将

## 成员

  - [登盛](../Page/登盛.md "wikilink")（Thein
    Sein）中将，2011年當選為[緬甸總統](../Page/緬甸總統.md "wikilink")
  - [丁昂敏烏](../Page/丁昂敏烏.md "wikilink")（Tin Aung Myint Oo）中将
  - 瑞曼（Shwe Mann）上将
  - 觉温（Kyaw Win）中将
  - 丁埃（Tin Aye）中将
  - 耶敏（Ye Myint）中将
  - 昂希（Aung Htwe）中将
  - 钦貌丹（Khin Maung Than）中将
  - 貌波（Maung Bo）中将

## 参考文献

## 外部連結

  - [Official Page of SPDC](http://www.myanmar.com/)
  - [Another Official Page of
    SPDC](https://web.archive.org/web/20051220003223/http://www.myanmar.gov.mm/)
  - [Members of State Peace and Development Council
    (SPDC)](https://web.archive.org/web/20051202053359/http://www.irrawaddy.org/aviewer.asp?a=454&z=14)

{{-}}

[Category:缅甸政府](../Category/缅甸政府.md "wikilink")
[Category:军政府](../Category/军政府.md "wikilink")