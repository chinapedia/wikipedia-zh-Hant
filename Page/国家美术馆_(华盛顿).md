[Gallery15Urlan.ogv](https://zh.wikipedia.org/wiki/File:Gallery15Urlan.ogv "fig:Gallery15Urlan.ogv")
**国家美术馆**（），是一座位於[華盛頓特區的藝術](../Page/華盛頓特區.md "wikilink")[博物館](../Page/博物館.md "wikilink")，常縮寫為**NGA**，隸屬於[美國政府](../Page/美國.md "wikilink")。建築物本身分為東西兩棟，靠地下通道相通。尽管该馆周边有多个[史密森尼学会名下的博物馆和美术馆](../Page/史密森尼学会.md "wikilink")，但该馆与史密森尼学会并没有隶属关系。

国家美术馆由[美國國會創建於](../Page/美國國會.md "wikilink")1937年，一開始是為了管理[安德魯·W·梅隆和](../Page/安德魯·W·梅隆.md "wikilink")[山缪·卡瑞斯兩人捐獻出的收藏](../Page/山缪·卡瑞斯.md "wikilink")（主要是[義大利藝術](../Page/義大利.md "wikilink")）。1941年3月17日国家美术馆的西棟建築正式開放給民眾參觀，西棟建築由[建築師](../Page/建築師.md "wikilink")[約翰·羅素·波普所設計](../Page/約翰·羅素·波普.md "wikilink")，東棟建築由建築師[貝聿銘所設計](../Page/貝聿銘.md "wikilink")，於1978年開放。此外，国家美术馆在1999年增設了一個[雕塑公園](../Page/雕塑.md "wikilink")。

## 收藏

国家美术馆是免費的，西棟主要收藏有歐洲中世紀到19世紀的重要畫作與雕塑。包括了全美唯一一幅[列奥纳多·达·芬奇的作品](../Page/列奥纳多·达·芬奇.md "wikilink")。東棟則主要收藏[現代藝術作品](../Page/現代藝術.md "wikilink")。包括有[畢加索](../Page/畢加索.md "wikilink")、[安迪·沃荷等](../Page/安迪·沃荷.md "wikilink")。此外，也是国家美术馆研究中心的所在地。

## 参考

## 外部連結

  - [国家美术馆官方網站](https://web.archive.org/web/20120403003248/http://www.nga.gov/home.htm)-英文
  - [实时展览(英)](https://web.archive.org/web/20130301174912/http://www.nga.gov/exhibitions/index.shtm)

[N](../Category/美國美術館.md "wikilink")
[Category:贝聿铭的建筑设计](../Category/贝聿铭的建筑设计.md "wikilink")
[Category:華盛頓哥倫比亞特區博物館](../Category/華盛頓哥倫比亞特區博物館.md "wikilink")
[Category:国家广场](../Category/国家广场.md "wikilink")
[Category:約翰·羅素·波普設計的建築](../Category/約翰·羅素·波普設計的建築.md "wikilink")