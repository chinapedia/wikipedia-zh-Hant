[中華人民共和國](../Page/中華人民共和國.md "wikilink")《**國家重點保護野生植物名錄**（第一批）》於1999年8月4日由[國務院批准並由國家林業局和農業部發佈](../Page/國務院.md "wikilink")，1999年9月9日起施行。2001年8月4日，农业部、[国家林业局发布第](../Page/国家林业局.md "wikilink")53号令，将[念珠藻科的](../Page/念珠藻科.md "wikilink")[发菜保护级别由二级调整为一级](../Page/发菜.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>中文名</p></th>
<th><p>學名</p></th>
<th><p>保護級別                     </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>  *<a href="../Page/水韭屬.md" title="wikilink">水韭屬</a>（所有種）</p></td>
<td><p><em>Isoetes</em> spp.</p></td>
<td><p>Ⅰ </p></td>
</tr>
<tr class="even">
<td><p>  *<a href="../Page/水蕨屬.md" title="wikilink">水蕨屬</a>（所有種）</p></td>
<td><p><em>Ceratopteris</em> spp.</p></td>
<td><p>Ⅱ                                                                                                                                                                                                                                                       </p></td>
</tr>
<tr class="odd">
<td><p>  *<a href="../Page/川藻.md" title="wikilink">川藻</a>（<a href="../Page/石蔓.md" title="wikilink">石蔓</a>）</p></td>
<td><p><em>Terniopsis sessilis</em></p></td>
<td><p>Ⅱ                                                              </p></td>
</tr>
<tr class="even">
<td><p>  *<a href="../Page/珊瑚菜.md" title="wikilink">珊瑚菜</a>（<a href="../Page/北沙蔘.md" title="wikilink">北沙蔘</a>）</p></td>
<td><p><em>Glehnia littoralis</em></p></td>
<td><p>Ⅱ           </p></td>
</tr>
<tr class="odd">
<td><p>  *<a href="../Page/蟲草.md" title="wikilink">蟲草</a>（<a href="../Page/冬蟲夏草.md" title="wikilink">冬蟲夏草</a>）</p></td>
<td><p><em>Cordyceps sinensis</em></p></td>
<td><p>Ⅱ</p></td>
</tr>
<tr class="even">
<td><p> <a href="../Page/口蘑科.md" title="wikilink">口蘑科</a>（<a href="../Page/白蘑科.md" title="wikilink">白蘑科</a>）</p></td>
<td><p>Tricholomataceae</p></td>
<td></td>
</tr>
</tbody>
</table>

注：标“\*”者由农业行政主管部门或渔业行政主管部門主管；未标“\*”者由林业行政主管部门主管。

## 外部連結

  - [中國自然保護區網](https://web.archive.org/web/20120413223605/http://www.nre.cn/htm/04/bhqsj/2004-07-10-12808.htm)

[category:文件](../Page/category:文件.md "wikilink")

[+](../Category/中国植物.md "wikilink")
[Category:中华人民共和国列表](../Category/中华人民共和国列表.md "wikilink")
[Category:中國自然保育](../Category/中國自然保育.md "wikilink")