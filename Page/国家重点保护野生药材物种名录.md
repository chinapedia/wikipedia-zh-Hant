**国家重点保护野生药材物种**，是根據《[瀕危野生動植物種國際貿易公約](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")》，並比對曾出現在[國家藥典中的中藥材制訂的名錄](../Page/國家藥典.md "wikilink")，用以保護這些已被國際公約所保護的物種。\[1\]
有關的行政法規則收錄在《[野生药材资源保护管理条例](../Page/野生药材资源保护管理条例.md "wikilink")》內。

本列表可分为三级：

  - 一级：濒临灭绝状态的稀有珍贵野生药材物种（简称一级保护野生药材物种）；
  - 二级：分布区域缩小、资源处于衰竭状态的重要野生药材物种（简称二级保护野生药材物种）；
  - 三级：资源严重减少的主要常用野生药材物种（简称三级保护野生药材物种）。

| 中名                                                                     | 学名                                                                                          | 保护级别 | 药材名称                                 |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- | ---- | ------------------------------------ |
| [猫科动物](../Page/猫科动物.md "wikilink")[虎](../Page/虎.md "wikilink")         | *Panthera tigris* Linnaeus(含国内所有[亚种](../Page/亚种.md "wikilink"))                             | Ⅰ    | [虎骨](../Page/虎骨.md "wikilink")       |
| [猫科动物](../Page/猫科动物.md "wikilink")[豹](../Page/豹.md "wikilink")         | *Panthera pardus* Linnaeus(含[云豹](../Page/云豹.md "wikilink") 、[雪豹](../Page/雪豹.md "wikilink")) | Ⅰ    | [豹骨](../Page/豹骨.md "wikilink")       |
| [牛科动物](../Page/牛科.md "wikilink")[赛加羚羊](../Page/赛加羚羊.md "wikilink")     | *Saiga tatarica* Linnaeus                                                                   | Ⅰ    | [羚羊角](../Page/羚羊角.md "wikilink")     |
| [鹿科动物](../Page/鹿科.md "wikilink")[梅花鹿](../Page/梅花鹿.md "wikilink")       | *Cervus nippon* Temminck                                                                    | Ⅰ    | [鹿茸](../Page/鹿茸.md "wikilink")       |
| [鹿科动物](../Page/鹿科.md "wikilink")[马鹿](../Page/马鹿.md "wikilink")         | *Cervus elaphus* Linnaeus                                                                   | Ⅱ    | [鹿茸](../Page/鹿茸.md "wikilink")       |
| [鹿科动物](../Page/鹿科.md "wikilink")[林麝](../Page/林麝.md "wikilink")         | *Moschus berezovskii* Flerov                                                                | Ⅱ    | [麝香](../Page/麝香.md "wikilink")       |
| [鹿科动物](../Page/鹿科.md "wikilink")[马麝](../Page/马麝.md "wikilink")         | *Moschus sifanicus* Przewalski                                                              | Ⅱ    | [麝香](../Page/麝香.md "wikilink")       |
| [鹿科动物](../Page/鹿科.md "wikilink")[原麝](../Page/原麝.md "wikilink")         | *Moschus moschiferus* Linnaeus                                                              | Ⅱ    | [麝香](../Page/麝香.md "wikilink")       |
| [熊科动物](../Page/熊科.md "wikilink")[黑熊](../Page/黑熊.md "wikilink")         | *Selenarctos thibetanus* Cuvier                                                             | Ⅱ    | [熊胆](../Page/熊胆.md "wikilink")       |
| [熊科动物](../Page/熊科.md "wikilink")[棕熊](../Page/棕熊.md "wikilink")         | *Ursus arctos* Linnaeus                                                                     | Ⅱ    | [熊胆](../Page/熊胆.md "wikilink")       |
| [鲮鲤科动物](../Page/鲮鲤科.md "wikilink")[穿山甲](../Page/穿山甲.md "wikilink")     | *Manis pentadactyla* Linnaeus                                                               | Ⅱ    | [穿山甲](../Page/穿山甲.md "wikilink")     |
| [蟾蜍科动物](../Page/蟾蜍科.md "wikilink")[中华大蟾蜍](../Page/中华大蟾蜍.md "wikilink") | *Bufo bufo gargarizans* Cantor                                                              | Ⅱ    | [蟾酥](../Page/蟾酥.md "wikilink")       |
| [蟾蜍科动物](../Page/蟾蜍科.md "wikilink")[黑眶蟾蜍](../Page/黑眶蟾蜍.md "wikilink")   | *Bufo melanostictus* Schneider                                                              | Ⅱ    | [蟾酥](../Page/蟾酥.md "wikilink")       |
| [蛙科动物](../Page/蛙科.md "wikilink")[中国林蛙](../Page/中国林蛙.md "wikilink")     | *Rana temporaria chensinensis* David                                                        | Ⅱ    | [哈蟆油](../Page/哈蟆油.md "wikilink")     |
| [眼镜蛇科动物](../Page/眼镜蛇科.md "wikilink")[银环蛇](../Page/银环蛇.md "wikilink")   | *Bungarus multicinctus multicinctus* Blyth                                                  | Ⅱ    | [金钱白花蛇](../Page/金钱白花蛇.md "wikilink") |
| [游蛇科动物](../Page/游蛇科.md "wikilink")[乌梢蛇](../Page/乌梢蛇.md "wikilink")     | *Zaocys dhumnades* (Cantor)                                                                 | Ⅱ    | [乌梢蛇](../Page/乌梢蛇.md "wikilink")     |
| [蝰科动物](../Page/蝰科.md "wikilink")[五步蛇](../Page/五步蛇.md "wikilink")       | *Agkistrodon acutus* (Guenther)                                                             | Ⅱ    | [蕲蛇](../Page/蕲蛇.md "wikilink")       |
| [壁虎科动物](../Page/壁虎科.md "wikilink")[蛤蚧](../Page/蛤蚧.md "wikilink")       | *Gekko gecko* Linnaeus                                                                      | Ⅱ    | [蛤蚧](../Page/蛤蚧.md "wikilink")       |
| [豆科植物](../Page/豆科.md "wikilink")[甘草](../Page/甘草.md "wikilink")         | *Glycyrrhiza uralensis* Fisch.                                                              | Ⅱ    | [甘草](../Page/甘草.md "wikilink")       |
| [豆科植物](../Page/豆科.md "wikilink")[胀果甘草](../Page/胀果甘草.md "wikilink")     | *Glycyrrhiza inflata* Bat.                                                                  | Ⅱ    | [甘草](../Page/甘草.md "wikilink")       |
| [豆科植物](../Page/豆科.md "wikilink")[光果甘草](../Page/光果甘草.md "wikilink")     | *Glycyrrhiza glabra* L.                                                                     | Ⅱ    | [甘草](../Page/甘草.md "wikilink")       |
| [毛茛科植物](../Page/毛茛科.md "wikilink")[黄连](../Page/黄连.md "wikilink")       | *Coptis chinensis* Franch.                                                                  | Ⅱ    | [黄连](../Page/黄连.md "wikilink")       |
| [毛茛科植物](../Page/毛茛科.md "wikilink")[三角叶黄连](../Page/三角叶黄连.md "wikilink") | *Coptis deltoidea* C. Y. Cheng et Hsiao                                                     | Ⅱ    | [黄连](../Page/黄连.md "wikilink")       |
| [毛茛科植物](../Page/毛茛科.md "wikilink")[云连](../Page/云连.md "wikilink")       | *Coptis teetoides* C. Y. Cheng                                                              | Ⅱ    | [黄连](../Page/黄连.md "wikilink")       |
| [五加科植物](../Page/五加科.md "wikilink")[人参](../Page/人参.md "wikilink")       | *Panax ginseng* C. A. Mey.                                                                  | Ⅱ    | [人参](../Page/人参.md "wikilink")       |
| [杜仲科植物](../Page/杜仲科.md "wikilink")[杜仲](../Page/杜仲.md "wikilink")       | *Eucommia ulmoides* Oliv.                                                                   | Ⅱ    | [杜仲](../Page/杜仲.md "wikilink")       |
| [木兰科植物](../Page/木兰科.md "wikilink")[厚朴](../Page/厚朴.md "wikilink")       | *Magnolia officinalis* Rehd. et Wils.                                                       | Ⅱ    | [厚朴](../Page/厚朴.md "wikilink")       |
| [木兰科植物](../Page/木兰科.md "wikilink")[凹叶厚朴](../Page/凹叶厚朴.md "wikilink")   | *Magnolia officinalis* Rehd. et Wils. var. biloba Rehd.et Wils.                             | Ⅱ    | [厚朴](../Page/厚朴.md "wikilink")       |
| [芸香科植物](../Page/芸香科.md "wikilink")[黄皮树](../Page/黄皮树.md "wikilink")     | *Phellodendron chinense* Schneid.                                                           | Ⅱ    | [黄柏](../Page/黄柏.md "wikilink")       |
| [芸香科植物](../Page/芸香科.md "wikilink")[黄檗](../Page/黄檗.md "wikilink")       | *Phellodendron amurense* Rupr.                                                              | Ⅱ    | [黄柏](../Page/黄柏.md "wikilink")       |
| [百合科植物](../Page/百合科.md "wikilink")[剑叶龙血树](../Page/剑叶龙血树.md "wikilink") | *Dracaena cochinchinensin* (Lour.) S. C. Chen                                               | Ⅱ    | [血竭](../Page/血竭.md "wikilink")       |
| [百合科植物](../Page/百合科.md "wikilink")[川贝母](../Page/川贝母.md "wikilink")     | *Fritillaria cirrhosa* D. Don                                                               | Ⅲ    | [川贝母](../Page/川贝母.md "wikilink")     |
| [百合科植物](../Page/百合科.md "wikilink")[暗紫贝母](../Page/暗紫贝母.md "wikilink")   | *Fritillaria unibracteata* Hsiao et K. C. Hsia                                              | Ⅲ    | [川贝母](../Page/川贝母.md "wikilink")     |
| [百合科植物](../Page/百合科.md "wikilink")[甘肃贝母](../Page/甘肃贝母.md "wikilink")   | *Fritillaria przewalskii* Maxim.                                                            | Ⅲ    | [川贝母](../Page/川贝母.md "wikilink")     |
| [百合科植物](../Page/百合科.md "wikilink")[梭砂贝母](../Page/梭砂贝母.md "wikilink")   | *Fritillaria delavayi* Franch.                                                              | Ⅲ    | [川贝母](../Page/川贝母.md "wikilink")     |
| [百合科植物](../Page/百合科.md "wikilink")[新疆贝母](../Page/新疆贝母.md "wikilink")   | *Fritillaria walujewii* Regel                                                               | Ⅲ    | [伊贝母](../Page/伊贝母.md "wikilink")     |
| [百合科植物](../Page/百合科.md "wikilink")[伊犁贝母](../Page/伊犁贝母.md "wikilink")   | *Fritillaria pallidiflora* Schrenk                                                          | Ⅲ    | [伊贝母](../Page/伊贝母.md "wikilink")     |
| [五加科植物](../Page/五加科.md "wikilink")[刺五加](../Page/刺五加.md "wikilink")     | *Acanthopanax senticosus* (Rupr. et Maxim.) Harms                                           | Ⅲ    | [刺五加](../Page/刺五加.md "wikilink")     |
| [唇形科植物](../Page/唇形科.md "wikilink")[黄岑](../Page/黄岑.md "wikilink")       | *Scutellaria baicalensis* Georgi                                                            | Ⅲ    | [黄芩](../Page/黄芩.md "wikilink")       |
| [百合科植物](../Page/百合科.md "wikilink")[天门冬](../Page/天门冬.md "wikilink")     | *Asparagus cochinchinensis* (Lour.) merr.                                                   | Ⅲ    | [天冬](../Page/天冬.md "wikilink")       |
| [多孔菌科真菌](../Page/多孔菌科.md "wikilink")[猪苓](../Page/猪苓.md "wikilink")     | *Polyporus umbellatus* (Pers.) Fries                                                        | Ⅲ    | [猪苓](../Page/猪苓.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[条叶龙胆](../Page/条叶龙胆.md "wikilink")   | *Gentiana manshurica* Kitag.                                                                | Ⅲ    | [龙胆](../Page/龙胆.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[龙胆](../Page/龙胆.md "wikilink")       | *Gentiana scabra* Bge                                                                       | Ⅲ    | [龙胆](../Page/龙胆.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[三花龙胆](../Page/三花龙胆.md "wikilink")   | *Gentiana triflora* Pall.                                                                   | Ⅲ    | [龙胆](../Page/龙胆.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[坚龙胆](../Page/坚龙胆.md "wikilink")     | *Gentiana regescens* Franch.                                                                | Ⅲ    | [龙胆](../Page/龙胆.md "wikilink")       |
| [伞形科植物](../Page/伞形科.md "wikilink")[防风](../Page/防风.md "wikilink")       | *Ledebouriella divaricata* (Turcz.) Hiroe                                                   | Ⅲ    | [防风](../Page/防风.md "wikilink")       |
| [远志科植物](../Page/远志科.md "wikilink")[远志](../Page/远志.md "wikilink")       | *Polygala tenuifolia* Willd.                                                                | Ⅲ    | [远志](../Page/远志.md "wikilink")       |
| [远志科植物](../Page/远志科.md "wikilink")[卵叶远志](../Page/卵叶远志.md "wikilink")   | *Polygala sibirica* L.                                                                      | Ⅲ    | [远志](../Page/远志.md "wikilink")       |
| [玄参科植物](../Page/玄参科.md "wikilink")[胡黄连](../Page/胡黄连.md "wikilink")     | *Picrorhiza scrophulariiflora* Pennell                                                      | Ⅲ    | [胡黄连](../Page/胡黄连.md "wikilink")     |
| [列当科植物](../Page/列当科.md "wikilink")[肉苁蓉](../Page/肉苁蓉.md "wikilink")     | *Cistanche deserticola* Y. C. Ma                                                            | Ⅲ    | [肉苁蓉](../Page/肉苁蓉.md "wikilink")     |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[秦艽](../Page/秦艽.md "wikilink")       | *Gentiana macrophylla* Pall.                                                                | Ⅲ    | [秦艽](../Page/秦艽.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[麻花秦艽](../Page/麻花秦艽.md "wikilink")   | *Gentiana macrophylla* Maxim.                                                               | Ⅲ    | [秦艽](../Page/秦艽.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[粗茎秦艽](../Page/粗茎秦艽.md "wikilink")   | *Gentiana crassicaulis* Duthie ex Burk.                                                     | Ⅲ    | [秦艽](../Page/秦艽.md "wikilink")       |
| [龙胆科植物](../Page/龙胆科.md "wikilink")[小秦艽](../Page/小秦艽.md "wikilink")     | *Gentiana dahurica* Fisch.                                                                  | Ⅲ    | [秦艽](../Page/秦艽.md "wikilink")       |
| [马兜铃科植物](../Page/马兜铃科.md "wikilink")[北细辛](../Page/北细辛.md "wikilink")   | *Asarum heterotropoides* Fr. var. mandshuricum (Maxim.)Kitag.                               | Ⅲ    | [细辛](../Page/细辛.md "wikilink")       |
| [马兜铃科植物](../Page/马兜铃科.md "wikilink")[汉城细辛](../Page/汉城细辛.md "wikilink") | *Asarum sieboldii* Miq. var. seoulense Nakai                                                | Ⅲ    | [细辛](../Page/细辛.md "wikilink")       |
| [马兜铃科植物](../Page/马兜铃科.md "wikilink")[细辛](../Page/细辛.md "wikilink")     | *Asarum sieboldii* Miq.                                                                     | Ⅲ    | [细辛](../Page/细辛.md "wikilink")       |
| [紫草科植物](../Page/紫草科.md "wikilink")[新疆紫草](../Page/新疆紫草.md "wikilink")   | *Arnebia euchroma* (Royle) Johnst.                                                          | Ⅲ    | [紫草](../Page/紫草.md "wikilink")       |
| [紫草科植物](../Page/紫草科.md "wikilink")[紫草](../Page/紫草.md "wikilink")       | *Lithospermum erythrorhizon* Sieb. et Zucc.                                                 | Ⅲ    | [紫草](../Page/紫草.md "wikilink")       |
| [木兰科植物](../Page/木兰科.md "wikilink")[五味子](../Page/五味子.md "wikilink")     | *Schisandra chinensis* (Turcz.) Baill.                                                      | Ⅲ    | [五味子](../Page/五味子.md "wikilink")     |
| [木兰科植物](../Page/木兰科.md "wikilink")[华中五味子](../Page/华中五味子.md "wikilink") | *Schisandra sphenanthera* Rehd. et Wils.                                                    | Ⅲ    | [五味子](../Page/五味子.md "wikilink")     |
| [马鞭草科植物](../Page/马鞭草科.md "wikilink")[单叶蔓荆](../Page/单叶蔓荆.md "wikilink") | *Vitex trifolia L. var. simplicifolia* Cham.                                                | Ⅲ    | [蔓荆子](../Page/蔓荆子.md "wikilink")     |
| [马鞭草科植物](../Page/马鞭草科.md "wikilink")[蔓荆](../Page/蔓荆.md "wikilink")     | *Vitex trifolia* L.                                                                         | Ⅲ    | [蔓荆子](../Page/蔓荆子.md "wikilink")     |
| [使君子科植物](../Page/使君子科.md "wikilink")[诃子](../Page/诃子.md "wikilink")     | *Terminalia chebula* Retz.                                                                  | Ⅲ    | [诃子](../Page/诃子.md "wikilink")       |
| [使君子科植物](../Page/使君子科.md "wikilink")[绒毛诃子](../Page/绒毛诃子.md "wikilink") | *Terminalia chebula Retz. var. tomentella* Kurt.                                            | Ⅲ    | [诃子](../Page/诃子.md "wikilink")       |
| [山茱萸科植物](../Page/山茱萸科.md "wikilink")[山茱萸](../Page/山茱萸.md "wikilink")   | *Cornus officinalis* sieb. et Zucc.                                                         | Ⅲ    | [山茱萸](../Page/山茱萸.md "wikilink")     |
| [兰科植物](../Page/兰科.md "wikilink")[环草石斛](../Page/环草石斛.md "wikilink")     | *Dendrobium loddigessii* Rolfe.                                                             | Ⅲ    | [石斛](../Page/石斛.md "wikilink")       |
| [兰科植物](../Page/兰科.md "wikilink")[马鞭石斛](../Page/马鞭石斛.md "wikilink")     | *Dendrobium fimbriatum* Hook. var. oculatum Hook.                                           | Ⅲ    | [石斛](../Page/石斛.md "wikilink")       |
| [兰科植物](../Page/兰科.md "wikilink")[黄草石斛](../Page/黄草石斛.md "wikilink")     | *Dendrobium chrysanthum* Wall.                                                              | Ⅲ    | [石斛](../Page/石斛.md "wikilink")       |
| [兰科植物](../Page/兰科.md "wikilink")[铁皮石斛](../Page/铁皮石斛.md "wikilink")     | *Dendrobium candidum* Wall. ex Lindl.                                                       | Ⅲ    | [石斛](../Page/石斛.md "wikilink")       |
| [兰科植物](../Page/兰科.md "wikilink")[金钗石斛](../Page/金钗石斛.md "wikilink")     | *Dendrobium nobile* Lindl.                                                                  | Ⅲ    | [石斛](../Page/石斛.md "wikilink")       |
| [伞形科植物](../Page/伞形科.md "wikilink")[新疆阿魏](../Page/新疆阿魏.md "wikilink")   | *Ferula sinkiangensis* K. M. shep.                                                          | Ⅲ    | [阿魏](../Page/阿魏.md "wikilink")       |
| [伞形科植物](../Page/伞形科.md "wikilink")[阜康阿魏](../Page/阜康阿魏.md "wikilink")   | *Ferula fukanensis* K. M. Shen.                                                             | Ⅲ    | [阿魏](../Page/阿魏.md "wikilink")       |
| [木犀科植物](../Page/木犀科.md "wikilink")[连翘](../Page/连翘.md "wikilink")       | *Forsythia suspensa* (Thunb.) Vahl                                                          | Ⅲ    | [连翘](../Page/连翘.md "wikilink")       |
| [伞形科植物](../Page/伞形科.md "wikilink")[羌活](../Page/羌活.md "wikilink")       | *Notopterygium incisum* Ting ex H. T. Chang                                                 | Ⅲ    | [羌活](../Page/羌活.md "wikilink")       |
| [伞形科植物](../Page/伞形科.md "wikilink")[宽叶羌活](../Page/宽叶羌活.md "wikilink")   | *Notopterygium forbesii* Boiss.                                                             | Ⅲ    | [羌活](../Page/羌活.md "wikilink")       |

## 參考文獻

[Category:中國自然保育](../Category/中國自然保育.md "wikilink")
[Category:中华人民共和国列表](../Category/中华人民共和国列表.md "wikilink")

1.