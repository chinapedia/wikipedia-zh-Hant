**国立中正大学**是中华民国时期在江西设立，當今已不存在的一所综合性大学。

## 简介

1940年五月教育部拨款法币一百万元给江西省政府，6月由江西省政府创建国立中正大学于[泰和县杏嶺村](../Page/泰和县.md "wikilink")，九月初奉國民政府令，任命首任校长为著名植物分类学家[胡先骕](../Page/胡先骕.md "wikilink")，校名取义于中国传统文化中的重要理念“大中至正”，院系組織分文法、工、農三學院，內設政治、經濟、社會教育、機電工程、土木工程、化學工程、農藝、森林、畜牧獸醫等九學系，並另設研究部為研討實際問題之機構。1941年起，文法學院增設文史學系，於農學院增設生物學系。1942年秋，開辦師範專修科及行政管理專修科，又於贛縣龍嶺村設立分校。1943年八月，復開辦稅務專修科。隔年，增設土木工程專修科，農學院農藝學系增設雙班。

1945年抗战胜利后，国立中正大学迁至[南昌](../Page/南昌.md "wikilink")。十月底開始遷建工作，至十二月竣事。三十五年一月在望成崗復課開學，經核准增辦理學院，新設數學、物理、化學三學系；並將原附屬於農學院之生物學系併入。文法學院原有之文史學系則析為中國文學、外國語文及歷史三學系；社會教育學系改為教育學系。
1949年9月，国立中正大学改名**国立南昌大学**，並先後併入江西工專、農專、體專、水(利)專。1950年10月直接称为**南昌大学**。设政治学院、文学艺术学院、理学院、工学院、农学院5个学院，16个系。1952年農學院以南昌大學農學院為基礎，改稱為[江西農學院](../Page/江西農學院.md "wikilink")（现[江西农业大学](../Page/江西农业大学.md "wikilink")），繼承了國立中正大學的農學院並延續至今，為原國立中正大學的分支。

1953年，全国高校院系调整中，原南昌大學解體，大部分理工學院分別調整到[華中工學院](../Page/華中工學院.md "wikilink")、[華南工學院](../Page/華南工學院.md "wikilink")、[中南土木建築學院](../Page/中南土木建築學院.md "wikilink")（後又併入[湖南大學](../Page/湖南大學.md "wikilink")）、[中南礦冶學院](../Page/中南礦冶學院.md "wikilink")、[中南體育學院](../Page/中南體育學院.md "wikilink")、[中南財經學院](../Page/中南財經學院.md "wikilink")、[中山大學](../Page/中山大學.md "wikilink")、[武漢大學等](../Page/武漢大學.md "wikilink")14所高校，以中文、历史、生物、数学、物理、化学和艺术7科的部分专业及老师为基础，在原址成立**江西师范学院**。江西师范学院继承了原南昌大学全部校舍场地、大部分图书资料、一部分仪器设备以及1/3的教师、大部分职员和全部工人。

江西师范学院不是以师范部为基础或主体建立的，因为师范部本身没有校舍场地、没有图书资料、没有仪器设备，甚至没有形成自己的教师队伍。根据原南昌大学1952年12月教职员录，师范部9科除艺术科独立设置外，8科（含中语、史地、教育、数学、物理、化学、生物、体育）的科主任都是文法学院、理学院和体育专修科各有关系科的系科主任兼任，即这8科均是挂靠在其他学院有关系科的。原南昌大学留给江西师范学院教师75人，79%是文法学院和理学院教师，师范部教师只有16人，其中艺术科教师就有6人，占
38%。原南昌大学给江西师范学院留下了理学院院长[郭庆棻](../Page/郭庆棻.md "wikilink")、副教务长[谷霁光](../Page/谷霁光.md "wikilink")、副总务长[熊化奇](../Page/熊化奇.md "wikilink")、数学系主任[彭先荫](../Page/彭先荫.md "wikilink")、文史系代主任[欧阳琛](../Page/欧阳琛.md "wikilink")、艺术科主任[刘天浪](../Page/刘天浪.md "wikilink")、还有校务委员[吴士栋](../Page/吴士栋.md "wikilink")，而师范部主任[任言却未留下](../Page/任言.md "wikilink")，被调到[华南师范学院](../Page/华南师范学院.md "wikilink")。江西师范学院于1983年10月改名为[江西师范大学](../Page/江西师范大学.md "wikilink")。

1958年[江西大学成立](../Page/江西大学.md "wikilink")，江西师范学院生物系并入江西大学。1993年5月，江西大学和1958年始建的[江西工业大学合并组建新的](../Page/江西工业大学.md "wikilink")[南昌大学](../Page/南昌大学.md "wikilink")。

國民政府遷台後，中正大學復校的期望一直沒在原校友的心中減弱過。相關單位也三番五次的招集這些老校友招開復校磋談事宜，多此舉辦餐會募款。1989年7月1日，台灣[國立中正大學成立](../Page/國立中正大學.md "wikilink")，自稱「因為紀念[蔣中正先生並平衡島內高等教育發展](../Page/蔣中正.md "wikilink")，在[嘉義縣成立國立中正大學](../Page/嘉義縣.md "wikilink")。與原國立中正大學名稱相同純屬巧合，並非原國立中正大學在台灣復校」。然其[校刊](http://secretar.ccu.edu.tw/Alumni/EarlierAlumnus.pdf)內歷史又常有所謂「前期校友」、傑出校友介紹又常提大陸時期之校友成就以勉勵「學弟妹」。到底是出於如何考量要說台灣中正與1949年的大陸中正大學毫無關係，誠或有其不明之因。

[江西师范大学](../Page/江西师范大学.md "wikilink")、[江西农业大学等为原国立中正大学的直接继承者](../Page/江西农业大学.md "wikilink")，其中[江西师范大学继承最多](../Page/江西师范大学.md "wikilink")。[江西师范大学](../Page/江西师范大学.md "wikilink")、[南昌大学等都以原国立中正大学为前身](../Page/南昌大学.md "wikilink")，國立中正大學在臺灣的建立也有傳祚原國立中正大學的淵源。

## 参见

  - [國立中正大學](../Page/國立中正大學.md "wikilink")
  - [江西师范大学](../Page/江西师范大学.md "wikilink")
  - [南昌大学](../Page/南昌大学.md "wikilink")

[Category:中華民國已不存在的公立大學](../Category/中華民國已不存在的公立大學.md "wikilink")
[Category:泰和县](../Category/泰和县.md "wikilink")