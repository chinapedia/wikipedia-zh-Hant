**國際體操總會**（，缩写作
****；）。國際體操總會於1881年的7月23日在[比利時的](../Page/比利時.md "wikilink")[列日市成立](../Page/列日.md "wikilink")，现总部设置在[瑞士的](../Page/瑞士.md "wikilink")[洛桑](../Page/洛桑.md "wikilink")。直至2005年，國際體操總會已有130個會員國，並於每年舉辦林林總總的[體操賽事和活動](../Page/體操.md "wikilink")。例如[第十三屆世界普及體操節2007在](../Page/第十三屆世界普及體操節2007.md "wikilink")[奧地利舉行](../Page/奧地利.md "wikilink")。

国际体操联合会的正式用语为[英语](../Page/英语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[德语](../Page/德语.md "wikilink")。

## 宗旨

  - 通过合理的[体育教育以及开展](../Page/体育教育.md "wikilink")[竞技与](../Page/竞技体操.md "wikilink")[艺术体操活动](../Page/艺术体操.md "wikilink")，把各种有利于身体发展的力量调动起来；
  - 研究体操理论与实践；
  - 印发文件；
  - 指定比赛规定动作和规程；
  - 对所属会员国给予技术援助；
  - 举办国际性比赛；
  - 鼓励和加强各国运动员之间的友好交往。

## 下设机构

  - 男子体操技术委员会
  - 女子体操技术委员会
  - [艺术体操技术委员会](../Page/艺术体操.md "wikilink")
  - [韵律体操委员会](../Page/韵律体操.md "wikilink")
  - 普通体操委员会

## 參考文獻

## 外部連結

  - [國際體操總會網](http://www.fig-gymnastics.com/)

{{-}}

[Category:国际体育组织](../Category/国际体育组织.md "wikilink")
[国际体操联合会](../Category/国际体操联合会.md "wikilink")
[Category:體操](../Category/體操.md "wikilink")
[Category:总部在洛桑的国际组织](../Category/总部在洛桑的国际组织.md "wikilink")
[Category:1881年建立的組織](../Category/1881年建立的組織.md "wikilink")