**国际消除对女性使用暴力日**是1999年[12月17日](../Page/12月17日.md "wikilink")，[联合国大会通过](../Page/联合国.md "wikilink")[决议54/134](../Page/联合国大会决议.md "wikilink")\[1\],
指定**[11月25日](../Page/11月25日.md "wikilink")**为国际消除对女性使用暴力日。事因在1960年的这一天，[多米尼加共和国的政治活动家](../Page/多米尼加.md "wikilink")[米拉瓦尔三姐妹](../Page/米拉瓦尔.md "wikilink")，被[特鲁希略政权](../Page/特鲁希略.md "wikilink")（1930-1961）暗杀
，从1981年起，一些妇女运动活动家就将这一天作为反抗妇女暴力问题的纪念日\[2\] 。

## 历史

1979年，[联合国通过了](../Page/联合国.md "wikilink")《[消除对妇女一切形式歧视公约](../Page/消除对妇女一切形式歧视公约.md "wikilink")》，确立了对基于性别歧视的妇女问题的反对和积极干预。1985年，[世界妇女大会通过了提高妇女地位的](../Page/世界妇女大会.md "wikilink")[内罗毕前瞻性战略](../Page/内罗毕.md "wikilink")，突出强调了妇女的暴力问题需要得到更多关注。1993年，联合国又发表了《消除针对妇女的暴力宣言》，第一次给“对妇女的暴力行为”下了定义：不论发生在公共场所或私人生活中，对妇女造成或可能造成身心上或性行为上的伤害或痛苦的任何基于性别的暴力行为。《宣言》指出，对妇女的家庭暴力是对妇女人权和基本自由的侵犯。1995年在北京举行的再次将这一问题列为主题，并成为会议通过的行动纲领的一项重点内容。1999年12月17日，[联合国大会通过](../Page/联合国.md "wikilink")11月25日为消除对妇女的暴力国际日\[3\]
。2000年12月22日，《消除对妇女一切形式歧视公约的任择议定书》开始生效。

## 外部連結

  - 《消除对妇女一切形式歧视公约》（中文）
    <http://www1.umn.edu/humanrts/chinese/CHe1cedaw.htm>
  - 《消除对妇女的暴力行为宣言》（中文）
    <http://www1.umn.edu/humanrts/chinese/CHe4devw.htm>
  - 《消除对妇女一切形式歧视公约的任择议定书》（中文）http://www1.umn.edu/humanrts/chinese/CHcedawopprot-2000.htm
  - [国际人权条约](http://www1.umn.edu/humanrts/chinese/CHainstls1.htm)
  - [December 17 - 國際終止對性工作者的暴力日](http://www.december17.org)

## 参考文献

[Category:11月節日](../Category/11月節日.md "wikilink")
[Category:聯合國紀念日](../Category/聯合國紀念日.md "wikilink")
[Category:女性](../Category/女性.md "wikilink")
[Category:厭女](../Category/厭女.md "wikilink")

1.

2.
3.