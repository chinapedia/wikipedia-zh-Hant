| 代碼  | 機場                                                     | 城市                                   | 国家和地区                                                |
| --- | ------------------------------------------------------ | ------------------------------------ | ---------------------------------------------------- |
| KBL | [喀布爾國際機場](../Page/喀布爾國際機場.md "wikilink")               | [喀布爾](../Page/喀布爾.md "wikilink")     | [阿富汗](../Page/阿富汗.md "wikilink")                     |
| KBS | [博城機場](../Page/博城機場.md "wikilink")                     | [博城](../Page/博城.md "wikilink")       | [塞拉利昂](../Page/塞拉利昂.md "wikilink")                   |
| KBV | [甲米機場](../Page/甲米機場.md "wikilink")                     | [甲米](../Page/甲米.md "wikilink")       | [泰國](../Page/泰國.md "wikilink")                       |
| KCA | [库车机场](../Page/库车机场.md "wikilink")                     | [阿克苏市](../Page/阿克苏市.md "wikilink")   | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KCH | [古晉國際機場](../Page/古晉國際機場.md "wikilink")                 | [古晉](../Page/古晉.md "wikilink")       | [馬來西亞](../Page/馬來西亞.md "wikilink")                   |
| KDH | [堪達哈國際機場](../Page/堪達哈國際機場.md "wikilink")               | [堪達哈](../Page/坎大哈.md "wikilink")     | [阿富汗](../Page/阿富汗.md "wikilink")                     |
| KEF | [凱夫拉維克國際機場](../Page/凱夫拉維克國際機場.md "wikilink")           | [凱夫拉維克](../Page/凱夫拉維克.md "wikilink") | [冰島](../Page/冰島.md "wikilink")                       |
| KEN | [凱內馬機場](../Page/凱內馬機場.md "wikilink")                   | [凱內馬](../Page/凱內馬.md "wikilink")     | [塞拉利昂](../Page/塞拉利昂.md "wikilink")                   |
| KGL | [基加利國際機場](../Page/基加利國際機場.md "wikilink")               | [吉佳利](../Page/吉佳利.md "wikilink")     | [盧安達](../Page/盧安達.md "wikilink")                     |
| KHG | [喀什机场](../Page/喀什机场.md "wikilink")                     | [喀什市](../Page/喀什市.md "wikilink")     | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KHH | [高雄國際機場](../Page/高雄國際機場.md "wikilink")                 | [高雄市](../Page/高雄市.md "wikilink")     | [台灣](../Page/台灣.md "wikilink")                       |
| KHN | [南昌昌北國際機場](../Page/南昌昌北國際機場.md "wikilink")             | [南昌市](../Page/南昌市.md "wikilink")     | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KIX | [关西国际机场](../Page/关西国际机场.md "wikilink")                 | [大阪府](../Page/大阪府.md "wikilink")     | [日本](../Page/日本.md "wikilink")                       |
| KJI | [喀納斯機場](../Page/喀納斯機場.md "wikilink")                   | [布尔津县](../Page/布尔津县.md "wikilink")   | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KMG | [昆明长水国际机场](../Page/昆明长水国际机场.md "wikilink")             | [昆明市](../Page/昆明市.md "wikilink")     | [中国](../Page/中華人民共和國.md "wikilink")                  |
| KMI | [宮崎機場](../Page/宮崎機場.md "wikilink")                     | [宮崎縣](../Page/宮崎縣.md "wikilink")     | [日本](../Page/日本.md "wikilink")                       |
| KMJ | [熊本機場](../Page/熊本機場.md "wikilink")                     | [熊本縣](../Page/熊本縣.md "wikilink")     | [日本](../Page/日本.md "wikilink")                       |
| KMQ | [小松飛行場](../Page/小松飛行場.md "wikilink")                   | [石川縣](../Page/石川縣.md "wikilink")     | [日本](../Page/日本.md "wikilink")                       |
| KNH | [金門機場](../Page/金門機場.md "wikilink")                     | [金門縣](../Page/金門縣.md "wikilink")     | [台灣](../Page/台灣.md "wikilink")                       |
| KOJ | [鹿兒島機場](../Page/鹿兒島機場.md "wikilink")                   | [鹿兒島縣](../Page/鹿兒島縣.md "wikilink")   | [日本](../Page/日本.md "wikilink")                       |
| KOW | [贛州黃金機場](../Page/贛州黃金機場.md "wikilink")                 | [贛州市](../Page/贛州市.md "wikilink")     | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KRK | [克拉科夫若望保祿二世國際機場](../Page/克拉科夫若望保祿二世國際機場.md "wikilink") | [克拉科夫](../Page/克拉科夫.md "wikilink")   | [波蘭](../Page/波蘭.md "wikilink")                       |
| KRL | [库尔勒机场](../Page/库尔勒机场.md "wikilink")                   | [库尔勒市](../Page/库尔勒市.md "wikilink")   | [中國](../Page/中華人民共和國.md "wikilink")                  |
| KRT | [喀土穆國際機場](../Page/喀土穆國際機場.md "wikilink")               | [喀土穆](../Page/喀土穆.md "wikilink")     | [蘇丹](../Page/蘇丹共和國.md "wikilink")                    |
| KRY | [克拉玛依机场](../Page/克拉玛依机场.md "wikilink")                 | [克拉玛依市](../Page/克拉玛依市.md "wikilink") | [中国](../Page/中華人民共和國.md "wikilink")                  |
| KTM | [加德滿都國際機場](../Page/加德滿都國際機場.md "wikilink")             | [加德滿都](../Page/加德滿都.md "wikilink")   | [尼泊尔](../Page/尼泊尔.md "wikilink")                     |
| KUL | [吉隆坡国际机场](../Page/吉隆坡国际机场.md "wikilink")               | [雪邦](../Page/雪邦.md "wikilink")       | [马来西亚](../Page/马来西亚.md "wikilink")                   |
| KUN | [考納斯機場](../Page/考納斯機場.md "wikilink")                   | [考納斯](../Page/考納斯.md "wikilink")     | [立陶宛](../Page/立陶宛.md "wikilink")                     |
| KUV | [群山機場](../Page/群山機場.md "wikilink")                     | [群山市](../Page/群山市.md "wikilink")     | [韓國](../Page/韓國.md "wikilink")                       |
| KWE | [贵阳龙洞堡国际机场](../Page/贵阳龙洞堡国际机场.md "wikilink")           | [贵阳市](../Page/贵阳市.md "wikilink")     | [中国](../Page/中華人民共和國.md "wikilink")                  |
| KWI | [科威特國際機場](../Page/科威特國際機場.md "wikilink")               | [科威特市](../Page/科威特市.md "wikilink")   | [科威特](../Page/科威特.md "wikilink")                     |
| KWJ | [光州機場](../Page/光州機場.md "wikilink")                     | [光州市](../Page/光州廣域市.md "wikilink")   | [韓國](../Page/韓國.md "wikilink")                       |
| KWL | [桂林两江国际机场](../Page/桂林两江国际机场.md "wikilink")             | [桂林市](../Page/桂林市.md "wikilink")     | [中国](../Page/中華人民共和國.md "wikilink")                  |
| KYD | [蘭嶼機場](../Page/蘭嶼機場.md "wikilink")                     | [蘭嶼鄉](../Page/蘭嶼鄉.md "wikilink")     | [台灣](../Page/台灣.md "wikilink")                       |
| KZN | [喀山國際機場](../Page/喀山國際機場.md "wikilink")                 | [喀山](../Page/喀山.md "wikilink")       | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KAB | Kariba                                                 | 加里巴                                  | [辛巴威](../Page/辛巴威.md "wikilink")                     |
| KAE |                                                        | Kake                                 | [美國](../Page/美國.md "wikilink")                       |
| KAL |                                                        | Kaltag                               | [美國](../Page/美國.md "wikilink")                       |
| KAN | Aminu Kano International                               | Kano                                 | [尼日利亞](../Page/尼日利亞.md "wikilink")                   |
| KAT | Kaitaia                                                | Kaitaia                              | [新西蘭](../Page/新西蘭.md "wikilink")                     |
| KBP | Borispol                                               | 基輔                                   | [烏克蘭](../Page/烏克蘭.md "wikilink")                     |
| KBR | Sultan Ismail Petra                                    | 哥打峇魯                                 | [馬來西亞](../Page/馬來西亞.md "wikilink")                   |
| KEH |                                                        | Kenmore Air Harbor                   | [美國](../Page/美國.md "wikilink")                       |
| KEJ | Kemerovo                                               | Kemerovo                             | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KEL | Holtenau                                               | 基爾                                   | [德國](../Page/德國.md "wikilink")                       |
| KEM | Kemi                                                   | Kemi/Tornio                          | [芬蘭](../Page/芬蘭.md "wikilink")                       |
| KER | Kerman                                                 | Kerman                               | [伊朗](../Page/伊朗.md "wikilink")                       |
| KGC |                                                        | Kingscote                            | [澳洲](../Page/澳洲.md "wikilink")                       |
| KGD | Kaliningrad Airport                                    | Kaliningrad                          | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KHI | Karachi                                                | 喀拉蚩                                  | [巴基斯坦](../Page/巴基斯坦.md "wikilink")                   |
| KHV | Novy                                                   | Khabarovsk                           | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KIN |                                                        | 金士頓                                  | [牙買加](../Page/牙買加.md "wikilink")                     |
| KIR | Kerry County                                           | Kerry County                         | [愛爾蘭](../Page/愛爾蘭.md "wikilink")                     |
| KIV | Kishinev                                               | Kishinev                             | [莫爾達瓦](../Page/莫爾達瓦.md "wikilink")                   |
| KJA |                                                        | Krasnojarsk                          | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KKN | Hoeyburtmoen                                           | Kirkenes                             | [挪威](../Page/挪威.md "wikilink")                       |
| KLO | Kalibo                                                 | Kalibo                               | [菲律賓](../Page/菲律賓.md "wikilink")                     |
| KLR | Kalmar                                                 | Kalmar                               | [瑞典](../Page/瑞典.md "wikilink")                       |
| KLU | Klagenfurt                                             | Klagenfurt                           | [奧地利](../Page/奧地利.md "wikilink")                     |
| KLW |                                                        | Klawock                              | [美國](../Page/美國.md "wikilink")                       |
| KNS | King Island                                            | King Island                          | [澳洲](../Page/澳洲.md "wikilink")                       |
| KOA | Keahole                                                | Kona                                 | [美國](../Page/美國.md "wikilink")                       |
| KOI | Kirkwall                                               | Kirkwall / Orkney Island             | [UnitedKingdom](../Page/UnitedKingdom.md "wikilink") |
| KOK | Kruunupyy                                              | Kokkola/Pietarsaari                  | [芬蘭](../Page/芬蘭.md "wikilink")                       |
| KRN | Kiruna                                                 | Kiruna                               | [瑞典](../Page/瑞典.md "wikilink")                       |
| KRP | Karup                                                  | Karup                                | [丹麥](../Page/丹麥.md "wikilink")                       |
| KRR | Krasnodar                                              | Krasnodar                            | [俄羅斯](../Page/俄羅斯.md "wikilink")                     |
| KRS | Kjevik                                                 | Kristiansand                         | [挪威](../Page/挪威.md "wikilink")                       |
| KSA |                                                        | Kosrae                               | [密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")               |
| KSC | Barca                                                  | Kosice                               | [斯洛伐克](../Page/斯洛伐克.md "wikilink")                   |
| KSH | Bakhtaran Iran                                         | Kermanshah                           | [伊朗](../Page/伊朗.md "wikilink")                       |
| KSJ | Kasos Island                                           | Kasos Island                         | [希臘](../Page/希臘.md "wikilink")                       |
| KSU | Kvernberget                                            | Kristiansund                         | [挪威](../Page/挪威.md "wikilink")                       |
| KTN | Ketchikan International                                | Ketchikan                            | [美國](../Page/美國.md "wikilink")                       |
| KTR | Tindal                                                 | Katherine                            | [澳洲](../Page/澳洲.md "wikilink")                       |
| KTW | Pyrzowice                                              | Katowice                             | [波蘭](../Page/波蘭.md "wikilink")                       |
| KUA | Padang Geroda                                          | 關丹                                   | [馬來西亞](../Page/馬來西亞.md "wikilink")                   |
| KUO | Kuopio                                                 | Kuopio                               | [芬蘭](../Page/芬蘭.md "wikilink")                       |
| KUS | Metropolitan Area                                      | Kulusuk                              | [格林蘭](../Page/格林蘭.md "wikilink")                     |
| KVA | Kavala                                                 | Kavala                               | [希臘](../Page/希臘.md "wikilink")                       |
| KWA |                                                        | Kwajalein                            | [馬紹爾群島](../Page/馬紹爾群島.md "wikilink")                 |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")