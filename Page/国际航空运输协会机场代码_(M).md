| 代碼  | 機場                                                           | 城市                                                                                | 国家和地区                                    |
| --- | ------------------------------------------------------------ | --------------------------------------------------------------------------------- | ---------------------------------------- |
| MAA | [金奈國際機場](../Page/金奈國際機場.md "wikilink")                       | [金奈](../Page/金奈.md "wikilink")                                                    | [印度](../Page/印度.md "wikilink")           |
| MAD | [马德里巴拉哈斯机场](../Page/马德里巴拉哈斯机场.md "wikilink")                 | [马德里](../Page/马德里.md "wikilink")                                                  | [西班牙](../Page/西班牙.md "wikilink")         |
| MAJ | [馬紹爾群島國際機場](../Page/馬紹爾群島國際機場.md "wikilink")                 | [馬久羅](../Page/馬久羅.md "wikilink")                                                  | [馬紹爾群島](../Page/馬紹爾群島.md "wikilink")     |
| MAN | [曼徹斯特國際機場](../Page/曼徹斯特國際機場.md "wikilink")                   | [曼徹斯特](../Page/曼徹斯特.md "wikilink")                                                | [英國](../Page/英國.md "wikilink")           |
| MCI | [堪薩斯市國際機場](../Page/堪薩斯市國際機場.md "wikilink")                   | [堪薩斯市](../Page/堪薩斯市.md "wikilink")                                                | [美國](../Page/美國.md "wikilink")           |
| MCT | [馬斯喀特國際機場](../Page/馬斯喀特國際機場.md "wikilink")                   | [馬斯喀特](../Page/馬斯喀特.md "wikilink")                                                | [阿曼](../Page/阿曼.md "wikilink")           |
| MDC | [薩姆·拉圖蘭吉機場](../Page/薩姆·拉圖蘭吉機場.md "wikilink")                 | [萬鴉老](../Page/萬鴉老.md "wikilink")                                                  | [印尼](../Page/印尼.md "wikilink")           |
| MDG | [牡丹江海浪機場](../Page/牡丹江海浪機場.md "wikilink")                     | [牡丹江市](../Page/牡丹江市.md "wikilink")                                                | [中國](../Page/中華人民共和國.md "wikilink")      |
| MEL | [墨爾本國際機場](../Page/墨爾本國際機場.md "wikilink")                     | [墨爾本](../Page/墨爾本.md "wikilink")                                                  | [澳洲](../Page/澳洲.md "wikilink")           |
| MEM | [孟斐斯國際機場](../Page/孟斐斯國際機場.md "wikilink")                     | [孟斐斯](../Page/孟斐斯_\(田納西州\).md "wikilink")                                         | [美國](../Page/美國.md "wikilink")           |
| MEX | [墨西哥城國際機場](../Page/墨西哥城國際機場.md "wikilink")                   | [墨西哥城](../Page/墨西哥城.md "wikilink")                                                | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MFK | [馬祖北竿機場](../Page/馬祖北竿機場.md "wikilink")                       | [北竿鄉](../Page/北竿鄉.md "wikilink")                                                  | [台灣](../Page/台灣.md "wikilink")           |
| MFM | [澳門國際機場](../Page/澳門國際機場.md "wikilink")                       | [氹仔](../Page/氹仔.md "wikilink")                                                    | [澳門](../Page/澳門.md "wikilink")           |
| MIA | [邁阿密國際機場](../Page/邁阿密國際機場.md "wikilink")                     | [邁阿密](../Page/邁阿密.md "wikilink")                                                  | [美國](../Page/美國.md "wikilink")           |
| MLE | [馬累國際機場](../Page/馬累國際機場.md "wikilink")                       | [馬累](../Page/馬累.md "wikilink")                                                    | [馬爾地夫](../Page/馬爾地夫.md "wikilink")       |
| MMB | [女滿別機場](../Page/女滿別機場.md "wikilink")                         | [大空町](../Page/大空町.md "wikilink")                                                  | [日本](../Page/日本.md "wikilink")           |
| MMK | [莫曼斯克機場](../Page/莫曼斯克機場.md "wikilink")                       | [莫曼斯克](../Page/摩爾曼斯克.md "wikilink")                                               | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| MNL | [馬尼拉艾奎諾國際機場](../Page/馬尼拉艾奎諾國際機場.md "wikilink")               | [馬尼拉](../Page/馬尼拉.md "wikilink")                                                  | [菲律賓](../Page/菲律賓.md "wikilink")         |
| MSJ | [三澤飛行場](../Page/三澤飛行場.md "wikilink")                         | [青森縣](../Page/青森縣.md "wikilink")                                                  | [日本](../Page/日本.md "wikilink")           |
| MSP | [明尼阿波利斯-聖保羅國際機場](../Page/明尼阿波利斯-聖保羅國際機場.md "wikilink")       | [明尼阿波利斯](../Page/明尼阿波利斯.md "wikilink")/[聖保羅](../Page/聖保羅_\(明尼蘇達州\).md "wikilink") | [美國](../Page/美國.md "wikilink")           |
| MSQ | [明斯克國際機場](../Page/明斯克國際機場.md "wikilink")                     | [明斯克](../Page/明斯克.md "wikilink")                                                  | [白俄羅斯](../Page/白俄羅斯.md "wikilink")       |
| MSY | [路易阿姆斯壯紐奧良國際機場](../Page/路易阿姆斯壯紐奧良國際機場.md "wikilink")         | [紐奧良](../Page/紐奧良.md "wikilink")                                                  | [美國](../Page/美國.md "wikilink")           |
| MUC | [慕尼黑弗朗茨·約瑟夫·施特勞斯機場](../Page/慕尼黑弗朗茨·約瑟夫·施特勞斯機場.md "wikilink") | [慕尼黑](../Page/慕尼黑.md "wikilink")                                                  | [德國](../Page/德國.md "wikilink")           |
| MUX | [木尔坦国际机场](../Page/木尔坦国际机场.md "wikilink")                     | [木爾坦](../Page/木爾坦.md "wikilink")                                                  | [巴基斯坦](../Page/巴基斯坦.md "wikilink")       |
| MXP | [米蘭馬爾彭薩國際機場](../Page/米蘭馬爾彭薩國際機場.md "wikilink")               | [米蘭](../Page/米蘭.md "wikilink")                                                    | [義大利](../Page/義大利.md "wikilink")         |
| MYJ | [松山機場](../Page/松山機場_\(日本\).md "wikilink")                    | [愛媛縣](../Page/愛媛縣.md "wikilink")                                                  | [日本](../Page/日本.md "wikilink")           |
| MZG | [馬公機場](../Page/馬公機場.md "wikilink")                           | [馬公市](../Page/馬公市.md "wikilink")                                                  | [台灣](../Page/台灣.md "wikilink")           |
| MAF | Midland Intl Airport                                         | Midland/Odessa                                                                    | [美國](../Page/美國.md "wikilink")           |
| MAG | Madang                                                       | Madang                                                                            | [巴布亞新磯內亞](../Page/巴布亞新磯內亞.md "wikilink") |
| MAH | Aerop De Menorca                                             | Menorca                                                                           | [西班牙](../Page/西班牙.md "wikilink")         |
| MAM | Servando Canales                                             | Matamoros                                                                         | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MAO | Eduardo Gomes                                                | Manaus                                                                            | [巴西](../Page/巴西.md "wikilink")           |
| MAR | La Chinita                                                   | Maracaibo                                                                         | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| MAY |                                                              | Mangrove Cay                                                                      | [巴哈馬](../Page/巴哈馬.md "wikilink")         |
| MAZ | El Maui                                                      | Mayaguez                                                                          | [美國](../Page/美國.md "wikilink")           |
| MBA | Moi International                                            | 蒙巴薩                                                                               | [肯雅](../Page/肯雅.md "wikilink")           |
| MBJ | Sangster                                                     | Montego Bay                                                                       | [牙買加](../Page/牙買加.md "wikilink")         |
| MBL | Manistee Blacker Airport                                     | Manistee                                                                          | [美國](../Page/美國.md "wikilink")           |
| MBS | Tri                                                          | Midland / Bay City / Saginaw                                                      | [美國](../Page/美國.md "wikilink")           |
| MCE | Merced Municipal Airport                                     | Merced                                                                            | [美國](../Page/美國.md "wikilink")           |
| MCK | Municipal                                                    | Mc Cook                                                                           | [美國](../Page/美國.md "wikilink")           |
| MCM | Hel De Monte Carlo                                           | 蒙地卡羅                                                                              | [摩納哥](../Page/摩納哥.md "wikilink")         |
| MCN | Lewis B Wilson                                               | Macon                                                                             | [美國](../Page/美國.md "wikilink")           |
| MCP |                                                              | Macapa                                                                            | [巴西](../Page/巴西.md "wikilink")           |
| MCW | Mason City Municipal Airport                                 | Mason City                                                                        | [美國](../Page/美國.md "wikilink")           |
| MDE | La Playas                                                    | Medellin                                                                          | [哥倫比亞](../Page/哥倫比亞.md "wikilink")       |
| MDQ |                                                              | Mar Del Plata                                                                     | [阿根廷](../Page/阿根廷.md "wikilink")         |
| MDZ | El Plumerillo                                                | Mendoza                                                                           | [阿根廷](../Page/阿根廷.md "wikilink")         |
| MED | Madinah                                                      | Medinah                                                                           | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")   |
| MEI | Key Field                                                    | Meridian                                                                          | [美國](../Page/美國.md "wikilink")           |
| MES | Polonia                                                      | Medan                                                                             | [印度尼西亞](../Page/印度尼西亞.md "wikilink")     |
| MEY | Meghauli                                                     | Meghauli                                                                          | [尼泊爾](../Page/尼泊爾.md "wikilink")         |
| MFE |                                                              | Mc Allen/Mission                                                                  | [美國](../Page/美國.md "wikilink")           |
| MFN |                                                              | Milford Sound                                                                     | [新西蘭](../Page/新西蘭.md "wikilink")         |
| MFR | Medford                                                      | Medford                                                                           | [美國](../Page/美國.md "wikilink")           |
| MGA |                                                              | Managua                                                                           | [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")       |
| MGM | Dannelly Field                                               | 蒙哥馬利                                                                              | [美國](../Page/美國.md "wikilink")           |
| MGQ |                                                              | 摩加迪休                                                                              | [索馬里](../Page/索馬里.md "wikilink")         |
| MGW | Morgantown Municipal Airport                                 | Morgantown                                                                        | [美國](../Page/美國.md "wikilink")           |
| MHH |                                                              | Marsh Harbour                                                                     | [巴哈馬](../Page/巴哈馬.md "wikilink")         |
| MHK | Manhattan Municipal                                          | Manhattan                                                                         | [美國](../Page/美國.md "wikilink")           |
| MHQ |                                                              | Mariehamn                                                                         | [芬蘭](../Page/芬蘭.md "wikilink")           |
| MHT | Manchester                                                   | 曼徹斯特                                                                              | [美國](../Page/美國.md "wikilink")           |
| MID | Merida Internationl                                          | Merida                                                                            | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MIE | Delaware County Airport                                      | Muncie                                                                            | [美國](../Page/美國.md "wikilink")           |
| MIR | Skanes                                                       | Monastir                                                                          | [突尼斯](../Page/突尼斯.md "wikilink")         |
| MJT | Mytilene                                                     | Mytilene                                                                          | [希臘](../Page/希臘.md "wikilink")           |
| MJV | San Javier                                                   | Murcia                                                                            | [西班牙](../Page/西班牙.md "wikilink")         |
| MKE | General Mitchell Field                                       | 密爾瓦基                                                                              | [美國](../Page/美國.md "wikilink")           |
| MKG | Muskegon County Intl Airport                                 | Muskegon                                                                          | [美國](../Page/美國.md "wikilink")           |
| MKM |                                                              | Mukah                                                                             | [馬來西亞](../Page/馬來西亞.md "wikilink")       |
| MKW | Rendani                                                      | Manokwari                                                                         | [印度尼西亞](../Page/印度尼西亞.md "wikilink")     |
| MKY | Mackay                                                       | Mackay                                                                            | [澳大利亞](../Page/澳大利亞.md "wikilink")       |
| MLA | Luqa                                                         | 馬爾它                                                                               | [馬爾它](../Page/馬爾它.md "wikilink")         |
| MLB | Melbourne Regional Airport                                   | Melbourne                                                                         | [美國](../Page/美國.md "wikilink")           |
| MLH | Mulhouse                                                     | Mulhouse                                                                          | [法國](../Page/法國.md "wikilink")           |
| MLI | Quad City Airport                                            | Moline                                                                            | [美國](../Page/美國.md "wikilink")           |
| MLM | Municipal                                                    | Morelia                                                                           | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MLO | Milos                                                        | Milos                                                                             | [希臘](../Page/希臘.md "wikilink")           |
| MLS | Miles City                                                   | Miles City                                                                        | [美國](../Page/美國.md "wikilink")           |
| MLU |                                                              | Monroe                                                                            | [美國](../Page/美國.md "wikilink")           |
| MMH | Mammoth Lakes Airport                                        | Mammoth Lakes                                                                     | [美國](../Page/美國.md "wikilink")           |
| MMU | Morristown                                                   | Morristown                                                                        | [美國](../Page/美國.md "wikilink")           |
| MMX | Sturup                                                       | Malmo                                                                             | [瑞典](../Page/瑞典.md "wikilink")           |
| MNI | [威廉·亨利·布蘭布爾機場](../Page/威廉·亨利·布蘭布爾機場.md "wikilink")           | 蒙特塞拉特                                                                             | [蒙特塞拉特](../Page/蒙特塞拉特.md "wikilink")     |
| MNM | Twin County Airport                                          | Menominee                                                                         | [美國](../Page/美國.md "wikilink")           |
| MOB | Mobile Municipal                                             | Mobile                                                                            | [美國](../Page/美國.md "wikilink")           |
| MOD | Harry Sham Feild                                             | Modesto                                                                           | [美國](../Page/美國.md "wikilink")           |
| MOL | Aro                                                          | Molde                                                                             | [挪威](../Page/挪威.md "wikilink")           |
| MOT | Minot International Airport                                  | Minot                                                                             | [美國](../Page/美國.md "wikilink")           |
| MOW | Bykovo                                                       | 莫斯科                                                                               | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| MPA |                                                              | Mpacha                                                                            | [納米比亞](../Page/納米比亞.md "wikilink")       |
| MPB | Miami Public Seaplane Base                                   | 邁阿密                                                                               | [美國](../Page/美國.md "wikilink")           |
| MPL | Frejorgues                                                   | Montpellier                                                                       | [法國](../Page/法國.md "wikilink")           |
| MPM | Maputo International                                         | Maputo                                                                            | [莫三比克](../Page/莫三比克.md "wikilink")       |
| MQL | Mildura                                                      | Mildura                                                                           | [澳大利亞](../Page/澳大利亞.md "wikilink")       |
| MQT | Marquette County Airport                                     | Marquette                                                                         | [美國](../Page/美國.md "wikilink")           |
| MRD | Alberto Carnevalli                                           | Merida                                                                            | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| MRS | Marseille                                                    | 馬賽                                                                                | [法國](../Page/法國.md "wikilink")           |
| MRU | Plaisance                                                    | Mauritius                                                                         | [毛里求斯](../Page/毛里求斯.md "wikilink")       |
| MRY | Monterey Peninsula Airport                                   | Monterey / Carmel                                                                 | [美國](../Page/美國.md "wikilink")           |
| MSL | Muscle Shoals                                                | Muscle Shoals / Florence / Sheffield                                              | [美國](../Page/美國.md "wikilink")           |
| MSN | Dane County Regional Airport                                 | 麥迪遜                                                                               | [美國](../Page/美國.md "wikilink")           |
| MSO | Missoula International                                       | Missoula                                                                          | [美國](../Page/美國.md "wikilink")           |
| MSS |                                                              | Massena                                                                           | [美國](../Page/美國.md "wikilink")           |
| MST | Zuid                                                         | Maastricht                                                                        | [荷蘭](../Page/荷蘭.md "wikilink")           |
| MSU | Maseru                                                       | Maseru                                                                            | [萊索托](../Page/萊索托.md "wikilink")         |
| MTH |                                                              | 馬拉松                                                                               | [美國](../Page/美國.md "wikilink")           |
| MTJ | Montrose County Airport                                      | Montrose                                                                          | [美國](../Page/美國.md "wikilink")           |
| MTY | Escobedo                                                     | Monterrey                                                                         | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MUB |                                                              | Maun                                                                              | [博茲瓦那](../Page/博茲瓦那.md "wikilink")       |
| MUN |                                                              | Maturin                                                                           | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| MVD | Carrasco                                                     | 蒙特維多                                                                              | [烏拉圭](../Page/烏拉圭.md "wikilink")         |
| MVN | Mount Vernon Outland Airport                                 | Mount Vernon                                                                      | [美國](../Page/美國.md "wikilink")           |
| MVY |                                                              | Marthas Vineyard                                                                  | [美國](../Page/美國.md "wikilink")           |
| MWA |                                                              | Marion                                                                            | [美國](../Page/美國.md "wikilink")           |
| MXL | Rodolfo Sanchez Taboada                                      | Mexicali                                                                          | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MYA | Moruya                                                       | Moruya                                                                            | [澳大利亞](../Page/澳大利亞.md "wikilink")       |
| MYR |                                                              | Myrtle Beach                                                                      | [美國](../Page/美國.md "wikilink")           |
| MYY | Miri                                                         | Miri                                                                              | [馬來西亞](../Page/馬來西亞.md "wikilink")       |
| MZL | Santaguida                                                   | Manizales                                                                         | [哥倫比亞](../Page/哥倫比亞.md "wikilink")       |
| MZT | Buelna                                                       | Mazatlan                                                                          | [墨西哥](../Page/墨西哥.md "wikilink")         |
| MZV | Mulu Airport                                                 | Mulu                                                                              | [馬來西亞](../Page/馬來西亞.md "wikilink")       |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")