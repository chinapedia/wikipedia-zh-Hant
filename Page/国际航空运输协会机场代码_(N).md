| 代碼       | 機場                                                           | 城市                                       | 国家和地区                                        |
| -------- | ------------------------------------------------------------ | ---------------------------------------- | -------------------------------------------- |
| NAG      | [巴巴萨海布·阿姆倍伽尔博士国际机场](../Page/巴巴萨海布·阿姆倍伽尔博士国际机场.md "wikilink") | [那格浦尔](../Page/那格浦尔.md "wikilink")       | [印度](../Page/印度.md "wikilink")               |
| NAO      | [南充高坪機場](../Page/南充高坪機場.md "wikilink")                       | [南充市](../Page/南充市.md "wikilink")         | [中國](../Page/中華人民共和國.md "wikilink")          |
| NAP      | [那不勒斯國際機場](../Page/那不勒斯國際機場.md "wikilink")                   | [那不勒斯](../Page/那不勒斯.md "wikilink")       | [義大利](../Page/義大利.md "wikilink")             |
| NAY      | [南苑机场](../Page/北京南苑机场.md "wikilink")                         | [北京市](../Page/北京市.md "wikilink")         | [中国](../Page/中華人民共和國.md "wikilink")          |
| NBO      | [喬莫·肯雅塔國際機場](../Page/喬莫·肯雅塔國際機場.md "wikilink")               | [奈洛比](../Page/奈洛比.md "wikilink")         | [肯亞](../Page/肯亞.md "wikilink")               |
| NGB      | [宁波栎社国际机场](../Page/宁波栎社国际机场.md "wikilink")                   | [宁波市](../Page/宁波市.md "wikilink")         | [中国](../Page/中華人民共和國.md "wikilink")          |
| NGO      | [中部國際機場](../Page/中部國際機場.md "wikilink")                       | [愛知縣](../Page/愛知縣.md "wikilink")         | [日本](../Page/日本.md "wikilink")               |
| NGS      | [長崎機場](../Page/長崎機場.md "wikilink")                           | [長崎縣](../Page/長崎縣.md "wikilink")         | [日本](../Page/日本.md "wikilink")               |
| NJC      | [下瓦爾托夫斯克機場](../Page/下瓦爾托夫斯克機場.md "wikilink")                 | [下瓦爾托夫斯克](../Page/下瓦爾托夫斯克.md "wikilink") | [俄羅斯](../Page/俄羅斯.md "wikilink")             |
| NKG      | [南京禄口国际机场](../Page/南京禄口国际机场.md "wikilink")                   | [南京市](../Page/南京市.md "wikilink")         | [中国](../Page/中華人民共和國.md "wikilink")          |
| NKM\[1\] | [名古屋机场](../Page/名古屋机场.md "wikilink")                         | [愛知縣](../Page/愛知縣.md "wikilink")         | [日本](../Page/日本.md "wikilink")               |
| NLT      | [那拉提机场](../Page/那拉提机场.md "wikilink")                         | [新源县](../Page/新源县.md "wikilink")         | [中國](../Page/中華人民共和國.md "wikilink")          |
| NNY      | [南阳姜营机场](../Page/南阳姜营机场.md "wikilink")                       | [南阳市](../Page/南阳市.md "wikilink")         | [中国](../Page/中華人民共和國.md "wikilink")          |
| NRT\[2\] | [成田國際機場](../Page/成田國際機場.md "wikilink")                       | [千葉縣](../Page/千葉縣.md "wikilink")         | [日本](../Page/日本.md "wikilink")               |
| NTG      | [南通兴东机场](../Page/南通兴东机场.md "wikilink")                       | [南通市](../Page/南通市.md "wikilink")         | [中国](../Page/中華人民共和國.md "wikilink")          |
| NUE      | [紐倫堡機場](../Page/紐倫堡機場.md "wikilink")                         | [紐倫堡](../Page/紐倫堡.md "wikilink")         | [德國](../Page/德國.md "wikilink")               |
| NAK      | Nakhon Ratchasima                                            | Nakhon Ratchasima                        | [泰國](../Page/泰國.md "wikilink")               |
| NAN      | International                                                | Nadi                                     | [斐濟](../Page/斐濟.md "wikilink")               |
| NAS      | Nassau International Airport                                 | [拿骚（巴哈马）](../Page/拿索.md "wikilink")      | [巴哈馬](../Page/巴哈馬.md "wikilink")             |
| NAT      | Agusto Severo                                                | Natal                                    | [巴西](../Page/巴西.md "wikilink")               |
| NCA      |                                                              | 北凯克斯                                     | [特克斯和凯科斯群岛](../Page/特克斯和凯科斯群岛.md "wikilink") |
| NCE      | Cote D'azur                                                  | 尼斯                                       | [法國](../Page/法國.md "wikilink")               |
| NDJ      | N'djamena                                                    | N Djamena                                | [乍得](../Page/乍得.md "wikilink")               |
| NEC      | Necochea                                                     | Necochea                                 | [阿根廷](../Page/阿根廷.md "wikilink")             |
| NEV      |                                                              | [尼维斯](../Page/尼维斯.md "wikilink")         | [圣基茨和尼维斯](../Page/圣基茨和尼维斯.md "wikilink")     |
| NKC      | Nouakchott                                                   | Nouakchott                               | [茅利塔尼亞](../Page/茅利塔尼亞.md "wikilink")         |
| NLA      | Ndola                                                        | Ndola                                    | [尚比亞](../Page/尚比亞.md "wikilink")             |
| NLD      |                                                              | Nuevo Laredo                             | [墨西哥](../Page/墨西哥.md "wikilink")             |
| NLP      |                                                              | Nelspruit                                | [南非](../Page/南非.md "wikilink")               |
| NNG      | [南寧吳圩機場](../Page/南寧吳圩機場.md "wikilink")                       | [南寧](../Page/南寧.md "wikilink")           | [中國](../Page/中國.md "wikilink")               |
| NOU      | Tontouta                                                     | Noumea                                   | [新卡裡多尼亞](../Page/新卡裡多尼亞.md "wikilink")       |
| NPL      | New Plymouth                                                 | New Plymouth                             | [新西蘭](../Page/新西蘭.md "wikilink")             |
| NQY      | Newquay Civil                                                | Newquay                                  | [聯合王國](../Page/聯合王國.md "wikilink")           |
| NSN      | Nelson                                                       | Nelson                                   | [新西蘭](../Page/新西蘭.md "wikilink")             |
| NTE      | Nantes                                                       | [南特](../Page/南特.md "wikilink")           | [法國](../Page/法國.md "wikilink")               |
| NTL      | Williamtown                                                  | Newcastle                                | [澳大利亞](../Page/澳大利亞.md "wikilink")           |
| NVT      |                                                              | Navegantes                               | [巴西](../Page/巴西.md "wikilink")               |
| NWI      | Norwich                                                      | [諾維奇](../Page/諾維奇.md "wikilink")         | [英國](../Page/英國.md "wikilink")               |

## 注釋

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")

1.  在[中部國際機場啟用前採用](../Page/中部國際機場.md "wikilink")**NGO**
2.  在[羽田機場恢復國際線運作時](../Page/羽田機場.md "wikilink")，一度使用代號**TYO**