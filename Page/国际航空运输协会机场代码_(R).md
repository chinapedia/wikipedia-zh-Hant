| 代碼  | 機場                                           | 城市                                 | 国家和地区                                    |
| --- | -------------------------------------------- | ---------------------------------- | ---------------------------------------- |
| REP | [吴哥国际机场](../Page/吴哥国际机场.md "wikilink")       | [暹粒市](../Page/暹粒市.md "wikilink")   | [柬埔寨](../Page/柬埔寨.md "wikilink")         |
| RGN | [仰光国际机场](../Page/仰光国际机场.md "wikilink")       | [仰光](../Page/仰光.md "wikilink")     | [缅甸](../Page/缅甸.md "wikilink")           |
| RIX | [里加國際機場](../Page/里加國際機場.md "wikilink")       | [里加](../Page/里加.md "wikilink")     | [拉脫維亞](../Page/拉脫維亞.md "wikilink")       |
| RMQ | [台中清泉崗機場](../Page/台中清泉崗機場.md "wikilink")     | [台中市](../Page/台中市.md "wikilink")   | [台灣](../Page/台灣.md "wikilink")           |
| RTM | [鹿特丹機場](../Page/鹿特丹機場.md "wikilink")         | [鹿特丹](../Page/鹿特丹.md "wikilink")   | [荷蘭](../Page/荷蘭.md "wikilink")           |
| RAB | Lakunai                                      | Rabaul                             | [巴布亞新磯內亞](../Page/巴布亞新磯內亞.md "wikilink") |
| RAJ | Rajkot                                       | Rajkot                             | [印度](../Page/印度.md "wikilink")           |
| RAP | Rapid City Regional Airport                  | Rapid City                         | [美國](../Page/美國.md "wikilink")           |
| RAR | Rarotonga                                    | Rarotonga                          | [科克群島](../Page/科克群島.md "wikilink")       |
| RBA | Sale                                         | Rabat                              | [摩洛哥](../Page/摩洛哥.md "wikilink")         |
| RCB | Richards Bay                                 | Richards Bay                       | [南非](../Page/南非.md "wikilink")           |
| RCH |                                              | Riohacha                           | [哥倫比亞](../Page/哥倫比亞.md "wikilink")       |
| RDD | Redding Municipal Airport                    | Redding                            | [美國](../Page/美國.md "wikilink")           |
| RDG | Municipal / Spaatz Field                     | Reading                            | [美國](../Page/美國.md "wikilink")           |
| RDU | Raleigh Durham International Arpt            | Raleigh/Durham                     | [美國](../Page/美國.md "wikilink")           |
| REC | Guararapes International                     | Recife                             | [巴西](../Page/巴西.md "wikilink")           |
| REG | Tito Menniti                                 | Reggio Calabria                    | [義大利](../Page/義大利.md "wikilink")         |
| RES |                                              | Resistencia                        | [阿根廷](../Page/阿根廷.md "wikilink")         |
| REU | Reus                                         | Reus                               | [西班牙](../Page/西班牙.md "wikilink")         |
| REX | General Lucio Blanco Airport                 | Reynosa                            | [墨西哥](../Page/墨西哥.md "wikilink")         |
| RFD | Greater Rockford                             | Rockford                           | [美國](../Page/美國.md "wikilink")           |
| RGA |                                              | Rio Grande                         | [阿根廷](../Page/阿根廷.md "wikilink")         |
| RGL | Rio Gallegos                                 | Rio Gallegos                       | [阿根廷](../Page/阿根廷.md "wikilink")         |
| RHI | Rhinelander Oneida County Airport            | Rhinelander                        | [美國](../Page/美國.md "wikilink")           |
| RHO | Paradisi                                     | Rhodes                             | [希臘](../Page/希臘.md "wikilink")           |
| RIC | Richmond International Airport               | 裡奇蒙                                | [美國](../Page/美國.md "wikilink")           |
| RIO |                                              | 里約熱內盧                              | [巴西](../Page/巴西.md "wikilink")           |
| RIZ | [日照山字河机场](../Page/日照山字河机场.md "wikilink")     | [日照市](../Page/日照市.md "wikilink")   | [中华人民共和国](../Page/中华人民共和国.md "wikilink") |
| RIW | Riverton Regional Airport                    | Riverton                           | [美國](../Page/美國.md "wikilink")           |
| RKD | Rockland                                     | Rockland                           | [美國](../Page/美國.md "wikilink")           |
| RKS | Rock Springs Sweetwater Cty Arpt             | Rock Springs                       | [美國](../Page/美國.md "wikilink")           |
| RKV | Reykjavik Domestic Airport                   | 雷克亞維克                              | [冰島](../Page/冰島.md "wikilink")           |
| RMA | Roma                                         | Roma                               | [澳大利亞](../Page/澳大利亞.md "wikilink")       |
| RNB | Kallinge                                     | Ronneby                            | [瑞典](../Page/瑞典.md "wikilink")           |
| RNO | Reno                                         | 雷諾                                 | [美國](../Page/美國.md "wikilink")           |
| RNS | St Jacques                                   | Rennes                             | [法國](../Page/法國.md "wikilink")           |
| ROA | Roanoke Regional Airport                     | Roanoke                            | [美國](../Page/美國.md "wikilink")           |
| ROC | Monroe County Airport                        | 羅切斯特                               | [美國](../Page/美國.md "wikilink")           |
| ROP | Rota                                         | Rota                               | [北馬利安納群島](../Page/北馬利安納群島.md "wikilink") |
| ROR | [羅曼·莫圖國際機場](../Page/羅曼·莫圖國際機場.md "wikilink") | [艾拉伊州](../Page/艾拉伊州.md "wikilink") | [帛琉共和國](../Page/帛琉共和國.md "wikilink")     |
| ROS | Fisherton                                    | Rosario                            | [阿根廷](../Page/阿根廷.md "wikilink")         |
| ROV | Rostov                                       | Rostov                             | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| ROW | Industrial Air Center                        | Roswell                            | [美國](../Page/美國.md "wikilink")           |
| RPR |                                              | Raipur                             | [印度](../Page/印度.md "wikilink")           |
| RRG | Rodrigues                                    | Rodrigues Island                   | [毛里求斯](../Page/毛里求斯.md "wikilink")       |
| RSD |                                              | Rock Sound                         | [巴哈馬](../Page/巴哈馬.md "wikilink")         |
| RST | Rochester Municipal                          | 羅切斯特                               | [美國](../Page/美國.md "wikilink")           |
| RTB | Roatan                                       | Roatan                             | [洪都拉斯](../Page/洪都拉斯.md "wikilink")       |
| RUH | King Khaled Intl                             | 利雅德                                | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")   |
| RUI |                                              | Ruidoso                            | [美國](../Page/美國.md "wikilink")           |
| RUT |                                              | Rutland                            | [美國](../Page/美國.md "wikilink")           |
| RWI | Wilson                                       | Rocky Mount                        | [美國](../Page/美國.md "wikilink")           |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")