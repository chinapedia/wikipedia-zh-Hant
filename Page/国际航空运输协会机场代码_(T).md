<table>
<thead>
<tr class="header">
<th><p>代碼</p></th>
<th><p>機場</p></th>
<th><p>城市</p></th>
<th><p>国家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>TAB</p></td>
<td></td>
<td><p><a href="../Page/斯卡伯勒_(多巴哥).md" title="wikilink">斯卡伯勒</a></p></td>
<td><p><a href="../Page/千里達及托巴哥.md" title="wikilink">千里達及托巴哥</a></p></td>
</tr>
<tr class="even">
<td><p>TAC</p></td>
<td><p><a href="../Page/獨魯萬機場.md" title="wikilink">獨魯萬機場</a></p></td>
<td><p><a href="../Page/獨魯萬市.md" title="wikilink">獨魯萬市</a></p></td>
<td><p><a href="../Page/菲律賓.md" title="wikilink">菲律賓</a></p></td>
</tr>
<tr class="odd">
<td><p>TAD</p></td>
<td></td>
<td><p><a href="../Page/特立尼达_(科罗拉多州).md" title="wikilink">特立尼达 (科罗拉多州)</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TAE</p></td>
<td><p><a href="../Page/大邱國際機場.md" title="wikilink">大邱國際機場</a></p></td>
<td><p><a href="../Page/大邱市.md" title="wikilink">大邱市</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p>TAG</p></td>
<td><p><a href="../Page/塔比拉蘭機場.md" title="wikilink">塔比拉蘭機場</a></p></td>
<td></td>
<td><p><a href="../Page/菲律賓.md" title="wikilink">菲律賓</a></p></td>
</tr>
<tr class="even">
<td><p>TAI</p></td>
<td><p><a href="../Page/塔伊茲國際機場.md" title="wikilink">塔伊茲國際機場</a></p></td>
<td><p><a href="../Page/塔伊茲.md" title="wikilink">塔伊茲</a></p></td>
<td><p><a href="../Page/葉門.md" title="wikilink">葉門</a></p></td>
</tr>
<tr class="odd">
<td><p>TAK</p></td>
<td><p><a href="../Page/高松機場.md" title="wikilink">高松機場</a></p></td>
<td><p><a href="../Page/高松市.md" title="wikilink">高松市</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="even">
<td><p>TAM</p></td>
<td></td>
<td><p><a href="../Page/坦皮科.md" title="wikilink">坦皮科</a></p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>TAO</p></td>
<td><p><a href="../Page/青岛流亭国际机场.md" title="wikilink">青岛流亭国际机场</a></p></td>
<td><p><a href="../Page/青岛市.md" title="wikilink">青岛市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="even">
<td><p>TAP</p></td>
<td></td>
<td><p><a href="../Page/塔帕丘拉_(恰帕斯州).md" title="wikilink">塔帕丘拉</a></p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>TAS</p></td>
<td><p><a href="../Page/塔什干國際機場.md" title="wikilink">塔什干國際機場</a></p></td>
<td><p><a href="../Page/塔什干.md" title="wikilink">塔什干</a></p></td>
<td><p><a href="../Page/烏茲別克.md" title="wikilink">烏茲別克</a></p></td>
</tr>
<tr class="even">
<td><p>TAT</p></td>
<td></td>
<td><p><a href="../Page/波普拉德.md" title="wikilink">波普拉德</a></p></td>
<td><p><a href="../Page/斯洛伐克.md" title="wikilink">斯洛伐克</a></p></td>
</tr>
<tr class="odd">
<td><p>TBP</p></td>
<td></td>
<td><p><a href="../Page/通貝斯.md" title="wikilink">通貝斯</a></p></td>
<td><p><a href="../Page/秘鲁.md" title="wikilink">秘鲁</a></p></td>
</tr>
<tr class="even">
<td><p>TBS</p></td>
<td><p><a href="../Page/提比里西國際機場.md" title="wikilink">提比里西國際機場</a></p></td>
<td><p><a href="../Page/第比利斯.md" title="wikilink">第比利斯</a></p></td>
<td><p><a href="../Page/喬治亞.md" title="wikilink">喬治亞</a></p></td>
</tr>
<tr class="odd">
<td><p>TBT</p></td>
<td></td>
<td><p><a href="../Page/塔巴廷加_(亞馬遜州).md" title="wikilink">塔巴廷加 (亞馬遜州)</a></p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="even">
<td><p>TCG</p></td>
<td><p><a href="../Page/塔城机场.md" title="wikilink">塔城机场</a></p></td>
<td><p><a href="../Page/塔城市.md" title="wikilink">塔城市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="odd">
<td><p>TCL</p></td>
<td></td>
<td><p><a href="../Page/塔斯卡盧薩_(阿拉巴馬州).md" title="wikilink">塔斯卡盧薩 (阿拉巴馬州)</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TDD</p></td>
<td></td>
<td><p><a href="../Page/特立尼達_(玻利維亞).md" title="wikilink">千里達</a></p></td>
<td><p><a href="../Page/玻利维亚.md" title="wikilink">玻利维亚</a></p></td>
</tr>
<tr class="odd">
<td><p>TED</p></td>
<td><p><a href="../Page/齊斯泰茲機場.md" title="wikilink">齊斯泰茲機場</a></p></td>
<td></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a></p></td>
</tr>
<tr class="even">
<td><p>TER</p></td>
<td></td>
<td><p><a href="../Page/特塞拉島.md" title="wikilink">特塞拉島</a>,<a href="../Page/亞速群島.md" title="wikilink">亞速</a></p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a></p></td>
</tr>
<tr class="odd">
<td><p>TEX</p></td>
<td></td>
<td><p><a href="../Page/特柳賴德.md" title="wikilink">特柳賴德</a>,(<a href="../Page/科罗拉多州.md" title="wikilink">科罗拉多州</a>)</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TEZ</p></td>
<td></td>
<td><p><a href="../Page/泰茲普爾.md" title="wikilink">泰茲普爾</a></p></td>
<td><p><a href="../Page/印度.md" title="wikilink">印度</a></p></td>
</tr>
<tr class="odd">
<td><p>TFN</p></td>
<td><p><a href="../Page/特內里費北部機場.md" title="wikilink">特內里費北部機場</a></p></td>
<td><p><a href="../Page/特內里費島.md" title="wikilink">特內里費島</a></p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a></p></td>
</tr>
<tr class="even">
<td><p>TFS</p></td>
<td><p><a href="../Page/特内里费南部机场.md" title="wikilink">特内里费南部机场</a></p></td>
<td><p><a href="../Page/特內里費島.md" title="wikilink">特內里費島</a></p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a></p></td>
</tr>
<tr class="odd">
<td><p>TGA</p></td>
<td><p><a href="../Page/登加機場.md" title="wikilink">登加機場</a></p></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="even">
<td><p>TGM</p></td>
<td></td>
<td><p><a href="../Page/特爾古穆列什.md" title="wikilink">特爾古穆列什</a></p></td>
<td><p><a href="../Page/羅馬尼亞.md" title="wikilink">羅馬尼亞</a></p></td>
</tr>
<tr class="odd">
<td><p>TGU</p></td>
<td><p><a href="../Page/通孔廷国际机场.md" title="wikilink">通孔廷国际机场</a></p></td>
<td><p><a href="../Page/德古斯加巴.md" title="wikilink">德古斯加巴</a></p></td>
<td><p><a href="../Page/宏都拉斯.md" title="wikilink">宏都拉斯</a></p></td>
</tr>
<tr class="even">
<td><p>TGZ</p></td>
<td></td>
<td><p><a href="../Page/圖斯特拉古鐵雷斯.md" title="wikilink">圖斯特拉古鐵雷斯</a></p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>THE</p></td>
<td></td>
<td><p><a href="../Page/特雷西納.md" title="wikilink">特雷西納</a></p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="even">
<td><p>THR</p></td>
<td><p><a href="../Page/梅赫拉巴德國際機場.md" title="wikilink">梅赫拉巴德國際機場</a></p></td>
<td><p><a href="../Page/德黑兰.md" title="wikilink">德黑兰</a></p></td>
<td><p><a href="../Page/伊朗.md" title="wikilink">伊朗</a></p></td>
</tr>
<tr class="odd">
<td><p>TIA</p></td>
<td><p><a href="../Page/地拉那特蕾莎修女國際機場.md" title="wikilink">地拉那國際機場</a></p></td>
<td><p><a href="../Page/地拉那.md" title="wikilink">地拉那</a></p></td>
<td><p><a href="../Page/阿爾巴尼亞.md" title="wikilink">阿爾巴尼亞</a></p></td>
</tr>
<tr class="even">
<td><p>TIJ</p></td>
<td></td>
<td><p><a href="../Page/提華納.md" title="wikilink">提華納</a></p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>TIQ</p></td>
<td><p><a href="../Page/天寧機場.md" title="wikilink">天寧機場</a></p></td>
<td><p><a href="../Page/天寧島.md" title="wikilink">天寧島</a></p></td>
<td><p><a href="../Page/北马里亚纳群岛.md" title="wikilink">北马里亚纳群岛</a>,<a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TIZ</p></td>
<td></td>
<td><p><a href="../Page/塔里.md" title="wikilink">塔里</a></p></td>
<td><p><a href="../Page/巴布亚新几内亚.md" title="wikilink">巴布亚新几内亚</a></p></td>
</tr>
<tr class="odd">
<td><p>TJA</p></td>
<td></td>
<td><p><a href="../Page/塔里哈.md" title="wikilink">塔里哈</a></p></td>
<td><p><a href="../Page/玻利维亚.md" title="wikilink">玻利维亚</a></p></td>
</tr>
<tr class="even">
<td><p>TKS</p></td>
<td><p><a href="../Page/德島機場.md" title="wikilink">德島機場</a></p></td>
<td><p><a href="../Page/德島縣.md" title="wikilink">德島縣</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="odd">
<td><p>TLL</p></td>
<td><p><a href="../Page/塔林倫納特·梅里機場.md" title="wikilink">塔林倫納特·梅里機場</a></p></td>
<td><p><a href="../Page/塔林.md" title="wikilink">塔林</a></p></td>
<td><p><a href="../Page/爱沙尼亚.md" title="wikilink">爱沙尼亚</a></p></td>
</tr>
<tr class="even">
<td><p>TLV</p></td>
<td><p><a href="../Page/本·古里安國際機場.md" title="wikilink">本·古里安國際機場</a></p></td>
<td><p><a href="../Page/特拉維夫.md" title="wikilink">特拉維夫</a></p></td>
<td><p><a href="../Page/以色列.md" title="wikilink">以色列</a></p></td>
</tr>
<tr class="odd">
<td><p>TMP</p></td>
<td><p><a href="../Page/坦佩雷-皮尔卡拉机场.md" title="wikilink">坦佩雷-皮尔卡拉机场</a></p></td>
<td><p><a href="../Page/坦佩雷.md" title="wikilink">坦佩雷</a></p></td>
<td><p><a href="../Page/芬蘭.md" title="wikilink">芬蘭</a></p></td>
</tr>
<tr class="even">
<td><p>TNA</p></td>
<td><p><a href="../Page/濟南遙牆國際機場.md" title="wikilink">濟南遙牆國際機場</a></p></td>
<td><p><a href="../Page/濟南市.md" title="wikilink">濟南市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="odd">
<td><p>TNN</p></td>
<td><p><a href="../Page/臺南機場.md" title="wikilink">臺南機場</a></p></td>
<td><p><a href="../Page/臺南市.md" title="wikilink">臺南市</a></p></td>
<td><p><a href="../Page/中华民国.md" title="wikilink">中华民国</a></p></td>
</tr>
<tr class="even">
<td><p>TOY</p></td>
<td><p><a href="../Page/富山機場.md" title="wikilink">富山機場</a></p></td>
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="odd">
<td><p>TPA</p></td>
<td><p><a href="../Page/坦帕國際機場.md" title="wikilink">坦帕國際機場</a></p></td>
<td><p><a href="../Page/坦帕_(佛羅里達州).md" title="wikilink">坦帕</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TPE</p></td>
<td><p><a href="../Page/臺灣桃園國際機場.md" title="wikilink">臺灣桃園國際機場</a></p></td>
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a></p></td>
<td><p><a href="../Page/中华民国.md" title="wikilink">中华民国</a></p></td>
</tr>
<tr class="odd">
<td><p>TSA[1]</p></td>
<td><p><a href="../Page/臺北松山機場.md" title="wikilink">臺北松山機場</a></p></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a></p></td>
<td><p><a href="../Page/中华民国.md" title="wikilink">中华民国</a></p></td>
</tr>
<tr class="even">
<td><p>TSN</p></td>
<td><p><a href="../Page/天津滨海国际机场.md" title="wikilink">天津滨海国际机场</a></p></td>
<td><p><a href="../Page/天津市.md" title="wikilink">天津市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="odd">
<td><p>TTJ</p></td>
<td><p><a href="../Page/鳥取機場.md" title="wikilink">鳥取機場</a></p></td>
<td><p><a href="../Page/鳥取縣.md" title="wikilink">鳥取縣</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="even">
<td><p>TTT</p></td>
<td><p><a href="../Page/臺東機場.md" title="wikilink">臺東機場</a></p></td>
<td><p><a href="../Page/臺東縣.md" title="wikilink">臺東縣</a></p></td>
<td><p><a href="../Page/中华民国.md" title="wikilink">中华民国</a></p></td>
</tr>
<tr class="odd">
<td><p>TVS</p></td>
<td><p><a href="../Page/唐山三女河机场.md" title="wikilink">唐山三女河机场</a></p></td>
<td><p><a href="../Page/唐山市.md" title="wikilink">唐山市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="even">
<td><p>TXK</p></td>
<td></td>
<td><p>(<a href="../Page/阿肯色州.md" title="wikilink">阿肯色州</a>)</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="odd">
<td><p>TXL</p></td>
<td><p><a href="../Page/柏林-泰格尔奥托·利林塔尔机场.md" title="wikilink">柏林-泰格尔奥托·利林塔尔机场</a></p></td>
<td><p><a href="../Page/柏林.md" title="wikilink">柏林</a></p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a></p></td>
</tr>
<tr class="even">
<td><p>TXN</p></td>
<td><p><a href="../Page/黃山屯溪機場.md" title="wikilink">黃山屯溪機場</a></p></td>
<td><p><a href="../Page/黃山市.md" title="wikilink">黃山市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="odd">
<td><p>TYN</p></td>
<td><p><a href="../Page/太原武宿機場.md" title="wikilink">太原武宿機場</a></p></td>
<td><p><a href="../Page/太原市.md" title="wikilink">太原市</a></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
</tr>
<tr class="even">
<td><p>TYR</p></td>
<td></td>
<td><p><a href="../Page/泰勒_(德克薩斯州).md" title="wikilink">泰勒 (德克薩斯州)</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="odd">
<td><p>TZX</p></td>
<td><p><a href="../Page/特拉布宗機場.md" title="wikilink">特拉布宗機場</a></p></td>
<td><p><a href="../Page/特拉布宗.md" title="wikilink">特拉布宗</a></p></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
</tr>
<tr class="even">
<td><p>TJM</p></td>
<td><p>Tyumen</p></td>
<td><p>Tyumen</p></td>
<td><p><a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a></p></td>
</tr>
<tr class="odd">
<td><p>TKK</p></td>
<td><p>Truk</p></td>
<td><p>Truk, <a href="../Page/卡罗琳群岛.md" title="wikilink">卡罗琳群岛</a></p></td>
<td><p><a href="../Page/密克罗尼西亚.md" title="wikilink">密克罗尼西亚</a></p></td>
</tr>
<tr class="even">
<td><p>TKU</p></td>
<td><p><a href="../Page/图尔库机场.md" title="wikilink">图尔库机场</a></p></td>
<td><p><a href="../Page/图尔库.md" title="wikilink">图尔库</a></p></td>
<td><p><a href="../Page/芬蘭.md" title="wikilink">芬蘭</a></p></td>
</tr>
<tr class="odd">
<td><p>TLH</p></td>
<td></td>
<td><p><a href="../Page/塔拉哈西.md" title="wikilink">塔拉哈西</a>, <a href="../Page/佛羅里達州.md" title="wikilink">佛羅里達州</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TLS</p></td>
<td><p>Blagnac</p></td>
<td><p>Toulouse</p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a></p></td>
</tr>
<tr class="odd">
<td><p>TMW</p></td>
<td><p>Tamworth</p></td>
<td><p>Tamworth, 新南威爾士州</p></td>
<td><p><a href="../Page/澳大利亞.md" title="wikilink">澳大利亞</a></p></td>
</tr>
<tr class="even">
<td><p>TNG</p></td>
<td><p>Boukhalef Souahel</p></td>
<td><p>Tangier</p></td>
<td><p>摩洛哥</p></td>
</tr>
<tr class="odd">
<td><p>TOL</p></td>
<td><p>Toledo Express Airport</p></td>
<td><p><a href="../Page/托雷多，俄亥俄州.md" title="wikilink">托雷多，俄亥俄州</a></p></td>
<td><p>美國</p></td>
</tr>
<tr class="even">
<td><p>TOS</p></td>
<td><p>Tromso/Langes</p></td>
<td><p>Tromso</p></td>
<td><p>挪威</p></td>
</tr>
<tr class="odd">
<td><p>TPL</p></td>
<td></td>
<td><p><a href="../Page/坦波.md" title="wikilink">坦波</a>，<a href="../Page/得克萨斯州.md" title="wikilink">得克萨斯州</a> (Temple,TX)</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>TPP</p></td>
<td><p>Tarapoto</p></td>
<td><p>Tarapoto</p></td>
<td><p>秘鲁</p></td>
</tr>
<tr class="odd">
<td><p>TPQ</p></td>
<td></td>
<td><p>Tepic, Nayarit</p></td>
<td><p>墨西哥</p></td>
</tr>
<tr class="even">
<td><p>TPS</p></td>
<td><p>Birgi</p></td>
<td><p>Trapani, 西西里</p></td>
<td><p>意大利</p></td>
</tr>
<tr class="odd">
<td><p>TRD</p></td>
<td><p>Trondheim-Vaernes</p></td>
<td><p>Trondheim</p></td>
<td><p>挪威</p></td>
</tr>
<tr class="even">
<td><p>TRC</p></td>
<td></td>
<td><p>Torreon, Coahuila</p></td>
<td><p>墨西哥</p></td>
</tr>
<tr class="odd">
<td><p>TRE</p></td>
<td><p>Tiree Island</p></td>
<td><p>Tiree, 蘇格蘭</p></td>
<td><p>联合王国</p></td>
</tr>
<tr class="even">
<td><p>TRG</p></td>
<td><p>Tauranga</p></td>
<td><p>Tauranga</p></td>
<td><p>紐西蘭</p></td>
</tr>
<tr class="odd">
<td><p>TRK</p></td>
<td><p>Tarakan</p></td>
<td><p>Tarakan</p></td>
<td><p>印度尼西亞</p></td>
</tr>
<tr class="even">
<td><p>TRN</p></td>
<td><p>Caselle</p></td>
<td><p>杜林</p></td>
<td><p>意大利</p></td>
</tr>
<tr class="odd">
<td><p>TRS</p></td>
<td><p>Ronchi Dei Legionari</p></td>
<td><p>Trieste</p></td>
<td><p>意大利</p></td>
</tr>
<tr class="even">
<td><p>TRU</p></td>
<td><p>Trujillo</p></td>
<td><p>Trujillo</p></td>
<td><p>秘鲁</p></td>
</tr>
<tr class="odd">
<td><p>TRV</p></td>
<td><p>Trivandrum</p></td>
<td><p>Trivandrum</p></td>
<td><p>印度</p></td>
</tr>
<tr class="even">
<td><p>TRZ</p></td>
<td><p>Civil</p></td>
<td><p>Tiruchirapally</p></td>
<td><p>印度</p></td>
</tr>
<tr class="odd">
<td><p>TSE</p></td>
<td><p>阿斯塔纳国际机场</p></td>
<td><p>阿斯塔纳</p></td>
<td><p>哈萨克斯坦</p></td>
</tr>
<tr class="even">
<td><p>TSR</p></td>
<td><p>Timisoara</p></td>
<td><p>Timisoara</p></td>
<td><p>羅馬尼亞</p></td>
</tr>
<tr class="odd">
<td><p>TSV</p></td>
<td><p>Townsville</p></td>
<td><p>Townsville, 昆士蘭省</p></td>
<td><p>澳大利亞</p></td>
</tr>
<tr class="even">
<td><p>TTE</p></td>
<td></td>
<td><p>Ternate, 印度尼西亞</p></td>
<td><p>Babullah</p></td>
</tr>
<tr class="odd">
<td><p>TTN</p></td>
<td><p>Mercer County</p></td>
<td><p>Trenton, 新泽西州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="even">
<td><p>TUC</p></td>
<td><p>Benjamin Matienzo</p></td>
<td><p>Tucuman, Tucuman</p></td>
<td><p>阿根廷</p></td>
</tr>
<tr class="odd">
<td><p>TUF</p></td>
<td><p>St Symphorien</p></td>
<td><p>Tours</p></td>
<td><p>法國</p></td>
</tr>
<tr class="even">
<td><p>TUL</p></td>
<td><p>Tulsa International Airport</p></td>
<td><p>Tulsa, 俄克拉何马州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td><p>TUN</p></td>
<td><p>Carthage</p></td>
<td><p>突尼斯</p></td>
<td><p>突尼斯</p></td>
</tr>
<tr class="even">
<td><p>TUO</p></td>
<td><p>Taupo</p></td>
<td><p>Taupo</p></td>
<td><p>紐西蘭</p></td>
</tr>
<tr class="odd">
<td><p>TUP</p></td>
<td><p>Tupelo Regional Airport</p></td>
<td><p>Tupelo, 密西西比州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="even">
<td><p>TUS</p></td>
<td><p>土桑國際機場</p></td>
<td><p>土桑, 亚利桑那州 (Tucson, AZ)</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td><p>TVC</p></td>
<td><p>Cherry Capital Airport</p></td>
<td><p>Traverse City, 密歇根州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="even">
<td><p>TVF</p></td>
<td><p>Thief River Falls Regional Airport</p></td>
<td><p>Thief River Falls, 明尼苏达州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td><p>TWB</p></td>
<td><p>Toowoomba</p></td>
<td><p>Toowoomba, 昆士蘭州</p></td>
<td><p>澳大利亞</p></td>
</tr>
<tr class="even">
<td><p>TWF</p></td>
<td></td>
<td><p>Twin Falls, 爱达荷州</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")

1.  在桃園機場啟用前之代碼為**TPE**