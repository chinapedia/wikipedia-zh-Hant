## V

| IATA代碼                    | ICAO代碼 | 機場名稱                                                    | 城市                                              | 国家和地区                                    |
| ------------------------- | ------ | ------------------------------------------------------- | ----------------------------------------------- | ---------------------------------------- |
|                           |        |                                                         |                                                 |                                          |
| <cite id="VA"></cite>-VA- |        |                                                         |                                                 |                                          |
| VAA                       | EFVA   | [瓦薩機場](../Page/瓦薩機場.md "wikilink")                      | [瓦薩](../Page/瓦薩.md "wikilink")                  | [芬蘭](../Page/芬蘭.md "wikilink")           |
| VAD                       | KVAD   | Moody Air Force Base                                    | Valdosta                                        | [美國](../Page/美國.md "wikilink")           |
| VAF                       | LFLU   | Valence-Chabeuil Airport                                | [瓦朗斯](../Page/瓦朗斯_\(德龍省\).md "wikilink")        | [法國](../Page/法國.md "wikilink")           |
| VAG                       | SBVG   | Major-Brigadeiro Trompowsky Airport                     | [瓦爾任阿](../Page/瓦爾任阿.md "wikilink")              | [巴西](../Page/巴西.md "wikilink")           |
| VAK                       | PAVA   | Chevak Airport                                          | [切瓦克](../Page/切瓦克_\(阿拉斯加州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")           |
| VAM                       | VRMV   | Maamigili Airport                                       | Maamigili                                       | [馬爾地夫](../Page/馬爾地夫.md "wikilink")       |
| VAN                       | LTCI   | Van Ferit Melen Airport                                 | [凡城](../Page/凡城.md "wikilink")                  | [土耳其](../Page/土耳其.md "wikilink")         |
| VAR                       | LBWN   | Varna Airport                                           | [瓦爾納](../Page/瓦爾納.md "wikilink")                | [保加利亞](../Page/保加利亞.md "wikilink")       |
| VAS                       | LTAR   | Sivas Airport                                           | [錫瓦斯](../Page/錫瓦斯.md "wikilink")                | [土耳其](../Page/土耳其.md "wikilink")         |
| VAW                       | ENSS   | Vardø Airport, Svartnes                                 | [瓦爾德](../Page/瓦爾德_\(挪威\).md "wikilink")         | [挪威](../Page/挪威.md "wikilink")           |
| <cite id="VB"></cite>-VB- |        |                                                         |                                                 |                                          |
| VBG                       | KVBG   | [范登堡空軍基地](../Page/范登堡空軍基地.md "wikilink")                | [隆波克](../Page/隆波克_\(加利福尼亞州\).md "wikilink")     | [美國](../Page/美國.md "wikilink")           |
| VBS                       | LIPO   | Brescia Airport Gabriele D'Annunzio                     | [布雷西亞](../Page/布雷西亞.md "wikilink")              | [義大利](../Page/義大利.md "wikilink")         |
| VBY                       | ESSV   | Visby Airport                                           | [維斯比](../Page/維斯比.md "wikilink")                | [瑞典](../Page/瑞典.md "wikilink")           |
| <cite id="VC"></cite>-VC- |        |                                                         |                                                 |                                          |
| VCA                       | VVCT   | [芹苴國際機場](../Page/芹苴國際機場.md "wikilink")                  | [芹苴市](../Page/芹苴市.md "wikilink")                | [越南](../Page/越南.md "wikilink")           |
| VCB                       | KVCB   | Nut Tree Airport                                        | Vacaville                                       | [美國](../Page/美國.md "wikilink")           |
| VCC                       |        | Limbe Airport                                           | [林貝](../Page/林貝_\(喀麥隆\).md "wikilink")          | [喀麥隆](../Page/喀麥隆.md "wikilink")         |
| VCE                       | LIPZ   | [威尼斯－泰塞拉機場](../Page/威尼斯－泰塞拉機場.md "wikilink")            | [威尼斯](../Page/威尼斯.md "wikilink")                | [義大利](../Page/義大利.md "wikilink")         |
| VCL                       |        | [茱萊機場](../Page/茱萊機場.md "wikilink")                      | Chu Lai                                         | [越南](../Page/越南.md "wikilink")           |
| VCP                       | SBKP   | Viracopos–Campinas International Airport                | [坎皮納斯](../Page/坎皮納斯.md "wikilink")              | [巴西](../Page/巴西.md "wikilink")           |
| VCS                       | VVCS   | [崑崙機場](../Page/崑崙機場.md "wikilink")                      | [崑崙島](../Page/崑崙島.md "wikilink")                | [越南](../Page/越南.md "wikilink")           |
| VCT                       | KVCT   | Victoria Regional Airport                               | [維多利亞](../Page/維多利亞_\(德克薩斯\).md "wikilink")     | [美國](../Page/美國.md "wikilink")           |
| VCV                       | KVCV   | Southern California Logistics Airport                   | [維克多維爾](../Page/維克多維爾_\(加利福尼亞州\).md "wikilink") | [美國](../Page/美國.md "wikilink")           |
| <cite id="VD"></cite>-VD- |        |                                                         |                                                 |                                          |
| VDA                       | LLOV   | Ovda Airport                                            | Ovda                                            | [以色列](../Page/以色列.md "wikilink")         |
| VDB                       | ENFG   | Fagernes Airport, Leirin                                | [法格內斯](../Page/法格內斯.md "wikilink")              | [挪威](../Page/挪威.md "wikilink")           |
| VDE                       | GCHI   | [耶罗机场](../Page/耶罗机场.md "wikilink")                      | [耶罗岛](../Page/耶罗岛.md "wikilink")                | [西班牙](../Page/西班牙.md "wikilink")         |
| VDI                       | KVDI   | Vidalia Regional Airport                                | Vidalia                                         | [美國](../Page/美國.md "wikilink")           |
| VDM                       | SAVV   | Gobernador Edgardo Castello Airport                     | [別德馬](../Page/別德馬.md "wikilink")                | [阿根廷](../Page/阿根廷.md "wikilink")         |
| VDP                       | SVVP   | Valle de la Pascua Airport                              | [帕斯夸谷鎮](../Page/帕斯夸谷鎮.md "wikilink")            | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| VDR                       | SAOD   | Villa Dolores Airport                                   | Villa Dolores                                   | [阿根廷](../Page/阿根廷.md "wikilink")         |
| VDS                       | ENVD   | Vadsø Airport                                           | [瓦德瑟](../Page/瓦德瑟.md "wikilink")                | [挪威](../Page/挪威.md "wikilink")           |
| VDZ                       | PAVD   | Valdez Airport                                          | [瓦爾迪茲](../Page/瓦爾迪茲_\(阿拉斯加州\).md "wikilink")    | [美國](../Page/美國.md "wikilink")           |
| <cite id="VE"></cite>-VE- |        |                                                         |                                                 |                                          |
| VEE                       | PAVE   | Venetie Airport                                         | Venetie                                         | [美國](../Page/美國.md "wikilink")           |
| VEL                       | KVEL   | Vernal Regional Airport                                 | [韋納爾](../Page/韋納爾_\(猶他州\).md "wikilink")        | [美國](../Page/美國.md "wikilink")           |
| VER                       | MMVR   | General Heriberto Jara International Airport            | [韋拉克魯斯](../Page/韋拉克魯斯_\(韋拉克魯斯州\).md "wikilink") | [墨西哥](../Page/墨西哥.md "wikilink")         |
| VEY                       | BIVM   | Vestmannaeyjar Airport                                  | [韋斯特曼納群島](../Page/韋斯特曼納群島.md "wikilink")        | [冰島](../Page/冰島.md "wikilink")           |
| VEX                       |        | Tioga Municipal Airport                                 | [泰奧加](../Page/泰奧加_\(北達科他州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")           |
| <cite id="VF"></cite>-VF- |        |                                                         |                                                 |                                          |
| VFA                       | FVFA   | Victoria Falls Airport                                  | Victoria Falls                                  | [辛巴威](../Page/辛巴威.md "wikilink")         |
| <cite id="VG"></cite>-VG- |        |                                                         |                                                 |                                          |
| VGA                       | VBOZ   | Vijayawada Airport                                      | [維傑亞瓦達](../Page/維傑亞瓦達.md "wikilink")            | [印度](../Page/印度.md "wikilink")           |
| VGD                       | ULWW   | Vologda Airport                                         | [沃洛格達](../Page/沃洛格達.md "wikilink")              | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| VGO                       | LEVX   | Vigo Airport                                            | [維戈](../Page/維戈.md "wikilink")                  | [西班牙](../Page/西班牙.md "wikilink")         |
| VGT                       | KVGT   | North Las Vegas Airport                                 | [北拉斯維加斯](../Page/北拉斯維加斯.md "wikilink")          | [美國](../Page/美國.md "wikilink")           |
| VGZ                       | SKVG   | Villa Garzón Airport                                    | [加爾松鎮](../Page/加爾松鎮.md "wikilink")              | [哥倫比亞](../Page/哥倫比亞.md "wikilink")       |
| <cite id="VH"></cite>-VH- |        |                                                         |                                                 |                                          |
| VHM                       | ESNV   | Vilhelmina Airport                                      | Vilhelmina                                      | [瑞典](../Page/瑞典.md "wikilink")           |
| VHN                       | KVHN   | Culberson County Airport                                | [范霍恩](../Page/范霍恩_\(得克萨斯州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")           |
| <cite id="VI"></cite>-VI- |        |                                                         |                                                 |                                          |
| VIA                       | SSVI   | Ângelo Ponzoni Municipal Airport                        | [維代拉](../Page/維代拉.md "wikilink")                | [巴西](../Page/巴西.md "wikilink")           |
| VIC                       | LIPT   | Vicenza “Tommaso Dal Molin” Airport                     | [維琴察](../Page/維琴察.md "wikilink")                | [義大利](../Page/義大利.md "wikilink")         |
| VID                       | LBVD   | Vidin Airport                                           | [維丁](../Page/維丁.md "wikilink")                  | [保加利亞](../Page/保加利亞.md "wikilink")       |
| VIE                       | LOWW   | [維也納國際機場](../Page/維也納國際機場.md "wikilink")                | [維也納](../Page/維也納.md "wikilink")                | [奧地利](../Page/奧地利.md "wikilink")         |
| VIF                       |        | Vieste Airport                                          | [維耶斯泰](../Page/維耶斯泰.md "wikilink")              | [義大利](../Page/義大利.md "wikilink")         |
| VIG                       | SVVG   | Juan Pablo Pérez Alfonso Airport                        | Alberto Adriani Municipality                    | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| VIH                       | KVIH   | Rolla National Airport                                  | [羅拉](../Page/羅拉.md "wikilink")                  | [美國](../Page/美國.md "wikilink")           |
| VII                       | VVVH   | [榮市國際機場](../Page/榮市國際機場.md "wikilink")                  | [榮市](../Page/榮市.md "wikilink")                  | [越南](../Page/越南.md "wikilink")           |
| VIJ                       | TUPW   | Virgin Gorda Airport                                    | [維爾京戈爾達島](../Page/維爾京戈爾達島.md "wikilink")        | [英屬維京群島](../Page/英屬維京群島.md "wikilink")   |
| VIL                       | GMMH   | Dakhla Airport                                          | [達克拉](../Page/達克拉.md "wikilink")                | [西撒哈拉](../Page/西撒哈拉.md "wikilink")       |
| VIN                       | UKWW   | Havryshivka International Airport                       | [文尼察](../Page/文尼察.md "wikilink")                | [烏克蘭](../Page/烏克蘭.md "wikilink")         |
| VIQ                       | WPVQ   | Viqueque Airport                                        | [維克克](../Page/維克克.md "wikilink")                | [東帝汶](../Page/東帝汶.md "wikilink")         |
| VIS                       | KVIS   | Visalia Municipal Airport                               | [維塞利亞](../Page/維塞利亞_\(加利福尼亞州\).md "wikilink")   | [美國](../Page/美國.md "wikilink")           |
| VIT                       | LEVT   | Vitoria Airport                                         | [維多利亞](../Page/維多利亞_\(西班牙\).md "wikilink")      | [西班牙](../Page/西班牙.md "wikilink")         |
| VIV                       |        | Vivigani Airfield                                       | Vivigani                                        | [巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink") |
| VIX                       | SBVT   | Eurico de Aguiar Salles Airport                         | [維多利亞](../Page/維多利亞_\(巴西\).md "wikilink")       | [巴西](../Page/巴西.md "wikilink")           |
| <cite id="VJ"></cite>-VJ- |        |                                                         |                                                 |                                          |
| VJB                       | FQXA   | Xai Xai Airport                                         | [賽賽](../Page/賽賽.md "wikilink")                  | [莫三比克](../Page/莫三比克.md "wikilink")       |
| VJI                       | KVJI   | Virginia Highlands Airport                              | [阿賓登](../Page/阿宾登_\(弗吉尼亚州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")           |
| <cite id="VK"></cite>-VK- |        |                                                         |                                                 |                                          |
| VKG                       | VVRG   | [迪石機場](../Page/迪石機場.md "wikilink")                      | [迪石市](../Page/迪石市.md "wikilink")                | [越南](../Page/越南.md "wikilink")           |
| VKO                       | UUWW   | [伏努科沃國際機場](../Page/伏努科沃國際機場.md "wikilink")              | [莫斯科](../Page/莫斯科.md "wikilink")                | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| VKS                       | KVKS   | Vicksburg Municipal Airport                             | [維克斯堡](../Page/維克斯堡_\(密西西比州\).md "wikilink")    | [美國](../Page/美國.md "wikilink")           |
| VKT                       | UUYW   | Vorkuta Airport                                         | [沃爾庫塔](../Page/沃爾庫塔.md "wikilink")              | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| <cite id="VL"></cite>-VL- |        |                                                         |                                                 |                                          |
| VLA                       | KVLA   | Vandalia Municipal Airport                              | Vandalia                                        | [美國](../Page/美國.md "wikilink")           |
| VLC                       | LEVC   | Valencia Airport                                        | [瓦倫西亞](../Page/瓦倫西亞.md "wikilink")              | [西班牙](../Page/西班牙.md "wikilink")         |
| VLD                       | KVLD   | Valdosta Regional Airport                               | Valdosta                                        | [美國](../Page/美國.md "wikilink")           |
| VLE                       |        | Valle Airport                                           | Valle                                           | [美國](../Page/美國.md "wikilink")           |
| VLG                       | SAZV   | Villa Gesell Airport                                    | Villa Gesell                                    | [阿根廷](../Page/阿根廷.md "wikilink")         |
| VLI                       | NVVV   | [維拉港國際機場](../Page/維拉港國際機場.md "wikilink")                | [維拉港](../Page/維拉港.md "wikilink")                | [萬那杜](../Page/萬那杜.md "wikilink")         |
| VLL                       | LEVD   | Valladolid Airport                                      | [瓦拉多利德](../Page/瓦拉多利德.md "wikilink")            | [西班牙](../Page/西班牙.md "wikilink")         |
| VLN                       | SVVA   | Arturo Michelena International Airport                  | [瓦倫西亞](../Page/瓦倫西亞_\(委內瑞拉\).md "wikilink")     | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| VLU                       | ULOL   | Velikiye Luki Airport                                   | [大盧基](../Page/大盧基.md "wikilink")                | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| VLV                       | SVVL   | Dr. Antonio Nicolás Briceño Airport                     | [巴萊拉](../Page/巴萊拉.md "wikilink")                | [委內瑞拉](../Page/委內瑞拉.md "wikilink")       |
| <cite id="VM"></cite>-VM- |        |                                                         |                                                 |                                          |
| VME                       | SAOR   | Pringles Airport                                        | [梅塞德斯鎮](../Page/梅塞德斯鎮.md "wikilink")            | [阿根廷](../Page/阿根廷.md "wikilink")         |
| VMU                       |        | Baimuru Airport                                         | Baimuru                                         | [巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink") |
| <cite id="VN"></cite>-VN- |        |                                                         |                                                 |                                          |
| VNC                       | KVNC   | Venice Municipal Airport                                | [威尼斯](../Page/威尼斯_\(佛羅里達州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")           |
| VNE                       | LFRV   | Vannes-Meucon Airport                                   | [瓦訥](../Page/瓦訥.md "wikilink")                  | [法國](../Page/法國.md "wikilink")           |
| VNO                       | EYVI   | [維爾紐斯國際機場](../Page/維爾紐斯國際機場.md "wikilink")              | [維爾紐斯](../Page/維爾紐斯.md "wikilink")              | [立陶宛](../Page/立陶宛.md "wikilink")         |
| VNS                       | VIBN   | Lal Bahadur Shastri Airport                             | [瓦拉納西](../Page/瓦拉納西.md "wikilink")              | [印度](../Page/印度.md "wikilink")           |
| VNT                       | EVVA   | [文茨皮爾斯機場](../Page/文茨皮爾斯機場.md "wikilink")                | [文茨皮爾斯](../Page/文茨皮爾斯.md "wikilink")            | [拉脫維亞](../Page/拉脫維亞.md "wikilink")       |
| VNX                       | FQVL   | Vilankulo Airport                                       | [維蘭庫洛](../Page/維蘭庫洛.md "wikilink")              | [莫三比克](../Page/莫三比克.md "wikilink")       |
| VNY                       | KVNY   | Van Nuys Airport                                        | Van Nuys                                        | [美國](../Page/美國.md "wikilink")           |
| <cite id="VO"></cite>-VO- |        |                                                         |                                                 |                                          |
| VOD                       | LKVO   | Vodochody Airport                                       | Vodochody                                       | [捷克](../Page/捷克.md "wikilink")           |
| VOG                       | URWW   | Volgograd International Airport                         | [伏爾加格勒](../Page/伏爾加格勒.md "wikilink")            | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| VOI                       | GLVA   | Voinjama Airport                                        | [沃因賈馬](../Page/沃因賈馬.md "wikilink")              | [賴比瑞亞](../Page/賴比瑞亞.md "wikilink")       |
| VOK                       | KVOK   | Volk Field Air National Guard Base                      | Camp Douglas                                    | [美國](../Page/美國.md "wikilink")           |
| VOL                       | LGBL   | Nea Anchialos National Airport                          | [佛洛斯](../Page/佛洛斯.md "wikilink")                | [希臘](../Page/希臘.md "wikilink")           |
| VOZ                       | UUOO   | Voronezh International Airport                          | [沃羅涅日](../Page/沃羅涅日.md "wikilink")              | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| <cite id="VP"></cite>-VP- |        |                                                         |                                                 |                                          |
| VPN                       | BIVO   | Vopnafjörður Airport                                    | [沃普納菲厄澤](../Page/沃普納菲厄澤.md "wikilink")          | [冰島](../Page/冰島.md "wikilink")           |
| VPS                       | KVPS   | Northwest Florida Regional Airport/Eglin Air Force Base | [瓦爾帕萊索](../Page/瓦爾帕萊索_\(佛羅里達州\).md "wikilink")  | [美國](../Page/美國.md "wikilink")           |
| VPY                       | FQCH   | Chimoio Airport                                         | [希莫尤](../Page/希莫尤.md "wikilink")                | [莫三比克](../Page/莫三比克.md "wikilink")       |
| VPZ                       | KVPZ   | Porter County Regional Airport                          | [瓦爾帕萊索](../Page/瓦爾帕萊索_\(印第安納州\).md "wikilink")  | [美國](../Page/美國.md "wikilink")           |
| <cite id="VQ"></cite>-VQ- |        |                                                         |                                                 |                                          |
| VQS                       | TJVQ   | Antonio Rivera Rodríguez Airport                        | [別克斯島](../Page/別克斯島.md "wikilink")              | [美國](../Page/美國.md "wikilink")           |
| <cite id="VR"></cite>-VR- |        |                                                         |                                                 |                                          |
| VRA                       | MUVR   | Juan Gualberto Gómez Airport                            | [巴拉德羅](../Page/巴拉德羅.md "wikilink")              | [古巴](../Page/古巴.md "wikilink")           |
| VRB                       | KVRB   | Vero Beach Municipal Airport                            | [維羅海灘](../Page/維羅海灘_\(佛羅里達州\).md "wikilink")    | [美國](../Page/美國.md "wikilink")           |
| VRC                       | RPUV   | Virac Airport                                           | Virac                                           | [菲律賓](../Page/菲律賓.md "wikilink")         |
| VRI                       |        | Varandey Airport                                        | [涅涅茨自治區](../Page/涅涅茨自治區.md "wikilink")          | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| VRK                       | EFVR   | [瓦爾考斯機場](../Page/瓦爾考斯機場.md "wikilink")                  | [約羅伊寧](../Page/約羅伊寧.md "wikilink")              | [芬蘭](../Page/芬蘭.md "wikilink")           |
| VRL                       | LPVR   | Vila Real Airport                                       | [雷阿爾城](../Page/雷阿爾城_\(葡萄牙\).md "wikilink")      | [葡萄牙](../Page/葡萄牙.md "wikilink")         |
| VRN                       | LIPX   | Verona Villafranca Airport                              | [維洛那](../Page/維洛那.md "wikilink")                | [義大利](../Page/義大利.md "wikilink")         |
| VRS                       |        | Roy Otten Memorial Airfield                             | Versailles                                      | [美國](../Page/美國.md "wikilink")           |
| VRY                       | ENVR   | Værøy Heliport                                          | [韋島](../Page/韋島_\(挪威\).md "wikilink")           | [挪威](../Page/挪威.md "wikilink")           |
| <cite id="VS"></cite>-VS- |        |                                                         |                                                 |                                          |
| VSA                       | MMVA   | Carlos Rovirosa Pérez International Airport             | [比亞埃爾莫薩](../Page/比亞埃爾莫薩.md "wikilink")          | [墨西哥](../Page/墨西哥.md "wikilink")         |
| VSF                       | KVSF   | Hartness State Airport                                  | [斯普林菲爾德](../Page/斯普林菲爾德_\(佛蒙特州\).md "wikilink") | [美國](../Page/美國.md "wikilink")           |
| VSG                       | UKCW   | Luhansk International Airport                           | [盧甘斯克](../Page/盧甘斯克.md "wikilink")              | [烏克蘭](../Page/烏克蘭.md "wikilink")         |
| VST                       | ESOW   | [斯德哥爾摩－韋斯特羅斯機場](../Page/斯德哥爾摩－韋斯特羅斯機場.md "wikilink")    | [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")            | [瑞典](../Page/瑞典.md "wikilink")           |
| <cite id="VT"></cite>-VT- |        |                                                         |                                                 |                                          |
| VTA                       |        | Victoria Airport                                        | Victoria                                        | [宏都拉斯](../Page/宏都拉斯.md "wikilink")       |
| VTE                       | VLVT   | [瓦岱國際機場](../Page/瓦岱國際機場.md "wikilink")                  | [永珍](../Page/永珍.md "wikilink")                  | [寮國](../Page/寮國.md "wikilink")           |
| VTG                       | VVVT   | Vung Tau Airport                                        | [頭頓市](../Page/頭頓市.md "wikilink")                | [越南](../Page/越南.md "wikilink")           |
| VTN                       | KVTN   | Miller Field                                            | Valentine                                       | [美國](../Page/美國.md "wikilink")           |
| VTZ                       | VEVZ   | Visakhapatnam Airport                                   | [維沙卡帕特南](../Page/維沙卡帕特南.md "wikilink")          | [印度](../Page/印度.md "wikilink")           |
| <cite id="VU"></cite>-VU- |        |                                                         |                                                 |                                          |
| VUP                       | SKVP   | Alfonso López Pumarejo Airport                          | [巴耶杜帕爾](../Page/巴耶杜帕爾.md "wikilink")            | [哥倫比亞](../Page/哥倫比亞.md "wikilink")       |
| VUS                       |        | Veliky Ustyug Airport                                   | [大烏斯秋格](../Page/大烏斯秋格.md "wikilink")            | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| <cite id="VV"></cite>-VV- |        |                                                         |                                                 |                                          |
| VVI                       | SLVR   | Viru Viru International Airport                         | [聖克魯斯](../Page/聖克魯斯_\(玻利維亞\).md "wikilink")     | [玻利維亞](../Page/玻利維亞.md "wikilink")       |
| VVO                       | UHWW   | [海參崴國際機場](../Page/海參崴國際機場.md "wikilink")                | [海參崴](../Page/海參崴.md "wikilink")                | [俄羅斯](../Page/俄羅斯.md "wikilink")         |
| <cite id="VX"></cite>-VX- |        |                                                         |                                                 |                                          |
| VXC                       | FQLC   | Lichinga Airport                                        | [利欣加](../Page/利欣加.md "wikilink")                | [莫三比克](../Page/莫三比克.md "wikilink")       |
| VXE                       | GVSV   | São Pedro Airport                                       | [聖維森特島](../Page/聖維森特島.md "wikilink")            | [維德角](../Page/維德角.md "wikilink")         |
| VXO                       | ESMX   | Växjö Småland Airport                                   | [韋克舍](../Page/韋克舍.md "wikilink")                | [瑞典](../Page/瑞典.md "wikilink")           |
| <cite id="VY"></cite>-VY- |        |                                                         |                                                 |                                          |
| VYD                       | FAVY   | Vryheid Airport                                         | [弗萊海德](../Page/弗萊海德.md "wikilink")              | [南非](../Page/南非.md "wikilink")           |
| VYS                       | KVYS   | Illinois Valley Regional Airport                        | Peru                                            | [美國](../Page/美國.md "wikilink")           |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")