**图们市**（）是[吉林省](../Page/吉林省.md "wikilink")[延边朝鲜族自治州的](../Page/延边朝鲜族自治州.md "wikilink")[县级市](../Page/县级市.md "wikilink")。面积1,142.3平方公里、人口13.6万人，其中[朝鮮族](../Page/朝鮮族.md "wikilink")7.8万人，佔57%。隔[图们江望](../Page/图们江.md "wikilink")[朝鮮](../Page/朝鮮.md "wikilink")。邮政编码133100。

## 地理

位于吉林省南边，南与[朝鮮民主主義人民共和国](../Page/朝鮮民主主義人民共和国.md "wikilink")[咸镜北道](../Page/咸镜北道.md "wikilink")[稳城郡接壤](../Page/稳城郡.md "wikilink")。
東接[珲春市](../Page/珲春.md "wikilink")、西接[延吉市](../Page/延吉.md "wikilink")、北接[汪清县](../Page/汪清.md "wikilink")。

## 历史

  - 1913年　设延吉縣，图们只是一个小村。
  - 1934年　[满洲国](../Page/满洲国.md "wikilink")[间岛省延吉县图们市成立](../Page/间岛.md "wikilink")。
  - 1936年　降格为图们街。
  - 1965年4月，把图们镇和石岘镇各从延吉县（今龙井市）和汪清县划出成立图们市，
  - 1965年5月1日，定称图们市。
  - 1968年7月16日，图们市革命委员会成立。
  - 1969年7月，将延吉县的月晴公社和汪清县的新农公社划归图们市。
  - 1980年7月23日，改图们市革委会为图们市人民政府。
  - 1983年8月，将图们市所属各人民公社，改为乡政府。

## 图们烧烤

图们烧烤主要指烤羊肉串、韩式烤牛肉。其中烤羊肉串比较有特点。烤肉搭配沾料、酱料、咸菜、冷面。非常有特色。而且烤羊肉串都免费供应蒜，烤蒜也很好吃。在图们大街设有羊肉串一条街，以羊肉串店为主。

## 行政区划

下辖3个街道、4个镇：

  - 街道：向上街道（향상가도）、新华街道（신화가도）、月宫街道（월궁가도）
  - 镇：月晴镇（월청진）、石峴镇（석현진）、长安镇（장안진）、凉水镇（량수진）

## 外部連結

  - [图们市](http://www.tumen.gov.cn/)

[图们市](../Category/图们市.md "wikilink") [市](../Category/延边市县.md "wikilink")
[延边](../Category/吉林县级市.md "wikilink")
[Category:中华人民共和国口岸](../Category/中华人民共和国口岸.md "wikilink")