**圖林根自由邦**（）是[德国](../Page/德国.md "wikilink")[十六个联邦州之一](../Page/联邦州_\(德国\).md "wikilink")，面积16,200平方公里，在联邦中列第十一位；人口245万，列第十二位。首府为[埃尔福特](../Page/埃尔福特.md "wikilink")。图林根绿色植被覆盖良好，加之位于德国中部，被称作“德国的绿色心脏”。

## 地理

图林根的边界由北顺时针数起包括[下萨克森](../Page/下萨克森.md "wikilink")、[萨克森-安尔哈特](../Page/萨克森-安哈尔特.md "wikilink")、[萨克森](../Page/萨克森.md "wikilink")、[巴伐利亚和](../Page/巴伐利亚.md "wikilink")[黑森](../Page/黑森.md "wikilink")。境內主要城市包括[埃尔福特](../Page/埃尔福特.md "wikilink")（20万人）、[格拉](../Page/格拉.md "wikilink")（11万人）、[耶拿](../Page/耶拿.md "wikilink")（10.3万人）、[魏瑪](../Page/魏玛.md "wikilink")（6.4万）、[哥达](../Page/哥达.md "wikilink")（5万人）、[苏尔](../Page/苏尔_\(德国\).md "wikilink")（4.67万人）、[诺德豪森](../Page/诺德豪森.md "wikilink")（4.5万人）和[艾森纳赫](../Page/艾森纳赫.md "wikilink")（4.42万人）。

图林根境內最著名的地貌是位于南部的[图林根森林](../Page/图林根森林.md "wikilink")（Thüringer
Wald），为一个山脉。西北部包括哈茨山脉的一小部分。东部普遍是平原地带。[萨勒河由南到北流过该片平原](../Page/萨勒河.md "wikilink")。

图林根境內，劃分成17个行政区（*Landkreise*）: [Map of Thuringia showing the boundaries
of the
districts](https://zh.wikipedia.org/wiki/File:Thüringen_Kreise_\(nummeriert\).svg "fig:Map of Thuringia showing the boundaries of the districts")

<table style="width:99%;">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ol>
<li><a href="../Page/阿尔滕堡县.md" title="wikilink">阿尔滕堡县</a></li>
<li><a href="../Page/艾希斯费尔德县.md" title="wikilink">艾希斯费尔德县</a></li>
<li><a href="../Page/哥达县.md" title="wikilink">哥达县</a></li>
<li><a href="../Page/格赖茨县.md" title="wikilink">格赖茨县</a></li>
<li><a href="../Page/希尔德堡豪森县.md" title="wikilink">希尔德堡豪森县</a></li>
<li><a href="../Page/伊尔姆县.md" title="wikilink">伊尔姆县</a></li>
</ol></td>
<td><ol start="7">
<li><a href="../Page/基夫霍伊瑟县.md" title="wikilink">基夫霍伊瑟县</a></li>
<li><a href="../Page/诺德豪森县.md" title="wikilink">诺德豪森县</a></li>
<li><a href="../Page/萨勒-霍尔茨兰县.md" title="wikilink">萨勒-霍尔茨兰县</a></li>
<li><a href="../Page/萨勒-奥拉县.md" title="wikilink">萨勒-奥拉县</a></li>
<li><a href="../Page/萨尔费尔德-鲁多尔施塔特县.md" title="wikilink">萨尔费尔德-鲁多尔施塔特县</a></li>
<li><a href="../Page/施马尔卡尔登-迈宁根县.md" title="wikilink">施马尔卡尔登-迈宁根县</a></li>
</ol></td>
<td><ol start="13">
<li><a href="../Page/瑟默达县.md" title="wikilink">瑟默达县</a></li>
<li><a href="../Page/松讷贝格县.md" title="wikilink">松讷贝格县</a></li>
<li><a href="../Page/翁斯特鲁特-海尼希县.md" title="wikilink">翁斯特鲁特-海尼希县</a></li>
<li><a href="../Page/瓦尔特堡县.md" title="wikilink">瓦尔特堡县</a></li>
<li><a href="../Page/魏玛县.md" title="wikilink">魏瑪縣</a></li>
</ol></td>
</tr>
</tbody>
</table>

另設有六个不属于任何行政区的独立市:

1.  [艾森纳赫](../Page/艾森纳赫.md "wikilink")（EA）
2.  [埃尔福特](../Page/埃尔福特.md "wikilink")（EF）
3.  [格拉](../Page/格拉.md "wikilink")（G）
4.  [耶拿](../Page/耶拿.md "wikilink")（J）
5.  [苏尔](../Page/苏尔_\(德国\).md "wikilink")（SHL）
6.  [魏瑪](../Page/魏玛.md "wikilink")（WE）

## 历史

[Luftbild_Erfurter_Rathaus.jpg](https://zh.wikipedia.org/wiki/File:Luftbild_Erfurter_Rathaus.jpg "fig:Luftbild_Erfurter_Rathaus.jpg")\]\]
[Stadtansicht_Weimar_mit_Schlosss_&_Herderkirche.jpg](https://zh.wikipedia.org/wiki/File:Stadtansicht_Weimar_mit_Schlosss_&_Herderkirche.jpg "fig:Stadtansicht_Weimar_mit_Schlosss_&_Herderkirche.jpg")\]\]
图林根是取名于公元三世纪在此定居的图林根人，图林根于公元六世纪受[法兰克人统治](../Page/法兰克人.md "wikilink")，成为日后的[神圣罗马帝国的一部分](../Page/神圣罗马帝国.md "wikilink")。

1247年，统治圖林根的伯爵绝嗣，隨即爆發了图林根王位继承战争（1247年-1264年）。戰後，图林根的西半部独立成为了[黑森](../Page/黑森.md "wikilink")，永远未能重新成为图林根的一部分；剩余的大部分图林根由附近[迈森藩国的](../Page/迈森藩国.md "wikilink")[维丁王朝统治](../Page/维丁王朝.md "wikilink")，是以后[萨克森公国及](../Page/萨克森公国.md "wikilink")[萨克森王国的核心](../Page/萨克森王国.md "wikilink")。维丁王朝于1485年分裂，图林根由該王朝的[恩斯特系继承](../Page/恩斯特系.md "wikilink")；日後又依据萨克森传统，在男性继承人中划分继承权（即薩利克繼承法），将土地细分为一系列邦国，史稱**[维丁家族恩斯廷系诸邦国](../Page/维丁家族恩斯廷系诸邦国.md "wikilink")**。這些恩斯廷系邦國，包括[薩克森-魏玛](../Page/萨克森-魏玛.md "wikilink")、[萨克森-艾森纳赫](../Page/萨克森-艾森纳赫.md "wikilink")、[萨克森-耶拿](../Page/萨克森-耶拿.md "wikilink")、[萨克森-迈宁根](../Page/萨克森-迈宁根.md "wikilink")、[萨克森-阿尔滕堡](../Page/萨克森-阿尔滕堡.md "wikilink")、[萨克森-科堡和](../Page/萨克森-科堡.md "wikilink")[萨克森-哥达](../Page/萨克森-哥达.md "wikilink")，「图林根」成为了一个地理概念。

在1806年由拿破仑主導的[莱茵联邦里](../Page/莱茵联邦.md "wikilink")，產生了一系列的领土变更，并在日後由歐洲各國參與的[维也纳会议](../Page/维也纳会议.md "wikilink")（1814-15年）中，建立了[德意志邦联而得到确定](../Page/德意志邦联.md "wikilink")。1871年，[普魯士國王威廉一世成立](../Page/普魯士.md "wikilink")[德意志帝国](../Page/德意志帝国.md "wikilink")，此時所谓的图林根邦国，包括[萨克森-魏玛-艾森纳赫](../Page/萨克森-魏玛-艾森纳赫.md "wikilink")、萨克森-迈宁根、萨克森-阿尔滕堡、[萨克森-科堡-哥达等国](../Page/萨克森-科堡-哥达.md "wikilink")。

在一战后1920年所成立的[魏玛共和国](../Page/魏瑪共和國.md "wikilink")，这些微细的图林根邦国合并成立一个邦，称为图林根；其中[萨克森-科堡经公民投票](../Page/萨克森-科堡.md "wikilink")，另行加入[巴伐利亚](../Page/巴伐利亚.md "wikilink")。[威瑪成为图林根的新首府](../Page/魏玛.md "wikilink")。

在1945年7月，图林根為[苏联紅軍占领](../Page/苏联.md "wikilink")；由被解散的[普鲁士邦劃入萨克森省諸城市](../Page/普鲁士.md "wikilink")，例如[埃爾福特](../Page/埃尔福特.md "wikilink")、[米尔豪森](../Page/米尔豪森.md "wikilink")（Mühlhausen）和[诺德豪森等地](../Page/诺德豪森.md "wikilink")。埃爾福特成为图林根的新首府。

1952年，[德意志民主共和国解散統治下各邦](../Page/德意志民主共和国.md "wikilink")，另行组织行政区（*Bezirke*）代替。圖林根境內由新成立的三个行政区埃爾福特、[格拉和](../Page/格拉.md "wikilink")[苏尔取代](../Page/苏尔_\(德国\).md "wikilink")。

1990年，[兩德统一](../Page/兩德统一.md "wikilink")，原屬[東德的图林根三個行政區在轻微修改原有边界后重新恢复成為一个邦](../Page/東德.md "wikilink")。

## 政治

### 选举

在这里，基民盟在选民中获得的支持率较其他大部分州份为低。2014年图林根州大选当中，虽然基民盟保住了议会第一大党的位置，但是仅获得31.2％的选票；左翼党凭借27.4％的选票成为了该州的第二大党；社民党、自民党和绿党分别获得了18.5％、7.6％和6.2％的选票，自民党和绿党均在15年之后，再次获得了州议会的席位。由于社民党与基民盟联合起来的得票率低于50%，在此次选举后社民党选择与左翼党、绿党组成左翼执政联盟。左翼党的[波多·拉梅洛出任新一任州长](../Page/波多·拉梅洛.md "wikilink")，成为1990年两德合并后全德首位来自左翼党的州长。

### 图林根邦總理列表

1.  1920年 - 1921年：Arnold Paulssen（[德国民主党](../Page/德国民主党.md "wikilink")）
2.  1921年 - 1923年：August
    Frölich（[德国社会民主党](../Page/德国社会民主党.md "wikilink")）
3.  1924年 - 1928年：Richard
    Leutheußer（[德国人民党](../Page/德国人民党.md "wikilink")，已解散）
4.  1928年 - 1929年：Karl Riedel（[德国人民党](../Page/德国人民党.md "wikilink")，已解散）
5.  1929年：Arnold Paulssen（[德国民主党](../Page/德国民主党.md "wikilink")）
6.  1930年 - 1932年：Erwin Baum（[德国农民党](../Page/德国农民党.md "wikilink")，已解散）
7.  1932年 - 1933年：Fritz
    Sauckel（[德意志國家社會主義工人黨](../Page/民族社会主义德意志工人党.md "wikilink")，即[纳粹党](../Page/纳粹党.md "wikilink")）
8.  1933年 - 1945年：Willy
    Marschler（[德意志國家社會主義工人黨](../Page/民族社会主义德意志工人党.md "wikilink")，即[纳粹党](../Page/纳粹党.md "wikilink")）
9.  1945年：Hermann Brill（[德国社会民主党](../Page/德国社会民主党.md "wikilink")）
10. 1945年 - 1947年：Rudolf
    Paul（起初无党派，之后加入[德国自由民主党](../Page/德国自由民主党.md "wikilink")）
11. 1947年 - 1952年：Werner
    Eggerath（[德国统一社会党](../Page/德国统一社会党.md "wikilink")）
12. 1990年 - 1992年：Josef
    Duchac（[德国基督教民主联盟](../Page/德国基督教民主联盟.md "wikilink")）
13. 1992年 - 2003年：Bernhard
    Vogel（[德国基督教民主联盟](../Page/德国基督教民主联盟.md "wikilink")）
14. 2003年 - 2009年：Dieter
    Althaus（[德国基督教民主联盟](../Page/德国基督教民主联盟.md "wikilink")）
15. 2009年 - 2014年：Christine
    Lieberknecht（[德国基督教民主联盟](../Page/德国基督教民主联盟.md "wikilink")）
16. 2014年至今：Bodo Ramelow（[德国左翼党](../Page/德国左翼党.md "wikilink")）

## 参考来源

## 外部链接

  - [图林根政府網站](http://www.thueringen.de/)
  - [图林根旅遊信息網站](http://www.thueringen.info/)
  - [图林根旅遊網站](http://www.thueringen-tourismus.de/)
  - [图林根旗](http://www.flaggenkunde.de/deutscheflaggen/de-th.htm)
  - [1](http://www.thueringen.de/imperia/md/content/lzt/chinesisch.pdf)

{{-}}

[图林根州](../Category/图林根州.md "wikilink")
[Category:德国联邦州](../Category/德国联邦州.md "wikilink")
[Category:1990年建立的行政區劃](../Category/1990年建立的行政區劃.md "wikilink")