**图瓦语**或**图佤语**（[西里尔字母](../Page/西里尔字母.md "wikilink")：****，[国际音标](../Page/国际音标.md "wikilink")：）是一种[突厥语族的语言](../Page/突厥语族.md "wikilink")，使用[西里尔字母书写](../Page/西里尔字母.md "wikilink")。主要使用者是[西伯利亚中南部的](../Page/西伯利亚.md "wikilink")[图瓦共和国的约](../Page/图瓦共和国.md "wikilink")20万居民。[中国](../Page/中国.md "wikilink")[新疆维吾尔族自治区北部地区也生活着数以千记的](../Page/新疆维吾尔族自治区.md "wikilink")[图瓦人](../Page/图瓦人.md "wikilink")，他们被官方划归为[蒙古族](../Page/蒙古族.md "wikilink")，但仍然在日常交谈中使用图瓦语，中国图瓦语目前尚无文字，在学术著作中多以宽式[国际音标记写](../Page/国际音标.md "wikilink")。

图瓦语从[蒙古语中借用了大量的词汇](../Page/蒙古语.md "wikilink")，也使用了一部分[俄语的词汇](../Page/俄语.md "wikilink")。

## 字母拼法

图瓦语的书写系统最初是由20世纪30年代一位图瓦僧人设计出来的，而在后来却受到了[斯大林主义者的迫害](../Page/斯大林主义.md "wikilink")。其中采用了大多数以[拉丁文为基础的字母](../Page/拉丁文.md "wikilink")，同时增加了部分特殊字母代表图瓦语中特殊的发音，有一些书籍(其中包括指导人们进行阅读的教科书)是按这样的字母印刷的。但是设计的字母随后被一种[西里尔字母](../Page/西里尔字母.md "wikilink")（也正是现在使用中的字母）所替代，而也逐渐自历史上消失。而在后[苏维埃时期](../Page/苏维埃.md "wikilink")，一些研究图瓦语和其他语言的学者又把目光重新投向了图瓦字母的历史以及建立图瓦语的历史纪录。

<table>
<caption>caption| <small>20世纪30年代圖瓦語字母</small></caption>
<tbody>
<tr class="odd">
<td><p>A a</p></td>
<td></td>
<td><p>C c</p></td>
<td><p>D d</p></td>
<td><p>E e</p></td>
<td><p>F f</p></td>
<td><p>G g</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>H h</p></td>
<td><p>I i</p></td>
<td><p>J j</p></td>
<td><p>K k</p></td>
<td></td>
<td><p>M m</p></td>
<td><p>N n</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>O o</p></td>
<td><p>Ө ө</p></td>
<td><p>P p</p></td>
<td><p>Q q</p></td>
<td><p>R r</p></td>
<td><p>S s</p></td>
<td><p>Ş ş</p></td>
<td><p>T t</p></td>
</tr>
<tr class="even">
<td><p>U u</p></td>
<td><p>V v</p></td>
<td><p>W w</p></td>
<td><p>X x</p></td>
<td><p>Y y</p></td>
<td><p>Z z</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

现在使用的图瓦语用一种俄语字母的变体书写。同标准俄语字母比，图瓦语有三个特殊字母： Ңң（拉丁语的
"[ng](../Page/ng.md "wikilink")" 或 "[ŋ](../Page/軟顎鼻音.md "wikilink")"）、
Өө （拉丁语的"[ö](../Page/ö.md "wikilink")"）、 Үү（拉丁语的
"[ü](../Page/ü.md "wikilink")"）。其他字母顺序同俄语字母表相同，其中Ң在 Н之后， Ө 在 О 之后， Ү 在
У之后。

|     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- |
| А а | Б б | В в | Г г | Д д | Е е | Ё ё | Ж ж |
| З з | И и | Й й | К к | Л л | М м | Н н | Ң ң |
| О о | Ө ө | П п | Р р | С с | Т т | У у | Ү ү |
| Ф ф | Х х | Ц ц | Ч ч | Ш ш | Щ щ | Ъ ъ | Ы ы |
| Ь ь | Э э | Ю ю | Я я |     |     |     |     |

caption| <small>現代圖瓦語字母</small>

## 語音系統

### 輔音

|                                       | [唇音](../Page/唇音.md "wikilink") | [齿龈音](../Page/齿龈音.md "wikilink") | [硬顎音](../Page/硬顎音.md "wikilink") | [软腭音](../Page/软腭音.md "wikilink") |
| ------------------------------------- | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- |
| [鼻音](../Page/鼻音_\(辅音\).md "wikilink") | m                              | n                                |                                  | ŋ                                |
| [塞音](../Page/塞音.md "wikilink")        | pʰ                             | p                                | tʰ                               | t                                |
| [擦音](../Page/擦音.md "wikilink")        |                                | s                                | z                                | ʃ                                |
| [R音](../Page/R音.md "wikilink")        |                                | ɾ                                |                                  |                                  |
| [近音](../Page/近音.md "wikilink")        | ʋ                              | l                                | j                                |                                  |

caption| 圖瓦語輔音音素表

### 元音

|                                | 短   | 長 | 低調 |
| ------------------------------ | --- | - | -- |
| 高                              | 低   | 高 | 低  |
| [前](../Page/前元音.md "wikilink") | 非圓唇 | i | e  |
| 圓唇                             | y   | ø | yː |
| [後](../Page/後元音.md "wikilink") | 非圓唇 | ɯ | a  |
| 圓唇                             | u   | o | uː |

caption| 圖瓦語元音音素表

#### 元音和諧

## 語法

## 文学

口头传颂的民间传说是图瓦语的传统。其中包括许多种类，既有精炼的谜语、格言，也有需要几小时才能讲完的[绕口令](../Page/绕口令.md "wikilink")、神话传说、英雄故事、惊奇历险及史诗奇迹。其中一部分伟大的作品，例如"Boktu-Kirish,Bora-Sheelei"
已经出版。然而这种古老的艺术形式如今却岌岌可危：上一代讲故事的人正逐渐变得年老力衰；而在新的一代中，却没有人愿意继续这样的职业。

## 參考資料

## 參考文獻

  - Anderson, Gregory D. S. (2004). *Auxiliary Verb Constructions in
    [Altai-Sayan](../Page/Altai-Sayan.md "wikilink") Turkic*. Wiesbaden:
    Otto Harrassowitz. ISBN 3-447-04636-8

  -
  - Harrison, K. David. (2001). "Topics in the Phonology and Morphology
    of Tuvan," Doctoral Dissertation, Yale University. (OCLC catalog
    \#51541112)

  - Harrison, K. David. (2005). "A Tuvan hero tale, with commentary,
    morphemic analysis and translation". *Journal of the American
    Oriental Society* **125**(1)1–30.

  -
  -
  - Mawkanuli, Talant. 1999. "The phonology and morphology of Jungar
    Tuva," Indiana University PhD dissertation.

  -
  - Nakashima, Yoshiteru (中嶋 善輝 *Nakashima Yoshiteru*). 2008 "Tyva Yapon
    Biche Slovar', トゥヴァ語・日本語 小辞典" [Tokyo University of Foreign
    Studies](../Page/Tokyo_University_of_Foreign_Studies.md "wikilink"),
    <http://www.aa.tufs.ac.jp/project/gengokensyu/08tuvan6.pdf>
    ([Archive](http://www.webcitation.org/6SOg2ztKG))

  - Ölmez, Mehmet; Tuwinischer Wortschatz mit alttürkischen und
    mongolischen Parallelen, Wiesbaden 2007, ISBN 978-3-447-05499-7

  - Rind-Pawloski, Monika. 2014. Text types and
    [evidentiality](../Page/evidentiality.md "wikilink") in Dzungar
    Tuvan. *Turkic Languages* 18.1: 159-188.

  - Sečenbaγatur, Qasgerel, Tuyaγ-a \[Туяa\], Bu. Jirannige, Wu Yingzhe,
    Činggeltei. 2005. *Mongγul kelen-ü nutuγ-un ayalγun-u sinǰilel-ün
    uduridqal* \[A guide to the regional dialects of Mongolian\].
    Kökeqota: ÖMAKQ. ISBN 7-204-07621-4.

  - Takashima, Naoki (高島 尚生 *Takashima Naoki*). 2008 "Kiso Tuba-go bunpō
    基礎トゥヴァ語文法," [Tokyo University of Foreign
    Studies](../Page/Tokyo_University_of_Foreign_Studies.md "wikilink"),
    <http://www.aa.tufs.ac.jp/project/gengokensyu/08tuvan1.pdf>
    ([Archive](http://www.webcitation.org/6SOevUaVj))

  - Takashima, Naoki. 2008 "Tuba-go kaiwa-shū トゥヴァ語会話集," [Tokyo
    University of Foreign
    Studies](../Page/Tokyo_University_of_Foreign_Studies.md "wikilink"),
    <http://www.aa.tufs.ac.jp/project/gengokensyu/08tuvan3.pdf>
    ([Archive](http://www.webcitation.org/6SOfUaT4l))

  - Taube, Erika. (1978). *Tuwinische Volksmärchen*. Berlin:
    Akademie-Verlag. LCCN: 83-853915

  - Taube, Erika. (1994). *Skazki i predaniia altaiskikh tuvintsev*.
    Moskva : Vostochnaia literatura. ISBN 5-02-017236-7

  - Todoriki, Masahiko (等々力 政彦 *Todoriki Masahiko*). 2011 "Possibly the
    oldest Tuvan vocabulary included in *Wu-li-ya-su-tai-zhi lue*, the
    Abridged Copy of the History of Uliastai,
    烏里蘇台志略にみえる，最古の可能性のあるトゥバ語語彙について".
    *Tōyōbunka-Kenkyūjo Kiyō* 東洋文化研究所紀要 **159** 238-220.  [The
    University of Tokyo](../Page/The_University_of_Tokyo.md "wikilink"),
    <http://repository.dl.itc.u-tokyo.ac.jp/dspace/bitstream/2261/43632/1/ioc159007.pdf>
    ([Archive](https://web.archive.org/web/20150715111050/http://repository.dl.itc.u-tokyo.ac.jp/dspace/handle/2261/43632?mode=full))

  - Oelschlägel, Anett C. (2013). *Der Taigageist. Berichte und
    Geschichten von Menschen und Geistern aus Tuwa. Zeitgenössische
    Sagen und andere Folkloretexte / Дух-хозяин тайги –Современные
    предания и другие фольклорные материалы из Тувы / Тайга ээзи –
    Болган таварылгалар болгаш Тывадан чыгдынган аас чогаалының өске-даа
    материалдары.* \[The Taiga Spirit. Reports and Stories about People
    and Spirits from Tuva. Contemporary Legends and other
    Folklore-Texts.\] Marburg: tectum-Verlag. ISBN 978-3-8288-3134-6

## 外部链接

  - [图瓦语的民族语言报告](http://www.ethnologue.com/show_language.asp?code=TUN)
  - [保护图瓦语](http://tuvin.blogbus.com/)
  - [兩星期就會有一種語言消失
    瀕危語言不用必死？](http://culture.ifeng.com/whrd/detail_2012_04/01/13603943_0.shtml)
  - [English-Tuvan, Tuvan-English online talking
    dictionary](http://tuvan.swarthmore.edu/)
  - [Тыва дыл кырында форум (An online forum in and about the Tyvan
    language)](https://web.archive.org/web/20050513084634/http://www.tyvanet.com/modules.php?name=Forums&file=viewforum&f=7)
  - [Tuvan language and folklore
    materials](http://www.swarthmore.edu/SocSci/dharris2/)
  - [TyvaWiki Language
    Articles](../Page/TyvaWiki:Category:Language.md "wikilink")
  - [Tuvan Alphabet](http://www.tarbagan.net/fotj/TuvanLang.htm)
  - [Research among the Tyvans (Tuvans) of South
    Siberia](https://web.archive.org/web/20160921220325/http://plural-world-interpretations.org/index.html)
  - [Contemporary Legends and other Folklore-Texts from
    Tuva](https://web.archive.org/web/20130904092048/http://plural-world-interpretations.org/home/russ-sagen-der-tyva.html)

{{-}}

[Category:突厥語族](../Category/突厥語族.md "wikilink")
[Category:俄罗斯语言](../Category/俄罗斯语言.md "wikilink")