**圆仁**（794年-864年2月24日），日本高[僧](../Page/僧.md "wikilink")，[日本](../Page/日本.md "wikilink")[天台宗三祖](../Page/天台宗.md "wikilink")，谥号**慈觉大师**，入唐八家（[最澄](../Page/最澄.md "wikilink")、[空海](../Page/空海.md "wikilink")、[常曉](../Page/常曉.md "wikilink")、[圆行](../Page/圆行.md "wikilink")、**圆仁**、[惠運](../Page/恵運.md "wikilink")、[圆珍](../Page/圆珍.md "wikilink")、[宗叡](../Page/宗叡.md "wikilink")）中的一位。

## 生平

生于[下野国](../Page/下野国.md "wikilink")[都贺郡](../Page/都贺郡.md "wikilink")。9岁进入[大慈寺修行](../Page/大慈寺.md "wikilink")，15岁进入[比叡山](../Page/比叡山.md "wikilink")[延历寺接受](../Page/延历寺.md "wikilink")[最澄的教导](../Page/最澄.md "wikilink")。

唐文宗开成三年(838年)，以请益僧身份随最后的一批遣唐使前往中国求法，先到威海半島的[赤山法華院唸經](../Page/赤山法華院.md "wikilink")，並計劃到天台山旅行，但朝廷的旅行许可没有批下来，在法华院期间，听说著名的天台宗座主[志远法师在五台山弘扬天台教义](../Page/志远法师.md "wikilink")，于是他决定改变初衷就把修行的场所換到五台山。

唐文宗开成五年(840年)3月，圆仁与其徒惟正、惟晓等人离开法华院，法華院院主[法清法师亲自把他们送到一百三十华里外的文登县](../Page/法清法师.md "wikilink")，依依惜别。结束了五臺山的修行，圆仁前往当时世界最大的城市、汇聚最尖端文化的唐朝首都[长安旅行](../Page/长安.md "wikilink")。

圆仁一行经青州(今山东益都)、淄州(今淄博市)、齐州(今济南市)、德州、唐州、冀州(今河北冀县)、赵州(今赵县)、镇州，于开成五年六月二日抵达五台山，在志远门下研习天台宗教义。两个月后的八月八日，他离开五台山，经并州(今太原市)、汾州(今山西汾阳)、晋州(今平阳)、蒲州(今永济)、同州(今陕西大荔)于九月十九日旅行至长安，奉敕居住在左街崇仁坊的资圣寺，一住近五载。初拜[大兴善寺](../Page/大兴善寺.md "wikilink")[元政](../Page/元政.md "wikilink")[阿闍黎学](../Page/阿闍黎.md "wikilink")[金刚界大法](../Page/金刚界大法.md "wikilink")。

[唐武宗](../Page/唐武宗.md "wikilink")[会昌元年](../Page/会昌.md "wikilink")(841年)，又从[青龙寺](../Page/青龙寺.md "wikilink")[义真习胎藏界法并蒙灌顶](../Page/义真.md "wikilink")，再随[元法寺](../Page/元法寺.md "wikilink")[法全受传仪轨](../Page/法全.md "wikilink")，依[醴泉寺](../Page/醴泉寺.md "wikilink")[宗颖修习止观](../Page/宗颖.md "wikilink")，实现了多年的求法夙愿。

旅行期间，圆仁撰写了《[入唐求法巡礼行记](../Page/入唐求法巡礼行记.md "wikilink")》日记，这是日本历史上最早的正式旅行记，记载了当时[唐朝](../Page/唐朝.md "wikilink")[武宗压制佛教的](../Page/武宗.md "wikilink")“[会昌灭法](../Page/会昌灭法.md "wikilink")”事件，具有很高的史料价值，与中国唐朝玄奘的《[大唐西域记](../Page/大唐西域记.md "wikilink")》和意大利马可波罗的《[东方见闻录](../Page/东方见闻录.md "wikilink")》并称为世界三大旅行记。

會昌三年(843年)農曆八月初，其弟子惟晓圆寂。

常住長安時，正巧碰上[唐武宗](../Page/唐武宗.md "wikilink")[會昌滅佛事件](../Page/會昌滅佛.md "wikilink")(“会昌法难”)，圆仁也深罹其厄。圆仁本人因无唐祠部牒，被“勒令还俗，遽归本国”。他对武宗皇帝异常痛恨，在《入唐求法巡礼行记》中对[唐武宗的毁法大加抨击](../Page/唐武宗.md "wikilink")。

会昌五年(845年)六月五日，圆仁假装还俗，离开长安。

## 徒弟

  - 惟正，(?-?)
  - 惟晓，(?-843年)，會昌三年(843年)農曆八月初圆寂，葬于长安春明门外的镇国寺附近(今西安市东郊皇甫庄一带)。

## 著作

《[入唐求法巡礼行记](../Page/入唐求法巡礼行记.md "wikilink")》

## 參見

  - [慈恩寺](../Page/慈恩寺_\(埼玉市\).md "wikilink")
  - [立石寺](../Page/立石寺.md "wikilink")
  - [入唐求法巡礼行记](../Page/入唐求法巡礼行记.md "wikilink")

## 外部連結

  - [山寺觀光協會](http://www4.dewa.or.jp/yamadera/)
  - 黃清連：〈[圓仁與唐代巡檢](http://buddhism.lib.ntu.edu.tw/FULLTEXT/JR-MISC/mag83544.htm)〉。

|-        [category:794年出生](../Page/category:794年出生.md "wikilink")
[category:864年逝世](../Page/category:864年逝世.md "wikilink")

[Category:平安時代僧人](../Category/平安時代僧人.md "wikilink")
[Category:下野國出身人物](../Category/下野國出身人物.md "wikilink")
[Category:天台座主](../Category/天台座主.md "wikilink")
[Category:遣唐使](../Category/遣唐使.md "wikilink")