《**國史遺編**》（），又題為《**養浩軒鼎輯國史遺編**》（），[越南](../Page/越南.md "wikilink")[阮朝](../Page/阮朝.md "wikilink")[編年體史書](../Page/編年體.md "wikilink")，以[漢文](../Page/漢文.md "wikilink")[文言文寫成](../Page/文言文.md "wikilink")，編輯為[越南官員](../Page/越南.md "wikilink")[潘叔直](../Page/潘叔直.md "wikilink")（1808年-1852年，）。該書的記述，以[阮朝的](../Page/阮朝.md "wikilink")[嘉隆帝](../Page/嘉隆帝.md "wikilink")、[明命帝](../Page/明命帝.md "wikilink")、[紹治帝時期的史事為主](../Page/紹治帝.md "wikilink")，包括詔諭、法令、建制、外交、社會、習俗等多個方面，當中內容有與官方史書《[大南實錄](../Page/大南實錄.md "wikilink")》互有出入之處，因此《國史遺編》在[阮朝史方面具有相當的引證參考的價值](../Page/阮朝.md "wikilink")。

## 潘叔直及編撰背景

### 潘叔直生平

《國史遺編》的编者[潘叔直是](../Page/潘叔直.md "wikilink")[越南](../Page/越南.md "wikilink")[乂安省安城人](../Page/乂安省.md "wikilink")，初名養浩，生於1808年（[嘉隆七年](../Page/嘉隆.md "wikilink")），在他年青時，便已聰悟博覽，甚有文名。1847年（[紹治七年](../Page/紹治.md "wikilink")）晉身[翰林院](../Page/翰林院.md "wikilink")，曾獲欽賜進入[內閣](../Page/內閣.md "wikilink")，授任集賢院侍講，充經筵起居注，應制詩文深得皇帝賞識，1851年（[嗣德四年](../Page/嗣德.md "wikilink")），奉詔前往[北圻蒐集遺文軼書](../Page/東京_\(越南\).md "wikilink")，次年在[清化病卒](../Page/清化省.md "wikilink")。\[1\]

### 編撰背景

學者[陳荊和認為](../Page/陳荊和.md "wikilink")，《國史遺編》當是[潘叔直於](../Page/潘叔直.md "wikilink")1851年（[嗣德四年](../Page/嗣德.md "wikilink")）奉[嗣德帝之命](../Page/嗣德帝.md "wikilink")，到[北圻蒐集遺文軼書後的私人著作](../Page/東京_\(越南\).md "wikilink")。不過，[潘叔直本人曾在朝廷](../Page/潘叔直.md "wikilink")[內閣服務](../Page/內閣.md "wikilink")，曾任集賢院侍講等職，對[阮朝內部的事情及動向](../Page/阮朝.md "wikilink")，應充份了解，因而增加了這本書的可靠性。\[2\]

## 內容

《國史遺編》分為上中下三集，三集都冠以「國朝大南紀」或「大南紀」的名稱。篇幅如下：

  - 上集：包括「參補玉譜帝系」（[阮潢之前十五代祖先的人名和職位](../Page/阮潢.md "wikilink")）、[阮世祖生父簡歷](../Page/阮福映.md "wikilink")、[嘉隆年間大事記](../Page/嘉隆.md "wikilink")，及附錄[阮世祖](../Page/阮福映.md "wikilink")《祭宋后文》
  - 中集：敍述[明命年間史事](../Page/明命.md "wikilink")，文尾附有[嘉隆帝](../Page/嘉隆帝.md "wikilink")、[明命帝兩朝詔諭及碑記](../Page/明命帝.md "wikilink")、《明命政要》目錄，以及當時的詩文著作等等。
  - 下集：敍述[紹治年間的史事](../Page/紹治.md "wikilink")，文尾有[紹治朝](../Page/紹治帝.md "wikilink")[科舉及第名單](../Page/科舉.md "wikilink")、[順化皇宮宮殿名稱](../Page/順化.md "wikilink")、詔令等等。

[潘叔直在書中](../Page/潘叔直.md "wikilink")，又隨處附加「參補」，以補充有關的史實，又以「參補外傳」，來引又他的另一本著作《[陳黎外傳](../Page/陳黎外傳.md "wikilink")》的記事。\[3\]

## 史料價值

學者[陳荊和指出](../Page/陳荊和.md "wikilink")，《國史遺編》的內容，普遍來說它的來源、觀點、解釋或分析，與官方史書《[大南實錄](../Page/大南實錄.md "wikilink")》相同之處固然有之，但由於《國史遺編》具有補逸性質，於是有不少地方跟《[大南實錄](../Page/大南實錄.md "wikilink")》互有出入。因此，《國史遺編》可與《[大南實錄](../Page/大南實錄.md "wikilink")》及其他[阮朝公私史料互為引證](../Page/阮朝.md "wikilink")。\[4\]

## 流傳情況

據[陳荊和所說](../Page/陳荊和.md "wikilink")，《國史遺編》在[法國遠東學院藏有一鈔本](../Page/法國遠東學院.md "wikilink")，封面題為《**養浩軒鼎輯國史遺編**》，缺序文及跋文，是一未定稿本，編者[潘叔直生前尚未予充份整理](../Page/潘叔直.md "wikilink")。1960年代，[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞研究所東南亞研究室曾將之校訂刋行](../Page/新亞研究所.md "wikilink")。

## 注釋

## 參考文獻

  -

<div class="references-small">

</div>

## 參見

  - [阮朝](../Page/阮朝.md "wikilink")
  - [阮福映](../Page/阮福映.md "wikilink")
  - [阮福晈](../Page/阮福晈.md "wikilink")
  - [阮福暶](../Page/阮福暶.md "wikilink")

[Category:編年體](../Category/編年體.md "wikilink")
[Category:阮朝](../Category/阮朝.md "wikilink")
[Category:越南漢文典籍](../Category/越南漢文典籍.md "wikilink")
[Category:越南古代史書](../Category/越南古代史書.md "wikilink")
[Category:阮朝典籍](../Category/阮朝典籍.md "wikilink")
[Category:越南史書](../Category/越南史書.md "wikilink")

1.  《國史遺編》陳荊和《「國史遺編」的編者與內容》，8-9頁。
2.  《國史遺編》陳荊和《「國史遺編」的編者與內容》，16頁。
3.  《國史遺編》陳荊和《「國史遺編」的編者與內容》，10-11頁。
4.  《國史遺編》陳荊和《「國史遺編」的編者與內容》，15頁。