**法國國家憲兵干預組**（，[縮寫](../Page/縮寫.md "wikilink")：**GIGN**）隶属于[法国国家宪兵](../Page/法国国家宪兵.md "wikilink")，為一支[特種部隊](../Page/特種部隊.md "wikilink")，負責[反恐及](../Page/反恐.md "wikilink")[拯救人質等等任務](../Page/拯救人質.md "wikilink")。

## 組織

[GIGN4_Domenjod_160316.jpg](https://zh.wikipedia.org/wiki/File:GIGN4_Domenjod_160316.jpg "fig:GIGN4_Domenjod_160316.jpg")和[HK
MP5衝鋒槍](../Page/HK_MP5衝鋒槍.md "wikilink")\]\]
國家憲兵干預組分為1個行動指揮組織、1個管理組織、4個作戰部隊內有20名作戰成員、一個作戰支援部隊、還有其他包括談判、溝通、破壞、情報、射擊、警犬、特殊裝備部門。特殊裝備部門提供修改及供應高科技的裝備，包括選擇或設計。國家憲兵干預組每年出動約60次。

大多數成員為自願者，除了體力，智力及自律亦相當的重要，訓練大多是壓力相當重而且高落榜率的訓練，以至於僅7至8%的自願者能夠通過訓練。

所有成員需通過[射擊](../Page/射擊.md "wikilink")、遠程射擊、空中路線、[以色列搏擊防身術等訓練](../Page/以色列搏擊防身術.md "wikilink")，因為有時侯他們必須準備徒手去解除嫌疑犯的武裝。此外，国家宪兵干預組成員亦與世界各國特種部隊及反恐隊伍交流。

### 歷屆指揮官

  - 少校（Squadron leader）Christian Prouteau：1973年至1982年
  - 上尉（Captain）Paul Barril：1982年至1983年（代任）
  - 上尉（Captain）Philippe Masselin：1983年至1985年
  - 上尉（Captain）Philippe Legorjus：1985年至1989年
  - 少校（Squadron leader）Lionel Chesneau：1989年至1992年
  - 少校（Squadron leader ）Denis Favier：1992年至1997年
  - 少校（Squadron leader）Eric Gerard：1997年至2002年
  - 中校（Lieutenant Colonel）Frédéric Gallois：2002年至2007年
  - 准將（Brigade General）Denis Favier：2007年至2011年
  - 准將（Brigade General）Thierry Orosco：2011年至2014年
  - 准將（Brigade General）Hubert Bonneau：2014年至2017年
  - 准將（Brigade General）Laurant Phélip:2017年至今

## 歷史

先後經過1971年的Clairvaux監獄騷亂及1972年的[慕尼黑慘案後](../Page/慕尼黑慘案.md "wikilink")，[法國政府開始著手研究處理極端主義之暴力事件的方法](../Page/法國政府.md "wikilink")。1973年，國家憲兵干預組成立，於翌年3月1日起投入服務。

## 任務

[GIGN10_Domenjod_160316.jpg](https://zh.wikipedia.org/wiki/File:GIGN10_Domenjod_160316.jpg "fig:GIGN10_Domenjod_160316.jpg")
國家憲兵干預組自創立以來，執行過逾千宗任務，拯救過救逾500名人質、拘捕過逾千名疑犯，及殺死過15名[恐怖份子](../Page/恐怖份子.md "wikilink")。7名和2名人員分別於訓練及行動中殉職，另外各有一隻[軍犬分別於訓練及行動中殉職](../Page/軍犬.md "wikilink")。

知名任務：

  - 1976年在吉布提地區，拯救了被[科西嘉民族解放阵线佔據公車裡的](../Page/科西嘉民族解放阵线.md "wikilink")30名兒童。
  - 1979年拯救在聖薩爾瓦多的法國大使館遭挾持的外交官（在攻堅前挾持人質的犯人投降）
  - 1979年11月至12月，国家宪兵干預組奪回Grand Mosque Seizure控制權。
  - 1980年在Fesch酒店逮捕了Corsican的突擊部隊。
  - 1988年5月解救Ouvéa cave hostage taking挾持行動中的人質。
  - 執行1992年冬季奧運的保安任務。
  - [法國航空8969號班機劫機事件劫機事件](../Page/法國航空8969號班機劫機事件.md "wikilink")。
  - 1995年在[葛摩逮捕](../Page/葛摩.md "wikilink")[鮑勃·德納爾](../Page/鮑勃·德納爾.md "wikilink")。
  - 在[波斯尼亞逮捕因為](../Page/波斯尼亞.md "wikilink")[戰爭罪行而被起訴的疑犯](../Page/戰爭罪行.md "wikilink")。

### 法國航空劫機事件

[法國航空8969號班機劫機事件為国家宪兵干預組執行過最知名的任務](../Page/法國航空8969號班機劫機事件.md "wikilink")，當時[法國航空8969號班機由](../Page/法國航空8969號班機.md "wikilink")[阿爾及利亞飛往](../Page/阿爾及利亞.md "wikilink")[法國](../Page/法國.md "wikilink")，遭到的恐怖份子挾持。該組織要求摧毀[艾菲爾鐵塔](../Page/艾菲爾鐵塔.md "wikilink")，在與阿爾及利亞政府談判時，已經處決了3名乘客。在飛機在阿國政府允許下飛往法國後，国家宪兵干預組成員進入飛機與恐怖份子駁火，最後將4名GIA劫機者全數射殺。

該事件造成4名劫機者以及3位乘客死亡，9名成員和13名乘客受傷，但也使国家宪兵干預組聲名大噪。此次事件在2010年亦被拍成電影《[突擊](../Page/突擊_\(法國電影\).md "wikilink")》（L'assaut）。

## 裝備

### [手槍](../Page/手槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/馬努漢MR-73左輪手槍.md" title="wikilink">馬努漢MR-73</a></p></td>
<td></td>
<td><p>[1].357口徑，具4吋、5吋和8吋三種版本[2]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史密斯威森M686左輪手槍.md" title="wikilink">史密斯&amp;威森M686</a></p></td>
<td></td>
<td><p>.357口徑[3]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史密斯威森M60左輪手槍.md" title="wikilink">史密斯&amp;威森M60長官型</a></p></td>
<td></td>
<td><p>.38口徑[4]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/M1911手槍.md" title="wikilink">M1911A1</a></p></td>
<td></td>
<td><p>[5]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MAC_1950手槍.md" title="wikilink">MAC 50</a></p></td>
<td></td>
<td><p>已退役</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_P7手槍.md" title="wikilink">HK P7</a></p></td>
<td></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FN_Five-seveN手槍.md" title="wikilink">FN Five-seveN</a></p></td>
<td></td>
<td><p>[7][8]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SIG_P226手槍.md" title="wikilink">SIG Sauer P226</a></p></td>
<td></td>
<td><p>已退役[9]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SIG_P228手槍.md" title="wikilink">SIG Sauer P228</a></p></td>
<td></td>
<td><p>已退役[10]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SIG_Pro手槍.md" title="wikilink">SIG Pro SP 2022</a></p></td>
<td></td>
<td><p>[11]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PAMAS-G1手槍.md" title="wikilink">PAMAS G1</a></p></td>
<td></td>
<td><p>[12]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格洛克17.md" title="wikilink">格洛克17</a></p></td>
<td></td>
<td><p>[13][14]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格洛克18.md" title="wikilink">格洛克18</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格洛克19.md" title="wikilink">格洛克19</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格洛克26.md" title="wikilink">格洛克26</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

### [衝鋒槍](../Page/衝鋒槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註{{#ifexpr:}}</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/烏茲衝鋒槍.md" title="wikilink">烏茲</a></p></td>
<td></td>
<td><p>[15]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MAT-49衝鋒槍.md" title="wikilink">MAT-49</a></p></td>
<td></td>
<td><p>已退役</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK_MP5衝鋒槍.md" title="wikilink">HK MP5</a></p></td>
<td></td>
<td><p>採用型號包括MP5A5、MP5SD和MP5K-PDW等[16][17]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_UMP衝鋒槍.md" title="wikilink">HK UMP9</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FN_P90衝鋒槍.md" title="wikilink">FN P90</a></p></td>
<td></td>
<td><p>[18][19]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_MP7衝鋒槍.md" title="wikilink">HK MP7A1</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

### [步槍](../Page/步槍.md "wikilink")／[卡賓槍](../Page/卡賓槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/MAS-49半自動步槍.md" title="wikilink">MAS 49/56 MSE</a></p></td>
<td></td>
<td><p>已退役[20]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FAMAS突擊步槍.md" title="wikilink">FAMAS F1</a></p></td>
<td></td>
<td><p>[21]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SIG_SG_550突擊步槍.md" title="wikilink">SIG SG 551</a></p></td>
<td></td>
<td><p>[22][23]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SIG_SG_552卡賓槍.md" title="wikilink">SIG SG 552</a></p></td>
<td></td>
<td><p>[24]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SIG_SG_553卡賓槍.md" title="wikilink">SIG SG 553</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SIG_SG_540突擊步槍.md" title="wikilink">SIG SG 543</a></p></td>
<td></td>
<td><p>已退役[25]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK33突擊步槍.md" title="wikilink">HK33</a></p></td>
<td></td>
<td><p>採用型號包括HK33A2及HK33K[26][27]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_G36突擊步槍.md" title="wikilink">HK G36EK</a></p></td>
<td></td>
<td><p>包括衍生型G36C[28]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK416突擊步槍.md" title="wikilink">HK416</a></p></td>
<td></td>
<td><p>[29]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FN_SCAR突擊步槍.md" title="wikilink">FN SCAR</a></p></td>
<td></td>
<td><p>採用型號為SCAR L及SCAR H[30]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/M4卡賓槍.md" title="wikilink">柯爾特M4A1</a></p></td>
<td></td>
<td><p>[31]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/儒格Mini-14半自動步槍.md" title="wikilink">Ruger AC-556</a></p></td>
<td></td>
<td><p>已退役</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK_G3自動步槍.md" title="wikilink">HK G3KA4</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_G3自動步槍.md" title="wikilink">HK G3 TGS/MSG</a></p></td>
<td></td>
<td><p>配備<a href="../Page/HK_MSG90狙擊步槍.md" title="wikilink">HK MSG90樣式</a><a href="../Page/槍托.md" title="wikilink">槍托</a>，通常會被當成<a href="../Page/精確射手步槍.md" title="wikilink">精確射手步槍使用</a>[32][33]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK417自動步槍.md" title="wikilink">HK417</a></p></td>
<td></td>
<td><p>[34]</p></td>
</tr>
</tbody>
</table>

### [狙擊步槍](../Page/狙擊步槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/FR-F1狙擊步槍.md" title="wikilink">FR-F1</a></p></td>
<td></td>
<td><p>[35][36]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FR-F2狙擊步槍.md" title="wikilink">FR-F2</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/精密國際AW狙擊步槍.md" title="wikilink">精密國際AW</a></p></td>
<td></td>
<td><p>採用型號包括AWSM .338和AWS等[37][38]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷明登.md" title="wikilink">雷明登Bravo 51</a></p></td>
<td></td>
<td><p>[39]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沙科蒂卡T3步槍.md" title="wikilink">沙科蒂卡T3</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PGM_338狙擊步槍.md" title="wikilink">PGM 338</a></p></td>
<td></td>
<td><p>[40]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PGM_Hecate_II狙擊步槍.md" title="wikilink">PGM Hecate II</a></p></td>
<td></td>
<td><p>[41][42]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SIG_SG_550突擊步槍.md" title="wikilink">SIG SG 550 SR</a></p></td>
<td></td>
<td><p>[43]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴雷特M95狙擊步槍.md" title="wikilink">巴雷特M95</a></p></td>
<td></td>
<td><p>已被PGM Hecate II所取代</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/TAC-50狙擊步槍.md" title="wikilink">麥克米蘭M88</a></p></td>
<td></td>
<td><p>已被PGM Hecate II所取代[44]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴雷特M82狙擊步槍.md" title="wikilink">巴雷特M82</a></p></td>
<td></td>
<td><p>已被PGM Hecate II所取代[45][46]</p></td>
</tr>
<tr class="even">
<td><p>.22口徑步槍</p></td>
<td><p><br />
</p></td>
<td><p>口徑有<a href="../Page/.22_LR.md" title="wikilink">.22 LR及</a><a href="../Page/.222雷明登.md" title="wikilink">.222雷明登兩種</a>，型號分別為「Unique X-51」、「Gevarm」和「Anschutz」[47]</p></td>
</tr>
</tbody>
</table>

### [霰彈槍](../Page/霰彈槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/雷明登870泵動式霰彈槍.md" title="wikilink">雷明登870</a></p></td>
<td></td>
<td><p>[48][49]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫斯伯格500泵動式霰彈槍.md" title="wikilink">莫斯伯格590</a></p></td>
<td></td>
<td><p>[50]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>[51]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/弗蘭基SPAS-12戰鬥霰彈槍.md" title="wikilink">弗蘭基SPAS-12</a></p></td>
<td></td>
<td><p>[52][53]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伯奈利M3_Super_90霰彈槍.md" title="wikilink">伯奈利M3T Super 90</a></p></td>
<td></td>
<td><p>[54][55]</p></td>
</tr>
</tbody>
</table>

### [機槍](../Page/機槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/FN_Minimi輕機槍.md" title="wikilink">FN Minimi</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

### [榴彈發射器](../Page/榴彈發射器.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/M79榴彈發射器.md" title="wikilink">M79</a></p></td>
<td></td>
<td><p>[56]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK69榴彈發射器.md" title="wikilink">HK69</a></p></td>
<td></td>
<td><p>[57]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HK79附加型榴彈發射器.md" title="wikilink">HK79</a></p></td>
<td></td>
<td><p>通常下掛於<a href="../Page/HK_G3自動步槍.md" title="wikilink">HK G3的</a><a href="../Page/護木.md" title="wikilink">護木之下</a>[58][59]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HK_AG36附加型榴彈發射器.md" title="wikilink">HK AG36</a></p></td>
<td></td>
<td><p>[60]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/B&amp;T_GL-06榴彈發射器.md" title="wikilink">B&amp;T GL-06</a></p></td>
<td></td>
<td><p>[61]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/米爾科姆轉輪連髮式榴彈發射器.md" title="wikilink">Milkor MGL</a></p></td>
<td></td>
<td><p>[62][63]</p></td>
</tr>
</tbody>
</table>

### [電槍](../Page/電槍.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>武器名稱</p></th>
<th><p>原產地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>X26<a href="../Page/泰瑟.md" title="wikilink">Taser</a></p></td>
<td></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

### 其他

  - [防彈盾](../Page/防彈盾.md "wikilink")
  - 防彈頭盔
  - [戰術燈](../Page/戰術燈.md "wikilink")
  - 手電筒
  - 各種[手榴彈](../Page/手榴彈.md "wikilink")（包括：破片手榴彈、[閃光彈及](../Page/閃光彈.md "wikilink")[煙霧彈等](../Page/煙霧彈.md "wikilink")）

## 流行文化

### 遊戲

  - 在《[絕對武力](../Page/絕對武力.md "wikilink")》系列當中皆有收錄國家憲兵干預組於遊戲的反恐部隊中。
  - 在《霹靂先鋒4》（SWAT 4）中，玩家也可選擇穿著國家憲兵干預組的制服。
  - 在《[特種部隊Online](../Page/特種部隊Online.md "wikilink")》中，玩家可選擇世界的反恐部隊當作整個主角部隊，GIGN也收錄其中。
  - 在《[風暴戰區](../Page/風暴戰區.md "wikilink")》中，出現於遊戲中供玩家使用。
  - 在《虹彩六號：封鎖行動》中，一名前国家宪兵干預組人員Louis Loiselle被送至虹彩小隊。
  - 在《[決勝時刻：現代戰爭3](../Page/決勝時刻：現代戰爭3.md "wikilink")》的劇情模式中，受到生化襲擊後倖存的國家憲兵干預組隊員在[巴黎與](../Page/巴黎.md "wikilink")[三角洲特種部隊會師](../Page/三角洲特種部隊.md "wikilink")，並一同執行了搜捕恐怖份子的任務，在生存模式及聯機模式中亦有出現。
  - 在《[戰地風雲3](../Page/戰地風雲3.md "wikilink")》的劇情模式關卡“戰友”當中，國家憲兵干預組被派往[巴黎阻止試圖從恐怖份子手上回收](../Page/巴黎.md "wikilink")[核武器的](../Page/核武器.md "wikilink")[格魯烏特務迪米特里](../Page/格魯烏.md "wikilink")·「帝瑪」·馬雅柯夫斯基一行人所引起的混亂。
  - 在《[虹彩六號：圍攻行動](../Page/虹彩六號：圍攻行動.md "wikilink")》中，GIGN為其中一個登場的反恐單位。

### 電影

  - 2010年上映的[電影](../Page/電影.md "wikilink")《》（L'Assaut）以在[法國航空8969號班機劫機事件中執行營救行動的国家宪兵干預組成員為主題](../Page/法國航空8969號班機劫機事件.md "wikilink")。

## 参考文献

## 外部連結

  - [官方網站](http://www.defense.gouv.fr/sites/gendarmerie/cache_dossiers/gsign./)

  - [非官方網站](http://www.gign.org)

## 參見

  - [法國軍事](../Page/法國軍事.md "wikilink")
  - [國家警察干預組](../Page/國家警察干預組.md "wikilink")
  - [第九國境守備隊](../Page/第九國境守備隊.md "wikilink")
  - [特種空勤團](../Page/特種空勤團.md "wikilink")
  - [特別任務連](../Page/特別任務連.md "wikilink")
  - [海豹部隊](../Page/海豹部隊.md "wikilink")

[F](../Category/法國特種部隊.md "wikilink")
[Category:法國國家憲兵](../Category/法國國家憲兵.md "wikilink")
[Category:反恐單位](../Category/反恐單位.md "wikilink")
[Category:軍事特種部隊](../Category/軍事特種部隊.md "wikilink")

1.  <http://m.youtube.com/watch?v=UeKy7NtUXxw>

2.  <http://www.gign.org/groupe-intervention/?page_id=1158>

3.
4.
5.  <http://www.gign.org/groupe-intervention/?page_id=1197>

6.
7.
8.  <http://img01.militaryblog.jp/usr/gign/GIGN_withsFivehseveN.gif>

9.
10.
11.
12.
13.
14.
15. <http://www.gign.org/groupe-intervention/?page_id=1247>

16.
17.
18.
19.
20.
21. <http://www.gign.org/groupe-intervention/?page_id=1395>

22.
23. <http://img01.militaryblog.jp/usr/gign/GIGN_sg550.jpg>

24.
25.
26.
27. <http://army-story.blogspot.hk/2013/02/gign-pasukan-elit-anti-teror-perancis.html>

28.
29.
30.
31.
32.
33.
34.
35. <http://img01.militaryblog.jp/usr/gign/GIGN_sniper_f1.jpg>

36. <http://www.gign.org/groupe-intervention/?page_id=1408>

37.
38. <http://m.youtube.com/results?q=GIGN&sm=3>

39.
40.
41.
42. <http://img01.militaryblog.jp/usr/gign/GIGN_sniper_HecatesII.jpg>

43.
44.
45.
46. <http://img01.militaryblog.jp/usr/gign/GIGN_sniper_barret_02.jpg>

47. <http://www.gign.org/groupe-intervention/?page_id=1260>

48. <http://www.gign.org/groupe-intervention/?page_id=1377>

49. <http://img01.militaryblog.jp/usr/gign/GIGN_Gassmasked.jpg>

50. <http://img01.militaryblog.jp/usr/gign/GIGN_weldingsM590.jpg>

51.
52.
53. <http://img01.militaryblog.jp/usr/gign/GIGN_weldingsSPAS12_center.jpg>

54.
55.
56.
57.
58.
59.
60. <http://img01.militaryblog.jp/usr/gign/GIGN_withslauncher.jpg>

61. <http://img01.militaryblog.jp/usr/gign/GIGN_PI2G_BkTsLauncher.jpg>

62.
63. <http://img01.militaryblog.jp/usr/gign/GIGNs_2012_03_revolberslauncher_s.jpg>