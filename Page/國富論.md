《**国富论**》（）全名為《**国民财富的性质和原因的研究**》（），[苏格兰](../Page/苏格兰.md "wikilink")[经济学家暨](../Page/经济学家.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")[亚当·斯密的一本经济学专著](../Page/亚当·斯密.md "wikilink")，其首份[中文译本是來自](../Page/中文.md "wikilink")[翻译家](../Page/翻译家.md "wikilink")[嚴復的](../Page/嚴復.md "wikilink")《**原富**》。

## 出版

国富论首版于[启蒙时代的](../Page/启蒙时代.md "wikilink")1776年3月9日。它不仅影响了作家和经济学家，同时也影响了各国政府和组织。

全书包括两卷共5部。在第一部的序言中，亚当·斯密对全书进行了概括描述：他认为国民财富的产生主要取决于两个因素，一是劳动力的技术、技巧和判断力，二是劳动力和总人口的比例；在这两个因素中，第一个因素起决定性作用。里面阐述了所谓市場上“一隻[看不見的手](../Page/看不見的手.md "wikilink")”。看不見的手說明了公私利調和論，透過價格機制的導引，使得追求自利的個體行動能促進社會整體的效率：「屠夫、釀酒者和麵包師傅，並不是因為想到我們的晚餐要吃喝什麼，才去做這些東西；他做這些事時，其實只想到自己的利益。」

## 内容

  - 第1部，共11章，主要内容是分析形成以及改善[劳动力生产能力的原因](../Page/劳动力.md "wikilink")，分析国民[财富分配的原则](../Page/财富.md "wikilink")；
  - 第2部，共5章，主要内容是讨论[资本的性质](../Page/资本.md "wikilink")、积累方式，分析对劳动力数量的需求取决于工作的性质；
  - 第3部，共4章，主要内容是介绍造成当时比较普遍的重视城市工商业，轻视[农业的政策的原因](../Page/农业.md "wikilink")；
  - 第4部，共9章，主要内容是列举和分析不同国家在不同阶段的各种经济理论；
  - 第5部，共3章，主要内容是分析国家收入的使用方式，是为全民还是只为少数人服务，如果为全民服务有多少种开支项目，各有什麽优缺点；为什麽当代政府都有[赤字和](../Page/赤字.md "wikilink")[国债](../Page/国债.md "wikilink")，这些赤字和国债对真实财富的影响等。

## 评价

一般认为这部著作是现代经济学的开山之作，后来的经济学家基本是沿着他的方法分析经济发展规律的。这部著作也奠定了[资本主义自由经济的理论基础](../Page/资本主义.md "wikilink")，第一次提出了“市场经济会由‘[無形之手](../Page/看不见的手.md "wikilink")’自行调节”的理论。后来的经济学家[李嘉图进一步发展了](../Page/大卫·李嘉图.md "wikilink")[自由经济](../Page/经济自由主义.md "wikilink")、[自由竞争的理论](../Page/自由竞争.md "wikilink")；[马克思则从中看出自由经济产生](../Page/卡尔·马克思.md "wikilink")“周期性经济危机”的必然性，提出“用[计划经济理论解决](../Page/计划经济.md "wikilink")”的思路；[凯恩斯则提出政府干预市场经济宏观调节的方法](../Page/约翰·梅纳德·凯恩斯.md "wikilink")。

目前的经济理论仍然处于不断探索、不断完善的过程，尚没有任何一种尽善尽美、可以完全解决经济发展的方法；但《國富論》这本书仍然可以看作是用现代经济学研究方法写作的第一部著作，对经济学研究仍然在起一定的作用；现代经济学研究都是在这部著作的基础上进行的，不论是发展它或反对它。

## 翻譯

[The_Wealth_of_Nations.jpg](https://zh.wikipedia.org/wiki/File:The_Wealth_of_Nations.jpg "fig:The_Wealth_of_Nations.jpg")

  - [嚴復最早譯有](../Page/嚴復.md "wikilink")《[原富](../Page/原富.md "wikilink")》一書，當時還是[文言文體裁](../Page/文言文.md "wikilink")，無[標點符號](../Page/標點符號.md "wikilink")；並附〈譯事例言〉，又仿[太史公筆法撰有](../Page/太史公.md "wikilink")〈斯密亞丹傳〉。光绪二十八年（1902年）十月，本書由[南洋公学译书院出版发行](../Page/南洋公学.md "wikilink")。[梁启超稱讚此書](../Page/梁启超.md "wikilink")：“严氏于中学西学，皆为我国第一流人物。此书复经数年之心力，屡易其稿，然后出世，其精美更何待言！”

## 原书

  - [《国富论》全文在线阅读 (简繁体翻译)](http://www.8bei8.com/book/wenku/guofulun.html)
  - [**The Wealth of
    Nations**](http://metalibri.wikidot.com/title:an-inquiry-into-the-nature-and-causes-of-the-wealth-of-nations:smith-a)
    at [MetaLibri Digital Library](http://metalibri.wikidot.com)
  - [**The Theory of Moral
    Sentiments**](http://metalibri.wikidot.com/title:theory-of-moral-sentiments:smith-a)
    at [MetaLibri Digital Library](http://metalibri.wikidot.com)
  - [《国民财富的性质和原因的研究》](http://www.sinology.cn/book/1/jjyj/aa/ydsm/fgl/index.html)中译，新国学网
  - [《国富论》（中譯）](http://www.hicourt.gov.cn/theory/hislaw_infonew.asp?id=3558)
    来自 [天涯法律网（海南）](http://www.hicourt.gov.cn/)
  - [《国富论》（中譯）](https://web.archive.org/web/20051028022941/http://www.cnread.net/cnread1/jjzp/s/simi/gmcf/)
    来自 [青少年新世纪读书计划指导委员会](http://www.cnread.net/)
  - [《国富论》](http://www.mondopolitico.com/library/wealthofnations/toc.htm)来自
    [Mondo Politico Library](http://www.mondopolitico.com/library/) –
    全文，屏幕适应格式。
  - [《国富论》](https://web.archive.org/web/20040708090656/http://www.gutenberg.net/etext02/wltnt10.txt)来自
    [古登堡计划](../Page/古登堡计划.md "wikilink") – 全文，纯文本格式。

## 延伸閱讀

  - 賴建誠：〈[亞當史密斯（Adam
    Smith）與嚴復：《國富論》與中國](http://ccsdb.ncl.edu.tw/ccs/image/01_007_002_01_16.pdf)〉。
  - 賴建誠：〈[嚴復對《國富論》的理解](http://nthur.lib.nthu.edu.tw/retrieve/111129/JA01_1999_p89.pdf)〉。

[Category:經濟史](../Category/經濟史.md "wikilink")
[Category:经济学理论与学派](../Category/经济学理论与学派.md "wikilink")
[Category:經濟書籍](../Category/經濟書籍.md "wikilink")