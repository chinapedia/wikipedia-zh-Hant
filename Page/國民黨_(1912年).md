**國民黨**由[宋教仁於](../Page/宋教仁.md "wikilink")[民國元年](../Page/民國.md "wikilink")（1912年）主導成立，是[中華民國北京政府時期一個主張](../Page/中華民國北京政府.md "wikilink")[內閣-{制}-的重要](../Page/內閣制.md "wikilink")[政黨](../Page/政黨.md "wikilink")。國民黨係由[中國同盟會和其他黨派合併而成](../Page/中國同盟會.md "wikilink")，與日後[孫中山成立的](../Page/孫中山.md "wikilink")[中華革命黨](../Page/中華革命黨.md "wikilink")，以及由其改組而來的[中國國民黨淵源頗深](../Page/中國國民黨.md "wikilink")。雖然國民黨的理事長[孫文及部分成員後來另立了中華革命黨](../Page/孫文.md "wikilink")，然而是否應將國民黨視為中華革命黨及中國國民黨之前身，仍存在諸多爭議。

## 背景

[中國同盟會原是](../Page/中國同盟會.md "wikilink")[清朝末年由多個革命團體組成的一個秘密結社](../Page/清朝.md "wikilink")，在1912年3月公開化後吸收了很多新會員，仍堅持革命理想。但由於[孫中山于](../Page/孫中山.md "wikilink")1912年3月辭去[中華民國臨時大總統職位](../Page/中華民國臨時大總統.md "wikilink")，[第一次唐紹儀內閣於](../Page/第一次唐紹儀內閣.md "wikilink")6月請辭，使同盟會在中央政府的政治勢力消失。而[袁世凱為把持政局遷都](../Page/袁世凱.md "wikilink")[北京](../Page/北京.md "wikilink")，在北京亦有很多前清的舊官僚，令同盟會受舊勢力包圍；加上同盟會部份保守派和支系社團——[民社與](../Page/民社.md "wikilink")[光復會分裂出去組成了](../Page/光復會.md "wikilink")[共和黨](../Page/共和党_\(中国\).md "wikilink")，使同盟會有江河日下之勢。

同盟會司法部檢事長[宋教仁為了避免同盟會再度分裂](../Page/宋教仁.md "wikilink")，確保同盟會在[北京臨時參議院保持多數席位](../Page/北京臨時參議院.md "wikilink")，亦為了實現他的[內閣制政治理想](../Page/內閣制.md "wikilink")，聯合[統一共和黨](../Page/統一共和黨.md "wikilink")、[國民共進會](../Page/國民共進會.md "wikilink")、[國民公黨](../Page/國民公黨.md "wikilink")、[共和實進會組成了國民黨](../Page/共和實進會.md "wikilink")。其目標是依據《[中華民國臨時約法](../Page/中華民國臨時約法.md "wikilink")》，通過[民主](../Page/民主.md "wikilink")[選舉組成國民黨的內閣](../Page/選舉.md "wikilink")，掌握政治實權。

而跟同盟會等革命派人士之主張向來南轅北轍的原立憲派人士，也聯合[民主黨](../Page/民主黨_\(1912年中华民国\).md "wikilink")、[共和黨](../Page/共和党_\(中国\).md "wikilink")、[統一黨三黨為](../Page/統一黨_\(中国\).md "wikilink")[進步黨](../Page/進步黨_\(中国\).md "wikilink")，在[國會與國民黨爭奪政權](../Page/國會.md "wikilink")。

## 成立

[Huguanghall2.jpg](https://zh.wikipedia.org/wiki/File:Huguanghall2.jpg "fig:Huguanghall2.jpg")\]\]

1912年8月5日至11日，[中國同盟會](../Page/中國同盟會.md "wikilink")、[統一共和黨](../Page/統一共和黨.md "wikilink")、[國民共進會](../Page/國民共進會.md "wikilink")、[國民公黨](../Page/國民公黨.md "wikilink")、[共和實進會分别派出代表共同磋商合并事宜](../Page/共和實進會.md "wikilink")，各方推举的代表为：\[1\]

  - [中国同盟会](../Page/中国同盟会.md "wikilink")：[宋教仁](../Page/宋教仁.md "wikilink")、[仇亮](../Page/仇亮.md "wikilink")、[刘彦](../Page/刘彦.md "wikilink")、[汤漪](../Page/汤漪.md "wikilink")、[张耀曾](../Page/张耀曾.md "wikilink")、[李肇甫](../Page/李肇甫.md "wikilink")
  - [统一共和党](../Page/统一共和党.md "wikilink")：[马邻翼](../Page/马邻翼.md "wikilink")、[彭允彝](../Page/彭允彝.md "wikilink")、[王树声](../Page/王树声_\(吉林议员\).md "wikilink")、[张寿森](../Page/张寿森.md "wikilink")、[谷钟秀](../Page/谷钟秀.md "wikilink")、[殷汝骊](../Page/殷汝骊.md "wikilink")
  - [国民共进会](../Page/国民共进会.md "wikilink")：[王宠惠](../Page/王宠惠.md "wikilink")、[徐谦](../Page/徐谦.md "wikilink")、[陆定](../Page/陆定.md "wikilink")、[沈其昌](../Page/沈其昌.md "wikilink")、[王善荃](../Page/王善荃.md "wikilink")、[蒋邦彦](../Page/蒋邦彦.md "wikilink")、[马振宪](../Page/马振宪.md "wikilink")、[姚憾](../Page/姚憾.md "wikilink")
  - [共和实进会](../Page/共和实进会.md "wikilink")：[董之云](../Page/董之云.md "wikilink")、[许廉](../Page/许廉.md "wikilink")、[夏树仁](../Page/夏树仁.md "wikilink")、[晏起](../Page/晏起.md "wikilink")
  - [国民公党](../Page/国民公党.md "wikilink")：[虞熙正](../Page/虞熙正.md "wikilink")

8月13日，中国同盟会发表將改組为国民党之宣言。

8月14日，[孙中山與](../Page/孙中山.md "wikilink")[黄兴就中国同盟会改组為国民党一事](../Page/黄兴.md "wikilink")，在上海联合致电同盟会各海外支部以征求同意。\[2\]

8月25日，國民黨成立大会在[北京湖广会馆舉行](../Page/北京湖广会馆.md "wikilink")。該黨由[中國同盟會](../Page/中國同盟會.md "wikilink")、[統一共和黨](../Page/統一共和黨.md "wikilink")、[國民共進會](../Page/國民共進會.md "wikilink")、[國民公黨](../Page/國民公黨.md "wikilink")、[共和實進會等五团体合併改組而成](../Page/共和實進會.md "wikilink")。\[3\]

在成立大会上，决定了如下主要人事安排：\[4\]\[5\]\[6\]

  - 理事长：[孙文](../Page/孙文.md "wikilink")（[宋教仁代理](../Page/宋教仁.md "wikilink")）
  - 理事（9人）：[孙文](../Page/孙文.md "wikilink")、[黄兴](../Page/黄兴.md "wikilink")、[宋教仁](../Page/宋教仁.md "wikilink")、[王宠惠](../Page/王宠惠.md "wikilink")、[王人文](../Page/王人文.md "wikilink")、[王芝祥](../Page/王芝祥.md "wikilink")、[张凤翙](../Page/张凤翙.md "wikilink")、[吴景濂](../Page/吴景濂.md "wikilink")、[贡桑诺尔布](../Page/贡桑诺尔布.md "wikilink")。
  - 参议（30人）：[阎锡山](../Page/阎锡山.md "wikilink")、[张继](../Page/张继_\(中华民国\).md "wikilink")、[李烈钧](../Page/李烈钧.md "wikilink")、[胡瑛](../Page/胡瑛.md "wikilink")、[王传炯](../Page/王传炯.md "wikilink")、[温宗尧](../Page/温宗尧.md "wikilink")、[陈锦涛](../Page/陈锦涛.md "wikilink")、[陈陶遗](../Page/陈陶遗.md "wikilink")、[莫永贞](../Page/莫永贞.md "wikilink")、[褚辅成](../Page/褚辅成.md "wikilink")、[松毓](../Page/松毓.md "wikilink")、[杨增新](../Page/杨增新.md "wikilink")、[于右任](../Page/于右任.md "wikilink")、[马君武](../Page/马君武.md "wikilink")、[田桐](../Page/田桐.md "wikilink")、[谭延闿](../Page/谭延闿.md "wikilink")、[张培爵](../Page/张培爵.md "wikilink")、[徐谦](../Page/徐谦.md "wikilink")、[王善荃](../Page/王善荃.md "wikilink")、[姚锡光](../Page/姚锡光.md "wikilink")、[赵炳麟](../Page/赵炳麟.md "wikilink")、[柏文蔚](../Page/柏文蔚.md "wikilink")、[沈秉堃](../Page/沈秉堃.md "wikilink")、[景耀月](../Page/景耀月.md "wikilink")、[虞汝钧](../Page/虞汝钧.md "wikilink")、[张琴](../Page/张琴_\(书画家\).md "wikilink")、[曾昭文](../Page/曾昭文.md "wikilink")、[蒋翊武](../Page/蒋翊武.md "wikilink")、[陈明远](../Page/陈明远.md "wikilink")。
  - 备补参议（10人）：[尹昌衡](../Page/尹昌衡.md "wikilink")、[袁家普](../Page/袁家普.md "wikilink")、[唐绍仪](../Page/唐绍仪.md "wikilink")、[唐文治](../Page/唐文治.md "wikilink")、
    [胡汉民](../Page/胡汉民.md "wikilink")、[王绍祖](../Page/王绍祖.md "wikilink")、[高金钊](../Page/高金钊.md "wikilink")、[许廉](../Page/许廉.md "wikilink")、[夏仁树](../Page/夏仁树.md "wikilink")、[贺国昌](../Page/贺国昌.md "wikilink")
  - 名誉参议（7人）：[溥伦](../Page/溥伦.md "wikilink")、[钮永建](../Page/钮永建.md "wikilink")、[徐绍桢](../Page/徐绍桢.md "wikilink")、[姚雨平](../Page/姚雨平.md "wikilink")、[林述庆](../Page/林述庆.md "wikilink")、[马安良](../Page/马安良.md "wikilink")、[张锡銮](../Page/张锡銮.md "wikilink")

國民黨的人事組織有理事九人、參議三十人、候補參議十人及名譽參議七人。九位理事分別是：[孫中山](../Page/孫中山.md "wikilink")、[黃興](../Page/黃興.md "wikilink")、[宋教仁](../Page/宋教仁.md "wikilink")、[王寵惠](../Page/王寵惠.md "wikilink")、[王人文](../Page/王人文.md "wikilink")、[王芝祥](../Page/王芝祥.md "wikilink")、[吳景濂](../Page/吳景濂.md "wikilink")、[張鳳翽及蒙古人](../Page/張鳳翽.md "wikilink")[貢桑諾爾布](../Page/貢桑諾爾布.md "wikilink")，以孫中山為理事長。由於孫當時為全國鐵路督辦，总部在上海，而黃興又是當時的粵川鐵路督辦，故實際黨務由宋教仁代理。

## 組織與黨綱

国民党本部设于[北京](../Page/北京.md "wikilink")，下设总务部、政事部、文事部、交际部、会计部、政务研究会等机构。

  - 总务部主任干事：[魏宸组](../Page/魏宸组.md "wikilink")、[殷汝骊](../Page/殷汝骊.md "wikilink")
  - 政务部主任干事：[谷钟秀](../Page/谷钟秀.md "wikilink")、[汤漪](../Page/汤漪.md "wikilink")
  - 文事部主任干事：[杨庶堪](../Page/杨庶堪.md "wikilink")
  - 交际部主任干事：[李肇甫](../Page/李肇甫.md "wikilink")
  - 政务研究会主任干事：[张耀曾](../Page/张耀曾.md "wikilink")、[刘彦](../Page/刘彦.md "wikilink")

国民党在中国各省及海外各埠设立支部，在国内各交通口岸设立交通部。

國民黨的黨綱有五條：(一)保持政治統一、(二)發展地方政治、(三)厲行種族同化、(四)採用民生政策、(五)維持國際和平。

## 國會選舉及宋教仁案

1912年底至1913年初的[中華民國](../Page/中華民國.md "wikilink")[國會選舉中](../Page/中华民国初年国会.md "wikilink")，國民黨若不計跨黨派人士，在參議院274個席位中共贏得132個席位，在眾議院596個席位中共贏得269個席位，參眾兩院均為第一大黨，代理事長[宋教仁可望成為國務總理](../Page/宋教仁.md "wikilink")。

在國會召開第一次正式會議之前，[袁世凱發電召宋教仁赴](../Page/袁世凱.md "wikilink")[北京共商國事](../Page/北京.md "wikilink")；宋教仁認為大局已定，遂毅然起行。臨行前，[陳其美曾經囑咐宋小心袁世凱](../Page/陳其美.md "wikilink")，[于右任亦勸宋改行海路](../Page/于右任.md "wikilink")，皆不為宋所接納。1913年3月20日晚上10時，宋教仁於[上海火車站遇刺](../Page/上海火車站.md "wikilink")；兩日後去世，時年32歲。

數日後，正凶[武士英及協凶](../Page/武士英.md "wikilink")[應夔丞落網](../Page/應夔丞.md "wikilink")，並且在應夔丞家中搜出了一封代國務總理[趙秉鈞給他的密電碼一冊](../Page/趙秉鈞.md "wikilink")，「叮囑『以後有電直寄國務院』」的密函一份及多件由內務部秘書[洪述祖指使他行刺宋教仁的電函](../Page/洪述祖.md "wikilink")。故此歷史學家基本上都認為[袁世凱或](../Page/袁世凱.md "wikilink")[趙秉鈞是幕後主謀](../Page/趙秉鈞.md "wikilink")，但亦有部份人認為幕後主謀者實為[孙中山或](../Page/孙中山.md "wikilink")[陈其美等人](../Page/陈其美.md "wikilink")。

## [二次革命及國民黨分裂](../Page/二次革命.md "wikilink")

在「宋案」發生後，國民黨內部彌漫著悲憤情緒，黨內一致認為[袁世凱是幕後主謀](../Page/袁世凱.md "wikilink")，但在追究責任的方式上存在嚴重分歧。[孫中山力主即刻起兵討袁](../Page/孫中山.md "wikilink")，理由係袁世凱為前清重臣，曾對革命黨與清廷首鼠兩端，不可信任又擁兵自重，靠法律討公道是行不通的；不如趁袁世凱的億兩萬銀元軍費（即[善後大借款](../Page/善後大借款.md "wikilink")）尚未到手，國民黨率先發難，或許尚有勝算。但與會者只有[戴季陶一人贊成](../Page/戴季陶.md "wikilink")，其餘黨員則支持[黃興的建議](../Page/黃興.md "wikilink")，通過法律途徑控告袁世凱。理由是國民黨沒有足夠強大的軍力去找袁興師問罪，且如今[民國是現代](../Page/民國.md "wikilink")[共和政體](../Page/共和.md "wikilink")，國民黨作為國會中的多數政黨，解決紛爭應訴諸法律而不是武力，否則將破壞中國好不容易初具雛形的代議民主制度；若袁世凱公然毀法，屆時再起兵亦不遲。

於是國民黨先請[江蘇都督](../Page/江蘇.md "wikilink")[程德全向國會提出成立特別法庭處理](../Page/程德全.md "wikilink")「宋案」，但被國民黨籍的司法總長[許世英駁回](../Page/許世英.md "wikilink")，他認為依法訴訟應由地方法庭開始，不可逾級，成立特別法庭更是干預司法獨立。另一方面孫中山要求國民黨籍的[江西都督](../Page/江西.md "wikilink")[李烈鈞](../Page/李烈鈞.md "wikilink")、[安徽都督](../Page/安徽.md "wikilink")[柏文蔚和](../Page/柏文蔚.md "wikilink")[廣東都督](../Page/廣東.md "wikilink")[胡漢民宣佈獨立](../Page/胡漢民.md "wikilink")，但三都督皆回覆無足夠實力，而孫在[武漢進行的地下活動亦被](../Page/武漢.md "wikilink")[黎元洪搗破](../Page/黎元洪.md "wikilink")。其他黨員如黃興等，則堅持通過法律途徑處理宋案，並無積極軍事佈署。

當袁世凱繞過國會程序，向六國列強商借的[善後大借款入手後](../Page/善後大借款.md "wikilink")，受到國會議員和[湖南都督](../Page/湖南.md "wikilink")[譚延闓](../Page/譚延闓.md "wikilink")、[江西都督](../Page/江西.md "wikilink")[李烈鈞](../Page/李烈鈞.md "wikilink")、[安徽都督](../Page/安徽.md "wikilink")[柏文蔚及](../Page/柏文蔚.md "wikilink")[廣東都督](../Page/廣東.md "wikilink")[胡漢民四位都督的抵制](../Page/胡漢民.md "wikilink")。此時孫中山自知不敵，為了國民黨不被袁世凱剿滅，同意以和平方式解決宋案，但袁堅持撤換國民黨籍都督李烈鈞、柏文蔚及胡漢民。1913年7月，孫中山在[上海召開黨務會議](../Page/上海.md "wikilink")，決定興師討袁，發動「[二次革命](../Page/二次革命.md "wikilink")」。

7月12日，[李烈鈞率先宣佈](../Page/李烈鈞.md "wikilink")[江西獨立](../Page/江西.md "wikilink")，接著[江蘇](../Page/江蘇.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[上海](../Page/上海.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[福建](../Page/福建.md "wikilink")、[湖南及](../Page/湖南.md "wikilink")[重慶亦宣佈獨立](../Page/重慶.md "wikilink")，[黃興被任命為討袁軍總司令](../Page/黃興.md "wikilink")，但不到兩個月就被袁世凱的中央軍和[唐繼堯的滇軍所擊敗](../Page/唐繼堯.md "wikilink")。孫中山、黃興、李烈鈞、柏文蔚、[陳炯明](../Page/陳炯明.md "wikilink")、[陳其美](../Page/陳其美.md "wikilink")、胡漢民等國民黨領袖相繼流亡[日本](../Page/日本.md "wikilink")。

孫中山檢討「二次革命」失敗的原因，認為國民黨在宋案發生後未能立即起兵，失去先機以致袁世凱得到善後大借款後，國民黨再無籌碼與其交涉。又表示根本原因是國民黨不但失去了[同盟會的武裝革命精神](../Page/中国同盟会.md "wikilink")，“散漫無力”，而且有很多所謂“投機份子”、“政客”、“軍閥”、“市井之徒”混入黨內。因此在「二次革命」期間，很多黨員便各自打算，並未服從理事長孫中山的號令（然而孫並不擁有國民黨的絕對領導權，民主政黨的黨員無義務聽從黨魁之命令），例如譚延闓；甚者，大多數國民黨員否定訴諸武力，支持政治解決。

有鉴于此，孫中山決定重新創立一個充滿革命精神、行動一致、服從領袖的[中華革命黨](../Page/中華革命黨.md "wikilink")，繼續討袁。然而對於中華革命黨的組織辦法，尤其是須宣誓服從孫中山一人，以及捺手印為記這類幫會的作法，黃興、李烈鈞、柏文蔚、陳炯明等無法接受，故拒絕加入。他們另組[歐事研究會](../Page/歐事研究會.md "wikilink")，對外仍用「國民黨」之名，在一定程度上繼續反袁。

位於國內北京的國民黨本部，則將孫、黃等參與「二次革命」者的黨籍撤銷，劃清界線，此時的國民黨已分裂。

## 國會召開及勒令解散國民黨

袁世凱未因國民黨部份黨員發動「二次革命」而立即將它定為非法組織，因為他仍需要國會選舉自己為正式大總統，而國民黨是國會的多數政黨，將其解散等同癱瘓國會。1913年10月6日，国会选举袁世凯为正式大总统；11月4日，袁下令解散國民黨，并收缴國民黨議員證書，國會因人数不足而无法开会；1914年1月，袁宣布解散國會。

## 参考文献

## 參見

  - [宋教仁](../Page/宋教仁.md "wikilink")
  - [革命党](../Page/革命党.md "wikilink")、[興中會](../Page/興中會.md "wikilink")、[中國同盟會](../Page/中國同盟會.md "wikilink")、[中華革命黨](../Page/中華革命黨.md "wikilink")、[五四運動](../Page/五四運動.md "wikilink")、[中國國民黨](../Page/中國國民黨.md "wikilink")

{{-}}

[Category:中华民国大陆时期政党](../Category/中华民国大陆时期政党.md "wikilink")
[-](../Category/中國國民黨歷史.md "wikilink")
[Category:1912年建立的政黨](../Category/1912年建立的政黨.md "wikilink")
[Category:1912年中國建立](../Category/1912年中國建立.md "wikilink")
[Category:1913年解散的政黨](../Category/1913年解散的政黨.md "wikilink")
[Category:1913年中國廢除](../Category/1913年中國廢除.md "wikilink")

1.  李松林、齐福麟、许小军等 编：《中国国民党大事记》，北京：解放军出版社，1988年

2.
3.
4.
5.  [山西芮城陌南镇——景耀月故居的历史承载，中国新闻网，2008年4月21日](http://www.sx.chinanews.com/2008-04-21/1/63019.html)

6.  《中国国民党史纲》，哈尔滨：黑龙江人民出版社，1991年，第67页