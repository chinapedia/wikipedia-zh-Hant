**國票金融控股股份有限公司**（英語：Waterland Financial Holding Co.,
Ltd.，簡稱**國票金控**），為目前[台灣唯一由](../Page/台灣.md "wikilink")[票券業者轉換設立的](../Page/票券.md "wikilink")[金融控股公司](../Page/金融控股公司.md "wikilink")\[1\]，由[國際票券金融公司](../Page/國際票券金融公司.md "wikilink")、[協和證券及](../Page/協和證券.md "wikilink")[大東證券於](../Page/大東證券.md "wikilink")2002年3月26日共同以股份轉換方式成立。國票金控目前以[美麗華集團](../Page/美麗華集團.md "wikilink")、[耐斯集團](../Page/耐斯集團.md "wikilink")、政府公股行庫及[旺旺集團等四方共治的局面為主](../Page/旺旺.md "wikilink")，近期由於公股行庫陸續減持股份，旺旺集團持股持續增加下，大股東的結構開始轉變\[2\]。

## 沿革

  - 2002年3月26日：國票金控成立，同日股票上市。
  - 2002年10月18日：旗下的國票聯合證券、協和證券與大東證券合併，以國票聯合證券為續存公司，並更名為[國票綜合證券](../Page/國票綜合證券.md "wikilink")。
  - 2004年：成立[國票創業投資公司](../Page/國票創業投資公司.md "wikilink")。
  - 2009年：透過子公司國票證券轉投資成立[國票證券(香港)](../Page/國票證券\(香港\).md "wikilink")。
  - 2010年：透過子公司國票證券轉投資成立[國票證券(亞洲)](../Page/國票證券\(亞洲\).md "wikilink")。國票金控內部大股東間曾因併購大都會國際人壽案而產生摩擦\[3\]，歷經半年多的努力，此併購案最後仍被金管會所否決，而未竟全功\[4\]。
  - 2011年：[華頓投信成為國票證券百分之百持股之子公司](../Page/華頓投信.md "wikilink")。
  - 2012年：透過子公司國票創業投資轉投資於[中國](../Page/中國.md "wikilink")[南京成立](../Page/南京.md "wikilink")[國旺國際融資租賃](../Page/國旺國際融資租賃.md "wikilink")。
  - 2015年：國票金控欲透過公開收購的方式併購[三信商銀](../Page/三信商業銀行.md "wikilink")，以擴大經營版圖，但最後並沒有成功\[5\]。
  - 2017年：國票金控去年獲利成長率為台灣全體金控第一名\[6\]，並宣布發派紅利：每股分配現金0.55元，股票0.15元，其中現金股利分配金額，更寫下該金控近十年最高\[7\]\[8\]。

## 金控成員及關係

{{ familytree/start }} {{ familytree | level0 |v| level1 |,| level2 |
level0 = **國票金融控股** | level1 = [國際票券金融](../Page/國際票券金融.md "wikilink") |
level2 = Waterland Securities (BVI) Co., Ltd }} {{ familytree | | | |\!|
| | |\!| }} {{ familytree | | | |\!| | | |)| level2 | level2 = 國票證券投顧顧問
}} {{ familytree | | | |\!| | | |\!| }} {{ familytree | | | |)| level1
|+| level2 | level1 = [國票綜合證券](../Page/國票綜合證券.md "wikilink") | level2 =
華頓證券投資信託 }} {{ familytree | | | |\!| | | |\!| }} {{ familytree | | |
|\!| | | |\`| level2 | level2 = 國票期貨 }} {{ familytree | | | |\!| }} {{
familytree | | | |\`| level1 |-| level2 |-| level3 | level1 =
[國票創業投資](../Page/國票創業投資.md "wikilink") | level2 = IBF
Financial Holding Co., Ltd | level3 = 國旺國際融資租賃 }} {{ familytree/end }}

## 參考資料

## 外部連結

  - [國票金控](http://www.waterland-fin.com.tw/)

[G](../Category/臺灣的金融控股公司.md "wikilink")
[2](../Category/臺灣證券交易所上市公司.md "wikilink")
[Category:總部位於臺北市中山區的工商業機構](../Category/總部位於臺北市中山區的工商業機構.md "wikilink")
[國票金控](../Category/國票金控.md "wikilink")

1.  楊筱筠，[國票金票券起家 穩步拓市](https://money.udn.com/money/story/5612/2206102)
    ，經濟日報，2017-01-03
2.  楊筱筠，[旺旺快成國票金最大股東](https://money.udn.com/money/story/5613/2496145)
    ，經濟日報，2017-06-01
3.  廖千瑩、王寓中、王孟倫，[國票金洪三雄主導
    硬吃大都會人壽](http://news.ltn.com.tw/news/business/paper/381043)，自由時報，2010-03-19
4.  顏真真，[國票金併大都會人壽　金管會4大理由駁回](https://www.nownews.com/news/20101007/624840)，NOWnews今日新聞網，2010-10-07
5.  李靚慧，[國票金收購失敗
    三信商銀：走自己的路，絕不被金控併購](http://news.ltn.com.tw/news/business/breakingnews/1415453)，自由時報，2015-08-17
6.  黃琮淵，[國票金獲利靚
    大發現金股利](http://www.chinatimes.com/realtimenews/20170322005619-260410)，中時電子報，2017-03-22
7.  楊筱筠，[國票金股息0.55元
    十年新高](https://money.udn.com/money/story/5613/2359338)
    ，UDN聯合新聞網，2017-03-23
8.  [國票金擬配現金0.55元、股票0.15元股利](http://www.appledaily.com.tw/realtimenews/article/new/20170323/1082251/)，蘋果日報，2017-03-23