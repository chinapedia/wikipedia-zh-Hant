[Plaza_Che,_Bogotá.jpg](https://zh.wikipedia.org/wiki/File:Plaza_Che,_Bogotá.jpg "fig:Plaza_Che,_Bogotá.jpg")廣場\]\]

**國立大學**（），是[公立大學的一種](../Page/公立大學.md "wikilink")，意同「國家直屬的大學」，由[中央政府所設立](../Page/中央政府.md "wikilink")，不受地方以及自治區政府的管轄。

## 亞洲

### 日本的國立大學

日本的國立大學常與[公立大學合稱](../Page/公立大學.md "wikilink")「國公立大學」，其中7所戰前舊[帝國大學](../Page/帝國大學.md "wikilink")，代表了日本[學術水準的前端](../Page/學術.md "wikilink")。依據《國立學校設置法》，日本的國立學校由文部科學大臣管轄，國立學校的經費，則依法由國庫負擔。為了管理好國立學校的經費，日本議會於1964年通過了《國立學校專項會計法》，把全部國立學校經費從國家一般會計中分離出來，設獨立的專項會計，進一步明確了國家負擔國立學校經費的具體事項。

### 韓國的国立大学

韩国的国立大学，是[韩国政府以](../Page/韩国政府.md "wikilink")「教育兴国」为目的兴建的[高等教育院校](../Page/高等教育.md "wikilink")，其中包括10所[国立旗帜大学](../Page/韩国国立旗帜大学.md "wikilink")。

### 中華民國（臺灣）的國立大學

屬於高等普通教育體系的院校，計有國立大學23所、國立藝術大學3所、國立體育大學2所；屬於師範體系計有國立師範大學3所、國立教育大學2所，合計5所。以上大學校院均需透過參加[大學學科能力測驗或](../Page/大學學科能力測驗.md "wikilink")[大學入學指定科目考試](../Page/大學入學指定科目考試.md "wikilink")，方能錄取。

## 歐洲

### 愛爾蘭的國立大學

[愛爾蘭國立大學](../Page/愛爾蘭國立大學.md "wikilink")，為[愛爾蘭獨立收集了大量的](../Page/愛爾蘭.md "wikilink")[愛爾蘭語和](../Page/愛爾蘭語.md "wikilink")[愛爾蘭文化的資料](../Page/愛爾蘭文化.md "wikilink")，往往同[國家的](../Page/國家.md "wikilink")[文化或](../Page/文化.md "wikilink")[政治願望密切聯繫](../Page/政治.md "wikilink")。

## 大洋洲

### 澳洲的國立大學

[澳洲僅有一所國立大學](../Page/澳洲.md "wikilink")，即是由澳洲[聯邦政府立法成立的](../Page/聯邦.md "wikilink")[澳洲國立大學](../Page/澳洲國立大學.md "wikilink")（The
Australian National
University；ANU），其餘三十多所[公立大學皆由各地](../Page/公立大學.md "wikilink")[州政府立法設立](../Page/州.md "wikilink")；少數一至兩所私立大學。然而，中文媒體經常分辨不清**國立大學**與**公立大學**的區隔，導致錯誤翻譯情況發生，例如誤將公立（州立）[南澳大學](../Page/南澳大學.md "wikilink")（University
of South Australia），錯翻為「*國立*」南澳大學。

## 参考文献

## 參見

  - [大學](../Page/大學.md "wikilink")
      - [私立大學](../Page/私立大學.md "wikilink")
      - [公立大學](../Page/公立大學.md "wikilink")
          - [州立大學系統](../Page/州立大學系統.md "wikilink")
          - [火車便當大學](../Page/火車便當大學.md "wikilink")
      - [天主教大学](../Page/天主教大学.md "wikilink")
          - [宗座大學](../Page/宗座大學.md "wikilink")
      - [大學院大學](../Page/大學院大學.md "wikilink")

{{-}}

[Category:大学类型](../Category/大学类型.md "wikilink")
[国立大学](../Category/国立大学.md "wikilink")
[Category:公立大學](../Category/公立大學.md "wikilink")
[Category:國立機構](../Category/國立機構.md "wikilink")