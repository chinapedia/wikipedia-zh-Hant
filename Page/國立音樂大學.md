**國立音樂大學**（[日語](../Page/日語.md "wikilink")：，）是校總區在[東京都](../Page/東京都.md "wikilink")[立川市](../Page/立川市.md "wikilink")[柏町](../Page/柏町.md "wikilink")3-3-1的一所[日本](../Page/日本.md "wikilink")[私立大學](../Page/私立.md "wikilink")。從1950年所創立。簡稱為**國立音大**（くにたちおんだい）、**國音**（くにおん）。

大學名稱是在1970年從[國立市創立而得](../Page/國立市.md "wikilink")：所以國立音大名稱的「國立」正確唸法應同國立市的「くにたち」，而非讀做「こくりつ」，亦即這是一所非由國家興辦的私立大學。

毎年12月[NHK教育頻道都會播放](../Page/NHK教育頻道.md "wikilink")[NHK交響樂團的](../Page/NHK交響樂團.md "wikilink")[貝多芬](../Page/貝多芬.md "wikilink")[第九號交響曲音樂會](../Page/第九號交響曲_\(貝多芬\).md "wikilink")，合唱團中由於有國立音大主修聲樂的學生擔任演出。

## 沿革

  - 1926年
    *東京高等音楽学院*在[東京都](../Page/東京都.md "wikilink")[四谷設校](../Page/四谷.md "wikilink")。之後遷到國立市
  - 1928年
    與新交響樂團（現[NHK交響樂團](../Page/NHK交響樂團.md "wikilink")）共同演出貝多芬第九號交響曲的合唱部分
  - 1947年 校名改為國立音樂學校
  - 1949年 設立國立中學校（現國立音樂大學附屬中學校）、國立音樂高等學校（現國立音樂大學附屬高等學校）
  - 1950年 國立音樂大學改為[新制大學](../Page/新制大學.md "wikilink")。國立音樂大学附屬幼稚園設立
  - 1953年 國立音樂大學附屬小學校設立
  - 1968年 大學院音樂研究科（修士課程）設置
  - 1978年 遷校至立川市
  - 1988年 樂器學資料館設置

## 學部・學科

  - 音樂學部
      - 演奏學科
          - [聲樂專修](../Page/聲樂.md "wikilink")
          - [鍵盤樂器專修](../Page/鍵盤樂器.md "wikilink")
          - [弦管打樂器專修](../Page/弦管打樂器.md "wikilink")
      - 音楽文化デザイン學科　
          - [音樂創作專修](../Page/音樂創作.md "wikilink")
          - [音樂研究專修](../Page/音樂研究.md "wikilink")
          - [音樂療法專修](../Page/音樂療法.md "wikilink")
      - 音樂教育學科
          - [音樂教育專攻](../Page/音樂教育.md "wikilink")
          - [幼兒教育專攻](../Page/幼兒教育.md "wikilink")
      - 別科　
          - [調律專修](../Page/調律.md "wikilink")

## 大學院

  - 音樂研究科
      - 聲樂專攻
      - 器樂專攻
      - 作曲專攻
      - 音樂學專攻
      - 音樂教育學專攻

## 系列校

  - [國立音樂大學附屬高等學校](../Page/國立音樂大學附屬高等學校.md "wikilink")（國立市）
      - 音樂科
      - 普通科
  - [國立音樂大學附屬中學校](../Page/國立音樂大學附屬中學校.md "wikilink")
  - [國立音樂大學附屬小學校](../Page/國立音樂大學附屬小學校.md "wikilink")
  - [國立音樂大學附屬幼稚園](../Page/國立音樂大學附屬幼稚園.md "wikilink")

## 相關設施

  - 音樂研究所
  - 樂器學資料館

## 知名畢業校友

[作曲家](../Page/作曲家.md "wikilink")

  - [佐藤勝](../Page/佐藤勝.md "wikilink") -
    作曲家、[黒澤明作品などの映画音楽を手がける](../Page/黒澤明.md "wikilink")
  - [神津善行](../Page/神津善行.md "wikilink") -
    作曲家、[中村メイコの夫](../Page/中村メイコ.md "wikilink")、[神津カンナの父](../Page/神津カンナ.md "wikilink")
  - [久石讓](../Page/久石讓.md "wikilink") -
    作曲家、[宮崎駿](../Page/宮崎駿.md "wikilink")、[北野武作品配樂](../Page/北野武.md "wikilink")
  - [松下耕](../Page/松下耕.md "wikilink") - 作曲家、合唱指揮者
  - [武部聰志](../Page/武部聰志.md "wikilink") -
    作曲家、[音乐制作人](../Page/音乐制作人.md "wikilink")、[钢琴家](../Page/钢琴家.md "wikilink")
  - [天野正道](../Page/天野正道.md "wikilink") - 作曲家
  - [大島ミチル](../Page/大島ミチル.md "wikilink") - 作曲家
  - [栗山和樹](../Page/栗山和樹.md "wikilink") - 作曲家、國立音樂大學副教授

[聲樂家](../Page/聲樂家.md "wikilink")

  - [佐藤しのぶ](../Page/佐藤しのぶ.md "wikilink") - 聲樂家
  - [永井和子](../Page/永井和子.md "wikilink") - 聲樂家、東京藝大教授、國立音樂大學兼任教授
  - [澤畑恵美](../Page/澤畑恵美.md "wikilink") - 聲樂家、國立音樂大學專任講師
  - [岡本知高](../Page/岡本知高.md "wikilink") - 聲樂家
  - [吉田浩之](../Page/吉田浩之.md "wikilink") - 聲樂家、東京藝大副教授
  - [錦織健](../Page/錦織健.md "wikilink") - 聲樂家
  - [福井敬](../Page/福井敬.md "wikilink") - 聲樂家、國立音樂大學副教授
  - [高橋薫子](../Page/高橋薫子.md "wikilink") - 聲樂家、國立音樂大學兼任講師
  - [秋川雅史](../Page/秋川雅史.md "wikilink") - 歌手
  - [菅原洋一](../Page/菅原洋一.md "wikilink") - 歌手
  - [遊佐未森](../Page/遊佐未森.md "wikilink") - 歌手
  - [友香](../Page/友香.md "wikilink") - 歌手

[演奏家](../Page/演奏家.md "wikilink")

  - [MAYUKO](../Page/MAYUKO.md "wikilink") - ピアノ弾き語りアーティスト、作詞、作曲家
  - [赤坂達三](../Page/赤坂達三.md "wikilink") -
    [クラリネット奏者](../Page/クラリネット奏者.md "wikilink")、作曲家
  - [小原孝](../Page/小原孝.md "wikilink") -
    [钢琴家](../Page/钢琴家.md "wikilink")、國立音樂大學兼任講師
  - [三村奈奈恵](../Page/三村奈奈恵.md "wikilink") -
    [马林巴](../Page/马林巴.md "wikilink")、[ヴィブラフォン奏者](../Page/ヴィブラフォン.md "wikilink")
  - [及川浩治](../Page/及川浩治.md "wikilink") -
    [钢琴家](../Page/钢琴家.md "wikilink")
  - [佐山雅弘](../Page/佐山雅弘.md "wikilink") - ジャズピアニスト、國立音樂大學客座講師
  - [本田竹廣](../Page/本田竹廣.md "wikilink") - ジャズピアニスト
  - [山下洋輔](../Page/山下洋輔.md "wikilink") - ジャズピアニスト、國立音樂大學教授
  - [金子健](../Page/金子健.md "wikilink") - ジャズベーシスト、國立音樂大學兼任講師
  - [國府弘子](../Page/國府弘子.md "wikilink") - ジャズピアニスト
  - [港大尋](../Page/港大尋.md "wikilink") - ミュージシャン、東京藝術大學・京都造形藝術大學兼任講師
  - [本田雅人](../Page/本田雅人.md "wikilink") -
    [萨克斯奏者](../Page/萨克斯.md "wikilink")(元T-SQUARE)
  - [茂木大輔](../Page/茂木大輔.md "wikilink") -
    [双簧管奏者](../Page/双簧管.md "wikilink")、エッセイスト、指揮者、コンサートプロデューサー
  - [齋藤行](../Page/齋藤行.md "wikilink") -
    [クラリネット奏者](../Page/クラリネット奏者.md "wikilink")、[指揮者](../Page/指揮者.md "wikilink")
  - [梅津和時](../Page/梅津和時.md "wikilink") - サックス奏者
  - [柴田義也](../Page/柴田義也.md "wikilink") -
    キーボード、ピアノプレイヤー(G2として[RCサクセションで活躍](../Page/RCサクセション.md "wikilink"))
  - [尾崎宏隆](../Page/尾崎宏隆.md "wikilink") -
    [指揮者](../Page/指揮者.md "wikilink")
  - [依田彩](../Page/依田彩.md "wikilink") - ジャズバイオリニスト
  - [滑川真希](../Page/滑川真希.md "wikilink") - 在独。ピアニスト
  - [分山貴美子](../Page/分山貴美子.md "wikilink") - くちぶえ演奏家
  - [国田朋宏](../Page/国田朋宏.md "wikilink") - Horn演奏家，NSO(Taiwan,R.O.C)演奏員。

其它

  - [竹内のぞみ](../Page/竹内のぞみ.md "wikilink") -
    [タレント](../Page/タレント.md "wikilink")
  - [小林亞希子](../Page/小林亞希子.md "wikilink") - [MY LITTLE
    LOVERのボーカリスト](../Page/MY_LITTLE_LOVER.md "wikilink")。[小林武史の](../Page/小林武史.md "wikilink")[元妻](../Page/元妻.md "wikilink")。
  - [廣瀨香美](../Page/廣瀨香美.md "wikilink") - シンガーソングライター
  - [宇野功芳](../Page/宇野功芳.md "wikilink") - 音樂評論家
  - [山崎睦](../Page/山崎睦.md "wikilink") - 音樂評論家
  - [小林一枝](../Page/小林一枝.md "wikilink") - 元朝日電視台アナウンサー
  - [本村由紀子](../Page/本村由紀子.md "wikilink") - フリーアナウンサー
  - [三柴理](../Page/三柴理.md "wikilink") -
    音樂プロデューサー、[特撮メンバー](../Page/特撮_\(バンド\).md "wikilink")（元[筋肉少女帯](../Page/筋肉少女帯.md "wikilink")）
  - [澤田亞矢子](../Page/澤田亞矢子.md "wikilink")（中退） - 女演員、歌手
  - [篠原恵美](../Page/篠原恵美.md "wikilink") -
    [配音員](../Page/配音員.md "wikilink")
  - [清水祥恵](../Page/清水祥恵.md "wikilink") - タレント
  - [飯島真理](../Page/飯島真理.md "wikilink")（中退） - 歌手
  - [橋本貴久](../Page/橋本貴久.md "wikilink") - 乙三．トロンボーン奏者
  - [ヒイズミマサユ機](../Page/ヒイズミマサユ機.md "wikilink") - PE'Z 鍵盤
  - [PE'Zメンバー](../Page/PE'Zメンバー.md "wikilink")（Nirehara、Ohyama以外）
  - [伊澤一葉](../Page/伊澤一葉.md "wikilink") -
    あっぱ、[東京事變](../Page/東京事變.md "wikilink")　鍵盤担当
  - [安福毅](../Page/安福毅.md "wikilink") -
    元[四季剧团](../Page/四季剧团.md "wikilink")、舞台俳優
  - [中島有香](../Page/中島有香.md "wikilink") - TSCアナウンサー
  - [タテタカコ](../Page/タテタカコ.md "wikilink") - シンガーソングライター

## 外部連結

  - [國立音樂大學](http://www.kunitachi.ac.jp/)

[Category:東京都的大學](../Category/東京都的大學.md "wikilink")
[Category:日本私立大學](../Category/日本私立大學.md "wikilink")
[Category:1950年創建的教育機構](../Category/1950年創建的教育機構.md "wikilink")
[Category:日本音樂學校](../Category/日本音樂學校.md "wikilink")
[Category:立川市](../Category/立川市.md "wikilink")