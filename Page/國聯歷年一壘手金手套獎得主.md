以下列出[美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")[國聯歷年在擔任](../Page/國家聯盟.md "wikilink")[一壘手這個位置時獲得](../Page/一壘手.md "wikilink")[金手套獎](../Page/美國職棒大聯盟金手套獎.md "wikilink")（）的球員。

-----

| **[金手套獎](../Page/美國職棒大聯盟金手套獎.md "wikilink")** |
| :-------------------------------------------: |
|      **[美聯](../Page/美聯.md "wikilink"):**      |
|      **[國聯](../Page/國聯.md "wikilink"):**      |

-----

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>球員</p></th>
<th><p>球隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1957年</p></td>
<td><p><a href="../Page/:en:Gil_Hodges.md" title="wikilink">Gil Hodges</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">布魯克林道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1958年</p></td>
<td><p><a href="../Page/:en:Gil_Hodges.md" title="wikilink">Gil Hodges</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">布魯克林道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1959年</p></td>
<td><p><a href="../Page/:en:Gil_Hodges.md" title="wikilink">Gil Hodges</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1960年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1961年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="even">
<td><p>1962年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1963年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="even">
<td><p>1964年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1965年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="even">
<td><p>1966年</p></td>
<td><p><a href="../Page/:en:Bill_White.md" title="wikilink">Bill White</a></p></td>
<td><p><a href="../Page/費城費城人.md" title="wikilink">費城費城人</a></p></td>
</tr>
<tr class="odd">
<td><p>1967年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1968年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1969年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1970年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td><p><a href="../Page/:en:Wes_Parker.md" title="wikilink">Wes Parker</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1973年</p></td>
<td><p><a href="../Page/:en:Mike_Jorgensen.md" title="wikilink">Mike Jorgensen</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">蒙特婁博覽會</a></p></td>
</tr>
<tr class="even">
<td><p>1974年</p></td>
<td><p><a href="../Page/:en:Steve_Garvey.md" title="wikilink">Steve Garvey</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1975年</p></td>
<td><p><a href="../Page/:en:Steve_Garvey.md" title="wikilink">Steve Garvey</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/:en:Steve_Garvey.md" title="wikilink">Steve Garvey</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p><a href="../Page/:en:Steve_Garvey.md" title="wikilink">Steve Garvey</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="even">
<td><p>1978年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1981年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a>／<a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="even">
<td><p>1986年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/:en:Keith_Hernandez.md" title="wikilink">Keith Hernandez</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/:en:Andrés_Galarraga.md" title="wikilink">Andrés Galarraga</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">蒙特婁博覽會</a></p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/:en:Andrés_Galarraga.md" title="wikilink">Andrés Galarraga</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">蒙特婁博覽會</a></p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/:en:Will_Clark.md" title="wikilink">Will Clark</a></p></td>
<td><p><a href="../Page/舊金山巨人.md" title="wikilink">舊金山巨人</a></p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/:en:Will_Clark.md" title="wikilink">Will Clark</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/:en:Will_Clark.md" title="wikilink">Will Clark</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/:en:Jeff_Bagwell.md" title="wikilink">Jeff Bagwell</a></p></td>
<td><p><a href="../Page/休士頓太空人.md" title="wikilink">休士頓太空人</a></p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/:en:Mark_Grace.md" title="wikilink">Mark Grace</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/:en:Mark_Grace.md" title="wikilink">Mark Grace</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/:en:J.T._Snow.md" title="wikilink">J.T. Snow</a></p></td>
<td><p><a href="../Page/舊金山巨人.md" title="wikilink">舊金山巨人</a></p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/:en:J.T._Snow.md" title="wikilink">J.T. Snow</a></p></td>
<td><p><a href="../Page/舊金山巨人.md" title="wikilink">舊金山巨人</a></p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/:en:J.T._Snow.md" title="wikilink">J.T. Snow</a></p></td>
<td><p><a href="../Page/舊金山巨人.md" title="wikilink">舊金山巨人</a></p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/:en:J.T._Snow.md" title="wikilink">J.T. Snow</a></p></td>
<td><p><a href="../Page/舊金山巨人.md" title="wikilink">舊金山巨人</a></p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/陶德·希爾頓.md" title="wikilink">陶德·希爾頓</a></p></td>
<td><p><a href="../Page/科羅拉多落磯.md" title="wikilink">科羅拉多落磯</a></p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/陶德·希爾頓.md" title="wikilink">陶德·希爾頓</a></p></td>
<td><p><a href="../Page/科羅拉多落磯.md" title="wikilink">科羅拉多落磯</a></p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/德瑞克·李.md" title="wikilink">德瑞克·李</a></p></td>
<td><p><a href="../Page/佛羅里達馬林魚.md" title="wikilink">佛羅里達馬林魚</a></p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/陶德·希爾頓.md" title="wikilink">陶德·希爾頓</a></p></td>
<td><p><a href="../Page/科羅拉多落磯.md" title="wikilink">科羅拉多落磯</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/德瑞克·李.md" title="wikilink">德瑞克·李</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/亞伯特·普荷斯.md" title="wikilink">亞伯特·普荷斯</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/德瑞克·李.md" title="wikilink">德瑞克·李</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/艾德里安·冈萨雷斯.md" title="wikilink">艾德里安·冈萨雷斯</a></p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/艾德里安·冈萨雷斯.md" title="wikilink">艾德里安·冈萨雷斯</a></p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/亞伯特·普荷斯.md" title="wikilink">亞伯特·普荷斯</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/喬伊·沃托.md" title="wikilink">喬伊·沃托</a></p></td>
<td><p><a href="../Page/辛辛那提紅人.md" title="wikilink">辛辛那提紅人</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Adam_LaRoche.md" title="wikilink">Adam LaRoche</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">華盛頓國民</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/保羅·高施密特.md" title="wikilink">保羅·高施密特</a></p></td>
<td><p><a href="../Page/亞利桑那響尾蛇.md" title="wikilink">亞利桑那響尾蛇</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/艾德里安·冈萨雷斯.md" title="wikilink">艾德里安·冈萨雷斯</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/保羅·高施密特.md" title="wikilink">保羅·高施密特</a></p></td>
<td><p><a href="../Page/亞利桑那響尾蛇.md" title="wikilink">亞利桑那響尾蛇</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/安東尼·瑞佐.md" title="wikilink">安東尼·瑞佐</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/保羅·高施密特.md" title="wikilink">保羅·高施密特</a></p></td>
<td><p><a href="../Page/亞利桑那響尾蛇.md" title="wikilink">亞利桑那響尾蛇</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/安東尼·瑞佐.md" title="wikilink">安東尼·瑞佐</a><br />
<a href="../Page/弗萊迪·弗里曼.md" title="wikilink">弗萊迪·弗里曼</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a><br />
<a href="../Page/亞特蘭大勇士.md" title="wikilink">亞特蘭大勇士</a></p></td>
</tr>
</tbody>
</table>

[en:List of NL Gold Glove winners at first
base](../Page/en:List_of_NL_Gold_Glove_winners_at_first_base.md "wikilink")

[Gold Glove Award, 3-NL](../Category/美國職棒獎項.md "wikilink")