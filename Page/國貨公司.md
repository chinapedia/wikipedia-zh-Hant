[HK_TST_Peking_Road_Yue_Hwa_Chinese_Products.JPG](https://zh.wikipedia.org/wiki/File:HK_TST_Peking_Road_Yue_Hwa_Chinese_Products.JPG "fig:HK_TST_Peking_Road_Yue_Hwa_Chinese_Products.JPG")

在[香港](../Page/香港.md "wikilink")，**國貨公司**是指[中資背景](../Page/中資.md "wikilink")，主要售賣[中國大陸產品的](../Page/中國大陸.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")。在中國大陸成為「世界工廠」之前，國貨公司是香港人接觸其產品的少數渠道之一。很多中國大陸[品牌產品或特產如藥材](../Page/品牌.md "wikilink")、傳統[手工藝只能在國貨公司買到](../Page/手工藝.md "wikilink")，亦以價廉物美著稱。國貨公司全盛時，店舖遍佈港、九、新界，多達300間。但時移勢易，大部份的國貨公司因不敵租金急升等種種原因，相繼走向結業之路。

在1980年代有所謂「國貨公司四寶」，就是棉襖、學生服裝、[睡衣及](../Page/睡衣.md "wikilink")[羊毛衣](../Page/羊毛.md "wikilink")\[1\]。

## 著名國貨公司

[Sun_Chung_Luen_Chinese_Products_Company_Limited_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Sun_Chung_Luen_Chinese_Products_Company_Limited_\(Hong_Kong\).jpg "fig:Sun_Chung_Luen_Chinese_Products_Company_Limited_(Hong_Kong).jpg")
[Chinese_Goods_Centre_Ltd._(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Chinese_Goods_Centre_Ltd._\(Hong_Kong\).jpg "fig:Chinese_Goods_Centre_Ltd._(Hong_Kong).jpg")

  - [裕華國貨](../Page/裕華國貨.md "wikilink")
  - [中藝](../Page/中藝.md "wikilink")
  - 中聯／新中聯（荃灣）
  - [中僑國貨](../Page/中僑國貨.md "wikilink")（荃灣）
  - [中國國貨](../Page/中國國貨.md "wikilink")（原位於荃灣[南豐中心](../Page/南豐中心.md "wikilink")，已結業）
  - [中都國貨](../Page/中都國貨.md "wikilink")（元朗）
  - [中邦國貨](../Page/中邦國貨.md "wikilink")
  - [華豐國貨](../Page/華豐國貨.md "wikilink")（北角[僑冠大廈](../Page/僑冠大廈.md "wikilink")）
  - [中匯國貨](../Page/中匯國貨.md "wikilink")（觀塘裕民坊）
  - [大華國貨](../Page/大華國貨.md "wikilink")（旺角中心第1期側）
  - [新華國貨](../Page/新華國貨.md "wikilink")
  - [新中華國貨](../Page/新中華國貨.md "wikilink")

### 台灣

  - [台灣中華國貨公司](../Page/菊元百貨.md "wikilink") -
    日治時代[菊元百貨店後身](../Page/菊元百貨.md "wikilink")，已停業

## 資料來源

## 另見

  - [民生物產](../Page/民生物產.md "wikilink")

[Category:香港百貨公司](../Category/香港百貨公司.md "wikilink")

1.