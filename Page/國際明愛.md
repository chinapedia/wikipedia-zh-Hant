**國際明愛**（），是由165個[天主教公益團體組成的國際](../Page/天主教.md "wikilink")[慈善](../Page/慈善.md "wikilink")、發展和社會服務組織，第一個成員團體在1897年11月9日於[德國](../Page/德國.md "wikilink")[弗萊堡成立](../Page/弗萊堡.md "wikilink")，隨後開始在各國獲得響應，最後終於在1950年由十三個國家的明愛團體組成了國際明愛會。

2011年時獲教廷認證為教會法人團體。現任國際明愛主席為[菲律賓籍的](../Page/菲律賓.md "wikilink")[類思·安多尼·塔格萊](../Page/類思·安多尼·塔格萊.md "wikilink")[樞機](../Page/樞機.md "wikilink")。

## 參見

  - [澳門明愛](../Page/澳門明愛.md "wikilink")
  - [香港明愛](../Page/香港明愛.md "wikilink")

## 外部連結

  - [國際明愛](http://www.caritas.org/)
  - [財團法人臺灣明愛文教基金會（台灣明愛會）](http://caritas.catholic.org.tw/)
  - [新加坡明愛](http://www.caritas-singapore.org/)
  - [日本明愛](http://www.caritas.jp/)
  - [韓國明愛](http://www.caritas.or.kr/)
  - [明愛(倫敦)學院](http://ming-ai.org.uk/)

## 參考文獻

<references/>

[Category:國際組織](../Category/國際組織.md "wikilink")
[Category:天主教組織](../Category/天主教組織.md "wikilink")
[Category:基督教組織](../Category/基督教組織.md "wikilink")