**國際森林學學生會** (International Forestry Students' Association，簡稱IFSA)
經過三十年的發展，到現在已經成長為一個全球性的組織，它讓全世界的[森林學學生聚在一起](../Page/森林學.md "wikilink")，進行各式各樣的活動。

## 歷史

### 研討會

剛開始，是一個[森林學學生的年度聚會](../Page/森林學.md "wikilink")：國際森林系學生研討會(the
International Forestry Students
Symposium，簡稱IFSS)。1973年的時候，[英國第一次籌畫IFSS](../Page/英國.md "wikilink")，之後連續主辦了13年，IFSS目的在於提供[森林系學生交流平台以便相互討論理想和觀點](../Page/森林系.md "wikilink")，使所有學生團結一致，彼此啟發。後來，IFSS轉移到[歐陸舉辦](../Page/歐陸.md "wikilink")，其中的3次會議，創立了資訊中心(INFOCENTER)，整合森林系學生間的資訊。

### 通過臨時章程

第18屆會議於[葡萄牙](../Page/葡萄牙.md "wikilink")[里斯本舉辦](../Page/里斯本.md "wikilink")，參與者決定在年度會議之外，進一步擴張森林系學生間的合作。在IFSA的創立大會上，創始成員通過了臨時[章程](../Page/章程.md "wikilink")(provisional
statutes)，並選出第1任代表。在[荷蘭的第](../Page/荷蘭.md "wikilink")19屆會議是一次空前的大成功，有來自38個國家的112個參與者，第一次會齊各大洲的成員。在第20屆會議中，第3屆會員大會(General
Assembly)成立中央機關，秘書處(the
Secretariat)，負責組織的行政事務、內部溝通及所有資訊中心的職務，使其成為IFSA的總部。

### 通過章程，正式登記

第21屆會議在[馬來西亞舉辦](../Page/馬來西亞.md "wikilink")，是第一次在[歐洲之外的地方舉辦因此意義重大](../Page/歐洲.md "wikilink")。在[瑞士的第](../Page/瑞士.md "wikilink")22屆IFSS中，第5屆會員大會通過了新的章程。IFSA正式登記為[社團法人](../Page/社團法人.md "wikilink")，並以[德國的](../Page/德國.md "wikilink")[哥廷根](../Page/哥廷根.md "wikilink")[Göttingen為總部所在](../Page/Göttingen.md "wikilink")。第25、26屆在[非洲的](../Page/非洲.md "wikilink")[南非和](../Page/南非.md "wikilink")[迦納舉辦](../Page/迦納.md "wikilink")，成就非凡。第27屆會議在[德國舉辦](../Page/德國.md "wikilink")，通過了修訂章程、[法令](../Page/法令.md "wikilink")(Decree)和[規則](../Page/規則.md "wikilink")(By-law)，而且會員大會把所在地移到[Freiburg](../Page/Freiburg.md "wikilink")。

### 區域會議

在2000年，IFSA多了一項新的活動，在[東歐和](../Page/東歐.md "wikilink")[西歐分別舉辦區域會議](../Page/西歐.md "wikilink")，分別由[丹麥](../Page/丹麥.md "wikilink")[哥本哈根和](../Page/哥本哈根.md "wikilink")[匈牙利](../Page/匈牙利.md "wikilink")[Sopron的學生主辦](../Page/Sopron.md "wikilink")。2001年地區集會由[瑞士](../Page/瑞士.md "wikilink")[蘇黎世和](../Page/蘇黎世.md "wikilink")[克羅埃西亞](../Page/克羅埃西亞.md "wikilink")[Zagreb的學生安排](../Page/Zagreb.md "wikilink")，區域會議的舉辦成為慣例。2001年的IFSS在[捷克共和國](../Page/捷克共和國.md "wikilink")，由[Brno](../Page/Brno.md "wikilink")[孟德爾大學的學生舉辦](../Page/孟德爾大學.md "wikilink")。

### 區際高峰會

在[捷克期間](../Page/捷克.md "wikilink")，[北歐和](../Page/北歐.md "wikilink")[波羅的海諸國的學生計畫一個稱為北歐區際高峰會](../Page/波羅的海.md "wikilink")(Northern
European Interregional
Summit，簡稱NEIS)的活動，後來也成為慣例。第一屆NEIS於2002年由[赫爾辛基的學生在](../Page/赫爾辛基.md "wikilink")[芬蘭舉辦](../Page/芬蘭.md "wikilink")，值得紀念的是，會中成員在[芬蘭式蒸汽浴之後進了結冰的湖泊游泳](../Page/芬蘭式蒸汽浴.md "wikilink")，該屆主題是「波羅的海區域林業的經濟未來」。

## 連結

  - [IFSA](http://www.ifsa.net/)

[category:林业组织](../Page/category:林业组织.md "wikilink")

[Category:国际专业性组织](../Category/国际专业性组织.md "wikilink")
[Category:国际学生组织](../Category/国际学生组织.md "wikilink")
[Category:学生交换](../Category/学生交换.md "wikilink")
[Category:总部在德国的国际组织](../Category/总部在德国的国际组织.md "wikilink")