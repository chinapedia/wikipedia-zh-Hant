**國際社會學協會**（簡稱**ISA**）是個[非營利組織](../Page/非營利組織.md "wikilink")，創立於1949年，與[聯合國教育科學文化組織](../Page/聯合國教育科學文化組織.md "wikilink")（UNESCO）有正式關係，目前共有來自於一百零九的國家的會員參與其中。其宗旨是："代表各地不同學校、不同的研究方法、不同意識形態的[社會學家](../Page/社會學家.md "wikilink")，並且在全世界推廣社會學知識"。除了聯合國教育科學文化組織，國際社會學協會同時也是[國際社會科學理事會](../Page/國際社會科學理事會.md "wikilink")（International
Social Science
Council）的一員，並且是[聯合國經濟及社會理事會的特別顧問](../Page/聯合國經濟及社會理事會.md "wikilink")。

## 世界大會

國際社會學協會每四年舉辦一次世界大會。最後一次大會，第十六屆大會，在[南非的](../Page/南非.md "wikilink")[德班舉辦](../Page/德班.md "wikilink")，主席是[法國](../Page/法國.md "wikilink")[社會學家Michel](../Page/社會學家.md "wikilink")
Wieviorka，副主席是[德國](../Page/德國.md "wikilink")[埃爾福特大學](../Page/埃爾福特大學.md "wikilink")（University
of Erfurt）社會學家Hans Joas。
世界大會是國際社會學協會最受矚目的活動，其節目內容和參加人數從第一屆大會以來便急遽增加，當初第一屆大會在[瑞士的](../Page/瑞士.md "wikilink")[蘇黎世舉辦](../Page/蘇黎世.md "wikilink")，其參與者只有一百多人而已。

## 歷屆世界大會舉辦地點

  - [列日](../Page/列日.md "wikilink"), [比利時](../Page/比利時.md "wikilink")
    (1953)
  - [阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink"), [荷蘭](../Page/荷蘭.md "wikilink")
    (1956)
  - [Stresa](../Page/Stresa.md "wikilink"),
    [義大利](../Page/義大利.md "wikilink") (1959)
  - [華盛頓特區](../Page/華盛頓特區.md "wikilink"), [美國](../Page/美國.md "wikilink")
    (1962)
  - [埃維昂萊班](../Page/埃維昂萊班.md "wikilink"), [法國](../Page/法國.md "wikilink")
    (1966)
  - [瓦爾那](../Page/瓦爾那.md "wikilink"), [保加利亞](../Page/保加利亞.md "wikilink")
    (1970)
  - [多倫多](../Page/多倫多.md "wikilink"), [加拿大](../Page/加拿大.md "wikilink")
    (1974)
  - [烏普薩拉](../Page/烏普薩拉.md "wikilink"), [瑞典](../Page/瑞典.md "wikilink")
    (1978)
  - [墨西哥城](../Page/墨西哥城.md "wikilink"), [墨西哥](../Page/墨西哥.md "wikilink")
    (1982)
  - [新德里](../Page/新德里.md "wikilink"), [印度](../Page/印度.md "wikilink")
    (1986)
  - [馬德里](../Page/馬德里.md "wikilink"), [西班牙](../Page/西班牙.md "wikilink")
    (1990)
  - [比勒費爾德](../Page/比勒費爾德.md "wikilink"), [德國](../Page/德國.md "wikilink")
    (1994)
  - [蒙特利爾](../Page/蒙特利爾.md "wikilink"), [加拿大](../Page/加拿大.md "wikilink")
    (1998)
  - [布里斯班](../Page/布里斯班.md "wikilink"),
    [澳大利亞](../Page/澳大利亞.md "wikilink") (2002)
  - [德班](../Page/德班.md "wikilink"), [南非](../Page/南非.md "wikilink")
    (2006)
  - [哥德堡](../Page/哥德堡.md "wikilink"), [瑞典](../Page/瑞典.md "wikilink")
    (2010)
  - [横滨市](../Page/横滨市.md "wikilink"), [日本](../Page/日本.md "wikilink")
    (2014)

## 历届会长

  - 1949-1952 [Louis Wirth](../Page/Louis_Wirth.md "wikilink"), 美国
  - 1953-1956 [Robert C.
    Angell](../Page/Robert_C._Angell.md "wikilink"), 美国
  - 1956-1959 [Georges
    Friedmann](../Page/Georges_Friedmann.md "wikilink"), 法国
  - 1959-1962 [Thomas
    Marshall](../Page/Thomas_Humphrey_Marshall.md "wikilink"), 英国
  - 1962-1966 [René König](../Page/René_König.md "wikilink"), 德国
  - 1966-1970 [Jan
    Szczepański](../Page/Jan_Szczepański_\(sociologist\).md "wikilink"),
    波兰
  - 1970-1974 [Reuben Hill](../Page/Reuben_Hill.md "wikilink"), 美国
  - 1974-1978 [Thomas
    Bottomore](../Page/Thomas_Bottomore.md "wikilink"), 英国
  - 1978-1982 [Ulf
    Himmelstrand](../Page/Ulf_Himmelstrand.md "wikilink"), 瑞典
  - 1982-1986 [费尔南多·恩里克·卡多佐](../Page/费尔南多·恩里克·卡多佐.md "wikilink"), 巴西
  - 1986-1990 [Margaret Archer](../Page/Margaret_Archer.md "wikilink"),
    英国
  - 1990-1994 [T.K. Oommen](../Page/T.K._Oommen.md "wikilink"), 印度
  - 1994-1998 [伊曼纽·华勒斯坦](../Page/伊曼纽·华勒斯坦.md "wikilink"), 美国
  - 1998-2002 [Alberto
    Martinelli](../Page/Alberto_Martinelli.md "wikilink"), 意大利
  - 2002-2006 [Piotr Sztompka](../Page/Piotr_Sztompka.md "wikilink"), 波兰
  - 2006-2010 [Michel
    Wieviorka](../Page/Michel_Wieviorka.md "wikilink"), 法国
  - 2010-2014 [麦克·布洛维](../Page/麦克·布洛维.md "wikilink"), 美国
  - 2014-2018 [Margaret
    Abraham](../Page/Margaret_Abraham.md "wikilink"), 美国

## 外部連結

  - [國際社會學協會](http://www.isa-sociology.org)

[Category:超組織](../Category/超組織.md "wikilink")
[Category:社會學組織](../Category/社會學組織.md "wikilink")
[Category:1949年建立的组织](../Category/1949年建立的组织.md "wikilink")