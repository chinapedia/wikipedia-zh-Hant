\-{zh-hans:**國際科學奥林匹克競賽**，又稱為**國際中學生奥林匹克競賽**;zh-hk:**國際科學奧林匹克**;zh-tw:**國際科學奧林匹亞**}-，是供全球各地[中學生的比賽](../Page/中學.md "wikilink")，一般指由世界各國尚未接受系統的高等教育的中學生參加的學科知識競賽。學科奧林匹克競賽每年舉辦一次，由參與競賽各國的國家級教育主管部門（通常為教育部）輪流舉辦。參加競賽的國家每國派出4-6人不等的中學生組成代表隊赴舉辦國參加比賽，決出金、銀、銅牌及其他各種獎項若干。

<table>
<thead>
<tr class="header">
<th><p>序號</p></th>
<th><p>項目(中文名稱)</p></th>
<th><p>項目(英文名稱)</p></th>
<th><p>縮寫</p></th>
<th><p>開始年分</p></th>
<th><p>官方網站</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/國際數學奧林匹克.md" title="wikilink">國際數學奧林匹克</a></p></td>
<td><p>International Mathematical Olympiad</p></td>
<td><p>IMO</p></td>
<td><p>1959</p></td>
<td><p><a href="http://www.imo-official.org/">http://www.imo-official.org/</a></p></td>
<td><p>1980年未舉行。</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/國際物理奧林匹克.md" title="wikilink">國際物理奧林匹克</a></p></td>
<td><p>International Physics Olympiad</p></td>
<td><p>IPhO</p></td>
<td><p>1967</p></td>
<td><p><a href="http://ipho.org/">http://ipho.org/</a></p></td>
<td><p><span style="font-size:90%;">1973年、1978年和1980年未舉行。</span></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/國際化學奧林匹克.md" title="wikilink">國際化學奧林匹克</a></p></td>
<td><p>International Chemistry Olympiad</p></td>
<td><p>IChO</p></td>
<td><p>1968</p></td>
<td><p><a href="http://www.ichosc.org/">http://www.ichosc.org/</a></p></td>
<td><p>1971年未舉行。</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/國際資訊奧林匹克.md" title="wikilink">國際資訊奧林匹克</a></p></td>
<td><p>International Olympiad in Informatics</p></td>
<td><p>IOI</p></td>
<td><p>1989</p></td>
<td><p><a href="http://www.ioinformatics.org/index.shtml">http://www.ioinformatics.org/index.shtml</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/國際生物奧林匹克.md" title="wikilink">國際生物奧林匹克</a></p></td>
<td><p>International Biology Olympiad</p></td>
<td><p>IBO</p></td>
<td><p>1990</p></td>
<td><p><a href="https://web.archive.org/web/20180820214151/http://www.ibo-info.org/">https://web.archive.org/web/20180820214151/http://www.ibo-info.org/</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/国际哲学奥林匹克.md" title="wikilink">国际哲学奥林匹克</a></p></td>
<td><p>International Philosophy Olympiad</p></td>
<td><p>IPO</p></td>
<td><p>1993</p></td>
<td><p><a href="http://www.philosophy-olympiad.org/">http://www.philosophy-olympiad.org/</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/國際天文奧林匹克.md" title="wikilink">國際天文奧林匹克</a></p></td>
<td><p>International Astronomy Olympiad</p></td>
<td><p>IAO</p></td>
<td><p>1996</p></td>
<td><p><a href="http://www.issp.ac.ru/iao/">http://www.issp.ac.ru/iao/</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/國際地理奥林匹克.md" title="wikilink">國際地理奥林匹克</a></p></td>
<td><p>International Geography Olympiad</p></td>
<td><p>iGeo</p></td>
<td><p>1996</p></td>
<td><p><a href="http://www.geoolympiad.org/">http://www.geoolympiad.org/</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/國際語言學奥林匹克.md" title="wikilink">國際語言學奥林匹克</a></p></td>
<td><p>International Linguistics Olympiad</p></td>
<td><p>IOL</p></td>
<td><p>2003</p></td>
<td><p><a href="http://www.ioling.org/">http://www.ioling.org/</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/國際初中科學奧林匹克.md" title="wikilink">國際-{zh-cn:初中;zh-hk:初中;zh-tw:國中;}-科學奧林匹克</a></p></td>
<td><p>International Junior Science Olympiad</p></td>
<td><p>IJSO</p></td>
<td><p>2004</p></td>
<td><p><a href="http://www.ijsoweb.org/">http://www.ijsoweb.org/</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/國際地球科學奧林匹亞.md" title="wikilink">國際地球科學奧林匹亞</a></p></td>
<td><p>International Earth Science Olympiad</p></td>
<td><p>IESO</p></td>
<td><p>2007</p></td>
<td><p><a href="http://www.ieso-info.org/">http://www.ieso-info.org/</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/國際天文和天體物理學奥林匹克競賽.md" title="wikilink">國際天文和天體物理學<br />
奥林匹克競賽</a></p></td>
<td><p>International Olympiad on<br />
Astronomy and Astrophysics</p></td>
<td><p>IOAA</p></td>
<td><p>2007</p></td>
<td><p><a href="https://ioaa2017.posn.or.th/">https://ioaa2017.posn.or.th/</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## International Olympiad List

### World

  - Science Olympiads are international
    [student](../Page/student.md "wikilink") competitions. There are
    international science olympiads to date:

| Number       | Science                                                                                                                                                                                  | Symbol  | Year       | Web                                                                   |
| ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | ---------- | --------------------------------------------------------------------- |
| Exam Base    |                                                                                                                                                                                          |         |            |                                                                       |
| 1            | [International Mathematical Olympiad](../Page/International_Mathematical_Olympiad.md "wikilink")                                                                                         | IMO     | since 1959 | <http://www.imo-official.org/>                                        |
| 2            | [International Physics Olympiad](../Page/International_Physics_Olympiad.md "wikilink")                                                                                                   | IPhO    | since 1967 | <http://ipho.org/>                                                    |
| 3            | [International Chemistry Olympiad](../Page/International_Chemistry_Olympiad.md "wikilink")                                                                                               | IChO    | since 1968 | <http://www.ichosc.org/>                                              |
| 4            | [International Olympiad in Informatics](../Page/International_Olympiad_in_Informatics.md "wikilink")                                                                                     | IOI     | since 1989 | <http://www.ioinformatics.org/index.shtml>                            |
| 5            | [International Biology Olympiad](../Page/International_Biology_Olympiad.md "wikilink")                                                                                                   | IBO     | since 1990 | <https://web.archive.org/web/20180820214151/http://www.ibo-info.org/> |
| 6            | [International Philosophy Olympiad](../Page/International_Philosophy_Olympiad.md "wikilink")                                                                                             | IPO     | since 1993 | <http://www.philosophy-olympiad.org/>                                 |
| 7            | [International Astronomy Olympiad](../Page/International_Astronomy_Olympiad.md "wikilink")                                                                                               | IAO     | since 1996 | <http://www.issp.ac.ru/iao/>                                          |
| 8            | [International Geography Olympiad](../Page/International_Geography_Olympiad.md "wikilink")                                                                                               | iGeo    | since 1996 | <http://www.geoolympiad.org/>                                         |
| 9            | [International Linguistics Olympiad](../Page/International_Linguistics_Olympiad.md "wikilink")                                                                                           | IOL     | since 2003 | <http://www.ioling.org/>                                              |
| 10           | [International Junior Science Olympiad](../Page/International_Junior_Science_Olympiad.md "wikilink")                                                                                     | IJSO    | since 2004 | <http://www.ijsoweb.org/>                                             |
| 11           | [International Earth Science Olympiad](../Page/International_Earth_Science_Olympiad.md "wikilink")                                                                                       | IESO    | since 2007 | <http://www.ieso-info.org/>                                           |
| 12           | [International Olympiad on Astronomy and Astrophysics](../Page/International_Olympiad_on_Astronomy_and_Astrophysics.md "wikilink")                                                       | IOAA    | since 2007 | <https://ioaa2017.posn.or.th/>                                        |
| 13           | [International Geometry Olympiad](../Page/International_Geometry_Olympiad.md "wikilink")                                                                                                 | IGO     | since 2014 | <http://igo-official.ir/?lang=en>                                     |
| 14           | [International Experimental Physics Olympiad](../Page/International_Experimental_Physics_Olympiad.md "wikilink")                                                                         | IEPhO   | since 2013 | <http://www.iepho.com>                                                |
| 15           | [International Foxford Olympiad](../Page/International_Foxford_Olympiad.md "wikilink")                                                                                                   | IFO     | since 2015 | <http://foxford.com>                                                  |
| 16           | [International Medicine Olympiad](../Page/International_Medicine_Olympiad.md "wikilink")                                                                                                 | IMDO    | since 2016 | <https://www.usmdo.org/>                                              |
| 17           | [International Nanotechnology Olympiad](../Page/International_Nanotechnology_Olympiad.md "wikilink")                                                                                     | INO     | since 2017 | <http://nanoolympiad.org/>                                            |
| 18           | [International Olympiad in Physics, Technology and Astronomy](../Page/International_Olympiad_in_Physics,_Technology_and_Astronomy.md "wikilink")                                         | IOPTA   | since 2017 | <http://iopta.com>                                                    |
| 19           | [International Olympiad in Biology and Astronomy](../Page/International_Olympiad_in_Biology_and_Astronomy.md "wikilink")                                                                 | IOBA    | since 2017 | <http://ioba.com>                                                     |
| Project Base |                                                                                                                                                                                          |         |            |                                                                       |
| 1            | [International Environmental Project Olympiad](../Page/International_Environmental_Project_Olympiad.md "wikilink")                                                                       | INEPO   | since 1993 | <http://www.inepo.com/>                                               |
| 2            | [International Young Inventors Project Olympiad](../Page/International_Young_Inventors_Project_Olympiad.md "wikilink")                                                                   | IYIPO   | since 2007 | <https://web.archive.org/web/20160307085233/http://iyipo.ge/eng>      |
| 3            | [International Sustainable World Engineering Energy Environment Project olympiad](../Page/International_Sustainable_World_Engineering_Energy_Environment_Project_olympiad.md "wikilink") | ISWEEEP | since 2008 | <http://isweeep.org/>                                                 |
| 4            | [International Environment and Sustainability Project Olympiad](../Page/International_Environment_and_Sustainability_Project_Olympiad.md "wikilink")                                     | INESPO  | since 2010 | <http://www.inespo.org/>                                              |
| 5            | [Global Environmental Issues and Us Olympiad](../Page/Global_Environmental_Issues_and_Us_Olympiad.md "wikilink")                                                                         | GENIUS  | since 2011 | <https://geniusolympiad.org/>                                         |
| 6            | [Golden Climate International Environmental Project Olympiad](../Page/Golden_Climate_International_Environmental_Project_Olympiad.md "wikilink")                                         | GCIEPO  | since 2011 | <http://www.goldenclimate.com/>                                       |

### Regional

#### Asian Olympiad List

| Number       | Science                                                                                                                     | Symbol | Year       | Web                                                                     |
| ------------ | --------------------------------------------------------------------------------------------------------------------------- | ------ | ---------- | ----------------------------------------------------------------------- |
| Exam Base    |                                                                                                                             |        |            |                                                                         |
| 1            | [Asian Pacific Mathematics Olympiad](../Page/Asian_Pacific_Mathematics_Olympiad.md "wikilink")                              | APMO   | since 1989 | <http://www.apmo-official.org/>                                         |
| 2            | [Asian Physics Olympiad](../Page/Asian_Physics_Olympiad.md "wikilink")                                                      | APhO   | since 2000 | <http://apho.ias-ntu.org/>                                              |
| 3            | [Asian-Pacific Astronomy Olympiad](../Page/International_Astronomy_Olympiad#Asian-Pacific_Astronomy_Olympiad.md "wikilink") | APAO   | since 2005 | <http://www.issp.ac.ru/iao/apao/>                                       |
| 4            | [Asia-Pacific Informatics Olympiad](../Page/Asia-Pacific_Informatics_Olympiad.md "wikilink")                                | APIO   | since 2007 | <http://apio-olympiad.org/>                                             |
| 5            | [Asian Science and Mathematics Olympiad](../Page/Asian_Science_and_Mathematics_Olympiad.md "wikilink")                      | ASMO   | since 2010 | <http://asmo2u.com/>                                                    |
| 6            | [Southeast Asian Mathematical Olympiad](../Page/Southeast_Asian_Mathematical_Olympiad.md "wikilink")                        | SEAMO  | since 2016 | <http://www.seamo-official.org/>                                        |
| Project Base |                                                                                                                             |        |            |                                                                         |
| 1            | [Euroasia Environmental Project Olympiad](../Page/Euroasia_Environmental_Project_Olympiad.md "wikilink")                    | INEPO  | since 2007 | <https://web.archive.org/web/20160306185833/http://inepo-euroasia.com/> |

#### European Olympiad List

| Number       | Science                                                                                                            | Symbol  | Year       | Web                                                                     |
| ------------ | ------------------------------------------------------------------------------------------------------------------ | ------- | ---------- | ----------------------------------------------------------------------- |
| Exam Base    |                                                                                                                    |         |            |                                                                         |
| 1            | [Balkan Mathematical Olympiad](../Page/Balkan_Mathematical_Olympiad.md "wikilink")                                 | BMO     | since 1984 |                                                                         |
| 2            | [Balkan Olympiad in Informatics](../Page/Balkan_Olympiad_in_Informatics.md "wikilink")                             | BOI     | since 1993 |                                                                         |
| 3            | [Central European Informatics Olympiad](../Page/Central_European_Informatics_Olympiad.md "wikilink")               | CEOI    | since 1995 | <http://ceoi.inf.elte.hu/>                                              |
| 4            | [Junior Balkan Mathematical Olympiad](../Page/Junior_Balkan_Mathematical_Olympiad.md "wikilink")                   | JBMO    | since 1997 |                                                                         |
| 5            | [European Union Science Olympiad](../Page/European_Union_Science_Olympiad.md "wikilink")                           | EUSO    | since 2003 | <http://euso.eu/>                                                       |
| 6            | [South Eastern European Mathematical Olympiad](../Page/South_Eastern_European_Mathematical_Olympiad.md "wikilink") | SEEMOUS | since 2007 | <http://www.massee-org.eu/>                                             |
| 7            | [Junior Balkan Olympiad in Informatics](../Page/Junior_Balkan_Olympiad_in_Informatics.md "wikilink")               | JBOI    | since 2007 |                                                                         |
| 8            | [Middle European Mathematical Olympiad](../Page/Middle_European_Mathematical_Olympiad.md "wikilink")               | MEMO    | since 2007 |                                                                         |
| 9            | [European Girls' Mathematical Olympiad](../Page/European_Girls'_Mathematical_Olympiad.md "wikilink")               | EGMO    | since 2012 | <https://www.egmo.org/>                                                 |
| 10           | [European Junior Olympiad in Informatics](../Page/European_Junior_Olympiad_in_Informatics.md "wikilink")           | EJOI    | since 2017 | <http://ejoi.org/>                                                      |
| 11           | [European Physics Olympiad](../Page/European_Physics_Olympiad.md "wikilink")                                       | EuPhO   | since 2017 | <http://eupho.ut.ee/>                                                   |
| Project Base |                                                                                                                    |         |            |                                                                         |
| 1            | [Euroasia Environmental Project Olympiad](../Page/Euroasia_Environmental_Project_Olympiad.md "wikilink")           | INEPO   | since 2007 | <https://web.archive.org/web/20160306185833/http://inepo-euroasia.com/> |

#### African Olympiad List

| Number       | Science                                                                                      | Symbol | Year       | Web |
| ------------ | -------------------------------------------------------------------------------------------- | ------ | ---------- | --- |
| Exam Base    |                                                                                              |        |            |     |
| 1            | [Pan-African Mathematics Olympiads](../Page/Pan-African_Mathematics_Olympiads.md "wikilink") | PAMO   | since 1987 |     |
| Project Base |                                                                                              |        |            |     |

#### American Olympiad List

| Number       | Science                                                                                      | Symbol | Year       | Web |
| ------------ | -------------------------------------------------------------------------------------------- | ------ | ---------- | --- |
| Exam Base    |                                                                                              |        |            |     |
| 1            | [American Mathematics Competitions](../Page/American_Mathematics_Competitions.md "wikilink") | AMC    | since 1950 |     |
| Project Base |                                                                                              |        |            |     |

## 参考文献

## 外部链接

  - [International Science Olympiads](http://olympiads.win.tue.nl/)
  - [特別資優學生培育支援計劃](https://web.archive.org/web/20070518064829/http://gifted.hkedcity.net/Gifted/main.html)（香港）

## 参见

  - [全国中学生学科奥林匹克竞赛](../Page/全国中学生学科奥林匹克竞赛.md "wikilink")

{{-}}

[国际科学奥林匹克竞赛](../Category/国际科学奥林匹克竞赛.md "wikilink")