**國際自然保護聯盟瀕危物種紅色名錄**（或稱**紅色名錄**，簡稱**紅皮書**）於1963年開始編製，是全球[動](../Page/動物.md "wikilink")[植物物種](../Page/植物.md "wikilink")[保護現狀最全面的名錄](../Page/保護現狀.md "wikilink")。此名錄由[國際自然保護聯盟編製及維護](../Page/國際自然保護聯盟.md "wikilink")。

紅色名錄是根據嚴格準則去評估數以千計[物種及](../Page/物種.md "wikilink")[亞種的](../Page/亞種.md "wikilink")[絕種風險所編製而成的](../Page/絕種.md "wikilink")。準則是根據物種及地區釐定，旨在向公眾及決策者反映保育工作的迫切性，並協助國際社會避免物種滅絕。

主要的物種評估機構有[國際鳥盟](../Page/國際鳥盟.md "wikilink")、[世界保护监测中心及](../Page/世界保护监测中心.md "wikilink")辖下[物種存續委員會內的專家團體](../Page/物種存續委員會.md "wikilink")。總括而言，上述團體所評估的物種數目佔整個紅色名錄接近一半。

紅色名錄被認定為對[生物多樣性狀況最具權威的指標](../Page/生物多樣性.md "wikilink")。

## 公佈

[2004_IUCN_Red_List_Cover.gif](https://zh.wikipedia.org/wiki/File:2004_IUCN_Red_List_Cover.gif "fig:2004_IUCN_Red_List_Cover.gif")
2008年更新的紅色名錄。估計總共物種數量有61,259種，評估了總共26,604種物種及2,160亞種（）、變種（）、族系（）、亞族（）。

所有已評估物種中，共有16,928種被視為受威脅，當中有8,462種為[動物](../Page/動物.md "wikilink")，8,457種為[植物](../Page/植物.md "wikilink")，9種為[地衣及](../Page/地衣.md "wikilink")[菇類](../Page/菇類.md "wikilink")。

名錄中公佈了共804種自公元1500年起[絕種的物種](../Page/絕種.md "wikilink")。與2000年公佈的766種比較，增加了38種。

每年都有少量的「絕種」物種再次被發現，被撥入[拉撒路物種](../Page/拉撒路物種.md "wikilink")（）（這是個古生物學名詞，指化石記錄中忽隱忽現的物種），或撥入數據缺乏（）級別之內。於2002年，絕種物種數量減少至759種，但後來又回升了。

## 保護級別

2001年，Mace和Lande提出并制定了Mace-Lande物种濒危等级标准(IUCN3.1)，IUCN物种生存委员会在此基础上经过反复修改，形成了《IUCN物种红色名录濒危等级和标准》(*Red
List Categories and Criteria of Endangered Species*)\[1\]。

物種被分類入9個級別，根據數目下降速度、物種總數、地理分佈、群族分散程度等準則分類。詳見[保護現狀的描述](../Page/保護現狀.md "wikilink")。
[Status_iucn3.1.svg](https://zh.wikipedia.org/wiki/File:Status_iucn3.1.svg "fig:Status_iucn3.1.svg")

  - **[絕滅](../Page/絕滅.md "wikilink")**（）
  - **[野外絕滅](../Page/野外絕滅.md "wikilink")**（）
  - **[極危](../Page/極危物種.md "wikilink")**（）
  - **[瀕危](../Page/瀕危物種.md "wikilink")**（）
  - **[易危](../Page/易危物種.md "wikilink")**（）
  - **[近危](../Page/近危物種.md "wikilink")**（）
  - **[無危](../Page/無危物種.md "wikilink")**（）
  - **[數據缺乏](../Page/數據缺乏.md "wikilink")**（）
  - **[未評估](../Page/未評估.md "wikilink")**（）

[Status_iucn2.3.svg](https://zh.wikipedia.org/wiki/File:Status_iucn2.3.svg "fig:Status_iucn2.3.svg")
1994年所訂立的準則只有8個級別(IUCN2.3)，低危（）級別下再有3個子分類：近危（）、無危（）、保護依賴（），2001年準則更改，將上述組別歸入近危（）級別。

當討論紅色名錄，「受威脅」（"）一詞是官方指定為以下3個級別的總稱： 極危（）、瀕危（）及易危（）。

## 相關條目

  - [國際自然保護聯盟](../Page/國際自然保護聯盟.md "wikilink")
  - [保護現狀](../Page/保護現狀.md "wikilink")
  - [已滅絕動物列表](../Page/已滅絕動物列表.md "wikilink")
  - [中国保护植物红皮书](../Page/中国保护植物红皮书.md "wikilink")
  - [中國國家重點保護野生植物名錄](../Page/中國國家重點保護野生植物名錄.md "wikilink")
  - [国家重点保护野生动物名录](../Page/国家重点保护野生动物名录.md "wikilink")

## 参考文献

### 引用

### 来源

  - IUCN, 2008.
    全球受威脅物種評論梗概（[1](https://web.archive.org/web/20090306194130/http://www.iucnredlist.org/documents/2008RL_stats_table_3a_v1223294385.pdf)，[2](https://web.archive.org/web/20090320040141/http://www.iucnredlist.org/documents/2008RL_stats_table_3b_v1223294385.pdf)

  - IUCN, 2001.
    [IUCN红色名录級別及準則（3.1版）](http://www.iucnredlist.org/technical-documents/categories-and-criteria/2001-categories-criteria)
    （[PDF文件](http://www.iucnredlist.org/documents/redlist_cats_crit_en.pdf)）

  - IUCN, 1994.
    [IUCN红色名录級別及準則（2.3版）](https://web.archive.org/web/20060718222843/http://www.iucnredlist.org/info/categories_criteria1994)

  - Rodrigues, A.S.L., Pilgrim, J.D., Lamoreaux, J.L., Hoffmann, M. &
    Brooks, T.M. 2006. The value of the IUCN Red List for conservation.
    Trends in Ecology & Evolution 21(2): 71-76.

## 外部連結

  - [紅色名錄](http://www.iucnredlist.org/)
  - [The IUCN Red List: A Barometer of
    Life](https://www.youtube.com/watch?v=VukyqMajAOU&feature=youtu.be)

[Category:生態學](../Category/生態學.md "wikilink")
[Category:IUCN红色名录](../Category/IUCN红色名录.md "wikilink")
[Category:自然保育](../Category/自然保育.md "wikilink")
[Category:红皮书](../Category/红皮书.md "wikilink")

1.