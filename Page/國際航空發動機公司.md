[Aero_Flight_Airbus_A320.jpg](https://zh.wikipedia.org/wiki/File:Aero_Flight_Airbus_A320.jpg "fig:Aero_Flight_Airbus_A320.jpg")\]\]
**國際航空發動機公司**（International Aero Engines
AG），縮寫為**IAE**，是一間在[瑞士](../Page/瑞士.md "wikilink")[蘇黎世登記的](../Page/蘇黎世.md "wikilink")[合資公司](../Page/合資公司.md "wikilink")，於1983年創立。主要的股東有：

  - [美國](../Page/美國.md "wikilink")[普惠](../Page/普惠公司.md "wikilink")（Pratt
    & Whitney）：32.5%
  - [英國](../Page/英國.md "wikilink")[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")（Rolls-Royce
    PLC）：32.5%
  - [德國](../Page/德國.md "wikilink")[MTU航空發動機](../Page/MTU航空發動機公司.md "wikilink")（MTU
    Aero Engines）：12%
  - [日本](../Page/日本.md "wikilink")[日本航空發動機](../Page/日本航空發動機公司.md "wikilink")（Japanese
    Aero Engines）：23%

飛雅特-{航天}-（FiatAvio）曾為主要股東之一，但在公司創立初期即撤資，不過現在的飛雅特-{航天}-在改名為Avio
S.p.A.後仍為一個供應商。產品名字裡的「[V](../Page/羅馬數字.md "wikilink")」是主要股東仍為五家時的遺留產物。

IAE曾為[空中巴士A340開發](../Page/空中巴士A340.md "wikilink")「超級扇（Superfan）」發動機。IAE現在的主要業務為發展、生產、和售後服務[V2500系列航空發動機](../Page/國際航空發動機V2500.md "wikilink")。V2500系列為IAE的唯一產品，由[空中巴士](../Page/空中巴士.md "wikilink")[A320系列家族和](../Page/空中巴士A320.md "wikilink")[麥克唐納·道格拉斯](../Page/麥克唐納·道格拉斯.md "wikilink")[MD-90系統客機採用](../Page/麥道MD-90.md "wikilink")。

## 外部連結

  - [IAE官方網站(英)](http://www.i-a-e.com/)
  - [普惠公司(英)](http://www.pratt-whitney.com/prod_comm_v2500.asp)
  - [勞斯萊斯公司(英)](http://www.rolls-royce.com/civil_aerospace/products/airlines/v2500/default.jsp)
  - [MTU航空發動機公司(英)](https://web.archive.org/web/20070103151529/http://www.mtu.de/en/geschaeftsbereiche/instand_zivil/programme/v2500/index.html)

[Category:飛行器發動機製造商](../Category/飛行器發動機製造商.md "wikilink")
[Category:苏黎世公司](../Category/苏黎世公司.md "wikilink")
[Category:1983年成立的公司](../Category/1983年成立的公司.md "wikilink")