**圓周**是指[圓或類似形狀的](../Page/圓.md "wikilink")[周長](../Page/周長.md "wikilink")。

圓周和數學上重要的[數學常數](../Page/數學常數.md "wikilink")[π有關](../Page/Pi.md "wikilink")。若定義圓周為\(C\)，[半徑為](../Page/半徑.md "wikilink")\(r\)，[直徑為](../Page/直徑.md "wikilink")\(d\)，圓周長和直徑的比值即為\(\pi\)\[1\]：

\[{C}=\pi{d}=2\pi r.\]

\(\pi\)的數值是3.14159 26535 89793 ... （參照）。 [
圖中黑色的為圓周（C），
淺藍色的是直徑（D），
紅色是半徑（R），
洋紅色的是圓心。
圓周=  × 直徑 = 2 ×  ×
半徑](https://zh.wikipedia.org/wiki/File:Circle-withsegments.svg "fig: 圖中黑色的為圓周（C）， 淺藍色的是直徑（D）， 紅色是半徑（R）， 洋紅色的是圓心。 圓周=  × 直徑 = 2 ×  × 半徑")
{{-}}
[2pi-unrolled.gif](https://zh.wikipedia.org/wiki/File:2pi-unrolled.gif "fig:2pi-unrolled.gif")為1，其圓周為2\]\]
[Pi-unrolled-720.gif](https://zh.wikipedia.org/wiki/File:Pi-unrolled-720.gif "fig:Pi-unrolled-720.gif")為1，其圓周為\]\]
{{-}} [數學常數π常用在數學](../Page/數學常數.md "wikilink")、工程學及科學中。

另一個和圓周有關的比例是圓周和半徑的比值\(\tfrac{C}{r} = 2\pi\)，雖然沒有正式的名稱，但也常用在[弧度及](../Page/弧度.md "wikilink")[物理常數等許多場合中](../Page/物理常數.md "wikilink")。

## 椭圓的周長

[椭圓也可以計算其圓周](../Page/椭圓.md "wikilink")，但需要透過[第二類完全橢圓積分才能表示](../Page/橢圓積分#第二類完全橢圓積分.md "wikilink")。

## 图论中的周長

在[图论中](../Page/图论.md "wikilink")，一个的[图周長是指其中所含的最长的](../Page/图.md "wikilink")[环](../Page/环_\(图论\).md "wikilink")。

## 相關條目

  - [弧长](../Page/弧长.md "wikilink")
  - [等周定理](../Page/等周定理.md "wikilink")
  - [圓周運動](../Page/圓周運動.md "wikilink")

## 參考資料

[Category:几何量测量](../Category/几何量测量.md "wikilink")
[Category:圆](../Category/圆.md "wikilink")

1.  [圓周率概說](http://web2.nmns.edu.tw/Web-Title/china/A-2-2_display.htm)