在[數學裡](../Page/數學.md "wikilink")，**圓群**標記為**T**，為所有模為1之[複數所組成的乘法](../Page/複數.md "wikilink")[群](../Page/群.md "wikilink")，即在複數平面上的[單位圓](../Page/單位圓.md "wikilink")。

\[\mathbb T = \{ z \in \mathbb C : |z| = 1 \}.\]
圓群為所有非零複數所組成之乘法群**C**<sup>×</sup>的[子群](../Page/子群.md "wikilink")。由于**C**<sup>×</sup>[可交換](../Page/阿貝爾群.md "wikilink")，**T**也是可交換的。

圓群的符號**T**源自於**T**<sup>*n*</sup>（*n*個**T**的[直積](../Page/直積.md "wikilink")）幾何上是個*n*-[環面的此一事實](../Page/環面.md "wikilink")。而圓群即正是一個1-環面。

## 基本介紹

[Circle-group.svg](https://zh.wikipedia.org/wiki/File:Circle-group.svg "fig:Circle-group.svg")

思考圓群的一種方法是描述其「角度」如何相加，其中只有0至360度的角度是被允許的。例如，右邊的圖表描述著如何將150度加上270度。其答案應該是150度+270度=420度，但以圓群的觀點來考慮，而必須要「忘記」掃過一整個圓的事實。因此，必須以360度來調整其答案，如此將會得出420度−360度=60度之答案。

另一種描述方法是使用原本的加法，但數字只限定在0和1之間。要完成此一描述，必須丟掉小數點前的數位。例如，當在算0.784+0.925+0.446時，其答案應該是2.155，但這裡必須丟掉前面的2，因此其答案（在圓群中）會是0.155。

## 拓撲與解析結構

圓群不只是一個抽象代數群而已。當將其視為複數平面的[子空間時](../Page/子空間拓撲.md "wikilink")，其會有一個自然的[拓撲](../Page/拓撲空間.md "wikilink")。因為乘法和反演是在**C**<sup>×</sup>上的[連續函數](../Page/連續函數_\(拓撲學\).md "wikilink")，圓群會有一[拓撲群的結構](../Page/拓撲群.md "wikilink")。更甚地，當單位圓是複數平面上的一個[閉子集時](../Page/閉集.md "wikilink")，圓群也會是**C**<sup>×</sup>（其自身被視為是一拓撲群）的閉子群。

更多地，因為圓是一個一維實[流形且其乘法和反演為圓上的圓變映射](../Page/流形.md "wikilink")，這給了圓群一個一維[李群的結構](../Page/李群.md "wikilink")。實際上，以同構來分，其為唯一的一個同構於**T**<sup>*n*</sup>的一維[緊緻](../Page/緊集.md "wikilink")[連通李群](../Page/連通空間.md "wikilink")

## 同構

圓群在數學裡可承現出很多種不同的類型。下面列出較常見的幾種類型，並證明

\[\mathbb T \cong \mbox{U}(1) \cong \mbox{SO}(2) \cong \mathbb R/\mathbb Z.\,\]

由所有一階[酉矩陣](../Page/酉矩陣.md "wikilink")（即單位複數）所組成之群顯然與圓群相對應；其酉的條件即等價於其元素的模為1的條件。因此圓群會同構於第一個[酉群U](../Page/酉群.md "wikilink")(1)。

[指數函數會產生一個由實數加法群](../Page/指數函數.md "wikilink")**R**映射至圓群**T**上之[群同態exp](../Page/群同態.md "wikilink"):**R**→**T**，其映射為

\[\theta \mapsto e^{i\theta} = \cos\theta + i\sin\theta\]

其最後一個等式為[欧拉公式](../Page/欧拉公式.md "wikilink")。實數*θ*會對應到單位圓上由正*x*軸量起的角度。這個映射是一個同態，因為單位複數的乘法可以對應到角度的加法上：

\[e^{i\theta_1}e^{i\theta_2} = e^{i(\theta_1+\theta_2)}\]

此一指數映射很明顯地是一個由**R**映射至**T**的[滿射函數](../Page/滿射.md "wikilink")，但它不是[單射](../Page/單射.md "wikilink")。這個映射的[核為所有](../Page/核_\(數學\).md "wikilink")*2π*[整數倍之集合](../Page/整數.md "wikilink")。基於[第一同構定理](../Page/同構定理.md "wikilink")，會有著

\[\mathbb T \cong \mathbb R/2\pi\mathbb Z\]

調整一下尺度後，也可以說**T**同構於**R**/**Z**。

若將複數視為二階實[矩陣](../Page/矩陣.md "wikilink")（見[複數](../Page/複數.md "wikilink")），單位複數則會對應至有單位[行列式的二階](../Page/行列式.md "wikilink")[正交矩陣上](../Page/正交矩陣.md "wikilink")。具體地說，會有如下之對應關係

\[e^{i\theta} \leftrightarrow \exp\left(\theta\begin{bmatrix}
    0 & -1 \\
    1 & 0
  \end{bmatrix}\right) = \begin{bmatrix}
\cos \theta & -\sin \theta \\
\sin \theta & \cos \theta \\
\end{bmatrix} = \cos{\theta}\begin{bmatrix}
    1 & 0 \\
    0 & 1
  \end{bmatrix}
  +\sin{\theta}\begin{bmatrix}
    0 & -1 \\
    1 & 0
  \end{bmatrix}\]

圓群因此會同構於[特殊正交群SO](../Page/特殊正交群.md "wikilink")(2)。此處有著一個單位複數之乘法的幾何解釋，即為複數平面上的旋轉，並且任何旋轉都可表達成這種形式。

## 性質

任何大於0之維度的緊緻李群*G*都會有一個會同構於圓群的[子群](../Page/子群.md "wikilink")。這是指以[對稱的觀點來思考](../Page/對稱.md "wikilink")，一「連續」作用的緊緻對稱群可以被表示成有一作用著的單參數圓子群；其在物理系統上的結果可以有如[旋轉不變性和](../Page/旋轉不變性.md "wikilink")[自發性對稱破壞等例子](../Page/自發性對稱破壞.md "wikilink")。

圓群有許多個[子群](../Page/子群.md "wikilink")，但其純[緊緻子群只由](../Page/緊集.md "wikilink")[單位根所構成](../Page/單位根.md "wikilink")。

## 表示

圓群的[表示是很容易描述的](../Page/群表示.md "wikilink")。[舒爾引理描述說一個阿貝爾群的所有](../Page/舒爾引理.md "wikilink")[不可約](../Page/不可約表示.md "wikilink")[複表示都是一維的](../Page/複數.md "wikilink")。圓群是緊緻的，任一表示\(\rho\colon\mathbb T\to \mathrm{GL}_1(\mathbb C) \cong \mathbb C^{\times}\)都必須在\(\mathrm{U}(1)\cong\mathbb T\)內取值。因此，圓群的不可約表示只是個由圓群映射至其本身的同態。每一個如此的同態都會有下面的形式

\[\phi_n(e^{i\theta}) = e^{in\theta},\qquad n\in\mathbb Z.\]
這些表示都是等價的。表示\(\phi_{-n}\)
[共軛於](../Page/共軛表示.md "wikilink")\(\phi_n\)

\[\phi_{-n} = \overline{\phi_n}.\]

這些表示都只是圓群的[特徵標](../Page/特徵標.md "wikilink")。而**T**的[特徵標群明顯為由](../Page/特徵標群.md "wikilink")\(\phi_1\)所產生之[無限循環群](../Page/循環群.md "wikilink")：

\[\mathrm{Hom}(\mathbb T,\mathbb T) \cong \mathbb Z.\]

圓群的不可約[實數表示為](../Page/實數.md "wikilink")（一維的）[當然表示](../Page/當然表示.md "wikilink")，且其表示

\[\rho_n(e^{i\theta}) = \begin{bmatrix}
\cos n\theta & -\sin n\theta \\
\sin n\theta & \cos n\theta \\
\end{bmatrix},\quad n\in\mathbb Z^{+}.\]
的值在SO(2)內。這裡只有正整數*n*，因為表示\(\rho_{-n}\)會等價於\(\rho_n\)。

## 代數結構

在此一章節中將不提及圓群的拓撲結構，而只專注於其代數結構。

圓群**T**是一個[可除群](../Page/可除群.md "wikilink")。其[撓子群是由所有](../Page/撓子群.md "wikilink")*n*次[單位根所組成之集合](../Page/單位根.md "wikilink")，且會同構於**Q**/**Z**。可除群的結構定理表示**T**會同構於**Q**/**Z**和一串**Q**的[直積](../Page/直積.md "wikilink")。這一串**Q**的數目必須為*c*（[連續勢](../Page/連續勢.md "wikilink")）為了使直積的勢會是正確的。但*c*個**Q**的直積會同構於**R**，**R**如同是在**Q**上的*c*維[向量空間](../Page/向量空間.md "wikilink")。因此

\[\mathbb T \cong \mathbb R \oplus (\mathbb Q / \mathbb Z).\,\]

同構

\[\mathbb C^\times \cong \mathbb R \oplus (\mathbb Q / \mathbb Z)\]

也可以以同樣的方式證明，因為**C**<sup>×</sup>也是其撓子群和**T**的撓子群相同的可除阿貝爾群。

## 另見

  - [環面](../Page/環面.md "wikilink")
  - [單參數子群](../Page/單參數子群.md "wikilink")
  - [酉群](../Page/酉群.md "wikilink")
  - [正交群](../Page/正交群.md "wikilink")

[U](../Category/群論.md "wikilink") [U](../Category/拓撲群.md "wikilink")
[U](../Category/李群.md "wikilink")