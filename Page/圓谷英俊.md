**圆谷英俊**（つぶらや ひでとし、Tsuburaya
Hidetosi）是一名出身自[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")[磯子區的](../Page/磯子區.md "wikilink")[職棒選手](../Page/職棒.md "wikilink")，隸屬於[讀賣巨人隊](../Page/讀賣巨人.md "wikilink")，擔任[內野手](../Page/內野手.md "wikilink")。身高181[公分](../Page/公分.md "wikilink")，體重75[公斤](../Page/公斤.md "wikilink")，攻守時的用手習慣是右投左打。

## 經歷

圆谷在就讀橫濱市立洋光台第一小學一年級時，首次參加與學校的棒球隊，並擔任[投手](../Page/投手.md "wikilink")、內野手，逐漸開始接觸棒球比賽。在其升讀橫濱高中及大學的過程中，一直參與校內的棒球活動，並曾在第83屆全國高等學校棒球選手資格大賽中出場比賽。畢業後進軍職棒世界，在2006年正式簽約讀賣巨人，以年薪1200萬[日元受聘](../Page/日元.md "wikilink")，並曾獲監督[原辰德的點名鼓勵](../Page/原辰德.md "wikilink")。2007年成為球隊的重點新人。其球技風格是著重廣角揮擊的進攻，與及華麗守備能力，集走、攻、守於一身的全面選手。其球隊號碼是「**32**」號。

### 球衣背號

  - **32** （2007年 - 2009年）
  - **52** （2010年 - ）

### 個人記錄

  - 首次出賽：2008年4月15日、對[中日龍](../Page/中日龍.md "wikilink")4回戰（[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")）、8局下接替[三壘手出場](../Page/三壘手.md "wikilink")
  - 首次打點：2009年8月18日、對[横濱灣星](../Page/横濱灣星.md "wikilink")16回戰（[東京巨蛋](../Page/東京巨蛋.md "wikilink")）、6局下[高崎健太郎二壘内野記録](../Page/高崎健太郎.md "wikilink")
  - 首次安打・首次本壘打：同上、8局下[加藤武治右外野](../Page/加藤武治.md "wikilink")3分彈

## 成績

<table>
<thead>
<tr class="header">
<th><p>年度|</p></th>
<th><p>球團|</p></th>
<th><p>試合|</p></th>
<th><p>打<br />
席|</p></th>
<th><p>打<br />
數|</p></th>
<th><p>得分|</p></th>
<th><p>安打|</p></th>
<th><p>二<br />
壘<br />
打|</p></th>
<th><p>三<br />
壘<br />
打|</p></th>
<th><p>本<br />
壘<br />
打|</p></th>
<th><p>壘<br />
打<br />
數|</p></th>
<th><p>打點|</p></th>
<th><p>盜壘|</p></th>
<th><p>盜<br />
壘<br />
刺|</p></th>
<th><p>觸擊|</p></th>
<th><p>犧<br />
牲<br />
打|</p></th>
<th><p>保送|</p></th>
<th><p>敬遠|</p></th>
<th><p>觸身|</p></th>
<th><p>三振|</p></th>
<th><p>雙殺|</p></th>
<th><p>打<br />
擊<br />
率|</p></th>
<th><p>上<br />
壘<br />
率|</p></th>
<th><p>長<br />
打<br />
率|</p></th>
<th><p>OPS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/讀賣巨人.md" title="wikilink">巨人</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.000</p></td>
<td><p>.000</p></td>
<td><p>.000</p></td>
<td><p>.000</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.500</p></td>
<td><p>.500</p></td>
<td><p>2.000</p></td>
<td><p>2.500</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>----</p></td>
<td><p>----</p></td>
<td><p>----</p></td>
<td><hr /></td>
<td></td>
</tr>
<tr class="even">
<td><p>通算：3年</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.250</p></td>
<td><p>.250</p></td>
<td><p>1.000</p></td>
<td><p>1.250</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 2010年度結束時
  - 各年度粗體字為該聯盟最高

## 外部連結

  - [Japanese
    Baseball：円谷英俊](http://www.japanesebaseball.com/players/player.jsp?PlayerID=2224)

[Category:1984年出生](../Category/1984年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[T](../Category/日本棒球選手.md "wikilink")
[T](../Category/神奈川縣出身人物.md "wikilink")
[T](../Category/橫濱市出身人物.md "wikilink")
[T](../Category/讀賣巨人隊球員.md "wikilink")