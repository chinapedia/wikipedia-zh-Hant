**圓頭龍屬**（[學名](../Page/學名.md "wikilink")：*Sphaerotholus*）是[厚頭龍科](../Page/厚頭龍科.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，化石在[美國西部發現](../Page/美國.md "wikilink")，年代屬於[白堊紀晚期](../Page/白堊紀.md "wikilink")。現時已有兩個[物種被描述](../Page/物種.md "wikilink")、命名，分別是在[新墨西哥州](../Page/新墨西哥州.md "wikilink")[嘉德蘭組發現的](../Page/嘉德蘭組.md "wikilink")[古德溫圓頭龍](../Page/古德溫圓頭龍.md "wikilink")（*S.
goodwini*），及從[蒙大拿州](../Page/蒙大拿州.md "wikilink")[海爾河組發現的](../Page/海爾河組.md "wikilink")[布仕郝利茨圓頭龍](../Page/布仕郝利茨圓頭龍.md "wikilink")（*S.
buchholtzae*）。

圓頭龍的學名是由[希臘文的](../Page/希臘文.md "wikilink")「球」及「圓頂」組成，是指牠那圓拱特徵的[頭顱骨](../Page/頭顱骨.md "wikilink")。圓頭龍的生存年代從晚[坎帕階的新墨西哥州至晚](../Page/坎帕階.md "wikilink")[麥斯特裡希特階的蒙大拿州](../Page/麥斯特裡希特階.md "wikilink")，顯示牠們生存了一段很長的時間（約7-8百萬年），而且分佈廣泛。圓頂龍被認為是高度進化的厚頭龍科。

## 古德溫圓頭龍

[模式種](../Page/模式種.md "wikilink")[古德溫圓頭龍的](../Page/古德溫圓頭龍.md "wikilink")[正模標本](../Page/正模標本.md "wikilink")（編號NMMNH
P-27403）包含了一個不完整的[頭顱骨](../Page/頭顱骨.md "wikilink")，缺乏了面部及上頜部份。從後方來看，古德溫圓頭龍的一個頂鱗骨棒的寬度，往深處逐漸變窄，較布仕郝利茨圓頭龍的還窄；[鱗狀骨之間的](../Page/鱗狀骨.md "wikilink")[頂骨段落](../Page/頂骨.md "wikilink")，成為薄片的形狀\[1\]。古德溫圓頭龍的[種小名是為紀念](../Page/種小名.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[馬克·古德溫](../Page/馬克·古德溫.md "wikilink")（Mark
Goodwin）在[厚頭龍下目的貢獻而起的](../Page/厚頭龍下目.md "wikilink")。第二個被指歸類於古德溫圓頭龍的標本（編號NMMNH
P-30068），是從[嘉德蘭組的較年輕地層發現](../Page/嘉德蘭組.md "wikilink")。第二個標本包含一個鱗狀骨碎片、一個接近完整的[齒骨及其他無法鑑定的頭顱骨部份](../Page/齒骨.md "wikilink")。

## 布仕郝利茨圓頭龍

[布仕郝利茨圓頭龍的](../Page/布仕郝利茨圓頭龍.md "wikilink")[正模標本](../Page/正模標本.md "wikilink")（編號TMP
87.113.3）是一個不完整的[頭顱骨](../Page/頭顱骨.md "wikilink")。從後方來看，[鱗狀骨之間的](../Page/鱗狀骨.md "wikilink")[頂骨段落很寬](../Page/頂骨.md "wikilink")，上有一個頂鱗骨瘤。頂鱗骨架的後緣，比[古德溫圓頭龍的更為陡峭](../Page/古德溫圓頭龍.md "wikilink")。頭部兩側的結節比古德溫圓頭龍的更為小，並位於頂鱗骨棒的腹側邊緣之上。在頂鱗骨板的兩側結節，在鱗狀骨上開始縮小，並在眼窩後隆起\[2\]。種名是為紀念布仕郝利茨（Emily
A. Buchholtz），另一位對[厚頭龍下目有貢獻的研究人員](../Page/厚頭龍下目.md "wikilink")。

有研究人員認為布仕郝利茨圓頭龍其實是[埃德蒙頓傾頭龍](../Page/傾頭龍.md "wikilink")（*P.
edmontonensis*）的[次客觀異名](../Page/次客觀異名.md "wikilink")\[3\]\[4\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [*Sphaerotholus* at
    *Thescelosaurus*\!](https://web.archive.org/web/20141007045544/http://www.thescelosaurus.com/pachycephalosauria.htm)
  - [恐龍博物館──圓頭龍](https://web.archive.org/web/20060303200951/http://www.dinosaur.net.cn/museum/Sphaerotholus.htm)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:厚頭龍下目](../Category/厚頭龍下目.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")

1.

2.
3.

4.  [Dinosaurian
    Ungulates](http://www4.ncsu.edu/~rjpatchu/paleobiology/Dino_Osteology/lect10.html)