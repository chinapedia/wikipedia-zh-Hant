**圖帕克·印卡·尤潘基**（[克丘亞語](../Page/克丘亞語.md "wikilink")：Tupaq Inka
Yupanki，[西班牙語](../Page/西班牙語.md "wikilink")：Túpac
Yupanqui。「圖帕克」意為閃光的或超群出眾的；\[1\]「印卡」是王室男子稱號；\[2\]尤潘基是對[印加君王的另一個典雅的稱號](../Page/薩帕·印卡#其他稱號.md "wikilink")。\[3\]），[印加帝國君主](../Page/印加帝國.md "wikilink")（[薩帕·印卡](../Page/薩帕·印卡.md "wikilink")），約於1471年至1493年在位。\[4\]圖帕克·印卡·尤潘基承繼先王[帕查庫特克的帝國基業](../Page/帕查庫特克.md "wikilink")，在位期間大肆征伐，大大地擴充了印加版圖。

## 早年生活

### 受命出征

圖帕克·印卡·尤潘基是[帕查庫特克的其中一名兒子](../Page/帕查庫特克.md "wikilink")（另一位兒子是阿馬魯），\[5\]在王子時代，已投入軍政事務。據《[印卡王室述評](../Page/印卡王室述評.md "wikilink")》的記載，印加王[帕查庫特克在位時](../Page/帕查庫特克.md "wikilink")，為了讓年輕的王子見習用兵之術，就派他陪同卡帕克·尤潘基（帕查庫特克的兄弟）出征西北部的[欽察蘇尤](../Page/欽察蘇尤.md "wikilink")。圖帕克·尤潘基汲取了豐富的戰鬥經驗後，帕查庫特克派他為主將，出征[秘魯西部的](../Page/秘魯.md "wikilink")[奇穆](../Page/奇穆王國.md "wikilink")（[Chimor](../Page/:en:Chimor.md "wikilink")）。經過曠日持久的消耗，[印加軍因有大批增援部隊](../Page/印加.md "wikilink")，圖帕克·尤潘基又以溫和的態度對待奇穆人的酋長，最終令敵方降服。圖帕克·尤潘基在那裡興修宮殿、水渠、糧倉、堡壘等設施，以方便[印加人的管治](../Page/印加人.md "wikilink")，並派官員及士兵留駐，然後班師回朝。\[6\]

### 處理國事

圖帕克·印卡·尤潘基在父親[帕查庫特克晚年時期](../Page/帕查庫特克.md "wikilink")，曾被委任為[印加帝國的共同執政者](../Page/印加帝國.md "wikilink")。\[7\]

## 對外擴張

[Expansion_Imperio_Inca-1-.JPG](https://zh.wikipedia.org/wiki/File:Expansion_Imperio_Inca-1-.JPG "fig:Expansion_Imperio_Inca-1-.JPG")領土擴張圖。其中**褐色**部份是圖帕克·印卡·尤潘基在王子時期所征服；**綠色**部份是圖帕克·印卡·尤潘基任[薩帕·印卡期間所征服](../Page/薩帕·印卡.md "wikilink")。\]\]

### 在西北的開拓

圖帕克·印卡·尤潘基在早年時期，便在西北[欽察蘇尤地區發動征戰](../Page/欽察蘇尤.md "wikilink")（這些戰事亦可能是在[帕查庫特克在位時期進行](../Page/帕查庫特克.md "wikilink")，見上文），佔領[卡薩馬卡](../Page/卡薩馬卡.md "wikilink")（[Cajamarca](../Page/:en:Cajamarca.md "wikilink")）、[圖米萬巴](../Page/圖米萬巴.md "wikilink")（[Tumebamba](../Page/:en:Tumebamba.md "wikilink")）、[基多](../Page/基多.md "wikilink")，把疆土伸延至[厄瓜多爾沿海地帶](../Page/厄瓜多爾.md "wikilink")。他拜訪了位於[利馬附近的](../Page/利馬.md "wikilink")[帕查卡馬克神廟](../Page/帕查卡馬克.md "wikilink")（[Pachacamac](../Page/:en:Pachacamac.md "wikilink")），以慶祝[印加帝國所取得的軍事勝利](../Page/印加帝國.md "wikilink")，並希望得到更多的「神諭」，以借助迷信管理國家。\[8\]

### 南征智利等地

[MapvsInc.JPG](https://zh.wikipedia.org/wiki/File:MapvsInc.JPG "fig:MapvsInc.JPG")地區[普魯毛卡人](../Page/:en:Promaucaes.md "wikilink")（左方）與[印加人](../Page/印加人.md "wikilink")（右方）激戰的情形。\]\]
在南方，圖帕克·印卡·尤潘基派軍越過[的的喀喀湖](../Page/的的喀喀湖.md "wikilink")，佔領了位於[玻利維亞的重鎮](../Page/玻利維亞.md "wikilink")[科恰班巴](../Page/科恰班巴.md "wikilink")、[阿根廷西北部及](../Page/阿根廷.md "wikilink")[智利中北部](../Page/智利.md "wikilink")。[印加軍隊在南進過程中](../Page/印加.md "wikilink")，所遇上過較大的反抗勢力，是位於[智利中部](../Page/智利.md "wikilink")[毛利河](../Page/毛利河.md "wikilink")（[Maule
River](../Page/:en:Maule_River.md "wikilink")）一帶的[普魯毛卡人](../Page/普魯毛卡人.md "wikilink")（[Promaucaes](../Page/:en:Promaucaes.md "wikilink")）。在重要戰役[毛利河之戰](../Page/毛利河之戰.md "wikilink")（[Battle
of the
Maule](../Page/:en:Battle_of_the_Maule.md "wikilink")）中，普魯毛卡人與鄰近部落組成一支約一萬八千至二萬人的軍隊，與[印加五萬大軍互相對峙](../Page/印加.md "wikilink")。在曠日持久的爭持中，[印加人雖多次游說對方歸降](../Page/印加人.md "wikilink")，但無法得逞。在談判無望後，印加軍與普魯毛卡軍展開連續三天的血戰，造成雙方大量折損，只好各自返回駐紮地點。最後，圖帕克·印卡·尤潘基下達命令，確定以毛利河為邊界，在他本人沒有下達任何新命令之前，部隊不許向前征服。至此，印加帝國完成在南方的擴張。\[9\]

## 政務管理

[Sacsahuaman_wall3.jpg](https://zh.wikipedia.org/wiki/File:Sacsahuaman_wall3.jpg "fig:Sacsahuaman_wall3.jpg")──位於[印加帝國國都](../Page/印加帝國.md "wikilink")[庫斯科以北](../Page/庫斯科.md "wikilink")。圖帕克·印卡·尤潘基在先王[帕查庫特克營建基礎上繼續興修](../Page/帕查庫特克.md "wikilink")。\]\]
圖帕克·印卡·尤潘基取得大片征服地後，便著手於政務管理。為了彰顯對[太陽神「因蒂」的崇拜及其個人的豐功偉績](../Page/因蒂.md "wikilink")，他大事興修堡壘、[太陽神廟](../Page/因蒂#對因蒂的崇拜.md "wikilink")、[太陽貞女宮](../Page/太陽貞女宮.md "wikilink")。在社區建設上，他營造王室糧倉及公共糧倉，開鑿大型水渠，墾殖梯田，以穩定政府稅收及人民生活。\[10\]

圖帕克·尤潘基對國都[庫斯科的城堡興修工程特別感興趣](../Page/庫斯科.md "wikilink")。早在其父[帕查庫特克在位時](../Page/帕查庫特克.md "wikilink")，便興建一座位於[萨克塞华曼的堡壘](../Page/萨克塞华曼.md "wikilink")。圖帕克·尤潘基致力放父親的未竟之功，動用大量人手，從高山地區把巨石運過來，然後用巧妙的技術堆砌起來。日後，[西班牙人對這項工程大感詫異](../Page/西班牙.md "wikilink")，一位神父[龐塞·德阿科斯塔](../Page/龐塞·德阿科斯塔.md "wikilink")（[José
de
Acosta](../Page/:en:José_de_Acosta.md "wikilink")）這樣形容：「為完成根據[印卡王命令在科斯科和王國其他地方興建的這類工程](../Page/印加王.md "wikilink")，從王國各地徵調了大批[印第安人](../Page/印第安人.md "wikilink")。這些工程規模非常之大，更令人吃驚的是，他們不使用灰漿，也沒有鐵製或鋼製工具來切割和打磨石料，更沒有機械和工具來遠載石料。儘管如此，石料卻打磨得如此平整精細，許多地方連石塊之間的接縫也看不出來。……許多石塊碩大驚人，如不親眼目睹就難以置信。」\[11\]

## 去世

圖帕克·印卡·尤潘基在[庫斯科大事興建一番之後](../Page/庫斯科.md "wikilink")，便患病去世，由王儲[瓦伊納·卡帕克繼承王位](../Page/瓦伊納·卡帕克.md "wikilink")。[印加人按照傳統](../Page/印加人.md "wikilink")，把圖帕克·尤潘基的遺體塗上防腐劑而成[木乃伊](../Page/木乃伊.md "wikilink")。據[印卡·加西拉索·德拉維加所說](../Page/印卡·加西拉索·德拉維加.md "wikilink")，在1559年時他還能看到這具木乃伊。\[12\]

## 評價

圖帕克·印卡·尤潘基在[印加歷史裡](../Page/印加.md "wikilink")，被視為一位平易和藹的指揮者，並有著出色的軍事本領。他一身致力於開疆拓土，故在他去世時，已把[印加擴展成北抵](../Page/印加.md "wikilink")[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")，南至[智利中部的大帝國](../Page/智利.md "wikilink")。按學者Nigel
Davies的計算，那等如是[亞歷山大帝國的長度](../Page/亞歷山大帝國.md "wikilink")。\[13\]

## 注釋

## 參考文獻

<div class="references-small">

  -
  -
  -

</div>

## 參見

  - [印加帝國](../Page/印加帝國.md "wikilink")
  - [庫斯科](../Page/庫斯科.md "wikilink")
  - [印加宗教](../Page/印加宗教.md "wikilink")
  - [薩帕·印卡](../Page/薩帕·印卡.md "wikilink")

## 外部連結

  -
  -
[Category:印加諸王](../Category/印加諸王.md "wikilink")
[Category:印加文明](../Category/印加文明.md "wikilink")

1.  印卡·加西拉索·德拉維加《印卡王室述評》，463頁。

2.  印卡·加西拉索·德拉維加《印卡王室述評》，76頁。

3.  印卡·加西拉索·德拉維加《印卡王室述評》，130頁。

4.  蒯世𤈇《新編中外歷史大系手冊》，565頁。

5.  Nigel Davies: ***The Ancient Kingdoms of Peru***, P.124.

6.  印卡·加西拉索·德拉維加《印卡王室述評》，中譯本，400-418頁；453-460頁。
    ※《印卡王室述評》裡誤將圖帕克·印卡·尤潘基分成「印卡·尤潘基」和「圖帕克·印卡·尤潘基」來處理。

7.
8.  Nigel Davies: ***The Ancient Kingdoms of Peru***, P.125.

9.  印卡·加西拉索·德拉維加《印卡王室述評》，中譯本，528-531頁。

10. 印卡·加西拉索·德拉維加《印卡王室述評》，中譯本，546-547頁。

11. 印卡·加西拉索·德拉維加《印卡王室述評》，中譯本，548-551頁。

12. 印卡·加西拉索·德拉維加《印卡王室述評》，中譯本，584-586頁。

13.