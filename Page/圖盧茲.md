**圖盧茲**（[法语](../Page/法语.md "wikilink")：****）位於[法國西南部](../Page/法國.md "wikilink")[加龍河畔](../Page/加龍河.md "wikilink")，大致处于[大西洋和](../Page/大西洋.md "wikilink")[地中海之間的中点](../Page/地中海.md "wikilink")，为[上加龍省](../Page/上加龍省.md "wikilink")、[欧西坦尼亚大区首府](../Page/欧西坦尼亚大区.md "wikilink")，距巴黎590公里（366英里）。截至2013年1月1日，图卢兹大市拥有1,291,517居民，是法國第四大城市，仅次于巴黎（1210万），里昂
（210万），和马赛（170万）。\[1\]

圖盧茲是欧洲航天产业的基地，[空中巴士](../Page/空中巴士.md "wikilink")、[伽利略定位系統](../Page/伽利略定位系統.md "wikilink")、[SPOT卫星的總部](../Page/SPOT卫星.md "wikilink")，[法國國家太空研究中心的图卢兹航天中心](../Page/法國國家太空研究中心.md "wikilink")(CST)，欧洲最大的航天中心都位于这里\[2\]。欧洲最大[卫星制造商](../Page/卫星.md "wikilink")[泰雷兹阿莱尼亚宇航公司](../Page/泰雷兹阿莱尼亚宇航公司.md "wikilink")，和EADS公司的子公司EADS
Astrium
Satellites也是图卢兹的重要存在。著名的[图卢兹大学创办于](../Page/图卢兹大学.md "wikilink")1229年，是[欧洲最古老的大学之一](../Page/欧洲.md "wikilink")，有超过97,000的学生，与[里尔并列](../Page/里尔.md "wikilink")[法国第三大大学校园](../Page/法国.md "wikilink")，仅次于[巴黎大学和](../Page/巴黎大学.md "wikilink")[里昂大学](../Page/里昂大学.md "wikilink")。\[3\]

图卢兹是前[朗格多克](../Page/朗格多克.md "wikilink")[省的首府](../Page/省.md "wikilink")（省级区划在[法国大革命期间被取消](../Page/法国大革命.md "wikilink")）。现在它是[南-比利牛斯](../Page/南部-比利牛斯.md "wikilink")[大区的首府](../Page/大区.md "wikilink")，该区是法国本土最大的地区。它也是[上加龙](../Page/上加龙省.md "wikilink")[二级行政区的首府](../Page/省_\(法国\).md "wikilink")。

## 历史

早在[鐵器時代圖盧茲是一個貿易樞紐](../Page/鐵器時代.md "wikilink")。在公元前2世紀，曾是古羅馬的軍事前哨。公元五世纪至六世纪初期，图卢兹是[西哥特王國的首都](../Page/西哥特王國.md "wikilink")。778年后成为图卢兹封建伯爵领地首府。

以后，修建了一些宗教建筑和大学（1229年）。1420年建立大理院，在[法国大革命前](../Page/法国大革命.md "wikilink")，它一直对朗格多克有管辖权。16世纪宗教战争时期，该市支持天主教联盟。

19世纪图卢兹随着铁路的兴建发展了商业。工业多样化，有化学工业和飞机与机器制造业，充分利用了比利牛斯的水力和拉克的天然气。航空航天工业已得到惊人的发展，包括研究、试验、专家培训和飞行器制造（快帆式喷气式飞机、协和式客机、空中客车和军用构件）。

## 地理

### 水文

城市有[Brienne运河](../Page/Brienne运河.md "wikilink")、[南运河和](../Page/米迪运河.md "wikilink")[加龙河](../Page/加龙河.md "wikilink")、[Touch河以及](../Page/Touch河.md "wikilink")[Hers-Mort河穿过](../Page/Hers-Mort河.md "wikilink")。

### 气候

图卢兹是[温带气候](../Page/温带.md "wikilink")，在[科彭气候分类下通常被列为](../Page/柯本气候分类法.md "wikilink")[海洋性气候](../Page/海洋性气候.md "wikilink")（Cfb），处于[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")（Cfa）分类的下方。图卢兹位于与[地中海气候区的交界处](../Page/地中海气候.md "wikilink")，但均匀的降水却让它免于被归入这种方式。

## 人口

| 历史人口 |
| ---- |
|      |
| 1695 |
| 1750 |
| 1790 |
| 1801 |
| 1831 |
| 1851 |
| 1872 |
| 1911 |
| 1936 |
| 1946 |
| 1954 |
| 1962 |
| 1968 |
| 1975 |
| 1982 |
| 1990 |
| 1999 |
| 2008 |
| 2013 |

在2008年1月1日人口普查时，城区（法语：*commune*，指[市镇](../Page/市镇_\(法国\).md "wikilink")）的人口数为439,553，大都市区（法语：*[aire
urbaine](../Page/aire_urbaine.md "wikilink")*）的居民数为1,202,889（在2008年的大都市区边界内），此前在1999年3月人口普查时该数值为964,797在（在大都市区1999年的边界内）。\[4\]在其2008年时的边界内，大都市区的人口在1999年和2008年间以每年超过1.87%的速度增长。

图卢兹是法国第四大城市，仅次于巴黎、[马赛和](../Page/马赛.md "wikilink")[里昂](../Page/里昂.md "wikilink")，是继巴黎、里昂和马赛之后的第四大都市圈。

由于航空航天和高科技产业的蓬勃发展，在20世纪90年代的大都市区人口以每年1.49%的速度增长（[全法国为](../Page/法国本土.md "wikilink")0.37%）；在21世纪初以纪录速度每年1.87%的增长（全法国为0.68%）。这是任何法国大于50万居民的大都市区中人口增长率最高的，这意味着图卢兹大都市区在2008年已经超越了里尔成为法国第四大都市圈。

据当地一个犹太人团体估计，图卢兹大约有2500个犹太人家庭，许多人在北非的阿尔及利亚独立并剥夺他们公民权时逃离，或是在摩洛哥的一系列反犹太人事件中被赶了出来。

另据一个穆斯林协会估计，城内有大约有35,000个穆斯林。\[5\]其中，大部分的穆斯林都集中居住在图卢兹市的西南部“Mirail”区域附近，也就是图卢兹第二大学附近。

## 政府和政治

### 图卢兹集聚区

图卢兹大集聚区（Communauté d'agglomération du Grand
Toulouse）创建于2001年，目的在于更好地协调图卢兹和其独立郊区之间的交通运输，基础设施和经济政策。它之前便是1992年创建的区域，当时议会的权力比现在要小得多。它结合了图卢兹市和24个独立*市镇*，覆盖面积380平方公里（147平方英里），总人口583,229居民（1999年人口普查），其中67%居住在图卢兹市。据2004年2月时估计，图卢兹大集聚区总人口为651，209居民，其中65.5%居住在图卢兹市。由于当地的政治争斗，大集聚区只覆盖了大都市区61%的人口，其他独立的郊区则拒绝加入。2009年以来，大集聚区已成为一个城市团体（法语：communauté
urbaine）。

## 经济

[Airbus_Toulouse_plant_entrance_DSC02696.jpg](https://zh.wikipedia.org/wiki/File:Airbus_Toulouse_plant_entrance_DSC02696.jpg "fig:Airbus_Toulouse_plant_entrance_DSC02696.jpg")的空客工厂，毗邻[图卢兹机场](../Page/图卢兹－布拉尼亚克机场.md "wikilink")\]\]
主要工业是[航空](../Page/航空.md "wikilink")、航天、电子、信息技术和[生物技术](../Page/生物技术.md "wikilink")。图卢兹拥有[空中客车公司总部和空客诸多总装线](../Page/空中客车.md "wikilink")，包括[A320](../Page/空中客车A320.md "wikilink")、[A330](../Page/空中客车A330.md "wikilink")、[A340和](../Page/空中客车A340.md "wikilink")[A380](../Page/空中客车A380.md "wikilink")，其他（[A318](../Page/空中客车A318.md "wikilink")、[A319](../Page/空中客车A319.md "wikilink")、[A321和](../Page/空中客车A321.md "wikilink")[A380内部装饰](../Page/A380.md "wikilink")）位于德国[汉堡](../Page/汉堡.md "wikilink")。空客公司打算将图卢兹的空客A320总装活动搬迁到汉堡，A350和A380的生产则相反。该计划是其前首席执行官克里斯蒂安·斯特雷夫（Christian
Streiff）倡导的Power8组织计划的一部分。空客公司总部位于图卢兹附近的[布拉尼亚克](../Page/布拉尼亚克.md "wikilink")。\[6\]\[7\]空客法国的主要办事机构位于图卢兹。
\[8\]

图卢兹也是[ATR公司以及](../Page/ATR.md "wikilink")公司的总部所在地。

根据《[新闻周刊](../Page/新闻周刊.md "wikilink")》报道，图卢兹在2006年在世界最具活力的城市中排名第五。

## 教育

[Toulouse_-_Université_Toulouse_III_(1).jpg](https://zh.wikipedia.org/wiki/File:Toulouse_-_Université_Toulouse_III_\(1\).jpg "fig:Toulouse_-_Université_Toulouse_III_(1).jpg")
图卢兹有97,000个学生，在法国城市中排行第三，仅次于[里昂和巴黎](../Page/里昂.md "wikilink")。[图卢兹大学](../Page/图卢兹大学.md "wikilink")（Université
de
Toulouse）成立于1229年，坐落在这里（现在分为三个独立的大学）。与[牛津和巴黎的大学一样](../Page/牛津.md "wikilink")，图卢兹大学成立时，欧洲人正开始翻译阿拉伯人安达卢斯和希腊哲学家著作。这些著作挑战了既有欧洲的思想，鼓舞人心的科学发现和进步的艺术使社会开始以一种新的方式来看待自己。这些学院是由教会支持，希望调和希腊哲学和基督教神学。在2012年，一群学生决定继续这个梦想，并创造了一个重大的事件“住在你的梦想”。第一年，他们在活动中邀请法国尼尔·阿姆斯特朗，菲利普·波伦，以尋求激发一个新时代的灵感。

图卢兹的三所大学：

  - [图卢兹第一大学](../Page/图卢兹第一大学.md "wikilink")、[图卢兹经济学院和](../Page/图卢兹经济学院.md "wikilink")[图卢兹治学院](../Page/图卢兹治学院.md "wikilink")
  - [图卢兹第二大学](../Page/图卢兹第二大学.md "wikilink")
  - [保罗·萨巴蒂尔大学](../Page/图卢兹第三大学.md "wikilink")（图卢兹第三大学）

图卢兹也是[图卢兹商学院](../Page/图卢兹商学院.md "wikilink")，[欧洲高等管理学院](../Page/欧洲高等管理学院.md "wikilink")、[ISEFAC商学院以及众多工程学校的所在地](../Page/ISEFAC商学院.md "wikilink")：

  - [ICAM
    Toulouse](../Page/Institut_Catholique_des_Arts_et_Metiers.md "wikilink")
    (Institut catholique d'arts et métiers)
  - [国立图卢兹应用科学学院](../Page/国立图卢兹应用科学学院.md "wikilink")
  - [ISAE SUPAERO](../Page/法国高等航空航天学院.md "wikilink") (École Nationale
    Supérieure de l'Aéronautique et de l'Espace)全法国工程师学校排名第五的航空航天工程师学校
  - [ISAE ENSICA](../Page/ENSICA.md "wikilink") (École nationale
    supérieure d'ingénieurs de constructions aéronautiques)
  - [国立民用航空学院](../Page/国立民用航空学院.md "wikilink")
  - [国立高等电力技术、电子学、计算机、水力学与电信学院](../Page/国立高等电力技术、电子学、计算机、水力学与电信学院.md "wikilink")
  - [INP
    ENSIACET](../Page/École_nationale_supérieure_des_ingénieurs_en_arts_chimiques_et_technologiques.md "wikilink")
    (École nationale supérieure d'ingénieurs en art chimique et
    technologique)
  - [INP
    ENSAT](../Page/École_Nationale_Supérieure_Agronomique_de_Toulouse.md "wikilink")
    ('École Nationale Supérieure Agronomique de Toulouse)
  - [Epitech](../Page/Epitech.md "wikilink")
  - [高等科学技术学院](../Page/高等科学技术学院.md "wikilink")
  - [EIPurpan](../Page/Ecole_d'Ingénieur_de_Purpan.md "wikilink") (École
    d'ingénieurs de Purpan)
  - [ENSA Toulouse
    图卢兹高等建筑学院](../Page/ENSA_Toulouse_图卢兹高等建筑学院.md "wikilink")

法国报纸《L'Etudiant》认为图卢兹是法国最好的学习城市，英国公司[夸夸雷利西蒙兹认为图卢兹在世界上最好的学生城市中排名第](../Page/Quacquarelli_Symonds.md "wikilink")46位。

图卢兹最知名的高中为皮埃尔·德·费马公立中学（Lycée Pierre de Fermat），圣-约瑟夫公立中学（Lycée
Saint-Joseph）和圣塞宁公立中学（Lycée
Saint-Sernin）。除此之外，包括[歐然那綜合高中](../Page/歐然那綜合高中.md "wikilink")(Lycée
Ozenne)等學校也相當著名。

## 交通

### 空運

  - [图卢兹-布拉尼亚克机场](../Page/图卢兹-布拉尼亚克机场.md "wikilink")

### 鐵路

  - [图卢兹马塔比尤站](../Page/图卢兹马塔比尤站.md "wikilink")

## 体育

[图卢兹足球俱乐部](../Page/图卢兹足球俱乐部.md "wikilink"),该队曾在1957年荣获[法国杯冠军](../Page/法国杯.md "wikilink")。

图卢兹最出名的体育项目当属图卢兹橄榄球俱乐部。该队曾多次获得法国国内联赛冠军，杯赛冠军以及欧洲冠军联赛冠军。

## 世界文化遺產

南運河、通往[圣地亚哥-德孔波斯特拉之路](../Page/圣地亚哥-德孔波斯特拉.md "wikilink")

<File:Xvolks> canal du midi 01.jpg|南運河 Façade de la cathédrale
Saint-Étienne de
Toulouse.jpg|[天主教圣斯德望主教座堂](../Page/图卢兹主教座堂.md "wikilink")
<File:Airbus> Toulouse plant entrance DSC02696.jpg|空中巴士廠房

## 国际关系

### 姐妹城市

图卢兹与以下城市结为姐妹城市：

  - [特拉維夫](../Page/特拉維夫.md "wikilink")

  - [重慶](../Page/重慶.md "wikilink")

  - [亞特蘭大](../Page/亞特蘭大.md "wikilink")

  - [基輔](../Page/基輔.md "wikilink")

  - [博洛尼亞](../Page/博洛尼亞.md "wikilink")

<!-- end list -->

  - [埃爾切](../Page/埃爾切.md "wikilink")

  - [黎巴嫩](../Page/黎巴嫩.md "wikilink")[的黎波里](../Page/的黎波里_\(黎巴嫩\).md "wikilink")

  - [英国](../Page/英国.md "wikilink")[布里斯托](../Page/布里斯托.md "wikilink")

  - [阿根廷](../Page/阿根廷.md "wikilink")[罗萨里奥](../Page/罗萨里奥.md "wikilink")

图卢兹也与以下城市有合作协定：

  - [波兰](../Page/波兰.md "wikilink")[比得哥什](../Page/比得哥什.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")

  - [越南](../Page/越南.md "wikilink")[河内](../Page/河内.md "wikilink")

  - [乍得](../Page/乍得.md "wikilink")[恩贾梅纳](../Page/恩贾梅纳.md "wikilink")

<!-- end list -->

  - [塞内加尔](../Page/塞内加尔.md "wikilink")[圣路易](../Page/圣路易.md "wikilink")

  - [巴西](../Page/巴西.md "wikilink")[圣若泽多斯坎波斯](../Page/圣若泽多斯坎波斯.md "wikilink")

  - [西班牙](../Page/西班牙.md "wikilink")[阿拉贡](../Page/阿拉贡.md "wikilink")[萨拉戈萨](../Page/萨拉戈萨.md "wikilink")

此外，图卢兹还带领一个城市：

  - [罗马尼亚](../Page/罗马尼亚.md "wikilink")[肯皮亚图尔济](../Page/肯皮亚图尔济.md "wikilink")

## 地有名人物与团体

## 参考文献

## 外部链接

  - [圖盧茲网站](http://www.toulouse.fr/)

[图卢兹](../Category/图卢兹.md "wikilink")
[T](../Category/上加龙省市镇.md "wikilink")

1.

2.

3.  [Atlas 2005-2006 de l'éducation
    nationale](ftp://trf.education.gouv.fr/pub/edutel/dpd/atlas/atlas2006/atlas2006.pdf)
    , \[pdf\] Consulté le 11/09/2007

4.

5.  [Killings sour good life for high-flying
    Toulouse](http://www.reuters.com/article/2012/03/20/us-france-shootings-toulouse-idUSBRE82J17C20120320)

6.  "[Airbus A380 lands after making aviation
    history](http://www.usatoday.com/travel/news/2005-04-27-airbus-flight_x.htm)."
    [USA Today](../Page/USA_Today.md "wikilink"). 27 April 2005. Updated
    28 April 2005. Retrieved 12 February 2010.

7.  "[Contacts](http://www.airbus.com/en/utilities/contacts.html) ."
    [Airbus](../Page/Airbus.md "wikilink"). Retrieved 12 February 2010.

8.