**土佐市**（）是位于[高知縣中部的城市](../Page/高知縣.md "wikilink")，位於[四國地區三大河川之一的](../Page/四國.md "wikilink")[仁淀川河口](../Page/仁淀川.md "wikilink")。為高知縣内11市中面積最小的[市](../Page/市.md "wikilink")。主要產業為農業和漁業，特產為土佐文旦。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[高岡郡](../Page/高岡郡.md "wikilink")**高岡村**、戶波村、北原村、波介村、蓮池村、高石村、宇佐村、新居村。
  - 1899年3月1日：高岡村改制為**高岡町**。
  - 1922年9月1日：宇佐村改制為[宇佐町](../Page/宇佐町_\(高知縣\).md "wikilink")。
  - 1942年4月1日：宇佐町和新居村[合併為](../Page/市町村合併.md "wikilink")[新宇佐町](../Page/新宇佐町.md "wikilink")。
  - 1949年4月1日：新宇佐町分割回復為宇佐町、新居村。
  - 1954年3月31日：高岡町、高石村、蓮池村、北原村、波介村、戶波村合併為新設置的**高岡町**。
  - 1958年4月1日：高岡町、宇佐町、新居村合併為新設置的**高岡町**。
  - 1959年1月1日：高岡町改制並改名為**土佐市**。名稱來自高知縣的舊名[土佐國](../Page/土佐國.md "wikilink")。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>高岡村</p></td>
<td><p>1899年3月1日<br />
高岡町</p></td>
<td><p>1954年3月31日<br />
高岡町</p></td>
<td><p>1958年4月1日<br />
高岡町</p></td>
<td><p>1959年1月1日<br />
土佐市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>戶波村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>波介村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>蓮池村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高石村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>宇佐村</p></td>
<td><p>1922年9月1日<br />
宇佐町</p></td>
<td><p>1942年4月1日<br />
新宇佐町</p></td>
<td><p>1949年4月1日<br />
宇佐町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>新居村</p></td>
<td><p>1949年4月1日<br />
新居村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  [鹽見俊雄](../Page/鹽見俊雄.md "wikilink")（1958年4月 - 1962年）
2.  山本信光（1962年4月 - 1968年12月）任期中辭職
3.  板原傳（1969年2月 - 1983年）
4.  近添美豐（1983年2月 - 1991年）
5.  籠尾源吉（1991年2月 - 1995年）
6.  五藤一成（1995年2月 - 1995年10月）
7.  森田康生（1995年10月29日 - 2007年10月28日）
8.  板原啓文（2007年10月29日 - ）

## 交通

### 鐵路

轄區內無鐵路通過，距離市中心最近的車站是為於[高知市的](../Page/高知市.md "wikilink")[土讚線](../Page/土讚線.md "wikilink")[朝倉車站](../Page/朝倉車站.md "wikilink")。

### 道路

  - 高速道路

<!-- end list -->

  - [高知自動車道](../Page/高知自動車道.md "wikilink")：[土佐交流道](../Page/土佐交流道.md "wikilink")
    - [土佐休息區](../Page/土佐休息區.md "wikilink")

## 觀光資源

[Shoryuji_01.JPG](https://zh.wikipedia.org/wiki/File:Shoryuji_01.JPG "fig:Shoryuji_01.JPG")

### 景點

  - 波介山展望公園
  - [蓮池城遺址](../Page/蓮池城.md "wikilink")
  - 龍之濱海水浴場
  - 橫浪三里
  - [四國八十八箇所](../Page/四國八十八箇所.md "wikilink")
      - 35番札所 醫王山[清瀧寺](../Page/清瀧寺_\(土佐市\).md "wikilink")
      - 36番札所 独鈷山[青龍寺](../Page/青龍寺_\(土佐市\).md "wikilink")
  - [松尾八幡宮](../Page/松尾八幡宮_\(土佐市\).md "wikilink")
  - 天崎鍾乳洞

### 祭典、活動

  - 宇佐大鍋祭（5月上旬）
  - 蓮池ハス祭（7月第1和第2個星期日）
  - 宇佐港祭（8月第2個星期六）
  - 大綱祭（8月第3個星期六）
  - 西之宮八幡宮秋祭＜蓮池太刀踊＞（11月3日）

## 教育

  - 高等學校

<!-- end list -->

  - [高知縣立高岡高等學校](../Page/高知縣立高岡高等學校.md "wikilink")
  - [高知縣立高知海洋高等學校](../Page/高知縣立高知海洋高等學校.md "wikilink")
  - [明德義塾中高等學校龍國際校區](../Page/明德義塾中高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [江別市](../Page/江別市.md "wikilink")（[北海道](../Page/北海道.md "wikilink")）
      -
        於1978年7月15日締結為友好都市

### 海外

  - [伊塔蒂巴](../Page/伊塔蒂巴.md "wikilink")（[巴西](../Page/巴西.md "wikilink")[圣保罗州](../Page/圣保罗州.md "wikilink")）

      -
        於1989年8月5日締結為姊妹都市

## 本地出身之名人

  - [中島信行](../Page/中島信行.md "wikilink")：第一任[眾議院](../Page/眾議院_\(日本\).md "wikilink")[議長](../Page/議長.md "wikilink")
  - [塩見俊二](../Page/塩見俊二.md "wikilink")：曾任[參議院](../Page/參議院_\(日本\).md "wikilink")[議員](../Page/議員.md "wikilink")、自治大臣、国家公安委員會委員長、厚生大臣
  - [西原清東](../Page/西原清東.md "wikilink")：曾任[衆議院議員](../Page/衆議院議員.md "wikilink")、第四任[同志社社長](../Page/同志社.md "wikilink")
  - [今村和郎](../Page/今村和郎.md "wikilink")：曾任[貴族院勅選議員](../Page/貴族院_\(日本\).md "wikilink")
  - [山脇正隆](../Page/山脇正隆.md "wikilink")：[日本帝國陸軍](../Page/日本帝國陸軍.md "wikilink")[大將](../Page/大將.md "wikilink")
  - [坂本政右衛門](../Page/坂本政右衛門.md "wikilink")：陸軍中將
  - [松永雄樹](../Page/松永雄樹.md "wikilink")：[日本帝國海軍中將](../Page/日本帝國海軍.md "wikilink")
  - [小川莚喜](../Page/小川莚喜.md "wikilink")：海軍少將
  - [有藤道世](../Page/有藤道世.md "wikilink")：前職業棒球總教練元ロッテ監督・TBS野球解説者）
  - [井本隆](../Page/井本隆.md "wikilink")：前職業棒球選手
  - [横山小次郎](../Page/横山小次郎.md "wikilink")：前職業棒球選手
  - [土佐豐祐哉](../Page/土佐豐祐哉.md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")
  - [純信](../Page/純信.md "wikilink")：僧侶
  - [楠みちはる](../Page/楠みちはる.md "wikilink")：[漫画家](../Page/漫画家.md "wikilink")
  - [中平和水](../Page/中平和水.md "wikilink")：警政官僚
  - [原知佐子](../Page/原知佐子.md "wikilink")：女演員
  - [市村光恵](../Page/市村光恵.md "wikilink")：憲法學者
  - [井上青龍](../Page/井上青龍.md "wikilink")：攝影師
  - [今村鞆](../Page/今村鞆.md "wikilink")：警官
  - [青木繁吉](../Page/青木繁吉.md "wikilink")：企業家

## 外部連結

  - [仁淀川流域交流会議](https://web.archive.org/web/20160309003527/http://www.niyodoriver.jp/)