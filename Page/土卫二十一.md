**土卫二十一**又直接音譯為「特弗斯」或「塔沃斯」（S/2000 S 4,
Tarvos），是环绕[土星运行的一颗](../Page/土星.md "wikilink")[卫星](../Page/卫星.md "wikilink")。

## 概述

這顆[衛星](../Page/衛星.md "wikilink")[半长轴約為](../Page/半长轴.md "wikilink")18,562,800[公里](../Page/公里.md "wikilink")，繞土星[公轉週期](../Page/公轉週期.md "wikilink")944.23天，與[黃道及](../Page/黃道.md "wikilink")[土星](../Page/土星.md "wikilink")[赤道的](../Page/赤道.md "wikilink")[軌道傾角為](../Page/軌道傾角.md "wikilink")34.679°，[軌道離心率](../Page/軌道離心率.md "wikilink")0.5305。

## 参见

  - [土星的卫星](../Page/土星的卫星.md "wikilink")

## 參考資料

[土卫21](../Category/土星的卫星.md "wikilink")