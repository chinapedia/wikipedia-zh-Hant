**土城機廠**（[英文](../Page/英文.md "wikilink")：**Tucheng
Depot**）位於[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[土城區](../Page/土城區.md "wikilink")，為[台北捷運](../Page/台北捷運.md "wikilink")[板南線的](../Page/板南線.md "wikilink")[機廠](../Page/機廠.md "wikilink")。

## 機廠概要

土城機廠因[新北市特二號道路跨越上方故與其合併施工](../Page/台65線.md "wikilink")，一併配合辦理發標、施工，以避免未來二次施工造成之衝擊。

土城機廠為台北捷運目前唯一設有[三角線設施的機廠](../Page/三角線.md "wikilink")。如有需要，列車可藉由三角線調頭。土城機廠一次可提供一個「三車組」調頭。其他機廠至目前為止均無相關調頭設施。目前已有調頭列車於線上營運。但該列車也已調回頭。

因納莉颱風帶來的洪水曾由[南港機廠漫進捷運系統](../Page/南港機廠.md "wikilink")，因此於土城機廠出土段設置二座全斷面防水隔艙閘門，以避免類似事情再度發生。

土城機廠原為台北捷運中，規模僅次於[北投機廠與](../Page/北投機廠.md "wikilink")[蘆洲機廠之四級修護廠](../Page/蘆洲機廠.md "wikilink")，為配合長遠規劃及分攤北投機廠負荷之考量，土城機廠已增建土木／軌道工廠等設施，已提升至具有五級修護廠功能。

## 機廠位置與結構

  - 地理位置：[新北市](../Page/新北市.md "wikilink")[土城區](../Page/土城區.md "wikilink")[大漢溪防汛河川舊址](../Page/大漢溪.md "wikilink")。
  - 廠房規模：五級修護廠。
  - 主要功能：車輛停駐、清洗、維修保養，以及相關備用品儲存、削車輪等。
  - 長度：約1,300公尺。
  - 寬度：最寬約540公尺，最窄約160公尺。
  - 面積：約26.8公頃。
  - 廠房分區：駐車廠、工作廠、電聯車清洗廠、變電站、捷運警察分隊、警衛室、辦公室、污水處理場等。
  - 軌道設備：駐車廠內有18條停車用軌，1條清洗用軌。
  - 駐車容量：駐車廠內可容納33列車停放於停車用軌，2列車停放於清洗用軌。

## 歷史

  - 2006年5月31日：土城機廠隨著板橋線「[新埔](../Page/新埔站_\(新北市\).md "wikilink")－[府中](../Page/府中站.md "wikilink")」段及土城線通車，正式投入維修調度。

## 重要工程

<table>
<tbody>
<tr class="odd">
<td><p>標號</p></td>
<td><p>工程內容</p></td>
<td><p>監工單位</p></td>
<td><p>承包商</p></td>
<td><p>開工日期</p></td>
<td><p>竣工日期</p></td>
<td><p>工程金額<br />
<small><a href="../Page/新台幣.md" title="wikilink">新台幣</a>（元）</small></p></td>
</tr>
<tr class="even">
<td><p>CD267A</p></td>
<td><p>土城機廠土方工程</p></td>
<td><p><a href="http://www.cdpo.dorts.gov.tw/">台北市捷運工程局中區工程處</a></p></td>
<td><p>林記營造股份有限公司</p></td>
<td><p>1995年9月1日</p></td>
<td><p>1999年6月7日</p></td>
<td><p>1,190,000,000</p></td>
</tr>
<tr class="odd">
<td><p>CD267</p></td>
<td><p>土城機廠土建工程</p></td>
<td><p><a href="http://www.cec.com.tw/">大陸工程股份有限公司</a></p></td>
<td></td>
<td></td>
<td><p>1,060,000,000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>CD314</p></td>
<td><p>土城機廠水電及環控系統工程</p></td>
<td><p><a href="http://www.ctci.com.tw/">中鼎工程股份有限公司</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CD511</p></td>
<td><p>板橋線第二階段及土城線軌道標</p></td>
<td><p><a href="http://www.senyeh.com.tw/">森業營造服份有限公司</a></p></td>
<td><p>2000年3月10日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>CD312</p></td>
<td><p>板橋線第二階段及土城線號誌工程</p></td>
<td><p><a href="http://semp.dorts.gov.tw">台北市政府捷運工程局機電系統工程處</a></p></td>
<td><p><a href="http://www.alstom.com">法商亞世通號誌股份有限公司台灣分公司 （ALSTOM Signaling Inc. Taiwan Branch）</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CD313</p></td>
<td><p>板橋線第二階段及土城線供電工程</p></td>
<td><p>中鼎工程股份有限公司</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>CD315</p></td>
<td><p>板橋線第二階段及土城線通訊工程</p></td>
<td><p>漢唐訊聯股份有限公司</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CD319A</p></td>
<td><p>土城機廠設備工程</p></td>
<td><p><a href="http://www.uisco.com.tw/">漢唐集成股份有限公司</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>IDXX03</p></td>
<td><p>台北縣特二號道路第一期工程「跨越土城捷運機廠段」</p></td>
<td><p>台北市捷運工程局中區工程處</p></td>
<td><p>大陸工程股份有限公司</p></td>
<td><p>2001年11月2日</p></td>
<td><p>2003年12月26日</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 該標案合併其他標案發標，或土城機廠僅為該標案範圍之一部份，因此開工日期、竣工日期、工程金額僅供參考。

[藍](../Category/台北捷運機廠.md "wikilink")