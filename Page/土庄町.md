**土庄町**（）是隸屬[日本](../Page/日本.md "wikilink")[香川縣](../Page/香川縣.md "wikilink")[小豆郡的一](../Page/小豆郡.md "wikilink")[町](../Page/町.md "wikilink")，主要轄區包括：[小豆島西北部和前島](../Page/小豆島.md "wikilink")、[豐島](../Page/豐島_\(香川縣\).md "wikilink")、[小豐島](../Page/小豐島.md "wikilink")。以世界最窄的海峽[土淵海峽聞名](../Page/土淵海峽.md "wikilink")。

1990年曾因豐島上長達16年的非法傾倒[事業廢棄物問題而登上日本全國媒體版面](../Page/事業廢棄物.md "wikilink")。

## 歷史

[江戶時代前](../Page/江戶時代.md "wikilink")，原屬於[備前國](../Page/備前國.md "wikilink")，為[細川氏的領地](../Page/細川氏.md "wikilink")，後先後成為[豐臣氏和](../Page/豐臣氏.md "wikilink")[江戶幕府的](../Page/江戶幕府.md "wikilink")[天領](../Page/天領.md "wikilink")，1830年後部分地區又成為德川家親藩[津山藩的領地](../Page/津山藩.md "wikilink")。\[1\]

[明治維新後](../Page/明治維新.md "wikilink")，先後隸屬[倉敷縣](../Page/倉敷縣.md "wikilink")（現已被拆分併入[岡山縣和](../Page/岡山縣.md "wikilink")[廣島縣](../Page/廣島縣.md "wikilink")）、[津山縣](../Page/津山縣.md "wikilink")（現已被合併為岡山縣的一部分）、[丸龜縣](../Page/丸龜縣.md "wikilink")（現已被合併為香川縣的一部分）、[北條縣](../Page/北條縣.md "wikilink")（現已被合併為岡山縣的一部分）、[名東縣](../Page/名東縣.md "wikilink")（現已被拆分成為[德島縣](../Page/德島縣.md "wikilink")、香川縣，並有部分地區被併入[兵庫縣](../Page/兵庫縣.md "wikilink")）、[愛媛縣](../Page/愛媛縣.md "wikilink")，最終屬於現在的香川縣。

### 年表

  - 1890年2月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[小豆郡](../Page/小豆郡.md "wikilink")**土庄村**、北浦村、四海村、豐島村、大鐸村、淵崎村、大部村。
  - 1898年2月11日：土庄村改制為**土庄町**。
  - 1955年4月1日：土庄町、北浦村、四海村、豐島村、大鐸村、淵崎村[合併為新設置的](../Page/市町村合併.md "wikilink")**土庄町**。
  - 1957年7月1日：大部村被併入土庄町。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1890年2月15日</p></th>
<th><p>1890年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>土庄村</p></td>
<td><p>1898年2月11日<br />
土庄町</p></td>
<td><p>1955年4月1日<br />
合併為土庄町</p></td>
<td><p>土庄町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>北浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>四海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>豐島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大鐸村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>淵崎村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大部村</p></td>
<td><p>1957年7月1日<br />
併入土庄町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

[Tonosho_Port_Shodo_Island_Kagawa_Pref_Japan09s3.jpg](https://zh.wikipedia.org/wiki/File:Tonosho_Port_Shodo_Island_Kagawa_Pref_Japan09s3.jpg "fig:Tonosho_Port_Shodo_Island_Kagawa_Pref_Japan09s3.jpg")

### 港口

  - 小豆島
      - 土庄港
      - 土庄東港
      - 北浦港
      - 大部港
  - 豐島
      - 家浦港
      - 唐櫃港

### 渡輪

  - 四國渡輪
      - 土庄港〜高松港
      - 土庄港〜新岡山
  - 小豆島急行渡輪
      - 土庄港〜高松港
  - 兩備渡輪
      - 土庄港〜新岡山
  - 小豆島渡輪
      - 土庄港〜唐櫃港
      - 家浦港〜唐櫃港
      - 家浦港〜宇野港（岡山縣玉野市）
  - 瀨戶內觀光汽船
      - 大部港〜日生港（岡山縣備前市）

## 觀光資源

[Angel_Road_Shodo_Island_Japan01s3.jpg](https://zh.wikipedia.org/wiki/File:Angel_Road_Shodo_Island_Japan01s3.jpg "fig:Angel_Road_Shodo_Island_Japan01s3.jpg")

  - 和平的群像：小說『[二十四之瞳](../Page/二十四之瞳.md "wikilink")』中老師和學生們的銅像。
  - [西光寺](../Page/西光寺_\(土庄町\).md "wikilink")：[小豆島八十八箇所札所](../Page/小豆島八十八箇所.md "wikilink")
  - [土渕海峽](../Page/土渕海峽.md "wikilink")：世界最狹窄的海峽
  - 天使路
  - [銚子溪](../Page/銚子溪.md "wikilink")（銚子溪展望台、銚子溪自然動物園）
  - 寶生院的[樟樹](../Page/樟樹.md "wikilink")：國家特別天然紀念物，推測樹齡超過1500年
  - [小豆島大觀音](../Page/小豆島大觀音.md "wikilink")
  - [豐島美術館](../Page/豐島美術館.md "wikilink")
  - [小豆島溫泉](../Page/小豆島溫泉.md "wikilink")

## 教育

  - 高等學校

<!-- end list -->

  - [香川縣立土庄高等學校](../Page/香川縣立土庄高等學校.md "wikilink")

## 姊妹、友好都市

  - [津山市](../Page/津山市.md "wikilink")（[岡山縣](../Page/岡山縣.md "wikilink")）

      -
        1837年，小豆島上的9個村自幕府天領改隸屬津山藩，在[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，最初也屬於[津山縣](../Page/津山縣.md "wikilink")（主要轄區即為現在的津山市）的轄區，基於這個歷史上的因緣，兩地於1985年4月7日締結為歴史友好都市。\[2\]

  - [南砺市](../Page/南砺市.md "wikilink")（[富山縣](../Page/富山縣.md "wikilink")）

      -
        1995年7月1日締結為友好都市。

  - [米洛斯島](../Page/米洛斯島.md "wikilink")（[希腊](../Page/希腊.md "wikilink")[愛琴海](../Page/愛琴海.md "wikilink")）

      -
        1989年10月8日締結為姊妹島。

## 本地出身之名人

  - [帖佐寛章](../Page/帖佐寛章.md "wikilink")：田徑選手，現為田徑教練

## 參考資料

## 外部連結

  - [小豆島觀光協會](http://www.shodoshima.or.jp/)

<!-- end list -->

1.

2.