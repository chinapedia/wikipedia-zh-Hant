**-{zh:土库曼斯坦;zh-hans:土库曼斯坦;zh-hant:土庫曼斯坦;zh-hk:土庫曼斯坦;zh-mo:土庫曼斯坦;zh-tw:土庫曼;}-**（）是一個[中亞國家](../Page/中亞.md "wikilink")，前[蘇聯加盟共和國之一](../Page/蘇聯加盟共和國.md "wikilink")，[蘇聯時期的名稱為](../Page/蘇聯.md "wikilink")[土库曼蘇維埃社會主義共和國](../Page/土库曼蘇維埃社會主義共和國.md "wikilink")，1991年從[蘇聯獨立](../Page/蘇聯.md "wikilink")。

## 歷史

公元前3000年，土库曼境內出現[階級社會](../Page/社会阶层.md "wikilink")。公元前600年左右後，先后被[阿契美尼德王朝](../Page/阿契美尼德王朝.md "wikilink")、[塞琉古帝国](../Page/塞琉古帝国.md "wikilink")、[大夏統治](../Page/大夏_\(中亚古国\).md "wikilink")。後來被[安息帝國之](../Page/安息帝國.md "wikilink")[阿尔沙克王朝](../Page/阿尔沙克王朝.md "wikilink")（前247年－224年）統治，期間又被[贵霜帝国入侵](../Page/贵霜帝国.md "wikilink")。

7世紀，阿拉伯人征服土库曼後，當地人民遂漸改信[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")。[烏古斯人在](../Page/烏古斯人.md "wikilink")8-9世紀來到這里。

[蒙古帝国在](../Page/蒙古帝国.md "wikilink")1219年－1221年征服[花剌子模後](../Page/花剌子模.md "wikilink")，土库曼归属[察合台汗国](../Page/察合台汗国.md "wikilink")。1370年－1506年，[帖木儿帝国时代](../Page/帖木儿帝国.md "wikilink")，才基本形成土库曼民族。

16世紀到17世紀，隸屬[希瓦汗國和](../Page/希瓦汗國.md "wikilink")[布哈拉汗國](../Page/布哈拉汗國.md "wikilink")。19世紀60－70年代，併入[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")。\[1\]

1917年底，主要領土併入[突厥斯坦蘇維埃社會主義自治共和國](../Page/突厥斯坦蘇維埃社會主義自治共和國.md "wikilink")，屬[俄羅斯聯邦](../Page/俄羅斯聯邦.md "wikilink")。1924年10月，成立土库曼蘇維埃社會主義共和國，成為[蘇聯加盟共和國](../Page/蘇聯.md "wikilink")。

1991年10月27日，宣佈獨立，改稱[土库曼斯坦共和國](../Page/土库曼斯坦共和國.md "wikilink")，同年12月21日，加入[獨立國家聯合體](../Page/獨立國家聯合體.md "wikilink")。[蘇聯解體後](../Page/蘇聯解體.md "wikilink")，土库曼成為獨立的國家。[薩帕爾穆拉特·尼亞佐夫于](../Page/薩帕爾穆拉特·尼亞佐夫.md "wikilink")1999年成為[終身總統](../Page/終身總統.md "wikilink")，實行獨裁統治。2005年8月26日，在[喀山會議上宣佈退出](../Page/獨聯體.md "wikilink")[獨立國家聯合體](../Page/獨立國家聯合體.md "wikilink")。

2006年12月21日，總統[薩帕爾穆拉特·尼亞佐夫因心臟驟停逝世](../Page/薩帕爾穆拉特·尼亞佐夫.md "wikilink")。[古爾班古雷·別爾德穆罕默多夫成為代總統](../Page/古爾班古雷·別爾德穆罕默多夫.md "wikilink")。\[2\]

2007年2月11日，舉行新的總統選舉。\[3\]最終由代總統別爾德穆罕默多夫當選。\[4\]

## 政治

[缩略图](https://zh.wikipedia.org/wiki/File:Jack-up-rig-in-the-caspian-sea_1.JPG "fig:缩略图")

土库曼斯坦的前身為[蘇聯的一個中亞地區加盟共和國](../Page/蘇聯.md "wikilink")，在1991年[蘇聯瓦解之后](../Page/蘇聯瓦解.md "wikilink")，成为獨立國家，實行[總統制](../Page/總統制.md "wikilink")。自1985年加盟共和國時期就出任當時的[土库-{}-曼共产党中央](../Page/土库曼共產黨.md "wikilink")[第一書記的](../Page/第一書記.md "wikilink")[薩帕爾穆拉特·阿塔耶維奇·尼亞佐夫](../Page/薩帕爾穆拉特·阿塔耶維奇·尼亞佐夫.md "wikilink")，在1991年12月繼任土库曼斯坦[總統](../Page/總統.md "wikilink")，隨即將擁護其執政的土库曼共產黨改名為[土库曼斯坦民主黨](../Page/土库曼斯坦民主黨.md "wikilink")，落實該黨配合土库曼斯坦政府的施政和政治立場。獨立後的土庫曼斯坦雖然與[波羅的海三國一樣沒有加入](../Page/波羅的海三國.md "wikilink")[獨立國家聯合體](../Page/獨立國家聯合體.md "wikilink")，卻以聯繫國身份加入有關組織，然而有關身份至今仍未正式獲得憲章批准。

尼亞佐夫被尊稱为[土库-{}-曼巴希](../Page/土库曼巴希.md "wikilink")，他在國內推行[個人崇拜](../Page/個人崇拜.md "wikilink")，於1994年起修改該國的[憲法](../Page/憲法.md "wikilink")，逐步將自己行使的總統權力擴大，在1999年獲無限期執行總統職權；傚法[古羅馬](../Page/古羅馬.md "wikilink")[凱撒大帝更改](../Page/凱撒大帝.md "wikilink")12個月份名稱—自己名字是一月，母親名字是四月，如此類同。2005年2月，尼亞佐夫宣佈放棄終身總統地位的待遇，並將下屆總統選舉定在2010年舉行，而尼亞佐夫本人在2006年12月21日因[心臟驟停而逝世](../Page/心臟驟停.md "wikilink")，享年67歲。

[外交方面](../Page/外交.md "wikilink")，聯合國在1995年12月12日承認土库曼斯坦為一個[永久中立國](../Page/永久中立國.md "wikilink")。土库曼斯坦奉行中立國和全方位外交政策，故此土库曼政府領導層和民間團體向來只是選擇性地參加區域聯盟活動。土库曼斯坦並没跟鄰近的中亞新興國家一样，在獨立之後靠近[美國](../Page/美國.md "wikilink")，反而由於土库曼斯坦與美國的「仇家」[伊朗因為在](../Page/伊朗.md "wikilink")[能源運輸上有很多大型合作](../Page/能源.md "wikilink")，關係良好而令土、美兩國關係並不緊密。而土库曼斯坦和[俄羅斯亦因歷史淵源而在經濟和軍事聯繫上有著糾纏不清的關係](../Page/俄羅斯.md "wikilink")。

近年土庫曼斯坦在外交上越趨「獨來獨往」，又逼使國內俄羅斯族人只能選擇俄羅斯或土库曼其中之一為國籍，若放棄土库曼斯坦國籍就會喪失在土库曼的所有財產。

## 行政區劃

[TurkmenistanNumbered.png](https://zh.wikipedia.org/wiki/File:TurkmenistanNumbered.png "fig:TurkmenistanNumbered.png")
土库曼斯坦行政區劃包括5個州和1個直轄市，5個州之下有16個市和46個區。以下列出各州和直轄市（括號內為首府）：

  - [阿什哈巴德](../Page/阿什哈巴德.md "wikilink")（直轄市）

<!-- end list -->

1.  [阿哈爾州](../Page/阿哈爾州.md "wikilink")（[安納烏](../Page/安納烏.md "wikilink")）
2.  [巴爾坎州](../Page/巴爾坎州.md "wikilink")（[巴爾坎納巴德](../Page/巴爾坎納巴德.md "wikilink")）
3.  [達沙古茲州](../Page/達沙古茲州.md "wikilink")（[達沙古茲](../Page/達沙古茲.md "wikilink")）
4.  [列巴普州](../Page/列巴普州.md "wikilink")（[土库曼納巴德](../Page/土库曼納巴德.md "wikilink")）
5.  [馬雷州](../Page/馬雷州.md "wikilink")（[馬雷](../Page/馬雷.md "wikilink")）

[LocationTurkmenistan.svg](https://zh.wikipedia.org/wiki/File:LocationTurkmenistan.svg "fig:LocationTurkmenistan.svg")

## 地理

[Turkmenistan_satellite_photo.jpg](https://zh.wikipedia.org/wiki/File:Turkmenistan_satellite_photo.jpg "fig:Turkmenistan_satellite_photo.jpg")

土库曼斯坦位於[伊朗以北](../Page/伊朗.md "wikilink")，東南面和[阿富汗接壤](../Page/阿富汗.md "wikilink")、東北面與[烏茲別克為鄰](../Page/烏茲別克.md "wikilink")、西北面是[哈薩克](../Page/哈薩克.md "wikilink")，西邊毗鄰鹹水湖[裏海](../Page/裏海.md "wikilink")，是一個[內陸國家](../Page/內陸國家.md "wikilink")。面積49.12萬平方公里\[5\]，是僅次於[哈薩克斯坦的第二个最大中亞國家](../Page/哈薩克斯坦.md "wikilink")；在全世界排名第52，比[西班牙略小](../Page/西班牙.md "wikilink")。

土库曼斯坦全境大部是低地，平原多在海拔200米以下，80%的領土被[卡拉庫姆沙漠覆蓋](../Page/卡拉庫姆沙漠.md "wikilink")，餘下的大多數都屬於橫跨土库曼斯坦、烏茲別克及哈薩克的[圖蘭低地的範圍](../Page/圖蘭低地.md "wikilink")。南部和西部為[科佩特山脈和](../Page/科佩特山脈.md "wikilink")[帕羅特米茲山脈](../Page/帕羅特米茲山脈.md "wikilink")，最高點有2912米。位於最西方的[土库曼巴爾坎山脈及位於最東方的](../Page/土库曼巴爾坎山脈.md "wikilink")[庫吉唐套山脈](../Page/庫吉唐套山脈.md "wikilink")（Kugitang）是其比較重要的高地。主要河流有[阿姆河](../Page/阿姆河.md "wikilink")、[捷詹河](../Page/捷詹河.md "wikilink")、[穆爾加布河及](../Page/穆爾加布河.md "wikilink")[阿特列克河等](../Page/阿特列克河.md "wikilink")，主要分佈在東部。於1967年建成的[卡拉庫姆運河長達](../Page/卡拉庫姆運河.md "wikilink")1400公里，橫貫東南部並灌溉面積約30萬公頃。

土库曼斯坦位處於喜馬拉雅——地中海地震帶上，時常受[地震威脅](../Page/地震.md "wikilink")。首都阿什哈巴德曾經在1948年10月6日發生災難性的地震，導致多人罹難。

土库曼斯坦位處亞洲大陸的中心處，故此屬於典型的[溫帶大陸性氣候](../Page/溫帶大陸性氣候.md "wikilink")，這裡是世界上最乾旱的地區之一。年度平均溫度為14℃-16℃，日夜和冬夏的溫差很大，夏季氣溫長期高達35℃以上（在東南部的Karakum曾經有50℃的極端紀錄），冬季在接近阿富汗的山區Gushgy，氣溫亦可以低至-33℃。年降水量則由西北面沙漠的80毫米，遞增至東南山區的每年240毫米，雨季主要在[春季](../Page/春季.md "wikilink")（1月至5月）。科佩特山脈是全國降雨量最高的地區。

土库曼斯坦靠近[裡海的海岸線有](../Page/裡海.md "wikilink")1768公里長，貨物經水路出口須經過俄羅斯的[伏爾加河和](../Page/伏爾加河.md "wikilink")[頓河](../Page/頓河.md "wikilink")。

主要城市除了首都[阿什哈巴德以外](../Page/阿什哈巴德.md "wikilink")，還有臨海的城市[土库曼巴希](../Page/土库曼巴希.md "wikilink")（Turkmenbaşy，舊名克拉斯諾沃斯克/Krasnovodsk）及[達沙古茲](../Page/達沙古茲.md "wikilink")（Daşoguz）。

## 人口

[Turkman_girl_in_national_dress.jpg](https://zh.wikipedia.org/wiki/File:Turkman_girl_in_national_dress.jpg "fig:Turkman_girl_in_national_dress.jpg")
人口505萬人（2011年），當中[土库曼族為大多數佔](../Page/土库曼族.md "wikilink")77%，其次[烏茲別克族佔](../Page/烏茲別克族.md "wikilink")9.2%，[俄羅斯族佔](../Page/俄羅斯族.md "wikilink")6.7%，[哈薩克族佔](../Page/哈薩克族.md "wikilink")2%，其他少數民族包括[亞美尼亞人](../Page/亞美尼亞人.md "wikilink")、[阿塞拜疆族](../Page/阿塞拜疆族.md "wikilink")、[韃靼人等](../Page/韃靼人.md "wikilink")。人口分佈極不平均，絕大部份人口聚居在城市和[綠洲](../Page/綠洲.md "wikilink")，當中有很多非土库曼族人在城市居住，他們是在蘇聯時期移居至土库曼斯坦的技術人員、工人等的後裔。而近三分之二的土库曼族人則住在村落。

土库曼族以往是遊牧民族，現多以務農為生。在沙俄入侵吞併前族人分成了多個部落，但是沙俄的兼併使部落間更團結。土库曼人的定義是父親有土库曼血統以及[遜尼派](../Page/遜尼派.md "wikilink")[穆斯林](../Page/穆斯林.md "wikilink")，牧人的地位比農人高，只有窮困的人才做農人，他們出售馬匹，有時出賣[奴隸](../Page/奴隸.md "wikilink")。

## 經濟

[500_Manat_TM_1999.png](https://zh.wikipedia.org/wiki/File:500_Manat_TM_1999.png "fig:500_Manat_TM_1999.png")
國內生產總值（2004年）：988萬億馬納特（約合190億美元）。 人均國內生產總值（2004年）：156萬馬納特。(3000美元
)國內生產總值增長率（2004年）：21.4%。

  - 貨幣名稱：馬納特（Manat）
  - 匯率：1美元＝23,700馬納特（2006年黑市匯率，官方匯率為1：5200）。

土库曼斯坦擁有豐富的天然氣（世界第五）和石油資源，石油天然氣工業為該國的支柱產業。而農業方面則以種植棉花和小麥為主，亦有畜牧業（[阿哈爾捷金馬](../Page/阿哈爾捷金馬.md "wikilink")、[卡拉庫爾羊](../Page/卡拉庫爾羊.md "wikilink")）。

### 旅遊

[Darvasa_gas_crater_panorama.jpg](https://zh.wikipedia.org/wiki/File:Darvasa_gas_crater_panorama.jpg "fig:Darvasa_gas_crater_panorama.jpg")氣坑全景圖\]\]
土库曼的旅遊業，尤其是醫療觀光，在土库曼創造了[裏海的Avaza旅遊區後快速成長](../Page/裏海.md "wikilink")。\[6\]
每個要進入土库曼的遊客都需要在進入前獲得簽證，而大部分國家的旅客需要透過當地的旅行社獲得簽證。土库曼的旅遊行程時常包含[達沙古茲](../Page/達沙古茲.md "wikilink")、[庫尼亞烏爾根奇](../Page/庫尼亞烏爾根奇.md "wikilink")、[尼薩](../Page/尼薩.md "wikilink")、[梅爾夫](../Page/梅爾夫.md "wikilink")、[馬雷](../Page/馬雷.md "wikilink")、Avaza的海灘旅遊以及
在Mollakara、Yylly suw、Archman等地舉行的慶典。

## 文化

[Ashgabat_museum_8.jpg](https://zh.wikipedia.org/wiki/File:Ashgabat_museum_8.jpg "fig:Ashgabat_museum_8.jpg")

[缩略图](https://zh.wikipedia.org/wiki/File:Independence_Day_Parade_-_Flickr_-_Kerri-Jo_\(105\).jpg "fig:缩略图")

在18世紀之前，土库曼地區受[伊斯蘭文化影響](../Page/伊斯蘭文化.md "wikilink")。18世紀[沙俄侵佔中亞並派俄羅斯人到中亞開墾發展](../Page/沙俄.md "wikilink")，俄羅斯的文化影響漸漸滲透。但自蘇聯瓦解、土库曼斯坦成為獨立國家之後，又由於俄羅斯在當地影響力逐減和國家大力提倡伊斯蘭文化，令兩者此消彼長。

### 民族

[土库曼族](../Page/土库曼族.md "wikilink")85％，[烏茲別克族](../Page/烏茲別克族.md "wikilink")5％，[俄羅斯族](../Page/俄羅斯族.md "wikilink")4％

### 語言

[土库曼語為官方語言](../Page/土库曼語.md "wikilink")。[俄語在土库曼斯坦仍然通用](../Page/俄語.md "wikilink")，另外在烏茲別克族群聚居地方亦流行[烏茲別克語](../Page/烏茲別克語.md "wikilink")。

## 宗教

[伊斯兰教為主要宗教](../Page/伊斯兰教.md "wikilink")，佔總人口89%，主要是[遜尼派信徒](../Page/遜尼派.md "wikilink")。另外的9%人口為[東正教徒](../Page/東正教.md "wikilink")。但在強權總統[薩帕爾穆拉特·阿塔耶維奇·尼亞佐夫的統治下](../Page/薩帕爾穆拉特·阿塔耶維奇·尼亞佐夫.md "wikilink")，大力提倡伊斯蘭文化，使信奉東正教的少數俄羅斯人逐漸移出土库曼斯坦，以致東正教在該國的影響力漸衰。

## 外交

## 參考文獻

## 外部連結

## 参见

  - [土库曼斯坦節日](../Page/土库曼斯坦節日.md "wikilink")
  - [靈魂之書](../Page/靈魂之書.md "wikilink")

{{-}}

[Category:土库曼斯坦](../Category/土库曼斯坦.md "wikilink")
[Category:中亞國家](../Category/中亞國家.md "wikilink")
[Category:內陸國家](../Category/內陸國家.md "wikilink")
[Category:俄語國家地區](../Category/俄語國家地區.md "wikilink")
[Category:1991年建立的國家或政權](../Category/1991年建立的國家或政權.md "wikilink")
[Category:永久中立国](../Category/永久中立国.md "wikilink")
[Category:独立国家联合体](../Category/独立国家联合体.md "wikilink")

1.
2.  王作葵，[土库曼斯坦副總理臨時代行總統職權](http://news.xinhuanet.com/world/2006-12/21/content_5518022.htm)，新華網
3.  魏良磊，徐力宇，[土库曼斯坦舉行總統選舉](http://news.xinhuanet.com/world/2007-02/11/content_5727178.htm)，新華網
4.  王作葵，徐力宇，[別爾德穆罕默多夫當選土库曼斯坦總統](http://news.xinhuanet.com/world/2007-02/14/content_5739899.htm)，新華網
5.  [土库曼斯坦概況](http://news.xinhuanet.com/ziliao/2002-06/18/content_445963.htm)。
6.