[Tuul_in_Mongolia_July_2018.webm](https://zh.wikipedia.org/wiki/File:Tuul_in_Mongolia_July_2018.webm "fig:Tuul_in_Mongolia_July_2018.webm")
[Tuul_River_Mongolia.JPG](https://zh.wikipedia.org/wiki/File:Tuul_River_Mongolia.JPG "fig:Tuul_River_Mongolia.JPG")
**土拉河**（），是[蒙古中北部的一條河流](../Page/蒙古.md "wikilink")。全長704公里，流域面積49,840平方公里。

發源於[肯特山](../Page/肯特山.md "wikilink")，並在首都[烏蘭巴托南面流過](../Page/烏蘭巴托.md "wikilink")。它是[鄂爾渾河的支流](../Page/鄂爾渾河.md "wikilink")，並經[色楞格河流入](../Page/色楞格河.md "wikilink")[貝加爾湖](../Page/貝加爾湖.md "wikilink")。是流入[北冰洋的](../Page/北冰洋.md "wikilink")[葉尼塞河系統的一部份](../Page/葉尼塞河.md "wikilink")。另外這條河也經過[哈斯台國家公園](../Page/哈斯台國家公園.md "wikilink")。

沿岸以[柳樹為主](../Page/柳樹.md "wikilink")，河中還有稀有的[鱘魚](../Page/鱘魚.md "wikilink")。

由於烏蘭巴托的生活廢水和的[金礦礦渣的流入](../Page/金礦.md "wikilink")，以及沿岸人口增加的原因，本河正受到污染的威脅。

## 外部链接

  - UNESCO
    [有關蒙古水資源利用的報告，包括了土拉河的資料](https://web.archive.org/web/20070928021107/http://www.unescobeijing.org/projects/view.do?channelId=004002002001002003)
  - [水流的科學分析](https://web.archive.org/web/20110722075858/http://www.suiri.tsukuba.ac.jp/pdf_papers/tercbull05s/t5supple_16.pdf)
    (PDF)
  - [金礦開發影響的報告](https://web.archive.org/web/20061110181827/http://www.tahoebaikal.org/projects/watershed/)
  - [金礦資源的報告](https://web.archive.org/web/20070926185535/http://www.mram.mn/documents/projects/9.pdf)
  - [圖片集](https://web.archive.org/web/20070704163432/http://www.nd.edu/~jglazier/MONGOLIA/Mongolia7_7_99/Mongolia7_7_99c.html)
  - [有關哈斯台國家公園和土拉河環境問題的報告](https://web.archive.org/web/20070221195543/http://www.aiaccproject.org/working_papers/Working%20Papers/AIACC_WP_No004.pdf)

[Category:蒙古国河流](../Category/蒙古国河流.md "wikilink")
[Category:色楞格河](../Category/色楞格河.md "wikilink")