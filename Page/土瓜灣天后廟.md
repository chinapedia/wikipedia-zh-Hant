**土瓜灣天后廟**是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[土瓜灣一座](../Page/土瓜灣.md "wikilink")[天后廟](../Page/天后廟.md "wikilink")，1885年建成，位於[下鄉道與](../Page/下鄉道.md "wikilink")[落山道交界](../Page/落山道.md "wikilink")，現時被列作[香港三級歷史建築](../Page/香港三級歷史建築.md "wikilink")，並由[華人廟宇委員會管理](../Page/華人廟宇委員會.md "wikilink")。

## 歷史

土瓜灣天后廟於[清朝](../Page/清朝.md "wikilink")[光緒十一年](../Page/光緒.md "wikilink")（1885年）由[客家漁民籌建](../Page/客家.md "wikilink")，建於當時的海邊，近當時的[土瓜灣村旁邊](../Page/土瓜灣村.md "wikilink")。建廟時所鑄的銅鐘至今仍然得以保存。該廟於1888年曾經重修，現存入口的門額「天后古廟」四字就是寫於當時。1860年代，該處有小村落叫土瓜灣村，當時可以由天后廟遠眺海心廟。後來數次小規模填海已把海岸線推出並拉直了\[1\]。

1970年代，土瓜灣[海心島的龍母廟因填海而拆卸](../Page/海心島.md "wikilink")，[龍母像便改設於土瓜灣天后廟](../Page/龍母.md "wikilink")。

## 建築

土瓜灣天后廟屬兩進三開設計，兩旁設有廂房，天井位置則改為拜殿。而後進的屋頂有[魚尾形的裝飾](../Page/魚.md "wikilink")。

天后廟正殿供奉[天后娘娘](../Page/天后娘娘.md "wikilink")，右側的偏殿則供奉[觀音](../Page/觀音.md "wikilink")、[王母](../Page/王母.md "wikilink")、[龍母及](../Page/龍母.md "wikilink")[列聖](../Page/列聖.md "wikilink")。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參考資料

<references/>

  - [土瓜灣天后廟](http://www.ctc.org.hk/b5/directcontrol/temple11.asp)，華人廟宇委員會

[Category:土瓜灣](../Category/土瓜灣.md "wikilink")
[Category:香港三級歷史建築](../Category/香港三級歷史建築.md "wikilink")
[Category:香港天后廟](../Category/香港天后廟.md "wikilink")

1.  [香港掌故-造地編 三之三](http://hk.epochtimes.com/7/9/14/51650.htm) 《香港掌故》