**土耳其大奖赛**是[一级方程式](../Page/一级方程式.md "wikilink")[赛车的一站](../Page/赛车.md "wikilink")，在2005年8月21日初次举行，是[2005年一级方程式赛车比赛的一部分](../Page/2005年一级方程式赛车比赛.md "wikilink")。它在新建的[伊斯坦布尔赛道举行](../Page/伊斯坦布尔赛道.md "wikilink")，该赛道由著名德国工程师[赫尔曼·蒂尔克设计](../Page/赫尔曼·蒂尔克.md "wikilink")，是一级方程式历史上继[意大利的](../Page/意大利.md "wikilink")[伊莫拉赛道和](../Page/伊莫拉赛道.md "wikilink")[巴西的](../Page/巴西.md "wikilink")[因特拉戈斯賽道之后第三条](../Page/因特拉戈斯賽道.md "wikilink")[逆时针赛道](../Page/逆时针.md "wikilink")。

[Category:一级方程式大奖赛](../Category/一级方程式大奖赛.md "wikilink")
[Category:土耳其大奖赛](../Category/土耳其大奖赛.md "wikilink")
[Category:土耳其體育](../Category/土耳其體育.md "wikilink")