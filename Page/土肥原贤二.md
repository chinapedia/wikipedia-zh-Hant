**土肥原贤二**（），[日本帝国](../Page/日本.md "wikilink")[昭和时代的陆军大将](../Page/昭和.md "wikilink")。从1913年起在[中国从事策划侵略活动](../Page/中国.md "wikilink")，有“帝国陆军头号[中国通](../Page/中国通.md "wikilink")”之称，参与策划了[九一八事变](../Page/九一八事变.md "wikilink")，扶植清逊帝[溥仪在](../Page/溥仪.md "wikilink")[中国东北地区成立](../Page/中国东北地区.md "wikilink")[傀儡政权](../Page/傀儡政权.md "wikilink")[满洲国以及在日军入侵中国](../Page/满洲国.md "wikilink")[华北地区中发挥核心作用](../Page/华北地区.md "wikilink")。經[远东国际军事法庭列为](../Page/远东国际军事法庭.md "wikilink")[甲级战犯](../Page/甲级战犯.md "wikilink")，[东京审判判其死刑](../Page/东京审判.md "wikilink")。

## 生涯

1883年（[明治](../Page/明治.md "wikilink")16年）8月8日出生于[岡山縣](../Page/岡山縣.md "wikilink")[岡山市](../Page/岡山市.md "wikilink")。[青山小學校](../Page/港區立青山小學校.md "wikilink")、[仙台](../Page/仙台.md "wikilink")[陸軍幼年學校](../Page/日本陸軍幼年學校.md "wikilink")、[陸軍中央幼年學校等校毕业](../Page/日本陸軍幼年學校.md "wikilink")。1904年10月自[日本陸軍士官學校](../Page/日本陸軍士官學校.md "wikilink")16期毕业。在[步兵第15聯隊](../Page/步兵第15聯隊.md "wikilink")、[第49聯隊任职](../Page/步兵第49聯隊.md "wikilink")。1912年11月自[日本陸軍大學校](../Page/日本陸軍大學校.md "wikilink")24期毕业。

从陆大毕业时由[參謀本部派往中国北京的板西](../Page/參谋本部_\(大日本帝国\).md "wikilink")（[坂西利八郎](../Page/坂西利八郎.md "wikilink")）特务机关长作為助理。土肥原长期在中国后来成为日本陆军中有名的“[中国通](../Page/中国通.md "wikilink")”。

1924年的[直奉战争中策动阴谋暗助](../Page/直奉战争.md "wikilink")[奉系军阀](../Page/奉系军阀.md "wikilink")[张作霖](../Page/张作霖.md "wikilink")。1928年3月，土肥原出任奉系督軍张作霖的顾问。因张作霖不能满足日本在中国东北地区的特殊权益，土肥原配合[关东军高级参谋](../Page/关东军.md "wikilink")[河本大作大佐策划](../Page/河本大作.md "wikilink")1928年6月4日[皇姑屯事件](../Page/皇姑屯事件.md "wikilink")，以除掉张作霖。1931年3月任天津特务机关长。1931年8月任[奉天特务机关长](../Page/奉天.md "wikilink")，土肥原贤二与[关东军参谋](../Page/关东军.md "wikilink")[板垣征四郎](../Page/板垣征四郎.md "wikilink")、[石原莞尔](../Page/石原莞尔.md "wikilink")、等人一同参与策划了[九一八事变](../Page/九一八事变.md "wikilink")。事变后，曾一度任奉天临时市长。后将隐居在天津的清逊帝[溥仪带出](../Page/溥仪.md "wikilink")，扶植其在中国东北地区成立傀儡政权[满洲国](../Page/满洲国.md "wikilink")。

1935年6月，在[察哈尔阴谋策动](../Page/察哈爾省.md "wikilink")“[张北事件](../Page/张北事件.md "wikilink")”，以此事件为借口逼迫[民国政府签署](../Page/民国政府.md "wikilink")《[秦土协定](../Page/秦土协定.md "wikilink")》，取得了察哈尔大部主权。1935年10月，策划[华北自治运动](../Page/华北五省自治.md "wikilink")，在河北扶植[殷汝耕成立由日本控制的](../Page/殷汝耕.md "wikilink")[冀东防共自治政府](../Page/冀东防共自治政府.md "wikilink")。

1937年3月任第14师团长，1937年[七七事变后](../Page/七七事变.md "wikilink")，他指挥日军[第14師團入侵中国华北地区沿](../Page/第14師團.md "wikilink")[平汉铁路作战](../Page/平汉铁路.md "wikilink")。1938年5月在[徐州会战中](../Page/徐州会战.md "wikilink")，第14师团从[濮阳南渡黄河包抄](../Page/濮阳.md "wikilink")[徐州](../Page/徐州.md "wikilink")[第五戰區中国军队的后方](../Page/抗日战争第五战区.md "wikilink")，截断[陇海铁路向西的交通](../Page/陇海铁路.md "wikilink")，在[兰封会战中击溃](../Page/兰封会战.md "wikilink")[国民革命军二十七军](../Page/国民革命军.md "wikilink")[桂永清部](../Page/桂永清.md "wikilink")，由于战线崩溃，随后迫使民国政府在[花园口黄河决堤以阻滞日军突进](../Page/花园口黄河决堤.md "wikilink")。1938年6月担任参謀本部在上海设立的[土肥原機關首長](../Page/土肥原機關.md "wikilink")，該特務機關成立後，在土肥原的主持下，先後對[唐紹儀](../Page/唐紹儀.md "wikilink")、[吳佩孚](../Page/吳佩孚.md "wikilink")、[靳雲鵬做拉拢工作](../Page/靳雲鵬.md "wikilink")。\[1\]

1939年5月任关东军所属的第5軍司令官。1940年任[軍事参議官兼任陸軍士官学校校長](../Page/军事参议院_\(日本\).md "wikilink")。1941年4月28日晋升陆军大将，曾担任陆军航空总监。1944年以后先后任第7方面军、第12方面军司令官。

第二次世界大战结束后，土肥原贤二被[驻日盟军总司令部下令逮捕](../Page/驻日盟军总司令.md "wikilink")。1948年11月12日，[远东国际军事法庭](../Page/远东国际军事法庭.md "wikilink")（东京审判）判定其為[甲级战犯](../Page/甲级战犯.md "wikilink")，判处[绞刑](../Page/绞刑.md "wikilink")，12月23日在[东京](../Page/东京.md "wikilink")[巢鸭监狱执行绞刑时](../Page/巢鸭监狱.md "wikilink")，抽签抽到第一个接受绞刑。1978年10月入[靖国神社合祀](../Page/靖国神社.md "wikilink")，並祀於[殉国七士廟](../Page/殉国七士廟.md "wikilink")。

## 年譜

[thumb](../Page/file:1903_Doihara_Kenji.png.md "wikilink")
[thumb](../Page/file:Doihara_1940_41?.png.md "wikilink")

  - 1904年（[明治](../Page/明治.md "wikilink")37年）10月 -
    [日本陸軍士官學校畢業](../Page/日本陸軍士官學校.md "wikilink")（新制第16期）。
      - 11月 – 晉升少尉。[步兵第15聯隊附](../Page/步兵第15聯隊.md "wikilink")。
  - 1905年（明治38年）4月 - [步兵第49聯隊附](../Page/步兵第49聯隊.md "wikilink")。
  - 1907年（明治40年）6月 - 步兵第15聯隊附。
      - 12月 – 晉升中尉。
  - 1912年（[大正元年](../Page/大正.md "wikilink")）11月 -
    [日本陸軍大學校畢業](../Page/日本陸軍大學校.md "wikilink")（第24期）。
  - 1913年（大正2年）1月 - [参謀本部](../Page/参谋本部_\(大日本帝国\).md "wikilink")
    參謀附（駐在[北京](../Page/北京.md "wikilink")）。
      - 8月 – 晉升大尉。
  - 1918年（大正7年）6月 – 參謀本部員。
  - 1919年（大正8年）7月 – 晉升少佐。
  - 1920年（大正9年）1月 – 參謀本部員。
      - 4月 -
        [步兵第25聯隊](../Page/步兵第25聯隊.md "wikilink")[大隊長](../Page/营.md "wikilink")。參謀本部參謀官（出差[俄羅斯](../Page/俄羅斯.md "wikilink")[濱海省](../Page/濱海省.md "wikilink")、[中國](../Page/中國.md "wikilink")。）
  - 1921年（大正10年）6月 – 出差[歐美](../Page/歐美.md "wikilink")。
  - 1922年（大正11年）8月 – 參謀本部員。
      - 12月 – 參謀本部附（出差中國。板西機關補佐官。）
  - 1923年（大正12年）8月 -晉升中佐。
  - 1926年（大正15年）3月 - [步兵第2聯隊附](../Page/步兵第2聯隊.md "wikilink")。
  - 1927年（[昭和](../Page/昭和.md "wikilink")2年）3月 -
    [步兵第3聯隊附](../Page/步兵第3聯隊.md "wikilink")。
      - 4月 - 出差中國。
      - 7月26日 –
        晉升[大佐](../Page/大佐.md "wikilink")。[第1师团司令部軍官](../Page/第1师团_\(日本陆军\).md "wikilink")。
  - 1928年（昭和3年）3月20日 - [奉天督軍顧問](../Page/奉天.md "wikilink")。
  - 1929年（昭和4年）3月16日 - [步兵第30聯隊長](../Page/步兵第30聯隊.md "wikilink")。
  - 1931年（昭和6年）3月 –
    出差中國。[Kenji_Doihara_01.jpg](https://zh.wikipedia.org/wiki/File:Kenji_Doihara_01.jpg "fig:Kenji_Doihara_01.jpg")
      - 7月26日 - [關東軍司令部軍官](../Page/關東軍.md "wikilink")（奉天特務機關長）。
  - 1932年（昭和7年）1月26日 –
    關東軍司令部軍官（[哈爾濱市特務機關長](../Page/哈爾濱市.md "wikilink")）。
      - 4月11日 – 晉升少將。步兵第9旅團長。再也未回[滿洲](../Page/滿洲.md "wikilink")。
  - 1933年（昭和8年）10月16日 - 關東軍司令部附（奉天特務機關長）。
  - 1936年（昭和11年）3月7日 – 晉升中將。[第12師團司令部軍官](../Page/第12師團.md "wikilink")。
      - 3月23日 – [第1師團留守司令官](../Page/第1師團.md "wikilink")。
  - 1937年（昭和12年）3月1日 - [第14師團長](../Page/第14師團.md "wikilink")。
  - 1938年（昭和13年）6月18日 –
    參謀本部附。授命為[大本營仰付](../Page/大本營.md "wikilink")（土肥原機關長）。
  - 1939年（昭和14年）5月19日 - [第5軍司令官](../Page/第5军_\(日本陆军\).md "wikilink")。
  - 1940年（昭和15年）6月 – 參謀本部附。
      - 9月28日 - [軍事參議官](../Page/軍事參議院.md "wikilink")。
      - 10月28日 – 日本陸軍士官學校校長（兼任）。
  - 1941年（昭和16年）4月28日 – 晉升[陸軍大將](../Page/陸軍大將.md "wikilink")。
      - 6月9日 - 陸軍航空總監。
  - 1943年（昭和18年）5月1日 - [東部軍司令官](../Page/东部军_\(日本陆军\).md "wikilink")。
  - 1944年（昭和19年）3月22日 -
    [新加坡](../Page/新加坡.md "wikilink")[第7方面軍司令官](../Page/第7方面军_\(日本陆军\).md "wikilink")。
  - 1945年（昭和20年）4月7日 - 教育總監。
      - 8月25日 -
        [第12方面軍司令官兼東部軍管區司令官](../Page/第12方面军_\(日本陆军\).md "wikilink")。
      - 9月24日 - [第1總軍司令官](../Page/第一总军_\(日本陆军\).md "wikilink")（兼任）。
      - 10月1日 - 軍事參議官。
      - 11月30日 -編入[預備役](../Page/預備役.md "wikilink")。
      - 之後，土肥原賢二被駐日盟軍總司令部下令逮捕。

[Doihara_Kenji.jpg](https://zh.wikipedia.org/wiki/File:Doihara_Kenji.jpg "fig:Doihara_Kenji.jpg")

  - 1948年（昭和23年）
      - 11月12日 - 被遠東國際軍事法庭（東京審判）判為甲級戰犯，判處絞刑。
      - 12月23日 - 在东京巢鸭监狱执行绞刑时，抽签抽到第一个接受绞刑。
  - 1978年（昭和53年）10月 -
    被[靖國神社合祀](../Page/靖國神社.md "wikilink")，並被祀於[殉國七士廟](../Page/殉國七士廟.md "wikilink")。

## 評價

土肥原賢二是當時[日本的](../Page/日本.md "wikilink")「[中国通](../Page/中国通.md "wikilink")」，他熟讀《[三國](../Page/三國.md "wikilink")》、《[水滸傳](../Page/水滸傳.md "wikilink")》，了解中國民族性，因此土肥原重信義、尚承諾也是為人所知。抗日名將[馬-{占}-山即認為土肥原不騙人](../Page/馬占山.md "wikilink")；[宋哲元也评价土肥原說話算話](../Page/宋哲元.md "wikilink")；[德王痛罵](../Page/德穆楚克栋鲁普.md "wikilink")[日本人時如果扯到土肥原上](../Page/日本人.md "wikilink")，便說「他懂，他懂，他說話算話」\[2\]。
土肥原有兩個外號，中國人叫他“土匪原”、西方人稱為“東方的[勞倫斯](../Page/托马斯·爱德华·劳伦斯.md "wikilink")”。

## 其他

  - [歌人](../Page/歌人.md "wikilink")[佐伯裕子為土肥原贤二的孫女](../Page/佐伯裕子.md "wikilink")。

## 註釋

## 參考文獻

  - 『秘録　土肥原賢二　- 日中友好の捨石』、　土肥原賢二刊行会、　芙蓉書房、　1972年発行

## 外部連結

  - [Declassified Documents--RG 263: Name
    File](http://www.archives.gov/iwg/declassified-records/rg-263-cia-records/first-release-name-files.html)
  - [CIA Records - Name
    Files](http://www.archives.gov/iwg/declassified-records/rg-263-cia-records/second-release-name-files.html)
  - [Research Aid: Cryptonyms and Terms in Declassified CIA Files Nazi
    War Crimes and Japanese Imperial Government Records Disclosure
    Acts](http://www.archives.gov/iwg/declassified-records/rg-263-cia-records/second-release-lexicon.pdf)

{{-}}

[Category:日本陆军大将](../Category/日本陆军大将.md "wikilink")
[Category:日本陸軍情報將校](../Category/日本陸軍情報將校.md "wikilink")
[Category:甲級戰犯](../Category/甲級戰犯.md "wikilink")
[Category:被處決的日本人](../Category/被處決的日本人.md "wikilink")
[Category:日本第二次世界大戰人物](../Category/日本第二次世界大戰人物.md "wikilink")
[Category:岡山縣出身人物](../Category/岡山縣出身人物.md "wikilink")
[Category:日本戰爭罪定罪者](../Category/日本戰爭罪定罪者.md "wikilink")
[Category:日本反人類罪定罪者](../Category/日本反人類罪定罪者.md "wikilink")
[Category:日本中国通](../Category/日本中国通.md "wikilink")

1.  臼井勝美・稲葉正夫『現代史資料9日中戦争2』284頁, みすず書房
2.  《東北抗日畫史》p.94-p.95，蘭溪出版社，1980