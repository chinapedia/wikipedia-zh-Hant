**圣乔治**（；）斯拉夫化稱為**聖尤里**，著名的[基督教](../Page/基督教.md "wikilink")[殉道](../Page/殉道.md "wikilink")[圣人](../Page/基督教.md "wikilink")，[英格蘭的](../Page/英格蘭.md "wikilink")[守護聖者](../Page/守護聖者.md "wikilink")。经常以屠龙英雄的形象出现在西方文学、雕塑、绘画等领域。

## 简介

聖喬治生于[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")（也有一說聖喬治原籍[卡帕多細亞](../Page/卡帕多細亞.md "wikilink")），為[羅馬](../Page/羅馬.md "wikilink")[騎兵軍官](../Page/騎兵.md "wikilink")，因試圖阻止[戴克里先皇帝治下對](../Page/戴克里先.md "wikilink")[基督徒的迫害](../Page/基督徒.md "wikilink")，于303年被殺。494年，为[教宗哲拉修一世封聖](../Page/教宗哲拉修一世.md "wikilink")。1061年，在[英格蘭的](../Page/英格蘭.md "wikilink")[唐卡斯特](../Page/唐卡斯特.md "wikilink")，有一座[教堂獻給他](../Page/教堂.md "wikilink")。

自從[君士坦丁大帝重建羅馬帝國國内宗教和平後](../Page/君士坦丁大帝.md "wikilink")，東西各國都熱烈敬禮聖喬治。聖喬治為[英格蘭](../Page/英格蘭.md "wikilink")、[格魯吉亞](../Page/格魯吉亞.md "wikilink")、[莫斯科](../Page/莫斯科.md "wikilink")、[加泰羅尼亞](../Page/加泰羅尼亞.md "wikilink")、[馬爾他](../Page/馬爾他.md "wikilink")、[立陶宛](../Page/立陶宛.md "wikilink")、[衣索比亞](../Page/衣索比亞.md "wikilink")、[童軍運動](../Page/童軍.md "wikilink")、[士兵的](../Page/士兵.md "wikilink")[保護聖徒](../Page/主保聖人.md "wikilink")。

与聖喬治相關的[阿拉伯](../Page/阿拉伯.md "wikilink")[民間故事](../Page/民間故事.md "wikilink")，聖喬治屠龍與聖喬治救少女，有消滅獸性的壓迫者和拯救無防御者的概念。他的故事傳達了保護弱者、直面侵略者、犧牲成就聖潔的精神。

在[中亞的](../Page/中亞.md "wikilink")[東方教會基督徒的宗教狂熱中](../Page/聶斯脫里派教會.md "wikilink")，聖喬治看來佔有特殊的地位。在一篇譯自[敍利亞文的](../Page/敘利亞語.md "wikilink")[粟特文傳奇中](../Page/粟特語.md "wikilink")，聖喬治斥責佛教中所崇拜的[大神王模樣的神靈](../Page/大黑天.md "wikilink")。在敍利亞原版和其他版本中，被斥責的大神王被定性為「毀滅者」，意即等同《[新約](../Page/新約聖經.md "wikilink")·[啟示錄](../Page/啟示錄.md "wikilink")》第9章第11節中出現的[亞巴頓](../Page/亞巴頓.md "wikilink")。在[回鶻語文書中](../Page/回鶻語.md "wikilink")，也保存了聖喬治最後受難的故事，這個故事可追溯至一個敍利亞的傳說。其中包括受刑前的祈禱，並聲稱在需要時，祇要呼喚他的名字，就能獲得幫助。因此，聖喬治在中亞基督徒心目中的地位，堪比佛教中有求必應的[觀世音菩薩](../Page/觀世音菩薩.md "wikilink")。\[1\]

聖喬治在九世紀開始逐漸成為英格蘭文化的重要部分，儘管歷史中他似乎從未去過英格蘭。1222年4月23日，[聖喬治日成為](../Page/聖喬治日.md "wikilink")[公共假日](../Page/公共假日.md "wikilink")。[爱德華三世定其為](../Page/爱德華三世.md "wikilink")[嘉德騎士的保護聖徒](../Page/嘉德騎士.md "wikilink")，聖喬治[十字成為其軍隊的重要](../Page/十字.md "wikilink")[紋章](../Page/紋章.md "wikilink")。[莎士比亞在](../Page/莎士比亞.md "wikilink")《[亨利五世](../Page/亨利五世_\(戲劇\).md "wikilink")》中寫道：“”。今日，[英格蘭國旗為一白底紅色聖喬治十字](../Page/英格蘭國旗.md "wikilink")，通稱“[聖喬治旗](../Page/聖喬治十字.md "wikilink")”。[倫敦有](../Page/倫敦.md "wikilink")[聖喬治花園](../Page/聖喬治花園.md "wikilink")（），靠近[羅素廣場](../Page/羅素廣場.md "wikilink")。

聖喬治也是[加泰羅尼亞文化的一部分](../Page/加泰羅尼亞文化.md "wikilink")。在[加泰羅尼亞的](../Page/加泰羅尼亞.md "wikilink")[聖喬治日當天](../Page/聖喬治日.md "wikilink")，人們贈送書本給孩子，贈送鮮花予女子。當日書店售書附贈[玫瑰](../Page/玫瑰.md "wikilink")。

，[聯合國教科文組織將往後每年的](../Page/聯合國教科文組織.md "wikilink")4月23日定為[世界圖書與版權日](../Page/世界圖書與版權日.md "wikilink")。

## 圣乔治屠龙

[缩略图](https://zh.wikipedia.org/wiki/File:Martorell_-_Sant_Jordi.jpg "fig:缩略图")
「圣乔治屠龙」的故事各地口傳不同，比较完整的版本是：

某日，圣乔治到[利比亚去](../Page/利比亚.md "wikilink")，当地沼泽中的一只恶龙（可能是類似[鳄鱼的生物](../Page/鳄鱼.md "wikilink")）在水泉旁边筑巢，这水泉是Silene城唯一的水源，市民为了取水，每天都要把两头[绵羊献祭给恶龙](../Page/绵羊.md "wikilink")。到后来，绵羊都吃完了，只好用活人来替代，所以每天[抽签决定何人应选派作](../Page/抽签.md "wikilink")[牺牲](../Page/牺牲.md "wikilink")。

有一天，當地[酋長](../Page/酋長.md "wikilink")（或者国王）的女儿被抽中，酋長也没有办法，悲痛欲绝。当少女走近水源，正要被恶龙吞吃时，圣乔治赶到，提起利矛对抗恶龙，用[十字架保护自己不受伤害](../Page/十字架.md "wikilink")，并用腰带把它束缚住，牵到城里，对众人说，如果城里的所有市民都意[皈依](../Page/皈依.md "wikilink")[基督教](../Page/基督教.md "wikilink")，他就把这条龙殺了。市民们被他的义举感化，都放弃[异教](../Page/异教.md "wikilink")，改信[天主](../Page/天主.md "wikilink")。\[2\]

## 殉道

[缩略图](https://zh.wikipedia.org/wiki/File:Paolo_Veronese_023.jpg "fig:缩略图"),
1564\]\]
四世纪初，罗马皇帝[戴克里先掀起迫害基督徒的運動](../Page/戴克里先.md "wikilink")，許多基督徒被捕受刑。圣乔治为了鼓励信友坚守[信德](../Page/信德.md "wikilink")，就跑到广场上当众高呼：“外教人的神道乃是虚假。唯有我们的[天主](../Page/天主.md "wikilink")，才是创造天地万物的真主宰。”乔治被捕，被判受棒击与[烙铁灼烧之刑](../Page/烙铁.md "wikilink")。其后，总督手下的士兵们先后把圣乔治放在带尖刺的巨轮中、放在熔化的铅水桶中，但圣乔治都没有受伤。

最后总督在无计可施的情况下，決定用甜言蜜语诱骗圣乔治向[神像献祭](../Page/偶像崇拜.md "wikilink")。於是圣乔治将计就计，故意装作有些动摇的样子。神庙里挤满了看热闹的人群，都想要看这个倔强的教徒屈服。圣乔治在神庙里向天主[祈祷](../Page/祈祷.md "wikilink")，于是天上降下烈火烧毁了庙宇、神像。总督夫人见此，立即信奉了基督教。而总督仍执迷不悟，遂大怒，下令将圣乔治斩首。據說後來总督在回家路上也被降下的天火烧死。\[3\]\[4\]

## 艺术形象

  -
    *关于更完整的圣乔治艺术形象，参见英文条目*: [Saint George
    gallery](../Page/:en:Commons:Saint_George_structured_gallery.md "wikilink").

Image:George
novgorod.jpg|15世紀的聖喬治圖像，來自[大諾夫哥羅德](../Page/大諾夫哥羅德.md "wikilink")
<File:Raphael> - Saint George and the Dragon - Google Art
Project.jpg|由[拉斐爾於](../Page/拉斐爾.md "wikilink")1505年至1506間繪製的聖喬治
Image:Orthodox Bulgarian icon of St. George fighting the
dragon.jpg|藏於[保加利亞正教會的聖喬治與龍戰鬥圖](../Page/保加利亞正教會.md "wikilink")
<File:Sankt> georg sign.JPG|德國一間神學院大門上的聖喬治
Image:LifeofStGeorge.JPG|[保加利亞](../Page/保加利亞.md "wikilink")「[Kremikovtsi
Monastery](../Page/Kremikovtsi_Monastery.md "wikilink")」的聖喬治圖像
<File:Tübingen_-_Stiftskirche_Sankt_Georg_52329.jpg>|[圖賓根](../Page/圖賓根.md "wikilink")[聖喬治教堂的殉教者聖喬治](../Page/聖喬治教堂.md "wikilink")

Image:georgeladoga.jpg|俄羅斯[舊拉多加的教堂中](../Page/舊拉多加.md "wikilink")
<File:Moldavian> battle
flag.jpg|一份15世紀的[軍旗](../Page/軍旗.md "wikilink")，來自[摩爾達維亞公國的](../Page/摩爾達維亞公國.md "wikilink")[斯特凡三世](../Page/斯特凡三世.md "wikilink")

<File:1914> Sydney Half Sovereign - St. George.jpg|英國
[File:RR5216-0060R.png|俄羅斯](File:RR5216-0060R.png%7C俄羅斯)
<File:Tetarteron>
sb1975.jpg|[希臘](../Page/希臘.md "wikilink")[塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")
<File:St>. George the Victorious - Google Art
Project.jpg|[烏克蘭](../Page/烏克蘭.md "wikilink")，1700年

## 參見

  - [圣乔治节](../Page/圣乔治节.md "wikilink")

  - （9世紀描述喬治生平的[古高地德語詩篇](../Page/古高地德語.md "wikilink")）

  -
  - [聖騎士](../Page/聖騎士.md "wikilink")

## 参考文献

## 擴展閱讀

  - Brook, E.W., 1925. *Acts of Saint George* in series *Analecta
    Gorgiana* **8** (Gorgias Press).
  - Burgoyne, Michael H. 1976. *A Chronological Index to the Muslim
    Monuments of Jerusalem*. In *The Architecture of Islamic Jerusalem*.
    Jerusalem: The British School of Archaeology in Jerusalem.
  - Gabidzashvili, Enriko. 1991. *Saint George: In Ancient Georgian
    Literature*. Armazi – 89: Tbilisi, Georgia.
  - Good, Jonathan, 2009. *The Cult of Saint George in Medieval England*
    (Woodbridge, Suffolk: The Boydell Press).
  - Loomis, C. Grant, 1948. *White Magic, An Introduction to the
    Folklore of Christian Legend* (Cambridge: Medieval Society of
    America)
  - Natsheh, Yusuf. 2000. "Architectural survey", in *Ottoman Jerusalem:
    The Living City 1517–1917*. Edited by Sylvia Auld and Robert
    Hillenbrand (London: Altajir World of Islam Trust) pp 893–899.
  - Whatley, E. Gordon, editor, with Anne B. Thompson and Robert K.
    Upchurch, 2004. *St. George and the Dragon in the South English
    Legendary (East Midland Revision, c. 1400)* Originally published in
    *Saints' Lives in Middle English Collections* (Kalamazoo, Michigan:
    Medieval Institute Publications) ([on-line
    introduction](http://www.lib.rochester.edu/camelot/teams/whgeodintro.htm))
  - George Menachery, *Saint Thomas Christian Encyclopaedia of India*.
    Vol.II Trichur – 73.

## 外部連結

  - [St. George and the Dragon, free illustrated book based on 'The
    Seven Champions' by Richard Johnson
    (1596)](https://web.archive.org/web/20070429070715/http://www.stgeorgesholiday.com/st_george.asp)
  - [Archnet](https://web.archive.org/web/20060218063758/http://archnet.org/library/sites/one-site.tcl?site_id=5549)
  - [Saint George and the Dragon
    links](https://web.archive.org/web/20060313162521/http://www.isidore-of-seville.com/dragons/6.html)
    and
    [pictures](https://web.archive.org/web/20050610083809/http://www.isidore-of-seville.com/dragons/38.html)
    (more than 125), from [Dragons in Art and on the
    Web](https://web.archive.org/web/20050526223744/http://www.isidore-of-seville.com/dragons/)
  - [Story of St. George from The Golden
    Legends](https://web.archive.org/web/20080316102135/http://www.catholic-forum.com/saints/golden184.htm)
  - [Saint George and the Boy
    Scouts](https://web.archive.org/web/20020802231548/http://www.pinetreeweb.com/stgeorge.htm),
    including a woodcut of a Scout on horseback slaying a dragon
  - [A prayer for St George's
    Day](https://web.archive.org/web/20040602002709/http://www.digitas.harvard.edu/cgi-bin/wiki/ken/SaintGeorge)
  - [St.
    George](https://web.archive.org/web/20050320173756/http://www.niranamchurch.com/StGeorge.asp)
  - [St. George and the Dragon: An
    Introduction](http://www.lib.rochester.edu/camelot/teams/whgeodintro.htm)
  - [Greatmartyr, Victory-bearer and Wonderworker
    George](http://ocafs.oca.org/FeastSaintsViewer.asp?SID=4&ID=1&FSID=101184)
    Orthodox [icon](../Page/icon.md "wikilink") and
    [synaxarion](../Page/synaxarion.md "wikilink") for April 23
  - [Dedication of the Church of the Greatmartyr George in
    Lydia](http://ocafs.oca.org/FeastSaintsViewer.asp?SID=4&ID=1&FSID=103161)
    Icon and synaxarion for November 3
  - [Dedication of the Church of the Greatmartyr George at
    Kiev](http://ocafs.oca.org/FeastSaintsViewer.asp?SID=4&ID=1&FSID=103398)
    Icon and synaxarion for November 26
  - [St. George in the church in
    [Plášťovce](../Page/Plášťovce.md "wikilink"),([Palást](../Page/:hu:Palást_\(település\).md "wikilink"))
    in
    [Slovakia](../Page/Slovakia.md "wikilink")](http://www.pht.eoldal.hu/fotoalbum/palast-nevezetessegei/az-1898-ban-epult-templom/96)
  - [The St George Orthodox Military
    Association](http://www.orthodoxmilitary.org/)
  - [Famous Georgian Pilgrim Center in India St. George Orthodox Church
    Puthuppally, Kerala, India](http://www.puthuppallypally.in/)
  - [Hail
    George](http://cas.podomatic.com/entry/index/2010-04-22T20_03_59-07_00/)
    Radio webcast explains how Saint George came to be confused with
    some Afro-Brazilian deities
  - [Blog Article on the Feast of St.
    George](https://web.archive.org/web/20141203064602/http://blog.catholicfaithstore.com/blog/2013/04/25/the-feast-of-st-george/)
    The feast of St. George is April 23 - About that Dragon…
  - [St. George,
    Martyr](http://www.christianiconography.info/george.html) at the
    [Christian Iconography](http://www.christianiconography.info) web
    site.
  - [Of St. George,
    Martyr](http://www.christianiconography.info/goldenLegend/george.htm)
    from Caxton's translation of the Golden Legend

{{-}}

[Category:4世纪基督教殉道聖人](../Category/4世纪基督教殉道聖人.md "wikilink")
[G](../Category/303年逝世.md "wikilink")
[Category:英格兰文化](../Category/英格兰文化.md "wikilink")
[Category:加泰罗尼亚文化](../Category/加泰罗尼亚文化.md "wikilink")
[Category:東正教聖人](../Category/東正教聖人.md "wikilink")

1.
2.  摘自《天主教圣人列传》
3.  .
4.  .