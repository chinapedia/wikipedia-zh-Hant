**圣凯瑟琳修道院**（[希腊语](../Page/希腊语.md "wikilink")：）位于[埃及](../Page/埃及.md "wikilink")[西奈半岛南端的](../Page/西奈半岛.md "wikilink")[西乃山山脚](../Page/西乃山.md "wikilink")，是一間仍在服務基督徒的古舊[修道院](../Page/修道院.md "wikilink")，被[联合国教科文组织列为](../Page/联合国教科文组织.md "wikilink")[世界文化遗产](../Page/世界文化遗产.md "wikilink")。

修道院由6世紀時在位的東羅馬皇帝[查士丁尼一世下令興建](../Page/查士丁尼一世.md "wikilink")。

## 世界文化遗产

## [圣像画](../Page/圣像画.md "wikilink")

[StJohnClimacus.jpg](https://zh.wikipedia.org/wiki/File:StJohnClimacus.jpg "fig:StJohnClimacus.jpg")《[神圣的天梯](../Page/神圣的天梯.md "wikilink")》\]\]

## 参考文献

## 外部链接

  - [圣凯瑟琳修道院官方网站](http://www.sinaimonastery.com/)
  - [Saint Catherine
    Foundation](http://www.saintcatherinefoundation.org/)

## 参见

  - [亚历山大的加大肋纳](../Page/亚历山大的加大肋纳.md "wikilink")
  - [东正教](../Page/东正教.md "wikilink")
  - [修道院](../Page/修道院.md "wikilink")
  - [阿布米那](../Page/阿布米那.md "wikilink")

{{-}}

[Category:东正教修道院](../Category/东正教修道院.md "wikilink")
[Category:埃及基督教建筑物](../Category/埃及基督教建筑物.md "wikilink")
[S](../Category/埃及世界遺產.md "wikilink")