[Francois-Etienne_Villeret_St_Sulpice_Paris.jpg](https://zh.wikipedia.org/wiki/File:Francois-Etienne_Villeret_St_Sulpice_Paris.jpg "fig:Francois-Etienne_Villeret_St_Sulpice_Paris.jpg")

**圣叙尔比斯教堂**（[法语](../Page/法语.md "wikilink")：Saint-Sulpice）是坐落于[巴黎第六区的一座](../Page/巴黎.md "wikilink")[天主教](../Page/天主教.md "wikilink")[教堂](../Page/教堂.md "wikilink")。

## 教堂

教堂初建于[十三世纪](../Page/十三世纪.md "wikilink"),并于1646年开始重建。[奥地利](../Page/奥地利.md "wikilink")[女皇](../Page/女皇.md "wikilink")[安妮是重建工程的发起者](../Page/安妮.md "wikilink")，整个重建工程持续了130多年，很多著名的建筑师相继为重建做出了贡献（[加马尔](../Page/加马尔.md "wikilink")，[路易得福](../Page/路易得福.md "wikilink")，[吉塔尔等](../Page/吉塔尔.md "wikilink")）。重建工程由于资金问题在1678年到1718年之间中断。

古典式的正门由建筑师设计，但是它始终没有完工。

同样没有完工的还有1749年建造的南楼（由Oudot de Maclaurin设计）。
相反于南楼，北楼由建筑师夏尔格兰设计，并多次翻修，最终成为了巴黎最著名的灯塔之一。

### 管风琴

圣叙尔比斯教堂的大[管风琴同样由建筑师夏尔格兰设计](../Page/管风琴.md "wikilink")，建造于1776年到1781年，并于1862年得到翻修。

著名的风琴师有： [夏爾-瑪麗·魏多](../Page/夏爾-瑪麗·魏多.md "wikilink")（1870年到1933年的管风琴主管）和
Marcel Dupré（1934年到1971年的管风琴主管）

圣叙尔比斯教堂的大[管风琴是全法国甚至全欧洲最著名的乐器之一](../Page/管风琴.md "wikilink")，前文提及的两位著名的管风琴师主管了它长达1世纪。现今，人们仍然能在每个星期天听到圣叙尔比斯教堂里悠扬的琴声。
[Lutte_de_Jacob_avec_l'Ange.jpg](https://zh.wikipedia.org/wiki/File:Lutte_de_Jacob_avec_l'Ange.jpg "fig:Lutte_de_Jacob_avec_l'Ange.jpg")

### 圣叙尔比斯日晷

在教堂的北侧，有一个外形方尖碑状的[日晷](../Page/日晷.md "wikilink")。它由巴黎天文台的学者安装于[十八世纪](../Page/十八世纪.md "wikilink")。

### 艺术品

圣叙尔比斯教堂里也珍藏了不少艺术品，著名的有：

  - 皮加尔的祈祷台
  - 德拉克鲁瓦壁画（包括《雅各布与天使角力》）

## 圣叙尔比斯广场

[Fontaine_Saint-Sulpice_00.JPG](https://zh.wikipedia.org/wiki/File:Fontaine_Saint-Sulpice_00.JPG "fig:Fontaine_Saint-Sulpice_00.JPG")
圣叙尔比斯广场正对教堂的正门，建造于1754年。1844年，在广场中心建造了由Louis
Visconti设计的喷泉。喷泉四角的四个雕塑分别用来纪念[路易十四时期的四位主教](../Page/路易十四.md "wikilink")。在当地，这个喷泉也别称为四枢机喷泉，因为这四位主教最终都没能成为枢机，以表达遗憾之意。

## 文学上的借用

  - 著名犹太裔作家[佩莱克试图在其作品对](../Page/佩莱克.md "wikilink")《巴黎一处衰竭的尝试》（Tentative
    d'épuisement d'un lieu parisien）中列数所有在圣叙尔比斯广场上发生过的历史事件。

<!-- end list -->

  - 圣叙尔比斯教堂是著名畅销小说《[达芬奇密码](../Page/达芬奇密码.md "wikilink")》中的场景之一，小说中虚构说巴黎中心线穿过教堂。圣叙尔比斯教堂的游客数也因此大大增加。相反于[卢浮宫等小说中描述的现实场景](../Page/卢浮宫.md "wikilink")，影片《[达芬奇密码](../Page/达芬奇密码.md "wikilink")》并没有得到允许在圣叙尔比斯教堂里拍摄。圣叙尔比斯教堂对《[达芬奇密码](../Page/达芬奇密码.md "wikilink")》一片而言是虽然说是一个相当重要的地方，而且影片导演朗·霍华德也很想在那里摄制一出夜景戏。然而天主教会公开指责[丹·布朗的这部小说](../Page/丹·布朗.md "wikilink")，并拒绝了制片方在巴黎著名教堂内进行拍摄。

<!-- end list -->

  - 著名作家[阿纳托尔·法朗士的小说](../Page/阿纳托尔·法朗士.md "wikilink")《堕落天使》中也有发生在圣叙尔比斯教堂中的片断。

## 外部链接

  - [圣叙尔比斯教堂（照片）](https://web.archive.org/web/20060630054809/http://www.forbidden-places.be/explo46fr.php)
  - [达芬奇密码](http://www.portail-rennes-le-chateau.com/saint_sulpice_paris.htm)
  - [圣叙尔比斯教堂风琴乐（下载）](http://www.culture.gouv.fr/culture/cavaille-coll/videos_sons/daniel_roth_improvisation.mp3)
  - [圣叙尔比斯教堂风琴](http://www.uquebec.ca/musique/orgues/france/ssulpice.html)

[Category:巴黎教堂](../Category/巴黎教堂.md "wikilink")
[Category:法国天主教堂](../Category/法国天主教堂.md "wikilink")
[Category:巴黎第六区](../Category/巴黎第六区.md "wikilink")
[Category:包含子午线的建筑物](../Category/包含子午线的建筑物.md "wikilink")