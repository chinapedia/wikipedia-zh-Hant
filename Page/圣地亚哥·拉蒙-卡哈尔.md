[Cajal-mi.jpg](https://zh.wikipedia.org/wiki/File:Cajal-mi.jpg "fig:Cajal-mi.jpg")

**圣地亚哥·拉蒙-卡哈尔**（，），[西班牙](../Page/西班牙.md "wikilink")[病理学家](../Page/病理学.md "wikilink")、[组织学家](../Page/组织学.md "wikilink")，[神经学家](../Page/神經科學.md "wikilink")。生於[西班牙](../Page/西班牙帝國.md "wikilink")[阿拉貢](../Page/阿拉貢自治區.md "wikilink")[佩蒂利亞德阿拉貢](../Page/佩蒂利亞德阿拉貢.md "wikilink")，1906年[诺贝尔生理学或医学奖得主](../Page/诺贝尔生理学或医学奖.md "wikilink")。他对于大脑的微观结构研究是开创性的，被许多人认为是现代神经科学之父。他绘图技能出众，他的关于脑细胞的几百个插图至今用于教学。\[1\]

## 生平

拉蒙-卡哈尔是医师和解剖学讲师[斯托·拉蒙](../Page/斯托·拉蒙.md "wikilink")（）和安东尼·卡哈尔（）的儿子。孩提时期由于他不良行为和反专制的态度，被调到许多不同的学校。他的早熟和叛逆的一个极端的例子是在他11岁的时候，用自制的[大炮摧毁鄰居庭院的門](../Page/大炮.md "wikilink")，他也因此受到监禁。他是一个狂热的[画家](../Page/画家.md "wikilink")、[艺术家和](../Page/艺术家.md "wikilink")[体操](../Page/体操.md "wikilink")[运动员](../Page/运动员.md "wikilink")。他曾作为[鞋匠和](../Page/鞋匠.md "wikilink")[理发师](../Page/理发师.md "wikilink")，他好斗的态度颇为知名。

拉蒙-卡哈尔于1873年从[萨拉戈萨大学](../Page/萨拉戈萨大学.md "wikilink")[医学院毕业](../Page/医学院.md "wikilink")。通过考试后在[西班牙军队担任医务人员](../Page/西班牙.md "wikilink")。他参与了1874-75年到[古巴的远征](../Page/古巴.md "wikilink")，在那里他染上[疟疾和](../Page/疟疾.md "wikilink")[肺结核](../Page/肺结核.md "wikilink")。回到西班牙后，1879年，他于与Silveria
Fañanás
García结婚，并育有4个女儿和3个儿子。1881年，他被任命为[瓦伦西亚大学教授](../Page/瓦伦西亚大学.md "wikilink")。1877年，他在[马德里获得医学学位的博士](../Page/马德里.md "wikilink")。后来他在[巴塞罗那和马德里都得到教授称号](../Page/巴塞罗那.md "wikilink")。他是[萨拉戈萨博物馆主任](../Page/萨拉戈萨博物馆.md "wikilink")（1879年）、[国家卫生研究所主任](../Page/国家卫生研究所.md "wikilink")（1899年）。他是[卡哈尔研究所的创始人](../Page/卡哈尔研究所.md "wikilink")。1934年，他在马德里逝世。

## 著作

  - 《研究科學的第一步》（Reglas y consejos sobre investigación científica）

## 参考资料

## 參考文獻

  -
  -
  -
  -
## 外部連結

  - Fishman RS.（2007）. [The Nobel Prize
    of 1906](http://archopht.ama-assn.org/cgi/reprint/125/5/690). Arch
    Ophthalmol. 125:690-4.（Review of the work of the 1906 [Nobel Prize
    in Physiology or Medicine](../Page/诺贝尔生理学或医学奖.md "wikilink") winners
    Camillo Golgi and Santiago Ramon y Cajal）
  - [The Nobel Prize in Physiology or
    Medicine 1906](http://nobelprize.org/medicine/laureates/1906/index.html)
  - [Life and discoveries of
    Cajal](https://web.archive.org/web/20080709032226/http://nobelprize.org/medicine/articles/cajal/)
  - [Special Collection: Santiago Ramón y
    Cajal](https://web.archive.org/web/20100630081831/http://www.bib.ub.edu/recursos-informacio/colleccions/colleccions-especials/fons-ramon-y-cajal/)（University
    of Barcelona Library）
  - [Brief overview of Cajal's
    career](http://www.psu.edu/nasa/cajal.htm)

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:西班牙医学家](../Category/西班牙医学家.md "wikilink")
[Category:西班牙神经科学家](../Category/西班牙神经科学家.md "wikilink")
[Category:组织学家](../Category/组织学家.md "wikilink")
[Category:西班牙諾貝爾獎獲得者](../Category/西班牙諾貝爾獎獲得者.md "wikilink")
[Category:马德里康普顿斯大学校友](../Category/马德里康普顿斯大学校友.md "wikilink")
[Category:萨拉戈萨大学校友](../Category/萨拉戈萨大学校友.md "wikilink")
[Category:皇家学会外籍会员](../Category/皇家学会外籍会员.md "wikilink")

1.