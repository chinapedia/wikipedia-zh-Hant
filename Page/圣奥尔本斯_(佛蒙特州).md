**圣奥尔本斯**（**St.
Albans**）位于[美国](../Page/美国.md "wikilink")[佛蒙特州西北部](../Page/佛蒙特州.md "wikilink")，是[富兰克林县的县治所在](../Page/富兰克林县_\(佛蒙特州\).md "wikilink")，面积5.3平方公里。根据[2000年美国人口普查](../Page/2000年美国人口普查.md "wikilink")，共有7,650人，其中[白人占](../Page/白人.md "wikilink")95.87%、[土著美国人占](../Page/土著美国人.md "wikilink")1.2%。

## 外部链接

  - [St. Albans City Official Website](http://www.stalbansvt.com/)
  - [Ceramics Company Located In St. Albans](http://ceramics.net)
  - ["By Way of Canada," NARA,
    Fall 2000](http://www.archives.gov/publications/prologue/2000/fall/us-canada-immigration-records-1.html)
  - [Google Answers: Searching for
    Ancestors](http://answers.google.com/answers/threadview?id=199473)

[S](../Category/佛蒙特州城市.md "wikilink")