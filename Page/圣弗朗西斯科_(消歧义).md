**圣弗朗西斯科**（[西班牙语](../Page/西班牙语.md "wikilink")：****）是西班牙语地区的常见地名，得名自[方济会创始人](../Page/方济会.md "wikilink")[圣方济各](../Page/圣方济各_\(亚西西\).md "wikilink")，可能指下列其中一个地方：

## 地名

  - [美国](../Page/美国.md "wikilink")

<!-- end list -->

1.  [加利福尼亚州的](../Page/加利福尼亚州.md "wikilink")[圣弗朗西斯科](../Page/旧金山.md "wikilink")（又译**旧-{}-金山**、**三藩-{}-市**）
2.  [科罗拉多州的](../Page/科罗拉多州.md "wikilink")[圣弗朗西斯科
    (科罗拉多)](../Page/圣弗朗西斯科_\(科罗拉多\).md "wikilink")
3.  [新墨西哥州的](../Page/新墨西哥州.md "wikilink")[圣弗朗西斯科
    (新墨西哥)](../Page/圣弗朗西斯科_\(新墨西哥\).md "wikilink")
4.  [得克萨斯州的](../Page/得克萨斯州.md "wikilink")[圣弗朗西斯科
    (得克萨斯)](../Page/圣弗朗西斯科_\(得克萨斯\).md "wikilink")

<!-- end list -->

  - [阿根廷](../Page/阿根廷.md "wikilink")

<!-- end list -->

1.  [科尔多瓦省的](../Page/科尔多瓦省.md "wikilink")[圣弗朗西斯科
    (阿根廷)](../Page/圣弗朗西斯科_\(阿根廷\).md "wikilink")

<!-- end list -->

  - [哥伦比亚](../Page/哥伦比亚.md "wikilink")

<!-- end list -->

1.  [San Francisco,
    Antioquia](../Page/San_Francisco,_Antioquia.md "wikilink")
2.  [San Francisco,
    Putumayo](../Page/San_Francisco,_Putumayo.md "wikilink")
3.  [San Francisco,
    Cundinamarca](../Page/San_Francisco,_Cundinamarca.md "wikilink")

<!-- end list -->

  - [哥斯达黎加](../Page/哥斯达黎加.md "wikilink")

<!-- end list -->

1.  [圣弗朗西斯科 (哥斯达黎加)](../Page/圣弗朗西斯科_\(哥斯达黎加\).md "wikilink")

<!-- end list -->

  - [古巴](../Page/古巴.md "wikilink")

<!-- end list -->

1.  [San Francisco de
    Paula](../Page/San_Francisco_de_Paula.md "wikilink")

<!-- end list -->

  - [多米尼加](../Page/多米尼加.md "wikilink")

<!-- end list -->

1.  [San Francisco de
    Macoris](../Page/San_Francisco_de_Macoris.md "wikilink")

<!-- end list -->

  - [墨西哥](../Page/墨西哥.md "wikilink")

<!-- end list -->

1.  [San Francisco
    Culhuacán](../Page/San_Francisco_Culhuacán.md "wikilink")
2.  [San Francisco del Oro](../Page/San_Francisco_del_Oro.md "wikilink")
3.  [San Francisco del
    Rincón](../Page/San_Francisco_del_Rincón.md "wikilink")

<!-- end list -->

  - [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")

<!-- end list -->

1.  [圣弗朗西斯科 (尼加拉瓜)](../Page/圣弗朗西斯科_\(尼加拉瓜\).md "wikilink")

<!-- end list -->

  - [菲律宾](../Page/菲律宾.md "wikilink")

<!-- end list -->

1.  [南阿古桑省的](../Page/南阿古桑省.md "wikilink")[圣弗朗西斯科
    (南阿古桑省)](../Page/圣弗朗西斯科_\(南阿古桑省\).md "wikilink")
2.  [宿霧省的](../Page/宿霧省.md "wikilink")[圣弗朗西斯科
    (宿务)](../Page/圣弗朗西斯科_\(宿务\).md "wikilink")
3.  [奎松省的](../Page/奎松省.md "wikilink")[圣弗朗西斯科
    (奎松省)](../Page/圣弗朗西斯科_\(奎松省\).md "wikilink")
4.  [南萊特省的](../Page/南萊特省.md "wikilink")[聖法蘭西斯科
    (南萊特省)](../Page/聖法蘭西斯科_\(南萊特省\).md "wikilink")
5.  [北蘇里高省的](../Page/北蘇里高省.md "wikilink")[聖法蘭西斯科
    (北蘇里高省)](../Page/聖法蘭西斯科_\(北蘇里高省\).md "wikilink")

<!-- end list -->

  - [波多黎各](../Page/波多黎各.md "wikilink")

<!-- end list -->

1.  [圣弗朗西斯科 (波多黎各)](../Page/圣弗朗西斯科_\(波多黎各\).md "wikilink")

<!-- end list -->

  - [萨尔瓦多](../Page/萨尔瓦多.md "wikilink")

<!-- end list -->

1.  [圣弗朗西斯科 (萨尔瓦多)](../Page/圣弗朗西斯科_\(萨尔瓦多\).md "wikilink")

<!-- end list -->

  - [西班牙](../Page/西班牙.md "wikilink")

<!-- end list -->

1.  临近[毕尔巴鄂的](../Page/毕尔巴鄂.md "wikilink")[圣弗朗西斯科
    (西班牙)](../Page/圣弗朗西斯科_\(西班牙\).md "wikilink")

<!-- end list -->

  - [委内瑞拉](../Page/委内瑞拉.md "wikilink")

<!-- end list -->

1.  [San Francisco de
    Macaira](../Page/San_Francisco_de_Macaira.md "wikilink")

<!-- end list -->

  - 巴西

<!-- end list -->

1.  [圣弗朗西斯科 (巴西)](../Page/圣弗朗西斯科_\(巴西\).md "wikilink")

## 建筑物

  -