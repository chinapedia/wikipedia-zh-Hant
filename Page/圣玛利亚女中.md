**圣玛利亚女中**（St. Mary's
Hall）是[美国](../Page/美国.md "wikilink")[圣公会在](../Page/圣公会.md "wikilink")[上海创办的一所著名的教会女子中学](../Page/上海.md "wikilink")。其渊源可以追溯到1851年，创办于[虹口文监师路和百老汇路口](../Page/虹口.md "wikilink")（今[塘沽路和](../Page/塘沽路.md "wikilink")[大名路口](../Page/大名路.md "wikilink")）圣公会救主堂后的文纪女塾，是一所带有慈善性质的小学。

1881年6月，正式成立了圣玛利亚女校。新校舍建在梵王渡[圣约翰书院后面](../Page/圣约翰书院.md "wikilink")，与圣约翰书院仅一墙之隔。学制定为8年。

1923年，圣玛利亚女校迁入[白利南路](../Page/白利南路.md "wikilink")（[公共租界在沪西的](../Page/公共租界.md "wikilink")[越界筑路](../Page/上海公共租界越界筑路.md "wikilink")，今长宁路1187号）新校舍，并改名圣玛利亚女子中学。占地面积64000平方米。

由于学费昂贵，招来的学生多为中上等家庭的女孩子，圣玛利亚女中被称为贵族教会女校，与[中西女中](../Page/中西女中.md "wikilink")（美国[卫理公会创办](../Page/卫理公会.md "wikilink")，在沪西[忆定盘路](../Page/忆定盘路.md "wikilink")，也就是今天的江苏路）齐名。

圣玛利亚女中的特色，在于[英文](../Page/英文.md "wikilink")、[家政和](../Page/家政.md "wikilink")[音乐](../Page/音乐.md "wikilink")[舞蹈](../Page/舞蹈.md "wikilink")；所以培养出来的女学生，大都谙熟[社交礼仪](../Page/社交礼仪.md "wikilink")、通晓英文，富于[文学](../Page/文学.md "wikilink")[艺术的修养](../Page/艺术.md "wikilink")，具有[上流社会](../Page/上流社会.md "wikilink")[淑女的风范](../Page/淑女.md "wikilink")。

1952年，[上海市教育局接管了圣玛利亚女中和中西女中](../Page/上海市教育局.md "wikilink")，把两校合并成为[上海市第三女子中学](../Page/上海市第三女子中学.md "wikilink")。原圣玛利亚女中的校址办起了上海纺织专科学校。1999年，该校被并入[东华大学](../Page/东华大学_\(上海\).md "wikilink")，成为东华大学纺织学院长宁分校区。

圣玛利亚女中的校园长期比较完整地保留了昔日的风格，有爬满[常春藤的钟楼](../Page/常春藤.md "wikilink")、环绕操场一圈的走廊等。被列为上海市第四批优秀历史建筑。2005年-2006年，1号教学楼和一幢宿舍楼乃至回廊均被开发商拆除，另外四幢保留建筑的屋顶亦被拆除，只有钟楼幸存\[1\]。2009年经媒体曝光后，
引发社会关注。随后，长宁区规划局表示，将责令开发商恢复被破坏的回廊及4幢保留建筑\[2\]，经研究决定保留钟楼，原地或异地重建其余所有建筑\[3\]。

## 著名校友

圣玛利亚女中的著名校友包括[张爱玲等人](../Page/张爱玲.md "wikilink")。其余見[上海市第三女子中学](../Page/上海市第三女子中学.md "wikilink")。

## 参考文献

{{-}}

[Category:教會創建的中國學校](../Category/教會創建的中國學校.md "wikilink")
[Category:上海前教会中学](../Category/上海前教会中学.md "wikilink")
[Category:中国女子中学](../Category/中国女子中学.md "wikilink")
[Category:1851年創建的教育機構](../Category/1851年創建的教育機構.md "wikilink")
[Category:1952年廢除](../Category/1952年廢除.md "wikilink")

1.  [张爱玲母校被开发商破坏
    成建筑工地](http://arch.m6699.com/content.php?fid=247&aid=17492)
2.  [张爱玲母校圣玛利亚女校将修复](http://arch.m6699.com/content-17757.htm)
3.  [长宁区88街坊32/8丘地块内保留历史建筑改造专家评审会议纪要](http://www.changning.sh.cn/govdiropen/jcms_files/jcms1/web21/site//attach/0/c9754b71ee6c4a76918477d99c619b37.pdf)