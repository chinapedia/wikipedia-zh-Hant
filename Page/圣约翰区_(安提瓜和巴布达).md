**聖約翰區**是[安地卡及巴布達](../Page/安地卡及巴布達.md "wikilink")[安地卡島的行政區](../Page/安地卡島.md "wikilink")，位於該島西北部，與聖約翰以外的各行政區相鄰。首都[聖約翰位於該區](../Page/聖約翰_\(安地卡及巴布達\).md "wikilink")。

## 地理

該區包括該國首都[聖約翰](../Page/圣约翰_\(安提瓜和巴布达\).md "wikilink")。

其他聚居地包括：[眾聖村](../Page/眾聖村.md "wikilink")、Renfrew、Aberdeen、Bendals、Branns
Hamlet、Buckleys、Cooks New Extension、Cooks
Hill、[錫達格羅夫](../Page/錫達格羅夫.md "wikilink")、Emanuel、[Five
Islands](../Page/Five_Islands,_Antigua_and_Barbuda.md "wikilink")、Grays
Farm、Gray Hill、Green Bay、戈爾登格羅夫、Nut Grove、維拉、Gamble's Terrace、Upper
Gamble's、Weatherhills和Tomlinson。

[Category:安提瓜和巴布達行政區劃](../Category/安提瓜和巴布達行政區劃.md "wikilink")