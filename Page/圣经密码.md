**圣经密码**，也称作Torah密码
（[妥拉](../Page/妥拉.md "wikilink")，[英文](../Page/英文.md "wikilink")：Torah，可泛指猶太教的全部律法教條，尤指《猶太聖經》的首五卷書），最初指的是在《[希伯来圣经](../Page/希伯来圣经.md "wikilink")·[创世记](../Page/创世记.md "wikilink")》的开头每隔50个字母跳读，就可以拼出“Torah”一词（意指《[摩西五经](../Page/摩西五经.md "wikilink")》，即《[创世记](../Page/创世记.md "wikilink")》、《[出埃及记](../Page/出埃及记.md "wikilink")》、《[利未记](../Page/利未记.md "wikilink")》、《[民数记](../Page/民数记.md "wikilink")》及《[申命记](../Page/申命记.md "wikilink")》），另外在《出埃及记》、《民数记》和《申命记》中亦是如此。这种现象后来被称做等距字母序列（Equidistant
letter sequences），简称“ELS”。这个密码，由于*The Bible
Code*一书的出版而闻名于世，书中作者声称这些密码可以预言将来。此論受到各方專家和许多宗教团体强烈質疑，且也不被[教廷所認可](../Page/教廷.md "wikilink")。

## 概述

最先出自13世纪一位名叫Bachayah的[犹太教](../Page/犹太教.md "wikilink")[拉比的](../Page/拉比.md "wikilink")-{著}-作。20世纪初，这一内容被居住在[捷克首都](../Page/捷克.md "wikilink")[布拉格的一位犹太教教士Michael](../Page/布拉格.md "wikilink")
Ber Weissmandl发现。并在他死后的1957年，由他的学生将其公之于众。

到了1980年代，[以色列希伯来大学的](../Page/以色列.md "wikilink")[数学家Eliyahu](../Page/数学家.md "wikilink")
Rips和[物理学家Doron](../Page/物理学家.md "wikilink")
Witstum利用计算机高速计算对比（一套精密的数学运算模式），挑選[圣经时代以来的](../Page/圣经时代.md "wikilink")32位知名人物，结果发现他们的名字和出生与死亡日期在《创世记》中都是编在一起的。后来他们把整本[希伯来文圣经原文去除了所有字间距](../Page/希伯来文.md "wikilink")，连贯成总长304805个字（因为根据传说，[摩西从上帝手中接受的圣经就是](../Page/摩西.md "wikilink")“字字相连，无一中断”），采用计算机跳跃码方式，在字符串中寻找名字、单词和词组，最终找到了一系列相关信息。据此完成了《聖經密碼》，经过[耶鲁大学](../Page/耶鲁大学.md "wikilink")、[哈佛大学](../Page/哈佛大学.md "wikilink")、[希伯来大学多名数学家验证](../Page/希伯来大学.md "wikilink")，以及[美国](../Page/美国.md "wikilink")*Statistical
Science*杂志（Institute of Mathematical
Statistics的機關期刊之一）的三次复核后（他们经过数学分析，证实圣经密码为巧合的可能性只有二十五亿分之一，后来研究人员以更高难度测试，发现为巧合的可能性低至五万兆分之一），于1994年8月，正式发表在了*Statistical
Science*杂志上（对于这篇论文的争论参见下面的“各方观点”）。

1997年面世的《[聖經密碼](../Page/聖經密碼_\(書\).md "wikilink")》（*The Bible
Code*）引起了世人对圣经密码的关注，作者Michael
Drosnin是一名无宗教信仰的记者，曾於《[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")》和《[华尔街日报](../Page/华尔街日报.md "wikilink")》工作，他经过5年的采访和深入调查研究后写成此书。他甚至通过密码找出了“Year
of Bible Code Revealed 1997”的词组。但Eliyahu
Rips在一篇文告中宣称：“我本人并不支持Drosnin对读码的见解，也不赞成他所下的结论……凡是从Torah（摩西五经）中擷取信息或以其为预言的根据，是无益和没有价值的。这不仅是我个人的意见，也是每一位从事解码研究的科学家的意见。”到目前为止，他已出版了续集《聖經密碼II》（*The
Bible Code Ⅱ*），其副标题为“倒數计时”（*The
Countdown*）。在文中他做的最后结论是：“圣经密码可能既不是‘正确的’，也不是‘错误的’。密码要告诉我们的，可能是‘什么事可能发生’，而不是‘什么事会发生’。不过，因我们不能让我们的世界毁灭掉，我们不能什么事都不做，只是在那里等待——我们必须假定，圣经密码里头的警告是真的”。但如果真有[上帝](../Page/上帝.md "wikilink")，祂的旨意人類又能如何呢？

## 密码原理

从圣经第一个字母开始，寻找一种可能的跳跃[序列](../Page/序列.md "wikilink")，从1、2、3个字母，依序到跳过数千个字母，看能拼出什么字，然后再从第2个字母开始，周而复始。一直到圣经最后一个字母。

  - 例如**R**ips **E**xpl**A**ine**D** tha**T** eac**H** cod**E** is a
    **C**ase **O**f ad**D**ing **E**very fourth or twelth or fiftieth to
    form a word得出隐含讯息为**READ THE CODE**。

找到关键词后，接着在关键字附近寻找相关讯息。

  - 例如寻找“[伊扎克·拉宾](../Page/伊扎克·拉宾.md "wikilink")”，结果这个名字只出现一次，跳跃序列为4772。于是程序将整本圣经分成64行，每行4772个字母。就在重新编排的圣经裡，与“伊扎克·拉宾”相交的地方，找到“刺客将行刺”，后又找到“艾米尔”（刺客的名字）。用这种方法还能找到“拉宾遇刺”、“特拉维夫”、“5756”（[希伯来年](../Page/希伯来年.md "wikilink")，始于公历1995年9月）这些单词。

## 宣稱已破译的密码

  - 1929——经济崩溃——[股票](../Page/股票.md "wikilink")——大萧条（申命记）
  - [爱迪生](../Page/湯瑪斯·愛迪生.md "wikilink")——[电](../Page/电.md "wikilink")——[灯](../Page/灯.md "wikilink")（民数记）
  - [爱因斯坦](../Page/爱因斯坦.md "wikilink")——他推翻现有的事实——[科学](../Page/科学.md "wikilink")——预告一位聪明绝顶的人——崭新而卓绝的知解（民数记）
  - [希特勒](../Page/希特勒.md "wikilink")——恶人——[纳粹与敌人](../Page/纳粹.md "wikilink")——屠杀（创世记）
  - [休梅克列維九號](../Page/休梅克列維九號.md "wikilink")——[木星](../Page/木星.md "wikilink")——第八阿伏月（以赛亚书）
  - [肯尼迪](../Page/肯尼迪.md "wikilink")——第二位治国者遇害——[达拉斯](../Page/达拉斯.md "wikilink")（出埃及记）
  - [克林顿](../Page/克林顿.md "wikilink")——[总统](../Page/总统.md "wikilink")（民数记）
  - [莎士比亚](../Page/莎士比亚.md "wikilink")——[哈姆雷特](../Page/哈姆雷特.md "wikilink")——[麦克白](../Page/麦克白.md "wikilink")（利未记）
  - [贝多芬](../Page/贝多芬.md "wikilink")——[约翰·巴赫](../Page/约翰·塞巴斯蒂安·巴赫.md "wikilink")——[德国作曲家](../Category/德國音樂家.md "wikilink")
  - [莫扎特](../Page/莫扎特.md "wikilink")——[音乐](../Page/音乐.md "wikilink")——[作曲家](../Page/作曲家.md "wikilink")
  - [毕加索](../Page/毕加索.md "wikilink")——[艺术家](../Page/艺术家.md "wikilink")
  - [莱特兄弟](../Page/莱特兄弟.md "wikilink")——[飞机](../Page/飞机.md "wikilink")
  - [薩達姆·海珊](../Page/薩達姆·海珊.md "wikilink")——[飞毛腿](../Page/飞毛腿.md "wikilink")
  - [中国](../Page/中国.md "wikilink")——大使馆——轰炸——[北约](../Page/北约.md "wikilink")——[美国](../Page/美国.md "wikilink")

<!-- end list -->

  - [台湾](../Page/台湾.md "wikilink")——[地震](../Page/地震.md "wikilink")——[台北](../Page/台北.md "wikilink")——5760(西元1999年，民國88年)
  - [奥姆真理教](../Page/奥姆真理教.md "wikilink")——[灾疫](../Page/灾疫.md "wikilink")——[毒气](../Page/毒气.md "wikilink")

## 各方观点

许多人相信圣经裏藏有密码——人类几千年来曾发生的事件和未来将要发生的事。也有人因此相信圣经当然是出自上帝之手。并认为圣经可能是“外星遗物”，设计成唯有地球文明达到一定的进化门坎时，才会自行示现的形式。

圣经密码的支持者认为，虽然任何一本书都可以找到随机字母组合，但要找到像“萨达姆”和“飞毛腿”及开战日期等相关讯息，除了圣经外，包括《[战争与和平](../Page/战争与和平.md "wikilink")》在内的各类十万甚至百万字母的书籍，和千百万种电脑制造的实验个案，都没有找到如此连贯的讯息。

反对者则认为，希伯来文圣经自古以来均出现许多版本，几千年来的抄写必定会有不同的字句，解码所研究的圣经原文已经跟古抄本有一定的差别，包括句点和字距等都不一样。而且所解码的希伯来文圣经，只有[辅音](../Page/辅音.md "wikilink")[字母](../Page/字母.md "wikilink")，没有[元音字母](../Page/元音.md "wikilink")。因此它完全不值得相信。另外，这种解码方式，实际上在传统的[犹太神秘哲学卡巴拉](../Page/犹太神秘哲学.md "wikilink")（Cabala）里关于如何用数学运算法解释希伯来文圣经时已经提出。整本圣经希伯来文有30多万个字母，起码可以发生100亿种字母组合，所谓的密码只是一种断章取义。

还有一些人的质疑来自一些没能应验的预言，比如Michael
Drosnin根据密码找到圣经关于1995年9月至1996年9月会发生“核武浩劫”并没有发生。据说他再用计算机程序找到结果出现Delay（延迟）的单词。

1999年，数学家Brendan McKay、Dror Bar-Natan和Gil Kalai及心理学家Maya
Bar-Hillel，在*Statistical
Science*上发表了一篇论文，声明他们提供了足够的证据来驳斥Witztum和Rips的论文。他们的主要观点是：

  - Witztum和Rips采用的数据是希伯来文的[拉宾名字](../Page/拉宾.md "wikilink")。希伯来文对于名字的拼写是很灵活的，每一个名字可以有很多写法。因此在搜索名字的时候要更加注意。所以他们的结果是不严格的。论文中说：“……他们的数据没有按照他们实验时的规则严格定义。相反，对于这个规则，他们应用时，有很多‘摆动’。特别是对于拉宾的名字。”
  - 对于他们的数据采集，有间接的证据表明是不正确的。也就是说名字的选择和拼写是不中立的，尤其向他们的假设偏向。
  - 对于再现Witztum和Rips论文陈述结果的尝试失败了。论文中说：“原来论文的作者不能提供他们最初的源程序。而且现在原作者发布的源程序（两个），还有我们自己的程序，都得不到原来论文的结果。”

对此，还有进一步的争论。尽管还有少数科学家在继续这个工作，但是现在绝大部分科学家对此持否定的态度。

### 《白鯨記》中的訊息

[Moby_Dick_code.gif](https://zh.wikipedia.org/wiki/File:Moby_Dick_code.gif "fig:Moby_Dick_code.gif")

許多人發現，Michael
Drosnin用的方法和等距字母序列那篇論文的方法相比，相當不嚴密。不少人用相同的方法，很容易發現到處都藏有密碼，就如英王欽定版的《聖經》裏，可以找到
[UFO](../Page/UFO.md "wikilink") 一樣，這下子整個懷疑都出來了。Michael
Drosnin面對這些批評，在《[新聞周刊](../Page/新聞周刊.md "wikilink")》的一次訪問裡，他說：「假如我的批評者，能夠在《[白鯨記](../Page/白鯨記.md "wikilink")》裡，找到某位總理被刺殺的密碼訊息，那麼我就會相信他們。」\[1\]這對批評者來說，是個挑戰，而這場戰爭到這個時候，已經是相當白熱化了。

[澳洲國立大學的一位計算機教授Brendan](../Page/澳洲國立大學.md "wikilink")
McKay，就接受這個挑戰，找到了底下[印度總理](../Page/印度總理.md "wikilink")[英迪拉·甘地被刺的](../Page/英迪拉·甘地.md "wikilink")「訊息」，並且把它放在自己的網站上。直行的
IGANDHI，第一個 I 是他的名字 Indira 的縮寫，按著是甘地 (Gandhi)。按著橫行是 the bloody
deed。死亡的契約，預示著甘地是會被殺的。\[2\]

## 参看

  - [圣经](../Page/圣经.md "wikilink")
  - [艾萨克·牛顿](../Page/艾萨克·牛顿.md "wikilink")
  - [等距字母序列](../Page/等距字母序列.md "wikilink")
  - [隐写术](../Page/隐写术.md "wikilink")
  - [燒餅歌](../Page/燒餅歌.md "wikilink")
  - [諾斯特拉達姆士](../Page/諾斯特拉達姆士.md "wikilink")

## 相关书籍或论文

  - Yacov Rambsel: Yeshua-The Name of Jesus Revealed in The Old
    Testament
  - Grant R. Jeffrey: Signature of God
  - Michael Drosnin: The Bible Code. Simon & Schuster, 1997. ISBN
    0684810794.
  - Jeffrey Satinover, MD: "Cracking the Bible Code". Wm. Morrow, 1997.
    ISBN 0688154638.
  - D. Witztum, E. Rips and Y. Rosenberg, Equidistant letter sequences
    in the Book of Genesis, Statistical Science, 9 (1994) 429-438.
  - [B. McKay, D. Bar-Natan, M. Bar-Hillel, G. Kalai, Solving the Bible
    Code Puzzle, Statistical Science, 13
    (1999) 150-173](http://cs.anu.edu.au/~bdm/dilugim/StatSci/)

## 參考資料

<references />

## 外部链接

  - [聖經真的藏有密碼嗎？](http://episte.math.ntu.edu.tw/articles/sm/sm_32_01_1/index.html)（《[科學月刊](../Page/科學月刊.md "wikilink")》第三十二卷第一期）
  - [犹太人与圣经密码](http://www.cclw.net/book/xinyangwenda/html/chapter17.html)
  - [圣经密码](https://web.archive.org/web/20050328044005/http://home.pacific.net.hk/~kinnik/biblecode.html)
  - [牛顿手稿骇人预言2060年世界毁灭](https://web.archive.org/web/20080929041230/http://popul.jqcq.com/focus/2003-02/1046070590.html)
  - [本土台灣人聖經密碼研究站](http://www.taconet.com.tw/inquisit/)

[圣经密码](../Category/圣经密码.md "wikilink")
[Category:數秘術](../Category/數秘術.md "wikilink")
[Category:妥拉](../Category/妥拉.md "wikilink")

1.
2.