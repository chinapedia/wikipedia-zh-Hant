**圣艾蒂安**（法语：**Saint-Étienne**）又写**圣太田**，位于[法国中东部](../Page/法国.md "wikilink")[罗讷-阿尔卑斯大区](../Page/罗讷-阿尔卑斯大区.md "wikilink")，是[卢瓦尔省的首府](../Page/卢瓦尔省.md "wikilink")。

## 历史

聖艾蒂安是[聖斯德望的法語稱呼](../Page/聖斯德望.md "wikilink")，城市是在一座紀念這位基督教第一位殉道者的教堂為中心建設的。

聖艾蒂安是在[中世紀時才有歷史記載](../Page/中世紀.md "wikilink")，當時僅是小市鎮。在十六世紀開始，工業開始發展，最先是武器製造，到十七世紀則有金飾或絲帶製造；到十九世紀，又成為採礦中心，其間人口由1832年的三萬多人，上升至1880年的十一萬。

## 运动

[法国足球甲级联赛球队](../Page/法国足球甲级联赛.md "wikilink")[圣艾蒂安足球俱乐部就位于该市](../Page/圣艾蒂安足球俱乐部.md "wikilink")。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Algeria.svg" title="fig:Flag_of_Algeria.svg">Flag_of_Algeria.svg</a><a href="../Page/阿尔及利亚.md" title="wikilink">阿尔及利亚</a><a href="../Page/安纳巴.md" title="wikilink">安纳巴</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Tunisia.svg" title="fig:Flag_of_Tunisia.svg">Flag_of_Tunisia.svg</a><a href="../Page/突尼斯.md" title="wikilink">突尼斯</a><a href="../Page/本阿鲁斯.md" title="wikilink">本阿鲁斯</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_England.svg" title="fig:Flag_of_England.svg">Flag_of_England.svg</a><a href="../Page/英格兰.md" title="wikilink">英格兰</a><a href="../Page/考文垂.md" title="wikilink">考文垂</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg" title="fig:Flag_of_the_United_States.svg">Flag_of_the_United_States.svg</a><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/得梅因_(艾奥瓦州).md" title="wikilink">得梅因</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg" title="fig:Flag_of_Italy.svg">Flag_of_Italy.svg</a><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/费拉拉.md" title="wikilink">费拉拉</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/格爾滕多夫.md" title="wikilink">格爾滕多夫</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg" title="fig:Flag_of_Canada.svg">Flag_of_Canada.svg</a><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/格兰比_(魁北克省).md" title="wikilink">格兰比</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Poland.svg" title="fig:Flag_of_Poland.svg">Flag_of_Poland.svg</a><a href="../Page/波兰.md" title="wikilink">波兰</a><a href="../Page/卡托维兹.md" title="wikilink">卡托维兹</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg" title="fig:Flag_of_Ukraine.svg">Flag_of_Ukraine.svg</a><a href="../Page/乌克兰.md" title="wikilink">乌克兰</a><a href="../Page/卢甘斯克.md" title="wikilink">卢甘斯克</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Israel.svg" title="fig:Flag_of_Israel.svg">Flag_of_Israel.svg</a><a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/拿撒勒.md" title="wikilink">拿撒勒</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Portugal.svg" title="fig:Flag_of_Portugal.svg">Flag_of_Portugal.svg</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/奥埃拉什.md" title="wikilink">奥埃拉什</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Greece.svg" title="fig:Flag_of_Greece.svg">Flag_of_Greece.svg</a><a href="../Page/希腊.md" title="wikilink">希腊</a><a href="../Page/帕特雷.md" title="wikilink">帕特雷</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Madagascar.svg" title="fig:Flag_of_Madagascar.svg">Flag_of_Madagascar.svg</a><a href="../Page/马达加斯加.md" title="wikilink">马达加斯加</a><a href="../Page/图阿马西纳.md" title="wikilink">图阿马西纳</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg" title="fig:Flag_of_Canada.svg">Flag_of_Canada.svg</a><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/温莎_(安大略省).md" title="wikilink">温莎</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/伍珀塔尔.md" title="wikilink">伍珀塔尔</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_People&#39;s_Republic_of_China.svg" title="fig:Flag_of_the_People&#39;s_Republic_of_China.svg">Flag_of_the_People's_Republic_of_China.svg</a><a href="../Page/中国.md" title="wikilink">中国</a><a href="../Page/徐州.md" title="wikilink">徐州</a></li>
</ul></td>
</tr>
</tbody>
</table>

[S](../Category/卢瓦尔省市镇.md "wikilink")