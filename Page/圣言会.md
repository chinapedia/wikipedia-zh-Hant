**圣言会**（，[缩写](../Page/缩写.md "wikilink")**SVD**；；）是一个国际性的[天主教传教](../Page/天主教.md "wikilink")[修会](../Page/修会.md "wikilink")，1875年由[楊生·爱諾德神父创立](../Page/楊生·爱諾德.md "wikilink")。

## 在華傳教沿革

1879年4月20日，其第一批[传教士](../Page/传教士.md "wikilink")[安治泰](../Page/安治泰.md "wikilink")（[德国籍](../Page/德國人.md "wikilink")）和[福若瑟](../Page/福若瑟.md "wikilink")（[奥国籍](../Page/奧地利.md "wikilink")）抵达[香港](../Page/香港.md "wikilink")。学习了一段时间[中文以后](../Page/中文.md "wikilink")，于1882年前往[山东西南部](../Page/山东.md "wikilink")[阳谷县坡里庄](../Page/阳谷.md "wikilink")，1885年起负责新成立的山东南境代牧区，这个教区的辖境包括半个山东省：[曹州府](../Page/曹州.md "wikilink")、[兖州府](../Page/兖州.md "wikilink")、[沂州府和](../Page/沂州.md "wikilink")[济宁直隸州](../Page/济宁.md "wikilink")。长期以来，[方济各会的](../Page/方济各会.md "wikilink")[神父们无法在这一地区传教](../Page/神父.md "wikilink")。坡里庄的100多名教友几乎就是新教区的全部成员。然而富于进取心的圣言会克服了种种阻力，终于在这一地区打开了局面。在[曹州教案以后](../Page/曹州教案.md "wikilink")，得以将主教座堂建造在兖州府城，并扩展到[青岛附近的](../Page/青岛.md "wikilink")4个县。1900年，福若瑟神父被选任为省会长；他在济宁附近的戴家庄建立了圣言会的省会院。1949年，圣言会在山东创建的5个教区——[兖州教区](../Page/兖州教区.md "wikilink")、[青岛教区](../Page/青岛教区.md "wikilink")（1925）、[阳谷教区](../Page/阳谷教区.md "wikilink")（1933，中国籍主教）、[曹州教区](../Page/曹州教区.md "wikilink")（1934）和[沂州教区](../Page/沂州教区.md "wikilink")（1937年从青岛教区分出）已经拥有20万教友。此外，圣言会在中国其他省份还陆续建立了[信阳代牧区](../Page/信阳教区.md "wikilink")（1923）、[新鄉監牧區](../Page/新鄉監牧區.md "wikilink")（1936）、[西宁监牧区](../Page/西宁监牧区.md "wikilink")（1937），并从[圣母圣心会手中接管了](../Page/圣母圣心会.md "wikilink")[兰州代牧区](../Page/兰州代牧区.md "wikilink")（1923）、和[新疆监牧区](../Page/新疆监牧区.md "wikilink")（1930）。

1933年，圣言会从[本笃会手中接管了](../Page/本笃会.md "wikilink")[北京辅仁大学](../Page/輔仁大學北平時期.md "wikilink")，至中國共產黨政權接管為止。[輔仁大學於](../Page/輔仁大學.md "wikilink")1963年[在台復校](../Page/在台復校.md "wikilink")，聖言會為3大辦學單位之一，對於輔大的重新建校助力甚多。在此之前，[北京輔大附中已先於](../Page/北平輔仁大學附屬中學.md "wikilink")1961年由聖言會在[嘉義市](../Page/嘉義市.md "wikilink")[獨立復校](../Page/嘉義市私立輔仁高級中學.md "wikilink")。

[國共內戰後](../Page/國共內戰.md "wikilink")，聖言會在[中國大陸的會士大多移往](../Page/中國大陸.md "wikilink")[台灣](../Page/台灣.md "wikilink")，並以[台北](../Page/台北.md "wikilink")、[嘉義兩地為主要據點](../Page/嘉義.md "wikilink")。1954年，[陽穀主教](../Page/天主教陽穀教區.md "wikilink")[牛會卿被任命为](../Page/牛會卿.md "wikilink")[嘉義](../Page/天主教嘉義教區.md "wikilink")[监牧](../Page/监牧.md "wikilink")；[北平總主教](../Page/天主教北京總教區.md "wikilink")[田耕莘](../Page/田耕莘.md "wikilink")[樞機也在](../Page/樞機.md "wikilink")1960年至1966年間擔任[台北總教區署理主教](../Page/台北總教區.md "wikilink")。

1970年，[甘百德神父到](../Page/甘百德.md "wikilink")[香港創辦](../Page/香港.md "wikilink")[聖言中學](../Page/聖言中學.md "wikilink")。

至2006年止，圣言会的会士有6,102人，分布在67个[国家](../Page/国家.md "wikilink")。

## 參見

  - （同由楊生神父創立的女修會）

  - [聖言中學](../Page/聖言中學.md "wikilink")

  - [嘉義市私立天主教輔仁高級中學](../Page/嘉義市私立天主教輔仁高級中學.md "wikilink")

  - [南山大學](../Page/南山大學.md "wikilink")

## 外部链接

  - [聖言會全球網站](http://www.svdmissions.org/)
  - [聖言會中華省](http://www.svdchina.org/)
  - [圣言会美国总部](https://web.archive.org/web/20060415181040/http://www.svdusa.org/)

[圣言会](../Category/圣言会.md "wikilink")
[Category:天主教传教修会](../Category/天主教传教修会.md "wikilink")