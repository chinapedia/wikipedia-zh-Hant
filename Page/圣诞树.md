[Juletræet.jpg](https://zh.wikipedia.org/wiki/File:Juletræet.jpg "fig:Juletræet.jpg")[树](../Page/树.md "wikilink")\]\]
**圣诞树 （Christmas
tree）**，是[圣诞节期间](../Page/圣诞节.md "wikilink")，庆祝中最有名的传统擺设之一。通常人们在圣诞前后把一棵[常绿植物如](../Page/常绿植物.md "wikilink")[松树弄进屋里或者在户外](../Page/松树.md "wikilink")，並用圣诞灯和彩色的装饰物装饰，把一个天使或星星放在树的顶上。

现时圣诞树的擺设已和宗教毫无关系。部分為了[政治正確的則稱之](../Page/政治正確.md "wikilink")**耶誕樹**，**節日樹**。

## 历史

就像圣诞起源于[异教的习俗](../Page/异教.md "wikilink")，圣诞树也有悠远的历史，並逐漸傳播成为很多文化中[冬季的普遍现象](../Page/冬季.md "wikilink")。

### 日耳曼部落

[Dionysus.jpg](https://zh.wikipedia.org/wiki/File:Dionysus.jpg "fig:Dionysus.jpg")凯旋；神之后，[维多利亚](../Page/维多利亚_\(神话\).md "wikilink")（也就是希臘神話中的[妮琪](../Page/妮琪.md "wikilink")）拿着一个常绿植物。\]\]
圣诞树一般解释为古代异教的主张在基督教化保留下来，[常绿树象征庆祝生命复苏](../Page/常绿植物.md "wikilink")。\[1\]

### 罗马帝国时期

[古罗马](../Page/古罗马.md "wikilink")[鑲嵌畫顯示](../Page/鑲嵌畫.md "wikilink")，酒神[巴克斯](../Page/巴克科斯.md "wikilink")（也就是希臘神話中的[戴歐尼修斯](../Page/戴歐尼修斯.md "wikilink")）凱旋自[印度](../Page/印度.md "wikilink")，拿着一个针叶树的枝条。基督教化进程也可以在[古英语诗](../Page/古英语.md "wikilink")[十字架之梦](../Page/十字架之梦.md "wikilink")（Dream
of the
rood）中得到印证，在诗中树就是[耶稣受刑的刑具](../Page/耶稣.md "wikilink")。这首诗也引用[创世记中记载的](../Page/创世记.md "wikilink")[辨识善恶树](../Page/辨识善恶树.md "wikilink")。

### 現代

現代的習俗無法直接證明來自異教的習俗。最早的文獻記載可以上溯到16世紀的[德國](../Page/德國.md "wikilink")；[馬爾堡歐洲](../Page/馬爾堡_\(德國\).md "wikilink")[人類學教授Ingeborg](../Page/人類學.md "wikilink")
Weber-Keller鑒定一份1570年[不萊梅市](../Page/不萊梅.md "wikilink")[工業協會](../Page/工業協會.md "wikilink")[年冊報道怎樣用一棵冷衫加上蘋果](../Page/編年史.md "wikilink")，堅果，棗椰，餅乾和紙花等裝飾樹立在工業協會的房子取悅聖誕節搜集糖果的工業協會成員的孩子。另一份參照資料來自[巴塞爾](../Page/巴塞爾.md "wikilink")，1597年有一個裁縫學徒在市區附近運一棵裝飾了蘋果和奶酪的樹。

[拉脫維亞首都](../Page/拉脫維亞.md "wikilink")[里加聲稱他們是聖誕樹的故鄉](../Page/里加.md "wikilink")；在市廣場上有塊牌子用8種語言寫着“1510年第一棵聖誕樹在里加”。17世紀，這一習俗進入家庭。有位[斯特拉斯堡牧師](../Page/斯特拉斯堡.md "wikilink")，Johann
Konrad Dannerhauer，抱怨這一習俗與《[聖經](../Page/聖經.md "wikilink")》相悖，讓人迷惑。

到18世紀早期，這一習俗依然只是在上萊茵地區的城鎮比對普遍，農村地區還很少見。[蠟燭據考證始於](../Page/蠟燭.md "wikilink")18世紀晚期。聖誕樹在這個地區的“推廣”更是比對緩慢。

[Godey'streeDec1850.jpg](https://zh.wikipedia.org/wiki/File:Godey'streeDec1850.jpg "fig:Godey'streeDec1850.jpg")
在英國，聖誕樹是[喬治三世國王的德國王后](../Page/喬治三世.md "wikilink")[梅克伦堡-施特雷利茨的夏洛特引入的](../Page/梅克伦堡-施特雷利茨的夏洛特.md "wikilink")，但是並沒有傳遞到皇室以外的民間。[維多利亞女王兒時就熟悉這一習俗](../Page/維多利亞女王.md "wikilink")，在她1832年平安夜這一天的日記裡，
快樂的13歲公主寫道:
"晚飯後……我們都走進餐廳旁的客廳……那的兩張大圓桌上放着兩棵掛滿了燈和糖果裝飾的樹。所有的禮物圍繞着它們放着..."。她嫁給德國的表親[艾伯特親王後](../Page/艾伯特親王.md "wikilink")，這一習俗迅速傳遞。

慷慨的艾伯特親王在聖誕節還贈送大量的聖誕樹給學校和生活在貧民窟的人。

很多[城市](../Page/城市.md "wikilink")，鄉鎮和[百貨公司都會在戶外的空地上樹立聖誕樹](../Page/百貨公司.md "wikilink")，如[喬治亞](../Page/喬治亞.md "wikilink")[亞特蘭大市的](../Page/亞特蘭大.md "wikilink")[Rich's
Great Tree](../Page/Rich's_Great_Tree.md "wikilink")，
[紐約的](../Page/紐約.md "wikilink")[洛克菲勒中心聖誕樹](../Page/洛克菲勒中心.md "wikilink")
和[Adelaide](../Page/Adelaide.md "wikilink")[維多利亞廣場上的巨大聖誕樹](../Page/維多利亞廣場.md "wikilink")。

[taiwan_christmas_tree_bunun.jpg](https://zh.wikipedia.org/wiki/File:taiwan_christmas_tree_bunun.jpg "fig:taiwan_christmas_tree_bunun.jpg")在基督宗教文化影響下裝飾聖誕樹\]\]
在一些城市[聖誕樹節](../Page/聖誕樹節.md "wikilink") 和樹上的裝飾被用來組織或從事慈善事業。

在有些情況下，聖誕樹還是有特別紀念意義的禮物， 例如：

  - [倫敦的](../Page/倫敦.md "wikilink")[特拉法爾加廣場來自](../Page/特拉法爾加廣場.md "wikilink")[挪威城市](../Page/挪威.md "wikilink")[奧斯陸的聖誕樹代表者挪威人民對](../Page/奧斯陸.md "wikilink")[二戰期間英國人民伸出援手抵抗納粹](../Page/第二次世界大戰.md "wikilink")；
  - [馬薩諸塞州的](../Page/馬薩諸塞.md "wikilink")[波士頓有一棵聖誕樹是加拿大新斯科舍省送的禮物](../Page/波士頓.md "wikilink")，感謝他們1917在[軍火船爆炸發生後提供快速有效的援助](../Page/哈利法克斯大爆炸.md "wikilink")；
  - 在[上泰恩河紐卡斯爾每年都會收到來自](../Page/上泰恩河紐卡斯爾.md "wikilink")[挪威](../Page/挪威.md "wikilink")[卑爾根市的高](../Page/卑爾根.md "wikilink")15[米的聖誕樹](../Page/米.md "wikilink")，感謝他們作為盟軍一部分，輔助說明從納粹占領軍手中奪回卑爾根。

[美國的](../Page/美國.md "wikilink")“國家聖誕樹”每年都立在[華盛頓特區](../Page/華盛頓特區.md "wikilink")[白宮南面](../Page/白宮.md "wikilink")。今天，點亮樹上的燈已經成為聖誕假期白宮的一個重要事情。

“[查理·布朗聖誕樹](../Page/查理·布朗.md "wikilink")”這個概念可以用來說明一棵很難看的小樹。有些購買聖誕樹的顧客有意購買這樣的樹，表示對它們處境的同情。

在[新西蘭](../Page/新西蘭.md "wikilink")，
[Pohutukawa樹被說明為](../Page/Pohutukawa.md "wikilink")“天然聖誕樹”，因為它們在聖誕期間開花，它們的紅花和綠葉看上去就像一棵裝飾好的聖誕樹。

## 关于日期

传统上，直到[平安夜](../Page/平安夜.md "wikilink")（12月24日）圣诞树才会立起来装饰，然后在[12夜](../Page/12夜_\(假期\).md "wikilink")（1月6日）之后移走。提前和推后这个日期都被认为是不吉利的。但是，

  - 现代圣诞节购物旺季让部份[商店在](../Page/零售业#商店.md "wikilink")10月底就把圣诞树立起来。
  - 大多数美国家庭的习惯是[感恩节](../Page/感恩节_\(美国\).md "wikilink")（11月的第四个星期四）后立即就竖起圣诞树直到[新年之后](../Page/新年.md "wikilink")。有些美国家庭直到12月第二个周才着手竖起圣诞树，圣诞树立到1月6日（[主显节](../Page/主显节_\(基督教\).md "wikilink")）。
  - 在德国，传统上圣诞树12月24日立起直到1月7日，不过也有人提前1-2周把树立起来。
  - 在澳大利亚，圣诞树一般在12月1日被立起来，正好是学校暑假前一周；不过南澳洲例外，那里大多数人在Adelaide Credit
    Union圣诞庆典后也就是11月上旬就把树立起来了。

## 被用作圣诞树的树

[Christmas_tree2.jpg](https://zh.wikipedia.org/wiki/File:Christmas_tree2.jpg "fig:Christmas_tree2.jpg")
天然或人造的树都被用作圣诞树。

### 天然树

首选[树种是](../Page/种_\(生物\).md "wikilink")[冷杉属](../Page/冷杉.md "wikilink")
（*Abies*），这个种的针形叶干燥以后叶不易脱落，颜色和气味也不错，不过其它[种类也可以用](../Page/属_\(生物\).md "wikilink")。

一般北[欧洲常用的有](../Page/欧洲.md "wikilink")：

  - [銀冷杉](../Page/銀冷杉.md "wikilink") *Abies alba* （原始种）
  - [高加索冷杉](../Page/高加索冷杉.md "wikilink") *Abies nordmanniana*
  - [壮丽冷杉](../Page/壮丽冷杉.md "wikilink") *Abies procera*
  - [歐洲云杉](../Page/歐洲云杉.md "wikilink")（挪威雲杉） *Picea abies* （通常最便宜）
  - [塞尔维亚云杉](../Page/塞尔维亚云杉.md "wikilink") *Picea omorika*
  - [欧洲赤松](../Page/欧洲赤松.md "wikilink") *Pinus sylvestris*

[北美洲](../Page/北美洲.md "wikilink")、[中美洲及](../Page/中美洲.md "wikilink")[南美洲常用的有](../Page/南美洲.md "wikilink"):

  - [加拿大冷杉](../Page/加拿大冷杉.md "wikilink") *Abies balsamea*
  - [弗雷澤冷杉](../Page/弗雷澤冷杉.md "wikilink") *Abies fraseri*
  - [大冷杉](../Page/大冷杉.md "wikilink") *Abies grandis*
  - 壮丽冷杉 *Abies procera*
  - [紫果冷杉](../Page/紫果冷杉.md "wikilink") *Abies magnifica*
  - [花旗松](../Page/花旗松.md "wikilink") *Pseudotsuga menziesii*
  - 欧洲赤松 *Pinus sylvestris*
  - [義大利石松](../Page/義大利石松.md "wikilink") *Pinus pinea* （作為小形桌面樹種）

有些树常常从[苗圃卖出时带着根和土壤](../Page/苗圃.md "wikilink")，
可以新年后移植到室外栽培多年使用。然而在挖掘时根部失去很多，室内环境又一般是[温度较高](../Page/温度.md "wikilink")[湿度较低](../Page/湿度.md "wikilink")，非常有害于树木健康，
成活率十分低。

此外，[歐洲冬青](../Page/歐洲冬青.md "wikilink")（Ilex aquifolium）也有被稱為聖誕樹。

### 人造树

[TSTNewWorldCentre.jpg](https://zh.wikipedia.org/wiki/File:TSTNewWorldCentre.jpg "fig:TSTNewWorldCentre.jpg")

人造圣诞树是指相对天然栽培的圣诞树而言，采用人工方式制造的圣诞树。最早的人造圣诞树分木质和羽毛质两种，均由德国人发明。现代的人造圣诞树多由聚氯乙烯（PVC）制成，但目前以及历史上还有很多种其它类型的人造圣诞树存在，包括铝制圣诞树、光纤圣诞树等。

## 環保問題

有英國環保顧問公司研究發現一棵真的聖誕樹，由種植到用完後焚毀，會產生3.5[公斤的](../Page/公斤.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")，人工製的假聖誕樹，每一棵則會釋放40公斤二氧化碳，是真聖誕樹的11倍多。原因是假聖誕樹是用塑膠製造，由加工過程、原料運輸，到假塑膠樹棄置後處理都要消耗大量的能源，導致更多碳排放。但假聖誕樹若每年重用，只要一棵假聖誕樹用12年以上，就會比用真樹環保。\[2\]

現時有園藝公司提供真聖誕樹出租的服務，真聖誕樹於節日過後交回園藝公司打理，以減輕丟棄真聖誕樹對環境的影響。

## 论战

关于圣诞树存在相当的争议，主要集中在长期或者非长期使用这种树和反对使用者声称，圣诞树来源于异教传统而没有《圣经》根据等。

### 名称争论

**假日树**这个概念于1990年代晚期（可能更早）产生于美国和加拿大试图将它推广到整个冬季假期而不知是在特殊宗教假期。[福克斯新闻撰稿人](../Page/福克斯新闻频道.md "wikilink")[Bill
O'Reilly和](../Page/Bill_O'Reilly_\(评论员\).md "wikilink")[Sean
Hannity起头發起的一个运动导致一些基督教团体和个人的激烈反应](../Page/Sean_Hannity.md "wikilink")，他们认为假日树就等于向圣诞节宣战，是某种意义的亵渎。

### 宗教争论

[Arbol_Navidad_03.gif](https://zh.wikipedia.org/wiki/File:Arbol_Navidad_03.gif "fig:Arbol_Navidad_03.gif")
有些基督徒，尽管是少数，认为“圣诞树”是被禁止的，其中《圣经·[耶利米书](../Page/耶利米书.md "wikilink")》10:1-5載：

  -
    「以色列家啊，你们要听耶和华对你们所说的话。耶和华说：“你们千万不要仿效列国的行径。列国因天上的征兆而惊慌，你们却不要因这些事而惊慌。万民的习俗只像一口气，他们的偶像不过是人从森林里砍下的树木，是工匠拿钩刀用手艺造成的。人用金银装饰偶像，又用钉子锤子钉牢，免得偶像东倒西歪。这些偶像不过像瓜田里的稻草人，不能说话，也不能走路，必须靠人搬运。**你们不要怕它们**，因为它们既不能降祸，也不能赐福。”」

在另外一本英王钦定本圣经的章节更清楚的提到制作偶像进行崇拜的习俗:

这一部分详细的介绍了一个人怎样把一块木头变成一个崇拜的偶像。他们随身把它带着来避祸或者祈福。除了装饰作用以外，与圣诞树相同的地方是他们都是用木头做的而且都用来崇拜。

少数基督徒认为圣诞树没有《圣经》依据，因此避免使用。出于同样的原因，他们也根本不会去庆祝圣诞节。然而很多教会把圣诞树看作圣诞节期间的装饰。有些教会会用同一棵圣诞树剥去皮后做[复活节的](../Page/复活节.md "wikilink")[基督十字](../Page/十字架.md "wikilink")。参看上面[The
Dream of the
Rood的描述](../Page/The_Dream_of_the_Rood.md "wikilink")。他们认为：[以西结书](../Page/以西结书.md "wikilink")47:12和[启示录](../Page/启示录.md "wikilink")22:2用树象征新的硕果累累的生命，与[创世记](../Page/创世记.md "wikilink")3:22-23提到的禁止亚当享用的生命树做类比。保罗清楚说明了亚当和基督的关系，在[罗马书](../Page/罗马书.md "wikilink")5：14说:「[亚当跟后来要到的那位相似](../Page/亚当.md "wikilink")。」故有將圣诞树視作生命树的象征，或者带来赎价的[十字架](../Page/十字架.md "wikilink")。

## 圣诞树产业

每年，仅美国的需求量就差不多3300-3600万棵，欧洲的需求量大约为5000－6000万。1998年，美国的圣诞树栽培商数目大约有15000，大约三分之一是"现选现砍"的农场主；同一年，估计仅圣诞树一项，美国人就花费了15亿美元。\[3\]

## 参看

[The_bigest_in_Europe_Project_365_Day_302.jpg](https://zh.wikipedia.org/wiki/File:The_bigest_in_Europe_Project_365_Day_302.jpg "fig:The_bigest_in_Europe_Project_365_Day_302.jpg")

  - [人造圣诞树](../Page/人造圣诞树.md "wikilink")
  - [美国圣诞节传统](../Page/美国圣诞节传统.md "wikilink")
  - [德国圣诞节传统](../Page/德国圣诞节传统.md "wikilink")
  - [三位一体](../Page/三位一体.md "wikilink")
  - [伯利恒之星](../Page/伯利恒之星.md "wikilink")
  - [大卫之星](../Page/大卫之星.md "wikilink")
  - [树 (神话)](../Page/树_\(神话\).md "wikilink")
  - [知识树](../Page/知识树.md "wikilink")
  - [生命树](../Page/生命树.md "wikilink")

## 参考文献

## 外部链接

  - [Riga， Latvia purported home of the original Christmas
    Tree](http://www.firstchristmastree.com)

[Category:圣诞节传统](../Category/圣诞节传统.md "wikilink")
[Category:树](../Category/树.md "wikilink")
[Category:节日相关主题](../Category/节日相关主题.md "wikilink")
[Category:圣诞节](../Category/圣诞节.md "wikilink")

1.  《警醒！》11/12刊
    [聖誕樹真的跟耶穌有關嗎？](http://wol.jw.org/zh-Hant/wol/d/r24/lp-ch/102011451?q=%E8%81%96%E8%AA%95%E6%A8%B9&p=par)
2.  [環保顧問指真聖誕樹比假樹環保](http://cablenews.i-cable.com/webapps/program/study/videoPlay.php?video_id=12172528)Cable
    News 新聞通識 2013/12/25
3.