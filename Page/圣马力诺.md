**圣马力诺共和国**\[1\]<small>（）</small>，\[2\]\[3\] 通稱**圣马力诺**
（；），也被称为“**最庄严尊贵的圣马力诺共和国**”<small>（）</small>，\[4\]
位于[意大利半岛的](../Page/意大利半岛.md "wikilink")[亚平宁山脉东北侧](../Page/亚平宁山脉.md "wikilink")，处于被[意大利包围的](../Page/意大利.md "wikilink")[国中国状态](../Page/国中国.md "wikilink")。圣马力诺国土面积，人口33,285人。\[5\]
其国家[首都是](../Page/首都.md "wikilink")[圣马力诺市](../Page/圣马力诺市.md "wikilink")，最大的城市是位于[塞拉瓦莱的](../Page/塞拉瓦莱.md "wikilink")。圣马力诺是[欧洲委员会所有国家中人口最少的国家](../Page/欧洲委员会.md "wikilink")。

圣马力诺的国名来源于圣玛利诺（圣马利诺斯），一个来自[拉布岛](../Page/拉布岛.md "wikilink")（位于现在的[克罗地亚](../Page/克罗地亚.md "wikilink")）[罗马帝国](../Page/罗马帝国.md "wikilink")[殖民地的石匠](../Page/殖民地_\(罗马\).md "wikilink")。公元257年，圣玛利诺参与了[里米尼的城墙重建工作](../Page/里米尼.md "wikilink")，该城市的城墙曾因为遭受[海盗的袭击而被破坏](../Page/海盗.md "wikilink")。公元301年，圣玛利诺在[蒂塔诺山修建了一个独立的](../Page/蒂塔诺山.md "wikilink")[修道院社区](../Page/修道院.md "wikilink")。因此圣玛利诺号称是现存最古老的[主权国家和历史最悠久的](../Page/主权国家.md "wikilink")[立宪制](../Page/共和立憲制.md "wikilink")[共和国](../Page/共和国.md "wikilink")。\[6\]

圣马力诺由《》管理，该[宪法成书于](../Page/宪法.md "wikilink")16世纪，由[拉丁文书写完成的一套六本的文件](../Page/拉丁文.md "wikilink")，其中规定了该国的政治制度。圣马力诺认为这是最古老的迄今仍然有效的政府文件或宪法。\[7\]

圣马力诺的国家经济主要依靠[金融业](../Page/金融业.md "wikilink")、[工业](../Page/工业.md "wikilink")、[服务业和](../Page/服务业.md "wikilink")[旅游业](../Page/旅游业.md "wikilink")。就以[购买力平价计算的人均国内生产总值来说](../Page/各国人均国内生产总值列表_\(购买力平价\).md "wikilink")，圣马力诺是全球最富裕的国家之一，与欧洲发达地区水准持平。圣马力诺被认为是国家经济高度稳定和欧洲[失业率最低的国家之一](../Page/失业率.md "wikilink")，该国既没有国家债务也没有预算盈余。\[8\]
圣马力诺也是全球唯一一个[车比人多的国家](../Page/各国人均汽车拥有量列表.md "wikilink")。在外交上，圣马力诺追随[意大利领导](../Page/意大利.md "wikilink")，而且它还是[团结谋共识集团的核心成员](../Page/团结谋共识.md "wikilink")。\[9\]

## 历史

[Marino_als_steinhauer.png](https://zh.wikipedia.org/wiki/File:Marino_als_steinhauer.png "fig:Marino_als_steinhauer.png")

圣玛利诺（圣马利诺斯）与他终身好友里奥离开现在[克罗地亚的](../Page/克罗地亚.md "wikilink")[拉布岛](../Page/拉布岛.md "wikilink")，以石匠的身份前往[里米尼](../Page/里米尼.md "wikilink")。当他在[基督教堂](../Page/基督教堂.md "wikilink")[布道之后](../Page/布道.md "wikilink")，圣玛利诺遇到了[戴克里先迫害](../Page/戴克里先迫害.md "wikilink")，他被迫逃往附近的[蒂塔诺山](../Page/蒂塔诺山.md "wikilink")。在那里，圣玛利诺修建了一个小教堂，这就是现在圣马力诺共和国和圣马力诺市的起源。有时，这个国家依然会被称为“蒂塔诺共和国”。\[10\]
现在圣马力诺共和国将官方的建国日定在301年9月3日。

[San_Marino_constitution_1600.jpg](https://zh.wikipedia.org/wiki/File:San_Marino_constitution_1600.jpg "fig:San_Marino_constitution_1600.jpg")

1631年，[罗马教廷承认圣马力诺共和国独立](../Page/罗马教廷.md "wikilink")。

1797年，[拿破仑军队的进军对于圣马力诺共和国的独立造成了一个短暂的威胁](../Page/拿破仑.md "wikilink")。但是由于时任[圣马力诺共和国执政官的](../Page/圣马力诺执政官列表.md "wikilink")的尊敬和友谊，圣马力诺摆脱了威胁。由于安东里奥的接入，拿破仑甚至写信给时任法国政府科学与艺术管理处的负责人兼科学家[加斯帕尔·蒙日](../Page/加斯帕尔·蒙日.md "wikilink")，承诺他不仅会保证圣马力诺共和国的独立性，还可以给予圣马力诺它所想要的任何领土。这个提议被安东里奥给拒绝了，因为他担心日后遭致相关国家的报复。\[11\]\[12\]

19世纪，在[意大利统一运动的后期](../Page/意大利統一.md "wikilink")，圣马力诺成为许多因支持统一而遭受迫害人士的避难所。为嘉奖这一支持，[朱塞佩·加里波底接受了圣马力诺共和国的愿求](../Page/朱塞佩·加里波底.md "wikilink")，没有将其纳入到统一的意大利之下。

圣马力诺共和国曾授予美国总统[亚伯拉罕·林肯为荣誉公民](../Page/亚伯拉罕·林肯.md "wikilink")。林肯为此回信道，圣马力诺共和国证明了“建立在共和原则基础上的政府可以如此管理以使国家长治久安”。\[13\]\[14\]

在[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，当[意大利于](../Page/意大利.md "wikilink")1915年5月23日向[奥匈帝国](../Page/奥匈帝国.md "wikilink")[宣战时](../Page/宣战.md "wikilink")，圣马力诺宣布保持[中立](../Page/中立.md "wikilink")。意大利对圣马力诺人的中立持敌意态度，他们认为圣马力诺的中立可能会让奥匈帝国间谍在此建立一个新的无线电站联系渠道。为此，意大利政府试图在圣马力诺强行建立一支[宪兵部队](../Page/宪兵.md "wikilink")，可以不接受圣马力诺的领导而在必要时切断整个圣马力诺的无线通讯。在此期间，有两组十名志愿者加入意大利军队并出现在意大利战场。第一组人员是作为战斗人员，而第二组人员则是加入到[红十字野战医院](../Page/红十字.md "wikilink")。由于该医院的存在，奥匈帝国宣布和圣马力诺断绝外交关系。\[15\]

1923年到1943年期间，圣马力诺共和国在的统治下。

[Guerra1.JPG](https://zh.wikipedia.org/wiki/File:Guerra1.JPG "fig:Guerra1.JPG")

在[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，圣马力诺共和国依旧宣布保持中立。但是在1940年9月17日一篇来自《[纽约时报](../Page/纽约时报.md "wikilink")》的错误新闻上，圣马力诺被报道向英国宣战了。\[16\]
随后，圣马力诺政府紧急联系英国政府，表示他们并没有向英国宣战。\[17\]

当[贝尼托·墨索里尼在意大利垮台三天后](../Page/贝尼托·墨索里尼.md "wikilink")，圣马力诺法西斯党的统治也宣告崩溃，新的圣马力诺政府宣布在冲突中保持中立。1944年4月1日，[法西斯主义者重新在圣马力诺执政](../Page/法西斯主义.md "wikilink")，但是依旧宣布保持中立。尽管如此，圣马力诺依旧在1944年6月26日遭受到[英国皇家空军的轰炸](../Page/英国皇家空军.md "wikilink")。理由是英国军方相信圣马力诺已经被德军占领，并且德军正在这里大肆搜刮物资和武器。圣马力诺政府在当天宣布，其领土上并没有任何军事设施，同时也没有任何交战国部队被允许进入。\[18\]
当盟军越过[哥德防线的时候](../Page/哥德防线.md "wikilink")，圣马力诺接纳了成千上万的平民[难民](../Page/难民.md "wikilink")。1944年9月，圣马力诺曾被在中被击溃的德军短暂占领。

圣马力诺拥有世界上第一个经过[民主选举而产生的](../Page/民主选举.md "wikilink")[共产主义政府](../Page/共产主义.md "wikilink")，它是由[圣马力诺共产党和](../Page/圣马力诺共产党.md "wikilink")组成的执政党联盟进行组阁。该联盟于1945年到1957年期间执政。\[19\]\[20\]

圣马力诺是世界上国土面积最小的[共和国](../Page/共和国.md "wikilink")，虽然1968年独立的[瑙鲁挑战了这一说法](../Page/瑙鲁.md "wikilink")，這是因為尽管瑙鲁的陆地面积只有21平方公里，但是其管辖是水域面积却多达43.1万平方公里，因此瑙鲁的实际国土面积是圣马力诺的数倍。\[21\]

1988年，圣马力诺成为[欧洲委员会成员](../Page/欧洲委员会.md "wikilink")；1992年，又成为[联合国会员国](../Page/联合国.md "wikilink")。虽然圣马力诺既不是[欧盟国家](../Page/欧盟.md "wikilink")，也不是[欧元区成员](../Page/欧元区.md "wikilink")，但它依旧以[欧元作为自己的](../Page/欧元.md "wikilink")[法定货币](../Page/法定货币.md "wikilink")。

{{-}}

## 地理

[Fortress_of_Guaita_2013-09-19.jpg](https://zh.wikipedia.org/wiki/File:Fortress_of_Guaita_2013-09-19.jpg "fig:Fortress_of_Guaita_2013-09-19.jpg")的[瓜伊塔森林](../Page/瓜伊塔.md "wikilink")。\]\]
[San_Marino_CIA_map.gif](https://zh.wikipedia.org/wiki/File:San_Marino_CIA_map.gif "fig:San_Marino_CIA_map.gif")

圣马力诺共和国是[意大利的](../Page/意大利.md "wikilink")[国中国](../Page/国中国.md "wikilink")，位于意大利[艾米利亚-罗马涅大区与](../Page/艾米利亚-罗马涅大区.md "wikilink")[马尔凯大区的交界处](../Page/马尔凯大区.md "wikilink")，距离[里米尼的](../Page/里米尼.md "wikilink")[亚得里亚海](../Page/亚得里亚海.md "wikilink")[海岸线约](../Page/海岸线.md "wikilink")10公里。

全国均为[丘陵地形](../Page/丘陵.md "wikilink")，并无平坦国土，属于[亚平宁山脉的一部分](../Page/亚平宁山脉.md "wikilink")。

圣马力诺共和国的国土最高点位于[蒂塔诺山山顶](../Page/蒂塔诺山.md "wikilink")，[海拔高度是](../Page/海拔高度.md "wikilink")749米。

圣马力诺共和国国境内无任何水体。

圣马力诺是欧洲第三小的国家，仅比[梵蒂冈和](../Page/梵蒂冈.md "wikilink")[摩纳哥面积略大](../Page/摩纳哥.md "wikilink")；同时它也是全球第五小的国家。\[22\]

### 气候

圣马力诺共和国是受大陆影响的[地中海气候](../Page/地中海气候.md "wikilink")，夏季温暖而冬季凉爽，是典型的意大利中部内陆地区气候。

<div style="width:80%; font-size:small; align:left">

</div>

## 政治

[Parliament_San_Marino.jpg](https://zh.wikipedia.org/wiki/File:Parliament_San_Marino.jpg "fig:Parliament_San_Marino.jpg")，圣马力诺共和国中央政府所在地。\]\]
[Captains_Regent_Tomassoni,_Rossi,_Mancini_and_Selva.jpg](https://zh.wikipedia.org/wiki/File:Captains_Regent_Tomassoni,_Rossi,_Mancini_and_Selva.jpg "fig:Captains_Regent_Tomassoni,_Rossi,_Mancini_and_Selva.jpg")：[米尔科·托马索尼](../Page/米尔科·托马索尼.md "wikilink")、、[亚历山德罗·曼奇尼和](../Page/亚历山德罗·曼奇尼.md "wikilink")[阿尔贝托·塞尔瓦](../Page/阿尔贝托·塞尔瓦.md "wikilink")<small>（从左至右）</small>。\]\]

圣马力诺共和国拥有一个[议会](../Page/议会制.md "wikilink")[代议民主](../Page/代议民主制.md "wikilink")[共和制政体框架](../Page/共和制.md "wikilink")：[执政官既是](../Page/圣马力诺执政官列表.md "wikilink")[国家元首](../Page/国家元首.md "wikilink")，又是[政府首脑](../Page/政府首脑.md "wikilink")；拥有[多元化的](../Page/多元化.md "wikilink")[多党制](../Page/多党制.md "wikilink")。[行政权由政府行使](../Page/行政权.md "wikilink")；[立法权则由政府和](../Page/立法权.md "wikilink")[大议会共同行使](../Page/大议会.md "wikilink")；[司法权独立于行政和立法机关之外](../Page/司法权.md "wikilink")。

圣马力诺最早是由各大家族领导人所组成的进行统治。13世纪，统治权被移交给大议会。1243年，大议会提名产生了首任两名执政官。直至2017年，这种提名方式依旧被采用。

圣马力诺共和国的立法机关是[大议会](../Page/大议会.md "wikilink")，这是一个由60名议员组成的[一院制](../Page/一院制.md "wikilink")[议会](../Page/议会.md "wikilink")。这些议员由圣马力诺九大行政区每隔5年进行的一次[代表比例选举而产生](../Page/比例代表制.md "wikilink")。这些行政区的划分与圣马力诺早期的旧[教区划分一致](../Page/教区.md "wikilink")。

凡年满18周岁的圣马力诺[公民均享有](../Page/公民.md "wikilink")[投票权](../Page/投票权.md "wikilink")。除了一般的立法权之外，大议会还拥有批准预算和选举执政官、国务会议（由10名拥有行政权的委员组成）、12人理事会（在大议会任期内行使司法权力的机关）、咨询委员会及政府工会（）的权力。大议会也有权批准与他国所缔结的条约。大议会下辖五个咨询委员会，每个委员会由15名议员组成，分别负责审查、提议和讨论执行正在议会议事日程上的新法律。

每隔六个月，大议会就会选举新的两名执政官出任国家元首。这两名执政官出身相互对立的党派，因此权力是平等和互相制衡的。他们的任期为六个月。每年4月1日和10月1日是大议会选举产生执政官的日子，一旦过了这个期限而没有选举新的执政官，公民有权在三日内决定是否针对执政官提起诉讼。一旦公民的诉求被通过，前任执政官将被展开司法调查。

圣马力诺共和国所采用双摄政官的[二头政治制度类似于](../Page/二头政治.md "wikilink")[古罗马时代的](../Page/古罗马.md "wikilink")[罗马执政官](../Page/罗马执政官.md "wikilink")；而经常选举的方式也是来自[罗马共和国时代的风俗](../Page/罗马共和国.md "wikilink")。大议会相当于[罗马元老院](../Page/罗马元老院.md "wikilink")；而执政官则相当于[罗马执政官](../Page/罗马执政官.md "wikilink")。这种制度的由来被认为是当地居民在罗马统治崩溃之后，自发形成的一个基本政府来保护他们免受外来统治。

圣马力诺共和国是一个[多党制](../Page/多党制.md "wikilink")[民主共和国](../Page/共和民主制.md "wikilink")。由于2008年的选举法提高了国内小党派进入大议会的门槛，因此此后的大议会由两大[政党联盟把持](../Page/政党联盟.md "wikilink")：右翼的“”联盟，由[圣马力诺天主教民主党领导](../Page/圣马力诺天主教民主党.md "wikilink")；左翼的“”联盟，由[社会主义者和民主人士党领导](../Page/社会主义者和民主人士党.md "wikilink")。社会主义者和民主人士党是由与合并而成。在2008年圣马力诺大选中，“为了圣马力诺协议”获得35个席位，而“改革和自由”获得25个席位。

2007年10月1日，[米尔科·托马索尼当选为执政官](../Page/米尔科·托马索尼.md "wikilink")。他成为该国历史上第一位出任执政官的[残障人士](../Page/残疾人.md "wikilink")。\[23\]

圣马力诺比其他国家拥有更多的女性国家元首。截止2014年10月，共有10名女性出任国家元首；其中三人曾两次任职。

### 行政区划

#### 市镇

[San_Marino.png](https://zh.wikipedia.org/wiki/File:San_Marino.png "fig:San_Marino.png")。\]\]

圣马力诺共和国共划分为九个基层政权地区，这些地区在当地被称之为“堡”

  - [圣马力诺是国家首都](../Page/聖馬力諾_\(城市\).md "wikilink")。

除此之外，还有其他八个堡：

  - [阿夸维瓦](../Page/阿夸维瓦.md "wikilink")
  - [博尔戈·马吉欧雷](../Page/博尔戈·马吉欧雷.md "wikilink")
  - [基埃萨努欧瓦](../Page/基埃萨努欧瓦.md "wikilink")
  - [菲奥伦蒂诺](../Page/菲奥伦蒂诺.md "wikilink")
  - [蒙泰吉阿迪诺](../Page/蒙泰吉阿迪诺.md "wikilink")
  - [塞拉瓦莱](../Page/塞拉瓦莱.md "wikilink")
  - [多玛尼亚诺](../Page/多玛尼亚诺.md "wikilink")
  - [法尔齐亚诺](../Page/法尔齐亚诺.md "wikilink")

圣马力诺的最大定居点是，但它并不是一个独立的堡，而是属于[塞拉瓦莱](../Page/塞拉瓦莱.md "wikilink")。

与意大利风俗相似，每个堡都有一个堡治所在，被称为“首要镇”（）。而堡内一些更小的地方则被称之为“镇”（）

#### 教区

圣马力诺共和国国内共有43个[教区](../Page/教区.md "wikilink")。\[24\]

  -
  - 卡基弗诺<small>（）</small>

  - 卡基安力诺<small>（）</small>

  - 卡美农<small>（）</small>

  - 卡格尼<small>（）</small>

  - 卡利戈<small>（）</small>

  -
  - 卡拉迪诺<small>（）</small>

  - 卡利加里斯<small>（）</small>

  -
  - 卡潘尼<small>（）</small>

<!-- end list -->

  -
  - 卡斯泰莱罗<small>（）</small>

  -
  - 辛客维<small>（）</small>

  -
  -
  -
  -
  -
  -
  - 加纳卧托<small>（）</small>

<!-- end list -->

  -
  - 拉塞纳<small>（）</small>

  - 赛伊诺<small>（）</small>

  - 莫拉里尼<small>（）</small>

  - 蒙塔尔博<small>（）</small>

  -
  -
  - 皮耶纳科奇<small>（）</small>

  - 皮安代弗诺<small>（）</small>

  -
  -
<!-- end list -->

  - 庞特梅利尼<small>（）</small>

  -
  -
  -
  - 斯派乔詹诺尼<small>（）</small>

  - 泰利廖<small>（）</small>

  -
  -
  -
  -
### 军事力量

圣马力诺共和国的[军事力量是世界上最小的](../Page/军事力量.md "wikilink")，其[国防是由](../Page/国防.md "wikilink")[意大利军队负责](../Page/意大利军队.md "wikilink")。不同的分支机构有不同的职能，这其中包括：执行仪仗职责、巡逻边界、护卫政府大楼和协助警方处理重大刑事案件。圣马力诺警方不包括在其军事力量内。

#### 弩兵军团

弩兵军团曾经是圣马力诺共和国的核心军事力量，但现在只是一支由80名志愿者组成的[仪仗队](../Page/仪仗队.md "wikilink")。自1925年以来，弩兵军团会在节日庆典上表演[弩箭射击](../Page/弩.md "wikilink")。该军团的制服源自[中世纪的设计](../Page/中世纪.md "wikilink")。虽然弩兵军团迄今仍是一个法定的军事单位，但是该军团并没有任何军事职能。

#### 岩石卫队

[Guardia_di_Rocca_al_Palazzo_Pubblico_San_Marino.jpg](https://zh.wikipedia.org/wiki/File:Guardia_di_Rocca_al_Palazzo_Pubblico_San_Marino.jpg "fig:Guardia_di_Rocca_al_Palazzo_Pubblico_San_Marino.jpg")

岩石卫队是圣马力诺共和国军事力量中的一支前线部队，他们的一个职责就是边界巡逻，负责巡视和保卫国境线；而他们的另一个职责则是卫队，负责保护圣马力诺市内圣马力诺政府所在地[共和国宫](../Page/共和国宫_\(圣马力诺\).md "wikilink")。这个角色是他们最为被民众所常见，而他们的换岗仪式也是当地的风景。

除了这些军事职责之外，根据1987年的法令，所有岩石卫队队员也是圣马力诺共和国的[刑警](../Page/刑警.md "wikilink")，以协助圣马力诺警方调查重大犯罪。岩石卫队的制服是独特的红绿色设计。

#### 大议会卫队

大议会卫队成立于1740年，是志愿性质的礼仪单位。大议会卫队也被当地人称为“议会卫队”或“贵族卫队”。由于他们制服上那醒目的蓝色、白色和金色，使得他们称为圣马力诺军事力量中最为人所熟知的部分，他们也多次出现在圣马力诺共和国的[明信片上](../Page/明信片.md "wikilink")。议会卫队的主要职责是保护执政官，并在正式会议期间内保护大议会成员。同时他们还在国家庆典或宗教节日里充当政府官员的礼仪保镖。

#### 军装民兵连

在过去，所有在圣马力诺境内拥有两名或两名以上男性的家庭必须有一半的成员在军装民兵连服役。这个部队是圣马力诺军事力量中的基本战斗力，但主要职责还是礼仪性的。许多圣马力诺人将在军装民兵连服役视为一件骄傲自豪的事情，现在在圣马力诺境内居住六年以上的公民都可以申请参加。

军装民兵连的制服是深蓝色，同时配有一顶戴着蓝色和白色羽毛的。而军装民兵连的礼服则还包括白色的定位带（）、蓝白相间的腰带、白色的肩章和有白色配饰的袖口。

#### 军乐队

军乐队在正式归属上属于圣马力诺陆军，是圣马力诺共和国的仪仗乐队。该乐队由50名音乐家组成，其制服类似陆军军服。在大多数的国家正式场合里，军乐队负责演奏音乐。

#### 宪兵

圣马力诺宪兵队成立于1842年，是一个军事执法机构。其成员是全职的，负责保护公民人身和财产安全，并维护治安。

圣马力诺军事力量由职业军人和他们的志愿者同事共同组成，这些志愿者也被称为“志愿兵部队”。

## 经济

[Via_Basilicus_din_San_Marino2.jpg](https://zh.wikipedia.org/wiki/File:Via_Basilicus_din_San_Marino2.jpg "fig:Via_Basilicus_din_San_Marino2.jpg")和[银行业是圣马力诺的主要收入来源](../Page/银行业.md "wikilink")。\]\]

虽然圣马力诺共和国并不是[欧盟国家](../Page/欧盟.md "wikilink")，但是仍然可以通过[欧盟理事会的许可而使用](../Page/欧盟理事会.md "wikilink")[欧元作为国家货币](../Page/欧元.md "wikilink")。同时圣马力诺也被授权在欧元[硬币的国家一侧拥有自己进行设计的权力](../Page/硬币.md "wikilink")。在使用欧元作为[法定货币之前](../Page/法定货币.md "wikilink")，圣马力诺里拉与[意大利里拉挂钩并可兑换](../Page/意大利里拉.md "wikilink")。圣马力诺欧元硬币的数量非常少，和以前的圣马力诺里拉一样，主要是钱币收藏家们的兴趣所在。

圣马力诺的[人均国内生产总值为](../Page/各国人均国内生产总值列表_\(购买力平价\).md "wikilink")55,449[美元](../Page/美元.md "wikilink")，其生活水平大致与[丹麦差不多](../Page/丹麦.md "wikilink")。其国内的关键产业是[银行业](../Page/银行业.md "wikilink")、电子产业和[陶瓷产业](../Page/陶瓷器.md "wikilink")；主要[农产品是](../Page/农产品.md "wikilink")[葡萄酒和](../Page/葡萄酒.md "wikilink")[奶酪](../Page/乾酪.md "wikilink")。圣马力诺主要是从[意大利进口货物](../Page/意大利.md "wikilink")。

圣马力诺[邮票一般是仅限在其境内使用](../Page/邮票.md "wikilink")，因此大部分是出售给[集邮爱好者](../Page/集邮.md "wikilink")，这也是圣马力诺的一大[财政收入](../Page/财政收入.md "wikilink")。圣马力诺共和国是的成员。

### 税收政策

在圣马力诺共和国，[公司利得税税率为](../Page/公司稅.md "wikilink")13%；税率为5%；[利息需缴纳](../Page/利息.md "wikilink")13%[预扣所得税](../Page/预扣所得税.md "wikilink")。

根据圣马力诺共和国与意大利在1939年签署的友好协议，圣马力诺在1972年从意大利引进了[增值税制度](../Page/增值税.md "wikilink")。此外，圣马力诺还将针对进口货物征收[关税](../Page/关税.md "wikilink")。但是这种税收制度不适用于圣马力诺境内货物。直到1996年，圣马力诺不再对境内生产或销售的货品征收[间接税](../Page/间接税.md "wikilink")。

根据，圣马力诺将继续对进口货物征收类似[进口关税的税](../Page/关税.md "wikilink")。同时，圣马力诺还将开征一般所得税来取代意大利所得税。

### 旅游产业

旅游业贡献了圣马力诺[国民生产总值](../Page/国民生产总值.md "wikilink")2.2%以上。\[25\]
在2014年，大约有200万人次的游客来访。\[26\]

### 与意大利的协定

圣马力诺与意大利在1862年缔结了一份协约，这份协约一直执行到今天。\[27\]
这份圣马力诺与意大利缔结的协约规定了一些在圣马力诺境内的经济活动。圣马力诺境内禁止种植烟草或生产一些受意大利政府垄断的商品。圣马力诺禁止直接进口货物，所有运抵圣马力诺的第三方国家或地区的商品和货物都必须经由意大利转运。虽然圣马力诺可以印刷自己的邮票，但却禁止印刷自己的货币，同时有义务使用意大利铸造的钱币。[赌博在圣马力诺境内是合法且有一定规范的](../Page/赌博.md "wikilink")，但是在2007年之前，圣马力诺一直禁止开设[赌场](../Page/赌场.md "wikilink")。现在在圣马力诺境内有一家合法赌场。

为了换取这些限制，意大利必须每年向圣马力诺提供[补贴并按成本价出售如下物资](../Page/补贴.md "wikilink")：[海盐](../Page/海盐.md "wikilink")（每年不超过250吨）、[烟草](../Page/烟草.md "wikilink")（40吨）、[香烟](../Page/香烟.md "wikilink")（20吨）和[火柴](../Page/火柴.md "wikilink")（不限量）。\[28\]

在圣马力诺与意大利的边界线上，并没有海关办事处。游客们可以前往圣马力诺的游客服务处凭借自己的[护照购买正式取消的纪念的邮票当作签证纪念](../Page/护照.md "wikilink")。

## 人口

### 人口统计

圣马力诺共和国共计拥有人口33,000人，其中4,800人为外国定居者，而其中绝大部分又都是[意大利人](../Page/意大利人.md "wikilink")。此外，另有12,000名圣马力诺人居住在海外。其中，5,700人居住在[意大利](../Page/意大利.md "wikilink")；3,000人居住在[美国](../Page/美国.md "wikilink")；1,900人居住在[法国](../Page/法国.md "wikilink")；1,600人居住在[阿根廷](../Page/阿根廷.md "wikilink")。

自1976年之后的第一次[人口普查在](../Page/人口普查.md "wikilink")2010年举行，预计2011年出结果，但是有13%的家庭尚未归还他们的调查表格。

圣马力诺主要使用的语言是[意大利语](../Page/意大利语.md "wikilink")；也在广泛使用。

圣马力诺的人均寿命是世界上最长的。\[29\]

### 知名人士

  - （1506年－）——圣马力诺[建筑师](../Page/建筑师.md "wikilink")

<!-- end list -->

  - （1630年－——圣马力诺[剧作家](../Page/剧作家.md "wikilink")

<!-- end list -->

  - （）——圣马力诺[政治家](../Page/政治家.md "wikilink")

### 宗教宗派

[San_Marino_katedra.jpg](https://zh.wikipedia.org/wiki/File:San_Marino_katedra.jpg "fig:San_Marino_katedra.jpg")

圣马力诺共和国是一个[天主教占主导地位的国家](../Page/天主教.md "wikilink")，\[30\]
该国公民有97%的比例信仰罗马天主教，但是天主教并非圣马力诺的[国教](../Page/国教.md "wikilink")。大约有一半的[天主教徒信仰罗马天主教](../Page/天主教徒.md "wikilink")。\[31\]
圣马力诺没有[主教座](../Page/主教座.md "wikilink")，尽管它的国名是[教区名的一部分](../Page/教区.md "wikilink")。历史上，圣马力诺各[堂区分别隶属意大利的两个教区](../Page/堂区.md "wikilink")。其中大部分属于，其余部分属于。1977年，由于蒙特菲尔特罗和里米尼两个教区的辖区进行了调整，因此圣马力诺目前全部归属于蒙特菲尔特罗教区。[圣马力诺与蒙特菲尔特罗教区的](../Page/天主教圣马力诺-蒙特费尔特罗教区.md "wikilink")[座堂位于意大利](../Page/座堂.md "wikilink")[佩萨罗和乌尔比诺省的](../Page/佩萨罗和乌尔比诺省.md "wikilink")[彭纳比利](../Page/彭纳比利.md "wikilink")。

根据圣马力诺[个人所得税法的规定](../Page/个人所得税.md "wikilink")，纳税人有权要求将自己所得税的0.3%捐赠给[天主教会或其他慈善机构](../Page/天主教会.md "wikilink")，这其中就包括[瓦勒度派和](../Page/瓦勒度派.md "wikilink")[耶和华见证人](../Page/耶和华见证人.md "wikilink")。

[天主教圣马力诺-蒙特费尔特罗教区从](../Page/天主教圣马力诺-蒙特费尔特罗教区.md "wikilink")1977年改名至今，原名是“蒙特菲尔特罗教区”，隶属于[拉韦纳-切尔维亚总教区](../Page/天主教拉韦纳-切尔维亚总教区.md "wikilink")。\[32\]
目前的[教区已经包含圣马力诺全境所有的](../Page/教区.md "wikilink")[堂区](../Page/堂区.md "wikilink")。最早提及蒙特菲尔特罗地名的是在[查理曼时期的](../Page/查理曼.md "wikilink")文献中。

## 交通

圣马力诺境内没有机场，入境需要从意大利进入

## 文化

## 参考文献

## 相关条目

  -
<!-- end list -->

  - [城邦](../Page/城邦.md "wikilink")

<!-- end list -->

  - [梵蒂冈](../Page/梵蒂冈.md "wikilink")

<!-- end list -->

  - [列支敦士登](../Page/列支敦士登.md "wikilink")

## 外部链接

  - [圣马力诺共和国](http://www.bbc.co.uk/news/world-europe-17842338)在[BBC新闻上的资料](../Page/BBC新闻.md "wikilink")

  - [圣马力诺共和国官方旅游观光网站](http://www.visitsangmarino.com/)

  -
  -
[\*](../Category/圣马力诺.md "wikilink")
[Category:城邦](../Category/城邦.md "wikilink")
[Category:共和国](../Category/共和国.md "wikilink")
[Category:欧洲国家](../Category/欧洲国家.md "wikilink")
[Category:微型國家](../Category/微型國家.md "wikilink")
[Category:內陸國家](../Category/內陸國家.md "wikilink")
[Category:联合国会员国](../Category/联合国会员国.md "wikilink")
[Category:歐洲委員會成員國](../Category/歐洲委員會成員國.md "wikilink")
[Category:義大利語國家地區](../Category/義大利語國家地區.md "wikilink")
[Category:301年建立的国家或政权](../Category/301年建立的国家或政权.md "wikilink")
[Category:300年代建立的國家或政權](../Category/300年代建立的國家或政權.md "wikilink")

1.

2.
3.

4.
5.
6.

7.

8.

9.

10. Charles, comte de Bruc, [*The Republic of San
    Marino*](https://archive.org/details/republicsanmari00brucgoog)
    (Cambridge: 1880).

11.

12. *Histoire abrégée des traités de paix entre les puissances de
    l'Europe depuis la Paix de Westphalie*, Christophe-Guillaume Koch,
    ed., Paris, 1817, vol. V, p. 19.

13.

14. Irving Wallace, *The Book of Lists 3*

15.

16.

17. [*Diplomatic papers, 1944*](../Page/#refDiplomatic.md "wikilink"),
    p. 292

18. [*Diplomatic papers, 1944*](../Page/#refDiplomatic.md "wikilink"),
    p. 291

19.

20.

21.

22.

23.

24.  . elezioni.sm

25.

26.

27.

28.

29.
30.
31.

32.