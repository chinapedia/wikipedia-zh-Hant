[Piazza_San_Marco_with_the_Basilica,_by_Canaletto,_1730._Fogg_Art_Museum,_Cambridge.jpg](https://zh.wikipedia.org/wiki/File:Piazza_San_Marco_with_the_Basilica,_by_Canaletto,_1730._Fogg_Art_Museum,_Cambridge.jpg "fig:Piazza_San_Marco_with_the_Basilica,_by_Canaletto,_1730._Fogg_Art_Museum,_Cambridge.jpg")之畫。\]\]

**聖馬可廣場**（）是[意大利](../Page/意大利.md "wikilink")[威尼斯的中心](../Page/威尼斯.md "wikilink")[廣場](../Page/廣場.md "wikilink")。在威尼斯，聖馬可廣場是唯一被稱為「」的廣場，其他的廣場無論大小皆被稱為「」。聖馬可廣場在[歐洲](../Page/歐洲.md "wikilink")[城市的廣場中是獨一無二的](../Page/歐洲城市.md "wikilink")，它座落在[市中心](../Page/市中心.md "wikilink")，卻不像其他廣場那樣受到[交通的喧鬧](../Page/交通.md "wikilink")，這歸功於威尼斯寧靜的[水路交通](../Page/威尼斯水道.md "wikilink")。作為威尼斯的[地標](../Page/地標.md "wikilink")，聖馬可廣場受到[遊客](../Page/遊客.md "wikilink")、[攝影師和](../Page/攝影師.md "wikilink")[鴿子的格外青睞](../Page/鴿子.md "wikilink")。19世紀[法國皇帝](../Page/法國皇帝.md "wikilink")[拿破崙曾稱讚其為](../Page/拿破崙.md "wikilink")「[歐洲最美的](../Page/歐洲.md "wikilink")[客廳](../Page/客廳.md "wikilink")」。

## 建筑

[Quadri-Moretti,_Piazza_San_Marco_(1831),_01.jpg](https://zh.wikipedia.org/wiki/File:Quadri-Moretti,_Piazza_San_Marco_\(1831\),_01.jpg "fig:Quadri-Moretti,_Piazza_San_Marco_(1831),_01.jpg")

圣马可广场是由[总督宫](../Page/总督宫_\(威尼斯\).md "wikilink")、[圣马可教堂](../Page/圣马可教堂.md "wikilink")、[圣马可钟楼](../Page/圣马可钟楼.md "wikilink")、新、旧行政官邸大楼、连接两大楼的拿破仑翼大楼、圣马可教堂的钟楼和[圣马可图书馆等建筑和](../Page/圣马可图书馆.md "wikilink")[威尼斯大运河所围成的长方形广场](../Page/威尼斯大运河.md "wikilink")，长约170[米](../Page/米.md "wikilink")，东边宽约80[米](../Page/米.md "wikilink")，西侧宽约55[米](../Page/米.md "wikilink")。广场四周的建筑都是[文艺复兴时期的精美建筑](../Page/文艺复兴.md "wikilink")。

圣马可广场周围开设有许多[咖啡店](../Page/咖啡店.md "wikilink")，桌椅露天摆放在广场上的新、旧行政官邸大楼前，是游人用餐、喝咖啡和听音乐好场所。

圣马可广场的南侧有一座附属的小广场，小广场南临威尼斯大运河敞口的[潟湖](../Page/潟湖.md "wikilink")，河边有两根威尼斯著名的白色石柱，一根柱子上雕刻的是威尼斯的[守护者](../Page/守护者.md "wikilink")[圣狄奥多](../Page/圣狄奥多.md "wikilink")，另一根柱子上雕刻有威尼斯另一位守护者[圣马可的飞狮](../Page/圣马尔谷.md "wikilink")，这两根石柱是威尼斯官方城门，威尼斯的贵宾都从石柱中间进入城市。这里也曾经是威尼斯执行[死刑的地方](../Page/死刑.md "wikilink")。

## 历史

圣马可广场初建于9世纪，当时只是圣马可教堂前的一座小广场。马可是[圣经中](../Page/圣经.md "wikilink")《[马可福音](../Page/马可福音.md "wikilink")》的作者，威尼斯人将他奉为主保圣人。相传828年两个威尼斯商人从[埃及](../Page/埃及.md "wikilink")[亚历山大将](../Page/亚历山大.md "wikilink")[耶稣](../Page/耶稣.md "wikilink")[圣徒](../Page/圣徒.md "wikilink")[马可的遗骨偷运到威尼斯](../Page/圣马尔谷.md "wikilink")，并在同一年为圣马可兴建教堂，教堂内有圣马可的陵墓，大教堂以圣马可的名字命名，大教堂前的广场也因此得名“圣马可广场”。

1177年为了[教宗](../Page/教宗.md "wikilink")[亚历山大三世和](../Page/亚历山大三世_\(教宗\).md "wikilink")[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[腓特烈一世的会面才将圣马可广场扩建成如今的规模](../Page/腓特烈一世_\(神圣罗马帝国\).md "wikilink")。

1797年[拿破仑进占威尼斯后](../Page/拿破仑.md "wikilink")，赞叹圣马可广场是“欧洲最美的客厅”和“世界上最美的广场”，并下令把广场边的行政官邸大楼改成了他自己的[行宫](../Page/行宫.md "wikilink")，还建造了连接两栋大楼的翼楼作为他的舞厅，命名为“拿破仑翼大楼”。

圣马可广场在历史上一直是威尼斯的[政治](../Page/政治.md "wikilink")、[宗教和](../Page/宗教.md "wikilink")[节庆中心](../Page/节庆.md "wikilink")，是威尼斯所有重要[政府机构的所在地](../Page/威尼斯政府.md "wikilink")，自从19世纪以来是[大主教的驻地](../Page/大主教.md "wikilink")，它同时也是许多威尼斯节庆选择的举办地。

## 水淹

[Venezia_acqua_alta_notte_2005_modificata.jpg](https://zh.wikipedia.org/wiki/File:Venezia_acqua_alta_notte_2005_modificata.jpg "fig:Venezia_acqua_alta_notte_2005_modificata.jpg")

聖馬可廣場是[威尼斯的地勢最低點](../Page/威尼斯地理.md "wikilink")，因此在[漲潮時和下](../Page/漲潮.md "wikilink")[大雨時](../Page/雨.md "wikilink")，它是威尼斯首先被水淹的地方。下大雨時，[雨水從](../Page/雨水.md "wikilink")[廣場的排水溝直接流入](../Page/廣場.md "wikilink")[威尼斯大運河](../Page/威尼斯大運河.md "wikilink")。這是下雨時的理想排水渠道，但是在漲潮時卻帶來了麻煩。每天[亞得里亞海潮漲時分](../Page/亞得里亞海.md "wikilink")，威尼斯大運河的河水同樣通過排水溝從聖馬可廣場的地下湧出，形成一潭潭的積水，每年還會出現幾次潮水鋪滿廣場甚至水淹廣場的情景。

<File:View> of St Marks Place Venice Sixteenth Century after Cesare
Vecellio.png|16世紀的聖馬可廣場 <File:XIX> century print, Piazza San Marco,
Venezia.jpg|19世紀的聖馬可廣場 <File:Venice> piazza san marco.jpg|2003年時的聖馬可廣場
<File:Piazzetta> San Marco.jpg|2006年時的聖馬可廣場 <File:Piazza> san
marco.jpg|廣場與[鴿子](../Page/鴿子.md "wikilink") <File:San>
marco-akelly.jpg|霧氣中的廣場
<File:Venice02.jpg>|[鐘樓](../Page/鐘樓.md "wikilink")[高塔](../Page/高塔.md "wikilink")（視覺一）
<File:Piazza> San Marco in Venice, with St Mark's Campanile and Basilica
in the background.jpg|鐘樓高塔（視覺二）

## 參考文獻

<div class="references-small">

  - Janson, Alban & Thorsten Bürklin. (2002). *Auftritte Scenes:
    Interaction with Architectural Space: the Campi of Venice.* Basel:
    Birkhauser. ISBN 3-7643-6585-4
  - Lien, Barbara. (May 2005). *The Role of Pavement in the Perceived
    Integration of Plazas: An Analysis of the Paving Designs of Four
    Italian Piazzas.* unpublished M.S. thesis. Washington State
    University Department of Horticulture and Landscape Architecture.
    [PDF](http://www.dissertations.wsu.edu/Thesis/Spring2005/b_lien_032505.pdf)
  - Norwich, John Julius, Tudy Sammartini, and Gabriele Crozzoli (1999).
    *Decorative Floors of Venice.* London: Merrell Publishers. ISBN
    1-85894-108-3
  - Puppi, Lionello. (2002). *The Stones of Venice*. New York: Vendome
    Press. ISBN 0-86565-245-7
  - Williams, Kim. (1997). *Italian Pavements: Patterns in Space.*
    Houston: Anchorage Press. ISBN 0-9655268-2-8

</div>

## 外部連結

  - [](http://www.italyguides.it/us/venice_italy/st_mark_s_square/piazza_san_marco/st_mark_s_square.htm)
  - [Satellite image from Google
    Maps](http://maps.google.com/maps?q=venice,+italy&ll=45.433914,12.337604&spn=0.003004,0.010274&t=k&hl=en)
  - [聖馬可廣場相片冊](https://web.archive.org/web/20070928004415/http://sabin.ro/gallery/venice_piazza_san_marco)

## 參見

  - [聖馬可](../Page/聖馬可.md "wikilink")

[Category:威尼斯广场](../Category/威尼斯广场.md "wikilink")
[Category:歐洲地標](../Category/歐洲地標.md "wikilink")