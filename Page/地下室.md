[HK_Night_Wan_Chai_C_C_Wu_Building_basement_Parkn_Shop_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Night_Wan_Chai_C_C_Wu_Building_basement_Parkn_Shop_a.jpg "fig:HK_Night_Wan_Chai_C_C_Wu_Building_basement_Parkn_Shop_a.jpg")\]\]

**地下室**，又名**地庫**，是[建築物](../Page/建築物.md "wikilink")、[大廈等的低於地面的一層或多層](../Page/大廈.md "wikilink")。樓層郵寄地址英文書寫為B1、B2等，越深層數愈多。

## 簡介

地下室在條件上是空氣比較不流通、[陽光不能照射](../Page/陽光.md "wikilink")，是次等的生活[空間](../Page/空間.md "wikilink")。所以地下室多數用作[貨倉](../Page/貨倉.md "wikilink")、[酒庫](../Page/酒庫.md "wikilink")、[機房](../Page/機房.md "wikilink")、[停車場之類](../Page/停車場.md "wikilink")。在寸金尺土的[都市](../Page/都市.md "wikilink")，地下室也可能利用作[出租房](../Page/出租房.md "wikilink")、[百貨公司](../Page/百貨公司.md "wikilink")、[酒吧](../Page/酒吧.md "wikilink")、[商場等](../Page/商場.md "wikilink")。而有些國家會規定戰時地下室需作為防空及避難之用。在[龍捲風經常發生的美國中部平原地區](../Page/龍捲風.md "wikilink")，住宅等建物會設有地窖供龍卷風來襲時避難用。

在[地下城](../Page/地下城.md "wikilink")、[地下街](../Page/地下街.md "wikilink")、[地下鐵發達的地區](../Page/地下鐵.md "wikilink")，地下室有通道互聯，其用途會更加多樣化。

但在經常有[水災的地方](../Page/水災.md "wikilink")，地下室有被水淹沒的[危機](../Page/危機.md "wikilink")，因此在進入地下室的入口須裝有[防水閘門](../Page/防水閘門.md "wikilink")。

## 命名

[香港與](../Page/香港.md "wikilink")[中國對地下室有不同的命名方法](../Page/中國.md "wikilink")，[香港的地下室如果是只有一層的](../Page/香港.md "wikilink")，該樓層則名為「地庫」（B，Basement），如果是多層的，該樓層則名「第一層地庫」（B1，Basement
Level 1）與「第二層地庫」（B2，Basement Level 2）或「地庫一樓」（B1，Basement First
Floor）與「地庫二樓」（B2，Basement Second Floor），如果只有兩層地庫也可以分別稱 UB
(Upper Basement) 與 LB (Lower Basement) 。 而中國的地下室則名為「負一」（-1）或「負二」（-2）。

臺灣地下室命名，採「地下一樓」、「地下二樓」的命名法，簡稱「B1」（讀作B one）、「B2」（B two）

## 參看

  - [防空洞](../Page/防空洞.md "wikilink")
  - [地下街](../Page/地下街.md "wikilink")
  - [地窖](../Page/地窖.md "wikilink")
  - [地道](../Page/地道.md "wikilink")
  - [地牢 (BDSM)](../Page/地牢_\(BDSM\).md "wikilink")

[Category:地下空間](../Category/地下空間.md "wikilink")
[Category:建筑工程](../Category/建筑工程.md "wikilink")
[Category:结构工程](../Category/结构工程.md "wikilink")