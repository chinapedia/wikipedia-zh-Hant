**地利店**（）是[香港一家](../Page/香港.md "wikilink")[連鎖式](../Page/連鎖式.md "wikilink")[便利商店](../Page/便利商店.md "wikilink")，透過[特許經營方式經營](../Page/特許經營.md "wikilink")。地利店曾是香港第三大便利商店，全盛時期擁有超過100家分店，網絡分佈於多個[香港地鐵沿線車站及](../Page/香港地鐵.md "wikilink")[九廣鐵路沿線車站](../Page/九廣鐵路.md "wikilink")、[商場](../Page/商場.md "wikilink")、[公共屋邨及屋苑等](../Page/公共屋邨.md "wikilink")。當時其競爭對手為[7-Eleven及](../Page/7-Eleven.md "wikilink")[OK便利店](../Page/OK便利店.md "wikilink")。

## 歷史

地利店原為「**香港電視（無綫電視）服務站**」，於1982年開業，曾銷售[無綫電視相關產品](../Page/電視廣播有限公司.md "wikilink")，是無綫電視的附屬業務。

1988年，當時[香港政府在](../Page/港英政府.md "wikilink")《電視條例》規定，禁止所有香港[電視台及旗下公司經營其他非廣播相關業務](../Page/電視台.md "wikilink")。因此，無線電視就分拆成[電視廣播有限公司和](../Page/電視廣播有限公司.md "wikilink")“香港電視集團”（後改稱“[電視企業](../Page/電視企業.md "wikilink")”並獨立上市，其後再被私有化），香港電視服務站與《[香港電視](../Page/香港電視_\(雜誌\).md "wikilink")》雜誌、[華星唱片及](../Page/華星唱片.md "wikilink")[見聞會社一同撥入香港電視集團](../Page/見聞會社.md "wikilink")，從此無綫電視的電視廣播業務和與電視廣播事業無關的業務劃清界線。其後集團控股權又轉讓予當時無線電視的股東[嘉里集團](../Page/嘉里集團.md "wikilink")。

1990年代，香港電視服務站改稱「地利店」，並將業務併入[南華早報集團](../Page/南華早報集團.md "wikilink")。地利店併入南華早報集團後，業務不斷擴大，曾開設超過100家便利店。

## 收購

2004年9月，[南華早報集團宣布以](../Page/南華早報集團.md "wikilink")1.05億港元出售旗下87間地利店予[牛奶公司旗下的](../Page/牛奶公司.md "wikilink")[7-Eleven](../Page/7-Eleven.md "wikilink")，所有地利店分店易名為7-Eleven。交易在2004年10月完成，南華早報集團在交易將可取得7,600萬港元收益。\[1\]\[2\]\[3\]

其後，7-11便利店的分店數目由510間大幅提升至597間。所有於地利店售賣的貨品售價已改用7-11便利店的售價，亦會提供[自由斟及](../Page/自由斟.md "wikilink")[思樂冰等](../Page/思樂冰.md "wikilink")7-11便利店產品。地利店終結22年的歷史，香港主要便利店業務從此維持7-11便利店和其唯一競爭對手[OK便利店互相爭霸的局面](../Page/OK便利店.md "wikilink")。

## 產品

地利店銷售[食品](../Page/食物.md "wikilink")、[饮料](../Page/饮料.md "wikilink")、[軟件](../Page/軟件.md "wikilink")、[日用品](../Page/日用品.md "wikilink")、[药物](../Page/药物.md "wikilink")、[香煙](../Page/香煙.md "wikilink")、[酒類](../Page/酒.md "wikilink")、[玩具](../Page/玩具.md "wikilink")、部份[精品](../Page/精品.md "wikilink")、[報紙及](../Page/報紙.md "wikilink")[雜誌等貨品](../Page/雜誌.md "wikilink")，還開設[電話卡及](../Page/電話卡.md "wikilink")[八達通等相關服務](../Page/八達通.md "wikilink")。

## 經營特色

  - 自1982年起，地利店成為了地鐵站內唯一的便利店，直至1990年代末期[地鐵公司](../Page/地鐵公司.md "wikilink")（今港鐵公司）開放站內便利店競爭為止。
  - 自2000年代開始，地利店新增非現金付款機制，接受[八達通付款](../Page/八達通.md "wikilink")。

## 註釋及參考來源

## 參見

  - [電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")
  - [南華早報集團](../Page/南華早報集團.md "wikilink")
  - [7-Eleven](../Page/7-Eleven.md "wikilink")
  - [OK便利店](../Page/OK便利店.md "wikilink")

## 外部連結

  - [7-Eleven® Hong Kong](http://www.7-eleven.com.hk)
  - [香港電視服務站開業廣告](http://www.youtube.com/watch?v=NAAGJgNhfKw)

[Category:1982年成立的公司](../Category/1982年成立的公司.md "wikilink")
[Category:香港已結業公司](../Category/香港已結業公司.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:便利商店](../Category/便利商店.md "wikilink")
[Category:無綫電視](../Category/無綫電視.md "wikilink")
[Category:嘉里集團](../Category/嘉里集團.md "wikilink")

1.  [SCMP售地利店](http://hk.biz.yahoo.com/040915/241/14nlg.html)
    ，[明報](../Page/明報.md "wikilink")-Yahoo\!新聞，2004年9月16日
2.  [南早零售以1.05億出售旗下地利店予牛奶](http://hk.biz.yahoo.com/040915/243/14npq.html)
    ，[東方日報](../Page/東方日報.md "wikilink")-Yahoo\!新聞，2004年9月16日
3.  [港"7.11"便利店將增至600間](http://www.takungpao.com.hk/news/2004-9-16/GW-306525.htm)
    ，大公網，2004年9月16日