[Temple_of_Earth_-_satellite_image_(1967-09-20).jpg](https://zh.wikipedia.org/wiki/File:Temple_of_Earth_-_satellite_image_\(1967-09-20\).jpg "fig:Temple_of_Earth_-_satellite_image_(1967-09-20).jpg")
**地壇（**　转写：na i
mukdehun**）**在[中国](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")[安定门外](../Page/安定门_\(北京\).md "wikilink")，是[明世宗以後](../Page/明世宗.md "wikilink")[明](../Page/明.md "wikilink")[清兩代](../Page/清.md "wikilink")[皇帝每年](../Page/皇帝.md "wikilink")[夏至祭祀土地神的地方](../Page/夏至.md "wikilink")，20世纪后逐渐开辟为公园。

## 歷史

地坛建於明[嘉靖九年](../Page/嘉靖.md "wikilink")（1530年），占地37.4[公頃](../Page/公頃.md "wikilink")。又名方澤壇。坐落于[安定门外东侧](../Page/安定门_\(北京\).md "wikilink")，与[天坛遥相对应](../Page/天坛.md "wikilink")，与[雍和宫](../Page/雍和宫.md "wikilink")、孔庙、国子监隔河相望。祭祀儀式每年[農曆夏至日舉行](../Page/農曆.md "wikilink")，由皇帝親自拜祭。1923年8月日本东京大地震，被黜清帝[溥仪为筹款救济日本灾民](../Page/溥仪.md "wikilink")，而首次开放地坛。1925年辟为“京兆公园”，1928年改称“市民公园”后荒废，1957年恢复公园称“地坛公园”，1981年以来，国家投资对公园的古建进行了修复，1984年5月地坛公园正式售票开放，并定为北京市文物保护单位，2006年5月25日晋升为国家级文物保护单位。\[1\]

## 主要建築

[缩略图](https://zh.wikipedia.org/wiki/File:South_Gate,_Temple_of_Earth.jpg "fig:缩略图")
**方澤壇**是地壇的主體建築，北向，是[磚石結構的兩層方台](../Page/磚石結構.md "wikilink")。有水池環繞，因池名方澤而稱之。方澤壇下層有四個石座，是祭祀時安放岳、鎮、海、瀆神位之處，壇外設有望燈臺和燎爐。
钟楼，始建于1530年，为三开间歇山式绿琉璃顶的重檐正方形建筑，通面阔多。于1965年拆除。2000年按原样重建。钟高2.58米，直径1.56米，重2324千克，铭文铸“大明嘉靖年月日制”八个字。\[2\]

## 建築特色

1.  天壇設於[紫禁城東南方](../Page/紫禁城.md "wikilink")，地壇則築於紫禁城東北部，以符合中國古代天南地北之說。
2.  天壇主體建築為圓形，地壇據《[大清會典](../Page/大清會典.md "wikilink")》稱：“方澤……形方象地”，呈方形，是中國古時[天圓地方的體現](../Page/天圓地方.md "wikilink")。
3.  據[道家](../Page/道家.md "wikilink")[陰陽學說](../Page/陰陽.md "wikilink")：“天為陽，地為陰”，天壇所用建材，如石塊、臺階、柱子等，均為[奇數](../Page/奇數.md "wikilink")（陽數）。地壇則採用[偶數](../Page/偶數.md "wikilink")（陰數）。如方澤壇的臺階為八級，壇面為六平方丈，所用石板均為偶數。

## 活动

[Red_lanterns,_Spring_Festival,_Ditan_Park_Beijing.JPG](https://zh.wikipedia.org/wiki/File:Red_lanterns,_Spring_Festival,_Ditan_Park_Beijing.JPG "fig:Red_lanterns,_Spring_Festival,_Ditan_Park_Beijing.JPG")
北京一年一度的[地坛庙会](../Page/地坛庙会.md "wikilink")（地坛春节文化庙会）在此舉行。

地坛一年还举办春季、夏季、秋季、冬季四次书市。
[缩略图](https://zh.wikipedia.org/wiki/File:Drum_Tower,_Ditan_of_Beijing_Sep_2018.jpg "fig:缩略图")

## 相关作品

[史铁生著有](../Page/史铁生.md "wikilink")《我与地坛》。

## 参考文献

## 外部链接

  - [地理坐标](../Page/地理坐标.md "wikilink")：

## 参见

  - [北京地坛书市](../Page/北京地坛书市.md "wikilink")
  - [九壇八廟](../Page/九壇八廟.md "wikilink")
      - [天壇](../Page/天壇.md "wikilink")

{{-}}

[Category:北京祭祀建筑物](../Category/北京祭祀建筑物.md "wikilink")
[Category:京城九壇](../Category/京城九壇.md "wikilink")
[Category:北京牌坊](../Category/北京牌坊.md "wikilink")

1.
2.