<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>本條目記敘的是**地拉那州**。其他同名的行政區尚有[地拉那區及](../Page/地拉那區.md "wikilink")[地拉那市](../Page/地拉那市.md "wikilink")。</small>

</div>

**地拉那州**（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：Tiranë）位於[阿爾巴尼亞中西部](../Page/阿爾巴尼亞.md "wikilink")，由[地拉那區](../Page/地拉那區.md "wikilink")、[-{zh-hans:卡耶亚区;
zh-hant:卡瓦耶區;}-組成](../Page/卡瓦耶區.md "wikilink")，與[-{zh-hans:迪勃拉;
zh-hant:第巴爾;}-](../Page/第巴爾州.md "wikilink")、[-{zh-hans:都拉斯;
zh-hant:杜勒斯;}-](../Page/杜勒斯州.md "wikilink")、[-{zh-hans:爱尔巴桑;
zh-hant:艾巴申;}-](../Page/艾巴申州.md "wikilink")、[-{zh-hans:费里;
zh-hant:非夏爾;}-相鄰](../Page/非夏爾州.md "wikilink")。阿爾巴尼亞首都[地拉那坐落於該州範圍內](../Page/地拉那.md "wikilink")。