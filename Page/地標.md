**地標**是指某地方具有獨特地理特色的[建築物或者](../Page/建築物.md "wikilink")[自然](../Page/自然.md "wikilink")[景觀](../Page/景觀.md "wikilink")[地形](../Page/地形.md "wikilink")，遊客或其他一般人可以看圖而認出自己所在之處，例如[摩天大樓](../Page/摩天大樓.md "wikilink")、[購物中心](../Page/購物中心.md "wikilink")、[會議中心](../Page/會議中心.md "wikilink")、[飯店](../Page/飯店.md "wikilink")、娛樂場所、[體育館](../Page/體育館.md "wikilink")、[劇院](../Page/劇院.md "wikilink")、[學校](../Page/學校.md "wikilink")、[博物館](../Page/博物館.md "wikilink")、[紀念碑](../Page/紀念碑.md "wikilink")、[廣場](../Page/廣場.md "wikilink")、[鐘樓](../Page/鐘樓.md "wikilink")、[市政廳](../Page/市政廳.md "wikilink")、[教堂](../Page/教堂.md "wikilink")、[寺廟](../Page/寺廟.md "wikilink")、[清真寺](../Page/清真寺.md "wikilink")、[雕像](../Page/雕像.md "wikilink")、[車站](../Page/車站.md "wikilink")、[機場](../Page/機場.md "wikilink")、[發電廠](../Page/發電廠.md "wikilink")、[天線](../Page/天線.md "wikilink")、[煙囪](../Page/煙囪.md "wikilink")、[水壩](../Page/水壩.md "wikilink")、[水塔](../Page/水塔.md "wikilink")、[燈塔](../Page/燈塔.md "wikilink")、[橋樑等公共建設](../Page/橋樑.md "wikilink")。

## 世界知名地標

### 亞洲

**[中国大陆](../Page/中国大陆.md "wikilink")**

<File:Tiananmen.JPG>|[北京](../Page/北京.md "wikilink")[天安門](../Page/天安門.md "wikilink")
<File:Hall> of Supreme
Harmony.JPG|[北京](../Page/北京.md "wikilink")[紫禁城](../Page/紫禁城.md "wikilink")[太和殿](../Page/太和殿.md "wikilink")
<File:TempleofHeaven-HallofPrayer.jpg>|[北京](../Page/北京.md "wikilink")[天坛](../Page/天坛.md "wikilink")
<File:GreatWall> 2004 Summer 4.jpg|[萬里長城](../Page/萬里長城.md "wikilink")
<File:The> Bund Day
View.JPG|[上海](../Page/上海.md "wikilink")[外滩](../Page/外滩.md "wikilink")
<File:Shanghai> oriental pearl
tower.JPG|[上海](../Page/上海.md "wikilink")[東方明珠塔](../Page/東方明珠塔.md "wikilink")
<File:Shanghai> SWFC\&Jing
Mao.JPG|[上海](../Page/上海.md "wikilink")[环球金融中心和](../Page/上海环球金融中心.md "wikilink")[金茂大厦](../Page/金茂大厦.md "wikilink")
<File:Expo> 2010 China Pavilion
Daytime.jpg|[上海](../Page/上海.md "wikilink")[世博会](../Page/上海世博会.md "wikilink")[中国馆](../Page/中国2010年上海世界博览会中国馆.md "wikilink")
<File:Guangzhou>
Tower.jpg|[廣州](../Page/廣州.md "wikilink")[新電視塔](../Page/廣州新電視塔.md "wikilink")
<File:Zhujiang> New
Town.jpg|[廣州](../Page/廣州.md "wikilink")[西塔](../Page/廣州西塔.md "wikilink")
<File:Shun> Hing
Square.jpg|[深圳](../Page/深圳.md "wikilink")[信興廣場](../Page/地王大廈.md "wikilink")
<File:Hall> of Sun Yat-sen
Mausoleum.jpg|[南京](../Page/南京.md "wikilink")[中山陵](../Page/中山陵.md "wikilink")
<File:Xi'an> montage.png|[西安市地標建築](../Page/西安市.md "wikilink") <File:The>
Fugong Temple Wooden Pagoda.jpg|[應縣木塔](../Page/應縣木塔.md "wikilink")
<File:Leifeng> Pagoda and West Lake, Hangzhou 120529
2.jpg|[杭州](../Page/杭州.md "wikilink")[西湖](../Page/西湖.md "wikilink")
<File:Chongqingdlt.jpg>|[重慶人民大禮堂](../Page/重慶人民大禮堂.md "wikilink")
<File:Chongqing> World Financial
Centre.jpg|[重庆环球金融中心](../Page/重庆环球金融中心.md "wikilink")
<File:Potala> Palace, August
2009.jpg|[拉萨](../Page/拉萨.md "wikilink")[布达拉宫](../Page/布达拉宫.md "wikilink")

**[港澳地区](../Page/港澳地区.md "wikilink")**

Image:Langham Place.jpg|[朗豪坊](../Page/朗豪坊.md "wikilink")
<File:International> Commerce Centre
201408.jpg|[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")
<File:Macau> Tower CE Centre.jpg|[澳門旅遊塔](../Page/澳門旅遊塔.md "wikilink")
<File:Grand> Lisboa Macau
2011.jpg|[澳門新葡京酒店](../Page/澳門新葡京酒店.md "wikilink")
<File:StPauls>
whole.JPG|[澳門](../Page/澳門.md "wikilink")[大三巴牌坊](../Page/大三巴牌坊.md "wikilink")

**[台湾](../Page/台湾.md "wikilink")**

檔案:101.portrait.altonthompson.jpg|[台北101](../Page/台北101.md "wikilink")
<File:Chiang_Kai-shek_memorial_amk.jpg>|[台北](../Page/台北市.md "wikilink")[中正紀念堂](../Page/中正紀念堂.md "wikilink")
<File:Taiwan> 2009 The Grand Hotel in Taipei FRD
7637.jpg|台北[圓山大飯店](../Page/圓山大飯店.md "wikilink")
[File:中華民國總統府.JPG|台北](File:中華民國總統府.JPG%7C台北)[總統府](../Page/總統府_\(臺灣政府建築\).md "wikilink")
<File:Miramar> Entertainment Park with the Ferris
Wheel.jpg|台北[美麗華百樂園](../Page/美麗華百樂園.md "wikilink")
<File:2016> 10 21 板橋
13.jpg|[新北市](../Page/新北市.md "wikilink")[新板特區](../Page/新板特區.md "wikilink")
<File:Taiwan> Hsinchu YinXi
Gate.JPG|[新竹](../Page/新竹市.md "wikilink")[竹塹城迎曦門](../Page/竹塹城.md "wikilink")
|[台中市政府](../Page/台中市政府.md "wikilink") <File:Taichung>
Park-1.JPG|[台中公園湖心亭](../Page/台中公園.md "wikilink")
<File:The> Luce Chapel
2.jpg|[台中](../Page/台中.md "wikilink")[路思義教堂](../Page/路思義教堂.md "wikilink")
<File:ChangHua>
Baguashan.jpg|[彰化](../Page/彰化.md "wikilink")[八卦山](../Page/八卦山.md "wikilink")
<File:台南孔廟.JPG>|[台南孔子廟](../Page/台南孔子廟.md "wikilink") <File:Kaohsiung> 85
Sky
Tower.JPG|[高雄](../Page/高雄.md "wikilink")[85大樓](../Page/85大樓.md "wikilink")

` Taipei Nan Shan Plaza 20171216c.jpg|`[`臺北南山廣場`](../Page/臺北南山廣場.md "wikilink")

<File:Taroko> Archway,taken by Tomas
Fano.jpg|[太魯閣牌樓](../Page/太魯閣牌樓.md "wikilink")

**[蒙古国](../Page/蒙古国.md "wikilink")**

<File:Ulan> Bator
16.JPG|[烏蘭巴托](../Page/烏蘭巴托.md "wikilink")[蘇赫巴托爾廣場](../Page/蘇赫巴托爾廣場.md "wikilink")

**[日本](../Page/日本.md "wikilink")**

|[富士山](../Page/富士山.md "wikilink") <File:Tokyo> Sky Tree
2012.JPG|[東京天空樹](../Page/東京天空樹.md "wikilink") <File:Tokyo>
Tower night.jpg|[東京鐵塔](../Page/東京鐵塔.md "wikilink") <File:Tokyo>
Metropolitan Government Building
Oka1.JPG|[東京都廳舍](../Page/東京都廳舍.md "wikilink")
[File:Kaminarimon1500.jpg|東京](File:Kaminarimon1500.jpg%7C東京)[淺草寺](../Page/淺草寺.md "wikilink")
|[東京](../Page/東京.md "wikilink")[彩虹大橋](../Page/彩虹橋_\(東京\).md "wikilink")
<File:Yokohama> MinatoMirai21.jpg|[橫濱地標大廈](../Page/橫濱地標大廈.md "wikilink")
<File:Todaiji18s3200.jpg>|[奈良市](../Page/奈良市.md "wikilink")[東大寺](../Page/東大寺.md "wikilink")
[File:金阁寺.jpg|京都](File:金阁寺.jpg%7C京都)[鹿苑寺](../Page/鹿苑寺.md "wikilink")
<File:Hokanji>
Kyoto01n4272.jpg|[京都](../Page/京都.md "wikilink")[八坂之塔](../Page/法觀寺.md "wikilink")
[File:Kyoto-Kiyomizudera,_Japan.jpg|京都](File:Kyoto-Kiyomizudera,_Japan.jpg%7C京都)[清水寺清水舞台](../Page/清水寺.md "wikilink")
<File:Osaka> Castle -
01.jpg|[大阪](../Page/大阪.md "wikilink")[大阪城](../Page/大阪城.md "wikilink")
<File:Abeno> Harukas 20140507-001.jpg
|[大阪](../Page/大阪.md "wikilink")[阿倍野HARUKAS](../Page/阿倍野HARUKAS.md "wikilink")
<File:Osakatowerday.jpg>|[大阪](../Page/大阪.md "wikilink")[通天閣](../Page/通天閣.md "wikilink")
<File:Kobe> Port Tower03bs3200.jpg|[神戶港塔](../Page/神戶港塔.md "wikilink")
<File:080405> nagoya csl sakura.JPG|[名古屋城](../Page/名古屋城.md "wikilink")
<File:Nagoya> TV Tower.JPG|[名古屋電視塔](../Page/名古屋電視塔.md "wikilink")
<File:JR> Central Towers01.JPG|[名古屋車站](../Page/名古屋車站.md "wikilink")
<File:Kamakura> Budda Daibutsu front
1885.jpg|[鎌倉大佛](../Page/鎌倉大佛.md "wikilink")
<File:TsurugaokaHachiman-M8867.jpg>|[鎌倉](../Page/鎌倉.md "wikilink")[鶴岡八幡宮](../Page/鶴岡八幡宮.md "wikilink")
<File:Torii> of Temple
Itsukushima.jpg|[宮島](../Page/宮島.md "wikilink")[嚴島神社鳥居](../Page/嚴島神社.md "wikilink")
<File:Sapporo> television tower
01.jpg|[札幌電視塔](../Page/札幌電視塔.md "wikilink")
<File:Sapporo> Clock Tower Hokkaido Japan
2.jpg|[札幌市鐘樓](../Page/札幌市鐘樓.md "wikilink")
<File:Seaside-momochi-2.jpg>|[福岡塔](../Page/福岡塔.md "wikilink")
<File:Naha_Shuri_Castle20s5s3200.jpg>|[那霸](../Page/那霸.md "wikilink")[首里城](../Page/首里城.md "wikilink")

**[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")**

<File:Gyeongbokgung-Gyeonghoeru-03.jpg>|[首爾](../Page/首爾.md "wikilink")[景福宮慶會樓](../Page/景福宮.md "wikilink")
<File:Seoul-Namdaemun-at.night-01.jpg>|[首爾](../Page/首爾.md "wikilink")[南大門](../Page/崇禮門.md "wikilink")
<File:Korea-Seoul-Bosingak-05.jpg>|[首爾](../Page/首爾.md "wikilink")[普信閣](../Page/普信閣.md "wikilink")
<File:Wongwt> N 首爾塔 (16941213800).jpg|[首爾塔](../Page/首爾塔.md "wikilink")
<File:Busan> tower 086.jpg|[釜山塔](../Page/釜山塔.md "wikilink") <File:Juche>
Tower.jpg|[平壤](../Page/平壤.md "wikilink")[主体思想塔](../Page/主体思想塔.md "wikilink")

**[东南亚](../Page/东南亚.md "wikilink")**

<File:Bangkok> Wat Phra Kaew Phra Sri Rattana
Chedi.jpg|[曼谷大皇宮](../Page/曼谷大皇宮.md "wikilink")[玉佛寺](../Page/玉佛寺_\(曼谷\).md "wikilink")
<File:Wat> Benchamabophit
face.jpg|[曼谷](../Page/曼谷.md "wikilink")[雲石寺](../Page/雲石寺.md "wikilink")
<File:Royal.Place.Phnom.Penh.Palais.Royal.Cambodge.001.jpg>|[金邊皇宮](../Page/金邊.md "wikilink")
<File:Phnompenh> skyline.jpg|金邊中央市場
<File:Shwedagon-Pano.jpg>|[仰光大金寺](../Page/仰光大金寺.md "wikilink")
<File:Pha> That Luang, Vientiane,
Laos.jpg|[永珍](../Page/永珍.md "wikilink")[塔鑾](../Page/塔鑾.md "wikilink")
<File:Kuala> Lumpur Sultan Abdul
Building.jpg|[苏丹阿都沙末大厦](../Page/苏丹阿都沙末大厦.md "wikilink")
<File:Firstchurchmelaka.JPG>|[马六甲基督堂](../Page/马六甲.md "wikilink")
<File:Angkor> wat
temple.jpg|[暹粒](../Page/暹粒.md "wikilink")[吳哥窟](../Page/吳哥窟.md "wikilink")
<File:Borobudur-Nothwest-view.jpg>|[婆羅浮屠](../Page/婆羅浮屠.md "wikilink")
<File:Jakarta> Pictures-4.jpg|[雅加達地標建築](../Page/雅加達.md "wikilink")
<File:Bunderan> Hotel Indonesia
Jakarta.JPG|[雅加達印尼飯店廣場噴泉](../Page/雅加達.md "wikilink")
<File:10-25-2005> JVBA Makati
Skyline.jpg|[馬尼拉大都會](../Page/馬尼拉大都會.md "wikilink")[馬卡蒂](../Page/馬卡蒂.md "wikilink")
<File:Merlion>, Dec
05.JPG|[新加坡](../Page/新加坡.md "wikilink")[魚尾獅雕像](../Page/鱼尾狮.md "wikilink")
<File:1> Singapore
skyline.jpg|[新加坡](../Page/新加坡.md "wikilink")[濱海灣](../Page/濱海灣_\(新加坡\).md "wikilink")
<File:KLCC>
PetronasTowers.JPG|[吉隆坡](../Page/吉隆坡.md "wikilink")[雙峰塔](../Page/雙峰塔.md "wikilink")
<File:KL> tower1.JPG|[吉隆坡塔](../Page/吉隆坡塔.md "wikilink") <File:Masjid>
Putra.jpg|[布城](../Page/布城.md "wikilink")[布特拉清真寺](../Page/布特拉清真寺.md "wikilink")
Kek Lok Si 7.jpg|[槟城极乐寺](../Page/槟城极乐寺.md "wikilink")
PenangBridge.jpg|[槟威大桥](../Page/槟威大桥.md "wikilink") Menara Alor Setar
- panoramio (1).jpg|[亚罗士打电讯塔](../Page/亚罗士打电讯塔.md "wikilink") Kuching New
State Legislative Assembly.jpg|[古晋议会大厦](../Page/古晋.md "wikilink")
<File:Tháp> Rùa
3.jpg|[河內](../Page/河內.md "wikilink")[還劍湖](../Page/還劍湖.md "wikilink")
<File:Chua> Mot Cot.jpg|河內[一柱寺](../Page/一柱寺.md "wikilink") Basílica de
Nuestra Señora, Ciudad Ho Chi Minh, Vietnam, 2013-08-14, DD
03.JPG|[胡志明市](../Page/胡志明市.md "wikilink")[西貢聖母聖殿主教座堂](../Page/西貢聖母聖殿主教座堂.md "wikilink")

**其它**

<File:IndiaGate.jpg>|[印度門](../Page/印度門.md "wikilink")
<File:Taj_Mahal_(south_view,_2006>).jpg|[阿格拉](../Page/阿格拉.md "wikilink")[泰姬瑪哈陵](../Page/泰姬瑪哈陵.md "wikilink")
<File:Chhatrapati> Shivaji Terminus (Victoria
Terminus).jpg|[孟買](../Page/孟買.md "wikilink")[賈特拉帕蒂·希瓦吉終點站](../Page/賈特拉帕蒂·希瓦吉終點站.md "wikilink")
<File:Mumbai>
TajMahalHotel.jpg|[孟買](../Page/孟買.md "wikilink")[泰姬瑪哈酒店](../Page/泰姬瑪哈酒店.md "wikilink")
[File:Modhera.jpg|摩多哈拉太陽廟聖井](File:Modhera.jpg%7C摩多哈拉太陽廟聖井)
<File:LotusDelhi.jpg>|[新德里](../Page/新德里.md "wikilink")[蓮花寺](../Page/蓮花寺_\(德里\).md "wikilink")
<File:FaisalMasjid.jpg>|[伊斯蘭瑪巴德](../Page/伊斯蘭瑪巴德.md "wikilink")[費薩爾清真寺](../Page/費薩爾清真寺.md "wikilink")
<File:Flag> of Pakistan on National
Monument.JPG|[伊斯蘭瑪巴德巴基斯坦紀念碑](../Page/伊斯蘭瑪巴德.md "wikilink")
<File:Patan1.jpg>|[拉利特普爾帕坦古城](../Page/拉利特普爾_\(尼泊爾\).md "wikilink")
<File:Swayambhunath> temple.jpg|[斯瓦揚布納特](../Page/斯瓦揚布納特.md "wikilink")
<File:City> of Baku 2011.jpg|[巴庫火焰塔](../Page/巴庫.md "wikilink")
<File:哈里發塔.jpg>|[杜拜](../Page/杜拜.md "wikilink")[哈里發塔](../Page/哈里發塔.md "wikilink")
<File:Entrance> to the Burj Al
Arab.jpg|[杜拜](../Page/杜拜.md "wikilink")[阿拉伯塔](../Page/阿拉伯塔.md "wikilink")
|[阿布達比](../Page/阿布達比.md "wikilink")[謝赫扎耶德大清真寺](../Page/謝赫扎耶德大清真寺.md "wikilink")
<File:Azadi> tower
9.jpg|[德黑蘭](../Page/德黑蘭.md "wikilink")[阿扎迪塔](../Page/阿扎迪塔.md "wikilink")
<File:Sattarkhan> Buildings and Milad
Tower.JPG|德黑蘭[默德塔](../Page/默德塔.md "wikilink")
|德黑蘭[霍梅尼陵](../Page/霍梅尼陵.md "wikilink") <File:Naghshe>
Jahan Square Isfahan
modified.jpg|[伊斯法罕](../Page/伊斯法罕.md "wikilink")[伊瑪目廣場](../Page/伊瑪目廣場.md "wikilink")
<File:Mazar-e> sharif - Steve
Evans.jpg|[馬扎里沙里夫藍色清真寺](../Page/馬扎里沙里夫.md "wikilink")
<File:Minaret> of jam 2009 ghor.jpg|[賈姆尖塔](../Page/賈姆尖塔.md "wikilink")
<File:Kingdom> Center
.jpg|[利雅德](../Page/利雅德.md "wikilink")[王國中心](../Page/王國中心.md "wikilink")
<File:Kaaba> mirror edit
jj.jpg|[麥加](../Page/麥加.md "wikilink")[天房](../Page/天房.md "wikilink")
<File:Abraj-al-Bait-Towers.JPG>|[麥加皇家鐘塔飯店](../Page/麥加皇家鐘塔飯店.md "wikilink")
<File:Doha> skyline in the morning
(12544910974).jpg|[多哈西灣區](../Page/多哈.md "wikilink")
<File:Le> Royal Hotel Amman.jpg|[安曼皇家飯店](../Page/安曼.md "wikilink")
<File:Downtownbeirut.jpg>|[貝魯特中央區](../Page/貝魯特中央區.md "wikilink")
<File:Palestine-2013(2)-Jerusalem-Temple> Mount-Dome of the Rock (SE
exposure).jpg|[耶路撒冷](../Page/耶路撒冷.md "wikilink")[圓頂清真寺](../Page/圓頂清真寺.md "wikilink")
<File:Al> aqsa moschee
2.jpg|[耶路撒冷](../Page/耶路撒冷.md "wikilink")[阿克薩清真寺](../Page/阿克薩清真寺.md "wikilink")
<File:Klagemauer.JPG>|[耶路撒冷](../Page/耶路撒冷.md "wikilink")[西牆](../Page/西牆.md "wikilink")
<File:TelAvivAvivMetroo.png>|[特拉維夫市中心](../Page/特拉維夫.md "wikilink")
<File:Samara> spiralovity minaret
rijen1973.jpg|[薩邁拉大清真寺](../Page/薩邁拉大清真寺.md "wikilink")
<File:Tachar> Persepolis Iran.JPG|[波斯波利斯](../Page/波斯波利斯.md "wikilink")

### 歐洲

<File:Tour> eiffel at sunrise from the
trocadero.jpg|[巴黎](../Page/巴黎.md "wikilink")[艾菲爾鐵塔](../Page/艾菲爾鐵塔.md "wikilink")
<File:Arc-de-triomphe-westside.jpg>|[巴黎凱旋門](../Page/巴黎凱旋門.md "wikilink")
<File:Louvre.jpg>|[巴黎](../Page/巴黎.md "wikilink")[羅浮宮](../Page/羅浮宮.md "wikilink")
<File:Palais> de Justice de
Lyon.JPG|[里昂正義宮](../Page/里昂正義宮.md "wikilink")
<File:Saint-benezet> in southeastern
France.jpg|[聖貝內澤橋](../Page/聖貝內澤橋.md "wikilink")
<File:Tower> of London, Traitors
Gate.jpg|[倫敦塔](../Page/倫敦塔.md "wikilink") <File:Buckingham>
Palace
-London-18Aug2008-4c.jpg|[倫敦](../Page/倫敦.md "wikilink")[白金漢宮](../Page/白金漢宮.md "wikilink")
London 01 2013 the Shard London Bridge
5205.JPG|[碎片大廈](../Page/碎片大廈.md "wikilink")
<File:Bigben.jpg>|[倫敦](../Page/倫敦.md "wikilink")[大笨鐘](../Page/大笨鐘.md "wikilink")
<File:London> Eye Twilight April
2006.jpg|[倫敦眼](../Page/倫敦眼.md "wikilink") <File:Tower>
bridge London Twilight - November
2006.jpg|[倫敦塔橋](../Page/倫敦塔橋.md "wikilink") <File:The>
Dubhlinn Gardens Dublin Castle
01.JPG|[都柏林城堡](../Page/都柏林城堡.md "wikilink")
<File:Brussels> Panorama
(8293237603).jpg|[布魯塞爾大廣場](../Page/布魯塞爾大廣場.md "wikilink")
<File:Bruxelles> Manneken
Pis.jpg|[布魯塞爾](../Page/布魯塞爾.md "wikilink")[尿尿小童](../Page/尿尿小童.md "wikilink")
<File:Minimundus117>
(edit).jpg|[布魯塞爾](../Page/布魯塞爾.md "wikilink")[原子球塔](../Page/原子球塔.md "wikilink")
<File:876MilanoDuomo.JPG>|[米蘭主教座堂](../Page/米蘭主教座堂.md "wikilink")
<File:Galleria> Vittorio Emanuele II -
evening.jpg|[米蘭](../Page/米蘭.md "wikilink")[艾曼紐二世迴廊](../Page/埃馬努埃萊二世拱廊.md "wikilink")
<File:Leaning> Tower of Pisa.jpg|[比薩斜塔](../Page/比薩斜塔.md "wikilink")
<File:Colosseum> in Rome, Italy - April
2007.jpg|[羅馬競技場](../Page/羅馬競技場.md "wikilink")
<File:Piazza> del
Campidoglio.jpg|[羅馬](../Page/羅馬.md "wikilink")[元老宮](../Page/元老宮.md "wikilink")
<File:Trevi> Fountain, Rome, Italy 2 - May
2007.jpg|[羅馬](../Page/羅馬.md "wikilink")[特萊維噴泉](../Page/特萊維噴泉.md "wikilink")
<File:Spanish> Steps & Trinita dei Monti from Piazza di Spagna
Rome.jpg|[羅馬](../Page/羅馬.md "wikilink")[西班牙廣場](../Page/西班牙廣場_\(羅馬\).md "wikilink")
<File:Piazza> San Marco in Venice, with St Mark's Campanile and Basilica
in the
background.jpg|[威尼斯](../Page/威尼斯.md "wikilink")[聖馬可廣場](../Page/聖馬可廣場.md "wikilink")
<File:Santa> Maria della Salute
Venice.JPG|[威尼斯大運河和](../Page/大運河_\(威尼斯\).md "wikilink")[安康聖母聖殿](../Page/安康聖母聖殿.md "wikilink")
<File:Petersdom> von Engelsburg
gesehen.jpg|[梵蒂岡](../Page/梵蒂岡.md "wikilink")[聖伯多祿大殿](../Page/聖伯多祿大殿.md "wikilink")
<File:Château> de Chillon -
Montreux.jpg|[維托](../Page/維托.md "wikilink")[西庸城堡](../Page/西庸城堡.md "wikilink")
<File:Amsterdam>
rijkmuseum.JPG|[阿姆斯特丹國家博物館](../Page/阿姆斯特丹國家博物館.md "wikilink")
<File:De> gooyer
amsterdam.jpg|[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[德古依爾風車](../Page/德古依爾風車.md "wikilink")
<File:Jardines> de Sabatini (Madrid)
06.jpg|[馬德里皇宮](../Page/馬德里皇宮.md "wikilink")
<File:Monumento> a Miguel de Cervantes -
03.jpg|[馬德里](../Page/馬德里.md "wikilink")[西班牙廣場](../Page/西班牙廣場_\(馬德里\).md "wikilink")
<File:MADRID> 060518 MXALX
087.jpg|[馬德里](../Page/馬德里.md "wikilink")[大都會大廈](../Page/大都會大廈_\(馬德里\).md "wikilink")
<File:Madrid>. La Cibeles
square.jpg|[馬德里](../Page/馬德里.md "wikilink")[西貝萊斯廣場](../Page/西貝萊斯廣場.md "wikilink")
<File:Tragabolas> en la Puerta del
Sol.jpg|[馬德里](../Page/馬德里.md "wikilink")[太陽門廣場](../Page/太陽門廣場.md "wikilink")
<File:Torre> de Belem
1.JPG|[里斯本](../Page/里斯本.md "wikilink")[貝倫塔](../Page/貝倫塔.md "wikilink")
<File:Lisboa-Padrão> dos
Descobrimentos.jpg|[里斯本](../Page/里斯本.md "wikilink")[發現者紀念碑](../Page/發現者紀念碑.md "wikilink")
<File:Brandenburger> Tor Blaue
Stunde.jpg|[柏林](../Page/柏林.md "wikilink")[布蘭登堡門](../Page/布蘭登堡門.md "wikilink")
<File:Berlin> reichstag west panorama
2.jpg|[柏林](../Page/柏林.md "wikilink")[帝國議會](../Page/帝國議會.md "wikilink")
(今德國聯邦下議會) <File:Cologne>
Cathedral.jpg|[科隆大教堂](../Page/科隆大教堂.md "wikilink")
<File:White> Tower Thessaloniki
2009.jpg|[塞薩洛尼基白塔](../Page/塞薩洛尼基白塔.md "wikilink")
<File:Ac.parthenon5.jpg>|[雅典衛城](../Page/雅典衛城.md "wikilink")
<File:SantoriniPartialPano.jpg>|[聖托里尼](../Page/聖托里尼.md "wikilink")
<File:Monaco> Cathedral
1.JPG|[摩納哥城](../Page/摩納哥城.md "wikilink")[聖母無染原罪主教座堂](../Page/聖母無染原罪主教座堂_\(摩納哥\).md "wikilink")
<File:Kremlin> birds eye
view-3.jpg|[莫斯科](../Page/莫斯科.md "wikilink")[克里姆林宮與](../Page/克里姆林宮.md "wikilink")[紅場](../Page/紅場.md "wikilink")

`Federation-Tower in July.jpg|`[`莫斯科`](../Page/莫斯科.md "wikilink")[`聯邦大廈`](../Page/聯邦大廈.md "wikilink")

City Of Capitals 20th October
2012.JPG|[莫斯科](../Page/莫斯科.md "wikilink")[首都之城](../Page/首都之城.md "wikilink")
<File:St> Basils
Cathedral-500px.jpg|[莫斯科](../Page/莫斯科.md "wikilink")[瓦西里·柏拉仁諾教堂](../Page/华西里·柏拉仁諾教堂.md "wikilink")
<File:Savior> on blood from
canal.jpg|[聖彼得堡](../Page/聖彼得堡.md "wikilink")[滴血救世主教堂](../Page/滴血救世主教堂.md "wikilink")
<File:Mamaev> kurgan
(ОКН).JPG|[馬馬耶夫山崗](../Page/馬馬耶夫山崗.md "wikilink")[祖國母親在召喚雕像](../Page/祖國母親在召喚.md "wikilink")
|[基輔](../Page/基輔.md "wikilink")[祖國母親雕像](../Page/:en:Mother_Motherland,_Kiev.md "wikilink")
<File:Atakule.JPG>|[安卡拉Atakule](../Page/安卡拉.md "wikilink") <File:Aya>
sofya.jpg|[伊斯坦布尔](../Page/伊斯坦布尔.md "wikilink")[聖索非亞大教堂](../Page/聖索非亞大教堂.md "wikilink")
<File:Büyük> Mecidiye Camii - Ortaköy
Mosque.jpg|[伊斯坦布尔](../Page/伊斯坦布尔.md "wikilink")[奧塔科伊清真寺](../Page/奧塔科伊清真寺.md "wikilink")
|[伊斯坦布尔少女塔](../Page/伊斯坦布尔.md "wikilink") <File:Kraków> - St. Mary Church
01.JPG|[克拉科夫](../Page/克拉科夫.md "wikilink")[聖母大殿](../Page/聖母大殿.md "wikilink")
<File:Żuraw> in Gdańsk.jpg|[格但斯克起重機](../Page/格但斯克.md "wikilink")
<File:Wrocław>, wieża ciśnień przy al.
Wiśniowej.JPG|[弗羅茨瓦夫水塔](../Page/弗羅茨瓦夫水塔.md "wikilink")
<File:Wiatrak>
Świnoujście.jpg|[希維諾烏伊希切Stawa](../Page/希維諾烏伊希切.md "wikilink")
Młyny 燈塔 檔案:Copenhagen - the little mermaid statue -
2013.jpg|[哥本哈根](../Page/哥本哈根.md "wikilink")[美人魚雕像](../Page/美人魚雕像.md "wikilink")
<File:PKiN> widziany z WFC.jpg|[華沙科學文化宮](../Page/華沙科學文化宮.md "wikilink")
<File:Narodowy-otwarcie19.JPG>|[華沙](../Page/華沙.md "wikilink")[國家體育場](../Page/國家體育場.md "wikilink")
<File:Helsinki> Cathedral in
winter.jpg|[赫爾辛基主教座堂](../Page/赫爾辛基主教座堂.md "wikilink")
<File:Palatul> Parlamentului
b.jpg|[羅馬尼亞議會宮](../Page/羅馬尼亞議會宮.md "wikilink")
<File:Praga>
0003.JPG|[布拉格](../Page/布拉格.md "wikilink")[布拉格天文鐘](../Page/天文鐘.md "wikilink")
<File:Old> Town Square, Prague - Staroměstské náměstí,
Praha.jpg|[布拉格](../Page/布拉格.md "wikilink")[老城廣場](../Page/老城廣場_\(布拉格\).md "wikilink")
<File:Graz> Uhrturm 2003 12.2.03
049.jpg|[格拉茨鐘樓](../Page/格拉茨鐘樓.md "wikilink")
<File:Wiener> Riesenrad
DSC02378.JPG|[維也納摩天輪](../Page/維也納摩天輪.md "wikilink")
<File:Sagrada_Familia_02.jpg>|[巴塞隆納](../Page/巴塞隆納.md "wikilink")[聖家堂](../Page/聖家堂.md "wikilink")
<File:Arc> de Triomf
Barcelona.jpg|[巴塞隆納](../Page/巴塞隆納.md "wikilink")[凱旋門](../Page/凱旋門_\(巴塞隆納\).md "wikilink")
<File:Corboba> mezquita1.jpg|[科爾多瓦主教座堂](../Page/科爾多瓦主教座堂.md "wikilink")
<File:Turning> Torso
3.jpg|[馬爾默](../Page/馬爾默.md "wikilink")[HSB旋轉中心](../Page/HSB旋轉中心.md "wikilink")
|[雷克雅未克](../Page/雷克雅未克.md "wikilink")[哈爾格林姆教堂](../Page/哈爾格林姆教堂.md "wikilink")
<File:Oslo> rådhus2.jpg|[奧斯陸市政廳](../Page/奧斯陸市政廳.md "wikilink")
<File:Nordlyskatedralen>, Alta, Northeast view 20150611
1.jpg|[阿塔北極光教堂](../Page/阿塔_\(挪威\).md "wikilink")
<File:Urnes> Stave Church
2.jpg|[烏爾內斯木板教堂](../Page/烏爾內斯木板教堂.md "wikilink")
<File:Gediminas> Tower in Vilnius
(cropped).jpg|[維爾紐斯](../Page/維爾紐斯.md "wikilink")[格迪米納斯塔](../Page/格迪米納斯塔.md "wikilink")

### 美洲

<File:Freiheitsstatue> NYC
full.jpg|[紐約](../Page/紐約.md "wikilink")[自由女神像](../Page/自由女神像.md "wikilink")
<File:Empire> State Building Apr
2005.jpg|紐約[帝國大廈](../Page/帝國大廈.md "wikilink")
<File:Chrysler> Building by David Shankbone
Retouched.jpg|紐約[克萊斯勒大廈](../Page/克萊斯勒大廈.md "wikilink")
[File:OneWorldTradeCenter.jpg|紐約](File:OneWorldTradeCenter.jpg%7C紐約)[世界貿易中心一號大樓](../Page/世界貿易中心一號大樓.md "wikilink")
<File:Brooklyn> Bridge
Postdlf.jpg|紐約[布魯克林大橋](../Page/布魯克林大橋.md "wikilink")
<File:New> york times
square-terabass.jpg|紐約[時代廣場](../Page/時代廣場.md "wikilink")
<File:WhiteHouseSouthFacade.JPG>|[華盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")[白宮](../Page/白宮.md "wikilink")
<File:Capitol> Building Full
View.jpg|[華盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")[國會山莊](../Page/美國國會大廈.md "wikilink")
<File:Washington> Monument Dusk Jan
2006.jpg|[華盛頓紀念碑](../Page/華盛頓紀念碑.md "wikilink")
<File:Sears> Tower
ss.jpg|[芝加哥](../Page/芝加哥.md "wikilink")[西爾斯大樓](../Page/西爾斯大樓.md "wikilink")
<File:Mountrushmore.jpg>|[南達科他州](../Page/南達科他州.md "wikilink")[拉什莫爾山](../Page/拉什莫爾山.md "wikilink")
<File:STL> Skyline 2007 crop (Gateway
Arch).jpg|[聖路易斯拱門](../Page/聖路易_\(密蘇里州\).md "wikilink")
<File:SkywalkFromOutsideLedge.jpg>|[大峽谷天空步道](../Page/大峽谷天空步道.md "wikilink")
<File:GoldenGateBridge-001.jpg>|[三藩市](../Page/三藩市.md "wikilink")[金門橋](../Page/金門橋.md "wikilink")
<File:Sfcityhall.jpeg>|[舊金山市政廳](../Page/舊金山市政廳.md "wikilink")
<File:Parliament> Hill,
Ottawa.jpg|[渥太華](../Page/渥太華.md "wikilink")[國會山莊](../Page/國會山莊_\(加拿大\).md "wikilink")
<File:Toronto's> CN
Tower.jpg|[多倫多](../Page/多倫多.md "wikilink")[加拿大国家电视塔](../Page/加拿大国家电视塔.md "wikilink")
<File:Calgary> tower 6.JPG|[卡加利塔](../Page/卡加利.md "wikilink")
<File:Canada> Place
Landing.jpg|[溫哥華](../Page/溫哥華.md "wikilink")[加拿大廣場](../Page/加拿大廣場.md "wikilink")
<File:Puente> Burrard, Vancouver, Canadá, 2017-08-14, DD
27.jpg|溫哥華[布勒橋](../Page/布勒橋.md "wikilink") <File:Montreal>
Clock Tower and Jacques Cartier
Bridge01.JPG|[蒙特婁鐘樓](../Page/蒙特婁.md "wikilink")
<File:Space-needle-cc-2.0.jpg>|[西雅图](../Page/西雅图.md "wikilink")[太空针塔](../Page/太空针塔.md "wikilink")
<File:Ahu-Akivi-1.JPG>|[復活節島](../Page/復活節島.md "wikilink")[摩艾石像](../Page/摩艾石像.md "wikilink")
<File:El> Castillo, Chichén
Itzá.jpg|[墨西哥犹加敦半岛](../Page/墨西哥.md "wikilink")[奇琴伊察](../Page/奇琴伊察.md "wikilink")
<File:Catedral> de México.jpg|[墨西哥城主教座堂](../Page/墨西哥城主教座堂.md "wikilink")
<File:PaseoAngelBicylistsDF.jpg>|[墨西哥城](../Page/墨西哥城.md "wikilink")[獨立紀念柱](../Page/獨立紀念柱.md "wikilink")
<File:Ciudad.de.Mexico.City.Distrito.Federal.DF.Paseo.Reforma.Skyline.jpg>|[墨西哥城](../Page/墨西哥城.md "wikilink")[改革大道天際](../Page/改革大道.md "wikilink")
<File:Torre> Latinoamericana Mexico
City.jpg|[墨西哥城拉丁美洲塔](../Page/墨西哥城.md "wikilink")
<File:80> - Machu Picchu - Juin 2009 -
edit.jpg|[馬丘比丘](../Page/馬丘比丘.md "wikilink")
<File:Costanera> Center Sep.
13.jpg|[圣地牙哥](../Page/聖地亞哥_\(智利\).md "wikilink")[大聖地牙哥塔](../Page/大聖地牙哥塔.md "wikilink")
<File:Virgen> San
Cristóbal.JPG|[圣地牙哥](../Page/聖地亞哥_\(智利\).md "wikilink")[圣克里斯托瓦尔山聖母像](../Page/:en:San_Cristóbal_Hill.md "wikilink")
<File:Catedral> metropol.jpg|[巴西利亞主教座堂](../Page/巴西利亞主教座堂.md "wikilink")
<File:Cristo> Redentor - Rio de Janeiro,
Brasil.jpg|[里約熱內盧基督像](../Page/里約熱內盧基督像.md "wikilink")
<File:PãoDeAçúcar>
2.jpg|[里約熱內盧](../Page/里約熱內盧.md "wikilink")[糖麵包山](../Page/糖麵包山.md "wikilink")
<File:Monserrate> Monastery
Colombia.jpg|[波哥大Monserrate](../Page/波哥大.md "wikilink")
<File:Capitolio>
full.jpg|[哈瓦那](../Page/哈瓦那.md "wikilink")[古巴科學院](../Page/國會大廈_\(哈瓦那\).md "wikilink")
<File:Revolution> square.jpg|哈瓦那[革命廣場](../Page/革命廣場.md "wikilink")
<File:The> Royal Tower Atlantis Paradise Island photo D Ramey
Logan.jpg|[拿騷](../Page/拿騷.md "wikilink")[亞特蘭蒂斯度假中心](../Page/亞特蘭蒂斯度假中心.md "wikilink")
<File:Bolivar-plaza-caballo-caracas.JPG>|[卡拉卡斯](../Page/卡拉卡斯.md "wikilink")[玻利瓦爾廣場](../Page/:en:Bolivar_Plaza_\(Caracas\).md "wikilink")
<File:Emancipation>
Park-Statues-1.jpg|[京斯敦解放公園](../Page/京斯敦.md "wikilink")

### 非洲、大洋洲

<File:Egypt.Giza.Sphinx.02.jpg>|[开罗](../Page/开罗.md "wikilink")[吉薩金字塔及](../Page/吉薩金字塔.md "wikilink")[人面獅身像](../Page/人面獅身像.md "wikilink")
<File:Cairotower.jpg>|[開羅塔](../Page/開羅塔.md "wikilink")
|[卡薩布蘭卡](../Page/卡薩布蘭卡.md "wikilink")[哈桑二世清真寺](../Page/哈桑二世清真寺.md "wikilink")
<File:Clock> tower, Tunis,
Tunisia.JPG|[突尼斯鐘塔](../Page/突尼斯.md "wikilink")
<File:Sea-gate-tunis.JPG>|[突尼斯海門](../Page/突尼斯.md "wikilink")
<File:Makamelchahid.JPG>|[阿爾及爾](../Page/阿爾及爾.md "wikilink")[烈士紀念碑](../Page/:en:Maqam_Echahid.md "wikilink")
<File:KICC> nairobi
kenya.jpg|[奈洛比](../Page/奈洛比.md "wikilink")[肯雅塔國際會議中心](../Page/:en:Kenyatta_International_Convention_Centre.md "wikilink")
<File:Tripoli> - Eingang zum
Nationalmuseum.jpg|[的黎波里](../Page/的黎波里.md "wikilink")[紅堡](../Page/:en:Red_Castle_Museum.md "wikilink")
<File:Luanda-SMiguelFort2.jpg>|[羅安達](../Page/羅安達.md "wikilink")[軍事博物館](../Page/:en:Museum_of_the_Armed_Forces_\(Angola\).md "wikilink")
<File:Timbuktu> Mosque Sankore.jpg|[廷巴克圖](../Page/廷巴克圖.md "wikilink")
<File:1997> 277-9A Agadez
mosque.jpg|[阿加德茲大清真寺](../Page/阿加德茲.md "wikilink")
<File:Central> mosque in
Nouakchott.jpg|[諾克少大清真寺](../Page/諾克少.md "wikilink")
<File:DakarGrandeMosquée.jpg>|[達喀爾大清真寺](../Page/達喀爾.md "wikilink")
<File:Lomé> Grand Marché with the Cathédrale du Sacré Coeur
(33592985581).jpg|[洛美聖心大教堂](../Page/洛美.md "wikilink") <File:Burj>
al-Fateh from Nile - by Nick
Hobgood.jpg|[喀土穆Corinthia飯店](../Page/喀土穆.md "wikilink")
<File:Sudan> Meroe Pyramids 2001.JPG|[麥羅埃](../Page/麥羅埃.md "wikilink")
<File:AbujaNationalMosque.jpg>|[阿布加國家清真寺](../Page/阿布加國家清真寺.md "wikilink")
<File:Zuma>
rock.jpg|[阿布賈](../Page/阿布賈.md "wikilink")[祖瑪岩](../Page/祖瑪岩.md "wikilink")
<File:Monument> Reunification.jpg|[雅溫得螺旋塔](../Page/雅溫得.md "wikilink")
<File:BasiliqueNDdelaPaix1.jpg>|[雅穆索戈和平之后大殿](../Page/雅穆索戈和平之后大殿.md "wikilink")
<File:The> National Arts Theatre in Lagos
(7099736099).jpg|[拉哥斯國家藝術劇院](../Page/拉哥斯.md "wikilink")
<File:Lekki> Ikoyi Link Bridge.jpg|[拉哥斯Lekki](../Page/拉哥斯.md "wikilink")
Ikoyi Link橋 <File:Zonnepoort>
tiwanaku.jpg|[蒂亞瓦納科](../Page/蒂亞瓦納科.md "wikilink")
<File:Bet> Giyorgis church Lalibela
03color.jpg|[拉利貝拉岩石教堂](../Page/拉利貝拉岩石教堂.md "wikilink")
<File:Katedrála> sv.
Jiří.jpg|[阿迪斯阿貝巴聖喬治大教堂](../Page/阿迪斯阿貝巴.md "wikilink")
<File:Basilique> - façade
principale.jpg|[布拉薩市](../Page/布拉薩市.md "wikilink")[聖安娜大教堂](../Page/:fr:Basilique_Sainte-Anne-du-Congo_de_Brazzaville.md "wikilink")
<File:Kirche>
Windhuk.JPG|[溫荷克](../Page/溫荷克.md "wikilink")[福音路德教堂](../Page/:en:Christ_Church,_Windhoek.md "wikilink")
<File:Grande> mosquee
porto-novo.jpg|[波多諾伏大教堂](../Page/波多諾伏.md "wikilink")
<File:Reconstructed> Rova Antananarivo
Madagascar.jpg|[馬達加斯加女王宮](../Page/馬達加斯加.md "wikilink")
<File:James> town 2.jpg|[阿克拉詹姆士敦燈塔](../Page/阿克拉.md "wikilink")
<File:Sun> City.jpg|[太陽城](../Page/太陽城_\(南非\).md "wikilink") <File:Hand>
der Fatima.jpg|[洪博里](../Page/洪博里.md "wikilink") <File:Sydney> Opera
House Sails edit02.jpg|[悉尼歌劇院](../Page/悉尼歌劇院.md "wikilink")
<File:SydneyHarbourBridge1>
gobeirne.jpg|[悉尼港灣大橋](../Page/悉尼港灣大橋.md "wikilink")
Q1 one.jpg|[Q1](../Page/Q1.md "wikilink") <File:1> flinders st station
melb.jpg|[弗林德斯街車站](../Page/弗林德斯街車站.md "wikilink")
<File:Ac.stpauls1.jpg>|[聖保羅座堂](../Page/聖保羅座堂_\(墨爾本\).md "wikilink")
<File:Royal> exhibition building tulips
straight.jpg|[皇家展覽館](../Page/皇家展覽館.md "wikilink")
<File:Auckland> skyline
(24045221962).jpg|[奧克蘭市](../Page/奧克蘭市.md "wikilink")[天空塔](../Page/天空塔.md "wikilink")
<File:Auckland> War Memorial Museum
rect.jpg|[奧克蘭戰爭紀念博物館](../Page/奧克蘭戰爭紀念博物館.md "wikilink")

## 参看

  - [旅遊](../Page/旅遊.md "wikilink")
  - [摩天輪](../Page/摩天輪.md "wikilink")
  - [觀光塔](../Page/觀光塔.md "wikilink")
  - [世界高塔列表](../Page/世界高塔列表.md "wikilink")
  - [摩天大樓列表](../Page/摩天大樓列表.md "wikilink")
  - [世界遺產](../Page/世界遺產.md "wikilink")
  - [世界新七大奇蹟](../Page/世界新七大奇蹟.md "wikilink")
  - [香港地標](../Page/香港地標.md "wikilink")

[\*](../Category/地標.md "wikilink")
[Category:航海](../Category/航海.md "wikilink")