**國際地球之友**（[英語](../Page/英語.md "wikilink")：**Friends of the Earth
International**,
**FOEI**）是一個國際間70餘國環保組織組成的網路，如[英國地球之友](../Page/英國.md "wikilink")、[韓國環境保護運動聯盟](../Page/韓國.md "wikilink")、[德國環境與自然保護聯盟等](../Page/德國環境與自然保護聯盟.md "wikilink")。國際地球之友擁有一個小型秘書處，位於[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，協助此聯盟體系運作與協調共同行動。執行委員會乃由各成員團體選出，參與制訂政策並審查秘書處工作。\[1\]

## 組織

國際地球之友是一個[邦聯制的聯盟團體](../Page/邦聯制.md "wikilink")，每個國家都有自己獨立的組織，並透過競選，將權利分配至的的全球網絡。國際地球之友最早的成員都集中在北美和歐洲，直到現在，其成員已包括了很多發展中國家的地球之友聯盟組織。國際地球之友的網絡相對於其他聯盟，較為鬆散，其成員團體多是在各國已經成立的環保團體，為了與國際連結而加入國際地球之友體系，因此偏重獨立運作，偶爾在行動、研究與會議上進行合作。也因為如此，各國地球之友成員擁有草根特性，能發揮區域整合的力量。

## 宗旨

國際地球之友的願景為根植於與自然合諧共存的社會的一個和平與續的世界。其宗旨包含下列六點：

1.  攜手確保[環境與](../Page/環境.md "wikilink")[社會正義](../Page/社會正義.md "wikilink")、人類尊嚴，並尊重人權與人類擁有安全[永續社會之權力](../Page/永續社會.md "wikilink")。
2.  停止與逆轉環境之弱化與自然資源之損耗，培育地球的[生態與](../Page/生物多樣性.md "wikilink")[文化多樣性](../Page/文化多樣性.md "wikilink")，確保永續的生計。
3.  保障[原住民](../Page/原住民.md "wikilink")、地方[社區](../Page/社區.md "wikilink")、[女性](../Page/女性.md "wikilink")、團體與個人的賦權，並確保決策的公共參與。
4.  以有創意的途徑與方式，朝向社會[永續與平等方向進行轉變](../Page/永續.md "wikilink")，
5.  投入活躍的行動，喚起意識、鼓勵民眾並與不同的運動組成聯盟，連結[草根](../Page/草根.md "wikilink")、國家與全球的抗爭。
6.  激勵彼此，利用、強化並補充彼此的能力，共度變遷，期望能團結合作。

## 運動

國際地球之友認為，環境問題在其社會，政治和人權的背景。他們的運動，伸展超越了傳統舞台的保護運動和尋求解決的經濟和發展方面的可持續性。

國際地球之友的運動\[2\]：

  - [氣候變化](../Page/氣候變化.md "wikilink")
  - [企業和公司問責制](../Page/企業和公司問責制.md "wikilink")
  - [基因改造](../Page/基因改造.md "wikilink")
  - [森林](../Page/森林.md "wikilink")
  - [國際金融機構](../Page/國際金融機構.md "wikilink")，如國際[貨幣](../Page/貨幣.md "wikilink")[基金組織](../Page/基金.md "wikilink")，[世界銀行](../Page/世界銀行.md "wikilink")，和出口信貸機構。
  - [貿易及其對](../Page/貿易.md "wikilink")[環境和](../Page/環境.md "wikilink")[可持續性](../Page/可持續性.md "wikilink")

## 歷史

1969年[David
Brower](https://en.wikipedia.org/wiki/David_Brower)辭去[美國](../Page/美國.md "wikilink")[塞拉俱樂部執行長職務](../Page/塞拉俱樂部.md "wikilink")，爾後籌組新組織。1971年由美國、法國、[瑞典與](../Page/瑞典.md "wikilink")[英國的四個組織組成地球之友](../Page/英國.md "wikilink")。秘書處為1981年設置，80年代期間，地球之友吸納[亞洲](../Page/亞洲.md "wikilink")、[中南美洲與](../Page/中南美洲.md "wikilink")[非洲的團體](../Page/非洲.md "wikilink")。

## 參考

<small>

<references/>

</small>

## 外部連結

  - [香港地球之友](https://web.archive.org/web/20080905124942/http://www.foe.org.hk/)註：香港地球之友並未加入國際地球之友網絡中。
  - [國際地球之友](http://www.foei.org/)
  - [歐洲地球之友](http://www.foeeurope.org/)
  - [英國地球之友](http://www.foe.co.uk/)
  - [西班牙地球之友](http://www.tierra.org)

[地球之友](../Category/地球之友.md "wikilink")
[Category:非政府間國際組織](../Category/非政府間國際組織.md "wikilink")
[Category:環境保護](../Category/環境保護.md "wikilink")
[Category:反核组织](../Category/反核组织.md "wikilink")
[Category:阿姆斯特丹组织](../Category/阿姆斯特丹组织.md "wikilink")
[Category:1969年建立的组织](../Category/1969年建立的组织.md "wikilink")

1.  [FOEI Who we are](http://www.foei.org/en/who-we-are/countries)
2.  [FOEI Campaigns](http://www.foei.org/en/campaigns/campaigns-home)