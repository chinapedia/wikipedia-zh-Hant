**地球化学**是使用[化学原理和工具来解释主要地质系统](../Page/化学.md "wikilink")，如[地壳及其](../Page/地壳.md "wikilink")[海洋背后机制的](../Page/海洋.md "wikilink")[科学](../Page/科学.md "wikilink")。\[1\]地球化学领域扩展到了地球以外，涵盖整个[太阳系](../Page/太阳系.md "wikilink")\[2\]，并且对于一些过程的理解做出了重要贡献，包括，[行星的形成](../Page/行星.md "wikilink")，和[花岗岩和](../Page/花岗岩.md "wikilink")[玄武岩的起源](../Page/玄武岩.md "wikilink")。\[3\]

地球化学是主要研究[地球各组成部分的化学成分及其变化规律](../Page/地球.md "wikilink")，化学过程及其制约因素，化学演化及其成因与机理的学科。是[地质学和](../Page/地质学.md "wikilink")[化学相互融合的边缘学科](../Page/化学.md "wikilink")，并涉及与自然过程有关的所有学科中的化学研究。[化学元素和](../Page/化学元素.md "wikilink")[同位素是其基本的研究对象](../Page/同位素.md "wikilink")。

## 参考文献

## 延伸阅读

  - Holland, H.D., & Turekian, K.K. (2004). [*Treatise on
    Geochemistry*](http://www.sciencedirect.com/science/referenceworks/9780080437514).
    9 Volumes. Elsevier
  - Marshall, C., & Fairbridge, R. (2006). [*Encyclopedia of
    Geochemistry*](http://www.springer.com/east/home/generic/search/results?SGWID=5-40109-22-33650945-0).
    ISBN 978-1-4020-4496-0. Berlin: Springer.
  - Bernard Gunn: [*The Geochemistry of Igneous
    Rocks*](http://www.Geokem.com)
  - [Gunter Faure](../Page/Gunter_Faure.md "wikilink") (1998),
    *Principles and Applications of Geochemistry*, 2nd edition,
    [Prentice Hall](../Page/Prentice_Hall.md "wikilink").
  - H.R. Rollinson (1993), *Using Geochemical Data: evaluation,
    presentation, interpretation*
    ([Longman](../Page/Longman.md "wikilink")). ISBN 978-0-582-06701-1
  - W.M. White: *Geochemistry* ([Free
    Download](http://www.imwa.info/geochemistry))
  - EarthDataModels library contains a logical data model and physical
    implementation of a Geochemistry database ([Free
    Download](http://www.earthdatamodels.org/designs/geochemistry_BGS.html))

## 参见

  - [勘查地球化学](../Page/勘查地球化学.md "wikilink")
  - [岩石学](../Page/岩石学.md "wikilink")
  - [宇宙化学](../Page/宇宙化学.md "wikilink")

{{-}}

[地球化学](../Category/地球化学.md "wikilink")

1.

2.

3.