\-{T|zh:地球帝国 III;zh-hans:地球帝国 III;zh-hant:地球帝國 III;zh-cn:地球帝国
III;zh-tw:世紀爭霸 III;zh-hk:世紀爭霸 III;zh-mo:世紀爭霸 III;}-
是一款运行于[windows平台的](../Page/windows.md "wikilink")[即时战略游戏](../Page/即时战略游戏.md "wikilink")，由[Mad
Doc Software开发](../Page/Mad_Doc_Software.md "wikilink")，[Sierra
Entertainment在](../Page/Sierra_Entertainment.md "wikilink")2007年11月6日发布。游戏只有5个时代；分別為古代、中古時代、殖民時代、現代和未來，比前作要少很多。本作是[地球帝国系列的最新作品](../Page/地球帝国系列.md "wikilink")。

## 游戏内容

游戏引入了一些新兵种和新建筑单位以及从[全面战争系列引入的](../Page/全面战争.md "wikilink")“自由战争系统”（freeform
campaign
structure）设计。玩家将体验到人类整个历史进程。游戏去掉了前作细化文明的设计，将全球文明整合进[西方文明](../Page/西方.md "wikilink")，[中东文明和](../Page/中东.md "wikilink")[东方文明](../Page/东方.md "wikilink")。而且每个文明都可以由玩家设定具体参数。

该作的宣传亮点之一就是“全球战争体验”，游戏有一个地图就是整个[地球](../Page/地球.md "wikilink")。在多人游戏部分，也加入了“统治地球”（World
Domination）模式。\[1\]

## 开发情况

该游戏的开发始于2005年\[2\]开发者利用[Sierra
Entertainment的官方论坛来收集玩家的意见](../Page/Sierra_Entertainment.md "wikilink")\[3\]\[4\]游戏引擎基于[地球帝国
II的](../Page/地球帝国_II.md "wikilink")[Gamebryo
2.0引擎](../Page/Gamebryo.md "wikilink")，但经过重新编写，使之更好的支持新的视觉特效，[布娃娃系统](../Page/布娃娃系统.md "wikilink")，以及大量的[多边形渲染](../Page/多边形.md "wikilink")，高[分辨率的](../Page/分辨率.md "wikilink")[材质贴图](../Page/材质贴图.md "wikilink").\[5\]\[6\]

## 参考资料

## 外部链接

  - 官方

<!-- end list -->

  -
  - [地球帝国
    III](https://web.archive.org/web/20071015035558/http://www.maddocsoftware.com/games_ee2.htm)
    [Mad Doc Software上的版面](../Page/Mad_Doc_Software.md "wikilink")

  - [地球帝国
    III](http://www.sierra.com/en/home/games/game_info.prod-L2NvbnRlbnQvc2llcnJhL2VuL3Byb2R1Y3RzL2VtcGlyZV9lYXJ0aF9paWk%3d.html)
    [Sierra
    Entertainment上的版面](../Page/Sierra_Entertainment.md "wikilink")

<!-- end list -->

  - 非官方

<!-- end list -->

  - [地球帝国天堂](http://ee.heavengames.com/) 玩家论坛

[de:Empire Earth\#Empire Earth
3](../Page/de:Empire_Earth#Empire_Earth_3.md "wikilink")

[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[3](../Category/地球帝國系列.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:Windows独占游戏](../Category/Windows独占游戏.md "wikilink")
[Category:即时战略游戏](../Category/即时战略游戏.md "wikilink")
[Category:Gamebryo引擎游戏](../Category/Gamebryo引擎游戏.md "wikilink")
[Category:多人及单人电子游戏](../Category/多人及单人电子游戏.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")
[Category:跨時代遊戲](../Category/跨時代遊戲.md "wikilink")

1.

2.
3.

4.

5.

6.