**地黃**（[学名](../Page/学名.md "wikilink")：**），又稱作**芐**、**芑**、**生地**，為[玄参科](../Page/玄参科.md "wikilink")[地黄属植物](../Page/地黄属.md "wikilink")。

地黄在传统纺织业中用作[黄色](../Page/黄色.md "wikilink")[染料](../Page/染料.md "wikilink")，故得名。地黄的[块根则為傳統](../Page/块根.md "wikilink")[中藥之一](../Page/中藥.md "wikilink")，最早出典於《[神農本草經](../Page/神農本草經.md "wikilink")》，依照[炮製方法在藥材上又分為](../Page/炮製.md "wikilink")**鮮地黃**、**乾地黃**與**熟地黃**。

## 原植物

[Rehmannia.JPG](https://zh.wikipedia.org/wiki/File:Rehmannia.JPG "fig:Rehmannia.JPG")株高15－30厘米，初夏开花，花大数朵，淡紅紫色。

## 產地

主要產地為[中國北方](../Page/中國.md "wikilink")，以[河南省](../Page/河南省.md "wikilink")[焦作市一帶](../Page/焦作市.md "wikilink")，如[溫縣](../Page/溫縣.md "wikilink")、[博愛縣](../Page/博愛縣.md "wikilink")、[武陟縣等地最为著名](../Page/武陟縣.md "wikilink")，在[朝鲜半岛和](../Page/朝鲜半岛.md "wikilink")[日本也有分布](../Page/日本.md "wikilink")。焦作市在古時為[怀庆府](../Page/怀庆府.md "wikilink")，因此地區出產的地黃功效最佳而頗負盛名，稱為「懷慶地黃」。

## 炮製方法

鮮生地即為採集新鮮者，可榨汁用，稱**地黃汁**。

乾生地為採集完後晒乾者。

熟地黃則是與良酒、[砂仁拌炒](../Page/砂仁.md "wikilink")，並經九次蒸、晒而得。炮製目的可使地黃的副作用（如：味苦、滋膩感、與服用後腸胃不適等情形）明顯的降低。

## 中医学用途

按炮製不同，性味、用途也不同。

### 鮮生地

新鲜地黄称为**鲜地黄**或**鲜生地**。

  - 性味

性寒，味甘苦。

  - 功效

清熱涼血，養陰生津
用於因[溫病或](../Page/溫病.md "wikilink")[熱毒而](../Page/熱病.md "wikilink")[吐](../Page/吐血.md "wikilink")[衄](../Page/衄血.md "wikilink")、發斑，可以清火、解熱、涼血而止血。

### 乾生地

經乾燥後的地黄稱为**乾地黄**或**乾生地**。

  - 性味

性涼，味甘微苦。

  - 功效

可以滋陰、涼血，治[消渴](../Page/消渴.md "wikilink")、[溫病入血分](../Page/溫病.md "wikilink")。主治阴虚内热、虚烦不眠、[月经过多等症](../Page/月经.md "wikilink")。

《[神農本草經](../Page/神農本草經.md "wikilink")》-{云}-：「乾地黄，味甘寒，主折跌、絶筋、傷中，逐血痺，填骨髓，長肌肉；作湯除寒熱、積聚，除痺。生者尤良，久服輕身不老。」

古方亦用在補腎藥，在《[傷寒雜病論桂林古本](../Page/傷寒雜病論桂林古本.md "wikilink")》中皆以乾生地入藥，如【[腎氣丸](../Page/腎氣丸.md "wikilink")】即採用乾生地、[桂枝](../Page/桂枝.md "wikilink")，配[黃酒服](../Page/黃酒.md "wikilink")；而不如後世相近的複方【[桂附地黃丸](../Page/桂附地黃丸.md "wikilink")】(又稱【金匱腎氣丸】)是採用熟地黃、[肉桂皮](../Page/肉桂.md "wikilink")。

### 熟地黃

经加工蒸制後的地黄称为**熟地黄**或**熟地**。

  - 性味

性微溫，味甘，歸肝、腎經。。

  - 功效

用於補血滋陰(尤其是腎陰)、益精填髓。主治肾虚阴亏、头晕目眩、腰酸、[遗精](../Page/遗精.md "wikilink")、[崩漏等症](../Page/崩漏.md "wikilink")。

[肝經調血用的知名複方](../Page/肝經.md "wikilink")——【[四物湯](../Page/四物湯.md "wikilink")】，尋常是以熟地黃入藥；除非病者有血熱、上火情形才改用乾生地。

## 註釋

1.  [漢方生藥地黃之考察](http://www.ohayoo.com.tw/%BA~%A4%E8%A5%CD%C3%C4%A6a%B6%C0%A4%A7%A6%D2%B9%EE.htm)

## 參見

  - [四物湯](../Page/四物湯.md "wikilink")
  - [六味地黄丸](../Page/六味地黄丸.md "wikilink")

## 外部連結

  - [中藥圖鑑--生地黃](http://www.herbno1.com/component/content/article/975-qingre/2780-2780.html)

  - [嘉南藥理科技大學藥材介紹--生地黄](https://web.archive.org/web/20060721123453/http://ethnomed.chna.edu.tw/medicane/viewcontent.php?id=36)

  - [地黃
    Dihuang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01122)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [地黃
    Dihuang](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00050)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [地黃 Di
    Huang](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00413)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[其](../Category/藥用植物.md "wikilink")
[Category:地黃屬](../Category/地黃屬.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")