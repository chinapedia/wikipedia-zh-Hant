**坂本九**（），是一名已故的日本籍名歌手及演員。

## 生平

[Kyu_Iku_Sakamoto.JPG](https://zh.wikipedia.org/wiki/File:Kyu_Iku_Sakamoto.JPG "fig:Kyu_Iku_Sakamoto.JPG")
坂本九出生於[神奈川縣](../Page/神奈川縣.md "wikilink")[川崎市](../Page/川崎市.md "wikilink")，本名為大島九，是家中最小的成員（排行第九）。其父母均在餐館中工作。坂本九幼年時曾在[茨城縣](../Page/茨城縣.md "wikilink")[笠間市居住](../Page/笠間市.md "wikilink")，以躲避戰亂。

坂本九於高中時開始唱歌。1958年，他加入了日本樂隊「[The
Drifters](../Page/The_Drifters.md "wikilink")」成為主音。是他的名曲之一。並於2001年由日本樂隊翻唱。

1961年，發表，獲得歷史性的巨大成功，也是他的生涯代表作。該曲亦被翻成多國語言的歌唱版本。

1971年，他和演員[柏木由紀子結婚](../Page/柏木由紀子.md "wikilink")，並誕下兩名女兒，分別是[大島花子和](../Page/大島花子.md "wikilink")[大島舞子](../Page/大島舞子.md "wikilink")。

他於公益工作方面也不遺餘力，他在日本是十分為小孩、長者及殘障人士熱誠服務。「何時都有明天」也是1964年東京傷殘人士奧運會的主題。

1985年8月12日，坂本九搭乘[日本航空123號班機往大阪時遇上空難身亡](../Page/日本航空123號班機空難.md "wikilink")，得年43歲。在飛機墜毀前的數分鐘，他寫下了與妻[柏木由紀子的訣別書](../Page/柏木由紀子.md "wikilink")。這場空難中，包括坂本九在內，共有520人死亡，是世界上單一飛行器出事的空難中，死傷最慘重的一宗。坂本九當時原定乘坐與日航123號班機同時刻出發的[全日空](../Page/全日空.md "wikilink")35號班機赴大阪，但由於全日空班機客滿，只得乘坐日航123號班機，並坐在這架[波音747SR上層的](../Page/波音747.md "wikilink")64H座位。\[1\]坂本九的遺體於8月14日被發現，並於空難發生95小時後（8月16日）被確認，依據是其遺體上佩戴的[笠間稻荷神社](../Page/笠間稻荷神社.md "wikilink")[墜飾](../Page/墜飾.md "wikilink")，而笠間稻荷神社正是坂本九的信仰所在。

[Kyu_Yachiyo.JPG](https://zh.wikipedia.org/wiki/File:Kyu_Yachiyo.JPG "fig:Kyu_Yachiyo.JPG")

## 成名作

[Kyu_Sakamoto_1964.png](https://zh.wikipedia.org/wiki/File:Kyu_Sakamoto_1964.png "fig:Kyu_Sakamoto_1964.png")画面）\]\]
坂本九最著名之作品為（中文直譯：[昂首向前走](../Page/昂首向前走.md "wikilink")，但在英國發售時則為了好記，而改取為與原意毫無關係的「Sukiyaki」，為日式料理「[壽喜燒](../Page/壽喜燒.md "wikilink")」之意）。這首歌除了在日本世代傳頌外，甚至在[美國也很受歡迎](../Page/美國.md "wikilink")，並曾在1963年連續三星期（6月15日至6月29日）蟬聯美國[BillboardHot](../Page/Billboard.md "wikilink")
100冠軍。美國公信榜自1958年設立至今50餘年，本曲依然是唯一曾登上該榜冠軍的日文歌曲。本曲也進入了英國單曲榜前10位。此歌的歌詞內容大約是講述一名孤獨一人在夜裡徘徊的男子，必須藉著抬頭向上望使眼淚不致落下的情景。自從發行，《Sukiyaki》世界各地總銷售量估計超過1300萬張。及後此歌於1981年由美國女子[R\&B組合](../Page/R&B.md "wikilink")翻唱，同樣與原曲一樣大受歡迎，並曾榮登美國100首最受歡迎歌曲流行榜達24周，最佳位置是第3名；此歌曲於1981年由香港歌手[杜麗莎翻唱為粵語歌曲](../Page/杜麗莎.md "wikilink")《[眉頭不再猛縐](../Page/眉頭不再猛縐.md "wikilink")》，再於1989年時由著名新秀冠軍梅艷芳改編成《願今宵一起醉死》（收錄於大碟《[情深惹恨苗](../Page/情深惹恨苗.md "wikilink")》和《In
Brasil》）；此歌曲於1994年由另一R\&B樂隊再次打入十大。

坂本九另一首榮登美國流行榜的歌曲為《[中华之夜](../Page/中华之夜_\(歌曲\).md "wikilink")》（China no
Yoru，），這首改編自[渡邊濱子演唱同名歌曲英日文混合歌曲於](../Page/渡邊濱子.md "wikilink")1963年排名第58位。他唯一一張在美國發行的專輯「**Sukiyaki
and Other Japanese Hits**」，在1963年曾榮登100張最受歡迎流行專輯達17周之久，最高排名是第14位。

1963年夏季，坂本九展開了世界巡迴演唱會，演出國家及地區包括[美國](../Page/美國.md "wikilink")、[德國](../Page/德国.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")，直至1964年初完成。當他在美國演出時，他亦成為了長壽綜藝節目「」其中一集的嘉賓。

## 曾推出的專輯

  - *《[Sukiyaki And Other Japanese
    Hits](../Page/Sukiyaki_And_Other_Japanese_Hits.md "wikilink")》*（1963年）
  - 《[坂本九紀念精選輯](../Page/坂本九紀念精選輯.md "wikilink")》（*Kyu Sakamoto Memorial
    Best*）（2005年）
  - 《[坂本九CD與DVD精選輯](../Page/坂本九CD與DVD精選輯.md "wikilink")》（*Kyu Sakamoto
    CD & DVD The best*）（2005年）

## 影视作品

  - 《》（1961年）
  - 《如果感到幸福你就拍拍手》（1964年）
  - 《》（1965年）
  - 《小九的伟大梦想（）》（1967年）
  - 《呐喊》（1975年）

## 軼事

  - [小行星6980被命名為坂本九](../Page/小行星6980.md "wikilink")，編號系由《昂首向前走》作詞者[永**六**輔](../Page/永六輔.md "wikilink")、演唱者坂本**九**與作曲者這三人名字的組合。
  - 坂本九的血型是A型
  - 互聯網上一度誤傳，日航123號班機殘骸中，在坂本九的行李內藏有[SM用品](../Page/施虐與受虐.md "wikilink")，後證實此消息為子虛烏有。\[2\]

## 紀念

  - [坂本九紀念堂](../Page/坂本九紀念堂.md "wikilink")（）

## 相关条目

  - [日本航空123號班機空難](../Page/日本航空123號班機空難.md "wikilink")

## 脚注

## 外部链接

  - [坂本九官方網站（日文）](http://www.sakamoto-kyu.com)
  - [Nippop關於坂本九簡介（英文）](http://nippop.com/artist/artist_id-117/artist_name-kyu_sakamoto)
  - [網際網路電影資料庫 坂本九資料](http://www.imdb.com/name/nm0757085)

[坂本九](../Category/坂本九.md "wikilink")
[Category:日本空難身亡者](../Category/日本空難身亡者.md "wikilink")
[Category:勝利娛樂旗下藝人](../Category/勝利娛樂旗下藝人.md "wikilink")
[Category:EMI音樂旗下藝人](../Category/EMI音樂旗下藝人.md "wikilink")
[Category:川崎市出身人物](../Category/川崎市出身人物.md "wikilink")

1.  ドラマ『20年目の誓い 〜天国にいるわが子へ〜』より。
2.