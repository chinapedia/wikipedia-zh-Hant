**坂本龍一**（，），是一位在[西方國家有影響力的](../Page/西方國家.md "wikilink")[日本](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[演员](../Page/演员.md "wikilink")，一生獲獎無數；所創作的音樂曲風空靈脫俗、融合東西古今，是日本當代繼[喜多郎之後](../Page/喜多郎.md "wikilink")，可稱為世界級音樂大師的代表人物，被譽為新音樂教父。

1999年後積極參與環保、和平活動，亦發起森林再造保育計畫“More
Tree”；2006年成立全新音樂品牌“commmons”，2009年2月於日本出版第一部口述自傳《音樂使人自由》\[1\]。

## 生涯與事業

坂本龍一出生于[東京都](../Page/東京都.md "wikilink")[中野區](../Page/中野區.md "wikilink")，自小就學習[古典音樂](../Page/古典音樂.md "wikilink")，畢業於[東京藝術大學研究所](../Page/東京藝術大學.md "wikilink")，主修[電子音樂及](../Page/電子音樂.md "wikilink")[民族音樂](../Page/民族音樂.md "wikilink")，渐露头角始于他与[细野晴臣以及](../Page/细野晴臣.md "wikilink")[高桥幸宏组建的合成器摇滚乐队](../Page/高桥幸宏.md "wikilink")[黃色魔術交響樂團](../Page/黃色魔術交響樂團.md "wikilink")（YMO）；该乐队在1980年代晚期和1990年代早期对[acid
house和](../Page/acid_house.md "wikilink")[techno运动有着开创式的影响](../Page/techno.md "wikilink")，另類搞怪風格也令人側目卻廣受歡迎（例如：早年1970年代末[黑膠唱片封面三人著](../Page/黑膠唱片.md "wikilink")[中國共產黨服](../Page/中國共產黨.md "wikilink")[紅星帽黑布鞋](../Page/紅星.md "wikilink")，與一名金髮美國[比基尼泳裝美女](../Page/比基尼.md "wikilink")，同桌翹、縮腳在長板凳打[麻將](../Page/麻將.md "wikilink")）。他和[大卫·西尔维安](../Page/大卫·西尔维安.md "wikilink")（David
Sylvian）合作了一系列[单曲和大部分西尔维安的专辑](../Page/单曲.md "wikilink")。

1992年，坂本與紐約音樂怪客DJ
Spooky为超过十亿观众通过电视收看的[巴塞罗那奥运会](../Page/1992年夏季奧林匹克運動會.md "wikilink")[开幕仪式谱曲](../Page/开幕仪式.md "wikilink")。

1998年為[三共製藥EB錠所製作的廣告配樂](../Page/三共製藥.md "wikilink")《Energy
flow》，於日本銷售達百萬張，療效遠勝於EB錠；坂本說：這首歌獻給承受社會壓力的人。

2014年7月坂本龍一在其官方網站說明醫師診斷[喉癌](../Page/喉癌.md "wikilink")，需要休養，決定暫時將工作放下，好好遵照醫師囑咐治療，並保證在痊癒後會再全心投入音樂工作。

## 電影配樂

1983年他在[大岛渚執導的電影](../Page/大岛渚.md "wikilink")《[俘虜](../Page/俘虜_\(電影\).md "wikilink")》（戦場のメリークリスマス／Merry
Christmas, Mr.
Lawrence）中，演出一位在印尼[戰俘營的跋扈日軍營長](../Page/戰俘營.md "wikilink")，參與演出此片的[演员還有英国摇滚歌手](../Page/演员.md "wikilink")[大卫·鲍伊與](../Page/大卫·鲍伊.md "wikilink")[北野武等](../Page/北野武.md "wikilink")。他还为此片写了电影音乐，其中由「Japan」樂團主唱[大衛·西爾維安所演唱的主题曲](../Page/大衛·西爾維安.md "wikilink")《[禁色](../Page/禁色.md "wikilink")》（*Forbidden
Colours*）引起了一定的轰动；此片在世界獲得極大成功，大受各界影評好評，且榮登[坎城影展評審團激賞大獎殊榮](../Page/坎城影展.md "wikilink")，並再次將上述三位演員演藝生涯再次捧上高峰。

1987年，他为[柏納多·貝托魯奇的影片](../Page/柏納多·貝托魯奇.md "wikilink")《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》（*The
Last
Emperor*）所作的音乐获得了[奥斯卡奖](../Page/奥斯卡奖.md "wikilink")，並再度飾演監控[溥儀的日本軍官](../Page/溥儀.md "wikilink")[甘粕正彦](../Page/甘粕正彦.md "wikilink")。他还为[佩德羅·阿爾莫多瓦爾的影片](../Page/佩德羅·阿爾莫多瓦爾.md "wikilink")《[高跟鞋](../Page/高跟鞋_\(電影\).md "wikilink")》（*High
Heels*）以及[奥利佛·史東的](../Page/奥利佛·史東.md "wikilink")《[野棕榈](../Page/野棕榈.md "wikilink")》（*Wild
Palms*）作曲，此外，[基努·李維主演的](../Page/基努·里维斯.md "wikilink")《[小活佛](../Page/小活佛.md "wikilink")》（*Little
Buddha*）及大島渚的閉門之作《[御法度](../Page/御法度.md "wikilink")》之電影配樂，亦出自坂本之手。

## 专辑

### 录音室专辑

[Ryuichi_Sakamoto_side.jpg](https://zh.wikipedia.org/wiki/File:Ryuichi_Sakamoto_side.jpg "fig:Ryuichi_Sakamoto_side.jpg")

*Thousand Knives*（1978年）
*B-2 Unit*（1980年）
*Left-Handed Dream*（1981年）
*Coda*（1983年）
*Illustrated Musical Encyclopedia*（1984年）
*Esperanto*（1985年）
*Neo Geo*（1987年）
*Beauty*（1989年）
*Heartbeat*（1991年）
*Sweet Revenge*（1994年）
*Smoochy*（1995年）
*Discord*（1997年）
*BTTB*（1998年）
*Comica*（2002年）
*Elephantism*（2002年）
*CM/TV*（2002年，[世嘉在](../Page/世嘉.md "wikilink")1998年發售的[家用遊戲機](../Page/家用遊戲機.md "wikilink")[Dreamcast的開機音樂均收錄於此](../Page/Dreamcast.md "wikilink")）
*CHASM*（2005年）
*out of noise*（2009年）
"async"（2017年）

### 电影配乐

*Merry Christmas Mr Lawrence* |
[俘虜](../Page/俘虜_\(電影\).md "wikilink")（1983年）
子猫物語（1986年）
[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")（1987年）
[末代皇帝](../Page/末代皇帝.md "wikilink")（1988年）
*The Sheltering Sky*（1991年）
*High Heels*（1991年）
*The Wuthering Heights*（1992年）
*Little Buddha*（1993年）
*Wild Side*（1995年）
*Snake Eyes*（1998年）
*Love is the Devil - Study for a portrait of Francis Bacon*（1999年）
鉄道員（1999年）
御法度（1999年）
*Alexei and the Spring*（2002年）
*Famme Fatale*（2002年）
*Derrida*（2003年）
[東尼瀧谷](../Page/東尼瀧谷.md "wikilink")（2004年）
星になった少年（2005年）
一命（2011年）
新しい靴を買わなくちゃ（2012年）
[神鬼獵人](../Page/神鬼獵人.md "wikilink")（2015年）
**怒**(2016年)
[请以你的名字呼唤我](../Page/请以你的名字呼唤我_\(电影\).md "wikilink")（2017年）
[南漢山城](../Page/南漢山城_\(電影\).md "wikilink")（2017年）
[你的臉](../Page/你的臉.md "wikilink")（2018年）

## 個人生活

坂本曾娶日本钢琴家兼歌手[矢野显子](../Page/矢野显子.md "wikilink")（現已離婚）为妻，并与她合作了一些她的专辑，女兒坂本美雨也在樂壇發展；[中谷美紀為其得意弟子](../Page/中谷美紀.md "wikilink")，與其合作作品有《[砂之果實](../Page/砂之果實.md "wikilink")》。

## 社會活動

坂本從學生時代起即積極投入社會運動，大學時代曾在[武滿徹的表演現場發放反武滿徹的小冊子](../Page/武滿徹.md "wikilink")，後來與武滿徹深談之後卻成為忘年之交，並於武滿徹過世後，於BTTB中以一曲Opus紀念他。他也因他对[版权法的批评而出名](../Page/版权法.md "wikilink")，他认为在[信息化时代那玩意已是非常落伍](../Page/信息化.md "wikilink")。

2001年因受到蘇格蘭人Chris
Moon所演出的紀錄影片「」\[2\]啟發，而發起反[地雷的音樂計劃](../Page/地雷.md "wikilink")-，並集結了為數眾多的世界音樂人共譜反地雷大作。2006年2月，因反對日本政府通過的《》（PSE法），自發性的募集7萬多名的反對法案簽名聯署；並於其個人[部落格內](../Page/部落格.md "wikilink")，反對[青森核電廠](../Page/青森核電廠.md "wikilink")\[3\]的建制。

## 參考資料

## 腳注

## 外部链接

  - [坂本龍一官方网站](https://www.sitesakamoto.com)（英文）
  - [坂本龍一索尼网站](http://www.ryuichi-sakamoto.com/)（英文）

[Category:日本音樂製作人](../Category/日本音樂製作人.md "wikilink")
[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本創作歌手](../Category/日本創作歌手.md "wikilink")
[Category:日本社會運動者](../Category/日本社會運動者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本唱片大獎編曲獎獲獎者](../Category/日本唱片大獎編曲獎獲獎者.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")
[Category:日本电影音乐作曲家](../Category/日本电影音乐作曲家.md "wikilink")
[Category:21世纪作曲家](../Category/21世纪作曲家.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:東京藝術大學校友](../Category/東京藝術大學校友.md "wikilink")
[Category:日本鍵盤手](../Category/日本鍵盤手.md "wikilink")
[Category:愛貝克思集團藝人](../Category/愛貝克思集團藝人.md "wikilink")
[Category:日本反核電活動家](../Category/日本反核電活動家.md "wikilink")
[Category:奥斯卡最佳原创音乐奖得主](../Category/奥斯卡最佳原创音乐奖得主.md "wikilink")

1.  [星（スター）を背負った人生　『音楽は自由にする』　坂本龍一著](http://ryukyushimpo.jp/news/prentry-142381.html)
2.  （關於[莫桑比克](../Page/莫桑比克.md "wikilink")16年内戰遺留下的大量[地雷](../Page/地雷.md "wikilink")，2005年11月25日播出）
3.  （參見[青森縣](../Page/青森縣.md "wikilink")[六所村](../Page/六所村.md "wikilink")）