**坂田街道**是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[龙岗区下辖的一个](../Page/龙岗区.md "wikilink")[街道](../Page/街道办事处.md "wikilink")\[1\]，2006年4月29日成立，位處深圳中北部，西邻[龙华区](../Page/龙华区.md "wikilink")，南接[福田区](../Page/福田区.md "wikilink")。

## 地理

辖区鄰近[龙华区](../Page/龙华区.md "wikilink")[民治街道和](../Page/民治街道.md "wikilink")[龍華街道](../Page/龍華街道.md "wikilink")，为[龙华](../Page/龙华区_\(深圳市\).md "wikilink")、[龙岗](../Page/龙岗区.md "wikilink")、[福田区三区交界](../Page/福田区.md "wikilink")。总面积28.51平方公里，下辖9个[社区](../Page/社区.md "wikilink")，总人口46.8万人，常住人口40多万人，其中户籍人口3.6万人。

由於坂田街道較為接近[福田區等商業中心](../Page/福田區.md "wikilink")，自2000年代開始出現了大量樓盤，如四季花城、万科城等，不少居民都是在原关内上班。另外坂田街道也有一個高新技术产业带，在此地区主要從事電子研發，代表者有[華為](../Page/華為.md "wikilink")。

## 行政区划

坂田街道下辖以下地区：\[2\]

。

## 經濟

坂田街道有规划面积达2.7平方公里的坂雪岗高新技术产业区。2007年街道工业总产值1145.06亿元，其中工业总产值超过1亿元的企业有24家。坂雪岗高新技术产业区最著名的有華為基地。

## 交通

### 长途客运

  - [坂田汽车站](../Page/坂田汽车站.md "wikilink")
  - [坂田火车站](../Page/坂田站_\(平南铁路\).md "wikilink")（已停运）

### 鐵路

  - [平南铁路](../Page/平南铁路.md "wikilink")
  - [深圳地鐵5號線](../Page/深圳地鐵5號線.md "wikilink")
  - [深圳地鐵10號線](../Page/深圳地鐵10號線.md "wikilink")（興建中）

### 道路

  - [梅观高速公路](../Page/梅观高速公路.md "wikilink")
  - [机荷高速公路](../Page/机荷高速公路.md "wikilink")
  - [南坪快速路](../Page/南坪快速路.md "wikilink")
  - [布龙路](../Page/布龙路.md "wikilink")
  - [清平高速公路](../Page/清平高速公路.md "wikilink")
  - [五和大道](../Page/五和大道.md "wikilink")
  - [坂雪岗大道](../Page/坂雪岗大道.md "wikilink")
  - [坂瀾大道](../Page/坂瀾大道.md "wikilink")
  - [吉华路](../Page/吉华路.md "wikilink")

在華為基地一帶有不少以中外古今科學家命名的街道，包括：

  - 稼先路（[邓稼先](../Page/邓稼先.md "wikilink")）
  - 冲之大道（[祖冲之](../Page/祖冲之.md "wikilink")）
  - 隆平路（[袁隆平](../Page/袁隆平.md "wikilink")）
  - 貝爾路（[亚历山大·格拉汉姆·贝尔](../Page/亚历山大·格拉汉姆·贝尔.md "wikilink")）
  - 居里夫人大道（[玛丽·居里](../Page/玛丽·居里.md "wikilink")）
  - 張衡路（[張衡](../Page/張衡.md "wikilink")）

## 参考资料

1.  [坂田街道简介](https://web.archive.org/web/20080830081858/http://www.szlgnews.com/jdxl/content/2008-04/21/content_1991113.htm)，龙岗新闻网，2008年7月5日造访。

[Category:龙岗区行政区划](../Category/龙岗区行政区划.md "wikilink")
[龙岗区](../Category/深圳街道_\(行政区划\).md "wikilink")

1.
2.