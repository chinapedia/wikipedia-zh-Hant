**坂町**（）是位於日本中國地方[廣島縣西南部沿海的一](../Page/廣島縣.md "wikilink")[町](../Page/町.md "wikilink")，沿海部分屬於[廣島港的一部分](../Page/廣島港.md "wikilink")。

## 历史

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，成立**坂村**。
  - 1950年8月1日：改制為**坂町**。

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [吳線](../Page/吳線.md "wikilink")：[坂車站](../Page/坂車站.md "wikilink")
        - [水尻車站](../Page/水尻車站.md "wikilink") -
        [小屋浦車站](../Page/小屋浦車站.md "wikilink")

### 道路

  - 收費道路

<!-- end list -->

  - [廣島吳道路](../Page/廣島吳道路.md "wikilink")：坂休息區 - 坂北交流道 - 坂南交流道
  - [海田大橋](../Page/海田大橋.md "wikilink")：海田大橋出入口

## 教育

### 大學

  - [廣島文化學園大學坂校區](../Page/廣島文化學園大學.md "wikilink")

### 高等學校

  - [廣島翔洋高等學校](../Page/廣島翔洋高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [川本町](../Page/川本町.md "wikilink")（[島根縣](../Page/島根縣.md "wikilink")[邑智郡](../Page/邑智郡.md "wikilink")）

## 外部連結