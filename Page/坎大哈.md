[View_of_Arghandab_Valley.JPG](https://zh.wikipedia.org/wiki/File:View_of_Arghandab_Valley.JPG "fig:View_of_Arghandab_Valley.JPG")
**坎大哈**（，），[阿富汗第二大](../Page/阿富汗.md "wikilink")[城市](../Page/城市.md "wikilink")，位于该国南部，是阿富汗南部的[经济](../Page/经济.md "wikilink")、[文化](../Page/文化.md "wikilink")、[交通中心](../Page/交通.md "wikilink")，也是[普什图族聚居的中心城市](../Page/普什图族.md "wikilink")。

坎大哈建城于公元前4世纪，在很长的时间里都是[波斯和](../Page/波斯.md "wikilink")[印度诸王朝争夺的](../Page/印度.md "wikilink")[军事重镇](../Page/军事.md "wikilink")。1747年，[艾哈迈德·沙阿·杜兰尼在此称王](../Page/艾哈迈德·沙阿·杜兰尼.md "wikilink")，建立[阿富汗王国](../Page/阿富汗王国.md "wikilink")，以坎大哈为[首都](../Page/首都.md "wikilink")。虽然阿富汗的首都很快便迁往[喀布尔](../Page/喀布尔.md "wikilink")，但是坎大哈作为阿富汗南部地区中心的地位自此一直未被动摇。

1995年，[塔利班便是从坎大哈突然崛起](../Page/塔利班.md "wikilink")，几乎一统阿富汗。阿富汗前总统[哈米德·卡尔扎伊](../Page/哈米德·卡尔扎伊.md "wikilink")，也是出身坎大哈的部族首领家庭。

## 氣候

## 參考文獻

[Category:阿富汗城市](../Category/阿富汗城市.md "wikilink")
[坎大哈](../Category/坎大哈.md "wikilink")