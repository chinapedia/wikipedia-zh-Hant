**坎莫尔诺蒂克中心省立公园**（）是[加拿大](../Page/加拿大.md "wikilink")[艾伯塔省的一个省立](../Page/艾伯塔省.md "wikilink")[公园](../Page/公园.md "wikilink")，位于[坎莫尔的西面](../Page/坎莫尔.md "wikilink")，距[卡尔加里](../Page/卡尔加里.md "wikilink")105[公里](../Page/公里.md "wikilink")。

公园坐落在[落基山脉](../Page/落基山脉.md "wikilink")[伦多山脚下](../Page/伦多山.md "wikilink")，沿着[弓河山谷和](../Page/弓河.md "wikilink")[冰原公路](../Page/冰原公路.md "wikilink")，海拔1,400[米](../Page/米.md "wikilink")，面积4.5[平方公里](../Page/平方公里.md "wikilink")，是[卡纳纳斯基斯镇公园系统的一部分](../Page/卡纳纳斯基斯镇.md "wikilink")。

## 1988年奥运会

坎莫尔诺蒂克中心承办了[1988年冬季奥林匹克运动会的部分项目](../Page/1988年冬季奥林匹克运动会.md "wikilink")，包括[越野滑雪](../Page/越野滑雪.md "wikilink")、[冬季两项和](../Page/冬季两项.md "wikilink")[北欧两项](../Page/北欧两项.md "wikilink")。

[Category:加拿大省立公园](../Category/加拿大省立公园.md "wikilink")