**坎迪多·波爾蒂納里**（**Candido
Portinari**，），[巴西最重要的](../Page/巴西.md "wikilink")[畫家之一](../Page/畫家.md "wikilink")。他也是繪畫界[新寫實主義](../Page/新寫實主義.md "wikilink")（[Neorealism](../Page/:en:Neorealism_\(art\).md "wikilink")）風格突出且具有影響力的實踐者。
[Discovery_of_the_Land1.jpg](https://zh.wikipedia.org/wiki/File:Discovery_of_the_Land1.jpg "fig:Discovery_of_the_Land1.jpg")
他出身是[義大利](../Page/義大利.md "wikilink")[移民](../Page/移民.md "wikilink")，出生於靠近[聖保羅州](../Page/聖保羅州.md "wikilink")[布羅多斯基](../Page/布羅多斯基.md "wikilink")（[Brodowski](../Page/:en:Brodowski,_São_Paulo.md "wikilink")）附近的[咖啡農園中](../Page/咖啡.md "wikilink")。波爾蒂納里於[里約熱內盧的](../Page/里約熱內盧.md "wikilink")[里約熱內盧聯邦大學](../Page/里約熱內盧聯邦大學.md "wikilink")（[Universidade
Federal do Rio de
Janeiro](../Page/:en:Universidade_Federal_do_Rio_de_Janeiro.md "wikilink")）的[美術學院](../Page/里約熱內盧聯邦大學美術學院.md "wikilink")（[Escola
Nacional de Belas
Artes](../Page/:en:Escola_Nacional_de_Belas_Artes.md "wikilink")，ENBA）就讀。1928年，他贏得[美術學院金牌獎](../Page/里約熱內盧聯邦大學美術學院.md "wikilink")，並前往[巴黎旅行](../Page/巴黎.md "wikilink")。在1930年回到[巴西以前](../Page/巴西.md "wikilink")，他都待在[巴黎](../Page/巴黎.md "wikilink")。

他在1947年加入[巴西共產黨](../Page/巴西共產黨.md "wikilink")（[Partido Comunista
Brasileiro](../Page/:en:Brazilian_Communist_Party.md "wikilink")）並成為[參議院](../Page/巴西參議院.md "wikilink")（[Senado
Federal](../Page/:en:Senate_of_Brazil.md "wikilink")）代表，但是卻因為對於[共產主義者的迫害被迫逃離](../Page/共產主義.md "wikilink")[巴西到](../Page/巴西.md "wikilink")[烏拉圭](../Page/烏拉圭.md "wikilink")。他在1951年再次回到[巴西](../Page/巴西.md "wikilink")，但卻在生命的尾聲遭受疾病的打擊，並且於1962年逝世。死因是來自他[畫作的](../Page/繪畫.md "wikilink")[鉛中毒](../Page/鉛中毒.md "wikilink")（[Lead
poisoning](../Page/:en:Lead_poisoning.md "wikilink")）。

2007年12月20日，他的畫作《咖啡種植園工人》（[O lavrador de
café](http://ritualcafe.files.wordpress.com/2007/07/lavrador-de-cafe.jpg)）於[聖保羅藝術博物館與](../Page/聖保羅藝術博物館.md "wikilink")[巴伯羅·畢卡索的](../Page/巴伯羅·畢卡索.md "wikilink")《[蘇珊布洛赫的肖像](../Page/蘇珊寶殊的肖像.md "wikilink")》（[Retrato
de Suzanne
Bloch](../Page/:en:Portrait_of_Suzanne_Bloch.md "wikilink")）一起被竊。[聖保羅警方於](../Page/聖保羅.md "wikilink")2008年1月8日宣布波爾蒂納里與[畢卡索遭竊的畫作已經尋獲](../Page/巴伯羅·畢卡索.md "wikilink")。警方也拘留了兩個嫌犯。

## 外部連結

  - [波爾蒂納里計畫美術館（Casa de Portinari）](http://casadeportinari.com.br/)

  - [波爾蒂納里計畫](http://www.portinari.org.br/)，波爾蒂納里的兒子Joan
    Portinari為了紀念其父誕生百年而發起的計畫，蒐集了他全部的作品

  - [Portinari in Dezenovevinte - Arte Brasileira do Século XIX e Início
    do XX](http://www.dezenovevinte.net/bios/bio_cp.htm)

  - [個人傳記](https://web.archive.org/web/20061119193035/http://www.vidaslusofonas.pt/candido_portinari2.htm)

[Category:巴西畫家](../Category/巴西畫家.md "wikilink")
[Category:現代藝術](../Category/現代藝術.md "wikilink")