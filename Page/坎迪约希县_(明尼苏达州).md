**坎迪約希縣** （**Kandiyohi County,
Minnesota**）是[美國](../Page/美國.md "wikilink")[明尼蘇達州中南部的一個縣](../Page/明尼蘇達州.md "wikilink")。面積2,232平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口41,203人。縣治[威爾馬](../Page/威爾馬_\(明尼蘇達州\).md "wikilink")（Wilmar）。

成立於1858年3月20日，1866年被廢，1871年重立。縣名來自同名的湖群，[蘇語的意思是](../Page/蘇語.md "wikilink")「[牛胭脂魚來的地方](../Page/牛胭脂魚.md "wikilink")」。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[K](../Category/明尼苏达州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.