**坦尚尼亞先令**是[坦尚尼亞的流通](../Page/坦尚尼亞.md "wikilink")[貨幣](../Page/貨幣.md "wikilink")。[貨幣編號TZS](../Page/ISO_4217.md "wikilink")。但在坦尚尼亞，[美元也被廣泛使用](../Page/美元.md "wikilink")。輔幣單位為分，1先令=100分。

## 流通中的貨幣

### 硬幣

### 紙幣

#### 1997年

[缩略图](https://zh.wikipedia.org/wiki/File:Tanzanian_shilling_banknote_set_1997.jpg "fig:缩略图")

  - 500 先令
      - 正面：[坦桑尼亚国徽](../Page/坦桑尼亚国徽.md "wikilink")、[长颈鹿](../Page/长颈鹿.md "wikilink")、[斑馬](../Page/斑馬.md "wikilink")
      - 背面：收成[丁香](../Page/丁香.md "wikilink")、烏呼魯火炬
  - 1,000 先令（1997年）
      - 正面：坦桑尼亚国徽、长颈鹿、[非洲象](../Page/非洲象.md "wikilink")
      - 背面：[煤礦](../Page/煤礦.md "wikilink")、[桑吉巴人民銀行](../Page/桑吉巴.md "wikilink")
  - 1,000 先令（2000年）
      - 正面：坦桑尼亚国徽、[朱利叶斯·尼雷尔](../Page/朱利叶斯·尼雷尔.md "wikilink")、[非洲象](../Page/非洲象.md "wikilink")
      - 背面：煤礦、桑吉巴人民銀行
  - 5,000 先令
      - 正面：坦桑尼亚国徽、长颈鹿、[犀牛](../Page/犀牛.md "wikilink")
      - 背面：长颈鹿、[乞力马扎罗山](../Page/乞力马扎罗山.md "wikilink")
  - 10,000 先令
      - 正面：坦桑尼亚国徽、長頸鹿、[獅子](../Page/獅子.md "wikilink")
      - 背面：[坦桑尼亚银行](../Page/坦桑尼亚银行.md "wikilink")、奇蹟之屋

#### 2003年

[缩略图](https://zh.wikipedia.org/wiki/File:Tanzanian_shilling_banknote_set_2003.jpg "fig:缩略图")

  - 500 先令
      - 正面：[非洲水牛](../Page/非洲水牛.md "wikilink")
      - 背面：恩克魯瑪大廳、[达累斯萨拉姆大学](../Page/达累斯萨拉姆大学.md "wikilink")
  - 1,000 先令
      - 正面：[朱利叶斯·尼雷尔](../Page/朱利叶斯·尼雷尔.md "wikilink")
      - 背面：州議會大廈、[达累斯萨拉姆](../Page/达累斯萨拉姆.md "wikilink")
  - 2,000 先令
      - 正面：[狮子](../Page/狮子.md "wikilink")、[乞力马扎罗山](../Page/乞力马扎罗山.md "wikilink")
      - 背面：[古堡](../Page/古堡.md "wikilink")、[桑給巴爾的](../Page/桑給巴爾.md "wikilink")[桑给巴尔石头城](../Page/桑给巴尔石头城.md "wikilink")
  - 5,000 先令
      - 正面：[黑犀牛](../Page/黑犀牛.md "wikilink")
      - 背面：蓋塔的金礦、[桑給巴爾的奇蹟之屋](../Page/桑給巴爾.md "wikilink")
  - 10,000 先令
      - 正面：[大象](../Page/大象.md "wikilink")
      - 背面：[坦桑尼亚银行](../Page/坦桑尼亚银行.md "wikilink")

#### 2011年

  - 500 先令
      - 正面：
      - 背面：
  - 1,000 先令
      - 正面：
      - 背面：
  - 2,000 先令
      - 正面：
      - 背面：
  - 5,000 先令
      - 正面：
      - 背面：
  - 10,000 先令
      - 正面：
      - 背面：

## 匯率

## 外部連結

[Category:1966年面世的貨幣](../Category/1966年面世的貨幣.md "wikilink")