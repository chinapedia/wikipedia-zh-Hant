[2010](../Page/2010年美國聯盟分區賽.md "wikilink") | misc5 = | OTHER DIV CHAMPS
= | WC = (2) | Wild Card
=[2011](../Page/2011年美國聯盟分區賽.md "wikilink"){{·}}[2013](../Page/2013年美國聯盟分區賽.md "wikilink")
| misc6 = | current league = 美國聯盟 | y1 = 1998年 | division =
[美聯東區](../Page/美國聯盟東區.md "wikilink") | y2 = 1998年 | misc2 = | y3
= 2008年 | pastnames = 坦帕灣魔鬼魚(－) | ballpark =
[純品康納室內球場](../Page/純品康納室內球場.md "wikilink")
| y4 = 1998年 | pastparks = | Uniform = ALE-Uniform-TB.png |
retirednumbers =
[12](../Page/Wade_Boggs.md "wikilink")，[42](../Page/傑基·羅賓森.md "wikilink")，[66](../Page/唐·季默.md "wikilink")
| Team =坦帕灣魔鬼魚 | Team1 =坦帕灣光芒 | manager =
[凱文·凱許](../Page/凱文·凱許.md "wikilink") | gm = [Andrew
Friedman](../Page/Andrew_Friedman.md "wikilink") | Uniform logo = Al
2005 tampabay 01.gif }}

**坦帕灣光芒**（Tampa Bay
Rays）是位於[佛羅里達州](../Page/佛羅里達州.md "wikilink")[聖彼德斯堡的美國職棒大聯盟球隊](../Page/聖彼德斯堡_\(佛羅里達州\).md "wikilink")，隸屬美國聯盟東區。於1998年的大聯盟擴充計畫中加入美國聯盟，主場是[純品康納室內球場](../Page/純品康納室內球場.md "wikilink")（）。

## 加入大聯盟之前

坦帕灣地區和職業棒球隊有關的歷史可以回溯到1913年，那年[芝加哥小熊隊把他們的春訓基地移到佛州坦帕](../Page/芝加哥小熊.md "wikilink")。聖彼德堡第一次成為大聯盟球隊的春訓基地是在1922年，亞特蘭大勇士隊進城舉行春訓。從此以後，有數支大聯盟球隊都曾在聖彼德堡進行春訓\[1\]。

聖彼德堡曾有9支大聯盟球隊舉行春訓：[巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")、[波士頓勇士](../Page/波士頓勇士.md "wikilink")、[紐約巨人](../Page/舊金山巨人.md "wikilink")、[紐約大都會](../Page/紐約大都會.md "wikilink")、[紐約洋基](../Page/紐約洋基.md "wikilink")、[費城費城人](../Page/費城費城人.md "wikilink")、[聖路易棕襪](../Page/巴爾的摩金鶯.md "wikilink")、[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")，以及現在的坦帕灣光芒隊。曾在坦帕進行春訓的則有7支球隊：
[波士頓紅襪](../Page/波士頓紅襪.md "wikilink")、[芝加哥小熊](../Page/芝加哥小熊.md "wikilink")、[芝加哥白襪](../Page/芝加哥白襪.md "wikilink")、[辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")、[底特律老虎](../Page/底特律老虎.md "wikilink")、[華盛頓參議員以及目前春訓球場設於坦帕的紐約洋基](../Page/明尼蘇達雙城.md "wikilink")。

坦帕灣區也曾是許多[小聯盟球隊的主場](../Page/小聯盟.md "wikilink")。1919年坦帕加入D級[佛州聯盟](../Page/佛州聯盟.md "wikilink")（Florida
State
League，FSL）。聖彼德堡也曾在1920年於佛州聯盟擁有1支球隊。兩個城市都是佛州聯盟的強隊。現在的佛州聯盟已改制為1A級小聯盟。至今，
[坦帕洋基仍然在佛州聯盟打球](../Page/坦帕洋基.md "wikilink")。坦帕灣區的其他城市，包含了[克利爾沃特](../Page/克利爾沃特_\(佛羅里達州\).md "wikilink")、[但尼丁及](../Page/达尼丁_\(佛罗里达州\).md "wikilink")[萊克蘭都擁有各自的球隊](../Page/莱克兰_\(佛罗里达州\).md "wikilink")，也在佛州聯盟有長久的歷史。此外，在新人級小聯盟[灣岸聯盟也有過幾個以前或是現存的球隊以坦帕灣區為主場](../Page/灣岸聯盟.md "wikilink")
。

聖彼德堡曾是短命的[Senior Professional Baseball
Association](../Page/Senior_Professional_Baseball_Association.md "wikilink")（1989年到1990年）中的[聖彼德堡鵜鶘的主場](../Page/聖彼德堡鵜鶘.md "wikilink")。這個聯盟以35歲（或以上）的前大聯盟球員為賣點，聖彼德堡鵜鶘隊也拿下了該聯盟唯一的一次冠軍。

由於此地長久以來的棒球傳統，加上逐漸增加的人口，坦帕灣區在1980年代及90年代曾有數次試圖設置大聯盟球隊卻未成功。[明尼蘇達雙城](../Page/明尼蘇達雙城.md "wikilink")、[奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")、芝加哥白襪、[德州遊騎兵](../Page/德州遊騎兵.md "wikilink")、[西雅圖水手在決定留在他們原主場之前](../Page/西雅圖水手.md "wikilink")，都曾考慮移入坦帕或聖彼德堡。[佛州日岸巨蛋](../Page/純品康納球場.md "wikilink")（現名純品康納球陽）就是為了吸引大聯盟球隊進駐而在1990年建於聖彼德堡。當大聯盟在1993年宣布新增兩支球隊時，很多人認為其中一支會位於聖彼德堡。但是最後的結果是[科羅拉多落磯以及](../Page/科羅拉多落磯.md "wikilink")[佛羅里達馬林魚](../Page/佛羅里達馬林魚.md "wikilink")。

1992年，舊金山巨人隊老闆[Bob
Lurie原則上同意把巨人隊賣給坦帕灣區的集團](../Page/Bob_Lurie.md "wikilink")，該集團也決定要把到手的球隊移到聖彼德堡。然後在11個小時後，大聯盟受到舊金山政府的壓力，取消這項交易，巨人隊也就被賣給舊金山區的集團，因此留在舊金山。

到了1995年5月9日，新增的大聯盟球隊終於有1支是以[Vince
Naimoli為首的坦帕灣區集團](../Page/Vince_Naimoli.md "wikilink")，另一支則是[亞利桑那響尾蛇](../Page/亞利桑那響尾蛇.md "wikilink")。

坦帕灣區總算有了大聯盟球隊，但是在聖彼德堡的球場卻需要升級了。1993年，球場改名為Thunderdome會成為曲棍球隊[坦帕灣閃電以及美式足球隊](../Page/坦帕灣閃電.md "wikilink")[Tampa
Bay
Storm的主場](../Page/Tampa_Bay_Storm.md "wikilink")。坦帕灣光芒隊誕生之後，球場命名權賣給[純品康納](../Page/纯果乐.md "wikilink")，並花費7000萬美元整修球場。

## 球隊歷史

### 1998年之前

坦帕灣魔鬼魚隊在1995年成立之後不久就開始建立其農場，先任命[Chuck
LaMar為球團資深副總裁及總經理](../Page/Chuck_LaMar.md "wikilink")。他們的第一場小聯盟比賽在1996年球季開始。1997年11月7日，[Larry
Rothschild被任命為該隊的第一位總教練](../Page/Larry_Rothschild.md "wikilink")。1997年11月18日的1997年球季大聯盟擴大選秀中，坦帕灣魔鬼魚隊取得35名球員。他們第1個選到的是原屬佛羅里達馬林魚的[Tony
Saunders](../Page/Tony_Saunders.md "wikilink")。此外
，坦帕灣魔鬼魚隊也選到後來的球星[巴比·阿布瑞尤](../Page/巴比·阿布瑞尤.md "wikilink")，並快速的把他交易到[費城費城人隊換來之後表現不太好的](../Page/費城費城人.md "wikilink")[Kevin
Stocker](../Page/Kevin_Stocker.md "wikilink")。在1998年球季之前，又取得明星球員 [Wade
Boggs](../Page/Wade_Boggs.md "wikilink")、[Fred
McGriff與](../Page/Fred_McGriff.md "wikilink") [Wilson
Alvarez](../Page/Wilson_Alvarez.md "wikilink")。

### 魔鬼魚時期

跟大部分新球隊一樣，魔鬼魚戰績慘不忍睹，一直處於聯盟墊底狀態；2006、07年，球團高層走投無路的情況下，開始著手更名計畫，希望能帶給球隊不同的運勢。

### 2007年：新希望

2007年開季魔鬼魚隊簽下了他們隊史第一位[日本籍球員](../Page/日本.md "wikilink")－[岩村明憲](../Page/岩村明憲.md "wikilink")。雖然這個球季戰績66勝96敗又再次墊底，但是不少未來的主力球員都在這年初試啼聲。例如左投[史考特·卡茲米爾](../Page/史考特·卡茲米爾.md "wikilink")，他在本季投出239次三振，拿下隊史第一個三振王；「神盾」[詹姆斯·席爾斯在生涯第一個完整球季也投出了](../Page/詹姆斯·席爾斯.md "wikilink")12勝8敗、防禦率3.85、184次奪三振的表現；[卡爾·克勞福近五年每季都能揮出](../Page/卡爾·克勞福.md "wikilink")177支以上的安打，並拿下四次盜壘王；季前用小聯盟合約簽來的[卡洛斯·佩尼亞整季擊出](../Page/卡洛斯·佩尼亞.md "wikilink")46發全壘打，創下隊史紀錄。

球隊於2007年11月上旬，正式更名為「Tampa Bay
Rays」，中譯為坦帕灣光芒隊，英文詞彙具有光芒和魟魚的意思，隊徽則改成光芒圖案。新的球衣正面圖案為光芒，側面的臂章則繡有傳統魟魚的造型，此乃代表正面圖案的涵義為主，側面圖案的涵義為副的新隊名。採訪大聯盟的華文媒體記者向坦帕灣光芒隊的公關部門尋求一個明確的中文譯名，獲得回答是「在英文裡，他們是不介意球迷怎麼解讀；但真的需要翻譯時，他們希望譯为往“光”的方向走」\[2\]\[3\]。

### 2008年灰姑娘傳奇

許多球評認為光芒队在2008年難逃分區墊底的命運，然而更名之后的光芒队不僅一帆风顺，戰績更在[洋基隊之上](../Page/洋基隊.md "wikilink")，最後更以97勝65敗拿下隊史第一個分區冠軍，也是首次晉級季後賽。

在分區系列賽光芒先以3勝1敗淘汰芝加哥白襪，接著又在美聯冠軍賽击败2007年[世界大賽冠军](../Page/世界大賽.md "wikilink")[波士顿红袜队](../Page/波士顿红袜队.md "wikilink")，一举夺得[美国联盟冠军](../Page/美国联盟.md "wikilink")，一年时间即完成从联盟最差球队到联盟冠军的“超级大跃进”（Super
Jump）。但是在[世界系列赛中以](../Page/世界系列赛.md "wikilink")1：4不敌[费城费城人](../Page/费城费城人.md "wikilink")，雖没有能成为新的世界冠军，然而此時的光芒隊已一躍成為超級球隊之一。

而2007年的選秀狀元[大衛·普萊斯在這年升上大聯盟後雖然在例行賽僅出賽](../Page/大衛·普萊斯.md "wikilink")5場，但在本季的季後賽卻扮演著相當重要的角色。

### 2009年－2010年

有了2008年成功的經驗以後，2009年球隊的目標除了繼續能夠進入季後賽，還要拿下隊史第一座世界大賽冠軍。由於[紐約洋基隊在](../Page/紐約洋基.md "wikilink")2008年底大肆補強，戰績優於前一年，使得光芒分區排名一直只在第三位。但球隊扔然保持著高度的競爭力，在分區和外卡並沒有落後太多，直到8月29日隊中的王牌投手[史考特·卡茲米爾被交易到](../Page/史考特·卡茲米爾.md "wikilink")[洛杉磯天使隊後](../Page/洛杉磯天使.md "wikilink")，球員才開始認為球隊放棄季後賽，而戰績變成勝少敗多，最後提前出局，無緣連續兩季進季後賽\[4\]。9月30日光芒在主場以5：3擊敗[巴爾的摩金鶯隊後](../Page/巴爾的摩金鶯.md "wikilink")，隊史首次達成連續兩年的勝率都在五成以上，這年的戰績結算為84勝78敗。

這年[本·佐布里斯特在獲得首個完整球季後就向世人展現他看似平凡卻又不俗的表現](../Page/本·佐布里斯特.md "wikilink")－打擊率.297、27轟、91分打點、17盜，並選到91次保送，更重要的是佐布里斯特是個可以守內外野的球員。[埃文·朗歌利亞也在這年擊出](../Page/埃文·朗歌利亞.md "wikilink")33轟、113打點，並且跑回100分。

[2010年明星賽球隊共有](../Page/2010年美國職棒大聯盟全明星賽.md "wikilink")[大衛·普萊斯](../Page/大衛·普萊斯.md "wikilink")、[拉斐爾·索利安諾](../Page/拉斐爾·索利安諾.md "wikilink")、[埃文·朗歌利亞及](../Page/埃文·朗歌利亞.md "wikilink")[卡爾·克勞福等](../Page/卡爾·克勞福.md "wikilink")4位選手獲選進入，而普萊斯更是成為隊史第1位在明星賽先發的投手。7月26日，[馬特·加爾薩在主場面對](../Page/馬特·加爾薩.md "wikilink")[底特律老虎時](../Page/底特律老虎.md "wikilink")，投出隊史的第1個[無安打比賽](../Page/無安打比賽.md "wikilink")，中間只有1位打者靠保送上壘\[5\]。在今年球季結束以後，球隊以96勝66敗拿下美國聯盟東區的冠軍，這是球隊3年裡面第2度拿下分區冠軍，也是美國聯盟戰績最好的球隊，在大聯盟30之球隊裡面，戰績只落後[費城費城人](../Page/費城費城人.md "wikilink")。普萊斯為球隊拿下19勝的成績，創下當時隊史單季投手最多勝的紀錄（2012年被普萊斯自己以20勝超越，2018年再被[布萊克·史涅爾以](../Page/布萊克·史涅爾.md "wikilink")21勝刷新）。在[分區賽裡面](../Page/2010年美國聯盟分區賽.md "wikilink")，光芒對手為[德州遊騎兵](../Page/德州遊騎兵.md "wikilink")，擁有主場優勢，但最後是以2勝3敗的成績被淘汰，其中3敗都是在主場，當中就有2敗是輸給遊騎兵隊的[克里夫·李](../Page/克里夫·李.md "wikilink")。

### 2011年

在2010年球季結束以後，光芒有陣容中有幾位主要的球員成為自由球員或被交易。在交易方面首先先以先發投手[馬特·加爾薩和芝加哥小熊交易](../Page/馬特·加爾薩.md "wikilink")5名小聯盟球員\[6\]；以游擊手[傑森·巴列特和聖地牙哥教士交易](../Page/傑森·巴列特.md "wikilink")4名小聯盟球員\[7\]。7名救援投手則成為自由球員。野手部分包括一壘手[卡洛斯·佩尼亞小熊簽下合約](../Page/卡洛斯·佩尼亞.md "wikilink")\[8\]；外野手[卡爾·克勞福和](../Page/卡爾·克勞福.md "wikilink")[波士頓紅襪簽下合約](../Page/波士頓紅襪.md "wikilink")\[9\]。

2011年2月，光芒和紅襪前2位球員[曼尼·拉米瑞茲和](../Page/曼尼·拉米瑞茲.md "wikilink")[強尼·戴蒙簽下](../Page/強尼·戴蒙.md "wikilink")1年的合約\[10\]。4月18日，拉米瑞茲因禁藥問題，而突然宣佈退休決定\[11\]。在此種種不利條件下，光芒開季0勝6敗，雖然表現不理想，但在4月結束時，他們還是以15勝12敗獲得五成以上的勝率，和[美國聯盟東區領先的](../Page/美國聯盟東區.md "wikilink")[紐約洋基只有](../Page/紐約洋基.md "wikilink")1.5場的勝差\[12\]。該年球季的最後1場比賽，在外卡的競爭上，光芒和紅襪戰績相同，將由最後一場比賽決定誰擁有外卡資格。最後光芒在延長賽12局時，由朗歌利亞擊出再見全壘打並以8：7擊敗洋基。這場比賽光芒曾經以0：7落後，靠著朗歌利亞的雙響炮以及其他球員的努力，最後演出大逆轉。在獲勝前3分鐘，[巴爾的摩金鶯則擊敗紅襪](../Page/巴爾的摩金鶯.md "wikilink")，讓光芒以91勝71敗、1場的勝差領先紅襪，獲得美聯外卡資格。在9月開始前，光芒在外卡還落後紅襪9場勝差，最後演出大逆轉，成為大聯盟歷史上，在9月落後最多的勝場差還能進入季後賽的球隊。

在[美國聯盟分區賽](../Page/2011年美國聯盟分區賽.md "wikilink")，光芒的對手為[德州遊騎兵](../Page/德州遊騎兵.md "wikilink")，雖然順利的贏得第1場比賽，但輸掉之後的3場比賽，其中兩場是一分敗，最後無緣晉級美聯冠軍賽。失望的結局也讓這奇蹟的一個球季劃上了句點。\[13\]

而這季光芒隊的先發輪值表現相當搶眼。[傑瑞米·海利克森靠著他慢吞吞的變速球混淆打者](../Page/傑瑞米·海利克森.md "wikilink")，13勝10敗、2.95的防禦率讓他獲得美聯新人王；「神盾」席爾斯在這年投出堪稱他生涯最佳的表現－16勝12敗、防禦率2.82，在249.局內投出225次三振。

### 2012年：拿了90勝，卻進不了外卡

2012年的光芒隊戰績依舊穩定，但同屬美聯東區的金鶯隊卻在此竄起，明星賽過後雙方的戰績依然緊咬不放。而光芒隊在季中遇到一些問題，那就是陣中主砲[埃文·朗歌利亞受傷而無法穩定出賽](../Page/埃文·朗歌利亞.md "wikilink")。球季結束後，金鶯隊以3場的勝差得到東區第二名，並進入外卡賽，整季有90勝的光芒只好在這季鎩羽而歸。

光芒隊的先發投手[大衛·普萊斯和老虎隊的](../Page/大衛·普萊斯.md "wikilink")[賈斯丁·韋蘭德在季後爭奪賽揚獎人選](../Page/賈斯丁·韋蘭德.md "wikilink")。雖然他的三振數和投球局數都遠不及韋蘭德，但普萊斯卻以較多的勝場數（20勝，是當時光芒隊史紀錄）和較低的防禦率贏得美聯賽揚獎，這是光芒隊第一位拿到賽揚獎的投手。而救援投手[費南多·羅尼除了整季拿下](../Page/費南多·羅尼.md "wikilink")48次救援成功，出賽76場防禦率只有0.60，是大聯盟史上所有主投50局以上的投手中最低的，也獲得該年的最佳進步獎。

球季結束後，球團也出現大動作的交易。而其中影響最大的就是將在隊中待了7年的主力先發[詹姆斯·席爾斯連同Wade](../Page/詹姆斯·席爾斯.md "wikilink")
Davis交易給皇家隊，換得[威爾·邁爾斯](../Page/威爾·邁爾斯.md "wikilink")、Jake
Odorizzi、[迈克·蒙哥马利](../Page/迈克·蒙哥马利_\(棒球运动\).md "wikilink")、Patrick
Leonard。而事後證明這個交易是值回票價的，威爾·邁爾斯在2013年球季為球隊擊出98支安打、13轟、53分打點和.293的打擊率，得到那年的新人王。

野手方面則失去了擊出28支全壘打的[B·J·厄普顿](../Page/梅爾文·厄普顿.md "wikilink")（2015年時改名為梅尔文·厄普顿Melvin
Upton,
Jr.）。雖然他有長打實力，但被三振率過高，打擊率也較低。當時面對可能無法留下，厄普顿曾一度表態想要續留光芒隊，還為此流淚。但光芒無法負擔他的薪資，最後厄普顿只好投入自由球員市場，加入勇士隊。

### 2013年

2013年季中光芒隊遭遇了一些小麻煩，像是[亞力士·柯布在](../Page/亞力士·柯布.md "wikilink")6月因為頭部強襲球受傷、傑瑞米·海利克森因手傷整季報銷、費南多·羅尼因為參加經典賽導致手感下滑頻頻放火，加上同區的紅襪隊在本季再次奮起，光芒的戰績雖然穩定但遲遲無法有進一步的突破。不過好在[麥特·摩爾在本季投出](../Page/麥特·摩爾.md "wikilink")17勝4敗與3.29的防禦率，他的挺身而出有效緩解了光芒輪值的傷兵問題。

例行賽結束後，光芒隊和遊騎兵隊因為戰績都是91勝71敗，在各自所屬的分區均已被封王的情況下，兩隊還要打一場加賽。光芒隊派出[大衛·普萊斯](../Page/大衛·普萊斯.md "wikilink")。在這場關鍵戰役中，普萊斯以118球完投9局，投出4次三振僅1四壞，雖然被遊騎兵打7支安打失2分，但還是摘下該季第10勝，以生涯第8場、該季第4場完投的表現，加上[埃文·朗歌利亞單場擊出](../Page/埃文·朗歌利亞.md "wikilink")3安打（包括3局上轟出寫下例行賽最後1戰新紀錄的2分砲）幫助光芒在這場加賽中以5：2擊敗德州遊騎兵，搶下美聯第2張外卡，進軍季後賽，這也是光芒隊6年內第4度闖進季後賽。

接下來光芒與克李夫蘭印地安人進行外卡驟死賽。6月15日被平飛球擊中頭部的光芒先發投手亞力士·柯布在外卡戰中主投6.2局送出5次三振，儘管被擊出多達8支安打，但關鍵時刻總是能化險為夷，沒有丟掉任何分數。其中3局上Delmon
Young開局就轟出陽春砲先馳得點，4局上[詹姆斯·隆尼與](../Page/詹姆斯·隆尼.md "wikilink")[埃文·朗歌利亞在](../Page/埃文·朗歌利亞.md "wikilink")1出局後連敲安打，2出局後，Desmond
Jennings適時擊出左外野方向二壘安打，一棒為光芒再添2分領先。最後接手的Joel Peralta、Jake
McGee及[費南多·羅尼聯手投出完封](../Page/費南多·羅尼.md "wikilink")，柯布生涯首度於季後賽出賽，就在這場關鍵的1戰定生死比賽中幫助光芒晉級。

光芒在外卡驟死賽中獲勝之後，晉級美聯分區系列賽，對上美聯戰績第一的波士頓紅襪。最終以1勝3敗遭到淘汰，這也是光芒隊近四個球季有三個球季都在美聯分區賽落敗。

### 2014年

光芒在這個球季開始黯淡，由於一連串連敗導致戰績不佳，球團決定展開重建。在731大限前他們將王牌投手大衛·普萊斯交易到[底特律老虎](../Page/底特律老虎.md "wikilink")，換得幾位農場新秀。這個球季光芒的戰績只有77勝85敗，在分區排名第四，他們自2008年起連續六年保持五成以上勝率的紀錄也隨之終止。

球季結束後光芒在11月15日拍賣了他們的先發投手傑瑞米·海利克森，向響尾蛇換回了Justin Williams與Andrew
Velazquez。12月時以一年短約簽下明星游擊手Asdrubal Cabrera，同月再將去年的新人王威爾·邁爾斯與捕手Ryan
Hanigan交易到教士隊，換來Jake Bauers、Burch Smith與René Rivera。

### 2015年：梅登時代結束

1月時光芒決定進一步重建，把本·佐布里斯特與Yunel Escobar交易到運動家，換來了Boog Powell、Daniel
Robertson與John Jaso。

總教練[喬·梅登在發覺光芒已無心衝刺季後賽後決定跳脫合約](../Page/喬·梅登.md "wikilink")，改前往芝加哥執教小熊隊。於是新球季改由[凱文·凱什總教練一職](../Page/凱文·凱什.md "wikilink")。在凱什執教的第一年光芒繳出80勝82敗，在美聯東區排名第四坐收。

[克理斯·亞契在本季](../Page/克理斯·亞契.md "wikilink")212局的投球內送出252次三振，創下隊史紀錄。終結者Brad
Boxberger投出41次救援成功，是美聯當中最多的。而中外野手Kevin Kiermaier則榮獲外野金手套獎。

11月時光芒將Nate Karns與C. J. Riefenhauser交易到水手隊，換來了Brad Miller、Danny
Farquhar與Logan Morrison。

### 2016年

1月時光芒將Jake McGee與Germán
Márquez交易到洛杉磯，換回外野手[柯瑞·迪克森與Kevin](../Page/柯瑞·迪克森.md "wikilink")
Padlo，再與Steve Pearce簽下一年的合約。

這個球季光芒隊的戰績依舊還是很不理想，68勝94敗讓他們繼2007年後又再次於分區墊底。季中光芒再次展開重建，他們將[麥特·摩爾交易到舊金山巨人](../Page/麥特·摩爾.md "wikilink")，換回Matt
Duffy、Lucius Fox與Michael Santos，又把Brandon Guyer交易到印地安人，換來Nathan
Lukes和Jhonleider
Salinas。在普萊斯離隊後，一直是光芒當家王牌的亞契在今年吞下了19敗，是美聯敗投最多的投手。而先發轉後援的Alex
Colome卻大放異彩，整季防禦率1.91，有37次救援成功，成為聯盟最優秀的終結者之一。埃文·朗歌利亞在這個球季則是擊出生涯新高的36支全壘打。

### 2017年：朗歌利亞離隊

2016年球季結束後光芒隊進行的最大補強就是以兩年合約簽下前國民隊的強打Wilson Ramos。隔年1月他們把二壘手Logan
Forsythe交易到道奇，換來潛力投手José De León。先發投手Drew Smyly則被交易到水手，換回Mallex Smith,
Ryan Yarbrough與Carlos Vargas；2月時他們用小聯盟合約簽下前金鶯隊的後援投手Tommy
Hunter與內野手Rickie Weeks Jr.。

靠著上半季[柯瑞·迪克森](../Page/柯瑞·迪克森.md "wikilink")、Logan Morrison、以及Steven
Souza
Jr.的好表現，光芒一度逼近季後賽，但沒想到在季中大肆補強後，戰績卻一落千丈。打線方面，雖然季中透過交易獲得馬林魚內野手[亞丹尼·哈查瓦瑞亞](../Page/亞丹尼·哈查瓦瑞亞.md "wikilink")、大都會強打[盧卡斯·杜達與簽下Trevor](../Page/盧卡斯·杜達.md "wikilink")
Plouffe，但始終拉不起來的串聯讓光芒打線成為盲砲軍團，整季團隊一共吞下了1538次三振。投手方面，終結者Colome頻頻失手，雖然拿下了聯盟最多的47次救援成功，但其他數據都不太好看。

而2008年的選秀狀元[提姆·貝克漢在季中被交易到金鶯隊](../Page/提姆·貝克漢.md "wikilink")，換回Tobias
Myers。

賽季結束後，決定年輕化的光芒先在11月把2015年的美聯救援王Brad Boxberger交易到響尾蛇，換取Curtis
Taylor；12月時又將當家球星『光芒先生』[埃文·朗歌利亞交易至](../Page/埃文·朗歌利亞.md "wikilink")[舊金山巨人](../Page/舊金山巨人.md "wikilink")，換回Denard
Span、Matt Krook、Stephen Woods與Christian Arroyo。

### 2018年：嘗試與驚奇

這年2月是光芒隊展開大筆交易的一個月份－陣中的[柯瑞·迪克森被交易到海盜隊](../Page/柯瑞·迪克森.md "wikilink")，換回Daniel
Hudson、Tristan Gray和一些現金；主力先發投手Jake Odorizzi交易到雙城隊換來Jermaine
Palacios；利用三方交易將重砲Steven Souza Jr.送到響尾蛇，迎回Anthony Banda、Nick
Solak、Colin Poche與Sam McWilliams；以Luis Rengifo為代價，向天使隊換來C. J.
Cron。3月時又簽下Carlos Gómez補強外野。

由於先發投手群傷兵為患，總教練[凱文·凱許決定採用](../Page/凱文·凱許.md "wikilink")「假先發」策略，以牛棚投手先行來掩護擔任長中繼的真正先發投手。這個奇招引發熱烈討論，有人覺得這步棋下的很妙，但也有人批判這是不按牌理出牌。像是原本為後援投手的Sergio
Romo，在5月19日的比賽中就創下連續兩天都先發一局的紀錄，這也是大聯盟近40年來第一位連續兩場先發投球的投手。

而上半季的光芒走的跌跌撞撞，時而連勝時而連敗，明星賽前戰績雖然一直維持在接近五成，不過管理層記取去年的教訓，選擇兜售陣中主力換取新秀及實用的球員－例如相伴多年的王牌投手[克理斯·亞契被交易至海盜](../Page/克理斯·亞契.md "wikilink")，換回Tyler
Glasnow、外野手Austin Meadows與Shane Baz；終結者Álex Colomé與Denard
Span被遠送至西雅圖水手，換取Andrew Moore與Tommy
Romero；表現火熱的Wilson Ramos被交易至費城人隊；投手Matt
Andriese被交易到響尾蛇，換回Michael Pérez與Brian Shaffer；野手Brad
Miller被交易到釀酒人，換回[韓國籍強打崔志萬](../Page/韓國.md "wikilink")；Roel
Ramirez、Genesis Cabrera與Justin Williams被交易到紅雀，換回長打及腳程兼具的Tommy Pham。

然而出乎意料的是，明星賽後光芒戰績一飛沖天，投手群彷彿適應了假先發機制，展現絕佳的壓制力，打線也開始甦醒，交易來的即戰力以及自家培養的新秀都繳出優異成績，成為季中交易的大贏家；一號先發Blake
Snell自8月起連兩個月獲得單月最佳球員，並在9月24日奪得單季21勝，刷新2012年的大衛·普萊斯，創下隊史紀錄。

然而，由於外卡前兩名的[奧克蘭運動家以及](../Page/奧克蘭運動家.md "wikilink")[紐約洋基也表現不俗](../Page/紐約洋基.md "wikilink")，再加上明星賽前光芒就已經落後太多，即使九月份繳出了19勝9敗的絕佳戰績，距離外卡仍有4場勝差。最終光芒在2018球季以90勝72敗的成績坐收，在分區排名第三，這是大聯盟史上第三次有90勝球隊進不了季後賽，同時也是隊史繼2012年後的第二次。

雖然沒有進入外卡，但這些拼勁十足的年輕選手讓球迷看到了希望，也證明了假先發的可能性。本季在假先發調度下一共打出了44勝34敗，對於光芒來說，這是個很成功的球季。

先發投手Blake
Snell本季投出21勝、防禦率1.89都是隊史及美聯最佳，並且還送出221次三振，儼然成為光芒繼普萊斯與亞契之後的新一任王牌投手。因此他在這年得到賽揚獎，是隊史第二位有此殊榮的投手，不過180.2局的投球局數是歷年來所有賽揚獎得主中最少的。而左投手Ryan
Yarbrough在本季做為接替假先發後的「真先發」也表現不俗，他38場出賽中僅有6場是真的擔任先發投手，就斬獲了16勝。

## 球隊沿革

  - 1995年，成立[坦帕灣魔鬼魚](../Page/坦帕灣魔鬼魚.md "wikilink")（Tampa Bay Devil Rays）
  - 1998年，成為[美國聯盟東區擴增的球隊](../Page/美國聯盟.md "wikilink")，正式出賽
  - 2008年，改名**坦帕灣光芒**　（Tampa Bay Rays）

## 歷年戰績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>隊名</p></th>
<th><p>勝敗</p></th>
<th><p>勝率</p></th>
<th><p>名次</p></th>
<th><p>季後賽成績</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>63-99</p></td>
<td><p>.389</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>69-93</p></td>
<td><p>.426</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>69-92</p></td>
<td><p>.429</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>62-100</p></td>
<td><p>.383</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>55-106</p></td>
<td><p>.342</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>63-99</p></td>
<td><p>.389</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>70-91</p></td>
<td><p>.435</p></td>
<td><p>美聯東區第四</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>67-95</p></td>
<td><p>.414</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>61-101</p></td>
<td><p>.377</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/坦帕灣魔鬼魚.md" title="wikilink">坦帕灣魔鬼魚</a></p></td>
<td><p>66-96</p></td>
<td><p>.407</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>97-65</p></td>
<td><p>.599</p></td>
<td><p><strong>美聯東區第一</strong></p></td>
<td><p>美聯分區賽以3勝1負擊敗<a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a><br />
美聯冠軍賽以4勝3負擊敗<a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a><br />
<a href="../Page/世界大賽.md" title="wikilink">世界大賽以</a>1勝4負敗給<a href="../Page/費城費城人.md" title="wikilink">費城費城人</a></p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>84-78</p></td>
<td><p>.519</p></td>
<td><p>美聯東區第三</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>96-66</p></td>
<td><p>.593</p></td>
<td><p><strong>美聯東區第一</strong></p></td>
<td><p>美聯分區賽以2勝3負敗給<a href="../Page/德州遊騎兵.md" title="wikilink">德州遊騎兵</a></p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>91-71</p></td>
<td><p>.562</p></td>
<td><p>美聯東區第二#</p></td>
<td><p>美聯分區賽以1勝3負敗給<a href="../Page/德州遊騎兵.md" title="wikilink">德州遊騎兵</a></p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>90-72</p></td>
<td><p>.556</p></td>
<td><p>美聯東區第三</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>91-71</p></td>
<td><p>.562</p></td>
<td><p>美聯東區第二#</p></td>
<td><p>外卡季後賽擊敗<a href="../Page/克里夫蘭印地安人.md" title="wikilink">克里夫蘭印地安人</a>（比數4:0）<br />
美聯分區賽以1勝3負敗給<a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>77-85</p></td>
<td><p>.475</p></td>
<td><p>美聯東區第四</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>80-82</p></td>
<td><p>.494</p></td>
<td><p>美聯東區第四</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>68-94</p></td>
<td><p>.420</p></td>
<td><p>美聯東區第五</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>80-82</p></td>
<td><p>.494</p></td>
<td><p>美聯東區第三</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td><p>90-72</p></td>
<td><p>.556</p></td>
<td><p>美聯東區第三</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>總計</strong></p></td>
<td><p><strong>21 季</strong></p></td>
<td><p><strong>1589-1810</strong></p></td>
<td><p><strong>.467</strong></p></td>
<td></td>
<td><p>季後賽戰績13-17 （.433）<br />
1次美聯冠軍</p></td>
</tr>
</tbody>
</table>

\#以外卡資格進入季後賽

## 登錄名單

## 退休背號

  - 12  （2000年4月7日）
  - 42 [傑基·羅賓森](../Page/傑基·羅賓森.md "wikilink") （1997年4月15日）
  - 66  （2015年4月6日）

## 附屬小聯盟球隊

  - **AAA**：[杜罕公牛](../Page/杜罕公牛.md "wikilink") (Durham Bulls),
    [国际联盟](../Page/国际联盟.md "wikilink") (International League)
  - **AA**：[蒙哥马里饼干](../Page/蒙哥马里饼干.md "wikilink") (Montgomery Biscuits),
    [南方联盟](../Page/南方联盟.md "wikilink") (Southern League)
  - **高级A**：[维洛海滩魔鬼魚](../Page/维洛海滩魔鬼魚.md "wikilink") (Vero Beach Devil
    Rays), [佛羅里達联盟](../Page/佛羅里達联盟.md "wikilink") (Florida State League)
    & [舊金山魔鬼魚](../Page/舊金山魔鬼魚.md "wikilink") (San Francisco Devil Rays),
    [加利福尼亞联盟](../Page/加利福尼亞联盟.md "wikilink") (California League)
  - **A**：[哥伦布鲶鱼](../Page/哥伦布鲶鱼.md "wikilink") (Columbus Catfish),
    [南大西洋联盟](../Page/南大西洋联盟.md "wikilink") (South Atlantic
    League)
  - **短A**：[哈德森谷叛徒](../Page/哈德森谷叛徒.md "wikilink") (Hudson Valley
    Renegades), [纽约─宾州联盟](../Page/纽约─宾州联盟.md "wikilink") (New York-Penn
    League)
  - **新秀**：[普林斯顿蝠魟](../Page/普林斯顿蝠魟.md "wikilink") (Princeton Devil
    Rays), [阿帕拉契联盟](../Page/阿帕拉契联盟.md "wikilink") (Appalachian League)

## 參考資料

## 外部連結

  - [MLB
    坦帕灣光芒](https://web.archive.org/web/20130417015602/http://tw.sports.yahoo.com/mlb/mlb_team_index/teamid/30/)

  - [坦帕灣光芒隊官方網站](http://tampabay.devilrays.mlb.com/NASApp/mlb/tb/homepage/tb_homepage.jsp)

  - [Rays of Light](http://www.futurerays.com/)

[坦帕灣光芒](../Category/坦帕灣光芒.md "wikilink")
[T](../Category/美國職棒大聯盟球隊.md "wikilink")
[T](../Category/佛羅里達州體育.md "wikilink")
[Category:1998年建立的體育俱樂部](../Category/1998年建立的體育俱樂部.md "wikilink")
[Category:1998年佛羅里達州建立](../Category/1998年佛羅里達州建立.md "wikilink")

1.  [RAYS
    HISTORY](http://tampabay.devilrays.mlb.com/NASApp/mlb/tb/history/tb_history_tampabay.jsp)
2.  [球隊正名
    魔鬼魚也可放光芒](http://mag.udn.com/mag/sports/storypage.jsp?f_ART_ID=119972)
3.  [「光芒」這隊名是怎麼來的？](http://jtrain-tpmbny.blogspot.com/2008/04/blog-post_08.html)
4.  [Scott Kazmir: Leaving a legacy with the Tampa Bay
    Rays](http://www.tampabay.com/sports/baseball/rays/scott-kazmir-leaving-a-legacy-with-the-tampa-bay-rays/1032053)

5.
6.
7.
8.
9.
10.
11.
12.
13. Kernan, Kevin (October 5, 2011). [Rays owner says team could move
    anywhere](http://www.nypost.com/p/sports/more_sports/owner_can_ee_ray_of_hope_n1RRlSbhIfvTf2i3KMINjP?CMP=OTC-rss&FEEDNAME=).
    *New York Post*. Retrieved October 5, 2011.