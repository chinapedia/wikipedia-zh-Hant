**埃利斯縣**（**Ellis County,
Oklahoma**）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州西部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，西鄰[德克薩斯州](../Page/德克薩斯州.md "wikilink")。面積3,190平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口4,075人。縣治[阿內特](../Page/阿內特_\(奧克拉荷馬州\).md "wikilink")
（Arnett）。

縣政府成立於1907年11月16日。縣名紀念制憲會議副主席阿伯特·H·埃利斯（Albert H. Ellis）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[E](../Category/俄克拉何马州行政区划.md "wikilink")

1.  <http://www.odl.state.ok.us/almanac/2005/counties/ellis.pdf>