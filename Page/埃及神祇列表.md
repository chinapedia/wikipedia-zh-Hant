埃及古物學者現今大多支持[弗林德斯·皮特里爵士的論點](../Page/弗林德斯·皮特里.md "wikilink")：埃及宗教是[多神論的](../Page/多神論.md "wikilink")。然而，他同時期的對手[華利斯·巴奇](../Page/華利斯·巴奇.md "wikilink")，所持觀點則是認為埃及宗教起初是[一神論](../Page/一神論.md "wikilink")，眾神皆為神祇[拉的延伸](../Page/拉_\(神祇\).md "wikilink")，類似基督教的[三位一體及印度教的](../Page/三位一體.md "wikilink")[三一論](../Page/三一論.md "wikilink")。[古埃及語的女神一詞是](../Page/古埃及語.md "wikilink")
**（**；**、**），而神則是 **（**；亦譯
**、**）。早期的[象形文字中](../Page/象形文字.md "wikilink")，女神是以旗狀物代表，或再加上一隻[埃及眼鏡蛇](../Page/埃及眼鏡蛇.md "wikilink")，從旗桿底部豎起。後期的象形文字（R8）中，這些神祇名詞則是描繪以旗狀物，旁邊跟著相對的性別符號。

## 古埃及神祇列表

[古埃及人的信仰属](../Page/古埃及.md "wikilink")[多神教类](../Page/多神教.md "wikilink")，[神祇多以动物作为其象徵](../Page/神祇.md "wikilink")，其形象多为人身动物头。

  - [九柱神](../Page/九柱神.md "wikilink")：[赫利歐波利斯](../Page/赫里奥波里斯.md "wikilink")[埃及神话中的九位主要神祇](../Page/埃及神话.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>圖像</p></th>
<th><p>神祇</p></th>
<th><p>簡介</p></th>
<th><p>圖像</p></th>
<th><p>神祇</p></th>
<th><p>簡介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Re-Horakhty.svg" title="fig:Re-Horakhty.svg">Re-Horakhty.svg</a></p></td>
<td><p><a href="../Page/拉_(埃及神祇).md" title="wikilink">拉</a> (<a href="../Page/亞圖姆.md" title="wikilink">亞圖姆</a>)</p></td>
<td><p>主神，<a href="../Page/太阳神.md" title="wikilink">太陽神</a>。其形象與<a href="../Page/阿蒙.md" title="wikilink">阿蒙结合在一起</a>。 早晨、中午、傍晚，分別有不同的名字； 黎明破曉的早晨稱做「<a href="../Page/凱布利.md" title="wikilink">凱布利</a>」，</p>
<p>蔚藍無比的中午稱做「瑞」或「<a href="../Page/拉_(埃及神祇).md" title="wikilink">拉</a>」， 彩霞滿天的傍晚稱做「<a href="../Page/亞圖姆.md" title="wikilink">亞圖姆</a>」。</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shu.svg" title="fig:Shu.svg">Shu.svg</a></p></td>
<td><p><a href="../Page/舒_(埃及神祇).md" title="wikilink">舒</a></p></td>
<td><p>空氣之神。</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tefnut.png" title="fig:Tefnut.png">Tefnut.png</a></p></td>
<td><p><a href="../Page/泰芙努特.md" title="wikilink">泰芙努特</a><br />
</p></td>
<td><p>濕氣女神、雨神。</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nut.svg" title="fig:Nut.svg">Nut.svg</a></p></td>
<td><p><a href="../Page/努特_(埃及神祇).md" title="wikilink">努特</a></p></td>
<td><p><a href="../Page/天空.md" title="wikilink">天空女神</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nepthys.svg" title="fig:Nepthys.svg">Nepthys.svg</a></p></td>
<td><p><a href="../Page/奈芙蒂斯.md" title="wikilink">奈芙蒂斯</a><br />
</p></td>
<td><p>死者的守護神，四位守護死者的女神之一。</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Standing_Osiris_edit1.svg" title="fig:Standing_Osiris_edit1.svg">Standing_Osiris_edit1.svg</a></p></td>
<td><p><a href="../Page/欧西里斯.md" title="wikilink">歐西里斯</a></p></td>
<td><p>冥神、<a href="../Page/農業.md" title="wikilink">農業豐饒之神</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Isis.svg" title="fig:Isis.svg">Isis.svg</a></p></td>
<td><p><a href="../Page/伊西斯.md" title="wikilink">伊西斯</a></p></td>
<td><p>死者的守護神，四位守護死者的女神之一，<a href="../Page/生育.md" title="wikilink">生育之神</a>、<a href="../Page/魔法.md" title="wikilink">魔法女神</a>， 代表<a href="../Page/天狼星.md" title="wikilink">天狼星</a>，能預知<a href="../Page/尼罗河.md" title="wikilink">尼羅河氾濫時間</a>。</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Set.svg" title="fig:Set.svg">Set.svg</a></p></td>
<td><p><a href="../Page/赛特.md" title="wikilink">赛特</a></p></td>
<td><p><a href="../Page/干旱.md" title="wikilink">干旱</a>、<a href="../Page/戰爭.md" title="wikilink">戰爭之神</a>、<a href="../Page/沙漠.md" title="wikilink">沙漠之神</a>、<a href="../Page/混沌.md" title="wikilink">混亂之神</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Geb.svg" title="fig:Geb.svg">Geb.svg</a></p></td>
<td><p><a href="../Page/盖布.md" title="wikilink">盖布</a></p></td>
<td><p><a href="../Page/大地.md" title="wikilink">大地男神</a>。</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 其他主要的神：
      - [阿蒙](../Page/阿蒙.md "wikilink")（Ammon）：[底比斯主神](../Page/底比斯_\(埃及\).md "wikilink")。原為赫爾莫波利斯供奉的[八元神中象徵](../Page/八元神.md "wikilink")「隱蔽」的神。
      - [凱布利](../Page/凱布利.md "wikilink")（Khepri）：[聖甲蟲](../Page/聖甲蟲.md "wikilink")，早晨的太陽神。
      - [阿頓](../Page/阿頓.md "wikilink")（Aton）：正午之太陽神。形象為伸出數隻手的太陽圓盤。
      - [亞圖姆](../Page/亞圖姆.md "wikilink")（Atum）：暮之太陽神，亦是[赫利奧波利斯的主神](../Page/赫里奥波里斯.md "wikilink")，[拉的别名](../Page/拉_\(神祇\).md "wikilink")。
      - [荷鲁斯](../Page/荷鲁斯.md "wikilink")（Horus）：[法老的守護神](../Page/法老.md "wikilink")、天空之神，王權的象征。外形為鷹頭人身。
      - [阿努比斯](../Page/阿努比斯.md "wikilink")（Anubis）：[死神](../Page/死神.md "wikilink")、製作[木乃伊的神](../Page/木乃伊.md "wikilink")。外形為胡狼頭人身。
      - [姆特](../Page/姆特.md "wikilink")（Mut；Golden
        Dawn，Auramooth）：[阿蒙之妻](../Page/阿蒙.md "wikilink")，司掌[战争](../Page/战争.md "wikilink")。
      - [孔斯](../Page/孔斯.md "wikilink")（Khons/Chons）：[阿蒙與](../Page/阿蒙.md "wikilink")[姆特之子](../Page/姆特.md "wikilink")，[月神](../Page/月神.md "wikilink")，亦司掌[醫藥](../Page/医药.md "wikilink")。
      - [哈索爾](../Page/哈索爾.md "wikilink")（Hathor/Het-Heru，Het-Hert）：[荷鲁斯之妻](../Page/荷鲁斯.md "wikilink")，司[愛情](../Page/爱情.md "wikilink")、[音樂](../Page/音乐.md "wikilink")、歡樂及豐饒。
      - [普塔](../Page/普塔.md "wikilink")（Ptah）：創造之神，[孟菲斯主神](../Page/孟菲斯_\(埃及\).md "wikilink")，亦是[工匠的守護神](../Page/手工藝人.md "wikilink")。
      - [托特](../Page/托特.md "wikilink")（Thoth）：智慧之神。外型為朱鷺頭人身或狒狒。
      - [瑪特](../Page/玛亚特.md "wikilink")（Maat）：[正義及秩序之神](../Page/正義.md "wikilink")。

<!-- end list -->

  - 動物神：
      - [阿佩普](../Page/阿佩普.md "wikilink")（Apep）：代表破壞、混亂的蛇。
      - [芭絲特](../Page/芭絲特.md "wikilink")（Bast/Bastet）：猫之女神，掌管家庭和樂。
      - [奈赫貝特](../Page/奈赫贝特.md "wikilink")（Nekhbet）：禿鷹神，[上埃及的象徵及守護神](../Page/上下埃及.md "wikilink")。
      - [瓦吉特](../Page/瓦吉特.md "wikilink")（Wadjet）：蛇神，[下埃及的象徵及守護神](../Page/上下埃及.md "wikilink")。
      - [海奎特](../Page/海奎特.md "wikilink")（Heqet）：蛙女神，生命女神，為新生的胎兒注入生命。
      - [蒙圖](../Page/蒙圖.md "wikilink")（Monthu/Mont，Menthu）：司戰爭。外形為鷹首人身或公牛頭人身。
      - [庫努牡](../Page/庫努牡.md "wikilink")（Khnum）：公羊神，以黏土創造人類以及各種動物。
      - [塞赫麥特](../Page/塞赫麦特.md "wikilink")（Sekhmet）：母獅神，[普塔之妻](../Page/卜塔.md "wikilink")，掌管[戰爭](../Page/战争.md "wikilink")、[疾病及](../Page/疾病.md "wikilink")[惡靈](../Page/邪靈.md "wikilink")。
      - [塞爾凱特](../Page/塞爾凱特.md "wikilink")（Serket/Selket）：蠍子女神，與醫術有所關聯，四位守護死者的女神之一。
      - [索贝克](../Page/索貝克.md "wikilink")（Sobek）：鱷魚神，[戰爭之神](../Page/战争.md "wikilink")，王權的守護神。
      - [麥里特塞蓋爾](../Page/麦里特塞盖尔.md "wikilink")（Meretseger）：眼鏡蛇女神，陵墓的守護神。
      - [阿匹斯](../Page/阿匹斯.md "wikilink")（Apis）：司豐饒及生產之神，為[普塔或](../Page/卜塔.md "wikilink")[歐西里斯的化身](../Page/欧西里斯.md "wikilink")，外形為公牛。
      - [瑪弗德特](../Page/瑪弗德特.md "wikilink")

<!-- end list -->

  - 其他神：
      - [努恩](../Page/努恩_\(古埃及\).md "wikilink")（Nun/Nu）：原初之水。
      - [哈皮](../Page/哈皮.md "wikilink")（Hapi）：[尼羅河神](../Page/尼罗河.md "wikilink")，有兩個身體以及象徵豐饒的大乳房及大肚子。
      - [阿努凱特](../Page/阿努凯特.md "wikilink")（Anuket）：[尼羅河神](../Page/尼罗河.md "wikilink")。
      - [貝斯](../Page/贝斯_\(埃及神祇\).md "wikilink")（Bes）：[音樂神](../Page/音乐.md "wikilink")、守護神，神格複雜。
      - [哈爾波克拉特斯](../Page/哈尔波克拉特斯.md "wikilink")（Harpocrates/Hor-pa-kraat；Golden
        Dawn，Hoor-par-kraat）：孩提時荷鲁斯的稱呼。
      - [荷鲁斯的四個兒子](../Page/荷鲁斯的四個兒子.md "wikilink")（Four Sons of
        Horus）：冥王身体以及卡諾卜罐的守护者。
          - [艾姆謝特](../Page/艾姆谢特.md "wikilink")（Imset，Amset，Mesta）：荷鲁斯四子之一，死者[肝臟的保護者](../Page/肝臟.md "wikilink")。
          - [哈碧](../Page/哈碧.md "wikilink")（Hapi，Golden
            Dawn，Ahephi）：荷鲁斯四子之一，死者[肺的保护者](../Page/肺.md "wikilink")。
          - [多姆泰夫](../Page/多姆泰夫.md "wikilink")（Duamutef，Tuamutef；Golden
            Dawn，Thmoomathph）：荷鲁斯四子之一，死者[胃的保护者](../Page/胃.md "wikilink")。
          - [凱布山納夫](../Page/凯布山纳夫.md "wikilink")（Qebshenuf，Kebechsenef，Kebehsenuf，Qebehsenuf）：荷鲁斯四子之一，死者[腸的保護者](../Page/腸.md "wikilink")。
      - [印和闐](../Page/印何闐.md "wikilink")（Imhotep/Imouthis）：[醫藥及](../Page/藥品.md "wikilink")[建築的守護神](../Page/建筑.md "wikilink")，設計了[第一座金字塔](../Page/左塞爾金字塔.md "wikilink")。
      - [梅傑德](../Page/梅傑德.md "wikilink")（Medjed）：記載在[死者之書中的小神](../Page/死者之書.md "wikilink")。
      - [敏](../Page/敏.md "wikilink")（Min/Menu，Amsu）：旅行者的守護神及精液與[陽具之神](../Page/陽具.md "wikilink")，亦司掌[生產及收穫](../Page/生产.md "wikilink")。
      - [涅伊特](../Page/涅伊特.md "wikilink")（Neith/Net，Neit；Gold
        Dawn，Thoum-aesh-neith）：司戰爭、狩獵、編織及智慧之神，四位守護死者的女神之一。
      - [奎特什](../Page/奎特什.md "wikilink")（Qetesh）：司[愛情及](../Page/愛情.md "wikilink")[美麗之神](../Page/美.md "wikilink")。
      - [瑟克](../Page/瑟克.md "wikilink")（Seker）：死亡之神。
      - [绍席斯](../Page/亞圖姆.md "wikilink")（Saosis）：擁有一棵生命之樹的女神。許多神祇（尤其是[荷鲁斯](../Page/荷鲁斯.md "wikilink")）都由她的樹中誕生。
      - [薩提斯](../Page/萨提斯.md "wikilink")（Satis）：洪水之神。

## 参考文献

## 参见

  - [埃及神话](../Page/埃及神话.md "wikilink")
  - [希腊神话](../Page/希腊神话.md "wikilink")
  - [罗马神话](../Page/罗马神话.md "wikilink")

{{-}}

[Category:埃及神话](../Category/埃及神话.md "wikilink")
[\*](../Category/埃及神祇.md "wikilink")