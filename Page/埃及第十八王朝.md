**第十八王朝**，是[古埃及](../Page/古埃及.md "wikilink")[新王国时期的第一个王朝](../Page/新王国时期.md "wikilink")，也是[古埃及历史上最强盛的王朝](../Page/古埃及历史.md "wikilink")。第十八王朝所处的时间大致是公元前16世纪至公元前13世纪（约公元前1575年－约公元前1308年\[1\]），由[阿赫摩斯继位开始](../Page/阿赫摩斯.md "wikilink")，到[哈伦海布死后传位给](../Page/哈伦海布.md "wikilink")[拉美西斯一世结束](../Page/拉美西斯一世.md "wikilink")，共经历了近300年和14位[法老](../Page/法老.md "wikilink")。

约公元前1575年，[第十七王朝法老](../Page/埃及第十七王朝.md "wikilink")[卡摩斯的弟弟阿赫摩斯继位](../Page/卡摩斯.md "wikilink")。阿赫摩斯在位期间成功驱逐了[喜克索斯人](../Page/喜克索斯人.md "wikilink")，并统一了[上](../Page/上埃及.md "wikilink")[下埃及](../Page/下埃及.md "wikilink")，强大的第十八王朝建立了。其间埃及称为帝国，在一个世纪时间内，势力延伸深入[亚洲](../Page/亚洲.md "wikilink")，很多名义上独立的国家都承认埃及为其[领主国](../Page/领主国.md "wikilink")。位于[非洲](../Page/非洲.md "wikilink")[尼罗河上游](../Page/尼罗河.md "wikilink")[第五瀑布的](../Page/第五瀑布.md "wikilink")[努比亚也归附埃及](../Page/努比亚.md "wikilink")。埃及人开始与北[叙利亚和](../Page/叙利亚.md "wikilink")[南部非洲进行频繁的](../Page/南部非洲.md "wikilink")[贸易](../Page/贸易.md "wikilink")。王朝前几任法老还消灭了国内敌对的地方势力，建立了法老对[政治和](../Page/政治.md "wikilink")[军事的](../Page/军事.md "wikilink")[中央集权](../Page/中央集权.md "wikilink")。这一集权模式持续了几乎500年未曾改变，并使埃及王朝度过了几个危机，例如[图特摩斯一世政变](../Page/图特摩斯一世.md "wikilink")，女法老[哈特谢普苏特摄政等](../Page/哈特谢普苏特.md "wikilink")。第十八王朝后期争夺国家最高权力的斗争越来越激烈，[宗教领袖的影响力变大](../Page/宗教.md "wikilink")，严重威胁了法老的权力和统治。由于宗教影响力的增加，法老[阿蒙霍特普四世](../Page/阿蒙霍特普四世.md "wikilink")（后改名为[阿肯納頓](../Page/阿肯納頓.md "wikilink")）试图更改埃及信仰称为一神总教（仅崇拜太阳神[阿顿](../Page/阿顿.md "wikilink")），但他死后继位的法老，特别是[图坦卡蒙](../Page/图坦卡蒙.md "wikilink")，又恢复了埃及传统的宗教和法老统治。\[2\]约前1308年，任政府官员和大祭司拉美西斯一世继承王位，建立了[第十九王朝](../Page/埃及第十九王朝.md "wikilink")，第十八王朝结束。

## 历史

### 统一埃及

埃及第十八王朝的第一位法老[阿赫摩斯是](../Page/阿赫摩斯.md "wikilink")[埃及第十七王朝最后一位法老](../Page/埃及第十七王朝.md "wikilink")[卡摩斯的弟弟](../Page/卡摩斯.md "wikilink")（一说表弟）。约前1575年，卡摩斯在埃及与外族[喜克索斯人的战争中战死](../Page/喜克索斯人.md "wikilink")，阿赫摩斯继位在继位最初十年时间里，控制南部[上埃及的阿赫摩斯同控制着北部](../Page/上埃及.md "wikilink")[下埃及的喜克索斯人保持](../Page/下埃及.md "wikilink")[和平](../Page/和平.md "wikilink")，但在之后的5年间，阿赫摩斯发动了驱逐喜克索斯人的战争并获胜，重新将埃及全境统一在自己的独一统治之下。

之后阿赫摩斯将反抗侵略的战争扩大为[侵略战争](../Page/侵略战争.md "wikilink")，先后攻击[叙利亚](../Page/叙利亚.md "wikilink")[巴勒斯坦和](../Page/巴勒斯坦.md "wikilink")[努比亚](../Page/努比亚.md "wikilink")，使埃及帝国的势力范围延伸到境外，向北到达[白布罗斯](../Page/白布罗斯.md "wikilink")（今黎巴嫩境内），向南到达（位于[尼罗河第二瀑布附近](../Page/尼罗河.md "wikilink")）。从此，埃及以军事帝国的姿态傲视邻国，古埃及历史进入了一个全新的时期——[新王国时期](../Page/新王国时期.md "wikilink")。\[3\]

### 图特摩斯一世

阿赫摩斯死后由儿子[阿蒙霍特普一世继位](../Page/阿蒙霍特普一世.md "wikilink")，这位法老没有频繁的发动战争，而是更注重建造神殿。阿蒙霍特普一世死后，由于没有儿子，王位归于当时军事统帅图特摩斯，即[图特摩斯一世](../Page/图特摩斯一世.md "wikilink")。图特摩斯父母都不是王室亲戚，为保持王室血统，迎娶了阿赫摩斯的女儿做妻子，但也有人认为其实此人是图特摩斯一世自己的妹妹。\[4\]

图特摩斯一世刚刚登基，南部归属于埃及的努比亚就起兵叛乱，图特摩斯一世亲率军队出击，亲手杀死努比亚王，将其尸体抗回埃及首部[底比斯](../Page/底比斯.md "wikilink")。此后图特摩斯一世又发动了对叙利亚巴勒斯坦和努比亚的战争，将埃及疆域扩大南至[尼罗河第四瀑布附近的](../Page/尼罗河.md "wikilink")[那帕塔](../Page/那帕塔.md "wikilink")，北至今[土耳其边境附近](../Page/土耳其.md "wikilink")。图特摩斯一世带回来的战利品以及附属国连年的进贡使当时的埃及前所未有的富庶，不过大部分财富都被图特摩斯一世用来建造太阳神[阿蒙·拉的神殿了](../Page/阿蒙·拉.md "wikilink")。图特摩斯一世死后被葬在新的陵墓——[帝王谷中](../Page/帝王谷.md "wikilink")，之后三个王朝的法老均葬于此地。

### 女皇

图特摩斯一世的儿子[图特摩斯二世是](../Page/图特摩斯二世.md "wikilink")[庶出](../Page/庶出.md "wikilink")（非[正宫](../Page/正宫.md "wikilink")[皇后所生](../Page/皇后.md "wikilink")），因此与具有王室血统的[哈特谢普苏特结婚](../Page/哈特谢普苏特.md "wikilink")。哈特谢普苏特是图特摩斯一世皇后所生，其王室血统是否传于阿赫摩斯则由于其生母的血统而有争议。图特摩斯二世在位后期实际权力可能已经掌握在哈特谢普苏特手中，图特摩斯二世死后哈特谢普苏特更是将幼年的图特摩斯三世架空，自己做法老。成為埃及第三任女皇。\[5\]

哈特谢普苏特统治埃及期间，停止了其父亲图特摩斯一世的征战，开始与邻国通商，特别是据记载的出海联络[庞特](../Page/庞特.md "wikilink")（[:en:Punt](../Page/:en:Punt.md "wikilink")）\[6\]。此时的埃及十分繁华，哈特谢普苏特同样用大量财富建造神殿和陵墓，其中最著名的是底比斯尼罗河西岸的[停靈廟](../Page/停靈廟.md "wikilink")（[:en:Deir
el-Bahri](../Page/:en:Deir_el-Bahri.md "wikilink")）。\[7\]但由于停止了战争，埃及对叙利亚巴勒斯坦的控制权减弱了，致使女法老死后叙利亚巴勒斯坦就宣布独立了。

### 帝国鼎盛时期

[Obelisk-Lateran.jpg](https://zh.wikipedia.org/wiki/File:Obelisk-Lateran.jpg "fig:Obelisk-Lateran.jpg")

图特摩斯三世幼年时，埃及的实权被后母哈特谢普苏特掌握着。哈特谢普苏特死后，图特摩斯三世开始独自统治埃及，他进行连续不断的战争，平息了叙利亚巴勒斯坦的叛乱，恢复了哈特谢普苏特时代丧失的对该地区的统治。他在[美吉多](../Page/美吉多.md "wikilink")(又称麦吉杜)、[卡迭石](../Page/卡迭石.md "wikilink")、[卡尔赫美什等地取得一系列军事胜利](../Page/卡尔赫美什.md "wikilink")（以[围攻麦吉杜的战役最为有名](../Page/美吉多战役.md "wikilink")）。后来，图特摩斯三世打败了[米坦尼国王](../Page/米坦尼.md "wikilink")，夺占米坦尼王国位于[幼发拉底河西岸的土地](../Page/幼发拉底河.md "wikilink")。经过长期的征服，埃及南部的边界被图特摩斯三世扩展至[尼罗河第四瀑布](../Page/尼罗河.md "wikilink")。他还使[利比亚](../Page/利比亚.md "wikilink")、[亚述](../Page/亚述.md "wikilink")、[巴比伦](../Page/巴比伦.md "wikilink")、[赫梯及](../Page/赫梯.md "wikilink")[克里特岛的统治者们都向他纳贡](../Page/克里特岛.md "wikilink")。由于图特摩斯三世的赫赫武功，一些历史学家称他为古埃及的[拿破仑](../Page/拿破仑.md "wikilink")。

同时，图特摩斯三世还对其归属国进行[文治](../Page/文治.md "wikilink")，他把各归属国的[皇子带到埃及](../Page/皇子.md "wikilink")，一方面作为[人质](../Page/人质.md "wikilink")，而更重要的是去教育他们永远忠于埃及。这一政策十分成功，使得图特摩斯三世的几位继任者不需要维持庞大的军队就可以维护埃及帝国对领国的控制和影响。\[8\]为了避免皇后干政的“悲剧”重演，图特摩斯三世执政后期将王位传给了儿子[阿蒙霍特普二世](../Page/阿蒙霍特普二世.md "wikilink")，两人共同执政，传位三年后图特摩斯三世逝世。

### 宗教改革

[La_salle_dAkhenaton_(1356-1340_av_J.C.)_(Musée_du_Caire)_(2076972086).jpg](https://zh.wikipedia.org/wiki/File:La_salle_dAkhenaton_\(1356-1340_av_J.C.\)_\(Musée_du_Caire\)_\(2076972086\).jpg "fig:La_salle_dAkhenaton_(1356-1340_av_J.C.)_(Musée_du_Caire)_(2076972086).jpg")

图特摩斯三世死后50多年，埃及没有发生重大战争，国力在其[曾孙](../Page/曾孙.md "wikilink")[阿蒙霍特普三世期间达到顶峰](../Page/阿蒙霍特普三世.md "wikilink")。阿蒙霍特普三世是古埃及历史上最杰出的建筑师之一，一生建造了许多宏伟的建筑。

阿蒙霍特普三世的儿子阿蒙霍特普四世在位时期推行了可能是古埃及历史上最重大的举措——宗教改革，为此阿蒙霍特普四世把自己的名字改为[阿肯納頓](../Page/阿肯納頓.md "wikilink")，意为“太阳神[阿顿的光芒](../Page/阿顿.md "wikilink")”。在此之前，古埃及宗教信仰中有很多神祇，其中重要的是太阳神拉、代表法老的荷鲁斯、和代表第十七王朝发源地底比斯的阿蒙。由于法老在埃及被认为是同神一体的人物（称为神王，god-king），政府对宗教和祭司十分重视，祭司门获得重权，法老也都热衷于修建神殿。阿蒙霍特普四世则在任期内简化了多神系统，要求全国统一-{只}-相信太阳神阿顿，其他神祇的神殿则被关闭，他还把埃及首都从底比斯迁至他新建的城市阿肯塔頓。这是世界上第一次出现一神论的宗教。但由于宗教改革撼动了掌握重权的祭司的地位，阿肯納頓死后，在[图坦卡門即位](../Page/图坦卡門.md "wikilink")3年后，阿蒙神被重新树立，寺庙也都恢复了，埃及首都也迁回底比斯，宗教改革失败了。

在此之后，王位落入政府高官手中，十八王朝最后两位法老均不是王室血统，最后一位法老[哈伦海布仍然没有儿子](../Page/哈伦海布.md "wikilink")，将法老位传给了当时的大祭司和税务总管帕拉米苏，即第十九王朝第一位法老[拉美西斯一世](../Page/拉美西斯一世.md "wikilink")，第十八王朝结束了。

## 法老列表

埃及第十八王朝法老列表以在位次序排列如下。\[9\]

<table>
<thead>
<tr class="header">
<th><p>法老</p></th>
<th><p>在位时间</p></th>
<th><p>王室血统</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿赫摩斯.md" title="wikilink">阿赫摩斯</a></p></td>
<td><p>25年</p></td>
<td><p><a href="../Page/卡摩斯.md" title="wikilink">卡摩斯之弟</a></p></td>
<td><p>约前1575年继位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿蒙霍特普一世.md" title="wikilink">阿蒙霍特普一世</a></p></td>
<td><p>21年</p></td>
<td><p>阿赫摩斯之子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/图特摩斯一世.md" title="wikilink">图特摩斯一世</a></p></td>
<td><p>12年</p></td>
<td><p>阿蒙霍特普一世女婿</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/图特摩斯二世.md" title="wikilink">图特摩斯二世</a></p></td>
<td><p>13年</p></td>
<td><p>图特摩斯一世之子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈特谢普苏特.md" title="wikilink">哈特谢普苏特</a></p></td>
<td><p>21年</p></td>
<td><p>图特摩斯二世正室</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/图特摩斯三世.md" title="wikilink">图特摩斯三世</a></p></td>
<td><p>33年（名义上为54年）</p></td>
<td><p>图特摩斯二世之子</p></td>
<td><p>非哈特谢普苏特所生</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿蒙霍特普二世.md" title="wikilink">阿蒙霍特普二世</a></p></td>
<td><p>26年</p></td>
<td><p>图特摩斯三世之子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/图特摩斯四世.md" title="wikilink">图特摩斯四世</a></p></td>
<td><p>10年</p></td>
<td><p>阿蒙霍特普二世之子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿蒙霍特普三世.md" title="wikilink">阿蒙霍特普三世</a></p></td>
<td><p>38年</p></td>
<td><p>图特摩斯四世之子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿蒙霍特普四世.md" title="wikilink">阿蒙霍特普四世</a></p></td>
<td><p>17年</p></td>
<td><p>阿蒙霍特普三世之子</p></td>
<td><p>后改名<a href="../Page/埃赫那顿.md" title="wikilink">埃赫那顿</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯门卡拉.md" title="wikilink">斯门卡拉</a>[10]</p></td>
<td><p>3年</p></td>
<td><p>埃赫那顿之子</p></td>
<td><p>有争议</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/图坦卡蒙.md" title="wikilink">图坦卡蒙</a></p></td>
<td><p>11年</p></td>
<td><p>埃赫那顿之子</p></td>
<td><p>有争议</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊特努特·阿伊.md" title="wikilink">伊特努特·阿伊</a></p></td>
<td><p>4年</p></td>
<td><p>以上三个法老的大臣</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈伦海布.md" title="wikilink">哈伦海布</a></p></td>
<td><p>约28年</p></td>
<td><p>伊特努特·阿伊的女婿和大臣</p></td>
<td><p>约前1308年逝世<br />
传位给<a href="../Page/拉美西斯一世.md" title="wikilink">拉美西斯一世</a></p></td>
</tr>
</tbody>
</table>

## 参见

  - [古埃及历史](../Page/古埃及历史.md "wikilink")
  - [埃及法老列表](../Page/埃及法老列表.md "wikilink")
  - [新王国时期](../Page/新王国时期.md "wikilink")

## 注释

|- | width="30%" align="center"
|[埃及第十七王朝](../Page/埃及第十七王朝.md "wikilink") |
width="40%" align="center" | **[古埃及王朝](../Page/古埃及历史.md "wikilink")**
约前1575年－前1308年 | width="30%" align="center"
|[埃及第十九王朝](../Page/埃及第十九王朝.md "wikilink")

[\*](../Category/第十八王朝.md "wikilink")

1.  古埃及历史的年代均有争议，新王国时期的年代误差一般在10年之内

2.  穆尔内（William J. Murnane），《古埃及》（The Penguin Guide to Ancient
    Egypt），英国：企鹅图书公司，1984年

3.  《第十八王朝》（18th Dynasty），古埃及网（Ancient-egypt.org），2007年1月13日更新。

4.
5.  前面兩任為埃及第六王朝的[尼托克里斯與埃及第十二王朝的](../Page/尼托克里斯.md "wikilink")[塞贝克诺弗鲁](../Page/塞贝克诺弗鲁.md "wikilink")

6.  庞特是古地名，仅在古埃及文档中有记载，目前不知道具体是什么地方

7.
8.
9.  加迪那，《法老埃及》（Egypt of the Pharaohs），牛津大学出版社，1961年

10. 部分學者認為在斯門卡拉與圖坦卡蒙之間或許有一位女皇[娜芙娜芙魯阿吞](../Page/娜芙娜芙魯阿吞.md "wikilink")，這女皇身份有爭議，或為[娜芙蒂蒂或](../Page/娜芙蒂蒂.md "wikilink")[梅莉塔吞](../Page/梅莉塔提.md "wikilink")。