**埃姆斯河**（[德語](../Page/德語.md "wikilink")：****；[荷蘭語](../Page/荷蘭語.md "wikilink")：****）是一條發源於[德國北部的河流](../Page/德國.md "wikilink")，流經[北萊茵-威斯特法倫與](../Page/北萊茵-威斯特法倫.md "wikilink")[下薩克森](../Page/下薩克森.md "wikilink")，而且也是德國下薩克森與荷蘭[格羅寧根省之間的國界](../Page/格羅寧根省.md "wikilink")。埃姆斯河總長371公里。

## 發源

埃姆斯河發源於德國北部的北萊茵－威斯特法倫[條頓堡林山](../Page/條頓堡林山.md "wikilink")（）的南部。在下薩克森州，埃姆河變成一條比較大的河流，埃姆沼澤也因此得名。大支流[哈瑟河在](../Page/哈瑟河.md "wikilink")[梅彭](../Page/梅彭.md "wikilink")（Meppen）注入埃姆斯河，然後埃姆斯河往北流，在接近荷蘭的國界的位置，進入了[東弗里西亞](../Page/東弗里西亞.md "wikilink")。然後在靠近[埃姆登的地方](../Page/埃姆登.md "wikilink")，埃姆斯河流入荷蘭，往[代尔夫宰尔前進](../Page/代尔夫宰尔.md "wikilink")。

在埃姆登與代尔夫宰尔之間，埃姆斯河成為德國與荷蘭的邊界。而在這裡也產生了爭論，荷蘭人認為埃姆斯河穿過了河口的地理中心，但是德國人卻認為埃姆斯河流經了最深的河道（接近荷蘭海岸）。因為德國與荷蘭關係良好，所以爭論並沒有進一步擴大。在流經代尔夫宰尔後，埃姆斯河進入了[瓦登海](../Page/瓦登海.md "wikilink")（北海的一部分）。

[Ems_Telgte.jpg](https://zh.wikipedia.org/wiki/File:Ems_Telgte.jpg "fig:Ems_Telgte.jpg")的埃姆斯河\]\]
[Ems_Wasserfall_Hanekenfaehr.jpg](https://zh.wikipedia.org/wiki/File:Ems_Wasserfall_Hanekenfaehr.jpg "fig:Ems_Wasserfall_Hanekenfaehr.jpg")
[Meppen_hubbruecke.jpg](https://zh.wikipedia.org/wiki/File:Meppen_hubbruecke.jpg "fig:Meppen_hubbruecke.jpg")

## 旅遊

有 3 條遠程[單車路線經過埃姆斯河](../Page/單車.md "wikilink")：

  - EmsAuenWeg
  - Emsland Route
  - Hase Ems Weg

## 外部連結

  - [www.ems.nrw.de/](http://www.ems.nrw.de/)
  - [The Ems with Emsbueren](http://www.unsere-schoene-ems.de/)

[E](../Category/荷兰河流.md "wikilink") [E](../Category/德國河流.md "wikilink")
[Category:埃姆斯河水系](../Category/埃姆斯河水系.md "wikilink")