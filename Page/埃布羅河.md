[Ebro_spanien.png](https://zh.wikipedia.org/wiki/File:Ebro_spanien.png "fig:Ebro_spanien.png")
**埃布羅河**（Ebro）是[伊比利半島第二長的河流](../Page/伊比利半島.md "wikilink")（长910公里），流域面积8.5万平方公里，也是完全在[西班牙境內最長的河流](../Page/西班牙.md "wikilink")（更長的[塔霍河下游在](../Page/塔霍河.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")）。发源于[坎塔布连山脈](../Page/坎塔布连山脈.md "wikilink")，上游水流湍急，中游进入盆地平原，流缓，多沉积，朝東南方流入[地中海並形成宽广的](../Page/地中海.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")。

歷史上是[羅馬共和國和](../Page/羅馬共和國.md "wikilink")[迦太基](../Page/迦太基.md "wikilink")，以及[查里曼帝国的](../Page/查里曼帝国.md "wikilink")[西班牙邊疆區和](../Page/西班牙马克.md "wikilink")[後倭馬亞王朝的分界線](../Page/後倭馬亞王朝.md "wikilink")。

## 參閱條目

  - [埃布羅協議](../Page/埃布羅協議.md "wikilink")

[E](../Category/西班牙河流.md "wikilink")