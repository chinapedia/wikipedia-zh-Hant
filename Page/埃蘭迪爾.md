航海家**埃蘭迪爾** *Eärendil the
Mariner*，是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·鲁埃爾·托爾金的史詩式奇幻小說](../Page/約翰·羅納德·鲁埃爾·托爾金.md "wikilink")《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》中的人物。他的父親是人類英雄[圖爾](../Page/图尔_\(小说人物\).md "wikilink")，母親則是隱藏王國[貢多林之王](../Page/貢多林.md "wikilink")[特剛之女](../Page/特剛.md "wikilink")[伊綴爾·凱勒布琳朵](../Page/伊綴爾·凱勒布琳朵.md "wikilink")。他與半精靈[愛爾溫結婚](../Page/愛爾溫.md "wikilink")，生下双胞胎儿子[愛洛斯和](../Page/愛洛斯.md "wikilink")[愛隆](../Page/愛隆.md "wikilink")，他是人類和精靈的救星，人称“聪慧的埃兰迪尔”，“蒙受祝福的埃兰迪尔”。

## 列傳

他幼年時居住在貢多林，在他七歲那年，貢多林陷落，他隨父母逃到[西瑞安河口](../Page/西瑞安河.md "wikilink")[阿佛尼恩港](../Page/阿佛尼恩.md "wikilink")
*Arvernien*。圖爾與伊綴爾前往西方，從此不見蹤影後，埃蘭迪爾成為阿佛尼恩港領袖，他一直想揚帆遠颺，尋找一去不返的父母，也想找到終極之岸，在他死前將人類和精靈的處境告訴[維拉](../Page/維拉.md "wikilink")，也許他們會因此受到感動，憐憫[中土大陸的悲傷](../Page/中土大陸.md "wikilink")。

他與造船者[瑟丹建立了良好的友誼](../Page/瑟丹.md "wikilink")，他幫埃蘭迪爾打造世上最美麗的一艘船，[威基洛特](../Page/威基洛特.md "wikilink")
*Vingilótë*（或稱*Vingilot*），意思是「浪花」
*foam-flower*，埃蘭迪爾常常用此船遊走中土大陸海岸，但從未抵達[維林諾](../Page/維林諾.md "wikilink")，他總是一次又一次被陰影與魔法擊退，被狂風驅逐。

埃蘭迪爾出外之時，[費諾長子](../Page/費諾.md "wikilink")[梅斯羅斯得知](../Page/梅斯羅斯.md "wikilink")[精靈寶鑽在西瑞安河口](../Page/精靈寶鑽_\(寶物\).md "wikilink")，他沒有採取任何行動。但那誓言不斷折磨他與他的弟弟，梅斯羅斯送了表示友好的信件，但堅持[精靈寶鑽的所有權](../Page/精靈寶鑽_\(寶物\).md "wikilink")，但愛爾溫拒絕他們，因為那顆[精靈寶鑽是由貝倫與露西安自](../Page/精靈寶鑽_\(寶物\).md "wikilink")[魔苟斯處奪回的](../Page/魔苟斯.md "wikilink")，而且愛爾溫的父親，[多瑞亞斯國王](../Page/多瑞亞斯.md "wikilink")[迪歐也是被費諾的兒子們所殺](../Page/迪歐.md "wikilink")，更令西瑞安河口精靈反感的是，他們在埃蘭迪爾不在時來搶奪[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。於是梅斯羅斯迫於無奈之下，下令攻打西瑞安河口，當諾多族最高君王[吉爾加拉德和造船者瑟丹趕到時](../Page/吉爾加拉德.md "wikilink")，愛爾溫已經帶住[精靈寶鑽跳下海中](../Page/精靈寶鑽_\(寶物\).md "wikilink")，而她的兒子愛洛斯和愛隆則被梅斯羅斯和[梅格洛爾俘虜](../Page/梅格洛爾.md "wikilink")。

不過[精靈寶鑽也未失落](../Page/精靈寶鑽_\(寶物\).md "wikilink")，維拉大海之神[烏歐牟自大海中捧起愛爾溫](../Page/烏歐牟.md "wikilink")，將她化為一隻白色的大海鳥，[精靈寶鑽鑲嵌在她胸口中](../Page/精靈寶鑽_\(寶物\).md "wikilink")。在一個夜裡，當埃蘭迪爾在船上掌舵，看見她飛快地向他衝來，跌落在威基洛特的甲板上，幾乎斷了氣。埃蘭迪爾捧起抱在懷中，第二天早晨，大海鳥變回愛爾溫。

埃蘭迪爾從愛爾溫口中得知阿佛尼恩港的慘劇，認為自己的兒子也有死無生了，於是他們不返航，直駛向維林諾。因為[精靈寶鑽的力量](../Page/精靈寶鑽_\(寶物\).md "wikilink")，他們通過了[魔法島嶼](../Page/魔法島嶼.md "wikilink")，逃過其中的迷咒，來到[陰影海峽](../Page/陰影海峽.md "wikilink")，通過了它的陰影。[伊瑞西亞島的](../Page/伊瑞西亞島.md "wikilink")[帖勒瑞族精靈看到有船自東而來](../Page/帖勒瑞族.md "wikilink")，無不驚訝萬分。

埃蘭迪爾在維林諾節慶時到達，所以人們皆聚集在[泰尼魁提爾山上](../Page/泰尼魁提爾山.md "wikilink")，[提理安城空無一人](../Page/提理安.md "wikilink")，使得埃蘭迪爾以為邪惡也入侵蒙福之地了，在他困惑之時，[邁雅](../Page/邁雅.md "wikilink")[伊昂威出現在他面前](../Page/伊昂威.md "wikilink")，[伊昂威对他说道](../Page/伊昂威.md "wikilink")：“你好，埃兰迪尔，最著名的水手，在众人未查之际来到，在渴望中冲破绝望来到\!你好，埃兰迪尔，身负日月上升之前的光芒\!壮丽的大地儿女啊，犹如
黑暗之中的明星，日落时分的宝石，光辉灿烂的黎明\!”

伊昂威帶埃蘭迪爾到[維利瑪](../Page/維利瑪.md "wikilink")，連烏歐牟也被召來了。埃蘭迪爾懇求維拉向人類和精靈伸出慈悲之手，他為[諾多族懇求原諒](../Page/諾多族.md "wikilink")，他的祈求得到應允。

埃蘭迪爾離開維利瑪，找尋妻子的時候，維拉冥王[曼督斯論及他的命運](../Page/曼督斯.md "wikilink")：「凡人活著涉足不死之地，豈還容他活著不死？」烏歐牟反問：「他生來正為達成此事。請告訴我：埃蘭迪爾究竟是人類[哈多家族的圖爾之子](../Page/哈多.md "wikilink")，還是精靈的[芬威家族](../Page/芬威.md "wikilink")，特剛的女兒伊綴爾之子？」曼督斯回答：「等同諾多之命運，他們任性剛愎踏上流亡之路，永遠不得歸返此地。」

維拉之首[曼威下了判決](../Page/曼威.md "wikilink")，他們不會受到懲罰，但他們永遠不容再踏上彼岸，他們和他們的子女得以自由選擇他們的命運，歸屬人類或精靈的命運。愛爾溫首先選擇成為精靈，埃蘭迪爾本想成為人類，但因妻子的選擇而選擇成為精靈。

曼威將威基洛特封為聖，埃蘭迪爾戴住[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")，駕住威基洛特飛往天上，化作暮星（在現實中，即為[金星](../Page/金星.md "wikilink")）。在[憤怒之戰中](../Page/憤怒之戰.md "wikilink")，埃蘭迪爾殺了黑龍[安卡拉剛](../Page/安卡拉剛.md "wikilink")
*Ancalagon*，安卡拉剛跌在[安戈洛墜姆上](../Page/安戈洛墜姆.md "wikilink")，與安戈洛墜姆一同崩塌傾倒。而《精靈寶鑽》中的舞台，[貝爾蘭地區](../Page/貝爾蘭.md "wikilink")，毀滅下沉了。

## 《魔戒》中

《魔戒》中，[凱蘭崔爾給](../Page/凱蘭崔爾.md "wikilink")[佛羅多·巴金斯的禮物星光寶瓶](../Page/佛羅多·巴金斯.md "wikilink")，盛放的正是埃蘭迪爾之光。在大蜘蛛[屍羅的攻擊中](../Page/屍羅.md "wikilink")，佛羅多拿住星光寶瓶大叫*Aiya
Eärendil Elenion
Ancalima\!*，這是[昆雅語](../Page/昆雅語.md "wikilink")，意思是「向最明亮的星辰埃兰迪尔致敬！」而埃蘭迪爾之光，源自[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。

## 半精靈家系

## 參見

  - [精靈寶鑽](../Page/精靈寶鑽.md "wikilink")

[Category:托爾金作品](../Category/托爾金作品.md "wikilink")
[Category:中土大陸的角色](../Category/中土大陸的角色.md "wikilink")