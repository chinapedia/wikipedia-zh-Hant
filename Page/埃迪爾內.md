**埃迪爾內**（），或稱**哈德良堡**或**阿德里安堡**（），因羅馬皇帝[哈德良所建而得名](../Page/哈德良.md "wikilink")。土耳其語埃迪爾內是希臘語阿德里安堡的音譯，故兩種稱呼並不是兩個不同的名字。該城是[土耳其](../Page/土耳其.md "wikilink")[埃迪爾內省省會](../Page/埃迪爾內省.md "wikilink")，位於鄰近[希臘和](../Page/希臘.md "wikilink")[保加利亞的邊境](../Page/保加利亞.md "wikilink")。2002年人口为128,400人。

## 历史

378年，[羅馬帝國皇帝](../Page/羅馬帝國.md "wikilink")[瓦倫斯在此处同](../Page/瓦倫斯.md "wikilink")[哥特人的战役中陣亡](../Page/哥特.md "wikilink")，史称[阿德里安堡戰役](../Page/阿德里安堡戰役.md "wikilink")。另外，1365至1453年这里是[鄂圖曼帝國的首都](../Page/鄂圖曼帝國.md "wikilink")。

## 活动

當地最著名的活動是[克爾克普那爾摔跤大會](../Page/克爾克普那爾摔跤大會.md "wikilink")（）。

## 参考资料

## 参看

  - [阿德里安堡战役](../Page/阿德里安堡战役.md "wikilink")

[Category:土耳其城市](../Category/土耳其城市.md "wikilink")
[Category:歐洲城市](../Category/歐洲城市.md "wikilink")