**埃里克·哈里斯**（[英文](../Page/英文.md "wikilink")：，1981年4月9日 –
1999年4月20日）和**迪倫·克萊伯德**（[英文](../Page/英文.md "wikilink")：，1981年9月11日
–
1999年4月20日）曾是[美國](../Page/美國.md "wikilink")[科罗拉多州](../Page/科罗拉多州.md "wikilink")[科倫拜高中](../Page/科倫拜高中.md "wikilink")[12年级](../Page/12年级.md "wikilink")[学生](../Page/学生.md "wikilink")。两人于1999年4月20日在校园内枪杀了13人，射伤24人，随後[开枪自尽](../Page/飲彈.md "wikilink")，製造了[科倫拜校園槍擊事件](../Page/科倫拜校園槍擊事件.md "wikilink")。

## 延伸閱讀

  -
  -
  -
  -
## 外部連結

  - [Crimelibrary
    feature](https://web.archive.org/web/20080629093128/http://www.trutv.com/library/crime/notorious_murders/mass/littleton/index_1.html)
  - [Makers of Luvox sued by victims'
    families](http://www.cbsnews.com/stories/2001/10/22/national/main315368.shtml)
  - [The Smoking Gun
    feature](http://www.thesmokinggun.com/archive/0707061columbine1.html)
    - Essay by Harris on keeping guns out of schools

[Category:美國殺童犯](../Category/美國殺童犯.md "wikilink")
[Category:美國自殺者](../Category/美國自殺者.md "wikilink")
[Category:校園槍擊事件人物](../Category/校園槍擊事件人物.md "wikilink")
[Category:科羅拉多州人](../Category/科羅拉多州人.md "wikilink")
[Category:美国谋杀－自杀案](../Category/美国谋杀－自杀案.md "wikilink")
[Category:科倫拜校園事件](../Category/科倫拜校園事件.md "wikilink")
[Category:美國校園暴力受害者](../Category/美國校園暴力受害者.md "wikilink")
[Category:美國縱火犯](../Category/美國縱火犯.md "wikilink")
[Category:自殺學生](../Category/自殺學生.md "wikilink")