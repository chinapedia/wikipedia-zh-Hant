**艾瑞克·愛默生·施密特**（；）是一位美国[企业家](../Page/企业家.md "wikilink")、计算机[软件工程師](../Page/软件工程師.md "wikilink")，2015年开始担任[Alphabet
Inc.的執行](../Page/Alphabet_Inc..md "wikilink")[董事長至](../Page/董事長.md "wikilink")2017年12月21日宣布卸任\[1\]，但依旧保留[Alphabet
Inc.董事会成员身份并且担任技术顾问一职](../Page/Alphabet_Inc..md "wikilink")
。2001年到2011年十年间在Google担任[CEO](../Page/首席执行官.md "wikilink")，也曾是[蘋果公司](../Page/蘋果公司.md "wikilink")[董事會成員](../Page/董事會.md "wikilink")\[2\]。同時，他亦為[卡內基美隆大學和](../Page/卡內基美隆大學.md "wikilink")[普林斯頓大學理事會託管者](../Page/普林斯頓大學.md "wikilink")，並是程式編譯器[lex的共同作者](../Page/lex.md "wikilink")。

## 簡史

施密特於[美國](../Page/美國.md "wikilink")[華盛頓特區出生](../Page/華盛頓特區.md "wikilink")，並於[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[阿靈頓郡](../Page/阿靈頓郡.md "wikilink")[約克敦高中畢業](../Page/約克敦高中.md "wikilink")。1976年，施密特在[普林斯顿大学取得電機工程](../Page/普林斯顿大学.md "wikilink")（BSEE）的[學士及](../Page/學士.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")；1979年，施密特於[柏克萊加州大學因為設計一電腦網絡而得到](../Page/柏克萊加州大學.md "wikilink")[理學](../Page/理學.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")；1982年，以探討分散式的管理軟體問題為主要論文課題，取得了電子工程暨電腦科學（EECS）[博士學位](../Page/博士.md "wikilink")。之後施密特並曾加入了程式編譯器[lex的編輯工程](../Page/lex.md "wikilink")，且於[-{zh-cn:斯坦福商学研究生院;
zh-tw:史丹佛商學研究所;
zh-hk:史丹福商學研究院;}-兼職教授](../Page/斯坦福商学研究生院.md "wikilink")。目前施密特與妻子溫蒂共同住在[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")。

## 商务

### 早期職業生涯

施密特早期主要於資訊科技公司工作，包含了[貝爾實驗室](../Page/貝爾實驗室.md "wikilink")、[Zilog和當時](../Page/Zilog.md "wikilink")[全錄轄下的](../Page/全錄.md "wikilink")[帕羅奧多研究中心擔任研究員](../Page/帕羅奧多研究中心.md "wikilink")。1983年，施密特以軟體經理的身分加入[昇陽電腦](../Page/昇陽電腦.md "wikilink")，並主導了[Java平台技術的發展](../Page/Java.md "wikilink")、昇陽電腦獨立平台的程式技術，並且確立了公司的網络策略。之後，他成為了昇陽電腦的[首席技术官](../Page/首席技术官.md "wikilink")（CTO）及企業总裁。

1997年，施密特担任了[Novell公司的](../Page/Novell.md "wikilink")[总裁](../Page/总裁.md "wikilink")，之後Novell收購了[Cambridge
Technology
Partners](../Page/Cambridge_Technology_Partners.md "wikilink")，但他之後也離開了此公司。在[风险投资家](../Page/风险投资.md "wikilink")[约翰·杜尔和](../Page/约翰·杜尔.md "wikilink")[Michael
Moritz的建議下](../Page/Michael_Moritz.md "wikilink")，[Google创始人](../Page/Google.md "wikilink")[拉里·佩奇和](../Page/拉里·佩奇.md "wikilink")[謝爾蓋·布林於](../Page/謝爾蓋·布林.md "wikilink")2001年招募艾瑞克·施密特來运作Google公司。

### Google公司

施密特在2001年3月以[董事會成員的身分擔任](../Page/董事會.md "wikilink")[Google的董事長](../Page/Google.md "wikilink")，並在同年8月兼任总裁一職。任職於Google時，施密特採取與兩位創始人佩奇及布林共同負責公司的日常運作的工作方式。在Google提交給[美國證券交易委員會的檔案](../Page/美國證券交易委員會.md "wikilink")\[3\]的第29頁中，說明了兩位創始人佩奇及布林與施密特運作Google公司的關係係屬三人同盟，施密特以執行長身份負責公司的正常運作並且專注於銷售部分。而從Google的公司網頁中得知，施密特也著重以下的工作：建設公司的基礎建設以應付Google的快速發展；確保產品的開發周期縮到最短，但仍能保持產品的品質。

2007年，由[PC
World所選出的](../Page/PC_World.md "wikilink")「50名重要影響網路世界的人」名單中，施密特與Google的兩位創始人[拉里·佩奇及布林等](../Page/拉里·佩奇.md "wikilink")3人榮任第一名。2011年1月施密特卸任CEO，由拉里·佩奇接替。

2013年1月，埃里克·施密特和[Jared
Cohen](../Page/Jared_Cohen.md "wikilink")、[新墨西哥州前州长](../Page/新墨西哥州.md "wikilink")[比尔·理查森](../Page/比尔·理查森.md "wikilink")、女儿苏菲访-{}-问[北朝鲜](../Page/北朝鲜.md "wikilink")\[4\]\[5\]\[6\]。
同年11月，埃里克·施密特呼籲中國[開放網際網路](../Page/中華人民共和國網路審查.md "wikilink")、尊重民眾的[言論自由](../Page/言論自由.md "wikilink")。他說中國的新聞檢查問題明顯惡化，他對中國當局加緊打壓網路言論、對轉發所謂“網路謠言”500次入罪的司法解釋感到擔憂，並表示如果中國不改變目前的[新聞檢查制度](../Page/中華人民共和國新聞自由.md "wikilink")，谷歌就不會重返中國\[7\]，并在当年四月出版的新书《新数字时代》(The
New Digital Age)中认为中国是一个危险、虎视眈眈的超级大国\[8\]。

### Apple公司

2006年8月28日，施密特當選成為[蘋果公司董事會成員](../Page/蘋果公司.md "wikilink")；但在2009年8月3日，因為Google與蘋果公司日益激烈的[利益衝突和競爭](../Page/利益衝突.md "wikilink")，施密特宣布退出董事會。

### 政治傾向

2008年10月19日，施密特擔任了[奧巴馬於](../Page/奧巴馬.md "wikilink")[美國總統選舉期間的非正式顧問](../Page/美國總統選舉.md "wikilink")，並開始競選活動。在宣布其支持奧巴馬後，施密特曾開玩笑的說奧巴馬成為[美國總統後](../Page/美國總統.md "wikilink")，他的1美元工資，將會得到減稅的機會。奧巴馬勝選後，施密特為其諮詢委員會的其中一員，並建議要解決美國內部問題最簡單的方法為獎勵[可再生能源的開發](../Page/可再生能源.md "wikilink")，以取代[化石燃料的使用](../Page/化石燃料.md "wikilink")。

2011年三月，艾瑞克·施密特获得[美国商务部长的提名](../Page/美国商务部.md "wikilink")。

## 薪資

施密特雖擔任Google董事長兼首席執行官，然而曾於2006年僅獲得1[美元的基本工資](../Page/美元.md "wikilink")。但儘管那時施密特並未擁有其他的現金、股票等，他仍為第一批以員工身分所獲得股權而成为亿万富翁的人（[微軟前总裁](../Page/微軟.md "wikilink")[史蒂夫·鲍尔默是第一個達到此目標的人](../Page/史蒂夫·鲍尔默.md "wikilink")）。2006年「全球富豪榜」排名上，《[福布斯](../Page/福布斯.md "wikilink")》把他排到116名（[Onsi
Sawiris](../Page/Onsi_Sawiris.md "wikilink")、[Alexei
Kuzmichov和](../Page/Alexei_Kuzmichov.md "wikilink")[Robert
Rowling亦在这个位置上](../Page/Robert_Rowling.md "wikilink")），合计个人资产達62亿[美元](../Page/美元.md "wikilink")。之後於2008年，其獲得補償508763美元；2009年，又再次獲得508763美元補償。

## 注釋

## 参考文献

## 外部链接

  - [CNET](../Page/CNET.md "wikilink"): [Google balances privacy,
    reach](https://archive.is/20121205064120/http://news.com.com/2102-1032_3-5787483.html?tag=st.util.print)（July
    14, 2005）, which uses Schmidt as an example of the personal
    information held by Google.

  - [AlwaysOn](../Page/AlwaysOn.md "wikilink"):
    [Interview](https://web.archive.org/web/20060821130436/http://www.alwayson-network.com/comments.php?id=325_0_1_0_C)（April
    22, 2003）, in which he discusses the future of computing, and
    mankind

  -
## 参见

  - [全球富豪榜](../Page/全球富豪榜.md "wikilink")
  - [Alphabet Inc.](../Page/Alphabet_Inc..md "wikilink")
  - [lex](../Page/lex.md "wikilink")

{{-}}

[Category:Google僱員](../Category/Google僱員.md "wikilink")
[Category:蘋果公司人物](../Category/蘋果公司人物.md "wikilink")
[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:美國億萬富豪](../Category/美國億萬富豪.md "wikilink")
[Category:美國電機工程師](../Category/美國電機工程師.md "wikilink")
[Category:史丹佛大學教師](../Category/史丹佛大學教師.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:華盛頓哥倫比亞特區人](../Category/華盛頓哥倫比亞特區人.md "wikilink")
[Category:昇陽電腦人物](../Category/昇陽電腦人物.md "wikilink")
[Category:貝爾實驗室科學家](../Category/貝爾實驗室科學家.md "wikilink")

1.  [Eric Schmidt to become technical advisor to
    Alphabet](https://abc.xyz/investor/news/releases/2017/1221.html).Alphabet
    Investor Relations.\[2017-12-21\].
2.  [CNET:Eric Schmidt leaves Apple
    board](http://news.cnet.com/8301-11424_3-10302007-90.html&anno=2)
3.   Google's [S-1
    Filing](http://www.sec.gov/Archives/edgar/data/1288776/000119312504142742/ds1a.htm)
4.
5.
6.
7.  [谷歌：中國不改言論審查就不會重返中國](http://www.voachinese.com/content/google-cheif-describe-china-internet-control-20131105/1783683.html)，[美國之音](../Page/美國之音.md "wikilink"),
    11.05.2013
8.  [谷歌施密特：中国是最危险的超级大国](http://tech.huanqiu.com/it/2013-07/4162944.html?from=mobile).环球网.