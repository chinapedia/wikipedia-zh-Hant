**埃里希·魯登道夫**（，），是[德国的著名将军](../Page/德意志帝国.md "wikilink")。他是[第一次世界大战时的重要主将](../Page/第一次世界大战.md "wikilink")。

## 早期生活

鲁登道夫出生于[普鲁士的](../Page/普鲁士.md "wikilink")[波森省](../Page/波森省.md "wikilink")（今[波兰](../Page/波兰.md "wikilink")[波茲南](../Page/波茲南.md "wikilink")）附近的一个小镇。他出生农家，虽然其母属于贵族阶级，严格而言他始终不属于[容克阶层](../Page/容克.md "wikilink")。他在12岁时进入陆军幼年学校。他拥有卓越的[数学能力与良好的](../Page/数学.md "wikilink")[职业道德](../Page/职业道德.md "wikilink")，後来与[海因茨·古德里安入读同一间训练出很多优秀军官的军校](../Page/海因茨·古德里安.md "wikilink")。1882年，即他17岁时他从陆军军官学校毕业，被授予[少尉军衔](../Page/少尉.md "wikilink")。

他18岁时就已经是军官，表现优异，在1894年晋升到[德国参谋部](../Page/德国参谋部.md "wikilink")，在1908年成为参谋部部署部总长。后来，他参与试验著名的[施里芬计划之细节](../Page/施里芬计划.md "wikilink")，研究[比利时的要塞城市](../Page/比利时.md "wikilink")[列日](../Page/列日.md "wikilink")。而且，他更大胆尝试让德军备战，却为多数军官所反对。

## 第一次世界大战

[Hindenburg-ludendorff.jpg](https://zh.wikipedia.org/wiki/File:Hindenburg-ludendorff.jpg "fig:Hindenburg-ludendorff.jpg")
(坐者) 与鲁登道夫。\]\]
[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，鲁登道夫首先被任命为德军第二军的副参谋长，成为[卡尔·馮·比洛的部下](../Page/卡尔·馮·比洛.md "wikilink")，负责攻陷[列日的炮台](../Page/列日.md "wikilink")，协助实行施里芬计划。成功后，他被调配到[东普鲁士与第八军司令](../Page/东普鲁士.md "wikilink")[保罗·冯·兴登堡共事](../Page/保罗·冯·兴登堡.md "wikilink")。兴登堡得到鲁登道夫与[马克斯·荷夫曼相助](../Page/马克斯·荷夫曼.md "wikilink")，成功在[坦南堡戰役与](../Page/坦南堡戰役.md "wikilink")[马祖尔湖战役击败俄军](../Page/马祖尔湖战役.md "wikilink")。

1916年8月，参谋部总长[馮·法爾根漢辞职](../Page/馮·法爾根漢.md "wikilink")，由兴登堡接任并掌管[最高陆军指挥](../Page/最高陆军指挥.md "wikilink")；鲁登道夫为陸軍總監（Generalquartiermeister），并与上司合作无间，鲁氏一跃成为德军的重要领导人。鲁登道夫大力支持[无限制潜艇战](../Page/无限制潜艇战.md "wikilink")，最后激发[美国在](../Page/美国.md "wikilink")1917年参战。

鲁登道夫与兴登堡的所谓[第三最高陆军指挥](../Page/第三最高陆军指挥.md "wikilink")，其实是军事与工业之[独裁](../Page/独裁.md "wikilink")，将德皇[威廉二世排出于统治阶层之外](../Page/威廉二世_\(德國\).md "wikilink")。他们操控内政，甚至能够迫使高级官员辞职，包括三度撤换[总理](../Page/总理.md "wikilink")，以及对国家的新官任命有否决权。

1916年，第三陆军最高指挥特意让[列宁秘密乘坐火车返回俄国](../Page/列宁.md "wikilink")。结果，一如鲁登道夫所愿，1917年[俄罗斯发生](../Page/俄罗斯.md "wikilink")[革命](../Page/俄国二月革命.md "wikilink")，东线战事缓和。1918年，鲁登道夫与[列宁议和](../Page/列宁.md "wikilink")，并签订有利德国的[布列斯特-立陶夫斯克條約](../Page/布列斯特-立陶夫斯克條約.md "wikilink")。同年，鲁氏成为西线统帅，发动多次进攻（[皇帝會戰](../Page/皇帝會戰.md "wikilink")），企图突围而出，但仍未能突破战线。美军的猛烈攻势令德军节节败退，令鲁氏十分慌张。陆军最高指挥在9月29日把权力交还德皇，直到皇帝被迫退位为止。鲁登道夫自己则逃到[瑞典](../Page/瑞典.md "wikilink")。

## 对战争的反思及未来展望

流亡期间，他写了不少文章与书籍，渲染德军的辉煌战绩，更发表著名的[刀刺在背传说](../Page/刀刺在背传说.md "wikilink")，宣称德军被[左派政客出卖](../Page/左派.md "wikilink")：他们以[革命之刀](../Page/德国革命.md "wikilink")，插入德军之背，最终令他们战败。不过，鲁氏本人其实也要为德国的战败负责。鲁登道夫认为德国所打的是防卫战争，也指出[威廉二世未能在战时组织有效的反](../Page/威廉二世_\(德国\).md "wikilink")[宣传与有效领导国家战斗](../Page/宣传.md "wikilink")。他强烈谴责社会民主分子与左派出卖德国，尤其是接受[凡尔赛条约](../Page/凡尔赛条约.md "wikilink")。另一方面，他又注意战时国内经济环境，宣称尤其以犹太人为主的商人借着操控生产与金融事业牟利，而非为国家着想。他研究德国在战争末段时的罢工，发觉“国内战线”比德军阵线还要早崩溃，让国内动荡影响前线军士的士气，触发很多士兵“暂时休假”。更重要的是，鲁氏觉得德国人低估了战争对他们的威胁：他断定[三国协约的战争目的就是要打败与肢解德国](../Page/三国协约.md "wikilink")。

鲁登道夫曾写道：“德国人以[德国之革命](../Page/德国革命.md "wikilink")，使其变为众国中之贱民—无以赢得盟友，只得为外国人与他们的资本为奴为婢，尊严尽丧。廿载之内，德国人将会咒骂那些为发动革命而自豪的人。”（我的战争回忆，1914-1918年）此话在后来似乎实现了。

## 战後的生活

[Bundesarchiv_Bild_102-16742,_Erich_Ludendorff_mit_Adolf_Hitler.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_102-16742,_Erich_Ludendorff_mit_Adolf_Hitler.jpg "fig:Bundesarchiv_Bild_102-16742,_Erich_Ludendorff_mit_Adolf_Hitler.jpg")
1920年，鲁登道夫以[右翼政客的身份重返德国](../Page/右翼.md "wikilink")。1923年，他企图协助[希特勒发动](../Page/希特勒.md "wikilink")[啤酒馆政变](../Page/啤酒馆政变.md "wikilink")，结果失败，但在之後的審判中被判無罪。1924年，他代表[纳粹党在国会获得议席](../Page/纳粹党.md "wikilink")，任期至1928年。1925年，他参加总统选举，但败于以前的上司[兴登堡](../Page/保羅·馮·興登堡.md "wikilink")。

纳粹党夺权之前，[魏玛共和国计划把鲁登道夫与几位出名将领送到](../Page/魏玛共和国.md "wikilink")[中国](../Page/中国.md "wikilink")，协助[国民党重整](../Page/中國國民黨.md "wikilink")[国民革命军](../Page/国民革命军.md "wikilink")。但由于《[凡尔赛条约](../Page/凡尔赛条约.md "wikilink")》的限制，以及将领非常知名、不宜受聘于人，故计划中断。\[1\]

1928年後，鲁登道夫与纳粹党不和，最后他退出政坛。对于世界的问题，他断定一切都是[基督徒](../Page/基督徒.md "wikilink")、[犹太人与](../Page/犹太人.md "wikilink")[共济会成员的错](../Page/共济会.md "wikilink")。他与第二任妻子创立了一个隐秘的会社（），会社到今天仍然存在。有传言指，他晚年时思想奇怪。1935年，希特勒想让他担任陆军元帅，但被拒绝；1937年，鲁氏逝世，获得国葬的礼待；希特勒出席了葬礼。

## 传记

  - 埃里希·魯登道夫将军自传 *My War Memoirs, 1914-1918*. 2v. ("Meine
    Kriegserinnerungen 1914-1918", written in Sweden, 1919).
  - Donald James Goodspeed, *[Ludendorff: Genius Of World War
    I](http://www.epwbooks.com/search.php?field=serial&q=8054862)*,
    Boston, Houghton Mifflin, 1966.
  - John Lee, *[The Warlords: Hindenburg And
    Ludendorff](https://web.archive.org/web/20051029215548/http://www.orionbooks.co.uk/HB-29877/The-Warlords.htm)*
    (Great Commanders S.)
  - Robert B Asprey, *The German High Command at War: Hindenburg and
    Ludendorff and the First World War*, Time Warner, 1994.

## 引用

## 外部連結

  - [埃里希·魯登道夫](http://www.firstworldwar.com/bio/ludendorff.htm)
    Firstworldwar.com 的 Who's Who
  - [Ludendorff by H. L.
    Mencken](http://www.theatlantic.com/issues/17jun/mencken.htm)
    published in the June 1917 edition of the *Atlantic Monthly*
  - [埃里希·魯登道夫的生平](https://web.archive.org/web/20060430082641/http://www.spartacus.schoolnet.co.uk/FWWludendorff.htm)
    From Spartacus Educational
  - [鲁登道夫和他的总体战](http://www.sspanzer.net/world_war_1/heer/Ludendorff/Ludendorff.htm)
  - [鲁登道夫的战争理论](http://www.warstudy.com/general/first_ww/ludendorff/ludendorff.xml)

[Category:第一次世界大戰](../Category/第一次世界大戰.md "wikilink")
[Category:德國軍事人物](../Category/德國軍事人物.md "wikilink")
[Category:普魯士將軍](../Category/普魯士將軍.md "wikilink")
[Category:波森人](../Category/波森人.md "wikilink")

1.  改派退役校級軍官以「工業顧問」身份擔任，而非「軍事顧問」，此即[德国顾问团](../Page/德国顾问团.md "wikilink")，《[俞大維傳](../Page/俞大維.md "wikilink")》.作者：[李元平](../Page/李元平.md "wikilink");出版社：[臺灣日報社](../Page/臺灣日報.md "wikilink");
    出版日：1992年01月05日，392 頁;
    ，[01](http://books.google.com.tw/books/about/%E4%BF%9E%E5%A4%A7%E7%B6%AD%E5%82%B3.html?id=PGHemgEACAAJ&redir_esc=y)，[02](http://books.google.com.tw/books/about/%E4%BF%9E%E5%A4%A7%E7%B6%AD%E5%82%B3.html?id=VqxoAAAAIAAJ&redir_esc=y)，[增訂版](http://www.lib.hcu.edu.tw/Webpac2/store.dll/?ID=24348&T=0)