[Citybus_Route_5P_(last_ride).JPG](https://zh.wikipedia.org/wiki/File:Citybus_Route_5P_\(last_ride\).JPG "fig:Citybus_Route_5P_(last_ride).JPG")

**城巴5X線**是[香港島一條專利](../Page/香港島.md "wikilink")[巴士路線](../Page/巴士.md "wikilink")，來往[堅尼地城及](../Page/堅尼地城.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")（[威非路道](../Page/威非路道.md "wikilink")）。

## 歷史

  - 1997年5月19日：5X線投入服務，取代原有城巴5M（堅尼地城←→金鐘站）的服務，初時來往堅尼地城及灣仔碼頭，以配合[四號幹線](../Page/四號幹線.md "wikilink")（[林士街天橋及](../Page/林士街天橋.md "wikilink")[干諾道西天橋](../Page/干諾道西天橋.md "wikilink")）通車，只於平日繁忙時間服務。
  - 1999年8月22日：5X線改為每日由早上至黃昏服務。
  - 2004年5月31日：[城巴](../Page/城巴.md "wikilink")[新巴西區路線重組](../Page/新巴.md "wikilink")，此路線\[1\]
      - 改為只在平日早上提供由單向往[盧押道](../Page/盧押道.md "wikilink")，以及下午繁忙時間由[修頓球場單向往](../Page/修頓球場.md "wikilink")的服務；
      - 往[盧押道方向繞經](../Page/盧押道.md "wikilink")[石塘咀一段](../Page/石塘咀.md "wikilink")[德輔道西及](../Page/德輔道西.md "wikilink")[水街](../Page/水街.md "wikilink")，並增停德輔道西近[嘉安街之車站](../Page/嘉安街.md "wikilink")，原位於干諾道西電車廠外的巴士站取消。
  - 2008年8月18日：提升為每日全日服務，並延長至銅鑼灣（[威非路道](../Page/威非路道.md "wikilink")），來回程改經[林士街天橋](../Page/林士街天橋.md "wikilink")。\[2\]\[3\]
  - 2010年8月16日：5X線（往堅尼地城方向）於夜間增加班次，每日銅鑼灣（威非路道）開出的尾班車延長至00:00。
  - 2011年8月1日：5X線取消繞經[中環（交易廣場）巴士總站並改經](../Page/中環（交易廣場）巴士總站.md "wikilink")[康樂廣場](../Page/康樂廣場.md "wikilink")，逢星期一至六往銅鑼灣（威非路道）方向的頭班車提早至06:45開出\[4\]。
  - 2015年5月10日：因應[港鐵](../Page/港鐵.md "wikilink")[西港島綫通車而作出的西區巴士路線重組計劃](../Page/港島綫西延.md "wikilink")，5X線及5P線作出以下改動\[5\]：
      - 5X線銅鑼灣每日頭班車提早至06:00開出
      - 5P線取消服務
  - 2015年9月13日：往堅尼地城方向，於抵達電氣道後改經[永興街及英皇道後返回原有路線](../Page/永興街.md "wikilink")，不再途經[帆船街](../Page/帆船街.md "wikilink")、[銀幕街及](../Page/銀幕街.md "wikilink")[琉璃街](../Page/琉璃街.md "wikilink")，並改停永興街站\[6\]。
  - 2016年3月20日：往銅鑼灣方向加停[干諾道西](../Page/干諾道西.md "wikilink")[中山紀念公園分站](../Page/中山紀念公園.md "wikilink")\[7\]。

## 服務時間及班次

| [堅尼地城開](../Page/堅尼地城.md "wikilink") | [銅鑼灣](../Page/銅鑼灣.md "wikilink")（[威非路道](../Page/威非路道.md "wikilink")）開 |
| ----------------------------------- | --------------------------------------------------------------------- |
| **日期**                              | **服務時間**                                                              |
| 星期一至五                               | 06:45-08:05                                                           |
| 08:05-09:05                         | 15                                                                    |
| 09:17                               | 09:57                                                                 |
| 09:30-16:15                         | 15                                                                    |
| 16:15-22:15                         | 20                                                                    |
|                                     | 18:40-21:40                                                           |
| 21:40-00:00                         | 20                                                                    |
| 星期六                                 | 06:45-08:05                                                           |
| 08:05-09:35                         | 15                                                                    |
| 09:35-22:15                         | 20                                                                    |
| 星期日及公眾假期                            | 08:00-16:20                                                           |
| 16:40                               |                                                                       |
| 16:55-22:15                         | 20                                                                    |

## 收費

全程：$4.4

  - 國際金融中心2期往銅鑼灣（威非路道）：$3.4

## 行車路線

**堅尼地城開**經：[西寧街](../Page/西寧街.md "wikilink")、[域多利道](../Page/域多利道.md "wikilink")、[加多近街](../Page/加多近街.md "wikilink")、[吉席街](../Page/吉席街.md "wikilink")、[堅彌地城海旁](../Page/堅彌地城海旁.md "wikilink")、[德輔道西](../Page/德輔道西.md "wikilink")、[水街](../Page/水街.md "wikilink")、[干諾道西](../Page/干諾道西.md "wikilink")、[林士街天橋](../Page/林士街天橋.md "wikilink")、[民寶街](../Page/民寶街.md "wikilink")、[民光街](../Page/民光街.md "wikilink")、[民耀街](../Page/民耀街.md "wikilink")、[康樂廣場](../Page/康樂廣場.md "wikilink")、[干諾道中](../Page/干諾道中.md "wikilink")、[夏慤道](../Page/夏慤道.md "wikilink")、[紅棉路支路](../Page/紅棉路.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[怡和街](../Page/怡和街.md "wikilink")、[高士威道](../Page/高士威道.md "wikilink")、[興發街](../Page/興發街.md "wikilink")、[歌頓道](../Page/歌頓道.md "wikilink")、[電氣道及](../Page/電氣道.md "wikilink")[威非路道](../Page/威非路道.md "wikilink")。

**銅鑼灣（威非路道）開**經：威非路道、興發街、歌頓道、電氣道、[永興街](../Page/永興街.md "wikilink")、[英皇道](../Page/英皇道.md "wikilink")、[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")、[摩頓臺](../Page/摩頓臺.md "wikilink")、高士威道、[伊榮街](../Page/伊榮街.md "wikilink")、[邊寧頓街](../Page/邊寧頓街.md "wikilink")、怡和街、軒尼詩道、金鐘道、*[德輔道中](../Page/德輔道中.md "wikilink")、[雪廠街](../Page/雪廠街.md "wikilink")*（-{[皇后大道中](../Page/皇后大道中.md "wikilink")}-、[畢打街](../Page/畢打街.md "wikilink")）、干諾道中、林士街天橋、[干諾道西天橋](../Page/干諾道西天橋.md "wikilink")、[城西道](../Page/城西道.md "wikilink")、[西祥街北](../Page/西祥街北.md "wikilink")、[西祥街](../Page/西祥街.md "wikilink")、[卑路乍街](../Page/卑路乍街.md "wikilink")、域多利道及西寧街。

  -
    星期日及公眾假期的班次不經*斜體字*之路段，改經皇-{后}-大道中及畢打街。

### 沿線車站

[Citybus5XRtMap.png](https://zh.wikipedia.org/wiki/File:Citybus5XRtMap.png "fig:Citybus5XRtMap.png")

| [堅尼地城開](../Page/堅尼地城.md "wikilink") | [銅鑼灣](../Page/銅鑼灣.md "wikilink")（[威非路道](../Page/威非路道.md "wikilink")）開 |
| ----------------------------------- | --------------------------------------------------------------------- |
| **序號**                              | **車站名稱**                                                              |
| 1                                   | [堅尼地城](../Page/堅尼地城.md "wikilink")                                    |
| 2                                   | [西市街](../Page/西市街.md "wikilink")                                      |
| 3                                   | [爹核士街](../Page/爹核士街.md "wikilink")                                    |
| 4                                   | [卑路乍灣公園](../Page/卑路乍灣公園.md "wikilink")                                |
| 5                                   | [西祥街](../Page/西祥街.md "wikilink")                                      |
| 6                                   | [歌連臣街](../Page/歌連臣街.md "wikilink")                                    |
| 7                                   | \-{[皇后大道西](../Page/皇后大道西.md "wikilink")}-                             |
| 8                                   | [山道](../Page/山道_\(香港\).md "wikilink")                                 |
| 9                                   | [嘉安街](../Page/嘉安街.md "wikilink")                                      |
| 10                                  | [水街](../Page/水街.md "wikilink")                                        |
| 11                                  | [中山紀念公園](../Page/中山紀念公園.md "wikilink")                                |
| 12                                  | [國際金融中心二期](../Page/國際金融中心_\(香港\).md "wikilink")                       |
| 13                                  | [郵政總局](../Page/香港郵政總局.md "wikilink")                                  |
| 14                                  | [大會堂](../Page/香港大會堂.md "wikilink")                                    |
| 15                                  | [金鐘站](../Page/金鐘站.md "wikilink")                                      |
| 16                                  | [盧押道](../Page/盧押道.md "wikilink")                                      |
| 17                                  | [史釗域道](../Page/史釗域道.md "wikilink")                                    |
| 18                                  | [杜老誌道](../Page/杜老誌道.md "wikilink")                                    |
| 19                                  | [崇光百貨](../Page/香港崇光百貨.md "wikilink")                                  |
| 20                                  | [維多利亞公園](../Page/維多利亞公園.md "wikilink")                                |
| 21                                  | [興發街](../Page/興發街.md "wikilink")                                      |
| 22                                  | 銅鑼灣（威非路道）                                                             |
|                                     | 21                                                                    |

  - 逢星期日及公眾假期不停有 \* 號之車站，改停有 ^ 之車站。

## 使用狀況

本線雖然取道[林士街天橋往返](../Page/林士街天橋.md "wikilink")[西區](../Page/西區.md "wikilink")，但特快效果有限（只限[中環至](../Page/中環.md "wikilink")[西祥街及](../Page/西祥街.md "wikilink")[水街至](../Page/水街.md "wikilink")[中環](../Page/中環.md "wikilink")），再加上收費較、及線高出一元及班次較疏落，因此客量稍低於上述路線。

雖然特快效果有限，但於港鐵西港島綫通車前，本線肩負起往返西區的主幹路線角色，不少西區居民選擇本線前往[中環接駁鐵路](../Page/中環.md "wikilink")，能避免[上環一帶塞車](../Page/上環.md "wikilink")，繁忙時間客量不俗，昔日城巴經常使用載客量達140人的「[水塘豪](../Page/富豪奧林比安.md "wikilink")」（511-535）行走本線以應付高企的客量。

港鐵西港島綫通車後，本線班次疏落，行車時間遠比港鐵長，因此客量跌幅較大，非繁忙時間從西區出發的班次不時吉車遊街（尤其是城巴於2016年11月起派出12.8米巴士行走本線更明顯見其客量跌幅高，加上班次疏落，選乘1號或較頻密5B、10線甚至已到達[上環](../Page/上環.md "wikilink")，令本線優勢下降），但從[銅鑼灣出發的班次偶爾亦有乘客趕時間](../Page/銅鑼灣.md "wikilink")「見車就上」，不計較本線收費較高，亦有不少不趕時間的西區居民仍選擇本線，因此西行班次客量仍有一定保證。另外，假日路面較暢順，若能成功登上本線，行車時間仍與[港鐵匹敵](../Page/港鐵.md "wikilink")，因此假日客量尚算可觀。

根據歷年相關文件中的資料顯示，本線客量數據如下：

  - 2013年：由堅尼地城開出，上午繁忙時段載客率為65-85%，平均為73%；由銅鑼灣開出，上午繁忙時段載客率為18-59%，平均為34%。
  - 2014年：繁忙時段的平均載客率約93%，非繁忙時段的平均載客率約54%。
  - 2015年：於港鐵西港島綫通車後，最繁忙半小時的平均載客率為55%，客量比通車前的85%下跌30%\[8\]。
  - 2016年：由銅鑼灣開出，上午繁忙時段最高載客率為66%；下午則為42%。該方向近八成乘客於[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[盧押道以東車站登車](../Page/盧押道.md "wikilink")。

西港島綫通車後，本線客量錄得較大幅度下跌，主要為流失以前從[堅尼地城一帶前往中環站或金鐘站轉乘港鐵的乘客](../Page/堅尼地城.md "wikilink")，以及受到西港島綫車程較本線快捷等因素影響。

## 參考資料及注釋

  - 《二十世紀港島區巴士路線發展史》，容偉釗編著，BSI出版
  - 《香港島巴士路線與社區發展》，ISBN：9789888310067，40頁，出版商：中華書局(香港)有限公司，出版日期：2014年11月

## 外部連結

  - 城巴

<!-- end list -->

  - [城巴5X線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=5X&route=5X&routetype=D&company=5&exactMatch=yes)
  - [城巴5X線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-CTB-5X-5X-D.pdf)

<!-- end list -->

  - 其他

<!-- end list -->

  - [681巴士總站－城巴5X線](http://www.681busterminal.com/hk-5x.html)

[005X](../Category/城巴及新世界第一巴士路線.md "wikilink")
[005X](../Category/中西區巴士路線.md "wikilink")
[005X](../Category/灣仔區巴士路線.md "wikilink")

1.  [城巴新巴路線重組宣傳單張](https://web.archive.org/web/20041122103214/http://www.nwfb.com.hk/pdf/Leaflet_CV.pdf)
2.  [城巴乘客通告：路線5X延長路線及全日雙向服務](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/14082008-CB.pdf)
3.  [城巴5X號線全日特快來往堅尼地城、中環及銅鑼灣](http://www.nwstbus.com.hk/tc/uploadedFiles/~PressRelease_TC/14082008-chi.pdf)，城巴新聞稿，2008年8月14日。
4.  [城巴5X號線實施新服務安排](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/28072011-CB_1.pdf)，2011年7月28日，城巴／新巴
5.  [城巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/Poster/WIE0501CTB.pdf)
6.  [城巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/evs_notice/C201501830a02.pdf)
7.  [城巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/evs_notice/C201600509a02.pdf)
8.  [配合西港島線通車的公共交通服務之最新修訂重組計劃](http://www.districtcouncils.gov.hk/central/doc/common/committee_meetings_doc/ttc/20150129-TTC-Paper-2-2015.pdf)，2015年1月。