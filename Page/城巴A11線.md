**城巴A11線**是[香港機場巴士路線](../Page/香港機場巴士路線.md "wikilink")，來往[北角碼頭及](../Page/北角碼頭.md "wikilink")[機場](../Page/香港國際機場.md "wikilink")\[1\]\[2\]，駛經港島北岸核心商業區包括中環、銅鑼灣。

**城巴NA11線**則是香港一條機場巴士路線，為A11線的通宵版本，但總站是設在[港珠澳大橋香港口岸](../Page/港珠澳大橋香港口岸.md "wikilink")，繞經機場客運大樓。全程$52，並與NA29線列為全香港現時收費第二貴的通宵路線及專營巴士線，其次是A10，全程$48。

## 簡介及歷史

**A11線**於1998年7月6日起隨[新機場啟用而開辦](../Page/香港國際機場.md "wikilink")。本線最初只來往銅鑼灣（摩頓台）和機場之間，同年12月20日遷往[天后地鐵站](../Page/天后站.md "wikilink")，但由於[中環](../Page/中環.md "wikilink")[德輔道中及](../Page/德輔道中.md "wikilink")[皇后大道中一帶經常交通擠塞](../Page/皇后大道中.md "wikilink")，令行車時間加長，令乘客改乘昂貴但更快捷的[機場快綫前往機場](../Page/機場快綫.md "wikilink")，引致乘客不斷流失。

因此城巴在2001年4月2日重組A11及[A12線](../Page/城巴A12線.md "wikilink")，本路線由天后地鐵站延長至北角碼頭，填補A12在[北角的服務](../Page/北角.md "wikilink")，A12線往機場方向改為直往中環，不停[金鐘](../Page/金鐘.md "wikilink")，往港島方向則改行[干諾道中](../Page/干諾道中.md "wikilink")，希望提升乘客量。

為配合[昂船洲大橋於](../Page/昂船洲大橋.md "wikilink")2009年12月20日通車，本路線由2010年1月31日起，來回程改經昂船洲大橋及[南灣隧道](../Page/南灣隧道.md "wikilink")，不經[長青公路](../Page/長青公路.md "wikilink")、[長青隧道及](../Page/長青隧道.md "wikilink")[青葵公路](../Page/青葵公路.md "wikilink")，成為首批行經昂船洲大橋的專利巴士路線之一；不過現時車長會因應封路措施（例如[青嶼幹線往青衣方向上層封閉](../Page/青嶼幹線.md "wikilink")、限制整體高度超過1.6米的車輛使用昂船洲大橋等）取道長青公路、長青隧道及青葵公路往來機場。

由2012年12月19日起，原於[A10線及](../Page/城巴A10線.md "wikilink")[A29線提供的網上版](../Page/城巴A29線.md "wikilink")「城巴機場快線抵站時間查詢服務」，擴展至所有城巴機場快線（包括本路線），方便乘客查詢巴士預計到達時間，該服務可兼容智能手機或平板電腦等裝置\[3\]。

2016年7月16日，新增路線NA11\[4\]，行車路線與A11線相同，惟往機場方向加停[琴行街分站](../Page/琴行街.md "wikilink")，方便在機場工作的香港島居民。

2018年10月24日：A11線來回繞經[港珠澳大橋香港口岸](../Page/港珠澳大橋.md "wikilink")，並與
A10、A12、A20、A26 及 A29P
號線增設八達通轉乘優惠；NA11線總站延長至港珠澳大橋香港口岸，往北角碼頭方向班次增至兩班，開出時間分別為00:40及01:00。\[5\]

2018年11月27日：A11線往北角碼頭方向，過港珠澳大橋香港口岸後，改經[順朗路往北大嶼山公路](../Page/順朗路.md "wikilink")。\[6\]

## 服務時間及班次

  - A11

| [北角碼頭開](../Page/北角碼頭.md "wikilink") | [機場（地面運輸中心）開](../Page/香港國際機場.md "wikilink") |
| ----------------------------------- | ------------------------------------------- |
| **日期**                              | **服務時間**                                    |
| 每日                                  | 05:10                                       |
| 05:30-06:45                         | 15                                          |
| 06:45-21:05                         | 20                                          |
| 21:30-22:30                         | 30                                          |
| 23:30                               | 00:30                                       |

  - NA11

<!-- end list -->

  - [北角碼頭開](../Page/北角碼頭.md "wikilink")：04:50
  - [港珠澳大橋香港口岸開](../Page/港珠澳大橋香港口岸.md "wikilink")：00:40、01:00

## 收費

### A11

全程：$40

  - [西區海底隧道收費廣場往](../Page/西區海底隧道.md "wikilink")[機場地面運輸中心](../Page/香港國際機場.md "wikilink")：$33
  - [青嶼幹線收費廣場往機場地面運輸中心](../Page/青嶼幹線.md "wikilink")：$17
  - [一號客運大樓往機場地面運輸中心](../Page/香港國際機場一號客運大樓.md "wikilink")：$4
  - 青嶼幹線收費廣場往[北角碼頭](../Page/北角碼頭巴士總站.md "wikilink")：$34
  - 西區海底隧道收費廣場往北角碼頭：$19
  - 過西區海底隧道後往北角碼頭：$6.5

### NA11

全程：$52

  - [青嶼幹線收費廣場往港珠澳大橋香港口岸](../Page/青嶼幹線.md "wikilink")：$40
  - [一號客運大樓往港珠澳大橋香港口岸](../Page/香港國際機場一號客運大樓.md "wikilink")：$10
  - 過西區海底隧道後往北角碼頭：$20

### 預售來回車票

來回車票售價$65（購買日起三個月內有效，適用於[A10](../Page/城巴A10線.md "wikilink")、A11、[A12](../Page/城巴A12線.md "wikilink")、[A26](../Page/城巴A26線.md "wikilink")、[A29或](../Page/城巴A29線.md "wikilink")[A29P任何往市區車程及往機場車程各一次](../Page/城巴A29P線.md "wikilink")），車票不適用於NA11線

乘客登車時需將指定方向的車票投入車上錢箱內。乘客可在[城巴新巴顧客服務中心](../Page/城巴新巴顧客服務中心.md "wikilink")、中環新渡輪碼頭或指定旅行社購買預售來回車票\[7\]。

### 八達通即日來回優惠

凡使用八達通卡乘搭城巴機場快線，即日來回機場（或[青嶼幹線收費廣場](../Page/青嶼幹線收費廣場.md "wikilink")）及市區的乘客，可獲回程半價優惠。乘客必須在同一服務日內（即頭班車至尾班車的服務時間內），以同一張八達通卡繳付去程及回程車資，方可享有回程半價優惠。回程優惠適用於任何一條城巴機場快線\[8\]（優惠不適用於NA11線）。

### 機場員工乘車優惠計劃

持「機場員工個人八達通卡」的合資格機場員工，可以用優惠價$27.0乘搭A11線，或$38.0乘搭NA11線。有關乘客需在登車時向車長出示「機場員工個人八達通卡」，並以該卡繳付車費。\[9\]

### 巴士轉乘計劃

乘客登上本線後指定時間內以同一張八達通卡轉乘以下路線，或從以下路線登車後指定時間內以同一張八達通卡轉乘本線，次程可獲車資折扣優惠（優惠不適用於NA11線）：

  - A11←→720/720A/720P、722，次程可獲$1.5折扣優惠

<!-- end list -->

  - **A11往北角** → 722往耀東邨
  - 720/720A/720P或722往中環 → **A11往機場**

<!-- end list -->

  - A11←→780、788，次程可獲$1.2折扣優惠

<!-- end list -->

  - **A11往北角** → 780往柴灣或788往小西灣
  - 780或788往中環 → **A11往機場**

<!-- end list -->

  - A11←→跑馬地／寶馬山路線

<!-- end list -->

  - **A11往北角** → 1往跑馬地、11往渣甸山、25A或27往寶馬山，次程車資全免
  - 1往堅尼地城、11/511往中環、25A往灣仔或27往北角 → **A11往機場**，回贈首程車資

<!-- end list -->

  - A11←→半山區路線

<!-- end list -->

  - **A11往北角** → 12、12A或12M往半山區，次程車資全免
  - 12、12A或12M往中環／金鐘 → **A11往機場**，回贈首程車資

<!-- end list -->

  - A11←→15，次程可獲$5.4折扣優惠

<!-- end list -->

  - **A11往北角** → 15往山頂
  - 15往中環 → **A11往機場**

<!-- end list -->

  - A11←→南區路'線

<!-- end list -->

  - **A11往北角** →
    6/6A/6X、260往赤柱、37A、37B/37X往置富、40M往華富、70往香港仔、75往深灣、90往鴨脷洲邨、90B往海怡半島或97往利東邨，次程車資全免
  - 6/6A/6X、260、37A、37B/37B、40M、70/70P、75、90、90B或97往中環／金鐘 →
    **A11往機場**，回贈首程車資

<!-- end list -->

  - A11往機場 → 973往赤柱，次程車資全免

<!-- end list -->

  - A11←→城巴北大嶼山路線

<!-- end list -->

  - **A11往機場** →
    [E11/E11A](../Page/城巴E11、E11A線.md "wikilink")、[E21](../Page/城巴E21線.md "wikilink")、[E21A](../Page/城巴E21A線.md "wikilink")、[E22](../Page/城巴E22線.md "wikilink")/[E22P](../Page/城巴E22P線.md "wikilink")/[E22X](../Page/城巴E22X線.md "wikilink")、[E22A或](../Page/城巴E22A線.md "wikilink")[E23往東涌市中心](../Page/城巴E23線.md "wikilink")／逸東／機場博覽館，次程車資全免
  - E11/E11A往天-{后}-站 → **A11往北角**，次程可獲$21折扣優惠
  - E21往大角咀 → **A11往北角**，次程可獲$14折扣優惠
  - E22/E22P/E22X往藍田／油塘 → **A11往北角**，次程可獲$18折扣優惠
  - E22A往將軍澳 → **A11往北角**，次程可獲$24折扣優惠
  - **A11往北角** →
    E11/E11A、E21、E21A/E21X、E22/E22P/E22X、E22A或E23往港島／九龍，次程車資全免
  - E11/E11A、E21、E21A/E21X、E22/E22P/E22X、E22A或E23往港島／九龍 →
    **A11往北角**，只需補付本線全程車費與首程車費之差額

<!-- end list -->

  - A11←→R8

<!-- end list -->

  - **A11任何方向** → R8往迪士尼，次程車資全免
  - R8往青嶼幹線繳費廣場 → **A11往北角**，次程可獲$7折扣優惠
  - R8往青嶼幹線繳費廣場 → **A11往機場**，次程可獲$1折扣優惠

<!-- end list -->

  - A11、A21、A22、A29→A10、A12、A20、A26、A29P，於青嶼幹線繳費廣場進行轉乘

<!-- end list -->

  - **A11、A21、A22、A29往市區** → A10、A12、A20、A26、A29P往市區，次程免費

<!-- end list -->

  - A10、A12、A20、A26、A29P→A11、A21、A22、A29，於機場(2號客運大樓)進行轉乘

<!-- end list -->

  - A10、A12、A20、A26、A29P往機場 → **A11、A21、A22、A29往機場**，次程免費
  - A11**往北角**→W1往**高鐵西九龍站**,次程可獲$1折扣優惠

## 使用車輛

A11線開辦至今均採用行走[城巴機場快線的客車版](../Page/城巴機場快線.md "wikilink")[Dennis
Trident](../Page/丹尼士三叉戟三型.md "wikilink")（2100-2161），隨著該批巴士年事已高，已達超過15年車齡，城巴於2012年訂購66輛新一代[亞歷山大丹尼士Enviro
500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")（命名為[Enviro 500
MMC](../Page/Enviro_500_MMC.md "wikilink")）12米客車版低地台雙層空調巴士（8000-8065）以替代客車版Dennis
Trident及臨時改裝為機場版本的亞歷山大丹尼士Enviro
500客車版（8203-8207）。首4輛同款巴士已於2013年3月出牌，其中兩輛於2013年3月29日於本線首航。2016年12月，12.8米客車版Enviro
500MMC（68xx）投入本線運營。

## 行車路線

**北角碼頭開**經：[琴行街](../Page/琴行街.md "wikilink")、[英皇道](../Page/英皇道.md "wikilink")、[高士威道](../Page/高士威道.md "wikilink")、[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")、[摩頓臺](../Page/摩頓臺.md "wikilink")、告士打道天橋、[告士打道、內告士打道](../Page/告士打道.md "wikilink")、[夏愨道](../Page/夏愨道.md "wikilink")、[-{干}-諾道中、-{干}-諾道西](../Page/干諾道.md "wikilink")、[西區海底隧道](../Page/西區海底隧道.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、^[青沙公路](../Page/青沙公路.md "wikilink")、^[昂船洲大橋](../Page/昂船洲大橋.md "wikilink")、^[南灣隧道](../Page/南灣隧道.md "wikilink")、^[長青公路](../Page/長青公路.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[青嶼幹線](../Page/青嶼幹線.md "wikilink")、[北大嶼山公路](../Page/北大嶼山公路.md "wikilink")、[機場路](../Page/機場路_\(香港\).md "wikilink")、機場路、暢航路、翔天路、機場南交匯處、暢連路、航天城交匯處、赤鱲角路、順暉路、順匯路、赤鱲角路、航天城路、暢連路、機場南交匯處及暢連路

  -
    ^若昂船洲大橋限制整體高度超過1.6米的車輛通過或青嶼幹線上層封閉，則改經[青葵公路](../Page/青葵公路.md "wikilink")、[長青橋及](../Page/長青橋.md "wikilink")[長青隧道](../Page/長青隧道.md "wikilink")。

**機場（地面運輸中心）開**經：暢連路、機場南交匯處、暢連路、航天城交匯處、赤鱲角路、順暉路、順匯路、赤鱲角路、順朗路、北大嶼山公路、青嶼幹線、青衣西北交匯處、^南灣隧道、^昂船洲大橋、^青沙公路、西九龍公路、西區海底隧道、-{干}-諾道西、-{干}-諾道中、[民吉街](../Page/民吉街.md "wikilink")、[統一碼頭道](../Page/統一碼頭道.md "wikilink")、民吉街、干諾道中、夏愨道、[紅棉路支路](../Page/紅棉路.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[怡和街](../Page/怡和街.md "wikilink")、高士威道、[興發街](../Page/興發街.md "wikilink")、[歌頓道](../Page/歌頓道.md "wikilink")、[電氣道](../Page/電氣道.md "wikilink")、[渣華道](../Page/渣華道.md "wikilink")、[書局街及](../Page/書局街.md "wikilink")[海港道](../Page/海港道.md "wikilink")。

  -
    ^若青嶼幹線上層封閉或昂船洲大橋限制整體高度超過1.6米的車輛通過，則改經長青隧道、長青橋及青葵公路。

### 沿線車站

[CityflyerA11RtMap.png](https://zh.wikipedia.org/wiki/File:CityflyerA11RtMap.png "fig:CityflyerA11RtMap.png")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/北角碼頭.md" title="wikilink">北角碼頭開</a></p></th>
<th><p><a href="../Page/香港國際機場.md" title="wikilink">機場（地面運輸中心）開</a>（A11）<br />
港珠澳大橋香港口岸開（NA11）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>序號</strong></p></td>
<td><p><strong>車站名稱</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/北角碼頭.md" title="wikilink">北角碼頭</a></p></td>
</tr>
<tr class="odd">
<td><p>2*</p></td>
<td><p><a href="../Page/琴行街.md" title="wikilink">琴行街</a></p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/美麗閣.md" title="wikilink">美麗閣</a></p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/新時代廣場_(北角).md" title="wikilink">新時代廣場</a></p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/炮台山站.md" title="wikilink">炮台山站</a></p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><a href="../Page/清風街.md" title="wikilink">清風街</a></p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><a href="../Page/皇仁書院.md" title="wikilink">皇仁書院</a></p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="../Page/中華游樂會.md" title="wikilink">中華游樂會</a></p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/京士頓街.md" title="wikilink">京士頓街</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><a href="../Page/景隆街.md" title="wikilink">景隆街</a></p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p><a href="../Page/伊利莎伯大廈.md" title="wikilink">伊利莎伯大廈</a></p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p><a href="../Page/史釗域道.md" title="wikilink">史釗域道</a></p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><a href="../Page/柯布連道.md" title="wikilink">柯布連道</a></p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><a href="../Page/分域街.md" title="wikilink">分域街</a></p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><a href="../Page/皇后像廣場.md" title="wikilink">皇后像廣場</a></p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p><a href="../Page/砵典乍街.md" title="wikilink">砵典乍街</a></p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p><a href="../Page/租庇利街.md" title="wikilink">租庇利街</a></p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p><a href="../Page/林士街.md" title="wikilink">林士街</a></p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p><a href="../Page/皇后街.md" title="wikilink">皇后街</a></p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>西區海底隧道收費廣場</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>青嶼幹線繳費廣場</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p><a href="../Page/香港國際機場一號客運大樓.md" title="wikilink">機場（1號客運大樓）</a></p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p><a href="../Page/香港國際機場二號客運大樓.md" title="wikilink">機場（2號客運大樓）</a></p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>港珠澳大橋旅檢大樓</p></td>
</tr>
<tr class="even">
<td><p>25^</p></td>
<td><p>機場（地面運輸中心）</p></td>
</tr>
</tbody>
</table>

有 \* 之車站祇限NA11線停靠

有 ^ 之車站祇限NA11線不停靠

有 \# 之車站祇限NA11線對調停靠

## 參考資料

  - 《二十世紀巴士路線發展史－渡海、離島、機場篇》，ISBN 9789628414680，BSI(香港)

## 外部連結

  - [城巴A11線官方路線資料](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=A11&route=A11&routetype=D&company=5&exactMatch=yes)
  - [城巴A11線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-CTB-A11-A11-D.pdf)
  - [城巴NA11線路線圖](https://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-CTB-NA11-NA11-N.pdf)
  - [681巴士總站 - 城巴機場快線A11線](http://www.681busterminal.com/a11.html)
  - [681巴士總站 - 城巴機場快線NA11線](http://www.681busterminal.com/na11.html)

## 相關條目

  - [城巴T11線](../Page/城巴T11線.md "wikilink")
  - [城巴E11線](../Page/城巴E11線.md "wikilink")

[A11](../Category/城巴及新世界第一巴士路線.md "wikilink")
[A11](../Category/機場巴士路線.md "wikilink")
[A11](../Category/香港東區巴士路線.md "wikilink")
[A11](../Category/離島區巴士路線.md "wikilink")

1.  <http://www.citybus.com.hk/chi/RouSer/RouteSearch/busroute_info.asp?route=A11&routetype=D&company=5>
2.
3.  [城巴機場快線抵站時間查詢服務](http://eta.nwst.com.hk/)
4.  <http://www.hkitalk.net/HKiTalk2/forum.php?mod=viewthread&tid=968371>
5.  [城巴機場快線連接港珠澳大橋香港口岸](http://www.nwstbus.com.hk/tc/uploadedPressRelease/11243_19102018_z_chi.pdf)
6.  [港珠澳大橋香港口岸機場巴士服務安排](https://www.td.gov.hk/filemanager/en/content_13/THS2/ta%20on%20pt%20arrangments%20iro%20commissioning%20of%20hzmb%20\(chi\).pdf)
7.  [「城巴機場快線」預售來回車票](http://www.nwstbus.com.hk/fareConcession/concession_detail.aspx?id=52&intLangID=2)，城巴網站
8.  [城巴機場快線即日來回優惠](http://www.nwstbus.com.hk/fareConcession/concession_detail.aspx?id=2&intLangID=2)，城巴網站
9.  [機場乘車指南](http://www.nwstbus.com.hk/_common/images/photo/Airport_Community_Travel_Guide.pdf)，城巴網站