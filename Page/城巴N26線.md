**城巴N26線**是[香港的一條通宵巴士路線](../Page/香港.md "wikilink")，來往[東涌站及](../Page/東涌站_\(東涌綫\).md "wikilink")[油塘](../Page/油塘.md "wikilink")。

## 歷史

  - 1999年12月20日：配合機場通宵路線重組而投入服務，初時由[廣田邨單程往](../Page/廣田邨.md "wikilink")[東涌站](../Page/東涌站.md "wikilink")
  - 2000年8月14日：增設深夜往廣田邨的回程服務，來回程並繞經[竹園邨](../Page/竹園邨.md "wikilink")
  - 2003年4月22日：延長至[油塘](../Page/油塘.md "wikilink")

## 服務時間（詳細班次）

  - 東涌站開：00:20
  - 油塘開：04:30、05:00、05:25

## 收費

全程：$23（機場員工優惠票價：$20）

  - [青嶼幹線收費廣場往](../Page/青嶼幹線.md "wikilink")[油塘](../Page/油塘.md "wikilink")：$20
  - [畢架山花園往油塘](../Page/畢架山花園.md "wikilink")：$12
  - [清水灣道往油塘](../Page/清水灣道.md "wikilink")：$8
  - 青嶼幹線收費廣場往[東涌站](../Page/東涌站_\(東涌綫\).md "wikilink")：$13
  - [暢運路](../Page/暢運路.md "wikilink")（二號閘）往東涌站：$5

## 行車路線

**東涌站開**經：[達東路](../Page/達東路.md "wikilink")、[順東路](../Page/順東路.md "wikilink")、[赤鱲角南路](../Page/赤鱲角南路.md "wikilink")、[駿坪路](../Page/駿坪路.md "wikilink")、[駿運路](../Page/駿運路.md "wikilink")、[駿運路交匯處](../Page/駿運路交匯處.md "wikilink")、[航膳東路](../Page/航膳東路.md "wikilink")、駿運路交匯處、[觀景路](../Page/觀景路.md "wikilink")、[東岸路](../Page/東岸路.md "wikilink")、[機場路](../Page/機場路_\(香港\).md "wikilink")、[暢連路](../Page/暢連路.md "wikilink")、[機場（地面運輸中心）巴士總站](../Page/機場（地面運輸中心）巴士總站.md "wikilink")、暢連路、[暢達路](../Page/暢達路.md "wikilink")、[機場北交匯處](../Page/機場北交匯處.md "wikilink")、機場路、[機場南交匯處](../Page/機場南交匯處.md "wikilink")、機場路、[北大嶼山公路](../Page/北大嶼山公路.md "wikilink")、[青嶼幹線](../Page/青嶼幹線.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、支路、[呈祥道](../Page/呈祥道.md "wikilink")、[龍翔道](../Page/龍翔道.md "wikilink")、[馬仔坑道](../Page/馬仔坑道.md "wikilink")、[竹園道](../Page/竹園道.md "wikilink")、[雅竹街](../Page/雅竹街.md "wikilink")、[黃大仙道](../Page/黃大仙道.md "wikilink")、[沙田坳道](../Page/沙田坳道.md "wikilink")、[鳳德道](../Page/鳳德道.md "wikilink")、[龍蟠街](../Page/龍蟠街.md "wikilink")、[大磡道](../Page/大磡道.md "wikilink")、龍翔道、[清水灣道](../Page/清水灣道.md "wikilink")、[新清水灣道](../Page/新清水灣道.md "wikilink")、[順清街](../Page/順清街.md "wikilink")、[順景街](../Page/順景街.md "wikilink")、[利安道](../Page/利安道.md "wikilink")、[順安道](../Page/順安道.md "wikilink")、[秀茂坪道](../Page/秀茂坪道.md "wikilink")、[秀明道](../Page/秀明道.md "wikilink")、[曉光街](../Page/曉光街.md "wikilink")、秀茂坪道、天橋、[連德道](../Page/連德道.md "wikilink")、[碧雲道](../Page/碧雲道.md "wikilink")、[高超道](../Page/高超道.md "wikilink")、[欣榮街](../Page/欣榮街.md "wikilink")、[茶果嶺道及高超道](../Page/茶果嶺道.md "wikilink")。

**油塘開**經：高超道、碧雲道、連德道、天橋、秀茂坪道、曉光街、秀明道、秀茂坪道、順安道、[順天巴士總站](../Page/順天巴士總站.md "wikilink")、利安道、新清水灣道、清水灣道、龍翔道、[蒲崗村道](../Page/蒲崗村道.md "wikilink")、鳳德道、沙田坳道、黃大仙道、雅竹街、竹園道、龍翔道、呈祥道、支路、青葵公路、長青隧道、長青公路、青衣西北交匯處、青嶼幹線、北大嶼山公路、機場路、暢連路、暢達路、機場北交匯處、機場路、機場南交匯處、機場路、東岸路、觀景路、[駿明路](../Page/駿明路.md "wikilink")、觀景路、駿運路交匯處、航膳東路、駿運路交匯處、駿運路、駿坪路、赤鱲角南路、順東路及達東路。

### 沿線車站

[N26_RtMap.png](https://zh.wikipedia.org/wiki/File:N26_RtMap.png "fig:N26_RtMap.png")

| [東涌站開](../Page/東涌站_\(東涌綫\).md "wikilink") | [油塘開](../Page/油塘.md "wikilink")               |
| ----------------------------------------- | --------------------------------------------- |
| **序號**                                    | **車站名稱**                                      |
| 1                                         | [東涌站](../Page/東涌站_\(東涌綫\).md "wikilink")      |
| 2                                         | [赤鱲角南路](../Page/赤鱲角南路.md "wikilink")          |
| 3                                         | [亞洲空運中心](../Page/亞洲空運中心.md "wikilink")        |
| 4                                         | [機場空運中心](../Page/機場空運中心.md "wikilink")        |
| 5                                         | [香港空運貨站](../Page/超級一號貨站.md "wikilink")        |
| 6                                         | [赤鱲角消防局](../Page/香港消防局列表#新界西南區.md "wikilink") |
| 7                                         | [國泰空廚](../Page/國泰空廚.md "wikilink")            |
| 8                                         | [國泰城](../Page/國泰城.md "wikilink")              |
| 9                                         | 二號檢查閘                                         |
| 10                                        | 機場（地面運輸中心）                                    |
| 11                                        | [暢達路](../Page/暢達路.md "wikilink")              |
| 12                                        | [富豪機場酒店](../Page/富豪機場酒店.md "wikilink")        |
| 13                                        | [青嶼幹線收費廣場](../Page/青嶼幹線收費廣場.md "wikilink")    |
| 14                                        | [畢架山花園](../Page/畢架山.md "wikilink")            |
| 15                                        | [豐力樓](../Page/豐力樓.md "wikilink")              |
| 16                                        | [天宏苑](../Page/天宏苑.md "wikilink")              |
| 17                                        | [浸信會天虹小學](../Page/浸信會天虹小學.md "wikilink")      |
| 18                                        | [竹園邨](../Page/竹園邨.md "wikilink")              |
| 19                                        | [竹園南邨趣園樓](../Page/竹園南邨.md "wikilink")         |
| 20                                        | [美德樓](../Page/美德樓.md "wikilink")              |
| 21                                        | [鑽石山站](../Page/鑽石山站.md "wikilink")            |
| 22                                        | [牛池灣村](../Page/牛池灣村.md "wikilink")            |
| 23                                        | [牛池灣街市](../Page/牛池灣市政大廈.md "wikilink")        |
| 24                                        | 彩雲邨白虹樓                                        |
| 25                                        | [基順小學](../Page/中華基督教會基順學校.md "wikilink")      |
| 26                                        | 順利邨利康樓                                        |
| 27                                        | 順安邨安逸樓                                        |
| 28                                        | 順天邨天韻樓                                        |
| 29                                        | 秀茂坪邨秀樂樓                                       |
| 30                                        | 秀茂坪商場                                         |
| 31                                        | 秀茂坪邨秀安樓                                       |
| 32                                        | 秀茂坪邨秀明樓                                       |
| 33                                        | 上秀茂坪                                          |
| 34                                        | 興田邨                                           |
| 35                                        | 德田邨德敬樓                                        |
| 36                                        | 德田邨德樂樓                                        |
| 37                                        | 德田邨德隆樓                                        |
| 38                                        | [廣田邨廣靖樓](../Page/廣田邨.md "wikilink")           |
| 39                                        | [廣田商場](../Page/廣田邨#廣田商場.md "wikilink")        |
| 40                                        | 高俊苑                                           |
| 41                                        | 高怡邨                                           |
| 42                                        | [油塘中心](../Page/油塘中心.md "wikilink")            |
| 43                                        | 油塘                                            |
|                                           | 44                                            |
| 45                                        | [東涌纜車站](../Page/東涌站_\(昂坪360\).md "wikilink")  |
| 46                                        | 東涌站                                           |

## 參考資料

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，ISBN 9789628414680，BSI (香港)，157頁

## 外部連結

  - [Allnight Citybus Route
    城巴通宵路線－N26](http://www.681busterminal.com/n26.html)

[N026](../Category/城巴及新世界第一巴士路線.md "wikilink")
[N026](../Category/離島區巴士路線.md "wikilink")
[N026](../Category/觀塘區巴士路線.md "wikilink")
[N026](../Category/機場巴士路線.md "wikilink")