[Shinjuku_2006-02-22_a.jpg](https://zh.wikipedia.org/wiki/File:Shinjuku_2006-02-22_a.jpg "fig:Shinjuku_2006-02-22_a.jpg")經濟圈\]\]
[Manhattan3_amk.jpg](https://zh.wikipedia.org/wiki/File:Manhattan3_amk.jpg "fig:Manhattan3_amk.jpg")是世界上最大的都市之一\]\]
[Shangai_at_dusk.jpg](https://zh.wikipedia.org/wiki/File:Shangai_at_dusk.jpg "fig:Shangai_at_dusk.jpg")[浦東地區](../Page/浦東.md "wikilink")\]\]
-{**城**、**市**、**都**、**城市**或**都市**}-是為[人口较為稠密](../Page/人口.md "wikilink")、[工](../Page/工業.md "wikilink")[商業較為發達的](../Page/商業.md "wikilink")[地区](../Page/地区.md "wikilink")，一般包括了[住宅区](../Page/住宅区.md "wikilink")、[工业区和](../Page/工业区.md "wikilink")[商业区等機能分區](../Page/商业区.md "wikilink")，并且具备行政管辖功能。城市的行政管辖功能可能涉及较其本身更广泛的区域。城市中有[楼房](../Page/建築.md "wikilink")、[街道和](../Page/街道.md "wikilink")[公園等](../Page/公園.md "wikilink")[基礎建設](../Page/基礎建設.md "wikilink")。

一般而言城市會有較完善的公共卫生設備、[公用事業](../Page/公用事業.md "wikilink")、土地規劃、住宅及[運輸系統](../Page/運輸.md "wikilink")。密集的開發方便人們的互動，也便於商業活動的進行。[大都市一般會有對應的](../Page/大都市.md "wikilink")[郊區及](../Page/郊區.md "wikilink")[卧城](../Page/卧城.md "wikilink")。大都市一般也都有其[市区](../Page/市区.md "wikilink")，許多人住在郊區，每天通勤到市区上班。若一個城市已擴展到其他都市接壤，此一區域就會形成[集合城市](../Page/集合城市.md "wikilink")。

## 定义

城市并没有一个明确的定义和标准，不同国家对城市的标准也不同，以下列举几例：

###

[丹麦的城市人口下限为](../Page/丹麦.md "wikilink")250人。

###

[加拿大的城市人口下限为](../Page/加拿大.md "wikilink")1000人。

###

[德国的城市人口下限为](../Page/德国.md "wikilink")2000人。

###

[美国城市的定義是由各州法律而定](../Page/美国.md "wikilink")，因此各州也會有不同的城市人口下限，例如[麻薩諸塞州的城市人口下限是](../Page/麻薩諸塞州.md "wikilink")12000人，而[馬里蘭州的下限則是](../Page/馬里蘭州.md "wikilink")300人\[1\]。

###

[印度的城市人口下限为](../Page/印度.md "wikilink")5000人。

###

[马来西亚的城市人口下限为](../Page/马来西亚.md "wikilink")10000人。

###

[中国](../Page/中华人民共和国.md "wikilink")：2006年3月10日，中國[国家统计局发布](../Page/国家统计局.md "wikilink")《关于统计上划分城乡的暂行规定》，对“城镇”和“鄉村”進行了新的划分。其中第四条规定，「城镇」是指在中国市镇建制和行政区划基础上，经该规定划定的区域。“城镇”又包括“城区”和“镇区”。第五条规定，城区是指在市辖区和不设区的市中，经该规定划定的区域。

  -
    根据该规定，
    城区包括：
    1.  街道办事处所辖的居民委员会地域；
    2.  城市公共设施、居住设施等连接到的其他居民委员会地域和村民委员会地域。镇区是指在城区以外的镇和其他区域中，经该规定划定的区域。

<!-- end list -->

  -
    镇区包括：
    1.  镇所辖的居民委员会地域；
    2.  镇的公共设施、居住设施等连接到的村民委员会地域；
    3.  常住人口在3000人以上独立的工矿区、开发区、科研单位、大专院校、[农场](../Page/农场.md "wikilink")、[林场等特殊区域](../Page/林场.md "wikilink")。

<!-- end list -->

  -
    乡村是指：以上划定的城镇以外的其他区域\[2\]。

###

[台灣的都市定義如下](../Page/台灣.md "wikilink")：

:\#[人口數](../Page/人口.md "wikilink")25000人以上;

:\#[人口密度](../Page/人口密度.md "wikilink")1000人/平方公里以上;

:\#居民有60％以上從事第[二](../Page/第二產業.md "wikilink")、[三級產業](../Page/第三產業.md "wikilink")。

###

[澳大利亚最少要有](../Page/澳大利亚.md "wikilink")10000至30000人才為城市。

###

[白俄羅斯的](../Page/白俄羅斯.md "wikilink")[憲法把城市分為三種](../Page/白俄羅斯憲法.md "wikilink")\[3\]：[首都](../Page/首都.md "wikilink")，城市地區的人口超過50000人的[Oblast州](../Page/Oblast.md "wikilink")，以及城市地區的人口超過6000人的[Raion地區](../Page/Raion.md "wikilink")。

## 形成及演進

城市是[人类文明的重要组成部分](../Page/人类文明.md "wikilink")，城市也是伴随人类文明与进步发展起来的。普遍認為，真正意義上的城市是工商業發展的產物。

### 起源

最早的城市起源於大約一萬年前的[中東](../Page/中東.md "wikilink")。在那之前人類過著散居的生活，以捕獵及游牧維生。後來進入了[农耕时代](../Page/农耕时代.md "wikilink")，人类才开始[定居](../Page/定居生活.md "wikilink")。位於[兩河流域的](../Page/兩河流域.md "wikilink")[美索不達米亞平原由於土壤肥沃](../Page/美索不達米亞.md "wikilink")、農產過剩，居民無須擔心食物缺乏，因此可以投入製造及發明的行業，城市崛起和[城市文明於是开始传播](../Page/城市文明.md "wikilink")。然而此時城市人口，仍以貴族、統治階級和军事防御、举行祭祀仪式為主，平民無力居住，因而居住、生產功能其次，因此规模不大。

### 中古時代

[中世紀](../Page/中世紀.md "wikilink")[歐洲城市的演進與東方相比相對遲緩](../Page/歐洲.md "wikilink")，因为周围的农村提供的余粮不多，再加上實施[封建制度的緣故](../Page/封建制度.md "wikilink")，每个城市和它控制的农村，构成一个小单位，形成相对封闭、自给自足的莊園經濟，工商業並不發達，以致於城市發展停滯不前。然而當時東方的商業城市卻開始崛起，如[唐朝的](../Page/唐朝.md "wikilink")[長安及](../Page/長安.md "wikilink")[宋朝的](../Page/宋朝.md "wikilink")[汴京](../Page/汴京.md "wikilink")；13世纪[地中海地區的](../Page/地中海.md "wikilink")[義大利半島的](../Page/義大利半島.md "wikilink")[米兰](../Page/米兰.md "wikilink")、[威尼斯](../Page/威尼斯.md "wikilink")、[佛羅倫斯及](../Page/佛羅倫斯.md "wikilink")[拜占庭的](../Page/拜占庭.md "wikilink")[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")，或者[阿拉伯半島的](../Page/阿拉伯半島.md "wikilink")[巴格達等](../Page/巴格達.md "wikilink")。這些都市由於位居交通要衝，不論是[商業](../Page/商業.md "wikilink")、[貿易及](../Page/貿易.md "wikilink")[文化交流均十分興盛](../Page/文化.md "wikilink")，因此得以蓬勃發展。

### 大航海時代

但是1453年君士坦丁堡被[鄂圖曼土耳其人攻陷](../Page/奧斯曼帝國.md "wikilink")，通往[印度及](../Page/印度.md "wikilink")[中國的貿易路線遭到阻斷](../Page/中國.md "wikilink")。由於對[香料及](../Page/香料.md "wikilink")[絲綢等東方商品的需求日益增加](../Page/絲綢.md "wikilink")，再加上對殖民地和財富的渴望，甚至傳教等因素，造成後來[大航海時代的來臨](../Page/地理大发现.md "wikilink")，歐洲經濟中心因此逐漸由地中海移至[大西洋沿岸的](../Page/大西洋.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[英國及](../Page/英國.md "wikilink")[荷蘭的港口城市](../Page/荷蘭.md "wikilink")。
[Urbanisation-degree.png](https://zh.wikipedia.org/wiki/File:Urbanisation-degree.png "fig:Urbanisation-degree.png")

### 工業時代

繼19世紀的[工业革命之后](../Page/工业革命.md "wikilink")，城市获得了前所未有的发展。工業革命造成了[科技快速的進步及許多發明的問世](../Page/科技.md "wikilink")，進而造成許多[工廠的建成](../Page/工廠.md "wikilink")。由于工廠大多設置在交通方便的大都市附近，造成[农民不断涌入這些都市](../Page/农民.md "wikilink")，[城市化的現象也開始由工業革命起源的](../Page/城市化.md "wikilink")[英國向世界各國擴張](../Page/英國.md "wikilink")。到[第一次世界大战前夕](../Page/第一次世界大战.md "wikilink")，[英](../Page/英国.md "wikilink")、[美](../Page/美国.md "wikilink")、[德与](../Page/德国.md "wikilink")[法国等工業強國](../Page/法国.md "wikilink")，都市化的程度都相當之高。这不仅是富足的标志，而且是文明的象征。然而在[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")，[武器的改良及新型態戰爭的出現](../Page/武器.md "wikilink")，遭受戰火波及而死傷的平民數量大幅增加，再加上都市居民為了躲避攻擊，而大舉遷移至鄉村，造成都市人口銳減。

### 戰後至今日

戰後，鄉村人口大量-{回}-流都市，使得都市人口迅速增加，由於市區人口日益擁擠，都市面積便大幅增加，與之附近的市鎮及郊區相連，因此形成了許多大型的[都會區](../Page/都會區.md "wikilink")。之後產業技術的改良及醫療設施的改善，更是加劇了都會區數量及規模的大幅增加，都市化的現象日趨明顯。到了今日，全球已有超過30億的人口居住於都市，然而[歐洲](../Page/歐洲.md "wikilink")、[美國及](../Page/美國.md "wikilink")[日本等已開發國家的都市發展已近飽和狀態](../Page/日本.md "wikilink")；在[拉丁美洲](../Page/拉丁美洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[中國大陸及](../Page/中國大陸.md "wikilink")[印度等地區的都市](../Page/印度.md "wikilink")，人口成長仍然相當迅速。

## 类型

“城市”的提法本身就包含了两方面的含义：“城”为[行政地域的概念](../Page/行政区划.md "wikilink")，即人口的集聚地；“市”为[商业的概念](../Page/商业.md "wikilink")，即[商品交换的场所](../Page/商品交换.md "wikilink")。而最早的“城市”（实际应为我们现在“城镇”）就是因[商品交换集聚人群后而形成的](../Page/商品.md "wikilink")。而**城市**的出现，也同商业的变革有着直接的渊源关系。最初城市中的工业集聚，也是为了使商品交换变得更为容易（可就地加工、就地销售）而形成的。在城市中直接加工销售相对于将已加工好的商品拿到城市中来交换而言，则正是一种随着工业城市的出现而产生的一种商业变革。城市包括城市规模、城市功能、城市布局和城市交通，而这几方面所发生的变化，都必然地会对城市的商业活动带来影响，促使其发生相应的变革。

[城市经济学对城市作了不同等级的分类](../Page/城市经济学.md "wikilink")，如[小城市](../Page/小城市.md "wikilink")、[中等城市](../Page/中等城市.md "wikilink")、[大城市](../Page/大城市.md "wikilink")、国际化大都市、[世界級城市等](../Page/世界級城市.md "wikilink")，对城市能级分类的一个标准是人口的规模。
按城市[综合经济实力和世界城市发展的](../Page/综合经济实力.md "wikilink")[历史来看](../Page/历史.md "wikilink")，城市分为集市型、功能型、综合性、城市群等类别，这些类别也是城市发展的各个阶段。任何城市都必须经过集市型阶段。

### 集市型城市

属于周边[农民或](../Page/农民.md "wikilink")[手工业者商品交换的集聚地](../Page/手工业.md "wikilink")，商业主要由[交易市场](../Page/交易市场.md "wikilink")、[商店和](../Page/商店.md "wikilink")[旅馆](../Page/旅馆.md "wikilink")、[饭店等配套服务设施所构成](../Page/饭店.md "wikilink")。处于集市型阶段的城市在[中国稱](../Page/中国.md "wikilink")[集镇](../Page/集镇.md "wikilink")。

### 功能型城市

通过[自然资源的开发和优势](../Page/自然资源.md "wikilink")[产业的集中](../Page/产业.md "wikilink")，开始发展其特有的工业产业，从而使城市具有特定的功能。不仅是商品的交换地，同时也是商品的生产地。但城市因产业分工而形成的功能单调，对其他地区和城市经济交流的依赖增强，商业开始由封闭型的城内交易为主转为开放性的城际交易为主，批发贸易业有了很大的发展。这类型城市主要有工业重镇、[旅游城市](../Page/旅游城市.md "wikilink")（觀光城市）等；

### 综合型城市

一些[地理位置优越和产业优势明显的](../Page/地理位置.md "wikilink")[城市经济功能趋于综合型](../Page/城市经济.md "wikilink")，[金融](../Page/金融.md "wikilink")、[贸易](../Page/贸易.md "wikilink")、[服务](../Page/服务.md "wikilink")、[文化](../Page/文化.md "wikilink")、[娱乐等功能得到发展](../Page/娱乐.md "wikilink")，城市的集聚力日益增强，从而使城市的经济能级大大提高，成为区域性、全国性甚至国际性的经济中心和贸易中心（“大都市”）。商业由单纯的商品交易向综合服务发展，商业活动也扩展延伸为促进商品流通和满足交易需求的一切活动。这类城市在[中国比较典型的有](../Page/中国.md "wikilink")[直辖市](../Page/直辖市.md "wikilink")、[省会城市](../Page/省会城市.md "wikilink")。

### [城市群](../Page/城市群.md "wikilink")

或城市帶、都市區、都市圈等。城市的经济功能已不再是在一个孤立的城市体现，而是由以一个中心城市为核心，同与其保持着密切经济联系的一系列中小城市共同组成的城市群来体现了。如[美国東岸的](../Page/美国.md "wikilink")“[波士頓-華盛頓城市帶](../Page/波士頓-華盛頓城市帶.md "wikilink")”、美国西岸的「[舊金山-聖迭戈城市帶](../Page/舊金山-聖迭戈城市帶.md "wikilink")」和[美國中西部的](../Page/美國中西部.md "wikilink")「[芝加哥-匹茲堡城市帶](../Page/芝加哥-匹茲堡城市帶.md "wikilink")」，[日本的](../Page/日本.md "wikilink")[东京](../Page/东京.md "wikilink")、[大阪](../Page/大阪市.md "wikilink")、[名古屋三大](../Page/名古屋.md "wikilink")[城市圈](../Page/城市圈.md "wikilink")，[台灣的](../Page/台灣.md "wikilink")“[台北](../Page/台北.md "wikilink")-[高雄市](../Page/高雄市.md "wikilink")”城市带（[台灣西部走廊](../Page/台灣西部走廊.md "wikilink")），[英国的](../Page/英国.md "wikilink")“[伦敦](../Page/伦敦.md "wikilink")-[利物浦](../Page/利物浦.md "wikilink")”城市带等。放眼中國，[香港所在的](../Page/香港.md "wikilink")[珠江三角洲地区和](../Page/珠江三角洲.md "wikilink")[上海所在的](../Page/上海.md "wikilink")[长江三角洲地区实际上也正在形成兩个经济关系密切的](../Page/长江三角洲.md "wikilink")[珠江三角洲城市群和](../Page/珠江三角洲城市群.md "wikilink")[长江三角洲城市群](../Page/长江三角洲城市群.md "wikilink")，其整体的经济功能已在日益凸现。

## 城市的规划与布局

都市規劃係相關單位為了避免都市發展過快造成的[都市問題而實施的計畫](../Page/都市問題.md "wikilink")，如管線設計、道路網規劃、劃分區域等等。
都市規劃大致上可分為[總體規劃及](../Page/城市总体规划.md "wikilink")[詳細規劃兩種](../Page/城市详细规划.md "wikilink")。

### 總體規劃

總體規劃，是指都市性質、發展方向、規模大小等都市「總體佈局」的規劃，一般以20年為規劃期。總體規劃之下又可分為數期的「近期建設」，乃總體規劃的組成階段，規劃期一般為5年。

都市總體規劃最有名的例子，有如[美國的](../Page/美國.md "wikilink")[華盛頓特區及](../Page/華盛頓特區.md "wikilink")[巴西的](../Page/巴西.md "wikilink")[巴西利亞](../Page/巴西利亞.md "wikilink")。

### 詳細規劃

相較於總體規劃，詳細規劃是都市規劃更進一步的具體化。主要是針對都市近期建設規劃範圍內的建築物、公共事業（水電）、公園綠地等細部設施作具體的佈置。

## 城市的外部影響

已知現在城市會形成[微氣候](../Page/微氣候.md "wikilink")，這是因為有大塊會吸熱的表面，在陽光照射時溫度會升高，而且有許多雨水會進下水道中，而不是變成地下水的一部份。

[垃圾和](../Page/垃圾.md "wikilink")[污水是城市二個主要的問題](../Page/污水.md "wikilink")，而各種形式燃燒造成的[空氣污染也是城市的問題之一](../Page/空氣污染.md "wikilink")\[4\]，燃燒來源包括壁爐、燒木頭或是煤的爐灶或是、其他加熱系統\[5\]以及[內燃機](../Page/內燃機.md "wikilink")（像汽機車的[引擎](../Page/發動機.md "wikilink")）。城市對其他地區的影響可以用[城市生態足跡來表示](../Page/生態足跡.md "wikilink")。其他負面影響包括[傳染性疾病](../Page/傳染性疾病.md "wikilink")、犯罪、交通流量高以及通勤時間長。在城市中人和人的互動比在鄉村複雜，因此更容易造成傳染性疾病的發生，不過像[疫苗接種](../Page/疫苗.md "wikilink")、水過濾系統等也對健康有幫助。[犯罪也是城市的一個重要問題](../Page/犯罪.md "wikilink")，已有研究指出城市中的犯罪率較高，被逮捕受罰的機率也比較低。以[竊盜而言](../Page/竊盜.md "wikilink")，城市人口的高度集中也造成有更多高價、值得偷竊的物品出現。城市交通流量大，也使得在市區行駛汽車和機車變的不一定便利，可能是因為交通堵塞造成路程時間長，而[停車也是一大問題](../Page/停車.md "wikilink")。

城市也有正面的外部影響，實體距離上的接近有助於，有助於個人和企業交換資訊，產生新的概念\[6\]。厚實的勞動市場也讓企業和個人有更好的技能匹配。人口密度高也有助於分享基礎設施及生產設施，不過若人口密度過高時，擁擠和等待的時間會帶來其他的負面影響\[7\]。
城市的另一個正面外部影響是因為不同背景的人在城市聚在一起，創造了多様化的社會機會。較大的城市有更廣泛的社會關注及活動，因此不同背景的人都可以找到有興趣，想要參與的內容。

## 城市与城市化相關術語

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/居民点.md" title="wikilink">居民点</a></li>
<li>城市（城镇）</li>
<li><a href="../Page/市.md" title="wikilink">市</a></li>
<li><a href="../Page/镇.md" title="wikilink">镇</a></li>
<li><a href="../Page/行政区划.md" title="wikilink">行政区划</a></li>
<li><a href="../Page/城市文化.md" title="wikilink">城市文化</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/城市化.md" title="wikilink">城市化</a></li>
<li><a href="../Page/都市衰退.md" title="wikilink">都市衰退</a></li>
<li><a href="../Page/城市群.md" title="wikilink">城市群</a></li>
<li><a href="../Page/城镇.md" title="wikilink">城镇体系</a></li>
<li><a href="../Page/卫星城.md" title="wikilink">卫星城</a>（卫星城镇）</li>
<li><a href="../Page/首位都市.md" title="wikilink">首位都市</a>：一個規模比同區域其他城市要大很多的城市</li>
</ul></td>
</tr>
</tbody>
</table>

**城市规划**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/城镇体系规划.md" title="wikilink">城镇体系规划</a></li>
<li><a href="../Page/城市规划.md" title="wikilink">城市规划</a></li>
<li><a href="../Page/城市设计.md" title="wikilink">城市设计</a></li>
<li><a href="../Page/城市总体规划纲要.md" title="wikilink">城市总体规划纲要</a></li>
<li><a href="../Page/城市规划区.md" title="wikilink">城市规划区</a></li>
<li><a href="../Page/城市建成区.md" title="wikilink">城市建成区</a></li>
<li><a href="../Page/开发区.md" title="wikilink">开发区</a></li>
<li><a href="../Page/旧城改建.md" title="wikilink">旧城改建</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/城市基础设施.md" title="wikilink">城市基础设施</a></li>
<li><a href="../Page/城市总体规划.md" title="wikilink">城市总体规划</a></li>
<li><a href="../Page/分区规划.md" title="wikilink">分区规划</a></li>
<li><a href="../Page/近期建设规划.md" title="wikilink">近期建设规划</a></li>
<li><a href="../Page/城市详细规划.md" title="wikilink">城市详细规划</a></li>
<li><a href="../Page/控制性详细规划.md" title="wikilink">控制性详细规划</a></li>
<li><a href="../Page/修建性详细规划.md" title="wikilink">修建性详细规划</a></li>
<li><a href="../Page/城市规划管理.md" title="wikilink">城市规划管理</a></li>
</ul></td>
</tr>
</tbody>
</table>

**城市规划编制**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/城市发展战略.md" title="wikilink">城市发展战略</a></li>
<li><a href="../Page/城市职能.md" title="wikilink">城市职能</a></li>
<li><a href="../Page/城市性质.md" title="wikilink">城市性质</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/城市规模.md" title="wikilink">城市规模</a></li>
<li><a href="../Page/城市发展方向.md" title="wikilink">城市发展方向</a></li>
<li><a href="../Page/城市发展目标.md" title="wikilink">城市发展目标</a></li>
</ul></td>
</tr>
</tbody>
</table>

**城市规划管理**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/城市规划法规.md" title="wikilink">城市规划法规</a></li>
<li><a href="../Page/规划审批程序.md" title="wikilink">规划审批程序</a></li>
<li><a href="../Page/城市规划用地管理.md" title="wikilink">城市规划用地管理</a></li>
<li><a href="../Page/选址意见书.md" title="wikilink">选址意见书</a></li>
<li><a href="../Page/建设用地规划许可证.md" title="wikilink">建设用地规划许可证</a></li>
<li><a href="../Page/城市规划建设管理.md" title="wikilink">城市规划建设管理</a></li>
<li><a href="../Page/建设工程规划许可证.md" title="wikilink">建设工程规划许可证</a></li>
<li><a href="../Page/建筑面积密度.md" title="wikilink">建筑面积密度</a>（建蔽率）</li>
<li><a href="../Page/容积率.md" title="wikilink">容积率</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/建筑密度.md" title="wikilink">建筑密度</a></li>
<li><a href="../Page/道路红线.md" title="wikilink">道路红线</a></li>
<li><a href="../Page/建筑红线.md" title="wikilink">建筑红线</a></li>
<li><a href="../Page/人口毛密度.md" title="wikilink">人口毛密度</a></li>
<li><a href="../Page/人口净密度.md" title="wikilink">人口净密度</a></li>
<li><a href="../Page/建筑间距.md" title="wikilink">建筑间距</a></li>
<li><a href="../Page/日照標準.md" title="wikilink">日照標準</a></li>
<li><a href="../Page/城市道路面积率.md" title="wikilink">城市道路面积率</a></li>
<li><a href="../Page/绿地率.md" title="wikilink">绿地率</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 相关条目

  - [各国城市列表](../Page/各国城市列表.md "wikilink")
  - [全球城市](../Page/全球城市.md "wikilink")
  - [中华人民共和国城市列表](../Page/中华人民共和国城市列表.md "wikilink")
      - [中华人民共和国城市建制](../Page/中华人民共和国城市建制.md "wikilink")
      - [2004年中国大陆城市竞争力排名](../Page/2004年中国大陆城市竞争力排名.md "wikilink")
      - [中国大陆百强城市](../Page/中国百强城市.md "wikilink")
      - [中國城市史](../Page/中國城市史.md "wikilink")
  - [日本城市列表](../Page/日本城市列表.md "wikilink")
      - [日本的城](../Page/城_\(日本\).md "wikilink")
  - [台灣城市列表](../Page/台灣城市列表.md "wikilink")
  - [城市建造遊戲](../Page/城市建造遊戲.md "wikilink")
  - [历史上的城市规模](../Page/历史上的城市规模.md "wikilink")
  - [城市化](../Page/城市化.md "wikilink")

## 参考资料

<div class="references" style="font-size:small">

<references/>

</div>

[城市](../Category/城市.md "wikilink")
[Category:城镇](../Category/城镇.md "wikilink")
[Category:城市社區](../Category/城市社區.md "wikilink")
[Category:行政區劃](../Category/行政區劃.md "wikilink")
[Category:依類型劃分的聚居地](../Category/依類型劃分的聚居地.md "wikilink")
[Category:聚居地的種類](../Category/聚居地的種類.md "wikilink")

1.  [Census -
    BureauPlaces](http://www.census.gov/geo/reference/pdfs/GARM/Ch9GARM.pdf)
2.  关于统计上划分城乡的暂行规定．<http://www.stats.gov.cn/tjbz/t20061018_402369828.htm>
     中华人民共和国国家统计局网站
3.
4.
5.
6.
7.