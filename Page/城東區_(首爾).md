[01-04-seongdong-zh.svg](https://zh.wikipedia.org/wiki/File:01-04-seongdong-zh.svg "fig:01-04-seongdong-zh.svg")
**城東區**（）是[大韓民國首都](../Page/大韓民國.md "wikilink")[首爾特別市的一個區](../Page/首爾特別市.md "wikilink")，設立於1943年。

## 歷史

  - 1943年6月10日：設置[京畿道](../Page/京畿道_\(日本統治時代\).md "wikilink")[京城府城東區](../Page/京城府.md "wikilink")，成為京城府7區之一，管轄區域：[上往十里町](../Page/上往十里町.md "wikilink")()、[下往十里町](../Page/下往十里町.md "wikilink")()、[馬場町](../Page/馬場町_\(京城府\).md "wikilink")()、[沙斤町](../Page/沙斤町.md "wikilink")()、[杏堂町](../Page/杏堂町.md "wikilink")()、[鷹峰町](../Page/鷹峰町.md "wikilink")()、[金湖町](../Page/金湖町.md "wikilink")()、[玉水町](../Page/玉水町.md "wikilink")()、[新堂町](../Page/新堂町.md "wikilink")()\[1\]。
  - 1946年9月28日：[首爾特別市城東區設置](../Page/首爾特別市.md "wikilink")，管轄區域：[上往十里洞](../Page/上往十里洞.md "wikilink")()、[下往十里洞](../Page/下往十里洞.md "wikilink")()、[馬場洞](../Page/馬場洞.md "wikilink")()、[沙斤洞](../Page/沙斤洞.md "wikilink")()、[杏堂洞](../Page/杏堂洞.md "wikilink")()、[鷹峰洞](../Page/鷹峰洞.md "wikilink")()、[金湖洞](../Page/金湖洞.md "wikilink")()、[玉水洞](../Page/玉水洞.md "wikilink")()、[新堂洞](../Page/新堂洞.md "wikilink")()。
  - 1949年8月13日：[京畿道](../Page/京畿道.md "wikilink")[高陽郡](../Page/高陽郡.md "wikilink")[纛島面被併入城東區](../Page/纛島面.md "wikilink")\[2\]，而新併入的地區設置城東區纛島出張所。城東區纛島出張所的管轄地域包括有：[西纛島里](../Page/西纛島里.md "wikilink")()、[東纛島里](../Page/東纛島里.md "wikilink")()、[松亭里](../Page/松亭里.md "wikilink")()、[華陽里](../Page/華陽里.md "wikilink")()、[毛陳里](../Page/毛陳里.md "wikilink")()、[陵里](../Page/陵里.md "wikilink")()、[君子-{里}-](../Page/君子里.md "wikilink")()、[中谷里](../Page/中谷里.md "wikilink")()、[面牧里](../Page/面牧里.md "wikilink")()、[九誼里](../Page/九誼里.md "wikilink")()、[廣壯里](../Page/廣壯里.md "wikilink")()、[新川里](../Page/新川里.md "wikilink")()、[蠶室里](../Page/蠶室里.md "wikilink")()、[紫陽里](../Page/紫陽里.md "wikilink")()\[3\]。

| 改編前                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 改編後                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [高陽郡](../Page/高陽郡.md "wikilink")[纛島面的](../Page/纛島面.md "wikilink")[西纛島里](../Page/西纛島里.md "wikilink")()、[東纛島里](../Page/東纛島里.md "wikilink")()、[松亭里](../Page/松亭里.md "wikilink")()、[華陽里](../Page/華陽里.md "wikilink")()、[毛陳里](../Page/毛陳里.md "wikilink")()、[陵里](../Page/陵里.md "wikilink")()、[君子里](../Page/君子里.md "wikilink")()、[中谷里](../Page/中谷里.md "wikilink")()、[面牧里](../Page/面牧里.md "wikilink")()、[九誼里](../Page/九誼里.md "wikilink")()、[廣壯里](../Page/廣壯里.md "wikilink")()、[新川里](../Page/新川里.md "wikilink")()、[蠶室里](../Page/蠶室里.md "wikilink")()、[紫陽里](../Page/紫陽里.md "wikilink")() | 城東區()纛島出張所() [西纛島里](../Page/西纛島里.md "wikilink")()、[東纛島里](../Page/東纛島里.md "wikilink")()、[松亭里](../Page/松亭里.md "wikilink")()、[華陽里](../Page/華陽里.md "wikilink")()、[毛陳里](../Page/毛陳里.md "wikilink")()、[陵里](../Page/陵里.md "wikilink")()、[君子里](../Page/君子里.md "wikilink")()、[中谷里](../Page/中谷里.md "wikilink")()、[面牧里](../Page/面牧里.md "wikilink")()、[九誼里](../Page/九誼里.md "wikilink")()、[廣壯里](../Page/廣壯里.md "wikilink")()、[新川里](../Page/新川里.md "wikilink")()、[蠶室里](../Page/蠶室里.md "wikilink")()、[紫陽里](../Page/紫陽里.md "wikilink")() |
| (沒有變動) [上往十里洞](../Page/上往十里洞.md "wikilink")()、[下往十里洞](../Page/下往十里洞.md "wikilink")()、[馬場洞](../Page/馬場洞.md "wikilink")()、[沙斤洞](../Page/沙斤洞.md "wikilink")()、[杏堂洞](../Page/杏堂洞.md "wikilink")()、[鷹峰洞](../Page/鷹峰洞.md "wikilink")()、[金湖洞](../Page/金湖洞.md "wikilink")1\~4街()、[玉水洞](../Page/玉水洞.md "wikilink")()、[新堂洞](../Page/新堂洞.md "wikilink")()                                                                                                                                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

1949年行政區域改編

  - 1950年：西纛島里、東纛島里改編成為[聖水洞1街](../Page/聖水洞1街.md "wikilink")()、[聖水洞2街](../Page/聖水洞2街.md "wikilink")()，其餘各里分別如下：[松亭里](../Page/松亭里.md "wikilink")()成為了[松亭洞](../Page/松亭洞.md "wikilink")()、[華陽里](../Page/華陽里.md "wikilink")()成為了[華陽洞](../Page/華陽洞.md "wikilink")()、[毛陳里](../Page/毛陳里.md "wikilink")()成為了[毛陳洞](../Page/毛陳洞.md "wikilink")()、[陵里](../Page/陵里.md "wikilink")()成為了[陵洞](../Page/陵洞.md "wikilink")()、[君子-{里}-](../Page/君子里.md "wikilink")()成為了[君子洞](../Page/君子洞.md "wikilink")()、[中谷里](../Page/中谷里.md "wikilink")()成為了[中谷洞](../Page/中谷洞.md "wikilink")()、[面牧里](../Page/面牧里.md "wikilink")()成為了[面牧洞](../Page/面牧洞.md "wikilink")()、[九誼里](../Page/九誼里.md "wikilink")()成為了[九誼洞](../Page/九誼洞.md "wikilink")()、[廣壯里](../Page/廣壯里.md "wikilink")()成為了[廣壯洞](../Page/廣壯洞.md "wikilink")()、[新川里](../Page/新川里.md "wikilink")()成為了[新川洞](../Page/新川洞.md "wikilink")()、[蠶室里](../Page/蠶室里.md "wikilink")()成為了[蠶室洞](../Page/蠶室洞.md "wikilink")()、[紫陽里](../Page/紫陽里.md "wikilink")()成為了[紫陽洞](../Page/紫陽洞.md "wikilink")()。
  - 1959年：下往十里洞的部份分離成為[道詵洞](../Page/道詵洞.md "wikilink")()。
  - 1963年1月1日：京畿道[廣州郡](../Page/廣州市_\(京畿道\).md "wikilink")()等併入城東區，面牧洞被併入[東大門區](../Page/東大門區.md "wikilink")\[4\]。

| 改編前                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | 改編後                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 廣州郡[九川面](../Page/九川面.md "wikilink")\[5\](): [風納里](../Page/風納里.md "wikilink")()、[古德里](../Page/古德里.md "wikilink")()、[下一里](../Page/下一里.md "wikilink")()、[上一里](../Page/上一里.md "wikilink")()、[明逸里](../Page/明逸里.md "wikilink")()、[岩寺里](../Page/岩寺里.md "wikilink")()、[曲橋里](../Page/曲橋里.md "wikilink")()、[城內里](../Page/城內里.md "wikilink")()、[吉里](../Page/吉洞.md "wikilink")()、[遁村-{里}-](../Page/遁村里.md "wikilink")()                                                                                                                                                                                                                                                                                                                                                                                                             | [城東區](../Page/城東區.md "wikilink")() [風納洞](../Page/風納洞.md "wikilink")()、[古德洞](../Page/古德洞.md "wikilink")()、[下一洞](../Page/下一洞.md "wikilink")()、[上一洞](../Page/上一洞.md "wikilink")()、[明逸洞](../Page/明逸洞.md "wikilink")()、[岩寺洞](../Page/岩寺洞.md "wikilink")()、[千戶洞](../Page/千戶洞.md "wikilink")()、[吉洞](../Page/吉洞.md "wikilink")()、[遁村洞](../Page/遁村洞.md "wikilink")()                                                                                                                     |
| 廣州郡 [彥州面](../Page/彥州面.md "wikilink")(): [盤浦里](../Page/盤浦里.md "wikilink")()、[論峴里](../Page/論峴里.md "wikilink")()、[鶴里](../Page/鶴里.md "wikilink")()、[大峙里](../Page/大峙里.md "wikilink")()、[良才里](../Page/良才里.md "wikilink")()、[三成里](../Page/三成里.md "wikilink")()、[新沙里](../Page/新沙里.md "wikilink")()、[狎鷗亭里](../Page/狎鷗亭里.md "wikilink")()、[驛三里](../Page/驛三里.md "wikilink")()、[清潭里](../Page/清潭里.md "wikilink")()、[內谷里](../Page/內谷里.md "wikilink")()、[廉谷里](../Page/廉谷里.md "wikilink")()、[新院里](../Page/新院里.md "wikilink")()                                                                                                                                                                                                                                                                                                          | 城東區\[6\] [開浦洞](../Page/開浦洞.md "wikilink")()、[論峴洞](../Page/論峴洞.md "wikilink")()、[鶴洞](../Page/鶴洞.md "wikilink")()、[大峙洞](../Page/大峙洞.md "wikilink")()、[道谷洞](../Page/道谷洞.md "wikilink")()、[三成洞](../Page/三成洞.md "wikilink")()、[新沙洞](../Page/新沙洞.md "wikilink")()、[狎鷗亭洞](../Page/狎鷗亭洞.md "wikilink")()、[驛三洞](../Page/驛三洞.md "wikilink")()、[清潭洞](../Page/清潭洞.md "wikilink")()、[內谷洞](../Page/內谷洞.md "wikilink")()、[廉谷洞](../Page/廉谷洞.md "wikilink")()、[新院洞](../Page/新院洞.md "wikilink")() |
| 廣州郡 [大旺面](../Page/大旺面.md "wikilink")(): [細谷里](../Page/細谷里.md "wikilink")()、[栗峴里](../Page/栗峴里.md "wikilink")()、[紫谷里](../Page/紫谷里.md "wikilink")()、[水西里](../Page/水西里.md "wikilink")()、[逸院里](../Page/逸院里.md "wikilink")()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 城東區 [細谷洞](../Page/細谷洞.md "wikilink")()、[栗峴洞](../Page/栗峴洞.md "wikilink")()、[紫谷洞](../Page/紫谷洞.md "wikilink")()、[水西洞](../Page/水西洞.md "wikilink")()、[逸院洞](../Page/逸院洞.md "wikilink")()                                                                                                                                                                                                                                                                                              |
| 廣州郡 [中垈面](../Page/中垈面.md "wikilink")(): [巨餘里](../Page/巨餘里.md "wikilink")()、[馬川里](../Page/馬川里.md "wikilink")()、[芳荑里](../Page/芳荑里.md "wikilink")()、[二里](../Page/二里.md "wikilink")()、[梧琴里](../Page/梧琴里.md "wikilink")()、[石村里](../Page/石村里.md "wikilink")()、[松坡里](../Page/松坡里.md "wikilink")()、[三田里](../Page/三田里.md "wikilink")()、[可樂里](../Page/可樂里.md "wikilink")()、[文井-{里}-](../Page/文井里.md "wikilink")()、[長旨里](../Page/長旨里.md "wikilink")()                                                                                                                                                                                                                                                                                                                                                                              | 城東區 [巨餘洞](../Page/巨餘洞.md "wikilink")()、[馬川洞](../Page/馬川洞.md "wikilink")()、[芳荑洞](../Page/芳荑洞.md "wikilink")()、[二洞](../Page/二洞.md "wikilink")()、[梧琴洞](../Page/梧琴洞.md "wikilink")()、[石村洞](../Page/石村洞.md "wikilink")()、[松坡洞](../Page/松坡洞.md "wikilink")()、[三田洞](../Page/三田洞.md "wikilink")()、[可樂洞](../Page/可樂洞.md "wikilink")()、[文井洞](../Page/文井洞.md "wikilink")()、[長旨洞](../Page/長旨洞.md "wikilink")()                                                                              |
| [城東區](../Page/城東區.md "wikilink")()[面牧洞](../Page/面牧洞.md "wikilink")()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | [東大門區](../Page/東大門區.md "wikilink")()[面牧洞](../Page/面牧洞.md "wikilink")()                                                                                                                                                                                                                                                                                                                                                                                                          |
| (沒有變動) [新堂洞](../Page/新堂洞.md "wikilink")()、[上往十里洞](../Page/上往十里洞.md "wikilink")()、[下往十里洞](../Page/下往十里洞.md "wikilink")()、[도선동洞](../Page/도선동洞.md "wikilink")()、[馬場洞](../Page/馬場洞.md "wikilink")()、[沙斤洞](../Page/沙斤洞.md "wikilink")()、[杏堂洞](../Page/杏堂洞.md "wikilink")()、[鷹峰洞](../Page/鷹峰洞.md "wikilink")()、[金湖洞](../Page/金湖洞.md "wikilink")1\~4街()、[玉水洞](../Page/玉水洞.md "wikilink")()、[聖水洞1](../Page/聖水洞1街.md "wikilink")、[2街](../Page/聖水洞2街.md "wikilink")()、[中谷洞](../Page/中谷洞.md "wikilink")()、[陵洞](../Page/陵洞.md "wikilink")()、[九誼洞](../Page/九誼洞.md "wikilink")()、[廣壯洞](../Page/廣壯洞.md "wikilink")()、[紫陽洞](../Page/紫陽洞.md "wikilink")()、[華陽洞](../Page/華陽洞.md "wikilink")()、[毛陳洞](../Page/毛陳洞.md "wikilink")()、[君子洞](../Page/君子洞.md "wikilink")()、[新川洞](../Page/新川洞.md "wikilink")()、[蠶室洞](../Page/蠶室洞.md "wikilink")() |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |

1963年行政區域改編

  - 1963年9月：下往十里洞的部份分離成為[弘益洞](../Page/弘益洞.md "wikilink")()。
  - 1966年：從[新堂洞](../Page/新堂洞.md "wikilink")()中分離出[黃鶴洞](../Page/黃鶴洞.md "wikilink")()和[興仁洞](../Page/興仁洞.md "wikilink")()；從[下往十里洞](../Page/下往十里洞.md "wikilink")()中分離出[舞鶴洞](../Page/舞鶴洞.md "wikilink")()。
  - 1975年10月1日：城東區在[漢江以南的地域](../Page/漢江_\(韓國\).md "wikilink")，包括：明逸洞、下一洞、
    古德洞、上一洞、吉洞、遁村洞、岩寺洞、城内洞、风纳洞、千户洞、松坡洞、石村洞、三田洞、可乐洞、夷洞、梧琴洞、芳夷洞、文井洞、长旨洞、巨馀洞、马川洞、蚕室洞、新川洞、逸院洞、水西洞、紫谷洞、栗岘洞、细谷洞、浦二洞、开普洞、道谷洞、论岘洞、新沙洞、鹤洞、狎鸥亭洞、清潭洞、三成洞、大峙洞、廉谷洞、内谷洞、新院洞、盘浦洞、蚕院洞、瑞草洞、良才洞、牛眠洞、院趾洞從城東區分離，成立新的[江南區](../Page/江南區_\(首爾\).md "wikilink")\[7\]。
  - 1995年3月1日：老遊1洞、老遊2洞、華陽洞、君子洞、中谷1洞、中谷2洞、中谷3洞、中谷4洞、陵洞、毛陳洞、九誼1洞、九誼2洞、廣壯洞、紫陽1洞、紫陽2洞、紫陽3洞從城東區分離，成立[廣津區](../Page/廣津區.md "wikilink")。
  - 2004年5月1日：區廳移往[往十里站前](../Page/往十里站.md "wikilink")。

## 交通

### 鐵路

  - [韓國鐵道公社](../Page/韓國鐵道公社.md "wikilink")

<!-- end list -->

  - ：[玉水站](../Page/玉水站_\(首爾\).md "wikilink") -
    [鷹峰站](../Page/鷹峰站.md "wikilink") -
    [往十里站](../Page/往十里站.md "wikilink")

  - ：[往十里站](../Page/往十里站.md "wikilink") -
    [首爾森林站](../Page/首爾森林站.md "wikilink")

<!-- end list -->

  - [首爾交通公社](../Page/首爾交通公社.md "wikilink")

<!-- end list -->

  - 本線：[上往十里站](../Page/上往十里站.md "wikilink") -
    [往十里站](../Page/往十里站.md "wikilink")（） -
    [漢陽大站](../Page/漢陽大站.md "wikilink") -
    [纛島站](../Page/纛島站.md "wikilink") -
    [聖水站](../Page/聖水站.md "wikilink")

      - 聖水支線：[聖水站](../Page/聖水站.md "wikilink") -
        [龍踏站](../Page/龍踏站.md "wikilink") -
        [新踏站](../Page/新踏站.md "wikilink")

  - ：[金湖站](../Page/金湖站_\(首爾\).md "wikilink") -
    [玉水站](../Page/玉水站_\(首爾\).md "wikilink")

  - ：[新金湖站](../Page/新金湖站.md "wikilink") -
    [杏堂站](../Page/杏堂站.md "wikilink") -
    [往十里站](../Page/往十里站.md "wikilink")（） -
    [馬場站](../Page/馬場站_\(首爾\).md "wikilink") -
    [踏十里站](../Page/踏十里站.md "wikilink")

## 參考資料

## 外部連結

  - [城東區網站](http://www.gangdong.go.kr/pub/chn/)

[首爾城東區](../Category/首爾城東區.md "wikilink")

1.  參照[朝鮮總督府令
    第163號](../Page/:s:ko:조선총독부령_제163호.md "wikilink")(1943年6月9日公佈)。

2.  시·도의관할구역및구·군의명칭·위치·관할구역변경의건 (제정 1949년 8월 13일
    [대통령령](../Page/大統領令.md "wikilink") 159호)에 의함.

3.  서울시구출장소설치에관한건 (제정 [1949년](../Page/1949년.md "wikilink") [8월
    13일](../Page/8월_13일.md "wikilink") 대통령령 160호)에 의함.

4.  서울특별시·도·군·구의관할구역변경에관한법률 (제정 [1962년](../Page/1962년.md "wikilink")
    [11월 21일](../Page/11월_21일.md "wikilink") 법률 1172호)에 의함.

5.  <http://k.daum.net/qna/view.html?qid=0D557&page=1&focus_on=restQnA&returl=list.html%3Fcategory_id%3DQFA%26status%3D1&sort=recC>

6.
7.  大統領令第7816號