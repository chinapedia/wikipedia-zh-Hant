[MtDavisWhiteHouse.jpg](https://zh.wikipedia.org/wiki/File:MtDavisWhiteHouse.jpg "fig:MtDavisWhiteHouse.jpg")
[University_of_Chicago_Hong_Kong_Campus_Main_Entrance.jpg](https://zh.wikipedia.org/wiki/File:University_of_Chicago_Hong_Kong_Campus_Main_Entrance.jpg "fig:University_of_Chicago_Hong_Kong_Campus_Main_Entrance.jpg")
[University_of_Chicago_Hong_Kong_Campus.jpg](https://zh.wikipedia.org/wiki/File:University_of_Chicago_Hong_Kong_Campus.jpg "fig:University_of_Chicago_Hong_Kong_Campus.jpg")
**域多利道扣押中心**\[1\]，俗稱**域多利拘留所**、**白屋**，位於[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[摩星嶺](../Page/摩星嶺_\(香港\).md "wikilink")[域多利道及](../Page/域多利道.md "wikilink")[摩星嶺徑交匯處](../Page/摩星嶺徑.md "wikilink")，曾經是[皇家香港警務處](../Page/皇家香港警務處.md "wikilink")[政治部的](../Page/政治部_\(香港警察\).md "wikilink")[拘留所](../Page/拘留所.md "wikilink")。

## 歷史

域多利道扣押中心於1950年代初期落成，佔用了[銀禧炮台的部份遺址](../Page/銀禧炮台.md "wikilink")，佔地1,573平方米，有4幢兩層高[建築物](../Page/建築物.md "wikilink")，初時作為[駐港英軍](../Page/駐港英軍.md "wikilink")[皇家工程兵](../Page/皇家工程兵.md "wikilink")（Royal
Engineers
）[會所](../Page/會所.md "wikilink")。於1950年代末期開始，政治部開始接管此座建築物。當時被懷疑在香港從事[間諜活動](../Page/間諜.md "wikilink")、或者意圖推翻[英國殖民地政府的](../Page/香港殖民地時期#香港政府.md "wikilink")[政治](../Page/政治.md "wikilink")[疑犯均被拘留於此](../Page/疑犯.md "wikilink")，及被嚴刑逼供以至涉及[私刑的審訊](../Page/私刑.md "wikilink")。在香港，不少[左派人士均曾經被拘留在此](../Page/香港親共人士.md "wikilink")；於[六七暴動後](../Page/六七暴動.md "wikilink")，情況尤甚。

臨近[香港回歸](../Page/香港回歸.md "wikilink")，政治部於1995年解散。香港回歸後，域多利道扣押中心被空置，及被劃作為[外景拍攝的場地](../Page/外景拍攝.md "wikilink")。

2013年7月11日，域多利道扣押中心用地獲得[教育局宣布分配予](../Page/教育局_\(香港\).md "wikilink")[芝加哥大學](../Page/芝加哥大學.md "wikilink")[布斯商學院發展自資](../Page/布斯商學院.md "wikilink")[高等院校](../Page/高等院校.md "wikilink")，其亞洲分校將會由[新加坡遷戶香港](../Page/新加坡.md "wikilink")。

## 活化

2018年11月30日，芝加哥大學香港校園正式開幕，校園由國際知名的 Revery
Architecture（前稱譚秉榮建築事務所)設計。「白屋」部分斥資約7,500萬美元（約港幣5.86億）進行活化及翻新。白屋A座的皇家陸軍工兵橋、石壁爐及木樓梯及校舍旁的銀禧炮台遺址獲得保留並復修；B座囚室部份改建為自習室及課室。

文物展示中心最快在2018年12月對外開放，周一至三及周六到訪須先在網上預約；周五及周日設粵語或英語導賞團。\[2\]

## 曾經被拘留的著名人士

  - [曾昭科](../Page/曾昭科.md "wikilink")（[警司](../Page/警司.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")[間諜](../Page/間諜.md "wikilink")）
  - [黃建立](../Page/黃建立.md "wikilink")（[漢華中學校長](../Page/漢華中學.md "wikilink")）
  - [黃祖芬](../Page/黃祖芬.md "wikilink")（[中華中學校長](../Page/中華中學.md "wikilink")），前[律政司司長](../Page/律政司司長.md "wikilink")[梁愛詩的](../Page/梁愛詩.md "wikilink")[大舅](../Page/大舅.md "wikilink")
  - [劉三](../Page/劉三.md "wikilink")（[打石工會主席](../Page/打石工會.md "wikilink")）
  - [傅奇](../Page/傅奇.md "wikilink")、[石慧夫婦](../Page/石慧.md "wikilink")（藝人）
  - [蔡渭衡](../Page/蔡渭衡.md "wikilink")（香港華人革新協會秘書），被拘留一年半。
  - [任意之](../Page/任意之.md "wikilink")

## 都市傳聞

域多利道扣押中心的官方地址為香港域多利道\[3\]，不設門牌編號。有電台曾經以該建築物作為節目名稱，名為《[摩星嶺4號](../Page/摩星嶺4號.md "wikilink")》，然而該節目主持已經澄清該物業並無門牌號碼，而摩星嶺道門牌號碼應該是從山上開始計算，真正的摩星嶺道4號是住宅恆琪園。而該建築轉交芝加哥大學使用後，已獲編配門牌號碼「域多利道168號」。

## 電影場景

  - 《[2046](../Page/2046_\(電影\).md "wikilink")》裡的東方酒店
  - 《[色戒](../Page/色，戒_\(電影\).md "wikilink")》裡易先生的秘密居所

## 參考註釋

<references />

## 外部連結

  - [香港賽馬會芝加哥大學文物庭院及展示中心](http://heritage.uchicago.hk/zh/)
  - [保育指引](http://www.ipass.gov.hk/chi/Annex%20A%20-%20Conservation%20Guidelines.pdf)
  - [港英時代政治犯囚室首度曝光](http://www.epochtimes.com/b5/6/5/7/n1310529.htm)《大紀元報》，2006年7月6日
  - [港英時代政治犯集中营“白屋”首度曝光](http://www.singtaonet.com/global/hk_macau/t20060507_206556.html)《星島日報》，2006年7月6日

[Category:香港三級歷史建築](../Category/香港三級歷史建築.md "wikilink")
[Category:香港已停用警務建築](../Category/香港已停用警務建築.md "wikilink")
[Category:摩星嶺](../Category/摩星嶺.md "wikilink")
[Category:集中營](../Category/集中營.md "wikilink")

1.  [古物古蹟辦事處 - 1444
    幢歷史建築物簡要](http://www.lcsd.gov.hk/CE/Museum/Monument/form/AAB_brief_info_b5.pdf)


2.

3.