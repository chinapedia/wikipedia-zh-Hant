[1hs007_l.jpg](https://zh.wikipedia.org/wiki/File:1hs007_l.jpg "fig:1hs007_l.jpg")
《**基地與帝國**》（**Foundation and
Empire**），是[美國](../Page/美國.md "wikilink")[作家](../Page/作家.md "wikilink")[以撒·艾西莫夫](../Page/以撒·艾西莫夫.md "wikilink")[出版於](../Page/出版.md "wikilink")1952年的[科幻小說中篇集](../Page/科幻小說.md "wikilink")，「[基地三部曲](../Page/基地系列#基地三部曲.md "wikilink")」的第二部（後來發展成「[基地系列](../Page/基地系列.md "wikilink")」），收錄的2篇中篇小說，最早是分成兩本書出版的。這部中篇集跟《[第二基地](../Page/第二基地.md "wikilink")》關係密切，各有部份「騾」出場的故事。

## 將軍

**基地紀元195-196年\[1\]**，原名〈冥冥之手〉（The Dead Hand）。
經過兩百多年勵精圖治，基地以科技貿易征服眾多星際間的政治力量，成為銀河一方的霸主，無可避免地與銀河帝國發生衝突。帝國將軍貝爾·里歐斯（[Bel
Riose](../Page/:en:Bel_Riose.md "wikilink")）急於建功立業，領軍進逼基地。另一方面，基地派出行商探子拉珊·迪伐斯假裝被捕，意欲一探虛實，不料慘遭反間。里歐斯頻頻回奏朝廷，大事將成，請求增兵，此舉引起帝國皇帝克-{里}-昂二世的猜忌，里歐斯功高震主，最後不得不功敗垂成。

故事裡的帝國皇帝克-{里}-昂二世和將軍貝爾·里歐斯，原型是[東羅馬帝國皇帝](../Page/東羅馬帝國.md "wikilink")[查士丁尼大帝和麾下大將](../Page/查士丁尼大帝.md "wikilink")[貝利撒留](../Page/貝利撒留.md "wikilink")。情節與英國詩人[羅伯特·格拉維斯](../Page/羅伯特·格拉維斯.md "wikilink")（[Robert
Graves](../Page/:en:Robert_Graves.md "wikilink")）的《[Count
Belisarius](../Page/:en:Count_Belisarius.md "wikilink")》相似。

## 騾

**基地紀元310-311年**。
時間又過百年，帝國名存實亡，基地腐敗滲透，銀河四處，軍閥割據。哈里·謝頓（[Hari
Seldon](../Page/:en:Hari_Seldon.md "wikilink")）再次現身說法，基地赫然遭受入侵，「謝頓危機」倏忽降臨，發生的一切完全偏離謝頓的心理史學預測，一個新崛起的軍閥勢力「騾」（[Mule](../Page/:en:Mule_\(Foundation\).md "wikilink")），兵不刃血，佔下基地。杜倫和貝泰·達瑞爾夫婦（[Toran
and Bayta
Darell](../Page/:en:Toran_and_Bayta_Darell.md "wikilink")），偕同一位基地的心理學家，跟一位見過「騾」的小丑，前往已受內戰洗劫一空的[川陀](../Page/川陀.md "wikilink")，意圖找到[第二基地](../Page/第二基地.md "wikilink")，求援制「騾」，他們最後才知道，敵人始終遠在天邊，近在眼前。

## 參考資料與外部連結

### 參考資料

<references/>

### 外部連結

  - [艾西莫夫官方網頁](http://www.asimovonline.com/)
  - [葉李華個人網站](http://yehleehwa.net/)，被譽為「艾西莫夫中文世界代言人」
  - [科幻國協在臺辦事處](http://www.wretch.cc/blog/danjalin)
  - [卡蘭坦斯蓋普恩基地](http://blog.yam.com/krantas)

### 延伸閱讀

  - [艾西莫夫：機器人與基地宇宙作品年表](http://blog.yam.com/krantas/article/1911265)，卡蘭坦斯蓋普恩基地，卡蘭坦斯
  - [艾西莫夫宇宙：銀河地圖／星球分區對照](http://blog.yam.com/krantas/article/3197914)，卡蘭坦斯蓋普恩基地，卡蘭坦斯

[sv:Stiftelseserien\#Stiftelsen och
imperiet](../Page/sv:Stiftelseserien#Stiftelsen_och_imperiet.md "wikilink")

[Category:美國小說](../Category/美國小說.md "wikilink")
[Category:科幻小说](../Category/科幻小说.md "wikilink")
[Category:基地系列](../Category/基地系列.md "wikilink")

1.  [艾氏機器人與基地系列年代表](http://www.asimovonline.com/oldsite/insane_list.html)（英文）