**基本电荷**（符号：**\(e\)**，也称**元电荷**），是一个[质子所带的](../Page/质子.md "wikilink")[电荷](../Page/电荷.md "wikilink")，或一个[电子所带的负电荷的量](../Page/电子.md "wikilink")。它是一个基本[物理常数](../Page/物理常数.md "wikilink")，是[原子单位和一些其它](../Page/原子单位.md "wikilink")[自然单位制中的电荷单位](../Page/自然单位制.md "wikilink")。

根据[国际科学技术数据委员会所公布](../Page/国际科学技术数据委员会.md "wikilink")[*e*的值](http://physics.nist.gov/cgi-bin/cuu/Value?e)，基本电荷的值大约为

\[e=1.602\ 176\ 6208(98)\times10^{-19}\ \mbox{C}\]\[1\]
在[高斯單位制中](../Page/高斯單位制.md "wikilink")，它的值为

\[e=4.803 204 27\times10^{-10}\ \mbox{statC}\].

自从1909年[罗伯特·密立根的](../Page/罗伯特·密立根.md "wikilink")[油滴实验中测量出基本电荷后](../Page/油滴实验.md "wikilink")，人们便认为它不可再分了。1960年发现了[夸克](../Page/夸克.md "wikilink")，它们的电荷为<small><sup>1</sup><big>⁄</big><sub>3</sub></small> *e*和<small><sup>2</sup><big>⁄</big><sub>3</sub></small> *e*，所以把“基本电荷”用来指电子的电荷便不完全正确了；然而单独的夸克至今没有探测到，都是两个以上的夸克聚集在一起，使得总电荷为基本电荷的整数倍。

新的基本电荷已被ISO设定为\(e=1.602\ 176\ 634\times10^{-19}\ \mbox{C}\)。\[2\]\[3\]

## 參閱

  - [普朗克電荷](../Page/普朗克電荷.md "wikilink")

## 参考文献

  - **Fundamentals of Physics**, 7th Ed., Halliday, Robert Resnick, and
    Jearl Walker. Wiley, 2005

<references />

[J](../Category/物理常数.md "wikilink") [J](../Category/电学单位.md "wikilink")

1.  [CODATA Value: elementary
    charge](http://physics.nist.gov/cgi-bin/cuu/Value?e)
2.
3.