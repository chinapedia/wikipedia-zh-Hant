[thumb](../Page/image:Spas_vsederzhitel_sinay.jpg.md "wikilink")

**基督**来自于[希腊语](../Page/希腊语.md "wikilink")或，是[亚伯拉罕诸教中的术语](../Page/亚伯拉罕诸教.md "wikilink")，原意是“[受膏者](../Page/受膏.md "wikilink")”（中東地區膚髮易乾裂，古代的[以色列王即位時必須將油倒在國王的頭上](../Page/以色列.md "wikilink")，滋潤膚髮，象徵這是神用來拯救[以色列人的王](../Page/以色列人.md "wikilink")，後來轉變成[救世主的意思](../Page/救世主.md "wikilink")），也等同于[希伯来语中的](../Page/希伯来语.md "wikilink")***'***'[彌賽亞](../Page/彌賽亞.md "wikilink")，意思为「受膏者」。在[基督教](../Page/基督教.md "wikilink")、[聖經當中基督是](../Page/聖經.md "wikilink")[拿撒勒的](../Page/拿撒勒人.md "wikilink")[耶稣专有名稱](../Page/耶稣.md "wikilink")，或者是稱號，即“耶穌基督”。

基督一词常被误认为是耶稣的姓，因为[圣经中曾多次提到](../Page/圣经.md "wikilink")「耶稣基督」（Jesus
Christ）。相信耶稣基督的人被称作[基督徒](../Page/基督徒.md "wikilink")，因为他们相信並清楚知道耶稣是他们的救世主，或在[旧约书中所预言的弥赛亚](../Page/旧约.md "wikilink")。大部分[犹太教徒反对这一观点](../Page/犹太教徒.md "wikilink")，并仍然等待着弥赛亚的到来。而所有基督徒现在正在等待基督耶穌的[再临](../Page/基督再临.md "wikilink")，从而验证弥赛亚预言余下的内容。

[基督教神學集中研究耶稣的身位](../Page/基督教神學.md "wikilink")、生活、教授、和工作。又被称为基督学。

## 各宗教觀點

### 犹太教中的基督

根據[希伯來人的](../Page/希伯來人.md "wikilink")[信仰傳統](../Page/信仰.md "wikilink")，被（其他人用油）[膏立是一種特別](../Page/受膏.md "wikilink")[宗教儀式](../Page/宗教儀式.md "wikilink")，意思指被選立的人，例如：[祭司](../Page/祭司.md "wikilink")、[君王及](../Page/君主.md "wikilink")[先知是神所選定的](../Page/先知.md "wikilink")。在某些场合下一些器具也被油膏抹，用以预备宗教仪式。受膏的重要性，是因为设立圣职（代表会众到神面前赎罪）的需要，而得以强调。例如：『受膏的祭司要取些公牛的血带到会幕』（[利未记](../Page/利未记.md "wikilink")）。在希伯来圣经之中，被称为“弥赛亚”的多人不是神，无神性含义。[犹太教与基督教在](../Page/犹太教与基督教.md "wikilink")[弥赛亚等一系列问题上都有分歧](../Page/彌賽亞.md "wikilink")）。

### 基督教中的基督

[彌賽亞是](../Page/彌賽亞.md "wikilink")《[舊約聖經](../Page/舊約聖經.md "wikilink")》中[受膏者的意思](../Page/受膏者.md "wikilink")，[希臘文譯作](../Page/希臘文.md "wikilink")[基督](../Page/基督.md "wikilink")。《[舊約](../Page/舊約.md "wikilink")》時代的[猶太人之中](../Page/猶太人.md "wikilink")，只有三種職份才可以[受膏](../Page/受膏.md "wikilink")，分別是[君王](../Page/君王.md "wikilink")，[先知和祭司](../Page/先知.md "wikilink")。不論在《[舊約](../Page/舊約.md "wikilink")》或《[新約](../Page/新約.md "wikilink")》，只有耶穌同時兼備這三個身份，正因為耶穌是君王\[1\]，是先知\[2\]，也是[祭司](../Page/祭司.md "wikilink")\[3\]，所以耶穌就是真正的受膏者，也就是[希臘文中基督的意思](../Page/希臘文.md "wikilink")，故用來指代耶穌。

在《[新約聖經](../Page/新約聖經.md "wikilink")》中，基督是指等候已久而來臨的[救世主](../Page/救世主.md "wikilink")。[聖靈如](../Page/聖靈.md "wikilink")[鴿子般降下](../Page/鸽属.md "wikilink")，且有聲音被[施洗約翰聽見](../Page/施洗約翰.md "wikilink")，見證耶穌就是基督、就是舊約所有　神的子民素來所盼望的那位——彌賽亞。[耶穌多次給他的門徒證明自己就是基督](../Page/耶穌.md "wikilink")，並且在他受審時三次聲稱他就是**基督**。

### 伊斯兰教中的基督

[伊斯兰教](../Page/伊斯兰教.md "wikilink")[穆斯林稱耶穌基督為](../Page/穆斯林.md "wikilink")[耶稣（爾撒）](../Page/爾撒.md "wikilink")，他是[基督](../Page/基督.md "wikilink")，即[彌賽亞](../Page/彌賽亞.md "wikilink")，更是一位早於[穆罕默德的](../Page/穆罕默德.md "wikilink")[阿拉使者](../Page/阿拉使者.md "wikilink")，儘管穆斯林相信耶稣是[聖母瑪利亞所生](../Page/聖母瑪利亞.md "wikilink")，但他们不相信[爾撒是](../Page/爾撒.md "wikilink")[上帝之子](../Page/上帝之子.md "wikilink")，他们也不相信耶稣死于[十字架後](../Page/十字架.md "wikilink")[復活](../Page/復活.md "wikilink")，而是直接被-{zh-cn:[安拉](../Page/安拉.md "wikilink");zh-tw:[阿拉](../Page/阿拉.md "wikilink");}-升上天堂了。

伊斯兰传统《[聖訓](../Page/聖訓.md "wikilink")》叙述[爾撒会在](../Page/爾撒.md "wikilink")[世界末日时](../Page/伊斯蘭教末世論.md "wikilink")，由[天國降临](../Page/天國.md "wikilink")[俗世](../Page/世界.md "wikilink")：恢复[正义](../Page/正义.md "wikilink")、摧毁各種[異端](../Page/異端.md "wikilink")、[叛教者與假弥赛亚](../Page/叛教.md "wikilink")（[敌基督](../Page/敌基督.md "wikilink")）以及[伊斯兰教的敌人](../Page/伊斯兰教.md "wikilink")。

## 参考文献

## 延伸阅读

  - Jewish Encyclopedia 中的 Messiah 条目
    [1](http://jewishencyclopedia.com/view.jsp?artid=510&letter=M&search=messiah)
  - Jewish Encyclopedia 中的 Jesus of Nazareth 条目
    [2](http://jewishencyclopedia.com/view.jsp?artid=254&letter=J)
  - A. J. Maas, *Origin of the Name of Jesus Christ*, Catholic
    Encyclopedia [3](http://www.newadvent.org/cathen/08374x.htm)
  - Ludwig Ott, *Fundamentals of Catholic Dogma*, 1957.
  - Paul A. Hughes, *The Gnostic Christ: Gnosticism vs. Christianity*
    [4](https://www.webcitation.org/query?id=1256578669623811&url=www.geocities.com/Athens/Crete/6111/pneumatikos/gnostic.htm)
  - *The Etymological Derivation Of The Name "Christ"*, NZs Hare Krishna
    Spiritual Network
    [5](http://www.salagram.net/jesus-christ-kristos-page.htm)

{{-}}

[fr:Christ\#Religion](../Page/fr:Christ#Religion.md "wikilink")

[Category:耶稣的称号](../Category/耶稣的称号.md "wikilink")
[Category:基督论](../Category/基督论.md "wikilink")
[Category:希腊语外来词](../Category/希腊语外来词.md "wikilink")
[Category:弥赛亚主义](../Category/弥赛亚主义.md "wikilink")

1.  《[馬太福音](../Page/馬太福音.md "wikilink")》2章2節
2.  《[約翰福音](../Page/約翰福音.md "wikilink")》6章14節
3.  《[希伯來書](../Page/希伯來書.md "wikilink")》9章11節