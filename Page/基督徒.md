
<small>另外，這裡的[第一手來源和其他相關來源的數量也不足以支持一篇正確的百科全書文章](../Page/WP:PRIMARY.md "wikilink")。
請協助補充符合[可靠來源要求的引用以](../Page/Wikipedia:可靠來源.md "wikilink")\[
改善這篇條目\]。</small>}}

**基督徒**（Christian）是對[基督宗教](../Page/基督宗教.md "wikilink")[信徒](../Page/信徒.md "wikilink")（教徒）的稱呼，泛指相信[耶穌是](../Page/耶穌.md "wikilink")[基督和](../Page/基督.md "wikilink")[神的兒子及跟從其教導的人](../Page/神的兒子.md "wikilink")，現泛指[基督宗教](../Page/基督宗教.md "wikilink")[教會的成員](../Page/教會.md "wikilink")，包括[天主教信徒](../Page/天主教.md "wikilink")、[正教會信徒](../Page/正教會.md "wikilink")、[新教信徒](../Page/新教.md "wikilink")、[獨立教會信徒在內的基督教各](../Page/獨立教會.md "wikilink")[宗派人士](../Page/基督教派系.md "wikilink")，部分信奉基督教但不加入任何基督教教派或教會的信徒，亦可被稱或自稱為基督徒，現時全世界基督徒約有24億人。因歷史發展的緣故，[漢語語境所稱的](../Page/漢語.md "wikilink")「基督徒」有時指新教信徒。此名稱出自《[新約聖經](../Page/新約聖經.md "wikilink")》，當時其著作所用的[通用希臘語將基督徒稱為](../Page/通用希臘語.md "wikilink")「」（），意思是“[基督的人](../Page/基督.md "wikilink")”。

「基督徒」一詞多次出現在《[新約聖經](../Page/新約聖經.md "wikilink")》的書信部分，例如在與：從[安提阿的](../Page/安提阿.md "wikilink")[教會聚集起首](../Page/教會.md "wikilink")，他們被稱為基督徒；及後，[希律亞基帕曾對](../Page/希律亞基帕.md "wikilink")[保羅表示](../Page/保罗_\(使徒\).md "wikilink")，「保羅少許的說話，便叫亞基帕作基督徒嗎！」。《[彼得前書](../Page/彼得前書.md "wikilink")》裡，[彼得把](../Page/彼得_\(使徒\).md "wikilink")「基督徒」與「受苦」連在一起，也指出「基督徒受苦，不是羞恥，而是榮耀上帝的事」。在保羅傳教時代，基督徒較常以「聖徒」自稱。聖經提到耶穌給基督徒的大使命，「所以你們要去、使萬民作我的門徒、奉父、子、聖靈的名、給他們施洗．〔或作給他們施洗歸於父子聖靈的名〕凡我所吩咐你們的、都教訓他們遵守。我就常與你們同在、直到世界的末了。」。

## 歷史

### 名稱的來源

[Famous_Christians_around_the_world.jpg](https://zh.wikipedia.org/wiki/File:Famous_Christians_around_the_world.jpg "fig:Famous_Christians_around_the_world.jpg")
公元44年左右，耶穌基督的忠貞跟從者開始以「基督徒」這個名稱為人所知。早期有些人認為，「基督徒」這個名稱是由外人懷着貶意稱呼他們的。可是，好些《聖經》詞典編者和注釋家卻指出，所用的動詞含有受上帝指引或啟迪的意思。因此，這節經文在《[聖經和合本](../Page/聖經和合本.md "wikilink")》譯作：“門徒稱為基督徒是從[安提阿起首](../Page/安提阿.md "wikilink")。”\[1\]到公元58年，甚至[羅馬官員也熟知](../Page/羅馬帝國.md "wikilink")「基督徒」這個名稱\[2\]。

耶穌基督的[使徒仍然在世的日子](../Page/使徒.md "wikilink")，基督徒這個名稱的確獨特而具有識别作用\[3\]。所有以基督徒自居，但行為或[信仰卻與基督的教訓相背的人](../Page/信仰.md "wikilink")，均被逐出基督徒群體之外。可是新約聖經記載使徒們去世以後，[撒但便趁機撒下一些産生假基督徒的種子](../Page/撒但.md "wikilink")。這些冒充信徒的人也自稱為基督徒。\[4\]

### 中国的基督徒

早於[唐朝時](../Page/唐朝.md "wikilink")，已自[波斯](../Page/波斯.md "wikilink")（今[伊朗](../Page/伊朗.md "wikilink")）傳[大秦景教入中國](../Page/大秦景教.md "wikilink")，現流傳[大秦景教流行中國碑](../Page/大秦景教流行中國碑.md "wikilink")。[清朝未年](../Page/清朝.md "wikilink")，在中國的基督徒被稱為「教民」；由於某些不平等因素（例如[教案](../Page/教案_\(宗教\).md "wikilink")），導致[義和團的出現](../Page/義和團.md "wikilink")。義和團與教民衝突導致雙方多人死亡。

1949年[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，由于[中國共產黨的國家](../Page/中國共產黨.md "wikilink")[無神論政策](../Page/無神論.md "wikilink")（state
atheism），加上中共擔心有組織性的宗教通過外部勢力介入中國的政治（如聽命于[羅馬教廷](../Page/羅馬教廷.md "wikilink")[聖座的](../Page/聖座.md "wikilink")[天主教會](../Page/天主教會.md "wikilink")），加上政治運動及[文化大革命等政治因素](../Page/文化大革命.md "wikilink")，宗教活动受到嚴厲打壓和限制，所有宗教的寺廟和教堂主持人都由官方雇用與規範，如[三自教會](../Page/三自教會.md "wikilink")，也有一些不受官方規管的[地下教會](../Page/地下教會.md "wikilink")。

## 成為教徒的定義

一些傳統的基督教會如[天主教](../Page/天主教.md "wikilink")、[浸信會及](../Page/浸信會.md "wikilink")[褔音派等主流](../Page/福音主義.md "wikilink")[教會認為教徒不必經過](../Page/教會.md "wikilink")[受洗後](../Page/受洗.md "wikilink")，才正式成為基督徒，只要口裡承認、心裡相信上帝和把耶穌基督視為救主、便成為基督徒，無需從屬于某一個教會或教派。

## 教籍

如果基督徒行为不检，例如[偷窃](../Page/偷窃.md "wikilink")、[搶劫](../Page/搶劫.md "wikilink")、[詐騙](../Page/詐騙.md "wikilink")、[通奸](../Page/通奸.md "wikilink")、[奸淫](../Page/奸淫.md "wikilink")、[殺人等](../Page/殺人.md "wikilink")，就有可能遭到所屬的宗派[開除教籍](../Page/開除教籍.md "wikilink")。天主教叫「[絕罰](../Page/絕罰.md "wikilink")」，又稱「[破門律](../Page/破門律.md "wikilink")」。

## 其他名稱

基督徒之間有些特別的相稱，如「[弟兄](../Page/弟兄.md "wikilink")」（[公教慣稱](../Page/公教.md "wikilink")「[兄弟](../Page/兄弟.md "wikilink")」）、「姊妹」、「教友」、「會友」等。但有基督徒認為，根據《聖經》\[5\]\[6\]，基督徒最好以弟兄姊妹相稱，不適合使用「教友」或「會友」。

## 人口統計

[Christianity_percent_population_in_each_nation_World_Map_Christian_data_by_Pew_Research.svg](https://zh.wikipedia.org/wiki/File:Christianity_percent_population_in_each_nation_World_Map_Christian_data_by_Pew_Research.svg "fig:Christianity_percent_population_in_each_nation_World_Map_Christian_data_by_Pew_Research.svg")

| 地區                                                                        | 基督徒           | % 基督徒比例                   |
| ------------------------------------------------------------------------- | ------------- | ------------------------- |
| [歐洲](../Page/歐洲.md "wikilink")                                            | 558,260,000   | 75.2                      |
| [拉丁美洲](../Page/拉丁美洲.md "wikilink")–[加勒比海國家](../Page/加勒比海國家.md "wikilink") | 531,280,000   | 90.0                      |
| [撒哈拉以南非洲](../Page/撒哈拉以南非洲.md "wikilink")                                  | 517,340,000   | 62.9                      |
| [亞太區](../Page/亞太區.md "wikilink")                                          | 286,950,000   | 7.1                       |
| [北美洲](../Page/北美洲.md "wikilink")                                          | 266,630,000   | 77.4                      |
| [中東](../Page/中東.md "wikilink")–[北非](../Page/北非.md "wikilink")             | 12,710,000    | 3.7                       |
| 全球                                                                        | 2,173,180,000 | text-align:center;"| 31.5 |

**基督徒全球各地比例** |+(皮尤研究中心，2011年)\[7\]\[8\]\[9\]

## 參考文獻

## 参见

  - [基督](../Page/基督.md "wikilink")[耶稣](../Page/耶稣.md "wikilink")
  - [使徒 (基督教)](../Page/使徒_\(基督教\).md "wikilink")
  - [门徒 (基督教)](../Page/门徒_\(基督教\).md "wikilink")
  - [耶稣真正的家人](../Page/耶稣真正的家人.md "wikilink")

{{-}}

[Category:基督教頭銜](../Category/基督教頭銜.md "wikilink")
[基督徒](../Category/基督徒.md "wikilink")
[Category:各宗教信徒](../Category/各宗教信徒.md "wikilink")

1.  類似的譯法見於Robert Young的*Literal Translation of the Holy
    Bible*聖經直譯本修訂本，1898年；
    *The Simple English Bible*（《簡明英文聖經》），1981年；
    Hugo McCord所編的*New Testament*（《新約聖經》），1988年
2.  参看
3.
4.  参看
5.  ：“凡遵行神旨意的人，就是我的弟兄姐妹和母親了。”
6.  ：“耶穌回答說：‘聽了神之道而遵行的人，就是我的母親、我的弟兄了。’”
7.
8.
9.