[CMA_Logo.jpg](https://zh.wikipedia.org/wiki/File:CMA_Logo.jpg "fig:CMA_Logo.jpg")
 **基督教宣道會**（簡稱**宣道會**，，简称：
**C\&MA**），意思是「[基督徒與宣教士同盟](../Page/基督徒.md "wikilink")」，即将原来强调圣洁生活的一个基督教组织和强调宣教的另一个组织合成一个机构，由[宣信](../Page/宣信.md "wikilink")[博士](../Page/博士.md "wikilink")（Dr.Albert
Simpson，1843年－1919年）於1887年成立，在二十世纪形成了一个新的宗派。宣道會總部設於[美國](../Page/美國.md "wikilink")。於1888年開始，差遣[傳教士到](../Page/傳教士.md "wikilink")[中国宣教](../Page/中国.md "wikilink")。現活躍於美國、[加拿大](../Page/加拿大.md "wikilink")、[中国大陆](../Page/中国大陆.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳洲及](../Page/澳洲.md "wikilink")[台灣等地](../Page/台灣.md "wikilink")。信徒分佈於75個國家，會眾人數超過200萬。

## 歷史

[宣信於](../Page/宣信.md "wikilink")1887年在[纽约组成基督徒联合会](../Page/纽约.md "wikilink")（the
Christian Alliance）。两年之后，成立福音宣道联合会（the Evangelical Missionary
Alliance），分别应付国内和海外的需要。1897年，两会合并成为基督教宣道会（Christian and Missionary
Alliance，意即「信徒與宣教士齊心聯盟」），在還没有教会的地方作宣揚基督教義的工作。

宣信成立宣道會背後的動力是他宣揚福音的心志。起初，宣道會並非是要成為一個教派，而是作為一個有組織的「全球普傳福音」運動。正如宣信博士所願，今天，宣道會肩負著「全球普傳福音」運動的領導角色。\[1\]

## 四重福音

宣道會會徽的四個標誌象徵著該會的「四重福音」教義，分別為：

  - 十字架：基督是拯救之主 （）
  - 洗濯盆：基督是成聖之主（）
  - 油瓶　：基督是醫治之主（）
  - 冠冕　：基督是再來之王（）

## 在中國和越南的傳教站（1936年）

  - 華中部（[湖北](../Page/湖北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[安徽](../Page/安徽省.md "wikilink")3省，總部[武昌](../Page/武昌.md "wikilink")）－[蕪湖](../Page/蕪湖.md "wikilink")（1891年〉；武昌（1893年）；南陵、青陽、灣址（1896年）；[常德](../Page/常德.md "wikilink")（1897年）；長沙（1899年）；漢口（1908年）；漢壽（1914年）；太平（1922年）；祁門
    、廬江（1923年）。
  - 華西部（[甘肅](../Page/甘肅.md "wikilink")）—洮州（1895年）；岷州（1897年）；狄道（1902年）；卓尼（1905年）；合作；河州（[臨夏](../Page/臨夏.md "wikilink")）；循化；鞏昌。
  - 華南部（[廣西](../Page/廣西.md "wikilink")，總部[梧州](../Page/梧州.md "wikilink")）——梧州（1897年）；慶遠（1898年）；百色（1923年）；[越南](../Page/越南.md "wikilink")[峴港](../Page/峴港.md "wikilink")、[海防](../Page/海防.md "wikilink")。
  - 川黔部—松桃、秀山（1923年）；彭水。
  - [上海](../Page/上海.md "wikilink")（1900年）。

宣道會在初期到中國傳教時，曾將傳教重點放在北部[长城沿線一帶](../Page/长城.md "wikilink")。有60名瑞典青年人在這一帶傳教，他們從[张家口開始](../Page/张家口.md "wikilink")，向西到甘肅、南到[大同](../Page/大同市.md "wikilink")、北到庫侖（今[乌兰巴托](../Page/乌兰巴托.md "wikilink")），總部設在歸化城（今[呼和浩特](../Page/呼和浩特.md "wikilink")）。到1900年，已經有16個傳教站和200名信徒，但在「[义和团事件](../Page/义和团事件.md "wikilink")」中，瑞典傳教士有21名成人和15名兒童被殺，有16人向北穿過沙漠逃到俄國；還有一群向南逃到漢口，華北工作完全結束。後來山西北部6個傳教站交給中國內地會負責。北京和天津的宣道會也在1900年被毀，[伍约翰夫婦從天津遷往](../Page/伍约翰.md "wikilink")[上海](../Page/上海.md "wikilink")，設立著名的[北四川路](../Page/北四川路.md "wikilink")[守真堂](../Page/守真堂.md "wikilink")。1913年，宣道会在广西[梧州开设宣道书局](../Page/梧州.md "wikilink")。

## 香港區聯會

### 轄下機構

  - [建道神學院](../Page/建道神學院.md "wikilink")（位於香港[長洲](../Page/長洲_\(香港\).md "wikilink")）
  - 宣道出版社
  - 香港宣道差會
  - 基督教宣道會社會服務處
  - 房角石協會
  - [宣道園](../Page/宣道園.md "wikilink")

### 其他機構

  - 西差會
  - 宣道廣播中心

### 主要人物

  - [滕近輝](../Page/滕近輝.md "wikilink")－前基督教宣道會北角堂堂主任、宣道會香港區聯會榮譽主席及榮譽牧顧長、宣道會北角堂榮譽顧問牧師、世界華福會國際會榮譽主席。2013年12月安息主懷。
  - [蔡元雲](../Page/蔡元雲.md "wikilink")－香港宣道差會董事、[突破機構榮譽總幹事](../Page/突破機構.md "wikilink")
  - [蕭壽華](../Page/蕭壽華.md "wikilink")－基督教宣道會香港區聯會執委會顧問、北角堂堂主任
  - [區伯平](../Page/區伯平.md "wikilink")－基督教宣道會香港區聯會執委會主席、香港宣道差會董事、康怡堂堂主任
  - [畢德富](../Page/畢德富.md "wikilink")－基督教宣道會香港區聯會執委會副主席、香港宣道差會董事、荃灣堂堂主任
  - [簡耀堂](../Page/簡耀堂.md "wikilink")－基督教宣道會香港區聯會執委會副主席、香港宣道差會主席、希伯崙堂堂主任
  - [梁家麟](../Page/梁家麟.md "wikilink")－建道神學院院長、香港區聯會執委會執委
  - [張慕皚](../Page/張慕皚.md "wikilink")－前建道神學院院長
  - [蕭如發](../Page/蕭如發.md "wikilink")－[明光社董事](../Page/明光社.md "wikilink")、基蔭堂堂主任
  - [譚文鈞](../Page/譚文鈞.md "wikilink")－前加拿大華人宣道會聯會執行總幹事
  - 蔡少琪－現建道神學院副院長、下屆建道神學院院長\[2\]

## 內部連結

  - [九龍塘宣道會](../Page/九龍塘宣道會.md "wikilink")
  - [基督教柏斯宣道會](../Page/基督教柏斯宣道會.md "wikilink")
  - [福音之声](../Page/苗族希望之声.md "wikilink")（Suab Xaa Moo Zoo，对老挝信众的福音广播电台）

## 外部連結

  - [基督教宣道會-美國](http://www.cmalliance.org/)
  - [基督教宣道會世界團契](http://www.allianceworldfellowship.org/)
  - [基督教宣道會-澳洲](http://www.cma.org.au/)
  - [基督教宣道會香港區聯會](http://www.cmacuhk.org.hk/)
  - [基督教宣道會香港區聯會－宣道會簡史](http://www.cmacuhk.org.hk/version4/html/intro/history_1.shtml)
  - [基督教宣道會香港區聯會－宣道會信仰](http://www.cmacuhk.org.hk/version4/html/intro/beliefs.shtml)
  - [基督教宣道會香港區聯會－宣道會會徽含義](http://www.cmacuhk.org.hk/version4/html/intro/logo.shtml)
  - [宣道出版社](http://www.capbooks.hk)
  - [宣道廣播中心](http://www.cmaradiohk.org/)
  - [臺灣省聯會](http://twcama.fhl.net/)

## 參考資料

[基督教宣道會](../Category/基督教宣道會.md "wikilink")
[Category:1887年美國建立](../Category/1887年美國建立.md "wikilink")

1.
2.