[CompositeJesus.JPG](https://zh.wikipedia.org/wiki/File:CompositeJesus.JPG "fig:CompositeJesus.JPG")

**基督教文化**（）是信仰[基督教為主的人群長期以來形成的一種](../Page/基督教.md "wikilink")[文化](../Page/文化.md "wikilink")，並構成了基督教文明（）的主體。

## 源來

基督教源於[中東地區](../Page/中東地區.md "wikilink")[遊牧民族](../Page/遊牧民族.md "wikilink")[以色列人的](../Page/以色列人.md "wikilink")[猶太教](../Page/猶太教.md "wikilink")。猶太教又是綜合了兩個相距較近、獨立起源的不同文化——[兩河流域文化和](../Page/兩河流域文化.md "wikilink")[尼羅河流域文化](../Page/尼羅河流域文化.md "wikilink")，從兩河流域文化中吸收了[單一神概念](../Page/單一神.md "wikilink")，[主神用六天創造了世界萬物](../Page/創世記.md "wikilink")，以及[大洪水的傳說](../Page/大洪水.md "wikilink")；從尼羅河流域文化中吸收了[世界末日和](../Page/世界末日.md "wikilink")[最後審判的傳說](../Page/最後審判.md "wikilink")，形成了一套完整的宗教理論。西元一世紀，從猶太教發展出一個分支，信仰[耶穌為拯救人類的](../Page/耶稣.md "wikilink")[彌賽亞](../Page/彌賽亞.md "wikilink")，逐步發展成為獨立於猶太教的宗教。

基督教將[猶太教從一種](../Page/猶太教.md "wikilink")[民族性宗教擴展為一種跨民族的宗教](../Page/民族.md "wikilink")。基督教文化是屬於擴張型的，其哲學觀念認為時間是有始有終的，而空間是無限可重複的，他們強調對世界及自然的管治。基督徒把說服全體人類信奉他們的[上帝作為自己的使命](../Page/上帝.md "wikilink")，他們要抓緊時間，在世界末日到來之前，讓全體人類成為上帝的子民，不斷地向其他民族派遣[傳教士](../Page/傳教士.md "wikilink")。傳教士們每到一個新的文化範圍內，都積極地學習當地語言，為沒有文字的民族創造文字翻譯《[聖經](../Page/聖經.md "wikilink")》，《聖經》目前已經成為世界上翻譯文字種類最多的一本書。

## 定義

基督教文化的這種擴張性鼓勵人們從事探險活動，不斷地去探索、征服[未知世界](../Page/新大陆.md "wikilink")，培育出大批的[探險家](../Page/探險家.md "wikilink")。基督教原來主要局限於在[羅馬帝國境內傳播](../Page/羅馬帝國.md "wikilink")，但隨著歐洲國家的殖民擴張，很快傳遍全世界，成為世界上信徒最多、影響最廣泛的[宗教](../Page/宗教.md "wikilink")。基督教文化對現代世界文化的影響最大，不僅影響[哲學](../Page/哲學.md "wikilink")、人們的思維方式，甚至影響到[科學發現和](../Page/科學.md "wikilink")[生活方式](../Page/生活方式.md "wikilink")。

基督教提倡社會要承擔關注弱者的責任，並帶頭付諸于行動，成立了大量[孤兒院](../Page/孤兒院.md "wikilink")、[養老院](../Page/養老院.md "wikilink")、[醫院](../Page/醫院.md "wikilink")。而有些文化則認為這是家庭的責任而不是社會的責任。基督教這種強調社會責任的主張，有助於培育基督教的[市民社會](../Page/市民社會.md "wikilink")，而不是培育[家族社會](../Page/家族.md "wikilink")。基督教的這種主張也是[社會主義思潮的來源](../Page/社會主義.md "wikilink")。

基督教文化強調[夫妻](../Page/夫妻.md "wikilink")（一男一女）是在上帝面前結合的，這在[中古時代社會是僅有的](../Page/中古時代.md "wikilink")（但是信徒也沒有嚴格遵守，[查理曼大帝就有四個妻子](../Page/查理曼.md "wikilink")，五個嬪妃）。因此，基督教文化特別強調愛情、歌頌愛情([雅歌](../Page/雅歌.md "wikilink"))。

基督教文化的擴張在南部遇到[伊斯蘭文化的阻礙](../Page/伊斯蘭文化.md "wikilink")，很長時間內只能局限於歐洲範圍內。但自[工業革命後](../Page/工業革命.md "wikilink")，隨著歐洲工業化國家的殖民擴張，基督教文化繞過[伊斯蘭教文化的障礙](../Page/伊斯蘭教.md "wikilink")，向全世界迅速傳播，目前[澳洲](../Page/澳洲.md "wikilink")、南北[美洲](../Page/美洲.md "wikilink")、[埃塞俄比亞和部分](../Page/埃塞俄比亞.md "wikilink")[撒哈拉以南的](../Page/撒哈拉.md "wikilink")[非洲國家](../Page/非洲.md "wikilink")、[菲律賓和](../Page/菲律賓.md "wikilink")[太平洋島國都已進入基督教文化圈](../Page/太平洋.md "wikilink")，基督教文化已經滲入到东亚[儒教文化圈的诸國內部](../Page/儒教文化.md "wikilink")。

隨著基督教分裂成為[東正教](../Page/東正教.md "wikilink")、[天主教](../Page/天主教.md "wikilink")、[新教](../Page/新教.md "wikilink")，基督教文化也呈現出不同的色彩，而新教文化地區首先跨入資本主義現代文明。德國社會學家[馬克斯·韋伯寫有一本著名的著作](../Page/馬克斯·韋伯.md "wikilink")《新教倫理與資本主義精神》，專門論述[基督教新教與資本主義發展的關係](../Page/新教.md "wikilink")。

## 参见

  - [西方文化](../Page/西方文化.md "wikilink")
  - [儒教文化](../Page/儒教文化.md "wikilink")
  - [伊斯兰文化](../Page/伊斯兰文化.md "wikilink")

[基督教文化](../Category/基督教文化.md "wikilink")
[西方文化](../Category/西方文化.md "wikilink")