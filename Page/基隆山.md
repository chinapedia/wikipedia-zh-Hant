**基隆山**是位於[臺灣東北部的一座](../Page/臺灣.md "wikilink")[山岳](../Page/山岳.md "wikilink")，行政區劃屬於[新北市](../Page/新北市.md "wikilink")[瑞芳區](../Page/瑞芳區.md "wikilink")，標高588[公尺](../Page/公尺.md "wikilink")。由於山勢高聳獨立，又加上位置緊鄰[東海](../Page/東海.md "wikilink")，基隆山在早期成為航海的重要指標，也是[九份的地標之一](../Page/九份.md "wikilink")。基隆山形貌多變，從海上正看山形似雞籠，故舊時稱為「**雞籠山**」\[1\]，而側看又似一位橫臥的孕婦，故又有「**大肚美人山**」之美名，從[瑞金公路看又似](../Page/縣道102號.md "wikilink")[富士山](../Page/富士山.md "wikilink")，故又有「**基隆富士山**」之別稱。由於[臺語流行歌曲](../Page/臺語流行歌曲.md "wikilink")《基隆山之戀》的知名，使得基隆山聞名全臺。

## 地質

基隆山於基隆火山群之最西北端，呈東西約1.9公里，南北約1.6公里，其最高點稍偏東南高度587公尺，北端伸入海中。地質為含普通[輝石](../Page/輝石.md "wikilink")[黑雲母](../Page/黑雲母.md "wikilink")[角閃](../Page/角閃石.md "wikilink")[石英](../Page/石英.md "wikilink")[安山岩或含普通輝石石英黑雲母角閃安山岩](../Page/安山岩.md "wikilink")。\[2\]
基隆山為一座錐形[死火山](../Page/死火山.md "wikilink")，屬於[基隆火山群](../Page/基隆火山群.md "wikilink")，為一座侵入式[火山地形之山](../Page/火山.md "wikilink")-{岳}-。岩層堅硬，經終年海風及東北季風吹拂，因此山下樹木稀疏，芒草叢生\[3\]。

[日治時期時](../Page/台灣日治時期.md "wikilink")，日本[台灣總督府以通過基隆山頂之正南北](../Page/台灣總督府.md "wikilink")[稜線為界](../Page/山稜線.md "wikilink")，劃分出[金瓜石與](../Page/金瓜石.md "wikilink")[九份地區的礦權](../Page/九份.md "wikilink")，而後發展出金九兩地不同的礦區風貌，也是九份、金瓜石兩[聚落的分界山](../Page/聚落.md "wikilink")\[4\]。

## 自然環境

**植物**\[5\]

  - [日本景天](../Page/日本景天.md "wikilink")（石板菜）
  - [漆姑草](../Page/漆姑草.md "wikilink")
  - 日本百金花（，百金）

## 登山資訊

[雞籠山登山步道.JPG](https://zh.wikipedia.org/wiki/File:雞籠山登山步道.JPG "fig:雞籠山登山步道.JPG")
登山口位於九份的舊鐵道與隔頂站之間：搭車過九份基山街老街入口，繼續往金瓜石的方向走兩百公尺，到達隔頂，左側路邊即有「雞籠山登山步道」石碑。從登山口到山頂路途中有三座涼亭可供休憩，山頂有[天線](../Page/天線.md "wikilink")[基地台和觀景平台觀濤坪](../Page/基地台.md "wikilink")，可眺望[八斗子](../Page/八斗子.md "wikilink")、[瑞濱的鋸齒狀海景及山下的九份](../Page/瑞濱.md "wikilink")、金瓜石聚落。路程約莫30至40分鐘。

### 望基隆山

<File:KeelungMountain_0001.jpg>| 基隆山稜線 <File:基隆山雲海IMG> 7378.jpg|基隆山雲海
<File:茶壺山望基隆山DSC> 0358.jpg|由茶壺山望基隆山 <File:Keelung>
Mountain.jpg|從[深澳岬角眺望基隆山](../Page/深澳岬角.md "wikilink")

### 在基隆山

[File:基隆山0146.jpeg|遠眺九份夜景](File:基隆山0146.jpeg%7C遠眺九份夜景)
[File:基隆山P1070144.jpg|遠眺台北101](File:基隆山P1070144.jpg%7C遠眺台北101)
[File:基隆山P1070020.jpg|遠眺蕃子澳](File:基隆山P1070020.jpg%7C遠眺蕃子澳)
[File:基隆山遠眺金瓜石夜景P1070140.jpg|遠眺金瓜石夜景](File:基隆山遠眺金瓜石夜景P1070140.jpg%7C遠眺金瓜石夜景)
[File:基隆山落日DSCF3659.jpg|看落日](File:基隆山落日DSCF3659.jpg%7C看落日)
[File:基隆山落日DSCF3624.JPG|看落日](File:基隆山落日DSCF3624.JPG%7C看落日)
[File:遠眺金瓜石山城P1060904.JPG|遠眺金瓜石山城](File:遠眺金瓜石山城P1060904.JPG%7C遠眺金瓜石山城)

## 相關條目

  - [九份](../Page/九份.md "wikilink")
  - [金瓜石](../Page/金瓜石.md "wikilink")
  - [金瓜石社](../Page/金瓜石社.md "wikilink")
  - [新北市立黃金博物館](../Page/新北市立黃金博物館.md "wikilink")
  - [金瓜石戰俘營](../Page/金瓜石戰俘營.md "wikilink")
  - [金瓜石勸濟堂](../Page/金瓜石勸濟堂.md "wikilink")
  - [陰陽海](../Page/陰陽海.md "wikilink")
  - [水湳洞](../Page/水湳洞.md "wikilink")

## 參考資料

  - 書目

<!-- end list -->

  -
<!-- end list -->

  - 引用

## 外部連結

[Category:新北市山峰](../Category/新北市山峰.md "wikilink")
[Category:台灣火山](../Category/台灣火山.md "wikilink")
[Category:台灣小百岳](../Category/台灣小百岳.md "wikilink")
[Category:台灣鄉土富士](../Category/台灣鄉土富士.md "wikilink")
[Category:基隆北海岸](../Category/基隆北海岸.md "wikilink")
[Category:瑞芳區](../Category/瑞芳區.md "wikilink")

1.

2.

3.  「九份 － 戀戀風塵」，新北市政府觀光旅遊局，2012年3月。

4.
5.