**堂真理子**（）是[日本](../Page/日本.md "wikilink")[朝日電視台](../Page/朝日電視台.md "wikilink")[播報員](../Page/播報員.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。[血型A型](../Page/血型.md "wikilink")。

## 人物

  - 自13歳起5年之間，曾待過[瑞士](../Page/瑞士.md "wikilink")[蘇黎世](../Page/蘇黎世.md "wikilink")，為歸國子女。
  - [桐朋女子高等學校](../Page/桐朋女子中學校・高等學校.md "wikilink")→[青山學院大學](../Page/青山學院大學.md "wikilink")[文學部英美文學科畢業](../Page/文學部.md "wikilink")。2004年4月，進入朝日電視臺擔任播報員。
  - [傳播媒體](../Page/傳播媒體.md "wikilink")[就職活動的方法](../Page/就職活動.md "wikilink")，在青山學院大學的入學簡介中有介紹。
  - 入社的同時被提拔為「[Music
    Station](../Page/Music_Station.md "wikilink")」第8代助理主持人。
  - 入社第3年2006年4月起，擔任朝日電視台晨間看板新聞節目「」之工作，開始早上進行工作。
  - 比較容易發生錯誤的是名字的羅馬拼音字標記不是**「DOU」**應為**「DO」**。
  - 持有普通[汽車](../Page/汽車.md "wikilink")[駕照](../Page/駕照.md "wikilink")、[日本漢字能力檢定](../Page/日本漢字能力檢定.md "wikilink")3級、[實用英語技能檢定準](../Page/實用英語技能檢定.md "wikilink")1級等資格。[TOEIC](../Page/TOEIC.md "wikilink")890分。
  - 喜歡的歌手是[Mr.Children的](../Page/Mr.Children.md "wikilink")[櫻井和壽](../Page/櫻井和壽.md "wikilink")，首次買的CD據說也是Mr.Children。
  - 在學生時代起就是[妻夫木聰的影迷](../Page/妻夫木聰.md "wikilink")。
  - 理想的男性類型是大方、笑容可愛的人。
  - 視力不良使用[隱形眼鏡](../Page/隱形眼鏡.md "wikilink")。
  - 在2007年1月12日的[MUSIC
    STATION](../Page/MUSIC_STATION.md "wikilink")，塔摩利說過「希望堂主播變得更性感！」，自此以後似乎也有意識到此事。

## 軼事

  - 擔任[Music
    Station的第二回時](../Page/Music_Station.md "wikilink")，將演出者[布袋寅泰的名字叫錯為](../Page/布袋寅泰.md "wikilink")**「」**，遭到來自歌迷的責難。之後，布袋於自己的公式網站有寫後續的回應。
  - 於新人時代，在參與「3（台譯：辣妹圍裙）」演出的時候，因為泡芙的麵糊燒焦，出現了在錄影中哭泣而前輩[武内繪美播報員在一旁安慰的場面](../Page/武内繪美.md "wikilink")（來賓[伊集院光給予嚴酷的評價](../Page/伊集院光.md "wikilink")，評語是「離朝日電視台還有一點距離（）」）
  - 在深夜綜藝節目[草野☆キッド中](../Page/草野☆キッド.md "wikilink")，於**｢●野仁｣**的●要寫**｢草｣**或**｢只｣**這個企畫中，曾發生寫**｢只｣**這件事。同節目也曾發生有妹妹菜保子代理主持情形。
  - 在06年1月17日播送的中之企畫，附有簽名的夾克在慈善[Yahoo\!拍賣中以](../Page/Yahoo!拍賣.md "wikilink")**12萬7500元日幣**賣出。
  - 為了晨間節目必須於週一至週四1時30分起床，因為有時單靠自己無法起床，因而需靠同居的妹妹幫忙。
  - 在[Music
    Station中](../Page/Music_Station.md "wikilink")，談到[今井翼的中文語音為](../Page/今井翼.md "wikilink")「（日文音近男性生殖器官之意）」時，曾發生主持人[塔摩利催促她講出](../Page/塔摩利.md "wikilink")「」的發言。
  - 在**voice5**(05年9月10～11日)中預定演出**女子高中生**但與總選舉的日程重疊而終止，但於「」中有披露[制服的造型](../Page/制服.md "wikilink")。
  - 在'''voice5 Returns
    '''(06年9月23～24日)中演出總理大臣秘書，但卻以[女僕服造型演出](../Page/女僕服.md "wikilink")。
  - 在同劇的記者會，與[武内繪美](../Page/武内繪美.md "wikilink")、[河野明子](../Page/河野明子.md "wikilink")3人是結婚禮服的造型。
  - 在06年11月與[西尾由佳理](../Page/西尾由佳理.md "wikilink")（[日本電視臺播報員](../Page/日本電視臺.md "wikilink")）、[竹内香苗](../Page/竹内香苗.md "wikilink")（[TBS播報員](../Page/東京放送.md "wikilink")）共同為[ザテレビジョン的](../Page/ザテレビジョン.md "wikilink")**封面**。
  - 作為DVD「幻」中的特典影像，收錄有未公開影像拳擊等七變化。

## 現在擔任之節目

  - \- 2005年4月5日-

  - 1部（周一、周二）- （2007年10月1日-2007年12月25日 /
    2008年3月31日-）、（周一-周三）（2008年1月7日-2008年3月26日）

  - [ナニコレ珍百景](../Page/ナニコレ珍百景.md "wikilink")- 2008年1月23日-

  - [News
    Access](../Page/News_Access.md "wikilink")（[BS朝日](../Page/BS朝日.md "wikilink")、不定期）

## 過去擔任之節目

  - （週一～週四）（2006年4月 - 2006年5月）

  - （不定期　～2006年3月）

  - [News
    Access](../Page/News_Access.md "wikilink")（[BS朝日](../Page/BS朝日.md "wikilink")、不定期）

  -
  -
  - [Mini Sute](../Page/Mini_Sute.md "wikilink")- （2004年4月-2008年9月5日）

  - [Music Station](../Page/Music_Station.md "wikilink")- （2004年4月 -
    2008年9月12日）

## DVD演出

## 雜誌連載

  - （2004年6月號 - 2005年3月號、全10回）

## 同期播報員

  - [佐佐木亮太](../Page/佐佐木亮太.md "wikilink")
  - [上宮菜菜子](../Page/上宮菜菜子.md "wikilink")

## 關連項目

  - [朝日電視臺](../Page/朝日電視臺.md "wikilink")
  - [日本播報員一覽](../Page/日本播報員一覽.md "wikilink")

## 外部連結

  - [朝日電視臺官方網頁](http://www.tv-asahi.co.jp/announcer/personal/women/dou/body1.html)
  - [Music Station
    Blog・](https://web.archive.org/web/20070210041704/http://www.tv-asahi.co.jp/music/contents/domari/)
  - [](http://www.tv-asahi.co.jp/announcer/special/newface2004/body.html)

[Category:朝日電視網播報員](../Category/朝日電視網播報員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:MUSIC STATION](../Category/MUSIC_STATION.md "wikilink")
[Category:青山學院大學校友](../Category/青山學院大學校友.md "wikilink")