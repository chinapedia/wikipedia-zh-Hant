[HK_Junction_MorrisonHillRoad_TinLokLane.JPG](https://zh.wikipedia.org/wiki/File:HK_Junction_MorrisonHillRoad_TinLokLane.JPG "fig:HK_Junction_MorrisonHillRoad_TinLokLane.JPG")與堅拿道之交匯點，遠望[灣仔道該處才是](../Page/灣仔道.md "wikilink")[天樂里的所在地](../Page/天樂里.md "wikilink")。\]\]

**堅拿道東**及**堅拿道西**是位於[香港](../Page/香港.md "wikilink")[港島](../Page/香港島.md "wikilink")[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[灣仔交界的街道](../Page/灣仔.md "wikilink")，沿以前的[黃泥涌](../Page/黃泥涌.md "wikilink")（[寶靈頓運河](../Page/寶靈頓運河.md "wikilink")）河道兩岸興建，因此堅拿道東與堅拿道西實際上是兩條並排南北走向的道路，而不是一條道路分成東西段。

「堅拿」是[英文Canal的](../Page/英文.md "wikilink")[粵語譯音](../Page/粵語.md "wikilink")，意思為[運河](../Page/運河.md "wikilink")。堅拿道的前身，是從跑馬地流向[維多利亞港的黃泥涌的河口](../Page/維多利亞港.md "wikilink")，其後於1860年代末擴建為[寶靈頓運河](../Page/寶靈頓運河.md "wikilink")。堅拿道東及堅拿道西是在這運河兩旁的道路。由於這條運河又長又窄，彎曲成[鵝頸的形狀](../Page/鵝.md "wikilink")，因此被當時的人稱為「[鵝頸澗](../Page/鵝頸澗.md "wikilink")」，橫過這段河道的橋便稱為「鵝頸橋」，該區附近一帶更得名為「[鵝頸區](../Page/鵝頸區.md "wikilink")」。後來於1922年至1929年，[香港政府於](../Page/香港政府.md "wikilink")[灣仔進行](../Page/灣仔.md "wikilink")[填海工程](../Page/填海.md "wikilink")，舊有的鵝頸橋被拆卸，建成大致成為現時的電車路的模樣。而軒尼詩道以北的一段寶靈頓運河不久亦變成暗渠，到了1960年代末為了配合[香港海底隧道的通車](../Page/香港海底隧道.md "wikilink")，香港政府更把於軒尼詩道以南，即電車公司以西的一段寶靈頓明渠填平，及後並在上方興建[堅拿道天橋](../Page/堅拿道天橋.md "wikilink")，俗稱的「鵝頸橋」也從此變成了行車天橋。其後為了配合香港仔隧道的通車，政府興建了[黃泥涌峽天橋](../Page/黃泥涌峽天橋.md "wikilink")，連接堅拿道天橋及[香港仔隧道](../Page/香港仔隧道.md "wikilink")[跑馬地出口](../Page/跑馬地.md "wikilink")。

Canal_1920s.jpg|1920年代的鵝頸橋 Canal Road Flyover Villain hitting
2018.jpg|鵝頸橋下打小人 Canal Road Flyover Bus stop 2017.jpg|鵝頸橋下的巴士站

## 著名地點

  - [時代廣場](../Page/時代廣場.md "wikilink")
  - [伊利莎伯大廈](../Page/伊利莎伯大廈.md "wikilink")
  - [堅拿道天橋底](../Page/堅拿道天橋.md "wikilink")[打小人](../Page/打小人.md "wikilink")
  - [寶靈頓道街市](../Page/寶靈頓道.md "wikilink")

## 相關

  - [黃泥涌村](../Page/黃泥涌村.md "wikilink")

[Category:灣仔街道](../Category/灣仔街道.md "wikilink")