**堅江省**（）位於[越南](../Page/越南.md "wikilink")，首府[迪石市](../Page/迪石市.md "wikilink")，區屬[湄公河三角洲](../Page/湄公河三角洲.md "wikilink")，北與[安江省](../Page/安江省.md "wikilink")、[柬埔寨相鄰](../Page/柬埔寨.md "wikilink")，南接[金甌省](../Page/金甌省.md "wikilink")，西鄰[泰國灣](../Page/泰國灣.md "wikilink")，東鄰[芹苴市](../Page/芹苴市.md "wikilink")。

[富國胡椒是堅江省的特產](../Page/富國胡椒.md "wikilink")。

## 历史

1956年，[越南共和国政府将](../Page/越南共和国.md "wikilink")[河仙省和](../Page/河仙省.md "wikilink")[沥架省合并为坚江省](../Page/沥架省.md "wikilink")，省莅位于沥架。

## 行政區劃

坚江省共辖2市13县，省莅在迪石市。

  - [迪石市](../Page/迪石市.md "wikilink")（Thành phố Rạch Giá）
  - [河仙市](../Page/河仙市.md "wikilink")（Thành phố Hà Tiên）
  - [安邊縣](../Page/安邊縣.md "wikilink")（Huyện An Biên）
  - [安明縣](../Page/安明縣.md "wikilink")（Huyện An Minh）
  - [周城縣](../Page/周城縣_\(堅江省\).md "wikilink")（Huyện Châu Thành）
  - [榕蓮縣](../Page/榕蓮縣.md "wikilink")（Huyện Giồng Riềng）
  - [鹅瓜縣](../Page/鹅瓜縣.md "wikilink")（Huyện Gò Quao）
  - [昏德縣](../Page/昏德縣.md "wikilink")（Huyện Hòn Đất）
  - [堅海縣](../Page/堅海縣.md "wikilink")（Huyện Kiên Hải）
  - [堅良縣](../Page/堅良縣.md "wikilink")（Huyện Kiên Lương）
  - [富國島縣](../Page/富國島縣.md "wikilink")（Huyện Đảo Phú Quốc）
  - [新協縣](../Page/新協縣.md "wikilink")（Huyện Tân Hiệp）
  - [永順縣](../Page/永順縣_\(越南\).md "wikilink")（Huyện Vĩnh Thuận）
  - [乌明上縣](../Page/乌明上縣.md "wikilink")（Huyện U Minh Thượng）
  - [江城县](../Page/江城县_\(越南\).md "wikilink")（Huyện Giang Thành）

## 参考文献

## 外部链接

  - [堅江省政府电子通信门户网站](http://www.kiengiang.gov.vn/)

## 參見

  - [河仙镇](../Page/河仙镇.md "wikilink")

{{-}}

[Category:越南省份](../Category/越南省份.md "wikilink")
[坚江省](../Category/坚江省.md "wikilink")