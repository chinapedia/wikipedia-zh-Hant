**堤岸**（\[1\]，）是[越南](../Page/越南.md "wikilink")[胡志明市的一個地區名稱](../Page/胡志明市.md "wikilink")，位於[西貢河西岸](../Page/西貢河.md "wikilink")，地界范围涵盖胡志明市[第五郡的西部和](../Page/胡志明市第五郡.md "wikilink")[第六郡部分區域](../Page/胡志明市第六郡.md "wikilink")。此地因為是越南規模最大的[華人聚居地和著名觀光景點而聞名](../Page/唐人街.md "wikilink")\[2\]，[胡志明市的數十萬華人中大多都集中住在堤岸地區](../Page/胡志明市華人.md "wikilink")\[3\]，並有「**小香港**」的別稱，每日出入人潮達數十萬之多\[4\]。此區內分布著數座中國式樓房、商店、寺廟和學校，歷史達100多年的是當地數座市場中最大的一個。

堤岸的越南文[國語字為](../Page/國語字.md "wikilink")「」，其對應的越南文[喃字為](../Page/喃字.md "wikilink")「」。其中“”意為“市場”，“”意為“大”。“堤岸”是在越華人為其起的中文名稱。在越南聚居的華人當中，以[廣府人為主](../Page/廣府人.md "wikilink")，[潮州人居次](../Page/潮州人.md "wikilink")，而[客家人和](../Page/客家人.md "wikilink")[閩南人則長期處於弱勢地位](../Page/閩南人.md "wikilink")，故此堤岸城內廣東食肆成行成市，而且區內最通用的主要語言是[粵語](../Page/粵語.md "wikilink")\[5\]。

在堤岸地区，多条街道以中国古代圣贤命名，包括[孔子街](../Page/孔子.md "wikilink")（）、[老子街](../Page/老子.md "wikilink")（）等。

## 歷史

1778年時，居住於今西貢東北方的[邊和的鋪洲華人社群由於支援過](../Page/邊和市.md "wikilink")[廣南國](../Page/廣南國.md "wikilink")，而受到[西山朝派兵報復的緣故](../Page/西山朝.md "wikilink")，必須南遷到今日堤岸的所在地避難。1782年，這些華人再度受到西山朝軍隊剿殺，而必須重建居所。因為華人在西貢河邊築起壩堤防範水患，而使他們逐漸使用「堤岸」來當成這個新居住地的名稱\[6\]。

與今西貢市中心大約相距10公里的堤岸，在1879年建市，1930年時已擴展到西貢的邊緣，因此在1931年4月27日由[法國殖民當局將兩者合併為](../Page/法屬印度支那.md "wikilink")「西貢堤岸」地區，並稱「西堤」\[7\]，但維持兩市分別的工部局和市長。1955年南越脫離法國獨立後在西貢建都，因此1956年正式將兩市合併，將「堤岸」從城市名稱中剔除，只稱「西貢」\[8\]。

[越南戰爭時期](../Page/越南戰爭.md "wikilink")，堤岸人口一度增長到100多萬人之譜\[9\]，[駐越美軍司令部](../Page/駐越美軍司令部.md "wikilink")（）、[美國海軍西貢支援設施](../Page/美國海軍.md "wikilink")（）和[美軍超市](../Page/美軍超市.md "wikilink")（）都曾設於此區\[10\]，美軍將士和逃兵在堤岸的大量活動形成了熱絡的[黑市交易](../Page/黑市.md "wikilink")，許多舶來品或美軍內部配給的物資從此區流入越南民間\[11\]。1975年[西貢陷落前的](../Page/西貢陷落.md "wikilink")4月27日，共軍火砲曾落入堤岸的民房區，造成傷亡\[12\]。

戰爭結束後越南政府推動的城鄉人口政策\[13\]和[排華政策一度造成堤岸地區華民的出走](../Page/排華.md "wikilink")\[14\]，直到1980年代黨中央開始改善越南華僑地位為止。近年來由於胡志明市的擴張和發展，使得堤岸部分華人搬遷到[第二郡](../Page/胡志明市第二郡.md "wikilink")、[第七郡](../Page/胡志明市第七郡.md "wikilink")、[富美興](../Page/富美興都市區.md "wikilink")、[茹𦨭縣等新興市區](../Page/茹𦨭縣.md "wikilink")\[15\]。

## 著名設施

  - [新街市](../Page/新街市.md "wikilink")
  - [溫陵會館](../Page/溫陵會館.md "wikilink")（堤岸觀音廟）
  - [胡志明市天后廟](../Page/胡志明市天后廟.md "wikilink")
  - [二府会馆](../Page/二府会馆.md "wikilink")
  - [明鄉嘉盛會館](../Page/明鄉嘉盛會館.md "wikilink")
  - [義安會館](../Page/義安會館.md "wikilink")
  - [玉皇殿](../Page/玉皇殿.md "wikilink")
  - [圣方济各天主堂](../Page/圣方济各天主堂_\(胡志明市\).md "wikilink")
  - [瓊府會館](../Page/瓊府會館.md "wikilink")

## 著名居住者

  - ，出生於堤岸的法國人，於1946年贏得[溫布頓男單冠軍](../Page/溫布頓網球錦標賽.md "wikilink")。

  - [高文園](../Page/高文園.md "wikilink")，前[南越陸軍](../Page/南越陸軍.md "wikilink")[上將](../Page/上將.md "wikilink")、三軍參謀總長\[16\]\[17\]。

  - [呂明光](../Page/尹光.md "wikilink")，藝名尹光，著名香港歌手。

  - [許偉文](../Page/許偉文.md "wikilink")，出生於堤岸的華裔越籍演員、歌手。

## 相關條目

  - [全球唐人街列表](../Page/唐人街列表.md "wikilink")
  - [越南華人](../Page/越南華人.md "wikilink")
  - [胡志明市華人](../Page/胡志明市華人.md "wikilink")

## 注釋及參考來源

## 外部連結

  - [劉為安散文集《堤岸今昔》](http://www.fengtipoeticclub.com/book/ChoLon/menu.html)

[Category:胡志明市](../Category/胡志明市.md "wikilink")
[Category:亞洲唐人街](../Category/亞洲唐人街.md "wikilink")
[Category:越南華人](../Category/越南華人.md "wikilink")

1.  “𢄂𡘯”见于《[南圻六省地舆志](../Page/南圻六省地舆志.md "wikilink")》，“𢄂𢀲”见于《[南國地輿誌](../Page/南國地輿誌.md "wikilink")》、《东洋政治》等书。

2.  [Chợ Lớn (Big Market)
    vietnam-beauty.com](http://www.vietnam-beauty.com/cities/ho-chi-minh-city/5-ho-chi-minh-city/67-ch-ln-big-market.html)

3.  [堤岸——全球最大的唐人街(組圖)](http://gb.cri.cn/14404/2007/07/06/2685@1666637.htm)
    - 國際線上 - 新聞中心

4.  [胡志明市 » 旅越須知 » 越南旅遊焦點](http://tecohcm.org.vn/tourguide-detail/3/43)
    －駐胡志明市[台北經濟文化辦事處](../Page/台北經濟文化辦事處.md "wikilink")

5.
6.
7.  [胡志明市堤岸區：粵語通行的「小香港」](http://news.xinhuanet.com/overseas/2005-04/07/content_2799756.htm)－新華網

8.
9.  [憶昔日西貢華人區－堤岸今與昔](http://thonhonschool.com/Travel_Cholon%20page.htm)

10. [A Vietnam War Clerk's Diary: Diary Entry 6: Saigon, Friday, 11
    June 1965](http://vietnamwarclerksdiary.blogspot.tw/2011/01/diary-entry-6-saigon-friday-11-june.html)

11. [A Vietnam War Clerk's Diary: From The Editor: The Navy Exchange,
    Hair Spray, HSAS, and
    Corruption](http://vietnamwarclerksdiary.blogspot.tw/2011/02/from-editor-navy-exchange-hair-spray.html)

12. [約翰·皮爾格](../Page/約翰·皮爾格.md "wikilink")，2005，〈[The Fall of
    Saigon 1975: An Eyewitness Report - by John
    Pilger](http://www.antiwar.com/orig/pilger.php?articleid=5579)〉

13. Dawson, 351.

14.
15. [劉為安散文集《堤岸今昔》
    （十三）今日的堤岸](http://www.fengtipoeticclub.com/book/ChoLon/ch58.html)

16. [St. George, Donna. "Cao Van
    Vien, 1921-2008."](http://www.washingtonpost.com/wp-dyn/content/article/2009/01/02/AR2009010200923.html)
    *Washington Post.* January 2, 2009.

17. Kinnard, Douglas. *The War Managers: American Generals Reflect on
    Vietnam.* Reprint ed. Cambridge, Massachusetts: Da Capo Press, 1991.
    ISBN 0-306-80449-2