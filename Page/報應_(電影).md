是2011年[羅永昌導演的](../Page/羅永昌.md "wikilink")[香港電影](../Page/香港電影.md "wikilink")，[杜琪峰任監製](../Page/杜琪峰.md "wikilink")。於2011年5月5日在香港上映。宣傳標語为「**一念天堂，取之救贖，用之殺戳**」。

本片為[第三十五屆](../Page/第三十五屆香港國際電影節.md "wikilink")[香港國際電影節參展作品](../Page/香港國際電影節.md "wikilink")，於2011年4月4日作世界首映。

由于审查问题，该片在中国大陆名稱改为《迷途追凶》于2011年7月12日上映，上映时做了删减并更换了结局（原版结局是王浩潮(黄秋生飾)及姚啟初(任賢齊飾)放过Amy(卢巧音飾)后，任贤齐在车里远远的看着自己的兒子，黄秋生去到女儿Daisy(文詠珊飾)想去的[玻利维亚天镜之湖](../Page/玻利维亚.md "wikilink")，撒了女儿的骨灰，就此结束。而大陆版本附加的结局是两位男主角最终自首并被判[终身监禁](../Page/终身监禁.md "wikilink")）。

## 故事

房地产富商王浩潮（黃秋生飾演）的女兒被綁架了，要他交纳赎金5000万港圆。他懷疑是反叛女兒Daisy（文詠珊飾）自導自演的敲詐把戲，可是變卦驟起，王浩潮的一個錯誤決定，引致嚴重後果。痛失女兒的王浩潮，委派前任保鏢姚啟初（任賢齊飾演）向一眾綁匪展開追查及復仇行動。但正當啟初將綁匪們逐一解決的同時，王浩潮自己亦跌入一個萬劫不復的境地……

## 演員表

  - [黃秋生](../Page/黃秋生.md "wikilink") 飾 王浩潮（Daisy及亞俊的父親，姚啟初、Amy及T.K.的老闆）
  - [任賢齊](../Page/任賢齊.md "wikilink") 飾 姚啟初（王浩潮的前任保鑣）
  - [區澤鋒](../Page/區澤鋒.md "wikilink") 飾 強仔 Jason (姚啟初的兒子)
  - [張可頤](../Page/張可頤.md "wikilink") 飾 王太太（王浩潮的第二任妻子，Daisy的继母）
  - [文詠珊](../Page/文詠珊.md "wikilink") 飾
    Daisy（王浩潮的長女，瘾君子，梦想是去[玻利维亚的](../Page/玻利维亚.md "wikilink")[烏尤尼鹽沼](../Page/烏尤尼鹽沼.md "wikilink")
    {天空之镜}）
  - [盧巧音](../Page/盧巧音.md "wikilink") 飾 Amy （王浩潮的僱員，Daisy的保姆）
  - [曹查理](../Page/曹查理.md "wikilink") 飾 T.K.（王浩潮的下属，负责收购村民地皮的项目）
  - [林　利](../Page/林利.md "wikilink") 飾 亞鵬（绑架案的老大）
  - [林敬剛](../Page/林敬剛.md "wikilink") 飾 綁匪甲（綁匪乙的表兄弟）
  - [恭碩良](../Page/恭碩良.md "wikilink") 飾 綁匪乙（綁匪甲的表兄弟）
  - [黃一一](../Page/黃一一.md "wikilink") 飾 亞俊（王浩潮的幼子，Daisy的弟弟）
  - [盧文康](../Page/盧文康.md "wikilink") 飾 王浩潮的新保鑣
  - [江美儀](../Page/江美儀.md "wikilink") 飾 王浩潮已過世的妻子（Daisy的母親）
  - [徐忠信](../Page/徐忠信.md "wikilink") 飾
  - [阮民安](../Page/阮民安.md "wikilink") 飾

## 工作人員

  - 導演：[羅永昌](../Page/羅永昌.md "wikilink")
  - 編劇：[馮志強](../Page/馮志強.md "wikilink")、[林逢](../Page/林逢.md "wikilink")
  - 監製：[杜琪峰](../Page/杜琪峰.md "wikilink")
  - 出品人：[莊澄](../Page/莊澄.md "wikilink")
  - 攝影指導：[高照林](../Page/高照林.md "wikilink")
    ([HKSC](../Page/HKSC.md "wikilink"))
  - 剪接：[David
    Richardson](../Page/David_Richardson.md "wikilink")、[彭正熙](../Page/彭正熙.md "wikilink")
  - 原創音樂：[Guy Zerafa](../Page/Guy_Zerafa.md "wikilink")、[Dave
    Klotz](../Page/Dave_Klotz.md "wikilink")
  - 動作指導：[黃偉亮](../Page/黃偉亮.md "wikilink")

## 開拍經緯

  - 2010年1月19日：於觀塘銀河映像總社開機拍攝。\[1\]
  - 時期不明：電影低調關機，進行後期製作。
  - 2011年3月9日：宣佈參展[第三十五屆](../Page/第三十五屆香港國際電影節.md "wikilink")[香港國際電影節](../Page/香港國際電影節.md "wikilink")，於2011年4月4日，[香港文化中心大劇院中作僅一場的世界首映](../Page/香港文化中心.md "wikilink")。\[2\]
  - 預定於2011年5月5日在香港正式上映。

## 軼事

  - 據紀錄，本片為[高照林繼](../Page/高照林.md "wikilink")《[暗花](../Page/暗花.md "wikilink")》、《[非常突然](../Page/非常突然.md "wikilink")》後，相隔十三年為銀河映像作品擔任攝影指導。
  - 此外，曾和[劉偉強和](../Page/劉偉強.md "wikilink")[彭氏兄弟有長期合作關係的剪接師](../Page/彭氏兄弟.md "wikilink")[彭正熙](../Page/彭正熙.md "wikilink")，首次擔任銀河映像作品的剪接。但剪接師出身的本片導演羅永昌則沒有擔任剪接該崗位。
  - 由於[任賢齊的粵語發音不是很標準](../Page/任賢齊.md "wikilink")，所以他飾演的角色會由[歐錦棠配音](../Page/歐錦棠.md "wikilink")。

## 參考資料

<references />

## 外部連結

  -
  - [香港國際電影節官方介紹](http://www.hkiff.org.hk/chi/film/detail/35090.html)，於2011年3月9日查閱。

  -
  - {{@movies|fbhk41604670|報應}}

  -
  -
  -
  -
  -
  -
[1](../Category/2010年代香港電影作品.md "wikilink")
[Category:香港犯罪片](../Category/香港犯罪片.md "wikilink")
[Category:粤语电影](../Category/粤语电影.md "wikilink")
[Category:銀河映像電影](../Category/銀河映像電影.md "wikilink")
[Category:杜琪峰電影](../Category/杜琪峰電影.md "wikilink")
[Category:寰亞電影](../Category/寰亞電影.md "wikilink")
[Category:報復題材電影](../Category/報復題材電影.md "wikilink")
[Category:悲劇電影](../Category/悲劇電影.md "wikilink")

1.  [1](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100120&sec_id=462&subsec_id=830&art_id=13644005#Scene_1)
    香港[蘋果日報娛樂新聞](../Page/蘋果日報.md "wikilink")，2010年1月20日
2.  [2](http://www.facebook.com/notes/%E5%A0%B1%E6%87%89-abduction/%E7%AC%AC35%E5%B1%86%E9%A6%99%E6%B8%AF%E5%9C%8B%E9%9A%9B%E9%9B%BB%E5%BD%B1%E7%AF%80%E9%9A%86%E9%87%8D%E5%91%88%E7%8D%BB%E5%A0%B1%E6%87%89%E4%B8%96%E7%95%8C%E9%A6%96%E6%98%A0/10150104095107231)
    官方Facebook專頁新聞稿，於2011年3月9日查閱。