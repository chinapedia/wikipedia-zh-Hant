[缩略图](https://zh.wikipedia.org/wiki/File:Tower-and-Stockade_Ein-Hashofet.jpg "fig:缩略图")
**塔与栅栏**（[希伯来语](../Page/希伯来语.md "wikilink"):*' חומה ומגדל*'
；[英语](../Page/英语.md "wikilink")：**Tower and
stockade**）是1936年-1939年巴勒斯坦阿拉伯起义期间，犹太复国运动者在英国托管巴勒斯坦建立定居点的方法。当时新建犹太定居点受到托管政府的限制。这个计划旨在移入犹太人，使他们能获取等多的土地，特别在偏远地区，造成既成事实。它们最终都得到增强，转变成农业定居点。在这次战役中获取的所有主要定居点（大部分是[基布兹集体农场](../Page/基布兹.md "wikilink")），都包括一个警戒塔河和周围的一道栅栏，通常在一夜之间形成。

在“塔与栅栏”战役期间，全国共新建了52个犹太人定居点。

  - [Kfar Hittim](../Page/Kfar_Hittim.md "wikilink"), 1936年12月7日
  - [Tel Amal](../Page/Tel_Amal.md "wikilink")（現今[Nir
    David](../Page/Nir_David.md "wikilink")）, 1936年12月10日
  - [Sde Nahum](../Page/Sde_Nahum.md "wikilink"), 1937年1月5日
  - [Shaar HaGolan](../Page/Shaar_HaGolan.md "wikilink"), 1937年1月31日
  - [Masada (kibbutz)](../Page/Masada_\(kibbutz\).md "wikilink"),
    1937年1月31日
  - [Ginosar](../Page/Ginosar.md "wikilink"), 1937年2月25日
  - [Beit Yoseph](../Page/Beit_Yoseph.md "wikilink"), 1937年4月9日
  - [Mishmar HaShelosha](../Page/Mishmar_HaShelosha.md "wikilink"),
    1937年4月13日
  - [Tirat Tzvi](../Page/Tirat_Tzvi.md "wikilink"), 1937年6月30日
  - [Bnei Brit](../Page/Bnei_Brit.md "wikilink")（現今[Moledet
    (settlement)](../Page/Moledet_\(settlement\).md "wikilink")）,
    1937年7月4日
  - [Ein HaShofet](../Page/Ein_HaShofet.md "wikilink"),1937年7月5日
  - [Ein Gev](../Page/Ein_Gev.md "wikilink"), 1937年7月6日
  - [Maoz Hayyim](../Page/Maoz_Hayyim.md "wikilink"), 1937年7月6日
  - [Kfar Menahem](../Page/Kfar_Menahem.md "wikilink"), 1937年7月27日
  - [Kfar Szold](../Page/Kfar_Szold.md "wikilink"), 1937年8月15日
  - [Tzur Moshe](../Page/Tzur_Moshe.md "wikilink"), 1937年9月13日
  - [Usha](../Page/Usha.md "wikilink"), 1937年11月7日
  - [Hanita](../Page/Hanita.md "wikilink"), 1938年3月21日
  - [Shavei Zion](../Page/Shavei_Zion.md "wikilink"), 1938年4月13日
  - [Sde Warburg](../Page/Sde_Warburg.md "wikilink"), 1938年5月17日
  - [Ramat Hadar](../Page/Ramat_Hadar.md "wikilink"), 1938年5月26日
  - [Alonim](../Page/Alonim.md "wikilink"), 1938年6月26日
  - [Maale HaHamisha](../Page/Maale_HaHamisha.md "wikilink"), 1938年7月17日
  - [Tel Yitzchak](../Page/Tel_Yitzchak.md "wikilink"), 1938年7月25日
  - [Beit Yehoshua](../Page/Beit_Yehoshua.md "wikilink"), 1938年8月17日
  - [Ein HaMifratz](../Page/Ein_HaMifratz.md "wikilink"), 1938年8月25日
  - [Ma'ayan Tzvi](../Page/Ma'ayan_Tzvi.md "wikilink"), 1938年8月30日
  - [Sharona](../Page/Sharona.md "wikilink"), 1938年11月16日
  - [Geulim](../Page/Geulim.md "wikilink"), 1938年11月17日
  - [Ayalon](../Page/Ayalon.md "wikilink"), 1938年11月24日
  - [Neve Eitan](../Page/Neve_Eitan.md "wikilink"), 1938年11月25日
  - [Kfar Rupin](../Page/Kfar_Rupin.md "wikilink"), 1938年11月25日
  - [Kfar Masaryk](../Page/Kfar_Masaryk.md "wikilink"), 1938年11月29日
  - [Mesilot](../Page/Mesilot.md "wikilink"), 1938年12月22日
  - [Dalia](../Page/Dalia_\(kibbutz\).md "wikilink"), 1939年5月2日
  - [Dafna](../Page/Dafna.md "wikilink"),1939年5月3日
  - [Dan](../Page/Kibbutz_Dan.md "wikilink"), 1939年5月4日
  - [Sde Eliyahu](../Page/Sde_Eliyahu.md "wikilink"), 1939年5月8日
  - [Mahanayim](../Page/Mahanayim.md "wikilink"), 1939年5月23日
  - [Shadmot Devorah](../Page/Shadmot_Devorah.md "wikilink") 1939年5月23日
  - [Shorashim](../Page/Shorashim_\(moshav\).md "wikilink"), 1939年5月23日
  - [HaZor'im](../Page/HaZor'im.md "wikilink"), 1939年5月23日
  - [Tel Tzur](../Page/Tel_Tzur.md "wikilink"), 1939年5月23日
  - [Kfar Glickson](../Page/Kfar_Glickson.md "wikilink"), 1939年5月23日
  - [Ma'apilim](../Page/Ma'apilim.md "wikilink"), 1939年5月23日
  - [Mishmar HaYam](../Page/Mishmar_HaYam.md "wikilink"), 1939年5月28日
  - [Hamadiah](../Page/Hamadiah.md "wikilink"), 1939年6月23日
  - [Kfar Netter](../Page/Kfar_Netter.md "wikilink"), 1939年6月26日
  - [Negba](../Page/Negba.md "wikilink"), 1939年7月12日
  - [Gesher](../Page/Kibbutz_Gesher.md "wikilink"), 1939年8月13日
  - [Beit Oren](../Page/Beit_Oren.md "wikilink"), 1939年10月1日
  - [Amir](../Page/Amir_\(kibbutz\).md "wikilink"), 1939年10月29日

[Category:以色列历史](../Category/以色列历史.md "wikilink")