**塔博·姆贝基**（；），[南非政治家](../Page/南非.md "wikilink")，曾任南非副总统、[非洲人国民大会主席](../Page/非洲人国民大会.md "wikilink")，[南非总统](../Page/南非总统.md "wikilink")。

## 政治生涯

1956年，年仅14岁的姆贝基在短暂加入[托派组织](../Page/托派.md "wikilink")“青年非洲人协会”后，转投“非国大青年联盟”\[1\]。[苏东剧变时](../Page/苏东剧变.md "wikilink")，时任[南非共产党中央委员的姆贝基自动脱党](../Page/南非共产党.md "wikilink")\[2\]。1997年，接替[纳尔逊·曼德拉出任非國大主席](../Page/纳尔逊·曼德拉.md "wikilink")。1999年接替放棄連任的曼德拉繼任總統，同年領導執政非國大贏得大選，並於2004年成功連任。2008年9月21日，因被指干涉[雅各布·祖玛的司法案件](../Page/雅各布·祖玛.md "wikilink")，在南非执政党非国大要求下辞职。

## 参考资料

## 外部連結

  - ["Thabo Mbeki – a man of two
    faces"](http://www.economist.com/people/displayStory.cfm?story_id=3576543)
    *The Economist* magazine profiles Mbeki. pay/member link

[Category:薩塞克斯大學校友](../Category/薩塞克斯大學校友.md "wikilink")
[Category:南非总统](../Category/南非总统.md "wikilink")
[Category:非洲联盟主席](../Category/非洲联盟主席.md "wikilink")
[Category:非洲人国民大会成员](../Category/非洲人国民大会成员.md "wikilink")

1.  [Thabo Mvuyelwa
    Mbeki](https://www.sahistory.org.za/people/thabo-mvuyelwa-mbeki)
2.