**塔哈尔省**（）位於[阿富汗東北](../Page/阿富汗.md "wikilink")，與[巴達赫尚省](../Page/巴達赫尚省.md "wikilink")、[巴格蘭省](../Page/巴格蘭省.md "wikilink")、[昆都士省](../Page/昆都士省.md "wikilink")、[潘傑希爾省等省份及](../Page/潘傑希爾省.md "wikilink")[塔吉克相鄰](../Page/塔吉克.md "wikilink")。该省总面积12,333
km² (4,762 sq mi)，总人口810,800
(2004年)。主要通行语言为[波斯语](../Page/波斯语.md "wikilink")，[乌兹别克语](../Page/乌兹别克语.md "wikilink")。

该省辖有17个县。

## 参考文献

<references/>

## 外部链接

  - Dupree, Nancy Hatch (1977): *An Historical Guide to Afghanistan*.
    1st Edition: 1970. 2nd Edition. Revised and Enlarged. Afghan Tourist
    Organization. [1](http://www.zharov.com/dupree/index.html)
  - <http://www.trekearth.com/gallery/Asia/Afghanistan/East/Takhar/> (A
    beautiful gallery of pictures of Takhar province)

[T](../Category/阿富汗行政區劃.md "wikilink")