|                                                                                                                          |
| ------------------------------------------------------------------------------------------------------------------------ |
| <big>**塔拉納基**</big>                                                                                                      |
| [國家](../Page/List_of_countries.md "wikilink"):                                                                           |
| [Position_of_Taranaki.png](https://zh.wikipedia.org/wiki/File:Position_of_Taranaki.png "fig:Position_of_Taranaki.png") |
| [行政地理](../Page/:en:List_of_regions_in_New_Zealand.md "wikilink")                                                         |
| 名稱:                                                                                                                      |
| 人口:                                                                                                                      |
| 土地面積:                                                                                                                    |
| 席位 :                                                                                                                     |
| 副主席:                                                                                                                     |
| 網站:                                                                                                                      |
| [毛利族](../Page/毛利族.md "wikilink")                                                                                         |
| 當地毛利族                                                                                                                    |
| 城鎮                                                                                                                       |
| 城市:                                                                                                                      |
| 城鎮，村莊和地區:                                                                                                                |
| 領土當局                                                                                                                     |
| 網站:                                                                                                                      |
| 網站:                                                                                                                      |

**塔拉纳基大区**\[1\]([英文](../Page/英文.md "wikilink"): Taranaki Region;
[毛利語](../Page/毛利語.md "wikilink")：Ko Taranaki
Takiwā)，為[紐西蘭的行政大区之一](../Page/紐西蘭.md "wikilink")，位于[北岛西部](../Page/北岛.md "wikilink")。该大区总面积7,257km²，总人口108,100(2009)。\[2\]首府是[新普利茅斯市](../Page/新普利茅斯市.md "wikilink")。塔拉纳基大区包含[新普利茅斯区](../Page/新普利茅斯区.md "wikilink")、[南塔拉纳基区以及](../Page/南塔拉纳基区.md "wikilink")[斯特拉特福德区的一部分](../Page/斯特拉特福德区.md "wikilink")。

## 地理

[NEO_egmont_big.jpg](https://zh.wikipedia.org/wiki/File:NEO_egmont_big.jpg "fig:NEO_egmont_big.jpg")地球觀測的[塔拉納基山](../Page/塔拉納基山.md "wikilink")[衛星圖片](../Page/衛星圖.md "wikilink")。\]\]
塔拉纳基大区首府新普利茅斯市，是新西兰西海岸唯一的海港城市，并且也是全新西兰距离[澳大利亚最近的海港城市](../Page/澳大利亚.md "wikilink")。該地區的土壤非常肥沃，奶牛養殖佔主導地位，[哈維拉的牛奶廠是南半球第二大的](../Page/哈維拉.md "wikilink")。塔拉纳基大区也有[石油和](../Page/石油.md "wikilink")[天然氣的礦藏無論是在岸或離岸](../Page/天然氣.md "wikilink")。在西南海岸之[毛伊天然氣田](../Page/毛伊天然氣田.md "wikilink")(Maui
Gas
Field)供應新西蘭大部分的[天然氣](../Page/天然氣.md "wikilink")，也曾经支援过两个[甲醇廠在](../Page/甲醇廠.md "wikilink")[摩突努伊](../Page/摩突努伊.md "wikilink")。更多[燃料和](../Page/燃料.md "wikilink")[化肥是在](../Page/化肥.md "wikilink")[卡普尼天然氣田](../Page/卡普尼天然氣田.md "wikilink")(Kapuni
Gas
Field)及其他[油井生产](../Page/油井.md "wikilink")。离哈維拉以南五十公里的[途易油田](../Page/途易油田.md "wikilink")（Tui
Oil
Field）含有五千万桶（790万立方米）之儲量。至2009年[瓦伊塔拉以北](../Page/瓦伊塔拉.md "wikilink")4.5公里的[波霍庫拉氣田](../Page/波霍庫拉氣田.md "wikilink")(Pohokura
Gas Field)
目前是[新西蘭最大的](../Page/新西蘭.md "wikilink")[天然氣](../Page/天然氣.md "wikilink")[生產領域](../Page/生產.md "wikilink"),
并製造量佔總生產量之42%。

## 经济

[Nzgasfield.svg](https://zh.wikipedia.org/wiki/File:Nzgasfield.svg "fig:Nzgasfield.svg")[氣田](../Page/氣田.md "wikilink")[生產量](../Page/生產.md "wikilink")。\]\]
[新普利茅斯市](../Page/新普利茅斯市.md "wikilink")，是塔拉纳基大区主要经济中心，以田园农业为主（主要是乳品业），同时也有[石油](../Page/石油.md "wikilink")，[天然气和石化产品的勘探和生产等行业](../Page/天然气.md "wikilink")。所以新普利茅斯市以新西兰的能源城市和乳品城市而著称。

## 风景名胜

塔拉纳基大区内的新普利茅斯市有大批卓越的[植物园](../Page/植物园.md "wikilink")（例如市中心的普克庫拉公园），在市中心海边45米高的有争议性的[风杖](../Page/风杖.md "wikilink")（wind
wand）还有独特的[塔纳纳奇山和](../Page/塔纳纳奇山.md "wikilink")[艾格蒙山的风景](../Page/艾格蒙山.md "wikilink")。新普利茅斯市同时也享有特别的海港城市的声誉，因为开车只要30分钟，这座城市的居民和游客都可以在一天之内在同一座城市里[滑雪](../Page/滑雪.md "wikilink")，[滑水和](../Page/滑水.md "wikilink")[冲浪](../Page/冲浪.md "wikilink")。

## 参考文献

<references/>

## 外部連結

[Category:紐西蘭大區](../Category/紐西蘭大區.md "wikilink")

1.  [新西兰概况](http://news.xinhuanet.com/ziliao/2002-06/23/content_453258.htm)，新华网
2.  [國家人口估計表](http://www.stats.govt.nz/methods_and_services/access-data/tables/subnational-pop-estimates.aspx)