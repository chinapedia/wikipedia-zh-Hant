<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**塔爾-蘇瑞安**（**Tar-Súrion**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻小說《[未完成的故事](../Page/未完成的故事.md "wikilink")》中的虛構人物。

他是[努曼諾爾帝國](../Page/努曼諾爾帝國.md "wikilink")（Númenor）第九任皇帝。

## 名字

**蘇瑞安**（Súrion）的名字來自[昆雅語](../Page/昆雅語.md "wikilink")，意思是「風之子」*Wind-son*。「塔爾」（Tar）是[努曼諾爾皇帝加在自己封號前的前綴](../Page/努曼諾爾皇帝.md "wikilink")，有着「高的、高貴的」的意思。

他的[阿督納克語](../Page/阿督納克語.md "wikilink")（Adûnaic）本名並沒有在任何作品中出現過。

## 總覽

塔爾-蘇瑞安是[努曼諾爾人](../Page/努曼諾爾人.md "wikilink")（Númenórean），是皇帝[塔爾-安那瑞安](../Page/塔爾-安那瑞安.md "wikilink")（Tar-Anárion）的第三名子女和繼承人。\[1\]他有兩位姐姐，全部名字不詳。蘇瑞安有兩名子女，[泰爾匹瑞安](../Page/塔爾-泰爾匹瑞安.md "wikilink")（Telperiën）和[伊西莫](../Page/伊西莫.md "wikilink")（Isilmo）。\[2\]

蘇瑞安是第九任努曼諾爾皇帝。本來他不會繼承皇位，但因為他的兩名姐姐厭惡祖母[塔爾-安卡林](../Page/塔爾-安卡林.md "wikilink")（Tar-Ancalimë）的關係，所以拒絕繼位，讓蘇瑞安成為了皇儲。\[3\]在他治下努曼諾爾繼續享受長期昇平穩定。他從父親繼承了寶劍[阿蘭路斯](../Page/阿蘭路斯.md "wikilink")（Aranrúth），這把劍只傳承於歷代皇帝之間，是努曼諾爾皇帝的權力象徵。\[4\]

他生於第二紀元1174年，在1574年去世，享年400歲。\[5\]

## 生平

蘇瑞安生於[第二紀元](../Page/第二紀元.md "wikilink")1174年的努曼諾爾島上。\[6\]1394年，他的父親塔爾-安那瑞安退位，\[7\]蘇瑞安繼位為帝。他後期統治時間裡，[伊瑞詹](../Page/伊瑞詹.md "wikilink")（Eregion）的精靈[凱勒布理鵬](../Page/凱勒布理鵬.md "wikilink")（Celebrimbor）鑄造了十九枚[力量之戒](../Page/力量之戒.md "wikilink")（Rings
of Power）。

第二紀元1556年，\[8\]塔爾-蘇瑞安自願退位給他的長女泰爾匹瑞安，結束了162年的統治，八年後逝世。

## 參考

  - 《[未完成的故事](../Page/未完成的故事.md "wikilink")》

## 資料來源

[category:中土大陸的角色](../Page/category:中土大陸的角色.md "wikilink")
[category:中土大陸的登丹人](../Page/category:中土大陸的登丹人.md "wikilink")

[en:Kings of
Númenor\#Tar-Súrion](../Page/en:Kings_of_Númenor#Tar-Súrion.md "wikilink")
[no:Númenoreanske
herskere\#Tar-Súrion](../Page/no:Númenoreanske_herskere#Tar-Súrion.md "wikilink")
[pl:Królowie
Númenoru\#Tar-Súrion](../Page/pl:Królowie_Númenoru#Tar-Súrion.md "wikilink")

1.
2.
3.  《[未完成的故事](../Page/未完成的故事.md "wikilink")》 "Aldarion and Erendis"
    P.273

4.
5.
6.
7.
8.