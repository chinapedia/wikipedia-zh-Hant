[Tongfukbeach.png](https://zh.wikipedia.org/wiki/File:Tongfukbeach.png "fig:Tongfukbeach.png")

[Tongfukbeach_1.JPG](https://zh.wikipedia.org/wiki/File:Tongfukbeach_1.JPG "fig:Tongfukbeach_1.JPG")

**塘福泳灘**，是[香港](../Page/香港.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[塘福的一處](../Page/塘福.md "wikilink")[泳灘](../Page/泳灘.md "wikilink")，為其中一處[假日](../Page/假日.md "wikilink")[渡假地點](../Page/渡假.md "wikilink")。

塘福泳灘在[塘福南部](../Page/塘福.md "wikilink")，其[景觀常常吸引](../Page/景觀.md "wikilink")[遊客在此](../Page/遊客.md "wikilink")[游泳](../Page/游泳.md "wikilink")，由塘福泳灘觀望，可遠眺[茶果洲和](../Page/茶果洲.md "wikilink")[石鼓洲兩處](../Page/石鼓洲.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")。

[沙灘上之](../Page/沙灘.md "wikilink")[沙粒黜黑](../Page/沙粒.md "wikilink")，其幼如粉，為香港少有。雖然塘福泳灘[海床較淺](../Page/海床.md "wikilink")，但風[浪大而急速](../Page/浪.md "wikilink")，不適合游泳初學者集泳，不過亦吸引[滑浪者練習](../Page/滑浪.md "wikilink")。

塘福泳灘有[更衣室](../Page/更衣室.md "wikilink")、[洗手間](../Page/洗手間.md "wikilink")、沖身[花灑等設備](../Page/花灑.md "wikilink")。

## 交通

塘福泳灘交通並不發達，只有一條[嶼南道途經塘福](../Page/嶼南道.md "wikilink")，[新大嶼山巴士公司有幾條巴士路線來往塘福](../Page/新大嶼山巴士.md "wikilink"):

  - 1 [梅窩至](../Page/梅窩.md "wikilink")[大澳](../Page/大澳.md "wikilink")
    （20至40分鐘一班）
  - 2：梅窩往返[昂坪](../Page/昂坪.md "wikilink")
  - 4 梅窩至塘福 （20至40分鐘一班）
  - 11 [東涌市中心至大澳](../Page/東涌.md "wikilink") （15至30分鐘一班）
  - N1 梅窩至大澳 （衹開一班，2:50am - 3:45am）
  - 23
    東涌市中心至[昂坪](../Page/昂坪.md "wikilink")[寶蓮寺](../Page/寶蓮寺.md "wikilink")
  - A35/N35 機場至梅窩（經東涌市中心）

## 圖集

<File:Tongfukbeach> 2.JPG|塘福泳灘上休憩的狗。 <File:Tongfukbeach>
4.JPG|塘福泳灘沙粒黜黑，其幼如粉。 <File:Tongfukbeach>
5.JPG|塘福泳灘風浪大而急速。 <File:Tongfukbeach> 6.JPG|大風浪。
<File:Tongfukbeach> 7.JPG|塘福泳灘上的泳客。 <File:Tongfukbeach> 8.JPG|大風浪。
<File:Tongfukbeach> 9.JPG|大風浪。 <File:Tongfukbeach> 10.JPG|塘福泳灘上的泳客。
<File:Cowcow> 1.JPG|流浪牛出沒。

## 參考資料

<references />

  - 《2004年香港街道地方指南》 通用圖書有限公司 [ISBN
    962-8797-32-8](../Page/ISBN.md "wikilink")

[Category:塘福](../Category/塘福.md "wikilink")