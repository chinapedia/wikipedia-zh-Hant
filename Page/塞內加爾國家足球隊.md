**塞內加爾國家足球隊**創立於1960年，並於1962年成為[國際足協](../Page/國際足聯.md "wikilink")（[FIFA](../Page/FIFA.md "wikilink")）的成員之一。

塞内加尔的球衣贊助商是[Puma](../Page/Puma.md "wikilink")。

## 歷史

塞內加爾是2002年[馬里](../Page/馬里.md "wikilink")[非洲國家盃亞軍](../Page/非洲國家盃.md "wikilink")。在[世界杯方面](../Page/世界杯.md "wikilink")，塞內加爾曾晉身到[2002年世界杯](../Page/2002年世界杯.md "wikilink")，且晉身八強，成為繼[喀麥隆於](../Page/喀麥隆國家足球隊.md "wikilink")[1990年第二支能晉身八強的非洲](../Page/1990年世界杯.md "wikilink")[國家](../Page/國家.md "wikilink")[足球隊](../Page/足球.md "wikilink")。塞內加爾在2002年世界杯決賽周前與[法國](../Page/法國國家足球隊.md "wikilink")、[丹麥和](../Page/丹麥國家足球隊.md "wikilink")[烏拉圭同組](../Page/烏拉圭國家足球隊.md "wikilink")，與2支前世界盃冠軍隊為伍，因此被視為其中一支極有可能在分組賽便出局的球隊。但事實其非如此，塞內加爾在該屆世界杯的揭幕戰面對[1998年世界杯的冠軍](../Page/1998年世界杯.md "wikilink")[法國](../Page/法國國家足球隊.md "wikilink")，並以1:0擊敗法國，這亦間接使法國在分組賽提前出局，塞內加爾頓時使人眼前一亮。

分組賽的第二場，塞內加爾以1:1迫和了[丹麥](../Page/丹麥國家足球隊.md "wikilink")，最後一場分組賽以3:3再迫和[烏拉圭](../Page/烏拉圭國家足球隊.md "wikilink")，令烏拉圭出局，塞內加爾以小組第2名晉級至十六強。十六強階段遇上[北歐強隊之一的](../Page/北歐.md "wikilink")[瑞典](../Page/瑞典國家足球隊.md "wikilink")，在發定時間內以1:1言和，[H.卡馬拉射入了](../Page/H.卡馬拉.md "wikilink")[黃金入球](../Page/黃金入球.md "wikilink")，把瑞典淘汰出局。到了八強遇上僅第二次晉身[世界杯的](../Page/世界杯.md "wikilink")[土耳其](../Page/土耳其國家足球隊.md "wikilink")，最終在黃金入球被土耳其的[伊利安入球](../Page/伊利安.md "wikilink")。

塞內加爾於[2006年世界杯外圍賽以](../Page/2006年世界杯外圍賽.md "wikilink")2分之微未能再次出席[世界杯的決賽周](../Page/世界杯.md "wikilink")。

## 著名球員

[-{zh-hans:维埃拉;zh-hk:韋拉;zh-tw:維埃拉;}-出生在](../Page/帕特裡克·維埃拉.md "wikilink")[塞內加爾](../Page/塞內加爾.md "wikilink")，但入籍[法國](../Page/法國.md "wikilink")，並是[法國國家足球隊陣中的球員之一](../Page/法國國家足球隊.md "wikilink")。而隊友[-{zh-hans:西塞;zh-hk:施斯;zh-tw:西塞;}-](../Page/贾布里勒·西塞.md "wikilink")
亦是塞內加爾裔，但卻在法國出生。

## 世界杯成績

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/世界盃足球賽.md" title="wikilink">世界盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2022年世界盃足球賽.md" title="wikilink">2022年</a></p></td>
</tr>
<tr class="even">
<td><p><strong>總結</strong></p></td>
</tr>
</tbody>
</table>

## 非洲國家杯成績

  - 1957年 *沒有參與*
  - 1959年 *沒有參與*
  - 1962年 *沒有參與*
  - 1963年 *沒有參與*
  - 1965年 **殿軍**
  - 1968年 *分組賽*
  - 1970年 *外圈賽*
  - 1972年 *外圈賽*
  - 1974年 *外圈賽*
  - 1976年 *外圈賽*
  - 1978年 *外圈賽*
  - 1980年 *沒有參與*
  - 1982年 *外圈賽*
  - 1984年 *外圈賽*
  - 1986年 *分組賽*
  - 1988年 *外圈賽*
  - 1990年 **殿軍**
  - 1992年 *四強出局*
  - 1994年 *四強出局*
  - 1996年 *外圈賽*
  - 1998年 *外圈賽*
  - 2000年 *四強出局*
  - 2002年 **亞軍**
  - [2004年](../Page/2004年非洲国家杯足球赛.md "wikilink") *四強出局*
  - [2006年](../Page/2006年非洲国家杯足球赛.md "wikilink") *第四名*
  - [2008年](../Page/2008年非洲国家杯足球赛.md "wikilink") *分組賽*
  - [2010年](../Page/2010年非洲国家杯足球赛.md "wikilink") *外圈賽*
  - [2012年](../Page/2012年非洲国家杯足球赛.md "wikilink") *分組賽*

## 近賽成績

### 2012年非洲國家盃

  - 分組賽A組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>+2</p></td>
<td><p><strong>7</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>+1</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p><strong>4</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td><p>−3</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

-----

<small>*原訂20:00開賽，因大雨而延遲。*</small>

-----

### 2014年世界盃

  - [外圍賽第二輪J組](../Page/2014年世界盃外圍賽非洲區第二輪#J組.md "wikilink")

## 著名球員

  -
  - [S.卡馬拉](../Page/S.卡馬拉.md "wikilink")

  - [法迪加](../Page/法迪加.md "wikilink")

  - [迪奧夫](../Page/迪奧夫.md "wikilink")

  - [戴奧](../Page/戴奧.md "wikilink")

  - [A.施斯](../Page/A.施斯.md "wikilink")

  - [阿利烏·西塞](../Page/阿利烏·西塞.md "wikilink")

## 外部連結

  - [RSSSF archive of
    results 1961–](http://www.rsssf.com/tabless/sene-intres.html)
  - [Website of the Senegal Football Team](http://www.allezleslions.com)

[category:塞內加爾國家足球隊](../Page/category:塞內加爾國家足球隊.md "wikilink")

[Category:非洲足球代表隊](../Category/非洲足球代表隊.md "wikilink")