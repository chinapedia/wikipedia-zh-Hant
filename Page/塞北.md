**塞北**，又称**塞外**，是過去[中國的一個地理區位名詞](../Page/中國.md "wikilink")，現已罕用。
[ROC_Geo_Regions_Text.png](https://zh.wikipedia.org/wiki/File:ROC_Geo_Regions_Text.png "fig:ROC_Geo_Regions_Text.png")

## 由來

**塞**是指[邊塞](../Page/邊疆.md "wikilink")、[要塞](../Page/要塞.md "wikilink")，這裡所謂的**塞**相當於今日的[明長城](../Page/明長城.md "wikilink")。而以此為界，以北的部分已經出邊塞，故名塞北。偶爾會與塞外一同使用。

## 範圍

[清代時的](../Page/清朝.md "wikilink")**塞北**大約是[漠南蒙古](../Page/漠南蒙古.md "wikilink")、[漠北蒙古](../Page/漠北蒙古.md "wikilink")、[科布多](../Page/科布多.md "wikilink")、[唐努烏梁海與](../Page/唐努烏梁海.md "wikilink")[阿拉善盟等以蒙古人居住為主的地理區域](../Page/阿拉善盟.md "wikilink")。[中華民國成立以後至今](../Page/中華民國.md "wikilink")，用來做為地理劃分的區域，包括[熱河省](../Page/熱河省_\(中華民國\).md "wikilink")、[察哈爾省](../Page/察哈爾省_\(中華民國\).md "wikilink")、[綏遠省](../Page/綏遠省_\(中華民國\).md "wikilink")、[寧夏省](../Page/寧夏省_\(中華民國\).md "wikilink")、[蒙古地方五個省區](../Page/蒙古地方.md "wikilink")。[中華人民共和國政府承認](../Page/中華人民共和國.md "wikilink")[蒙古人民共和國](../Page/蒙古人民共和國.md "wikilink")（[蒙古國](../Page/蒙古國.md "wikilink")）政權期間，塞北用以代指其他的四個省份。

## 逐漸罕用

**塞北**一詞，在作為劃分地理區域時並不像[東北](../Page/中国东北地区.md "wikilink")、[西南之類單純的方位概念](../Page/中国西南.md "wikilink")。塞有[邊界的含意](../Page/邊界.md "wikilink")，是指历史上汉族农耕文明与北方游牧部落的分野。

[中华人民共和国成立後](../Page/中华人民共和国.md "wikilink")，由於外蒙古的獨立，而另外四省在行政區重置後全數撤銷（省名僅留下“[寧夏](../Page/寧夏.md "wikilink")”，但省區大量縮小），部分區域劃入[河北省及](../Page/河北省.md "wikilink")[寧夏回族自治區外](../Page/寧夏回族自治區.md "wikilink")，皆成為[內蒙古自治區](../Page/內蒙古自治區.md "wikilink")。由於只剩一個行政區，加上內蒙古本身也不僅包含**塞北**還有东北的西部部分，再加上上述原因，在如今塞北一词在地理区劃上已很少使用。
[中華民國政府遷台後](../Page/中華民國政府.md "wikilink")，**塞北**的區劃方式與概念仍在地理教科書上繼續使用，直至1990年代[教改開始及教科書開放後](../Page/教改.md "wikilink")，為順應現實劃分，現在也已經棄用，但地理位置劃分和大陸不同。中国大陆將[內蒙古劃入](../Page/內蒙古自治區.md "wikilink")[華北地區](../Page/華北地區.md "wikilink")，而[蒙古地方](../Page/蒙古地方.md "wikilink")（[外蒙古](../Page/外蒙古.md "wikilink")）則移到[世界地理索引作為獨立國家標示](../Page/世界地理索引.md "wikilink")。

## 参考文献

## 參見

  - [中国地理](../Page/中国地理.md "wikilink")：[漠北](../Page/漠北.md "wikilink")、[塞外](../Page/塞外.md "wikilink")
  - [蒙古](../Page/蒙古.md "wikilink")：[內蒙古](../Page/內蒙古.md "wikilink")、[外蒙古](../Page/外蒙古.md "wikilink")
  - [中華民國行政區劃](../Page/中華民國行政區劃.md "wikilink")

{{-}}

[塞北四省](../Category/塞北四省.md "wikilink")