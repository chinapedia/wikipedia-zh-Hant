**塞尔维亚帝国**（[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")：****）位于[巴尔干半岛](../Page/巴尔干半岛.md "wikilink")，由[塞尔维亚人于](../Page/塞尔维亚人.md "wikilink")1346年建立，1371年消亡，是当时[欧洲最大的国家之一](../Page/欧洲.md "wikilink")。

## 君主

1.  [斯特凡·乌罗什四世](../Page/斯特凡·乌罗什四世.md "wikilink")
2.  [斯特凡·乌罗什五世](../Page/斯特凡·乌罗什五世.md "wikilink")

## 注释

## 参考文献

## 参见

  - [塞尔维亚历史](../Page/塞尔维亚历史.md "wikilink")

[Category:塞尔维亚历史](../Category/塞尔维亚历史.md "wikilink")
[Category:中世紀各國](../Category/中世紀各國.md "wikilink")
[Category:已不存在的帝國](../Category/已不存在的帝國.md "wikilink")
[Category:14世纪建立的国家或政权](../Category/14世纪建立的国家或政权.md "wikilink")
[Category:14世紀終結的國家或政權](../Category/14世紀終結的國家或政權.md "wikilink")