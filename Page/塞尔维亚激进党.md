**塞爾維亞激進黨**（[塞爾維亞語](../Page/塞爾維亞語.md "wikilink")：****或****，**SRS**）是[塞爾維亞](../Page/塞爾維亞.md "wikilink")[右翼](../Page/右翼.md "wikilink")[民族主義](../Page/民族主義.md "wikilink")[政黨](../Page/政黨.md "wikilink")。

1991年成立，由「人民激進黨」（創立於1990年，並非[尼古拉·帕斯契](../Page/尼古拉·帕斯契.md "wikilink")[Nikola
Pašić的](../Page/:en:Nikola_Pašić.md "wikilink")[人民激進黨](../Page/人民激進黨.md "wikilink")[People's
Radical
Party](../Page/:en:People's_Radical_Party.md "wikilink")）與[塞爾維亞切特尼克運動](../Page/塞爾維亞切特尼克運動.md "wikilink")（Serbian
Chetnik
Movement）所組成。原持有較激端的民族主義傾向，反對[歐盟及](../Page/歐盟.md "wikilink")[北約介入塞爾維亞的內政](../Page/北約.md "wikilink")。2008年激進黨出現嚴重分裂，黨主席[尼古拉·索科奇以及大部分激進黨議員退黨](../Page/尼古拉·索科奇.md "wikilink")，加入新成立的親歐盟及民族主義的[塞爾維亞進步黨](../Page/塞爾維亞進步黨.md "wikilink")。其後的國會選舉，激進黨失去所有席次。

在[2016年塞爾維亞國會選舉獲得](../Page/2016年塞爾維亞國會選舉.md "wikilink")8.10%。為第三大黨，重返國會。

## 進階閱讀

  -
## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

[Category:塞爾維亞政黨](../Category/塞爾維亞政黨.md "wikilink")
[Category:民族主義政黨](../Category/民族主義政黨.md "wikilink")
[Category:歐洲懷疑主義政黨](../Category/歐洲懷疑主義政黨.md "wikilink")
[Category:右翼民粹主義](../Category/右翼民粹主義.md "wikilink")
[Category:1991年建立的政黨](../Category/1991年建立的政黨.md "wikilink")
[Category:1991年塞爾維亞建立](../Category/1991年塞爾維亞建立.md "wikilink")