**塞巴斯蒂奥·拉扎罗尼**（**Sebastião
Lazaroni**，），是一名[巴西足球主教练](../Page/巴西.md "wikilink")。

## 执教生涯

拉扎罗尼最高荣誉是曾经率领[巴西国家足球队参加](../Page/巴西国家足球队.md "wikilink")[1990年世界杯足球赛](../Page/1990年世界杯足球赛.md "wikilink")，其麾下有[卡雷卡](../Page/卡雷卡.md "wikilink")、[邓加](../Page/邓加.md "wikilink")、[罗马里奥](../Page/罗马里奥.md "wikilink")、[贝贝托](../Page/贝贝托.md "wikilink")、[尤尔金霍](../Page/佐真奴.md "wikilink")、[塔法雷尔等一大批世界著名球星](../Page/克劳迪奥·塔法雷尔.md "wikilink")，在小组赛中三战三捷，不过小组出线后却立即遭遇[马拉多纳率领的](../Page/马拉多纳.md "wikilink")[阿根廷而被淘汰](../Page/阿根廷国家足球队.md "wikilink")。

1999年，拉扎罗尼成为[中国足球甲A联赛](../Page/甲A联赛.md "wikilink")[上海申花的主帅](../Page/上海申花.md "wikilink")，这也是中国足球历史上第一位曾经率领世界顶级球队征战世界杯赛事的主教练。但是拉扎罗尼却成为申花历史上众多外籍主帅中口碑最差的一个，除了后来将自己踢业余足球的儿子加入申花队训练的惊人之举外，还被体育记者认为不尊重中国足球，被球员认为是“伪劣产品”\[1\]，甚至因为辱骂申花领队戴春华几乎导致当时的助理教练、后来率领申花夺得联赛冠军的[吴金贵出手打人](../Page/吴金贵.md "wikilink")\[2\]。

## 注释

<small>

<references />

[Category:巴西足球主教練](../Category/巴西足球主教練.md "wikilink")
[Category:上海申花教练](../Category/上海申花教练.md "wikilink")
[Category:橫濱水手主教練](../Category/橫濱水手主教練.md "wikilink")
[Category:法林明高主教練](../Category/法林明高主教練.md "wikilink")
[Category:華斯高主教練](../Category/華斯高主教練.md "wikilink")
[Category:巴里主教練](../Category/巴里主教練.md "wikilink")
[Category:義大利裔巴西人](../Category/義大利裔巴西人.md "wikilink")

1.  谢晖、王娜力，《谢晖：这就是生活》，[拉扎罗尼](http://book.sina.com.cn/nzt/sal/xiehui/26.shtml)。
2.