**塞提捕蟲堇**（*Pinguicula*
'Sethos'）（[親本](../Page/親本.md "wikilink")：*Pinguicula
ehlersiae* x *Pinguicula
moranensis*）墨西哥[捕蟲堇的](../Page/捕蟲堇.md "wikilink")[園藝品系之一](../Page/栽培種.md "wikilink")，是由[愛勒氏捕蟲堇與](../Page/愛勒氏捕蟲堇.md "wikilink")[墨蘭捕蟲堇的](../Page/墨蘭捕蟲堇.md "wikilink")[雜交種再進一步培育而得](../Page/雜交種.md "wikilink")。描述指出本種具有大型的花朵且中心為淺白色的星狀。通常花朵略帶紫色\[1\]\[2\]，冬季休眠時會呈現縮小的肉質化葉片。由於栽培上較為容易，而且顏色和外觀型態十分多變\[3\]，因此也是目前許多愛好者廣泛種植的[食肉植物](../Page/食肉植物.md "wikilink")。

## 參照

<div class="references-small">

</div>

## 外部連結

  - [如何種捕蟲菫](http://www.exoticaplants.com/backissue/9/database/cps/knowledge/pinguicula.htm)

  - [P.
    'Sethos'──塔內植物園食蟲植物版](http://x4.net.vnu.edu.tw/~tbgweb/cgi-bin/topic.cgi?forum=19&topic=63&show=7500)
    生長期型態

  - [Pinguicula potosiensis and P. 'Sethos'
    photos](https://web.archive.org/web/20080228234547/http://icps.proboards105.com/index.cgi?board=pinguicula)
    關於塞提捕蟲堇和其它捕蟲堇的分辨討論

[Category:捕蟲堇屬](../Category/捕蟲堇屬.md "wikilink")

1.
2.
3.