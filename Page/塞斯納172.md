**塞斯納172型天鷹**（）是一個單引擎四座位的小型飛機，是受歡迎的[教練機](../Page/教練機.md "wikilink")。1956年至今生產已超過42,500架\[1\]。

## 發展

[CESSNA_172SP_SKYHAWK_D-EXAD_vr.jpg](https://zh.wikipedia.org/wiki/File:CESSNA_172SP_SKYHAWK_D-EXAD_vr.jpg "fig:CESSNA_172SP_SKYHAWK_D-EXAD_vr.jpg")
[G-BGMP_Reims_F172_@Cotswold_Airport,_July_2005.jpg](https://zh.wikipedia.org/wiki/File:G-BGMP_Reims_F172_@Cotswold_Airport,_July_2005.jpg "fig:G-BGMP_Reims_F172_@Cotswold_Airport,_July_2005.jpg")
[GLFC9.jpg](https://zh.wikipedia.org/wiki/File:GLFC9.jpg "fig:GLFC9.jpg")
[Cessna.172rg.jpg](https://zh.wikipedia.org/wiki/File:Cessna.172rg.jpg "fig:Cessna.172rg.jpg")
[Cessna.172e.g-asss.arp.jpg](https://zh.wikipedia.org/wiki/File:Cessna.172e.g-asss.arp.jpg "fig:Cessna.172e.g-asss.arp.jpg")

塞斯納C-172是歷史上最成功生產量最多的小型飛機。首架飛機於1956年交付，直到今天C-172仍在投產中，其主要競爭對手是Beechcraft的Musketeer
，Piper的Cherokee及Grumman公司的AA-5系列。

### 初期

早期的C-172參照[塞斯納C-170及標準的三輪傳動裝置飛機](../Page/塞斯納C-170.md "wikilink")，單是在1956年已出售超過1400架。早期的172型與170型十分相似，具有相同機身，最大起飛重量為2200磅。後來重新設計成前三點[起落架](../Page/起落架.md "wikilink")，增加後窗以改良飛行員的視野，成為一架能夠360度觀察四周環境的飛機。

### 中期

C-172A引入了可移動的尾翼及方向舵及向後傾的機尾，而172B則改變了儀器設備，從此型號開始得到空中之鷹的稱號。172D降低了機身的高度，172F加入了電力襟翼。在1968年，塞斯納公司開始停產初期的172型，並設計了172J型，而172J型將會成為全新的[塞斯納C-177](../Page/塞斯納C-177.md "wikilink")。後來由於塞斯納取消了與引擎公司的合約，塞斯納C-172J需要重新更換引擎。引擎由原來的145[馬力更換為](../Page/馬力.md "wikilink")150馬力。

### 後期

C-172R於1996年投產，推力為160馬力，最大起飛重量提升至2450磅。172R型是現代化的小型飛機，加入了標準的無線電設備及隔音設備。C-172SP型是172系列最現代化的機型，於1998年投產，推力為180馬力，比R型高出20馬力，最大起飛重量提升至2550磅，此型號能加裝G1000玻璃駕駛艙。

## 軍用

**T-41**是C-172的衍生型號，廣泛地被[美國空軍及](../Page/美國空軍.md "wikilink")[美國陸軍用作教練機](../Page/美國陸軍.md "wikilink")，於1997年停產。
T-41的使用國包括：

  - [哥倫比亞](../Page/哥倫比亞.md "wikilink")
  - [菲律賓](../Page/菲律賓.md "wikilink")
  - [泰國](../Page/泰國.md "wikilink")
  - [美國](../Page/美國.md "wikilink")
  - [土耳其](../Page/土耳其.md "wikilink")
  - [希臘](../Page/希臘.md "wikilink")
  - [烏拉圭](../Page/烏拉圭.md "wikilink")

## 著名C-172飛行

  - 1987年，一名年輕[德國飛行員](../Page/德國.md "wikilink")[馬蒂亞斯·魯斯特租了一架C](../Page/馬蒂亞斯·魯斯特.md "wikilink")-172，在沒有許可的情況下由[赫爾辛基飛至](../Page/赫爾辛基.md "wikilink")[蘇聯上空並降落於](../Page/蘇聯.md "wikilink")[紅場附近](../Page/紅場.md "wikilink")，途中沒有蘇聯空軍阻止，這便是著名的[紅場事件](../Page/紅場事件.md "wikilink")。

<!-- end list -->

  - 2002年1月5日，美國高中學生[查理·J·比索偷取了一架C](../Page/查理·J·比索.md "wikilink")-172飛機並墜毀於[佛羅里達州](../Page/佛羅里達州.md "wikilink")[坦帕的](../Page/坦帕.md "wikilink")[美國高塔銀行附近](../Page/美國高塔銀行.md "wikilink")，而他本人也在此事件中死亡。
  - 日本电影《[追捕](../Page/追捕.md "wikilink")》中，杜丘冬人（高倉健飾）驾驶C-172R从北海道飞回本州。

## G1000

G1000玻璃駕駛艙\[2\]於2005年安裝在172R及172SP上。G1000取替了傳統的儀表，改由兩個12吋液晶螢幕顯示器。這使了飛行員在操作上更加方便，而兩個液晶螢幕顯示器是可以互相轉換的。在正常設計中，左方液晶螢幕顯示器是用來顯示高度、速度以及姿態的主要飛行儀器（PFD，primary
flight display）及。而右方的則用作多功能顯示（MFD，multi-function display）。

## 性能（172R）

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [塞斯納製造商首頁](http://www.cessna.com/)

[Category:塞斯納](../Category/塞斯納.md "wikilink")
[Category:小型飛機](../Category/小型飛機.md "wikilink")
[Category:美國航空器](../Category/美國航空器.md "wikilink")
[Category:教練機](../Category/教練機.md "wikilink")

1.  [<span style="font-size:smaller;">（英文）</span>The Cessna 172
    Skyhawk](http://www.airliners.net/info/stats.main?id=141)
2.  [<span style="font-size:smaller;">（英文）</span>G1000玻璃駕駛艙](http://skyhawk.cessna.com/avionics.chtml)