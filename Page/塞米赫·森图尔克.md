**塞米赫·森图尔克**（，），出生于土耳其[伊兹密尔](../Page/伊兹密尔.md "wikilink")，[土耳其足球运动员](../Page/土耳其.md "wikilink")，现效力於[土耳其足球超级联赛的俱樂部](../Page/土耳其足球超级联赛.md "wikilink")[费内巴切](../Page/費倫巴治體育會.md "wikilink")。

## 外部链接

  - [Profile at
    fenerbahce.org](http://www.fenerbahce.org/futbol/detay.asp?ContentID=792&k=e)
  - [Profile at
    TFF.org](http://www.tff.org.tr/Default.aspx?pageId=30&kisiID=267117)
  - [Profile at
    transfermarkt.de](https://web.archive.org/web/20080419122732/http://www.transfermarkt.de/de/spieler/7043/semihsentuerk/profil.html)

[Category:土耳其足球运动员](../Category/土耳其足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:費倫巴治球員](../Category/費倫巴治球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")