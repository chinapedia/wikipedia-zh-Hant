**塞缪尔·刘易斯·索瑟德**（**Samuel Lewis
Southard**，），[美国政治家](../Page/美国.md "wikilink")，[美国民主-共和党成员](../Page/美国民主-共和党.md "wikilink")，曾任[美国参议员](../Page/美国参议员.md "wikilink")（1821年-1823年）、[美国海军部长](../Page/美国海军部长.md "wikilink")（1823年-1829年）和[新泽西州](../Page/新泽西州.md "wikilink")[州长](../Page/州长.md "wikilink")（1832年-1833年）。

[S](../Category/美国辉格党州长.md "wikilink")
[S](../Category/普林斯頓大學校友.md "wikilink")