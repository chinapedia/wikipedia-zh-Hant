[Seretse_Khama_photo.jpg](https://zh.wikipedia.org/wiki/File:Seretse_Khama_photo.jpg "fig:Seretse_Khama_photo.jpg")
**塞雷茨·卡马**爵士，[KBE](../Page/KBE.md "wikilink")（Sir **Seretse
Khama**，1921年7月1日[塞罗韦](../Page/塞罗韦.md "wikilink") -
1980年7月13日[哈博罗内](../Page/哈博罗内.md "wikilink")），[博茨瓦纳政治家](../Page/博茨瓦纳.md "wikilink")，[博茨瓦纳民主党成员](../Page/博茨瓦纳民主党.md "wikilink")，曾任[博茨瓦纳首任总统](../Page/博茨瓦纳总统.md "wikilink")（1966年-1980年）。

塞雷茨是波扎那共和國前身-
[貝專納保護國的王位繼承人](../Page/貝專納保護國.md "wikilink")，前往[英國求學期間透過一次舞會](../Page/英國.md "wikilink")，結識了在勞埃德保險社（Lloyds
of London）任職的露絲（Ruth Williams
Khama），快速地陷入熱戀並於一年後結婚。哪知道本應被祝福的婚姻，卻引來英國政府、殖民部（Colonial
Office）、貝專納國內輿論以及實行「[種族隔離政策](../Page/種族隔離.md "wikilink")」鄰居[南非的阻擾](../Page/南非.md "wikilink")。

戰後[英國工黨政府有鑑於欠下大筆債務](../Page/英國工黨.md "wikilink")，必須仰賴南非這塊金雞母的挹注，最後在迫於[南非威脅將以武力解決](../Page/南非.md "wikilink")「問題」的壓力之下，於1951年宣布中止塞雷茨的繼承權，並將當時已返鄉定居的他與妻子放逐海外。原先英國政府為安撫塞雷茨夫婦，安排兩人前往[中美洲](../Page/中美洲.md "wikilink")[加勒比海地區的](../Page/加勒比海地區.md "wikilink")[牙買加任職與定居](../Page/牙買加.md "wikilink")，但遭到拒絕；最後兩人選擇定居於英格蘭東南部的[薩里郡](../Page/薩里郡.md "wikilink")，以便照顧2名年幼的孩子。

被流放的塞雷茨並未因此放棄希望，在結束五年的流放生涯後，他選擇創立[波札那民主黨](../Page/波札那民主黨.md "wikilink")，1965年議會大選中贏得多數，被推選為總理，持續致力於獨立運動。終於在一年後，家鄉貝專納保護國於1966年正式宣布，脫離英國獨立為如今的波扎那共和國後，在首次總統大選中被選為共和國首任總統。他一直擔任總統至1980年7月因癌症病逝為止。28年後他兒子[伊恩·卡馬亦被推為第四屆總統](../Page/伊恩·卡馬.md "wikilink")。妻子露絲則於2002年病逝於波扎那。


[K](../Category/1921年出生.md "wikilink")
[K](../Category/1980年逝世.md "wikilink")
[K](../Category/博茨瓦纳总统.md "wikilink")
[K](../Category/KBE勳銜.md "wikilink")