[Long_Valley_Overview_201408.jpg](https://zh.wikipedia.org/wiki/File:Long_Valley_Overview_201408.jpg "fig:Long_Valley_Overview_201408.jpg")和[西洋菜的濕](../Page/西洋菜.md "wikilink")[農田](../Page/農田.md "wikilink")\]\]
[Long_Valley_Overview1_201408.jpg](https://zh.wikipedia.org/wiki/File:Long_Valley_Overview1_201408.jpg "fig:Long_Valley_Overview1_201408.jpg")
**塱原**（）是[香港](../Page/香港.md "wikilink")[新界北部的一片淡水](../Page/新界.md "wikilink")[濕地](../Page/濕地.md "wikilink")，位於[上水的](../Page/上水.md "wikilink")[燕崗](../Page/燕崗.md "wikilink")、[松柏塱及](../Page/松柏塱.md "wikilink")[河上鄉一帶](../Page/河上鄉.md "wikilink")，即[上水市中心的西北方](../Page/上水.md "wikilink")，佔地25[公頃](../Page/公頃.md "wikilink")，[石上河與](../Page/石上河.md "wikilink")[雙魚河分別於東西兩側包圍塱原並交匯](../Page/雙魚河.md "wikilink")，該片濕地是除了[米埔之外的](../Page/米埔.md "wikilink")[候鳥主要停留之地](../Page/候鳥.md "wikilink")。2000年代[九廣鐵路計劃興建](../Page/九廣鐵路.md "wikilink")[落馬洲支線經過該地](../Page/落馬洲支線.md "wikilink")，引起了對該處生態影響的關注。

## 特色

地理學上，塱原一帶屬於[洪泛平原](../Page/洪泛平原.md "wikilink")，現時仍設有種植[通菜和](../Page/通菜.md "wikilink")[西洋菜的濕](../Page/西洋菜.md "wikilink")[農田](../Page/農田.md "wikilink")，以及[紅蟲塘](../Page/紅蟲.md "wikilink")、[荷花池等淡水塘](../Page/荷花.md "wikilink")。這些地方形成各式各樣的小[生境](../Page/生境.md "wikilink")，吸引一些喜愛淡水濕地及開闊地方的[鳥類](../Page/鳥類.md "wikilink")。目前塱原共錄得234種[鳥類品種](../Page/鳥類.md "wikilink")，例如[彩鷸](../Page/彩鷸.md "wikilink")、[紫背葦鳽](../Page/紫背葦鳽.md "wikilink")、[紅隼](../Page/紅隼.md "wikilink")、[斑鶇和](../Page/斑鶇.md "wikilink")[小蝗鶯等](../Page/小蝗鶯.md "wikilink")。

## 發展

1994年12月，[香港政府發表](../Page/香港政府.md "wikilink")[鐵路發展策略](../Page/鐵路發展策略.md "wikilink")，建議興建「[西部走廊鐵路](../Page/西部走廊鐵路.md "wikilink")」，其後發展為由[九廣鐵路於](../Page/九廣鐵路.md "wikilink")1996年取得營運權的[九廣西鐵](../Page/西鐵綫.md "wikilink")。其中，九廣西鐵第2期，即現時的[落馬洲支線](../Page/落馬洲支線.md "wikilink")，會以像西鐵第1期穿過[八鄉等地的高架](../Page/八鄉.md "wikilink")[天橋形式穿越塱原](../Page/天橋.md "wikilink")。但這個計劃遭到各環保團體強烈反對，指興建高架橋時將會嚴重破壞塱原的生境及地貌。即使九鐵提議工程完工後會盡量回復原來面貌，但環保團體認為這無補於事。有關計劃亦於2000年被[環保署否決](../Page/香港環保署.md "wikilink")\[1\]。最終九鐵經過一段時間考慮、研究及評估後，決定會以地底[隧道形式穿過塱原](../Page/隧道.md "wikilink")。雖然這個方案會使引致整條支線的興建成本上升、回報率下降及完工日期押後等等，不過此方案獲大多數人所接受及支持。

## 參考資料

  - [塱原](https://web.archive.org/web/20070306104104/http://www.hkbws.org.hk/BirdingHongKong/inform/lvc.html)，香港觀鳥會

## 參見

  - [米埔](../Page/米埔.md "wikilink")

[Category:香港濕地](../Category/香港濕地.md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")
[Category:上水](../Category/上水.md "wikilink")

1.  <http://www.legco.gov.hk/yr01-02/chinese/panels/tp/papers/tpeaplw1127cb1-295-c.pdf>