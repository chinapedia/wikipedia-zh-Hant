[TU_Dresden_58.jpg](https://zh.wikipedia.org/wiki/File:TU_Dresden_58.jpg "fig:TU_Dresden_58.jpg")
[komgmiao.jpg](https://zh.wikipedia.org/wiki/File:komgmiao.jpg "fig:komgmiao.jpg")围墙\]\]
[HK_Peak_The_Center_West_Kln_COSCO_Tower.JPG](https://zh.wikipedia.org/wiki/File:HK_Peak_The_Center_West_Kln_COSCO_Tower.JPG "fig:HK_Peak_The_Center_West_Kln_COSCO_Tower.JPG")，應用[玻璃幕墙的大廈很多](../Page/玻璃幕墙.md "wikilink")。\]\]

**墙**（或称**壁**、**墙壁**）在[建筑学上是指一种重直向的](../Page/建筑学.md "wikilink")[空间隔断结构](../Page/空间.md "wikilink")，用来围合、分割或保护某一区域，是[建筑设计中最重要的元素之一](../Page/建筑设计.md "wikilink")。根据墙在建筑物中是否承重，分为[承重墙和非承重墙](../Page/承重墙.md "wikilink")。承重墙是建筑结构的一部分，承接其上及附近建築物的重量，不容許因裝修等理由移除。

墙身兼两重作用：一方面作为[建筑物的外维护结构需要提供足够优良的防水](../Page/建筑物.md "wikilink")、防风、[保温](../Page/保温.md "wikilink")、[隔热性能](../Page/隔热.md "wikilink")，为室内环境提供保护；另一方面墙又是[建筑师进行空间划分的主要手段](../Page/建筑师.md "wikilink")，来满足建筑[功能](../Page/功能.md "wikilink")、[空间的要求](../Page/空间.md "wikilink")。
有名的牆:萬里長城 柏林圍牆 九龍壁〈位於北京〉 哭牆

## 材料

几乎所有重要的建筑材料都可以成为建造墙的材料，例如[木材](../Page/木材.md "wikilink")、[石材](../Page/石材.md "wikilink")、[砖](../Page/砖.md "wikilink")、[混凝土](../Page/混凝土.md "wikilink")、[金属](../Page/金属.md "wikilink")、[冰](../Page/冰.md "wikilink")、[高分子材料或](../Page/高分子材料.md "wikilink")[玻璃](../Page/玻璃.md "wikilink")。

## 构造

### 砖墙

[砖的特点是块小](../Page/砖.md "wikilink")、[抗压强度远大于](../Page/抗压强度.md "wikilink")[抗拉强度](../Page/抗拉强度.md "wikilink")、[抗扭强度和](../Page/抗扭强度.md "wikilink")[抗剪强度](../Page/抗剪强度.md "wikilink")。中国标准[粘土砖的尺寸为](../Page/粘土.md "wikilink")240×115×53mm，这一尺寸的目的是为了保证砖的長宽高之比为4:2:1（包括10mm的灰缝宽度）。砖墙的厚度多以砖的倍数称呼，由于砖的长度为240mm，厚度为半砖的墙称为“一二墙”,厚度为半砖加一块站砖的称为“十八墙”，厚度为一块砖称为“二四墙”，厚度为一砖半的墙称为“三七墙”。
砖墙构造需要把小尺寸的砖头以一种合理的方式通过[砂浆组合成墙体](../Page/砂浆.md "wikilink")。
砖墙砌筑的主要标准是不能有上下“通缝”以保证砖墙的坚固，因此砖块的砌筑应遵循内外搭接上下错缝的原则。一层砖术语称为一“皮”，和墙体方向平行的砖称为“顺”，和墙体方向垂直的砖称为“丁”，要保证没有通缝就需要把砖交错砌筑，即有丁有顺，具体上主要有三种砌法：

  - [一顺一丁](../Page/一顺一丁.md "wikilink")——又称“满丁满条”，指一皮砖按照顺一皮砖按照丁的方式交替砌筑，这种砌法最为常见，对工人的技术要求也较低。
  - [二顺一丁](../Page/二顺一丁.md "wikilink")——指两皮砖按照顺一皮砖按照丁的方式砌筑，这种砌法比一顺一丁更容易，但是强度略低。
  - [梅花丁](../Page/梅花丁.md "wikilink")——指每一皮砖都有顺有丁，上下皮又顺丁交错，这种砌法难度最大，但是墙体强度最高。

为了保证在错缝搭接的要求，在墙的转角处、门窗洞口或端部的第一块砖需要采用3/4转，过去一般由工人用砌刀把整块砖敲掉1/4，成为三四找砖，现在则多用[电锯批量锯除](../Page/电锯.md "wikilink")。除了三四找砖还有一二找砖，即敲掉半块砖，用在半砖墙的端部。

#### 加強磚牆

磚牆左右均有钢筋混凝土柱、上下均有钢筋混凝土樑或基腳者。

### 剪力牆

也叫做抗震牆，結構牆，是指房屋或構築物中承受水平力（如[地震](../Page/地震.md "wikilink")）的牆體。剪力牆一般為[鋼筋混凝土造](../Page/鋼筋混凝土.md "wikilink")。

### 玻璃幕墙

随着[玻璃](../Page/玻璃.md "wikilink")[物料科學的發展](../Page/物料科學.md "wikilink")，出現[強化玻璃](../Page/強化玻璃.md "wikilink")，大塊的玻璃片代替[石墻成為可能](../Page/石.md "wikilink")，[玻璃幕墙開始出現](../Page/玻璃幕墙.md "wikilink")。
在1910年，[建筑](../Page/建筑.md "wikilink")[设计师](../Page/设计师.md "wikilink")[格罗皮乌斯](../Page/格罗皮乌斯.md "wikilink")
在[包豪斯的校舍建築中](../Page/包豪斯.md "wikilink")，首次采用玻璃幕墙。\[1\]

## 相關條目

  - [壁紙](../Page/壁紙.md "wikilink")
  - [迷牆](../Page/迷牆.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:墙体结构](../Category/墙体结构.md "wikilink")
[Category:安全屏障](../Category/安全屏障.md "wikilink")

1.  饶平山，[玻璃幕墙的应用及其发展](http://www.wanfangdata.com.cn/qikan/periodical.articles/gyjz/gyjz99/gyjz9912/991210.htm)
    ，《工业建筑》1999年 第29卷 第12期。