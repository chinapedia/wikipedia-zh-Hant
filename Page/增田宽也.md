**增田
寬也**（），[日本政治家](../Page/日本.md "wikilink")，前任[总务大臣](../Page/总务大臣.md "wikilink")。

增田出生于[东京都](../Page/东京都.md "wikilink")[世田谷区](../Page/世田谷区.md "wikilink")。1977年毕业于[東京大學](../Page/東京大學.md "wikilink")，获[法學士學位](../Page/法學士.md "wikilink")，进入[建设省任](../Page/建设省.md "wikilink")[公务员](../Page/公务员.md "wikilink")。增田于1994年12月5日辞职，并于次年当选[岩手县知事](../Page/岩手县知事.md "wikilink")，当时他是日本最年轻的[都道府县政府长官](../Page/都道府县.md "wikilink")。其后于1999年、2003年两次连任岩手县知事。

2007年8月27日，增田出任[安倍晋三内阁的总务大臣](../Page/安倍晋三.md "wikilink")。2007年9月26日，增田出任[福田康夫内阁总务大臣](../Page/福田康夫.md "wikilink")。

[Category:岩手縣知事](../Category/岩手縣知事.md "wikilink")
[Category:日本总务大臣](../Category/日本总务大臣.md "wikilink")
[Category:第一次安倍內閣閣僚](../Category/第一次安倍內閣閣僚.md "wikilink")
[Category:福田康夫內閣閣僚](../Category/福田康夫內閣閣僚.md "wikilink")
[Category:东京都出身人物](../Category/东京都出身人物.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")