**墨尔本**（，[缩写为](../Page/缩写.md "wikilink")）简稱**墨城**，被[華僑稱為](../Page/華僑.md "wikilink")**新金山**，历史上曾譯作**美爾鉢**\[1\]。是位于[澳洲東岸](../Page/澳洲東岸.md "wikilink")[維多利亞州南部的一座](../Page/維多利亞州.md "wikilink")[城市](../Page/城市.md "wikilink")。墨尔本是维多利亞州首府和最大城市，亦是[澳洲人口排名的第](../Page/澳洲人口.md "wikilink")2大城市，也曾是[世界最佳宜居城市的首榜](../Page/世界最佳宜居城市.md "wikilink")\[2\]。大墨尔本地區面積達到3858.1平方英里（9992.5平方公里），是全球最廣大的都會區之一。[墨尔本中央商業區](../Page/墨爾本中央商業區.md "wikilink")\[3\]是整個大都會區的中心，經1993年的行政改制後，現時中央商業區人口約20,000\[4\]。大墨尔本坐落於[菲利普港灣旁](../Page/菲利普港灣.md "wikilink")，一直往內陸伸延。中央商業區位處港灣的北端，亦即是[雅拉河](../Page/雅拉河.md "wikilink")[河口灣](../Page/河口灣.md "wikilink")\[5\]。市民大多数居住在中央商業區周邊30公里范围内，大都會區截止2016年共有488萬人。\[6\]\[7\]\[8\]\[9\]\[10\]都會區由[墨尔本市及](../Page/墨爾本市.md "wikilink")31個[地方政府區域組成](../Page/維多利亞州地方政府區域.md "wikilink")\[11\]。

墨尔本曾经举办过[1956年夏季奧運會](../Page/1956年夏季奧運會.md "wikilink")，是南半球首個舉行奧運會的城市；[2006年英联邦运动会亦在此舉行](../Page/2006年英联邦运动会.md "wikilink")，被譽為「**澳洲的文化首都**」\[12\]，同時亦是全國的[文化](../Page/文化.md "wikilink")、[商業](../Page/商業.md "wikilink")、[教育](../Page/教育.md "wikilink")、[娛樂](../Page/娛樂.md "wikilink")、[體育及](../Page/體育.md "wikilink")[旅遊中心](../Page/旅遊.md "wikilink")\[13\]。墨尔本在[服飾](../Page/服飾.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[音樂](../Page/音樂.md "wikilink")、[電視製作](../Page/電視劇.md "wikilink")、[電影及](../Page/電影.md "wikilink")[舞蹈等潮流文化領域引領澳洲](../Page/舞蹈.md "wikilink")，甚至于全球該領域範圍都具備一定的影響力。墨尔本也是澳洲多民族文化的中心，拥有着为数众多的移民。

2008年，墨尔本獲得[英格蘭](../Page/英格蘭.md "wikilink")[羅浮堡大學的全球化與世界級城市研究小組與網絡評為](../Page/羅浮堡大學.md "wikilink")“[第一YO級世界都市](../Page/全球城市.md "wikilink")”\[14\]。从2002年至今，墨尔本一直佔據由《[經濟學人](../Page/經濟學人.md "wikilink")》選出的[世界最佳居住城市的三甲位置](../Page/世界最佳居住城市.md "wikilink")\[15\]\[16\]\[17\]\[18\]\[19\]，2011年到2017年连续七年位列第一\[20\]\[21\]。墨尔本城市的[座右銘是](../Page/座右銘.md "wikilink")“[隨行聚力](../Page/隨行聚力.md "wikilink")”。

## 歷史

墨尔本原稱“****”，在歐洲人殖民[范迪門斯地](../Page/塔斯曼尼亞州.md "wikilink")47年後，即1835年設立\[22\]。1837年，更名為墨尔本，以紀念[英國首相](../Page/英國首相.md "wikilink")[第二代墨爾本子爵威廉·蘭姆](../Page/第二代墨爾本子爵威廉·蘭姆.md "wikilink")\[23\]。本城最早的原住民居住於現在的[雅拉河畔以及菲利普島](../Page/雅拉河.md "wikilink")，原住民在墨城所居住的歲月超過40,000年。英國政府於1803年-{制}-定殖民地法案，-{並}-在沙利文灣規劃菲利普港區，但是這一規劃在-{制}-定后即被放棄數月。

1835年5月至6月間，在位於現在的墨城市中心以及北區籌組「菲利普港口協會」，-{並}-與當地原住民部落領袖
酋长商討且達成了一項600,000畝的土地的永久交易計劃。隨後John
Batman當即選擇了位於雅拉河北岸的一片廣大土地，表示將建設成一個大型的村落，隨後其與大部分菲利普港協會成員返回位於[塔斯馬尼亞島的根據地](../Page/塔斯馬尼亞島.md "wikilink")。於此同時，另外一個由[约翰·贝霖領導的協會抵達同一地點](../Page/约翰·贝霖.md "wikilink")-{並}-建立新的居住地。至此兩個不同的協會開始為這片土地的所有權產生不愉快。1835年8月30日，兩個協會達成一個共同分享的和平協議，而此前由菲利普港口協會與原住民所達成的交易被取消，且由[新南威爾士州政府](../Page/新南威爾士州.md "wikilink")給予菲利普港口協會適當的補償。至此，定居者可以任意地選擇該區域的任何位置定居，政府亦開始勉強地接受來自英國的新定居者，並-{准}-許在此建立新的[城市](../Page/城市.md "wikilink")。

墨尔本於1835年正式建城。1840年全城人口有1萬人，最早的城市移民來自[英國的自由民](../Page/英國.md "wikilink")。墨尔本於1847年獲[維多利亞女皇授予城市地位](../Page/維多利亞女皇.md "wikilink")，其後成為剛設立的[維多利亞州](../Page/維多利亞州.md "wikilink")[首府](../Page/首府.md "wikilink")。1850年，殖民者在墨尔本周邊發現金礦，使其快速發展，來自世界各地的大批人口紛紛移民至，其中包括[英國](../Page/英國.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[德國](../Page/德國.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[荷蘭及](../Page/荷蘭.md "wikilink")[中國等](../Page/中國.md "wikilink")，墨尔本遂得名新金山；另外的一座在此前發現金礦的美國城市金山則被改稱為[舊-{金}-山](../Page/舊金山.md "wikilink")。至1854年，墨尔本的人口達到123,000人，超過舊金山的淘金人數。1850年代，英國設立[維多利亞殖民區期間](../Page/維多利亞殖民區.md "wikilink")，擇定墨尔本為殖民區的[府城](../Page/府城.md "wikilink")。

1901年，澳洲聯邦成立時，墨尔本已是全國的金融與文化中心，[澳大利亚联邦议会的驻地也在](../Page/澳大利亚联邦议会.md "wikilink")[墨尔本国会大厦](../Page/国会大厦_\(墨尔本\).md "wikilink")，然而當時[新南威爾斯州的首府](../Page/新南威爾斯州.md "wikilink")[悉尼人口已超越墨尔本](../Page/悉尼.md "wikilink")，工商業的發展亦高於墨尔本，雙城皆力爭聯邦首都的地位，使定都的問題陷入僵局。為了平衡雙城之爭，1900年通過的[澳大利亞憲法規定首都應設於悉尼所在的](../Page/澳大利亞憲法.md "wikilink")[新南威爾士境内但必須距離悉尼至少](../Page/新南威爾士.md "wikilink")200英里。最後澳洲在1908年決議將首都設在相當於雙城之間的[坎培拉](../Page/坎培拉.md "wikilink")。惟遲至1927年，[澳洲國會才真正從墨尔本遷往坎培拉](../Page/澳洲國會.md "wikilink")\[24\]。

## 气候

墨尔本地处[大分水岭以东](../Page/大分水岭.md "wikilink")，常年受南太平洋暖流影响，可以说是[地中海气候或](../Page/地中海式气候.md "wikilink")[温带海洋性气候](../Page/温带海洋性气候.md "wikilink")，全年气温变化较为和缓，降水分布均匀（下半年降水量略多），但由于同时受到印度洋冷风的影响，墨尔本日温差大（超过10°C）的天气状况比较常见。冬季温暖，略微潮湿，降雪少见（墨尔本地区上一次降雪追溯到1986年7月）；夏季温热，相对干燥，高温天气少见，偶有极端高温（尤其是[热浪发生时](../Page/热浪.md "wikilink")）。最冷月为7月，平均气温9.8℃；最热月为1月，平均气温20.1℃。极端最低气温−2.8
°C（1901年7月4日），极端最高气温46.4℃（2009年2月7日）。

墨尔本的气候虽然普遍较为舒适，但由于受[气候变暖的影响](../Page/气候变暖.md "wikilink")，近年来其平均温度（尤其是夏季）呈明显上升趋势，极端天气事件发生的频率和程度也显著升高和加深。例如2009年1月末至2月初，热浪突袭澳大利亚，墨尔本是主要的受影响城市之一。该市保持了150年之久的最高温纪录就在此期间被打破
\[25\]。加上[臭氧層空洞的問題](../Page/臭氧層空洞.md "wikilink")，墨尔本夏季的阳光更加容易引起[皮肤癌](../Page/皮肤癌.md "wikilink")。市民在外出時，均會塗上[防曬油以減少紫外线的傷害](../Page/防曬油.md "wikilink")。

## 行政區劃

[Greater_Melbourne_Map_4_-_May_2008.png](https://zh.wikipedia.org/wiki/File:Greater_Melbourne_Map_4_-_May_2008.png "fig:Greater_Melbourne_Map_4_-_May_2008.png")

自1989年行政區域改制后，大墨尔本地區（Greater Melbourne）原本的30個區被重新規劃爲32個市議會。
大墨尔本地區的範圍包括墨尔本中央商務區周邊的地區以及改制之前墨尔本的區，而這32個地方議會亦包括了三個“[郡](../Page/郡.md "wikilink")”。近二十年來，墨尔本市城市化發展迅速，為澳洲人口增長最快的城市；這亦直接導致了大墨尔本地區的不斷快速外擴。因為不斷增長的人口以及城區的擴大，隨之而來的帶動了交通以及服務行業的交叉外擴，許多新區域的形成在行政方面亦擴大了當地議會、警察局、學校等服務部門的影響力。

近數十年來隨著墨尔本的工業區轉型，之前的大部份[重工業工廠以及污染行業全部遷移至亞洲地區](../Page/重工業.md "wikilink")；一些之前的工業區亦變成以住宅為主的區域。而隨著人口增長以及交通的快速發展亦會帶來許多相應的新型住宅區。

下列是墨尔本地區的32個地方議會，而這些議會亦是由諾干個區組成。

  - [墨爾本市](../Page/墨爾本市.md "wikilink")

  - [菲利普港](../Page/菲利普港.md "wikilink")

  - [云登市](../Page/云登市.md "wikilink")

  -
  - [雅拉市](../Page/雅拉市.md "wikilink")

  -
  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  - [賴德市](../Page/賴德市.md "wikilink")

<!-- end list -->

  -
  -
  -
  - [白馬市](../Page/白馬市.md "wikilink")

  -
  -
  - [弗蘭克斯頓](../Page/弗蘭克斯頓.md "wikilink")

  - [蘭域市](../Page/蘭域市.md "wikilink")

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
## 經濟

墨尔本是全澳乃至[亞太地區的經濟和商業中心城市之一](../Page/亞太地區.md "wikilink")。多家澳洲大型公司的總部位於墨尔本中央商務區以及東南區的[莫納什大學周邊](../Page/莫納什大學.md "wikilink")，包括澳洲四大銀行當中的[澳紐銀行](../Page/澳紐銀行.md "wikilink")，[澳洲國民銀行](../Page/澳洲國民銀行.md "wikilink")；全球礦業巨頭[力拓集團和](../Page/力拓集團.md "wikilink")[必和必拓](../Page/必和必拓.md "wikilink")；[Medibank私人保險](../Page/Medibank私人保險.md "wikilink")，電信行業巨頭[Telstra](../Page/Telstra.md "wikilink")，零售業巨頭[Coles超級市場](../Page/Coles超級市場.md "wikilink")，百货行业巨头[邁爾](../Page/邁爾.md "wikilink")；汽车业巨头[霍顿](../Page/霍顿汽车.md "wikilink")；電子零售業巨頭[JB
HI-FI等](../Page/JB_HI-FI.md "wikilink")。亦有多家跨国公司的澳洲乃至南半球总部设立于墨尔本，包括[奔驰](../Page/奔驰.md "wikilink")、[寶馬](../Page/寶馬.md "wikilink")、[東芝](../Page/東芝.md "wikilink")、[惠普](../Page/惠普.md "wikilink")、[索尼](../Page/索尼.md "wikilink")、[馬自達汽車](../Page/馬自達汽車.md "wikilink")、[豐田汽車](../Page/豐田汽車.md "wikilink")、[本田汽車](../Page/本田汽車.md "wikilink")、[日產汽車](../Page/日產汽車.md "wikilink")、[福特汽車](../Page/福特汽車.md "wikilink")、[7-Eleven](../Page/7-Eleven.md "wikilink")、[安聯](../Page/安聯.md "wikilink")、[野村證券](../Page/野村證券.md "wikilink")、[德勤會計師事務所](../Page/德勤會計師事務所.md "wikilink")、[畢馬威會計師事務所](../Page/畢馬威會計師事務所.md "wikilink")、[ING集團](../Page/ING集團.md "wikilink")、[摩根大通](../Page/摩根大通.md "wikilink")、[KFC等](../Page/KFC.md "wikilink")。

2012年4月，墨城的失業率為5.2%，略低於全國平均水準的5.4%。\[26\]根據《[經濟學人](../Page/經濟學人.md "wikilink")》於2012年的統計，墨尔本是世界上生活成本第8昂貴的城市。\[27\]而墨城的房價自1990年代開始后亦逐年遞增，漲幅度在20年間達到了400%。

當地政府亦會保障當地人優先就業，而在墨爾本，時薪平均約25元，最低工資為18元\[28\]。

墨尔本亦是全球[生物科技的中心城市之一和全澳的](../Page/生物科技.md "wikilink")[科技中心](../Page/科技.md "wikilink")，澳洲以及世界各國的大多數[生命科學](../Page/生命科學.md "wikilink")、[電子](../Page/電子.md "wikilink")、[高科技](../Page/高科技.md "wikilink")、[能源企業總部或是澳洲](../Page/能源.md "wikilink")（澳紐）總部都位於墨城東南部的[莫納什大學周邊地區](../Page/莫納什大學.md "wikilink")；該地區附加了多個莫納什大學以及[墨尔本大學的大型實驗室](../Page/墨爾本大學.md "wikilink")，亦被稱為是“澳洲矽谷”。
墨尔本的服務行業極為發達，是澳洲和南半球的购物中心城市。眾多大型購物中心遍佈大墨尔本地區。
[Melbourne_Skyline_from_Rialto_Crop_-_Nov_2008.jpg](https://zh.wikipedia.org/wiki/File:Melbourne_Skyline_from_Rialto_Crop_-_Nov_2008.jpg "fig:Melbourne_Skyline_from_Rialto_Crop_-_Nov_2008.jpg")是全澳乃至亞太地區的工商業中心之一\]\]

## 文化

[Melbourne_Flinders_St._Station.jpg](https://zh.wikipedia.org/wiki/File:Melbourne_Flinders_St._Station.jpg "fig:Melbourne_Flinders_St._Station.jpg")
[Rialto_Towers_Eastern.jpg](https://zh.wikipedia.org/wiki/File:Rialto_Towers_Eastern.jpg "fig:Rialto_Towers_Eastern.jpg")525號的[丽奥图大厦双塔於](../Page/丽奥图大厦双塔.md "wikilink")1986年完工，亦是[世界高塔聯盟的總部所在地](../Page/世界高塔聯盟.md "wikilink")\]\]

墨尔本被公認為是[澳洲文化的靈魂城市](../Page/澳大利亚文化.md "wikilink")，市民以極高的文化涵養而著稱於世。城市無論在[建築](../Page/建築.md "wikilink")、[多元文化](../Page/多元文化.md "wikilink")、[飲食](../Page/飲食.md "wikilink")、[文學](../Page/文學.md "wikilink")、[體育](../Page/體育.md "wikilink")、[藝術及](../Page/藝術.md "wikilink")[時尚方面都處於澳洲乃至世界的最前沿](../Page/時尚.md "wikilink")。此外多項聞名的文化及藝術活動亦會每年於墨尔本舉行。

### 建築

墨尔本市拥有众多的[維多利亞式建築](../Page/維多利亞式房屋.md "wikilink"),
数量在全球仅次于[伦敦](../Page/伦敦.md "wikilink")。此外一些近代產生的[哥德式建築亦成為當代建築的典範](../Page/哥德式建築.md "wikilink")。墨城市中心擁有眾多的新式以及代表著城市發展史的古老建築，交相辉映，景象独特。此外，有[小意大利之稱的](../Page/小意大利.md "wikilink")以及附近擁有大量的數代[義大利移民帶來的意式文化以及意大利風格的建築](../Page/義大利.md "wikilink")。此外在例如[海德堡等](../Page/海德堡.md "wikilink")[德國後裔雲集的區域亦有各式各樣的德式風格建築](../Page/德國.md "wikilink")。

墨城的城中心亦擁有眾多地標建築，包括位於墨尔本大學內的大量飽含歷史的維多利亞式建築，有“旋舞的裙子”之稱的世界知名建築[維多利亞藝術中心](../Page/維多利亞藝術中心.md "wikilink")、現代維多利亞式建築的典範[墨尔本演奏中心位於](../Page/墨爾本演奏中心.md "wikilink")[南岸](../Page/南岸_\(維多利亞州\).md "wikilink")、而現代建築[丽奥图大厦双塔以其碧藍的塔身聞名於世](../Page/丽奥图大厦双塔.md "wikilink")、[發現大樓則曾是世界上最高的純住宅大廈](../Page/發現大樓.md "wikilink")，為目前墨城的最高建築物、[維多利亞國立美術館](../Page/維多利亞國立美術館.md "wikilink")（NGV）宽阔的水幕墙，皆是墨市多元文化匯聚的體現。墨城其它的著名建築還包括[維多利亞州立圖書館及](../Page/維多利亞州立圖書館.md "wikilink")[墨尔本市政廳等](../Page/墨爾本市政廳.md "wikilink")。

墨尔本有南半球的“教堂之城”之美譽，城內存在大量[維多利亞時期遺留下的大量各式的大型教堂](../Page/維多利亞女王.md "wikilink")，其中包括著名的[衛斯理堂](../Page/衛斯理堂_\(墨爾本\).md "wikilink")、[聖保羅座堂](../Page/聖保羅座堂_\(墨爾本\).md "wikilink")、[蘇格蘭教堂](../Page/蘇格蘭教堂_\(墨爾本\).md "wikilink")、[聖巴特利爵主教座堂及](../Page/聖巴特利爵主教座堂_\(墨爾本\).md "wikilink")[聖米迦勒聯合教會等](../Page/聖米迦勒聯合教會.md "wikilink")。

[皇家展覽館位於墨城市中心東北部的卡頓園林內](../Page/皇家展覽館.md "wikilink")，是澳洲第一座被評為[聯合國教科文組織](../Page/聯合國教科文組織.md "wikilink")[世界遺產的建築](../Page/世界遺產.md "wikilink")。展覽館於1880年由澳洲著名建築師設計，並於1888年完工在墨尔本國際展覽會時投入使用，此后成为城市最受到矚目的地標建築之一。另外一座地標建築是弗林德斯街火車站，該車站始建於1879年，並與1883年開放使用。現今車站作為[墨尔本軌道交通中的中心環線的主要中轉車站之一平均每日大概接納](../Page/墨爾本軌道交通.md "wikilink")850,900人次的通勤者。\[29\]現今弗林德斯街站因為其獨特的建築風格以及所處的地理位置，已經成為墨尔本的城市象徵之一，曾多次出現於電影和電視等公眾媒體當中。位于市中心东端的[国会大厦则是澳大利亚新古典主义风格政府建筑的突出代表](../Page/国会大厦_\(墨尔本\).md "wikilink")，公认为澳大利亚各级立法机构使用的历史建筑中最庄严宏伟的一座。

### 多元文化

同澳洲其它城市一樣，墨城的居民以[盎格魯和](../Page/盎格魯.md "wikilink")[撒克遜血統的英國和愛爾蘭移民人數以及後裔為最多](../Page/撒克遜.md "wikilink")。且另有大量来自歐洲大陸的移民，包括[德國](../Page/德國.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[希臘](../Page/希臘.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[法國](../Page/法國.md "wikilink")、[俄羅斯以及](../Page/俄羅斯.md "wikilink")[東歐諸國](../Page/東歐.md "wikilink")。亚洲移民主要以来自[中東](../Page/中東.md "wikilink")、[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[印度](../Page/印度.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")（[中華民國](../Page/中華民國.md "wikilink")）、[香港](../Page/香港.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印尼以及](../Page/印尼.md "wikilink")[越南爲主](../Page/越南.md "wikilink")。

### 華人及亞裔文化

[Chinese-new-year-melbourne.JPG](https://zh.wikipedia.org/wiki/File:Chinese-new-year-melbourne.JPG "fig:Chinese-new-year-melbourne.JPG")時的[墨尔本華埠](../Page/墨爾本唐人街.md "wikilink")\]\]

墨城中央的[中國城聚集着很多十九世紀中葉淘金热时代来澳的華人後裔](../Page/墨爾本唐人街.md "wikilink")。1990年代后，與西方其它移民國家大城市一樣，隨著一些華人社區在郊區的崛起，華埠在墨尔本亦有衰敗的跡象。城市遠郊的[博士山](../Page/博士山.md "wikilink")（Box
Hill）、唐卡斯特（Doncaster）、威福利谷（Glen
Waverley）等以華人為主的區域先後雲集了大量的華人商業中心，亦有越來越多的華人聚集在這些區域，形成了墨城的多個[華人社區](../Page/華人社區.md "wikilink")。墨尔本唐人街在2012年被美國有線新聞網評選為世界最佳唐人街之一，墨城的唐人街亦會在每年的[端午節舉辦世界上最大的](../Page/端午節.md "wikilink")[龍舟比賽](../Page/龍舟.md "wikilink")\[30\]。2000年代后，隨著中國大陸改革開放的進一步深入，越來越多的中國留學生和新移民湧入墨尔本，亦為這些區域的工商行業帶來了繁榮。

如今的博士山和威福利谷等區域雲集了大量的亞洲超級市場、中餐館以及其他的專營亞洲商品的商店，而食品中亦充滿了中港台以及東南亞地區各國的產品。而墨城本地亦有多份華語報紙，一些香港和台灣的傳媒亦有進入墨尔本發展，如：《[星島日報](../Page/星島日報.md "wikilink")（墨尔本版）》、《[新報](../Page/新報.md "wikilink")（墨尔本版）》、《大洋時報》、《墨尔本日報》、《澳洲日報》、《東方郵報》、《澳洲新報》、《聯合時報》等。而本地的華人電臺在1990年代后亦開始繁榮。

主要集中在富士貴區（Footscray）、史賓威區（Springvale）和列治文區（Richmond）。[韓裔社區主要集中在卡內基區](../Page/韓裔澳洲人.md "wikilink")（Carnegie）、庫揚區（Kooyong）、圖羅加區(Tooronga)。主要在[南岸區以及布萊頓區](../Page/南岸_\(維多利亞州\).md "wikilink")(Brighton)。留学生大部聚居在各大学附近，如考菲尔德區（Caulfield）、克雷顿區（Clayton）等。

墨尔本現任市長是道爾（Robert
Doyle）。上一任是華裔澳洲人[蘇震西](../Page/蘇震西.md "wikilink")，出生於[香港](../Page/香港.md "wikilink")；少年時隨父母移民澳洲。2001年7月，蘇震西當選了府城首任民選市長，在此之前墨市市長皆由城市委員會所任命。2004年蘇震西成功連任墨尔本市市長，2006年12月獲選為全球最佳市長（World
Mayor 2006）\[31\]。

### 藝術和時尚

墨尔本是澳洲的藝術和時尚之都，亦是南半球最有浪漫格調和藝術家雲集的城市。\[32\]位於[墨尔本中央商業區的](../Page/墨爾本中央商業區.md "wikilink")[科林斯街是全澳乃至南半球最為著名的商業街及](../Page/科林斯街.md "wikilink")[奢侈品購物集中地](../Page/奢侈品.md "wikilink")，雲集了世界各地名牌服飾、珠寶、手錶等品牌的旗艦店。

位於墨城東南的是南半球最大的購物中心，面積達19萬平方米，亦是全球第三大的購物中心\[33\]\[34\]。

### 音樂

墨尔本是澳洲音樂產業最為繁榮的城市之一，自二戰以來產生了大量知名的流行音樂團體或個人。杰出代表有[Kylie
Minogue等](../Page/凯莉·米洛.md "wikilink")。墨尔本的中央商務區亦是全澳乃至世界各地的街頭藝人自由表演的大型舞臺，每天都有許多不同的藝術家在墨城的市中心進行公開表演。而[維多利亞藝術中心以及](../Page/維多利亞藝術中心.md "wikilink")[墨尔本演奏中心幾乎每週都有大型樂團進行演奏表演或音樂劇目表演](../Page/墨爾本演奏中心.md "wikilink")。

是澳洲歷史最為悠久的城市交響樂團，亦是一個具有國際聲譽的樂團之一；樂團目前的指揮為英國[指揮家](../Page/指揮家.md "wikilink")。

墨尔本每一年亦會舉辦的多個音樂活動，如：墨尔本國際爵士音樂節（Melbourne International Jazz
Festiva），聲浪音樂節 (SoundWave)，聖杰洛米巷道音樂節 (St. Jerome’s Laneway
Festival)，Stereosonic音樂節及未來音樂節 (Future Music Festival)等。

### 教育

墨尔本市是全澳大利亚乃至南半球的教育中心之一。主要大学有：

  - [墨尔本大學](../Page/墨爾本大學.md "wikilink")
  - [莫納什大學](../Page/蒙納什大學.md "wikilink")
  - [拉籌伯大學](../Page/拉籌伯大學.md "wikilink")
  - [迪肯大学](../Page/迪肯大学.md "wikilink")
  - [墨尔本皇家理工大学](../Page/墨尔本皇家理工大学.md "wikilink")
  - [旋濱科技大學](../Page/旋濱科技大學.md "wikilink")
  - [維多利亞大學](../Page/維多利亞大學_\(澳大利亞\).md "wikilink")

### 傳媒

[The_Age_Collins_St_2010.jpg](https://zh.wikipedia.org/wiki/File:The_Age_Collins_St_2010.jpg "fig:The_Age_Collins_St_2010.jpg")報社的總部，位於墨尔本著名的[柯林斯大街](../Page/科林斯街.md "wikilink")\]\]

墨尔本是全澳的新聞中心城市之一，[新聞集團的](../Page/新聞集團.md "wikilink")[默多克家族早期的發展亦於墨尔本起步](../Page/默多克.md "wikilink")。城市發行量最大的兩份報紙分屬1840年創刊的《》和1854年開始發行的《[時代報](../Page/世纪报.md "wikilink")》。這兩份地方報紙都是在澳洲全國具有相當影響的報紙，在其他州和地區亦有小量發行。《太陽先驅報》隸屬於[新聞集團](../Page/新聞集團.md "wikilink")，以綜合新聞報導為主，文稿的風格亦偏具娛樂性。而《時代報》則為旗下最為重要的報紙之一，風格保守，以政治性主題新聞見長。

墨尔本是[七號電視網的全國總部所在地](../Page/七號電視網.md "wikilink")，七號電視網是全國最大的私營電視傳媒機構之一，亦是目前全國收視群體最高的電視臺。隸屬總部位於墨尔本的，而西部七號傳媒集團亦控制[雅虎澳洲及](../Page/雅虎.md "wikilink")[天空電視等全國性質的傳媒機構](../Page/天空電視.md "wikilink")。[Parkville_-_University_of_Melbourne_(Ormond_College).jpg](https://zh.wikipedia.org/wiki/File:Parkville_-_University_of_Melbourne_\(Ormond_College\).jpg "fig:Parkville_-_University_of_Melbourne_(Ormond_College).jpg")\]\]

### 体育

[CG-MelbCricketGround-Pano.jpg](https://zh.wikipedia.org/wiki/File:CG-MelbCricketGround-Pano.jpg "fig:CG-MelbCricketGround-Pano.jpg")(MCG)是全澳最大的體育場，亦是[1956年奧運會的主賽場](../Page/1956年奧運會.md "wikilink")。圖為[澳洲隊於MCG迎戰前來挑戰的](../Page/澳洲國家足球隊.md "wikilink")[希臘隊](../Page/希臘國家足球隊.md "wikilink")\]\]

墨尔本市是世界上最重要的体育中心城市之一，1956年曾舉辦澳洲以至南半球首次的[奧運會](../Page/1956年墨爾本奥运会.md "wikilink")。2006年，墨尔本舉辦[英聯邦運動會亦堪稱成功](../Page/2006年英聯邦運動會.md "wikilink")。[2007年世界游泳錦標賽於墨尔本成功舉辦](../Page/2007年世界游泳錦標賽.md "wikilink")。

[國際男子職業網球總會四大满贯之一的](../Page/國際男子職業網球總會.md "wikilink")[澳大利亚网球公开赛於每年的一月於墨尔本公園舉行](../Page/澳大利亚网球公开赛.md "wikilink")，每年均有來自世界各地的網球愛好者以及遊客雲集於[墨尔本公園外場的草坪安營紮寨](../Page/墨爾本公園.md "wikilink")，形成一個獨特的風景。作為全球網球運動的重鎮，墨尔本在歷史中曾產生大量的網球巨星；而位於城市東南的（Kooyong
Stadium）亦是1988年以前的澳网主賽場，之後成為澳洲以及世界許多網球巨星的主要訓練場地。

[国际汽车联合会的](../Page/国际汽车联合会.md "wikilink")[一级方程式於每年賽事的第一站的](../Page/一级方程式.md "wikilink")[澳洲大獎賽都在墨尔本的](../Page/澳洲大獎賽.md "wikilink")[亞伯公園賽道举行](../Page/亞伯公園賽道.md "wikilink")，亞伯公園賽道亦是世界上三大城市賽道之一，在不進行比賽的普通時段，是作為公共道路開放於社會使用的。

墨尔本亦是[澳式足球的发源地](../Page/澳式足球.md "wikilink")，大墨尔本地區擁有眾多的[AFL大聯盟球隊](../Page/AFL.md "wikilink")。每年9月中旬AFL的總決賽均固定在[墨尔本板球场举行](../Page/墨尔本板球场.md "wikilink")。

每年11月有春季赛马节，[墨尔本杯是全球四大赛马赛事之一](../Page/墨尔本杯.md "wikilink")，亦是澳大利亚全国矚目的重大赛事。本市还有许多职业的澳式足球俱乐部，板球俱乐部以及足球俱乐部。主要的足球俱乐部以[A-League的](../Page/A-League.md "wikilink")[墨尔本勝利最为有名](../Page/墨爾本勝利足球俱樂部.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>联盟</p></th>
<th><p>运动</p></th>
<th><p>主场</p></th>
<th><p>成立年份</p></th>
<th><p>锦标赛</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/板球.md" title="wikilink">板球</a></p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1851年</p></td>
<td><p>24次謝菲爾德盾杯賽冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡爾頓澳式足球會.md" title="wikilink">卡爾頓</a></p></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p><a href="../Page/澳式足球.md" title="wikilink">澳式足球</a></p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1864年</p></td>
<td><p>16次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列治文.md" title="wikilink">列治文</a></p></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1885年</p></td>
<td><p>10次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哥寧伍澳式足球會.md" title="wikilink">哥寧伍</a></p></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1892年</p></td>
<td><p>15次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1902年</p></td>
<td><p>10次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/墨爾本木球場.md" title="wikilink">墨尔本木球場</a></p></td>
<td><p>1859年</p></td>
<td><p>12次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿辛頓澳式足球會.md" title="wikilink">阿辛頓</a></p></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/阿提哈德球場.md" title="wikilink">阿提哈德球場</a></p></td>
<td><p>1871年</p></td>
<td><p>16次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/阿提哈德球場.md" title="wikilink">阿提哈德球場</a></p></td>
<td><p>1869年</p></td>
<td><p>4次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/阿提哈德球場.md" title="wikilink">阿提哈德球場</a></p></td>
<td><p>1873年</p></td>
<td><p>1次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/澳大利亞澳式足球聯盟.md" title="wikilink">澳大利亞澳式足球聯盟</a>（AFL）</p></td>
<td><p>澳式足球</p></td>
<td><p><a href="../Page/阿提哈德球場.md" title="wikilink">阿提哈德球場</a></p></td>
<td><p>1883年</p></td>
<td><p>9次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳洲職業棒球聯盟.md" title="wikilink">澳洲職業棒球聯盟</a>（ABL）</p></td>
<td><p><a href="../Page/棒球.md" title="wikilink">棒球</a></p></td>
<td></td>
<td><p>2009年</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>（AIHL）</p></td>
<td><p><a href="../Page/冰球.md" title="wikilink">冰球</a></p></td>
<td></td>
<td><p>2002年</p></td>
<td><p>2次聯盟總冠軍</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/超級橄欖球聯賽.md" title="wikilink">超級橄欖球聯賽</a>（Super Rugby）</p></td>
<td><p><a href="../Page/英式橄欖球.md" title="wikilink">英式橄欖球</a></p></td>
<td><p><a href="../Page/墨爾本矩形球場.md" title="wikilink">墨尔本矩形球場</a></p></td>
<td><p>2010年</p></td>
<td><p>1次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>（AHL）</p></td>
<td><p><a href="../Page/曲棍球.md" title="wikilink">曲棍球</a></p></td>
<td></td>
<td><p>1991年</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/墨爾本胜利.md" title="wikilink">墨尔本胜利</a></p></td>
<td><p><a href="../Page/澳洲足球超級聯賽.md" title="wikilink">澳洲足球超級聯賽</a>（A-League）</p></td>
<td><p><a href="../Page/足球.md" title="wikilink">足球</a></p></td>
<td><p><a href="../Page/墨爾本矩形球場.md" title="wikilink">墨尔本矩形球場和</a><a href="../Page/阿提哈德球場.md" title="wikilink">阿提哈德球場</a></p></td>
<td><p>2004年</p></td>
<td><p>2次聯盟總決賽冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/墨爾本城足球俱樂部.md" title="wikilink">墨爾本城</a></p></td>
<td><p><a href="../Page/澳洲足球超級聯賽.md" title="wikilink">澳洲足球超級聯賽</a>（A-League）</p></td>
<td><p><a href="../Page/足球.md" title="wikilink">足球</a></p></td>
<td><p><a href="../Page/墨爾本矩形球場.md" title="wikilink">墨尔本矩形球場</a></p></td>
<td><p>2008年</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳洲國家籃球聯賽.md" title="wikilink">澳洲國家籃球聯賽</a>（NBL）</p></td>
<td><p><a href="../Page/籃球.md" title="wikilink">籃球</a></p></td>
<td></td>
<td><p>1931年</p></td>
<td><p>4次聯盟總冠軍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/全國橄欖球聯賽.md" title="wikilink">國家橄欖球聯盟</a>（NRL）</p></td>
<td><p><a href="../Page/橄榄球.md" title="wikilink">橄榄球</a></p></td>
<td><p><a href="../Page/墨爾本矩形球場.md" title="wikilink">墨尔本矩形球場</a></p></td>
<td><p>1997年</p></td>
<td><p>4次聯盟總冠軍</p></td>
</tr>
</tbody>
</table>

## 交通

### 公路

[Peak_hour_traffic_in_melbourne.jpg](https://zh.wikipedia.org/wiki/File:Peak_hour_traffic_in_melbourne.jpg "fig:Peak_hour_traffic_in_melbourne.jpg")的莫納什高速公路\]\]

墨尔本擁有一個由數個交叉高速公路組成的龐大高速公路網絡，市區內的16條高速公路在城市的南北東西縱橫交錯，其中有兩條路段因為交通流量擁堵而在2000年代后開始採用[電子道路收費系統向通勤者收取費用](../Page/電子道路收費系統.md "wikilink")。

#### 收費路段

[Bolte_bridge_dusk.jpg](https://zh.wikipedia.org/wiki/File:Bolte_bridge_dusk.jpg "fig:Bolte_bridge_dusk.jpg")

  - [Australian_Alphanumeric_State_Route_M1.svg](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M1.svg "fig:Australian_Alphanumeric_State_Route_M1.svg")
    市區專線（City Link）和机场专线。
  - [Australian_Alphanumeric_State_Route_M3.PNG](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M3.PNG "fig:Australian_Alphanumeric_State_Route_M3.PNG")
    東部專線（Eastern Link）只在南北走向的東部地區收費。

#### 高速公路

  - [Australian_Alphanumeric_State_Route_M1.svg](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M1.svg "fig:Australian_Alphanumeric_State_Route_M1.svg")
    莫納什高速公路：是一條連接墨尔本東南區和西區的主要高速公路之一，起始於太子路東段直到墨尔本西南部的衛星城市[吉朗](../Page/吉朗.md "wikilink")，全長159公里。
  - [Australian_Alphanumeric_State_Route_M2.PNG](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M2.PNG "fig:Australian_Alphanumeric_State_Route_M2.PNG")
    圖拉馬瑞尼高速公路：是一條可以銜接[Australian_Alphanumeric_State_Route_M1.svg](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M1.svg "fig:Australian_Alphanumeric_State_Route_M1.svg")莫納什高速公路的一條機場高速公路，全長13公里。
  - [Australian_Alphanumeric_State_Route_M3.PNG](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M3.PNG "fig:Australian_Alphanumeric_State_Route_M3.PNG")
    東部高速公路：一條由[莫寧頓半島自墨城東部而到達市中心的東部主要幹道](../Page/莫寧頓半島.md "wikilink")，全長62公里。
  - [Australian_Alphanumeric_State_Route_M11.PNG](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M11.PNG "fig:Australian_Alphanumeric_State_Route_M11.PNG")
    半岛链接 : 一从登上玛莎南端到东部高速公路北端，全長25公里。
  - [Australian_National_Route_M80.svg](https://zh.wikipedia.org/wiki/File:Australian_National_Route_M80.svg "fig:Australian_National_Route_M80.svg")
    墨尔本環線（西部環線）：為一條環繞墨尔本都市西部的高速公路，可以連接至[Australian_Alphanumeric_State_Route_M2.PNG](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M2.PNG "fig:Australian_Alphanumeric_State_Route_M2.PNG")的圖拉馬瑞尼高速公路直達墨尔本國際機場以及[Australian_Alphanumeric_State_Route_M1.svg](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_M1.svg "fig:Australian_Alphanumeric_State_Route_M1.svg")進入市中心和東南區，全長38公里。
  - [Australian_National_Route_M8.svg](https://zh.wikipedia.org/wiki/File:Australian_National_Route_M8.svg "fig:Australian_National_Route_M8.svg")
    西部高速公路：是一條可以連接墨尔本西南區以及西北區的主要高速路，通過[Australian_National_Route_M8.svg](https://zh.wikipedia.org/wiki/File:Australian_National_Route_M8.svg "fig:Australian_National_Route_M8.svg")西部高速可以直接連接至洲際公路[Australian_Alphanumeric_State_Route_B220.svg](https://zh.wikipedia.org/wiki/File:Australian_Alphanumeric_State_Route_B220.svg "fig:Australian_Alphanumeric_State_Route_B220.svg")從而到達澳洲南部城市[阿德萊德](../Page/阿德萊德.md "wikilink")。

### 軌道交通

墨尔本擁有全澳最為龐大，以長度計算為世界都市中排名第3大的[軌道交通系統](../Page/城市軌道交通系統.md "wikilink")，其城市[輕軌系統全長達到](../Page/輕軌.md "wikilink")372公里（不包含有軌電車系統）\[35\]，擁有200個車站\[36\]。

#### 通勤铁路

墨尔本的通勤铁路由包含16条电气化线路的4个线组(Group)以及位于市中心的城市環線(City
Loop)组成，总长度达到了830km，2016年总客流达到了2.354亿人次。不过由于大多数线路始建于100多年前，设施老化，导致在日常运营中经常发生延误甚至中断运营的情况。另外，城市环线由于只为每个线组分配了1条轨道，导致在高峰时班次受到了很大限制。为了解决这一问题，一个长度为9公里，有5座車站的墨尔本地铁隧道项目已经开工，预计于2026投入使用。该项目旨在构建一个地铁式的(Metro
Style)运行系统，连接现有的克兰本线、派肯汉线以及森伯里线、未来将会在2019年电气化的梅尔顿线。\[37\]

#### 有軌電車系統

[缩略图](https://zh.wikipedia.org/wiki/File:Melbourne_trams_map.gif "fig:缩略图")
墨尔本为全球最為龐大的有軌電車系統，\[38\]\[39\]該系統始於1880年代，亦是全球最古老的輕鐵系統之一。2010-2011年度有1億2千8百70萬人次的乘客使用該系統。每天有487輛[有軌電車在](../Page/有軌電車.md "wikilink")28條線路上運營該系統，車站的數量更是達到1,773個之多。\[40\]\[41\]\[42\]

### 機場

大墨尔本地區擁有共五座[飛機場](../Page/飛機場.md "wikilink")，其中[墨尔本國際機場為最大型的綜合國際機場](../Page/墨爾本國際機場.md "wikilink")，機場貨物吞吐量為澳洲最大，客運量第二大，僅次於[悉尼國際機場](../Page/悉尼國際機場.md "wikilink")。

### 港口

[墨尔本港開通於](../Page/墨爾本港.md "wikilink")1889年。港區面積143,000平方米，擁有超過30個的泊位、以及16個平臺。年吞吐量在190萬個[貨櫃](../Page/20呎標準貨櫃.md "wikilink")。以目前的設計能力，預計到2015年港口將會飽和，擴建亦迫在眉睫。\[43\]

墨尔本港以港口吞吐量和面積計算都是澳洲的第一大港口。

## 城市景觀

[Xmas_melbourne.jpg](https://zh.wikipedia.org/wiki/File:Xmas_melbourne.jpg "fig:Xmas_melbourne.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Garden_City_Melbourne.jpg "fig:缩略图")
[墨尔本市中心的外圍有環繞市區的免費](../Page/墨爾本中央商業區.md "wikilink")[旅遊觀光](../Page/旅遊.md "wikilink")[電車](../Page/電車.md "wikilink")，沿途經過著名觀光景點，包括：

  - [南岸](../Page/南岸_\(維多利亞州\).md "wikilink")
  - [国会大厦](../Page/国会大厦_\(墨尔本\).md "wikilink")
  - [維多利亞藝術中心](../Page/維多利亞藝術中心.md "wikilink")
  - [維多利亞市場](../Page/維多利亞市場.md "wikilink")
  - [墨尔本皇家植物园](../Page/墨尔本皇家植物园.md "wikilink")（Royal Botanical Gardens）
  - [墨尔本會議中心](../Page/墨爾本會議中心.md "wikilink")
  - [皇家展覽館](../Page/皇家展覽館.md "wikilink")
  - [IMAX電影院](../Page/IMAX.md "wikilink")
  - [墨尔本舊監獄](../Page/墨爾本舊監獄.md "wikilink")（Melbourne Old Gaol）
  - [聯邦廣場](../Page/聯邦廣場.md "wikilink")
  - [墨尔本動物園](../Page/墨爾本動物園.md "wikilink")

## 友好城市

墨尔本市共有6个[友好城市](../Page/友好城市.md "wikilink")\[44\]：

  - [大阪](../Page/大阪.md "wikilink")

  - [塞萨洛尼基](../Page/塞萨洛尼基.md "wikilink")

  - [波士顿](../Page/波士顿.md "wikilink")

  - [天津](../Page/天津.md "wikilink")

  - [圣彼得堡](../Page/圣彼得堡.md "wikilink")

  - [米兰](../Page/米兰.md "wikilink")

## 注释

## 参考文獻

## 外部链接

  - [維多利亞州官方旅遊導覽網站](http://www.visitvictoria.com)

  - [墨尔本生活網站](http://www.thatsmelbourne.com.au)

  - [墨尔本水族館網站](http://www.melbourneaquarium.com.au)

  - [墨尔本航空站官網](http://www.melbourne-airport.com.au)

  - [墨尔本交通網站](http://www.metlinkmelbourne.com.au)

  -
## 参见

  - [澳洲](../Page/澳洲.md "wikilink")
  - [維多利亞州](../Page/維多利亞州.md "wikilink")
  - [悉尼大道](../Page/悉尼大道.md "wikilink")

{{-}}

[墨爾本](../Category/墨爾本.md "wikilink")
[M](../Category/維多利亞州城市.md "wikilink")
[Category:澳大利亚沿海城市](../Category/澳大利亚沿海城市.md "wikilink")
[Category:夏季奥林匹克运动会主办城市](../Category/夏季奥林匹克运动会主办城市.md "wikilink")
[Category:19世紀建立的聚居地](../Category/19世紀建立的聚居地.md "wikilink")
[Category:维多利亚州各区域](../Category/维多利亚州各区域.md "wikilink")

1.  [典藏臺灣：駐美爾鉢領事館電外交部有關澳外長將繼續尋求擱置准許中共進入聯合國事](http://catalog.digitalarchives.tw/item/00/47/b5/d9.html)

2.

3.

4.

5.
6.

7.  [3218.0 - Regional Population Growth,
    Australia, 2011](http://www.abs.gov.au/ausstats/abs@.nsf/Latestproducts/3218.0Main%20Features62011?opendocument&tabname=Summary&prodno=3218.0&issue=2011&num=&view=)


8.  [ESTIMATED RESIDENT POPULATION, States and Territories - Capital
    City and Balance of
    State/Territory](http://www.abs.gov.au/ausstats/abs@.nsf/Products/3218.0~2010-11~Main+Features~Main+Features?OpenDocument#PARALINK1)

9.

10.

11.

12.

13. [澳洲》墨爾本古典天空下
    塗鴉自由](http://travel.udn.com/mag/travel/storypage.jsp?f_MAIN_ID=120&f_SUB_ID=618&f_ART_ID=23830)

14.

15.

16.

17.

18.

19. [The Economist:Liveability
    ranking 2011](http://www.economist.com/blogs/gulliver/2011/08/liveability-ranking)

20.

21.

22.

23.
24.

25. [墨爾本飆46.4℃高溫
    打破150年紀錄](http://udn.com/NEWS/WORLD/BREAKINGNEWS5/4723458.shtml)

26. [Melbourne jobless rate shoots
    up](http://www.theage.com.au/opinion/political-news/melbourne-jobless-rate-shoots-up-20120419-1xa5x.html)

27.

28. [【自由城市．墨爾本】「低端」窮人　都能享受生活](https://bkb.mpweekly.com/cu0001/20171219-61198)

29.

30. [美媒评全球最佳中国城
    墨市唐人街上榜](http://www.yeeyi.com/bbs/portal.php?mod=view&aid=34420)

31.

32. 《世界十大浪漫城市之南半球艺术之都：墨爾本》 知更出版社 2009年

33.

34.

35. [Melbourne – ringing in the
    changes](http://www.railexpress.com.au/archive/2009/november/november-11-09/melbourne-2013-ringing-in-the-changes/)


36. [Metro Trains Melbourne
    (MTM)](http://www.johnholland.com.au/Documents.asp?ID=13980&ProjectID=27&ProjCont=Details)


37.

38.

39.

40.

41.
42.

43.

44. [墨爾本市政府官方網站](http://www.melbourne.vic.gov.au/info.cfm?top=161&pg=2979)