**墨累河**（[英語](../Page/英語.md "wikilink")：****）是[澳大利亞最長的](../Page/澳大利亞.md "wikilink")[河流](../Page/河流.md "wikilink")，長度為2,995[公里](../Page/公里.md "wikilink")（1,861英里）\[1\]，發源於澳大利亞最高的[山脈](../Page/山脈.md "wikilink")[澳大利亞阿爾卑斯山脈](../Page/澳大利亞阿爾卑斯山脈.md "wikilink")（Australian
Alps，[大分水嶺的一部分](../Page/大分水嶺.md "wikilink")）。墨累河往西北方流，構成[維多利亞州與](../Page/維多利亞州.md "wikilink")[新南威爾斯州的邊界](../Page/新南威爾斯州.md "wikilink")\[2\]。墨累河在最後500公里往南移動，進入[南澳大利亞州](../Page/南澳大利亞州.md "wikilink")，經由[亚历山德里娜湖](../Page/亚历山德里娜湖.md "wikilink")（Lake
Alexandrina），於[阿德萊德附近的](../Page/阿德萊德.md "wikilink")[古爾瓦](../Page/古爾瓦.md "wikilink")（Goolwa）入海\[3\]\[4\]（[印度洋](../Page/印度洋.md "wikilink")）。

根據2010年的資料顯示，墨累河流域接收當地降水量的58%\[5\]。墨累河也許是澳大利亞最重要的灌溉區域，也是糧食的主要供應區。

## 地理

墨累河是長達3,750公里（2,330英里）的[墨累-達令流域的一部份](../Page/墨累-達令流域.md "wikilink")，範圍涵蓋[昆士蘭州的南部](../Page/昆士蘭州.md "wikilink")、[維多利亞州與](../Page/維多利亞州.md "wikilink")[新南威爾斯州](../Page/新南威爾斯州.md "wikilink")，含蓋澳大利亞七分之一陸地面積。墨累河的流量變化很大，在枯水期間甚至曾經完全乾涸，雖然這種情況相當罕見，歷史上的官方紀錄只有過2或3次。墨累河沿岸也有許多英國人所建立的[城市](../Page/城市.md "wikilink")，包括[沃東加](../Page/沃東加_\(維多利亞州\).md "wikilink")、[奥尔伯里](../Page/奥尔伯里.md "wikilink")、[伊丘卡](../Page/伊丘卡.md "wikilink")（Echuca）、[米爾杜拉](../Page/米爾杜拉_\(維多利亞州\).md "wikilink")、[墨累橋等](../Page/墨累橋.md "wikilink")。

## 生態

因為墨累河的流量變化很大，所以生態系統也變化多端。棲息於墨累河中的[魚類種類繁多](../Page/魚類.md "wikilink")，包括[墨累河鱈](../Page/墨累河鱈.md "wikilink")（Murray
cod）、[黃金鱸](../Page/黃金鱸.md "wikilink")（Golden
perch）、[銀鱸](../Page/銀鱸.md "wikilink")（Silver
Perch）、[鱒鱈](../Page/鱒鱈.md "wikilink")（trout
cod）、[澳洲麥氏鱸](../Page/澳洲麥氏鱸.md "wikilink")（Macquarie
perch）、[澳洲鰻鯰](../Page/澳洲鰻鯰.md "wikilink")（eel tailed
catfish）等。除了魚類以外，[墨累河螯蝦](../Page/墨累河螯蝦.md "wikilink")（Murray
River
Crayfish）、長頸[龜](../Page/龜.md "wikilink")、水[鼠與](../Page/鼠.md "wikilink")[鴨嘴獸也棲息在墨累河流域](../Page/鴨嘴獸.md "wikilink")。墨累河流域也是[赤桉](../Page/赤桉.md "wikilink")（river
red gum）主要的生長區域。

自從[歐洲人移居](../Page/歐洲.md "wikilink")[澳大利亞後](../Page/澳大利亞.md "wikilink")，墨累河的生態逐漸遭到破壞，許多獨特的生物數量逐漸減少，甚至消失。2000年至2007年間所發生的大乾旱也對[赤桉的生長造成巨大的壓力](../Page/赤桉.md "wikilink")。墨累河有時也會發生大洪水，例如1956年的[水災就淹沒許多城鎮](../Page/水災.md "wikilink")，並持續6個月之久。

許多外來魚類也對當地的原生魚類造成莫大的威脅，例如[鯉魚](../Page/鯉魚.md "wikilink")、[柳條魚](../Page/柳條魚.md "wikilink")（Gambusia）、[河鱸](../Page/河鱸.md "wikilink")、[虹鱒與](../Page/虹鱒.md "wikilink")[褐鱒](../Page/褐鱒.md "wikilink")（brown
trout）。其中[鯉魚被引進澳大利亞後](../Page/鯉魚.md "wikilink")，嚴重引響墨累河與支流的生態，鯉魚在部分地區甚至已成為唯一一種魚類。

## 神話

墨瑞河這條澳大利亞最長的河流對於[澳洲原住民來說有許多重要文化影響](../Page/澳洲原住民.md "wikilink")。對[亞利珊卓拉湖](../Page/亞利珊卓拉湖.md "wikilink")（Lake
Alexandrina）附近的原住民來說，墨瑞河是他們偉大的祖先Ngurunderi追趕墨累河鱈（Murray
cod）時所留下的路徑痕跡。Ngurunderi從新南威爾斯州搭乘[桉樹製成的木筏開始追趕墨累河鱈](../Page/桉樹.md "wikilink")，但是墨累河鱈屢次逃過他的攻擊，Ngurunderi最後來到亞利珊卓拉湖畔，並在此居住。

## 探險

[Night_travel_on_the_Murray_c1880.jpg](https://zh.wikipedia.org/wiki/File:Night_travel_on_the_Murray_c1880.jpg "fig:Night_travel_on_the_Murray_c1880.jpg")

[探險家](../Page/探險家.md "wikilink")[漢密爾頓·休姆](../Page/漢密爾頓·休姆.md "wikilink")（Hamilton
Hume）與[威廉姆·霍維爾](../Page/威廉姆·霍維爾.md "wikilink")（William
Hovell）於1824年首次進入墨累河流域，殖民地阿爾伯里也在同一年成立。休姆以父親的名字，將墨累河稱為休姆河。[查理斯·斯德特](../Page/查理斯·斯德特.md "wikilink")（Charles
Sturt）在1830年抵達墨累河流域，對[馬蘭比吉河](../Page/馬蘭比吉河.md "wikilink")（Murrumbidgee
River）進行探險。斯德特將這條河命名為墨累河，以紀念[英國戰爭及殖民地大臣](../Page/英國戰爭及殖民地大臣.md "wikilink")（Secretary
of State for War and the Colonies）[墨累爵士](../Page/美利爵士.md "wikilink")。

斯德特繼續沿著墨累河下游探險，最終抵達[亞利珊卓拉湖](../Page/亞利珊卓拉湖.md "wikilink")（Lake
Alexandrina）與[墨累河口](../Page/墨累河口.md "wikilink")。[法蘭西斯·卡德爾](../Page/法蘭西斯·卡德爾.md "wikilink")（Francis
Cadell）於1852年從[斯旺希島](../Page/斯旺希島.md "wikilink")（Swan
Hill）駕船沿著墨累河航行1,300公里\[6\]。

## 河運

因為墨累河沒有[河口灣](../Page/河口灣.md "wikilink")，所以船隻無法從[海上進入內陸](../Page/海.md "wikilink")。然而歐洲人在19世紀時使用[蒸汽船在墨累河進行](../Page/蒸汽船.md "wikilink")[商業活動](../Page/商業.md "wikilink")，始於1853年。[法蘭西斯·卡德爾](../Page/法蘭西斯·卡德爾.md "wikilink")（Francis
Cadell）從[斯旺希島](../Page/斯旺希島.md "wikilink")（Swan
Hill）駕船沿著墨累河航行，而另一位船長William
Randell則駕駛瑪莉安號蒸汽船抵達[莫馬](../Page/莫馬.md "wikilink")\[7\]。一艘載運[黃金的蒸汽船在](../Page/黃金.md "wikilink")1855年抵達阿爾伯里，雖然大部分的船隻會在[伊丘卡](../Page/伊丘卡.md "wikilink")（Echuca）更換成較小的船隻再抵達阿爾伯里.\[8\]。

[Confluence_of_Murray_&_Darling_Rivers,_Wentworth,_NSW,_9.7.2007.jpg](https://zh.wikipedia.org/wiki/File:Confluence_of_Murray_&_Darling_Rivers,_Wentworth,_NSW,_9.7.2007.jpg "fig:Confluence_of_Murray_&_Darling_Rivers,_Wentworth,_NSW,_9.7.2007.jpg")於新南威爾斯州[交匯](../Page/交匯.md "wikilink")\]\]

## 參考資料

<references/>

  - Isaacs J(1980)*Australian Dreaming: 40,000 Years of Aboriginal
    History*, Lansdowne Press, Sydney, New South Wales, ISBN
    978-0-7018-1330-7

  -
  - Jennings, J.T. (Ed.) (2009) Natural History of the Riverland and
    Murraylands. (Royal Society of South Australia Inc.), ISBN
    978-0-9596627-9-5

## 外部連結

  - [The Murray Darling Crisis - ABC TV
    Catalyst](http://www.abc.net.au/catalyst/murraydarling/)墨累河及大令河危機
  - [Murray-Darling Basin Commission: The River Murray and Lower
    Darling](https://web.archive.org/web/20121003023031/http://www.mdbc.gov.au/river_murray/river_murray.htm)
  - [River pilot maps 1880-1918](http://nla.gov.au/nla.map-erpc1)
  - [Down the River
    Murray](https://web.archive.org/web/20040930053544/http://www.abc.net.au/rn/talks/lnl/index/murray.htm)

[M](../Category/澳大利亚河流.md "wikilink")

1.

2.

3.
4.

5.  [Guide to the Proposed Basin Plan, Murray Darling Basin
    Authority 2010](http://thebasinplan.mdba.gov.au/)

6.

7.

8.  **Railways and Riverboats** Rowland, E.C. Australian Railway History
    January, 1976 pp1-16