**墨西哥州**（）是[墨西哥](../Page/墨西哥.md "wikilink")31個[州之一](../Page/墨西哥行政區劃.md "wikilink")，位於該國中部，從北、東、西三個方向包圍聯邦區，即首都[墨西哥城](../Page/墨西哥城.md "wikilink")。

該州長期是[革命制度黨的大本營](../Page/革命制度黨.md "wikilink")。

## 參考文獻

[Category:墨西哥行政區劃](../Category/墨西哥行政區劃.md "wikilink")
[Category:1823年建立的一級行政區](../Category/1823年建立的一級行政區.md "wikilink")