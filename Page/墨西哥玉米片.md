[Mission_Tortilla_Triangles.JPG](https://zh.wikipedia.org/wiki/File:Mission_Tortilla_Triangles.JPG "fig:Mission_Tortilla_Triangles.JPG")

**墨西哥玉米片**、**玉米餅**、或**墨西哥脆餅**（tortilla
chips）是由[墨西哥薄餅](../Page/墨西哥薄餅.md "wikilink")（tortilla）翻製的[零食](../Page/零食.md "wikilink")。一般是將整塊由[玉米](../Page/玉米.md "wikilink")、[蔬菜油](../Page/蔬菜油.md "wikilink")、糖、[鹽和](../Page/鹽.md "wikilink")[水做成的墨西哥薄餅切成楔形小片](../Page/水.md "wikilink")、或直接從揉製階段的玉米麵團[馬薩壓切](../Page/馬薩_\(食品\).md "wikilink")，再經[油炸或](../Page/油炸.md "wikilink")[烘烤後而得](../Page/烘烤.md "wikilink")。雖然玉米餅是在1940年代末期於[洛杉磯首先被大量生產](../Page/洛杉磯.md "wikilink")\[1\]，但還是普遍被認為是[墨西哥食物](../Page/墨西哥.md "wikilink")。玉米餅的原料通常是[黃玉米](../Page/黃玉米.md "wikilink")，但也可以是[白玉米](../Page/白玉米.md "wikilink")、[藍玉米或是](../Page/藍玉米.md "wikilink")[紅玉米](../Page/紅玉米.md "wikilink")。

## 起源

這種[三角型狀的墨西哥脆餅或玉米片開始普及](../Page/三角型.md "wikilink")，是始自企業家[瑞貝卡·韋伯·卡蘭薩](../Page/瑞貝卡·韋伯·卡蘭薩.md "wikilink")（Rebecca
Webb
Carranza）和她的丈夫在[洛杉磯西南區所創設的墨西哥熱食和玉米片加工廠](../Page/洛杉磯.md "wikilink")。他們將玉米片生產機器製造出來的畸形玉米片切成三角形並且油炸，她發現這玉米片還滿受歡迎的，便開始在El
Zarape玉米片工廠販賣一袋1角的玉米片，自此玉米片就被卡蘭薩所推廣\[2\]。1994年，卡蘭薩被頒穫黃金玉米餅獎以表彰她對墨西哥食品工業的貢獻。2006年1月19日，她逝世於[鳳凰城](../Page/鳳凰城.md "wikilink")，享壽98歲。

## 食用方式

[BBQ_Nachos_015.jpg](https://zh.wikipedia.org/wiki/File:BBQ_Nachos_015.jpg "fig:BBQ_Nachos_015.jpg")
墨西哥脆餅在美國和其他地方墨西哥餐廳常常是經典的[開胃菜](../Page/開胃菜.md "wikilink")。1970年代末期，玉米片在加州以外地區的[人氣持續穩定的上升](../Page/人氣.md "wikilink")，於是開始用沾醬玉米片競爭。玉米片通常會附帶一碟[莎莎醬](../Page/莎莎醬.md "wikilink")、[辣椒乳酪醬](../Page/辣椒乳酪醬.md "wikilink")（chili
con
queso）或是[酪梨醬](../Page/鳄梨醬.md "wikilink")，而沒附帶沾醬的玉米片通常都已經用香料調味過。雖然現在全世界都看得到墨西哥玉米片了，但是[美國還是其中的主要市場](../Page/美國.md "wikilink")，例如一些品牌像是[多力多滋](../Page/多力多滋.md "wikilink")（Doritos）和[托斯蒂多滋](../Page/托斯蒂多滋.md "wikilink")（Tostitos）。

[烤乾酪辣味玉米片](../Page/烤乾酪辣味玉米片.md "wikilink")（）是一道較費工的玉米片料理。在玉米片上會灑上融化或是切絲的[乳酪](../Page/乳酪.md "wikilink")，或者還有其他的添加和替代物，例如肉、[薩爾薩辣醬](../Page/薩爾薩辣醬.md "wikilink")（例如）、[煎豆泥](../Page/煎豆泥.md "wikilink")、[鱷梨醬](../Page/鱷梨醬.md "wikilink")、[酸奶酪](../Page/酸奶酪.md "wikilink")、[洋蔥丁](../Page/洋蔥.md "wikilink")、[橄欖和醃製的](../Page/橄欖.md "wikilink")[墨西哥哈拉貝紐辣椒](../Page/墨西哥哈拉貝紐辣椒.md "wikilink")。更費工的烤乾酪辣味玉米片通常還會烤過讓上面的乳酪融化。[伊格納西歐·納寇·阿納亞](../Page/伊格納西歐·納寇·阿納亞.md "wikilink")（Ignacio
"Nacho" Anaya）約在1943年第一次創造出了這道料理，之後就成了玉米片料理的象徵\[3\]。

## 類似零食

[Corn_chips.JPG](https://zh.wikipedia.org/wiki/File:Corn_chips.JPG "fig:Corn_chips.JPG")
另一種類似的油炸玉米零食，稱作[玉米脆片](../Page/玉米脆片.md "wikilink")（），但不是由[墨西哥薄餅作成的](../Page/墨西哥薄餅.md "wikilink")。通常是由[玉米麵粉做成其他特定的形狀](../Page/玉米麵粉.md "wikilink")，例如圓錐或勺狀。美國品牌[菲力多滋](../Page/菲力多滋.md "wikilink")（Fritos）就是其中的代表作\[4\]。

和玉米片最大的差別，在於玉米片使用的玉米在製作過程中有一道叫做
（糊化程序）的工法，以添加[生石灰處理未加工的玉米](../Page/生石灰.md "wikilink")。

但是在[澳洲和](../Page/澳洲.md "wikilink")[大洋洲地區](../Page/大洋洲.md "wikilink")，玉米脆片和玉米片指的都是corn
chips。其他和玉米脆片還有玉米片競爭的主要零食是[馬鈴薯作成的](../Page/馬鈴薯.md "wikilink")[洋芋片](../Page/洋芋片.md "wikilink")

## 其他

相關油炸或烘烤的[墨西哥薄餅或](../Page/墨西哥薄餅.md "wikilink")[馬薩食品](../Page/馬薩_\(食品\).md "wikilink")：

  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參考資料

<references/>

## 外部連結

  - [Snack food review
    site](http://www.taquitos.net/snacks.php?category_code=54)

  -
[Category:小吃](../Category/小吃.md "wikilink")
[Category:零食](../Category/零食.md "wikilink")
[Category:美國食品](../Category/美國食品.md "wikilink")
[Category:墨西哥食品](../Category/墨西哥食品.md "wikilink")
[Category:速食](../Category/速食.md "wikilink")

1.

2.
3.

4.