[Messina_dot.png](https://zh.wikipedia.org/wiki/File:Messina_dot.png "fig:Messina_dot.png")
[Messina_Dome.jpg](https://zh.wikipedia.org/wiki/File:Messina_Dome.jpg "fig:Messina_Dome.jpg")
**墨西拿**（）是[意大利](../Page/意大利.md "wikilink")[西西里岛上第三大的城市](../Page/西西里岛.md "wikilink")，也是[墨西拿省的首府](../Page/墨西拿省.md "wikilink")。墨西拿在西西里岛的东北角，正對[墨西拿海峡](../Page/墨西拿海峡.md "wikilink")。

墨西拿为古代來自希腊[麥西尼亞的殖民者于公元前](../Page/麥西尼亞.md "wikilink")8世记时為了逃避當時[征服麥西尼亞的斯巴達人統治而建立](../Page/麥西尼亞戰爭.md "wikilink")，古称**梅萨纳**（Messana），又译**梅西那**或**麦散那**，距今有二千八百多年历史。1908年12月28日的[大地震和海嘯將墨西拿夷為平地](../Page/1908年墨西拿地震.md "wikilink")。二战时又遭炸毁。但经半个世纪的重建，墨西拿现在人口已达二十七万。

墨西拿教堂建于十二世纪，有钟楼和古天文钟。

## 交通

墨西拿有渡轮来往西西里岛和[雷焦卡拉布里亚](../Page/雷焦卡拉布里亚.md "wikilink")，也有良好的深水港，可供地中海邮轮停泊。墨西拿有公路通往[塔奥敏纳](../Page/塔奥敏纳.md "wikilink")，[埃特纳火山等旅游胜地](../Page/埃特纳火山.md "wikilink")。

## 參考文獻

[Category:地中海沿海城市](../Category/地中海沿海城市.md "wikilink")
[M](../Category/墨西拿省市镇.md "wikilink")