**壬申**为[干支之一](../Page/干支.md "wikilink")，顺序为第9个。前一位是[辛未](../Page/辛未.md "wikilink")，后一位是[癸酉](../Page/癸酉.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之壬屬陽之水](../Page/天干.md "wikilink")，[地支之申屬陽之金](../Page/地支.md "wikilink")，是金生水相生，名偵探柯南有一集「猴子與熊手的追捕令」中，有提到壬申又叫「水江」。

## 壬申年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")9年称“**壬申年**”。以下各个[公元年份](../Page/公元.md "wikilink")，年份數除以60餘12，或年份數減3，除以10的餘數是9，除以12的餘數是9，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“壬申年”：

<table>
<caption><strong>壬申年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/12年.md" title="wikilink">12年</a></li>
<li><a href="../Page/72年.md" title="wikilink">72年</a></li>
<li><a href="../Page/132年.md" title="wikilink">132年</a></li>
<li><a href="../Page/192年.md" title="wikilink">192年</a></li>
<li><a href="../Page/252年.md" title="wikilink">252年</a></li>
<li><a href="../Page/312年.md" title="wikilink">312年</a></li>
<li><a href="../Page/372年.md" title="wikilink">372年</a></li>
<li><a href="../Page/432年.md" title="wikilink">432年</a></li>
<li><a href="../Page/492年.md" title="wikilink">492年</a></li>
<li><a href="../Page/552年.md" title="wikilink">552年</a></li>
<li><a href="../Page/612年.md" title="wikilink">612年</a></li>
<li><a href="../Page/672年.md" title="wikilink">672年</a></li>
<li><a href="../Page/732年.md" title="wikilink">732年</a></li>
<li><a href="../Page/792年.md" title="wikilink">792年</a></li>
<li><a href="../Page/852年.md" title="wikilink">852年</a></li>
<li><a href="../Page/912年.md" title="wikilink">912年</a></li>
<li><a href="../Page/972年.md" title="wikilink">972年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1032年.md" title="wikilink">1032年</a></li>
<li><a href="../Page/1092年.md" title="wikilink">1092年</a></li>
<li><a href="../Page/1152年.md" title="wikilink">1152年</a></li>
<li><a href="../Page/1212年.md" title="wikilink">1212年</a></li>
<li><a href="../Page/1272年.md" title="wikilink">1272年</a></li>
<li><a href="../Page/1332年.md" title="wikilink">1332年</a></li>
<li><a href="../Page/1392年.md" title="wikilink">1392年</a></li>
<li><a href="../Page/1452年.md" title="wikilink">1452年</a></li>
<li><a href="../Page/1512年.md" title="wikilink">1512年</a></li>
<li><a href="../Page/1572年.md" title="wikilink">1572年</a></li>
<li><a href="../Page/1632年.md" title="wikilink">1632年</a></li>
<li><a href="../Page/1692年.md" title="wikilink">1692年</a></li>
<li><a href="../Page/1752年.md" title="wikilink">1752年</a></li>
<li><a href="../Page/1812年.md" title="wikilink">1812年</a></li>
<li><a href="../Page/1872年.md" title="wikilink">1872年</a></li>
<li><a href="../Page/1932年.md" title="wikilink">1932年</a></li>
<li><a href="../Page/1992年.md" title="wikilink">1992年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2052年.md" title="wikilink">2052年</a></li>
<li><a href="../Page/2112年.md" title="wikilink">2112年</a></li>
<li><a href="../Page/2172年.md" title="wikilink">2172年</a></li>
<li><a href="../Page/2232年.md" title="wikilink">2232年</a></li>
<li><a href="../Page/2292年.md" title="wikilink">2292年</a></li>
<li><a href="../Page/2352年.md" title="wikilink">2352年</a></li>
<li><a href="../Page/2412年.md" title="wikilink">2412年</a></li>
<li><a href="../Page/2472年.md" title="wikilink">2472年</a></li>
<li><a href="../Page/2532年.md" title="wikilink">2532年</a></li>
<li><a href="../Page/2592年.md" title="wikilink">2592年</a></li>
<li><a href="../Page/2652年.md" title="wikilink">2652年</a></li>
<li><a href="../Page/2712年.md" title="wikilink">2712年</a></li>
<li><a href="../Page/2772年.md" title="wikilink">2772年</a></li>
<li><a href="../Page/2832年.md" title="wikilink">2832年</a></li>
<li><a href="../Page/2892年.md" title="wikilink">2892年</a></li>
<li><a href="../Page/2952年.md" title="wikilink">2952年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 672年 - [壬申之乱](../Page/壬申之乱.md "wikilink")（)），是日本的内亂。
  - 1872年 - （），是日本編製的戶籍。

## 壬申月

天干甲年和己年，[立秋到](../Page/立秋.md "wikilink")[白露的期間](../Page/白露.md "wikilink")，就是**壬申月**：

  - ……
  - [1974年](../Page/1974年.md "wikilink")8月立秋到9月白露
  - [1979年](../Page/1979年.md "wikilink")8月立秋到9月白露
  - [1984年](../Page/1984年.md "wikilink")8月立秋到9月白露
  - [1989年](../Page/1989年.md "wikilink")8月立秋到9月白露
  - [1994年](../Page/1994年.md "wikilink")8月立秋到9月白露
  - [1999年](../Page/1999年.md "wikilink")8月立秋到9月白露
  - [2004年](../Page/2004年.md "wikilink")8月立秋到9月白露
  - ……

## 壬申日

## 壬申、他時

天干甲日和己日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）15時到17時，就是**壬申時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/壬申年.md "wikilink")