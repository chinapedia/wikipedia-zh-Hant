《**壹號皇庭II**》（[英文](../Page/英語.md "wikilink")：），[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台時裝法律](../Page/翡翠台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，原定於[壹號皇庭第一集推出後的一周年播出](../Page/壹號皇庭.md "wikilink")，但因本劇為星期日劇集，加上1993年4月19日為星期一，故改由1993年4月18日首播，共15集，監製[鄧特希](../Page/鄧特希.md "wikilink")。逢星期日播出。\[1\]\[2\]

## 與上輯的矛盾

第一輯結尾時蘇永康飾的周志輝往英國進修，並與鄭秀文飾的方家琪發展感情。唯於第二輯卻變成方家琪在加拿大，周志輝留在香港，兩人以長途電話維繫，後分手，其中原因未有交代。因鄭秀文不再拍攝此劇，故剧中透露方家琪已結識新男友而分手。

## 各集主題

| 集數 | 單元主題 |
| -- | ---- |
| 1  | 失足   |
| 2  | 殺夫   |
| 3  | 蹂躪   |
| 4  | 童黨   |
| 5  | 疏忽   |
| 6  | 狎玩   |
| 7  | 虔誠   |
| 8  | 秘密   |
| 9  | 索償   |
| 10 | 吞贓   |
| 11 | 公僕   |
| 12 | 贖罪   |
| 13 | 公憤   |
| 14 | 伏擊   |
| 15 | 真相   |

## 演員表

### 主要演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></p></td>
<td><p>余在春</p></td>
<td><p>Ben<br />
大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳秀雯.md" title="wikilink">陳秀雯</a></p></td>
<td><p>丁　柔</p></td>
<td><p>Michelle<br />
高級检控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a></p></td>
<td><p>江承宇</p></td>
<td><p>Michael<br />
師爺<br />
江承宙之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇杏璇.md" title="wikilink">蘇杏璇</a></p></td>
<td><p>江李頌雲</p></td>
<td><p>Lora<br />
御用大律師<br />
江承宙, 江承宇之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉美娟.md" title="wikilink">劉美娟</a></p></td>
<td><p>江承宙</p></td>
<td><p>Helen<br />
大律師<br />
江承宇之妹</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/駱應鈞.md" title="wikilink">駱應鈞</a></p></td>
<td><p>周文彬</p></td>
<td><p>彬Sir<br />
警長</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a></p></td>
<td><p>周志輝</p></td>
<td><p>Raymond<br />
律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王靖雯.md" title="wikilink">王靖雯</a></p></td>
<td><p>唐毓文</p></td>
<td><p>Man／Mandy<br />
检控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林漪娸.md" title="wikilink">林漪娸</a></p></td>
<td><p>林學宜</p></td>
<td><p>Doris<br />
督察</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧梓峰.md" title="wikilink">鄧梓峰</a></p></td>
<td><p>李國柱</p></td>
<td><p>Henry<br />
<a href="../Page/大律師.md" title="wikilink">大律師</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔣志光.md" title="wikilink">蔣志光</a></p></td>
<td><p>周少聰</p></td>
<td><p>Eric<br />
大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳梅馨.md" title="wikilink">陳梅馨</a></p></td>
<td><p>關　靜</p></td>
<td><p>Jessica<br />
律師<br />
喜歡周志輝</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洪朝豐.md" title="wikilink">洪朝豐</a></p></td>
<td><p>張家賢</p></td>
<td><p>Jeff<br />
检控官</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷珊</p></td>
<td><p>Queenie<br />
夜總會公關<br />
同性戀者</p></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃一飛.md" title="wikilink">黃一飛</a></p></td>
<td><p>春　父</p></td>
<td><p>余在春父親</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁健平.md" title="wikilink">梁健平</a></p></td>
<td><p>Paul</p></td>
<td><p>檢控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>英　姐</p></td>
<td><p>江家傭人<br />
即第一輯<a href="../Page/梁愛.md" title="wikilink">梁愛的角色</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蕭玉燕.md" title="wikilink">蕭玉燕</a></p></td>
<td><p>王美玲</p></td>
<td><p>May</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>超</p></td>
<td><p>探員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李煌生.md" title="wikilink">李煌生</a></p></td>
<td><p>強</p></td>
<td><p>探員</p></td>
</tr>
<tr class="even">
<td><p>-{<a href="../Page/黃鳳琼.md" title="wikilink">黃鳳琼</a>}-</p></td>
<td><p>女探員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃成想.md" title="wikilink">黃成想</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳啟泰.md" title="wikilink">陳啟泰</a></p></td>
<td><p>Wilson</p></td>
<td><p>林學宜男友, 後分手</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王翠玉.md" title="wikilink">王翠玉</a></p></td>
<td><p>Wilson女友</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/唐葳娜.md" title="wikilink">唐葳娜</a></p></td>
<td><p>Wendy</p></td>
<td><p>律師樓職員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴少民.md" title="wikilink">戴少民</a></p></td>
<td><p>法庭職員甲<br />
／宇　友</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭君寧.md" title="wikilink">鄭君寧</a></p></td>
<td><p>法庭職員乙</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳燕航.md" title="wikilink">陳燕航</a></p></td>
<td><p>法庭職員丙</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葉振聲.md" title="wikilink">葉振聲</a></p></td>
<td><p>檢控助理甲<br />
／律師助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥嘉倫.md" title="wikilink">麥嘉倫</a></p></td>
<td><p>法庭職員丁</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王維德.md" title="wikilink">王維德</a></p></td>
<td><p>檢控助理乙<br />
／律師助手</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 案件內容

#### 一、失足

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [郭政鴻](../Page/郭政鴻.md "wikilink") | 馮志權    | 被告        |
| [陳　蕊](../Page/陳蕊.md "wikilink")  | 阮莉莉    | 死者        |
| [侯蔚雲](../Page/侯蔚雲.md "wikilink") | 阮　娜    | 阮莉莉姊      |

|          |        |
| -------- | ------ |
| **被告**   | 馮志權    |
| **罪名**   | 謀殺     |
| **被害人**  | 阮莉莉    |
| **檢控官**  | 張家賢    |
| **辯方律師** | 余在春    |
| **控方證人** |        |
| **辯方證人** |        |
| **結果**   | 謀殺罪名成立 |

#### 二、殺夫

|                                         |        |           |
| --------------------------------------- | ------ | --------- |
| **演員**                                  | **角色** | **暱稱/身份** |
| [何嘉麗](../Page/何嘉麗_\(演員\).md "wikilink") | 朱美英    | 被告        |
| [莫燕嫦](../Page/莫燕嫦.md "wikilink")        | 儀      |           |
| [唐葳娜](../Page/唐葳娜.md "wikilink")        | Wendy  |           |
| [吳列華](../Page/吳列華.md "wikilink")        | 舞女甲    |           |

|          |                |
| -------- | -------------- |
| **被告**   | 朱美英            |
| **罪名**   |                |
| **被害人**  |                |
| **檢控官**  | 唐毓文            |
| **辯方律師** | 江承宙            |
| **控方證人** |                |
| **辯方證人** |                |
| **結果**   | 謀殺罪名不成立 誤殺罪名成立 |

#### 三、蹂躪

|                                         |        |             |
| --------------------------------------- | ------ | ----------- |
| **演員**                                  | **角色** | **暱稱/身份**   |
| [許實賢](../Page/許實賢.md "wikilink")        | 趙天雄    | 第一次及第二次審訊被告 |
| [陳潔儀](../Page/陳潔儀_\(香港\).md "wikilink") | 芬      | 首次審訊案件受害人   |
| [林漪娸](../Page/林漪娸.md "wikilink")        | 林學宜    | 第二次審訊案件受害人  |
| [曹　濟](../Page/曹濟.md "wikilink")         | 芬　父    |             |
| [黎秀英](../Page/黎秀英.md "wikilink")        | 芬　母    |             |
| [李衛民](../Page/李衛民.md "wikilink")        | 教練森    |             |
| [凌　漢](../Page/凌漢.md "wikilink")         | 看　更    |             |
| [譚泉慶](../Page/譚泉慶.md "wikilink")        | 宜上司    |             |
| [譚一清](../Page/譚一清.md "wikilink")        | 法　官    |             |
| [林尚武](../Page/林尚武.md "wikilink")        | 辯護律師   | 第二次審訊辯方律師   |
|                                         |        |             |

|           |
| :-------: |
| **第一次審訊** |
|  **被告**   |
|  **罪名**   |
|  **被害人**  |
|  **檢控官**  |
| **辯方律師**  |
| **控方證人**  |
| **辯方證人**  |
|  **結果**   |

|           |
| :-------: |
| **第二次審訊** |
|  **被告**   |
|  **罪名**   |
|  **被害人**  |
|  **檢控官**  |
| **辯方律師**  |
| **控方證人**  |
| **辯方證人**  |
|  **結果**   |

#### 四、童黨

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/施培豐.md" title="wikilink">施培豐</a></p></td>
<td><p>辣　雞</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韋先倫.md" title="wikilink">韋先倫</a></p></td>
<td><p>大　傻</p></td>
<td><p>被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尹志雄.md" title="wikilink">尹志雄</a></p></td>
<td><p>童黨甲</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡子傑.md" title="wikilink">蔡子傑</a></p></td>
<td><p>童黨乙</p></td>
<td><p>被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥皓為.md" title="wikilink">麥皓為</a></p></td>
<td><p>德　叔</p></td>
<td><p>陳孝珠父親<br />
死者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文潔雲.md" title="wikilink">文潔雲</a></p></td>
<td><p>德　嬸</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃君儀.md" title="wikilink">黃君儀</a></p></td>
<td><p>陳孝珠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/呂劍光.md" title="wikilink">呂劍光</a></p></td>
<td><p>大傻父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳惠瑜.md" title="wikilink">陳惠瑜</a></p></td>
<td><p>大傻母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孫季卿.md" title="wikilink">孫季卿</a></p></td>
<td><p>卿　叔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳曉雲.md" title="wikilink">陳曉雲</a></p></td>
<td><p>舞小姐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊瑞麟.md" title="wikilink">楊瑞麟</a></p></td>
<td><p>辯護律師</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/譚一清.md" title="wikilink">譚一清</a></p></td>
<td><p>民事法官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丘國良.md" title="wikilink">丘國良</a></p></td>
<td><p>原訴律師</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王偉樑.md" title="wikilink">王偉樑</a></p></td>
<td><p>化驗師</p></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><strong>被告</strong></p></td>
<td><p>大傻</p></td>
<td><p>辣雞<br />
童黨甲<br />
童黨乙</p></td>
</tr>
<tr class="even">
<td><p><strong>罪名</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>被害人</strong></p></td>
<td><p>德叔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>檢控官</strong></p></td>
<td><p>張家賢</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>辯方律師</strong></p></td>
<td><p>余在春</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>控方證人</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>辯方證人</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>結果</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 五、疏忽

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾威.md" title="wikilink">艾　威</a></p></td>
<td><p>林國權</p></td>
<td><p>醫生<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江寧_(演員).md" title="wikilink">江　寧</a></p></td>
<td><p>表姨丈</p></td>
<td><p>全名阮志華<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td><p>表　姨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林家棟.md" title="wikilink">林家棟</a></p></td>
<td><p>Alex</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔡國權.md" title="wikilink">蔡國權</a></p></td>
<td><p>梁醫生</p></td>
<td><p>控方專家證人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李桂英.md" title="wikilink">李桂英</a></p></td>
<td><p>聰助手</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |
| :------: |
| **死因裁判** |
| **被害人**  |
|  **律師**  |
|  **證人**  |
|  **結果**  |

|          |      |
| -------- | ---- |
| **被告**   | 林國權  |
| **罪名**   |      |
| **被害人**  | 阮志華  |
| **檢控官**  | 江李頌雲 |
| **辯方律師** | 周少聰  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

#### 六、狎玩

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳均榕.md" title="wikilink">陳均榕</a></p></td>
<td><p>添</p></td>
<td><p>陸子敏父親</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王書麒.md" title="wikilink">王書麒</a></p></td>
<td><p>陳展明</p></td>
<td><p>教師<br />
被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鍾淑欣.md" title="wikilink">鍾淑欣</a></p></td>
<td><p>陸子敏</p></td>
<td><p>受害人</p></td>
</tr>
<tr class="odd">
<td><p>-{<a href="../Page/劉寶琼.md" title="wikilink">劉寶琼</a>}-</p></td>
<td><p>萍</p></td>
<td><p>受害人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬麗琪.md" title="wikilink">馬麗琪</a></p></td>
<td><p>玲</p></td>
<td><p>受害人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉凱儀.md" title="wikilink">劉凱儀</a></p></td>
<td><p>兒</p></td>
<td><p>受害人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/謝美思.md" title="wikilink">謝美思</a></p></td>
<td><p>欣</p></td>
<td><p>受害人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳中堅.md" title="wikilink">陳中堅</a></p></td>
<td><p>校　長</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亦羚.md" title="wikilink">亦　羚</a></p></td>
<td><p>舞小姐甲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾令茵.md" title="wikilink">曾令茵</a></p></td>
<td><p>舞小姐乙</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林文森.md" title="wikilink">林文森</a></p></td>
<td><p>陳太太</p></td>
<td><p>Ann<br />
陳展明太太</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李海生.md" title="wikilink">李海生</a></p></td>
<td><p>贊　叔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁舜燕.md" title="wikilink">梁舜燕</a></p></td>
<td><p>賢　母</p></td>
<td><p>張家賢母親</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何璧堅.md" title="wikilink">何璧堅</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><strong>被告</strong></p></td>
<td><p>陳展明</p></td>
</tr>
<tr class="even">
<td><p><strong>罪名</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>被害人</strong></p></td>
<td><p>陸子敏<br />
萍<br />
玲<br />
兒<br />
欣</p></td>
</tr>
<tr class="even">
<td><p><strong>檢控官</strong></p></td>
<td><p>丁柔</p></td>
</tr>
<tr class="odd">
<td><p><strong>辯方律師</strong></p></td>
<td><p>江承宙</p></td>
</tr>
<tr class="even">
<td><p><strong>控方證人</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>辯方證人</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>結果</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

#### 七、虔誠

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/焦雄.md" title="wikilink">焦　雄</a></p></td>
<td><p>何　勝</p></td>
<td><p>何乾生之父<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黎宣.md" title="wikilink">黎　宣</a></p></td>
<td><p>梁七妹</p></td>
<td><p>七姑<br />
被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔡欣樺.md" title="wikilink">蔡欣樺</a></p></td>
<td><p>葉玉娟</p></td>
<td><p>何乾生之母</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧汝超.md" title="wikilink">鄧汝超</a></p></td>
<td><p>的士司機</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉桂芳.md" title="wikilink">劉桂芳</a></p></td>
<td><p>女法官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊瑞麟.md" title="wikilink">楊瑞麟</a></p></td>
<td><p>辯護律師</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃煒琳.md" title="wikilink">黃煒琳</a></p></td>
<td><p>醫　生</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸麗燕.md" title="wikilink">陸麗燕</a></p></td>
<td><p>教　師</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |     |
| -------- | --- | --- |
| **被告**   | 何　勝 | 梁七妹 |
| **罪名**   |     |     |
| **被害人**  | 何乾生 |     |
| **檢控官**  | 唐毓文 |     |
| **辯方律師** |     |     |
| **控方證人** |     |     |
| **辯方證人** |     |     |
| **結果**   |     |     |

#### 八、秘密

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a></p></td>
<td><p>洪振東</p></td>
<td><p>Andy<br />
被告<br />
同性戀者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮曉文.md" title="wikilink">馮曉文</a></p></td>
<td><p>林小姐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴志偉.md" title="wikilink">戴志偉</a></p></td>
<td><p>麥國成</p></td>
<td><p>Francis</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/談佩珊.md" title="wikilink">談佩珊</a></p></td>
<td><p>麥　太</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳勉良.md" title="wikilink">陳勉良</a></p></td>
<td><p>看　更</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 洪振東 |
| **罪名**   |     |
| **被害人**  | 陳錦昇 |
| **檢控官**  | 張家賢 |
| **辯方律師** | 余在春 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 九、索偿

|                                        |        |           |
| -------------------------------------- | ------ | --------- |
| **演員**                                 | **角色** | **暱稱/身份** |
| [林　偉](../Page/林偉_\(演員\).md "wikilink") | 方世豪    | 被告        |
| [麥子雲](../Page/麥子雲.md "wikilink")       | 陳雄生    | 死者        |
| [黃振寧](../Page/黃振寧.md "wikilink")       | 標      |           |
|                                        |        |           |

|          |     |
| -------- | --- |
| **被告**   | 方世豪 |
| **罪名**   |     |
| **被害人**  | 陳雄生 |
| **檢控官**  | 丁柔  |
| **辯方律師** | 江承宙 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十、吞赃

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [駱應鈞](../Page/駱應鈞.md "wikilink") | 周文彬    | 被告        |
| [關　菁](../Page/關菁.md "wikilink")  | 周金勝    | 死者        |
| [鄭　雷](../Page/鄭雷.md "wikilink")  | 強      |           |
| [黃仲匡](../Page/黃仲匡.md "wikilink") | 崩牙九    |           |
| [博　君](../Page/博君.md "wikilink")  | 向　東    |           |
| [芳靄鈴](../Page/芳靄鈴.md "wikilink") | 盛　嫂    |           |

|          |     |
| -------- | --- |
| **被告**   | 周文彬 |
| **罪名**   |     |
| **被害人**  | 周金勝 |
| **檢控官**  | 唐毓文 |
| **辯方律師** | 周少聰 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十一、公僕

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洪朝豐.md" title="wikilink">洪朝豐</a></p></td>
<td><p>張家賢</p></td>
<td><p>被告<br />
涉觸犯防止賄賂條例入獄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李耀敬.md" title="wikilink">李耀敬</a></p></td>
<td><p>江耀明</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳曉雲.md" title="wikilink">陳曉雲</a></p></td>
<td><p>Rosa</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林嘉麗.md" title="wikilink">林嘉麗</a></p></td>
<td><p>Annie</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/方傑.md" title="wikilink">方　傑</a></p></td>
<td><p>蒲錦源</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/區人山.md" title="wikilink">區人山</a></p></td>
<td><p>江耀明手下</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁少狄.md" title="wikilink">梁少狄</a></p></td>
<td><p>殺　手</p></td>
<td><p>被江耀明收買殺害證人</p></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 張家賢 |
| **罪名**   |     |
| **被害人**  |     |
| **檢控官**  | 李國柱 |
| **辯方律師** | 余在春 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十二、赎罪

|                                        |          |           |
| -------------------------------------- | -------- | --------- |
| **演員**                                 | **角色**   | **暱稱/身份** |
| [彭鎮南](../Page/彭鎮南.md "wikilink")       | 馬更生      | 被告        |
| [林映輝](../Page/林映輝.md "wikilink")       | 馬淑珍      |           |
| [區　嶽](../Page/區嶽.md "wikilink")        | 馬偉忠      |           |
| [-{于}-　楓](../Page/于楓.md "wikilink")    | \-{杰}-　母 |           |
| [白　蘭](../Page/白蘭_\(演員\).md "wikilink") | 垃圾婆      |           |
|                                        |          |           |

|          |         |
| -------- | ------- |
| **被告**   | 馬更生     |
| **罪名**   |         |
| **被害人**  | 張偉-{杰}- |
| **檢控官**  | 唐毓文     |
| **辯方律師** | 江李頌雲    |
| **控方證人** |         |
| **辯方證人** |         |
| **結果**   |         |

#### 十三、公愤

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [黃文標](../Page/黃文標.md "wikilink") | 李志滔    | 被告        |
| [虞天偉](../Page/虞天偉.md "wikilink") | 方友邦    | 被告        |
|                                  |        |           |

|          |      |     |
| -------- | ---- | --- |
| **被告**   | 李志滔  | 方友邦 |
| **罪名**   |      |     |
| **被害人**  |      |     |
| **檢控官**  | Paul |     |
| **辯方律師** | 余在春  |     |
| **控方證人** |      |     |
| **辯方證人** |      |     |
| **結果**   |      |     |

#### 十四、伏击

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕭玉燕.md" title="wikilink">蕭玉燕</a></p></td>
<td><p>王美玲</p></td>
<td><p>May<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a></p></td>
<td><p>江承宇</p></td>
<td><p>Michael</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷珊</p></td>
<td><p>Queenie</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 王美玲 |
| **罪名**   |     |
| **被害人**  | 江承宇 |
| **檢控官**  | 唐毓文 |
| **辯方律師** |     |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十五、真相

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a></p></td>
<td><p>江承宇</p></td>
<td><p>Michael<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蕭玉燕.md" title="wikilink">蕭玉燕</a></p></td>
<td><p>王美玲</p></td>
<td><p>May<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷珊</p></td>
<td><p>Queenie<br />
與王美玲爭奪手槍時將她錯手殺死</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 江承宇  |
| **罪名**   |      |
| **被害人**  | 王美玲  |
| **檢控官**  | Paul |
| **辯方律師** | 江李頌雲 |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

## 主題音樂及插曲

  - 主題音樂：《[Your Latest Trick](../Page/Your_Latest_Trick.md "wikilink")》
      - SAXO-PHONE：[Phil Rombaoa](../Page/Phil_Rombaoa.md "wikilink")
      - 作曲：[Mark Knopfler](../Page/Mark_Knopfler.md "wikilink") /
        [羅雁武](../Page/羅雁武.md "wikilink")

<!-- end list -->

  - 插曲《執迷不悔》
      - 作曲：[袁惟仁](../Page/袁惟仁.md "wikilink")
      - 填詞：[陳少琪](../Page/陳少琪.md "wikilink")
      - 主唱：[王靖雯](../Page/王靖雯.md "wikilink")

<!-- end list -->

  - 插曲《割愛》
      - 作曲：[黃尚偉](../Page/黃尚偉.md "wikilink")
      - 填詞：[梁芷珊](../Page/梁芷珊.md "wikilink")
      - 主唱：[蘇永康](../Page/蘇永康.md "wikilink")

## 參考

## 外部連結

  - [無綫電視官方網頁 -
    壹號皇庭II](https://web.archive.org/web/20080611124143/http://tvcity.tvb.com/drama/just2/index.htm)
  - [《壹號皇庭II》 GOTV
    第1集重溫](https://web.archive.org/web/20140222163103/http://gotv.tvb.com/programme/102463/151902/)

## 電視節目的變遷

[Category:1993年無綫電視劇集](../Category/1993年無綫電視劇集.md "wikilink")
[Category:無綫電視劇集系列](../Category/無綫電視劇集系列.md "wikilink")
[Category:無綫電視1990年代背景劇集](../Category/無綫電視1990年代背景劇集.md "wikilink")

1.  [蘇永廉自爆當年拍《壹號皇庭II》
    王菲被碰手嚇到彈開](https://news.mingpao.com/ins/instantnews/web_tc/article/20170922/s00007/1506074827015)
2.  [通頂翻煲《皇庭II》
    蘇永康自嘲同王菲演美女與野獸](http://hd.stheadline.com/news/realtime/ent/1024352/)