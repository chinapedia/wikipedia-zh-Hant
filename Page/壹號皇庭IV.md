《**壹號皇庭IV**》（[英文](../Page/英語.md "wikilink")：），[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台時裝法律](../Page/翡翠台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，於1995年9月11日首播，共26集，監製[鄧特希](../Page/鄧特希.md "wikilink")。星期一至五播出。\[1\]\[2\]本劇前往[新加坡拍攝外景](../Page/新加坡.md "wikilink")。

## 演員表

### 主要演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></p></td>
<td><p>余在春</p></td>
<td><p>Ben<br />
大律師<br />
丁柔之夫，後離婚<br />
曾與殷芷-{杰}-有婚外情</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a></p></td>
<td><p>江承宇</p></td>
<td><p>Michael<br />
師爺<br />
江李頌雲之子<br />
湯芷琪前男友<br />
程若暉之男友<br />
劇集尾段遇車禍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳秀雯.md" title="wikilink">陳秀雯</a></p></td>
<td><p>丁　柔</p></td>
<td><p>Michelle<br />
高級检控官，余在春之妻，後離婚</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a></p></td>
<td><p>周志輝</p></td>
<td><p>Raymond<br />
律師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></p></td>
<td><p>程若暉</p></td>
<td><p>Sam／Samantha<br />
大律師<br />
程樂天之幼女<br />
江承宇之女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林保怡.md" title="wikilink">林保怡</a></p></td>
<td><p>方偉豪</p></td>
<td><p>Kelvin<br />
高級检控官<br />
程樂天的學生<br />
殷芷-{杰}-之前夫<br />
程若曦之男友<br />
妨礙司法公正被判入獄</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧萃雯.md" title="wikilink">鄧萃雯</a><br />
<a href="../Page/黃美棋.md" title="wikilink">黃美棋</a>（童年）</p></td>
<td><p>殷芷-{杰}-</p></td>
<td><p>Joyce<br />
法醫官<br />
方偉豪之前妻</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳芷菁.md" title="wikilink">陳芷菁</a></p></td>
<td><p>程若曦</p></td>
<td><p>Rachel<br />
大律師<br />
程樂天之長女<br />
方偉豪之女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楚原.md" title="wikilink">楚　原</a></p></td>
<td><p>程樂天</p></td>
<td><p>Victor<br />
法官<br />
程若曦, 程若暉之父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔣志光.md" title="wikilink">蔣志光</a></p></td>
<td><p>周少聰</p></td>
<td><p>Eric<br />
大律師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a></p></td>
<td><p>邱永康</p></td>
<td><p>Chris<br />
检控官<br />
梁美英之子<br />
邱永健之弟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a></p></td>
<td><p>邱永健</p></td>
<td><p>Anthony<br />
律師樓職員<br />
梁美英之子<br />
邱永康之兄<br />
曾為周志輝之情敵<br />
後為Ada男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳梅馨.md" title="wikilink">陳梅馨</a></p></td>
<td><p>關　靜</p></td>
<td><p>Jessica<br />
检控官</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/駱應鈞.md" title="wikilink">駱應鈞</a></p></td>
<td><p>周文彬</p></td>
<td><p>彬Sir<br />
沙展</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林漪娸.md" title="wikilink">林漪娸</a></p></td>
<td><p>林學宜</p></td>
<td><p>Doris<br />
督察</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬浚偉.md" title="wikilink">馬浚偉</a></p></td>
<td><p>陳卓堯</p></td>
<td><p>Stephen<br />
醫生<br />
程若暉之男友, 後分手為好友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷琪</p></td>
<td><p>Susan<br />
設計師<br />
江承宇前女友<br />
在<a href="../Page/新加坡.md" title="wikilink">新加坡被羅偉林殺死</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彭子晴.md" title="wikilink">彭子晴</a></p></td>
<td><p>方家琪</p></td>
<td><p>Jo／Josephine<br />
（即第一輯的角色，第一輯由<a href="../Page/鄭秀文.md" title="wikilink">鄭秀文飾演</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洪朝豐.md" title="wikilink">洪朝豐</a></p></td>
<td><p>張家賢</p></td>
<td><p>Jeff</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>梁美英</p></td>
<td><p>英姐<br />
江家前傭人<br />
邱永健、邱永康之母<br />
（第一輯由<a href="../Page/梁愛.md" title="wikilink">梁愛飾演</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李龍基.md" title="wikilink">李龍基</a></p></td>
<td><p>羅偉林</p></td>
<td><p>Philip<br />
湯芷琪前度情夫<br />
在<a href="../Page/新加坡.md" title="wikilink">新加坡因犯下謀殺罪被判處死刑而被問吊處死</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葉振聲.md" title="wikilink">葉振聲</a></p></td>
<td><p>韋志華</p></td>
<td><p>Kenny</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭政鴻.md" title="wikilink">郭政鴻</a></p></td>
<td><p>王　安</p></td>
<td><p>安仔<br />
探員<br />
因開槍誤殺途人趙鳳蘭被判入獄</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭耀明.md" title="wikilink">郭耀明</a></p></td>
<td><p>鄭子偉</p></td>
<td><p>Tommy<br />
探員<br />
代替被判入獄的王安</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃清榕.md" title="wikilink">黃清榕</a></p></td>
<td><p>Annie</p></td>
<td><p>探員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李煌生.md" title="wikilink">李煌生</a></p></td>
<td><p>強</p></td>
<td><p>探員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>超</p></td>
<td><p>探員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔣克.md" title="wikilink">蔣　克</a></p></td>
<td><p>探　員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戴耀明.md" title="wikilink">戴耀明</a></p></td>
<td><p>探　員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎秀英.md" title="wikilink">黎秀英</a></p></td>
<td><p>玲　姐</p></td>
<td><p>程家傭人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃凱詩.md" title="wikilink">黃凱詩</a></p></td>
<td><p>Ada</p></td>
<td><p>律師樓職員<br />
後為邱永健女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔡劍雪.md" title="wikilink">蔡劍雪</a></p></td>
<td><p>律師樓職員<br />
／接待員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/可心.md" title="wikilink">可　心</a></p></td>
<td><p>律師樓職員</p></td>
<td><p>Brenda</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張韻紅.md" title="wikilink">張韻紅</a></p></td>
<td><p>Maggie</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳忠華.md" title="wikilink">陳忠華</a></p></td>
<td><p>的士佬</p></td>
<td><p>新加坡的士司機</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金舉棋.md" title="wikilink">金舉棋</a></p></td>
<td><p>Alex</p></td>
<td><p>私家偵探</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王偉樑.md" title="wikilink">王偉樑</a></p></td>
<td><p>新加坡警察</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卓凡.md" title="wikilink">卓　凡</a></p></td>
<td><p>Mike</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文嘉永.md" title="wikilink">文嘉永</a></p></td>
<td><p>Simon</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/許思敏.md" title="wikilink">許思敏</a></p></td>
<td><p>杜　太</p></td>
<td><p>杜永富太太</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李國麟_(演員).md" title="wikilink">李國麟</a></p></td>
<td><p>張志倫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>陳初九</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮瑞珍.md" title="wikilink">馮瑞珍</a></p></td>
<td><p>馬修女</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伍文生.md" title="wikilink">伍文生</a></p></td>
<td><p>陳社工</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古明華.md" title="wikilink">古明華</a></p></td>
<td><p>沈　榮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭金.md" title="wikilink">郭　金</a></p></td>
<td><p>余愛蓮</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何美好.md" title="wikilink">何美好</a></p></td>
<td><p>Bartender</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/文潔雲.md" title="wikilink">文潔雲</a></p></td>
<td><p>方偉豪母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭家生.md" title="wikilink">鄭家生</a></p></td>
<td><p>方偉豪父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭瑞曉.md" title="wikilink">鄭瑞曉</a></p></td>
<td><p>法官助手<br />
／律師助手<br />
／程若暉助手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸希揚.md" title="wikilink">陸希揚</a></p></td>
<td><p>程若曦助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈寶思.md" title="wikilink">沈寶思</a></p></td>
<td><p>法醫助手</p></td>
<td><p>殷芷-{杰}-之助手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湯俊明.md" title="wikilink">湯俊明</a></p></td>
<td><p>法醫助手</p></td>
<td><p>殷芷-{杰}-之助手</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盧剛_(演員).md" title="wikilink">盧　剛</a></p></td>
<td><p>律師助手<br />
／柔助手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李衛民.md" title="wikilink">李衛民</a></p></td>
<td><p>律師助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈星銘.md" title="wikilink">沈星銘</a></p></td>
<td><p>聰助手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王大偉_(演員).md" title="wikilink">王大偉</a></p></td>
<td><p>律師助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曹濟.md" title="wikilink">曹　濟</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳中堅.md" title="wikilink">陳中堅</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華忠男.md" title="wikilink">華忠男</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何璧堅.md" title="wikilink">何璧堅</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅國維.md" title="wikilink">羅國維</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/呂劍光.md" title="wikilink">呂劍光</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 案件內容

### 一、王安案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭政鴻.md" title="wikilink">郭政鴻</a></p></td>
<td><p>王　安</p></td>
<td><p>安仔<br />
探員<br />
被告<br />
因開槍誤殺途人趙鳳蘭被判入獄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林芷筠.md" title="wikilink">林芷筠</a></p></td>
<td><p>趙鳳蘭</p></td>
<td><p>細鳳<br />
死者<br />
被王安誤認為持械劫匪擊斃</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 王安  |
| **罪名**   |     |
| **被害人**  | 趙鳳蘭 |
| **檢控官**  | 邱永康 |
| **辯方律師** | 余在春 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

### 二、孔先生案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何英偉.md" title="wikilink">何英偉</a></p></td>
<td><p>仁</p></td>
<td><p>孔先生<br />
辯方<br />
謝秀美前僱主</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝鳳儀.md" title="wikilink">謝鳳儀</a></p></td>
<td><p>謝秀美</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余慕蓮.md" title="wikilink">余慕蓮</a></p></td>
<td><p>Irene Chan</p></td>
<td><p>法官<br />
謝秀美案主審</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 孔先生 |
| **事項**   |     |
| **原告人**  | 謝秀美 |
| **控方律師** | 程若暉 |
| **辯方律師** | 周志輝 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

### 三、李偉成案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李家強.md" title="wikilink">李家強</a></p></td>
<td><p>李偉成</p></td>
<td><p>的士司機<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳寶靈.md" title="wikilink">陳寶靈</a></p></td>
<td><p>阿　娟</p></td>
<td><p>全名趙麗娟<br />
李偉成強姦案受害人</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 李偉成  |
| **罪名**   |      |
| **被害人**  | 趙麗娟等 |
| **檢控官**  | 丁柔   |
| **辯方律師** | 方偉豪  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

### 四、陳美蓮案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何嘉麗_(演員).md" title="wikilink">何嘉麗</a></p></td>
<td><p>陳美蓮</p></td>
<td><p>王華前妻<br />
被告<br />
毒殺兩子王文忠, 王文龍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃成想.md" title="wikilink">黃成想</a></p></td>
<td><p>王　華</p></td>
<td><p>陳美蓮前夫<br />
王文忠, 王文龍之父</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><strong>被告</strong></p></td>
<td><p>陳美蓮</p></td>
</tr>
<tr class="even">
<td><p><strong>罪名</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>被害人</strong></p></td>
<td><p>王文忠<br />
王文龍</p></td>
</tr>
<tr class="even">
<td><p><strong>檢控官</strong></p></td>
<td><p>關靜</p></td>
</tr>
<tr class="odd">
<td><p><strong>辯方律師</strong></p></td>
<td><p>程若曦</p></td>
</tr>
<tr class="even">
<td><p><strong>控方證人</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>辯方證人</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>結果</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

### 五、李立光案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李家明_(演員).md" title="wikilink">李家明</a></p></td>
<td><p>李立光</p></td>
<td><p>Marco<br />
醫生</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸婉芬.md" title="wikilink">陸婉芬</a></p></td>
<td><p>姚　倩</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭家麗.md" title="wikilink">彭家麗</a></p></td>
<td><p>茵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何美鈿.md" title="wikilink">何美鈿</a></p></td>
<td><p>護　士</p></td>
<td><p>李立光診所護士</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉桂芳.md" title="wikilink">劉桂芳</a></p></td>
<td><p>鄰　居</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚一清.md" title="wikilink">譚一清</a></p></td>
<td><p>茵　父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td><p>茵　母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石云.md" title="wikilink">石　-{云}-</a></p></td>
<td><p>管理員</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 李立光 |
| **罪名**   |     |
| **被害人**  | 姚倩  |
| **檢控官**  | 丁柔  |
| **辯方律師** | 程若曦 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

### 六、唐先生案

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [夏韶聲](../Page/夏韶聲.md "wikilink") | 昌      | 唐先生       |
|                                  |        |           |

|          |     |
| -------- | --- |
| **被告**   | 胡向強 |
| **事項**   |     |
| **原告人**  | 唐先生 |
| **控方律師** | 周志輝 |
| **辯方律師** |     |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

### 七、神油公司案

|                                  |          |           |
| -------------------------------- | -------- | --------- |
| **演員**                           | **角色**   | **暱稱/身份** |
| [王維德](../Page/王維德.md "wikilink") | 葉世邦      |           |
| [李桂英](../Page/李桂英.md "wikilink") | 葉世邦妻     |           |
| [林道權](../Page/林道權.md "wikilink") | 神油公司東主   |           |
| [曾健明](../Page/曾健明.md "wikilink") | 神油公司代表律師 |           |
|                                  |          |           |

|          |      |
| -------- | ---- |
| **被告**   | 神油公司 |
| **事項**   |      |
| **原告人**  | 葉世邦  |
| **控方律師** | 余在春  |
| **辯方律師** |      |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

### 八、趙中正、李正堅案

|                                         |        |           |
| --------------------------------------- | ------ | --------- |
| **演員**                                  | **角色** | **暱稱/身份** |
| [陳立志](../Page/陳立志_\(演員\).md "wikilink") | 趙中正    | 被告        |
| [黃賦裴](../Page/黃賦裴.md "wikilink")        | 李正堅    | 被告        |
| [關　菁](../Page/關菁.md "wikilink")         | 德　父    |           |
| [蘇恩磁](../Page/蘇恩磁.md "wikilink")        | 德　母    |           |
| [博　君](../Page/博君.md "wikilink")         | 張　勝    |           |
| [郭德信](../Page/郭德信.md "wikilink")        | 陳律師    |           |
|                                         |        |           |

|          |     |     |
| -------- | --- | --- |
| **被告**   | 趙中正 | 李正堅 |
| **罪名**   |     |     |
| **被害人**  | 梁偉德 |     |
| **檢控官**  | 丁柔  |     |
| **辯方律師** | 周少聰 | 陳律師 |
| **控方證人** |     |     |
| **辯方證人** |     | /   |
| **結果**   |     |     |

### 九、胡貴蘭案

|                                          |        |           |
| ---------------------------------------- | ------ | --------- |
| **演員**                                   | **角色** | **暱稱/身份** |
| [張　捷](../Page/張捷_\(內地演員\).md "wikilink") | 胡貴蘭    |           |
| [鄧英敏](../Page/鄧英敏.md "wikilink")         | 陳貴東    |           |
| [談佩珊](../Page/談佩珊.md "wikilink")         | 梁慧珍    | 陳貴東之妻     |
| [曹倩婷](../Page/曹倩婷.md "wikilink")         | 陳美芝    | 陳貴東與胡貴蘭女兒 |
|                                          |        |           |

|          |      |
| -------- | ---- |
| **被告**   | 胡貴蘭  |
| **罪名**   |      |
| **被害人**  | 陳貴東  |
| **檢控官**  | 余在春  |
| **辯方律師** | 程若暉  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   | 無罪釋放 |

### 十、Frankie Fan案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳展鵬.md" title="wikilink">陳展鵬</a></p></td>
<td><p>Frankie Fan</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳燕航.md" title="wikilink">陳燕航</a></p></td>
<td><p>售貨員</p></td>
<td><p>全名歐冠英<br />
上華百貨職員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃仲匡.md" title="wikilink">黃仲匡</a></p></td>
<td><p>保安主任</p></td>
<td><p>上華百貨職員</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |             |
| -------- | :---------: |
| **被告**   | Frankie Fan |
| **罪名**   |             |
| **被害人**  |    上華百貨     |
| **檢控官**  |     邱永康     |
| **辯方律師** |     周志輝     |
| **控方證人** |             |
| **辯方證人** |             |
| **結果**   |             |

### 十一、韋志華案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葉振聲.md" title="wikilink">葉振聲</a></p></td>
<td><p>韋志華</p></td>
<td><p>Kenny<br />
設計師<br />
被告<br />
因強姦關文玉被判入獄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉美珊.md" title="wikilink">劉美珊</a></p></td>
<td><p>關文玉</p></td>
<td><p>弱智人士</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 韋志華  |
| **罪名**   |      |
| **被害人**  | 關文玉  |
| **檢控官**  | 關靜   |
| **辯方律師** | 江李頌雲 |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

### 十二、劉錦權案

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [黎漢持](../Page/黎漢持.md "wikilink") | 蔡樹基    | 被告        |
| [饒秋寶](../Page/饒秋寶.md "wikilink") | 馬淑芳    | 劉錦權之妻     |
| [鄺佐輝](../Page/鄺佐輝.md "wikilink") | 王律師    | 蔡樹基辯護律師   |
|                                  |        |           |

|          |     |
| -------- | --- |
| **被告**   | 蔡樹基 |
| **罪名**   |     |
| **被害人**  | 劉錦權 |
| **檢控官**  | 丁柔  |
| **辯方律師** | 王律師 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

### 十三、丁柔案

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳秀雯.md" title="wikilink">陳秀雯</a></p></td>
<td><p>丁　柔</p></td>
<td><p>Michelle<br />
高級检控官<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林保怡.md" title="wikilink">林保怡</a></p></td>
<td><p>方偉豪</p></td>
<td><p>Kelvin<br />
高級检控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洪朝豐.md" title="wikilink">洪朝豐</a></p></td>
<td><p>張家賢</p></td>
<td><p>Jeff<br />
證人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/饒秋寶.md" title="wikilink">饒秋寶</a></p></td>
<td><p>馬淑芳</p></td>
<td><p>劉錦權之妻</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 丁柔  |
| **罪名**   |     |
| **被害人**  |     |
| **檢控官**  | 方偉豪 |
| **辯方律師** | 余在春 |
| **控方證人** |     |
| **辯方證人** | 張家賢 |
| **結果**   |     |

## 歌曲

  - 主題音樂：《[Your Latest Trick](../Page/Your_Latest_Trick.md "wikilink")》
      - SAXO-PHONE：[Phil Rombaoa](../Page/Phil_Rombaoa.md "wikilink")
      - 作曲：[Mark Knopfler](../Page/Mark_Knopfler.md "wikilink") /
        [羅雁武](../Page/羅雁武.md "wikilink")

<!-- end list -->

  - 插曲《[Dreaming Of You](../Page/夢見你_\(音樂專輯\).md "wikilink")》
      - 作曲：[Franne Golde](../Page/Franne_Golde.md "wikilink") 及 [Tom
        Snow](../Page/Tom_Snow.md "wikilink")
      - 填詞：[Franne Golde](../Page/Franne_Golde.md "wikilink") 及 [Tom
        Snow](../Page/Tom_Snow.md "wikilink")
      - 主唱：[Selena](../Page/莎麗娜.md "wikilink")

<!-- end list -->

  - 插曲《假使有日能忘記》
      - 作曲：[黃尚偉](../Page/黃尚偉.md "wikilink")
      - 填詞：[梁芷珊](../Page/梁芷珊.md "wikilink")
      - 主唱：[蘇永康](../Page/蘇永康.md "wikilink")

<!-- end list -->

  - 插曲《願望用一生愛你》
      - 作曲：Park Kang Young、Park Ju Yeon
      - 填詞：[張美賢](../Page/張美賢.md "wikilink")
      - 主唱：[馬浚偉](../Page/馬浚偉.md "wikilink")

## 外部連結

  - [無綫電視官方網頁 -
    壹號皇庭IV](https://web.archive.org/web/20060825094120/http://tvcity.tvb.com/drama/just4/index.htm)
  - [《壹號皇庭IV》 GOTV
    第1集重溫](https://web.archive.org/web/20140222163157/http://gotv.tvb.com/programme/102401/150402/)

## 參考

## 電視節目的變遷

[Category:1995年無綫電視劇集](../Category/1995年無綫電視劇集.md "wikilink")
[Category:無綫電視1990年代背景劇集](../Category/無綫電視1990年代背景劇集.md "wikilink")

1.  [【集體回憶】逐個數！《壹號皇庭》造就紅星](http://hk.on.cc/hk/bkn/cnt/entertainment/20171210/bkn-20171210120011076-1210_00862_001.html)
2.  [蘇永康自爆冇睇過《壹號皇庭IV》要晚晚追返](http://hd.stheadline.com/news/realtime/ent/1055792/)