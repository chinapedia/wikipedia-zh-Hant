[Railways_at_Kaohsiung.svg](https://zh.wikipedia.org/wiki/File:Railways_at_Kaohsiung.svg "fig:Railways_at_Kaohsiung.svg")
**壽山車站**位於[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[鼓山區](../Page/鼓山區.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[縱貫線](../Page/縱貫線_\(南段\).md "wikilink")、[屏東線已廢除的](../Page/屏東線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。位置約在高雄市北斗街平交道口，[高雄環狀輕軌](../Page/高雄環狀輕軌.md "wikilink")[文武聖殿站與](../Page/文武聖殿站.md "wikilink")[鼓山區公所站之間](../Page/鼓山區公所站.md "wikilink")。

## 車站構造

  - 通過站，月臺2座（縱貫線上行側式月臺一座，縱貫線下行及屏東線上下行單線雙向島式月臺一座）。

## 利用狀況

  - 已廢止。

## 車站週邊

  - [高雄大義宮](../Page/高雄大義宮.md "wikilink")
  - [文武聖殿](../Page/文武聖殿.md "wikilink")

## 歷史

  - 1931年8月1日：設**山下町臨時停車場**。
  - 1942年：汽油車停駛，休業。
  - 1952年2月10日：復設**山下町車站**，初期為鐵路員工通勤列車站，不對外營業。
  - 1952年7月1日：改為**壽山車站**，正式對外營業，為簡易站，只停靠汽油車。
  - 1958年4月5日：降為招呼站，由[高雄港車站管理](../Page/高雄港車站.md "wikilink")。
  - 1962年：廢止。
  - 目前僅存一座月臺的殘跡，其餘者皆已拆卸。

## 鄰近車站

|-

[廢](../Category/縱貫線車站_\(南段\).md "wikilink")
[廢](../Category/屏東線車站.md "wikilink")
[高](../Category/台灣鐵路廢站.md "wikilink")
[廢](../Category/鼓山區鐵路車站.md "wikilink")
[Category:1931年启用的铁路车站](../Category/1931年启用的铁路车站.md "wikilink")
[S](../Category/1962年關閉的鐵路車站.md "wikilink")
[Category:台灣已拆除交通建築物](../Category/台灣已拆除交通建築物.md "wikilink")
[Category:以山命名的臺灣鐵路車站](../Category/以山命名的臺灣鐵路車站.md "wikilink")
[Category:台灣鐵道旅遊景點](../Category/台灣鐵道旅遊景點.md "wikilink")