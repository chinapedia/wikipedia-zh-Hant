在[計算複雜度理論中](../Page/計算複雜度理論.md "wikilink")，一個**複雜度類**指的是一群複雜度類似的問題的集合。一個典型的複雜度類的定義有以下-{zh-hant:型式;zh-hans:形式}-：

  -
    可以被同一個[抽象機器M使用](../Page/抽象機器.md "wikilink")[O](../Page/大O符号.md "wikilink")(f(n))的資源R所解決的問題的集合（*n*是輸入資料的大小）。

例如[**NP**類別就是一群可以被一](../Page/NP_\(複雜度\).md "wikilink")[非確定型圖靈機以](../Page/非確定型圖靈機.md "wikilink")[多項式時間解決的](../Page/多項式時間.md "wikilink")[決定型問題](../Page/決定型問題.md "wikilink")。而**[P](../Page/P_\(複雜度\).md "wikilink")**類別則是一群可以被[確定型圖靈機以](../Page/圖靈機.md "wikilink")[多項式時間解決的決定型問題](../Page/多項式時間.md "wikilink")。某些複雜度類是一群[函式問題的集合](../Page/函式問題.md "wikilink")，例如[**FP**](../Page/FP_\(複雜度\).md "wikilink")。

許多複雜度類可被描述它的[數學邏輯特徵化](../Page/數學邏輯.md "wikilink")，請見。

而[Blum公理用於不需實際](../Page/Blum_axioms.md "wikilink")[計算模型就可定義複雜度類的情況](../Page/計算模型.md "wikilink")。

## 複雜度類之間的關係

下表簡列了一些屬於複雜度理論的問題（或語言、文法等）類別。如果類別**X**是類別**Y**的[子集合](../Page/子集合.md "wikilink")，則**X**將會畫在**Y**底下，並以一黑線相連。如果**X**是子集合，但不知是否與**Y**相等，則以較輕的虛線相連。實際上可解與不可解問題更屬於[可計算性理論](../Page/可計算性理論.md "wikilink")，但是它有助於更透徹了解複雜度類的問題。

<table cellpadding="0" cellspacing="0" border="0" style="margin:auto;">

<tr style="text-align:center;">

<td colspan=2>

</td>

<td colspan=4>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[決定性問題](../Page/決定性問題.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=2>

</td>

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td colspan=2>

</td>

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[類型0（递归可枚举）](../Page/递归可枚举语言.md "wikilink")

</td>

</tr>

</table>

</td>

<td>

</td>

<td colspan=4>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:200%; height:200%;">

<tr>

<td style="text-align:center;">

[未決定問題](../Page/未決定問題列表.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[递归](../Page/递归语言.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[EXPSPACE](../Page/EXPSPACE.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[NEXPTIME](../Page/NEXPTIME.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[EXPTIME](../Page/EXPTIME.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=8>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[PSPACE](../Page/PSPACE.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td width=40>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[類型1（上下文有关）](../Page/上下文有关语言.md "wikilink")

</td>

</tr>

</table>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td border="1">

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[反NP](../Page/反NP.md "wikilink")

</td>

</tr>

</table>

</td>

<td>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[BQP](../Page/BQP.md "wikilink")

</td>

</tr>

</table>

</td>

<td>

</td>

<td colspan=2>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[NP](../Page/NP_\(複雜度\).md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[BPP](../Page/BPP_\(複雜度\).md "wikilink")

</td>

</tr>

</table>

</td>

<td width=10>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td colspan=5>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[P](../Page/P_\(複雜度\).md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

<td>

[image:dottedLine.png](../Page/image:dottedLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td colspan=2>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightGreen; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[NC](../Page/NC_\(複雜度\).md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

<td colspan=2>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[類型2（上下文无关）](../Page/上下文无关语言.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

[image:solidLine.png](../Page/image:solidLine.png.md "wikilink")

</td>

</tr>

<tr style="text-align:center;">

<td colspan=3>

<table cellpadding="0" cellspacing="0" border="1" style="background:lightBlue; width:100%; height:100%;">

<tr>

<td style="text-align:center;">

[類型3（正则）](../Page/正则语言.md "wikilink")

</td>

</tr>

</table>

</td>

</tr>

</table>

## 擴充閱讀

  - [複雜度類大觀園](https://complexityzoo.uwaterloo.ca/Complexity_Zoo)：一個巨大的複雜度類列表，專家級使用。
  - [複雜度類架構圖](https://web.archive.org/web/20160416021243/https://people.cs.umass.edu/~immerman/complexity_theory.html)，由[Neil
    Immerman製作](../Page/Neil_Immerman.md "wikilink")，展示複雜度類的階層架構與它們是如何定位的。
  - [Michael Garey與](../Page/Garey,_Michael_R..md "wikilink")[David S.
    Johnson](../Page/David_S._Johnson.md "wikilink"): *Computers and
    Intractability: A Guide to the Theory of NP-Completeness.* New York:
    W. H. Freeman & Co., 1979.
    [NP-complete](../Page/NP-complete.md "wikilink")（NPC問題是解決某些[數學難題的重要關鍵](../Page/P/NP問題.md "wikilink")，這問題暗示人們是否可以將某些[演算法的執行效率提升到可接受的範圍](../Page/演算法.md "wikilink")）問題的標準指南。

## 参见

  - [複雜度類列表](../Page/複雜度類列表.md "wikilink")

{{-}}

[複雜度類](../Category/複雜度類.md "wikilink")
[Category:計算複雜性理論](../Category/計算複雜性理論.md "wikilink")