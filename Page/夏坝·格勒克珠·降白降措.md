[Rinpoche.jpg](https://zh.wikipedia.org/wiki/File:Rinpoche.jpg "fig:Rinpoche.jpg")
**夏坝·格勒克珠·降白降措**（）俗名**降央克珠**，法名**格勒克珠·降白降措**，[藏族](../Page/藏族.md "wikilink")，今[四川省](../Page/四川省.md "wikilink")[理塘县](../Page/理塘县.md "wikilink")[下坝片区](../Page/下坝片区.md "wikilink")[觉吾乡觉吾村人](../Page/觉吾乡.md "wikilink")，第四世（不计追认）[夏坝活佛](../Page/夏坝活佛.md "wikilink")。\[1\]

## 生平

1968年10月6日（农历八月十五日）生于觉吾村的荣贞家。6岁入夏坝小学读书。\[2\]\[3\]8岁时，在下坝[麻通寺法台](../Page/麻通寺.md "wikilink")[赤勒绕央的座下剃度出家](../Page/赤勒绕央.md "wikilink")，起法名为“格勒克珠·降白降措”。此后9年中，随赤勒绕央学习以《菩提道次第广论》、《密宗道次第广论》为主的[格鲁派显密教法](../Page/格鲁派.md "wikilink")。17岁时，被[下密院住持](../Page/下密院.md "wikilink")[果索及](../Page/果索.md "wikilink")[益西旺秋共同认定为](../Page/益西旺秋.md "wikilink")[理塘县](../Page/理塘县.md "wikilink")[长青春科尔寺第三世](../Page/长青春科尔寺.md "wikilink")[夏坝活佛](../Page/夏坝活佛.md "wikilink")[夏坝·昂旺罗桑丹增应巴的](../Page/夏坝·昂旺罗桑丹增应巴.md "wikilink")[转世灵童](../Page/转世灵童.md "wikilink")，随后入[色拉寺学习](../Page/色拉寺.md "wikilink")[五部大论](../Page/五部大论.md "wikilink")。1995年10月，国家正式认定其为第四世夏坝活佛，并举办[坐床典礼](../Page/坐床.md "wikilink")。\[4\]

夏坝活佛曾经先后赴[拉萨三大寺](../Page/拉萨三大寺.md "wikilink")、[扎什伦布寺](../Page/扎什伦布寺.md "wikilink")、[塔尔寺](../Page/塔尔寺.md "wikilink")、[拉卜楞寺等寺院修习显密教授](../Page/拉卜楞寺.md "wikilink")，并曾经相继拜[十世班禅](../Page/十世班禅.md "wikilink")、[阿旺提秋](../Page/阿旺提秋.md "wikilink")、果索、永嘉、[益西旺秋](../Page/益西旺秋.md "wikilink")、[阿钦等](../Page/阿钦.md "wikilink")30余位[格鲁派高僧为师](../Page/格鲁派.md "wikilink")。他还曾经在[北京大学宗教学系研究生班进修](../Page/北京大学.md "wikilink")。\[5\]

1989年起，夏坝活佛在[理塘县](../Page/理塘县.md "wikilink")[长青春科尔寺法相院任教](../Page/长青春科尔寺.md "wikilink")。1994年，出任下坝[麻通寺住持](../Page/麻通寺.md "wikilink")。1997年，夏坝活佛主持了下坝麻通寺的整体修复工程，并且设立教育基金会、法务基金会、建设基金会以及福利医院。1998年，出任理塘长青春科尔寺法相院住持。2000年，出任理塘长青春科尔寺金刚上师。2000年，为理塘长青春科尔寺修建了以如来八宝塔为主题的长达四千米的千塔围墙。2002年4月25日，出任理塘长青春科尔寺第九十三代住持。同年，应[沈阳市宗教局及](../Page/沈阳市.md "wikilink")[沈阳市佛教协会的邀请](../Page/沈阳市佛教协会.md "wikilink")，经[甘孜藏族自治州宗教局同意](../Page/甘孜藏族自治州.md "wikilink")，于2002年7月21日兼任沈阳北塔[护国法轮寺住持](../Page/护国法轮寺.md "wikilink")。2004年，主持修复了下坝麻通寺文殊院。2005年，向[拉萨三大寺](../Page/拉萨三大寺.md "wikilink")、[扎什伦布寺等](../Page/扎什伦布寺.md "wikilink")11座寺院近万名僧众供养[袈裟以及法帽](../Page/袈裟.md "wikilink")。2006年5月，应[大庆市人民政府](../Page/大庆市.md "wikilink")、大庆市宗教局、[大庆市佛教协会的邀请](../Page/大庆市佛教协会.md "wikilink")，经[四川省宗教局同意](../Page/四川省宗教局.md "wikilink")，兼任[大庆](../Page/大庆.md "wikilink")[富余正洁寺住持](../Page/富余正洁寺.md "wikilink")。\[6\]\[7\]

夏坝活佛曾任[理塘县人民政府副县长](../Page/理塘县.md "wikilink")，理塘县政协副主席。后来出任[四川省佛教协会常务理事](../Page/四川省佛教协会.md "wikilink")、[中国宗教学会理事](../Page/中国宗教学会.md "wikilink")、[甘孜藏族自治州政协委员](../Page/甘孜藏族自治州.md "wikilink")、理塘县人大常委会副主任。\[8\]

夏坝活佛还经常进行佛教交流活动。1996年，代表[四川省佛教协会接待了第一次访问中国的](../Page/四川省佛教协会.md "wikilink")[俄罗斯佛教代表团](../Page/俄罗斯.md "wikilink")。1997年10月，作为四川省藏语系佛教代表，到[日本参加中国](../Page/日本.md "wikilink")、日本、[韩国三国佛教友好交流会](../Page/韩国.md "wikilink")。1998年初，应[泰国邀请](../Page/泰国.md "wikilink")，作为中国[藏传佛教的代表](../Page/藏传佛教.md "wikilink")，参加[泰国](../Page/泰国.md "wikilink")[法身寺法身塔装藏仪式及中泰佛教友好交流会](../Page/法身寺.md "wikilink")。\[9\]

## 参考文献

## 外部链接

  - [夏坝仁波切](http://www.xiaba.org/)
  - [佛学宝藏](http://www.beita.org/)

{{-}}

[Category:夏坝活佛](../Category/夏坝活佛.md "wikilink")
[Category:理塘人](../Category/理塘人.md "wikilink")
[Category:藏族人](../Category/藏族人.md "wikilink")

1.  [上师简介，夏坝仁波切，于2013-03-16查阅](http://www.xiaba.org/ShowIntro.aspx?id=12)

2.  [夏坝仁波切简介，佛教导航，2012年10月25日](http://www.fjdh.com/wumin/2012/10/105109189142.html)


3.  [第四世夏坝活佛--降央克珠，显密文库，于2013-03-16查阅](http://read.goodweb.cn/news/news_view.asp?newsid=22193)


4.
5.
6.
7.
8.
9.