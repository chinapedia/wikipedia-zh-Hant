**夏威夷州州旗**（[夏威夷語](../Page/夏威夷語.md "wikilink")：Ka Hae
Hawaii；[英語](../Page/英語.md "wikilink")：Flag of
Hawaii）經歷了[王国](../Page/夏威夷王國.md "wikilink")、[保护国](../Page/夏威夷共和国.md "wikilink")、[共和國](../Page/夏威夷共和國.md "wikilink")、[领地](../Page/夏威夷领地.md "wikilink")，以及[夏威夷州五個不同政治體制](../Page/夏威夷州.md "wikilink")，是美國各州旗幟獨有的。另外本旗亦是美國州旗中唯一有[英國國旗圖案的](../Page/英國國旗.md "wikilink")。
[Flag-of-hawaii-flying.jpg](https://zh.wikipedia.org/wiki/File:Flag-of-hawaii-flying.jpg "fig:Flag-of-hawaii-flying.jpg")

## 樣式

夏威夷州州旗的[旗角繪有英國國旗](../Page/旗角.md "wikilink")，旗地為八道橫帶，代表八大島嶼，自上而下的次序為白－紅－藍－白－紅－藍－白－紅。所代表的島嶼為[夏威夷島](../Page/夏威夷島.md "wikilink")、[歐胡島](../Page/歐胡島.md "wikilink")、[考愛島](../Page/考愛島.md "wikilink")、[卡胡拉威島](../Page/卡胡拉威島.md "wikilink")、[拉奈島](../Page/拉奈島.md "wikilink")、[茂宜島](../Page/茂宜島.md "wikilink")、[莫洛凱島和](../Page/莫洛凱島.md "wikilink")[尼豪島](../Page/尼豪島.md "wikilink")。\[1\]

## 沿革

對夏威夷州州旗的起源有多種說法。其中一種是統一夏威夷的[卡美哈梅哈大帝為表示與](../Page/卡美哈梅哈大帝.md "wikilink")[英王](../Page/英國君主.md "wikilink")[喬治三世友好升起由英國探險家](../Page/喬治三世_\(英國\).md "wikilink")[喬治·溫哥華所贈的英國國旗](../Page/喬治·溫哥華.md "wikilink")。後來他的顧問注意到這樣可能會引發國際衝突，因為這樣會令人認為夏威夷是英國的盟國。於是國王就把旗幟從王宮降下來了。這故事的真實性存有爭議，但有一個說法指，後來為了在[1812年戰爭期間安撫美國](../Page/1812年戰爭.md "wikilink")，國王升起了[美國國旗](../Page/美國國旗.md "wikilink")，直到朝廷中的英國軍官強烈反對才作罷。這就解釋了為什麼夏威夷旗幟混合了英美兩國國旗的元素。\[2\]

於是卡美哈梅哈大帝在1816年委託製作夏威夷自己的旗幟，以避免衝突。歷史學家據旗幟的基礎是[英國海軍旗](../Page/英國海軍旗.md "wikilink")，便認為設計者是一位[英國皇家海軍軍官](../Page/英國皇家海軍.md "wikilink")，但軍官的名字並沒有統一的說法：有人認為他是[亞歷山大·亞當斯](../Page/亞歷山大·亞當斯.md "wikilink")，另一些人認為是喬治·貝克利（George
Beckley）。原來的旗幟有人認為是受了英國的影響，所以條紋的次序為交替的紅－白－藍，另一些人則認為是受到美國的影響。事實上，第一面正式使用的旗幟，條紋的順序就白－紅－藍，即目前的樣子。條紋的數目也有過變化：原來的旗幟有七條橫條，1845年正式改為八條，以後沿襲至今。\[3\]

## 州長旗

[Flag_of_the_Governor_of_Hawaii.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Governor_of_Hawaii.svg "fig:Flag_of_the_Governor_of_Hawaii.svg")
[夏威夷州州長所用的旗幟由紅藍兩色組成](../Page/夏威夷州州長.md "wikilink")，中間書有大寫的「夏威夷」字樣，圍以八顆星星。在準州時期初旗面「夏威夷」的位置上所標的是「TH」（即Territory
of Hawaii的簡寫）\[4\]

## 州旗日

1990年，時任州長的 [約翰·D·威希三世](../Page/約翰·D·威希三世.md "wikilink")（John D. Waihee
III）宣佈每年的7月31日為州旗日（Ka Hae Hawaii）。當地至今每年都慶祝這個節日。\[5\]

## 历代旗帜

  -
    {| class="wikitable"

|- \! 时间 \! 备注 \! 旗帜 |- |1793–1794
|[英国](../Page/英国.md "wikilink")[红船旗](../Page/红船旗.md "wikilink")\[6\]
|
style="text-align:center;"|[Red_Ensign_of_Great_Britain_(1707-1800).svg](https://zh.wikipedia.org/wiki/File:Red_Ensign_of_Great_Britain_\(1707-1800\).svg "fig:Red_Ensign_of_Great_Britain_(1707-1800).svg")
|- |1794–1816 |[大不列颠王国国旗](../Page/英国国旗.md "wikilink") (一直使用到1801年) |
style="text-align:center;"|[Flag_of_Great_Britain_(1707-1800).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Great_Britain_\(1707-1800\).svg "fig:Flag_of_Great_Britain_(1707-1800).svg")
|- |1816–1843 | [夏威夷王国旗帜](../Page/夏威夷王国.md "wikilink")(官方旗帜) |
style="text-align:center;"|[Flag_of_Hawaii_(1816).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii_\(1816\).svg "fig:Flag_of_Hawaii_(1816).svg")
|- |1843年2月 – 1843年7月 |[夏威夷王国旗帜](../Page/夏威夷王国.md "wikilink")(官方旗帜) |
style="text-align:center;"|[Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")
|- |1843年7月 – 1845年5月 | [夏威夷王国旗帜](../Page/夏威夷王国.md "wikilink")(官方旗帜) |
style="text-align:center;"|[Flag_of_Hawaii_(1816).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii_\(1816\).svg "fig:Flag_of_Hawaii_(1816).svg")
|- |1845年5月 –1893年1月 |[夏威夷王国旗帜](../Page/夏威夷王国.md "wikilink")(官方旗帜) |
style="text-align:center;"|[Flag_of_Hawaii_(1896).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii_\(1896\).svg "fig:Flag_of_Hawaii_(1896).svg")
|- |1893年2月 – 1893年4月 |[夏威夷王国](../Page/夏威夷王国.md "wikilink")
(直接使用[美国国旗](../Page/美国国旗.md "wikilink")) |
style="text-align:center;"|[Flag_of_the_United_States_(1891-1896).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States_\(1891-1896\).svg "fig:Flag_of_the_United_States_(1891-1896).svg")
|- |1894–1898 |[夏威夷共和国国旗](../Page/夏威夷共和国.md "wikilink") |
style="text-align:center;"|[Flag_of_Hawaii_(1896).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii_\(1896\).svg "fig:Flag_of_Hawaii_(1896).svg")
|- |1898–1960 |[夏威夷领地旗帜](../Page/夏威夷领地.md "wikilink") |
style="text-align:center;"|[Flag_of_Hawaii.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii.svg "fig:Flag_of_Hawaii.svg")
|- |1960–至今 |现在的夏威夷州州旗 |
style="text-align:center;"|[Flag_of_Hawaii.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii.svg "fig:Flag_of_Hawaii.svg")
|}

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [州憲對展示州旗的規定](https://web.archive.org/web/20150828154512/http://www.capitol.hawaii.gov/hrscurrent/vol01_ch0001-0042f/hrs0005/hrs_0005-0019.htm)
  - [Flags of the World :
    Hawaii](http://www.crwflags.com/fotw/flags/us-hi.html)

[Category:夏威夷州](../Category/夏威夷州.md "wikilink")
[Category:美國州旗](../Category/美國州旗.md "wikilink")

1.

2.

3.
4.
5.

6.