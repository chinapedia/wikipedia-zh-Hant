[美國](../Page/美國.md "wikilink")[夏威夷州所有](../Page/夏威夷州.md "wikilink")5個縣，相對[美國本土上的縣享有更高等的地位](../Page/美國本土.md "wikilink")。夏威夷州的縣是美國唯一能夠合法成立自己的政府機構的縣，但這造成了縣之下不能有自己的政府機構，例如城市不能擁有自己的政府機構。即使檀香山是夏威夷州首府，但它也是以「[檀香山市縣](../Page/檀香山市縣.md "wikilink")」的形式存在。夏威夷州是美國境內縣數量最少的州之一，另一個縣數量為5的州為[羅德島州](../Page/羅德島州.md "wikilink")。而[特拉華州僅有](../Page/特拉華州.md "wikilink")3個縣，使之成為美國縣數量最少的州。與美國其餘的49個州不同，夏威夷州的公眾教育是由[夏威夷州教育部負責](../Page/夏威夷州教育部.md "wikilink")，但其餘49個州的公眾教育則是由[教育委員會負責](../Page/教育委員會.md "wikilink")。\[1\]夏威夷州的縣收集得來的稅款，其用途主要為道路維修、社區活動、公園（包括在海灘公園上的救生員）、垃圾收集、警察（即州警，由夏威夷公共安全部負責）、醫療服務和消防服務。\[2\]所有夏威夷州的縣皆於1905年建立，即[夏威夷領地建立](../Page/夏威夷領地.md "wikilink")7年後。\[3\]\[4\]在5個縣中，[卡拉沃县是特別為](../Page/卡拉沃县.md "wikilink")[麻风病人而設的](../Page/麻风病.md "wikilink")，因此其政府機構人數極少。\[5\]

## 县份列表

<table>
<thead>
<tr class="header">
<th><p>英文县名</p></th>
<th><p>中文译名</p></th>
<th><p><a href="../Page/FIPS.md" title="wikilink">FIPS</a></p></th>
<th><p>县治</p></th>
<th><p>成立年份</p></th>
<th><p>起源</p></th>
<th><p>人口</p></th>
<th><p>面积<br />
（）</p></th>
<th><p>位置</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Hawaiʻi</p></td>
<td><p><a href="../Page/夏威夷县_(夏威夷州).md" title="wikilink">夏威夷县</a></p></td>
<td><p><a href="https://www.webcitation.org/606PwXfba?url=http://quickfacts.census.gov/qfd/states/15/15001.html">001</a></p></td>
<td><p><a href="../Page/希洛_(夏威夷州).md" title="wikilink">希洛</a><br />
</p></td>
<td><p>1905年</p></td>
<td><p>得名自<a href="../Page/夏威夷島.md" title="wikilink">夏威夷島</a>，夏威夷島則得名自傳說中的波利尼西亞航海家夏威夷羅亞（Hawaiiloa）。包含島嶼：<a href="../Page/夏威夷島.md" title="wikilink">夏威夷島</a>。</p></td>
<td><p>185,079</p></td>
<td><p>10,432</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Hawaii_highlighting_Hawaii_County.svg" title="fig:Map_of_Hawaii_highlighting_Hawaii_County.svg">Map_of_Hawaii_highlighting_Hawaii_County.svg</a></p></td>
</tr>
<tr class="even">
<td><p>Honolulu</p></td>
<td><p><a href="../Page/檀香山市县_(夏威夷州).md" title="wikilink">檀香山市县</a><br />
</p></td>
<td><p><a href="https://www.webcitation.org/606XdugFM?url=http://quickfacts.census.gov/qfd/states/15/15003.html">003</a></p></td>
<td><p>(合并市县)</p></td>
<td><p>1905年</p></td>
<td><p>得名自州府<a href="../Page/檀香山.md" title="wikilink">檀香山</a>（又稱火奴魯魯），火奴魯魯（Honolulu）在夏威夷語有「避風港」或「庇護所」的意思。[6]包含島嶼：<a href="../Page/歐胡島.md" title="wikilink">歐胡島</a>。</p></td>
<td><p>953,207</p></td>
<td><p>1,546</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Hawaii_highlighting_Honolulu_County.svg" title="fig:Map_of_Hawaii_highlighting_Honolulu_County.svg">Map_of_Hawaii_highlighting_Honolulu_County.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>Kalawao</p></td>
<td><p><a href="../Page/卡拉沃县_(夏威夷州).md" title="wikilink">-{zh-hans:卡拉沃;zh-hant:卡拉瓦歐}-县</a></p></td>
<td><p><a href="https://www.webcitation.org/607ZKjteb?url=http://quickfacts.census.gov/qfd/states/15/15005.html">005</a></p></td>
<td><p>无</p></td>
<td><p>1905年</p></td>
<td><p>得名自縣內同名村落。包含島嶼：<a href="../Page/摩洛凱島.md" title="wikilink">摩洛凱島小部分</a>。</p></td>
<td><p>90</p></td>
<td><p>135</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Hawaii_highlighting_Kalawao_County.svg" title="fig:Map_of_Hawaii_highlighting_Kalawao_County.svg">Map_of_Hawaii_highlighting_Kalawao_County.svg</a></p></td>
</tr>
<tr class="even">
<td><p>Kauaʻi</p></td>
<td><p><a href="../Page/考艾县_(夏威夷州).md" title="wikilink">-{zh-hans:考艾;zh-hant:考艾;zh-tw:可愛}-县</a></p></td>
<td><p><a href="https://www.webcitation.org/608ep2uGp?url=http://quickfacts.census.gov/qfd/states/15/15007.html">007</a></p></td>
<td><p><a href="../Page/利胡埃_(夏威夷州).md" title="wikilink">利胡埃</a><br />
</p></td>
<td><p>1905年</p></td>
<td><p>得名自縣內最大島嶼<a href="../Page/考愛島.md" title="wikilink">考愛島</a>，考愛島則得名自傳說中的夏威夷羅亞的長子考埃（Kauaʻi）。包含島嶼：<a href="../Page/考愛島.md" title="wikilink">考愛島</a>、<a href="../Page/你好島.md" title="wikilink">你好島</a>、<a href="../Page/里花島.md" title="wikilink">里花島</a>、<a href="../Page/考拉島.md" title="wikilink">考拉島</a>。</p></td>
<td><p>67,091</p></td>
<td><p>1,611</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Hawaii_highlighting_Kauai_County.svg" title="fig:Map_of_Hawaii_highlighting_Kauai_County.svg">Map_of_Hawaii_highlighting_Kauai_County.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>Maui</p></td>
<td><p><a href="../Page/茂宜县_(夏威夷州).md" title="wikilink">茂宜县</a></p></td>
<td><p><a href="https://www.webcitation.org/60AwNQB0R?url=http://quickfacts.census.gov/qfd/states/15/15009.html">009</a></p></td>
<td><p><a href="../Page/怀卢库_(夏威夷州).md" title="wikilink">怀卢库</a><br />
</p></td>
<td><p>1905年</p></td>
<td><p>得名自縣內最大島嶼<a href="../Page/茂宜島.md" title="wikilink">茂宜島</a>。而茂宜島則得名自土著神話中的同名半神人。包含島嶼：<a href="../Page/茂宜島.md" title="wikilink">茂宜島</a>、<a href="../Page/拉奈島.md" title="wikilink">拉奈島</a>、<a href="../Page/卡胡拉威島.md" title="wikilink">卡胡拉威島</a>、<a href="../Page/摩洛凱島.md" title="wikilink">摩洛凱島大部分</a>、<a href="../Page/摩洛基尼島.md" title="wikilink">摩洛基尼島</a>。</p></td>
<td><p>154,834</p></td>
<td><p>2,901</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Hawaii_highlighting_Maui_County.svg" title="fig:Map_of_Hawaii_highlighting_Maui_County.svg">Map_of_Hawaii_highlighting_Maui_County.svg</a></p></td>
</tr>
</tbody>
</table>

## 參考文獻

{{-}}

[HI](../Category/美国各州行政区划.md "wikilink")
[夏威夷州行政区划](../Category/夏威夷州行政区划.md "wikilink")

1.

2.

3.
4.

5.

6.