**夏尔·纪尧姆·马里·阿波利奈尔·安托万·库赞·蒙托邦，八里桥伯爵**（，），[法國將軍和政治家](../Page/法國.md "wikilink")，生于[巴黎](../Page/巴黎.md "wikilink")。1815年成為軍官，後畢業於參謀學院，1823年參加對[西班牙的遠征](../Page/西班牙.md "wikilink")。1831∼1857年蒙托邦以骑兵军官身份参与法军在[阿尔及利亚的行动](../Page/阿尔及利亚.md "wikilink")。1847年，捕获[阿卜杜卡迪尔](../Page/阿卜杜卡迪尔.md "wikilink")。1855年任師長。一度升至少将军衔指挥君士坦丁省，1858年被任命为本土司令，1859年末获选带领英法聯軍远征中国。迫使中國屈從1858年的《[天津條約](../Page/天津條約.md "wikilink")》。1862年，[拿破仑三世封蒙托邦为八里橋伯爵](../Page/拿破仑三世.md "wikilink")（因在[北京](../Page/北京.md "wikilink")[八里桥获胜](../Page/八里桥.md "wikilink"))。1865年他被任命指挥[里昂第四军团](../Page/里昂.md "wikilink")，在训练当中他表现出特殊的才能和行政能力。

他死在[凡尔赛](../Page/凡尔赛.md "wikilink")。

## 来源和参考

  -
[Category:法國總理](../Category/法國總理.md "wikilink")
[Category:法国伯爵](../Category/法国伯爵.md "wikilink")
[Category:法國軍事人物](../Category/法國軍事人物.md "wikilink")
[Category:第二次鴉片戰爭人物](../Category/第二次鴉片戰爭人物.md "wikilink")