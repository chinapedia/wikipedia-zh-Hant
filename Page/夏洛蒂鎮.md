[University_of_Virginia_Rotunda_2006.jpg](https://zh.wikipedia.org/wiki/File:University_of_Virginia_Rotunda_2006.jpg "fig:University_of_Virginia_Rotunda_2006.jpg")\]\]
[USA_Charlottesville_city,_Virginia_age_pyramid.svg](https://zh.wikipedia.org/wiki/File:USA_Charlottesville_city,_Virginia_age_pyramid.svg "fig:USA_Charlottesville_city,_Virginia_age_pyramid.svg")
**夏洛特鎮**或**夏洛茨维尔**（Charlottesville）是[美国](../Page/美国.md "wikilink")[弗吉尼亚州的一座城市](../Page/弗吉尼亚州.md "wikilink")，[詹姆斯河的支流rivanna河流经市内](../Page/詹姆斯河.md "wikilink")，[藍嶺山位于城市以西約](../Page/藍嶺山.md "wikilink")20英里。

该市成立於1762年，以前的路被稱為「三道缺口」（今[美國國道250](../Page/美國國道250.md "wikilink")）。其他通過夏律第的主要公路有[美國國道29](../Page/美國國道29.md "wikilink")，從[華盛頓到](../Page/華盛頓.md "wikilink")[丹維爾](../Page/丹維爾_\(維吉尼亞州\).md "wikilink")，和[64號州際公路](../Page/64號州際公路.md "wikilink")、[美國國道250](../Page/美國國道250.md "wikilink")。

[托马斯·杰斐逊创建的美国第一所公立大学](../Page/托马斯·杰斐逊.md "wikilink")[弗吉尼亚大学位于该市](../Page/弗吉尼亚大学.md "wikilink")。杰斐逊的故居位于夏洛特鎮的[蒙蒂塞洛](../Page/蒙蒂塞洛.md "wikilink")，每年吸引数十万的游客到访。该市附近还有不少美國[南北戰爭时期的战场](../Page/南北戰爭.md "wikilink")。由于这些原因，
這個城市是美國的文化和歷史縮影。

## 歷史

於1762年，貿易商來到了這地，他們沿著三道缺口（現今的[美國國道250](../Page/美國國道250.md "wikilink")）從[里奇蒙到](../Page/里奇蒙.md "wikilink")[丹維爾](../Page/丹維爾.md "wikilink")。它的命名是由[喬治三世的妻子](../Page/喬治三世.md "wikilink")[夏綠蒂王后來命名的](../Page/夏綠蒂王后.md "wikilink")。在這期間[美國獨立戰爭](../Page/美國獨立戰爭.md "wikilink")，1779年到1781年之間，軍隊被監禁在夏律第鎮的[阿爾伯馬爾營房在](../Page/阿爾伯馬爾營房.md "wikilink")。

自十九世紀末，此城的人口始見增長，由原來二千多人，增加至1910年的六千多人；而在1910-20年間，人口首次破萬，增長達58%。

### 著名事件

  - [2017年团结右翼集会](../Page/2017年团结右翼集会.md "wikilink")

## 經濟

按2013年鎮政府統計\[1\]，該鎮首五大僱主別為：

  - 維珍尼亞醫科大學：1000多人
  - Martha Jefferson醫院：1000多人
  - 夏律第鎮政府：1000多人
  - 夏律第鎮校區：500-900多人
  - Aramark物流公司：250-499人

## 體育

夏律第鎮沒有職業體育隊，而是大學體育隊，[維吉尼亞騎士](../Page/維吉尼亞騎士.md "wikilink")。維吉尼亞騎士體育領域從足球到籃球。維吉尼亞騎士的[橄欖球季在學年期間吸引大量的觀眾](../Page/橄欖球.md "wikilink")，橄欖球賽在[史考特體育場](../Page/史考特體育場.md "wikilink")。

## 教育

[2008-0830-Charlottesville-DowntownMall.jpg](https://zh.wikipedia.org/wiki/File:2008-0830-Charlottesville-DowntownMall.jpg "fig:2008-0830-Charlottesville-DowntownMall.jpg")
夏律第鎮由夏律第鎮公立學校服務。學校系統有六所[小學](../Page/小學.md "wikilink")、Buford[中學](../Page/中學.md "wikilink")，和夏律第鎮高中。區域私立學校包括夏律第鎮天主教學校、夏律第鎮Waldorf
學校、契約學校、夏律第鎮領域學校,阿爾伯馬爾米勒學校,Montessori
社區學校，北部分支學校，皮博迪學校，新生學校，聖安妮-Belfield
學校，縱排朋友學校，村莊學校，維吉尼亞學院。阿爾伯馬爾縣公立學校系統在夏律第鎮市區範圍外面。

## 交通

夏律第鎮由[夏律第阿爾伯馬爾機場和](../Page/夏律第阿爾伯馬爾機場.md "wikilink")[夏律第車站駐地](../Page/夏律第車站.md "wikilink")，和[灰狗巴士城市間公共汽車總站服務](../Page/灰狗巴士.md "wikilink")。城市間的公共汽車服務是由星光快運提供。夏律第鎮由JAUNT提供區域公共汽車服務。通過夏律第鎮的高速公路為[64號州際公路](../Page/64號州際公路.md "wikilink"),
它是東西向路線，另有[美國國道250](../Page/美國國道250.md "wikilink")，和南北向的[美國國道29](../Page/美國國道29.md "wikilink")。

## 參考文獻

## 外部链接

  - [Official City Government website](http://www.charlottesville.org/)
  - [Charlottesville, A Brief Urban
    History](http://www3.iath.virginia.edu/schwartz/cville/cville.history.html)
  - [Albemarle Charlottesville Historical Society Online
    Exhibits](https://web.archive.org/web/20080509071959/http://albemarlehistory.org/on-line_exhibits.htm)

[Monticello_reflected.JPG](https://zh.wikipedia.org/wiki/File:Monticello_reflected.JPG "fig:Monticello_reflected.JPG")

[C](../Category/弗吉尼亚州城市.md "wikilink")

1.