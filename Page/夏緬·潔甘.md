**夏緬·潔甘**（**Charmaine
Dragun**，）\[1\]曾是[澳大利亞](../Page/澳大利亞.md "wikilink")[第十頻道的新聞主播](../Page/十號電視網.md "wikilink")，她生前主持過該頻道於[珀斯的五點鐘地區新聞節目](../Page/珀斯.md "wikilink")。

## 出生

夏緬1978年生于[西澳首府](../Page/西澳大利亞州.md "wikilink")[伯斯](../Page/珀斯.md "wikilink")。

## 廣播事業

夏緬以優異成績畢業于[伊迪斯科文大學](../Page/伊迪斯科文大學.md "wikilink")（Edith Cowan
University）之附屬[西澳表演藝術學院](../Page/西澳表演藝術學院.md "wikilink")（Western
Australian Academy of Performing
Arts；WAAPA），隨後在珀斯的6PR和96FM電臺擔任記者\[2\]；工作表現十分出色，曾經被提名為澳洲的「年度最佳年輕記者」。

她於2002年加盟第十頻道，起初主要報導司法新聞，其後分別於2003年和2004年的夏季擔任後備主播。從2005年7月起主持西澳的五點鐘地區新聞節目\[3\]。由於第十頻道的地區新聞是從位於[悉尼的新聞中心製作的](../Page/悉尼.md "wikilink")，因此於這段時期她定居於悉尼。

夏緬十分積極參與公益事業，曾大力為西澳的乳癌症患者募捐\[4\]。此外，她也十分關心祖國[克羅地亞](../Page/克羅地亞.md "wikilink")；曾于2007年7月返鄉觀光歷史景點並以第一人稱撰寫文章、於7月22日發表在《[悉尼晨鋒報](../Page/悉尼晨鋒報.md "wikilink")》（Sydney
Morning Herald）上\[5\]。

## 自殺

2007年11月2日（悉尼時間）下午4時左右，夏緬縱身跳下位于東悉尼著名的[斷魂谷](../Page/断魂谷_\(新南威尔士州\).md "wikilink")（The
Gap），因[憂鬱症自我結束生命](../Page/憂鬱症.md "wikilink")，\[6\]\[7\]\[8\]得年29歲。

第十頻道台長[Grant
Blackley先生在敘述此事時說](../Page/:en:Grant_Blackley.md "wikilink")：「夏緬是個智慧超群、充滿活力和關心他人的人，同事們無不愛慕著她，我們謹對她的伴侶Simon和家庭表示最衷心的同情和哀悼。」（原文：Charmaine
was a highly intelligent, vibrant and caring person, universally liked
and admired by her colleagues. Our deepest sympathies go to her partner,
Simon, and her family.）\[9\]

## 參考

[Category:澳大利亚主持人](../Category/澳大利亚主持人.md "wikilink")
[Category:澳洲自殺者](../Category/澳洲自殺者.md "wikilink")
[Category:澳大利亚电视记者](../Category/澳大利亚电视记者.md "wikilink")
[Category:女性自杀者](../Category/女性自杀者.md "wikilink")
[Category:克罗地亚裔澳大利亚人](../Category/克罗地亚裔澳大利亚人.md "wikilink")
[Category:伯斯人](../Category/伯斯人.md "wikilink")
[Category:自杀记者](../Category/自杀记者.md "wikilink")
[Category:情感障碍患者](../Category/情感障碍患者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.