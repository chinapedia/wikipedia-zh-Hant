《**外星神犬**》（），由[Jim Henson
Pictures和](../Page/Jim_Henson_Pictures.md "wikilink")[米高梅公司制作的较为成功的影片](../Page/米高梅公司.md "wikilink")，影片讲述来自外星的狗的故事。

影片的主角欧文·贝克由[连姆·艾肯饰演](../Page/连姆·艾肯.md "wikilink")，影片中分别由[馬修·波特歷](../Page/馬修·波特歷.md "wikilink"),
[Delta Burke](../Page/Delta_Burke.md "wikilink"), [Donald
Faison](../Page/Donald_Faison.md "wikilink"), [Cheech
Marin](../Page/Cheech_Marin.md "wikilink"),
[布蘭妮·墨菲](../Page/布蘭妮·墨菲.md "wikilink"),
[凡妮莎·蕾格烈芙](../Page/凡妮莎·蕾格烈芙.md "wikilink"), [Carl
Reiner](../Page/Carl_Reiner.md "wikilink"), [Keri Jo
Chapman和](../Page/Keri_Jo_Chapman.md "wikilink")[Daniel
Joseph给狗配音](../Page/Daniel_Joseph.md "wikilink")。

影片讲述的是高智能的外星犬曾派狗到地球，并打算获得对地球的统治权。但不料却发现到达地球的狗成为了人类的宠物。电影中的主角狗，哈勃由[馬修·波特歷配音](../Page/馬修·波特歷.md "wikilink")。

影片改编自[泽克·查理森的](../Page/泽克·查理森.md "wikilink")《[狗从外星来](../Page/狗从外星来.md "wikilink")》（*Dogs
from Outer
Space*）。本片导演[约翰·罗伯特·霍夫曼和](../Page/约翰·罗伯特·霍夫曼.md "wikilink")[泽克·查理森联合将其改编成剧本](../Page/泽克·查理森.md "wikilink")。

《外星神犬》与《[-{zh-hans:杀死比尔;zh-hk:標殺令;zh-tw:追殺比爾;}-](../Page/杀死比尔.md "wikilink")》在同一档期上映，以至于[昆廷·塔伦蒂诺开玩笑说](../Page/昆廷·塔伦蒂诺.md "wikilink")：孩子们应该拿着*《外星神犬》*的票去看《[-{zh-hans:杀死比尔;zh-hk:標殺令;zh-tw:追殺比爾;}-](../Page/杀死比尔.md "wikilink")》。

## 外部链接

  -
[Category:2003年电影](../Category/2003年电影.md "wikilink")
[Category:美国电影作品](../Category/美国电影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美国喜剧科幻片](../Category/美国喜剧科幻片.md "wikilink")
[Category:2000年代喜剧片](../Category/2000年代喜剧片.md "wikilink")
[Category:2000年代科幻片](../Category/2000年代科幻片.md "wikilink")
[Category:人与外星生物接触题材电影](../Category/人与外星生物接触题材电影.md "wikilink")
[Category:狗电影](../Category/狗电影.md "wikilink")
[Category:拟人化角色电影](../Category/拟人化角色电影.md "wikilink")
[Category:米高梅电影](../Category/米高梅电影.md "wikilink")