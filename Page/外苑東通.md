[Gaien-Higashi-dori_Street_Roppongi_Tokyo.jpg](https://zh.wikipedia.org/wiki/File:Gaien-Higashi-dori_Street_Roppongi_Tokyo.jpg "fig:Gaien-Higashi-dori_Street_Roppongi_Tokyo.jpg")
**外苑東通**（，Gaienhigashi-dori
Ave.）是[日本](../Page/日本.md "wikilink")[東京都一條道路](../Page/東京都.md "wikilink")，由[新目白通交匯的東京都](../Page/新目白通.md "wikilink")[新宿區早稲田鶴巻町交差點開始](../Page/新宿區.md "wikilink")，經[國道1號](../Page/國道1號_\(日本\).md "wikilink")、[櫻田通連接東京都](../Page/櫻田通.md "wikilink")[港區麻布台之飯倉交差點之道路通稱](../Page/港區_\(東京\).md "wikilink")，全線屬[東京都道319號環狀三號線一部份](../Page/東京都道319號環狀三號線.md "wikilink")。與[外苑西通相同](../Page/外苑西通.md "wikilink")，為[神宮外苑東側通過的](../Page/神宮外苑.md "wikilink")[道路名稱](../Page/道路.md "wikilink")。

## 路線

早稲田鶴巻町
[新目白通](../Page/東京都道8號千代田練馬田無線.md "wikilink")（[東京都道8號千代田練馬田無線](../Page/東京都道8號千代田練馬田無線.md "wikilink")）
　　|
辯天町 [早稲田通](../Page/早稲田通.md "wikilink")
　　|
市谷柳町
[大久保通](../Page/東京都道433號神樂坂高圓寺線.md "wikilink")（[東京都道433號神樂坂高圓寺線](../Page/東京都道433號神樂坂高圓寺線.md "wikilink")）
　　|
[合羽坂](../Page/合羽坂.md "wikilink")
[靖國通](../Page/靖國通.md "wikilink")（側道）（[東京都道302號新宿兩國線](../Page/東京都道302號新宿兩國線.md "wikilink")）
　　|
[曙橋](../Page/曙橋.md "wikilink")
（立體交差）[靖國通](../Page/靖國通.md "wikilink")（本線）（[東京都道302號新宿兩國線](../Page/東京都道302號新宿兩國線.md "wikilink")）
　　|
四谷3丁目
[新宿通](../Page/新宿通.md "wikilink")（[國道20號](../Page/國道20號.md "wikilink")）
　　|
權田原 都道414號線
　　|
青山1丁目
[青山通](../Page/青山通.md "wikilink")（[國道246號](../Page/國道246號.md "wikilink")）
　　|
青山Twin大廈裏 [環三通](../Page/環三通.md "wikilink")
　　|
乃木坂 [赤坂通](../Page/赤坂通.md "wikilink")
　　|
六本木
[六本木通](../Page/六本木通.md "wikilink")（[東京都道412號霞關澀谷線](../Page/東京都道412號霞關澀谷線.md "wikilink")）
　　|
飯倉片町 放射1號線
　　|
飯倉
[櫻田通](../Page/櫻田通.md "wikilink")（[國道1號](../Page/國道1號.md "wikilink")）

## 與外苑東通連接的路線、車站

  - [曙橋站](../Page/曙橋站.md "wikilink")（[都營地下鐵新宿線](../Page/都營地下鐵新宿線.md "wikilink")）
  - [四谷三丁目站](../Page/四谷三丁目站.md "wikilink")（[東京地下鐵丸之內線](../Page/東京地下鐵丸之內線.md "wikilink")）
  - [信濃町站](../Page/信濃町站.md "wikilink")（[JR](../Page/東日本旅客鐵道.md "wikilink")、[中央・總武緩行線](../Page/中央、總武緩行線.md "wikilink")）
  - [青山一丁目站](../Page/青山一丁目站.md "wikilink")（[東京地下鐵銀座線](../Page/東京地下鐵銀座線.md "wikilink")、[半藏門線](../Page/半藏門線.md "wikilink")）
  - [乃木坂站](../Page/乃木坂站.md "wikilink")（[東京地下鐵千代田線](../Page/東京地下鐵千代田線.md "wikilink")）
  - [六本木站](../Page/六本木站.md "wikilink")（[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")、[都營地下鐵大江戶線](../Page/都營地下鐵大江戶線.md "wikilink")）

## 相關條目

  - [外苑西通](../Page/外苑西通.md "wikilink")

  -
  - [關東地方道路列表](../Page/關東地方道路列表.md "wikilink")

[Gaienhigashi](../Category/東京都道路.md "wikilink")