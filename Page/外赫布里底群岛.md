[Na_h-Eileanan_Siarcouncil.PNG](https://zh.wikipedia.org/wiki/File:Na_h-Eileanan_Siarcouncil.PNG "fig:Na_h-Eileanan_Siarcouncil.PNG")

**外赫布里底群岛**（），又称**西部群岛**（Western Isles）、**长岛**（Long
Island）或**埃利安锡尔**（Eilean
Siar），是[英国](../Page/英国.md "wikilink")[苏格兰](../Page/苏格兰.md "wikilink")32个一级行政区之一。面积3071平方公里，2013年中估計人口27,400\[1\]。主要城镇是[斯托诺韦](../Page/斯托诺韦.md "wikilink")。

外赫布里底群岛与[大不列颠岛和内赫布里底岛之间有风雨多变的](../Page/大不列颠岛.md "wikilink")[明奇海峡和](../Page/明奇海峡.md "wikilink")[小明奇海峡阻隔](../Page/小明奇海峡.md "wikilink")，是整个英国乃至欧洲交通最为不便的地区之一。人口稀少，平均每平方公里仅有9人。

[Western_Isles_Council_Flag.svg](https://zh.wikipedia.org/wiki/File:Western_Isles_Council_Flag.svg "fig:Western_Isles_Council_Flag.svg")
这些岛屿最早在公元9世纪之前就有北欧的[诺斯人定居](../Page/诺斯人.md "wikilink")，经过几个世纪的纷争，在公元13世纪最终归属[苏格兰王国](../Page/苏格兰王国.md "wikilink")。直到今天，岛屿的政治、文化等与[大不列颠岛上仍有很大差异](../Page/大不列颠岛.md "wikilink")，英国和苏格兰各大[政党在当地基本毫无影响力](../Page/政党.md "wikilink")，地方政府内绝大多数是独立和无党派人士。

地区内的主要岛屿是[刘易斯岛和哈里斯岛](../Page/刘易斯岛.md "wikilink")。但是虽然地名分离，其实却是同一个岛屿。刘易斯岛在北部，而哈里斯岛在南部。

## 参见

  - [苏格兰巴拉机场](../Page/苏格兰巴拉机场.md "wikilink")

## 注釋

## 參考文獻

## 外部链接

  - [外赫布里底群岛地方议会](http://www.cne-siar.gov.uk/)

[E](../Category/蘇格蘭行政區劃.md "wikilink")

1.