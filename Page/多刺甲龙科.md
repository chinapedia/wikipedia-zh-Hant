**多刺甲龍亞科**（學名：Polacanthinae）或是[甲龍下目](../Page/甲龍下目.md "wikilink")[甲龍科的一個亞科](../Page/甲龍科.md "wikilink")，生存於[侏儸紀晚期到](../Page/侏儸紀.md "wikilink")[白堊紀早期](../Page/白堊紀.md "wikilink")。多年來，其分類仍有爭議，根據不同的研究人員，多刺甲龍亞科被歸類於[結節龍科或甲龍科](../Page/結節龍科.md "wikilink")，或是獨立建立為**多刺甲龍科**（Polacanthidae）。

## 敘述

多刺甲龍亞科的化石發現於[北美洲與](../Page/北美洲.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，較常出現在北美洲。第一個發現的多刺甲龍亞科是[林龍](../Page/林龍.md "wikilink")，在1832年發現於[英國](../Page/英國.md "wikilink")[薩塞克斯的](../Page/薩塞克斯.md "wikilink")[蒂爾蓋特](../Page/蒂爾蓋特.md "wikilink")[森林](../Page/森林.md "wikilink")。在1865年，[多刺甲龍發現於英國](../Page/多刺甲龍.md "wikilink")。在1898年，[美國](../Page/美國.md "wikilink")[南達科他州發現了](../Page/南達科他州.md "wikilink")[裝甲龍的化石](../Page/裝甲龍.md "wikilink")\[1\]。之後在北美洲發現了其他的多刺甲龍類化石，這些原始的恐龍，有助於了解甲龍類的演化。

多刺甲龍亞科生存於侏儸紀晚期到白堊紀早期，存在了將近3000萬年。[詹姆士·柯克蘭](../Page/詹姆士·柯克蘭.md "wikilink")（James
Kirkland）提出，多刺甲龍亞科的存在時期，北美洲與亞洲之間有陸橋連接\[2\]。

多刺甲龍亞科缺乏甲龍科特有的尾槌，但背部的長刺可用來抵抗掠食動物。牠們與其他甲龍類的差異在於具有大片的薦部盾甲（位於[薦骨上方](../Page/薦骨.md "wikilink")），上有多排的瘤狀物。

與[結節龍科](../Page/結節龍科.md "wikilink")、[甲龍科相比](../Page/甲龍科.md "wikilink")，多刺甲龍亞科的鱗甲較輕型。與結節龍科相比，牠們的背部尖刺較薄、較小型。研究人員推測，多刺甲龍類的鱗甲主要在視覺辨識功能，身體防護是次要功能\[3\]。

多刺甲龍亞科之中，體型最大的是林龍，身長可達6公尺；最小的是[怪嘴龍與](../Page/怪嘴龍.md "wikilink")[邁摩爾甲龍](../Page/邁摩爾甲龍.md "wikilink")，身長2.7到3公尺，牠們同時也是最小的甲龍類恐龍之一。體重介於1到3公噸之間\[4\]。牠們可能是群居動物。

## 分類

在1911年，George R.
Wieland建立多刺甲龍科，以包含無法歸類於[甲龍科](../Page/甲龍科.md "wikilink")、[結節龍科的甲龍類恐龍](../Page/結節龍科.md "wikilink")。

傳統上，甲龍下目可分為[結節龍科與甲龍科](../Page/結節龍科.md "wikilink")。結節龍科具有狹窄的頭部，缺乏尾槌，肩膀處有顯目的大型尖刺；甲龍科的頭部寬廣，具有尾槌。多刺甲龍亞科同時具有結節龍科與甲龍科的特徵，頭部類似甲龍科，身體類似結節龍科。由於多刺甲龍亞科大多破碎，或頭部骨頭殘缺不全，造成分類上的困難。

根據不同的研究人員，多刺甲龍亞科曾被歸類於甲龍科。例如在1998年，詹姆士·柯克蘭將其修改為多刺甲龍亞科，是甲龍科的一個亞科，包含：[多刺甲龍](../Page/多刺甲龍.md "wikilink")、[加斯頓龍](../Page/加斯頓龍.md "wikilink")、[邁摩爾甲龍](../Page/邁摩爾甲龍.md "wikilink")\[5\]\[6\]\[7\]\[8\]\[9\]，或是[結節龍科](../Page/結節龍科.md "wikilink")\[10\]。

在2001年，[肯尼思·卡彭特](../Page/肯尼思·卡彭特.md "wikilink")（Kenneth
Carpenter）在一個甲龍下目的[系統發生學研究中](../Page/系統發生學.md "wikilink")，再將多刺甲龍亞科修改為多刺甲龍科，親緣關係較接近甲龍科，離結節龍科較遠；並將多刺甲龍科定義為：包含所有接近加斯頓龍，而離[埃德蒙頓甲龍](../Page/埃德蒙頓甲龍.md "wikilink")、[包頭龍較遠的所有物種](../Page/包頭龍.md "wikilink")。他認為這些甲龍類恐龍的身體構造相當特殊。甲板接近三角形，而且薄。甲板的排列與牙齒相當原始\[11\]。但有其他研究人員提出異議。

M.
Vickaryous等人在2004年的研究，則將[怪嘴龍與](../Page/怪嘴龍.md "wikilink")[加斯頓龍列為原始的甲龍科](../Page/加斯頓龍.md "wikilink")，而其餘四屬則是甲龍下目的未定屬\[12\]。

許多近年研究將多刺甲龍亞科視為一群原始甲龍科恐龍，但大部分並沒有經過詳細研究證實。在2012年的一份多刺甲龍亞科詳細研究，提出牠們不是原始甲龍科的[並系群](../Page/並系群.md "wikilink")，也不是結節龍科的一個亞科\[13\]。

  - [甲龍科](../Page/甲龍科.md "wikilink") Ankylosauridae
      - [甲龍亞科](../Page/甲龍亞科.md "wikilink") Ankylosaurinae
      - **多刺甲龍亞科** *Polacanthinae*
          - [怪嘴龍](../Page/怪嘴龍.md "wikilink") *Gargoyleosaurus*
          - [加斯頓龍](../Page/加斯頓龍.md "wikilink") *Gastonia*
          - [裝甲龍](../Page/裝甲龍.md "wikilink") *Hoplitosaurus*
          - [林龍](../Page/林龍.md "wikilink") *Hylaeosaurus*
          - [邁摩爾甲龍](../Page/邁摩爾甲龍.md "wikilink") *Mymoorapelta*
          - [多刺甲龍](../Page/多刺甲龍.md "wikilink") *Polacanthus*

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - <http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_name=Gastonia&is_real_user=1>

  - [多刺甲龍科 - The Tree of Life Web
    Project](http://tolweb.org/Polacanthidae/15774)

  - [Detalles
    多刺甲龍亞科的定義與歷史](https://web.archive.org/web/20080405194947/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=257)

[多刺甲龍亞科](../Category/多刺甲龍亞科.md "wikilink")

1.  Lucas, F.A. (1901). A new dinosaur, Stegosaurus marshi, from the
    Lower Cretaceous of South Dakota. Proceedings of the United States
    National Museum 23(1224):591-592.
2.  Kirkland, J. I. (1996). Biogeography of western North America's
    mid-Cretaceous faunas - losing European ties and the first great
    Asian-North American interchange. J. Vert. Paleontol. 16 (Suppl. to
    3): 45A
3.  Hayashi, S., Carpenter, K., Scheyer, T.M., Watabe, M. and Suzuki. D.
    (2010). "Function and evolution of ankylosaur dermal armor." *Acta
    Palaeontologica Polonica*, **55**(2): 213-228.
4.  J. Peczkis. 1995. Implications of body-mass estimates for dinosaurs.
    Journal of Vertebrate Paleontology 14(4):520-533
5.  K. Carpenter, J. I. Kirkland, C. Miles, K. Cloward, and D. Burge.
    1996. Evolutionary significance of new ankylosaurs (Dinosauria) from
    the Upper Jurassic and Lower Cretaceous, Western Interior. Journal
    of Vertebrate Paleontology 16(3, suppl.):25A
6.  K. Carpenter and J. I. Kirkland. 1998. Review of Lower and middle
    Cretaceous ankylosaurs from North America. Lower and Middle
    Cretaceous Terrestrial Ecosystems, S. G. Lucas, J. I. Kirkland & J.
    W. Estep (eds.). New Mexico Museum of Natural History and Science
    Bulletin 14:249-270
7.  J. I. Kirkland. 1998. A polacanthine ankylosaur (Ornithischia:
    Dinosauria) from the Early Cretaceous (Barremian) of eastern Utah.
    Lower and Middle Cretaceous Terrestrial Ecosystems (S. G. Lucas, J.
    I. Kirkland, & J. W. Estep, eds.), New Mexico Museum of Natural
    History and Science Bulletin 14:271-281
8.  K. Carpenter, J. I. Kirkland, D. Burge and J. Bird. 1999.
    Ankylosaurs (Dinosauria: Ornithischia) of the Cedar Mountain
    Formation, Utah, and their stratigraphic distribution. Vertebrate
    Paleontology in Utah, D. D. Gillette (ed.), Utah Geological Survey
    Miscellaneous Publication 99-1:243-251
9.  W. T. Blows. 2001. Dermal armor of the polacanthine dinosaurs. In K.
    Carpenter (ed.), The Armored Dinosaurs. Indiana University Press,
    Bloomington 363-385
10. X. Pereda Suberbiola and P. M. Galton. 2001. Reappraisal of the
    nodosaurid ankylosaur Struthiosaurus austriacus Bunzel from the
    Upper Cretaceous Gosau Beds of Austria. In K. Carpenter (ed.), The
    Armored Dinosaurs. Indiana University Press, Bloomington 173-210
11.
12. Matthew K. Vickaryous, Teresa Maryańska und David B. Weishampel:
    *Ankylosauria*. In: David Weishampel, Peter Dodson und Halszka
    Osmólska (Hrsg.): *The Dinosauria*. University of California Press,
    2004. ISBN 0520242092; S. 363-392.
13. Thompson, R.S., Parish, J.C., Maidment, S.C.R. and Barrett, P.M.
    (2012). "Phylogeny of the ankylosaurian dinosaurs (Ornithischia:
    Thyreophora)." *Journal of Systematic Palaeontology*, **10**(2):
    301-312.