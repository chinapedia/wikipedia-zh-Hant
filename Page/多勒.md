**多勒**（）在[苏格兰](../Page/苏格兰.md "wikilink")[克拉克曼南郡](../Page/克拉克曼南郡.md "wikilink")[奥克尔山南麓](../Page/奥克尔山.md "wikilink")。这里19世纪是一个村，现在是镇，多勒镇中有德雯河流过。现有A91号马路过镇，多勒镇东8公里有行雷桥（）。多勒镇西行6公里有[蒂利库特-{里}-镇](../Page/蒂利库特里.md "wikilink")（），再往西5公里有[阿罗威镇](../Page/阿罗威.md "wikilink")（）。多勒镇的产业在18世纪以[铜矿](../Page/铜.md "wikilink")、[铅矿](../Page/铅.md "wikilink")、[铁矿为主](../Page/铁矿.md "wikilink")，19世纪以[纺织业为主](../Page/纺织业.md "wikilink")。

## 多勒镇特色

  - 奥克尔山。在多勒镇北，分东西二峰，最高的本克鲁峰(Ben Cleuch)有721米高，两山联合处架有长桥，瀑布奔腾而下成山涧。
  - 坎伯古堡（）。位于多勒镇北的奥克尔山多勒幽谷，为14、15世纪阿该尔诸侯所建，16世纪时苏格兰的[玛丽女王曾在此住过](../Page/玛丽一世_\(苏格兰\).md "wikilink")。

[RumblingPark.jpg](https://zh.wikipedia.org/wiki/File:RumblingPark.jpg "fig:RumblingPark.jpg")
绘）\]\]

  - 行雷桥（Rumbling Bridge）在镇东。“桥下泉水之喧若雷”——王韬
  - 多勒学院。

## 名人

1.  [苏格兰](../Page/苏格兰.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")[理雅各的老家在多勒](../Page/理雅各.md "wikilink")。

[Dolllartown.jpg](https://zh.wikipedia.org/wiki/File:Dolllartown.jpg "fig:Dolllartown.jpg")

1.  [中国早期改良派作家](../Page/中国.md "wikilink")[王韬曾在](../Page/王韬.md "wikilink")1870年至1872年侨居此地，协助理雅各翻译中国古典书籍。王韬著有《畅游灵囿》、《多勒游山》两篇游记，描写多勒风景。\[1\]
2.  [英国](../Page/英国.md "wikilink")[浪漫主义画家](../Page/浪漫主义.md "wikilink")[约瑟夫·玛罗德·威廉·透纳曾来多勒描绘风景](../Page/约瑟夫·玛罗德·威廉·透纳.md "wikilink")。
3.  [世界语创建人](../Page/世界语.md "wikilink")[威廉·奥德曾在多勒居住](../Page/威廉·奥德.md "wikilink")。
4.  [杜瓦瓶发明人](../Page/杜瓦瓶.md "wikilink")[杜瓦出自多勒学院](../Page/杜瓦.md "wikilink")。

## 参考文献

<div class="references-small">

<references/>

</div>

## 外部链接

  - [多勒镇](http://www.multimap.com/map/browse.cgi?client=public&X=295000.959990039&Y=700000.516061862&width=700&height=400&gride=295921.959990039&gridn=698071.516061862&srec=0&coordsys=gb&db=freegaz&addr1=&addr2=&addr3=&pc=&advanced=&local=&localinfosel=&kw=&inmap=&table=&ovtype=&zm=0&scale=100000&down.x=289&down.y=4)

[Category:克拉克曼南郡](../Category/克拉克曼南郡.md "wikilink")

1.  《漫游随录图记》，ISBN 7-80603-956-2