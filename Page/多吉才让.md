**多吉才让**（，），[藏族](../Page/藏族.md "wikilink")，[甘肅](../Page/甘肃省.md "wikilink")[夏河人](../Page/夏河县.md "wikilink")，藏文高中學歷。1955年8月參加
工作。1960年10月加入[中国共产党](../Page/中国共产党.md "wikilink")。曾任[西藏自治区人民政府主席](../Page/西藏自治区人民政府主席.md "wikilink")、[民政部部長](../Page/中华人民共和国民政部.md "wikilink")、[全國人大民族委員會主任委員等職務](../Page/全国人民代表大会民族委员会.md "wikilink")。

## 简介

1983年任[西藏自治区人民政府副主席](../Page/西藏自治区人民政府.md "wikilink")。1985年任中共西藏自治區黨委副書記。1986年任西藏自治區人民政府代主席、主席。1990年5月任民政部副部長。1993年3月[第八届全国人民代表大会第一次会议決定任命為民政部部長](../Page/第八届全国人民代表大会.md "wikilink")，成為歷來第一位擔任正部長級官員的藏人。1998年3月[第九届全国人民代表大会第一次会议再次連任民政部部長](../Page/第九届全国人民代表大会.md "wikilink")。1998年7月任[中國國際減災十年委員會副主任](../Page/中國國際減災十年委員會.md "wikilink")。2000年10月任中國國際減災十年委員會主任。2003年3月任第十屆[全國人大民族委員會主任委員](../Page/全国人民代表大会民族委员会.md "wikilink")。

中共第十二屆[中央纪委委員](../Page/中国共产党中央纪律检查委员会.md "wikilink")，第十三屆、十四屆、十五屆、十六屆[中共中央委員](../Page/中国共产党中央委员会.md "wikilink")，第十屆[全國人大常委會委員](../Page/全国人民代表大会常务委员会.md "wikilink")。

{{-}}

[Category:中华人民共和国民政部部长](../Category/中华人民共和国民政部部长.md "wikilink")
[Category:第十届全国人大常委会委员](../Category/第十届全国人大常委会委员.md "wikilink")
[Category:西藏自治區全國人民代表大會代表](../Category/西藏自治區全國人民代表大會代表.md "wikilink")
[Category:藏族全國人大代表](../Category/藏族全國人大代表.md "wikilink")
[Category:西藏自治區人民政府主席](../Category/西藏自治區人民政府主席.md "wikilink")
[Category:西藏自治區人民政府副主席](../Category/西藏自治區人民政府副主席.md "wikilink")
[Category:中国共产党第十三届中央委员会委员](../Category/中国共产党第十三届中央委员会委员.md "wikilink")
[Category:中国共产党第十四届中央委员会委员](../Category/中国共产党第十四届中央委员会委员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中共西藏自治區黨委副書記](../Category/中共西藏自治區黨委副書記.md "wikilink")
[Category:中华人民共和国时期藏族中国共产党党员](../Category/中华人民共和国时期藏族中国共产党党员.md "wikilink")
[Category:中华人民共和国藏族高级干部](../Category/中华人民共和国藏族高级干部.md "wikilink")
[Category:藏族人](../Category/藏族人.md "wikilink")
[Category:夏河人](../Category/夏河人.md "wikilink")