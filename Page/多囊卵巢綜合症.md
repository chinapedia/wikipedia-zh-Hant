**多囊性卵巢綜合症**（，簡稱**PCOS**），又稱**斯-李二氏症**（），是一連串女性因為[雄性激素上升所導致的症狀](../Page/雄性激素.md "wikilink")\[1\]。多囊性卵巢的症狀包含[月經不規律或是無](../Page/月經.md "wikilink")[月經](../Page/月經.md "wikilink")、[月經量過多](../Page/Menorrhagia.md "wikilink")、[多毛症](../Page/多毛症.md "wikilink")、[粉刺](../Page/粉刺.md "wikilink")、盆腔疼痛、[難以受孕與](../Page/不孕.md "wikilink")[黑棘皮症](../Page/黑棘皮症.md "wikilink")\[2\]。相關的病症包含[第二型糖尿病](../Page/第二型糖尿病.md "wikilink")、[肥胖症](../Page/肥胖症.md "wikilink")、[阻塞性睡眠呼吸暫停](../Page/阻塞性睡眠呼吸暫停.md "wikilink")、[心血管疾病](../Page/心血管疾病.md "wikilink")、[情感障礙與](../Page/情感障礙.md "wikilink")[子宮內膜癌](../Page/子宮內膜癌.md "wikilink")\[3\]。

多囊性卵巢會受基因遺傳與環境因素影響\[4\]\[5\]。其危險因子包含[肥胖症](../Page/肥胖症.md "wikilink")、運動量不足或是有家族病史\[6\]。如果有以下三種症狀中的兩種便可診斷患者有多囊性卵巢：無排卵、雄性激素過高與卵巢囊腫\[7\]。囊腫可以由[超音波影像檢測](../Page/超音波.md "wikilink")
。其他造成類似症狀的疾病包含[先天性腎上腺增生症](../Page/先天性腎上腺增生症.md "wikilink")，[甲狀腺機能低下症與](../Page/甲狀腺機能低下症.md "wikilink")\[8\]。

多囊性卵巢綜合症目前並無特效藥可以治療\[9\]。現行治療方式包括減重和運動等生活型態調整\[10\]\[11\]
，[避孕藥物對於調整經期](../Page/避孕藥物.md "wikilink")、抑制多餘的毛髮生長與改善青春痘有所幫助
。[二甲双胍和](../Page/二甲双胍.md "wikilink")[抗雄性激素可能能夠改善多囊性卵巢綜合症](../Page/抗雄性激素.md "wikilink")
，且針對青春痘和多毛等症狀的症狀治療有一定效果\[12\]。減重或是使用[可洛米分](../Page/可洛米分.md "wikilink")、降血糖藥物[每福敏則有助於改善不孕的狀況](../Page/每福敏.md "wikilink")
。若以上治療對於病人都沒有效果且病人有生育考量，則可考慮[體外人工授精這個選項](../Page/體外人工授精.md "wikilink")\[13\]。

多囊性卵巢綜合症是18歲到44歲女性間最常見的\[14\]。一般認為，多囊卵巢綜合症的發生率，在女性生育年齡期間佔約百分之二十（根據鹿特丹診斷指引，英國為百分之26、澳洲為百分之17.8、土耳其為百分之19.9、伊朗為百分之15.2\[15\]。）多囊卵巢綜合症是現今導致不孕的主要原因之一\[16\]。目前已知最早的多囊卵巢綜合症病例於1721年紀錄於義大利\[17\]。

## 體徵及症狀

常見的體徵和症狀如下：

  - [月經失調](../Page/月經失調.md "wikilink")：
    多囊卵巢綜合症主要導致月經次數過少（一年月經次數少於九次）或[閉經](../Page/閉經.md "wikilink")（沒有月經連續三到數個月以上），也可能造成其他類型的月經失調\[18\]\[19\]。
  - [不孕](../Page/不孕.md "wikilink")\[20\]： 這是一個由慢性導致的常見症狀 (缺乏排卵)\[21\]。
  - 雄性激素分泌過多：
    即為。最常見的症狀為[痤瘡和](../Page/痤瘡.md "wikilink")[多毛症](../Page/多毛症.md "wikilink")，也可能導致[月經過多](../Page/月經過多.md "wikilink")
    （週期長且量多的月經週期），  （增加毛髮脫落程度及瀰漫性脫髮）， 或其他症狀\[22\]\[23\]。
    約有四分之三的多囊卵巢綜合症女性患者 （根據 NIH/NICHD
    1990的診斷標準)）有出現的症狀\[24\]。
  - [代謝症候群](../Page/代謝症候群.md "wikilink")：\[25\]有[肚腩贅肉且出現與](../Page/肚腩贅肉.md "wikilink")[胰島素抗性有關的症狀](../Page/胰島素抗性.md "wikilink")\[26\]。
    罹患多囊卵巢綜合症的女性的血清中[胰島素與升半胱胺酸濃度較正常女性來的高且有胰島素抗性](../Page/胰島素.md "wikilink")\[27\]。

## 原因

多囊卵巢綜合症為一種成因不明的異質性失調。\[28\]\[29\]\[30\] 有一些證據如:
家族病例、同卵雙胞胎相比異卵雙胞胎在多囊卵巢綜合症的內分泌和代謝之遺傳性特徵上有較高的一致性等，點出多囊性卵巢綜合症可能為遺傳性疾病\[31\]\[32\]\[33\]。亦有證據表明若胎兒於子宮中暴露於高濃度的[雄性激素與](../Page/雄性激素.md "wikilink")[抗穆氏管荷爾蒙中](../Page/抗穆氏管荷爾蒙.md "wikilink")，隨著年紀增長會提升其患多囊卵巢綜合症的風險。\[34\]

### 基因

致病性的基因為體染色體顯性遺傳，在女性身上具有高度的基因外顯性且多變的表現度；這代表小孩有50%的機率從雙親其中一方獲得可能致病的基因片段，如果獲得此基因片段的為女兒，則會出現一定程度上的病徵\[35\]\[36\]\[37\]\[38\]。
異變基因可能從父母雙方遺傳而來，也可能遺傳給兒女(可能成為帶原者或是早期[脫髮或毛髮過多症狀者](../Page/脫髮.md "wikilink"))
\[39\]\[40\]。

[表現型](../Page/表現型.md "wikilink")：有一部分患者個疾病表現為分泌過多的雄性素
\[41\]。確切的基因影響方式尚未被確認\[42\]\[43\]\[44\]。在少數的案例，單個基因的突變有可能造成綜合性的突變症狀\[45\]。目前對該綜合症的發病病理機致的研究指出，多囊卵巢綜合症為複雜的多基因疾病\[46\]。

多囊卵巢綜合症症狀的嚴重程度似乎主要取決於[肥胖症](../Page/肥胖症.md "wikilink")\[47\]\[48\]\[49\]。

多囊卵巢綜合症可視為一種[代謝疾病](../Page/代謝疾病.md "wikilink")，其的部分症狀為「可逆的」。
即便多囊卵巢綜合症由28個症狀組成，但其仍被視為一種婦科疾病。

即使多囊卵巢綜合症的病名表明卵巢為該疾病的病理核心,但是囊腫為一種症況而非病因。就算兩個卵巢被摘除，部分多囊卵巢綜合症的症狀仍會持續下去，其症狀在不存在囊種的狀況下仍有可能出現。自從1935年Stein和Leventhal首次描述以來，診斷、症狀和致病因素的標準仍為爭議的主題之一。因為卵巢為首要受影響的器官，婦科學者們通常視其為一種婦科疾病。然而，近年來許多觀察顯示多囊性卵巢唯一種多重系統性失調疾病，主要問題源自於[下視丘賀爾蒙調節失調](../Page/下視丘.md "wikilink")
，許多器官也與此調節有關。多囊性卵巢這個名稱源自於[超聲波診斷之影像](../Page/超聲波.md "wikilink")。多囊性卵巢綜合症的症狀非常多變，且只有約百分之15的患者可由超音波影像看出其卵巢中有囊腫。\[50\]

### 環境

多囊卵巢綜合症可能與產前經期、[表觀遺傳學因子](../Page/表觀遺傳學.md "wikilink")、環境影響（尤其是工業中產生的[內分泌干擾素](../Page/內分泌干擾素.md "wikilink")\[51\]如[雙酚Ａ與特定藥物](../Page/雙酚Ａ.md "wikilink")）及肥胖比例增加有關，上述任何原因之一都有可能是使病症惡化的緣由。\[52\]\[53\]\[54\]\[55\]\[56\]\[57\]\[58\]

## 發病機制

多囊性卵巢的發展會刺激卵巢持續製造分泌過量的雄性激素，尤其是睪固酮，通常會伴隨著以下的其中一個症狀或是兩者皆有（幾乎肯定其具有遺傳易受性\[59\]）：

  - 腦下垂體前葉分泌過量的[黃體激素](../Page/黃體激素.md "wikilink")
  - 血液中[胰島素濃度高](../Page/胰岛素.md "wikilink")（[高胰島素血症](../Page/高胰島素血症.md "wikilink")）的女性，[卵巢對於高胰島素濃度刺激敏感](../Page/卵巢.md "wikilink")\[60\]

多囊性卵巢綜合症因其在超音波診斷中普遍可見的大量卵巢囊腫聞名。這些「囊腫」其實是未成熟的[濾泡而非囊腫](../Page/濾泡.md "wikilink")。這些濾泡由初級濾泡發育而成，
但在空腔濾泡期早期因為卵巢功能停止發育，這些濾泡會出現在卵巢周邊，在超音波檢驗的影像中看起來像成串的珍珠。

患有多囊性卵巢症候群的婦女因為下視丘釋放促性腺激素釋放激素的頻率增加，導致黃體成長激素與濾泡刺激素的比值升高\[61\]。

大多數具有PCOS的婦女具有胰島素抵抗或肥胖的症狀。
他們的胰島素濃度異常的提高導致「」區域中的異常並引起PCOS的症狀。高胰島素血症提高[GnRH的釋放頻率](../Page/促性腺激素释放激素.md "wikilink")、黃體成長激素量多過濾泡刺激素，因而佔了主導地位、增加卵巢雄激素的產生、減少濾泡的成熟並減少[SHBG的作用](../Page/性激素结合球蛋白.md "wikilink")\[62\]。此外，過多的胰島素
會透過調高的活性，而17α羥化酶的活性也和雄激素前体的合成有關\[63\]，因此過多胰島素的幾個效果都會提高PCOS的風險\[64\]。胰島素抵抗是女性常見的情形，在正常體重的女性及體重過重的女性都可能會出現\[65\]\[66\]\[67\]。
 Adipose tissue possesses , an enzyme that converts androstenedione to
estrone and testosterone to estradiol. The excess of adipose tissue in
obese women creates the paradox of having both excess androgens (which
are responsible for hirsutism and ) and estrogens (which inhibits FSH
via negative feedback).\[68\]

PCOS may be associated with chronic inflammation,\[69\]\[70\]with
several investigators correlating inflammatory mediators with
anovulation and other PCOS symptoms.\[71\]\[72\]Similarly, there seems
to be a relation between PCOS and increased level of [oxidative
stress](../Page/氧化应激.md "wikilink").\[73\]

It has previously been suggested that the excessive androgen production
in PCOS could be caused by a decreased serum level of , in turn
increasing the level of free [IGF-I](../Page/胰岛素样生长因子1.md "wikilink"),
which stimulates ovarian androgen production, but recent data concludes
this mechanism to be unlikely.\[74\]

目前研究指出多囊性卵巢綜合症與特定的基因型亞型有關. The research suggests that women with
*heterozygous-normal/low* FMR1 have polycystic-like symptoms of
excessive follicle-activity and hyperactive ovarian function.\[75\]

[跨性別男性](../Page/跨性別男性.md "wikilink")
如果選擇服用賀爾蒙來做為性別轉換療程的一環，他們有可能因為體內睪固酮濃度提高，導致出現多囊性卵巢症候群症狀的機率提高。
\[76\]\[77\]

## 診斷

並不是每一個多囊卵巢綜合症的病人都有出現多囊卵巢的症狀，
也並非所有卵巢曩腫的病患都有多囊卵巢綜合症的症狀；雖然[骨盆超聲波](../Page/骨盆超聲波.md "wikilink")
是主要的診斷工具， 但也並不是唯一個診斷工具\[78\]。 雖然該綜合徵與廣泛的症狀相關，最直接的診斷方法是使用鹿特丹診斷標準。

[File:PCOS.jpg|多囊卵巢在超聲波檢查上顯示的圖樣](File:PCOS.jpg%7C多囊卵巢在超聲波檢查上顯示的圖樣)
[File:Polycystic_ovary.jpg|多囊卵巢在陰道超聲波檢查上顯示的圖樣](File:Polycystic_ovary.jpg%7C多囊卵巢在陰道超聲波檢查上顯示的圖樣)
<File:PCO> polycystic ovary.jpg|多囊卵巢在超聲波檢查上顯示的圖樣

### 定義

以下是兩種常見的定義：

#### 美國國立衛生研究院診斷標準

  -
    在1990年，由[國立衛生研究院 (美國)](../Page/國立衛生研究院_\(美國\).md "wikilink")/
    贊助的協商研討會提出，如果一個人具有以下所有症狀，表示該員罹患多囊卵巢綜合症\[79\]:

<!-- end list -->

1.  出現[排卵不規則的症狀](../Page/排卵不規則.md "wikilink")
2.  出現的症狀 （臨床上或生化上）
3.  排除可能導致月經不規則和雄激素過多的其他疾病導致上述症狀的產生

#### 鹿特丹診斷標準（Rotterdam diagnostic criteria）

  -
    2003年，[鹿特丹](../Page/鹿特丹.md "wikilink")
    [ESHRE](../Page/ESHRE.md "wikilink") /
    [ASRM贊助的共識研討會提出](../Page/ASRM.md "wikilink")，在沒有可能導致這些發現的其他實體的情況下，如果滿足3項標準中的任何一項，多囊卵巢就會出現\[80\]\[81\]\[82\]：

<!-- end list -->

1.  出現[排卵不規則](../Page/排卵不規則.md "wikilink") 或 或是兩者同時出現
2.  具有症狀
3.  具有多囊卵巢症狀 (藉由 檢查）

鹿特丹診斷標準涵蓋更廣泛的有症狀婦女，最顯著的部分是在於並未有雄性素過剩的婦女也被列入在其中。
評論家認為，從研究雄激素過多的婦女獲得的結果不一定可以推廣到給沒有雄激素過剩的婦女身上\[83\]\[84\]。

  - Androgen Excess PCOS Society
    2006年，Androgen Excess PCOS Society提出了一套嚴謹診斷標準\[85\]：

<!-- end list -->

1.  具有雄性激素過剩症狀
2.  出現排卵不規則、無排卵或多囊卵巢症狀，亦或同時出現上述症狀
3.  排除會引起過量雄激素活性的其他因素\*

### 標準評估

  - 若需依據病史診斷上，月經週期、痤瘡、多毛症、肥胖症均為具體的判斷依據。
    一份發現這四項指標在診斷PCOS方面具有77.1%的靈敏度（95%的信賴區間下62.7%–88.0%）和93.8%的特異性（95%的信賴區間下82.8%–98.7%）\[86\]。

,
尋找小的[濾泡為主要的檢查方式](../Page/濾泡.md "wikilink")。目前確信，濾泡的功能喪失或是排軟失敗是排卵失敗或是月經不正常的狀況所造成的。
在正常的[月經週期](../Page/月經週期.md "wikilink"),一個卵子從濾泡中釋放，在本質上，一個囊腫破裂之後釋放出卵子。
排卵之後,殘留的濾泡變形為產生[孕酮的](../Page/孕酮.md "wikilink")[黃體](../Page/黃體.md "wikilink")，並會在12-14天之後消失。在多囊卵巢綜合症的案例中，有一種被稱為"濾泡捕獲(follicular
arrest)"的狀況,即數個濾泡發展至5-7毫米就停止，並沒有發展至排卵前應有的大小(約16毫米或更大)。根據鹿特丹診斷標準\[87\]，超聲波檢查應在卵巢中看到12個或更多的小濾泡。
\[88\]更多近期的研究甚至建議，要判斷18-35歲的女性具有的症狀的話，至少需要25個濾泡才能確定。\[89\]The follicles
may be oriented in the periphery, giving the appearance of a 'string of
pearls'.\[90\]If a high resolution transvaginal ultrasonography machine
is not available, an ovarian volume of at least 10 ml is regarded as an
acceptable definition of having polycystic ovarian morphology instead of
follicle count.\[91\]

  - [Laparoscopic](../Page/laparoscopic_surgery.md "wikilink")
    examination may reveal a thickened, smooth, pearl-white outer
    surface of the ovary. (This would usually be an incidental finding
    if laparoscopy were performed for some other reason, as it would not
    be routine to examine the ovaries in this way to confirm a diagnosis
    of PCOS.)
  - Serum (blood) levels of [androgens](../Page/androgen.md "wikilink")
    (hormones associated with male development), including
    [androstenedione](../Page/androstenedione.md "wikilink") and
    [testosterone](../Page/testosterone.md "wikilink") may be
    elevated.\[92\][Dehydroepiandrosterone
    sulfate](../Page/Dehydroepiandrosterone_sulfate.md "wikilink")
    levels above 700–800 µg/dL are highly suggestive of adrenal
    dysfunction because DHEA-S is made exclusively by the adrenal
    glands.\[93\]\[94\]The free testosterone level is thought to be the
    best measure,\[95\]\[96\]with \~60% of PCOS patients demonstrating
    supranormal levels.\[97\]The [Free androgen
    index](../Page/Free_androgen_index.md "wikilink") (FAI) of the ratio
    of testosterone to [sex hormone-binding
    globulin](../Page/sex_hormone-binding_globulin.md "wikilink") (SHBG)
    is high\[98\]\[99\]and is meant to be a predictor of free
    testosterone, but is a poor parameter for this and is no better than
    testosterone alone as a marker for PCOS,\[100\]possibly because FAI
    is correlated with the degree of obesity.\[101\]

Some other blood tests are suggestive but not diagnostic. The ratio of
LH ([Luteinizing hormone](../Page/Luteinizing_hormone.md "wikilink")) to
FSH ([Follicle-stimulating
hormone](../Page/Follicle-stimulating_hormone.md "wikilink")), when
measured in [international
units](../Page/international_unit.md "wikilink"), is elevated in women
with PCOS. Common
[cut-offs](../Page/cut-off_\(reference_value\).md "wikilink") to
designate abnormally high LH/FSH ratios are 2:1\[102\]or 3:1\[103\]as
tested on Day 3 of the menstrual cycle. The pattern is not very
sensitive; a ratio of 2:1 or higher was present in less than 50% of
women with PCOS in one study.\[104\]There are often low levels of [sex
hormone-binding
globulin](../Page/sex_hormone-binding_globulin.md "wikilink"),\[105\]in
particular among obese or overweight women.

[Anti-Müllerian hormone](../Page/Anti-Müllerian_hormone.md "wikilink")
(AMH) is increased in PCOS, and may become part of its diagnostic
criteria.\[106\]\[107\]\[108\]

### 相關條件

  - Fasting biochemical screen and lipid profile\[109\]
  - 2-Hour oral [glucose tolerance
    test](../Page/glucose_tolerance_test.md "wikilink") (GTT) in women
    with risk factors (obesity, family history, history of gestational
    diabetes)\[110\]may indicate impaired glucose tolerance (insulin
    resistance) in 15–33% of women with PCOS.\[111\]Frank diabetes can
    be seen in 65–68% of women with this condition. Insulin resistance
    can be observed in both normal weight and overweight people,
    although it is more common in the latter (and in those matching the
    stricter NIH criteria for diagnosis); 50–80% of people with PCOS may
    have insulin resistance at some level.\[112\]
  - Fasting insulin level or GTT with insulin levels (also called IGTT).
    Elevated insulin levels have been helpful to predict response to
    medication and may indicate women needing higher dosages of
    metformin or the use of a second medication to significantly lower
    insulin levels. Elevated [blood
    sugar](../Page/blood_sugar.md "wikilink") and insulin values do not
    predict who responds to an insulin-lowering medication, low-glycemic
    diet, and exercise. Many women with normal levels may benefit from
    combination therapy. A hypoglycemic response in which the two-hour
    insulin level is higher and the blood sugar lower than fasting is
    consistent with insulin resistance. A mathematical derivation known
    as the HOMAI, calculated from the fasting values in glucose and
    insulin concentrations, allows a direct and moderately accurate
    measure of insulin sensitivity (glucose-level x insulin-level/22.5).
  - [Glucose tolerance
    testing](../Page/Glucose_tolerance_test.md "wikilink") (GTT) instead
    of fasting glucose can increase diagnosis of impaired glucose
    tolerance and frank diabetes among people with PCOS according to a
    prospective controlled trial.\[113\]While fasting glucose levels may
    remain within normal limits, oral glucose tests revealed that up to
    38% of asymptomatic women with PCOS (versus 8.5% in the general
    population) actually had impaired glucose tolerance, 7.5% of those
    with frank diabetes according to ADA guidelines.\[114\]

### 鑑別診斷

應該調查其他原因，例如[甲狀腺機能低下症](../Page/甲狀腺機能低下症.md "wikilink")、[先天性腎上腺增生症](../Page/先天性腎上腺增生症.md "wikilink")（21-羥化酶缺乏症）、[庫興氏症候群](../Page/庫興氏症候群.md "wikilink")、、雄激素分泌性腫瘤以及其他垂體或腎上腺疾病。\[115\]\[116\]\[117\]

## 管理

多囊卵巢綜合症的主要治疗方法包括：調整生活方式及藥物治療\[118\]。

主要治療目標大致可分為下列四點：

  - 降低病患之胰島素抗性
  - 恢復生育能力
  - 治療[多毛症及](../Page/先天性遺傳多毛症.md "wikilink")[痤瘡](../Page/痤瘡.md "wikilink")
  - 恢復規律月經週期，並預防[子宮內膜增生症和](../Page/子宮內膜增生症.md "wikilink")[子宫内膜癌的發生](../Page/子宫内膜癌.md "wikilink")

目前現行療法中何為最佳治療方式仍是相當大的爭議，其中一個主要原因是因為缺乏比較不同療程療效的大規模臨床試驗。[樣本往往是](../Page/樣本_\(統計學\).md "wikilink")因此可能產生矛盾不可靠的結果。

有助於減輕體重或降低胰島素抗性的一般干預措施對於改善多囊卵巢綜合症的症狀有益，體重增加與胰島素抗性被視為是潛在的病因。由於多囊卵巢綜合症有可能會引起嚴重的情緒障礙，因此適當的精神支持對於病情可能是有益的。\[119\]

### 飲食

多囊卵巢綜合症與超重或肥胖有關，減肥是恢復規律月經的最有效方法，但是很多女性很難達到並維持顯著的體重減輕。
2013年的科學評估發現，與飲食組成無關，重量和體重組成、、月經規律、排卵、雄激素過高、[胰島素抗性](../Page/胰岛素抵抗.md "wikilink")、脂質以及生活質量均有相似的降低。\[120\]然而，其中大部分的碳水化合物從水果、蔬菜和全穀物獲得，而不是[營養素均衡的健康飲食](../Page/營養素.md "wikilink")，會導致更嚴重的月經失衡\[121\]。

[維生素D缺乏症可能在](../Page/維生素D缺乏症.md "wikilink")[代謝症候群的發展中發揮一定的作用](../Page/代謝症候群.md "wikilink")，故遵照醫囑補充缺乏的營養素是極為重要的\[122\]\[123\]。然而，2015年的系統評估並沒有發現維生素D具有減輕多囊卵巢綜合症中代謝和激素失調情況的證據。\[124\]截至2012年，使用[營養補充品預防多囊卵巢綜合症患者代謝缺陷的干預措施已經在小型](../Page/營養補充品.md "wikilink")、不受控制的和非隨機的臨床試驗中進行了測試；而所得數據不足以推薦使用。\[125\]

### 藥物

用於治療多囊卵巢綜合症的藥物有[避孕藥及](../Page/避孕藥.md "wikilink")[二甲雙胍](../Page/二甲雙胍.md "wikilink")。口服避孕藥能增加體內[性激素结合球蛋白生產](../Page/性激素结合球蛋白.md "wikilink")，促進游離睾酮的結合。這減少了由高睾丸激素引起的[多毛症狀](../Page/多毛症.md "wikilink")，並調節恢復正常[月經週期](../Page/月經週期.md "wikilink")。二甲雙胍是在[2型糖尿病中常用的一種降低胰島素排斥的藥物](../Page/2型糖尿病.md "wikilink")，並在英國，美國，澳大利亞和歐盟用來治療多囊卵巢綜合症中的胰島素排斥。在許多情況下，二甲雙胍也能協助卵巢功能並恢復正常排卵\[126\]\[127\]\[128\]。[螺內酯可用於其抗雄激素作用](../Page/螺內酯.md "wikilink")，而[二氟甲基鳥氨酸則可用於減少面部毛髮](../Page/二氟甲基鳥氨酸.md "wikilink")。較新的胰島素抵抗藥物顯示出與二甲雙胍相當的功效，但二甲雙胍具有更輕微的副作用\[129\]\[130\]。
在2004年提出建議，當其他治療未能產生效果時，將給予給予[BMI高於](../Page/身高體重指數.md "wikilink")25的病患服用二甲雙胍\[131\]\[132\]。二甲雙胍在每種類型的多囊卵巢綜合症中可能並非有效的，因此對於是否應該用作一般一線治療存在一些分歧\[133\]。[羟甲基戊二酸单酰辅酶A还原酶抑制剂在治療潛在代謝綜合徵方面的應用尚不清楚](../Page/羟甲基戊二酸单酰辅酶A还原酶抑制剂.md "wikilink")\[134\]。

多囊卵巢綜合症可能導致難以受孕，因為它會導致不規律[排卵](../Page/排卵.md "wikilink")。試圖懷孕時，會使用誘導生育的藥物包括排卵誘導劑克羅米酚或
脈衝亮丙瑞林 。
二甲雙胍與克羅米酚組合使用時，可提高生殖治療的療效。\[135\]二甲雙胍被認為在懷孕期間使用是安全的，於美國的[懷孕分級為B](../Page/懷孕分級.md "wikilink")\[136\]。2014年的評論得出結論，在三個月內使用二甲雙胍治療的女性並不會增加產下[先天性障礙嬰兒的風險](../Page/先天性障礙.md "wikilink")\[137\]。

### 不孕症

### 多毛症及痤瘡

### 月經不規律

如果病患目前沒有懷孕需求，可以藉由服用[複合口服避孕藥來調節](../Page/複合口服避孕藥.md "wikilink")[月經紊亂](../Page/月經.md "wikilink")\[138\]\[139\]。
調理生理週期的主要目的是讓女性生活上比較方便，改善女性對於生理週期與自己的感受；若能自然規律行經，便不需藉由服藥使生理週期變得規律。

如果不期望定期的月經週期，則不一定需要不規則循環的治療。大多數專家說，如果至少每三個月發生一次月經，那麼子宮內膜就會經常流下來，以防止增加子宮內膜異常或癌症的風險。\[140\]
如果月經頻率極低或根本沒來，推薦使用某些形式的助孕素。\[141\]
其中一種替代方案即是定期服用一次口服助孕素（例如，每三個月）以誘發可預測的月經出血。\[142\]

### 替代藥物

一篇2017年發表的科學論文指出肌醇與D-手性肌醇可能具有調節生理週期與改善排卵的功效，然而目前仍然缺乏其是否能影響懷孕機率的證據，一篇於2011年發表的[綜述文章亦指出目前缺乏足夠的證據去證明D](../Page/綜述文章.md "wikilink")-手性肌醇具有任何功效。
\[143\]\[144\] \[145\]
兩篇分別發表於2012年與2017年的[綜述文章點出補充](../Page/綜述文章.md "wikilink")[肌醇似乎能夠有效改善多囊性卵巢綜合症的幾種激素紊亂](../Page/肌醇.md "wikilink")，且能降低採取體外受精之女性的促性腺激素釋放激素濃度與改善其卵巢過度刺激症的發作時間。\[146\]\[147\]
\[148\] 對於針灸是否能改善多囊性卵巢綜合症方面，目前仍然缺乏足夠的科學證據來證實其效用。\[149\]\[150\]

## 預測

多囊性卵巢綜合症的高風險伴隨症狀：

  - 可能發生[子宮內膜增生症和](../Page/子宮內膜增生症.md "wikilink")[子宫内膜癌](../Page/子宫内膜癌.md "wikilink")，因為子宮內膜過度積累，且也缺乏[黃體素導致雌激素對子宮細胞的刺激延遲](../Page/孕酮.md "wikilink")\[151\]\[152\]\[153\]。這種風險是否直接歸因於綜合徵或相關的肥胖（[高胰島素血症和](../Page/高胰島素血症.md "wikilink")）還不清楚。\[154\]\[155\]\[156\]

  - [胰島素抗性或](../Page/胰島素抗性.md "wikilink")[第二型糖尿病](../Page/2型糖尿病.md "wikilink")\[157\]：2010年發表的一項論文得出，即使在控制[身高體重指數](../Page/身高體重指數.md "wikilink")（BMI）時，多囊性卵巢綜合症患者的胰島素抗性和II型糖尿病患病率也會升高。\[158\]\[159\]多囊性卵巢綜合症也使一個女人，容易患有[妊娠糖尿病](../Page/妊娠糖尿病.md "wikilink")，特別是肥胖症患者\[160\]。

  - [高血壓](../Page/高血壓.md "wikilink")，特別是肥胖或懷孕期間\[161\]。

  - [憂鬱和](../Page/憂鬱.md "wikilink")[焦慮](../Page/焦慮.md "wikilink")\[162\]\[163\]

  - \[164\] （脂質代謝障礙 ）
    膽固醇和三酸甘油酯。具有多囊卵巢綜合症的婦女顯示[动脉粥样硬化去除機制會減弱](../Page/动脉粥样硬化.md "wikilink")，導致殘餘物，似乎與胰島素抗性或II型糖尿病無關。\[165\]

  - [心血管疾病](../Page/心血管疾病.md "wikilink")\[166\]\[167\]研究分析估計有多囊性卵巢綜合症患者的動脈疾病風險相對於沒有女性的2倍，與BMI無關\[168\]。

  - [中風](../Page/中風.md "wikilink")\[169\]\[170\]

  - \[171\]

  - [流產](../Page/流產.md "wikilink")\[172\]\[173\]

  - [睡眠呼吸中止症](../Page/睡眠呼吸暂停.md "wikilink")，特別是肥胖症患者\[174\]

  - [非酒精性脂肪肝](../Page/非酒精性脂肪肝.md "wikilink")，特別是肥胖症患者\[175\]

  - [黑棘皮症](../Page/黑棘皮症.md "wikilink")（腋下，鼠蹊部與後頸出現暗色皮膚斑紋）\[176\]

  - \[177\]

早期診斷和治療可能會降低其中一些風險，如II型糖尿病和心臟病。

卵巢癌和乳腺癌的風險總體上沒有顯著增加。\[178\]

## 流行病學

多囊性卵巢綜合症的盛行率受到診斷標準的影響。
世界衛生組織在2010年時估計全世界約有一億一千六百萬名女性（約3.4%的女性）受多囊性卵巢綜合症影響。\[179\]一份以鹿特丹診斷診斷指引為準的多囊性卵巢綜合症流行率社區研究發現，大約有百分之18的女性患有多囊性卵巢，而這些患者約有百分之70先前並未被確診出患有多囊性卵巢綜合症。\[180\]

約有百分之8到25的一般女性在超音波診斷中會看到有多囊性卵巢。\[181\]\[182\]\[183\]\[184\]服用口服避孕藥的女性中，有14%發現有多囊性卵巢\[185\]。卵巢囊腫也是使用釋放[黃體素的](../Page/黃體素.md "wikilink")[子宮環後常見的副作用](../Page/子宮環.md "wikilink")。\[186\]

## 歷史

這種症狀最早在1935年由美國婦科醫生Irving F. Stein, Sr.與Michael L.
Leventhal首次描述，其原始名稱為斯-李二氏症\[187\]\[188\] 。

目前已知最早的多囊卵巢綜合症記錄，是1721年間源自義大利的記錄\[189\]。關於卵巢囊腫的相關變化描述最早出現在1844年\[190\]。

## 社會及文化

### 經費

2005年在美國有約四百萬起多囊卵巢綜合症的病例，其醫療費用有43.6億美元\[191\]。2016年國立衛生研究院的研究預算有323億美元，其中有0.1%用在多囊卵巢綜合症的研究上\[192\]。

### 名稱

這種綜合徵的其他名稱包括多囊卵巢疾病、功能性卵巢雄激素過多症、、硬皮囊性卵巢綜合徵和斯-李二氏症。斯-李二氏症為其原始名稱，現在使用這個名稱都僅限於具有[不孕症](../Page/不孕症.md "wikilink")、[多毛症](../Page/先天性遗传多毛症.md "wikilink")、卵巢有多發囊狀腫大
的[閉經女性](../Page/閉經.md "wikilink")\[193\]。

這種疾病的名稱「多囊卵巢綜合症」是因為在醫學影像中可見多囊性卵巢而得此稱\[194\]。多囊性卵巢在靠近卵巢表面處有極大量正在發育的卵子，其在超音波中可用肉眼鑑別\[195\]，看起來像許多小[囊腫](../Page/囊腫.md "wikilink")\[196\]。

## 參見

  -
  - 《》（真實改編電視劇）

## 參考文獻

[Category:婦科疾病](../Category/婦科疾病.md "wikilink")
[Category:與肥胖有關的醫學症狀](../Category/與肥胖有關的醫學症狀.md "wikilink")
[Category:症候群](../Category/症候群.md "wikilink")
[Category:人類繁殖](../Category/人類繁殖.md "wikilink")
[Category:内分泌相关皮肤疾病](../Category/内分泌相关皮肤疾病.md "wikilink")

1.

2.

3.
4.

5.

6.

7.
8.

9.

10.

11.

12.

13.

14.

15. Diagnostic Criteria and Epidemiology of PCOS; Heather R. Burks and
    Robert A. Wild; Book Title: Polycystic Ovary Syndrome; Subtitle:
    Current and Emerging Concepts; Part I; Pages: pp 03-10; Copyright:
    2014; DOI: 10.1007/978-1-4614-8394-6_17; Print [ISBN
    978-1-4614-8393-9](../Page/Special:网络书源/9781461483939.md "wikilink");
    Online [ISBN
    978-1-4614-8394-6](../Page/Special:网络书源/9781461483946.md "wikilink");
    Publisher: Springer New York.
    (http://link.springer.com/chapter/10.1007/978-1-4614-8394-6_17).

16.
17.

18.
19.
20.
21.
22.
23.

24.

25.
26.
27.

28.
29. Page 836 (Section:*Polycystic ovary syndrome*) in:

30.

31.
32.
33.
34.

35.
36.
37.
38.
39.

40.

41.

42.
43.
44.

45.

46.

47.
48.
49.

50.

51.

52.
53.

54.

55.

56.

57.

58.

59.
60.

61.

62.
63.

64.

65.
66.
67.
68. Kumar Cotran Robbins: Basic Pathology 6th ed. / Saunders 1996

69.
70.

71.

72.

73.

74.

75.

76. <http://www.obgyn.net/articles/transgenderpcos>

77.

78.
79.

80.
81.

82.

83.

84.

85.
86.

87.
88.
89.

90.

91.
92.
93.

94.

95.
96.

97.
98.
99.
100.

101.

102.
103.
104.

105.
106.

107.

108.

109.
110.
111.
112.
113.

114.
115.
116.
117.
118.

119.

120.

121.
122.
123.

124.

125.

126.
127.
128.

129.

130.

131.

132.

133.

134.

135.

136.

137.

138.
139.

140.

141.

142.
143.

144.

145.

146.

147.

148.

149.

150.

151.
152.
153.
154.

155.

156.

157.
158.
159.

160.
161.
162.
163.

164.
165.

166.
167.
168.

169.
170.
171.
172.

173.

174.
175.
176.
177.

178.

179.

180.
181.

182.

183.

184.

185.
186.

187.

188.
189.
190.
191.

192.

193.
194.
195.
196.