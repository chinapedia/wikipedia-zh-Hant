[Lake-toba.jpg](https://zh.wikipedia.org/wiki/File:Lake-toba.jpg "fig:Lake-toba.jpg")
**多峇湖**（[印尼语](../Page/印尼语.md "wikilink")：**Danau
Toba**，[英語](../Page/英語.md "wikilink")：**Lake
Toba**），是一座位于[印尼](../Page/印尼.md "wikilink")[苏门达腊岛北部的](../Page/苏门达腊岛.md "wikilink")[火山湖](../Page/火山湖.md "wikilink")，此湖呈菱形，座標西北角，东南角，长100公里，宽30公里，面積1,130平方公里，是世界上最大的[火山湖](../Page/火山湖.md "wikilink")\[1\]，最深處505公尺，海拔905公尺。湖中有世界上最大的島中島[沙摩西島](../Page/沙摩西島.md "wikilink")。

多峇湖所在的[破火山口形成於距今七万五千年前的](../Page/破火山口.md "wikilink")[超级火山喷发](../Page/超級火山.md "wikilink")，该次爆发被认为具有8[VEI的强度](../Page/火山爆發指數.md "wikilink")。同时这次喷发被认为是过去25万年來地球上最大的一次喷发，很可能导致了大规模气候变化和一场全球性灾难。一些科学家认为，这次火山喷发可能造成了大部分人类死亡，造成人口缩减期，直接影响到所有幸存者的继承基因（參見[多峇巨災理論](../Page/多峇巨災理論.md "wikilink")、[种群瓶頸](../Page/种群瓶頸.md "wikilink")）。

島上有部族巴塔族。

{{-}}

## 註解

[Category:印尼地理的世界之最](../Category/印尼地理的世界之最.md "wikilink")
[Category:印尼湖泊](../Category/印尼湖泊.md "wikilink")
[Category:湖泊之最](../Category/湖泊之最.md "wikilink")
[Category:火山湖](../Category/火山湖.md "wikilink")
[Category:超级火山](../Category/超级火山.md "wikilink")

1.  [Worldlakes.org](http://www.worldlakes.org/lakedetails.asp?lakeid=8367)