[Gulong054.jpg](https://zh.wikipedia.org/wiki/File:Gulong054.jpg "fig:Gulong054.jpg")出版社版本，1997年出版\]\]
《**多情環**》為[古龍晚期小說](../Page/古龍.md "wikilink")，為《[七種武器](../Page/七種武器.md "wikilink")》之四，列為1974年10月[南琪](../Page/南琪.md "wikilink")《[武林七靈](../Page/武林七靈.md "wikilink")》之四。但部分出版社將其列為第五，而原本第五的《[霸王槍](../Page/霸王槍.md "wikilink")》列為第四。

## 內容簡介

[古龍在書末說](../Page/古龍.md "wikilink")：「仇恨的本身，就是種[武器](../Page/武器.md "wikilink")，而且是最可怕的一種。所以我說的第四種[武器也不是多情環](../Page/武器.md "wikilink")，而是仇恨。」

雙環門幫主盛天霸憑著多情環縱橫天下，建立近三十年的基業。他每殺一個對手，會在多情環上劃上一條痕跡。但短短的一個月裡，盛天霸和他的雙環門卻被天香堂堂主葛停香摧毀，多情環從此落入他手。盛極一時的雙環門，從此煙消雲散，留下的只有一雙銀環…

## 主要人物

  - [蕭少英](../Page/蕭少英.md "wikilink")：《多情環》主角，家道中落的世家子，因為酗酒鬧事，非禮師姐，已於兩年前被逐出雙環門。其實是個絕頂聰明的人，卻很會裝傻。

## 次要人物

  - [葛停香](../Page/葛停香.md "wikilink")：天香堂主人。已是個老人，手指卻仍然和少年時同樣靈敏有力，無論他想要什麼，他總是拿得到的。
  - [郭玉娘](../Page/郭玉娘.md "wikilink")：葛停香量珠聘來的江南名妓，他最寵愛的一位如夫人。不但美，而且柔媚溫順，善體人意。
  - [盛若蘭](../Page/盛若蘭.md "wikilink")：盛天霸之女，精暗器。不但是蕭少英的情人，也是蕭少英的妻子。
  - [李千山](../Page/李千山.md "wikilink")：青龍會九月初九的人。

## 小說目錄

1.  多情自古空餘恨
2.  暴雨荒塚
3.  殺人的人
4.  盤問
5.  密謀
6.  秘密室談
7.  暗殺
8.  廝殺
9.  仇恨

## 改編電影

  - 1979年，[台灣](../Page/台灣.md "wikilink")，《多情雙寶環》，導演：[張鵬翼](../Page/張鵬翼.md "wikilink")，[七種武器之多情環](../Page/七種武器.md "wikilink")

## 外部連結

  - [七種武器網上閱讀(繁體)](http://www.cnnovels.net/wx/gulong/seven-arm/index.html)
  - [七種武器網上閱讀(簡體)](http://www.oklink.net/wxsj/gu-long/seven-arm/index.html)
  - [神兵暗器](https://web.archive.org/web/20080917062557/http://gulong04.hp.infoseek.co.jp/gulong5.htm)
  - [蕭少英](https://web.archive.org/web/20040710211404/http://gulong04.hp.infoseek.co.jp/Dragon34.htm)

[Category:七種武器](../Category/七種武器.md "wikilink")
[Category:古龍筆下武器](../Category/古龍筆下武器.md "wikilink")