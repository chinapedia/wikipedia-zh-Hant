**多物網絡流問題（Multi-commodity Flow
Problem）**是多種物品（或貨物）在網絡中從不同的源點流向不同的匯點的[網絡流問題](../Page/網絡流.md "wikilink")。

## 定義

已知一流網絡\(\,G(V,E)\)，其中邊\((u,v) \in E\)的容量為\(\,c(u,v)\)。有\(\,k\)件物品\(K_1,K_2,\dots,K_k\)，定義為\(\,K_i=(s_i,t_i,d_i)\)，其中\(\,s_i\)和\(\,t_i\)是物品\(\,i\)的**源點**及**匯點**，及\(\,d_i\)是需求。物品\(\,i\)沿邊\(\,(u,v)\)的流量是\(\,f_i(u,v)\)。求一個符合以下限制的流量分配：

  -
    {|

| **容量限制**：|| \(\,\sum_{i=1}^{k} f_i(u,v) \leq c(u,v)\) |- | **流守恆**：||
\(\,\sum_{w \in V} f_i(u,w) = 0 \quad \mathrm{when} \quad u \neq s_i, t_i\)
|- | **需求的滿足**：||
\(\,\sum_{w \in V} f_i(s_i,w) = d_i \Leftrightarrow \sum_{w \in V} f_i(w,t_i) = d_i\)
|}

在**最小成本多物網絡流問題**中，在\(\,(u,v)\)上傳送需要成本\(a(u,v) \cdot f(u,v)\)。目的是要最小化

\[\sum_{(u,v)\in E} \left(a(u,v) \sum_{i=1}^{k} f_i(u,v) \right)\]

在**最大多物網絡流問題**中，每件物品都沒有硬性的需求，但最大化總生產量：

\[\sum_{i=1}^{k} \sum_{w \in V} f_i(s_i,w)\]

在**最大同時網絡流問題**中，任務是要將物品的流量對它的需求的最小比例最大化：

\[\min_{1 \leq i \leq k} \frac{\sum_{w \in V} f_i(s_i,w)}{d_i}\]

## 與其它問題的關係

最小成本變體是普遍化的[最小成本網絡流問題](../Page/最小成本網絡流問題.md "wikilink")。[環流問題的變體是所有網絡流問題的概括](../Page/環流問題.md "wikilink")。

## 用途

利用多物網絡流的公式可以接近在[光學網絡的](../Page/光學網絡.md "wikilink")[光學群聚交換中的](../Page/光學群聚交換.md "wikilink")[路由波長分配（RWA,
Routing Wavelength Assignment）](../Page/路由波長分配.md "wikilink")。

## 解

這問題已知的解是建基於[線性規劃](../Page/線性規劃.md "wikilink")\[1\].

就算只有兩件物品，對於整體流來說，這問題是[NP完全](../Page/NP完全.md "wikilink")\[2\]。在有錯誤限下，已有完全多項式時間近似值的方法去解決這難題\[3\]。對於這難題的分數變體，在多項式時間中已有解。

## 參考

<references/>

[Category:網絡流](../Category/網絡流.md "wikilink")

1.
2.
3.