在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）的[中土大陸裡](../Page/中土大陸.md "wikilink")，**多瑞亞斯**（Doriath）是[辛達族](../Page/辛達族.md "wikilink")（Sindar）精靈的領土，辛達族精靈的國王是[埃盧·庭葛](../Page/埃盧·庭葛.md "wikilink")（Elu
Thingol）。多瑞亞斯即是「柵欄之地」，因為[美麗安](../Page/美麗安.md "wikilink")（Melian）設下了魔法環帶，除了國王庭葛離開，否則無人能夠進入多瑞亞斯。

> *"A dark and hidden king did dwell,
> *Lord of the forest and the fell;*
> *And sharp his sword and high his helm,*
> *The king of beech and oak and elm.''" \[1\]

## 地理

多瑞亞斯是森林地區，鄰接[西瑞安河](../Page/西瑞安河.md "wikilink")（River
Sirion）及其東面的支流：[明代布河](../Page/明代布河.md "wikilink")（Mindeb）、[斯卡督印河](../Page/斯卡督印河.md "wikilink")（Esgalduin）及[阿羅斯河](../Page/阿羅斯河.md "wikilink")（Aros）。[奈爾多雷斯](../Page/奈爾多雷斯.md "wikilink")（Neldoreth）、[尼弗瑞姆](../Page/尼弗瑞姆.md "wikilink")（Nivrim）等森林都在多瑞亞斯。還有，[貝西爾](../Page/貝西爾.md "wikilink")（Brethil）森林及[艾莫斯谷森林](../Page/艾莫斯谷森林.md "wikilink")（Nan
Elmoth）都屬於多瑞亞斯，這兩個地方卻在魔法環帶以外。埃盧·庭葛視整個貝爾蘭為他的領土。多瑞亞斯的中央地帶是天然景觀，有一個佈滿洞穴的大山，埃盧·庭葛及矮人在這裡建造了一個不可思議的城市，叫[明霓國斯](../Page/明霓國斯.md "wikilink")
*Menegroth*（千石窟宮殿）。

## 历史

### 第一纪元前

精靈由[庫維因恩](../Page/庫維因恩.md "wikilink")（Cuiviénen）前往[阿門洲](../Page/阿門洲.md "wikilink")（Aman）時亦通過明霓國斯。[芬威](../Page/芬威.md "wikilink")（Finwë）和諾多精靈曾在此住過一段時間。他們抵達[伊瑞西亞島](../Page/伊瑞西亞島.md "wikilink")（Tol
Eressëa），這時，[帖勒瑞族](../Page/帖勒瑞族.md "wikilink")（Teleri）也到達了。埃盧·庭葛卻於艾莫斯谷迷失了，部分人也留下了。他們就是[辛達族](../Page/辛達族.md "wikilink")（Sindar）精靈，埃盧·庭葛成為他們的國王，統治多瑞亞斯。

多瑞亞斯原名是**埃戈拉多**（Eglador），意思是「精靈之地」。[第一紀元前](../Page/第一紀元.md "wikilink")，多瑞亞斯受到[魔苟斯的半獸人攻擊](../Page/魔苟斯.md "wikilink")。美麗安以霧陣抵禦。埃盧·庭葛率領弓箭手於邊界迎擊。在矮人的協助下，他的精靈軍隊都配備斧頭、、長矛、長劍、甲衣及盾。埃盧·庭葛並召集所有辛達族精靈到多瑞亞斯，但仍有許多精靈留在荒野。在[第一次會戰](../Page/第一次會戰.md "wikilink")（First
Battle of Beleriand）後，許多[綠精靈](../Page/綠精靈.md "wikilink")（Green
Elves）及[亞維瑞](../Page/亞維瑞.md "wikilink")（Avari）都來到多瑞亞斯。

黑暗精靈[伊奧](../Page/伊奧.md "wikilink")（Eöl）移居到艾莫斯谷，打造了[安格拉赫爾劍](../Page/安格拉赫爾劍.md "wikilink")（Anglachel）送給埃盧·庭葛。

[貝磊勾斯特](../Page/貝磊勾斯特.md "wikilink")（Belegost）及[諾格羅德](../Page/諾格羅德.md "wikilink")（Nogrod）的矮人協助修建明霓國斯，明霓國斯是埃盧·庭葛的都城。
埃盧·庭葛將露西安置在明霓國斯前門的大山毛櫸樹中，防止她和[貝倫](../Page/貝倫.md "wikilink")（Beren）會面。愛斯卡督印河上的巨大石橋是唯一的入口。除了使用橋樑及船隻，西瑞安河及支流愛斯卡督印河都不可能通過。\[2\]

### 第一纪元

第一紀元初，諾多精靈返回中土大陸。多瑞亞斯的邊界因須準備防禦魔苟斯而要關閉。埃盧·庭葛只允許[費納芬](../Page/費納芬.md "wikilink")（Finarfin）的兒女通過。

其後，人類也抵達貝爾蘭，他們並不允許進入多瑞亞斯，但在[芬羅德·費拉剛的要求下](../Page/芬羅德·費拉剛.md "wikilink")，准許哈萊斯族人住在貝西爾。

[巴拉漢](../Page/巴拉漢.md "wikilink")（Barahir）之子[貝倫](../Page/貝倫.md "wikilink")（Beren）通過了魔法環帶，抵達奈爾多雷斯（Neldoreth）。埃盧·庭葛女兒露西安與他相戀。[精靈寶鑽任務結束後](../Page/精靈寶鑽_\(寶物\).md "wikilink")，巨狼[卡黑洛斯](../Page/卡黑洛斯.md "wikilink")（Carcharoth）也通過了魔法環帶。但埃盧·庭葛、貝倫、神犬胡安（Huan）及埃盧·庭葛的將領[畢烈格](../Page/畢烈格.md "wikilink")（Beleg）和[馬伯龍](../Page/馬伯龍.md "wikilink")（Mablung）擊殺了巨狼。

[胡林](../Page/胡林.md "wikilink")（Húrin）及[莫玟](../Page/莫玟.md "wikilink")（Morwen）之子[圖林被派到多瑞亞斯](../Page/圖林.md "wikilink")，並居住在這裡，他在一場致命的爭吵後逃離。後來，他的母親及妹妹[妮諾爾](../Page/妮諾爾.md "wikilink")（Nienor）也移居多瑞亞斯，直至去尋找圖林並失蹤。

芬羅德·費拉剛的國度敗亡後，胡林將[納國斯隆德](../Page/納國斯隆德.md "wikilink")（Nargothrond）的寶藏帶到多瑞亞斯。埃盧·庭葛委託諾格羅德的矮人以項鍊[諾格萊迷爾](../Page/諾格萊迷爾.md "wikilink")（Nauglamír）
結合貝倫和露西安的[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。矮人垂涎於項鍊，殺死埃盧·庭葛，並偷去項鍊逃遁。大部分矮人被殺，項鍊被奪回。多瑞亞斯在貝倫及露西安之子[迪歐](../Page/迪歐.md "wikilink")（Dior）的統治下，國力稍微恢復，但他在皇室相殘中被殺。多瑞亞斯遭到廢棄，直至在[憤怒之戰](../Page/憤怒之戰.md "wikilink")（War
of Wrath）被摧毀。

## 注釋

<div class="references-small">

<references/>

</div>

[de:Regionen und Orte in Tolkiens
Welt\#Doriath](../Page/de:Regionen_und_Orte_in_Tolkiens_Welt#Doriath.md "wikilink")

[D](../Category/中土大陸的地理.md "wikilink")

1.  *[中土世界的歷史](../Page/中土世界的歷史.md "wikilink")*第三部， 「Lay of Leithian」，
    156頁
2.  *[胡林的子女](../Page/胡林的子女.md "wikilink")*，118頁，「unbridged and
    unforded」；*中土世界的歷史*，第三部, 「Lay of the Children of Húrin」，59頁，
    1457-1471行