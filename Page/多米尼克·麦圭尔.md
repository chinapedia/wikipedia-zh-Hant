[Dominic_McGuire.jpg](https://zh.wikipedia.org/wiki/File:Dominic_McGuire.jpg "fig:Dominic_McGuire.jpg")

**多米尼克·麦圭尔**（，），出生于美国[圣迭戈](../Page/圣迭戈.md "wikilink")，美国职业篮球运动员，司职[大前锋](../Page/大前锋.md "wikilink")、[小前鋒或](../Page/小前鋒.md "wikilink")[得分後衛](../Page/得分後衛.md "wikilink")。他身高6英尺9英寸，体重220磅，有着惊人的弹跳和篮板意识并且球风顽强。

## 学生时期

多米尼克·麦圭尔毕业于圣迭戈的林肯高中（Lincoln High
School）。随后读于[加利福尼亚大学弗雷斯诺分校](../Page/加利福尼亚大学弗雷斯诺分校.md "wikilink")，主修非洲文化。在他大学的首个赛季中他出场26场，其中4场首发，取得场均4.3分和3.4个篮板，得到全队最高的22个盖帽并以场均0.85个盖帽排在太平洋大十联盟第十位；在2004-05赛季，马克圭尔出场28场比赛，其中23场首发，平均每场取得7.2分，抓下4.9个篮板和全队最高的场均1.1次封盖，其中有11场比赛取得两双；

## NBA生涯

在2007年6月28日的[2007年NBA选秀中在第二轮第](../Page/2007年NBA选秀.md "wikilink")47顺位被[华盛顿奇才选中](../Page/华盛顿奇才.md "wikilink")，多米尼克·麦克奎尔在选秀现场见证了这一时刻。

## 参考资料

## 外部链接

  - [Player Bio: Dominic McGuire :: Men's
    Basketball](http://gobulldogs.cstv.com/sports/m-baskbl/mtt/mcguire_dominic00.html)
  - [Dominic McGuire NCAA Basketball at CBS
    SportsLine.com](http://sportsline.com/collegebasketball/players/playerpage/568829)
  - [USAToday.com - Dominic McGuire
    Biography](https://web.archive.org/web/20120526170014/http://fantasybasketball.usatoday.com/content/player.asp?sport=NBA&id=1417)

[Category:非洲裔美國籃球運動員](../Category/非洲裔美國籃球運動員.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:中国篮球联赛外援](../Category/中国篮球联赛外援.md "wikilink")
[Category:華盛頓奇才隊球員](../Category/華盛頓奇才隊球員.md "wikilink")
[Category:夏洛特山貓隊球員](../Category/夏洛特山貓隊球員.md "wikilink")
[Category:萨克拉门托国王队球员](../Category/萨克拉门托国王队球员.md "wikilink")
[Category:金州勇士隊球員](../Category/金州勇士隊球員.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")
[Category:紐奧良鵜鶘隊球員](../Category/紐奧良鵜鶘隊球員.md "wikilink")
[Category:印第安纳步行者队球员](../Category/印第安纳步行者队球员.md "wikilink")