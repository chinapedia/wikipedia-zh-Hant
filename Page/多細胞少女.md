《**多細胞少女**》（，），是[韓國](../Page/大韓民國.md "wikilink")2006年上映的一部[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/韓國電影.md "wikilink")，改編自漫畫家[蔡正澤的同名網路漫畫](../Page/蔡正澤.md "wikilink")。透過尚未刻劃完成的各種人物，來諷刺社會以物質和偏見去評價一個人，傳遞不能用普遍的觀念和標準去衡量別人的個性取向，也不能隨意謾罵的訊息。

## 劇情介紹

學校的會長和副會長是一對出名的SM情侶，一個喜歡施虐，另一個喜歡受虐，兩人因染上性病被早退，這是該校紊亂的校風。不過有一群人的純情，在這不良風氣下蔓延著。用援助交際養家的孝女「遭遇許多困難的少女」，被從瑞士轉學來的安新力深深吸引，但是安新力卻看上「獨眼龍」漂亮的弟弟「雙眼龍」。

## 演員陣容

  - [金玉彬](../Page/金玉彬.md "wikilink") 飾 遭遇許多困難的少女
  - [朴鎮宇](../Page/朴鎮宇.md "wikilink") 飾 安新力
  - [李　全](../Page/李全_\(演員\).md "wikilink") 飾 獨眼龍
  - [朴慧媛](../Page/朴慧媛.md "wikilink") 飾 班長少女
  - [南好貞](../Page/南好貞.md "wikilink") 飾 副會長少女
  - [宋昰昀](../Page/宋昰昀.md "wikilink") 飾 桔梗少女
  - [李勇株](../Page/李勇株.md "wikilink") 飾 會長少年
  - [李民赫](../Page/李民赫.md "wikilink") 飾 US
  - [劉　健](../Page/劉健_\(韓國\).md "wikilink") 飾 TERI
  - [李恩承](../Page/李恩誠.md "wikilink") 飾 雙眼龍
  - [李原種](../Page/李原種.md "wikilink")
  - [林藝真](../Page/林藝真.md "wikilink")
  - [朴容植](../Page/朴容植.md "wikilink")
  - [李在勇](../Page/李在勇.md "wikilink")

### 特別出演

  - [金守美](../Page/金守美.md "wikilink")
  - [趙靜林](../Page/趙靜林.md "wikilink")
  - [李秉俊](../Page/李秉俊.md "wikilink")

## 參考資料

  - [新片《多細胞少女》用豐富的想像詮釋性問題](http://app.yonhapnews.co.kr/yna/basic/articlechina/new_search/YIBW_showSearchArticle.aspx?contents_id=ACK20060803000300999)
    韓聯網

[D](../Category/2006年電影.md "wikilink")
[D](../Category/韓國喜劇片.md "wikilink")
[D](../Category/韓語電影.md "wikilink")
[D](../Category/2000年代歌舞喜劇片.md "wikilink")
[D](../Category/電影原著漫畫.md "wikilink")
[D](../Category/韓國漫畫改編電影.md "wikilink")