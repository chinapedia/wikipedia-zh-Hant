**多鳍鱼科**（[学名](../Page/学名.md "wikilink")：）是[軟骨硬鱗亞綱](../Page/軟骨硬鱗亞綱.md "wikilink")
(Chondrostei)[多鳍鱼目](../Page/多鳍鱼目.md "wikilink")（）目前仅存的一科，现存二属，又稱**恐龍魚**，是比较古老的[辐鳍鱼类](../Page/辐鳍鱼类.md "wikilink")，全身有类似骨板的硬鳞，胸鳍有小的肉质叶，有鳍基骨，并不直接和肩胛骨连接，多鳍鱼有厚實的硬鱗，表面光滑有黏膜包裹，背部依據品種不同而有6至20個棘狀背鳍，
這些棘狀背鳍中的每一條都有鋒利的刺狀鰭骨。他們的下頜結構比起[真骨附類的魚](../Page/真骨附類.md "wikilink")
，更相似於[四足類的結構](../Page/四足類.md "wikilink")。
多鳍鱼有一定原始特徵。這些的特徵是有的肉質基叶的胸鳍，這和[腔棘魚相似](../Page/腔棘魚.md "wikilink")，而他們也有[呼吸孔](../Page/呼吸孔.md "wikilink")
([spiracles](../Page/spiracles.md "wikilink"))的。
所有多鳍鱼目成員都在[非洲的淡水河](../Page/非洲.md "wikilink")、湖區棲息，主要為沼澤、淺氾濫區和出海的河口。\[1\]\[2\]

他們也有基礎的[肺部](../Page/肺部.md "wikilink")，可以從空氣中吸取氧氣，当在水中的氧氣不足时，多鰭魚會快速地游到表面吸氣，此魚在幼魚期會像[兩棲類的有外鰓](../Page/兩棲類.md "wikilink")。在这些種類之中的最大长度为30cm至100cm以上。

## 分類

  - [蘆鰻屬](../Page/蘆鰻屬.md "wikilink")（*Erpetoichthys*）
      - [蘆鰻](../Page/蘆鰻.md "wikilink")（*E. calabaricus*）（又稱為蘆鱗魚）
  - [多鰭魚屬](../Page/多鰭魚屬.md "wikilink")（*Polypterus*）
      - [安氏多鰭魚](../Page/安氏多鰭魚.md "wikilink")（*P. ansorgii*）
      - [多鰭魚](../Page/多鰭魚.md "wikilink")（''P. bichir ''）
      - [戴氏多鰭魚](../Page/戴氏多鰭魚.md "wikilink")（*P. delhezi*）
      - [剛果多鰭魚](../Page/剛果多鰭魚.md "wikilink")（*P. endlicherii congicus*）
      - [恩氏多鰭魚](../Page/恩氏多鰭魚.md "wikilink")（*P. endlicherii
        endlicherii*）
      - [莫克多鰭魚](../Page/莫克多鰭魚.md "wikilink")(*Polypterus mokelembembe*)
      - [飾翅多鰭魚](../Page/飾翅多鰭魚.md "wikilink")（*P. ornatipinnis*）
      - [綠多鰭魚](../Page/綠多鰭魚.md "wikilink")（*P. palmas*）
      - [波氏多鰭魚](../Page/波氏多鰭魚.md "wikilink")（*P. polli*）
      - [後翼多鰭魚](../Page/後翼多鰭魚.md "wikilink")（*P. retropinnis*）
      - [塞內加爾多鰭魚](../Page/塞內加爾多鰭魚.md "wikilink")（*P. senegalus*）
      - [托氏多鰭魚](../Page/托氏多鰭魚.md "wikilink")(*Polypterus teugelsi*)
      - [魏氏多鰭魚](../Page/魏氏多鰭魚.md "wikilink")（*P. weeksii*）
  - †[巴氏巴威多鰭魚屬](../Page/巴氏巴威多鰭魚屬.md "wikilink")（*Bawitius*）
    <small>Grandstaff et al. 2012</small>
      - †[巴氏巴威多鰭魚](../Page/巴氏巴威多鰭魚.md "wikilink")（*Bawitius bartheli*）
        <small>(Schaal 1984) Grandstaff et al. 2012</small> -
        [白堊紀晚期](../Page/白堊紀.md "wikilink")（[森諾曼期](../Page/森諾曼期.md "wikilink")）[埃及](../Page/埃及.md "wikilink")

## 参考资料

[\*](../Category/多鳍鱼目.md "wikilink")

1.  [FishBase：多鳍鱼介绍(英文)](http://www.fishbase.org/Summary/FamilySummary.cfm?ID=31)
2.  [Digimorph - Polypterus senegalus (senegal
    bichir)](http://www.digimorph.org/specimens/Polypterus_senegalus/whole/)