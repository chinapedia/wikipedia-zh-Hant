[nightfall.jpg](https://zh.wikipedia.org/wiki/File:nightfall.jpg "fig:nightfall.jpg")
**夜幕低垂**又译**日暮**（），原本是[美國作家](../Page/美國作家.md "wikilink")[以撒·艾西莫夫的短篇](../Page/以撒·艾西莫夫.md "wikilink")[科幻小說](../Page/科幻小說.md "wikilink")《夜歸》（Nightfall，英文名稱相同），1941年《》雜誌（Astounding
Science Fiction）的主編[約翰·W·坎貝爾](../Page/約翰·W·坎貝爾.md "wikilink")（John W.
Compbell,
Jr）在看到[愛默生的一段文字](../Page/愛默生.md "wikilink")“設若蒼穹繁星，千年僅得一見，試問世間凡人，又當如何讚嘆，且將這片刻記憶，千秋萬代流傳？”，他請當時年僅21歲的艾西莫夫將它寫成一篇短篇小說，成為艾西莫夫的第32號科幻作品。《夜歸》在1968年被[美國科幻和奇幻作家協會](../Page/美國科幻和奇幻作家協會.md "wikilink")（Science
Fiction Writers of America）票選為「史上最佳科幻短篇小說」\[1\]。

艾西莫夫晚年健康狀況甚差，到最後根本無力編寫長篇小說，出版商遂建議他選出最心愛的中短篇[科幻小說當骨架](../Page/科幻小說.md "wikilink")，和[羅伯特·西爾柏格](../Page/羅伯特·西爾柏格.md "wikilink")（Robert
Silverberg）合作，將《夜歸》、《[醜小孩](../Page/醜小孩.md "wikilink")》（The Ugly Little
Boy）《》（The Bicentennial Man）擴充成長篇科幻小說\[2\]。

## 劇情簡介

故事內容是凱葛星球的天空有著六個太陽——火精（Onos）、赤蓋（Dovim）、金鉦（Tano）、金鑼（Sitha）、曦軒（Trey）、曦輪（Patru），各依不同週期此起彼落，星球上的居民一生中從沒有遇過黑夜。

薩羅大學天文學系助理教授預測出了日蝕（黯黮）的時間，每2049年一次的「夜幕低垂」，會失去幾小時的光明；「秘教寶典」記錄著世界末日的事實，與記載不謀而合的事逐一的發生；考古學家挖出了古代遺跡，證明星球之前曾有極致文明的存在，激烈的光明使徒宣稱：“每隔2049年便降下天火而一切焚燬”。於是星球上的人類一步一步走上預言、進入宿命的軌道，艾西莫夫逐一描繪夜幕低垂過程中的人心演變，2049年一次的文化摧毀，由恐懼衍生出種種暴行。日蝕（黯黮）的來臨，讓星球上的人類第一次見到了滿天星斗，了解到自我渺小的存在。人們因衝擊、恐懼而陷入瘋狂，因懼怕而舉起了火焰，此時烽煙四起，衍生出種種暴行。

## 参见

  - [多体问题](../Page/多体问题.md "wikilink")
  - [黑夜恐惧症](../Page/黑夜恐惧症.md "wikilink")

## 參考資料與外部連結

### 參考資料

<references/>

### 外部連結

  - [艾西莫夫官方網頁](http://www.asimovonline.com/)
  - [科幻國協在臺辦事處](http://danjalin.blogspot.com/)

[Category:美國小說](../Category/美國小說.md "wikilink")
[Category:科幻小说](../Category/科幻小说.md "wikilink")
[Category:1990年美國小說](../Category/1990年美國小說.md "wikilink")

1.  [以撒·艾西莫夫訃聞](http://www.rudysbooks.com/asimovobit.html)（英文）
2.  《夜幕低垂》P.11，葉李華，天下文化出版