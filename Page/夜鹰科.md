**夜鹰科**（[学名](../Page/学名.md "wikilink")：）在[动物分类学上是](../Page/动物分类学.md "wikilink")[鸟纲](../Page/鸟纲.md "wikilink")[夜鹰目中的一个](../Page/夜鹰目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，是中等体型的夜行鸟类，长翼、短腿、短喙。世界各地均有分布。

## 分类

夜鹰科分为三个亚科，约19属。

  - [耳夜鹰亚科](../Page/耳夜鹰亚科.md "wikilink")
    Eurostopodinae：有时被归类为独立的[耳夜鹰科](../Page/耳夜鹰科.md "wikilink")（Eurostopodidae）
      - [耳夜鹰属](../Page/耳夜鹰属.md "wikilink") *Eurostopodus*
      - [毛腿夜鹰属](../Page/毛腿夜鹰属.md "wikilink") *Lyncornis*
  - [美洲夜鹰亚科](../Page/美洲夜鹰亚科.md "wikilink") Chordeilinae
      - [斑尾夜鹰属](../Page/斑尾夜鹰属.md "wikilink") *Nyctiprogne*
      - [半领夜鹰属](../Page/半领夜鹰属.md "wikilink") *Lurocalis*
      - [美洲夜鹰属](../Page/美洲夜鹰属.md "wikilink") *Chordeiles*
  - [夜鹰亚科](../Page/夜鹰亚科.md "wikilink") Caprimulginae
      - [领夜鹰属](../Page/领夜鹰属.md "wikilink") *Gactornis*
      - [暗色夜鹰属](../Page/暗色夜鹰属.md "wikilink") *Nyctipolus*
      - [帕拉夜鹰属](../Page/帕拉夜鹰属.md "wikilink") *Nyctidromus*
      - [弱夜鷹屬](../Page/弱夜鷹屬.md "wikilink") *Phalaenoptilus*
      - [中美夜鷹屬](../Page/中美夜鷹屬.md "wikilink") *Siphonorhis*
      - [角夜鷹屬](../Page/角夜鷹屬.md "wikilink") *Nyctiphrynus*
      - [夜鹰属](../Page/夜鹰属.md "wikilink") *Caprimulgus*
      - [水夜鹰属](../Page/水夜鹰属.md "wikilink") *Hydropsalis*
      - [卡氏夜鹰属](../Page/卡氏夜鹰属.md "wikilink") ''Antrostomus ''
      - [小夜鹰属](../Page/小夜鹰属.md "wikilink") ''Setopagis ''
      - [琴尾夜鷹屬](../Page/琴尾夜鷹屬.md "wikilink") *Uropsalis*
      - [長曳夜鷹属](../Page/長曳夜鷹属.md "wikilink") *Macropsalis*
      - [镰翅夜鹰属](../Page/镰翅夜鹰属.md "wikilink") *Eleothreptus*
      - [帶翅夜鷹屬](../Page/帶翅夜鷹屬.md "wikilink") ''Systellura ''

## 参考文献

[\*](../Category/夜鹰科.md "wikilink")
[Category:夜鹰目](../Category/夜鹰目.md "wikilink")