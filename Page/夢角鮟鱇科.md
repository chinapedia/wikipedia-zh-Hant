**夢角鮟鱇科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮟鱇目的其中一科](../Page/鮟鱇目.md "wikilink")。

## 分類

**夢角鮟鱇科**下分17個屬，如下：

### 異夢角鮟鱇屬(*Bertella*)

  - [異夢角鮟鱇](../Page/異夢角鮟鱇.md "wikilink")(*Bertella idiomorpha*)

### 蟾口鮟鱇屬(*Chaenophryne*)

  - [龍蟾口鮟鱇](../Page/龍蟾口鮟鱇.md "wikilink")(*Chaenophryne draco*)
  - [長頭蟾口鮟鱇](../Page/長頭蟾口鮟鱇.md "wikilink")(*Chaenophryne longiceps*)
  - [黑紋蟾口鮟鱇](../Page/黑紋蟾口鮟鱇.md "wikilink")(*Chaenophryne melanorhabdus*)
  - [頭柄蟾口鮟鱇](../Page/頭柄蟾口鮟鱇.md "wikilink")(*Chaenophryne quasiramifera*)
  - [拉邁蟾口鮟鱇](../Page/拉邁蟾口鮟鱇.md "wikilink")(*Chaenophryne
    ramifera*)：又稱太平洋蟾口鮟鱇。

### 夢蜍鰭魚屬(*Chirophryne*)

  - [夢蜍鰭魚](../Page/夢蜍鰭魚.md "wikilink")(*Chirophryne xenolophus*)

### 櫛夢角鮟鱇屬(*Ctenochirichthys*)

  - [櫛夢角鮟鱇](../Page/櫛夢角鮟鱇.md "wikilink")(*Ctenochirichthys
    longimanus*)：又稱智利櫛夢鮟鱇。

### 戴娜角鮟鱇屬(*Danaphryne*)

  - [黑褐戴娜角鮟鱇](../Page/黑褐戴娜角鮟鱇.md "wikilink")(*Danaphryne nigrifilis*)

### 韌皮夢鮟鱇屬(*Dermatias*)

  - [平腹韌皮夢鮟鱇](../Page/平腹韌皮夢鮟鱇.md "wikilink")(*Dermatias platynogaster*)

### 狡鮟鱇屬(*Dolopichthys*)

  - [加拿大狡鮟鱇](../Page/加拿大狡鮟鱇.md "wikilink")(*Dolopichthys allector*)
  - [達納狡鮟鱇](../Page/達納狡鮟鱇.md "wikilink")(*Dolopichthys danae*)
  - [雙線狡鮟鱇](../Page/雙線狡鮟鱇.md "wikilink")(*Dolopichthys dinema*)
  - [朱巴狡鮟鱇](../Page/朱巴狡鮟鱇.md "wikilink")(*Dolopichthys jubatus*)
  - [卡氏狡鮟鱇](../Page/卡氏狡鮟鱇.md "wikilink")(*Dolopichthys karsteni*)
  - [長角狡鮟鱇](../Page/長角狡鮟鱇.md "wikilink")(*Dolopichthys longicornis*)
  - [黑狡鮟鱇](../Page/黑狡鮟鱇.md "wikilink")(*Dolopichthys pullatus*)

### 弱棘鮟鱇屬(*Leptacanthichthys*)

  - [弱棘鮟鱇](../Page/弱棘鮟鱇.md "wikilink")(*Leptacanthichthys
    gracilispinis*)

### 冠鮟鱇屬(*Lophodolos*)

  - [棘頜冠鮟鱇](../Page/棘頜冠鮟鱇.md "wikilink")(*Lophodolos acanthognathus*)
  - [印度冠鮟鱇](../Page/印度冠鮟鱇.md "wikilink")(*Lophodolos indicus*)

### 侏鮟鱇屬(*Microlophichthys*)

  - [深淵侏鮟鱇](../Page/深淵侏鮟鱇.md "wikilink")(*Microlophichthys
    andracanthus*)
  - [侏鮟鱇](../Page/侏鮟鱇.md "wikilink")(*Microlophichthys microlophus*)

### 夢角鮟鱇屬(*Oneirodes*)

  - [棘夢角鮟鱇](../Page/棘夢角鮟鱇.md "wikilink")(*Oneirodes acanthias*)
  - [印尼夢角鮟鱇](../Page/印尼夢角鮟鱇.md "wikilink")(*Oneirodes alius*)
  - [異棘夢角鮟鱇](../Page/異棘夢角鮟鱇.md "wikilink")(*Oneirodes anisacanthus*)
  - [巴氏夢角鮟鱇](../Page/巴氏夢角鮟鱇.md "wikilink")(*Oneirodes basili*)
  - [布氏夢角鮟鱇](../Page/布氏夢角鮟鱇.md "wikilink")(*Oneirodes bradburyae*)
  - [球夢角鮟鱇](../Page/球夢角鮟鱇.md "wikilink")(*Oneirodes bulbosus*)
  - [卡氏夢角鮟鱇](../Page/卡氏夢角鮟鱇.md "wikilink")(*Oneirodes carlsbergi*)
  - [克拉克夢角鮟鱇](../Page/克拉克夢角鮟鱇.md "wikilink")(*Oneirodes clarkei*)
  - [冠毛夢角鮟鱇](../Page/冠毛夢角鮟鱇.md "wikilink")(*Oneirodes cristatus*)
  - [熱帶夢角鮟鱇](../Page/熱帶夢角鮟鱇.md "wikilink")(*Oneirodes dicromischus*)
  - [上瓣夢角鮟鱇](../Page/上瓣夢角鮟鱇.md "wikilink")(*Oneirodes epithales*)
  - [伊氏夢角鮟鱇](../Page/伊氏夢角鮟鱇.md "wikilink")(*Oneirodes eschrichtii*)
  - [鞭夢角鮟鱇](../Page/鞭夢角鮟鱇.md "wikilink")(*Oneirodes flagellifer*)
  - [單絲夢角鮟鱇](../Page/單絲夢角鮟鱇.md "wikilink")(*Oneirodes haplonema*)
  - [異絲夢角鮟鱇](../Page/異絲夢角鮟鱇.md "wikilink")(*Oneirodes heteronema*)
  - [克氏夢角鮟鱇](../Page/克氏夢角鮟鱇.md "wikilink")(*Oneirodes kreffti*)
  - [柳氏夢角鮟鱇](../Page/柳氏夢角鮟鱇.md "wikilink")(*Oneirodes luetkeni*)
  - [大絲夢角鮟鱇](../Page/大絲夢角鮟鱇.md "wikilink")(*Oneirodes macronema*)
  - [大瓣夢角鮟鱇](../Page/大瓣夢角鮟鱇.md "wikilink")(*Oneirodes macrosteus*)
  - [黑尾夢角鮟鱇](../Page/黑尾夢角鮟鱇.md "wikilink")(*Oneirodes melanocauda*)
  - [小絲夢角鮟鱇](../Page/小絲夢角鮟鱇.md "wikilink")(*Oneirodes micronema*)
  - [奇夢角鮟鱇](../Page/奇夢角鮟鱇.md "wikilink")(*Oneirodes mirus*)
  - [多絲夢角鮟鱇](../Page/多絲夢角鮟鱇.md "wikilink")(*Oneirodes myrionemus*)
  - [背絲夢角鮟鱇](../Page/背絲夢角鮟鱇.md "wikilink")(*Oneirodes notius*)
  - [皮氏夢角鮟鱇](../Page/皮氏夢角鮟鱇.md "wikilink")(*Oneirodes pietschi*)
  - [斜絲夢角鮟鱇](../Page/斜絲夢角鮟鱇.md "wikilink")(*Oneirodes plagionema*)
  - [波氏夢角鮟鱇](../Page/波氏夢角鮟鱇.md "wikilink")(*Oneirodes posti*)
  - [鰭尾夢角鮟鱇](../Page/鰭尾夢角鮟鱇.md "wikilink")(*Oneirodes pterurus*)
  - [羅氏夢角鮟鱇](../Page/羅氏夢角鮟鱇.md "wikilink")(*Oneirodes rosenblatti*)
  - [砂夢角鮟鱇](../Page/砂夢角鮟鱇.md "wikilink")(*Oneirodes sabex*)
  - [裂絲夢角鮟鱇](../Page/裂絲夢角鮟鱇.md "wikilink")(''Oneirodes schistonema')
  - [史氏夢角鮟鱇](../Page/史氏夢角鮟鱇.md "wikilink")(*Oneirodes schmidti*)
  - [西氏夢角鮟鱇](../Page/西氏夢角鮟鱇.md "wikilink")(*Oneirodes theodoritissieri*)
  - [湯氏夢角鮟鱇](../Page/湯氏夢角鮟鱇.md "wikilink")(*Oneirodes thompsoni*)
  - [莖絲夢角鮟鱇](../Page/莖絲夢角鮟鱇.md "wikilink")(*Oneirodes thysanema*)

### 哀鮟鱇屬(*Pentherichthys*)

  - [白鼻哀鮟鱇](../Page/白鼻哀鮟鱇.md "wikilink")(*Pentherichthys atratus*)

### 葉吻鮟鱇屬(*Phyllorhinichthys*)

  - [巴氏葉吻鮟鱇](../Page/巴氏葉吻鮟鱇.md "wikilink")(*Phyllorhinichthys
    balushkini*)
  - [葉吻鮟鱇](../Page/葉吻鮟鱇.md "wikilink")(*Phyllorhinichthys micractis*)

### 普鮟鱇屬(*Puck*)

  - [普鮟鱇](../Page/普鮟鱇.md "wikilink")(*Puck pinnata*)

### 棘蟾鮟鱇屬(*Spiniphryne*)

  - [杜氏棘蟾鮟鱇](../Page/杜氏棘蟾鮟鱇.md "wikilink")(*Spiniphryne duhameli*)
  - [劍狀棘蟾鮟鱇](../Page/劍狀棘蟾鮟鱇.md "wikilink")(*Spiniphryne gladisfenae*)

### 蟾鮟鱇屬(*Tyrannophryne*)

  - [暴龍蟾鮟鱇](../Page/暴龍蟾鮟鱇.md "wikilink")(*Tyrannophryne pugnax*)

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/夢角鮟鱇科.md "wikilink")