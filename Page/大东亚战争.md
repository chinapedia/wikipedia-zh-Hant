[缩略图](https://zh.wikipedia.org/wiki/File:Japanese_wartime_national_debt.jpg "fig:缩略图")
[Japanese_wartime_obligations_in_1942.JPG](https://zh.wikipedia.org/wiki/File:Japanese_wartime_obligations_in_1942.JPG "fig:Japanese_wartime_obligations_in_1942.JPG")
**大東亞戰爭**（）是[日本对](../Page/日本.md "wikilink")[第二次世界大战時在遠東和太平洋戰場的](../Page/第二次世界大战.md "wikilink")[戰爭總稱](../Page/戰爭.md "wikilink")。其目的是為建立以日本為中心的「[大東亞共榮圈](../Page/大東亞共榮圈.md "wikilink")」，解放受[英國](../Page/英國.md "wikilink")、[美國](../Page/美國.md "wikilink")、[荷蘭殖民的东亚人民及](../Page/荷蘭.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。這個名稱是在1941年12月12日確定的，其意義為「為大東亞新秩序建設而進行的戰爭」。这一词在日本战败后被[驻日盟军总司令部视为](../Page/驻日盟军总司令部.md "wikilink")“战时用语”而禁止使用，如今日本多使用“[太平洋战争](../Page/太平洋战争.md "wikilink")”和“[十五年战争](../Page/十五年战争.md "wikilink")”两词来表示这一时期日本对美英，以及对中国发动的战争。而在中韩两国，“大东亚战争”一词则因对侵略战争带有正义情绪，而被人排斥。

## 概要

這一系列戰爭，是日本自[九一八事變後便從](../Page/九一八事變.md "wikilink")[軍備](../Page/軍備.md "wikilink")、[政治體制](../Page/政治體制.md "wikilink")、[外交關係](../Page/外交關係.md "wikilink")、[意識形態上準備已久](../Page/意識形態.md "wikilink")、尋機發動的[侵略戰爭](../Page/侵略戰爭.md "wikilink")。對他國[主權和侵略地區人民的](../Page/主權.md "wikilink")[人權有嚴重的侵害](../Page/人權.md "wikilink")。在相當長的時間內破壞了東亞的發展，給[中国和日本殖民地](../Page/中国.md "wikilink")[朝鲜半岛以及](../Page/朝鲜半岛.md "wikilink")[东南亚各地造成了嚴重的經濟破壞](../Page/东南亚.md "wikilink")、深重的人道主義災難、意識形態上的惡果、不斷延續下去的戰亂、以及長期的後遺症。

在日本國內有些人不认为日本发动的这一系列战争为侵略战争，肯定它為“驅逐白人[殖民者](../Page/殖民.md "wikilink")，解放亞洲的[聖戰](../Page/聖戰.md "wikilink")”。另外有的人雖然观点相近，但卻抱怨成功的渺茫和戰敗所带来的痛苦。譬如[司馬遼太郎在](../Page/司馬遼太郎.md "wikilink")《龍馬行》中表示:“……大東亞戰爭恐怕是世界史上最大宗的怪事件，依常識判斷也知道此役必敗，為何[陸軍軍閥仍執意發動戰爭呢](../Page/大日本帝國陸軍.md "wikilink")？那是因為曾被主導[維新的志士一把推開的這個未開化](../Page/明治維新.md "wikilink")、盲目且土味濃厚的宗教性攘夷思想，到了[昭和時代卻在無知的軍人腦中復甦](../Page/昭和.md "wikilink")。駭人的是，此思想竟披著‘革命思想’的外衣煽動軍方，最後將數百萬國民逼上絕路。”\[1\]。

### 八紘一宇

[Flag_of_Hakkoichiu.jpg](https://zh.wikipedia.org/wiki/File:Flag_of_Hakkoichiu.jpg "fig:Flag_of_Hakkoichiu.jpg")
“[八紘一宇](../Page/八紘一宇.md "wikilink")”是當時日軍宣揚大東亞戰爭正當用語，意為“天下一家”，對日本軍部[法西斯勢力來說](../Page/法西斯.md "wikilink")，其意是“統治世界”，展示其侵略的野心。八纮，指八方极远之地。此詞語出中國古籍《[列子湯問](../Page/列子.md "wikilink")》：“……汤又问：‘物有巨细乎？有修短乎？有同异乎？’革曰：‘渤海之东不知几亿万里，有大壑焉，实惟无底之谷，其下无底，名曰归墟。八纮九野之水，天汉之流，莫不注之，而无增无减焉。’……”。

## 內部連結

  - [中國抗日戰爭](../Page/中國抗日戰爭.md "wikilink")
  - [太平洋战争](../Page/太平洋战争.md "wikilink")
  - [大日本帝國](../Page/大日本帝國.md "wikilink")

## 參考資料

  - 《太平洋戦争史》，，1953年。
  - 《大本営機密日誌》，著，ダイヤモンド社，1952年3月；芙蓉書房出版，1995年8月新装版，ISBN 4829501537。
  - 《[大東亜戦争肯定論](../Page/大東亞戰爭肯定論.md "wikilink")》，著，番町書房，1964年8月；夏目書房（新装版），2001年8月，ISBN
    4931391923。
  - 《大東亜戦争始末記 自決編》，著，経済往来社，1966年。
  - 《大東亜戦争を見直そう》，著，原書房，1968年8月；明成社（新装版），2007年8月，ISBN 9784944219599。
  - 《大日本帝国の興亡》全5卷，原著、譯，每日新聞社，1984年。
  - 《艦長たちの太平洋戦争―34人の艦長が語った勇者の条件》，著，光人社，1983年，ISBN 4769802072。
  - 《大東亜戦争への道》，著，展転社，1990年12月，ISBN 4886560628。
  - 《閉された言語空間―占領軍の検閲と戦後日本》，[江藤淳](../Page/江藤淳.md "wikilink")，文藝春秋，1994年，ISBN
    4167366088。
  - 《抹殺された大東亜戦争―米軍占領下の検閲が歪めたもの》，著，明成社，2005年8月，ISBN 49442193。

[Category:日本战争](../Category/日本战争.md "wikilink")
[Category:太平洋战争](../Category/太平洋战争.md "wikilink")
[Category:日本歷史認識問題](../Category/日本歷史認識問題.md "wikilink")

1.  參看《龍馬行(三)》，[司馬遼太郎著](../Page/司馬遼太郎.md "wikilink")，[遠流出版公司](../Page/遠流出版公司.md "wikilink")，2012年4月1日，ISBN
    9789573269458，頁123。