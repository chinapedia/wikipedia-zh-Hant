**大中**（847年正月—860年十月）是[唐宣宗的](../Page/唐宣宗.md "wikilink")[年號](../Page/年號.md "wikilink")，共计14年。

大中十三年八月[唐懿宗李漼即位沿用](../Page/唐懿宗.md "wikilink")。

## 大事記

## 出生

## 逝世

  - **大中十二年**
      - [李商隱](../Page/李商隱.md "wikilink")
  - **大中十三年**
      - [唐宣宗李忱](../Page/唐宣宗.md "wikilink")

## 紀年

| 大中                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 847年                           | 848年                           | 849年                           | 850年                           | 851年                           | 852年                           | 853年                           | 854年                           | 855年                           | 856年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") |
| 大中                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 857年                           | 858年                           | 859年                           | 860年                           |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") |                                |                                |                                |                                |                                |                                |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他使用[大中年號的政權](../Page/大中.md "wikilink")
  - 同期存在的其他政权年号
      - [羅平](../Page/羅平_\(裘甫\).md "wikilink")（860年二月至八月）：[唐朝時期領袖](../Page/唐朝.md "wikilink")[裘甫之年號](../Page/裘甫.md "wikilink")
      - [承和](../Page/承和_\(仁明天皇\).md "wikilink")（834年正月三日至848年六月十三日）：[平安時代](../Page/平安時代.md "wikilink")[仁明天皇之年號](../Page/仁明天皇.md "wikilink")
      - [嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")（848年六月十三日至851年四月二十八日）：平安時代[仁明天皇](../Page/仁明天皇.md "wikilink")、[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [仁壽](../Page/仁壽_\(文德天皇\).md "wikilink")（851年四月二十八日至854年十一月三十日）：平安時代[仁明天皇](../Page/仁明天皇.md "wikilink")、[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [齊衡](../Page/齊衡.md "wikilink")（854年十一月三十日至857年二月二十一日）：平安時代[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [天安](../Page/天安_\(文德天皇\).md "wikilink")（857年二月二十一日至859年四月十五日）：平安時代[文德天皇](../Page/文德天皇.md "wikilink")、[清和天皇之年號](../Page/清和天皇.md "wikilink")
      - [貞觀](../Page/貞觀_\(清和天皇\).md "wikilink")（859年四月十五日至877年四月十六日）：平安時代[清和天皇](../Page/清和天皇.md "wikilink")、[陽成天皇之年號](../Page/陽成天皇.md "wikilink")
      - [咸和](../Page/咸和_\(大彝震\).md "wikilink")（831年至857年）：[渤海宣王](../Page/渤海國.md "wikilink")[大彝震之年號](../Page/大彝震.md "wikilink")
      - [天啓](../Page/天启_\(劝丰佑\).md "wikilink")（840年至859年）：[南詔領袖](../Page/南詔.md "wikilink")[勸豐祐之年號](../Page/勸豐祐.md "wikilink")
      - [建極](../Page/建極.md "wikilink")（860年起）：南詔領袖[世隆之年號](../Page/世隆.md "wikilink")
      - [法堯](../Page/法堯.md "wikilink")（至877年）：南詔領袖世隆之年號

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1998年3月，ISBN 4639007116

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:840年代中国政治](../Category/840年代中国政治.md "wikilink")
[Category:850年代中国政治](../Category/850年代中国政治.md "wikilink")
[Category:860年代中国政治](../Category/860年代中国政治.md "wikilink")
[Category:870年代中国政治](../Category/870年代中国政治.md "wikilink")