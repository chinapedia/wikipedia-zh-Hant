列車棚、月台及軌道 | photo2a = USA-NYC-Grand Central Terminal
Clock.jpg{{\!}}中央大堂時鐘 | photo2b = GCT in Blizzard of
2015.jpg{{\!}}主大堂，面向東 | spacing = 2 | position = center | color_border
= white | color = white | size = 300 }} | image_caption =
由左上起順時針：42街正面、地下列車棚及軌道、主大堂、資訊台上方的標誌性時鐘 | alt =
主大堂，由大理石和石頭組成的中央資訊台以及售票窗在側邊 | address =
[紐約州](../Page/紐約州.md "wikilink")[紐約](../Page/紐約市.md "wikilink")[東42街及](../Page/42街_\(曼哈頓\).md "wikilink")[公園大道交界](../Page/公園大道.md "wikilink")89號
| country = [美國](../Page/美國.md "wikilink") | owned =

<hr/>

| operator =  | line =  | platform =
44個：43個[島式月台](../Page/島式月台.md "wikilink")，1個[側式月台](../Page/側式月台.md "wikilink")
（6條軌道使用[西班牙式月台佈局](../Page/西班牙式月台佈局.md "wikilink")） | tracks =
67條：56條客運軌道（30條上層，26條下層）
43條用作客運服務
11條側線 | other = [MTA紐約地鐵](../Page/紐約地鐵.md "wikilink")

，[大中央-42街車站](../Page/大中央-42街車站.md "wikilink")
[NYCT巴士](../Page/MTA區域巴士營運.md "wikilink")：
NYCT巴士，[MTA巴士](../Page/MTA區域巴士營運.md "wikilink")，：[快車服務](../Page/#連接服務.md "wikilink")
| depth = | levels = 2層 | parking = | bicycle = | ADA = 是 | opened = |
closed = | rebuilt = | years = 興建 | events = 1903–1913
<small>1913年2月2日啟用</small> | mpassengers =  | passengers = | pass_year
= | pass_percent = | website =  | services = | nrhp =

| baggage_check = }}

**大中央總站**（，又譯為**大中央火車總站**，[中文慣稱為](../Page/中文.md "wikilink")**大中央車站**、**中央車站**）位於[美國](../Page/美國.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")[曼哈顿](../Page/曼哈顿.md "wikilink")[中城](../Page/中城.md "wikilink")，為[大都會北方鐵路](../Page/大都會北方鐵路.md "wikilink")、[紐約地鐵](../Page/紐約地鐵.md "wikilink")[7號線](../Page/紐約地鐵7號線.md "wikilink")、[萊辛頓大道線](../Page/IRT萊辛頓大道線.md "wikilink")（[4號](../Page/紐約地鐵4號線.md "wikilink")、[5號](../Page/紐約地鐵5號線.md "wikilink")、[6號線](../Page/紐約地鐵6號線.md "wikilink")）及[S線交會的](../Page/紐約地鐵42街接駁線.md "wikilink")[地下化](../Page/铁路地下化.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。該站占地面积48英亩（19公顷），與[賓夕法尼亞車站同為大紐約地區重要的交通樞紐](../Page/賓夕法尼亞車站_\(紐約市\).md "wikilink")，也是美国最繁忙、最著名的铁路车站之一。

大中央總站座落於纽约市[42街](../Page/42街.md "wikilink")89號，42街与[公园大道](../Page/公园大道.md "wikilink")[路口](../Page/路口.md "wikilink")。如果按[月台的数量来说](../Page/月台.md "wikilink")，大中央總站是世界上公共建築空間最大的鐵路車站\[1\]，它拥有44個月台和56個[股道](../Page/股道.md "wikilink")。车站的地下部份有2层，地下一樓有30個股道，第2层則有26個股道。共有43条轨道现用于客运。邻近站台和位于站场的铁轨总数超过了100条。

## 車站架構

[Image-Grand_central_Station_Outside_Night_2.jpg](https://zh.wikipedia.org/wiki/File:Image-Grand_central_Station_Outside_Night_2.jpg "fig:Image-Grand_central_Station_Outside_Night_2.jpg")
[Grand_Central_Terminal_-_Upper_Level_Diagram_1939.jpg](https://zh.wikipedia.org/wiki/File:Grand_Central_Terminal_-_Upper_Level_Diagram_1939.jpg "fig:Grand_Central_Terminal_-_Upper_Level_Diagram_1939.jpg")
[American_flag_in_Grand_Central_Terminal.jpg](https://zh.wikipedia.org/wiki/File:American_flag_in_Grand_Central_Terminal.jpg "fig:American_flag_in_Grand_Central_Terminal.jpg")
[NYCSub_7_Grand_Central.jpg](https://zh.wikipedia.org/wiki/File:NYCSub_7_Grand_Central.jpg "fig:NYCSub_7_Grand_Central.jpg")
大中央總站為一座[布雜學院式建築](../Page/布雜藝術.md "wikilink")，是一座地下化鐵路及地鐵交會車站。

## 歷史

[1880_Grand_Central.jpg](https://zh.wikipedia.org/wiki/File:1880_Grand_Central.jpg "fig:1880_Grand_Central.jpg")

大中央總站係由美國[紐約中央鐵路](../Page/紐約中央鐵路.md "wikilink")（New York Central
Railroad）出資建造，於1913年正式啟用；並取代了之前的舊站（於1871年由[康內留斯·范德比爾特所建](../Page/康內留斯·范德比爾特.md "wikilink")）。自車站落成後，車站附近的公園大道上如雨後春筍般出現了許多飯店、辦公大樓及豪宅，也因此使這裏成為全曼哈頓島地價最高的地區。1963年，龐大的泛美大廈（現[大都會人壽大廈](../Page/大都會人壽大廈.md "wikilink")）於大中央總站北邊建成，由於其落成破壞了大中央總站附近的景致，引來抗議聲浪，但麻煩還沒結束，1968年，賓州中央鐵路公司曾計畫拆除大中央總站42街的一部分候車室，以興建辦公大樓，但由於當時美國前第一夫人[杰奎琳·肯尼迪的強烈反對](../Page/杰奎琳·肯尼迪.md "wikilink")，大中央總站才逃過改建的命運。1983年，大中央總站列入國家歷史文物保護名冊。1992年，大中央總站開始進行全面整修的工作，以應付日趨增長的通勤人潮。

### 中央大厅

中央大厅位于车站的正中央，經常挤满人潮，售票亭和詢問處就設在此處。[九一一恐怖攻擊事件之後](../Page/九一一恐怖攻擊事件.md "wikilink")，大厅上掛了一幅[美國國旗](../Page/美國國旗.md "wikilink")。中央大廳經常是人們見面的地方，而詢問處上的四面鐘是車站內最注目的地標，這四面鐘的盤面都是用[貓眼石作的](../Page/貓眼石.md "wikilink")，价值大約为1000万[美元至](../Page/美元.md "wikilink")2000万美元之間。詢問處裡，有一個祕密通道可通往地下。

[Grand_Central_Mart_Lex_Av_food_corridor_jeh.jpg](https://zh.wikipedia.org/wiki/File:Grand_Central_Mart_Lex_Av_food_corridor_jeh.jpg "fig:Grand_Central_Mart_Lex_Av_food_corridor_jeh.jpg")

大中央總站外，面對[42街的正門有一面世界最大的](../Page/42街.md "wikilink")[蒂芬妮玻璃](../Page/蒂芬妮玻璃.md "wikilink")，玻璃兩旁有[彌涅耳瓦](../Page/彌涅耳瓦.md "wikilink")、[赫耳墨斯和](../Page/赫耳墨斯.md "wikilink")[墨丘利等希臘神話神祇的](../Page/墨丘利.md "wikilink")[雕像](../Page/雕像.md "wikilink")，為世界最大型的雕像群，由[法國的雕刻家](../Page/法國.md "wikilink")[Jules-Felix
Coutan建造](../Page/Jules-Felix_Coutan.md "wikilink")，有48呎（14.6公尺）高，而鐘的周長有13呎（4公尺）長。

大廳內有許多的穿堂走道，乘客可進出不同的候車月台。[蘋果公司於](../Page/蘋果公司.md "wikilink")2011年11月在大廳二樓開設直營的[蘋果商店](../Page/蘋果商店.md "wikilink")（Apple
Store）。

### 天花板

[NYC_Grand_Central_Terminal_ceiling.jpg](https://zh.wikipedia.org/wiki/File:NYC_Grand_Central_Terminal_ceiling.jpg "fig:NYC_Grand_Central_Terminal_ceiling.jpg")
大厅裡的星空穹庐原画於1912年，由[法国艺术家](../Page/法国.md "wikilink")[保罗·塞萨尔·埃勒](../Page/保罗·塞萨尔·埃勒.md "wikilink")（）创作，但在30年代后期因脫落而重新繪製。星空图所绘的天空是反向的，范德比尔特家族的后人解释说这是从上帝的视角俯瞰星空，所以和人类的视角相反，从星空以外的世界看星空。另外，星空位置也和現在的真实位置不一样，原因是这是根据[中世纪时期的星空图描绘的](../Page/中世纪.md "wikilink")。

1998年，殘舊的屋頂進行翻修主要是為了清除屋頂的黑色污垢。翻修之前，大家都認為是那個年代的[蒸汽火車和](../Page/蒸汽火車.md "wikilink")[柴油機車燃燒的煙塵弄髒屋頂](../Page/柴油機車.md "wikilink")，經過取樣化驗後卻發現幾乎都是[尼古丁殘留物](../Page/尼古丁.md "wikilink")，反映出當時人們抽香菸的普遍程度。

### 用餐大廳和底层铁轨

用餐大廳位于中央大厅的下面。这里有很多高級餐馆和速食店，包括了世界著名的Oyster
Bar饭店。大厅可以直接通到底层铁轨，其間由數座楼梯，斜坡和電扶梯连接。

### 紐約地鐵站

## 車站周邊

  - [大都會人壽大樓](../Page/大都會人壽大樓.md "wikilink")（MetLife Building）

  - （Helmsley Building）

  - （Chanin Building）

  - （Grand Hyatt）

  - （Philip Morris Headquarters）

  - [克萊斯勒大樓](../Page/克萊斯勒大樓.md "wikilink")（Chrysler Building）

## 注釋

## 參見

  - [中央車站](../Page/中央車站.md "wikilink")
  - [紐約交通](../Page/紐約交通.md "wikilink")

## 外部連結

  - [大中央總站官方網站](http://www.grandcentralterminal.com/)

  - [大中央總站的歷史](https://web.archive.org/web/20080310203226/http://manhattan.about.com/od/historyandlandmarks/a/grandcentral.htm/)

  - [大中央總站的祕密](https://web.archive.org/web/20071218225236/http://manhattan.about.com/od/historyandlandmarks/a/secretgrandcent.htm/)

  - [大中央總站的內部](https://web.archive.org/web/20120807041148/http://channel.nationalgeographic.com/channel/insidegrandcentral/)

  - [紐約建築照片－大中央總站](http://www.nyc-architecture.com/MID/MID031.htm)

  - [電影場景內的大中央總站](https://web.archive.org/web/20080201163009/http://manhattan.about.com/od/artsandculture/tp/moviesgct.htm/)

  - [《紐約時報》刊登關於北中央總站的文章](http://query.nytimes.com/gst/fullpage.html?res=9F02E2DA1339F937A15753C1A96F958260)

  - [紐約中央車站的前世今生－談舊車站的再利用](https://web.archive.org/web/20070927184717/http://www.cc-chen.idv.tw/archzone/archive/index.php/t-1005.html)

[Category:紐約市鐵路車站](../Category/紐約市鐵路車站.md "wikilink")
[Category:曼哈頓交通](../Category/曼哈頓交通.md "wikilink")
[Category:1913年啟用的鐵路車站](../Category/1913年啟用的鐵路車站.md "wikilink")
[Category:美国建筑的世界之最](../Category/美国建筑的世界之最.md "wikilink")
[Category:交通之最](../Category/交通之最.md "wikilink")
[Category:美國地標](../Category/美國地標.md "wikilink")
[Category:曼哈顿旅游景点](../Category/曼哈顿旅游景点.md "wikilink")

1.  如果是全世界人流量最大的車站，則為[日本](../Page/日本.md "wikilink")[東京的](../Page/東京.md "wikilink")[新宿車站](../Page/新宿車站.md "wikilink")；另外[名古屋車站若加算商店街空間](../Page/名古屋車站.md "wikilink")，會比本站還大。