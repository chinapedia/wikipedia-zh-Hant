**大中山車站**（）是[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")（JR北海道）[函館本線沿線的](../Page/函館本線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")，位於[龜田郡](../Page/龜田郡.md "wikilink")[七飯町大中山](../Page/七飯町.md "wikilink")1丁目2番地。

## 車站構造

  - 全站皆位於地面上，配置有[側式月台兩座](../Page/側式月台.md "wikilink")，鐵路路線兩條，月台之間設有跨線[人行天橋](../Page/人行天橋.md "wikilink")。
  - 設有一木造平房型式的站屋，為[無人車站](../Page/無人車站.md "wikilink")。站內設置有簡易的[自動售票機](../Page/自動售票機.md "wikilink")，由[七飯車站管理](../Page/七飯車站.md "wikilink")。

### 月台配置

<table>
<caption>月台配置</caption>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>函館本線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/函館站.md" title="wikilink">函館方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/七飯站.md" title="wikilink">七飯</a>、<a href="../Page/長萬部站.md" title="wikilink">長萬部方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 歷史

  - 1946年（[昭和](../Page/昭和.md "wikilink")21年）12月1日 -
    以[臨時停車場](../Page/臨時停車場.md "wikilink")（）的定位開始營運。
  - 1950年（昭和25年）1月15日 - 升級成客運車站。
  - 1984年（昭和59年）2月1日 - 行李托運服務停止。
  - 1984年11月1日 - 降為無人車站。

<!-- end list -->

  -
    大中山車站雖然在1984年時降級為無人車站，但為了因應[日本國鐵末期人員編制過剩的情況](../Page/日本國鐵.md "wikilink")，因此在大中山車站臨時設有站務員負責車票的販賣。由於當時的管理車站為鄰近的七飯車站，因此車票上是記載著「」的字樣，並在1986年11月1日改為[簡易委託車站營運](../Page/簡易委託車站.md "wikilink")。與大中山類似的簡易委託車站還有仁山信號站（今日的[仁山車站](../Page/仁山車站.md "wikilink")，車票上是記載「」），[鹿部車站](../Page/鹿部車站.md "wikilink")（）與[渡島砂原車站](../Page/渡島砂原車站.md "wikilink")（）。

<!-- end list -->

  - 1987年（昭和62年）4月1日 -
    [國鐵分割民營化](../Page/國鐵分割民營化.md "wikilink")，車站經營權由JR北海道承接。
  - 1992年（[平成](../Page/平成.md "wikilink")4年）4月1日 - 簡易委託終止，完全無人化。

## 車站周邊

  - [國道5號](../Page/國道5號_\(日本\).md "wikilink")（名列日本街道百景之一的[赤松街道](../Page/赤松街道.md "wikilink")）
  - [橫津岳登山口](../Page/橫津岳.md "wikilink")
  - 橫津岳國際滑雪場
  - [函館巴士大中山站](../Page/函館巴士.md "wikilink")

## 相鄰車站

  - 北海道旅客鐵道（JR北海道）

    函館本線

      -

        「」

          -
            **通過**

        （包括普通「函館Liner」）

          -
            [桔梗](../Page/桔梗站.md "wikilink")（H73）－**大中山（H72）**－[七飯](../Page/七飯站.md "wikilink")（H71）

## 參考文獻

  - 《道內時刻表》2006年8月號，交通新聞社（日本，札幌）。

[Category:函館本線車站](../Category/函館本線車站.md "wikilink")
[Category:渡島管內鐵路車站](../Category/渡島管內鐵路車站.md "wikilink")
[Onakayama](../Category/日本鐵路車站_O.md "wikilink")
[Category:七飯町](../Category/七飯町.md "wikilink")
[Category:1946年啟用的鐵路車站](../Category/1946年啟用的鐵路車站.md "wikilink")