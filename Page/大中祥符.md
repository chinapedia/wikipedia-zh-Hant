**大中祥符**（1008年—1016年）是[宋真宗的第三个](../Page/宋真宗.md "wikilink")[年號](../Page/年號.md "wikilink")，[北宋使用这个年号共](../Page/北宋.md "wikilink")9年。

## 改元原因和涵義

**祥符**意即**祥瑞的符籙**，据《[宋史](../Page/宋史.md "wikilink")·本纪第七》：“大中祥符元年春正月乙丑，有黄帛曳左承天门南鸱尾上，守门卒涂荣告，有司以闻。上召群臣拜迎于朝元殿启封，号称天书。丁卯，紫云见，如龙凤覆宫殿。戊辰，大赦，改元。”大意為:某天承天門南角出現一條上面有文字的黃帛，宋真宗聽聞後說他曾夢見神仙，神仙說會降下天書三篇，名曰《大中祥符》，就是這條黃帛了。黃帛從城門上取下，上面寫滿了為宋真宗歌功頌德的文字。真宗大喜，當場大赦並改元為“大中祥符”。

## 出生

  - [大中祥符元年](../Page/1008年.md "wikilink")——[狄青](../Page/狄青.md "wikilink")，北宋军事人物
  - 大中祥符元年——[苏舜钦](../Page/苏舜钦.md "wikilink")，北宋诗人
  - [大中祥符二年](../Page/1009年.md "wikilink")——[苏洵](../Page/苏洵.md "wikilink")，北宋文学家
  - [大中祥符四年](../Page/1011年.md "wikilink")——[邵雍](../Page/邵雍.md "wikilink")，北宋理学家
  - [大中祥符五年](../Page/1012年.md "wikilink")——[蔡襄](../Page/蔡襄.md "wikilink")，北宋大臣，书法家
  - 大中祥符五年[十二月十日](../Page/十二月十日.md "wikilink")——[胡渊](../Page/胡渊.md "wikilink")，北宋大臣

## 逝世

  - [大中祥符四年](../Page/1011年.md "wikilink")——[吕蒙正](../Page/吕蒙正.md "wikilink")，北宋大臣

## 大事記

  - [大中祥符六年](../Page/1013年.md "wikilink")[八月十三日](../Page/八月十三日.md "wikilink")——《[冊府元龜](../Page/冊府元龜.md "wikilink")》成书。
  - [大中祥符八年四月壬申](../Page/1015年.md "wikilink")\[1\]——禁中大火，荣王[赵元俨宫最先起火](../Page/趙元儼.md "wikilink")，自三鼓至翌日亭午仍止，延烧内藏左藏库、朝元门、崇文院、秘阁。宋真宗为此下[罪已诏](../Page/罪已诏.md "wikilink")，令[丁谓](../Page/丁谓.md "wikilink")\]等负责修复。

## 紀年

| 大中祥符                           | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 1008年                          | 1009年                          | 1010年                          | 1011年                          | 1012年                          | 1013年                          | 1014年                          | 1015年                          | 1016年                          |
| [干支](../Page/干支.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") |

## 參考

  - 同期存在的其他政權之紀年
      - [统和](../Page/统和.md "wikilink")（983年至1012年）：[契丹](../Page/辽朝.md "wikilink")—[辽圣宗耶律隆绪之年號](../Page/辽圣宗.md "wikilink")
      - [開泰](../Page/開泰_\(遼聖宗\).md "wikilink")（1012年十一月至1021年十一月）：[契丹](../Page/遼朝.md "wikilink")—遼聖宗耶律隆绪之年號
      - [廣明](../Page/廣明_\(段素英\).md "wikilink")：[大理國](../Page/大理國.md "wikilink")—[段素英](../Page/段素英.md "wikilink")（985年至1009年在位）之年號
      - [廣德](../Page/廣德.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明治](../Page/明治.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明應](../Page/明應.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明統](../Page/明統.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明德](../Page/明德.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明聖](../Page/明聖.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明法](../Page/明法.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明運](../Page/明運.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明啟](../Page/明啟.md "wikilink")（1010年至1022年在位）：大理國—[段素廉之年號](../Page/段素廉.md "wikilink")
      - [寬弘](../Page/寬弘.md "wikilink")（1004年至1013年）：[日本](../Page/日本.md "wikilink")—[一條天皇與](../Page/一條天皇.md "wikilink")[三條天皇之年號](../Page/三條天皇.md "wikilink")
      - [長和](../Page/長和.md "wikilink")（1013年至1017年）：日本—三條天皇與[後一條天皇之年号](../Page/後一條天皇.md "wikilink")
      - [景瑞](../Page/景瑞_\(黎龙铤\).md "wikilink")（1008年至1010年）：前黎朝—黎龍鋌之年號
      - [順天](../Page/順天_\(李公蘊\).md "wikilink")（1010年至1028年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李公蘊之年號](../Page/李公蘊.md "wikilink")

[Category:北宋年号](../Category/北宋年号.md "wikilink")
[Category:11世纪中国年号](../Category/11世纪中国年号.md "wikilink")
[Category:1000年代中国政治](../Category/1000年代中国政治.md "wikilink")
[Category:1010年代中国政治](../Category/1010年代中国政治.md "wikilink")

1.  《[续资治通鉴长编](../Page/续资治通鉴长编.md "wikilink")·卷八十四》