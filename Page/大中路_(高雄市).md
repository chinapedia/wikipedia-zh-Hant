**大中路（Dazhong
Rd.）**是[高雄市北高雄地區的東西向重要道路](../Page/高雄市.md "wikilink")，本道路共分為二個部分，由[仁武區](../Page/仁武區.md "wikilink")[澄觀路西行橫跨](../Page/澄觀路.md "wikilink")[鼎金系統交流道進](../Page/鼎金系統交流道.md "wikilink")[左營區續接](../Page/左營區.md "wikilink")。為銜接[國道十號與](../Page/國道十號_\(中華民國\).md "wikilink")[高雄都會區快速道路的橋下平面道路](../Page/高雄都會區快速道路.md "wikilink")，沿高架橋下直行可通往[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
[高鐵左營站](../Page/高鐵左營站.md "wikilink")，並跨過[縱貫鐵路至](../Page/縱貫線_\(南段\).md "wikilink")[翠華路口往](../Page/翠華路_\(高雄市\).md "wikilink")[蓮池潭風景區](../Page/蓮池潭.md "wikilink")。

## 行經行政區域

（由東至西）

  - [三民區](../Page/三民區.md "wikilink")
  - [左營區](../Page/左營區.md "wikilink")

## 分段

大中路共分成二個部分，為[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[國道10號與](../Page/國道十號_\(中華民國\).md "wikilink")[高雄都會區快速道路高架橋下道路](../Page/高雄都會區快速道路.md "wikilink")：

  - 大中一路：東起於橫跨[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[鼎金系統交流道接](../Page/鼎金系統交流道.md "wikilink")[仁武區](../Page/仁武區.md "wikilink")[澄觀路](../Page/澄觀路.md "wikilink")，西於[民族一路口接大中二路](../Page/民族路_\(高雄市\).md "wikilink")。
  - 大中二路：東於民族一路口接大中一路，西於[華夏路口橫跨](../Page/華夏路_\(高雄市\).md "wikilink")[縱貫鐵路抵](../Page/縱貫線_\(南段\).md "wikilink")[翠華路口](../Page/翠華路_\(高雄市\).md "wikilink")。

## 歷史

## 沿線設施

（由東至西）

  - 大中一路
      - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[鼎金系統交流道](../Page/鼎金系統交流道.md "wikilink")
      - [高雄榮民總醫院](../Page/高雄榮民總醫院.md "wikilink")
          - 榮總郵局（高雄榮民總醫院內）
          - 榮總圖書館（高雄榮民總醫院內）

<!-- end list -->

  - 大中二路
      - [高雄市立新莊高級中學](../Page/高雄市立新莊高級中學.md "wikilink")
      - [高雄市左營區福山國中](../Page/高雄市立福山國民中學.md "wikilink")
      - [TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[左營端](../Page/左營端.md "wikilink")
      - [高雄都會區快速道路](../Page/高雄都會區快速道路.md "wikilink")
      - [大肚魚游泳池](../Page/大肚魚游泳池.md "wikilink")（已歇業，現址為車廠）
      - [可利亞餐廳](../Page/可利亞餐廳.md "wikilink")（博愛分店）
      - 大中路機慢車地下道（華夏路到翠華路間）
      - [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
        [高鐵左營站](../Page/高鐵左營站.md "wikilink")

### 鄰近商圈

  - [榮總商圈](../Page/榮總商圈.md "wikilink")

## 公車資訊

  - [南台灣客運](../Page/南台灣客運.md "wikilink")
      - 28
        [加昌站](../Page/加昌站_\(公車\).md "wikilink")－楠梓區公所－[高雄火車站](../Page/高雄火車站.md "wikilink")
      - 91
        [金獅湖站](../Page/金獅湖站.md "wikilink")－[歷史博物館](../Page/歷史博物館.md "wikilink")
      - 紅50
        [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
        [高鐵左營站](../Page/新左營車站.md "wikilink")－捷運[生態園區站](../Page/生態園區站.md "wikilink")
  - [港都客運](../Page/港都客運.md "wikilink")
      - 38 左營南站－[榮民總醫院](../Page/高雄榮民總醫院.md "wikilink")
      - 39 左營南站－榮民總醫院
  - [漢程客運](../Page/漢程客運.md "wikilink")
      - 72
        [金獅湖站](../Page/金獅湖站.md "wikilink")－[中正高工](../Page/高雄市立中正高級工業職業學校.md "wikilink")
      - 92 自由幹線 金獅湖站－高雄火車站
  - [高雄客運](../Page/高雄客運.md "wikilink")
      - 24A 坔埔圓照寺－ 捷運[巨蛋站](../Page/巨蛋站.md "wikilink")
      - 24B 楠梓－大社－大灣－高雄
      - 8021 鳳山－彌陀
      - 8025
        高雄火車站－[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[國道十號](../Page/國道十號_\(中華民國\).md "wikilink")－旗山－美濃－六龜－寶來（返程經[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
        [高鐵左營站](../Page/新左營車站.md "wikilink")）
      - 8028
        高雄火車站－[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")國道十號－旗山－美濃
      - 8032
        高雄火車站－[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")國道十號－旗山－甲仙
      - 8038
        高雄火車站－[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")國道十號－旗山－美濃－六龜－寶來
  - [高雄客運](../Page/高雄客運.md "wikilink")、[屏東客運](../Page/屏東客運.md "wikilink")、[中南客運](../Page/中南客運.md "wikilink")、[國光客運聯營](../Page/國光客運.md "wikilink")
      - 9189「墾丁快捷」（經[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[TWHW10.svg](https://zh.wikipedia.org/wiki/File:TWHW10.svg "fig:TWHW10.svg")[鼎金系統交流道](../Page/鼎金系統交流道.md "wikilink")、[TW_PHW88.svg](https://zh.wikipedia.org/wiki/File:TW_PHW88.svg "fig:TW_PHW88.svg")[台88線](../Page/台88線.md "wikilink")、[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號](../Page/福爾摩沙高速公路.md "wikilink")）
          - [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
            [高鐵左營站](../Page/新左營車站.md "wikilink")－大鵬灣－枋寮－楓港－恆春－墾丁

<!-- end list -->

  - [義大客運](../Page/義大客運.md "wikilink")
      - 紅52
        [義大醫院](../Page/義大醫院.md "wikilink")－[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
        [高鐵左營站](../Page/新左營車站.md "wikilink")

## 近期施工

  - [鐵路地下化](../Page/鐵路地下化.md "wikilink")
  - 大中路匝道配置

## 相關連結

  - [高雄市主要道路列表](../Page/高雄市主要道路列表.md "wikilink")

[Category:高雄市街道](../Category/高雄市街道.md "wikilink")