**Big5**，又稱為**大五碼**或**五大碼**，是使用[繁体中文](../Page/繁体中文.md "wikilink")（正-{}-體中文）社群中最常用的電腦[漢字](../Page/汉字.md "wikilink")[字符集標準](../Page/字符集.md "wikilink")，共收錄13,060個漢字\[1\]。

中文碼分為[內碼及](../Page/內碼.md "wikilink")[交換碼兩類](../Page/交換碼.md "wikilink")，Big5屬中文內碼，知名的中文交換碼有[CCCII](../Page/CCCII.md "wikilink")、[CNS11643](../Page/CNS11643.md "wikilink")。

Big5雖普及於[台灣](../Page/台灣.md "wikilink")、[香港與](../Page/香港.md "wikilink")[澳門等繁體中文通行區](../Page/澳門.md "wikilink")，但長期以來並非當地的國家/地區標準或官方標準，而只是**[業界標準](../Page/de_facto.md "wikilink")**。[倚天中文系統](../Page/倚天中文系統.md "wikilink")、[Windows繁體中文版等主要系統的字符集都是以Big](../Page/Windows.md "wikilink")5為基準，但廠商又各自增加不同的造字與造字區，衍生成多種不同版本。

2003年，Big5收錄到CNS11643中文標準交換碼的附錄當中，取得了較正式的地位。這個最新版本稱為**Big5-2003**。

## 歷史及名稱

「大五碼」（）是由台灣[財團法人資訊工業策進會為](../Page/資訊工業策進會.md "wikilink")[五大中文套裝軟體所設計的中文共通內碼](../Page/五大中文套裝軟體.md "wikilink")，在1983年12月完成公告\[2\]\[3\]，隔年3月，資訊工業策進會與[臺灣](../Page/臺灣.md "wikilink")13家廠商簽定｢16位元個人電腦套裝軟體合作開發（BIG-5）計畫（[五大中文套裝軟體](../Page/五大中文套裝軟體.md "wikilink")）｣\[4\]，因為此中文內碼是為臺灣自行製作開發之「五大中文套裝軟體」所設計的，所以就稱為Big5中文內碼\[5\]\[6\]\[7\]\[8\]。五大中文套裝軟體雖然並沒有如預期的取代國外的套裝軟體，但隨著採用Big5碼的[國喬中文系統及](../Page/國喬中文系統.md "wikilink")[倚天中文系統先後在台灣市場獲得成功](../Page/倚天中文系統.md "wikilink")，使得Big5碼深遠地影響正體中文電腦[內碼](../Page/內碼.md "wikilink")，直至今日。「五大碼」的英文名稱「」後來被人按英文字序譯回中文，以致現在有「五大碼」和「大五碼」兩個中文名稱。

Big5碼的產生，是因為當時[個人電腦沒有共通的內碼](../Page/個人電腦.md "wikilink")，導致廠商推出的中文應用軟體無法推廣，並且與[IBM
5550](../Page/IBM_5550.md "wikilink")、[王安碼等內碼](../Page/王安碼.md "wikilink")，彼此不能兼容；另一方面，台灣當時尚未推出中文編碼標準。在這樣的時空背景下，為了使台灣早日進入資訊時代，所採行的一個計畫；同時，這個計畫對於以台灣為核心的亞洲繁體漢字圈也產生了久遠的影響。

Big5產生前，研發[中文電腦的](../Page/中文電腦.md "wikilink")[朱邦復認為內碼字集應該廣納所有的正異體字](../Page/朱邦復.md "wikilink")，以顧及如戶政等應用上的需要，故在當時的內碼會議中，建議希望採用他的五萬多字的字庫。工程師認為雖其技術可行，但是三個位元組（超過兩個位元組）長度的內碼卻會造成英文螢幕畫面映射成中文畫面會發生文字無法對齊的問題，因為當時盛行之倚天中文系統畫面係以兩個位元組文字寬度映射成一個中文字圖樣，英文軟體中只要以兩個英文字寬度去顯示一個中文字，畫面就不會亂掉，造成中文系統業者偏愛二個位元組長度的內碼\[9\]；此外以倉頡輸入碼壓縮成的內碼不具排序等功能，因此未被採用。1983年有人誣指朱邦復為[共產黨](../Page/共產黨.md "wikilink")，其研究成果更不可能獲採用。\[10\]

在Big5碼誕生後，大部分台灣的電腦軟體都使用了Big5碼，加上後來[倚天中文系統的高度普及](../Page/倚天中文系統.md "wikilink")，使後來的[微軟](../Page/微软.md "wikilink")[Windows
3.x等亦予以採用](../Page/Windows_3.x.md "wikilink")。雖然後來台灣還有各種想要取代Big5碼，像是倚天中文系統所推行的[倚天碼](../Page/倚天碼.md "wikilink")、台北市電腦公會所推動的[公會碼等](../Page/公會碼.md "wikilink")，但是由於Big5字碼已沿用多年，因此在習慣不易改變的情況下，始終無法成為主流字碼。而台灣後來發展的[國家標準CNS
11643中文標準交換碼由於非一般的內碼系統](../Page/CNS11643.md "wikilink")，是以交換使用為目的，受先天所限，必須使用至少三個位元組來表示一個漢字，所以普及率遠遠不及Big5碼。

在1990年代初期，當[中國大陸的](../Page/中國大陸.md "wikilink")[電郵和轉碼軟體還未普遍之時](../Page/電郵.md "wikilink")，在[深圳的港商和台商公司亦曾經使用Big](../Page/深圳.md "wikilink")5系統，以方便與總部的文件交流、以及避免為大陸的辦公室再寫一套不同內碼的系統。使用[简体中文的社群](../Page/简体中文.md "wikilink")，最常用的是[GB
2312](../Page/GB_2312.md "wikilink")、[GBK及其後續的](../Page/GBK.md "wikilink")[國標碼](../Page/国家标准代码.md "wikilink")（[GB
18030](../Page/GB_18030.md "wikilink")）。

除了台灣外，其他使用繁體漢字的地區，如[香港](../Page/香港.md "wikilink")（[香港增補字符集](../Page/香港增補字符集.md "wikilink")）、[澳門](../Page/澳門.md "wikilink")（[澳門增補字符集](../Page/澳門增補字符集.md "wikilink")），及使用繁體漢字的海外華人，都曾普遍使用Big5碼做為中文內碼及交換碼。

## 中文碼相關標準歷程

|              |                                                                                                                 |
| ------------ | --------------------------------------------------------------------------------------------------------------- |
| nowrap|1983年 | 「通用漢字標準交換碼」試用版發行，包括13,053個字與441個符號，分為二個字面，先筆畫數，後部首序排列。 12月：Big-5大五碼，包括13,053個字與441個符號，字集與字序與交換碼試用版完全相同，僅字碼定義不同。 |
| 1984年        | 3月：臺灣資策會與其國內13家廠商簽定｢五大中文套裝軟體｣開發計畫，而｢大五碼｣即是為｢五大中文套裝軟體｣所設計之中文內碼。                                                  |
| 1986年        | 國家標準「CNS 11643通用漢字標準交換碼」正式版發行，包括13,051個字（刪除2個重複字，調整20個字順序）與441個符號，其餘均與試用版相同。                                    |
| 1988年        | 公佈「通用漢字標準交換碼」－使用者加字區交換碼，即第十四字面，共6,148字。                                                                         |
| 1989年        | 再公佈使用者加字區交換碼（增編）157字。                                                                                           |
| 1992年        | 國家標準「CNS 11643中文標準交換碼」新版發行，擴充第3-7字面並更換名稱，總共包括48,027個字與684個符號（增加部首和數字符號）。                                        |
| 1997年        | Big-5E大五碼擴充，同1984年版，包括13,053個字與441個符號，另於造字區定義3,954個較常使用的造字。                                                     |
| 2002年        | 國際標準ISO 10646 / Unicode的中文版「CNS 14649廣用多八位元編碼字元集」，包括中、日、韓、越等20,902個漢字（現已擴充至十萬字左右），及全球使用的字符。                     |
| 2008年        | 國家標準「CNS 11643中文標準交換碼」擴充版發行，增加了戶政用字與異體字等。                                                                       |

## 字節結構

Big5碼是一套[雙位元組字符集](../Page/雙位元組字符集.md "wikilink")，使用了雙八碼儲存方法，以兩個字節來安放一個字。第一個字節稱為「高位字節」，第二個字節稱為「低位字節」。

「高位字節」使用了0x81-0xFE，「低位字節」使用了0x40-0x7E，及0xA1-0xFE。在Big5的分區中：

<table>
<tbody>
<tr class="odd">
<td><p>0x8140-0xA0FE</p></td>
<td><p>保留給使用者自定義字元（<a href="../Page/造字.md" title="wikilink">造字區</a>）</p></td>
</tr>
<tr class="even">
<td><p>0xA140-0xA3BF</p></td>
<td><p><a href="../Page/標點符號.md" title="wikilink">標點符號</a>、<a href="../Page/希腊字母.md" title="wikilink">希臘字母及特殊符號</a>，<br />
包括在0xA259-0xA261，安放了九個<a href="../Page/計量用漢字.md" title="wikilink">計量用漢字</a>：-{兙兛兞兝兡兣嗧瓩糎}-。</p></td>
</tr>
<tr class="odd">
<td><p>0xA3C0-0xA3FE</p></td>
<td><p>保留。此區沒有開放作造字區用。</p></td>
</tr>
<tr class="even">
<td><p>0xA440-0xC67E</p></td>
<td><p><a href="../Page/常用國字標準字體表.md" title="wikilink">常用漢字</a>，先按<a href="../Page/笔划.md" title="wikilink">筆劃再按</a><a href="../Page/部首.md" title="wikilink">部首排序</a>。</p></td>
</tr>
<tr class="odd">
<td><p>0xC6A1-0xC8FE</p></td>
<td><p>保留給使用者自定義字元（造字區）</p></td>
</tr>
<tr class="even">
<td><p>0xC940-0xF9D5</p></td>
<td><p><a href="../Page/次常用國字標準字體表.md" title="wikilink">次常用漢字</a>，亦是先按筆劃再按部首排序。</p></td>
</tr>
<tr class="odd">
<td><p>0xF9D6-0xFEFE</p></td>
<td><p>保留給使用者自定義字元（造字區）</p></td>
</tr>
</tbody>
</table>

值得留意的是，Big5重複收錄了兩個相同的字：「兀、兀」（0xA461\[U+5140\]及0xC94A\[U+FA0C\]）、「嗀、嗀」（0xDCD1\[U+55C0\]及0xDDFC\[U+FA0D\]）。此外「十」、「卅」也在符號區又重複了一次，在檢索系統中常會造成查詢不到字。

### 衝碼問題

因為低位元字元中包含了[程式語言](../Page/程式語言.md "wikilink")、[shell](../Page/Unix_shell.md "wikilink")、[script中](../Page/腳本語言.md "wikilink")，字串或命令常會用到的特殊字元，例如0x5C“\\”、0x7C“|”等。「\\」在許多用途的字串中是當作轉義符號，例如\\n（換行）、\\r（歸位）、\\t（tab）、\\\\（\\本身符號）、\\"（引號）等等。而「|」在[UNIX作業系統中大多當作命令管線的使用](../Page/UNIX.md "wikilink")，如"ls
-la |
more"等等。如果在字串中有這些特殊的轉義字元，會被[程式或](../Page/程式.md "wikilink")[直譯器解釋為特殊用途](../Page/直譯器.md "wikilink")。但是因為是中文的原因，故無法正確解釋為上面所述的行為，因此程式可能會忽略此轉義符號或是誤當作轉義符號而中斷執行。若此，就違反了使用者本來要當成[中文字元一部份使用的本意](../Page/中文字元.md "wikilink")。\[11\]

低位元字元與ASCII重疊的字元如下︰

    @ A-Z [ \ ] ^ _ ` a-z { | } ~

在常用字如「-{功}-」（0xA55C）、「-{許}-」（0xB35C）、「-{蓋}-」（0xBB5C）、「-{育}-」（0xA87C）中時常出現，造成了許多軟體無法正確處理以Big5編碼的字串或文件。這個問題被戲謔性地人名化，稱為「**-{許功蓋}-**」或「**-{許蓋功}-**」（這三個字都有這種問題）。

一般的解決方法，是額外增加“\\”的字元，因為“\\\\”會被解釋為“\\”，所以“成功\\因素”這個字串就能無誤地被程式當作“成功因素”的字串來處理。但是額外的困擾是，有些輸出功能並不會把“\\”當作特殊字元看待，所以有些程式或網頁就會錯誤地常常出現在「許功蓋」這些字後面多了“\\”。\[12\]

#### 與畫線字元相衝

Big5碼字元的首位元組會與DOS[代碼頁437的](../Page/代碼頁437.md "wikilink")[畫線字元相衝而產生亂碼](../Page/方框绘制字符.md "wikilink")。

例如

`┌───────┐`
`│維基百科│`
`└───────┘`

當中的中文字如以Big5碼儲存，顯示時會變成：

`┌────────┐`
`竟�穧坌鮈�`
`└────────┘`

也有替代方法就是用[全形劃線符號](../Page/方框绘制字符#Big5.md "wikilink")，但較佔空間。倚天公司另外推出[倚天碼解決此問題](../Page/倚天碼.md "wikilink")，但不流行。

### 私人造字區

在[倚天中文系統](../Page/倚天中文系統.md "wikilink")，以及後來的[Windows
3.1](../Page/Windows_3.x.md "wikilink")、[95及](../Page/Windows_95.md "wikilink")[98中](../Page/Windows_98.md "wikilink")，定義四個私人造字區範圍：0xFA40-0xFEFE、0x8E40-0xA0FE、0x8140-0x8DFE、0xC6A1-0xC8FE。

私人造字區的原意，是供使用者加入本來在編碼表中缺少的字元，但當每個使用者都在不同的地方加上不同的字元後，當交換資料時，對方便難以知道某一個編碼究竟想表達什麼字。

## 影響

自中文電腦流行後，由於很多日常用字被視為[異體字而未收錄](../Page/異體字.md "wikilink")。很多人，甚至[電視台的字幕](../Page/電視台.md "wikilink")、[報紙的用字習慣都被改變](../Page/報紙.md "wikilink")。

例如台灣教育部視「-{着}-」為「-{著}-」的異體字，故沒有收錄「-{着}-」字。康熙字典中的一些部首用字（如「-{亠}-」、「-{疒}-」、「-{辵}-」、「-{癶}-」等）、常見的人名用字（如「-{堃}-」（[台灣前行政院長](../Page/台灣.md "wikilink")[游錫堃](../Page/游錫堃.md "wikilink")）、「-{煊}-」（台灣前[監察院院長](../Page/監察院.md "wikilink")、前財政部長[王建煊](../Page/王建煊.md "wikilink")）、「-{栢}-」（歌手[張栢芝](../Page/張栢芝.md "wikilink")）、「-{峯}-」（歌手[吳青峯](../Page/吳青峯.md "wikilink")、[林峯](../Page/林峯.md "wikilink")）、「-{喆}-」（歌手[陶喆](../Page/陶喆.md "wikilink")）等），雖被中文社會廣泛採用，也沒有收錄到Big5之中。

另外像臺灣的[廍](../Page/糖廍.md "wikilink")，閩南語指製糖所，常見於鄉間地名。但由於大五碼未收此字，也被被「廓」、「部」代替。

在互聯網上，經常能見到把游錫堃、王建煊、張栢芝、陶喆等名字，寫成為「游錫方方土」、「王建火宣」、「張木百芝」和「陶吉吉」等。電視上日本動畫的中文字幕中也會看到像**“-{木堅}-”**（-{樫}-）這樣的字。

### Big5未收錄字舉例

  - 在[倉頡輸入法中卻可輸入](../Page/倉頡輸入法.md "wikilink")。
  - 「邨」、「-{着}-」及「-{綫}-」在香港極為常用。而且「邨」和「村」、「-{着}-」和「-{著}-」在香港有客觀的字義分工，不能視爲異體字。
  - 塵「蟎」因為現代人體質易過敏、環境常有過敏源，「蟎」很常用。

| 未收錄的字  | 有收錄的字  | 原因                             | 倉頡碼      |
| ------ | ------ | ------------------------------ | -------- |
| \-{綫}- | \-{線}- | [俗字](../Page/俗字.md "wikilink") | 女火戈戈     |
| \-{綉}- | \-{繡}- | 俗字                             | 女火竹木尸    |
| \-{銹}- | \-{鏽}- | 俗字                             | 金竹木尸     |
| \-{滙}- | \-{匯}- | 異體字                            | 水尸人土     |
| \-{栢}- | \-{柏}- | 異體字                            | 木一日      |
| \-{峯}- | \-{峰}- | 異體字                            | 山竹水十（2）  |
| \-{頴}- | \-{穎}- | 異體字                            | 心火一月金（3） |
| \-{邨}- | \-{村}- | 被認為是異體字                        | 心山弓中（2）  |
| \-{着}- | \-{著}- | 被認為是異體字                        | 廿手月山     |
| \-{双}- | \-{雙}- | 俗字                             | 水水（2）    |
| \-{啓}- | \-{啟}- | 異體字                            |          |

## Big5延伸

由於Big5碼內的一萬多個字，只是根據[中華民國教育部頒布的](../Page/中華民國教育部.md "wikilink")《[常用國字標準字體表](../Page/常用國字標準字體表.md "wikilink")》、《[次常用國字標準字體表](../Page/次常用國字標準字體表.md "wikilink")》等用字匯編而成，並沒有考慮社會上流通的人名、地名用字、方言用字、化學及生物科学等用字，亦沒有放入[日語](../Page/日语.md "wikilink")[平假名及](../Page/平假名.md "wikilink")[片假名字母](../Page/片假名.md "wikilink")。

所以在市面上支援Big5碼的軟件，有不少都自行在原本的編碼外，添加一些符號及用字。

### 非官方Big5延伸

#### 倚天Big5延伸

[倚天中文系統為與](../Page/倚天中文系統.md "wikilink")[IBM5550碼相容](../Page/IBM5550碼.md "wikilink")，在Big5碼添加了以下字元，稱為[倚天擴充字集](../Page/倚天擴充字集.md "wikilink")：

  - 在0xA3C0-0xA3E0，添加了33個[控制字符圖象](../Page/控制字符.md "wikilink")。
  - 罕用符號區。在0xC6A1-0xC875，添加了圓形1-10、括號1-10、小寫[羅馬數字](../Page/羅馬數字.md "wikilink")ⅰ、ⅱ、ⅲ……ⅸ、ⅹ等章節符號、一些[部首及](../Page/部首.md "wikilink")[筆劃結構](../Page/筆劃.md "wikilink")，[日語](../Page/日语.md "wikilink")[平假名](../Page/平假名.md "wikilink")、[片假名及](../Page/片假名.md "wikilink")[俄語使用的](../Page/俄语.md "wikilink")[西里爾字母](../Page/西里尔字母.md "wikilink")。
  - 在0xF9D6-0xF9FE，添加了7個[倚天擴充字](../Page/倚天擴充字.md "wikilink")：-{碁、銹、裏、墻、恒、粧、嫺}-和34個製表符號和區塊元件。

| \! 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07   | 08 | 09 | 0A | 0B | 0C | 0D  | 0E | 0F |
| ----- | -- | -- | -- | -- | -- | -- | ---- | -- | -- | -- | -- | -- | --- | -- | -- |
| F9D0  |    |    |    |    |    |    | \-{碁 | 銹  | 裏  | 墻  | 恒  | 粧  | 嫺}- | ╔  | ╦  |
| F9E0  | ╠  | ╬  | ╣  | ╚  | ╩  | ╝  | ╒    | ╤  | ╕  | ╞  | ╪  | ╡  | ╘   | ╧  | ╛  |
| F9F0  | ╥  | ╖  | ╟  | ╫  | ╢  | ╙  | ╨    | ╜  | ║  | ═  | ╭  | ╮  | ╰   | ╯  | ▓  |

這個延伸有時稱為Big5-Eten。由於倚天中文系統是[Windows
95推出之前市場佔有率最高的中文系統](../Page/Windows_95.md "wikilink")，此延伸是各種非官方延伸當中最重要的一個。

在後期版本的倚天中文系統中，更加入了一些圖案和簡體中文字，但未被廣泛接受。

#### Code Page 950

[Windows使用的](../Page/Microsoft_Windows.md "wikilink")[Code Page
950](../Page/Code_Page_950.md "wikilink")（係參照IBM Big 5碼的編碼頁號Code Page
950，簡稱CP950）之中，只添加了上述0xF9D6-0xF9FE的倚天擴充字及表格符號，並沒有加入日文假名字母等其他延伸。

在[Windows
ME之中](../Page/Windows_ME.md "wikilink")，微軟首度在0xA3E1加入了歐元（[€](../Page/€.md "wikilink")）符號，之後所有Windows版本的Code
Page 950也都有這個符號。

#### 中國海字集

「[中國海字集](../Page/中國海字集.md "wikilink")」是中國海公司所出品的繁體漢字造字檔。它本身雖然是一套商品，但中國海公司很少將之單獨販售，往往是與其他軟體一同銷售。例如：[中國海字集就曾經與](../Page/中國海字集.md "wikilink")《漢書》、《輕鬆輸入法》等一同發售。由於它包括了不少社會上常見的用字、日文假名、和字等，加上曾與[Office
97中文版一併發售](../Page/Microsoft_Office.md "wikilink")，所以比起其他官方Big5延伸，更被台灣民眾所接受。香港部份BBS網絡在[香港增补字符集未出現之前](../Page/香港增補字符集.md "wikilink")，一度以中國海字集為標準。

#### 日和字集

「日和字集」乃一香港個人開發的造字檔，以兼容[香港增補字符集為賣點](../Page/香港增補字符集.md "wikilink")，為字集中仍沒函蓋的[日本漢字和](../Page/日本漢字.md "wikilink")[日本國字作增補](../Page/日文漢字#國字.md "wikilink")，並附有倉頡、速成等輸入法作輔助。

#### Unicode補完計畫

「Unicode補完計畫」前稱「BIG5 Extension」，透過修改Microsoft
Windows及[Mozilla的編碼表](../Page/Mozilla.md "wikilink")，從而使用者能在網上傳遞及交換文字。

有鑑於「中國海字集」的成功，「Unicode補完計畫」第二版採用了「中國海字集」原有的造字，再加上「中國海字集」所欠的部分簡體中文字及香港粵語用字，建成一個能在Big5及Unicode之間轉換的編碼表；該計劃目前已推出了64位元測試版。

### 官方Big5延伸

#### 中華民國教育部造字檔

[中華民國教育部有它本身的一套造字檔](../Page/中華民國教育部.md "wikilink")，主要給部門內使用，亦於教育部的網路字典使用。

#### 中華民國行政院農委會常用中文外字集

[中華民國行政院](../Page/中華民國行政院.md "wikilink")[農業委員會曾制訂一套有](../Page/行政院農業委員會.md "wikilink")133個漢字的造字檔，其中有84個是魚字部漢字、7個是鳥字部漢字。

#### Big5+

1997年中華民國行政院研究發展考核委員會成立專案委託[中文數位化技術推廣基金會](../Page/中文數位化技術推廣基金會.md "wikilink")（中推會）辦理Big5+擴編計畫，使用了兩萬多碼位，納入了Unicode
1.1下所有漢字。由於編碼使用到的範圍超過原先Big5定義（Big5+使用了高位元組0x81-0xFE，低位元組0x40-0x7E、0x80-0xFE），無法安裝在Microsoft
Windows上，現幾乎無人使用。

#### Big-5E

為了使Microsoft
Windows使用者可以使用造字檔，中華民國行政院研考會再度委託中推會推出一個補充字集Big-5E（與Big5+並不兼容），共收3954字。因為Big5+的編碼限制，再加上Unicode已漸成氣候，除了部分政府單位使用之外，Big-5E並沒有被廣泛的接受。[Mac
OS X 10.4及以上支援Big](../Page/Mac_OS_X_10.4.md "wikilink")-5E。

#### Big5-2003

鑑於Big5不是官方標準，中推會接受經濟部標準檢驗局委託，召集台灣國內業者、專家和學者編製一個Big5的對照表，並把它放到台灣官方的[CNS 11643附錄裡](../Page/國家標準中文交換碼.md "wikilink")，正式成為官方標準的一部分。

在Big5-2003之中，收錄了所有在1984年Big5編碼的所有字元，另外再加入微軟CP950的歐元符號、倚天延伸字集的0xA3C0-0xA3E0、0xC6A1-0xC7F2、0xF9D6-0xF9FE的用字。Big5-2003沒有收錄[行列輸入法特殊符號及](../Page/行列輸入法.md "wikilink")0xC7F3-0xC875的俄語西里爾字母，理由是以CNS 11643沒有這些字符。除此之外，所有倚天延伸全部收錄。

相對於Big5-2003，最早沒有加上延伸之Big5則對稱為Big5-1984。

#### 香港增補字符集

香港增補字符集（Hong Kong Supplementary Character
Set，簡稱HKSCS）是[香港政府基於大五碼之上擴展的字符集標準](../Page/香港政府.md "wikilink")，是現時香港的中文資訊交換內碼標準。香港增補字符集以前稱為《政府通用字庫》，本來只是香港政府內部統一使用的造字檔，有三千多字。但由於香港電腦業界不斷要求政府迎合本地需要，提出官方的字符集方案，以便與政府進行文件來往，於是香港政府便在1995年把這個內部使用的標準公開。到了1999年，此字集增加到四千多字，並改為現名。

此字符集由中文界面諮詢委員會管理，仍在不斷擴編之中。字符集主要包括[香港地名](../Page/香港地方.md "wikilink")、人名用漢字、[粵語用字](../Page/粵語.md "wikilink")（包括粗言穢語在內，這是應[警方及](../Page/警方.md "wikilink")[法庭需要記錄](../Page/法庭.md "wikilink")[口供的需要](../Page/口供.md "wikilink")）、[異體字](../Page/異體字.md "wikilink")、小部份[簡體字](../Page/簡體字.md "wikilink")、[平假名](../Page/平假名.md "wikilink")、[片假名及](../Page/片假名.md "wikilink")[俄語](../Page/俄語.md "wikilink")[西里爾字母](../Page/西里爾字母.md "wikilink")。

#### 澳門增補字符集

## 發展

由於各廠商及政府推出的Big5延伸，彼此互不兼容，造成[亂碼問題](../Page/亂碼.md "wikilink")。鑑於[Unicode能正確地處理七萬多個漢字](../Page/Unicode.md "wikilink")，近年的[作業系統和應用程式](../Page/作業系統.md "wikilink")（如[蘋果電腦](../Page/蘋果電腦.md "wikilink")[Mac
OS
X和以](../Page/Mac_OS_X.md "wikilink")[Cocoa](../Page/Cocoa.md "wikilink")
API撰寫之程式、[Microsoft](../Page/Microsoft.md "wikilink") [Windows
2000及之後版本](../Page/Windows_2000.md "wikilink")、[Microsoft
Office](../Page/Microsoft_Office.md "wikilink")
2000及之後版本、[Mozilla瀏覽器](../Page/Mozilla.md "wikilink")、[Internet
Explorer瀏覽器](../Page/Internet_Explorer.md "wikilink")、[Java語言等等](../Page/Java.md "wikilink")），已改用Unicode編碼。可惜現時仍有一些舊的軟件（如[Visual
Basic](../Page/Visual_Basic.md "wikilink")
6、部分[Telnet或](../Page/Telnet.md "wikilink")[BBS軟件](../Page/BBS.md "wikilink")），未能支援Unicode編碼，故相信Big5缺字的問題仍會困擾用戶一段時間，直至所有程式都能改用Unicode為止。

## 輸入方法

  - [VimIM在Vim環境中](../Page/VimIM.md "wikilink")，可以直接鍵入十進制或十六進制[Big5](http://maxiangjiang.googlepages.com/vimim.html#big5)碼。既不需要啟動輸入法，也不需要碼表。

## 参考文献

<div class="references-small">

<references />

  -
  - 《中文字碼 萬碼奔騰 一碼當先》作者：黃大一，永麒科技出版

</div>

## 參看

  - [CCCII](../Page/CCCII.md "wikilink")
  - [GB 18030](../Page/GB_18030.md "wikilink")《信息交换用汉字编码字符集基本集的扩充》
  - [Unicode](../Page/Unicode.md "wikilink")
  - [中日韓統一表意文字](../Page/中日韓統一表意文字.md "wikilink")
  - [中文亂碼](../Page/中文亂碼.md "wikilink")
  - [香港增補字符集](../Page/香港增補字符集.md "wikilink")（HKSCS）
  - [國家標準中文交換碼](../Page/國家標準中文交換碼.md "wikilink")（CNS 11643）

## 外部連結

  - [Big5字符集簡介](https://web.archive.org/web/20041212084615/http://www.haiyan.com/steelk/navigator/ref/big5/b5index.htm)
  - [Big5編碼系統](http://libai.math.ncu.edu.tw/~shann/Chinese/big5.html)
  - [BIG5编码查询](https://web.archive.org/web/20090708003902/http://xxcx.org/hzbm/)
  - [CCCII中文資訊交換碼](https://web.archive.org/web/20041210171124/http://www.cccii.org.tw/)
  - [CNS 11643國家標準中文交換碼](http://www.cns11643.gov.tw/)
  - [ICU Converter Explorer
    (Big5)](http://demo.icu-project.org/icu-bin/convexp?conv=Big5)
  - [Mozilla系列與Big5中文字碼](http://moztw.org/docs/big5/)
  - [RFC1922附錄中含Big5和CNS11643相轉資訊](http://www.ietf.org/rfc/rfc1922.txt)
  - [中文數位化技術推廣基金會](http://www.cmex.org.tw/)
  - [微软CP950编码表](http://www.microsoft.com/globaldev/reference/dbcs/950.htm)
  - [香港特區政府：香港增補字符集](https://web.archive.org/web/20041209055050/http://www.info.gov.hk/digital21/chi/hkscs/introduction.html)
  - [補完計畫跟Big5 2003相異處（扣掉保留區、造字區）](https://web.archive.org/web/20051216103930/http://cpatch.org/witchfive/1/uao-big52k3-diff.htm)

\-{}-

[DWM](../Category/雙位元組字元集.md "wikilink")
[DWM](../Category/中文信息处理.md "wikilink")
[Category:字符集](../Category/字符集.md "wikilink")
[Category:台灣發明](../Category/台灣發明.md "wikilink")
[Category:台灣資訊科技](../Category/台灣資訊科技.md "wikilink")

1.  普遍認為大五碼包含13,053字，但在計算0xA259-0xA261的九個[度量衡單位用字](../Page/計量用漢字.md "wikilink")（兙兛兞兝兡兣嗧瓩糎），再減去重收了兩次的「兀」（0xC94A）和「嗀」（0xDDFC）後，應為13,060字。
2.  來源參考：[資策會大事紀要，1983年](http://www.iii.org.tw/about/history_1983.html)
3.  來源參考：[資策會大事紀要](http://www.iii.org.tw/about/1_6.asp)
4.  來源參考：[資策會大事紀要，1984年](http://www.iii.org.tw/about/history_1984.html)
5.  來源參考：[行政院主計處電子處理資料中心中文全字庫——中文碼介紹-BIG-5碼介紹](http://www.cns11643.gov.tw/AIDB/encodings.do#encode4)
6.  來源參考：[數位雜談 -
    中文的電腦或電腦的中文？，諶家雄](http://www.libertytimes.com.tw/2001/new/mar/11/today-i1.htm)

7.  來源參考：[CMEX財團法人中文數位化技術推廣基金會 -
    認識中文碼：九、Big5和Big5E](http://www.cmex.org.tw/page.do?path=service_1-2.html)
8.  來源參考：[與文字共舞___中文數位化發展簡介](http://www.stat.gov.tw/ct.asp?xItem=26971&ctNode=99)
9.  來源參考：倚天中文系統、國喬中文系統
10. 來源參考：[朱邦復專欄——中文微電腦之歷史見証](http://www.cbflabs.com/book/essay/agdi0.htm)
11. 參考：[perlop－Perl operators and
    precedence](http://linux.tnc.edu.tw/CPAN/perl/pod/perlop.html)
    以及[淺談許蓋功-中文衝碼問題](http://www.twvbb.com/vbb/archive/t-163.html)

12. 參考：[perlop－Perl operators and
    precedence](http://linux.tnc.edu.tw/CPAN/perl/pod/perlop.html)
    以及[淺談許蓋功-中文衝碼問題](http://www.twvbb.com/vbb/archive/t-163.html)