**大卫·马蒂森·沃克**（，），前[美國海軍](../Page/美國海軍.md "wikilink")[上校及](../Page/上校.md "wikilink")[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[STS-51-A](../Page/STS-51-A.md "wikilink")、[STS-30](../Page/STS-30.md "wikilink")、[STS-53以及](../Page/STS-53.md "wikilink")[STS-69任务](../Page/STS-69.md "wikilink")。

## 外部链接

  - [美国国家航空航天局网站的沃克介绍](http://www.jsc.nasa.gov/Bios/htmlbios/walker.html)

[W](../Category/美國海軍上校.md "wikilink")
[W](../Category/鹰级童军.md "wikilink")
[W](../Category/第八组宇航员.md "wikilink")
[Category:美國海軍學院校友](../Category/美國海軍學院校友.md "wikilink")