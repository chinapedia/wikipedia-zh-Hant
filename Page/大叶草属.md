**大叶草属**（[学名](../Page/学名.md "wikilink")：）是**大叶草科**（，又名**洋二仙草科**、**根乃拉草科**或**古奴科**）下唯一[属](../Page/属.md "wikilink")，属[真双子叶植物](../Page/真双子叶植物.md "wikilink")[大叶草目](../Page/大叶草目.md "wikilink")，含约50[种](../Page/种.md "wikilink")，都是原生于南半球的[植物](../Page/植物.md "wikilink")，1981年的[克朗奎斯特分类法将其和](../Page/克朗奎斯特分类法.md "wikilink")[小二仙草科一起放到](../Page/小二仙草科.md "wikilink")[小二仙草目中](../Page/小二仙草目.md "wikilink")，但1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其和](../Page/APG_分类法.md "wikilink")[折扇叶科一起合并成可以任选或分开的两个](../Page/折扇叶科.md "wikilink")[科](../Page/科.md "wikilink")，没有放入任何一[目中](../Page/目.md "wikilink")，2003年经过修订的[APG
II
分类法直接设立了一个](../Page/APG_II_分类法.md "wikilink")[大叶草目](../Page/大叶草目.md "wikilink")。

洋二仙草喜歡溫暖濕潤的環境，主要分佈在拉美、東非和東南亞的熱帶地區。在智利南部和阿根廷的部分地區，洋二仙草也被用來食用。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[大叶草科](http://delta-intkey.com/angio/www/gunnerac.htm)
  - [NCBI分类中的大叶草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=24955&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)
  - [links at
    CSDL分类中的大叶草科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Gunneraceae)

[\*](../Category/大叶草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")