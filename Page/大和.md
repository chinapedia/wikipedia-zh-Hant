**大和**可以指：

## 日本

  - [大和時代](../Page/大和時代.md "wikilink")（250年－538年），[日本历史上的一个时代](../Page/日本历史.md "wikilink")
  - [大和王权](../Page/大和王权.md "wikilink")，4世纪至7世纪，以大和地区为中心，君临日本各地豪族联合之上的王权
  - [大和族](../Page/大和族.md "wikilink")，日本的主体民族
  - [大和國](../Page/大和國.md "wikilink")，日本的古分国之一
  - [大和市](../Page/大和市.md "wikilink")，位於日本[神奈川县中心的都市](../Page/神奈川县.md "wikilink")
  - [大和町](../Page/大和町.md "wikilink")，位於日本[宮城縣的町](../Page/宮城縣.md "wikilink")
  - [大和村](../Page/大和村.md "wikilink")，位於日本[鹿兒島縣的村](../Page/鹿兒島縣.md "wikilink")
  - [大和
    (初代)](../Page/大和_\(初代\).md "wikilink")，日本葛城型的2號艦，1887年竣工，1936年除籍，1950年解體
  - [大和級戰列艦](../Page/大和級戰列艦.md "wikilink")，日本帝国海军战列舰
  - [大和号战列舰](../Page/大和号战舰.md "wikilink")，日本大和級戰列艦的1號艦，第二次世界大戰期間世界最大的戰艦
  - [宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")，日本動畫

## 朝鮮

  - [大和面](../Page/大和面.md "wikilink")，位於大韓民國[江原道](../Page/江原道.md "wikilink")[平昌郡的一個](../Page/平昌郡.md "wikilink")[面](../Page/面_\(朝鮮\).md "wikilink")

## 人物

  - [大和悠河](../Page/大和悠河.md "wikilink")，演員。
  - [近藤勇的別名](../Page/近藤勇.md "wikilink")。
  - [大和
    (火影忍者)](../Page/大和_\(火影忍者\).md "wikilink")，《[火影忍者](../Page/火影忍者.md "wikilink")》的角色。
  - [大和
    (艦隊Collection)](../Page/大和_\(艦隊Collection\).md "wikilink")，《[艦隊Collection](../Page/艦隊Collection.md "wikilink")》的角色。

## 年号

  - [大和 (唐朝)](../Page/大和_\(唐朝\).md "wikilink")（827年─835年），唐文宗李昂使用的年号，
  - [大和 (南吴)](../Page/大和_\(南吴\).md "wikilink")（929年─935年），十国南吴睿帝杨溥使用的年号
  - [大和
    (黎邦基)](../Page/大和_\(黎邦基\).md "wikilink")（1443年—1453年），越南後黎朝仁宗黎邦基使用的年號

## 其他

  - [大和乡](../Page/大和乡.md "wikilink")，中国四川省[巴中市](../Page/巴中市.md "wikilink")[巴州区下辖的一个乡](../Page/巴州区.md "wikilink")
  - [大和冰川](../Page/大和冰川.md "wikilink")，南极洲冰川
  - [大和島](../Page/大和島.md "wikilink")，[朝鮮半島西岸](../Page/朝鮮半島.md "wikilink")[黃海的一個海島](../Page/黃海.md "wikilink")，[韓戰時曾發生](../Page/韓戰.md "wikilink")[大和島戰役](../Page/大和島戰役.md "wikilink")
  - 大和炮，[星海爭霸2裡的人類戰巡艦的主動技能](../Page/星海爭霸2.md "wikilink")
  - 超大和號戰艦，[人中之龍6裡](../Page/人中之龍6.md "wikilink")，廣島黑道組織陽明聯合會一直守護的「尾道的秘密」，原本在二戰時期建造，但直到戰爭結束都尚未完成。(詳細參照[陽明聯合會與](../Page/#巖見兵三.md "wikilink")[政治人物](../Page/#大道寺稔.md "wikilink"))
  - 大和（），游戏《[传颂之物 虚伪的假面](../Page/传颂之物_虚伪的假面.md "wikilink")》、《[传颂之物
    二人的白皇](../Page/传颂之物_二人的白皇.md "wikilink")》及其衍生作品中出现的一个国家。

## 参见

  - [大和町 (消歧義)](../Page/大和町_\(消歧義\).md "wikilink")
  - [大和村 (消歧義)](../Page/大和村_\(消歧義\).md "wikilink")