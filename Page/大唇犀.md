**大唇犀**（[學名](../Page/學名.md "wikilink")：*Chilotherium*）是一[屬已](../Page/屬.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[犀牛](../Page/犀牛.md "wikilink")，生存於[中新世至](../Page/中新世.md "wikilink")[上新世](../Page/上新世.md "wikilink")（13.7—3.4
百萬年前）的[歐亞大陸上](../Page/歐亞大陸.md "wikilink")，包括[中國](../Page/中國.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")。

## 描述

大唇犀為體型粗壯的動物，依物種的不同肩高範圍為 ，而體重範圍在  之間\[1\]。

雄性與雌性均不具有角，下唇比上唇大，[下頜骨粗狀成鏟子狀](../Page/下頜骨.md "wikilink")。上顎沒有[門齒](../Page/門齒.md "wikilink")，下顎的門齒闊大，並且向上彎，齒式為
。頭部比現今的犀大稍大，[頭顱骨沒有角](../Page/頭顱骨.md "wikilink")。大唇犀矮小，四肢短小，每肢有三趾\[2\]。

大唇犀是[牧食性的](../Page/放牧.md "wikilink")[動物](../Page/動物.md "wikilink")，生活於[沼澤地帶](../Page/沼澤.md "wikilink")。

<File:Chilotherium> skull 1.JPG|

<center>

大唇犀頭顱骨的側面

</center>

<File:Chilotherium> skull 2.JPG|

<center>

大唇犀頭顱骨的正面

</center>

## 參考文獻

[Category:犀科](../Category/犀科.md "wikilink")
[Category:已滅絕動物](../Category/已滅絕動物.md "wikilink")
[Category:中新世哺乳類](../Category/中新世哺乳類.md "wikilink")
[Category:上新世哺乳類](../Category/上新世哺乳類.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")

1.
2.