[Tai_Wai_Hui_aerial_view_201802.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wai_Hui_aerial_view_201802.jpg "fig:Tai_Wai_Hui_aerial_view_201802.jpg")
[HK_TaiWaiVillage_Archway.JPG](https://zh.wikipedia.org/wiki/File:HK_TaiWaiVillage_Archway.JPG "fig:HK_TaiWaiVillage_Archway.JPG")
[TaiWaiVillage2_20071201.JPG](https://zh.wikipedia.org/wiki/File:TaiWaiVillage2_20071201.JPG "fig:TaiWaiVillage2_20071201.JPG")

**大圍村**原名**積存圍**，於[明朝](../Page/明朝.md "wikilink")[萬曆初年建圍](../Page/萬曆.md "wikilink")，是[香港](../Page/香港.md "wikilink")[沙田歷史最悠久](../Page/沙田區.md "wikilink")（1574年建村）以及規模最大的[圍村](../Page/圍村.md "wikilink")\[1\]。現時位於[大圍站附近](../Page/大圍站.md "wikilink")[城門河邊](../Page/城門河.md "wikilink")，在積福街及積富街交界處。大圍村是一條雜姓村，膾炙人口的[九約竹枝詞有](../Page/新界九約竹枝詞.md "wikilink")「大圍風景實如何，村裡人居雜姓多」之語，住著十六姓：包括[韋](../Page/韋姓.md "wikilink")、[陳](../Page/陳姓.md "wikilink")、[吳](../Page/吳姓.md "wikilink")、[楊](../Page/楊姓.md "wikilink")、[黃](../Page/黃姓.md "wikilink")、[李](../Page/李姓.md "wikilink")、[許](../Page/許姓.md "wikilink")、[鄭](../Page/鄭姓.md "wikilink")、[唐](../Page/唐姓.md "wikilink")、[袁](../Page/袁姓.md "wikilink")、[游](../Page/游姓.md "wikilink")、[林](../Page/林姓.md "wikilink")、[駱](../Page/駱姓.md "wikilink")、[譚](../Page/譚姓.md "wikilink")、[莫及](../Page/莫姓.md "wikilink")[蔡](../Page/蔡姓.md "wikilink")，以韋姓最多，成為村中大族，亦只有韋氏族人在圍村內設有[祠堂](../Page/祠堂.md "wikilink")。大圍村亦是[沙田九約的鄉村之一](../Page/沙田九約.md "wikilink")。在[沙田新市鎮開始發展](../Page/沙田新市鎮.md "wikilink")，以及[九廣鐵路](../Page/九廣鐵路.md "wikilink")[大圍站落成後](../Page/大圍站.md "wikilink")，大圍村的「大圍」之名逐漸演變成包含[城門河人工河道以南和](../Page/城門河.md "wikilink")[獅子山隧道公路以西的整個沙田區南部的地區名稱](../Page/獅子山隧道公路.md "wikilink")。

## 起源

明朝初年許多人逃避戰亂從[東莞遷移到](../Page/東莞.md "wikilink")[大圍附近](../Page/大圍.md "wikilink")，以耕作為生，由於地方治安不靖，當中廿九戶村民發起建圍以保安全，大圍村建成時，四面有池塘，可種蓮養魚及防盜，圍村四角建有圍斗，有小窗可以觀察圍外情況，更有兩門大砲保護圍村。

### 侯王宮

大圍村侯王宮除供奉侯王爺外，還供奉[車大元帥](../Page/車公.md "wikilink")、[德福土地](../Page/土地公.md "wikilink")、廿九名開村的始祖和三名對圍村有貢獻的英雄神位。相傳侯王爺是[宋朝國舅爺](../Page/宋朝.md "wikilink")[楊亮節](../Page/楊亮節.md "wikilink")，[南宋末年](../Page/南宋.md "wikilink")[文天祥及](../Page/文天祥.md "wikilink")[陸秀夫立](../Page/陸秀夫.md "wikilink")[益王昰](../Page/趙昰.md "wikilink")，崩於[碙州](../Page/碙州.md "wikilink")，秀夫復立[廣王昺於](../Page/趙昺.md "wikilink")[崖山](../Page/崖山.md "wikilink")，楊亮節率眾南下迎帝，遭[元兵追殺](../Page/元朝.md "wikilink")，秀夫負帝蹈海死，楊亮節操勞過度，病歿[官富場](../Page/九龍城.md "wikilink")，但侯王爺真正的身份仍有存疑。

侯王宮早期儼然大圍村的雜性祠堂和村公所，舉凡村中大小爭執都交侯王宮處理，直到1976年大圍村村公所落成才接手處理村務。侯王宮是[沙田區香火最鼎盛的廟宇之一](../Page/沙田區.md "wikilink")。

### 韋氏宗祠

大圍村、[上徑口村及](../Page/上徑口村.md "wikilink")[田心村韋姓村民據傳為](../Page/田心村.md "wikilink")[漢朝](../Page/漢朝.md "wikilink")[淮陰侯](../Page/淮陰.md "wikilink")[韓信的後人](../Page/韓信.md "wikilink")，韓信被[呂后設計殺害](../Page/呂后.md "wikilink")，[蕭何命](../Page/蕭何.md "wikilink")[蒯徹匿藏韓信之子於](../Page/蒯徹.md "wikilink")[南粵](../Page/南粵.md "wikilink")，隱姓埋名，取韓字之半，改姓韋，是為今日[中山](../Page/中山縣.md "wikilink")[翠薇村](../Page/翠薇村.md "wikilink")、沙田三村和[西貢](../Page/西貢區.md "wikilink")[沙角尾村韋氏家族的源頭](../Page/沙角尾村.md "wikilink")。大圍韋氏宗祠門頂橫匾題為「京兆堂」，兩旁對聯「淮陰世澤」和「京兆家聲」，顯然與韓信大有關連，據說韋氏宗祠內供奉的[神主牌背後皆書](../Page/神主牌.md "wikilink")「韓」字，以示飲水思源，不忘祖先之意\[2\]。

大圍村韋氏始祖建元祖先由中山翠薇村遷移至[深圳居住](../Page/深圳.md "wikilink")，十四歲時到[元朗當牧童](../Page/元朗區.md "wikilink")，一日返回深圳老家發覺父母及全部村民為鄰村袁姓村民所屠殺，建元祖逃亡回元朗，娶妻後偕家眷遷居沙田建基立業。

## 歷史建築

大圍村積存圍的[圍門](../Page/圍門.md "wikilink")，於2010年8月31日，被確定為[二級歷史建築物](../Page/香港歷史建築.md "wikilink")。

## 當區議員

[董健莉](../Page/董健莉.md "wikilink")

## 實景

<File:TaiWaiVillageSchool>
200712.JPG|[大圍公立學校](../Page/大圍公立學校.md "wikilink")（已棄置）
<File:TaiWaiVillageAdminCentre> 20071201.jpg|大圍村村公所
<File:TaiWaiVillage1> 20071201.jpg|大圍村古舊村屋
[File:HK_TaiWaiVillage_OldBldg2.JPG|大圍村古舊村屋](File:HK_TaiWaiVillage_OldBldg2.JPG%7C大圍村古舊村屋)
<File:TaiWaiVillage> MainEntrance Front 2007.JPG|積存圍門（前）
[File:TaiWaiVillage_KingHsuTemple.jpg|積存圍侯王宮](File:TaiWaiVillage_KingHsuTemple.jpg%7C積存圍侯王宮)
<File:TaiWaiVillage> WaiTemple 2007.JPG|韋氏宗祠大閘
[File:TaiWaiVillage_WaiTemple.jpg|韋氏宗祠](File:TaiWaiVillage_WaiTemple.jpg%7C韋氏宗祠)

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{東鐵綫色彩}}">█</font><a href="../Page/東鐵綫.md" title="wikilink">東鐵綫</a>、<font color="{{馬鞍山綫色彩}}">█</font><a href="../Page/馬鞍山綫.md" title="wikilink">馬鞍山綫</a>：<a href="../Page/大圍站.md" title="wikilink">大圍站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參見

  - [香港圍村](../Page/香港圍村.md "wikilink")
  - 沙田古今風貌：歷史掌故 大圍 （第14-19頁）《1997年，沙田區議會編印》

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [九廣鐵路沿線旅遊景點：大圍](https://web.archive.org/web/20070308193207/http://www.kcrc.com/text/tch/services/attractions_along_KCR/east_rail/taiwai/chik_chuen_wai.asp)
  - [始祖亮節公世系溯源辨正](https://archive.is/20060508124736/http://163.13.215.224/me/lineage/book4-1.htm)
  - [《中國帝王皇后親王公主世系錄》宋　第四篇　皇后篇](https://web.archive.org/web/20120626014235/http://nrch.cca.gov.tw/ccahome/website/site4/BY_Collect/b9000e0/e004/cca220003-li-wpkbbyb9000e0040026-0096-u.xml)

[Category:沙田區鄉村](../Category/沙田區鄉村.md "wikilink")
[Category:大圍](../Category/大圍.md "wikilink")
[Category:香港圍頭鄉村](../Category/香港圍頭鄉村.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.  [沙田區議會：地區特色及盛事](http://www.districtcouncils.gov.hk/st/chinese/welcome.htm)

2.  [街知巷聞﹕圍．住記憶](http://news.sina.com.hk/news/20120819/-23-2748475/l/951118.html)
    《明報》2012年8月19日