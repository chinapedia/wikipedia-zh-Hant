**大圣伯纳德山口**
（[德語](../Page/德語.md "wikilink")：****；[法語](../Page/法語.md "wikilink")：****；[意大利語](../Page/意大利語.md "wikilink")：****）是迄今最古老的跨越西[阿尔卑斯山的山口](../Page/阿尔卑斯山.md "wikilink")，那里目前仍然能够看到上溯至[青铜时代的人类活动遗迹](../Page/青铜时代.md "wikilink")、[罗马时代的羊肠小道和](../Page/罗马.md "wikilink")[拿破仑军队](../Page/拿破仑.md "wikilink")1800年进入[意大利时使用的道路](../Page/意大利.md "wikilink")。山口处一所始建于1049年的旅舍目前仍在接待游客，旅舍以马松的[圣伯纳德](../Page/圣伯纳德.md "wikilink")（Saint
Bernard of
Menthon）命名，包括两幢分别于1560年和1898年建造的房屋（如右图）。1923年，罗马[教宗](../Page/教宗.md "wikilink")[庇护十一世确认伯纳德为](../Page/庇护十一世.md "wikilink")“守护阿尔卑斯山的圣徒”（Patron
Saint of the Alps）。

## 地理环境

圣伯纳德大山口海拔2,469米（8,101英尺），东北西南延伸并横跨[阿尔卑斯山](../Page/阿尔卑斯山.md "wikilink")。山口以北是[瑞士](../Page/瑞士.md "wikilink")，向南分别经过[河](../Page/河.md "wikilink")（Drance
River）河谷和广袤的[Dranse
d'Entremont](../Page/Dranse_d'Entremont.md "wikilink")。目前通过山口的主要途径是位于海拔1,915米（6,283英尺）一线的[圣伯纳德大隧道](../Page/圣伯纳德大隧道.md "wikilink")；从[Simplon
Tunnel向东](../Page/Simplon_Tunnel.md "wikilink")，部分山路已由铁路取代。另外一条蜿蜒于山体之上的小路是瑞士与意大利的边境线，仅在每年7月到9月间可以通行。在山口的南侧是[圣伯纳德大山谷和最终注入](../Page/圣伯纳德大山谷.md "wikilink")[奥斯塔河](../Page/奥斯塔河.md "wikilink")（Aosta）的[阿坦阿瓦兹河](../Page/阿坦阿瓦兹河.md "wikilink")（Artanavaz
River）。

圣伯纳德大山口之下是一小湖，由几幢房屋组成的旅舍分布在山口南北两侧，原有的小路目前仍然保留；阿尔比斯山的第二高峰[Mont
Mort和](../Page/Mont_Mort.md "wikilink")[Pic de
Dorna居于山口之侧](../Page/Pic_de_Dorna.md "wikilink")。由于海拔高度关系，在每年7月之前此处一直大雪封山，而建立旅舍的真正目的正是为了帮助人们克服翻越山口的困难。

## 圣伯纳犬

参见：[圣伯纳犬](../Page/圣伯纳犬.md "wikilink")

根据传说：当地的圣伯纳德犬最初从[亚洲引进](../Page/亚洲.md "wikilink")，早在罗马时代就作为看门狗为人们服务了。它们待人友好，几百年来挽救了数以百计试图徒步穿越山口的冒险者的生命。在修道士的精心喂养下，圣伯纳德犬拥有硕大的体形和灵敏的嗅觉，这能有助他们在深雪堆中穿行并且快速找到需要帮助的迷路者。圣伯纳德犬在执行营救任务时会携带装有[白兰地酒的小木桶来帮助迷路者御寒](../Page/白兰地酒.md "wikilink")。而事实上这仅仅是依靠心理作用“御寒”而并无真正的药理作用，因为当体温降低时饮酒反倒会加快身体热量的散失。

## 圣伯纳德修道院

参见:[圣伯纳德修道院](../Page/圣伯纳德修道院.md "wikilink")

著名的建造于11世纪的圣伯纳德修道院位于距离山口1英里远的地方。每年夏季会有众多游客乘车通过山口并且顺路拜访此处，而冬天除了一些[滑雪爱好者以外访问者寥寥无几](../Page/滑雪.md "wikilink")。

## 外部链接

  - [由climbbybike.com提供的相关简介（英语）](http://www.climbbybike.com/climb.asp?Col=Col%20du%20Grand%20Saint%20Bernard%20&qryMountainID=1847)
  - [圣伯纳德大山口的历史（英语）](https://archive.is/20121208215024/http://switzerland.isyours.com/e/guide/valais/grandstbernardhistory.html)
  - [圣伯纳德大山口鸟瞰图（英语）](http://www.swisscastles.ch/aviation/Montagne/stbernard.html)
  - [由艾德里安·佛莱彻（Adrian
    Fletcher）拍摄的圣伯纳德大山口照片（英语）](https://web.archive.org/web/20090427044138/http://paradoxplace.com/Perspectives/Venice%20%26%20N%20Italy/Alps/Gran_San_Bernardo.htm)

## 参考书目

  - L.G.亚历山大：《新概念英语》第三册第八课《A Famous Monastery 著名的修道院》

[Category:瑞士山口](../Category/瑞士山口.md "wikilink")