[HK_CWB_Tai_Hang_Tung_Lo_Wan_Road_church.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Tai_Hang_Tung_Lo_Wan_Road_church.JPG "fig:HK_CWB_Tai_Hang_Tung_Lo_Wan_Road_church.JPG")\]\]
[HK_CWB_Tai_Hang_Moreton_north.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Tai_Hang_Moreton_north.JPG "fig:HK_CWB_Tai_Hang_Moreton_north.JPG")天橋往[銅鑼灣避風塘海旁方向](../Page/銅鑼灣避風塘.md "wikilink")\]\]
[HK_CWB_Tai_Hang_Road_east.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Tai_Hang_Road_east.JPG "fig:HK_CWB_Tai_Hang_Road_east.JPG")香港分區總部附近\]\]

**大坑道**（**Tai Hang
Road**），是[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[銅鑼灣一帶半山的一條主要道路](../Page/銅鑼灣.md "wikilink")，依山而建，連接山上多個地區，包括[大坑](../Page/大坑.md "wikilink")、[渣甸山](../Page/渣甸山.md "wikilink")、[跑馬地及](../Page/跑馬地.md "wikilink")[黄泥涌峽](../Page/黄泥涌峽.md "wikilink")。

大坑道起點在[大坑北面](../Page/大坑.md "wikilink")，連接[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")，並建有[天橋連接](../Page/天橋.md "wikilink")[告士打道直通至海旁](../Page/告士打道.md "wikilink")、[香港島其他地區及](../Page/香港島.md "wikilink")[海底隧道](../Page/海底隧道.md "wikilink")，而隔鄰道路亦可連接東區海底隧道和北角。道路先向東南方向上山，經過[大坑道一號](../Page/大坑道一號.md "wikilink")、[永威閣](../Page/永威閣.md "wikilink")、[光明臺](../Page/光明臺.md "wikilink")、[帝-{后}-臺](../Page/帝后臺.md "wikilink")、[上林](../Page/上林.md "wikilink")、[虎豹別墅](../Page/虎豹別墅.md "wikilink")、[春暉8號](../Page/春暉8號.md "wikilink")、[大寶閣](../Page/大寶閣.md "wikilink")、[渣甸山](../Page/渣甸山.md "wikilink")、[掃桿埔](../Page/掃桿埔.md "wikilink")[衛斯理村](../Page/衛斯理村.md "wikilink")，連接大坑[大坑徑](../Page/大坑徑.md "wikilink")、[跑馬地](../Page/跑馬地.md "wikilink")[藍塘道及](../Page/藍塘道.md "wikilink")[黃泥涌峽道](../Page/黃泥涌峽道.md "wikilink")﹔全長3.7公里，沿着大坑道建有不少高級住宅。

## 大坑道天橋

大坑道天橋（俗稱摩頓台天橋）是連接大坑道北端及告士打道的行車天橋。天橋起始於[香港聖約翰救傷隊總部一帶](../Page/香港聖約翰救傷隊.md "wikilink")，南段位處[摩頓台的上方](../Page/摩頓台.md "wikilink")，中間跨過[高士威道](../Page/高士威道.md "wikilink")，並在[皇室大廈一帶接上告士打道](../Page/皇室大廈.md "wikilink")。天橋雙向行車，全長約0.5公里，另有支路通往[銅鑼灣道近](../Page/銅鑼灣道.md "wikilink")[摩頓台巴士總站](../Page/摩頓台巴士總站.md "wikilink")。

大坑道天橋於1983年11月15日通車，並由行政局議員[張奧偉揭幕](../Page/張奧偉.md "wikilink")\[1\]。

## 沿路著名地點及高級住宅

### 私人屋苑

  - [大坑道一號](../Page/大坑道一號.md "wikilink") （大坑道1號）
  - [Y.I](../Page/Y.I.md "wikilink") （大坑道10號）
  - [光明臺](../Page/光明臺.md "wikilink")（大坑道5-7號）
  - [帝-{后}-臺](../Page/帝后臺.md "wikilink")（大坑道26號）
  - [華峰樓](../Page/華峰樓.md "wikilink")（大坑道27號）
  - [麗星樓](../Page/麗星樓.md "wikilink")（現重建為[豪宅](../Page/豪宅.md "wikilink")「上林」；大坑道11號）
  - [龍園](../Page/龍園.md "wikilink")（大坑道春暉台1號）
  - [渣甸山名門](../Page/名門_\(香港\).md "wikilink")
  - [大寶閣](../Page/大寶閣.md "wikilink") (大坑道70號)
  - [嘉崙臺](../Page/嘉崙臺.md "wikilink") (大坑道152號)
  - [大坑道339號](../Page/大坑道339號.md "wikilink") (大坑道339號)
  - [嘉園](../Page/嘉園.md "wikilink") (大坑道345號)

### 其他

  - [聖公會聖馬利亞堂](../Page/聖公會聖馬利亞堂.md "wikilink")
  - [瑪利曼小學](../Page/瑪利曼小學.md "wikilink")
  - [香港真光中學](../Page/香港真光中學.md "wikilink")
  - [中華基督教會公理高中書院](../Page/中華基督教會公理高中書院.md "wikilink")
  - [虎豹別墅 (香港)](../Page/虎豹別墅_\(香港\).md "wikilink")
  - [渣甸山居民協會](../Page/渣甸山.md "wikilink")
  - [摩頓台天橋](../Page/摩頓台.md "wikilink")
  - [衛斯理村](../Page/衛斯理村.md "wikilink")
  - [聖約翰救傷隊](../Page/香港聖約翰救護機構.md "wikilink")[港島總區總部暨](../Page/港島.md "wikilink")[救護車站](../Page/救護車.md "wikilink")（大坑道2號）
  - [中央圖書館](../Page/中央圖書館.md "wikilink")

[Happy_Valley_1.jpg](https://zh.wikipedia.org/wiki/File:Happy_Valley_1.jpg "fig:Happy_Valley_1.jpg")[布力徑俯瞰](../Page/布力徑.md "wikilink")[大坑](../Page/大坑.md "wikilink")、[跑馬地至](../Page/跑馬地.md "wikilink")[渣甸山一帶](../Page/渣甸山.md "wikilink")\]\]

## 交滙道路（由北至南）

  - [銅鑼灣道](../Page/銅鑼灣道.md "wikilink")
  - [嘉寧徑](../Page/嘉寧徑.md "wikilink")
  - [利群道](../Page/利群道.md "wikilink")
  - [勵德邨道](../Page/勵德邨道.md "wikilink")
  - [春暉台](../Page/春暉台.md "wikilink")
  - [福群道](../Page/福群道.md "wikilink")
  - [宏豐台](../Page/宏豐台.md "wikilink")
  - [大坑徑](../Page/大坑徑.md "wikilink")
  - [白建時道](../Page/白建時道.md "wikilink")
  - [樂活道](../Page/樂活道.md "wikilink")
  - [畢拉山道](../Page/畢拉山道.md "wikilink")
  - [藍塘道](../Page/藍塘道.md "wikilink")
  - [司徒拔道](../Page/司徒拔道.md "wikilink")
  - [黃泥涌峽道](../Page/黃泥涌峽道.md "wikilink")

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

</div>

</div>

## 參考資料

<references/>

## 外部連結

  - [香港地方: **大坑道**
    於1931年2月13日命名](http://www.hk-place.com/view.php?id=342)
  - [光明臺](https://web.archive.org/web/20080709050920/http://www.illuminationterrace.com.hk/)

[Category:銅鑼灣街道](../Category/銅鑼灣街道.md "wikilink")
[Category:黃泥涌峽街道](../Category/黃泥涌峽街道.md "wikilink")
[Category:渣甸山街道](../Category/渣甸山街道.md "wikilink")

1.  大坑道天橋今通車 銅鑼灣樽頸可消除，《華僑日報》，1983年11月15日