**帕那空思阿瑜陀耶**（，），又譯作**阿育他亞**、**艾尤塔雅**，是[泰国](../Page/泰国.md "wikilink")[大城府的首府](../Page/大城府.md "wikilink")，坐落在[昭披耶河谷](../Page/昭披耶河.md "wikilink")。泰國華人稱其為**大城**（[潮州话](../Page/潮州话.md "wikilink")：），此名字更為華人世界所知。因《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》记载中的不落之城[阿约提亚而得名](../Page/阿约提亚.md "wikilink")。

，2014年人口52952\[1\]。

## 歷史

阿瑜陀耶由[乌通王始建于](../Page/乌通王.md "wikilink")1350年，為逃避[華富里的天花爆發](../Page/華富里.md "wikilink")，國王將此地確定為[阿瑜陀耶王国首都](../Page/阿瑜陀耶王国.md "wikilink")。阿瑜陀耶的特徵普朗（Prang，舍利塔）和巨大的寺院，充分表現藝術和建築的特色，展現城市過去的輝煌。據估計，1600年阿瑜陀耶有人口約30萬，到了1700年人口也許達到一百萬，使其成為當時世界最大的城市之一。1767年，该城被[缅甸攻占后焚毁](../Page/缅甸.md "wikilink")，城市備受大屠殺、強姦、奴役和破壞，導致王國的崩潰。[郑昭复国后将都城南迁的](../Page/郑昭.md "wikilink")[吞武里](../Page/吞武里.md "wikilink")，建立[吞武里王國](../Page/吞武里王國.md "wikilink")，在老城东幾公里重建新城。老城废墟现为[世界文化遗产](../Page/世界文化遗产.md "wikilink")[阿瑜陀耶历史公园所在地](../Page/阿瑜陀耶历史公园.md "wikilink")。

## 外部链接

  - [Photos and Videos of
    Ayutthaya](http://www.ianandwendy.com/OtherTrips/ThailandMalaysia/Thailand/index.htm)

[A](../Category/泰国城市.md "wikilink")
[Category:大城府](../Category/大城府.md "wikilink")

1.  <https://www.google.com/search?newwindow=1&q=phra+nakhon+si+ayutthaya+population>