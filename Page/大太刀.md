[Samurai_wearing_a_nodachi_(field_sword).png](https://zh.wikipedia.org/wiki/File:Samurai_wearing_a_nodachi_\(field_sword\).png "fig:Samurai_wearing_a_nodachi_(field_sword).png")
**大太刀**（[日文](../Page/日文.md "wikilink")：*おおたち*）為[太刀的一種型式](../Page/太刀.md "wikilink")，一般稱呼上也可稱為「**野太刀**」。

## 簡介

一般都把刀身長達90公分以上的[太刀稱為大太刀](../Page/太刀.md "wikilink")，和當時[日本人的平均身高](../Page/日本人.md "wikilink")（150公分至165公分）對比下算是相當巨大的[武器](../Page/武器.md "wikilink")。大太刀通常都是背負在背後，不過若是180公分以上的人則會像[打刀一樣別在腰間](../Page/打刀.md "wikilink")。

通常文獻中的**大太刀**指的是刀身150公分以上，而90公分以上則稱為**野太刀**。但是普通人都把大型的[太刀叫做大太刀](../Page/太刀.md "wikilink")，而把野太刀當作別稱。

## 發展歷史

[日本進入](../Page/日本.md "wikilink")[鎌倉時代後](../Page/鎌倉時代.md "wikilink")[武家開始掌權](../Page/武家.md "wikilink")，此時武器也成為武人炫耀腕力與豪氣的工具，因此刀身特別加長的[太刀開始出現在戰場上](../Page/太刀.md "wikilink")。

大太刀的主要使用方式是騎在馬上然後快速衝向敵人，藉由座騎快速衝刺時的力道來斬殺敵方的步行士兵。由於大太刀太長太笨重，故大太刀只適合做為騎兵武器，不適合於單兵攜帶。

由於大太刀的長度與重量（2.5公斤到8公斤重）的關係，和[太刀相同形狀且柄和刀身長度比也相同的大太刀在使用上相當不便](../Page/太刀.md "wikilink")。為了克服這點，於是開發刀身的一部分纏上劍繩的中卷野太刀，後來發展為[長卷](../Page/長卷.md "wikilink")。

此後大太刀改成為了供奉於[神社等處而製作](../Page/神社.md "wikilink")，並因為長度的關係而分段打造後重新接合而成，實際上也不能當作[武器使用](../Page/武器.md "wikilink")（強度太弱的關係而無法揮動）。

而進入[戰國時代後適合步戰的](../Page/戰國_\(日本\).md "wikilink")[打刀開始流行](../Page/打刀.md "wikilink")，殘留下來的大太刀數量也不多。

曾與[宮本武藏決鬥過的戰國時代著名劍豪](../Page/宮本武藏.md "wikilink")[佐佐木小次郎為歷史上少數善用野太刀的武術家](../Page/佐佐木小次郎.md "wikilink")。

## 長度

根據[軍記物語](../Page/軍記物語.md "wikilink")《[太平記](../Page/太平記.md "wikilink")》記載，許多大太刀其刀刃超過150公分，甚至有刀刃超過280公分以上的大太刀。

而現存的大太刀中，包含神社的供奉品、收藏[德川幕府珍品的](../Page/德川幕府.md "wikilink")[德川美術館所藏](../Page/德川美術館.md "wikilink")（[柳生大太刀](../Page/柳生大太刀.md "wikilink")）、[渡邊美術館等](../Page/渡邊美術館.md "wikilink")[博物館收藏等等](../Page/博物館.md "wikilink")，其中被認定為[重要文化財的](../Page/重要文化財.md "wikilink")[新潟縣彌彥神社所藏的](../Page/新潟縣.md "wikilink")220公分大太刀為最長者。

## 倭刀

大太刀在明朝時傳入中國，被稱為[倭刀](../Page/倭刀.md "wikilink")、單刀或長刀。明朝末年，鄭成功軍中使用類似這種外型的刀，稱雲南[斬馬刀](../Page/斬馬刀.md "wikilink")。

因為名稱，造成在一般人的想像裡，大太刀是種能把騎士和馬一起砍成兩半的[武器](../Page/武器.md "wikilink")。但大太刀在戰場上並不是這樣使用的，它與中國斬馬刀相同，是用來掃擊馬腳，或是步兵腰部之用。

## 外部連結

  - [新潟縣彌彥神社 彌彥文化財大太刀](http://www.e-yahiko.com/bunkazai/10.htm)
    此為目前所留最長的大太刀，刀刃長220.4公分，刀柄長101.8公分，全長322.2公分。

[O](../Category/日本刀.md "wikilink") [O](../Category/太刀.md "wikilink")