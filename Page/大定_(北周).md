**大定**（581年正月—二月）是北周政权周靜帝[宇文衍的](../Page/宇文衍.md "wikilink")[年号](../Page/年号.md "wikilink")，歷時數月。

## 大事记

## 出生

## 逝世

## 纪年

| 大定                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 581年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[大定年號的政權](../Page/大定.md "wikilink")
  - 同期存在的其他政权年号
      - [天保](../Page/天保_\(萧岿\).md "wikilink")（562年二月—585年十二月）：[西梁政權梁明帝](../Page/西梁.md "wikilink")[萧岿的年号](../Page/萧岿.md "wikilink")
      - [太建](../Page/太建.md "wikilink")（569年正月—582年十二月）：[南朝陈政權陳宣帝](../Page/南朝陈.md "wikilink")[陈顼的年号](../Page/陈顼.md "wikilink")
      - [延昌](../Page/延昌_\(麴乾固\).md "wikilink")（561年—601年）：[高昌政权](../Page/高昌.md "wikilink")[麴乾固年号](../Page/麴乾固.md "wikilink")
      - [鴻濟](../Page/鴻濟.md "wikilink")（572年—584年）：[新羅](../Page/新羅.md "wikilink")[真智王之年號](../Page/新羅真智王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北周年号](../Category/北周年号.md "wikilink")
[Category:580年代中国政治](../Category/580年代中国政治.md "wikilink")
[Category:581年](../Category/581年.md "wikilink")