**大宝**（550年正月—551年十二月）是梁簡文帝[蕭綱的](../Page/蕭綱.md "wikilink")[年号](../Page/年号.md "wikilink")，共计近2年。

[侯景建汉代梁后](../Page/侯景.md "wikilink")，各地拥梁大臣拒绝承认侯景的合法性，继续沿用**大宝**年号至大宝三年（552年）。

## 大事记

## 出生

## 逝世

## 纪年

| 大宝                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 550年                           | 551年                           | 552年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[大宝年號的政權](../Page/大宝.md "wikilink")
  - 同期存在的其他政权年号
      - [天正](../Page/天正_\(萧栋\).md "wikilink")（551年八月—十一月）：[南朝梁政權](../Page/南朝梁.md "wikilink")[豫章王](../Page/豫章.md "wikilink")[萧棟的年号](../Page/萧棟.md "wikilink")
      - [太始](../Page/太始_\(侯景\).md "wikilink")（551年十一月—552年三月）：[南朝梁時期](../Page/南朝梁.md "wikilink")[侯景的年号](../Page/侯景.md "wikilink")
      - [武定](../Page/武定.md "wikilink")（543年正月—550年五月）：[東魏政权](../Page/東魏.md "wikilink")[東魏孝靜帝元善見年号](../Page/東魏孝靜帝.md "wikilink")
      - [天保](../Page/天保_\(高洋\).md "wikilink")（550年五月—559年十二月）：[北齊政权北齊文宣帝](../Page/北齊.md "wikilink")[高洋年号](../Page/高洋.md "wikilink")
      - [-{乾}-明](../Page/乾明.md "wikilink")：[西魏年号](../Page/西魏.md "wikilink")
      - [大統](../Page/大统_\(元宝炬\).md "wikilink")（535年正月—551年十二月）：[西魏政权西魏文帝](../Page/西魏.md "wikilink")[元宝炬年号](../Page/元宝炬.md "wikilink")
      - [永平](../Page/永平_\(麴玄喜\).md "wikilink")（549年—550年）：[高昌政权](../Page/高昌.md "wikilink")[麴玄喜年号](../Page/麴玄喜.md "wikilink")
      - [和平](../Page/和平_\(高昌\).md "wikilink")（551年—554年）：[高昌政权年号](../Page/高昌.md "wikilink")
      - [建元](../Page/建元_\(新羅法興王\).md "wikilink")（536年—551年）：[新羅](../Page/新羅.md "wikilink")[真興王的年号](../Page/新羅真興王.md "wikilink")
      - [開國](../Page/開國.md "wikilink")（551年—568年）：新羅真興王的年號

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南梁年号](../Category/南梁年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:550年代中国政治](../Category/550年代中国政治.md "wikilink")
[Category:550年](../Category/550年.md "wikilink")
[Category:551年](../Category/551年.md "wikilink")