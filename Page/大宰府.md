**大宰府**是[日本在](../Page/日本.md "wikilink")[7世紀時設立管理](../Page/7世紀.md "wikilink")[九州地區的行政機關](../Page/九州.md "wikilink")，設於[筑前國](../Page/筑前國.md "wikilink")（位於現在的[福岡縣](../Page/福岡縣.md "wikilink")[太宰府市](../Page/太宰府市.md "wikilink")），大宰府下設有一職負責所有事務，其下還設有[大宰權帥及大宰大貳兩位副手](../Page/大宰權帥.md "wikilink")。

## 歷史

663年日本在[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[白江口之战大敗後](../Page/白江口之战.md "wikilink")，為了預防中國趁勢進攻日本，在[對馬島](../Page/對馬島.md "wikilink")、[壹岐島及九州的筑紫地區設立防衛據點](../Page/壹岐島.md "wikilink")，並設置[大宰府](../Page/大宰府.md "wikilink")\[1\]，作為九州及周邊區域的軍事指揮中心。

在8世紀之後，大宰府的功能從原本的軍事目的，變成日本的外交窗口與管轄全九州的地方行政機關\[2\]，但也因為遠離朝廷，成為中央流放官員的地方。

大宰府一直到12世紀都是九州地區的政治中心，當時[中國](../Page/中國.md "wikilink")、[朝鮮和日本接觸時](../Page/朝鮮.md "wikilink")，都是以大宰府作為涉外窗口\[3\]，同時也是接待[渡來人的地方](../Page/渡來人.md "wikilink")。12世紀[鎌倉幕府成立後](../Page/鎌倉幕府.md "wikilink")，由於日本朝廷的勢力衰退，政局由[武家所掌控](../Page/武家.md "wikilink")，大宰府失去功能，約在12世紀中時停止運作。\[4\]

在文獻中最初名為「**大**宰府」，但在[8世紀](../Page/8世紀.md "wikilink")[奈良時代之後的文獻開始出現](../Page/奈良時代.md "wikilink")「**太**宰府」的名稱，此後「**太**宰府」使用比例逐漸增加，到了近代的地名中就完全只採「**太**宰府」的用法。\[5\]現今「大宰府」的用法僅使用於表示這歷史的機關。

## 大宰府政廳

**大宰府政廳遺址**位於南側，通稱為「都府樓跡」；共可分為三個時期的建築，包括：

  - 第1期：屬於7世紀後半至8世紀初；為大宰府政廳初建時期建築。
  - 第2期：屬於8世紀初至10世紀中葉；為朝堂院形式建築。（規模：東西111.6公尺、南北188.4公尺）
  - 第3期：屬於10世紀中葉至12世紀；為擴充的朝堂院形式建築。

## 西之都

在大宰府旁被認為同時也有依[里坊制規劃的城市街道](../Page/里坊制.md "wikilink")，在當時日本的規模僅次於平城京，因此也被稱為「西之都」。\[6\]

<File:Ruin> the Tofuro Up200607030320.JPG|大宰府政廳遺跡(都府楼跡) <File:Tofuroｈ>
dazaifu.JPG|大宰府遺跡（都府楼跡）
[File:Dazaifu.gif|大宰府街道推測圖](File:Dazaifu.gif%7C大宰府街道推測圖)

## 參考資料

## 相關條目

  - [太宰府市](../Page/太宰府市.md "wikilink")
  - [大野城 (筑前國)](../Page/大野城_\(筑前國\).md "wikilink")
  - [少貳氏](../Page/少貳氏.md "wikilink")
  - [菅原道真](../Page/菅原道真.md "wikilink")

## 外部連結

  - [日本遺產太宰府](http://www.dazaifu-japan-heritage.jp/zh-tw/intro/)

  - [日本遺產太宰府](http://www.dazaifu-japan-heritage.jp/zh-cn/intro)

  - [古都大宰府保存協會](http://www.kotodazaifu.net/)

[Category:太宰府市](../Category/太宰府市.md "wikilink")
[Category:九州地方歷史](../Category/九州地方歷史.md "wikilink")
[Category:筑前國](../Category/筑前國.md "wikilink")
[Category:西海道](../Category/西海道.md "wikilink")
[Category:特別史跡](../Category/特別史跡.md "wikilink")
[Category:福岡縣史跡](../Category/福岡縣史跡.md "wikilink")
[Category:日本律令制](../Category/日本律令制.md "wikilink")

1.
2.
3.
4.
5.
6.