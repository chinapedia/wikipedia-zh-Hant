**大島**可以指：

## 島嶼

### [日本](../Page/日本.md "wikilink")

  - [太平洋側](../Page/太平洋.md "wikilink")
      - 大島（[北海道](../Page/北海道.md "wikilink")[色丹郡](../Page/色丹郡.md "wikilink")[色丹村](../Page/色丹村.md "wikilink")）
      - 大島（[岩手縣](../Page/岩手縣.md "wikilink")[下閉伊郡](../Page/下閉伊郡.md "wikilink")[山田町](../Page/山田町.md "wikilink")）
      - 大島（）（岩手縣下閉伊郡山田町）
      - [大島](../Page/大島_\(宮城縣氣仙沼市\).md "wikilink")（[宮城縣](../Page/宮城縣.md "wikilink")[氣仙沼市](../Page/氣仙沼市.md "wikilink")）
      - 大島（宮城縣[牡鹿郡](../Page/牡鹿郡.md "wikilink")[女川町](../Page/女川町.md "wikilink")）
      - 大島（[千葉縣](../Page/千葉縣.md "wikilink")[鴨川市](../Page/鴨川市.md "wikilink")）
      - [伊豆大島](../Page/伊豆大島.md "wikilink")（[東京都](../Page/東京都.md "wikilink")[大島支廳](../Page/大島支廳_\(東京都\).md "wikilink")[大島町](../Page/大島町.md "wikilink")）
      - [三河大島](../Page/三河大島.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")[蒲郡市](../Page/蒲郡市.md "wikilink")）
      - 大島（愛知縣[幡豆郡](../Page/幡豆郡.md "wikilink")[一色町](../Page/一色町.md "wikilink")）
      - 大島（[三重縣](../Page/三重縣.md "wikilink")[志摩市志摩町](../Page/志摩市.md "wikilink")）
      - 大島（三重縣[北牟婁郡](../Page/北牟婁郡.md "wikilink")[紀北町](../Page/紀北町.md "wikilink")）（[入会地](../Page/入会地.md "wikilink")）
      - 大島（[和歌山縣](../Page/和歌山縣.md "wikilink")[和歌山市](../Page/和歌山市.md "wikilink")）
      - 大島（和歌山縣[西牟婁郡](../Page/西牟婁郡.md "wikilink")[白濱町](../Page/白濱町.md "wikilink")）
      - [紀伊大島](../Page/紀伊大島.md "wikilink")（和歌山縣[東牟婁郡](../Page/東牟婁郡.md "wikilink")[串本町](../Page/串本町.md "wikilink")）
      - 大島（[德島縣](../Page/德島縣.md "wikilink")[海部郡](../Page/海部郡_\(德島縣\).md "wikilink")[牟岐町](../Page/牟岐町.md "wikilink")）
      - 大島（[愛媛縣](../Page/愛媛縣.md "wikilink")[八幡濱市](../Page/八幡濱市.md "wikilink")）
      - 大島（[高知縣](../Page/高知縣.md "wikilink")[宿毛市](../Page/宿毛市.md "wikilink")）
      - [大島](../Page/大島_\(大分縣佐伯市\).md "wikilink")（[大分縣](../Page/大分縣.md "wikilink")[佐伯市](../Page/佐伯市.md "wikilink")）
      - 大島（[宮崎縣](../Page/宮崎縣.md "wikilink")[日南市](../Page/日南市.md "wikilink")）
  - [日本海](../Page/日本海.md "wikilink")・[東海側](../Page/東海.md "wikilink")
      - [渡島大島](../Page/渡島大島.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[松前郡](../Page/松前郡.md "wikilink")[松前町](../Page/松前町_\(北海道\).md "wikilink")）
      - 大島（[青森縣](../Page/青森縣.md "wikilink")[東津輕郡](../Page/東津輕郡.md "wikilink")[平内町](../Page/平内町.md "wikilink")）
      - 大島（青森縣[西津輕郡](../Page/西津輕郡.md "wikilink")[深浦町](../Page/深浦町.md "wikilink")）
      - 大島（[新潟縣](../Page/新潟縣.md "wikilink")[佐渡市](../Page/佐渡市.md "wikilink")）
      - 大島（新潟縣佐渡市）
      - 大島（[石川縣](../Page/石川縣.md "wikilink")[輪島市](../Page/輪島市.md "wikilink")）
      - 大島（石川縣[七尾市能登島](../Page/七尾市.md "wikilink")）
      - [冠島](../Page/冠島.md "wikilink")（[京都府](../Page/京都府.md "wikilink")[舞鶴市](../Page/舞鶴市.md "wikilink")）的通稱
      - 大島（京都府[京丹後市](../Page/京丹後市.md "wikilink")）
      - 大島（[兵庫縣](../Page/兵庫縣.md "wikilink")[香美町](../Page/香美町.md "wikilink")）
      - 大島（兵庫縣[美方郡](../Page/美方郡.md "wikilink")[新溫泉町](../Page/新溫泉町.md "wikilink")）
      - 大島（[鳥取縣](../Page/鳥取縣.md "wikilink")[鳥取市](../Page/鳥取市.md "wikilink")）
      - 大島（[島根縣](../Page/島根縣.md "wikilink")[江津市](../Page/江津市.md "wikilink")）
      - 大島（島根縣[松江市](../Page/松江市.md "wikilink")）
      - 大島（島根縣[濱田市](../Page/濱田市.md "wikilink")）
      - 大島（島根縣[隱岐郡](../Page/隱岐郡.md "wikilink")[知夫村](../Page/知夫村.md "wikilink")）
      - [大島
        (萩市)](../Page/大島_\(萩市\).md "wikilink")（[山口縣](../Page/山口縣.md "wikilink")[萩市](../Page/萩市.md "wikilink")）
      - 大島（山口縣[長門市](../Page/長門市.md "wikilink")）
      - 大島（山口縣[長門市油谷向津具下](../Page/長門市.md "wikilink")）
      - [大島
        (福岡縣)](../Page/大島_\(福岡縣\).md "wikilink")（[福岡縣](../Page/福岡縣.md "wikilink")[宗像市大島](../Page/宗像市.md "wikilink")）
      - 大島（[長崎縣](../Page/長崎縣.md "wikilink")[佐世保市](../Page/佐世保市.md "wikilink")）
      - [大島
        (對馬市)](../Page/大島_\(對馬市\).md "wikilink")（長崎縣[對馬市](../Page/對馬市.md "wikilink")）
      - 大島（長崎縣[壹岐市](../Page/壹岐市.md "wikilink")）
      - 大島（長崎縣[西海市](../Page/西海市.md "wikilink")）
      - 大島（長崎縣[北松浦郡](../Page/北松浦郡.md "wikilink")[小值賀町](../Page/小值賀町.md "wikilink")）
      - 大島（長崎縣北松浦郡[鹿町町](../Page/鹿町町.md "wikilink")）
      - 大島（[熊本縣](../Page/熊本縣.md "wikilink")[天草市](../Page/天草市.md "wikilink")）
      - [阿久根大島](../Page/阿久根大島.md "wikilink")（[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[阿久根市](../Page/阿久根市.md "wikilink")）
  - [瀨戶內海](../Page/瀨戶內海.md "wikilink")
      - 大島（[岡山縣](../Page/岡山縣.md "wikilink")[笠岡市](../Page/笠岡市.md "wikilink")）
      - [周防大島](../Page/周防大島.md "wikilink")（屋代島）（[山口縣](../Page/山口縣.md "wikilink")[大島郡](../Page/大島郡_\(山口縣\).md "wikilink")[周防大島町](../Page/周防大島町.md "wikilink")）
      - 大島（[香川縣](../Page/香川縣.md "wikilink")[小豆郡](../Page/小豆郡.md "wikilink")[土庄町](../Page/土庄町.md "wikilink")）
      - 大島（香川縣[高松市](../Page/高松市.md "wikilink")）
      - [新居大島](../Page/新居大島.md "wikilink")（[愛媛縣](../Page/愛媛縣.md "wikilink")[新居濱市](../Page/新居濱市.md "wikilink")）
      - [大島
        (愛媛縣)](../Page/大島_\(愛媛縣\).md "wikilink")（愛媛縣[今治市](../Page/今治市.md "wikilink")）
      - 大島（[大分縣](../Page/大分縣.md "wikilink")[杵築市](../Page/杵築市.md "wikilink")）
  - 其他地區
      - 大島（[北海道](../Page/北海道.md "wikilink")[釧路市](../Page/釧路市.md "wikilink")）（[阿寒湖](../Page/阿寒湖.md "wikilink")）
      - [奄美大島](../Page/奄美大島.md "wikilink")（[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[奄美市](../Page/奄美市.md "wikilink")、[大島郡](../Page/大島郡_\(鹿兒島縣\).md "wikilink")[龍鄉町](../Page/龍鄉町.md "wikilink")、[大和村](../Page/大和村.md "wikilink")、[宇檢村](../Page/宇檢村.md "wikilink")、[瀨戶內町](../Page/瀨戶內町.md "wikilink")）

### [美國](../Page/美國.md "wikilink")

  - 大島（Big
    Island）：位於[夏威夷州](../Page/夏威夷州.md "wikilink")[夏威夷群島](../Page/夏威夷群島.md "wikilink")[夏威夷島的俗稱](../Page/夏威夷島.md "wikilink")。

### [俄罗斯](../Page/俄罗斯.md "wikilink")

  - [阿巴该图洲渚](../Page/阿巴该图洲渚.md "wikilink")（Большой остров）

## 地名

### 日本

  - [大島町](../Page/大島町.md "wikilink")：日本曾有5個[行政區劃名為大島町](../Page/行政區劃.md "wikilink")，但目前僅剩東京都[大島町](../Page/大島町.md "wikilink")。
  - [大島村](../Page/大島村.md "wikilink")：日本曾有7個行政區劃名為大島村，但目前皆已被併入其他行政區。
  - [東京都](../Page/東京都.md "wikilink")[江東區大島](../Page/江東區_\(東京\).md "wikilink")：有[大島車站
    (東京都)](../Page/大島車站_\(東京都\).md "wikilink")。
  - [岐阜縣](../Page/岐阜縣.md "wikilink")[郡上市白鳥町大島](../Page/郡上市.md "wikilink")：有[大島車站
    (岐阜縣)](../Page/大島車站_\(岐阜縣\).md "wikilink")。

### [加拿大](../Page/加拿大.md "wikilink")

  - 大島（Big Island）：[努那福特的地名](../Page/努那福特.md "wikilink")。
  - 大島（Big Island）：[安大略省的地名](../Page/安大略省.md "wikilink")。

### [美國](../Page/美國.md "wikilink")

  - [大島縣](../Page/大島縣.md "wikilink")（Grand Isle
    County）：[佛蒙特州的一個行政區劃](../Page/佛蒙特州.md "wikilink")。

## 其他

  - [大島車站](../Page/大島車站.md "wikilink")：日本境內歷史上共有10個以大島為名的[車站](../Page/車站.md "wikilink")。
  - 日本人的[姓](../Page/姓.md "wikilink")。

## 相關條目

  - [大島町 (消歧義)](../Page/大島町_\(消歧義\).md "wikilink")
  - [大島郡](../Page/大島郡.md "wikilink")
  - [大島支廳](../Page/大島支廳.md "wikilink")

[de:Big Island](../Page/de:Big_Island.md "wikilink") [en:Big
Island](../Page/en:Big_Island.md "wikilink") [et:Big
Island](../Page/et:Big_Island.md "wikilink") [sr:Биг
(острво)](../Page/sr:Биг_\(острво\).md "wikilink")

[D大島](../Category/日本地名消歧義.md "wikilink")
[Category:地名姓氏](../Category/地名姓氏.md "wikilink")