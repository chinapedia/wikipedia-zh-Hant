[German_losses_after_WWI.svg](https://zh.wikipedia.org/wiki/File:German_losses_after_WWI.svg "fig:German_losses_after_WWI.svg")和[但泽自由市](../Page/但泽自由市.md "wikilink")。\]\]
[Deutsches_Reich_(1871-1918)-de.svg](https://zh.wikipedia.org/wiki/File:Deutsches_Reich_\(1871-1918\)-de.svg "fig:Deutsches_Reich_(1871-1918)-de.svg")
**大德意志**（[德语](../Page/德语.md "wikilink")：）是一个历史上的思潮。其作为[德意志问题的一个解决方案](../Page/德意志统一.md "wikilink")，在1848年[法兰克福国民议会列席会议上就讨论过](../Page/法兰克福国民议会.md "wikilink")。[奥地利帝国最初提出了这一构想模型](../Page/奥地利帝国.md "wikilink")，希冀建立一个包括奥地利在内的统一的德意志[民族国家](../Page/民族国家.md "wikilink")，且被其领导。这一模型与最终被普鲁士采用的[小德意志统一解决方案所相对](../Page/小德意志.md "wikilink")，且被抛弃。“小德意志”包括了除奥地利以外的所有[德意志邦联国家](../Page/德意志邦联.md "wikilink")，并听命于德意志北方[普鲁士王国的](../Page/普鲁士王国.md "wikilink")[霸权](../Page/霸权.md "wikilink")。

20世纪初，[民族自由主义和](../Page/民族自由主义.md "wikilink")[民主主义开始兴起](../Page/民主主义.md "wikilink")。又因为[一战的和约](../Page/一战.md "wikilink")《[凡尔赛条约](../Page/凡尔赛条约.md "wikilink")》和《[聖日耳曼條約](../Page/聖日耳曼條約.md "wikilink")》对战败的德国惩罚过强，“大德意志”成为了一个与其反抗而带有愤怒感情的概念，并为[德国和](../Page/德国.md "wikilink")[奥地利的右翼党派所用](../Page/德意志奥地利共和国.md "wikilink")。他们的目标是共同建立一个以德意志民族为主体的民族国家，即除德奥外也包括周围以德裔人口为主的地区，无视[法国和](../Page/法国.md "wikilink")[意大利的反对](../Page/意大利.md "wikilink")。纳粹主义者认为，德意志问题的解决方案应该为：在纳粹政权下建立“大德意志帝国”，在欧洲大陆占据主体地位\[1\]，并协助其他非日耳曼地区（主要指斯拉夫人地区\[2\]）日耳曼化\[3\]。

大德意志与泛德意志（）意义不同。后者为德意志帝国时期的一种[极端民族主义和](../Page/极端民族主义.md "wikilink")[反犹主义的集合](../Page/反犹主义.md "wikilink")。

[二次大战結束后](../Page/二次大战.md "wikilink")，《[奥地利国家条约](../Page/奥地利国家条约.md "wikilink")》声明奥地利需保持独立，而不許与德国合併。今日「大德意志」一词，也時常与[纳粹主义相关](../Page/纳粹主义.md "wikilink")。

## 大德意志民族运动

[Maerz1848_berlin.jpg](https://zh.wikipedia.org/wiki/File:Maerz1848_berlin.jpg "fig:Maerz1848_berlin.jpg")
大德意志（）这一个词汇是[法兰克福国民议会主席爱德华](../Page/法兰克福国民议会.md "wikilink")·冯·西姆森（）最先提出来的。这一运动主要在南方的德意志邦国中广泛传播，如[巴登](../Page/巴登.md "wikilink")、[符腾堡](../Page/符腾堡.md "wikilink")、[巴伐利亚以及](../Page/巴伐利亚.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")。这一思想的追随者实际上大多为自由主义者。在[德意志1848年革命中](../Page/德意志1848年革命.md "wikilink")，德国作为一个民族国家的概念开始出现，并认为包括德意志诸国，以及周边[波西米亚](../Page/波西米亚.md "wikilink")、[克拉尼斯卡](../Page/克拉尼斯卡.md "wikilink")、[奥地利海岸区](../Page/奥地利海岸区.md "wikilink")\[4\]等德意志人历史居住的地域。然而这一大德意志的理念当时只能被奥匈帝国所实现，因其占有南方德语区领土。然而若没有[匈牙利王国的革命](../Page/匈牙利王国.md "wikilink")，这理念的实施是不可想象的。随着运动走向衰落，[小德意志方案被提出](../Page/小德意志.md "wikilink")，希冀普鲁士王国建立统一的[君主立宪制德意志国家](../Page/君主立宪制.md "wikilink")。

最终在1871年，[德意志帝国成立](../Page/德意志帝国.md "wikilink")。它只包括普鲁士与其西方的德意志邦国，不包括奥地利的领土。

## 哈布斯堡王朝的大德意志

在[哈布斯堡家族奥地利皇帝](../Page/哈布斯堡家族.md "wikilink")[弗朗茨·约瑟夫一世治下](../Page/弗朗茨·约瑟夫一世.md "wikilink")，和[黑山的](../Page/黑山.md "wikilink")[费利克斯亲王等几次协助努力下](../Page/施瓦岑贝格_\(费利克斯亲王\).md "wikilink")，松散的[德意志邦联得以建立](../Page/德意志邦联.md "wikilink")。这其中最著名的努力是1863年的法兰克福亲王议会（）。邦联宪法使得[奥地利成为统一的德意志国家的领导者](../Page/奥地利.md "wikilink")，傲立于中欧。这也被称为“大奥地利方案”\[5\]。奥地利领导的德意志邦联各国，与普鲁士为首的联合军队在1866年爆发了[普奥战争](../Page/普奥战争.md "wikilink")。奥地利在战争中被以普鲁士为首的西北德意志各国击败。主要原因是奥匈帝国的种族成分复杂，包括大量[匈牙利人](../Page/匈牙利人.md "wikilink")、[羅馬尼亞人和](../Page/羅馬尼亞人.md "wikilink")[斯拉夫人](../Page/斯拉夫人.md "wikilink")。斯拉夫人民族还分为：[波兰人](../Page/波兰人.md "wikilink")、[捷克人](../Page/捷克人.md "wikilink")、[斯洛伐克人](../Page/斯洛伐克人.md "wikilink")、[斯洛文尼亞人](../Page/斯洛文尼亞人.md "wikilink")、[乌克兰人](../Page/乌克兰人.md "wikilink")、[克罗地亚人与](../Page/克罗地亚人.md "wikilink")[塞尔维亚人](../Page/塞尔维亚人.md "wikilink")。以上多数族群根本不愿意与德国人统一。[波西米亚](../Page/波西米亚.md "wikilink")、[摩拉维亚与](../Page/摩拉维亚.md "wikilink")[西里西亚地区的的捷克人更在](../Page/西里西亚.md "wikilink")1848年反对以上的方案。

## 世界大战期间

[German_Reich_1942.svg](https://zh.wikipedia.org/wiki/File:German_Reich_1942.svg "fig:German_Reich_1942.svg")
[一战结束后](../Page/一战.md "wikilink")，战败的[奥匈帝国毁灭](../Page/奥匈帝国.md "wikilink")。原奥匈帝国德语区建立了[奥地利共和国](../Page/奥地利共和国.md "wikilink")。与[魏玛共和国的议会都支持德奥统一](../Page/魏玛共和国.md "wikilink")。奥地利建立了共和国，企图与德国合并。德意志奥地利国会也已经1918年11月12日批准。战间期，[大德意志人民党特别推动德奥合并](../Page/大德意志人民党.md "wikilink")。然而，获胜的[協約國阻止了这决定](../Page/協約國.md "wikilink")。

在1938年，经济复苏后的[纳粹德国实现了](../Page/纳粹德国.md "wikilink")[德奥合并](../Page/德奥合并.md "wikilink")。[希特勒庆祝了大德意志方案的胜利](../Page/希特勒.md "wikilink")，在1943年，把德国国名由[德意志國](../Page/德意志國.md "wikilink")（Deutsches
Reich）更改为[大德意志国](../Page/德意志國.md "wikilink")（Großdeutsches
Reich）。希特勒后来借此夺取[苏台德地区](../Page/苏台德地区.md "wikilink")，甚至后继侵略[波兰](../Page/波兰.md "wikilink")，引起[第二次世界大战](../Page/第二次世界大战.md "wikilink")。

## 当代

纳粹德国被击败后，战胜国决定重新恢复建立奥地利。1945年起，大德意志一词因与纳粹德国联系过于紧密而被丧失了信用。新成立的[联邦德国和](../Page/联邦德国.md "wikilink")[民主德国使用](../Page/民主德国.md "wikilink")“统一德国”一词来描述德意志国家的未来，对应着小德意志解决方案。但新方案的东方边界已经做出了修改。

在奥地利，自1945年以后与德国合并已经不是一个政治相关问题了。1943年同盟国的[莫斯科宣言中](../Page/莫斯科宣言.md "wikilink")，奥地利就作为“希特勒的第一个受害者”被谈及应当重新恢复独立。这也完全对应着战后人民的情绪。1955年，[奥地利国家条约中明确规定](../Page/奥地利国家条约.md "wikilink")，奥地利被禁止与德国结盟，就像1919年圣日耳曼条约一样。自那时候起，大部分奥地利人民加强了自身意识觉醒。小部分人仍然基于传统理念，保有强烈的德意志情绪。

在当今德国和奥地利，大德意志这一理念通常与右翼极端主义和新纳粹主义相关。不过，也有一小部分保守主义者，如[奥地利自由党](../Page/奥地利自由党.md "wikilink")，追求着19世纪大德意志理念的本源。他们期望在[基本法上基础上与德国结盟](../Page/基本法.md "wikilink")，但是避免“合并”。在德国也有一些右翼极端自由主义者希望通过君主制实现大德意志方案。

但总体上，现代已广泛接受存在一个独立的[奥地利人国家](../Page/奥地利人.md "wikilink")。

## 参考资料

## 文献

  - Rudolf Lill: Großdeutsch und Kleindeutsch im Spannungsfeld der
    Konfessionen. In: [Anton
    Rauscher](../Page/Anton_Rauscher.md "wikilink") (Hrsg.): Probleme
    des Konfessionalismus in Deutschland seit 1800, Paderborn 1984, S.
    29–47.
  - Adam Wandruszka: Großdeutsche und kleindeutsche Ideologie 1840–1871.
    In: [Robert Kann](../Page/Robert_Kann.md "wikilink"); [Friedrich
    Prinz](../Page/Friedrich_Prinz.md "wikilink") (Hrsg.): Deutschland
    und Österreich. Ein bilaterales Geschichtsbuch, Wien 1980, S.
    110–142.

## 參見

  - [小德意志](../Page/小德意志.md "wikilink")
  - [德意志邦联](../Page/德意志邦联.md "wikilink")
  - [德意志帝国](../Page/德意志帝国.md "wikilink")（Deutsches Kaiserreich）
  - [大德意志帝國](../Page/大德意志帝國.md "wikilink")（Großdeutsches
    Reich，[納粹德國](../Page/納粹德國.md "wikilink")）
  - [凡尔赛条约](../Page/凡尔赛条约.md "wikilink")
  - [德奧合併](../Page/德奧合併.md "wikilink")
  - [德意志統一](../Page/德意志統一.md "wikilink")、[兩德統一](../Page/兩德統一.md "wikilink")

[分類:德國地理](../Page/分類:德國地理.md "wikilink")
[分類:泛運動](../Page/分類:泛運動.md "wikilink")

[Category:普鲁士历史](../Category/普鲁士历史.md "wikilink")
[Category:德國歷史](../Category/德國歷史.md "wikilink")

1.  Zur Bedeutung siehe die *Akten der Partei-Kanzlei der NSDAP*, Bd. 1,
    Teil 1, RKF 15680 – K 101 13763 f. (722), hrsg. von Helmut Heiber,
    [Institut für
    Zeitgeschichte](../Page/Institut_für_Zeitgeschichte.md "wikilink"),
    Oldenbourg, München 1983,
    [S. 671](http://books.google.de/books?id=E8Oaty1BAwUC&lpg=PA671&dq=slawen%20artverwandtes%20blut&pg=PA671#v=onepage&q=slawen%20artverwandtes%20blut&f=false).
2.  Nach der im Sinne der NS-Rassenideologie letztgenannten Gruppe, s.
    Gertraud Eva Schrage in: *Jahrbuch für die Geschichte Mittel- und
    Ostdeutschlands*, Bd. 54, De Gruyter Saur, München 2008, [210
    f.](http://books.google.de/books?id=tkzBM-6-0RoC&pg=PA210)
3.  Heinrich August Winkler, *Der lange Weg nach Westen*, Bd. 2, C.H.
    Beck, München, 4., durchgesehene Auflage 2002,
    [S. 77 f.](http://books.google.de/books?id=3hQmwTWoqHYC&pg=PA78),
    [87](http://books.google.de/books?id=3hQmwTWoqHYC&pg=PA87); [Der
    Spiegel 45/2008 vom 3.
    November 2008](http://www.spiegel.de/spiegel/print/d-61822072.html).
4.  ，今领土分属意大利、克罗地亚和斯洛文尼亚
5.  Fritz Molden: *Die Österreicher oder Die Macht der Geschichte*, 2.
    Auflage, Langen Müller, München 1987,
    [S. 154](http://books.google.de/books?ei=7w2rUeveHtD2sgaknYH4BQ&hl=de&id=NocMAQAAMAAJ&dq=%22so+genannten+gro%C3%9F%C3%B6sterreichischen+l%C3%B6sung%22&q=%22gro%C3%9F%C3%B6sterreichischen+L%C3%B6sung%22#search_anchor).