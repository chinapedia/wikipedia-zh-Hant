[DSC01089_Hoi_Chu-_Tai_Tak.JPG](https://zh.wikipedia.org/wiki/File:DSC01089_Hoi_Chu-_Tai_Tak.JPG "fig:DSC01089_Hoi_Chu-_Tai_Tak.JPG")

**大德路**是[中國](../Page/中國.md "wikilink")[廣州市](../Page/廣州市.md "wikilink")[越秀區的一條東西走向的](../Page/越秀區.md "wikilink")[道路](../Page/道路.md "wikilink")，位於[广州起义路以西](../Page/广州起义路.md "wikilink")，[上九路以東](../Page/上九路.md "wikilink")。大德路全長1194米，寬16米。全程為西至東單行綫。

## 歷史

[明清时期為廣州城牆](../Page/明清.md "wikilink")。清代大德街（大德路）起，沿尚果里、拐角楼、铁炉巷至诗书街（诗书路）南段是镶黄旗驻地，難以成為街市\[1\]。

1921年，市政府拆城牆，建大德路。因原址有[歸德門](../Page/歸德門.md "wikilink")，故稱大德路。同年长安金属制品厂在大德路开业\[2\]。

[文革時期](../Page/文革.md "wikilink")，1966年为紀念[向秀麗烈士而改称](../Page/向秀麗.md "wikilink")**秀麗三路**，1981年恢復大德路。

而在20世紀上半葉，廣州市的[殯儀館曾在大德路經營](../Page/殯儀館.md "wikilink")。“粵光製殮股份有限公司”、“樂天殯儀有限公司”、“別有天殯儀館”都曾在大德路設分店。[中華人民共和國成立後](../Page/中華人民共和國.md "wikilink")，先後合併遷址[東川路](../Page/东川路_\(广州\).md "wikilink")。

## 特色

在民國拆城墙开大德路後，原左近铁炉巷的鐵匠轉集聚於此，吸引相關行當，形成五金铁器专业街。

至今全路段仍有是[五金舖的集聚地方](../Page/五金.md "wikilink")。由於車多路窄，出入[廣東省中醫院車輛較多](../Page/廣東省中醫院.md "wikilink")，交通在繁忙時間較爲擠塞。

大德路亦是一條典型的廣州老城區道路，還有保留較多的[騎樓建築](../Page/騎樓.md "wikilink")。

2006年，[廣州電視台以一系列路名創作的公益廣告中](../Page/廣州電視台.md "wikilink")，就包括**大德路**，原話為“大德，不止大德路”。\[3\]

## 與之交匯道路

道路顺序由東往西排列

  - **[廣州起義路](../Page/廣州起義路.md "wikilink")**
  - **[解放中路](../Page/解放路_\(广州市\).md "wikilink")**/**[解放南路](../Page/解放路_\(广州市\).md "wikilink")**
  - [海珠中路](../Page/海珠中路.md "wikilink")/[海珠南路](../Page/海珠南路.md "wikilink")
  - [詩書路](../Page/詩書路.md "wikilink")/[天成路](../Page/天成路.md "wikilink")
  - [**人民中路**](../Page/人民路_\(广州\).md "wikilink")/[**人民南路**](../Page/人民路_\(广州\).md "wikilink")

## 附近設施

[唐代曾興建有](../Page/唐代.md "wikilink")**护国仁王禅寺**，至[明代荒廢](../Page/明代.md "wikilink")。在广东提学[魏校大毁广东寺庙时](../Page/魏校.md "wikilink")，仁王寺被改建作**紫阳书院**。書院址現為大德市场\[4\]。

其他道路建築設施有：

  - [大德路小學](../Page/大德路小學.md "wikilink")
  - 天工帆布廠（不存）
  - [亞洲汽水廠](../Page/亞洲汽水.md "wikilink")（不存）
  - [廣東省中醫院](../Page/廣東省中醫院.md "wikilink")

## 交通

巴士

  - 起義路口站
  - 解放南路口站
  - 省中醫院站
  - 大德路站

## 外部連結

<references />

[D大](../Category/广州特色街道.md "wikilink")
[D大](../Category/越秀区.md "wikilink")
[Category:广州经济史](../Category/广州经济史.md "wikilink")

1.  [想打铁啊？去大德路啦！](http://www.sohu.com/a/125298782_526351) 历史现场
    2017-01-31

2.  [大德路：炉火红星乱紫烟](http://www.gzzxws.gov.cn/qxws/yxws/yxzj/yx1_1_1_1/200906/t20090623_13041.htm)
    越秀商业街巷

3.  [“和諧廣州，文明城市”公益廣告](http://www.tudou.com/programs/view/y75jHiqFpKE/)

4.