[Fairwood_(old).svg](https://zh.wikipedia.org/wiki/File:Fairwood_\(old\).svg "fig:Fairwood_(old).svg")
**大快活**（，）是[香港](../Page/香港.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[快餐集團](../Page/快餐.md "wikilink")，主要在香港、[珠三角地区及](../Page/珠三角地区.md "wikilink")[京](../Page/北京.md "wikilink")[津經營連鎖](../Page/天津.md "wikilink")[港式快餐店](../Page/港式快餐.md "wikilink")、分租食品攤位及物業投資等業務。

大快活主要同業競爭對手為[大家樂及](../Page/大家樂.md "wikilink")[美心MX](../Page/美心MX.md "wikilink")。

## 歷史及簡介

大快活與[大家樂的創辦人分別是](../Page/大家樂.md "wikilink")[羅芳祥及](../Page/羅芳祥.md "wikilink")[羅騰祥](../Page/羅騰祥.md "wikilink")，同為[維他奶創辦人](../Page/維他奶.md "wikilink")[羅桂祥的弟弟](../Page/羅桂祥.md "wikilink")。首間大快活於1972年12月在[香港](../Page/香港.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[眾安街開幕](../Page/眾安街.md "wikilink")\[1\]；其後於1981年成立了中央食品加工中心，以確保食品質素及提高效率。集團於1991年10月9日在[香港聯合交易所](../Page/香港聯合交易所.md "wikilink")[上市](../Page/上市.md "wikilink")，當年全線共有52家分店。

現任[董事會主席是](../Page/董事會主席.md "wikilink")[羅開揚](../Page/羅開揚.md "wikilink")，[行政總裁是](../Page/行政總裁.md "wikilink")[陳志成](../Page/陳志成.md "wikilink")。至2008年3月，該集團共有106間店鋪，在香港開設82間快餐店、3間[友天地燒味小廚及](../Page/友天地燒味小廚.md "wikilink")5間特色餐廳；在[中國內地則設有](../Page/中國內地.md "wikilink")12間快餐店。2009年營業額增加6.6%，至15.62億元；毛利率由12.8%擴至
13.9%；錄得純利9326.9萬元，按年增長16.6%。截至2009年3月底，集團於香港共經營101間店鋪，包括96間快餐店、3間友天地及2間特色餐廳，內地則共設有16間快餐店。截至2015年3月底，集團於香港及中國內地共經營130間店鋪。

根据大快活官网，目前中国国内共经营32间快餐店，较2009年翻一倍，主要以广佛地区及莞深地区为主。\[2\]

## 董事會

  - 執行董事

<!-- end list -->

  - [羅開揚](../Page/羅開揚.md "wikilink")（執行主席）
  - 陳志成（行政總裁）
  - 麥綺薇（公司秘書）

<!-- end list -->

  - 非執行董事

<!-- end list -->

  - 吳志強

<!-- end list -->

  - 獨立非執行董事

<!-- end list -->

  - 陳棨年
  - 劉國權
  - [蔡東豪](../Page/蔡東豪.md "wikilink")
  - 尹錦滔

## 形象改革

[Fairwood_interior_2015.jpg](https://zh.wikipedia.org/wiki/File:Fairwood_interior_2015.jpg "fig:Fairwood_interior_2015.jpg")
大快活以往在品牌策略上，並沒有鎖定固定客群，令品牌欠缺鮮明形象，其後於2003年11月耗資1,500萬港元推出全新品牌形象，包括更改商標（原本商標為已有30年歷史的[小丑笑臉圖案](../Page/小丑.md "wikilink")）、改善食物質素及把分店重新裝修，新商標由著名設計師[陳幼堅設計](../Page/陳幼堅.md "wikilink")，日本設計師[森田恭通](../Page/森田恭通.md "wikilink")（Yasumichi
Morita）（日文：もりた　やすみち　英：Morita
Yasumichi）及香港建築師[梁志天則合作為大快活旗下餐廳的室內佈置重新設計](../Page/梁志天.md "wikilink")。希望維持傳統快餐店的效率，及給予顧客咖啡店的格調。在食品方面，引入即磨[咖啡](../Page/咖啡.md "wikilink")、彈牙意粉及秘製香料[咖喱](../Page/咖喱.md "wikilink")[牛腩等](../Page/牛腩.md "wikilink")。

### 獎項

大快活曾獲得多個獎項，包括[HKMA](../Page/HKMA.md "wikilink")/[TVB](../Page/TVB.md "wikilink")2004年度超級品牌大獎、亞洲最具有影響力設計大獎－優秀中國設計、2004亞太區室內設計獎及2005年IF中國設計大獎。

### 代言人

大快活曾由演員[杜汶澤](../Page/杜汶澤.md "wikilink")\[3\]、[梁洛施](../Page/梁洛施.md "wikilink")、「尖Girls」（[泳兒](../Page/泳兒.md "wikilink")、[鍾舒漫及](../Page/鍾舒漫.md "wikilink")[湯怡組成](../Page/湯怡.md "wikilink")，其中泳兒在2006年－2007年連續2年任代言人，鍾及湯在2007年加入任代言人）\[4\]\[5\]、[王祖藍和](../Page/王祖藍.md "wikilink")[林欣彤](../Page/林欣彤.md "wikilink")（2011年）\[6\]出任代言人，現時再度由[杜汶澤擔任代言人](../Page/杜汶澤.md "wikilink")。

## 圖集

Shatin Citylink Fairwood
201108.jpg|[沙田](../Page/沙田.md "wikilink")[連城廣場分店](../Page/連城廣場.md "wikilink")
HK Fairwood
ShuiCheKwunSt.JPG|[元朗](../Page/元朗.md "wikilink")[水車館街分店](../Page/水車館街.md "wikilink")
HK Kln Bay evening 麗晶商場 Richland Garden Shopping Centre interior shop
Fairwood.JPG|[九龍灣](../Page/九龍灣.md "wikilink")[麗晶商場分店](../Page/麗晶花園.md "wikilink")
HK 香港仔中心 Aberdeen Centre mall 大快活 Fairwood Restaurant interior May 2016
DSC.jpg|[香港仔中心分店](../Page/香港仔中心.md "wikilink") Fairwood, Caribbean Coast
branch (Hong Kong).jpg|[映灣薈分店](../Page/映灣園.md "wikilink") HK 觀塘道 392
Kwun Tong Road 創紀之城六期 Millennium City phase 6 mall 大快活餐廳 Fairwood
Restaurant Apr-2013.JPG|[創紀之城六期分店](../Page/創紀之城六期.md "wikilink") Hong
Kong Western Style breakfast at Fairwood Fast Food (Hong
Kong).jpg|大快活所提供的典型西式[早餐](../Page/早餐.md "wikilink") HK CWB
Tung Lo Wan Road 大快活快餐店 Fairwood Restaurant food lunch 斑球腩 fish filets
cooked rice Nov-2013.JPG|大快活提供的中式飯餐 Beef Steak with Turkey Sausage in
Black Pepper Sauce.JPG|大快活的[鐵板餐](../Page/鐵板餐.md "wikilink") Fairwood
Factroy in Tai Po.jpg|[大埔工業邨的中央食品加工中心](../Page/大埔工業邨.md "wikilink")
Peking
Road.jpg|[尖沙咀](../Page/尖沙咀.md "wikilink")[北京道的大快活分店舊商標](../Page/北京道.md "wikilink")（2003年）
Shenzhen LowuCommCity
Fairwood.JPG|[深圳](../Page/深圳.md "wikilink")[罗湖商业城的大快活](../Page/罗湖商业城.md "wikilink")（2008年）
Fairwood in Link
City.jpg|[深圳](../Page/深圳.md "wikilink")[连城新天地的大快活](../Page/连城新天地.md "wikilink")（2019年）

## 其他

### 售賣「4蚊飯」

2012年9月，大快活為慶祝開業40週年，在9月24日至28日一連5日推出1970年代價格水平，只售港幣4元的「4蚊飯」，每天在不同分店發售，引來不少市民來臨光顧，其中以長者佔多數。部份員工更戴上[假髮](../Page/假髮.md "wikilink")、身穿[喇叭褲](../Page/喇叭褲.md "wikilink")，仿照當年制服造型向顧客派發[飯盒](../Page/飯盒.md "wikilink")\[7\]。該4,000多個以「經典價格」售出的飯盒包括[吉列豬扒](../Page/吉列豬扒.md "wikilink")[火腿飯和](../Page/火腿.md "wikilink")[粟米](../Page/粟米.md "wikilink")[肉粒飯](../Page/豬肉.md "wikilink")\[8\]\[9\]。不少顧客表示香港的[通貨膨脹日益嚴重](../Page/通貨膨脹.md "wikilink")，現在吃一頓飯的花費已較7至8年前昂貴一倍，4元一盒飯的光景已成為絕唱。

### 陳年小丑商標的糖包成一時熱話

2017年3月31日，《[明報](../Page/明報.md "wikilink")》報道，有網民在facebook專頁分享印有多年前象徵大快活的經典[小丑商標的](../Page/小丑.md "wikilink")[糖包](../Page/糖.md "wikilink")，這個快餐店商標的小丑頭像從1972年至2003年使用，所以報道估計它已經有14年歷史，引來不少網友熱烈討論，成為一時佳話\[10\]。

### 爭議事件

#### 涉無牌製燒味\[11\]

2011年9月15日，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》報道指大快活有一半分店未有申請「食物製造廠牌照」，卻供應燒味到其他分店。大快活前僱員、第4區區域總廚師梁先生稱，大快活的分店都沒有「食物製造廠牌照」，「大快活一年前已經開始供應燒味到其他分店，但當時偷偷摸摸，都是十間八間這樣做，現在就增加到50間咁做，有的分店供應給兩、三間。」。根據[香港法例](../Page/香港法例.md "wikilink")，在香港樓宇內烹製或製造食品供外售予市民食用，必須向發牌當局申領食物製造廠牌照。

## 參考資料

  -
  -
## 外部連結

  - [大快活官方網頁](http://www.fairwood.com.hk/jspwww/chi/home.html)

[Category:香港连锁餐厅](../Category/香港连锁餐厅.md "wikilink")
[Category:香港快餐店](../Category/香港快餐店.md "wikilink")
[Category:1972年成立的公司](../Category/1972年成立的公司.md "wikilink")
[Category:羅桂祥家族](../Category/羅桂祥家族.md "wikilink")
[Category:香港家族式企業](../Category/香港家族式企業.md "wikilink")
[Category:香港上市飲食業公司](../Category/香港上市飲食業公司.md "wikilink")

1.
2.  [内地分点详细介绍](http://www.fairwood.com.hk/jspwww/chi/location_cn.html#guangzhou)


3.  廣告照
    [1](http://3.bp.blogspot.com/-_nszQDMjNkc/Uz2hKQNTY5I/AAAAAAAAKH8/tD-0macV8Ng/s1600/DSC07040.jpg)[2](http://2.bp.blogspot.com/-kGSjJeJb5bo/Uz2hLK3Cm2I/AAAAAAAAKIQ/_4ppOdc39sY/s1600/DSC07040a.jpg)

4.

5.  [廣告照](https://people42.files.wordpress.com/2013/05/793e6a3577ee8204f3aee2f5d4553baf.jpeg)

6.

7.

8.

9.

10.

11.