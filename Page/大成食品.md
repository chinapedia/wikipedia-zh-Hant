**大成食品（亞洲）有限公司**，是一家[東亞地區的大型](../Page/東亞.md "wikilink")[雞肉供應商](../Page/雞肉.md "wikilink")。該公司成立於1995年，其總部位在[香港](../Page/香港.md "wikilink")，[母公司為](../Page/母公司.md "wikilink")[大成長城企業](../Page/大成長城企業.md "wikilink")。該企業現為[肯德基在](../Page/肯德基.md "wikilink")[中國大陸最主要的雞肉供應商](../Page/中國大陸.md "wikilink")\[1\]，其主要競爭對手為[卜蜂](../Page/卜蜂集團.md "wikilink")。\[2\]

## 歷史

1957年，韓浩然與牟清善在臺灣[臺南縣開設](../Page/臺南縣.md "wikilink")[黃豆](../Page/黃豆.md "wikilink")[榨](../Page/壓榨_\(葡萄酒\).md "wikilink")[油](../Page/油.md "wikilink")[工廠](../Page/工廠.md "wikilink")。\[3\]\[4\]1960年，[投標購入長城麵粉](../Page/投標.md "wikilink")，從事[麵粉生產業](../Page/麵粉.md "wikilink")。1971年，出品[沙拉油](../Page/沙拉油.md "wikilink")。1974年，[大成集團在臺南成立](../Page/大成集團.md "wikilink")。1978年，大成集團在[臺灣證券交易所上市](../Page/臺灣證券交易所.md "wikilink")，屬於臺灣農畜食品股票上市公司（，1978年5月20日上市）。

1982年，[統一企業](../Page/統一企業.md "wikilink")、[泰華油脂](../Page/泰華油脂.md "wikilink")、[大成長城](../Page/大成長城.md "wikilink")、[三菱商事等企業在臺灣臺南](../Page/三菱商事.md "wikilink")[官田合資成立榨油廠公司](../Page/官田區.md "wikilink")「大統益」（，1996年2月9日上市）；該廠產出之油品由投資企業各自以其品牌通路銷售。
\[5\]

1989年，在[印尼](../Page/印尼.md "wikilink")[泗水開辦](../Page/泗水_\(印尼\).md "wikilink")[水產工廠](../Page/水產.md "wikilink")，又在[中國大陸](../Page/中國大陸.md "wikilink")[遼寧](../Page/遼寧.md "wikilink")[瀋陽設立](../Page/瀋陽.md "wikilink")[飼料廠](../Page/飼料.md "wikilink")，[廣東](../Page/廣東.md "wikilink")[深圳](../Page/深圳.md "wikilink")[蛇口設](../Page/蛇口工业区.md "wikilink")[麵粉廠](../Page/麵粉廠.md "wikilink")。

1995年，與[丸紅株式會社合資在遼寧](../Page/丸紅株式會社.md "wikilink")[大連成立](../Page/大連.md "wikilink")「大成食品(大連)有限公司」，於大連瓦房店市砲臺經濟開發區從事種雞飼養、雞雛孵化、飼料產銷、肉雞屠宰及深加工[一條龍產業化經營](../Page/供應鏈.md "wikilink")。\[6\]

2007年，大成食品(亞洲)在[香港交易所上市](../Page/香港交易所.md "wikilink")。

## 相關條目

  - [百勝餐飲集團](../Page/百勝餐飲集團.md "wikilink")
  - [肯德基餐廳](../Page/肯德基.md "wikilink")
  - [大成長城企業](../Page/大成長城企業.md "wikilink")

## 參考

## 外部連結

  - [大成食品](http://www.dachanfoodasia.com/)

[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:中國食品公司](../Category/中國食品公司.md "wikilink")
[Category:台灣食品公司](../Category/台灣食品公司.md "wikilink")
[Category:大連公司](../Category/大連公司.md "wikilink")

1.
2.

3.

4.

5.

6.  [大成食品（大连）有限公司](http://www.dlaefi.org.cn/v/member/member-intro/dacheng)