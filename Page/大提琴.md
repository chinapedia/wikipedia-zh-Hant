**大提琴**（；）是一種[弓弦樂器](../Page/弓弦樂器.md "wikilink")，為[維奧爾家族](../Page/維奧爾.md "wikilink")（包括[小提琴](../Page/小提琴.md "wikilink")、[中提琴](../Page/中提琴.md "wikilink")、**大提琴**及[低音提琴](../Page/低音提琴.md "wikilink")）樂器之一，除可擔當獨奏外，在[室內樂](../Page/室內樂.md "wikilink")、[弦樂團和](../Page/弦樂團.md "wikilink")[管弦樂團中也負責低音弦樂的部份](../Page/管弦樂團.md "wikilink")。

大提琴演奏者最初是坐著且兩腳夾琴演奏，後來改良增加[尾針支撐琴體](../Page/尾針.md "wikilink")。但仍有少部分國家的演奏家是夾琴呈幾乎水平的姿態演奏。\[1\]其最常見的演奏方式是持弓擦過琴上的弦（拉弦）\[2\]，但有時也會撥弦或敲弦。

大提琴每條弦之間的音程相差[完全五度](../Page/完全五度.md "wikilink")。4根[琴弦由高音至低音稱為](../Page/弦_\(樂器\).md "wikilink")1弦至4弦，音高分別為A<sup>3</sup>、D<sup>3</sup>、G<sup>2</sup>和C<sup>2</sup>\[3\]。大提琴譜通常使用[低音譜號記譜](../Page/低音譜號.md "wikilink")\[4\]；而演奏較高音域時會使用[次中音譜號或](../Page/次中音譜號.md "wikilink")[高音譜號](../Page/高音譜號.md "wikilink")。\[5\]

[NW_Folklife_2008_-_Ashraf_Hakim_01.jpg](https://zh.wikipedia.org/wiki/File:NW_Folklife_2008_-_Ashraf_Hakim_01.jpg "fig:NW_Folklife_2008_-_Ashraf_Hakim_01.jpg")

大提琴有時候會在[國樂團作為低音樂器以取代](../Page/國樂團.md "wikilink")[低胡](../Page/低胡.md "wikilink")，後有人發明[琶琴](../Page/琶琴.md "wikilink")、[拉阮](../Page/拉阮.md "wikilink")、[革胡等低音乐器](../Page/革胡.md "wikilink")，但效果未如大提琴。

## 簡述

大提琴以[完全五度定弦](../Page/完全五度.md "wikilink")，從C<sub>2</sub>開始（中間C下雙八度），其次是G<sub>2</sub>、D<sub>3</sub>，最高弦是A<sub>3</sub>。這四條弦與[中提琴的定弦相同](../Page/中提琴.md "wikilink")，只是下降了八度。\[6\]與[小提琴](../Page/小提琴.md "wikilink")、中提琴不同（但與[低音提琴相似](../Page/低音提琴.md "wikilink")）的是，大提琴具有擱在地板上的支架「尾針」，以支持樂器的重量。大提琴與[歐洲古典音樂密不可分](../Page/古典音乐.md "wikilink")，並被描述為「最接近人聲的樂器」。\[7\]\[8\]\[9\]大提琴是[樂團弦樂部分的標準配備](../Page/管弦樂團.md "wikilink")，也是[弦樂四重奏的低音伴奏](../Page/弦樂四重奏.md "wikilink")（儘管許多作曲家也讓它演奏主旋律）。

## 歷史

大提琴發源於十六世紀，由[義大利人](../Page/義大利.md "wikilink")[安德雷亚·阿马蒂製造出世界上第一把大提琴](../Page/安德雷亚·阿马蒂.md "wikilink")。初期大提琴的體積過於龐大，難以彈奏快速音群，因而不受歡迎；直到18世紀初，義大利製琴師[史特拉底瓦里縮小並統一了大提琴的規格](../Page/安东尼奥·斯特拉迪瓦里.md "wikilink")，改良演奏上的困難，因此而沿用至今。\[10\]

隨著18、19世紀的不斷改良，19世紀到21世紀，大提琴已經可以獨奏，並有著許多出名的獨奏曲。\[11\]

### 現代使用

#### 管弦樂團

大提琴是標準[交響樂團編制的一部分](../Page/交響樂團.md "wikilink")，一個樂團通常有八至十二個大提琴演奏者。大提琴一般坐在舞台左前方（觀眾右側）\[12\]，與第一小提琴聲部相對。大提琴首席是此聲部的領導，負責確認演奏者的弓法及演奏，並通常擔當樂曲中獨奏的部分。首席總是坐在最靠近觀眾的位置。

大提琴是管弦樂的重要組成部分；所有交響作品幾乎都涉及大提琴，並有許多作品需要大提琴來獨奏。大部分時間，大提琴負責低音[和聲伴奏](../Page/和聲.md "wikilink")，以豐富音樂效果。有時大提琴會演奏一小段主旋律，隨後恢復伴奏的身分。\[13\]

#### [中樂團](../Page/中樂團.md "wikilink")

由於[國樂中低音樂器甚是缺乏](../Page/國樂.md "wikilink")，國樂團遂引進大提琴作為低音弦樂聲部來取代[低胡](../Page/低胡.md "wikilink")。\[14\]後有人發明[拉阮](../Page/拉阮.md "wikilink")、[革胡等低音樂器](../Page/革胡.md "wikilink")，但效果未如大提琴。\[15\]

## 著名曲目

歷代作曲家們為大提琴寫了大量的[協奏曲和](../Page/協奏曲.md "wikilink")[奏鳴曲](../Page/奏鳴曲.md "wikilink")。其中最知名的[巴洛克時期作品大提琴是](../Page/巴洛克艺术.md "wikilink")[约翰·巴赫的](../Page/约翰·塞巴斯蒂安·巴赫.md "wikilink")《[無伴奏大提琴组曲](../Page/无伴奏大提琴组曲.md "wikilink")》\[16\]\[17\]，其序曲〈Prelude〉尤為著名。[古典樂派時期以來](../Page/古典主義音乐.md "wikilink")，[约瑟夫·海顿的兩首](../Page/约瑟夫·海顿.md "wikilink")、協奏曲脫穎而出\[18\]，而[路德维希·范·贝多芬所作的](../Page/路德维希·范·贝多芬.md "wikilink")5首大提琴與古鋼琴的奏鳴曲亦是一枝獨秀，這5首樂曲跨越了其作曲生涯中的3個重要時期。[浪漫樂派時期的曲目包括](../Page/浪漫主義音樂.md "wikilink")[罗伯特·舒曼的](../Page/罗伯特·舒曼.md "wikilink")、[安东宁·德沃夏克的](../Page/安东宁·德沃夏克.md "wikilink")[協奏曲和](../Page/大提琴協奏曲_\(德沃夏克\).md "wikilink")[約翰尼斯·布拉姆斯的兩首奏鳴曲以及](../Page/約翰尼斯·布拉姆斯.md "wikilink")。二十世紀初的樂曲主要有[爱德华·埃尔加的](../Page/爱德华·埃尔加.md "wikilink")\[19\]、[阿希爾-克勞德·德布西的](../Page/阿希尔-克洛德·德彪西.md "wikilink")以及[柯达伊·佐尔丹和](../Page/柯达伊·佐尔丹.md "wikilink")[保羅·欣德米特的無伴奏大提琴奏鳴曲](../Page/保羅·欣德米特.md "wikilink")。由於大提琴豐富的可應用性以及現代音樂獨奏者（如、[罗斯特罗波维奇等人](../Page/姆斯蒂斯拉夫·列奥波尔多维奇·罗斯特罗波维奇.md "wikilink")）陸續地委託、鼓舞作曲家創作，大提琴深獲20世紀中、後期作曲家，如[普羅高菲夫](../Page/謝爾蓋·謝爾蓋耶維奇·普羅科菲耶夫.md "wikilink")、[肖斯塔科维奇](../Page/德米特里·德米特里耶维奇·肖斯塔科维奇.md "wikilink")、[本杰明·布里顿](../Page/本杰明·布里顿.md "wikilink")、[利盖蒂·捷尔吉](../Page/利盖蒂·捷尔吉.md "wikilink")、[维托尔德·卢托斯瓦夫斯基和](../Page/维托尔德·卢托斯瓦夫斯基.md "wikilink")[亨利·杜替耶等人的青睞](../Page/亨利·杜替耶.md "wikilink")。

## 參考來源

## 外部連結

  - [The Internet Cello Society](http://www.cello.org): an online
    community of cellists; includes several forums.
  - [cellist.nl](http://cellist.nl/): An international register of
    professional cellists, teachers, and students.

[Category:弓弦樂器](../Category/弓弦樂器.md "wikilink")

1.
2.

3.
4.
5.

6.
7.

8.

9.
10.

11.
12.
13.

14.

15.
16.
17.
18.
19.