{{ Infobox Film | name = 敵對邊緣 | original_name = *Enemy at the Gates* |
image = Enemy at the gates ver2.jpg | image_size = | caption =
《敵對邊緣》英語海報 | director =
[让-雅克·阿诺](../Page/让-雅克·阿诺.md "wikilink") |
producer = [让-雅克·阿诺](../Page/让-雅克·阿诺.md "wikilink")
[約翰·史考菲德](../Page/約翰·史考菲德.md "wikilink") | writer =
[让-雅克·阿诺](../Page/让-雅克·阿诺.md "wikilink")
[艾倫·哥達](../Page/艾倫·哥達.md "wikilink") | narrator = | starring =
[約瑟夫·范恩斯](../Page/約瑟夫·范恩斯.md "wikilink")
[艾德·哈里斯](../Page/艾德·哈里斯.md "wikilink")
[瑞秋·懷茲](../Page/瑞秋·懷茲.md "wikilink")
[裘德·洛](../Page/裘德·洛.md "wikilink") | music =
[詹姆斯·霍納](../Page/詹姆斯·霍納.md "wikilink") |
cinematography = [羅伯·費斯](../Page/羅伯·費斯.md "wikilink") | editing =
[諾勒·波森](../Page/諾勒·波森.md "wikilink")
[匈福瑞·狄克森](../Page/匈福瑞·狄克森.md "wikilink") | distributor =
[派拉蒙影業](../Page/派拉蒙影業.md "wikilink") | released = 2001年3月16日
2001年4月20日 | runtime = 131 分鐘 | country =

\[1\] | language = [英語](../Page/英語.md "wikilink")
[德語](../Page/德語.md "wikilink")
[俄語](../Page/俄語.md "wikilink") | budget = 6800萬美元 \[2\] | gross =
$96,976,270美元\[3\] | preceded_by = | followed_by = | website = |
amg_id = | imdb_id = 0215750 | cn_name = 兵临城下 | hk_name = 敵對邊緣 |
tw_name = 大敵當前 }}
《**大敵當前**》（），2001年的電影，由[让-雅克·阿诺執導](../Page/让-雅克·阿诺.md "wikilink")。片名是從1973年的書《大敵當前：史達林格勒戰役》（）擷取而來，描述[苏德战争](../Page/苏德战争.md "wikilink")[史達林格勒戰役時](../Page/史達林格勒戰役.md "wikilink")，蘇俄傳奇狙擊手[瓦西里·扎伊采夫上尉與德國狙击手](../Page/瓦西里·扎伊采夫.md "wikilink")[柯尼希上校決鬥的故事](../Page/海恩兹·托尔伐特.md "wikilink")。现在史界一般认为这场对决是当时苏联军事宣传需要而产生的传奇故事，并非实际存在。德军也不存在柯尼希此人。儘管此故事為虛構，電影的架構大部分來源於瓦西里·扎伊采夫口述的故事。

## 主角

  - [裘德·洛](../Page/裘德·洛.md "wikilink") 飾演
    [蘇聯陸軍狙擊手師士兵](../Page/蘇聯陸軍.md "wikilink")
    [瓦西里·扎伊采夫](../Page/瓦西里·扎伊采夫.md "wikilink")

  - [艾德·哈里斯](../Page/艾德·哈里斯.md "wikilink") 飾演
    [納粹德國](../Page/納粹德國.md "wikilink")[少校](../Page/少校.md "wikilink")、[措森狙擊學校校長](../Page/措森.md "wikilink")
    [艾文·科尼希](../Page/海恩茲·托爾伐特.md "wikilink")

  - [瑞秋·懷茲](../Page/瑞秋·懷茲.md "wikilink") 飾演
    [蘇聯民兵](../Page/蘇聯.md "wikilink")

  - [約瑟夫·范恩斯](../Page/約瑟夫·范恩斯.md "wikilink") 飾演
    蘇聯總參謀部[政治委員](../Page/政治委員.md "wikilink")
    丹尼洛夫（Commissar Danilov）

  - [鮑伯·霍金斯](../Page/鮑伯·霍金斯.md "wikilink") 飾演
    蘇聯[中將](../Page/中將.md "wikilink")、史達林格勒[方面軍](../Page/方面軍.md "wikilink")[黨委書記](../Page/黨委書記.md "wikilink")
    [尼基塔·謝爾蓋耶維奇·赫魯雪夫](../Page/尼基塔·謝爾蓋耶維奇·赫魯雪夫.md "wikilink")

  - 飾演 納粹德國裝甲軍團[中將](../Page/中將.md "wikilink")
    [弗里德里希·包路斯](../Page/弗里德里希·包路斯.md "wikilink")

  - [朗·帕爾曼](../Page/朗·帕爾曼.md "wikilink") 飾演 蘇聯陸軍狙擊手師士兵 庫利科夫（Koulikov）

  - [加布里埃爾·湯姆森](../Page/加布里埃爾·湯姆森.md "wikilink") 飾演 崇拜瓦西里，同時接近艾文的男童

## 参考资料

## 外部連結

  -
  -
  -
  -
  -
[Category:2001年電影](../Category/2001年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:德語電影](../Category/德語電影.md "wikilink")
[Category:俄語電影](../Category/俄語電影.md "wikilink")
[Category:美國戰爭劇情片](../Category/美國戰爭劇情片.md "wikilink")
[Category:法国电影作品](../Category/法国电影作品.md "wikilink")
[Category:德國電影作品](../Category/德國電影作品.md "wikilink")
[Category:英國電影作品](../Category/英國電影作品.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:苏德战争电影](../Category/苏德战争电影.md "wikilink")
[Category:歐洲真人真事改編電影](../Category/歐洲真人真事改編電影.md "wikilink")
[Category:小說改編電影](../Category/小說改編電影.md "wikilink")
[Category:狙擊戰](../Category/狙擊戰.md "wikilink")
[Category:1942年背景电影](../Category/1942年背景电影.md "wikilink")
[Category:蘇聯背景電影](../Category/蘇聯背景電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:詹姆斯·霍纳配乐电影](../Category/詹姆斯·霍纳配乐电影.md "wikilink")
[Category:让-雅克·阿诺电影](../Category/让-雅克·阿诺电影.md "wikilink")

1.

2.

3.