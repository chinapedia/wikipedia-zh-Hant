**大明**（931年-937年）是[大义宁](../Page/大义宁.md "wikilink")[杨干真的](../Page/杨干真.md "wikilink")[年号](../Page/年号.md "wikilink")，共计7年。诸书记载其后还有**[鼎新](../Page/鼎新.md "wikilink")**，**[光圣](../Page/光圣.md "wikilink")**（或作**[克圣](../Page/克圣.md "wikilink")**）。

李家瑞认为\[1\]光圣乃[兴圣之误](../Page/兴圣.md "wikilink")。《滇云历年记》综各书作改元光圣，又改兴圣，李崇智认为\[2\]亦误。李兆洛《纪元编》又称光圣一作克圣，注曰：别见。

又[沈德符](../Page/沈德符.md "wikilink")《正闰考》载有鼎新年号，不详何时。《护国寺南钞》中有“义宁二年”之语，李家瑞认为\[3\]应是杨干真国号纪年，而非年号。《云南志略》又有不同，谓“干真国号义宁，改元曰光圣，曰皇兴，曰大明，曰鼎新，曰建国，凡九年。”今不取。

## 纪年

| 大明                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 931年                           | 932年                           | 933年                           | 934年                           | 935年                           | 936年                           | 937年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") |

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[大明年号](../Page/大明.md "wikilink")
  - 同期存在的其他政权年号
      - [長興](../Page/長興_\(李嗣源\).md "wikilink")（930年二月至933年十二月）：後唐—李嗣源之年號
      - [應順](../Page/應順.md "wikilink")（934年正月至四月）：後唐—[李從厚之年號](../Page/李從厚.md "wikilink")
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：[吳](../Page/吴_\(十国\).md "wikilink")—[楊溥之年號](../Page/楊溥.md "wikilink")
      - [天祚](../Page/天祚.md "wikilink")（935年九月至937年十月）：吳—楊溥之年號
      - [昇元](../Page/昇元.md "wikilink")（937年十月至943年二月）：[南唐](../Page/南唐.md "wikilink")—[李昪之年號](../Page/李昪.md "wikilink")
      - [龍啟](../Page/龍啟.md "wikilink")（933年至934年）：[閩](../Page/闽_\(十国\).md "wikilink")—[王延鈞之年號](../Page/王延鈞.md "wikilink")
      - [永和](../Page/永和_\(王延鈞\).md "wikilink")（935年正月至936年二月）：閩—王延鈞之年號
      - [通文](../Page/通文.md "wikilink")（936年三月至939年七月）：閩—[王繼鵬之年號](../Page/王繼鵬.md "wikilink")
      - [宝正](../Page/宝正.md "wikilink")（926年至931年）：[吳越](../Page/吳越.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[-{于}-闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗太祖](../Page/高麗.md "wikilink")[王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年[閏四月十一日至](../Page/閏四月十一.md "wikilink")931年[四月廿六](../Page/四月廿六.md "wikilink")）：[日本](../Page/日本.md "wikilink")[平安時代之](../Page/平安時代.md "wikilink")[醍醐天皇年号](../Page/醍醐天皇.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本天皇[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")

## 参考文献

-----

**鼎新**

-----

**光圣**

[Category:南诏年号](../Category/南诏年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")

1.  李家瑞，用文物补正南诏及大理国的纪年，历史研究，1958年第7期

2.  李崇智，中国历代年号考，中华书局，2004年，128 ISBN 7101025129

3.