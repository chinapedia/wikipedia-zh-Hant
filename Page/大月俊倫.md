**大月俊倫**出生於日本[茨城縣](../Page/茨城縣.md "wikilink")，目前為日本[Kingrecords的執行長](../Page/Kingrecords.md "wikilink")、前[Starchild的](../Page/Starchild.md "wikilink")[總監](../Page/總監.md "wikilink")（），另外也是動畫、特攝企劃公司[GANSIS](../Page/GANSIS.md "wikilink")（鋼吉斯）的社長。大月畢業於[日本大學農獸醫學系](../Page/日本大學.md "wikilink")。目前為[大阪藝術大學藝術學系人物造形學科的動畫課](../Page/大阪藝術大學.md "wikilink")[客座教授](../Page/客座教授.md "wikilink")，[日本工學院](../Page/日本工學院.md "wikilink")、[81
Produce的臨時特別講師](../Page/81_Produce.md "wikilink")。

## 經歷

1984年，大月還在學時就以打工之姿進入Kingrecords所屬的Starchild。在這期間經由他之手製作的原聲帶有『[哥吉拉](../Page/哥吉拉.md "wikilink")』、『[必殺仕事人](../Page/必殺仕事人.md "wikilink")』等。

身為音樂製作人來說，大月在1990年代起積極開發新進有潛力的女性聲優，成為聲優潮的推動者。特別是大月發掘了[林原惠的才能](../Page/林原惠.md "wikilink")；而林原事實上也身為Starchild的專屬藝人，並且銷售成績極佳，最終成為名人。

身為動畫的製作人來說，大月也有為數不少的動畫作品在其上展露頭角，特別是1995年的[新世紀福音戰士造成社會現象的大熱潮](../Page/新世紀福音戰士.md "wikilink")，因之也成為名製作人。

以[新世紀福音戰士的這個好例子來看](../Page/新世紀福音戰士.md "wikilink")，大月多積極地運用動畫創作者的強烈個性，其作品多被指稱有較強的[作家性風格及鮮少的劣作等意見](../Page/作家.md "wikilink")。

但另一方面，他還是免不了動畫CG化熱潮的製作態勢而放棄原來不願調整的企劃，如『[秋葉原電腦組](../Page/秋葉原電腦組.md "wikilink")』、『[妹妹公主](../Page/妹妹公主.md "wikilink")』、『[魔法老師](../Page/魔法老師_\(動畫\).md "wikilink")』等作品則因時程的壓迫下而製作，播放時仍有腳本與作畫的問題，而『[失落的宇宙](../Page/失落的宇宙.md "wikilink")』第四話則是快速作畫而導致[作畫崩壞](../Page/作畫崩壞.md "wikilink")，動畫迷則視為傳說。

動畫迷之間則有「大月作品的成功與失敗相當劇烈」、「好壞難分」等評論，在某個廣播節目也發生「大騙氏」、「大亂搞氏」等惡搞的名字來稱呼大月俊倫。而因為「大月」的姓之故，也有被稱作英文「Big
Moon」的情況。他因為有動畫界易怒及可怕人物前三大之稱，也有數度使林原惠哭泣的說法出現。

另外，大月對於預算的管理外傳也是相當嚴謹，以『[鐵人28號](../Page/鐵人28號.md "wikilink")』來說，針對[今川泰宏的機器人動畫認為的](../Page/今川泰宏.md "wikilink")「400
cut以上×張數」是無法完成的這件事，他認為有必要將策略作全面轉換。

此外，大月本人與深交的好友[千葉繁](../Page/千葉繁.md "wikilink")，一同推動主導清水愛的出道（之後的[Lantis與](../Page/Lantis.md "wikilink")[Geneon
Entertainment有關的活動](../Page/Geneon_Entertainment.md "wikilink")，主要也是他們構成的）。

「超星神」系列作，大月本人是沒有在片頭、片尾中打出其名，反而是寫上GANSIS的名稱。

對以上作品的製作與收益管理來說，大月是相當嚴謹的人，但他也是對於作品內容不會去干涉與自我思想的傳播去加諸其中的人。他所主導的作品中，以往所看到思想非中立作品的產出是很少的，關於這些部分，是被動畫迷們抱持一定的信賴感的。

## 相關作品的正式頭銜

  - **製作人**
  - **執行製作**（幾乎包辦的都是藝人的CD。而其中一部分則是Kingrecords但不是Starchild所屬的藝人，如水樹奈奈的CD，中途才掛上其名）
  - **企劃**（最近不以個人名稱，以GANSIS的名義居多）

另有其他稱呼族繁不及備載。

## 主要手中完成的作品

### 動畫

  - [強殖裝甲](../Page/強殖裝甲.md "wikilink")（[OVA](../Page/OVA.md "wikilink")）
  - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")
  - [失落的宇宙](../Page/失落的宇宙.md "wikilink")
  - [魁\!\!天兵高校](../Page/魁!!天兵高校.md "wikilink")
  - [秋葉原電腦組劇場版](../Page/秋葉原電腦組.md "wikilink")‧2011年的暑假
  - [機動戰艦撫子號劇場版](../Page/機動戰艦撫子.md "wikilink")‧ -The prince of darkness-
  - [少女革命](../Page/少女革命.md "wikilink")
  - [無敵王](../Page/無敵王.md "wikilink")
  - [櫻花大戰劇場版](../Page/櫻花大戰.md "wikilink")‧活動寫真
  - [秀逗魔導士](../Page/秀逗魔導士.md "wikilink")
  - [時空幻境3](../Page/傳奇系列.md "wikilink")
  - [羅蕾萊](../Page/羅蕾萊.md "wikilink")（Loreley）
  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink")
  - [陸上防衛隊](../Page/陸上防衛隊.md "wikilink")
  - [朝霧的巫女](../Page/朝霧的巫女.md "wikilink")
  - [鐵人28號](../Page/鐵人28號.md "wikilink")（第四部作品）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")
  - [處女愛上姊姊](../Page/處女愛上姊姊.md "wikilink")
  - [犬神](../Page/犬神.md "wikilink")
  - [校園烏托邦 學美向前衝！](../Page/校園烏托邦_學美向前衝！.md "wikilink")
  - [魔法老師](../Page/魔法老師.md "wikilink")
  - [英雄時代](../Page/英雄時代.md "wikilink")
  - [藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")

### 特攝

  - [超星神系列](../Page/超星神.md "wikilink")
  - [獅子丸G](../Page/獅子丸G.md "wikilink")

### 電視劇

  -
另有其他，都是Starchild與旗下藝人相關的作品。

[Category:日本音樂製作人](../Category/日本音樂製作人.md "wikilink")
[Category:動畫相關人物](../Category/動畫相關人物.md "wikilink")
[Category:茨城縣出身人物](../Category/茨城縣出身人物.md "wikilink") [Category:King
Records人物](../Category/King_Records人物.md "wikilink")