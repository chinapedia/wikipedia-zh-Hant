[HK_TaiYauBuilding.JPG](https://zh.wikipedia.org/wiki/File:HK_TaiYauBuilding.JPG "fig:HK_TaiYauBuilding.JPG")
[Tai_Yau_Plaza_Void_2015.jpg](https://zh.wikipedia.org/wiki/File:Tai_Yau_Plaza_Void_2015.jpg "fig:Tai_Yau_Plaza_Void_2015.jpg")
[HK_Wan_Chai_night_Tai_Yau_Plaza_大有商場_exterior_Fleming_Road_March_2016_DSC_(6).JPG](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_night_Tai_Yau_Plaza_大有商場_exterior_Fleming_Road_March_2016_DSC_\(6\).JPG "fig:HK_Wan_Chai_night_Tai_Yau_Plaza_大有商場_exterior_Fleming_Road_March_2016_DSC_(6).JPG")
**大有大廈**（），是[灣仔區內的一座商業大廈](../Page/灣仔區.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[莊士敦道](../Page/莊士敦道.md "wikilink")181號。大廈於1985年落成，樓高24層。

寫字樓租戶包括[澳洲會計師公會](../Page/澳洲會計師公會.md "wikilink")（CPA
Australia，20樓全層）、琥珀教育、迅興信貸等。基座為「大有廣場」，樓高5層，有多間服裝零售商店、食肆及酒樓。

## 歷史

大有大廈的前身，南面近電車路是[英京大酒家](../Page/英京大酒家.md "wikilink")，北面近[譚臣道有香港](../Page/譚臣道.md "wikilink")[東方戲院](../Page/東方戲院.md "wikilink")。

英京大酒家是舊灣仔的地標，1960年代曾經設宴款待[英國女王皇夫](../Page/英國.md "wikilink")[菲臘親王](../Page/菲利普亲王_\(爱丁堡公爵\).md "wikilink")。東方戲院是當年香港首輪西方電影的戲院。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[灣仔站A](../Page/灣仔站.md "wikilink")3出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 翻新工程

大有大廈基座設樓高5層的「大有商場」，在2006年中進行大型裝修工程，同年11月重開，並且改名為「大有廣場」（Tai Yau
Plaza）。與其譚臣道以北鄰近的「[大同大廈](../Page/大同大廈.md "wikilink")」（Tai Tung
Building）裝修風格一致。「大有廣場」更不定期舉辦展覽活動。

<File:27-07-06> 1234.jpg|2006年底大有商場改名為「大有廣場」 <File:27-07-06>
1237.jpg|大廈前身是[英京大酒家及](../Page/英京大酒家.md "wikilink")[東方戲院](../Page/東方戲院.md "wikilink")

## 外部連結

  - [大有廣場](http://taiyauplaza.com/en/index.html)
  - [德成置業有限公司的大有大廈資料](https://web.archive.org/web/20070311015336/http://www.yp.com.hk/product/templates/geniypoldtwophotopage1.asp?prd_master_id=136432&contract_id=IYP-Old_Profile_Page&company_id=223476&page_id=&website_id=1&language_id=9)
  - <http://edmundchowkalai.mysinablog.com/index.php?op=ViewArticle&articleId=134514>
  - <http://rainmoon.mocasting.com/?p=59455&comment_type=1>
  - <http://edmundchowkalai.mysinablog.com/index.php?op=ViewArticle&articleId=131477>
  - <https://web.archive.org/web/20060703101236/http://bowen.chi.cuhk.edu.hk/question_bank/dbs/3/19.htm>

[Category:灣仔區寫字樓](../Category/灣仔區寫字樓.md "wikilink")
[Category:灣仔區商場](../Category/灣仔區商場.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")