[Tai_Lam_Chung_2015.jpg](https://zh.wikipedia.org/wiki/File:Tai_Lam_Chung_2015.jpg "fig:Tai_Lam_Chung_2015.jpg")望向大欖涌\]\]
[Maritime_Service_Training_Institue_201504.jpg](https://zh.wikipedia.org/wiki/File:Maritime_Service_Training_Institue_201504.jpg "fig:Maritime_Service_Training_Institue_201504.jpg")\]\]
**大欖**（）（有時將**欖**寫作**杬**），又稱**大欖涌**（），是[香港](../Page/香港.md "wikilink")[屯門區最東南面地方](../Page/屯門區.md "wikilink")，位於[青山公路](../Page/青山公路.md "wikilink")16英里处，[小欖之東](../Page/小欖_\(香港\).md "wikilink")，與[荃灣區的](../Page/荃灣區.md "wikilink")[青龍頭接壤](../Page/青龍頭.md "wikilink")。

「大欖涌」原指由「大欖」旁邊流出海的[涌](../Page/涌.md "wikilink")，涌旁亦有一村落以此為名；但現時已升格為分區地名，與「大欖」通用。因現時大部份人不懂「涌」字原意，而將原來河涌誤稱為「大欖涌河」或「大欖涌渠」\[1\]。

## 政府設施

  - [海關訓練學校](../Page/海關訓練學校.md "wikilink")
  - [大欖懲教所](../Page/大欖懲教所.md "wikilink")
  - [大欖女懲教所](../Page/大欖女懲教所.md "wikilink")
  - [大欖涌消防局](../Page/大欖涌消防局.md "wikilink")
  - [職業訓練局海事訓練學院](../Page/職業訓練局海事訓練學院.md "wikilink")
  - [職業訓練局和富慈善基金李宗德博士全人發展教育中心](../Page/職業訓練局和富慈善基金李宗德博士全人發展教育中心.md "wikilink")

## 康樂設施

有兩個[燒烤場](../Page/燒烤場.md "wikilink")，分別位於[大欖涌路](../Page/大欖涌路.md "wikilink")。

## 學校

  - [大欖涌公立學校](../Page/大欖涌公立學校.md "wikilink")（已被[殺校](../Page/殺校.md "wikilink")）
  - [躍思（大欖）幼稚園](../Page/躍思（大欖）幼稚園.md "wikilink")
  - [大欖涌幼稚園](../Page/大欖涌幼稚園.md "wikilink")（已經[結業](../Page/結業.md "wikilink")）

[File:HK_TaiLamChungPublicSchool.JPG|大欖涌公立學校](File:HK_TaiLamChungPublicSchool.JPG%7C大欖涌公立學校)
[File:HK_CreativityTaiLamKindergarten.JPG|躍思（大欖）幼稚園](File:HK_CreativityTaiLamKindergarten.JPG%7C躍思（大欖）幼稚園)

## 鄉村

  - [大欖涌村](../Page/大欖涌村.md "wikilink")
  - [黄屋村](../Page/黄屋村.md "wikilink")
  - [胡屋村](../Page/胡屋村.md "wikilink")
  - [聯安新村](../Page/聯安新村.md "wikilink")

## 交通

  - [大欖涌巴士總站](../Page/大欖涌巴士總站.md "wikilink")

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00 margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

  - [屯門公路轉車站](../Page/屯門公路轉車站.md "wikilink")（往屯門方向）

## 誤解

以「大欖」命名的[大欖隧道並不能通往](../Page/大欖隧道.md "wikilink")**大欖**，隧道只是穿過[大欖郊野公園](../Page/大欖郊野公園.md "wikilink")。

## 區議會議席分佈

由於大欖和小欖現時以低密度住宅為主，人口較少，為方便比較，以下列表以[青山公路大欖段沿線地區為範圍](../Page/青山公路.md "wikilink")。

| 年度/範圍                                   | 2000-2003                                                                   | 2004-2007                                                                            | 2008-2011 | 2012-2015 | 2016-2019 | 2020-2023 |
| --------------------------------------- | --------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ | --------- | --------- | --------- | --------- |
| [青山公路大欖段以南](../Page/青山公路.md "wikilink") | [三聖選區](../Page/三聖_\(選區\).md "wikilink")                                     | [三聖選區](../Page/三聖_\(選區\).md "wikilink")                                              |           |           |           |           |
| [青山公路大欖段以北](../Page/青山公路.md "wikilink") | 分散在[恆福選區及](../Page/恆福.md "wikilink")[三聖選區](../Page/三聖_\(選區\).md "wikilink") | 分散在[掃管笏選區及](../Page/掃管笏_\(選區\).md "wikilink")[三聖選區](../Page/三聖_\(選區\).md "wikilink") |           |           |           |           |

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參見

  - [小欖](../Page/小欖_\(香港\).md "wikilink")：位於大欖涌以西

## 注釋

<references/>

{{-}}

[Category:大欖涌](../Category/大欖涌.md "wikilink")
[Category:屯門區](../Category/屯門區.md "wikilink")

1.