**大歧舌苔**（[學名](../Page/學名.md "wikilink")：）又名，**狭瓣苔**\[1\]，是[苔綱](../Page/苔綱.md "wikilink")[葉苔目](../Page/葉苔目.md "wikilink")[歧舌苔科之下的一個物種](../Page/歧舌苔科.md "wikilink")；[屬有爭議](../Page/屬_\(生物\).md "wikilink")，有作[歧舌苔屬](../Page/歧舌苔屬.md "wikilink")\[2\]或[狭瓣苔屬](../Page/狭瓣苔屬.md "wikilink")（）\[3\]的。

## 特徵

较粗大，扁平，黄绿色或草绿色，无光泽，有层次的成片平横贴生基质。

[茎长](../Page/茎.md "wikilink")4-5厘米，[连叶片宽](../Page/连叶片.md "wikilink")0.8-1厘米，稀少分枝，腹面及侧面具不规则叉状分枝的鳞毛。叶有2列，干燥时略卷曲，湿润时平展，紧密覆瓦状抱茎;背瓣小于腹瓣，舌形，尖部平截，具一长齿，与腹瓣连接处形成背翅。腹瓣长椭圆形或长舌形，长约5毫米，宽1.3毫米，尖部宽阔，渐尖或近于平截，共多数粗齿；边缘波曲，基部有2-3条毛状齿；叶细胞圆形，三角体极显明。雌雄异株。孢蒴长椭圆形，不常见。

見於[海南岛](../Page/海南岛.md "wikilink")、[锡兰及](../Page/锡兰.md "wikilink")[印度尼西亞](../Page/印度尼西亞.md "wikilink")，着生沟谷温暖而湿润的林内树干或阴湿岩面。

## 亞種

在2007年之前，[菲律賓狹瓣苔](../Page/菲律賓狹瓣苔.md "wikilink")（）被認為是本物種的亞種\[4\]；其後獨立成單一物種\[5\]\[6\]。

## 參考資料

## 外部連結

  -
[aligera](../Page/category:歧舌苔属.md "wikilink")

[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")

1.

2.

3.
4.
5.  Lai (2007 as G. aligera and G. philippinensis)

6.  Gao & Wu (2008 as G. aligera and G. philippinensis)