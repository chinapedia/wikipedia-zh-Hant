**大气科学**研究[大气的结构](../Page/大气.md "wikilink")、组成、物理现象、化学反应、运动规律，是[地球科学的一个分支](../Page/地球科学.md "wikilink")。研究对象主要是[地球以及](../Page/地球.md "wikilink")[太阳系其他行星的](../Page/太阳系.md "wikilink")[大气圈](../Page/大气圈.md "wikilink")。大氣研究的時空範圍很廣，空間[尺度從一個城市](../Page/尺度_\(天氣\).md "wikilink")、區城向全球擴展，研究的時間尺度則從幾天到幾年，以至幾十年不等。研究的對象主要是[對流層和](../Page/對流層.md "wikilink")[平流層](../Page/平流層.md "wikilink")。研究的手段有[現場觀測](../Page/In_situ.md "wikilink")、[遙測](../Page/遙測.md "wikilink")、和[數值模擬等](../Page/數值模擬.md "wikilink")。

## 大气动力学

大气动力学涉及观测研究以及关于具有气象学重要性的运动系统的理论
。这些系统包括[雷暴](../Page/雷暴.md "wikilink")，[龙卷风](../Page/龙卷风.md "wikilink")，[重力波](../Page/重力波.md "wikilink")，[热带气旋](../Page/热带气旋.md "wikilink")，[温带气旋](../Page/温带气旋.md "wikilink")，[急流](../Page/急流.md "wikilink")，和全球规模的环流。大气动力学研究的目标是用基本的物理原理解释所观测到的环流。这些研究的目标包括改善[天气预报](../Page/天气预报.md "wikilink")，发展预报季节和年际气候波动的方法和理解人为扰动（例如，增加二氧化碳浓度或臭氧层耗竭）与全球气候的内在关系。\[1\]

## 大气物理学

大气物理学是物理学在大气研究中的应用。大气物理学家们试图通过利用流体流动方程、化学模型、辐射平衡以及大气和深层海洋的能量传输过程，模拟地球和其他行星的大气层。为了模拟天气系统，大气物理学家利用散射理论、波的传播模式、[云物理](../Page/云物理.md "wikilink")、统计力学和空间[统计学的原理](../Page/统计学.md "wikilink")，而这些原理是高度数学化和物理相关的。大气物理学与[气象学和](../Page/气象学.md "wikilink")[气候学都有密切联系](../Page/气候学.md "wikilink")，而且涵盖设计和建造用于研究大气的仪器，以及对它们——包括遥感仪器——提供的数据的解释。

在英国，大气研究建基于气象局。美国[国家海洋和大气管理局](../Page/国家海洋和大气管理局.md "wikilink")（NOAA）的各司监督研究项目和涉及大气物理的天气模拟。美国国家天文学和电离层中心还开展高层大气研究。

## 大气化学

[Atmosphere_composition_diagram-en.svg](https://zh.wikipedia.org/wiki/File:Atmosphere_composition_diagram-en.svg "fig:Atmosphere_composition_diagram-en.svg")物在大氣中的傳遞及沉降。\]\]

大气科学是对地球及其他行星大气进行研究的一门科学。这是一个多学科研究领域，需要借鉴环境化学、物理学、气象学、计算机模拟、海洋学、地质学、火山学以及其他学科，並涉及光化學、均相非均相反應動力學、大氣擴散理論、痕量分析化學等領域；不僅研究大氣的化學反應，還要研究大氣成分複雜的物理化學過程。

由於一些原因使得大气组成及其化学很重要，但主要是因为与大气和生物之间的相互作用。地球大气组成已被人类活动改变，其中有些改变时对人类健康、作物和生态系统有害的。大氣化學之研究成果与其他领域（如气候学）愈來愈相关联。其中通过大气化学研究，已解决问题的例子包括酸雨，光化学烟雾和全球变暖。大气化学致力于找出了解这类问题的原因，并通过获得这类问题的理论性的理解，以便试验可能的解决方案，以及对政府政策变化的影响进行评估。

## 綜觀天氣學

## 大氣測計學

## 气候学

[El_Nino_regional_impacts.png](https://zh.wikipedia.org/wiki/File:El_Nino_regional_impacts.png "fig:El_Nino_regional_impacts.png")

相对于研究短期[天气系统数周的](../Page/天气系统.md "wikilink")[天氣學而言](../Page/天氣學.md "wikilink")，气候学則主要研究這些系統發生的頻率與趨勢。气候学关注从多年直至千年的天气事件的周期性，以及与大气条件相关的长期天气平均模式。
**气候学家**的研究包括：局地或区域或全球自然气候、导致气候变化的自然和人为因素。气候学基本观点是过去的气候状况能够帮助预报未来的[气候变化](../Page/气候变化.md "wikilink")。

气候学关注的气候现象包括[大气边界层](../Page/大气边界层.md "wikilink")、[环流模式](../Page/大气环流.md "wikilink")、[热力传输](../Page/热力传输.md "wikilink")（[辐射](../Page/热辐射.md "wikilink")、[对流和](../Page/对流.md "wikilink")[潜热通量](../Page/潜热通量.md "wikilink")）、[海气相互作用和](../Page/海气相互作用.md "wikilink")[地貌](../Page/地貌.md "wikilink")（特别是[植被](../Page/植被.md "wikilink")，[土地使用和](../Page/土地使用.md "wikilink")[地形](../Page/地形.md "wikilink")）
、大气化学组成和物理结构。相关学科包括[天体物理学](../Page/天体物理学.md "wikilink")，[大气物理学](../Page/大气物理学.md "wikilink")，[化学](../Page/化学.md "wikilink")，[生态学](../Page/生态学.md "wikilink")，[地质学](../Page/地质学.md "wikilink")，[地球物理学](../Page/地球物理学.md "wikilink")，[冰川学](../Page/冰川学.md "wikilink")，[水文学](../Page/水文学.md "wikilink")，[海洋学](../Page/海洋学.md "wikilink")，和[火山学等](../Page/火山学.md "wikilink")。

## 其他行星之大气层

## 参考文献

### 引用

### 来源

  - B. J. Finlayson-Pitts and J. N. Pitts, Jr., *Atmospheric Chemistry:
    Fundamentals and Experimental Techniques*, John Wiley & Sons, 1986,
    NewYork

## 外部链接

  - [Atmospheric fluid dynamics applied to weather
    maps](http://www.stuffintheair.com/chasing-storms.html) - Principles
    such as Advection, Deformation and Vorticity

  - [中国科普博览-大气科学馆](http://www.kepu.com.cn/gb/earth/weather/index.html)

{{-}}

[Category:地球科学分支](../Category/地球科学分支.md "wikilink")
[大气科学](../Category/大气科学.md "wikilink")

1.  [University of
    Washington](../Page/University_of_Washington.md "wikilink").
    [Atmospheric
    Dynamics.](http://www.atmos.washington.edu/academic/atmosdyn.html)
    Retrieved on 1 June 2007.