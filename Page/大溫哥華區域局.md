[Greater_Vancouver_Regional_District,_British_Columbia_Location.png](https://zh.wikipedia.org/wiki/File:Greater_Vancouver_Regional_District,_British_Columbia_Location.png "fig:Greater_Vancouver_Regional_District,_British_Columbia_Location.png")地區在加拿大卑詩省的位置\]\]
[View_of_Vancouver_from_Arbutus_Ridge.JPG](https://zh.wikipedia.org/wiki/File:View_of_Vancouver_from_Arbutus_Ridge.JPG "fig:View_of_Vancouver_from_Arbutus_Ridge.JPG")
[SkyTrain_in_New_Westminster.jpg](https://zh.wikipedia.org/wiki/File:SkyTrain_in_New_Westminster.jpg "fig:SkyTrain_in_New_Westminster.jpg")\]\]
**大溫哥華區域局**（[英語](../Page/英語.md "wikilink")：）是[加拿大](../Page/加拿大.md "wikilink")[卑詩省的一個](../Page/卑詩省.md "wikilink")[區域局](../Page/不列颠哥伦比亚省行政区划.md "wikilink")，位於卑詩省西南部的[低陸平原](../Page/低陸平原.md "wikilink")，是包含[溫哥華在内的](../Page/溫哥華.md "wikilink")[都會區](../Page/都會區.md "wikilink")。就是**[大溫哥華地區](../Page/大溫哥華.md "wikilink")**。區域局的最大城市是[溫哥華](../Page/溫哥華.md "wikilink")，行政中心則在[本拿比](../Page/本拿比.md "wikilink")。大溫區域局的主要職能是管理和協調區内各城鎮的共用資源以及應付各城鎮所面對的共同問題，如區域性規劃、食水供應、污水處理、交通、空氣質素等。

[加拿大統計局所界定的溫哥華人口普查區與](../Page/加拿大統計局.md "wikilink")[大溫區域局的界綫完全相同](../Page/大溫區域局.md "wikilink")。根据2016年人口普查，[大温地区的人口有](../Page/大温地区.md "wikilink")2,463,431人，较2011年普查人口2,313,328人上升6.5%，大概占[卑诗省人口一半](../Page/卑诗省.md "wikilink")。
\[1\]省內30個人口最高的城鎮中，有13個都是位於大溫地區。 \[2\]大溫地區的面積有2878.52
平方公里，是省內人口密度最高的區域局。

## 歷史

大溫區域局的歷史可以追溯至20世紀初。[溫哥華及區域聯合污水處理局](../Page/溫哥華及區域聯合污水處理局.md "wikilink")（）於1914年成立，\[3\]\[4\]協調溫哥華地區城鎮的污水處理事務。大溫水務局（）則於1926年成立，管理區内的食水供應。

[菲沙河水位於](../Page/菲沙河.md "wikilink")1948年暴漲引致大溫哥華廣泛地區水浸，並造成嚴重破壞，反映出城鎮之間協調規劃工作的重要性。有見及此，省政府於1949年成立低陸平原區域規劃局（），以便計劃都會區内未來的發展，規劃局並於1950年落實區域性的排水和堤壩工程方案。\[5\]污水處理局則於1956年改稱大溫污水處理局（）。

到1965年，卑詩省政府著手整理省内的地方行政區劃，設立了「區域局」此行政等級。低陸平原區域規劃局於1966年解散，而覆蓋溫哥華地區的區域局則於1967年6月29日正式成立，當時名為「菲沙－布勒區域局」（）\[6\]，到1968年則改稱「大溫哥華區域局」（，簡稱）\[7\]。區域局當時轄有13個市鎮，而污水處理局和水務局亦併入區域局的行政架構。\[8\][杜德尼-亞魯艾特區域局於](../Page/:en:Dewdney-Alouette_Regional_District.md "wikilink")1995年解散後\[9\]，原屬該區域局的[匹特草原和](../Page/匹特草原.md "wikilink")[楓樹嶺改歸大溫哥華](../Page/楓樹嶺.md "wikilink")。

大溫區域局於2007年決定將其英文名稱改為「」。新名稱於同年九月生效，但其[專利特許證仍沿用舊稱](../Page/專利特許證.md "wikilink")。

## 人口

根据[加拿大统计局](../Page/加拿大统计局.md "wikilink")2016年人口普查，[大温哥华地区的人口为](../Page/大温哥华地区.md "wikilink")2,463,431人，比2011年2,313,328人增加了6.5％。私人住宅1,027,613户，其中有人居住的私人住宅的960,894户，空置率6.5%。
土地面积为2,883平方公里（1,113平方英里），2016年人口密度为855人
/平方公里（2,213人/平方英里），使其成为[卑诗省](../Page/卑诗省.md "wikilink")，人口和人口密度最大的地区。

## 市鎮

[Metro_Vancouver_Chinese_Map.png](https://zh.wikipedia.org/wiki/File:Metro_Vancouver_Chinese_Map.png "fig:Metro_Vancouver_Chinese_Map.png")

大溫區域局轄下有21個市鎮，如下：

| 市鎮                                       | 英文名字                        | 類別   | 人口 （2016） | 加入年份  |
| ---------------------------------------- | --------------------------- | ---- | --------- | ----- |
| [安莫爾](../Page/安莫爾.md "wikilink")         | Anmore                      | 村    | 2,210     | 1997年 |
| [貝卡拉](../Page/貝卡拉.md "wikilink")         | Belcarra                    | 村    | 643       | 1993年 |
| [寶雲島](../Page/寶雲島.md "wikilink")         | Bowen Island                | 海島市鎮 | 3,680     | 1968年 |
| [本拿比](../Page/本拿比.md "wikilink")         | Burnaby                     | 市    | 232,755   | 1967年 |
| [高貴林](../Page/高貴林.md "wikilink")         | Coquitlam                   | 市    | 139,284   | 1967年 |
| [三角洲](../Page/三角洲_\(卑詩省\).md "wikilink") | Delta                       | 區域市鎮 | 102,238   | 1967年 |
| [蘭里市](../Page/蘭里市.md "wikilink")         | Langley City                | 市    | 25,888    | 1988年 |
| [蘭里區](../Page/蘭里區.md "wikilink")         | District of Langley         | 區域市鎮 | 117,285   | 1988年 |
| [獅子灣](../Page/獅子灣.md "wikilink")         | Lions Bay                   | 村    | 1,334     | 1972年 |
| [楓樹嶺](../Page/楓樹嶺.md "wikilink")         | Maple Ridge                 | 區域市鎮 | 82,256    | 1995年 |
| [新威斯敏斯特](../Page/新威斯敏斯特.md "wikilink")   | New Westminster             | 市    | 70,996    | 1967年 |
| [北溫哥華市](../Page/北溫哥華市.md "wikilink")     | City of North Vancouver     | 市    | 52,898    | 1967年 |
| [北溫哥華區](../Page/北溫哥華區.md "wikilink")     | District of North Vancouver | 區域市鎮 | 85,935    | 1967年 |
| [匹特草原](../Page/匹特草原.md "wikilink")       | Pitt Meadows                | 市    | 18,573    | 1995年 |
| [高貴林港](../Page/高貴林港.md "wikilink")       | Port Coquitlam              | 市    | 58,612    | 1967年 |
| [滿地寶](../Page/滿地寶.md "wikilink")         | Port Moody                  | 市    | 33,551    | 1967年 |
| [列治文](../Page/列治文.md "wikilink")         | Richmond                    | 市    | 198,309   | 1967年 |
| [素里](../Page/素里.md "wikilink")           | Surrey                      | 市    | 517,887   | 1967年 |
| [溫哥華](../Page/溫哥華.md "wikilink")         | Vancouver                   | 市    | 631,486   | 1967年 |
| [西溫哥華](../Page/西溫哥華.md "wikilink")       | West Vancouver              | 區域市鎮 | 42,473    | 1967年 |
| [白石](../Page/白石_\(卑詩省\).md "wikilink")   | White Rock                  | 市    | 19,952    | 1967年 |

另外，[大溫區域局境内](../Page/大溫區域局.md "wikilink")[不設地方行政架構的地區](../Page/:en:Unincorporated_area.md "wikilink")（如[大學保留地](../Page/大學保留地.md "wikilink")、北溫和西溫以北的高地以及[菲沙河上的](../Page/菲沙河.md "wikilink")[班斯敦島](../Page/:en:Barnston_Island,_British_Columbia.md "wikilink")）則統稱[大温A选区](../Page/大温A选区.md "wikilink")。2016年[大温A选区的人口為](../Page/大温A选区.md "wikilink")16,133。

區域局境内另設17個原住民保留地，不受任何市鎮管轄。2005年保留地的人口合共為7,177。當中，[措瓦森原住民](../Page/措瓦森.md "wikilink")（）也是[大溫區域局和大溫水務局成員之一](../Page/大溫區域局.md "wikilink")\[10\]。

其他位於低陸平原東部的市鎮（如[阿伯茨福德](../Page/阿伯茨福德.md "wikilink")、[智利域和](../Page/智利域.md "wikilink")[米遜](../Page/:en:Mission,_British_Columbia.md "wikilink")）雖然隷屬[菲沙河谷區域局](../Page/菲沙河谷區域局.md "wikilink")，但有时也被當成[大溫地區的一部分](../Page/大溫地區.md "wikilink")。當中阿伯茨福德也是[大溫區域局成員之一](../Page/大溫區域局.md "wikilink")，但只限於公園事務\[11\]。

## 參考文獻

<div class="references-small">

<references/>

</div>

## 外部連結

  - [大溫哥華區域局官方網站](http://www.metrovancouver.org/)

[不列颠哥伦比亚省行政区划](../Category/不列颠哥伦比亚省行政区划.md "wikilink")
[大溫哥華地區](../Category/大溫哥華地區.md "wikilink")

1.

2.  [加拿大统计局](http://www12.statcan.ca/english/census01/products/standard/popdwell/Table-CSD-P.cfm?PR=59&T=1&SR=1&S=1&O=A)
    - BC municipalities - Population

3.  [Metro Vancouver -
    History](http://www.metrovancouver.org/about/Pages/history.aspx)

4.  [Western Economic Diversification
    Canada](http://www.wd.gc.ca/ced/wuf/capable/7a_e.asp) - The Greater
    Vancouver Regional District (GVRD)

5.  [Timeline of Local Government in
    B.C.](http://www.cd.gov.bc.ca/lgd/history/pdfs/timeline.pdf)

6.

7.

8.
9.  [BCGNIS Geographical Name
    Details](http://www.env.gov.bc.ca/bcgn-bin/bcg10?name=14745)

10.

11.