**大潟村**（）是位于[秋田縣中部的](../Page/秋田縣.md "wikilink")[村](../Page/村.md "wikilink")。面積170.05平方公里，總人口3,313人。

## 地理

  - 山:[大潟富士](../Page/大潟富士.md "wikilink")
  - 湖沼:[八郎潟](../Page/八郎潟.md "wikilink")

### 相鄰的自治体

  - [男鹿市](../Page/男鹿市.md "wikilink")、[潟上市](../Page/潟上市.md "wikilink")
  - [南秋田郡](../Page/南秋田郡.md "wikilink")：[八郎潟町](../Page/八郎潟町.md "wikilink")、[五城目町](../Page/五城目町.md "wikilink")、[井川町](../Page/井川町.md "wikilink")
  - [山本郡](../Page/山本郡.md "wikilink")：[三種町](../Page/三種町.md "wikilink")

## 歷史

  - 1957年 - 八郎潟[围垦事業開始](../Page/围垦.md "wikilink")。
  - 1964年10月1日 - 秋田縣内第69個自治体大潟村誕生。

## 外部連結

  - [大潟村](http://www.ogata.or.jp/index.htm)

[Ogata-mura](../Category/秋田縣的市町村.md "wikilink")