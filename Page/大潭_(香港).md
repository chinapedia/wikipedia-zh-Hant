[Tai_Tam_Tuk_Reservoir.jpg](https://zh.wikipedia.org/wiki/File:Tai_Tam_Tuk_Reservoir.jpg "fig:Tai_Tam_Tuk_Reservoir.jpg")**大潭**（）是一個位於[香港](../Page/香港.md "wikilink")[港島](../Page/香港島.md "wikilink")[南區東南部的](../Page/南區_\(香港\).md "wikilink")[近郊地方](../Page/近郊.md "wikilink")，[赤柱以東](../Page/赤柱.md "wikilink")、[石澳以西](../Page/石澳.md "wikilink")。其唯一主要道路是[大潭道](../Page/大潭道.md "wikilink")，大部分地方為[大潭水塘](../Page/大潭水塘.md "wikilink")、[大潭郊野公園和](../Page/大潭郊野公園.md "wikilink")[大潭郊野公園（鰂魚涌擴建部份）所覆蓋](../Page/大潭郊野公園（鰂魚涌擴建部份）.md "wikilink")。沿著大潭道的都是一些[歷史悠久的豪華公寓](../Page/歷史.md "wikilink")。大潭，名字意指「一大片的深水處」，亦即[赤柱半島](../Page/赤柱半島.md "wikilink")、[鶴咀半島和大潭篤所包圍的三角水域](../Page/鶴咀半島.md "wikilink")。

## 地理位置

[缩略图](https://zh.wikipedia.org/wiki/File:TaiTamMapWithWord.jpg "fig:缩略图")
大潭的東邊是[大潭港和](../Page/大潭港.md "wikilink")[大潭灣這兩個海灣](../Page/大潭灣.md "wikilink")。它們位於[赤柱半島和](../Page/赤柱半島.md "wikilink")[鶴咀半島兩](../Page/鶴咀半島.md "wikilink")[半島之間](../Page/半島.md "wikilink")。

大潭的南邊是[紅山半島](../Page/紅山半島.md "wikilink")，該半島大部分土地興建成低層豪宅；西邊是大潭水塘和大潭郊野公園；北邊鄰近[柴灣](../Page/柴灣.md "wikilink")。

現在的大潭比以前[殖民地時期初期所指的範圍少](../Page/香港開埠初期歷史.md "wikilink")，所以「大潭篤」和「大潭頭」分別是指以前大潭的北端和大潭半島的南端。

## 歷史

[Tsing_Yi_Island_in_Yuet_Tai_Kei.png](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Island_in_Yuet_Tai_Kei.png "fig:Tsing_Yi_Island_in_Yuet_Tai_Kei.png")》中已標有**大潭**\]\]
「大潭」一名在[明朝著作的](../Page/明朝.md "wikilink")《[粵大記](../Page/粵大記.md "wikilink")》內已經有記載。

位於附近的大潭灣（Tytam
Bay）曾經作為18世紀代表香港島的一個港口\[1\]，而[維多利亞港在當時的航海記載中只是港島北部的一個海峽](../Page/維多利亞港.md "wikilink")，而且地勢險要及[海盜猖獗](../Page/海盗.md "wikilink")。為安全起見，船隻都取道南方的[萬山群島到達](../Page/萬山群島.md "wikilink")[澳門和](../Page/澳門.md "wikilink")[廣州](../Page/广州市.md "wikilink")。因此當時大潭灣亦稱作「香港港口」（Hong-kong
Harbour）。

大潭灣最早的記載見於[美國船長約翰](../Page/美国.md "wikilink")．肯德里克（John
Kendrick）於1792年9月的航海記錄，他帶領商船「華盛頓夫人號」（Lady
Washington）駛經「澳門航道」前，取香港島以南的「[担竿水道](../Page/担竿水道.md "wikilink")」，並中途於大潭灣內取用食水及向村民採購一些魚和家禽作補給\[2\]。

直至[開埠初期](../Page/香港開埠初期歷史.md "wikilink")，英國人在港島南岸（即大潭灣、香港仔和赤柱一帶）以及港島北岸（即中上環一帶）之間作研究物色軍事、行政和商業中心，結果發現南岸的海灣分散而且被不少山坡阻隔，難以發展道路基建。這也是他們最後放棄南岸，而選擇在北岸建設[維多利亞城的其中一個原因](../Page/維多利亞城.md "wikilink")。

## 設施

[Tai_Tam_Scout_Centre_201307.jpg](https://zh.wikipedia.org/wiki/File:Tai_Tam_Scout_Centre_201307.jpg "fig:Tai_Tam_Scout_Centre_201307.jpg")[香港童軍總會也在大潭港上設立](../Page/香港童軍總會.md "wikilink")[大潭童軍中心](../Page/大潭童軍中心.md "wikilink")，以用作宿營之用。該童軍中心鄰近大潭村，村外是一大片的[紅樹林](../Page/紅樹林.md "wikilink")。

此外，包辦中小學的[香港國際學校也位於大潭](../Page/香港國際學校.md "wikilink")。

## 區議會議席分佈

為方便比較，以下列表以[赤柱峽道](../Page/赤柱峽道.md "wikilink")、[赤柱村道](../Page/赤柱村道.md "wikilink")、[大潭道交界起向東至](../Page/大潭道.md "wikilink")[石澳道](../Page/石澳道.md "wikilink")、[大潭道交界以西為範圍](../Page/大潭道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019@@2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大潭道.md" title="wikilink">大潭道沿線</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參見

  - [大潭道](../Page/大潭道.md "wikilink")
  - [大潭水塘](../Page/大潭水塘.md "wikilink")
  - [大潭郊野公園](../Page/大潭郊野公園.md "wikilink")
  - [畢拿山](../Page/畢拿山.md "wikilink")
  - [紫羅蘭山](../Page/紫羅蘭山.md "wikilink")
  - [孖崗山](../Page/孖崗山.md "wikilink")
  - [大潭港](../Page/大潭港.md "wikilink")
  - [大潭灣](../Page/大潭灣.md "wikilink")
  - [紅山半島](../Page/紅山半島.md "wikilink")

## 參考資料

<references />

[大潭](../Category/大潭.md "wikilink") [Category:南區
(香港)](../Category/南區_\(香港\).md "wikilink")

1.
2.