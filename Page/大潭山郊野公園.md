**大潭山郊野公園**（）位於[澳門](../Page/澳門.md "wikilink")[氹仔東面](../Page/氹仔.md "wikilink")[大潭山上](../Page/大潭山.md "wikilink")，鄰近[澳門國際機場](../Page/澳門國際機場.md "wikilink")，現時由[市政署管理](../Page/市政署_\(澳門\).md "wikilink")。是[氹仔第一個也是唯一的](../Page/氹仔.md "wikilink")[郊野公園](../Page/郊野公園.md "wikilink")，公園佔面積約為49.31[公頃](../Page/公頃.md "wikilink")，覆蓋範圍包括[雞頸馬路](../Page/雞頸馬路.md "wikilink")、[嘉樂庇總督馬路及](../Page/嘉樂庇總督馬路.md "wikilink")[司徒澤雄神父馬路一帶的山林](../Page/司徒澤雄神父馬路.md "wikilink")，氹仔的最高點（160.4米）亦包括於園內。園內設有各類休憩設施及步行徑，是一個多功能的[郊野公園](../Page/郊野公園.md "wikilink")。

## 沿革

其本為[垃圾堆填區](../Page/垃圾堆填區.md "wikilink")，1988年因堆填達飽和而停止運作。經綠化後，於2001年正式開放予大眾使用。

## 設施

### 步行徑

[Parque_Natural_da_Taipa_Grande_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Parque_Natural_da_Taipa_Grande_Entrance.jpg "fig:Parque_Natural_da_Taipa_Grande_Entrance.jpg")
園內設有三條步行徑，分別為：

  - 自然教育徑：全長1150米，位於[大潭山的山腰部份](../Page/大潭山.md "wikilink")，沿途林蔭密佈，可觀賞到各類[植物](../Page/植物.md "wikilink")、[昆蟲及](../Page/昆蟲.md "wikilink")[雀鳥](../Page/鸟.md "wikilink")。
  - 踏板健身徑：全長1050米，是原有氹仔步行徑的山脊路徑，沿途設有各類健身器材。
  - 生態漫遊徑：全長600米，設有原生[魚類保育區](../Page/鱼.md "wikilink")。

### 其他設施

[Water_Treatment_Plant.jpg](https://zh.wikipedia.org/wiki/File:Water_Treatment_Plant.jpg "fig:Water_Treatment_Plant.jpg")

  - 自來水處理廠
  - 澳門原生魚類保育區
  - 綠蔭長廊
  - 滑草場
  - 燒烤區
  - 兒童遊戲區
  - 中華民族雕塑園（包括展覽館）
  - 暸望台

## 參見

  - [澳門郊野公園](../Page/澳門郊野公園.md "wikilink")
  - [澳門大潭山壹號](../Page/澳門大潭山壹號.md "wikilink")

## 外部連結

[Category:澳門公園](../Category/澳門公園.md "wikilink")
[Category:澳門康樂設施](../Category/澳門康樂設施.md "wikilink")
[Category:澳門郊野公園](../Category/澳門郊野公園.md "wikilink")