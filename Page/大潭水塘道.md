**大潭水塘道**（）是[香港的一條](../Page/香港.md "wikilink")[道路](../Page/道路.md "wikilink")，大部份路段沿山坡上落，部份則建於[水壩上](../Page/水坝.md "wikilink")。大潭水塘道始至[香港島](../Page/香港島.md "wikilink")[黃泥涌峽](../Page/黃泥涌峽.md "wikilink")，[黃泥涌水塘公園之西北](../Page/黃泥涌水塘公園.md "wikilink")、[淺水灣道](../Page/淺水灣道.md "wikilink")、[黃泥涌峽道與](../Page/黃泥涌峽道.md "wikilink")[布力徑之間的交通交匯處](../Page/布力徑.md "wikilink")，該處也是[港島徑第四段的終點](../Page/港島徑.md "wikilink")，同時也是港島徑第五段之起點。大潭水塘道之中段與[大潭道之中段](../Page/大潭道.md "wikilink")，交加於[大潭篤水塘以南](../Page/大潭篤水塘.md "wikilink")、[大潭童軍中心以西](../Page/大潭童軍中心.md "wikilink")。大潭水塘道終於[大潭童軍中心以東](../Page/大潭童軍中心.md "wikilink")、[大潭港以西](../Page/大潭港.md "wikilink")、[紅山半島以北](../Page/紅山半島.md "wikilink")。

此路[陽明山莊以西一段是](../Page/陽明山莊.md "wikilink")[灣仔區和](../Page/灣仔區.md "wikilink")[南區區界的一部分](../Page/南區_\(香港\).md "wikilink")，以東一段則是[東區和](../Page/東區_\(香港\).md "wikilink")[南區區界的一部分](../Page/南區_\(香港\).md "wikilink")，近港島徑和[衞奕信徑的交滙點是灣仔區](../Page/衞奕信徑.md "wikilink")、東區和南區的分界點\[1\]。

## 沿途建築

  - [黃泥涌水塘公園](../Page/黃泥涌水塘公園.md "wikilink")
  - [陽明山莊](../Page/陽明山莊.md "wikilink")
  - [大潭水塘](../Page/大潭水塘.md "wikilink")
  - [大潭童軍中心](../Page/大潭童軍中心.md "wikilink")
  - [香港國際學校](../Page/香港國際學校.md "wikilink")

## 外部連結

  - [大潭水塘道地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=837973&cy=813152&zm=5&mx=837973&my=813152&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

## 參考

<references/>

[Category:黃泥涌峽街道](../Category/黃泥涌峽街道.md "wikilink")
[Category:灣仔區街道](../Category/灣仔區街道.md "wikilink")
[Category:東區街道 (香港)](../Category/東區街道_\(香港\).md "wikilink")
[Category:南區街道 (香港)](../Category/南區街道_\(香港\).md "wikilink")

1.  <http://www.eac.gov.hk/pdf/distco/2011dc/map_dc2011b.pdf>
    2011年區議會選舉選區分界