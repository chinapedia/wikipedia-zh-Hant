{{ Galaxy | | image = | name = 大熊座I矮星系 | epoch =
[J2000](../Page/J2000.md "wikilink") | type = dSph | ra = \[1\] | dec =
\[2\] | dist_ly = 330[kly](../Page/光年.md "wikilink") (100
[kpc](../Page/秒差距.md "wikilink")) | z = | appmag_v = | size_v = |
constellation name = [大熊座](../Page/大熊座.md "wikilink") | notes = | names
= UMa dwarf,\[3\] Ursa Major dSph\[4\] }}

**大熊座I矮星系**（**UMa I
dSph**）是[銀河系的衛星星系](../Page/銀河系.md "wikilink")，分類上屬於[矮橢球星系](../Page/矮橢球星系.md "wikilink")，由[貝絲等人在](../Page/Beth_Willman.md "wikilink")2005年宣布此一發現。

這是一個小的[矮星系](../Page/矮星系.md "wikilink")，測量得到的直徑只有幾千[光年](../Page/光年.md "wikilink")。在2006年，它亮度的絕對星等只有-6.75等，只比[牧夫座矮星系](../Page/牧夫座矮星系.md "wikilink")（絕對星等-5.7等）和[大熊座II矮星系](../Page/大熊座II矮星系.md "wikilink")（絕對星等-3.8等）亮
，是已知星系中倒數第三亮的（扣除[室女座星系團的](../Page/室女座星系團.md "wikilink")[室女座HI21](../Page/室女座HI21.md "wikilink")[暗星系不計](../Page/暗星系.md "wikilink")）。這意味著它比一些亮星，像是[銀河中的](../Page/銀河.md "wikilink")[天津四](../Page/天津四.md "wikilink")（天鵝座α）還要黯淡，大概與[參宿七相當](../Page/參宿七.md "wikilink")。它被描述是相似於[六分儀座矮星系](../Page/六分儀座矮星系.md "wikilink")，這兩個星系都是古老且[缺乏金屬](../Page/金屬量.md "wikilink")。

它距離地球約330,000[光年](../Page/光年.md "wikilink")，大約是[銀河系最大與最亮的衛星星系](../Page/銀河系.md "wikilink")[大麥哲倫雲距離的兩倍](../Page/大麥哲倫雲.md "wikilink")。

在1949年[愛德溫·哈伯發現另一個也曾被稱為](../Page/愛德溫·哈伯.md "wikilink")*大熊座矮星系*的天體，被標示為[帕羅馬
4](../Page/帕羅馬_4.md "wikilink")。由於它奇特的外觀，一度被懷疑是矮球狀星系或是[橢圓星系](../Page/矮橢圓星系.md "wikilink")。不過，最後確認是一個距離非常遙遠（大約360,000光年）的[球狀星團](../Page/球狀星團.md "wikilink")，但仍屬於我們的銀河系。

## 資料來源

  -
  -
  - Willman, Dalcanton, Martinez-Delgado, et al. (2005) "*A New Milky
    Way Dwarf Galaxy in Ursa Major*", submitted to Astrophysical Journal
    Letters, on arXiv.org:
    [astro-ph/0503552](http://arxiv.org/abs/astro-ph/0503552)

## 外部連結

  - [SIMBAD Ursa Major
    Dwarf](http://simbad.u-strasbg.fr/sim-id.pl?protocol=html&Ident=Ursa+Major+Dwarf)

## 相關條目

  - [小熊座矮星系](../Page/小熊座矮星系.md "wikilink")
  - [大熊座II矮星系](../Page/大熊座II矮星系.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:本星系群](../Category/本星系群.md "wikilink")
[Category:大熊座](../Category/大熊座.md "wikilink")

1.

2.
3.
4.