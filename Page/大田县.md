**大田县**，别称**岩城**，是[福建省](../Page/福建省.md "wikilink")[三明市下辖的一个县](../Page/三明市.md "wikilink")。

## 地理

大田县位于福建省中部的[戴云山西侧](../Page/戴云山.md "wikilink")，西邻[永安市](../Page/永安市.md "wikilink")，东邻[泉州市](../Page/泉州市.md "wikilink")[德化县](../Page/德化县.md "wikilink")，南与泉州的[永春](../Page/永春.md "wikilink")、[龙岩的](../Page/龙岩.md "wikilink")[漳平接壤](../Page/漳平.md "wikilink")，北部毗邻三明市区、[沙县和](../Page/沙县.md "wikilink")[尤溪县](../Page/尤溪县.md "wikilink")。

## 历史

大田县的历史可追溯到[明朝](../Page/明朝.md "wikilink")。明朝[嘉靖十四年](../Page/嘉靖.md "wikilink")（1535年），从周边的尤溪、德化、永安、漳平四个县各划出一部分地域组成节爱县，隶属于[延平府](../Page/延平府.md "wikilink")。后改名新民县，四十五年（1566年），改名大田县。清雍正十二年（1734年），永春县升格为[永春州](../Page/永春州.md "wikilink")，大田县改隶永春州。民国元年（1912年），隶属南路道（两年后改名[厦门道](../Page/厦门道.md "wikilink")）。民国十六年，民国政府废除道制，大田直接隶属于福建省。二十二年，发生[福建事变](../Page/福建事变.md "wikilink")，[中华共和国成立](../Page/中华共和国.md "wikilink")，隶属于[兴泉省](../Page/兴泉省.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，隶属于[永安专区](../Page/永安专区.md "wikilink")。1950年9月划归[晋江专区](../Page/晋江专区.md "wikilink")，1963年3月划归[三明专区](../Page/三明专区.md "wikilink")。1983年，三明专区被撤销，改为三明市，大田县仍属之。

## 语言

大田县方言复杂，大部分地区通行三种方言，分别为：[前路话](../Page/前路话.md "wikilink")、[后路话](../Page/后路话.md "wikilink")、闽南话；其中前路话和后路话属[闽南语](../Page/闽南语.md "wikilink")[大田片方言](../Page/大田片.md "wikilink")；当地的“闽南话”与[泉州通行的](../Page/泉州.md "wikilink")[泉漳片](../Page/泉漳片.md "wikilink")[泉州话相近](../Page/泉州话.md "wikilink")，但它们之间仍存在一定差异。大田县的北部和西部也存在一些[客家语的语言岛](../Page/客家语.md "wikilink")。

前路、后路的提法是按照地域位置进行区分，前路、后路以文江龙门为界，在大田的18个乡镇中，属于的前路的有均溪镇、上京镇、石牌镇、华兴乡、屏山乡、吴山乡、济阳乡、武陵乡、谢洋乡、湖美乡、前坪乡、桃源镇、太华镇；属于后路的有广平镇、建设镇、奇韬镇、文江乡、梅山乡。前路中桃源镇、吴山乡、济阳乡、屏山乡、谢洋乡讲闽南话，其他乡镇讲[前路话](../Page/前路话.md "wikilink")（即大田话、前道话）；后路中各乡镇基本讲后路话（即后道话、尤溪县称作新桥话），该方言通行于[尤溪县的](../Page/尤溪县.md "wikilink")[上尤溪](../Page/上尤溪.md "wikilink")、[沙县的部分乡镇](../Page/沙县.md "wikilink")。

此外，桃源镇一带通行的前路话与其他地域的前路话存在一定差异，也有学者将其单独划出，称为[桃源话](../Page/桃源话.md "wikilink")。

由于大田县方言复杂、交流不便，当地政府一直在致力[推广普通话](../Page/推广普通话.md "wikilink")，被评为全国推广普通话先进县。

## 文化

大田县的[大田板灯龙被列入](../Page/大田板灯龙.md "wikilink")[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

婚俗：
高额聘礼是最大的一个特点，在[訂婚时](../Page/訂婚.md "wikilink")，男女双方家属議定好[聘礼的礼金](../Page/聘礼.md "wikilink")，额度一般都是当地家庭年均收入的5-10倍左右。

丧俗：
大田的后路风俗较特别，[扫墓时间並非](../Page/扫墓.md "wikilink")[福建人掃墓的](../Page/福建人.md "wikilink")[清明或](../Page/清明.md "wikilink")[小清明](../Page/小清明.md "wikilink")，另有三種，一是[中元](../Page/中元.md "wikilink")，二是[中秋](../Page/中秋.md "wikilink")，三是[冬至](../Page/冬至.md "wikilink")。一般人都選擇在中元節前（七月初一至七月十五）或中秋節前（八月初一至八月十五）掃墓，如這兩次都錯過，則選在冬至前後十天內扫墓。

## 行政区划

辖8个镇、10个乡：

  - 镇：[均溪镇](../Page/均溪镇.md "wikilink")、[上京镇](../Page/上京镇.md "wikilink")、[广平镇](../Page/广平镇.md "wikilink")、[桃源镇](../Page/桃源镇_\(大田县\).md "wikilink")、[太华镇](../Page/太华镇.md "wikilink")、[建设镇](../Page/建设镇.md "wikilink")、[石牌镇](../Page/石牌镇.md "wikilink")、[奇韬镇](../Page/奇韬镇.md "wikilink")。
  - 乡：[华兴乡](../Page/华兴乡.md "wikilink")、[屏山乡](../Page/屏山乡.md "wikilink")、[吴山乡](../Page/吴山乡.md "wikilink")、[济阳乡](../Page/济阳乡.md "wikilink")、[武陵乡](../Page/武陵乡（大田县）.md "wikilink")、[谢洋乡](../Page/谢洋乡.md "wikilink")、[文江乡](../Page/文江乡.md "wikilink")、[梅山乡](../Page/梅山乡.md "wikilink")、[湖美乡](../Page/湖美乡.md "wikilink")、[前坪乡](../Page/前坪乡.md "wikilink")。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[大田县](../Category/大田县.md "wikilink")
[县](../Category/三明区县市.md "wikilink")
[三明](../Category/福建省县份.md "wikilink")