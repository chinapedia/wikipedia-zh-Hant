**大發Move**是[日本](../Page/日本.md "wikilink")[大發汽車公司使用同廠Cuore底盤建造的](../Page/大發汽車.md "wikilink")[輕型車](../Page/輕型車.md "wikilink")，其子型號包括Gran
Move、Move Latte及Move
Custom。近期也推出一款[混合動力版本的Move](../Page/混合動力.md "wikilink")，建基於Pyzar，使用[汽油和](../Page/汽油.md "wikilink")[電力作動力](../Page/電力.md "wikilink")，現已開始投產。

## 內部連結

  - [速霸陸Stella](../Page/速霸陸Stella.md "wikilink")：兄弟車

## 外部連結

  - [Move官方網站](https://web.archive.org/web/20080226161402/http://www.daihatsu.co.jp/showroom/lineup/move/)
  - [Move
    Custom官方網站](https://web.archive.org/web/20080309085647/http://www.daihatsu.co.jp/showroom/lineup/move_custom/)
  - [Move
    Latte官方網站](https://web.archive.org/web/20080321005056/http://www.daihatsu.co.jp/showroom/lineup/move_latte/)

## 參考資料

[Category:大發車輛](../Category/大發車輛.md "wikilink")
[Category:前置引擎](../Category/前置引擎.md "wikilink")
[Category:前輪驅動](../Category/前輪驅動.md "wikilink")
[Category:四輪驅動](../Category/四輪驅動.md "wikilink")
[Category:掀背車](../Category/掀背車.md "wikilink")
[Category:輕型車](../Category/輕型車.md "wikilink")
[Category:CVT變速系統車輛](../Category/CVT變速系統車輛.md "wikilink")