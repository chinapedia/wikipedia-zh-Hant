**大白菜**（学名：*Brassica rapa pekinensis*，异名*Brassica campestris
pekinensis*或*Brassica
pekinensis*）是一种原产于[亞洲的](../Page/亞洲.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")，古稱「菘」，又稱“結球白菜”、“包心白菜”、“黃芽白”、“膠菜”等，在[粤语裡叫](../Page/粤语.md "wikilink")“紹菜”。

大白菜與[小白菜是近親](../Page/小白菜.md "wikilink")，同属[蕓薹一](../Page/蕓薹.md "wikilink")[種](../Page/種_\(生物\).md "wikilink")，和原产[地中海沿岸的](../Page/地中海.md "wikilink")[圓白菜也較近](../Page/結球甘藍.md "wikilink")，同屬[十字花科](../Page/十字花科.md "wikilink")[蕓薹屬](../Page/蕓薹屬.md "wikilink")。

## 形态

二年生[草本](../Page/草本.md "wikilink")。叶生于短缩茎上，叶片薄而大，分为外叶和内叶，椭圆或长圆形，浓绿或淡绿色，心叶为白、绿白或淡黄色，叶柄宽扁，两侧有明显的叶翼；扁圆形到长筒形的叶球；总状花序，黄色花。长角果，内生褐色的种子数十粒；种子近圆形，红褐色或黄褐色。

大白菜有宽大的菜叶和白色菜帮。多重菜叶紧紧包裹在一起形成圆柱体，多数会形成一个密实的头部。被包在里面的菜叶由于见不到阳光绿色较淡以至呈淡[黄色](../Page/黄色.md "wikilink")。按叶球的包合情况，分结球、半结球、花心和散叶等类型。

## 历史

[1963-10_1963年_白菜种植.jpg](https://zh.wikipedia.org/wiki/File:1963-10_1963年_白菜种植.jpg "fig:1963-10_1963年_白菜种植.jpg")
白菜原产[中国](../Page/中国.md "wikilink")。在[西安](../Page/西安.md "wikilink")[新石器时代](../Page/新石器时代.md "wikilink")[半坡遗址中出土的一个陶罐裡有白菜籽](../Page/半坡遗址.md "wikilink")，有6000多年的历史，比除[稻谷外其他原产中国的粮食作物要古远](../Page/稻谷.md "wikilink")。白菜古时称“菘”\[1\]，[春秋战国已有大量栽培](../Page/春秋战国.md "wikilink")，最早得名于[汉代](../Page/汉代.md "wikilink")。[南北朝时是中国南方最常食用的蔬菜之一](../Page/南北朝.md "wikilink")。[唐代出现了白菘](../Page/唐代.md "wikilink")、紫菘和牛肚菘等不同的品种。[宋代](../Page/宋代.md "wikilink")[陆佃的](../Page/陆佃.md "wikilink")《[埤雅](../Page/埤雅.md "wikilink")》中说：“菘性凌[冬不凋](../Page/冬.md "wikilink")，四时常见，有[松之操](../Page/松.md "wikilink")，故其字会意，而本草以为耐[霜](../Page/霜.md "wikilink")[雪也](../Page/雪.md "wikilink")”。[寇宗奭](../Page/寇宗奭.md "wikilink")《本草衍義》說：“菘菜，其味微苦，葉嫩。”。[蘇東坡在黃州煮魚是將鮮](../Page/蘇東坡.md "wikilink")[鯽魚或](../Page/鯽魚.md "wikilink")[鯉魚收拾好](../Page/鯉魚.md "wikilink")，未開火，先下魚，放鹽，然後“以菘菜心芼之”\[2\]。[元朝时民间开始称其为](../Page/元朝.md "wikilink")“白菜”。[明朝](../Page/明朝.md "wikilink")[中医学家](../Page/中医学.md "wikilink")[李时珍在](../Page/李时珍.md "wikilink")《[本草纲目](../Page/本草纲目.md "wikilink")》中记载：“菘性凌冬晚凋，四时常见，有松之操，故曰菘。今俗之白菜，其色清白。”《本草纲目》记载了一些白菜的[药用价值](../Page/中药.md "wikilink")。

明代以前白菜主要在[长江下游](../Page/长江.md "wikilink")[太湖地区栽培](../Page/太湖.md "wikilink")，明清时期不结球白菜（小白菜）在北方得到了迅速的发展。与此同时在[浙江地区培育成功结球白菜](../Page/浙江.md "wikilink")（大白菜）。18世纪中叶（[康乾盛世](../Page/康乾盛世.md "wikilink")）在北方，大白菜取代了小白菜，且产量超过南方。[华北](../Page/华北.md "wikilink")、[山东出产的大白菜开始沿](../Page/山东.md "wikilink")[京杭大运河销往](../Page/京杭大运河.md "wikilink")[江](../Page/江苏.md "wikilink")[浙以至](../Page/浙江.md "wikilink")[华南](../Page/华南.md "wikilink")。[鲁迅在](../Page/鲁迅.md "wikilink")《[朝花夕拾](../Page/朝花夕拾.md "wikilink")》的《[藤野先生](../Page/藤野先生.md "wikilink")》一文中说：“大概是物以希（稀）为贵罢。北京的白菜运往浙江，便用红头绳系住菜根，倒挂在水果店头，尊为‘胶菜’”。

大白菜是在[明朝时由中国传到](../Page/明朝.md "wikilink")[朝鮮王朝的](../Page/朝鮮王朝.md "wikilink")，之后成了[朝鲜泡菜的主要原料](../Page/朝鲜泡菜.md "wikilink")。[韩国电视剧](../Page/韩国.md "wikilink")《[大長今](../Page/大長今_\(電視劇\).md "wikilink")》中有主人公试种从明国引进的菘菜（大白菜）的情节。

日本初见大白菜是1875年明治8年的东京博览会清政府的展台，明治政府花钱买回了3株展品，在爱知植试种2颗，东京试种1颗，但全都结不了菜球以失败告终。20世纪初，[日俄战争期间](../Page/日俄战争.md "wikilink")，有些[日本士兵在](../Page/日本.md "wikilink")[中国东北尝到这种菜觉得味道不错](../Page/中国东北.md "wikilink")，于是把它的白菜种子带到了日本。最終到了仙台宫城农学校的老师沼仓吉兵卫手上，花了20年，往返中日歷經挫折終於在仙台培育出白菜。目前在日本市场上出售的[食品工厂生产的](../Page/食品.md "wikilink")[饺子](../Page/饺子.md "wikilink")，基本都是[猪肉](../Page/猪.md "wikilink")[白菜馅的](../Page/白菜.md "wikilink")。

今天，世界各地许多国家都引种了白菜。

## 食用

[1968-04_1967年_北京郊区的白菜市场.jpg](https://zh.wikipedia.org/wiki/File:1968-04_1967年_北京郊区的白菜市场.jpg "fig:1968-04_1967年_北京郊区的白菜市场.jpg")
大白菜耐储存，所以在中国，特别是北方的老百姓对白菜有特殊的感情。在经济困难的时期，大白菜是他们在整个冬季唯一可吃的蔬菜，一户人家往往需要储存数百斤白菜以应付过冬，因此白菜在中国演变出了炖、炒、腌、拌各种烧法。[冬季在最低气温为](../Page/冬季.md "wikilink")-5℃左右时，大白菜完全可以在室外堆储安全过冬，外部[叶子干燥后可以为内部保温](../Page/叶.md "wikilink")。如果[温度再低](../Page/温度.md "wikilink")，则需要窖藏。不过在过于寒冷的北方还有另外几种冬季储存白菜的方法，如在[朝鲜北方和中国东北东部腌制](../Page/朝鲜.md "wikilink")[朝鲜冬菜](../Page/朝鲜冬菜.md "wikilink")，在中国东北西部、[内蒙东部和](../Page/内蒙.md "wikilink")[河北北部寒冷以前又缺乏食](../Page/河北.md "wikilink")[盐的地区习惯用渍](../Page/盐.md "wikilink")[酸菜的方法等储存白菜](../Page/酸菜.md "wikilink")，韓國最出名的泡菜也是以大白菜為主。

白菜含有豐富的[維生素A](../Page/維生素.md "wikilink")、C、[鈣](../Page/鈣.md "wikilink")、[鎂等](../Page/鎂.md "wikilink")，多吃白菜對女性會有很好的護膚及養顏效果，白菜中含有鉬，可以抑制人體內[亞硝酸胺的產生](../Page/亞硝酸胺.md "wikilink")，對於乳腺癌有預防作用。

由于大白菜是在[秋季](../Page/秋季.md "wikilink")[玉米收获后播种](../Page/玉米.md "wikilink")，初冬收获，产量大，管理容易，但储存需要占地，所以收获期间同时上市价格非常便宜。一些商家在促销商品时常用“某某商品白菜价”的口号宣传其廉价。
煮大白菜時適量添加醋，除了可提味，還能使大白菜所含的鈣、[磷](../Page/磷.md "wikilink")、[鐵等營養素分解](../Page/鐵.md "wikilink")，更有利於人體吸收鈣等養分。

## 品种

大白菜品种繁多，基本有散叶型、花心型、结球型和半结球型几类，主要品种有：

  - 以[天津为代表的](../Page/天津.md "wikilink")[大运河沿岸有三](../Page/大运河.md "wikilink")、四百年种植历史的**青麻叶**（**天津绿**），绿色菜叶较多，帮薄，纤维少，叶内柔嫩。《[静海县志](../Page/静海县.md "wikilink")》中曾写道“昔[周颙](../Page/周颙.md "wikilink")（[南朝齊人](../Page/南朝齊.md "wikilink")）称乡味之美，春初早[韭](../Page/韭菜.md "wikilink")，秋末晚菘是也，味美而食久，运河沿岸产者最良。”
  - 黄色菜叶为主的品种又称**黄芽白菜**、**黄芽菜**、**黄芽白**，有南北两种。黄芽菜[清朝](../Page/清朝.md "wikilink")[光绪二十四年](../Page/光绪.md "wikilink")（1898年）《[津门纪略](../Page/津门纪略.md "wikilink")》中记有“黄芽白菜，胜于江南冬笋者，以其百吃不厌也”，以至其又有“北笋”之称。
  - 在[台湾种植的](../Page/台湾.md "wikilink")**台湾白菜**也是大白菜的一种，比北京大白菜细一些。
  - 在[台湾](../Page/台湾.md "wikilink")，大白菜具有調節夏季蔬菜短缺的功用。

[BrassicaPekinensis2.jpg](https://zh.wikipedia.org/wiki/File:BrassicaPekinensis2.jpg "fig:BrassicaPekinensis2.jpg")

  - [娃娃菜](../Page/娃娃菜.md "wikilink")，[雲南原產的小號品種](../Page/雲南.md "wikilink")，於山地或高原上哉種，比一般黃芽白小得多，但仍然比[小白菜大一些](../Page/小白菜.md "wikilink")。而有些商店或菜販會把外層葉殘舊的普通黃芽白，削去殘葉當作娃娃葉發售。
  - 紹菜，有時專指比普通黃芽白更長的品種。

## 诗歌、谚语

  - “春初早韭，秋末晚菘”六朝古都金陵，《南齐书》载周颙于锺山西立隐舍，清贫寡欲，终日长蔬食，卫将军王俭问他“山中何所食？”顒答曰：“赤米白盐，绿葵紫蓼。”文惠太子问：“菜食何味最胜？”顒曰：“春初早韭，秋末晚菘。”
  - [唐朝诗人](../Page/唐朝.md "wikilink")[韩愈](../Page/韩愈.md "wikilink")：“早菘细切肥牛肚”
  - [宋朝词人](../Page/宋朝.md "wikilink")[苏东坡](../Page/苏东坡.md "wikilink")：“白菘似羔豚，冒土出熊蟠”
  - 《津门竹枝词》：“芽韭交春色半黄，锦衣桥畔价偏昂，三冬利赖资何物，白菜甘菘是窖藏”
  - 关于白菜的民间[谚语有](../Page/谚语.md "wikilink")：
      - 方便食用：“白菜可做百样菜”、“种一季吃半年，从冬可以吃到春”、“萝卜白菜，各有所爱”
      - 便宜：“大头白菜论斤卖，一二文钱价不昂”
      - 种植方法：“头伏[萝卜二伏](../Page/萝卜.md "wikilink")[芥](../Page/芥菜.md "wikilink")，三伏里头种白菜”、“白菜密插，芥菜跑马”、“淹不死的白菜，旱不死的[葱](../Page/葱.md "wikilink")”、“萝卜白菜葱，多用大粪攻”、“[霜降摘](../Page/霜降.md "wikilink")[柿子](../Page/柿子.md "wikilink")，[小雪砍白菜](../Page/小雪.md "wikilink")”。
      - 营养价值：“白菜萝卜汤，益寿保健康”、“白菜是个宝，赛过[灵芝草](../Page/灵芝.md "wikilink")”、“[鱼生火](../Page/鱼.md "wikilink")，肉生痰，白菜[豆腐保平安](../Page/豆腐.md "wikilink")”、“[立冬白菜赛](../Page/立冬.md "wikilink")[羊肉](../Page/羊肉.md "wikilink")”
  - 香港歌手[林子祥的](../Page/林子祥.md "wikilink")《分分鐘需要你》一句「[鹹魚白菜也好好味](../Page/鹹魚.md "wikilink")」是貧窮生活的鮮活反映

## 注釋

## 參考文獻

  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，薛聰賢出版社，2001年，ISDN:957-97452-1-8

## 参看

  - [甘蓝](../Page/甘蓝.md "wikilink")（中国椰菜）
  - [朝鲜泡菜](../Page/朝鲜泡菜.md "wikilink")

[Category:蕓薹](../Category/蕓薹.md "wikilink")
[Category:中国食品](../Category/中国食品.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")

1.  宋代《日華子諸家本草》說：“梗長葉瘦高者為菘，葉闊厚短肥而梗細者為蕪菁菜也。”
2.  《蘇軾文集》卷七十三《雜記·草木飲食·煮魚法》