**大目降社**（荷文音譯：；[閩南語音譯](../Page/閩南語音譯.md "wikilink")：）為[台灣](../Page/台灣.md "wikilink")17世紀中葉仍有之[平埔族群](../Page/平埔族.md "wikilink")[西拉雅族群社](../Page/西拉雅族.md "wikilink")，該社合併自[新港社旁的Teopan](../Page/新港社.md "wikilink")、Tatenpoan及Tibolegan三小社。其位置約在今[台南市](../Page/臺南市.md "wikilink")[新化區](../Page/新化區.md "wikilink")，人數則在369人左右。在[荷治時期](../Page/臺灣荷治時期.md "wikilink")，約在1659年12月左右時基於宗教與政治因素，荷蘭東印度公司已將大目降社居民遷到有一名牧師的[新港以接受更好的基督教教育](../Page/新港社.md "wikilink")，而大目降社的耕地則被荷蘭東印度公司以1500里耳賣給了漢人耕種，顏愛靜、陳立人〈荷據時期臺灣赤崁一帶原住民土地利用與地權型態變遷之研究〉推論「大目降社」在明鄭時期變成「大目降民社」，即大目降社土地已全數自原住民手中流失的現象為此一買賣造成的結果。

[清朝](../Page/台灣清治時期.md "wikilink")，因漢人移民屯居或本身漢化影響，大目降社已日漸沒落且鮮少文獻記載，不過，[台灣日治時期各項數據顯示](../Page/台灣日治時期.md "wikilink")，原大目降社所在地區仍有近千人的「[熟番](../Page/熟番.md "wikilink")」，惟疑因被歸類成新港社的一部份，而失去自己的社名稱呼。

## 註釋

## 參考資料

## 參考文獻

  - 《台灣歷史辭典》
  - [台南縣鄉土教學](https://web.archive.org/web/20120203172337/http://ltrc.tnc.edu.tw/modules/tadbook2/view.php?book_sn=1&bdsn=83)

[Category:西拉雅族](../Category/西拉雅族.md "wikilink")
[Category:台南市歷史](../Category/台南市歷史.md "wikilink")
[Category:源自閩南語的台灣地名](../Category/源自閩南語的台灣地名.md "wikilink")