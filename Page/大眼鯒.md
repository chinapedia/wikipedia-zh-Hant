**大眼鯒**（[学名](../Page/学名.md "wikilink")：），又稱**大眼牛尾魚**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目](../Page/鮋形目.md "wikilink")[牛尾魚科的其中一](../Page/牛尾魚科.md "wikilink")[種](../Page/種.md "wikilink")。分布于[朝鲜](../Page/朝鲜.md "wikilink")、日本南部以及中国[东海](../Page/东海.md "wikilink")、[台湾海峡等海域](../Page/台湾海峡.md "wikilink")。该物种的模式产地在長崎、Yedo。\[1\]

## 深度

水深5\~100公尺。

## 特徵

外觀大略與[大棘大眼鯒同](../Page/大棘大眼鯒.md "wikilink")，但其前鰓蓋棘中最長之棘短於眼徑，且其側線前部僅具8\~11棘。體呈灰色，頭部和體側散布著黑斑。背鰭硬棘部散部黑色的雲狀斑，尾鰭有數條不明知橫斑。體長可達25公分。

## 生態

本魚為底棲魚類，活動力差，不善游泳，通常將自己埋於沙中，露出兩眼，以捕食魚類及[甲殼類](../Page/甲殼類.md "wikilink")。

## 經濟利用

食用魚，適合用油煎。

## 参考文献

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[meerdervoorti](../Category/大眼鲬属.md "wikilink")

1.