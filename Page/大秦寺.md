[Da_Qin_Pagoda.jpg](https://zh.wikipedia.org/wiki/File:Da_Qin_Pagoda.jpg "fig:Da_Qin_Pagoda.jpg")

**大秦寺**是在[中国的](../Page/中国.md "wikilink")[景教寺院](../Page/景教.md "wikilink")（[教堂](../Page/教堂.md "wikilink")）的通称。景教即传到中国的[基督教](../Page/基督教.md "wikilink")[聂斯脱留派](../Page/聂斯脱留派.md "wikilink")（[景教](../Page/景教.md "wikilink")）。[唐朝时](../Page/唐朝.md "wikilink")，在[长安的大秦寺頗負盛名](../Page/长安.md "wikilink")。

## 歷史沿革

景教在中国的传播，从[唐代](../Page/唐代.md "wikilink")[贞观九年](../Page/贞观.md "wikilink")（635年）聂斯脱留派传教士团到达长安开始。那时，[唐太宗命宰相](../Page/唐太宗.md "wikilink")[房玄龄到长安西郊迎接传教团的团长](../Page/房玄龄.md "wikilink")[阿罗本](../Page/阿罗本.md "wikilink")（罗宾，Alopen），并亲自会见了阿罗本。[大秦景教流行中国碑记载](../Page/大秦景教流行中国碑.md "wikilink")：“太宗文皇帝光华启运，明圣临人，大秦国有上德曰阿罗本，占青云而载真经，望风律以驰艰险，贞观九祀，至于长安，帝使宰臣房公玄龄，总使西效入内，翻经书殿，问道禁闱。”

三年后的唐贞观十二年（638年），景教为唐朝所认可，并由唐朝政府资助在[长安义宁坊修建寺院](../Page/长安.md "wikilink")（教堂）。当时被称为“波斯寺”或“波斯经寺”，并没有使用大秦寺的名字；后因为[天宝四年](../Page/天宝.md "wikilink")（745年）[宗教的名称由波斯经教](../Page/宗教.md "wikilink")、波斯教变为了大秦景教，寺院的通称由波斯寺改名为大秦寺。（参见《[旧唐书](../Page/旧唐书.md "wikilink")》中大秦寺的记载）。唐朝人把景教的寺院和[摩尼教](../Page/摩尼教.md "wikilink")、[祆教的寺院统称为](../Page/祆教.md "wikilink")[三夷寺](../Page/三夷寺.md "wikilink")。

大秦寺的名称在正式文件中的使用要在唐天宝四年更名之后，但是，也有人认为745年前传抄的大秦景教《[宣元至本经](../Page/宣元至本经.md "wikilink")》也使用了大秦寺的名称，或许是在正式改名之前，存在大秦寺的名称在非正式场合使用的可能性。

唐代有多处大秦寺：[长安义宁坊大秦寺](../Page/长安.md "wikilink")，[洛阳修养坊大秦寺](../Page/洛阳.md "wikilink")，灵武大秦寺，五郡大秦寺，[盩厔大秦寺](../Page/盩厔.md "wikilink")，[四川](../Page/四川.md "wikilink")[成都西门外大秦寺等](../Page/成都.md "wikilink")。

唐[会昌五年](../Page/会昌.md "wikilink")（845年），景教与[佛教](../Page/佛教.md "wikilink")、[祆教一起遭到禁绝](../Page/祆教.md "wikilink")。随着景教在中国的衰落，中国的大秦寺也遭受了严酷的命运，不过在中国的边疆地区，聂斯脱里派信仰在[克烈部](../Page/克烈部.md "wikilink")、[乃蛮部](../Page/乃蛮部.md "wikilink")、[汪古部](../Page/汪古部.md "wikilink")、[畏兀儿部等](../Page/维吾尔族.md "wikilink")[蒙古高原和](../Page/蒙古高原.md "wikilink")[中亚的部落间继续存在着](../Page/中亚.md "wikilink")，这些信仰在[元代由于朝廷的支持而又一次活跃起来](../Page/元朝.md "wikilink")，并在[华南的港口城市](../Page/华南.md "wikilink")，如[泉州](../Page/泉州.md "wikilink")，兴建教堂。但此时教派的名称与唐朝时的名称不同，在元代聂斯脱留派称为**也里可温教**（也里可温，即[福音](../Page/福音.md "wikilink")
evangelion 的音译）。

元代的景教寺称为“**[十字寺](../Page/十字寺.md "wikilink")**”，据《[元史](../Page/元史.md "wikilink")》记载，有72所，分布在[吐鲁番](../Page/吐鲁番.md "wikilink")、[哈密](../Page/哈密.md "wikilink")、[伊犁](../Page/伊犁.md "wikilink")、[沙州](../Page/沙州.md "wikilink")、[兰州](../Page/兰州.md "wikilink")、[宁夏](../Page/宁夏.md "wikilink")、[太原](../Page/太原.md "wikilink")、[济南](../Page/济南.md "wikilink")、[扬州](../Page/扬州.md "wikilink")、[泉州](../Page/泉州.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[福州](../Page/福州.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[房山](../Page/房山.md "wikilink")、[广州](../Page/广州.md "wikilink")、[昆明等地](../Page/昆明.md "wikilink")。

由于[元朝的灭亡](../Page/元.md "wikilink")，且[中亞逐漸信奉](../Page/中亞.md "wikilink")[回教](../Page/回教.md "wikilink")，[蒙古人也多半](../Page/蒙古人.md "wikilink")[皈依了](../Page/皈依.md "wikilink")[藏密](../Page/藏密.md "wikilink")[佛法](../Page/佛法.md "wikilink")，兩教的蠶食之下，景教信仰衰落了，直到[明代](../Page/明朝.md "wikilink")[天启五年](../Page/天启.md "wikilink")（1625年）[大秦景教流行中国碑在](../Page/大秦景教流行中国碑.md "wikilink")[西安市郊被发现之前](../Page/西安.md "wikilink")，景教幾乎可說是被中国人完全忘却了。

## 参见

  - [大秦寺塔](../Page/大秦寺塔.md "wikilink")
  - [大秦景教流行中国碑](../Page/大秦景教流行中国碑.md "wikilink")

## 参考文献

  - [朱谦之](../Page/朱谦之.md "wikilink") 《中国景教》
    [人民出版社](../Page/人民出版社.md "wikilink") 1998 ISBN
    7-01-002626-2
  - 佐伯好郎《景教之研究》 东方文化学院 东京研究所 1935年
  - 西脇常記《关于〈[大秦景教宣元至本经](../Page/大秦景教宣元至本经.md "wikilink")〉残卷》，《禅文化研究紀要》
    15号 1988年。
  - Ian Gillman and [Hans-Joachim
    Klimkeit](../Page/Hans-Joachim_Klimkeit.md "wikilink"). Christians
    in Asia before 1500.Published in the united states of America by
    University of Michigan Press 1999.

{{-}}

[Category:唐朝宗教建筑物](../Category/唐朝宗教建筑物.md "wikilink")
[Category:中国景教教堂](../Category/中国景教教堂.md "wikilink")