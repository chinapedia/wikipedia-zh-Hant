是[香港的](../Page/香港.md "wikilink")[公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[葵青區](../Page/葵青區.md "wikilink")[中葵涌](../Page/中葵涌.md "wikilink")[大窩口](../Page/大窩口.md "wikilink")，於1961年入伙，是新界第一個[徙置區](../Page/徙置區.md "wikilink")，也是在所有葵青區的公共屋邨之中，是最接近[荃灣區的屋邨](../Page/荃灣區.md "wikilink")，於1970年代開始重建，新建樓宇於分別於1979年至1993年入伙，現由[香港房屋委員會負責管理屋邨](../Page/香港房屋委員會.md "wikilink")。

是[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，屬於大窩口邨重建第五期項目，項目還包括[大窩口體育館](../Page/大窩口體育館.md "wikilink")\[1\]，共有兩座樓宇，在1993年落成。

原是居者有其屋屋苑，位於[石頭街](../Page/石頭街.md "wikilink")，原址為第20座徙置大廈，共有兩座樓宇，屬於大窩口邨重建第六期項目，後來因為政府決定停售居屋，於2004年改為[紀律部隊](../Page/紀律部隊.md "wikilink")[宿舍](../Page/宿舍.md "wikilink")，其中第二座率先在2005年6月交付警務處宿舍組入伙。

## 歷史

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，大量[中國大陸難民移港定居](../Page/中國大陸.md "wikilink")，由於當時[香港政府沒有一個房屋政策來為低下階層而設的](../Page/香港政府.md "wikilink")，所以很多難民在1949年至1953年間在山邊興建木屋來居住，但這些木屋設備簡陋，衛生環境惡劣，加上木屋的密度高，所以經常發生火警，最終於1953年的聖誕夜，[石硤尾木屋區發生了史無前例的大火](../Page/石硤尾.md "wikilink")，令5萬多人無家可歸，於是政府在大窩口一帶興建[徙置大廈](../Page/徙置大廈.md "wikilink")，用以安置災民。

除了大窩口外，還在[大坑東](../Page/大坑東.md "wikilink")、石硤尾和[李鄭屋興建徙置大廈](../Page/李鄭屋.md "wikilink")，除了石硤尾美荷樓及[石蘺中轉房屋外](../Page/石蘺中轉房屋.md "wikilink")，所有徙置大廈已拆卸進行重建。

## 屋邨狀況

直至1970年代末，大窩口邨開始進行重建工程，與[石硤尾邨和](../Page/石硤尾邨.md "wikilink")[大坑東邨不同的是](../Page/大坑東邨.md "wikilink")，[香港房屋委員會把大窩口邨所有徙置大廈拆卸重建](../Page/香港房屋委員會.md "wikilink")，新樓宇於1979年至1993年入伙，而其中的富秀樓及富平樓採用了相連長型樓宇類型中較少使用的「L」款的樓宇，相連長型L款樓宇全港公共屋邨中只有四座，另外2座是[橫頭磡邨的宏光樓及宏顯樓](../Page/橫頭磡邨.md "wikilink")。

大窩口邨附近設有商場，加上設有行人天橋往[大窩口站](../Page/大窩口站.md "wikilink")，同有（逾30條）巴士路線途經[葵涌](../Page/葵涌.md "wikilink")[青山公路](../Page/青山公路.md "wikilink")（兩旁）及[紅、綠小巴等往返港九新界各區](../Page/香港小巴.md "wikilink")，交通極為方便。由於大窩口位處山谷位置，經[葵涌邨可到達](../Page/葵涌邨.md "wikilink")[葵興](../Page/葵興.md "wikilink")、[葵盛東邨及](../Page/葵盛東邨.md "wikilink")[葵芳](../Page/葵芳.md "wikilink")、[荃灣市中心及](../Page/荃灣市中心.md "wikilink")[石圍角邨](../Page/石圍角邨.md "wikilink")、[城門谷一帶亦可步行前往](../Page/城門谷.md "wikilink")。

2004年，政府向房委會購入大窩口邨重建第六期的居者有其屋「葵蓉苑」，作為紀律部隊宿舍，以應付紀律部隊人員的宿舍需要及解決居屋空置問題，其中二座由[警務處宿舍組支配](../Page/警務處.md "wikilink")。\[2\]\[3\]\[4\]

## 屋邨資料

### 重建後

| 屋邨/屋苑 | 樓宇名稱                                       | 樓宇類型                                | 落成年份\[5\] |
| ----- | ------------------------------------------ | ----------------------------------- | --------- |
| 大窩口邨  | 富貴樓                                        | [舊長型](../Page/舊長型.md "wikilink")    | 1979      |
| 富國樓   |                                            |                                     |           |
| 富強樓   | 1984                                       |                                     |           |
| 富安樓   | [單座工字型](../Page/單座工字型.md "wikilink")（特別設計） | 1980                                |           |
| 富榮樓   | [雙工字型](../Page/雙工字型.md "wikilink")         |                                     |           |
| 富華樓   |                                            |                                     |           |
| 富邦樓   | 1984                                       |                                     |           |
| 富民樓   |                                            |                                     |           |
| 富秀樓   | [相連長型L款](../Page/相連長型大廈.md "wikilink")     | 1988                                |           |
| 富平樓   |                                            |                                     |           |
| 富逸樓   | 相連長型第一款                                    |                                     |           |
| 富雅樓   |                                            |                                     |           |
| 富靜樓   | 1989                                       |                                     |           |
| 富碧樓   |                                            |                                     |           |
| 富泰樓   | [和諧一型](../Page/和諧一型.md "wikilink")         | 1993                                |           |
| 富德樓   |                                            |                                     |           |
| 富賢樓   | 和諧一型連和諧附翼三型                                |                                     |           |
| 葵賢苑   | 葵義閣                                        | [新十字型](../Page/新十字型.md "wikilink")  |           |
| 葵謙閣   |                                            |                                     |           |
| 葵蓉苑   | 第1座                                        | [康和一型](../Page/康和型大廈.md "wikilink") | 2004      |
| 第2座   |                                            |                                     |           |

### 重建前

[Tai_Wo_Hau_Estate_Harmony_Block_2008.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Harmony_Block_2008.jpg "fig:Tai_Wo_Hau_Estate_Harmony_Block_2008.jpg")
[Tai_Wo_Hau_Shopping_Centre.JPG](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Shopping_Centre.JPG "fig:Tai_Wo_Hau_Shopping_Centre.JPG")
[Tsuen_Wan_The_Fresh_One_Market.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_The_Fresh_One_Market.jpg "fig:Tsuen_Wan_The_Fresh_One_Market.jpg")
[Tai_Wo_Hau_Estate_Pond.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Pond.jpg "fig:Tai_Wo_Hau_Estate_Pond.jpg")
[Tai_Wo_Hau_Estate_Jogging_Path.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Jogging_Path.jpg "fig:Tai_Wo_Hau_Estate_Jogging_Path.jpg")
[Tai_Wo_Hau_Estate_Pavilion.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Pavilion.jpg "fig:Tai_Wo_Hau_Estate_Pavilion.jpg")
[Tai_Wo_Hau_Estate_Football_Field_and_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Football_Field_and_Gym_Zone.jpg "fig:Tai_Wo_Hau_Estate_Football_Field_and_Gym_Zone.jpg")
[Tai_Wo_Hau_Estate_Playground.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Playground.jpg "fig:Tai_Wo_Hau_Estate_Playground.jpg")
[Tai_Wo_Hau_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Estate_Car_Park.jpg "fig:Tai_Wo_Hau_Estate_Car_Park.jpg")

<div class="NavFrame collapsed">

<div class="NavHead">

已清拆樓宇

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>樓宇名稱</p></th>
<th><p>樓宇類型</p></th>
<th><p>落成年份</p></th>
<th><p>拆卸年份</p></th>
<th><p>重建後原地所屬的樓宇</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1座</p></td>
<td><p><a href="../Page/第二型.md" title="wikilink">第二型</a></p></td>
<td><p>1961</p></td>
<td><p>1977</p></td>
<td><p>富強樓</p></td>
</tr>
<tr class="even">
<td><p>第2座</p></td>
<td><p>富邦樓<br />
富民樓<br />
大窩口商場</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4座</p></td>
<td><p>1982</p></td>
<td><p>富秀樓<br />
富碧樓</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6座</p></td>
<td><p><a href="../Page/大窩口體育館.md" title="wikilink">大窩口體育館</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7座</p></td>
<td><p>葵賢苑</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8座</p></td>
<td><p>富雅樓<br />
富平樓<br />
富靜樓<br />
富逸樓</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第11座</p></td>
<td><p>1977</p></td>
<td><p>富安樓</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12座</p></td>
<td><p>1975</p></td>
<td><p>富榮樓<br />
富華樓</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第13座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14座</p></td>
<td><p>1989</p></td>
<td><p>富泰樓<br />
富德樓<br />
富賢樓<br />
大窩口第二商場</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第15座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第17座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第18座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第20座（富裕樓）</p></td>
<td><p><a href="../Page/第四型.md" title="wikilink">第四型</a></p></td>
<td><p>1968</p></td>
<td><p>2000</p></td>
<td><p>葵蓉苑</p></td>
</tr>
</tbody>
</table>

</div>

</div>

### 商場、街市及康樂設施

#### 大窩口商場及住宅地舖

Tai Wo Hau Shopping Centre Floor 2.jpg|第一商場2樓 Tai Wo Hau Shopping Centre
Floor 3.jpg|第一商場3樓酒家及[惠康超級市場](../Page/惠康超級市場.md "wikilink") Tai Wo Hau
Shopping Centre Floor 3 (2).jpg|第一商場3樓食肆 Fu Pong House Shops, Covered
Walkway and Tai Wo Hau Shopping Centre Floor 3
(3).jpg|第一商場3樓[日本城](../Page/日本城.md "wikilink")（右）、有蓋行人通道（中）及富邦樓商店（左）
Fu Pik House Shop.jpg|富碧樓[大快活](../Page/大快活.md "wikilink") Fu Man House
Shops.jpg|富民樓地舖

#### 鮮薈市場·荃灣

2017年10月\[6\]，大窩口街市開始翻新並加裝空調，於2018年1月16日重開，易名為「鮮薈市場·荃灣」。

Tsuen Wan The Fresh One Market (2).jpg|凍肉店 Tsuen Wan The Fresh One
Market (3).jpg|菜及水果檔、手撕雞專門店 Tsuen Wan The Fresh One Market
(4).jpg|糧油、服裝及[單剪店](../Page/剪髮.md "wikilink") Tsuen Wan The
Fresh One Market (5).jpg|香燭祭品、傢俱及雜貨店 Tsuen Wan The Fresh One Market
(6).jpg|花檔及水電維修店

Tsuen Wan The Fresh One Market (7).jpg|麵包店及藥房 Tsuen Wan The Fresh One
Market (8).jpg|小食店及茶餐廳 Tsuen Wan The Fresh One Market
(9).jpg|小食店（2）及自助乾洗店 Tsuen Wan The Fresh One Market
Entrance.jpg|入口及[燒味檔](../Page/燒味.md "wikilink")

<div class="NavFrame collapsed">

<div class="NavHead">

翻新前的大窩口街市

</div>

<div class="NavContent">

Tai Wo Hau Market.jpg|花店、菜檔及肉檔 Tai Wo Hau Market (2).jpg|水果檔 Tai Wo Hau
Market (3).jpg|凍肉檔 Tai Wo Hau Market (4).jpg|雜貨店及服裝店 Tai Wo Hau Market
Entrance.jpg|[燒味檔及入口](../Page/燒味.md "wikilink")

</div>

</div>

#### 康樂設施

Tai Wo Hau Estate Basketball Court.jpg|籃球場 Tai Wo Hau Estate Badminton
Court.jpg|羽毛球場 Tai Wo Hau Estate Volleyball Court.jpg|排球場 Tai Wo Hau
Estate Multi-Propose Ball Games Court.jpg|多用途球場 Tai Wo Hau Estate Tennis
Practice Court.jpg|網球練習場

Tai Wo Hau Estate Table Tennis Zone.jpg|乒乓球檯 Tai Wo Hau Estate Chess
Zone.jpg|棋藝區 Tai Wo Hau Estate Pebble Walking Trail and Gym
Zone.jpg|卵石路步行徑及健體區 Tai Wo Hau Estate Gym Zone (3) and
Playground (6).jpg|健體區及兒童遊樂場（2） Tai Wo Hau Estate Playground
(3).jpg|[鞦韆遊樂場](../Page/鞦韆.md "wikilink")

## 教育及福利設施

### 幼稚園

  - [香港聖公會聖尼哥拉幼兒學校](http://www.snns.edu.hk)（1965年創辦）（位於大窩口社區中心6樓）
  - [救世軍富強幼稚園](http://www.salvationarmy.org.hk/esd/fkkg/)（1984年創辦）（位於大窩口邨富強樓地下）
  - [救世軍大窩口幼兒學校](http://www.salvationarmy.org.hk/esd/twhns/)（1987年創辦）（位於大窩口邨富強樓2樓）
  - [保良局張心瑜幼稚園](https://web.archive.org/web/20170707204845/http://www.plkfcsykg.edu.hk/home.php)（1989年創辦）（位於大窩口邨富靜樓）
  - [仁愛堂彭鴻樟幼稚園](http://ppe.yot.org.hk/kg03/index.php)（1993年創辦）（位於大窩口邨富泰樓地下）

### 綜合服務中心

  - [協康會大窩口中心](https://www.heephong.org/center/detail/tai-wo-hau-centre)（位於大窩口邨富賢樓地下）

### 小學

  - [佛教林炳炎紀念學校（香港佛教聯合會主辦）](http://www.blbyms.edu.hk)

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[大窩口站](../Page/大窩口站.md "wikilink")
    [Tai_Wo_Hau_Station_2011_part3.jpg](https://zh.wikipedia.org/wiki/File:Tai_Wo_Hau_Station_2011_part3.jpg "fig:Tai_Wo_Hau_Station_2011_part3.jpg")

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [荃灣至](../Page/荃灣.md "wikilink")[觀塘線](../Page/觀塘.md "wikilink")
    (24小時服務)\[7\]
  - 荃灣至[藍田](../Page/藍田.md "wikilink")/[油塘線](../Page/油塘.md "wikilink")
    (上午及下午服務)\[8\]
  - 荃灣至[九龍灣線](../Page/九龍灣.md "wikilink") (黃昏服務)\[9\]
  - 荃灣至[坪石線](../Page/坪石邨.md "wikilink")\[10\]
  - 荃灣至[黃大仙](../Page/黃大仙.md "wikilink")/[九龍城線](../Page/九龍城.md "wikilink")\[11\]
  - 荃灣至[何文田線](../Page/何文田.md "wikilink") (平日上午及上午服務)\[12\]
  - 荃灣至[慈雲山線](../Page/慈雲山.md "wikilink")\[13\]
  - 荃灣至[土瓜灣](../Page/土瓜灣.md "wikilink")/[紅磡線](../Page/紅磡.md "wikilink")\[14\]
  - [青山道至荃灣線](../Page/青山道.md "wikilink")\[15\]
  - 荃灣至[佐敦道線](../Page/佐敦道.md "wikilink") (24小時服務)\[16\]
  - 荃灣至[旺角線](../Page/旺角.md "wikilink") (24小時服務)\[17\]
  - 荃灣至[西環線](../Page/西環.md "wikilink")\[18\]
  - 荃灣至[灣仔線](../Page/灣仔.md "wikilink")\[19\]

<!-- end list -->

  - [大窩口道](../Page/大窩口道.md "wikilink")/[石頭街](../Page/石頭街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [荃灣至](../Page/荃灣.md "wikilink")[旺角](../Page/旺角.md "wikilink")/[佐敦道線](../Page/佐敦道.md "wikilink")
    (24小時服務)\[20\]\[21\]

<!-- end list -->

  - [德士古道](../Page/德士古道.md "wikilink")

<!-- end list -->

  - [荃富街](../Page/荃富街.md "wikilink")/[荃華街](../Page/荃華街.md "wikilink")

<!-- end list -->

  - [國瑞路](../Page/國瑞路.md "wikilink")

</div>

</div>

## 所屬區域

大窩口邨位於[葵青區的西葵涌選區](../Page/葵青區.md "wikilink")。然而，不少人認為大窩口邨屬於[荃灣區](../Page/荃灣區.md "wikilink")（甚至連大窩口邨的居民在填寫自己的[地址時](../Page/地址.md "wikilink")，會寫「[荃灣大窩口邨](../Page/荃灣.md "wikilink")」），相信是因為大窩口邨鄰近步行十分鐘內便到[荃灣市中心](../Page/荃灣市中心.md "wikilink")。其實，大窩口邨與東亞、荃灣花園相隔了一條[德士古道](../Page/德士古道.md "wikilink")，這條路正是葵青區與荃灣區的分界線。雖然大窩口邨被劃分入葵青區，但由於地理位置來説大窩口邨相隔荃灣比同屬荃灣區的[石圍角邨還要近](../Page/石圍角邨.md "wikilink")。

## 區議會

大窩口邨被分入上大窩口選區及下大窩口選區，以大窩口道為界。

上大窩口選區從1994年起由民主黨議員當選。1994年，[民主黨](../Page/香港民主黨.md "wikilink")[黃炳權當選](../Page/黃炳權.md "wikilink")，主權移交後被委任為臨時區議員。1999年，黃氏轉戰下大窩口，由同黨的[許祺祥以](../Page/許祺祥.md "wikilink")600票擊敗[民建聯的](../Page/民建聯.md "wikilink")[何良接棒](../Page/何良.md "wikilink")。2003年，許氏以1,000多票之差打敗民建聯的何良連任。

下大窩口在1994年區議會選舉中，民協[王海文擊敗民主黨的](../Page/王海文.md "wikilink")[鍾文輝](../Page/鍾文輝.md "wikilink")。主權移交後，民建聯[黃錦輝成為委任區議員](../Page/黃錦輝.md "wikilink")。1999年，上大窩口區議員[黃炳權轉戰下大窩口](../Page/黃炳權.md "wikilink")，以不足300票擊敗黃錦輝，2003年，黃炳權以2,500多票的壓倒性差距擊敗民建聯的孫滿根以及無黨籍麥偉賢連任。

## 參考

## 外部連結

  - [房屋署大窩口邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2811)

  - [房屋署葵賢苑資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3182)

  - [大窩口邨嶄新的第二型徙廈
    (香港公共圖書館圖片)](http://s127.photobucket.com/albums/p131/LJ7006/?action=view&current=TWHEstate1961.jpg)

  - [大窩口邨第20座
    (香港公共圖書館圖片)](http://s127.photobucket.com/albums/p131/LJ7006/?action=view&current=TWHEstateBLK201966.jpg)

  -
{{-}}

[Category:葵涌](../Category/葵涌.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [大窩口體育館](http://www.lcsd.gov.hk/lsb/b5/facilities.php?ftid=0&did=5)
2.  [政府20億購居屋作宿舍](http://paper.wenweipo.com/2004/05/19/HK0405190029.htm)
3.  [政府斥27億購房委會居屋單位](http://www3.news.gov.hk/ISD/ebulletin/tc/Category/infrastructureandlogistics/040519/html/040519tc06001.htm)

4.  [新居屋宿舍開始入伙](http://www.police.gov.hk/offbeat/804/chi/n05.htm)
5.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>
6.
7.  [荃灣荃灣街市街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kn40.html)
8.  [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
9.  [九龍灣　＞　荃灣荃灣街市街](http://www.16seats.net/chi/rmb/r_kn24.html)
10. [荃灣荃灣街市街　—　新蒲崗及坪石](http://www.16seats.net/chi/rmb/r_kn42.html)
11. [荃灣荃灣街市街　—　黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)
12. [荃灣荃灣街市街　—　何文田](http://www.16seats.net/chi/rmb/r_kn41.html)
13. [荃灣海壩街　—　慈雲山](http://www.16seats.net/chi/rmb/r_kn38.html)
14. [荃灣川龍街　—　土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kn30.html)
15. [深水埗福榮街　＞　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn43.html)
16. [佐敦道寧波街　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn33.html)
17. [旺角朗豪坊　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn47.html)
18. [荃灣荃灣街市街　＞　西環荷蘭街,
    上環信德中心　＞　荃灣荃灣街市街](http://www.16seats.net/chi/rmb/r_hn30.html)
19. [荃灣福來邨　—　灣仔站](http://www.16seats.net/chi/rmb/r_hn40.html)
20. [佐敦道寧波街　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn33.html)
21. [旺角朗豪坊　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn47.html)