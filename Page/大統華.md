**大統華超級市場**（**T\&T
Supermarket**），是一間位於[加拿大的主要售賣](../Page/加拿大.md "wikilink")[大中華地區及](../Page/大中華地區.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")[食品的連鎖](../Page/食品.md "wikilink")[超級市場](../Page/超級市場.md "wikilink")，在全加拿大僱用的[員工達](../Page/員工.md "wikilink")5000人。大統華成立於1993年，目前在[卑詩省](../Page/卑詩省.md "wikilink")[大溫哥華地區](../Page/大溫哥華地區.md "wikilink")、[亞伯達省的](../Page/亞伯達省.md "wikilink")[卡加利和](../Page/卡加利.md "wikilink")[愛民頓以及](../Page/愛民頓.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[多倫多共有](../Page/多倫多.md "wikilink")23家分店。大統華擁有四處物流中心，其中3處在[大溫哥華地區](../Page/大溫哥華地區.md "wikilink")，1處在[多倫多](../Page/多倫多.md "wikilink")。

## 歷史

大統華的創辦人是由[台灣移居](../Page/台灣.md "wikilink")[温哥華多年的](../Page/温哥華.md "wikilink")[商人](../Page/商人.md "wikilink")[李羅昌鈺](../Page/李羅昌鈺.md "wikilink")
(Cindy Lee) 和她的丈夫李安邦 (Jack
Lee)。他們於稍後覓得台灣的[統一企業和](../Page/統一企業.md "wikilink")[美國的](../Page/美國.md "wikilink")[大華超級市場以及一些](../Page/:en:Tawa_Supermarket.md "wikilink")[加拿大投資者合資經營](../Page/加拿大.md "wikilink")。2014年，李羅昌鈺交棒由女兒李佩婷
(Tina Lee) 接任 CEO。

### 由[大溫哥華地區開展業務](../Page/大溫哥華地區.md "wikilink")

1993年[冬季大統華第一家門市於在](../Page/冬季.md "wikilink")[本那比市的](../Page/本那比.md "wikilink")[鐵道鎮開幕](../Page/:en:Metrotown.md "wikilink")。

之後在數年間，大統華在大溫地區繼續開設分店，總共10家分店分別如下:

  - 1993年--[本那比市的](../Page/本那比.md "wikilink")[鐵道鎮分店](../Page/:en:Metropolis_at_Metrotown.md "wikilink")
  - 1993年--[列治文市的](../Page/列治文.md "wikilink")[統一廣場分店](../Page/:en:President_Plaza.md "wikilink")
    (moved to Marine Gateway in 2016)
  - 1996年--[温哥華市的](../Page/温哥華.md "wikilink")[國際村分店](../Page/:en:International_Village.md "wikilink")
  - 1997年--温哥華市的東一街分店
  - 1997年--列治文市[八佰伴中心的](../Page/八佰伴中心.md "wikilink")[大阪超市](../Page/大阪超市.md "wikilink")〔前身是屬於前[日本](../Page/日本.md "wikilink")[八佰伴集團的](../Page/八佰伴.md "wikilink")[八佰伴超市](../Page/八佰伴超市.md "wikilink")，1997年被大统華收購並易名為「大阪超級市場」（Osaka
    Supermarket），以和風精緻路線經營〕
  - 1998年--[素里市](../Page/素里.md "wikilink")152街分店
  - 2000年--[高貴林市的](../Page/高貴林.md "wikilink")[高貴林中心分店](../Page/:en:Coquitlam_Centre.md "wikilink")
  - 2006年--素里市[中央城購物中心分店](../Page/:en:Central_City_Shopping_Centre.md "wikilink")
  - 2010年--[西温哥華市](../Page/西温哥華.md "wikilink")[Park
    Royal商場的](../Page/:en:Park_Royal_Shopping_Centre.md "wikilink")[大阪超市](../Page/大阪超市.md "wikilink")〔以和風精緻路線經營〕
  - 2014年--[列治文市的](../Page/列治文.md "wikilink")[ORA奧福村分店](../Page/ORA奧福村.md "wikilink")
  - 2016年--[溫哥華市的](../Page/溫哥華.md "wikilink")[Marine
    Gateway分店](../Page/Marine_Gateway.md "wikilink")
  - 2018年--[列治文市的](../Page/列治文.md "wikilink")[Lansdowne分店](../Page/Lansdowne.md "wikilink")

### 圖片集

<File:Metrotown>
T\&T.JPG|1993年在[本那比市開業的](../Page/本那比.md "wikilink")[鐵道鎮分店](../Page/:en:Metropolis_at_Metrotown.md "wikilink")
<File:Richmond>
T\&T.JPG|1993年在[列治文市開業的](../Page/列治文.md "wikilink")[統一廣場分店](../Page/:en:President_Plaza.md "wikilink")
<File:International>
Village.JPG|1996年在[温哥華市開業的](../Page/温哥華.md "wikilink")[國際村分店](../Page/:en:International_Village.md "wikilink")
<File:Yaohan> Centre Osaka Supermarket
2018.jpg|1997年在[列治文市](../Page/列治文.md "wikilink")[八佰伴中心開業的](../Page/八佰伴中心.md "wikilink")[大阪超市](../Page/大阪超市.md "wikilink")
<File:T&T> Supermarket parking lot, Vancouver, British Columbia
(2009-07-27).jpg|1997年在[温哥華市開業的東一街分店](../Page/温哥華.md "wikilink")
<File:Coquitlam>
T\&T.JPG|2000年在[高貴林市開業的](../Page/高貴林.md "wikilink")[高貴林中心分店](../Page/:en:Coquitlam_Centre.md "wikilink")
<File:West> Van Osaka
Supermarket.JPG|2010年在[西温哥華市](../Page/西温哥華.md "wikilink")[Park
Royal商場開業的](../Page/:en:Park_Royal_Shopping_Centre.md "wikilink")[大阪超市分店](../Page/大阪超市.md "wikilink")
[File:WardenT\&T.JPG|萬錦市分店](File:WardenT&T.JPG%7C萬錦市分店)

### 1999年東進[亞伯達省](../Page/亞伯達省.md "wikilink")

  - 1999年--[卡加利市的Pacific](../Page/卡加利.md "wikilink") Place分店
  - 2002年--[愛民頓市的](../Page/愛民頓.md "wikilink")[西愛民頓購物中心分店](../Page/西愛民頓購物中心.md "wikilink")
  - 2006年--卡加利市的Harvest Hill分店

### 2002年揮軍東岸[大多倫多地區](../Page/大多倫多地區.md "wikilink")

  - 2002年--[康山市的](../Page/湯山_\(安大略\).md "wikilink")[Promenade Shopping
    Centre分店](../Page/:en:Promenade_Shopping_Centre.md "wikilink")
  - 2004年--[萬錦市分店](../Page/萬錦.md "wikilink")
  - 2005年--[士嘉堡市分店](../Page/:en:Scarborough,_Ontario.md "wikilink")
  - 2006年--[密西沙加市分店](../Page/:en:Mississauga.md "wikilink")
  - 2007年--[多倫多市中心分店](../Page/多倫多.md "wikilink")
  - 2009年--[渥太華市分店](../Page/渥太華.md "wikilink")
  - 2010年--[列治文山市威域分店](../Page/列治文山.md "wikilink")

[T\&T_-_Cherry_Street.JPG](https://zh.wikipedia.org/wiki/File:T&T_-_Cherry_Street.JPG "fig:T&T_-_Cherry_Street.JPG")

### 2009年易主[羅布勞公司](../Page/:en:_Loblaws_Inc..md "wikilink")

2009年7月24日，加拿大最大零售商[羅布勞公司](../Page/:en:_Loblaws_Inc..md "wikilink")（[Loblaws
Inc.](../Page/:en:_Loblaws_Inc..md "wikilink")）宣布以2.25億[加幣收購大統華超級市場](../Page/加幣.md "wikilink")。2009年7月25日，[統一企業公告](../Page/統一企業.md "wikilink")，統一企業[子公司](../Page/子公司.md "wikilink")「[統一亞洲股份有限公司](../Page/統一亞洲.md "wikilink")」（President
Asian Enterprises Inc.）賣出大統華股權以後，總處分利益將近3100萬加幣。

大統華總裁李羅昌鈺強調，大統華被收購後，一切業務不變如昔。大統華仍然以原名繼續經營，經營理念和管理團隊跟以往一樣，包括李羅昌鈺她自己也繼續擔任大統華總裁一職。只是大統華的前股東台商「[統一集團](../Page/統一集團.md "wikilink")」將股分完全賣出，改由羅布勞公司全資擁有。從此大統華便完全成為一間由加拿大资金經營的本地公司\[1\]。

## 集團模式

### 經營模式

過往在[加拿大的](../Page/加拿大.md "wikilink")[華人](../Page/華人.md "wikilink")[超級市場通常衛生環境十分惡劣](../Page/超級市場.md "wikilink")，令很多喜歡購買[華人食物的](../Page/華人.md "wikilink")[白人甚至](../Page/白人.md "wikilink")[華人](../Page/華人.md "wikilink")[移民都不喜歡在這些地方購物](../Page/移民.md "wikilink")。大統華的規模跟地區性連鎖超級市場相若，也可算是加國唯一一個連鎖式經營售賣亞洲食品的超級市場，加上大統華運用西式超級市場的管理方式經營，改善了過往華人超級市場在洋人心目的壞印象。

### 分店部門

每家分店均有鮮肉部、蔬果部、冷品部、雜貨部、以及海鮮部。但每家分店都會因應不同地區的需要而設置不同的部門，例如麵包部及廚食部等。

### 員工架構

大統華所有員工都分為十二個等級，新入職的員工通常都由第十二級開始，而集團總裁李羅昌鈺屬於第一級。每間分店大至上由正副店長、正副部門主管、幹備幹部、主任、技術人員及部門助理等員工

2007年3月，大統華實施了新的員工等級制度，基層員工改為第一級，部門主任(supervisor)則為第二級，如此類推。在每一級之內則再細分為幾個職級，如一個新入職的基層員工為11級（可以理解為1-1級），而資深的基層員工則為1-2、3、4級。

## 服務對象

大統華超級市場最初主要的服務對象是亞洲移民。因為經營模式跟西式超級市場相若，所以很多喜歡中國食品的西方人士會到大統華超級市場購物。

## 位置

### 安大略省

### Alberta省

  - 8882 170 St NW West Edmonton Mall, Edmonton, AB T5T 5X1
  - 9450 137 Ave NW Edmonton, AB T5E 6C2

## 外部連結

  - [大統華超市](http://www.tnt-supermarket.com/)

[Category:加拿大公司](../Category/加拿大公司.md "wikilink")
[Category:加拿大華人公司](../Category/加拿大華人公司.md "wikilink")
[Category:超級市場](../Category/超級市場.md "wikilink")

1.