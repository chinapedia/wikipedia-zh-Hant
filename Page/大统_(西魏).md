**大统**（535年正月—551年十二月）是[西魏文帝](../Page/西魏.md "wikilink")[元宝炬的第一個](../Page/元宝炬.md "wikilink")[年号](../Page/年号.md "wikilink")，西魏使用这个年号歷時16年餘。元宝炬死后废帝[元钦和恭帝](../Page/元钦.md "wikilink")[元廓都没有自己的年号](../Page/元廓.md "wikilink")，因此大统也是西魏唯一的一个年号。

[宇文觉取代西魏称帝后](../Page/宇文觉.md "wikilink")，也没有使用年号。直到[宇文毓时](../Page/宇文毓.md "wikilink")，从559年八月重新开始使用年号[武成](../Page/武成_\(北周\).md "wikilink")。

## 大事记

  - [大统元年](../Page/535年.md "wikilink")——元宝炬在[宇文泰支持下登基](../Page/宇文泰.md "wikilink")，建立西魏。
  - [大统二年](../Page/536年.md "wikilink")——[东魏攻西魏](../Page/东魏.md "wikilink")。
  - [大统三年正月十七日](../Page/537年.md "wikilink")——[潼关之战](../Page/潼关之战.md "wikilink")，东魏攻西魏失败。
  - 大统三年十月初——[沙苑之战](../Page/沙苑之战.md "wikilink")，西魏攻东魏获胜。
  - [大统十年](../Page/544年.md "wikilink")——西魏攻[江陵](../Page/江陵.md "wikilink")。

## 出生

## 逝世

## 纪年

| 大统                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 535年                           | 536年                           | 537年                           | 538年                           | 539年                           | 540年                           | 541年                           | 542年                           | 543年                           | 544年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") |
| 大统                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 545年                           | 546年                           | 547年                           | 548年                           | 549年                           | 550年                           | 551年                           |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") |                                |                                |                                |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[大统年號的政權](../Page/大统.md "wikilink")
  - 同期存在的其他政权年号
      - [上願](../Page/上願.md "wikilink")（535年）：[南朝梁時期領導](../Page/南朝梁.md "wikilink")[鮮于琛的年号](../Page/鮮于琛.md "wikilink")
      - [大同](../Page/大同_\(萧衍\).md "wikilink")（535年正月—546年四月）：[南朝梁政權梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [永漢](../Page/永漢_\(劉敬躬\).md "wikilink")（542年正月—二月）：[南朝梁時期領導](../Page/南朝梁.md "wikilink")[劉敬躬的年号](../Page/劉敬躬.md "wikilink")
      - [中大同](../Page/中大同.md "wikilink")（546年四月—547年四月）：[南朝梁政權梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [太清](../Page/太清_\(萧衍\).md "wikilink")（547年四月—549年十二月）：[南朝梁政權梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [正平](../Page/正平_\(蕭正德\).md "wikilink")（548年十一月—549年六月）：[南朝梁政權](../Page/南朝梁.md "wikilink")[臨賀王](../Page/臨賀.md "wikilink")[蕭正德的年号](../Page/蕭正德.md "wikilink")
      - [大寳](../Page/大宝_\(萧纲\).md "wikilink")（550年正月—551年十二月）：[南朝梁政權梁簡文帝](../Page/南朝梁.md "wikilink")[萧綱的年号](../Page/萧綱.md "wikilink")
      - [天正](../Page/天正_\(萧栋\).md "wikilink")（551年八月—十一月）：[南朝梁政權](../Page/南朝梁.md "wikilink")[豫章王](../Page/豫章.md "wikilink")[萧棟的年号](../Page/萧棟.md "wikilink")
      - [太始](../Page/太始_\(侯景\).md "wikilink")（551年十一月—552年三月）：[南朝梁時期](../Page/南朝梁.md "wikilink")[侯景的年号](../Page/侯景.md "wikilink")
      - [神嘉](../Page/神嘉.md "wikilink")（525年十二月—535年三月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升年号](../Page/劉蠡升.md "wikilink")
      - [天平](../Page/天平_\(元善見\).md "wikilink")（534年十月—537年十二月）：[東魏政权東魏孝靜帝](../Page/東魏.md "wikilink")[元善見年号](../Page/元善見.md "wikilink")
      - [平都](../Page/平都.md "wikilink")（536年九月）：[東魏時期領導](../Page/東魏.md "wikilink")[王迢觸](../Page/王迢觸.md "wikilink")、[曹貳龍年号](../Page/曹貳龍.md "wikilink")
      - [元象](../Page/元象.md "wikilink")（538年正月—539年十一月）：[東魏政权東魏孝靜帝](../Page/東魏.md "wikilink")[元善見年号](../Page/元善見.md "wikilink")
      - [興和](../Page/兴和.md "wikilink")（539年十一月—542年十二月）：[東魏政权東魏孝靜帝](../Page/東魏.md "wikilink")[元善見年号](../Page/元善見.md "wikilink")
      - [武定](../Page/武定.md "wikilink")（543年正月—550年五月）：[東魏政权東魏孝靜帝](../Page/東魏.md "wikilink")[元善見年号](../Page/元善見.md "wikilink")
      - [天保](../Page/天保_\(高洋\).md "wikilink")（550年五月—559年十二月）：[北齊政权北齊文宣帝](../Page/北齊.md "wikilink")[高洋年号](../Page/高洋.md "wikilink")
      - [乾明](../Page/乾明.md "wikilink")：[西魏年号](../Page/西魏.md "wikilink")
      - [天德](../Page/天德_\(李賁\).md "wikilink")（544年—548年）：[越南君主](../Page/越南.md "wikilink")[李賁的年号](../Page/李賁.md "wikilink")
      - [章和](../Page/章和_\(麴坚\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴坚年号](../Page/麴坚.md "wikilink")
      - [永平](../Page/永平_\(麴玄喜\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴玄喜年号](../Page/麴玄喜.md "wikilink")
      - [和平](../Page/和平_\(高昌\).md "wikilink")：[高昌政权年号](../Page/高昌.md "wikilink")
      - [建元](../Page/建元_\(新羅法興王\).md "wikilink")（536年—551年）：[新羅](../Page/新羅.md "wikilink")[法興王](../Page/新羅法興王.md "wikilink")、[真興王的年号](../Page/新羅真興王.md "wikilink")
      - [開國](../Page/開國.md "wikilink")（551年—568年）：新羅真興王之年號

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南北朝年号](../Category/南北朝年号.md "wikilink")
[Category:西魏政治](../Category/西魏政治.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:530年代中国政治](../Category/530年代中国政治.md "wikilink")
[Category:540年代中国政治](../Category/540年代中国政治.md "wikilink")
[Category:550年代中国政治](../Category/550年代中国政治.md "wikilink")