**大耳狐**，學名*Otocyon
megalotis*，又称**蝠耳狐**、**好望狐**。是生活在[非洲草原上的](../Page/非洲.md "wikilink")[犬科動物](../Page/犬科.md "wikilink")，因其耳朵巨大而得名。大耳狐體毛為黃褐色，耳、腿和臉的一部分為[黑色](../Page/黑色.md "wikilink")。體長為55[厘米](../Page/厘米.md "wikilink")，体重3-4.5公斤，耳長達到13[厘米](../Page/厘米.md "wikilink")，是[大耳狐屬唯一的一種](../Page/大耳狐屬.md "wikilink")。

## 描述

[缩略图](https://zh.wikipedia.org/wiki/File:Otocyon_megalotis_-_Etosha_2014.jpg "fig:缩略图")
大耳狐的牙齒48颗，比其它犬科动物多6颗，但要比其他犬科動物小許多，這是由於它們適應了主要以[昆蟲為食](../Page/昆蟲.md "wikilink")，其食物的80%為昆蟲，這些昆蟲包括[白蟻](../Page/白蟻.md "wikilink")、[蝗蟲等等](../Page/蝗蟲.md "wikilink")。此外，它們也吃[嚙齒類](../Page/嚙齒類.md "wikilink")、[鳥類和](../Page/鳥類.md "wikilink")[卵](../Page/卵.md "wikilink")，有時也吃[水果](../Page/水果.md "wikilink")。

夜行動物，妊娠期60-70天，一般以一對成年狐和幼仔（2-5仔）為單位組成種群，居住在[巢穴中](../Page/巢.md "wikilink")。

## 亞種

已知包括以下3個亞種：

  - *Otocyon megalotis
    megalotis*：南[津巴布韋](../Page/津巴布韋.md "wikilink")、[博茨瓦納](../Page/博茨瓦納.md "wikilink")、[納米比亞和](../Page/納米比亞.md "wikilink")[南非](../Page/南非.md "wikilink")
  - *Otocyon megalotis
    virgatus*：[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")、[肯尼亞](../Page/肯尼亞.md "wikilink")
  - *Otocyon megalotis
    canescens*：[埃塞俄比亞](../Page/埃塞俄比亞.md "wikilink")、[索馬里](../Page/索馬里.md "wikilink"))

## 參考

  -
[Category:狐亞科](../Category/狐亞科.md "wikilink")