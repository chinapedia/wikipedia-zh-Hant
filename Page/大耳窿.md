[Payday_loan_shop_window.jpg](https://zh.wikipedia.org/wiki/File:Payday_loan_shop_window.jpg "fig:Payday_loan_shop_window.jpg")[福爾斯徹奇的一個商店櫥窗](../Page/福爾斯徹奇_\(維珍尼亞州\).md "wikilink")\]\]
**大耳窿**（）是從事[高利貸工作的](../Page/高利貸.md "wikilink")[粵語](../Page/粵語.md "wikilink")[俗稱](../Page/俗稱.md "wikilink")。

## 由來

  - 昔日[粵省](../Page/粵.md "wikilink")、[港澳](../Page/港澳.md "wikilink")，主要在[街市放數](../Page/街市.md "wikilink")，街市檔口無論肉販、菜販，遇有輸錢賭徒，或周轉不靈者，便向有[三合會背景的地區性](../Page/三合會.md "wikilink")[黑社會](../Page/黑社會.md "wikilink")“大耳窿”集團借錢，正所謂“跑了和尚跑不了廟”，債務人在街市謀生，每日要做生意，不怕逃跑，乃常借得款。昔日[銀錢交易](../Page/銀錢.md "wikilink")，大數目多數用「[大頭](../Page/銀元.md "wikilink")」，小數目為「[銅板](../Page/硬幣.md "wikilink")」，高利貸者收數後，多數將銀元、硬幣之類塞在耳窿，年久月深，久而久之把耳窿也撐大，故稱為「大耳窿」。
  - 楊子靜編著《[粵語鈎沉](../Page/粵語鈎沉.md "wikilink")》（廣東高等教育出版社）收錄了「大耳窿」的條目：「[香港](../Page/香港.md "wikilink")、[澳門放高利貸的](../Page/澳門.md "wikilink")，被稱為『大耳窿』，乃『大耳窿鬼』的略稱。原指從前活動於[上海的](../Page/上海.md "wikilink")[猶太富人](../Page/猶太.md "wikilink")，一般載一大耳窿（即[耳環](../Page/耳環.md "wikilink")）作飾物，多以放高利貸出名。」
  - 吳昊著《俗文化語言．港式廣府話研究I》（次文化堂）對「大耳窿」的出處有兩種說法。一如上述之兩類。
      - 第一種與《粵語鈎沉》的說法相似，只是上海[猶太人換了港澳的](../Page/猶太人.md "wikilink")「[白頭摩囉](../Page/印度人.md "wikilink")」（[印度](../Page/印度.md "wikilink")、[巴基斯坦籍人士](../Page/巴基斯坦.md "wikilink")，他們多以白巾纏頭），由於他們扮相古怪、愛載一隻大如銅元的耳環，所以耳朵要穿耳窿，港人覺得可憎，便稱之為「大耳窿」。\[1\]\[2\]
      - 第二種說法是，因為活躍於街市放高利貸的人，愛在耳朵塞一枚銀元（當時已是非常大的數目）作記號，以示自己有錢可借，所以便叫他們為「大耳窿」。

## 參考資料

[分類:香港文化](../Page/分類:香港文化.md "wikilink")
[分類:香港歷史](../Page/分類:香港歷史.md "wikilink")
[分類:粵語俚語](../Page/分類:粵語俚語.md "wikilink")

1.
2.