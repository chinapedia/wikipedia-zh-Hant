[Dutch_and_Spanish_Taiwan.png](https://zh.wikipedia.org/wiki/File:Dutch_and_Spanish_Taiwan.png "fig:Dutch_and_Spanish_Taiwan.png")

**大肚王國**（未知—1732年）為一在[臺灣中部由](../Page/臺灣中部.md "wikilink")[臺灣原住民的](../Page/臺灣原住民.md "wikilink")[拍瀑拉族與](../Page/拍瀑拉族.md "wikilink")[巴布薩族](../Page/巴布薩族.md "wikilink")、[巴則海族](../Page/巴則海族.md "wikilink")、[洪雅族](../Page/洪雅族.md "wikilink")、[道卡斯族所建立的跨](../Page/道卡斯族.md "wikilink")[部落聯盟](../Page/部落.md "wikilink")，在鼎盛時期的領域範圍南端約到[鹿港](../Page/鹿港鎮.md "wikilink")，北方則可至[桃園以南之地](../Page/桃園市.md "wikilink")（後來的範圍則在[大肚溪上中下游的流域](../Page/大肚溪.md "wikilink")）。[荷蘭時期的領域範圍主要在今天的](../Page/臺灣荷蘭統治時期.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")，以及[彰化縣北部和](../Page/彰化縣.md "wikilink")[南投縣的一部分](../Page/南投縣.md "wikilink")，\[1\]\[2\]其性質為一鬆散的部落聯盟，因為其[政體無文字證據證明具有組織和有效率的統治形式](../Page/政體.md "wikilink")\[3\]。

## 大肚王稱呼

**大肚王**有文獻記載且較能考證的君主有兩位，分別是[甘仔轄·阿拉米](../Page/甘仔轄·阿拉米.md "wikilink")（荷蘭文：Camachat
Aslamie）和[甘仔轄·馬祿](../Page/甘仔轄·馬祿.md "wikilink")（荷蘭文：Camachat
Maloe）。[漢人稱Aslamie為Quataong](../Page/漢人.md "wikilink")，學者翁佳音推測可能係[閩南語Hoan](../Page/閩南語.md "wikilink")-á-ong（番仔王）的轉訛\[4\]。[荷語稱其為Keizer](../Page/荷語.md "wikilink")
van
Middag，[臺灣原住民則稱之為Lelien](../Page/臺灣原住民.md "wikilink")，意為[白晝之王](../Page/白晝之王.md "wikilink")。歷任大肚王皆以Camachat為[姓氏](../Page/姓氏.md "wikilink")，而Camachat語也是[拍瀑拉語的別稱](../Page/拍瀑拉語.md "wikilink")。Camacht
Aslamie於1648年逝世後，其外甥Maloe繼任大肚王。Maloe繼位後，由於尚年輕，所以與[荷蘭東印度公司交涉時](../Page/荷蘭東印度公司.md "wikilink")，大部份都由其繼父Tarraboe持籐杖出席集會，且因當時[大肚社係傾向以女性來核心維持家系](../Page/大肚社.md "wikilink")，當地實權在Maloe的外祖母手中。\[5\]

## 統治性質與內涵

有學者認為，荷蘭文獻中的大肚王之「王」與 kingship概念不一定相同\[6\]。

1722年（[康熙六十一年](../Page/康熙.md "wikilink")），出任巡臺御史的[黃叔璥在他的](../Page/黃叔璥.md "wikilink")《[臺海使槎錄](../Page/臺海使槎錄.md "wikilink")》一書中，有這樣的記載：「大肚山形，遠望如百雉高城，昔有**番長**名大眉。」說明17世紀臺灣中部確實有一個「番長」存在，而其可能是跨部落的。

「王」的角色，是在祭典儀式中扮演對眾人祈福的角色，社眾對「王」呈貢獵物。「王權」的內涵，包括對轄下之社發生紛爭時擔任仲裁，並提供實質庇護的能力。此一屬巴布拉族的「王」，轄下統治19或20社，包含巴布拉、巴則海、道卡斯、洪雅、巴布薩等不同語族；依據蘇格蘭人大衛·萊特（David
Wright）的記載，大肚王國最強盛的時候曾統治27個部落，只是後來有9至10部落獨立\[7\]。「王」所屬的大肚南社傾向以女性為核心來維持家系；王外出時的儀式性權力展現並不明顯，只有一、二名隨從跟隨，也沒有定奪屬民生死的權利\[8\]。

「大肚王」透過流域體系建立的政治主權，具有地理環境相對穩定、握有河口區位等地理因素的基礎，利於在當時的背景下，透過流域體系建立跨語族的統治；然後，「王」的統治內涵，再透過諸如控制首獲獵物權、仲裁屬民世俗紛爭、提供部份實質庇護，與扮演祭儀中的象徵性角色等含括經濟、政治、軍事與意識型態上的實踐，形塑出以「王」為統治範圍——以河口一帶為權力核心、流域體系為空間網絡的區域輪廓\[9\]\[10\]。

## 歷史

### 史前時期

### 日本戰國時期

日本戰國時代文祿2年（1593年），豐臣秀吉派遣使者原田孫七郎前往臺灣（當時日本人稱為高山國）要求納貢所攜帶的諭令文書，但是因為使者找不到可以傳遞文書給高山國的人而不得要領。
由此可知雖然當時日本人認為台灣島上有政體存在，實際上卻找不到。

### 大航海時代

[荷蘭東印度公司於](../Page/荷蘭東印度公司.md "wikilink")1624年起殖民[臺灣](../Page/臺灣.md "wikilink")，到了1642年征服[北臺灣後](../Page/北臺灣.md "wikilink")，荷蘭人的目標轉為征服西部平原的原住民，以連通臺灣南北的道路。[荷蘭人獲海盜情報](../Page/荷蘭人.md "wikilink")，知在中部馬芝遴地區（[彰化縣](../Page/彰化縣.md "wikilink")[福興鄉與](../Page/福興鄉_\(臺灣\).md "wikilink")[鹿港鎮一帶](../Page/鹿港鎮.md "wikilink")）計22社，其中[大甲溪以南的](../Page/大甲溪.md "wikilink")18社是由一位叫甘仔轄·阿拉米（Kamachat
Aslamie）的領袖所統轄。

1638年，[荷蘭即已知大肚王國的存在](../Page/荷蘭.md "wikilink")，但始終未派兵征服，直到1644年，[荷蘭上尉Piter](../Page/荷蘭.md "wikilink")
Boon率兵遠征[北臺灣未臣服的原住民](../Page/北臺灣.md "wikilink")，戰勝後南下打通陸路\[11\]，因為遭遇[巴布拉族的強烈反擊而未成功](../Page/巴布拉族.md "wikilink")。翌年Piter
Boon再度進攻，摧毀了13座反荷部落\[12\]，Kamachat Aslamie只得接受范布鍊（Simon van
Breen/1643-1647）[牧師的協調](../Page/牧師.md "wikilink")。\[13\]

1645年4月荷蘭人召開南部的地方會議，Kamachat
Aslamie跟荷蘭東印度公司訂約，表示服從，不過直到1662年荷蘭人離開臺灣為止，大肚王國都維持半獨立狀態。大肚王國不肯接受[基督教](../Page/基督教.md "wikilink")，只讓歐洲人通過領土而不准他們定居，也無荷語的翻譯員\[14\]。

大肚王國在1645年左右所統轄的範圍主要是[大肚溪流域](../Page/大肚溪.md "wikilink")，大約是[大甲溪北岸的](../Page/大甲溪.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[后里之南至大肚溪流域](../Page/后里.md "wikilink")。脫離的這十餘社據考證大約位於[大安溪南岸一帶](../Page/大安溪.md "wikilink")，意味著大肚王國的勢力曾及於[大安溪南岸](../Page/大安溪.md "wikilink")，是跨部落的統治或聯盟關係。

### 明鄭時期

大肚王國對於後來的[明鄭時期鄭氏政權亦對抗之](../Page/明鄭時期.md "wikilink")。1661年[鄭成功領兵渡海攻打荷蘭東印度公司軍隊](../Page/鄭成功.md "wikilink")，取得台湾統治權，大肚王國頑強抵抗[鄭軍](../Page/鄭軍.md "wikilink")，導致[鄭成功等人認為他們受到](../Page/鄭成功.md "wikilink")[荷蘭的煽動](../Page/荷蘭.md "wikilink")。由於鄭軍當時尚未攻下[安平](../Page/安平_\(臺南市地名\).md "wikilink")，就已缺糧，因此下令往北屯墾，就地取糧或種植，因此與大肚王國諸社發生激烈衝突，此次衝突，鄭軍將領楊祖陣亡，一鎮之兵（約五百人）「無一生還者」。後來鄭軍再派出將領黃安，以埋伏之計襲殺大肚社頭目。

鄭成功趕走荷蘭人之後，為了反清之需要，實施「[兵農合一](../Page/兵農合一.md "wikilink")」政策，派遣鄭軍分赴各地屯墾，侵害到原住民族的活動空間，導致鄭氏政權和大肚王國數次武裝衝突。

其中1670年大肚王國轄下的[沙轆社抵抗鄭氏侵略](../Page/沙轆社.md "wikilink")，遭鄭氏政權將領[劉國軒強力進攻](../Page/劉國軒.md "wikilink")，屠殺至僅剩六人，幾乎滅族，史稱[劉國軒屠村事件](../Page/劉國軒屠村事件.md "wikilink")。

### 臺灣清治時期

1731年（[雍正九年](../Page/雍正.md "wikilink")），[清廷官吏對原住民指派勞役過多](../Page/清.md "wikilink")，引起原住民群起反抗，發生[大甲西社番亂](../Page/大甲西社番亂.md "wikilink")（[大甲西社抗清事件](../Page/大甲西社抗清事件.md "wikilink")），清軍利用[岸裡社](../Page/岸裡社.md "wikilink")「以番制番」，翌年被鎮壓下來，各族人陸續逃離原居地，遷往[埔里](../Page/埔里鎮.md "wikilink")（[南投縣北部](../Page/南投縣.md "wikilink")）一帶，大肚王國翌年亦告瓦解。

## 祖靈祭

台中沙鹿地區[拍瀑拉族人於農曆八月二日舉行祖靈祭](../Page/拍瀑拉族.md "wikilink")，也是傳統的過年節慶。\[15\]

## 歷史年表

*起始年代見[註釋1](../Page/大肚王國#cite_note-1.md "wikilink")。*

  - 1624年 荷蘭人登陸臺灣。

<!-- end list -->

  - 1638年 [荷蘭東印度公司獲海盜情報](../Page/荷蘭東印度公司.md "wikilink")，得知大肚王國的存在。

<!-- end list -->

  - 1644年 荷蘭人進攻北臺灣的[凱達格蘭族](../Page/凱達格蘭族.md "wikilink")，事成後南下進攻大肚王國，失敗。

<!-- end list -->

  - 1645年
    荷人再度進攻，毀13部落，國王與荷蘭東印度公司簽約，依約定要參加地方會議，同年[牛罵社](../Page/牛罵社.md "wikilink")，[沙轆社與](../Page/沙轆社.md "wikilink")
    Deredonsel社獨立。

<!-- end list -->

  - 1648年 大肚王Kamacht Aslamie逝，由其姊妹之子Camacht Maloe獲選為繼任人選。

<!-- end list -->

  - 1654年 Camachat Maloe和繼父 Terroge（Tarraboe）一同出席集會，領受籐杖。

<!-- end list -->

  - 1650年代 遊記：「Formosa 是個非常富庶之島...最富庶的地方現由大肚王所統轄」。

<!-- end list -->

  - 1640年代到1650年代 ：第三區的領土為 Keizer van Midag（King of Middag）王所轄有。

<!-- end list -->

  - 1661年
    鄭成功軍隊與大肚王國發生衝突，鄭軍高凌被殺；鄭成功令[楊祖征之](../Page/楊祖.md "wikilink")，又被殺；鄭成功派[黃安、陳瑞進攻](../Page/鄭成功征臺部將列表.md "wikilink")，誘殺大肚番[阿德狗讓](../Page/阿德狗讓.md "wikilink")，戰火遍及大肚社以及至Taurinap諸社。

<!-- end list -->

  - 1662年 [鄭成功打敗荷蘭佔領南臺灣](../Page/鄭成功.md "wikilink")。

<!-- end list -->

  - 1664年
    [鄭經派劉國軒到](../Page/鄭經.md "wikilink")[半線](../Page/半線.md "wikilink")[屯田](../Page/屯田.md "wikilink")

<!-- end list -->

  - 1670年
    劉國軒進攻[沙轆社](../Page/沙轆社.md "wikilink")，屠殺數百人，並和鄭經一起進攻斗尾龍岸社，致大肚社民遷往南投埔里，水里社民（現改稱臺中市[龍井區](../Page/龍井區.md "wikilink")）逃往南投水里，大肚王國開始解體，而[東寧王朝則得到](../Page/東寧.md "wikilink")[大肚臺地以西](../Page/大肚臺地.md "wikilink")、神岡等地。

<!-- end list -->

  - 1699年
    [吞霄社事件爆發](../Page/吞霄社事件.md "wikilink")，清得[岸裡社之助平定](../Page/岸裡社.md "wikilink")。

<!-- end list -->

  - 1715年
    [巴則海族岸裡社頭目](../Page/巴宰族.md "wikilink")[阿莫帶族人投清](../Page/阿莫.md "wikilink")，任[張達京為通事](../Page/張達京.md "wikilink")。

<!-- end list -->

  - 1731年 [大甲西社抗清事件爆發](../Page/大甲西社抗清事件.md "wikilink")。

<!-- end list -->

  - 1732年
    負責征伐大甲西社的福建分巡臺灣道[倪象愷的劉姓表親為求立功](../Page/倪象愷.md "wikilink")，將大肚社（在今臺中市大肚區）五名前來幫助官府運糧的「良番」（歸順的原住民）斬首引發不滿，大肚王國集結大肚社與十餘社原住民圍攻彰化，清廷派臺灣鎮總兵[王郡以及岸裡社的部分原住民征伐](../Page/王郡.md "wikilink")，大肚社等社兵敗投降，大肚王國亦告瓦解。

<!-- end list -->

  - 1822年
    [中部](../Page/中臺灣.md "wikilink")[平埔族因生活困難而大舉遷往埔里](../Page/平埔族.md "wikilink")。

## 相關作品

  - [文化研究](../Page/文化研究.md "wikilink")
      - [伊能嘉矩](../Page/伊能嘉矩.md "wikilink")《[臺灣文化誌](../Page/臺灣文化誌.md "wikilink")》
  - [中文小說](../Page/中文小說.md "wikilink")
      - [許瑞芳](../Page/許瑞芳.md "wikilink")《[大肚王傳奇](../Page/大肚王傳奇.md "wikilink")》
      - [趙慧琳](../Page/趙慧琳.md "wikilink")《[大肚城、歸來](../Page/大肚城、歸來.md "wikilink")》
      - [張秋鳳](../Page/張秋鳳.md "wikilink")《[大肚王國的故事](../Page/大肚王國的故事.md "wikilink")》
      - [莫凡](../Page/莫凡.md "wikilink")、[蔡達源](../Page/蔡達源.md "wikilink")《[大肚王：甘仔轄.阿拉米](../Page/大肚王：甘仔轄.阿拉米.md "wikilink")》
      - [臺中一中](../Page/臺中一中.md "wikilink")
        [何冠威](../Page/何冠威.md "wikilink")《鹿與白晝》（「第二屆全國高中職奇幻文學獎」首獎）

## 参见

  - [臺灣原住民](../Page/臺灣原住民.md "wikilink")
  - [臺灣政治史](../Page/臺灣政治史.md "wikilink")
  - [臺中歷史](../Page/臺中歷史.md "wikilink")
  - [阿德狗讓](../Page/阿德狗讓.md "wikilink")
  - [劉國軒屠村事件](../Page/劉國軒屠村事件.md "wikilink")
  - [吞霄社事變](../Page/吞霄社事變.md "wikilink")
  - [大甲西社抗清事件](../Page/大甲西社抗清事件.md "wikilink")

## 註釋

## 參考文獻

## 外部連結

  - [大肚番王傳奇](https://web.archive.org/web/20091213050122/http://ianthro.tw/p/64)
    中央研究院平埔文化資訊網
  - [中部平埔族群青年聯盟](https://www.facebook.com/CTPIYA/) 中部平埔族群青年聯盟

[大肚王國](../Category/大肚王國.md "wikilink")
[M](../Category/台灣原住民歷史.md "wikilink")
[M](../Category/台中市歷史.md "wikilink")
[M](../Category/台灣史前時期.md "wikilink")
[M](../Category/台灣政治史.md "wikilink")
[MK](../Category/台灣歷史政權.md "wikilink")
[MK](../Category/大肚區.md "wikilink")
[Category:已不存在的亞洲君主國](../Category/已不存在的亞洲君主國.md "wikilink")
[Category:東亞歷史國家](../Category/東亞歷史國家.md "wikilink")
[Category:16世紀台灣](../Category/16世紀台灣.md "wikilink")
[Category:17世紀台灣](../Category/17世紀台灣.md "wikilink")

1.

2.

3.  陳國棟，《台灣的山海經驗》，台北市:遠流，ISBN 978-957-32-5679-3，頁382

4.  翁佳音，〈被遺忘的台灣原住民史——Quata（大肚番王）初考〉《臺灣風物》42卷4期，頁184

5.  康培德，《[環境、空間與區域：地理學觀點下十七世紀中葉「大肚王」統治的消長](http://homepage.ntu.edu.tw/~bcla/e_book/59/05.pdf)》，頁12

6.
7.

8.
9.
10. Mann 1986; Wickham 1988: 63-78

11.
12.
13. 賴永祥，[十七世紀荷蘭駐臺宣教師名單](http://www.laijohn.com/missionaries/Dutch/taiwan/17cent.htm)，賴永祥長老史料庫，retrieved
    on 2013-07-29。

14.
15.