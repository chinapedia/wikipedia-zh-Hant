**大英博物馆**（）是一座於[英國](../Page/英國.md "wikilink")[倫敦的綜合](../Page/倫敦.md "wikilink")[博物館](../Page/博物館.md "wikilink")，也是世界上规模最大、最著名的博物馆之一，成立于1753年。目前博物馆拥有藏品800多万件。由于空间的限制，目前还有大批藏品未能公开展出。博物馆在1759年1月15日起正式对公众开放。

大英博物馆的渊源最早可追溯到1753年。[汉斯·斯隆](../Page/汉斯·斯隆.md "wikilink")（1660至1753年）爵士是当时的一位著名收藏家，1753年他去世后遗留下来的个人藏品达將近80000件，还有大批植物[标本及书籍](../Page/标本.md "wikilink")、手稿。根据他的遗嘱，所有藏品都捐赠给国家。这些藏品最后被交给了[英国国会](../Page/英国国会.md "wikilink")。在通过公众募款筹集建筑博物馆的资金後，大英博物馆最终于1759年1月15日在伦敦市区附近的[蒙塔古宮](../Page/蒙塔古宮.md "wikilink")（Montague
House）成立並对公众开放。

博物馆在开放后通过[英国人在各地的各种活动攫取了大批珍贵藏品](../Page/英国.md "wikilink")，早期的大英博物馆倾向于收集自然历史标本，但也有大量文物、书籍，因此吸引了大批参观者。到了19世纪初，蒙塔古宮已经显得不敷使用了。于是1824年博物馆决定在蒙塔古宮北面建造一座新馆，並在1840年代完成，舊蒙塔古宮不久後便被拆除。新馆建成后不久又在院子裡建了对公众开放的圆形阅览室。

由于空间的限制，1880年大英博物馆将自然历史标本与考古文物分离，大英博物馆专门收集考古文物。1973年，博物馆再次重新劃分，将书籍、[手稿等内容分离组成新的](../Page/手稿.md "wikilink")[大英图书馆](../Page/大英图书馆.md "wikilink")。

大英博物馆历史上除了1972年的几个月外，一直都是免费对公众开放。2002年博物馆面临了严重的财政危机，由于馆员抗议裁员，博物馆甚至被迫关闭了几天。几个星期后，一个[希腊雕像失窃](../Page/希腊.md "wikilink")，主要原因是保安人员的缺乏。

## 结构

[British_Museum_from_NE_2.JPG](https://zh.wikipedia.org/wiki/File:British_Museum_from_NE_2.JPG "fig:British_Museum_from_NE_2.JPG")
[British_Museum_Dome.jpg](https://zh.wikipedia.org/wiki/File:British_Museum_Dome.jpg "fig:British_Museum_Dome.jpg")
大中庭（Great
Court）位于大英博物馆中心，于2000年12月建成开放，目前是[欧洲最大的有顶](../Page/欧洲.md "wikilink")[广场](../Page/广场.md "wikilink")。广场的顶部是用3312块三角形的玻璃片组成的。广场中央为大英博物馆的阅览室，对公众开放。

[大英博物馆阅览室原来属于](../Page/大英博物馆阅览室.md "wikilink")[大英图书馆](../Page/大英图书馆.md "wikilink")，但图书馆阅览室已经搬到了图书馆大楼，现在为博物馆的阅览室。

大英博物馆目前分为10个研究和专业馆：非洲、大洋洲和美洲馆；古埃及和苏丹馆；亚洲馆；不列颠、欧洲和史前时期馆；硬币和纪念币馆；保护和科学研究馆；希腊和罗马馆；中东馆；便携古物和珍宝馆；版画和素描馆。

[British_Museum_Reading_Room_Panorama_Feb_2006.jpg](https://zh.wikipedia.org/wiki/File:British_Museum_Reading_Room_Panorama_Feb_2006.jpg "fig:British_Museum_Reading_Room_Panorama_Feb_2006.jpg")

## 开放信息

地址：大羅素街（Great Russell Street，郵遞區號London WC1B
3DG），可乘坐[伦敦地铁在](../Page/伦敦地铁.md "wikilink")[霍本站](../Page/霍本站.md "wikilink")、[-{zh-hans:托特纳姆宮路站;zh-hk:托定咸宮路站;zh-tw:托登罕宮路站;}-](../Page/圖騰漢廳路站.md "wikilink")、[古吉街站或](../Page/古吉街站.md "wikilink")[羅素廣場站下车](../Page/羅素廣場站.md "wikilink")。[倫敦地鐵中央線曾經設有](../Page/倫敦地鐵中央線.md "wikilink")，但已於1933年廢止。

大英博物馆，入场免费。

开放时间：

  - 博物馆开放时间
      - 星期六至星期四：10:00 - 18:00
      - 星期五：10:00 - 20:30
  - 广场开放时间
      - 星期一：09:00 - 18:00
      - 星期二和星期三：09:00 - 21:00
      - 星期四至星期六：09:00 - 23:00
      - 星期日：09:00 - 21:00

## 著名的藏品

### 源自[埃及](../Page/埃及.md "wikilink")

  - [埃及](../Page/埃及.md "wikilink")[羅塞塔石碑](../Page/羅塞塔石碑.md "wikilink")（[拿破仑在埃及溃败后英国获得的一批埃及文物中最珍贵的一个](../Page/拿破仑.md "wikilink")）

  - [拉美西斯二世头像](../Page/拉美西斯二世.md "wikilink")（1818年由英国驻埃及总领事捐献）

  - [法老](../Page/法老.md "wikilink")[阿孟霍特普三世头像](../Page/阿孟霍特普三世.md "wikilink")（1823年购得）

  - （1867年获得）

  - [格林菲爾德莎草紙卷](../Page/格林菲爾德莎草紙卷.md "wikilink")

<File:BM>; RM18 - GR, The Parthenon Galleries 1 Temple of Athena
Parthenos (447-438 B.C) + North Slip Room, -Full Elevation & Viewing
North-.JPG|[帕德嫩神廟石雕](../Page/帕德嫩神廟.md "wikilink")
<File:BM,_AES_Egyptian_Sulpture_~_Colossal_bust_of_Ramesses_II,_the_'Younger_Memnon'_(1250_BC)_(Room_4).jpg>|[拉美西斯二世头像](../Page/拉美西斯二世.md "wikilink")
<File:England>; London - The British Museum, Egypt Egyptian Sculpture \~
Colossal granite head of Amenhotep III (Room
4).2.JPG|[阿孟霍特普三世头像](../Page/阿孟霍特普三世.md "wikilink")
<File:BM>, AES Egyptian Sculpture (Room 4), View North.4.JPG|埃及館展出的石雕

### 源自[西亞](../Page/西亞.md "wikilink")

  - 大英博物館西亞館所展出的公牛男人

<File:Winged> Human-headed Bulls.JPG|西亚馆展出的公牛男人

### 源自[希臘](../Page/希臘.md "wikilink")

  - [雅典](../Page/雅典.md "wikilink")[帕德农神廟的大理石雕刻](../Page/帕德农神廟.md "wikilink")，即[埃爾金石雕](../Page/埃爾金石雕.md "wikilink")

### 源自[羅馬帝國時期](../Page/羅馬帝國.md "wikilink")

  - [波特兰花瓶](../Page/波特兰花瓶.md "wikilink")

<File:Portland> Vase BM Gem4036 n5.jpg|波特兰花瓶

### 源自[中國](../Page/中國.md "wikilink")

  - [康侯簋](../Page/康侯簋.md "wikilink")
  - 傳為晉[顧愷之的](../Page/顧愷之.md "wikilink")[女史箴圖](../Page/女史箴圖.md "wikilink")，但現代學者普遍認為該畫卷為5世紀到8世紀時期的摹品。（1900年[八国联军入侵北京時劫掠所得](../Page/八国联军.md "wikilink")）

<File:Primo> periodo degli zhou dell'est, contenitore rituale in bronzo
(kang hou gui), XI sec. ac..JPG|[康侯簋](../Page/康侯簋.md "wikilink")
<File:Admonitions> Scroll Scene 4.jpg|
[女史箴圖](../Page/女史箴圖.md "wikilink") <File:Dinastia> liao,
luohan (arhat), da hebei, 907-1125
ca..JPG|大英博物館藏[遼三彩](../Page/遼三彩.md "wikilink")[羅漢像](../Page/羅漢.md "wikilink")
<File:Mandarin>
fish.jpg|大英博物館藏[元代](../Page/元代.md "wikilink")[鳜鱼图案](../Page/鳜鱼.md "wikilink")[青花瓷盘](../Page/青花.md "wikilink")

### 源自[大洋洲](../Page/大洋洲.md "wikilink")

  - [摩艾石像](../Page/摩艾石像.md "wikilink")

<File:Moai>, British Museum
London.jpg|大英博物館藏[摩艾石像](../Page/摩艾石像.md "wikilink")

## 参见

  - [大英图书馆](../Page/大英图书馆.md "wikilink")
  - 伦敦[自然史博物馆](../Page/自然史博物馆_\(伦敦\).md "wikilink")
  - [伦敦科学博物馆](../Page/伦敦科学博物馆.md "wikilink")
  - [维多利亚和阿尔伯特博物馆](../Page/维多利亚和阿尔伯特博物馆.md "wikilink")

## 参考资料

<references/>

## 外部連結

  -
  - [歷史](http://www.thebritishmuseum.ac.uk/the_museum/history/general_history.aspx)

  - [大英博物館最新中國瓷器展記](http://qiao.iorz.com/post/2/83)

  -
  -
  -
[Category:倫敦博物館](../Category/倫敦博物館.md "wikilink")
[Category:英国美术馆](../Category/英国美术馆.md "wikilink")
[Category:英国历史博物馆](../Category/英国历史博物馆.md "wikilink")
[Category:大英博物館](../Category/大英博物館.md "wikilink")
[Category:浮世绘博物馆](../Category/浮世绘博物馆.md "wikilink")
[Category:亚洲艺术博物馆](../Category/亚洲艺术博物馆.md "wikilink")
[Category:一级登录博物馆建筑](../Category/一级登录博物馆建筑.md "wikilink")
[Category:古希腊博物馆](../Category/古希腊博物馆.md "wikilink")
[Category:古罗马博物馆](../Category/古罗马博物馆.md "wikilink")
[Category:古代近东博物馆](../Category/古代近东博物馆.md "wikilink")
[Category:货币博物馆](../Category/货币博物馆.md "wikilink")
[Category:行政法人](../Category/行政法人.md "wikilink")
[Category:1753年英格蘭建立](../Category/1753年英格蘭建立.md "wikilink")
[Category:1753年建立的博物館](../Category/1753年建立的博物館.md "wikilink")