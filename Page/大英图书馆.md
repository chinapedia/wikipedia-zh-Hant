**大英图书馆**（British
Library）是目前英国的[国家图书馆](../Page/国家图书馆.md "wikilink")，也是世界上最大的[学术图书馆之一](../Page/学术图书馆.md "wikilink")，擁有超過一億五千萬件館藏。大英图書館中的館藏包括世上幾乎所有語文的作品；\[1\]它們不只有書籍，還有其他印刷品和數碼內容，包括[手稿](../Page/手稿.md "wikilink")、[期刊](../Page/期刊.md "wikilink")、[報紙](../Page/報紙.md "wikilink")、[雜誌](../Page/雜誌.md "wikilink")、[劇本](../Page/劇本.md "wikilink")、[專利](../Page/專利.md "wikilink")、[資料庫](../Page/資料庫.md "wikilink")、[地圖](../Page/地圖.md "wikilink")、[郵票](../Page/郵票.md "wikilink")、[圖畫](../Page/圖畫.md "wikilink")、[樂譜](../Page/樂譜.md "wikilink")、[錄影和](../Page/錄影.md "wikilink")[錄音](../Page/錄音.md "wikilink")。單以藏書量計，大英图書館有一千四百萬本藏書，是除了[美國國會圖書館外全球藏書量最大的圖書館](../Page/國會圖書館_\(美國\).md "wikilink")。大英图書館的館藏既有最近出版的書籍和報刊，也有古老至公元前二千年的古籍手抄本。

根據英國法例，任何出版商在英國出版書籍，都需要[法定送存一本複本予大英图書館收藏](../Page/法定送存.md "wikilink")；大英图書館也會主動從購入外國書籍和其他藏品。每一年，大英图書館都會添置約三百萬件新館藏，每年的新館藏共需約十一公里長的書架擺置。跟大英博物館一樣，大英图書館的營運經費由英國政府提供，但它不是英國政府的一部分。

大英图書館有兩個館址。主館址設於[倫敦](../Page/倫敦.md "wikilink")[聖潘克拉斯火車站旁](../Page/聖潘克拉斯火車站.md "wikilink")；另有一藏書庫設於[西約克郡](../Page/西約克郡.md "wikilink")。

## 历史

[British_Library_-_Kings_Cross_-_London_-_England_-_020504.jpg](https://zh.wikipedia.org/wiki/File:British_Library_-_Kings_Cross_-_London_-_England_-_020504.jpg "fig:British_Library_-_Kings_Cross_-_London_-_England_-_020504.jpg")
大英图书馆的前身是在1753年在[伦敦成立的](../Page/伦敦.md "wikilink")[大英博物馆](../Page/大英博物馆.md "wikilink")。1857年建成的拱形屋顶阅览室是图书馆建筑史上的杰作。曾在大英图书馆经常借阅书籍的著名读者有：[马克思](../Page/马克思.md "wikilink")、[列宁](../Page/列宁.md "wikilink")、[查尔斯·狄更斯](../Page/查尔斯·狄更斯.md "wikilink")、[弗吉尼亚·伍尔芙等](../Page/弗吉尼亚·伍尔芙.md "wikilink")。图书馆的珍贵藏书有：两部在1215年6月英国国王约翰在拉尼米德签署的[大宪章](../Page/大宪章.md "wikilink")、现存的唯一一部[裴欧沃夫的原始手稿](../Page/贝奥武夫.md "wikilink")、两部[谷登堡印制的](../Page/谷登堡.md "wikilink")《[圣经](../Page/圣经.md "wikilink")》（一部是在1454到1455年印制的[42行圣经](../Page/谷登堡圣经.md "wikilink")，另一部是在后期印制的36行圣经）、[莎士比亚的原本手稿](../Page/莎士比亚.md "wikilink")、古老的圣经《[西乃抄本](../Page/西乃抄本.md "wikilink")》、佛教典籍《[金剛經](../Page/金剛經.md "wikilink")》的早期印刷本，这些无价的珍贵藏品在大英图书馆中会经常展出。

2014年5月16日，大英图书馆開始陸續將館藏珍品以[創用CC](../Page/創用CC.md "wikilink")4.0條款授權在其網站釋出圖片。\[2\]

## 法定送存

1911年起，大英图书馆开始收藏所有在英国出版的受版权保护的书籍。1973年开始英国通过法律正式规定大英图书馆为英国的国家图书馆。愛爾蘭共和國也在2000年的著作權法中，指定大英圖書館為法定送存機構。

## 图书馆结构

[Brit_library_pancras.JPG](https://zh.wikipedia.org/wiki/File:Brit_library_pancras.JPG "fig:Brit_library_pancras.JPG")
现今的大英图书馆由三个部分组成：

  - 在1998年在伦敦新建的图书馆。这里举办的展览都可以免费参观
  - 期刊图书馆收藏了超过5万种不同的报纸和杂志
  - 大英图书馆馆藏资料库每年的书籍借阅量超过400万次

## 注釋

## 外部链接

  - [大英图书馆主页](http://www.bl.uk/)
  - [大英图书馆发展研究室](https://web.archive.org/web/20040812080019/http://www.ids.ac.uk/blds/)

[Category:国家图书馆](../Category/国家图书馆.md "wikilink")
[\*](../Category/大英图书馆.md "wikilink")
[Category:法定送存圖書館](../Category/法定送存圖書館.md "wikilink")

1.
2.  [British Library puts literary treasures
    online](http://bigstory.ap.org/article/british-library-puts-literary-treasures-online)，[William
    Blake](http://bigstory.ap.org/tags/william-blake)，[美聯社](../Page/美聯社.md "wikilink")，2014-05-16，2014-05-16查閱。