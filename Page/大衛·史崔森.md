**大衛·史崔森**
(，)是一名美國男演員，他曾經被[奧斯卡提名電影](../Page/奧斯卡.md "wikilink")、電視、舞台劇演員。1970年畢業於[馬薩諸塞州的](../Page/馬薩諸塞州.md "wikilink")[威廉姆斯學院](../Page/威廉姆斯學院.md "wikilink")。

## 作品

  - 《[七人返鄉](../Page/七人返鄉.md "wikilink")》 (1980年)
  - 《[戀愛症候群](../Page/戀愛症候群.md "wikilink")》 (1983年)
  - 《施活的遭遇》 (Silkwood1983年)
  - 《The Brother from Another Planet》 (1984年)
  - 《When Nature Calls》 (1985年)
  - 《[強盜爸爸](../Page/強盜爸爸.md "wikilink")》 (1986年)
  - 《Broken Vows》 (1987年)([TV](../Page/TV.md "wikilink"))
  - 《[暴亂風雲](../Page/暴亂風雲.md "wikilink")》 (1987年)
  - 《Stars and Bars》 (1988年)
  - 《Eight Men Out》 (1988年)
  - 《[英烈的歲月](../Page/英烈的歲月.md "wikilink")》 (1990年)
  - 《[熱浪](../Page/熱浪.md "wikilink")》
    (1990年)([TV](../Page/TV.md "wikilink"))
  - 《Son of the Morning Star》 (1991年)([TV](../Page/TV.md "wikilink"))
  - 《[希望之城](../Page/希望之城.md "wikilink")》 (1991年)
  - 《[紅粉聯盟](../Page/紅粉聯盟.md "wikilink")》 (1992年)
  - 《[天生贏家](../Page/天生贏家.md "wikilink")》 (1992年)
  - 《[神鬼尖兵](../Page/神鬼尖兵.md "wikilink")》 (1992年)
  - 《[激情魚](../Page/激情魚.md "wikilink")》 (1992年)
  - 《我的天才家庭》 (1993年)
  - 《[黑色豪門企業](../Page/黑色豪門企業.md "wikilink")》 (1993年)
  - 《[驚濤駭浪](../Page/驚濤駭浪.md "wikilink")》 (1994年)
  - 《[生母養母的戰爭](../Page/生母養母的戰爭.md "wikilink")》 (1995年)
  - 《[熱淚傷痕](../Page/熱淚傷痕.md "wikilink")》 (1995年)
  - 《[心情故事](../Page/心情故事.md "wikilink")》 (1995年)
  - 《[今生情與恨](../Page/今生情與恨.md "wikilink")》 (1996年)
  - 《Bad Manners》 (1997年)
  - 《[鐵面特警隊](../Page/鐵面特警隊.md "wikilink")》 (1997年)
  - 《The Climb》 (1998年)
  - 《[一路上有你](../Page/一路上有你.md "wikilink")》 (1998年)
  - 《[仲夏夜之夢](../Page/仲夏夜之夢.md "wikilink")》 (1999年)
  - 《[叢林地獄](../Page/叢林地獄.md "wikilink")》 (1999年)
  - 《[心靈控訴](../Page/心靈控訴.md "wikilink")》 (1999年)
  - 《[捍衛自由](../Page/捍衛自由.md "wikilink")》
    (2000年)([TV](../Page/TV.md "wikilink"))
  - 《Harrison's Flowers》 (2000年)
  - 《Ball in the House》 (2001年)
  - 《[藍色蘿莉塔](../Page/藍色蘿莉塔.md "wikilink")》 (2002年)
  - 《Lathe of Heaven》 (2002年)([TV](../Page/TV.md "wikilink"))
  - 《Master Spy: The Robert Hanssen Story》
    (2002年)([TV](../Page/TV.md "wikilink"))
  - 《[非常命案](../Page/非常命案.md "wikilink")》 (2004年)
  - 《The Notorious Bettie Page》 (2005年)
  - 《Missing in America》 (2005年)
  - 《[晚安，祝你好運](../Page/晚安，祝你好運.md "wikilink")》 (2005年)
      - 提名[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")
      - 提名[金球獎最佳戲劇類電影男主角](../Page/金球獎最佳戲劇類電影男主角.md "wikilink")
      - 提名[英國電影學院獎最佳男主角](../Page/英國電影學院獎最佳男主角.md "wikilink")
      - 提名[美國演員工會獎最佳男主角](../Page/美國演員工會獎最佳男主角.md "wikilink")
  - 《Heavens Fall》 (2006年)
  - 《[希望不滅](../Page/希望不滅.md "wikilink")》 (2006年)
  - 《The Sensation of Sight》 (2007年)
  - 《Steel Toes》 (2007年)
  - 《[破綻](../Page/破綻.md "wikilink")》 (2007年)
  - 《[藍莓之夜](../Page/藍莓之夜.md "wikilink")》 (2007年)
  - 《[神鬼認證：最後通牒](../Page/神鬼認證：最後通牒.md "wikilink")》 (2007年)
  - 《Hereafter》 (前製中，預定2008年)
  - 《The Loss of a Teardrop Diamond》 (2008年)
  - 《[詩吼](../Page/詩吼.md "wikilink")》（*Howl*）
  - 《[告密者](../Page/告密者_\(2010年电影\).md "wikilink")》（2010年）
  - 《[美國心風暴](../Page/美國心風暴.md "wikilink")》（2016年）

## 参考资料

## 外部連結

  -
  -
  -
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:独立精神奖获得者](../Category/独立精神奖获得者.md "wikilink")
[Category:加利福尼亚州男演员](../Category/加利福尼亚州男演员.md "wikilink")
[Category:黃金時段艾美獎迷你影集或電影最佳男配角](../Category/黃金時段艾美獎迷你影集或電影最佳男配角.md "wikilink")
[Category:威廉姆斯学院校友](../Category/威廉姆斯学院校友.md "wikilink")