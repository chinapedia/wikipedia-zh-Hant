**大衛·艾倫·萊特**（**David Allen
Wright**、），是[美國職棒大聯盟球員](../Page/美國職棒大聯盟.md "wikilink")，目前效力[紐約大都會隊](../Page/紐約大都會.md "wikilink")。

生於[維珍尼亞州](../Page/維珍尼亞州.md "wikilink")[諾福克](../Page/诺福克_\(弗吉尼亚州\).md "wikilink")。[高中就讀Hickory](../Page/高中.md "wikilink")
High
School，三次代表州際隊參加全國賽。2001年在選秀會被大聯盟隊選走。2004年7月21日首次在大聯盟亮相。2005年成為了大都會隊的主力三壘手。2006年首次入選明星賽。

2007年球季，更成為了國聯[金手套獎及](../Page/美國職棒大聯盟金手套獎.md "wikilink")[銀棒獎三壘手得主](../Page/銀棒獎.md "wikilink")。在該球季的最有價值球員選舉排名第四[1](http://sports.espn.go.com/mlb/news/story?id=3120573)。

2018年9月，萊特在新聞發佈會上宣佈他終將在過了2年3個月又22天的9月25日出傷兵，並在9月29日在大都會主場打馬林魚的比賽先發三壘手作為生涯最終戰。雖然說自己不是退休，但說以後不會再回來打球了。

## 外部連結

  -
[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:紐約大都會隊球員](../Category/紐約大都會隊球員.md "wikilink")
[Category:美國職棒大聯盟金手套獎得主](../Category/美國職棒大聯盟金手套獎得主.md "wikilink")
[Category:全明星未來之星賽球員](../Category/全明星未來之星賽球員.md "wikilink")
[Category:2013年世界棒球經典賽選手](../Category/2013年世界棒球經典賽選手.md "wikilink")
[Category:維吉尼亞州人](../Category/維吉尼亞州人.md "wikilink")