[Bnu-tower01.jpg](https://zh.wikipedia.org/wiki/File:Bnu-tower01.jpg "fig:Bnu-tower01.jpg")
[Bnu-sign01.jpg](https://zh.wikipedia.org/wiki/File:Bnu-sign01.jpg "fig:Bnu-sign01.jpg")
[大西洋銀行_高士德分行.jpg](https://zh.wikipedia.org/wiki/File:大西洋銀行_高士德分行.jpg "fig:大西洋銀行_高士德分行.jpg")
[Banco_Nacional_Ultramarino_01.jpg](https://zh.wikipedia.org/wiki/File:Banco_Nacional_Ultramarino_01.jpg "fig:Banco_Nacional_Ultramarino_01.jpg")
[Banco_Ultramar_200301.jpg](https://zh.wikipedia.org/wiki/File:Banco_Ultramar_200301.jpg "fig:Banco_Ultramar_200301.jpg")大樓外彫有[葡萄牙及其](../Page/葡萄牙.md "wikilink")[殖民地的徽章](../Page/葡萄牙殖民地.md "wikilink")。\]\]
**大西洋銀行**（，直譯為「國家海外銀行」），是[葡萄牙昔日在其](../Page/葡萄牙.md "wikilink")[殖民地設立的](../Page/葡萄牙殖民地.md "wikilink")[銀行](../Page/銀行.md "wikilink")，1864年在[里斯本建行](../Page/里斯本.md "wikilink")，具備負責推動當地[經濟發展的職能](../Page/經濟.md "wikilink")，後來同時負責發行葡萄牙海外殖民地的[鈔票](../Page/鈔票.md "wikilink")。該銀行於1865年在[安哥拉和](../Page/安哥拉.md "wikilink")[佛得角開設分行](../Page/佛得角.md "wikilink")、1868年又在[聖多美和普林西比以及](../Page/聖多美和普林西比.md "wikilink")[莫桑比克開設分行](../Page/莫桑比克.md "wikilink")。[澳門分行於](../Page/澳門.md "wikilink")1902年成立，與[幾內亞比紹同年](../Page/幾內亞比紹.md "wikilink")。[東帝汶分行則於](../Page/東帝汶.md "wikilink")1912年成立。

至1960年代，大西洋銀行為葡萄牙本土擁有最多分行網絡的銀行。1974年[四二五革命後](../Page/四二五革命.md "wikilink")，新政府奉行[國有化政策](../Page/國有化.md "wikilink")，大西洋銀行於同年被國有化。1988年，大西洋銀行被部分私有化，成為該行的最大股東。2001年3月28日，大西洋銀行和葡國儲蓄信貸銀行宣佈合併，大西洋銀行成為葡國儲蓄信貸銀行全資擁有的附屬機構；而大西洋銀行澳門分行改為在澳門註冊的本地銀行，原有名稱維持不變。

## 澳門分行

大西洋銀行於1902年在澳門成立分行，為澳門首間銀行。成立初期，中文名稱為“大西洋國海外匯理銀行”。1960年代一段短暫時期，曾改稱為“葡國海外銀行”。

大西洋銀行澳門分行於1905年開始負責[澳門元的發鈔工作](../Page/澳門元.md "wikilink")，曾經是澳門地區唯一的發鈔銀行，直到1995年有關工作與[中國銀行澳門分行共同負責為止](../Page/中國銀行澳門分行.md "wikilink")。此外，大西洋銀行澳門分行也曾是唯一的[澳門政府公共庫房出納代理銀行](../Page/澳門政府公共庫房出納代理銀行.md "wikilink")，直到2000年與中國銀行澳門分行共同擔任澳門政府公共庫房出納代理銀行為止。

大西洋銀行澳門分行總部位於[亞美打利比盧大馬路](../Page/亞美打利比盧大馬路.md "wikilink")（[新馬路](../Page/新馬路.md "wikilink")），其大樓建於1926年，於1997年擴建，原大樓外牆被保留。該行共設立20間分行，其中14間位於[澳門半島](../Page/澳門半島.md "wikilink")、4間位於[氹仔](../Page/氹仔.md "wikilink")、1間位於路環、1間位於珠海[橫琴](../Page/橫琴.md "wikilink")，並在[上海設有代表處](../Page/上海.md "wikilink")。

2017年1月18日，大西洋銀行於[中國（廣東）自由貿易試驗區](../Page/中國（廣東）自由貿易試驗區.md "wikilink")[珠海橫琴新區片區開設橫琴分行](../Page/横琴新区.md "wikilink")，成為首家於[中國內地開設分行的澳門本土銀行](../Page/中國內地.md "wikilink")。

## 外部連結

  - 澳門大西洋銀行[官方網站](https://www.bnu.com.mo/cn/Pages/BNU.aspx)／[Facebook專頁](https://www.facebook.com/BNUMacauBank/)

[Category:葡萄牙公司](../Category/葡萄牙公司.md "wikilink")
[Category:澳門銀行](../Category/澳門銀行.md "wikilink")
[Category:1864年成立的公司](../Category/1864年成立的公司.md "wikilink")