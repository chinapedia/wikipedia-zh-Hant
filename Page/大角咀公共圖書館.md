[Tai_Kok_Tsui_Municipal_Services_Building_Level_3_Library_Entrance_2014.jpg](https://zh.wikipedia.org/wiki/File:Tai_Kok_Tsui_Municipal_Services_Building_Level_3_Library_Entrance_2014.jpg "fig:Tai_Kok_Tsui_Municipal_Services_Building_Level_3_Library_Entrance_2014.jpg")
[Tai_Kok_Tsui_Public_Library_Interior_2018.jpg](https://zh.wikipedia.org/wiki/File:Tai_Kok_Tsui_Public_Library_Interior_2018.jpg "fig:Tai_Kok_Tsui_Public_Library_Interior_2018.jpg")
[Tai_Kok_Tsui_Public_Library_Children_Library_2018.jpg](https://zh.wikipedia.org/wiki/File:Tai_Kok_Tsui_Public_Library_Children_Library_2018.jpg "fig:Tai_Kok_Tsui_Public_Library_Children_Library_2018.jpg")
**大角咀公共圖書館**（英語：**Tai Kok Tsui Public
Library**）是[香港一所](../Page/香港.md "wikilink")[公共圖書館](../Page/公共圖書館.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[大角咀](../Page/大角咀.md "wikilink")，[大角咀市政大廈之內](../Page/大角咀市政大廈.md "wikilink")，屬於[小型圖書館規模](../Page/小型圖書館.md "wikilink")，由[康樂及文化事務署所管理](../Page/康樂及文化事務署.md "wikilink")，[面積約](../Page/面積.md "wikilink")670[平方米](../Page/平方米.md "wikilink")。\[1\]

## 歷史

大角咀公共圖書館原址為[鐵樹街](../Page/鐵樹街.md "wikilink")9號，因[大角咀市政大廈的建成而搬遷到現址](../Page/大角咀市政大廈.md "wikilink")，於2005年12月2日啟用。

## 設施及服務

大角咀公共圖書館的設施包括：

  - 成人圖書館
  - 兒童圖書館
  - 電腦資訊區
  - 推廣活動室
  - 報刊閱覽部

大角咀公共圖書館所提供的服務則包括:

  - 集體借閱服務
  - 還書箱服務
  - 互聯網/電子資源服務
  - 聯機公眾檢索目錄
  - 自助借書服務

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: white; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: white; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: white; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參見

  - [香港康樂及文化事務署](../Page/香港康樂及文化事務署.md "wikilink")
  - [香港公共圖書館](../Page/香港公共圖書館.md "wikilink")

## 外部連結

  - [香港公共圖書館](http://www.hkpl.gov.hk/)
  - [香港公共圖書館 -
    大角咀公共圖書館](https://web.archive.org/web/20070110122126/http://www.hkpl.gov.hk/tc_chi/locat_hour/locat_hour_ll/locat_hour_ll_kr/library_62.html)

[Category:大角咀](../Category/大角咀.md "wikilink")
[Category:香港公共圖書館](../Category/香港公共圖書館.md "wikilink")

1.