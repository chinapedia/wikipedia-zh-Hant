**大豹溪**位於[新北市](../Page/新北市.md "wikilink")[三峽區境內](../Page/三峽區.md "wikilink")，是[台灣危險河川之一](../Page/台灣.md "wikilink")\[1\]。大豹溪屬[淡水河](../Page/淡水河.md "wikilink")[水系](../Page/水系.md "wikilink")，是[大漢溪支流](../Page/大漢溪.md "wikilink")[三峽河的上游](../Page/三峽河.md "wikilink")，流域分佈於[三峽區南部](../Page/三峽區.md "wikilink")，其源流為**[熊空溪](../Page/熊空溪.md "wikilink")**。上游有[滿月圓森林遊樂區](../Page/滿月圓森林遊樂區.md "wikilink")、樂樂谷、鴛鴦谷等景點，是台灣河川與旅遊景點。大豹溪發源於江南腳山東南方約1.5公里之[三峽區](../Page/三峽區.md "wikilink")、[烏來區邊境山區](../Page/烏來區.md "wikilink")，先向西轉西南流至[樂樂谷](../Page/樂樂谷.md "wikilink")，匯集另一支流[蚋仔溪後](../Page/蚋仔溪.md "wikilink")，始稱為大豹溪。大豹溪自此往西北流經有木、插角，至湊合與西南方流來之[五寮溪會合後](../Page/五寮溪.md "wikilink")，改稱為[三峽河](../Page/三峽河.md "wikilink")。

## 歷史

[日治初期以前](../Page/日治時期.md "wikilink")，該溪一帶為[原住民](../Page/原住民.md "wikilink")[泰雅族](../Page/泰雅族.md "wikilink")[大嵙崁群](../Page/大嵙崁群.md "wikilink")[大豹社的居住地](../Page/大豹社.md "wikilink")，故名「大豹溪」\[2\]。1900年至1906年的[大豹社事件後](../Page/大豹社事件.md "wikilink")，族人被日本人逼迫集體遷居到[桃園市](../Page/桃園市.md "wikilink")[復興區詩朗](../Page/復興區_\(桃園市\).md "wikilink")、志繼一帶，始由漢人遷入取代之\[3\]。

## 大豹溪水系主要河川

  - **大豹溪**：新北市三峽區
      - [東眼溪](../Page/東眼溪.md "wikilink")
      - [蚋仔溪](../Page/蚋仔溪.md "wikilink")
          - [中坑溪](../Page/中坑溪.md "wikilink")
      - **[熊空溪](../Page/熊空溪.md "wikilink")**

## 關聯項目

  - [大豹社事件](../Page/大豹社事件.md "wikilink")

## 參考來源

<div class="references-small">

<references />

</div>

[Category:大漢溪水系](../Category/大漢溪水系.md "wikilink")
[Category:新北市河川](../Category/新北市河川.md "wikilink")
[Category:三峽區](../Category/三峽區.md "wikilink")

1.  [【中華民國海洋運動協會】遠離危險水域](http://tmsa-tw.org.tw/files/20070630105321_0.doc)

2.  [《新店市志》，開拓篇，第一章
    早期開發史](http://library.sindian.gov.tw/tw/record/page.asp?record_id=2&record_detail_id=4&page=42)
3.  [《原民228 大豹社抗日出專輯》，
    m.y.，祖靈之邦網站，2007/1/28](http://www.abohome.org.tw/modules/news/article.php?storyid=1446)