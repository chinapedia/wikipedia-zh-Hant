[Große_Fuge.jpg](https://zh.wikipedia.org/wiki/File:Große_Fuge.jpg "fig:Große_Fuge.jpg")

《**降B大調弦樂四重奏大賦格**》（[德文](../Page/德文.md "wikilink")：****），**[作品](../Page/作品号.md "wikilink")133**，是[路德维希·范·貝多芬于](../Page/路德维希·范·貝多芬.md "wikilink")1825年至1826年间创作的單樂章[弦樂四重奏](../Page/弦樂四重奏.md "wikilink")，原本打算用作[Op.130的終曲樂章](../Page/第13弦乐四重奏_\(贝多芬\).md "wikilink")。樂章以技術上難度及其內省性格（即使是貝多芬晚期作品之中）出名。乐曲创作时貝多芬已全聾。

貝多芬起初作此曲作為[第十三弦樂四重奏的結尾](../Page/第13弦樂四重奏_\(貝多芬\).md "wikilink")，但由於樂章对於當時樂手的演奏技术要求太高，聽眾亦不太能接受，因而出版者請他另寫一段新的終曲。一向不理會觀眾意見及口味的貝多芬卻同意了，於是以作品編號133出版此賦格。

大多數19世紀樂評都不看好此大賦格。[丹尼爾·梅森批評此曲使人很](../Page/丹尼爾·梅森.md "wikilink")「反感」（），而[路易斯·史波就稱它及其它貝多芬晚期作品為](../Page/路易斯·史波.md "wikilink")「無法解释和無法修改的恐怖」。但自從20世紀初，人們對其評價逐渐好轉，現在大賦格被認為是貝多芬最出色的作品之一。[斯特拉文斯基說](../Page/斯特拉文斯基.md "wikilink")：

## 分析

全曲时长约18至20分钟。

樂章開始時是24音節「序幕」（*Overtura*），介紹[賦格的兩主題之一](../Page/賦格.md "wikilink")：來自貝多芬十五號弦樂四重奏（op.132）第一樂章的旋律。接着作品突然跳進強烈的[雙賦格](../Page/雙賦格.md "wikilink")，其中第二主題有大幅度的[跳音](../Page/跳音.md "wikilink")，四支琴發出三連音、斷音及交叉旋律。

接着的賦格是一連串調、節奏、緩急互相對比的樂段。有時一節會忽然斷掉。將近尾時，音樂慢落來，時有長的停頓，接着序幕重現，帶入急速的終結。

像貝多芬其它晚期的終曲（例如第九交響曲的[歡樂頌](../Page/歡樂頌.md "wikilink")）一樣，此賦格可以當作由多段子樂章組成的大樂章。每一小節都建立於原初主題的一次[變奏上](../Page/變奏.md "wikilink")。作曲的手法亦為同期之代表：夾雜着[變奏曲式](../Page/變奏曲式.md "wikilink")、[奏鳴曲式和](../Page/奏鳴曲式.md "wikilink")[賦格曲](../Page/賦格曲.md "wikilink")。

## 聯彈版大賦格及其手稿

1826年初，出版商阿爾塔利亞在提出將大賦格從原本的弦樂四重奏作品中分拆之前，已向貝多芬指有「不少意見」希望此曲有一個鋼琴四手的改編版本（由於此曲當時並不受歡迎，因此這有可能僅是出版商的託辭），\[1\]但當時貝多芬對此並無興趣。因此阿爾塔利亞轉而邀請奧地利作曲家改編。然而貝多芬對該改編不甚滿意，於是自行譜寫一更忠於原作的版本。此版本於其第十四弦樂四重奏（作品131）後完成，阿爾塔利亞以作品134出版。\[2\]

2005年10月13日，有報導称[賓夕凡尼亞州](../Page/賓夕凡尼亞州.md "wikilink")的，一名圖書館管理員找到鑑證過的貝多芬1826年手稿，標題為「Grosse
Fuge (a piano four-hands version of the op. 133 [string
quartet](../Page/弦樂四重奏.md "wikilink")
finale)」\[3\]\[4\]。此篇樂章即是貝多芬的鋼琴四手聯彈改編，作品134，已經失蹤了115年。2005年12月1日，[蘇富比拍賣此手稿](../Page/蘇富比.md "wikilink")，以112万[英鎊](../Page/ISO_4217.md "wikilink")（合195万美元）賣了給一名不知名的人。後來此人於2006年2月将此手稿及其它139份罕本原稿送給了紐約朱利亞音樂學院，大家這才知道他是亿万富翁。

此手稿曾出现在1890年[柏林的一次拍卖会上](../Page/柏林.md "wikilink")，拍賣給了俄亥俄[辛辛那提的一個企业家](../Page/辛辛那提.md "wikilink")。1952年，其女兒将該手稿及其他音乐作品（包括莫扎特的幻想曲）送給了賓州[費城的一所教堂](../Page/費城.md "wikilink")。其后那所圖書館不知道用什么方式得到了此份手稿。

## 大賦格與大眾文化

值得注意的是，这篇作品在大眾文化中得到了回響。[P·D·Q·巴哈寫了篇](../Page/P·D·Q·巴哈.md "wikilink")「最大賦格」（*Grossest
Fugue*）。[金·史丹利·羅賓森寫了一本小說](../Page/金·史丹利·羅賓森.md "wikilink")《Fifty Degrees
Below》（New York: Bantam Books,
2005）說全球暖化，其中一幕，有一個人一邊在發了瘋似的吸塵，一邊将不同的房间中的麥克風聲響開至震耳欲聋，播放大賦格及「[槌子键琴奏鳴曲](../Page/第29钢琴奏鸣曲_\(贝多芬\).md "wikilink")」（*Hammerklavier*）的賦格樂章。

## 參考資料

  - *The Beethoven Quartet Companion*, edited by Robert Winter and
    Robert Martin (1994: University of California Press) is an excellent
    reference for information on Beethoven's quartets.
  - *Beethoven's Quartets* by Joseph de Marliave (originally published
    in 1928; republished in 1961 by Dover Press).
  - [約瑟夫·克爾曼](../Page/約瑟夫·克爾曼.md "wikilink")（Joseph Kerman），*The
    Beethoven Quartets*. New York, W.W. Norton & Co., 1966. ISBN
    0393009092

## 外部連結

  - [樂譜](http://imslp.ca/images/2/2b/Op133-Grosse_Fuge.pdf)
  - [亞歷士·羅斯](../Page/亞歷士·羅斯.md "wikilink")，["Great Fugue: Secrets of a
    Beethoven
    manuscript,"](http://www.newyorker.com/printables/critics/060206crmu_music)
    《[紐約客](../Page/紐約客.md "wikilink")》, 2006-02-06.

[Category:贝多芬弦乐四重奏](../Category/贝多芬弦乐四重奏.md "wikilink")

1.
2.
3.  <http://www.nytimes.com/2005/10/13/arts/music/13beet.html>
4.  <http://www.cbc.ca/arts/story/2005/10/13/beethoven_051013.html>