**大足**（701年正月—十月）是[武則天的年号](../Page/武則天.md "wikilink")，共计10个月。

## 大事记

  - 久视二年正月初三丁丑（[701年](../Page/701年.md "wikilink")[2月15日](../Page/2月15日.md "wikilink")），改元大足。
  - 大足元年十月廿二辛酉（701年[11月26日](../Page/11月26日.md "wikilink")），改元长安。

## 出生

## 逝世

  - [李重润](../Page/李重润.md "wikilink")

## 纪年

| 大足                               | 元年                                 |
| -------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [701年](../Page/701年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink")     |

## 參看

  - [大足](../Page/大足.md "wikilink")
  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [大寳](../Page/大寶_\(文武天皇\).md "wikilink")（[701年三月二十一日](../Page/701年.md "wikilink")—[704年五月十日](../Page/704年.md "wikilink")）：[飛鳥時代](../Page/飛鳥時代.md "wikilink")[文武天皇年號](../Page/文武天皇.md "wikilink")

[Category:武周年号](../Category/武周年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:700年代中国](../Category/700年代中国.md "wikilink")
[Category:701年](../Category/701年.md "wikilink")