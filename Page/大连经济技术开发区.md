**大连经济技术开发区**（简称**大连开发区**）是1984年9月经[国务院批准设立的第一个](../Page/国务院.md "wikilink")[国家级开发区](../Page/国家级经济技术开发区.md "wikilink")，也是目前中国开发面积最大的经济技术开发区。现已开发50平方公里，该区已有外商投资企业1500余家，日商为主。地处大连[金州区南部](../Page/金州区.md "wikilink")，西北为金州城区，南滨大连湾，东临[金石滩](../Page/金石滩.md "wikilink")。划为开发区前，这里仅仅是一片农村，包括马桥子、小孤山、湾里、王官寨等村落。开发区与大连市区之间有城市快速路振兴路和振连路相连。[快轨三号线东西穿过本区](../Page/大连轨道交通#快轨三号线.md "wikilink")，设金马路、开发区（五彩城）、保税区、双D港车站。区内主干道有金马路、辽河路、东北大街、辽宁街等。道路多以[中国东北地区城市命名](../Page/中国东北地区.md "wikilink")。管委会周边为繁华地段。

[大连新港位于开发区以南的大孤山半岛](../Page/大连新港.md "wikilink")。

区内企业有[英特尔](../Page/英特尔.md "wikilink")、[佳能](../Page/佳能.md "wikilink")、[三菱](../Page/三菱.md "wikilink")、[三洋](../Page/三洋.md "wikilink")、[东芝](../Page/东芝.md "wikilink")、[西太平洋石化](../Page/西太平洋石化.md "wikilink")、、[一汽](../Page/一汽.md "wikilink")[大连客车厂](../Page/大连客车厂.md "wikilink")、大柴[道依茨等](../Page/道依茨.md "wikilink")。还有[大连民族学院](../Page/大连民族学院.md "wikilink")、[大连大学等高等院校](../Page/大连大学.md "wikilink")。

## 参见

  - [国家级经济技术开发区](../Page/国家级经济技术开发区.md "wikilink")

## 外部链接

  - [大连开发区官方网站](http://www.dda.gov.cn)

[Category:国家级经济技术开发区](../Category/国家级经济技术开发区.md "wikilink")
[Category:大连经济](../Category/大连经济.md "wikilink")
[Category:大连](../Category/大连.md "wikilink")
[Category:金州区](../Category/金州区.md "wikilink")
[Category:1984年建立](../Category/1984年建立.md "wikilink")