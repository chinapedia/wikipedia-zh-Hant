**大野市**（）是[福井縣位于東部的](../Page/福井縣.md "wikilink")[市](../Page/市.md "wikilink")。為縣內最大的市町村，占該縣面積的約五分之一。面積872.30平方公里，總人口37,844人。

## 外部連結

  - [大野市](http://www.city.fukui-ono.lg.jp/)

[Ono-shi](../Category/福井縣的市町村.md "wikilink")