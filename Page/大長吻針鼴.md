**大長吻針鼴**（[學名](../Page/學名.md "wikilink")：**）是在[新畿內亞的一](../Page/新畿內亞.md "wikilink")[種](../Page/種.md "wikilink")[原針鼴屬](../Page/原針鼴屬.md "wikilink")。牠們主要分佈在[巴布亞新畿內亞](../Page/巴布亞新畿內亞.md "wikilink")[海拔](../Page/海拔.md "wikilink")2000-3000米的地區。

大長吻針鼴的前肢有五趾，後肢則有四趾，趾上均有爪。牠重5-10公斤，體長60-100厘米，是[單孔目中體型最大的](../Page/單孔目.md "wikilink")。牠有濃密的黑毛，沒有尾巴及行動緩慢。牠們受到攻擊時會捲曲身體來防禦。

大長吻針鼴共有四個[亞種](../Page/亞種.md "wikilink")：

  - *Z. b. bartoni*
  - *Z. b. clunius*
  - *Z. b. smeenki*
  - *Z. b. diamondi*

每一個亞種都在[地理上分隔的](../Page/地理.md "wikilink")，牠們之間主要在體型上有分別。

## 參考

  - Flannery, T.F. and Groves, C.P. 1998 A revision of the genus
    *Zaglossus* (Monotremata, Tachyglossidae), with description of new
    species and subspecies. *Mammalia*, 62(3): 367-396

  -
[Category:原針鼴屬](../Category/原針鼴屬.md "wikilink")