**大阪城**，位於[日本](../Page/日本.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[中央區](../Page/中央區_\(大阪市\).md "wikilink")（古屬[攝津國](../Page/攝津國.md "wikilink")[東成郡](../Page/東成郡.md "wikilink")）的[大阪城公園內](../Page/大阪城公園.md "wikilink")，為大阪名勝之一，和[名古屋城](../Page/名古屋城.md "wikilink")、[熊本城並列日本歷史上的](../Page/熊本城.md "wikilink")[三名城](../Page/日本三名城.md "wikilink")，別名「金城」或「錦城」。

在[天下統一的](../Page/統一.md "wikilink")[桃山時代是](../Page/桃山時代.md "wikilink")[豐臣秀吉的居城](../Page/豐臣秀吉.md "wikilink")、[豐臣政權的中心](../Page/豐臣政權.md "wikilink")。後來[德川家康以兩次](../Page/德川家康.md "wikilink")[大坂之役](../Page/大坂之役.md "wikilink")（冬之陣、夏之陣）消滅了[豐臣家](../Page/豐臣家.md "wikilink")，此後大坂城成為[江戶幕府控制西日本](../Page/江戶幕府.md "wikilink")[大名的重要據點](../Page/大名_\(稱謂\).md "wikilink")。

[城郭含](../Page/城郭.md "wikilink")[城下町等周長約](../Page/城下町.md "wikilink")7.8公里，與[江戶城初期](../Page/江戶城.md "wikilink")（內郭）相當（江戶後期含城下町外郭周長約15.8ｋｍ）。在豐臣時代，大坂城曾經是全日本規模最大的城郭。

## 概要

[Osaka_Castle_Nishinomaru_Garden_April_2005.JPG](https://zh.wikipedia.org/wiki/File:Osaka_Castle_Nishinomaru_Garden_April_2005.JPG "fig:Osaka_Castle_Nishinomaru_Garden_April_2005.JPG")

大阪城矗立於[上町台地北端](../Page/上町台地.md "wikilink")，北臨[-{淀}-川](../Page/淀川.md "wikilink")，居交通要津，最早為[羽柴秀吉在大抵統一日本後所建](../Page/羽柴秀吉.md "wikilink")，規模宏偉、金碧輝煌。曾多次毀於天災兵禍又重修改建，今日之大阪城為[昭和年間以](../Page/昭和.md "wikilink")[鋼骨](../Page/鋼骨.md "wikilink")[鋼筋](../Page/鋼筋.md "wikilink")[混凝土復築](../Page/混凝土.md "wikilink")，1997年日本政府指定為[登錄有形文化財](../Page/登錄有形文化財.md "wikilink")。
[大阪城_2018-7-30.jpg](https://zh.wikipedia.org/wiki/File:大阪城_2018-7-30.jpg "fig:大阪城_2018-7-30.jpg")

「大阪」在[明治維新前寫作](../Page/明治維新.md "wikilink")「大**坂**」，維新後忌於「坂」字可拆為「士反」，有「武士叛亂」之諱，因此於[明治三年](../Page/明治.md "wikilink")（1870年）改名為「大**阪**」，「大**坂**城」也因而更名為「大**阪**城」。一般講述更名前的歷史時仍會以舊寫「大**坂**城」稱之，以示時代區別。

## 歷史

### 豐臣時代

[Osaka_Castle_Keep_tower_of_「A_figure_of_camp_screen_of_the_Osaka_summer」.jpg](https://zh.wikipedia.org/wiki/File:Osaka_Castle_Keep_tower_of_「A_figure_of_camp_screen_of_the_Osaka_summer」.jpg "fig:Osaka_Castle_Keep_tower_of_「A_figure_of_camp_screen_of_the_Osaka_summer」.jpg")
1583年，豐臣秀吉在[石山本願寺的原根據地上建造大坂城](../Page/石山本願寺.md "wikilink")，以一年半左右的時間完成了本丸（主郭）。直到秀吉病逝前，仍持續不斷地建設二之丸及三之丸等附郭，以及多重水堀（[護城河](../Page/護城河.md "wikilink")）和運河等防禦設施。大坂城[天守外觀四層或五層](../Page/天守.md "wikilink")，瓦上覆以[金箔](../Page/金箔.md "wikilink")，極其奢華，位置在今日大阪公園蓄水池付近，含[石垣四十公尺高](../Page/石垣.md "wikilink")，[德川時代之新天守則約為](../Page/德川時代.md "wikilink")58公尺。秀吉於大坂城內設有一間活動式的「」，茶室的牆壁、天花板、地板甚至連門的骨架皆由[黃金打造](../Page/黃金.md "wikilink")。秀吉位於奧御殿的寢宮同樣奢華至極，棉被的材料為鮮紅色高級布料，床頭以黃金雕刻作裝飾。

豐臣秀吉後來赴[京任職](../Page/京都.md "wikilink")[關白](../Page/關白.md "wikilink")，次第建造了[京都的](../Page/京都.md "wikilink")[聚樂第和](../Page/聚樂第.md "wikilink")[伏見城](../Page/伏見城.md "wikilink")，作為居住和處理政務的中心，較少住在大坂城的奧御殿。1598年秀吉病逝後，豐臣政權第二代天下人[豐臣秀賴又從伏見城移居到已完成的大坂城奧御殿](../Page/豐臣秀賴.md "wikilink")。此外，實際負責政務的豐臣家大老[德川家康](../Page/德川家康.md "wikilink")，也入住西之丸便於辦公。

### 江戶時代

[Osaka_castle_Otemon_and_Sengann-yagura.jpg](https://zh.wikipedia.org/wiki/File:Osaka_castle_Otemon_and_Sengann-yagura.jpg "fig:Osaka_castle_Otemon_and_Sengann-yagura.jpg")
1603年（[慶長八年](../Page/慶長.md "wikilink")）[江戶幕府成立後](../Page/江戶幕府.md "wikilink")，豐臣秀賴依然居住在大坂城支配[攝津國](../Page/攝津國.md "wikilink")。1614年（慶長十九年）[德川家和](../Page/德川家.md "wikilink")[豐臣家矛盾激烈化](../Page/豐臣家.md "wikilink")，德川家康率大軍包圍大坂城，史稱[大坂冬之陣](../Page/大坂冬之陣.md "wikilink")。戰役中雙方講和，德川家提出條件要求拆毀二之丸和三之丸，填平外圍的水堀，使大坂城成為一座僅有內堀和本丸的裸城。豐臣家雖然一時接受了這個屈辱的條件，但後來毀約重新開挖水堀，讓德川家康得到藉口於次年發動[大坂夏之陣](../Page/大坂夏之陣.md "wikilink")，消滅了豐臣家，而大坂城也在此役中化為灰燼。

江戶幕府控有大坂城後，家康一度把該城封給外孫松平忠明，但1619年（[元和五年](../Page/元和.md "wikilink")）又收歸[幕府直轄](../Page/天領.md "wikilink")。1620年（元和六年）二代將軍[德川秀忠開始重建大坂城](../Page/德川秀忠.md "wikilink")，並在1629年（[寬永六年](../Page/寬永.md "wikilink")）完成。

德川家將豐臣家所建的大坂城石牆和水堀全部破壞，覆以數公尺厚的土，再興建更高的石牆，把豐臣家大坂城的遺跡全部埋在地底。天守設置在不同的地點，位置更高，並採用完全不同的設計。德川家用全新且更為雄壯的城郭，將豐臣家留給世人的記憶徹底埋葬，並誇耀幕府統治全日本的威信更甚於豐臣家。

作為德川幕府的直轄城，歷代將軍都由自己兼任大坂城的城主，並且從重要的[譜代大名](../Page/譜代大名.md "wikilink")（德川家直系老臣）中選派[大坂城代](../Page/大坂城代.md "wikilink")（代理城主）。

1665年（[寬文五年](../Page/寬文.md "wikilink")）大坂城天守被閃電擊中燒毀，並未重建，自此成為沒有天守的城池。

江戶末期，1866年第二次[長州征伐](../Page/長州征伐.md "wikilink")，將軍[德川家茂進駐大坂城](../Page/德川家茂.md "wikilink")，卻於不久後病歿於大坂城。1868年（[明治元年](../Page/明治.md "wikilink")）1月3日[明治天皇頒佈](../Page/明治天皇.md "wikilink")[王政復古的大號令](../Page/王政復古.md "wikilink")，撤除幕府，由天皇親政。前將軍[德川慶喜改以大坂城為居城](../Page/德川慶喜.md "wikilink")。但隨即發生了舊幕府軍反抗維新政府的[鳥羽伏見之戰](../Page/鳥羽伏見之戰.md "wikilink")，舊幕府軍失利，德川慶喜倉皇逃回江戶城，大坂城也在此役中遭到兵火幾近全毀。

### 近代

[Osaka_Castel_tower_reconstruction_1930.jpg](https://zh.wikipedia.org/wiki/File:Osaka_Castel_tower_reconstruction_1930.jpg "fig:Osaka_Castel_tower_reconstruction_1930.jpg")

[明治時代](../Page/明治.md "wikilink")，將大坂城劃為陸軍基地，並在東側廣大的空地上興建[大阪砲兵工廠](../Page/大阪砲兵工廠.md "wikilink")。因此在[第二次世界大戰時成為美軍空襲的目標](../Page/第二次世界大戰.md "wikilink")。

1928年（[昭和三年](../Page/昭和.md "wikilink")），大阪市市長關一提議重建大阪城，並在半年內募得市民捐款150萬元。

1930年（昭和五年），天守動工，以[鋼筋](../Page/鋼筋.md "wikilink")[水泥復築](../Page/水泥.md "wikilink")，隔年完成。這座復興的天守並未忠實複製豐臣或德川時代，基本上以德川時代的白漆風格為主，但最上面一層重現豐臣時代的黑漆描金風格。

第二次世界大戰期間，大阪城四周受到美軍猛烈空襲，雖然復興天守此次僥倖無事，但先前逃過戰禍的週邊建築卻劫數難逃。日本敗戰後大阪城被美軍接收，1948年時美軍不慎引發大火，又失去了紀州御殿。1950年代開始正式的復修及學術調查。1959年發現了豐臣時代的遺跡。大阪城現今為大阪城公園。1997年日本政府指定為登錄有形文化財。

## 外部連結

  - [大阪城天守閣博物館](http://www.museum.or.jp/osakajo/)
  - [大阪城天守閣](http://www.osakacastle.net/)
  - [大阪城導覽](https://web.archive.org/web/20060811000400/http://www.tourism.city.osaka.jp/ja/castle/history/index.htm)

## 參考文獻

[Inside_Osaka_Castle.JPG](https://zh.wikipedia.org/wiki/File:Inside_Osaka_Castle.JPG "fig:Inside_Osaka_Castle.JPG")

  - 岡本良一『大坂城』岩波書店、1970年
  - 『大坂城』学習研究社、1994年
  - 『復元大系日本の城5近畿』ぎょうせい、1992年
  - 中村博司『天下統一の城　大坂城』新泉社、2008年
  - 『芦屋市文化財調査報告第60集　徳川大坂城東六甲採石場Ⅳ　岩ヶ平石切丁場跡』芦屋市教育委員会、2005年
  - 朱和之，2007，〈日本的古城〉，《印刻文學生活誌》49期。

[Osaka](../Category/大阪府城堡.md "wikilink")
[Category:日本宮殿](../Category/日本宮殿.md "wikilink")
[Category:日本地標](../Category/日本地標.md "wikilink")
[Category:安土桃山時代建築](../Category/安土桃山時代建築.md "wikilink")
[Category:大阪府重要文化財](../Category/大阪府重要文化財.md "wikilink")
[Category:特别史跡](../Category/特别史跡.md "wikilink")
[Category:豐臣秀吉](../Category/豐臣秀吉.md "wikilink")
[Category:豐臣氏](../Category/豐臣氏.md "wikilink")
[Category:德川氏](../Category/德川氏.md "wikilink")
[Category:平成百景](../Category/平成百景.md "wikilink")
[Category:攝津國](../Category/攝津國.md "wikilink")
[Category:大阪市建築物](../Category/大阪市建築物.md "wikilink")
[Category:登錄有形文化財](../Category/登錄有形文化財.md "wikilink") [Category:中央區
(大阪市)](../Category/中央區_\(大阪市\).md "wikilink")