**大阪城北詰站**（）是一個位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[都島區網島町](../Page/都島區.md "wikilink")，屬於[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[JR東西線的](../Page/JR東西線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。車站編號是**JR-H42**。車站象徵是[豐臣秀吉的](../Page/豐臣秀吉.md "wikilink")[馬印](../Page/馬印.md "wikilink")「[葫蘆](../Page/葫蘆.md "wikilink")」。

## 車站構造

[島式月台](../Page/島式月台.md "wikilink")1面2線的[地底車站](../Page/地底車站.md "wikilink")。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>JR東西線</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/北新地站.md" title="wikilink">北新地</a>、<a href="../Page/尼崎站_(JR西日本).md" title="wikilink">尼崎方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/京橋站_(大阪府).md" title="wikilink">京橋</a>、、方向</p></td>
<td></td>
</tr>
</tbody>
</table>

## 歷史

  - 1997年3月18日：車站開業。

## 相鄰車站

  - 西日本旅客鐵道

    JR東西線

      -

        快速、直通快速、區間快速、普通（JR東西線內各站停車）

          -
            [京橋](../Page/京橋站_\(大阪府\).md "wikilink")（JR-H41）－**大阪城北詰（JR-H42）**－[大阪天滿宮](../Page/大阪天滿宮站.md "wikilink")（JR-H43）

## 關連條目

  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

## 外部連結

  - [JR西日本 大阪城北詰站](http://www.jr-odekake.net/eki/top.php?id=0612301)

[Osakajoukitazume](../Category/日本鐵路車站_O.md "wikilink")
[Category:JR東西線車站](../Category/JR東西線車站.md "wikilink")
[Category:都島區鐵路車站](../Category/都島區鐵路車站.md "wikilink")
[Category:1997年啟用的鐵路車站](../Category/1997年啟用的鐵路車站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")
[Category:大阪城](../Category/大阪城.md "wikilink")