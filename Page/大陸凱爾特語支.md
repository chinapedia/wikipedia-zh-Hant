**大陸凱爾特語支**之下歸納了那些非屬於[蓋爾亞支或](../Page/蓋爾亞支.md "wikilink")[布立吞亞支的](../Page/布立吞亞支.md "wikilink")[凱爾特語族語言](../Page/凱爾特語族.md "wikilink")。雖然在前[羅馬帝國時期](../Page/羅馬帝國.md "wikilink")，[凱爾特人曾在歐洲大陸上說著數十種凱爾特語言](../Page/凱爾特人.md "wikilink")，但在歐陸使用的凱爾特語言之中也只有四種有真正得到考證證實其存在：

  - [勒龐蒂語](../Page/勒龐蒂語.md "wikilink")（[南阿爾卑高盧語](../Page/南阿爾卑高盧語.md "wikilink")，西元前7世紀到西元前3世紀）
  - [高盧語](../Page/高盧語.md "wikilink")（西元前3世紀到西元2世紀）
  - [加拉提亞語](../Page/加拉提亞語.md "wikilink")（西元前3世紀到西元4世紀）
  - [凱爾特伊比利亞語](../Page/凱爾特伊比利亞語.md "wikilink")（西元前1世紀）

以下兩種語言以可能歸入：

  - [盧西塔尼亞語](../Page/盧西塔尼亞語.md "wikilink")（Lusitanian，西元2世紀前）()，現列為未歸類的印歐語言
  - [諾里語](../Page/諾里語.md "wikilink")（西元前4世紀到西元1世紀或2世紀），為中歐和東歐的凱爾特方言

一般認為南阿爾卑高盧語只是高盧語的一種方言，而加拉提亞語可能也是。

「大陸凱爾特語支」一詞乃是相對於[海島凱爾特語支而言](../Page/海島凱爾特語支.md "wikilink")。當多數的研究學者都同意海島凱爾特語支是一支獨立發展的凱爾特語言，並且擁有共同的[語言學演變特徵的同時](../Page/語言學.md "wikilink")，我們卻沒有足夠的證據將大陸凱爾特語支諸語歸納為相似的一個語群。因此「大陸凱爾特語支」只是一個[駢系](../Page/駢系.md "wikilink")，用來指非海島凱爾特語支的凱爾特諸語。而一方面，因為我們鮮少發現大陸凱爾特諸語的語料被保存下來，也因此[歷史語言學中的](../Page/歷史語言學.md "wikilink")[比較法分析一直難以在這個領域表現使用](../Page/比較法.md "wikilink")。

大陸凱爾特語支的譜系如下：

**大陸凱爾特語支**

  - [高盧語](../Page/高盧語.md "wikilink")
      - [南阿爾卑高盧語](../Page/南阿爾卑高盧語.md "wikilink")（勒龐蒂語，Lepontic）()
      - [山南高盧語](../Page/山南高盧語.md "wikilink")（Cisalpine Gaulish）()
      - [山外高盧語](../Page/山外高盧語.md "wikilink")（Transalpine Gaulish）()
      - [加拉提亞語](../Page/加拉提亞語.md "wikilink")（Galatian）()
  - [諾里語](../Page/諾里語.md "wikilink")（Noric）()
  - [凱爾特伊比利亞語](../Page/凱爾特伊比利亞語.md "wikilink") ()

## 參考文獻

  - Ball M and Fife J (1993). The Celtic Languages.

  -
  -
  -
  -
  -
  - Stifter, David (2008), *Old Celtic 2008 (classroom material)*,
    [1](https://web.archive.org/web/20080705092001/http://www.univie.ac.at/indogermanistik/dateien.cgi?download)

{{-}}

[Category:凱爾特語族](../Category/凱爾特語族.md "wikilink")
[大陸凱爾特語支](../Category/大陸凱爾特語支.md "wikilink")