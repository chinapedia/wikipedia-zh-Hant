**大隅半島**（おおすみはんとう）為[日本](../Page/日本.md "wikilink")[九州岛的一個](../Page/九州岛.md "wikilink")[半島](../Page/半島.md "wikilink")。也是九州的最南端。西岸隔[鹿兒島灣](../Page/鹿兒島灣.md "wikilink")（錦江灣）與[薩摩半島相對](../Page/薩摩半島.md "wikilink")，東岸為[太平洋](../Page/太平洋.md "wikilink")。南端為[佐多岬](../Page/佐多岬.md "wikilink")。

## 行政区域

  - 鹿兒島縣
      - [鹿兒島市一部份](../Page/鹿兒島市.md "wikilink")（[櫻島](../Page/櫻島.md "wikilink")）
      - [鹿屋市](../Page/鹿屋市.md "wikilink")
      - [垂水市](../Page/垂水市.md "wikilink")
      - [曾於市](../Page/曾於市.md "wikilink")
      - [志布志市](../Page/志布志市.md "wikilink")
      - [曾於郡](../Page/曾於郡.md "wikilink")[大崎町](../Page/大崎町.md "wikilink")
      - [肝屬郡](../Page/肝屬郡.md "wikilink")[東串良町](../Page/東串良町.md "wikilink")、[肝付町](../Page/肝付町.md "wikilink")、[錦江町](../Page/錦江町.md "wikilink")、[南大隅町](../Page/南大隅町.md "wikilink")
  - 宮崎縣
      - [串間市](../Page/串間市.md "wikilink")
      - [日南市](../Page/日南市.md "wikilink")

## 相關

  - [大隅號](../Page/大隅號.md "wikilink")：日本於1970年2月11日發射升空的日本最初製造的人造衛星，得名於發射基地所在地的此處。

## 參考文獻

  - 町田洋他編 『日本の地形 7 九州・南西諸島』 東京大学出版会、2001年、口絵、193-199頁。 ISBN
    4-13-064717-2

  - \- 宮崎県・鹿児島県、2005年12月。

## 外部連結

  - [Description of
    Osumi](http://www.kagoshima-kankou.com/for/areaguides/osumi.html)

[Category:日本半岛](../Category/日本半岛.md "wikilink")
[Category:鹿兒島市](../Category/鹿兒島市.md "wikilink")
[Category:鹿兒島縣地理](../Category/鹿兒島縣地理.md "wikilink")
[Category:鹿屋市](../Category/鹿屋市.md "wikilink")
[Category:垂水市](../Category/垂水市.md "wikilink")
[Category:曾於市](../Category/曾於市.md "wikilink")
[Category:志布志市](../Category/志布志市.md "wikilink")
[Category:大崎町](../Category/大崎町.md "wikilink")
[Category:東串良町](../Category/東串良町.md "wikilink")
[Category:肝付町](../Category/肝付町.md "wikilink")
[Category:錦江町](../Category/錦江町.md "wikilink")
[Category:南大隅町](../Category/南大隅町.md "wikilink")
[Category:串間市](../Category/串間市.md "wikilink")
[Category:日南市](../Category/日南市.md "wikilink")