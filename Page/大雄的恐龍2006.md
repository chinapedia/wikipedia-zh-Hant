是2006年3月4日公映的第26部[哆啦A夢電影作品](../Page/哆啦A夢電影作品.md "wikilink")（水田版系列的第1作和原作漫畫誕生35週年的紀念作品）。本作品改編自《[大雄的恐龍](../Page/大雄的恐龍.md "wikilink")》，但有不少地方與原作有出入（例如結局部份）。另外在特別版中附帶了渡邊步繪制的繪本《[大雄的恐龍](../Page/大雄的恐龍.md "wikilink")》。另外還發行了《哆啦A夢‧大雄的恐龍2006
DS》。此外岡田康則也繪制了漫畫《哆啦A夢〈大雄的恐龍2006
DS〉原創喜劇》。該作品是首部重製作品，票房收入達32.8億日圓，而且作品中的傷感主義也獲得了一些好評。被認為是藤子去世后較好的大長篇作品。该作品获得了第1回Invitation
AWARDS動畫大獎（《[穿越時空的少女](../Page/穿越時空的少女.md "wikilink")》同時受賞）。台灣已在2007年取得代理權，命名為《**新·-{}-大雄的恐龍**》，由堡壘數位取得。

## 配音員

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th style="text-align: center;"><p>配音員</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日本</p></td>
<td style="text-align: center;"><p>台灣</p></td>
</tr>
<tr class="even">
<td><p><strong>哆啦A梦</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/水田山葵.md" title="wikilink">水田山葵</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>大雄</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/大原惠.md" title="wikilink">大原惠</a></p></td>
</tr>
<tr class="even">
<td><p><strong>靜香</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/嘉數由美.md" title="wikilink">嘉數由美</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>胖虎</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/木村昴.md" title="wikilink">木村昴</a></p></td>
</tr>
<tr class="even">
<td><p><strong>小夫</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/關智一.md" title="wikilink">關智一</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>皮皮</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/神木隆之介.md" title="wikilink">神木隆之介</a></p></td>
</tr>
<tr class="even">
<td><p><strong>黑衣人</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/船越英一郎.md" title="wikilink">船越英一郎</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>德爾曼斯坦</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/內海賢二.md" title="wikilink">內海賢二</a></p></td>
</tr>
</tbody>
</table>

## 人物介紹

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th><p>配音員</p></th>
<th><p>介紹</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日本</p></td>
<td><p>台灣</p></td>
<td><p>香港</p></td>
</tr>
<tr class="even">
<td><p><strong>皮皮(嗶之助)</strong></p></td>
<td><p><a href="../Page/神木隆之介.md" title="wikilink">神木隆之介</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>崖下</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>時空警察</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 本片敵人

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th><p>配音員</p></th>
<th><p>介紹</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日本</p></td>
<td><p>台灣</p></td>
<td><p>香港</p></td>
</tr>
<tr class="even">
<td><p><strong>黒衣人</strong></p></td>
<td><p><a href="../Page/船越英一郎.md" title="wikilink">船越英一郎</a></p></td>
<td><p><a href="../Page/陳宏瑋.md" title="wikilink">陳宏瑋</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>德尔曼斯坦</strong></p></td>
<td><p><a href="../Page/内海賢二.md" title="wikilink">内海賢二</a></p></td>
<td><p><a href="../Page/于正昌.md" title="wikilink">于正昌</a></p></td>
</tr>
</tbody>
</table>

## 工作人員

本片由出身於[吉卜力工作室的小西賢一擔任美術指導](../Page/吉卜力工作室.md "wikilink")。而森久司、松本憲生、橋本晉治等著名動畫師也加入製作團隊中，各自繪出的風格也出現在本片中。

美術背景則以美術指導西田稔以寫實方式描繪，配合木船德光率領的IKIF+視覺特技公司所製作的視覺特技而顯得在背景繪畫時隨心所欲。

本片製作初期，小學館製作株式會社參加了本片的出資及製作。

  - 原作：[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")
  - 總導演：楠葉宏三
  - 導演、故事圖 (storyboard) 繪製：[渡邊步](../Page/渡邊步.md "wikilink")
  - 編劇：渡邊步、楠葉宏三
  - 演出（導演助理）：宮下新平
  - 繪畫指導：小西賢一
  - 動畫檢查：大野順子、山西晃嗣、澤田裕美、八木郁乃
  - [色彩設計](../Page/色彩設計.md "wikilink")：松谷早苗、崛越智子
  - [色彩檢查](../Page/色彩設計.md "wikilink")：吉田春繪
  - [仕上檢查](../Page/色彩設計.md "wikilink")：今泉ひろみ
  - [仕上擔當](../Page/色彩設計.md "wikilink")：野中幸子
  - 特別效果攝影：千葉豐
  - 美術指導：西田稔
  - 視覺效果指導：木船德光
  - 視覺效果製作：IKIF+
  - 攝影指導：熊谷正弘 (Anime Film)
  - 剪接：[岡安肇](../Page/岡安肇.md "wikilink")
    ([岡安Promotion](../Page/岡安Promotion.md "wikilink"))
  - 剪接數據管理：三宅圭貴
  - 錄音指導：田中章喜
  - 動作音效設計：糸川幸良 (Group And I)
  - 音效後期製作：[AUDIO PLANNING
    U株式會社](../Page/AUDIO_PLANNING_U.md "wikilink")
  - 音響製作辦理：中村友子、佐佐木愛
  - 錄音室：[APU MEGURO STUDIO](../Page/APU_MEGURO_STUDIO.md "wikilink")
  - 混音師：田口信孝
  - 助理混音：村越直
  - 對話剪接：山本壽、內山敬章、金子俊也
  - 原創音樂：澤田完
  - 音響工程師 (Sound engineer)：北川照明
  - 助理音響工程師：奈良美幸
  - 底片沖印：株式會社東京現像所
  - 底片調光師 (Color timing)：吉川富雄
  - 底片輸出技術員：增田悅史
  - 沖印室聯絡 (Lab. coordinator)：大渕智一
  - 沖印室管理 (Lab. management)：加藤善仁
  - 附加動畫繪畫：麥原伸太郎
  - 附加動畫製作：Time Machine 株式會社（）
  - 原畫製作：別紙直樹
  - 制作事務：杉野友紀、服部高弘、宮澤英太郎
  - 製作辦理：外崎真、山崎智史
  - 監製：小倉久美、吉川大祐、山崎立士
  - 執行監製（出品人）：増子相二郎、杉山登
  - 動畫製作：SHIN-EI動畫株式會社
  - 動畫製作協力：Vega Entertainment () （松土隆二、武井健、笠木昇、安本久美子、河村武雄）
  - 出品：電影多啦A夢製作委員會（藤子製作株式會社、[小學館株式會社](../Page/小學館.md "wikilink")、[朝日電視台株式會社](../Page/朝日電視台.md "wikilink")、[SHIN-EI动画株式會社](../Page/SHIN-EI动画.md "wikilink")、ADK
    ([Asatsu-DK](../Page/Asatsu-DK.md "wikilink"))、小學館製作株式會社）

## 主題歌

  - 《[擁抱吧](../Page/擁抱吧.md "wikilink")》

<!-- end list -->

  -
    作詞：[阿木燿子](../Page/阿木燿子.md "wikilink")／作曲：[宇崎龍童](../Page/宇崎龍童.md "wikilink")／編曲：[京田誠一](../Page/京田誠一.md "wikilink")／歌：[夏川里美](../Page/夏川里美.md "wikilink")

<!-- end list -->

  - 《[我的声音](../Page/我的声音.md "wikilink")》(ボクノート)

<!-- end list -->

  -
    作詞・作曲・編曲：[大橋卓彌](../Page/大橋卓彌.md "wikilink")・[常田真太郎](../Page/常田真太郎.md "wikilink")／歌：[無限開關](../Page/無限開關.md "wikilink")（[BMG
    JAPAN](../Page/BMG_JAPAN.md "wikilink")／AUGUSTA RECORDS）

<!-- end list -->

  - 《爱的大冒险》

<!-- end list -->

  -
    台湾版主题曲，《我的声音》翻唱版。作曲・编曲：大桥卓弥・常田真太郎／作词：庄景云／歌：[音樂鐵人](../Page/音樂鐵人.md "wikilink")／主唱：劉明峰（[音樂鐵人](../Page/音樂鐵人.md "wikilink")）

## 電影口號

  - 日本：為了你，我會努力到底（君がいるから、がんばれる。）（2006年3月4日公映）
  - 香港：讓多啦A夢與你一起穿越時空，勇闖恐龍世紀（2007年7月26日公映）
  - 中國大陆：超越時空的愛和勇氣（2007年7月20日公映）
  - 台灣：超越時空的愛與勇氣（2007年9月14日公映）

## 新版與舊作的差異

| 情節                                                                                                             | 新版情景        | 舊版情景                                   |
| -------------------------------------------------------------------------------------------------------------- | ----------- | -------------------------------------- |
| (名稱)'''''                                                                                                      | 皮皮          | 嗶之助                                    |
| **小夫的雷龍模型**                                                                                                    | 被靜香弄壞       | 沒有                                     |
| **大雄發誓(用[鼻子吃](../Page/鼻子.md "wikilink")[-{zh-hk:意大利粉;zh-tw:義大利直麵;zh-cn:面条}-](../Page/意大利粉.md "wikilink"))的原因** | 被小夫和胖虎誤會為說謊 | 妒忌小夫擁有恐龍[化石](../Page/化石.md "wikilink") |
| **大雄被小夫和胖虎誤會**                                                                                                 | 有           | 沒有                                     |
| **皮皮險被[生物學會捕捉](../Page/生物.md "wikilink")**                                                                     | 有           | 沒有                                     |
| **多啦A夢把成長促進劑借給大雄**                                                                                             | 沒有          | 有                                      |
| **恐龍**                                                                                                         | 皮皮的外形       |                                        |
| **大雄送走皮皮時打牠**                                                                                                  | 沒有          | 有                                      |
| **睡覺**                                                                                                         | 大雄是和皮皮一起睡覺  | 大雄是和嗶之助分開睡覺                            |

  - 恐龍[獵人的服裝](../Page/獵人.md "wikilink")
  - 皮球的顏色：在舊作中的是排球，而新作中的是粉紅色的皮球
  - 黑衣人和德爾曼斯坦的交通工具
  - 出現恐龍的種類大增，包括[厚頭龍](../Page/厚頭龍.md "wikilink")、[甲龍](../Page/甲龍.md "wikilink")、[速龍](../Page/速龍.md "wikilink")、[慈母龍](../Page/慈母龍.md "wikilink")（[鴨嘴龍](../Page/鴨嘴龍.md "wikilink")）、[三角龍](../Page/三角龍.md "wikilink")、[劍龍](../Page/劍龍.md "wikilink")、以及最後[棘龍與](../Page/棘龍.md "wikilink")[暴龍的對決儼然](../Page/暴龍.md "wikilink")《[侏羅紀公園3](../Page/侏羅紀公園3.md "wikilink")》的經典場景重現
  - 在火山湖偶遇一行人的，舊作說的是[雷龍](../Page/雷龍.md "wikilink")（正確應是迷惑龍），新作提到是比[梁龍](../Page/梁龍.md "wikilink")「遠更巨大幾個級別」的[阿拉莫龍](../Page/阿拉莫龍.md "wikilink")
  - 竹蜻蜓故障的方式，舊作是突然掉下，新作是誇張的[螺旋型自轉](../Page/螺旋.md "wikilink")
  - 在峽谷追逐一行人的，舊作只說是[翼手龍](../Page/翼手龍.md "wikilink")（正確應是無齒翼龍），新作提到是「更大一號」的[風神翼龍](../Page/風神翼龍.md "wikilink")
  - 恐龍獵人的基地外形與外觀，在新作中基地是在瀑布底下，而舊作中是在一個偽裝的稀疏樹林下方
  - 新作中畫出恐龍獵人捕捉的數種恐龍。
  - 舊作省略了收集[露營需要的](../Page/露營.md "wikilink")[食物](../Page/食物.md "wikilink")，以及用「自動罐頭加工機」煮食
  - 換衣服照相機的場景：新作的描寫是因為多啦A夢的失誤，誤將靜香和胖虎的泳衣調換，而舊作的描寫是小夫誤將靜香的泳衣畫成泳褲更換
  - 新作的描寫是隨意門並沒有輸入[白堊紀的地圖](../Page/白堊紀.md "wikilink")，舊作並沒有提及（漫畫有提及）
  - 新作省略了恐龍獵人讀取靜香等人記憶
  - 新作的描寫是靜香等人被困在鐵籠中，而舊作則是被吊在半空中
  - 新作的結局是大雄等人乘坐多啦A夢的時光機返回二十世紀，舊作是大雄等人乘坐時光巡邏隊的時光船返回二十世紀
  - 時光巡邏隊的時光機外型不同，體型也較小，船身字體由舊作只有英文的「T‧P」改成加入了漢字的「時警」
  - 吃到糯米糰子的[暴龍的走路方法](../Page/暴龍.md "wikilink"):在舊作中是挺起上身走路，而新作中是后背水平走路
  - 原作版內容與電影版的台詞（部分）有經過修改。例如原作版內有句台詞「」（依據日本法令規定，在廣播不得使用某些字彙，而電視或DVD亦是如此）而原作台詞為「」

## 其他

  - 本作品是第一部於中国大陆院线公映的[日本](../Page/日本.md "wikilink")[動畫電影](../Page/動畫電影.md "wikilink")，由於中國大陸並未曾上映過1980年版本的大雄的恐龍，因此並沒有像香港和台灣般加上「新」字以旨析別。
  - 香港於2007年7月26日上映此作品，累積票房為166萬港元。較2004年的《[大雄的貓狗時空傳](../Page/大雄的貓狗時空傳.md "wikilink")》（300萬）少44.66%。网络评论认为这样的结果与香港代理的包装方式有很大关联，本作的卖点是“感动”“怀旧”而非“探险”。而香港代理依然以包装旧作的方式（如电影口号），导致与同档期的Keroro剧场版竞争落败。另外，[香港三大叮当网站关闭也是原因之一](../Page/香港哆啦A夢迷網站版權爭議事件.md "wikilink")。
  - 本作品也首度開創於劇終後附上次年預告的慣例。內容是大雄在懸崖上挖化石，卻挖到哆啦A夢，並大叫「明年也要看喔」。不過畫面與第二年的《-{zh-hant:新魔界大冒險;zh-cn:奇幻大冒险}-》毫無關係。

## 外部連結

  - [哆啦A夢電影『大雄的恐龍2006』日本官方網站](http://doraeiga.com/2006/top/top.html)

  - [哆啦A夢電影『新大雄的恐龍』台灣官方網站](https://web.archive.org/web/20090111232155/http://www.dora-world.com.tw/event/200708_movie/)

  -
  - {{@movies|fdjp60802983}}

  -
  -
[2006](../Category/哆啦A梦电影作品.md "wikilink")
[Category:2006年日本劇場動畫](../Category/2006年日本劇場動畫.md "wikilink")
[Category:重製動畫電影](../Category/重製動畫電影.md "wikilink")
[Category:恐龍動畫電影](../Category/恐龍動畫電影.md "wikilink")
[Category:渡邊步電影](../Category/渡邊步電影.md "wikilink")
[Category:蛇頸龍電影](../Category/蛇頸龍電影.md "wikilink")
[Category:上海电影译制厂译制作品](../Category/上海电影译制厂译制作品.md "wikilink")
[Category:白堊紀背景電影](../Category/白堊紀背景電影.md "wikilink")
[Category:華視外購動畫](../Category/華視外購動畫.md "wikilink")
[Category:北美洲背景電影](../Category/北美洲背景電影.md "wikilink")
[Category:時間旅行題材動畫電影](../Category/時間旅行題材動畫電影.md "wikilink")
[Category:2000年代科幻片](../Category/2000年代科幻片.md "wikilink")
[Category:2000年代冒險片](../Category/2000年代冒險片.md "wikilink")