，是[藤子·F·不二雄執筆的](../Page/藤子·F·不二雄.md "wikilink")[哆啦A夢大長篇作品](../Page/哆啦A梦大长篇.md "wikilink")，于1989年10月到1990年3月連載於月刊《[龙漫CORO-CORO](../Page/龙漫CORO-CORO.md "wikilink")》。並於1990年3月10日正式公映，是第10部哆啦A夢大長篇作品，也是第11部哆啦A夢電影。導演為芝山努。票房收入19億日元、觀賞人數380萬人。並映作品為《[大耳鼠·愛莉活動大寫真](../Page/大耳鼠·愛莉活動大寫真.md "wikilink")》（）。

## 概説

自本作开始，对環境問題的关注，成为藤子在世时期后期大长篇作品的重要要素。与《[大雄的猫狗时空传](../Page/大雄的猫狗时空传.md "wikilink")》并列，能看到「带耳朵的蓝色哆啦A梦」成为作品的新奇之处。

正式公開上映時適逢[東映動畫片節的](../Page/東映動畫片博覽會.md "wikilink")《[七龍珠Z](../Page/七龍珠.md "wikilink")：這世上最強的傢伙》也同時上映，一直到1996年以後的《[大雄與銀河超特急](../Page/大雄與銀河超特急.md "wikilink")》上映為止，兩部片子展開激烈的競爭，票房收入以及動員觀客數都被拿來做比較。許多人稱這是場「哆啦·Dora的對決」。（，哆啦A夢日本原名：****、七龍珠日本原名：****，取兩個前面的文字而來）

## 故事概要

某天夜晚，大雄从一片粉红色的雾气裡（後來得知是22世紀的任意瓦斯，動物們稱為光之梯）进入一片从未见过的森林，在那里动物用人的语言交流，而且都是直立行走。次日，大雄将这件事告诉同学，大家笑他是「在發童話世界的夢」。尽管大雄拿了一朵从梦境里带来的花，一样无人相信。同时，开发商有将后山兴建成为高尔夫球场的计划。那个夜晚，大雄再次进入那片森林，原来那不是梦，而是一个动物星球。大雄和哆啦A梦遇到了吉波，并成为好朋友。

次日，打算削平后山的人开始了勘测，尽管哆啦A梦用道具轰走了，但是这些人仍不死心。后来，静香也跟着大雄穿过烟雾，来到动物行星。而偷偷跟来的小夫和胖虎却因胡亂使用噴霧機而來到了動物星。一场大冒险就此展开…

## 故事舞台

  - 動物星
    一个[和平星球](../Page/和平.md "wikilink")。拥有先进的[科学技术](../Page/科学技术.md "wikilink")，如[太阳能和](../Page/太阳能.md "wikilink")[风力发电](../Page/风力发电.md "wikilink")，利用[光](../Page/光.md "wikilink")、[水](../Page/水.md "wikilink")、[空气来制造](../Page/空气.md "wikilink")[食物](../Page/食物.md "wikilink")，[污水淨化装置等](../Page/污水.md "wikilink")。而且[环境清洁](../Page/环境.md "wikilink")，没有[军队](../Page/军队.md "wikilink")。星球上没有国家这个概念，即[世界大同](../Page/大同_\(思想\).md "wikilink")。
  - 地獄星
    邻近动物星的一个[星球](../Page/星球.md "wikilink")。过去拥有高度的[文明](../Page/文明.md "wikilink")，但后来因为[環境問題](../Page/環境問題.md "wikilink")、[核战争](../Page/核战争.md "wikilink")、[自然灾害而荒废](../Page/自然灾害.md "wikilink")，残留下来的人又从石器时代的文明程度开始发展，但后来又恢复到可自行制造宇宙飞船的文明水平。由于[空气污染和](../Page/空气污染.md "wikilink")[水污染太过严重](../Page/水污染.md "wikilink")，導致人们都要穿[防护衣](../Page/防毒面具.md "wikilink")。

## 出場人物

### 原設

| 角色       |                 配音員                  |
| -------- | :----------------------------------: |
| 日本       |                  香港                  |
| **哆啦A夢** |  [大山羨代](../Page/大山羨代.md "wikilink")  |
| **大雄**   | [小原乃梨子](../Page/小原乃梨子.md "wikilink") |
| **靜香**   |  [野村道子](../Page/野村道子.md "wikilink")  |
| **胖虎**   |  [立壁和也](../Page/立壁和也.md "wikilink")  |
| **小夫**   |  [肝付兼太](../Page/肝付兼太.md "wikilink")  |

### 動物星

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th style="text-align: center;"><p>配音員</p></th>
<th><p>介紹</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日本</p></td>
<td style="text-align: center;"><p>台灣</p></td>
<td><p>香港</p></td>
</tr>
<tr class="even">
<td><p><strong>吉波</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/田中真弓.md" title="wikilink">田中真弓</a></p></td>
<td><p><a href="../Page/許淑嬪.md" title="wikilink">許淑嬪</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>汪格爾</strong></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/劉傑.md" title="wikilink">劉傑</a></p></td>
</tr>
<tr class="even">
<td><p><strong>吉波的母親</strong></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/陳美貞.md" title="wikilink">陳美貞</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>蘿咪</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/西原久美子.md" title="wikilink">西原久美子</a></p></td>
<td><p><a href="../Page/楊凱凱.md" title="wikilink">楊凱凱</a></p></td>
</tr>
<tr class="even">
<td><p><strong>烏丹</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/川久保潔.md" title="wikilink">川久保潔</a></p></td>
<td><p><a href="../Page/于正昌.md" title="wikilink">于正昌</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>大猩猩</strong></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/于正昌.md" title="wikilink">于正昌</a></p></td>
</tr>
<tr class="even">
<td><p><strong>阿猩/猩太郎</strong></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/劉傑.md" title="wikilink">劉傑</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>鹈鹕</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/茶風林.md" title="wikilink">茶風林</a></p></td>
<td><p><a href="../Page/劉傑.md" title="wikilink">劉傑</a></p></td>
</tr>
<tr class="even">
<td><p><strong>羊醫生</strong></p></td>
<td style="text-align: center;"><p><a href="../Page/田中亮一.md" title="wikilink">田中亮一</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 尼姆奎

存在於地獄星的邪惡勢力，自古以來就流傳於動物星的神話。真面目為秘密地下組織－蟑螂隊，此組織全體成員最終被聯邦警察逮捕。

| 角色     |                 配音員                  | 介紹                               |
| ------ | :----------------------------------: | -------------------------------- |
| 日本     |                  台灣                  | 香港                               |
| **首領** |   [森功至](../Page/森功至.md "wikilink")   | [于正昌](../Page/于正昌.md "wikilink") |
| **隊長** | [小杉十郎太](../Page/小杉十郎太.md "wikilink") | [劉傑](../Page/劉傑.md "wikilink")   |
| **成員** |   [西尾德](../Page/西尾德.md "wikilink")   |                                  |

### 其他

| 角色       |                配音員                 | 介紹                                                                                       |
| -------- | :--------------------------------: | ---------------------------------------------------------------------------------------- |
| **間諜**   | [平野正人](../Page/平野正人.md "wikilink") | 聯邦警察在尼姆奎的臥底。雖然中途被尼姆奎的隊長逮到，但在大雄的幫助下幸運脫逃，並且在大雄一夥與動物星全體居民對抗尼姆奎的戰爭中出手搭救，成功將尼姆奎繩之以法，同時也與大雄重逢。 |
| **地產仲介** |  [渡部猛](../Page/渡部猛.md "wikilink")  |                                                                                          |
| **社長**   |  [加藤治](../Page/加藤治.md "wikilink")  | 買下學校後山的社長。                                                                               |

## 制作人员

  - 監督：[芝山努](../Page/芝山努.md "wikilink")
  - 脚本：[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")
  - 演出助手：[塚田庄英](../Page/塚田庄英.md "wikilink")、[平井峰太郎](../Page/平井峰太郎.md "wikilink")
  - 作画監督：[富永貞義](../Page/富永貞義.md "wikilink")
  - 作画監督補佐：[渡邊歩](../Page/渡邊歩.md "wikilink")
  - 美術設定：[川本征平](../Page/川本征平.md "wikilink")
  - 美術監督：高野正道
  - 色設計：野中幸子
  - 撮影監督：斋藤秋男
  - 特殊撮影：渡邊由利夫
  - 編集：井上和夫、渡瀨祐子
  - 録音監督：[浦上靖夫](../Page/浦上靖夫.md "wikilink")
  - 効果：柏原満
  - 音楽：[菊池俊輔](../Page/菊池俊輔.md "wikilink")
  - 監修：[楠部大吉郎](../Page/楠部大吉郎.md "wikilink")
  - 製作擔當：市川芳彦
  - 製片人：別紙壮一、[山田俊秀](../Page/山田俊秀.md "wikilink")、小泉美明、加藤守啓
  - 制作協力：[藤子·F·不二雄製作公司](../Page/藤子·F·不二雄製作公司.md "wikilink")、[ASATSU](../Page/ASATSU.md "wikilink")
  - 制作：[SHIN-EI動画](../Page/SHIN-EI動画.md "wikilink")、[小学館](../Page/小学館.md "wikilink")、[朝日电视台](../Page/朝日电视台.md "wikilink")

## 主題歌

  - 片尾曲：「」（寄语蓝天）（詞：[武田鐵矢](../Page/武田鐵矢.md "wikilink")／作曲：[堀内孝雄](../Page/堀内孝雄.md "wikilink")／編曲：[原田末秋](../Page/原田末秋.md "wikilink")／演唱：武田鐵矢）
  - 片头曲：「哆啦A梦之歌」（作詞：[楠部工](../Page/楠部工.md "wikilink")／作曲：[菊池俊輔](../Page/菊池俊輔.md "wikilink")／演唱：[山野智子](../Page/山野智子.md "wikilink")）

## 作品特点

该作品是哆啦A梦电影系列中说教性和严肃性较浓的一部作品（特别是**環境問題是人類文明的警鐘**这一主题）。诸如大雄的妈妈加入反对兴建高尔夫球场的居民委员会，大雄妈妈在环境问题上对大雄的说教之类的场面在本作中经常可见。

原作中出木杉曾经登场，但在电影版裡其没有出现。这是首次出现电影版将原作裡的一个登场人物戏份全砍的事情。

在先进的污水处理设施、太阳能发电、光的台阶、禁入森林等元素构成了动物行星这个奇妙世界。但是一方面也有评判意见认为这部作品说教味道太过浓厚。

该作品的另外一个特点是与原著相比，结尾所用的时间大幅增加（类似的作品还有《[大雄與銀河超特急](../Page/大雄與銀河超特急.md "wikilink")》）。另外，电影版裡还提及了高尔夫球场计划最后中止，而原作中则没有提及。与之前的作品《[大雄的日本誕生](../Page/大雄的日本誕生.md "wikilink")》一様、该作是少数电影评价高于原作漫画评价的大长篇电影作品。特别是大雄与吉普离别的场面获得了高评价。

在动物星球裡，到处都可以看见人类世界的影子，动物被高度拟人化，出现了「在狗的帮助下找到了迷路的小猫」「吃了黑羊先生寄的信的白羊先生」等著名[童话](../Page/童话.md "wikilink")。同时，好不容易可以戴耳朵的哆啦A梦还是被当作狸猫，哆啦A梦因此也非常生气。而之后动物行星裡真正的狸猫也显示出对自己身份的自卑，这与现实世界都是相同的。而到电影结束，哆啦A梦也能接受称其为狸猫了。

普遍认为胖虎对于该电影也有加分作用，然而描绘了胖虎的软弱一面成为该电影的新奇之处之一。

## 遊戲作品

哆啦A夢2 動物行星傳說(日文：ドラえもん2
アニマル惑星伝説)－機種：[GB](../Page/GB.md "wikilink")，發售日期:1992年12月14日，發售：[Epoch](../Page/Epoch.md "wikilink")

## 外部链接与参考资料

  - 《大雄与动物行星》，藤子·F·不二雄，吉林美術出版社，ISBN 7-5386-1778-7

  -
  -
  -
[Category:芝山努電影](../Category/芝山努電影.md "wikilink")
[1990](../Category/哆啦A梦电影作品.md "wikilink")
[Category:1990年日本劇場動畫](../Category/1990年日本劇場動畫.md "wikilink")
[Category:外星生命題材動畫電影](../Category/外星生命題材動畫電影.md "wikilink")
[Category:動物題材電影](../Category/動物題材電影.md "wikilink")
[Category:环境保护电影](../Category/环境保护电影.md "wikilink")
[Category:虛構行星背景電影](../Category/虛構行星背景電影.md "wikilink")
[Category:1990年代科幻片](../Category/1990年代科幻片.md "wikilink")
[Category:1990年代冒險片](../Category/1990年代冒險片.md "wikilink")