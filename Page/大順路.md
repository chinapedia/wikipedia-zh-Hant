**大順路**為[台灣](../Page/台灣.md "wikilink")[高雄市東西向的主要道路](../Page/高雄市.md "wikilink")，西起[中華一路口](../Page/中華路_\(高雄市\).md "wikilink")，東在[建工路口而轉為南北向道路至](../Page/建工路_\(高雄市\).md "wikilink")[中正一路口](../Page/中正路_\(高雄市\).md "wikilink")，往南接[河南路可以到](../Page/河南路_\(高雄市\).md "wikilink")[三多一路](../Page/三多路_\(高雄市\).md "wikilink")，全線共分為三段。是[鼓山區經](../Page/鼓山區.md "wikilink")[左營區](../Page/左營區.md "wikilink")、[三民區往](../Page/三民區.md "wikilink")[苓雅區的重要道路](../Page/苓雅區.md "wikilink")。此路種有大量的[雨豆樹](../Page/雨豆樹.md "wikilink")，是市區裡極為罕見的綠色隧道，未來[環狀輕軌第二階段將會行經此路](../Page/環狀輕軌.md "wikilink")，沿路樹木亦全數保留\[1\]，同時為了配合施工，將會進入交通黑暗期\[2\]。

## 行經行政區域

（由西至東）

  - [鼓山區](../Page/鼓山區.md "wikilink")
  - [左營區](../Page/左營區.md "wikilink")
  - [三民區](../Page/三民區.md "wikilink")
  - [苓雅區](../Page/苓雅區.md "wikilink")

## 分段

大順路共分為三個部分：

  - 大順一路：西起[中華一路口](../Page/中華路_\(高雄市\).md "wikilink")，東於[民族一路口接大順二路](../Page/民族路_\(高雄市\).md "wikilink")。
  - 大順二路：西於民族一路口接大順一路，東在[建工路口轉南於](../Page/建工路_\(高雄市\).md "wikilink")[九如一路口接大順陸橋往大順三路](../Page/九如路_\(高雄市\).md "wikilink")。
  - 大順三路：北於九如一路口接大順二路，南止於[中正一路口](../Page/中正路_\(高雄市\).md "wikilink")。

## 歷史

根據1980年的高雄市行政區域圖，大順路的範圍僅包含今大順三路，以及大順陸橋以東至澄清路之間的九如一路及自強陸橋至澄清湖之間的澄清路等皆名為大順路，今天的大順一、二路皆尚未修建，而建興路則有連接至今大順九如路口處；1985年經建版地形圖第一版，隨著大順路在寶珠溝及灣仔內的路段（大順一、二路）通車，大順陸橋以東至澄清路之間的路段改為九如一路。

### 大順陸橋

位於[三民區](../Page/三民區.md "wikilink")[九如一路至](../Page/九如路_\(高雄市\).md "wikilink")[苓雅區憲政路之間](../Page/苓雅區.md "wikilink")，跨越台鐵[屏東線鐵路](../Page/屏東線.md "wikilink")，1975年10月竣工，2018年10月14日因鐵路遷入地下永久路線營運啟用，同年11月5日先拆除橋下空間隔間，2019年3月16日上午起拆除橋體\[3\]，四天橋體拆除完畢\[4\]</ref>。同年4月8日上午10時恢復平面道路開放通行\[5\]</ref>。

## 沿線設施

（大順一、二路由西至東，大順三路由北至南。）

  - 大順一路
      - [高雄市鼓山區龍華國小](../Page/高雄市鼓山區龍華國民小學.md "wikilink")（大順一路858號）
      - [好市多大順店](../Page/好市多.md "wikilink")（大順一路111號）
      - 高雄市左營區龍華國中（自由二路2號）
      - [高雄市左營區新上國小](http://www.shsps.kh.edu.tw/)（大順一路100號）
      - 龍華橋
      - [悅誠文創廣場](../Page/悅誠文創廣場.md "wikilink")

<!-- end list -->

  - 大順二路
      - 全國加油站大順站
      - [家樂福鼎山店](../Page/家樂福.md "wikilink")
      - [摩斯漢堡鼎山門市](../Page/摩斯漢堡.md "wikilink")
      - [台北富邦銀行三民分行](../Page/台北富邦銀行.md "wikilink")（大順二路530號）
      - 建安診所洗腎中心（大順二路649號）
      - [三民區民眾活動中心](../Page/三民區.md "wikilink")（大順二路468號）
          - [高雄市政府消防局第一中隊](../Page/高雄市政府消防局.md "wikilink")、大昌分隊（三民區民眾活動中心1樓、3樓）
          - [高雄銀行灣內分行](../Page/高雄銀行.md "wikilink")（三民區民眾活動中心1樓）
          - [高雄市立圖書館寶珠分館](../Page/高雄市立圖書館.md "wikilink")（三民區民眾活動中心4樓）
          - [財政部](../Page/財政部.md "wikilink")[高雄市國稅局三民分局第二辦公室](../Page/高雄市國稅局.md "wikilink")（三民區民眾活動中心6樓）
          - 三民東區綜合社會福利服務中心（三民區民眾活動中心7樓）
          - 高雄市三民區第二戶政事務所（三民區民眾活動中心9樓）
          - [高雄市政府](../Page/高雄市政府.md "wikilink")[勞工局訓練就業中心三民就業服務站](../Page/勞工局.md "wikilink")（三民區民眾活動中心10樓）
      - [好樂迪建工店](../Page/好樂迪.md "wikilink")
      - [雄工駕訓班](../Page/高雄市立高雄高級工業職業學校.md "wikilink")
      - [高雄市三民區正興國小](http://www.jsps.kh.edu.tw/)
      - [肯德基大順二門市](../Page/肯德基.md "wikilink")
      - 大順郵局（大順二路207號）
      - [陽信銀行大順分行](../Page/陽信銀行.md "wikilink")（大順二路41號）

<!-- end list -->

  - 大順三路
      - [链接=<https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg>](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:链接=https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg")[台鐵科工館車站](../Page/科工館車站.md "wikilink")
      - [家樂福大順店](../Page/家樂福.md "wikilink")（已於2013年10月31日結束營業）
      - [特力屋高雄店](../Page/特力屋.md "wikilink")
      - [環球影城](../Page/環球影城.md "wikilink")
      - [POYA寶雅高雄大順店](../Page/POYA寶雅.md "wikilink")
      - [肯德基大順門市](../Page/肯德基.md "wikilink")
      - [Mister
        Donut建國門市](../Page/Mister_Donut.md "wikilink")（已歇業，原址現2018年11月21日為頂好超市）
      - [星巴克建國門市](../Page/星巴克.md "wikilink")（已歇業，原址現2018年11月21日為頂好超市）

## 公車資訊

  - 大順一路
      - [漢程客運](../Page/漢程客運.md "wikilink")
          - [環狀168東](../Page/高雄市公車環狀幹線.md "wikilink")
            [金獅湖站](../Page/金獅湖站.md "wikilink")－金獅湖站
          - [環狀168西](../Page/高雄市公車環狀幹線.md "wikilink") 金獅湖站－金獅湖站
          - 0南 金獅湖站－金獅湖站
          - 0北 金獅湖站－金獅湖站
      - [南台灣客運](../Page/南台灣客運.md "wikilink")
          - 紅32
            捷運[凹子底站](../Page/凹子底站.md "wikilink")－[市立美術館](../Page/高雄市立美術館.md "wikilink")－[大榮高中](../Page/高雄市私立大榮高級中學.md "wikilink")

<!-- end list -->

  - 大順二路
      - [漢程客運](../Page/漢程客運.md "wikilink")
          - 環狀168東 [金獅湖站](../Page/金獅湖站.md "wikilink")－金獅湖站
          - 環狀168西 金獅湖站－金獅湖站
          - 0南 金獅湖站－金獅湖站
          - 0北 金獅湖站－金獅湖站
          - 33 金獅湖站－鹽埕埔
      - [港都客運](../Page/港都客運.md "wikilink")
          - 217 新昌幹線 [加昌站](../Page/加昌站_\(公車\).md "wikilink")－合群社區－鳥松區公所
      - [南台灣客運](../Page/南台灣客運.md "wikilink")
          - 16
            [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
            [高鐵左營站](../Page/新左營車站.md "wikilink")－
            捷運[巨蛋站](../Page/巨蛋站.md "wikilink")－[高雄科技大學建工校區](../Page/國立高雄科技大學.md "wikilink")
          - 紅29 捷運後驛站－陽明國小
      - [東南客運](../Page/東南客運.md "wikilink")
          - 37
            [前鎮站](../Page/前鎮站.md "wikilink")－[育英醫專](../Page/育英醫護管理專科學校.md "wikilink")
          - 81 [瑞豐站](../Page/瑞豐站.md "wikilink")－育英醫專
      - [高雄客運](../Page/高雄客運.md "wikilink")
          - 8021 鳳山－彌陀
      - [義大客運](../Page/義大客運.md "wikilink")
          - 8503
            [義大世界](../Page/義大世界.md "wikilink")－[高雄市政府四維行政中心](../Page/高雄市政府.md "wikilink")

<!-- end list -->

  - 大順三路
      - [漢程客運](../Page/漢程客運.md "wikilink")
          - 環狀168東 [金獅湖站](../Page/金獅湖站.md "wikilink")－金獅湖站
          - 環狀168西 金獅湖站－金獅湖站
          - 0南 金獅湖站－金獅湖站
          - 0北 金獅湖站－金獅湖站
          - 76 金獅湖站－[歷史博物館](../Page/歷史博物館.md "wikilink")
          - 77 金獅湖站－歷史博物館
      - [東南客運](../Page/東南客運.md "wikilink")
          - 37
            [前鎮站](../Page/前鎮站.md "wikilink")－[育英醫專](../Page/育英醫護管理專科學校.md "wikilink")
          - 81 [瑞豐站](../Page/瑞豐站.md "wikilink")－育英醫專
      - [義大客運](../Page/義大客運.md "wikilink")
          - 8503
            [義大世界](../Page/義大世界.md "wikilink")－[高雄市政府四維行政中心](../Page/高雄市政府.md "wikilink")

## 來源

## 相關連結

  - [高雄市主要道路列表](../Page/高雄市主要道路列表.md "wikilink")

[Category:鼓山區](../Category/鼓山區.md "wikilink")
[Category:三民區](../Category/三民區.md "wikilink")
[Category:苓雅區](../Category/苓雅區.md "wikilink")
[Category:高雄市街道](../Category/高雄市街道.md "wikilink")

1.  [〈南部〉高市雨豆隧道強碰輕軌
    捷局：全數保留](http://news.ltn.com.tw/news/local/paper/996751)

2.  [輕軌2階段工程動土
    北高雄交通將陷黑暗期](https://newtalk.tw/news/view/2017-02-09/81723)

3.

4.
5.