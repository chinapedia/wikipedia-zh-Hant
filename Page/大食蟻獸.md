**大食蟻獸**（[學名](../Page/學名.md "wikilink")：*Myrmecophaga
tridactyla*）是[異關節總目](../Page/異關節總目.md "wikilink")[披毛目](../Page/披毛目.md "wikilink")[食蟻獸的一種](../Page/食蟻獸.md "wikilink")，為一種大型的[食蟲動物](../Page/食蟲動物.md "wikilink")，和[樹懶為近親](../Page/樹懶.md "wikilink")。分布在[中美洲與](../Page/中美洲.md "wikilink")[南美洲部份地區](../Page/南美洲.md "wikilink")，生活在[草地](../Page/草地.md "wikilink")、[落葉林和](../Page/落葉林.md "wikilink")[雨林地區](../Page/雨林.md "wikilink")。主要以[螞蟻和](../Page/螞蟻.md "wikilink")[白蟻為食](../Page/白蟻.md "wikilink")，一天最多可食30,000隻[昆蟲](../Page/昆蟲.md "wikilink")\[1\]。

大食蟻獸在現存四種[食蟻獸中體型最大的成員](../Page/食蟻獸.md "wikilink")，體長可達 。雄性體重通常為 ，雌性則為
\[2\]。[體毛長而堅硬](../Page/毛皮.md "wikilink")，可長40釐米，尾部密生长毛\[3\]，头细长，眼耳极小并吻成管状，无齿，舌细长并能伸缩，借以舐食蚁类、白蚁及其他昆虫。前肢除第五指外，均具钩爪，后肢短，五爪大小相仿。体灰白色，背面两侧有宽阔的黑色纵纹，纹的边缘白色。大食蟻獸為獨居動物，個體之間少有互動，少數例外為：雄性之間的示威、交配、以及養育後代。雌性大食蟻獸會將幼仔背在背上活動。

大食蟻獸被[國際自然保護聯盟列為](../Page/國際自然保護聯盟.md "wikilink")[易危物種](../Page/易危物種.md "wikilink")，在許多地區面臨[局部地區滅絕](../Page/局部地區滅絕.md "wikilink")。大食蟻獸所面臨的威脅包括棲息地喪失以及盜獵取得其毛皮與[肉](../Page/叢林肉.md "wikilink")。

包括兩個亞種：

  - *Myrmecophaga tridactyla artata* Osgood, 1912
  - *Myrmecophaga tridactyla centralis* Lyon, 1906

## 描述

[Myrmecophaga_tridactyla_-_Phoenix_Zoo.jpg](https://zh.wikipedia.org/wiki/File:Myrmecophaga_tridactyla_-_Phoenix_Zoo.jpg "fig:Myrmecophaga_tridactyla_-_Phoenix_Zoo.jpg")
[Myrmecophaga_tridactyla_skeleton2.jpg](https://zh.wikipedia.org/wiki/File:Myrmecophaga_tridactyla_skeleton2.jpg "fig:Myrmecophaga_tridactyla_skeleton2.jpg")
大食蟻獸的頭顱長達
\[4\]，即使和食蟻獸比起來仍然特別細長\[5\]，管狀的口鼻部佔了大部分。大食蟻獸的眼睛與耳朵很小\[6\]，視力不好，但嗅覺的敏感度為人類的
40 倍\[7\]。在人工飼育的環境下，大食蟻獸的可活約 16 年\[8\]。

大食蟻獸的前後足均有五趾，其中前足有四趾帶爪，第二與第三趾上的爪子長度最長\[9\]。大食蟻獸用指節著地的形式行走，類似於現今的[大猩猩](../Page/大猩猩.md "wikilink")、[黑猩猩](../Page/黑猩猩.md "wikilink")，以及已滅絕的[大地懶與](../Page/大地懶.md "wikilink")[爪獸](../Page/爪獸.md "wikilink")，可以避免其長爪因接觸到地面而磨損。主要承受重量的第三趾，具有延長的，可彎曲於\[10\]。和前足不同，大食蟻獸的後足五趾上均具有短爪，為\[11\]。大食蟻獸擴大的給予更多的附著點，強化前足回拉的力量。而[三頭肌則提供前足第三趾彎曲的力量](../Page/三頭肌.md "wikilink")\[12\]，這些肌肉可以幫助大食蟻獸挖掘並破壞白蟻的。

## 參考文獻

[Category:食蚁兽科](../Category/食蚁兽科.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")
[Category:南美洲動物](../Category/南美洲動物.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.
9.
10.

11.
12.