**大麥哲倫星系**又称**大麦哲伦云**（，簡寫為*LMC*），是一個環繞著[太陽所在的](../Page/太陽.md "wikilink")[銀河系運轉的](../Page/銀河系.md "wikilink")[星系](../Page/星系.md "wikilink")，距離約為50,000[秒差距](../Page/秒差距.md "wikilink")（～160,000光年），直徑大約是[銀河系的](../Page/銀河系.md "wikilink")1/20，恆星數量約為1/10（大約是100億顆恆星）。虽然比大多數星系為大，但在讨论银河系的时候也会被当做[矮星系](../Page/矮星系.md "wikilink")。

大麦哲伦星系的形态类似[不规则星系](../Page/不规则星系.md "wikilink")，但似乎有一些螺旋結構的痕跡。有些推測認為大麦哲伦星系以前是[棒旋星系](../Page/棒旋星系.md "wikilink")，受到[銀河系的重力擾動才成為](../Page/銀河系.md "wikilink")[不規則星系](../Page/不規則星系.md "wikilink")，因此在中央仍保有短棒的結構。在[NASA銀河系外資料庫中依據](../Page/NASA銀河系外資料庫.md "wikilink")[哈伯星系分類為Irr](../Page/星系分類#哈伯序列.md "wikilink")/SB(s)m。

大麦哲伦星系是[本星系群中第四大的星系](../Page/本星系群.md "wikilink")，其餘三個依序為[仙女座星系](../Page/仙女座星系.md "wikilink")（M31）、[銀河系及](../Page/銀河系.md "wikilink")[三角座星系](../Page/三角座星系.md "wikilink")（M33）。

在[南半球的夜空中](../Page/南半球.md "wikilink")，大麦哲伦星系是一個昏暗的天體，跨立在[山案座和](../Page/山案座.md "wikilink")[劍魚座兩個星座的邊界之間](../Page/劍魚座.md "wikilink")。它的名稱來自航海家[斐迪南·麥哲倫](../Page/斐迪南·麥哲倫.md "wikilink")，在他繞行地球一週的遠航中觀察了它與[小麥哲倫星系](../Page/小麥哲倫星系.md "wikilink")（SMC）。（其實早在約西元964年，波斯天文学家[阿布德·热哈曼·阿尔苏飞就已經在](../Page/阿布德·热哈曼·阿尔苏飞.md "wikilink")[恆星之書](../Page/恆星之書.md "wikilink")（Book
of Fixed Stars）中記錄了這兩個星系）。

## 特征

[A_spectacular_view_over_the_Atacama.jpg](https://zh.wikipedia.org/wiki/File:A_spectacular_view_over_the_Atacama.jpg "fig:A_spectacular_view_over_the_Atacama.jpg")，大麥哲倫星系和[小麥哲倫星系位於右方](../Page/小麥哲倫星系.md "wikilink")\]\]
如很多不规则星系一样，大麦哲伦星系里存在丰富的气体和星际物质，并且正在经历着明显的[恒星形成活动](../Page/恒星形成.md "wikilink")。\[1\]这种大量恒星的形成现象可能是因为大麦哲伦星系受到了银河系[潮汐力的影响](../Page/潮汐力.md "wikilink")。并且，银河系的潮汐力也从大麦哲伦星系中剥离了一些恒星和星际物质，形成了漫长的[麦哲伦星流](../Page/麦哲伦星流.md "wikilink")。

大麦哲伦星系内充满了各种星体和天文现象。当前已经在大麦哲伦星系内发现了60个球状星团，400个行星状星云和700个疏散星团，以及数十万计的巨星和超巨星。[SN
1987A是最近在大麦哲伦星系里发现的](../Page/SN_1987A.md "wikilink")[超新星](../Page/超新星.md "wikilink")。

## 幾何形狀

大麥哲倫雲通常被視為[不規則星系](../Page/不規則星系.md "wikilink")，然而它顯示出有棒狀結構的跡象，因此曾經被重歸類為[麥哲倫型](../Page/麥哲倫螺旋星系.md "wikilink")[矮螺旋星系](../Page/矮螺旋星系.md "wikilink")。

大麥哲倫雲有一個顯著的中央棒和[螺旋臂](../Page/螺旋臂.md "wikilink")\[2\]。中央棒似乎有著扭曲，東西兩端比中心靠近銀河系\[3\]。利用哈伯太空望遠鏡的觀測，在2014年測量出他的自轉週期大約是2億5千萬年\[4\]。

長久以來，大麥哲倫雲被認為是一個二維（平面）的星系，與銀河系的距離是單一的。但是，科德威和考森在1986年\[5\]發現在東北部的[造父變星比西南部的造父變星接近銀河系](../Page/造父變星.md "wikilink")。最近，通過觀測星場中其它的造父變星\[6\]、核心燃燒氦的紅叢集\[7\]和紅巨星分支等的觀測\[8\]證實了這種幾何上的傾斜。綜合這三篇論文得到的傾斜約為35°，以正面朝向我們的星系傾斜被定為0°。利用碳星運動壆進一步的研究顯示大麥哲倫雲的盤面結構是兩個厚片\[9\]和向外傾斜的\[10\]。關於大麥哲倫內[星團的研究](../Page/星團.md "wikilink")，[休梅克等人](../Page/羅伯特·休梅克.md "wikilink")\[11\]測量了大約80個星團的速度，並且發現在大麥哲倫雲的群體運動學符合纇似盤面分佈的星團運動。這些結果也得到了科德威等人的證實\[12\]，他們計算了大麥哲倫雲中一些星團的距離，顯示它們的分佈與星場的平面是相同的。

## 距離

跟所有的星系一樣，確認LMC正確的距離是一項挑戰，多年來測量所得的數值變化極大。主要的困難肇因於作為銀河系外測量距離[標準燭光的](../Page/標準燭光.md "wikilink")[造父變星和](../Page/造父變星.md "wikilink")[天琴座RR變星](../Page/天琴座RR變星.md "wikilink")，在[銀河系內的](../Page/銀河系.md "wikilink")[視差還未能正確的校準](../Page/視差.md "wikilink")；另一個原因則是LMC的[金屬含量偏低對](../Page/金屬.md "wikilink")[發光效率的影響尚不確知](../Page/發光效率.md "wikilink")。在過去的十年，已經將距離確認在155,000～165,000光年的範圍內，以一個最近的[距離模數估計的值是](../Page/距離模數.md "wikilink")18.56，大約是51.5千[秒差距](../Page/秒差距.md "wikilink")。\[13\]

[Satellite_Galaxies.JPG](https://zh.wikipedia.org/wiki/File:Satellite_Galaxies.JPG "fig:Satellite_Galaxies.JPG")

## 相关图集

Image:Turquoise-tinted plumes in the Large Magellanic Cloud.jpg|\[14\]
Image:The star formation region NGC 2035 imaged by the ESO Very Large
Telescope.jpg| [NGC 2035](../Page/NGC_2035.md "wikilink") \[15\]
Image:LHA 120-N11 in the Large Magellanic Cloud.jpg|LHA 120-N11 Image:LH
95.jpg|[LH 95](../Page/LH_95.md "wikilink") Image:ESO-SNR B0544-6910 in
the LMC-phot-34d-04-fullres.jpg|SNR B0544-6910 Image:ESO-SNR 0543-689 in
the LMC-phot-34c-04-fullres.jpg|SNR 0543-689 Image:Southern part of the
spectacular N44 H II region in the Large Magellanic Cloud.jpg|N44
Image:ESO-DEM L 159 Nebula KMHK 840 and 831 clusters
LMC-phot-31c-03-fullres.jpg| Image:Image from ESO’s La Silla Observatory
of part of the Large Magellanic Cloud.jpg| Image:Large and small
magellanic cloud from new zealand.jpg| Image:Large Magellanic Cloud.jpg|
Image:Hubble_Peers_into_the_Storm_(29563971405).jpg|N159星雲

## 參見

  - [小麥哲倫星系](../Page/小麥哲倫星系.md "wikilink")
  - [麥哲倫雲](../Page/麥哲倫雲.md "wikilink")
  - [SN 1987A](../Page/SN_1987A.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [NASA銀河系外資料庫](http://nedwww.ipac.caltech.edu/index.html)
  - [天文詞條百科全書](http://www.daviddarling.info/encyclopedia/L/LMC.html)
  - [SEDS LMC
    page](https://web.archive.org/web/20060831173158/http://www.seds.org/messier/xtra/ngc/lmc.html)

[Category:不規則星系](../Category/不規則星系.md "wikilink")
[Category:本星系群](../Category/本星系群.md "wikilink")
[Category:麥哲倫雲](../Category/麥哲倫雲.md "wikilink")
[Category:大麥哲倫星系](../Category/大麥哲倫星系.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.
10.

11.

12.

13. \[<http://simbad.u-strasbg.fr/cgi-bin/cdsbib?2005ApJ>...627..224G
    Direct distances to Cepheids in the Large Magellanic Cloud\]

14.

15.