**大鼠狐猴**（学名: *Cheirogaleus
major*）是一种生活在[马达加斯加东海岸的](../Page/马达加斯加.md "wikilink")[狐猴](../Page/狐猴.md "wikilink")，属于[鼠狐猴属](../Page/鼠狐猴属.md "wikilink")。体毛为灰色或红褐色，眼睛周围有深色的圈。

## 参考

[Category:鼠狐猴科](../Category/鼠狐猴科.md "wikilink")