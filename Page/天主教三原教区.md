**天主教三原教区**（拉丁文：Saniüenensis）是[天主教在中国](../Page/天主教.md "wikilink")[陕西省中部建立的一个著名](../Page/陕西省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1932年，教廷把陕西中境代牧区分为5个教区：[西安代牧区](../Page/西安教区.md "wikilink")、[三原监牧区](../Page/三原教区.md "wikilink")（1944年升为代牧区）、[凤翔监牧区](../Page/凤翔教区.md "wikilink")（1942年升为代牧区）、[盩厔监牧区和](../Page/盩厔教区.md "wikilink")[同州自治区](../Page/同州教区.md "wikilink")（1935年升为监牧区）。1946年成立圣统制，这些代牧区都改设正式教区。盩厔监牧区1951年直接升为教区。

## 现状

三原教区辖咸阳、铜川2市12个县，有教友35000多名，神父34位，修女80余名。

## 历任主教

  - 三原宗座监牧
      - 司铎[班錫宜](../Page/班錫宜.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Fr. Ferdinando Fulgenzio Pasini,
        O.F.M.（后升任主教，1932年6月4日-1944年7月13日）

<!-- end list -->

  - 三原宗座代牧
      - 主教[班錫宜](../Page/班錫宜.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Ferdinando Fulgenzio Pasini,
        O.F.M.（1944年7月13日-1946年4月11日）

<!-- end list -->

  - 三原主教
      - 主教[班錫宜](../Page/班錫宜.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Ferdinando Fulgenzio Pasini,
        O.F.M.（1946年4月11日-1985年4月17日）
          - 代理主教[王秉德](../Page/王秉德.md "wikilink")（1954年-1985年11月24日）
      - 主教[宗怀德](../Page/宗怀德.md "wikilink") Bishop Joseph Zong
        Huai-de（1987年8月9日-2003年）1922年3月5日出生于三原县西阳镇武官坊新城村十一队，1949年6月5日在[通远坊天主堂被班锡宜主教祝圣为神父](../Page/通远坊天主堂.md "wikilink")，1987年8月9日为三原教区正权主教，2003年荣休。
      - 主教[兰石](../Page/兰石.md "wikilink") Bishop John Chrysostom Lan Shi
        （2003年-2008年）
      - 主教[韩英进](../Page/韩英进.md "wikilink") Bishop Joseph Han Yingjin
        （2010年6月24日-）

## 教堂

  - 三原县
      - 辕门巷[耶稣圣心主教座堂](../Page/耶稣圣心主教座堂_\(三原\).md "wikilink")（建于1935年，1992年重建）
      - 陂西乡张二册天主堂　
      - 陂西乡庙张天主堂　　
      - 陂西乡桃李天主堂　
      - 西阳镇武官坊天主堂 　
  - 富平县
      - 涝池安多尼堂
  - 泾阳县
      - 修石渡圣母无染原罪堂
      - 大训堡安多尼堂
  - 礼泉县
      - 茨林村玫瑰堂
  - 乾县
      - 马连天主堂
  - 淳化县
      - 地母庄天主堂
  - 高陵县
      - 张白若瑟堂
      - 郭路圣母七苦堂
      - 通远坊[圣母无染原罪堂](../Page/圣母无染原罪堂_\(三原\).md "wikilink")
  - 咸阳市[耶稣圣心堂](../Page/耶稣圣心堂_\(咸阳\).md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

{{-}}

[Category:中國天主教教區](../Category/中國天主教教區.md "wikilink")
[Category:陕西天主教](../Category/陕西天主教.md "wikilink")