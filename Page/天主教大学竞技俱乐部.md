**天主教大学竞技俱乐部**（**Club Deportivo Universidad
Católica**）是[智利最著名的](../Page/智利.md "wikilink")[足球俱乐部之一](../Page/足球.md "wikilink")。球队位于国家首都[圣地亚哥](../Page/圣地亚哥.md "wikilink")，球迷们通常简称球队**UC**。

## 荣誉

Image:Trofeo del Torneo Internacional de Pascua 1950.png|

<center>

</center>

Image:Copa Torneo Nacional de Ascenso 1956.JPG|

<center>

</center>

Image:Copa República.JPG|

<center>

</center>

Image:Trofeo Ciudad de Palma.JPG|

<center>

</center>

Image:Trofeo del Campeonato Nacional 1987 de Chile.JPG|

<center>

</center>

|

<center>

</center>

Image:Trofeo Apertura 1997 Chile.JPG|

<center>

</center>

Image:Huemul de Plata.JPG|

<center>

</center>

Image:Replica copa chile.JPG|

<center>

</center>

Image:Copa UC Sub 17.JPG|

<center>

</center>

Image:Manchester Premier Cup.JPG|

<center>

</center>

### 国内荣誉

  - **[智利足球甲级联赛](../Page/智利足球甲级联赛.md "wikilink")**
      - **冠军(12次)**:1949年,1954年,1961年,1966年,1984年
        1987年,1997年,2002年(春季),2005年(秋季),2010年, 2016年(秋季), 2016年
  - **[智利足协杯](../Page/智利足协杯.md "wikilink")**
      - **冠军(4)**:1983年.1991年,1995年,2011年
  - **超级杯智利**
      - **冠军(1)**:2016年
  - **智利足球乙级联赛**
      - **冠军(2)**: 1956年,1975年
  - **[Polla Gol杯](../Page/Polla_Gol杯.md "wikilink")**
      - **冠军(1)**:1984年
  - **[共和国杯](../Page/共和国杯.md "wikilink")**
      - **冠军(1)**:1984年

### 国际赛场荣誉

  - **[南美解放者杯](../Page/南美解放者杯.md "wikilink")**
      - **亚军(1):** 1993年
  - **[美洲洲际杯](../Page/美洲洲际杯.md "wikilink")** (参见备注)
      - **冠军 (1):** 1994年

<!-- end list -->

  - **备注**:
    1993年[南美解放者杯冠军](../Page/南美解放者杯.md "wikilink"),，[圣保罗拒绝参加洲际杯赛](../Page/圣保罗足球俱乐部.md "wikilink")，使得这一年的解放者被亚军天主教大学有了增补出赛的机会。

## 外部链接

  - [Official site](http://www.lacatolica.cl/)

  - [CDUCatolica.com](http://www.cducatolica.com/)

  - [Los Cruzados](http://www.loscruzados.com/)

  - [Ceatolei](https://web.archive.org/web/20070101055840/http://www.ceatolei.cl/)

[Category:南美洲天主教大学](../Category/南美洲天主教大学.md "wikilink")
[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")