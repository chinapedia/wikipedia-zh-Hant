**天主教汀州教区**（**Dioecesis
Timceuvensis**）是[罗马天主教在中国](../Page/罗马天主教.md "wikilink")[福建省设立的一个](../Page/福建省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1923年12月27日，教廷宣布，从福建北境代牧区分设**汀州监牧区**，首任宗座监牧主教为德国籍[欧培徒](../Page/欧培徒.md "wikilink")，驻扎武平。1947年5月8日，汀州监牧区升格为**汀州教区**，雷新基任主教，主教堂由武平遷長汀。1953年9月取締[聖母軍](../Page/聖母軍.md "wikilink")，正副主教雷新基、德方濟限期驅逐出境，并成立長汀天主教三自革新會\[1\]。1966年[文化大革命開始](../Page/文化大革命.md "wikilink")，天主教活動被禁止，城關教堂被佔用，三洲教堂改作畜牧場。1985年12月，長汀天主教堂歸還。

## 教堂

  - [圣母玫瑰主教座堂](../Page/圣母玫瑰主教座堂_\(长汀\).md "wikilink")

## 主教

  - [欧培徒](../Page/欧培徒.md "wikilink")（Fr. Edber M. Pelzer,
    O.P.），1925年－1945年
  - [雷新基](../Page/雷新基.md "wikilink")（Bishop Johann Werner Lesinski,
    O.P.），1947年－1963年（1953年9月被逐出大陆）

## 信徒

1950年，汀州教区有信徒4,318人\[2\]。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:福建天主教](../Category/福建天主教.md "wikilink")
[Category:龙岩宗教](../Category/龙岩宗教.md "wikilink")
[Category:长汀县](../Category/长汀县.md "wikilink")

1.  [長汀天主教史](http://www.minxi.com.cn/gxz/ct/fszj/jj3.htm)
2.  [Dioecesis
    Timceuvensis](http://www.catholic-hierarchy.org/diocese/dting.html)