**天主教济南总教区**（**Archidioecesis
Zinanensis**）是罗马天主教在中国[山东省设立的一个教区](../Page/山东省.md "wikilink")。

## 历史

1839年9月3日，教廷成立山东代牧区，任命意大利会士[罗类思为山东代牧区首任主教](../Page/罗类思.md "wikilink")，主教公署设在武城十二里庄。1885年12月2日南部分出后更名为山东北境代牧区，1924年12月3日更名为济南府代牧区，1946年4月11日更名为济南总教区。

## 教堂

  - [洪家楼耶稣圣心主教座堂](../Page/洪家楼耶稣圣心主教座堂.md "wikilink")
  - [将军庙街圣母无染原罪堂](../Page/将军庙街圣母无染原罪堂.md "wikilink")
  - [陈家楼圣若瑟堂](../Page/陈家楼圣若瑟堂.md "wikilink")
  - [尖山圣母堂](../Page/尖山圣母堂.md "wikilink")
  - [長清東莊堂](../Page/長清東莊堂.md "wikilink")

## 主教

  - [罗类思](../Page/罗类思.md "wikilink")（Bishop Lodovico Maria (dei Conti)
    Besi ）1839年-1848年
  - [江类思](../Page/江类思.md "wikilink")（Bishop Luigi Moccagatta,
    O.F.M.）1848年 – 1870年9月27日
  - [顾立爵](../Page/顾立爵.md "wikilink")(Bishop Eligio Pietro Cosi,
    O.F.M.）1870年9月27日 - 1885年1月24日
  - [李博明](../Page/李博明.md "wikilink")（Benjamino Geremia, O.F.M.
    ），1885年1月12日 - 1888年10月
  - [马天恩](../Page/马天恩.md "wikilink")（Pietro Paolo de Marchi, O.F.M.）
    1889年2月13日 - 1901年8月30日
  - [申永福](../Page/申永福.md "wikilink")（Bishop Ephrem Giesen,
    O.F.M.）1902年7月18日 – 1919年8月6日
  - [瑞明轩](../Page/瑞明轩.md "wikilink")（Bishop Adalberto Schmücker,
    O.F.M.）1920年8月2日 –1927年8月8日
  - [杨恩赉](../Page/杨恩赉.md "wikilink")（Archbishop Cyrill Rudolph Jarre,
    O.F.M.）1929年5月18日 – 1952年3月8日
  - [平达观](../Page/平达观.md "wikilink")，署理主教（Fr. John P’ing Ta-kuam,
    O.F.M.） (1952年11月7日 – ?)
  - [赵子平](../Page/赵子平.md "wikilink")（雅各伯），1912年生，1988年到2008年
  - [张宪旺](../Page/张宪旺.md "wikilink")，2008年-

## 信徒

1950年，信徒44,016人\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:济南天主教](../Category/济南天主教.md "wikilink")

1.  [Archdiocese of
    Tsinan](http://www.catholic-hierarchy.org/diocese/dtsin.html)