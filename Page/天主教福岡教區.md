**天主教福岡教區**（日語：；拉丁語：Dioecesis
Fukuokaensis）位於[日本](../Page/日本.md "wikilink")[福岡市](../Page/福岡市.md "wikilink")，是[羅馬天主教在日本設立的](../Page/羅馬天主教會.md "wikilink")16個[教區之一](../Page/教區.md "wikilink")，屬長崎教省的子教區。轄[福岡](../Page/福岡縣.md "wikilink")、[熊本](../Page/熊本縣.md "wikilink")、[佐賀三縣](../Page/佐賀縣.md "wikilink")。

## 歷史

[Giappone_-_Diocesi_di_Fukuoka.png](https://zh.wikipedia.org/wiki/File:Giappone_-_Diocesi_di_Fukuoka.png "fig:Giappone_-_Diocesi_di_Fukuoka.png")
該教區於1927年7月16日從當時的長崎教區分拆出來。

## 主教

  - 福岡教區主教（羅馬禮）
      - Fernand-Jean-Joseph Thiry主教（1927年7月14日－1930年5月10日）
      - Albert Henri Charles Breton主教（1931年6月9日－1941年1月16日）
      - 深堀仙右衛門主教（宗座署理，1941年－1944年3月9日）
      - 深堀仙右衛門主教（Dominic Senyemon Fukahori，1944年3月9日－1969年11月15日）
      - 平田三郎主教（Peter Saburo Hirata，1969年11月15日－1990年10月6日）
      - 松永久次郎主教（Joseph Hisajiro Matsunaga，1990年10月6日－2006年6月2日）
      - 宮原良治主教（Dominic Ryoji Miyahara，2008年3月19日－）

## 請參閱

  - [日本天主教教區列表](../Page/日本天主教教區列表.md "wikilink")

## 外部連結

  - [Giga-Catholic
    Information](http://www.gcatholic.com/dioceses/diocese/fuku0.htm)
  - [Catholic
    Hierarchy](http://www.catholic-hierarchy.org/diocese/dfuku.html)

[Category:日本天主教教區](../Category/日本天主教教區.md "wikilink") [Category:中央區
(福岡市)](../Category/中央區_\(福岡市\).md "wikilink")