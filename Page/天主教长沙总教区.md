**天主教长沙总教区**（**Archdioecesis
Ciamscianus**）是[罗马天主教在中国](../Page/罗马天主教.md "wikilink")[湖南省设立的一个](../Page/湖南省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1856年4月2日，教廷宣布，湖广代牧区分设湖北代牧区和**湖南代牧区**。1879年9月19日，湖南代牧区分为**湖南南境代牧区**和湖南北境代牧区，分别由意大利[方济各会和西班牙](../Page/方济各会.md "wikilink")[奥斯定会负责](../Page/奥斯定会.md "wikilink")，湖南南境代牧区首任宗座代牧主教为[范怀德](../Page/范怀德.md "wikilink")，1892年上任，驻扎衡州北门外黄沙湾。1900年7月3日，庚子[衡州教案爆发](../Page/衡州教案.md "wikilink")，衡州府城南门外的英国福音堂被焚毁。次日，3万余人冲入[黄沙湾天主教总堂](../Page/黄沙湾天主教总堂.md "wikilink")，打伤副主教[任德高](../Page/任德高.md "wikilink")，又将[董哲西神甫](../Page/董哲西.md "wikilink")（S.
Cesidio da Fossa,
OFM，1873－1900）打伤后拽至堂外烧死，然后放火烧毁了总堂和育婴堂。5日，当时正在耒阳杉木桥重建被拆毁教堂的主教范怀德接到任德高的告急信，和[安守仁神甫](../Page/安守仁.md "wikilink")（S.
Giuseppe Maria Gambaro,
OFM，1869－1900）乘船返衡，7月7日，船刚抵岸，二人被痛欧致死，并焚尸灭迹\[1\]\[2\]。此后数日之间，衡属衡阳、清泉、衡山、常宁、耒阳、安仁6县城乡大小教堂30余所全部被毁。

1924年12月3日，湖南南境代牧区以主教驻地更名为**长沙代牧区**。1925年，分设[永州监牧区](../Page/永州监牧区.md "wikilink")。1930年，分设[衡州代牧区](../Page/衡州教区.md "wikilink")。1937年，分设[湘潭监牧区](../Page/湘潭监牧区.md "wikilink")。1938年，分设[宝庆监牧区](../Page/宝庆监牧区.md "wikilink")。1946年4月11日，长沙代牧区升格为**长沙总教区**。

## 所辖教区

  - [天主教常德教区](../Page/天主教常德教区.md "wikilink")
  - [天主教沅陵教区](../Page/天主教沅陵教区.md "wikilink")
  - [天主教衡州教区](../Page/天主教衡州教区.md "wikilink")
  - [天主教澧州监牧区](../Page/天主教澧州监牧区.md "wikilink")
  - [天主教宝庆监牧区](../Page/天主教宝庆监牧区.md "wikilink")
  - [天主教湘潭监牧区](../Page/天主教湘潭监牧区.md "wikilink")
  - [天主教岳州监牧区](../Page/天主教岳州监牧区.md "wikilink")
  - [天主教永州监牧区](../Page/天主教永州监牧区.md "wikilink")

## 教堂

  - [圣母无原罪主教座堂](../Page/长沙圣母无染原罪主教座堂.md "wikilink")

## 主教

  - [范怀德](../Page/范怀德.md "wikilink")（Bishop Antonio Fantosati,
    O.F.M.），1892年－1900年
  - [翁明德](../Page/翁明德.md "wikilink")（Pellegrino Luigi Mondaini,
    O.F.M.），1902年－1930年
  - Bishop Gaudenzio Giacinto Stanchi, O.F.M. 1933年－1939年
  - [蓝泽民](../Page/蓝泽民.md "wikilink")（Archbishop Secondino Petronio
    Lacchio, O.F.M.） MONS. PETRONIUS LACCHIO
    1940年－1976年（1952年3月30日被逐出大陆\[3\]
  - [屈天錫](../Page/屈天錫.md "wikilink")，1987年－2000年\[4\]
  - [屈藹林](../Page/屈藹林.md "wikilink")，2012年4月25日晉牧

## 信徒

1950年，长沙教区有信徒9,038人\[5\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 相关条目

  - [中国天主教教区](../Page/中国天主教教区.md "wikilink")

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:湖南天主教](../Category/湖南天主教.md "wikilink")
[Category:长沙宗教](../Category/长沙宗教.md "wikilink")

1.  《拳祸一瞥》，P102
2.  [1](http://www.tianzhujiao.org/html/2006-03/1070.html)
3.  [桃園聖母聖心天主堂簡史](http://www.catholic.org.tw/taoyuanchurch/church/1history.htm)

4.  [長沙教區屈天錫主教安息主懷](http://76.75.218.2/2000/11/22/CH7150/)
5.  [Archdioecesis
    Ciamteanus](http://www.catholic-hierarchy.org/diocese/dchng.html)