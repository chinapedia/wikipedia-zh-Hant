是[日本漫畫家](../Page/日本.md "wikilink")[北条司的](../Page/北条司.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")。第一部從2001年開始在《週刊Comic
Bunch》連載，直到2010年《週刊Comic
Bunch》停刊，[單行本共](../Page/單行本.md "wikilink")33冊。漫畫第二部在2010年末在《月刊Comic
Zenon》上重新連載，單行本至2017年完结，全16冊。系列漫畫的累計發行量已達到2500萬冊\[1\]。2015年10月，由[日本電視台改拍為真人版電視連續劇](../Page/日本電視台.md "wikilink")，由[上川隆也主演](../Page/上川隆也.md "wikilink")。

根據北条司的說法，《天使心》並非《[城市獵人](../Page/城市獵人.md "wikilink")》的續集，只是延用《城市獵人》故事人物的[平行世界作品](../Page/平行世界.md "wikilink")\[2\]。

## 劇情大綱

故事一開始，**槙村香**不幸於[交通事故中逝世](../Page/交通事故.md "wikilink")，其[心臟被](../Page/心臟.md "wikilink")[移植到](../Page/器官移植.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[殺手](../Page/殺手.md "wikilink")**李香瑩**身上。之後，李香瑩脫離組織，成為**[冴羽獠](../Page/冴羽獠.md "wikilink")**的女兒，共組新的[城市獵人](../Page/城市獵人.md "wikilink")。

《天使心》劇情多著墨在槙村香與李香瑩之間的[心靈溝通部分](../Page/心靈.md "wikilink")，故整體風格比較偏向感性發展，與《城市獵人》相同角色的背景設定略有不同。例如：原本在《城市獵人》裡的**海坊主**與**美樹**這對夫妻一同經營貓眼咖啡店，但在《天使心》裡則是由海坊主（海怪）一人經營，海怪則單身，沒有美樹這個角色。而海坊主（海怪）另外收養了一名少女（美紀）。而冴羽獠跟槙村香的初次相遇也不同。還有，槙村香的姐姐從未跟香見過面。

### 跟《城市獵人》的差異

  - 《天使心》的槙村香品性善良有如天使，是朋友圈的核心。《城市獵人》沒有這樣的描述。
  - 槙村香的哥哥秀幸在《城市獵人》是死於獠的養父海原神的神經藥物「天使塵」殺手；《天使心》內，槙村秀幸是死於暗戀冴子的連環殺手。
  - 《城市獵人》記載冴羽獠與海坊主的僱傭兵故事都是發生在中南美洲，而且兩人是敵對的傭兵集團，海坊主雙目是冴羽獠所傷。但《天使心》冴羽獠與海坊主是同一僱傭兵部隊，海坊主是為了拯救被孤軍包圍的冴羽獠而失去雙目，戰事多是發生在亞洲、東南亞。
  - 《城市獵人》記載海坊主拯救了孤女美樹，美樹長大後追求海坊主，結成夫婦。《天使心》記載海坊主與戰場護士彌生相戀，而美樹(未來)是海坊主退役後收養的一個年幼女孩。

## 角色

  - 冴羽獠
    [声優](../Page/声優.md "wikilink")：[神谷明](../Page/神谷明.md "wikilink")；[梁興昌](../Page/梁興昌.md "wikilink")（台灣）；[陳欣](../Page/陳欣_\(配音員\).md "wikilink")（香港）
    初期是一名在世界各地打仗的[傭兵](../Page/傭兵.md "wikilink")，而後成了一名殺手，在暗殺槙村秀幸的任務失敗後，與槙村成為拍檔，槙村死後，與槙村秀幸的妹妹槙村香成為了拍檔，之後兩人決定結婚。但在拍婚紗照的當天，香車禍身亡。失去生活意義的獠開始過著自暴自棄的生活，直到擁有阿香心臟的女人「香瑩」的出現，冴羽獠再度有了生活重心，亦重新開始CH的業務。冴，是冱的異體字，音「互」。

<!-- end list -->

  - 李香瑩
    [声優](../Page/声優.md "wikilink")：[川崎真央](../Page/川崎真央.md "wikilink")；[許雲雲](../Page/許雲雲.md "wikilink")（台灣）；[鄭麗麗](../Page/鄭麗麗.md "wikilink")（香港）
    [台灣人](../Page/台灣.md "wikilink")，曾是職業[殺手](../Page/殺手.md "wikilink")。代號玻璃心（Glass
    Heart），為了脫離組織「正道會」不再殺人而自殺。其後移植了槙村香的心臟，成為冴羽獠和槙村香的養女。與香的性格是南轅北轍，“不同以往乾淨利落手法，這不經大腦的破壞”，但因獲得了香的心臟，繼承了香的一部分性格，更繼承了香的“鐵槌”。

<!-- end list -->

  - 槙村香
    [声優](../Page/声優.md "wikilink")：[伊倉一惠](../Page/伊倉一惠.md "wikilink")；[許雲雲](../Page/許雲雲.md "wikilink")（台灣）；[許盈](../Page/許盈.md "wikilink")（香港）
    獠最愛的女拍擋，於交通事故逝世。其心臟和靈魂便移植了香瑩的身上。當香瑩照上鏡子或受委託人士心靈困惑時，就會以影子形式再次出現。

<!-- end list -->

  - 槙村秀幸
    [声優](../Page/声優.md "wikilink")：[田中秀幸](../Page/田中秀幸.md "wikilink")；[伍博民](../Page/伍博民.md "wikilink")（香港）
    一名刑警，阿香的哥哥，小時候並不想當一名警察，但因為父親的殉職，讓他選擇了警校而非東大。之後他因強烈的正義感，成為了初代CH，並且和獠搭檔。他在之後被一名連續殺人犯殺害。

<!-- end list -->

  - 海坊主（海怪）
    [声優](../Page/声優.md "wikilink")：[玄田哲章](../Page/玄田哲章.md "wikilink")；[陳宏瑋](../Page/陳宏瑋.md "wikilink")（台灣）；[梁志達](../Page/梁志達.md "wikilink")（香港）
    前僱傭兵，獠的拍檔。失明後退役，當了「[貓之眼](../Page/貓之眼.md "wikilink")」咖啡廳 Cat's Eye
    的老闆，其咖啡廳裝有防彈玻璃及內藏很多危險武器。
    在城市獵人中本名為伊集院隼人而人稱
    Falcon（隼）（台灣版本譯為「法爾肯」），但在本作中由作者訪談得知其身分是「名為Falcon的黑人」

<!-- end list -->

  - 李堅強
    [声優](../Page/声優.md "wikilink")：[有本欽隆](../Page/有本欽隆.md "wikilink")（青年期
    [野島裕史](../Page/野島裕史.md "wikilink")）；[譚炳文](../Page/譚炳文.md "wikilink")（青年期
    [李錦綸](../Page/李錦綸.md "wikilink")）（香港）
    台灣黑幫正道會龍頭老大，香瑩的親生父親。

<!-- end list -->

  - 李謙德
    [声優](../Page/声優.md "wikilink")：有本欽隆（青年期[野島健兒](../Page/野島健兒.md "wikilink")）；（青年期
    [招世亮](../Page/招世亮.md "wikilink")）（香港）
    李堅強的孖生弟弟，為了救最珍愛的女兒（事實上姪女）香瑩，被組織的叛亂分子射殺。

<!-- end list -->

  - 野上冴子
    [声優](../Page/声優.md "wikilink")：[麻上洋子](../Page/麻上洋子.md "wikilink")；[馬君珮](../Page/馬君珮.md "wikilink")（台灣）；[陸惠玲](../Page/陸惠玲.md "wikilink")（香港）
    獠的舊相識，新宿警署署長。槙村秀幸的戀人。秀幸死後，與獠關係一直曖昧至今。

<!-- end list -->

  - 劉信宏
    [声優](../Page/声優.md "wikilink")：[鈴木千尋](../Page/鈴木千尋.md "wikilink")；[陳宏瑋](../Page/陳宏瑋.md "wikilink")（台灣）；[張裕東](../Page/張裕東.md "wikilink")（香港）
    香瑩訓練生時代的同伴，對香瑩抱有好感，獲李大人“恩准”留在新宿，現在於海坊主的咖啡廳工作。

<!-- end list -->

  - 陳侍從長
    [声優](../Page/声優.md "wikilink")：[矢田耕司](../Page/矢田耕司.md "wikilink")；[黃子敬](../Page/黃子敬.md "wikilink")（香港）
    李大人的左右手，在新宿有一間叫“玄武門”的中華料理店，暗中照顧香瑩，向李大人報告其女兒的情況。

<!-- end list -->

  - 楊芳玉
    [声優](../Page/声優.md "wikilink")：[緒方惠美](../Page/緒方惠美.md "wikilink")；[傅其慧](../Page/傅其慧.md "wikilink")（台灣）；[謝潔貞](../Page/謝潔貞.md "wikilink")
    （香港）
    同樣是獠的舊相識，統領僱傭兵部隊“黑豹”，獠認為她是危險的女人。獠曾許諾娶楊芳玉。楊芳玉以此為由，不斷要脅獠，讓獠十分懼怕。

<!-- end list -->

  - 餅山秀夫
    [声優](../Page/声優.md "wikilink")：[龍田直樹](../Page/龍田直樹.md "wikilink")；[陳永信](../Page/陳永信.md "wikilink")（香港）
    別名阿餅，原是黑幫的小頭目，現在在“玄武門”工作。

<!-- end list -->

  - 美紀未來
    [声優](../Page/声優.md "wikilink")：[小山茉美](../Page/小山茉美.md "wikilink")；[傅其慧](../Page/傅其慧.md "wikilink")（台灣）；[楊善諭](../Page/楊善諭.md "wikilink")（香港）
    以前是街童，其實是A國國王的私生女。因國家陷入爭奪王位的問題，美紀的母親逃亡到日本，成為遊民，在母親死後，美紀經常會念故事書給寂寞的人聽，之後成為法爾肯（海坊主）的養女。

<!-- end list -->

  - 變色龍（chameleon）
    殺手，冷酷，絕不透露自我感情。擅長易容改裝，不論男女老幼都十分相似。似乎是男性，但極之喜歡以性感女裝示人，極之喜歡俊美男子。發現香瑩有殺手的兇殘本能，渴望將她培養為冷酷殺手，視香瑩深處的槙村香善良內心為一大障礙，不斷為城市獵人找難題麻煩，希望藉以摧毀香瑩的善良人格。變色龍的心靈、動機都十分神秘，似乎毫無感情，時而兇殘，時而尋迷玩樂而忘掉行動原意。在第二季，海坊主的養女美紀，發現自己無法閱讀變色龍的面目。變色龍發覺了美紀的面目讀心術特殊能力，視美紀為他的新目標。

<!-- end list -->

  - 笠井彌生（君塚彌生）
    冴羽獠跟海坊主僱傭兵部隊的護士，與海坊主漸生情愫。可惜部隊戰敗，海坊主與彌生失去聯絡，互相認定對方已經陣亡。退出戰場，建立家庭。後來與丈夫死於空難。遺下女兒笠井葉月。

<!-- end list -->

  - 笠井葉月
    新宿一診所的護士，自小聽父母說戰場往事，知道海坊主與母親的故事。偶遇海坊主，心生愛慕，時常出入「貓之眼」咖啡店。後來，向海坊主表白，戀情開始。葉月與海坊主的養女美紀關係良好，有如一對母女，似欲與海坊主建立家庭為目標。

## 電視動畫

### 主題歌

#### 片頭曲

  - 「虚ろな心」（第1話）
    作曲：岩崎琢
  - 「[Finally](../Page/to_YOU.md "wikilink")」（第2話 - 第24話）
    作詞 - [藤林聖子](../Page/藤林聖子.md "wikilink") / 作曲 - 森元康介 / 編曲 - Takuya
    Harada / 歌 - [Sowelu](../Page/Sowelu.md "wikilink")
  - 「[Lion](../Page/Lion_\(玉置浩二\).md "wikilink")」（第25話 - 第38話）
    作詞 - [松井五郎](../Page/松井五郎.md "wikilink") / 作曲・歌 -
    [玉置浩二](../Page/玉置浩二.md "wikilink")
  - 「Battlefield of Love」（第39話 - 最終話）
    作詞・作曲・歌 - [伊沢麻未](../Page/伊沢麻未.md "wikilink") / 編曲 - DJ CLAZZIQUAI

#### 片尾曲

  - 「虚ろな心」(第1話)
    作曲：岩崎琢
  - 「[誰かが君を想ってる](../Page/誰かが君を想ってる.md "wikilink")」（第2話 - 第12話、第14話 -
    第19話、第24話、最終話）
    作詞・歌 - [Skoop On Somebody](../Page/Skoop_On_Somebody.md "wikilink")
    / 作曲・編曲 - 土肥真生 + SOS
  - 「[Daydream
    Tripper](../Page/Daydream_Tripper.md "wikilink")」（第13話、第20話
    - 第23話）
    作詞 - [森雪之丞](../Page/森雪之丞.md "wikilink") / 作曲 - 石井妥師 / 編曲 - 土橋安騎夫 &
    石井妥師 / 歌 - [U WAVE](../Page/U_WAVE.md "wikilink")
  - 「My Destiny」（第25話 - 第41話）
    作詞・作曲・編曲・歌 - [カノン](../Page/カノン_\(歌手\).md "wikilink")
  - 「哀しみのAngel」（第42話 - 第46話）
    作詞 - Satomi / 作曲 - 羽場仁志 / 編曲 - 水島康貴 / 歌 -
    [稲垣潤一](../Page/稲垣潤一.md "wikilink")
  - 「FEEL ME」（第47話 - 第49話）
    作詞・作曲 - 中西圭三 / 編曲 -上野圭市 / 歌 - [中西圭三](../Page/中西圭三.md "wikilink")

### 每集標題

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>中文標題</p></th>
<th><p>日語標題</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td><p>玻璃的心臟</p></td>
<td><p>|- </p></td>
</tr>
<tr class="even">
<td><p>第3話</p></td>
<td><p>XYZ的街</p></td>
<td><p>|- </p></td>
</tr>
<tr class="odd">
<td><p>第5話</p></td>
<td><p>永別了，香</p></td>
<td><p>|- </p></td>
</tr>
<tr class="even">
<td><p>第7話</p></td>
<td><p>我應該愛的街道</p></td>
<td><p>|- </p></td>
</tr>
<tr class="odd">
<td><p>第9話</p></td>
<td><p>香塋，遺失的名字</p></td>
<td><p>|- </p></td>
</tr>
<tr class="even">
<td><p>第11話</p></td>
<td><p>父女的時間</p></td>
<td><p>|- </p></td>
</tr>
<tr class="odd">
<td><p>第13話</p></td>
<td><p>李大人的禮物</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14話</p></td>
<td><p>復活的城市獵人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第15話</p></td>
<td><p>幫我找爸爸</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16話</p></td>
<td><p>成為獵人的資格</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第17話</p></td>
<td><p>夢中的相遇</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第18話</p></td>
<td><p>父女的羈絆</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19話</p></td>
<td><p>陳老人的店</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第20話</p></td>
<td><p>宿命的前奏曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第21話</p></td>
<td><p>悲傷的守護者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第22話</p></td>
<td><p>不公平的幸福</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第23話</p></td>
<td><p>啟程的旋律</p></td>
<td><p>|-</p></td>
</tr>
<tr class="even">
<td><p>第25話</p></td>
<td><p>求死的委託人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第26話</p></td>
<td><p>再一次回到那時刻</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第27話</p></td>
<td><p>我，戀愛了嗎？</p></td>
<td><p>|-</p></td>
</tr>
<tr class="odd">
<td><p>第29話</p></td>
<td><p>我的妹妹…香</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第30話</p></td>
<td><p>這裹是我的全部</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第31話</p></td>
<td><p>在最後一晚看到的奇蹟</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第32話</p></td>
<td><p>組織派來的女子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第33話</p></td>
<td><p>神賜予的女兒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第34話</p></td>
<td><p>兩人的決心</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第35話</p></td>
<td><p>面向未來</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第36話</p></td>
<td><p>傳遞幸福的女子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第37話</p></td>
<td><p>一塵不染的心</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第38話</p></td>
<td><p>請當我的眼睛</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第39話</p></td>
<td><p>委託人是女明星</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第40話</p></td>
<td><p>蜜奇所隱藏的秘密</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第41話</p></td>
<td><p>自己的所在</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第42話</p></td>
<td><p>只屬於兩人的訊息</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第43話</p></td>
<td><p>我平日的生活</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第44話</p></td>
<td><p>為了我們共同的孩子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第45話</p></td>
<td><p>人體核彈頭，楊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第46話</p></td>
<td><p>慈母心</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第47話</p></td>
<td><p>光明的未來？</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第48話</p></td>
<td><p>漸行漸近的命運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第49話</p></td>
<td><p>拯救我的生命</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第50話</p></td>
<td><p>最後的禮物</p></td>
<td></td>
</tr>
</tbody>
</table>

### 製作團隊

  - 原作：北条司\[3\]
  - 企劃：諏訪道彦
  - 監督：平野俊貴
  - 系列構成：植竹須美男
  - 角色設計、總作畫監督：西城隆詞（1－24話）→青野厚司（11－50話）
  - 美術監督：佐藤ヒロム、本田修
  - 色彩設計：小林美代子
  - 撮影監督：桑良人
  - 編輯：田熊純
  - 音樂：岩崎琢
  - 音響監督：長崎行男
  - 首席製作人：諏訪道彦、吉岡昌仁、植田益朗
  - 製作人：北田修一、西村政行、高橋優
  - 動畫製作：[TMS娛樂](../Page/TMS娛樂.md "wikilink")
  - 製作：天使心製作委員會（[讀賣電視台](../Page/讀賣電視台.md "wikilink")、TMS娛樂、[Aniplex](../Page/Aniplex.md "wikilink")、讀賣電視事業）

### 播出頻道

《天使心》[電視動畫版是由](../Page/電視動畫.md "wikilink")[讀賣電視台](../Page/讀賣電視台.md "wikilink")、[TMS
Entertainment](../Page/TMS_Entertainment.md "wikilink")、[Aniplex](../Page/Aniplex.md "wikilink")、[讀賣電視企業聯合製作](../Page/讀賣電視企業.md "wikilink")，2005年10月至2006年9月由讀賣電視台、[日本電視台](../Page/日本電視台.md "wikilink")、[中京電視台](../Page/中京電視台.md "wikilink")、[札幌電視台](../Page/札幌電視台.md "wikilink")、[山形放送](../Page/山形放送.md "wikilink")、[靜岡第一電視台](../Page/靜岡第一電視台.md "wikilink")、[西日本放送](../Page/西日本放送.md "wikilink")、[福岡放送](../Page/福岡放送.md "wikilink")、[長崎國際電視台與](../Page/長崎國際電視台.md "wikilink")[鹿兒島讀賣電視台在日本國內播映](../Page/鹿兒島讀賣電視台.md "wikilink")，2006年4月至2007年3月由[TV新潟放送網與](../Page/TV新潟放送網.md "wikilink")[Kids
Station在日本國內播映](../Page/Kids_Station.md "wikilink")，2007年1月至2007年3月由[千葉電視台與](../Page/千葉電視台.md "wikilink")[埼玉電視台在日本國內播映第](../Page/埼玉電視台.md "wikilink")1至13話，2007年3月起由[栃木電視台在日本國內播映](../Page/栃木電視台.md "wikilink")。

《天使心》台灣代理權是由[曼迪傳播取得](../Page/曼迪傳播.md "wikilink")，2007年3月22日至2007年10月17日於[中視無線台以](../Page/中視無線台.md "wikilink")[雙語](../Page/雙語.md "wikilink")（主聲道為[華語](../Page/華語.md "wikilink")[配音](../Page/配音.md "wikilink")，副聲道為[日語原音](../Page/日語.md "wikilink")）播映完畢。而香港在[無綫電視](../Page/無綫電視.md "wikilink")[J2台以雙語](../Page/J2台.md "wikilink")（主聲道為[粵語](../Page/粵語.md "wikilink")[配音](../Page/配音.md "wikilink")，副聲道為[日語原音](../Page/日語.md "wikilink")）於SUN
1:30am–2:45am （重播 SUN 11:15pm–00:05am） 第47集開始改為 SAT 11:15pm–12:05am （重播
SUN 9:30pm–10:30pm）播放動畫版。

## 電視劇

，2015年10月由[日本電視台改編自日本漫畫家](../Page/日本電視台.md "wikilink")[北條司漫畫作品](../Page/北條司.md "wikilink")《天使心》之同名連續劇，於每週日22:30
-
23:25（[JST](../Page/日本標準時.md "wikilink")）[週日連續劇時段播出](../Page/日本電視台週日連續劇.md "wikilink")\[4\]，由[上川隆也](../Page/上川隆也.md "wikilink")
主演\[5\]。

### 主要角色

|        |                                         |                                           |
| ------ | --------------------------------------- | ----------------------------------------- |
| **角色** | **演員**                                  | **介紹**                                    |
| 冴羽獠    | [上川隆也](../Page/上川隆也.md "wikilink")      | 此劇主角，為接受委託清除地方上不良分子的「城市獵人」。未婚妻槙村香因意外喪生。   |
| 香瑩     | [三吉彩花](../Page/三吉彩花.md "wikilink")\[6\] | 被組織養育成人的職業殺手。在槙村香的心臟移植至自己身上後，也同時擁有了槙村的記憶。 |
| 槙村香    | [相武紗季](../Page/相武紗季.md "wikilink")\[7\] | 冴羽獠的未婚妻，原為花園診所護士。不幸過世後，心臟被殺手組織移植至香瑩身上。    |

#### 咖啡廳

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>介紹</strong></p></td>
</tr>
<tr class="even">
<td><p>海坊主（Falcon）</p></td>
<td><p>[8]</p></td>
<td><p>咖啡廳老闆。雙眼全盲，擁有強大火力。收留從組織脫逃的劉信宏。</p></td>
</tr>
<tr class="odd">
<td><p>劉信宏</p></td>
<td><p><a href="../Page/三浦翔平.md" title="wikilink">三浦翔平</a>[9]</p></td>
<td><p>與香瑩同組織的殺手。因故脫離組織，對香瑩有好感。</p></td>
</tr>
<tr class="even">
<td><p>波莉（ホーリー）</p></td>
<td><p><a href="../Page/山寺宏一.md" title="wikilink">山寺宏一</a>[10]</p></td>
<td><p>男大姊三人組之一，新宿的包打聽情報通。</p></td>
</tr>
<tr class="odd">
<td><p>望（モッチー）</p></td>
<td><p><a href="../Page/猩爺.md" title="wikilink">猩爺</a></p></td>
<td><p>男大姊三人組之一。</p></td>
</tr>
<tr class="even">
<td><p>洛可（ロッコ）</p></td>
<td><p><a href="../Page/戶塚純貴.md" title="wikilink">戶塚純貴</a></p></td>
<td><p>男大姊三人組之一。</p></td>
</tr>
</tbody>
</table>

#### 新宿西警察署

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>介紹</strong></p></td>
</tr>
<tr class="even">
<td><p>野上冴子</p></td>
<td><p><a href="../Page/高島禮子.md" title="wikilink">高島禮子</a>（特別演出）[11]</p></td>
<td><p>警署主管，為冴羽獠舊識。</p></td>
</tr>
<tr class="odd">
<td><p>小宮山忍</p></td>
<td><p>[12]</p></td>
<td><p>警署警員。</p></td>
</tr>
</tbody>
</table>

#### 花園診所

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>介紹</strong></p></td>
</tr>
<tr class="even">
<td><p>醫生</p></td>
<td><p>[13]</p></td>
<td><p>開設花園診所。槙村香生前工作之處的醫生，因而認識冴羽獠。</p></td>
</tr>
<tr class="odd">
<td><p>智</p></td>
<td><p>[14]</p></td>
<td><p>花園診所的護士。</p></td>
</tr>
</tbody>
</table>

#### 組織Region

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>介紹</strong></p></td>
</tr>
<tr class="even">
<td><p>李堅強</p></td>
<td><p>[15]</p></td>
<td><p>組織的幕後BOSS，香瑩的親生爸爸</p></td>
</tr>
<tr class="odd">
<td><p>卡利托</p></td>
<td><p>[16]</p></td>
<td><p>組織殺手，聽命於李堅強，目標為補抓香瑩，若違抗則格殺勿論。</p></td>
</tr>
</tbody>
</table>

### 每集客串

  - 第1話

<!-- end list -->

  -
    河本麗子：[小澤真珠](../Page/小澤真珠.md "wikilink")
    鈴木研吾：[野間口徹](../Page/野間口徹.md "wikilink")

<!-- end list -->

  - 第2話・第6話

<!-- end list -->

  -
    槇村秀幸：[葛山信吾](../Page/葛山信吾.md "wikilink")

<!-- end list -->

  - 第3話

<!-- end list -->

  -
    小佐藤：[山田絹代](../Page/山田絹代.md "wikilink")
    留（トメ）：[螢雪次朗](../Page/螢雪次朗.md "wikilink")

<!-- end list -->

  - 第4話

<!-- end list -->

  -
    高畑稔：[鳥羽潤](../Page/鳥羽潤.md "wikilink")
    倉本彩菜：-{[高田里穂](../Page/高田里穂.md "wikilink")}-（少女時期：[林香帆](../Page/林香帆.md "wikilink")）
    倉本沙織：[中川真櫻](../Page/中川真櫻.md "wikilink")
    倉本莊子：[朝加真由美](../Page/朝加真由美.md "wikilink")

<!-- end list -->

  - 第5話

<!-- end list -->

  -
    白蘭：[前田亞季](../Page/前田亞季.md "wikilink")
    早川俊輔：[岩城滉一](../Page/岩城滉一.md "wikilink")

<!-- end list -->

  - 第6話

<!-- end list -->

  -
    遠山一真：[澀谷謙人](../Page/澀谷謙人.md "wikilink")（少年時期：[澀谷龍生](../Page/澀谷龍生.md "wikilink")）

<!-- end list -->

  - 第7話

<!-- end list -->

  -
    茅野夢：[石井萌萌果](../Page/石井萌萌果.md "wikilink")
    高波遙：[松本若菜](../Page/松本若菜.md "wikilink")
    茅野英介：[水橋研二](../Page/水橋研二.md "wikilink")
    風間雅臣：[加藤虎之介](../Page/加藤虎之介.md "wikilink")

### 製作團隊

  - 編劇：高橋悠也
  - 導演：狩山俊輔
  - 音樂：金子隆博
  - 主題曲：〈〉[西內麻里亞](../Page/西內麻里亞.md "wikilink")（[SONIC
    GROOVE](../Page/SONIC_GROOVE.md "wikilink")）
  - 音效設計：
  - 動作指導：
  - 手槍效果：早川光
  - 技術協力：NTV Technical Resources、
  - 美術協力：日本電視台ART
  - 音響效果：Spot
  - 企畫協力：星野由宇
  - 原作協力：堀江信彦（）
  - 宣傳協力：飛田野和彦（COAMIX）
  - 製作統籌：伊藤響
  - 製作人：次屋尚、千葉行利（K Factory）、宮川晶（K Factory）
  - 製作協力：
  - 攝影棚：國際放映
  - 製作：日本電視台

### 播出日程與收視率

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>播出日</p></th>
<th><p>日文標題</p></th>
<th><p>中文標題</p></th>
<th><p>劇本</p></th>
<th><p>導演</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>10月11日</p></td>
<td></td>
<td><p>傳說的城市獵人復活！ 已逝未婚妻的心臓被奪走！現身的謎樣美少女…那是命運的再會</p></td>
<td><p>高橋悠也</p></td>
<td><p>狩山俊輔</p></td>
<td><p>12.5%</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>10月18日</p></td>
<td></td>
<td><p>何謂活著？城市獵人不為人知的的出生真相！</p></td>
<td><p>鈴木勇馬</p></td>
<td><p>10.0%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>10月25日</p></td>
<td></td>
<td><p>幸福的少女教我的…重要的承諾</p></td>
<td><p>狩山俊輔</p></td>
<td><p>8.1%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>11月1日</p></td>
<td></td>
<td><p>姐妹間的羈絆與生命的證明！城市獵人復活</p></td>
<td><p>鈴木勇馬</p></td>
<td><p>10.3%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>11月8日</p></td>
<td></td>
<td><p>暗殺計畫與自己愛上的女間諜之取捨</p></td>
<td><p>久保田充</p></td>
<td><p>7.6%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>11月15日</p></td>
<td></td>
<td><p>奪走最愛的人性命的殺人犯之事實真相</p></td>
<td><p>高橋悠也<br />
根津大樹</p></td>
<td><p>狩山俊輔</p></td>
<td><p>7.1%</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>11月22日</p></td>
<td></td>
<td><p>是我殺了那孩子的父親</p></td>
<td><p>高橋悠也</p></td>
<td><p>鈴木勇馬</p></td>
<td><p>9.0%</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>11月29日</p></td>
<td></td>
<td><p>被囚禁的是香瑩 終於到了最終章！</p></td>
<td><p>久保田充</p></td>
<td><p>8.5%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>12月6日</p></td>
<td><p>{{lang|ja|衝撃の真実!全ては愛する者のために</p></td>
<td><p>}}</p></td>
<td><p>震撼的真相！！為了我所愛的人們！</p></td>
<td><p>狩山俊輔</p></td>
<td><p>7.8%</p></td>
</tr>
<tr class="even">
<td><p>平均收視率9.3%（收視率由<a href="../Page/Video_Research.md" title="wikilink">Video Research於日本</a><a href="../Page/關東地方.md" title="wikilink">關東地區統計</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 軼事

  - 《天使心》已經是第三次北條司在他自己的作品中使用「貓眼咖啡店」，其餘兩次分別在《[貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")》和《[城市獵人](../Page/城市獵人.md "wikilink")》中。有趣的是在每個作品中，貓眼咖啡店的店主都是不同人：在《貓眼三姐妹》中店主為貓眼三姐妹，在《城市獵人》中店主為海坊主及美樹（後爲夫婦），在《天使心》中店主為海坊主一人。
  - 《天使心》電視動畫版聲優[有本欽隆也同時在](../Page/有本欽隆.md "wikilink")《城市獵人》的電影中為野上冴子的父親配音。
  - 《天使心》電視動畫版第42至43話中出現之板東實道由[內海賢二配音](../Page/內海賢二.md "wikilink")。內海賢二在名作《[北斗之拳](../Page/北斗之拳.md "wikilink")》中擔任拳王拉奧的配音，和擔任拳四郎配音的[神谷明分別在片中留下了動畫史上二句經典台詞](../Page/神谷明.md "wikilink")——「吾之生涯一片無悔！」與「你已經死了！」。更有趣的是，内海賢二還在《貓眼》電視動畫版中為内海俊夫的課長配音。
  - 《天使心》漫畫第二部在結集成單行本時，將在月刊 Comic Zenon 時的倒數第二回(第79回「凶彈的行踪」)及最終回(第80回
    「Forever, Angel Heart\!\!」)合二為一，並將標題改為「Angel
    City」。所以如果根據單行本回數計，《天使心》全篇漫畫為442回（第一部363回+第二部79回）；而實際上連載回數是443回。（番外篇「一夜的友情」及將第109回大幅加筆的特別篇「獠的求婚」不包括在內。）

## 參考資料

## 相關條目

  - [城市獵人](../Page/城市獵人.md "wikilink")
  - [猫眼三姐妹](../Page/猫眼三姐妹.md "wikilink")（貓眼）

## 外部連結

  - [](http://www.ytv.co.jp/angelheart/) 天使心動畫官方網站

  - [](https://web.archive.org/web/20070920221549/http://www.hojo-tsukasa.com/voice-audition/)
    北条司官方網站

  - [Sweeper Office 北条司情報，天使心文字連載](http://www.netlaputa.ne.jp/~arcadia/)

  - [](http://www.ntv.co.jp/angelheart/) 天使心電視劇日本電視台官方網站

  - [城市獵人天使心](http://japan.videoland.com.tw/channel/angelheart/index.html)天使心電視劇緯來日本台官方網站

  -
  -
[Category:城市猎人](../Category/城市猎人.md "wikilink")
[Category:北条司](../Category/北条司.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:青年漫畫](../Category/青年漫畫.md "wikilink")
[Category:新宿背景作品](../Category/新宿背景作品.md "wikilink")
[Category:2005年日本電視動畫](../Category/2005年日本電視動畫.md "wikilink")
[Category:Aniplex](../Category/Aniplex.md "wikilink") [Category:TMS
Entertainment](../Category/TMS_Entertainment.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:細胞記憶題材作品](../Category/細胞記憶題材作品.md "wikilink")
[Category:殺手主角題材動畫](../Category/殺手主角題材動畫.md "wikilink")
[Category:2015年開播的日本電視劇集](../Category/2015年開播的日本電視劇集.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:日本動作劇](../Category/日本動作劇.md "wikilink")
[Category:殺手主角題材漫畫](../Category/殺手主角題材漫畫.md "wikilink")
[Category:日本電視台週日晚間十點半連續劇](../Category/日本電視台週日晚間十點半連續劇.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")

1.  <https://www.mangazenkan.com/ranking/books-circulation.html>

2.  《天使心》單行本第一期封面內頁聲稱。

3.  [エンジェル・ハート
    (2005)](http://www.allcinema.net/prog/show_c.php?num_c=323500)allcinema

4.

5.

6.

7.

8.
9.

10.
11.
12.
13.
14.
15.
16.