**天兔座 R**
(**R Lep**)，通常被稱為**欣德的紅星**\[1\]，是位於[天兔座鄰近](../Page/天兔座.md "wikilink")[波江座邊界的一顆](../Page/波江座.md "wikilink")[變星](../Page/變星.md "wikilink")。在右邊的圖中以**閃爍的紅圈**標示出來\[2\]。

它是一顆[碳星](../Page/碳星.md "wikilink")，呈現出明顯的[紅色](../Page/紅色.md "wikilink")。他以[英國](../Page/英國.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")[約翰·羅素·欣德之名為名](../Page/約翰·羅素·欣德.md "wikilink")，是因為欣德在1845年的觀測使它聲名大噪。它的[光度在](../Page/光度.md "wikilink")5.5至11.7等之間變化，週期是418至441天，但是最近的測量得到的週期是427.07天。它可能還有一個長達40年的次要週期\[3\]。

在它的光度最亮時，天兔座
R的顏色經常被認為是會強烈冒煙的紅色，但是這種感受是難以用言語來形容的。當他光度昏暗時，它的顏色會更紅，這個現象以14.5個月的週期反復著。在這時，他很容易成為全天可見顏色最紅恆星的候選人，但這種看法仍有爭議。它之所以如此的紅，可能是外層大氣將[可見光](../Page/可見光.md "wikilink")[光譜中的藍色過濾掉了](../Page/光譜.md "wikilink")。這顆恆星的發現者，欣德在報告中說："看來就像是黑色背景上的一滴血\[4\]。"

## 參考資料

## 外部連結

  - [Best of the Advanced Observation Program: R
    Leporis](http://www.noao.edu/outreach/aop/observers/rleporis.html)
  - [USA Today.com - NightSky: The Hare and the
    Dove](http://www.usatoday.com/tech/news/2004-01-30-nightsky_x.htm)
  - [Smoky Mountain Astronomical Society - Hind's Crimson Star: R
    Leporis](http://www.smokymtnastro.org/scraps/Jan2004.pdf)
  - [Geody Hinds Crimson
    Star](http://www.geody.com/geospot.php?world=space&ufi=23203&alc=hnd)

[Category:C型恆星](../Category/C型恆星.md "wikilink")
[Category:米拉變星](../Category/米拉變星.md "wikilink")
[Category:天兔座](../Category/天兔座.md "wikilink")
[031996](../Category/HD天體.md "wikilink")
[023203](../Category/Hipparcos_objects.md "wikilink")

1.
2.
3.
4.  p. 269, [*Star-names and their
    meanings*](http://books.google.com/books?id=5xQuAAAAIAAJ), Richard
    Hinckley Allen, New York: G. E. Stechert, 1899.