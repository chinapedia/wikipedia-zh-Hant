**天衛五**（[英文](../Page/英文.md "wikilink")：，，或音譯：**米兰达**）是[天王星主要的衛星中最小與最內側的一顆](../Page/天王星.md "wikilink")，於1948年2月16日被[古柏](../Page/杰拉德·柯伊伯.md "wikilink")（）在[麥唐納天文台發現](../Page/麥唐納天文台.md "wikilink")，以[莎士比亚歌劇](../Page/威廉·莎士比亚.md "wikilink")《[暴風雪](../Page/暴風雪.md "wikilink")》中[Prospero的女兒命名](../Page/Prospero.md "wikilink")\[1\]，是天王星的第五顆衛星。

到目前為止，只有[航海家2號的探測](../Page/航海家2號.md "wikilink")，曾在1986年1月飛越過米蘭達南半球時，拍攝過他朝向[太陽那一面唯一的近距離特寫照](../Page/太陽.md "wikilink")，所以只有這一部分曾被研究過，顯示他是天王星的系統中最活躍的地質。

## 物理性質

[Miranda_scarp.jpg](https://zh.wikipedia.org/wiki/File:Miranda_scarp.jpg "fig:Miranda_scarp.jpg")
(Verona Rupes)的特寫鏡頭，這是米蘭達上20公里高的斷層懸崖，也是太陽系中最高的峭壁。\[2\]\]\]

米蘭達的表面也許主要是碎冰、低密度[矽酸鹽和](../Page/矽酸鹽.md "wikilink")[有機化合物組成的岩石](../Page/有機化合物.md "wikilink")。米蘭達表面殘破有如補丁的地形，表明在這顆衛星上曾有強烈的地質活動進行過，才會有巨大的峽谷交叉往來於表面。

被稱為[冕狀物](../Page/冕狀物.md "wikilink")（coronae）的巨大溝槽結構，可能是被溫暖的冰[刺穿或湧出造成的](../Page/刺穿.md "wikilink")\[3\]\[4\]
。這種作用可能改變了衛星內部的密度分佈，也可能造成米蘭達自身的重組。\[5\]
相似的情況相信也曾在[土星的衛星](../Page/土星.md "wikilink")[恩塞拉都斯發生過](../Page/土衛二.md "wikilink")。這種活動的能源被認為是來自天王星的潮汐力，可能在過去曾和其他天王星的衛星有軌道共振的關係。

米蘭達過去的地質活動相信在軌道離心率比目前大時，曾經歷過[潮汐加熱](../Page/潮汐加速#潮汐熱.md "wikilink")。在他早期的歷史，米蘭達曾經和[烏伯瑞爾有](../Page/天衛二.md "wikilink")3:1的[軌道共振](../Page/軌道共振.md "wikilink")，之後才從那種狀態脫離\[6\]。共振會使[軌道離心率增加](../Page/軌道離心率.md "wikilink")，隨著時間的變化，由天王星產生的[潮汐力引起潮汐摩擦](../Page/潮汐力.md "wikilink")，導致衛星內部被加熱。在天王星的系統中，由於行星的[扁率和相對於衛星的尺度不是很大](../Page/扁率.md "wikilink")，衛星要從共振的軌道中脫離比在[木星或](../Page/木星.md "wikilink")[土星的系統中容易](../Page/土星.md "wikilink")。對一顆靠近行星的衛星而言，米蘭達的軌道傾斜（4.34°）是很大的，因此米蘭達能從與[烏伯瑞爾的次要的共振中逃逸而出](../Page/天衛二.md "wikilink")，而這個逃脫的機制相信可以解釋為何他的軌道傾斜超過天王星其他大衛星的10倍以上（參見[天王星的衛星](../Page/天王星的衛星.md "wikilink")）\[7\]\[8\]。

早期的理論，在[航海家2號飛掠之後的短時間但現在已經被屏棄了](../Page/航海家2號.md "wikilink")，認為米蘭達的前身曾被巨大的撞擊擊碎掉，然後碎片再重新聚集呈現在這種奇怪的模樣\[9\]。

[Miranda_eclipse.jpg](https://zh.wikipedia.org/wiki/File:Miranda_eclipse.jpg "fig:Miranda_eclipse.jpg")，米蘭達越過天王星的中心造成一次短暫的[日食](../Page/日食.md "wikilink")。\]\]

科學家在米蘭達上發現了下列的[地質特徵](../Page/地質.md "wikilink")：

  - [撞擊坑](../Page/撞擊坑.md "wikilink")
  - [冕狀物](../Page/冕狀物.md "wikilink")（大的[卵圓形形狀](../Page/卵圓形.md "wikilink")）
  - [區域](../Page/區域.md "wikilink")（地質學的領域）
  - [斷崖](../Page/斷崖.md "wikilink")（[懸崖](../Page/懸崖.md "wikilink")）
  - [溝槽](../Page/溝槽.md "wikilink")

## 瑣事

  - [Astronomy
    Domine](../Page/Astronomy_Domine.md "wikilink")：搖滾歌手[平克·佛洛伊德創作的一首歌曲](../Page/平克·佛洛伊德.md "wikilink")，裡面提到天衛三、天衛四和米蘭達（天衛五）。

## 參見

  - [天衛五表面特徵列表](../Page/天衛五表面特徵列表.md "wikilink")

## 參考

## 外部連結

  - [杰勒德 P. 古柏., "天王星的第五顆衛星", PASP **61** (1949)
    129](http://adsabs.harvard.edu//full/seri/PASP./0061//0000129.000.html)
  - [八大行星–米蘭達](http://www.nineplanets.org/miranda.html)
  - [觀看太陽系–米蘭達，天王星的一顆衛星](http://www.solarviews.com/eng/miranda.htm)

[天卫05](../Category/天王星的卫星.md "wikilink")
[Category:天卫五](../Category/天卫五.md "wikilink")

1.  Kuiper, G. P., [*The Fifth Satellite of
    Uranus*](http://adsabs.harvard.edu//full/seri/PASP./0061//0000129.000.html),
    Publications of the Astronomical Society of the Pacific, Vol. 61,
    No. 360, p. 129, June 1949
2.
3.
4.
5.
6.
7.
8.
9.