**天卫十九**（S/1999 U 1,
Setebos）是环绕[天王星运行的一颗](../Page/天王星.md "wikilink")[卫星](../Page/卫星.md "wikilink")，**S/1999 U 1**.\[1\]，1999年被發現。

## 參考

## 連結

  - [Setebos
    Profile](https://web.archive.org/web/20070609142446/http://solarsystem.nasa.gov/planets/profile.cfm?Object=Ura_Setebos)（by
    [NASA's Solar System Exploration](http://solarsystem.nasa.gov)）
  - [David Jewiit
    pages](https://web.archive.org/web/20070624084542/http://www.ifa.hawaii.edu/~jewitt/irregulars.html)
  - [Uranus' Known
    Satellites](https://web.archive.org/web/20131015094120/http://www.dtm.ciw.edu/users/sheppard/satellites/urasatdata.html)（[斯科特·謝帕德](../Page/斯科特·謝帕德.md "wikilink")）
  - [MPC: Natural Satellites Ephemeris
    Service](http://www.minorplanetcenter.org/iau/NatSats/NaturalSatellites.html)

[天卫19](../Category/天王星的卫星.md "wikilink")

1.