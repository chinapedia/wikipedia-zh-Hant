**天宇魔劫**是[台灣電視戲劇](../Page/台灣.md "wikilink")[天宇布袋戲中的一個劇集](../Page/天宇布袋戲.md "wikilink")。

## 劇情簡介

### 黑流天宇無天道

  - [耶修渡重回天宇](../Page/耶修渡.md "wikilink")，行無天之道，引發黑流蔓延，天宇血劫。
  - [蛾門強取冰海蛟鬚](../Page/蛾門.md "wikilink")，欲介入黑流之爭，立場難測？
  - 蛟龍蚡言，[冉七](../Page/冉七.md "wikilink")[管九之爭勢如水火](../Page/管九.md "wikilink")，東北調，西南曲，將帶來什麼變數？
  - [紀子焉遊走各勢力](../Page/紀子焉.md "wikilink")，企圖為何？

### 靈海鍛身飛凡塵

  - [藥叉共王顛倒靈海](../Page/藥叉共王.md "wikilink")，以自身鍛造於天山，重生！
  - 仙府君首毒計生，菊殘兄弟雙雙斷魂。
  - 冥書擇主，芥子台上，君首、藥叉誰計高一著？
  - 花月之爭，有何陰謀？血祭是非走不可的結局嗎？

### 風火開道應血災

  - [風火開道](../Page/風火道.md "wikilink")，應證血劫天數。
  - 酆都惡人侵入天宇，風渡塵能阻黑流犯清流嗎？
  - 慕紫侯霧鎖靈山，道皇黑流來犯，紀神人應劫。

### 花月血競須彌宴

  - 花月府血競，花靜夜與花月曉仍無法擺脫宿命之爭，花月曉亡。
  - 須彌山芥子台冥書出世，飛凡塵眾人相爭。

`*上一部：`[`天宇獸圖之風火城`](../Page/天宇獸圖之風火城.md "wikilink")`：2006年發行，共10集`
`*下一部：`[`天宇嘯龍紀`](../Page/天宇嘯龍紀.md "wikilink")`：2007年發行，共20集`

## 主要人物

### 飛凡塵

  - [藥叉共王](../Page/藥叉共王.md "wikilink")（煞星）
  - [菊殘](../Page/菊殘.md "wikilink")
  - [閑仇無痕](../Page/閑仇無痕.md "wikilink")
  - [花靜夜](../Page/花靜夜.md "wikilink")
  - 七夕劍仕[花月曉](../Page/花月曉.md "wikilink")
  - [帝釋君首鳧徯天](../Page/帝釋君首鳧徯天.md "wikilink")
  - [鳳擎天](../Page/鳳擎天.md "wikilink")
  - [朴天翁](../Page/朴天翁.md "wikilink")
  - [太劍夫](../Page/太劍夫.md "wikilink")
  - 荒山羽客[燕孤城](../Page/燕孤城.md "wikilink")
  - [赫聯新月](../Page/赫聯新月.md "wikilink")（飛天）
  - 劍師[端木紘](../Page/端木紘.md "wikilink")

### 天宇

  - [紅雲驕子兩卷書](../Page/紅雲驕子兩卷書.md "wikilink")
  - [碧海春霖杜鳳兒](../Page/碧海春霖杜鳳兒.md "wikilink")
  - [冉七](../Page/冉七.md "wikilink")
  - [管九](../Page/管九.md "wikilink")
  - [玉宇飛絮紀子焉](../Page/玉宇飛絮紀子焉.md "wikilink")
  - 蛾門 / [蛾帥](../Page/蛾帥.md "wikilink")
  - 蛾門 / 周遊山巔修真道‧[述千里](../Page/述千里.md "wikilink")
  - [耶修渡](../Page/耶修渡.md "wikilink")：黑流無天道之主
  - [清香花流風渡塵](../Page/清香花流風渡塵.md "wikilink")
  - [釋靈真大掌院](../Page/釋靈真.md "wikilink")[寵愛喀爾](../Page/寵愛喀爾.md "wikilink")
  - 一江字
  - 奇謀鬼隱

## 各集名稱

1.  魔劫
      - 初登場：執兇刀、赤旋鋒、巨弦石指夫、帝釋君首
      - 死亡：廉命刀無價(中毒亡於假扮優質人龍的雲宇蒼龍)
2.  新權勢
      - 初登場：花香清流風渡塵、奇謀鬼隱
      - 死亡：
3.  登基第一戰
      - 初登場：半月掩
      - 死亡：
4.  無法無天
      - 初登場：
      - 死亡
5.  撼雲鼓
      - 初登場：
      - 死亡：苦嘆古今破傘子(為了告知所見所聞於耶修渡，亡於風渡塵之清流掌)
6.  以毒攻毒
      - 初登場：
      - 死亡：
7.  鋒掃清流
      - 初登場：
      - 死亡：
8.  哀酒三巡
      - 初登場：盤江尊者、妙佐琴
      - 死亡：笑月飲(與執兇刀與赤旋鋒對戰，傷重且無接受治療，與[兩卷書飲酒後而亡](../Page/兩卷書.md "wikilink"))、四通法戒(為偷襲紅雲，亡於紅雲寄體之招)
9.  狂速一瞬刀
      - 初登場：風雅詩、火流星
      - 死亡：
10. 毒蟾涎
      - 初登場：劍師端木紘
      - 死亡：侍郎-抿善(因為渡慈將擊殺論形相知，而被一江字作為人質，擊殺之)
11. 教授犯劫
      - 初登場：
      - 死亡：論形相如、盤江尊者(不敵黑流入侵亡於耶修渡)
12. 刀下真面目
      - 初登場：太陽副書記金野羽(轎行現身)
      - 死亡：花太郎(亡於無影人的千失刀之招，)、論形相知(亡於笠原風犬凌辱下)、煉魔師(偷襲笠原瘋犬不成被殺)
13. 黑白對流
      - 初登場：
      - 死亡：笠原風犬(因體力不支被赤旋鋒與執兇刀互攻，導致失血過多而亡)
14. 簫聲蟾鳴
      - 初登場：神日幻龍齋、夜神忍、山力士
      - 死亡：塵涯無雙太劍夫(誤觸三星劍會的陷阱而亡)
15. 染血面具
      - [藥叉共王重生](../Page/藥叉共王.md "wikilink")
      - 死亡：菊殘、閑仇無痕(兄弟兩被魔子吸食功力而亡)、山力士(不敵刀隼變化的刀法)
16. 落雲隼
      - 初登場：鼓王雷麒
      - 死亡：刀隼\[詐死\]、火流星(被蟾蜍毒液傷眼而被其當食物)
17. 無影無形
      - 初登場：飛天
      - 死亡：寵愛喀爾(與耶修渡一招對決，被餘波震碎全身而亡)
18. 六禪祥雲
      - 初登場：歸雲僧隱六禪靜、無曄子、紫府鈞座
      - 死亡：
19. 血戰
      - 初登場：
      - 死亡：蛾帥(不願投降無天道，被述千里背後偷襲，被執兇刀所斬首)、夜神忍(與無影人對決，被神日幻龍齋所殺)
20. 紅色閃電
      - 初登場：
      - 死亡：
21. 百里風雲
      - 初登場：丹波、蕭莫名
      - 死亡：
22. 冷夜太陽
      - 初登場：童鬼天無邪、遊夜鬼凶、嗜血蝠鬼、無言
      - 死亡：
23. 聲色道中
      - 初登場：
      - 死亡：嗜血蝠鬼(被解救翎的無言以飛劍所殺)、神日幻龍齋(與丹波決鬥任務失敗，被童鬼天無邪所殺)、金野羽(因為在部屬與丹波對決時，不顧部屬生命被丹波所斬首)
24. 唱曲
      - 初登場：百里衍秀、衡蒼靖宇慕紫侯
      - 死亡：
25. 變色皇(楓)途
      - 初登場：厭修羅、毒醫恨殘年(身影未現形)
      - 死亡：執兇刀(無天道入侵靈山，身中紀子焉樂聲而亡)、奇謀鬼隱(奉耶修渡命令，在接近紀子焉時，被無言劍氣所殺)、童鬼天無邪(十速變速度不及無影人而亡)、雲宇蒼龍(被厭修羅所傷，身體未亡，但意識已死，神仙難救)
      - 靈山神人[紀子焉詐死](../Page/紀子焉.md "wikilink")
26. 冰映無言
      - 初登場：花月鵬
      - 死亡：赤旋鋒(在靈山，因為無言的出現，對其不要動的語言無視，被其殺死)
27. 月下血競
      - 初登場：毒醫恨殘年(身體現形)、闇行魔使
      - 死亡：花月曉(與花靜夜月下血競，雖其與花靜夜有事先說明平手之結果，但是其並無答應，般若劫脫手，亡於花靜夜的鏡花水月之下)、修真道‧述千里(亡於蛾主蝕魂術，慕紫侯解救無效)
28. 夜香轎
      - 初登場：炫甲(預告時出現)、太陽書記、斷非命、靈海魔子
      - 死亡：
29. 凶厄
      - 初登場：蛾主
      - 死亡：正聯會長(身中遊夜鬼凶的毒粉傷眼，欲逃脫，被鐵鍊掐脖窒息而亡)、斷非命(被風渡塵以異招留命，回無天道覆命而亡)、蛾主(不敵慕紫侯的殘風破骨雷霆印)
30. 炫甲
      - 初登場：
      - 死亡：白容(闇行獄使所殺)、尊道(靈海魔子所殺)、燕孤城(幫蕭瑟飛飛擋下魔子攻擊，與魔子對決而亡)

[category:布袋戲](../Page/category:布袋戲.md "wikilink")
[category:天宇布袋戲](../Page/category:天宇布袋戲.md "wikilink")
[category:天宇劇集](../Page/category:天宇劇集.md "wikilink")