**天宝**（908年-912年）是[吴越太祖](../Page/吴越.md "wikilink")[錢鏐的](../Page/錢鏐.md "wikilink")[年號](../Page/年號.md "wikilink")，共计5年。是吴越政权第一个真正改元的年号\[1\]。

## 紀年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|天宝||元年||二年||三年||四年||五年 |-
style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||908年||909年||910年||911年||912年
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[戊辰](../Page/戊辰.md "wikilink")||
[己巳](../Page/己巳.md "wikilink")|| [庚午](../Page/庚午.md "wikilink")||
[辛未](../Page/辛未.md "wikilink")|| [壬申](../Page/壬申.md "wikilink") |}

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他时期使用的[天宝年号](../Page/天宝.md "wikilink")
  - 同期存在的其他政权年号
      - [開平](../Page/開平_\(朱全忠\).md "wikilink")（907年四月-911年四月）：[后梁太祖](../Page/后梁.md "wikilink")[朱全忠的年号](../Page/朱全忠.md "wikilink")
      - [乾化](../Page/乾化.md "wikilink")（911年五月至913年正月、913二月至915年十月）：後梁—朱全忠、[朱友貞之年號](../Page/朱友貞.md "wikilink")
      - [應天](../Page/應天_\(劉守光\).md "wikilink")（911年八月至913年十一月）：[燕](../Page/燕_\(五代\).md "wikilink")—[劉守光之年號](../Page/劉守光.md "wikilink")
      - [武成](../Page/武成_\(王建\).md "wikilink")（908年正月至910年十二月）：前蜀—王建之年號
      - [永平](../Page/永平_\(王建\).md "wikilink")（911年正月至915年十二月）：前蜀—王建之年號
      - [安國](../Page/安國_\(鄭買嗣\).md "wikilink")（903年至909年）：[大長和國](../Page/大長和國.md "wikilink")—[鄭買嗣之年號](../Page/鄭買嗣.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [聖冊](../Page/聖冊.md "wikilink")（905年—911年）：後高句麗—金弓裔之年號
      - [永德萬歲](../Page/永德萬歲.md "wikilink")（911年—914年）：後高句麗—金弓裔之年號
      - [延喜](../Page/延喜.md "wikilink")（901年-922年）：[平安時代](../Page/平安時代.md "wikilink")[醍醐天皇之年號](../Page/醍醐天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:吴越年号](../Category/吴越年号.md "wikilink")
[Category:900年代中国政治](../Category/900年代中国政治.md "wikilink")
[Category:910年代中国政治](../Category/910年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，146-148 ISBN 7101025129