**天宮小百合**是[日本遊戲廠商](../Page/日本.md "wikilink")[KONAMI所製作的著名](../Page/KONAMI.md "wikilink")[戀愛遊戲系列](../Page/戀愛遊戲.md "wikilink")《[心跳回憶系列](../Page/心跳回憶系列.md "wikilink")》中，2006年作品《[心跳回憶Online](../Page/心跳回憶Online.md "wikilink")》中的角色。該作後來被製作成[電視動畫](../Page/電視動畫.md "wikilink")《[心跳回憶
Only Love](../Page/心跳回憶_Only_Love.md "wikilink")》。

## 提要

天宮小百合在故事中作為主要場地的私立中學裡就讀高中二年級，與另兩位主角[犬飼洸也和](../Page/犬飼洸也.md "wikilink")[青葉陸同班](../Page/青葉陸.md "wikilink")。

小百合是一位近乎完美的女孩，既擁有端莊的美貌，而且學業成績、運動能力等各方面都有很高的水平。她以最溫柔的態度對待每一個人，也很懂得照顧班上的同學。她的完美，令她擁有作為「女主角」的天然氣質。

在校內，天宮小百合非常受歡迎，甚至有很多男同學為她組織了具有擁護性質的「親衛隊」。校內的人為了強調小百合所擁有的人氣，便在周圍宣揚名為「天宮小百合傳說」的小故事，但那些故事有很大程度都是虛構的。另外，小百合家裡飼養了一隻寵物狗「」。

## 資料來源

  - [科樂美數位娛樂《心跳回憶 Only
    Love》角色資料](http://www.konami.jp/visual/tokimemo-anime/character/index.html#chara_01)
  - [東京電視台《心跳回憶 Only
    Love》角色資料](http://www.tv-tokyo.co.jp/contents/tokimemo/chara/index.html)

[Category:心跳回憶系列角色](../Category/心跳回憶系列角色.md "wikilink")
[Category:虛構女性電子遊戲角色](../Category/虛構女性電子遊戲角色.md "wikilink")
[Category:虛構女性日本人](../Category/虛構女性日本人.md "wikilink")