**天應**（927年）是[大长和](../Page/大长和.md "wikilink")[鄭隆亶的](../Page/鄭隆亶.md "wikilink")[年号](../Page/年号.md "wikilink")，共计1年。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|天應||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||927年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[丁亥](../Page/丁亥.md "wikilink")
|}

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[天應年号](../Page/天應.md "wikilink")
  - 同期存在的其他政权年号
      - [天成](../Page/天成_\(李嗣源\).md "wikilink")（926年四月至930年二月）：後唐—[李嗣源之年號](../Page/李嗣源.md "wikilink")
      - [順義](../Page/順義_\(楊溥\).md "wikilink")（921年二月至927年十月）：吳—[楊溥之年號](../Page/楊溥_\(吳\).md "wikilink")
      - [乾貞](../Page/乾貞.md "wikilink")（927年十一月至929年十月）：吳—楊溥之年號
      - [宝正](../Page/宝正.md "wikilink")（926年至931年）：吳越—錢鏐之年號
      - [白龍](../Page/白龍.md "wikilink")（925年十二月至928年二月）：南漢—劉龑之年號
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗太祖](../Page/高麗.md "wikilink")[王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：平安時代之[醍醐天皇年号](../Page/醍醐天皇.md "wikilink")

[Category:南诏年号](../Category/南诏年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")
[Category:927年](../Category/927年.md "wikilink")