[Tin_Yuet_Stop.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yuet_Stop.jpg "fig:Tin_Yuet_Stop.jpg")前的天悅站\]\]
**天悅站**（英文：**Tin Yuet
Stop**）是[香港](../Page/香港.md "wikilink")[輕鐵車站](../Page/香港輕鐵.md "wikilink")，車站編號是510，屬於單程車票[第5A收費區](../Page/輕鐵第5A收費區.md "wikilink")。此站位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗](../Page/元朗.md "wikilink")[天水圍的天華路近](../Page/天水圍.md "wikilink")[天城路](../Page/天城路.md "wikilink")[天悅邨對出](../Page/天悅邨.md "wikilink")，為天悅及週邊地區居民提供服務。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/天悅邨.md" title="wikilink">天悅邨</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 天水圍循環線（往<a href="../Page/天榮站.md" title="wikilink">天榮方向</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/天晴邨.md" title="wikilink">天晴邨</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 車站周邊

  - [天悅邨](../Page/天悅邨.md "wikilink")
  - [天晴邨](../Page/天晴邨.md "wikilink")
  - 天悅服務設施綜合大樓
  - 耀道小學

## 鄰接車站

## 使用情況

由於此站毗連天悅邨及天晴邨,因此在平日早上繁忙時開人流極高。尤其2號月台，只要706綫等待時間長便人山人海。

## 相關新聞

  - 2011年5月8日，一列[706綫的](../Page/香港輕鐵705、706綫.md "wikilink")[輕鐵列車在天悅站附近遭一輛貨櫃車攔腰相撞](../Page/香港輕鐵.md "wikilink")，導致列車第一節出軌。意外造成23人受傷，以及鐵路的供電系統及路軌損毀。事後，[天恆站至天悅站四個輕鐵站服務受阻](../Page/天恆站.md "wikilink")，[輕鐵705及706號線需作臨時改道](../Page/香港輕鐵705、706綫.md "wikilink")\[1\]。

## 參考資料

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://www.mtr.com.hk/archive/ch/services/maps/14.gif)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵轉乘優惠](http://www.mtr.com.hk/chi/lr_bus/promotions_index.html)
  - [港鐵公司 - 輕鐵及巴士服務 -
    所有輕鐵街道圖（14張）](http://www.mtr.com.hk/chi/lr_bus/stmap_index.html)

[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:2003年启用的铁路车站](../Category/2003年启用的铁路车站.md "wikilink")
[Category:元朗區鐵路車站](../Category/元朗區鐵路車站.md "wikilink")

1.  [輕鐵通宵搶修
    服務復常](http://hk.news.yahoo.com/article/110509/3/o84r.html)《星島日報》2011-05-09