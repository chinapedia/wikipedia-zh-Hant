**天才兒童MAX音樂單元一覽**（Music TV
Kun），是介紹[天才兒童](../Page/天才兒童.md "wikilink")、[天才兒童WIDE](../Page/天才兒童WIDE.md "wikilink")、[天才兒童MAX內播出過的音樂單元之一覽表](../Page/天才兒童MAX.md "wikilink")。

**音樂TV君**（**ミュージックてれびくん**）是[日本](../Page/日本.md "wikilink")[NHK教育台針對兒童與青少年所播放的](../Page/NHK教育台.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")(天才てれびくんMAX)』中、由1998年開始一直播出到現在的受歡迎單元。簡稱為MTK。

2005年度開始改成以片尾曲的方式播出、畫面下方加入了告知事項與製作人員一覽表。在本放送的6週間與長期重播總集編的期間裡交互播出。「音樂TV君」裡有一些是翻唱曲、但是在轉為「片尾曲MTK(エンディングMTK)」之後就變成全部都是原創的曲子。各個年度都分別在固定的播出時間播出。另外在CD唱片裡會收錄完整版的曲子。

## 歷史

### 2005年度

2005年開始作為節目最後面的單元「片尾曲MTK」來播送。

  - 本放送的第6週星期四的現場直播時,會以現場演奏的方式播出完整版。
  - 歌唱團體的名稱是公開向觀眾徵求命名而產生。
  - 放送歌曲的時間統一為2分鐘。
  - 每5週1次的總集編週與一部分的長期總集編期間不播出片尾曲MTK,改播出「未来はジョウキゲン」的完整版。
  - 3月遇到星期四時,會把片尾曲MTK現場演奏的影片作為片尾曲來重播。因為是在片尾,所以也加上了製作人員一覽表。在播放製作人員一覽表時,畫面會調整成和通常的片尾曲MTK相同的尺寸。
  - NHK數位教育台播出的時候,畫面左右兩側黑邊的左上方會加上MTK的彩色標記。原本是在右上方。另外,從11月7日的節目起「未来はジョウキゲン」也加上了MTK的彩色標記,但是從1月5日的節目起又回復到節目原本的標記。

### 2006年度

  - 2006年度這個單元也繼續播出、但是因為播出的是主題曲「ダンゼン\!未来」的關係,類似「今年的MTK是不是被廢止了？」的詢問紛紛湧向NHK。官方網頁上則說明會繼續播出、一直以來只有在NHK數位教育台播出時才會加上的MTK彩色標記,從4月17日的節目開始在用類比方式播出的NHK教育台上也看的到,放在MTK畫面的左上方。
  - 放送歌曲的時間統一為1分鐘30秒。但是重播總集編的時候因為單元時間的關係,有時會改為播出縮短的版本。
  - 有時播出[天てれ部活動時](../Page/天才兒童MAX的單元#天てれ部活動.md "wikilink"),會把這個單元的影片與MTK一起播出。（其他像是紙飛機達陣賽第1階段最後一場比賽的重播日,與播出夏季公演幕後時也有過這樣的例子。）遇到這種情況,左上方就不會顯示MTK彩色標記。
  - 10月4日與12月5日的節目（總集編）因為片尾是「天てれ冒険部」的關係而暫停播出MTK。
  - 2006年度的第1學期時,本放送6週時會播出新曲、每5週1次的總集編重播週則固定播出「ダンゼン\!未来」。
  - 另外、從第2學期開始,經過4週本放送+總集編週後,播出接下來的新曲。
  - 本年度在播出第2首曲子「ユウキスイッチ」之前,就在節目的主網頁上先公佈了曲名與歌唱的[TV戰士](../Page/TV戰士.md "wikilink")。
  - 第3曲的「誕生日の歌2006」是2004年度公開的「誕生日の歌」的翻唱曲。
  - 第4曲的「Friends\&Dreams」由[天てれ女子一輪車部演唱](../Page/天てれ女子一輪車部.md "wikilink")、[Yum\!Yum\!ORANGE提供樂曲](../Page/Yum!Yum!ORANGE.md "wikilink")。

## 歴代主題曲

### 片頭曲

  - タイムマシーンでいこう／[すかんち](../Page/すかんち.md "wikilink")(1993～1995)
  - 星を見上げて／[SWITCH](../Page/SWITCH.md "wikilink")(1996)
  - パリは恋の街／[TT CHARLIE](../Page/TT_CHARLIE.md "wikilink")(1997)
  - テレビ万歳／[POA](../Page/POA.md "wikilink")(1999)
  - スーパースピードスター／[carnies](../Page/carnies.md "wikilink")(2000)
  - 夢をつかんで／[渡辺ヒロコ](../Page/渡辺ヒロコ.md "wikilink")(2001)
  - 青い星／[LOVE JETS](../Page/LOVE_JETS.md "wikilink")(2002)
  - 2003年度片頭主題曲(2003)
  - プラズマ回遊（縮短版）／[TV戰士](../Page/TV戰士.md "wikilink")'04(2004)
  - 未来はジョウキゲン（縮短版）／[TV戰士](../Page/TV戰士.md "wikilink")'05(2005)
  - ダンゼン\!未来（縮短版）／[TV戰士](../Page/TV戰士.md "wikilink")'06(2006)

#### 片尾曲

  - YOU YOU YOU／[すかんち](../Page/すかんち.md "wikilink")(1993～1994前半)
  - がんばってダーリン\!／[\#クリマカーユ](../Page/#クリマカーユ.md "wikilink")(1994後半～1995前半)
  - ガンバレ\!アインシュタイン／[ダチョウ倶楽部](../Page/ダチョウ倶楽部.md "wikilink") with
    [TV戰士](../Page/TV戰士.md "wikilink")1995(1995後半)
  - キミはすてきさ\!ベイビーメイビー／[TV戰士](../Page/TV戰士.md "wikilink")1996(1996前半)
  - SUPER KIDS ARE
    GO\!\!／[\#ストロベリーパフェ](../Page/#ストロベリーパフェ.md "wikilink")(1996後半)
  - BE ALL RIGHT\!\!／[河相我聞](../Page/河相我聞.md "wikilink")(1997前半)
  - BANG BANG BANG／[キャイ～ン](../Page/キャイ～ン.md "wikilink") with
    [TV戰士](../Page/TV戰士.md "wikilink")1997(1997後半)
  - 君にクラクラ／[TV戰士](../Page/TV戰士.md "wikilink")1998 with
    [山崎邦正](../Page/山崎邦正.md "wikilink")&[リサ・ステッグマイヤー](../Page/リサ・ステッグマイヤー.md "wikilink")(1998)
  - 恋の天才～ジョンとミケの場合／[TV戰士](../Page/TV戰士.md "wikilink")1999(1999)
  - [ドキドキのち晴れ](../Page/ドキドキのち晴れ.md "wikilink")／[TV戰士](../Page/TV戰士.md "wikilink")2000(2000)
  - きらいじゃ★ブギ／[TV戰士](../Page/TV戰士.md "wikilink")2001(2001)
  - [LOVE IS
    POP](../Page/LOVE_IS_POP.md "wikilink")／[TV戰士](../Page/TV戰士.md "wikilink")2002(2002)
  - good day／[TV戰士](../Page/TV戰士.md "wikilink")2003(2003)
  - [プラズマ回遊](../Page/プラズマ回遊.md "wikilink")／[TV戰士](../Page/TV戰士.md "wikilink")2004(2004)
  - 未来はジョウキゲン／[TV戰士](../Page/TV戰士.md "wikilink")2005(2005)
  - ダンゼン\!未来／[TV戰士](../Page/TV戰士.md "wikilink")2006※(2006)

此外,由於2005年度開始播出「片尾曲MTK」的關係、播出的次數減少。

### 音樂TV君組成的團體

依組成的時間排序。包括了「音樂TV君」之前的團體。

#### クリマカーユ

**クリマカーユ**是[天才兒童MAX的單元](../Page/天才兒童MAX.md "wikilink")「ポコ・ア・ポコ」（93-95年度）開始誕生的[樂團](../Page/樂團.md "wikilink")。活動期間是1993年起的2年間。團體名稱是由各成員名字的頭一個文字所組成。

##### 成員

  - [クリスティー](../Page/クリスティー_\(タレント\).md "wikilink")
  - [石川磨依](../Page/石川磨依.md "wikilink")
  - [鈴木加奈枝](../Page/鈴木加奈枝.md "wikilink")
  - [中村友里子](../Page/中村友里子.md "wikilink")

#### 曲目

  - がんばってダーリン\!（1994後半-1995前半片尾曲）
      - 1994年9月10日發行了CD單曲。

### ドレミファキッズ

1993年度。舞蹈團體。

#### 成員

  - [中村友里子](../Page/中村友里子.md "wikilink")
  - [石川磨依](../Page/石川磨依.md "wikilink")
  - [鈴木加奈枝](../Page/鈴木加奈枝.md "wikilink")
  - [細谷麻衣](../Page/細谷麻衣.md "wikilink")
  - [金子麻里](../Page/金子麻里.md "wikilink")
  - [草村麻利子](../Page/草村麻利子.md "wikilink")
  - [倉沢桃子](../Page/倉沢桃子.md "wikilink")
  - [石井明日香](../Page/石井明日香.md "wikilink")
  - [井上愛香](../Page/井上愛香.md "wikilink")
  - [岡沢千穗梨](../Page/岡沢千穗梨.md "wikilink")
  - [小泉真由美](../Page/小泉真由美.md "wikilink")
  - [糀本季央](../Page/糀本季央.md "wikilink")
  - [斎藤恵里奈](../Page/斎藤恵里奈.md "wikilink")
  - [菅原彩](../Page/菅原彩.md "wikilink")
  - [寺嶋あゆみ](../Page/寺嶋あゆみ.md "wikilink")
  - [富樫麻里](../Page/富樫麻里.md "wikilink")
  - [西村有香](../Page/西村有香.md "wikilink")
  - [平野春花](../Page/平野春花.md "wikilink")
  - [藤田あゆ](../Page/藤田あゆ.md "wikilink")
  - [藤村陽子](../Page/藤村陽子.md "wikilink")
  - [本多沙由里](../Page/本多沙由里.md "wikilink")
  - [三浦遊](../Page/三浦遊.md "wikilink")

### クリス with CHU-CHU-くらぶ

**クリス with
CHU-CHU-くらぶ**（クリスウィズチューチューくらぶ）是1995年度「[ポコ・ア・ポコ](../Page/ポコ・ア・ポコ.md "wikilink")」所組成的樂團。

#### 成員

  - [クリスティー](../Page/クリスティー_\(タレント\).md "wikilink")
  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink")
  - [篠原麻里](../Page/篠原麻里.md "wikilink")
  - [星野涼子](../Page/星野涼子.md "wikilink")
  - [冨貴塚桂香](../Page/冨貴塚桂香.md "wikilink")

#### 原創曲

  - 「あべこべLove Motion」
  - 「スターチャイルド・スペースランナー」
  - 「道草ブギ」
  - 「サンデーモーニングフィーバー」
  - 「メッセージ」

### ストロベリーパフェ

**ストロベリーパフェ**是[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")1996年度所組成的樂團。團體名稱是由音樂家[王様所命名](../Page/王様.md "wikilink")。

1997年度的公演中、以所持有當時的片頭曲、片尾曲等3曲來進行現場演唱會的演奏,這是他們最後的活動。

#### 成員

  - [生田斗真](../Page/生田斗真.md "wikilink")（負責吉他）
  - [伊東亮輔](../Page/伊東亮輔.md "wikilink")（負責肩背式電子琴）
  - [前田亞季](../Page/前田亞季.md "wikilink")（負責電子琴）
  - [ジェニファー・ペリマン](../Page/ジェニファー・ペリマン.md "wikilink")（同上）
  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink")（負責貝斯）
  - [篠原麻里](../Page/篠原麻里.md "wikilink")（負責鼓）

#### 作品

  - SUPER KIDS ARE GO\!\!（1996年度後半的片尾主題曲）

### ザ・フルース・ブラザース

**ザ・フルース・ブラザース**是1998年度在「音樂TV君」登場,由[海道亮平](../Page/海道亮平.md "wikilink")・[橋田紘緒所組成的二人組](../Page/橋田紘緒.md "wikilink")。前身是「はしかつブラザーズ」。2人都是關西出身。此外、00年度時,代替海道亮平的[伊藤俊輔加入](../Page/伊藤俊輔_\(タレント\).md "wikilink")、變成了「ザ・フルース・ブラザース2000」。

#### 成員

  - ザ・フルース・ブラザース
      - [海道亮平](../Page/海道亮平.md "wikilink")
      - [橋田紘緒](../Page/橋田紘緒.md "wikilink")
  - ザ・フルース・ブラザース2000
      - 橋田紘緒
      - [伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")

#### 曲目

  - 極楽はどこだ（作詞・作曲：小宮山雄飛/1998年11月16日放送/原曲：極楽はどこだ／[ホフディラン](../Page/ホフディラン.md "wikilink")）
  - I LIKE
    YOU（作詞・作曲：[忌野清志郎](../Page/忌野清志郎.md "wikilink")/2000年10月9日放送/原曲：I
    LIKE YOU／RC Succession）

### モンキークイーン

**モンキークイーン**是在「音樂TV君」登場,由女性3人所組成的團體。演唱的歌曲數是「音樂TV君」中最多的3首歌曲。是個既有歌唱力也有舞蹈技巧的團體。1999年時結成、那一年演唱了2首曲子、2001年再結成之後又演唱了1首歌曲。2001年時[モニーク・ローズ以外的](../Page/モニーク・ローズ.md "wikilink")2個人已經從『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』畢業。當時原本是要以現場直播的方式登場,因為播出日與美國的911事件重疊,相關人員因而無法演出。不過改在接著的那一週演出、那時是以現場直播方式演唱[恋のギルティー](../Page/恋のギルティー.md "wikilink")。

#### 成員

  - [ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")（じゃすみんあれん）（1）當時是國中2年級（2）當時是高中1年級
  - [佐久間信子](../Page/佐久間信子.md "wikilink")（さくまのぶこ）（1）當時是小學5年級（2）當時是國中1年級
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")（もにーくろーず）（1）當時是小學5年級（2）國中1年級

※（1）是1999年度時的年齢。（2）是2001年度時的年齢

#### 曲目

  - 恋のギルティー Love in the first degree
  - ママ・ミア
  - Everything You Do

### 悪魔なエンジェル

1999年度。

#### 成員

  - [中田あすみ](../Page/中田あすみ.md "wikilink")
  - [大沢あかね](../Page/大沢あかね.md "wikilink")
  - [安斎舞](../Page/安齊舞.md "wikilink")
  - [徐桑安](../Page/徐桑安.md "wikilink")

#### 曲目

  - 運命'99

### 飛び出せ\!ステッグマイヤーズ

1999年度。

#### 成員

  - [リサ・ステッグマイヤー](../Page/リサ・ステッグマイヤー.md "wikilink")
  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink")
  - [ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")
  - [棚橋由希](../Page/棚橋由希.md "wikilink")
  - [ジェームス・マーティン](../Page/ジミーMackey.md "wikilink")
  - [ダーブロウ有紗](../Page/有紗.md "wikilink")

#### 曲目

  - The Longest Time

### ワイルドベリーズ

2000年度。

#### 成員

  - [ダーブロウ有紗](../Page/有紗.md "wikilink")
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")

#### 曲目

  - Roam～愛があれば～

### ザ・ヤマチーズ

**ザ・ヤマチーズ**是2000年由[山元竜一](../Page/山元竜一.md "wikilink")・[村田Chihiro所組成](../Page/村田Chihiro.md "wikilink"),「音樂TV君」裡的一個團體。特徵是2個人都是6年戰士,以及具有嘶啞的嗓音。

#### 作品

  - 「Dynamite」（2000年5月8日播出）
      - 原曲：Dynamite／[ロッド・スチュワート](../Page/ロッド・スチュワート.md "wikilink")
  - 「Twilight」（2001年10月22日播出）
      - [\#アップル・シェイク的](../Page/#アップル・シェイク.md "wikilink")[ダーブロウ有紗](../Page/有紗.md "wikilink")・[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")・[岩井七世](../Page/岩井七世.md "wikilink")、原TV戰士的[伊藤俊輔有参加](../Page/伊藤俊輔_\(タレント\).md "wikilink")（The
        Yamachee's feat. 以[\#Apple
        Shake](../Page/#アップル・シェイク.md "wikilink") with
        SHUN的名義）。
      - 原曲：Twilight／[エレクトリック・ライト・オーケストラ](../Page/エレクトリック・ライト・オーケストラ.md "wikilink")
  - 「ぼくらのロックシティ 」（2003年4月7日播出）
      - [岩井七世](../Page/岩井七世.md "wikilink")・[俵小百合有参加](../Page/俵小百合.md "wikilink")（以[\#ふたりいる。的搭檔身分](../Page/#ふたりいる。.md "wikilink")）。
      - 原曲：We Built This City／[スターシップ](../Page/スターシップ.md "wikilink")

### ランランズ

2000年度。

#### 成員

  - [俵有希子](../Page/俵有希子.md "wikilink")
  - [須田泰大](../Page/須田泰大.md "wikilink")
  - [徐桑安](../Page/徐桑安.md "wikilink")
  - [ローブリィ翔](../Page/ローブリィ翔.md "wikilink")

### スウィート・ピクルス

2000年度。

#### 成員

  - [佐久間信子](../Page/佐久間信子.md "wikilink")
  - [ダーブロウ有紗](../Page/有紗.md "wikilink")
  - [俵有希子](../Page/俵有希子.md "wikilink")

### 水ふうせん

2000年度。

#### 成員

  - [福田亮太](../Page/福田亮太.md "wikilink")
  - [逵優希](../Page/逵優希.md "wikilink")

### MO-SHI-YA

2000年度。

#### 成員

  - [饗場詩野](../Page/饗場詩野.md "wikilink")
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")
  - [山元竜一](../Page/山元竜一.md "wikilink")

### c@mo.ne

2000年度。

#### 成員

  - [中田あすみ](../Page/中田あすみ.md "wikilink")
  - [福田亮太](../Page/福田亮太.md "wikilink")
  - [石田比奈子](../Page/石田比奈子.md "wikilink")
  - [逵優希](../Page/逵優希.md "wikilink")
  - [熊木翔](../Page/熊木翔.md "wikilink")
  - [松下博昭](../Page/松下博昭.md "wikilink")

### セブン・ハーツ

**セブン・ハーツ**是由7個人所組成的團體。當時是「音樂TV君」人數最多的團體。（在2004年度比這團體還多的8個人團體,[\#ユービック登場](../Page/#ユービック.md "wikilink")）當時只有伊藤俊輔是國中2年級、其他全部都是小學6年級。7個人全員以年紀來說都是高年級。

#### 成員

  - [伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")（いとうしゅんすけ、1986年9月24日-）當時是國中2年級
  - [佐久間信子](../Page/佐久間信子.md "wikilink")（さくまのぶこ、1988年7月16日-）當時是小學6年級
  - [ダーブロウ有紗](../Page/有紗.md "wikilink")（だーぶろうありさ、1988年4月16日-）當時是小學6年級
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")（もにーく・ろーず、1989年1月11日-）當時是小學6年級
  - [饗場詩野](../Page/饗場詩野.md "wikilink")（あいばしの、1988年5月16日-）當時是小學6年級
  - [エバンス太郎](../Page/エバンス太郎.md "wikilink")（えばんすたろう、1988年7月11日-）當時是小學6年級
  - [熊木翔](../Page/熊木翔.md "wikilink")（くまきしょう、1989年3月2日-）當時是小學6年級

#### 曲目

  - 愛はきらめきの中にHow deep is your love

### プチ・チェリーズ

2000年度。

#### 成員

  - [安齊舞](../Page/安齊舞.md "wikilink")
  - [徐桑安](../Page/徐桑安.md "wikilink")

### ハッスル3

2001年度。

#### 成員

  - [山元竜一](../Page/山元竜一.md "wikilink")
  - [熊木翔](../Page/熊木翔.md "wikilink")
  - [竪山隼太](../Page/竪山隼太.md "wikilink")

### バースデイ・ガール

2001年度。

#### 成員

  - [中田あすみ](../Page/中田あすみ.md "wikilink")
  - [村上東奈](../Page/村上東奈.md "wikilink")

### アップル・シェイク

2001年度。

#### 成員

  - [ダーブロウ有紗](../Page/有紗.md "wikilink")
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")
  - [岩井七世](../Page/岩井七世.md "wikilink")

### ホリデーズ

**ホリデーズ**是2001年度「音樂TV君」中演唱VACATION的團體。成員是ローブリィ翔、卓也、小百合、有沙4個人。當時似乎爆發了ローブリィ翔與中村有沙間的領隊爭奪戰。

#### 成員

  - [井出卓也](../Page/井出卓也.md "wikilink")（いでたくや、1991年3月12日-）當時是小學5年級
  - [ローブリィ翔](../Page/ローブリィ翔.md "wikilink")（ローブリィしょう、1991年11月27日-）當時是小學4年級
  - [俵小百合](../Page/俵小百合.md "wikilink")（たわらさゆり、1991年3月27日-）當時是小學5年級
  - [中村有沙](../Page/中村有沙.md "wikilink")（なかむらありさ、1993年1月27日-）當時是小學3年級

#### 曲目

  - VACATION

### ノーティー・ハリー

2001年度。

#### 成員

  - [エバンス太郎](../Page/エバンス太郎.md "wikilink")
  - [松井蘭丸](../Page/松井蘭丸.md "wikilink")
  - [八木俊彦](../Page/八木俊彦.md "wikilink")

### 3サイズ

2001年度。

#### 成員

  - [中田あすみ](../Page/中田あすみ.md "wikilink")
  - [安齊舞](../Page/安齊舞.md "wikilink")
  - [豕瀬志穗](../Page/豕瀬志穗.md "wikilink")

### コバンザメ

2001年度。

#### 成員

  - [橋田紘緒](../Page/橋田紘緒.md "wikilink")
  - [福田亮太](../Page/福田亮太.md "wikilink")

### TIMKY

2001年度。

#### 成員

  - [熊木翔](../Page/熊木翔.md "wikilink")
  - [山元竜一](../Page/山元竜一.md "wikilink")
  - [俵小百合](../Page/俵小百合.md "wikilink")
  - [岩井七世](../Page/岩井七世.md "wikilink")
  - [村上東奈](../Page/村上東奈.md "wikilink")

<!-- end list -->

  - [俵有希子](../Page/俵有希子.md "wikilink")
  - [種ともこ](../Page/種ともこ.md "wikilink")

### SPACE FAIRY

**SPACE FAIRY**（スペースフェアリー）是在2002年度的「音樂TV君」中登場的3人團體。

#### 成員

  - [俵有希子](../Page/俵有希子.md "wikilink")（たわら ゆきこ、1990年2月4日 -）當時是國中1年級
  - [白木杏奈](../Page/杏奈_\(タレント\).md "wikilink")（しらき あんな、1990年6月19日
    -）當時是小學6年級
  - [中村有沙](../Page/中村有沙.md "wikilink")（なかむら ありさ、1993年1月27日 -）當時是小學4年級

#### 作品

  - 1-2-3
  - 恋にメリーゴーラウンド *In for a penny,In for a pound*

### Boys Be Boys

**Boys Be Boys**（ボーイズビーボーイズ）是在2002年度的「音樂TV君」登場的團體。

#### 成員

  - [山元竜一](../Page/山元竜一.md "wikilink")（やまもと りゅういち 1989年7月22日 - ）5代目TV戰士。
  - [熊木翔](../Page/熊木翔.md "wikilink")（くまき しょう 1989年3月2日 - ）7代目TV戰士。
  - [松井蘭丸](../Page/松井蘭丸.md "wikilink")（まつい らんまる 1991年2月24日 - ）8代目TV戰士。

#### 曲目

  - 僕であるために（作詞：[浜崎貴司](../Page/浜崎貴司.md "wikilink")・作曲：[FLYING
    KIDS](../Page/FLYING_KIDS.md "wikilink")／2002年4月22日播出）
      - 原曲：僕であるために／FLYING KIDS

### T for TWO

2002年度。

#### 成員

  - [俵有希子](../Page/俵有希子.md "wikilink")
  - [俵小百合](../Page/俵小百合.md "wikilink")

#### 作品

  - 「BYE BYE DAYS」

##### コーラス

  - 「クレマチス」（[山川恵里佳ソロ](../Page/山川恵里佳.md "wikilink")、2001年度）
  - 「STEADY BOYS」（[岩井七世ソロ](../Page/岩井七世.md "wikilink")、2002年度）

### CHU-LIPS

2002年度。

#### 成員

  - [村田Chihiro](../Page/村田Chihiro.md "wikilink")
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-

### ギャラクシー

2002年度。

#### 成員

  - [安齊舞](../Page/安齊舞.md "wikilink")
  - [井出卓也](../Page/井出卓也.md "wikilink")
  - [ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")

### マイ☆コウ

2002年度。

#### 成員

  - [堀江幸生](../Page/堀江幸生.md "wikilink")
  - [マイケル・メンツァー](../Page/Mikey.md "wikilink")

### ふたりいる。

2003年度。

#### 成員

  - [俵小百合](../Page/俵小百合.md "wikilink")
  - [岩井七世](../Page/岩井七世.md "wikilink")

### システム★エラー

2003年度。

#### 成員

  - [岩井七世](../Page/岩井七世.md "wikilink")
  - [白木杏奈](../Page/白木杏奈.md "wikilink")
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")
  - [堀口美咲](../Page/堀口美咲.md "wikilink")

### TDD

2003年度。

#### 成員

  - [井出卓也](../Page/井出卓也.md "wikilink")
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
  - [前田公輝](../Page/前田公輝.md "wikilink")
  - [山川恵里佳](../Page/山川恵里佳.md "wikilink")
  - [間寛平](../Page/間寛平.md "wikilink")

### ノイジー・モンキーズ

2003年度。

#### 成員

  - [ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")
  - [近藤Emma](../Page/近藤Emma.md "wikilink")
  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")

### MEMORIES

2003年度。

#### 成員

  - [山元竜一](../Page/山元竜一.md "wikilink")
  - [俵小百合](../Page/俵小百合.md "wikilink")
  - [岩井七世](../Page/岩井七世.md "wikilink")
  - [白木杏奈](../Page/白木杏奈.md "wikilink")
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
  - [マイケル・メンツァー](../Page/Mikey.md "wikilink")

### SYANIS

2003年度。

#### 成員

  - [中村有沙](../Page/中村有沙.md "wikilink")
  - [豕瀬志穗](../Page/豕瀬志穗.md "wikilink")
  - [桜井結花](../Page/桜井結花.md "wikilink")

### A.T.7

2003年度。

#### 成員

  - [岩井七世](../Page/岩井七世.md "wikilink")
  - [俵小百合](../Page/俵小百合.md "wikilink")
  - [八木俊彦](../Page/八木俊彦.md "wikilink")
  - [ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")
  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")
  - [張沢紫星](../Page/張沢紫星.md "wikilink")
  - [川﨑樹音](../Page/樹音.md "wikilink")

### ホワイト・クローバー

2003年度。

#### 成員

  - [村田Chihiro](../Page/村田Chihiro.md "wikilink")
  - [岩井七世](../Page/岩井七世.md "wikilink")

### SMAX

**SMAX**（スマックス）是2003年度[天才兒童MAX的單元](../Page/天才兒童MAX.md "wikilink")「なりきり\!シンガーズ」的暑假特別編中、由5位TV戰士演唱[SMAP大受歡迎的作品](../Page/SMAP.md "wikilink")「[世界上唯一的花](../Page/世界上唯一的花.md "wikilink")」時所組成的團體。名稱是結合SMAP與天才兒童MAX所產生的。

#### 成員（換裝變身的SMAP成員）

  - [山元竜一](../Page/山元竜一.md "wikilink")（**[木村拓哉](../Page/木村拓哉.md "wikilink")**）
  - [ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")（**[稲垣吾郎](../Page/稲垣吾郎.md "wikilink")**）
  - [マイケル・メンツァー](../Page/マイケル・メンツァー.md "wikilink")（**[草彅剛](../Page/草彅剛.md "wikilink")**）
  - [前田公輝](../Page/前田公輝.md "wikilink")（**[中居正廣](../Page/中居正廣.md "wikilink")**）
  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")（**[香取慎吾](../Page/香取慎吾.md "wikilink")**）

### まえだたくや

2003年度。笑藝部內所組成的搞笑搭檔。

#### 成員

  - [井出卓也](../Page/井出卓也.md "wikilink") **負責吐嘈**
  - [前田公輝](../Page/前田公輝.md "wikilink") **負責裝笨**

### ランダム\!\!スロー

**ランダム\!\!スロー**是在2004年度「音樂TV君」登場,由4個人所組成的團體。集合了很有歌唱能力的成員。演唱『ラジオスターの悲劇
Video killed the radio star』的翻唱曲。

#### 成員

  - [白木杏奈](../Page/白木杏奈.md "wikilink")（しらきあんな）當時是國中2年級
  - [前田公輝](../Page/前田公輝.md "wikilink")（まえだごうき）當時是國中1年級
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")（じょあんやまざき）當時是小學6年級
  - [Barnes勇氣](../Page/Barnes勇氣.md "wikilink")（ばーんずゆうき）當時是小學6年級

#### 曲目

  - ラジオスターの悲劇 Video killed the radio star

### KIZZ

**KIZZ**是在2004年度「音樂TV君」中登場,由4個人所組成的團體。讓人印象深刻的是他們臉上的化妝。由於演唱原曲的[KISS樂團的臉上有誇張的化妝](../Page/KISS.md "wikilink")、因此他們也加以模仿。另外在2004年度的夏季公演中,KIZZ也有出場。

#### 成員

  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")
  - [堀江幸生](../Page/堀江幸生.md "wikilink")
  - [篠原愛實](../Page/篠原愛實.md "wikilink")

#### 曲目

  - 明日への叫び SHOUT IT OUT LOUD

### ヴィーナス&マーズ

**ヴィーナス&マーズ**是在2004年度的「音樂TV君」登場,由6個人所組成的團體。由Chihiro的主旋律與卓也的饒舌歌,搭配杏奈、甜歌、ジョアン、樹音的合唱所組成。

#### 成員

  - [村田Chihiro](../Page/村田Chihiro.md "wikilink")（むらたちひろ
    1991年12月9日-）當時是國中1年級
  - [井出卓也](../Page/井出卓也.md "wikilink")（いでたくや 1991年3月12日-）當時是國中2年級
  - [白木杏奈](../Page/白木杏奈.md "wikilink")（しらきあんな 1990年6月19日-）當時是國中2年級
  - [橋本甜歌](../Page/橋本甜歌.md "wikilink")（はしもとてんか 1993年11月19日-）當時是小學5年級
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")（じょあんやまざき
    1992年7月16日-）當時是小學6年級
  - [川崎樹音](../Page/川崎樹音.md "wikilink")（かわさきじゅね 1995年4月28日-）當時是小學3年級

#### 曲目

  - [\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")
      - 在2006年舉行的夏季公演中,除了唯一的成員[川崎樹音之外](../Page/川崎樹音.md "wikilink")、還加入了[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")・[篠原愛實](../Page/篠原愛實.md "wikilink")・[木內梨生奈](../Page/木內梨生奈.md "wikilink")・[細田羅夢一起演唱](../Page/細田羅夢.md "wikilink")。

### じゅげむさん

2004年度。

#### 成員

  - [堀江幸生](../Page/堀江幸生.md "wikilink")
  - [近藤Emma](../Page/近藤Emma.md "wikilink")
  - [伊倉愛美](../Page/伊倉愛美.md "wikilink")
  - [高橋郁哉](../Page/高橋郁哉.md "wikilink")

### グラスオニオン

**グラスオニオン**是在2004年度的「音樂TV君」中登場的3人組團體。

#### 成員

  - [井出卓也](../Page/井出卓也.md "wikilink")（いで たくや、1991年3月12日生、當時是國中2年級）
  - [前田公輝](../Page/前田公輝.md "wikilink")（まえだ ごうき、1991年4月3日生、當時是國中1年級）
  - [Barnes勇氣](../Page/Barnes勇氣.md "wikilink")（ばーんず
    ゆうき、1992年12月27日生、當時是小學6年級）

#### 曲目

  - MARCH（2004年9月20日）
      - 作詞・作曲：[原田真二](../Page/原田真二.md "wikilink")
      - 原曲：MARCH／原田真二

### トゥインクル

2004年度。

#### 成員

  - [中村有沙](../Page/中村有沙.md "wikilink")
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-

### Time And A Word

2004年度。

#### 成員

  - [白木杏奈](../Page/白木杏奈.md "wikilink")
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")

### ゴーライブ

2004年度。

#### 成員

  - [張沢紫星](../Page/張沢紫星.md "wikilink")
  - [篠原愛實](../Page/篠原愛實.md "wikilink")
  - [高橋郁哉](../Page/高橋郁哉.md "wikilink")

### フルーツ♥パーティー

2004年度。

#### 成員

  - [村田Chihiro](../Page/村田Chihiro.md "wikilink")
  - [川﨑樹音](../Page/樹音.md "wikilink")
  - [橋本甜歌](../Page/橋本甜歌.md "wikilink")

### ユービック

**ユービック**是在2004年度的「音樂TV君」中登場的團體。由2004年度的分裂隊伍・R.G.（[レインボー・ガーディアンズ](../Page/レインボー・ガーディアンズ.md "wikilink")）、U.W.F.（[アンダーワールドファミリー](../Page/アンダーワールドファミリー.md "wikilink")）中各自選出4個人所組成,是過去登場過的MTK團體中,人數最多的8人團體。

#### 成員（※依照年齢順序）

  - レインボー・ガーディアンズ
      - [井出卓也](../Page/井出卓也.md "wikilink")
      - [千秋レイシー](../Page/千秋レイシー.md "wikilink")
      - [淺野優梨愛](../Page/淺野優梨愛.md "wikilink")
      - [川﨑樹音](../Page/樹音.md "wikilink")
  - アンダーワールドファミリー
      - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")
      - [洸太レイシー](../Page/洸太レイシー.md "wikilink")
      - [伊倉愛美](../Page/伊倉愛美.md "wikilink")
      - [近藤Emma](../Page/近藤Emma.md "wikilink")

#### 曲目

  - [\#ヴォヤージュ±](../Page/#ヴォヤージュ±.md "wikilink")（作詞：サエキけんぞう・作曲・編曲：福岡ユタカ／2005年1月10日播出）

### Our Treasures

**Our
Treasures**（**アワートレジャーズ**）是[NHK教育電視台受歡迎的節目](../Page/NHK教育電視台.md "wikilink")[天才兒童MAX中](../Page/天才兒童MAX.md "wikilink"),2005年度所組成的團體。

節目中由1998年度開始播出的「音樂TV君」單元、一直到2004年度為止都是由在節目中演出的[TV戰士組成形形色色的團體來演唱](../Page/TV戰士.md "wikilink"),但是2005年度開始變成固定的團體,其中之一是[\#Our
Treasures](../Page/#Our_Treasures.md "wikilink")。歌曲是節目片尾的「エンディングMTK(片尾曲MTK)」,原則上是在星期一～星期三播出。

團體的名稱是公開向節目的觀眾們徵求命名而產生。

「僕らのハーモニー」由[原田真二作詞](../Page/原田真二.md "wikilink")・作曲。並在節目公演『ユゲデールを救え\!てれび戦士史上最大の危機』中演唱了重新編曲,不使用電子樂器的版本
。也預定收錄在[原田真二本人的CD唱片中](../Page/原田真二.md "wikilink")。

「ムカつくけれど……スキ\!」是由[武川行秀作詞](../Page/武川行秀.md "wikilink")・作曲、在星期四現場直播的特別現場演唱會中,4個人以現場演奏的方式歌唱。（負責的樂器請參照成員部分。）

節目中的另外一個團體是[\#Tiny Circus](../Page/#Tiny_Circus.md "wikilink")。

#### 成員

  - [前田公輝](../Page/前田公輝.md "wikilink")（國中2年級、負責吉他）
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-（國中2年級、負責電子琴）
  - [Barnes勇氣](../Page/Barnes勇氣.md "wikilink")（國中1年級、負責鼓）
  - [木內江莉](../Page/木內江莉.md "wikilink")（小學6年級、負責貝斯）

※年級是2005年度當時的年級

#### 曲目

  - 僕らのハーモニー（作詞・作曲：[原田真二](../Page/原田真二.md "wikilink")／2005年4月4日～5月19日播出、1月9日～1月11日應要求加演而重播）
  - ムカつくけれど……スキ\!（作詞・作曲：[武川行秀](../Page/武川行秀.md "wikilink")／2005年9月5日～10月20日播出）

### Tiny Circus

**Tiny
Circus**（**タイニーサーカス**）是[NHK教育電視台廣受歡迎的節目](../Page/NHK教育電視台.md "wikilink")[天才兒童MAX中](../Page/天才兒童MAX.md "wikilink"),2005年度組成的團體。

節目中由1998年度開始播出的「音樂TV君」單元、一直到2004年度為止都是由在節目中演出的[TV戰士組成形形色色的團體來演唱](../Page/TV戰士.md "wikilink"),但是2005年度開始變成固定的團體,其中之一是[\#Tiny
Circus](../Page/#Tiny_Circus.md "wikilink")。歌曲是節目片尾的「エンディングMTK(片尾曲MTK)」,原則上是在星期一～星期三播出。

團體的名稱是公開向節目的觀眾們徵求命名而產生。

在節目公演『ユゲデールを救え\!てれび戦士史上最大の危機』中、因為成員的[橋本甜歌得了夏季感冒而無法持續參加練習](../Page/橋本甜歌.md "wikilink"),所以急遽由同樣是TV戰士的[伊倉愛美來代替演出](../Page/伊倉愛美.md "wikilink")。

節目中的另外一個團體是[\#Our Treasures](../Page/#Our_Treasures.md "wikilink")。

#### 成員

  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")（國中2年生）
  - [村田Chihiro](../Page/村田Chihiro.md "wikilink")（國中2年級）
  - [橋本甜歌](../Page/橋本甜歌.md "wikilink")（小學6年級）
  - [木內梨生奈](../Page/木內梨生奈.md "wikilink")（小學5年級）

代役・[伊倉愛美](../Page/伊倉愛美.md "wikilink")（小學6年級） ※年級是2005年度當時的年級

#### 曲目

  - さかさまさかさ（作詞：[カヒミ・カリィ](../Page/カヒミ・カリィ.md "wikilink")、作曲・編曲：[神田朋樹](../Page/神田朋樹.md "wikilink")／2005年5月23日～7月7日播出、2006年1月16日～1月18日應要求加演而重播）
  - 僕はドライブ（作詞：[サエキけんぞう](../Page/サエキけんぞう.md "wikilink")、作曲・編曲：polymoog（[ELEKTEL](../Page/ELEKTEL.md "wikilink")）／2005年10月24日～12月8日播出）

### 天てれJAPAN

在2006年度的現場直播所組成的隊伍名。

#### 成員

  - [高橋郁哉](../Page/高橋郁哉.md "wikilink")
  - [木村遼希](../Page/木村遼希.md "wikilink")
  - [千葉一磨](../Page/千葉一磨.md "wikilink")

### 女子一輪車部

2006年度。

#### メンバー

  - [加藤Gina](../Page/加藤Gina.md "wikilink")
  - [伊倉愛美](../Page/伊倉愛美.md "wikilink")
  - [大木梓彩](../Page/大木梓彩.md "wikilink")
  - [木內江莉](../Page/木內江莉.md "wikilink")
  - [細川藍](../Page/細川藍.md "wikilink")
  - [橋本甜歌](../Page/橋本甜歌.md "wikilink")
  - [一木有海](../Page/一木有海.md "wikilink")

## 代表曲

### 8cm

**8cm**（はっせんち）是『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』1998年開始的受歡迎單元「音樂TV君」（ミュージックてれびくん）最先播出的歌曲。

負責演唱的是[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")。首次播出是在1998年4月6日。

名稱的由來是由「心情雀躍地由地面向上跳的距離之印象」而取名。

#### 影片

影片中除了Wentz瑛士之外,[山元竜一與](../Page/山元竜一.md "wikilink")[伊藤俊輔也有出現](../Page/伊藤俊輔_\(タレント\).md "wikilink")。

其他像是歌曲的拍攝場面大部分都是Wentz瑛士的家與鄰近地區的風景,此外他所飼養的狗也有出現在影片中。

### 誕生日のうた

**誕生日のうた**（たんじょうびのうた）是2004年度「音樂TV君」中播出的歌曲。

由[\#ヴィーナス&マーズ演唱](../Page/#ヴィーナス&マーズ.md "wikilink")。首次播出是2004年5月24日。由[武川行秀所作詞](../Page/武川行秀.md "wikilink")・作曲。星期四現場直播内的[HAPPYサプライズ這單元的背景音樂播放](../Page/HAPPYサプライズ.md "wikilink")。2004年度的長期總集編期間與2005年度[\#MTKクラシック的重播與沒有現場直播的月份](../Page/#MTKクラシック.md "wikilink"),會以字幕方式播出當月生日的小朋友們的名字。2006年度把歌曲縮短成1分鐘,加入由22位TV戰士（日向滉一與一木有海缺席）拿著鞭炮等等慶祝生日的影片。）
此外,2006年度在夏季公演時就事先公開了2006年度的版本,9月4日的節目開始作為片尾曲MTK而播出。
不過,在2004年度演唱的[川崎樹音有參加演唱](../Page/川崎樹音.md "wikilink"),但是[橋本甜歌則沒有參加演唱](../Page/橋本甜歌.md "wikilink")。

### ヴォヤージュ±

**ヴォヤージュ±**（ヴォヤージュ・プラスマイナス）是2004年度「音樂TV君」中播出的歌曲。首次播出是在2005年1月10日。由サエキけんぞう作詞,福岡ユタカ作曲。是2004年度最後的歌曲。是[天才兒童MAX冬季公演](../Page/天才兒童MAX.md "wikilink")「テンタニック号危機一髪\!プラズマ大歌合戦」中唯一沒有演唱的曲子。由[RG與](../Page/RG.md "wikilink")[UWF的混合團體](../Page/UWF.md "wikilink")[\#ユービック歌唱](../Page/#ユービック.md "wikilink")。是首充滿不可思議感覺的歌曲不。

### ドキドキのち晴れ

**ドキドキのち晴れ**（ドキドキのちはれ）是2000年度[天才兒童MAX的片尾主題曲](../Page/天才兒童MAX.md "wikilink")。是首非常明朗的曲子。2003年1月28日那天也有播放。由2000年度的[TV戰士演唱](../Page/TV戰士.md "wikilink")。作詞・作曲・合唱是[種ともこ](../Page/種ともこ.md "wikilink")、演奏是[柳沢"230"二三男](../Page/柳沢"230"二三男.md "wikilink")。首次播出は2000年4月3日。

### LOVE IS POP

**LOVE IS
POP**（ラブ・イズ・ポップ）是2002年度[天才兒童MAX的片尾主題曲](../Page/天才兒童MAX.md "wikilink")。是提倡愛與和平的曲子,歌詞裡面多次出現了「LOVE」與「PEACE」。由2002年度的[TV戰士演唱](../Page/TV戰士.md "wikilink")。作詞・作曲・合唱是[森若香織](../Page/森若香織.md "wikilink")。演奏是[KANAME](../Page/KANAME.md "wikilink")。首次播出是在2002年4月8日。

### プラズマ回遊

**プラズマ回遊**（プラズマかいゆう）是2004年度[天才兒童MAX的主題曲](../Page/天才兒童MAX.md "wikilink")。由2004年度的[TV戰士演唱](../Page/TV戰士.md "wikilink")。這首歌的歌詞裡有一句話叫做「YOSORO」,它的意思是「這樣就夠了」。由這句話可以知道這像是首深奧的曲子。首次播出是在2004年4月5日。作詞是[Yen
chang+js](../Page/Yen_chang+js.md "wikilink")、作曲是[福岡ユタカ](../Page/福岡ユタカ.md "wikilink")。演奏是[Yen
chang/Mecken](../Page/Yen_chang/Mecken.md "wikilink")。合唱是[Takeru,F\&Kanade,F](../Page/Takeru,F&Kanade,F.md "wikilink")。

## 樂曲一覧

從1998年的『[\#8cm](../Page/#8cm.md "wikilink")』到現在一共有91首曲子（其中6曲是片尾曲MTK）。此外,2000年度的『HONEYBEAT』與『www.access.me』合在一起,發行了單曲CD。另外2004年度演唱『MARCH』的[\#グラスオニオン](../Page/#グラスオニオン.md "wikilink"),以同首曲子和原田真二作了合計三回（現場演唱會・原田真二本人的演唱會・冬季公演）的共同表演,2004年度的NHK教育博覽會中由[天才ビットくん的樂團](../Page/天才ビットくん.md "wikilink")[The
Bittles](../Page/The_Bittles.md "wikilink")、2005年度秋季的NHK教育博覽會中由[\#Our
Treasures進行現場演奏與歌唱](../Page/#Our_Treasures.md "wikilink")。

### 1998年度

  - [\#8cm](../Page/#8cm.md "wikilink")／[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")
      - 作詞：今泉泰幸、作曲：遠藤肇、合唱：タケカワゆい、編曲・演奏：That's on
        Noise、演出者：伊藤俊輔／山元竜一、原曲：[\#8cm](../Page/#8cm.md "wikilink")／[スキップカウズ](../Page/スキップカウズ.md "wikilink")
  - 恋はあせらず *You can't hurry
    love*／[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")
      - 作詞・作曲：Lamont Dozier／Brian Holland／Eddie
        Holland、譯詞：武川行秀、合唱：タケカワユキヒデ／タケカワユイ、編曲・演奏：Thet's
        on NoiseThat's on Noise、舞蹈指導：星野利満、演出者：Girls of '98、原曲：You Can't
        Hurry Love／[TheSUPREMES](../Page/TheSUPREMES.md "wikilink")
  - YAKINIC GO GO／[松川佳以](../Page/松川佳以.md "wikilink")
      - 作詞・作曲・編曲：種ともこ、演奏：尾上サトシ／松林正志、舞蹈指導：まりそう、演出者：グルグル／プリン／フー／ペペロンチーノ／ストレッチマン／ゆかちゃん／ポッケ／海道亮平／田中樹里／橋田紘緒
  - YOU ARE MY
    DREAM／[中田あすみ](../Page/中田あすみ.md "wikilink")&[佐久間信子](../Page/佐久間信子.md "wikilink")
      - 作詞・作曲・譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：TTKダンスプロジェクト
  - JUMP／[安藤奏](../Page/安藤奏.md "wikilink")&[ダーブロウ有紗](../Page/有紗.md "wikilink")
      - 作詞・作曲：Alex Van Halen／Edward Van Halen／David Lee
        Roth、譯詞：武川行秀、編曲・演奏：That's on
        Noise、演出者：山崎邦正／リサ・ステッグマイヤー／TV戰士1998、原曲：JUMP／[VANHALEN](../Page/ヴァン・ヘイレン.md "wikilink")
  - 極楽はどこだ／[\#ザ・フルース・ブラザース](../Page/#ザ・フルース・ブラザース.md "wikilink")（[海道亮平](../Page/海道亮平.md "wikilink")、[橋田紘緒](../Page/橋田紘緒.md "wikilink")）
      - 合唱：タケカワユキヒデ／タケカワゆい、作詞・作曲：小宮山雄飛、編曲：武川行秀與That's on Noise、演奏：That's
        on Noise、原曲： 極楽はどこだ／[ホフディラン](../Page/ホフディラン.md "wikilink")

### 1999年度

  - 恋のギルティー *Love in the first
    degree*／[\#モンキークイーン](../Page/#モンキークイーン.md "wikilink")（[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")、[佐久間信子](../Page/佐久間信子.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")）
      - 合唱：タケカワユキヒデ／タケカワゆい、作詞：Matt Aitken／Mike Stock／Peter
        Waterman／Sarah Dallin／Siobhan Fahey／Keren
        Woodward、譯詞：武川行秀、作曲：Matt Aitken／Mike
        Stock／Peter Waterman、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：シンパンマン、原曲：Love In The First
        Degree／[バナナラマ](../Page/バナナラマ.md "wikilink")
  - 空にまいあがれ／[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")
      - 合唱：タケカワユキヒデ／タケカワゆい、作詞・作曲：倉持陽一、編曲：武川行秀與That's on Noise、演奏：That's
        on
        Noise／森田彩華、原曲：空にまい上がれ／[真心ブラザーズ](../Page/真心ブラザーズ.md "wikilink")
  - Can do
    it\!／[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")・[福田亮太](../Page/福田亮太.md "wikilink")・[山元竜一](../Page/山元竜一.md "wikilink")
      - 作詞：坂田和子、作曲：MASAKI、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、原曲：Can do it\!／[Go-full
        sun](../Page/Go-full_sun.md "wikilink")
  - ゴォ\!／[棚橋由希](../Page/棚橋由希.md "wikilink")
      - 合唱：饗場詩野、作詞：サエキけんぞう、作曲：奥田民生、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、演出者：Friends of
        Turner、原曲：ゴォ\!／[山瀬まみ](../Page/山瀬まみ.md "wikilink")
  - 恋は早いもの勝ち／[大沢あかね](../Page/大沢あかね.md "wikilink")&[中田あすみ](../Page/中田あすみ.md "wikilink")
      - 作詞：岡部真理子、作曲：藤生善一、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、拍攝外景的地點：原宿、原曲：恋は早いもの勝ち／[YO YO
        YO](../Page/YO_YO_YO.md "wikilink")
  - [スリラー](../Page/スリラー.md "wikilink")／[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")&[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")&[ダーブロウ有紗](../Page/有紗.md "wikilink")&[ジェームス・マーティン](../Page/ジミーMackey.md "wikilink")
      - 原詞・作曲：Rod Temperton、譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、演出者：TV戰士1999／笑福亭松之助／豊田眞唯、原曲：Thriller／[マイケル・ジャクソン](../Page/マイケル・ジャクソン.md "wikilink")
  - ハッピーマン／[石部里紗](../Page/石部里紗.md "wikilink")
      - 合唱
        [\#モンキークイーン](../Page/#モンキークイーン.md "wikilink")、作詞・作曲：奥井香、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise、拍攝外景的地點：静岡縣浜松市中田島砂丘、ハッピーマン／[奥居香](../Page/奥居香.md "wikilink")
  - ママ・ミア／[\#モンキークイーン](../Page/#モンキークイーン.md "wikilink")（[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")、[佐久間信子](../Page/佐久間信子.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")）
      - 作詞・作曲：Benny Andersson／Stig Anderson／Bjorn
        Ulvaeus、譯詞：武川行秀、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise／佐藤ふくみ、舞蹈指導：T-ASADA、演出者：Wentz瑛士／福田亮子／シンパンマン／T-ASADA、原曲：Mamma
        Mia／ABBA（[アバ](../Page/アバ.md "wikilink")）
  - 君の声がする／[饗場詩野](../Page/饗場詩野.md "wikilink")
      - 作詞・作曲：遊佐未森、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise／佐藤ふくみ／嵜田ハヂメ／大町剛、演出者：ジェームス・マーティン／遊佐未森
  - カーマは気まぐれ *Carma Chameleon*／[山崎邦正](../Page/山崎邦正.md "wikilink")
      - 合唱：E-Monkey Projest、作詞・作曲：George O'Dowd／Roy Hay／Jon Moss／Michael
        Craig／Phil Pickett、譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise／佐藤ふくみ、舞蹈指導：T-ASADA、演出者：Boys of '99（1999年度のTV戰士）、原曲：Karma
        Chameleon／[カルチャー・クラブ](../Page/カルチャー・クラブ.md "wikilink")
  - 運命'99／[\#悪魔なエンジェル](../Page/#悪魔なエンジェル.md "wikilink")（[中田あすみ](../Page/中田あすみ.md "wikilink")、[大沢あかね](../Page/大沢あかね.md "wikilink")、[安斎舞](../Page/安齊舞.md "wikilink")、[徐桑安](../Page/徐桑安.md "wikilink")）
      - 作詞：森若香織、作曲：黒沢健一、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：Wentz瑛士／福田亮子／シンパンマン、原曲：運命'95／[Melody](../Page/Melody_\(アイドルグループ\).md "wikilink")
  - はるかな旅へ *Where'll we go from
    now*／[橋田紘緒](../Page/橋田紘緒.md "wikilink")&[福田亮太](../Page/福田亮太.md "wikilink")&[須田泰大](../Page/須田泰大.md "wikilink")
      - 原詞：奈良橋陽子、補作詞：武川行秀、作曲：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：Where'll we go from
        now／[ゴダイゴ](../Page/ゴダイゴ.md "wikilink")
  - The Longest
    Time／[\#飛び出せ\!ステッグマイヤーズ](../Page/#飛び出せ!ステッグマイヤーズ.md "wikilink")（[リサ・ステッグマイヤー](../Page/リサ・ステッグマイヤー.md "wikilink")、[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")、[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")、[棚橋由希](../Page/棚橋由希.md "wikilink")、[ジェームス・マーティン](../Page/ジミーMackey.md "wikilink")、[ダーブロウ有紗](../Page/有紗.md "wikilink")）
      - 作詞・作曲：Billy Joel、譯詞：武川行秀／Billy Joel、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise／イシヅ“U－HIP”マスオ、原曲：The Longest
        Time／[ビリー・ジョエル](../Page/ビリー・ジョエル.md "wikilink")
  - 世界中の全ての色／[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")&[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")&[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")
      - 作詞・作曲：坂本サトル、編曲：武川行秀與That's on Noise、演奏：That's on Noise

### 2000年度

  - Roam～愛があれば～／[\#ワイルドベリーズ](../Page/#ワイルドベリーズ.md "wikilink")（[ダーブロウ有紗](../Page/有紗.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")）
      - 作詞：Robert Waldrop、譯詞：武川行秀、作曲：The B-52's、編曲：武川行秀與That's on
        Noise、演奏：That's on
        Noise、原曲：Roam／[B-52's](../Page/B-52's.md "wikilink")
  - たんぽぽ／[福田亮太](../Page/福田亮太.md "wikilink")
      - 作詞・作曲：つじあやの、編曲：武川行秀與That's on Noisee、演奏：That's on
        Noise／新谷キヨシ、演出者：安齋舞
  - Dynamite／[\#ザ・ヤマチーズ](../Page/#ザ・ヤマチーズ.md "wikilink")（[山元竜一](../Page/山元竜一.md "wikilink")、[村田千宏](../Page/村田Chihiro.md "wikilink")）
      - 合唱：伊藤俊輔／安齋舞、作詞・作曲：Rod Stewart／Andy
        Taylor、譯詞：桑原永江、編曲：武川行秀、演奏：武川行秀與That's
        on
        Noise、原曲：Dynamite／[ロッド・スチュワート](../Page/ロッド・スチュワート.md "wikilink")
  - おかしな午後／[安斎舞](../Page/安齊舞.md "wikilink")
      - 合唱：タケカワユイ、作詞：小川美潮、作曲：板倉文、編曲：武川行秀與That's on Noisee、演奏：That's on
        Noise、原曲：おかしな午後／[小川美潮](../Page/小川美潮.md "wikilink")
  - 走れRUN²\!／[\#ランランズ](../Page/#ランランズ.md "wikilink")（[俵有希子](../Page/俵有希子.md "wikilink")、[須田泰大](../Page/須田泰大.md "wikilink")、[徐桑安](../Page/徐桑安.md "wikilink")、[ローブリィ翔](../Page/ローブリィ翔.md "wikilink")）
      - 作詞・作曲：森若香織、編曲・演奏：KANAME
  - ラッキー・ラブ *I should be so
    lucky*／[\#スウィート・ピクルス](../Page/#スウィート・ピクルス.md "wikilink")（[佐久間信子](../Page/佐久間信子.md "wikilink")、[ダーブロウ有紗](../Page/有紗.md "wikilink")、[俵有希子](../Page/俵有希子.md "wikilink")）
      - 作詞・作曲：Matt Aitken／Mike Stock／Peter Waterman、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise、舞蹈指導：T-ASADA、演出者：ローブリィ翔／げんしじん、原曲：I
        Should be so Lucky／[カイリー・ミノーグ](../Page/カイリー・ミノーグ.md "wikilink")
  - わかってナイ\!／[\#水ふうせん](../Page/#水ふうせん.md "wikilink")（[福田亮太](../Page/福田亮太.md "wikilink")、[逵優希](../Page/逵優希.md "wikilink")）
      - 作詞・作曲：種ともこ、編曲：松林正志、演奏：松林正志／尾上サトシ、演出者：饗場詩野／げんしじん
  - I LIKE
    YOU／[\#ザ・フルース・ブラザース](../Page/#ザ・フルース・ブラザース.md "wikilink")2000（[海道亮平](../Page/海道亮平.md "wikilink")、[橋田紘緒](../Page/橋田紘緒.md "wikilink")、[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")）
      - 合唱：[\#スウィート・ピクルス](../Page/#スウィート・ピクルス.md "wikilink")、原曲：I LIKE
        YOU／RC Succession、作詞・作曲：忌野清志郎、編曲：武川行秀與That's on Noise、演奏：That's
        on Noise、演出者：仲根紗央莉／げんしじん
  - HONEY
    BEAT／[\#MO-SHI-YA](../Page/#MO-SHI-YA.md "wikilink")（[饗場詩野](../Page/饗場詩野.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")、[山元竜一](../Page/山元竜一.md "wikilink")）
      - 合唱：饗場詩野／モニーク・ローズ、作詞・作曲：広瀬香美、編曲・演奏：島津正多、舞蹈指導：T-ASADA
  - www.access.me／[\#c@mo.ne](../Page/#c@mo.ne.md "wikilink")（[中田あすみ](../Page/中田あすみ.md "wikilink")、[福田亮太](../Page/福田亮太.md "wikilink")、[石田比奈子](../Page/石田比奈子.md "wikilink")、[逵優希](../Page/逵優希.md "wikilink")、[熊木翔](../Page/熊木翔.md "wikilink")、[松下博昭](../Page/松下博昭.md "wikilink")）
      - 作詞・作曲：広瀬香美、サビモチーフ：石田比奈子、編曲・演奏：島津正多、舞蹈指導：T-ASADA
  - SUPERGIRL／[石田比奈子](../Page/石田比奈子.md "wikilink")
      - 合唱・作詞・作曲・編曲・演奏：浅田祐介／種ともこ
  - Angel Snow／[中田あすみ](../Page/中田あすみ.md "wikilink")
      - 作詞・作曲：遊佐未森、編曲：KANAME、演奏：KANAME／河野伸、演出者：遊佐未森／奈良本浩樹
  - 愛はきらめきの中に *How deep is your
    love?*／[\#セブン・ハーツ](../Page/#セブン・ハーツ.md "wikilink")（[饗場詩野](../Page/饗場詩野.md "wikilink")、[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")、[佐久間信子](../Page/佐久間信子.md "wikilink")、[ダーブロウ有紗](../Page/有紗.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")、[熊木翔](../Page/熊木翔.md "wikilink")、[エバンス太郎](../Page/エバンス太郎.md "wikilink")）
      - 作詞・作曲：Barry, Robin and Maurice Gibb、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise
  - 歩いていこう／[\#プチ・チェリーズ](../Page/#プチ・チェリーズ.md "wikilink")（[安齊舞](../Page/安齊舞.md "wikilink")、[徐桑安](../Page/徐桑安.md "wikilink")）
      - 作詞・作曲・編曲：田村敦、演奏：池田成人／鈴木とよき

### 2001年度

  - 君のそばにいたい *I only want to be with
    you*／[\#ハッスル3](../Page/#ハッスル3.md "wikilink")（[山元竜一](../Page/山元竜一.md "wikilink")、[熊木翔](../Page/熊木翔.md "wikilink")、[竪山隼太](../Page/竪山隼太.md "wikilink")）
      - 作詞・作曲：Mike Hawker／Ivor Raymonde、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on
        Noise、演出者：奈良沙緒理／げんしじん、拍攝外景的地點：日光国立公園ウエスタン村、原曲：I
        only Want to be with
        You／[ダスティ・スプリングフィールド](../Page/ダスティ・スプリングフィールド.md "wikilink")、[ベイシティローラーズ](../Page/ベイシティローラーズ.md "wikilink")
  - 本当は何になりたいんだろう／[\#バースデイ・ガール](../Page/#バースデイ・ガール.md "wikilink")（[中田あすみ](../Page/中田あすみ.md "wikilink")、[村上東奈](../Page/村上東奈.md "wikilink")）
      - 作詞：金城綾乃、作曲：玉城千春、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：本当は何になりたいんだろう／[玉城千春](../Page/玉城千春.md "wikilink")
  - 元気を出して *Gotta Pull Myself
    Together*／[\#アップル・シェイク](../Page/#アップル・シェイク.md "wikilink")（[ダーブロウ有紗](../Page/有紗.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")）
      - 作詞・作曲：Ben Findon／Mike Myers／Bob Puzey、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise、舞蹈指導：T-ASADA、原曲：Gotta Pull Myself
        Together／[ノーランズ](../Page/ノーランズ.md "wikilink")
  - きれいな水／[熊木翔](../Page/熊木翔.md "wikilink")
      - 作詞・作曲：YO-KING、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：きれいな水／[YO-KING](../Page/YO-KING.md "wikilink")
  - 本日晴天／[俵有希子](../Page/俵有希子.md "wikilink")
      - 合唱：[\#アップル・シェイク](../Page/#アップル・シェイク.md "wikilink")、作詞：谷穗ちろる、作曲：松浦雅也、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise、演出者：[\#アップル・シェイク](../Page/#アップル・シェイク.md "wikilink")、原曲：本日晴天／[原田知世](../Page/原田知世.md "wikilink")
  - VACATION／[\#ホリデーズ](../Page/#ホリデーズ.md "wikilink")（[ローブリィ翔](../Page/ローブリィ翔.md "wikilink")、[井出卓也](../Page/井出卓也.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")、[中村有沙](../Page/中村有沙.md "wikilink")）
      - 作詞・作曲・編曲・演奏：松江潤、舞蹈指導 T-ASADA
  - Everything You
    Do／[\#モンキークイーン](../Page/#モンキークイーン.md "wikilink")（[ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")、[佐久間信子](../Page/佐久間信子.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")）
      - 作詞・作曲：Lars Aass／Marit Larsen／Marion Raven、譯詞：武川行秀、編曲：武川行秀與That's
        on Noise、演奏：That's on Noise／佐藤ふくみ、舞蹈指導：T-ASADA、原曲：Everything You
        DO／[M2M](../Page/M2M_\(組合\).md "wikilink")
  - 花のランランパワー／[\#ノーティー・ハリー](../Page/#ノーティー・ハリー.md "wikilink")（[エバンス太郎](../Page/エバンス太郎.md "wikilink")、[松井蘭丸](../Page/松井蘭丸.md "wikilink")、[八木俊彦](../Page/八木俊彦.md "wikilink")）
      - 作詞・作曲：倉持陽一、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：花のランランパワー／[真心ブラザーズ](../Page/真心ブラザーズ.md "wikilink")
  - BIG SMILE BABIES／[ダーブロウ有紗](../Page/有紗.md "wikilink")
      - 合唱・作詞・作曲：森若香織、編曲・演奏：KANAME、演出者：森若香織／KANAME
  - Twilight／[\#The Yamachee's](../Page/#ザ・ヤマチーズ.md "wikilink") feat.
    [\#Apple Shake](../Page/#アップル・シェイク.md "wikilink") with
    SHUN（[山元竜一](../Page/山元竜一.md "wikilink")、[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[ダーブロウ有紗](../Page/有紗.md "wikilink")、[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")、[伊藤俊輔](../Page/伊藤俊輔_\(タレント\).md "wikilink")）
      - 合唱：[\#アップル・シェイク](../Page/#アップル・シェイク.md "wikilink")、作詞・作曲：Jeff
        Lynne、譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise／佐藤ふくみ、演出者：[\#アップル・シェイク](../Page/#アップル・シェイク.md "wikilink")／伊藤俊輔／うなぎ大王、原曲：Twilight／[エレクトリック・ライト・オーケストラ](../Page/エレクトリック・ライト・オーケストラ.md "wikilink")
  - Jump in the Line／[角田信朗](../Page/角田信朗.md "wikilink")
      - 合唱：[\#ワイルドベリーズ](../Page/#ワイルドベリーズ.md "wikilink")、作詞：Harry
        Belafonte／Ralph De Leon／Gabriel Oller／Steve
        Samuel、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on
        Noise／イシヅ“U－HIP”マスオ、舞蹈指導：T-ASADA、演出者：[\#ワイルドベリーズ](../Page/#ワイルドベリーズ.md "wikilink")／真壁としみ、原曲：Jump
        in the Line／[ハリー・ベラフォンテ](../Page/ハリー・ベラフォンテ.md "wikilink")
  - Be My Friend／[モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")
      - 合唱・作詞・作曲・編曲・演出者：種ともこ、演奏 種ともこ／古川昌義／松永孝義／楠均
  - クレマチス／[山川恵里佳](../Page/山川恵里佳.md "wikilink")
      - 合唱：[\#T for
        TWO](../Page/#T_for_TWO.md "wikilink")、作詞・作曲：遊佐未森、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise、原曲：クレマチス／[遊佐未森](../Page/遊佐未森.md "wikilink")
  - Material Girl／[\#3サイズ](../Page/#3サイズ.md "wikilink") with
    [\#コバンザメ](../Page/#コバンザメ.md "wikilink")（[中田あすみ](../Page/中田あすみ.md "wikilink")、[安齊舞](../Page/安齊舞.md "wikilink")、[豕瀬志穗](../Page/豕瀬志穗.md "wikilink")、[橋田紘緒](../Page/橋田紘緒.md "wikilink")、[福田亮太](../Page/福田亮太.md "wikilink")）
      - 作詞・作曲：Peter H. Brown／Robert S. Rans、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise、舞蹈指導：T-ASADA、演出者：やぎっち様／どーよ、原曲：Material
        Girl／[マドンナ](../Page/マドンナ_\(歌手\).md "wikilink")
  - 空になりたい／[\#TIMKY](../Page/#TIMKY.md "wikilink")（[熊木翔](../Page/熊木翔.md "wikilink")、[山元竜一](../Page/山元竜一.md "wikilink")、[俵有希子](../Page/俵有希子.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")、[村上東奈](../Page/村上東奈.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")、[種ともこ](../Page/種ともこ.md "wikilink")）
      - 作詞・作曲・演奏：[\#TIMKY](../Page/#TIMKY.md "wikilink")、編曲：種ともこ、拍攝外景的地點：北海道札幌市教育文化会館大ホール

### 2002年度

  - 1-2-3／[\#SPACE
    FAIRY](../Page/#SPACE_FAIRY.md "wikilink")（[俵有希子](../Page/俵有希子.md "wikilink")、[中村有沙](../Page/中村有沙.md "wikilink")、[白木杏奈](../Page/白木杏奈.md "wikilink")）
      - 作詞・作曲：Gloria Estefan／Enrique Garcia、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：ローブリィ翔、原曲：1・2・3／[グロリア・エステファン&マイアミサウンドマシン](../Page/グロリア・エステファン&マイアミサウンドマシン.md "wikilink")
  - 僕であるために／[\#Boys Be
    Boys](../Page/#Boys_Be_Boys.md "wikilink")（[山元竜一](../Page/山元竜一.md "wikilink")、[熊木翔](../Page/熊木翔.md "wikilink")、[松井蘭丸](../Page/松井蘭丸.md "wikilink")）
      - 作詞：浜崎貴司、作曲：FLYING KIDS、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：僕であるために／[FLYING
        KIDS](../Page/FLYING_KIDS.md "wikilink")
  - クエスチョン／[ローブリィ翔](../Page/ローブリィ翔.md "wikilink")
      - 合唱：村上東奈／井出卓也 、作詞：今泉泰幸 、作曲：遠藤肇
        、編曲：スキップカウズ、演奏：スキップカウズ／武藤星兒、演出者：スキップカウズ／村上東奈／井出卓也
  - BYE BYE DAYS／[\#T for
    TWO](../Page/#T_for_TWO.md "wikilink")（[俵有希子](../Page/俵有希子.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")）
      - 作詞：アイコ、作曲：石坂義晴、編曲：advantage Lucy、演奏：advantage Lucy／千ヶ崎学／堀越和子
  - スピリチュアル／[\#CHU-LIPS](../Page/#CHU-LIPS.md "wikilink")（[村田Chihiro](../Page/村田Chihiro.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
      - 合唱：熊木翔／村上東奈、作詞・作曲：渡辺慎、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、演出者：熊木翔／村上東奈、原曲：スピリチュアル／[ホフディラン](../Page/ホフディラン.md "wikilink")
  - STEADY BOYS／[岩井七世](../Page/岩井七世.md "wikilink")
      - 合唱 [\#T for
        TWO](../Page/#T_for_TWO.md "wikilink")、作詞：透影月奈、作曲：安藤芳彦、編曲：武川行秀與That's
        on Noise、演奏：That's on Noise、演出者：[\#T for
        TWO](../Page/#T_for_TWO.md "wikilink")、原曲：STEADY
        BOYS／[原田知世](../Page/原田知世.md "wikilink")
  - 星の軌跡／[\#ギャラクシー](../Page/#ギャラクシー.md "wikilink")（[安齊舞](../Page/安齊舞.md "wikilink")、[井出卓也](../Page/井出卓也.md "wikilink")、[ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")）
      - 作詞・作曲：田村敦、編曲：イチケン、演奏：JAETTER3、原曲：ホシノキセキ／[JETTER3](../Page/JETTER3.md "wikilink")
  - あこがれ／[村上東奈](../Page/村上東奈.md "wikilink") ※分成acoustic version與hip-hop
    versionに各播出2週
      - 『あこがれ～acoustic version～』作詞・作曲：遊佐未森、編曲・演奏：KANAME
      - 『あこがれ～hip-hop version～』作詞・作曲：遊佐未森、編曲・演奏：KANAME
  - BE HAPPY
    DAY／[八木俊彦](../Page/八木俊彦.md "wikilink")&[豕瀬志穗](../Page/豕瀬志穗.md "wikilink")
      - 合唱・作詞・作曲：森若香織、編曲・演奏：KANAME
  - 恋にメリーゴーラウンド *In for a penny,In for a pound*／[\#SPACE
    FAIRY](../Page/#SPACE_FAIRY.md "wikilink")（[俵有希子](../Page/俵有希子.md "wikilink")、[中村有沙](../Page/中村有沙.md "wikilink")、[白木杏奈](../Page/白木杏奈.md "wikilink")）
      - 作詞・作曲：John Moering、譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：ローブリィ翔、原曲：In for a Penny, In for a
        Pound／[アラベスク](../Page/アラベスク.md "wikilink")
  - ルーキー／[山元竜一](../Page/山元竜一.md "wikilink")
      - 合唱・演出者：[\#Boys Be
        Boys](../Page/#Boys_Be_Boys.md "wikilink")、作詞・作曲：玉置浩二、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise、原曲：ルーキー／[玉置浩二](../Page/玉置浩二.md "wikilink")
  - 地図／[\#マイ☆コウ](../Page/#マイ☆コウ.md "wikilink")（[堀江幸生](../Page/堀江幸生.md "wikilink")、[マイケル・メンツァー](../Page/Mikey.md "wikilink")）
      - 合唱：熊木翔、作詞：今泉泰幸、作曲：遠藤肇、編曲：スキップカウズ、演奏：スキップカウズ／山本哲也

### 2003年度

  - ぼくらのロックシティ *We built this
    city*／[\#ザ・ヤマチーズ](../Page/#ザ・ヤマチーズ.md "wikilink")
    with
    [\#ふたりいる。](../Page/#ふたりいる。.md "wikilink")（[山元竜一](../Page/山元竜一.md "wikilink")、[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")）
      - 合唱：[\#ふたりいる。](../Page/#ふたりいる。.md "wikilink")、DJ・演出者：ブライアン・ウォルターズ、作詞・作曲：Lambert,
        Page, Taugh, Wolf、譯詞：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：We Built This
        City／[スターシップ](../Page/スターシップ.md "wikilink")
  - サンデーモーニング／-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
      - 合唱
        [\#ふたりいる。](../Page/#ふたりいる。.md "wikilink")、作詞・作曲：つじあやの、編曲：武川行秀與That's
        on Noise、演奏：That's on
        Noise、原曲：サンデーモーニング／[つじあやの](../Page/つじあやの.md "wikilink")
  - 虹の都へ／[ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")
      - 合唱：山元竜一、作詞・作曲：高野寛、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：虹の都へ／[高野寛](../Page/高野寛.md "wikilink")
  - 泣けちゃうの *You caught me
    out*／[\#システム★エラー](../Page/#システム★エラー.md "wikilink")（[岩井七世](../Page/岩井七世.md "wikilink")、[白木杏奈](../Page/白木杏奈.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")、[堀口美咲](../Page/堀口美咲.md "wikilink")）
      - 作詞・作曲：K.MacColl／Briquette／Crowe、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise、演出者：伊藤俊輔／ジャスミン・アレン、原曲：You Caught Me
        Out／[Tracey Ullman](../Page/Tracey_Ullman.md "wikilink")
  - Go\!Go\!たまご丼／間寛平 with
    [\#TDD](../Page/#TDD.md "wikilink")（[間寛平](../Page/間寛平.md "wikilink")、[井出卓也](../Page/井出卓也.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-、[前田公輝](../Page/前田公輝.md "wikilink")、[山川恵里佳](../Page/山川恵里佳.md "wikilink")）
      - 作詞：間寛平／KANAME、作曲：間寛平、編曲：KANAME、演奏：KANAME／阿部耕作、舞蹈指導：T-ASADA
  - それっきゃないかもね／[堀江幸生](../Page/堀江幸生.md "wikilink")
      - 合唱・作詞・作曲・編曲：種ともこ、演奏：尾上サトシ／周藤寿和／種ともこ、演出者：村田Chihiro／de・Lencquesaing望／堀口美咲／小井沼愛
  - JUNGLE
    FUTURE／[\#ノイジー・モンキーズ](../Page/#ノイジー・モンキーズ.md "wikilink")（[ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")、[近藤Emma](../Page/近藤Emma.md "wikilink")、[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")）
      - 作詞・作曲・編曲：福岡ユタカ、演奏 福岡ユタカ／Whacho／John Sackett
  - BAKAはここにいる／[俵小百合](../Page/俵小百合.md "wikilink")
      - 合唱・作詞・作曲・編曲：種ともこ、演奏 尾上サトシ／周藤寿和／種ともこ、演出者 山元竜一／中村有沙／八木俊彦／桜井結花
  - 旅人は星を数える *Days Are
    Numbers*／[\#MEMORIES](../Page/#MEMORIES.md "wikilink")（[山元竜一](../Page/山元竜一.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")、[白木杏奈](../Page/白木杏奈.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-、[マイケル・メンツァー](../Page/Mikey.md "wikilink")）
      - 合唱：岩井七世／俵小百合／白木杏奈／飯田里穗、作詞・作曲：Alan Parsons, Eric
        Woolfson、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise／佐藤ふくみ／武川行秀、原曲：Days Are Numbers
        (Traveller)／[アラン・パーソンズ・プロジェクト](../Page/アラン・パーソンズ・プロジェクト.md "wikilink")
  - Together
    Forever／[\#SYANIS](../Page/#SYANIS.md "wikilink")（[中村有沙](../Page/中村有沙.md "wikilink")、[豕瀬志穗](../Page/豕瀬志穗.md "wikilink")、[桜井結花](../Page/桜井結花.md "wikilink")）
      - 作詞・作曲：Stock／Altken／Waterman、譯詞：武川行秀、編曲：武川行秀與That's on
        Noise演奏：That's on Noise／佐藤ふくみ、舞蹈指導：T-ASADA
  - カンペキ／[井出卓也](../Page/井出卓也.md "wikilink")、原曲：Together
    Forever／[リック・アストリー](../Page/リック・アストリー.md "wikilink")
      - 作詞：今泉泰幸、作曲：遠藤肇、編曲：スキップカウズ、演奏：スキップカウズ／藤原マヒト、演出者：マイ★コウ／藤原ひとみ
  - 星と月の仲間／[白木杏奈](../Page/白木杏奈.md "wikilink")
      - 作詞・作曲：高橋朋子、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：星と月の仲間／[elliott](../Page/elliott.md "wikilink")
  - モンキーマジック／[\#A.T.7](../Page/#A.T.7.md "wikilink")（[岩井七世](../Page/岩井七世.md "wikilink")、[俵小百合](../Page/俵小百合.md "wikilink")、[八木俊彦](../Page/八木俊彦.md "wikilink")、[ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")、[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")、[張沢紫星](../Page/張沢紫星.md "wikilink")、[川﨑樹音](../Page/樹音.md "wikilink")）
      - 作詞：奈良橋陽子、譯詞・作曲：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：Monkey Magic／[ゴダイゴ](../Page/ゴダイゴ.md "wikilink")
  - 水玉／[\#ホワイト・クローバー](../Page/#ホワイト・クローバー.md "wikilink")（[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[岩井七世](../Page/岩井七世.md "wikilink")）
      - 作詞・作曲：遊佐未森、編曲：武川行秀與That's on Noise、演奏：That's on Noise

### 2004年度

  - (R)ラジオスターの悲劇 *Video killed the radio
    star*／[\#ランダム\!\!スロー](../Page/#ランダム!!スロー.md "wikilink")（[白木杏奈](../Page/白木杏奈.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")、[前田公輝](../Page/前田公輝.md "wikilink")、[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")）
      - 作詞・作曲：Bruce Woolley／Geoffey Downes\&Trevor
        Horn、譯詞：小川哲朗、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise、舞蹈指導：T-ASADA、原曲：Video Killed radio
        star／[バグルス](../Page/:en:The_Buggles.md "wikilink")
  - (U)明日への叫び *SHOUT IT OUT
    LOUD*／[\#KIZZ](../Page/#KIZZ.md "wikilink")（-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-、[堀江幸生](../Page/堀江幸生.md "wikilink")、[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")、[篠原愛實](../Page/篠原愛實.md "wikilink")）（畫面比例是16：9）
      - 作詞・作曲：G.Simmons／P.Stanley／B.Ezrin、譯詞：王様、編曲・演奏：Gargoyle、原曲：Shout
        it out Loud／[Kiss](../Page/キッス.md "wikilink")
  - (R)[\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")／[\#ヴィーナス&マーズ](../Page/#ヴィーナス&マーズ.md "wikilink")（[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[井出卓也](../Page/井出卓也.md "wikilink")、[白木杏奈](../Page/白木杏奈.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")、[川﨑樹音](../Page/樹音.md "wikilink")、[橋本甜歌](../Page/橋本甜歌.md "wikilink")）（畫面比例是16：9）
      - 合唱：白木杏奈／ジョアン・ヤマザキ／川﨑樹音／橋本甜歌、作詞・作曲：武川行秀、編曲：武川行秀與That's on
        Noise、演奏：That's on Noise
  - (U)未来船ゴー／[\#じゅげむさん](../Page/#じゅげむさん.md "wikilink")（[堀江幸生](../Page/堀江幸生.md "wikilink")、[近藤Emma](../Page/近藤Emma.md "wikilink")、[伊倉愛美](../Page/伊倉愛美.md "wikilink")、[高橋郁哉](../Page/高橋郁哉.md "wikilink")）
      - 作詞：小川美潮、作曲・演出者：仙波清彦、編曲：沼井雅之、演奏：仙波清彦／沼井雅之／高橋香織／神田朋樹、舞蹈指導：中本雅俊
  - (R)MARCH／[\#グラスオニオン](../Page/#グラスオニオン.md "wikilink")（[井出卓也](../Page/井出卓也.md "wikilink")、[前田公輝](../Page/前田公輝.md "wikilink")、[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")）（畫面比例是16：9）
      - 作詞・作曲：原田真二、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、拍攝外景的地點：屏風ヶ浦
  - (U)きみのとなりで／[\#トゥインクル](../Page/#トゥインクル.md "wikilink")（[中村有沙](../Page/中村有沙.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-）
      - 作詞：遊佐未森、作曲・編曲・演奏：西村由紀江
  - (R)ココロ磁石／[\#Time And A
    Word](../Page/#Time_And_A_Word.md "wikilink")（[白木杏奈](../Page/白木杏奈.md "wikilink")、[ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")）（畫面比例是16：9）
      - 作詞・作曲：高橋朋子、編曲：苅和奈央、演奏：苅和奈央／TOMOKO
  - (U)ホーリー&ブライト／[\#ゴーライブ](../Page/#ゴーライブ.md "wikilink")（[張沢紫星](../Page/張沢紫星.md "wikilink")、[篠原愛實](../Page/篠原愛實.md "wikilink")、[高橋郁哉](../Page/高橋郁哉.md "wikilink")）
      - 原詞：山上路夫／奈良橋陽子、作曲：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on
        Noise、原曲：[ホーリー&ブライト](../Page/ホーリー&ブライト.md "wikilink")／[ゴダイゴ](../Page/ゴダイゴ.md "wikilink")
  - (R)ダンシング・シスター／[\#フルーツ♥パーティー](../Page/#フルーツ♥パーティー.md "wikilink")（[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[川﨑樹音](../Page/樹音.md "wikilink")、[橋本甜歌](../Page/橋本甜歌.md "wikilink")）
      - 作詞・作曲：Findon／Myers／Puzey、譯詞：小山哲朗、編曲：武川行秀與That's on
        Noise、演奏：That's on
        Noise、舞蹈指導：T-ASADA、演出者：Barnes勇氣、原曲：I'm
        in the Mood for
        Dancing／[NORLANS](../Page/:en:The_Nolans.md "wikilink")
  - [\#ヴォヤージュ±](../Page/#ヴォヤージュ±.md "wikilink")／[\#ユービック](../Page/#ユービック.md "wikilink")（[井出卓也](../Page/井出卓也.md "wikilink")、[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")、[洸太レイシー](../Page/洸太レイシー.md "wikilink")、[伊倉愛美](../Page/伊倉愛美.md "wikilink")、[近藤Emma](../Page/近藤Emma.md "wikilink")、[千秋レイシー](../Page/千秋レイシー.md "wikilink")、[淺野優梨愛](../Page/淺野優梨愛.md "wikilink")、[川﨑樹音](../Page/樹音.md "wikilink")）（畫面比例是16：9）
      - 作詞：サエキけんぞう、作曲・編曲：福岡ユタカ、演奏：Yen chang／伊丹雅博／Takeru,F &
        Kanade,F、拍攝外景的地點：首都圏外郭放水路

※(R)……RG曲、(U)……UWF曲

### 2005年度

  - (S)僕らのハーモニー／[\#Our
    Treasures](../Page/#Our_Treasures.md "wikilink")（[前田公輝](../Page/前田公輝.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-、[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")、[木內江莉](../Page/木內江莉.md "wikilink")）
      - 作詞・作曲・編曲・演奏：原田真二
  - (J)さかさまさかさ／[\#Tiny
    Circus](../Page/#Tiny_Circus.md "wikilink")（[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")、[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[橋本甜歌](../Page/橋本甜歌.md "wikilink")、[木內梨生奈](../Page/木內梨生奈.md "wikilink")）
      - 作詞：カヒミ・カリィ、作曲 神田朋樹、演奏：仙波清彦／神田朋樹
  - (S)ムカつくけれど……スキ\!／[\#Our
    Treasures](../Page/#Our_Treasures.md "wikilink")（[前田公輝](../Page/前田公輝.md "wikilink")、-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-、[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")、[木內江莉](../Page/木內江莉.md "wikilink")）
      - 作詞・作曲：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on Noise
  - (J)僕はドライブ／[\#Tiny
    Circus](../Page/#Tiny_Circus.md "wikilink")（[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")、[村田Chihiro](../Page/村田Chihiro.md "wikilink")、[橋本甜歌](../Page/橋本甜歌.md "wikilink")、[木內梨生奈](../Page/木內梨生奈.md "wikilink")）

<!-- end list -->

  -   - 作詞
        サエキけんぞう、作曲・編曲：polymoog（[ELEKTEL](../Page/ELEKTEL.md "wikilink")）、演奏
        polymoog（[ELEKTEL](../Page/ELEKTEL.md "wikilink")）／仙波清彦

※(S)……[蒸氣騎士團曲](../Page/蒸氣騎士團.md "wikilink")(J)……[蒸氣魔術師曲](../Page/蒸氣魔術師.md "wikilink")

### 2006年度

  - ダンゼン\!未来(斷然\!未來)／[TV戰士](../Page/TV戰士.md "wikilink")2006（[洸太Lacey](../Page/洸太Lacey.md "wikilink")、[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")、[篠原愛實](../Page/篠原愛實.md "wikilink")、[永島謙二郎](../Page/永島謙二郎.md "wikilink")、[伊倉愛美](../Page/伊倉愛美.md "wikilink")、[高橋郁哉](../Page/高橋郁哉.md "wikilink")、[橋本甜歌](../Page/橋本甜歌.md "wikilink")、[木內江莉](../Page/木內江莉.md "wikilink")、[大木梓彩](../Page/大木梓彩.md "wikilink")、[木村遼希](../Page/木村遼希.md "wikilink")、[日向滉一](../Page/日向滉一.md "wikilink")、[千秋Lacey](../Page/千秋Lacey.md "wikilink")、[木內梨生奈](../Page/木內梨生奈.md "wikilink")、[一木有海](../Page/一木有海.md "wikilink")、[細川藍](../Page/細川藍.md "wikilink")、[藤本七海](../Page/藤本七海.md "wikilink")、[渡邊Elly](../Page/渡邊Elly.md "wikilink")、[小關裕太](../Page/小關裕太.md "wikilink")、[笠原拓巳](../Page/笠原拓巳.md "wikilink")、[千葉一磨](../Page/千葉一磨.md "wikilink")、[藤田Ryan](../Page/藤田Ryan.md "wikilink")、[細田羅夢](../Page/細田羅夢.md "wikilink")、[川崎樹音](../Page/川崎樹音.md "wikilink")、[加藤Gina](../Page/加藤Gina.md "wikilink")）
      - 作詞：Out of Box、作曲：笹沼直、編曲：浜崎大地
        拍攝外景的地點：埼玉縣立大學、2006年度的節目主題曲。以2種不同的版本播出。（歌詞有所不同,影像部份則相同）
  - ユウキスイッチ(勇氣開關)／[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")・[高橋郁哉](../Page/高橋郁哉.md "wikilink")・[小關裕太](../Page/小關裕太.md "wikilink")・[千葉一磨](../Page/千葉一磨.md "wikilink")
      - 合唱：[高橋郁哉](../Page/高橋郁哉.md "wikilink")／[小關裕太](../Page/小關裕太.md "wikilink")／[千葉一磨](../Page/千葉一磨.md "wikilink")、作詞・作曲：武川行秀、編曲：武川行秀與That's
        on Noise、演奏：That's on Noise
  - 誕生日のうた2006(誕生日之歌2006)／[Barnes勇氣](../Page/Barnes勇氣.md "wikilink")・[篠原愛實](../Page/篠原愛實.md "wikilink")・[木內梨生奈](../Page/木內梨生奈.md "wikilink")・[細田羅夢](../Page/細田羅夢.md "wikilink")・[川崎樹音](../Page/川崎樹音.md "wikilink")
      - 作詞・作曲：武川行秀、編曲：武川行秀與That's on Noise、演奏：That's on Noise
  - Friends\&Dreams(朋友與夢想)／[天TV女子一輪車部](../Page/天TV女子一輪車部.md "wikilink")([加藤Gina](../Page/加藤Gina.md "wikilink")・[伊倉愛美](../Page/伊倉愛美.md "wikilink")・[大木梓彩](../Page/大木梓彩.md "wikilink")・[木內江莉](../Page/木內江莉.md "wikilink")・[細川藍](../Page/細川藍.md "wikilink")・[橋本甜歌](../Page/橋本甜歌.md "wikilink")・[一木有海](../Page/一木有海.md "wikilink"))
      - 作詞：[IZUMI](../Page/IZUMI.md "wikilink")・[KUMI](../Page/KUMI.md "wikilink")、作曲：[RYU](../Page/RYU.md "wikilink")、演奏：[Yum\!Yum\!ORANGE](../Page/Yum!Yum!ORANGE.md "wikilink")

　※這首曲子是由[Yum\!Yum\!ORANGE提供的樂曲所翻唱](../Page/Yum!Yum!ORANGE.md "wikilink")。

  - 冬のアゲハ(冬天的揚羽蝶)／[木內梨生奈](../Page/木內梨生奈.md "wikilink")・[細田羅夢](../Page/細田羅夢.md "wikilink")
      - 作詞・作曲：RYOHEI（[R-21](../Page/R-21.md "wikilink")）

現在全部有95首曲子

## 經典MTK (MTKクラシック)

**經典MTK(MTKクラシック)**是[NHK教育電視台的節目](../Page/NHK教育電視台.md "wikilink")『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』受歡迎的單元,「音樂TV君」應觀眾要求再次播出過去歌曲的單元。由2002年開始持續播出。播出時間為不定期,2005年度遇到沒有現場直播日的月份時,最少會有一天播出「[\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")」。2006年2月開始換成新的片頭影片,由從前的HK學園（畫面比例是4：3）換成介紹到2005年度為止全部90首曲子的影片（畫面比例是16：9）。另外,2006年度還沒有播出過。

### 過去播出過的曲子

#### 2002年度播出

  - 5月6日：君の声がする
  - 5月7日：君の声がする／空になりたい
  - 5月8日：HONEY BEAT
  - 5月9日：HONEY BEAT
  - 6月10日：極楽はどこだ／空になりたい
  - 6月11日：極楽はどこだ
  - 6月12日：YAKINIC GO GO／恋はあせらず
  - 6月13日：YAKINIC GO GO
  - 7月15日：たんぽぽ
  - 7月16日：たんぽぽ／わかってナイ\!
  - 7月17日：わかってない\!／SUPERGIRL
  - 7月18日：SUPERGIRL／www.access.me
  - 8月27日：Angel snow／空になりたい
  - 8月28日：Angel snow／恋は早いもの勝ち
  - 8月29日：恋は早いもの勝ち／運命 '99／走れRUN²\!
  - 9月30日：運命 '99／歩いていこう
  - 10月1日：[\#8cm](../Page/#8cm.md "wikilink")／Can do it\!
  - 10月2日：[\#8cm](../Page/#8cm.md "wikilink")／空にまいあがれ／カーマは気まぐれ
  - 10月3日：空にまいあがれ／世界中の全ての色
  - 11月4日：ゴォ\!／恋はあせらず
  - 11月5日：ゴォ\!／The Longest Time／空になりたい
  - 11月6日：The Longest Time／ハッピーマン／恋はあせらず
  - 11月7日：ハッピーマン
  - 12月9日：JUMP／BIG SMILE BABIES
  - 12月10日：JUMP／Roam～愛があれば～／Be My Friend
  - 12月11日：Roam～愛があれば～／スリラー
  - 12月12日：スリラー／The Longest Time
  - 12月16日：恋のギルティー／YOU ARE MY DREAM／YAKINIC GO GO
  - 12月17日：恋のギルティー／ママ・ミア／ラッキー・ラブ
  - 12月18日：ラッキー・ラブ／Everything you do／愛はきらめきの中に～How deep is your love?～
  - 12月19日：愛はきらめきの中に～How deep is your love?～
  - 12月23日：[\#8cm](../Page/#8cm.md "wikilink")
  - 12月24日：恋はあせらず
  - 12月25日：YAKINIC GO GO
  - 12月26日：You are my dream
  - 12月30日：JUMP
  - 1月6日：極楽はどこだ
  - 1月7日：[\#8cm](../Page/#8cm.md "wikilink")
  - 1月8日：恋はあせらず
  - 1月9日：YAKINIC GO GO
  - 1月13日：YOU ARE MY DREAM
  - 1月15日：ダイナマイト
  - 2月3日：はるかな旅へ
  - 2月5日：おかしな午後
  - 2月6日：I LIKE YOU
  - 2月13日：君のそばにいたい
  - 2月20日：空になりたい
  - 2月25日：空になりたい
  - 2月27日：空になりたい

#### 2003年度播出

  - 1月19日：[\#8cm](../Page/#8cm.md "wikilink")
  - 1月21日：YAKINIC GO GO
  - 1月22日：スリラー
  - 1月26日：ママ・ミア
  - 1月27日：カーマは気まぐれ
  - 1月28日：愛はきらめきの中に
  - 1月29日：Angel Snow
  - 2月2日：わかってナイ\!
  - 2月3日：Everything You Do
  - 2月4日：Twilight
  - 2月5日：君のそばにいたい
  - 2月9日：ルーキー
  - 2月10日：恋にメリーゴーラウンド
  - 2月16日：あこがれ（アコースティック版）

#### 2004年度播出

  - 5月3日：JUMP
  - 5月4日：Can do it\!
  - 5月5日：ママ・ミア
  - 5月6日：極楽はどこだ
  - 6月7日：Roam～愛があれば
  - 6月8日：SUPERGIRL
  - 6月9日：本当は何になりたいんだろう
  - 6月10日：きれいな水
  - 7月12日：あこがれ（アコースティック版）
  - 7月13日：スピリチュアル
  - 7月14日：ぼくらのロック・シティ
  - 7月15日：泣けちゃうの
  - 10月4日：星と月の仲間（モンキー座版）
  - 10月5日：ルーキー（MTK現場演奏版。以途中淡出的手法做結尾）
  - 10月6日：HONEY BEAT（MTK現場演奏01版）
  - 10月7日：空になりたい（以途中淡出的手法做結尾）
  - 12月13日：恋はあせらず
  - 12月14日：はるかな旅へ
  - 12月15日：ラッキー・ラブ
  - 12月16日：元気を出して

#### 2005年度播出

  - 8月10日：ココロ磁石／[\#ヴォヤージュ±](../Page/#ヴォヤージュ±.md "wikilink")
  - 8月17日：モンキーマジック／ホーリー&ブライト
  - 8月29日：明日への叫び
  - 8月30日：[\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")（用字幕方式介紹8月誕生的孩子們）
  - 8月31日：きみのとなりで
  - 1月5日：ココロ磁石
  - 2月27日：ダンシングシスター
  - 2月28日：[\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")（用字幕方式介紹2月誕生的孩子們）
  - 3月1日：きみのとなりで
  - 3月27日：明日への叫び（畫面比例是16：9）
  - 3月28日：[\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")（用字幕方式介紹3月誕生的孩子們）（畫面比例是16：9）

## 相關商品

### CD單曲

  - YOU YOU YOU／タイムマシーンでいこう／[すかんち](../Page/すかんち.md "wikilink")
    (1993/06/21)
  - がんばってダーリン\!／[クリマカーユ](../Page/クリマカーユ.md "wikilink") (1994/09/10)
  - ガンバレ\!アインシュタイン／[ダチョウ倶楽部](../Page/ダチョウ倶楽部.md "wikilink") with
    [TV戰士](../Page/TV戰士.md "wikilink")'95 (1995/09/06)
  - 星を見上げて／[SWITCH](../Page/SWITCH.md "wikilink") (1996/06/26)
  - パリは恋の街／[TT CHARLIE](../Page/TT_CHARLIE.md "wikilink") (1997/05/08)
  - BE ALL RIGHT\!\!／[河相我聞](../Page/河相我聞.md "wikilink") (1997/06/25)
  - BANG BANG BANG／[キャイ～ン](../Page/キャイ～ン.md "wikilink") with
    [TV戰士](../Page/TV戰士.md "wikilink")'97 (1997/12/10)
  - 君にクラクラ／[TV戰士](../Page/TV戰士.md "wikilink")'98 with
    [山崎邦正](../Page/山崎邦正.md "wikilink")&[リサ・ステッグマイヤー](../Page/リサ・ステッグマイヤー.md "wikilink")
    (1998/07/18)
  - 空のたまご／[浜野和子](../Page/浜野和子.md "wikilink")(1999/11/20)（作詞・作曲：清水和彦（[The
    Water Of Life](../Page/The_Water_Of_Life.md "wikilink")））
  - スーパースピードスター／[carnies](../Page/carnies.md "wikilink") (2000/06/21)
  - きらいじゃ★ブギ／[TV戰士](../Page/TV戰士.md "wikilink")'01 (2001/04/21)
  - 夢をつかんで／[渡辺ヒロコ](../Page/渡辺ヒロコ.md "wikilink")
    (2001/07/04)（「その日まで空を見て」のカップリング）

### CD專輯

#### うたの詰め合わせ～天てれ福袋～

**うたの詰め合わせ～天てれ福袋～**（うたのつめあわせ
てんてれふくぶくろ）是1999年6月19日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中1998年度歌曲的CD。

##### 概要

##### 收錄曲

1.  テレビ万歳
2.  恋のギルティー LOVE IN THE FIRST DEGREE
3.  [\#8cm](../Page/#8cm.md "wikilink")
4.  恋はあせらず YOU CAN'T HURRY LOVE
5.  YAKINIC GO GO
6.  YOU ARE MY DREAM
7.  JUMP
8.  極楽はどこだ
9.  空にまいあがれ
10. S・O・S（縮短版）
11. S・O・S（完整版）
12. 恋の天才～ジョンとミケの場合～
13. 恋の天才～ジョンとミケの場合～（for boys）（※）
14. 恋の天才～ジョンとミケの場合～（for girls）（※）
15. 恋のギルティー LOVE IN THE FIRST DEGREE（※）
16. [\#8cm](../Page/#8cm.md "wikilink")（※）
17. 恋はあせらず YOU CAN'T HURRY LOVE（※）
18. YAKINIC GO GO（※）
19. YOU ARE MY DREAM（※）
20. JUMP（※）
21. 極楽はどこだ（※）
22. 空にまいあがれ（※）
23. S・O・S（縮短版）（※）
24. S・O・S（完整版）（※）

（※）卡拉OK版

#### 天てれ大入り袋

**天てれ大入り袋**（てんてれおおいりぶくろ）是2000年6月1日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中1999年度歌曲的CD。

##### 概要

##### 收錄曲

1.  スーパースピードスター（TVサイズ）
2.  Can do it\!
3.  ゴォ\!
4.  恋は早いもの勝ち
5.  スリラーThriller
6.  ハッピーマン
7.  君の声がする
8.  運命’99
9.  はるかな旅へWhere'll we go from now
10. 世界中の全ての色
11. [\#ドキドキのち晴れ](../Page/#ドキドキのち晴れ.md "wikilink")
12. Can do it\! (※）
13. ゴォ\!（※）
14. 恋は早いもの勝ち（※）
15. ハッピーマン（※）
16. 君の声がする（※）
17. 運命’99（※）
18. はるかな旅へWhere'll we go from now（※）
19. 世界中の全ての色（※）
20. [\#ドキドキのち晴れ](../Page/#ドキドキのち晴れ.md "wikilink")（※）

（※）卡拉OK版

#### 天てれ宝船

**天てれ宝船**（てんてれたからぶね）是2001年6月21日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2000年度歌曲的CD。

##### 概要

##### 收錄曲

1.  夢をつかんで（TV版）
2.  Roam～愛があれば～
3.  たんぽぽ
4.  おかしな午後
5.  走れRUN²\!
6.  ラッキー・ラブ I shoud be so lucky
7.  わかってナイ\!
8.  I LIKE YOU
9.  SUPERGIRL
10. Angel snow（TV版）
11. 愛はきらめきの中に How deep is your love?
12. 歩いていこう
13. きらいじゃ★ブギ
14. たんぽぽ（※）
15. おかしな午後（※）
16. 走れRUN²\!（※）
17. わかってナイ\!（※）
18. I LIKE YOU（※）
19. SUPERGIRL（※）
20. 歩いていこう（※）

（※）卡拉OK版

#### 天てれ歌まくら

**天てれ歌まくら**（てんてれうたまくら）是2002年5月18日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2001年度歌曲的CD。

##### 概要

##### 收錄曲

1.  青い星（TV版）
2.  君のそばにいたい I only want to be with you
3.  本当は何になりたいんだろう
4.  元気を出して Gotta Pull MyselfTogether
5.  きれいな水
6.  本日晴天
7.  VACATION
8.  花のランランパワー
9.  BIG SMILE BABIES
10. Jump In The Line
11. Be MY Friend
12. クレマチス
13. Material Girl
14. 空になりたい
15. Dynamite
16. [\#LOVE IS POP](../Page/#LOVE_IS_POP.md "wikilink")（完整版）
17. [\#LOVE IS POP](../Page/#LOVE_IS_POP.md "wikilink")（※）

（※）卡拉OK版

#### 天てれ猫だましぃ

**天てれ猫だましぃ**（てんてれねこだましぃ）是2002年11月21日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2002年度前半歌曲的CD。

##### 概要

##### 收錄曲

1.  青い星
2.  1-2-3
3.  僕であるために
4.  クエスチョン
5.  BYE BYE DAYS
6.  スピリチュアル
7.  STEADY BOYS
8.  星の軌跡
9.  あこがれ
10. LOVE IS POP
11. 1-2-3（※）
12. 僕であるために（※）
13. クエスチョン（※）
14. BYE BYE DAYS（※）
15. スピリチュアル（※）
16. STEADY BOYS（※）
17. 星の軌跡（※）
18. あこがれ（※）
19. LOVE IS POP（※）

（※）卡拉OK版

#### 天てれ上カルビ

**天てれ上カルビ**（てんてれじょうかるび）是2003年6月18日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2002年度後半歌曲的CD。

##### 概要

##### 收錄曲

1.  2003年度オープニング・主題曲
2.  あこがれ ～hip-hop version～
3.  BE HAPPY DAY
4.  恋にメリーゴーラウンド IN FOR A PENNY, IN FOR A POUND
5.  ルーキー
6.  地図
7.  good day（完整版）
8.  あこがれ ～hip-hop version～ (※）
9.  BE HAPPY DAY（※）
10. 恋にメリーゴーラウンド IN FOR A PENNY, IN FOR A POUND（※）
11. ルーキー（※）
12. 地図（※）
13. good day（完整版）（※）
14. 恋人は宇宙人 ～2002年度單元･SF（？）スペース新喜劇｢ラフィン☆スター」的插入歌～（追加曲）

（※）卡拉OK版

#### 天てれビッグバン

**天てれビッグバン**（てんてれびっくばん）是2003年12月17日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2003年度前半歌曲的CD。

##### 概要

##### 收錄曲

1.  2003年度オープニング・主題曲
2.  サンデーモーニング
3.  虹の都へ
4.  泣けちゃうの You caught me out
5.  Go\!Go\!たまご丼（[\#TDD出動](../Page/#TDD.md "wikilink")\!Ver.）
6.  それっきゃないかもね
7.  JUNGLE FUTURE
8.  BAKAはここにいる
9.  good day（完整版）
10. サンデーモーニング（※）
11. 虹の都へ（※）
12. 泣けちゃうの You caught me out（※）
13. Go\!Go\!たまご丼（[\#TDD出動](../Page/#TDD.md "wikilink")\!Ver.）（※）
14. それっきゃないかもね（※）
15. JUNGLE FUUTRE（※）
16. BAKAはここにいる（※）
17. good day（完整版）（※）
18. ダンス×2 for システム☆エラー（追加曲）

（※）卡拉OK版

#### MTK the 8th

**MTK the
8th**（エムティーケーザエイツ）是2004年6月30日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2003年度後半歌曲的CD。

##### 概要

##### 收錄曲

1.  [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（片頭曲版）
2.  旅人は星を数える Days Are Numbers
3.  カンペキ
4.  星と月の仲間
5.  Monkey Magic
6.  水玉
7.  [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（完整版）
8.  旅人は星を数える Days Are Numbers（※）
9.  カンペキ（※）
10. 星と月の仲間（※）
11. Monkey Magic（※）
12. 水玉（※）
13. [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（完整版）（※）

（※）卡拉OK版

#### MTK the 9th

**MTK the
9th**（エムティーケーザナインツ）是2005年2月23日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2004年度歌曲的CD。

##### 概要

##### 收錄曲

1.  [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（片頭曲版）
2.  明日への叫び SHOUT IT OUT LOUD
3.  [\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")
4.  未来船ゴー
5.  MARCH
6.  きみのとなりで
7.  ココロ磁石
8.  ホーリー&ブライト
9.  [\#ヴォヤージュ±](../Page/#ヴォヤージュ±.md "wikilink")
10. Chip Trick
11. Opposite side
12. PARTY IS OPEN
13. [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（完整版）
14. 明日への叫び SHOUT IT OUT LOUD（※）
15. [\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")（※）
16. 未来船ゴー（※）
17. MARCH（※）
18. きみのとなりで（※）
19. ココロ磁石（※）
20. ホーリー&ブライト（※）
21. [\#ヴォヤージュ±](../Page/#ヴォヤージュ±.md "wikilink")（※）
22. PARTY IS OPEN（※）
23. [\#プラズマ回遊](../Page/#プラズマ回遊.md "wikilink")（完整版）（※）

（※）卡拉OK版

#### MTK The BEST I for LOVE

**MTK The BEST I for
LOVE**（エムティーケーザベストワンフォアラブ）是2005年7月27日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中由1998年度到2004年度有關戀愛方面歌曲的精選集
。

##### 概要

##### 收錄曲

1.  恋のギルティー love in the First Degree
2.  元気を出して Gotta Pull Myself Together
3.  恋はあせらず You Can't Hurry Love
4.  ラッキー・ラブ I should be so lucky
5.  恋は早いもの勝ち
6.  サンデーモーニング
7.  あこがれ～acoustic version～
8.  たんぽぽ
9.  Material Girl
10. 泣けちゃうの You caught me out
11. STEADY BOYS
12. Angel snow (single version)
13. 君の声がする
14. それっきゃないかもね
15. 空にまいあがれ
16. BIG SMILE BABIES
17. 運命 '99

#### MTK The BEST II for LIFE

''' MTK The BEST II for LIFE
'''（エムティーケーザベストツーフォアライフ）是2005年7月27日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中由1998年度到2004年度中所精選出歌曲的精選集
。

##### 概要

##### 收錄曲

1.  YAKINIC GO GO
2.  本当は何になりたいんだろう
3.  明日への叫びSHOUT IT OUT LOUD
4.  旅人は星を数えるDays Are Numbers
5.  SUPERGIRL
6.  本日晴天
7.  極楽はどこだ
8.  スピリチュアル
9.  星と月の仲間
10. クエスチョン
11. JUNGLE FUTURE
12. はるかな旅へWhere'll we go from now
13. [\#誕生日のうた](../Page/#誕生日のうた.md "wikilink")
14. きれいな水
15. ココロ磁石
16. MARCH
17. ルーキー

#### MTK the 10th

**MTK the
10th**（エムティーケーザテンツ）是2006年3月1日所發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』的單元「音樂TV君」中2005年度歌曲的CD。

##### 概要

##### 收錄曲

1.  未来はジョウキゲン（片頭曲版）
2.  僕らのハーモニー
3.  さかさまさかさ
4.  ムカつくけれど……スキ\!
5.  僕はドライブ
6.  僕らのハーモニー（現場演唱版）
7.  ダンシング・シスター I'M IN THE MOOD FOR DANCING
8.  未来はジョウキゲン（完整版）
9.  僕らのハーモニー（※）
10. さかさまさかさ（※）
11. ムカつくけれど……スキ\!（※）
12. 僕はドライブ（※）
13. ダンシング・シスター I'M IN THE MOOD FOR DANCING（※）
14. 未来はジョウキゲン（）（※）

（※）卡拉OK版

#### MTK the 11th

是在2007年3月21日發行,集合廣受歡迎的節目『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』2006年度歌曲的CD。

  - 收錄曲

<!-- end list -->

1.  ダンゼン！未来（完整版）
2.  ダンゼン！未来（縮短版）
3.  ダンゼン！未来（片頭曲版）
4.  ユウキスイッチ
5.  誕生日のうた2006
6.  Friends & Dreams（完整版）
7.  Friends & Dreams（縮短版）
8.  冬のアゲハ（完整版）
9.  冬のアゲハ（縮短版）
10. ダンゼン！未来（※）
11. ユウキスイッチ（※）
12. 誕生日のうた2006（※）
13. Friends & Dreams（※）
14. 冬のアゲハ（※）
15. ダッシュマンのうた（追加曲,取自「天TV連續劇・衝刺超人」）
16. ラクラクハッピー（追加曲,取自「天才兒童MAX特別公演　in　ＮＨＫ表演廳」）
17. ユウキスイッチ（追加曲,取自「天才兒童MAX特別公演　in　ＮＨＫ表演廳」）
18. 誕生日のうた2006（追加曲,取自「天才兒童MAX特別公演　in　ＮＨＫ表演廳」）
19. ダンゼン！未来（追加曲,取自「天才兒童MAX特別公演　in　ＮＨＫ表演廳」）

（※）卡拉OK版

## 關聯項目

  - [天才兒童MAX](../Page/天才兒童MAX.md "wikilink")

## 外部連結

  - [天才兒童MAX官方網頁](http://www3.nhk.or.jp/tvkun)

[音樂TV君](../Category/天才兒童MAX.md "wikilink")