**天文学家**是研究[天文学](../Page/天文学.md "wikilink")、[宇宙学](../Page/宇宙学.md "wikilink")、[天体物理学等相关学科的](../Page/天体物理学.md "wikilink")[科学家](../Page/科学家.md "wikilink")。因为有些[哲学家](../Page/哲学家.md "wikilink")、[物理学家](../Page/物理学家.md "wikilink")、[数学家对天文理论有着不可忽视的影响](../Page/数学家.md "wikilink")，所以下面的列表中也包括这些人。

## 现代天文学的研究范围

當代的天文学是以觀測為主之研究學科，部分以[望远镜與各種鏡後特殊接收裝置為接收星光各波長光波並加以處理](../Page/望远镜.md "wikilink")，探視各種性質與變化以研究天體，結合物理學角度研究各天體以至宇宙之演化。

## 著名天文学家（按出生年排列）

### 毕达哥拉斯 Pythagoras

  - [毕达哥拉斯](../Page/毕达哥拉斯.md "wikilink")，前582年至前507年
      - [古希腊](../Page/古希腊.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[数学家](../Page/数学家.md "wikilink")、[音乐理论家](../Page/音乐理论家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：大地是球形的、自转的天体，并围绕这一个中心火球（没说是太阳）旋转。抛弃[地心说](../Page/地心说.md "wikilink")。
      - [三角](../Page/三角.md "wikilink")：[毕达哥拉斯定理或称](../Page/毕达哥拉斯定理.md "wikilink")[勾股定理](../Page/勾股定理.md "wikilink")

### 苏格拉底 Socrates

  - [苏格拉底](../Page/苏格拉底.md "wikilink")，前469年至前399年
      - [古希腊](../Page/古希腊.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")
      - [辩证法](../Page/辩证法.md "wikilink")
      - 被列为[希腊三贤之一](../Page/希腊三贤.md "wikilink")。
      - [柏拉图的老师](../Page/柏拉图.md "wikilink")。

### 柏拉图 Plato

  - [柏拉图](../Page/柏拉图.md "wikilink")，约前427年至约前347年
      - [古希腊](../Page/古希腊.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：宇宙源自超自然造物主，宇宙最初为一片混沌。
      - [天文](../Page/天文.md "wikilink")：[行星沿着圆形轨道围绕地球公转](../Page/行星.md "wikilink")，而地球静止不动。[地心说](../Page/地心说.md "wikilink")。
      - 被列为[希腊三贤之一](../Page/希腊三贤.md "wikilink")。
      - [苏格拉底的徒弟](../Page/苏格拉底.md "wikilink")，[亚里士多德的老师](../Page/亚里士多德.md "wikilink")。

### 歐多克索斯 Eudoxus

  - [尤得塞斯](../Page/尤得塞斯.md "wikilink")，前408年至前355年
      - [古希腊](../Page/古希腊.md "wikilink")**天文学家**、[数学家](../Page/数学家.md "wikilink")、[医学家](../Page/医学家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：用数学模型解释天文理念。天体沿着三十三个固定的球体围绕地球旋转。
      - [柏拉图的徒弟](../Page/柏拉图.md "wikilink")。

### 亚里士多德 Aristotle

  - [亚里士多德](../Page/亚里士多德.md "wikilink")，约前384年至前322年
      - [古希腊](../Page/古希腊.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[科学家](../Page/科学家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：运行的天体是物质实体。天体沿着五十五个球体旋转。
      - [物理](../Page/物理.md "wikilink")：各物体只有在一个不断作用着的推动者直接接触下，才能够保持运动。
      - 被列为[希腊三贤之一](../Page/希腊三贤.md "wikilink")。
      - [苏格拉底的徒孙](../Page/苏格拉底.md "wikilink")，[柏拉图的徒弟](../Page/柏拉图.md "wikilink")。

### 阿里斯塔克斯 Aristarchus

  - [阿里斯塔克斯](../Page/阿里斯塔克斯.md "wikilink")，约前310年至前230年
      - [古希腊](../Page/古希腊.md "wikilink")**天文学家**、[数学家](../Page/数学家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：[日心说](../Page/日心说.md "wikilink")，[地球和其他](../Page/地球.md "wikilink")[行星自转并围绕](../Page/行星.md "wikilink")[太阳公转](../Page/太阳.md "wikilink")。

### 托勒密 Ptolemy

  - [托勒密](../Page/托勒密.md "wikilink")[克劳狄乌斯·托勒密](../Page/克劳狄乌斯·托勒密.md "wikilink")，约90年至168年
      - [古希腊](../Page/古希腊.md "wikilink")**天文学家**、[地理学家](../Page/地理学家.md "wikilink")、[光学家](../Page/光学家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：[地心说](../Page/地心说.md "wikilink")，perfected
        epicycle theory。

### 哥白尼 Copernicus

  - [尼古拉·哥白尼](../Page/尼古拉·哥白尼.md "wikilink")，1473年至1543年
      - [波兰](../Page/波兰.md "wikilink")**天文学家**，现代[天文学创始人](../Page/天文学.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：[日心说](../Page/日心说.md "wikilink")

### 第谷 Tycho Brahe

  - [第谷·布拉赫](../Page/第谷·布拉赫.md "wikilink")，1546年至1601年
      - [丹麦贵族](../Page/丹麦.md "wikilink")，**天文学家**、[占星术士](../Page/占星术士.md "wikilink")、[炼金术士](../Page/炼金术士.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：精确的天文观测记录。
      - [天文](../Page/天文.md "wikilink")：他的助手[开普勒尝试让他接受](../Page/开普勒.md "wikilink")[日心说](../Page/日心说.md "wikilink")，但不成功。
      - [历法](../Page/历法.md "wikilink")：[清朝](../Page/清朝.md "wikilink")[时宪历的主要依据就是第谷的天文观测结果](../Page/时宪历.md "wikilink")。、

### 开普勒 Kepler

  - [约翰内斯·开普勒](../Page/约翰内斯·开普勒.md "wikilink")，1571年至1630年
      - [德国](../Page/德国.md "wikilink")**天文学家**、[数学家](../Page/数学家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：提出[开普勒定律](../Page/开普勒定律.md "wikilink")

### 伽利略 Galileo

  - [伽利略·伽利莱](../Page/伽利略·伽利莱.md "wikilink")，1564年至1642年
      - [意大利](../Page/意大利.md "wikilink")[比萨城人](../Page/比萨.md "wikilink")。[物理学家](../Page/物理学家.md "wikilink")、**天文学家**，近代实验科学的奠基者之一。
      - [天文](../Page/天文.md "wikilink")：他最早使用望远镜观测天体来支持[哥白尼的](../Page/哥白尼.md "wikilink")[日心说](../Page/日心说.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：发现[木星的](../Page/木星.md "wikilink")[卫星](../Page/卫星.md "wikilink")，sunspots，phases
        of Venus

### 牛顿 Newton

  - [艾萨克·牛顿](../Page/艾萨克·牛顿.md "wikilink")，1643年至1727年
      - [英国](../Page/英国.md "wikilink")[数学家](../Page/数学家.md "wikilink")、[科学家](../Page/科学家.md "wikilink")、[哲学家](../Page/哲学家.md "wikilink")、[炼金术热衷者](../Page/炼金术.md "wikilink")
      - [经典力学](../Page/经典力学.md "wikilink")：[万有引力定律](../Page/万有引力定律.md "wikilink")、[牛顿运动定律](../Page/牛顿运动定律.md "wikilink")
      - 发明：制作了世界第一架[反射望远镜](../Page/反射望远镜.md "wikilink")

### 赫歇尔 Herschel

  - [弗里德里希·威廉·赫歇尔](../Page/弗里德里希·威廉·赫歇尔.md "wikilink")，1738年至1822年
      - [英国](../Page/英国.md "wikilink")**天文学家**、[音乐家](../Page/音乐家.md "wikilink")
      - [天文](../Page/天文.md "wikilink")：观测[星云](../Page/星云.md "wikilink")；发现[天王星](../Page/天王星.md "wikilink")

### 其他

  - [阿尔伯特·爱因斯坦](../Page/阿尔伯特·爱因斯坦.md "wikilink")
  - [亞歷山大·弗里德曼](../Page/亞歷山大·弗里德曼.md "wikilink")
  - [乔治·勒梅特](../Page/乔治·勒梅特.md "wikilink")
  - [愛德文·哈勃](../Page/愛德文·哈勃.md "wikilink")
  - [威廉·德西特](../Page/威廉·德西特.md "wikilink")
  - [卡爾·薩根](../Page/卡爾·薩根.md "wikilink")
  - [史蒂芬·霍金](../Page/史蒂芬·霍金.md "wikilink")
  - [德谟克利特](../Page/德谟克利特.md "wikilink")
  - [巴门尼德](../Page/巴门尼德.md "wikilink")
  - [张衡](../Page/张衡.md "wikilink")
  - [一行](../Page/一行.md "wikilink")
  - [祖冲之](../Page/祖冲之.md "wikilink")
  - [汤若望](../Page/汤若望.md "wikilink")

## 参考文献

  - 著：《*The Big Bang*》第3版

## 参见

  - [天文学家列表](../Page/天文学家列表.md "wikilink")
  - [天文学](../Page/天文学.md "wikilink")
  - [宇宙学](../Page/宇宙学.md "wikilink")
  - [天体物理学](../Page/天体物理学.md "wikilink")
  - [诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")

{{-}}

[天文学家](../Category/天文学家.md "wikilink")