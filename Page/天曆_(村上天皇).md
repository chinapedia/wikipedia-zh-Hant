**天曆**（947年四月廿二至957年十月廿七）是[日本的](../Page/日本.md "wikilink")[年號](../Page/年號.md "wikilink")。使用這年號之[日本天皇是](../Page/日本天皇.md "wikilink")[村上天皇](../Page/村上天皇.md "wikilink")。

## 改元

  - 天慶十年四月廿二（947年5月15日） ：改元。
  - 天曆十一年十月廿七（957年11月21日） ：改元[天德](../Page/天德_\(村上天皇\).md "wikilink")。

## 出處

《[論語](../Page/論語.md "wikilink")·堯曰》：“**天**之**曆**数在尔躬”

## 大事記

## 出生

  - 天曆2年——[源賴光](../Page/源賴光.md "wikilink")，[平安時代中期大將](../Page/平安時代.md "wikilink")，[攝津源氏始祖](../Page/攝津源氏.md "wikilink")。

## 逝世

## 紀年、干支、西曆對照表

| 天曆                             | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 947年                           | 948年                           | 949年                           | 950年                           | 951年                           | 952年                           | 953年                           | 954年                           | 955年                           | 956年                           |
| [干支](../Page/干支.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") |
| 天曆                             | 十一年                            |                                |                                |                                |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元.md "wikilink") | 957年                           |                                |                                |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |                                |                                |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 其他使用[天曆年號的政權](../Page/天曆.md "wikilink")
  - 同期存在的其他政權之紀年
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（947年）：[後漢](../Page/後漢.md "wikilink")—[劉知遠之年號](../Page/劉知遠.md "wikilink")
      - [乾祐](../Page/乾祐_\(劉知遠\).md "wikilink")（948年至956年）：後漢—劉知遠、[劉承祐](../Page/劉承祐.md "wikilink")，[北漢](../Page/北漢.md "wikilink")—[劉旻](../Page/劉旻.md "wikilink")、[劉鈞之年號](../Page/劉鈞.md "wikilink")
      - [天會](../Page/天會_\(劉鈞\).md "wikilink")（957年至973年）：北漢—劉鈞、劉繼恩、劉繼元之年號
      - [廣順](../Page/廣順.md "wikilink")（951年至953年）：[後周](../Page/後周.md "wikilink")—[郭威之年號](../Page/郭威.md "wikilink")
      - [顯德](../Page/顯德.md "wikilink")（954年至960年正月）：後周—郭威、柴榮、柴宗訓之年號
      - [保大](../Page/保大_\(李璟\).md "wikilink")（943年三月至957年）：[南唐](../Page/南唐.md "wikilink")—[李璟之年號](../Page/李璟.md "wikilink")
      - [乾和](../Page/乾和.md "wikilink")（943年十一月至958年七月）：[南漢](../Page/南漢.md "wikilink")—[劉晟之年號](../Page/劉晟.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：[後蜀](../Page/後蜀.md "wikilink")—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [至治](../Page/至治_\(段思良\).md "wikilink")（946年至951年）：[大理國](../Page/大理國.md "wikilink")—[段思良之年號](../Page/段思良.md "wikilink")
      - [明德](../Page/明德_\(段思聰\).md "wikilink")（952年起）：大理國—[段思聰之年號](../Page/段思聰.md "wikilink")
      - [廣德](../Page/廣德_\(段思聰\).md "wikilink")（至967年）：大理國—段思聰之年號
      - [大同](../Page/大同_\(辽太宗\).md "wikilink")（947年二月至九月）：[遼](../Page/遼朝.md "wikilink")—[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [天祿](../Page/天祿_\(遼世宗\).md "wikilink")（947年九月至951年九月）：遼—[遼世宗耶律阮之年號](../Page/遼世宗.md "wikilink")
      - [應曆](../Page/應曆.md "wikilink")（951年九月至969年二月）：遼—[遼穆宗耶律璟之年號](../Page/遼穆宗.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:10世纪日本年号](../Category/10世纪日本年号.md "wikilink")
[Category:940年代日本](../Category/940年代日本.md "wikilink")
[Category:950年代日本](../Category/950年代日本.md "wikilink")