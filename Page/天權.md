{{ Starbox catalog | names=Megrez, 69 Ursae Majoris,
[HR](../Page/Star_catalogue#BS,_BSC,_HR.md "wikilink") 4660,
[HD](../Page/Henry_Draper_Catalogue.md "wikilink") 106591,
[BD](../Page/Star_catalogue##BD.2FCD.2FCPD.md "wikilink")+57 1363,
FK5 456,
[HIP](../Page/Hipparcos_catalogue.md "wikilink") 59774,
[SAO](../Page/Star_catalogue#SAO.md "wikilink") 28315,
GC 16736, CCDM 12155+5702 }}

**天權**（大熊座δ）是在[大熊座內的一顆恆星](../Page/大熊座.md "wikilink")，它在西方的固有的名稱是**Megrez**，源自
*al-maghriz*。"熊尾巴的根部"和*Kaffa*。

天權的[視星等是](../Page/視星等.md "wikilink")+3.32，在[北斗七星中是最暗的一顆](../Page/北斗七星.md "wikilink")，距離地球81[光年](../Page/光年.md "wikilink")，它的[光譜分類是A](../Page/光譜類型.md "wikilink")3V，是顆藍白色的[主序星](../Page/主序星.md "wikilink")。它有兩顆暗淡的伴星，11等的大熊座δB距離是190[弧秒](../Page/弧秒.md "wikilink")，10等的大熊座δC相距186[弧秒](../Page/弧秒.md "wikilink")。

它也是[大熊座移動星群的成員之一](../Page/大熊座移動星群.md "wikilink")。唐代密教教典《佛說北斗七星延命經》中則稱天權為「**文曲**」，原文如下：「南無文曲星。是東方無憂世界最勝吉祥如來佛。」

[Ursa_Major2.jpg](https://zh.wikipedia.org/wiki/File:Ursa_Major2.jpg "fig:Ursa_Major2.jpg")

## 參考資料

[大熊座Delta](../Category/拜耳天體.md "wikilink")
[U](../Category/大熊座.md "wikilink")
[G](../Category/三合星.md "wikilink")
[Category:A-型主序星](../Category/A-型主序星.md "wikilink")
[Category:有固有名的恆星](../Category/有固有名的恆星.md "wikilink")
[G](../Category/紫微垣.md "wikilink") [G](../Category/北斗七星.md "wikilink")
[U](../Category/北斗_\(星官\).md "wikilink")
[Category:大熊座移動星群](../Category/大熊座移動星群.md "wikilink")