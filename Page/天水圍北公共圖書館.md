[Tin_Shui_Wai_North_Public_Library.JPG](https://zh.wikipedia.org/wiki/File:Tin_Shui_Wai_North_Public_Library.JPG "fig:Tin_Shui_Wai_North_Public_Library.JPG")
**天水圍北公共圖書館**（[英文](../Page/英文.md "wikilink")：**Tin Shui Wai North Public
Library**），是[香港公共圖書館](../Page/香港公共圖書館.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[天水圍新市鎮第](../Page/天水圍新市鎮.md "wikilink")2間[公共圖書館](../Page/公共圖書館.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[天水圍新市鎮](../Page/天水圍新市鎮.md "wikilink")[天澤邨](../Page/天澤邨.md "wikilink")[天澤商場](../Page/天澤商場.md "wikilink")313號鋪位，為繼[天水圍公共圖書館後](../Page/天水圍公共圖書館.md "wikilink")[天水圍的第二個圖書館](../Page/天水圍.md "wikilink")，於2006年12月20日正式開幕，屬於[小型圖書館規模](../Page/小型圖書館.md "wikilink")，由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。

## 歷史

天水圍北公共圖書館主要是紓緩天水圍北居民對圖書館服務的需求，由於原有[天水圍公共圖書館服務不少天水圍居民](../Page/天水圍公共圖書館.md "wikilink")，使有關[資源經常超出原有負荷](../Page/資源.md "wikilink")，加上是位於[天福路和](../Page/天福路.md "wikilink")[屏廈路交界](../Page/屏廈路.md "wikilink")（近[西鐵綫](../Page/西鐵綫.md "wikilink")[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")）的[屏山天水圍文化康樂大樓高座全座的](../Page/屏山天水圍文化康樂大樓.md "wikilink")[屏山天水圍公共圖書館遲遲未能落成](../Page/屏山天水圍公共圖書館.md "wikilink")。為解決天水圍北居民的需求，[康樂及文化事務署決定於](../Page/康樂及文化事務署.md "wikilink")[天澤邨開始這一個小型圖書館](../Page/天澤邨.md "wikilink")，以便暫時紓緩為區內對圖書館服務的需求。

## 設施及服務

天水圍北公共圖書館的設施包括：

  - 成人圖書館
  - 兒童圖書館
  - 推廣活動室
  - 互聯網／電子資源工作站
  - 報刊閱覽部
  - 聯機公眾檢索目錄
  - 自助借書機

天水圍北公共圖書館所提供的服務則包括：

  - 集體借閱服務
  - 還書箱服務
  - 兒童多媒體資料服務
  - 互聯網／電子資源服務
  - 借閱服務
  - 報刊閱覽服務
  - 聯機公眾檢索目錄
  - 推廣活動
  - 讀者諮詢服務
  - 預約圖書館資料服務
  - 自助借書服務

## 開放時間

  - 星期二至星期五：09:00-20:00
  - 星期六：09:00-17:00
  - 星期日及公眾假期：09:00-13:00
  - 星期一、[元旦日](../Page/元旦日.md "wikilink")、農曆[年初一至](../Page/年初一.md "wikilink")[年初三](../Page/年初三.md "wikilink")
    、[耶穌受難日](../Page/耶穌受難日.md "wikilink")
    、[聖誕節及](../Page/聖誕節.md "wikilink")[聖誕節翌日](../Page/聖誕節翌日.md "wikilink")：全日休息

## 地址

[天澤邨](../Page/天澤邨.md "wikilink")[天澤商場](../Page/天澤商場.md "wikilink")313號鋪位

## 途經的公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港輕鐵.md" title="wikilink">輕鐵</a></dt>

</dl>
<ul>
<li><a href="../Page/天逸站.md" title="wikilink">天逸站</a>：<a href="../Page/香港輕鐵705綫.md" title="wikilink">705綫</a>、<a href="../Page/香港輕鐵706綫.md" title="wikilink">706綫</a>、<a href="../Page/香港輕鐵751綫.md" title="wikilink">751綫</a>、<a href="../Page/香港輕鐵751P綫.md" title="wikilink">751P綫</a>、<a href="../Page/香港輕鐵761P綫.md" title="wikilink">761P綫</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參見

  - [康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")
  - [香港公共圖書館](../Page/香港公共圖書館.md "wikilink")

## 外部連結

  - [香港公共圖書館](http://www.hkpl.gov.hk/)
  - [香港公共圖書館 -
    天水圍北公共圖書館](https://web.archive.org/web/20070110223418/http://www.hkpl.gov.hk/tc_chi/locat_hour/locat_hour_ll/locat_hour_ll_ntr/library_81.html)

[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:香港公共圖書館](../Category/香港公共圖書館.md "wikilink")