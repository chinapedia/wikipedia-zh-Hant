**天燕座**是南天星座之一，在[南三角座之南](../Page/南三角座.md "wikilink")，紧接[南极座](../Page/南极座.md "wikilink")。

天燕座沒有神話，由[Poeter Dirkszoon
Keyser和](../Page/Poeter_Dirkszoon_Keyser.md "wikilink")[Frederick de
Houtman所創](../Page/Frederick_de_Houtman.md "wikilink")，為的是紀念新畿內亞的[極樂鳥](../Page/極樂鳥.md "wikilink")。

## 深空天体

  - [NGC5612](../Page/NGC5612.md "wikilink")
  - [NGC5799](../Page/NGC5799.md "wikilink")
  - [NGC5833](../Page/NGC5833.md "wikilink")
  - [NGC5967](../Page/NGC5967.md "wikilink")
  - [NGC6101](../Page/NGC6101.md "wikilink")——这是一个相当典型的[球状星团](../Page/球状星团.md "wikilink")，比一般球状星团略暗，结构疏松。距离地球约为5万光年。具体位置：赤经16时25分，赤纬-72°。
  - [NGC6151](../Page/NGC6151.md "wikilink")
  - [NGC6209](../Page/NGC6209.md "wikilink")
  - [NGC6392](../Page/NGC6392.md "wikilink")

## 重要主星

|      |      |      |      |           |
| ---- | ---- | ---- | ---- | --------- |
| 西方星名 | 中國星官 | 英文名字 | 英文含意 | 視星等       |
| 天燕座α | 異雀八  | \-   | \-   | 3.83      |
| 天燕座β | 異雀三  | \-   | \-   | 4.23      |
| 天燕座γ | 異雀四  | \-   | \-   | 3.86      |
| 天燕座δ | 異雀六  | \-   | \-   | 4.68+5.27 |
| 天燕座ε | 異雀九  | \-   | \-   | 5.06      |
| 天燕座ζ | 異雀一  | \-   | \-   | 4.79      |
| 天燕座η | 異雀七  | \-   | \-   | 4.89      |
| 天燕座ι | 異雀二  | \-   | \-   | 5.39      |

  - 異雀Exotic Bird（極9）

[Tian1](../Category/星座.md "wikilink")
[Category:天燕座](../Category/天燕座.md "wikilink")