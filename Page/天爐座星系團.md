**天爐座星系團**距離[銀河系約](../Page/銀河系.md "wikilink")6千萬光年遠處\[1\]，

它雖遠比[室女座星系團小許多](../Page/室女座星系團.md "wikilink")，但仍是在1億光年範圍內排名第二的富星系團。他主要的部份位於[天爐座](../Page/天爐座.md "wikilink")，並且可能與作為星系團還太小的[波江座星系群結合在一起](../Page/波江座星系群.md "wikilink")，因此**天爐座星系團**或許可提供星系集團發展的訊息，顯示小群的合併在主集團中的作用。\[2\]，

反過來也可以提供伴生的超星系團結構的訊息。\[3\]

## 相關條目

  - [室女座星系團](../Page/室女座星系團.md "wikilink")
  - [后髮座星系團](../Page/后髮座星系團.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

[天爐座星系團](../Category/天爐座星系團.md "wikilink")
[Category:星系團](../Category/星系團.md "wikilink")
[Category:天爐座](../Category/天爐座.md "wikilink")
[S0373](../Category/阿貝爾天體.md "wikilink")

1.
2.
3.