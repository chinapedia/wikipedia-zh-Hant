是一部[香港電影](../Page/香港電影.md "wikilink")，2006年3月2日在[香港上映](../Page/香港.md "wikilink")。由[楊千嬅及](../Page/楊千嬅.md "wikilink")[任賢齊領銜主演](../Page/任賢齊.md "wikilink")，由[羅永昌擔任](../Page/羅永昌.md "wikilink")[導演](../Page/導演.md "wikilink")。電影以[乳癌為主題](../Page/乳癌.md "wikilink")，故事改編自香港女作家[西西的著名小說](../Page/西西.md "wikilink")《[哀悼乳房](../Page/哀悼乳房.md "wikilink")》。

## 劇情

冰（楊千嬅 飾）是廣告公司的客戶經理，公司她是女強人，下班後她跟很多女人一樣，苦苦尋覓自己的愛情。 一天晚上她在酒吧遇到了心理醫生V仔（任賢齊
飾），兩個人纏綿的時候V仔意外發現了冰的胸部有腫塊。 經醫生診斷證實冰真的患上了乳癌，冰大受打擊，懼怕自己因為失去了一邊乳房而沒有好歸宿。
自從因為V仔發現自己的病情後，冰就對V仔存在了怨恨，可是V仔也因為她留下了陰影，因此不舉。
冰差點被朋友騙財，幸好有V仔及時解圍，之後二人便互相扶持幫助，情愫暗生。

## 演員

  - [任賢齊](../Page/任賢齊.md "wikilink")：V仔
  - [楊千嬅](../Page/楊千嬅.md "wikilink")：梁冰傲
  - [郭涛](../Page/郭涛_\(演员\).md "wikilink")
  - [谷祖琳](../Page/谷祖琳.md "wikilink")
  - [鄔玉君](../Page/鄔玉君.md "wikilink")
  - [賈思樂](../Page/賈思樂.md "wikilink")
  - [側田](../Page/側田.md "wikilink")
  - [許紹雄](../Page/許紹雄.md "wikilink")
  - [郭涛](../Page/郭涛_\(演员\).md "wikilink")
  - [岑寶兒](../Page/岑寶兒.md "wikilink")
  - [邵美琪](../Page/邵美琪.md "wikilink")
  - [秦煌](../Page/秦煌.md "wikilink")
  - [艾威](../Page/艾威.md "wikilink")
  - [李麗麗](../Page/李麗麗.md "wikilink")
  - [郭少芸](../Page/郭少芸.md "wikilink")
  - [林家棟](../Page/林家棟.md "wikilink")
  - [黃文慧](../Page/黃文慧.md "wikilink")
  - [馮克安](../Page/馮克安.md "wikilink")
  - [林雪](../Page/林雪.md "wikilink")
  - [張兆輝](../Page/張兆輝.md "wikilink")
  - [龔慈恩](../Page/龔慈恩.md "wikilink")
  - 周家怡

## 外部連結

  -
  - {{@movies|fbhk42546231|天生一對}}

  -
  -
  -
  -
  -
  -
  - [風雪中的故居／《天生一對》：現代女性的美麗與哀愁](http://hegu.mysinablog.com/index.php?op=ViewArticle&articleId=2609825)

[6](../Category/2000年代香港電影作品.md "wikilink")
[Category:2000年代爱情片](../Category/2000年代爱情片.md "wikilink")
[Category:香港小说改编电影](../Category/香港小说改编电影.md "wikilink")
[Category:香港爱情片](../Category/香港爱情片.md "wikilink")
[Category:癌症題材電影](../Category/癌症題材電影.md "wikilink")
[Category:寰亞電影](../Category/寰亞電影.md "wikilink")
[Category:杜琪峰電影](../Category/杜琪峰電影.md "wikilink")