[Tin_Shing_Court.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Court.jpg "fig:Tin_Shing_Court.jpg")
[Tin_Shing_Court_Pond_201606.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Court_Pond_201606.jpg "fig:Tin_Shing_Court_Pond_201606.jpg")
[Tin_Shing_Court_Amphitheater_2016.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Court_Amphitheater_2016.jpg "fig:Tin_Shing_Court_Amphitheater_2016.jpg")
[Tin_Shing_Court_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Court_Covered_Walkway.jpg "fig:Tin_Shing_Court_Covered_Walkway.jpg")
**天盛苑**（）是[香港房屋委員會建成的居屋屋苑](../Page/香港房屋委員會.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[天水圍新市鎮南部的天水圍市鎮地段](../Page/天水圍新市鎮.md "wikilink")17號，地址為天靖街3號，鄰近[港鐵](../Page/港鐵.md "wikilink")[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")，是天水圍區內與港鐵站距離最近的居屋屋苑，目前由[啟勝物業管理有限公司負責屋苑物業管理](../Page/啟勝物業管理有限公司.md "wikilink")。屋苑分3期出售，建有4座和諧一型，12座康和一型及1座康和二型住宅大廈，共有17座住宅樓宇，於1999年落成，共有6,580個單位，是全港提供最多單位的居屋屋苑，首次推出時售價介乎423,200至1,285,400港元。天盛苑亦是全港佔地範圍最廣的居屋屋苑。

## 屋苑資料

天盛苑內設有多項設施，包括可容納1,316輛私家車及84輛電單車的多層停車場、樓高2層的天盛商場、露天劇場、魚池、噴水池、流水盆景、花園、涼亭、園藝區、籃球場、網球場、羽毛球場、乒乓球枱、兒童遊樂場等。

屋苑內的服務設施大樓（天盛商場）樓高2層。地面層是冷氣街市、茶餐廳、外賣店、特色小食店和便利店等商舖，一樓主要是酒樓、西餐廳、中菜館、琴行、服裝店以及其他商店。附翼還設有兩所幼稚園。

因此天盛苑是公認天水圍區內，居住環境最佳的居屋屋苑。

天盛苑（包括住宅大廈）屬於私人地方，保安嚴密，閒人一概不得擅進，住戶進入大廈時須使用智能住戶卡或密碼。管理處亦設立新規例，除本苑住戶和單位訪客外，閒雜人等均不得進入屋苑範圍。

天盛苑是全港提供最多單位及佔地範圍最廣的居屋屋苑，

Tin Shing Court Basketball Court.jpg|籃球場 Tin Shing Court Football
Field.jpg|足球場 Tin Shing Court Badminton Court.jpg|羽毛球場 Tin Shing Court
Tennis Court.jpg|網球場 Tin Shing Court Volleyball Court.jpg|排球場 Tin Shing
Court Pebble Walking Trail and Gym Zone.jpg|卵石路步行徑及健體區

Tin Shing Court Playground.jpg|兒童遊樂場 Tin Shing Court Playground
(2).jpg|兒童遊樂場（2） Tin Shing Court Playground (3).jpg|兒童遊樂場（3） Tin Shing
Court Playground (4).jpg|兒童遊樂場（4） Tin Shing Court Playground
(5).jpg|兒童遊樂場（5） Tin Shing Court Pavilion.jpg|涼亭

### 樓宇

| 座別/樓宇名稱 | 樓宇類型                                | 落成年份\[1\] | 備註\[2\] |
| ------- | ----------------------------------- | --------- | ------- |
| A座/盛彩閣  | [和諧一型](../Page/和諧式大廈.md "wikilink") | 1999      | 不適用     |
| B座/盛昭閣  |                                     |           |         |
| C座/盛頤閣  | 2000                                |           |         |
| D座/盛賢閣  |                                     |           |         |
| E座/盛勤閣  | [康和一型](../Page/康和型大廈.md "wikilink") | 不適用       |         |
| F座/盛志閣  |                                     |           |         |
| G座/盛匯閣  | [康和二型](../Page/康和型大廈.md "wikilink") | 不適用       |         |
| H座/盛謙閣  | [康和一型](../Page/康和一型.md "wikilink")  | 不適用       |         |
| J座/盛悅閣  |                                     |           |         |
| K座/盛鼎閣  | [康和一型](../Page/康和一型.md "wikilink")  |           |         |
| L座/盛亨閣  |                                     |           |         |
| M座/盛旭閣  |                                     |           |         |
| N座/盛珍閣  |                                     |           |         |
| O座/盛泉閣  | [康和一型](../Page/康和一型.md "wikilink")  |           |         |
| P座/盛譽閣  |                                     |           |         |
| Q座/盛坤閣  |                                     |           |         |
| R座/盛麗閣  |                                     |           |         |
|         |                                     |           |         |

### 建築詳情

天盛苑第一期（包括B、C及D座）B、C及D座由房屋署總建築師(1)設計；B座由[新昌營造承建](../Page/新昌營造.md "wikilink")，C及D座由[中國海外房屋工程有限公司承建](../Page/中國海外房屋工程有限公司.md "wikilink")。電梯方面，B座由[迅達升降機（香港）有限公司提供](../Page/迅達集團.md "wikilink")，其他使用LG產品。

第二期（包括E、F、H、J、K、L、M、N及O座）E及F座由房屋署總建築師（1）設計，H、J、K、L、M、N及O座由[王歐陽（香港）有限公司設計](../Page/王歐陽_\(香港\).md "wikilink")；E及F座由[中國海外房屋工程有限公司承建](../Page/中國海外房屋工程有限公司.md "wikilink")，H、J、K及L座由[西松匯成建築有限公司承建](../Page/西松建設.md "wikilink")，M、N及O座由[新昌營造承建](../Page/新昌營造.md "wikilink")；其中，H-L座於1999年9月27日完工，而其他大廈則早一個月竣工。電梯方面，E至G座由[LG提供](../Page/LG.md "wikilink")，其他使用[英國通用電氣-捷運產品](../Page/英國通用電氣.md "wikilink")。

第三期（包括A、G、P、Q、R座）A、Q及R座由關善明建築師事務所設計，G座由房屋署總建築師(1)設計，P座由[王歐陽（香港）有限公司設計](../Page/王歐陽_\(香港\).md "wikilink")；整個第三期均由[新昌營造承建](../Page/新昌營造.md "wikilink")，並於1999年9月完工。電梯方面，康和式大廈由英國通用電氣-捷運提供，而屬和諧式的A座則使用迅達產品。

## 天盛商場

[Tin_Shing_Shopping_Centre_201606.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Shopping_Centre_201606.jpg "fig:Tin_Shing_Shopping_Centre_201606.jpg")
[Tin_Shing_Market_Interior_2016.jpg](https://zh.wikipedia.org/wiki/File:Tin_Shing_Market_Interior_2016.jpg "fig:Tin_Shing_Market_Interior_2016.jpg")及[西班牙火腿等特色食物](../Page/西班牙火腿.md "wikilink")\]\]
天盛商場位於天盛服務設施大樓中，大樓設行人天橋連接[天耀邨](../Page/天耀邨.md "wikilink")。商場初期由[香港房屋委員會持有](../Page/香港房屋委員會.md "wikilink")，在2005年出售予[領展後由](../Page/領展.md "wikilink")[領展管理](../Page/領展.md "wikilink")。商場初期在地下設有「天盛（萬有）街市」，由業主外判予[宏安集團管理](../Page/宏安集團.md "wikilink")。商場內其他主要商戶包括[百佳超級市場](../Page/百佳.md "wikilink")、[萬寧](../Page/萬寧.md "wikilink")、[日本城和](../Page/日本城.md "wikilink")[施培記凍肉超級市場等](../Page/施培記凍肉超級市場.md "wikilink")。

到2014年，天盛商場及街市範圍封閉並進行大規模翻新。其中天盛街市已於2015年聖誕並在2016年農曆新年前後，分兩階段投入服務。翻新後的街市位於地下全層，面積由8000呎增至1.7萬呎，舖位由約77個增至逾110個，走高檔路線。建築風格參照歐陸市集，其中「SOHO區」售賣西班牙[火腿等各地特色食物](../Page/火腿.md "wikilink")，又設類似日本築地市場的海鮮街，賣凍[蟹](../Page/蟹.md "wikilink")、[和牛](../Page/和牛.md "wikilink")、[吉品鮑](../Page/吉品鮑.md "wikilink")，以及提供熟食的「食深宵立食街」，方便跨區工作市民晚上可吃[夜宵](../Page/夜宵.md "wikilink")。街市設有冷氣，並提供免費[Wi-Fi服務](../Page/Wi-Fi.md "wikilink")。

領展稱投入數千萬元翻新街市，加租是必然，但未透露加幅，又稱租金低未必代表物價低。領展企業傳訊及對外關係總監盧炳松表示，天耀和天盛街市只是相隔一條馬路，而且天耀商場仍保留乾貨店及超市，相信影響不大。有天水圍居民組織成員認為，新街市的高檔次產品「離地」，未必符合居民需要，又稱「貴租就會一定會加價」。\[3\]

<File:Tin> Shing Shopping Centre Atrium
2011.jpg|2011年翻新前的天盛商場中庭，2016年翻新後已消失
<File:Tin> Shing Market.JPG|2015年初的天盛臨時街市（其餘部份翻新中）
[File:TSmarket.jpg|2015年底翻新後的天盛街市入口](File:TSmarket.jpg%7C2015年底翻新後的天盛街市入口)
<File:Tin> Shing Market Interior1 2016.jpg|街市內貌 <File:Tin> Shing Market
Japanse Food Kiosk 2016.jpg|位於「SOHO區」的日本築地市場的海鮮街

## 附近的教育設施

### 幼稚園

  - [幼聯主辦安泰幼兒學校](http://www.ceces-aetna.edu.hk)（2000年創辦）（位於天盛商場附翼地下）
  - [基督教聖約教會小天使（天盛）幼稚園](http://www.mcc.org.hk/mccts/v2/intro.html)（2000年創辦）（位於天盛商場附翼一樓）
  - [香港中文大學校友會聯會順龍仁澤幼稚園](http://www.slyck.edu.hk/?flag=1)（2015年創辦）（位於天盛苑服務設施大樓2樓）
  - [仁濟醫院林李婉冰幼稚園](http://YCHNLKG.EDU.HK)（1992年創辦）（位於天河路6號仁濟醫院第24屆董事局社會服務中心地下）
  - [天主教聖葉理諾幼稚園](http://www.stjeromesckg.edu.hk)（2001年創辦）（位於天美街6號2樓及3樓）

### 小學

  - [伊利沙伯中學舊生會小學分校](http://www.bps.qesosa.edu.hk)（1992年創辦）
  - [獅子會何德心小學](http://www.htsps.edu.hk)（1999年創辦）

### 中學

  - [天主教培聖中學](../Page/天主教培聖中學.md "wikilink")（1963年創辦）
  - [元朗公立中學校友會鄧兆棠中學](../Page/元朗公立中學校友會鄧兆棠中學.md "wikilink")（1999年創辦）

## 問題

### 崩牆塌石屎

2015年11月30日晚上7時許，由[中國海外房屋工程有限公司所承建的天水圍天盛苑盛勤閣](../Page/中國海外房屋工程有限公司.md "wikilink")（E座）13樓一單位，當業主正與兒子在客廳貼近牆邊的餐桌吃晚飯時，突然牆身傳來異響，幾秒後即感到有硬物墮下，擊中其腳部。她查看下赫見牆身近地板位置出現崩裂，面積約5呎乘2呎，有石屎塌下，露出鋼筋。\[4\]戶主稱經工程師檢驗後，透露懷疑主力牆有空心部份，可能要剷去再整。\[5\]

## 區議會議席分佈

天盛苑原為一個選區，但2018年[選管會提出將天盛苑A](../Page/選管會.md "wikilink")、B、C、D及G座劃入新設的M17（盛欣）選區；其餘E至Q座則屬M18（天盛）選區。屆時同一屋苑將有兩個當區區議員。有地區人士認為會出現協調問題，同時將政黨鬥爭進入屋苑而出現混亂。現任當區區議員陳思靜亦對選管會的安排感到不滿。\[6\]

## 鄰近

  - [嘉湖山莊](../Page/嘉湖山莊.md "wikilink")[樂湖居](../Page/嘉湖山莊#樂湖居（第1期）.md "wikilink")
  - [嘉湖新北江商場](../Page/嘉湖新北江商場.md "wikilink")
  - [天耀邨](../Page/天耀邨.md "wikilink")
  - [天水圍警署](../Page/天水圍警署.md "wikilink")、[天水圍消防局](../Page/天水圍消防局.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")、[輕鐵](../Page/香港輕鐵.md "wikilink")[天水圍站及](../Page/天水圍站_\(輕鐵\).md "wikilink")[天耀站](../Page/天耀站.md "wikilink")
  - [屏欣苑](../Page/屏欣苑.md "wikilink")

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")(B出口)
  - [輕鐵](../Page/香港輕鐵.md "wikilink")：[天耀站](../Page/天耀站.md "wikilink")
  - 天福路

<!-- end list -->

  - 天耀路、[天耀巴士總站](../Page/天耀巴士總站.md "wikilink")

<!-- end list -->

  - 屏廈路

</div>

</div>

## 註釋

## 參考資料

## 外部連結

  - [房屋署天盛苑資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3196)
  - [天盛商場-領展](https://web.archive.org/web/20170111081644/http://www.linkreit.com/TC/properties/Pages/Property-Locator-Details.aspx?pid=172)

[en:Tin Shing Court](../Page/en:Tin_Shing_Court.md "wikilink")

[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>
2.  天盛苑噪音評估報告-附圖
3.  [天水圍街市斥千萬歐陸化 領展稱加租必然 居民：產品未符需要
    《明報》 2015-10-03](http://news.mingpao.com/pns/dailynews/web_tc/article/20151003/s00002/1443808357325)
4.  [天盛苑崩牆塌石屎　險壓傷食飯母子
    on.cc東網 2015年11月30日](http://hk.on.cc/hk/bkn/cnt/news/20151130/bkn-20151130225214830-1130_00822_001.html)
5.  [【居屋爆主力牆】嚇人！ 驗樓師驚訝牆身空心 勁多石仔
    蘋果日報 2015年12月1日](http://hk.apple.nextmedia.com/realtime/breaking/20151201/54488021)
6.