**天祥**（**Dodwell**）是[香港一間已結業的](../Page/香港.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")，屬英資百年老店[天祥洋行的百貨部門](../Page/天祥洋行.md "wikilink")，[英之傑集團旗下](../Page/英之傑.md "wikilink")，於1970年代曾在香港設有多家分店。

天祥的經營方式與現在的[馬莎百貨甚為接近](../Page/馬莎百貨.md "wikilink")，主要銷售入口自[英國的產品](../Page/英國.md "wikilink")，包括男女衣著服飾、童裝、[玩具及](../Page/玩具.md "wikilink")[食品等](../Page/食品.md "wikilink")，部份為自家品牌，顧客多為外籍人士，其衣物尺碼亦較大。當年天祥出售的貨品不少更為英國製造的入口貨品，貨品售價較高。

1990年代初期，天祥結業，部份店舖轉為馬莎百貨分店。而當年天祥出售的代理品牌「St.
Michaels」，現在由馬莎百貨自行經銷。天祥洋行則在1995年被[利豐收購](../Page/利豐.md "wikilink")。

## 分店

  - [尖沙咀](../Page/尖沙咀.md "wikilink")[海運大廈商場](../Page/海運大廈.md "wikilink")
  - [旺角](../Page/旺角.md "wikilink")[惠豐中心地庫](../Page/惠豐中心.md "wikilink")
  - [中環](../Page/中環.md "wikilink")[萬邦行](../Page/萬邦行.md "wikilink")
  - [銅鑼灣怡安大廈](../Page/銅鑼灣.md "wikilink")
  - [太古城中心](../Page/太古城中心.md "wikilink")

## 資料來源

  - [集體回憶 - 百貨公司](http://macpig.blogspot.com/2007/05/blog-post_08.html)
  - [香港地方 - 天祥的相關討論](http://www.hk-place.com/vp.php?board=2&id=914-1)
  - [集體回憶](http://minna-collectivememory.blogspot.com/feeds/posts/default)
  - [科學園地:
    星期五不適（一）](http://hansthefox.blogspot.com/2006/10/blog-post_22.html)
  - [廣東道上　還有海運](http://talkcinema.wordpress.com/2007/01/15/%E5%BB%A3%E6%9D%B1%E9%81%93%E4%B8%8A%E3%80%80%E9%82%84%E6%9C%89%E6%B5%B7%E9%81%8B/)
  - [利豐：併購家的獵豹式生存](http://www.cncapital.net/index1.asp?file=news.asp&id=16464)

[Category:香港百貨公司](../Category/香港百貨公司.md "wikilink")
[Category:香港已結業公司](../Category/香港已結業公司.md "wikilink")