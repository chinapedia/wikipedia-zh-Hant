**天通苑南站**位于[北京市](../Page/北京市.md "wikilink")[昌平区](../Page/昌平区.md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[5号线的一个车站](../Page/北京地铁5号线.md "wikilink")。車站色調為白色及紅色。

## 位置

这个站位于立汤路西侧，天通苑西单购物中心的对面，天通西苑一区东大门以北。

## 出口

这座车站仅有一个出口：A出口，在车站南端。临近天通苑西一区

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>立汤路（西侧）、公交天通西苑北站、天通西苑、天通苑</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 结构

这座车站为高架车站，侧式站台设计。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地上二层</strong></p></td>
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
</tr>
<tr class="even">
<td><p>北</p></td>
<td><p>列车往方向 <small>（） <small></small></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>列车往<a href="../Page/宋家庄站_(北京市).md" title="wikilink">宋家庄方向</a> <small>（）</small> 南</p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地面层</strong></p></td>
<td><p>站厅层</p></td>
</tr>
</tbody>
</table>

## 临近车站

[Category:昌平区地铁车站](../Category/昌平区地铁车站.md "wikilink")
[Category:2007年啟用的鐵路車站](../Category/2007年啟用的鐵路車站.md "wikilink")