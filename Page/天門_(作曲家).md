**天門**（，），本名為**白川
篤史**（），專職[日本電腦遊戲及動畫音樂的](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。出生於[東京都](../Page/東京都.md "wikilink")。

## 簡介

1990年，加入[日本Falcom](../Page/Falcom.md "wikilink")，並協助公司製作著名遊戲系列《[Brandish
撼天神塔系列](../Page/Brandish.md "wikilink")》及《[英雄傳說系列](../Page/英雄傳說系列.md "wikilink")》等遊戲的配樂。1999年，為當時同在Falcom工作的合約CG畫師[新海誠之自主動畫](../Page/新海誠.md "wikilink")《[她與她的貓](../Page/她與她的貓.md "wikilink")》擔任音樂製作，成為當時話題。2002年，退出
Falcom
公司並加入遊戲公司[minori](../Page/minori.md "wikilink")，亦有繼續為[新海誠的動畫製作配樂](../Page/新海誠.md "wikilink")。

  - 主要以[鋼琴與](../Page/鋼琴.md "wikilink")[小提琴為基本旋律構築優美的曲子](../Page/小提琴.md "wikilink")。
  - 高中時就認識的後輩[松江名俊後來成為漫畫家](../Page/松江名俊.md "wikilink")，現在會提供樂曲給松江名俊私人音樂使用。
  - [鋼琴是自己學習的](../Page/鋼琴.md "wikilink")，當時對於音樂理論與作曲理論其實並不了解。

## 主要作品

### 電視動畫

  -
  - [ef - a tale of
    memories.](../Page/ef_-_a_tale_of_memories..md "wikilink")（2007年
    minori/「ef」製作委員會）

  - [ef - a tale of
    melodies.](../Page/ef_-_a_tale_of_melodies..md "wikilink")（2008年
    minori/「ef2」製作委員會）

  - [猫的集会](../Page/猫的集会.md "wikilink")

  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")

  - [徒然喜歡你](../Page/徒然喜歡你.md "wikilink")

### 劇場版動畫

  - [她與她的貓](../Page/她與她的貓.md "wikilink")（1999年）
  - [星之聲](../Page/星之聲.md "wikilink")（2001年 Comix Wave）
  - [雲之彼端，約定的地方](../Page/雲之彼端，約定的地方.md "wikilink")（2004年 Comix Wave）
  - [秒速五公分](../Page/秒速五公分.md "wikilink")（2007年 Comix Wave）
  - [追逐繁星的孩子](../Page/追逐繁星的孩子.md "wikilink")（2011年 Comix Wave）

### 遊戲

#### 日本 Falcom

  - [Brandish撼天神塔系列](../Page/Brandish.md "wikilink")
      - Brandish（1991年）
      - Brandish 2（1993年）
      - Brandish 3（1994年）
      - Brandish VT（1996年）
      - Brandish 4（1998年）
  - [LORD MONARCH](../Page/LORD_MONARCH.md "wikilink")（1991年）
  - [Popful Mail](../Page/Popful_Mail.md "wikilink") 美少女劍士梅兒（1991年）
  - [英雄傳說2](../Page/英雄傳說.md "wikilink")（1992年）
  - [Ys](../Page/Ys.md "wikilink") 伊蘇系列
      - Ys IV（1993年）
      - Ys V（1995年）
      - Ys IE（1998年）
      - Ys IIE（2000年）
      - Ys I.II完全版（2001年）
  - [風之傳說XANADU](../Page/風之傳說XANADU.md "wikilink") 系列
      - 風之傳說XANADU（1994年）
      - 風之傳說XANADU II（1995年）
  - [英雄傳說 卡卡布三部曲](../Page/卡卡布三部曲.md "wikilink")
      - 英雄傳說3：白髮魔女（1994年、1999年）
      - 英雄傳說4：朱紅的淚（1996年、2000年）
      - 英雄傳說5：海之檻歌（1999年）
  - [XANADU](../Page/XANADU.md "wikilink") 雷諾尼都紀事（1995年）
  - [Sorcerian](../Page/Sorcerian.md "wikilink") 魔域傳說系列
  - [Zwei\!\!](../Page/Zwei!!.md "wikilink") 奇幻仙境（双星物语 2001年）

#### 其他

  - みずのかけら -once summer of islet-（2001年 io）
  - てんしのかけら -the lost article of memory-（2003年 io）
  - はるのあしおと -Deliver Spring to your heart-（中譯：春天的足音）（2004年 minori）
  - [ef - a fairy tale of the
    two.](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink")（2006年、2008年
    minori）
  - [eden\*](../Page/eden*.md "wikilink")（2009年 minori）
  - [夏空的英仙座](../Page/夏空的英仙座.md "wikilink")（2012年 minori）

## 關聯條目

  - [新海誠](../Page/新海誠.md "wikilink")

## 外部連結

  - [電奏楽団](http://www.interq.or.jp/piano/tenmon/)，天門的官方網站。

  - [電奏楽団　天門ぶろぐ](http://tenmon2009.blog37.fc2.com/)，天門的網誌。

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")