[Praha_1989,_Václavské_náměstí,_dav.jpg](https://zh.wikipedia.org/wiki/File:Praha_1989,_Václavské_náměstí,_dav.jpg "fig:Praha_1989,_Václavské_náměstí,_dav.jpg")的[瓦茨拉夫廣場上的人們](../Page/瓦茨拉夫廣場.md "wikilink")\]\]
[Albertov_deska.jpg](https://zh.wikipedia.org/wiki/File:Albertov_deska.jpg "fig:Albertov_deska.jpg")
[Havla_1989.jpg](https://zh.wikipedia.org/wiki/File:Havla_1989.jpg "fig:Havla_1989.jpg")向革命中的受伤人士致以敬意。
\]\]
[Bratislava_Slovakia_213.JPG](https://zh.wikipedia.org/wiki/File:Bratislava_Slovakia_213.JPG "fig:Bratislava_Slovakia_213.JPG")首都[布拉迪斯拉发的一块纪念物](../Page/布拉迪斯拉发.md "wikilink")(Námestie
SNP)\]\]
[17listopadu89_pomnik.JPG](https://zh.wikipedia.org/wiki/File:17listopadu89_pomnik.JPG "fig:17listopadu89_pomnik.JPG")

**天鵝絨革命**（[捷克文](../Page/捷克文.md "wikilink")：；[斯洛伐克文](../Page/斯洛伐克文.md "wikilink")：），又譯**絲絨革命**，狭义上是指[捷克斯洛伐克於](../Page/捷克斯洛伐克.md "wikilink")1989年11月（[东欧剧变时期](../Page/东欧剧变.md "wikilink")）發生的反[共產党](../Page/共產党.md "wikilink")[統治的](../Page/統治.md "wikilink")[民主化](../Page/民主化.md "wikilink")[革命](../Page/革命.md "wikilink")。从广义上讲，天鵝絨革命是和暴力[革命對比而来的](../Page/革命.md "wikilink")，指没有经过大规模的暴力冲突就实现了政权更迭，如[天鹅绒般平與柔滑](../Page/天鹅绒.md "wikilink")。21世纪初期一系列发生在中欧、东欧独联体国家亲[西方化的](../Page/西方國家.md "wikilink")[颜色革命基本上都是属于广义的](../Page/颜色革命.md "wikilink")“天鹅绒革命”类型。

## 歷史

雖然捷克斯洛伐克的民主化運動，在1968年的[布拉格之春中受到](../Page/布拉格之春.md "wikilink")[蘇聯的鎮壓](../Page/蘇聯.md "wikilink")，但是，國內對共產黨統治不滿的[知識份子仍然以地下方式活動](../Page/知識份子.md "wikilink")，並且於1977年提出要求政府遵守[赫爾辛基协议中人權條款的](../Page/赫爾辛基协议.md "wikilink")[七七憲章](../Page/七七憲章.md "wikilink")；而繼任[古斯塔夫·胡萨克出任第一書記的](../Page/古斯塔夫·胡萨克.md "wikilink")[米洛什·雅克什](../Page/米洛什·雅克什.md "wikilink")，企圖推動經濟改革與民主化，不過為時已晚。

捷克斯洛伐克在1989年之前就有不少要求民主的遊行活動，1989年11月17日（[国际学生日](../Page/国际学生日.md "wikilink")），首都[布拉格出現超過十萬人的遊行活動](../Page/布拉格.md "wikilink")，防暴警察在布拉格镇压学生示威。该事件引发了从11月19日到12月下旬的一系列示威。11月20日，在布拉格聚集的抗议者的数量从前一天的20万增加到大约50万。包括捷克斯洛伐克所有公民在内的两小时总罢工于11月27日举行。11月24日，为了应对其他共产主义政权的崩溃和越来越多的街头抗议，共产党的整个高级领导人包括总统秘书长米洛什·雅克什辞职。

[捷克斯洛伐克共产党在](../Page/捷克斯洛伐克共产党.md "wikilink")11月28日宣布，它将放弃权力并取消[一党专政](../Page/一党专政.md "wikilink")。两天后，立法机关正式删除了宪法中赋予共产党垄断权力的部分。捷克斯洛伐克与西德和奥地利国界上的铁丝网和其他障碍物在12月初被移除。
在布拉格之春中失勢的[亚历山大·杜布切克于](../Page/亚历山大·杜布切克.md "wikilink")12月28日当选为联邦议会议长，12月29日，根据多党选举的结果，「[公民論壇](../Page/公民論壇.md "wikilink")」獲得勝利，[瓦茨拉夫·哈维尔当选为捷克斯洛伐克总统](../Page/瓦茨拉夫·哈维尔.md "wikilink")，完成政權的和平轉移。

天鵝絨革命促進了捷克斯洛伐克的民主化與和平移轉政權。隨著民主化的影響，[斯洛伐克也出現了日益強烈的獨立建國主張](../Page/斯洛伐克.md "wikilink")，最後於1993年宣佈獨立，捷克斯洛伐克的聯邦體制也告瓦解。而這次斯洛伐克的和平獨立與聯邦體制的和平瓦解，亦稱「[天鵝絨離婚](../Page/天鵝絨離婚.md "wikilink")」。

## 参看

  - [捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")
  - [顏色革命](../Page/顏色革命.md "wikilink")

[天鹅绒革命](../Category/天鹅绒革命.md "wikilink")
[Category:东欧剧变](../Category/东欧剧变.md "wikilink")
[Category:1989年11月](../Category/1989年11月.md "wikilink")
[Category:捷克歷史](../Category/捷克歷史.md "wikilink")
[Category:斯洛伐克歷史](../Category/斯洛伐克歷史.md "wikilink")
[Category:20世紀的革命](../Category/20世紀的革命.md "wikilink")
[Category:1989年政治](../Category/1989年政治.md "wikilink")
[Category:民主转型](../Category/民主转型.md "wikilink")