**太兴**（431年正月—436年五月）是[十六國時期](../Page/十六國.md "wikilink")[北燕政權](../Page/北燕.md "wikilink")，北燕昭成帝[冯弘的](../Page/冯弘.md "wikilink")[年號](../Page/年號.md "wikilink")，共計5年餘。

## 纪年

| 太兴                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 431年                           | 432年                           | 433年                           | 434年                           | 435年                           | 436年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元嘉](../Page/元嘉_\(南朝宋文帝\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [勝光](../Page/胜光.md "wikilink")（428年二月-431年六月）：[夏国政权](../Page/夏国.md "wikilink")[赫连定年号](../Page/赫连定.md "wikilink")
      - [永弘](../Page/永弘.md "wikilink")（428年五月—431年正月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏暮末年号](../Page/乞伏暮末.md "wikilink")
      - [承玄](../Page/承玄.md "wikilink")（428年六月-431年）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [義和](../Page/义和_\(北凉\).md "wikilink")（431年六月-433年四月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [承和](../Page/承和_\(北凉\).md "wikilink")（433年四月-439年九月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠牧犍年号](../Page/沮渠牧犍.md "wikilink")
      - [神䴥](../Page/神䴥.md "wikilink")（428年正月-431年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [延和](../Page/延和_\(北魏太武帝\).md "wikilink")（432年二月-435年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [太延](../Page/太延.md "wikilink")（435年正月-440年六月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [泰始](../Page/泰始_\(趙廣\).md "wikilink")（432年正月-437年四月）：[南朝時](../Page/南朝.md "wikilink")[益州領導](../Page/益州.md "wikilink")[趙廣](../Page/趙廣.md "wikilink")、[程道養年号](../Page/程道養.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北燕年號](../Category/北燕年號.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")