[云岗石窟.JPG](https://zh.wikipedia.org/wiki/File:云岗石窟.JPG "fig:云岗石窟.JPG")，主要建于453年到495年（太和十九年）间，中国第一处由皇室显贵主持开凿、建于国都的大型石窟。2001年列为世界文化遗产。\]\]
[Longmen_Grottoes_Pano.JPG](https://zh.wikipedia.org/wiki/File:Longmen_Grottoes_Pano.JPG "fig:Longmen_Grottoes_Pano.JPG")，始建于北魏太和年间、迁都洛阳（494年，太和十八年）的前几年。2000年列为世界文化遗产。\]\]
**太和**（477年正月-499年十二月）是[北魏的君主魏孝文帝](../Page/北魏.md "wikilink")[元宏的第三个](../Page/元宏.md "wikilink")[年号](../Page/年号.md "wikilink")，也是他的最后一个年号，共计近23年。太和二十三年四月北魏宣武帝[元恪即位沿用](../Page/元恪.md "wikilink")，次年改元[景明](../Page/景明.md "wikilink")。

春正月乙酉朔，诏曰：“朕夙承宝业，惧不堪荷，而天贶具臻，地瑞并应，风和气晼，天人交协。岂朕冲昧所能致哉？实赖神祇七庙降福之助。今三正告初，祗感交切，宜因阳始，协典革元，其改今号为太和元年。”

## 大事记

  - 477年（太和元年）：[文成文明皇后冯氏临朝揽政](../Page/文成文明皇后.md "wikilink")，魏孝文帝听政，群臣奏事并称“二圣”。
  - 486年（太和十年）：北魏孝文帝开始穿戴皇帝冠冕亲政。但一切大事他都禀明文明冯太后之后再施行。
  - 494年（太和十八年）：北魏孝文帝迁都[洛阳](../Page/洛阳.md "wikilink")。
  - 495年（太和十九年）：創建[少林寺](../Page/少林寺.md "wikilink")，供养西域高僧跋陀。

## 出生

  - 493年（太和十七年）：[尔朱荣](../Page/尔朱荣.md "wikilink")

## 逝世

  - 490年（太和十四年）：[冯太后](../Page/冯太后.md "wikilink")

## 纪年

| 太和                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 477年                           | 478年                           | 479年                           | 480年                           | 481年                           | 482年                           | 483年                           | 484年                           | 485年                           | 486年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") |
| 太和                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| [公元](../Page/公元纪年.md "wikilink") | 487年                           | 488年                           | 489年                           | 490年                           | 491年                           | 492年                           | 493年                           | 494年                           | 495年                           | 496年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") |
| 太和                               | 二十一年                           | 二十二年                           | 二十三年                           |                                |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 497年                           | 498年                           | 499年                           |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他时期使用的[太和年号](../Page/太和.md "wikilink")
  - 同期存在的其他政权年号
      - [元徽](../Page/元徽.md "wikilink")（473年正月—477年七月）：[南朝宋](../Page/南朝宋.md "wikilink")[宋后废帝刘昱的年号](../Page/宋后废帝.md "wikilink")
      - [昇明](../Page/昇明.md "wikilink")（477年七月—479年四月）：[南朝宋](../Page/南朝宋.md "wikilink")[宋順帝刘准的年号](../Page/宋順帝.md "wikilink")
      - [建元](../Page/建元_\(萧道成\).md "wikilink")（479年四月—482年十二月）：[南朝齊齊高帝](../Page/南朝齊.md "wikilink")[蕭道成的年号](../Page/蕭道成.md "wikilink")
      - [永明](../Page/永明.md "wikilink")（483年正月—493年十二月）：[南朝齊](../Page/南朝齊.md "wikilink")[齊武帝萧赜的年号](../Page/齊武帝.md "wikilink")
      - [興平](../Page/兴平_\(唐寓之\).md "wikilink")（486年）：[唐寓之的年号](../Page/唐寓之.md "wikilink")
      - [隆昌](../Page/隆昌_\(萧昭业\).md "wikilink")（494年正月—七月）：[南朝齊鬱林王](../Page/南朝齊.md "wikilink")[萧昭业的年号](../Page/萧昭业.md "wikilink")
      - [延興](../Page/延兴_\(萧昭文\).md "wikilink")（494年七月—十月）：[南朝齊海陵王](../Page/南朝齊.md "wikilink")[萧昭文的年号](../Page/萧昭文.md "wikilink")
      - [建武](../Page/建武_\(齐明帝\).md "wikilink")（494年十月—498年四月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐明帝萧鸾的年号](../Page/齐明帝.md "wikilink")
      - [永泰](../Page/永泰_\(齐明帝\).md "wikilink")（498年四月—十二月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐明帝萧鸾的年号](../Page/齐明帝.md "wikilink")
      - [永元](../Page/永元_\(齊東昏侯\).md "wikilink")（499年正月—501年三月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐東昏侯萧宝卷的年号](../Page/齐東昏侯.md "wikilink")
      - [永康](../Page/永康_\(柔然\).md "wikilink")（464年—484年）：[柔然政權受羅部真可汗](../Page/柔然.md "wikilink")[予成年號](../Page/予成.md "wikilink")
      - [太平](../Page/太平_\(柔然\).md "wikilink")（485年—491年）：[柔然政權伏名敦可汗](../Page/柔然.md "wikilink")[豆崙年號](../Page/豆崙.md "wikilink")
      - [太安](../Page/太安_\(柔然\).md "wikilink")（492年—505年）：[柔然政權候其伏代庫者可汗](../Page/柔然.md "wikilink")[那蓋年號](../Page/那蓋.md "wikilink")
      - [建初](../Page/建初_\(高昌\).md "wikilink")（489年—491年）：[高昌政权年号](../Page/高昌.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

{{-}}

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")