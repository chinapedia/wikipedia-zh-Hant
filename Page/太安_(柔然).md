**太安**（492年-505年）是[柔然的君主候其伏代库者可汗](../Page/柔然.md "wikilink")[那盖的年號](../Page/那盖.md "wikilink")，共计14年。

## 大事记

## 出生

## 逝世

## 纪年

| 太安                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 492年                           | 493年                           | 494年                           | 495年                           | 496年                           | 497年                           | 498年                           | 499年                           | 500年                           | 501年                           |
| [干支](../Page/干支纪年.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") |
| 太安                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 502年                           | 503年                           | 504年                           | 505年                           |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") |                                |                                |                                |                                |                                |                                |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[太安年號的政權](../Page/太安.md "wikilink")
  - 同期存在的其他政权年号
      - [永明](../Page/永明.md "wikilink")（483年正月—493年十二月）：[南朝齊](../Page/南朝齊.md "wikilink")[齊武帝萧赜的年号](../Page/齊武帝.md "wikilink")
      - [隆昌](../Page/隆昌_\(萧昭业\).md "wikilink")（494年正月—七月）：[南朝齊鬱林王](../Page/南朝齊.md "wikilink")[萧昭业的年号](../Page/萧昭业.md "wikilink")
      - [延興](../Page/延兴_\(萧昭文\).md "wikilink")（494年七月—十月）：[南朝齊海陵王](../Page/南朝齊.md "wikilink")[萧昭文的年号](../Page/萧昭文.md "wikilink")
      - [建武](../Page/建武_\(齐明帝\).md "wikilink")（494年十月—498年四月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐明帝萧鸾的年号](../Page/齐明帝.md "wikilink")
      - [永泰](../Page/永泰_\(齐明帝\).md "wikilink")（498年四月—十二月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐明帝萧鸾的年号](../Page/齐明帝.md "wikilink")
      - [永元](../Page/永元_\(齊東昏侯\).md "wikilink")（499年正月—501年三月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐東昏侯萧宝卷的年号](../Page/齐東昏侯.md "wikilink")
      - [建義](../Page/建义_\(雍道晞\).md "wikilink")（500年二月—三月）：[雍道晞的年号](../Page/雍道晞.md "wikilink")
      - [中興](../Page/中兴_\(齐和帝\).md "wikilink")（501年三月—502年三月）：[南朝齊](../Page/南朝齊.md "wikilink")[齐和帝萧宝融的年号](../Page/齐和帝.md "wikilink")
      - [天監](../Page/天監.md "wikilink")（502年四月—519年十二月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [太和](../Page/太和_\(北魏孝文帝\).md "wikilink")（477年正月-499年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝文帝元宏年号](../Page/北魏孝文帝.md "wikilink")
      - [景明](../Page/景明.md "wikilink")（500年正月-504年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏宣武帝元恪年号](../Page/北魏宣武帝.md "wikilink")
      - [正始](../Page/正始_\(北魏宣武帝\).md "wikilink")（504年正月-508年八月）：[北魏政权](../Page/北魏.md "wikilink")[北魏宣武帝元恪年号](../Page/北魏宣武帝.md "wikilink")
      - [建初](../Page/建初_\(高昌\).md "wikilink")）：[高昌政权年号](../Page/高昌.md "wikilink")
      - [承平](../Page/承平_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:柔然年号](../Category/柔然年号.md "wikilink")
[Category:5世纪年号](../Category/5世纪年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:490年代中国政治](../Category/490年代中国政治.md "wikilink")
[Category:500年代中国政治](../Category/500年代中国政治.md "wikilink")