**太平劇團**由[馬師曾在](../Page/馬師曾.md "wikilink")1933年所成立，於1943年日軍入侵時結束。資金來自[香港](../Page/香港.md "wikilink")[太平戲院的老闆](../Page/太平戲院.md "wikilink")[源杏翹](../Page/源杏翹.md "wikilink")。由於十年來能夠在同一間[太平戲院連續演出](../Page/太平戲院.md "wikilink")，所以成為[香港著名並非常賣座的粵劇劇團](../Page/香港.md "wikilink")。\[1\]

1933年春天，[馬師曾從](../Page/馬師曾.md "wikilink")[美國返回](../Page/美國.md "wikilink")[香港後](../Page/香港.md "wikilink")，馬上接受[太平戲院老闆](../Page/太平戲院.md "wikilink")[源杏翹的邀請](../Page/源杏翹.md "wikilink")，組織「太平歌劇社」(後改名太平劇團)。當初，他找到了著名男花旦[陳非儂](../Page/陳非儂.md "wikilink")，還有[半日安](../Page/半日安.md "wikilink")、[馮醒錚](../Page/馮醒錚.md "wikilink")、[馮俠魂](../Page/馮俠魂.md "wikilink")、[袁是我](../Page/袁是我.md "wikilink")、[謝醒儂](../Page/謝醒儂.md "wikilink")、[馮玉君等人加盟](../Page/馮玉君.md "wikilink")。後來[陳非儂因為接了](../Page/陳非儂.md "wikilink")[越南及](../Page/越南.md "wikilink")[緬甸之約](../Page/缅甸.md "wikilink")，便離開了太平劇團。\[2\]

太平劇團的班主[源杏翹適逢其會](../Page/源杏翹.md "wikilink")，聘請[譚蘭卿](../Page/譚蘭卿.md "wikilink")、[上海妹及](../Page/上海妹.md "wikilink")[麥顰卿三位女藝人為花旦](../Page/麥顰卿.md "wikilink")，打破了當年香港不准男女同班的禁令。這樣令觀眾耳目一新，盛極一時，其它各班亦紛紛聘用女花旦，於是男花旦漸遭淘汰，是粵劇歷史大轉變之一。\[3\]\[4\]期後加盟的還有[衛少芳](../Page/衛少芳.md "wikilink")、[區倩明](../Page/區倩明.md "wikilink")、[區倩坤](../Page/區倩坤.md "wikilink")、[楚岫雲](../Page/楚岫雲.md "wikilink")、[鳳凰女等名女花旦](../Page/鳳凰女.md "wikilink")，丑生王[梁醒波](../Page/梁醒波.md "wikilink")、[黃鶴聲](../Page/黃鶴聲.md "wikilink")、[馮俠魂](../Page/馮俠魂.md "wikilink")、[趙驚魂等](../Page/趙驚魂.md "wikilink")。1942年，紅線女加入並得馬師曾的重用。同年她因演出《[雷雨](../Page/雷雨.md "wikilink")》中繁漪一角，逐漸成名。

十年來的編劇曾有[盧有容](../Page/盧有容.md "wikilink")、[馮顯洲](../Page/馮顯洲.md "wikilink")、[黃金昌](../Page/黃金昌.md "wikilink")。他們為[馬師曾編寫古今中外內容及其形式的劇目](../Page/馬師曾.md "wikilink")。譬如《[刁蠻公主戇駙馬](../Page/刁蠻公主戇駙馬.md "wikilink")》、《[野花香](../Page/野花香.md "wikilink")》、《[斗氣姑爺](../Page/斗氣姑爺.md "wikilink")》、《[審死官](../Page/審死官.md "wikilink")》、《[我為卿狂](../Page/我為卿狂.md "wikilink")》、《[野玫瑰怒殺洪承疇](../Page/野玫瑰怒殺洪承疇.md "wikilink")》。在長達近10年時間中，新劇迭出，大受觀眾的熱烈歡迎。\[5\]

同時，[馬師曾在太平劇團內積極進行粵劇改革](../Page/馬師曾.md "wikilink")。他把西方電影的導演、排練制度引進粵劇，戒除[提綱戲那種臨場即興的做法](../Page/提綱戲.md "wikilink")。佈景方面則運用了硬景、燈光等舞台技術和器材。音樂方面，引用大量西洋樂器來豐富[粵劇的音樂](../Page/粵劇.md "wikilink")。他被新聞界稱為「新派粵劇泰斗導演
兼主演藝術鉅子」。太平劇團與[薛覺先所帶領的](../Page/薛覺先.md "wikilink")[覺先聲劇團展開了粵劇藝術上的競爭](../Page/覺先聲劇團.md "wikilink")。由於雙方各有千秋，彼此並駕齊驅，粵劇歷史上稱為「薛馬爭雄」時期。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:粵劇劇團](../Category/粵劇劇團.md "wikilink")

1.  [戲班與劇團選介](http://www.gdyjy.com/gb/news_view.asp?newsid=19&anclassid=2&nclassid=2),廣東粵劇院
2.  [粵劇的源流與歷史](https://web.archive.org/web/20010627024142/http://hk.geocities.com/musicproject_2001/chinese_opera2.htm)
3.  [戲行史話](http://www.yanruyu.com/jhy/12289.shtml)
4.  [譚蘭卿](http://www.009y.com/212/viewspace-642)
5.  [廣州市誌](http://www.gzsdfz.org.cn/gzsz/17/wh/sz17wh020304.htm)