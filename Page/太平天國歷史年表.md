**太平天國大事年表**按時間先後，列出與[太平天國相關的大事](../Page/太平天國.md "wikilink")。

  - [1843年](../Page/1843年.md "wikilink")
      - 7月
        [洪秀全與](../Page/洪秀全.md "wikilink")[馮雲山](../Page/馮雲山.md "wikilink")、[洪仁玕在家乡广东](../Page/洪仁玕.md "wikilink")[花县建立](../Page/花县.md "wikilink")[拜上帝會](../Page/拜上帝會.md "wikilink")，自稱上帝之子，並稱呼上帝為天父、[耶穌為天兄](../Page/耶穌.md "wikilink")。
  - [1844年](../Page/1844年.md "wikilink")
      - 4月 洪秀全、馮雲山等人離開家鄉傳教。
      - 5月
        洪秀全及馮雲山到[廣西](../Page/廣西.md "wikilink")[貴縣](../Page/貴縣.md "wikilink")（[廣西](../Page/廣西.md "wikilink")[貴港](../Page/貴港.md "wikilink")）傳教，後馮雲山轉至紫荊山區一帶，洪秀全回[廣東](../Page/廣東.md "wikilink")。
  - [1847年](../Page/1847年.md "wikilink")
      - 3月
        洪秀全及洪仁玕到廣東，在[美國教士](../Page/美國.md "wikilink")[羅孝全](../Page/羅孝全.md "wikilink")（Issachar
        Jacob
        Roberts）那裡學習，首次讀到[聖經](../Page/聖經.md "wikilink")。後來洪秀全請求洗禮，為羅孝全拒絕。
      - 8月 洪秀全回廣西紫荊山，與馮雲山發展拜上帝會。
  - [1848年](../Page/1848年.md "wikilink")
      - 4月 [楊秀清首次假裝](../Page/楊秀清.md "wikilink")「天父下凡」\[1\]。
      - 10月 [蕭朝貴首次假裝](../Page/蕭朝貴.md "wikilink")「天兄下凡」。
  - [1851年](../Page/1851年.md "wikilink")
      - 1月 拜上帝會於廣西桂平金田村起義，建國號“太平天国”，改元“太平天国元年”。
      - 3月 洪秀全稱“天王”。
      - 9月 太平軍攻佔永安。
      - 12月 在永安封東、南、西、北、翼王，建立太平天國基本制度，稱為“永安建制”。
  - [1852年](../Page/1852年.md "wikilink")
      - 4月 太平軍從永安突圍，進圍[桂林](../Page/桂林.md "wikilink")。
      - 6月 南王馮雲山傷重死亡。
      - 9月 太平軍攻[長沙](../Page/長沙市.md "wikilink")，西王蕭朝貴戰死。
  - [1853年](../Page/1853年.md "wikilink")
      - 1月
        太平軍攻陷[武漢](../Page/武漢.md "wikilink")，湖北巡撫[常大淳死](../Page/常大淳.md "wikilink")。太平軍增至五十萬。[曾國藩辦湖南團練](../Page/曾國藩.md "wikilink")，即為湘軍。
      - 2月
        攻陷[安慶](../Page/安慶.md "wikilink")，安徽巡撫[蔣文慶自殺](../Page/蔣文慶.md "wikilink")。
      - 3月
        攻陷[南京](../Page/南京.md "wikilink")，江寧將軍祥厚、兩江總督[陸建瀛戰死](../Page/陸建瀛.md "wikilink")。南京易名[天京](../Page/天京.md "wikilink")，定為首都。清軍建江南、江北大營。
      - 5月 太平軍北伐，西征。
  - [1854年](../Page/1854年.md "wikilink")
      - 1月
        太平軍攻克[廬州](../Page/廬州.md "wikilink")，安徽巡撫[江忠源自殺](../Page/江忠源.md "wikilink")。
      - 2月 湖廣總督[吳文鎔戰死](../Page/吳文鎔.md "wikilink")。
      - 4月 湘潭之戰、靖港之戰。曾立昌所率的北伐援軍潰敗。
      - 6月 太平軍二克武昌。
      - 10月 清軍收復武昌。
  - [1855年](../Page/1855年.md "wikilink")
      - 1月
        翼王[石達開於](../Page/石達開.md "wikilink")[鄱陽湖口大敗](../Page/鄱陽湖.md "wikilink")[湘軍](../Page/湘軍.md "wikilink")。
      - 3月 清軍攻下連鎮，北伐軍主將[林鳳祥被俘](../Page/林鳳祥.md "wikilink")，不久處死。
      - 4月 太平軍三克武昌。
      - 5月 北伐軍全軍覆沒，[李開芳被俘後遭處死](../Page/李開芳.md "wikilink")。
  - [1856年](../Page/1856年.md "wikilink")
      - 年初 太平天国東征，攻取[揚州](../Page/揚州.md "wikilink")，破江北大營；石達開破江南大營；天京解圍。
      - 9月
        [天京事变](../Page/天京事变.md "wikilink")，太平天国內訌，諸王互殺，东王杨秀清、北王[韦昌辉](../Page/韦昌辉.md "wikilink")、燕王[秦日纲先後被杀](../Page/秦日纲.md "wikilink")。
  - [1857年](../Page/1857年.md "wikilink")
      - 6月 [石達開帶兵出走](../Page/石達開.md "wikilink")。
  - [1858年](../Page/1858年.md "wikilink")
      - 5月 九江失守，守將[林啟榮戰死](../Page/林啟榮.md "wikilink")。
      - 9月
        [李秀成](../Page/李秀成.md "wikilink")、[陳玉成破江北大营](../Page/陳玉成.md "wikilink")。
      - 11月 [三河镇大捷](../Page/三河.md "wikilink")。
  - [1859年](../Page/1859年.md "wikilink")
      - 4月 洪仁玕從[香港抵天京](../Page/香港.md "wikilink")，封-{干}-王。
      - 陳玉成封英王、李秀成封忠王。
  - [1860年](../Page/1860年.md "wikilink")
      - 4月 太平軍破江南大營，迫近[上海](../Page/上海.md "wikilink")。
      - 5月 破江北大營。
      - 6月 上海成立“中外會防局”，由美國人[華爾建](../Page/華爾.md "wikilink")「洋鎗隊」，助攻太平軍。
  - [1861年](../Page/1861年.md "wikilink")
      - 9月 安慶失守，守將[葉云來戰死](../Page/葉云萊.md "wikilink")。
      - [李鴻章辦](../Page/李鴻章.md "wikilink")[淮軍](../Page/淮軍.md "wikilink")。
      - 12月 李秀成攻佔杭州，巡撫[王有齡自殺](../Page/王有齡.md "wikilink")。
  - [1862年](../Page/1862年.md "wikilink")
      - 2月 洋鎗隊改為[常勝軍](../Page/常勝軍.md "wikilink")。
      - 5月 李秀成大敗常勝軍。
      - 6月 陳玉成被清軍殺害。
  - [1863年](../Page/1863年.md "wikilink")
      - 3月
        [英國人](../Page/英國.md "wikilink")[戈登接任常勝軍管帶](../Page/查理·喬治·戈登.md "wikilink")。
      - 6月 [石達開於大渡河旁投降](../Page/石達開.md "wikilink")，在成都被處死。
      - 12月 淮軍、常勝軍攻克[蘇州](../Page/蘇州.md "wikilink")。
  - [1864年](../Page/1864年.md "wikilink")
      - 6月 洪秀全病逝，子[洪天貴福即位](../Page/洪天貴福.md "wikilink")，是为“幼天王”。
      - 7月 天京陷落。
      - 8月 李秀成被清軍處死。
      - 10月 洪仁玕及幼天王先後在江西被俘。
      - 11月 幼天王及洪仁玕先後在南昌被處死。

## 注釋

<div class="references-small">

<references/>

</div>

[Category:太平天国](../Category/太平天国.md "wikilink")

1.