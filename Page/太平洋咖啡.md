[PacificCoffee_logo.svg](https://zh.wikipedia.org/wiki/File:PacificCoffee_logo.svg "fig:PacificCoffee_logo.svg")
[Pacific_Coffee_in_Hollywood_Centre_(Sheung_Wan)_2015.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Coffee_in_Hollywood_Centre_\(Sheung_Wan\)_2015.jpg "fig:Pacific_Coffee_in_Hollywood_Centre_(Sheung_Wan)_2015.jpg")[荷李活商業中心分店](../Page/荷李活商業中心_\(上環\).md "wikilink")\]\]
[Pacific_Coffee_in_cityplaza_2018.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Coffee_in_cityplaza_2018.jpg "fig:Pacific_Coffee_in_cityplaza_2018.jpg")分店\]\]

**太平洋咖啡**（，英語舊稱：**Pacific Coffee Company** ,
**PCC**）是[亚洲的一個連鎖美式](../Page/亚洲.md "wikilink")[咖啡店](../Page/咖啡店.md "wikilink")[品牌](../Page/品牌.md "wikilink")，在香港是最大的咖啡連鎖店之一。由一對在1992年來自[美國](../Page/美國.md "wikilink")[西雅圖的夫婦Thomas](../Page/西雅圖.md "wikilink")
Neir在香港創立。其起源地與[星巴克相同](../Page/星巴克.md "wikilink")，惟[星巴克於](../Page/星巴克.md "wikilink")2000年5月才在香港成立首間分店。

其公司宗旨為**一杯、一念、一世界**，意思分別「為用心調製品質優越的咖啡」、「盡心實踐謙卑感恩的服務理念」及「專心營造寧靜溫馨的時空」。各店鋪內均設有梳化、[無線上網服務](../Page/無線上網.md "wikilink")(其服務由[網上行寬頻提供](../Page/網上行寬頻.md "wikilink"))，讓顧客於舒適的環境下享受完美的咖啡樂趣。

2005年被其士泛亞控股有限公司收購，自此品牌發展迅速，分店數量不斷增加，更擴展至[新加坡](../Page/新加坡.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")，2010年被[華潤收購后更拓展至](../Page/華潤.md "wikilink")[中國內地](../Page/中國內地.md "wikilink")，中國內地的店鋪分別位於[廣州](../Page/廣州.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[上海](../Page/上海.md "wikilink")、[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[珠海](../Page/珠海.md "wikilink")、[佛山](../Page/佛山.md "wikilink")、[西安](../Page/西安.md "wikilink")、[成都](../Page/成都.md "wikilink")、[武漢](../Page/武漢.md "wikilink")、[青島](../Page/青島.md "wikilink")、[瀋陽](../Page/瀋陽.md "wikilink")、[長沙](../Page/長沙.md "wikilink")、[南京](../Page/南京.md "wikilink")、[蘇州](../Page/蘇州.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[廈門等地](../Page/廈門.md "wikilink")。

太平洋咖啡以[香港島為重心地](../Page/香港島.md "wikilink")，亦進駐各大專院校，如[香港公開大學及](../Page/香港公開大學.md "wikilink")[香港英國文化協會校院內](../Page/英國文化協會.md "wikilink")。
除經營[零售店舖外](../Page/零售.md "wikilink")，太平洋咖啡亦供應其品牌的咖啡豆及[瑞士牌子JURA的](../Page/瑞士.md "wikilink")[咖啡機予批發商及公司顧客](../Page/咖啡機.md "wikilink")，包括銀行、航空公司、會所、酒店及大型商業機構，同時亦於香港、[澳門及](../Page/澳門.md "wikilink")[新加坡從事咖啡豆](../Page/新加坡.md "wikilink")[批發](../Page/批發.md "wikilink")。

## 簡史

1993年，Thomas
Neir創立太平洋咖啡。第一所咖啡店設於[香港](../Page/香港.md "wikilink")[中環的](../Page/中環.md "wikilink")[美國銀行中心](../Page/美國銀行中心.md "wikilink")。

2005年4月6日，[其士國際集團旗下](../Page/其士國際集團.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")[其士科技](../Page/其士泛亞.md "wikilink")(於2007年更名「[其士泛亞](../Page/其士泛亞.md "wikilink")」)以2.05億港元收購太平洋咖啡。

2008-09年度錄得税後[虧損](../Page/虧損.md "wikilink")2,030萬港元。

2009年7月，太平洋咖啡於[澳門](../Page/澳門.md "wikilink")[中區](../Page/中區_\(澳門\).md "wikilink")[新馬路中央廣場試業](../Page/新馬路.md "wikilink")，於澳門開設首家分店。

2009-10年度錄得税後[盈利](../Page/盈利.md "wikilink")1,760萬港元。

2010年6月29日，[華潤創業以](../Page/華潤創業.md "wikilink")3.266億港元(4,187.2萬美元)，向其士泛亞收購香港太平洋咖啡八成股份，為集團打開內地的咖啡店市場作好準備。而其士泛亞將繼續保留太平洋咖啡兩成的權益。而品牌的主調顏色調整為鮮明的紅、黑、白，＂Company＂一字也從商標上被刪除。

2015年，其商標正式全面更新為紅圈與咖啡杯及咖啡豆圖案的組合，紅圈內取而代之的是 “Pacific Coffee”。Pacific
Coffee提供的不僅僅是一杯完美的咖啡，而是一種更豐富的生活方式。“一杯一念一世界” 成為 Pacific Coffee新的使命。

## 分店

除香港外，太平洋咖啡於中國、新加坡、馬來西亞亦有分店。此外亦曾於[塞浦路斯擁有一家分店](../Page/塞浦路斯.md "wikilink")。

## 奬項

2005年，[電腦晶片製造商](../Page/電腦晶片.md "wikilink")[英特爾一項大型投票活動選舉](../Page/英特爾.md "wikilink")「亞洲十大上網熱點」，由[太平山頂的](../Page/太平山_\(香港\).md "wikilink")「太平洋咖啡」奪得榜首\[1\]。

## 產品及服務

[Pacific_Coffee_sandwich.JPG](https://zh.wikipedia.org/wiki/File:Pacific_Coffee_sandwich.JPG "fig:Pacific_Coffee_sandwich.JPG")\]\]
產品：

1.  烘焙咖啡
2.  特色咖啡
3.  冷凍飲品
4.  烘烤食品及[酥皮糕點](../Page/酥皮糕點.md "wikilink")
5.  小食
6.  與咖啡有關之器具及配件

服務：

1.  咖啡零售
2.  咖啡及咖啡豆批發

## 相集

<File:Ifc> PacificCoffee
20071110.jpg|位於[中環](../Page/中環.md "wikilink")[國際金融中心
(香港)的分店](../Page/國際金融中心_\(香港\).md "wikilink")（已結業） <File:HK> Central
Lyndhurst Terrace Pacific Coffee
Company.JPG|位於[上環](../Page/上環.md "wikilink")[擺花街的分店](../Page/擺花街.md "wikilink")
<File:HK> Sheung Wan Hollywood Road Pacific Coffee
Co.JPG|位於[上環](../Page/上環.md "wikilink")[荷李活道的分店](../Page/荷李活道.md "wikilink")（已結業）
<File:HK> Kln Bay Telford Plaza Pacific Coffee Company
a.jpg|[德福廣場分店](../Page/德福廣場.md "wikilink")（已搬遷）
<File:Pacific> Coffee Company at Grand Century Place.JPG|
位於[旺角](../Page/旺角.md "wikilink")[新世紀廣場中庭](../Page/新世紀廣場.md "wikilink")2樓的分店（已搬遷）

## 主要競爭對手

  - [星巴克](../Page/星巴克.md "wikilink")
  - [Pret A Manger](../Page/Pret_A_Manger.md "wikilink")
  - [McCafé](../Page/McCafé.md "wikilink")

## 参考文献

## 外部链接

  - [Pacific Coffee Company
    中文主頁](http://www.pacificcoffee.com/chi/index.php)
  - [Pacific Coffee Company
    英文主頁](http://www.pacificcoffee.com/eng/index.php)
  - [Thomas Neir -
    太平洋咖啡店創辦人及行政總裁](https://web.archive.org/web/20071006054718/http://www.darden.edu/uploadedFiles/Features/Showcase_Ad_Campaign/shc_Neir%5B1%5D.pdf)
  - [香港上市公司其士，公佈有關太平洋咖啡店事項](https://web.archive.org/web/20061019235719/http://www.chevalier.com/english/files/inv_inf/citl/2005/a_8_apr_2005.pdf)
  - [香港上市公司華潤創業，公佈有關太平洋咖啡店事項](http://www.cre.com.hk/press/c-20100629R.pdf)

{{-}}

[category:華潤](../Page/category:華潤.md "wikilink")

[Category:中国咖啡店](../Category/中国咖啡店.md "wikilink")
[Category:香港西餐廳](../Category/香港西餐廳.md "wikilink")
[Category:香港公司](../Category/香港公司.md "wikilink")
[Category:其士國際](../Category/其士國際.md "wikilink")

1.  [亞洲票選上網熱點
    香港山頂太平洋咖啡上網最歎](http://travel.atnext.com/index.cfm?fuseaction=InfoCenter.TravelNews&issueID=20051111&articleID=5385024&pageNumber=33)