[Plates_tect2_zh.svg](https://zh.wikipedia.org/wiki/File:Plates_tect2_zh.svg "fig:Plates_tect2_zh.svg")

**太平洋板塊**是一塊海洋地殼[板塊](../Page/板塊.md "wikilink")，大部分位於[太平洋海面下](../Page/太平洋.md "wikilink")。它是法国地质学家[勒皮雄](../Page/勒皮雄.md "wikilink")1968年首次提出的六大板块之一，自提出以来，其范围基本没有大的变动。

太平洋板块的東北面與[探险家板块](../Page/探险家板块.md "wikilink")、[胡安·德富卡板块以及](../Page/胡安·德富卡板块.md "wikilink")[戈尔达板块之間具有](../Page/戈尔达板块.md "wikilink")[张裂型板块边界](../Page/张裂型板块边界.md "wikilink")，分別形成[探险家海岭](../Page/探险家海岭.md "wikilink")、[胡安·德富卡海岭和](../Page/胡安·德富卡海岭.md "wikilink")[戈尔达海岭](../Page/戈尔达海岭.md "wikilink")。中部東面與[北美洲板块之間沿著](../Page/北美洲板块.md "wikilink")[聖安地列斯斷層形成](../Page/聖安地列斯斷層.md "wikilink")[转换边界](../Page/转换边界.md "wikilink")，並與[里维拉板块和](../Page/里维拉板块.md "wikilink")[科科斯板块形成张裂型边界](../Page/科科斯板块.md "wikilink")。东南面则与[纳斯卡板块形成张裂型边界](../Page/纳斯卡板块.md "wikilink")，即[东太平洋海隆](../Page/东太平洋海隆.md "wikilink")。探险家板块、胡安·德富卡板块、戈尔达板块、里维拉板块、科科斯板块和纳斯卡板块都是古[法拉龙板块的残余](../Page/法拉龙板块.md "wikilink")。

南面與[南極板塊之間也是张裂型邊界](../Page/南極板塊.md "wikilink")，形成[太平洋-南極洋脊](../Page/太平洋-南極洋脊.md "wikilink")（Pacific-Antarctic
Ridge）

西面與[歐亞板塊之間存在](../Page/歐亞板塊.md "wikilink")[聚合型板块边界](../Page/聚合型板块边界.md "wikilink")，其中靠北方的一邊沉入[隐没于歐亞板塊之下](../Page/隐没带.md "wikilink")，中間部分則與[菲律賓板塊形成](../Page/菲律賓板塊.md "wikilink")[馬里亞納海溝](../Page/馬里亞納海溝.md "wikilink")。西南面與[印度-澳大利亞板塊](../Page/印度-澳大利亞板塊.md "wikilink")（印澳板塊）形成複雜但主要為聚合型的邊界，並於[紐西蘭北方潜没于印澳板塊之下](../Page/紐西蘭.md "wikilink")，兩者之間造成了一個轉換邊界，形成Alpine斷層。更往南一點，則是印澳板塊沉入太平洋板塊下方。

北面沉入北美板塊，為聚合型邊界，並形成[阿留申海溝與鄰近的](../Page/阿留申海溝.md "wikilink")[阿留申群島](../Page/阿留申群島.md "wikilink")。

太平洋板块内部存在许多的[热点](../Page/热点_\(地质学\).md "wikilink")，大部分[太平洋岛屿都是由这些热点形成的](../Page/太平洋岛屿.md "wikilink")。其中最有名、研究最充分的热点形成了[夏威夷-天皇海山链](../Page/夏威夷-天皇海山链.md "wikilink")。

[Category:板塊](../Category/板塊.md "wikilink")
[Category:夏威夷州自然史](../Category/夏威夷州自然史.md "wikilink")
[Category:北美洲自然史](../Category/北美洲自然史.md "wikilink")
[Category:加利福尼亚州地质](../Category/加利福尼亚州地质.md "wikilink")