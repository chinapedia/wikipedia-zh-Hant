**太庙**是[中國](../Page/中國.md "wikilink")、[越南](../Page/越南.md "wikilink")[皇帝的](../Page/皇帝.md "wikilink")[宗庙](../Page/宗庙.md "wikilink")。太庙在[夏朝时称为](../Page/夏朝.md "wikilink")“世室”，[殷商时称为](../Page/殷商.md "wikilink")“重屋”，[周称为](../Page/周朝.md "wikilink")“明堂”，[秦](../Page/秦朝.md "wikilink")[漢時起稱為](../Page/漢朝.md "wikilink")“太廟”。

最早太庙只是供奉皇帝先祖的地方。后来皇后和功臣的神位在皇帝的批准下也可以被供奉在太庙。而帝王将相的神主被供奉入太庙的仪式则称为[升祔礼](../Page/升祔礼.md "wikilink")。

## 现存太庙

  - 中国：
      - [北京太庙](../Page/北京太庙.md "wikilink")：[明朝和](../Page/明朝.md "wikilink")[清朝的太庙](../Page/清朝.md "wikilink")。
  - 韓國
      - [漢城宗廟](../Page/宗廟_\(首爾\).md "wikilink")：[李氏朝鮮的宗廟](../Page/李氏朝鮮.md "wikilink")。
  - 越南：
      - [顺化太庙](../Page/太庙_\(顺化\).md "wikilink")：越南[阮朝的太庙](../Page/阮朝.md "wikilink")。
      - [李八帝廟](../Page/李八帝廟.md "wikilink")：越南[李朝皇帝宗廟](../Page/李朝.md "wikilink")，位於[北寧省](../Page/北寧省.md "wikilink")。

[Category:宗廟](../Category/宗廟.md "wikilink")