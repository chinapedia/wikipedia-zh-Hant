**太延**（435年正月-440年六月）是[北魏太武帝](../Page/北魏.md "wikilink")[拓跋燾的年號](../Page/拓跋燾.md "wikilink")，歷時5年餘。[北凉政权曾以與太延諧音的太緣作爲年號](../Page/北凉.md "wikilink")。

## 紀年

| 太延                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 435年                           | 436年                           | 437年                           | 438年                           | 439年                           | 440年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元嘉](../Page/元嘉_\(南朝宋文帝\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [泰始](../Page/泰始_\(趙廣\).md "wikilink")（432年正月-437年四月）：[南朝時](../Page/南朝.md "wikilink")[益州領導](../Page/益州.md "wikilink")[趙廣](../Page/趙廣.md "wikilink")、[程道養年号](../Page/程道養.md "wikilink")
      - [建義](../Page/建义_\(杨难当\).md "wikilink")（436年三月-442年五月）：[仇池政权](../Page/仇池.md "wikilink")[楊難當年号](../Page/楊難當.md "wikilink")
      - [太興](../Page/太兴_\(冯弘\).md "wikilink")（431年正月-436年五月）：[北燕政权](../Page/北燕.md "wikilink")[冯弘年号](../Page/冯弘.md "wikilink")
      - [承和](../Page/承和_\(沮渠牧犍\).md "wikilink")（433年四月-439年九月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠牧犍年号](../Page/沮渠牧犍.md "wikilink")
      - [建平](../Page/建平_\(北凉\).md "wikilink")：與[北凉和](../Page/北凉.md "wikilink")[高昌政权有關年号](../Page/高昌.md "wikilink")
      - [太緣](../Page/太緣.md "wikilink")：與[北凉政权有關年号](../Page/北凉.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
  - 施新榮（2003年），《國内20世纪以來的高昌史研究綜述》，歐亞學研究網：<http://www.eurasianhistory.com/data/articles/l02/578.html>，[UTC時間](../Page/UTC.md "wikilink")2006年1月19日
    03:53更新。

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")