**太田站**（）位於[日本](../Page/日本.md "wikilink")[群馬縣](../Page/群馬縣.md "wikilink")[太田市東本町](../Page/太田市.md "wikilink")，是[東武鐵道的](../Page/東武鐵道.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")\[1\]。車站編號是**TI
18**\[2\]。

## 歷史

  - 1909年（[明治](../Page/明治.md "wikilink")42年）2月17日 開業。

## 車站結構

[島式月台](../Page/島式月台.md "wikilink")3面6線的地面車站，1、2號月台與7、8號月台同線，3、4號月台與9、10號月台同線。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 伊勢崎線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/館林站.md" title="wikilink">館林</a>、<a href="../Page/久喜站.md" title="wikilink">久喜</a>、<span style="font-size:small"><a href="https://zh.wikipedia.org/wiki/File:Tobu_Skytree_Line_(TS)_symbol.svg" title="fig:Tobu_Skytree_Line_(TS)_symbol.svg">Tobu_Skytree_Line_(TS)_symbol.svg</a> <a href="../Page/伊勢崎線.md" title="wikilink">東武晴空塔線</a></span> <a href="../Page/北千住站.md" title="wikilink">北千住</a>、<br />
<a href="../Page/東京晴空塔站.md" title="wikilink">東京晴空塔</a>、<a href="../Page/淺草站.md" title="wikilink">淺草方向</a></p></td>
<td><p>與7號月台共線</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>與8號月台共線</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>館林、久喜、<span style="font-size:small"><a href="https://zh.wikipedia.org/wiki/File:Tobu_Skytree_Line_(TS)_symbol.svg" title="fig:Tobu_Skytree_Line_(TS)_symbol.svg">Tobu_Skytree_Line_(TS)_symbol.svg</a> 東武晴空塔線</span> 北千住、<br />
東京晴空塔、淺草方向</p></td>
<td><p>與9號月台共線</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 桐生線</p></td>
<td><p>、<a href="../Page/赤城站.md" title="wikilink">赤城方向</a>（只有特急）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 伊勢崎線</p></td>
<td><p>上行</p></td>
<td><p>館林、久喜、<span style="font-size:small"><a href="https://zh.wikipedia.org/wiki/File:Tobu_Skytree_Line_(TS)_symbol.svg" title="fig:Tobu_Skytree_Line_(TS)_symbol.svg">Tobu_Skytree_Line_(TS)_symbol.svg</a> 東武晴空塔線</span> 北千住、<br />
東京晴空塔、淺草方向</p></td>
<td><p>與10號月台共線</p></td>
</tr>
<tr class="even">
<td><p>下行</p></td>
<td><p><a href="../Page/伊勢崎站.md" title="wikilink">伊勢崎方向</a>（只有特急）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 小泉線</p></td>
<td><p>方向</p></td>
<td><p>部分於6號月台發車</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 桐生線</p></td>
<td><p>新桐生、赤城方向</p></td>
<td><p>部分於5號月台發車</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7、8<br />
9、10</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 伊勢崎線</p></td>
<td><p>下行<br />
（部分上行）</p></td>
<td><p>伊勢崎方向<br />
（部分館林方向）</p></td>
<td><p>與1 - 4號月台共線</p></td>
</tr>
</tbody>
</table>

## 使用情況

1913年（[大正](../Page/大正.md "wikilink")2年）度的年間旅客數為108,331人，一日平均約300人，比館林站的使用者要多\[3\]。

2016年度一日平均上下車人次為**11,505人**\[4\]。這個數值不包括在東武線各線之間轉乘人次。群馬縣內的東武線車站第1位。

近年一日平均上下車人次的推移如下表。

| 年度           | 一日平均上下車人次\[5\] | 來源     |
| ------------ | -------------- | ------ |
| 2001年（平成13年） | 10,173         | \[6\]  |
| 2002年（平成14年） | 9,675          | \[7\]  |
| 2003年（平成15年） | 9,561          | \[8\]  |
| 2004年（平成16年） | 9,610          | \[9\]  |
| 2005年（平成17年） | 9,574          | \[10\] |
| 2006年（平成18年） | 9,799          | \[11\] |
| 2007年（平成19年） | 9,805          | \[12\] |
| 2008年（平成20年） | 9,901          | \[13\] |
| 2009年（平成21年） | 9,540          | \[14\] |
| 2010年（平成22年） | 9,636          | \[15\] |
| 2011年（平成23年） | 9,792          | \[16\] |
| 2012年（平成24年） | 10,329         | \[17\] |
| 2013年（平成25年） | 10,764         | \[18\] |
| 2014年（平成26年） | 10,819         | \[19\] |
| 2015年（平成27年） | 11,256         | \[20\] |
| 2016年（平成28年） | 11,505         |        |

## 可供轉乘的巴士路線

### 東口

南口發着。

  - [朝日巴士](../Page/朝日自動車.md "wikilink")：[熊谷站行](../Page/熊谷站.md "wikilink")

  - ：（市內循環）、（[大泉町](../Page/大泉町.md "wikilink")、[千代田町方面](../Page/千代田町.md "wikilink")）、（熊谷站南口行）、永旺太田購物中心行

  - 桐生 - 羽田空港線：[羽田空港行](../Page/羽田空港.md "wikilink")

  - ：[成田空港行](../Page/成田空港.md "wikilink")

  - ：[名古屋站太閣通口](../Page/名古屋站.md "wikilink")、[京都站八條口](../Page/京都站.md "wikilink")、大阪OCAT行

## 相鄰車站

  - [Tōbu_Tetsudō_Logo.svg](https://zh.wikipedia.org/wiki/File:Tōbu_Tetsudō_Logo.svg "fig:Tōbu_Tetsudō_Logo.svg")
    東武鐵道

<!-- end list -->

  - 特急「」停車站

<!-- end list -->

  -
    [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    伊勢崎線
      -

        區間急行、區間準急（平日只有1班上行列車）

          -
            [韮川](../Page/韮川站.md "wikilink")（TI-17）－**太田（TI-18）**

        普通（細谷方向為只有一人運行的列車）

          -
            韮川（TI-17）－**太田（TI-18）**－[細谷](../Page/細谷站_\(群馬縣\).md "wikilink")（TI-19）
    [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    小泉線、[Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    桐生線
      -

        普通（直通運行）

          -

            （TI-47）（小泉線）－**太田（TI-18）**－（TI-51）（桐生線）

## 參見

  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

## 參考資料

  - 注釋

## 延伸閱讀

  - 『太田市史 通史 近現代』1994年、605 - 606頁

## 外部連結

  - [東武鐵道 太田站](http://www.tobu.co.jp/station/info/1708.html)

[Ota](../Category/日本鐵路車站_O.md "wikilink")
[Category:群馬縣鐵路車站](../Category/群馬縣鐵路車站.md "wikilink")
[Category:伊勢崎線車站](../Category/伊勢崎線車站.md "wikilink")
[Category:桐生線車站](../Category/桐生線車站.md "wikilink")
[Category:小泉線支線車站](../Category/小泉線支線車站.md "wikilink")
[Category:1909年启用的铁路车站](../Category/1909年启用的铁路车站.md "wikilink")
[Category:太田市](../Category/太田市.md "wikilink")
[Category:1909年日本建立](../Category/1909年日本建立.md "wikilink")

1.
2.
3.
4.  [駅情報（乗降人員）| 企業情報 |
    東武鉄道ポータルサイト](http://www.tobu.co.jp/corporation/rail/station_info/)
     2016年1月23日閲覧。

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.