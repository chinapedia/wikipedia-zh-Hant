[TAIYANGGONG_Station_Hall_20130813.jpg](https://zh.wikipedia.org/wiki/File:TAIYANGGONG_Station_Hall_20130813.jpg "fig:TAIYANGGONG_Station_Hall_20130813.jpg")
**太阳宫站**是[北京地铁](../Page/北京地铁.md "wikilink")[10号线的一个车站](../Page/北京地铁10号线.md "wikilink")。

## 位置

太阳宫站位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")[太阳宫中路与](../Page/太阳宫中路.md "wikilink")[太阳宫南街交叉路口](../Page/太阳宫南街.md "wikilink")。因地处东北三环路与东北四环路之间的[太阳宫地区而得名](../Page/太阳宫地区.md "wikilink")。供奉太阳的庙宇称为太阳宫，太阳宫地区原有一座太阳宫，后来成为了地名。

## 车站结构

### 车站楼层

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
<td><p>A—D出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>站厅</p></td>
<td><p>端头厅</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层</strong></p></td>
<td><p>轨道（北）</p></td>
<td><p>列车往方向 <small>（外环）</small></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>轨道（南）</p></td>
<td><p>　列车往方向 <small>（内环）</small> </p></td>
<td></td>
</tr>
</tbody>
</table>

### 车站出口

太阳宫站目前有4个出入口，其中C（东南）口设有出入口直梯。

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>太阳宫中路、太阳宫大街、太阳宫、十字口村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>太阳宫中路、新纪家园、万方景轩、太阳宫公园、太阳星城</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>太阳宫大街、茂华UHN国际村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>北京跃铭医院、和泰园、西坝河小学</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 邻近车站

[Category:北京市朝阳区地铁车站](../Category/北京市朝阳区地铁车站.md "wikilink")
[Category:2008年启用的铁路车站](../Category/2008年启用的铁路车站.md "wikilink")