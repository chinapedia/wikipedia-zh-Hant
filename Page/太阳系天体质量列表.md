这是一张以逆序排列的**太阳系天体质量列表**.
这张表格并不完整因为大量的[小行星并不为人类所知](../Page/小行星.md "wikilink").

表格的顺序与[太阳系天体半径列表是不同的因为有些](../Page/太阳系天体半径列表.md "wikilink")[天体密度较大](../Page/天体.md "wikilink").如[海王星比](../Page/海王星.md "wikilink")[天王星重但体积却比天王星小](../Page/天王星.md "wikilink"),[水星比起](../Page/水星.md "wikilink")[木卫三和](../Page/木卫三.md "wikilink")[土卫六要重的多但体积却小的多](../Page/土卫六.md "wikilink").

|                                                          |                               |
| -------------------------------------------------------- | ----------------------------- |
| [太阳](../Page/太阳.md "wikilink")                           | (1.9891 × 10<sup>30</sup> kg) |
| [木星](../Page/木星.md "wikilink")                           | (1.899 × 10<sup>27</sup> kg)  |
| [土星](../Page/土星.md "wikilink")                           | (5.6846 × 10<sup>26</sup> kg) |
| [海王星](../Page/海王星.md "wikilink")                         | (1.0243 × 10<sup>26</sup> kg) |
| [天王星](../Page/天王星.md "wikilink")                         | (8.6832 × 10<sup>25</sup> kg) |
| [地球](../Page/地球.md "wikilink")                           | (5.9736 × 10<sup>24</sup> kg) |
| [金星](../Page/金星.md "wikilink")                           | (4.8685 × 10<sup>24</sup> kg) |
| [火星](../Page/火星.md "wikilink")                           | (6.4185 × 10<sup>23</sup> kg) |
| [水星](../Page/水星.md "wikilink")                           | (3.302 × 10<sup>23</sup> kg)  |
| [木卫三](../Page/木卫三.md "wikilink")                         | (1.482 × 10<sup>23</sup> kg)  |
| [土卫六](../Page/土卫六.md "wikilink")                         | (1.345 × 10<sup>23</sup> kg)  |
| [木卫四](../Page/木卫四.md "wikilink")                         | (1.076 × 10<sup>23</sup> kg)  |
| [木卫一](../Page/木卫一.md "wikilink")                         | (8.94 × 10<sup>22</sup> kg)   |
| [月球](../Page/月球.md "wikilink")                           | (7.349 × 10<sup>22</sup> kg)  |
| [木卫二](../Page/木卫二.md "wikilink")                         | (4.8 × 10<sup>22</sup> kg)    |
| [海卫一](../Page/海卫一.md "wikilink")                         | (2.147 × 10<sup>22</sup> kg)  |
| [阋神星](../Page/阋神星.md "wikilink")                         | (1.67 × 10<sup>22</sup> kg)   |
| [冥王星](../Page/冥王星.md "wikilink")                         | (1.25 × 10<sup>22</sup> kg)   |
| [妊神星](../Page/妊神星.md "wikilink")                         | (4.006 × 10<sup>21</sup> kg)  |
| [天卫三](../Page/天卫三.md "wikilink")                         | (3.526 × 10<sup>21</sup> kg)  |
| [天卫四](../Page/天卫四.md "wikilink")                         | (3.014 × 10<sup>21</sup> kg)  |
| [土卫五](../Page/土卫五.md "wikilink")                         | (2.3166 × 10<sup>21</sup> kg) |
| [土卫八](../Page/土卫八.md "wikilink")                         | (1.9739 × 10<sup>21</sup> kg) |
| [冥卫一](../Page/冥卫一.md "wikilink")                         | (1.90 × 10<sup>21</sup> kg)   |
| [土卫四](../Page/土卫四.md "wikilink")                         | (1.096 × 10<sup>21</sup> kg)  |
| 小行星1[谷神星](../Page/谷神星.md "wikilink")                     | (8.7 × 10<sup>20</sup> kg)    |
| [土卫三](../Page/土卫三.md "wikilink")                         | (6.176 × 10<sup>20</sup> kg)  |
| [小行星20000Varuna](../Page/小行星20000.md "wikilink")         | (4 × 10<sup>20</sup> kg)      |
| 小行星2[智神星](../Page/智神星.md "wikilink")                     | (3.18 × 10<sup>20</sup> kg)   |
| 小行星4[灶神星](../Page/灶神星.md "wikilink")                     | (3.0 × 10<sup>20</sup> kg)    |
| [土卫二](../Page/土卫二.md "wikilink")                         | (8.6 × 10<sup>19</sup> kg)    |
| [天卫五](../Page/天卫五.md "wikilink")                         | (6.59 × 10<sup>19</sup> kg)   |
| [土卫一](../Page/土卫一.md "wikilink")                         | (3.84 × 10<sup>19</sup> kg)   |
| 小行星3[婚神星](../Page/婚神星.md "wikilink")                     | (2.0 × 10<sup>19</sup> kg)    |
| [土卫七](../Page/土卫七.md "wikilink")                         | (1.08 × 10<sup>19</sup> kg)   |
| [木卫七](../Page/木卫七.md "wikilink")                         | (8.7 × 10<sup>18</sup> kg)    |
| [木卫五](../Page/木卫五.md "wikilink")                         | (7.43 × 10<sup>18</sup> kg)   |
| [土卫九](../Page/土卫九.md "wikilink")                         | (7.2 × 10<sup>18</sup> kg)    |
| [木卫六](../Page/木卫六.md "wikilink")                         | (6.74 × 10<sup>18</sup> kg)   |
| [小行星45Eugenia](../Page/小行星45.md "wikilink")              | (6.1 × 10<sup>18</sup> kg)    |
| [小行星2060Chiron](../Page/小行星2060.md "wikilink")           | (4 × 10<sup>18</sup> kg)      |
| [Pasiphaë](../Page/Pasiphaë_\(moon\).md "wikilink")      | (3.1 × 10<sup>18</sup> kg)    |
| [Puck](../Page/Puck_\(moon\).md "wikilink")              | (2.89 × 10<sup>18</sup> kg)   |
| [土卫十](../Page/土卫十.md "wikilink")                         | (1.98 × 10<sup>18</sup> kg)   |
| [小行星140Siwa](../Page/小行星140.md "wikilink")               | (1.5 × 10<sup>18</sup> kg)    |
| [Thebe](../Page/Thebe_\(moon\).md "wikilink")            | (7.557 × 10<sup>17</sup> kg)  |
| [Caliban](../Page/Caliban_\(moon\).md "wikilink")        | (7.3 × 10<sup>17</sup> kg)    |
| [Juliet](../Page/Juliet_\(moon\).md "wikilink")          | (5.57 × 10<sup>17</sup> kg)   |
| [Epimetheus](../Page/Epimetheus_\(moon\).md "wikilink")  | (5.35 × 10<sup>17</sup> kg)   |
| [Belinda](../Page/Belinda_\(moon\).md "wikilink")        | (3.57 × 10<sup>17</sup> kg)   |
| [Cressida](../Page/Cressida_\(moon\).md "wikilink")      | (3.43 × 10<sup>17</sup> kg)   |
| [Prometheus](../Page/Prometheus_\(moon\).md "wikilink")  | (3.3 × 10<sup>17</sup> kg)    |
| [Rosalind](../Page/Rosalind_\(moon\).md "wikilink")      | (2.54 × 10<sup>17</sup> kg)   |
| [Pandora](../Page/Pandora_\(moon\).md "wikilink")        | (1.94 × 10<sup>17</sup> kg)   |
| [Desdemona](../Page/Desdemona_\(moon\).md "wikilink")    | (1.78 × 10<sup>17</sup> kg)   |
| [Carme](../Page/Carme_\(moon\).md "wikilink")            | (1.3 × 10<sup>17</sup> kg)    |
| [253 Mathilde](../Page/253_Mathilde.md "wikilink")       | (1.033 × 10<sup>17</sup> kg)  |
| [243 Ida](../Page/243_Ida.md "wikilink")                 | (1 × 10<sup>17</sup> kg)      |
| [Metis](../Page/Metis_\(moon\).md "wikilink")            | (9.5467 × 10<sup>16</sup> kg) |
| [Bianca](../Page/Bianca_\(moon\).md "wikilink")          | (9.3 × 10<sup>16</sup> kg)    |
| [Sinope](../Page/Sinope_\(moon\).md "wikilink")          | (7.6 × 10<sup>16</sup> kg)    |
| [Lysithea](../Page/Lysithea_\(moon\).md "wikilink")      | (6.3 × 10<sup>16</sup> kg)    |
| [Ophelia](../Page/Ophelia_\(moon\).md "wikilink")        | (5.4 × 10<sup>16</sup> kg)    |
| [Cordelia](../Page/Cordelia_\(moon\).md "wikilink")      | (4.5 × 10<sup>16</sup> kg)    |
| [Ananke](../Page/Ananke_\(moon\).md "wikilink")          | (3.82 × 10<sup>16</sup> kg)   |
| [Adrastea](../Page/Adrastea_\(moon\).md "wikilink")      | (1.8894 × 10<sup>16</sup> kg) |
| [Leda](../Page/Leda_\(moon\).md "wikilink")              | (1.09 × 10<sup>16</sup> kg)   |
| [Phobos](../Page/Phobos_\(moon\).md "wikilink")          | (1.08 × 10<sup>16</sup> kg)   |
| [951 Gaspra](../Page/951_Gaspra.md "wikilink")           | (1.0 × 10<sup>16</sup> kg)    |
| [24 Themis](../Page/24_Themis.md "wikilink")             | (7.2 × 10<sup>15</sup> kg)    |
| [433 Eros](../Page/433_Eros.md "wikilink")               | (7.2 × 10<sup>15</sup> kg)    |
| [Pan](../Page/Pan_\(moon\).md "wikilink")                | (2.7 × 10<sup>15</sup> kg)    |
| [Deimos](../Page/Deimos_\(moon\).md "wikilink")          | (1.8 × 10<sup>15</sup> kg)    |
| [4979 Otawara](../Page/4979_Otawara.md "wikilink")       | (2 × 10<sup>14</sup> kg)      |
| [4179 Toutatis](../Page/4179_Toutatis.md "wikilink")     | (5.0 × 10<sup>13</sup> kg)    |
| [1620 Geographos](../Page/1620_Geographos.md "wikilink") | (4 × 10<sup>12</sup> kg)      |
| [1862 Apollo](../Page/1862_Apollo.md "wikilink")         | (2 × 10<sup>12</sup> kg)      |
| [1566 Icarus](../Page/1566_Icarus.md "wikilink")         | (1 × 10<sup>12</sup> kg)      |
| [4769 Castalia](../Page/4769_Castalia.md "wikilink")     | (5.0 × 10<sup>11</sup> kg)    |
|                                                          |                               |

表中的许多数值一部分来直接自于来自相关的英文[维基百科文章](../Page/维基百科.md "wikilink").其他的来自[这张行星数据表](http://nssdc.gsfc.nasa.gov/planetary/planetfact.html).
[小行星20000Varuna的质量根据](../Page/小行星20000.md "wikilink")[这里](http://www.ifa.hawaii.edu/faculty/jewitt/varuna.html)估计出其直径与密度的.一些大[天体](../Page/天体.md "wikilink")([2003
EL61](../Page/2003_EL61.md "wikilink")、[2003
UB313](../Page/2003_UB313.md "wikilink")、[小行星31](../Page/小行星31.md "wikilink")(Euphrosyne)、[小行星65](../Page/小行星65.md "wikilink")(Cybele)、[小行星52](../Page/小行星52.md "wikilink")(歐羅巴)、[小行星90377](../Page/小行星90377.md "wikilink")(塞德娜)、[小行星28978](../Page/小行星28978.md "wikilink")(Ixion)、[小行星50000](../Page/小行星50000.md "wikilink")(Quaoar)以及[小行星90482](../Page/小行星90482.md "wikilink")(Orcus)没有在表格中列出因为它们还没有被仔细研究过。

## 参见

  - [太阳系天体半径列表](../Page/太阳系天体半径列表.md "wikilink")

[Category:太阳系](../Category/太阳系.md "wikilink")