[英國](../Page/英國.md "wikilink")《**太陽報**》（*The
Sun*）是富商[默多克擁有的](../Page/默多克.md "wikilink")[新聞集團](../Page/新聞集團.md "wikilink")（News
Corporation）旗下的一份[小開型日報](../Page/小開型日報.md "wikilink")。《太陽報》是全英國銷量最高的報章，2004年後期該報的每日發行量達320萬份。星期日版的《太陽報》名為《周日太陽報》（The
Sun On Sunday），在2011年以前為《[世界新聞報](../Page/世界新聞報.md "wikilink")》（*News of
the World*）。

太陽報銷量、知名度高，有調查指在[ABC1類高級讀者群](http://www.businessballs.com/demographicsclassifications.htm)之中，《太陽報》讀者比同集團的知識型報章《[泰晤士報](../Page/泰晤士報.md "wikilink")》高出一倍。

## 歷史

1964年，《太陽報》為取代被賣盤的《》（*Daily
Herald*）而創辦。《太陽報》初期銷量不好，於是被轉售予傳媒大亨梅鐸。該報正於此時轉為小報型報刊。《太陽報》轉型後加入大量的通俗內容，不過仍然維持親[工黨的立場](../Page/英国工党.md "wikilink")。該報因而銷量激增，1970年起更推出著名的「[三版女郎](../Page/三版女郎.md "wikilink")」（Page
3
Girl）。第三頁女郎原先只是著名[模特兒的寫真](../Page/模特兒.md "wikilink")，後來形式轉為每天登場的上身裸女。在撒切尔执政时期，《太阳报》为亲保守党报刊，曾批评当时权力过大的工会。
1997年～2007年支持[東尼·布萊爾的](../Page/東尼·布萊爾.md "wikilink")[新工黨路線](../Page/新工黨.md "wikilink")，2007年[戈登·布朗接任首相後](../Page/戈登·布朗.md "wikilink")，逐漸回到親[保守黨的立場](../Page/保守黨_\(英國\).md "wikilink")。

## 編寫手法

批評《太陽報》的人主要針對報內大量的低俗、煽情的內容和不專業的編寫手法，以及政治立場偏重中下阶层[保守派民众的喜好](../Page/保守派.md "wikilink")。《太陽報》曾多次把英國內外的[國家元首](../Page/國家元首.md "wikilink")、大事拿來開玩笑。例如在[福克蘭戰爭步入尾聲的時候](../Page/福克蘭戰爭.md "wikilink")，[阿根廷军舰](../Page/阿根廷.md "wikilink")[贝尔格拉诺将军号被擊沉](../Page/貝爾格拉諾將軍號巡洋艦.md "wikilink")，該報把頭條寫作「打中了！（Gotcha\!）」《太陽報》對[美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")、反[歐洲一體化](../Page/歐盟.md "wikilink")、反[法國](../Page/法國.md "wikilink")[德國](../Page/德國.md "wikilink")、反[新移民的立場也十分鮮明](../Page/新移民.md "wikilink")。[英軍總是被](../Page/英軍.md "wikilink")《太陽報》暱稱為「咱們的阿兵哥」（our
boys）。而[伊拉克戰爭初期](../Page/伊拉克戰爭.md "wikilink")，《太陽報》更曾把反戰的[法國總統](../Page/法國總統.md "wikilink")[希拉克謔稱為](../Page/希拉克.md "wikilink")「[蚯蚓](../Page/蚯蚓.md "wikilink")」（le
Worm），然後專程把當天的《太陽報》譯成[法語在](../Page/法語.md "wikilink")[巴黎街頭派發](../Page/巴黎.md "wikilink")。

## 相关事件

  - 1989年，《太陽報》臭名昭著的“The
    Truth”頭版，虛構了[利物浦球迷在](../Page/利物浦.md "wikilink")[希爾斯堡慘劇中偷盜死者財物](../Page/希爾斯堡慘劇.md "wikilink")，阻止救援等行為。其後《太陽報》開始被禁止專訪[利物浦俱樂部主教練或者球員](../Page/利物浦足球俱乐部.md "wikilink")，但可以報導比賽和參加新聞發佈會。

<!-- end list -->

  - 2012年1月7日，时任[英国广播公司电视汽车节目](../Page/英国广播公司.md "wikilink")《[Top
    Gear](../Page/Top_Gear.md "wikilink")》主持人[杰里米·克拉克森](../Page/杰里米·克拉克森.md "wikilink")（Jeremy
    Clarkson）在报上刊文，称“‘[花样游泳](../Page/花样游泳.md "wikilink")’只是一些在水中头朝下、带着帽子的中国女人，你可以在莫克姆海滩免费看到这些”影射[拾貝慘案中不幸溺死的中国婦女](../Page/拾貝慘案.md "wikilink")，此举引来了中方的不满。中国驻英使馆发言人要求英方表态，“纠正错误”。
    \[1\]

<!-- end list -->

  - 2017年2月11日。[利物浦俱樂部決定](../Page/利物浦足球俱乐部.md "wikilink")，禁止《太陽報》記者進入安菲爾德球場和梅爾伍德訓練基地。

## 大選支持政黨

| 大選年份                                             | 支持政黨 |
| ------------------------------------------------ | ---- |
| [1966年大選](../Page/1966年英國大選.md "wikilink")       |      |
| [1970年大選](../Page/1970年英國大選.md "wikilink")       |      |
| [1974年2月大選](../Page/1974年2月英國大選.md "wikilink")   |      |
| [1974年10月大選](../Page/1974年10月英國大選.md "wikilink") |      |
| [1979年大選](../Page/1979年英國大選.md "wikilink")       |      |
| [1983年大選](../Page/1983年英國大選.md "wikilink")       |      |
| [1987年大選](../Page/1987年英國大選.md "wikilink")       |      |
| [1992年大選](../Page/1992年英國大選.md "wikilink")       |      |
| [1997年大選](../Page/1997年英國大選.md "wikilink")       |      |
| [2001年大選](../Page/2001年英國大選.md "wikilink")       |      |
| [2005年大選](../Page/2005年英國大選.md "wikilink")       |      |
| [2010年大選](../Page/2010年英國大選.md "wikilink")       |      |
| [2015年大選](../Page/2015年英國大選.md "wikilink")       |      |
| [2017年大選](../Page/2017年英國大選.md "wikilink")       |      |

## 外部連結

  - [英國《太陽報》網站](http://www.thesun.co.uk)

## 参考文献

[Category:1964年建立](../Category/1964年建立.md "wikilink")
[Category:新聞集團](../Category/新聞集團.md "wikilink")
[Category:英國全國性報紙](../Category/英國全國性報紙.md "wikilink")

1.