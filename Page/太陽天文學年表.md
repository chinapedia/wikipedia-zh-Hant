這是**[太陽](../Page/太陽.md "wikilink")**的**[天文學](../Page/天文學.md "wikilink")[年表](../Page/年表.md "wikilink")**，記錄人類有關太陽的發現。

## 9世紀

  - 850年─（Ahmad ibn Muhammad ibn Kathīr
    al-Farghānī）得到[黃道傾角的值](../Page/黃道.md "wikilink")，和[太陽](../Page/太陽.md "wikilink")[遠地點的進動運動](../Page/拱點.md "wikilink")。

## 10世紀

  - 900–929年─[巴塔尼](../Page/巴塔尼.md "wikilink")（Muhammad ibn Jābir
    al-Harrānī al-Battānī）發現太陽[離心率方向的變化](../Page/軌道離心率.md "wikilink")。
  - 950–1000年─使用直徑約1.4米的[星盤](../Page/星盤.md "wikilink")，花了許多年的時間測量了超過10,000次的太陽位置。

## 11世紀

  - 1031年─[比魯尼在他的著作](../Page/比魯尼.md "wikilink")*Canon
    Mas'udicus*中計算出地球和太陽的距離。

## 17世紀

  - [1613年](../Page/1613年.md "wikilink")─[伽利略使用](../Page/伽利略.md "wikilink")[太陽黑子的運動證明](../Page/太陽黑子.md "wikilink")[太陽自轉](../Page/太陽.md "wikilink")。
  - [1619年](../Page/1619年.md "wikilink")─[約翰·刻卜勒主張有](../Page/約翰內斯·克卜勒.md "wikilink")[太陽風](../Page/太陽風.md "wikilink")，來解釋[彗星尾巴背向太陽的方向](../Page/彗星.md "wikilink")。

## 19世紀

  - [1802年](../Page/1802年.md "wikilink")─觀察到太陽[光譜中的暗線](../Page/電磁頻譜.md "wikilink")。
  - [1814年](../Page/1814年.md "wikilink")─[約瑟夫·夫朗和斐有系統的研究太陽光譜中的暗線](../Page/約瑟夫·夫朗和斐.md "wikilink")（參見[夫朗和斐譜線](../Page/夫朗和斐譜線.md "wikilink")）。
  - [1834年](../Page/1834年.md "wikilink")─[赫爾曼·馮·亥姆霍茲提出](../Page/赫爾曼·馮·亥姆霍茲.md "wikilink")[重力的收縮作為太陽能量的來源](../Page/重力.md "wikilink")。
  - [1843年](../Page/1843年.md "wikilink")─[海涅·施瓦布宣布他發現](../Page/海因利希·史瓦貝.md "wikilink")[太陽黑子的](../Page/太陽黑子.md "wikilink")[循環周期大約為十年](../Page/太陽活動.md "wikilink")。
  - [1852年](../Page/1852年.md "wikilink")─表示，太陽黑子的數量與[地磁變動有關](../Page/地磁.md "wikilink")。
  - [1859年](../Page/1859年.md "wikilink")─[理查·卡靈頓發現](../Page/理查·克里斯多福·卡林頓.md "wikilink")[太陽耀斑](../Page/耀斑.md "wikilink")（閃焰）。
  - [1860年](../Page/1860年.md "wikilink")─[古斯塔夫·基爾霍夫和](../Page/古斯塔夫·羅伯特·基爾霍夫.md "wikilink")[羅伯特·本生發現每一種化學元素都有自己獨特的](../Page/羅伯特·威廉·本生.md "wikilink")[譜線](../Page/譜線.md "wikilink")，並且可以用來解釋太陽光譜中的暗線。
  - [1861年](../Page/1861年.md "wikilink")─[古斯塔夫·史波勒發現太陽黑子在周期中的緯度變化](../Page/古斯塔夫·史波勒.md "wikilink")，也就是所知的[史波勒定律](../Page/史波勒定律.md "wikilink")（Spörer's
    law）。
  - [1863年](../Page/1863年.md "wikilink")─[理查·卡靈頓發現太陽的微差轉動](../Page/理查·克里斯多福·卡林頓.md "wikilink")（不同緯度的自轉週期不同）。
  - [1868年](../Page/1868年.md "wikilink")─[皮埃爾·讓森和](../Page/皮埃爾·讓森.md "wikilink")發現[日珥中不明的黃色譜線](../Page/日珥.md "wikilink")，認為是一種未知的元素，現已知道是[氦元素造成](../Page/氦.md "wikilink")。
  - [1893年](../Page/1893年.md "wikilink")─[愛德華·蒙德發現從](../Page/愛德華·沃爾特·蒙德.md "wikilink")[1645年](../Page/1645年.md "wikilink")─[1715年太陽黑子異常的減少](../Page/1715年.md "wikilink")，現在稱為黑子的[蒙德極小期](../Page/蒙德極小期.md "wikilink")。

## 20世紀

  - [1904年](../Page/1904年.md "wikilink")─[愛德華·蒙德首度繪出](../Page/愛德華·沃爾特·蒙德.md "wikilink")[太陽黑子的](../Page/太陽黑子.md "wikilink")「蝴蝶圖」。
  - [1906年](../Page/1906年.md "wikilink")─[卡爾·史瓦西解釋太陽邊緣昏暗的原因](../Page/卡爾·史瓦西.md "wikilink")。
  - [1908年](../Page/1908年.md "wikilink")─[喬治·海爾發現](../Page/喬治·海爾.md "wikilink")[太陽黑子譜線中的](../Page/太陽黑子.md "wikilink")[則曼裂線](../Page/塞曼效應.md "wikilink")。
  - [1929年](../Page/1929年.md "wikilink")─[伯納德·李奧發明](../Page/伯納德·李奧.md "wikilink")[日冕儀](../Page/日冕儀.md "wikilink")，並利用人造的[日食觀察](../Page/日食.md "wikilink")[日冕](../Page/日冕.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")─[詹姆斯·史坦利·海伊檢測出來自太陽的](../Page/詹姆斯·史坦利·海伊.md "wikilink")[無線電波](../Page/無線電波.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")─[赫爾伯特·傅利曼檢測出來自太陽的](../Page/赫爾伯特·傅利曼.md "wikilink")[X射線](../Page/X射線.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")─[羅伯特·萊頓](../Page/羅伯·雷頓.md "wikilink")、[羅伯特·諾伊斯和](../Page/羅伯特·諾伊斯.md "wikilink")[喬治·西蒙在觀測太陽暗線的](../Page/喬治·西蒙.md "wikilink")[都卜勒位移時發現太陽的五分鐘振盪](../Page/都卜勒效應.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")─提出磁螺線管的黑子理論。
  - [1970年](../Page/1970年.md "wikilink")─[羅傑·歐路奇](../Page/羅傑·歐路奇.md "wikilink")、[約翰賴巴契和](../Page/約翰賴巴契.md "wikilink")[羅伯特·斯坦從理論的太陽模型推論出太陽的內部可能有](../Page/羅伯特·斯坦.md "wikilink")[共振的](../Page/共鳴.md "wikilink")[音響洞](../Page/音響洞.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")─[法郎茲·路德維格](../Page/法郎茲·路德維格.md "wikilink")（Franz-Ludwig
    Deubner）首度精確的測量出太陽五分鐘振盪的週期和水平方向的波長。
  - [1981年](../Page/1981年.md "wikilink")─[NASA從數據中檢索出](../Page/NASA.md "wikilink")1978年有彗星撞入太陽。

## 21世紀

  - [2004年](../Page/2004年.md "wikilink")─發生迄今紀錄到的最大的和第四大的[耀斑](../Page/耀斑.md "wikilink")。

[太陽天文學年表](../Category/天文學年表.md "wikilink")