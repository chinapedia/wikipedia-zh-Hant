這是**[太陽系的](../Page/太陽系.md "wikilink")[天文學](../Page/天文學.md "wikilink")[年表](../Page/年代學.md "wikilink")**，列出人類對太陽系的主要發現與研究成果。

## 古代

  - [前2137年](../Page/前21世紀.md "wikilink")10月22日─中國天文學家記錄[日食](../Page/日食.md "wikilink")。
  - 約公元[前2000年](../Page/前2000年.md "wikilink")─中國測量出木星繞天球一周約為12年。
  - [前1000s
    年](../Page/2nd_millennium_BC.md "wikilink")─[太陽系以](../Page/太陽系.md "wikilink")[太陽為中心的想法開始萌芽](../Page/太陽.md "wikilink")，最早提到太陽在球形中心的是[古印度的經書](../Page/印度歷史.md "wikilink")《韋達經》（意為知識或光明）。
  - 約公元[前1400年](../Page/前1400年.md "wikilink")─[中國已有規律的記錄日食與](../Page/中國.md "wikilink")[月食](../Page/月食.md "wikilink")，並有最早的[日珥記錄](../Page/日珥.md "wikilink")。
  - 約公元[前1100年](../Page/前1100年.md "wikilink")─中國最早測出[春分點與](../Page/春分點.md "wikilink")[黃赤交角](../Page/黃赤交角.md "wikilink")。
  - [前776年](../Page/前776年.md "wikilink")─中國完成世界最早可信的[日食記錄](../Page/日食.md "wikilink")。
  - [前600年](../Page/前7世紀.md "wikilink")─[埃及天文學家據稱能預測日食](../Page/埃及.md "wikilink")。
  - [前613年](../Page/前613年.md "wikilink")7月─[中國古籍](../Page/中國.md "wikilink")《春秋》記錄的[彗星](../Page/彗星.md "wikilink")，可能是[哈雷彗星最早的出現記錄](../Page/哈雷彗星.md "wikilink")。
  - [前586年](../Page/前586年.md "wikilink")─古希臘[米粒都的](../Page/米粒都.md "wikilink")[泰勒斯宣稱能預測日食](../Page/泰勒斯.md "wikilink")。
  - [前350年](../Page/前350年.md "wikilink")─古希臘[亞里士多德以月食的陰影和其他的觀測](../Page/亞里士多德.md "wikilink")，論述[地球是圓的](../Page/地球.md "wikilink")。
  - [前280年](../Page/前280年.md "wikilink")─古希臘[薩摩斯的](../Page/薩摩斯.md "wikilink")[阿里斯塔克斯利用地球投射在月球上的影子估計月球的半徑是地球的三分之一](../Page/阿里斯塔克斯.md "wikilink")，並且估計月球和太陽的大小與距離。
  - [前200年](../Page/前200年.md "wikilink")─[厄拉多塞使用陰影測量出地球的半徑約為](../Page/埃拉托斯特尼.md "wikilink")6,400公里。
  - [前150年](../Page/前150年.md "wikilink")─[喜帕恰斯利用](../Page/喜帕恰斯.md "wikilink")[視差測量出月球的距離約為](../Page/視差.md "wikilink")380,000公里。
  - [前134年](../Page/前134年.md "wikilink")─喜帕恰斯發現[分點歲差](../Page/分點歲差.md "wikilink")（分點進動）。
  - [前28年](../Page/前28年.md "wikilink")─[中國](../Page/中國.md "wikilink")《[漢書](../Page/漢書.md "wikilink")》最早記錄[黑子](../Page/太陽黑子.md "wikilink")。

## 中世紀時期

  - 前[150
    CE](../Page/150.md "wikilink")─[托勒密完成](../Page/托勒密.md "wikilink")《[天文學大成](../Page/天文學大成.md "wikilink")》，記錄當時的天文學知識，並且鞏固以地球為中心的太陽系模型。
  - [499
    CE](../Page/499.md "wikilink")─[印度的](../Page/印度.md "wikilink")[數學家](../Page/數學家.md "wikilink")[阿耶波多](../Page/阿耶波多.md "wikilink")（Aryabhata）在阿耶波多曆書（*Aryabhatiya*）中建議以太陽為中心，在[萬有引力下的太陽系模型](../Page/萬有引力.md "wikilink")，行星以橢圓軌道的週轉圓繞著太陽，行星和月亮的光輝來自反射的陽光。
  - [500年](../Page/500年.md "wikilink")─阿耶波多精確算出地球的[圓周](../Page/圓周.md "wikilink")、[日食和](../Page/日食.md "wikilink")[月食](../Page/月食.md "wikilink")、地球公轉軌道的長度。
  - [620年代](../Page/620年代.md "wikilink")─[印度數學](../Page/印度.md "wikilink")、天文學家[婆羅門笈多首先認同重力是相吸的](../Page/婆羅門笈多.md "wikilink")，並且對[萬有引力定律作簡要描述](../Page/萬有引力定律.md "wikilink")。
  - [628年](../Page/628年.md "wikilink")─婆羅門笈多提出行星出沒、合、和日食與月食等變化的計算方法。
  - [687年](../Page/687年.md "wikilink")─中國紀錄已知最早的[流星雨](../Page/流星雨.md "wikilink")。
  - [820年](../Page/820年.md "wikilink")─阿拉伯天文學家[花拉子密運用阿拉伯數字完成天文算表](../Page/花拉子密.md "wikilink")。
  - [1150年](../Page/1150年.md "wikilink")─印度數學、天文學家[婆什迦罗在](../Page/婆什迦罗.md "wikilink")《Siddhanta
    Shiromani》以[代數計算行星](../Page/代數.md "wikilink")[經度和](../Page/經度.md "wikilink")[緯度](../Page/緯度.md "wikilink")、日食和月食、天體出沒、月球的[新月和朔望](../Page/新月.md "wikilink")，還有行星合與恆星的合；並且解釋三個[周日運動的問題](../Page/周日運動.md "wikilink")。
  - [1150年代](../Page/1150年代.md "wikilink")─婆什迦罗計算行星的[平均運動](../Page/平均運動.md "wikilink")、橢圓、新月、行星首見的時間、[季節](../Page/季節.md "wikilink")，和地球繞太陽公轉軌道的長度至小數點下第9位。
  - [1150年代](../Page/1150年代.md "wikilink")─[傑拉德將](../Page/傑拉德\(克雷莫納的\).md "wikilink")[托勒密的](../Page/托勒密.md "wikilink")《[天文學大成](../Page/天文學大成.md "wikilink")》從[阿拉伯文翻譯成](../Page/阿拉伯文.md "wikilink")[拉丁文](../Page/拉丁文.md "wikilink")，最後成為教會核准教本。
  - 約[1350年](../Page/1350年.md "wikilink")─阿拉伯天文學家[伊本·沙蒂爾](../Page/伊本·沙蒂爾.md "wikilink")（Ibn
    al-Shatir）在行星的計算中，早於哥白尼，已經摒棄[托勒密的](../Page/托勒密.md "wikilink")[等分點](../Page/等分點.md "wikilink")。

## 文藝復興時代

  - 約[1514年](../Page/1514年.md "wikilink")─[尼古拉·哥白尼在一篇](../Page/尼古拉·哥白尼.md "wikilink")《短論》的手稿中陳述他以太陽為中心的理論。
  - [1543年](../Page/1543年.md "wikilink")─[哥白尼倡議](../Page/哥白尼.md "wikilink")[日心說的著作](../Page/日心說.md "wikilink")《[天體運行論](../Page/天體運行論.md "wikilink")》（De
    revolutionibus orbium coelestium）出版。
  - [1577年](../Page/1577年.md "wikilink")─[第谷以](../Page/第谷·布拉赫.md "wikilink")[視差證明](../Page/視差.md "wikilink")[彗星是地球大氣層外的遙遠天體](../Page/彗星.md "wikilink")。
  - [1609年](../Page/1609年.md "wikilink")─[约翰内斯·开普勒提出行星軌道是橢圓形而不是圓型的行星運動第一定律](../Page/约翰内斯·开普勒.md "wikilink")，解決許多古代行星模型的問題。
  - [1610年](../Page/1610年.md "wikilink")─[伽利略發現木星的四大衛星](../Page/伽利略·伽利莱.md "wikilink")：[埃歐](../Page/木衛一.md "wikilink")、[歐羅巴](../Page/木衛二.md "wikilink")、[甘尼米德](../Page/木衛三.md "wikilink")、[卡利斯多](../Page/木衛四.md "wikilink")，看見土星環（但不認為是圓環），還觀察了[金星的](../Page/金星.md "wikilink")[相位並據以駁斥托勒密的](../Page/相位.md "wikilink")[地心說](../Page/地心說.md "wikilink")。
  - [1619年](../Page/1619年.md "wikilink")─开普勒提出他的行星運動第三定律。
  - [1655年](../Page/1655年.md "wikilink")─[卡西尼發現](../Page/乔凡尼·多美尼科·卡西尼.md "wikilink")[木星的大紅斑](../Page/木星.md "wikilink")。
  - [1656年](../Page/1656年.md "wikilink")─[惠更斯確認土星環和發現](../Page/克里斯蒂安·惠更斯.md "wikilink")[土衛六](../Page/土衛六.md "wikilink")。
  - [1665年](../Page/1665年.md "wikilink")─卡西尼測量[木星](../Page/木星.md "wikilink")、[火星和](../Page/火星.md "wikilink")[金星的自轉速度](../Page/金星.md "wikilink")。
  - [1672年](../Page/1672年.md "wikilink")─卡西尼發現[土衛五](../Page/土衛五.md "wikilink")。
  - [1672年](../Page/1672年.md "wikilink")─[吉恩·里奇和卡西尼測出](../Page/吉恩·里奇.md "wikilink")[天文單位大約是](../Page/天文單位.md "wikilink")138,370,000公里。
  - [1675年](../Page/1675年.md "wikilink")─[奧勒·羅默使用木衛掩週期的規律性變化](../Page/奧勒·羅默.md "wikilink")，最先[證實光波以有限速度傳播](../Page/羅默測定光速.md "wikilink")，並且估計[光速大約是每秒](../Page/光速.md "wikilink")227,000公里。

## 18世紀

  - [1705年](../Page/1705年.md "wikilink")─[愛德蒙·哈雷計算出](../Page/愛德蒙·哈雷.md "wikilink")[哈雷彗星的軌道週期](../Page/哈雷彗星.md "wikilink")，並公開預言將在1757年回歸。
  - [1715年](../Page/1715年.md "wikilink")─愛德蒙·哈雷計算日食陰影的路徑。
  - [1716年](../Page/1716年.md "wikilink")─愛德蒙·哈雷提出利用[金星凌日精確測量日地距離的方法](../Page/金星凌日.md "wikilink")。
  - [1729年](../Page/1729年.md "wikilink")─[詹姆斯·布萊德雷測出](../Page/詹姆斯·布萊德雷.md "wikilink")[光行差](../Page/光行差.md "wikilink")，首度直接證明地球在運動。
  - [1755年](../Page/1755年.md "wikilink")─[康德首度提出太陽系誕生的](../Page/伊曼努尔·康德.md "wikilink")[星雲說](../Page/康德-拉普拉斯假設.md "wikilink")。
  - [1758年](../Page/1758年.md "wikilink")─[約翰·帕利奇觀測到回歸的哈雷彗星](../Page/約翰·帕利奇.md "wikilink")。由於木星[攝動使回歸日期延後](../Page/攝動.md "wikilink")618天，法國天文學家[拉卡依建議將其命名為](../Page/拉卡依.md "wikilink")[哈雷彗星](../Page/哈雷彗星.md "wikilink")。
  - [1766年](../Page/1766年.md "wikilink")─德國人[提丟斯發現行星距離的規律性質](../Page/提丟斯.md "wikilink")。
  - [1772年](../Page/1772年.md "wikilink")─[波德獨立發表與](../Page/波德.md "wikilink")[提丟斯雷同之行星距離規律性質](../Page/提丟斯.md "wikilink")，此定則命名為「提丟斯─波德定則」。
  - [1781年](../Page/1781年.md "wikilink")─[威廉·赫歇爾在望遠鏡的北天巡天中發現](../Page/威廉·赫歇爾.md "wikilink")[天王星](../Page/天王星.md "wikilink")。
  - [1796年](../Page/1796年.md "wikilink")─[拉普拉斯再提出太陽系是從旋轉中的](../Page/皮埃尔-西蒙·拉普拉斯.md "wikilink")[星雲誕生的星雲假說](../Page/星雲.md "wikilink")。

## 19世紀

  - [1801年](../Page/1801年.md "wikilink")─[朱塞普·皮亞齊發現第一顆小行星](../Page/朱塞普·皮亞齊.md "wikilink")[穀神星](../Page/穀神星.md "wikilink")。
  - [1802年](../Page/1802年.md "wikilink")─[奧伯斯](../Page/奧伯斯.md "wikilink")
    發現第二顆小行星[智神星](../Page/智神星.md "wikilink")。
  - [1821年](../Page/1821年.md "wikilink")─[布瓦](../Page/布瓦.md "wikilink")（Alexis
    Bouvard）發現[天王星軌道不規律](../Page/天王星.md "wikilink")。
  - [1825年](../Page/1825年.md "wikilink")─拉普拉斯完成他對[萬有引力的研究](../Page/萬有引力.md "wikilink")，在[《天體力學》中闡明太陽系的穩定](../Page/《天體力學》.md "wikilink")、潮汐、分點歲差、月球的[天秤動和土星環等等](../Page/天秤動.md "wikilink")。
  - [1838年](../Page/1838年.md "wikilink")─[白塞耳測量出](../Page/弗里德里希·威廉·贝塞尔.md "wikilink")[天鵝座61的](../Page/天鵝座61.md "wikilink")[視差](../Page/視差.md "wikilink")，再度駁斥了最古老的[地心說](../Page/地心說.md "wikilink")。
  - [1846年](../Page/1846年.md "wikilink")─[勒维耶由天王星軌道的不規則](../Page/勒维耶.md "wikilink")，預測海王星的位置。
  - [1846年](../Page/1846年.md "wikilink")─[約翰·加雷發現海王星](../Page/约翰·格弗里恩·伽勒.md "wikilink")。
  - [1846年](../Page/1846年.md "wikilink")─[威廉·拉塞爾發現](../Page/威廉·拉塞爾.md "wikilink")[崔頓](../Page/海衛一.md "wikilink")。
  - [1849年](../Page/1849年.md "wikilink")─[愛德華·洛希發現天體重力創造的](../Page/愛德華·洛希.md "wikilink")[潮汐力](../Page/潮汐力.md "wikilink")，阻止鄰近的小天體凝聚的界限，稱為[洛希極限](../Page/洛希極限.md "wikilink")，解釋了土星環為何不能凝聚成衛星。
  - [1856年](../Page/1856年.md "wikilink")─[馬克士威論證固體的土星環將會被土星的引力撕裂](../Page/馬克士威.md "wikilink")，所以土星環包含許多微小的衛星。
  - [1862年](../Page/1862年.md "wikilink")─[安吉洛·西奇神父分析和比較太陽與恆星的光譜](../Page/安吉洛·西奇.md "wikilink")，確定太陽是顆恆星。
  - [1866年](../Page/1866年.md "wikilink")─[斯基亞帕雷利意識到](../Page/斯基亞帕雷利.md "wikilink")[流星雨是地球穿過彗星殘留在軌道上的碎屑造成的](../Page/流星雨.md "wikilink")。

## 20世紀

  - [1906年](../Page/1906年.md "wikilink")─德國天文學家[馬克斯·沃夫發現第一顆](../Page/馬克斯·沃夫.md "wikilink")[特洛伊小行星](../Page/特洛伊小行星.md "wikilink")(588)
    [阿基里斯](../Page/阿基里斯.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")─[克莱德·汤博發現](../Page/克莱德·汤博.md "wikilink")[冥王星](../Page/冥王星.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")─[赛斯·尼克尔森測量月球的表面溫度](../Page/赛斯·尼克尔森.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")─[杰拉德·柯伊伯發現](../Page/杰拉德·柯伊伯.md "wikilink")[泰坦有濃密的大氣層](../Page/土衛六.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")─[扬·奥尔特建議](../Page/扬·奥尔特.md "wikilink")[彗星起源於太陽系邊界的](../Page/彗星.md "wikilink")[歐特雲](../Page/奥尔特云.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")─[杰拉德·柯伊伯提出週期彗星來自於距離太陽](../Page/杰拉德·柯伊伯.md "wikilink")40至100[天文單位的環帶](../Page/天文單位.md "wikilink")，現稱為[柯伊伯带](../Page/柯伊伯带.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")─[月球3號傳送回月球背面的圖片](../Page/月球3號.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")─[詹姆斯·艾略特在](../Page/詹姆斯·艾略特.md "wikilink")[柯伊伯機載天文台的天王星掩星觀測發現天王星環](../Page/柯伊伯機載天文台.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")─[詹姆斯·克里斯蒂發現冥王星的衛星](../Page/詹姆斯·克里斯蒂.md "wikilink")[凱倫](../Page/冥衛一.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")─[彼得·高德瑞克和](../Page/彼得·高德瑞克.md "wikilink")[史考特·特瑞曼提出](../Page/史考特·特瑞曼.md "wikilink")[行星環動力學的](../Page/行星環動力學.md "wikilink")[波茲曼方程式模型](../Page/波茲曼方程式.md "wikilink")，認為環的光深度和微粒的復原係數間的關係使還得以穩定而不會毀壞。
  - [1979年](../Page/1979年.md "wikilink")─[航海家1號發現木星暗淡的環系統和](../Page/航海家1號.md "wikilink")[埃歐上的火山](../Page/木衛一.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")─[航海家1號發現兩顆木星的新衛星和土星的六顆新衛星](../Page/航海家1號.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")─[航海家2號飛越](../Page/航海家2號.md "wikilink")[天王星](../Page/天王星.md "wikilink")，首度傳回表面特徵、環和衛星的特寫鏡頭，並發現十顆新衛星。
  - [1988年](../Page/1988年.md "wikilink")─[馬丁·鄧肯](../Page/馬丁·鄧肯.md "wikilink")、[湯馬斯·奎恩和](../Page/湯馬斯·奎恩.md "wikilink")[史考特·特瑞曼論證短週期彗星源自](../Page/史考特·特瑞曼.md "wikilink")[柯伊伯带而不是](../Page/柯伊伯带.md "wikilink")[奥尔特云](../Page/奥尔特云.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")─[航海家2號飛越](../Page/航海家2號.md "wikilink")[海王星](../Page/海王星.md "wikilink")：首度觀察到表面特徵、確定弧是完整的環、[崔頓的影像和發現五顆新衛星](../Page/海衛一.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")─發現[脈衝星](../Page/脈衝星.md "wikilink")[PSR
    B1257+12有行星系統](../Page/PSR_B1257+12.md "wikilink")。這是除太陽系之外，最先被發現的行星系統。
  - [1992年](../Page/1992年.md "wikilink")─[夏威夷大學的](../Page/夏威夷大學.md "wikilink")[大衛·朱維特和](../Page/大衛·朱維特.md "wikilink")[哈佛大學的](../Page/哈佛大學.md "wikilink")[珍妮·劉發現第一個](../Page/珍妮·劉.md "wikilink")[柯伊伯带天體](../Page/柯伊伯带.md "wikilink")[1992
    QB<sub>1</sub>](../Page/1992_QB1.md "wikilink")。

## 21世紀

### 2001–2019

  - [2003年](../Page/2003年.md "wikilink")─[布朗](../Page/米高·布朗.md "wikilink")（[加州理工學院](../Page/加州理工學院.md "wikilink")）、[特魯希略](../Page/乍德·特魯希略.md "wikilink")（[雙子星天文台](../Page/雙子星天文台.md "wikilink")）及[拉比諾維茨](../Page/大衛·拉比諾維茨.md "wikilink")（[耶魯大學](../Page/耶魯大學.md "wikilink")）共同發現發現史無前例的巨大小行星[塞德娜](../Page/小行星90377.md "wikilink")，軌道週期12,000年。
  - [2004年](../Page/2004年.md "wikilink")─[航海家1號送回在太陽系的](../Page/航海家1號.md "wikilink")[日鞘內觀測到的第一批資料](../Page/日鞘.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")─[卡西尼-惠更斯號成為環繞土星的第一艘太空船](../Page/卡西尼-惠更斯號.md "wikilink")。它發現土星環有著複雜的運動，幾顆新的小衛星，和[恩克拉多斯的](../Page/土衛二.md "wikilink")[冰火山](../Page/冰火山.md "wikilink")，和在更小的[土衛八](../Page/土衛八.md "wikilink")（Iapetus）赤道上的奇怪山脊，並從[泰坦表面提供第一張影像](../Page/土衛六.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")─[布朗等人發現在](../Page/米高·布朗.md "wikilink")2003年發現的小行星[鬩神星是比](../Page/鬩神星.md "wikilink")[冥王星還要大的](../Page/冥王星.md "wikilink")[海王星外天體](../Page/海王星外天體.md "wikilink")，並且有顆衛星[鬩衛一](../Page/小行星136199_I.md "wikilink")。鬩神星還是自從1846年發現海王星的衛星，[崔頓之後](../Page/海衛一.md "wikilink")，在太陽系內被發現的最大天體。
  - [2005年](../Page/2005年.md "wikilink")─[火星探測漫遊者執行首度在另一顆行星表面觀察火星的衛星](../Page/火星探測漫遊者.md "wikilink")[傅伯斯從太陽前方掠過造成的日食](../Page/火衛一.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")─[國際天文學聯合會第](../Page/國際天文學聯合會.md "wikilink")26屆會員大會投票贊成[重新定義行星並正式宣布](../Page/IAU的行星定義.md "wikilink")[穀神星](../Page/穀神星.md "wikilink")、[冥王星和](../Page/冥王星.md "wikilink")[鬩神星是](../Page/鬩神星.md "wikilink")[矮行星](../Page/矮行星.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")─國際天文學聯合會宣告[鳥神星和](../Page/鳥神星.md "wikilink")[妊神星是矮行星](../Page/妊神星.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")─*[曙光號](../Page/曙光號.md "wikilink")*太空船進入環繞[灶神星的軌道進行詳細的測量](../Page/灶神星.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")─[卡西尼-惠更斯號近距離拍攝](../Page/卡西尼-惠更斯號.md "wikilink")[土衛三十二](../Page/土衛三十二.md "wikilink")（Methone）的影像，揭示他有非常光滑的表面。
  - [2012年](../Page/2012年.md "wikilink")─*[曙光號](../Page/曙光號.md "wikilink")*太空船離開[灶神星朝向](../Page/灶神星.md "wikilink")[穀神星前進](../Page/穀神星.md "wikilink")。
  - [2015年](../Page/2015年.md "wikilink")─*[曙光號](../Page/曙光號.md "wikilink")*太空船進入環繞[穀神星的軌道](../Page/穀神星.md "wikilink")，進行仔細的測量。
  - [2015年](../Page/2015年.md "wikilink")─*[新視野號](../Page/新視野號.md "wikilink")*太空船飛越[冥王星](../Page/冥王星.md "wikilink")。
  - [2019年](../Page/2019年.md "wikilink")─*[新視野號](../Page/新視野號.md "wikilink")*太空船執行飛越古柏帶天體的任務。

## 相關條目

  - [太陽系探索年表](../Page/太陽系探索年表.md "wikilink")
  - [太陽系天體發現時間列表](../Page/太陽系天體發現時間列表.md "wikilink")
  - [中國天文學史](../Page/中國天文學史.md "wikilink")

[T](../Category/天文學年表.md "wikilink")