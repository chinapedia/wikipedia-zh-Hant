**太陽與月亮**（[日文](../Page/日文.md "wikilink")：****，又稱為**T\&C
Bomber**，[日文](../Page/日文.md "wikilink")：****）是一個來自於[Hello\!
Project的](../Page/Hello!_Project.md "wikilink")[日本女子組合](../Page/日本.md "wikilink")，於1999年創立，於2000年解散。成員有[稻葉貴子](../Page/稻葉貴子.md "wikilink")、[本多RuRu](../Page/本多RuRu.md "wikilink")、[信田美帆](../Page/信田美帆.md "wikilink")、[小湊美和等人](../Page/小湊美和.md "wikilink")。

## 音乐作品

### 專輯

  - 1999年10月27日：《Taiyo & Ciscomoon 1》
  - 2000年9月27日：《2nd Stage》
  - 2008年12月10日：《太陽與月亮／T\&C Bomber》

### 單曲

  - 1999年4月21日：《月亮太陽》
  - 1999年6月23日：《Gatamekira》
  - 1999年7月28日：《宇宙La Ta Ta》
  - 1999年8月25日：《Everyday Everywhere》
  - 1999年9月29日：《Magic of Love》
  - 1999年12月8日：《圓圓的太陽(Winter Version)》
  - 2000年4月19日：《Don't Stop 戀愛中?》
  - 2000年7月19日：《Hey\!正午的海市蜃樓》

[T](../Category/Hello!_Project.md "wikilink")