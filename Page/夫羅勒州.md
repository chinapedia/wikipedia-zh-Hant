<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>本條目記敘的是**发罗拉州**。其他同名的行政區尚有[夫羅勒區及](../Page/夫羅勒區.md "wikilink")[夫羅勒市](../Page/夫羅勒市.md "wikilink")。</small>

</div>

**夫羅勒州**（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：Vlorë）位於[阿爾巴尼亞西南部的](../Page/阿爾巴尼亞.md "wikilink")[亞得里亞海畔](../Page/亞得里亞海.md "wikilink")，由[夫羅勒區](../Page/夫羅勒區.md "wikilink")、[-{zh-hans:达尔维那区;
zh-hant:德維納區;}-](../Page/德維納區.md "wikilink")、[-{zh-hans:萨兰达区;
zh-hant:潔蘭德區;}-州所組成](../Page/潔蘭德區.md "wikilink")，與[-{zh-hans:费里;
zh-hant:非夏爾;}-州](../Page/非夏爾州.md "wikilink")、[吉羅卡斯特相鄰](../Page/吉羅卡斯特州.md "wikilink")。