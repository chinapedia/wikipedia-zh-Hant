《**失樂園**》（*Paradise
Lost*）是17世紀[英國](../Page/英國.md "wikilink")[詩人](../Page/詩人.md "wikilink")[約翰·彌爾頓](../Page/約翰·彌爾頓.md "wikilink")（John
Milton）以《[舊約聖經](../Page/舊約聖經.md "wikilink")·[創世紀](../Page/創世紀.md "wikilink")》為基础創作的[史詩](../Page/史詩.md "wikilink")，出版於1667年。

## 概述

此書內容主要是描述墮落天使[路西法](../Page/路西法.md "wikilink")（撒旦）從對[神的反叛失敗後再重新振作](../Page/神.md "wikilink")，他對人間的嫉妒，以及他運用他的謀略化身為[蛇](../Page/蛇.md "wikilink")，引誘[亞當和](../Page/亞當.md "wikilink")[夏娃違反神的禁令偷嘗](../Page/夏娃.md "wikilink")[智慧樹](../Page/智慧樹.md "wikilink")（分別善惡樹）的果實，導致人類被逐出[伊甸園的故事](../Page/伊甸園.md "wikilink")。在此書中，彌爾頓說明，本書是為了「辯證神對人類的態度」，及闡明神的預見與人類的[自由意志之間的衝突](../Page/自由意志.md "wikilink")。
彌爾頓融合了[異教信仰](../Page/異教.md "wikilink")、[古希臘文獻以及](../Page/古希臘.md "wikilink")[基督教信仰](../Page/基督教.md "wikilink")，在此詩中探討了多樣的主題，從婚姻、政治（彌爾頓本人在英國內戰期間是活躍的政治份子）到[君主政體](../Page/君主政體.md "wikilink")；同時也辯證許多困難的[神學議題](../Page/神學.md "wikilink")，包括[命運](../Page/命運.md "wikilink")、[宿命](../Page/宿命.md "wikilink")、[三位一體](../Page/三位一體.md "wikilink")、以及[原罪和](../Page/原罪.md "wikilink")[死亡在世界中的出現](../Page/死亡.md "wikilink")。另外還包含了[天使](../Page/天使.md "wikilink")、[墮落天使](../Page/墮落天使.md "wikilink")、[撒旦](../Page/撒旦.md "wikilink")、以及天堂中的戰爭。這是一部由基督徒詩人用古希臘風格所作，用宗教反派撒旦為要角的希伯來歷史相關史詩。

## 參見

  - [復樂園](../Page/復樂園.md "wikilink")

## 外部链接

## 在线阅读

## 相关信息

[\*](../Category/失樂園.md "wikilink")
[Category:英國文學](../Category/英國文學.md "wikilink")
[Category:史诗](../Category/史诗.md "wikilink")
[Category:1660年代書籍](../Category/1660年代書籍.md "wikilink")