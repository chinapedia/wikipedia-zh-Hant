《**失眠的解藥**》（*The Cure for Insomnia*），由 John Henry Timmis IV
執導，是當時經由[金氏世界記錄認可的世界上片長最長的](../Page/金氏世界紀錄大全.md "wikilink")[電影](../Page/電影.md "wikilink")，該片於1987年首映。該片全長5,220分鐘（87小時），全片並沒有[劇情](../Page/劇情.md "wikilink")，而是由藝術家Lee
Groban朗讀他的長詩"A Cure for
Insomnia"三天半之久，間中穿插著[重金属音乐以及](../Page/重金属音乐.md "wikilink")[色情影片](../Page/色情影片.md "wikilink")。\[1\]

該電影於1987年1月31日至2月3日在[美國](../Page/美國.md "wikilink")[芝加哥藝術學院無間斷進行首映](../Page/芝加哥藝術學院.md "wikilink")。

## 參見

  - 《[24小時觸目驚心](../Page/24小時觸目驚心.md "wikilink")》

## 參考資料

## 外部連結

  -
[Category:1987年電影](../Category/1987年電影.md "wikilink")
[Category:电影之最](../Category/电影之最.md "wikilink")
[Category:美国的世界之最](../Category/美国的世界之最.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")

1.  [The Cure for Insomnia (1987)](http://www.imdb.com/title/tt0284020/)