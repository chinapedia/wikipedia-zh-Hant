[Quark_confinement.svg](https://zh.wikipedia.org/wiki/File:Quark_confinement.svg "fig:Quark_confinement.svg")

[Gluon_tube-color_confinement_animation.gif](https://zh.wikipedia.org/wiki/File:Gluon_tube-color_confinement_animation.gif "fig:Gluon_tube-color_confinement_animation.gif")

**夸克禁闭**是一种物理现象，描述[夸克不会单独存在](../Page/夸克.md "wikilink")。由于[强相互作用力](../Page/强相互作用力.md "wikilink")，带色荷的夸克被限制和其他夸克在一起（两个或三个组成一个[粒子](../Page/粒子.md "wikilink")），使得总[色荷为零](../Page/色荷.md "wikilink")。夸克之间的作用力随着距离的增加而增加，因此而不能发现单独存在的夸克。

## 产生

夸克是在[宇宙产生](../Page/宇宙.md "wikilink")10<sup>−10</sup>秒后产生的，那时宇宙的温度已经下降到低于100GeV，这是产生成对的W和Z粒子的门槛。从那个时候起，W和Z粒子接过了进行粒子间[弱相互作用的任务而且也不会单独存在](../Page/弱相互作用.md "wikilink")，除非在它们产生的地方（短暂的）。宇宙诞生后约10<sup>−6</sup>\~10<sup>−3</sup>秒之间，其温度降到夸克不再有足够的能量自由漫游的程度，而是两个或三个约束在一起。宇宙诞生大约1[微秒后](../Page/微秒.md "wikilink")，可用能量低于几百TeV，夸克和[反夸克凝聚成](../Page/反夸克.md "wikilink")[重子和](../Page/重子.md "wikilink")[反重子](../Page/反重子.md "wikilink")。夸克等离子阶段在宇宙起源10<sup>−4</sup>秒后就结束了。

## 参考

  - Michael E. Peskin , Dan V. Schroeder, "An Introduction to Quantum
    Field Theory", ISBN 978-0-201-50397-5

  -
  - [Quarks](http://hyperphysics.phy-astr.gsu.edu/hbase/particles/quark.html)

[Category:量子色動力學](../Category/量子色動力學.md "wikilink")
[Category:夸克](../Category/夸克.md "wikilink")