**奪門之變**，又稱**南宮復辟**，是[明代宗朱祁鈺](../Page/景泰帝.md "wikilink")[景泰八年](../Page/景泰_\(年号\).md "wikilink")（[1457年](../Page/1457年.md "wikilink")）正月，发生的一場[政變](../Page/政變.md "wikilink")，[太上皇](../Page/太上皇.md "wikilink")[朱祁鎮成功](../Page/明英宗.md "wikilink")[復辟](../Page/復辟.md "wikilink")，奪回皇位。

## 前因

[明英宗皇帝.jpg](https://zh.wikipedia.org/wiki/File:明英宗皇帝.jpg "fig:明英宗皇帝.jpg")
[正統十四年](../Page/正统_\(年号\).md "wikilink")
([1449年](../Page/1449年.md "wikilink"))
發生[土木堡之變](../Page/土木之变.md "wikilink")，[明英宗被](../Page/明英宗.md "wikilink")[瓦剌俘虜](../Page/瓦剌.md "wikilink")，其弟郕王[朱祁鈺被眾](../Page/景泰帝.md "wikilink")[大臣推舉為皇帝](../Page/大臣.md "wikilink")，是為明景帝（南明尊稱為代宗），改元景泰。

[景泰元年](../Page/景泰_\(年号\).md "wikilink")（[1450年](../Page/1450年.md "wikilink")），[兵部侍郎](../Page/兵部侍郎.md "wikilink")[于谦成功](../Page/于谦.md "wikilink")[抗敵](../Page/京师保卫战.md "wikilink")，並與[瓦剌議和](../Page/瓦剌.md "wikilink")，經過使臣[楊善個人的斡旋](../Page/楊善.md "wikilink")，瓦剌首領[也先見新君已立](../Page/也先.md "wikilink")，英宗已經無利用價值，反而不想因英宗為虜之事成為與大明修好的障礙，於是同意放回英宗。但朱祁鈺對大臣說：「我並不是貪戀帝位，而是當初擁立我的是你們啊。」不願英宗返國，經大臣陳述其利弊後，朱祁鈺将英宗迎接回京，置於[南宮](../Page/南三所.md "wikilink")，尊為[太上皇](../Page/太上皇.md "wikilink")。並以[錦衣衛對英宗加以](../Page/錦衣衛.md "wikilink")[軟禁](../Page/軟禁.md "wikilink")，嚴密控管，宮門不但上鎖，並且灌[鉛](../Page/鉛.md "wikilink")，食物僅能由小洞遞入。其後廢原太子—英宗之子[朱見深](../Page/明宪宗.md "wikilink")，並立其長子[朱見濟為新](../Page/朱见济.md "wikilink")[太子](../Page/储君.md "wikilink")。朱见济夭折后，朱祁钰已无亲子，却也没有复立朱见深，储位空悬。

景泰七年（[1456年](../Page/1456年.md "wikilink")），朱祁鈺病，在對抗[瓦剌時立下大功的](../Page/瓦剌.md "wikilink")[將領](../Page/將領.md "wikilink")[石亨為了自身利益](../Page/石亨.md "wikilink")，有意協助英宗奪回帝位。在拉攏身邊人商討後，與[宦官](../Page/宦官.md "wikilink")[曹吉祥](../Page/曹吉祥.md "wikilink")、[都督](../Page/都督.md "wikilink")[張軏](../Page/張軏.md "wikilink")、[都察院](../Page/都察院.md "wikilink")[左都御史](../Page/左都御史.md "wikilink")[楊善](../Page/楊善.md "wikilink")、[太常卿](../Page/太常卿.md "wikilink")[許彬以及左](../Page/許彬_\(明朝\).md "wikilink")[副都御史](../Page/副都御史.md "wikilink")[徐有貞等人行事](../Page/徐有貞.md "wikilink")。

## 经过

景泰八年（[1457年](../Page/1457年.md "wikilink")）正月，朱祁鈺病重。十六日夜，石亨、徐有贞等大臣带一千餘士兵偷襲[紫禁城](../Page/紫禁城.md "wikilink")，撞开南宮宫门，接出英宗直奔[东华门](../Page/东华门.md "wikilink")。守门的武士不开门，英宗上前说道：“朕乃太上皇帝也。”武士只好打开城门。

黎明时分，众大臣到了「[奉天殿](../Page/太和殿.md "wikilink")」，只见英宗坐于龙椅之上，徐有贞高喊：“太上皇帝復位。”史称「奪門之變」或「南宮復辟」。

英宗[復辟後](../Page/復辟.md "wikilink")，朱祁鈺被遷至西宮，不久去世。

## 影響

[談遷評論](../Page/談遷.md "wikilink")：“于少保最留心兵事，爪牙四布，若奪門之謀，懵然不少聞，何貴本兵哉！或聞之倉卒，不及發耳！”

明英宗復辟後，于謙以謀逆罪名被處死，而曾助英宗回復[帝位的功臣](../Page/帝位.md "wikilink")，如石亨、徐元玉、許彬、楊善、張軏與曹吉祥等人都被封為大官。其中，曹吉祥等在朝中橫行霸道，後期更發生了曹吉祥企圖弒位的[曹石之變](../Page/曹石之變.md "wikilink")。

值得一提的是，景泰八年春正月，[明英宗重登大寶後](../Page/明英宗.md "wikilink")，废景泰年号，改景泰八年为[天顺元年](../Page/天顺_\(明朝\).md "wikilink")，但倉促之中忘記罷黜朱祁鈺，直到同年二月[乙未才將朱祁鈺廢為郕王](../Page/乙未.md "wikilink")。因此，在這幾天之內，名義上英宗和景帝兩位合法的皇帝同時並存，成為中國[帝制史上絕無僅有的奇觀](../Page/君主制.md "wikilink")。

[曹石之变前](../Page/曹石之變.md "wikilink")，英宗在[李贤提醒下](../Page/李贤_\(大学士\).md "wikilink")，意识到朱祁钰时日无多，没有在世的儿子，也没有立储，一旦朱祁钰去世，自己复位顺理成章，夺门功臣其实是投机以求自己获益，一旦事败，英宗自己反而要受到牵连；于是开始罢黜夺门功臣的爵位。楊善、張軏已去世，爵位已分别由儿子[杨宗](../Page/楊宗.md "wikilink")、[张瑾继承](../Page/张瑾_\(太平侯\).md "wikilink")。[明宪宗初年](../Page/明宪宗.md "wikilink")，罢黜杨宗、张瑾，因夺门之功所授爵位至此全部收回。

## 参考文献

  - [谈迁](../Page/谈迁.md "wikilink")《[国榷](../Page/国榷.md "wikilink")》卷32

## 参见

  - [曹石之變](../Page/曹石之變.md "wikilink")

{{-}}

[Category:1457年](../Category/1457年.md "wikilink")
[Category:明朝政变](../Category/明朝政变.md "wikilink")
[Category:明朝历史事件](../Category/明朝历史事件.md "wikilink")
[Category:北京政治事件](../Category/北京政治事件.md "wikilink")
[Category:1450年代中国](../Category/1450年代中国.md "wikilink")