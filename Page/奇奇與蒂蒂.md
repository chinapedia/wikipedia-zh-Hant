[Hong_Kong_Disneyland_1396.JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_Disneyland_1396.JPG "fig:Hong_Kong_Disneyland_1396.JPG")
**奇奇與蒂蒂**（英語：**Chip 'n'
Dale**，香港譯為**鋼牙與大鼻**，台灣譯為**奇奇與蒂蒂**），是美國[迪士尼所創造的經典卡通人物](../Page/迪士尼.md "wikilink")，為一對[花栗鼠雙胞胎兄弟](../Page/花栗鼠.md "wikilink")，生日為4月2日，首次登場是在1943年的[動畫](../Page/動畫.md "wikilink")《大兵[布魯托](../Page/布魯托.md "wikilink")》（英語：Private
Pluto）。奇奇與蒂蒂的命名是來自於[英國著名](../Page/英國.md "wikilink")[家具工匠](../Page/家具.md "wikilink")[齊本德爾](../Page/湯瑪斯·齊本德爾.md "wikilink")（英語：Chippendale）的[諧音](../Page/諧音.md "wikilink")。

奇奇（英語：Chip）的外觀特徵是[黑色的](../Page/黑色.md "wikilink")[鼻子和一顆位於正中央的大](../Page/鼻子.md "wikilink")[門牙](../Page/門牙.md "wikilink")，蒂蒂（英語：Dale）則是[紅色的大鼻子和兩顆](../Page/紅色.md "wikilink")[虎牙](../Page/虎牙.md "wikilink")（初期與奇奇長得一模一樣），並且頭上有一小撮短毛。它們在背上都有兩條白色的直線毛，而在人物性格設定上，奇奇的[邏輯性較強](../Page/邏輯.md "wikilink")、較聰明，蒂蒂則常常少根筋、笨笨傻傻的感覺，所以故事情節多半是由奇奇扮演策劃、出點子的主謀，蒂蒂則扮演聽命、執行的角色，並藉由蒂蒂出人意料的脫序行動來引發[笑點](../Page/笑.md "wikilink")，基本上走的是[鬧劇路線](../Page/鬧劇.md "wikilink")，主要與[唐老鴨或](../Page/唐老鴨.md "wikilink")[布魯托做對手戲](../Page/布魯托.md "wikilink")，其中又以戲弄[唐老鴨居多](../Page/唐老鴨.md "wikilink")。

奇奇與蒂蒂登場近半[世紀後](../Page/世紀.md "wikilink")，1989年迪士尼終於製作了以兩隻花栗鼠為主角的動畫系列《[救難小福星](../Page/救難小福星.md "wikilink")》（英語：Chip
'n Dale: Rescue Rangers，香港則譯作《花鼠俱樂部》）。\[1\]

## 外部連結

  - [Chip 'n'
    Dale愛好者網站](https://web.archive.org/web/20070615143740/http://www.chipndaleonline.com/home.html)

[Category:1943年面世的虛構角色](../Category/1943年面世的虛構角色.md "wikilink")
[Category:戴尔漫画作品](../Category/戴尔漫画作品.md "wikilink")
[Category:迪士尼漫画角色](../Category/迪士尼漫画角色.md "wikilink")
[Category:唐老鸭世界角色](../Category/唐老鸭世界角色.md "wikilink")
[Category:虚构拟人化角色](../Category/虚构拟人化角色.md "wikilink")
[Category:虚构花栗鼠](../Category/虚构花栗鼠.md "wikilink")
[Category:虚构二人组](../Category/虚构二人组.md "wikilink")
[Category:虚构双胞胎](../Category/虚构双胞胎.md "wikilink")
[Category:金钥匙漫画作品](../Category/金钥匙漫画作品.md "wikilink")
[Category:王国之心系列角色](../Category/王国之心系列角色.md "wikilink")
[Category:米老鼠世界角色](../Category/米老鼠世界角色.md "wikilink")

1.