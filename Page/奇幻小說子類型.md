[奇幻小說的類型中已多出了許多和神話及童話](../Page/奇幻小說.md "wikilink")（這些是傳統奇幻小說的基本）等之間無明顯對照的新子類型，雖然由神話及童話中的靈感使其維持著一定的架構。因為大眾奇幻小說在二十世紀的興盛，作品數量龐大到可以再被細分成數種的類型。

由於早期的現代奇幻作品通常都混在一起談，近期的作品基本上都會被歸類進某個子類型中。這些子類型也多會擴展至包含早期的現代奇幻作品。

## 架空歷史小說

雖然許多類型的架空歷史小說被歸類為科幻小說，但其中含有魔法和奇幻生物的小說則被歸類為奇幻的。

其和[歷史奇幻與](../Page/歷史奇幻.md "wikilink")[當代奇幻不同的點在於兩者之間對於歷史和地理分別有著明顯的差異與明顯的關連](../Page/當代奇幻.md "wikilink")。

  - [蘭德爾·加勒特所著的](../Page/蘭德爾·加勒特.md "wikilink")《Lord Darcy》系列
  - [基斯·羅伯茨所著的](../Page/基斯·羅伯茨.md "wikilink")《[帕凡舞](../Page/帕凡舞_\(小說\).md "wikilink")》
  - [波爾·安德森所著的](../Page/波爾·安德森.md "wikilink")《[混沌行動](../Page/混沌行動.md "wikilink")》
  - [喬治·凱斯所著的](../Page/喬治·凱斯.md "wikilink")《[非理性的時代](../Page/非理性的時代.md "wikilink")》
  - 電視影集《Cast A Deadly Spell》

## 班斯奇幻

**班斯奇幻**（**Bangsian
fantasy**）是以[約翰·肯德里克·班斯來命名的](../Page/約翰·肯德里克·班斯.md "wikilink")，他是十九世紀末、二十世紀初的一位作家，著有《Associated
Shades》系列的小說，內容描寫著許多知名人士的死後生活。常用的有[地下世界](../Page/地下世界.md "wikilink")/[靈薄獄](../Page/靈薄獄.md "wikilink")/[煉獄](../Page/煉獄.md "wikilink")（中性）、[依利森](../Page/依利森.md "wikilink")/[涅槃](../Page/涅槃.md "wikilink")/[天堂](../Page/天堂.md "wikilink")(好的)和[厄瑞玻斯](../Page/厄瑞玻斯.md "wikilink")/[欣嫩谷](../Page/欣嫩谷.md "wikilink")/[地獄](../Page/地獄.md "wikilink")(壞的)等詞。

  - 《Inferno》，[拉瑞·尼文著](../Page/拉瑞·尼文.md "wikilink")（1976年）
  - 《Heroes in
    Hell》，[C.J.切莉與](../Page/C.J.切莉.md "wikilink")[珍妮特‧莫里斯著](../Page/珍妮特‧莫里斯.md "wikilink")（1986年）
  - 《God Bless You, Dr.
    Kevorkian》，[庫爾特·馮內古特著](../Page/庫爾特·馮內古特.md "wikilink")（1999年）

## 喜劇奇幻

喜劇奇幻在其含義和氛圍上主要以幽默的方式來描述，通常設定在虛構的世界上，且多採用或仿作其他奇幻作品的題材，如《[啥利波特](../Page/啥利波特.md "wikilink")》即仿自《[哈利波特](../Page/哈利波特.md "wikilink")》。

雖然許多此類的作品，因其設定被歸類為[嚴肅奇幻](../Page/奇幻小說的子類型#嚴肅奇幻.md "wikilink")。但其在理論上，是可以和其他的子類型也有所重合的。

  - 《[沉悶的戒指](../Page/沉悶的戒指.md "wikilink")》，[亨利·貝兒多和](../Page/亨利·貝兒多.md "wikilink")[道格拉·斯肯尼著](../Page/道格拉·斯肯尼.md "wikilink")（1969年）
  - 《The Eye of Argon》，Jim Theis著（1970年）
  - 《[變色龍的符咒](../Page/變色龍的符咒.md "wikilink")》，[皮爾斯·安東尼著](../Page/皮爾斯·安東尼.md "wikilink")（1977年）
  - 《Hordes of the
    Things》，[安德魯·馬歇爾和](../Page/安德魯·馬歇爾.md "wikilink")[約翰·羅伊德著](../Page/約翰·羅伊德.md "wikilink")（1980年）
  - 《[魔法的顏色](../Page/魔法的顏色.md "wikilink")》，[特里·普拉切特著](../Page/特里·普拉切特.md "wikilink")（1983年）

## 當代奇幻

## 黑暗奇幻

**黑暗奇幻**（**Dark
fantasy**）是指：專注在通常在恐怖小說中出現的元素，但發生在更像是[劍與魔法或](../Page/劍與魔法.md "wikilink")[嚴肅奇幻的設定上](../Page/嚴肅奇幻.md "wikilink")。黑暗奇幻包括「較勇敢」的奇幻，在設定上比一般奇幻的傳統理想化描述更真實地顯示出中世紀的殘酷，通常會加入一點超自然的恐怖元素。它可能有或可能沒有自己的[奇幻世界](../Page/奇幻世界.md "wikilink")。

更一般地，「黑暗奇幻」或許可以被當做[超自然恐怖的同義詞](../Page/超自然恐怖.md "wikilink")，以區分有無含有照自然元素的恐怖小說。例如，一個有關由墳墓裡爬出來的[木乃伊或](../Page/木乃伊.md "wikilink")[吸血鬼的小說](../Page/吸血鬼.md "wikilink")，最有可能被歸類為「黑暗奇幻」、「超自然恐怖」或「恐怖奇幻」裡；而一個有關連續殺人魔的小說則會簡單地被歸類至「恐怖小說」。在此一觀點下，「黑暗奇幻」和「當代奇幻」之間有著極大重合的部份。

或許黑暗奇幻中最精典的作品是[霍華德·菲利普·洛夫克拉夫特所寫的小說吧](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")！其對奇幻和恐怖的混合並不能簡單地被歸類至哪一類型，但其作品已直接或間接地大量的影響了幾乎所有的奇幻類型。

  - 《[克蘇魯神話](../Page/克蘇魯神話.md "wikilink")》，[霍華德·菲利普·洛夫克拉夫特](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")
  - 《[吸血鬼獵人D](../Page/吸血鬼獵人D.md "wikilink")》，[菊地秀行著](../Page/菊地秀行.md "wikilink")（1983年）
  - 《[烙印勇士](../Page/烙印勇士.md "wikilink")》，[三浦建太郎著](../Page/三浦建太郎.md "wikilink")（1989年）

## 情色奇幻

情色奇幻將[情色的元素也加入奇幻的設定中](../Page/情色.md "wikilink")。

此一子類型事實上可以和其他的所有子類型有重合的地方，因為它所含的不同元素和其他子類型的奇幻元素或設定完全地不同。

  - 《Kushiel's Dart》，[賈桂琳·凱瑞著](../Page/賈桂琳·凱瑞.md "wikilink")

## 童話奇幻

**童話奇幻**（**Fairytale
fantasy**）和其他子類型不同的地方在於其大量地使用了童話裡的主題及情節。它們有時會忽略一般奇幻小說所會做的[建構世界](../Page/建構世界.md "wikilink")，而和其來源的童話同樣樂觀，但也不總是如此；也有使用了嚴肅奇幻、當代或是歷史等設計的故事，因為需要有建構世界，也可能被認為是此類型的一種。

  - [喬治·麥當勞](../Page/喬治·麥當勞.md "wikilink")，《[公主與哥布林](../Page/公主與哥布林.md "wikilink")》（1872年）
  - [詹姆斯·瑟伯](../Page/詹姆斯·瑟伯.md "wikilink")，《The 13 Clocks》（1950年）
  - [蘿賓·麥金莉](../Page/蘿賓·麥金莉.md "wikilink")，《Beauty: A Retelling of the
    Story of Beauty and the Beast》（1978年）
  - [塔妮絲·李](../Page/塔妮絲·李.md "wikilink")，《Red As Blood, or Tales from
    the Sisters Grimmer》（1983年）
  - [派翠西亞·弗雷德](../Page/派翠西亞·弗雷德.md "wikilink")，《[白玫瑰和紅玫瑰](../Page/白玫瑰和紅玫瑰.md "wikilink")》（1989年）

## 硬式奇幻

**硬式奇幻**（**Hard
fantasy**）是奇幻小說的一種子類型，小說裡的世界（和其他的奇幻設定不同）緊密地遵順著科學的定律。其和一般的奇幻小說之間的關係，與硬式科幻小說和一般的科幻小說之間的關係有著極相似的類比。

## 英雄奇幻

**英雄奇幻**（**Heroic fantasy**）一部份為嚴肅奇幻，一部份為劍與魔法的子類型。

## 嚴肅奇幻

**嚴肅奇幻**（**High
fantasy**）通常是指：「在和我們的世界無關的[奇幻世界裡](../Page/奇幻世界.md "wikilink")，正與邪之間如史詩般戰鬥的奇幻小說」。小說裡的道德觀是採取客觀的角度，而不是由單方面來做判斷。

道德的氛圍與高度的危險－通常是世界動盪－將其和[劍與魔法相區分](../Page/奇幻小說的子類型#劍與魔法.md "wikilink")；另外，這個世界不是基於一個真實世界的歷史將其與[歷史奇幻相區分](../Page/奇幻小說的子類型#歷史奇幻.md "wikilink")。

  - 《[歐魯勃洛斯之蟲](../Page/歐魯勃洛斯之蟲.md "wikilink")》，[艾立克·洛克·艾丁生著](../Page/艾立克·洛克·艾丁生.md "wikilink")
  - 《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》，[克利夫·史戴普·路易斯著](../Page/克利夫·史戴普·路易斯.md "wikilink")
  - 《[魔戒](../Page/魔戒.md "wikilink")》，[約翰·羅納德·瑞爾·托爾金著](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")
  - 《[莫信者湯瑪斯‧考佛南特傳奇](../Page/莫信者湯瑪斯‧考佛南特傳奇.md "wikilink")》，[史帝芬·唐納森著](../Page/史帝芬·唐納森.md "wikilink")
  - 《[沙娜拉之劍](../Page/沙娜拉之劍.md "wikilink")》，[特里·布魯克斯著](../Page/特里·布魯克斯.md "wikilink")
  - 《[時光之輪](../Page/時光之輪.md "wikilink")》，[羅伯特·喬丹著](../Page/羅伯特·喬丹.md "wikilink")

## 歷史奇幻

**歷史奇幻**（**Historical
fantasy**）有兩種不同的類型，第一種是將時間設在歷史的過去，但有著奇幻的元素，如同當代奇幻將時間設定在現在一樣；第二種是設定在一個奇幻世界裡，這個奇幻世界和我們的世界緊密地平行著，有著可辨識的國家、歷史事件或歷史人物等類比。

  - 《The Russian Stories》，[C.J.切莉著](../Page/C.J.切莉.md "wikilink")
  - 《[賽倫廷壁畫](../Page/賽倫廷壁畫.md "wikilink")》，[凱佛列·凱伊](../Page/凱佛列·凱伊.md "wikilink")
  - 《Jonathan Strange & Mr
    Norrell》，[蘇珊·克拉克](../Page/蘇珊·克拉克.md "wikilink")
  - 《[冰與火之歌](../Page/冰與火之歌.md "wikilink")》，[喬治·馬丁](../Page/喬治·馬丁.md "wikilink")
  - 《Prince of Nothing》，[斯哥特·拜克](../Page/斯哥特·拜克.md "wikilink")

## 少年奇幻

**少年奇幻**（**Juvenile fantasy**）較偏向少年取向，且通常會和其他的類型有所重合。

  - [喬安·羅琳](../Page/喬安·羅琳.md "wikilink")：《[哈利波特](../Page/哈利波特.md "wikilink")》
  - [克利夫·史戴普·路易斯](../Page/克利夫·史戴普·路易斯.md "wikilink")：《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》
  - [法蘭克·鮑姆](../Page/法蘭克·鮑姆.md "wikilink")：《[綠野仙蹤](../Page/綠野仙蹤_\(童話\).md "wikilink")》
  - [諾頓·加斯特](../Page/諾頓·加斯特.md "wikilink")：《[幻像天堂](../Page/幻像天堂.md "wikilink")》
  - [瑪麗·諾頓](../Page/瑪麗·諾頓.md "wikilink")：《[地板下的小矮人](../Page/地板下的小矮人.md "wikilink")》
  - [肯尼思·格拉姆](../Page/肯尼思·格拉姆.md "wikilink")：《[柳林中的風聲](../Page/柳林中的風聲.md "wikilink")》

## 低度奇幻

**低度奇幻**（**Low
fantasy**）不是一個純粹的子類型，而是表示一個以定義較明確的[嚴肅奇幻相反方式書寫的奇幻文學作品](../Page/嚴肅奇幻.md "wikilink")。亦即，是指嘗試著不去強調魔法的奇幻小說、設定在真實世界裡的奇幻小說、含有[現實主義和較悲觀世界觀的奇幻小說和](../Page/現實主義.md "wikilink")[黑暗奇幻等小說類型](../Page/黑暗奇幻.md "wikilink")。

## 世態奇幻

世態奇幻是[世態喜劇在奇幻小說裡的類比](../Page/世態喜劇.md "wikilink")。其世界裡有著極複雜的社會階層，且劇情圍繞在角色們因其不同階層而產生的互動，以[簡·奧斯丁和](../Page/簡·奧斯丁.md "wikilink")[安東尼·霍普的作品為其精典](../Page/安東尼·霍普.md "wikilink")。

許多的世態奇幻依其設定，可以被歸類為[架空歷史小說](../Page/奇幻小說的子類型#架空歷史小說.md "wikilink")、[嚴肅奇幻或](../Page/奇幻小說的子類型#嚴肅奇幻.md "wikilink")[歷史奇幻](../Page/奇幻小說的子類型#歷史奇幻.md "wikilink")。此一子類型是由其氛圍、情節和角色間應對的核心與禮儀等來標示的。

  - 《Swordspoint》，[埃倫·庫什納著](../Page/埃倫·庫什納.md "wikilink")（1987年）
  - 《[王后的項練](../Page/王后的項練.md "wikilink")》，[德雷莎·埃哲頓著](../Page/德雷莎·埃哲頓.md "wikilink")
  - 《The Death of the
    Necromancer》，[瑪莎·威爾斯著](../Page/瑪莎·威爾斯.md "wikilink")

## 神話虛構作品

**神話虛構作品**（**Mythic
fiction**）是一個很常使用在歷史和現實世界奇幻（包括以主流小說發表的奇幻作品）的詞彙，表示以使用了神話和童話的架構、符號和原型。神話奇幻和[都會奇幻有所重合](../Page/都會奇幻.md "wikilink")，且有時會交替地使用著，但神話小說也包括非設定在都會裡的當代作品。

雖然其神話上的基礎通常「很」少，但其會使用類似的神話角色的原型（如[惡作劇精靈或](../Page/惡作劇精靈.md "wikilink")[雷神](../Page/雷神.md "wikilink")）。此一類型和許多其他奇幻的類型不同（童話奇幻則通常會是例外），如[托爾金的作品](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")，便通常會創造屬於它們自己的傳說和童話，且有著全新的眾神或試圖以其他名稱來仿造現實存在的神話。

  - 《[任我遨翔](../Page/任我遨翔.md "wikilink")》，[查爾斯·迪·林特著](../Page/查爾斯·迪·林特.md "wikilink")
  - 《[失竊的孩子](../Page/失竊的孩子.md "wikilink")》，[凱斯·唐納修著](../Page/凱斯·唐納修.md "wikilink")
  - 《The Antelope Wife》，[路易絲·厄迪茲著](../Page/路易絲·厄迪茲.md "wikilink")
  - 《Strandloper》，[艾倫.加納著](../Page/艾倫.加納.md "wikilink")
  - 《Anansi Boys》，[尼爾·蓋曼著](../Page/尼爾·蓋曼.md "wikilink")
  - 《Mythago Wood》，[羅柏·霍史道克著](../Page/羅柏·霍史道克.md "wikilink")
  - 《[魔法的限度](../Page/魔法的限度.md "wikilink")》，[格拉漢姆·喬伊斯著](../Page/格拉漢姆·喬伊斯.md "wikilink")
  - 《[飢餓之路](../Page/飢餓之路.md "wikilink")》，[班·歐克里著](../Page/班·歐克里.md "wikilink")
  - 《Troll, A Love Story》，[約翰娜·斯尼薩洛著](../Page/約翰娜·斯尼薩洛.md "wikilink")
  - 《The Wood Wife》，[泰芮·溫德琳著](../Page/泰芮·溫德琳.md "wikilink")
  - 《[Rise of the Imperial
    Guardians](https://www.amazon.com/dp/1494883252)》，[蘆葦草著](../Page/蘆葦草.md "wikilink")
  - 《[Sacred Weapons of Terra
    Ocean](https://www.amazon.com/dp/1495459411)》，[蘆葦草著](../Page/蘆葦草.md "wikilink")
  - 《[Realm of
    Chaos](https://www.amazon.com/dp/1511949112)》，[蘆葦草著](../Page/蘆葦草.md "wikilink")
  - 《[Legend of Terra
    Ocean](https://www.amazon.com/dp/1533690510)》，[蘆葦草著](../Page/蘆葦草.md "wikilink")

### 神話龐克

[神話龐克是](../Page/神話龐克.md "wikilink")[神話小說的一個子類型](../Page/神話小說.md "wikilink")，被Catherynne
M.
Valente和其他美國奇幻作家用來定義非現實小說的一種特定類型，大部份的作家是女性的，開始於童話和神話裡，並由此處以都會奇幻、自白詩、非線性故事、語言柔軟操、世界建構和學園奇幻等後現代奇幻手法來鋪呈。

## 奇幻羅曼史

**奇幻羅曼史**（**Romantic
fantasy**）的情節集中在主角間的戀愛關係，和包含奇幻元素的情節和設定，在奇幻小說和羅曼史的類別中都曾經出版到。

雖然此一子類型可以和其他的奇幻子類型有重合之處，因為其和其他奇幻類型的不同之處並不在奇幻的元素和設定上，但大部份的奇幻羅曼史還是會有戀愛關係之外的一些設定元素。奇幻羅曼史通常有著是英勇女戰士的主角，且在大部份的此類書中，女性的士兵和佣兵也常常出現在故事之中。

  - 《The Door into Fire》，[戴安·杜尼著](../Page/戴安·杜尼.md "wikilink")（1979年）
  - 《Arrows of the
    Queen》，[瑪賽靼絲.雷克希著](../Page/瑪賽靼絲.雷克希.md "wikilink")（1987年）
  - 《Wild Magic》，[塔莫拉·皮爾士著](../Page/塔莫拉·皮爾士.md "wikilink")（1992年）

## 科學奇幻

奇幻小說和[科幻小說相接共享的子類型稱為](../Page/科幻小說.md "wikilink")**科學奇幻**（**Science
fantasy**），有著許多如太空旅遊和電射槍等科幻小說的設定，但也包括比起科學，更偏向魔法的重要元素，或在某些其他的部份，比起科幻小說，描繪得更接近於奇幻小說。科學奇幻最有名的一個例子即是[星際大戰](../Page/星際大戰.md "wikilink")，有著太空船且故事發生在外星球上，但也有一個是神氣活現的騎士的主角、一個不幸的公主、以及一個征服了全銀河的黑魔法師、與一個稱做「原力」的魔法力量、甚至還有一個開頭：「很久很久以前，在一個遙遠的星系」。

  - 《[星際大戰](../Page/星際大戰.md "wikilink")》
  - [瑪麗安·紀默·布蕾利所著的](../Page/瑪麗安·紀默·布蕾利.md "wikilink")《[黑暗星球](../Page/黑暗星球.md "wikilink")》系列
  - [沙兵宇宙也可以被視為是科學奇幻](../Page/沙兵宇宙.md "wikilink")。

## 超級英雄虛構作品

**超級英雄虛構作品**（**Superhero
fiction**）起始於美國的[漫画书裡](../Page/漫画书.md "wikilink")，並演化成了科學奇幻和當代奇幻的組合。亦即，這是一個基本設定在當代的世界裡，並且是個由外星生命及未來科術至魔法與古代神話生物等奇幻的概念都有可能共存的世界。而且，其中的主要角色是有著奇幻的能力、技巧或裝備，並穿著固定服裝的英雄。

  - 《Lois & Clark: A Superman
    Novel》，[C.J.切莉著](../Page/C.J.切莉.md "wikilink")（1996年）
  - 《Wild Cards》，[喬治·馬丁著](../Page/喬治·馬丁.md "wikilink")（連載）
  - 《[魔光奇兵](../Page/魔光奇兵.md "wikilink")》

## 劍與魔法

由[勞勃·霍華的作品](../Page/勞勃·霍華.md "wikilink")，特別是《[蠻王科南](../Page/蠻王科南.md "wikilink")》系列作開始確立了此一類型風格。劍與魔法比嚴肅奇幻更關注於身體上的威脅和動作的場面上。更甚之，比起嚴肅奇幻，劍與魔法偏向於描繪[非道德性的主角和](../Page/非道德性.md "wikilink")/或世界－很少有客觀的價值或某種形式的絕對正義。即使主角的行動是道德的，且偶爾因此而有些好的作為，但一般主角的動機都會是自私的。

  - 《[蠻王科南](../Page/蠻王科南.md "wikilink")》系列，[勞勃·霍華著](../Page/勞勃·霍華.md "wikilink")
  - 《Jirel of Joiry》系列，[C·L·摩爾著](../Page/C·L·摩爾.md "wikilink")
  - 《[永恆戰士](../Page/永恆戰士.md "wikilink")》系列，[麥克·摩考克著](../Page/麥克·摩考克.md "wikilink")
  - 《[法夫哈德和老練的搜尋者](../Page/法夫哈德和老練的搜尋者.md "wikilink")》系列，[福利茲·雷柏著](../Page/福利茲·雷柏.md "wikilink")
  - 《Kane》系列，[卡爾·華格納著](../Page/卡爾·華格納.md "wikilink")
  - 《War of the Spider
    Queen》系列，由[R·A·薩爾瓦多構思](../Page/R·A·薩爾瓦多.md "wikilink")，許多位作家著作

[de:Fantasy\#Subgenres](../Page/de:Fantasy#Subgenres.md "wikilink")

[奇幻類型](../Category/奇幻類型.md "wikilink")
[Category:奇幻文學](../Category/奇幻文學.md "wikilink")