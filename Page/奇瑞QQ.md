**奇瑞QQ** （代号 *S11*）
是[中国汽车制造商](../Page/中国.md "wikilink")[奇瑞汽车公司于](../Page/奇瑞汽车有限公司.md "wikilink")2003年推出的一款[微型车](../Page/微型车.md "wikilink")。在2006年奇瑞公司推出其三箱版本升级版[奇瑞QQ6后](../Page/奇瑞QQ6.md "wikilink")，原车型更名为QQ3。

## 历史

在中国大陆市场该车型售价3万到4.5万[人民币](../Page/人民币.md "wikilink")，在欧洲市场售价约在5000[欧元左右](../Page/欧元.md "wikilink")，取代[雷诺公司的Dacia](../Page/雷诺.md "wikilink")
Logan成为了市场上最便宜的车型。在[伊朗由当地的莫迪兰公司](../Page/伊朗.md "wikilink")（Modiran
Vehicle Manufacturing Company）引进组装生产，并取名以MVM 110销售。

奇瑞QQ配有以下两种排量的引擎（均符合欧III标准）:

  - **0.8 L** SQR372 I3 DOHC 12v — 38 kW at 6000 rpm, 70 N·m at 3500 rpm
  - **1.1 L** SQR472F I4 DOHC 16v — 50 kW at 6000 rpm, 90 N·m at
    3500 rpm

## 争议

该车型自诞生之日其外型就在知识产权方面饱受的争议，[通用汽车公司宣称奇瑞QQ抄袭了该公司旗下的产品](../Page/通用汽车公司.md "wikilink")[雪佛兰乐弛](../Page/雪佛兰.md "wikilink")（Chevrolet
Spark）与[大宇](../Page/大宇.md "wikilink")
Matiz，并将其告上法院。\[1\]通用公司行政人员甚至声称QQ车的车门无须修改就可以用在雪佛兰乐弛上。\[2\]

## 外部链接

  - [奇瑞QQ官方网站](https://web.archive.org/web/20070127104336/http://www.chery.cn:8080/web/product/qqproduct_index.asp)

  - [Chery QQ Global
    site](https://web.archive.org/web/20070125202618/http://www.cheryglobal.com/qq.htm)
  - [Chery QQ news
    site](https://web.archive.org/web/20070127175819/http://www.china-motors.ru/english/)
  - [Chery QQ Forums](http://www.chinacarforums.com)

## 注释

<references/>

[Category:都市車](../Category/都市車.md "wikilink")
[Category:奇瑞車輛](../Category/奇瑞車輛.md "wikilink")

1.  [中华网新闻：QQ与Matiz Spark惊人相似
    奇瑞遭通用大宇起诉](http://news.china.com/zh_cn/finance/11009723/20050508/12293742.html)

2.