**-{zh:奇異果;zh-hans:猕猴桃;zh-hant:獼猴桃;zh-hk:奇異果;zh-tw:奇異果;zh-mo:奇異果;}-**，又稱**-{zh:獼猴桃;zh-hans:奇异果;zh-hant:奇異果;zh-hk:獼猴桃;zh-tw:獼猴桃;zh-mo:獼猴桃;}-**、**-{zh:中國醋栗;zh-hans:中国醋栗;zh-hant:中國醋栗;zh-hk:中國醋栗;zh-tw:中國醋栗;zh-mo:中國醋栗;}-**，是[獼猴桃屬多種](../Page/獼猴桃屬.md "wikilink")[木質](../Page/木本植物.md "wikilink")[攀緣植物可食漿果的名字](../Page/攀緣植物.md "wikilink")。\[1\]\[2\]其他中文名稱還有**猕猴梨**、**藤梨**、**毛梨**、**羊桃**、**陽桃**、**几维果**、**木子**與**毛木果**\[3\]等。最常見的獼猴桃品種（'Hayward'）是橢圓形，約一顆大型[雞蛋的大小](../Page/雞蛋.md "wikilink")，長度為5-8厘米（2.0-3.1英寸），直徑為4.5-5.5厘米（1.8-2.2英寸）。
它具有纖維狀、暗綠色的棕色皮膚、明亮的綠色或金色的果肉、可食用的黑色種子。它有柔軟的質地，甜而獨特的味道。它是幾個國家的商業作物，如[新西蘭](../Page/新西蘭.md "wikilink")、[智利](../Page/智利.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[希臘和](../Page/希臘.md "wikilink")[法國](../Page/法國.md "wikilink")。\[4\]

## 历史

此物種原生地為中國，1904年[伊莎貝爾女士把](../Page/伊莎貝爾.md "wikilink")[中國](../Page/中國.md "wikilink")[湖北](../Page/湖北.md "wikilink")[宜昌的獼猴桃種子帶回紐西蘭之後](../Page/宜昌.md "wikilink")，把种子转送给当地的果树专家，之后辗转送到当地知名的园艺专家亚历山大手中，培植出[新西兰第一株奇异果树](../Page/新西兰.md "wikilink")。迄今为止，新西兰出产的绿色奇异果「kiwifruit」的名字已经风行全世界，而新西兰在奇异果营销、研发和价格方面的影响力，也是世界第一。\[5\]\[6\]
除綠色奇異果外，新西蘭還育出了一種「金奇異果」味道較清甜，酸味弱。现在主要出产国家有[新西兰](../Page/新西兰.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[法國](../Page/法國.md "wikilink")、中国、[智利等](../Page/智利.md "wikilink")。

## 營養

## 圖庫

`Image:KiwifarmHP356.JPG|`[`印度`](../Page/印度.md "wikilink")[`喜马偕尔邦猕猴桃`](../Page/喜马偕尔邦.md "wikilink")[`果园`](../Page/果园.md "wikilink")
`Image:Kiwi-Flower, male.jpg|雌花`
`Image:Kiwi-maennliche-bluete.jpg|雄花`
`Image:Kiwifruitlg.jpg|刨切猕猴桃`
`Image:Kiwi (Actinidia chinensis) 1 Luc Viatour.jpg|猕猴桃`

## 功效

1.
2.  奇異果含豐富的纖維質、果膠及胺基酸，有助於改善消化不良、食慾不振、尿路結石、關節炎、高血壓、心血管疾病、肝斑等。\[7\]

3.
4.
5.
## 參考文獻

  - 《台灣蔬果實用百科第三輯》，薛聰賢 著，薛聰賢出版社，2003年

[Category:水果](../Category/水果.md "wikilink")
[Category:獼猴桃屬](../Category/獼猴桃屬.md "wikilink")
[Category:原產於亞洲的水果](../Category/原產於亞洲的水果.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [奇異果-惟德堂藥房](http://weidetang.weebly.com/blog/kiwi)