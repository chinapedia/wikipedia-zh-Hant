[The_Black_Man.jpg](https://zh.wikipedia.org/wiki/File:The_Black_Man.jpg "fig:The_Black_Man.jpg")

**奈亚拉托提普**（）是美国小说家[霍华德·菲利普·洛夫克拉夫特所创造的](../Page/霍华德·菲利普·洛夫克拉夫特.md "wikilink")[克苏鲁神话中的一个邪恶存在](../Page/克苏鲁神话.md "wikilink")，在为克苏鲁神话构建的体系中，奈亚拉托提普为外神的統帥阿撒托斯的使者；它也是在以克苏鲁神话为背景的小说中经常出现的一个角色。

## 神话中的奈亚拉托提普

奈亚拉托提普是以[阿撒托斯為首的](../Page/阿撒托斯.md "wikilink")[外神們在地球上的使者兼代行者](../Page/外神.md "wikilink")，它常常化作人形在地球上行走，通常表現為一個高大、纖瘦、歡快、膚色黝黑的男人形象。奈亚拉托提普总是热衷于欺骗、诱惑人类，并以使人类陷入恐怖与绝望为其最高的喜悦。在克苏鲁神话中，它的形象最接近于传统“[恶魔](../Page/恶魔.md "wikilink")”與詐欺師（Trickster）的概念。

在[罗伯特·A·布洛奇](../Page/罗伯特·A·布洛奇.md "wikilink")（Robert Albert
Bloch）的短篇小说《來自尖塔的陰影》（The Shadow from the
Steeple）中，甚至暗示是奈亚拉托提普唆使人类造出了[核武器](../Page/核武器.md "wikilink")。

在奧古斯特·威廉·德雷斯的短篇小說《黑暗住民》（The Dweller in
Darkness）裡與旧日支配者中，作为象征“火”的存在的[克图格亚互相對抗](../Page/克图格亚.md "wikilink")。
而在小說《未知卡達斯的幻夢探求》（The Dream-Quest of Unknown Kadath）中也於幻夢境中與古神（Elder
Gods）之一的深淵大帝諾登斯（Nodens）敵對，各自在夢幻境中擁有一定的勢力相抗，兩者在抗衡的同時卻也共同庇護著古老者（Great
Ones）。

## 奈亚拉托提普的化身

  - 伏行之混沌（The Crawling Chaos）
  - 無貌之神（The Faceless God）
  - 無貌的獅身人面（The Faceless Sphinx）
  - 黑法老（The Black Pharaoh）
  - 腫脹之女（Bloated Woman）
  - 伏行之霧（Crawling Mist）
  - 黑暗惡魔（Dark Demon）
  - 黑暗者（Dark One）
  - 浮動的恐懼（The Floating Horror）
  - 夜吼者（Howler in the Dark）
  - 黃色假面覆蓋之物（The Thing in the Yellow Mask ）
  - 獵黑行者（The Haunter of the Dark）
  - 暗之住民（The Dweller in Darkness）

從與普通人類無異的樣貌到混亂不定形的肉團怪物，擁有上千個以上顯現的奈亞拉托提普在各种不同的作品里，有着许许多多的[化身](../Page/化身.md "wikilink")（Avatar）和称号，有「持擁千面之神」或「千貌之神」之稱。

## 相关物品

闪耀的[偏方三八面体是召唤奈亚拉托提普的化身的神器](../Page/偏方面体.md "wikilink")。

于《》（又译《夜魔》）中提到，该物品若置于有光线照耀的地方便不会启动，相反若置于无光之处封存便会召唤出奈亚拉托提普的化身。

## 登場作品

  - 克蘇魯神話小說作品

<!-- end list -->

  - 《奈亚拉托提普》（Nyarlathotep）
    [霍華德·菲利普·洛夫克拉夫特](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")（Howard
    Phillips Lovecraft）著
  - 《魔女屋之夢》（The Dreams in the Witch House）
    [霍華德·菲利普·洛夫克拉夫特](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")（Howard
    Phillips Lovecraft）著
  - 《未知卡達斯的幻夢探求》（The Dream-Quest of Unknown Kadath）
    [霍華德·菲利普·洛夫克拉夫特](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")（Howard
    Phillips Lovecraft）
  - 《獵黑行者》（The Haunter of the Dark）
    [霍華德·菲利普·洛夫克拉夫特](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")（Howard
    Phillips Lovecraft）著
  - 《來自尖塔的陰影》（The Shadow from the Steeple）
    [罗伯特·A·布洛奇](../Page/罗伯特·A·布洛奇.md "wikilink")（Robert
    Albert Bloch）著
  - 《無貌之神》（The Faceless God）
    [罗伯特·A·布洛奇](../Page/罗伯特·A·布洛奇.md "wikilink")（Robert
    Albert Bloch）著
  - 《黒法老的神殿》（Fane of the Black Pharaoh）
    [罗伯特·A·布洛奇](../Page/罗伯特·A·布洛奇.md "wikilink")（Robert
    Albert Bloch）著
  - 《黒法老的詛咒》（Curse of the Black Pharaoh）
    [林·卡特](../Page/林·卡特.md "wikilink") （Lin Carter）著
  - 《黑暗住民》（The Dweller in Darkness）
    [奧古斯特·威廉·德雷斯](../Page/奧古斯特·威廉·德雷斯.md "wikilink")（August
    William Derleth）著

<!-- end list -->

  - 其他作品

<!-- end list -->

  - [斬魔大聖](../Page/斬魔大聖.md "wikilink")（ナイア）

  - [新本格魔法少女莉絲佳](../Page/新本格魔法少女莉絲佳.md "wikilink")（ニャルラトテップ）

  - [襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")（ニャル子） - 作品中外星種族名稱。

  - [鋼炎のソレイユ](../Page/鋼炎のソレイユ.md "wikilink")（無貌なる神）

  - [しゅぷれ～むキャンディ](../Page/Supreme_Candy.md "wikilink")（ニャルラトテップ）

  - [素晴らしき日々 〜不連続存在〜](../Page/美好的每一天.md "wikilink")（ないあらとほてっぷ、這い寄る混沌）

  - [魔法少女プリティ☆ベル](../Page/魔法少女プリティ☆ベル.md "wikilink")（ニャルラトテップ）

  - COCO\!內連載漫畫，[逆境的黑獵師](../Page/逆境的黑獵師.md "wikilink")，五魔將軍內的黑魔將軍，名為奈亞。雖是提亞姆特所管領的五魔將軍之一，但似乎不是忠於她，而是另有其人

  - (ニャルラトテプ) 鬃狗獸人的原因捏他原著\[無貌之神\]。

[de:Cthulhu-Mythos\#Nyarlathotep](../Page/de:Cthulhu-Mythos#Nyarlathotep.md "wikilink")

[N](../Category/克苏鲁神话.md "wikilink") [N](../Category/文学角色.md "wikilink")
[N](../Category/邪恶神.md "wikilink") [N](../Category/虛構變身者.md "wikilink")