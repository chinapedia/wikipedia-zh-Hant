**奉俊昊**（，），[韓國電影](../Page/韓國電影.md "wikilink")[導演](../Page/導演.md "wikilink")、[編劇](../Page/編劇.md "wikilink")。

## 生平

處女作是1993年短片《白色人》。2000年，《绑架门口狗》作為他的长篇处女作。2003年，他執導了《[殺人回憶](../Page/殺人回憶.md "wikilink")》，這部作品改编自真人真事，講述十幾件連環殺人事件，上映當時吸引了五百万人觀看。奉俊昊也因此被譽為當年度最佳導演，以及獲得國際賞識。

他的第三部作品是2006年上映的電影《[駭人怪物](../Page/駭人怪物.md "wikilink")》，打破了南韓電影史賣座諸多記錄。而这部作品的構思是源自奉俊昊對於韓國在1990年代後期所發生的一連串事件的感悟，這些場景都完整的呈現在作品裡。

## 導演作品

  - 2000年：《[綁架門口狗](../Page/綁架門口狗.md "wikilink")》（Barking Dogs Never
    Bite）
  - 2003年：《[殺人回憶](../Page/殺人回憶.md "wikilink")》（Memories of Murder）
  - 2006年：《[駭人怪物](../Page/駭人怪物.md "wikilink")》（The Host）
  - 2008年：《[東京](../Page/東京_\(電影\).md "wikilink")》（Tokyo\!）
  - 2009年：《[非常母親](../Page/非常母親.md "wikilink")》（Mother）
  - 2013年：《[末日列車](../Page/末日列車.md "wikilink")》（Snow Piercer） \[1\]
  - 2017年：《[玉子](../Page/玉子_\(電影\).md "wikilink")》（Okja）
  - 2019年：《[寄生蟲](../Page/寄生蟲_\(電影\).md "wikilink")》（Parasite）

## 其他作品

  - 2014年：《[海霧](../Page/海霧_\(電影\).md "wikilink")》（製作、劇本）

## 獲獎

  - 2006年MBC大韩民国电影大奖，最佳導演獎
  - 第11屆[釜山國際電影節](../Page/釜山國際電影節.md "wikilink")，特別導演獎
  - 第五屆大韓民國電影大賞，最佳導演賞
  - 第27届（2007年）波尔图国际电影节，最佳導演獎

## 参考资料

## 外部連結

  -
  - [The Bong Joon-ho Page](http://www.koreanfilm.org/bongjoonho.html)

  - [Korea Society Podcast: Q\&A with Director Bong Joon-Ho about *The
    Host*](http://www.koreasociety.org/dmdocuments/2007-02-27-thehost-bongjoonho.mp3)

  - [Interview with the director at
    *KoreanFilm.org*](http://www.koreanfilm.org/bongjh.html). by
    Giuseppe Sedia

[B](../Category/韩国编剧.md "wikilink") [B](../Category/韓國導演.md "wikilink")
[B](../Category/韓國電影導演.md "wikilink")
[B](../Category/延世大學校友.md "wikilink")
[B](../Category/大邱廣域市出身人物.md "wikilink")
[B](../Category/奉姓.md "wikilink")
[Category:亚洲电影大奖最佳编剧得主](../Category/亚洲电影大奖最佳编剧得主.md "wikilink")

1.