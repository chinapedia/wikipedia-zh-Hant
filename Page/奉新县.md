**奉新县**在[江西省西北部](../Page/江西省.md "wikilink")，为[宜春市下辖的一个县](../Page/宜春市.md "wikilink")。

## 历史沿革

奉新县[春秋时属](../Page/春秋.md "wikilink")[吴](../Page/吴.md "wikilink")，[战国时属](../Page/战国.md "wikilink")[楚](../Page/楚.md "wikilink")，[秦属](../Page/秦.md "wikilink")[九江郡](../Page/九江郡.md "wikilink")，[汉初属](../Page/汉.md "wikilink")[豫章郡](../Page/豫章郡.md "wikilink")。

[汉景帝三年](../Page/汉景帝.md "wikilink")（公元前154年）置海昏县。

[和帝](../Page/和帝.md "wikilink")[永元十六年](../Page/永元.md "wikilink")（公元104年）分海昏置[建昌县](../Page/建昌县.md "wikilink")。

[灵帝](../Page/灵帝.md "wikilink")[中平二年](../Page/中平.md "wikilink")（公元185年）分[海昏](../Page/海昏.md "wikilink")、[建昌两地](../Page/建昌.md "wikilink")，设置新吴县，县治在今[会埠故县](../Page/会埠.md "wikilink")，属[豫章](../Page/豫章.md "wikilink")。

[唐中宗](../Page/唐中宗.md "wikilink")[神龙二年](../Page/神龙.md "wikilink")（公元706年）县衙由故县迁至冯川。

[南唐](../Page/南唐.md "wikilink")[保太元年](../Page/保太.md "wikilink")（公元973年）[吴把帝位让于唐](../Page/吴.md "wikilink")，唐嫌“吴”字，遂改新吴为奉新。

公元[1949年](../Page/1949.md "wikilink")7月8日，中共占领[解放](../Page/解放.md "wikilink")。

公元1958年前属[南昌专区管辖](../Page/南昌专区.md "wikilink")。

公元1959年1月始划归[宜春地区管辖](../Page/宜春地区.md "wikilink")。

公元2000年8月[宜春撤地设市](../Page/宜春.md "wikilink")，仍辖奉新县。 　　

## 地理

位于[修水支流潦水上游](../Page/修水.md "wikilink")；境内以[山地为主](../Page/山地.md "wikilink")。县境三面环山，地势西高东低，西部中低山地、中部多丘陵、东部低丘河谷平原，属典型的丘陵山区地形地貌。
最高峰[五梅山](../Page/五梅山.md "wikilink")（海拔1516.3米），最低点[宋埠中堡](../Page/宋埠中堡.md "wikilink")（海拔27米），均匀海拔300米。[潦河水流贯境内西东](../Page/潦河.md "wikilink")。
属中亚热带潮湿天气，年均匀气温为17.3℃，年均匀降雨量为1612毫米，年相对湿度均匀为79%，无霜期年均匀为260天左右，年日照时数达1803小时。
国家商品粮、毛竹林基地，省猕猴桃开发基地。矿藏有花岗石、瓷土、萤石、钾长石等。举铜、干大、寺棠、万修等公途经境。

## 交通

226、232、313、314省道，[昌铜高速公路](../Page/昌铜高速公路.md "wikilink")。

## 经济

以农业为主，有一个江西省十大开发区之一的冯田经济开发区，目前正在建设另一个黄溪开发区，努力使奉新经济朝省内二十强努力，纺织，化工，制药原料，碳素材料产值占大头。

## 文化教育

高中学校只有一所奉新一中和奉新冶城职业学校。 中学 奉新二中 华林中学 阳光中学

## 风景名胜

  - [宋应星纪念馆](../Page/宋应星纪念馆.md "wikilink")
  - [奉新森林公园](../Page/奉新森林公园.md "wikilink")
  - [百丈山](../Page/百丈山.md "wikilink")
  - [越山](../Page/越山.md "wikilink")
  - [浮云八百洞](../Page/浮云八百洞.md "wikilink")
  - [四方牌楼](../Page/四方牌楼.md "wikilink")
  - [青云塔](../Page/青云塔.md "wikilink")
  - [百丈寺](../Page/百丈寺.md "wikilink")
  - [九仙汤温泉](../Page/九仙汤温泉.md "wikilink")
  - [萝卜潭瀑布](../Page/萝卜潭瀑布.md "wikilink")
  - [江南国际体育健身中心](../Page/江南国际体育健身中心.md "wikilink")
  - [天工开物园](../Page/天工开物园.md "wikilink")
  - [余城](../Page/余城.md "wikilink")
  - [大成殿](../Page/大成殿.md "wikilink")
  - [故县古县城](../Page/故县古县城.md "wikilink")
  - [岳纳堂](../Page/岳纳堂.md "wikilink")
  - [张勋庄园](../Page/张勋庄园.md "wikilink")
  - [万年宫牌坊](../Page/万年宫牌坊.md "wikilink")
  - [八百洞天](../Page/八百洞天.md "wikilink")

## 名人

  - [百丈禪師](../Page/百丈禪師.md "wikilink")：[唐朝](../Page/唐朝.md "wikilink")[禪宗高僧](../Page/禪宗.md "wikilink")
  - [刘慎虚](../Page/刘慎虚.md "wikilink")：[唐朝诗人](../Page/唐朝.md "wikilink")
  - [华林胡氏家族](../Page/华林胡氏家族.md "wikilink")：著名宗族
  - [袁去华](../Page/袁去华.md "wikilink")：著名词人
  - [宋应星](../Page/宋应星.md "wikilink")：[天工开物的作者](../Page/天工开物.md "wikilink")
  - [八大山人](../Page/八大山人.md "wikilink")：著名山水画家
  - [张勋](../Page/张勋.md "wikilink")：拥立[溥仪复辟的北洋军阀](../Page/溥仪.md "wikilink")
  - [赵晓](../Page/赵晓.md "wikilink")：中国大陆著名经济学家

[奉新县](../Page/category:奉新县.md "wikilink")
[县](../Page/category:宜春区县市.md "wikilink")
[宜春](../Page/category:江西省县份.md "wikilink")