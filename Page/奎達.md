**奎達**（）是[巴基斯坦](../Page/巴基斯坦.md "wikilink")[俾路支省首府](../Page/俾路支省.md "wikilink")，人口759,894（2006年估計）。位於靠近[阿富汗邊境的河谷](../Page/阿富汗.md "wikilink")，有公路通至西北面的[坎大哈](../Page/坎大哈.md "wikilink")。原归阿富汗，英国占领后归英属印度，巴基斯坦独立后归巴基斯坦。

## 辭源

奎達(Quetta)一詞也可拼成**Kuwatah**，為Kot一詞演變而來，該詞在[普什圖語中意為](../Page/普什圖語.md "wikilink")「堡」\[1\]\[2\]。

## 氣候

## 參考文獻

[Q](../Category/俾路支省城市.md "wikilink")
[Category:巴基斯坦省會](../Category/巴基斯坦省會.md "wikilink")

1.  ["History of Quetta" Government of
    Quetta](http://www.quetta.gov.pk/History.htm)
2.  頁67，楊翠柏，劉成瓊，《巴基斯坦史--清真之國的文化與歷史發展》-國別史，2005年5月，ISBN 957-14-4134-1