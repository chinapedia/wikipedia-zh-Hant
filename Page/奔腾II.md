**奔騰II**（Pentium
II）為[英特爾於](../Page/英特爾.md "wikilink")1997年5月7日推出的微處理器。它基於[Pentium
Pro使用的](../Page/Pentium_Pro.md "wikilink")[P6架構](../Page/P6.md "wikilink")，但加強16位元的效能，以及加入[MMX指令集](../Page/MMX.md "wikilink")。虽然MMX指令集最早出现在[Pentium
MMX](../Page/Pentium_MMX.md "wikilink") CPU，但是其后研发的所有型号的[Pentium
Pro都没有包含MMX指令集](../Page/Pentium_Pro.md "wikilink")，这影响在图形计算方面的性能。

## 產品簡介

**Pentium
II**為[英特爾推出的一枚](../Page/英特爾.md "wikilink")[X86架構處理器](../Page/X86.md "wikilink")，基於[Pentium
Pro使用的P](../Page/Pentium_Pro.md "wikilink")6微處理架構，但另一方面它的[16位元處理能力獲得改善](../Page/16位元.md "wikilink")，並加入[MMX指令集](../Page/MMX.md "wikilink")。[Pentium
Pro的](../Page/Pentium_Pro.md "wikilink")16位元计算性能有明显缺陷，这影响它在当时的Win9X操作系统上的性能。而Pentium
II改进这一重要问题。

第一代Pentium II核心代號為**Klamath**，使用350納米製程，就當時而言，其功耗算非常高的-{zh-hans:水平;
zh-hant:水準}-。推出時時脈只有233及266MHz，使用66MHz[前端匯流排](../Page/前端匯流排.md "wikilink")，後期另推出時脈300MHz的版本。

第二代Pentium
II核心代號為**Deschutes**，運行時脈為333MHz，於1998年1月推出，使用250納米製程，而且溫度亦有效降低。支援100MHz前端匯流排，英特爾於1998年另外推出了時脈為266、300、350、400、450的Pentium
II處理器。

1998年由于低時脈Pentium II产品的短缺，一度将250纳米制程工艺的Pentium II 400MHZ和Pentium II
450MHZ产品降低规格为Pentium II 266MHZ
(序号为SL2W7)和300MHZ(序号为SL2W8)销售，掀起了当时狂热的超频风。

基於Pentium II的電腦系統亦加入新世代的記憶體標準——SDRAM〔替代EDO RAM〕，以及AGP顯示卡。

與Pentium及Pentium Pro處理器不同，Pentium
II使用一種插槽式設計。處理器晶片與其他相關晶片皆在一塊類似子卡的電路板上，而電路板上有一塊塑膠蓋，有時亦有一風扇。Pentium
II亦把L2 Cache放到這電路板上，但只運行處理器時脈一半的速度。此舉增加處理器的良品率，從而減低製作成本。

這個方法亦可更容易改變Pentium II處理器的L2 Cache，可以使英特爾用相同的晶片，但可製作廉價低效能處理器，也可製貴價高效能處理器。

Pentium II的入門級處理器，為了減少L2
Cache〔甚至取消〕的[Celeron](../Page/Celeron.md "wikilink")。由於它低效能，所以一般的專業人士都不使用Celeron處理器，但因為它的可超频性〔L2
Cache比處理器晶片對時脈敏感〕，它亦有一定的市場，另外，亦由於Celeron A 版本中的L2
Cache為全速，因此其性能直迫Pentium II。

Pentium II的高階處理器，為[Pentium II
Xeon](../Page/Xeon.md "wikilink")。它針對伺服器設計，而且有一個全速的L2
Cache。它分別有512KB、1MB、2MB的版本。

Pentium II亦有流動版處理器，有256KB L2 Cache，但是內置在晶片內，為最快的Pentium II。

Pentium II於1999年被[Pentium III取代](../Page/Pentium_III.md "wikilink")。

## 核心

[Pentium_II_front.jpg](https://zh.wikipedia.org/wiki/File:Pentium_II_front.jpg "fig:Pentium_II_front.jpg")
[Pentium_II_inside_front.jpg](https://zh.wikipedia.org/wiki/File:Pentium_II_inside_front.jpg "fig:Pentium_II_inside_front.jpg")

### Klamath (350 nm)

  - L1：16 + 16 KB 〔資料 + 指令〕
  - L2：512 KB，位於處理器晶片外部，只有處理器時脈的50%
  - [Slot 1](../Page/Slot_1.md "wikilink")
  - [MMX](../Page/MMX.md "wikilink")
  - [FSB](../Page/FSB.md "wikilink")：66 MHz
  - VCore：2.8 V
  - 首次推出：1997年5月7日
  - 時脈：233, 266, 300 MHz

### Tonga (250 nm), Pentium II Mobile

  - L1：16 + 16 KB 〔資料 + 指令〕
  - L2：512 KB，位於處理器晶片外部，只有處理器時脈的50%
  - [MMC-1](../Page/MMC-1.md "wikilink"),
    [MMC-2](../Page/MMC-2.md "wikilink"),
    [Mini-Cartridge](../Page/Mini-Cartridge.md "wikilink")
  - [MMX](../Page/MMX.md "wikilink")
  - [FSB](../Page/FSB.md "wikilink")：66 MHz
  - VCore：1.6 V
  - 首次推出：?
  - 時脈：233, 266, 300 MHz

### Deschutes (250 nm)

  - L1：16 + 16 KB 〔資料 + 指令〕
  - L2：512 KB，位於處理器晶片外部，只有處理器時脈的50%
  - [Slot 1](../Page/Slot_1.md "wikilink")
  - [MMX](../Page/MMX.md "wikilink")
  - [FSB](../Page/FSB.md "wikilink"): 66, 100 MHz
  - VCore: 2.0 V
  - 首次推出：1998年1月26日
  - 時脈：266 - 450 MHz
      - 66 MHz FSB : 266, 300, 333 MHz
      - 100 MHz FSB: 350, 400, 450 MHz

### Dixon (250 nm), mobile Pentium II PE ("Performance Enhanced")

  - L1 cache: 16 + 16 KiB (Data + Instructions)
  - L2 cache: 256 KiB, on-die, full CPU speed
  - [BGA1](../Page/BGA1.md "wikilink"),
    [μPGA1](../Page/Micro-PGA1.md "wikilink") (GTL+)
  - [MMX](../Page/MMX.md "wikilink")
  - [Front side bus](../Page/Front_side_bus.md "wikilink"): 66 MHz
  - VCore: 1.5, 1.55, 1.6 V
  - 第一次發行：1999年1月25日
  - Clockrate: 266 - 400 MHz

## 外部連結

[Category:Intel x86处理器](../Category/Intel_x86处理器.md "wikilink")