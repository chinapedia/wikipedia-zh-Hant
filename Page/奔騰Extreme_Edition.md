[Intel-logo.svg](https://zh.wikipedia.org/wiki/File:Intel-logo.svg "fig:Intel-logo.svg")
****
是在2005年春天的開發者論壇中推出的一系列微處理器之品牌名稱。這個處理器是以雙核心的為基礎，但他擁有[超執行緒](../Page/超執行緒.md "wikilink")，因此任何的作業系統會看到四個邏輯的處理器（2x2實體核心）。這個處理器也支援和。它沒有鎖頻。早期的效能報告提到這個處理器最高能以空氣冷卻而在3.8GHz穩定執行。是在2005年早期推出，價格是999.99美元（）或1,200美元（零售）。

## 核心

| Intel Pentium D 處理器家族                                                                                                             |
| --------------------------------------------------------------------------------------------------------------------------------- |
| 起初標誌                                                                                                                              |
| 代號                                                                                                                                |
| [Pentium_D.jpg](https://zh.wikipedia.org/wiki/File:Pentium_D.jpg "fig:Pentium_D.jpg")                                            |
| [Pentium_Extreme_Edition.jpg](https://zh.wikipedia.org/wiki/File:Pentium_Extreme_Edition.jpg "fig:Pentium_Extreme_Edition.jpg") |
| <small>[Intel Pentium D 處理器列表](../Page/Intel_Pentium_D_處理器列表.md "wikilink")</small>                                               |

### Smithfield

它是第一代Pentium Extreme Edition所用的核心，跟Penitum D
8XX系列一樣，唯一不同的是它有[超執行緒技術](../Page/超執行緒.md "wikilink")。這個系列只有一枚處理Pentium
Extreme Edition 840。
這個系列必須使用[英特爾的i](../Page/英特爾.md "wikilink")955X或i975X及[NVIDIA的](../Page/NVIDIA.md "wikilink")[NForce
4 SLI Intel
Edition晶片組](../Page/NForce_4_SLI_Intel_Edition.md "wikilink")，使用i945系列晶片組的話會關閉超執行緒功能。

### Presler

它是第二代Pentium Extreme Edition的核心，跟Pentium 9XX系列一樣，但開啟了超執行緒技術，使用1066MHz
[FSB](../Page/FSB.md "wikilink")，以及解除了倍頻鎖。這個系列有兩枚處理器，分別是955及965。Presler必須搭配英特爾的i975X晶片組或NVIDIA的nForce4系列。

## 後繼者


英特爾將以使用[Intel Core微處理器架構的](../Page/Intel_Core微處理器架構.md "wikilink")[Intel
Core 2
Extreme取代Pentium](../Page/Intel_Core_2#Conroe_XE_\(Core_2_Extreme\).md "wikilink")
Extreme Edition。

## 參考

  - [Pentium D](../Page/Pentium_D.md "wikilink")
  - [Intel 微處理器列表](../Page/Intel_微處理器列表.md "wikilink")

[en:Pentium D\#Smithfield
XE](../Page/en:Pentium_D#Smithfield_XE.md "wikilink")

[Category:Intel x86处理器](../Category/Intel_x86处理器.md "wikilink")