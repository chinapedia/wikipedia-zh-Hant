**奥克兰市市长**是[奥克兰市政府首脑和](../Page/奥克兰市政府.md "wikilink")[奥克兰市议会的主席](../Page/奥克兰市议会.md "wikilink")。奥克兰市长的管辖范围仅限在[奥克兰市本身](../Page/奥克兰市.md "wikilink")。[奥克兰的周边地区如](../Page/奥克兰.md "wikilink"):[曼努考](../Page/曼努考.md "wikilink")，[北岸市和Waitakere都有自己本市的市长](../Page/北岸市.md "wikilink")。市长选举采取直选制。

[奥克兰于](../Page/奥克兰.md "wikilink")1851年成立了第一个地方政府，创立之初当时是一个自治市。在这个很短的时期内，奥克兰市市长为[Archibald
Clark](../Page/Archibald_Clark.md "wikilink")。
当[奥克兰于](../Page/奥克兰.md "wikilink")1871年真正设市的时候，[奥克兰市议会的最初的](../Page/奥克兰市议会.md "wikilink")[主席为](../Page/主席.md "wikilink")[Walter
Lee](../Page/Walter_Lee.md "wikilink")。后来没多久，真正意义上具有现代市长职能的奥克兰市长诞生了。自此以后，总共有39位人士先后担任过奥克兰市市长一职直到今天。其中[Dove-Myer
Robinson先后两次担任了长达](../Page/Dove-Myer_Robinson.md "wikilink")18年的奥克兰市长一职，[约翰·班克斯于](../Page/约翰·班克斯.md "wikilink")2007年击败了在2004年击败他当选**奥克兰市长**的[迪克·哈伯德](../Page/迪克·哈伯德.md "wikilink")，重新当选**奥克兰市长**。这两位也是唯二两次当选奥克兰市长的人士。在总共39位市长当中，有两位是女性市长，分别为：35任市长[凯瑟琳·蒂泽德](../Page/凯瑟琳·蒂泽德.md "wikilink")（1983-1990）和37任市长[凯瑟琳·弗莱彻](../Page/凯瑟琳·弗莱彻.md "wikilink")（1998——2001）。

2010年11月1日，奧克蘭市和其他臨近的城市合並組成新的奧克蘭市。最後一任奥克兰市市长为[约翰·班克斯](../Page/约翰·班克斯.md "wikilink")。

## 奥克兰市市长列表

| No.  | 姓名                                                               | 就职    | 离职    |
| ---- | ---------------------------------------------------------------- | ----- | ----- |
| 01   | [Philip Philips](../Page/Philip_Philips.md "wikilink")           | 1871年 | 1874年 |
| 02   | [亨利·艾萨克斯](../Page/亨利·艾萨克斯.md "wikilink")                         | 1874年 | 1874年 |
| 03   | [Frederick Prime](../Page/Frederick_Prime.md "wikilink")         | 1874年 | 1875年 |
| 04   | [Benjamin Tonks](../Page/Benjamin_Tonks.md "wikilink")           | 1875年 | 1876年 |
| 05   | [威廉·赫斯特](../Page/威廉·赫斯特.md "wikilink")                           | 1876年 | 1877年 |
| 06   | [亨利·布雷特](../Page/亨利·布雷特.md "wikilink")                           | 1877年 | 1878年 |
| 07   | [Thomas Peacock](../Page/Thomas_Peacock.md "wikilink")           | 1877年 | 1880年 |
| 08   | [詹姆斯·克拉克](../Page/詹姆斯·克拉克.md "wikilink")                         | 1880年 | 1883年 |
| 09   | [William Waddel](../Page/William_Waddel.md "wikilink")           | 1883年 | 1886年 |
| 10   | [Albert Devore](../Page/Albert_Devore.md "wikilink")             | 1886年 | 1889年 |
| 11   | [John·Upton](../Page/John·Upton.md "wikilink")                   | 1889年 | 1891年 |
| 12   | [James·Crowther](../Page/James·Crowther.md "wikilink")           | 1891年 | 1893年 |
| 13   | [James Holland](../Page/James_Holland.md "wikilink")             | 1893年 | 1896年 |
| 14   | [Abraham Boardman](../Page/Abraham_Boardman.md "wikilink")       | 1896年 | 1897年 |
| 15   | [Peter Dignan](../Page/Peter_Dignan.md "wikilink")               | 1897年 | 1898年 |
| 16   | [David Goldie](../Page/David_Goldie.md "wikilink")               | 1898年 | 1901年 |
| 17   | [John Logan Campbell](../Page/John_Logan_Campbell.md "wikilink") | 1901年 | 1901年 |
| 18   | [Alfred Kidd](../Page/Alfred_Kidd.md "wikilink")                 | 1901年 | 1903年 |
| 19   | [Edwin Mitchelson](../Page/Edwin_Mitchelson.md "wikilink")       | 1903年 | 1905年 |
| 20   | [Arthur Myers](../Page/Arthur_Myers.md "wikilink")               | 1905年 | 1909年 |
| 21   | [查爾斯·格雷](../Page/查爾斯·格雷_\(新西蘭\).md "wikilink")                   | 1909年 | 1910年 |
| 22   | [Lemuel Bagnall](../Page/Lemuel_Bagnall.md "wikilink")           | 1910年 | 1911年 |
| 23   | [Christopher Parr](../Page/Christopher_Parr.md "wikilink")       | 1911年 | 1915年 |
| 24   | [James Gunson](../Page/James_Gunson.md "wikilink")               | 1915年 | 1925年 |
| 25   | [George Baildon](../Page/George_Baildon.md "wikilink")           | 1925年 | 1931年 |
| 26   | [George Hutchison](../Page/George_Hutchison.md "wikilink")       | 1931年 | 1935年 |
| 27   | [Ernest Davis](../Page/Ernest_Davis.md "wikilink")               | 1935年 | 1941年 |
| 28   | [John Allum](../Page/John_Allum.md "wikilink")                   | 1941年 | 1952年 |
| 29   | [John Luxford](../Page/John_Luxford.md "wikilink")               | 1953年 | 1956年 |
| 30   | [Thomas Ashby](../Page/Thomas_Ashby.md "wikilink")               | 1956年 | 1957年 |
| 31   | [Keith Buttle](../Page/Keith_Buttle.md "wikilink")               | 1957年 | 1959年 |
| 32   | [Dove-Myer Robinson](../Page/Dove-Myer_Robinson.md "wikilink")   | 1959年 | 1965年 |
| 33   | [Roy McElroy](../Page/Roy_McElroy.md "wikilink")                 | 1965年 | 1968年 |
| 二次当选 | [Dove-Myer Robinson](../Page/Dove-Myer_Robinson.md "wikilink")   | 1968年 | 1980年 |
| 34   | [Colin Kay](../Page/Colin_Kay.md "wikilink")                     | 1980年 | 1983年 |
| 35   | [凯瑟琳·蒂泽德](../Page/凯瑟琳·蒂泽德.md "wikilink")                         | 1983年 | 1990年 |
| 36   | [Les Mills](../Page/Les_Mills.md "wikilink")                     | 1990年 | 1998年 |
| 37   | [凯瑟琳·弗莱彻](../Page/凯瑟琳·弗莱彻.md "wikilink")                         | 1998年 | 2001年 |
| 38   | [约翰·班克斯](../Page/约翰·班克斯.md "wikilink")                           | 2001年 | 2004年 |
| 39   | [迪克·哈伯德](../Page/迪克·哈伯德.md "wikilink")                           | 2004年 | 2007年 |
| 二次当选 | [约翰·班克斯](../Page/约翰·班克斯.md "wikilink")                           | 2007年 | 2010年 |
|      |                                                                  |       |       |

[\*](../Page/category:奧克蘭市長.md "wikilink")