| 数据                                                                                                        |
| --------------------------------------------------------------------------------------------------------- |
| 省会:                                                                                                       |
| 面积:                                                                                                       |
| 人口:                                                                                                       |
| 地图                                                                                                        |
| [Holguin_in_Cuba.svg](https://zh.wikipedia.org/wiki/File:Holguin_in_Cuba.svg "fig:Holguin_in_Cuba.svg") |

**奥尔金省**（**provincia de
Holguín**）是[古巴十四个省份之一](../Page/古巴.md "wikilink")，位于古巴岛的东部，2010年人口1,037,161人\[1\]
。主要城市包括省府[奥尔金](../Page/奥尔金.md "wikilink")、[巴内斯](../Page/巴内斯.md "wikilink")、[安蒂亚](../Page/安蒂亚_\(古巴\).md "wikilink")、[马亚里及](../Page/马亚里.md "wikilink")[莫阿](../Page/莫亚_\(古巴\).md "wikilink")。该省面积9300km²，为全岛面积第四大的省，其中25%为森林所覆盖。[菲德尔·卡斯特罗出生于该省的](../Page/菲德尔·卡斯特罗.md "wikilink")[比兰](../Page/比兰.md "wikilink")。

## 历史

[哥伦布于](../Page/哥伦布.md "wikilink")1492年10月27日在位于今天奥尔金省的[拉斐尔弗里尔的Bariay村登陆](../Page/拉斐尔弗里尔.md "wikilink")。他称这是他“所看到过的最美的国家”。

省府[奥尔金由西班牙征服者](../Page/奥尔金.md "wikilink")[加西亚·德·奥尔金](../Page/加西亚·德·奥尔金.md "wikilink")（García
de Holguín）于1545年4月4日带领其妻子及一直50-80人的小队建立\[2\]。并命名为奥尔金的圣伊西多尔（San Isidoro
de Holguín）。

奥尔金省于1978年从原东方省分出后成立。

## 经济

如古巴其他省份一样，奥尔金省的经济以[甘蔗为基础](../Page/甘蔗.md "wikilink")，另外还出产[玉米](../Page/玉米.md "wikilink")、[咖啡](../Page/咖啡.md "wikilink")。采矿业亦是该省的重要经济来源。

[莫阿有一座带有海运设施的大型](../Page/莫亚_\(古巴\).md "wikilink")[钴加工厂](../Page/钴.md "wikilink")，用于外国投资，主要是[加拿大](../Page/加拿大.md "wikilink")。其他矿物还有为[铬](../Page/铬.md "wikilink")、[镍及](../Page/镍.md "wikilink")[铁等](../Page/铁.md "wikilink")，其中镍矿储量全世界最高。

旅游业近来才得到发展，有包括[萨埃蒂亚岛及部分](../Page/萨埃蒂亚岛.md "wikilink")[亚历杭德罗·德洪堡国家公园等地的自然观光资源](../Page/亚历杭德罗·德洪堡国家公园.md "wikilink")。

## 行政区域

奥尔金省分为14个市：

<table>
<thead>
<tr class="header">
<th><p>市</p></th>
<th><p>人口<br />
(2004)</p></th>
<th><p>面积<br />
(km²)</p></th>
<th><p>坐标</p></th>
<th><p>附注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/安蒂亚_(古巴).md" title="wikilink">安蒂亚</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴瓜诺斯.md" title="wikilink">巴瓜诺斯</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴内斯.md" title="wikilink">巴内斯</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡科库姆.md" title="wikilink">卡科库姆</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卡利斯托加西亚.md" title="wikilink">卡利斯托加西亚</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td><p>治所在Buenaventura</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/库埃托.md" title="wikilink">库埃托</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弗兰克帕伊斯.md" title="wikilink">弗兰克帕伊斯</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td><p>治所在Cayo Mambí</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉希拉.md" title="wikilink">吉希拉</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥尔金.md" title="wikilink">奥尔金</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td><p>省府</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马亚里.md" title="wikilink">马亚里</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫亚_(古巴).md" title="wikilink">莫阿</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉斐尔弗里尔.md" title="wikilink">拉斐尔弗里尔</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萨瓜德塔纳莫.md" title="wikilink">萨瓜德塔纳莫</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乌尔瓦诺诺里斯.md" title="wikilink">乌尔瓦诺诺里斯</a></p></td>
<td></td>
<td></td>
<td><p><small></small></p></td>
<td></td>
</tr>
</tbody>
</table>

  -
    *资料来源：人口数量来自2004年普查。*\[3\] *面积来自1976年政区重组。*\[4\]

## 著名人物

  - [菲德尔·卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")
  - [劳尔·卡斯特罗](../Page/劳尔·卡斯特罗.md "wikilink")
  - [阿罗鲁迪斯·查普曼](../Page/阿罗鲁迪斯·查普曼.md "wikilink")

## 参考资料

[H](../Category/古巴省份.md "wikilink")

1.
2.
3.
4.