**奥帕瓦**（[捷克语](../Page/捷克语.md "wikilink")：****，[德语](../Page/德语.md "wikilink"):**Troppau**，**特罗保**）是位于[捷克](../Page/捷克.md "wikilink")[摩拉维亚-西里西亚州](../Page/摩拉维亚-西里西亚州.md "wikilink")[奥帕瓦河畔的一座城市](../Page/奥帕瓦河.md "wikilink")，人口约6万余（2008年）。在歷史上是[捷克西里西亞的首都](../Page/捷克西里西亞.md "wikilink")。

奥帕瓦从1195年开始出现在文献上，1224年获得城市自治权，成为[波希米亚王国的](../Page/波希米亚王国.md "wikilink")[西里西亚首府](../Page/西里西亚.md "wikilink")，后来成为[奥地利的奥帕瓦公爵领地](../Page/奥地利.md "wikilink")。以后一直处于[奥匈帝国的统治下](../Page/奥匈帝国.md "wikilink")，根据1910年奥匈帝国政府的统计，当时这里共有约3万居民，90%以上是以[德语为](../Page/德语.md "wikilink")[母语的](../Page/母语.md "wikilink")。[第一次世界大战后的](../Page/第一次世界大战.md "wikilink")1919年，奥帕瓦划归新独立的[捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")。1938年，被[纳粹德国占领](../Page/纳粹德国.md "wikilink")，1945年，回归捷克，德语居民被放逐。

现在的奥帕瓦已经成为捷克的采矿机械制造中心，也是學術、文化和商業都市，此地的西里西亚博物馆是捷克最古老的[博物馆](../Page/博物馆.md "wikilink")。

## 外部链接

  - [官方网站](http://www.opava-city.cz/)
  - [奥帕瓦国际管风琴大赛](http://konzervator.cz/organ/)

[O](../Category/捷克城市.md "wikilink")