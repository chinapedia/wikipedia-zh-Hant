**奥托·瓦拉赫**（，），[德国化学家](../Page/德国.md "wikilink")，1910年以研究[脂環族化合物而獲得](../Page/脂環族化合物.md "wikilink")[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。\[1\]\[2\]

## 生平

1847年，瓦拉赫出生於[柯尼斯堡](../Page/柯尼斯堡.md "wikilink")，他的父親是一位信仰[路德教的](../Page/路德教.md "wikilink")[猶太人](../Page/猶太人.md "wikilink")，職業是[普魯士公務員](../Page/普魯士.md "wikilink")；母親則是信仰[新教的](../Page/新教.md "wikilink")[德國人](../Page/德國人.md "wikilink")。他們後來因為父親工作而相繼搬家到[斯塞新](../Page/斯塞新.md "wikilink")、[波茨坦](../Page/波茨坦.md "wikilink")。瓦拉赫在[波茨坦開始上學](../Page/波茨坦.md "wikilink")，並開始對影響他未來生活的兩個重要學科起了興趣：[文學及](../Page/文學.md "wikilink")[藝術歷史](../Page/藝術歷史.md "wikilink")，他在這段時期也開始在他父母親的房子進行私人的化學實驗。

1867年，瓦拉赫進入[哥廷根大學學習化學](../Page/哥廷根大學.md "wikilink")，並師承於當時[有機化學領域十分有名的](../Page/有機化學.md "wikilink")[弗里德里希·維勒](../Page/弗里德里希·維勒.md "wikilink")；一學期後他進入[柏林洪堡大學](../Page/柏林洪堡大學.md "wikilink")，師承於[奧古斯特·威廉·馮·霍夫曼](../Page/奧古斯特·威廉·馮·霍夫曼.md "wikilink")。1869年，瓦拉赫在[哥廷根大學獲得](../Page/哥廷根大學.md "wikilink")[博士學位](../Page/博士.md "wikilink")，並在[波昂大學](../Page/波昂大學.md "wikilink")(1870年至1889年)及[哥廷根大學](../Page/哥廷根大學.md "wikilink")(1889年至1915年)擔任教授。瓦拉赫在1931年逝世於[哥廷根](../Page/哥廷根.md "wikilink")。

## 成就

瓦拉赫與[弗里德里希·奧古斯特·凱庫勒·馮·斯特拉多尼茨在](../Page/弗里德里希·奧古斯特·凱庫勒·馮·斯特拉多尼茨.md "wikilink")[波昂進行](../Page/波昂.md "wikilink")[精油內](../Page/精油.md "wikilink")[萜烯的系統分析](../Page/萜烯.md "wikilink")，這時僅有少數信息被解明，關於結構的資訊還是很零散。[熔點的比較及](../Page/熔點.md "wikilink")[混合物的測量是確認相同物質的方法之一](../Page/混合物.md "wikilink")，為了進行此方法，液體的[萜烯必須被轉換成結晶化合物](../Page/萜烯.md "wikilink")。以階段性的衍生反應，特別是增加[萜烯的](../Page/萜烯.md "wikilink")[雙鍵](../Page/雙鍵.md "wikilink")，他成功將液體的萜烯轉換成結晶化合物。調查環狀不飽和萜烯的[重排反應](../Page/重排反應.md "wikilink")，可以由以確認的萜烯結構得到未知萜烯的結構。

[AlphaPinene.png](https://zh.wikipedia.org/wiki/File:AlphaPinene.png "fig:AlphaPinene.png")

瓦拉赫命名了[萜烯及蒎烯](../Page/萜烯.md "wikilink")，並負責最早的蒎烯系統研究。他在1909年出版了「萜烯與樟腦」，是一本關於萜烯化學反應的書籍。他以[劉卡特反應](../Page/劉卡特反應.md "wikilink")(和魯道夫·劉卡特共同發現)及[Wallach重排反應在學界聞名](../Page/Wallach重排反應.md "wikilink")。

## 链接

  - [诺贝尔官方网站关于奥托·瓦拉赫传记](http://nobelprize.org/nobel_prizes/chemistry/laureates/1910/wallach-bio.html)

## 參考資料

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:波恩大學教師](../Category/波恩大學教師.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:東普魯士人](../Category/東普魯士人.md "wikilink")
[Category:戴维奖章](../Category/戴维奖章.md "wikilink")

1.
2.  Christmann, M. (2010), *Otto Wallach: Founder of Terpene Chemistry
    and Nobel Laureate 1910*. Angewandte Chemie International Edition,
    49: 9580–9586.