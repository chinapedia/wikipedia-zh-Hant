**奥拉夫五世**（Olav V，亚历山大·爱德华·克里斯蒂安·弗雷德里克，Alexander Edward Christian
Frederik；），[挪威国王](../Page/挪威国王.md "wikilink")，1957年至1991年在位。1903年7月2日生于[英国的](../Page/英国.md "wikilink")[桑德林漢姆府](../Page/桑德林漢姆府.md "wikilink")，他是[丹麦的卡尔王子](../Page/哈康七世.md "wikilink")（后来的[挪威国王](../Page/挪威国王.md "wikilink")[哈康七世](../Page/哈康七世.md "wikilink")）和[莫德公主](../Page/莫德王后_\(挪威\).md "wikilink")（英王[爱德华七世之女](../Page/爱德华七世.md "wikilink")）之子。

1929年3月21日, 他娶表姐瑞典[瑪塔公主](../Page/瑪塔公主_\(瑞典\).md "wikilink")（Märtha of
Sweden）为妻，生有一子两女，
[哈拉尔](../Page/哈拉尔五世.md "wikilink")、[拉格茜尔公主和](../Page/拉格茜尔公主.md "wikilink")[阿斯特丽公主](../Page/阿斯特丽公主.md "wikilink")。

[二战流亡时](../Page/二战.md "wikilink")，[太子妃玛塔和孩子們住在](../Page/太子妃.md "wikilink")[美国](../Page/美国.md "wikilink")[华府](../Page/华盛顿哥伦比亚特区.md "wikilink"),
与[美国总统](../Page/美国总统.md "wikilink")[富兰克林·罗斯福保持着良好的友谊](../Page/富兰克林·罗斯福.md "wikilink")，玛塔在丈夫即位前的1954年去世。

奥拉夫五世曾获得[1928年夏季奥林匹克运动会](../Page/1928年夏季奥林匹克运动会.md "wikilink")[帆船比赛金牌](../Page/帆船.md "wikilink")，是获得过奥运金牌的两位国王之一，另一位為同樣獲得風帆比賽金牌的前希臘國王[康斯坦丁二世](../Page/康斯坦丁二世_\(希腊\).md "wikilink")。

## 王室稱謂

  - 1903年-1905年 丹麥的亞歷山大王子殿下
  - 1905年-1957年 挪威王太子殿下
  - 1957年-1991年 挪威國王陛下

## 祖先

<center>

</center>

[Category:挪威君主](../Category/挪威君主.md "wikilink")
[O](../Category/格呂克斯堡王朝.md "wikilink")
[O](../Category/玫瑰勋章得主.md "wikilink")
[O](../Category/牛津大学贝利奥尔学院校友.md "wikilink")
[Category:挪威帆船運動員](../Category/挪威帆船運動員.md "wikilink")
[Category:挪威奧林匹克運動會金牌得主](../Category/挪威奧林匹克運動會金牌得主.md "wikilink")
[Category:1928年夏季奧林匹克運動會獎牌得主](../Category/1928年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會帆船獎牌得主](../Category/奧林匹克運動會帆船獎牌得主.md "wikilink")