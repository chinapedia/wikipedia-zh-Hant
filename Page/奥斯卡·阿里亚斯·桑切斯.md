**奥斯卡·拉斐尔·德·赫苏斯·阿里亚斯·桑切斯**（，），前[哥斯达黎加共和国](../Page/哥斯达黎加.md "wikilink")[总统](../Page/总统.md "wikilink")。

## 簡歷

阿里亚斯1940年生于哥斯达黎加[埃雷迪亚省](../Page/埃雷迪亚省.md "wikilink")[埃雷迪亚市的一个咖啡种植园主家庭](../Page/埃雷迪亚市.md "wikilink")。中学毕业后赴[美国](../Page/美国.md "wikilink")[波士顿大学学习](../Page/波士顿大学.md "wikilink")。回国后于1961年—1967年在[哥斯达黎加](../Page/哥斯达黎加.md "wikilink")[大学学习](../Page/大学.md "wikilink")，获得[硕士学位](../Page/硕士.md "wikilink")。1967年去[英国深造](../Page/英国.md "wikilink")，先后在[埃塞克斯大学和](../Page/埃塞克斯大学.md "wikilink")[伦敦政治經濟学院攻读](../Page/伦敦政治經濟学院.md "wikilink")，获得政治学[博士学位](../Page/博士.md "wikilink")。

1979年，阿里亚斯当选为民族解放党总书记，历任哥斯达黎加大学政治学院教授、中央银行董事会副主席、国家计划部部长。1986年作为民族解放党的候选人当选总统，同年5月就职，任期4年。

1987年，他提出《阿里亚斯计划》，和平解决了中美洲危机。1987年8月7日在[危地马拉中美洲](../Page/危地马拉.md "wikilink")5国总统签署了以《阿里亚斯计划》为基础的《在中美洲建立稳定和持久和平的程序》（又称《[中美洲和平协议](../Page/中美洲和平协议.md "wikilink")》）和《[第二个埃斯基普拉斯协议](../Page/第二个埃斯基普拉斯协议.md "wikilink")》。阿里亚斯因此于1987年10月获得[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。

2006年2月5日哥斯达黎加总统大选，阿里亚斯战胜公民行动党（Partido de Acción Ciudadana,
PAC）候选人奥通·索利斯·法亚斯（Ottón Solís
Fallas），以40.92%的得票率，时隔20年再次当选哥斯达黎加总统。他于2006年5月8日就任总统，任期4年。

阿里亚斯曾经於1992年訪問[台灣](../Page/台灣.md "wikilink")。但是在2007年6月7日，阿里亞斯總統宣佈[哥斯大黎加與](../Page/哥斯大黎加.md "wikilink")[中華人民共和國建交](../Page/中華人民共和國.md "wikilink")，[中華民國立刻宣佈與](../Page/中華民國.md "wikilink")[哥斯大黎加斷絕自](../Page/哥斯大黎加.md "wikilink")1941年之以來所建立之外交關係。

## 外部連結

  -

[Category:1940年出生](../Category/1940年出生.md "wikilink")
[Category:诺贝尔和平奖获得者](../Category/诺贝尔和平奖获得者.md "wikilink")
[Category:哥斯达黎加总统](../Category/哥斯达黎加总统.md "wikilink")
[Category:波士頓大學校友](../Category/波士頓大學校友.md "wikilink")
[Category:艾塞克斯大學校友](../Category/艾塞克斯大學校友.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")