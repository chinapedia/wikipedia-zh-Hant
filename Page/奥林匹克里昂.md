[Parc_OL.jpg](https://zh.wikipedia.org/wiki/File:Parc_OL.jpg "fig:Parc_OL.jpg")
**奧林匹克里昂**（Olympique
lyonnais，簡稱：OL及Lyon，[中文簡稱](../Page/中文.md "wikilink")**里昂**）是一間位於[法國東南部](../Page/法國.md "wikilink")[羅納-阿爾卑斯區的](../Page/羅納-阿爾卑斯.md "wikilink")[里昂市的](../Page/里昂.md "wikilink")[足球會](../Page/足球.md "wikilink")，成立於1950年8月3日，前身為里昂·奧林匹克（Lyon
Olympique）體育會其中一個分支的足球隊，1889年離開體育會自立門戶成立新球會，但官方網站表示球會於1950年正式成立。現時在[法國甲組足球聯賽比賽](../Page/法甲.md "wikilink")，球會同時設立男子及[女子足球隊](../Page/奥林匹克里昂_\(女子足球隊\).md "wikilink")。

## 球會歷史

里昂是首屆法國甲組足球聯賽成員之一，可惜名列第十五位而降落乙組，1951年以乙組聯賽冠軍獲得創會後首次錦標。球隊在法國足球史上沒有取得輝煌成績，比較優異的算是六十年代曾殺入[歐洲盃賽冠軍盃四強](../Page/歐洲盃賽冠軍盃.md "wikilink")，及3度晉身法國盃決賽並2次成功獲冠。

直至九十年代末里昂由[辛天尼帶領](../Page/積基斯·辛天尼.md "wikilink")，先連續取得聯賽頭三名，到2002年終於首次登上法國頂級聯賽冠軍寶座，同年勒岡（Paul
Le
Guen）接替執教[法國國家足球隊的辛天尼](../Page/法國國家足球隊.md "wikilink")，他其後繼續帶領里昂保持氣勢，加上隊中球員[-{zh-hans:小儒尼尼奥;
zh-hk:P·祖連奴;}-](../Page/小儒尼尼奧.md "wikilink")、[迪亞拉](../Page/穆罕穆德·迪亞拉.md "wikilink")、[-{zh-hans:克里斯蒂亞諾·馬克斯·戈麥斯;
zh-hk:基斯;}-](../Page/克里斯蒂亞諾·馬克斯·戈麥斯.md "wikilink")、[-{zh-hans:迈克尔·埃辛;
zh-hk:艾辛;}-](../Page/迈克尔·埃辛.md "wikilink")、[-{zh-hans:西德尼·戈武;
zh-hk:高禾;}-及門將](../Page/西德尼·戈武.md "wikilink")[-{zh-hans:格雷戈里·库佩;
zh-hk:哥柏;}-表現突出](../Page/格雷戈里·库佩.md "wikilink")，2003年至2005年橫掃3屆聯賽冠軍，創下連續四年奪得聯賽錦標，平了1960年代末[-{zh-hans:圣艾蒂安;
zh-hk:聖伊天;}-及](../Page/圣艾蒂安足球俱乐部.md "wikilink")1990年代初[馬賽的四連冠紀錄](../Page/馬賽足球俱樂部.md "wikilink")。

2005年前[利物浦領隊](../Page/利物浦足球會.md "wikilink")[-{zh-hans:热拉尔·霍利尔;
zh-hk:侯利亞;}-重返法國擔任新任領隊](../Page/热拉尔·烏利耶.md "wikilink")，並加入[葡萄牙中場](../Page/葡萄牙.md "wikilink")[-{zh-hans:蒂亚戈;
zh-hk:泰亞高;}-](../Page/蒂亚戈·门德斯.md "wikilink")，和前[華倫西亞前鋒](../Page/華倫西亞足球會.md "wikilink")[-{zh-hans:约翰·卡鲁;
zh-hk:卡維;}-](../Page/约翰·卡鲁.md "wikilink")。他亦成功帶領里昂贏得一屆法甲冠軍。

2007年里昂成為首支上市的法國足球會，招股價21至24.4[歐元](../Page/歐元.md "wikilink")，發行370萬股，集資8400萬歐元\[1\]。

2007年4月21日，聯賽次名[圖盧茲二比三不敵](../Page/圖盧茲足球會.md "wikilink")[雷恩](../Page/雷恩足球會.md "wikilink")，令處於榜首的里昂領先次席多達17分距離，里昂因此提前六輪聯賽慶祝球會連續第六年奪得聯賽冠軍，亦是歐洲五大聯賽（英格蘭、德國、西班牙、意大利及法國）歷史上首支聯賽六連冠隊伍\[2\]。在[2007-08年球季](../Page/2007年至2008年法國足球甲級聯賽.md "wikilink")，里昂再一次成功衛冕聯賽錦標，達成七連霸偉業。不過在[2008-09賽季](../Page/2008年至2009年法國足球甲級聯賽.md "wikilink")，里昂排名法甲第三位，聯賽冠軍被[波爾多所獲得](../Page/波爾多足球俱樂部.md "wikilink")。

於2010年4月，里昂以兩回合3比2的比數於歐洲聯賽冠軍盃擊敗[波爾多躋身四強](../Page/波爾多足球俱樂部.md "wikilink"),此乃里昂首次晉級此項頂級盃賽的四強階段。

## 球會榮譽

<table style="width:63%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 30%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white"> <strong>榮譽</strong></p></th>
<th><p><font color="white"> <strong>次數</strong></p></th>
<th><p><font color="white"> <strong>年度</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><font color="white"><strong>本 土 聯 賽</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/法甲.md" title="wikilink">甲組聯賽</a> 冠軍</strong></p></td>
<td><p><strong>7</strong></p></td>
<td><p><a href="../Page/2001年至2002年法國足球甲級聯賽.md" title="wikilink">2001/02年</a>，<a href="../Page/2002年至2003年法國足球甲級聯賽.md" title="wikilink">2002/03年</a>，<a href="../Page/2003年至2004年法國足球甲級聯賽.md" title="wikilink">2003/04年</a>，<a href="../Page/2004年至2005年法國足球甲級聯賽.md" title="wikilink">2004/05年</a>，<a href="../Page/2005年至2006年法國足球甲級聯賽.md" title="wikilink">2005/06年</a>，<a href="../Page/2006年至2007年法國足球甲級聯賽.md" title="wikilink">2006/07年</a>，<a href="../Page/2007年至2008年法國足球甲級聯賽.md" title="wikilink">2007/08年</a>；</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/法乙.md" title="wikilink">乙組聯賽</a> 冠軍</strong></p></td>
<td><p><strong>3</strong></p></td>
<td><p>1950/51年，1953/54年，1988/89年；</p></td>
</tr>
<tr class="even">
<td><p><font color="white"><strong>本 土 盃 賽</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/法國盃.md" title="wikilink">法國盃</a> 冠軍</strong></p></td>
<td><p><strong>5</strong></p></td>
<td><p>1963/64年，1966/67年，1972/73年, 2007/08年, 2011/12年；</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/法國超級盃.md" title="wikilink">超級盃</a> 冠軍</strong></p></td>
<td><p><strong>7</strong></p></td>
<td><p>1972/73年，2001/02年，2002/03年，2003/04年，2004/05年，<br />
2005/06年，2006/07年；</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/法國聯賽盃.md" title="wikilink">聯賽盃</a> 冠軍</strong></p></td>
<td><p><strong>1</strong></p></td>
<td><p>2000/01年；</p></td>
</tr>
</tbody>
</table>

## 著名球員

  - [積基特](../Page/艾梅·雅凯.md "wikilink")（Aimé Jacquet）

  - [杜明尼治](../Page/杜明尼治.md "wikilink")（Raymond Domenech）

  - [泰簡拿](../Page/讓·蒂加納.md "wikilink")（Jean Tigana）

  - [佐卡夫](../Page/尤里·德約卡夫.md "wikilink")（Jean Djorkaeff）

  - [馬列](../Page/馬列_\(法國足球員\).md "wikilink")（Steve Marlet）

  - （Bernard Lacombe）

  - （Bruno N'Gotty）

<!-- end list -->

  - [-{zh-hans:吕多维克·久利;zh-hk:古利;zh-tw:呂多維克·久利;}-](../Page/吕多维克·久利.md "wikilink")（Ludovic
    Giuly）

  - [-{zh-hans:弗劳; zh-hk:法奧;}-](../Page/弗劳.md "wikilink")（Pierre-Alain
    Frau）

  - [-{zh-hans:弗洛朗·马卢达;
    zh-hk:馬路達;}-](../Page/弗洛倫特·馬盧達.md "wikilink")（Florent
    Malouda）

  - [-{zh-hans:埃里克·阿比达尔;
    zh-hk:艾比度;}-](../Page/埃里克·阿比达尔.md "wikilink")（Eric
    Abidal）

  - [-{zh-hans:卡里姆·本泽马;zh-tw:本澤馬;zh-hk:賓施馬;zh-mo:賓森馬;}-](../Page/卡里姆·本泽马.md "wikilink")（Karim
    Benzema）

  - [-{zh-hans:吉奥瓦尼·埃尔伯;
    zh-hant:艾爾巴;}-](../Page/吉奧瓦尼·埃爾伯.md "wikilink")（Giovane
    Elber）

  - [-{zh-hans:埃德米尔森;zh-hk:艾美臣;zh-tw:埃德米爾森;}-](../Page/埃德米爾森.md "wikilink")（Edmilson
    Gomes）

<!-- end list -->

  - （Sonny Anderson）

  - [-{zh-hans:儒尼尼奥·佩南布卡诺;zh-hk:祖連奴·比南布肯奴;zh-tw:儒尼尼奧·佩南布卡諾;}-](../Page/祖連奴·比南布肯奴.md "wikilink")（Juninho
    Pernambucano）

  - [-{zh-hans:约翰·卡鲁; zh-hk:卡維;}-](../Page/约翰·卡鲁.md "wikilink")（John
    Carew）

  - [-{zh-hans:蒂亚戈; zh-hk:泰亞高;}-](../Page/蒂亚戈·门德斯.md "wikilink")（Tiago
    Mendes）

  - [-{zh-hans:埃辛;zh-hk:艾辛;zh-tw:埃辛;}-](../Page/艾辛.md "wikilink")（Michael
    Essien）

  - [-{zh-hans:法比奥·格罗索;
    zh-hk:格羅素;}-](../Page/法比奥·格罗索.md "wikilink")（Fabio
    Grosso）

  - [-{zh-hans:米兰·巴罗什; zh-hk:巴路士;}-](../Page/米兰·巴罗什.md "wikilink")（Milan
    Baroš）

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [里昂官方網站](http://www.olweb.fr/)
  - [里昂中文官网](http://ol.titan24.com/)

[分類:1950年建立的足球俱樂部](../Page/分類:1950年建立的足球俱樂部.md "wikilink")

[L](../Category/法國足球俱樂部.md "wikilink")
[奥林匹克里昂](../Category/奥林匹克里昂.md "wikilink")

1.  [里昂招股集資8400萬歐元](http://soccernet.espn.go.com/news/story?id=404733&cc=4716)
2.  [里昂連續第六年奪得法甲聯賽冠軍](http://soccernet.espn.go.com/news/story?id=423966&cc=5739)