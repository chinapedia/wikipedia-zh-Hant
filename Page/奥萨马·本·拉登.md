**奧薩瑪·本·穆罕默德·本·阿瓦德·本·拉登**（，拉丁轉寫：Usāmah bin Muḥammad bin Awaḍ bin
Lādin，\[1\]\[2\]），简称为**奧薩瑪·賓拉登**、**奥萨马**、**本-{}-·拉登**、**宾-{}-·拉登**或**-{拉登}-**。全名意思是「-{拉登}-的兒子阿瓦德的兒子穆罕默德的兒子奧薩瑪」（名·<small>بن</small>·父·<small>بن</small>·祖·<small>بن</small>·曾祖）；「奧薩瑪·-{}-賓·拉登」（）意思是「-{拉登}-的子孫奧薩瑪」，事实上是拉登的玄孙。他是[沙烏地阿拉伯王國](../Page/沙烏地阿拉伯王國.md "wikilink")[利雅得省人](../Page/利雅得省.md "wikilink")，是[基地组织](../Page/基地组织.md "wikilink")[首领](../Page/領袖.md "wikilink")，该组织被认为是全球性的[恐怖组织](../Page/恐怖组织.md "wikilink")。本·拉登篤信[伊斯兰教](../Page/伊斯兰教.md "wikilink")[遜尼宗](../Page/遜尼宗.md "wikilink")[瓦哈比派](../Page/瓦哈比派.md "wikilink")。

本·拉登被[美国政府指控为](../Page/美國聯邦政府.md "wikilink")[1998年美國大使館爆炸案的幕後主謀](../Page/1998年美國大使館爆炸案.md "wikilink")，名列[联邦调查局](../Page/联邦调查局.md "wikilink")（Federal
Bureau of
Investigation，FBI）[十大通緝要犯](../Page/美國聯邦調查局十大通緝要犯.md "wikilink")。

本·拉登最初否認與[911恐怖攻擊有關](../Page/911恐怖攻擊.md "wikilink")，但已於2004年承認責任\[3\]\[4\]。然而FBI发言人Rex
Tomb在2006年表示没有确实证据证明賓拉登涉及911事件\[5\]\[6\]。因此始终没有就911事件正式起诉本·拉登\[7\]\[8\]。2011年本·拉登被殺後，FBI在其官方網站表示本·拉登是911襲擊的主謀\[9\]。

2004年3月18日，[美国众议院一致通过一项法案](../Page/美国众议院.md "wikilink")，将提供线索导致本·拉登被捕的赏金从2500万[美元增加到](../Page/美元.md "wikilink")5000万美元。虽然普遍认为本·拉登藏身于[阿富汗与](../Page/阿富汗.md "wikilink")[巴基斯坦边境一带](../Page/巴基斯坦.md "wikilink")，实际上外界没有人知道本·拉登身在何处，甚至指他早已[身亡](../Page/死亡.md "wikilink")。

2011年5月2日，[美国总统](../Page/美国总统.md "wikilink")[奥巴马發表聲明](../Page/贝拉克·奥巴马.md "wikilink")，指本·拉登在巴基斯坦[阿伯塔巴德的一座豪宅裏被](../Page/阿伯塔巴德.md "wikilink")[美國海軍](../Page/美國海軍.md "wikilink")[海豹部隊第六分隊突襲击毙](../Page/海豹部隊第六分隊.md "wikilink")\[10\]。其[屍體於次日](../Page/屍體.md "wikilink")[海葬於北](../Page/海葬.md "wikilink")[阿拉伯海](../Page/阿拉伯海.md "wikilink")。另外雖然是反恐夥伴之一，但美方政府未經[巴基斯坦討論即決定粗暴的行動](../Page/巴基斯坦.md "wikilink")，事後仍引來巴國的強烈不滿。

## 人名

“奧薩瑪”的本意指“[狮子](../Page/狮.md "wikilink")”。“本·拉-{}-登”的意思是「-{拉登}-的兒子」或「-{拉登}-的後裔」，媒體稱他「賓·-{}-拉登」而非「奧薩瑪」是根据[歐美的](../Page/西方世界.md "wikilink")[姓氏觀念來套用](../Page/姓氏.md "wikilink")。其[阿拉伯语全名意为](../Page/阿拉伯语.md "wikilink")：「-{拉登}-之子阿瓦德之子穆罕默德之子奧薩瑪」，亦即「奧薩瑪」才是他自己真正的[本名](../Page/本名.md "wikilink")，而-{拉登}-則只是他[曾祖父的名字](../Page/曾祖父.md "wikilink")。因此，稱他「奧薩瑪」或全名「奧薩瑪·賓·拉登」才是真正符合[阿拉伯人命名習慣的稱法](../Page/阿拉伯人.md "wikilink")。然而大部份[中文](../Page/中文.md "wikilink")[媒體都跟隨著](../Page/媒介.md "wikilink")[西方媒體的叫法](../Page/西方.md "wikilink")，利用[羅馬拼音將之翻譯或轉寫為](../Page/羅馬拼音.md "wikilink")「-{拉登}-」或「-{賓·拉登}-」。

因為阿拉伯文裡没有利用[羅馬拼音的](../Page/罗马化.md "wikilink")[转写系统](../Page/转写.md "wikilink")，奧薩瑪有很多不同的羅馬拼音译名，當中有几个常见翻译，通常被媒体使用的是（奧薩瑪·賓·拉登），而[情报界通常使用](../Page/美國情報體系.md "wikilink")或（烏撒瑪·賓·拉登），缩写为。

虽然阿拉伯人习惯上不把“本·拉-{}-登”作为[姓氏](../Page/姓氏.md "wikilink")，但為了與歐美[企业做生意](../Page/企业.md "wikilink")，基於權宜上方便，有時也會依照歐美人的習慣，用此名来称呼家族企業的名称以權充姓氏；此時強調的是自己乃是「-{拉登}-的子孫」。

## 生平

1957年3月10日，賓·拉登生于[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")[首都](../Page/首都.md "wikilink")[利雅德的一个以](../Page/利雅德.md "wikilink")[建筑业](../Page/建筑.md "wikilink")（[沙烏地賓拉登集團](../Page/沙特本拉登集團.md "wikilink")）為生的富裕家族。其父[穆罕默德·本·拉登为沙特本拉登集团的创始人](../Page/穆罕默德·本·拉登.md "wikilink")，一生有55个子女，而賓·拉登本人排行第17個。

根据美国情报表示，賓·拉登身高约在6[呎](../Page/英尺.md "wikilink")4[吋至](../Page/英寸.md "wikilink")6呎6吋之间（193－198[公分](../Page/公分.md "wikilink")），体重大約160[磅](../Page/磅.md "wikilink")（73[公斤](../Page/公斤.md "wikilink")），高大英俊而清秀\[11\]。

1974年，17歲的賓·拉登在[拉塔基亚迎娶了](../Page/拉塔基亚.md "wikilink")[叙利亚籍的妻子Najwa](../Page/叙利亚.md "wikilink")
Ghanem。賓·拉登本來就有三位妻子，而且育有13名子女，不過由於結婚又離異，他擁有過22次左右的婚姻，所以他實際有多少名子女並沒有正式的統計過\[12\]。在[阿卜杜拉国王大学进修期间](../Page/阿卜杜勒阿齊茲國王大學.md "wikilink")，他學習了[经济学和](../Page/经济学.md "wikilink")[工商管理](../Page/工商管理.md "wikilink")\[13\]。有消息表明他在1979年获得了[土木工程学位](../Page/土木工程.md "wikilink")\[14\]，與1981年所得到的[公共行政学位](../Page/公共行政.md "wikilink")\[15\]。有资料显示賓·拉登在大学时期非常刻苦\[16\]，但亦有传言表明，说他在就读了3年之后肄业离校\[17\]。大学生的他遇上了2位[伊斯蘭學者](../Page/烏理瑪.md "wikilink")，并從此執迷于[伊斯兰教中的](../Page/伊斯兰教.md "wikilink")《[古兰经](../Page/古兰经.md "wikilink")》教义和[圣战論](../Page/圣战.md "wikilink")。父亲在早年去世后，他继承家裏的大筆[遗产](../Page/遗产.md "wikilink")，並擔任[家族企業的總](../Page/家族生意.md "wikilink")[工程师](../Page/工程师.md "wikilink")。当时估计他的财产达到3亿[美元](../Page/美元.md "wikilink")，但是现在一般认为只有2500万。

1988年他成立[蓋達组织抵抗蘇聯](../Page/蓋達组织.md "wikilink")。直至1989年[蘇聯军队撤离](../Page/苏联武装力量.md "wikilink")[阿富汗](../Page/阿富汗.md "wikilink")。

1990年[波斯灣战争爆发前](../Page/波斯灣战争.md "wikilink")，[美军进驻](../Page/美军.md "wikilink")[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")。賓·拉登对[沙烏地阿拉伯王室允许主要由](../Page/沙烏地阿拉伯王室.md "wikilink")[基督徒所组成的美军保卫伊斯兰教圣地非常的不满](../Page/基督徒.md "wikilink")，双方矛盾逐渐深化。后来他离开沙烏地阿拉伯，前往[苏丹和](../Page/苏丹.md "wikilink")[阿富汗组织对美军的開始了战爭](../Page/阿富汗.md "wikilink")\[18\]。1994年，在美国的压力下，沙特阿拉伯政府剥夺了本·拉登的国籍\[19\]，自此本·拉登成为无国籍人士直至其在2011年被美军在巴基斯坦击毙。

賓·拉登是21世紀国际恐怖组织中，最具代表性的人物之一。很多人都认为他是当今世界最大的恐怖分子，他也被称为[恐怖主义的](../Page/恐怖主义.md "wikilink")“精神领袖”。沙烏地阿拉伯的[教長曾表示](../Page/阿訇.md "wikilink")：「[屠杀無辜平民](../Page/屠杀.md "wikilink")，是[恐怖活动而不是](../Page/恐怖活动.md "wikilink")[圣战](../Page/圣战.md "wikilink")，亦絕對不是出於[安拉或是](../Page/安拉.md "wikilink")[穆罕默德的](../Page/穆罕默德.md "wikilink")[啟示](../Page/啟示.md "wikilink")，因為[真主與](../Page/安拉.md "wikilink")[先知是慈愛而無私的](../Page/先知.md "wikilink")」。不过《[法新社](../Page/法新社.md "wikilink")》評論則指出，「賓·拉登顯然獲得了許多[阿拉伯人的支持](../Page/阿拉伯人.md "wikilink")，其蓋達組織也日益擴張，美國必須要檢討其對[中东與伊斯蘭的政策](../Page/中东.md "wikilink")，以免此類[恐怖主义的產生](../Page/恐怖主义.md "wikilink")」。而美國政府的說法是：「針對的是以賓·拉登為首的[恐怖份子](../Page/恐怖主义.md "wikilink")，對伊斯蘭教與[其教徒並無敵意](../Page/穆斯林.md "wikilink")，但是[反恐行動將是永不休止](../Page/反恐.md "wikilink")」\[20\]。

## 恐怖活动

[Kenya_bombing_1.jpg](https://zh.wikipedia.org/wiki/File:Kenya_bombing_1.jpg "fig:Kenya_bombing_1.jpg")
[WTC_smoking_on_9-11.jpeg](https://zh.wikipedia.org/wiki/File:WTC_smoking_on_9-11.jpeg "fig:WTC_smoking_on_9-11.jpeg")

本·拉登在1988年左右成立了[蓋達组织](../Page/蓋達组织.md "wikilink")，与其他[恐怖主义组织合作](../Page/恐怖主义.md "wikilink")，开始在[中东之外招收成员](../Page/中东.md "wikilink")，分布于[东南亚](../Page/东南亚.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[欧洲和美国](../Page/欧洲.md "wikilink")。

1992年12月针对驻[索马里](../Page/索马里.md "wikilink")[美军的也门旅馆爆炸案](../Page/美军.md "wikilink")。

1993年2月[美国的](../Page/美国.md "wikilink")[世贸中心爆炸案](../Page/世界贸易中心.md "wikilink")，导致6人死亡、数百人受伤。

1993年6月[谋杀](../Page/谋杀.md "wikilink")[约旦](../Page/约旦.md "wikilink")[王储阿卜杜勒未遂](../Page/储君.md "wikilink")。

1994年賓·拉登宣佈向[美國發動](../Page/美國.md "wikilink")[圣戰](../Page/圣戰.md "wikilink")。

1995年1月[刺杀](../Page/刺杀.md "wikilink")[罗马](../Page/罗马.md "wikilink")[天主教](../Page/天主教.md "wikilink")[教皇](../Page/教宗.md "wikilink")[若望·保祿二世未遂](../Page/若望·保祿二世.md "wikilink")。

1995年11月[利雅德美军军营爆炸案](../Page/利雅德.md "wikilink")，5人死亡；[埃及驻](../Page/埃及.md "wikilink")[巴基斯坦使馆爆炸案](../Page/巴基斯坦.md "wikilink")，17人丧生。

1995年賓·拉登承认在沙烏地阿拉伯本土的利雅得和[宰赫兰执行恐怖活动](../Page/宰赫兰.md "wikilink")，被剥夺沙烏地阿拉伯的[公民权](../Page/公民.md "wikilink")。

1996年[宰赫兰美空军住所爆炸案](../Page/宰赫兰.md "wikilink")。

1997年[开罗外国旅游者客车爆炸案](../Page/开罗.md "wikilink")。

1998年8月东非[美國大使館爆炸案](../Page/1998年美国大使馆爆炸案.md "wikilink")。257人死亡、5000余人受伤。

2000年10月也门美国“科尔”军舰爆炸案，17人死亡。

2001年9月11日早晨（[美国东部时间](../Page/北美东部时区.md "wikilink")），美国本土发生了[九一一袭击事件](../Page/九一一袭击事件.md "wikilink")，事件中4架民航客机被劫持，其中2架撞向[纽约](../Page/纽约.md "wikilink")[世界贸易中心双塔](../Page/世界贸易中心.md "wikilink")，其中1架撞向了[五角大廈](../Page/五角大廈.md "wikilink")，第4架因机舱内乘客与劫机歹徒搏斗，最终在[宾夕法尼亚州坠毁](../Page/宾夕法尼亚州.md "wikilink")。此事件导致近3000人死亡。

2006年6月30日賓·拉登在[網際網路上公布的一段录音中](../Page/網際網路.md "wikilink")，称被美军炸死的[統一聖戰組織](../Page/伊斯兰国.md "wikilink")[領袖](../Page/領袖.md "wikilink")[扎克维为](../Page/阿布·穆萨布·扎卡维.md "wikilink")**圣战雄狮**，并表示蓋達组织将继续在[伊拉克的圣战](../Page/伊拉克.md "wikilink")。

2007年9月賓·拉登继2004年後首次在一卷录影带《告美国人民书》中发表讲话，号召追随者以加剧杀戮和战斗的方式结束[伊拉克战争](../Page/伊拉克战争.md "wikilink")，但没有发出具体的恐怖袭击威胁。

2009年賓·拉登最後一次录音，警告[美国总统](../Page/美国总统.md "wikilink")[歐巴馬](../Page/歐巴馬.md "wikilink")。但是否真為賓·拉登本尊，或是由其他[替身成員](../Page/替身.md "wikilink")[捉刀](../Page/捉刀.md "wikilink")，亦備受質疑。

## 死亡

[Obama_and_Biden_await_updates_on_bin_Laden.jpg](https://zh.wikipedia.org/wiki/File:Obama_and_Biden_await_updates_on_bin_Laden.jpg "fig:Obama_and_Biden_await_updates_on_bin_Laden.jpg")
[Osama_Bin_Laden_marked_deceased_on_FBI_Ten_Most_Wanted_List_May_3_2011.jpg](https://zh.wikipedia.org/wiki/File:Osama_Bin_Laden_marked_deceased_on_FBI_Ten_Most_Wanted_List_May_3_2011.jpg "fig:Osama_Bin_Laden_marked_deceased_on_FBI_Ten_Most_Wanted_List_May_3_2011.jpg")
美国当地时间2011年5月2日凌晨，美国总统[奥巴马于](../Page/奥巴马.md "wikilink")[白宮宣布](../Page/白宮.md "wikilink")，賓·拉登当晚在巴基斯坦[首都](../Page/首都.md "wikilink")[伊斯兰堡以北](../Page/伊斯兰堡.md "wikilink")150公里的城市[阿伯塔巴德於住宅內被击毙](../Page/阿伯塔巴德.md "wikilink")。通过对比賓·拉登和他死去的亲姐姐的[DNA](../Page/脱氧核糖核酸.md "wikilink")，証实了他的身份。

賓·拉登生前藏身于在阿伯塔巴德的一座豪宅裏，据悉该宅建于2005年，价值约100万美元，没有[电话和](../Page/电话.md "wikilink")[網路服务](../Page/網路.md "wikilink")，从外观上看，该建筑的大小，约为该区其他房屋8倍，并配有铁丝网护栏和高墙等保衛设施。有目击者称，该住所距离巴基斯坦陆军军官学校的大门仅100公尺之近。

此次秘密行动由奥巴马授权“一小队美军”执行。在这次行动中，賓·拉登的一个儿子哈立德被打死，同时被捕的包括賓·拉登的2个妻子、6个孩子和4名亲信\[21\]\[22\]。据美国政府官员披露，[美国海军](../Page/美国海军.md "wikilink")[海豹特种部隊](../Page/海豹部隊.md "wikilink")[第六分隊大約](../Page/美國海軍特戰開發小組.md "wikilink")24位官兵，在得到奥巴马总统命令后，分乘4架[直升机从巴基斯坦西北部的](../Page/直升机.md "wikilink")[加齐空军基地出发突袭賓](../Page/加齐空军基地.md "wikilink")·拉登藏身地，在其住宅外降落，由外部攻坚。美军攻入住宅后，在3楼一个房间找到無[武裝的賓](../Page/武裝.md "wikilink")·拉登，美軍隨即開火，賓·拉登胸部先中一枪，接着左眼上方中了致命一枪，倒地身亡，部分头骨遭[子弹击飞](../Page/子弹.md "wikilink")，鮮血混合腦漿四溢。整个过程持续约30分钟，美军没有人员伤亡，但有一架直升机墜毀，官方聲稱可能是因為其住宅圍牆過高使該機降落時之上升氣流不足或是因為該機為增強隱蔽性能而多經改裝致使駕駛困難；後此機於美軍離去的同時被以炸藥炸毀，美軍突襲殺死賓·拉登的[隱蔽行動](../Page/隱蔽行動.md "wikilink")，被[美國政府判定為合法](../Page/美國政府.md "wikilink")。

战斗结束后美军搜查了这栋建筑，带走賓·拉登的[電腦](../Page/電腦.md "wikilink")[硬碟](../Page/硬碟.md "wikilink")、[DVD](../Page/DVD.md "wikilink")[光碟和其它資料與数据](../Page/光碟.md "wikilink")，连同其屍體搭[直升机离开现场返回美軍基地](../Page/直升机.md "wikilink")，宅内其他人员移交巴基斯坦当局安置。除了賓·拉登外，还有3名成年男子死亡，其中1人是賓·拉登的儿子，另外两人是本·拉登的警卫、还有一名女子丧生，2名在现场的女子受伤。之前有謠傳賓·拉登槍戰時躲在女子身後，以作為人肉盾牌或者擋箭牌之說法，已被証實為假消息。美軍查獲的[DVD](../Page/DVD.md "wikilink")[光碟中](../Page/光碟.md "wikilink")，有些是現代[色情片](../Page/色情片.md "wikilink")，不知是否為賓·拉登本人還是其部下所擁有，因為連同住的3名賓·拉登[妻](../Page/妻.md "wikilink")[妾也不知情](../Page/妾.md "wikilink")\[23\]。

奥巴马政府考虑到賓·拉登是一名虔诚的[伊斯蘭教徒](../Page/穆斯林.md "wikilink")，在3日内[下葬是对伊斯蘭教传统的尊重](../Page/葬礼.md "wikilink")，但美國捨棄了伊斯蘭習用的[土葬而采用](../Page/土葬.md "wikilink")[海葬](../Page/海葬.md "wikilink")，因此賓·拉登被[枪杀后的](../Page/枪杀.md "wikilink")24小时内，其尸体被送往美军[卡爾文森號航空母艦](../Page/卡爾文森號航空母艦.md "wikilink")，在清洗與唸誦《[古兰经](../Page/古兰经.md "wikilink")》后，投入北[阿拉伯海](../Page/阿拉伯海.md "wikilink")。採用海葬使賓·拉登葬身于汪洋大海，亦是為了防止賓·拉登的[坟墓日後变成其他](../Page/坟墓.md "wikilink")[恐怖分子的](../Page/恐怖主义.md "wikilink")[朝聖或者纪念之圣地](../Page/朝聖.md "wikilink")\[24\]。然而遺體已消失於[阿拉伯海](../Page/阿拉伯海.md "wikilink")，加上[美國政府一直以影像太過駭人為理由](../Page/美國政府.md "wikilink")，不公佈其死亡的照片與[錄影](../Page/錄影.md "wikilink")，也造成不少人質疑賓·拉登是否已經死亡\[25\]\[26\]。

原先只有[美國總統](../Page/美國總統.md "wikilink")[奥巴馬與](../Page/奥巴馬.md "wikilink")[國務卿](../Page/美國國務卿.md "wikilink")[希拉蕊·柯林頓等人可得覽照片](../Page/希拉蕊·柯林頓.md "wikilink")，5月12日迫于各方压力的[中央情报局](../Page/中央情报局.md "wikilink")，开始向[國会](../Page/美国国会.md "wikilink")[议员展示照片](../Page/议员.md "wikilink")，[參议员詹姆士](../Page/美國參议员.md "wikilink")·英霍夫（James
Inhofe）表示賓·拉登死狀悽慘恐怖，但仍支持将照片公之于众，以定民心\[27\]。

賓·拉登死后，蓋達組織现由賓·拉登的副手[艾曼·查瓦希里](../Page/艾曼·查瓦希里.md "wikilink")[醫師接班](../Page/医生.md "wikilink")，查瓦希里是一名[埃及的](../Page/埃及.md "wikilink")[眼科醫師](../Page/眼科.md "wikilink")，根據美國[中央情报局的調查](../Page/中央情报局.md "wikilink")，查瓦希里亦是九一一事件的幕後策劃人之一，相較於賓·拉登，查瓦希里的作風更為激進\[28\]。

虽然美国无公开任何賓·拉登死亡的照片，但根据各方公开的消息，美军方面说在击毙賓·拉登后曾让屋内的人（包括賓·拉登的妻儿、侍衛等）去辨认尸体，而这些人由事后赶来的巴基斯坦方面接管（后来部分人被遣返回原籍，部分人在巴基斯坦服刑）。来自巴基斯坦方面的消息说屋中的这些人承认与賓·拉登的密切关系（与调查资料吻合），并明确表示賓·拉登已死。另外蓋達组织也确认賓·拉登已被美军击毙，并发动了几次针对性的报复行動，据傳媒报道参与猎杀行动的海豹部队在其后的任务中遇伏，死伤惨重。

## 人物年表

  - 1957年3月10日，出生于[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")[首都](../Page/首都.md "wikilink")[利雅德一个建筑业富商的家庭](../Page/利雅德.md "wikilink")，家中排行17。
  - 1974年，与[纳吉瓦·加尼姆结婚](../Page/纳吉瓦·加尼姆.md "wikilink")。
  - 1976年，長子阿卜杜拉出生。
  - 1976年－1979年，就学於[阿卜杜勒·阿齐兹国王大学](../Page/阿卜杜勒·阿齐兹国王大学.md "wikilink")（King
    Abdul-Aziz University）
  - 1979年－1989年，放弃学业离开家庭参加了阿富汗“[伊斯兰圣战组织](../Page/伊斯兰圣战组织.md "wikilink")”，反抗[苏联](../Page/苏联.md "wikilink")[紅軍的侵略](../Page/苏联红军.md "wikilink")。
  - 1988年，建立蓋達组织。
  - 1989年，前苏联从阿富汗撤军后，本·拉登及其追随者返回沙特阿拉伯的家乡。
  - 1991年，由于和沙烏地阿拉伯政府发生矛盾，本·拉登逃亡到苏丹重建基地组织。
  - 1994年4月，沙特阿拉伯剥夺了本·拉登的公民资格。
  - 1996年5月，被要求离开苏丹，辗转返回阿富汗。
  - 1998年2月，组织了“[伊斯兰反犹太人和十字军国际阵线](../Page/伊斯兰反犹太人和十字军国际阵线.md "wikilink")”（“伊斯兰圣战组织”是其派生组织）。
  - 1998年，轰炸美国驻肯尼亚和坦桑尼亚大使馆。
  - 2001年9月11日，美国“911”事件发生，造成3000多人伤亡。
  - 2001年10月7日，美国发动针对“蓋達組織”及其大本營所在國阿富汗的反恐战争，本·拉登开始了逃亡生涯，傳言其經常藏身於巴基斯坦與阿富汗的邊境地區。
  - 2011年，呼籲[利比亚人民及蓋達组织反抗](../Page/利比亚.md "wikilink")[卡扎菲政權](../Page/穆阿迈尔·卡扎菲.md "wikilink")。
  - 2011年5月2日，本·拉登在美國軍事行动中，於巴基斯坦[阿伯塔巴德的一座豪宅內](../Page/阿伯塔巴德.md "wikilink")，被[海豹第六分隊擊毙](../Page/美国海军特种作战研究大队.md "wikilink")，终年54岁。

## 流行文化

本·拉登的个人形象经常被用来制成[网络模因](../Page/网络模因.md "wikilink")，通常会在他的肖像上添加一句[大赞辞](../Page/大赞辞.md "wikilink")"Allahu
Akbar "(意为“真主至大”）。

## 注释

## 参考文献

## 外部链接

## 参见

  - [基地组织](../Page/基地组织.md "wikilink")
  - [加里·福克納](../Page/加里·福克納.md "wikilink")
  - [美國聯邦調查局十大通緝要犯](../Page/美國聯邦調查局十大通緝要犯.md "wikilink")

{{-}}

[Category:恐怖分子](../Category/恐怖分子.md "wikilink")
[Category:九一一事件人物](../Category/九一一事件人物.md "wikilink")
[Category:阿富汗戰爭人物](../Category/阿富汗戰爭人物.md "wikilink")
[Category:美国政治](../Category/美国政治.md "wikilink")
[Category:利雅德人](../Category/利雅德人.md "wikilink")
[Category:泛阿拉伯民族主義](../Category/泛阿拉伯民族主義.md "wikilink")
[Category:遇刺身亡者](../Category/遇刺身亡者.md "wikilink")
[Category:海葬者](../Category/海葬者.md "wikilink")
[Category:土木工程师](../Category/土木工程师.md "wikilink")
[Category:無國籍人士](../Category/無國籍人士.md "wikilink")
[Category:基地組織成員](../Category/基地組織成員.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.  [Bin Laden Killed —
    FBI](https://www.fbi.gov/news/stories/bin-laden-killed)

10. [中國評論新聞：奧巴馬將宣布賓拉登死訊](http://www.chinareviewnews.com/doc/1016/7/9/8/101679850.html?coluid=7&kindid=0&docid=101679850&mdate=0502112422)

11. [追捕进行时：美国政府利用PS技术修改本拉登样貌照片_cnBeta
    最新消息_cnBeta.COM](http://www.cnbeta.com/articles/102267.htm)

12.

13. Messages to the World: The Statements of Osama bin Laden, Verso,
    2005, p. xii.

14. [*Encyclopedia of World Biography
    Supplement*](http://galenet.galegroup.com/), Vol. 22. Gale Group,
    2002. (link requires username/password)

15.

16.

17.

18.

19.

20. 王亦名. 《帝國主義？資本主義？論美國與中東形勢》. [邯鄲](../Page/邯郸市.md "wikilink")

21.
22.

23. [拉登大宅藏大量色情片
    星島日報](http://news.sina.com.hk/news/12/1/1/2329994/1.html)

24.

25. [賓·拉登生死都如謎
    死訊引疑問](http://iservice.libertytimes.com.tw/liveNews/news.php?no=492334&type=%E7%A4%BE%E6%9C%83),
    自由時报. 2011年5月3日

26. [賓·拉登斃命罩疑雲
    陰謀論四起](http://www.cna.com.tw/ShowNews/Detail.aspx?pNewsID=201105030340&pType1=JF&pType0=aALL&pTypeSel=0&pPNo=1)
    , 中央社. 2011年5月3日

27.

28. [英媒稱扎瓦赫里有望接替拉登成基地組織領導人](http://www.ycwb.com/ePaper/ycwb/html/2011-05/03/content_1102329.htm)
    , 羊城晚報. 2011-05-04