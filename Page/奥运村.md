**奧運村**（別稱**奧林匹克村**或**選手村**），是於每屆[夏季奧運與](../Page/夏季奧林匹克運動會.md "wikilink")[冬季奧運中](../Page/冬季奧林匹克運動會.md "wikilink")，主辦國為各國運動員、官員、裁判、志願者和工作人員等提供的[宿舍](../Page/宿舍.md "wikilink")。

## 起源

由[1896年夏季奧運至](../Page/1896年夏季奧林匹克運動會.md "wikilink")[1920年的奧運中](../Page/1920年夏季奧林匹克運動會.md "wikilink")，各國的運動員、官員都分散於不同的[賓館內](../Page/賓館.md "wikilink")。此舉被[國際奧委會認為並](../Page/國際奧委會.md "wikilink")「不利於奧運組織工作及各國運動員之間的交流」。於是並在《[奧林匹克憲章](../Page/奧林匹克憲章.md "wikilink")》中規定：奧運會組委會應提供「至少一座在奧林匹克運動會開幕式前兩周至閉幕式後3天期間可入住的奧林匹克村」。\[1\]\[2\]\[3\]

## 開始

到了1924年舉行的[第8屆巴黎奧運中](../Page/1924年夏季奧林匹克運動會.md "wikilink")，[法國首次把運動員等刻意的集中於指定的房屋中](../Page/法國.md "wikilink")，後來，奧運村漸漸的成為了每屆奧運的指定宿舍。當中的宿舍一定要在主體育場、練習場附近，並且必須提供膳食和[交通服務](../Page/交通.md "wikilink")，同時要有如[醫院](../Page/醫院.md "wikilink")、[商店等其他配套設施](../Page/商店.md "wikilink")。

隨著奧運愈來愈廣泛，愈多國家和運動員參與，奧運村的面積亦越大和越多配套設施，如於[2004年雅典奧運中](../Page/2004年夏季奧林匹克運動會.md "wikilink")，整個奧運村佔地1.24[平方公里](../Page/平方公里.md "wikilink")，更包括了一所醫院、興奮劑檢測中心、幾個宗教場所、[餐廳](../Page/餐廳.md "wikilink")、[游泳池](../Page/游泳池.md "wikilink")、[跑道和兩個室內訓練館](../Page/跑道_\(體育\).md "wikilink")。除了奧運以外，其他大規模的體育賽事也會提供選手村。

奧運結束後，有些奧運村會拆除，另外有些則會改裝後做為[商品房](../Page/商品房.md "wikilink")、[公營住宅或](../Page/公營住宅.md "wikilink")[社會住宅出租](../Page/社會住宅.md "wikilink")、出售，或轉交[大學使用作寄宿用途](../Page/大學.md "wikilink")。

## 參考資料

## 外部連結

[Category:奧林匹克運動會](../Category/奧林匹克運動會.md "wikilink")
[Category:房屋](../Category/房屋.md "wikilink")
[Category:1932年面世](../Category/1932年面世.md "wikilink")

1.
2.
3.