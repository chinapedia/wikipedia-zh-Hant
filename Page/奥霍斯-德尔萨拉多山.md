**奥霍斯-德尔萨拉多山**（[西班牙语](../Page/西班牙语.md "wikilink")：****）是[安第斯山脈中的一座火山](../Page/安第斯山脈.md "wikilink")，標高6,891米，是世界上最高的[活火山](../Page/活火山.md "wikilink")，智利的[最高峰](../Page/各国最高点列表.md "wikilink")。该火山位于[阿根廷和](../Page/阿根廷.md "wikilink")[智利的边境](../Page/智利.md "wikilink")，西半球最高山峰[阿空加瓜山以北约](../Page/阿空加瓜山.md "wikilink")600公里。

奥霍斯-德尔萨拉多山靠近[阿塔卡马沙漠](../Page/阿塔卡马沙漠.md "wikilink")，气候干燥，只有接近山巅处有积雪；但该火山东侧有一直径约100米的[火山口湖](../Page/火山口湖.md "wikilink")，海拔6,390米，\[1\]是世上海拔最高的湖泊。

首次成功攀登该山峰顶的是[波兰人](../Page/波兰人.md "wikilink")[扬·阿尔弗雷德·什切潘斯基](../Page/扬·阿尔弗雷德·什切潘斯基.md "wikilink")（）和[尤斯廷·沃伊什尼斯](../Page/尤斯廷·沃伊什尼斯.md "wikilink")（）。

## 火山活动

奥霍斯-德尔萨拉多山是一座[活火山](../Page/活火山.md "wikilink")。根据[史密森尼学会](../Page/史密森尼学会.md "wikilink")“全球火山作用计划”的资料，该山最近一次爆发约在1,000至1,500年前。\[2\]

## 參看

  - [尤耶亞科山](../Page/尤耶亞科山.md "wikilink")

## 注释

## 参考文献

<div class="references-small">

  - <small>(in Spanish; also includes volcanoes of Argentina, Bolivia,
    and Peru)</small>

  -
  -

</div>

## 外部链接

  - [1-Dec-2006 Star Trails at 19,000
    Feet](http://antwrp.gsfc.nasa.gov/apod/ap061202.html)－NASA每日图片
  - [安第斯山脉资料](https://web.archive.org/web/20090716050935/http://www.andes.org.uk/andes-information-files/6000m-peaks.htm)
  - [Peak bagger](http://www.peakbagger.com/peak.aspx?pid=8569)
  - [Summit
    post](http://www.summitpost.org/show/mountain_link.pl/mountain_id/126)
  - [Peak list](http://www.peaklist.org/WWsurveys/SA/ojos.html)

[Category:活火山](../Category/活火山.md "wikilink")

1.
2.