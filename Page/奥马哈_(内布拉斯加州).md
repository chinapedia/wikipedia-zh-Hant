**奥马哈**（，，）位于[美国](../Page/美国.md "wikilink")[内布拉斯加州东部边界](../Page/内布拉斯加州.md "wikilink")[密苏里河畔](../Page/密苏里河.md "wikilink")，是该州最大的城市，也是[道格拉斯县的县治所在](../Page/道格拉斯县_\(内布拉斯加州\).md "wikilink")。建立在1854年。根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，共有人口390,007人，其中[白人占](../Page/白人.md "wikilink")78.39%、[非裔美国人占](../Page/非裔美国人.md "wikilink")13.31%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")1.74%。在2005年统计人口升至424,988。

## 歷史

早於古時奧馬哈就已有原住民居住。而歐洲人最早抵達奧馬哈之記錄，則是在1804年[劉易斯與克拉克遠征](../Page/劉易斯與克拉克遠征.md "wikilink")。當時[美国陆军的](../Page/美国陆军.md "wikilink")[梅里韦瑟·刘易斯上尉](../Page/梅里韦瑟·刘易斯.md "wikilink")（Meriwether
Lewis）和[威廉·克拉克少尉](../Page/威廉·克拉克.md "wikilink")（William
Clark），於1804年7月30日至8月3日，抵達了奧馬哈現址。隨後幾年，歐洲人在此修建了炮台和皮毛貿易據點。

1854年7月4日，奧馬哈市正式成立，並成立治安隊伍和法院。時值[淘金熱](../Page/淘金熱.md "wikilink")，奧馬哈在當時，為西進淘金者的中轉站。也因此奧馬哈經濟蓬勃起來。於1870年，奧馬哈人口共有16,083人；至1890年，人口已達140,452人。而到了1920年，更有191,061人。當時奧馬哈的畜牧業甚為發達，全美五家肉類包裝廠，有四家是位於奧馬哈；而全市有一半勞動力，是從事肉類加工。

[二次大戰後奧馬哈開始轉型](../Page/二次大戰.md "wikilink")。自1950年代起，有40多家保險公司在奧馬哈設立總部。與此同時肉類加工業開始衰落，相關職位一共流失了100,000個。亦因此在往後數十年，原來工廠舊址紛紛被改造，成為新式商業大廈和購物商場。

## 地理

奥马哈總面積307.9km²（118.9 sq mi），其中陸地面積115.7 sq mi（299.7 km²），水域面積3.2 sq
mi（8.2 km²）。

### 氣候

奥马哈属[溫帶大陸性濕潤氣候](../Page/溫帶大陸性濕潤氣候.md "wikilink")，四季分明。冬季寒冷而漫长，潮湿，日照少，日最高气温低于的平均日数为43天，日最低气温低于的平均日数为19天，以下的有6.7天；夏季相对炎热潮湿，日最高气温超过的日数年均有54天，以上的有8.7天。\[1\]最冷月（1月）均温，极端最低气温（1884年1月5日）。\[2\]最热月（7月）均温，极端最高气温（1936年7月25日）。\[3\]无霜期平均为173天（4月21日至10月10日）；可测量降雪平均期为11月10日至3月28日。\[4\]年均降水量约，年极端最少降水量为（1934年），最多为（1883年）。\[5\]年均降雪量为；1953–54年的降雪量最少，积累降雪量只有，1911–12年的降雪量最多，积累降雪量为。\[6\]

## 經濟

傳統上奥馬哈的經濟主要和農業有關，也是著名牲畜市場和肉類加工城市。今日則主要是銀行業務、保險、電信、建築和運輸。

[美國著名投資家](../Page/美國.md "wikilink")[巴菲特的](../Page/沃倫·巴菲特.md "wikilink")[旗艦公司](../Page/旗艦.md "wikilink")[波克夏·哈薩威公司即設址於此](../Page/波克夏·哈薩威公司.md "wikilink")。

按美國官方資料，全市最大僱主首五位分別是：

  - 奧夫特空軍基地：逾7,500名員工
  - CHI醫療：逾7,500名員工
  - 奧馬哈公立學校：逾5,000名員工
  - 市衛生醫療系統：逾5,000名員工
  - 州立醫療中心：逾5,000名員工

## 名人

  - [杰拉尔德·福特](../Page/杰拉尔德·福特.md "wikilink")：美国第三十八任总统
  - [沃伦·巴菲特](../Page/沃伦·巴菲特.md "wikilink")：美国亿万富翁
  - [马尔科姆·X](../Page/马尔科姆·X.md "wikilink")：[美国黑人民权运动家](../Page/美国黑人民权运动.md "wikilink")
  - [唐纳德·基奥](../Page/唐纳德·基奥.md "wikilink")：可口可乐前总裁
  - [查理·芒格](../Page/查理·芒格.md "wikilink")：美国亿万富翁、伟大投资人
  - [劳伦斯·克莱因](../Page/劳伦斯·克莱因.md "wikilink")，著名经济学家，1980年[诺贝尔经济学奖得主](../Page/诺贝尔经济学奖.md "wikilink")

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a> <a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/不伦瑞克.md" title="wikilink">不伦瑞克</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Lithuania.svg" title="fig:Flag_of_Lithuania.svg">Flag_of_Lithuania.svg</a> <a href="../Page/立陶宛.md" title="wikilink">立陶宛</a><a href="../Page/希奥利艾.md" title="wikilink">希奥利艾</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ireland.svg" title="fig:Flag_of_Ireland.svg">Flag_of_Ireland.svg</a> <a href="../Page/爱尔兰.md" title="wikilink">爱尔兰</a><a href="../Page/内斯.md" title="wikilink">内斯</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Mexico.svg" title="fig:Flag_of_Mexico.svg">Flag_of_Mexico.svg</a> <a href="../Page/墨西哥.md" title="wikilink">墨西哥</a><a href="../Page/哈拉帕.md" title="wikilink">哈拉帕</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg" title="fig:Flag_of_Ukraine.svg">Flag_of_Ukraine.svg</a> <a href="../Page/乌克兰.md" title="wikilink">乌克兰</a><a href="../Page/阿爾喬莫夫斯克.md" title="wikilink">阿爾喬莫夫斯克</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

{{-}}

[奧馬哈_(內布拉斯加州)](../Category/奧馬哈_\(內布拉斯加州\).md "wikilink")

1.
2.
3.
4.
5.
6.