**奥克兰**（[英语](../Page/英语.md "wikilink")：**Auckland**； 或
），老華僑譯作**屋崙**，是[新西兰的一個都會區](../Page/新西兰.md "wikilink")，位於南[太平洋南緯](../Page/太平洋.md "wikilink")37度，為[北岛最大的城市](../Page/北岛_\(纽西兰\).md "wikilink")。人口约150万，聚居了全国约32％的人口，\[1\]也是世界上[波利尼西亚人最多的城市](../Page/波利尼西亚人.md "wikilink")。\[2\]奥卡兰被称为“帆之都”，同时也是新西兰[工业和](../Page/工业.md "wikilink")[商业中心](../Page/商业.md "wikilink")。

[新西兰的首都早先設在奧克蘭](../Page/新西兰.md "wikilink")，後來於1865年遷至北岛南端的[惠灵顿](../Page/惠灵顿.md "wikilink")
。現今的奧克蘭仍然為[新西兰最發達的地區之一](../Page/新西兰.md "wikilink")，同时也是[南太平洋的枢纽](../Page/南太平洋.md "wikilink")，旅客出入境的主要地點。在2014年的[世界最佳居住城市評選中](../Page/世界最佳居住城市.md "wikilink")，奧克蘭高居全球第三位。\[3\]在2015年的[世界最佳居住城市評選中](../Page/世界最佳居住城市.md "wikilink")，奧克蘭則排名全球第十位。奧克蘭因其在商業，藝術和教育方面的重要性而被GaWC歸類為[Beta+世界城市](../Page/全球城市#GaWC的世界級城市名冊.md "wikilink")。\[4\]

## 历史

### 早期历史

奥克兰这个地区早在1350年就有[毛利人从南太平洋岛群移民过来](../Page/毛利人.md "wikilink")，在欧洲探险家到来之前，大约有两万名毛利人在此地生活。\[5\]\[6\]毛利人把这个地方取名叫"Tamaki
Makarau"就是“一千个情人的地峡”代表很多人想在这里住。\[7\]纳提瓦图瓦（Ngati
Whatua）和太奴伊（Tainui）曾经是世代生活在那片区域的两个主要部落。因为这火山的土地非常肥沃，竞争对手很多，他们就必须要建造“帕”（pā
，就是防卫性的山塞村在火山峰顶）。那些遗留在火山上的土木工程遗迹至今仍清晰可见，比如[伊甸山](../Page/伊甸山.md "wikilink")（Mount
Eden）以及[一树山](../Page/一树山.md "wikilink")（One Tree
Hill）上的那些。在那段欧洲人的移民时期，由于战争和后来的大批欧洲移民，奥克兰中部区域的毛利人急速减少，但也没有足够证据来证明是欧洲移民的到来使得毛利人口剧减。\[8\]\[9\]

### 奥克兰的诞生

在1769年的时候，英国船长[詹姆斯·庫克经过奥卡兰这个地区](../Page/詹姆斯·庫克.md "wikilink")，但是他却没有进入到懷特瑪塔港（Waitemata
Harbour）和豪拉基灣（Hauraki Gulf）。直到1820年时，山缪·马斯丹才发现今日所谓的奥克兰市中心。

到1840年2月6日的时候，[威廉·霍布森](../Page/威廉·霍布森.md "wikilink")（William
Hobson）上尉被[英国政府派到](../Page/英国.md "wikilink")[新西兰跟当地的毛利原住民签](../Page/新西兰.md "wikilink")《[怀唐伊条约](../Page/怀唐伊条约.md "wikilink")》后，这块地就用六英镑被买下来，霍布森他就选择奥克兰做为这新[殖民地的首都](../Page/殖民地.md "wikilink")。城市名以之后担任[印度总督的](../Page/印度总督.md "wikilink")[乔治·伊登，第一代奥克兰伯爵命名](../Page/乔治·伊登，第一代奥克兰伯爵.md "wikilink")。\[10\]1841年，奥克兰正式成为新西兰的首都。\[11\]然而，虽然自1840年就认为惠灵顿十分靠近[南岛](../Page/南岛.md "wikilink")（即南北岛的中心）是作为行政首都的最好选择，但正式迁都时已经是1865年了。1865年之后，虽然奥克兰失去了首都的地位，但仍然是设立于北岛的奥克兰省（1853-1876）的重要城市。

### 现代发展

[360_auckland.jpg](https://zh.wikipedia.org/wiki/File:360_auckland.jpg "fig:360_auckland.jpg")

  - 奥克兰北、中、東至西區皆有來自亞洲各地的華人及亞洲人居住，1990年時期很多台灣移民居住於中區，而東區以台灣和香港移民為多。2000年後漸漸有中國大陸移民遷入中區，東區和北岸。
  - 2012年以后，奥克兰一直在强调其
    “大奥克兰”的行政区域划分，目前随着人口的高速增长，北岸和东区在亚洲人逐步增长显著，西区也已经被很好的开发，吸引大量移民，南区大量岛民，但是也被逐步开发。
  - 2015年由于过高的奥克兰房价，均价超过90万纽币（别墅），政府出台一系列措施去抑制房价，在2015年底初见成效，但是同时也逼迫大量的投资转向奥克兰之外的区域，比如较为著名的：皇后镇。
  - 2016年9月奥克兰房价均价突破100万纽币。2017年《Demographia全球住房可负担性报告》中，奥克兰在全球最住不起城市中排名更进一步，跻身前四。房价暴涨和收入不动是住房危机恶化的直接原因。现在只有香港、悉尼和温哥华劣於此城。

## 觀光

  - [天空塔](../Page/天空塔.md "wikilink")（Sky City）—— 奧克蘭的地標建築
    位于奥克兰市中心,旁边有Skycity赌场
  - [教會灣](../Page/教會灣.md "wikilink")（Mission Bay）
    离奥克兰市区驾车10-15分钟可达，是距离市中心最近的海滩
  - [德文港](../Page/德文港_\(新西兰\).md "wikilink")（Devonport）—— 新西蘭皇家海軍基地就設于此
  - [朗伊托托島](../Page/朗伊托托島.md "wikilink")（Rangitoto）朗伊托托島是新西蘭的火山島，位於奧克蘭附近的豪拉基灣，面積23.1平方公里，最高點海拔高度260米，該島在1890年被列為自然保育區，島上無人居住。
  - [懷赫科島](../Page/怀希基岛.md "wikilink")（Waiheke Island）-- 怀希基岛是豪拉基湾的一座岛.

该岛是豪拉基湾第二大岛，仅次于大巴里尔岛。它是群岛中人口最多的，定居者近8000。从奥克兰市中心Downtown码头坐船可达

  - [奧克蘭戰爭紀念博物館](../Page/奧克蘭戰爭紀念博物館.md "wikilink")（Auckland War Memorial
    Museum）
  - [北島佛光山](../Page/北島佛光山.md "wikilink")
  - Auckland Museum
  - Kelly Tarlton's Antarctic Encounter & Underwater World
  - Piha Piha海滩 位于奥克兰郊区,可自驾车抵达

## 姐妹城市

奧克蘭與以下城市為姐妹城市。\[12\]

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/布里斯本.md" title="wikilink">布里斯本</a></p></li>
<li><p><a href="../Page/廣州.md" title="wikilink">廣州</a>[13]</p></li>
<li><p><a href="../Page/寧波.md" title="wikilink">寧波</a></p></li>
<li><p><a href="../Page/青島.md" title="wikilink">青島</a></p></li>
<li><p><a href="../Page/漢堡.md" title="wikilink">漢堡</a></p></li>
<li><p><a href="../Page/哥爾威.md" title="wikilink">哥爾威</a></p></li>
<li><p><a href="../Page/福岡.md" title="wikilink">福岡</a></p></li>
<li><p><a href="../Page/富岡町.md" title="wikilink">富岡町</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/品川.md" title="wikilink">品川</a></p></li>
<li><p><a href="../Page/加古川市.md" title="wikilink">加古川市</a></p></li>
<li><p><a href="../Page/宇都宮.md" title="wikilink">宇都宮</a></p></li>
<li><p><a href="../Page/釜山.md" title="wikilink">釜山</a></p></li>
<li><p><a href="../Page/浦項.md" title="wikilink">浦項</a></p></li>
<li><p><a href="../Page/楠迪.md" title="wikilink">楠迪</a></p></li>
<li><p><a href="../Page/臺中.md" title="wikilink">臺中</a></p></li>
<li><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 參看

  - [奧克蘭市](../Page/奧克蘭市.md "wikilink")：包括了奧克蘭海峽的城市。
  - [奧克蘭文法學校](../Page/奧克蘭文法學校.md "wikilink")：奧克蘭市聲譽和知名度較高的公立高中。
  - [乔治·伊登](../Page/乔治·伊登.md "wikilink")：奧克蘭的第一位伯爵。

## 注釋

[分類:古都](../Page/分類:古都.md "wikilink")

[奧克蘭_(紐西蘭)](../Category/奧克蘭_\(紐西蘭\).md "wikilink")
[Category:紐西蘭城市](../Category/紐西蘭城市.md "wikilink")
[Category:1840年建立](../Category/1840年建立.md "wikilink")
[Category:城市群](../Category/城市群.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [Historical
    Context](http://www.aucklandcity.govt.nz/council/documents/managementplans/victoriapark/history.asp)
     (from the Auckland City Council website, Victoria Park Management
    Plan)
8.
9.
10. *[What's Doing In;
    Auckland](http://query.nytimes.com/gst/fullpage.html?res=9C0CEFD6123BF936A15752C1A966958260&scp=31&sq=auckland&st=nyt)*
    – *[The New York Times](../Page/The_New_York_Times.md "wikilink")*,
    25 November 1990
11.
12.
13.