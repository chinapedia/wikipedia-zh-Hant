<div style="float:right;">

| 奧克蘭市   |
| ------ |
|        |
| 地区     |
| 范围     |
| 地方议会住址 |

</div>

**奧克蘭市**（英文：**Auckland
City**）是[紐西蘭](../Page/紐西蘭.md "wikilink")[北島大](../Page/北島_\(紐西蘭\).md "wikilink")[奧克蘭大都市地区當中的一個主要城市](../Page/奧克蘭_\(紐西蘭\).md "wikilink")，位于奧克蘭地峡上。它周遭的城市還有[北岸市](../Page/北岸市.md "wikilink")、[懷塔卡瑞市和](../Page/懷塔卡瑞市.md "wikilink")[曼努考市](../Page/曼努考.md "wikilink")。上一任[奥克兰市長是](../Page/奥克兰市長.md "wikilink")[约翰·班克斯](../Page/约翰·班克斯.md "wikilink")。

## 历史

1989年新西兰中央政府对地区行政区划进行重大改革，奧克蘭市与附近8个小的行政区合并成[奧克蘭大區](../Page/奧克蘭大區.md "wikilink")。新的市区比过去人口增加了一倍，其边界是今天的市区边界。
2010年11月1日新西兰中央政府依據相關的法律，合並臨近的7個城市與8個議會成立[超級奧克蘭市](../Page/奧克蘭都會區.md "wikilink")（繼續沿用奧克蘭市的稱號）。新一屆當選的市長
Len Brown 和市議會議員宣誓就任。

## 地理

奧克蘭市位于一个[地峡上](../Page/地峡.md "wikilink")。它连接[豪拉基湾上的](../Page/豪拉基湾.md "wikilink")[懷特瑪塔港与新西兰北岸地区](../Page/懷特瑪塔港.md "wikilink")，以及[塔斯曼海上的](../Page/塔斯曼海.md "wikilink")[曼努考港湾与南部的](../Page/曼努考港湾.md "wikilink")[曼努考市](../Page/曼努考.md "wikilink")。

## 市区列表

以下列表的顺序按地理位置相对于市中心顺时针方向。

  - **市中心**：[密歇尼克湾](../Page/密歇尼克湾.md "wikilink")（Mechanics
    Bay）、[帕奈尔](../Page/帕奈尔.md "wikilink")（Parnell）、[瑞慕拉](../Page/瑞慕拉.md "wikilink")（Remuera）、[纽马克特](../Page/纽马克特.md "wikilink")（Newmarket，又譯紐梅克特）、[格拉夫顿](../Page/格拉夫顿.md "wikilink")（GRAFTON）、[伊甸山](../Page/伊甸山.md "wikilink")（Mount
    Eden）、[贝尔慕勒](../Page/贝尔慕勒.md "wikilink")（Balmoral）、[晨曦地](../Page/晨曦地.md "wikilink")（Morningside）、[格瑞林](../Page/格瑞林.md "wikilink")（Grey
    Lynn）、[庞逊贝](../Page/庞逊贝.md "wikilink")（Ponsonby）、[弗里曼湾](../Page/弗里曼湾.md "wikilink")(Freemans
    Bay)、[赫恩海湾](../Page/赫恩海湾.md "wikilink")（Herne
    Bay）、[圣玛丽湾](../Page/圣玛丽湾.md "wikilink")（Saint
    Mary's Bay）
  - **东部**：[使命湾](../Page/使命湾.md "wikilink")（Mission
    Bay）、[多希玛拉玛](../Page/多希玛拉玛.md "wikilink")（Kohimarama）、[圣海利尔斯](../Page/圣海利尔斯.md "wikilink")(Saint
    Heliers)、[奧拉基](../Page/奧拉基.md "wikilink")（Orakei）、[格林德威](../Page/格林德威.md "wikilink")（Glendowie）、[格林因斯](../Page/格林因斯.md "wikilink")(Glen
    Innes)、[密多班克](../Page/密多班克.md "wikilink")（Meadowbank）、[英格兰角](../Page/英格兰角.md "wikilink")（Point
    England）、[圣约翰](../Page/圣约翰.md "wikilink")（Saint
    John）、[塔玛基](../Page/塔玛基.md "wikilink")（Tamaki）、[潘姆拉](../Page/潘姆拉.md "wikilink")（Panmure）、[威灵顿山](../Page/威灵顿山.md "wikilink")（Mount
    Wellington）、[艾勒斯利](../Page/艾勒斯利.md "wikilink")（Ellerslie）
  - **南部**：[潘罗丝](../Page/潘罗丝.md "wikilink")（Penrose）、[格林蘭](../Page/格林蘭.md "wikilink")(Greenlane)、[奥拉加](../Page/奥拉加.md "wikilink")（Oranga）、[德帕帕帕](../Page/德帕帕帕.md "wikilink")（Te
    Papapa）、[奥尼湖加](../Page/奥尼湖加.md "wikilink")（Onehunga）、[独树山](../Page/独树山.md "wikilink")（One
    Tree Hill）、[罗耶沃卡](../Page/罗耶沃卡.md "wikilink")(Royal
    Oak)、[伊普森](../Page/伊普森.md "wikilink")（Epsom）、[希尔斯伯拉](../Page/希尔斯伯拉.md "wikilink")（Hillsborough）、[三王区](../Page/三王区.md "wikilink")（Three
    Kings）、[洛斯基山](../Page/洛斯基山.md "wikilink")（Mount
    Roskill）、[林菲尔德](../Page/林菲尔德.md "wikilink")（Lynfield）、[布洛克霍斯湾](../Page/布洛克霍斯湾.md "wikilink")（Blockhouse
    Bay）、[桑德汉姆](../Page/桑德汉姆.md "wikilink")（Sandringham）
  - **西部**：[渥瓦拉加](../Page/渥瓦拉加.md "wikilink")（Owairaka）、[亞芳代爾](../Page/亞芳代爾.md "wikilink")、[艾伯特山](../Page/艾伯特山.md "wikilink")（Mount
    Albert）、[国王地](../Page/国王地.md "wikilink")（Kingsland）、[沃特维尔](../Page/沃特维尔.md "wikilink")（Waterview）、[韦斯顿斯普林斯](../Page/韦斯顿斯普林斯.md "wikilink")(Western
    Springs)、[骑士角](../Page/骑士角.md "wikilink")（Point
    Chevalier）、[韦斯特梅尔](../Page/韦斯特梅尔.md "wikilink")（Westmere）

[360_auckland.jpg](https://zh.wikipedia.org/wiki/File:360_auckland.jpg "fig:360_auckland.jpg")

## 姐妹城市

以下为奥克兰市与其他各国家及地区友好城市的内容：\[1\] \[2\]

  - [布里斯班](../Page/布里斯班.md "wikilink"),
    [澳大利亚](../Page/澳大利亚.md "wikilink")

  - [廣州](../Page/廣州.md "wikilink"),
    [中華人民共和國](../Page/中華人民共和國.md "wikilink")

  - [宁波](../Page/宁波.md "wikilink"), 中華人民共和國

  - [青岛](../Page/青岛.md "wikilink"), 中華人民共和國

  - [汉堡](../Page/汉堡.md "wikilink"), [德国](../Page/德国.md "wikilink")

  - [戈尔韦](../Page/戈尔韦.md "wikilink"), [爱尔兰](../Page/爱尔兰.md "wikilink")

  - [福冈市](../Page/福冈市.md "wikilink"), [日本](../Page/日本.md "wikilink")

  - [富冈町](../Page/富冈町.md "wikilink"), 日本

  - [品川区](../Page/品川区.md "wikilink"), 日本

  - [加古川市](../Page/加古川市.md "wikilink"), 日本

  - [宇都宫市](../Page/宇都宫市.md "wikilink"), 日本

  - [釜山](../Page/釜山.md "wikilink"), [韩国](../Page/韩国.md "wikilink")

  - [浦项](../Page/浦项.md "wikilink"), 韩国

  - [楠迪](../Page/楠迪.md "wikilink")，[斐济](../Page/斐济.md "wikilink")

  - [臺中](../Page/臺中.md "wikilink"), [中華民國](../Page/中華民國.md "wikilink")

  - [洛杉矶](../Page/洛杉矶.md "wikilink"), [美国](../Page/美国.md "wikilink")

## 参考文献

[Category:紐西蘭城市](../Category/紐西蘭城市.md "wikilink") [Category:奧克蘭
(紐西蘭)](../Category/奧克蘭_\(紐西蘭\).md "wikilink")

1.
2.