[1911](../Page/1911世界大賽.md "wikilink"){{·}}[1913](../Page/1913世界大賽.md "wikilink"){{·}}[1929](../Page/1929世界大賽.md "wikilink"){{·}}[1930](../Page/1930世界大賽.md "wikilink"){{·}}[1972](../Page/1972世界大賽.md "wikilink"){{·}}[1973](../Page/1973世界大賽.md "wikilink"){{·}}[1974](../Page/1974世界大賽.md "wikilink"){{·}}[1989](../Page/1989世界大賽.md "wikilink")
| LEAGUE = 美國聯盟 | P = (15) | PENNANTS =
1902、1905、1910、1911、1913、1914、1929、1930、1931、1972、1973、1974、1988、1989、1990
| misc1 = | OTHER PENNANTS = | DIV = 美聯西區 | DV = (16) <sup>\[1\]</sup> |
Division Champs =
1971、1972、1973、1974、1975、1981、1988、1989、1990、1992、2000、2002、2003、2006、2012、2013
| misc5 = | OTHER DIV CHAMPS = | WC = (3) | Wild Card =
2001、[2014](../Page/2014年美國聯盟外卡晉級賽.md "wikilink")、[2018](../Page/2018年美國聯盟外卡晉級賽.md "wikilink")
| current league = 美國聯盟 | misc6 =
<small>\[1\]—1994年因為球員罷工，使得球季最後八個星期賽事被取消。奧克蘭運動家當時戰績雖然是少於五成勝率12場，但卻是排名第二，在[德州遊騎兵後](../Page/德州遊騎兵.md "wikilink")。當年沒有宣佈任何正式獎項。
2002年103勝 </small> | y1 = 1901年 | division =
[西區](../Page/美國聯盟西區.md "wikilink") | y2 = 1969年 | misc2
= | nickname = 奧克蘭運動家(Oakland Athletics) | y3 = 1968年 | pastnames
=堪薩斯市運動家 (1955年-1967年)

  - 費城運動家 (1901年-1954年)

<small> | ballpark =
[奧克蘭–阿拉米達郡競技場](../Page/奧克蘭–阿拉米達郡競技場.md "wikilink")—(1968年-1998年；現名)
| y4 = 1968年 | pastparks =\*舊名**麥考菲競技場**(2004年-2008年)

  -   - 舊名**Network Associates競技場** (1998年-2004年)

  - [**市立體育場**](../Page/市立體育場_\(堪薩斯市\).md "wikilink") (堪薩斯市)
    (1955年-1967年)

  - **[Shibe球場](../Page/Shibe球場.md "wikilink")**（費城） (1909年-1954年)

      - 舊名**唐尼麥克體育場** (1953年-1954年)

  - **[哥倫比亞球場](../Page/哥倫比亞球場.md "wikilink")**（費城）(1901年-1908年)

| retirednumbers = 9、27、34、42、43 | Uniform = ALW-Uniform-OAK.png |owner
= [Lew Wolff](../Page/:en:Lewis_Wolff.md "wikilink") |manager =
[鮑勃·馬文](../Page/鮑勃·馬文.md "wikilink") (Bob Melvin) |gm =
[大衛·佛斯特](../Page/大衛·佛斯特.md "wikilink") (David Frost) |}}

**奥克兰运动家**（），是一支主場位於[加州](../Page/加利福尼亚州.md "wikilink")[奥克兰的](../Page/奥克兰_\(加利福尼亚州\).md "wikilink")[美國職棒大聯盟球隊](../Page/美國職棒大聯盟.md "wikilink")，隸屬於[美國聯盟](../Page/美國聯盟.md "wikilink")[西區](../Page/美國聯盟西區.md "wikilink")。自1968年遷至奧克蘭後至今的主場，就一直是[奧克蘭–阿拉米達郡競技場](../Page/奧克蘭–阿拉米達郡競技場.md "wikilink")。

## 球队历史

### 奧克蘭 (1968年至今)

#### 第三王朝(1972年-1975年)

剛遷入奧克蘭的運動家隊是一支正凝聚在一起的隊伍, 當時的老闆是Charlie
Finley。球隊遷入了剛完工一年的[奧克蘭–阿拉米達郡競技場](../Page/奧克蘭–阿拉米達郡競技場.md "wikilink")。1968年5月8日，在一場對[明尼蘇達雙城的比賽裡](../Page/明尼蘇達雙城.md "wikilink")，[Jim
"Catfish"
Hunter投出了美國聯盟自](../Page/Catfish_Hunter.md "wikilink")1922年以來的第一場[完全比賽](../Page/完全比賽.md "wikilink")。在總教練[Bon
Kennedy的帶領下](../Page/Bon_Kennedy.md "wikilink")，A's於1969年完成了他們自1952年來的第一個勝利球季。Finley在1970年正式把隊名從「Athletics」改到「A's」，這是第一年「撇號-s」出現在傳統「A」球隊標誌後。

A's在剛進入1970年代時終於打出好成績。在1970年又一次分區第二名後，A's於1971年贏得美聯西區冠軍，這是他們1931年來首次進入季後賽。但是，運動家隊於[美國聯盟冠軍賽敗給](../Page/美國聯盟冠軍賽.md "wikilink")[巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")。1972年，A's贏得1931年來第一個美國聯盟冠軍，得到在1972年[世界大賽對上](../Page/世界大賽.md "wikilink")[辛辛那提紅人的機會](../Page/辛辛那提紅人.md "wikilink")。

那年，A's開始穿上全綠色或全黃色制服上衣，配合對比的白球褲，同時期其他球隊仍穿著全白主場制服，和全灰客場制服。這類似業餘壘球球隊的制服在當時被認為是獨樹一格的。又為了配合「鬍子日」（Moustache
Day）的促銷活動，Finley表示將發給任何在父親節前蓄鬍球員500美元的獎金，同時其他球隊仍禁止球員讓臉上毛髮長長。父親節時，每一位球員都獲得了這獎金。因為這個原因，1972年世界大賽被稱為「毛髮對上整潔」（The
Hairs vs. the
Squares），紅人隊仍穿著傳統式制服以及要求球員剪短髮和剃鬍乾淨。一本同時期的敘述這時期A's的書，稱他們為「鬍子幫」(Moustache
Gang)。運動家隊在七場比賽擊敗被看好的紅人隊，得到1930年來球隊首次世界大賽冠軍。

運動家隊成功的於1973年與1974年保住世界大賽冠軍。不像Mack的冠軍陣容可以以大幅優勢擊敗對手，1970年代的A's只以打出比其他對手稍好的成績，在被戲稱為「美國聯盟最小」（American
League
Least）的美聯西區獲得分區冠軍。他們以優異的防守、投球、和適時的安打，擊敗季賽時勝場比他們多的隊伍。Finley稱呼這個隊伍為「揮擊A's(Swinin'
A's)」。球員如[Reggie Jackson](../Page/Reggie_Jackson.md "wikilink")、[Sal
Bando](../Page/Sal_Bando.md "wikilink")、[Joe
Rudi](../Page/Joe_Rudi.md "wikilink")、[Bert
Campaneris](../Page/Bert_Campaneris.md "wikilink")、[Catfish
Hunter](../Page/Catfish_Hunter.md "wikilink")、[Rollie
Fingers](../Page/Rollie_Fingers.md "wikilink")、和[Vida
BLue是這時期運動家隊的核心陣容](../Page/Vida_BLue.md "wikilink")。

許多球員在數年後表示，當時隊伍能有這優異表現，是因為共同對Finley的厭惡。舉例來說，在1969年在Jackson打出47支全壘打後，Finley揚言要把他送下[小聯盟](../Page/小聯盟.md "wikilink")；聯盟理事長[Bowie
Kuhn被迫介入這合約糾紛](../Page/Bowie_Kuhn.md "wikilink")。在Blue獲得1971年[賽揚獎後](../Page/賽揚獎.md "wikilink")，Finley威脅把他送下小聯盟，Kuhn又再次被迫介入協調。Finley喜歡-{介入}-球隊運作的習慣源起[堪薩斯市時期](../Page/堪薩斯市.md "wikilink")。在這時候較著名的事件是1967年的近乎叛變；Finley對此做出的回應是釋出當時A's的最佳打者[Ken
Harrelson](../Page/Ken_Harrelson.md "wikilink")，他後來與[紅襪隊簽約](../Page/波士頓紅襪.md "wikilink")，幫助他們贏得冠軍。

運動家隊於1973年世界大賽擊敗[紐約大都會過程的唯一污點](../Page/紐約大都會.md "wikilink")，是Finley誇張的舉動。Finley強迫[Mike
Andrews簽下一張聲明書](../Page/Mike_Andrews.md "wikilink")，表示他因為受傷，才在第二場輸給大都會隊的比賽裡，第12局時發生連續兩次失誤。當其他球員、總教練[Dick
Williams](../Page/Dick_Williams.md "wikilink")、和幾乎所有的觀眾為Andrews辯護時，Kuhn強迫Finley撤回這聲明書。但這並不表示A's一定要讓Andrews上場比賽。當Andrews在第四場第8局上場擔任代打時，大都會隊球迷同情的起立鼓掌；他之後擊出滾地球出局，Finley隨即命令他在剩下來的比賽坐冷板凳，不得上場。Andrews之後再未於任何大聯盟比賽上場。因為這個事件，僅以82勝79敗完成球季的大都會隊，得以與明顯較優秀的運動家隊，纏鬥到第七場才敗下陣來。對這事件感到極端厭惡的Williams在比賽結束後隨即辭職。對此Finley的報復是否決Williams去擔任洋基隊的總教練。Finley聲稱因為Williams與運動家隊仍剩下一年合約，他並不能擔任其他球隊的總教練。Finley於1974年軟化了他的立場，容許Williams擔任[加州天使的總教練](../Page/洛杉磯安那翰天使.md "wikilink")。

在總教練[Alvin
Dark的帶領下](../Page/Alvin_Dark.md "wikilink")，運動家隊擊敗[洛杉磯道奇](../Page/洛杉磯道奇.md "wikilink")，取得1974年世界大賽冠軍，但在這之後，投手Catfish
Hunter提出申訴，指稱球隊違約，沒有在1974年準時對一項保險條款付款。1974年12月13日，仲裁人[Peter
Seitz做出有利於Hunter的裁決](../Page/Peter_Seitz.md "wikilink")。Hunter據此成為[自由球員](../Page/自由球員.md "wikilink")，然後與洋基隊簽訂1975年球季的合約。雖然沒有了Hunter，A's仍取得1975年美聯西區冠軍，但在聯盟冠軍賽被紅襪隊三場橫掃，輸下陣來。

#### 自由球員制度、A's的拆解、和Finley年代的結束

1975年，Finley對運動家隊冠軍時期在奧克蘭的差勁票房感到不滿，開始考慮再次把球隊遷往他處。當[西雅圖因為](../Page/西雅圖.md "wikilink")[西雅圖飛行員隊搬遷至](../Page/密爾瓦基釀酒人.md "wikilink")[密爾瓦基而控告大聯盟時](../Page/密爾瓦基.md "wikilink")，Finley和其他球隊擁有人想出了一個複雜的計劃，計劃把同時也表現不佳的[芝加哥白襪遷至西雅圖](../Page/芝加哥白襪.md "wikilink")，然後把運動家隊遷至芝加哥，進駐[康米斯基公園](../Page/美國行動通訊球場.md "wikilink")，這樣球隊將會離Finley在[印地安納州LaPorte的家較近](../Page/印地安納州.md "wikilink")。這個計劃最後沒有成功，因為白襪擁有人Arthur
Allyn把球隊賣給了另一位獨特的人士，Bill Veeck。Veeck沒有興趣把球隊遷離芝加哥。

隨著1976年球季的開始，大聯盟球員的合約也發生基本的變化。仲裁者Seitz裁定球隊對球員的保留條款，在球員合約到期後，只能保留一年。因此，所有未簽有多年合約的球員將在1976球季後成為[自由球員](../Page/自由球員.md "wikilink")。這是自早年[聯邦聯盟時期後](../Page/聯邦聯盟.md "wikilink")，議約權力首次轉移到球員身上。就像Mack之前兩次行為一樣，Finley對裁決的反應是把許多明星球員交易給其他球隊，和試著把其他球員賣出。1976年6月15日，Finley以各100萬美元把左外野手Rudi和後援投手Fingers交易到[波士頓](../Page/波士頓紅襪.md "wikilink")，和以150萬美元把投手Blue交易給[紐約洋基](../Page/紐約洋基.md "wikilink")。三天後，聯盟理事長Kuhn以「維護棒球的最佳利益」理由，撤銷了這兩個合約。在眾多紛擾下，A's仍以西區第二名完成球季，僅落後第一名[皇家隊](../Page/堪薩斯市皇家.md "wikilink")2.5場勝差。

許多運動家隊資深球員在1976年球季結束後取得自由球員資格，就如預期的絕大多數選擇離開球隊。這支大聯盟最富有傳奇之一的球隊，在移動3000英里距離和數十年後，再次遭到王朝陣容被分解的命運。就像1900年代初期的分解一樣，球隊的崩潰是完全和快速的。接下來的三年就像在[費城和](../Page/費城.md "wikilink")[堪薩斯市最差的時候一樣](../Page/堪薩斯市.md "wikilink")，A's兩次得到最後一名和一次得到倒數第二名。舉例來說，1977年，僅在A's贏得世界大賽冠軍三年後，球隊以美國聯盟最差的戰績結束球季，比擴編球隊[西雅圖水手更差](../Page/西雅圖水手.md "wikilink")。

在1977年球季結束後，Finley試著把Blue交易到[紅人隊來換來數位不突出和薪資較低的球員](../Page/辛辛那提紅人.md "wikilink")，但聯盟理事長Kuhn否決了這項交易，認定這不過是另一件試著把明星先發投手便宜賣出去的交易，就像他在1976年否決的交易一樣。Kuhn又認為國家聯盟西區的冠軍爭奪戰，將會隨著Blue加入已極優秀的紅人隊而變成一個玩笑。理事長在這之後批准一項比較平衡的交易，把後援投手[Doug
Bair交換到紅人隊](../Page/Doug_Bair.md "wikilink")。同時，以相同的理由，理事長批准了另一項與灣區另一邊的[舊金山巨人以Blue交換數位球員的交易](../Page/舊金山巨人.md "wikilink")。

自從遷來奧克蘭後，A's的票房總是不佳，即使在[世界大賽時期也是一樣](../Page/世界大賽.md "wikilink")，接下來三年的票房更差，主場[奧克蘭競技場](../Page/奧克蘭–阿拉米達郡競技場.md "wikilink")（Oakland
Coliseum）因此有人戲稱為「奧克蘭陵墓」（Oakland
Mausoleum），而球場的維護也越來越差。有一段時間，A's比賽由KALX轉播，KALX是一由[柏克萊加州大學經營](../Page/柏克萊加州大學.md "wikilink")，發射功率僅有10瓦的大學廣播電台。有些球迷戲稱這時的A's為「三A's」（Triple-A's），意味球隊這時只有3A級小聯盟水準，不像一支大聯盟球隊。Finley幾乎把球隊賣給將在1978年球季把球隊遷往[丹佛](../Page/丹佛.md "wikilink")、和在1979年遷往[紐奧良的買家](../Page/紐奧良.md "wikilink")。不意外的，1979年總共只有30萬6763位付費觀眾來觀賞A's比賽，是離開費城後最差的票房。

在三個場上和票房都很悽慘的球季後，球隊開始凝聚在一起。神來一筆的，Finley雇用紐約洋基隊的火爆浪子[Billy
Martin來成為這個年輕球隊的總教練](../Page/Billy_Martin.md "wikilink")。Martin讓他的年輕球員重新再相信自己的球隊，在「Billyball」打帶跑的球風和新秀盜壘王[瑞奇.韓德森出現下](../Page/瑞奇.韓德森.md "wikilink")，運動家隊於1980年球季得到了分區第二名。1981年，球隊贏得因罷工而中斷一陣子球季的「上半球季」冠軍，但在聯盟冠軍賽敗下陣來。

然而，Finley的妻子在同一球季訴求[離婚](../Page/離婚.md "wikilink")。因為Finley妻子不接受部份球隊的所有權為財產分配，球隊必須要被售出。雖然Finley找到一個將會把球隊遷往丹佛的買家，這項交易在奧克蘭市政府和[阿拉米達縣政府拒絕讓運動家隊提前解除球場租約後被宣告無效](../Page/阿拉米達縣.md "wikilink")。Finley尋找替代的當地買主，最後在1981年球季前把球隊賣給舊金山商人[Walter
A. Hass,
Jr.](../Page/Walter_A._Hass,_Jr..md "wikilink")，著名[牛仔褲製造商](../Page/牛仔褲.md "wikilink")[Levi
Strauss & Co.的當時董事長](../Page/Levi_Strauss_&_Co..md "wikilink")。

#### 當地人擁有運動家隊：Haas的年代

球隊贏得三次[世界大賽和兩次](../Page/世界大賽.md "wikilink")[美國聯盟西區冠軍的優異紀錄並未反映在Finley時代的票房上](../Page/美國聯盟西區.md "wikilink")。1968年至1980年的平均主場觀眾人數為每球季77萬7000人，1975年的107萬5518人是所有Finley擁有球隊的最多觀眾紀錄。與這相比的是，在Haas擁有球隊的第一年即有觀眾130萬4052人，而這一球季更是因為球員罷工而縮短的一年。假如沒有罷工的話，1981年A's全季觀眾人數將可超過220萬人。在Haas擁有球隊的15年裡，運動家隊是全聯盟在票房上最成功的球隊之一，在1990年球季裡共有290萬217人進場看球，仍是至今的運動家隊單季觀眾紀錄。這幾年的平均觀眾人數，在剔除1981年和1994年罷工年後，均多於190萬人。

Haas試著改變球隊的形象：他替換了球隊吉祥物「查理O」（Charlie O），[Connie
Mack和其他費城時期的球星照片開始出現在球隊辦公室裡](../Page/Connie_Mack.md "wikilink")。傳統球隊名稱「Athletics」被重新使用，擁有人團隊正式改名為「奧克蘭運動家棒球隊公司(Oakland
Athletics Baseball Company)」。球隊的隊色仍然是綠色、金色、和白色，但是原先偏灰色的凱利綠（Kelly
green）由較緩和的森林綠取代。在間斷了23年後，[大象於](../Page/大象.md "wikilink")1986年重新成為球隊吉祥物。之前在1954年至1960年客場球衣上出現的「Athletics」書寫體，在1987年被重新繡上主場球衣。

在Haas的擁有經營下，運動家隊的小聯盟農場系統被重建，得以在數年後成功培育出數位被選為美聯[年度最佳新人的明星球員](../Page/美國職棒大聯盟年度最佳新人獎.md "wikilink")，如[荷西·坎塞柯](../Page/荷西·坎塞柯.md "wikilink")(Jose
Canseco)（1986年）、[馬克·麥奎爾](../Page/馬克·麥奎爾.md "wikilink")(Mark
McGwire)（1987年）[Weiss](https://en.wikipedia.org/wiki/Walt)(1988年）。[湯尼·拉魯沙在](../Page/湯尼·拉魯沙.md "wikilink")1986年被聘請為球隊總教練，直到1995年底才離職。1987年，拉魯沙擔任總教練的第一年，A's以81勝81敗完成球季，是7個球季來的最佳戰績。自1988年起，A's連續三年贏得[美國聯盟冠軍](../Page/美國聯盟.md "wikilink")。有如球隊在費城時期一樣，運動家隊在這三年都以全聯盟最佳戰績完成球季：104勝（1988年）、99勝（1989年）以及109勝（1990年），球隊陣中有明星球員麥奎爾、坎塞柯、Weiss、[Carney
Lansford](../Page/Carney_Lansford.md "wikilink")、[Dave
Stewart](../Page/Dave_Stewart.md "wikilink")、和[Dennis
Eckersley](../Page/Dennis_Eckersley.md "wikilink")。
[Oakland_Athletics_logo_1983_to_1992.png](https://zh.wikipedia.org/wiki/File:Oakland_Athletics_logo_1983_to_1992.png "fig:Oakland_Athletics_logo_1983_to_1992.png")

季賽時的優勢僅為球隊帶來少許季後賽勝利。運動家的唯一冠軍是於1989年世界大賽，以四戰全勝橫掃[舊金山灣區對岸的](../Page/舊金山灣區.md "wikilink")[舊金山巨人](../Page/舊金山巨人.md "wikilink")。可惜的，A's橫掃巨人隊的喜悅被同時發生的[1989年舊金山大地震掩蓋過去](../Page/1989年舊金山大地震.md "wikilink")；地震發生在第三戰剛開始時，經由電視轉播到全國。這迫使剩下的比賽被延後數天。當世界大賽恢復進行時，場上的氣氛明顯的從球迷慶祝喜悅，轉變到鬆了口氣的感覺。在1988年和1990年世界大賽被看好的運動家隊卻分別敗給[洛杉磯道奇和](../Page/洛杉磯道奇.md "wikilink")[辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")。其中輸給紅人隊的那屆更是被四場橫掃，有如76年前敗給[波士頓勇士一樣](../Page/亞特蘭大勇士.md "wikilink")。之後球隊逐漸衰退，於1992年贏得美聯西區冠軍，但於美聯冠軍賽輸給[多倫多藍鳥](../Page/多倫多藍鳥.md "wikilink")，1993年更以最後一名結束球季。

#### Schott-Hofmann的年代

[Walter
Haas於](../Page/Walter_Haas.md "wikilink")1995年去世，球隊也隨後在1996年球季前售給了[舊金山灣區開發商](../Page/舊金山灣區.md "wikilink")[Steve
Schott](../Page/Stephen_Schott.md "wikilink")（與[辛辛那提紅人前擁有人](../Page/辛辛那提紅人.md "wikilink")[Marge
Schott無親戚關係](../Page/Marge_Schott.md "wikilink")）和[Ken
Hofmann](../Page/Ken_Hofmann.md "wikilink")。再一次的，運動家隊的明星球員都被交易出去，因為新老闆的首要目標是刪減球隊薪資支出。許多球員轉至[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")，這包括麥奎爾、Eckersley、和總教練拉魯沙。冥冥之中就像是38年前運動家隊把[Roger
Maris交易出去的事件重演](../Page/Roger_Maris.md "wikilink")，麥奎爾在轉到紅雀隊後的第一年就創下了新大聯盟單季最多全壘打紀錄。其實麥奎爾在1997年也有機會破紀錄，他在運動家隊和紅雀隊總共擊出58支全壘打。

Schott-Hofmann擁有人團隊把較多的資源，放在建立和維持一個強大的小聯盟系統，但也經常拒絕以高薪與即將成為[自由球員的明星球員續約](../Page/自由球員.md "wikilink")。或許是這個原因，21世紀交替時的運動家隊是一支經常在美聯西區名列前矛，但是無法勝出第一輪季後賽的球隊。運動家隊自2000年起連續四年進入季後賽，但是每一年都以2勝3負的成績，被淘汰於5戰3勝的第一輪季後賽。在2001年對[紐約洋基和](../Page/紐約洋基.md "wikilink")2003年對[波士頓紅襪的季後賽裡](../Page/波士頓紅襪.md "wikilink")，運動家隊都被上演讓二追三戲碼。2004年，A's沒有進入季後賽；A's輸掉了球季最後的一個系列賽，也輸掉了分區冠軍，給[安那翰天使](../Page/洛杉磯安那翰天使.md "wikilink")。

運動家隊在近年來以通稱「三巨投」的三位先發投手：[提姆·哈德森](../Page/提姆·哈德森.md "wikilink")、[Mark
Mulder](../Page/Mark_Mulder.md "wikilink")、和[貝瑞·齊托](../Page/貝瑞·齊托.md "wikilink")，以及三位[內野手](../Page/內野手.md "wikilink")：[Eric
Chavez](../Page/Eric_Chavez.md "wikilink")、[傑森·吉昂比](../Page/傑森·吉昂比.md "wikilink")、和[米格爾·特哈達著稱](../Page/米格爾·特哈達.md "wikilink")。吉昂比在2001年球季結束成為自由球員後與[紐約洋基簽約](../Page/紐約洋基.md "wikilink")，特哈達則在2003年球季後與[巴爾的摩金鶯簽約](../Page/巴爾的摩金鶯.md "wikilink")。

球隊總經理[比利·比恩在近年來](../Page/比利·比恩.md "wikilink")，也因作家[麥可‧路易士針對比恩新奇的球隊管理經營方式出版的](../Page/麥可‧路易士.md "wikilink")《[魔球](../Page/魔球—逆境中致勝的智慧.md "wikilink")》而聞名。運動家隊的球隊結構，讓傳統大聯盟評價一位球員能力的標準，開始改變。比恩選來許多缺乏傳統棒球能力如傳球、守備、長打能力和速度的球員、取而代之的球員都擁有一些通常被忽略的能力，如用高[上壘率取代高](../Page/上壘率.md "wikilink")[打擊率](../Page/打擊率.md "wikilink")、用高[三振](../Page/三振.md "wikilink")／[保送比例取代高球速](../Page/保送.md "wikilink")，而通常擁有這些能力的球員要求的薪資都不高。奧克蘭運動家在2002年，以全聯盟第六低的總薪資，贏得的美國聯盟最多的103場比賽。那年的全隊總薪資為四千一百萬美元，而同樣贏得103場比賽的洋基隊全隊總薪資則為一億二千六百萬美元。運動家隊經常違背市場趨勢的，繼續贏得比賽，即使總薪資常常是聯盟排名後幾名的。舉例來說，比恩在球隊於2004年球季得到分區第二名後，把提姆·哈德森交易到[亞特蘭大勇士](../Page/亞特蘭大勇士.md "wikilink")，和把[Mark
Mulder交易到](../Page/Mark_Mulder.md "wikilink")[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")。許多人不解為什麼把兩位在其巔峰的球員交易出去，但這項交易遵守了在《魔球》裡敘述的比恩式管理行為。

#### Wolff的年代

2005年3月30日，運動家隊被售給了由土地開發商[Lewis
Wolff帶領的團隊](../Page/Lewis_Wolff.md "wikilink")。雖然Wolff是一位[洛杉磯的商人](../Page/洛杉磯.md "wikilink")，但他也成功的執行許多在[聖荷西與鄰近地區的開發案](../Page/聖荷西_\(美國加州\).md "wikilink")。前一個球隊擁有人雇請Wolff來幫他們尋找適合興建新球場的土地。因為Wolff的背景，許多人猜測他將會把球隊遷往聖荷西。但所有遷往聖荷西的計劃將會受到灣區另一邊球隊[舊金山巨人的反對](../Page/舊金山巨人.md "wikilink")，因為巨人隊宣稱他們擁有聖荷西和[聖塔克拉拉縣的市場獨佔權](../Page/聖塔克拉拉縣_\(美國加州\).md "wikilink")（territorial
rights）。參見在下面的「球場問題」。

在2005年球季，因為三巨投被比恩拆散了，有些專家認為運動家隊將得到最後一名。起先，這些專家看起來是對的，5月31日時運動家隊以19勝32敗(勝率0.373)墊底。但在這之後，球隊開始發揮，打出了0.622的勝率，直到球季結束，最後的戰績是88勝74敗(勝率0.543)、僅落後剛改名的[洛杉磯安那翰天使](../Page/洛杉磯安那翰天使.md "wikilink")7場勝差，也曾有數個星期的時間，有機會登上美聯西區第一名的位置。

[投手](../Page/投手.md "wikilink")[Huston
Street被選為](../Page/Huston_Street.md "wikilink")2005年美聯[新人王](../Page/美國職棒大聯盟年度最佳新人.md "wikilink")，連續兩年一位運動家隊的球員得到這個獎項，[游擊手](../Page/游擊手.md "wikilink")[Bobby
Crosby在](../Page/Bobby_Crosby.md "wikilink")2004年獲得這個獎項。[三壘手](../Page/三壘手.md "wikilink")[Eric
Chavez連續五年得到美聯](../Page/Eric_Chavez.md "wikilink")[三壘手金手套獎](../Page/美國職棒大聯盟金手套獎.md "wikilink")。

在缺席兩年後，運動家隊於2006年回到季後賽裡。雖然運動家隊以93勝69負、領先[天使隊](../Page/洛杉磯安那翰天使.md "wikilink")4場的戰績得到美聯西區冠軍，但在與[明尼蘇達雙城的](../Page/明尼蘇達雙城.md "wikilink")[分區系列賽裡運動家隊是不被看好的](../Page/美國聯盟分區系列賽.md "wikilink")。在客場開始比賽與失去[二壘手](../Page/二壘手.md "wikilink")[Mark
Ellis的劣勢下](../Page/Mark_Ellis.md "wikilink")，運動家隊最後仍以3勝0負橫掃雙城隊，進入[美國聯盟冠軍賽](../Page/美國聯盟冠軍賽.md "wikilink")；Ellis在第二場因為一個觸身球，造成手指骨折而不得不退出之後的賽事。但勝利的喜悅是短暫的，在聯盟冠軍賽裡，運動家隊被[底特律老虎以](../Page/底特律老虎.md "wikilink")0勝4負橫掃。10月16日，在輸了聯盟冠軍賽後的第四天，總教練[Ken
Macha被球隊總經理](../Page/Ken_Macha.md "wikilink")[比利·比恩突然開除](../Page/比利·比恩.md "wikilink")。比恩宣佈因為是Macha與球員互動不佳、與球隊氣氛不快樂，為開除的原因\[1\]。

助理教練同時也是前大聯盟[捕手的](../Page/捕手.md "wikilink")[Bob
Geren繼任為總教練](../Page/Bob_Geren.md "wikilink")。2006年球季結束後，運動家隊王牌投手齊托在得到[自由球員資格後與](../Page/自由球員.md "wikilink")[舊金山巨人簽約](../Page/舊金山巨人.md "wikilink")，明星指定打擊與MVP候選人[法蘭克·湯瑪斯也在得到自由球員資格後與](../Page/法蘭克·湯瑪斯.md "wikilink")[多倫多藍鳥簽約](../Page/多倫多藍鳥.md "wikilink")，但運動家隊則簽來未來[名人堂成員](../Page/名人堂.md "wikilink")[麥克·皮耶薩來代替湯瑪斯](../Page/麥克·皮耶薩.md "wikilink")。皮耶薩在與運動家隊簽約前只在[國家聯盟球隊待過](../Page/國家聯盟.md "wikilink")，這也是他生涯首次當專職的[指定打擊](../Page/指定打擊.md "wikilink")。

對運動家隊而言，2007年球季是一個失望的球季，隊上主力球員如[Rich
Harden](../Page/Rich_Harden.md "wikilink")、[Huston
Street](../Page/Huston_Street.md "wikilink")、[Eric
Chavez](../Page/Eric_Chavez.md "wikilink")、和皮耶薩相繼受傷。季末，運動家隊的勝率不到五成，這是自1998年球季後首次以輸球戰績結束球季。

#### 2012年和2013年連續兩年贏得美聯西區冠軍

從季初開始運動家原本要重建，甚至沒人看好進入季後賽，但是因西區另一支強權天使季初雖然補強[亞伯特·普荷斯](../Page/亞伯特·普荷斯.md "wikilink")、C.J.
Wilson，但是天使戰績起伏不定而給予奧克蘭運動家崛起的契機，一直縮小跟遊騎兵之間的勝場差（最大勝場差曾達到13場），最後在球季的最後一個跟遊騎兵的系列賽，運動家意外的橫掃贏得系列賽，也贏得睽違六年美聯西區的冠軍，分區賽碰到2006年的對手底特律老虎，不過前2戰都輸，儘管運動家後2戰都贏球甚至在第4戰1：３落後最後再9下演出逆轉勝逼到決勝第五戰，只可惜運動家受到去年三冠王和美聯MVP的[賈斯丁·韋蘭德](../Page/賈斯丁·韋蘭德.md "wikilink")9局壓制下最後以0：6遭到完封，無緣闖進美聯冠軍賽。

2013年魔球大軍連續兩年在美聯西區封王，分區冠軍賽對手又是底特律老虎，卻還是同樣敗於[賈斯丁·韋蘭德領軍的老虎](../Page/賈斯丁·韋蘭德.md "wikilink")，且在分區冠軍賽第5戰已經6連敗。

### 2014年：無緣連續三年進季後賽

從季初開始運動家上半季表現優異以59勝36敗成績結束，下半季表現不如上半季最後連分區冠軍也被天使搶走，最後只能打[外卡驟死賽](../Page/外卡驟死賽.md "wikilink")，最終他們在外卡賽以8比9輸給了[堪薩斯城皇家隊](../Page/堪薩斯城皇家.md "wikilink")。

### 2015年到2017年：連三年成為爐主

2015年球季，運動家僅拿下68勝94敗，成為美聯西區最後一名。

2016年球季，運動家拿下69勝93敗。Coco
Crisp季中被交易到印地安人。球季結束後由John.J.Fisher擔任球隊新老闆。\[2\]

2017年，隊中王牌[桑尼·葛雷被交易至洋基](../Page/桑尼·葛雷.md "wikilink")。該年球隊拿下75勝87敗，連續三年都成為美西爐主。然而魔球軍本季並非毫無收穫，因為陣中的年輕強打們在9月放開來打，打得為季後賽做準備的列強們聞之色變，也替之後的重建之路點亮希望的燭光。

### 2018年至今：重返季後賽

2018年球季，雖然成為[大谷翔平連續](../Page/大谷翔平.md "wikilink")2場先發的最佳陪襯，潛力不差的運動家靠打線的精湛輸出打出驚奇，先後取代墮入凡間的天使和迷航的水手，成為挑戰太空人的刺客。看到有機會重返季後賽的運動家，在季中透過交易強化牛棚戰力，全力守護晉級的燭光。

9月12日，4月主場對紅襪投出無安打比賽的王牌左投Sean Manaea旋轉肌發炎，本季提前報銷讓魔球大軍先發輪值更加吃緊。

9月24日，靠著洋基4：1擊敗最後一個外卡競爭對手光芒，運動家繼2014年後再度打進季後賽。

9月26日，運動家在延長賽11下被對手再見轟，加上太空人4:1客場擊敗[多倫多藍鳥外](../Page/多倫多藍鳥.md "wikilink")，運動家只能從作客紐約的外卡割喉戰開始季後賽之旅，爭取對決紅襪的資格，最終2:7輸給[紐約洋基](../Page/紐約洋基.md "wikilink")，對運動家來說算是成功球季。

## 棒球名人堂球員

  - [Reggie Jackson](../Page/Reggie_Jackson.md "wikilink")
  - [Rickey Henderson](../Page/Rickey_Henderson.md "wikilink")
  - [Dennis Eckersley](../Page/Dennis_Eckersley.md "wikilink")
  - [Catfish Hunter](../Page/Catfish_Hunter.md "wikilink")
  - [Rollie Fingers](../Page/Rollie_Fingers.md "wikilink")

## 退休号码

<center>

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired09.PNG" title="fig:OaklandRetired09.PNG">OaklandRetired09.PNG</a><br />
<span style="font-weight:bold;"><a href="../Page/瑞吉·傑克森.md" title="wikilink">瑞吉·傑克森</a></span><br />
<a href="../Page/外野手.md" title="wikilink">外野手</a><br />
<small>2004年5月22日</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired24.png" title="fig:OaklandRetired24.png">OaklandRetired24.png</a><br />
<span style="font-weight:bold;"><a href="../Page/瑞奇·韓德森.md" title="wikilink">瑞奇·韓德森</a></span><br />
<a href="../Page/外野手.md" title="wikilink">外野手</a><br />
<small>2009年8月1日</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired27.PNG" title="fig:OaklandRetired27.PNG">OaklandRetired27.PNG</a><br />
<span style="font-weight:bold;"></span><br />
<a href="../Page/投手.md" title="wikilink">投手</a><br />
<small>1991年6月9日</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired34.PNG" title="fig:OaklandRetired34.PNG">OaklandRetired34.PNG</a><br />
<span style="font-weight:bold;"></span><br />
<a href="../Page/投手.md" title="wikilink">投手</a><br />
<small>1993年7月5日</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired43.PNG" title="fig:OaklandRetired43.PNG">OaklandRetired43.PNG</a><br />
<span style="font-weight:bold;"><a href="../Page/丹尼斯·艾克斯利.md" title="wikilink">丹尼斯·艾克斯利</a></span><br />
<a href="../Page/投手.md" title="wikilink">投手</a><br />
<small>2005年8月13日</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:WalterHaas.png" title="fig:WalterHaas.png">WalterHaas.png</a><br />
<span style="font-weight:bold;">Walter A. Haas, Jr.</span><br />
<small>尊敬</small><br />
<small>1995年</small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OaklandRetired42.PNG" title="fig:OaklandRetired42.PNG">OaklandRetired42.PNG</a><br />
<span style="font-weight:bold;"><a href="../Page/傑基·羅賓森.md" title="wikilink">傑基·羅賓森</a></span><br />
<small>大聯盟各隊的退休背號</small><br />
<small>1997年4月15日</small></p></td>
</tr>
</tbody>
</table>

</center>

### 登錄名單

## 小聯盟關係球隊 (2019年)

  - **AAA**：[拉斯維加斯飛行者](../Page/拉斯維加斯飛行者.md "wikilink") (Las Vegas
    Aviators)，[太平洋岸联盟](../Page/太平洋岸联盟.md "wikilink") (Pacific Coast
    League)
  - **AA**： (Midland Rock Hounds)， (Texas League)
  - **高级A**： (Stockton Ports)， (California League)
  - **A**： (Beloit Snappers)， (Midwest League)
  - **短A**：  (Vermont Lake Monsters)， (New York–Penn League)
  - **新秀**： (AZL Athletics)， (Arizona League)

## 参见

  - [比利·比恩](../Page/比利·比恩.md "wikilink")
  - [点球成金](../Page/点球成金.md "wikilink")

## 參考文獻

## 外部链接

  - [奧克蘭運動家隊官方網站](http://oakland.athletics.mlb.com/NASApp/mlb/oak/homepage/oak_homepage.jsp)

[O](../Category/美國職棒大聯盟球隊.md "wikilink")
[O](../Category/加利福尼亞州體育.md "wikilink")
[Category:舊金山灣區](../Category/舊金山灣區.md "wikilink")
[Category:1901年建立](../Category/1901年建立.md "wikilink")

1.  <http://sports.espn.go.com/mlb/news/story?id=2628358>
2.