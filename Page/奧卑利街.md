[Old_Bailey_Street.jpg](https://zh.wikipedia.org/wiki/File:Old_Bailey_Street.jpg "fig:Old_Bailey_Street.jpg")

**奧卑利街**是位於[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[半山區的一條下山車道](../Page/半山區.md "wikilink")。
奧卑利街全長200米，由[堅道](../Page/堅道.md "wikilink")[嘉諾撒聖心商學書院至](../Page/嘉諾撒聖心商學書院.md "wikilink")[-{荷里活}-道](../Page/荷里活道.md "wikilink")，途經[舊中區警署](../Page/舊中區警署.md "wikilink")、前[域多利監獄和](../Page/域多利監獄.md "wikilink")[士丹頓街](../Page/士丹頓街.md "wikilink")。

奧卑利街是香港俗稱[長命斜的道路之一](../Page/長命斜.md "wikilink")，其原因並非是該道路特別長或特別斜，而是與建於該道路上的[域多利監獄及昔日死刑制度有關](../Page/域多利監獄.md "wikilink")。當時執行死刑的刑場是較接近[亞畢諾道](../Page/亞畢諾道.md "wikilink")，因此死囚是囚禁在較接近那一邊的監倉，其他[囚犯則囚禁在較接近奧卑利街那一邊](../Page/囚犯.md "wikilink")。所以，能從奧卑利街門口出獄者即非「短命」死囚，而從這條路去探監的人，探望的都是「長命」的囚犯\[1\]。

## 歷史

[倫敦奧卑利](../Page/倫敦.md "wikilink") ([Old
Bailey](../Page/:en:Old_Bailey.md "wikilink"))
是[英國高等法院所在地](../Page/英國.md "wikilink")，[香港政府建](../Page/香港殖民地時期#香港政府.md "wikilink")[域多利監獄所在地街名也稱為奧卑利街](../Page/域多利監獄.md "wikilink")。

## 鄰近

  - [伊利近街](../Page/伊利近街.md "wikilink")
  - [中環蘇豪區](../Page/中環蘇豪區.md "wikilink")
  - [士丹頓街](../Page/士丹頓街.md "wikilink")
  - [贊善里](../Page/贊善里.md "wikilink")
  - 奧卑利街已婚警察宿舍
  - [中環至半山自動扶梯系統](../Page/中環至半山自動扶梯系統.md "wikilink")
  - [閣麟街](../Page/閣麟街.md "wikilink")
  - [擺花街](../Page/擺花街.md "wikilink")

## 資料來源

## 外部参考

  - [奧卑利街地圖](http://www.centamap.com/scripts/centamapgif.asp?lg=B5&tp=2&sx=&sy=&sl=&ss=0&mx=833853&my=815839&vm=&ly=&lb=&ms=2&ca=0&x=833853&y=815839&z=2)

[Category:中環街道](../Category/中環街道.md "wikilink")
[Category:半山區街道](../Category/半山區街道.md "wikilink")

1.