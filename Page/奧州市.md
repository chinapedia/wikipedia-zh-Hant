**奧州市**（）為[東北地方中部](../Page/東北地方.md "wikilink")、[岩手縣西南部](../Page/岩手縣.md "wikilink")、[胆澤平原的](../Page/北上盆地.md "wikilink")[市](../Page/市.md "wikilink")。為岩手縣內人口第二多的主要都市。

## 自然地理

  - 山岳丘陵:燒石岳（1548ｍ）、[種山](../Page/種山原.md "wikilink")（870m）、經塚山（519m）、束稻山（595m）
    河川水路:[北上川](../Page/北上川.md "wikilink")、胆澤川、[衣川](../Page/衣川_\(岩手縣\).md "wikilink")
    湖水池沼:金名水、銀名水、中沼

## 外部連結

  - [奧州市官方網站](http://www.city.oshu.iwate.jp/)
  - [奧州市官方臉書Facebook](http://www.facebook.com/oshucity)
  - [奧州市官方推特Twitter](http://twitter.com/oshu_city)

[Oshu-shi](../Category/岩手縣的市町村.md "wikilink")