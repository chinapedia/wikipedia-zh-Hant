**奧希金斯足球會**（**O'Higgins
F.C.**）是[智利足球俱乐部](../Page/智利.md "wikilink")，位于[兰卡瓜](../Page/兰卡瓜.md "wikilink")。俱乐部创建于1955年4月7日，现为[智利足球甲级联赛球队](../Page/智利足球甲级联赛.md "wikilink")。球队主体育场为厄尔特尼恩特体育场（Estadio
El Teniente），球场拥有24,000个座位。

## 球队荣誉

  - [智利足球乙级联赛](../Page/智利足球乙级联赛.md "wikilink") 冠军: 1964年

## 知名球员

  - [卡洛斯·巴斯托斯](../Page/卡洛斯·巴斯托斯.md "wikilink")

  - [马里奥·丹希德里奥](../Page/马里奥·丹希德里奥.md "wikilink")

  - [Juvenal Soto](../Page/Juvenal_Soto.md "wikilink")

  -  [尼尔森·阿科斯塔](../Page/尼尔森·阿科斯塔.md "wikilink")

  - [Clarence Acuña](../Page/Clarence_Acuña.md "wikilink")

  - [埃杜阿尔多·邦瓦莱特](../Page/埃杜阿尔多·邦瓦莱特.md "wikilink")

  - [克劳迪奥·博尔吉](../Page/克劳迪奥·博尔吉.md "wikilink")

  - [Juan Rogelio Núñez](../Page/Juan_Rogelio_Núñez.md "wikilink")

  - [Jaime Riveros](../Page/Jaime_Riveros.md "wikilink")

  - [约根·奥利弗·罗贝勒多](../Page/约根·奥利弗·罗贝勒多.md "wikilink")

  - [尼尔森·塔皮亚](../Page/尼尔森·塔皮亚.md "wikilink")

  - [马里奥·努涅斯](../Page/马里奥·努涅斯.md "wikilink")

  - [詹卡洛·马尔多纳多](../Page/詹卡洛·马尔多纳多.md "wikilink")

  - [Herly Alcázar](../Page/Herly_Alcázar.md "wikilink")

## 外部链接

  - [La Celeste Fan Site](http://www.laceleste.cl/)

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")