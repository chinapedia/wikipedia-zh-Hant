**奧托·弗朗茨大公**（Otto Franz of
Austria,），[奧地利哈布斯堡王朝皇室成員](../Page/奧地利哈布斯堡王朝.md "wikilink")，[皇储](../Page/皇储.md "wikilink")[弗朗茨·斐迪南大公之弟](../Page/弗朗茨·斐迪南大公.md "wikilink")。其子[卡爾於](../Page/卡爾一世_\(奧匈帝國\).md "wikilink")1916年即位為帝，成為[奧匈帝國末代皇帝](../Page/奧匈帝國.md "wikilink")。

奧圖·法蘭茲以其浪荡荒淫，行为不端而声名狼藉。
他在1886年娶了[萨克森王国的约瑟珐公主](../Page/萨克森王国.md "wikilink")。其兄[弗朗茨·斐迪南大公和妻子](../Page/弗朗茨·斐迪南大公.md "wikilink")[霍恩貝格女公爵蘇菲为](../Page/霍恩貝格女公爵蘇菲.md "wikilink")[贵庶通婚](../Page/贵庶通婚.md "wikilink")，子女失去继承权。他放弃继承权，让位于其长子[卡爾](../Page/卡爾一世_\(奧匈帝國\).md "wikilink")。

1906年因染[梅毒逝世](../Page/梅毒.md "wikilink")，年仅41岁。

## 家庭成员

  - 父亲:[卡爾·路德維希大公](../Page/卡爾·路德維希大公.md "wikilink")
  - 母亲: [两西西里王国的玛丽亚](../Page/两西西里王国.md "wikilink")·安农齐亚塔公主公主
  - 妻子:[萨克森王国的约瑟珐公主](../Page/萨克森王国.md "wikilink")
  - 子女:

<!-- end list -->

1.  [卡爾一世](../Page/卡爾一世_\(奧匈帝國\).md "wikilink")(1887年8月17日—1922年4月1日)
2.  马克西米利安·欧根大公(1895年4月13日-19.1.1952年1月19日)

[Category:奥地利大公](../Category/奥地利大公.md "wikilink")
[Category:奥匈帝国皇族](../Category/奥匈帝国皇族.md "wikilink")
[Category:施泰尔马克州人](../Category/施泰尔马克州人.md "wikilink")
[Category:死于梅毒的人](../Category/死于梅毒的人.md "wikilink")