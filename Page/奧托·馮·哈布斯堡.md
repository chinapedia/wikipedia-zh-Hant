**奧托·馮·哈布斯堡**(**Otto von
Habsburg**，)，全名是**弗蘭茨·約瑟夫·<u>奧托</u>·羅伯特·瑪麗亞·安東·卡爾·馬克斯·海因里希·西克斯圖斯·克薩韋爾·費利克斯·雷納圖斯·路德維希·加埃坦·皮烏斯·伊格納蒂烏斯·馮·哈布斯堡-洛林**（****），在世時是[哈布斯堡家族的大家長](../Page/哈布斯堡家族.md "wikilink")，[奧匈帝國末代皇帝及](../Page/奧匈帝國.md "wikilink")[匈牙利王國末代國王](../Page/匈牙利王國.md "wikilink")[卡爾一世/四世的長子](../Page/卡爾一世_\(奧匈帝國\).md "wikilink")，為[奧匈帝國末代](../Page/奧匈帝國.md "wikilink")[王儲](../Page/王儲.md "wikilink")。

## 生平

1916年父親卡爾一世即位，他隨即成為[皇儲](../Page/皇儲.md "wikilink")，為奧匈帝國最後一位[皇儲](../Page/皇儲.md "wikilink")；1918年奧匈帝國瓦解，隨父流亡海外。1922年父親在[葡屬](../Page/葡萄牙.md "wikilink")[馬德拉群島逝世](../Page/馬德拉群島.md "wikilink")，他以未滿10歲稚齡成為哈布斯堡家族族長與奧地利皇位繼承人。1961年他宣布放棄一切尋求復辟的行動，並在1966年回到奧地利。他曾於1979年—1999年間任[歐洲議會的議員](../Page/歐洲議會.md "wikilink")，代表[德國](../Page/德國.md "wikilink")[巴伐利亞的](../Page/巴伐利亞.md "wikilink")[拜恩基督教社會聯盟](../Page/拜恩基督教社會聯盟.md "wikilink")。他亦曾是[國際泛歐聯盟的總裁及](../Page/國際泛歐聯盟.md "wikilink")[馬耳他騎士團的成員](../Page/馬耳他騎士團.md "wikilink")。晚年長期在[德國南部的](../Page/德國.md "wikilink")[巴伐利亞州史坦恩堡湖](../Page/巴伐利亞州.md "wikilink")（Lake
Starnberg）畔的Pöcking居住\[1\]\[2\]，並同時享有[奧地利](../Page/奧地利.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[德國及](../Page/德國.md "wikilink")[克羅地亞的公民身份](../Page/克羅地亞.md "wikilink")。

2011年7月4日，奧托·馮·哈布斯堡於德国的家中逝世\[3\]，享年98歲。同年7月16日葬在位於[奧地利](../Page/奧地利.md "wikilink")[維也納的家族陵墓](../Page/維也納.md "wikilink")，並由維也納總主教熊伯恩樞機（Christoph
Schoenborn）在聖史蒂芬（St.
Stephen）主教座堂主持殯葬彌撒，而其心臟依家族傳統在隔日的家族喪禮中下葬於[匈牙利帕農哈馬](../Page/匈牙利.md "wikilink")（Pannonhalma）本篤會修道院。\[4\]

由於他的特殊身份，各國對他的稱呼亦有所不同。例如奧地利他被稱為**奧托·哈布斯堡-洛林**、**奧地利大公奧托**或**奧地利皇儲**，而在匈牙利，他只簡單被叫作**哈布斯堡·奧托**（[匈牙利人習慣把姓氏放在前](../Page/匈牙利人名.md "wikilink")，與東亞一樣）。

## 家庭

[Coronation_Hungary_1916.jpeg](https://zh.wikipedia.org/wiki/File:Coronation_Hungary_1916.jpeg "fig:Coronation_Hungary_1916.jpeg")的加冕典禮。\]\]
1951年5月10日奥托和[萨克森-迈宁根公国的](../Page/萨克森-迈宁根.md "wikilink")於[法國](../Page/法國.md "wikilink")[南錫结婚](../Page/南錫.md "wikilink")，共育有2男5女。

## 參見

  - [泛欧野餐](../Page/泛欧野餐.md "wikilink")

## 參考資料

## 外部連結

  - [奧托·馮·哈布斯堡官方網站](http://www.ottovonhabsburg.org/)

{{-}}     |-

[Category:奥匈帝国皇储](../Category/奥匈帝国皇储.md "wikilink")
[Category:奥地利君主儿子](../Category/奥地利君主儿子.md "wikilink")
[Category:德国籍欧洲议员](../Category/德国籍欧洲议员.md "wikilink")
[Category:奧地利天主教徒](../Category/奧地利天主教徒.md "wikilink")
[Category:德國天主教徒](../Category/德國天主教徒.md "wikilink")
[Category:奧地利反共主義者](../Category/奧地利反共主義者.md "wikilink")
[Category:天主教鲁汶大学校友(1970年前)](../Category/天主教鲁汶大学校友\(1970年前\).md "wikilink")
[Category:馬爾他騎士團人物](../Category/馬爾他騎士團人物.md "wikilink")
[Category:下奥地利州人](../Category/下奥地利州人.md "wikilink")
[Category:哈布斯堡-洛林王朝](../Category/哈布斯堡-洛林王朝.md "wikilink")

1.  [Official Website of Otto von
    Habsburg](http://www.ottovonhabsburg.org/content.asp?lang=en)

2.  [Eldest son of last Austrian emperor dies
    at 98](http://uk.reuters.com/article/2011/07/04/us-austria-habsburg-idUKTRE7630YX20110704)

3.
4.  [奧匈帝國末代皇儲榮葬維也納](http://news.rti.org.tw/index_newsContent.aspx?nid=307854&id=5&id2=2)，[中央社](../Page/中央社.md "wikilink")，2011年7月17日