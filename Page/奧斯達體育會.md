**奧斯達足球會**（），是一支位於[韋克舍的](../Page/韋克舍.md "wikilink")[瑞典足球球會](../Page/瑞典.md "wikilink")，成立於1930年4月20日（當時叫Östers
Fotbollförening），球會現正於[瑞典足球中級聯賽角逐](../Page/瑞典足球中級聯賽.md "wikilink")。奧斯達是目前唯一一支第一次亮相[瑞典足球超級聯賽就獲得冠軍的球隊](../Page/瑞典足球超級聯賽.md "wikilink")，當年是1968年。

## 著名球員

  - [Björn Andersson](../Page/Björn_Andersson.md "wikilink")
  - [Inge Ejderstedt](../Page/Inge_Ejderstedt.md "wikilink")
  - [Anders Linderoth](../Page/Anders_Linderoth.md "wikilink")
  - [Jan Mattsson](../Page/Jan_Mattsson.md "wikilink")
  - [Thomas Ravelli](../Page/Thomas_Ravelli.md "wikilink")
  - [Pia Sundhage](../Page/Pia_Sundhage.md "wikilink") (女子隊)
  - [Stig Svensson](../Page/Stig_Svensson.md "wikilink")
    (他出名主要是因為他在1946年─1989年當上球會主席)
  - [Tommy Svensson](../Page/Tommy_Svensson.md "wikilink")
  - [Mark Watson](../Page/Mark_Watson.md "wikilink")
  - [Peter Wibrån](../Page/Peter_Wibrån.md "wikilink")

## 成就

  - **[瑞典冠軍](../Page/瑞典.md "wikilink"):**
      - **冠軍(4):** 1968, 1978, 1980, 1981

<!-- end list -->

  - **[瑞典足球超級聯賽](../Page/瑞典足球超級聯賽.md "wikilink"):**
      - **冠軍(4):** 1968, 1978, 1980, 1981
      - **亞軍(3):** 1973, 1975, 1992

<!-- end list -->

  - **[瑞典足球超級聯賽季後賽](../Page/瑞典足球超級聯賽.md "wikilink"):**
      - **亞軍(1):** 1983

## 外部連結

  - [Östers IF](http://www.osterfotboll.com/) - 官方網站
  - [East Front](http://www.eastfront.net/) - 官方球迷會網站
  - [Smålands Stolthet](http://www.svenskafans.com/fotboll/oster/) -
    球迷網站

[Category:瑞典足球俱樂部](../Category/瑞典足球俱樂部.md "wikilink")