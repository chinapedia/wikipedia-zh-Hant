**奧海鎮**（[英文](../Page/英文.md "wikilink")：**Ojai**，其印第安本意為「月亮谷」），或譯**歐亥**，是[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[文图拉县一個人口只有八千左右的小鎮](../Page/文图拉县_\(加利福尼亚州\).md "wikilink")，她在[洛杉磯以北](../Page/洛杉磯.md "wikilink")110公里處，這個小鎮曾經在無數[西部電影中出現](../Page/西部電影.md "wikilink")，所以電影觀眾對於這裡是不會感到陌生的。

## 奧海景觀

  - [西班牙街](../Page/奧海西班牙街.md "wikilink")；
  - [奧海老客棧](../Page/奧海老客棧.md "wikilink")，迄今已經有一百三十年。

## 奧海名人

  - [印度](../Page/印度.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[克里希那穆提](../Page/克里希那穆提.md "wikilink")1939年二戰爆發時隱居於此長達8年，並達到完全開悟境界。其後，晚年在此安度，他於1986年逝世於小鎮。
  - [霍華德·休斯](../Page/霍華德·休斯.md "wikilink") 曾就讀奧海鎮的Thacher School
  - [安東尼·霍普金斯](../Page/安東尼·霍普金斯.md "wikilink")
  - [拜倫·凱蒂](https://en.wikipedia.org/wiki/Byron_Katie)被《時代雜誌》讚譽為「21世紀的靈性革新者」，美國最著名的心靈導師之一與心靈書籍暢銷作家，「轉念作業(The
    Work)」創始人。巧合的是，她剛好在克里希那穆提逝世的1986年2月覺醒於真相。

## 外部連結

  - [奧海鎮官府](http://www.ci.ojai.ca.us/)
  - [奧海景觀（一）](http://ojaiphoto.smugmug.com/gallery/2357742#138228863)
  - [奧海景觀（二）](http://www.magney.org/photofiles/Ojai-UrbanPhotos1.htm)
  - [奧海公園景觀](http://www.magney.org/photofiles/Ojai-CluffVistaParkPhotos1.htm)
  - [奧海鎮電影節](http://www.ojaifilmfestival.com/)
  - [奧海鎮詩歌節](http://www.ojaipoetryfestival.com/)
  - [奧海滾石音樂節](http://www.howardpresents.com/)

[Category:文图拉县城市](../Category/文图拉县城市.md "wikilink")
[Category:1921年加利福尼亞州建立](../Category/1921年加利福尼亞州建立.md "wikilink")
[Category:1921年建立的聚居地](../Category/1921年建立的聚居地.md "wikilink")