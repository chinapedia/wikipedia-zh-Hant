**奮進號**（Endeavour，又譯為**努力號**）可能意指下列事物：

  - 船隻
      - [英國皇家海軍](../Page/英國皇家海軍.md "wikilink")
          - [奮進號砲艦](../Page/奮進號砲艦.md "wikilink")（HMS
            Endeavour）：一艘曾在1652年至1656年間服役的36門砲艦。
          - [奮進號臼砲艦](../Page/奮進號臼砲艦.md "wikilink")（HMS
            Endeavour）：一艘曾於1694年至1696年間服役的4門[臼砲艦](../Page/臼砲艦.md "wikilink")（bomb
            vessel）。
          - [奮進號火船](../Page/奮進號火船.md "wikilink")（HMS
            Endeavour）：一艘曾於1694年至1696年間服役的[火船](../Page/火船.md "wikilink")。
          - [奮進號沿岸帆船](../Page/奮進號沿岸帆船.md "wikilink")（HMS
            Endeavour）：一艘曾於1694年至1705年間服役的[沿岸帆船](../Page/沿岸帆船.md "wikilink")（hoy）。
          - [奮進號軍需船](../Page/奮進號軍需船.md "wikilink")（HMS Endeavour
            Transport）：一艘曾於1708年至1713年間服役的[軍需船](../Page/軍需船.md "wikilink")（storeship）。
          - [奮進號單獨縱帆船](../Page/奮進號單獨縱帆船.md "wikilink")（HMS
            Endeavour）：一艘曾於1763年至1771年間服役的[獨桅縱帆船](../Page/獨桅縱帆船.md "wikilink")（cutter）。
          - [奮進號單桅縱帆船](../Page/奮進號單桅縱帆船.md "wikilink")（HMS
            Endeavour）：一艘曾於1763年至1780年間服役的14門砲[單桅縱帆砲艦](../Page/單桅縱帆船.md "wikilink")，在1780年的一場[颶風中於](../Page/颶風.md "wikilink")[牙買加外海進水沈沒](../Page/牙買加.md "wikilink")。
          - [奮進號三桅帆船](../Page/奮進號三桅帆船.md "wikilink")（HM Bark
            Endeavour）：知名探險家[詹姆斯·庫克船長在](../Page/詹姆斯·庫克.md "wikilink")1768年至1771年之間探索[太平洋時的座艦](../Page/太平洋.md "wikilink")，也是一系列以「奮進」為名的英國船隻中最廣為人知者。
          - [奮進號雙桅縱帆船
            (1775年)](../Page/奮進號雙桅縱帆船_\(1775年\).md "wikilink")（HMS
            Endeavour）：一艘於1775年至1782年之間服役的[雙桅縱帆船](../Page/雙桅縱帆船.md "wikilink")（schooner）。
          - [奮進號雙桅縱帆船
            (1782年)](../Page/奮進號雙桅縱帆船_\(1782年\).md "wikilink")（HMS
            Endeavour）：一艘於1782年時服役的雙桅縱帆船。
          - [奮進號測量船](../Page/奮進號測量船.md "wikilink")（HMS
            Endeavour）：一艘於1912年時服役的[測量船](../Page/測量船.md "wikilink")（survey
            ship），曾在1940年時轉作支援母船用途，並在1946年除役出售。
      - [紐西蘭皇家海軍](../Page/紐西蘭皇家海軍.md "wikilink")
          - [奮進號支援船](../Page/奮進號支援船.md "wikilink")（HMNZS
            Endeavor）：一艘曾在1956年至1962年間支援[南極探索任務的改裝](../Page/南極洲.md "wikilink")[設網船](../Page/設網船.md "wikilink")（net
            tender）。
          - 奮進號運油艦（HMNZS Endeavour
            A814）：是一艘曾在1962年至1971年間服役的南極探索支援艦，其前身為美國海軍納美卡根號運油艦（USS
            Namakagon
            AOG-53），在結束了於紐西蘭皇家海軍的軍事援助計畫之後，又出借給[中華民國海軍](../Page/中華民國海軍.md "wikilink")，改名成為[龍泉艦](../Page/龍泉號運油艦.md "wikilink")（ROCS
            Lung Chuan,
            AOG-515）。該艦自[美國海軍船藉](../Page/美國海軍船藉.md "wikilink")（NVR）中註銷之後賣給中華民國，並在2005年時除役。
          - [奮進號運油艦 (A11)](../Page/奮進號運油艦_\(A11\).md "wikilink")（HMNZS
            Endeavour A11）：一艘1988年時服役的艦隊運油艦。
      - 虛構船隻：
          - 在2007年上映的美國電影《[神鬼奇航3：世界的盡頭](../Page/神鬼奇航3：世界的盡頭.md "wikilink")》中，曾出現過一艘杜撰的三桅縱帆戰艦「奮進號」（HMS
            Endeavour），是劇中反派角色、[不列顛東印度公司貝克特爵士](../Page/不列顛東印度公司.md "wikilink")（Lord
            Cutler Beckett）的旗艦。
  - 其他
      - [奮進號太空梭](../Page/奮進號太空梭.md "wikilink")（STS Endeavour,
        OV-105）：是一架於1992年至2011年之間服役的美國[太空梭](../Page/太空梭.md "wikilink")。其命名源自庫克船長的旗艦。