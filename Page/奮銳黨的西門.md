**宗徒西满**（），在《[新約](../Page/新約.md "wikilink")》中經常被稱為**[热诚者西滿](../Page/奮銳黨.md "wikilink")**（[新教翻译为](../Page/新教.md "wikilink")**[奮銳黨的西门](../Page/奮銳黨.md "wikilink")**；；；）\[1\]，也被稱為**客纳罕人西满**（**迦南人西门**；；），[耶穌的](../Page/耶穌.md "wikilink")[十二门徒之一](../Page/十二门徒.md "wikilink")，[基督教圣人](../Page/基督教圣人.md "wikilink")，是[十二门徒中留下資料最少的一個](../Page/十二门徒.md "wikilink")。

在教會傳統中，西满往往和宗徒[猶达連在一起](../Page/猶達·達陡.md "wikilink")，他們的[瞻礼日同在](../Page/罗马天主教圣人历.md "wikilink")10月28日。\[2\]傳說指出，他先去[埃及傳福音](../Page/埃及.md "wikilink")，然後在[波斯和](../Page/波斯.md "wikilink")[亞美尼亞和猶达會合](../Page/亞美尼亞.md "wikilink")，並一同在此殉道，可見於中世紀著作[《黃金傳說》中](../Page/《黃金傳說》.md "wikilink")。

## 身份推测

[SimonTheZealotWithSaw.JPG](https://zh.wikipedia.org/wiki/File:SimonTheZealotWithSaw.JPG "fig:SimonTheZealotWithSaw.JPG")

西满（；）是一個常見的希伯來文名字。在[对观福音与](../Page/对观福音.md "wikilink")[宗徒大事录中](../Page/宗徒大事录.md "wikilink")，西满的名字每一次都出现在[十二门徒的名单中](../Page/十二门徒.md "wikilink")，对其他关于他的细节几乎没有描述。極少[托名文學用他的名字](../Page/托名文學.md "wikilink")，而[哲罗姆在](../Page/哲罗姆.md "wikilink")*De
viris illustribus*也沒記載他的作品。

為了和[西满伯多禄分別](../Page/西門彼得.md "wikilink")，這一位西满特別被冠上[热诚者的稱呼](../Page/奮銳黨.md "wikilink")\[3\]。学者[羅勃特·伊斯曼](../Page/羅勃特·伊斯曼.md "wikilink")（Robert
Eisenman）指出，根據當時《[塔木德](../Page/塔木德.md "wikilink")》記載，[热诚者](../Page/奮銳黨.md "wikilink")「並不真是一個組織，毋寧說是一群想向統轄天主的應許地[巴勒斯坦的](../Page/巴勒斯坦.md "wikilink")[羅馬帝國](../Page/羅馬帝國.md "wikilink")，進行復仇與革命行動之激進份子。」

在《[玛窦福音](../Page/玛窦福音.md "wikilink")》與《[马尔谷福音](../Page/马尔谷福音.md "wikilink")》中，他被稱為，或，也就是[热诚者](../Page/奮銳黨.md "wikilink")（Zealo，Zelotes）的意思。在[希伯來文中](../Page/希伯來文.md "wikilink")，字尾的
qana，意思是热诚者（The Zealous）。

因為宗徒[猶达](../Page/猶达.md "wikilink")，有可能即是[耶穌的兄弟猶达](../Page/猶大_\(耶穌的兄弟\).md "wikilink")。西满的名稱又經常與宗徒犹达連在一起，因此有學者認為，宗徒西满就是[耶穌的兄弟西满](../Page/西門_\(耶穌的兄弟\).md "wikilink")。

[东正教会传统上认为](../Page/东正教会.md "wikilink")，[基督和他的门徒所参加的](../Page/基督.md "wikilink")[加纳婚宴就是西满的婚宴](../Page/加纳婚宴.md "wikilink")，在那次婚宴上基督将水变作了美酒。西满之所以取得了“[热诚者](../Page/奮銳黨.md "wikilink")”的称号，是因为当他看见了这[神迹之后](../Page/神迹.md "wikilink")，抛下了自己的家庭、父母与新婚妻子去追随基督。同时，也提到在[五旬节之后](../Page/五旬节.md "wikilink")，西满去往北非[毛里塔尼亚传教](../Page/毛里塔尼亚.md "wikilink")。

《[天主教百科全書](../Page/天主教百科全書.md "wikilink")》相信，热诚者西满與[耶路撒冷的西满是同一個人](../Page/耶路撒冷的西門.md "wikilink")，他是[耶穌的堂兄弟](../Page/耶穌.md "wikilink")，或是由[若瑟的前妻所生](../Page/若瑟.md "wikilink")，也就是[耶穌的兄弟西满](../Page/西門_\(耶穌的兄弟\).md "wikilink")。\[4\]

在教會传统上，[公义者雅各伯的繼承者](../Page/公義者雅各.md "wikilink")，耶路撒冷的第二任[主教](../Page/主教.md "wikilink")[耶路撒冷的西满](../Page/耶路撒冷的西門.md "wikilink")，也有可能就是宗徒西满。\[5\]\[6\]

## 教会传统与逝世地点

[塞维利亚的圣依西多禄在他的著作](../Page/圣依西多禄.md "wikilink")*De Vita et
Morte*中收集了关于圣西满的各种传闻，之后完整的传说则出现于*[Legenda
Aurea](../Page/Golden_Legend.md "wikilink")* (*约于* 1260)。\[7\]

教会传统上，西满经常与[宗徒](../Page/宗徒.md "wikilink")[犹达联系在一起](../Page/犹达.md "wikilink")，在[西方基督教中](../Page/西方基督教.md "wikilink")，他们的庆日都是10月28日。最广为人知的故事为，西满在往[埃及传教之后](../Page/埃及.md "wikilink")，在[帕提亚帝国境内会合](../Page/帕提亚帝国.md "wikilink")，再一起往[亚美尼亚王国](../Page/亚美尼亚王国.md "wikilink")，或者往[黎巴嫩的](../Page/黎巴嫩.md "wikilink")[贝鲁特](../Page/贝鲁特.md "wikilink")，在那里他们于65年一同殉道。此故事同样见于Golden
Legend。西满可能作为[耶路撒冷主教被判钉十字架而死](../Page/耶路撒冷牧首.md "wikilink")。

另一说指出他往[中东与埃及传教](../Page/中东.md "wikilink")。[埃塞俄比亚的基督徒们宣称他在](../Page/埃塞俄比亚.md "wikilink")[撒玛利亚被钉十字架](../Page/撒玛利亚.md "wikilink")，而[尤斯图斯·利普修斯则写道他在](../Page/尤斯图斯·利普修斯.md "wikilink")[波斯的素尼亚被人锯成两半而死](../Page/波斯.md "wikilink")。\[8\]然而，写道西满在[高加索伊比利亚王国的Weriosphora殉道](../Page/高加索伊比利亚王国.md "wikilink")。\[9\]还有的说法是，他在[美索不达米亚城市](../Page/美索不达米亚.md "wikilink")[埃德萨安然离世](../Page/埃德萨.md "wikilink")。\[10\]
此外，还有提到西满去往[罗马帝国的](../Page/罗马帝国.md "wikilink")[不列颠尼亚省](../Page/不列颠尼亚_\(罗马行省\).md "wikilink")（可能是[格拉斯顿伯里](../Page/格拉斯顿伯里.md "wikilink")），并在[卡斯托殉道](../Page/卡斯托.md "wikilink")（今位于[林肯郡](../Page/林肯郡.md "wikilink")）。

在藝術中他的[象征是以鋸來辨認](../Page/圣人符号学.md "wikilink")，因為傳統上認為他是被鋸子鋸死。

## 参考文献

## 外部連結

  - [All appearances of "Simon" in the New
    Testament](http://www.virtualology.com/virtualmuseumofhistory/hallofspirituality/stsimontheapostle.com/)

  - [*Legenda
    Aurea*:](https://web.archive.org/web/20100312052835/http://www.catholic-forum.com/saints/golden296.htm)
    Lives of Saints Simon and Jude

  - [Catholic Encyclopedia article on St. Simon the
    Apostle](http://www.newadvent.org/cathen/13796b.htm)

  - *[Ὁ Ἅγιος Σίμων ὁ Ἀπόστολος ὁ
    Ζηλωτής](http://www.synaxarion.gr/gr/sid/3087/sxsaintinfo.aspx).*
    10 Μαΐου. ΜΕΓΑΣ ΣΥΝΑΞΑΡΙΣΤΗΣ.

  - [Simon the Zealot (novel) by Alfred
    Brenner](http://www.scribd.com/doc/45050784/Simon-the-Zealot-Pt1)

{{-}}

[Category:耶稣的十二使徒](../Category/耶稣的十二使徒.md "wikilink")
[Category:1世纪基督教殉道圣人](../Category/1世纪基督教殉道圣人.md "wikilink")
[Category:新约圣经中的人](../Category/新约圣经中的人.md "wikilink")
[Category:耶稣的家人](../Category/耶稣的家人.md "wikilink")

1.  和

2.  [CALENDARIUM ROMANUM
    GENERALE](http://www.binetti.ru/collectio/liturgia/missale_files/crg.htm)

3.  ；

4.

5.  [St. Simon the Apostle](http://www.newadvent.org/cathen/13796b.htm),
    from the *[Catholic
    Encyclopedia](../Page/Catholic_Encyclopedia.md "wikilink")*

6.  Appendix to the Works of Hippolytus 49.11

7.

8.
9.
10.