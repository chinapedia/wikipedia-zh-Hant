**不信的時候～woman
wars～**（原名：）是由[有吉佐和子的原作小説](../Page/有吉佐和子.md "wikilink")『不信的時候』改編的[電視劇](../Page/電視劇.md "wikilink")。[富士電視台](../Page/富士電視台.md "wikilink")[系列從](../Page/系列.md "wikilink")2006年7月6日至9月21日、富士電視台于每週四的22:00～22:54期間播放。共12集(第一集增加15分鐘，23:09結束)。也曾在[美國](../Page/美國.md "wikilink")[紐約的Channel](../Page/紐約.md "wikilink")63播出過。台灣[緯來日本臺於](../Page/緯來日本臺.md "wikilink")2007年6月20日首播。香港[無線劇集台於](../Page/無線劇集台.md "wikilink")2007年7月8日首播。

## 簡介

原作是刊載於1967年「[日本經濟新聞](../Page/日本經濟新聞.md "wikilink")」的小說 -
「[不信的時候](../Page/不信的時候.md "wikilink")」。這套「不信的時候～Woman
Wars～」在以往亦曾經被拍成電影和電視劇。 故事内容是關於一個同時有妻子和情婦的男人，周旋於兩個女人之間，苦惱非常。

## 演員

  - 淺井道子：[米倉涼子](../Page/米倉涼子.md "wikilink")（香港配音：[陳凱婷](../Page/陳凱婷.md "wikilink")）
  - 野上MACHI子（本名野上路子）：[松下由樹](../Page/松下由樹.md "wikilink")（香港配音：[陸惠玲](../Page/陸惠玲.md "wikilink")）
  - 沖中和子：[杉田薰](../Page/杉田薰.md "wikilink")（香港配音：[雷碧娜](../Page/雷碧娜.md "wikilink")）
  - 小柳新吾：[石田純一](../Page/石田純一.md "wikilink")（香港配音：[李錦綸](../Page/李錦綸.md "wikilink")）
  - 近藤慶：[小泉孝太郎](../Page/小泉孝太郎.md "wikilink")（香港配音：[梁偉德](../Page/梁偉德.md "wikilink")）
  - 伊藤MAYUMI（本名神山雪子）：[福田沙紀](../Page/福田沙紀.md "wikilink")（香港配音：[林雅婷](../Page/林雅婷.md "wikilink")）
  - 野上俊也：[和田正人](../Page/和田正人.md "wikilink")（香港配音：[曹啟謙](../Page/曹啟謙.md "wikilink")）
  - 椎名千明：[寶積有香](../Page/寶積有香.md "wikilink")
  - 小柳MIDORI：[秋山菜津子](../Page/秋山菜津子.md "wikilink")
  - 淺井朋子：[江波杏子](../Page/江波杏子.md "wikilink")
  - 淺井義雄：[石黑賢](../Page/石黑賢.md "wikilink")（香港配音：[潘文柏](../Page/潘文柏.md "wikilink")）

## 客串演員

第1話

  - [遠藤玲子](../Page/遠藤玲子.md "wikilink")（富士電視台主播）

第1話、第3話、第11話

  - [高岡早紀](../Page/高岡早紀.md "wikilink")（義雄的舊情婦・大澤千鶴子）（香港配音：[曾秀清](../Page/曾秀清.md "wikilink")）

第4話

  - [鷲尾真知子](../Page/鷲尾真知子.md "wikilink")（MACHI子的叔母）

第6話、第11話

  - [平泉成](../Page/平泉成.md "wikilink")（島原常務）

第7話、第8話

  - [古舘玖優](../Page/古舘玖優.md "wikilink")・[古舘優空](../Page/古舘優空.md "wikilink")（野上法子）

第8話

  - [淺野和之](../Page/淺野和之.md "wikilink")（MAYUMI的父親・神山聡一郎）
  - [木村多江](../Page/木村多江.md "wikilink")（MAYUMI的母親<後妻>・神山沙織）
  - [古賀湧志](../Page/古賀湧志.md "wikilink")（HACCHI）

## 歌曲

  - 主題曲：[Ann Lewis](../Page/Ann_Lewis.md "wikilink")「あゝ無情」
  - 劇中插曲：[福田沙紀](../Page/福田沙紀.md "wikilink")「[Good Bye My
    Love](../Page/Good_Bye_My_Love.md "wikilink")」

## 工作人員

  - 原作：[有吉佐和子](../Page/有吉佐和子.md "wikilink")
  - 劇本：[小野沢美暁](../Page/小野沢美暁.md "wikilink")、[栗原美和子](../Page/栗原美和子.md "wikilink")
  - 音樂：[石田勝範](../Page/石田勝範.md "wikilink")
  - 製作：[栗原美和子](../Page/栗原美和子.md "wikilink")、[林徹](../Page/林徹.md "wikilink")
  - 導演：[林徹](../Page/林徹.md "wikilink")、[大木綾子](../Page/大木綾子.md "wikilink")、[谷村政樹](../Page/谷村政樹.md "wikilink")
  - 技術製作：小椋真人
  - 技術協力：nac Image Technology Inc.
  - 制作：Fuji Television Drama Production Center

## 播放日期、副題、收視率

| 集數    | 播放日期       | 副題                 | 收視率\[1\]                  |
| ----- | ---------- | ------------------ | ------------------------- |
| 1     | 2006年7月6日  | 不妊症…情事を覗き見しませんか    | 14.0%                     |
| 2     | 2006年7月13日 | 妻と愛人の妊娠戦争…         | 13.5%                     |
| 3     | 2006年7月20日 | 妻と夫のダブル不倫          | 12.5%                     |
| 4     | 2006年7月27日 | 妻の謎そして反撃\!         | 12.5%                     |
| 5     | 2006年8月3日  | 大波瀾愛人出産\!妻妊娠\!     | 11.5%                     |
| 6     | 2006年8月10日 | 愛が狂気に変わる瞬間         | <font color="green">10.3% |
| 7     | 2006年8月17日 | 危険な情事…愛人の逆襲        | 11.5%                     |
| 8     | 2006年8月24日 | 二重生活の落とし穴…天罰が下る瞬間  | 12.9%                     |
| 9     | 2006年8月31日 | 遂に正面衝突…妻が愛人に宣戦布告\! | 12.2%                     |
| 10    | 2006年9月7日  | 涙の一騎打ち…それぞれが衝撃の告白  | <font color="red">15.5%   |
| 11    | 2006年9月14日 | 女たちの復讐…究極の制裁       | 13.7%                     |
| 大結局   | 2006年9月21日 | 真相あなたは誰を信じますか…?    | 14.8%                     |
| 平均收視率 | 12.9%      |                    |                           |

## 相關人物

  - [田宮二郎](../Page/田宮二郎.md "wikilink")（演出1968年的電影版。電影『不信的時候』事件）
  - [若尾文子](../Page/若尾文子.md "wikilink")（演出1968年的電影版）
  - [岡田茉莉子](../Page/岡田茉莉子.md "wikilink")（演出1968年的電影版）
  - [加賀まりこ](../Page/加賀まりこ.md "wikilink")（演出1968年的電影版）
  - [岸田今日子](../Page/岸田今日子.md "wikilink")（演出1968年的電影版）

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [女人心機日本官方網站](http://www.fujitv.co.jp/b_hp/fushin/)
  - [女人心機台灣官方網站](http://japan.videoland.com.tw/channel/woman_s/)

## 作品的變遷

[Category:富士電視台週四連續劇](../Category/富士電視台週四連續劇.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:翻拍電視劇](../Category/翻拍電視劇.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:日本小說改編電視劇](../Category/日本小說改編電視劇.md "wikilink")

1.  為日本關東地區．由[Video Research調查](../Page/Video_Research.md "wikilink")。