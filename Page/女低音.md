[Sopran.png](https://zh.wikipedia.org/wiki/File:Sopran.png "fig:Sopran.png")\]\]
[Range_of_alto_voice_marked_on_keyboard.svg](https://zh.wikipedia.org/wiki/File:Range_of_alto_voice_marked_on_keyboard.svg "fig:Range_of_alto_voice_marked_on_keyboard.svg")
[Tenor.png](https://zh.wikipedia.org/wiki/File:Tenor.png "fig:Tenor.png")\]\]
[Range_of_bass_voice_marked_on_keyboard.png](https://zh.wikipedia.org/wiki/File:Range_of_bass_voice_marked_on_keyboard.png "fig:Range_of_bass_voice_marked_on_keyboard.png")\]\]
在歌唱方面，**女低音**（**Contralto**）是女聲中最低的聲部，[音域通常是從中央Ç下的](../Page/音域.md "wikilink")˚F音往上爬升二個[八度](../Page/純八度.md "wikilink")（F3〜F5）[音色相比](../Page/音色.md "wikilink")[女高音較寬厚深沉](../Page/女高音.md "wikilink")，有時候一些音色比較磁性暗啞，而聲音又渾厚的[女中音](../Page/女中音.md "wikilink")，也會被歸類爲女低音。諸如
[Lady gaga](../Page/Lady_gaga.md "wikilink")
，[艾美·懷恩豪斯以及](../Page/艾美·懷恩豪斯.md "wikilink")[愛黛兒等流行歌手](../Page/愛黛兒.md "wikilink")。\[1\]。[柴可夫斯基的](../Page/彼得·伊里奇·柴可夫斯基.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")《[叶甫根尼·奥涅金](../Page/叶甫盖尼·奥涅金_\(歌剧\).md "wikilink")》第一幕第一场的《奥尔伽的[咏叹调](../Page/詠嘆調.md "wikilink")》就是由女低音独唱。另外在一些[乐器名称里带有](../Page/乐器.md "wikilink")“女低音的音域”之含意。

## 著名女低音歌唱家

  - [玛丽安·安德森](../Page/玛丽安·安德森.md "wikilink")／Marian
    Anderson（[美国](../Page/美国.md "wikilink")，1897年—1993年）
  - [凯瑟林·菲瑞儿](../Page/凯瑟林·菲瑞儿.md "wikilink")／Kathleen
    Ferrier（[英国](../Page/英国.md "wikilink")，1912年—1953年）
  - [莉薩·傑勒德](../Page/莉薩·傑勒德.md "wikilink") ／Lisa Gerrard
  - [朱莉娅·卡尔普](../Page/朱莉娅·卡尔普.md "wikilink")／Julia Culp
  - [娜塔莉·史杜兹曼](../Page/娜塔莉·史杜兹曼.md "wikilink")／Nathalie
    Stutzmann（[法国](../Page/法国.md "wikilink")）
  - [汉娜·施瓦茨](../Page/汉娜·施瓦茨.md "wikilink")／Hannah Schwartz
  - [杨建生](../Page/杨建生.md "wikilink")／Jiansheng Yang（德籍华人）;
  - [黑木香保里](../Page/黑木香保里.md "wikilink")／Kuroki Kaori（日本）

## 著名女低音流行歌手

  - [戚𡤕](../Page/戚𡤕.md "wikilink")／Ceca （塞爾維亞）
  - [阿烈山㼀娜·納朵棫嬨](../Page/阿烈山㼀娜·納朵棫嬨.md "wikilink")／Aleksandra Radović
    （塞爾維亞）
  - [艾蓮娜·帕帕茢索](../Page/艾蓮娜·帕帕茢索.md "wikilink")／Elena Paparizou （希臘）
  - [唐妮·布蕾斯頓](../Page/唐妮·布蕾斯頓.md "wikilink")／Toni Braxton （美國）
  - [安娜塔西亞](../Page/安娜塔西亞.md "wikilink")／Anastacia （美國）
  - [徐小鳳](../Page/徐小鳳.md "wikilink") （香港）
  - [梅艷芳](../Page/梅艷芳.md "wikilink") (香港 )

## 含alto的乐器名

  - 中音[长笛](../Page/长笛.md "wikilink")（alto flute）
  - 中音[单簧管](../Page/單簧管.md "wikilink")（alto clarinet）
  - 中音[萨克斯管](../Page/薩克斯風.md "wikilink")（alto saxophone）
  - 中音[长号](../Page/长号.md "wikilink")（alto trombone）
  - [中音号](../Page/中音號.md "wikilink")（alto horn）
  - [中提琴](../Page/中提琴.md "wikilink")（viola）的[法语名](../Page/法语.md "wikilink")
  - 中音[牧童笛](../Page/直笛.md "wikilink") （Alto Recorder）

## 其他聲部

  - [女高音](../Page/女高音.md "wikilink")（Soprano）
  - [女中音](../Page/女中音.md "wikilink")（Mezzo-soprano，又作[次女高音](../Page/次女高音.md "wikilink")）
  - [男高音](../Page/男高音.md "wikilink")（Tenor）
  - [男中音](../Page/男中音.md "wikilink")（Baritone）
  - [男低音](../Page/男低音.md "wikilink")（Bass）

## 外部連結

[女低音](../Category/女低音.md "wikilink")
[Category:聲樂](../Category/聲樂.md "wikilink")

1.  [女低音名單](https://en.wikipedia.org/wiki/List_of_contraltos_in_non-classical_music)