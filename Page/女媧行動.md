《**女媧行動**》（），是[香港](../Page/香港.md "wikilink")[麗的電視在](../Page/麗的電視.md "wikilink")1981年拍攝的一部[電視劇](../Page/電視劇.md "wikilink")，由[朱江及](../Page/朱江.md "wikilink")[張瑛主演](../Page/張瑛.md "wikilink")，共10集。首播於1981年4月27日。此劇曾於1994年在[亞洲電視重播](../Page/亞洲電視.md "wikilink")，於2009年1月6日起[16:9重播](../Page/16:9.md "wikilink")。第二次重播前亚视曾多次在其官网上宣布会在2008年间重播该剧，但最终都取消。

這電視劇以政治阴谋暗杀为主题，即使在廿一世紀的今日來看，亦是很大膽。由于经费有限，拍摄该题材也很困难，这套剧只能够拍摄十集，不同于一般最少20集的香港电视剧。

## 故事大纲

這套電視劇在當時來說是一個非常嶄新的創作，原因如下：

1.  **這套電視劇以“未來”為題材，以五年後（1986年）的香港為故事背景；**
2.  *'
    故事大膽的推測在當時以外籍人士為主的香港政府，已有一位華籍的助理[保安司王大衛](../Page/香港保安局.md "wikilink")(David
    Wong，[張瑛飾](../Page/張瑛.md "wikilink"))；*'
3.  **這位助理保安司其實是某國間諜組織的卧底（其上司为“卡刚诺维奇將軍”），為求達到目的，殺人不擇手段。**

David
Wong曾在香港參與策劃[雙十暴動](../Page/雙十暴動.md "wikilink")、[廣東信託銀行](../Page/廣東信託銀行.md "wikilink")[擠提事件](../Page/擠提.md "wikilink")、[天星小輪加價事件](../Page/天星小輪加價事件.md "wikilink")、左派[六七暴動](../Page/六七暴動.md "wikilink")、[1973年香港股災等](../Page/1973年香港股災.md "wikilink")，以破壞香港繁榮和治安。由於故事中的殺手王英，之前為[國軍高級將領](../Page/中華民國國軍.md "wikilink")，為[戴笠的下屬](../Page/戴笠.md "wikilink")，後來叛逃，與David
Wong為數十年朋友，似乎暗示David Wong的主子為當時中共的共產黨政府，“卡刚诺维奇將軍”可能是隱喻中共輸出革命，荼毒東南亞各國。

1986年，[香港立法局議員譚勵恩被謀殺](../Page/香港立法局.md "wikilink")，以保全該組織之秘密，嫁禍政府，使社會動亂，影響中國[四化](../Page/四化.md "wikilink")。案發時，攝影師程素琪（[余安安飾](../Page/余安安.md "wikilink")）拍攝到了幾位現場證人的面目表情，而這幾位關鍵證人亦隨之被謀殺。程素琪之男友江柱石（[朱江飾](../Page/朱江.md "wikilink")）身為港督委任的專案調查委員會主席（同時為[律政司署檢控官](../Page/律政司署.md "wikilink")），介入調查這些謀殺案，卻殊不知David
Wong已策劃好“女媧行動”，準備“煉石補青天”……

## 每集開場白

[女媧](../Page/女媧.md "wikilink")，神話傳說，上古嘅女神，亦係[伏羲氏嗰妹](../Page/伏羲氏.md "wikilink")。因為當時，天上有一個漏洞，為咗補救，不惜自焚，煉石補青天。

（電話聲響）

神秘電話中人：「我哋嗰計劃而家洩漏咗出去，而家要補鑊，不擇手段都要，你知嚸做啦。（我们的计划现在泄露了出去，现在不择手段都要补救，你知道该怎么做吧。）」

神秘接電話人：「我知道啦。」

## 演員表

  - [朱江](../Page/朱江.md "wikilink") 飾 江柱石
  - [余安安](../Page/余安安.md "wikilink") 飾 程素琪 (Suki)
  - [梁小龍](../Page/梁小龍.md "wikilink") 飾 梁瑞節 (水唧)
  - [黃植森](../Page/黃植森.md "wikilink") 飾 李啟文 (傻豹)
  - [張瑛](../Page/張瑛.md "wikilink") 飾 王大衛（David Wong）
  - [吳回](../Page/吳回.md "wikilink") 飾 成伯
  - [梁漢威](../Page/梁漢威.md "wikilink") 飾 周卓雄
  - [麥天恩](../Page/麥天恩.md "wikilink") 飾 王瑛
  - [金山](../Page/金山_\(香港演員\).md "wikilink") 飾 麥木麟
  - [莫少聰](../Page/莫少聰.md "wikilink") 飾 明仔
  - [伍國成](../Page/伍國成.md "wikilink") 飾 何子雄
  - [楊家安](../Page/楊家安.md "wikilink") 飾 王永祥
  - [蕭若元](../Page/蕭若元.md "wikilink") 飾 蕭若元
  - [盧雨岐](../Page/盧雨岐.md "wikilink") 飾 歐基
  - [韓江](../Page/韓江.md "wikilink") 飾 陳江
  - [顏國樑](../Page/顏國樑.md "wikilink") 飾 殺手
  - [梁炳坤](../Page/梁炳坤.md "wikilink") 飾 殺手
  - [佘文炳](../Page/佘文炳.md "wikilink") 飾 羅森
  - [司馬華龍](../Page/司馬華龍.md "wikilink") 飾 保安司
  - [孫季卿](../Page/孫季卿.md "wikilink") 飾 按察司
  - [袁一飛](../Page/袁一飛.md "wikilink") 飾 銓敘司
  - [林國明](../Page/林國明.md "wikilink") 飾 地盤工人
  - [Ronie](../Page/Ronie.md "wikilink") 飾 希姆萊
  - [陳惠瑜](../Page/陳惠瑜.md "wikilink") 飾 譚勵恩議員
  - [鄺福生](../Page/鄺福生.md "wikilink") 飾 政務處主任
  - [陳志雄](../Page/陳志雄.md "wikilink") 飾 議員
  - [梁竟遜](../Page/梁竟遜.md "wikilink") 飾 警司
  - [徐新義](../Page/徐新義.md "wikilink") 飾 法官
  - [陳中堅](../Page/陳中堅.md "wikilink") 飾 驅魔人/歐家駒（保安科高級首席行政官）
  - [丁櫻](../Page/丁櫻.md "wikilink") 飾 歐家駒妻
  - [梅愛芳](../Page/梅愛芳.md "wikilink") 飾 美蓮 (舞小姐, 王瑛情婦)
  - [梅艷芳](../Page/梅艷芳.md "wikilink") 飾 小娟 (舞小姐)
  - [黃一飛](../Page/黃一飛.md "wikilink") 飾 楊超
  - [林偉祺](../Page/林偉祺.md "wikilink") 飾 程忠元
  - [利永錫](../Page/利永錫.md "wikilink") 飾 麥七
  - [周星馳](../Page/周星馳.md "wikilink") 飾 議員辦事處職員
  - [蘇民峰](../Page/蘇民峰.md "wikilink") 飾 檢察官

## 評價

[陶傑在](../Page/陶傑.md "wikilink")2005年11月24日的《[光明頂](../Page/光明頂.md "wikilink")》節目提及這套電視劇，指這套劇印證了香港在主權移交之後言論自由收窄了，很多人為免得罪中國政府而自我設限。

[Category:麗的電視劇集](../Category/麗的電視劇集.md "wikilink")
[Category:1981年香港電視劇集](../Category/1981年香港電視劇集.md "wikilink")
[Category:香港懸疑劇](../Category/香港懸疑劇.md "wikilink")
[Category:政治電視劇](../Category/政治電視劇.md "wikilink")
[Category:1986年背景作品](../Category/1986年背景作品.md "wikilink")
[Category:香港背景電視劇](../Category/香港背景電視劇.md "wikilink")
[Category:恐怖活動題材作品](../Category/恐怖活動題材作品.md "wikilink")