**女皇帝**，即[女性的](../Page/女性.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")。汉语中“皇帝”一词无性别之分，男女皆称“皇帝”（如[武则天自封](../Page/武则天.md "wikilink")“圣神皇帝”），因此，中国的“女皇帝”一词及其简称“女皇”、“女帝”都不是称号，而是仅在需要强调其女性身份时才使用的词语，在中国，正式称号男女都是“皇帝”。西方语言有性别之分，“女皇帝”一词是[君主为皇帝的](../Page/君主.md "wikilink")[君主制](../Page/君主制.md "wikilink")[国家名义上的最高统治者为女性时的称号](../Page/国家.md "wikilink")，与“皇帝”称号有别，一般简称**女皇**，在日本也可简称为**女帝**（如英语中“皇帝”为emperor，“女皇帝”为empress）。

开创日本[飞鸟时代的女天皇](../Page/飞鸟时代.md "wikilink")[推古天皇](../Page/推古天皇.md "wikilink")（西元592年[登基](../Page/登基.md "wikilink")）是世界历史上第一位女皇帝（這個存在一定爭議，因为根据确切的记载，[日本自](../Page/日本.md "wikilink")[天武天皇才開始改稱](../Page/天武天皇.md "wikilink")[天皇](../Page/天皇.md "wikilink")，在此之前，其正式稱號應為大王），若不把天武天皇之前的[日本天皇列入計算](../Page/日本天皇.md "wikilink")，则[日本](../Page/日本.md "wikilink")[持統天皇才是世界历史上第一位女皇帝](../Page/持統天皇.md "wikilink")\[1\]。

整个[世界历史上被普遍承认的女皇帝只有](../Page/世界历史.md "wikilink")19位（包括天武天皇之前的两位女天皇），远远少于[男性皇帝](../Page/男性.md "wikilink")，这与[父系社会确立以来全世界普遍存在的](../Page/父系社会.md "wikilink")[男尊女卑思想有直接关系](../Page/男尊女卑.md "wikilink")。

## 名词解释

汉语中“皇帝”一词没有性别区分，男女皆称皇帝，如武则天自封“圣神皇帝”。“女皇帝”、“女帝”、“女皇”一词仅在需要强调其女性身份的时候才使用。正式称号男女都是“皇帝”。

女皇帝一词在[中国](../Page/中国.md "wikilink")、[日本等属于](../Page/日本.md "wikilink")[汉字文化圈的](../Page/汉字文化圈.md "wikilink")[东亚国家并不需要太多解释](../Page/东亚.md "wikilink")，因为其意义很容易明白且不会与其他词混淆；而在有些西方国家则比较模糊，原因是在这些国家的[语言中](../Page/语言.md "wikilink")，女皇帝和-{**[皇后](../Page/皇后.md "wikilink")**}-是同一词，不像在使用[汉字的](../Page/汉字.md "wikilink")[汉语](../Page/汉语.md "wikilink")、[日语等东亚语言中那样区别明显](../Page/日语.md "wikilink")。例如[英语中的empress](../Page/英语.md "wikilink")，既可以解释为“女皇帝”，又可以理解为“-{皇后}-”，为了区别，就在empress后加上regnant（占统治地位的）表示女皇帝（empress
regnant），加上consort（[王配](../Page/君主配偶.md "wikilink")）则表示是男性皇帝的配偶，即皇后（empress
consort）；而[法语则在](../Page/法语.md "wikilink")“皇帝”（empereur）一词前加femme（[女性](../Page/女性.md "wikilink")）以示区别。

严格意义上来说，只有头衔为“[皇帝](../Page/皇帝.md "wikilink")”的女性君主，也就是[帝国的女性君主才能被称作](../Page/帝国.md "wikilink")“女皇帝”，但是[英国的](../Page/英国.md "wikilink")[维多利亚女王则是一个例外](../Page/维多利亚女王.md "wikilink")。[大英帝国的女性君主在严格意义上仅是](../Page/大英帝国.md "wikilink")[女王](../Page/女王.md "wikilink")，只有到了[维多利亚女王时期](../Page/维多利亚女王.md "wikilink")，创立了“[印度女皇](../Page/印度女皇.md "wikilink")”的头衔并在担任英国女王的同时兼任印度女皇；但此时的维多利亚女王仍然不是整个大英帝国的女皇，而只是大英帝国海外[殖民地](../Page/殖民地.md "wikilink")[英属印度的女皇](../Page/英属印度.md "wikilink")，且是唯一的一位“印度女皇”——随着英属印度独立，“印度女皇”的头衔也不存于世了。于是维多利亚女王成为了[英国历史上唯一的拥有](../Page/英国历史.md "wikilink")“女皇”称号的君主。[殖民地時期的](../Page/英属香港.md "wikilink")[香港](../Page/香港.md "wikilink")，因为其[宗主国的关系](../Page/宗主国.md "wikilink")，加上[廣府話](../Page/廣府話.md "wikilink")「王」「皇」同音，多稱作“英女皇”。现在很多媒体（包括官方的）和个人也会把如今的[英国君主](../Page/英国君主.md "wikilink")[伊丽莎白二世称作](../Page/伊丽莎白二世.md "wikilink")“[英女皇](../Page/英女皇.md "wikilink")”，实际是不严谨的，因为一来她本人并没有获此头衔，二来大英帝国已经解体，而如今[英国只是](../Page/英国.md "wikilink")[王国](../Page/王国.md "wikilink")，女性君主只能是“女王”（Queen）而不是“女皇”（Empress）。本条目叙述的是严格意义上的女皇帝。

## 概述

女皇帝或女王的出现一般有两种情况，第一种是前任君主的女性[血亲](../Page/血亲.md "wikilink")（如[公主](../Page/公主.md "wikilink")）继位，通常在这种情况下国家法律规定女性拥有帝位继承权，帝位继承以和平的方式进行，例如[俄罗斯帝国的](../Page/俄罗斯帝国.md "wikilink")[安娜·伊凡诺夫娜女皇和日本的](../Page/安娜·伊凡诺夫娜.md "wikilink")[元正女帝](../Page/元正女帝.md "wikilink")。

第二种情况是前任皇帝的[皇后在夫君死后继承帝位](../Page/皇后.md "wikilink")，出现这种情况很多是由于国家法律没有规定女性的帝位继承权而女性的权力足够大或临时获得统治利益集团的支持，帝位更替往往会伴随着流血或不流血的[政变的发生](../Page/政变.md "wikilink")，例如[中国的](../Page/中国.md "wikilink")[武则天和](../Page/武则天.md "wikilink")[东罗马帝国的](../Page/东罗马帝国.md "wikilink")[伊琳娜女皇](../Page/伊琳娜女皇.md "wikilink")。

第三种情况是第一种情况和第二种情况都符合，即前任皇帝血亲也是皇后（或[妃子](../Page/妃子.md "wikilink")），这种情况在古代盛行[近亲结婚的日本比较常见](../Page/近亲结婚.md "wikilink")，日本前四位[女天皇](../Page/女天皇.md "wikilink")[推古天皇](../Page/推古天皇.md "wikilink")、[皇极天皇](../Page/皇极天皇.md "wikilink")（[齐明天皇](../Page/齐明天皇.md "wikilink")）、[持统天皇和](../Page/持统天皇.md "wikilink")[元明天皇都属于这种情况](../Page/元明天皇.md "wikilink")。

值得一提的是，[日本在](../Page/日本.md "wikilink")592年至770年不到200年的时间里出现了6位、8代[女天皇](../Page/女天皇.md "wikilink")，共掌权90年，其中[推古天皇和](../Page/推古天皇.md "wikilink")[元明天皇分别是](../Page/元明天皇.md "wikilink")[飞鸟时代和](../Page/飞鸟时代.md "wikilink")[奈良时代的开创者](../Page/奈良时代.md "wikilink")。这段时期是[日本历史上独特的](../Page/日本历史.md "wikilink")“**女帝时代**”。日本也是世界历史上最多女性皇帝的国家，共有10代、8位女天皇。

## 历史人物列表

### [中国](../Page/中国.md "wikilink")（共1位）

[A_Tang_Dynasty_Empress_Wu_Zetian.JPG](https://zh.wikipedia.org/wiki/File:A_Tang_Dynasty_Empress_Wu_Zetian.JPG "fig:A_Tang_Dynasty_Empress_Wu_Zetian.JPG")[皇帝](../Page/皇帝.md "wikilink")\]\]

  - **[武则天](../Page/武则天.md "wikilink")**（690年－705年在位，655年－705年掌握實權）：姓名**[武曌](../Page/武曌.md "wikilink")**，[中国历史上唯一普遍承認的女皇帝](../Page/中国历史.md "wikilink")，[唐太宗李世民的](../Page/唐太宗.md "wikilink")[才人](../Page/才人.md "wikilink")、[唐高宗李治的](../Page/唐高宗.md "wikilink")[皇后](../Page/皇后.md "wikilink")，[唐中宗李显和](../Page/唐中宗.md "wikilink")[唐睿宗李旦的生母](../Page/唐睿宗.md "wikilink")，[武周唯一的皇帝](../Page/武周.md "wikilink")。武氏14岁进入唐太宗的[皇宫](../Page/皇宫.md "wikilink")，因美貌和胆识为太宗所赏识。后与太子李治有染；649年，太宗駕崩，高宗即位，以先帝嫔妃身份入感业寺出家；651年奉诏重入後宫；经过幾番宫廷斗争於655年被高宗冊立為皇后。高宗在位中後期因疾不能視朝，乃逐步放權於武-{后}-。武-{后}-创建[垂帘听政制度](../Page/垂帘听政.md "wikilink")，掌握实际权力；683年高宗駕崩後，先後擁立其子中宗和睿宗为帝，以[皇太后之身](../Page/皇太后.md "wikilink")[临朝称制](../Page/临朝称制.md "wikilink")；690年迫睿宗李旦退位，於同年九月自立为皇帝，[尊号](../Page/尊号.md "wikilink")“**圣神皇帝**”，建立[武周政權](../Page/武周.md "wikilink")，史称“[武周革命](../Page/武周革命.md "wikilink")”，武氏因而成為世界歷史上首位非當朝君主血親或皇室成員\[2\]而成為皇帝的女性。705年因“[神龍政变](../Page/神龍政变.md "wikilink")”被迫[内禅于皇太子](../Page/内禅.md "wikilink")[李显](../Page/李显.md "wikilink")，被尊为“**则天大圣皇帝**”，亦成为中国历史上唯一的女性[太上皇](../Page/太上皇.md "wikilink")，退位數月後崩逝，享壽八十一歲；死后[谥号多次改动](../Page/谥号.md "wikilink")，其孙[唐玄宗时終定谥](../Page/唐玄宗.md "wikilink")**则天顺圣皇后**，不再變更。武氏自成为高宗皇后起便逐步掌權，往後在高宗在位晚期掌握實權直到[退位](../Page/退位.md "wikilink")，历时50年；在位期间对外战争多数胜利，巩固并拓展了[中国的版图](../Page/中国.md "wikilink")；开创[殿试和](../Page/殿试.md "wikilink")[武举](../Page/武举.md "wikilink")，发展了[科举制度](../Page/科举制度.md "wikilink")；继续推行[均田制](../Page/均田制.md "wikilink")，促进了[农业生产](../Page/农业.md "wikilink")；国家较[贞观之治时期更有所发展](../Page/贞观之治.md "wikilink")，史称“[貞觀遺風](../Page/貞觀遺風.md "wikilink")”、“小貞觀之治”。

### [日本](../Page/日本.md "wikilink")（共8位、10代）

[日本天皇是](../Page/日本天皇.md "wikilink")[日本](../Page/日本.md "wikilink")[皇帝的称号](../Page/皇帝.md "wikilink")，在日本“帝”等同于“天皇”，如[神武天皇也称](../Page/神武天皇.md "wikilink")“神武帝”；因此日本的[女性天皇也属于女皇帝范畴](../Page/女性天皇.md "wikilink")。而所謂10代8位女天皇，是由於其中有兩位女天皇—[皇極天皇和](../Page/皇極天皇.md "wikilink")[孝謙天皇](../Page/孝謙天皇.md "wikilink")，曾在退位後又因當時政局需要而再次登基。

  - **[推古天皇](../Page/推古天皇.md "wikilink")**（592年－628年在位）：[钦明天皇之女](../Page/钦明天皇.md "wikilink")，[敏达天皇妹](../Page/敏达天皇.md "wikilink")、皇后，[崇峻天皇妹](../Page/崇峻天皇.md "wikilink")，[日本历史上第](../Page/日本历史.md "wikilink")33代[天皇](../Page/日本天皇.md "wikilink")、第一位[女天皇](../Page/女天皇.md "wikilink")；592年其兄崇峻天皇遭[苏我马子杀害后被拥立为天皇](../Page/苏我马子.md "wikilink")；即位初立侄儿[圣德太子为](../Page/圣德太子.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")，推行[改革](../Page/改革.md "wikilink")；在位期间限制[贵族权力](../Page/贵族.md "wikilink")，推崇[佛教](../Page/佛教.md "wikilink")，4次遣使团访问中国隋朝，开创[飞鸟时代](../Page/飞鸟时代.md "wikilink")；屡次发兵[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")，讨伐[新罗](../Page/新罗.md "wikilink")，未果。
  - **[皇极天皇](../Page/皇极天皇.md "wikilink")**（642年－645年在位，[重祚为](../Page/重祚.md "wikilink")**[齐明天皇](../Page/齐明天皇.md "wikilink")**，655年－661年在位）：[舒明天皇皇后](../Page/舒明天皇.md "wikilink")，[孝德天皇姐](../Page/孝德天皇.md "wikilink")，日本历史上第35、37代天皇、第二位女天皇；642年其夫舒明天皇死后被权臣[苏我虾夷拥立即位](../Page/苏我虾夷.md "wikilink")；645年其子中大兄皇子（后来的[天智天皇](../Page/天智天皇.md "wikilink")）发动宫廷政变，灭[苏我入鹿](../Page/苏我入鹿.md "wikilink")，乃内禅于其弟孝德天皇；654年孝德天皇死后于次年重祚，而实权掌握在儿子中大兄皇子手中；统治后期奢侈挥霍，广营宫室；在位最后一年发兵朝鲜半岛，支援[百济](../Page/百济.md "wikilink")，对抗[新罗和](../Page/新罗.md "wikilink")[大唐联军](../Page/大唐.md "wikilink")，未交战先病逝。两年后（663年），日本、百济联军在[白江口之战中大败](../Page/白江口之战.md "wikilink")。

[Hyakuninisshu_002.jpg](https://zh.wikipedia.org/wiki/File:Hyakuninisshu_002.jpg "fig:Hyakuninisshu_002.jpg")\]\]

  - **[持统天皇](../Page/持统天皇.md "wikilink")**（690年－697年在位）：[天智天皇之女](../Page/天智天皇.md "wikilink")，[天武天皇侄女](../Page/天武天皇.md "wikilink")、皇后，日本历史上第41代天皇、第三位女天皇；686年其叔父兼[丈夫的天武天皇死後始](../Page/丈夫.md "wikilink")[临朝称制](../Page/临朝称制.md "wikilink")；689年其子[草壁皇子逝世](../Page/草壁皇子.md "wikilink")，乃于次年正月正式即位；697年内禅于其孙[文武天皇](../Page/文武天皇.md "wikilink")，称[太上天皇](../Page/太上天皇.md "wikilink")；统治期间日本文化得到很大发展，开始了[白凤时代](../Page/白凤时代.md "wikilink")。
  - **[元明天皇](../Page/元明天皇.md "wikilink")**（707年－715年在位）：[天智天皇之女](../Page/天智天皇.md "wikilink")，[持统天皇妹](../Page/持统天皇.md "wikilink")、儿媳妇，[草壁皇子姑母](../Page/草壁皇子.md "wikilink")、[太子妃](../Page/太子妃.md "wikilink")，[文武天皇姑祖母](../Page/文武天皇.md "wikilink")、母亲，日本历史上第43代天皇、第四位女天皇；707年其侄孙、也是儿子的文武天皇死后，由于其侄儿兼丈夫的草壁皇子早逝，乃即位；715年内禅于其女[元正天皇](../Page/元正天皇.md "wikilink")，为太上天皇；710年迁都[平城京](../Page/平城京.md "wikilink")，开创了日本历史上的“[奈良时代](../Page/奈良时代.md "wikilink")”；715年内禅于女儿[元正天皇](../Page/元正天皇.md "wikilink")，为太上天皇。
  - **[元正天皇](../Page/元正天皇.md "wikilink")**（715年－724年在位）：[草壁皇子与](../Page/草壁皇子.md "wikilink")[元明天皇之女](../Page/元明天皇.md "wikilink")，日本历史上第44代天皇、第五位女天皇；715年其母元明女帝内禅，乃即位；724年内禅于其侄儿[圣武天皇](../Page/圣武天皇.md "wikilink")，为太上天皇。
  - **[孝谦天皇](../Page/孝谦天皇.md "wikilink")**（749年－758年在位，重祚为**[称德天皇](../Page/称德天皇.md "wikilink")**，764年－770年在位）：[圣武天皇与](../Page/圣武天皇.md "wikilink")[光明皇后之女](../Page/光明皇后.md "wikilink")，日本历史上第46、48代天皇、第六位女天皇；749年其父圣武天皇内禅，乃即位；758年内禅于远房表弟[淳仁天皇](../Page/淳仁天皇.md "wikilink")，为太上天皇，仍掌握实际大权；764年因[藤原仲麻吕之乱](../Page/藤原仲麻吕.md "wikilink")，废黜淳仁天皇，重祚；在位期间多次派[遣唐使出使](../Page/遣唐使.md "wikilink")[中国](../Page/中国.md "wikilink")，是“奈良时代”的全盛期；晚年宠信[道镜法王](../Page/道镜.md "wikilink")，颇多弊政。
  - **[明正天皇](../Page/明正天皇.md "wikilink")**（1629年－1643年在位）：[后水尾天皇与](../Page/后水尾天皇.md "wikilink")[德川和子之女](../Page/德川和子.md "wikilink")，日本历史上第109代天皇、第七位女天皇；1629年其父后水尾天皇内禅，乃即位；1643年内禅于异母弟[后光明天皇](../Page/后光明天皇.md "wikilink")，为太上天皇。
  - **[后樱町天皇](../Page/后樱町天皇.md "wikilink")**（1762年－1770年在位）：[樱町天皇之女](../Page/樱町天皇.md "wikilink")，[桃园天皇姐](../Page/桃园天皇.md "wikilink")，日本历史上第117代天皇、第八位也是至今最后一位女天皇；1762年其弟桃园天皇死，由于桃园天皇之子英仁（后来的[后桃园天皇](../Page/后桃园天皇.md "wikilink")）尚幼，乃即位；1770年内禅于侄儿后桃园天皇，为太上天皇，继续辅政。

### [越南](../Page/越南.md "wikilink")（共1位）

  - **[李昭皇](../Page/李昭皇.md "wikilink")**\[3\]（1224年－1225年在位）：即**[李天馨](../Page/李天馨.md "wikilink")**，本名**[李佛金](../Page/李佛金.md "wikilink")**，[越南历史上唯一的女皇帝](../Page/越南历史.md "wikilink")，[越南李朝第九位也是末代皇帝](../Page/越南李朝.md "wikilink")，[李惠宗次女](../Page/李惠宗.md "wikilink")，初称**昭圣公主**；1224年因惠宗长女[顺天公主已嫁](../Page/顺天公主.md "wikilink")，又无男嗣，乃立其为[皇太女](../Page/皇太女.md "wikilink")；同年权臣[陈守度废惠宗](../Page/陈守度.md "wikilink")，立其为帝，时年仅七岁，实际权力掌握在守度手中；1225年嫁守度侄[陈日煚](../Page/陈日煚.md "wikilink")（即[陈太宗](../Page/陈太宗.md "wikilink")），同年[禅位于夫君](../Page/禅位.md "wikilink")，降为**昭圣皇后**，李朝为[陈朝所取代](../Page/越南陈朝.md "wikilink")；因无子嗣，于1237年被陈守度逼令太宗废其-{后}-位，复称**昭圣公主**；1258年復被下令嫁給大臣[黎輔陳](../Page/黎輔陳.md "wikilink")。

### [拜占庭帝国](../Page/拜占庭帝国.md "wikilink")（即[东罗马帝国](../Page/东罗马帝国.md "wikilink")，共3位）

  - **[伊琳娜女皇](../Page/伊琳娜女皇.md "wikilink")**（797年－802年在位）：拜占庭帝国[伊苏里亚王朝皇帝](../Page/伊苏里亚王朝.md "wikilink")[利奥四世皇后](../Page/东罗马帝国.md "wikilink")，[君士坦丁六世生母](../Page/君士坦丁六世.md "wikilink")，拜占庭帝国第一位女皇，也是伊苏里亚王朝末代女皇；780年其夫利奥四世死后立其子君士坦丁六世为帝，为皇太后，掌握实际大权；797年又废儿子，自立为女皇；802年被废黜，伊苏里亚王朝告终。她是拜占庭帝国历史上唯一使用[阳性](../Page/男性.md "wikilink")*男皇帝*（[希腊语](../Page/希腊语.md "wikilink")：，[英语](../Page/英语.md "wikilink")：）而非[阴性](../Page/女性.md "wikilink")*女皇帝*（希腊语：，英语：）称号的女皇。

[Zoe_mosaic_Hagia_Sophia.jpg](https://zh.wikipedia.org/wiki/File:Zoe_mosaic_Hagia_Sophia.jpg "fig:Zoe_mosaic_Hagia_Sophia.jpg")\]\]

  - **[佐伊女皇](../Page/佐伊女皇.md "wikilink")**（1042年－1050年在位）：又称“**正统嫡系佐伊**”，拜占庭帝国[马其顿王朝皇帝](../Page/马其顿王朝.md "wikilink")[君士坦丁八世与海伦娜皇后之女](../Page/君士坦丁八世.md "wikilink")，皇帝[罗梅纳斯三世皇后](../Page/东罗马帝国.md "wikilink")，拜占庭帝国第二位女皇；其父死后，夫罗梅纳斯三世即位，她与心腹[宦官约翰共谋](../Page/宦官.md "wikilink")，于1034年杀其夫，同年与出身微贱的[帕夫勒戈尼亚人米海尔结婚](../Page/帕夫拉戈尼亞.md "wikilink")，助其登位为[米海尔四世](../Page/米海尔四世.md "wikilink")；1041年米海尔四世死，她立侄儿兼养子[米海尔五世为帝](../Page/米海尔五世.md "wikilink")；米海尔五世图谋将她排除，但她被忠诚于马其顿王朝的[首都](../Page/首都.md "wikilink")[君士坦丁堡市民拥立](../Page/君士坦丁堡.md "wikilink")，废米海尔五世，作为马其顿王朝正统嫡系与妹[狄奥多拉共同在](../Page/狄奥多拉女皇.md "wikilink")1042年即帝位；同年，以六十余岁之身与[君士坦丁九世结婚](../Page/君士坦丁九世.md "wikilink")，三位皇帝共同执政。
  - **[狄奥多拉女皇](../Page/狄奥多拉女皇.md "wikilink")**（1042年－1056年在位）：又称“**正统嫡系狄奥多拉**”，拜占庭帝国马其顿王朝皇帝[君士坦丁八世与海伦娜皇后之女](../Page/君士坦丁八世.md "wikilink")，[佐伊女皇之妹](../Page/佐伊女皇.md "wikilink")，拜占庭帝国第三位、也是最后一位女皇，马其顿王朝末代皇帝；1042年起与其姐佐伊和姐夫[君士坦丁九世作为拜占廷帝国](../Page/君士坦丁九世.md "wikilink")[共主](../Page/共主.md "wikilink")，三位皇帝共同执政；1052年其姐佐伊女皇死，与姐夫君士坦丁九世两人执政；1054年君士坦丁九世死后为单独的女皇；无嗣，1056年临终前指定[米海尔六世即位](../Page/米海尔六世.md "wikilink")，马其顿王朝告终。

### [俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")[罗曼诺夫王朝](../Page/罗曼诺夫王朝.md "wikilink")（共4位）

[Louis_Caravaque,_Portrait_of_Empress_Anna_Ioannovna_(1730).jpg](https://zh.wikipedia.org/wiki/File:Louis_Caravaque,_Portrait_of_Empress_Anna_Ioannovna_\(1730\).jpg "fig:Louis_Caravaque,_Portrait_of_Empress_Anna_Ioannovna_(1730).jpg")\]\]
[Carle_Vanloo,_Portrait_de_l’impératrice_Élisabeth_Petrovna_(1760).jpg](https://zh.wikipedia.org/wiki/File:Carle_Vanloo,_Portrait_de_l’impératrice_Élisabeth_Petrovna_\(1760\).jpg "fig:Carle_Vanloo,_Portrait_de_l’impératrice_Élisabeth_Petrovna_(1760).jpg")\]\]
[Catherine_II_by_F.Rokotov_after_Roslin_(c.1770,_Hermitage).jpg](https://zh.wikipedia.org/wiki/File:Catherine_II_by_F.Rokotov_after_Roslin_\(c.1770,_Hermitage\).jpg "fig:Catherine_II_by_F.Rokotov_after_Roslin_(c.1770,_Hermitage).jpg")\]\]

  - **[叶卡捷琳娜一世](../Page/叶卡捷琳娜一世.md "wikilink")**（1725年－1727年在位）：全名**叶卡捷琳娜（一世）·阿列克谢耶芙娜·罗曼诺娃**，俄罗斯帝国第一位皇帝[彼得一世](../Page/彼得一世.md "wikilink")（即[彼得大帝](../Page/彼得大帝.md "wikilink")）的第二任皇后，俄罗斯帝国第二位皇帝、第一位女皇；本为[立陶宛一](../Page/立陶宛.md "wikilink")[农民之女](../Page/农民.md "wikilink")，在[大北方战争中为俄军所俘虏](../Page/大北方战争.md "wikilink")，旋为彼得一世所宠，成为他的[情妇](../Page/情妇.md "wikilink")；1712年正式成为彼得一世的皇后；1725年其夫彼得一世死后得到近卫军支持乃登位為女皇，也因此成為世界歷史上第二位非當朝君主血親或皇室成員而成為皇帝的女性。在位时间短，政治上无所建树；她与彼得一世育有五子六女，僅次女与三女长大成人，次女即后来的[伊丽莎白女皇](../Page/伊丽莎白一世_\(俄国\).md "wikilink")；死后无男嗣，令继孙[彼得二世继位](../Page/彼得二世_\(俄羅斯\).md "wikilink")。
  - **[安娜一世](../Page/安娜一世_\(俄国\).md "wikilink")**（1730年－1740年在位）：全名**[安娜·伊凡诺芙娜·罗曼诺娃](../Page/安娜·伊凡诺芙娜·罗曼诺娃.md "wikilink")**，[俄国沙皇](../Page/俄国沙皇.md "wikilink")[伊凡五世之女](../Page/伊凡五世.md "wikilink")，俄罗斯帝国皇帝[彼得一世侄女](../Page/彼得一世.md "wikilink")、[彼得二世姑母](../Page/彼得二世.md "wikilink")，俄罗斯帝国第四位皇帝、第二位女皇；1710年与[库尔兰](../Page/库尔兰.md "wikilink")[公爵结婚](../Page/公爵.md "wikilink")，但不到三个月其夫死，遂以遗孀身份成为库尔兰女公爵，以后终身未嫁；1730年其侄彼得二世皇帝死后无嗣，遂即位为女皇；在位期间长期对外用兵，在[波兰王位继承战争中与](../Page/波兰王位继承战争.md "wikilink")[哈布斯堡家族的](../Page/哈布斯堡家族.md "wikilink")[神圣罗马帝国皇帝](../Page/神圣罗马帝国皇帝.md "wikilink")[卡尔六世联合对抗](../Page/卡尔五世_\(神圣罗马帝国\).md "wikilink")[波旁王室的](../Page/波旁王室.md "wikilink")[法国国王](../Page/法国.md "wikilink")[路易十五并取得胜利](../Page/路易十五.md "wikilink")，但在南下进攻[奥斯曼帝国的战争](../Page/奥斯曼帝国.md "wikilink")（第四次[俄土战争](../Page/俄土战争.md "wikilink")）中受挫；以风流著称，无后，令其甥孙[伊凡六世继位](../Page/伊凡六世.md "wikilink")。
  - **[伊丽莎白一世](../Page/伊丽莎白一世_\(俄国\).md "wikilink")**（1741年－1762年在位）：全名**[伊丽莎白·彼得罗芙娜·罗曼诺娃](../Page/伊丽莎白·彼得罗芙娜·罗曼诺娃.md "wikilink")**，俄罗斯帝国皇帝[彼得一世和女皇](../Page/彼得一世.md "wikilink")[叶卡捷琳娜一世之女](../Page/叶卡捷琳娜一世.md "wikilink")，皇帝[伊凡六世之舅祖母](../Page/伊凡六世.md "wikilink")，俄罗斯帝国第六位皇帝、第三位女皇；1741年发动宫廷[政变](../Page/政变.md "wikilink")，推翻其甥孙伊凡六世，自立为女皇，以[彼得大帝合法继承人自居](../Page/彼得大帝.md "wikilink")；在位期间鼓励[商业发展](../Page/商业.md "wikilink")，使俄国经济有所复苏；资助文学艺术创作，创建[莫斯科大学和艺术院](../Page/莫斯科大学.md "wikilink")；对外参加[七年战争](../Page/七年战争.md "wikilink")，与[法国和](../Page/法国.md "wikilink")[奥地利帝国结盟](../Page/奥地利帝国.md "wikilink")，对[普鲁士作战屡屡获胜](../Page/普鲁士.md "wikilink")；但她给[贵族以](../Page/贵族.md "wikilink")[特权](../Page/特权.md "wikilink")，使广大[农奴处境更加艰难](../Page/农奴.md "wikilink")；亦以荒淫著称，终身未婚，死后无嗣，令外甥[彼得三世继位](../Page/彼得三世_\(俄国\).md "wikilink")。
  - **[叶卡捷琳娜二世](../Page/叶卡捷琳娜二世.md "wikilink")**（1762年－1796年在位）：即**[叶卡捷琳娜大帝](../Page/叶卡捷琳娜大帝.md "wikilink")**，全名**叶卡捷琳娜（二世）·阿列克谢耶芙娜·罗曼诺娃**，俄罗斯帝国皇帝[彼得三世皇后](../Page/彼得三世_\(俄国\).md "wikilink")，俄罗斯帝国第八位皇帝、第四位也是最后一位女皇；本为[德意志一](../Page/德意志.md "wikilink")[公爵之女](../Page/公爵.md "wikilink")，1745年嫁于后来的彼得三世为妻；1762年[伊丽莎白女皇死](../Page/伊丽莎白一世_\(俄国\).md "wikilink")，无嗣，其夫彼得三世即位，为皇后，同年发动宫廷政变，废黜其夫彼得三世，自立为女皇。其在位期间发动第五、六次[俄土战争并取得胜利](../Page/俄土战争.md "wikilink")，击败[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")，吞并[克里米亚汗国](../Page/克里米亚汗国.md "wikilink")，获得[黑海的出海口](../Page/黑海.md "wikilink")；与[普鲁士和](../Page/普鲁士.md "wikilink")[奥地利帝国](../Page/奥地利帝国.md "wikilink")[三次瓜分波兰](../Page/三次瓜分波兰.md "wikilink")，使俄国的地位空前提高，成为地跨[欧亚大陆的全世界最大帝国](../Page/欧亚大陆.md "wikilink")，故被尊称为“**[大帝](../Page/大帝.md "wikilink")**”，是[俄罗斯历史上仅有的两个](../Page/俄罗斯历史.md "wikilink")“大帝”之一（另一个是[彼得大帝](../Page/彼得大帝.md "wikilink")）；与[法国](../Page/法国.md "wikilink")[启蒙思想家](../Page/启蒙思想.md "wikilink")[伏尔泰等交往密切](../Page/伏尔泰.md "wikilink")，但思想仍旧保守，反对[法国大革命](../Page/法国大革命.md "wikilink")；亦以风流著称，男宠众多，两个儿子（包括后来继帝位的[保罗一世](../Page/保罗一世_\(俄国\).md "wikilink")）都有可能是她与男宠之子。但关于保罗一世是私生子的传闻並不可靠，因为保罗一世无论相貌、性格皆與其父彼得三世相似。而也是因为保罗太像彼得三世的缘故，所以叶卡捷琳娜二世十分讨厌保罗。（传闻彼得三世没有生育能力，但并非事实，彼得三世曾与一情人有私生子）。

### [印度帝國](../Page/英属印度.md "wikilink")（共1位）

[Queen_Victoria_bw.jpg](https://zh.wikipedia.org/wiki/File:Queen_Victoria_bw.jpg "fig:Queen_Victoria_bw.jpg")\]\]

  - **[亚历山德丽娜·维多利亚](../Page/亚历山德丽娜·维多利亚.md "wikilink")**（1877年－1901年在位），即[大不列颠及爱尔兰联合王国之](../Page/大不列颠及爱尔兰联合王国.md "wikilink")**[维多利亚女王](../Page/维多利亚女王.md "wikilink")**（1837年－1901年在位），第一任[英属印度皇帝](../Page/印度皇帝.md "wikilink")，英属印度唯一的女皇，也是[英国历史上获得](../Page/英国历史.md "wikilink")“女皇”称号的君主，[英国国王](../Page/英国国王.md "wikilink")[威廉四世的侄女](../Page/威廉四世_\(英國\).md "wikilink")；1837年其伯父威廉四世死后即位为[英国女王](../Page/英国女王.md "wikilink")；1840年发动[鸦片战争](../Page/鸦片战争.md "wikilink")，击败[大清帝国](../Page/大清帝国.md "wikilink")，签订《[南京条约](../Page/南京条约.md "wikilink")》，开西方列强与[中国签订](../Page/中国.md "wikilink")[不平等条约之先河](../Page/不平等条约.md "wikilink")；1877年接受“[印度女皇](../Page/印度女皇.md "wikilink")”称号；其在位期间英国空前强盛，经济、科学、文学、艺术都有很大的发展，尤其是在殖民统治方面，使[大英帝国进入全盛时期](../Page/大英帝国.md "wikilink")，成为第二个“[日不落帝国](../Page/日不落帝国.md "wikilink")”（第一个是[西班牙帝国](../Page/西班牙帝国.md "wikilink")），被誉为“[维多利亚时代](../Page/维多利亚时代.md "wikilink")”；如今世界上许多[河流](../Page/河流.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")、[沙漠](../Page/沙漠.md "wikilink")、[瀑布](../Page/瀑布.md "wikilink")、[城市等都是以她的名字来命名的](../Page/城市.md "wikilink")；其子女众多，与[欧洲各国](../Page/欧洲.md "wikilink")[王室](../Page/王室.md "wikilink")[联姻](../Page/联姻.md "wikilink")，有“欧洲的祖母”之称。

### [阿比西尼亚](../Page/阿比西尼亚.md "wikilink")（今[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")，共1位）

  - **[佐迪圖](../Page/佐迪圖.md "wikilink")**（1916年－1930年在位）：阿比西尼亚皇帝[孟尼利克二世的私生女](../Page/孟尼利克二世.md "wikilink")，[埃塞俄比亚历史上唯一的女皇](../Page/埃塞俄比亚历史.md "wikilink")（正式称号是“**众王之女王**”）；1913年其父孟尼利克二世病故，其异母姐妹之子埃雅苏五世继位；1916年埃雅苏五世被废，遂即位为女皇，以堂弟[塔法里·马康南](../Page/塔法里·马康南.md "wikilink")[亲王](../Page/亲王.md "wikilink")[摄政并立为](../Page/摄政.md "wikilink")[皇储](../Page/皇储.md "wikilink")；后与摄政王在改革还是守旧的问题上发生分歧，秘密策动前夫发动地方叛乱，但被摄政王镇压；1930年4月在惊恐中病逝；摄政王即位，即阿比西尼亚末代皇帝[海尔·塞拉西一世](../Page/海尔·塞拉西一世.md "wikilink")。

## 神话传说

[Anonymous-Fuxi_and_Nüwa3.jpg](https://zh.wikipedia.org/wiki/File:Anonymous-Fuxi_and_Nüwa3.jpg "fig:Anonymous-Fuxi_and_Nüwa3.jpg")与[牺皇](../Page/牺皇.md "wikilink")\]\]

  - **[神功皇后](../Page/神功皇后.md "wikilink")**：日本[古墳时代的皇族](../Page/古墳时代.md "wikilink")，為[仲哀天皇](../Page/仲哀天皇.md "wikilink")[皇后](../Page/皇后.md "wikilink")、[應神天皇生母](../Page/應神天皇.md "wikilink")，在仲哀天皇駕崩後開始[攝政](../Page/攝政.md "wikilink")，攝政期間長達39年，成為[倭國實際上的統治者](../Page/倭國.md "wikilink")。在[明治維新以前多被視為](../Page/明治維新.md "wikilink")-{[准天皇](../Page/日本天皇.md "wikilink")}-或[女天皇](../Page/女天皇.md "wikilink")，甚至一度被列入天皇任數的計算內，但現今只將神功皇后視為攝政皇后而非女天皇。

## 未被普遍承認的歷史人物

**中國**

  - **[元氏](../Page/元氏_\(北魏孝明帝女\).md "wikilink")**（528年－？）：[北魏孝明帝与](../Page/北魏孝明帝.md "wikilink")[潘外憐之女](../Page/潘外憐.md "wikilink")；528年二月，其祖母[胡太后毒死其父孝明帝](../Page/宣武靈皇后.md "wikilink")，假称她为男婴，以[皇太子身份继承皇位](../Page/皇太子.md "wikilink")，當时元氏甫出生50天；然而她即位当天，胡太后宣布她的真实身份並废之，改立[元钊为帝](../Page/元钊.md "wikilink")；不到一个月，[尔朱荣攻陷首都](../Page/尔朱荣.md "wikilink")[洛阳](../Page/洛阳.md "wikilink")，发动[河阴之变](../Page/河阴之变.md "wikilink")，沉胡太后和幼主元钊于[黄河](../Page/黄河.md "wikilink")，另立[孝莊帝](../Page/北魏孝庄帝.md "wikilink")，女婴皇帝则不知所終。她的女皇帝身分普遍不被后世所承认，一来是因为她是胡太后的[傀儡](../Page/傀儡.md "wikilink")，二来是因为她是以冒名男婴而即帝位的。
  - **[陳碩真](../Page/陳碩真.md "wikilink")**（620年－653年）：一作**陈硕贞**，[唐高宗时](../Page/唐高宗.md "wikilink")[睦州](../Page/睦州_\(仁寿\).md "wikilink")[清溪县](../Page/清溪县_\(永贞\).md "wikilink")（今[浙江省](../Page/浙江省.md "wikilink")[淳安县](../Page/淳安县.md "wikilink")）[农民起义领袖](../Page/农民起义.md "wikilink")；本为[女巫](../Page/女巫.md "wikilink")，自称从上天回到人间，化身为[男子](../Page/男性.md "wikilink")，以妖言惑众；653年十月初起兵，自称“**文佳皇帝**”；但起义很快失败，她本人也于当年十一月被杀。后世大多不承认她为女皇帝，一来是因为她所领导的起义在国泰民安的高宗时代只称得上是叛乱，二来是因为她已经化为男身了；但也有小部分学者承认她的女皇帝身份，如现代史学家[翦伯赞在其所编撰的](../Page/翦伯赞.md "wikilink")《[中国史纲要](../Page/翦伯赞.md "wikilink")》中称她为“中国第一个女皇帝”。

**日本**

  - *'
    [飯豐青皇女](../Page/飯豐青皇女.md "wikilink")**，（440年－484年11月〉，或稱**清貞天皇*'，[古事記記載為](../Page/古事記.md "wikilink")[履中天皇第一皇女](../Page/履中天皇.md "wikilink")，但[日本書紀中卻是](../Page/日本書紀.md "wikilink")[市邊押磐皇子之女](../Page/市邊押磐皇子.md "wikilink")。在[清寧天皇駕崩直至](../Page/清寧天皇.md "wikilink")[顯宗天皇繼位以前](../Page/顯宗天皇.md "wikilink")，曾短暫[臨朝聽政](../Page/臨朝聽政.md "wikilink")，但上述兩本史籍都不承認其為女天皇。而在[扶桑略記一書中稱飯豐青皇女為](../Page/扶桑略記.md "wikilink")「第24代飯豐天皇」。
  - *'
    [間人皇女](../Page/間人皇女.md "wikilink")**，（？－665年3月16日），或稱**中津天皇*'，[舒明天皇](../Page/舒明天皇.md "wikilink")[皇女](../Page/皇女.md "wikilink")、[孝德天皇](../Page/孝德天皇.md "wikilink")[皇后](../Page/皇后.md "wikilink")。於[齊明天皇駕崩直至](../Page/齊明天皇.md "wikilink")[天智天皇即位前的空檔](../Page/天智天皇.md "wikilink")，她曾經短暫即位（或[攝政](../Page/攝政.md "wikilink")）。有一說認為[萬葉集中以](../Page/萬葉集.md "wikilink")「中皇命」之名留有和歌傳世者即間人皇女。

[Dona_Isabel.jpg](https://zh.wikipedia.org/wiki/File:Dona_Isabel.jpg "fig:Dona_Isabel.jpg")
**巴西帝國**

  - **[伊莎贝尔一世](../Page/伊莎贝尔_\(巴西\).md "wikilink")**（1846年－1921年）：[巴西帝国末代皇帝](../Page/巴西帝国.md "wikilink")[佩德罗二世的长女](../Page/佩德罗二世_\(巴西\).md "wikilink")，1850年弟弟皇储阿方索去世，她被繼立為女皇储。1889年他的父亲被推翻，[君主制覆亡](../Page/君主制.md "wikilink")。1891年她的父亲去世，她成为名义上的**巴西女皇**伊莎贝尔一世。但只得到了君主制支持者的承认。

**墨西哥**

  - **[玛利亚·何塞法](../Page/墨西哥君主列表.md "wikilink")**（1872年－1949年）：[墨西哥皇帝](../Page/墨西哥.md "wikilink")[阿古斯汀一世的后代](../Page/阿古斯汀一世_\(墨西哥\).md "wikilink")，阿古斯汀之子皇储阿古斯汀的长孙女。墨西哥君主制于1867年覆亡，但阿古斯汀一世的后代仍要求墨西哥的皇位。1925年墨西哥王子阿古斯汀（三世）去世，玛利亚·何塞法作为他的侄女成为名义上的**墨西哥女皇**，但只得到了极少部分君主制支持者的承认。

**朝鮮王朝**

  - **[李海瑗](../Page/李海瑗.md "wikilink")**（1919年4月24日－），[朝鮮王朝王室](../Page/朝鮮王朝.md "wikilink")、[大韓帝國皇室後裔](../Page/大韓帝國.md "wikilink")，[朝鮮高宗孫女](../Page/朝鮮高宗.md "wikilink")、義親王[李堈次女](../Page/李堈.md "wikilink")，目前是朝鮮王朝王室後裔中最年長的。她是第三十任也是現任[朝鮮王朝家族首領](../Page/朝鲜王朝君主列表.md "wikilink")，自稱**文化大韓帝國女皇**。

## 其他

在历史上还有些[朝代中也有权力超越了皇帝但并未篡权称帝的](../Page/朝代.md "wikilink")[皇后](../Page/皇后.md "wikilink")、[皇太后等女性统治者](../Page/皇太后.md "wikilink")。这种情况，即是历史上所称的[临朝称制](../Page/临朝称制.md "wikilink")，自[武则天以后又叫](../Page/武则天.md "wikilink")[垂帘听政](../Page/垂帘听政.md "wikilink")，最典型的例子是[战国时](../Page/战国.md "wikilink")[秦国的](../Page/秦国.md "wikilink")[宣太后](../Page/宣太后.md "wikilink")、[西汉的](../Page/西汉.md "wikilink")[吕后和](../Page/吕后.md "wikilink")[晚清的](../Page/晚清.md "wikilink")[慈安](../Page/慈安太后.md "wikilink")、[慈禧太后](../Page/慈禧太后.md "wikilink")。慈禧太后在慈安太后逝世后，成为[大清唯一的实际统治者](../Page/大清.md "wikilink")，继续掌控[光绪帝的決策](../Page/光绪帝.md "wikilink")，甚至被称为“无冕女皇”。

其他例子如[西辽的](../Page/西辽.md "wikilink")[感天后和](../Page/感天后.md "wikilink")[承天太后](../Page/西辽承天后.md "wikilink")，[蒙古帝国的](../Page/蒙古帝国.md "wikilink")[乃马真-{后}-和](../Page/乃馬真后.md "wikilink")[海迷失-{后}-等](../Page/海迷失后.md "wikilink")，她们临朝称制时皇位空缺，成为实际上的一国之主，虽然未称帝，但一般把她们作为君主甚至准皇帝，最典型的标志就是以她们的名字和[年号来](../Page/年号.md "wikilink")[纪年](../Page/纪年.md "wikilink")。

女性临朝称制在東亞历史中较常出现，原因在于[儒家](../Page/儒家.md "wikilink")[文化的独特性](../Page/文化.md "wikilink")——“[孝](../Page/孝.md "wikilink")”。汉代以来，中国历代封建王朝无不对“孝”推崇有加，渗透于国家政治的方方面面至近代。在此背景下，太后凭借[先皇帝遗孀和新皇帝母亲](../Page/先帝遺孀.md "wikilink")（[嫡母](../Page/嫡母.md "wikilink")）的身份，藉由[父权](../Page/父權.md "wikilink")，进入国家权力中心。

日本的传说與历史中，曾出现皇位空缺而暫由皇后臨朝的情況，如[神功皇后曾在應神天皇即位前稱制](../Page/神功皇后.md "wikilink")69年\[4\]，因此在[明治维新以前](../Page/明治维新.md "wikilink")，她也被视为[天皇或准天皇](../Page/日本天皇.md "wikilink")；[南北朝時期在](../Page/南北朝_\(日本\).md "wikilink")[足利尊氏的策劃下](../Page/足利尊氏.md "wikilink")，使北朝方面完全沒有天皇管理朝政（天皇、[上皇與](../Page/上皇.md "wikilink")[法皇皆無](../Page/太上法皇.md "wikilink")），因此由[後伏見上皇](../Page/後伏見天皇.md "wikilink")[女御](../Page/女御.md "wikilink")－[西園寺寧子](../Page/西園寺寧子.md "wikilink")，以[國母](../Page/國母.md "wikilink")（[光嚴天皇及](../Page/光嚴天皇.md "wikilink")[光明天皇生母](../Page/光明天皇.md "wikilink")）的身分代行天皇職權，成為實際上的[治天之君](../Page/治天之君.md "wikilink")。

## 参见

  - [女王](../Page/女王.md "wikilink")
  - [皇后](../Page/皇后.md "wikilink")
  - [皇帝](../Page/皇帝.md "wikilink")
  - [日本天皇](../Page/日本天皇.md "wikilink")
  - [君主](../Page/君主.md "wikilink")
  - [女性天皇](../Page/女性天皇.md "wikilink")
  - [临朝称制](../Page/临朝称制.md "wikilink")
  - [垂帘听政](../Page/垂帘听政.md "wikilink")
  - [春日局](../Page/春日局.md "wikilink")

## 注释

<div class="references-small">

<references />

</div>

## 参考资料

  - 《[魏书](../Page/魏书.md "wikilink")》，[北齐](../Page/北齐.md "wikilink")·[魏收](../Page/魏收.md "wikilink")
  - 《[北史](../Page/北史.md "wikilink")》，[唐](../Page/唐代.md "wikilink")·[李延寿](../Page/李延寿.md "wikilink")
  - 《[史记](../Page/史记.md "wikilink")·补三皇本纪》，唐·[司马贞](../Page/司马贞.md "wikilink")
  - 《[旧唐书](../Page/旧唐书.md "wikilink")·则天皇后本纪》，[后晋](../Page/后晋.md "wikilink")·[刘昫等](../Page/刘昫.md "wikilink")
  - 《[新唐书](../Page/新唐书.md "wikilink")·则天皇后本纪》，[北宋](../Page/北宋.md "wikilink")·[欧阳修](../Page/欧阳修.md "wikilink")、[宋祁](../Page/宋祁.md "wikilink")
  - 《新唐书·后妃[列传上](../Page/列传.md "wikilink")·则天武皇后传》，同上
  - 《[资治通鉴](../Page/资治通鉴.md "wikilink")》，[北宋](../Page/北宋.md "wikilink")·[司马光](../Page/司马光.md "wikilink")
  - 《[s:中国史纲要](../Page/s:中国史纲要.md "wikilink")》，当代·[翦伯赞](../Page/翦伯赞.md "wikilink")
  - 《[古事记](../Page/古事记.md "wikilink")》，[日本](../Page/日本.md "wikilink")
  - 《[日本书纪](../Page/日本书纪.md "wikilink")》，同上
  - 《[大越史记全书](../Page/大越史记全书.md "wikilink")》，[越南](../Page/越南.md "wikilink")·[吴士连等](../Page/吴士连.md "wikilink")
  - 《叶卡捷琳娜大帝——传记与传说》，约翰·T·亚历山大，[纽约](../Page/纽约.md "wikilink")[牛津大学出版社](../Page/牛津大学出版社.md "wikilink")（美国），1989年
    ISBN 0-19-506162-4
  - 《叶卡捷琳娜——全俄罗斯女皇》，[文森特·克罗宁](../Page/俄罗斯文学.md "wikilink")，[伦敦柯林斯出版社](../Page/伦敦.md "wikilink")，1996年
    ISBN 1-86046-091-7
  - 《拜占庭女皇伊琳娜》（法文），多米尼克·芭比，1990年，[巴黎](../Page/巴黎.md "wikilink")
  - 《拜占庭的女皇和皇后们——公元527年至1204年拜占庭帝国的女人和权力》，琳达
    ·加兰，1999年，[伦敦Routledge出版社](../Page/伦敦.md "wikilink")
    ISBN 0415146887.

## 備註

[hr:Kraljica
(razdvojba)](../Page/hr:Kraljica_\(razdvojba\).md "wikilink")

[女皇帝](../Category/女皇帝.md "wikilink")
[Category:女性称谓](../Category/女性称谓.md "wikilink")
[Category:皇帝称谓](../Category/皇帝称谓.md "wikilink")
[Category:女性与政治](../Category/女性与政治.md "wikilink")

1.  日本持統天皇與中國的武則天，皆於西元690年登位，但持統天皇於該年正月即位，早於該年九月才登基的武則天。
2.  武則天乃透過與唐高宗之間的婚姻而位列皇族。此處所指的皇室成員是指其上一代也是皇室成員者，而非透過婚姻位列皇族。
3.  [越南当时为](../Page/越南.md "wikilink")[中国的](../Page/中国.md "wikilink")[朝贡国](../Page/朝贡国.md "wikilink")，国君在国内自称**大越皇帝**，对中国只称**[安南](../Page/越南.md "wikilink")[国王](../Page/国王.md "wikilink")**而不称皇帝，故[李昭皇也称](../Page/李昭皇.md "wikilink")“**李昭王**”。
4.  [神功皇后是否确有其人史学界有很大争议](../Page/神功皇后.md "wikilink")，她是介于[神话传说和真实历史之间的人物](../Page/神话.md "wikilink")。