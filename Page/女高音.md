[Sopran.png](https://zh.wikipedia.org/wiki/File:Sopran.png "fig:Sopran.png")
[Range_of_alto_voice_marked_on_keyboard.svg](https://zh.wikipedia.org/wiki/File:Range_of_alto_voice_marked_on_keyboard.svg "fig:Range_of_alto_voice_marked_on_keyboard.svg")\]\]
[Tenor.png](https://zh.wikipedia.org/wiki/File:Tenor.png "fig:Tenor.png")\]\]
[Range_of_bass_voice_marked_on_keyboard.png](https://zh.wikipedia.org/wiki/File:Range_of_bass_voice_marked_on_keyboard.png "fig:Range_of_bass_voice_marked_on_keyboard.png")\]\]

在[音乐](../Page/音乐.md "wikilink")，**女高音**（**soprano**）音域指成年女歌手能达到的[声音](../Page/声音.md "wikilink")[频率](../Page/频率.md "wikilink")（而非[响度](../Page/响度.md "wikilink")）最高的（通常是C4-C6）范围。注意，英文中soprano一词还表示一个系列的乐器中声音频率最高的一种。

## 女高音種類

演唱女高音声部的歌手现在主要是女高音歌手。按音色、音区等不同特点分为：[花腔女高音](../Page/花腔女高音.md "wikilink")（Coloratura
soprano）（声音最高，能唱出相当高的颤音）、[抒情女高音](../Page/抒情女高音.md "wikilink")（lyric
soprano）（声音较高，连贯流畅）和[戏剧女高音](../Page/戏剧女高音.md "wikilink")（dramatic
soprano）（声音较低但有力度）等。[威尔第的](../Page/威尔第.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")《[阿伊达](../Page/阿伊达.md "wikilink")》第一幕第一场中的《胜利归来》就是一首典型的戏剧女高音独唱曲。

## 其他演唱女高音音域的歌手

下列歌手也能唱女高音音域，但音色与女高音歌手相似但略有不同：男童声高音歌手（boy
soprano，或称作treble）（指未变声的儿童），女高音声部的高男高音音歌手（male
soprano，用[假声和其它技巧来达到这个音域](../Page/假声.md "wikilink")，很少有男歌手能达到这样的音高，故很罕见。参看女低音声部的[高男高音歌手](../Page/高男高音歌手.md "wikilink")([Countertenor](../Page/Countertenor.md "wikilink")/[Alto](../Page/Alto.md "wikilink"))）和[阉人歌手](../Page/阉人歌手.md "wikilink")（现已绝迹）。因为西方国家古代教堂里不允许女性加入唱诗班，所以女童声高音歌手，尽管也可以唱女高音音域，却很少见。这种性别歧视也促使了阉人歌手在15-18世纪的流行，及男歌手的假声演唱技巧（这种歌手包括女低音声部的高男高音歌手和女高音声部的高男高音音歌手）的发展。

## 其他声部

  - [女中音](../Page/女中音.md "wikilink")（Mezzo-soprano，又作[次女高音](../Page/次女高音.md "wikilink")）
  - [女低音](../Page/女低音.md "wikilink")（Contralto，又作Alto）
  - [男高音](../Page/男高音.md "wikilink")（Tenor）
  - [男中音](../Page/男中音.md "wikilink")（Baritone）
  - [男低音](../Page/男低音.md "wikilink")（Bass）

[Category:声乐](../Category/声乐.md "wikilink")
[女高音](../Category/女高音.md "wikilink")