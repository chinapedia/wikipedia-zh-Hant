[HK_Yau_Ma_Tei_文華新邨_Man_Wah_Sun_Chuen_restaurant_Hot_milk_tea_June-2011.jpg](https://zh.wikipedia.org/wiki/File:HK_Yau_Ma_Tei_文華新邨_Man_Wah_Sun_Chuen_restaurant_Hot_milk_tea_June-2011.jpg "fig:HK_Yau_Ma_Tei_文華新邨_Man_Wah_Sun_Chuen_restaurant_Hot_milk_tea_June-2011.jpg")港式奶茶\]\]
[Japanese_canned_milk_tea.jpg](https://zh.wikipedia.org/wiki/File:Japanese_canned_milk_tea.jpg "fig:Japanese_canned_milk_tea.jpg")
[Ice_Chilled_Milk_Tea.jpg](https://zh.wikipedia.org/wiki/File:Ice_Chilled_Milk_Tea.jpg "fig:Ice_Chilled_Milk_Tea.jpg")
**奶茶**是一種將[茶和](../Page/茶.md "wikilink")[奶](../Page/奶.md "wikilink")（或[奶精](../Page/奶精.md "wikilink")、沖泡[奶粉](../Page/奶粉.md "wikilink")）混合的飲料，可加以調理飲用，世界各地都可見其蹤影，而此飲料的起源和製作方式則因各地特色有所不同。

## 種類

### 袋泡式奶茶

袋泡式奶茶，是将[茶叶](../Page/茶叶.md "wikilink")、[糖与](../Page/糖.md "wikilink")[奶粉包覆在棉纸袋中](../Page/奶粉.md "wikilink")，所制作而成的袋泡式奶茶；有别于一般市面上常见的速溶奶茶。一般来说，茶叶即使完成了所有的[烘培过程后](../Page/烘培.md "wikilink")，还是会有[发酵的情况产生](../Page/发酵.md "wikilink")，所以茶叶要在保存的时间内使用完，才能喝到最佳风味。而茶叶与糖、奶精放在一起，更容易加速发酵的情况，因此得靠特别的技术才要克服这样的问题——稳定茶叶的状态，保持最佳的风味。

Tea and Milk.jpeg|把奶加進茶的過程 Assam Milk
Tea3.jpg|在超市售賣以塑膠瓶裝載的奶茶飲品，圖為[统一阿萨姆奶茶](../Page/统一阿萨姆奶茶.md "wikilink")

## 社會及文化

### 中國大陸

[蒙古族](../Page/蒙古族.md "wikilink")、[哈薩克族](../Page/哈薩克族.md "wikilink")、[柯爾克孜族](../Page/柯爾克孜族.md "wikilink")、[藏族等均有製作奶茶的習慣](../Page/藏族.md "wikilink")。[蒙古族的奶茶以](../Page/蒙古族.md "wikilink")[磚茶](../Page/磚茶.md "wikilink")、[牛奶](../Page/牛奶.md "wikilink")，[羊奶或](../Page/羊乳.md "wikilink")[馬奶](../Page/馬乳.md "wikilink")、和以[黄油煮成](../Page/黄油.md "wikilink")，加[鹽調理使味道偏鹹](../Page/鹽.md "wikilink")\[1\]；南方的[港式奶茶又稱為](../Page/港式奶茶.md "wikilink")「[絲襪奶茶](../Page/絲襪奶茶.md "wikilink")」，當地飲用奶茶的習慣起源於英國的[下午茶](../Page/下午茶.md "wikilink")，但製法有所不同，以[紅茶混和濃鮮奶加糖製成](../Page/紅茶.md "wikilink")，用乳量及[糖份較多](../Page/糖.md "wikilink")，冷熱飲均可。

### 香港、澳門

[HK_McDonald's_instant_noodles_breakfast.JPG](https://zh.wikipedia.org/wiki/File:HK_McDonald's_instant_noodles_breakfast.JPG "fig:HK_McDonald's_instant_noodles_breakfast.JPG")的早餐，右上方是紅茶包在熱水中浸泡，然後加入左上方的半對半[鮮奶油](../Page/鮮奶油.md "wikilink")\]\]
港式奶茶在香港、澳門都十分常見，是普羅大眾的廉價飲料，全香港人每日饮用超过250万杯奶茶\[2\]，[快餐店](../Page/快餐店.md "wikilink")、[茶餐廳等亦會提供奶茶隨快餐發售](../Page/茶餐廳.md "wikilink")，配搭中餐或[西餐均可](../Page/西餐.md "wikilink")；把奶茶和咖啡混在一起則稱為[鴛鴦](../Page/鴛鴦_\(飲料\).md "wikilink")。

港式奶茶又稱為**[絲襪奶茶](../Page/絲襪奶茶.md "wikilink")**，原因是用作過濾奶茶的棉紗網經奶茶浸泡後，顏色與外狀猶如[絲襪](../Page/絲襪.md "wikilink")。據說以棉紗茶袋濾過的奶茶口質特別細滑，茶味更均勻\[3\]。

### 台灣

起源於[台灣的](../Page/台灣.md "wikilink")[珍珠奶茶](../Page/珍珠奶茶.md "wikilink")，則於奶茶中內加入煮熟後外觀烏黑晶透的[粉圓](../Page/粉圓.md "wikilink")，遂以「珍珠」命名，另可加入[布丁](../Page/布丁.md "wikilink")、[椰果](../Page/椰果.md "wikilink")、[仙草等各式配料](../Page/仙草.md "wikilink")，調製特殊風味。

### 馬來半島

[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡的奶茶稱為](../Page/新加坡.md "wikilink")「[拉茶](../Page/拉茶.md "wikilink")」，製作方法與香港奶茶差不多，唯多一道「拉」的程序，已成為講技巧的一門手藝。所謂「拉茶」即是將已煮好的奶茶由一個器皿高空倒入另一個器皿中，此過程會被重覆數次，高度的衝力被認為可以激發奶茶濃郁之香氣並使之滑潤均勻。但亦有人指這些工序欠缺衛生考量，可能對奶茶的品質構成負面影響。

### 英國

傳統的英式奶茶以[紅茶為基礎](../Page/紅茶.md "wikilink")，只用少量[牛奶](../Page/牛奶.md "wikilink")，不用[鮮奶油](../Page/鮮奶油.md "wikilink")，有時亦會加入[食糖](../Page/食糖.md "wikilink")。茶杯的體積較少，一般於[早餐](../Page/早餐.md "wikilink")、[下午茶或](../Page/下午茶.md "wikilink")[晚餐後聊天時飲用](../Page/晚餐.md "wikilink")。

## 參考資料

[奶茶](../Category/奶茶.md "wikilink")

1.
2.  [港人日饮奶茶250万杯](http://news.xinhuanet.com/2011-08/01/c_121755214.htm)
3.