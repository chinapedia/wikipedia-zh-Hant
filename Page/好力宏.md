**好力宏**是歌手[王力宏的第一张](../Page/王力宏.md "wikilink")[精選輯](../Page/精選輯.md "wikilink")，於1998年8月12日正式發行。

| **曲目**        | **出自專輯**                                   |
| ------------- | ------------------------------------------ |
| 01.因为有你       | [New！](../Page/New！.md "wikilink")         |
| 02.白纸         | [白纸](../Page/白纸.md "wikilink")             |
| 03.喊我一千遍      | [如果你听见我的歌](../Page/如果你听见我的歌.md "wikilink") |
| 04.好想你        | [好想你](../Page/好想你.md "wikilink")           |
| 05.在每一秒里都想见到你 | [白纸](../Page/白纸.md "wikilink")             |
| 06.有了你就足够     | [如果你听见我的歌](../Page/如果你听见我的歌.md "wikilink") |
| 07.不说后悔       | [好想你](../Page/好想你.md "wikilink")           |
| 08.最好的爱       | [New！](../Page/New！.md "wikilink")         |
| 09.雪人         | （Single）                                   |
| 10.不要开灯       | [白纸](../Page/白纸.md "wikilink")             |
| 11.风中的遗憾      | [好想你](../Page/好想你.md "wikilink")           |
| 12.如果你听见我的歌   | [如果你听见我的歌](../Page/如果你听见我的歌.md "wikilink") |
| 13.头版摇滚       | [如果你听见我的歌](../Page/如果你听见我的歌.md "wikilink") |
| 14.四月还会下雪     | [白纸](../Page/白纸.md "wikilink")             |

## 参考文献

[Category:1998年音樂專輯](../Category/1998年音樂專輯.md "wikilink")
[Category:王力宏音樂專輯](../Category/王力宏音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:福茂唱片音乐专辑](../Category/福茂唱片音乐专辑.md "wikilink")