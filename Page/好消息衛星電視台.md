**好消息電視台**（），是[台灣第一家](../Page/台灣.md "wikilink")[基督教](../Page/基督教.md "wikilink")[電視台](../Page/電視台.md "wikilink")，也是一間[非營利的電視台](../Page/非營利.md "wikilink")，1997年建台，1998年9月9日開播。好消息1台定頻在台灣[有線電視第](../Page/有線電視.md "wikilink")15頻道'''
HD
'''高畫質頻道（各地系統業者陸續升級)，節目總監是[寇紹恩](../Page/寇紹恩.md "wikilink")[牧師](../Page/牧師.md "wikilink")，代言人是[孫越](../Page/孫越_\(台灣演員\).md "wikilink")。
[Good_TV_headquarters_view_from_Highway_64_20171028.jpg](https://zh.wikipedia.org/wiki/File:Good_TV_headquarters_view_from_Highway_64_20171028.jpg "fig:Good_TV_headquarters_view_from_Highway_64_20171028.jpg")
2012年7月1日，好消息電視台從長期租用的[台北市](../Page/台北市.md "wikilink")[內湖區](../Page/內湖區.md "wikilink")[公視大樓遷至](../Page/公共電視文化事業基金會.md "wikilink")[新北市](../Page/新北市.md "wikilink")[中和區中正路](../Page/中和區.md "wikilink")911號6樓\[1\]。

## 旗下頻道

### 綜合台

原好消息一台，定頻在台灣播放。2012年7月12日[台灣時間上午](../Page/台灣時間.md "wikilink")9時整起，[中華電信MOD第](../Page/中華電信MOD.md "wikilink")3頻道改以HD訊號播出好消息一台。

### 真理台

原好消息二台，主要向台灣以外播放（目前右上台標為「GOOD 2」）。

2005年8月30日，[行政院衛星電視審議委員會審議通過好消息二台申設案](../Page/行政院.md "wikilink")，好消息二台主要服務海外[華人](../Page/華人.md "wikilink")[教友](../Page/教友.md "wikilink")\[2\]。2009年7月31日[台灣時間中午](../Page/台灣時間.md "wikilink")12時整起，好消息二台進駐[中華電信MOD第](../Page/中華電信MOD.md "wikilink")15頻道，取代好消息一台。2010年4月1日台灣時間中午起，由於中華電信MOD調整頻道區塊及編號，好消息二台的中華電信MOD頻道號碼改為第3頻道。2011年10月5日台灣時間中午12時整起，中華電信MOD第3頻道改播好消息一台，取代好消息二台。

2012年10月，好消息二台轉型更名為好消息真理造就台，並於同年10月15日於[凱擘](../Page/凱擘.md "wikilink")、[台固媒體旗下有線電視系統的數位頻道](../Page/台固媒體.md "wikilink")212台可觀賞到。

2018年2月1日起，於[中嘉網路旗下有線電視系統的數位頻道第](../Page/中嘉網路.md "wikilink")131台就可收看。

## 節目列表

节目也邀请了著名运动员如[周天成和](../Page/周天成.md "wikilink")[林书豪分享生命见证](../Page/林书豪.md "wikilink")。

## 參考文獻

## 外部連結

  - [好消息電視台台灣網站](http://www.goodtv.tv/)

  - [好消息電視台北美網站](http://www.goodtvusa.tv/)

  -
{{-}}

[Category:台灣電視台](../Category/台灣電視台.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:臺灣基督教媒體](../Category/臺灣基督教媒體.md "wikilink")
[Category:基督教電視台](../Category/基督教電視台.md "wikilink")
[Category:無廣告電視網](../Category/無廣告電視網.md "wikilink")

1.
2.