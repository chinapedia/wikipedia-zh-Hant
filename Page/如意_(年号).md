**如意**（692年四月—九月）是[武則天的年号](../Page/武則天.md "wikilink")。

## 大事记

  - 天授三年四月初一丙申（[692年](../Page/692年.md "wikilink")[4月22日](../Page/4月22日.md "wikilink")），改元如意。
  - 如意元年九月初九庚子（692年[10月23日](../Page/10月23日.md "wikilink")），改元长寿。

## 出生

## 逝世

## 纪年

| 如意                               | 元年                                 |
| -------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [692年](../Page/692年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink")     |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号

[Category:武周年号](../Category/武周年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:690年代中国政治](../Category/690年代中国政治.md "wikilink")
[Category:692年](../Category/692年.md "wikilink")