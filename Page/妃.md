**妃**，和-{[后](../Page/后.md "wikilink")}-相較，是較次一級的稱謂，在[東亞](../Page/東亞.md "wikilink")[漢字文化圈內](../Page/漢字文化圈.md "wikilink")，為多國君王[正配或](../Page/嫡妻.md "wikilink")[側室的位號](../Page/側室.md "wikilink")。

## 中國

  - [夏](../Page/夏.md "wikilink")[商時期](../Page/商朝.md "wikilink")，君王正配稱為妃，或稱王妃、正妃，側室則稱為側妃、次妃等。
  - [周代以後](../Page/周代.md "wikilink")，君王正配改稱王后，自此妃轉為君王側室、男性[皇族正配的稱呼](../Page/皇族.md "wikilink")：
  - [秦漢時期](../Page/秦漢.md "wikilink")：[皇太子正室為](../Page/皇太子.md "wikilink")[太子妃](../Page/太子妃.md "wikilink")，並為後世沿用。[親王](../Page/親王.md "wikilink")、[王的正室亦稱妃](../Page/王.md "wikilink")，如唐[廣平王正配崔氏称](../Page/唐代宗.md "wikilink")[崔妃](../Page/崔妃_\(唐代宗\).md "wikilink")。
  - [魏晋時期](../Page/魏晋.md "wikilink")：[魏明帝設](../Page/魏明帝.md "wikilink")[淑妃](../Page/淑妃.md "wikilink")，位次於皇后、[夫人和](../Page/夫人_\(位号\).md "wikilink")[贵嫔](../Page/贵嫔.md "wikilink")。[晋武帝時](../Page/晋武帝.md "wikilink")，淑妃為[九嬪之一](../Page/九嬪.md "wikilink")。
  - [唐](../Page/唐.md "wikilink")[宋兩朝](../Page/宋朝.md "wikilink")：[貴妃](../Page/貴妃.md "wikilink")、淑妃、[德妃](../Page/德妃.md "wikilink")、[賢妃等為正一品](../Page/賢妃.md "wikilink")，位在皇后之下。
  - [金朝](../Page/金朝.md "wikilink")：元妃、貴妃、淑妃、德妃、賢妃等為正一品且僅次於皇后。[金宣宗](../Page/完顏珣.md "wikilink")[貞祐年間改為](../Page/貞祐.md "wikilink")[貴妃](../Page/貴妃.md "wikilink")、真妃、淑妃、麗妃、柔妃等。
  - [明朝](../Page/明朝.md "wikilink")：皇帝側室中[皇貴妃為眾側室之首且僅次於皇后](../Page/皇貴妃.md "wikilink")，其次贵妃。贵妃以下，妃级别以寓意[吉祥或](../Page/吉祥.md "wikilink")[美德等字作為](../Page/美德.md "wikilink")[徽號](../Page/徽號.md "wikilink")。
  - [清朝](../Page/清朝.md "wikilink")：[康熙帝定](../Page/康熙帝.md "wikilink")-{制}-，[皇后以下为](../Page/皇后.md "wikilink")[皇貴妃](../Page/皇貴妃.md "wikilink")、[貴妃](../Page/貴妃.md "wikilink")、妃、[嬪](../Page/嬪.md "wikilink")、[貴人](../Page/貴人.md "wikilink")、[常在](../Page/常在.md "wikilink")、[答應](../Page/答應.md "wikilink")。其中妃定員四人。\[1\]不過在康熙以及乾隆朝卻時常出現四妃以上的情況。

:\***康熙三十九年至康熙五十年**，有五妃：德妃、惠妃、榮妃、宜妃、良妃

:\***康熙五十七年起**，有七妃：德妃、惠妃、榮妃、宜妃、和妃、宣妃、成妃

:\***乾隆二十八年**，有六妃：愉妃、慶妃、舒妃、穎妃、忻妃、豫妃

:\***乾隆二十九年至乾隆三十二年**，有五妃：愉妃、慶妃、舒妃、穎妃、豫妃

:\***乾隆三十三年至乾隆三十八年**，有五妃：愉妃、容妃、舒妃、穎妃、豫妃

:\***乾隆四十一年至乾隆四十二年**，有六妃：愉妃、容妃、舒妃、穎妃、惇妃、順妃

:\***乾隆四十三年**，有五妃：愉妃、容妃、穎妃、惇妃、順妃

:\***乾隆四十五年至乾隆五十年**，有五妃：愉妃、容妃、穎妃、惇妃、順妃

## 日本

[平安時代初期以前](../Page/平安時代.md "wikilink")，後宮設有妃的位號，定員二人且位在皇后之下。平安時代中期以後，妃的位號消失，轉由[女御和更衣取代](../Page/女御.md "wikilink")。

## 越南

[越南](../Page/越南.md "wikilink")[阮朝時期](../Page/阮朝.md "wikilink")，國王正配生前只封為王妃，稱一階妃（[越南文](../Page/越南文.md "wikilink")：），死後追封王后。一階妃下一級有二階妃（[越南文](../Page/越南文.md "wikilink")：））。

## 注釋

<references/>

## 參看

  - [王妃](../Page/王妃.md "wikilink")
  - [王大妃](../Page/王大妃.md "wikilink")
  - [太妃](../Page/太妃.md "wikilink")
  - [中宮](../Page/中宮.md "wikilink")
  - [太子妃](../Page/太子妃.md "wikilink")

[\*](../Page/category:妃.md "wikilink")

1.  《[清史稿](../Page/清史稿.md "wikilink")‧后妃》，卷214，曰：「[康熙以後](../Page/康熙帝.md "wikilink")，典制大備。皇后居中宮；皇貴妃一，貴妃二，妃四，嬪六，貴人、常在、答應無定數，分居東、西十二宮」。