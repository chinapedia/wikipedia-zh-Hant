**妖怪**，指草木或者[动物等改变成為的精怪](../Page/动物.md "wikilink")\[1\]\[2\]\[3\]，也指怪异、反常的事物与现象\[4\]\[5\]。
[Shunkosai_Hokuei_Obake.jpg](https://zh.wikipedia.org/wiki/File:Shunkosai_Hokuei_Obake.jpg "fig:Shunkosai_Hokuei_Obake.jpg")\]\]

## 概要

妖怪通常存在於人類想像與[傳說之中](../Page/傳說.md "wikilink")，難以運用科學方法證明其真偽。研究這方面的學問，稱之為**妖怪學**，包含在[民俗學](../Page/民俗學.md "wikilink")、[民族學](../Page/民族學.md "wikilink")、[文化人類學](../Page/文化人類學.md "wikilink")、[語言社會學等學科研究裡](../Page/語言社會學.md "wikilink")，雖不屬於獨自發展的學問，亦是一門有系統的學說。

有關妖怪的起源，已經無從稽考。被稱為妖怪之列的，大多給人的印象都是鬼異邪祟，傾向做壞事為多的一類；有些則給予人神秘莫測，不知是敵是友的感覺。有些则是被当做[神一样尊敬](../Page/神.md "wikilink")，令人崇拜。

在歐洲語言中没有完全對應於妖怪的词汇，僅有意義相近的詞彙，例如英語的
monster（[怪物](../Page/怪物.md "wikilink")）、ghost、spook（[鬼](../Page/鬼.md "wikilink")）、giant（[巨人](../Page/巨人.md "wikilink")）、undead（[不死生物](../Page/不死生物.md "wikilink")）、devil（[恶魔](../Page/恶魔.md "wikilink")）、demon、fiend、evil
spirit（[邪靈](../Page/邪靈.md "wikilink")）、elf（[精灵](../Page/精灵.md "wikilink")）、goblin（[哥布林](../Page/哥布林.md "wikilink")）、bogy、fairy（[小仙子](../Page/小仙子.md "wikilink")）。就像[龙和](../Page/龙.md "wikilink")[龙
(西方)的差异](../Page/龙_\(西方\).md "wikilink")

## 妖怪的學術研究

從古至今，隨著時代的不同，有形形色色的妖怪從人類的生活之中誕生。我們人类所使用的「妖怪」這個名詞，大致上包含了兩種涵義，一種是屬於[文化社會學的範疇](../Page/文化社會學.md "wikilink")，妖怪被認為是一種尚未開化的文化現象，因此妖怪是不存在的，它代表著一切非理性力量的總合。另一種是從[文化人類學](../Page/文化人類學.md "wikilink")、[民俗學的角度來看待妖怪](../Page/民俗學.md "wikilink")，從聚落共同體的農業時代開始，妖怪就已經存在先民們的意識裡，作為天候變異、災禍預知、社會秩序以及[超自然現象的合理化解釋](../Page/超自然現象.md "wikilink")。有些人認為妖怪是實存的，也有人認為是人們的錯覺或者是幻覺所導致的誤判，許多考古研究和生物研究的成果，證明了某些妖怪，其實是古代的罕見生物或科学的现象，列如鬼火被認為是不祥之兆，是鬼魂作祟的現象，但后来被证明是由於人體與動物身體含有磷，屍體腐化時產生磷化氫，當周邊氣溫較高，磷化氫就會於空氣中自燃，產生鬼火現象。但仍有許多未知的妖怪，在科學上以及歷史文獻上仍無法確切地判定其真偽，妖怪可說是致力於民俗學研究的學者們今後努力的方向。

日本受到中國的古代典籍《[山海經](../Page/山海經.md "wikilink")》、《[封神演义](../Page/封神演义.md "wikilink")》、《[西遊記](../Page/西遊記.md "wikilink")》、《[聊齋誌異](../Page/聊齋誌異.md "wikilink")》的深刻影响，在江戶時代出版盛行的時期，陸續編撰記載著奇聞異事的圖文事典，例如《[和漢三才圖繪](../Page/和漢三才圖繪.md "wikilink")》等，加上浮世繪的流行，許多知名畫家紛紛投入妖怪繪卷的製作，如[鳥山石燕](../Page/鳥山石燕.md "wikilink")、[歌川國芳](../Page/歌川國芳.md "wikilink")、[月岡芳年](../Page/月岡芳年.md "wikilink")、[河鍋曉齋](../Page/河鍋曉齋.md "wikilink")、[葛飾北齋等人](../Page/葛飾北齋.md "wikilink")，奠定了日本傳統妖怪的造型基礎。而中國礙於儒家思想的箝制，對於怪力亂神之事敬而遠之，除了上述經典作品的插圖本之外，對於妖怪故事的傳承，大多僅以筆記小說的形式留存，除了文學上的研究之外，並未獨立出來成為單一的民俗研究系統，殊為可惜，近來因[奇幻小說逐漸躍升為大眾文學的主流](../Page/奇幻小說.md "wikilink")，因而中國的妖怪、鬼狐、民間傳說的研究，必然也會跟著受到重視，其結果指日可待。

隨著現代都市文明的發展，妖怪以[都市傳說的形態延續其生命力](../Page/都市傳說.md "wikilink")，例如[廁所裡的花子](../Page/廁所裡的花子.md "wikilink")、[裂口女](../Page/裂口女.md "wikilink")、[人面犬等許多新的妖怪應運而生](../Page/人面犬.md "wikilink")。

## 大眾文化中的妖怪

「妖魔鬼怪」是[動漫](../Page/動漫.md "wikilink")、[電玩](../Page/電玩.md "wikilink")、[神魔小說](../Page/神魔小說.md "wikilink")、[玄幻小說](../Page/玄幻小說.md "wikilink")、[恐怖小說和](../Page/恐怖小說.md "wikilink")[奇幻文學的題材之一](../Page/奇幻文學.md "wikilink")。在舊有的作品裡，妖怪往往是不被[人類或](../Page/人類.md "wikilink")[神仙所容的存在](../Page/神仙.md "wikilink")，不論好壞一律視為仇人趕盡殺絕（像是「照妖鏡」、「斬妖除魔」等等）。因此，妖怪和人或神仙之間常有[暴力衝突](../Page/暴力.md "wikilink")，不過也有表現出包容心的（例如《[倩女幽魂](../Page/倩女幽魂.md "wikilink")》就是以[愛情來解除恩怨的](../Page/愛情.md "wikilink")）。

在現代的作品，尤其於漫畫和電玩，妖怪的致命、恐怖形象有所改變甚至一掃而空，雖仍有部份作品描寫兇殘妖怪和人妖敵對的情節，但有一定數量的作品以人情、奇幻的風格描寫妖怪，甚至是喜劇、熱血、偶像等的手法。

日本漫畫家[水木茂先生曾说](../Page/水木茂.md "wikilink")：「妖怪在人类还没有出现的时候，就已经存在了，或许现在仍存在着妖怪，只是我们不知道妖怪躲在什么地方罢了。」

## 以妖怪作為創作題材的作家

  - [漫畫家](../Page/漫畫家.md "wikilink")

<!-- end list -->

  - [的良米蘭](../Page/的良米蘭.md "wikilink") -
    代表作《[守護貓娘緋鞠](../Page/守護貓娘緋鞠.md "wikilink")》。
  - [水木茂](../Page/水木茂.md "wikilink")（1922年—2015年）-
    代表作《[鬼太郎](../Page/鬼太郎.md "wikilink")》、《[恶魔君](../Page/恶魔君.md "wikilink")》、《[河童三平](../Page/河童三平.md "wikilink")》、《昭和史》。
  - [高橋留美子](../Page/高橋留美子.md "wikilink")（1957年—）-
    代表作《[犬夜叉](../Page/犬夜叉.md "wikilink")》。
  - [楠桂](../Page/楠桂.md "wikilink")（1966年—）-
    代表作《[鬼切丸](../Page/鬼切丸.md "wikilink")》、《[人狼草紙](../Page/人狼草紙.md "wikilink")》。
  - [真倉翔原作](../Page/真倉翔.md "wikilink")，[岡野剛漫畫](../Page/岡野剛.md "wikilink")
    -
    代表作《[靈異教師神眉](../Page/靈異教師神眉.md "wikilink")》、《[靈媒師東名](../Page/靈媒師東名.md "wikilink")》。
  - [今市子](../Page/今市子.md "wikilink") -
    代表作《[百鬼夜行抄](../Page/百鬼夜行抄.md "wikilink")》。
  - [冬目景](../Page/冬目景.md "wikilink") -
    代表作《[文车馆来访记](../Page/文车馆来访记.md "wikilink")》。
  - [綠川幸](../Page/綠川幸.md "wikilink") -
    代表作《[夏目友人帳](../Page/夏目友人帳.md "wikilink")》。
  - [佐藤友生](../Page/佐藤友生.md "wikilink") -
    代表作《[妖怪醫生](../Page/妖怪醫生.md "wikilink")》。
  - [椎橋寬](../Page/椎橋寬.md "wikilink") -
    代表作《[滑頭鬼之孫](../Page/滑頭鬼之孫.md "wikilink")》。
  - [永井豪](../Page/永井豪.md "wikilink") -
    《[鬼公子炎魔](../Page/鬼公子炎魔.md "wikilink")》。
  - [池田晃久](../Page/池田晃久.md "wikilink") -
    《[十字架与吸血姬](../Page/十字架与吸血姬.md "wikilink")》。
  - [安田典生](../Page/安田典生.md "wikilink") -
    《[夜樱四重奏](../Page/夜樱四重奏.md "wikilink")》。
  - [楳圖一雄](../Page/楳圖一雄.md "wikilink") -
    《[猫目小僧](../Page/猫目小僧.md "wikilink")》。
  - [鈴木JULIETTA](../Page/鈴木JULIETTA.md "wikilink")-《[元氣少女緣結神](../Page/元氣少女緣結神.md "wikilink")》。
  - [妖怪手錶](../Page/妖怪手錶.md "wikilink")

<!-- end list -->

  - 小說家

<!-- end list -->

  - [上田秋成](../Page/上田秋成.md "wikilink")（1734年—1809年）-
    代表作《[雨月物語](../Page/雨月物語.md "wikilink")》。[文學家](../Page/文學家.md "wikilink")。
  - [小泉八雲](../Page/小泉八雲.md "wikilink")（1850年—1904年）-
    代表作《[怪談](../Page/怪談.md "wikilink")》。[英文學者](../Page/英文學者.md "wikilink")。
  - [京極夏彥](../Page/京極夏彥.md "wikilink")（1963年—）-
    《[巷說百物語](../Page/巷說百物語.md "wikilink")》、《[姑獲鳥之夏](../Page/姑獲鳥之夏.md "wikilink")》。妖怪研究者。
  - [畠中恵](../Page/畠中恵.md "wikilink")（1970年4月13日—）。
  - [結城光流](../Page/結城光流.md "wikilink")（2001年12月－）《少年陰陽師》

<!-- end list -->

  - 其他

<!-- end list -->

  - [鳥山石燕](../Page/鳥山石燕.md "wikilink")（1712年—1788年）-
    代表作《[畫圖百鬼夜行](../Page/畫圖百鬼夜行.md "wikilink")》。[浮世繪畫家](../Page/浮世繪畫家.md "wikilink")。
  - [井上圆了](../Page/井上圆了.md "wikilink")（1858年—1919年）-
    日本[佛教](../Page/佛教.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")。
  - [南方熊楠](../Page/南方熊楠.md "wikilink")（1867年—1941年）。
  - [柳田国男](../Page/柳田国男.md "wikilink")（1875年—1962年）-
    日本妖怪[民俗学家](../Page/民俗学家.md "wikilink")。
  - [江馬務](../Page/江馬務.md "wikilink")（1884年—1979年）。
  - [岩井宏实](../Page/岩井宏实.md "wikilink")（1932年—）-
    日本[民俗学家](../Page/民俗学家.md "wikilink")。
  - [宮田登](../Page/宮田登.md "wikilink")（1936年—2000年）- 日本民俗学家。
  - [小松和彦](../Page/小松和彦.md "wikilink")（1947年—）-
    [文化人类学者](../Page/文化人类学者.md "wikilink")、[民俗学者](../Page/民俗学.md "wikilink")。专门研究[妖怪论](../Page/妖怪论.md "wikilink")、[巫术](../Page/巫术.md "wikilink")、[民间信仰](../Page/民间信仰.md "wikilink")。
  - [多田克己](../Page/多田克己.md "wikilink")（1961年—）- 妖怪研究家。
  - [歌川國芳](../Page/歌川國芳.md "wikilink")
  - [月岡芳年](../Page/月岡芳年.md "wikilink")
  - [竹原春泉](../Page/竹原春泉.md "wikilink")
  - [平田篤胤](../Page/平田篤胤.md "wikilink")
  - [山谷先生](../Page/山谷先生.md "wikilink")
  - [木原浩勝](../Page/木原浩勝.md "wikilink")
  - [草野巧](../Page/草野巧.md "wikilink")

## 相關作品

<div class="references-small" style="-moz-column-count:4; column-count:4;">

  -
    动漫画

<!-- end list -->

  - [深邃美丽的亚细亚](../Page/深邃美丽的亚细亚.md "wikilink")
  - [大唐玄筆錄](../Page/大唐玄筆錄.md "wikilink")
  - [潮與虎](../Page/潮與虎.md "wikilink")
  - [閻小妹](../Page/閻小妹.md "wikilink")
  - [怪博士與機器娃娃](../Page/怪博士與機器娃娃.md "wikilink")
  - [守護貓娘緋鞠](../Page/守護貓娘緋鞠.md "wikilink")
  - [夏目友人账](../Page/夏目友人账.md "wikilink")
  - [物怪](../Page/物怪.md "wikilink")
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")
  - [怪物王女](../Page/怪物王女.md "wikilink")
  - [鬼太郎](../Page/鬼太郎.md "wikilink")
  - [魔法公主](../Page/魔法公主.md "wikilink")
  - [神隱少女](../Page/神隱少女.md "wikilink")
  - [捉鬼天狗帮](../Page/捉鬼天狗帮.md "wikilink")
  - [犬夜叉](../Page/犬夜叉.md "wikilink")
  - [少年阴阳师](../Page/少年阴阳师.md "wikilink")
  - [妖逆門](../Page/妖逆門.md "wikilink")
  - [地狱少女](../Page/地狱少女.md "wikilink")
  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")
  - [×××HOLiC](../Page/×××HOLiC.md "wikilink")
  - [化物语](../Page/化物语.md "wikilink")
  - [貓目小僧](../Page/貓目小僧.md "wikilink")
  - [子不语 (漫画)](../Page/子不语_\(漫画\).md "wikilink")
  - [咚隆隆炎魔君](../Page/咚隆隆炎魔君.md "wikilink")
  - [雨月](../Page/雨月.md "wikilink")
  - [妖怪醫生](../Page/妖怪醫生.md "wikilink")
  - [結界師](../Page/結界師.md "wikilink")
  - [河童之夏](../Page/河童之夏.md "wikilink")
  - [東方Project](../Page/東方Project.md "wikilink")
  - [平成狸合戰](../Page/平成狸合戰.md "wikilink")
  - [虫师](../Page/虫师.md "wikilink")
  - [蓝兰岛漂流记](../Page/蓝兰岛漂流记.md "wikilink")
  - [我家有个狐仙大人](../Page/我家有个狐仙大人.md "wikilink")
  - [元氣少女緣結神](../Page/元氣少女緣結神.md "wikilink")
  - [妖狐×僕SS](../Page/妖狐×僕SS.md "wikilink")
  - [流浪神差](../Page/流浪神差.md "wikilink")
  - [怪 ～ayakashi～](../Page/怪_～ayakashi～.md "wikilink")
  - [屍體派對](../Page/屍體派對.md "wikilink")
  - [Another](../Page/Another.md "wikilink")
  - [幽遊白書](../Page/幽遊白書.md "wikilink")
  - [妖怪公寓的優雅日常](../Page/妖怪公寓的優雅日常.md "wikilink")
  - [不愉快的妖怪庵](../Page/不愉快的妖怪庵.md "wikilink")
  - [妖怪旅館營業中](../Page/妖怪旅館營業中.md "wikilink")

<!-- end list -->

  -
    特摄与电影

<!-- end list -->

  - [忍者战队隐连者](../Page/忍者战队隐连者.md "wikilink")
  - [侍战队真剑者](../Page/侍战队真剑者.md "wikilink")
  - [假面骑士响鬼](../Page/假面骑士响鬼.md "wikilink")
  - [白狮子假面](../Page/白狮子假面.md "wikilink")
  - [超神三战士](../Page/超神三战士.md "wikilink")
  - [手裏劍戰隊忍忍者](../Page/手裏劍戰隊忍忍者.md "wikilink")
  - [妖怪大战争](../Page/妖怪大战争.md "wikilink")
  - [大眼睛](../Page/大眼睛_\(特摄\).md "wikilink")
  - [妖怪百物语](../Page/妖怪百物语.md "wikilink")
  - [怪物的孩子](../Page/怪物的孩子.md "wikilink")
  - [多罗罗](../Page/多罗罗.md "wikilink")
  - [咯咯咯鬼太郎 (电影)](../Page/咯咯咯鬼太郎_\(电影\).md "wikilink")

<!-- end list -->

  -
    遊戲

<!-- end list -->

  - [仙劍奇俠傳](../Page/仙劍奇俠傳.md "wikilink")
  - [軒轅劍](../Page/軒轅劍.md "wikilink")
  - [妖怪百姬](../Page/妖怪百姬.md "wikilink")
  - [妖怪手錶](../Page/妖怪手錶.md "wikilink")
  - [陰陽師](../Page/陰陽師.md "wikilink")

</div>

## 參考文獻

<div class="references-small">

  - 其他參考文獻

<!-- end list -->

1.  [江馬務著](../Page/江馬務.md "wikilink")《日本妖怪變化史》

2.  [柳田國男著](../Page/柳田國男.md "wikilink")《妖怪談義》[講談社](../Page/講談社.md "wikilink")［講談社学術文庫］。ISBN
    978-4-06-158135-7

3.  [小松和彥著](../Page/小松和彥.md "wikilink")《日本妖怪異聞錄》[小學館](../Page/小學館.md "wikilink")［小学館ライブラリー］。ISBN
    978-4-09-460073-5

4.  [小松和彥著](../Page/小松和彥.md "wikilink")《妖怪學新考――從妖怪看日本人的心理》小学館ライブラリー。ISBN
    978-4-09-460132-9

5.  [小松和彥編](../Page/小松和彥.md "wikilink")《日本妖怪学大全》小學館。ISBN
    978-4-09-626208-5

6.  [小松和彥著](../Page/小松和彥.md "wikilink")《妖怪文化入門》せりか書房。ISBN
    978-4-7967-0271-3

7.  [井上圓了著](../Page/井上圓了.md "wikilink")，[蔡元培譯](../Page/蔡元培.md "wikilink")《妖怪學》。

8.  香川雅信著『江戸の妖怪革命』[河出書房新社](../Page/河出書房新社.md "wikilink")。ISBN
    978-4-309-22433-6

9.  小松和彦著『怪異・妖怪百物語　－異界の杜への誘い－』[明治書院](../Page/明治書院.md "wikilink")。ISBN
    978-4-625-68363-3

10.
11.

</div>

## 參看

  - [民俗學](../Page/民俗學.md "wikilink")
  - [神秘生物學](../Page/神秘生物學.md "wikilink")
  - [史诗](../Page/史诗.md "wikilink")、[博物誌](../Page/博物誌.md "wikilink")、[笔记小说](../Page/笔记小说.md "wikilink")、[民间传说](../Page/民间传说.md "wikilink")、[都市傳說](../Page/都市傳說.md "wikilink")
  - [神话](../Page/神话.md "wikilink")、[宗教](../Page/宗教.md "wikilink")
  - [科幻](../Page/科幻.md "wikilink")、[奇幻](../Page/奇幻.md "wikilink")
  - [童话](../Page/童话.md "wikilink")
  - [超常现象](../Page/超常现象.md "wikilink")
  - [日本妖怪列表](../Page/日本妖怪列表.md "wikilink")
  - [中國妖怪列表](../Page/中國妖怪列表.md "wikilink")
  - [世界妖怪列表](../Page/世界妖怪列表.md "wikilink")
  - [台湾妖怪列表](../Page/台湾妖怪列表.md "wikilink")
  - [妖怪著作列表](../Page/妖怪著作列表.md "wikilink")
  - [傳說生物](../Page/傳說生物.md "wikilink")、[宇宙人](../Page/宇宙人.md "wikilink")、[宇宙生物](../Page/宇宙生物.md "wikilink")、[神](../Page/神.md "wikilink")

## 外部連結

  - [扶桑的妖怪传说](http://www.17k.com/html/books/0/5/510/51066/c646a8/1859855.shtml)

  - [幻想動物の事典](http://www.toroia.info/)

  - [幻想学事典](https://web.archive.org/web/20100725202433/http://www004.upp.so-net.ne.jp/thor/fd_top.htm)

  - [幻想世界神話辞典](http://www.jiten.info/)

  - [神魔精妖名辞典](https://web.archive.org/web/20091006031334/http://dug.main.jp/sinma/leftex.htm)

  - [世界神話事典](http://www.pandaemonium.net/rdb/index.html)

  - [日本物怪観光 -
    妖怪学面白講話](https://web.archive.org/web/20110714112703/http://www.mononokekanko.com/)

  - [宗優子 妖怪キッズ](http://www7a.biglobe.ne.jp/~youkai-kids/index.html)

  - [和漢百魅缶](http://www10.plala.or.jp/cotton-candy/momomikan.htm)（[クロヌシカガミ](http://www10.plala.or.jp/cotton-candy/)内）

  - [ゲゲゲの鬼太郎　妖怪大事典](https://web.archive.org/web/20090922061009/http://www1.ocn.ne.jp/~mispla/kitaro-list/kitaro-yokai.html)

  - [*The Obakemono Project*: Eine Datenbank über Obake und
    Yōkai](http://www.obakemono.com/)

  - [Youkai and
    Kaidan](https://web.archive.org/web/20101124012034/http://k-i-a.or.jp/kokusai/jigyou/english-lesson/ts-report/r-report.pdf)
    ([PDF file](../Page/Portable_Document_Format.md "wikilink"))

  - [Tales of Ghostly
    Japan](http://www.seekjapan.jp/article-2/766/Tales+of+Ghostly+Japan)

  - [Hyakumonogatari.com](http://hyakumonogatari.com/category/yokai/)
    Translated yokai stories from Hyakumonogatari.com

  - [The Ooishi Hyoroku Monogatari Picture
    Scroll](https://web.archive.org/web/20090417015138/http://www.rekihaku.ac.jp/e-rekihaku/106/index.html)

  - [1](http://youkai.wikia.com/)

[妖怪](../Category/妖怪.md "wikilink")

1.  《搜神记》卷六：“妖怪者，盖精气之依物者也。气乱于中，物变于外。”
2.  许慎《说文解字》：“魅，老精物也。”
3.  王充《论衡》:“物之老者，其精为人。”
4.  《楚辞》南方民族的卜筮祭文。如果地上出现违反正常状态的事物，就命名为“妖”。
5.  《左传‧宣公十五年》：“天反时为灾，地反物为妖。”