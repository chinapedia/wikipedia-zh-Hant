是[袁祥仁執導的](../Page/袁祥仁.md "wikilink")[電影作品](../Page/電影.md "wikilink")，也是他自行演出之作品，由[王晶編成一部具有搞笑](../Page/王晶_\(導演\).md "wikilink")、恐怖的劇情片。本片依[香港電影分級制度](../Page/香港電影分級制度.md "wikilink")（即：兒童不宜法例）是屬於[第三級](../Page/第三級.md "wikilink")，非18歲以上人士，請選擇剪輯版收看。

## 劇情概要

乃密（[周比利](../Page/周比利.md "wikilink")
飾）與金莎（[徐曼華](../Page/徐曼華.md "wikilink")
飾）練成「陰陽屍」，為了尋找仇人林正（[林正英](../Page/林正英.md "wikilink")
飾）與十靈時出生的人，從[泰國來到](../Page/泰國.md "wikilink")[香港](../Page/香港.md "wikilink")，陰陽屍找上阿正時，卻意外得知阿正的女兒（[陳雅倫](../Page/陳雅倫.md "wikilink")
飾）是十靈時出生，逃脫後來到警局報案，陰陽屍不斷吸取人[腦來得知他們下落](../Page/腦.md "wikilink")，阿正帶著大B（[張國強](../Page/張國強_\(香港\).md "wikilink")
飾）與小婷找一位命理師（[胡楓](../Page/胡楓.md "wikilink")
飾）幫助尋找十靈時出生的處男，意外得知大B就是要與小婷[交合的對象](../Page/交配.md "wikilink")，但兩人彼此常吵架，始終不願聽從，最後在生死交關之時，在阿正的苦勸與母親（[朱咪咪](../Page/朱咪咪.md "wikilink")
飾）苦心營造氣氛，兩人才逐漸投入情感…

## 演員

  - [林正英](../Page/林正英.md "wikilink") 飾 林正
  - [周比利](../Page/周比利.md "wikilink") 飾 乃密
  - [張國強](../Page/張國強_\(香港\).md "wikilink") 飾 大B 本名 張國強
  - [陳雅倫](../Page/陳雅倫.md "wikilink") 飾 林小婷

## 外部連結

  - {{@movies|fwhk40105868}}

  -
  -
  -
  -
[Category:香港恐怖片](../Category/香港恐怖片.md "wikilink")
[2](../Category/1990年代香港電影作品.md "wikilink")