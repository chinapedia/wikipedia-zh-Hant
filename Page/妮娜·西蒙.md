[The-Essential-Nina-Simone.jpg](https://zh.wikipedia.org/wiki/File:The-Essential-Nina-Simone.jpg "fig:The-Essential-Nina-Simone.jpg")
**妮娜·西蒙**（Nina
Simone，），是一位美國歌手、作曲家與[鋼琴表演家](../Page/鋼琴.md "wikilink")。原名**尤妮斯·凱瑟林·偉蒙**（Eunice
Kathleen
Waymon）、以藝名妮娜·西蒙著稱。雖然一般人認為她是一位優秀的[爵士女伶](../Page/爵士.md "wikilink")、但妮娜不喜歡被這樣歸類。她的創作歌曲類型主要包括有[藍調](../Page/藍調.md "wikilink")、[節奏藍調和](../Page/節奏藍調.md "wikilink")[靈魂樂](../Page/靈魂樂.md "wikilink")。她的演唱方式則以富有情感、帶有氣息的變化音（tremolo）為主要特色。她的名字有時也譯做**妮娜·席夢**。

## 童年

西蒙出生於[南卡羅萊納州的](../Page/南卡羅萊納州.md "wikilink")[崔恩鎮](../Page/崔恩鎮.md "wikilink")（Tryon），家中共有八個孩子，她就像當時的非裔孩童一樣，喜歡在[教會裡唱](../Page/教會.md "wikilink")[福音聖歌](../Page/福音.md "wikilink")，並受到了聲樂家[瑪麗安·安德森](../Page/瑪麗安·安德森.md "wikilink")（Marian
Anderson）的啟蒙。在西蒙的童年時期，她就開始學鋼琴，並顯現出她卓越的演奏天份，在她10歲公開演奏時，西蒙的父母一反當時種族順位的慣例（凡不是白人者，一律都坐在後排位置），往前坐到了前排的位置，而且拒絕換位到後面，這件事情讓西蒙後來加入了美國[種族平權的活動](../Page/種族平權.md "wikilink")。

## 就學

17歲時，西蒙搬到了[賓州的](../Page/賓州.md "wikilink")[費城](../Page/費城.md "wikilink")。並開始教鋼琴，同時也做一些伴奏歌手的工作，當時她曾獲[紐約市知名的](../Page/紐約市.md "wikilink")[朱利亞德音樂學院入學許可](../Page/朱利亞德音樂學院.md "wikilink")，但由於她籌不足學費，也找不到有力的保證人，只好放棄入學（同時也放棄了她很有可能是美國第一位非裔鋼琴演奏家的希望）。後來西蒙又向另一家科提斯音樂學院（Curtis
Institute）申請入學，主修鋼琴，但在面試時被拒絕了。西蒙曾說，她認為被拒的原因有很大一部份在於她的膚色。

## 爵士生涯

1954年，西蒙21歲時，她正式以藝名「妮娜西蒙」在大西洋城夜總會登台演唱爵士與藍調歌曲。「妮娜」（Nina）是她男友稱她的[西班牙語小名](../Page/西班牙語.md "wikilink")（在西班牙語中，Nina代表了「女孩」）；「西蒙」則是取自於[法國知名女演員](../Page/法國.md "wikilink")[西蒙娜·西戈諾雷特](../Page/西蒙娜·西戈諾雷特.md "wikilink")（Simone
Signoret）的名字。

1959年，西蒙以《我愛你，波吉》（I Loves You
Porgy）一曲中的哀傷動人唱腔，獲得了爵士樂界的注目。《我愛你，波吉》是歌劇家[喬治·蓋希文在](../Page/喬治·蓋希文.md "wikilink")[歌劇](../Page/歌劇.md "wikilink")《[波吉與貝絲](../Page/波吉與貝絲.md "wikilink")》中的著名歌曲。西蒙並因此曲而成為美國唱片銷售排行前40位著名的爵士歌手。隨後西蒙又唱出了《我的孩子只想我》（My
Baby Just Cares for
Me）的著名單曲。此曲在1980年代在[英國是廣為人知的](../Page/英國.md "wikilink")[香奈兒](../Page/香奈兒.md "wikilink")[五號香水廣告曲](../Page/五號香水.md "wikilink")。

1960年代，西蒙積極參與種族平權運動，演唱了許多有關敘述非裔美國人在種族不平權下生活的歌曲。如：

  - 知名的《要年輕、有天賦和膚色黑》（To Be Young, Gifted and
    Black；此曲被美國靈魂歌后[艾瑞莎·弗蘭克林在](../Page/艾瑞莎·弗蘭克林.md "wikilink")1980年代廣為傳唱）
  - 《剝削的憂傷》（Backlash Blues）
  - 《天殺的密西西比》（Mississippi
    Goddam；這首歌是針對當時一連串針對非裔美人的[謀殺案件所寫的](../Page/謀殺.md "wikilink")，包括有民權領袖[麥格·艾佛斯Medgar](../Page/麥格·艾佛斯.md "wikilink")
    Evers被暗殺，伯明罕教會炸彈案與[阿拉巴馬州四位黑人小孩被謀殺的案件](../Page/阿拉巴馬州.md "wikilink")）
  - 《希望我知道自由的滋味》（I Wish I Knew How it Would Feel to be Free）
  - 《海盜珍妮》（Pirate Jenny；由[柯特·懷爾作曲](../Page/柯特·懷爾.md "wikilink")）

1961年，西蒙發行了一張民歌風格的唱片《日昇之屋》（House of the Rising
sun），這張唱片後來被搖滾歌手[鮑伯·狄倫](../Page/鮑伯·狄倫.md "wikilink")
Bob Dylan又翻唱之後，變成動物保護行動的主題曲。此外亦有知名歌曲如《我對你下了些魔法》（I Put a Spell on
You；原唱是Screamin' Jay Hawkins），《四位女人》（Four Women）、《我將釋放》（I shall
be released）與《我活著》（Aint got no (I got life)）。

在西蒙走紅的這段時期，她以藝術家般的演出令人印象深刻，沒有豪華的爵士大樂隊，而只有[民歌般簡單的演奏團體](../Page/民歌.md "wikilink")。在演唱時，西蒙能夠很快的從福音美聲唱腔轉為爵士藍調的憂鬱唱腔，並在許多爵士歌曲中加入了歐洲古典樂與[賦格的唱法](../Page/賦格.md "wikilink")，如知名的《我們都知道》（For
All We Know）。

1968年電影《[天羅地網
(1968年電影)](../Page/天羅地網_\(1968年電影\).md "wikilink")》中，選入了西蒙的歌《罪人》（Sinner
Man），使得她的知名度廣為擴散。（此電影在1999年由[皮爾斯·布洛斯南重演](../Page/皮爾斯·布洛斯南.md "wikilink")）

1971年，西蒙離開美國，原因有很多，包括了與經紀人、唱片商的不合，也包括了稅務不清與種族活動的各種爭議。西蒙在1978年回到美國，隨後因逃稅而被逮捕了一陣子（她曾在[越戰時拒絕繳稅了幾年](../Page/越戰.md "wikilink")），她曾住在一些不同的國家，如加勒比、非洲、歐洲，以60歲的高齡仍在[朗尼·史葛於](../Page/朗尼·史葛.md "wikilink")[倫敦開辦的](../Page/倫敦.md "wikilink")[朗尼史葛爵士俱樂部中演唱](../Page/朗尼史葛爵士樂俱樂部.md "wikilink")。

## 晚年

[I-Put-A-Spell-on-You.jpg](https://zh.wikipedia.org/wiki/File:I-Put-A-Spell-on-You.jpg "fig:I-Put-A-Spell-on-You.jpg")
1995年，西蒙被報導以玩具BB槍射擊鄰居小孩，原因只是因為小孩笑聲吵鬧到她的專注，因此揭露了西蒙在音樂世界中難相處、難以協調的一些特性，雖然她的舞台作風的確有些傲慢而且疏離，但西蒙對這些評論都給與正面的回應。在西蒙的晚年，她傳出許多與歌迷戀愛而且[訂婚的逸事](../Page/訂婚.md "wikilink")，也有許多趣聞，基於她時而幽默、時而權威的演唱作風，她得到了一個「靈魂教母」（High
Priestess of Soul）稱號。

她的女兒西蒙（Simone／Lisa Celeste Stroud／Lisa Simone
Kelly）是歌手兼舞台劇演員，曾在紐約[百老匯舞劇](../Page/百老匯.md "wikilink")《[阿依達](../Page/阿依達.md "wikilink")》中演出。

西蒙的自傳《我對你下了些魔法》（I Put a Spell on You ISBN
0-306-80525-1）在1992年出版。1993年她開始定居在[法國南部的Aix](../Page/法國.md "wikilink")-en-Provence。2003年她在睡夢中去世，地點是Carry-le-Rouet。1993年後有不少電影採用了她的歌曲，包括1993年的《[一觸即發](../Page/一觸即發.md "wikilink")》、1996年的《[羅密歐與茱麗葉](../Page/羅密歐與茱麗葉後現代激情篇.md "wikilink")》、1999年新版的《[天羅地網
(1999年電影)](../Page/天羅地網_\(1999年電影\).md "wikilink")》、2006年的《[內陸帝國](../Page/內陸帝國.md "wikilink")》、2011年的《[逆轉人生](../Page/逆轉人生.md "wikilink")》和2015年的《[紳士密令](../Page/紳士密令.md "wikilink")》，使得西蒙的歌開始受到一些年輕世代的喜愛。以她為題材的紀錄片《[妮娜西蒙：女伶的靈魂](../Page/妮娜西蒙：女伶的靈魂.md "wikilink")》（What
Happened, Miss
Simone？）則入圍了2016年第88屆[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")。

## 西蒙的名句

  - 爵士是白人對黑人音樂的用詞。我說我的音樂是黑色古典樂。Jazz is a white term to define Black
    people. My music is Black classical music.

## 外部連結

  - [官方網站](https://web.archive.org/web/20050613075626/http://ninasimone.com/welcome.html)
  - [L'hommage: Nina Simone - Tribute and Archival
    site](https://web.archive.org/web/20050404202732/http://www.high-priestess.com/index.html)
  - [BBC的專評](http://news.bbc.co.uk/1/hi/entertainment/2965225.stm)
  - [Mark Anthony
    Neal描寫西蒙的文章](https://web.archive.org/web/20070715151155/http://www.seeingblack.com/2003/x060403/nina_simone.shtml)

[Category:美国女歌手](../Category/美国女歌手.md "wikilink")
[Category:美國鋼琴家](../Category/美國鋼琴家.md "wikilink")
[Category:女性鋼琴家](../Category/女性鋼琴家.md "wikilink")
[Category:非洲裔美國音樂家](../Category/非洲裔美國音樂家.md "wikilink")
[Category:南卡羅來納州人](../Category/南卡羅來納州人.md "wikilink")
[Category:美国人权活动家](../Category/美国人权活动家.md "wikilink")
[Category:爵士歌手](../Category/爵士歌手.md "wikilink")
[Category:節奏藍調歌手](../Category/節奏藍調歌手.md "wikilink")
[Category:靈魂樂歌手](../Category/靈魂樂歌手.md "wikilink")
[Category:美国民权活动家](../Category/美国民权活动家.md "wikilink")
[S](../Category/搖滾名人堂入選者.md "wikilink")