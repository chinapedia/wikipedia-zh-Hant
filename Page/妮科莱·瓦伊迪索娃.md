**妮科莱·瓦伊迪索娃**（，），出生於[德國](../Page/德國.md "wikilink")[紐倫堡](../Page/紐倫堡.md "wikilink")，是[捷克](../Page/捷克.md "wikilink")[網球運動員](../Page/網球.md "wikilink")，现已退役。

由于幼年时成长于德国，而且当时正是德国女子网球传奇人物[格拉芙的巅峰时期](../Page/格拉芙.md "wikilink")，所以瓦伊迪索娃自幼十分崇拜格拉芙。

瓦伊迪索娃六歲開始打網球，是[博萊蒂耶里](../Page/博萊蒂耶里.md "wikilink")（）的學生。2003年起轉入職業比賽。2006年8月7日，她的[WTA世界排名達到第](../Page/女子网球联合会.md "wikilink")9，成為進身前10名第十二年輕的球手，時為17歲3個月又兩星期。

由於她分別在德國和[美國生活了一段日子](../Page/美國.md "wikilink")，因此她在說話的時候帶有這兩種混合[口音](../Page/口音.md "wikilink")。

## 外部連結

  -
  -
  - [Fed Cup - Player profile - Nicole VAIDISOVA
    (CZE)](http://www.fedcup.com/en/players/player/profile.aspx?playerid=100022661)

  - <http://nicolevaidisovaforum.com>

  - <https://web.archive.org/web/20140418090515/http://nicole-vaidisova.org/>

[Category:捷克女子网球运动员](../Category/捷克女子网球运动员.md "wikilink")