[Mwai_Kibaki_&_wife_at_White_House,_October_2003.jpg](https://zh.wikipedia.org/wiki/File:Mwai_Kibaki_&_wife_at_White_House,_October_2003.jpg "fig:Mwai_Kibaki_&_wife_at_White_House,_October_2003.jpg")訪問\]\]
**姆瓦伊·齐贝吉**（**Mwai
Kibaki**，），[肯尼亚政治人物](../Page/肯尼亚.md "wikilink")，[肯尼亚全国彩虹同盟成员](../Page/肯尼亚全国彩虹同盟.md "wikilink")，曾任肯尼亚财政部长、卫生部长、[肯尼亚副总统](../Page/肯尼亚副总统.md "wikilink")，2002年12月28日至2013年4月9日任[肯尼亚总统](../Page/肯尼亚总统.md "wikilink")。

## 教育

齐贝吉畢業於[烏干達](../Page/烏干達.md "wikilink")[麥克雷雷大學](../Page/马凯雷雷大学.md "wikilink")，攻讀[經濟學](../Page/經濟學.md "wikilink")、[政治學和](../Page/政治學.md "wikilink")[歷史](../Page/歷史.md "wikilink")。畢業後，他遠赴[英國進修](../Page/英國.md "wikilink")，在[倫敦政治經濟學院修讀](../Page/倫敦政治經濟學院.md "wikilink")[公共財政](../Page/公共財政.md "wikilink")。然後回國在麥克雷雷大學擔任經濟系教授。\[1\]

## 政治

他在1978－1988擔任肯尼亚副總統，曾任财政部长、卫生部长，后由政見不和與[丹尼尔·阿拉普·莫伊不和](../Page/丹尼尔·阿拉普·莫伊.md "wikilink")，被罷黜，他其後成為反對派成員之一。

2002年總統選舉，他領導反對派肯尼亚全国彩虹同盟在大選中獲勝，擊敗執政黨候選人[乌胡鲁·肯雅塔](../Page/乌胡鲁·肯雅塔.md "wikilink")，結束[肯尼亚非洲民族联盟長達四十年的執政](../Page/肯尼亚非洲民族联盟.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[K](../Category/肯尼亚总统.md "wikilink")
[K](../Category/肯尼亚副总统.md "wikilink")
[K](../Category/倫敦政治經濟學院校友.md "wikilink")
[K](../Category/马凯雷雷大学校友.md "wikilink")
[K](../Category/基庫尤人.md "wikilink")

1.  [肯尼亞總統齊貝吉](http://news.xinhuanet.com/misc/2002-12/30/content_674885.htm),新華網