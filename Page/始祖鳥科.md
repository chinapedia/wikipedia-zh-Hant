**始祖鳥科**（[學名](../Page/學名.md "wikilink")：）是活於[侏羅紀及](../Page/侏羅紀.md "wikilink")[白堊紀的一](../Page/白堊紀.md "wikilink")[科早期](../Page/科.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。其下包含了最為知名的[始祖鳥](../Page/始祖鳥.md "wikilink")。始祖鳥科與其他早期鳥類不同的是其長及骨質的尾巴，一些[物種甚至有較長的第二趾](../Page/物種.md "wikilink")。

## 分類

[Solnhofen_Specimen.jpg](https://zh.wikipedia.org/wiki/File:Solnhofen_Specimen.jpg "fig:Solnhofen_Specimen.jpg")的[化石標本](../Page/化石.md "wikilink")。\]\]
始祖鳥科屬於獨有的[始祖鳥目](../Page/始祖鳥目.md "wikilink")，最初其下只有[始祖鳥一](../Page/始祖鳥.md "wikilink")[屬](../Page/屬.md "wikilink")。當未有任何[種系發生學的定義時](../Page/種系發生學.md "wikilink")，始祖鳥科是一個包含了所有接近始祖鳥而又疏於[新鳥亞綱](../Page/新鳥亞綱.md "wikilink")[鳥類的](../Page/鳥類.md "wikilink")[分支](../Page/分支.md "wikilink")。\[1\]

[馳龍科傳統上被認為是非鳥類的](../Page/馳龍科.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，也被分類到始祖鳥目中。\[2\]後來發現的一些原始形態令早期鳥類的關係變得模糊，使[伶盜龍及相似的恐龍都有可能是鳥類之一](../Page/伶盜龍.md "wikilink")。[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S.
Paul）將馳龍類分類到始祖鳥目中，但大部份[支序分類學的研究都認為牠們是在](../Page/支序分類學.md "wikilink")[鳥綱之外](../Page/鳥綱.md "wikilink")。\[3\]

[Archiesizeall1.png](https://zh.wikipedia.org/wiki/File:Archiesizeall1.png "fig:Archiesizeall1.png")的大小。\]\]
[原始祖鳥最初被認為是屬於始祖鳥目](../Page/原始祖鳥.md "wikilink")，但大部份[古生物學家都認為牠們是屬於](../Page/古生物學家.md "wikilink")[偷蛋龍下目](../Page/偷蛋龍下目.md "wikilink")。其他的如[侏鳥及](../Page/侏鳥.md "wikilink")[烏禾爾龍等都有可能是始祖鳥的](../Page/烏禾爾龍.md "wikilink")[異名或不是屬於同一類](../Page/異名.md "wikilink")。[金鳳鳥起初被認為是屬於始祖鳥類](../Page/金鳳鳥.md "wikilink")，但後來卻確認是[傷齒龍科](../Page/傷齒龍科.md "wikilink")。\[4\]\[5\]\[6\]

## 參考文献

[始祖鳥目](../Category/始祖鳥目.md "wikilink")
[始祖鳥科](../Category/始祖鳥科.md "wikilink")
[Archaeopterygidae](../Category/鳥翼類.md "wikilink")

1.

2.
3.

4.

5.

6.