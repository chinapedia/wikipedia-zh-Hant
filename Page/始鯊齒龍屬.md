[Eocarcharia_Sil3.PNG](https://zh.wikipedia.org/wiki/File:Eocarcharia_Sil3.PNG "fig:Eocarcharia_Sil3.PNG")的體型比較圖\]\]
**始鯊齒龍屬**（[學名](../Page/學名.md "wikilink")：*Eocarcharia*）屬於[獸腳亞目](../Page/獸腳亞目.md "wikilink")[鯊齒龍科](../Page/鯊齒龍科.md "wikilink")，生存於1億1000萬年前的[撒哈拉](../Page/撒哈拉.md "wikilink")。始鯊齒龍的化石，是在2000年由[芝加哥大學的古生物學家](../Page/芝加哥大學.md "wikilink")[保羅·塞里諾在](../Page/保羅·塞里諾.md "wikilink")[尼日發現](../Page/尼日.md "wikilink")。目前只有唯一種**怒眼始鯊齒龍**（*E.
dinops*）\[1\]。牠們的牙齒呈[匕首狀](../Page/匕首.md "wikilink")，可用來咬入獵物身體，撕下肉塊。牠們的眼睛上方有明顯的隆起，這也是牠們的種名來源\[2\]。根據估計，始鯊齒龍的身長約8到12公尺。

在尼日的[艾爾雷茲組](../Page/艾爾雷茲組.md "wikilink")（Elrhaz
Formation，又名Echkar組）地層，還發現[獸腳類的](../Page/獸腳類.md "wikilink")[隱面龍與](../Page/隱面龍.md "wikilink")[似鱷龍](../Page/似鱷龍.md "wikilink")、[蜥腳類的](../Page/蜥腳類.md "wikilink")[尼日龍](../Page/尼日龍.md "wikilink")、[鳥腳類的](../Page/鳥腳類.md "wikilink")[豪勇龍](../Page/豪勇龍.md "wikilink")、[沉龍](../Page/沉龍.md "wikilink")、[艾爾雷茲龍](../Page/艾爾雷茲龍.md "wikilink")\[3\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [BBC報導：新種肉食性恐龍被發現（始鯊齒龍與隱面龍）](http://www.theworld.org/wma.php?id=0215088)
    - 2008/2/15 （影音檔）

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:鯊齒龍科](../Category/鯊齒龍科.md "wikilink")

1.

2.  [New Meat-eating Dinosaur Duo From Sahara Ate Like Hyenas,
    Sharks](http://www.sciencedaily.com/releases/2008/02/080213193749.htm)

3.