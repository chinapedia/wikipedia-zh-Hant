**姚嘉妮**（，），現為[無線電視基本藝人合約藝員](../Page/無線電視.md "wikilink")，拍攝劇集及《[都市閒情](../Page/都市閒情.md "wikilink")》節目主持之一。1998年參加[亞洲小姐後獲得當屆](../Page/亞洲小姐.md "wikilink")[季軍](../Page/季軍.md "wikilink")，成為[亞洲電視藝員](../Page/亞洲電視.md "wikilink")，並開始擔任[電視節目](../Page/電視節目.md "wikilink")《[音樂快線](../Page/音樂快線.md "wikilink")》[主持人](../Page/主持人.md "wikilink")。在亞視拍攝過《[海瑞鬥嚴嵩](../Page/海瑞鬥嚴嵩.md "wikilink")》、《[英雄廣東十虎](../Page/英雄廣東十虎.md "wikilink")》等[電視劇集之後](../Page/電視劇集.md "wikilink")，姚嘉妮在2001年轉投至[無綫電視以繼續其在](../Page/無綫電視.md "wikilink")[電視](../Page/電視.md "wikilink")[戲劇方面的演藝事業](../Page/戲劇.md "wikilink")。她的父親[姚仰龍為](../Page/姚仰龍.md "wikilink")[香港輔助警察隊前總監](../Page/香港輔助警察隊.md "wikilink")\[1\]。而她曾就讀[美國國際學校](../Page/美國國際學校.md "wikilink")。

## 經歷

2006年11月11日，姚嘉妮與前[亞視藝員](../Page/亞視.md "wikilink")[林祖輝結束七年愛情長跑](../Page/林祖輝.md "wikilink")，於香港[屯門](../Page/屯門.md "wikilink")[黃金海岸酒店註冊並擺設婚宴](../Page/香港黃金海岸酒店.md "wikilink")，共諧連理\[2\]。

2008年11月25日，據她在她自己網誌\[3\]表示她已經懷孕（連載自己有了身孕相片）。同樣她的好友陳敏之亦有在陳敏之網誌提及此事\[4\]。

2009年4月20日，姚嘉妮於同日下午誕下女兒Kyra。

2012年2月，姚嘉妮出席公開活動時表示自己已懷孕四個月。

2013年8月，復出拍劇，復出作為客串劇集《[忠奸人](../Page/忠奸人.md "wikilink")》，並與女主角[田蕊妮有對手戲](../Page/田蕊妮.md "wikilink")。目前，姚嘉妮與丈夫林祖輝共同經營五間服裝店售賣童裝。

## 演出戲劇

### 電視劇（亞洲電視）

|                                      |                                        |        |
| ------------------------------------ | -------------------------------------- | ------ |
| **首播**                               | **劇名**                                 | **角色** |
| 1999年                                | [英雄廣東十虎](../Page/英雄廣東十虎.md "wikilink") | 杜攸言    |
| [海瑞鬥嚴嵩](../Page/海瑞鬥嚴嵩.md "wikilink") | \-                                     |        |
| 2000年                                | [香港一家人](../Page/香港一家人.md "wikilink")   | \-     |

### 電視劇（無線電視）

|                                                    |                                                  |                |        |
| -------------------------------------------------- | ------------------------------------------------ | -------------- | ------ |
| **首播**                                             | **劇名**                                           | **角色**         | **性質** |
| 2001年                                              | **[公私戀事多](../Page/公私戀事多.md "wikilink")**         | **莊美儀**        |        |
| 2003年                                              | [花樣中年](../Page/花樣中年.md "wikilink")               | 楊　寧（Ling）      |        |
| [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink")       | Pauline張                                         | 客串第127-129集    |        |
| [撲水冤家](../Page/撲水冤家.md "wikilink")                 | Angel                                            |                |        |
| 2004年                                              | [無名天使3D](../Page/無名天使3D.md "wikilink")           | 方小嫻（Susan）     |        |
| [棟篤神探](../Page/棟篤神探.md "wikilink")                 | 趙美美（Mia）                                         |                |        |
| [下一站彩虹](../Page/下一站彩虹.md "wikilink")               | CoCo                                             |                |        |
| [爭分奪秒](../Page/爭分奪秒.md "wikilink")                 | 朱詠琪（Wing）                                        |                |        |
| [廉政行動2004](../Page/廉政行動2004.md "wikilink")         | Eva                                              |                |        |
| 2005年                                              | [妙手仁心III](../Page/妙手仁心III.md "wikilink")         | 戴玉瑩（Grace）     |        |
| 2006年                                              | **[天幕下的戀人](../Page/天幕下的戀人.md "wikilink")**       | **葉嘉雲（Karen）** |        |
| **[肥田囍事](../Page/肥田囍事.md "wikilink")**             | **宋曼頤（Rachel）**                                  |                |        |
| [匯通天下](../Page/滙通天下_\(電視劇\).md "wikilink")         | 喬　菁                                              |                |        |
| 2007年                                              | **[紅衣手記](../Page/紅衣手記.md "wikilink")**           | **馮少媚**        |        |
| [廉政行動2007](../Page/廉政行動2007.md "wikilink")         | Wendy                                            |                |        |
| **[舞動全城](../Page/舞動全城.md "wikilink")**             | **游琳琳（Donna）**                                   |                |        |
| 2008年                                              | [金石良緣](../Page/金石良緣.md "wikilink")               | 何醫生            | 客串     |
| **[甜言蜜語](../Page/甜言蜜語.md "wikilink")**             | **袁少娜**                                          |                |        |
| [溏心風暴之家好月圓](../Page/溏心風暴之家好月圓.md "wikilink")       | 邱詠琳                                              | 特別演出           |        |
| 2009年                                              | [ID精英](../Page/ID精英.md "wikilink")               | 張思敏            |        |
| 2011年                                              | [居家兵團](../Page/居家兵團.md "wikilink")               | 宋子淇（Jessie）    |        |
| [Only You 只有您](../Page/Only_You_只有您.md "wikilink") | May                                              |                |        |
| [女拳](../Page/女拳.md "wikilink")                     | 李飄紅                                              | 客串             |        |
| [怒火街頭](../Page/怒火街頭.md "wikilink")                 | 馬卓爾德                                             | 單元女主角          |        |
| [團圓](../Page/團圓_\(香港電視劇\).md "wikilink")           | 王秀萍                                              |                |        |
| [真相](../Page/真相_\(無綫電視劇集\).md "wikilink")          | 周　敏                                              |                |        |
| [我的如意狼君](../Page/我的如意狼君.md "wikilink")             | 溫　柔                                              |                |        |
| 2012年                                              | [盛世仁傑](../Page/盛世仁傑.md "wikilink")               | 韋　妃            |        |
| 2014年                                              | [廉政行動2014](../Page/廉政行動2014.md "wikilink")       | 梁蘊賢            |        |
| [忠奸人](../Page/忠奸人_\(電視劇\).md "wikilink")           | 葉應心                                              | 客串             |        |
| 2015年                                              | [愛·回家 (第一輯)](../Page/愛·回家_\(第一輯\).md "wikilink") | 李　明（Ming）      |        |
| [張保仔](../Page/張保仔_\(2015年電視劇\).md "wikilink")      | 黃娣女                                              | 客串             |        |
| 2016年                                              | [愛·回家 (第二輯)](../Page/愛·回家_\(第二輯\).md "wikilink") | 李　明（Ming）      |        |
| [純熟意外](../Page/純熟意外.md "wikilink")                 | 莫曦瑜                                              | 客串             |        |
| [幕後玩家](../Page/幕後玩家.md "wikilink")                 | 傅嘉兒（Carrie）                                      |                |        |
| 2017年                                              | **[財神駕到](../Page/財神駕到.md "wikilink")**           | **徐　嬋**        |        |
| [超時空男臣](../Page/超時空男臣.md "wikilink")               | 郭鳳嬌（Gillian）                                     |                |        |
| [誇世代](../Page/誇世代.md "wikilink")                   | 麻煩客                                              | 客串             |        |
| 2018年                                              | **[BB來了](../Page/BB來了.md "wikilink")**           | **許　朗**        |        |
| [特技人](../Page/特技人.md "wikilink")                   | 沈　穎                                              |                |        |
| 未播映                                                | [街坊財爺](../Page/街坊財爺.md "wikilink")               | 司徒秀麗           |        |
| [她她她的少女時代](../Page/她她她的少女時代.md "wikilink")         | 沙甜甜                                              |                |        |

## 綜藝節目（無線電視）

  - 2002年：[名人飯局](../Page/名人飯局.md "wikilink")（主持）
  - 2006年：[學廚出餐](../Page/學廚出餐.md "wikilink")（嘉賓）
  - 2006年：[最緊要健康(第三輯)](../Page/最緊要健康\(第三輯\).md "wikilink")（主持）
  - 2007年：[最緊要健康(第四輯)](../Page/最緊要健康\(第四輯\).md "wikilink")（主持）
  - 2010年：[更上一層樓](../Page/更上一層樓.md "wikilink")（主持）
  - 2011年：[心有靈犀](../Page/心有靈犀.md "wikilink")（嘉賓）
  - 2013年：[千奇百趣玩FREE D](../Page/千奇百趣玩FREE_D.md "wikilink")（嘉賓）
  - 2013-2018年：[都市閒情](../Page/都市閒情.md "wikilink")（主持）
  - 2013年：[萬里尋爸](../Page/萬里尋爸.md "wikilink")（主持）
  - 2014年：[活得好Easy](../Page/活得好Easy.md "wikilink")（主持）
  - 2015年：[民峰而至](../Page/民峰而至.md "wikilink")（嘉賓）
  - 2017年：[都市閒情之赤口格鬥營](../Page/都市閒情.md "wikilink")（嘉賓）
  - 2018年：[都市閒情之安齡初一冇時停](../Page/都市閒情.md "wikilink")（主持）

## 節目主持（無線電視）

  - 2001年：[旅遊大搜索 德國](../Page/旅遊大搜索_德國.md "wikilink")
  - 2002年：[四洲特約：午年煙花匯演](../Page/四洲特約：午年煙花匯演.md "wikilink")
  - 2006年：[靈犬吉祥煙花耀香港](../Page/靈犬吉祥煙花耀香港.md "wikilink")
  - 2017年：[玩轉香港日與夜](../Page/玩轉香港日與夜.md "wikilink")

## 演出電影

  - 1999年：[刃手](../Page/刃手.md "wikilink")
  - 2000年：[逆我者死](../Page/逆我者死.md "wikilink")
  - 2003年：[賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink")
  - 2004年：[月滿抱西環](../Page/月滿抱西環.md "wikilink")

## 音樂錄影帶

  - 2002年：[陳奕迅](../Page/陳奕迅.md "wikilink") - 人來人往 （飾演陳奕迅前女友）
  - 2004年：[梁漢文](../Page/梁漢文.md "wikilink") - 新聞女郎
    （飾演[新聞主播](../Page/新聞主播.md "wikilink")）
  - 2017年：[黃浩琳](../Page/黃浩琳.md "wikilink") - 纏路 （飾演黃浩琳內心世界裡面的自己）

## 電台節目

  - [香港電台第二台](../Page/香港電台第二台.md "wikilink")《[瘋show快活人](../Page/瘋show快活人.md "wikilink")》嘉賓主持

## 獎項

  - 1998年度[亞洲小姐](../Page/亞洲小姐.md "wikilink")（季軍）

## 廣告代言

  - 2013年：馬百良八寶盐蛇散\[5\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [TVB藝人錄－姚嘉妮](http://jade.tvb.com/artiste/yiu_claire/)
  - [姚嘉妮 blog](http://hk.myblog.yahoo.com/yiu_claire/)

## 相關

[k](../Category/姚姓.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:亞洲小姐](../Category/亞洲小姐.md "wikilink")
[Category:左撇子](../Category/左撇子.md "wikilink")
[Category:美國國際學校校友](../Category/美國國際學校校友.md "wikilink")

1.
2.  《[怪風吹塌台百無禁忌
    姚嘉妮又喊又笑嫁林祖輝](http://the-sun.on.cc/channels/ent/20061112/)》，香港太陽報，文：電視小組，2006年11月12日
3.  《[姚嘉妮的blog
    台慶頒獎典禮](http://hk.rd.yahoo.com/blog/mod/art_title/*http://hk.myblog.yahoo.com/yiu-claire/article?mid=25)》，Yahoo\!Blog
    2008年11月25日
4.  《[陳敏之blog
    出年就快有B玩](http://blog.tvb.com/sharonchan/2008/11/22/%E5%87%BA%E5%B9%B4%E5%B0%B1%E5%BF%AB%E6%9C%89b%E7%8E%A9/)》，陳敏之blog，2008年11月22日
5.