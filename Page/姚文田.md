**姚文田**（），[字](../Page/表字.md "wikilink")**秋農**，[號](../Page/號.md "wikilink")**梅漪**。[浙江](../Page/浙江.md "wikilink")[歸安縣](../Page/歸安縣.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")[吴兴](../Page/吴兴.md "wikilink")）人。[清朝政治人物](../Page/清朝.md "wikilink")、[狀元](../Page/狀元.md "wikilink")。官至禮部尚書。

## 生平

姚文田生于[乾隆二十三年](../Page/乾隆.md "wikilink")（1758年）。[乾隆五十九年](../Page/乾隆.md "wikilink")（1794年）[高宗巡幸](../Page/乾隆帝.md "wikilink")[天津](../Page/天津.md "wikilink")，姚文田收召應試，獲得第一，授[內閣中書](../Page/內閣中書.md "wikilink")，充[軍機章京](../Page/軍機章京.md "wikilink")。[嘉庆四年](../Page/嘉庆.md "wikilink")（1799年）中式一甲第一名[進士](../Page/進士.md "wikilink")（状元），授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")。曾出典[廣東](../Page/廣東.md "wikilink")、[福建](../Page/福建.md "wikilink")[鄉試](../Page/鄉試.md "wikilink")，出督[廣東](../Page/廣東.md "wikilink")、[河南](../Page/河南.md "wikilink")[學政](../Page/學政.md "wikilink")，累遷[國子監祭酒](../Page/國子監祭酒.md "wikilink")。

[嘉慶十八年](../Page/嘉慶.md "wikilink")（1813年），直[南書房](../Page/南書房.md "wikilink")。朝廷因[林清之變](../Page/林清.md "wikilink")，下詔求言，姚文田上疏，建議減稅賦、慎刑罰，得到[清仁宗嘉納](../Page/清仁宗.md "wikilink")。[嘉慶二十年](../Page/嘉慶.md "wikilink")（1815年），擢[兵部侍郎](../Page/兵部侍郎.md "wikilink")，歷改[戶部](../Page/戶部.md "wikilink")、[禮部](../Page/禮部.md "wikilink")。[嘉慶二十二年](../Page/嘉慶.md "wikilink")（1817年）典[會試](../Page/會試.md "wikilink")。[嘉慶二十四年](../Page/嘉慶.md "wikilink")（1819年）督[江蘇](../Page/江蘇.md "wikilink")[學政](../Page/學政.md "wikilink")。

[道光元年](../Page/道光.md "wikilink")（1821年），[江](../Page/江.md "wikilink")[浙](../Page/浙.md "wikilink")[督撫](../Page/督撫.md "wikilink")[孫玉庭等商議禁絕漕務浮收](../Page/孫玉庭.md "wikilink")，明定為八折，實則許其加二。姚文田上疏反對，切中要害，於是朝廷下詔禁浮收，裁革運丁陋規，八折之議告寢。[道光四年](../Page/道光.md "wikilink")（1824年）擢[左都御史](../Page/左都御史.md "wikilink")。[道光七年](../Page/道光.md "wikilink")（1827年）遷[禮部尚書](../Page/禮部尚書.md "wikilink")。不久卒，朝廷依照尚書例賜卹，[諡](../Page/諡.md "wikilink")**文僖**。《[清史稿](../Page/清史稿.md "wikilink")》有傳。\[1\]

## 著作

姚文田工書法，著作甚豐。有《學易討原》一卷、《春秋經傳朔閏表》二卷、《四書瑣語》一卷、《邃雅堂學古錄》七卷、《偏旁舉略》一卷、《古音諧》八卷、《四聲易知錄》四卷、《漢初年月日表》一卷、《廣陵事略》七卷、《周初年月日歲星表》一卷、《邃雅堂文集》十卷、《說文聲系》三十卷、《說文考異》三十卷、《春秋日月表》五卷、《後漢郡國志校補》一卷、《進御集》二卷、《四子義》一卷、《內經脈法一卷、《疑龍經注》一卷、《撼龍經注》一卷、《相宅》一卷。

## 註釋

## 參考文獻

  - 《清史稿》，[趙爾巽等](../Page/趙爾巽.md "wikilink")，中華書局點校本。

## 外部連結

  - [姚文田](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:0:mctauac:NO%3DNO447)
    中研院史語所

{{-}}

[Category:清朝內閣中書](../Category/清朝內閣中書.md "wikilink")
[Category:軍機章京](../Category/軍機章京.md "wikilink")
[Category:清朝狀元](../Category/清朝狀元.md "wikilink")
[Category:清朝翰林院修撰](../Category/清朝翰林院修撰.md "wikilink")
[Category:清朝廣東學政](../Category/清朝廣東學政.md "wikilink")
[Category:清朝河南學政](../Category/清朝河南學政.md "wikilink")
[Category:清朝翰林院侍講](../Category/清朝翰林院侍講.md "wikilink")
[Category:清朝翰林院侍讀](../Category/清朝翰林院侍讀.md "wikilink")
[Category:清朝國子監祭酒](../Category/清朝國子監祭酒.md "wikilink")
[Category:清朝詹事府詹事](../Category/清朝詹事府詹事.md "wikilink")
[Category:內閣學士](../Category/內閣學士.md "wikilink")
[Category:清朝兵部侍郎](../Category/清朝兵部侍郎.md "wikilink")
[Category:清朝禮部侍郎](../Category/清朝禮部侍郎.md "wikilink")
[Category:清朝戶部侍郎](../Category/清朝戶部侍郎.md "wikilink")
[Category:清朝江蘇學政](../Category/清朝江蘇學政.md "wikilink")
[Category:清朝左都御史](../Category/清朝左都御史.md "wikilink")
[Category:清朝禮部尚書](../Category/清朝禮部尚書.md "wikilink")
[Category:湖州人](../Category/湖州人.md "wikilink")
[W文](../Category/姚姓.md "wikilink")
[Category:諡文僖](../Category/諡文僖.md "wikilink")
[Category:南書房行走](../Category/南書房行走.md "wikilink")

1.  《清史稿·卷三百七十四》：姚文田，字秋農，浙江歸安人。乾隆五十九年，高宗幸天津，召試第一，授內閣中書，充軍機章京。嘉慶四年一甲一名進士，授修撰。迭典廣東、福建鄉試，督廣東、河南學政，累遷祭酒。
    十八年，入直南書房。會因林清之變，下詔求言，文田疏陳，略謂：「堯、舜、三代之治，不越教養兩端：為民正趨向之路，知有長上，自不干左道之誅；為民廣衣食之源，各保身家，自不致有為惡之意。近日南方患賦重，北方患徭多，民困官貧，急宜省事。久督撫任期，則州縣供億少，寬州縣例議，則人才保全多。」次年復上疏，言：「上之於下，不患其不畏，而患其不愛。漢文吏治蒸蒸，不至於姦，愛故也。秦顓法律，衡石程書，一夫夜呼，亂者四起，畏故也。自數年來，開上控之端，刁民得逞其奸；大吏畏其京控，遇案親提，訐訴不過一人，牽涉常至數十，農商廢業，中道奔波，受胥吏折辱，甚至瘐死道斃。國家慎刑之意，亦曰有冤抑耳。……」奏入，仁宗嘉納之，特詔飭各省以勸課農桑為亟，速清訟獄，嚴懲誣枉。
    二十年，擢兵部侍郎，歷戶部、禮部。二十二年，典會試。二十四年，督江蘇學政。道光元年，江、浙督撫孫玉庭等議禁漕務浮收，明定八折，實許其加二。文田疏陳積弊曰：「乾隆三十年以前，並無所謂浮收。……其於紀綱法度，所關實為匪細。」疏入，下部議。時在廷諸臣多以為言，文田持議切中時弊，最得其平。詔禁浮收，裁革運丁陋規，八折之議遂寢。
    四年，擢左都御史。七年，遷禮部尚書。尋卒，依尚書例賜卹，諡文僖。
    文田持己方嚴，數督學政，革除陋例，斥偽體，拔真才，典試號得士。論學尊宋儒，所著書則宗漢學。博綜群籍，兼諳天文占驗。林清之變未起，彗入紫微垣；道光初，彗見南斗下，主外夷兵事：文田皆先事言之。