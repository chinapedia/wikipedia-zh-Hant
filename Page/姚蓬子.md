**姚蓬子**（），原名**方仁**，字**裸人**，后改名**杉尊**，笔名**丁爱**、**小莹**、**姚梦生**、**梦业**、**慕容梓**，[浙江](../Page/浙江.md "wikilink")[诸暨姚公埠人](../Page/诸暨.md "wikilink")。中国文学家、翻译家、诗人。\[1\]

## 生平

曾就学于[诸暨县立中学](../Page/诸暨县立中学.md "wikilink")、绍兴[越材中学](../Page/越材中学.md "wikilink")、上海[中国公学与](../Page/中国公学.md "wikilink")[北京大学](../Page/北京大学.md "wikilink")。1924年，他到上海，任[光华书局编辑](../Page/光华书局.md "wikilink")，并开始文学创作。1927年加入[中国共产党](../Page/中国共产党.md "wikilink")。1930年，参加[中国自由运动大同盟](../Page/中国自由运动大同盟.md "wikilink")。1931年，参加[中国左翼作家联盟](../Page/中国左翼作家联盟.md "wikilink")（左联），任党组宣传部长。1931年2月，为[上海联合书店出版之](../Page/上海联合书店.md "wikilink")《[文艺生活](../Page/文艺生活.md "wikilink")》月刊主编。1932年6月，与[周起应合编左联的](../Page/周起应.md "wikilink")《[文学月报](../Page/文学月报.md "wikilink")》，任主编。后来调入[中共中央特科工作](../Page/中共中央特科.md "wikilink")。1933年12月，在[天津被逮捕](../Page/天津.md "wikilink")，入[中央反省院](../Page/中央反省院.md "wikilink")。1934年5月，在《[中央日报](../Page/中央日报.md "wikilink")》发表《脱离共产党宣言》，被释放。此后任[中央文化运动委员会委员](../Page/中央文化运动委员会.md "wikilink")、[中央图书杂志审查委员会委员](../Page/中央图书杂志审查委员会.md "wikilink")，并为[曾养甫的](../Page/曾养甫.md "wikilink")《[扶轮日报](../Page/扶轮日报.md "wikilink")》编辑副刊。\[2\]

[中国抗日战争开始后](../Page/中国抗日战争.md "wikilink")，1938年3月，他加入[中华全国文艺界抗敌协会](../Page/中华全国文艺界抗敌协会.md "wikilink")。同年5月，与[老舍合编该协会](../Page/老舍.md "wikilink")《[抗战文艺](../Page/抗战文艺.md "wikilink")》三日刊。1938年10月，日军占领[武汉](../Page/武汉.md "wikilink")，他赴[重庆](../Page/重庆.md "wikilink")，1940年9月任[国民政府军事委员会政治部文化工作委员会委员](../Page/国民政府军事委员会政治部文化工作委员会.md "wikilink")。1942年，他在重庆创办[作家书屋](../Page/作家书屋.md "wikilink")，又与[老舍](../Page/老舍.md "wikilink")、[赵铭彝等创刊](../Page/赵铭彝.md "wikilink")《[文坛小报](../Page/文坛小报.md "wikilink")》。1945年抗战胜利后，他迁作家书屋至[上海继续营业](../Page/上海.md "wikilink")，并出版《北方文丛》等。\[3\]

1954年[公私合營](../Page/公私合營.md "wikilink")，作家書屋并入[上海教育出版社](../Page/上海教育出版社.md "wikilink")，姚蓬子、周修文夫婦同入[上海教育出版社任職員](../Page/上海教育出版社.md "wikilink")。1955年姚蓬子因卷入[潘漢年](../Page/潘漢年.md "wikilink")、[楊帆反黨小集團案而被捕](../Page/楊帆.md "wikilink")，同年被释放后成为[自由职业者](../Page/自由职业者.md "wikilink")，以译著和写作为生。1963年后，任教于[上海师范学院中文系](../Page/上海师范学院.md "wikilink")。\[4\]\[5\]

1969年，姚蓬子在上海病逝。\[6\]\[7\]

## 著作

著有短篇小说集《浮世画》、诗集《银铃》、《蓬子诗钞》、《剪影集》等。\[8\]

## 家庭

  - 父亲[姚汉章](../Page/姚汉章.md "wikilink")，清末[举人](../Page/举人.md "wikilink")。
  - 儿子[姚文元](../Page/姚文元.md "wikilink")，[文化大革命时期活跃的](../Page/文化大革命.md "wikilink")[中国共产党领导人](../Page/中国共产党.md "wikilink")，[四人帮成员之一](../Page/四人帮.md "wikilink")。

## 参考文献

{{-}}

[category:绍兴人](../Page/category:绍兴人.md "wikilink")
[P](../Page/category:姚姓.md "wikilink")

[Category:中华民国翻译家](../Category/中华民国翻译家.md "wikilink")
[Category:中华民国诗人](../Category/中华民国诗人.md "wikilink")
[Category:中华民国编辑](../Category/中华民国编辑.md "wikilink")
[Category:中共中央特科人物](../Category/中共中央特科人物.md "wikilink")
[Category:中国共产党党员
(1927年入党)](../Category/中国共产党党员_\(1927年入党\).md "wikilink")
[Category:前中國共產黨黨員](../Category/前中國共產黨黨員.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:中国公学校友](../Category/中国公学校友.md "wikilink")

1.  [姚蓬子(1905～1969)，上海通，于2012-5-23查阅](http://www.shtong.gov.cn/newsite/node2/node2245/node4521/node31943/node63920/node63922/userobject1ai54771.html)

2.
3.
4.
5.  [王炳毅，姚蓬子與徐恩曾在南京的特殊交往，書屋2006年第8期](http://big5.ycwb.com/culture/2010-08/26/content_2629105.htm)

6.
7.
8.