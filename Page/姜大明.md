**姜大明**（），[山东](../Page/山东.md "wikilink")[荣成人](../Page/荣成.md "wikilink")，[中共中央党校研究生学历](../Page/中共中央党校.md "wikilink")；曾任[山东省省长](../Page/山东省.md "wikilink")，[国土资源部部长](../Page/国土资源部.md "wikilink")\[1\]，兼任国家土地总督察。[中共十五大代表](../Page/中共十五大.md "wikilink")，[第十六屆中共中央候補委員](../Page/中国共产党第十六届中央委员会候补委员列表.md "wikilink")，[第十七届](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")、[第十八届中共中央委员](../Page/中国共产党第十八届中央委员会委员列表.md "wikilink")；[第九届全国政协委员](../Page/第九届全国政协委员.md "wikilink")。

## 生平经历

1969年，16岁的姜大明作为[知青下放](../Page/知青.md "wikilink")[黑龙江生产建设兵团劳动](../Page/黑龙江生产建设兵团.md "wikilink")。1969年9月至1975年4月历任[黑龙江生产建设兵团战士](../Page/黑龙江生产建设兵团.md "wikilink")、班长、副排长、团政治处新闻报道员。1975年4月至1977年3月任[黑龙江生产建设兵团团政治处书记](../Page/黑龙江生产建设兵团.md "wikilink")，其间于1976年12月加入[中国共产党](../Page/中国共产党.md "wikilink")。1977年3月至1978年3月任黑龙江国营[锦河农场办公室秘书](../Page/锦河农场.md "wikilink")。1978年3月进入[黑龙江大学哲学系学习](../Page/黑龙江大学.md "wikilink")，任党支部副书记。1982年1月毕业后进入[中国共产主义青年团中央组织部工作](../Page/中国共产主义青年团.md "wikilink")，历任干事，组织处副处长、处长，组织部副部长。1990年6月出任[团中央组织部部长](../Page/团中央.md "wikilink")，1991年12月升任[团中央常委](../Page/团中央.md "wikilink")。1993年5月，姜大明出任[共青团中央书记处书记](../Page/共青团中央.md "wikilink")，并于1998年2月当选[全国青联副主席](../Page/全国青联.md "wikilink")。

1998年7月，姜大明调往山东省工作，任山东省委常委、组织部部长；2000年12月升任山东省委副书记兼组织部部长；2004年1月，姜大明调任[济南市委书记](../Page/济南市.md "wikilink")。2007年6月13日，姜大明被任命为山东省副省長、代省长，2008年1月26日在山东省第十一届人民代表大会第一次会议正式当选山东省省长，主要负责财政、税务、审计方面的工作。

2013年3月16日，十二届全国人大一次会议举行第六次全体会议。根据[国务院总理](../Page/国务院总理.md "wikilink")[李克强提名](../Page/李克强.md "wikilink")，会议经过投票表决，决定姜大明为[国土资源部部长](../Page/国土资源部.md "wikilink")。[2013年3月](../Page/2013年3月.md "wikilink")29日，山东省人大常委会同意姜大明辞去省长职务的请求\[2\]。2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[3\]。

## 参考文献

## 外部链接

  -
{{-}}

[Category:荣成人](../Category/荣成人.md "wikilink")
[D大明](../Category/姜姓.md "wikilink")
[Category:黑龙江大学校友](../Category/黑龙江大学校友.md "wikilink")
[Category:中国共产党第十六届中央委员会候补委员](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:第九屆全國政協委員](../Category/第九屆全國政協委員.md "wikilink")
[Category:中国共产党党员
(1976年入党)](../Category/中国共产党党员_\(1976年入党\).md "wikilink")
[Category:中共山东省委副书记](../Category/中共山东省委副书记.md "wikilink")
[Category:中共山東省委組織部部長](../Category/中共山東省委組織部部長.md "wikilink")
[Category:中共济南市委书记](../Category/中共济南市委书记.md "wikilink")
[Category:中华人民共和国国土资源部部长](../Category/中华人民共和国国土资源部部长.md "wikilink")
[Category:中华人民共和国国家土地总督察](../Category/中华人民共和国国家土地总督察.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")

1.  [全国人大决定姜大明为国土资源部部长](http://news.qq.com/a/20130316/001068.htm)
    中国新闻网_2013-03-16
2.  [姜大明辞去山东省省长职务
    郭树清任山东省代省长](http://henan.china.com.cn/news/china/201303/A24128TGJT.html)
    中国网_2013-03-29
3.