**姜宸英**（），[字](../Page/字.md "wikilink")**西溟**，[號](../Page/號.md "wikilink")**淇園**，又號**韋間**，[明末](../Page/明.md "wikilink")[清初](../Page/清.md "wikilink")[書法家](../Page/書法家.md "wikilink")、史学家。[浙江](../Page/浙江.md "wikilink")[慈谿縣](../Page/慈谿縣.md "wikilink")（今[慈溪市](../Page/慈溪市.md "wikilink")）人。

## 生平

初以布衣荐修明史，曾撰《张使君提调陕西乡试闱政记》。康熙三十六年（1697年），七十歲始舉進士，[康熙帝亲置于一甲第三名](../Page/康熙帝.md "wikilink")（[探花](../Page/探花.md "wikilink")）\[1\]，授翰林院編修。

康熙三十八年（1699年），任[順天](../Page/順天.md "wikilink")[鄉試副考官](../Page/鄉試.md "wikilink")。是科发榜后，物议沸腾，當時落第士子戲稱：“老-{姜}-全无辣气，小李大有甜头。”因科場案牽連，十一月丁酉（初三日），被江南道御史[鹿祐弹劾](../Page/鹿祐.md "wikilink")，姜宸英與[李蟠入獄](../Page/李蟠.md "wikilink")，宸英饮药自尽，死於獄中。死前自拟挽联：“这回算吃亏受罪，只因入了孔氏牢门，坐冷板凳，作老猢狲，只说是限期弗满，竟挨到头童齿豁，两袖俱空，书呆子何足算也；此去却喜地欢天，必须假得孟婆村道，赏剑树花，观刀山瀑，方可称眼界别开，和这些酒鬼诗魔，一堂常聚，南面王以加之耳。”

不久[康熙帝得知浙江宁波考生](../Page/康熙帝.md "wikilink")[姚观確有文才](../Page/姚观.md "wikilink")，下旨释放姜宸英，追回被充军的李蟠。康熙得知宸英已於獄中自盡，痛惜咨嗟不已，“举朝知其无罪，莫不叹息”\[2\]。

## 书法、学术

有文名，与[朱彝尊](../Page/朱彝尊.md "wikilink")、[严绳孙合称](../Page/严绳孙.md "wikilink")“江南三布衣”。擅書法，尤精楷書，宗[米芾](../Page/米芾.md "wikilink")、[董其昌](../Page/董其昌.md "wikilink")，書法以摹古為本，融合各家之長。與[笪重光](../Page/笪重光.md "wikilink")、[汪士鋐](../Page/汪士鋐.md "wikilink")、[何焯並稱為康熙四大家](../Page/何焯.md "wikilink")。曾參與修纂《[明史](../Page/明史.md "wikilink")》，史稱為文“宏博雅健，但敘事稍差”。康熙二十八年，徐乾學被彈劾告假歸田，奉命即家纂修《[大清一統志](../Page/大清一統志.md "wikilink")》，姜宸英與[黃虞稷偕行](../Page/黃虞稷.md "wikilink")，後設局於洞庭東山。

## 评论

  - “西溟少时学米（芾）、董（其昌）书有名，至戊辰后，方用第四指学晋人书，丁丑后方用大指，专工小楷，是时年已七十矣。使其少时即知笔法，力学至老，岂非丰劳功后一人哉？”——[杨宾](../Page/杨宾.md "wikilink")
    《大瓢偶笔》

<!-- end list -->

  - “韦间先生每临帖多佳，能以自家性情，合古人神理，不似而似，所以妙也。”——《频罗庵论书》

## 著作

  - 《湛園未定稿》
  - 《湛园题跋》
  - 《葦間詩集》
  - 《西溟文鈔》
  - 《姜先生全集》33卷，后人编撰。

## 註釋

[category:清朝探花](../Page/category:清朝探花.md "wikilink")
[category:清朝翰林院編修](../Page/category:清朝翰林院編修.md "wikilink")
[category:清朝书法家](../Page/category:清朝书法家.md "wikilink")
[category:中国历史学家](../Page/category:中国历史学家.md "wikilink")
[category:慈溪人](../Page/category:慈溪人.md "wikilink")
[C](../Page/category:姜姓.md "wikilink")

[Category:清朝自殺人物](../Category/清朝自殺人物.md "wikilink")

1.  [福格](../Page/福格.md "wikilink")《[聽雨叢談](../Page/聽雨叢談.md "wikilink")·[卷九](../Page/s:聽雨叢談/卷09.md "wikilink")》：（康熙）三十六年丁丑會試。總裁：尚書[熊賜履](../Page/熊賜履.md "wikilink")、禮書[張英](../Page/張英.md "wikilink")、左都[吴琠](../Page/吴琠.md "wikilink")、兵侍[田雯](../Page/田雯.md "wikilink")。中式一百五十九人。探花**姜宸英**以古文名世，上久知其名。殿試卷進呈，擬二甲。上問十卷中有姜某乎，尚書[韓菼對以宸英在史館久](../Page/韓菼.md "wikilink")，臣識其字，某卷當是也。拔置一甲第三，時年七十三矣，自古探花無此際遇。十月，諭嗣後宗室子弟一體應試。
2.  《清史稿》卷三百六十六