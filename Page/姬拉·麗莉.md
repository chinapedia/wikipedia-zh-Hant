**姬拉·克莉絲蒂娜·麗莉**（，\[1\]；）是一個曾獲[奧斯卡金像獎和](../Page/奧斯卡金像獎.md "wikilink")[金球獎提名的](../Page/金球獎.md "wikilink")[英國](../Page/英國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。\[2\]她在孩童時已經開始其演藝事業，在2003年電影《[我爱贝克汉姆](../Page/我愛碧咸.md "wikilink")》和《[神鬼奇航：鬼盜船魔咒](../Page/加勒比海盜.md "wikilink")》中擔任主角後，成為世界知名的影星，其後陸續在多齣[荷里活電影中演出](../Page/荷里活電影.md "wikilink")。2005年演出改編自[珍·奧斯汀小說的同名電影](../Page/珍·奧斯汀.md "wikilink")《[傲慢與偏見](../Page/傲慢與偏見_\(2005年電影\).md "wikilink")》，以此劇提名入圍奧斯卡最佳女主角獎。2014年再憑藉《[模仿遊戲](../Page/模仿遊戲.md "wikilink")》提名奧斯卡最佳女配角。

## 個人生平

### 早期生活

姬拉·麗莉於[英格蘭](../Page/英格蘭.md "wikilink")[大倫敦](../Page/大倫敦.md "wikilink")[泰晤士河畔列治文區](../Page/泰晤士河畔列治文區.md "wikilink")[特丁頓](../Page/特丁頓.md "wikilink")[密德塞克斯出生](../Page/密德塞克斯.md "wikilink")。其母親是[蘇格蘭](../Page/蘇格蘭.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")[莎文·麥當勞](../Page/莎文·麥當勞.md "wikilink")，其父為[英格蘭電影及電視演員](../Page/英格蘭.md "wikilink")[偉·來利](../Page/偉·來利.md "wikilink")，二人還有一名長子，於1979年出生的Caleb。姬拉半生在[里奇蒙度過](../Page/里奇蒙_\(倫敦\).md "wikilink")，上特丁頓學校和。雖然姬拉患了[失讀症](../Page/失讀症.md "wikilink")，但在學校取得優良成績，使其父母同意其演藝事業。姬拉曾回憶說她孩童時期「專心致志地演出」\[3\]。她在數個業餘電影中演出，包括由其母親寫作的「」和由後來成為她戲劇老師的John
McShane寫作的「」。

### 奇特的名字

許多人對綺拉名字奇異的拼法感到好奇，而綺拉也曾在訪問中提到，她的名字"Keira"本來應該要拼作"Kiera"\<ref name="Keira
Knightley Reveals Her Name Is Spelled Wrong, Should Be Kiera: Find Out
Who's to
Blame\!\>[1](http://www.usmagazine.com/celebrity-style/news/keira-knightley-name-spelled-wrong-photos-2015271)</ref>，一個她爸爸當時很喜歡的俄羅斯滑冰選手。但是當時去幫她登記出生證明的是她媽媽，而她媽媽的拼字又很爛，所以就不小心變成了"Keira"。

### 事業

[Keira_Knightley.jpg](https://zh.wikipedia.org/wiki/File:Keira_Knightley.jpg "fig:Keira_Knightley.jpg")
姬拉於7歲時便開始演出，1993年的電視劇*Royal
Celebration*便是她首次演出，她也在1995年電視連續劇中亮相，也在1990年代後期演出多齣[電視電影](../Page/電視電影.md "wikilink")。她在《[星球大战I：幽灵的威胁](../Page/星際大戰首部曲：威脅潛伏.md "wikilink")》飾演薩貝（Sabé）。她獲邀演出這角色的原因是，她與飾演[佩咪·艾米達拉的](../Page/佩咪·艾米達拉.md "wikilink")[娜塔莉·波曼不僅長得像也是好友](../Page/娜塔莉·波曼.md "wikilink")，她們的母親也說難以把她們分辨與分離。\[4\]姬娜的首次擔正演出是在2001年，在[和路迪士尼電視電影](../Page/和路迪士尼.md "wikilink")《》中飾演[羅賓漢的女兒](../Page/羅賓漢.md "wikilink")。與此同時，她在[錄影帶首映的](../Page/錄影帶首映.md "wikilink")《[鬼地方](../Page/鬼地方.md "wikilink")》中裸體演出，當時只有15歲。她也在《[齊瓦哥醫生
(影集)](../Page/齊瓦哥醫生_\(影集\).md "wikilink")》的2002年迷你電視劇版中演出。

姬拉的突破性演出是以[足球為題材的英國電影](../Page/足球.md "wikilink")《[我愛碧咸](../Page/我愛碧咸.md "wikilink")》，在2002年8月的英國票房取得成功，也在2003年3月的美國票房獲得不錯的成績，票房高達3千2百萬美元。\[5\]其後，她的知名度大大提升，演出大制作《[加勒比海盜](../Page/加勒比海盜.md "wikilink")》。此電影在2003年7月上演，影評一致讚好，帶來極高票房收入，也成為暑期的熱潮。

2003年11月上演的英國喜劇電影《[真的戀愛了](../Page/真的戀愛了.md "wikilink")》中，姬拉飾演新婚的茱麗葉。她的下一齣電影《[-{zh-hans:亚瑟王;zh-hk:王者無敵;zh-tw:亞瑟王;}-](../Page/王者無敵.md "wikilink")》於2004年7月上演，影評褒貶不一；但是[Roger
Ebert大加讚賞](../Page/Roger_Ebert.md "wikilink")，稱電影的質素配合戲中演員，而他形容姬拉為「既性感，又滿佈泥濘」。\[6\]同月，*[Hello\!](../Page/Hello!.md "wikilink")*雜誌的讀者投票選出姬拉為電影界中最有前途的青年明星。此外，《[時代雜誌](../Page/時代雜誌.md "wikilink")》在2004年的一個專欄指出，姬拉似乎願意獻身成為認真的演員，而非電影明星。

姬拉在2005年的三齣電影中演出：3月上演的驚嚇電影《[生死時空](../Page/生死時空.md "wikilink")》、10月上演，由[東尼·史考特導演](../Page/東尼·史考特.md "wikilink")，改編自為酬金殺人的[多明妮·哈維故事的](../Page/多明妮·哈維.md "wikilink")《[女模煞](../Page/女模煞.md "wikilink")》，還有在改編自[珍·奧斯汀著名小說](../Page/珍·奧斯汀.md "wikilink")《[傲慢與偏見](../Page/傲慢與偏見.md "wikilink")》的[同名電影飾演](../Page/傲慢與偏見_\(2005年電影\).md "wikilink")[伊莉莎白·班內特](../Page/伊莉莎白·班內特.md "wikilink")。雖然《生死時空》和《女模煞》的票房和口碑皆不佳\[7\]\[8\]，但《傲慢與偏見》成為姬拉第6次在英國票房中取得首位的電影。她憑此片的演出，獲得[金球獎](../Page/金球獎.md "wikilink")「最佳女主角──喜劇／音樂」和[奧斯卡](../Page/奧斯卡.md "wikilink")「[最佳女主角](../Page/奧斯卡最佳女主角獎.md "wikilink")」兩項提名，但都未能獲獎。

姬拉於2006年完成了《[異旅情絲](../Page/異旅情絲.md "wikilink")》（由[Alessandro
Baricco的小說改編](../Page/Alessandro_Baricco.md "wikilink")）和《[贖罪
(電影)](../Page/贖罪_\(電影\).md "wikilink")》（由[Ian
McEwan同名小說改編](../Page/Ian_McEwan.md "wikilink")，與[-{zh-hans:詹姆斯·麦卡沃伊;zh-hk:占士·麥艾禾;zh-tw:詹姆斯·麥艾維;}-](../Page/詹姆斯·麥艾維.md "wikilink")、[-{zh-hans:瓦妮莎·雷德格瑞夫;zh-hk:雲尼莎·韋姬芙;zh-tw:凡妮莎·蕾格烈芙;}-](../Page/凡妮莎·蕾格烈芙.md "wikilink")、[-{zh-hk:白蓮達·比芙蓮;zh-tw:布蘭達·布蕾辛;zh-cn:布兰达·布莱斯;}-合作](../Page/布蘭達·布蕾辛.md "wikilink")）的拍攝\[9\]。她所演出的《[-{zh-hans:加勒比海盗3：世界的尽头;zh-hk:加勒比海盜：魔盜王終極之戰;zh-tw:加勒比海盜
神鬼奇航3：世界的盡頭;}-](../Page/加勒比海盜3.md "wikilink")》於2007年5月上演。她也確定演出由其母親寫作，有關威爾斯詩人[狄蘭·托馬斯的電影](../Page/狄蘭·托馬斯.md "wikilink")《美麗誘情》(The
Edge of Love)。

### 名聲

[Knightley_-_Johansson_-_Vanity_Fair_Full_Cover.jpg](https://zh.wikipedia.org/wiki/File:Knightley_-_Johansson_-_Vanity_Fair_Full_Cover.jpg "fig:Knightley_-_Johansson_-_Vanity_Fair_Full_Cover.jpg")
(左)
与[湯姆·福特在](../Page/湯姆·福特.md "wikilink")《[浮華世界](../Page/浮華世界_\(杂志\).md "wikilink")》杂志好莱坞版2006三月期上裸体出镜。\]\]

自從姬拉急速冒起，成為紅星之後，她便成為了眾多傳媒的關注焦點。雖然她已表明「不會談及私人生活」\[10\]，但仍有些報道形容她為「著名地向傳媒友好」\[11\]。

她獲《[帝國雜誌](../Page/帝國雜誌.md "wikilink")》（Empire）的讀者投票選為全時期最性感電影明星；排英國版《*FHM*》雜誌2004年100大最性感女性的第79位，05年排第18，06年排首位；美國版*FHM*列她為2004年第54位、2005年第11位、2006年第5位。姬拉和[-{zh-hans:斯佳丽·约翰逊;zh-hk:絲嘉莉·鍾安遜;zh-tw:史嘉蕾·喬韓森;}-在](../Page/史嘉蕾·喬韓森.md "wikilink")2006年3月*Vanity
Fair*全裸登上封面\[12\]。2006年5月，她排《*Maxim*》的Hot 100\[13\]的第9位。

姬拉是Asprey品牌的代言人，也在日本電視廣告上推銷Lux護髮用品。2006年4月，她證實為[香奈兒香水](../Page/香奈兒.md "wikilink")「Coco
Mademoiselle」的產品代言人。\[14\]

格拉斯哥組合Endrick Brothers的新歌*Star of the Silver
Screen*（「銀幕之星」）便是以姬拉為主題，於他們的[音樂專輯](../Page/音樂專輯.md "wikilink")"Attraction
versus Love"推出。此曲創作前，他們曾於格拉斯哥的一個派對上見面，當時姬拉正在拍攝《生死時空》。

出席2006年[金球獎頒獎禮時](../Page/金球獎.md "wikilink")，她身穿的禮服為她帶來讚譽，甚至使她躋身於*Entertainment
Tonight*節目的最佳衣著獎。她把出席[2006年奧斯卡金像獎頒獎禮的裙捐贈給](../Page/第78屆奧斯卡金像獎.md "wikilink")[樂施會](../Page/樂施會.md "wikilink")，並籌得4300[歐元](../Page/歐元.md "wikilink")。\[15\]

### 生活

[Keira_Knightley_07.jpg](https://zh.wikipedia.org/wiki/File:Keira_Knightley_07.jpg "fig:Keira_Knightley_07.jpg")
姬拉現時居住[中倫敦](../Page/中倫敦.md "wikilink")。她從2005年起，與《[傲慢與偏見](../Page/傲慢與偏見_\(2005年電影\).md "wikilink")》的演員[魯伯特·弗蘭德談戀愛](../Page/魯伯特·弗蘭德.md "wikilink")，但兩人已在2010年12月分手\[16\]；之前她曾與愛爾蘭時裝模特兒[傑米·道南談戀愛](../Page/傑米·道南.md "wikilink")\[17\]。雖然有報道指她與演員分手時，男方曾意圖自殺，但雙方都否認報道。\[18\]姬拉曾表明她不喜歡盛裝打扮，也覺得英國男士比美國男士好，因為她認為英國男士較少注意自己的外貌。\[19\]

雖然在《[加勒比海盗2：聚魂棺](../Page/加勒比海盗2：聚魂棺.md "wikilink")》的首映禮上，她窈窕的身材使傳媒猜測她患上了[進食障礙](../Page/進食障礙.md "wikilink")，她也說她有厭食症的家族史，但她已否認患上[神經性厭食症的流言](../Page/神經性厭食症.md "wikilink")，說自己的體重和身型都是自然而來的。\[20\]\[21\]2006年7月，她說自己已變成[工作狂](../Page/工作狂.md "wikilink")，說自己「已不能分辨去年和前年的事；把過去五年的事都混在一起了」，覺得自己「工作得太多了」，\[22\]「恐怕長此下去，會開始討厭我所喜愛的」。\[23\]故此，姬拉表示「已厭倦了當名人」，說「這名人圈的東西完全使人瘋狂」，並有意從演藝事業中退出，重過新生活。\[24\]
因此，她有可能會放一年長假期，把精神放在私事上。\[25\]

姬拉仍然是英國足球隊[韋斯咸的忠實球迷](../Page/韋斯咸.md "wikilink")，孩童時曾多次進入球場觀賽。\[26\]

2015年5月，綺拉生下了與James Righton的第一個女兒，Edie。\[27\]

## 電影作品

[KeiraKnightleyJuly06.jpg](https://zh.wikipedia.org/wiki/File:KeiraKnightleyJuly06.jpg "fig:KeiraKnightleyJuly06.jpg")》倫敦首映會場\]\]
[Keira_Knightley_at_BAFTA_Film_Awards_2008.jpg](https://zh.wikipedia.org/wiki/File:Keira_Knightley_at_BAFTA_Film_Awards_2008.jpg "fig:Keira_Knightley_at_BAFTA_Film_Awards_2008.jpg")會場\]\]

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/飛越驚魂陣.md" title="wikilink">飛越驚魂陣</a>（<em>Innocent Lies</em>）</p></td>
<td><p>Young Celia</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/星際大戰首部曲：威脅潛伏.md" title="wikilink">星際大戰首部曲：威脅潛伏</a><br />
</p></td>
<td><p>薩貝（Sabé，女王的侍女）</p></td>
<td><p>第一個大銀幕角色</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/俠盜公主.md" title="wikilink">俠盜公主</a>（<em>Princess of Thieves</em>）</p></td>
<td><p><a href="../Page/羅賓漢.md" title="wikilink">羅賓漢的女兒Gwyn</a></p></td>
<td><p>首次擔當主角</p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/Deflation_(film).md" title="wikilink">Deflation</a></p></td>
<td><p>Jogger</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/鬼地方.md" title="wikilink">鬼地方</a>（<em>The Hole</em>）</p></td>
<td><p>Frances 'Frankie' Almond Smith</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/我愛貝克漢.md" title="wikilink">我愛貝克漢</a>（Bend It Like Beckham）</p></td>
<td><p>Juliette 'Jules' Paxton</p></td>
<td><p>憑此片在美國一炮而紅</p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/Thunderpants.md" title="wikilink">Thunderpants</a></p></td>
<td><p>Music school student</p></td>
<td><p>Uncredited</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/Pure_(2002_film).md" title="wikilink">Pure</a></p></td>
<td><p>Louise</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/加勒比海盜.md" title="wikilink">-{zh-hans:加勒比海盜;zh-hk:魔盜王決戰鬼盜船;zh-tw:神鬼奇航：鬼盜船魔咒;}-</a></p></td>
<td><p><a href="../Page/伊莉莎白·史旺.md" title="wikilink">-{A</a></p></td>
<td><p>提名-<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女配角</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/真的戀愛了.md" title="wikilink">-{zh-hans:真爱至上;zh-hk:真的戀愛了;zh-tw:愛是您‧愛是我;}-</a>（<em>Love Actually</em>）</p></td>
<td><p>茱麗葉</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/王者無敵.md" title="wikilink">-{zh-hans:亚瑟王;zh-hk:王者無敵;zh-tw:亞瑟王;}-</a>（<em>King Arthur</em>）</p></td>
<td><p><a href="../Page/桂妮薇兒.md" title="wikilink">桂妮薇兒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/顫慄時空.md" title="wikilink">顫慄時空</a>（<em>The Jacket</em>）</p></td>
<td><p>Jackie</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多米諾(女模煞).md" title="wikilink">多米諾(女模煞)</a>（<em>Domino</em>）</p></td>
<td><p><a href="../Page/Domino_Harvey.md" title="wikilink">Domino Harvey</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傲慢與偏見_(2005年電影).md" title="wikilink">傲慢與偏見</a></p></td>
<td><p><a href="../Page/伊莉莎白·班內特.md" title="wikilink">伊莉莎白·班內特</a></p></td>
<td><p>提名-<a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a><br />
提名-<a href="../Page/金球獎最佳音樂及喜劇類電影女主角.md" title="wikilink">金球獎最佳音樂及喜劇類電影女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/神鬼奇航2.md" title="wikilink">-{zh-hans:加勒比海盜2;zh-hk:加勒比海盜：決戰魔盜王;zh-tw:神鬼奇航2：加勒比海盜;}-</a>''</p></td>
<td><p><a href="../Page/伊莉莎白·史旺.md" title="wikilink">伊莉莎白·史旺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/加勒比海盜3.md" title="wikilink">-{zh-hans:加勒比海盜3;zh-hk:加勒比海盜：魔盜王終極之戰;zh-tw:神鬼奇航3：世界的盡頭;}-</a></p></td>
<td><p><a href="../Page/伊莉莎白·史旺.md" title="wikilink">伊莉莎白·史旺</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Helene Joncour</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贖罪_(電影).md" title="wikilink">贖罪 (電影)</a></p></td>
<td><p>Cecilia Tallis</p></td>
<td><p>提名-<a href="../Page/英國電影學院獎最佳女主角.md" title="wikilink">英國電影學院獎最佳女主角</a><br />
提名-<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p>美麗誘情（<em>The Edge of Love</em>）</p></td>
<td><p>Vera Phillips</p></td>
<td><p>由母親<a href="../Page/莎文·麥當勞.md" title="wikilink">莎文·麥當勞編劇</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/浮華一世情.md" title="wikilink">浮華一世情</a>（<em>The Duchess</em>）</p></td>
<td><p>乔治亚娜</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/別讓我走.md" title="wikilink">別讓我走</a>（<em>Never Let Me Go</em>）</p></td>
<td><p>露絲</p></td>
<td><p>提名-<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女配角</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伦敦大道.md" title="wikilink">伦敦大道</a>（<em>London Boulevard</em>）</p></td>
<td><p>Charlotte</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/一夜誘情.md" title="wikilink">一夜誘情</a>（<em>Last Night</em>）</p></td>
<td><p>祖安娜</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/危險療程.md" title="wikilink">危險療程</a>（<em>A Dangerous Method</em>）</p></td>
<td><p><a href="../Page/莎賓娜·史碧爾埃.md" title="wikilink">莎賓娜·史碧爾埃</a>（Sabina Spielrein）</p></td>
<td><p>提名-<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/末日倒數怎麼伴.md" title="wikilink">末日倒數怎麼伴</a>（<em>Seeking a Friend for the End of the World</em>）</p></td>
<td><p>Penelope Lockhart</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/安娜·卡列尼娜_(2012年電影).md" title="wikilink">安娜‧卡列妮娜</a> (<em>Anna Karenina</em>)</p></td>
<td><p><a href="../Page/安娜·卡列尼娜.md" title="wikilink">安娜‧卡列妮娜</a> (Anna Karenina)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/傑克萊恩︰詭影任務.md" title="wikilink">傑克萊恩︰詭影任務</a> (<em>Jack Ryan: Shadow Recruit</em>)</p></td>
<td><p>Cathy Muller</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼哈頓戀習曲.md" title="wikilink">曼哈頓戀習曲</a> (<em>Begin Again / Can a Song Save Your Life?</em>)</p></td>
<td><p>Greta</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大愛晚成（<em>Laggies</em>）</p></td>
<td><p>Megan</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/模仿遊戲.md" title="wikilink">模仿遊戲</a>（<em>The Imitation Game</em>）</p></td>
<td></td>
<td><p>提名-<a href="../Page/第72屆金球獎.md" title="wikilink">第72屆金球獎最佳女配角</a><br />
榮獲-<a href="../Page/好萊塢電影獎.md" title="wikilink">好萊塢電影獎最佳女配角</a><br />
提名-<a href="../Page/第87屆奧斯卡金像獎.md" title="wikilink">第87屆奧斯卡金像獎最佳女配角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/聖母峰_(電影).md" title="wikilink">聖母峰</a>（<em>Everest</em>）</p></td>
<td><p>Jan Hall</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/最美的安排.md" title="wikilink">最美的安排</a>（<em>Collateral Beauty</em>）</p></td>
<td><p>Aimee Moore / "Love"</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><a href="../Page/加勒比海盜5：死無對證.md" title="wikilink">加勒比海盜5：死無對證</a>（''Pirates of the Caribbean: Dead Men Tell No Tales ''）</p></td>
<td><p><a href="../Page/伊莉莎白·史旺.md" title="wikilink">伊莉莎白·史旺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p>（<em>Colette</em>）</p></td>
<td><p><a href="../Page/科萊特.md" title="wikilink">科萊特</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Rachael Morgan</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡桃鉗與奇幻四國.md" title="wikilink">胡桃鉗與奇幻四國</a>（<em>The Nutcracker and the Four Realms</em>）</p></td>
<td><p>糖梅仙子</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/柏林，我愛你.md" title="wikilink">柏林我愛你</a>[28]</p></td>
<td><p>珍</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 錄音作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>電影配音</p></th>
<th><p>歌名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/神鬼奇航3：世界的盡頭.md" title="wikilink">神鬼奇航3：世界的盡頭</a>（Pirates of the Caribbean: At World's End (soundtrack)）</p></td>
<td><p>"Hoist the Colours"</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/愛的邊緣.md" title="wikilink">愛的邊緣</a>（The Edge of Love (Music from the Original Motion Picture)）</p></td>
<td><p>"Overture / Blue Tahitian Moon" feat. <a href="../Page/Angelo_Badalamenti.md" title="wikilink">Angelo Badalamenti</a><br />
"After the Bombing / Hang Out the Stars in Indiana" feat. <a href="../Page/Angelo_Badalamenti.md" title="wikilink">Angelo Badalamenti</a><br />
"Drifting and Dreaming" feat. <a href="../Page/Angelo_Badalamenti.md" title="wikilink">Angelo Badalamenti</a><br />
"Maybe It's Because I Love You Too Much" feat. <a href="../Page/Angelo_Badalamenti.md" title="wikilink">Angelo Badalamenti</a></p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/曼哈頓戀習曲.md" title="wikilink">曼哈頓戀習曲</a>（Begin Again (Music From and Inspired By the Original Motion Picture)）</p></td>
<td><p>"Tell Me If You Wanna Go Home"<br />
"Lost Stars"<br />
"Like a Fool"<br />
"Coming Up Roses"<br />
"A Step You Can't Take Back"<br />
"Tell Me If You Wanna Go Home [Rooftop Mix]" feat. <a href="../Page/Hailee_Steinfeld.md" title="wikilink">Hailee Steinfeld</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

### 引用

### 来源

  -
## 外部連結

  -
  -
  -
  - 姬拉在上的[簡歷](https://web.archive.org/web/20070602162339/http://www.pfd.co.uk/clients/knightlk/a-act.html)

  -
{{-}}

[Category:21世纪英格兰女演员](../Category/21世纪英格兰女演员.md "wikilink")
[Category:英格蘭舞台演員](../Category/英格蘭舞台演員.md "wikilink")
[Category:英格兰女电视演员](../Category/英格兰女电视演员.md "wikilink")
[Category:英格兰女童星](../Category/英格兰女童星.md "wikilink")
[Category:英格蘭女性模特兒](../Category/英格蘭女性模特兒.md "wikilink")
[Category:英格兰女电影演员](../Category/英格兰女电影演员.md "wikilink")
[Category:蘇格蘭裔英格蘭人](../Category/蘇格蘭裔英格蘭人.md "wikilink")
[Category:威爾斯裔英格蘭人](../Category/威爾斯裔英格蘭人.md "wikilink")
[Category:英格蘭無神論者](../Category/英格蘭無神論者.md "wikilink")
[Category:失读症患者](../Category/失读症患者.md "wikilink")

1.  見[Keira
    Knightley的讀音](http://inogolo.com/pronunciation/d97/Keira_Knightley)。

2.

3.

4.

5.

6.

7.
8.

9.

10.

11.

12.

13. [](http://www.maximonline.com/slideshows/index.aspx?slideId=1914&imgCollectId=94&src=wiki)

14.
15.

16.

17.

18.

19.

20.

21.

22.

23.

24.
25.

26.

27. [2](http://www.dailymail.co.uk/tvshowbiz/article-3223148/Keira-Knightley-s-husband-James-Righton-cradles-new-born-daughter-step-NYC.html)

28.