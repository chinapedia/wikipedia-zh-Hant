**威克島**（）是[馬紹爾群島中的小岛](../Page/馬紹爾群島.md "wikilink")、北[太平洋上的環礁](../Page/太平洋.md "wikilink")，海岸線長19公里，大約自[夏威夷至](../Page/夏威夷.md "wikilink")[北馬里亞納群島的三分之二航程](../Page/北馬里亞納群島.md "wikilink")。\[1\]因為它在[國際換日線之西](../Page/國際換日線.md "wikilink")，所以比[美國](../Page/美國.md "wikilink")50州快近一日，使用UTC+12標準時。威克島是美國的[無建制領地](../Page/無建制領地.md "wikilink")，面積6.5平方公里，由威克、威爾克斯和皮爾三個小礁島組成，位於太平洋中被海水淹沒的火山島群中，\[2\]東距[夏威夷](../Page/夏威夷.md "wikilink")3200公里，西距[關島](../Page/關島.md "wikilink")2060公里，戰略地位重要，被稱為“太平洋的踏腳石”。威克島暫住人口包括美國軍人、文職人員及一些[外勞和](../Page/外勞.md "wikilink")[軍方承包商等數百人](../Page/國防承包商.md "wikilink")，食品和工業品完全依靠進口。

威克島郵政和電話編制上屬於夏威夷（郵區號碼96898、電話區號+1-808），統計上屬於「[美國本土外小島嶼](../Page/美國本土外小島嶼.md "wikilink")」（United
States Minor Outlying Islands）之一（[ISO
3166-1國碼UM](../Page/ISO_3166-1.md "wikilink")）。

<File:Wake> Island air.JPG|威克島全貌 <File:Orthographic> projection over
Wake Island.png|威克島位置圖 <File:Wake> Island Memorial Chapel damaged by
Hurricane-Typhoon Ioke 2006.jpg|2006年風災過後

## 历史

威克島上原無居民，1568年一支西班牙探險隊首次發現該島。1796年，英國船長[威廉·威克正式注明此島](../Page/威廉·威克.md "wikilink")，並以他的名字命名該島。此後該島一度湮沒無聞，直到1841年美國海軍在原島重新修建。1899年美國政府正式將該島佔為己有。1935年威克島劃歸美國海軍管轄並在島上建立商用水上飛機基地和旅館，為[泛美航空橫跨太平洋的客機提供服務](../Page/泛美航空.md "wikilink")。[二戰期間威克島](../Page/二戰.md "wikilink")。1962年美國在島上建成了現代化機場並轉交內政部管轄，但實際上是由[美國空軍管轄](../Page/美國空軍.md "wikilink")。1964年完成了新的[海底電纜的鋪設](../Page/海底電纜.md "wikilink")。該島還是[檀香山和](../Page/檀香山.md "wikilink")[關島海底電纜的連結點](../Page/關島.md "wikilink")。1972年，該島由美國國防部接管，民事方面授權空軍總律師管轄，並由威克島駐軍司令作為他的代理人。1974年用作導彈試驗基地，1975年曾收容[越戰難民](../Page/越南船民.md "wikilink")。70年代中期至今，[威克島機場一直是軍用和民用飛機的緊急降落](../Page/威克島機場.md "wikilink")[備用機場](../Page/備降機場.md "wikilink")，也是美軍飛機從[檀香山到](../Page/檀香山.md "wikilink")[東京和](../Page/東京.md "wikilink")[關島的加油站](../Page/關島.md "wikilink")。

## 地理\[3\]

  - 經緯度：
  - 陸地面積：6.5平方公里
  - 海岸線：19.3公里
  - 海域主權聲索
      - 經濟專區：200海里（370.4公里）
      - 領海：12海里（22.2公里）
  - 氣候：[熱帶](../Page/熱帶.md "wikilink")，有偶然的[颱風](../Page/颱風.md "wikilink")
  - 高度極點：
      - 最低點：太平洋0米
      - 最高點：未命名地點6米

## 參考資料

<references/>

[Category:大洋洲](../Category/大洋洲.md "wikilink")
[Category:美國屬地](../Category/美國屬地.md "wikilink")
[Category:太平洋島嶼](../Category/太平洋島嶼.md "wikilink")
[Category:威克岛](../Category/威克岛.md "wikilink")
[Category:2009年設立的保護區](../Category/2009年設立的保護區.md "wikilink")

1.  美國中央情報局[世界概况](../Page/世界概况.md "wikilink")：[威克島](https://www.cia.gov/library/publications/the-world-factbook/geos/wq.html)

2.
3.