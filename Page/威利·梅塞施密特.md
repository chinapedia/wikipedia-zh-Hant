**威廉·艾梅爾·“威利”· 梅塞施密特**（**Wilhelm Emil "Willy"
Messerschmitt**，）是一位著名的[德國](../Page/德國.md "wikilink")[飛機設計家和製造家](../Page/飛機.md "wikilink")。梅塞施密特出生在德國[法蘭克福](../Page/法蘭克福.md "wikilink")，生父是一位酒商。他的繼父是一位任教於[慕尼黑藝術學院的美國教授](../Page/慕尼黑藝術學院.md "wikilink")－[卡爾·馮·馬爾](../Page/卡爾·馮·馬爾.md "wikilink")。他是1938年第二届[德国国家艺术与科学奖获奖者](../Page/德国国家艺术与科学奖.md "wikilink")。

梅塞施密特最出名的設計大概就是屬於與[華特·雷特爾共同研發的](../Page/華特·雷特爾.md "wikilink")[Bf
109戰鬥機了](../Page/Bf_109戰鬥機.md "wikilink")。Bf
109戰鬥機在[二次世界大戰中成為](../Page/二次世界大戰.md "wikilink")[德國空軍最重要的](../Page/德國空軍.md "wikilink")[戰鬥機](../Page/戰鬥機.md "wikilink")。直至今日，它仍然是產量最多的戰鬥機，大約有35,000架。另一架梅塞施密特所設計的飛機－[Me
209戰鬥機](../Page/Me_209戰鬥機.md "wikilink")，則打破了活塞螺槳飛機平飛速度的世界絕對記錄。他的公司－[梅塞施密特也製造了世界第一種服役的](../Page/梅塞施密特.md "wikilink")[噴射戰鬥機](../Page/噴射機.md "wikilink")，雖然梅塞施密特本人並沒有親自參與設計。

## 創業初期

1932年梅塞施密特於[奥格斯堡創立了屬於自己的公司](../Page/奥格斯堡.md "wikilink")。初期，梅塞施密特只建造一些[滑翔機](../Page/滑翔機.md "wikilink")，但兩年後便著手研發競賽用和遊憩用動力飛機。梅塞施密特的事業於1927年接獲[巴伐利亞飛機製造廠](../Page/巴伐利亞飛機製造廠.md "wikilink")（簡稱BFW）對[梅塞施密特
M17和](../Page/梅塞施密特_M17.md "wikilink")[梅塞施密特
M18的訂單時達到頂峰](../Page/梅塞施密特_M18.md "wikilink")，[巴伐利亞當局甚至希望兩家公司能夠合併](../Page/巴伐利亞.md "wikilink")。然而在1928年，[梅塞施密特
M20輕型運輸機的墜機意外卻重重地打擊了BFW和梅塞施密特本人](../Page/梅塞施密特_M20.md "wikilink")。兩架[漢莎航空的M](../Page/德国汉莎航空.md "wikilink")20在成交不久便先後發生意外，導致漢莎航空取消所有此型號的訂單。這件事讓梅塞施密特的公司出現嚴重的週轉不靈，便於1931年破產。M20的墜機事故也造就了梅塞施密特最大的敵人－[艾爾哈德·米爾希](../Page/艾爾哈德·米爾希.md "wikilink")。這位漢莎航空的老闆因為在一次M20的墜機事故中失去了他的摯友所以對梅塞施密特懷恨在心。

## 納粹德國和二次大戰

1933年[納粹政府建立](../Page/納粹德國.md "wikilink")[帝國航空部](../Page/帝國航空部.md "wikilink")，由米爾希擔任部長，志在振興德國航空業並讓BFW起死回生。梅塞施密特和[羅伯特·魯塞爾共通合作為重振公司設計出一架下單翼機](../Page/羅伯特·魯塞爾.md "wikilink")，名為
“[Bf 108颱風式](../Page/Bf_108颱風式.md "wikilink")”。隔年，梅塞施密特根據Bf
108的特點設計出了[Bf 109戰鬥機](../Page/Bf_109戰鬥機.md "wikilink")

然而，梅塞施密特卻幾乎無法在米爾希的眼皮底下拿到政府的訂單，無法在國內生存的他只好轉而向[羅馬尼亞簽訂協議](../Page/羅馬尼亞.md "wikilink")，出售M37和[梅塞施密特
M36運輸機](../Page/梅塞施密特_M36.md "wikilink")。當米爾希得知此事，他公開指責梅塞施密特為叛國賊，並命令[蓋世太保去質問梅塞施密特和公司的其他成員](../Page/蓋世太保.md "wikilink")。幸好，梅塞施密特和納粹高官[鲁道夫·赫斯和](../Page/鲁道夫·赫斯.md "wikilink")[赫尔曼·戈林有著密切的聯繫才能讓後來的審問不了了之](../Page/赫尔曼·戈林.md "wikilink")。

Bf
109戰鬥機於1936年贏得了帝國航空部所舉辦的單坐戰鬥機大賽，從而成為[德國空軍最主要的戰鬥機機型](../Page/德國空軍.md "wikilink")，之後的Bf
110也脫穎而出，成為多用途戰鬥機的贏家。梅塞施密特的公司從此成為德國重整軍備的關鍵部分。

1938年7月11日梅塞施密特被任命為BFW的董事長和總經理，而公司也重新命名為[梅塞施密特股份公司](../Page/梅塞施密特.md "wikilink")。同一年，梅塞施密特公司開始進行[Me
262戰鬥機和用來替代Bf](../Page/Me_262戰鬥機.md "wikilink") 110的[Me
210重型戰鬥機的研究](../Page/Me_210戰鬥機.md "wikilink")。Me
210在開發過程中出現許多問題，直到後來的Me 410才獲得解決，可是這一延遲卻傷害了梅塞施密特和以他為名的公司的信譽。

## 戰後

因為被控奴役勞工，梅塞施密特入監服刑兩年。出獄後的梅塞施密特重新指掌他的公司，但是因為德國被禁止生產航空器直到1955年，梅塞施密特公司轉而生產裁縫車和小型汽車。因為他的才能，梅塞施密特應邀為西班牙設計[Hispano
HA-200教練機](../Page/Hispano_HA-200教練機.md "wikilink")，直到禁令解除，重新回到德國製造[Fiat
G.91攻擊機和洛克希德的](../Page/Fiat_G.91攻擊機.md "wikilink")[F-104星式戰鬥機](../Page/F-104星式戰鬥機.md "wikilink")。爾後他還設計了[Helwan
HA-300超音速攔截機](../Page/Helwan_HA-300攔截機.md "wikilink")。

梅塞施密特於1970年退休，8年後去世於[慕尼黑的一家醫院中](../Page/慕尼黑.md "wikilink")。

## 參見

  - Frank Vann: Willy Messerschmitt. First full biography of an
    aeronautical genius. Sparkford: Stephens, 1993

[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:納粹德國人物](../Category/納粹德國人物.md "wikilink")
[Category:德國企業家](../Category/德國企業家.md "wikilink")
[Category:德國工程師](../Category/德國工程師.md "wikilink")
[Category:飞机设计师](../Category/飞机设计师.md "wikilink")
[Category:慕尼黑工業大學教師](../Category/慕尼黑工業大學教師.md "wikilink")
[Category:慕尼黑工業大學校友](../Category/慕尼黑工業大學校友.md "wikilink")
[Category:法蘭克福人](../Category/法蘭克福人.md "wikilink")