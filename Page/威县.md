**威县**在[河北省南部](../Page/河北省.md "wikilink")，是[邢台市下辖的一个县](../Page/邢台.md "wikilink")。县人民政府驻洺州镇。

## 历史

威县历史悠久，[西汉后期](../Page/西汉.md "wikilink")，汉平帝封宗亲刘如意为广宗王，置广宗国（治所在今县城东8公里方家营），威县地属之；[隋初置经城县](../Page/隋.md "wikilink")，后分置府城县属[贝州](../Page/贝州.md "wikilink")（今清河）；601年，因避太子杨广名讳，改广宗县为宗城县；[元太宗六年](../Page/元.md "wikilink")（1234年）宗城县并入洺水县，仍属洺州；1247年，洺水县遥隶邢洺路威州（州治在今井陉县）；[明洪武二年](../Page/明.md "wikilink")（1369年）四月，威州降为县，始称威县，属广平府。

## 行政区划

下辖9个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 链接

  - [邢台市威县商务之窗](https://web.archive.org/web/20090416154522/http://xingtaiweixian.mofcom.gov.cn/)

[威县](../Page/category:威县.md "wikilink")
[县](../Page/category:邢台区县市.md "wikilink")
[邢台](../Page/category:河北省县份.md "wikilink")