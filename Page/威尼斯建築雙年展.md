**威尼斯建築雙年展**（，），在意大利[威尼斯举行的建筑展览](../Page/威尼斯.md "wikilink")。

## 歷史

  - 2004年以“蜕变”的主题呈现当代建筑的历史观
  - 2006年的“超越城市”希望解决城市发展规划方面的焦点议题：第十次的展覽在2006年舉行，主題為“超大型城市：城市規劃中的問題”，日期为2006年9月－11月19日。
  - 2008年的“盖房子之外的建筑”将展览领入装置世界，建筑理论家们始终希望从辩证的角度来引导建筑展览的未来发展方向
  - 2010年的“人们相逢于建筑”把建筑仅作为事件、人和社会的“容器”

## 外部連結

  - [Venice Biennale
    website](https://web.archive.org/web/20170610074245/http://www.labiennale.org/en/architecture/exhibition%7C)
  - \[<http://archrecord.construction.com/biennale2006/default.asp>|
    Architectural Record 2006 Venice Biennale website\]

[Category:雙年展](../Category/雙年展.md "wikilink")
[Category:建築展覽](../Category/建築展覽.md "wikilink")
[Category:欧洲建筑](../Category/欧洲建筑.md "wikilink")
[Category:威尼斯文化](../Category/威尼斯文化.md "wikilink")
[Category:1980年建立](../Category/1980年建立.md "wikilink")