**威廉·哈里斯·克劳福德**（**William Harris
Crawford**，），[美国政治家](../Page/美国.md "wikilink")，[美国民主-共和党成员](../Page/美国民主-共和党.md "wikilink")，曾任[美国参议员](../Page/美国参议员.md "wikilink")（1807年-1813年）、[美国战争部长](../Page/美国战争部长.md "wikilink")（1815年-1816年）和[美国财政部长](../Page/美国财政部长.md "wikilink")（1816年-1825年）。

[C](../Category/1772年出生.md "wikilink")
[C](../Category/1834年逝世.md "wikilink")
[C](../Category/美国战争部长.md "wikilink")