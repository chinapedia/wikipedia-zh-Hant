**威廉·施文克·吉尔伯特**爵士（，），[英国](../Page/英国.md "wikilink")[剧作家](../Page/剧作家.md "wikilink")、[文学家](../Page/文学家.md "wikilink")、[诗人](../Page/诗人.md "wikilink")。他与[作曲家](../Page/作曲家.md "wikilink")[阿瑟·萨利文](../Page/阿瑟·萨利文.md "wikilink")[合作的](../Page/吉爾伯特與萨利文.md "wikilink")14部[喜剧闻名于世](../Page/喜剧.md "wikilink")，其中最著名的为《[皮纳福号军舰](../Page/皮纳福号军舰.md "wikilink")》（*[H.M.S.
Pinafore](../Page/:en:H.M.S._Pinafore.md "wikilink")*）、《[彭赞斯的海盗](../Page/彭赞斯的海盗.md "wikilink")》（*[The
Pirates of
Penzance](../Page/:en:The_Pirates_of_Penzance.md "wikilink")*）以及在[音乐剧院中表演场次最多的](../Page/音乐剧院.md "wikilink")[历史剧之一](../Page/历史剧.md "wikilink")《[日本天皇](../Page/日本天皇_\(戲劇\).md "wikilink")》（*[The
Mikado](../Page/:en:The_Mikado.md "wikilink")*）\[1\]\[2\]。这些作品以及他们其他[萨沃伊歌剧作品如今在使用英语的国家依旧经常演出](../Page/萨沃伊歌剧.md "wikilink")，包括歌剧公司、剧团、学校和社区演出团体。这些作品中的一些台词已经成为[英语的一部分](../Page/英语.md "wikilink")\[3\]\[4\]。

## 参考资料

[Category:英国剧作家](../Category/英国剧作家.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:倫敦國王學院校友](../Category/倫敦國王學院校友.md "wikilink")

1.  [Kenrick, John, 《G\&S Story: Part
    III》](http://www.musicals101.com/gilbert3.htm)，2008年6月19日查阅。
2.  [Powell, Jim, 《William S. Gilbert's Wicked Wit for
    Liberty》](http://www.libertystory.net/LSARTSGILBERT.htm)，2008年6月19日查阅。
3.  [Lawrence, Arthur H. 《An illustrated interview with Sir Arthur
    Sullivan》](http://math.boisestate.edu/GaS/other_sullivan/lawrence/lawrence_3.html)
     Part 3, from *The Strand Magazine*, Vol. xiv, No.84 (December 1897)
4.  Green, Edward, [《Ballads, songs, and
    speeches》](http://news.bbc.co.uk/2/hi/uk_news/magazine/3634126.stm)，BBC，2004年9月20日发表，2008年6月19日查阅。