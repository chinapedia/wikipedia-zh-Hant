[Captain_William_Caine.jpg](https://zh.wikipedia.org/wiki/File:Captain_William_Caine.jpg "fig:Captain_William_Caine.jpg")

**威廉·堅**（又譯**金尼**；[英文](../Page/英文.md "wikilink")：**William
Caine**，1799年3月17日－1871年9月19日），出生於[愛爾蘭](../Page/愛爾蘭.md "wikilink")，於[鴉片戰爭時](../Page/鴉片戰爭.md "wikilink")，為[英國軍隊第](../Page/英國軍隊.md "wikilink")26團上尉。[英國佔領](../Page/英國.md "wikilink")[香港島後](../Page/香港島.md "wikilink")，由於行政官[義律需要一名對](../Page/義律.md "wikilink")[中國法律稍有認識的軍官作為](../Page/中國法律.md "wikilink")[法官](../Page/法官.md "wikilink")，威廉·堅被認為是最適合的人選，於1841年4月30日成為[香港政府首任](../Page/香港殖民地時期#香港政府.md "wikilink")[裁判官](../Page/裁判官.md "wikilink")，於3年後建立[香港警察隊](../Page/香港警察隊.md "wikilink")，並且兼辦[監獄事務](../Page/監獄.md "wikilink")，直至於1859年9月離任，共服務18年，共歷5朝[香港總督及](../Page/香港總督.md "wikilink")[副香港總督](../Page/副香港總督.md "wikilink")。

## 軼事

位於[香港島](../Page/香港島.md "wikilink")[半山區的](../Page/半山區.md "wikilink")[堅道就是以威廉](../Page/堅道.md "wikilink")·堅命名。《[華友西報](../Page/華友西報.md "wikilink")》編輯[泰倫](../Page/泰倫.md "wikilink")（William
Tarrant）曾經批評他的[貪污行為](../Page/貪污.md "wikilink")，結果被下囚。

## 外部連結

  - [香港早年監獄發展歷史](https://web.archive.org/web/20061213145039/http://www.csd.gov.hk/tc_chi/abt/abt_his/abt_his_early.html)

|width=20% align=center|**前任：**
首任 |width=60% align=center|**[總裁判司](../Page/總裁判司.md "wikilink")**
1841年4月30日─1844年 |width=20% align=center|**繼任：**
[希利](../Page/希利.md "wikilink")（Captain Haly）

[C](../Category/香港總督.md "wikilink")
[C](../Category/香港歷史人物.md "wikilink")
[C](../Category/香港政治人物.md "wikilink")
[C](../Category/英國政治人物.md "wikilink")
[Category:香港警務處處長](../Category/香港警務處處長.md "wikilink")
[C](../Category/前香港政府官員.md "wikilink")
[C](../Category/前香港行政局議員.md "wikilink")
[C](../Category/前香港立法局議員.md "wikilink")
[Category:第一次鴉片戰爭人物](../Category/第一次鴉片戰爭人物.md "wikilink")