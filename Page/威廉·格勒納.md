[Bundesarchiv_Bild_183-1986-0425-500,_Wilhelm_Groener.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1986-0425-500,_Wilhelm_Groener.jpg "fig:Bundesarchiv_Bild_183-1986-0425-500,_Wilhelm_Groener.jpg")
**威廉·格勒纳**全名是**卡尔·爱德华·威廉·格勒纳** ( )
，是一位[德国将军及政治家](../Page/德国.md "wikilink")，最高軍階為陸軍中將。

威廉·格勒纳出生于[符腾堡的](../Page/符腾堡.md "wikilink")[路德维希堡](../Page/路德维希堡.md "wikilink")，父亲是军部主计员。1884年，他加入符腾堡军队，于1893至1897年入读军事学院，后来在1899年升任到[德国参谋部](../Page/德国参谋部.md "wikilink")。在往后十七年之间，他一直从事铁路部门的工作，并在1912年升任主管。1916年11月，格勒纳转而到[普鲁士陆军部](../Page/普鲁士陆军部.md "wikilink")
(Prussian Minister of War)
担任副部长并监控军事生产。1917年8月，他到[乌克兰指挥作战](../Page/乌克兰.md "wikilink")。

1918年10月29日，[最高陆军指挥总指挥官](../Page/最高陆军指挥.md "wikilink") (Quartiermeister)
[埃里希·鲁登道夫辞职](../Page/埃里希·鲁登道夫.md "wikilink")，格勒纳升任为第二任总指挥官，职位仅次于参谋总长[保罗·冯·兴登堡元帅](../Page/保罗·冯·兴登堡.md "wikilink")。德军当时在[盟军猛攻下节节败退](../Page/盟军.md "wikilink")，而士兵哗变、社会动荡不安接踵而来，[德国革命爆发](../Page/德国革命.md "wikilink")。11月，格勒纳向德皇[威廉二世表示](../Page/威廉二世_\(德国\).md "wikilink")，后者已经失去军队支持，故此劝喻皇帝[逊位](../Page/逊位.md "wikilink")。

1918年11月9日，皇帝终于退位。[马克思主义的](../Page/马克思主义.md "wikilink")[斯巴达克同盟在柏林宣布成立一个](../Page/斯巴达克同盟.md "wikilink")[苏维埃共和国](../Page/苏维埃共和国.md "wikilink")。新任总理、[德国社民党主席](../Page/德国社会民主党.md "wikilink")[弗里德里希·艾伯特与](../Page/弗里德里希·艾伯特.md "wikilink")[腓力·赛德曼希望阻止共产党人](../Page/腓力·赛德曼.md "wikilink")
(后来更组成[德国共产党](../Page/德国共产党.md "wikilink")) 的行动，他们在没有周详计划下成立了共和国。

作为德军第二号人物的格勒纳，在当晚与艾伯特联络。二人达成了[艾伯特-格勒纳协定](../Page/艾伯特-格勒纳协定.md "wikilink")，并在几年之内将之保守为秘密。
艾伯特同意镇压[革命](../Page/德国革命.md "wikilink")，并答应维持被打败的德军作为德国重要支柱之地位；格勒纳则答应他的军队会把权力交给新政府。格勒纳因此在军部不太受欢迎，基于大部分将领都希望借助军队帮助皇帝复辟。

1918年11月11日德国投降，结束了[第一次世界大战](../Page/第一次世界大战.md "wikilink")。格勒纳其后监督德军的撤退与解散。

1919年9月30日，他退出军队。1920年代，他数次退休及复出。1920至23年，他出任交通部长；1928年，他接替[奥托·格斯勒为国防部长](../Page/奥托·格斯勒.md "wikilink")，直到1932年；1931至32年，他是内政部长。他支持禁制[纳粹党的](../Page/纳粹党.md "wikilink")[冲锋队](../Page/冲锋队.md "wikilink")。[弗朗茨·冯·帕彭接替](../Page/弗朗茨·冯·帕彭.md "wikilink")[海因里希·布吕宁成为](../Page/海因里希·布吕宁.md "wikilink")[德国总理后](../Page/德国总理.md "wikilink")，格勒纳退出政坛。

格勒纳曾结婚两次，并先后与两位妻子育有两名子女。他在1939年于[波茨坦逝世](../Page/波茨坦.md "wikilink")。

## 参考文献

  - Wheeler-Bennett, Sir John *The Nemesis of Power: German Army in
    Politics, 1918-1945* New York: Palgrave Macmillan Publishing
    Company, 2005.

[G](../Category/德國元帥.md "wikilink")
[G](../Category/德國政治人物.md "wikilink")
[G](../Category/1864年出生.md "wikilink")
[G](../Category/1939年逝世.md "wikilink")
[G](../Category/德国政府部长.md "wikilink")