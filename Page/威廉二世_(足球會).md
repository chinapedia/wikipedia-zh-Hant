**威廉二世（Willem
II）**，是一間位於[荷蘭](../Page/荷蘭.md "wikilink")[蒂爾堡的足球會](../Page/蒂爾堡.md "wikilink")，成立於1896年。球會的球衣由紅白藍三色垂直間條組成，而他們的主場是。球場於1995年5月31日啟用，可容納14,700人，而2004/05球季的平均入座人數為12,500人。

## 歷史

**威廉二世**在1896年8月12日以蒂爾堡人（Tilburgia）之名成立。在1898年1月12日，球會更名為威廉二世，因為[荷蘭國王](../Page/荷蘭.md "wikilink")[威廉二世](../Page/威廉二世_\(尼德兰\).md "wikilink")（1840年-1849年）作為（House
of
Orange-Nassau）的太子，和[荷蘭皇家陸軍的總司令](../Page/荷蘭皇家陸軍.md "wikilink")，1830年當[比利時崛起時](../Page/比利時.md "wikilink")，在蒂爾堡設立軍事總部。在2004年，球會在名字後面加上蒂爾堡（Tilburg）。

威廉二世贏得三次聯賽冠軍（1916、1952、1955）和兩次[荷蘭杯冠軍](../Page/荷蘭杯.md "wikilink")（1944、1963），並在1987、1990、1999年獲選為年度最佳球會。在1999年，球會獲得[歐洲聯賽冠軍杯的參賽資格](../Page/歐洲聯賽冠軍杯.md "wikilink")，在分組賽（G組）6場得2分出局。在1963年，球會在[歐洲杯賽冠軍杯第一圈](../Page/歐洲杯賽冠軍杯.md "wikilink")，以總比數2-7被[曼聯淘汰](../Page/曼聯.md "wikilink")。在1998/99球季，球會參加了[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")，在第一圈以總比數6-0淘汰[格魯吉亞球會](../Page/格魯吉亞.md "wikilink")[第比利斯戴拿模](../Page/第比利斯戴拿模.md "wikilink")，但在第二圈被[西班牙球會](../Page/西班牙.md "wikilink")[貝迪斯以總比數](../Page/貝迪斯.md "wikilink")1-4淘汰。

在2005-06球季，球會在留級附加賽以總比數3-1（首回合1-0，次回合2-1）擊敗[迪加史卓普](../Page/迪加史卓普.md "wikilink")，得以繼續留在甲組。

## 榮譽

  - **[荷蘭甲組足球聯賽](../Page/荷蘭甲組足球聯賽.md "wikilink")**
      - '''冠軍(3): ''' 1916, 1952, 1955
  - **[荷蘭乙組足球聯賽](../Page/荷蘭乙組足球聯賽.md "wikilink")**
      - '''冠軍(3): ''' 1957, 1965, 2014
      - '''亞軍(1): ''' 1987
  - **[荷蘭杯](../Page/荷蘭杯.md "wikilink")**
      - '''冠軍(2): ''' 1944, 1963
      - '''亞軍(1): ''' 2005

## 著名球員

  - [希比亞](../Page/薩米·海皮亞.md "wikilink")

  - [蘭沙特](../Page/丹尼·兰扎特.md "wikilink")

  - [奧華馬斯](../Page/馬克·奧維馬斯.md "wikilink")

  - [史譚](../Page/雅普·斯塔姆.md "wikilink")

## 外部連結

  - [官方網站](http://www.willem-ii.nl)

[W](../Category/荷蘭足球俱樂部.md "wikilink")
[Category:1896年建立的足球會](../Category/1896年建立的足球會.md "wikilink")