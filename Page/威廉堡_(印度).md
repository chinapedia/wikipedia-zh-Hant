[1844_Map_of_Fort_William_and_Esplanade.jpg](https://zh.wikipedia.org/wiki/File:1844_Map_of_Fort_William_and_Esplanade.jpg "fig:1844_Map_of_Fort_William_and_Esplanade.jpg")

**威廉堡**是位于印度[加尔各答](../Page/加尔各答.md "wikilink")[胡格利河](../Page/胡格利河.md "wikilink")（[恒河的一条重要支流](../Page/恒河.md "wikilink")）东岸的一座城堡，建于[英属印度时期](../Page/英属印度.md "wikilink")，得名于英格兰国王[威廉三世](../Page/威廉三世.md "wikilink")。堡垒前面是[Maidan](../Page/Maidan_\(Kolkata\).md "wikilink")，通常作为堡垒的一部分，是加尔各答最大的城市公园。

## 历史

事实上共有2个威廉堡：旧威廉堡和新威廉堡。最初的城堡由[英国东印度公司建造](../Page/英国东印度公司.md "wikilink")，在John
Goldsborough的监督之下 。Charles Eyre 爵士首先开始建造旧城堡得东南棱堡和邻近的城墙。1701年，John
Beard，他的继任者, 加筑了东北棱堡，1702年，他开始建造政府

城堡呈[八角形](../Page/八角形.md "wikilink")，三面临[胡格利河](../Page/胡格利河.md "wikilink")。旧城堡自1766年起经修复作为海关使用。

新城堡还在使用，作为印度军队东部军区的总部。

## 建筑

## 外部链接

  - [Fort William on
    catchcal.com](http://www.catchcal.com/kaleidoscope/places_to_visit/fort_william.asp)
  - [Governor Generals of Fort William
    (1774-1834)](https://web.archive.org/web/20070927190717/http://rajbhavankolkata.gov.in/html/governors03.htm)
  - [Governors of the presidency of Fort William in West Bengal and
    Governor Generals of India
    (1834-1854)](https://web.archive.org/web/20070124181939/http://rajbhavankolkata.gov.in/html/governors04.htm)

## 参见

  - [威廉堡学院](../Page/威廉堡学院.md "wikilink")

[Category:加爾各答建築物](../Category/加爾各答建築物.md "wikilink")
[Category:加爾各答觀光地](../Category/加爾各答觀光地.md "wikilink")
[Category:加尔各答历史](../Category/加尔各答历史.md "wikilink")
[Category:印度城堡](../Category/印度城堡.md "wikilink")