**威斯康星大学密尔沃基分校** (University of Wisconsin-Milwaukee, UWM, UW-Milwaukee)
位于[美国](../Page/美国.md "wikilink")[威斯康星州的文化和经济中心](../Page/威斯康星州.md "wikilink")[密尔沃基市](../Page/密尔沃基市.md "wikilink")，是[美国](../Page/美国.md "wikilink")[威斯康星大学体系中两所可授予](../Page/威斯康星大学.md "wikilink")[博士学位的综合研究型大学之一](../Page/博士学位.md "wikilink")。该校是在密尔沃基大都市地区最大的大学，也是该州第二大的大学。

威斯康星大学密尔沃基分校，坐落于[密歇根湖畔西岸](../Page/密歇根湖.md "wikilink")。该校包括14所学院和70多个学术中心、研究所和实验室，提供180个学位课程，包括94个学士、53个硕士和32个博士学位。\[1\]

这所大学的运动队称为美洲豹。15个美洲豹运动队在美国NCAA-I竞技。

## 历史

[State_Normal_School_at_Milwaukee.jpeg](https://zh.wikipedia.org/wiki/File:State_Normal_School_at_Milwaukee.jpeg "fig:State_Normal_School_at_Milwaukee.jpeg")
[UWM-0904-downer-quad.jpg](https://zh.wikipedia.org/wiki/File:UWM-0904-downer-quad.jpg "fig:UWM-0904-downer-quad.jpg")
1885年，密尔沃基州立师范学校在[密尔沃基市中心地区建校开课](../Page/密尔沃基.md "wikilink")，最初仅有76名学生。在随后的42年中，学校快速发展，先后有7位校长执任，并增加了音乐、人文科学等专业。1919年，密尔沃基州立师范学校从市中心搬到密歇根湖畔西岸。1937年，学校更名为威斯康星州立师范学院。20世纪40年代，该学院成为美国顶尖的教师培训学院之一。1953年，改名为威斯康星州立密尔沃基学院。5年后，该学院与威斯康星大学合并。

1955年，[威斯康星州议会通过法案](../Page/威斯康星州.md "wikilink")，在威斯康星州最大的城市[密尔沃基建立一个大型公立大学](../Page/密尔沃基.md "wikilink")。1956年，威斯康星州立密尔沃基学院和威斯康星大学在密尔沃基的延伸机构合并成目前的威斯康星大学密尔沃基分校。1971年，威斯康星大学与威斯康星州立大学系统合并，形成威斯康星大学系统。该校成为该新系统的一分校。
1994年，威斯康星大学密尔沃基分校成为卡内基基金会（博士/研究型大学，广泛）院校。

在加入威斯康星大学系统后的50年中，威斯康星大学密尔沃基分校已扩大到14所学校和学院，拥有87个本科专业，51个硕士和31个博士课程。2006年，威斯康星大学密尔沃基分校因对其所在城市社会发展的积极贡献被新英格兰高等教育委员会（NEBHE）评为“城市救世主”之一，并被市民投票选为“密尔沃基”十大宝石之一。

## 学术概况

威斯康星密尔沃基大学是威斯康星州两个公立博士学位研究型大学，是125所美国卡内基基金会Research
II大学之一。该校淡水科学学院是美国唯一的一所淡水科学研究生院，也是世界第三所这种学院之一。该校的建筑与城市规划学院，护理学院和健康科学学院是威斯康星州规模最大的。

威斯康星密尔沃基大学在美国公立大学在国家研究理事会（NRC）排名中排名72，\[2\]是美国大学先锋排行榜的100所大学之一，在美国大学表现评估中心大学排名中排189位，\[3\]
在2011法国[国立巴黎高等矿业学校](../Page/国立巴黎高等矿业学校.md "wikilink")（EMP）基于《财富》杂志发布的各校毕业生在世界500强企业中担任首席执行官或与之相当职位的人数指标进行的世界大学排名中，该校名列第98位。\[4\]

该学院在美国公立大学在国家研究理事会（NRC）排名中排名73，\[5\]
其中[工业工程学排第](../Page/工业工程学.md "wikilink")34，\[6\]
[材料科学排第](../Page/材料科学.md "wikilink")60，\[7\]
[土木工程排第](../Page/土木工程.md "wikilink")69，\[8\]
[机械工程排第](../Page/机械工程.md "wikilink")87，\[9\]
[电子工程排第](../Page/电子工程.md "wikilink")96。\[10\]

该校建筑与城市规划学院在美国新闻与世界报道排名前20。在一期“美国最佳建筑与设计学校”年度版DesignIntelligence也名列前20名和第二次建筑与城市规划学院在中西部地区，以及并列第三的他们的'最Innovative程序'类别。

## 學院

[UWM_Mitchell_hall.jpg](https://zh.wikipedia.org/wiki/File:UWM_Mitchell_hall.jpg "fig:UWM_Mitchell_hall.jpg")
[Uw-mke-0904-sarup.jpg](https://zh.wikipedia.org/wiki/File:Uw-mke-0904-sarup.jpg "fig:Uw-mke-0904-sarup.jpg")
该大学开设152门本科, 硕士和博士学位课程，包括：

  - 建筑和城市规划学院
  - 文理学院
  - 商业管理学院
  - 教育学院
  - 工程和应用科学学院
  - 健康科学学院
  - 信息研究学院
  - 研究生院
  - 公众健康学院
  - 社会福利学院
  - 护理学院
  - 艺术学院
  - 淡水科学学院
  - 公共健康学院

## 著名校友

  - [薩帝亞·納德拉](../Page/薩帝亞·納德拉.md "wikilink"):
    [微软首席执行官](../Page/微软.md "wikilink")
  - [杰克·基尔比](../Page/杰克·基尔比.md "wikilink")：2000年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")，集成电路的两位发明人之一
  - [阿尔韦托·藤森](../Page/阿尔韦托·藤森.md "wikilink")：[秘鲁总统](../Page/秘鲁总统.md "wikilink")（1990年7月28日至2000年11月17日）
  - [果尔达·梅厄](../Page/果尔达·梅厄.md "wikilink")：[以色列总理](../Page/以色列.md "wikilink")
  - [菲尔·卡茨](../Page/菲尔·卡茨.md "wikilink")：ZIP压缩软件的发明者

## 相关连接

  - [Einstein@Home](../Page/Einstein@Home.md "wikilink")

## 参考资料

## 外部链接

  - [学校主页](http://www.uwm.edu)

[Milwaukee](../Category/威斯康辛大學.md "wikilink")
[Category:1885年創建的教育機構](../Category/1885年創建的教育機構.md "wikilink")

1.  [1](http://www4.uwm.edu/future_students/general_information/degrees/),
    University of Wisconsin–Milwaukee. Retrieved on October 13, 2009.
2.  [A Brief Summary of the NRC
    Rankings](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc1.html#PUBLIC),
    Texas A\&M University
3.  [Top American Research
    Universities](http://mup.asu.edu/research2009.pdf) ,The center for
    Measuring University Performance
4.  [The Annual Higher Education
    Survey](http://www.ensmp.fr/Actualites/PR/EMP-ranking.html) , [École
    des Mines de Paris](../Page/École_des_Mines_de_Paris.md "wikilink")
5.  [A Brief Summary of the NRC
    Rankings](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc1.html#RANKBYAREA),
    Texas A\&M University
6.  [NRC Rankings in each of the 41
    Areas:](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc41.html#area24)
    Industrial Engineering, Texas A\&M University
7.  [NRC Rankings in each of the 41
    Areas:](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc41.html#area25)
    Materials science, Texas A\&M University
8.  [NRC Rankings in each of the 41
    Areas:](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc41.html#area22)
    civil engineering, Texas A\&M University
9.  [NRC Rankings in each of the 41
    Areas:](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc41.html#area26)
    Mechanical Engineering, Texas A\&M University
10. [NRC Rankings in each of the 41
    Areas:](http://www.stat.tamu.edu/~jnewton/nrc_rankings/nrc41.html#area23)
    Electronic Engineering, Texas A\&M University