**威斯康辛州**（）是[美國](../Page/美國.md "wikilink")[中北部的一個](../Page/美國中西部.md "wikilink")[州](../Page/州.md "wikilink")，緊臨[五大湖區](../Page/五大湖區.md "wikilink")。該州的西邊為[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")，西南邊為[愛荷華州](../Page/愛荷華州.md "wikilink")，南邊為[伊利諾州](../Page/伊利諾州.md "wikilink")，東邊為[密西根湖](../Page/密西根湖.md "wikilink")，東北邊為[密西根上半島](../Page/密西根上半島.md "wikilink")，北邊為[蘇必略湖](../Page/蘇必略湖.md "wikilink")。威斯康辛州面積在全美各州排名第23，人口則排名第20。威斯康辛州共設有72個[郡](../Page/郡.md "wikilink")，州政府位於[麥迪遜](../Page/麥迪遜.md "wikilink")，最大城市則為密西根湖西岸的[密爾瓦基](../Page/密爾瓦基.md "wikilink")。美国2012年人口估算显示，该州人口为572.6万。

州名来源于[法语对某](../Page/法语.md "wikilink")[印第安地名的音译](../Page/印第安.md "wikilink")，意为“我们居住的地方”。

## 州名

威斯康辛州的英文原名是由法文中的一個印地安外來語改拼而来。它也许来自[歐及布威族語言](../Page/歐及布威族.md "wikilink")「密斯瓦新宁」（Miskwasiniing），意指「红石之地」，这也可能影响到[威斯康辛河的命名](../Page/威斯康辛河.md "wikilink")，

威斯康辛之名最初只被用於威斯康辛河，河名由法文记载的「威斯孔桑」（Ouisconsin），意思是遍佈青草，而後改至今日的英文名称（Wisconsin）。
在威斯康辛正式成为一州後泛指整个州。

在现今歐及布威语中的「威属寇星」（Wiishkoonsing）或「瓦沙斯寇星」（Wazhashkoonsing），意指「麝鼠栖息之地」或「小麝鼠之地」。其它說法则有威斯康辛之名源于「水的聚会」或「巨石」之说。

## 地理

威斯康辛州位於美国中西部北邊，别称[獾州](../Page/獾.md "wikilink")。西北濒苏必利尔湖，首府[麦迪逊](../Page/麦迪逊.md "wikilink")。

北部是苏必利尔高地，南部是平原，有1万多个湖泊。第四纪时全境除西南部外，均遭[冰川覆盖](../Page/冰川.md "wikilink")，故多数为[冰蚀湖](../Page/冰蚀湖.md "wikilink")。温带大陆性气候，冬季严寒、夏季炎热，受大湖调节，滨湖地带气候较温和。年平均降水量760毫米，春夏较多。森林覆盖率45％，主要分布在北部。

本州特征有二：第一，[乳牛之州](../Page/乳牛.md "wikilink")。本州全是波状地面，生长茂草，有乳牛两百多万头。全州农场之中，有80%是畜牧乳牛的农场。每年出产的牛奶、牛油及干酪，在各州之中，均列第一位。第二，植林造纸之州。本州地形、气候、土壤均宜于重新植林。通过数十年之努力，已可大量用以制纸。

本州西北滨苏必略湖，东滨密歇根湖，东北界密歇根州，西邻明尼苏达州，西南与南两方面与爱荷华及伊利诺依两州接壤。本州可分两区：北部是苏必略台地区，中部及南部是低丘浅谷区。冰河时代，密西西比河以东之本州南部，可能经受北部地势较高的影响，无不蚀冰碛地形。这一个地区叫做“无冰碛区”。其他各地，冰碛层分布极广。本州地面二分之一以上是林地，出产硬木材甚富。主要作物为[玉蜀黍](../Page/玉蜀黍.md "wikilink")、[燕麦](../Page/燕麦.md "wikilink")、[蔓橘](../Page/蔓橘.md "wikilink")、[枫糖浆](../Page/枫糖.md "wikilink")。[火鸡](../Page/火鸡.md "wikilink")、[猪](../Page/猪.md "wikilink")、[牛也多](../Page/牛.md "wikilink")。

本州工业很盛，产品总值占本州总生产值的40%。矿产有锌、铁。本州有十六个港口。其中最大的一个是位于东南部之[密爾沃基](../Page/密爾沃基.md "wikilink")，滨密歇根湖，人口140万，是美国北部制造[内燃机主要都市之一](../Page/内燃机.md "wikilink")。

威斯康星州行政区划一共有62个县。详见[威斯康星州行政区划](../Page/威斯康星州行政区划.md "wikilink")。

## 經濟

威斯康辛州被称为“奶制品之州”，本州出产的[奶酪尤其出名](../Page/奶酪.md "wikilink")。威斯康辛州居民很幽默地称自己为“奶酪大头”（cheeseheads），而且在[体育比赛](../Page/体育.md "wikilink")，[音乐会和其他公共活动中总是头戴一顶貌似圆饼形](../Page/音乐.md "wikilink")[瑞士奶酪一角的泡沫帽子](../Page/瑞士奶酪.md "wikilink")。其他有名的饮料食品包括[啤酒和](../Page/啤酒.md "wikilink")[德國油煎香腸](../Page/德國油煎香腸.md "wikilink")，[华人则对本州盛产的](../Page/华人.md "wikilink")[花旗蔘比较熟悉](../Page/花旗蔘.md "wikilink")。

该州2013年的GDP为2514亿美元，占全国总GDP的1.73%，GDP排名全美第20位，人均GDP排名全美第29位。

著名人力资源服务公司[万宝盛华](../Page/万宝盛华.md "wikilink")、保险商和汽车配件生产商[江森自控均位于该州](../Page/江森自控.md "wikilink")。

## 重要城鎮

  - [麥迪遜](../Page/麥迪遜.md "wikilink")－首府
  - [密爾沃基](../Page/密爾沃基.md "wikilink")
  - [綠灣](../Page/綠灣.md "wikilink")
  - [水清](../Page/水清.md "wikilink")（Eau Claire）
  - [拉克罗斯](../Page/拉克罗斯.md "wikilink")
  - [沃索](../Page/沃索.md "wikilink")（Wausau）－人參出品区
  - [阿普尔頓](../Page/阿普尔頓.md "wikilink")（Appleton）
  - [奧什科什](../Page/奧什科什.md "wikilink")（Oshkosh）
  - [马尼托沃克](../Page/马尼托沃克.md "wikilink")（Manitowoc）
  - [希博伊根](../Page/希博伊根.md "wikilink")（Sheboygan）
  - [拉辛](../Page/拉辛_\(威斯康星州\).md "wikilink")
  - [基诺沙](../Page/基诺沙_\(威斯康辛州\).md "wikilink")（Kenosha）

## 教育

### 州立大學

  - [威斯康辛大學](../Page/威斯康辛大學.md "wikilink")
  - [威斯康星大学密尔沃基分校](../Page/威斯康星大学密尔沃基分校.md "wikilink")

### 私立大學

  - [馬凯特大學](../Page/馬凯特大學.md "wikilink")
  - 卡羅學院（Carroll）
  - 羅倫斯大學（Lawrence University）
  - 畢洛伊特學院（Beloit College）
  - 卡爾系局學院（Carthage College）
  - 威斯康星医学院（Medical College of Wisconsin）
  - 瑞盆學院（Ripon College）
  - 聖諾伯特學院（St. Norbert College）
  - 密爾瓦基工程學校（Milwaukee School of Engineering）

## 交通

### 重要機場

  - 密爾瓦基米契爾將軍國際機場（MKE）－[中西航空](../Page/中西航空.md "wikilink")（Midwest
    Airlines）轉運中心

### 重要高速公路

  - [90號州際公路I](../Page/90號州際公路.md "wikilink")-90
  - [94號州際公路I](../Page/94號州際公路.md "wikilink")-94
  - [39號州際公路I](../Page/39號州際公路.md "wikilink")-39
  - [43號州際公路I](../Page/43號州際公路.md "wikilink")-43

## 重要體育團體

### 美式足球

  - [NFL](../Page/NFL.md "wikilink")
      - [綠灣包裝工](../Page/綠灣包裝工.md "wikilink")
  - [NCAA](../Page/NCAA.md "wikilink")
      - University of Wisconsin（Badgers）

### 棒球

  - [大聯盟](../Page/美國職棒大聯盟.md "wikilink")
      - [密尔沃基酿酒人](../Page/密尔沃基酿酒人.md "wikilink")
  - [小聯盟](../Page/小聯盟.md "wikilink")
      - 貝洛伊特齧龜（Beloit
        Snapper，1A級─中西聯盟，母隊：[奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")）
      - 威斯康辛木紋響尾蛇（Wisconsin Timber
        Rattlers，1A級─中西聯盟，母隊：[密尔沃基酿酒人](../Page/密尔沃基酿酒人.md "wikilink")）

### 籃球

  - [NBA](../Page/NBA.md "wikilink")
      - [密尔沃基雄鹿](../Page/密尔沃基雄鹿.md "wikilink")
  - [NCAA](../Page/NCAA.md "wikilink")
      - University of Wisconsin - Green Bay（Phoenix）
      - University of Wisconsin（Badgers）
      - 威斯康辛大学密尔瓦基分校（University of Wisconsin at Milwaukee）
      - 马凯特大学（Marquette University - Golden Eagles）

## 外部連結

  - [University of Wisconsin - Green Bay](http://www.uwgb.edu/)
  - [University of Wisconsin - Madison英文](http://www.wisc.edu/)
  - [马凯特大学 Marquette University英文](http://www.marquette.edu/)
  - [University of Wisconsin - Colleges英文](http://www.uwc.edu/)

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/威斯康辛州.md "wikilink")