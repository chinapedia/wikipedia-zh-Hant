[英國](../Page/英國.md "wikilink")**查爾斯**殿下（，），全名為**查爾斯·菲利普·亞瑟·喬治**（），現任[英國](../Page/英國.md "wikilink")[王儲](../Page/储君.md "wikilink")。他是現任[英國君主](../Page/英國君主.md "wikilink")[伊麗莎白二世和王夫愛丁堡公爵](../Page/伊丽莎白二世.md "wikilink")[菲利普親王的長子](../Page/爱丁堡公爵菲利普亲王.md "wikilink")，因此他排在[英国王位继承序列的首位](../Page/英国王位继承.md "wikilink")。

他是[英國歷史上在位時間最長的](../Page/英國歷史.md "wikilink")[王儲](../Page/储君.md "wikilink")，自1952年至今\[1\]，共67年\[2\]。若其将来成为英国国王，在[欧洲君主中](../Page/欧洲.md "wikilink")，等待继承王位的时间可能仅次于等候了70年的[列支敦士登大公](../Page/列支敦士登亲王列表.md "wikilink")。

## 早年生活

[Elizabeth,_Philip,_Charles_and_Anne.jpg](https://zh.wikipedia.org/wiki/File:Elizabeth,_Philip,_Charles_and_Anne.jpg "fig:Elizabeth,_Philip,_Charles_and_Anne.jpg")于1957年10月的合影\]\]
查爾斯於1948年11月14日，晚上9時14分在[伦敦](../Page/伦敦.md "wikilink")[白金汉宫出生](../Page/白金汉宫.md "wikilink")，是為**愛丁堡的查爾斯王子**（Prince
Charles of
Edinburgh）。在他母親1952年繼承英國王位後，他自动成為[康沃爾公爵](../Page/康沃爾公爵.md "wikilink")。這個头衔始自英國國王[愛德華三世繼位後](../Page/爱德华三世_\(英格兰\).md "wikilink")，就一直賜封與[英國君主的長子](../Page/英國君主列表.md "wikilink")。而查爾斯也立即獲封一系列傳統的蘇格蘭的[貴族头衔](../Page/蘇格蘭貴族.md "wikilink")，如[羅撒西公爵](../Page/羅撒西公爵.md "wikilink")、[卡利克伯爵](../Page/卡利克伯爵.md "wikilink")。

## 獲封威爾士親王

早在1958年，英女王伊麗莎白二世已發出諭旨，封查爾斯為[威爾士親王和](../Page/威爾士親王.md "wikilink")[切斯特伯爵](../Page/切斯特.md "wikilink")（Earl
of
Chester）。但正式的冊封大典直至1969年7月1日才在13世紀以來冊封大典的舉行地，北[威爾士的](../Page/威爾士.md "wikilink")[卡納封城堡](../Page/卡那封城堡.md "wikilink")（Caernarfon
Castle）舉行。為慶祝這次冊封，威爾士南部的一個海港[斯溫西獲女王御賜](../Page/斯旺西.md "wikilink")[都市地位](../Page/英國城市地位.md "wikilink")。

查爾斯其後入讀蘇格蘭的[高登斯頓學校](../Page/高登斯頓學校.md "wikilink")（Gordonstoun
School）和[剑桥大学三一学院](../Page/剑桥大学三一学院.md "wikilink")。期間還到[澳洲的一個戶外學習中心上了一個學期的學](../Page/澳洲.md "wikilink")。值得一提的是，查爾斯是[英國史上第一位能說](../Page/英國史.md "wikilink")[威爾士語的威爾士親王](../Page/威爾士語.md "wikilink")。為此，他特意入讀[威爾士大學](../Page/威爾士大學.md "wikilink")。雖然此舉提昇了查爾斯在[威爾士的民望](../Page/威爾士.md "wikilink")，但他的冊封大典還是在威爾士民族主義武裝組織的襲擊威脅下舉行。
[HRH_Prince_Charles_43_Allan_Warren.jpg](https://zh.wikipedia.org/wiki/File:HRH_Prince_Charles_43_Allan_Warren.jpg "fig:HRH_Prince_Charles_43_Allan_Warren.jpg")
1970年代末，當時的[英国首相](../Page/英国首相.md "wikilink")[詹姆斯·卡拉漢邀請查爾斯王儲參與內閣會議](../Page/詹姆斯·卡拉漢.md "wikilink")，讓他了解政府內閣如何運作。查爾斯是繼英國國王[喬治一世後第一位參與](../Page/乔治一世_\(大不列颠\).md "wikilink")[內閣會議的王室成員](../Page/英国内阁.md "wikilink")。

## 戀愛經歷

作為英國[王儲](../Page/王儲.md "wikilink")，查爾斯的戀愛經歷一直是英國大小傳媒追訪的話題。

查爾斯曾經和-{若干}-女子傳出過緋聞，包括[喬治安娜·羅素](../Page/喬治安娜·羅素.md "wikilink")（Georgiana
Russell，前英國駐[西班牙](../Page/西班牙.md "wikilink")[大使之女](../Page/大使.md "wikilink")）、[珍·衛斯利](../Page/珍·衛斯利.md "wikilink")（Lady
Jane
Wellesley，第八代[威靈頓公爵之女](../Page/威靈頓公爵.md "wikilink")）、[戴維娜·謝菲爾德](../Page/戴維娜·謝菲爾德.md "wikilink")（Davina
Sheffield）、名模[菲安娜·瓦特森](../Page/菲安娜·瓦特森.md "wikilink")（Fiona
Watson）、還有[戴安娜王妃的姊姊](../Page/戴安娜_\(威爾斯王妃\).md "wikilink")[莎拉·斯宾塞](../Page/莎拉·斯宾塞.md "wikilink")（Lady
Sarah Spencer）等等。

他現在的妻子，[卡米拉·珊德在查爾斯婚前也傳出過緋聞](../Page/卡米拉.md "wikilink")，但在1970年代末傳出兩人分手的消息。

1981年2月24日，白金漢宮宣佈查爾斯與第八代[斯宾塞伯爵之女](../Page/斯宾塞家族.md "wikilink")，[戴安娜·斯宾塞訂婚](../Page/威爾斯王妃戴安娜.md "wikilink")。當時戴安娜年僅19歲，雖然她擁有貴族身分，但她的職業只是一位幼稚園老師。

據說，卡米拉曾經幫助查爾斯選擇戴安娜為妻。

## 與戴安娜王妃的婚姻

[Prince_Charles,_Princess_Diana,_Nancy_Reagan,_and_Ronald_Reagan_(1985).jpg](https://zh.wikipedia.org/wiki/File:Prince_Charles,_Princess_Diana,_Nancy_Reagan,_and_Ronald_Reagan_\(1985\).jpg "fig:Prince_Charles,_Princess_Diana,_Nancy_Reagan,_and_Ronald_Reagan_(1985).jpg")伉儷，攝於1985年\]\]
1981年7月29日，查爾斯和[戴安娜在](../Page/威爾斯王妃戴安娜.md "wikilink") 3,500
名來自世界各地的嘉賓（包括卡蜜拉）的見證下，在[倫敦](../Page/倫敦.md "wikilink")[聖保羅大教堂完婚](../Page/聖保羅大教堂.md "wikilink")。歐洲各國的君主和領導人除了[西班牙國王](../Page/西班牙君主列表.md "wikilink")[胡安·卡洛斯一世](../Page/胡安·卡洛斯一世.md "wikilink")（因王儲夫婦蜜月的其中一站在和西班牙有主權爭議的[直布羅陀](../Page/直布罗陀.md "wikilink")）、[希臘總統](../Page/希腊总统列表.md "wikilink")（因英王室以「希臘人的國王」的頭銜，邀請流亡英國、屬遠親的[前希臘國王君士坦丁出席婚禮](../Page/康斯坦丁二世_\(希腊\).md "wikilink")）和[愛爾蘭總統](../Page/爱尔兰总统.md "wikilink")（因[北愛爾蘭](../Page/北爱尔兰.md "wikilink")[爭端](../Page/北爱尔兰问题.md "wikilink")）外，都出席了婚禮。與此同時，[全球約有](../Page/全球.md "wikilink")
7.5億人在電視機前見證這次豪華的王家婚禮。
[Kensington_Palace.jpg](https://zh.wikipedia.org/wiki/File:Kensington_Palace.jpg "fig:Kensington_Palace.jpg")\]\]
戴安娜在婚後獲得了“[威爾士王妃殿下](../Page/威爾士王妃.md "wikilink")” ( Princess of Wales
)的头衔，但是大眾還是偏好使用一個錯誤的稱呼，“戴安娜王妃” ( Princess Diana
)。婚後王儲伉儷的居所分別是倫敦的[肯辛頓宮和](../Page/肯辛顿宫.md "wikilink")[告士打郡的](../Page/格洛斯特郡.md "wikilink")[海格洛夫莊園](../Page/海格洛夫莊園.md "wikilink")（Highgrove）。從此，戴安娜成為王室的明星，常遭到「[狗仔隊](../Page/狗仔隊.md "wikilink")」的貼身追訪，而她的打扮、衣著甚至舉止成為了英國女性爭相模仿的對象。

但王儲夫婦的婚姻很快觸礁。據傳，在巨大的压力下，戴安娜王妃婚後的脾氣變得暴躁且很不穩定，她不但辭退了長期服務王儲的伺從，還和她的親友經常吵架，包括她的[父](../Page/約翰·史賓沙，第八代史賓沙伯爵.md "wikilink")[母](../Page/法蘭西絲·布奇·洛茲.md "wikilink")、弟弟、著名音樂人[艾尔顿·约翰](../Page/艾爾頓·約翰.md "wikilink")[爵士](../Page/爵士.md "wikilink")，甚至自己的侍從。而查爾斯和他的舊情人卡蜜拉藕断丝连。這段婚外情令王儲的婚姻在五年內迅速敗壞，卡蜜拉亦受不少英國人指責為王儲這段童話式婚姻的破壞者。

[Marlborough_House.jpg](https://zh.wikipedia.org/wiki/File:Marlborough_House.jpg "fig:Marlborough_House.jpg")南立面\]\]
查爾斯王儲伉儷因為各自都經歷了不愉快的童年，因此都十分投入慈善事業。而有一點佐證了查爾斯伉儷的婚姻觸礁，當戴安娜王妃全力幫助[愛滋病病人](../Page/愛滋病.md "wikilink")，查爾斯卻在擴展他名下的基金受助機構的數目。

而查爾斯[外遇了卡蜜拉之后](../Page/外遇.md "wikilink")，不堪忍受的戴安娜也與她的[副官開始了婚外情](../Page/副官.md "wikilink")。1990年代末，王儲伉儷已經開始非正式分居，王儲主要居住地在海格洛夫莊園，而戴安娜則住在倫敦的肯辛頓宮。兩人在1992年正式分居，而兩人也開始被媒體稱為「威爾士王室之戰（War
of the
Waleses）」的口水戰。在此期間，王儲與[卡蜜拉婚外情的詳情更被戴安娜王妃完全曝光](../Page/卡蜜拉_\(康沃尔公爵夫人\).md "wikilink")。

1996年8月28日，查爾斯和戴安娜正式離婚，結束了兩人15年的夫妻關係。他們在婚姻期間產下兩位王子，分別是[威廉王子和](../Page/劍橋公爵威廉王子.md "wikilink")[哈利王子](../Page/哈里王子.md "wikilink")，兩人共同擁有撫養權。

1997年8月31日，戴安娜王妃因車禍死於[法國](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。查爾斯王儲的處理前妻後事的手法，首先是不顧顧問反對親赴巴黎接回王妃遺體，其後堅持為戴安娜安排王家葬禮，更不惜特別設計一套新的葬禮程序。最後更帶著兩位王子在王妃靈柩後步行，陪她走完最後一程。此後，查爾斯成為兩位王子的單親爸爸，令他得到英國民眾的不少同情。

## 與卡蜜拉的婚姻

[Duchess_of_Cornwall_in_2014.jpg](https://zh.wikipedia.org/wiki/File:Duchess_of_Cornwall_in_2014.jpg "fig:Duchess_of_Cornwall_in_2014.jpg")
在戴安娜去世後，卡蜜拉與查爾斯的關係逐漸轉到檯面上。卡蜜拉多次陪同王儲出席公開活動，甚至獲准參與王室內部的私人晚宴。但他們兩人能否結婚一直是一個爭議性話題。

根據宗教和傳統的角度來說，查爾斯將會繼任為[英國聖公會的最高首領](../Page/英國國教會.md "wikilink")，因此他的王后一定不能是[天主教徒](../Page/天主教.md "wikilink")，而且他的王后在嫁給他以前，必須不能離婚。而卡蜜拉離過婚，肯定不符資格。另一方面，聖公會極力反對教徒在前配偶在生時再婚，卡蜜拉的夫婿仍然在生，令這段婚姻更富爭議性。因此，聖公會部分工作人員一直反對這段可能的婚姻，而部分虔誠的教徒也對這段婚姻的合法性持懷疑態度。

而卡蜜拉離婚婦人的身分也惹來第二個問題——她的頭銜。根據《1772年婚姻法案》，王儲和卡蜜拉的婚姻可以按照[贵庶通婚法的特例來處理](../Page/貴賤通婚.md "wikilink")。根據這個法案，卡蜜拉在婚後將不會獲得威爾士王妃殿下和將來王后陛下的稱號。

2005年2月10日，英王室宣佈查爾斯王儲和[卡蜜拉·帕克-鲍尔斯將於同年](../Page/康沃爾公爵夫人卡蜜拉.md "wikilink")4月8日完婚，行禮的地點將會是[溫莎城堡的](../Page/溫莎城堡.md "wikilink")[聖喬治教堂](../Page/圣乔治礼拜堂_\(温莎城堡\).md "wikilink")，婚禮僅以私人形式舉行，並由[坎特伯雷大主教](../Page/坎特伯雷大主教.md "wikilink")[羅萬·威廉斯](../Page/羅恩·威廉斯.md "wikilink")[博士主持](../Page/博士.md "wikilink")。後來由於[聖喬治教堂並未持有合法主持婚禮註冊的執照](../Page/聖喬治教堂.md "wikilink")，註冊儀式改到在[溫莎](../Page/溫莎_\(伯克郡\).md "wikilink")[市政廳舉行](../Page/市政廳.md "wikilink")，並在[聖喬治教堂舉行祝福禮](../Page/圣乔治礼拜堂_\(温莎城堡\).md "wikilink")。最後因婚期與[教宗](../Page/教宗.md "wikilink")[若望保祿二世的葬禮碰撞](../Page/若望·保祿二世.md "wikilink")，二人最終把婚期順延一天，於4月9日舉行大婚儀式。

根據王室的公佈訂婚的文件，卡蜜拉婚後不會使用“威爾士王妃殿下”的稱號，而改用“康沃爾公爵夫人殿下”（英格蘭）和“羅撒西公爵夫人殿下”（蘇格蘭）這兩個頭銜。而在查爾斯登基為國王以後，卡蜜拉將只可能使用“國王貴妃殿下／國王伴妃殿下”的稱號，亦不是“王后陛下”。卡蜜拉將會成為英國歷史上第一位不使用王后稱號的國王的合法配偶。不過，英國政府後來卻宣布，除非修改英國和各英聯邦王國的相關法律，卡蜜拉將在查爾斯登基後自動成為實際上的「王后陛下」。此舉引來廣泛的不滿。

## 家庭

### 祖先

<center>

</center>

[Clarence_house.jpg](https://zh.wikipedia.org/wiki/File:Clarence_house.jpg "fig:Clarence_house.jpg")\]\]

### 子女

在第一段婚姻中，查尔斯与戴安娜共育有两子：

  - [劍橋公爵威廉王子](../Page/劍橋公爵威廉王子.md "wikilink")，全名威廉·亚瑟·菲利浦·路易斯（1982年6月21日生於[倫敦](../Page/倫敦.md "wikilink")[聖瑪麗醫院](../Page/聖瑪麗醫院.md "wikilink")）
  - [薩塞克斯公爵哈利王子](../Page/薩塞克斯公爵哈里王子.md "wikilink")，全名亨利·查尔斯·艾伯特·大卫，暱稱為“哈利”（1984年9月15日生於[倫敦](../Page/倫敦.md "wikilink")[聖瑪麗醫院](../Page/聖瑪麗醫院.md "wikilink")）

## 私生活

### 興趣

王儲是一位熱愛[馬術和打獵的王子](../Page/馬術.md "wikilink")。他曾經在英國皇家海軍服役，在1976年2月至12月指揮過掃雷艦*保羅寧頓號*。

另一方面，他成立的**威爾士親王基金**，為一些無法得到主流財經機構援助的組織，公司或個人（如藝術家）提供貸款和援助，同時也為英國各地的失業人士提供再培訓和求職的機會。

王儲的性格十分複雜。他承認他有間歇性[憂鬱症](../Page/重性抑郁障碍.md "wikilink")，不過他在公眾場合的表現常常十分得體。

另外，他十分熱心於環境保護、建築、都市重建和提昇生活質素等問題。他決定重建[多賽特郡的](../Page/多塞特郡.md "wikilink")[彭布里鎮](../Page/庞德伯里.md "wikilink")（Poundbury,
Dorset），以實踐他的建築和城鎮規劃的新理念。縱使遭到諷刺，但王儲仍熱心於他的公爵領地內的有機作物的種植和推廣等事宜。

王儲也很熱心國際事務，縱使他並不是主責處理。例如他出訪[愛爾蘭](../Page/愛爾蘭.md "wikilink")，他在當地的致詞是經過精心準備，事前進行過資料搜集，而不是簡單乏味的致詞。此舉受到愛爾蘭上下的歡迎。

王儲也是繪畫愛好者，他的畫作多被拿出來義賣，他也曾將作品輯錄成書出版。

王儲對於[哲學十分有興趣](../Page/哲学.md "wikilink")，特別是對來自[亞洲和](../Page/亚洲.md "wikilink")[中東的哲學有興趣](../Page/中东.md "wikilink")。他和著名作家[勞仁斯·凡·德·波斯特的友誼始於](../Page/勞仁斯·凡·德·波斯特.md "wikilink")1977年，直至波斯特去世。波斯特因此被认为是「英國的太傅」，更有幸成為威廉王子的義父。

雖然王儲的受歡迎度一般，但他被公認為英國歷史上最熱心於慈善事業的儲君。與卡米拉的關係固然令他的名聲受損，但他的一名助理涉嫌[雞姦的案件](../Page/雞姦.md "wikilink")，成為他人生中的一大污點。

### 居所

查爾斯王儲現時的官方居所為[克拉倫斯府](../Page/克拉伦斯府.md "wikilink")（Clarence
House），此原為查爾斯的外祖母[伊麗莎白王太后的居所](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")。

王儲過往的官方居所包括[聖詹姆士宮](../Page/聖詹姆士宮.md "wikilink")（St James's
Palace）和[馬爾巴羅別墅](../Page/馬爾博羅大樓.md "wikilink")（Marlborough
House）。

## 未来前途

如果查尔斯王储未来继承王位，将成为英国和其他15个英联邦王国国王，若按照英王室使用名字作为国王尊号的传统，他将被称作「查理三世」。但也有传言说王储并不喜欢这一尊号，因为容易引起民众对[斯图亚特王朝的](../Page/斯图亚特王朝.md "wikilink")[查理一世](../Page/查理一世_\(英格蘭\).md "wikilink")（被处决）和[查理二世](../Page/查理二世_\(英格兰\).md "wikilink")（曾经流亡）的联想。传言并指王储曾考虑改用「乔治七世」以纪念自己的外祖父[乔治六世](../Page/乔治六世.md "wikilink")\[3\]，但王储本人曾经在公开场合否认过这一猜测\[4\]。另外民間流傳，[伊莉莎白二世迟迟不退位並傳位與查爾斯王子](../Page/伊丽莎白二世.md "wikilink")，是因為他與處處維持[英國王家名譽與名聲的](../Page/英國.md "wikilink")[黛安娜王妃離異](../Page/威爾斯王妃戴安娜.md "wikilink")，與[卡蜜拉女爵成婚](../Page/康沃爾公爵夫人卡蜜拉.md "wikilink")（這是可能的原因之一）。另外因長孫[威廉王子頗有大將之風](../Page/劍橋公爵威廉王子.md "wikilink")，具有領袖氣質且親民，其[威廉王子大婚時還特地安排](../Page/劍橋公爵威廉王子.md "wikilink")[威廉王子與其妻子要朝向已亡故的生母](../Page/劍橋公爵威廉王子.md "wikilink")[黛安娜王妃的墓地致意](../Page/黛安娜王妃.md "wikilink")，並有意培養[威廉王子為她的接班人](../Page/劍橋公爵威廉王子.md "wikilink")，故還未[遜位](../Page/逊位.md "wikilink")。當然這些只是民間與新聞媒體的猜測，真正原因為何不為人知。

2018年4月20日，英联邦政府首脑会议举行，并当天发表声明说，批准查尔斯王子担任下一任[英联邦元首](../Page/英联邦元首.md "wikilink")，英联邦各成员对现任元首英国女王伊丽莎白二世的领导表示认可。声明未明确说明查尔斯王子将从何时起担任英联邦元首\[5\]。

## 頭銜和徽章

[Coat_of_Arms_of_Charles,_Prince_of_Wales.svg](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Charles,_Prince_of_Wales.svg "fig:Coat_of_Arms_of_Charles,_Prince_of_Wales.svg")
查爾斯王儲作為王位繼承人和英國（名義上）三軍統帥的長子，因此獲得不少头衔。

### 貴族頭銜

王儲的正式全名包括他所有的頭銜。由於十分冗長，因此甚少使用。

英文：<span style="font-family:Georgia, serif;"></span>

中文譯文：“**查爾斯·菲利浦·亞瑟·喬治王子殿下**，[威爾士親王和](../Page/威爾士親王.md "wikilink")[切斯特伯爵](../Page/切斯特伯爵.md "wikilink")，[康沃爾公爵](../Page/康沃爾公爵.md "wikilink")，[羅撒西公爵](../Page/羅撒西公爵.md "wikilink")，[卡里克伯爵](../Page/卡里克伯爵.md "wikilink")，[伦弗鲁男爵](../Page/伦弗鲁男爵.md "wikilink")，[群岛领主](../Page/群岛领主.md "wikilink")，[蘇格蘭王子和苏格兰王室總管](../Page/苏格兰王室总务官.md "wikilink")，最高贵[嘉德勋章](../Page/嘉德勋章.md "wikilink")[騎士](../Page/騎士.md "wikilink")，最古老和最高贵[薊花勋章骑士](../Page/薊花勋章.md "wikilink")，最尊敬的[巴斯大十字勳章的高贵主人和首席騎士](../Page/巴斯勳章.md "wikilink")，[功绩勋章成員](../Page/功绩勋章.md "wikilink")，[澳大利亞勋章骑士](../Page/澳大利亞勳章.md "wikilink")，[女王服務勋章成員](../Page/女王服務勋章.md "wikilink")，女王陛下最尊敬的[枢密院大臣](../Page/英国枢密院.md "wikilink")，女王陛下的侍從武官。”

### 軍銜

[St_james_palace.jpg](https://zh.wikipedia.org/wiki/File:St_james_palace.jpg "fig:St_james_palace.jpg")正門\]\]
名義上，查爾斯王儲是一位軍人，而且擁有眾多[軍銜](../Page/軍銜.md "wikilink")，但是都是由他母亲賜封，並不是以戰功換來的。

王儲在[英國陸軍及英國皇家](../Page/英國陸軍.md "wikilink")[海](../Page/英國皇家海軍.md "wikilink")、[空軍都擁有](../Page/英國皇家空軍.md "wikilink")[元帅的軍銜](../Page/元帅.md "wikilink")\[6\]，而且還是若干[軍團的名義領袖](../Page/軍團.md "wikilink")：

  - 威爾士皇家步兵團名譽[團長](../Page/團長.md "wikilink")（自1969年7月1日）
  - 威爾士衛隊上校（自1975年3月1日）
  - [加拿大航空預備隊名譽團長](../Page/加拿大.md "wikilink")（自1977年6月11日）
  - 英格蘭[柴郡](../Page/柴郡.md "wikilink")(Cheshire)22軍團名譽團長（自1977年6月11日）
  - 加拿大皇家斯卡特科拿勳爵騎兵團名譽團長（自1977年6月11日）
  - 英國皇家空軍傘兵團名譽團長（自1977年6月11日）
  - [澳大利亞皇家盔甲步兵團名譽團長](../Page/澳大利亞.md "wikilink")（自1977年6月11日）
  - 英國皇家廓喀步槍兵團名譽團長（自1977年6月11日）
  - [紐西蘭皇家空軍名譽最高長官](../Page/紐西蘭.md "wikilink")（自1977年6月11日）
  - 加拿大皇家步兵團名譽團長（自1977年6月11日）
  - 加拿大溫尼伯步槍兵團名譽團長（自1977年6月11日）
  - 英國皇家太平洋群島步兵團名譽團長（自1984年8月8日）
  - 加拿大皇家重騎兵團名譽團長（自1985年9月17日）
  - 英國皇家陸軍空戰兵團名譽團長（自1992年3月1日）
  - 英國皇家重騎兵衛隊名譽團長（自1992年7月1日）
  - 英國皇家空軍名譽準將（自1993年4月1日）
  - 蘇格蘭高地聯隊名譽副隊長（自1994年9月1日）
  - 蘇格蘭皇家高地人步兵團（黑守夜人兵團）名譽團長（自2003年7月1日）
  - 英皇直轄第一重騎兵衛隊名譽團長（自2003年7月1日）
  - 英皇步兵團名譽團長（自2003年7月1日）

## 個人旗幟

<File:Royal> Standard of the Prince of Wales.svg|在英格蘭或北愛爾蘭使用

<File:Personal> Banner of the Prince of
Wales.svg|在[威爾斯使用](../Page/威爾斯.md "wikilink")

<File:Personal> Banner of the Duke of Rothesay.svg|在蘇格蘭使用

<File:Flag> of the Duke of
Cornwall.svg|[康沃爾公爵旗](../Page/康沃爾公爵.md "wikilink")

<File:Royal> Standard of the Prince of Wales (in
Canada).svg|在[加拿大的私人旗](../Page/加拿大.md "wikilink")

## 备注

## 注释

## 參考書目

  -
  -
## 外部連結

  - [威爾斯親王的官方網站（英文）](http://www.princeofwales.gov.uk/)

|-                           |-

[Category:威爾斯親王](../Category/威爾斯親王.md "wikilink")
[Category:康沃爾公爵](../Category/康沃爾公爵.md "wikilink")
[Category:溫莎王朝貴族](../Category/溫莎王朝貴族.md "wikilink")
[Category:格呂克斯堡王朝](../Category/格呂克斯堡王朝.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:英国陆军元帅](../Category/英国陆军元帅.md "wikilink")
[Category:英國皇家海軍元帥](../Category/英國皇家海軍元帥.md "wikilink")
[Category:英國皇家空軍元帥](../Category/英國皇家空軍元帥.md "wikilink")
[Category:劍橋大學三一學院校友](../Category/劍橋大學三一學院校友.md "wikilink")
[Category:威爾士大學校友](../Category/威爾士大學校友.md "wikilink")
[Category:園藝與造園學家](../Category/園藝與造園學家.md "wikilink")
[Category:皇家文学学会院士](../Category/皇家文学学会院士.md "wikilink")
[Category:英国之最](../Category/英国之最.md "wikilink")
[Category:倫敦人](../Category/倫敦人.md "wikilink")

1.  [Prince Charles becomes longest-serving heir
    apparent](http://www.bbc.co.uk/news/uk-13133587)
2.  [Will Prince Charles be king? Or will he step aside for William
    after the
    Queen?](http://www.express.co.uk/news/royal/746076/Will-Prince-Charles-be-king-step-aside-William-Queen-Elizabeth-death-abdicate)
3.  英皇**-{zh-cn: 佐治六世; zh-tw: 佐治六世; zh-hk:佐治六世}-**銅像由英國著名雕塑家Gilbert
    Ledward所鑄，於1958年豎設於香港動植物公園，以紀念香港開埠一百周年（1841至1941年)
    <http://www.lcsd.gov.hk/tc/parks/hkzbg/facilitie/outdoor/george.html>
4.  [Charles denies planning to reign as King
    George](http://www.guardian.co.uk/uk/2005/dec/27/monarchy.michaelwhite)
5.
6.  [英国查尔斯王子晋升三军元帅](http://news.xinhuanet.com/world/2012-06/17/c_123293796.htm)．新华网．2012年6月17日