**娜克希迪尔**（**Nakshidil**或**Nakşidil**）（1768年-1817年8月28日），[奥斯曼帝国苏丹](../Page/奥斯曼帝国.md "wikilink")[阿卜杜勒·哈米德一世的第四位](../Page/阿卜杜勒·哈米德一世.md "wikilink")，[马哈茂德二世养母](../Page/马哈茂德二世.md "wikilink")，1808年至1817年间為皇太后，称**娜克希迪尔·瓦立德·苏丹娜**，实际掌握奥斯曼帝国大权。

## 生平

**娜克希迪尔**来自[法国](../Page/法国.md "wikilink")，自小在法国受教育，1784年左右，当她乘船从[南特出发回家乡的时候](../Page/南特.md "wikilink")，被[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[海盗所虏](../Page/海盗.md "wikilink")，阿尔及利亚总督将其买下并进贡给[苏丹](../Page/苏丹_\(称谓\).md "wikilink")[阿卜杜勒·哈米德一世](../Page/阿卜杜勒·哈米德一世.md "wikilink")。在后宫中，她很快得宠，成为苏丹排名第四的正妻，皇子[马哈茂德的养母](../Page/马哈茂德.md "wikilink")，被赐名**娜克希迪尔**，义为“绣着花边的心灵”。

1789年，阿卜杜勒·哈米德一世去世，其弟[谢里姆三世继位](../Page/谢里姆三世.md "wikilink")。谢里姆三世非常尊重娜克希迪尔，将其留在宫中（按規定，蘇丹逝世後其[遺孀無子者須全部移居舊皇宮](../Page/先朝君主遺孀.md "wikilink")，有子者則隨兒子到其管轄地居住），但是并未将其纳入自己的后宫。

[Tomb_of_Nakshidil.jpg](https://zh.wikipedia.org/wiki/File:Tomb_of_Nakshidil.jpg "fig:Tomb_of_Nakshidil.jpg")

1807年，反对谢里姆改革的守旧派[穆斯林](../Page/穆斯林.md "wikilink")[耶尼切里发动政变](../Page/耶尼切里.md "wikilink")，废黜谢里姆，推举其弟[穆斯塔法四世即位](../Page/穆斯塔法四世.md "wikilink")。禁卫军更攻入皇宫试图杀死心目中的恶魔娜克希迪尔，她被迫与其养子马哈茂德躲在一个熄灭的炉子中方才得以幸免于难。

1808年，大将[阿拉姆達爾·穆斯塔法帕夏进军首都](../Page/阿拉姆達爾·穆斯塔法帕夏.md "wikilink")[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")，要求谢里姆复位，穆斯塔法四世拒绝，更杀死了谢里姆，自己随即也死于乱军之手。这样，马哈茂德作为第一顺序的继承人，登上了苏丹宝座，娜克希迪尔也随之掌握了整个帝国的军政大权。

1817年，娜克希迪尔去世，她的墓地至今还保存在伊斯坦布尔城[法提赫区的](../Page/法提赫区.md "wikilink")[法提赫庭院中](../Page/法提赫庭院.md "wikilink")。

## 传说

据传说，她原名**Aimee De Buc De
Rivery**，是[法国殖民地](../Page/法国.md "wikilink")[马提尼克岛一贵族之女](../Page/马提尼克岛.md "wikilink")，与[拿破仑的皇后](../Page/拿破仑.md "wikilink")[約瑟菲娜·德博阿爾內是表姐妹](../Page/約瑟菲娜·德博阿爾內.md "wikilink")。幼年时，曾有一名[女巫为二人看相](../Page/女巫.md "wikilink")，并预言两人均会成为皇后，分别统治西方和东方。

[Aimee.jpg](https://zh.wikipedia.org/wiki/File:Aimee.jpg "fig:Aimee.jpg")

据说正是由于约瑟芬成为法国皇后的原因，土耳其在[拿破仑战争的前期一直站在法国一边](../Page/拿破仑战争.md "wikilink")。但是拿破仑却一直不知道娜克希迪尔的存在，抛弃了其表姐，而迎娶[奥地利公主](../Page/奥地利.md "wikilink")。这立刻招致了报复，在1812年，娜克希迪尔突然改变了与法国结盟的立场，与宿敌[俄国议和](../Page/俄国.md "wikilink")，导致俄国能够抽调俄土边境的大军北上，在[别列津纳河战役中重创拿破仑](../Page/别列津纳河战役.md "wikilink")。

据传说，娜克希迪尔从没有放弃过[天主教信仰](../Page/天主教.md "wikilink")，即便是在临终前，仍然提出要[神父为其忏悔](../Page/神父.md "wikilink")，马哈茂德答应了她的要求，使得有史以来第一次允许天主教神父踏入了后宫。

[Intimate_power.jpg](https://zh.wikipedia.org/wiki/File:Intimate_power.jpg "fig:Intimate_power.jpg")

后世有多部小说描写她的传奇经历，其中尤以[希腊迈克尔王子的](../Page/希腊.md "wikilink")“Sultana”和Janet
Wallach的“Seraglio”最为出名。1989年，[好莱坞将其经历搬上银幕](../Page/好莱坞.md "wikilink")，名为“[温柔的权力](../Page/温柔的权力.md "wikilink")”，由[奥斯卡](../Page/奥斯卡奖.md "wikilink")[影帝](../Page/奥斯卡最佳男主角奖.md "wikilink")[F·默里·亚伯拉罕主演](../Page/F·默里·亚伯拉罕.md "wikilink")。

## 历史评价

不少人相信谢里姆三世正是在娜克希迪尔的影响下，才开始大规模向西方，尤其是法国学习，组建新军，进而推动整个国家开始改革。她掌权之后，秉承了谢里姆的改革措施，打开了土耳其封闭的国门。

在她死后，马哈茂德继续她的改革措施，废除了禁卫军和穆斯林[原教旨主义的教团](../Page/原教旨主义.md "wikilink")，并对[麦加的](../Page/麦加.md "wikilink")[原教旨主义者](../Page/原教旨主义.md "wikilink")[瓦哈比教派发动战争](../Page/瓦哈比教派.md "wikilink")。

## 参见

  - [阿卜杜勒·哈米德一世](../Page/阿卜杜勒·哈米德一世.md "wikilink")
  - [拿破仑战争](../Page/拿破仑战争.md "wikilink")

## 外部链接

  - [Two Empresses from Martinique- Part
    I](https://web.archive.org/web/20070927210614/http://www.dailytidings.com/2004/0920/092004c1.shtml)
  - [Two Empresses from Martinique - Part
    II](http://67.15.1.122/2004/0927/092704c1.shtml)
  - [The Veiled Empress'
    tomb](http://www.roxanephoto.com/turquie/istanbul/fatih/corne-d-or/fatih-conquerant/photos/fatih_conquerantaimee01.htm)
  - [An excerpt from *Seraglio*, a novel about the
    Empress](http://www.loc.gov/catdir/samples/random043/2002028698.html)
  - [Historic Sites of
    Martinique](http://www.tripadvisor.com/Attractions-g147327-Activities-c5-Martinique.html)

## 相关书籍

[N](../Category/1768年出生.md "wikilink")
[N](../Category/1817年逝世.md "wikilink")
[N](../Category/法国人.md "wikilink")
[N](../Category/土耳其政治人物.md "wikilink")
[Category:蘇丹皇太后](../Category/蘇丹皇太后.md "wikilink")