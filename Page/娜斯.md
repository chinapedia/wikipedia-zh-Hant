**娜斯**（），原名**娜日斯**（[达斡尔语松树的意思](../Page/达斡尔语.md "wikilink")）。专栏作家，自由撰稿人。她的文章主要关于对[美国文化特别是](../Page/美国文化.md "wikilink")[纽约文化的观察](../Page/纽约文化.md "wikilink")，受其父母影响，对法国文化也有涉猎。主要表现为对电影电视的观感，书评，食评，游记。由于她独特的经历和多元化的背景，她对于东西方文化差异、城市生活的观察与点评尤为独到。文章主要发表在《[三联生活周刊](../Page/三联生活周刊.md "wikilink")》，《[书城](../Page/书城.md "wikilink")》，《[读书](../Page/读书_\(杂志\).md "wikilink")》，《[万象](../Page/万象_\(杂志\).md "wikilink")》等刊物上，结集出书出版的有《纽约明信片》，《东看西看》和《想像舞蹈的马格丽特》。

## 经历

娜斯出生于北京。父亲是[达斡尔族](../Page/达斡尔族.md "wikilink")；母亲[张暖忻是满族和汉族混血](../Page/张暖忻.md "wikilink")，已故电影导演。

她就读于北京大学计算机系和中文系，1987年获得文学学士学位。1990年赴美，获[爱荷华大学东亚研究硕士学位](../Page/爱荷华大学.md "wikilink")，后两年进入艺术设计系研究生院就读。

1992年起作品开始出现在美国华文刊物《今天》、《世界日报》、《国风》上。
1994年起居住纽约，先后在职广告公司，电讯公司，互联网公司任职。1996年起任《三联生活周刊》驻纽约记者，并在其开设《纽约明信片》专栏，结集于2001年出版。2001年后开设新专栏《东看西看》，于2003年结集出版。她的作品还发表在《[书城](../Page/书城.md "wikilink")》，《[读书](../Page/读书_\(杂志\).md "wikilink")》，《[万象](../Page/万象_\(杂志\).md "wikilink")》等刊物上。在其他报刊杂志上发表的文章，结集为《想像舞蹈的马格丽特》出版。

娜斯现居北京。

## 主要作品

  - 2001年 《纽约明信片》
  - 2003年 《东看西看》
  - 2005年 《想像舞蹈的马格丽特》

## 外部链接

  - [北京人兼纽约客：三联专栏作家娜斯访谈](http://book.sina.com.cn/mediacoop/lmone/2005-08-03/1640187674.shtml)
  - [娜斯个人BLOG--食万粒豆](http://rna.blog.hexun.com/)
  - [娜斯在三联生活周刊上的专栏文章选编](http://www.lifeweek.com.cn/iRelease/jsp/article/AuthorPage.jsp?author=%C4%C8%CB%B9)
  - [娜斯的出版作品](http://www.douban.com/book/tag/%E5%A8%9C%E6%96%AF)

[category:中華人民共和國散文家](../Page/category:中華人民共和國散文家.md "wikilink")

[Category:中華人民共和國女性作家](../Category/中華人民共和國女性作家.md "wikilink")
[Category:中華人民共和國記者](../Category/中華人民共和國記者.md "wikilink")
[Category:愛荷華大學校友](../Category/愛荷華大學校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:达斡尔族人](../Category/达斡尔族人.md "wikilink")