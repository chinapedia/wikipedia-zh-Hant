****（Lucina）是第146颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1875年6月8日由[阿方斯·路易·尼古拉斯·包瑞利发现](../Page/阿方斯·路易·尼古拉斯·包瑞利.md "wikilink")。的[直径为](../Page/直径.md "wikilink")132.2千米，[质量为](../Page/质量.md "wikilink")2.4×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1637.739天。


[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1875年发现的小行星](../Category/1875年发现的小行星.md "wikilink")