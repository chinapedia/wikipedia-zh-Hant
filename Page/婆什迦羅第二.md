**婆什迦罗**（，），也称为**婆什迦罗第二**（ II）和
**婆什迦罗老師**（），是一个[印度](../Page/印度.md "wikilink")[数学家](../Page/数学家.md "wikilink")。他生于[卡纳塔克邦的](../Page/卡纳塔克邦.md "wikilink")[比贾布尔区的](../Page/比贾布尔.md "wikilink")[Bijjada
Bida附近](../Page/Bijjada_Bida.md "wikilink")，並成为[鄔闍衍那的](../Page/鄔闍衍那.md "wikilink")[天文台的台长](../Page/天文台.md "wikilink")，延续了[伐罗诃密希罗和](../Page/伐罗诃密希罗.md "wikilink")[婆羅摩笈多的数学传统](../Page/婆羅摩笈多.md "wikilink")。

在很多方面，婆什迦罗代表了十二世纪数学知识的巅峰。他对数字系统和方程解法的理解在几个世纪中在世界其他地方无人能及。他主要的作品有（处理[算数](../Page/算数.md "wikilink")），
（[代数](../Page/代数.md "wikilink")）和[Siddhantasiromani](../Page/Siddhantasiromani.md "wikilink")（由两部分组成:
Goladhyaya（[球](../Page/球.md "wikilink")）和 Grahaganita
（[行星的数学](../Page/行星.md "wikilink")））。

## 贡献

他比牛顿和莱布尼茨早五个世纪就构想了微积分，而他们被视为微积分的创立者。现在称为"微分系数"的一个实例和现在称为[罗尔定理的基本思想可以在他的著作中找到](../Page/罗尔定理.md "wikilink")（
(1340年)，而Kerala学派进一步在印度发展了微积分）。

他對[配爾方程的研究比](../Page/配爾方程.md "wikilink")[約翰·配爾要早好几个世纪](../Page/約翰·配爾.md "wikilink")。

他给出了[勾股定理的一个证明](../Page/勾股定理.md "wikilink")，该证明是通过用两种不同方法计算相同[面积然后消去一些项以给出](../Page/面积.md "wikilink")
\(a^2+b^2=c^2\)。

他也因证明了任何数除以0是无穷大而无穷大除以任何数依然是无穷大而著称。

## 传奇

他关于算数的书Lilavati背後有很多有趣的传奇，那些传奇认定该书是写给他的女儿Lilavati的。其中一个故事說，婆什迦罗占星，预知了她丈夫会在婚后很快死去。为了避免这个悲剧发生，他要在一个他用一种特殊仪器测量的精确的时间地点举办婚礼。他把仪器放在一个房间，放上警示提醒Lilavati不要靠近。Lilavati好奇，往仪器里窥视，剛巧她鼻环上的一颗珍珠掉了进去，干扰了仪器。婚礼在錯誤的时间举行了，她很快成了寡妇。据说婆什迦罗教授了她很多数学来给她散心，並为她写成了那本书。

## 参看

  -
  -
  - [阿耶波多](../Page/阿耶波多.md "wikilink")

## 外部链接

  -
  - [婆什迦罗](https://web.archive.org/web/20051201205501/http://www.4to40.com/legends/index.asp?article=legends_bhaskara)

  - [Kerala的微积分](https://web.archive.org/web/20060806040307/http://www.canisius.edu/topos/rajeev.asp)

[Bhaskara](../Category/印度数学家.md "wikilink")
[Bhaskara](../Category/12世纪数学家.md "wikilink")
[Bhaskara](../Category/印度天文学家.md "wikilink")