**婆娑尼師今**（），姓**-{朴}-**名**婆娑**。[新羅第](../Page/新羅.md "wikilink")5代君主，[儒理尼師今之子](../Page/儒理尼師今.md "wikilink")。在位期間，婆娑尼師今將新羅版圖擴大。他节俭省用，关爱国民，受到国民的拥戴。在位32年，传位于其子[祇摩尼师今](../Page/祇摩尼师今.md "wikilink")。

## 治世

  - 84年，立[明宣为伊飡](../Page/明宣.md "wikilink")，[允良为波珍飡](../Page/允良.md "wikilink")。85年，立[吉元为阿飡](../Page/吉元.md "wikilink")。
  - 87年，筑[加召城](../Page/加召城.md "wikilink")、[馬头城](../Page/馬头城.md "wikilink")。
  - 93年，任允良为伊飡，[启其为波珍飡](../Page/启其.md "wikilink")。94年，[伽倻贼围攻马头城](../Page/伽倻.md "wikilink")，婆娑遣阿飡吉元领1000骑兵击退。
  - 96年，伽倻人攻击新罗南方边境，婆娑遣加城主[长世去抵抗](../Page/长世.md "wikilink")，结果被杀。婆娑大怒，领5000勇士击败伽倻人。
  - 101年，修筑[月城](../Page/半月城.md "wikilink")。102年，[音汁伐国与](../Page/音汁伐国.md "wikilink")[悉直谷国争土地](../Page/悉直谷国.md "wikilink")。婆娑听说伽倻的[首露王足智多谋](../Page/首露王.md "wikilink")，便命六部会飨首露王。首露王因[汉祇部主](../Page/汉祇部.md "wikilink")[保其官位低下](../Page/保其.md "wikilink")，以为不敬，命奴杀之。奴逃依音汁伐主[陁邹干家](../Page/陁邹.md "wikilink")。婆娑命人索要奴，施邹不给。婆娑遂伐[音汁伐国](../Page/音汁伐国.md "wikilink")，后[悉直](../Page/悉直.md "wikilink")、[押督二国王来降](../Page/押督.md "wikilink")。
  - 104年，悉直起兵叛乱，婆娑发兵讨平。106年，命马头城城主伐伽倻。108年，遣兵伐[比只国](../Page/比只国.md "wikilink")、[多伐国](../Page/多伐国.md "wikilink")、[草八国](../Page/草八国.md "wikilink")，并兼并。
  - 112年，婆娑尼师今去世，葬于[蛇陵](../Page/蛇陵.md "wikilink")。传位于其子[祇摩尼师今](../Page/祇摩尼师今.md "wikilink")。

## 家庭

[Silla-monarch(1-12).png](https://zh.wikipedia.org/wiki/File:Silla-monarch\(1-12\).png "fig:Silla-monarch(1-12).png")中的新罗国世系\]\]

  - 父：[儒理尼师今](../Page/儒理尼师今.md "wikilink")（一说[奈老](../Page/奈老.md "wikilink")）
  - 妻：[史省夫人](../Page/史省夫人.md "wikilink")（[许娄葛文王之女](../Page/许娄.md "wikilink")）
  - 子：[祇摩尼师今](../Page/祇摩尼师今.md "wikilink")

## 注释

## 外部链接

  - [《三国史记》校勘说明](https://web.archive.org/web/20051103021242/http://dprk-cn.com/history/samguksagi.htm)
  - [韩国首爾大学奎章阁（The Kyujanggak
    Archives）电子文本下载](http://kyujanggak.snu.ac.kr/info/info01.jsp)

[Category:三國時代新羅君主](../Category/三國時代新羅君主.md "wikilink")