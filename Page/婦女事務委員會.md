**婦女事務委員會**（[英文](../Page/英文.md "wikilink")：）是[香港專責處理婦女事務的](../Page/香港.md "wikilink")[公營機構](../Page/香港公營機構.md "wikilink")，於2001年1月15日成立。委員會的職責，主要是促進香港[女性的權益與福祉](../Page/女性.md "wikilink")，並且向[香港政府提供婦女事務政策的意見](../Page/香港政府.md "wikilink")。

## 創立使命

「促使女性在生活各方面充分獲得應有的地位、權利及機會。」

## 組織架構

委員會轄下成立了四個工作小組推展其工作：

  - 締造有利環境工作小組
  - 增強能力工作小組
  - 公眾教育工作小組
  - 協作工作小組

## 歷任主席

  - [梁劉柔芬](../Page/梁劉柔芬.md "wikilink")（2001年1月15日—2006年1月15日）
  - [高靜芝](../Page/高靜芝.md "wikilink")　（2006年1月15日—2010年1月15日）
  - [劉靳麗娟](../Page/劉靳麗娟.md "wikilink")（2010年1月15日—2018年1月15日）
  - [陳婉嫻](../Page/陳婉嫻.md "wikilink")　（2018年1月15日—）

## 委員會成員

婦女事務委員會由非官方成員擔任主席，另有20名非官方成員和三名官方成員，全部由[香港特別行政區行政長官委任](../Page/香港特別行政區行政長官.md "wikilink")。婦女事務委員會於2018年1月15日公布的成員名單\[1\]：

**主席**

  - [陳婉嫻](../Page/陳婉嫻.md "wikilink")

**官方成員**

  - [勞工及福利局常任秘書長或代表](../Page/勞工及福利局.md "wikilink")
  - [政制及內地事務局局長或代表](../Page/政制及內地事務局.md "wikilink")
  - [社會福利署署長或代表](../Page/社會福利署.md "wikilink")

**非官方成員**

  - [陳麗雲](../Page/陳麗雲_\(教授\).md "wikilink")
  - [趙麗娟](../Page/趙麗娟.md "wikilink")
  - [Aruna Gurung](../Page/Aruna_Gurung.md "wikilink")
  - [洪雪蓮](../Page/洪雪蓮.md "wikilink")
  - [關蕙](../Page/關蕙.md "wikilink")
  - [林恬兒](../Page/林恬兒.md "wikilink")
  - [劉仲恆](../Page/劉仲恆.md "wikilink")
  - [李錦霞](../Page/李錦霞.md "wikilink")
  - [梁頌恩](../Page/梁頌恩.md "wikilink")
  - [羅婉文](../Page/羅婉文.md "wikilink")
  - [伍婉婷](../Page/伍婉婷.md "wikilink")
  - [龐愛蘭](../Page/龐愛蘭.md "wikilink")
  - [蘇潔瑩](../Page/蘇潔瑩.md "wikilink")
  - [鄧銘心](../Page/鄧銘心.md "wikilink")
  - [蔡曉慧](../Page/蔡曉慧.md "wikilink")
  - [蔡永忠](../Page/蔡永忠.md "wikilink")
  - [王少華](../Page/王少華.md "wikilink")
  - [黃何淑英](../Page/黃何淑英.md "wikilink")
  - [楊建霞](../Page/楊建霞.md "wikilink")
  - [嚴楚碧](../Page/嚴楚碧.md "wikilink")

**秘書**

  - [勞工及福利局助理秘書長](../Page/勞工及福利局.md "wikilink")（福利）2A

## 參考資料

## 外部連結

  - [婦女事務委員會](http://www.women.gov.hk)

[婦女事務委員會](../Category/香港公營機構.md "wikilink")

1.