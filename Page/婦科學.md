**婦科學**是一門研究女性在非孕期[生殖系統](../Page/生殖系統.md "wikilink")（例如[子宮](../Page/子宮.md "wikilink")、[卵巢](../Page/卵巢.md "wikilink")、[輸卵管或](../Page/輸卵管.md "wikilink")[陰道等](../Page/陰道.md "wikilink")）的生理病理改變，並對其進行診斷、處理的[臨床醫學學科](../Page/臨床醫學.md "wikilink")。

婦科學通常包括婦科學基礎（女性一生生理變化、[月經生理](../Page/月經生理.md "wikilink")、女性生殖內分泌等）、女性生殖器炎症（[外陰炎](../Page/外陰炎.md "wikilink")、[陰道炎](../Page/陰道炎.md "wikilink")、[宮頸炎](../Page/宮頸炎.md "wikilink")、、[附件炎](../Page/附件炎.md "wikilink")、[盆腔炎](../Page/盆腔炎.md "wikilink")、[性傳染疾病等](../Page/性傳染疾病.md "wikilink")）、女性生殖器腫瘤（[外陰](../Page/女陰.md "wikilink")、[陰道](../Page/陰道.md "wikilink")、[宮頸](../Page/宮頸.md "wikilink")、[子宮](../Page/子宮.md "wikilink")、[輸卵管](../Page/輸卵管.md "wikilink")、[卵巢等良惡性腫瘤等](../Page/卵巢.md "wikilink")）、生殖內分泌疾病（[功能失調性子宮出血](../Page/功能失調性子宮出血.md "wikilink")、[閉經](../Page/閉經.md "wikilink")、[多囊卵巢綜合症](../Page/多囊卵巢綜合症.md "wikilink")、[痛經](../Page/痛經.md "wikilink")、[絕境綜合症等](../Page/絕境綜合症.md "wikilink")）、女性生殖器官損傷性疾病（[子宮脫垂](../Page/子宮脫垂.md "wikilink")、、等）、女性生殖器官發育異常及先天畸形、女性其他生殖疾病（[子宮內膜異位症](../Page/子宮內膜異位症.md "wikilink")、[子宮腺肌病](../Page/子宮腺肌病.md "wikilink")、[不孕症](../Page/不孕症.md "wikilink")）等。

## 妇科疾病

[Obstetrical_examination_(1822).jpg](https://zh.wikipedia.org/wiki/File:Obstetrical_examination_\(1822\).jpg "fig:Obstetrical_examination_(1822).jpg")

### 外阴上皮内非瘤样病变

  - [外阴鳞状上皮增生](../Page/外阴鳞状上皮增生.md "wikilink")
  - [外阴硬化性苔藓](../Page/外阴硬化性苔藓.md "wikilink")
  - [外阴硬化性苔藓合并鳞状上皮增生](../Page/外阴硬化性苔藓合并鳞状上皮增生.md "wikilink")

### [外阴炎](../Page/外阴炎.md "wikilink")

### [阴道炎](../Page/阴道炎.md "wikilink")

### [宫颈炎](../Page/宫颈炎.md "wikilink")

### [生殖器结核](../Page/生殖器结核.md "wikilink")

### [盆腔炎](../Page/盆腔炎.md "wikilink")

### 外阴肿瘤

  - [外阴良性肿瘤](../Page/外阴良性肿瘤.md "wikilink")
  - [外阴上皮内瘤变](../Page/外阴上皮内瘤变.md "wikilink")
  - [外阴恶性肿瘤](../Page/外阴恶性肿瘤.md "wikilink")

### 宫颈肿瘤

  - [宫颈上内瘤变](../Page/宫颈上内瘤变.md "wikilink")
  - [宫颈癌](../Page/宫颈癌.md "wikilink")

### 子宫肿瘤

  - [子宫肌瘤](../Page/子宫肌瘤.md "wikilink")
  - [子宫内膜癌](../Page/子宫内膜癌.md "wikilink")
  - [子宫肉瘤](../Page/子宫肉瘤.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
## 外部連結

  - [Ingenious](http://www.ingenious.org.uk/See/Medicineandhealth/Obstetricsgynaecologyandcontraception/):
    archive of historical images related to obstetrics, gynaecology, and
    contraception.
  - [U.S Federal Government Website for Women´s Health
    Information](http://www.womenshealth.gov).

## 參見

  - [婦產科學](../Page/婦產科學.md "wikilink")
  - [產科學](../Page/產科學.md "wikilink")
  - [女性疾病](../Page/女性疾病.md "wikilink")
  - [陰道炎](../Page/陰道炎.md "wikilink")

{{-}}

[Category:医学专业](../Category/医学专业.md "wikilink")
[婦科學](../Category/婦科學.md "wikilink")
[Category:医学院所教科目](../Category/医学院所教科目.md "wikilink")