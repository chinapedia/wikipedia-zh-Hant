[子喉七劉海東訃聞1965March25Thu.jpg](https://zh.wikipedia.org/wiki/File:子喉七劉海東訃聞1965March25Thu.jpg "fig:子喉七劉海東訃聞1965March25Thu.jpg")
**子喉七**（），原名**劉海東**，[廣東省](../Page/廣東省.md "wikilink")[順德縣人](../Page/順德縣.md "wikilink")，[粵劇及電影演員](../Page/粵劇.md "wikilink")。於清末民初時期成名，是20年代粵劇名-{丑}-。\[1\]\[2\]由於唱功十分獨到，所以在[珠江一帶很受歡迎](../Page/珠江.md "wikilink")。在“大中華班”與[靚少華合作無間](../Page/靚少華.md "wikilink")，曾經演出《[醜女洞房](../Page/醜女洞房.md "wikilink")》，在《[風流天子](../Page/風流天子.md "wikilink")》飾演[楊貴妃](../Page/楊貴妃.md "wikilink")，在《[十奏嚴嵩](../Page/十奏嚴嵩.md "wikilink")》飾演[嚴嵩](../Page/嚴嵩.md "wikilink")。\[3\]他還與[朱次伯在](../Page/朱次伯.md "wikilink")「環球樂戲院」拍演過。\[4\]\[5\]當時[粵劇已經開始盛行](../Page/粵劇.md "wikilink")[六柱制](../Page/六柱制.md "wikilink")。無論他所在那一班，都以「通天」來掛牌，表示他的戲路甚廣。1930年，“人壽年劇團”的六大臺柱是武生[新珠](../Page/新珠.md "wikilink")、小武[靚少佳](../Page/靚少佳.md "wikilink")、花旦[嫦娥英](../Page/嫦娥英.md "wikilink")、二花[小嫦娥](../Page/小嫦娥.md "wikilink")、小生[陳醒威](../Page/陳醒威.md "wikilink")、醜生[蛇仔利](../Page/蛇仔利.md "wikilink")，而他就是第七大臺柱。

1965年3月24日(星期三)凌晨2時，在[香港島](../Page/香港島.md "wikilink")[西環](../Page/西環.md "wikilink")[卑利士道](../Page/卑利士道.md "wikilink")[雅麗氏何妙齡那打素醫院因患淋巴腺癌病逝](../Page/雅麗氏何妙齡那打素醫院.md "wikilink")，享齡77歲，遺下妻子曹連璧、任職香港官立學校教師的兒子劉振華，以及任職護士的女兒劉冰壺。1965年3月25日(星期四)下午1時在[香港殯儀館出殯](../Page/香港殯儀館.md "wikilink")，遺體安葬柴灣華人永遠墳場\[6\]。

## 部份參演電影

子喉七 （劉海東）一生參演約50多齣黑白影片。

  - 銀幕處男作，[廣州](../Page/廣州.md "wikilink")[默片](../Page/默片.md "wikilink")《[裂痕](../Page/裂痕.md "wikilink")》1933年，飾演一位[人力車](../Page/人力車.md "wikilink")[搭客](../Page/搭客.md "wikilink")。
  - [呆佬拜壽](../Page/呆佬拜壽.md "wikilink")
    1933年，飾演第一[男主角呆佬](../Page/男主角.md "wikilink")。
  - [可憐女](../Page/可憐女.md "wikilink") 1935年
  - [傻佬祝壽](../Page/傻佬祝壽.md "wikilink") 1935年
  - [標準老婆](../Page/標準老婆.md "wikilink") 1936年
  - [鄉下佬探親家](../Page/鄉下佬探親家.md "wikilink") 1937年
    ([邵逸夫執導](../Page/邵逸夫.md "wikilink"))
  - [神出鬼沒](../Page/神出鬼沒.md "wikilink") 1937年
  - [鬥氣姑爺](../Page/鬥氣姑爺.md "wikilink") 1937年
  - [唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink") 1937年
  - [財運亨通](../Page/財運亨通.md "wikilink") 1937年
  - [神秘之夜](../Page/神秘之夜.md "wikilink") 1937年
  - [妻多夫賤續集](../Page/妻多夫賤續集.md "wikilink") 1937年
  - [七十二行](../Page/七十二行.md "wikilink") 1937年
  - [大義滅親](../Page/大義滅親.md "wikilink") 1937年
  - [拉車被辱](../Page/拉車被辱.md "wikilink") 1938年
  - [太平洋上的風雲](../Page/太平洋上的風雲.md "wikilink")
    1938年，飾演田慕橫，一名戇國軍兵丁，獻唱粵曲《攬住大刀當老婆》。
  - [真假武則天](../Page/真假武則天.md "wikilink") 1939年
  - [蓋世女英雄](../Page/蓋世女英雄.md "wikilink") 1939年
  - [棺材精](../Page/棺材精.md "wikilink") 1939年
  - [瓦崗寨](../Page/瓦崗寨.md "wikilink") 1939年
  - [廣東先生大破謀人寺](../Page/廣東先生大破謀人寺.md "wikilink") 1948年
  - 《大擺烏龍陣》(1950年)，息影電影

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  -
  - [中文電影資料庫](http://www.dianying.com/ft/person/Zihouqi)

[Category:順德人](../Category/順德人.md "wikilink")
[Category:粵劇演員](../Category/粵劇演員.md "wikilink")
[Z](../Category/香港粵劇演員.md "wikilink")
[Z](../Category/香港電影演員.md "wikilink")
[Z](../Category/香港粵語片演員.md "wikilink")
[Z](../Category/劉姓.md "wikilink")
[Category:罹患淋巴癌逝世者](../Category/罹患淋巴癌逝世者.md "wikilink")

1.  [鶴鳴 (Brunswick)
    七十八轉粗紋老唱片](http://www.bh2000.net/special/patzak/detail.php?id=2183)

2.  [《廣州市志》](http://www.gzsdfz.org.cn/gzsz/18/fs/sz18fs060106.htm)

3.  [粤劇小生王白駒榮](http://www.sdlib.com.cn/dfwx/sdyj/bjr.htm)，順德文叢，順德粤劇

4.
5.  [粤劇在南寧](http://www.huadu.gov.cn:8080/was40/detail?record=4784&channelid=4374),廣州市花都區人民政府

6.  1965年3月25日(星期四)，香港《大公報》，第一張，第三版。