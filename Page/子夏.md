[Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Bu_Shang_Zixia_(卜商_子夏).jpg](https://zh.wikipedia.org/wiki/File:Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Bu_Shang_Zixia_\(卜商_子夏\).jpg "fig:Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Bu_Shang_Zixia_(卜商_子夏).jpg")藏）。\]\]
**卜商**，字**子夏**，家貧，勤奮好學，是[孔子的重要弟子](../Page/孔子.md "wikilink")，與[子游同列](../Page/子游.md "wikilink")[孔門十哲的文學科](../Page/孔門十哲.md "wikilink")，長於[文學](../Page/文學.md "wikilink")，對《[詩經](../Page/詩經.md "wikilink")》有深入的研究。曾在魏國[西河](../Page/西河.md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[渭南](../Page/渭南.md "wikilink")）創辦學堂並授業，[魏文侯曾向子夏問問題](../Page/魏文侯.md "wikilink")。[李克](../Page/李悝.md "wikilink")、[吳起](../Page/吳起.md "wikilink")、[西門豹都是他的學生](../Page/西門豹.md "wikilink")，一般認為傳授《[公羊傳](../Page/公羊傳.md "wikilink")》的[公羊高](../Page/公羊高.md "wikilink")、《[穀梁傳](../Page/穀梁傳.md "wikilink")》的[穀梁子](../Page/穀梁子.md "wikilink")，也是子夏一派的門人。

## 生平

子夏出身贫穷，约前483年，他来到[鲁国拜孔子为师](../Page/鲁国.md "wikilink")。前476年他赴[晋国西河](../Page/晋国.md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[渭南](../Page/渭南.md "wikilink")）创办了一所学堂并在那裏[教書](../Page/教書.md "wikilink")，[三家分晉後該地成為](../Page/三家分晉.md "wikilink")[魏國領土](../Page/魏國.md "wikilink")。开创的“[西河学派](../Page/西河学派.md "wikilink")”培育出大批经国治世的良材，并成为前期[法家成长的摇篮](../Page/法家.md "wikilink")。[西河地方之人](../Page/西河.md "wikilink")，將子夏作為孔子看待。據-{云}-《[毛詩](../Page/毛詩.md "wikilink")》之學，是由子夏傳下。《[論語](../Page/論語.md "wikilink")》一書疑多出於他和門人手撰。

子夏長於[文學](../Page/文學.md "wikilink")，對[詩有深入的研究](../Page/詩.md "wikilink")，能通其義理，著有詩序，他教人致知求[仁的方法](../Page/仁.md "wikilink")：「學識要廣，志向要堅定，凡事要細心去問，要從淺近處去思考，以類推於的遠大地方，仁道就在這裏面了。」

後人非议子夏者多由二事，一是子夏语：“学而优则仕，仕而优则学。”二是子夏為喪子而痛哭失明，不愛惜身體。 子曰：『吾乃弟子亦６９之聖

## 子夏的性格

《[荀子](../Page/荀子.md "wikilink")》大略篇：子夏家貧，衣若縣鶉。人曰：「子何不仕？」曰：「諸侯之驕我者，吾不為臣；大夫之驕我者，吾不復見。[柳下惠與後門者同衣](../Page/柳下惠.md "wikilink")，而不見疑，非一日之聞也。爭利如蚤甲，而喪其掌。」其性格可見一斑。子夏向志於學，故被孔子譽為文學第一，與[子游同列](../Page/子游.md "wikilink")。和[子游一樣](../Page/子游.md "wikilink")，子夏也是孔子晚年的得意門生。

《论语》中子夏曾与孔子讨论《诗》句“巧笑倩兮，美目盼兮”，孔子以为“绘事后素”，子夏发挥夫子之意云“礼后乎？”甚得孔子嘉许。

子夏的勤奮好學，在孔門弟子中亦相當突出，並因此獲得孔子的贊賞和鼓勵，孔子也盡可能地加以引導和訓練，從而使子夏在孔門弟子中似乎是最全面地掌握了孔子的學說。以此而言，孔子去世之後，子夏最有資格統領孔門弟子。然因子夏性格所致，使其與其他弟子不能友好相處。他在孔子去世之後策劃推舉[有若而未果](../Page/有若.md "wikilink")，只好離開孔門，前往他國聚徒講學。

## 評價

早在子夏追隨孔子問學之前，好談論別人是非的[子貢曾問孔子](../Page/子貢.md "wikilink")，「[子張與子夏相比孰賢](../Page/子張.md "wikilink")？」孔子答曰：「子張也過，子夏也不及。」又問：「然則子張愈與？」曰：「過猶不及。」孔子的評價耐人尋味。[朱熹在注這段話時說](../Page/朱熹.md "wikilink")：「子張才高意廣，而好為苛難，故常過中；子夏篤信遵守，而規模狹隘，故常不及。」孔子也曾當面告誡子夏：「汝為君子儒，無為小人儒。」

## 歷代追封

  - [唐玄宗尊之为](../Page/唐玄宗.md "wikilink")“**魏侯**”
  - [宋真宗加封为](../Page/宋真宗.md "wikilink")“**河东公**”
  - [宋度宗又尊为](../Page/宋度宗.md "wikilink")“**魏公**”
  - [明世宗嘉靖九年](../Page/明世宗.md "wikilink")，改称“**先贤卜子**”。

## 參見

  - [儒家](../Page/儒家.md "wikilink")
  - [儒教](../Page/儒教.md "wikilink")（[孔教](../Page/孔教.md "wikilink")）
  - [孔子](../Page/孔子.md "wikilink")
  - [卜子家族翰林世系](../Page/卜子家族翰林世系.md "wikilink")
  - [卜子家族](../Page/卜子家族.md "wikilink")

[Category:儒家](../Category/儒家.md "wikilink")
[Category:春秋戰國儒家人物](../Category/春秋戰國儒家人物.md "wikilink")
[Category:孔子弟子](../Category/孔子弟子.md "wikilink")
[Category:卜姓](../Category/卜姓.md "wikilink")
[Category:生年不详](../Category/生年不详.md "wikilink")
[Category:卒年不详](../Category/卒年不详.md "wikilink")