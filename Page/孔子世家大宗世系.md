**[孔子世家大宗世系](../Page/孔子.md "wikilink")**：自[少典至孔子父](../Page/少典.md "wikilink")[叔梁紇](../Page/叔梁紇.md "wikilink")，歷超过五十世。自[孔子始](../Page/孔子.md "wikilink")，至清末民國初衍聖公[孔令貽](../Page/孔令貽.md "wikilink")，孔子世家大宗歷七十六代。第七十七代嫡孫[孔德成於](../Page/孔德成.md "wikilink")1920年襲封三十二代[衍聖公](../Page/衍聖公.md "wikilink")，後由[國民政府改任大成至聖先師奉祀官](../Page/國民政府.md "wikilink")。現任大成至聖先師奉祀官為第七十九代嫡孫[孔垂長](../Page/孔垂長.md "wikilink")，2009年就任。

## 孔子先祖

### 傳疑時期

| 世系     | 称谓                                   | 备注                              |
| ------ | ------------------------------------ | ------------------------------- |
| 五十有余世祖 | [传约八代的少典](../Page/少典氏.md "wikilink") | 神农氏传八代，五百有余年，因此少典氏至少有约八代，约五百有余年 |
| 四十九世祖  | [少典](../Page/少典.md "wikilink")       | 五百有余年，诸侯名称，非人名。                 |
| 四十八世祖  | [黃帝軒轅氏](../Page/黃帝軒轅氏.md "wikilink") |                                 |
| 四十七世祖  | [少昊金天氏](../Page/少昊.md "wikilink")    | 玄囂                              |
| 四十六世祖  | [蟜極](../Page/蟜極.md "wikilink")       |                                 |
| 四十五世祖  | [帝嚳高辛氏](../Page/帝嚳高辛氏.md "wikilink") |                                 |

### 虞夏時期

| 世系    | 称谓                                  | 备注 |
| ----- | ----------------------------------- | -- |
| 四十四世祖 | [契](../Page/契.md "wikilink")        |    |
| 四十三世祖 | [昭明](../Page/昭明.md "wikilink")      |    |
| 四十二世祖 | [相土](../Page/相土.md "wikilink")      |    |
| 四十一世祖 | [昌若](../Page/昌若.md "wikilink")      |    |
| 四十世祖  | [曹圉](../Page/曹圉.md "wikilink")      |    |
| 卅九世祖  | [季](../Page/季_\(商族\).md "wikilink") |    |
| 卅八世祖  | [王亥](../Page/王亥.md "wikilink")      |    |
| 卅七世祖  | [上甲微](../Page/上甲微.md "wikilink")    |    |
| 卅六世祖  | [報乙](../Page/報乙.md "wikilink")      |    |
| 卅五世祖  | [報丙](../Page/報丙.md "wikilink")      |    |
| 卅四世祖  | [報丁](../Page/報丁.md "wikilink")      |    |
| 卅三世祖  | [主壬](../Page/主壬.md "wikilink")      |    |
| 卅二世祖  | [主癸](../Page/主癸.md "wikilink")      |    |

### 殷商時期

| 世系   | 称谓                                   | 备注 |
| ---- | ------------------------------------ | -- |
| 卅一世祖 | 商王[成湯](../Page/成湯.md "wikilink")（太乙） |    |
| 三十世祖 | 商王[太丁](../Page/太丁.md "wikilink")     |    |
| 廿九世祖 | 商王[太甲](../Page/太甲.md "wikilink")（太宗） |    |
| 廿八世祖 | 商王[太庚](../Page/太庚.md "wikilink")     |    |
| 廿七世祖 | 商王[太戊](../Page/太戊.md "wikilink")（中宗） |    |
| 廿六世祖 | 商王[仲丁](../Page/仲丁.md "wikilink")     |    |
| 廿五世祖 | 商王[祖乙](../Page/祖乙.md "wikilink")     |    |
| 廿四世祖 | 商王[祖辛](../Page/祖辛.md "wikilink")     |    |
| 廿三世祖 | 商王[祖丁](../Page/祖丁.md "wikilink")     |    |
| 廿二世祖 | 商王[小乙](../Page/小乙.md "wikilink")     |    |
| 廿一世祖 | 商王[武丁](../Page/武丁.md "wikilink")（高宗） |    |
| 二十世祖 | 商王[祖甲](../Page/祖甲.md "wikilink")     |    |
| 十九世祖 | 商王[康丁](../Page/康丁.md "wikilink")     |    |
| 十八世祖 | 商王[武乙](../Page/武乙.md "wikilink")     |    |
| 十七世祖 | 商王[文丁](../Page/文丁.md "wikilink")     |    |
| 十六世祖 | 商王[帝乙](../Page/帝乙.md "wikilink")     |    |

### 西周及春秋時期

<table>
<thead>
<tr class="header">
<th><p>世系</p></th>
<th><p>称谓</p></th>
<th><p>封号 <sub>及其年代</sub></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>十五世祖</p></td>
<td><p><a href="../Page/微仲衍.md" title="wikilink">微仲衍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>十四世祖</p></td>
<td><p><a href="../Page/宋公稽.md" title="wikilink">宋公稽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>十三世祖</p></td>
<td><p><a href="../Page/宋丁公.md" title="wikilink">宋丁公</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>十二世祖</p></td>
<td><p><a href="../Page/宋前湣公.md" title="wikilink">宋前湣公</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>十一世祖</p></td>
<td><p><a href="../Page/弗父何.md" title="wikilink">弗父何</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>十世祖</p></td>
<td><p><a href="../Page/宋父周.md" title="wikilink">宋父周</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九世祖</p></td>
<td><p><a href="../Page/世子勝.md" title="wikilink">世子勝</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八世祖</p></td>
<td><p><a href="../Page/正考父.md" title="wikilink">正考父</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七世祖</p></td>
<td><p><a href="../Page/孔父嘉.md" title="wikilink">孔父嘉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六世祖</p></td>
<td><p><a href="../Page/木金父.md" title="wikilink">木金父</a></p></td>
<td><p><strong>肇聖王</strong> <sub><a href="../Page/雍正.md" title="wikilink">雍正元年</a>（1724年）追封</sub></p></td>
</tr>
<tr class="odd">
<td><p>高祖</p></td>
<td><p><a href="../Page/祁父.md" title="wikilink">祁父</a></p></td>
<td><p><strong>裕聖王</strong> <sub>雍正元年追封</sub></p></td>
</tr>
<tr class="even">
<td><p>曾祖</p></td>
<td><p><a href="../Page/防叔.md" title="wikilink">防叔</a></p></td>
<td><p><strong>詒聖王</strong> <sub>雍正元年追封</sub></p></td>
</tr>
<tr class="odd">
<td><p>祖父　</p></td>
<td><p><a href="../Page/伯夏.md" title="wikilink">伯夏</a></p></td>
<td><p><strong>昌聖王</strong> <sub>雍正元年追封</sub></p></td>
</tr>
<tr class="even">
<td><p>父</p></td>
<td><p><a href="../Page/叔梁紇.md" title="wikilink">叔梁紇</a></p></td>
<td><p><strong>齊國公</strong> <sub><a href="../Page/宋真宗.md" title="wikilink">宋真宗大中祥符元年</a>（1008年）追封</sub><br />
<strong>啟聖王</strong> <sub><a href="../Page/元文宗.md" title="wikilink">元文宗至順元年</a>（1330年）加封</sub><br />
<strong>啟聖公</strong> <sub><a href="../Page/明世宗.md" title="wikilink">明世宗嘉靖九年</a>（1530年）改封</sub><br />
<strong>啟聖王</strong> <sub>雍正元年追封</sub></p></td>
</tr>
</tbody>
</table>

## 孔子

  - **[孔子](../Page/孔子.md "wikilink")**，孔丘（字仲尼）

## 孔子後裔

### 戰國時期

<table>
<thead>
<tr class="header">
<th><p>世系</p></th>
<th><p>姓名</p></th>
<th><p>字</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>封号<sub>及其年代</sub></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>子</p></td>
<td><p><a href="../Page/孔鯉.md" title="wikilink">孔鯉</a></p></td>
<td><p>伯魚</p></td>
<td></td>
<td></td>
<td><p><strong>泗水侯</strong> <sub><a href="../Page/宋徽宗.md" title="wikilink">宋徽宗</a><a href="../Page/崇宁.md" title="wikilink">崇宁元年</a>（公元1102年）追封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>孫</p></td>
<td><p><a href="../Page/孔伋.md" title="wikilink">孔伋</a></p></td>
<td><p>子思</p></td>
<td></td>
<td></td>
<td><p><strong>沂水侯</strong> <sub><a href="../Page/宋徽宗.md" title="wikilink">宋徽宗</a><a href="../Page/崇宁.md" title="wikilink">崇宁元年</a>（公元1102年）追封</sub><br />
<strong>沂国述圣公</strong> <sub><a href="../Page/元英宗.md" title="wikilink">元英宗</a><a href="../Page/至順.md" title="wikilink">至順元年</a>（公元1330年）追封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>曾孫</p></td>
<td><p><a href="../Page/孔白.md" title="wikilink">孔白</a></p></td>
<td><p>子上</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/齐威王.md" title="wikilink">齐威王召为国相</a>，不就。</p></td>
</tr>
<tr class="even">
<td><p>玄孫</p></td>
<td><p><a href="../Page/孔求.md" title="wikilink">孔求</a></p></td>
<td><p>子家</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/楚國.md" title="wikilink">楚國召為官</a>，不受。</p></td>
</tr>
<tr class="odd">
<td><p>六代孫</p></td>
<td><p><a href="../Page/孔箕.md" title="wikilink">孔箕</a></p></td>
<td><p>子京</p></td>
<td></td>
<td></td>
<td></td>
<td><p>曾为魏相</p></td>
</tr>
<tr class="even">
<td><p>七代孫</p></td>
<td><p><a href="../Page/孔穿.md" title="wikilink">孔穿</a></p></td>
<td><p>子高</p></td>
<td></td>
<td></td>
<td></td>
<td><p>曾為<a href="../Page/楚.md" title="wikilink">楚</a>、<a href="../Page/魏.md" title="wikilink">魏</a>、<a href="../Page/赵.md" title="wikilink">赵争聘</a>，不仕。</p></td>
</tr>
<tr class="odd">
<td><p>八代孫</p></td>
<td><p><a href="../Page/孔谦.md" title="wikilink">孔谦</a></p></td>
<td><p>子順</p></td>
<td></td>
<td></td>
<td><p><strong>文信君</strong></p></td>
<td><p>曾为魏相。</p></td>
</tr>
</tbody>
</table>

### 文通君、奉祀君等

| 世系                             | 姓名                               | 字  | 生年 | 卒年                                                             | 封号<sub>及其年代</sub>                                                                  | 备注                                                                                                         |
| ------------------------------ | -------------------------------- | -- | -- | -------------------------------------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| 九代孫                            | [孔鮒](../Page/孔鮒.md "wikilink")   | 子魚 |    |                                                                | **魯國文通君** <sub>[秦始皇封](../Page/秦始皇.md "wikilink")</sub>                             | [秦始皇時封拜](../Page/秦始皇.md "wikilink")[少傅](../Page/少傅.md "wikilink")，後為[陳勝聘作博士](../Page/陳勝.md "wikilink")、太師。 |
| [孔騰](../Page/孔騰.md "wikilink") | 子襄                               |    |    | **奉祀君** <sub>[漢高祖十二年](../Page/漢高祖.md "wikilink")（前195年）封</sub> | 孔鮒弟。[汉惠帝时](../Page/汉惠帝.md "wikilink")，孔腾被徵为博士，后迁长沙王[太傅](../Page/太傅.md "wikilink")。 |                                                                                                            |
| 十代孫                            | [孔忠](../Page/孔忠.md "wikilink")   | 子貞 |    |                                                                |                                                                                    | 孔騰子，[漢文帝時徵为博士](../Page/漢文帝.md "wikilink")。                                                                 |
| 十一代孫                           | [孔武](../Page/孔武.md "wikilink")   | 子威 |    |                                                                |                                                                                    | [漢景帝時徵为博士](../Page/漢景帝.md "wikilink")。                                                                     |
| 十二代孫                           | [孔延年](../Page/孔延年.md "wikilink") |    |    |                                                                |                                                                                    | [漢武帝時徵为博士](../Page/漢武帝.md "wikilink")，后转太傅，迁大将军。                                                           |

### 褒成侯等

<table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>世系</p></th>
<th><p>姓名</p></th>
<th><p>字</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>封号<sub>及其年代</sub></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>十三代孫</p></td>
<td><p><a href="../Page/孔霸.md" title="wikilink">孔霸</a></p></td>
<td><p>次孺</p></td>
<td></td>
<td></td>
<td><p><strong>褒成君</strong></p></td>
<td><p><a href="../Page/漢元帝.md" title="wikilink">漢元帝时因曾为帝师拜太师</a>，赐爵关内侯，食邑八百戶。日后孔霸请求奉孔子祭祀，汉元帝下令以孔霸的食邑八百户祀孔子。孔霸因此令长子<a href="../Page/孔福.md" title="wikilink">孔福还鲁</a>，奉孔子祀。</p></td>
</tr>
<tr class="even">
<td><p>十四代孫</p></td>
<td><p><a href="../Page/孔吉.md" title="wikilink">孔吉</a> <sub>嫡长孫</sub></p></td>
<td><p>元士</p></td>
<td></td>
<td></td>
<td><p><strong>殷紹嘉侯</strong> <sub><a href="../Page/漢成帝.md" title="wikilink">漢成帝</a><a href="../Page/綏和.md" title="wikilink">綏和元年封</a></sub><br />
<strong>宋公</strong> <sub><a href="../Page/汉平帝.md" title="wikilink">汉平帝</a><a href="../Page/元始.md" title="wikilink">元始四年改</a></sub></p></td>
<td><p>奉商王<a href="../Page/成汤.md" title="wikilink">成汤祀</a>，食邑一千六百七十戶，为汉之<a href="../Page/二王三恪.md" title="wikilink">二王三恪</a>。一个月后，晋公爵。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔福.md" title="wikilink">孔福</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>褒成君</strong> <sub><a href="../Page/汉成帝.md" title="wikilink">汉成帝时袭</a></sub></p></td>
<td><p>孔霸长子。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>十五代孫</p></td>
<td><p><a href="../Page/孔房.md" title="wikilink">孔房</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>褒成君</strong> <sub><a href="../Page/漢哀帝.md" title="wikilink">漢哀帝建平二年袭</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>十六代孫</p></td>
<td><p><a href="../Page/孔均.md" title="wikilink">孔均</a></p></td>
<td><p>長平</p></td>
<td></td>
<td></td>
<td><p><strong>褒成侯</strong> <sub><a href="../Page/漢平帝.md" title="wikilink">漢平帝元始元年封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>十七代孫</p></td>
<td><p><a href="../Page/孔志.md" title="wikilink">孔志</a></p></td>
<td><p>道甫</p></td>
<td></td>
<td></td>
<td><p><strong>褒成侯</strong> <sub><a href="../Page/漢光武帝.md" title="wikilink">漢光武帝建武十四年封</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>十八代孫</p></td>
<td><p><a href="../Page/孔損.md" title="wikilink">孔損</a></p></td>
<td><p>君益</p></td>
<td></td>
<td></td>
<td><p><strong>褒亭侯</strong> <sub><a href="../Page/漢和帝.md" title="wikilink">漢和帝永元四年年封</a></sub></p></td>
<td><p>食邑一千户</p></td>
</tr>
<tr class="even">
<td><p>十九代孫</p></td>
<td><p><a href="../Page/孔曜.md" title="wikilink">孔曜</a></p></td>
<td><p>君曜</p></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub><a href="../Page/漢安帝.md" title="wikilink">漢安帝延光三年封</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>二十代孫</p></td>
<td><p><a href="../Page/孔完.md" title="wikilink">孔完</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>褒成侯</strong> <sub><a href="../Page/漢靈帝.md" title="wikilink">漢靈帝建寧二年封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>廿一代孫</p></td>
<td><p><a href="../Page/孔羨.md" title="wikilink">孔羨</a></p></td>
<td><p>子佘</p></td>
<td></td>
<td></td>
<td><p><strong>宗聖侯</strong> <sub><a href="../Page/魏文帝.md" title="wikilink">魏文帝黃初二年封</a></sub></p></td>
<td><p>孔完弟<a href="../Page/孔讚.md" title="wikilink">孔讚子</a>，拜奉议郎</p></td>
</tr>
<tr class="odd">
<td><p>廿二代孫</p></td>
<td><p><a href="../Page/孔震.md" title="wikilink">孔震</a></p></td>
<td><p>伯起</p></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub><a href="../Page/晉武帝.md" title="wikilink">晉武帝泰始三年封</a></sub></p></td>
<td><p>拜太常卿、黄门侍郎，食邑二百户。</p></td>
</tr>
<tr class="even">
<td><p>廿三代孫</p></td>
<td><p><a href="../Page/孔嶷.md" title="wikilink">孔嶷</a></p></td>
<td><p>成功</p></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub><a href="../Page/晉明帝.md" title="wikilink">晉明帝太寧三年封</a></sub></p></td>
<td><p>食邑二千户。</p></td>
</tr>
<tr class="odd">
<td><p>廿四代孫</p></td>
<td><p><a href="../Page/孔撫.md" title="wikilink">孔撫</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub>袭封</sub></p></td>
<td><p>豫章太守。袭封奉圣亭侯，食邑一千户。</p></td>
</tr>
<tr class="even">
<td><p>廿五代孫</p></td>
<td><p><a href="../Page/孔懿.md" title="wikilink">孔懿</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub>袭封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>廿六代孫</p></td>
<td><p><a href="../Page/孔鮮.md" title="wikilink">孔鮮</a></p></td>
<td><p>鲜之</p></td>
<td></td>
<td></td>
<td><p><strong>奉聖亭侯</strong> <sub><a href="../Page/宋文帝.md" title="wikilink">宋文帝元嘉十九年封</a></sub><br />
<strong>崇圣侯</strong> <sub>后改</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>廿七代孫</p></td>
<td><p><a href="../Page/孔乘.md" title="wikilink">孔乘</a></p></td>
<td><p>敬山</p></td>
<td></td>
<td></td>
<td><p><strong>崇聖大夫</strong> <sub><a href="../Page/北魏孝文帝.md" title="wikilink">北魏孝文帝延興三年封</a></sub></p></td>
<td><p>食邑五百户。</p></td>
</tr>
<tr class="odd">
<td><p>廿八代孫</p></td>
<td><p><a href="../Page/孔靈珍.md" title="wikilink">孔靈珍</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>崇聖侯</strong> <sub>北魏孝文帝太和十九年封</sub></p></td>
<td><p>食邑一百户。</p></td>
</tr>
<tr class="even">
<td><p>廿九代孫</p></td>
<td><p><a href="../Page/孔文泰.md" title="wikilink">孔文泰</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>崇聖侯</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三十代孫</p></td>
<td><p><a href="../Page/孔渠.md" title="wikilink">孔渠</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>崇聖侯</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>卅一代孫</p></td>
<td><p><a href="../Page/孔長孫.md" title="wikilink">孔長孫</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>恭聖侯</strong> <sub><a href="../Page/北齊文宣帝.md" title="wikilink">北齊文宣帝天保元年封</a></sub><br />
<strong>鄒國公</strong> <sub><a href="../Page/北周靜帝.md" title="wikilink">北周靜帝大象二年改封</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>卅二代孫</p></td>
<td><p><a href="../Page/孔英悊.md" title="wikilink">孔英悊</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>奉聖侯</strong> <sub><a href="../Page/陈伯宗.md" title="wikilink">陈废帝光大元年改封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔嗣悊.md" title="wikilink">孔嗣悊</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>紹聖侯</strong> <sub><a href="../Page/隋煬帝.md" title="wikilink">隋煬帝大業四年封</a></sub></p></td>
<td><p>孔英悊弟</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>卅三代孫</p></td>
<td><p><a href="../Page/孔德倫.md" title="wikilink">孔德倫</a></p></td>
<td><p>大经</p></td>
<td></td>
<td></td>
<td><p><strong>褒聖侯</strong> <sub><a href="../Page/唐高祖.md" title="wikilink">唐高祖武德九年封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>卅四代孫</p></td>
<td><p><a href="../Page/孔崇基.md" title="wikilink">孔崇基</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>褒聖侯</strong> <sub><a href="../Page/武后.md" title="wikilink">武后證聖元年封</a></sub></p></td>
<td></td>
</tr>
</tbody>
</table>

### 文宣公

<table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>世系</p></th>
<th><p>姓名</p></th>
<th><p>字</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>封号<sub>及其年代</sub></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>卅五代孫</p></td>
<td><p><a href="../Page/孔璲之.md" title="wikilink">孔璲之</a></p></td>
<td><p>藏暉</p></td>
<td></td>
<td></td>
<td><p><strong>褒聖侯</strong> <sub><a href="../Page/唐玄宗.md" title="wikilink">唐玄宗開元五年封</a></sub><br />
<strong>文宣公</strong> <sub>開元廿七年封</sub></p></td>
<td><p>兼兗州長史遷都水使者，食邑一千戶</p></td>
</tr>
<tr class="even">
<td><p>卅六代孫</p></td>
<td><p><a href="../Page/孔萱.md" title="wikilink">孔萱</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>卅七代孫</p></td>
<td><p><a href="../Page/孔齊卿.md" title="wikilink">孔齊卿</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/唐德宗.md" title="wikilink">唐德宗建中三年封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>卅八代孫</p></td>
<td><p><a href="../Page/孔惟晊.md" title="wikilink">孔惟晊</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/唐憲宗.md" title="wikilink">唐憲宗元和十三年封</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>卅九代孫</p></td>
<td><p><a href="../Page/孔策.md" title="wikilink">孔策</a></p></td>
<td><p>元勋</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/唐武宗.md" title="wikilink">唐武宗會昌二年封</a></sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四十代孫</p></td>
<td><p><a href="../Page/孔振.md" title="wikilink">孔振</a></p></td>
<td><p>國文</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/唐懿宗.md" title="wikilink">唐懿宗咸通四年封</a></sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>四十一代孫</p></td>
<td><p><a href="../Page/孔昭儉.md" title="wikilink">孔昭儉</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四十一代孫</p></td>
<td><p><a href="../Page/孔邈_(五代).md" title="wikilink">孔邈</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>四十二代孫</p></td>
<td><p><a href="../Page/孔光嗣.md" title="wikilink">孔光嗣</a></p></td>
<td><p>齋郎</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/唐哀帝.md" title="wikilink">唐哀帝天佑二年封泗水主簿</a></p></td>
</tr>
<tr class="even">
<td><p>四十三代孫</p></td>
<td><p><a href="../Page/孔仁玉.md" title="wikilink">孔仁玉</a></p></td>
<td><p>溫如</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/後唐明宗.md" title="wikilink">後唐明宗長興三年封</a></sub><br />
<strong>文宣公</strong> <sub><a href="../Page/后周太祖.md" title="wikilink">后周太祖广顺二年封</a></sub></p></td>
<td><p><a href="../Page/後唐明宗.md" title="wikilink">後唐明宗長興元年任曲阜县主簿</a>，改任邱县令。后周时，兼曲阜縣令、监察御史</p></td>
</tr>
<tr class="odd">
<td><p>四十四代孫</p></td>
<td><p><a href="../Page/孔宜.md" title="wikilink">孔宜</a></p></td>
<td><p>不疑</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/宋太宗.md" title="wikilink">宋太宗太平興國三年袭封</a></sub></p></td>
<td><p>宋太祖乾德四年封曲阜縣主簿，<a href="../Page/宋太宗.md" title="wikilink">宋太宗太平興國三年升为右赞善大夫</a></p></td>
</tr>
<tr class="even">
<td><p>四十五代孫</p></td>
<td><p><a href="../Page/孔延世.md" title="wikilink">孔延世</a></p></td>
<td><p>茂先</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub>宋太宗至道三年袭封</sub></p></td>
<td><p>宋太宗至道三年授曲阜令</p></td>
</tr>
<tr class="odd">
<td><p>四十六代孫</p></td>
<td><p><a href="../Page/孔聖佑.md" title="wikilink">孔聖佑</a></p></td>
<td><p>福卿</p></td>
<td></td>
<td></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/宋真宗.md" title="wikilink">宋真宗天禧五年封</a></sub></p></td>
<td><p>以光禄寺丞知仙源县事</p></td>
</tr>
</tbody>
</table>

### 衍聖公

<table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>世系</p></th>
<th><p>姓名</p></th>
<th><p>字</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>封号<sub>及其年代</sub></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>四十六代孫<br />
<sub>第一代衍聖公</sub></p></td>
<td><p><a href="../Page/孔宗願.md" title="wikilink">孔宗願</a></p></td>
<td><p>子莊</p></td>
<td><p>1003</p></td>
<td><p>1068</p></td>
<td><p><strong>文宣公</strong> <sub><a href="../Page/宋仁宗.md" title="wikilink">宋仁宗景祐二年</a>（1035年）</sub><br />
<strong>衍聖公</strong> <sub>至和二年改封</sub></p></td>
<td><p>孔宜次子<a href="../Page/孔延澤.md" title="wikilink">孔延澤之子</a>，孔圣佑堂弟。<a href="../Page/宋仁宗.md" title="wikilink">宋仁宗景祐二年</a>（1035年）任国子监主簿。</p></td>
</tr>
<tr class="even">
<td><p>四十七代孫<br />
<sub>第二代衍聖公</sub></p></td>
<td><p><a href="../Page/孔若蒙.md" title="wikilink">孔若蒙</a></p></td>
<td><p>公明</p></td>
<td></td>
<td></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋神宗.md" title="wikilink">宋神宗熙寧元年</a>（1068年）封</sub><br />
<strong>奉圣公</strong> <sub><a href="../Page/宋哲宗.md" title="wikilink">宋哲宗元祐元年</a>（1086年）改封</sub></p></td>
<td><p><a href="../Page/宋哲宗.md" title="wikilink">宋哲宗元符元年</a>（1098年）坐事奪爵</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔若虛.md" title="wikilink">孔若虛</a></p></td>
<td><p>公實</p></td>
<td></td>
<td></td>
<td><p><br />
<strong>奉圣公</strong> <sub><a href="../Page/宋哲宗.md" title="wikilink">宋哲宗元符元年袭</a></sub></p></td>
<td><p><a href="../Page/孔若虛.md" title="wikilink">孔若虛为孔若蒙弟</a>。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四十八代孫<br />
<sub>第三代衍聖公</sub></p></td>
<td><p><a href="../Page/孔端友.md" title="wikilink">孔端友</a> <sub><a href="../Page/孔氏南宗.md" title="wikilink">孔氏南宗</a></sub></p></td>
<td><p>子交</p></td>
<td><p>1078</p></td>
<td><p>1132</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋徽宗.md" title="wikilink">宋徽宗崇寧三年</a>（1104年）封</sub></p></td>
<td><p>孔若蒙子。後隨<a href="../Page/宋高宗.md" title="wikilink">宋高宗南渡</a>，為<a href="../Page/孔氏南宗.md" title="wikilink">孔氏南宗之祖</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔端操.md" title="wikilink">孔端操</a> <sub><a href="../Page/孔氏北宗.md" title="wikilink">孔氏北宗</a></sub></p></td>
<td><p>子堅</p></td>
<td></td>
<td><p>1133</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/刘齐.md" title="wikilink">刘齐阜昌二年</a>（1132年）被封权袭封</sub></p></td>
<td><p>孔若蒙子，孔端友弟。临时主持曲阜孔庙祭祀，此为北宗之始。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四十九代孫<br />
<sub>第四代衍聖公</sub></p></td>
<td><p><a href="../Page/孔玠.md" title="wikilink">孔玠</a> <sub>南宗</sub></p></td>
<td><p>锡老</p></td>
<td><p>1122</p></td>
<td><p>1154</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋高宗.md" title="wikilink">宋高宗</a><a href="../Page/绍兴.md" title="wikilink">绍兴二年</a>（1132年）袭封</sub></p></td>
<td><p>孔端操第四子，过继于孔端友为嗣</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔璠.md" title="wikilink">孔璠</a> <sub>北宗</sub></p></td>
<td><p>文老</p></td>
<td><p>1106</p></td>
<td><p>1143</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/金熙宗.md" title="wikilink">金熙宗</a><a href="../Page/天眷.md" title="wikilink">天眷三年</a>（1140年）封</sub></p></td>
<td><p><a href="../Page/孔端操.md" title="wikilink">孔端操次子</a>。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>五十代孫<br />
<sub>第五代衍聖公</sub></p></td>
<td><p><a href="../Page/孔搢.md" title="wikilink">孔搢</a> <sub>南宗</sub></p></td>
<td><p>季绅</p></td>
<td><p>1145</p></td>
<td><p>1193</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋高宗.md" title="wikilink">宋高宗绍兴二十四</a>（1154年）年袭封</sub></p></td>
<td><p>孔玠子，<a href="../Page/宋高宗.md" title="wikilink">宋高宗绍兴二十四</a>（1154年）年，补授为右承奉郎，袭封。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔拯_(金朝).md" title="wikilink">孔拯</a> <sub>北宗</sub></p></td>
<td><p>元济</p></td>
<td><p>1136</p></td>
<td><p>1161</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/金熙宗.md" title="wikilink">金熙宗皇統二年</a>（1142年）封</sub></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔摠.md" title="wikilink">孔摠</a> <sub>北宗</sub></p></td>
<td><p>元会</p></td>
<td><p>1138</p></td>
<td><p>1191</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/金世宗.md" title="wikilink">金世宗大定三年</a>（1163年）封</sub></p></td>
<td><p>孔拯弟。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五十一代孫<br />
<sub>第六代衍聖公</sub></p></td>
<td><p><a href="../Page/孔文遠.md" title="wikilink">孔文遠</a> <sub>南宗</sub></p></td>
<td><p>绍光</p></td>
<td><p>1185</p></td>
<td><p>1226</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋光宗.md" title="wikilink">宋光宗绍熙四年</a>（1193年）袭封</sub></p></td>
<td><p>孔搢子。<a href="../Page/宋光宗.md" title="wikilink">宋光宗绍熙四年</a>（1193年），授承奉郎，袭封。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔元措.md" title="wikilink">孔元措</a> <sub>北宗</sub></p></td>
<td><p>夢得</p></td>
<td><p>1182</p></td>
<td><p>1251</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/金章宗.md" title="wikilink">金章宗明昌二年</a>（1192年）封</sub><br />
<strong>衍聖公</strong> <sub><a href="../Page/元太宗.md" title="wikilink">元太宗五年</a>（1233年）續封</sub></p></td>
<td><p>孔摠子。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔元用.md" title="wikilink">孔元用</a> <sub>北宗</sub></p></td>
<td><p>俊卿</p></td>
<td></td>
<td><p>1227</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋.md" title="wikilink">宋</a><a href="../Page/寶慶.md" title="wikilink">寶慶元年</a>（1225年）封</sub></p></td>
<td><p><a href="../Page/孔拂.md" title="wikilink">孔拂子</a>，<a href="../Page/孔琥.md" title="wikilink">孔琥孫</a>，<a href="../Page/孔端立.md" title="wikilink">孔端立曾孫</a>，<a href="../Page/孔若愚.md" title="wikilink">孔若愚玄孫</a>，孔若愚乃孔宗願第三子。寶慶元年（1225年），<a href="../Page/宋.md" title="wikilink">宋軍收復山東時</a>，以孔元用為衍聖公。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>五十二代孫<br />
<sub>第七代衍聖公</sub></p></td>
<td><p><a href="../Page/孔萬春.md" title="wikilink">孔萬春</a> <sub>南宗</sub></p></td>
<td><p>耆老</p></td>
<td></td>
<td><p>1241</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋理宗.md" title="wikilink">宋理宗宝庆二年</a>（1226年）袭封</sub></p></td>
<td><p>孔文远子。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔之全.md" title="wikilink">孔之全</a> <sub>北宗</sub></p></td>
<td><p>工叔</p></td>
<td></td>
<td></td>
<td><p><strong>衍聖公</strong></p></td>
<td><p><a href="../Page/孔元用.md" title="wikilink">孔元用子</a>，<a href="../Page/蒙古.md" title="wikilink">蒙古滅</a><a href="../Page/金朝.md" title="wikilink">金朝前受封</a>，滅金後還爵於<a href="../Page/孔元措.md" title="wikilink">孔元措</a>。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>五十三代孫<br />
<sub>第八代衍聖公</sub></p></td>
<td><p><a href="../Page/孔洙.md" title="wikilink">孔洙</a> <sub>南宗</sub></p></td>
<td><p>思鲁</p></td>
<td><p>1229</p></td>
<td><p>1287</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/宋理宗.md" title="wikilink">宋理宗绍定四年袭封</a></sub></p></td>
<td><p>孔万春子。任吉州通判。元至元十九年（1282年）让爵于北宗。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔湞.md" title="wikilink">孔湞</a> <sub>北宗</sub></p></td>
<td><p>昭度</p></td>
<td></td>
<td></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/元憲宗.md" title="wikilink">元憲宗元年</a>（1251年）封</sub></p></td>
<td><p>孔元措弟<a href="../Page/孔元綋.md" title="wikilink">孔元綋孫</a>，<a href="../Page/孔之固.md" title="wikilink">孔之固子</a>，孔元措以為己嗣。後遭黜。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔治.md" title="wikilink">孔治</a> <sub>北宗</sub></p></td>
<td><p>世安</p></td>
<td><p>1236</p></td>
<td><p>1307</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/元成宗.md" title="wikilink">元成宗元貞元年</a>（1295年）封</sub></p></td>
<td><p>孔之全子。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五十四代孫<br />
<sub>第九代衍聖公</sub></p></td>
<td><p><a href="../Page/孔思誠.md" title="wikilink">孔思誠</a></p></td>
<td><p>致道</p></td>
<td></td>
<td></td>
<td><p><strong>衍聖公</strong></p></td>
<td><p>孔治子，後遭黜；</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔思晦.md" title="wikilink">孔思晦</a> <sub>南北合宗</sub></p></td>
<td><p>明道</p></td>
<td><p>1267</p></td>
<td><p>1333</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/元仁宗.md" title="wikilink">元仁宗</a><a href="../Page/延祐.md" title="wikilink">延祐三年</a>（1316年）封</sub></p></td>
<td><p><a href="../Page/孔浣.md" title="wikilink">孔浣子</a>，<a href="../Page/孔之厚.md" title="wikilink">孔之厚孫</a>，<a href="../Page/孔元孝.md" title="wikilink">孔元孝曾孫</a>，孔拂玄孫，<a href="../Page/元仁宗.md" title="wikilink">元仁宗查孔氏族谱</a>，察觉孔宗愿之后孔思晦是北宗长支，封孔思晦为衍圣公。自此南北正式合宗。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五十五代孫<br />
<sub>第十代衍聖公</sub></p></td>
<td><p><a href="../Page/孔克堅.md" title="wikilink">孔克堅</a></p></td>
<td><p>璟夫</p></td>
<td><p>1316</p></td>
<td><p>1370</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/元惠宗.md" title="wikilink">元惠宗</a>（<a href="../Page/元順帝.md" title="wikilink">元順帝</a>）至正六年（1346年）封</sub></p></td>
<td><p><a href="../Page/孔思晦.md" title="wikilink">孔思晦子</a></p></td>
</tr>
<tr class="even">
<td><p>五十六代孫<br />
<sub>第十一代衍聖公</sub></p></td>
<td><p><a href="../Page/孔希學.md" title="wikilink">孔希學</a></p></td>
<td><p>士行</p></td>
<td><p>1335</p></td>
<td><p>1381</p></td>
<td><p><strong>衍聖公</strong> <sub>元惠宗至正十五年（1355年）封</sub><br />
<strong>衍聖公</strong> <sub><a href="../Page/明太祖.md" title="wikilink">明太祖洪武元年</a>（1368年）續封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五十七代孫<br />
<sub>第十二代衍聖公</sub></p></td>
<td><p><a href="../Page/孔訥.md" title="wikilink">孔訥</a></p></td>
<td><p>言伯</p></td>
<td><p>1356</p></td>
<td><p>1400</p></td>
<td><p><strong>衍聖公</strong> <sub>明太祖洪武十七年（1384年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>五十八代孫<br />
<sub>第十三代衍聖公</sub></p></td>
<td><p><a href="../Page/孔公鑑.md" title="wikilink">孔公鑑</a></p></td>
<td><p>昭文</p></td>
<td><p>1380</p></td>
<td><p>1402</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明惠帝.md" title="wikilink">明惠帝建文二年</a>（1400年）封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五十九代孫<br />
<sub>第十四代衍聖公</sub></p></td>
<td><p><a href="../Page/孔彥縉.md" title="wikilink">孔彥縉</a></p></td>
<td><p>朝紳</p></td>
<td><p>1401</p></td>
<td><p>1455</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明成祖.md" title="wikilink">明成祖永樂八年</a>（1410年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六十代孫<br />
<sub>第十五代衍聖公</sub></p></td>
<td><p><a href="../Page/孔承慶.md" title="wikilink">孔承慶</a></p></td>
<td><p>永祚</p></td>
<td><p>1420</p></td>
<td><p>1450</p></td>
<td><p><strong>衍聖公</strong> <sub>早卒未襲爵，追封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>六十一代孫<br />
<sub>第十六代衍聖公</sub></p></td>
<td><p><a href="../Page/孔宏緒.md" title="wikilink">孔宏緒</a></p></td>
<td><p>以敬</p></td>
<td><p>1448</p></td>
<td><p>1504</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明景帝.md" title="wikilink">明景帝景泰元年封</a>，坐事奪爵</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔宏泰.md" title="wikilink">孔宏泰</a></p></td>
<td><p>以和</p></td>
<td><p>1450</p></td>
<td><p>1503</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明憲宗.md" title="wikilink">明憲宗成化五年</a>（1469年）封</sub></p></td>
<td><p>卒後，還爵於宏緒子聞韶</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>六十二代孫<br />
<sub>第十七代衍聖公</sub></p></td>
<td><p><a href="../Page/孔聞韶.md" title="wikilink">孔聞韶</a></p></td>
<td><p>知德</p></td>
<td><p>1482</p></td>
<td><p>1546</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明孝宗.md" title="wikilink">明孝宗弘治十六年</a>（1503年）封</sub></p></td>
<td><p>孔宏緒子</p></td>
</tr>
<tr class="even">
<td><p>六十三代孫<br />
<sub>第十八代衍聖公</sub></p></td>
<td><p><a href="../Page/孔貞幹.md" title="wikilink">孔貞幹</a></p></td>
<td><p>用濟</p></td>
<td><p>1522</p></td>
<td><p>1559</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明世宗.md" title="wikilink">明世宗嘉靖廿五年</a>（1546年）封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>六十四代孫<br />
<sub>第十九代衍聖公</sub></p></td>
<td><p><a href="../Page/孔尚賢.md" title="wikilink">孔尚賢</a></p></td>
<td><p>象之</p></td>
<td><p>1542</p></td>
<td><p>1620</p></td>
<td><p><strong>衍聖公</strong> <sub>明世宗嘉靖卅五年（1556年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六十五代孫<br />
<sub>第二十代衍聖公</sub></p></td>
<td><p><a href="../Page/孔衍植.md" title="wikilink">孔衍植</a></p></td>
<td><p>懋甲</p></td>
<td><p>1592</p></td>
<td><p>1647</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/明熹宗.md" title="wikilink">明熹宗天啟元年</a>（1621年）封</sub></p></td>
<td><p>孔聞韶次子<a href="../Page/孔貞寧.md" title="wikilink">孔貞寧孫</a>，<a href="../Page/孔尚坦.md" title="wikilink">孔尚坦子</a></p></td>
</tr>
<tr class="odd">
<td><p>六十六代孫<br />
<sub>第廿一代衍聖公</sub></p></td>
<td><p><a href="../Page/孔興燮.md" title="wikilink">孔興燮</a></p></td>
<td><p>起呂</p></td>
<td><p>1636</p></td>
<td><p>1667</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清世祖.md" title="wikilink">清世祖順治五年</a>（1648年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六十七代孫<br />
<sub>第廿二代衍聖公</sub></p></td>
<td><p><a href="../Page/孔毓圻.md" title="wikilink">孔毓圻</a></p></td>
<td><p>鍾在</p></td>
<td><p>1657</p></td>
<td><p>1723</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清聖祖.md" title="wikilink">清聖祖康熙六年</a>（1667年）封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>六十八代孫<br />
<sub>第廿三代衍聖公</sub></p></td>
<td><p><a href="../Page/孔傳鐸.md" title="wikilink">孔傳鐸</a></p></td>
<td><p>振路</p></td>
<td><p>1673</p></td>
<td><p>1732</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清世宗.md" title="wikilink">清世宗雍正元年</a>（1723年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六十九代孫<br />
<sub>第廿四代衍聖公</sub></p></td>
<td><p><a href="../Page/孔繼濩.md" title="wikilink">孔繼濩</a></p></td>
<td><p>體和</p></td>
<td><p>1697</p></td>
<td><p>1719</p></td>
<td><p><strong>衍聖公</strong> <sub>清世宗雍正十三年（1735年）追贈</sub></p></td>
<td><p>早卒</p></td>
</tr>
<tr class="odd">
<td><p>七十代孫<br />
<sub>第廿五代衍聖公</sub></p></td>
<td><p><a href="../Page/孔廣棨.md" title="wikilink">孔廣棨</a></p></td>
<td><p>京立</p></td>
<td><p>1713</p></td>
<td><p>1743</p></td>
<td><p><strong>衍聖公</strong> <sub>清世宗雍正九年（1731年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>七十一代孫<br />
<sub>第廿六代衍聖公</sub></p></td>
<td><p><a href="../Page/孔昭煥.md" title="wikilink">孔昭煥</a></p></td>
<td><p>顯文</p></td>
<td><p>1735</p></td>
<td><p>1782</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清高宗.md" title="wikilink">清高宗乾隆九年</a>（1744年）封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七十二代孫<br />
<sub>第廿七代衍聖公</sub></p></td>
<td><p><a href="../Page/孔憲培.md" title="wikilink">孔憲培</a></p></td>
<td><p>養元</p></td>
<td><p>1756</p></td>
<td><p>1793</p></td>
<td><p><strong>衍聖公</strong> <sub>清高宗乾隆四十八年（1783年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>七十三代孫<br />
<sub>第廿八代衍聖公</sub></p></td>
<td><p><a href="../Page/孔慶鎔.md" title="wikilink">孔慶鎔</a></p></td>
<td><p>陶甫</p></td>
<td><p>1787</p></td>
<td><p>1841</p></td>
<td><p><strong>衍聖公</strong> <sub>清高宗乾隆五十九年（1794年）封</sub></p></td>
<td><p>孔憲培弟<a href="../Page/孔憲增.md" title="wikilink">孔憲增子</a></p></td>
</tr>
<tr class="odd">
<td><p>七十四代孫<br />
<sub>第廿九代衍聖公</sub></p></td>
<td><p><a href="../Page/孔繁灝.md" title="wikilink">孔繁灝</a></p></td>
<td><p>文淵</p></td>
<td><p>1804</p></td>
<td><p>1860</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清宣宗.md" title="wikilink">清宣宗道光廿一年</a>（1841年）封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>七十五代孫<br />
<sub>第三十代衍聖公</sub></p></td>
<td><p><a href="../Page/孔祥珂.md" title="wikilink">孔祥珂</a></p></td>
<td><p>則君</p></td>
<td><p>1848</p></td>
<td><p>1876</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清穆宗.md" title="wikilink">清穆宗同治二年</a>（1863年）封</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七十六代孫<br />
<sub>第卅一代衍聖公</sub></p></td>
<td><p><a href="../Page/孔令貽.md" title="wikilink">孔令貽</a></p></td>
<td><p>穀孫</p></td>
<td><p>1872</p></td>
<td><p>1919</p></td>
<td><p><strong>衍聖公</strong> <sub><a href="../Page/清德宗.md" title="wikilink">清德宗光緒三年</a>（1877年）封，1915年續封</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>七十七代孫<br />
<sub>第卅二代衍聖公</sub></p></td>
<td><p><a href="../Page/孔德成.md" title="wikilink">孔德成</a></p></td>
<td><p>玉汝</p></td>
<td><p>1920</p></td>
<td><p>2008</p></td>
<td><p><strong>衍聖公</strong> <sub>1920年封</sub><br />
<strong><a href="../Page/大成至聖先師奉祀官.md" title="wikilink">大成至聖先師奉祀官</a></strong> <sub>1935年改封</sub></p></td>
<td></td>
</tr>
</tbody>
</table>

### 大成至聖先師奉祀官

| 世系    | 姓名                               | 字  | 生年    | 卒年    | 封号<sub>及其年代</sub>                | 备注     |
| ----- | -------------------------------- | -- | ----- | ----- | -------------------------------- | ------ |
| 七十七代孫 | [孔德成](../Page/孔德成.md "wikilink") | 玉汝 | 1920年 | 2008年 | **大成至聖先師奉祀官**                    |        |
| 七十八代孫 | [孔维益](../Page/孔维益.md "wikilink") |    | 1939年 | 1989年 |                                  | 早逝，未袭封 |
| 七十九代孫 | [孔垂长](../Page/孔垂长.md "wikilink") |    | 1975年 |       | **大成至聖先師奉祀官** <sub>2009年袭封</sub> |        |
| 八十代孫  | [孔佑仁](../Page/孔佑仁.md "wikilink") |    | 2006年 |       |                                  |        |

## 參見

  - [商朝君主世系图](../Page/商朝君主世系图.md "wikilink")
  - [孔子家族](../Page/孔子家族.md "wikilink")
  - [孔氏南宗](../Page/孔氏南宗.md "wikilink")
  - [孔子世家](../Page/孔子世家.md "wikilink")
  - [孔子世家谱](../Page/孔子世家谱.md "wikilink")

## 参考文献

## 外部链接

  - [孔氏宗亲网 总世系图](http://www.kong.org.cn/bbs2/a/a.asp?B=34&ID=30459)

{{-}}

[孔子世系](../Category/孔子世系.md "wikilink")