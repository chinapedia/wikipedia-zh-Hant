**孔尚任**（），[字](../Page/表字.md "wikilink")**聘之**，又字**季重**，[号](../Page/号.md "wikilink")**东塘**，又号**岸堂**，一号**雲亭山人**，[山東](../Page/山東.md "wikilink")[曲阜縣人](../Page/曲阜縣.md "wikilink")，[孔子第六十四代孫](../Page/孔子.md "wikilink")。歷官[国子监](../Page/国子监.md "wikilink")[博士](../Page/博士.md "wikilink")、[户部](../Page/户部.md "wikilink")[主事](../Page/主事.md "wikilink")、[员外郎](../Page/员外郎.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[戲曲](../Page/戲曲.md "wikilink")[作家](../Page/作家.md "wikilink")，著有《[桃花扇](../Page/桃花扇.md "wikilink")》，與《[长生殿](../Page/长生殿.md "wikilink")》作者[洪-{昇}-齊名](../Page/洪昇.md "wikilink")，俗謂“**南洪北孔**”。

## 生平

[顺治五年](../Page/顺治.md "wikilink")（1648年），九月十七日出生于[山东](../Page/山东.md "wikilink")[曲阜](../Page/曲阜.md "wikilink")。是[孔子第六十四代孙](../Page/孔子.md "wikilink")，孔贞璠之子，为[孔子世家六十户中的官庄户](../Page/孔子世家.md "wikilink")。早年考取[秀才](../Page/秀才.md "wikilink")，后来避乱随父在[曲阜北](../Page/曲阜.md "wikilink")[石门山中读书](../Page/石门山.md "wikilink")。

康熙二十三年（1684年），[康熙帝南巡](../Page/康熙帝南巡.md "wikilink")，路过[曲阜](../Page/曲阜.md "wikilink")，到[孔庙](../Page/曲阜孔庙.md "wikilink")[祭孔](../Page/祭孔.md "wikilink")，经人举荐，由孔尚任在天子前讲[经學](../Page/经學.md "wikilink")，受到赏识，被任命为[国子监](../Page/国子监.md "wikilink")[博士](../Page/博士.md "wikilink")。

康熙二十五年（1686年）随[工部侍郎到](../Page/工部侍郎.md "wikilink")[淮阳](../Page/淮阳.md "wikilink")，疏浚[黄河入海口](../Page/黄河.md "wikilink")，两年间他结识了一些[明代](../Page/明代.md "wikilink")[遗民](../Page/遗民.md "wikilink")，到[扬州参拜](../Page/扬州.md "wikilink")[史可法衣冠塚](../Page/史可法.md "wikilink")，到[金陵登](../Page/金陵.md "wikilink")[燕子矶](../Page/燕子矶.md "wikilink")，游[秦淮河](../Page/秦淮河.md "wikilink")，过明故宫，拜[明孝陵](../Page/明孝陵.md "wikilink")，到[栖霞山白云庵拜访道士](../Page/栖霞山.md "wikilink")**张瑶星**，了解许多[晚明與](../Page/晚明.md "wikilink")[南明的情况](../Page/南明.md "wikilink")，为他的作品《[桃花扇](../Page/桃花扇.md "wikilink")》搜集了许多素材。

康熙二十八年（1689年）底回[燕京后](../Page/燕京.md "wikilink")，与[顾天石合写剧本](../Page/顾天石.md "wikilink")《小忽雷》上演，当时[京師](../Page/京師.md "wikilink")[戏曲演出非常盛行](../Page/戏曲.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:The_Peach_Blossom_Fan_2012.JPG "fig:缩略图")
康熙三十三年（1694年），孔尚任迁任[户部](../Page/户部.md "wikilink")[主事](../Page/主事.md "wikilink")。康熙三十八年（1699年），升任户部广东司[员外郎](../Page/员外郎.md "wikilink")。同年6月，《[桃花扇](../Page/桃花扇.md "wikilink")》脱稿，演出立即轰动\[1\]。并受到康熙帝的重视，康熙从中吸取末代王朝的教训，認同忠孝節義觀念，经常阅读这部剧本。孔尚任声名大振，被当时称为“南洪北孔”文坛双星。（“南洪”指《[长生殿](../Page/长生殿.md "wikilink")》作者[洪昇](../Page/洪昇.md "wikilink")）。孔尚任在剧本中揭露了[晚明](../Page/晚明.md "wikilink")[黨爭狀況以及](../Page/黨爭.md "wikilink")[南明王朝權力核心的腐朽](../Page/南明王朝.md "wikilink")，也褒揚忠君思想，表扬了[史可法等明朝忠臣](../Page/史可法.md "wikilink")，讽刺了投降[清朝的叛将](../Page/清朝.md "wikilink")，尤其在最后一出一個魏国公[徐達后人的皂隶角色](../Page/徐達.md "wikilink")，在〈馀韵〉说出：“开国元勋留狗尾，换朝元老缩龟头”的词，暗讽明朝顯貴降清[剃髮留辮和](../Page/剃髮令.md "wikilink")[帽子服饰](../Page/帽子.md "wikilink")。

康熙三十九年（1700年），孔尚任因事被斥責而退休去職\[2\]。

康熙四十一年（1702年），孔尚任回到家乡[石门山隐居](../Page/石门山.md "wikilink")，六七年后，得到[天津诗人](../Page/天津.md "wikilink")[佟鋐](../Page/佟鋐.md "wikilink")（字蔗村）的帮助，《桃花扇》才得以刻板刊印。

康熙五十七年（1718年），春正月十五日逝于曲阜家中，年七十一。

## 遺跡

[Kong_Shangren_tomb.jpg](https://zh.wikipedia.org/wiki/File:Kong_Shangren_tomb.jpg "fig:Kong_Shangren_tomb.jpg")
孔尚任墓在[曲阜](../Page/曲阜.md "wikilink")[孔林](../Page/孔林.md "wikilink")，墓碑立于[雍正十三年](../Page/雍正.md "wikilink")（1735年）四月，上书“大清奉直大夫户部广东清吏司员外郎东塘先生之墓”。

## 著作

孔尚任的其他[著作尚有](../Page/著作.md "wikilink")《出山异数记》，记载他出任经过；《湖海集》，记述他疏浚河口时的诗文；《享金薄》，记录他收藏的[书画](../Page/书画.md "wikilink")[古玩](../Page/古玩.md "wikilink")。

## 參考文獻

  - 石玲，《桃花扇与传统文化二题》 ，載《齐鲁文化研究》第三期，（濟南：山东师范大学，2004年）。

## 扩展阅读

  - Owen, Stephen, "Kong Shang-ren, *Peach Blossom Fan*: Selected Acts,"
    in Stephen Owen, ed. *An Anthology of Chinese Literature: Beginnings
    to 1911*. New York: ), 1997.
    [p. 942-972](http://courses.washington.edu/chin463/OwenPeachBlossom.pdf)
    ([Archive](http://www.webcitation.org/6PoEo2Lqa)).

[Category:清朝戶部主事](../Category/清朝戶部主事.md "wikilink")
[Category:清朝戶部員外郎](../Category/清朝戶部員外郎.md "wikilink")
[Category:清朝劇作家](../Category/清朝劇作家.md "wikilink")
[Category:曲阜人](../Category/曲阜人.md "wikilink")
[孔](../Category/孔子六十四代孫.md "wikilink")
[S尚任](../Category/孔姓.md "wikilink")

1.  《洪昇年谱》[康熙三十八年](../Page/康熙.md "wikilink")（1699年）载：“六月孔尚任《桃花扇》成，遂盛行于世。秋，康熙索《桃花扇》观之，其后孔尚任罢官，世多疑其系以《桃花扇》贾祸。”
2.  《乾隆曲阜县志》卷八十七说孔尚任“以事休致”。一說孔尚任因创作描寫康熙三十八年已卯（1699年）[顺天](../Page/顺天.md "wikilink")[乡试舞弊案的](../Page/乡试.md "wikilink")《通天榜》传奇而被皇帝“斥逐”罢官。[蒋攸銛](../Page/蒋攸銛.md "wikilink")《修撰李公蟠传》稱“而郎中孔尚任以作《通天榜》传奇，宣播都下，斥逐”。[章培恒等主编的](../Page/章培恒.md "wikilink")《中国文学史》（下）认为，孔尚任“后迁至户都员外郎，因故罢官”，“孔氏罢官原因不详。或以为与《桃花扇》的写成有关，此说不可靠。按作者于《桃花扇本末》中记宫中索剧本一事，是带炫耀的；又记他解官之后京中大僚犹群聚观赏此剧，更说明他井非由《桃花扇》得祸”。