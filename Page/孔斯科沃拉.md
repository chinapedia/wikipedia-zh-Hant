**孔斯科沃拉（Końskowola）**位于[波兰东南部](../Page/波兰.md "wikilink")，[卢布林省](../Page/卢布林省.md "wikilink")，介于[普瓦維和](../Page/普瓦維.md "wikilink")[卢布林之间](../Page/卢布林.md "wikilink")，离[库鲁夫市很近](../Page/库鲁夫市.md "wikilink")。在[庫魯夫卡河上](../Page/庫魯夫卡河.md "wikilink")。人口2000多人。

## 名称

孔斯科沃拉的波兰文Końskowola按照字面翻译是“马的意愿”，其实际的来源是Wola（[沃拉](../Page/沃拉.md "wikilink")，是波兰农村的一种形态）和它的主人Jan
z Konina的名字的组合，Konińskawola这个名字在1442年被提及。

## 历史

这个村庄可能建于14世纪，开始叫Witowska Wola。在19世纪的时候村子的名字改为现在的孔斯科沃拉。

1532年6月8日，孔斯科沃拉建立城镇，并逐渐成为辐射周边地区的食品贸易中心。当地还成立了几家纺织厂。很多人从波兰的其他地方迁移过来，还有人从[萨克森迁来](../Page/萨克森.md "wikilink")。

## 外部链接

  - [Końskowola during a war -
    照片](http://www.ushmm.org/uia-cgi/uia_query/photos?hr=null&query=kw123812)
  - [History of Końskowola](http://www.konskowola.eu)

[Category:卢布林省城市](../Category/卢布林省城市.md "wikilink")