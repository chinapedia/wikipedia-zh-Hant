[A-small_glyphs.svg](https://zh.wikipedia.org/wiki/File:A-small_glyphs.svg "fig:A-small_glyphs.svg")

**字形**（），又稱**字圖**或**書形**，是指[字的形体](../Page/文字.md "wikilink")。[中华人民共和国国家标准](../Page/中华人民共和国国家标准.md "wikilink")[GB/T
16964](../Page/GB/T_16964.md "wikilink")《信息技术·字型信息交换》中定义字形为“**一个可以辨认的抽象的图形符号，它不依赖于任何特定的设计**”。在[語言學中](../Page/語言學.md "wikilink")，[字](../Page/字.md "wikilink")（character）是[語意的最基本單位](../Page/語意.md "wikilink")，即[語素](../Page/語素.md "wikilink")；字形是指為了表達這個意義的具體表達。同一字可以有不同的字形，而不影響其表達的意思，例如[拉丁字母第一個字母可以寫作](../Page/拉丁字母.md "wikilink")或，漢字中的“-{強／强}-”、“-{戶／户／戸}-”。

## 東方（漢字）字形大

### 形音義

每個漢字均有三個屬性：形狀（形）、聲韻（音）、意義（義），統稱「形音義」。

### 新舊字形

[中國大陸於](../Page/中國大陸.md "wikilink")1965年出版的《[印刷通用漢字字形表](../Page/印刷通用漢字字形表.md "wikilink")》規定了印刷字形的標準，在表中規定的字形俗稱為*新字形*，之前通用的字形則稱為*舊字形*\[1\]。其字形標準於1988年由《[現代漢語通用字表](../Page/現代漢語通用字表.md "wikilink")》取代。

由於大部分新字形的筆劃都較舊字形少，它們很多時會被錯認為[簡化字](../Page/簡化字.md "wikilink")。事實上新字形除了適用於簡化字，還適用於繁體字，例如「<span lang="zh-cn">-{過}-（-{过}-）</span>」、「<span lang="zh-cn">-{編}-（-{编}-）</span>」；較接近傳統的台灣寫法是「<span lang="zh-tw">-{過}-</span>」、「<span lang="zh-tw">-{編}-</span>」。在中國大陸廢除之異體字，於收入[中日韓統一表意文字時亦會在不會產生](../Page/中日韓統一表意文字.md "wikilink")[重形字的情況下根據新字形調整](../Page/重形字.md "wikilink")，稱為「無重形GB化」，例如「<span lang="zh-cn">-{諮}-（-{咨}-）</span>」、「<span lang="zh-cn">-{僱}-（-{雇}-）</span>」；較接近傳統的台灣寫法是「<span lang="zh-tw">-{諮}-</span>」、「<span lang="zh-tw">-{僱}-</span>」，Unicode
Code Chart[1](http://www.unicode.org/charts/)
所示的就是這種字形。原则上[Unicode](../Page/Unicode.md "wikilink")/[ISO
10646只对字](../Page/ISO_10646.md "wikilink")，而非字形[编码](../Page/编码.md "wikilink")。[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")
[簡體中文版中](../Page/簡體中文.md "wikilink")[宋體Simsun是中國大陸新字形Font](../Page/宋體.md "wikilink")，但是卻也包含不少舊字形漢字。其實這些舊字形漢字並非是宋體新舊漢字同時收錄，舊字形是來源於Unicode/ISO
10646中臺灣韓國日本區域的漢字，臺灣韓國日本區域的某些漢字則是中國大陸的舊字形。

下面所列舉的新舊字形，在Unicode/ISO
10646中，是中國大陸跟香港、臺灣、日本、韓國同源漢字不同形的關係，對於中國大陸而言，則是新舊字形的關係。中國大陸並沒有給舊字形編碼，給舊字形編碼的主要是臺灣和日本區域。

例：
[Xin-jiu-zixing.png](https://zh.wikipedia.org/wiki/File:Xin-jiu-zixing.png "fig:Xin-jiu-zixing.png")

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>旧字形</p></th>
<th><p>新字形</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p>令</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6</p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>旧字形</p></th>
<th><p>新字形</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p>直</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p>兪</p></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td><p>骨</p></td>
<td><p>10</p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>旧字形</p></th>
<th><p>新字形</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td><p>敝</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p>彗</p></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>13</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>13</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>13</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>13</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>18</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

### 字形標準

以上所稱字形分別通常只是很小分別，筆劃差異在一兩筆之間。差異很大的通常歸類為[異體字](../Page/異體字.md "wikilink")，中國大陸標準為《[第一批異體字整理表](../Page/第一批異體字整理表.md "wikilink")》。和中國大陸不同，香港和台灣將同一字的不同的字形，即使只有很小分別，都歸類為[異體字](../Page/異體字.md "wikilink")。香港教育界所據標準為[常用字字形表](../Page/常用字字形表.md "wikilink")。台灣所據標準為[常用國字標準字體表](../Page/常用國字標準字體表.md "wikilink")、[次常用國字標準字體表](../Page/次常用國字標準字體表.md "wikilink")、[罕用字體表及](../Page/罕用字體表.md "wikilink")[異體字表](../Page/異體字表.md "wikilink")。

## 西方字形

[LatinAgraphemeVariations.svg](https://zh.wikipedia.org/wiki/File:LatinAgraphemeVariations.svg "fig:LatinAgraphemeVariations.svg")「a」之兩種常用字形，一為[手寫體](../Page/手寫體.md "wikilink")，一為[印刷體](../Page/印刷體.md "wikilink")\]\]

最常舉的例子是貨幣「圓」之符號「[$](../Page/$.md "wikilink")」，使用一豎、兩豎、穿透或是不穿透皆不影響該符號之意義。

和文字有關：

  - 多於一個字母連寫時產生的[連寫體](../Page/合字.md "wikilink")（Ligature）
  - 同一字母，因寫於字（Word）的不同位置而改變形狀的[變寫體](../Page/變寫體.md "wikilink")（例子：[s](../Page/s.md "wikilink")
    vs [長s](../Page/長s.md "wikilink")）

## 参考文献

### 引用

### 网页

  - [旧字形的使用现状，文中第9层](https://web.archive.org/web/20090504172506/http://www.pkucn.com/viewthread.php?tid=144689&extra=page%3D1)

## 参见

  - [异体字](../Page/异体字.md "wikilink")
  - [字體](../Page/字體.md "wikilink")
  - [字位](../Page/字位.md "wikilink")
  - [校對](../Page/校對.md "wikilink")

{{-}}

[Category:文字](../Category/文字.md "wikilink")

1.  <http://www.yywzw.com/show.aspx?cid=114&id=613>