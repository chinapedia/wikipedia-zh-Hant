**孙凤武**（），[江苏省](../Page/江苏省.md "wikilink")[淮阴专区](../Page/淮阴专区.md "wikilink")[淮安县](../Page/淮安县_\(1914年\).md "wikilink")（今[淮安市](../Page/淮安市.md "wikilink")[淮安区](../Page/淮安区.md "wikilink")）人，[中国男子篮球运动员](../Page/中华人民共和国.md "wikilink")、教练，前[中国男篮主力](../Page/中国国家男子篮球队.md "wikilink")[组织后卫](../Page/组织后卫.md "wikilink")、队长，现任[中国女篮主教练](../Page/中国国家女子篮球队.md "wikilink")。

## 职业生涯

孙凤武1970年开始接受专业[篮球训练](../Page/篮球.md "wikilink")，1979年入选江苏省青年队，1982年进入[钱澄海率领的中国男篮](../Page/钱澄海.md "wikilink")，在之后的十年中，他作为国家队队长率队获得过6次亚洲冠军，参加过三次[奥运会](../Page/奥运会.md "wikilink")。

他技术全面，运球、突破能力强，行进间传接球技术熟练，投篮准确，战术意识和组织能力强。

## 教练生涯

1992年退役后的孙凤武前往[新加坡](../Page/新加坡.md "wikilink")，担任新加坡国家队总教练。1996年担任[江苏南钢龙篮球俱乐部主教练](../Page/江苏南钢龙篮球俱乐部.md "wikilink")，1998年出任北京精狮篮球俱乐部主教练。2002年出任国家青年女篮主教练，率队获得世青赛第三名。2005年出任国家青年男篮主教练。2007年出任中国女篮二队主教练。

## 个人荣誉

  - 1983年 [第五届全运会评为优秀得分手](../Page/中华人民共和国第五届运动会.md "wikilink")
  - 1983年 入选[亚洲篮球锦标赛最佳阵容](../Page/亚洲篮球锦标赛.md "wikilink")，最佳防守队员
  - 1999年 被选为新中国篮球运动50杰之一

## 参考资料

## 外部链接

  - [孙凤武迎来亚锦赛首胜
    功勋教练终极目标只有夺冠](http://www.ba55.cn/SportsNews/k/2007-06-04/20262962783.shtml)
  - [天南地北淮安人·孙凤武](http://szb.huaian.gov.cn/szb/jsp/content/content.jsp?articleId=54933)
  - [2012伦敦奥运会中国体育代表团成员名录
    孙凤武](http://2012teamchina.olympic.cn/index.php/personview/persons/15901)

[F](../Category/孙姓.md "wikilink")
[Category:淮安区人](../Category/淮安区人.md "wikilink")
[Category:中国奥运篮球运动员](../Category/中国奥运篮球运动员.md "wikilink")
[Category:中国国家男子篮球队运动员](../Category/中国国家男子篮球队运动员.md "wikilink")
[Category:中国篮球教练](../Category/中国篮球教练.md "wikilink")
[Category:中国国家队教练](../Category/中国国家队教练.md "wikilink")
[Category:江苏籍篮球运动员](../Category/江苏籍篮球运动员.md "wikilink")
[Category:1984年夏季奥林匹克运动会篮球运动员](../Category/1984年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:1988年夏季奥林匹克运动会篮球运动员](../Category/1988年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:1992年夏季奥林匹克运动会篮球运动员](../Category/1992年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:1986年亞洲運動會金牌得主](../Category/1986年亞洲運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會金牌得主](../Category/1990年亞洲運動會金牌得主.md "wikilink")