**孙如法**（），[字](../Page/表字.md "wikilink")**世行**，[号](../Page/号.md "wikilink")**俟居**。[浙江](../Page/浙江.md "wikilink")[餘姚縣人](../Page/餘姚縣.md "wikilink")。[明朝政治人物](../Page/明朝.md "wikilink")，萬曆癸未進士，官至[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")。赠[光禄寺](../Page/光禄寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")。

## 生平

孙如法[嘉靖三十八年](../Page/嘉靖.md "wikilink")（1559年）生于[浙江](../Page/浙江.md "wikilink")[余姚](../Page/余姚.md "wikilink")。[万历十一年](../Page/万历.md "wikilink")（1583年），二十四歲中[进士](../Page/进士.md "wikilink")，次年授[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")。[万历十四年](../Page/万历.md "wikilink")（1586年），孙如法因论建储及谏阻[郑贵妃进封得罪](../Page/郑贵妃.md "wikilink")，谪[潮阳尉](../Page/潮阳.md "wikilink")（[潮阳县](../Page/潮阳县.md "wikilink")[典史添注](../Page/典史.md "wikilink")，闲职）。孙如法是当时罕见的“九尉贬潮”的官吏之一\[1\]。孙如法抵[潮州后](../Page/潮州.md "wikilink")，大兴义举，游学讲艺，提携后进。後以疾归，累荐不出，隐居柳城别墅，以图史自娱。

## 佚事

《萬曆野獲編》載孫如法身材矮小，貌類侏儒\[2\]。

## 家族

孙家三代[尚书](../Page/尚书.md "wikilink")，其曾祖父[孙燧](../Page/孙燧.md "wikilink")，謚忠烈，赠[礼部](../Page/礼部.md "wikilink")[尚书](../Page/尚书.md "wikilink")；祖父曾任[南京](../Page/南京.md "wikilink")[礼部尚书](../Page/礼部尚书.md "wikilink")；父亲[吏部尚书](../Page/吏部尚书.md "wikilink")，赠[太子太保](../Page/太子太保.md "wikilink")；时有“昭代阀阅之盛，首数孙氏”之喻。

## 參考

[Category:明朝刑部主事](../Category/明朝刑部主事.md "wikilink")
[Category:明朝書法家](../Category/明朝書法家.md "wikilink")
[Category:余姚孙氏](../Category/余姚孙氏.md "wikilink")
[R如](../Category/孙姓.md "wikilink")

1.  “孙如法于万历十四年尉[潮阳](../Page/潮阳.md "wikilink")，杨文焕于万历十九年尉[海阳](../Page/海阳.md "wikilink")，[高攀龙于万历二十二年尉](../Page/高攀龙.md "wikilink")[揭阳](../Page/揭阳.md "wikilink")，[刘宏宝于万历二十三年尉](../Page/刘宏宝.md "wikilink")[惠来以及此期间](../Page/惠来.md "wikilink")[陈泰来尉](../Page/陈泰来.md "wikilink")[饶平](../Page/饶平.md "wikilink")，林材尉[程乡](../Page/程乡.md "wikilink")，周宏尉[澄海](../Page/澄海.md "wikilink")，沈昌尉[大埔](../Page/大埔縣.md "wikilink")，周元蹿尉[平远等](../Page/平远.md "wikilink")。除[普宁因新置](../Page/普宁.md "wikilink")，县冶首设[潮阳](../Page/潮阳.md "wikilink")[贵屿外](../Page/贵屿.md "wikilink")，九尉贬潮，十载之内，无邑无之……”
2.  《萬曆野獲編·[卷十二](../Page/s:萬曆野獲編/卷12#士大夫偉狀.md "wikilink")》：士紳短小者，如予所識，泰和郭司馬青螺（子章）、餘姚孫刑部俟居（如法）、常熟瞿都運洞觀（汝稷），皆渺小丈夫，貌類侏儒。然均為一時名碩，羽儀當世，真所謂失之子羽。