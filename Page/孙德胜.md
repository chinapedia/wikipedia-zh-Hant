**孙德胜**（，），他是[越南共产党](../Page/越南共产党.md "wikilink")、[越南民主共和国和](../Page/越南民主共和国.md "wikilink")[越南社会主义共和国的主要缔造者和领导人之一](../Page/越南社会主义共和国.md "wikilink")。繼[胡志明後任](../Page/胡志明.md "wikilink")[越南國家主席](../Page/越南國家主席.md "wikilink")。

## 生平

孙德胜生于[安江省](../Page/安江省.md "wikilink")[龙川市](../Page/龙川市.md "wikilink")，比[胡志明年长](../Page/胡志明.md "wikilink")2岁。早年即為忠誠的共產主義者。1912年去[法国](../Page/法国.md "wikilink")。1919年参加法国劳工总联合会。1920年回到越南，参加西贡市的地下工会运动。1926年参加越南青年革命同志会。1929年被法国殖民当局逮捕，判30年苦役并流放至[昆仑岛监狱](../Page/昆仑岛监狱.md "wikilink")。在狱中创建党支部，加入[印度支那共产党](../Page/印度支那共产党.md "wikilink")。1945年，[八月革命爆发](../Page/八月革命.md "wikilink")，孙德胜出狱，并担任南部抗日行政委员会委员。1946年任越南人民政府检察委员会委员长和中央爱国竞赛委员会主席。1949年当选越南国民大会常委会代主席。1950年被选为越中友协会长和越南保卫世界和平委员会主席。1951年当选越南国民联合战线全国委员会主席。1955年—1977年任祖国战线中央委员会主席团主席。在越党二大、三大、四大上均当选中央委员。

1960年7月至1969年9月任越南民主共和国副主席。1968年，越南劳动党主席兼国家主席[胡志明因](../Page/胡志明.md "wikilink")[心脏病病重](../Page/心脏病.md "wikilink")，无法处理国事，由孙德胜代行国家大事。1969年9月2日，胡志明因心脏病逝世，享年79岁。1969年9月[胡志明去世后继任国家主席](../Page/胡志明.md "wikilink")，与越南劳动党第一书记[黎笋和政府总理](../Page/黎笋.md "wikilink")[范文同共掌权力](../Page/范文同.md "wikilink")。1975年5月，北越攻占[南越首都](../Page/南越.md "wikilink")，长达16年的[越南战争结束](../Page/越南战争.md "wikilink")，全国统一，改国号为越南社会主义共和国。1976年4月25日，越南进行南北统一以后的第一次普选，孙德胜再次选举为国家主席。1977年当选[越南祖国阵线名誉主席](../Page/越南祖国阵线.md "wikilink")。

1980年3月30日，孙德胜在[河内因病逝世](../Page/河内.md "wikilink")，享年92岁。

越南官方评价：孙德胜是越南革命的伟大领导者与实践者，越南民主共和国与越南社会主义共和国的伟大领袖。

1967年曾获[列宁和平奖](../Page/列宁和平奖.md "wikilink")。

他还担任过国防会议主席、国会常务委员会主席、政府检查委员会总检查长、祖国战线名誉主席、越南国民联合战线全国委员会主席、越南总工会名誉主席、越南少年儿童委员会名誉主席、[世界和平理事会理事等职](../Page/世界和平理事会.md "wikilink")。越南[胡志明市](../Page/胡志明市.md "wikilink")以他命名。

[Category:越南国家主席](../Category/越南国家主席.md "wikilink")
[Category:安江省人](../Category/安江省人.md "wikilink")
[Category:孙姓](../Category/孙姓.md "wikilink")
[Category:越南國會主席](../Category/越南國會主席.md "wikilink")
[Category:第一屆越南共產黨中央委員會委員](../Category/第一屆越南共產黨中央委員會委員.md "wikilink")
[Category:第二屆越南共產黨中央委員會委員](../Category/第二屆越南共產黨中央委員會委員.md "wikilink")
[Category:第三屆越南共產黨中央委員會委員](../Category/第三屆越南共產黨中央委員會委員.md "wikilink")
[Category:第四屆越南共產黨中央委員會委員](../Category/第四屆越南共產黨中央委員會委員.md "wikilink")
[Category:越南共產黨中央委員會委員](../Category/越南共產黨中央委員會委員.md "wikilink")
[Category:第一屆越南國會代表](../Category/第一屆越南國會代表.md "wikilink")
[Category:第二屆越南國會代表](../Category/第二屆越南國會代表.md "wikilink")
[Category:第三屆越南國會代表](../Category/第三屆越南國會代表.md "wikilink")
[Category:第四屆越南國會代表](../Category/第四屆越南國會代表.md "wikilink")
[Category:第五屆越南國會代表](../Category/第五屆越南國會代表.md "wikilink")
[Category:第六屆越南國會代表](../Category/第六屆越南國會代表.md "wikilink")
[Category:斯大林和平奖得主](../Category/斯大林和平奖得主.md "wikilink")