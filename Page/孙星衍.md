**孙星衍**（），[字](../Page/表字.md "wikilink")**伯渊**，一字**渊如**，[号](../Page/号.md "wikilink")**季述**。[江蘇](../Page/江蘇.md "wikilink")[陽湖縣](../Page/陽湖縣.md "wikilink")（今屬[江苏省](../Page/江苏省.md "wikilink")[常州市](../Page/常州市.md "wikilink")）人，[祖籍](../Page/祖籍.md "wikilink")[安徽](../Page/安徽.md "wikilink")[濠州](../Page/濠州.md "wikilink")，[清代经史学家](../Page/清代.md "wikilink")，[考据学者](../Page/考据学.md "wikilink")，[金石学家](../Page/金石学.md "wikilink")。

## 生平

清[乾隆五十二年](../Page/乾隆.md "wikilink")（1787年）一甲第二名進士（[榜眼](../Page/榜眼.md "wikilink")），授[翰林院](../Page/翰林院.md "wikilink")[編修](../Page/編修.md "wikilink")，充[三通館校理](../Page/三通館.md "wikilink")。乾隆五十四年（1789年）[散館](../Page/散館.md "wikilink")，試厲志賦，用《[史記](../Page/史記.md "wikilink")》「如畏」典故，大學士[和珅疑為別字](../Page/和珅.md "wikilink")，置為三等，改部屬用。官[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")，為法寬恕，深得大學士[阿桂](../Page/阿桂.md "wikilink")、尚書[胡季堂器重](../Page/胡季堂.md "wikilink")。洊升[郎中](../Page/郎中.md "wikilink")。

乾隆六十年（1795年），授[山東](../Page/山東.md "wikilink")[兗沂曹濟道](../Page/兗沂曹濟道.md "wikilink")。[嘉慶元年](../Page/嘉慶.md "wikilink")（1796年）七月，曹南水漫灘潰，在[單縣決口](../Page/單縣.md "wikilink")，孫星衍與按察使[康基田率人奮戰抵禦五晝夜](../Page/康基田.md "wikilink")，未釀成大禍。不久，權[按察使](../Page/按察使.md "wikilink")。嘉慶四年（1799年），[丁母憂歸里](../Page/丁憂.md "wikilink")，浙江巡撫[阮元聘其為](../Page/阮元.md "wikilink")[詁經精舍主講](../Page/詁經精舍.md "wikilink")。服闋入都，仍發往山東。嘉慶十年（1805年），補山東[督糧道](../Page/督糧道.md "wikilink")。嘉慶十二年（1807年），權[布政使](../Page/布政使.md "wikilink")。嘉慶十六年（1811年），稱病歸返。晚年任[钟山书院](../Page/钟山书院.md "wikilink")[山長](../Page/山長.md "wikilink")。嘉慶二十三年（1818年）卒。《[清史稿](../Page/清史稿.md "wikilink")》有傳。\[1\]

## 成就

初以文学著称，与[洪亮吉](../Page/洪亮吉.md "wikilink")、[黄景仁齐名](../Page/黄景仁.md "wikilink")，后专事经史文字音韵训诂之学，兼及诸子百家，工于篆隶。对经学、史学、金石碑版、天文、地理诸领域皆造诣颇深。曾主持诂经精舍、钟山书院讲习。

## 著述

著有《平津馆读碑记》、《平津馆丛书》、《孙渊如全集》、《孔子集语》、《孫子集註》、《尚书今古文注疏》、《[祠堂書目](../Page/祠堂書目.md "wikilink")》等\[2\]\[3\]。

## 註釋

<div class="references-small">

<references />

</div>

## 参考资料

  - 《[清史列传](../Page/清史列传.md "wikilink")》
  - 《[清史稿](../Page/清史稿.md "wikilink")》
  - 《[辞源](../Page/辞源.md "wikilink")》

## 外部連結

  - [孫星衍](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:0:mctauac:NO%3DNO8158)
    中研院史語所

{{-}}

[Category:清朝榜眼](../Category/清朝榜眼.md "wikilink")
[Category:清朝翰林院編修](../Category/清朝翰林院編修.md "wikilink")
[Category:清朝刑部郎中](../Category/清朝刑部郎中.md "wikilink")
[Category:清朝道員](../Category/清朝道員.md "wikilink")
[Category:清朝儒學學者](../Category/清朝儒學學者.md "wikilink")
[Category:清朝經學家](../Category/清朝經學家.md "wikilink")
[Category:清朝訓詁學家](../Category/清朝訓詁學家.md "wikilink")
[Category:清朝駢文家](../Category/清朝駢文家.md "wikilink")
[Category:清朝金石学家](../Category/清朝金石学家.md "wikilink")
[Category:常州人](../Category/常州人.md "wikilink")
[X星](../Category/孫姓.md "wikilink")

1.  《清史稿·卷四八一》：孫星衍，字淵如，陽湖人。……
2.  梅江林,. 文献家孙星衍和《平津馆丛书》. 科教文汇, 2006, (04)
3.  刘蔷,. 论孙星衍的考据学思想及实践. 清华大学学报(哲学社会科学版) , 2005, (06)