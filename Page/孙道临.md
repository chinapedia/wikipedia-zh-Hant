[孙道临.jpg](https://zh.wikipedia.org/wiki/File:孙道临.jpg "fig:孙道临.jpg")照片\]\]
[Screenshot_from_The_Eternal_Wave.jpg](https://zh.wikipedia.org/wiki/File:Screenshot_from_The_Eternal_Wave.jpg "fig:Screenshot_from_The_Eternal_Wave.jpg")》劇照，中间的是**孙道临**饰演的角色李侠\]\]
**孙道临**（），原名**孙以亮**。祖籍[中国](../Page/中国.md "wikilink")[浙江](../Page/浙江.md "wikilink")[嘉善](../Page/嘉善.md "wikilink")，生于[北京](../Page/北京.md "wikilink")，居住于[上海](../Page/上海.md "wikilink")。[中国大陆著名](../Page/中国大陆.md "wikilink")[演员](../Page/演员.md "wikilink")、著名配音演员、[导演](../Page/导演.md "wikilink")、[朗诵家](../Page/朗诵家.md "wikilink")。

## 生平

1938年考入[燕京大学哲学系](../Page/燕京大学.md "wikilink")，受同学[黄宗江的影响](../Page/黄宗江.md "wikilink")，在校期间参加了[燕京剧社](../Page/燕京剧社.md "wikilink")，出演了《[雷雨](../Page/雷雨.md "wikilink")》、《生死恋》等剧。1941年[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，燕京大学及其剧社被迫关闭。孙道临一度以饲养奶牛为生。1943年，他先后在[中国旅行剧团](../Page/中国旅行剧团.md "wikilink")、上海[国华剧社](../Page/国华剧社.md "wikilink")、北平[南北剧社等做职业话剧演员](../Page/南北剧社.md "wikilink")。抗战胜利后，他又返回燕京大学读书，1947年毕业，在[焦菊隐艺术馆话剧队演黄宗江的话剧](../Page/焦菊隐.md "wikilink")《大团圆》。1948年应[金山邀请赴上海从影](../Page/金山_\(演員\).md "wikilink")，先后拍摄《大团圆》、《大雷雨》、《乌鸦与麻雀》等影片。上海战役后，进入[上海电影制片厂](../Page/上海电影制片厂.md "wikilink")。在半个世纪中，他在近30部电影和电视剧中担任过主角或重要角色。由于富于感情的嗓音，孙道临曾在《王子复仇记》、《基督山恩仇记》、《白痴》等二十余部外国影片配音，也曾导演电影《[詹天佑](../Page/詹天佑_\(电影\).md "wikilink")》。孙道临亦于1988年与[姜昆](../Page/姜昆.md "wikilink")、[侯耀文](../Page/侯耀文.md "wikilink")、[王刚](../Page/王刚_\(演员\).md "wikilink")、[薛飞共同主持](../Page/薛飞_\(电视主播\).md "wikilink")[当届](../Page/1988年中国中央电视台春节联欢晚会.md "wikilink")[中国中央电视台春节联欢晚会](../Page/中国中央电视台春节联欢晚会.md "wikilink")。

2007年12月28日上午，[北京时间](../Page/北京时间.md "wikilink")8点58分，因[心脏病突发于](../Page/心脏病.md "wikilink")[上海华东医院逝世](../Page/上海华东医院.md "wikilink")，享年86岁。胡锦涛、江泽民、温家宝、曾庆红、李长春、习近平、李克强、贺国强、王刚、王兆国、刘云山、李源潮、张高丽、李鹏、乔石、朱镕基、李瑞环、尉健行、李岚清、吴官正、华建敏、陈至立、丁关根、王太华、孙家正、胡振民、金炳华等通过各种方式表哀悼，向家属表示慰问。

2008年1月3日，自发到[上海龙华殡仪馆送别孙道临的普通上海市民仍络绎不绝](../Page/上海龙华殡仪馆.md "wikilink")。灵堂里有一个巨大荧幕，播放他生前最喜欢的舒伯特的音乐，滚动播放的是他早年电影片段，及他主持第一届[上海国际电影节](../Page/上海国际电影节.md "wikilink")，参加2005年中国电影百年庆典时影像画面。大厅外有孙道临早年电影海报为背景的签名墙。[上海万裕国际影城为纪念他](../Page/上海万裕国际影城.md "wikilink")，特别放映《[早春二月](../Page/早春二月.md "wikilink")》。

## 主要作品

### 银幕作品

  - 1948年－《大团圆》饰 三弟
  - 1949年- 《[乌鸦与麻雀](../Page/乌鸦与麻雀.md "wikilink")》 饰华洁之
  - 1950年- 《[民主青年进行曲](../Page/民主青年进行曲.md "wikilink")》饰 方哲仁
  - 1954年- 《[渡江侦察记](../Page/渡江侦察记.md "wikilink")》饰 李连长
  - 1955年- 《[南岛风云](../Page/南岛风云.md "wikilink")》饰 韩承光
  - 1956年- 《[家](../Page/家_\(电影\).md "wikilink")》饰觉新
  - 1957年- 《[不夜城](../Page/不夜城_\(电影\).md "wikilink")》饰 张伯韩
  - 1958年- 《[永不消逝的电波](../Page/永不消逝的电波.md "wikilink")》饰李侠
  - 1958年- 《[红色的种子](../Page/红色的种子.md "wikilink")》饰 雷鸣
  - 1961年- 《[革命家庭](../Page/革命家庭.md "wikilink")》饰 江梅清
  - 1961年－《51号兵站》饰 政委
  - 1963年- 《[早春二月](../Page/早春二月.md "wikilink")》饰肖涧秋
  - 1979年- 《[李四光](../Page/李四光_\(电影\).md "wikilink")》饰 李四光
  - 1982年- 《[一盘没有下完的棋](../Page/一盘没有下完的棋.md "wikilink")》饰况易山
  - 1983年- 《[雷雨](../Page/雷雨_\(电影\).md "wikilink")》饰周朴园
  - 1986年- 《[非常大总统](../Page/非常大总统_\(电影\).md "wikilink")》饰 孙中山
  - 1992年- 《[继母](../Page/继母_\(电影\).md "wikilink")》

### 配音作品

| 时间    | 名称             | 角色        |
| ----- | -------------- | --------- |
| 1965年 | 《战争与和平》*（前苏联）* | 旁白 \[32\] |
| 1958年 | 《王子复仇记》        | 哈姆雷特      |

### 导演作品

| 时间    | 名称      | 备注       |
| ----- | ------- | -------- |
| 2000年 | 《詹天佑》   | 与姚寿康合作导演 |
| 1992年 | 《继母》    | /        |
| 1986年 | 《非常大总统》 | 担任主演     |
| 1984年 | 《雷雨》    | 担任编剧、主演  |

## 参考资料

## 外部链接

  -
  -
[Category:嘉兴人](../Category/嘉兴人.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:北京演员](../Category/北京演员.md "wikilink")
[Category:北京男演员](../Category/北京男演员.md "wikilink")
[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[Category:燕京大學校友](../Category/燕京大學校友.md "wikilink")
[Category:葬于宋庆龄陵园](../Category/葬于宋庆龄陵园.md "wikilink")
[D](../Category/孙姓.md "wikilink")