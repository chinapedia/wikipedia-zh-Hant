**孝基**（529年四月—五月）是[北魏北海王](../Page/北魏.md "wikilink")[元顥的年号](../Page/元顥.md "wikilink")，共计兩個月。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|孝基||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||529年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[己酉](../Page/己酉.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [大通](../Page/大通_\(萧衍\).md "wikilink")（527年三月—529年九月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [永安](../Page/永安_\(北魏孝莊帝\).md "wikilink")（528年九月—530年十月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝莊帝元子攸年号](../Page/北魏孝莊帝.md "wikilink")
      - [神嘉](../Page/神嘉.md "wikilink")（525年十二月—535年三月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升年号](../Page/劉蠡升.md "wikilink")
      - [天統](../Page/天统_\(邢杲\).md "wikilink")（528年六月—529年四月）：[北魏時期領導](../Page/北魏.md "wikilink")[邢杲年号](../Page/邢杲.md "wikilink")
      - [神獸](../Page/神獸.md "wikilink")（528年七月—530年四月）：[北魏時期領導](../Page/北魏.md "wikilink")[万俟丑奴年号](../Page/万俟丑奴.md "wikilink")
      - [甘露](../Page/甘露_\(麴光\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴光年号](../Page/麴光.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:520年代中国政治](../Category/520年代中国政治.md "wikilink")
[Category:529年](../Category/529年.md "wikilink")