**竇皇后**（前205-前135年），[中国](../Page/中国.md "wikilink")[西汉時期皇族女性](../Page/西汉.md "wikilink")，為[汉文帝](../Page/汉文帝.md "wikilink")[皇后](../Page/皇后.md "wikilink")、[漢景帝生母](../Page/漢景帝.md "wikilink")，出身[清河郡](../Page/清河郡.md "wikilink")[观津](../Page/观津.md "wikilink")（今[河北省](../Page/河北省.md "wikilink")[衡水市](../Page/衡水市.md "wikilink")[武邑縣審坡鎮](../Page/武邑縣.md "wikilink")）。儘管《[史记](../Page/史记.md "wikilink")》和《[汉书](../Page/汉书.md "wikilink")》均未提及她的名字，但東漢趙歧在《三輔決錄》有提到其名為“猗”\[1\]\[2\]。另外，唐代[司马贞撰写的](../Page/司马贞.md "wikilink")《[史记索隐](../Page/史记索隐.md "wikilink")》中，提到[西晋](../Page/西晋.md "wikilink")[皇甫谧称窦皇后的名字为](../Page/皇甫谧.md "wikilink")“猗房”\[3\]。竇氏掌握政權橫跨景帝、武帝兩代，主張[黃老之術](../Page/黃老之術.md "wikilink")，成為西漢初年[道家](../Page/道家.md "wikilink")「[無為而治](../Page/無為而治.md "wikilink")」的忠實執行者及維護者。雖然竇氏不如[呂雉](../Page/呂雉.md "wikilink")、[衛子夫般為人熟悉](../Page/衛子夫.md "wikilink")，但對西漢的政治影響深遠。

## 早年

窦氏生年不详。吕雉為[皇太后時](../Page/皇太后.md "wikilink")（前195年—前180年），窦氏以[家人子的身份被选入宫服侍呂雉](../Page/家人子.md "wikilink")。之後，吕雉将一批宫女送出宫，赐予[诸侯王](../Page/诸侯王.md "wikilink")，每王五名，窦氏就在其中。窦姬因家乡在清河郡，于是請求主管此事的[宦官將她分去赵国](../Page/宦官.md "wikilink")。宦官答應，卻粗心大意，把竇氏排入前往[代国的队伍中](../Page/代_\(西汉\).md "wikilink")，竇氏不得已来到代国，卻得到了代王[刘恒的寵幸](../Page/汉文帝.md "wikilink")。窦氏先生一女[刘嫖](../Page/刘嫖.md "wikilink")，后生[刘启](../Page/汉景帝.md "wikilink")、[刘武等二子](../Page/劉武_\(梁王\).md "wikilink")。

代王尚未入朝为帝时，[代王王后早已亡故](../Page/代王王后.md "wikilink")。[陳平](../Page/陈平_\(西汉\).md "wikilink")、[周勃等](../Page/周勃.md "wikilink")[元老](../Page/元老.md "wikilink")[誅滅諸呂](../Page/誅滅諸呂.md "wikilink")，迎立代王劉恆為漢文帝後，代王王后所生的四子也相繼逝世。[前元元年](../Page/前元#汉文帝.md "wikilink")（前179年），刘启以庶长子身分被冊立為[皇太子](../Page/皇太子.md "wikilink")。同年三月，群臣请立皇后。文帝之母[薄太后說](../Page/薄姬.md "wikilink")：“诸侯皆同姓，立太子母为皇后。”窦氏因而被冊立為皇后，女儿刘嫖封为馆陶公主。次年，幼子刘武被立为[代王](../Page/代王.md "wikilink")，不久改迁[梁国](../Page/梁国.md "wikilink")，改封为[梁王](../Page/梁王.md "wikilink")。

## 家庭背景

窦氏的父母早亡，安葬在[观津](../Page/观津.md "wikilink")（現河北省重點文物保護單位：[竇氏青山墓](../Page/竇氏青山.md "wikilink")）。薄太后下诏追封窦皇后的父亲为安成侯，母亲为安成夫人。并且在清河设置二百户的园邑，由长丞侍奉看守，一切按薄太后父親灵文侯之墓——靈文園的做法。

窦皇后有兄弟二人，兄長为[窦长君](../Page/窦长君.md "wikilink")，弟弟为[窦少君](../Page/窦少君.md "wikilink")，在竇氏四五岁时，因家境贫困，兄弟二人皆被掳走並[贩卖到外地](../Page/人口贩卖.md "wikilink")，成為[奴工](../Page/奴工.md "wikilink")，渺无音讯。後又被转卖了十几户人家，最後到了宜阳（今[河南](../Page/河南.md "wikilink")[宜阳](../Page/宜阳.md "wikilink")）進山挖[石炭](../Page/石炭.md "wikilink")。一天黄昏，山崖边有一百多人在睡觉，突然[山崩](../Page/山崩.md "wikilink")，睡在崖边的人都压死了，只有[竇少君脱险逃生](../Page/竇少君.md "wikilink")。不久，窦少君跟随主人到了[京師](../Page/京師.md "wikilink")，听说新皇后姓窦，[原籍在](../Page/原籍.md "wikilink")[观津](../Page/观津.md "wikilink")，窦少君認為這就是他的姊姊。竇少君离家的时候虽然年纪幼小，却记得自己的[籍贯和姓氏](../Page/籍贯.md "wikilink")，还隐约记得与姊姊一起採[桑叶並从树上摔下来的情景](../Page/桑叶.md "wikilink")。他把这些事详细地写下来後，託人转交给窦皇后。窦皇后见到了这一封信後大驚，即刻將窦少君召来，并详细问了其他一些情况，姊弟就此相認。竇皇后还要窦少君回忆一些过去的情景，少君回忆道：「姊姊离我西去，我记得在[驿站分别时](../Page/驿站.md "wikilink")，讨来米汤给我洗头，临走时又给我吃了饭才走的。」窦皇后泣不成声，緊握著窦少君的手。

窦皇后重赏两个兄弟，把他们安置在[長安城居住](../Page/長安城.md "wikilink")。后来又为他俩请了有德行的长者对他们进行教育。由于这样，窦长君、窦少君兄弟俩后来成为谦让有礼的君子，不因地位显贵而盛气凌人。

竇氏成為皇太后時，[漢景帝封其弟](../Page/漢景帝.md "wikilink")[窦少君为章武侯](../Page/窦少君.md "wikilink")，其兄窦长君早逝，封他的兒子[窦彭祖为南皮侯](../Page/窦彭祖.md "wikilink")；因平定[七國之亂其堂侄](../Page/七國之亂.md "wikilink")[窦婴被封为魏其侯](../Page/窦婴.md "wikilink")。

## 皇后生涯

窦皇后因病而双目失明。同时，文帝有新宠[慎夫人和](../Page/慎夫人.md "wikilink")[尹姬](../Page/尹姬.md "wikilink")。其中慎夫人甚至可以与窦皇后、汉文帝同席而坐，直到[袁盎以](../Page/袁盎.md "wikilink")「當年[戚夫人得寵後被呂太后所虐殺](../Page/戚夫人.md "wikilink")」為由勸阻，才讓慎夫人坐於下方\[4\]。但由于慎夫人與尹姬都没有儿子，对窦皇后和刘启的地位无法构成威胁。

## 皇太后时期

汉文帝去世后，太子刘启即位，是为景帝。窦皇后被尊为[皇太后](../Page/皇太后.md "wikilink")。窦太后尊崇[道家](../Page/道家.md "wikilink")，因此汉景帝与窦氏宗族都必须学习[黄老学说](../Page/黄老.md "wikilink")。由於竇太后的影响，在汉景帝在位期间与[汉武帝早期](../Page/汉武帝.md "wikilink")，[儒家学者都無法得到重用](../Page/儒家.md "wikilink")。

窦太后命景帝和窦姓[外戚宗族得读](../Page/外戚.md "wikilink")[老子的](../Page/老子.md "wikilink")《[道德經](../Page/道德經.md "wikilink")》，并推尊其学说，因此她在世时“故诸博士具官待问，未有进者”（《史记·儒林传》）。

景帝时，她曾召[博士](../Page/博士.md "wikilink")[辕固生问他](../Page/辕固生.md "wikilink")《[道德經](../Page/道德經.md "wikilink")》是怎样的一部书，辕固生不识时务，猝然答道：“这不过是部平常人家读的书。”窦太后大怒道：“難道一定要讀『司空城旦书』（[刑法之類的书](../Page/刑法.md "wikilink")）吗？”（讥讽[儒教嚴苛](../Page/儒教_\(宗教\).md "wikilink")，類似於[司空之類的](../Page/司空.md "wikilink")[法官](../Page/法官.md "wikilink")，[城旦之類的](../Page/城旦.md "wikilink")[刑罰](../Page/刑罰.md "wikilink")。）太后憤怒，命辕固生到猪圈裏殺猪。[汉景帝见辕固生为一文弱书生](../Page/汉景帝.md "wikilink")，恐不敌猪，於是給他一把利刃，才让辕固生把猪刺死。

景帝在位十六年，始终未用[儒生](../Page/儒生.md "wikilink")。通常认为[文景之治的主角是汉文帝与汉景帝](../Page/文景之治.md "wikilink")。实际上，竇太后在[文景之治时代所起的作用也是举足轻重的](../Page/文景之治.md "wikilink")。在汉景帝统治时期，汉景帝曾经想启用[儒家学者](../Page/儒家.md "wikilink")，但都被其阻止。

竇太后极其溺爱自己的小儿子[刘武](../Page/劉武_\(梁王\).md "wikilink")。不但赏赐无数，而且还希望他能在景帝去世后继位。前154年，汉景帝曾在宴会上对梁王刘武表示，愿在自己死后传位与他，但遭到竇婴等朝臣的反对。尽管后来窦太后对景帝多次施加压力，但终未成功，刘武更是在景帝生前即去世。

## 太皇太后時期

前141年，[漢景帝](../Page/漢景帝.md "wikilink")[劉啟](../Page/劉啟.md "wikilink")[駕崩](../Page/駕崩.md "wikilink")，[太子](../Page/太子.md "wikilink")[劉徹即位](../Page/劉徹.md "wikilink")，是為[漢武帝](../Page/漢武帝.md "wikilink")，奉生母[皇后王氏為](../Page/王皇后_\(汉景帝\).md "wikilink")[皇太后](../Page/皇太后.md "wikilink")，尊[祖母竇太后為](../Page/祖母.md "wikilink")[太皇太后](../Page/太皇太后.md "wikilink")。

竇太皇太后闻武帝好[儒學](../Page/儒學.md "wikilink")，非常不以為然，常出面干预朝政。武帝也不便违忤祖母，所有朝廷政事，都随时向她请示。当时[御史大夫](../Page/御史大夫.md "wikilink")[赵绾和](../Page/赵绾.md "wikilink")[郎中令](../Page/郎中令.md "wikilink")[王臧](../Page/王臧.md "wikilink")，迎[鲁國耆儒](../Page/鲁國.md "wikilink")[申公来朝](../Page/申公.md "wikilink")，并建议仿古制，设[明堂辟雍](../Page/明堂.md "wikilink")，改曆易服，行[巡狩](../Page/巡狩.md "wikilink")[封禅等礼仪](../Page/封禅.md "wikilink")，还建议政事“不必事事请命太皇太后”。太皇太后听罢，怒不可遏，命武帝下令革去赵绾、王臧官职。至她去世前，武帝都不重用[儒生](../Page/儒生.md "wikilink")，可见她在政治上的影响。

[建元六年](../Page/建元_\(西漢\).md "wikilink")（前135年），竇太皇[太后去世](../Page/太后.md "wikilink")，遗[诏將](../Page/诏.md "wikilink")[東宮](../Page/東宮.md "wikilink")（[長樂宮](../Page/長樂宮.md "wikilink")）的金钱财物全部赐给女儿[刘嫖](../Page/刘嫖.md "wikilink")。她与[汉文帝合葬在](../Page/汉文帝.md "wikilink")[灞陵](../Page/灞陵_\(漢\).md "wikilink")。

## 历史评价

窦氏干预朝政，推崇[道家](../Page/道家.md "wikilink")，自她之后，没有一位[中国的统治者能像她一样真正的以](../Page/中国.md "wikilink")「[黄老思想](../Page/黄老.md "wikilink")」来「[无为而治](../Page/无为而治.md "wikilink")」。有人说其扮演了一个逆历史潮流而动的角色。但[文景之治政局稳定海晏河清](../Page/文景之治.md "wikilink")，宽政待民，窦氏经历[漢文帝](../Page/漢文帝.md "wikilink")、[漢景帝二朝](../Page/漢景帝.md "wikilink")，史称“[文景之治](../Page/文景之治.md "wikilink")”的盛世，与推行[黄老之术的宽民政策有很大关系](../Page/黄老.md "wikilink")。后[漢武](../Page/汉武帝.md "wikilink")[罢黜百家](../Page/罢黜百家.md "wikilink")，[独尊儒术](../Page/独尊儒术.md "wikilink")，虽布威名于四方，却将[汉朝折腾得千疮百孔筋疲力尽](../Page/汉朝.md "wikilink")。由此可见窦氏之功过不可一概而论。

## 陵墓被盗事件

2001年10月，窦氏的陵墓被盗，大量陪葬[陶俑被盗墓贼出售](../Page/陶俑.md "wikilink")，其中6件被偷运出境，并且于2002年3月20日在[美國](../Page/美國.md "wikilink")[纽约](../Page/纽约.md "wikilink")[蘇富比拍卖](../Page/蘇富比.md "wikilink")。[中国国家文物局及](../Page/中华人民共和国国家文物局.md "wikilink")[外交部闻讯后与美方进行了大量交涉](../Page/中华人民共和国外交部.md "wikilink")，蘇富比才撤銷拍賣，并且于2003年6月将这6件陶俑归还中国。

## 家族

## 影视作品

|                                                |                                          |
| ---------------------------------------------- | ---------------------------------------- |
| **影視作品**                                       | **飾演竇皇后的演員**                             |
| 《[衛子夫](../Page/衛子夫_\(電視劇\).md "wikilink")》     | [陳莎莉飾演](../Page/陳莎莉.md "wikilink") 竇太皇太后 |
| 《[大汉天子](../Page/大汉天子.md "wikilink")》           | [陳莎莉飾演](../Page/陳莎莉.md "wikilink") 竇太后   |
| 《[汉武大帝](../Page/汉武大帝.md "wikilink")》           | [歸亞蕾飾演](../Page/歸亞蕾.md "wikilink") 竇漪房   |
| 《[美人心计](../Page/美人心计.md "wikilink")》           | [林心如飾演](../Page/林心如.md "wikilink") 窦漪房   |
| 《[大風歌](../Page/大风歌_\(电视剧\).md "wikilink")》     | [羅憶楠飾演](../Page/羅憶楠.md "wikilink")       |
| 《[真命天子](../Page/真命天子_\(香港电视劇\).md "wikilink")》 | [周海媚](../Page/周海媚.md "wikilink") 飾演 竇青蓮  |

## 参考资料

  - 《[史记](../Page/史记.md "wikilink")·卷四十九·外戚世家第十九》
  - 《[汉书](../Page/汉书.md "wikilink")·卷九十七上·外戚传第六十七上》
  - 《[史记索隐](../Page/史记索隐.md "wikilink")》

## 注释

<references />

[D](../Category/西漢皇后.md "wikilink") [D](../Category/西漢皇太后.md "wikilink")
[D](../Category/西漢太皇太后.md "wikilink")
[D](../Category/中国盲人.md "wikilink")
[\~](../Category/竇姓.md "wikilink")
[Category:諡孝文](../Category/諡孝文.md "wikilink")

1.
2.  《[後漢書](../Page/後漢書.md "wikilink")·卷六十四·吳延史盧趙列傳·趙歧》：「（趙）歧多所述作，著《孟子章句》、《三輔決錄》傳於時。」

3.  史記索隱：「......[皇甫谧](../Page/皇甫谧.md "wikilink")-{云}-名猗房......」[司马贞注释《史记
    外戚世家第十九》](http://www.guoxue.com/shibu/24shi/shiji/sj_049.htm)

4.  《[史记](../Page/史记.md "wikilink") 卷一百一
    袁盎晁错列传第四十一》上幸上林，皇后、慎夫人从。其在禁中，常同席坐。及坐，郎署长布席，袁盎引卻慎夫人坐。慎夫人怒，不肯坐。上亦怒，起，入禁中。盎因前说曰：“臣闻尊卑有序则上下和。今陛下既已立-{后}-，慎夫人乃妾，妾主岂可与同坐哉！適所以失尊卑矣。且陛下幸之，即厚赐之。陛下所以为慎夫人，適所以祸之。陛下独不见‘人彘’乎？”於是上乃说，召语慎夫人。慎夫人赐盎金五十斤。