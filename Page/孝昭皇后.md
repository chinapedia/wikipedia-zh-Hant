**孝昭皇后**（），上官氏，名不详，[汉昭帝皇后](../Page/汉昭帝.md "wikilink")。[上官安與霍氏](../Page/上官安.md "wikilink")（追封「敬夫人」）之女，外祖父是權傾朝野的大將軍[霍光](../Page/霍光.md "wikilink")。

## 生平

祖父[上官桀透過](../Page/上官桀.md "wikilink")[鄂邑蓋長公主的影響力使年幼的孫女成为](../Page/鄂邑蓋長公主.md "wikilink")[漢昭帝劉弗陵的](../Page/漢昭帝.md "wikilink")[婕妤](../Page/婕妤.md "wikilink")，一个多月后又立为皇后，以此來鞏固外戚的地位。當時她年僅6歲（一說5歲\[1\]），而丈夫也僅僅12歲。兩年後[上官桀父子因為謀反被殺](../Page/上官桀.md "wikilink")，但上官皇后由於年幼，又身為霍光外孫女的身分得免牽連，依舊保持-{后}-位。[霍光利用自己的權勢鞏固外孫女的地位](../Page/霍光.md "wikilink")，令她長成後一度獲得專寵，但漢昭帝天不假年，21歲便撒手人寰，未和她生育子女。

[劉賀繼位](../Page/劉賀.md "wikilink")，上官皇后以15歲之齡升為[皇太后](../Page/皇太后.md "wikilink")。劉賀在位27日後，因過度無能而被朝臣請命，由上官太后下旨廢位。之後[漢宣帝繼位](../Page/漢宣帝.md "wikilink")，因宣帝虽非昭帝子孙，但已过继为昭帝嗣子，故上官太后仍为皇太后。[元帝即位](../Page/汉元帝.md "wikilink")，上官太后又升為[太皇太后](../Page/太皇太后.md "wikilink")，成為中國史上最年輕的皇太后和太皇太后，也是中国第一位曾祖母级太皇太后。

上官氏於漢元帝[建昭二年過世](../Page/建昭.md "wikilink")，享年52歲，與漢昭帝合葬[平陵](../Page/漢平陵.md "wikilink")。她居國母之位四十餘年，一生中有大半時間都在守寡。

## 參考文獻

  - 《漢書·卷九十七上·外戚傳第六十七上》

## 文內注釋

[category:西漢皇太后](../Page/category:西漢皇太后.md "wikilink")

[Category:西漢皇后](../Category/西漢皇后.md "wikilink")
[Category:西漢太皇太后](../Category/西漢太皇太后.md "wikilink")
[\~](../Category/上官姓.md "wikilink")
[Category:諡孝昭](../Category/諡孝昭.md "wikilink")

1.  辛德勇. 2012-2-20. 汉宣帝地节改元事发微:
    其出生时间也应该定在昭帝登基的后元二年年底...《汉书·外戚传》中上官氏六岁立为皇后的记载，似乎略有差误。