**孟买管辖区**（、、、、）是过去[英属印度的一个省份](../Page/英属印度.md "wikilink")，它成立于17世纪，起初作为[英国东印度公司的贸易据点](../Page/英国东印度公司.md "wikilink")，后来发展到包括印度西部和中部的大部分，以及一部分巴基斯坦和阿拉伯半岛。在它范围最大的时候，孟买管辖区包括今天印度的
[古吉拉特邦](../Page/古吉拉特邦.md "wikilink")、[马哈拉施特拉邦西部的三分之二](../Page/马哈拉施特拉邦.md "wikilink")，
和[卡纳塔克邦西北部](../Page/卡纳塔克邦.md "wikilink")；还包括今天
[巴基斯坦的](../Page/巴基斯坦.md "wikilink")[信德省以及](../Page/信德省.md "wikilink")[也门](../Page/也门.md "wikilink")[亚丁的英国领地](../Page/亚丁.md "wikilink")。孟买管辖区的一部分直接由英国人统治，还有一部分是由臣服英国总督的当地王公统治的[土邦](../Page/土邦.md "wikilink")。

## 早期历史

[英国在孟买管辖区的第一个居留地建于](../Page/英国.md "wikilink")1618年，当时东印度公司在[苏拉特建立了一个商行](../Page/苏拉特.md "wikilink")，并获得了[莫卧儿帝国皇帝](../Page/莫卧儿帝国.md "wikilink")[Jahangir的保护](../Page/Jahangir.md "wikilink")。1626年，[荷兰](../Page/荷兰.md "wikilink")
和英国做出了一次成功的努力，从[葡萄牙手中获取了](../Page/葡萄牙.md "wikilink")

## 领地扩张

在1803年，孟买管辖区只包括[撒尔塞特岛](../Page/撒尔塞特岛.md "wikilink")（自1774年起）、苏拉特和

## 地理

[Bombay_Prov_north_1909.jpg](https://zh.wikipedia.org/wiki/File:Bombay_Prov_north_1909.jpg "fig:Bombay_Prov_north_1909.jpg")
[Bombay_Prov_south_1909.jpg](https://zh.wikipedia.org/wiki/File:Bombay_Prov_south_1909.jpg "fig:Bombay_Prov_south_1909.jpg")
孟买管辖区的北部与[俾路支](../Page/俾路支.md "wikilink")、[旁遮普和](../Page/旁遮普.md "wikilink")[拉杰普塔纳相邻](../Page/拉杰普塔纳.md "wikilink")；东部是[中央省的](../Page/中央省.md "wikilink")[印多尔和](../Page/印多尔.md "wikilink")[海得拉巴](../Page/海得拉巴.md "wikilink")；南面是[马德拉斯管辖区和](../Page/马德拉斯管辖区.md "wikilink")[迈索尔王国](../Page/迈索尔王国.md "wikilink")；西面到
[阿拉伯海](../Page/阿拉伯海.md "wikilink")。在以上边界之内有一个葡萄牙殖民地[果阿](../Page/果阿.md "wikilink")、[达曼和第乌](../Page/达曼和第乌.md "wikilink")，和直接与印度政府发生关系的[巴罗达土邦](../Page/巴罗达土邦.md "wikilink")。在政治上孟买管辖区还包括位于今天[也门的亚丁领地](../Page/也门.md "wikilink")。其总面积，包括信德尔不包括亚丁，是188,745平方英里，其中122,984平方英里由英国直接统治，而65,761平方英里为土邦。1901年总人口达到25,468,209人，其中有18,515,587人住在英国管辖区
territory，6,908,648人住在土邦。

## 行政

## 人民

孟买管辖区的人口众多，构成也极其复杂。1901年的人口统计表明，其人口总数为25,468,209，宗教构成为：[印度教](../Page/印度教.md "wikilink")19,916,438人，[穆斯林](../Page/穆斯林.md "wikilink")4,567,295人，[耆那教](../Page/耆那教.md "wikilink")535,950人，[祆教](../Page/祆教.md "wikilink")78,552人，[基督徒大约](../Page/基督徒.md "wikilink")200,000人。

在信德，自从在8世纪被阿拉伯人征服以后，伊斯兰教就在当地宗教中占支配地位。在古吉拉特的支配宗教是印度教，虽然伊斯兰教王国在省内许多地方留下了影响。

在孟买管辖区内，信德的主要语言是[信德语](../Page/信德语.md "wikilink")，
古吉拉特是[古吉拉特语和](../Page/古吉拉特语.md "wikilink")[印度斯坦语](../Page/印度斯坦语.md "wikilink")，塔那和中央区是马拉地语，
在南部是马拉地语和[埃纳德语](../Page/埃纳德语.md "wikilink")。

## 农业

[智利胡椒](../Page/智利胡椒.md "wikilink")、[马铃薯](../Page/马铃薯.md "wikilink")、[姜黄和](../Page/姜黄.md "wikilink")[烟草](../Page/烟草.md "wikilink")。

## 工业

孟买管辖区的主要产业是棉纺织业。19世纪末蒸汽机在孟买、 普遍使用。1905年，在孟买管辖区有432座工厂，大部分都属于棉花

## 交通

## 军事

## 教育

[孟买大学成立于](../Page/孟买大学.md "wikilink")1857年

## 20世纪的改革

1919年，英属印度开始了[蒙太古-切姆斯福德改革](../Page/蒙太古-切姆斯福德改革.md "wikilink")，在1921年制定了法律，扩充了议会，将更多选举出来的印度本地权威人士充实进去，并提出[二头政治的原则](../Page/二头政治.md "wikilink")，将某些职权，如农业、健康、教育和地方政府，从中央政府移交给省。1935年
将孟买管辖区变为正是省份，信德则单独建省 。

## 独立以后

1947年，孟买省成为新独立的印度的一部分，而信德省成为[巴基斯坦的一部分](../Page/巴基斯坦.md "wikilink")。1950年孟买省改称[孟买邦](../Page/孟买邦.md "wikilink")，从前附属于孟买管辖区的土邦也并入新成立的邦。

## 参考

  -
[Category:英属印度诸省](../Category/英属印度诸省.md "wikilink")