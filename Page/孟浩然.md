[Meng_Haoran.jpg](https://zh.wikipedia.org/wiki/File:Meng_Haoran.jpg "fig:Meng_Haoran.jpg")

**孟浩然**（689年－740年\[1\]），名**浩**，[字](../Page/表字.md "wikilink")**浩然**，號**鹿門處士**，[以字行](../Page/以字行.md "wikilink")，[唐代襄州襄陽](../Page/唐代.md "wikilink")（今[湖北](../Page/湖北.md "wikilink")[襄阳](../Page/襄阳.md "wikilink")）人，又称「孟襄阳」，盛唐時期著名[诗人](../Page/诗人.md "wikilink")，屬於山水田園（田園山水）派。孟浩然年輕時曾遊歷四方，故後人稱他孟鹿門、鹿門處士。

## 生平

[唐玄宗在位時](../Page/唐玄宗.md "wikilink")，开元十五年（727年）冬，孟浩然赴[京尋求仕官之途](../Page/長安.md "wikilink")，次年應考[進士未成](../Page/進士.md "wikilink")。王維曾經向玄宗推薦孟浩然，孟浩然的一句詩“不才明主弃”，讓玄宗不滿，失去了在朝廷任官的機會。

开元十七年（729年）孟浩然漫游吴越。开元二十年（732年）回到襄阳。

开元二十二年（734年）再度入长安。[韓朝宗十分欣賞孟浩然](../Page/韓朝宗.md "wikilink")，於是邀請他參加飲宴，並且向朝廷推薦他，孟浩然因為與朋友喝酒而錯過了與韓朝宗的約定。

开元二十三年（735年）浩然游蜀。

开元二十五年（737年），[张九龄为荆州长史](../Page/张九龄.md "wikilink")，招致幕府。不久，仍返故居。开元二十八年（740年），[王昌齡遊襄陽](../Page/王昌齡.md "wikilink")，访孟浩然，相见甚欢。适浩然病疹发背，本來將要痊癒，因為縱情飲酒，食鲜疾发逝世\[2\]。

新、旧《唐书》中也收錄孟浩然的傳記。

著名詩集為《孟浩然集》

## 著作

孟浩然的诗歌绝大部分为五言律詩，题材大多關於山水田园和隐逸、旅行等内容。他與[王維](../Page/王维.md "wikilink")、[李白](../Page/李白.md "wikilink")、[張九齡交好](../Page/張九齡.md "wikilink")，继[陶渊明](../Page/陶渊明.md "wikilink")、[谢灵运](../Page/谢灵运.md "wikilink")、[谢朓之后](../Page/谢朓.md "wikilink")，开盛唐[山水詩之先声](../Page/山水詩.md "wikilink")。知名詩作有《[秋登萬山寄張五](../Page/秋登萬山寄張五.md "wikilink")》、《[過故人莊](../Page/過故人莊.md "wikilink")》、《[春曉](../Page/春曉.md "wikilink")》等篇。

孟浩然的诗与[王维齐名](../Page/王维.md "wikilink")，並稱「王孟」。现通行的《孟浩然集》收诗263首，但其中有他人作品。

## 评价

[李白对孟浩然极为崇拜](../Page/李白.md "wikilink")，曾作数诗赠年长自己十二岁的孟浩然，最著名的一首是《赠孟浩然》：“吾爱孟夫子，风流天下闻。红颜弃轩冕，白首卧松云。醉月频中圣，迷花不事君。高山安可仰，徒此揖清芬。”

## 墓葬

[孟浩然墓在湖北襄阳城东风林南麓](../Page/孟浩然墓.md "wikilink")。

## 注釋

朋友

## 外部链接

  - 《[孟浩然年谱](http://web.it.nctu.edu.tw/~lccpan/newpage7.htm)》
  - 《[古诗库：孟浩然全集](http://www.shigeku.org/xlib/lingshidao/gushi/menghaoran.htm)》
  - 下定雅弘：〈[孟浩然对陶渊明的敬慕：从前期到后期的变化](http://www.nssd.org/articles/article_read.aspx?id=42148369)〉。

[category:襄阳人](../Page/category:襄阳人.md "wikilink")
[category:唐朝诗人](../Page/category:唐朝诗人.md "wikilink")

[Category:孟子後裔](../Category/孟子後裔.md "wikilink")
[Category:孟姓](../Category/孟姓.md "wikilink")
[Category:689年出生](../Category/689年出生.md "wikilink")
[Category:740年逝世](../Category/740年逝世.md "wikilink")

1.  [http://db1.ihp.sinica.edu.tw/cbdbc/cbdbkm?@17^960248073^107](http://db1.ihp.sinica.edu.tw/cbdbc/cbdbkm?@17%5E960248073%5E107)^^^1^1@@2021438441
2.  王士源（元）《孟浩然詩集序》記載：“開元二十八年（740年）王昌齡遊襄陽，時浩然疾發背，且愈，得相歡飲。浩然宴謔，食鮮疾動，終於南園。”