**後蜀高祖孟知祥**（\[1\]）字**保胤**，邢州龙冈（今河北[邢台市西南](../Page/邢台市.md "wikilink")）人。[后唐太祖](../Page/后唐.md "wikilink")[李克用婿](../Page/李克用.md "wikilink")。他是[五代十国时期](../Page/五代十国.md "wikilink")[后蜀開國皇帝](../Page/后蜀.md "wikilink")（934年在位），在位不到1年，享壽61岁。

## 生平

后唐建立后，以孟知祥为太原尹充西京副留守。

后唐灭前蜀后，任孟知祥为西川[节度使](../Page/节度使.md "wikilink")。孟知祥不久即谋求独立，先和[东川节度使](../Page/东川节度使.md "wikilink")[董璋合作](../Page/董璋.md "wikilink")，击退后唐来讨伐的军队。932年，又与董璋决裂，董璋来攻打孟知祥，反被消灭。933年，孟知祥被后唐封为蜀王。934年正月，他不受[后唐闵帝官爵](../Page/后唐闵帝.md "wikilink")，在[成都即皇帝位](../Page/成都.md "wikilink")，建国号“大蜀”，史称“后蜀”，改元“[明德](../Page/明德_\(后蜀\).md "wikilink")”。

### 逝世

孟知祥只做了7个月皇帝就病重了，臨終時，由第三子（按孟知祥夫妇墓志铭，实为第五子，孟知祥前两个儿子疑因早夭故未序齿）[孟昶监国](../Page/孟昶.md "wikilink")。孟知祥死后，秘不发丧，[王处回与](../Page/王处回.md "wikilink")[赵季良立孟昶后才发丧](../Page/赵季良.md "wikilink")。孟知祥[谥号为](../Page/谥号.md "wikilink")**文武圣德英烈明孝皇帝**，[庙号](../Page/庙号.md "wikilink")**高祖**。其墓称[和陵](../Page/孟知祥墓.md "wikilink")\[2\]，位于[四川省](../Page/四川省.md "wikilink")[成都市北郊青龙乡石岭村的磨盘山南](../Page/成都市.md "wikilink")。陵墓以青石砌筑，建22级阶梯通向墓室。墓呈圆锥形，主室高8.16米，直径6.7米，是在南方少有的具[五代](../Page/五代.md "wikilink")[后蜀的北方草原建筑风格的陵墓](../Page/后蜀.md "wikilink")。现已发掘\[3\]。

## 家庭

### 妻妾

1.  [李皇后](../Page/李皇后_\(後蜀高祖\).md "wikilink")（873-932）：[李克用长女](../Page/李克用.md "wikilink")，亦即[后唐莊宗李存勗胞姐](../Page/李存勗.md "wikilink")，[同光三年十一月封琼华长公主](../Page/同光.md "wikilink")。[天成三年](../Page/天成_\(后唐\).md "wikilink")，[後唐明宗改封琼华公主为福庆长公主](../Page/後唐明宗.md "wikilink")。[長興三年正月去世](../Page/长兴_\(后唐\).md "wikilink")，四年，追冊為晉國雍順長公主。
2.  [李贵妃](../Page/李太后_\(後蜀高祖\).md "wikilink")（？-965）：[后唐莊宗李存勗登基前的妾室](../Page/李存勗.md "wikilink")，[孟昶生母](../Page/孟昶.md "wikilink")，后为太后。

### 妹

1.  孟氏：嫁[李克用弟弟](../Page/李克用.md "wikilink")[李克寧](../Page/李克寧_\(五代\).md "wikilink")，生[李存瓌](../Page/李存瓌.md "wikilink")。后李克宁图谋取代李存勖事败被杀，孟氏被遣回孟知祥处

### 子女

#### 子

1.  [孟貽範](../Page/孟貽範.md "wikilink")：福庆长公主所生，于长兴三年（932年）前早夭
2.  [孟貽邕](../Page/孟貽邕.md "wikilink")：福庆长公主所生，于长兴三年（932年）前早夭
3.  [孟貽矩](../Page/孟貽矩.md "wikilink")：摄彭州刺吏、银青光禄大夫、检校尚书、左仆射、兼御史大夫、上柱国
4.  燕王
    [孟贻邺](../Page/孟贻邺.md "wikilink")：左右牢城都指挥使、金紫光禄大夫、检校尚书右仆射、兼御史大夫、上柱国
5.  後主 [孟仁贊](../Page/孟昶.md "wikilink")
6.  夔恭孝王[孟仁毅](../Page/孟仁毅.md "wikilink")（？-955）
7.  雅王 [孟仁贄](../Page/孟仁贄.md "wikilink")（928-971，字忠美）
8.  彭王 [孟仁裕](../Page/孟仁裕.md "wikilink")（927-970，字鳴謙）
9.  嘉王 [孟仁操](../Page/孟仁操.md "wikilink")（？-986）

#### 女

1.  [崇华公主](../Page/崇华公主.md "wikilink")
    孟久柱：丈夫[伊延瓌](../Page/伊延瓌.md "wikilink")，生子[伊審徵](../Page/伊審徵.md "wikilink")。
2.  玉清公主
    [孟延意](../Page/孟延意.md "wikilink")：丈夫或為[董璋之子](../Page/董璋.md "wikilink")[董光嗣](../Page/董光嗣.md "wikilink")。
3.  金仙公主：嫁匡国奉圣叶力功臣清河[张虔钊长子](../Page/张虔钊.md "wikilink")[张匡弼](../Page/张匡弼.md "wikilink")。
4.  兰英公主：嫁[孙汉韶长子](../Page/孙汉韶.md "wikilink")[孙晏琮](../Page/孙晏琮.md "wikilink")。

#### 孫

1.  [孟德崇](../Page/孟德崇.md "wikilink")：[孟貽鄴之子](../Page/孟貽鄴.md "wikilink")\[4\]。
2.  [孟玄喆](../Page/孟玄喆.md "wikilink")（938-992）：[孟昶長男](../Page/孟昶.md "wikilink")，初封[秦王](../Page/秦王.md "wikilink")，後為太子。
3.  [孟玄珏](../Page/孟玄珏.md "wikilink")（？-992）：孟昶次男，封[褒王](../Page/褒王.md "wikilink")。
4.  [孟玄寶](../Page/孟玄寶.md "wikilink")（944-950：孟昶三男，封[遂王](../Page/遂王.md "wikilink")。

## 注释

## 参考文献

### 引用

### 来源

  - 《[新五代史](../Page/新五代史.md "wikilink") 卷六十四 后蜀世家第四》

{{-}}

[1](../Category/后蜀皇帝.md "wikilink") [M](../Category/后唐官员.md "wikilink")
[M](../Category/開國君主.md "wikilink") [M](../Category/邢台人.md "wikilink")
[Z](../Category/孟姓.md "wikilink")
[Category:葬于成都](../Category/葬于成都.md "wikilink")

1.  生卒日期都来自《[蜀梼杌校笺](../Page/蜀梼杌校笺.md "wikilink")》。
2.  Mote, F.W. (1999). Imperial China (900-1800). Harvard University
    Press. pp. 11–15. ISBN 0-674-01212-7
3.  《中国历代帝王陵》
4.  [耿煥](../Page/耿煥.md "wikilink")《[野人閒話](../Page/野人閒話.md "wikilink")》