**孟語**（[孟文](../Page/孟文.md "wikilink")：，）是[南亞語系主要由生活在](../Page/南亞語系.md "wikilink")[緬甸和](../Page/緬甸.md "wikilink")[泰國的](../Page/泰國.md "wikilink")[孟族人講的語言](../Page/孟族.md "wikilink")。它和[高棉語很像](../Page/高棉語.md "wikilink")，都不是[聲調語言](../Page/聲調語言.md "wikilink")，這和大多數[中南半島的語言不同](../Page/中南半島.md "wikilink")。\[1\]

使用孟文的人數在不斷減少，年輕一代尤甚。\[2\]

在緬甸，大多數使用孟文的人都生活在[孟邦](../Page/孟邦.md "wikilink")，其次是[德林達依和](../Page/德林達依.md "wikilink")[克倫邦](../Page/克倫邦.md "wikilink")。\[3\]

## 参考文献

[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:緬甸語言](../Category/緬甸語言.md "wikilink")
[Category:泰國語言](../Category/泰國語言.md "wikilink")

1.

2.
3.