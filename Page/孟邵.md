**孟邵**（），[字](../Page/表字.md "wikilink")**少逸**，[號](../Page/號.md "wikilink")**鷺洲**，[晚號](../Page/晚號.md "wikilink")**蝶叟**，[四川](../Page/四川.md "wikilink")[中江人](../Page/中江.md "wikilink")，[清朝政治人物](../Page/清朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

[乾隆二十五年](../Page/乾隆.md "wikilink")（1760年）登庚辰科[進士](../Page/進士.md "wikilink")，選[翰林院庶吉士](../Page/翰林院庶吉士.md "wikilink")，[散館改](../Page/散館.md "wikilink")[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")，升[員外郎](../Page/員外郎.md "wikilink")。[乾隆三十二年](../Page/乾隆.md "wikilink")（1767年）任[山東道](../Page/山東.md "wikilink")[監察御史](../Page/監察御史.md "wikilink")。[乾隆四十二年](../Page/乾隆.md "wikilink")（1777年）以兼任[福建道](../Page/福建.md "wikilink")[監察御史之差至](../Page/監察御史.md "wikilink")[臺灣擔任](../Page/臺灣.md "wikilink")[巡視臺灣監察御史](../Page/巡視臺灣監察御史.md "wikilink")\[1\]。[乾隆四十四年](../Page/乾隆.md "wikilink")（1779年）升[禮科](../Page/禮科.md "wikilink")[給事中](../Page/給事中.md "wikilink")。歷官[鴻臚寺](../Page/鴻臚寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")、[鴻臚寺卿](../Page/鴻臚寺卿.md "wikilink")、[光祿寺卿](../Page/光祿寺卿.md "wikilink")、[太常寺卿](../Page/太常寺卿.md "wikilink")、[宗人府](../Page/宗人府.md "wikilink")[府丞](../Page/府丞.md "wikilink")、[左副都御史](../Page/左副都御史.md "wikilink")、[大理寺卿](../Page/大理寺卿.md "wikilink")。[嘉庆九年](../Page/嘉庆.md "wikilink")（1804年）回鄉，主[成都草堂书院](../Page/成都.md "wikilink")，講席十余年。著有《蝶叟集》。

## 註釋

## 參考文獻

  - 紀昀，《閱微草堂筆記》
  - 國立故宮博物院圖書文獻處清國史館傳稿，70100768號
  - 朱汝珍，《詞林輯略·卷四》， 清代傳記叢刊， 學林類（18）， 明文書局發行。
  - 劉寧顏編，《重修臺灣省通志》，臺北市，臺灣省文獻委員會，1994年。
  - 管锡庆，《中江进士孟鹭洲巡治台湾》，德阳日报，2004-11-09

{{-}}

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝刑部主事](../Category/清朝刑部主事.md "wikilink")
[Category:清朝刑部員外郎](../Category/清朝刑部員外郎.md "wikilink")
[Category:清朝山東道監察御史](../Category/清朝山東道監察御史.md "wikilink")
[Category:清朝福建道監察御史](../Category/清朝福建道監察御史.md "wikilink")
[Category:巡視台灣監察御史](../Category/巡視台灣監察御史.md "wikilink")
[Category:清朝禮科給事中](../Category/清朝禮科給事中.md "wikilink")
[Category:清朝鴻臚寺少卿](../Category/清朝鴻臚寺少卿.md "wikilink")
[Category:清朝鴻臚寺卿](../Category/清朝鴻臚寺卿.md "wikilink")
[Category:清朝光祿寺卿](../Category/清朝光祿寺卿.md "wikilink")
[Category:清朝太常寺卿](../Category/清朝太常寺卿.md "wikilink")
[Category:清朝宗人府丞](../Category/清朝宗人府丞.md "wikilink")
[Category:清朝左副都御史](../Category/清朝左副都御史.md "wikilink")
[Category:清朝大理寺卿](../Category/清朝大理寺卿.md "wikilink")
[Category:中江人](../Category/中江人.md "wikilink")
[S](../Category/孟姓.md "wikilink")

1.  [清](../Page/清.md "wikilink")·[紀昀](../Page/紀昀.md "wikilink")，《[閱微草堂筆記](../Page/閱微草堂筆記.md "wikilink")》（卷19）：“孟鹭洲自记巡视台湾事曰：乾隆丁酉，偶与友人扶乩，乩赠余以诗曰：乘槎万里渡沧溟，风雨鱼龙会百灵，海气粘天迷岛屿，潮声簸地走雷霆，鲸波不阻三神岛，鲛室争看二使星，记取白云飘渺处，有人同望蜀山青。时将有巡视台湾之役，余疑当往数日，果命下，六月启行，八月至厦门渡海，驻半载始归。归时风利，一昼夜即登岸，去时飘荡十七日，险阻异常。初出厦门，即雷雨交作，云雾晦冥，信帆而往，莫知所适。忽腥风触鼻，舟人曰：黑水洋也，其水比海水凹下数十丈，阔数十里，长不知其所极，黝然而深，视如泼墨。舟中摇手戒勿语，云其下即龙宫为第一险处，度此可无虞矣。至白水洋，遇巨鱼鼓鬣而来，举其首如危峰障日，每一拨刺，浪涌如山，声砰訇如霹雳。移数刻始过。尽计其长，当数百里。舟人云来迎天使，理或然欤？既而飓风四起，舟几覆没，忽有小鸟数十环绕樯竿，舟人喜跃，称天后来拯。风果顿止，遂得泊澎湖。圣人在上，百神效职，不诬也。遐思所历，一一与诗语相符，非鬼神能前知欤？时先大夫尚在堂，闻余有过海之役，命兄到赤嵌来视余，遂同登望海楼，并末二句亦巧合，益信数皆前定，非人力所能为矣。戊午秋，扈从滦阳，与晓岚宗伯话及，宗伯方草滦阳续录，因书其大略付之，或亦足资谈柄耶？以上皆鹭洲自序，考唐钟辂作定命录，大旨在戒人躁竞，毋涉妄求，此乩仙预告未来，其语皆验，可使人知无关祸福之惊恐，与无心聚散之踪迹，皆非偶然。亦足消趋避之机械矣。”