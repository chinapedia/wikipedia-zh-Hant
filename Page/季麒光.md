**季麒光**，[字](../Page/表字.md "wikilink")**昭聖**，[號](../Page/號.md "wikilink")**蓉洲**，[江南](../Page/江南省.md "wikilink")[無錫縣人](../Page/無錫.md "wikilink")，[清朝官員](../Page/清朝.md "wikilink")。

## 生平

[順治十七年](../Page/順治.md "wikilink")（1660年）中[舉人](../Page/舉人.md "wikilink")，[康熙十五年](../Page/康熙.md "wikilink")（1676年）丙辰科[進士](../Page/進士.md "wikilink")，榜姓[鄭](../Page/鄭姓.md "wikilink")，一說姓[趙](../Page/趙姓.md "wikilink")，其後復姓[季](../Page/季姓.md "wikilink")。由[內閣中書出知](../Page/內閣中書.md "wikilink")[梅縣](../Page/梅縣.md "wikilink")。康熙二十三年（1684年）由[閩清縣遷](../Page/閩清縣.md "wikilink")[臺灣府](../Page/臺灣府.md "wikilink")[諸羅縣](../Page/諸羅縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")，於該年十一月初八（12月13日）到任。康熙二十四年（1685年）因[丁憂去職](../Page/丁憂.md "wikilink")。

## 著作

著作有：

  - 《[臺灣郡志稿](../Page/臺灣郡志稿.md "wikilink")》六卷
  - 《蓉洲文稿》四卷
  - 《蓉洲詩稿》七卷
  - 《[臺灣雜記](../Page/臺灣雜記.md "wikilink")》一卷
  - 《山川考略》一卷
  - 《海外集》一卷
  - 《華陽懷古》一卷
  - 《三國史論》一卷

## 貢獻

他於地方首興官學，頗有政績。另外，季麒光亦將施政經過一一記載，稱《蓉州文稿》。他還與[沈光文等十三人發起](../Page/沈光文.md "wikilink")[詩社](../Page/詩社.md "wikilink")「[東吟社](../Page/東吟社.md "wikilink")」，致力於傳統舊文學的播種，培養了許多[詩人](../Page/詩人.md "wikilink")。\[1\]

## 參考文獻

### 註釋

### 一般參考

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。
  - [葉石濤](../Page/葉石濤.md "wikilink")《[台灣文學史綱](../Page/台灣文學史綱.md "wikilink")》
  - [全臺詩作者個人資料](http://cls.hs.yzu.edu.tw/TWPAPP/ShowAuthorInfo.aspx?AID=000063)

{{-}}

[Category:順治十七年庚子科舉人](../Category/順治十七年庚子科舉人.md "wikilink")
[Category:清朝內閣中書](../Category/清朝內閣中書.md "wikilink")
[Category:清朝諸羅縣知縣](../Category/清朝諸羅縣知縣.md "wikilink")
[Category:無錫人](../Category/無錫人.md "wikilink")
[Q](../Category/季姓.md "wikilink")

1.  [葉石濤](../Page/葉石濤.md "wikilink")《[台灣文學史綱](../Page/台灣文學史綱.md "wikilink")》