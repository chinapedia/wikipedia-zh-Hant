[Admiral_Courbet_in_Hue.jpg](https://zh.wikipedia.org/wiki/File:Admiral_Courbet_in_Hue.jpg "fig:Admiral_Courbet_in_Hue.jpg")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:20150630_孤拔衣冠塚（澎湖馬公）.jpg "fig:缩略图")
**阿納托爾-阿梅代-龐斯柏·库尔贝**（，），中国史料称**孤拔**，越南史料称**姑陂**，是一名[法國的](../Page/法國.md "wikilink")[海軍](../Page/海軍.md "wikilink")[將領](../Page/將領.md "wikilink")。他曾任法國[殖民地](../Page/殖民地.md "wikilink")[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")[行政長官](../Page/行政長官.md "wikilink")；後率領遠征軍出兵[越南](../Page/越南.md "wikilink")[阮朝](../Page/阮朝.md "wikilink")、促成[法屬印度支那殖民地的建立](../Page/法屬印度支那.md "wikilink")；又出任[遠東艦隊司令](../Page/遠東艦隊_\(法國\).md "wikilink")，於[中法戰爭期間擊敗](../Page/中法戰爭.md "wikilink")[清帝國](../Page/清朝.md "wikilink")[南洋水師](../Page/南洋水師.md "wikilink")、一度攻佔[台灣](../Page/台灣.md "wikilink")[基隆](../Page/基隆市.md "wikilink")，並封鎖[台灣海峽及中國東南沿岸](../Page/台灣海峽.md "wikilink")，最後攻佔並將所部由台灣方面轉移至[澎湖](../Page/澎湖.md "wikilink")。1885年6月，在《[中法新約](../Page/中法新約.md "wikilink")》簽訂的两天後，病逝於澎湖媽宮（現[澎湖縣](../Page/澎湖縣.md "wikilink")[馬公市](../Page/馬公市.md "wikilink")）。

## 生平

孤拔出生於[阿布維爾區](../Page/阿布維爾區.md "wikilink")，父親為富裕的紅酒批發商，但於1836年孤拔九歲時去世。孤拔畢業於[巴黎理工大學](../Page/巴黎理工大學.md "wikilink")，1849年進入法國海軍，擔任見習士官。1880年官拜海軍少將，前往新喀里多尼亞擔任總督至1882年。

## 軍事生涯

1883年擔任印度支那舰队司令，後來入侵[越南](../Page/越南.md "wikilink")，率領艦隊前往[順化](../Page/順化.md "wikilink")，逼迫阮朝皇帝[阮福昇簽訂](../Page/阮福昇.md "wikilink")《[順化條約](../Page/第一次順化條約.md "wikilink")》，使越南成為法國的保護國，他因功升為海軍中將。1884年任远东舰队司令，中法戰爭爆發后，孤拔率領[中國海艦隊前往](../Page/中國海艦隊.md "wikilink")[中國東南沿海](../Page/中國.md "wikilink")，以威脅清軍海防。

### 馬江海戰

1884年8月22日，孤拔接獲指示，攻擊[南洋水師與](../Page/南洋水師.md "wikilink")[福州一帶的海防設施](../Page/福州.md "wikilink")。8月23日下午孤拔在[伏爾達號上下達攻擊令](../Page/伏爾達號.md "wikilink")，[馬江海戰正式爆發](../Page/馬江海戰.md "wikilink")。由於清軍戰艦無論就裝備與訓練皆遠不及法軍，加上孤拔戰術高明，開戰後一個小時法軍已殲滅[馬尾港內的多數船艦](../Page/馬尾.md "wikilink")，並在當天傍晚之前消滅清軍所有的海軍戰力。隨後三日，孤拔指揮法軍擊毀附近的所有砲臺和軍營，直到8月26日為止。

這場戰役中，法軍損失極小。

### 進攻台灣、鎮海和澎湖

1884年8月29日，中國海艦隊與[李士卑斯的](../Page/李士卑斯.md "wikilink")[東京灣艦隊完成合併](../Page/東京灣艦隊.md "wikilink")，組成[遠東艦隊](../Page/遠東艦隊_\(法國\).md "wikilink")，由孤拔率領，並被下令攻打基隆，計畫取得當地的煤礦作為船隻燃料，並佔領台灣以威脅清國。9月2日孤拔乘[凡得號](../Page/凡得號.md "wikilink")（Triomphante）巡視基隆，然後於9月4日又搭船前往[馬祖](../Page/馬祖.md "wikilink")，並發電報告知法國政府，希望能轉而攻打[煙台](../Page/煙台.md "wikilink")、[威海衛等地](../Page/威海衛.md "wikilink")。但9月17日部長回絕其建議，只好繼續攻打基隆。9月30日孤拔集合11艘戰艦，命李士卑斯率領4艘戰艦攻打滬尾（今[新北市](../Page/新北市.md "wikilink")[淡水區](../Page/淡水區.md "wikilink")），自己率領7艘戰艦攻打基隆；在台灣督戰的清國大員[刘铭传放弃基隆](../Page/刘铭传.md "wikilink")、退守[臺北](../Page/臺北.md "wikilink")。法[海軍陸戰隊於](../Page/海軍陸戰隊.md "wikilink")10月2日佔領基隆，隨即又以水兵攻滬尾但被清軍击退；孤拔转而从10月23日起对台湾实行海上封锁，企圖切斷對台兵糧支援。

1885年初，法海军陆战队又从基隆向台北进攻而不果。法舰則截击由[上海往援](../Page/上海.md "wikilink")[福建的五艘清国军舰](../Page/福建.md "wikilink")，在[浙江石浦击沉其中两艘](../Page/浙江.md "wikilink")，並[追擊至镇海砲台與清軍對峙駁火](../Page/镇海之战_\(1885年\).md "wikilink")；據中方紀錄，此役孤拔的座舰遭招宝山炮台開火击中，孤拔本人則為斷裂的壓傷但可信度很低，法方记载只断两根桅索无一负伤。3月底，法軍攻佔[澎湖](../Page/澎湖縣.md "wikilink")；6月11日，孤拔因[霍亂死于](../Page/霍亂.md "wikilink")[澎湖](../Page/澎湖縣.md "wikilink")[馬公](../Page/馬公市.md "wikilink")。

## 影響

基隆大沙灣為法軍登陸之地，[日治時代改稱為](../Page/台灣日治時期.md "wikilink")「クールベー濱」（孤拔濱），1909年（[明治](../Page/明治.md "wikilink")42年）設立海水浴場。1942年因二戰爆發，日本政府為強化認同，以所在地[真砂町改名為](../Page/真砂町.md "wikilink")「-{眞砂ケ濱}-」\[1\]。

昔日在[上海和](../Page/上海.md "wikilink")[天津两地](../Page/天津.md "wikilink")，均有以其命名的古拔路。[上海法租界西部的古拔路](../Page/上海法租界.md "wikilink")，今为富民路；[天津法租界的古拔路](../Page/天津法租界.md "wikilink")，今为和平区松江路。

法國海軍曾有以此人命名的[孤拔號戰艦](../Page/孤拔號戰艦.md "wikilink")。

孤拔病逝，葬於澎湖的[孤拔墓](../Page/孤拔墓.md "wikilink")，而後法國政府將遺骨遷回，留有孤拔衣冠塚（當地人俗稱法國將軍墓），位於今日馬公市民生路之縣立羽毛球館旁。衣冠塚刻有法文碑文：「
† A LA MEMOIRE DE LAMIRAL COURBET ET DES BRAVES MORTS POUR LA FRANCE AUX
PESCADORES 1885」（悼念於1885年為法國死在澎湖島的孤拔和勇士們。）

## 參考文獻

  - 書目

<!-- end list -->

  - Duboc, E., *Trente cinq mois de campagne en Chine, au Tonkin*
    (Paris, 1899)
  - Garnot, *L'expédition française de Formose, 1884–1885* (Paris, 1894)
  - Loir, M., *L'escadre de l'amiral Courbet* (Paris, 1886)
  - Lonlay, D. de, *L'amiral Courbet et le « Bayard »: récits, souvenirs
    historiques* (Paris, 1886)
  - Lung Chang \[龍章\], *Yueh-nan yu Chung-fa chan-cheng* \[越南與中法戰爭,
    Vietnam and the Sino-French War\] (Taipei, 1993)
  - Thomazi, A., *Histoire militaire de l'Indochine française* (Hanoi,
    1931)
  - Thomazi, A., *La conquête de l'Indochine* (Paris, 1934)
  - [Biographie de l'Amiral
    Courbet](http://bstorg.free.fr/Courbet/biocourb.htm)  Jean-Vincent
    Brisset

<!-- end list -->

  - 引用

## 相關條目

  - [孤拔级战列舰](../Page/孤拔级战列舰.md "wikilink")
  - [西仔反](../Page/西仔反.md "wikilink")
  - [鎮海之戰](../Page/镇海之战_\(1885年\).md "wikilink")
  - [馬江海戰](../Page/馬江海戰.md "wikilink")
  - [澎湖之役 (1885年)](../Page/澎湖之役_\(1885年\).md "wikilink")

[C](../Category/法國海軍將領.md "wikilink")
[C](../Category/巴黎綜合理工學院校友.md "wikilink")
[C](../Category/中法战争人物.md "wikilink")

1.