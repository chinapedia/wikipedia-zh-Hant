[Sun_Qian_2016_Han_Zhao_Lie_Miao.jpg](https://zh.wikipedia.org/wiki/File:Sun_Qian_2016_Han_Zhao_Lie_Miao.jpg "fig:Sun_Qian_2016_Han_Zhao_Lie_Miao.jpg")孙乾像\]\]
**孫乾**，（），字**公祐**，[青州](../Page/青州刺史部.md "wikilink")[北海人](../Page/北海郡.md "wikilink")。[三國時期](../Page/三國.md "wikilink")[蜀漢官吏](../Page/蜀漢.md "wikilink")，劉備在徐州被[呂布打敗後](../Page/呂布.md "wikilink")，投靠曹操,打敗呂布後,背叛[曹操](../Page/曹操.md "wikilink"),孫乾也跟隨劉備先向北投靠[袁紹](../Page/袁紹.md "wikilink")。袁紹敗後,再向南投靠劉表作出貢獻.
與[簡雍](../Page/簡雍.md "wikilink")、[麋竺同為](../Page/麋竺.md "wikilink")[蜀漢最高待遇的老臣子](../Page/蜀漢.md "wikilink")。

## 生平

[劉備入主](../Page/劉備.md "wikilink")[徐州時](../Page/徐州.md "wikilink")，名士[鄭玄推舉孫乾給劉備](../Page/鄭玄.md "wikilink")，劉備便辟他為從事中郎。

199年（39岁），劉備遣回朱靈、路招,自己佔據下邳。200年（40岁），反曹事迹敗露，董承被殺。劉備便殺死徐州刺史[車冑](../Page/車冑.md "wikilink")，留關羽守下邳，自己回守小沛，另一方面派遣[孫乾與袁紹連合](../Page/孫乾.md "wikilink")，打出對抗曹操的名目。曹操曾派[劉岱](../Page/劉岱_\(曹操\).md "wikilink")、[王忠領軍攻打劉備](../Page/王忠.md "wikilink")，但反為劉備所敗([三國志](../Page/三國志.md "wikilink").武帝紀稱「不能勝」)。同時，[東海](../Page/東海.md "wikilink")[昌霸反叛](../Page/昌霸.md "wikilink")，郡縣多投靠劉備，劉備軍再次聚起數萬人，並連同多個地方勢力一起反曹。曹操決定親自東征劉備，雖然曹軍中將領多認為袁紹才是大敵，但曹操卻覺得劉備是英傑，必要先行討伐，[郭嘉亦贊同曹操](../Page/郭嘉.md "wikilink")。

最後劉備大敗，小沛被佔，曹操虜獲劉備妻子及生擒關羽、[夏侯博](../Page/夏侯博.md "wikilink")。劉備逃至[青州](../Page/青州.md "wikilink")，青州刺史[袁譚親自迎接](../Page/袁譚.md "wikilink")，並報知其父袁紹，袁紹出[鄴城](../Page/鄴城.md "wikilink")200里迎接。待了一個多月後，以前的部下又重新聚會。不久，曹操與袁绍於[官渡交戰](../Page/官渡.md "wikilink")，[汝南黃巾餘軍](../Page/汝南郡.md "wikilink")[劉辟等响應袁绍叛曹](../Page/劉辟.md "wikilink")，袁绍便派劉備率軍與劉辟會合。曹操派[曹仁攻打汝南](../Page/曹仁.md "wikilink")，劉備惟有再次還軍袁绍。當時劉備想離開袁绍，便說服袁绍應南連[劉表](../Page/劉表.md "wikilink")，袁绍再次派劉備到汝南與[龔都會合](../Page/龔都.md "wikilink")。曹操另派[蔡陽攻擊劉備](../Page/蔡陽.md "wikilink")，為劉備所殺。

201年，於[官渡之戰大敗袁绍的曹操南攻汝南](../Page/官渡之戰.md "wikilink")，劉備敗走棄城，並派麋竺、[孫乾與劉表會面](../Page/孫乾.md "wikilink")，劉表親自到郊外迎接劉備，待以上賓之禮，准他屯於[新野](../Page/新野.md "wikilink")。劉表表面雖禮待劉備，但内心對其有所顧忌。因此，当刘备向刘表提出趁曹操进攻[乌桓时偷袭许都的建议时](../Page/乌桓.md "wikilink")，刘表没有采纳。

建安八年（203年），曹操攻黎陽，大敗袁尚和袁譚，二人退守鄴。曹操追擊到鄴，並收割麥田，卻被袁尚所敗。此時曹軍諸將都希望乘勢消滅袁氏殘餘勢力，但曹操最終依從郭嘉之計，撤軍任由袁尚與袁譚自相殘殺。袁譚要求袁尚增送鎧甲及士兵，但遭拒絕。在郭圖、辛評挑撥下，袁譚攻袁尚，但戰敗，退回南皮。王修率兵救援袁譚，並勸導兄弟應和睦；荊州牧劉表亦曾寫信給[袁尚](../Page/袁尚.md "wikilink")，信中說自己每與劉備、孫乾談及袁尚兄弟自相殘殺的事，沒不感心痛悲傷，可見孫乾被劉表十分看重。勸他們齊心，努力經營現在所領有的地區，但二人都不接納。

214年，劉備入主[益州](../Page/益州.md "wikilink")，孫乾被拜為秉忠將軍，其待遇僅次於[麋竺](../Page/麋竺.md "wikilink")，與[簡雍一樣](../Page/簡雍.md "wikilink")。不久便病死。

## 評價

[三國志評曰](../Page/三國志.md "wikilink")：「麋竺、孫乾、簡雍、[伊籍](../Page/伊籍.md "wikilink")，皆雍容風議，見禮於世。」

## 參考資料

  - 《[三國志‧蜀書‧孫乾傳](http://www.guoxue.com/shibu/24shi/sangzz/sgzz_038.htm)》

[S孫](../Category/蜀漢政治人物.md "wikilink")
[S](../Category/濰坊人.md "wikilink")
[Q](../Category/孙姓.md "wikilink")