[Sun_Wu_1879－1939.jpg](https://zh.wikipedia.org/wiki/File:Sun_Wu_1879－1939.jpg "fig:Sun_Wu_1879－1939.jpg")
**孫武**（），原名**葆仁**，[字](../Page/字.md "wikilink")**尧卿**（又作**揺清**），号**夢飛**，[湖北省](../Page/湖北省.md "wikilink")[武昌府](../Page/武昌府.md "wikilink")[夏口厅](../Page/夏口厅.md "wikilink")（治今[漢口](../Page/漢口.md "wikilink")\[1\]）柏泉乡人。近代民主革命家\[2\]。[共進会领导人之一](../Page/共進会.md "wikilink")。\[3\]

## 生平

### 清末的活動

孫武自幼喜好武術。1897年（[光緒二十三年](../Page/光緒.md "wikilink")），入[張之洞創設的](../Page/張之洞.md "wikilink")[湖北武備学堂](../Page/湖北武備学堂.md "wikilink")。後来[吴禄贞等学友赴日本留学](../Page/吴禄贞.md "wikilink")，孫武的母亲则将其留下，不让其赴日本。湖北武備学堂毕业\[4\]。孫武入湖北省的新軍。\[5\]

1900年（光緒二十六年）參加[唐才常的](../Page/唐才常.md "wikilink")[自立軍](../Page/自立軍.md "wikilink")\[6\]，任岳州司令。[自立軍起义失敗](../Page/自立軍起义.md "wikilink")，孫武逃往[广州](../Page/广州.md "wikilink")。1903年，孫武秘密回到家乡为母亲奔丧。1904年，在[武昌加入](../Page/武昌.md "wikilink")[科学補習所](../Page/科学補習所.md "wikilink")，謀響應[華興會](../Page/華興會.md "wikilink")[长沙起义](../Page/长沙起义.md "wikilink")，事泄\[7\]，于1904年9月流亡日本。結果孫武再度兴起留学的念头，入[成城学校学习](../Page/成城学校.md "wikilink")。1905年（[明治三十八年](../Page/明治.md "wikilink")）11月2日，日本[文部省发出](../Page/文部省.md "wikilink")《[清国留学生取締規則](../Page/清国留学生取締規則.md "wikilink")》，孫武归国。\[8\]

归国後，孫武到[東三省同](../Page/東三省.md "wikilink")[吴禄贞从事革命活動](../Page/吴禄贞.md "wikilink")，在东三省进行偵察，并同[馬賊进行交涉](../Page/馬賊.md "wikilink")。他还加入[武昌](../Page/武昌.md "wikilink")[日知会](../Page/日知会.md "wikilink")，帮[刘静庵办](../Page/刘静庵.md "wikilink")[江汉公学](../Page/江汉公学.md "wikilink")。1908年夏，再赴日本，入[大森军事讲习所](../Page/大森军事讲习所.md "wikilink")，研究野外战术及新式炸弹。1908年8月，孫武、[焦達峰](../Page/焦達峰.md "wikilink")、[張伯祥等在東京](../Page/张百祥.md "wikilink")，參加組織[共進会](../Page/共進会.md "wikilink")\[9\]，孫武推為軍務部長。不久，被推为湖北主盟。1908年，孫武回到湖北省，继续暗中从事革命活动。1909年（[宣統元年](../Page/宣統.md "wikilink")）在武漢建立湖北共進會\[10\]。孫武将会党编为五镇，赴[梧州参加起义](../Page/梧州.md "wikilink")。起义失敗，孙武逃往[香港](../Page/香港.md "wikilink")\[11\]。随后经[馮自由介绍](../Page/馮自由.md "wikilink")\[12\]，次年在香港加入[中国同盟会](../Page/中国同盟会.md "wikilink")\[13\]。

### 武昌起義及其後

[Sun_Wu_Buddhist_prayer_beads.jpg](https://zh.wikipedia.org/wiki/File:Sun_Wu_Buddhist_prayer_beads.jpg "fig:Sun_Wu_Buddhist_prayer_beads.jpg")1911年9月，[共进会與](../Page/共进会.md "wikilink")[文学社聯合](../Page/文学社.md "wikilink")，準備起義，孙武被舉为参谋长\[14\]，推為主席\[15\]。孫武、[居正等革命党人决定](../Page/居正.md "wikilink")10月举行起义。但在起义前的10月9日，孫武在[汉口俄租界](../Page/汉口俄租界.md "wikilink")[寶善里试制炸弹](../Page/寶善里.md "wikilink")，不慎爆炸，孫武负重傷。由此起义计划泄露，清政府镇压革命派。10月10日清晨，清兵將革命份子[彭楚藩](../Page/彭楚藩.md "wikilink")、[劉復基](../Page/劉復基.md "wikilink")、[楊洪聖三人](../Page/楊洪聖.md "wikilink")[斬首](../Page/斬首.md "wikilink")，受到追缉的革命派发动[武昌起義](../Page/武昌起義.md "wikilink")。此时，孫武在革命同志及日本医生的帮助下藏匿，起义时傷未愈，故未参加起义。10月15日，[黎元洪任都督的](../Page/黎元洪.md "wikilink")[湖北軍政府成立](../Page/湖北軍政府.md "wikilink")\[16\]，孫武任[軍務部部長](../Page/中华民国军政府鄂军都督府组织结构与主要官员.md "wikilink")\[17\]。

1912年（[民国元年](../Page/中華民国历.md "wikilink")）元旦，南京的[中華民国臨時政府成立](../Page/南京临时政府.md "wikilink")。\[18\]1912年1月組織「民社」，擁護黎元洪\[19\]。2月，[武汉革命党人发动](../Page/武汉.md "wikilink")“倒孙”运动\[20\]。有文章称1912年初，孙武听闻[孙发绪讲述孙文如何吹牛](../Page/孙发绪.md "wikilink")，南京政府如何卖国后，孙武表示“南京政府如此败坏，我宁可承认袁世凯，不承认南京”\[21\]。旋又贊成袁世凱稱帝\[22\]。但根据孙武后人孙吉森所言，这种说法“很无稽”，孙吉森的说法是：“你看，1912年4月1日，孙中山辞去临时大总统一职后，4月9日去慰问武汉首义的同志。接连四天，都是孙武亲自陪同。”又其家人回忆，孙武曾经告诉家人：“我对南京政府有意见，不代表我对孙文有意见”。\[23\]。[袁世凯任大总统后](../Page/袁世凯.md "wikilink")，孫武任总统府高等顾问。\[24\]\[25\]1913年8月，孫武任[進步党](../Page/進步党_\(中国\).md "wikilink")（黎元洪任理事長）的理事。\[26\]1914年10月20日，获授[将军府義威将軍](../Page/北洋将军府.md "wikilink")。\[27\]\[28\]1915年12月，被任命为[參政院參政](../Page/參政院.md "wikilink")。\[29\]

1916年6月，[袁世凯病逝](../Page/袁世凯.md "wikilink")，黎元洪任[大总統](../Page/大总統.md "wikilink")。\[30\]後曾任漢口地畝清查督辦、湖北地畝清查督辦等職\[31\]。1922年，被[萧耀南任命为](../Page/萧耀南.md "wikilink")[汉口地亩清查督办](../Page/汉口.md "wikilink")。1925年，依靠[吴佩孚](../Page/吴佩孚.md "wikilink")，出任湖北官矿督办。\[32\]1926年夏，孙武出任湖北地亩清查督办。孙武曾变卖[地产支持](../Page/地产.md "wikilink")[國民黨北伐](../Page/國民黨北伐.md "wikilink")\[33\]。但[國民黨抵达武汉后](../Page/國民黨.md "wikilink")，卻撤销了湖北地亩清查督办公署，孙武失去职位。\[34\]\[35\]\[36\]晚年寄寓[北京](../Page/北京.md "wikilink")（1928年更名[北平](../Page/北平.md "wikilink")）[上海間](../Page/上海.md "wikilink")\[37\]。

1939年（民国二十八年）11月10日，孫武在[北平](../Page/北平.md "wikilink")[拈花寺病逝](../Page/拈花寺.md "wikilink")。享年61岁（满60歳）。\[38\]\[39\]

## 参考文献

[Category:共进会会员](../Category/共进会会员.md "wikilink")
[Category:日知会会员](../Category/日知会会员.md "wikilink")
[Category:中国同盟会会员](../Category/中国同盟会会员.md "wikilink")
[Category:鄂军将领](../Category/鄂军将领.md "wikilink")
[Category:北洋将军府冠字将军](../Category/北洋将军府冠字将军.md "wikilink")
[Category:参政院参政](../Category/参政院参政.md "wikilink")
[Category:武汉人](../Category/武汉人.md "wikilink")
[W武](../Category/孙姓.md "wikilink")

1.
2.

3.  萧栋梁「孫武」

4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.

22.
23. [外滩画报-辛亥后人今何在](http://www.bundpic.com/2011/10/16045.shtml)

24.
25. 何浩, 冯崇德，湖北历史人物辞典，湖北人民出版社，1984年，第258页

26.
27. 1914年10月21日第884号. 《政府公报》第四十二册.

28.
29. 刘寿林等编，民国职官年表，北京：中华书局，1995年

30.
31.
32.
33.
34. 贺覚非，辛亥武昌首义人物传 第1卷，中华书局，1982年，第170页

35. 中华文史资料文库: 军政人物编，中国文史出版社，1996年，第694页

36. [孙武，www.xhgm100.com，2010-09-17](http://www.xhgm100.com/zy/ls/201009/t1219562.htm)

37.
38.
39. 袁远，江岸史话，武汉出版社，2004年，第100页