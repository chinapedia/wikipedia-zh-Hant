**孫治平**（）為[中華民國國父](../Page/中華民國.md "wikilink")[孫中山之長孫](../Page/孫中山.md "wikilink")、前[行政院院長](../Page/行政院院長.md "wikilink")[孫科長子](../Page/孫科.md "wikilink")。[上海](../Page/上海.md "wikilink")[聖約翰大學畢業](../Page/圣约翰大学_\(上海\).md "wikilink")，[美國](../Page/美國.md "wikilink")[加州州立大學政治經濟學碩士](../Page/加州州立大學.md "wikilink")。

## 生平

1913年11月15日在美國[加州](../Page/加州.md "wikilink")[柏克萊市出生](../Page/柏克萊.md "wikilink")。

1955年取得[美國](../Page/美國.md "wikilink")[加州州立大學政治經濟學碩士](../Page/加州州立大學.md "wikilink")，與比自己小1歲的弟弟[孫治強長期旅居美國](../Page/孫治強.md "wikilink")。

1965年，孫治平陪同父親[孫科到台灣定居](../Page/孫科.md "wikilink")，曾擔任[中國國民黨中央委員](../Page/中國國民黨.md "wikilink")、[中國國民黨中央評議委員會委員](../Page/中國國民黨.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")[總統府](../Page/中華民國總統府.md "wikilink")[國策顧問](../Page/中華民國總統府國策顧問.md "wikilink")、[台灣電視公司副董事長等職](../Page/台灣電視公司.md "wikilink")。

1966年1月，在孫治平的安排下，其同父異母妹妹[孫穗芳得以在](../Page/孫穗芳.md "wikilink")[台北首次與父親孫科見面](../Page/台北.md "wikilink")\[1\]。

孫治平於1980年開始定居於[香港](../Page/香港.md "wikilink")\[2\]。

2001年11月，孫治平在兒子[孫國雄等親屬陪同下](../Page/孫國雄.md "wikilink")，在[中華人民共和國成立後首次踏足中國大陸地區](../Page/中華人民共和國.md "wikilink")，遊覽了[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[南京](../Page/南京.md "wikilink")、[廣州等地](../Page/廣州.md "wikilink")。於11月25日，到祖父[孫中山故鄉](../Page/孫中山.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[中山市](../Page/中山市.md "wikilink")[翠亨村探親祭祖](../Page/翠亨村.md "wikilink")，逗留三天\[3\]。

2005年4月6日，孫治平在[香港](../Page/香港.md "wikilink")[柴灣](../Page/柴灣.md "wikilink")[東區尤德夫人那打素醫院病逝](../Page/東區尤德夫人那打素醫院.md "wikilink")，享壽91歲\[4\]。4月14日，孫治平於[香港殯儀館舉殯](../Page/香港殯儀館.md "wikilink")，除了香港知名人士、[中山市與](../Page/中山市.md "wikilink")[翠亨村代表外](../Page/翠亨村.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[台灣的政界高層人士](../Page/台灣.md "wikilink")[連戰](../Page/連戰.md "wikilink")、[江丙坤](../Page/江丙坤.md "wikilink")、[馬英九](../Page/馬英九.md "wikilink")、[陳水扁](../Page/陳水扁.md "wikilink")，[中華民國外交部駐港機構](../Page/中華民國外交部.md "wikilink")[中華旅行社均有派代表出席](../Page/中華旅行社.md "wikilink")\[5\]。孫治平遺體最後葬在[美國](../Page/美國.md "wikilink")[夏威夷坟场](../Page/夏威夷.md "wikilink")。

## 家庭

<center>

</center>

孫治平生母是[孫科元配夫人](../Page/孫科.md "wikilink")[陳淑英](../Page/陳淑英.md "wikilink")，有一弟[孫治強](../Page/孫治強.md "wikilink")、二妹[孫穗英](../Page/孫穗英.md "wikilink")、[孫穗華](../Page/孫穗華.md "wikilink")，以及兩名同父異母妹妹[孫穗芳](../Page/孫穗芳.md "wikilink")、[孫穗芬](../Page/孫穗芬.md "wikilink")。

孫治平第一任妻子為[謝秀玲](../Page/謝秀玲.md "wikilink")（後離異），育有一名兒子[孫國雄](../Page/孫國雄.md "wikilink")，孫為[孫偉仁](../Page/孫偉仁.md "wikilink")。第二任妻子為[張佩霞](../Page/張佩霞.md "wikilink")。

## 参考資料

<div class="references-small">

<references />

</div>

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:孫中山家族](../Category/孫中山家族.md "wikilink")
[Category:上海聖約翰大學校友](../Category/上海聖約翰大學校友.md "wikilink")
[Category:华南师范大学附属中学校友](../Category/华南师范大学附属中学校友.md "wikilink")
[Category:廣東裔美國人](../Category/廣東裔美國人.md "wikilink") [Category:香山县人
(中山)](../Category/香山县人_\(中山\).md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")
[Zhi治平](../Category/孫姓.md "wikilink")
[人](../Category/台視.md "wikilink")

1.  [民國第一家庭悲歡](http://www.chinanews.com/tw/kong/news/2008/06-25/1292542.shtml)
2.  [孫文族裔
    遍及台日港澳美](http://n.yam.com/tlt/politics/201101/20110102653715.html)
3.  [孫中山故居紀念館——孫治平](http://www.sunyat-sen.org:1980/b5/www.sunyat-sen.org/sun/showqshy.php?id=24)
4.  [世界新聞報-孫中山長孫的珍藏記憶](http://gb.cri.cn/2201/2005/08/29/145@677816.htm)
5.  [孫中山孫子孫治平逝世](http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=at&Path=3493978932/19at.cfm)