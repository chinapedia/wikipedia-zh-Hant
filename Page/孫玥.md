**孫玥**（），[中國](../Page/中國.md "wikilink")[江蘇](../Page/江蘇.md "wikilink")[南京人](../Page/南京.md "wikilink")，綽號「小老虎」，為前[中國女排隊主攻手](../Page/中國女排.md "wikilink")，專打四號位置，身高186公分，體重72公斤，扣球高度可達314公分。

孫玥的父親與母親都是排球選手，她在1992年首次進入國家隊，由於她高挑的身材及強大的扣球威力，從此之後便成為國家隊不可或缺的主攻手，她在國家隊的期間，最慣用的背號為11號。

孫玥在國家隊服役的期間，正逢[古巴女排八連霸的朝代](../Page/古巴女排.md "wikilink")，所以她身披國家隊戰袍雖然長達10年，卻始終沒有拿過三大賽的冠軍頭銜。2001年，孫玥正式告別國家隊，前往[義大利的職業球隊發展](../Page/義大利.md "wikilink")，並連續三年協助球隊獲得聯賽亞軍。2004年孫玥返國，回到了老東家江蘇省女排隊，2005年10月22日她帶領了江蘇隊在十運會中奪下了第四名，從此之後正式退役。

2006年3月，脫下球衣的孫玥轉而擔任[揚子晚報體育記者的工作](../Page/揚子晚報.md "wikilink")。2007年3月31日，歷經六年的愛情長跑，孫玥決定在自己34歲生日這天，與男友何國文在南京完成終身大事。

孫玥是近年中國女排隊最有名的球員之一，除了她強力的攻擊與清秀的臉龐之外，最讓人印象深刻的莫過於她親切的態度與一貫的笑容。依據中國女排的官網描述，孫玥雖然作為隊中的核心隊員，但她身上卻沒有絲毫的明星架子。她總是能在球隊處於逆境之時，迅速地帶動全隊的氣氛與士氣，力挽狂瀾。

現時，孫玥與丈夫育有兩女，分別於2011年及2013年出生。

## 外部連結及相關影片

  - [崢嶸歲玥](http://www.sunyue11.com/)
  - [中國女排官方網頁](https://web.archive.org/web/20070901151457/http://cnwvt.server101.com/player.php?player=9211)
  - [大洋新聞—同心鎖見證浪漫愛情](https://web.archive.org/web/20080917132902/http://sports.dayoo.com/gb/content/2007-03/09/content_2773442.htm)
  - [膠東新聞—排壇美女出嫁](https://web.archive.org/web/20070403220015/http://www.jiaodong.net/2007/3/428208.htm)
  - [【奥运故事会】孙玥的爱情故事](http://space.tv.cctv.com/video/VIDE1210667180684633)
  - [【亲历奥运】小老虎孙玥：国家队前的日子](http://space.tv.cctv.com/video/VIDE1208273798285710)
  - [【亲历奥运】小老虎孙玥：伤感巴塞罗那](http://space.tv.cctv.com/video/VIDE1208273798278709)
  - [【亲历奥运】小老虎孙玥：辉煌亚特兰大](https://web.archive.org/web/20101109022826/http://space.tv.cctv.com/video/VIDE1208273798271706)
  - [【亲历奥运】小老虎孙玥：不完美的悉尼](http://space.tv.cctv.com/video/VIDE1208273798265704)
  - [【亲历奥运】小老虎孙玥：退役生活](http://space.tv.cctv.com/video/VIDE1208273798258703)

[Category:南京籍运动员](../Category/南京籍运动员.md "wikilink")
[Category:中国女子排球运动员](../Category/中国女子排球运动员.md "wikilink")
[Category:中国奥运排球运动员](../Category/中国奥运排球运动员.md "wikilink")
[Category:中華人民共和國記者](../Category/中華人民共和國記者.md "wikilink")
[Y玥](../Category/孙姓.md "wikilink")
[Category:取得香港特別行政區居留權人士](../Category/取得香港特別行政區居留權人士.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1992年夏季奥林匹克运动会排球运动员](../Category/1992年夏季奥林匹克运动会排球运动员.md "wikilink")
[Category:1996年夏季奥林匹克运动会排球运动员](../Category/1996年夏季奥林匹克运动会排球运动员.md "wikilink")
[Category:2000年夏季奥林匹克运动会排球运动员](../Category/2000年夏季奥林匹克运动会排球运动员.md "wikilink")
[Category:奧林匹克運動會排球獎牌得主](../Category/奧林匹克運動會排球獎牌得主.md "wikilink")