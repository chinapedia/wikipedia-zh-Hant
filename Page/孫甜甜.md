**孫甜甜**，[籍貫](../Page/籍貫.md "wikilink")[河南](../Page/河南.md "wikilink")[郑州](../Page/郑州.md "wikilink")，前[中國女子網球運動員](../Page/中國.md "wikilink")。迄今為止，唯一在大滿貫賽事（[澳網混雙](../Page/澳網.md "wikilink")，2008），[夏季奧運會](../Page/夏季奧運會.md "wikilink")（雅典女雙，2004）以及WTA職業巡迴賽（塔什幹女單，2006）均獲得過冠軍的[華人球員](../Page/華人.md "wikilink")。2008年後淡出網球圈並退役。

## 生涯

孫甜甜比[李婷遲一年練習網球](../Page/李婷_\(網球運動員\).md "wikilink")，她於1989年開始於河南省業餘體校練習網球。四年後成為河南隊的球員之一，後來在2000年入選國家隊。

孫甜甜在加入國家隊入的第三年與李婷贏得WTA三站的女雙[冠軍](../Page/冠軍.md "wikilink")。一年後在WTA[海德拉巴站中的女雙項目得亞軍](../Page/海德拉巴.md "wikilink")，又在[澳洲網球公開賽中在女雙](../Page/澳洲網球公開賽.md "wikilink")16強出局，但表現不俗。其後，更在[雅典奧運會歷史性地殺入決賽](../Page/2004年夏季奧林匹克運動會.md "wikilink")，更8月22日的決賽中，她們以直落兩盤6-3，擊敗[西班牙組合马丁内斯](../Page/西班牙.md "wikilink")（1994年[温布尔登女單冠軍](../Page/溫網.md "wikilink")）和Virginia
Ruano Pascual，為中國首次奪得奧運會網球女雙金牌，這是中國隊歷來在該項目最出眾的成績。

2005年，孫甜甜與李婷奪得2005中國十佳勞倫斯最佳突破獎。

2006年，孫甜甜奪得[烏茲別克](../Page/烏茲別克.md "wikilink")-{[塔什干](../Page/塔什干.md "wikilink")}-女網公開賽女單冠軍，她在女單決賽以6-2，6-4擊敗東道主球員Iroda
Tulyaganova奪標，成為繼[李娜](../Page/李娜_\(網球運動員\).md "wikilink")、[鄭潔和](../Page/鄭潔.md "wikilink")[晏紫](../Page/晏紫.md "wikilink")-{zh-hans:后;zh-hk:後;zh-tw:後;}-，第四位獲得WTA巡迴賽單打冠軍的中國球員。

2008年，孫甜甜伙拍[塞爾維亞球員](../Page/塞爾維亞.md "wikilink")[泽蒙季奇](../Page/内纳德·齐莫尼奇.md "wikilink")，在[澳網混合雙打決賽](../Page/澳網.md "wikilink")，以盤數2-0（比分
: 7-6(4),
6-4）擊敗[印度組合](../Page/印度.md "wikilink")[米尔扎和](../Page/米尔扎.md "wikilink")[布帕蒂](../Page/布帕蒂.md "wikilink")，勇奪冠軍。

她亦是首位在奧運網球和大滿貫比賽中都曾獲冠軍的中國網球運動員。

## WTA巡迴賽冠軍

  - 單打 (1)
      - 2006年 (1) - 塔什-{干}-四級賽（塔什-{干}-公開賽）

<!-- end list -->

  - 雙打 (12)
      - 2003年 (3) - 維也納三級賽、魁北克城三級賽、芭堤雅四級賽
        \[搭檔皆為[李婷](../Page/李婷_\(網球運動員\).md "wikilink")\]
      - 2004年 (2) - [雅典奧運會](../Page/2004年夏季奧林匹克運動會.md "wikilink")、廣州公開賽
        \[搭檔皆為**李婷**\]
      - 2005年 (1) - [葡萄牙埃斯托利爾四級賽](../Page/葡萄牙.md "wikilink") \[搭檔 :
        **李婷**\]
      - 2006年 (3) - 芭提雅四級賽、埃斯托利爾四級賽、廣州公開賽 \[搭檔皆為**李婷**\]
      - 2007年 (2) -
        AIG日本公開賽、曼谷（泰國公開賽）\[搭檔皆為[晏紫](../Page/晏紫.md "wikilink")\]
      - 2008年 (1) - [印度班加羅爾](../Page/印度.md "wikilink")
        \[搭檔為[彭帥](../Page/彭帥.md "wikilink")\]

## [大滿貫混雙冠軍](../Page/網球大滿貫.md "wikilink")

  - 2008年 (1) - **澳網** \[搭檔 : [泽蒙季奇](../Page/内纳德·齐莫尼奇.md "wikilink")\]

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>比賽</p></td>
<td><p>場地</p></td>
<td><p>搭檔</p></td>
<td><p>決賽對手</p></td>
<td><p>勝方比分</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><strong>澳網</strong></p></td>
<td><p>硬地</p></td>
<td><p><a href="../Page/内纳德·齐莫尼奇.md" title="wikilink">泽蒙季奇</a> (<a href="../Page/塞爾維亞.md" title="wikilink">塞爾維亞</a>)</p></td>
<td><p>米尔扎 (<a href="../Page/印度.md" title="wikilink">印度</a>) 和<br />
布帕蒂 (<a href="../Page/印度.md" title="wikilink">印度</a>)</p></td>
<td><p>7-6(4), 6-4</p></td>
</tr>
</tbody>
</table>

## 國內比賽

  - 單打季軍
      - 2005年 - [十運會](../Page/十運會.md "wikilink")

## 外部連結

  -
  -
[Category:郑州籍运动员](../Category/郑州籍运动员.md "wikilink")
[Category:中国女子网球运动员](../Category/中国女子网球运动员.md "wikilink")
[Category:中国奥运网球运动员](../Category/中国奥运网球运动员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:2006年亞洲運動會網球運動員](../Category/2006年亞洲運動會網球運動員.md "wikilink")
[Tiantian](../Category/孫姓.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:亞洲運動會網球獎牌得主](../Category/亞洲運動會網球獎牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會網球運動員](../Category/2004年夏季奧林匹克運動會網球運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會網球運動員](../Category/2008年夏季奧林匹克運動會網球運動員.md "wikilink")
[Category:2006年亚洲运动会铜牌得主](../Category/2006年亚洲运动会铜牌得主.md "wikilink")
[Category:2002年亞洲運動會網球運動員](../Category/2002年亞洲運動會網球運動員.md "wikilink")
[Category:奥林匹克运动会网球奖牌得主](../Category/奥林匹克运动会网球奖牌得主.md "wikilink")
[Category:中共十九大代表](../Category/中共十九大代表.md "wikilink")