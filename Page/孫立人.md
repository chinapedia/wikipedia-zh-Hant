**孫立人**，[CBE](../Page/CBE.md "wikilink")（），[字](../Page/表字.md "wikilink")**撫民**，[號](../Page/號.md "wikilink")**仲能**。[安徽](../Page/安徽.md "wikilink")[廬江人](../Page/廬江.md "wikilink")，[二次大戰](../Page/二次大戰.md "wikilink")[緬甸戰場重要將領](../Page/緬甸戰場.md "wikilink")。[清華大學](../Page/清華大學.md "wikilink")[庚子賠款留美預科](../Page/庚子賠款.md "wikilink")1923屆，[普渡大學土木工程學士](../Page/普渡大學.md "wikilink")，[維吉尼亞軍校](../Page/維吉尼亞軍校.md "wikilink")[博雅教育學士](../Page/博雅教育.md "wikilink")，抗日戰爭時期少數留美、不是[黃埔軍校出身的軍官](../Page/黃埔軍校.md "wikilink")。留美的[財政部長](../Page/中華民國財政部.md "wikilink")[宋子文賞識留美軍人](../Page/宋子文.md "wikilink")，孫立人從財政部[稅警總團特科兵團團長起家](../Page/稅警總團.md "wikilink")，1942年任師長指揮[新三十八師](../Page/国民革命军陆军新编第三十八师.md "wikilink")（後編入[新一军](../Page/国民革命军陆军新编第一军.md "wikilink")）在[緬甸](../Page/英屬緬甸.md "wikilink")[仁安羌之战](../Page/仁安羌之战.md "wikilink")，以寡敵眾擊退日軍，救出7000名英軍及500名西方記者和傳教士，贏得國際聲譽。同年拒絕與上級[杜聿明穿過熱帶叢林](../Page/杜聿明.md "wikilink")[野人山撤回](../Page/野人山.md "wikilink")[雲南](../Page/雲南省_\(中華民國\).md "wikilink")，改為帶隊撤往[英屬印度](../Page/英屬印度.md "wikilink")，是他一生最重要軍事決定，最終杜聿明領兵逾半出不了山，大多死於[瘴氣](../Page/瘴氣.md "wikilink")。英國授[大英帝國司令勳章](../Page/大英帝國勳章.md "wikilink")（CBE），是極少數外國軍官得主。亦是四名獲授美國[功勳勳章的中國軍官](../Page/功勋勋章_\(消歧义\).md "wikilink")（與[蔣中正](../Page/蔣中正.md "wikilink")、[戴安瀾和](../Page/戴安瀾.md "wikilink")[謝莽](../Page/謝莽.md "wikilink")）。

[國共內戰前期在東北戰場與共軍](../Page/第二次國共內戰.md "wikilink")[林彪相持](../Page/林彪.md "wikilink")，參與[第二次四平战役](../Page/第二次四平战役.md "wikilink")、[臨江戰役等](../Page/臨江戰役.md "wikilink")，但與上級[杜聿明將帥不和](../Page/杜聿明.md "wikilink")，1947年4月被調閒職，沒參與第二階段大戰。1947年11月被調台灣。\[1\]1955年遭蔣中正指控[兵變案](../Page/孫立人兵變案.md "wikilink")，軟禁33年。

## 生平

### 家譜與祖籍疑團

孫立人，[龍舒孫氏第八代](../Page/舒城县.md "wikilink")。始祖孫正仁本為[徽州府](../Page/徽州府.md "wikilink")[休寧縣人](../Page/休寧縣.md "wikilink")，[貢生](../Page/貢生.md "wikilink")，[雍正年間遷](../Page/雍正.md "wikilink")[舒城縣東](../Page/舒城縣.md "wikilink")[三河鎮南岸經商](../Page/三河镇_\(肥西县\).md "wikilink")，自此書香門第，八代之內出了三個[進士](../Page/進士.md "wikilink")。\[2\]孫立人天祖的弟弟[孫序賢](../Page/孫序賢.md "wikilink")[嘉慶二十五年進士](../Page/:Template:嘉慶二十五年庚辰科殿試金榜.md "wikilink")（1820年），[刑部雲南司員外郎](../Page/刑部.md "wikilink")，其子[孫觀在](../Page/孫觀.md "wikilink")[道光二十七年進士](../Page/:Template:道光二十七年丁未科殿試金榜.md "wikilink")（1847年），任[直隸布政使](../Page/直隶.md "wikilink")。自[孫觀起](../Page/孫觀.md "wikilink")，孫家與[淮軍關係密切](../Page/淮軍.md "wikilink")。高祖孫恒是[太學生](../Page/太學生.md "wikilink")。曾祖孫明詩是[貢生](../Page/貢生.md "wikilink")。祖父孫炳焱是[邑庠生](../Page/邑庠生.md "wikilink")（秀才），授七品[文林郎](../Page/文林郎.md "wikilink")。1853年[太平军躆守三河鎮](../Page/太平军.md "wikilink")，祖父孫炳焱往南18公里移居[廬江縣北](../Page/廬江縣.md "wikilink")[金牛鎮](../Page/金牛鎮_\(廬江縣\).md "wikilink")，蓋仿徽宅，然而族人仍居三河鎮做生意。1869年父親孫熙澤生於金牛鎮，[舉人](../Page/舉人.md "wikilink")，受[淮軍勢力支持](../Page/淮軍.md "wikilink")，官至济南商埠地方审判厅厅长、山东高等审判厅厅长。父親的親兄（後來出繼）[孫浤澤](../Page/孫浤澤.md "wikilink")[光緒六年進士](../Page/:template:光緒六年庚辰科殿試金榜.md "wikilink")（1880年），[廣西](../Page/廣西.md "wikilink")[賀縣](../Page/賀縣.md "wikilink")[縣令](../Page/縣令.md "wikilink")，被上司奏劾奪官，[淮軍將領](../Page/淮軍.md "wikilink")、[台灣巡撫](../Page/台灣巡撫.md "wikilink")[刘铭传急](../Page/刘铭传.md "wikilink")[電召孫浤澤任台灣](../Page/電報.md "wikilink")[滬尾海關監督](../Page/滬尾.md "wikilink")。\[3\]孫立人1900年生於金牛鎮。\[4\][舒城县](../Page/舒城县.md "wikilink")[三河镇南岸今屬](../Page/三河镇_\(肥西县\).md "wikilink")[肥西縣](../Page/肥西縣.md "wikilink")。祖籍按傳統以祖父為準，故孫立人祖籍[三河镇](../Page/三河镇_\(肥西县\).md "wikilink")，父子故居均在[金牛鎮](../Page/金牛鎮_\(廬江縣\).md "wikilink")。\[5\]舒城縣、廬江縣均處[合肥與](../Page/合肥.md "wikilink")[桐城之間](../Page/桐城.md "wikilink")，孫家書香門弟，受[桐城派影響甚深](../Page/桐城派.md "wikilink")，與合肥反無關係。

### 童年

孫立人父親[孫熙澤是清末](../Page/孫熙澤.md "wikilink")[舉人](../Page/舉人.md "wikilink")，被派任為[山東](../Page/山東.md "wikilink")[登州府](../Page/登州府.md "wikilink")[知府](../Page/知府.md "wikilink")，舉家前往[青島居住](../Page/青島.md "wikilink")。[清朝末年](../Page/清朝.md "wikilink")，由於國土被[列強強行](../Page/列強.md "wikilink")[租借](../Page/租借.md "wikilink")，當時青島為[德國的勢力範圍](../Page/德國.md "wikilink")。孫立人9歲時，某天在海邊玩耍，發現了一顆極為漂亮的石頭，於是高興地將這顆石頭拿在手裡面玩耍。這時，來了幾個德國小孩，他們看見了孫立人手上的石頭，於是聯手欺負孫立人，搶走了石頭，並且打了孫立人兩記耳光，還辱罵了中國人。日後這次事件對於孫立人的影響極大，他認為「自己的國家一定要強盛，才能夠讓人民都活著有尊嚴」，也成為他日後投身軍旅的重要觸媒。

### 早期生涯

[1921_Sun_Li-ren_as_basketball_captain.jpg](https://zh.wikipedia.org/wiki/File:1921_Sun_Li-ren_as_basketball_captain.jpg "fig:1921_Sun_Li-ren_as_basketball_captain.jpg")畢業生名冊亦用此圖。\]\]
孫立人於1914年以安徽省第一名的成績考取[清華學校](../Page/清華學校.md "wikilink")（今[清華大學](../Page/清華大學_\(消歧義\).md "wikilink")）[庚子賠款留美預科](../Page/庚子賠款.md "wikilink")，接受八年的留[美預備訓練](../Page/美國.md "wikilink")。清華历来十分注重學生的[體育鍛煉](../Page/體育.md "wikilink")，孫立人在校風薰陶下，熱衷於[籃球](../Page/籃球.md "wikilink")、[足球](../Page/足球.md "wikilink")、[排球](../Page/排球.md "wikilink")、[網球](../Page/網球.md "wikilink")、[手球](../Page/手球.md "wikilink")、[棒球等各項球類運動](../Page/棒球.md "wikilink")，在眾多項目中孫立人最擅長的是籃球。1920年他任清華籃球隊隊長，率隊擊敗當時稱霸京津籃壇的[北京高等師範學校](../Page/北京高等師範學校.md "wikilink")，獲得華北大學聯賽冠軍。1921年入選[中國國家男子籃球隊](../Page/中國國家男子籃球隊.md "wikilink")，參加了在[上海舉行的第五屆](../Page/上海.md "wikilink")[遠東運動會](../Page/遠東運動會.md "wikilink")，身高1米85的孫立人當時擔任球隊的主力後衛。當時籃球項目有[菲律賓](../Page/菲律賓.md "wikilink")、[中國](../Page/中華民國.md "wikilink")、[日本三國參加](../Page/日本.md "wikilink")，東道主中國隊經過激戰，先以32-29擊敗日本，再以30-27擊敗菲律賓，獲得本屆運動會籃球冠軍，這是中國在國際大賽中第一次獲得的籃球冠軍。孫立人進清華後的第二個學期，因玩[蹺蹺板受傷而住院治療](../Page/蹺蹺板.md "wikilink")，又因[輸尿管障礙](../Page/輸尿管.md "wikilink")，休學一年，終於治癒，故在清華九年，於1923年畢業。

同年赴美留學。因其在清華學校已習基礎工程多門，故直入[普渡大學三年級](../Page/普渡大學.md "wikilink")[土木工程學](../Page/土木工程學.md "wikilink")，1925年[學士畢業](../Page/學士.md "wikilink")。期間曾為美國橋樑公司（）受聘當設計繪圖師。1926年孫立人进入[維吉尼亞軍校](../Page/維吉尼亞軍校.md "wikilink")，因其已有學士學位，故直入[博雅教育系](../Page/博雅教育.md "wikilink")（Liberal
Arts）三年級，1927年以文學士畢業，遊歷[歐洲](../Page/歐洲.md "wikilink")，參觀[英](../Page/英國.md "wikilink")、[法](../Page/法國.md "wikilink")、[德等國軍事](../Page/德國.md "wikilink")。

1928年孙立人回國，在[國民黨](../Page/國民黨.md "wikilink")[中央黨務學校](../Page/中央黨務學校.md "wikilink")（今[國立政治大學](../Page/國立政治大學.md "wikilink")），任中尉军训隊長。1930年入陸海空軍總司令部侍衛總隊任上校副總隊長。1932年調[財政部](../Page/中華民國財政部.md "wikilink")[稅警總團任第二支隊上校司令兼第四團](../Page/稅警總團.md "wikilink")[團長](../Page/團長_\(軍隊\).md "wikilink")。起先於1933年隨稅警總團調往江西剿共，他以一個團接下一個師的防地：在永豐、丁毛山、七琴等地以寡擊眾，大破共產黨軍隊。

稅警總團由財政部部長[宋子文一手創建](../Page/宋子文.md "wikilink")，武器從[德國購買](../Page/德國.md "wikilink")，排以上軍官大部分由留美學生擔任。在孫立人的訓練下，其所屬部隊官兵教育水準、以及學科、術科和緝私方式的水準都遠高於一般部隊。事實上，稅警總團、第36、第87、第88師、和中央教導總隊即是第一批接受德械裝備與訓練的部隊。

由於當年孫立人任團長的第四團曾在華東射擊比赛中獲得第一名，而前十名裡面第四團共佔了七名，孫式訓練遂逐漸嶄露頭角；也因孫熱愛[籃球](../Page/籃球.md "wikilink")，是政府撤退[臺灣後](../Page/臺灣.md "wikilink")，[中華民國國軍早期籃球運動鼓勵提倡者](../Page/中華民國國軍.md "wikilink")。

### 抗戰初期

[Sun_Li-jen.jpg](https://zh.wikipedia.org/wiki/File:Sun_Li-jen.jpg "fig:Sun_Li-jen.jpg")

1937年10月，孫立人率稅警總團第四團參加[淞滬會戰](../Page/淞滬會戰.md "wikilink")，與日軍血戰兩周，在周家橋破壞日軍機械化橡皮橋，七次擊退強渡[蘇州河的日軍](../Page/蘇州河.md "wikilink")，使該地成為淞滬會戰中日軍傷亡最重之處，日軍久留米師團稱孫團為其在華遭遇戰力最強之部隊。但此役孫立人為奪回友軍失去陣地，遭[迫擊炮彈攻擊身受十三處創傷](../Page/迫擊炮.md "wikilink")，由其部屬[李鴻營長指揮號長甦醒與機槍連長](../Page/李鴻_\(民國\).md "wikilink")[胡讓梨背負救出](../Page/胡讓梨.md "wikilink")，昏厥三天後[宋子文派弟弟](../Page/宋子文.md "wikilink")[宋子安将其立即送往](../Page/宋子安.md "wikilink")[香港接受完整治療](../Page/香港.md "wikilink"),以免日軍發現被俘。

次年傷癒後，孫立人返回[武漢加入財政部重組之緝私總隊擔任少將總團長](../Page/武漢.md "wikilink")，又參加了保衛武漢的戰鬥，之後率部遷移到[貴州](../Page/貴州.md "wikilink")[都勻練兵](../Page/都勻.md "wikilink")。重組之緝私總隊為[淞滬會戰後傷癒之稅警總團殘兵](../Page/淞滬會戰.md "wikilink")（未受傷的被團長[黃杰帶走改組為](../Page/黃杰_\(將軍\).md "wikilink")40師）加上新募為主，規模3團，經過兩年嚴格的訓練，孫立人將緝私總隊由原本之三團殘兵新兵逐步擴張至六團規模。

在國民政府急需有力部隊之要求下，1941年12月財政部被迫交出部分緝私總隊半數兵力給國民革命軍重組為[新編第三十八師](../Page/國民革命軍新編第三十八師.md "wikilink")，作為交換條件由原本財政部體系的孫立人晉任少將師長，隸屬於第六十六軍，這支部隊成為中華民國當時的主力部隊之一。

### 第一次中緬印作战

[Sun_Liren_n_Mountbatten.jpg](https://zh.wikipedia.org/wiki/File:Sun_Liren_n_Mountbatten.jpg "fig:Sun_Liren_n_Mountbatten.jpg")（英國）正與孫立人交談中。\]\]

1942年2月，[中華民國組成](../Page/中華民國.md "wikilink")[遠征軍](../Page/中國遠征軍.md "wikilink")，下轄第五軍、第六軍和第六十六軍。4月，孫立人率[新三十八師於進駐](../Page/新三十八師.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")[曼德勒](../Page/曼德勒.md "wikilink")，兼任衛戍司令，參加[曼德勒會戰](../Page/曼德勒會戰.md "wikilink")。4月14日，西線英帝國緬甸軍步兵第一師及裝甲第七旅被日軍包圍於[仁安羌](../Page/仁安羌.md "wikilink")，糧盡彈缺，水源斷絕，陷於絕境。孫將軍奉[羅卓英之命](../Page/羅卓英.md "wikilink")，派113團星夜馳援，[劉放吾團長](../Page/劉放吾.md "wikilink")16日下午四時率部趕到[巧克伯當](../Page/巧克伯當.md "wikilink")。英緬甸軍司令[斯利姆將軍](../Page/威廉·斯利姆，第一代斯利姆子爵.md "wikilink")17日親往會晤，命令該團乘汽車至[平牆河地區會同](../Page/平牆河.md "wikilink")[安提司准將之戰車](../Page/安提司.md "wikilink")，攻擊並消滅平牆河北岸約兩英哩公路兩側之敵。18日凌晨113團會同安提司准將之戰車向日軍發起猛烈攻擊，至午即攻克日軍陣地，殲敵一個大隊，解救了被包围的英国南方军区司令[哈罗德·亚历山大上将和](../Page/哈罗德·亚历山大，第一代突尼斯的亚历山大伯爵.md "wikilink")[威廉·斯利姆中将及七千英軍](../Page/威廉·斯利姆，第一代斯利姆子爵.md "wikilink")，並救出被日軍俘虜的[美國](../Page/美國.md "wikilink")[傳教士](../Page/傳教士.md "wikilink")、各國[新聞記者及婦女五百餘人](../Page/新聞記者.md "wikilink")。[仁安羌大捷是中國遠征軍入緬後第一個勝仗](../Page/仁安羌大捷.md "wikilink")，孫立人以一個[團不滿一千的兵力](../Page/團_\(軍隊\).md "wikilink")，擊退數倍的敵人，救出近十倍於己的友軍，[蔣中正頒發四等](../Page/蔣中正.md "wikilink")[雲麾勳章表彰孫立人的戰績](../Page/雲麾勳章.md "wikilink")。[美國](../Page/美國.md "wikilink")[小羅斯福總統亦授予他](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")[豐功勳章](../Page/豐功勳章.md "wikilink")，英王[喬治六世則授予他](../Page/喬治六世.md "wikilink")[大英帝國司令勳章](../Page/CBE.md "wikilink")。

仁安羌戰後，[英國方面決定棄守緬甸](../Page/英國.md "wikilink")，撤往印度，新三十八師奉命掩護盟軍撤退。4月下旬，英軍撤過曼德勒後繼續向西逃往[印度](../Page/印度.md "wikilink")。由于英军的潰退，导致了中國遠征軍陷入日軍包圍。中國遠征軍第一路軍副司令官[杜聿明因為對蔣中正表達忠誠](../Page/杜聿明.md "wikilink")，拒絕了時任[中國戰區參謀長](../Page/中緬印戰區.md "wikilink")[史迪威要他撤往印度的指示](../Page/史迪威.md "wikilink")，而执行了蒋中正令他經[野人山熱帶叢林北上撤回](../Page/野人山.md "wikilink")[雲南的命令](../Page/雲南.md "wikilink")，這也埋下東北剿共戰役杜聿明處處為難孫立人的伏筆。孫立人則認為野人山屬[瘴癘之區](../Page/瘴癘.md "wikilink")，縱橫千里，難以穿越，當機立斷率新三十八師向西撤往印度。由于日軍被杜聿明率领北上的大部隊所吸引，新三十八師在撤退途中比較順利地打垮日軍的阻擊。部隊裝備不但沒有損失，還收容了數以千計的[難民和英印散兵](../Page/難民.md "wikilink")。而杜聿明所率的第五軍因遭到日軍追擊，丧失了穿越野人山的最好时机，半數葬送在野人山中，孫將軍得知後立刻派遣新三十八師搜尋並救出部分友軍轉而撤往印度。

5月底，孫立人率新三十八師到達印度邊境。英駐印邊防軍要求中國軍隊解除武裝，以難民身份進入印度。孫立人拒絕解除武裝。恰巧，為新三十八師在仁安羌解救過的英聯軍第一師師長正於當地醫院療傷，聞知孫立人部隊的情況後，即前往調解。第二天，新三十八師開進印度，英軍儀仗隊列隊奏樂，鳴炮十響以表歡迎。

### 第二次中缅印作战

[Parade_of_US_equipped_Chinese_Army_in_India.jpg](https://zh.wikipedia.org/wiki/File:Parade_of_US_equipped_Chinese_Army_in_India.jpg "fig:Parade_of_US_equipped_Chinese_Army_in_India.jpg")，正手持中國國旗於印度進行閱兵。1942年8月中國駐印軍正式成立，由盟軍中國戰區參謀長約瑟夫·史迪威擔任總司令一職。\]\]

1942年8月先後到達印度的中國遠征軍新三十八師和[新二十二師進駐印度](../Page/新二十二師.md "wikilink")[蘭姆珈訓練基地](../Page/蘭姆珈.md "wikilink")，番號改為[中國駐印軍](../Page/中國駐印軍.md "wikilink")，開始裝備美械和訓練。10月，中國駐印軍改編成[新一軍](../Page/新一軍.md "wikilink")，[鄭洞國任軍長](../Page/鄭洞國.md "wikilink")，下轄孫立人新三十八師和[廖耀湘新二十二師](../Page/廖耀湘.md "wikilink")。1943年10月，中國駐印軍開始向緬北反攻。

戰役發動後孫立人指揮新三十八師进攻[胡康河谷](../Page/胡康河谷.md "wikilink")，新一軍戰車第一營在胡康河谷之戰立下輝煌戰績。10月29日占領[新平洋](../Page/新平洋.md "wikilink")，12月29日攻佔[於邦](../Page/於邦.md "wikilink")，1944年2月1日攻克[太白加](../Page/太白加.md "wikilink")，3月4日與廖耀湘新二十二師兩路夾擊攻克[孟關](../Page/孟關.md "wikilink")。3月9日，新三十八師113團與美軍[麥瑞爾突擊隊聯手攻佔](../Page/麥瑞爾突擊隊.md "wikilink")[瓦魯班](../Page/瓦魯班.md "wikilink")。日軍號稱“叢林作戰之王”的[第18師團死傷過半](../Page/第18師團.md "wikilink")，狼狽逃出胡康河谷。據日軍戰史記載：“師團長接到兩軍交鋒報告後，判斷這支敵軍只是為了掩護中美軍主力越境派出来的一支先遣部隊，首先命令[富昆南部地区的第](../Page/富昆.md "wikilink")56聯隊急速前進，企圖將其各個擊破。及至該聯隊到達戰場交戰後才搞清楚，敵軍原来是中國軍第38師（孫立人师）一支勁旅，和第18师团过去在中國大陸上接觸過的中國軍隊，在素质上完全不同，因而大吃一驚。過去，日軍以優異的單兵作戰能力及較先進的裝備，在面對中國軍隊時都有壓倒性的攻勢。尤其是這個由[九州編成](../Page/九州_\(日本\).md "wikilink")，素有把握的[第18師團轉戰中國](../Page/第18師團.md "wikilink")，同中國交戰最有自信，豈料胡康河谷富昆的中國駐印軍，無論是[編制](../Page/編制.md "wikilink")、[裝備](../Page/裝備.md "wikilink")，還是[戰術](../Page/戰術.md "wikilink")、[技術](../Page/技術.md "wikilink")，都完全改變了面貌，儘管第56聯隊奮勇猛攻，敵軍圓形陣地在緻密的火力攻擊網和空軍的支援下不僅毫不動搖，而日軍的損失卻不斷增加。中國軍雖已遭到了將近九百名的損失，卻依舊頑強抵抗，堅守密林陣地，毫不退讓。於是立即向上级報告了這個情况，使全軍不禁大為愕然。”\[6\]。

駐印軍攻占胡康河谷後，3月14日乘勝向[孟拱河谷進攻](../Page/孟拱河谷.md "wikilink")。新三十八師113團從左翼翻山越嶺迂回到[堅布山後方](../Page/堅布山.md "wikilink")，和新二十二師兩面夾擊，29日攻佔堅布山天險，叩開了孟拱河谷的大門。4月24日，按[史迪威的計劃](../Page/史迪威.md "wikilink")，新三十八師和新二十二師分別向[孟拱和](../Page/孟拱.md "wikilink")[加邁攻擊前進](../Page/加邁.md "wikilink")。5月下旬，孫立人將軍從繳獲的日軍信件中獲知：由於日軍第18師團主力在[索卡道被新二十二師包圍](../Page/索卡道.md "wikilink")，加邁城內兵力極為空虛，師團長[田中新一坐守空城](../Page/田中新一.md "wikilink")，惊恐万状。孫立人見機而行，不拘泥於原定計劃，以112團祕密渡過[南高江](../Page/南高江.md "wikilink")，向加邁南面的西通迂回，切斷加邁日軍的後路；以113團向西進取加邁；以114團向南對孟拱實施大縱深穿插。6月16日，113團與新二十二師會師加邁，日第18師團團長田中新一率1,500餘殘兵倉皇南逃。6月25日，孫師114團攻克孟拱。

8月3日，中美聯軍克復[密支那](../Page/密支那.md "wikilink")。至此，反攻緬北的第一期戰鬥結束。中國駐印軍給日軍王牌[第18師團等部予毀滅性打擊](../Page/第18師團.md "wikilink")，殲滅日軍2萬多人，一雪兩年前退兵緬甸的恥辱。史迪威稱此戰為“中國歷史上對第一流敵人的第一次持久進攻戰”。中國駐印軍攻克密支那後，部隊進行休整擴編，由新一軍擴編成兩個軍，即新一軍和[新六軍](../Page/新六軍.md "wikilink")。孫立人任新一軍中將軍長，下轄新三十八師和新三十師（後[廖耀湘新六軍回國增援國內抗戰](../Page/廖耀湘.md "wikilink")，其五十師編入新一軍）。

[Sun_Liren_n_Wei_Lih-huang.jpg](https://zh.wikipedia.org/wiki/File:Sun_Liren_n_Wei_Lih-huang.jpg "fig:Sun_Liren_n_Wei_Lih-huang.jpg")會師」：1945年1月中緬邊境[芒友](../Page/芒友.md "wikilink")，[中國遠征軍司令長官](../Page/中國遠征軍.md "wikilink")[衛立煌與](../Page/衛立煌.md "wikilink")[中國駐印軍](../Page/中國駐印軍.md "wikilink")[新一軍軍長孫立人握手](../Page/国民革命军新编第一军.md "wikilink")。衛立煌比孫立人大三年，安徽合肥人，兩人出生地僅距60餘公里。\]\]

1944年10月反攻緬北的第二期戰鬥開始。[中國駐印軍由](../Page/中國駐印軍.md "wikilink")[密支那](../Page/密支那.md "wikilink")、[孟拱分兩路繼續向南進攻](../Page/孟拱.md "wikilink")。孫立人率新一軍為東路，沿密支那至[八莫的公路向南進攻](../Page/八莫.md "wikilink")，連續攻取緬甸八莫、中國[南坎](../Page/南坎.md "wikilink")。

1945年1月1日，[國民政府明令頒授](../Page/國民政府.md "wikilink")[鄭洞國](../Page/鄭洞國.md "wikilink")、[霍揆彰](../Page/霍揆彰.md "wikilink")、[蕭毅肅](../Page/蕭毅肅.md "wikilink")、[周福成](../Page/周福成.md "wikilink")、[何紹周](../Page/何紹周.md "wikilink")、[王凌雲](../Page/王凌雲.md "wikilink")、孫立人、[廖耀湘以](../Page/廖耀湘.md "wikilink")[青天白日勳章](../Page/青天白日勳章.md "wikilink")，頒授[衛立煌以一等](../Page/衛立煌.md "wikilink")[寶鼎勳章](../Page/寶鼎勳章.md "wikilink")，頒授巴梯斯達以特種大綬[卿雲勳章](../Page/卿雲勳章.md "wikilink")，頒授薩拉里架以特種大綬[景星勳章](../Page/景星勳章.md "wikilink")\[7\]。1月27日，新一軍與滇西[中國遠征軍聯合攻克中國境內的](../Page/中國遠征軍.md "wikilink")[芒友](../Page/芒友.md "wikilink")，打通了[中印公路](../Page/中印公路.md "wikilink")，次日兩軍於芒友舉行會師，作為在越南[河內](../Page/河內.md "wikilink")（時稱東京）會師的前哨。隨後，孫立人指揮新一軍各師團繼續猛進，3月8日攻佔[臘戍](../Page/臘戍.md "wikilink")，3月23日占領[南圖](../Page/南圖.md "wikilink")，24日占領[西保](../Page/西保.md "wikilink")，27日攻克[猛岩](../Page/猛岩.md "wikilink")，消滅中緬印邊界所有的日軍部隊，第二次中緬印戰役以勝利告終。孫立人因戰功獲頒[青天白日勳章](../Page/青天白日勳章.md "wikilink")。

網上有人杜撰孫立人曾下令將1200名日軍俘虜中所有到過中國的都活埋，僅一名台灣新兵未到過中國而倖免於難。此乃無稽之談。\[8\]\[9\]

一些當年隨孫立人遠征的38師老兵也明確否認孫下令殺過戰俘。比如葉兆言曾寫文章記載一位抗戰老兵的說法：
"廣州一位三十八師老兵託人打電話給我……這位老者是孫立人部下，當年在師部諜報隊服役，是活著的見證人。出於對老長官的熱愛，他強調了以下幾點：第一，日本人非常頑強，生俘的很少，所謂活埋是胡說八道。第二，活捉俘虜可以獎勵，為了邀功，也不會這麼做。第三，為獲得情報，任何一名被俘的日本士兵都有價值，為防止他們自殺，常捆綁在門板上，戰俘待遇很高，有時甚至用飛機押送，如果要殺，根本沒必要浪費時間。這位叫梁振奮的老兵特別提到了孫立人的留美經歷，說孫將軍在美國學習軍事，他接受的教育，不可能做出虐殺戰俘的不人道行為。"（《葉兆言：孫立人將軍的傳說》）

《中央日報》遠征軍隨軍記者孫克剛（孫立人的堂侄）撰寫的《緬甸蕩寇志》中記載，「前後兩期攻勢作戰，和我軍（新一軍）對壘的日軍有第二、十八、四十九、五十三和五十六五個師團，及第三十四獨立旅和其它特種兵部隊，我軍擊斃日軍共33082人，其中包括3個聯隊長和其他高級軍官，傷其75499員名，俘虜田代一大尉以下官兵323人，敵人幾乎全軍覆沒，我軍和敵軍傷亡的比列，是1：6。」

《緬甸蕩寇志》還記載：「我們又在一個叫今田寬美的俘虜嘴裡得到一篇坦白的自供，他說：『當我被俘的時候，心裡很害怕，以為一定是死，不想反而受到優待』。……後來有一個日本俘虜問反正過來的台灣籍日語譯員鍾正平說：『你看見過孫立人將軍沒有……』，鍾正平回答他說：『是的……』，『你既然見了他，為什麼不行刺？』，俘虜誤認鍾正平是日本人。鍾正平說：『人家待我好，我為什麼要加害於他呢？』俘虜默然！」

### 抗戰勝利後

[Sun_Liren_and_George_S_Patton_in_Southern_Germany..jpg](https://zh.wikipedia.org/wiki/File:Sun_Liren_and_George_S_Patton_in_Southern_Germany..jpg "fig:Sun_Liren_and_George_S_Patton_in_Southern_Germany..jpg")將軍合影，攝於1945年。\]\]

孫立人將軍指挥新三十八師，在遠征緬甸，協同盟軍抗擊[日本的戰鬥中](../Page/日本.md "wikilink")，屢克強敵，戰功卓著，其運用的戰術、顯示的戰力備受國內外各方肯定，有“[東方](../Page/東方.md "wikilink")[隆美爾](../Page/隆美爾.md "wikilink")”之譽\[10\]；而被打敗的日軍在緬甸戰後史料上，尊稱他為“中國[軍神](../Page/軍神.md "wikilink")”。1945年5月，孫立人率新一軍返抵[廣西](../Page/廣西.md "wikilink")[南寧](../Page/南寧.md "wikilink")，準備反攻[廣州](../Page/廣州.md "wikilink")。同月，應[歐洲盟軍最高司令](../Page/歐洲.md "wikilink")[艾森豪威爾之邀](../Page/艾森豪威爾.md "wikilink")，孫立人赴歐考察歐洲戰場，是中國唯一被邀請的高級軍官。8月15日，侵華日軍投降。9月7日，新一軍進入廣州，接受日軍第二十三軍投降，並建造新一軍印緬抗日陣亡將士公墓。嗣後，新一軍進行了休整和擴充，成為[國軍五大主力之一](../Page/國軍五大主力.md "wikilink")，號稱“蓝鹰部队”、“天下第一軍”。

### [第二次國共內戰時期](../Page/第二次國共內戰.md "wikilink")

1945年8月[日本投降後](../Page/日本投降.md "wikilink")，蘇聯紅軍迅速佔領東北，把日本關東軍的軍火裝備提供給中共。林彪共軍至少接受“步骑枪30万支左右,
各种炮1600多门 ( 含迫击炮、掷弹筒) ，坦克 20
辆左右”\[11\]。就在幾個月前，整個八路軍總共只有154門炮。蘇聯紅軍把投降的滿洲國20萬軍隊交給中共整編，再加上成千上萬新近失業的男子，為生活只好當兵，使得林彪共軍從最初的6萬人轉瞬就發展到30萬，並成為共軍中作戰能力最強的部隊。
林彪是共軍將領中最能戰的“常勝將軍”，號稱東北“黑土地之狐”，率領四野佔領長春、哈爾濱、齊齊哈爾等重要城市。毛澤東命令林彪
“不惜任何犧牲”，“死守長春”，“死守四平，寸土必爭”。

1946年3月下旬，[國民革命軍新編第一軍乘美艦在](../Page/國民革命軍新編第一軍.md "wikilink")[秦皇島登陸](../Page/秦皇島.md "wikilink")，同時孫立人被派往[美國參加](../Page/美國.md "wikilink")[聯合國軍事參謀團會議](../Page/聯合國.md "wikilink")，由於抗戰後國共關係迅速惡化，國共兩黨爭奪[中國東北戰事受阻](../Page/中國東北.md "wikilink")，1946年4月，國共雙方在東北戰略重鎮吉林省四平開始了第一次主力決戰。原先蔣中正曾下令限東北行營4月2日前攻下四平，可是東北保安司令杜聿明指揮國軍北上進展遲緩，前後拖延幾近兩月，中間尚有挫敗，蔣中正為此十分焦急，急電孫立人返國指揮新一軍。5月15日夜，孫立人趕到新一軍軍部，連夜制定攻擊計畫。5月16日，新編第五十師從正面攻擊，新編第三十師和新編第三十八師從側翼攻擊，孫立人親到各師前沿，新一軍官兵見軍長回來了，士氣大振，奮勇猛撲林彪部防線。5月17日，新五十師第一五〇團首先突破，攻克四平東南制高點五頂山，林彪指揮部隊連續反擊，始終無法奪回。
5月17日，當戰事持久拉鋸不下，雙方攻守最激烈的時刻，蔣介石派首任國防部長白崇禧飛赴東北督戰。白崇禧一到東北，即召開軍事作戰會議，重新調整作戰部署，國軍士氣大振。
5月18日，白崇禧偕杜聿明赴前線指揮所督戰，指揮孫立人新一軍、廖耀湘新六軍、陳明仁第七十一軍這些中央軍精銳，分三路向四平林彪共軍進逼包抄。新編第三十八師第一一四團突破四平東北防線，新三十師第八十八團突破城南防線，林彪棄守四平。5月19日，孫立人之[新一軍進占四平街](../Page/新一軍.md "wikilink")，陳林達之第一九五師攻占[四平街東側哈福屯](../Page/四平街.md "wikilink")，歷時33天之四平街攻防戰至此結束\[12\]。[鄭洞國久攻不下的四平](../Page/鄭洞國.md "wikilink")，孫立人等新軍將領只用3天於5月19日便徹底就攻克，瓦解了杜聿明5個月來與共產黨[東北民主聯軍林彪對峙的僵局](../Page/東北民主聯軍.md "wikilink")，林彪執行[毛澤東](../Page/毛澤東.md "wikilink")“讓開大路，佔領兩廂”的命令退到[公主嶺](../Page/公主嶺.md "wikilink")，杜聿明在接到投降的東北民主聯軍總部作戰科長王繼芳的情報报告共軍戰力已失，命令所屬5個軍進行超越追擊，孫立人一路前進，親率新五十師強渡[遼河](../Page/遼河.md "wikilink")，攻擊公主嶺，林彪又北撤，五日內攻陷[長春](../Page/長春.md "wikilink")，隨後取回[農安](../Page/農安.md "wikilink")、[德惠等戰略要地](../Page/德惠.md "wikilink")，進展順利。6月5日，攻占陶賴昭車站。此時哈爾濱中共黨軍及各類機關是一片混亂﹐正在往[佳木斯方向潰散](../Page/佳木斯.md "wikilink")。

此時林彪部隊流傳“只要不打新一軍，不怕中央百萬兵”\[13\]。但在救援[海城的問題上](../Page/海城市.md "wikilink")，孫立人與杜聿明發生了矛盾，杜聿明調度有失，孫立人向蔣中正抗議，埋下將帥失和伏筆！至此1946年四、五月間，東北第一次“四平街會戰”，國軍大勝。但此時，正值馬歇爾來華調停，馬歇爾不斷向蔣中正施壓，為迫使國共停戰不惜以美國援華五億貸款為要挾，敦促東北國軍停止追擊到松花江南岸為止，此舉讓林彪潰敗之軍，得以喘息。但蔣中正為了顯示國軍在松花江以北的存在，指令杜聿明派一個團渡過松花江，佔領交通要點陶賴昭堡，此地易守難攻，杜聿明命令孫立人率新五十師渡江，隔日，攻取了[陶賴昭堡](../Page/陶賴昭堡.md "wikilink")，此時距[哈爾濱僅六十公里](../Page/哈爾濱.md "wikilink")，位在哈爾濱的中共黨政軍組織均已做好撤退準備，在孫立人即將攻進哈爾濱之時，1946年6月6日，蔣中正下第二次停戰令；國共停戰。[張正隆的](../Page/張正隆.md "wikilink")《[雪白血紅](../Page/雪白血紅.md "wikilink")》一書中寫道：“從當年的林彪到今天的老人，都說國民黨沒向江北推進是失算。否則，共產黨的日子將更難過。”蔣中正後來也反省他在1946年6月6日頒發第二次停戰令對東北戰局的影響，在他撰寫的《蘇俄在中國》中做出了這樣的結論：「可說這第二次停戰令之結果，就是政府在東北最後失敗之唯一關鍵。」

[Lin_Wang_and_Sun.jpg](https://zh.wikipedia.org/wiki/File:Lin_Wang_and_Sun.jpg "fig:Lin_Wang_and_Sun.jpg")一同合影，攝於1947年。\]\]

1947年林彪為打破杜聿明的先南後北的作戰方針，先後發動了[三下江南](../Page/三下江南.md "wikilink")、[四保臨江作戰](../Page/四保臨江.md "wikilink")，1947年1月5日，孫立人的新三十八師1個營被東北民主聯軍第3師包圍，孫立人命第一一三團主力解圍，結果在其塔木被全殲，但孫立人卻一直堅守松花江南岸，沒有給林彪以更大的戰果。[德惠之役後](../Page/德惠之役.md "wikilink")，新一軍扼守松花江南岸的一個連及堅守德惠的第五十師第一四九團獲蔣中正賜名“中正連”、“中正團”。但隨後，杜聿明屢次發電向蔣中正批評孫立人，指責其作戰不力，驕橫跋扈。

4月26日，新編第一軍軍長孫立人改任東北保安副司令官，潘裕昆繼任軍長\[14\]。蔣中正鑑於孫立人和杜聿明不和，而且在東北国军將領中比較孤立，不可能取代杜聿明統領各軍，所以蔣中正將他升為東北保安司令部副司令長官虛職，暫時解除兵權，其新一軍軍長之職由[黃埔軍校出身的第五十師師長](../Page/黃埔軍校.md "wikilink")[潘裕昆接任](../Page/潘裕昆.md "wikilink")。1947年6月16日，蔣中正接見東北保安副司令孫立人，聽取對東北戰況之報告；蔣經國飛四平、公主嶺等地視察\[15\]。

同年7月，蔣中正將孫立人調離東北，在[南京成立陸軍訓練司令部](../Page/南京.md "wikilink")，負責全國國防新軍訓練的重任。此消息一傳到哈爾濱後，毛澤東開慶祝會道：「我們唯一的敵人被杜聿明趕走了，東北將是我們的天下了。」\[16\]8月4日，國民政府特派孫立人代理陸軍總司令兼[陸軍總司令部陸軍訓練司令](../Page/陸軍總司令部.md "wikilink")\[17\]。8月31日，陸軍副總司令兼訓練司令孫立人，自南京飛抵台北，籌劃國軍新軍訓練事\[18\]。[陳誠赴東北](../Page/陳誠.md "wikilink")，拆散新一軍組成新一軍、新七軍、把新一軍原有主要武器移交其他黃埔系將領;並且將原本已編入地方[保安](../Page/保安.md "wikilink")，接受日本關東軍精良訓練的原[滿軍裁撤](../Page/滿洲國軍.md "wikilink")，使許多滿軍因頓時失去生活來源而紛紛加入共產黨軍隊，大大增加共產黨在東北的實力。孫立人離開東北時，東北局勢雖已被動但尚未惡化至不可為地步，是国军在東北唯一全身而退的將領。

1947年11月，孫立人將陸軍訓練司令部遷到[臺灣的](../Page/臺灣.md "wikilink")[高雄縣](../Page/高雄縣.md "wikilink")[鳳山鎮](../Page/鳳山區.md "wikilink")，並從新一軍調去幾百名他在稅警總團和在緬甸作戰時期的幹部，一同前往臺灣訓練新兵，在臺灣建立新軍。

### 臺灣時期

1949年1月21日[蔣中正發佈](../Page/蔣中正.md "wikilink")“引退文告”，由副總統[李宗仁任代理總統與共產黨進行和談](../Page/李宗仁.md "wikilink")。2月11日，美國[麥克阿瑟將軍派一名中將特使到台灣](../Page/麥克阿瑟.md "wikilink")，以專機邀請孫立人到[東京會談](../Page/東京.md "wikilink")。同月，青年軍[第二零一師調臺灣受訓](../Page/第二零一師.md "wikilink")。

1949年9月1日，孫立人正式就職[臺灣防衛司令](../Page/臺灣防衛司令.md "wikilink")，第二零一師10月在[金門參加](../Page/金門.md "wikilink")[金門戰役](../Page/金門戰役.md "wikilink")。11月，蔣中正在国民黨中央非常委員會第二分會中提議孫立人再兼[東南軍政長官公署副長官](../Page/東南軍政長官公署.md "wikilink")，[國防部亦隨之發表新職](../Page/中華民國國防部.md "wikilink")。

1950年3月1日，蔣宣佈復行視事，重新擔任中華民國總統，並發表文告。\[19\]蔣核定國防機構系統，撤銷東南軍政長官公署。\[20\]蔣以孫為[陸軍總司令](../Page/中華民國陸軍司令.md "wikilink")。\[21\]孫兼任[臺灣防衛總司令](../Page/臺灣防衛總司令.md "wikilink")（當時陸軍總司令部與臺灣防衛總司令部址乃同一駐所）。10月[中国人民志愿军進入](../Page/中国人民志愿军.md "wikilink")[朝鮮參戰](../Page/朝鮮.md "wikilink")。美方對蔣中正的態度轉變。美國[参谋长联席会议為了減緩在韓戰的壓力](../Page/参谋长联席会议.md "wikilink")，立即制定一份中華民國國軍行動綱領，希望中華民國國軍進行在中國邊境進行游擊戰，奪回並控制廣西、雲南等西南省分，來達到削弱甚至推翻[中共的目的](../Page/中共.md "wikilink")。孫立人曾向蔣中正提出反攻計畫，遭蔣中正拒絕。蔣中正擔心形成僵局，且顧忌曾留學美國的孫立人威脅他的權力\[22\]。蔣中正願意派遣軍隊到韓國加入韓戰，美方也不同意。

孫立人主張軍隊國家化，對於曾留學[蘇聯而時任國防部總政治作戰部主任的](../Page/蘇聯.md "wikilink")[蔣經國以](../Page/蔣經國.md "wikilink")[政工制度破壞現代軍事體制有不滿之意](../Page/中華民國政戰.md "wikilink")，對當時主管警備總部的[彭孟緝也不假辭色](../Page/彭孟緝.md "wikilink")。曾在1950年12月，孫立人召開「新年第一次年終擴大良心會」时，致詞說：「現在社會黑暗，人心不古，不但做事騙人，說話也騙人，所以社會動盪不安，就是彼此不能開誠相見，埋沒了良心之故。」因此也与蔣經國的政工系统埋下冲突之因。[蔣經國向蔣中正構陷孫立人](../Page/蔣經國.md "wikilink")，認為孫立人希望藉由反攻大陸計劃，獲得更多軍事力量，會威脅到蔣中正的權力，主張拔除他所有軍權。

1951年5月，孫立人晉升陸軍[二級上將](../Page/二級上將.md "wikilink")，但卻只掛二星在肩頭，他堅持第三顆星要在反攻大陸時掛上。[美軍顧問團](../Page/美軍顧問團_\(中華民國\).md "wikilink")（MAAG）在台北正式成立，美援的條件是中華民國政府建立起一套美國認可的國防軍事預算與監督機制。7月，一份代號為「三七五」的反攻總計畫，擬定以閩南與[海南島作為反攻登陸地點](../Page/海南島.md "wikilink")，此後數月裡，美方不斷向臺北推銷反攻海南島的方案，孫立人也持贊同立場，然而蔣中正卻並不熱衷\[23\]。

1952年4月，孫立人連任陸軍總司令。1953年6月，孫立人在稅警總團的老長官[黃杰領三萬軍隊](../Page/黃杰_\(將軍\).md "wikilink")，自[越南](../Page/越南.md "wikilink")[富國島到台灣](../Page/富國島.md "wikilink")。8月黃杰任台北衛戍司令。9月，晉升陸軍二級上將。
[屏東市孫立人將軍行館.jpg](https://zh.wikipedia.org/wiki/File:屏東市孫立人將軍行館.jpg "fig:屏東市孫立人將軍行館.jpg")孫立人將軍行館\]\]
孫立人始終主張反攻大陸。1954年、1955年接受[美聯社記者訪問時他承認反攻大陸是一個極危險的賭博](../Page/美聯社.md "wikilink")；不過認為延遲反攻並不會減少危險性（當時[第一次台灣海峽危機正熾](../Page/第一次台灣海峽危機.md "wikilink")）。他表示：「局勢將因中共在華南沿海增強防務而告揭曉。共黨於完成增強防務後便能向中美在臺灣海峽的空中與海面優勢挑戰。一旦失去海空控制權後便很難守住任何島嶼。」「共黨的力量外看似乎要比實際上強大；但中華民國軍隊反攻後，大陸上的匪軍將普遍起來反正，被奴役的人民將起來幫助我們。」「進攻總是一樁冒險的事，不過我們非做此冒險不可——我們在這裏除此之外還為了什麼？」\[24\]

孫立人致力於國軍現代化，整編撤退來臺之國軍，建立完善之[兵役制度與](../Page/兵役.md "wikilink")[預備軍官制度](../Page/預備軍官.md "wikilink")。前[行政院院長](../Page/行政院院長.md "wikilink")[郝柏村亦曾經歷孫立人興辦的軍事訓練計畫](../Page/郝柏村.md "wikilink")，成績甚佳。[古寧頭戰役的勝利](../Page/古寧頭戰役.md "wikilink")，與孫的努力有關。防守第一線的二零一師就是經過孫的陸軍訓練司令部的訓練。

1954年6月24日，蔣中正任命黃杰為陸軍總司令，將孫立人調任至無實權之[總統府參軍長](../Page/總統府侍衛長.md "wikilink")，12月3日中華民國與美國雙方簽訂《中美共同防禦條約》，蔣氏政權在獲得美國充分保障後，已不再需要依靠孫立人來維繫臺美關係。

1955年5月25日，孫立人舊部屬[郭廷亮被捕](../Page/郭廷亮.md "wikilink")，遭嚴刑拷問，軍統局長毛人鳳逼迫孫立人舊部屬郭廷亮自誣為匪諜，引發“郭廷亮匪諜案”。5月28日蔣中正召見孫立人，說他打仗不行，解除了他的職務，將他軟禁在家，派人監視。6月，政府當局以其部屬少校郭廷亮預謀發動兵變為由，對孫實施看管偵訊。8月20日，[孫立人兵變事件公开化](../Page/孫立人兵變案.md "wikilink")，政府以“纵容”部属武装叛乱，“窝藏[共匪](../Page/共匪.md "wikilink")”，“密谋犯上”等罪名，公開革除孙总统府参军长职务。總統府參軍長孫立人因『匪』諜[郭廷亮案引咎辭職](../Page/郭廷亮.md "wikilink")，蔣中正指定[陈诚为主任等](../Page/陈诚.md "wikilink")9人組織调查委员会澈查。\[25\]10月，孫立人案調查委員會提出調查報告，蔣中正以孫立人抗戰有功，特准予自新，毋庸議處。\[26\]孙被判处“长期[拘禁](../Page/拘禁.md "wikilink")”在[台中市](../Page/台中市.md "wikilink")[向上路寓所](../Page/向上路_\(台中市\).md "wikilink")。自孙立人被拘禁后，其亲信部属一一被调离军职查办，前后有300多人因与本案有牵连而被捕入狱，包括當時擔任英文秘書的黃正以及擔任女青年工作大隊中校組長的姊姊黃玨等人。

孫立人在台中寓所被軟禁時，出入都遭到特務監視，不能與人交談。孫立人妻子在院子裡種玫瑰然後拿到市場去賣，於是就有人稱那些玫瑰叫做“將軍玫瑰”。1988年1月13日蔣經國過逝後，同年5月接任總統的[李登輝](../Page/李登輝.md "wikilink")，下令解除了孫長達33年的軟禁，恢復其自由。

1990年11月19日，孫立人病逝于臺中市西區的寓所，享寿89岁。

時任行政院院長[郝柏村接受總統李登輝命令](../Page/郝柏村.md "wikilink")，派陸軍總司令部辦理治喪事宜。故總統蔣中正遺孀[宋美齡致送花圈](../Page/宋美齡.md "wikilink")、總統[李登輝頒發](../Page/李登輝.md "wikilink")[褒揚令給予褒揚](../Page/褒揚令.md "wikilink")：

喪禮由[總統府資政](../Page/總統府資政.md "wikilink")[鄭為元主祭](../Page/鄭為元.md "wikilink")，[參謀本部](../Page/中華民國國防部參謀本部.md "wikilink")、三軍總司令部等陸海空高級軍官皆出席。靈柩由清華大學校旗覆旗委員[洪同](../Page/洪同.md "wikilink")、[李榦](../Page/李榦.md "wikilink")、[劉兆玄](../Page/劉兆玄.md "wikilink")、[張昌華覆蓋校旗](../Page/張昌華.md "wikilink")，維吉尼亞軍校校旗覆旗委員[溫哈熊](../Page/溫哈熊.md "wikilink")、[溫-{于}-儉](../Page/溫于儉.md "wikilink")、[葉晨暉覆蓋校旗](../Page/葉晨暉.md "wikilink")，再由中華民國政府指派之覆旗官，[許歷農](../Page/許歷農.md "wikilink")、[羅本立](../Page/羅本立.md "wikilink")、溫哈熊、[黃幸強四位](../Page/黃幸強.md "wikilink")[上將代表國家覆蓋](../Page/上將.md "wikilink")[國旗](../Page/中華民國國旗.md "wikilink")，備極哀榮。遺體安葬[臺中市](../Page/臺中市.md "wikilink")[北屯區](../Page/北屯區.md "wikilink")[大坑](../Page/大坑風景區.md "wikilink")[東山墓園](../Page/東山墓園.md "wikilink")。

孫立人對二次大戰的貢獻也獲得國際肯定，維吉尼亞軍校在校史館中，將孫立人同另兩位第二次世界大戰中的傑出校友──馬歇爾將軍和巴頓將軍並列，永久展覽孫立人生前所用的軍服、軍帽、馬靴、馬鞭、繳獲的日軍軍旗、畢業證書和畫像。

[1945_Sun_Liren_on_Chinese_papers_front_page2.jpg](https://zh.wikipedia.org/wiki/File:1945_Sun_Liren_on_Chinese_papers_front_page2.jpg "fig:1945_Sun_Liren_on_Chinese_papers_front_page2.jpg")

## 孫立人兵變案

孫立人兵變案（郭廷亮匪諜案），對「孫案」之調查，最初由副總統[陳誠為主任委員](../Page/陳誠.md "wikilink")，與[王寵惠](../Page/王寵惠.md "wikilink")、[許世英](../Page/許世英.md "wikilink")、[張群](../Page/張群.md "wikilink")、[何應欽](../Page/何應欽.md "wikilink")、[吳忠信](../Page/吳忠信.md "wikilink")、[王雲五](../Page/王雲五.md "wikilink")、[黃少谷](../Page/黃少谷_\(政治\).md "wikilink")、[俞大維等組成](../Page/俞大維.md "wikilink")「九人委員會」，負責調查「郭廷亮匪諜案」。另外監察院也由中國國民黨籍[監察委員](../Page/監察委員.md "wikilink")[陶百川](../Page/陶百川.md "wikilink")、無黨籍監委[曹啟華](../Page/曹啟華.md "wikilink")、[蕭一山](../Page/蕭一山.md "wikilink")、[王枕華](../Page/王枕華.md "wikilink")、[余俊賢等](../Page/余俊賢.md "wikilink")「五人小組」自行發動調查。陳誠的九人小組審訊郭廷亮等，得到彼等「坦承不諱」之「自首」結果。由於郭係孫立人之聯絡官，九人調查小組認為孫「未適當防範，有失查之責。」。軍事法庭遂以[匪諜罪判處郭廷亮死刑](../Page/匪諜.md "wikilink")，隨後由高層下令改為無期徒刑，逕送綠島。孫立人被判無期限監禁在家。

監察院[陶百川等](../Page/陶百川.md "wikilink")「五人小組」之調查結果與「九人小組」差異甚大。監院認為郭廷亮等確係主張軍事改革，但絕無興兵叛亂之情節。所謂遭人檢舉，乃屬國民黨軍隊內部派系構陷，不足為憑。孫立人對此應毫無責任。監院調查結果因與「九人小組」差異過大，調查期間遭到情治跟監，最後僅在院內會議秘密報告後即以極機密封存，不再公開。

1991年，郭廷亮[假釋自綠島返臺](../Page/假釋.md "wikilink")，至臺北奔走，希望平反孫立人罪名。卻在[桃園縣](../Page/桃園市.md "wikilink")[中壢車站](../Page/中壢車站.md "wikilink")，自火車摔至月臺上後身亡。司法調查以意外或自殺結案，但學者認為可能是遭謀殺。

1998年孫案涉案人及家屬要求監察院公佈「五人小組」報告並還原真相。2001年1月8日，監察院通過決議，稱孫案乃「被陰謀設局的假案」。中央研究院近代史研究所[朱浤源教授獲政府新臺幣六十萬元經費補助](../Page/朱浤源.md "wikilink")，就孫立人兵變案進行專門研究。朱浤源認為找不到任何證據證明孫立人有軍事叛變行為。

2014年7月，[監察委員](../Page/監察委員.md "wikilink")[李炳南](../Page/李炳南.md "wikilink")、[趙榮耀](../Page/趙榮耀.md "wikilink")、[馬秀如等人公布對當初](../Page/馬秀如.md "wikilink")「九人小組」調查檔案的報告。在檔案中，[總政治部主任](../Page/總政治部.md "wikilink")[蔣經國曾向蔣中正建議拔除孫立人兵權](../Page/蔣經國.md "wikilink")，以防他叛變。調查結果認為，蔣中正曾直接對孫案下達許多指示，這個案件是利用[美國](../Page/美國.md "wikilink")[麥卡錫主義興起](../Page/麥卡錫主義.md "wikilink")，[反共意識抬頭的機會](../Page/反共.md "wikilink")，設局將孫立人軟禁，以免蔣中正權力遭威脅。[美國政府在本案發生後](../Page/美國政府.md "wikilink")，曾私下對[台灣當局表示關切](../Page/台灣當局.md "wikilink")，因此事件才未擴大。這個報告中，同時對郭廷亮進行平反，認為他係遭構陷。

2017年10月，由新聞報導一起軍備局產權訴訟判決，法院文件揭露出1955年陸軍第十軍中校劉永德，曾於一九五五年檢舉郭廷亮意圖發動兵變，經第十軍政戰部主任阮成章少將轉報，由前總統蔣經國（時任國防會議副秘書長）指派劉永德，深入叛亂組織擔任內線為時數月揭發叛變案。後來蔣經國下令「便簽」交辦，特准核撥獎勵專款八萬元給予劉永德獎勵。

## 評價

孫立人被認為是[國民革命軍中](../Page/國民革命軍.md "wikilink")，具備豐富現代化作戰經驗和卓越指揮才能的將領，他非常愛護部屬，因此由他所帶領的軍隊極具向心力、非常地忠貞愛國。孫立人並且擁有崇高的國際聲望，是國民革命軍中與眾不同的一名將軍，也是[中華民國國軍當中極少數從美國軍校畢業的高階將領](../Page/中華民國國軍.md "wikilink")，因受西式教育的影響，陸軍總司令任內他批評政工權力過大，是威權時期少數勇於提出改革建言者。孙立人是以客攻主、以少戰多、以弱敵強而皆能克敵致勝的名將。他長期在补给完善的精锐部队任职，无论在中缅印对日战役，或在东北[新一軍对中共的战事等](../Page/新一軍.md "wikilink")，面对各种艰苦复杂的作战环境皆有深刻的认识与卓越的战绩；但因個性心直口快、人際關係處理得不圓通，再加上深得部屬們的尊敬與美國政府的青睞，使他於不知不覺中，陷入政治漩渦，而遭人構陷。

孫立人終身抱持忠貞信念不改。在《孫立人言論選集》中，孫立人多次強調做人要腳踏實地，以「誠」與「拙」自許，主張誠懇，反對以小聰明奉承上級。中華民國前駐泰大使、曾隨孫立人將軍擔任秘書四年的[沈克勤](../Page/沈克勤.md "wikilink")，在日後公開的蔣介石日記中關於孫立人一案記載，以自對孫立人一生的觀察，用「直性而行，率性而為」八字，作為孫立人個性與行事作風的評價。沈克勤表示，「孫立人的精忠報國是不容懷疑的。」他回憶，1974年，他偷偷前去病床探視長期遭軟禁而臥病的孫立人，提及以佛家忘我來平撫被監禁與生病的苦難，遭孫立人指責：“如此，誰來救國？”他始終不曾忘記對國家的責任。但孫立人對於當時以國民黨政府為主所力行「以黨領軍」與蔣中正、蔣經國希望推動個人威權崇拜之理念作風頗有不贊同的微詞，造成他受到蔣中正整肅的結局。

孫立人過去在對如作戰策略或佈局等諸多處置作法之會議上，常會為一件事情的看法而與自身意見相左的長官或同軍階將領發生爭執，即便從事後觀點來看孫立人之意見看似是較為有利或正確，但當下之堅持己見的固執行為已令與孫立人共事過的長官、同軍階將領感到不悅。他功業過大，引起蔣中正與其他將領側目，因非出身黃埔系統，也不擅長逢迎，因此不得蔣中正信任。

反对意见则认为，孙立人孤高自傲、無黨籍，与作为国民革命军主流的[黄埔系将领格格不入](../Page/黃埔軍校.md "wikilink")，也不擅协调各方面间之矛盾，使他在注重人脉的国民革命军中难有大作为。[劉宜良](../Page/劉宜良.md "wikilink")（江南）的《[蔣經國傳](../Page/蔣經國傳.md "wikilink")》中曾提到：「孫是個非常優秀的帶兵官，但是位很壞的領袖。講人際關係，和他的同輩，幾乎沒有人可以和得來。」[香港](../Page/香港.md "wikilink")《[七十年代](../Page/九十年代_\(雜誌\).md "wikilink")》刊「孫立人在台兵變經過」一文，對孫立人受排擠的情況有相當生動的描寫：「可憐的陸軍，四面是海，可憐的總司令，孤掌難鳴，陸軍提出的許多問題，都遭到空海軍的聯合杯葛、阻礙。有時孫立人氣急了，就在會議上向蔣中正報告說：海軍、空軍如何好、如何行，那麼請總統將陸海空三軍測驗一下，比一比，看究竟那一軍好。先從我們三軍總司令考起，比文也好、比武也好、比立正稍息也好、比X+Y也好，由你們海空軍決定好了。像這樣情形，最後還是由蔣中正打圓場。」\[27\]其部屬沈克勤回憶，空軍總司令[周至柔曾多次表示想與孫立人結拜](../Page/周至柔.md "wikilink")，但孫立人為了爭取陸軍權益，曾當眾與周至柔吵架。因為對政戰系統不滿，蔣經國來拜年時，孫立人也置之不理。

1955年孫立人事件發生，據當時擔任總政戰部主任的[張彝鼎指出](../Page/張彝鼎.md "wikilink")：「這件事情很複雜，孫立人是一個很有才能的人，老總統（蔣中正）也很看重他，但是孫立人的個性太强。」「做為一個武將，能和古代的[郭子儀或民國的](../Page/郭子儀.md "wikilink")[徐永昌那樣](../Page/徐永昌.md "wikilink")，隨和一點，會比較好。像[徐永昌](../Page/徐永昌.md "wikilink")，先後在[馮玉祥](../Page/馮玉祥.md "wikilink")、[閻錫山及老總統手下做過事](../Page/閻錫山.md "wikilink")，和每一個人都處得很好，這樣才算成功。」\[28\]

### 與美國關係

孫立人曾在美國留學，在第二次世界大戰中，率領中國遠征軍，獲得美國軍方的高度評價，被認為是中華民國當時最優秀、最具國際觀的將領。但也因此在國民政府遷台之後遭到蔣中正懷疑他挾美國以自重。

1949年2月11日，美國[麥克阿瑟將軍派一名中將特使到台灣](../Page/麥克阿瑟.md "wikilink")，邀請孫立人到[東京會談](../Page/東京.md "wikilink")。孫立人通過臺灣省政府主席、臺灣省警備總司令部總司令[陳誠先要求蔣中正的許可](../Page/陳誠.md "wikilink")，蔣中正同意後，孫立人搭乘麥克阿瑟的飛機飛往日本。麥克阿瑟告訴孫立人，美國希望孫承擔防衛台灣的責任，美國將給予他全力支持，但孫立人表明他支持蔣中正。當回到台灣後，孫立人立即向陳誠報告麥克阿瑟與他的談話，並要求陳向蔣中正報告此事。

[美軍顧問團至台灣對蔣中正提供建議](../Page/美軍顧問團_\(中華民國\).md "wikilink")，蔣中正曾經詢問過美軍顧問，對中華民國國軍將領的評價。其中只有孫立人得到美軍顧問的高度評價，認為他可以指揮20萬人以上的部隊，至於[陳誠](../Page/陳誠.md "wikilink")，被認為只有團長的能力。

1950年，美國國務院曾提出內部假想方案，在台灣發生動亂時，考慮要求蔣中正下野，軍隊指揮權交給孫立人；以[胡適替代蔣](../Page/胡適.md "wikilink")。但在6月[韓戰爆發後](../Page/韓戰.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")[杜魯門下令](../Page/杜魯門.md "wikilink")[第七艦隊巡弋](../Page/第七艦隊.md "wikilink")[台灣海峽](../Page/台灣海峽.md "wikilink")。美國國務院內部轉而支持蔣中正，假想方案並未實施。

孫立人長期主張反攻大陸，在[韓戰期間](../Page/韓戰.md "wikilink")，曾主張借此機會反攻。從[蔣中正以後被公開的日記裡可知](../Page/蔣中正.md "wikilink")，從1951年的1月初開始，孫立人提出反攻大陸計劃，極力想說服蔣同意這個方案，同時希望取得反攻大陸全盤之軍事指揮權，甚至以辭職要脅。結果被蔣批評為「自不量力，只想藉美國之感情保護。」\[29\]

中華民國前陸軍上將、寓居在香港的[張發奎](../Page/張發奎.md "wikilink")，在《張發奎口述自傳》文中評論中華民國過去活躍於檯面上的政壇人物時，對孫立人之下場的是如此評論：「恃才傲物是孫立人失勢的主要原因，美國人想扶植他反而害了他。他以為美國人支持他，以至得意忘形。然而，美國人是不可靠的，孫立人的矜才使氣使他得不到蔣先生的信任。」

## 家庭

### 故居

  - 台北故居：[日治時期為](../Page/台灣日治時期.md "wikilink")[臺灣軍司令官官邸](../Page/原臺灣軍司令官官邸.md "wikilink")，採和洋混風建築樣式，風格特殊。民國三十六年（1947年）七月，代理陸軍副總司令兼陸軍訓練司令孫立人將軍，抵臺視察並決定於臺灣[鳳山設立訓練基地](../Page/鳳山區.md "wikilink")，民國三十九年（1950年）此建築物成為孫將軍官邸，直到民國四十四年（1955年）八月[孫立人事件發生止](../Page/孫立人事件.md "wikilink")。2004年1月15日[臺北市政府指定為市定古蹟](../Page/臺北市政府.md "wikilink")（府文化二字第09304425500號），現為中華民國國防部陸軍聯誼廳，現址為臺北市南昌路一段136號。

<!-- end list -->

  - 台中寓所：為一棟日治時期的[臺灣日治時期建成的日本傳統木構建築](../Page/臺灣日治時期.md "wikilink")，孫立人事件後全家移居此處被政府持續監視，1956年6月被迫遷居[臺中市](../Page/臺中市.md "wikilink")[西區](../Page/西區_\(臺中市\).md "wikilink")，1988年5月政府解除監視後依舊居住此處，孫立人平時有空種種[玫瑰](../Page/玫瑰.md "wikilink")，靠家人賣「將軍花」維生，1990年11月19日病逝於此處。2002年7月1日[台中市政府指定為](../Page/台中市政府.md "wikilink")[歷史建築](../Page/台灣歷史建築.md "wikilink")，2010年11月開放為孫立人將軍紀念館，2018年9月改登錄為臺中市第一個[紀念建築](../Page/紀念建築_\(中華民國文化資產\).md "wikilink")。

孫立人始終思念著安徽老家，1988年，孫的舊屬潘德輝回鄉探親，孫委託他到安徽廬江代為祭祖。經過查訪發現，孫的祖墳早在文革時就被挖掉了，但潘還是代孫立人舉行了隆重的掃墓儀式，並拍照帶回。孫立人看到照片，激動地要向潘行跪拜大禮。1990年，潘再度受孫委託，回鄉洽商墓地遷葬事宜。返臺後，孫立人已處昏迷狀態，喃喃地説：“為什麼到今天才回來，我等你好久啊！”隨即又陷入昏迷。

### 妻

  - 龔夕濤是孫立人元配，1919年完婚，1979年逝，享壽八十歲。\[30\]
  - 張晶英（即[孫張清揚](../Page/孫張清揚.md "wikilink")，）\[31\]
  - 張美英，1928年12月19日出生於台灣[台南州](../Page/台南州.md "wikilink")[曾文郡](../Page/曾文郡.md "wikilink")[麻豆街](../Page/麻豆街.md "wikilink")（今
    [台南市](../Page/台南市.md "wikilink")[麻豆區](../Page/麻豆區.md "wikilink")），[護士出身](../Page/護士.md "wikilink")，和張晶英非親戚關係。張晶英因為一直未能生育，感到愧疚，於是主動安排與孫立人年紀有段差距的張美英嫁入作[妾](../Page/妾.md "wikilink")，膝下四子女皆為她所生。2016年1月15日卒於美國[北卡羅萊納州](../Page/北卡羅萊納州.md "wikilink")[杜克大學醫院](../Page/杜克大學醫院.md "wikilink")\[32\]。

### 子女

孫立人始終認為「中國安定，天下太平」，因此其四位子女即取其中“中”、“安”、“天”、“太”再加上“平”字做為他們的名字。孫立人出身北京[清華大學土木工程學士](../Page/清華大學.md "wikilink")，子女不少亦入讀[國立清華大學](../Page/國立清華大學.md "wikilink")。

  - 長女「孫中平」（1952年—）[國立清華大學核子工程系學士](../Page/國立清華大學.md "wikilink")、美國[康乃爾大學材料科學暨工程博士](../Page/康乃爾大學.md "wikilink")，曾任美商IBM台灣總經理、[台積電企業規劃組織副總經理](../Page/台積電.md "wikilink")，後擔任台積電行銷業務部門的產品定價暨事業流程處主管。\[33\]\[34\]
  - 長子「孫安平」（1955年—）國立清華大學物理學碩士，美國[南加州大學材料工程碩士](../Page/南加州大學.md "wikilink")。
    現任職於美國矽谷。\[35\]\[36\]
  - 次子「孫天平」（1957年—）美國[維拉諾瓦大學電子計算機工程碩士](../Page/維拉諾瓦大學.md "wikilink")，美國[威廉瑪麗學院工商管理碩士](../Page/威廉瑪麗學院.md "wikilink")。
    現任職於台資企業漢民科技（上海）有限公司，職務為區域總監。\[37\]\[38\]
  - 次女「孫太平」（1958年—）\[39\][國立清華大學](../Page/國立清華大學.md "wikilink")\[40\]化學系畢業，美國[杜克大學生物化學博士](../Page/杜克大學.md "wikilink")，美國杜克大學生物系教授。\[41\]\[42\]\[43\]
  - 義子「揭鈞」（1939年—）\[44\]\[45\]加拿大[滑鐵盧大學化學系的教授](../Page/滑鐵盧大學.md "wikilink")\[46\]。

## 相關條目

  - [中國遠征軍](../Page/中國遠征軍.md "wikilink")
  - [驼峰航线](../Page/驼峰航线.md "wikilink")
  - [飛虎隊](../Page/飛虎隊.md "wikilink")
  - [滇緬公路](../Page/滇緬公路.md "wikilink")
  - [新一军](../Page/新一军.md "wikilink")
  - [新六军](../Page/新六军.md "wikilink")
  - [杜聿明](../Page/杜聿明.md "wikilink")
  - [郭廷亮](../Page/郭廷亮.md "wikilink")
  - [麥瑞爾突擊隊](../Page/麥瑞爾突擊隊.md "wikilink")
  - [第75游騎兵團](../Page/第75游騎兵團.md "wikilink")
  - [美國陸軍遊騎兵部隊](../Page/美國陸軍遊騎兵部隊.md "wikilink")

## 註釋

  -

    孫立人始祖至他八代書香門第，尤其第四、第五代有大量[太學生與](../Page/太學生.md "wikilink")[舉人](../Page/舉人.md "wikilink")，在此只錄入直系列祖的功名。詳見引用文獻。\[47\]

## 参考文献

## 外部連結

  -
  - [臺中市文化資產處參訪民眾線上預約導覽](http://culturalvolunteer.tchac.taichung.gov.tw/public/1-2.asp?viid=1&proid=20120612201758)

  - [榮民文化網 -
    常勝將軍孫立人](http://lov.vac.gov.tw/Protection/Content.aspx?Para=51&Control=5)

  - [公共電視_孫立人三部曲](http://www.pts.org.tw/~web01/sun_li_jen/)

  - [飛躍四海軍事網 -
    孫立人將軍](https://web.archive.org/web/20100706073617/http://hifayay.com/seehi/phparticle/article.php/122)

  - [義子揭鈞紀念孫立人之網頁](http://www.science.uwaterloo.ca/~cchieh/sun/)

  - [父親那場永不止息的戰爭 -
    王明珂](https://web.archive.org/web/20140810111716/http://www.infzm.com/content/45883)

  - [麥瑞爾突擊隊](../Page/麦瑞尔突击队.md "wikilink")

  - [美國75遊騎兵團](../Page/第75突擊兵團.md "wikilink")

  - [美國遊騎兵](../Page/美國陸軍遊騎兵部隊.md "wikilink")

  - [中國評論新聞：檔案解密：美國策劃台灣軍隊“政變”始末](http://www.chinareviewnews.com/doc/1003/6/6/3/100366347.html?coluid=6&kindid=26&docid=100366347)

  - [《人民日报》历史上的今天，11月19日](http://www.people.com.cn/GB/historic/1119/5277.html)

  - [再談孫立人－1984的回顧](http://taiwantt.org.tw/books/cryingtaiwan4/content/1980/19840716-9.htm)

  - [孫立人的歷史回顧](http://taiwantt.org.tw/books/cryingtaiwan4/content/1980/19880325b-95.htm)

  - [孫立人鎮寺偷兒止步](https://web.archive.org/web/20090223145206/http://www.libertytimes.com.tw/r-s/r-s040223-2.htm)

  - [美國杜克大學孫太平教授英文簡介](http://fds.duke.edu/db/aas/Biology/faculty/tps)

  - [风范大国民：玫瑰将军孙立人](http://v.ifeng.com/his/200907/393972bf-e7b4-40e1-bc79-afd20d411d51.shtml)

  - [臺灣保三警察隊歷史](https://web.archive.org/web/20110625012946/http://www.tcopp.gov.tw/index_m01-2.asp)部份官兵又轉回原本的稅警

  - [孫立人將軍忘年交為他平反為他愁](https://archive.is/20140508121436/http://www.udn.com/2014/4/19/NEWS/NATIONAL/NAT1/8622244.shtml?ch=rss_politics)

  - [孫立人「佔用」興大校地爭議始末 –
    中興大學鹿鳴文化資產中心](http://deer.nchu.edu.tw/2009/08/%E5%AD%AB%E7%AB%8B%E4%BA%BA%E3%80%8C%E4%BD%94%E7%94%A8%E3%80%8D%E8%88%88%E5%A4%A7%E6%A0%A1%E5%9C%B0%E7%88%AD%E8%AD%B0%E5%A7%8B%E6%9C%AB/)

{{-}}   |- |colspan="3" style="text-align:
center;"|**[中華民國](../Page/中華民國.md "wikilink")[國防部](../Page/中華民國國防部.md "wikilink")**


[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國陸軍總司令](../Category/中華民國陸軍總司令.md "wikilink")
[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:孫立人兵變案](../Category/孫立人兵變案.md "wikilink")
[Category:中国反共主义者](../Category/中国反共主义者.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:清華大學校友](../Category/清華大學校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:普渡大學校友](../Category/普渡大學校友.md "wikilink")
[Category:弗吉尼亚军事学院校友](../Category/弗吉尼亚军事学院校友.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:台灣戰後安徽移民](../Category/台灣戰後安徽移民.md "wikilink")
[Category:中華民國總統府參軍長](../Category/中華民國總統府參軍長.md "wikilink")
[Category:中國遠征軍人物](../Category/中國遠征軍人物.md "wikilink")
[Category:青天白日勳章獲得者](../Category/青天白日勳章獲得者.md "wikilink")
[Category:雲麾勳章獲得者](../Category/雲麾勳章獲得者.md "wikilink")
[Category:功績勳章獲得者](../Category/功績勳章獲得者.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:廬江人](../Category/廬江人.md "wikilink")
[L立](../Category/孫姓.md "wikilink")

1.

2.  光緒十一年(1885)《續修廬州府志》記載：「舒城孫氏，原出休寧縣，遷舒城東鄉三河鎮南岸者，自貢生正仁始。其後子孫以科第起家，蔚為望族。」

3.

4.

5.

6.  （服部卓四郎：《大东亚战争全史》，第3卷，1051页）

7.

8.  [史上最无聊的杜撰孙立人活埋日俘的真相考证](http://culture.china.com/zh_cn/history/lead/11022885/20090421/15441710.html)
    2009-04-21.

9.  [历史必须还原-谈孙立人将军活埋1200个日本俘虏真假2014.1.15](http://blog.sina.com.cn/s/blog_c1b9a8650101dkzc.html)

10. English Wikipedia：Sun Li-jen (Rommel of the East)

11.

12.
13. [老兵不死，只是悄然隱去](http://www.open.com.hk/content.php?id=1923)

14.
15.
16. 鄭錦玉：《碧海鉤沉回憶思錄：孫立人將軍功業與冤案真相紀實》

17.
18.
19. [陳-{布}-雷等編著](../Page/陳布雷.md "wikilink")，《蔣介石先生年表》，台北：[傳記文學出版社](../Page/傳記文學.md "wikilink")，1978年6月1日

20.
21.
22.

23.
24. （美聯社臺北廿八日訊）《中央日報》、《聯合報》，1954年10月29日
    （美聯社臺北七日訊）《聯合報》，1955年4月8日

25.
26.
27. 香港《七十年代》，「孫立人在台兵變經過」，1974年9月

28. 《新新聞周刊》193期，1990年11月19日-25日

29.

30. 沈克勤著作《孫立人傳下集》1004頁

31.

32.

33. [傑出校友孫中平](http://www.nthu.edu.tw/intro/aboutnthu15-p49.php)
    。[國立清華大學](../Page/國立清華大學.md "wikilink")

34. [孫立人的女兒台灣IBM第一女將軍](http://www.businessweekly.com.tw/KArticle.aspx?id=14055)

35.
36.

37.

38.

39.
40.
41.
42.

43.

44.

45. <http://www.science.uwaterloo.ca/~cchieh/sun/cnachang1.html>

46.

47.