**孫翠鳳**（），[台灣](../Page/台灣.md "wikilink")[女性](../Page/女性.md "wikilink")[藝術家](../Page/藝術家.md "wikilink")，[歌仔戲](../Page/歌仔戲.md "wikilink")[演員](../Page/演員.md "wikilink")、工生行（[武生](../Page/武生.md "wikilink")、[小生等](../Page/小生.md "wikilink")），生於[台灣](../Page/台灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")，祖籍[河北省](../Page/河北省.md "wikilink")[天津](../Page/天津.md "wikilink")[吳橋縣](../Page/吳橋縣.md "wikilink")，是台灣**[明華園](../Page/明華園.md "wikilink")**戲劇總團當家台柱，也是戲劇藝術界難得一見允文允武、亦[生亦](../Page/生.md "wikilink")[旦的演員](../Page/旦.md "wikilink")。對於歌仔戲推廣及創新工作均成果卓著，為臺灣歌仔戲知名小生之一。

## 家族

  - 父親：[孫貴](../Page/孫貴.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[京劇](../Page/京劇.md "wikilink")、[歌仔戲演員](../Page/歌仔戲.md "wikilink")。
  - 母親：[陳玉桂](../Page/陳玉桂.md "wikilink")，台灣早期歌仔戲團「登興社」二女兒，也是明華園創辦人[陳明吉夫人](../Page/陳明吉.md "wikilink")[陳水涼的妹妹](../Page/陳水涼.md "wikilink")。
  - 丈夫：[陳勝福](../Page/陳勝福.md "wikilink")，明華園戲劇總團團長。
  - 女兒：[陳昭婷](../Page/陳昭婷.md "wikilink")、[陳昭賢](../Page/陳昭賢.md "wikilink")，明華園新一代演員。

## 經歷

  - 1930年，父親孫貴隨團來台定居。由於早期從事歌仔戲演出生活困苦，孫翠鳳的父母舉家遷入[台北](../Page/台北.md "wikilink")，從此打算專心栽培孩子，讓他們與歌仔戲絕緣，因此自幼在台北成長的孫翠鳳，對於歌仔戲的接觸不深，連台語也是說不好。

<!-- end list -->

  - 1984年，嫁給表哥[陳勝福](../Page/陳勝福.md "wikilink")，正式進入明華園大家族而與歌仔戲結下不解之緣。一次因緣際會，劇團臨時缺一位不需要開口的龍套丫環，孫翠鳳被推上舞台，自此愛上站在舞台的感覺。接著跑了三年[龍套](../Page/龍套.md "wikilink")，決定成為正式的歌仔戲演員。當年開始學習[閩南話發音與歌仔戲基本功開始](../Page/閩南話.md "wikilink")，以驚人的毅力克服身體和年齡的障礙，從跑龍套到各種角色的試煉。
  - 1986年，在明華園《劉全進瓜》一劇中首度擔綱女主角後，表現突出的她逐漸接演更多樣的角色，並漸漸成為劇團的一線小生。
  - 1988年，終於靠著自身的努力與堅強的意志力成為舞台上耀眼的主角明星。
  - 1996年，獲選[中華民國十大傑出青年](../Page/中華民國十大傑出青年.md "wikilink")。
  - 1997年，榮獲[紐約美華亞洲最傑出藝人金獎](../Page/紐約.md "wikilink")。
  - 2008年，擔任[中華民國總統府](../Page/中華民國總統府.md "wikilink")[藝文界](../Page/藝文界.md "wikilink")[國策顧問](../Page/國策顧問.md "wikilink")，並於2012年卸任。

## 獎項

  - 1996年，獲選「[十大傑出青年](../Page/十大傑出青年.md "wikilink")」
  - 1997年，獲得「[亞洲最傑出藝人獎](../Page/亞洲最傑出藝人獎.md "wikilink")」

## 參與明華園演出年表

[Taiwanese_opera2.JPG](https://zh.wikipedia.org/wiki/File:Taiwanese_opera2.JPG "fig:Taiwanese_opera2.JPG")演出的[歌仔戲](../Page/歌仔戲.md "wikilink")\]\]

  - 1986年 《[刘全进瓜](../Page/刘全进瓜.md "wikilink")》　　

<!-- end list -->

  - 1988年 《[红尘菩提](../Page/红尘菩提.md "wikilink")》

<!-- end list -->

  - 1989年 《[财神下凡](../Page/财神下凡.md "wikilink")》、《真命天子》

<!-- end list -->

  - 1990年 《[济公活佛](../Page/济公活佛.md "wikilink")》

<!-- end list -->

  - 1992年 《[逐鹿天下](../Page/逐鹿天下.md "wikilink")》

<!-- end list -->

  - 1993年 《[李靖斩龙](../Page/李靖斩龙.md "wikilink")》、《界牌关传说》

<!-- end list -->

  - 1994年 《[薛丁山传奇](../Page/薛丁山传奇.md "wikilink")》、《鸳鸯枪》

<!-- end list -->

  - 1996年 《[燕云十六州](../Page/燕云十六州.md "wikilink")》

<!-- end list -->

  - 1999年 《[武松打虎](../Page/武松打虎.md "wikilink")》

<!-- end list -->

  - 2001年
    《[乘愿再来](../Page/乘愿再来.md "wikilink")》、《[白蛇传](../Page/白蛇传.md "wikilink")》

<!-- end list -->

  - 2002年
    《[狮子王](../Page/狮子王.md "wikilink")》、《[鸭母王](../Page/鸭母王.md "wikilink")》

<!-- end list -->

  - 2003年 《[剑神吕洞宾](../Page/剑神吕洞宾.md "wikilink")》

<!-- end list -->

  - 2004年 《[韩湘子](../Page/韩湘子.md "wikilink")》

<!-- end list -->

  - 2005年 《王子复仇记》

<!-- end list -->

  - 2006年 《何仙姑》上下集

<!-- end list -->

  - 2007年 《[超炫白蛇傳](../Page/超炫白蛇傳.md "wikilink")》（全新改版）

<!-- end list -->

  - 2009年
    《[猫神](../Page/猫神.md "wikilink")》、《[海神家族](../Page/海神家族.md "wikilink")》

<!-- end list -->

  - 2010年 《[曹國舅](../Page/曹國舅.md "wikilink")》

<!-- end list -->

  - 2011年
    《[蓬萊仙島](../Page/蓬萊仙島.md "wikilink")》、《[火鳳凰](../Page/火鳳凰.md "wikilink")》
  - 2012年
    《[劍神呂洞賓-巡迴](../Page/劍神呂洞賓-巡迴.md "wikilink")》、《[蓬萊仙島-巡迴](../Page/蓬萊仙島-巡迴.md "wikilink")》
  - 2013年
    《[吆嘍正傳](../Page/吆嘍正傳.md "wikilink")》、《[媽祖](../Page/媽祖.md "wikilink")》
  - 2014年 《[吆嘍正傳-首部曲-巡迴](../Page/吆嘍正傳-首部曲-巡迴.md "wikilink")》
  - 2015年 《流星》(青春劇作)
  - 2016年 《散戲》(文學跨界劇作)
  - 2016年 《四兩皇后》
  - 2017年 《愛的波麗路》(文學跨界劇作)
  - 2017年 《王子復仇記之龍抬頭》(舊作重編之新戲-上集)
  - 2018年 《俠貓》(文學跨界劇作)
  - 2018年 《王子復仇記之龍逆麟》(舊作重編之新戲-下集)
  - 2019年 《龍城爭霸》(吆嘍正傳續集-二部曲)、《皇上有喜》

## 出演电视歌仔戏

  - 1985年 《[父子情深](../Page/父子情深.md "wikilink")》

<!-- end list -->

  - 1987年 《[千里姻缘路](../Page/千里姻缘路.md "wikilink")》

<!-- end list -->

  - 1988年 《[一见情](../Page/一见情.md "wikilink")》

<!-- end list -->

  - 1989年 《[铁胆英豪](../Page/铁胆英豪.md "wikilink")》

<!-- end list -->

  - 1995年 《[皇甫少华与孟丽君](../Page/皇甫少华与孟丽君.md "wikilink")》

<!-- end list -->

  - 2012年 《[菩提禪心](../Page/菩提禪心.md "wikilink")》之木槍刺足

<!-- end list -->

  - 2013年
      - 《[菩提禪心](../Page/菩提禪心.md "wikilink")》
          - 六牙象王
          - 內修德外行善
          - 為善得福
          - 知因識果
          - 蓮華
          - 孤兒獻金
          - 度化獵戶
          - 報恩牛
          - 以沙供佛
          - 九代同居

<!-- end list -->

  - 2014年
      - 《[菩提禪心](../Page/菩提禪心.md "wikilink")》
          - 王舍城由來
          - 拘那羅王子的眼睛
          - 億萬里
          - 神射手王子
          - 直心無我一儒童
          - 離婆多遇鬼
          - 心火

<!-- end list -->

  - 2015年
      - 《[菩提禪心](../Page/菩提禪心.md "wikilink")》
          - 大樹上的彩帶
          - 掌拳公主
          - 五世母子
          - 摩男的選擇
          - 以愛化仇
          - 仁慈的長壽王
          - 五不死薄拘羅

<!-- end list -->

  - 2016年
      - 《[菩提禪心](../Page/菩提禪心.md "wikilink")》
          - 雪山仙人
          - 秀才狀元夢
          - 老鞋匠的國王夢
          - 日難王棄國學道
          - 心魔千指串
          - 國王身施為奴
          - 魚籃女度馬郎
  - 2017年
      - 菩提禪心
          - 高僧傳-弘一法師
          - 貧者之歌
          - 宰相之子
          - 高僧傳-安世高大師
          - 高僧傳-窺基法師

## 出演电视剧

| 年份                             | 頻道                                             | 劇名                                              | 飾演                               |
| ------------------------------ | ---------------------------------------------- | ----------------------------------------------- | -------------------------------- |
| 1992年                          | [華視](../Page/華視.md "wikilink")                 | 《[嘉庆君遊台湾](../Page/嘉庆君遊台湾.md "wikilink")》        | 高世國（嘉慶君）                         |
| 1997年                          | [中視](../Page/中視.md "wikilink")                 | 《[大巡按蕃薯官](../Page/大巡按蕃薯官.md "wikilink")》玫瑰玫瑰我愛你 | 玫瑰賊(金仲文)                         |
| [華視](../Page/華視.md "wikilink") | 《[施公奇案](../Page/施公奇案_\(台灣\).md "wikilink")》法外情 | 唐圣南(李蝶飛)                                        |                                  |
| [中視](../Page/中視.md "wikilink") | 《[大姐当家](../Page/大姐当家.md "wikilink")》           | 葉英淑                                             |                                  |
| 1998年                          | [中視](../Page/中視.md "wikilink")                 | 《[女巡按](../Page/女巡按.md "wikilink")》              | 包秀秀(文必正) |-)                     |
| 2002年                          | [民視](../Page/民視.md "wikilink")                 | 《[移山倒海樊梨花](../Page/移山倒海樊梨花.md "wikilink")》      | [樊梨花](../Page/樊梨花.md "wikilink") |
| 2005年                          | [台視](../Page/台視.md "wikilink")                 | 《[海誓山盟](../Page/海誓山盟.md "wikilink")》            | 葉明珠                              |
| 2006年9月12日                     | [公共電視](../Page/公共電視.md "wikilink")             | 《[祖師爺的女兒](../Page/祖師爺的女兒.md "wikilink")》        | 洪繡屏                              |

## 出演电影

佳譽時期：

| 年份         | 片名                                         | 飾演 |
| ---------- | ------------------------------------------ | -- |
| |《大地勇士》    |                                            |    |
| |《人肉战车》    |                                            |    |
| |《女学生与机关枪》 |                                            |    |
| 1987年      | 《金水婶》                                      |    |
| 1988年      | 《桂花巷》                                      |    |
| 1983年      | 《进攻要塞地》                                    |    |
| 1994年      | 《我的一票选总统》                                  |    |
| 2007年      | 《[海之傳說－媽祖](../Page/海之傳說－媽祖.md "wikilink")》 | 配音 |
| |《青春歌仔》    | 客串                                         |    |
| 2016年      | 《[人生按個讚](../Page/人生按個讚.md "wikilink")》     |    |

## 出演舞台剧

  - 1990年 兰陵剧坊《[戏蚂蚁](../Page/戏蚂蚁.md "wikilink")》

<!-- end list -->

  - 1991年 黄梅调舞台剧《梁山伯与祝英台》

<!-- end list -->

  - 2009年 舞台剧《海神家族》

<!-- end list -->

  - 2016年 舞台劇 《散戲》

<!-- end list -->

  - 2017年 舞台劇 《愛的波麗路》

## 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行</p></th>
<th style="text-align: left;"><p>年月</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>勇敢的查某子（新歌+精選）</p></td>
<td style="text-align: left;"><p>華納唱片</p></td>
<td style="text-align: left;"><p>2004-12</p></td>
<td style="text-align: left;"><ol>
<li>勇敢的查某子</li>
<li>自在消遙遊</li>
<li>紅塵報恩</li>
<li>歡喜入洞房</li>
<li>酒酒酒</li>
<li>一剎那</li>
<li>五月五</li>
<li>烈焰戰火</li>
<li>往事歸塵埋</li>
<li>心慌意亂</li>
<li>泰山崩</li>
<li>心境自然清</li>
<li>笑傲紅塵</li>
<li>鳥啊展翅</li>
</ol></td>
</tr>
</tbody>
</table>

## 著作

  - 祖師爺的女兒──孫翠鳳的故事 ／作者：孫翠鳳口述、黃秀錦著 ／出版社：時報文化 ／出版日期：2000年／ ISBN
    9571331023\[1\]\[2\]\[3\]

## 參考資料

## 外部連結

  - [台灣國立傳統藝術中心孫翠鳳資料網頁](https://web.archive.org/web/20070928031003/http://kn.ncfta.gov.tw/opera/HTML/talent82.htm)

  - [明華園-孫翠鳳官方部落格](http://blog.twopera.com/sun/)

  - [翠鳳小築](http://www.suncuifeng.com)

  -
  -
  -
  -
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:戲曲演員](../Category/戲曲演員.md "wikilink")
[Category:台灣歌仔戲演員](../Category/台灣歌仔戲演員.md "wikilink")
[Category:臺北市立士林高級商業職業學校校友](../Category/臺北市立士林高級商業職業學校校友.md "wikilink")
[Category:天津裔台灣人](../Category/天津裔台灣人.md "wikilink")
[C翠](../Category/孫姓.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:明華園](../Category/明華園.md "wikilink")

1.  [黃秀錦，《祖師爺的女兒─孫翠鳳的故事》，時報出版](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=rtpe\(SellItems\)&id=PE0201)
2.  [公共電視_祖師爺的女兒](http://web.pts.org.tw/~web01/Daughter/)
3.  [公視網路商城-祖師爺的女兒](http://shop.pts.org.tw/Module_J.aspx?xxx=21039001D)