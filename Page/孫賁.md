**孫賁**（），[字](../Page/表字.md "wikilink")**伯陽**，[吳郡](../Page/吳郡.md "wikilink")[富春](../Page/富春.md "wikilink")（今[杭州](../Page/杭州.md "wikilink")[富陽](../Page/富陽.md "wikilink")）人。[孫堅兄](../Page/孫堅.md "wikilink")[孫羌之子](../Page/孫羌.md "wikilink")，[東漢末東吳勢力將領](../Page/東漢.md "wikilink")。

## 生平

孫賁父母早死，弟弟[孫輔還是嬰孩](../Page/孫輔.md "wikilink")，孫賁獨力養育孫輔，大家都很親愛對方。

孫賁初時是郡督郵守長，後來孫堅於[長沙起兵](../Page/長沙.md "wikilink")，孫賁就棄官跟隨孫堅。後來孫堅戰死，孫賁統領孫堅部眾扶送靈柩。

後來[袁術遷移到](../Page/袁術.md "wikilink")[壽春](../Page/壽春.md "wikilink")，孫賁前去依附。[袁紹任命會稽人](../Page/袁紹.md "wikilink")[周昂為九江太守](../Page/周昂.md "wikilink")，袁術大怒，派遣孫賁攻擊周昂，並於[陰陵擊破他](../Page/陰陵.md "wikilink")。袁術於是表孫賁領[豫州刺史](../Page/豫州.md "wikilink")，轉丹楊都尉，行征虜將軍，參與討平[山越](../Page/山越.md "wikilink")。袁術佔據壽春後，揚州刺史[劉繇遷至](../Page/劉繇.md "wikilink")[曲阿](../Page/曲阿.md "wikilink")，並驅逐孫賁，孫賁惟有退還[歷陽](../Page/歷陽.md "wikilink")。不久，袁術命孫賁與[吳景聯手攻擊劉繇部將](../Page/吳景.md "wikilink")[樊能](../Page/樊能.md "wikilink")、[張英等人](../Page/張英_\(東漢\).md "wikilink")，未能擊破。孫策後來到江東，幫助孫賁等人擊破張英、樊能，並且追擊劉繇，劉繇敗走[豫章](../Page/豫章.md "wikilink")。

[建安二年](../Page/建安_\(東漢\).md "wikilink")（197年），袁術於壽春稱帝，當時孫策遣回孫賁和吳景向袁術報告。袁術任命孫賁為[九江太守](../Page/九江.md "wikilink")，孫賁不從，拋棄妻兒回到江南。孫賁到時，孫策已平定[吳郡和](../Page/吳郡.md "wikilink")[會稽](../Page/會稽.md "wikilink")，並與孫策在建安四年（199年）征伐[廬江太守](../Page/廬江.md "wikilink")[劉勳及](../Page/刘勋_\(东汉\).md "wikilink")[江夏太守](../Page/江夏.md "wikilink")[黃祖](../Page/黃祖.md "wikilink")，回軍時知道劉繇病死，於是又平定豫章，由孫賁領豫章太守，後來封都亭侯。建安十三年（208年），[劉隱奉詔拜孫賁為征虜將軍](../Page/劉隱_\(三國\).md "wikilink")，繼續領豫章太守。曹操平定荆州时，孙贲畏惧，想送儿子给曹操为质，被[朱治劝止](../Page/朱治.md "wikilink")。在職第十一年逝世。

## 親屬

### 父親

  - [孫羌](../Page/孫羌.md "wikilink")，孫堅兄。

### 叔父

  - [孫堅](../Page/孫堅.md "wikilink")，長沙太守，隨漢軍討伐黃巾賊，後再討董卓時斬殺華雄擊退呂布，率先進入洛陽。子孫權稱帝追尊為武烈皇帝。
  - [孫靜](../Page/孫靜.md "wikilink")，孫堅弟，孫堅死後，跟隨孫策繼續打江東，打江東時孫策無法攻克城池，孫靜獻計讓孫策攻下城池。後江東平定在家病逝。

### 兄弟

  - [孫輔](../Page/孫輔.md "wikilink")，孫賁弟，東漢末年東吳勢力將領。

### 堂弟

  - [孫策](../Page/孫策.md "wikilink")，孫堅長子，領兵平定江東，奠下東吳基礎。弟孫權稱帝追尊為長沙桓王。
  - [孫權](../Page/孫權.md "wikilink")，孫堅次子，孫策死後繼而執掌江東，222年拜吳王，229年稱帝。
  - [孫翊](../Page/孫翊.md "wikilink")，孫堅三子，偏將軍領丹楊太守，204年遭部下邊鴻所殺害。
  - [孫匡](../Page/孫匡.md "wikilink")，孫堅四子，孫堅死後承襲烏程侯爵位，孫策打下江東後，曹操聞之便以弟之女嫁配給孫策之幼弟。

### 子女

  - [孫鄰](../Page/孫鄰.md "wikilink")，孫賁長子，三國時東吳將領，官至威遠將軍，鎮守夏口一帶。
  - [孫安](../Page/孫安_\(孫賁子\).md "wikilink")
  - [孫熙](../Page/孫熙.md "wikilink")
  - [孫績](../Page/孫績.md "wikilink")
  - 孫氏，嫁給[曹操的兒子](../Page/曹操.md "wikilink")[曹彰](../Page/曹彰.md "wikilink")，可能有子[曹楷](../Page/曹楷.md "wikilink")。

### 孫代

  - [孫震](../Page/孙震_\(三国\).md "wikilink")，孫鄰子，無難督。[晉滅吳之戰時任護軍](../Page/晉滅吳之戰.md "wikilink")\[1\]，為[周浚等諸晉將擊敗](../Page/周浚.md "wikilink")，與[張悌一同戰死](../Page/張悌.md "wikilink")。\[2\]
  - [孫苗](../Page/孫苗.md "wikilink")，孫鄰子，夏口沔中督，父親逝後襲其爵位。
  - [孫旅](../Page/孫旅.md "wikilink")，孫鄰子。
  - [孫述](../Page/孫述.md "wikilink")，孫鄰子，曾為武昌督，平荊州事。
  - [孫諧](../Page/孫諧.md "wikilink")，孫鄰子，城門校尉。
  - [孫歆](../Page/孫歆.md "wikilink")，孫鄰子，樂鄉督。晉滅吳之時於樂鄉為晉將[周旨所俘後處死](../Page/周旨.md "wikilink")。\[3\]
  - [孙寒华](../Page/孙寒华.md "wikilink")，女道士。《真诰》记载为孙贲孙女，父或为孙熙。

### 曾孙

  - [孙惠](../Page/孙惠.md "wikilink")，[东晋临湘县公](../Page/东晋.md "wikilink")。未载其父及祖父为何人。

## 參考資料

  - 《三國志·吳書·宗室傳》
  - 《三國志·吳書·孫破虜討逆傳》

[S孫](../Category/東漢軍事人物.md "wikilink")
[S孫](../Category/东漢太守.md "wikilink")
[B賁](../Category/東吳宗室.md "wikilink")
[B賁](../Category/孫姓.md "wikilink")

1.  干寶《晉紀》作護軍，《晉書·王渾傳》作大將軍，今從《晉紀》
2.  《晉書·王渾傳》：「吳丞相張悌、大將軍孫震等率眾數萬指城陽，渾遣司馬孫疇、揚州刺史周浚擊破之，臨陣斬二將，及首虜七千八百級，吳人大震。」
3.  《資治通鑑·卷八十一》：「（周）旨等發伏兵隨歆軍而入，歆不覺，直至帳下，虜歆而還。」