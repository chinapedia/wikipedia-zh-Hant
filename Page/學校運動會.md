[Undoukai.jpg](https://zh.wikipedia.org/wiki/File:Undoukai.jpg "fig:Undoukai.jpg")
**學校運動會**，是[學校教育生活的一部份](../Page/學校教育.md "wikilink")，也是[體育教育活動的一種](../Page/體育.md "wikilink")，可分為**陸運會**和**水運會**。通常每年舉行一次，校內運動會通常記載於[時間表之內](../Page/時間表.md "wikilink")，而獲獎成績[印刷於每年](../Page/印刷.md "wikilink")[校刊之內](../Page/校刊.md "wikilink")。而在[日本](../Page/日本.md "wikilink")，學校運動會（体育祭）更是教育重要的一環。即使在中国大陆，校运会也日渐成重要的学校活动，老师借助这项活动来培养学生之间的竞争意识。

此外，還有[分區學校運動會以及](../Page/分區學校運動會.md "wikilink")[聯校運動會](../Page/聯校運動會.md "wikilink")，讓不同學校的學生[運動員互相交流切磋](../Page/運動員.md "wikilink")。

## 場地

  - [日本](../Page/日本.md "wikilink")：陸運會多於學校[運動場](../Page/運動場.md "wikilink")（空間較小的學校則會租用學校附近地區的運動場）舉行。
  - [香港](../Page/香港.md "wikilink")，由於只有極少學校有自己的運動場，所以通常會租用[康樂及文化事務署屬下的運動場來舉行](../Page/康樂及文化事務署.md "wikilink")。[學界體育比實成績較佳的中學可以選擇較佳運動場](../Page/香港學界體育聯會.md "wikilink")（例︰[灣仔運動場](../Page/灣仔運動場.md "wikilink")）作陸運會主場。

## 內容及目的

[Sports-Fest1.jpg](https://zh.wikipedia.org/wiki/File:Sports-Fest1.jpg "fig:Sports-Fest1.jpg")
[UndokaiMarch.jpg](https://zh.wikipedia.org/wiki/File:UndokaiMarch.jpg "fig:UndokaiMarch.jpg")
運動會可以增加學生對運動的興趣，校內運動會可培養學生對社或班級的[歸屬感和合作精神](../Page/歸屬感.md "wikilink")，訓練時亦可鍛鍊體格。而擔任工作人員的學生亦可學到[課堂以外的知識](../Page/課堂.md "wikilink")。在有「社」（house）的學校裡，運動員會獲發一片以自己社色為底色的[布](../Page/布.md "wikilink")，上面有運動員[編號或](../Page/編號.md "wikilink")[姓名](../Page/姓名.md "wikilink")，比賽時需佩戴於身上，並需穿著代表自己所屬的社的服裝參加比賽。

此外，運動會中還有其他表演和展覽，讓學生能夠發揮多方面才能，並為會場增添歡樂氣氛，例如[啦啦隊歌舞](../Page/啦啦隊.md "wikilink")[表演與](../Page/表演.md "wikilink")[比賽](../Page/啦啦隊比賽.md "wikilink")、場地[佈置比賽等](../Page/佈置.md "wikilink")，啦啦隊以[歌唱](../Page/歌唱.md "wikilink")、[舞蹈](../Page/舞蹈.md "wikilink")、喊[口號的方式為所屬的社或班級打氣](../Page/口號.md "wikilink")，他們會穿上代表自己社成班級[顏色的](../Page/顏色.md "wikilink")[衣服](../Page/衣服.md "wikilink")，有些會佩戴其他代表所屬的社或班級的[飾物](../Page/飾物.md "wikilink")。這些活動令不擅長體育項目的學生同樣能投入活動，發揮所長，亦可培養學生的[團隊精神](../Page/團隊精神.md "wikilink")。不少學校規定低年級學生必須參加啦啦隊，並規定他們必須於場內用膳。另外有些學校亦有[步操表演和儀式](../Page/步操.md "wikilink")，培養學生的[紀律](../Page/紀律.md "wikilink")。

除了學生參與的比賽外，有些學校有專為教職員、家長而設的比賽，也有師生、親子競賽，促進師生與[父母子女之間的交流](../Page/父母.md "wikilink")。[日本的學校非常重視運動會](../Page/日本.md "wikilink")，表演節目在正式會前常經過相當時日之排練，以展現學校校風。此外[日本學校有一項特別習俗](../Page/日本.md "wikilink")，即是於[體操及各項競技時](../Page/體操.md "wikilink")，半強迫[男子赤裸上身參加](../Page/男子.md "wikilink")，此舉一方面可向外界展示學校之雄威、[紀律](../Page/紀律.md "wikilink")，也可讓男子互相比較，使其平日主動鍛練體格，如訓練[腹肌](../Page/腹肌.md "wikilink")、皮膚曬成小麥色等。

## 紀律

有些學校規定全校[學生必須出席陸運會](../Page/學生.md "wikilink")，甚至規定每人至少要參加一項[田徑](../Page/田徑.md "wikilink")[比賽](../Page/競賽.md "wikilink")，無故[缺席者視作](../Page/缺席.md "wikilink")[曠課](../Page/曠課.md "wikilink")。由學生[風紀及](../Page/風紀.md "wikilink")[教師負責](../Page/教師.md "wikilink")[紀律](../Page/紀律.md "wikilink")，把守[運動場出口](../Page/運動場.md "wikilink")，活動期間出外者要出示[學生証](../Page/學生証.md "wikilink")，作[登記簽名](../Page/登記.md "wikilink")。而水運會則未必要所有學生出席及參加。

## 活動項目

[SWUSTStadium.jpg](https://zh.wikipedia.org/wiki/File:SWUSTStadium.jpg "fig:SWUSTStadium.jpg")运动会开幕式\]\]
[小學及部份初中的運動會以競技遊戲項目為主](../Page/小學.md "wikilink")，如[二人三足](../Page/二人三足.md "wikilink")、[拔河](../Page/拔河.md "wikilink")、[繞障礙走](../Page/繞障礙走.md "wikilink")、拋[豆袋](../Page/豆袋.md "wikilink")、擲壘球等。[初中](../Page/初中.md "wikilink")、[高中及大學則以](../Page/高中.md "wikilink")[田徑項目為主](../Page/田徑.md "wikilink")，如[推鉛球](../Page/推鉛球.md "wikilink")、[接力賽](../Page/接力賽.md "wikilink")、[跳遠](../Page/跳遠.md "wikilink")、[跳高](../Page/跳高.md "wikilink")、[跨欄](../Page/跨欄.md "wikilink")、[標槍等](../Page/標槍.md "wikilink")。

## 安全

[UndokaiKibasen.jpg](https://zh.wikipedia.org/wiki/File:UndokaiKibasen.jpg "fig:UndokaiKibasen.jpg")
由於陸運會部份比賽項目可能對參賽者或工作人員構成危險，學校常會於場地制訂一些規則保障選手安全，例如[標槍比賽場地部份範圍禁止進入](../Page/標槍.md "wikilink")，徑賽進行時非參賽者不得在跑道上走動等。運動會進行時，[運動場亦會設置](../Page/運動場.md "wikilink")[救護站](../Page/救護站.md "wikilink")，為[受傷或不適的人士進行急救](../Page/受傷.md "wikilink")。

在[日本](../Page/日本.md "wikilink")，學校進行騎馬戰或組體操等項目時，一般學校規定男生必須光裸上半身且赤腳參加，以避免衣物之拉扯危險，也可展示自身之肌肉，而女生亦要赤腳參賽。

## 相關

  - [運動會](../Page/運動會.md "wikilink")
  - [起步槍](../Page/起步槍.md "wikilink")
  - [司令員](../Page/司令員.md "wikilink")
  - [司令台](../Page/司令台.md "wikilink")
  - [攝影師](../Page/攝影師.md "wikilink")
  - [獎杯](../Page/獎杯.md "wikilink")

## 參考資料

[Category:學校](../Category/學校.md "wikilink")
[Category:运动会](../Category/运动会.md "wikilink")