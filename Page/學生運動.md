**學生運動** ，簡稱**學運**，亦稱**學潮**
，是一種以[學生為主要成員的](../Page/學生.md "wikilink")[社會運動](../Page/社會運動.md "wikilink")。这种运动一般是自發的（也有可能是受[新聞](../Page/新聞.md "wikilink")[媒體](../Page/媒體.md "wikilink")、[社論](../Page/社論.md "wikilink")、社會意見、[專欄作家](../Page/專欄作家.md "wikilink")、[電台名嘴](../Page/電台.md "wikilink")、[意見領袖等所引導](../Page/意見領袖.md "wikilink")）、为集體建立某種[社會](../Page/社會.md "wikilink")[價值](../Page/價值.md "wikilink")[文化判斷](../Page/文化.md "wikilink")，从而引發行動方向、回應或引發社會運動的大潮流。

學運目標有為[學生組織的](../Page/學生組織.md "wikilink")[正名運動](../Page/正名.md "wikilink")、聲援被捕的政治人物、反[殖民主義](../Page/殖民主義.md "wikilink")、反[一黨專政等](../Page/一黨專政.md "wikilink")。

## 亞洲重大事件

### 中國大陸

  - ;漢朝

<!-- end list -->

  - [黨錮之禍](../Page/黨錮之禍.md "wikilink")

<!-- end list -->

  - ;明朝

<!-- end list -->

  - [东林党爭](../Page/东林党爭.md "wikilink")：是一群朝廷退職的官吏任教的「[東林書院](../Page/東林書院.md "wikilink")」師徒們，與[閹黨等人的政治鬥爭](../Page/閹黨.md "wikilink")

<!-- end list -->

  - ;清朝

<!-- end list -->

  - [公车上书](../Page/公车上书.md "wikilink")

<!-- end list -->

  - ;中華民國（大陆时期）

<!-- end list -->

  - [五四运动](../Page/五四运动.md "wikilink")：指1919年的5月4日，是一場發生於中國北京、以青年學生為主的學生運動。
  - [三一八慘案](../Page/三一八慘案.md "wikilink")
  - [一二九运动](../Page/一二九运动.md "wikilink")：指1935年12月9日中國青年發起的反分裂、反割據愛國運動，要求保全中國領土的完整。
  - [反苏运动](../Page/反苏运动.md "wikilink")：指1946年2月因中华民国国民政府官员张莘夫接收抚顺煤矿时被苏联保护的东北民主联军杀害而导致的中国学生抗议苏联侵犯中国主权而进行的一系列游行示威活动。亦称[張莘夫事件](../Page/張莘夫.md "wikilink")。
  - [北平七五事件](../Page/北平七五事件.md "wikilink")

<!-- end list -->

  - ;中華人民共和國

<!-- end list -->

  - [反右运动初期](../Page/反右运动.md "wikilink")
  - [红卫兵运动](../Page/红卫兵运动.md "wikilink")：[文革初期由中国大陆政治高层斗争及对毛个人崇拜所引领的一场学生运动](../Page/文革.md "wikilink")。
  - [上山下乡运动](../Page/上山下乡运动.md "wikilink")：指的是于1960年代至1970年代中华人民共和国政府组织大量城市知识青年离开城市，在农村定居和劳动的政治运动。
  - [四五运动](../Page/四五运动.md "wikilink")：以1976年4月5日在中华人民共和国首都北京天安门广场发生的大规模群众抗议事件为代表的全国性的抗议活动。
  - [八六学潮](../Page/八六学潮.md "wikilink")：由[中国科技大学于](../Page/中国科技大学.md "wikilink")1986年12月所发起的民主学运，随后大陆各地高等学校约150所连锁响应。
  - [六四事件](../Page/六四事件.md "wikilink")：或稱八九民運，指發生在1989年4月15日至6月4日，一場由中國北京大學學生所主導的學生運動，主要領導者為[王丹](../Page/王丹.md "wikilink")、[吾爾開希與](../Page/吾爾開希.md "wikilink")[柴玲](../Page/柴玲.md "wikilink")。

### 香港

  - ;英屬香港

<!-- end list -->

  - [1970年代香港學生運動](../Page/1970年代香港學生運動.md "wikilink")

<!-- end list -->

  - ;香港特別行政區

<!-- end list -->

  - [學民思潮反](../Page/學民思潮.md "wikilink")[國教](../Page/德育及國民教育科.md "wikilink")
  - [2014年香港學界大罷課](../Page/2014年香港學界大罷課.md "wikilink")

### 中華民國（台灣）

  - [四六事件](../Page/四六事件.md "wikilink")
  - [自覺運動為](../Page/自覺運動.md "wikilink")1960年代台灣青年學生發起的運動，內容為提振國民公德心的運動。
  - [野百合學運](../Page/野百合學運.md "wikilink")（[三月學運](../Page/三月學運.md "wikilink")）是台灣在1990年3月16日至3月22日發生的學生運動。
  - [新野百合學運發生於台灣](../Page/新野百合學運.md "wikilink")，從2004年4月2日至5月17日，由一群台灣大學學生所發起的活動主要是集結在中正紀念堂靜坐抗議。最後在5月17日被警察強制驅離為止。
  - [野草莓運動](../Page/野草莓運動.md "wikilink")
  - [反媒體壟斷運動](../Page/反媒體壟斷運動.md "wikilink")
  - [太陽花學運](../Page/太陽花學運.md "wikilink")（[318青年佔領立法院](../Page/318青年佔領立法院.md "wikilink")、[318學運](../Page/318學運.md "wikilink")）
  - [反高中課綱微調運動](../Page/反高中課綱微調運動.md "wikilink")

### 日本

  - [東京大學事件](../Page/東京大學事件.md "wikilink")

### 韓國

  - [4·19學生革命](../Page/4·19學生革命.md "wikilink")

### 蘇聯

  - [阿拉木图十二月事件](../Page/杰勒托克桑事件.md "wikilink")（[前苏联哈萨克加盟共和国](../Page/哈薩克蘇維埃社會主義共和國.md "wikilink")）

## 歐洲重大事件

### 法國

  - [五月風暴](../Page/五月風暴.md "wikilink")

### 匈牙利

  - [匈牙利十月事件](../Page/匈牙利十月事件.md "wikilink")

## 相關

  - [革命](../Page/革命.md "wikilink")
  - [罷課](../Page/罷課.md "wikilink")
  - [學生會](../Page/學生會.md "wikilink")
  - [工會](../Page/工會.md "wikilink")

[學生運動](../Category/學生運動.md "wikilink")
[Category:学生文化](../Category/学生文化.md "wikilink")