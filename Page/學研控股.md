**株式會社學研控股**（），是[日本一家以](../Page/日本.md "wikilink")[教育](../Page/教育.md "wikilink")[書籍為業務中心的](../Page/書籍.md "wikilink")[出版社](../Page/出版社.md "wikilink")，常被略稱為**學研**()。另有經營「[學研教室](../Page/學研教室.md "wikilink")」。

前身為「株式會社學習研究社」（），是以『中學課程』、『科學與學習』等教育雜誌、参考書、辭書、辭典等教育關係的出版品為中心所發展的出版社。學習雜誌不透過書店，而是由小學老師在教室內收款來發放的販賣系統，與送貨員直接運送到家裡的直銷制度來擴展版圖。過去曾經手[玩具事業](../Page/玩具.md "wikilink")，也曾將玩具事業轉移至子公司「學研Toy
Hobby」。2004年7月，學研吸收合併子公司「立風書房」()。2005年秋季，學研將過去的子公司「學研CREGIT」賣給了[MBO的](../Page/Management_BuyOut.md "wikilink")[NIFNIF
SMBC
Ventures](../Page/NIFNIF_SMBC_Ventures.md "wikilink")-{系}-投資公司。2009年10月1日，[控股公司化](../Page/控股公司.md "wikilink")，變更商號為「株式會社學研控股」()，簡稱「學研HD」。

## 沿革

  - 1946年4月 - 學習研究社成立。
  - 1982年 -
    股票於[東京證券交易所](../Page/東京證券交易所.md "wikilink")2部[上市](../Page/上市.md "wikilink")。
  - 1984年 - 股票轉至東京證券交易所1部上市。
  - 2002年 - 踏入映像內容事業，成立****。
  - 2004年 - 與內容制作大手的[Index
    Holdings合作](../Page/Index_Holdings.md "wikilink")。
  - 2006年 - 和[史克威爾艾尼克斯合作](../Page/史克威爾艾尼克斯.md "wikilink")。於5月成立新公司**SG
    Lavo**。
  - 2009年 - 組織改編，變更商號為。

## 事業

### 雜誌

  - 『』(教育雜誌)
  - 『』(教育雜誌)
      - 『1年的科學』
      - 『2年的科學』
      - 『3年的科學』
      - 『4年的科學』
      - 『5年的科學』
      - 『6年的科學』
      - 『1年的學習』
      - 『2年的學習』
      - 『3年的學習』
      - 『4年的學習』
      - 『5年的學習』
      - 『6年的學習』
  - 『中學課程』(教育雜誌)- 1999年廢刊
      - 『中一課程』
      - 『中二課程』
      - 『中三課程』
  - 『[Megami MAGAZINE](../Page/Megami_MAGAZINE.md "wikilink")』(美少女角色雜誌)
  - 『』(科學雜誌)
  - 『[TV LIFE](../Page/TV_LIFE.md "wikilink")』(電視雜誌)
  - 『[Animedia](../Page/Animedia.md "wikilink")』([動畫雜誌](../Page/動畫雜誌.md "wikilink"))
  - 『[AnimeV](../Page/AnimeV.md "wikilink")』(動畫雜誌)※休刊
  - 『[Pichi Lemon](../Page/Pichi_Lemon.md "wikilink")』(流行雜誌)
  - 『[BOMB](../Page/BOMB.md "wikilink")』(偶像雜誌)
  - 『[MU](../Page/MU.md "wikilink")』(オカルト雜誌)
  - 『』(高爾夫雜誌)
  - 『[STRIKER](../Page/Striker_\(消歧義\).md "wikilink")』(足球専門誌　2004年夏季より月刊から不定期刊→2005年2月より隔月刊の『ストライカー・DX』に変更)
  - 『少年Challenge』([漫畫雜誌](../Page/漫畫雜誌.md "wikilink"))※休刊
  - 『[月刊Comic NORA](../Page/月刊Comic_NORA.md "wikilink")』(漫畫雜誌)※休刊
  - 『Famicon Top』(ゲーム雜誌)※休刊
  - 『』(自動車雜誌　旧立風書房)
  - 『主婦早安』(生活雜誌)
  - 『嬰兒早安』(育兒雜誌)
  - 『CAPA』(相機雜誌)
  - 『Digital CAPA』(相機雜誌)
  - 『Camera GET\!』(相機雜誌)
  - 『四季的寫真』(相機雜誌)
  - 『歷史群像』(戦略・戦術・戦史雜誌　隔月)
  - 『MakeUp Magazine』
  - 『OutRider』(摩托車賽車雜誌　旧立風書房　隔月)

### 主要書籍

  - 『[ひみつシリーズ](../Page/ひみつシリーズ.md "wikilink")』([學習漫畫](../Page/學習漫畫.md "wikilink"))
  - [大学受験・超基礎シリーズ](../Page/大学受験・超基礎シリーズ.md "wikilink")

[井川治久](../Page/井川治久.md "wikilink")
『もっと[わかりすぎる！](../Page/わかりすぎる！.md "wikilink")[英語のルール](../Page/英語.md "wikilink")55』、『井川治久の[超基礎英語](../Page/超基礎.md "wikilink")[塾](../Page/塾.md "wikilink")
超基礎英語[わかりすぎる！](../Page/わかりすぎる！.md "wikilink")』、『井川式
秘密の英[文法ノート](../Page/文法.md "wikilink")』、『[早慶](../Page/早慶.md "wikilink")[ミッション系私大用](../Page/ミッションスクール.md "wikilink")
英単語頻出問題』、『頻出問題[わかりすぎる！](../Page/わかりすぎる！.md "wikilink")』
([高校生向け](../Page/高校生.md "wikilink")[學習参考書](../Page/學習参考書.md "wikilink"))
など

[荻野文子](../Page/荻野文子.md "wikilink")
『荻野文子の[超基礎国語塾](../Page/超基礎.md "wikilink")　[マドンナ](../Page/マドンナ.md "wikilink")[古文](../Page/古文.md "wikilink")』、『[マドンナ古文](../Page/マドンナ古文.md "wikilink")[単語](../Page/単語.md "wikilink")
230』、『古文 [マドンナ解法](../Page/マドンナ.md "wikilink")』(高校學参) など

  - 大学受験・ポケットシリーズ

[樋口裕一](../Page/樋口裕一.md "wikilink")
『[読むだけ](../Page/読むだけ.md "wikilink")[小論文](../Page/小論文.md "wikilink")』など

### 玩具

  - [メカモ](../Page/メカニマル.md "wikilink")
  - 學研[電子ブロック](../Page/電子ブロック.md "wikilink")
  - 學研[マイキット](../Page/マイキット.md "wikilink")
  - ボーイズコマンダー
  - [Nゲージ](../Page/Nゲージ.md "wikilink")[鉄道模型](../Page/鉄道模型.md "wikilink")
  - [LSIゲーム](../Page/LSIゲーム.md "wikilink")
  - 學研[大人的科學](../Page/大人的科學.md "wikilink")

### 動畫

  - [スプーンおばさん](../Page/スプーンおばさん.md "wikilink")
  - [番茄十勇士](../Page/番茄十勇士.md "wikilink")()
  - [地球SOS それいけコロリン](../Page/地球SOS_それいけコロリン.md "wikilink")
  - [尼爾斯騎鵝旅行記](../Page/尼爾斯騎鵝旅行記.md "wikilink")()
  - [チックンタックン](../Page/チックンタックン.md "wikilink")
  - [忍者亂太郎](../Page/忍者亂太郎.md "wikilink")()
  - [動畫秘密花園](../Page/動畫秘密花園.md "wikilink")()
  - [ジャングル大帝](../Page/ジャングル大帝.md "wikilink")(第3作)
  - [ボスコアドベンチャー](../Page/ボスコアドベンチャー.md "wikilink")
  - [まいっちんぐマチコ先生](../Page/まいっちんぐマチコ先生.md "wikilink")
  - [三眼神童](../Page/三眼神童.md "wikilink")()
  - [少棒小魔投](../Page/少棒小魔投.md "wikilink")
  - [動畫三劍客](../Page/動畫三劍客.md "wikilink")()
  - [はじめ人間　ゴン](../Page/ギャートルズ.md "wikilink")
  - [天使的尾巴](../Page/天使的尾巴.md "wikilink")()
  - [流星戰隊Musumet](../Page/流星戰隊Musumet.md "wikilink")

### 遊戲軟體

  - [任天堂](../Page/任天堂.md "wikilink")

<!-- end list -->

  - [タイタニックミステリー](../Page/タイタニックミステリー.md "wikilink")
  - [ムーランルージュ戦記 メルヴィルの炎](../Page/ムーランルージュ戦記_メルヴィルの炎.md "wikilink")
  - [マイト・アンド・マジック](../Page/マイト・アンド・マジック.md "wikilink")

<!-- end list -->

  - [3DO](../Page/3DO.md "wikilink")

<!-- end list -->

  - 以英語 GO\!()

### 文學賞

[小川未明文學賞](../Page/小川未明文學賞.md "wikilink")

## 產品爭議事件

學研Toy
Hobby於[中國大陸製造](../Page/中國大陸.md "wikilink")、附語音導覽功能的「聰明[地球儀](../Page/地球儀.md "wikilink")」中，將[台灣列為](../Page/台灣.md "wikilink")[台灣島](../Page/台灣島.md "wikilink")，並於導覽中稱台灣為[中華人民共和國的領土](../Page/中華人民共和國.md "wikilink")，領導為[胡錦濤](../Page/胡錦濤.md "wikilink")，使用[人民幣等](../Page/人民幣.md "wikilink")，於2008年1月10日，被《[產經新聞](../Page/產經新聞.md "wikilink")》以「抹消台灣」為標題大幅報導，並遭到台灣駐日代表及大量台灣人的无理抗議。學研Toy的說明是：「當初原本決定遵循当时的日本學校教科書的標記法，但因為工廠設於中國，按照法律规定說如果不改變標記的話，地球儀就無法[出口到日本](../Page/出口.md "wikilink")。由於訂單已大量湧進，为了不造成不必要的损失，就改变了台湾的标记。」[1](http://news.msn.com.tw/news546026.aspx)
遭到抗議後，學研道歉並停售該款地球儀，並全面回收第一批已售出之一萬個地球儀。2008年1月17日，學研Toy以「台湾人就是矫情FA几把干调哦」，宣布於該年三月底解散。[2](https://web.archive.org/web/20080120185437/http://www.libertytimes.com.tw/2008/new/jan/18/today-p10.htm)[3](http://www.gakken.co.jp/pdf/toys080117.pdf)

## 相關項目

  - [古岡秀人](../Page/古岡秀人.md "wikilink")
  - [川村龍一](../Page/川村龍一.md "wikilink")：在成為廣播主持人前曾任學習研究社的音樂雜誌編輯組。
  - [畑正憲](../Page/畑正憲.md "wikilink")：成為作家前任職學習研究社的電影部門職員。
  - [内山安二](../Page/内山安二.md "wikilink")：學習研究社旗下的漫畫家之一。

## 外部連結

  - [學研集團](http://www.gakken.co.jp/)

  - [學研控股](http://ghd.gakken.co.jp/)

  - [學研教室・學研高木教室](https://web.archive.org/web/20061215180634/http://www.manabinomori.info/gakken/)

  - [地球儀產品傷台灣人民感情 日商道歉停售](http://news.msn.com.tw/news546026.aspx)

  - [矮化台灣
    日地球儀公司解散](https://web.archive.org/web/20080120185437/http://www.libertytimes.com.tw/2008/new/jan/18/today-p10.htm)

  - [學研Toy解散聲明](http://www.gakken.co.jp/pdf/toys080117.pdf)

[學習研究社](../Category/學習研究社.md "wikilink")
[9](../Category/東京證券交易所上市公司.md "wikilink")
[Category:品川區公司](../Category/品川區公司.md "wikilink")
[Category:1946年日本建立](../Category/1946年日本建立.md "wikilink")
[Category:日本出版社](../Category/日本出版社.md "wikilink")
[Category:動畫產業公司](../Category/動畫產業公司.md "wikilink")
[G](../Category/日本控股公司.md "wikilink")