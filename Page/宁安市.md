**宁安市**在[中國](../Page/中國.md "wikilink")[黑龙江省东南部](../Page/黑龙江省.md "wikilink")，是[牡丹江市下辖的一个](../Page/牡丹江市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。宁安市东与[穆棱市毗邻](../Page/穆棱市.md "wikilink")，西与[海林市交界](../Page/海林市.md "wikilink")，南与[吉林省](../Page/吉林省.md "wikilink")[汪清县接壤](../Page/汪清县.md "wikilink")，北与牡丹江市相连。

## 历史

[唐代时](../Page/唐代.md "wikilink")[渤海国在此建都](../Page/渤海国.md "wikilink")[上京龙泉府](../Page/上京龙泉府.md "wikilink")，在今宁安市[渤海镇有](../Page/渤海镇_\(宁安市\).md "wikilink")[城池遗址](../Page/渤海国上京龙泉府遗址.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[康熙五年](../Page/康熙.md "wikilink")（1666年）“[宁古塔](../Page/宁古塔.md "wikilink")”城从旧城（今[海林市](../Page/海林市.md "wikilink")）移至这里的新城。

1903年，清政府于宁古塔副都统辖区的三岔口（今东宁县三岔口镇）设置绥芬厅，1909年6月2日升改为绥芬府，移驻宁古塔城。1910年5月，以府治[宁古塔城取义](../Page/宁古塔.md "wikilink")，改为宁安府。1913年3月改为宁安县。1993撤县建市。

2005年[沙兰镇曾发生](../Page/沙兰镇.md "wikilink")[泥石流事件](../Page/2005年沙兰镇洪灾.md "wikilink")，造成众多小学生死亡。

## 行政区划

下辖7个镇、5个乡：\[1\] 。

根據牡丹江市城市規劃局的《城市未來發展規劃藍圖》，寧安市將在今後被規劃爲牡丹江市的一個行政區。\[2\]

## 特产

东京城镇盛产[水稻](../Page/水稻.md "wikilink")，其中“响水大米”是[清代皇家御用](../Page/清代.md "wikilink")[大米](../Page/大米.md "wikilink")。宁安镇红城村盛产[大蒜](../Page/大蒜.md "wikilink")、[洋葱](../Page/洋葱.md "wikilink")。兰岗镇的[西瓜闻名全国](../Page/西瓜.md "wikilink")。

## 教育

宁安一中是该市重点中学（省级重点高中）。

## 名胜古迹

  - [镜泊湖](../Page/镜泊湖.md "wikilink")：为[中国国家级风景名胜区](../Page/中国国家级风景名胜区.md "wikilink")、[国家5A级旅游景区](../Page/国家5A级旅游景区.md "wikilink")；
  - [渤海国上京龙泉府遗址](../Page/渤海国上京龙泉府遗址.md "wikilink")：为[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

## 名人

中国著名革命家[马骏就出生在这里](../Page/马骏.md "wikilink")。

## 参考文献

[\*](../Category/宁安市.md "wikilink")
[Category:黑龙江省县级市](../Category/黑龙江省县级市.md "wikilink")

1.
2.  [牡丹江新聞網：绘制一幅“新牡丹江、大牡丹江”的壮美蓝图](http://mudanjiang.northeast.cn/system/2008/03/25/051187983.shtml)