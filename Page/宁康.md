**宁康**（373年-375年）是[東晋皇帝](../Page/東晋.md "wikilink")[晋孝武帝司马曜的第一个](../Page/晋孝武帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

《[魏书](../Page/魏书.md "wikilink")》错写作“康宁”。

## 纪年

| 宁康                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 373年                           | 374年                           | 375年                           |
| [干支](../Page/干支纪年.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [升平](../Page/升平.md "wikilink")：[前凉年号](../Page/前凉.md "wikilink")
      - [建元](../Page/建元.md "wikilink")（365年-385年七月）：[前秦政权](../Page/前秦.md "wikilink")[苻坚年号](../Page/苻坚.md "wikilink")
      - [黑龙](../Page/黑龙.md "wikilink")（374年六月-九月）：[张育自立年号](../Page/张育.md "wikilink")
      - [建国](../Page/建国.md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍年号](../Page/拓跋什翼犍.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:东晋年号](../Category/东晋年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:370年代中国政治](../Category/370年代中国政治.md "wikilink")