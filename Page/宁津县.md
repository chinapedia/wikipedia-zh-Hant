**宁津县**在[中国](../Page/中国.md "wikilink")[山东省西北部](../Page/山东省.md "wikilink")，是[德州市所辖的一个](../Page/德州市.md "wikilink")[县](../Page/县.md "wikilink")，邻接[河北省](../Page/河北省.md "wikilink")。原屬河北省，1950年，改属德州专区。1952年，宁津县划归河北省属沧州专区。1958年，沧州专区并入天津市，宁津县属天津市。1961年恢复沧州专区，宁津县复属沧州。1965年3月19日，宁津县划归山东省，属德州地区至今。

## 历史沿革

[东魏](../Page/东魏.md "wikilink")[天平元年](../Page/天平_\(东魏\).md "wikilink")（534年），于今县城西保店胡苏亭置胡苏县；[唐](../Page/唐.md "wikilink")[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")，废胡苏改为临津县；[金](../Page/金.md "wikilink")[天会六年秋](../Page/天会_\(金朝\).md "wikilink")，临津城闹大水，城毁，东迁二十五里卧牛岗为治，改名宁津县，取“安宁”之意。

## 行政区划

宁津县辖9个镇、2个乡：

  - 镇：宁津镇、柴胡店镇、长官镇、杜集镇、保店镇、大柳镇、大曹镇、相衙镇、时集镇
  - 乡：张大庄乡、刘营伍乡。

## 名人

[:Category:宁津人](../Category/宁津人.md "wikilink")

## 外部链接

  - [宁津县简介](https://web.archive.org/web/20060209035153/http://www.sd.xinhuanet.com/dzwq/dz/indexnj.htm)

[宁津县](../Category/宁津县.md "wikilink")
[县](../Category/德州区县市.md "wikilink")
[德州](../Category/山东省县份.md "wikilink")