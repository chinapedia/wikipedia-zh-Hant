**宁财神**（），[上海人](../Page/上海人.md "wikilink")，本名**陈万宁**。他最初给自己取藝名为“宁采臣”，後改為其諧音“宁[财神](../Page/财神.md "wikilink")”\[1\]。[情景喜剧](../Page/情景喜剧.md "wikilink")《[武林外传](../Page/武林外传.md "wikilink")》的[编剧](../Page/编剧.md "wikilink")。

代表作有剧本《[武林外传](../Page/武林外传.md "wikilink")》(2006)、《[防火墙5788](../Page/防火墙5788.md "wikilink")》、《[大笑江湖](../Page/大笑江湖.md "wikilink")》、《[人在囧途](../Page/人在囧途.md "wikilink")》、《[龙门镖局](../Page/龙门镖局.md "wikilink")》(2013)，[小说](../Page/小说.md "wikilink")《缘分的天空》、《假装纯情》、《无数次亲密接触》。

## 生平

他15岁进入大学，毕业于[华东理工大学](../Page/华东理工大学.md "wikilink")[金融专业](../Page/金融.md "wikilink")，毕业后随朋友到[北京](../Page/北京.md "wikilink")，从事与自己专业相关的工作——[期货交易](../Page/期货交易.md "wikilink")。他的业余爱好是听歌、看碟。他喜欢[张学友的歌和](../Page/张学友.md "wikilink")[周星驰的电影](../Page/周星驰.md "wikilink")\[2\]。妻子程娇娥。

2014年6月24日傍晚，他因涉嫌吸食[冰毒](../Page/甲基苯丙胺.md "wikilink")，在北京市[朝陽區
(北京市)工體北路一公寓內被当地公安查获并](../Page/朝陽區_\(北京市\).md "wikilink")[行政拘留](../Page/行政拘留.md "wikilink")15天，及後承認過去半年曾數次吸食，他对自己的吸毒行为表示“深切的歉意和懊悔”\[3\]\[4\]，知情人曝宁财神吸毒被拘与之前因吸毒被抓的电影导演[张元有关](../Page/张元_\(导演\).md "wikilink")。\[5\]从拘留所释放后，他在个人微博发表了一篇《道歉信》\[6\]，并表示此生再不涉毒。他于7月6日被释放\[7\]，他吸毒后的首次公开亮相是出席[张靓颖新专辑](../Page/张靓颖.md "wikilink")《[第七感](../Page/第七感.md "wikilink")》[发布会](../Page/发布会.md "wikilink")。\[8\]

## 其他

宁财神自2013年7月14日（按播出时间计）起担任“[非诚勿扰](../Page/非诚勿扰_\(节目\).md "wikilink")”节目的现场评论员。\[9\]从2014年6月28日（按播出时间计）起，宁财神不再出现在《非诚勿扰》节目中。\[10\]

2014年6月24日宁财神因吸食毒品被公安机关查获逮捕。在警方审问时，宁财神交代自己在7个月内共购买了三次[冰毒](../Page/冰毒.md "wikilink")，总共价值三千[元](../Page/人民币.md "wikilink")。\[11\]。10月8日，宁财神己正式入[广电总局的](../Page/广电总局.md "wikilink")“封杀劣迹艺人”列单中，其国内参与制作的电影、电视剧、电视节目、广告节目、网络剧、微电影等，都列入暂停播出范围。\[12\]

## 註釋

## 參考文獻

## 外部链接

  - [宁财神：手中有剑心中无码 - 宁财神](http://blog.sina.com.cn/ningcaishen) 新浪BLOG

  - [文学网站“榕树下”的编辑](http://ningcaishen.rongshu.org)

  -
  -
[Category:武林外传](../Category/武林外传.md "wikilink")
[Category:上海作家](../Category/上海作家.md "wikilink")
[Category:中華人民共和國编剧](../Category/中華人民共和國编剧.md "wikilink")
[Category:中華人民共和國小說家](../Category/中華人民共和國小說家.md "wikilink")
[Category:华东理工大学校友](../Category/华东理工大学校友.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[W](../Category/陈姓.md "wikilink")

1.
2.
3.  [北京市公安局微博通報](http://ww1.sinaimg.cn/large/4cd3493fjw1ehrmquhvejj20de1i5qbo.jpg)，2014年6月26日
4.
5.
6.
7.  [宁财神发道歉信
    好友姚晨继续力挺：说到要做到](http://ent.ifeng.com/a/20140707/40163881_0.shtml)
8.  [获释后助阵张靓颖
    宁财神称“还活着”](http://news.hexun.com/2014-07-23/166912322.html)
9.
10.
11.
12. [广电总局下发通知 影视网全面叫停“劣迹艺人”](http://ent.qq.com/a/20141009/007422.htm)