[Toyota_QD200_201.JPG](https://zh.wikipedia.org/wiki/File:Toyota_QD200_201.JPG "fig:Toyota_QD200_201.JPG")
[President_Transnet_7T-278_20100209.jpg](https://zh.wikipedia.org/wiki/File:President_Transnet_7T-278_20100209.jpg "fig:President_Transnet_7T-278_20100209.jpg")

**宅急便**是[日本的](../Page/日本.md "wikilink")（雅瑪多運輸）於1976年所創造的[宅配服務](../Page/宅配.md "wikilink")[品牌及](../Page/品牌.md "wikilink")[商標](../Page/商標.md "wikilink")；並由於大和運輸的[黑貓親子商標的緣故](../Page/黑貓.md "wikilink")，所以也被慣稱為「**黑貓宅急便**」。

## 簡介

宅配服務是藉由各種陸上[交通工具的小區域經營及轉運系統](../Page/交通工具.md "wikilink")，經營戶對戶包裹的收取與配送。與傳統的[郵局寄送最大的差別是](../Page/郵局.md "wikilink")，郵局僅能由寄件人在平日上班時間親自到郵局或郵政代辦所付費寄送；而宅配服務是可以到包括[便利商店等代收通路](../Page/便利商店.md "wikilink")，或是打電話請宅急便的服務人員直接到家裡收取包裹。

台灣另有其他民營公司經營近似宅急便的輕量物流服務，例如[東元集團與](../Page/東元集團.md "wikilink")[日本通運合資的](../Page/日本通運.md "wikilink")[台灣宅配通](../Page/台灣宅配通.md "wikilink")。傳統以企業客戶作為主要客層的大宗貨物運輸公司，如[嘉里大榮貨運](../Page/嘉里大榮貨運.md "wikilink")、[新竹物流等](../Page/新竹物流.md "wikilink")，也經營輕量物流服務。國營的[中華郵政亦加入此服務](../Page/中華郵政.md "wikilink")。

## 名稱問題

由於宅急便是創新的服務模式，並有著極高的[市場佔有率](../Page/市場佔有率.md "wikilink")，因此在日本也成為宅配服務的[代名詞](../Page/代名詞.md "wikilink")。而在與日本一海之隔的[台灣](../Page/台灣.md "wikilink")，隨著[吉卜力工作室動畫](../Page/吉卜力工作室.md "wikilink")《[魔女宅急便](../Page/魔女宅急便.md "wikilink")》（）在台上映，[統一速達與大和運輸合作並以相同商標引進此一服務後](../Page/統一速達.md "wikilink")，「宅急便」一詞亦在台灣引起相同的現象。然而這種現象會造成宅急便商標有[普通名詞化的可能](../Page/通用商標.md "wikilink")，而造成商標所有權脫離法律保護傘，此情況並非廠商所樂見。事實上，《魔女宅急便》的原作者角野榮子，就是在未經大和運輸的同意下，使用了「宅急便」一詞，而一度引起大和運輸的不滿（雖然後來大和運輸也同意贊助《魔女宅急便》的動畫製作）。

因為「宅急便」一詞終究還是大和運輸專有的商標名稱，任意使用可能造成混亂或面臨[法律的](../Page/法律.md "wikilink")[侵權問題](../Page/侵權.md "wikilink")（如Coke一詞與可口可樂之間的關係），因此日本另以「宅配便」一詞來統稱此類運送服務，而台灣企業對民眾因商業交換產生的物流行為則用「宅配」一詞稱之。

## 加值服務

  - 貨到收（付）款
  - 低溫與冷凍環境配送
  - 網路查詢配送進度
  - 店配店（收送件地點皆為便利商店）

## 參見

  - [統一速達](../Page/統一速達.md "wikilink")

## 外部連結

  - [大和運輸](http://www.kuronekoyamato.co.jp/)
      - [大和運輸的宅急便介紹](http://www.kuronekoyamato.co.jp/takkyubin/takkyu.html)
  - [統一速達的黑貓宅急便](https://web.archive.org/web/20131105072441/http://www.t-cat.com.tw/company/about.aspx)
  - [雅瑪多運輸（香港）有限公司](http://hk.ta-q-bin.com/chi/index.aspx)

[Category:貨運](../Category/貨運.md "wikilink")
[Category:速遞公司](../Category/速遞公司.md "wikilink")