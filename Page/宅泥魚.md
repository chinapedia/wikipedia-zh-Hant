**宅泥魚**（[学名](../Page/学名.md "wikilink")：），又稱**三帶圓雀鯛**，俗名為厚殼仔、三間雀，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[雀鯛科的其中一](../Page/雀鯛科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")[南海](../Page/南海.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[所羅門群島](../Page/所羅門群島.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[萬那杜](../Page/萬那杜.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[法屬玻里尼西亞等海域](../Page/法屬玻里尼西亞.md "wikilink")。该物种的模式产地在南亚和东南亚。\[1\]

## 深度

水深0至20公尺。

## 特徵

本魚圓形而側扁，體色以白色為底，身上有3條橫帶，其中第一條起於背鰭起點之前，經眼而達下頜，但吻部仍為白色；吻短而鈍圓。口中型；兩頜齒小而呈圓錐狀，靠外緣之齒列漸大且齒端背側有不規則之絨毛帶。眶前骨具鱗，眶下骨具鱗，下緣具鋸齒。第二條則起背鰭中央經胸鰭基部而達腹鰭；第三條則由背鰭後端至臀鰭。尾鰭白色，略呈叉形，背鰭略嘿。背鰭硬棘12枚、背鰭軟條11至13枚、臀鰭硬棘2枚、臀鰭軟條11至13枚。體長可達10公分。

## 生態

本魚棲息在珊瑚礁和沙地交接的枝狀珊瑚或[軸孔珊瑚上](../Page/軸孔珊瑚.md "wikilink")，具強烈的領域性。以浮游動物、底棲的無脊椎動物和藻類為食。平時在珊瑚上方活動，遇到危險時就躲入珊瑚枝椏中以躲避敵害。生殖期時，雄魚會邀請雌魚產卵於他們的巢中，並保護卵直到它們孵化，而此時的親魚變成對其他的魚非常有侵略性。

## 經濟利用

為可愛的觀賞魚。很少人食用。

## 参考文献

  -
[DA](../Category/觀賞魚.md "wikilink")
[aruanus](../Category/宅泥魚屬.md "wikilink")

1.