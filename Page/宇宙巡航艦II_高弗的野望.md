，歐美名稱《**火神奇航**》（）是由日本遊戲公司[科樂美所開發與發行之橫向捲軸射擊遊戲](../Page/科樂美.md "wikilink")《[宇宙巡航艦系列](../Page/宇宙巡航艦系列.md "wikilink")》作品，於1988年3月24日在日本上市。

## 劇情

  - 街機版

兩年前，Gradius星突然遭受了Bacterion軍團的攻擊。面臨敵人強大的攻擊，使得Gradius軍陷入了苦戰，因此決定使用最後的手段，派遣超時空戰鬥機Vic
Viper前往攻擊Bacterion軍團，並成功毀滅了Zelos要塞。兩年後的現在，Bacterion軍團派出了特殊部隊GOFER前往攻擊Gradius星，因此Gradius軍立刻派出了Vic
Viper進行反擊。\[1\]

  - FC版

當Metalion星系四大行星「Gradius」、「Larz」、「Shin」、「Latis」開始進行10000年一次的行星直線排列的時候，四顆行星的中心處出現了黑暗的氣體雲，並使周圍的太空牧場開始燃燒。接著突如出現了神秘部隊「GOFER」，並對Gradius星展開了攻擊。為了阻止GOFER的攻擊，超時空戰鬥機「Vic
Viper」再度出擊。\[2\]

## 遊戲系統

本遊戲除了延續[宇宙巡航艦
(遊戲)的基本系統及操作之外](../Page/宇宙巡航艦_\(遊戲\).md "wikilink")，追加了幾項新要素與變更。

  - 能量膠囊

藍色能量膠囊除了可以消滅低耐久度敵人，並可讓畫面上的敵方子彈全部消失。

  - 能量表

每取得一次紅色膠囊即可提升一格能量表，每一格能量表各自表示一種裝備供玩家選擇。在FC版中,1P操縱的戰機為Vic
Viper(機身為藍色,Option為紅色),2P操縱的戰機為Lord British(機身為紅色,Option為藍色)

<center>

|          |                                      |                |             |              |           |            |       |
| -------- | ------------------------------------ | -------------- | ----------- | ------------ | --------- | ---------- | ----- |
| **Type** | **Image color**                      | **Speedup**    | **Missile** | **Double**   | **Laser** | **Option** | **?** |
| Type 1   | <font color="#008000">綠</font color> | Speed up       | Missile     | Double       | Laser     | Option     | ?     |
| Type 2   | <font color="#FFA500">橙</font color> | Spread Bomb    | Tail Gun    |              |           |            |       |
| Type 3   | <font color="#0000FF">藍</font color> | Photon Torpedo | Double      | Ripple Laser |           |            |       |
| Type 4   | <font color="#FFA500">橙</font color> | 2-Way Missile  | Tail Gun    |              |           |            |       |

</center>

*註：**粗體字**表示既有裝備。*

  - 速度提升 (Speed Up)
    機體飛行速度可以提升五次。

<!-- end list -->

  - 飛彈

<!-- end list -->

  - **飛彈 (Missile)**

<!-- end list -->

  -
    向前下方發射對地飛彈，落到地面後會延著地面前進直到擊中敵人為止。

<!-- end list -->

  - 廣域破壞炸彈 (Spread Bomb)

<!-- end list -->

  -
    向前方投擲的無誘導炸藥，可以摧毀一定範圍內的敵人。

<!-- end list -->

  - 光子魚雷 (Photon Torpedo)

<!-- end list -->

  -
    投下之後會沿著直線前進，並可貫穿敵人。

<!-- end list -->

  - 雙向飛彈 (2-Way Missile)

<!-- end list -->

  -
    向前上方與前下方拋射飛彈。

<!-- end list -->

  - 雙向機槍

<!-- end list -->

  - **雙向機槍 (Double)**

<!-- end list -->

  -
    原本的兩發前向子彈轉移一發往前上方發射。無法與雷射並用。

<!-- end list -->

  - 後方機槍 (Tailgun)

<!-- end list -->

  -
    原本的兩發前向子彈轉移一發往後方發射。無法與雷射並用。

<!-- end list -->

  - 雷射

<!-- end list -->

  - **雷射 (Laser)**

<!-- end list -->

  -
    具有貫穿能力的雷射。無法與雙向機槍並用。

<!-- end list -->

  - 波紋雷射 (Ripple Laser)

<!-- end list -->

  -
    上下幅度會隨著發射距離而增加的雷射武器，威力與一般子彈相同。無法與雙向機槍並用。

<!-- end list -->

  - 子機 (Option)
    子機會跟在自機後方並進行一樣的攻擊，最多可以攜帶四枚。FC版在取得四枚子機之後，會出現第五段具有時間限制的強化，讓四枚子機環繞於自機周圍。

`   歐美版本 "火神奇航" 在取得子機時，系統音效會說「Multiple」而非「Option」。  `

  - ？

在遊戲開始之前，可供玩家選擇防護罩。FC版無法選擇防護罩，防護罩皆為力場 (Force Field)。

  - **盾牌 (Shield)**

<!-- end list -->

  -
    於機首追加兩枚用來防禦來自前方的攻擊，可以抵擋16次攻擊。FC版無此裝備。

<!-- end list -->

  - **力場 (Force Field)**

<!-- end list -->

  -
    機體周圍會出現防護用的力場，用來防禦來自各方位的攻擊，可以抵擋3次攻擊。FC版可以抵擋5次攻擊。

## 關卡介紹

### 一般版

<center>

<table>
<tbody>
<tr class="odd">
<td><p><strong>關卡順序</strong></p></td>
<td><p><strong>關卡名稱</strong></p></td>
<td><p><strong>頭目</strong></p></td>
<td><p><strong>配樂</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>人工太陽</p></td>
<td><p>不死鳥（Phoenix）</p></td>
<td><p>Burning Heat</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/異形_(電影).md" title="wikilink">異型區域</a></p></td>
<td><p>巨眼 (Big Eye)</p></td>
<td><p>Synthetic Life</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>水晶區域</p></td>
<td><p>水晶核心戰艦 (Crystal Core)</p></td>
<td><p>Crystal World</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>火山區域</p></td>
<td><p>迪斯二式 (Des Mk-II)</p></td>
<td><p>A Way Out of the Difficulty</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/摩艾石像.md" title="wikilink">摩艾</a></p></td>
<td><p>巨大摩艾 (Big Moai)</p></td>
<td><p>The Old Stone Age 1<br />
The Old Stone Age 2</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>沙漠遺跡 (PCE版限定關卡)</p></td>
<td><p>沙漠核心戰艦 (Desert Core)</p></td>
<td><p>Deprived Sanctity</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>高速關卡</p></td>
<td><p>核心戰艦二式 (Big Core Mk-II)</p></td>
<td><p>Maximum Speed</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>連續頭目戰鬥</p></td>
<td><p>核心戰艦 (Big Core)<br />
格雷姆 (Golem)<br />
巡洋艦鐵特朗 (Tetran)<br />
卡烏 (Gau)<br />
侵入者 (Intruder)<br />
覆蓋式核心戰艦 (Covered Core)</p></td>
<td><p>Aircraft Carrier<br />
Poison of Snake (Hard Battle)<br />
Fire Dragon</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>母艦</p></td>
<td><p>中間要塞<br />
六腳巨蟹 (Crab)<br />
高弗 (GOFER)</p></td>
<td><p>Into Hostile Ship<br />
Shoot and Shoot<br />
The Final Enemy</p></td>
</tr>
</tbody>
</table>

</center>

  - 頭目戰配樂為「Take Care\!」。

### Famicom版

<center>

<table>
<tbody>
<tr class="odd">
<td><p><strong>關卡順序</strong></p></td>
<td><p><strong>關卡名稱</strong></p></td>
<td><p><strong>頭目</strong></p></td>
<td><p><strong>配樂</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/人工.md" title="wikilink">人工</a><a href="../Page/太陽.md" title="wikilink">太陽</a></p></td>
<td><p><a href="../Page/不死鳥.md" title="wikilink">火鳥</a>（Phoenix）</p></td>
<td><p>Burning Heat</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>吉加</p></td>
<td><p>大眼 (Big Eye) / 吉加 (Giga)</p></td>
<td><p>Fortress</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/火山.md" title="wikilink">火山</a>&amp;<a href="../Page/結晶.md" title="wikilink">結晶</a></p></td>
<td><p>結晶核心 (Crystal Core)</p></td>
<td><p>Heavy Blow / Dead End</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/摩艾石像.md" title="wikilink">摩艾石像</a></p></td>
<td><p>巨大摩艾 (Big Moai)</p></td>
<td><p>The Old Stone Age</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>連續頭目戰鬥</p></td>
<td><p>核心戰艦 (Big Core)<br />
格雷姆 (Golem)<br />
鐵特朗 (Tetran)<br />
傑洛斯原力 (Zeros Force)<br />
覆蓋式核心戰艦 (Covered Core)</p></td>
<td><p>Aircraft Carrier<br />
Poison of Snake (Hard Battle)</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><a href="../Page/要塞.md" title="wikilink">要塞</a></p></td>
<td><p>戴墨斯 (Demos)<br />
巨蟹 (Crab)</p></td>
<td><p>Over Heat<br />
The Final Enemy</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><a href="../Page/細胞.md" title="wikilink">細胞</a></p></td>
<td><p>高弗 (GOFER)</p></td>
<td><p>Something Ghostly</p></td>
</tr>
</tbody>
</table>

</center>

  - 一般頭目戰配樂為「The Final Enemy」，最終關卡頭目戰配樂為「Take Care\!」。

## 秘技

  - 增加殘機數：在標題畫面出現時，依順序按上、上、下、下、左、右、左、右、B、A鍵，然後按START鍵，可令殘機數變成30架。
  - 音樂測試：在標題畫面出現時，同時按A、B鍵，再按START鍵就可出現音樂測試畫面。

## 移植版本

本遊戲具有以下幾種移植版本：

  - [FC](../Page/FC遊戲機.md "wikilink")

:\*i-revo FC移植版

:\*[Wii虛擬平臺FC移植版](../Page/Wii.md "wikilink")

  - [X68000](../Page/X68000.md "wikilink")
  - [PC Engine](../Page/PC_Engine.md "wikilink")

:\*[Wii虛擬平臺PCE移植版](../Page/Wii.md "wikilink")

  - 手機[Java程式版](../Page/Java.md "wikilink")

<!-- end list -->

  - \*

<!-- end list -->

  - [PlayStation](../Page/PlayStation.md "wikilink") - 宇宙巡航艦DX包 (GRADIUS
    Deluxe Pack)
  - [Sega Saturn](../Page/Sega_Saturn.md "wikilink") - 宇宙巡航艦DX包 (GRADIUS
    Deluxe Pack)
  - [PlayStation Portable](../Page/PSP.md "wikilink") - GRADIUS Portable

## 動畫版

  - 沙羅曼蛇ADVANCED SAGA －高弗的野望－
    *詳細請參見[沙羅曼蛇：高弗的野望](../Page/沙羅曼蛇：高弗的野望.md "wikilink")*。

## 得獎情報

  - 1988年度

## 註記

  - 街機版及一般移植版的頭目配樂為「Take Care\!」，最終關後半部分（與高弗戰鬥之前）的配樂則是「The Final
    Enemy」，而FC版的頭目配樂為「The Final Enemy」，「Take
    Care\!」則是與最終頭目高弗戰鬥的配樂。
  - PCE版的第六關類似[宇宙巡航艦2的第三關](../Page/宇宙巡航艦2.md "wikilink")，都是在遺跡的內部進行戰鬥。

## 參考資料

## 外部連結

  - [GRADIUS Portable官方網站](http://www.konami.jp/gs/game/gra_psp/) (日文)
  - [GRADIUS II -GOFER's Ambition-
    手機版官方網站](http://www.konami.jp/mobile/appli/gradius/)
    (日文)
  - [GRADIUS II
    i-revo版官方網站](http://game.i-revo.jp/dl/search/displayDetail.do?productId=10072)
    (日文)
  - [Wii虛擬平臺 GRADIUS II](http://www.nintendo.co.jp/wii/vc/vc_gr2/) (日文)
  - [Wii虛擬平臺 GRADIUS II -GOFER's Ambition-
    PCE版](http://vc-pce.com/jpn/j/title/gradius2.html) (日文)

[Category:1988年电子游戏](../Category/1988年电子游戏.md "wikilink") [2 GOFER's
Ambition](../Category/宇宙巡航艦系列電子遊戲.md "wikilink")
[Category:街機遊戲](../Category/街機遊戲.md "wikilink")
[Category:红白机游戏](../Category/红白机游戏.md "wikilink")
[Category:PlayStation
Network游戏](../Category/PlayStation_Network游戏.md "wikilink")
[Category:Wii Virtual
Console游戏](../Category/Wii_Virtual_Console游戏.md "wikilink")
[Category:后传电子游戏](../Category/后传电子游戏.md "wikilink")

1.  GRADIUS Portable公式Guide, page 28，圖書印刷株式會社, 2006 Mar 28, ISBN
    4-86155-111-0
2.  Gradius II遊戲說明書