**鄭州宇通集團有限公司**（简称**宇通集团**）是[中國的大型客車企業集團](../Page/中國.md "wikilink")。旗下企業包括[宇通客車](../Page/宇通客車.md "wikilink")、宇通重工、宇通专用车、精益达、绿都置业、国家电动客车电控与安全工程技术研究中心。宇通集團現有總資產約59.83億[人民幣](../Page/人民幣.md "wikilink")。

2016年8月，在[全国工商联发布](../Page/全国工商联.md "wikilink")“2016中国[民营企业](../Page/民营企业.md "wikilink")500强”榜单中，宇通集团以382亿元的收入排名第105位。\[1\]

## 参考资料

## 外部链接

  - [官方网站](http://www.yutong.com/)

[宇通集团](../Category/宇通集团.md "wikilink")
[Category:中國汽車公司](../Category/中國汽車公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:1963年中国建立](../Category/1963年中国建立.md "wikilink")
[Category:1963年成立的公司](../Category/1963年成立的公司.md "wikilink")

1.