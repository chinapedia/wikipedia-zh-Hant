**宇陀市**（）是[奈良縣東北部的](../Page/奈良縣.md "wikilink")[市](../Page/市.md "wikilink")，于2006年1月1日由[宇陀郡](../Page/宇陀郡.md "wikilink")、、、[合併而成](../Page/市町村合併.md "wikilink")。

## 地理

### 氣候

位於大和高原南端，與[奈良盆地相比夏季稍涼](../Page/奈良盆地.md "wikilink")，冬季寒冷。雨量相對較多。

### 人口

## 歷史

### 沿革

  - 2006年（[平成](../Page/平成.md "wikilink")18年）1月1日 -
    [宇陀郡](../Page/宇陀郡.md "wikilink")、、、[合併](../Page/市町村合併.md "wikilink")，設宇陀市。制定市旗、市章\[1\]\[2\]。

### 市域變遷

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/明治.md" title="wikilink">明治</a>22年<br />
1889年</p></th>
<th><p>明治23年<br />
1890年</p></th>
<th><p>明治26年<br />
1893年</p></th>
<th><p><a href="../Page/昭和.md" title="wikilink">昭和</a>10年<br />
1935年</p></th>
<th><p>昭和17年<br />
1942年</p></th>
<th><p>昭和29年<br />
1954年</p></th>
<th><p>昭和30年<br />
1955年</p></th>
<th><p>昭和31年<br />
1956年</p></th>
<th><p><a href="../Page/平成.md" title="wikilink">平成</a>18年<br />
2006年</p></th>
<th><p>平成23年<br />
2011年</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>奈良縣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宇陀郡</p></td>
<td><p><strong>宇陀市</strong></p></td>
<td><p><strong>宇陀市</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大宇陀町</p></td>
<td><p><em>大宇陀区</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>神戶村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>政始村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>吉野郡</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>龍門村</p></td>
<td><p>上龍門村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宇太村</p></td>
<td><p>宇太町</p></td>
<td><p>菟田野町</p></td>
<td><p><em>菟田野区</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>宇賀志村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>榛原村</p></td>
<td><p>榛原町</p></td>
<td><p>榛原町</p></td>
<td><p>榛原町</p></td>
<td><p><em>榛原区</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>伊那佐村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>内牧村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>室生村</p></td>
<td><p>室生村</p></td>
<td><p><em>室生区</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三本松村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山邊郡</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東里村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

  - 市長：竹内幹郎（2010年3月28日就任 1期目）

<!-- end list -->

  - 歷代市長

<!-- end list -->

  - 初代：前田禎郎（2006年2月12日 - 2010年2月11日 旧榛原町長）

另外，[眾議院議員選舉的選舉區為](../Page/日本眾議院.md "wikilink")「」，奈良縣議會議員選舉的選舉區為「宇陀郡・宇陀市選舉區」（定数：1）\[3\]。

### 行政機關

  - 警察

<!-- end list -->

  - 奈良県警察桜井警察署 宇陀警察分署（旧[宇陀警察署](../Page/宇陀警察署.md "wikilink")）（榛原萩原1953-1）

<!-- end list -->

  - 消防

<!-- end list -->

  - 宇陀消防署（榛原萩原）

      - 北分署（室生大野）
      - 南分署（大宇陀守道）
      - 東分署（御杖土屋原）

<!-- end list -->

  - 裁判所

<!-- end list -->

  - 宇陀簡易裁判所（奈良地裁葛城支部）（大宇陀下茶）

<!-- end list -->

  - 縣廳關係

<!-- end list -->

  - 宇陀土木事務所（大宇陀迫間）
  - 高原農業振興中心（奈良縣農業技術中心分場）
  - 畜產技術中心（大宇陀下竹）
  - 東部農林振興事務所
  - 宇陀川凈化中心
  - 奈良縣消防學校

<!-- end list -->

  - 其他

<!-- end list -->

  - [独立行政法人](../Page/独立行政法人.md "wikilink")室生壩管理所

## 經濟

### 產業

以農業、林業為中心。知名的有吉野葛（大宇陀）和肉用的宇陀牛（大宇陀、榛原）。菟田野的毛皮革產業亦很興盛。

### 金融機構

  - 榛原支店（榛原萩原）、大宇陀支店（大宇陀迫間）、菟田野支店（菟田野古市場）

  - 櫻井支店大宇陀出張所（大宇陀上本）

  - 榛原支店（榛原萩原）

### 農業協同組合

  - （，JA奈良縣）

      - 榛原支店・宇陀經濟中心（榛原篠樂）
      - 大宇陀支店（大宇陀西山）
          - 上龍門出張所（大宇陀田原）
      - 菟田野支店（菟田野古市場）
      - 室生支店（室生大野）

## 交通

### 鐵道

  - [近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")（近鐵）
      - [大阪線](../Page/大阪線.md "wikilink")： -  -
  - 代表站：榛原站（近鐵大阪線）

### 巴士

  - 榛原營業所管內

<!-- end list -->

  - 小型巴士

<!-- end list -->

  -
  - （路線大部分在[三重縣](../Page/三重縣.md "wikilink")[名張市境內](../Page/名張市.md "wikilink")，一部分延伸至本市）

### 道路

  - 一般国道

<!-- end list -->

  -
  -
  -
  -
<!-- end list -->

  - 主要地方道

<!-- end list -->

  - 奈良縣道28号吉野室生寺針線
  - 奈良縣道31号榛原菟田野御杖線

<!-- end list -->

  - 一般縣道

<!-- end list -->

  - 奈良縣道127号北野吐山線
  - 奈良縣道135号宇太三茶屋線
  - 奈良縣道164号室生口大野停車場線
  - 奈良縣道198号粟原榛原線
  - 奈良縣道217号高塚野依線
  - 奈良縣道218号内牧菟田野線
  - 奈良縣道219号佐倉大宇陀線
  - 奈良縣道242号上笠間三本松停車場線
  - 奈良縣道・三重縣道781号都祁名張線
  - 奈良縣道・三重縣道782号上笠間八幡名張線

## 名勝・古跡

[Udamikumari_Shr_Kami.jpg](https://zh.wikipedia.org/wiki/File:Udamikumari_Shr_Kami.jpg "fig:Udamikumari_Shr_Kami.jpg")
[Mikumari_Zakura01.jpg](https://zh.wikipedia.org/wiki/File:Mikumari_Zakura01.jpg "fig:Mikumari_Zakura01.jpg")
[Uda_Matsuyama20s3872.jpg](https://zh.wikipedia.org/wiki/File:Uda_Matsuyama20s3872.jpg "fig:Uda_Matsuyama20s3872.jpg")

  - [室生寺](../Page/室生寺.md "wikilink")（有[国宝五重塔](../Page/日本國寶.md "wikilink")）

  -
  -
  -
  -
  -
  - （有国宝社殿）

  - 水分櫻（菟田野） -
    芳野川堤防上的約100棵並排的[樱花樹](../Page/樱花.md "wikilink")，開花期間在燈光照耀下倒映於江面。

  - （瀧櫻）

  - \-
    作為商家町於2006年（平成18年）7月5日被選定為[重要傳統的建造物群保存地區](../Page/重要傳統的建造物群保存地區.md "wikilink")（17公頃）。

      - 松山西口關門（国之[史跡](../Page/史跡.md "wikilink")） - 松山城下町旧大手門，通称黑門

      - 森野舊藥園（国之史跡） - 日本最古老的民間藥草園

      - （旧細川家住宅） - 江戶時代末期築、以「藥之館」之形式開放。

      - （松山地區城市規劃中心「千軒舍」）（旧内藤家）

      - 春日神社

  - \-
    旧縣社、[式內社](../Page/式內社.md "wikilink")。有[能剧舞台](../Page/能剧.md "wikilink")。

  - 大願寺

  - 光明寺

  - 阿騎野・人麻呂公園 - 有遺跡公園、復元建造物、[柿本人麻呂像等](../Page/柿本人麻呂.md "wikilink")

  - 万葉公園（）

  - 大龜和尚民藝館

  - 平井大師寺（菟田野）

  - [風車自生地](../Page/转子莲.md "wikilink")（国之[天然記念物](../Page/天然記念物.md "wikilink")）

## 參考資料

## 外部連結

  -

  -

<!-- end list -->

1.
2.
3.