**安七炫**（[韓語](../Page/韓語.md "wikilink")：**안칠현**，），藝名**Kangta**（韓語：**강타**，有"強擊"、"強打"之意），[韓國](../Page/韓國.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")、[男演員](../Page/男演員.md "wikilink")、[舞者](../Page/舞者.md "wikilink")、[詞曲作家](../Page/詞曲作家.md "wikilink")、[音樂製作人和](../Page/音樂製作人.md "wikilink")[廣播主持人](../Page/主持人.md "wikilink")，所屬公司是[SM娛樂](../Page/SM娛樂.md "wikilink")。1996年以韓國作為[韓國人氣組合](../Page/韓國.md "wikilink")[H.O.T.的主唱出道](../Page/H.O.T..md "wikilink")，2001年5月H.O.T.解散後展開獨立發展，於同年8月發行了第一張個人專輯《北極星願》，獲得[Mnet亞洲音樂大獎](../Page/Mnet亞洲音樂大獎.md "wikilink")「男子獨唱領域獎」。出道至今創作過上百首歌曲\[1\]，包括[NRG](../Page/NRG_\(團體\).md "wikilink")、[Fly
to the
Sky等團體和女歌手](../Page/Fly_to_the_Sky.md "wikilink")[寶兒等都曾採用他的創作](../Page/寶兒.md "wikilink")。2003年，他與好友[申彗星](../Page/申彗星.md "wikilink")（[神話主唱](../Page/Shinhwa.md "wikilink")）、[李志勳組成團體](../Page/李志勳.md "wikilink")「S」，意指「Supreme」，發行了第一張專輯《Fr.
In. Cl.》（意為Friends in Classic），主打歌〈I
swear〉的詞曲創作便是由他一手包辦。2006年初，與[F4成員](../Page/F4_\(男子團體\).md "wikilink")[吳建豪組成團體](../Page/吳建豪.md "wikilink")「Kangta
&
Vanness」，於全亞洲推出專輯《Scandal》。2013年擔任《韓國好聲音》第二季導師；同年11月加盟中國戶外公益旅遊節目《[兩天一夜](../Page/兩天一夜.md "wikilink")》。2014年10月重返「S」，發行專輯《Autumn
Breeze》回歸樂壇。2016年4月9日，榮獲第16屆[音樂風雲榜年度盛典](../Page/音樂風雲榜.md "wikilink")「韓流偶像大獎」。2018年2月15日，與H.O.T.其餘四名成員合體演出。

## 經歷

1995年，在[樂天世界遊樂園跳舞而被SM娛樂公司相中](../Page/樂天世界.md "wikilink")，與[文熙俊](../Page/文熙俊.md "wikilink")、[張佑赫](../Page/張佑赫.md "wikilink")、[Tony
An](../Page/安勝浩.md "wikilink")（安勝浩）、[李在元組成](../Page/李載沅.md "wikilink")[男孩團體](../Page/男孩團體.md "wikilink")[H.O.T.](../Page/H.O.T..md "wikilink")\[2\]。1996年9月7日，H.O.T.發行首張專輯《[反對一切暴力](../Page/反對一切暴力.md "wikilink")》，並在[MBC
TV熱門節目](../Page/MBC_TV.md "wikilink")《》第497集獲得罕見的20分鐘新人曝光，正式出道\[3\]。H.O.T.出道三個月便取得巨大成功，自此之後一直位居韓國男孩團體的首位（直到2001年5月13日解散為止）\[4\]。他創作出多首歌曲，如〈希望〉、〈制式愛情〉、〈聖誕婚禮〉、〈我的妳〉、〈祝福〉、〈歡喜〉、〈神秘〉、〈夢中祈禱〉、〈永恆不變〉等等。2001年5月，H.O.T.成員分散不同公司後結束活動。安七炫與文熙俊在[首爾](../Page/首爾.md "wikilink")[新羅酒店召開記者會](../Page/新羅酒店.md "wikilink")，宣布續留SM娛樂、展開個人活動並擔任該公司的作曲家和音樂監製\[5\]。

2001年8月16日，發行第一張個人專輯《北極星願》，獲得[Mnet亞洲音樂大獎男子獨唱領域獎](../Page/Mnet亞洲音樂大獎.md "wikilink")。2002年8月21日，發行第二張個人專輯《望情樹》。2003年，與[申彗星](../Page/申彗星.md "wikilink")（[神話主唱](../Page/Shinhwa.md "wikilink")）、[李志勳組成團體](../Page/李志勳.md "wikilink")「S」，意指「Supreme」，發行第一張專輯《Fr.
In.
Cl.》。2004年12月31日，參與[中國中央電視台元旦晚會的表演](../Page/中國中央電視台.md "wikilink")，演唱〈吻別〉。2004年，與台灣演員[林心如](../Page/林心如.md "wikilink")、[蘇有朋合作演出電視連續劇](../Page/蘇有朋.md "wikilink")《魔術奇緣》。2005年3月4日，回到韓國發行第三張個人專輯《面具》。2005年，經SM娛樂提拔，擔任該公司訓練藝人的理事、創意總監及8,000股認股權\[6\]。2006年初，與[F4成員](../Page/F4_\(男子團體\).md "wikilink")[吳建豪組成團體](../Page/吳建豪.md "wikilink")「Kangta
& Vanness」，於全亞洲推出專輯《Scandal》。2007年，與黎萱出演電視連續劇《男才女貌2》\[7\]。

2008年3月12號，安七炫推出入伍前夕的專輯《Eternity-永遠》，並在3月29日與30日兩天，於[延世大學大講堂舉辦告別演唱會](../Page/延世大學.md "wikilink")。4月1日，他在歌迷與好友的歡送下，進入[京畿道議政府](../Page/京畿道.md "wikilink")306補充兵營服役。他曾在上節目時透露不打算加入藝工隊，希望當一般兵，從中學習。17歲就出道的他說：「我雖然很早就出社會，但卻沒有經歷過平常人的社會生活，希望藉著當一般兵學習」。2009年，因在軍中優異表現，被授予「特級戰士」稱號。2010年2月19日退伍\[8\]，翌日在首爾舉行首場粉絲見面會，7月24日，在[北京](../Page/北京.md "wikilink")[五棵松體育館舉辦個人演唱會](../Page/凯迪拉克中心.md "wikilink")。

2011年，主演中國的古裝電視劇《[帝锦](../Page/帝锦.md "wikilink")》，飾演皇帝凌宣，是首位外國人身分飾演中國的皇帝角色。

2012年，年初，主演中國的中韓家庭輕喜劇《辣椒與泡菜》開播。2月，與白智英（[白智榮](../Page/白智榮.md "wikilink")）、申勝勛、吉（[吉成俊](../Page/吉成俊.md "wikilink")）共同擔任韓國好聲音（The
Voice of Korea）第一季導師。並参演韩國電視劇《完美结局Happy
Enging》的拍摄，他在劇中飾演“具承載”。12月，參與首部中國的電影《[秘密花園](../Page/秘密花園.md "wikilink")》，飾演大明星奧斯卡，與[鍾漢良](../Page/鍾漢良.md "wikilink")、[譚維維合作](../Page/譚維維.md "wikilink")。

2013年，擔任韓國SBS連續劇《[那年冬天風在吹](../Page/那年冬天風在吹.md "wikilink")》的OST製作人，並詞曲創作《墨紙》（演唱：[Super
Junior](../Page/Super_Junior.md "wikilink")[藝聲](../Page/藝聲.md "wikilink")）《還有一個》（演唱：[少女時代](../Page/少女時代.md "wikilink")[泰妍](../Page/泰妍.md "wikilink")）。擔任韓國好聲音（The
Voice of Korea）第二季導師，指導學生李藝俊奪得冠軍。10月，參加中國戶外旅遊節目[兩天一夜
(中國版)](../Page/兩天一夜_\(中國版\).md "wikilink")，與[吳宗憲](../Page/吳宗憲.md "wikilink")、[馬可](../Page/馬可.md "wikilink")、[張睿](../Page/張睿.md "wikilink")、[姜超和](../Page/姜超.md "wikilink")[朱梓驍共同擔任主持人](../Page/朱梓驍.md "wikilink")。11月30日，在北京出席“世界好聲音”演唱会嘉賓。

2014年10月27日，與「S」重返樂壇，發行迷你專輯《Autumn Breeze》，安七炫負責整張專輯詞曲與製作。

2014年11月22日與11月29日，「S」三人首次參加不朽的名曲，得到一勝。因為有很好的迴響，所以和李智勛兩人又受邀參加了七次節目（共九集）。

2015年4月6日，獲得SM娛樂10,000股認股權\[9\]。

2015年8月6日，首爾市長到中國北京宣傳推廣首爾旅遊，安七炫被首爾市長朴元淳授予「首爾名譽旅遊宣傳大使」。

2015年9月20日，和李智勳在東京豊洲PIT舉行兩場歌迷見面會。

2018年2月15日，在首爾[奧林匹克大廳與H](../Page/奧林匹克公園_\(首爾\).md "wikilink").O.T.正式合體演出。

## 音樂作品

### H.O.T.時期

*---詳細內容見[H.O.T.](../Page/H.O.T..md "wikilink")---*

  - 第1輯：《[反對一切暴力](../Page/反對一切暴力.md "wikilink")》
  - 第2輯：《[狼與羊](../Page/狼與羊.md "wikilink")》
  - 第3輯：《[復甦](../Page/復甦.md "wikilink")》
  - 第4輯：《[孩子！](../Page/孩子！.md "wikilink")》
  - 第5輯：《[大愛](../Page/局外人的孤堡.md "wikilink")》

### 個人韓文專輯

#### 正規專輯

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>首張正規專輯《<strong><a href="../Page/北極星願.md" title="wikilink">北極星願</a></strong>》</p>
<ul>
<li>發行日期：2001年8月16日</li>
<li>語言：<a href="../Page/韓語.md" title="wikilink">韓語</a></li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2nd</strong></p></td>
<td><p>第二張正規專輯《<strong><a href="../Page/望情樹.md" title="wikilink">望情樹</a></strong>》</p>
<ul>
<li>發行日期：2002年8月21日</li>
<li>語言：<a href="../Page/韓語.md" title="wikilink">韓語</a></li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3rd</strong></p></td>
<td><p>第三張正規專輯《<strong><a href="../Page/面具.md" title="wikilink">面具</a></strong>》</p>
<ul>
<li>發行日期：2005年3月4日</li>
<li>語言：<a href="../Page/韓語.md" title="wikilink">韓語</a></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### 精選專輯

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>首張正規專輯《<strong><a href="../Page/Kang_Ta_&amp;_Best.md" title="wikilink">Kang Ta &amp; Best</a></strong>》</p>
<ul>
<li>發行日期：2007年1月30日</li>
<li>語言：韓語</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### 迷你專輯

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>暫別歌壇專輯《<strong><a href="../Page/永遠(Eternity).md" title="wikilink">永遠(Eternity)</a></strong>》</p>
<ul>
<li>發行日期：2008年3月14日</li>
<li>語言：韓語</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1st</strong></p></td>
<td><p>首張迷你專輯《<strong><a href="../Page/&#39;HOME&#39;_Chapter_1.md" title="wikilink">'HOME' Chapter 1</a></strong>》</p>
<ul>
<li>發行日期：2016年11月3日</li>
<li>語言：韓語</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

### 個人中文專輯

#### 迷你專輯

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>首張中文迷你專輯《<strong><a href="../Page/靜享七樂.md" title="wikilink">靜享七樂</a></strong>》</p>
<ul>
<li>發行日期：2010年9月13日</li>
<li>語言：國語</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 限定/企劃組合

#### Group S (Supreme) ：安七炫，**[李志勳](../Page/李志勳.md "wikilink")**，**[申彗星](../Page/申彗星.md "wikilink")**

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>首張Project專輯《<strong><a href="../Page/Friend_in_Classic.md" title="wikilink">Friend in Classic</a></strong>》</p>
<ul>
<li>發行日期：2003年9月724日</li>
<li>語言：韓語</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2nd</strong></p></td>
<td><p>迷你專輯《Autumn Breeze》</p>
<ul>
<li>發行日期：2014年10月27日</li>
<li>語言：韓語</li>
<li>唱片公司：S.M.Entertainment</li>
<li>詞，曲，製作：安七炫</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### Kangta & Vanness：安七炫，吳建豪

<table>
<thead>
<tr class="header">
<th><p>專輯 #</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>首張合作專輯《<strong><a href="../Page/SCANDAL.md" title="wikilink">SCANDAL</a></strong>》</p>
<ul>
<li>發行日期：2006年5月19日</li>
<li>語言：韓語、國語、英語</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

### OST

<table>
<thead>
<tr class="header">
<th><p>單曲 #</p></th>
<th><p>單曲資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>《<strong><a href="../Page/Arrow.md" title="wikilink">Arrow</a></strong>》</p>
<ul>
<li>發行日期：2011年1月18日</li>
<li>語言：<a href="../Page/韓語.md" title="wikilink">韓語</a></li>
<li>備註：收錄於《雅典娜:戰爭女神OST》</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

## 影視作品

### 電視劇

  - 1998年：[KBS](../Page/KBS.md "wikilink")《[學校](../Page/學校_\(韓國電視劇\).md "wikilink")》
  - 2002年：《Emergency Act 19 (韓國電影)》
  - 2005年：KBS《[愛情中毒](../Page/愛情中毒.md "wikilink")》飾演 徐姜旭
  - 2005年：《[魔術奇緣](../Page/魔術奇緣.md "wikilink")》飾演 金修（中韓合拍）
  - 2007年：《[男才女貌2](../Page/男才女貌2.md "wikilink")》飾演 張偉齊（中韓合拍）
  - 2007年：《[丁家有女喜洋洋](../Page/丁家有女喜洋洋.md "wikilink")》飾演 安七炫（中韓合拍）
  - 2011年：《[辣椒與泡菜](../Page/辣椒與泡菜.md "wikilink")》飾演 宇赫
  - 2011年：《[帝錦](../Page/帝錦.md "wikilink")》飾演 凌宣
  - 2011年：[JTBC](../Page/JTBC.md "wikilink")《[Happy
    Ending](../Page/Happy_Ending.md "wikilink")》 飾演 具承宰
  - 待播：《[朋友圈兒之溫州媳婦](../Page/朋友圈兒之溫州媳婦.md "wikilink")》

### 電影

  - 2002年：《[賣火柴女孩的再臨](../Page/賣火柴女孩的再臨.md "wikilink")》飾演 何俊吾（特別出演）
  - 2012年：《I AM. (韓國電影)》飾演 自己
  - 2012年：《秘密花園 (中國電影)》飾演 奧斯卡
  - 2016年：《[济公 (2016年电影)](../Page/济公_\(2016年电影\).md "wikilink")》

### 綜藝節目

  - 2010年：[MBC](../Page/MBC.md "wikilink")《[無限挑戰](../Page/無限挑戰.md "wikilink")》7月31日
    偶像選秀（上）
  - 2013年：《[两天一夜](../Page/两天一夜_\(中国版\).md "wikilink")(第一季)》固定班底
  - 2013年7月15日：[tvN](../Page/tvN.md "wikilink")《[現場脫口秀Taxi](../Page/現場脫口秀Taxi.md "wikilink")》
  - 2013年9月12日：[MBC](../Page/MBC.md "wikilink")《[黃金漁場 Radio
    Star](../Page/黃金漁場_Radio_Star.md "wikilink")》E344
  - 2015年：《[叮咯咙咚呛](../Page/叮咯咙咚呛.md "wikilink")》
  - 2015年：[SBS](../Page/SBS.md "wikilink")《[Healing
    Camp](../Page/Healing_Camp.md "wikilink")》 Ep208 (視頻出演)
  - 2016年7月27日：[MBC](../Page/MBC.md "wikilink")《[黃金漁場 Radio
    Star](../Page/黃金漁場_Radio_Star.md "wikilink")》E488
  - 2016年10月28日：[KBS2](../Page/KBS2.md "wikilink")《[歌曲之爭—勝負](../Page/歌曲之爭—勝負.md "wikilink")》E02
  - 2016年10月29日：[KBS](../Page/KBS.md "wikilink")《[戰鬥旅行](../Page/戰鬥旅行.md "wikilink")》E25
    (和Tony An)
  - 2016年10月29日：[JTBC](../Page/JTBC.md "wikilink")《[認識的哥哥](../Page/認識的哥哥.md "wikilink")》E48
  - 2016年10月31日：[JTBC](../Page/JTBC.md "wikilink")《[非首腦會談](../Page/非首腦會談.md "wikilink")》E122
  - 2016年11月2日：[tvN](../Page/tvN.md "wikilink")《[周三美食匯](../Page/周三美食匯.md "wikilink")》
    E90
  - 2016年11月4日：[KBS2](../Page/KBS2.md "wikilink")《[歌曲之爭—勝負](../Page/歌曲之爭—勝負.md "wikilink")》E03
  - 2016年11月5日：[KBS](../Page/KBS.md "wikilink")《[柳熙烈的寫生簿](../Page/柳熙烈的寫生簿.md "wikilink")》
    E340
  - 2016年11月5日：[KBS](../Page/KBS.md "wikilink")《[戰鬥旅行](../Page/戰鬥旅行.md "wikilink")》
    E26 (和Tony An)
  - 2016年12月18日：[MBC](../Page/MBC.md "wikilink")《[隱秘而偉大](../Page/隱秘而偉大.md "wikilink")》E3（主人公）
  - 2017年1月13日：[tvN](../Page/tvN.md "wikilink")《[把便利店掏空吧](../Page/把便利店掏空吧.md "wikilink")》E01-至今
  - 2017年9月17日、9月24日 ：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[Fantastic
    Duo S2](../Page/Fantastic_Duo_S2.md "wikilink")》E25-26
  - 2018年2月17日、2月24日
    ：[MBC](../Page/MBC.md "wikilink")《[無限挑戰](../Page/無限挑戰.md "wikilink")》E557-558
    [六六歌3－H.O.T.篇](../Page/星期六星期六是歌手第三季_－_H.O.T..md "wikilink")
  - 2018年5月5日：[MBC](../Page/MBC.md "wikilink")《[意外的Q](../Page/意外的Q.md "wikilink")》E1

## 獎項

### 大型頒獎禮獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001</p></td>
<td><ul>
<li>M.net 音樂電視慶典 - 男子獨唱領域獎</li>
<li>第9屆韓國最高人氣演藝大獎頒獎典禮 - 青少年組歌曲大獎</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><ul>
<li>中國時尚大獎頒獎典禮 - 韓日最佳服裝獎</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><ul>
<li>韓國文化觀光部 - 年輕藝術家獎</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><ul>
<li>第14届韩国演艺艺术奖 - 演艺艺术发展功劳奖</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><ul>
<li>第2屆酷音樂亞洲盛典－酷音樂韓流推廣貢獻大獎</li>
<li>第16屆音樂風雲榜年度盛典－韩流偶像大奖</li>
<li>MBC演藝大賞－廣播部門新人獎</li>
</ul></td>
</tr>
</tbody>
</table>

### 音樂節目獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>日期</p></th>
<th><p>電視台</p></th>
<th><p>節目名稱</p></th>
<th><p>獲獎歌曲</p></th>
<th><p>排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>9月22日</p></td>
<td><p><a href="../Page/MBC.md" title="wikilink">MBC</a></p></td>
<td></td>
<td><p>Polaris</p></td>
<td><p>1位</p></td>
</tr>
<tr class="even">
<td><p>9月29日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>10月13日</p></td>
<td><p><a href="../Page/SBS.md" title="wikilink">SBS</a></p></td>
<td><p><a href="../Page/人氣歌謠.md" title="wikilink">人氣歌謠</a></p></td>
<td><p>Memories</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>10月19日</p></td>
<td><p>I Swear（Mutizen Song）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10月26日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>11月1日</p></td>
<td><p><a href="../Page/MBC.md" title="wikilink">MBC</a></p></td>
<td></td>
<td><p>I Swear| 1位</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月2日</p></td>
<td><p><a href="../Page/SBS.md" title="wikilink">SBS</a></p></td>
<td><p><a href="../Page/人氣歌謠.md" title="wikilink">人氣歌謠</a></p></td>
<td><p>I Swear（Mutizen Song）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

## 外部链接

  - [H.O.T 歷史](../Page/:H.O.T..md "wikilink")
  - [H.O.T
    官方网站](https://web.archive.org/web/20070212041557/http://www.smtown.com/smtown/hot/index.html)
  - [安七炫的Instagram帳戶](https://www.instagram.com/an_chil_hyun/)

[Category:H.O.T.](../Category/H.O.T..md "wikilink")
[Category:SM娛樂](../Category/SM娛樂.md "wikilink")
[Category:韓國男歌手](../Category/韓國男歌手.md "wikilink")
[Category:韓語流行音樂歌手](../Category/韓語流行音樂歌手.md "wikilink")
[Category:韓國華語流行音樂歌手](../Category/韓國華語流行音樂歌手.md "wikilink")
[Category:韓國流行音樂歌手](../Category/韓國流行音樂歌手.md "wikilink")
[Category:韓國創作歌手](../Category/韓國創作歌手.md "wikilink")
[Category:韓國節奏藍調歌手](../Category/韓國節奏藍調歌手.md "wikilink")
[Category:韓國舞者](../Category/韓國舞者.md "wikilink")
[Category:韓國音樂製作人](../Category/韓國音樂製作人.md "wikilink")
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:東國大學校友](../Category/東國大學校友.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Chil](../Category/安姓.md "wikilink")
[Category:MTV亞洲大獎獲得者](../Category/MTV亞洲大獎獲得者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.