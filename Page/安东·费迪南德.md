**安東·費迪南**（，）是一名[英格蘭職業足球員](../Page/英格蘭.md "wikilink")，出身於[韋斯咸青訓系統](../Page/西汉姆联足球俱乐部.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，2008年季初加盟[新特蘭](../Page/桑德兰足球俱乐部.md "wikilink")，其後於2011年夏季轉投新升[英超的](../Page/英超.md "wikilink")[昆士柏流浪](../Page/女王公园巡游者足球俱乐部.md "wikilink")，於在[土耳其渡過一個半球季後](../Page/土耳其.md "wikilink")，在2014年重返英格蘭加盟[英冠球會](../Page/英冠.md "wikilink")[雷丁](../Page/雷丁足球俱乐部.md "wikilink")。

其兄為現時已退役的[里奧·費迪南](../Page/里奧·費迪南.md "wikilink")。

## 生平

### 球會

  - 韋斯咸

安东·费迪南德與其兄里奥·费迪南德都是於[韋斯咸接受足球訓練](../Page/西汉姆联足球俱乐部.md "wikilink")，其後，里奧費迪南加盟[列斯聯](../Page/利兹联足球俱乐部.md "wikilink")，再於2002年至2003年的球季成為[曼聯的一份子](../Page/曼聯.md "wikilink")。

而弟弟安東費迪南則依然效力韋斯咸，他於2003年8月球會對[普雷斯頓時首次於一線隊上陣](../Page/普雷斯顿足球俱乐部.md "wikilink")，在該季中共些陣26場球賽。次季，安東費迪南在對[屈福特時得到他在球會中的第一個進球](../Page/沃特福德足球俱乐部.md "wikilink")，在效力球會的第3季中，安東費迪南得到球會的認同，與安東費迪南續約至2010年的6月。

  - 新特蘭

安東·費迪南在2008年8月27日轉投[新特蘭](../Page/桑德兰足球俱乐部.md "wikilink")，簽約4年\[1\]。

  - 昆士柏流浪

2011年8月31日與新特蘭尚餘一年合約的安東·費迪南轉投[昆士柏流浪](../Page/女王公园巡游者足球俱乐部.md "wikilink")，簽約三年\[2\]。

  - 布尔萨体育 (租借)

2013年1月29日，安東·費迪南被借用到[土超前列球會](../Page/土超.md "wikilink")[布尔萨体育至](../Page/布尔萨体育.md "wikilink")2012/13球季結朿\[3\]。

2013年8月9日，昆士柏流浪與安東·費迪南雙方達成協議提早解約\[4\]。

  - 安塔利亞

2013年8月13日，剛回復自由身的安東·費迪南加盟[土超球會](../Page/土超.md "wikilink")，合約為期三年\[5\]。

  - 雷丁

2014年8月11日，安東·費迪南轉投[英冠球會](../Page/英冠.md "wikilink")[雷丁](../Page/雷丁足球俱乐部.md "wikilink")，合約為期兩年\[6\]。

### 國家隊

在國家隊中，安東費迪南在2004年8月17日首次為英格蘭21歲以下足球代表隊上陣，當時的對手是[烏克蘭](../Page/烏克蘭國家足球隊.md "wikilink")21歲以下國家足球隊。現時，安東費迪南的目標是能與其兄里奧費迪南能在將來共同地在[英格蘭國家隊中上陣](../Page/英格蘭國家足球隊.md "wikilink")。

## 參考資料

## 外部链接

  -
  - [安東費迪南新特蘭官方球員介紹](http://www.safc.com/team/?page_id=2623&player_id=225)

[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:韋斯咸球員](../Category/韋斯咸球員.md "wikilink")
[Category:新特蘭球員](../Category/新特蘭球員.md "wikilink")
[Category:昆士柏流浪球員](../Category/昆士柏流浪球員.md "wikilink")
[Category:布爾薩球員](../Category/布爾薩球員.md "wikilink")
[Category:雷丁球員](../Category/雷丁球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:种族和法律](../Category/种族和法律.md "wikilink")

1.  [安東費迪南完成新特蘭轉會](http://news.bbc.co.uk/sport2/hi/football/teams/s/sunderland/7580244.stm)
2.
3.
4.
5.
6.