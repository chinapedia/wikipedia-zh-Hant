[Safe_mode.PNG](https://zh.wikipedia.org/wiki/File:Safe_mode.PNG "fig:Safe_mode.PNG")

**安全模式**（英語：Safe
Mode）通常指作業系統的一種特殊[偵錯模式與一些軟件的一種運行模式](../Page/偵錯.md "wikilink")。

## 作業系統的安全模式

對於[微軟視窗](../Page/微軟視窗.md "wikilink")、[Mac
OS等](../Page/Mac_OS.md "wikilink")[作業系統來講](../Page/作業系統.md "wikilink")，安全模式是一種特別操作模式，用於系統除錯。當系統在安全模式時，系統的功能會減少（例如以[DirectX](../Page/DirectX.md "wikilink")
API寫成的程式會不能運行），但是因為很多非內核的程式不會運行，所以比較容易，而且更適合於[除錯](../Page/除錯.md "wikilink")。一個安裝好的作業系統只會在有重大問題時進入安全模式，例如[硬碟問題](../Page/硬碟.md "wikilink")、中[電腦病毒和安裝了一些有問題或設計不良的軟件](../Page/電腦病毒.md "wikilink")，而該軟件就阻礙作業系統的運作。

每個作業系統的安全模式也有不同。一般來說，作業系統在進入安全模式時，只會載入小量的，必須的程式和驅動程式，而大部分的裝置均不能使用，除了最小的顯示與輸入需要外，其他的[驅動程式均不會載入](../Page/驅動程式.md "wikilink")。

安全模式也可以作為一種平衡的，微型化的作業系統存在，這些作業系統與安裝在電腦上的作業系統之間沒有任何共用的設定參數。
例如[微軟視窗的](../Page/微軟視窗.md "wikilink")「[修復主控台](../Page/修復主控台.md "wikilink")」－－一種基於[命令列介面的偵錯模式](../Page/命令列介面.md "wikilink")，與微軟視窗本身分離。而微軟視窗本身也有數種不同的安全模式，啟動時不載入大部分的驅動程式。

安全模式通常提供不同偵錯工具，供用戶解決系統上的問題，使電腦回復正常。

安全模式也是絕大部分電子產品的除錯模式，例如[手提電話與](../Page/手提電話.md "wikilink")[太空船](../Page/太空船.md "wikilink")。

### 進入安全模式的方法

微軟視窗的安全模式可以透過在開機時不斷按F8鍵進入（雖然微軟網站建議只需要按F8鍵一次，但是通常要按幾次才能進入）。對於安裝了數套作業系統，使用[多重開機環境的電腦](../Page/多重開機.md "wikilink")，在選擇作業系統的選單彈出時，按一次F8就可。

在類[Unix的作業系統](../Page/Unix.md "wikilink")（如：[Linux](../Page/Linux.md "wikilink")）裏，相類似的模式是[單用戶模式](../Page/單用戶模式.md "wikilink")，在這模式下，[X視窗不會載入](../Page/X視窗.md "wikilink")，並且只有根用戶才能登入。在Mac
OS
6、7、8、9，類似的模式可以透過在開機時長按[Shift鍵進入](../Page/Shift.md "wikilink")，以在不載入任何外掛的情況下載入作業系統（在Mac
OS 10，開機時長按Shift鍵會進入安全模式）。

### 帶網絡功能的安全模式

**帶網絡功能的安全模式**是一種安全模式的變體，供用戶解決網絡問題。

## 軟件的安全模式

應用軟體有時也會使用安全模式，在第六版之前的[PHP編譯器](../Page/PHP編譯器.md "wikilink")，安全模式提供更嚴格的保安措施。

[Mozilla
Firefox的安全模式容許用戶移除阻礙](../Page/Mozilla_Firefox.md "wikilink")[瀏覽器載入的外掛](../Page/瀏覽器.md "wikilink")，相對地，Internet
Explorer可以運行「不載入外掛」模式與保護模式。

## 參看

  - [作業系統](../Page/作業系統.md "wikilink")
  - [當機](../Page/當機.md "wikilink")

## 外部連結

  - 如何啟動電腦至「安全模式」<https://web.archive.org/web/20050303001446/http://service1.symantec.com/SUPPORT/INTER/traditionalchinesekb.nsf/twdocid/20021018150235932>
  - Windows XP 安全模式開機選項說明<http://support.microsoft.com/kb/315222/zh-tw>
  - 你知道什麼是安全模式嗎？ -
    IT邦幫忙::IT知識分享社群:<http://ithelp.ithome.com.tw/question/10003131>
  - [使用Win10安全模式](https://www.ytyzx.org/index.php/Windows_10\(WIN10\)%E5%A6%82%E4%BD%95%E8%BF%9B%E5%AE%89%E5%85%A8%E6%A8%A1%E5%BC%8F)

[Category:操作系統](../Category/操作系統.md "wikilink")