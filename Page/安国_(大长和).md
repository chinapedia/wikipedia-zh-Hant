**安国**（903年-909年）是[大长和](../Page/大长和.md "wikilink")[郑买嗣的](../Page/郑买嗣.md "wikilink")[年号](../Page/年号.md "wikilink")，共计7年。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|安国||元年||二年||三年||四年||五年||六年||七年 |-
style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||903年||904年||905年||906年||907年||908年||909年
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[癸亥](../Page/癸亥.md "wikilink")||[甲子](../Page/甲子.md "wikilink")||
[乙丑](../Page/乙丑.md "wikilink")|| [丙寅](../Page/丙寅.md "wikilink")||
[丁卯](../Page/丁卯.md "wikilink")|| [戊辰](../Page/戊辰.md "wikilink")||
[己巳](../Page/己巳.md "wikilink") |}

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天復](../Page/天復.md "wikilink")（901年四月至904年閏四月）：唐朝唐昭宗之年號
      - [天祐](../Page/天祐_\(唐昭宗\).md "wikilink")（904年閏四月至907年三月）：唐朝—唐昭宗、[唐哀帝之年號](../Page/唐哀帝.md "wikilink")
      - [開平](../Page/開平_\(朱全忠\).md "wikilink")（907年四月至911年四月）：[後梁](../Page/後梁.md "wikilink")—[朱全忠之年號](../Page/朱全忠.md "wikilink")
      - [天佑](../Page/天祐_\(李克用\).md "wikilink")（907年四月至908年）：[晉](../Page/後唐.md "wikilink")—[李克用之年號](../Page/李克用.md "wikilink")
      - [天佑](../Page/天祐_\(李存勗\).md "wikilink")（909年至923年）：晉—[李存勗之年號](../Page/李存勗.md "wikilink")
      - [天佑](../Page/天祐_\(李茂貞\).md "wikilink")（907年四月至924年）：[岐](../Page/岐.md "wikilink")—[李茂貞之年號](../Page/李茂貞.md "wikilink")
      - [天復](../Page/天復_\(楊行密\).md "wikilink")（902年三月至904年）：[吳](../Page/吳_\(十國\).md "wikilink")—[楊行密之年號](../Page/楊行密.md "wikilink")
      - [天祐](../Page/天祐_\(楊行密\).md "wikilink")（904年月至919年）：吳—楊行密、[楊渥之年號](../Page/楊渥.md "wikilink")
      - [天祐](../Page/天祐_\(錢鏐\).md "wikilink")（907年五月）：[吳越](../Page/吳越.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [天寳](../Page/天宝_\(钱镠\).md "wikilink")（908年正月至912年十二月）：[吳越](../Page/吳越.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [天復](../Page/天復_\(王建\).md "wikilink")（907年九月）：[前蜀](../Page/前蜀.md "wikilink")—[王建之年號](../Page/王建_\(前蜀\).md "wikilink")
      - [武成](../Page/武成_\(王建\).md "wikilink")（908年正月至910年十二月）：前蜀—王建之年號
      - [武泰](../Page/武泰_\(泰封\).md "wikilink")（904年—905年）：[後高句麗](../Page/後高句麗.md "wikilink")[金弓裔之年號](../Page/金弓裔.md "wikilink")
      - [聖冊](../Page/聖冊.md "wikilink")（905年—911年）：後高句麗—金弓裔之年號
      - [延喜](../Page/延喜.md "wikilink")（901年七月十五日至923年閏四月十一日）：平安時代醍醐天皇年号

[Category:南诏年号](../Category/南诏年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:900年代中国政治](../Category/900年代中国政治.md "wikilink")