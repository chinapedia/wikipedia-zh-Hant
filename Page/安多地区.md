[Map_of_Tibet_Ü-Tsang_Amdo_and_Kham.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Tibet_Ü-Tsang_Amdo_and_Kham.jpg "fig:Map_of_Tibet_Ü-Tsang_Amdo_and_Kham.jpg")传统边界\]\]
[Tibet_provinces.png](https://zh.wikipedia.org/wiki/File:Tibet_provinces.png "fig:Tibet_provinces.png")传统分区：[衛藏](../Page/衛藏.md "wikilink")（紅）、安多（藍）、[康区](../Page/康区.md "wikilink")（綠）\]\]

**安多**（），源自[藏族传统文化](../Page/藏族.md "wikilink")，是藏地的區劃之一，常与[衛藏和](../Page/衛藏.md "wikilink")[康并列](../Page/康区.md "wikilink")。安多位于[青藏高原东部](../Page/青藏高原.md "wikilink")，其範圍大致相當於今[中华人民共和国](../Page/中华人民共和国.md "wikilink")[青海省的](../Page/青海省.md "wikilink")[海北](../Page/海北藏族自治州.md "wikilink")、[海南](../Page/海南藏族自治州.md "wikilink")、[黃南](../Page/黃南藏族自治州.md "wikilink")、[果洛四個藏族](../Page/果洛藏族自治州.md "wikilink")[自治州](../Page/自治州.md "wikilink")、[甘肅省的](../Page/甘肅省.md "wikilink")[甘南藏族自治州和](../Page/甘南藏族自治州.md "wikilink")[四川省的](../Page/四川省.md "wikilink")[阿坝藏族羌族自治州北部](../Page/阿坝藏族羌族自治州.md "wikilink")。

## 释义

安多又称为**多麥**（），意思是[朵甘思的上部](../Page/朵甘思.md "wikilink")（北部）。在[元朝时期译为](../Page/元朝.md "wikilink")**朵思麻**或**脫思麻**。

安多与[康区形成一个更大的地区](../Page/康区.md "wikilink")，在[吐蕃王朝时期称爲](../Page/吐蕃王朝.md "wikilink")**[多康](../Page/多康.md "wikilink")**（），又譯**朵甘斯**、**朵甘**，意思是匯合的区域；在[元朝称为](../Page/元朝.md "wikilink")[朵甘思](../Page/朵甘思.md "wikilink")。

## 文化

[Kumbum_Monastery_in_Amdo.jpg](https://zh.wikipedia.org/wiki/File:Kumbum_Monastery_in_Amdo.jpg "fig:Kumbum_Monastery_in_Amdo.jpg")\]\]
在安多，最有名的寺院是[拉卜楞寺](../Page/拉卜楞寺.md "wikilink")（甘肃省），[瞿昙寺和](../Page/瞿昙寺.md "wikilink")[塔尔寺](../Page/塔尔寺.md "wikilink")（青海省）。

安多地區主要通行藏語[安多方言](../Page/安多方言.md "wikilink")，藏文是根據[近古藏語的發音書寫](../Page/近古藏語.md "wikilink")，所以不受方言影響，藏區各地都一樣。

## 历史

在[吐蕃帝国兴起之前](../Page/吐蕃帝国.md "wikilink")，安多为[苏毗](../Page/苏毗.md "wikilink")、[吐谷浑等国的地区](../Page/吐谷浑.md "wikilink")，后都被吐蕃吞并。吐蕃崩溃之后，吐蕃王室末裔[唃厮啰在安多建立](../Page/唃厮啰.md "wikilink")[宗喀国](../Page/唃厮啰国.md "wikilink")。

[元朝建立后](../Page/元朝.md "wikilink")，在朵思麻設立脫思麻路軍民萬戶府，秩正三品，归[吐蕃等處宣慰使司都元帥府管辖](../Page/吐蕃等處宣慰使司都元帥府.md "wikilink")，隶属于[宣政院](../Page/宣政院.md "wikilink")。

1642年，[固始汗率部进入安多](../Page/固始汗.md "wikilink")，建立[和硕特汗国](../Page/和硕特汗国.md "wikilink")。固始汗被[五世达赖喇嘛授予](../Page/五世达赖喇嘛.md "wikilink")“护教法王”的称号。自此蒙古人开始迁入青海湖一带。1723年雍正帝征服青藏高原后，对藏区实行分治。规定[青海蒙古人在](../Page/青海蒙古.md "wikilink")[黄河以北游牧](../Page/黄河.md "wikilink")，藏人在黄河以南游牧。同时改西宁卫为[西宁府](../Page/西宁府.md "wikilink")，由[甘肃省管辖](../Page/甘肃省.md "wikilink")。设置[青海办事大臣](../Page/青海办事大臣.md "wikilink")，总理青海蒙古诸旗、番人（藏族）事务。自此，藏人传统的安多地区被从西藏地方划出，成为青海地区，由满清朝廷派驻的军政长官[西宁办事大臣管理](../Page/西宁办事大臣.md "wikilink")。

[中华民国成立后](../Page/中华民国.md "wikilink")，以安多地区设置[青海省](../Page/青海省.md "wikilink")。[中华人民共和国成立后沿袭了这一行政区划](../Page/中华人民共和国.md "wikilink")。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - A. Gruschke: *The Cultural Monuments of Tibet’s Outer Provinces:
    Amdo*, 2 vols., Bangkok 2001.

## 参见

  - [藏区](../Page/藏区.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[西藏
    (消歧義)](../Page/西藏_\(消歧義\).md "wikilink")
  - [安多語](../Page/安多語.md "wikilink")
  - [衛藏](../Page/衛藏.md "wikilink")、[康區](../Page/康區.md "wikilink")、[朵甘思](../Page/朵甘思.md "wikilink")
  - [藏區分治](../Page/藏區分治.md "wikilink")

{{-}}

[安多地区](../Category/安多地区.md "wikilink")
[Category:藏区地理](../Category/藏区地理.md "wikilink")