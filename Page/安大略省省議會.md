**安大略省省議會**（；）是[加拿大](../Page/加拿大.md "wikilink")[安大略省的](../Page/安大略省.md "wikilink")[立法部門](../Page/立法部門.md "wikilink")，其會議場所位於省府[多倫多的皇后公園內的](../Page/多倫多.md "wikilink")[議會大樓](../Page/安大略省議會大樓.md "wikilink")。安大略省省議會採[單院制](../Page/單院制.md "wikilink")，現時有124個議席\[1\]，全數採用[單議席單票制選出](../Page/單議席單票制.md "wikilink")。

第41屆省議會於2014年6月12日由當日舉行的省選產生，首屆會期則於同年7月2日展開。

第42屆省議會於2018年6月7日由當日舉行的省選產生。

## 政党

<table>
<thead>
<tr class="header">
<th><p>政黨</p></th>
<th><p>黨魁</p></th>
<th><p>狀態</p></th>
<th><p>議席數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2018年省選前</p></td>
<td><p>选后</p></td>
<td><p>當前</p></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/安大略進步保守黨.md" title="wikilink">進步保守黨</a></p></td>
<td><p><a href="../Page/道格·福特.md" title="wikilink">道格·福特</a></p></td>
<td><p>执政党</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td></td>
<td></td>
<td><p>官方反对党</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/安大略自由黨.md" title="wikilink">自由党</a></p></td>
<td><p>（临时）</p></td>
<td><p>无</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td></td>
<td></td>
<td><p>无官方政党地位</p></td>
</tr>
<tr class="even">
<td><p> <strong>總共</strong></p></td>
<td><p>107</p></td>
<td><p>124</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p> <strong>多數政府優勢</strong></p></td>
<td><p>13</p></td>
<td><p>10</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部連結

  - [安省議會官方網站](https://www.ola.org/)

[Category:安大略省政治](../Category/安大略省政治.md "wikilink")
[Category:1867年安大略省建立](../Category/1867年安大略省建立.md "wikilink")

1.  [Ontario Votes 2018
    Results](https://newsinteractives.cbc.ca/onvotes/results/)