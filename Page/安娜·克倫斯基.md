**安娜·克倫斯基**（，）是一位[美國](../Page/美國.md "wikilink")[女演員](../Page/女演員.md "wikilink")，出生於[芝加哥](../Page/芝加哥.md "wikilink")。在1991年演出《[小鬼初戀](../Page/小鬼初戀.md "wikilink")》（*My
Girl*），1994年再演出續集《甜蜜13歲》（*My Girl
2*）。2002年從[芝加哥大學](../Page/芝加哥大學.md "wikilink")（The
University of Chicago）畢業。

## 外部連結

  -
  -
  - [Anna Chlumsky Fan
    Community](https://web.archive.org/web/20170626033606/http://annachlumsky.net/)

  - [Where Are They Now? article on Anna
    Chlumsky](https://web.archive.org/web/20071017015721/http://www.paramountcomedy.com/comedy/watn/article.aspx?id=38)

[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")
[Category:捷克裔美國人](../Category/捷克裔美國人.md "wikilink")