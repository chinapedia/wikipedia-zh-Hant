**安娜·查克韋塔澤**（[俄語](../Page/俄語.md "wikilink")：****，**Anna
Chakvetadze**，），[俄羅斯職業](../Page/俄羅斯.md "wikilink")[網球女運動員](../Page/網球.md "wikilink")（2003年—），截至目前最高單打排名為世界第5。

2013年9月，查維達迪絲因傷宣佈退役[1](http://news.now.com/home/sports/player?newsId=8124881249)。

## 個人背景

她是[-{zh-hans:格鲁吉亚;zh-hk:格魯吉亞;zh-tw:喬治亞;}-人和](../Page/格魯吉亞.md "wikilink")[烏克蘭人的混血兒](../Page/烏克蘭.md "wikilink")，在俄羅斯出生。

## 外部連結

  -
  -
  -
  - [Fed Cup - Player profile - Anna CHAKVETADZE
    (RUS)](http://www.fedcup.com/en/players/player/profile.aspx?playerid=100013778)

  - [官方網站](http://www.theannachakvetadze.com/)

  - [相片](http://www.tennis-reinecke.de/spieler.php?kez=anch)

  - [安娜](http://web.archive.org/20120326094453/hotfemaletennisplayers.blogspot.com/2011/06/anna-djambulilovna-chakvetadze.html)

[Category:俄羅斯女子網球運動員](../Category/俄羅斯女子網球運動員.md "wikilink")