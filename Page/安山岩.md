[Amygdaloidal_andesite.jpg](https://zh.wikipedia.org/wiki/File:Amygdaloidal_andesite.jpg "fig:Amygdaloidal_andesite.jpg")

**安山岩**()是一种中性[火山喷出岩](../Page/火山.md "wikilink")，是造山带最普通的火山岩，其中含有斑晶，斑晶是中性[斜长石](../Page/斜长石.md "wikilink")，深色矿物有[辉石](../Page/辉石.md "wikilink")、[角闪石](../Page/角闪石.md "wikilink")，基质为隐晶质，由斜长石和极少量[正长石或](../Page/正长石.md "wikilink")[石英组成](../Page/石英.md "wikilink")。主要含有的[矿物是](../Page/矿物.md "wikilink")[铁](../Page/铁.md "wikilink")、[钛的氧化物](../Page/钛.md "wikilink")。“安山岩”这个词是来源自[南美洲的](../Page/南美洲.md "wikilink")“[安第斯山脈](../Page/安第斯山脈.md "wikilink")”。

安山岩的分类主要依据起斑晶中含有的不同成分，如辉石安山岩、云母安山岩、角闪石安山岩等。安山岩的化学和矿物成分同[闪长岩相当](../Page/闪长岩.md "wikilink")，是[流纹岩和](../Page/流纹岩.md "wikilink")[玄武岩之间的过渡類型岩石](../Page/玄武岩.md "wikilink")，含有范围在57-62%的[二氧化硅](../Page/二氧化硅.md "wikilink")（[化学式](../Page/化学式.md "wikilink")：[Si](../Page/硅.md "wikilink")）。

安山岩為構成島弧火山的重要[火成岩](../Page/火成岩.md "wikilink")，在[新生代环](../Page/新生代.md "wikilink")[太平洋地区有大量的发现](../Page/太平洋.md "wikilink")。[台灣的](../Page/台灣.md "wikilink")[大屯火山群](../Page/大屯火山群.md "wikilink")、[富貴角](../Page/富貴角.md "wikilink")\[1\]都屬於安山岩。

## 外部链接

  - [Island arc
    magmatism](http://www.nsm.buffalo.edu/courses/gly206/SubductionMagmas.pdf)

  - [火成岩的纹理](https://web.archive.org/web/20071208203514/http://geology.csupomona.edu/alert/igneous/texture.htm)

## 註釋

## 外部链接

  - [Origins of the Continental
    Crust](https://web.archive.org/web/20071104205758/http://nsw.royalsoc.org.au/journal_archive/132_34.html#Arculus)
  - [Island arc
    magmatism](http://www.nsm.buffalo.edu/courses/gly206/SubductionMagmas.pdf)
  - [Experimental and Theoretical Constraints on Peridotite Partial
    Melting in the Mantle
    Wedge](https://web.archive.org/web/20060901174449/http://www.margins.wustl.edu/Eugene_PDF/SubFac_abstract_Gaetani.pdf)

[Category:噴出岩](../Category/噴出岩.md "wikilink")
[Category:火山岩](../Category/火山岩.md "wikilink")

1.  Tony,[台北／富貴角步道udn旅遊休閒](http://travel.udn.com/mag/travel/storypage.jsp?f_ART_ID=29568),[聯合報](../Page/聯合報.md "wikilink"),2009/06/19.