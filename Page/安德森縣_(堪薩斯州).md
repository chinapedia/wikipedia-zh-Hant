**安德森县**（**Anderson County,
Kansas**，簡稱**AN**）是[美國](../Page/美國.md "wikilink")[堪薩斯州東部的一個縣](../Page/堪薩斯州.md "wikilink")。面積1,513平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口8,110人。縣治[加尼特](../Page/加尼特_\(堪薩斯州\).md "wikilink")
（Garnett）。

成立於1855年8月25日。縣名紀念準州議員約瑟·L·安德森（Joseph C.
Anderson）。\[1\]本縣直至1996年時為一[禁酒的縣](../Page/禁酒.md "wikilink")，後來經[公民投票而被部份撤銷](../Page/公民投票.md "wikilink")。營業額中食品銷售佔30%以上的場所可以售賣酒精類飲品。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/堪萨斯州行政区划.md "wikilink")
[安德森縣_(堪薩斯州)](../Category/安德森縣_\(堪薩斯州\).md "wikilink")
[Category:1855年堪薩斯領地建立](../Category/1855年堪薩斯領地建立.md "wikilink")

1.  [堪薩斯百科全書
    (1912年)](http://skyways.lib.ks.us/genweb/archives/1912/a/anderson_county.html)

2.