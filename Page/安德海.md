**安德海**（），一說名**安得海**\[1\]\[2\]，[直隸](../Page/直隶省.md "wikilink")[天津府](../Page/天津府.md "wikilink")[南皮县](../Page/南皮县.md "wikilink")（今[河北省](../Page/河北省.md "wikilink")[滄州市南皮县](../Page/滄州市.md "wikilink")）人。[宦官](../Page/宦官.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[咸豐皇帝](../Page/咸豐皇帝.md "wikilink")、[慈禧太后的寵臣](../Page/慈禧太后.md "wikilink")。

安德海出生年份有多种说法：1837年\[3\]，1842年\[4\]，1844年\[5\]\[6\]\[7\]\[8\]。自幼自閹入宮，成為[咸豐帝身邊御前太監](../Page/咸豐帝.md "wikilink")，人稱「小安子」，頗知書，通曉論（《[論語](../Page/論語.md "wikilink")》）、孟（《[孟子](../Page/孟子.md "wikilink")》）。[咸丰十一年](../Page/咸丰_\(年号\).md "wikilink")（1861年）发生的[祺祥之變中](../Page/祺祥之變.md "wikilink")，安德海把咸豐遺詔密報慈禧太后那拉氏，深得慈禧太后的歡心，升為總管太監。

[同治八年](../Page/同治.md "wikilink")（1869年）七月，安德海出京辦貨，乘上兩艘太平船從[京杭大運河一路南下](../Page/京杭大運河.md "wikilink")，“招搖煽惑，聲勢赫然”\[9\]，七月二十日，駛入山東境內，抵魯北古城[德州](../Page/德州.md "wikilink")。[山東巡撫](../Page/山東巡撫.md "wikilink")[丁寶楨以](../Page/丁寶楨.md "wikilink")[順治帝祖訓](../Page/順治帝.md "wikilink")「宦豎非經差遣，不許擅出皇城」表示：“宦豎私出，非制。且大臣未聞有命，必詐無疑”為由，命[泰安縣](../Page/泰安縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")[何毓福將安德海與其隨從逮捕](../Page/何毓福.md "wikilink")，派人秘密向[慈安太后請示](../Page/慈安太后.md "wikilink")，慈安命[軍機處回文](../Page/軍機處.md "wikilink")，內稱：“該太監擅離遠出，並有種種不法情事，若不從嚴懲辦，何以肅宮禁而儆效尤？著丁寶楨迅速派委幹員，於所屬地方將六品藍翎安姓太監嚴密查拿，令隨從人等指證確實，毋庸審訊，即行[就地正法](../Page/就地正法.md "wikilink")，不准任其狡飾。如該太監聞風折回[直境](../Page/直隸.md "wikilink")，即著[曾國藩飭屬一體嚴拿正法](../Page/曾國藩.md "wikilink")。倘有疏縱，惟該督撫是問。其隨從人等，有跡近匪類者，並著嚴拿，分別懲辦，毋庸再行請旨。”八月七日，丁寶楨於[濟南西門外丁字街](../Page/濟南.md "wikilink")（今飲虎池街北段）[斬首安德海](../Page/斬首.md "wikilink")，暴屍三日，隨行二十餘人，一律處死。

安德海死後，[李連英取代其地位](../Page/李連英.md "wikilink")。

## 影视形象

|                                      |                                        |      |     |
| ------------------------------------ | -------------------------------------- | ---- | --- |
| **演員**                               | **作品**                                 | 年份   | 类型  |
| [李凌江](../Page/李凌江.md "wikilink")     | [滿清十三皇朝](../Page/滿清十三皇朝.md "wikilink") | 1987 | 電視劇 |
| [翁世傑](../Page/翁世傑.md "wikilink")     | [滿清禁宮奇案](../Page/滿清禁宮奇案.md "wikilink") | 1994 | 電影  |
| [陳國邦](../Page/陳國邦.md "wikilink")     | [慈禧秘密生活](../Page/慈禧秘密生活.md "wikilink") | 1995 |     |
| [吳灝璋](../Page/吳灝璋.md "wikilink")（童年） | [大太監](../Page/大太監.md "wikilink")       | 2012 | 電視劇 |
| [曹永廉](../Page/曹永廉.md "wikilink")     |                                        |      |     |

## 注釋

## 外部链接

  - [太监安德海之死——细说清官十三朝](http://www.peacehall.com/news/gb/lianzai/2005/07/200507261218.shtml)

[Category:清朝宦官](../Category/清朝宦官.md "wikilink")
[Category:清朝被處決者](../Category/清朝被處決者.md "wikilink")
[Category:南皮人](../Category/南皮人.md "wikilink")
[D](../Category/安姓.md "wikilink")

1.
2.
3.

4.

5.

6.

7.

8.

9.  薛福成《庸庵筆記》