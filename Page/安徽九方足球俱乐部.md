**安徽九方足球俱乐部**，是一家[中國已解散的职业](../Page/中華人民共和國.md "wikilink")[足球俱乐部](../Page/足球.md "wikilink")，俱乐部成立于2005年，曾参加[中国足球甲级联赛](../Page/中国足球甲级联赛.md "wikilink")，是安徽省第一家职业足球俱乐部，2009年的联赛主场设在[安徽省](../Page/安徽省.md "wikilink")[芜湖市](../Page/芜湖市.md "wikilink")。

俱乐部的前身是成立于2003年的[辽宁青少年足球俱乐部](../Page/辽宁青少年足球俱乐部.md "wikilink")（即原辽宁青年队被[南京有有收购后成立的新辽宁青年队](../Page/南京有有足球俱乐部.md "wikilink")）和2004年解散的[深圳科健的青年队](../Page/深圳科健足球俱乐部.md "wikilink")。

## 球队历史

2005年**安徽九方足球俱乐部**成立，並參加[中乙联赛](../Page/2005年中国足球乙级联赛.md "wikilink")，最終取得第四名创下佳績。

2006年在[中乙联赛以](../Page/2006年中国足球乙级联赛.md "wikilink")[点球敗於](../Page/点球.md "wikilink")[哈尔滨毅腾](../Page/哈尔滨毅腾.md "wikilink")，未能升級到[中国足球甲级联赛](../Page/中国足球甲级联赛.md "wikilink")，夺得了[季军](../Page/季军.md "wikilink")。

2007年俱乐部獲冠名為**安徽九华山**，[中乙联赛取得](../Page/2007年中国足球乙级联赛.md "wikilink")[季軍](../Page/季軍.md "wikilink")，升級到中甲联赛。

2008年首次参加[中甲联赛](../Page/2008年中国足球甲级联赛.md "wikilink")，最后取得第四名的好成绩。

2009年把主场从[合肥搬到了](../Page/合肥.md "wikilink")[芜湖](../Page/芜湖.md "wikilink")。

2010年把主场从[芜湖又搬回了](../Page/芜湖.md "wikilink")[合肥](../Page/合肥.md "wikilink")，同年赛季结束后，由于安徽九方俱乐部与当地体育主管部门之间关系恶化，俱乐部董事会鉴于得不到当地政府的支持，无法满足职业联赛俱乐部必须具备相关硬件条件，于11月1日宣布退出中甲联赛，不过安徽九方没有即时解散。

2011年1月15日，[天津润宇隆和安徽九方正式达成协议](../Page/天津润宇隆.md "wikilink")，润宇隆购入九方俱乐部的中甲资格。

## 曾使用名稱

  - 2005-2006年**安徽九方**
  - 2007年**安徽九华山**
  - 2008-2010年**安徽九方**

## 歷季成績

| **年份**                                       | **聯賽級別**                             | **名次** | **場次** | **勝** | **和** | **負** | **得球** | **失球** | **積分** |
| -------------------------------------------- | ------------------------------------ | ------ | ------ | ----- | ----- | ----- | ------ | ------ | ------ |
| [2005年](../Page/2005年中国足球乙级联赛.md "wikilink") | [中乙](../Page/中國足球乙級聯賽.md "wikilink") | 第4名    | 14     | 7     | 4     | 3     | 24     | 9      | **25** |
| [2006年](../Page/2006年中国足球乙级联赛.md "wikilink") | [中乙](../Page/中國足球乙級聯賽.md "wikilink") | 第1名    | 16     | 12    | 1     | 3     | 24     | 11     | **37** |
| [2007年](../Page/2007年中国足球乙级联赛.md "wikilink") | [中乙](../Page/中國足球乙級聯賽.md "wikilink") | 第2名    | 12     | 7     | 2     | 3     | 17     | 11     | **23** |
| [2008年](../Page/2008年中國足球甲級聯賽.md "wikilink") | [中甲](../Page/中國足球甲級聯賽.md "wikilink") | 第4名    | 24     | 7     | 9     | 8     | 33     | 37     | **30** |
| [2009年](../Page/2009年中國足球甲級聯賽.md "wikilink") | [中甲](../Page/中國足球甲級聯賽.md "wikilink") | 第7名    | 24     | 7     | 8     | 9     | 35     | 44     | **29** |
| [2010年](../Page/2010年中國足球甲級聯賽.md "wikilink") | [中甲](../Page/中國足球甲級聯賽.md "wikilink") | 第9名    | 24     | 7     | 3     | 14    | 17     | 36     | **24** |

[AnhuiJiufangFC_.jpg](https://zh.wikipedia.org/wiki/File:AnhuiJiufangFC_.jpg "fig:AnhuiJiufangFC_.jpg")

## 外部連結

[安徽九方足球俱乐部官方網站](https://web.archive.org/web/20070526053049/http://www.ahjiufang.com/index.asp)

[安徽九方宣布退出职业联赛](https://web.archive.org/web/20101106004654/http://www.ahjiufang.com/show.asp?id=512)

[Category:中国已解散足球俱乐部](../Category/中国已解散足球俱乐部.md "wikilink")
[Category:合肥足球史](../Category/合肥足球史.md "wikilink")
[Category:芜湖足球史](../Category/芜湖足球史.md "wikilink")
[Category:2005年建立的足球俱樂部](../Category/2005年建立的足球俱樂部.md "wikilink")
[Category:2011年解散的足球俱樂部](../Category/2011年解散的足球俱樂部.md "wikilink")
[Category:2005年中國建立](../Category/2005年中國建立.md "wikilink")
[Category:2011年中國廢除](../Category/2011年中國廢除.md "wikilink")