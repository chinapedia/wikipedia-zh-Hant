**安斯巴赫县**（Landkreis
Ansbach）是[德国](../Page/德国.md "wikilink")[巴伐利亚州面积最大的一个县](../Page/巴伐利亚州.md "wikilink")，隶属于[中弗兰肯行政区](../Page/中弗兰肯行政区.md "wikilink")，首府[安斯巴赫](../Page/安斯巴赫.md "wikilink")。

## 華語譯名

由於兩岸對於德國行政區「**Landkreis**」翻譯的不同，台灣譯為「**安斯巴赫-{zh-tw:郡;
zh-cn:郡}-**」；中國大陸譯為「**安斯巴赫-{zh-tw:縣; zh-cn:县}-**」。

## 地理

安斯巴赫县北面与[瓦尔德纳布河畔诺伊施塔特县](../Page/瓦尔德纳布河畔诺伊施塔特县.md "wikilink")，东面与[菲尔特县和](../Page/菲尔特县.md "wikilink")[罗特县](../Page/罗特县.md "wikilink")，东南面与[魏森堡-贡岑豪森县](../Page/魏森堡-贡岑豪森县.md "wikilink")，南面与[多瑙-里斯县](../Page/多瑙-里斯县.md "wikilink")，西面与[巴登-符腾堡州的](../Page/巴登-符腾堡州.md "wikilink")[施韦比施哈尔县](../Page/施韦比施哈尔县.md "wikilink")、[奥斯特阿尔布县和](../Page/奥斯特阿尔布县.md "wikilink")[美因-陶伯县相邻](../Page/美因-陶伯县.md "wikilink")。[安斯巴赫市被安斯巴赫县完全包围在内](../Page/安斯巴赫.md "wikilink")。

[A](../Category/巴伐利亚州行政区划.md "wikilink")