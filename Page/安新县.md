**安新县**是[河北省](../Page/河北省.md "wikilink")[保定市下辖的一个县](../Page/保定.md "wikilink")，位于保定东部。面积726平方千米，其中大约1/3面积是水域，整个[白洋淀除了东部沿岸外](../Page/白洋淀.md "wikilink")，全部在安新县辖区内。县政府驻[安新镇育才路](../Page/安新镇.md "wikilink")。

由于地处白洋淀，旅游业发展迅速，此外在陆地区域乡镇工业有相当的规模。

## 历史沿革

[金](../Page/金朝.md "wikilink")[大定二十八年移](../Page/大定.md "wikilink")[安州治](../Page/安州.md "wikilink")**葛城**（今安州镇），并升葛城为县；[泰和四年以混泥城](../Page/泰和.md "wikilink")（今县城）为**渥城县**，[泰和八年移安州治渥城县](../Page/泰和.md "wikilink")。[元改渥城县为](../Page/元朝.md "wikilink")**新安县**，安州还治葛城县，新安县仍隶于安州。[明省葛城县入安州](../Page/明朝.md "wikilink")，新安仍属安州。[清](../Page/清朝.md "wikilink")[道光十二年省新安入安州](../Page/道光.md "wikilink")。[民国二年改安州为](../Page/民国.md "wikilink")**安县**，民国三年改为**安新县**。1950年4月，安新县城迁至新安。现属[河北省](../Page/河北省.md "wikilink")[保定市](../Page/保定市.md "wikilink")。

## 行政区划

下辖9个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 参考文献

## 外部链接

  - [安新县政府信息网--数字白洋淀](http://www.baiyangdian.gov.cn/)

{{-}}

[保定](../Category/河北省县份.md "wikilink")
[安新县](../Category/安新县.md "wikilink")
[县](../Category/保定区县市.md "wikilink")