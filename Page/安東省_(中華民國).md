**安東省**，為[中華民國東北東南部的一個省級行政區](../Page/中華民國.md "wikilink")，是[東九省之一](../Page/東九省.md "wikilink")，省會[通化市](../Page/通化市.md "wikilink")。

## 名稱由來

取安寧東疆之意。也有說該地是唐朝[安東都護府的舊地而命名](../Page/安東都護府.md "wikilink")。

## 轄境範圍

民國三十六年（1947年）6月5日國民政府公佈的[東北新省區方案](../Page/東北新省區方案.md "wikilink")，以滿州國的[安東](../Page/安东省_\(满洲国\).md "wikilink")、[通化二省合併而成](../Page/通化省.md "wikilink")，面積為62279.23[平方公里](../Page/平方公里.md "wikilink")，轄境相當於今[遼寧省清原](../Page/遼寧省.md "wikilink")、新賓、桓仁、寬甸、鳳城、丹東、東港及[吉林省](../Page/吉林省.md "wikilink")[通化市](../Page/通化市.md "wikilink")，是[東北九省面積最小的](../Page/東北九省.md "wikilink")。

## 地理

全省地形以丘陵山地為主體，境內多山，呈震旦走向，一般在600至1000公尺左右。東部高峻。向西南低傾，南邊有[鴨綠江河谷及沿海平原](../Page/鴨綠江.md "wikilink")，中北部主要為[長白山地的一部分](../Page/長白山.md "wikilink")。長白山主峰兵使巖是東北第一高峰（2744公尺），峰下[天池](../Page/天池.md "wikilink")，係一鍋狀火口湖，是松花江發源處。[鴨綠江貫穿東南境](../Page/鴨綠江.md "wikilink")，是最重要的河流。富含水力，建有[水豐發電廠](../Page/水豐發電廠.md "wikilink")。

本省位於北緯40至43度之間，1月均溫在攝氏零下10度至18度之間，冬季長達4、5個月；夏季7、8月的平均溫雖在攝氏20度以上，但無逾攝氏23度者。本省冬寒而長，夏熱苦短。

本省屬[溫帶季風氣候](../Page/溫帶季風氣候.md "wikilink")，加上在東北緯度較低，是氣候比較溫和的地方。由於多山，且近海迎風，故雨量較豐，以鴨綠江中、下游最多，並由此向東北遞減；全省各地年雨量概在700公厘以上，六、七、八三個月是雨季。近海，年雨量在1000公釐左右，

安東的山地林區廣大，伐木業以鴨綠江流域最盛。安東是東北最大木材市揚，其地木材、木漿、造船等工業發達。丘陵區柞蠶的養殖業頗盛，絲產量冠全國，安東也是柞蠶絲集散和絲織中心。

人口民國三十六年（1947年）上半年統治資料為316萬3911人，下半年統計資料為297萬1170人。

## 歷史

历史上是各民族混杂之地，[燕](../Page/燕国.md "wikilink")、[秦](../Page/秦朝.md "wikilink")、[汉](../Page/汉朝.md "wikilink")、[西晋以及以后多个朝代均有行政管理](../Page/西晋.md "wikilink")。[汉朝大致为](../Page/汉朝.md "wikilink")[幽州](../Page/幽州.md "wikilink")[辽东郡与](../Page/辽东郡.md "wikilink")[汉四郡管辖范围](../Page/漢四郡.md "wikilink")。[唐朝时属](../Page/唐朝.md "wikilink")[安东都护府](../Page/安东都护府.md "wikilink")。

[清代時歸](../Page/清代.md "wikilink")[奉天將軍所轄](../Page/奉天將軍.md "wikilink")，清[光緒三十三年](../Page/光緒.md "wikilink")（1907年）東北設省，屬[奉天省](../Page/奉天省.md "wikilink")。[民國十八年](../Page/民國.md "wikilink")（1929年），奉天省改名為遼寧省，繼續為[遼寧省所轄](../Page/遼寧省.md "wikilink")。

民國二十一年（1932年）[日軍侵佔中國東北](../Page/日軍.md "wikilink")，東北成為「[滿洲國](../Page/滿洲國.md "wikilink")」領土。1934年10月，滿洲國把東北4省劃為14省，新增設的安東省管轄安東、鳳城、賽馬、寬甸等12縣，治所設於安東縣城，安東遂成為安東省軍事、政治、經濟、文化活動中心。

1937年12月，滿洲國從安東縣析出設安東市，為安東省會。安東市與安東縣同隸屬于安東省，市區劃金湯、元寶、中興、鎮安、浪頭、大和、旭日7個區。

民國三十四年（1945年）[抗日戰爭勝利後](../Page/抗日戰爭.md "wikilink")，[國民政府準備接收東北](../Page/國民政府.md "wikilink")，明令將東北三省劃為九省，於9月4日任命安東省主席。此時中國共產黨的軍隊早己進駐安東市，11月初成立安東省民主政府。蘇軍從東北撤離後，國共在東北發生大規模軍事衝突。民國三十五年（1946年）5月，國軍相繼攻克海龍龍、柳河、東豐；10月下旬，攻克清原、新賓、鳳城、安東；12月，攻克通化、輯安，全省大部分地區己被國軍收復，國府人員進駐安東各地。[東北民主聯軍在](../Page/東北民主聯軍.md "wikilink")[南满根据地仅控制臨江縣](../Page/南满根据地.md "wikilink")、長白縣、撫松縣、濛江縣（中共政权已将其改名为[靖宇县](../Page/靖宇县.md "wikilink")）。中共的安東省政府撤出省境。同年12月17日－次年4月3日，「[三下江南、四保临江战役](../Page/三下江南、四保临江战役.md "wikilink")」後，[林彪指揮東北民主聯軍轉入戰略反攻](../Page/林彪.md "wikilink")。民國三十六年（1947年）5月下旬，國軍被迫撤離通化、安東、新賓等地，共軍隨後佔領。6月10日，中共的安東省政府遷回安東市。中華民國的安東省政府消亡。

## 行政區劃

民國三十六年（1947年）6月5日國民政府公佈的[東北新省區方案](../Page/東北新省區方案.md "wikilink")，省會為通化市，下轄2市、18縣，由於全省轄境較小，不設行政督察區。抗戰結束後，國民政府一度控制全省大部，但在國民政府公佈行政區劃之前，全境己被中共東北民主聯軍控制，因此國民政府公佈的行政區劃並未得到執行。大部分縣份在[九一八事變以前屬遼寧省](../Page/九一八事變.md "wikilink")，濛江縣原屬吉林省，通化、安東二市在滿州國時期已經存在，僅新設孤山縣：

| 安東省      |
| -------- |
| 所屬督察區及盟部 |
| 省直轄地區    |
| 27002    |
| 27003    |
| 27004    |
| 27005    |
| 27006    |
| 27007    |
| 27008    |
| 27009    |
| 27010    |
| 27011    |
| 27012    |
| 27013    |
| 27014    |
| 27015    |
| 27016    |
| 27017    |
| 27018    |
| 27019    |
| 27020    |

## 政府

  - 安東省政府
      - 主席[高惜冰](../Page/高惜冰.md "wikilink")
      - 參議[蔡景華](../Page/蔡景華.md "wikilink")
      - 秘書長[王同寅](../Page/王同寅.md "wikilink")
      - 財政廳長[于學思](../Page/于學思.md "wikilink")
      - 建設廳長[李酉山](../Page/李酉山.md "wikilink")
      - 教育廳長[吳希庸](../Page/吳希庸.md "wikilink")
      - 會計長[高鳳桐](../Page/高鳳桐.md "wikilink")
      - 接收專員[關崇義](../Page/關崇義.md "wikilink")
      - 安東市政府市長[包景華](../Page/包景華.md "wikilink")
      - 安東市政府參事[袁華東](../Page/袁華東.md "wikilink")
      - 安東市警察局長[胡春華](../Page/胡春華_\(民國\).md "wikilink")
      - 安東縣長[劉德鄰](../Page/劉德鄰.md "wikilink")
      - 安東省保安司令部副司令[王景烈](../Page/王景烈.md "wikilink")
      - 參謀長[孫成城](../Page/孫成城.md "wikilink")

## 参见

  - [中國東北](../Page/中國東北.md "wikilink")
  - [東北九省](../Page/東北九省.md "wikilink")
  - [通化市](../Page/通化市.md "wikilink")
  - [丹東市](../Page/丹東市.md "wikilink")

{{-}}

[Category:东北民国省份](../Category/东北民国省份.md "wikilink")
[Category:吉林民国时期行政区划](../Category/吉林民国时期行政区划.md "wikilink")
[Category:辽宁民国时期行政区划](../Category/辽宁民国时期行政区划.md "wikilink")