**安格拉·多羅特婭·默克爾**（，），婚前姓**卡斯納**（Kasner），[德国](../Page/德国.md "wikilink")[基督教民主联盟籍女性](../Page/德國基督教民主聯盟.md "wikilink")[政治家](../Page/政治家.md "wikilink")，现任德国联邦总理。2005年，她成为[德国历史上首位女性](../Page/德国历史.md "wikilink")[聯邦总理](../Page/德國聯邦總理.md "wikilink")，她也是[兩德統一後首位出身](../Page/兩德統一.md "wikilink")[前東德地區的聯邦总理](../Page/東德.md "wikilink")\[1\]。

2015年12月，默克尔当选《[时代周刊](../Page/时代周刊.md "wikilink")》2015年度风云人物，《时代周刊》总编赞扬她在[欧洲主权债务](../Page/欧洲主权债务危机.md "wikilink")、中东难民及俄罗斯干预乌克兰等危机期间所展现的非凡领导能力\[2\]。近年，默克尔處理[歐洲難民危機的表現受到爭議及批評](../Page/歐洲難民危機.md "wikilink")。不少國家及政治人物批評德國總理默克爾為代表的難民政策，某些政治人物認為這樣的政策应证了[右派民粹主義的預言](../Page/右派民粹主義.md "wikilink")，而造成了右派民粹主義的崛起。

## 生平背景

默克爾生于[汉堡](../Page/汉堡.md "wikilink")，全名**安格拉·多羅特婭·卡斯納**（Angela Dorothea
Kasner），父系先祖是普鲁士王国[波森省的](../Page/波森省.md "wikilink")[波蘭人](../Page/波蘭人.md "wikilink")。默克爾是她第一任丈夫的姓氏，離婚後保留下來。

她父亲是一位[路德教会牧师](../Page/信义宗.md "wikilink")，在她出生後不久，由於父親從教會接到新的任命，全家移居[東柏林以北](../Page/東柏林.md "wikilink")80公里的[滕普林](../Page/滕普林.md "wikilink")（Templin）。她在那里完成初等教育，俄語造詣佳，八年級時就獲准參加全國俄文競賽（此競賽只有十年制專科學校的十年級生才能參加），獲選為俄文最佳女學生，得到在東西德四處旅行以及到莫斯科旅遊的獎賞\[3\]，到莫斯科買了第一張披頭士唱片。在[莱比锡大学攻读物理学](../Page/莱比锡大学.md "wikilink")（1973年－1978年），之后在科学院物理化学中央学会工作学习（1978年－1990年），研究领域是[量子化学](../Page/量子化学.md "wikilink")，后来取得博士学位\[4\]\[5\]。

前東德的生活背景给她带来不少好处。她作为牧师的女儿，并在每间屋子都可能存在东德[警察告密者的社会里度过生命的前](../Page/史塔西.md "wikilink")36年，养成了很好的掩饰或控制情绪的能力。除了[德語外](../Page/德語.md "wikilink")，默克爾会说[俄語與](../Page/俄語.md "wikilink")[英语](../Page/英语.md "wikilink")，当人家评论她身为东德人的背景时，她说：“真的有话可说的人，是連[化妆](../Page/化妆.md "wikilink")（掩飾）都不需要的。”

### 婚姻

1974年，梅克爾與同學去蘇聯參加青少年交流活動，並到[列寧格勒州和](../Page/列寧格勒州.md "wikilink")[莫斯科找俄國的物理系同學遊玩](../Page/莫斯科.md "wikilink")，在此認識第一任丈夫烏爾里希·梅克爾（Ulrich
Merkel），兩年後住在一起，兩人希望在同一個城市工作，結婚才有機會分配到住房，且夫妻找工作時，國家不會將他們分隔兩地\[6\]。1977年，婚禮在滕普林老家舉行，安格拉冠上夫姓「梅克爾」。四年後婚姻出現裂痕，兩人形同陌路，她幾乎在一夜之間就從兩人在[柏林共有的房子搬出去](../Page/柏林.md "wikilink")，留下相當震驚的烏爾里希·梅克爾\[7\]。1982年[離婚](../Page/離婚.md "wikilink")\[8\]。

她现任（即第二任）丈夫是毕业并任教于德国[洪堡大学的](../Page/洪堡大学.md "wikilink")[博士](../Page/博士.md "wikilink")、教授、[量子化学家](../Page/量子化学.md "wikilink")[约阿希姆·绍尔](../Page/约阿希姆·绍尔.md "wikilink")，两人是1981年认识的，当时绍尔是她的博士研究生导师。绍尔的前妻是一名化学家，两人育有两个儿子，夫妻感情不和，早就分居，于1985年离婚。1986年默克尔在博士毕业后，两人住到了一起。直到1998年，默克尔升任基民盟主席后，两人才正式结婚。两德统一之后，默克尔从政，绍尔继续科研事业。自1993年开始，绍尔在母校德国洪堡大学担任全职教授\[9\]。

### 個人特質

梅克爾熱愛歌劇，尤其喜歡[理察·華格納](../Page/理察·華格納.md "wikilink")（Richard
Wagner）與悲愴及命運有關的所有作品，[崔斯坦與伊索德](../Page/崔斯坦與伊索德.md "wikilink")（Tristan
and
Isolde）是她最欣儀的歌劇。不向命運屈服的梅克爾形容[理察·華格納](../Page/理察·華格納.md "wikilink")：「事情無法從出口倒轉回來，讓我覺得悲痛。起步對，滿盤皆對。」做事有條理有計劃，從容不迫。她在思索問題時，首先會要全面徹底了解事情，要看事實，即使反面沒有任何論據，她還是會要了解事情的反面。因此，梅克爾會動手分析她對世界的認識，估量論據，收集事實，再權衡出一個淨值。\[10\]

梅克爾欣賞沈默緘默，安靜沉著。她所有信得過的人都能保持沈默，對她而言這是忠心耿耿的證明。若有人不符合梅克爾的期待，她會變得非常冷淡簡慢，甚至挖苦對方\[11\]。如果有些人擔任的職位不是由她決定，而她又迫切需要他們，她在那些人面前的態度就不一樣了：如結盟黨派的主席，或其他國家的領導人\[12\]。她不容許自己冷硬，甚至禁止流露出好惡。在交友方面，梅克爾對共事者始終保持距離，即便是從1995年開始至今擔任辦公室主任包嫚，也一直都以「您」互稱，不直呼其名\[13\]。

梅克爾價值觀核心是「自由」，開始還有些隱諱，一共花了十年，才將自由列進自己即興演出中\[14\]。2010年，梅克爾頒獎給以漫畫諷刺默罕默德的漫畫家[威斯特葛德](../Page/威斯特葛德.md "wikilink")（[Kurt
Westergaard](https://en.wikipedia.org/wiki/Kurt_Westergaard)），並以「自由的秘密是勇氣」為題發表演講。梅克爾試圖找出自由的定義，自由最主要與責任有關\[15\]：「自由一方面從一些什麼而來，另一方面又朝向著一些而去。我們談到自由的時候，總是在談論別人的自由。」「我相信，自由社會更具創造力，而且能發展出長期有效的解決辦法。」　自由包含責任、包容與勇氣\[16\]。

## 政治生涯

[Bundesarchiv_Bild_183-1990-0803-017,_Lothar_de_Maiziere_und_Angela_Merkel.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1990-0803-017,_Lothar_de_Maiziere_und_Angela_Merkel.jpg "fig:Bundesarchiv_Bild_183-1990-0803-017,_Lothar_de_Maiziere_und_Angela_Merkel.jpg")（左）在一起\]\]
1989年[柏林墙推倒之后](../Page/柏林墙.md "wikilink")，她投入到蓬勃发展的民主运动中。1989年底她加入新党“民主觉醒”。东德第一次民主选举后，她得到新政府一个副政府发言人的职务，在洛塔·德·梅齊艾（Lothar
de
Maizière）手下工作。1990年12月[两德统一后](../Page/两德统一.md "wikilink")，她成为[赫尔穆特·科尔内阁中妇女青年部部长](../Page/赫尔穆特·科尔.md "wikilink")。1994年出任环境和核能安全部长。1993年6月至2000年5月任[梅克伦堡-前波莫瑞州](../Page/梅克伦堡-前波莫瑞州.md "wikilink")[德国基督教民主联盟](../Page/德国基督教民主联盟.md "wikilink")（基民盟）主席。安格拉·默克爾的政治生涯得益于前[聯邦總理](../Page/德国总理.md "wikilink")[赫尔穆特·科尔的提拔](../Page/赫尔穆特·科尔.md "wikilink")。科尔有一次叫她小姑娘（），因此很多媒体也把默克爾叫做“科尔的小姑娘”。

### 反对党领袖

1998年，科尔領導的中間偏右聯合政府在选举中失利，[社民党相隔十六年再次執政](../Page/德国社会民主党.md "wikilink")，並與[綠党組成](../Page/德國綠党.md "wikilink")[中間偏左執政聯盟](../Page/中間偏左.md "wikilink")。金融丑闻危及基民盟很多领导的形象，首当其冲的是科尔本人，然后是主席[沃尔夫冈·朔伊布勒](../Page/沃尔夫冈·朔伊布勒.md "wikilink")。2000年，曾任基民盟秘書長的默克爾宣布參選基民盟領導人，並公开声明与科尔等人划清界限。默克爾最終接替朔伊布勒成为基民盟第一位女性领导人。默克爾当选在很多方面都是让人十分惊讶：基民盟被認為是傳統保守派及天主教背景的政党，男性主導一切。

[Angela_Merkel_and_Vladimir_Putin_in_Moscow_2002.jpg](https://zh.wikipedia.org/wiki/File:Angela_Merkel_and_Vladimir_Putin_in_Moscow_2002.jpg "fig:Angela_Merkel_and_Vladimir_Putin_in_Moscow_2002.jpg")，2002年\]\]
很多人拿默克爾与前英国首相[戴卓爾夫人](../Page/戴卓爾夫人.md "wikilink")（两个人都是國家首位女性政黨領袖及領導人，有[鐵娘子之稱](../Page/鐵娘子.md "wikilink")，还都曾经是科学家，分別在於對歐洲的態度，戴卓爾夫人是疑歐派，默克爾則是親歐派）比较，把她叫做（多数是非德国媒体）“铁姑娘”。有时新闻媒体还用“Angie”称呼她。

除了基民盟領導人的职务外，2002年起她还是联邦议会中基民盟中[保守派的代表](../Page/保守派.md "wikilink")。

2002年，默克爾不顾全国的主流反对，公开赞成[伊拉克战争](../Page/伊拉克战争.md "wikilink")，声称这是“不可避免”的，這導致她領導的基民盟在聯邦選舉中落敗，但中間偏左執政聯盟基民盟與社民黨的議席差距減至僅三席，[施羅德的中間偏左聯合政府僅以微弱優勢取勝](../Page/施羅德.md "wikilink")。施羅德其後把选举胜利，归功于他強烈反对美国發動戰爭中[霸权和军事冒险的强硬态度](../Page/霸权.md "wikilink")，但事實上，施羅德的聯合政府優勢減少，從此變得不穩定。不過，默克爾並非基民盟推舉的總理候選人，也是基民盟落敗的因素之一。

2005年5月30日，她正式代表[基民盟](../Page/德国基督教民主联盟.md "wikilink")/[基社盟](../Page/巴伐利亚基督教社会联盟.md "wikilink")，在9月18日提前举行的[联邦选举中与总理施罗德角逐下任总理](../Page/2005年德国联邦选举.md "wikilink")。10月10日，大选中几乎打成平手的基民盟与社民党在近兩個月的联合[组阁谈判中取得共识](../Page/组阁.md "wikilink")，确认由默克爾出任聯邦总理，內閣人事由社民黨佔優，社民黨有8個席次，基民盟有6個席次，組成四十年首個「左右大聯盟」。

2005年11月22日，她正式成为德国第一位女性聯邦总理，也是一千年前[神聖羅馬帝國的](../Page/神聖羅馬帝國.md "wikilink")皇后（956－991年）之后，第一位领导[日耳曼的女性](../Page/日耳曼.md "wikilink")。她也是[兩德統一後首位出身](../Page/兩德統一.md "wikilink")[前東德地區的聯邦总理](../Page/東德.md "wikilink")。

## 總理

[Flickr_-_europeanpeoplesparty_-_EPP_Summit_19_June_2008_(19).jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_europeanpeoplesparty_-_EPP_Summit_19_June_2008_\(19\).jpg "fig:Flickr_-_europeanpeoplesparty_-_EPP_Summit_19_June_2008_(19).jpg")和意大利總理[貝盧斯科尼](../Page/西尔维奥·贝卢斯科尼.md "wikilink")，2008年\]\]
[Angela_Merkel_&_GWBush_2007Jan04.jpg](https://zh.wikipedia.org/wiki/File:Angela_Merkel_&_GWBush_2007Jan04.jpg "fig:Angela_Merkel_&_GWBush_2007Jan04.jpg")，2007年\]\]
[Vladimir_Putin_8_March_2008-3.jpg](https://zh.wikipedia.org/wiki/File:Vladimir_Putin_8_March_2008-3.jpg "fig:Vladimir_Putin_8_March_2008-3.jpg")[弗拉基米爾·普京](../Page/弗拉基米爾·普京.md "wikilink")，2008年3月8日\]\]
默克爾担心[欧盟未能定义](../Page/欧盟.md "wikilink")“未来贸易战”的共同利益，现在欧洲在后冷战时代保持“和平和自由”的目的已经实现。“这就是我认为的，欧洲需要学习很多东西，不是把太多精力放到诸如‘要不要在[葡萄牙修建与德国西北部相同的自行车专用道](../Page/葡萄牙.md "wikilink")’这样的事情上。”

梅克爾赞同国家自愿参与模式需要变革。“在德国，我们总是面对‘总是慢一拍’的危险。我们要加速变革。”

[President_and_First_Lady_Obama_with_Chancellor_Merkel.jpg](https://zh.wikipedia.org/wiki/File:President_and_First_Lady_Obama_with_Chancellor_Merkel.jpg "fig:President_and_First_Lady_Obama_with_Chancellor_Merkel.jpg")[奥巴马及其夫人会见默克尔及其丈夫](../Page/奥巴马.md "wikilink")\]\]
[2009年德國聯邦議院選舉](../Page/2009年德國聯邦議院選舉.md "wikilink")，經過與社民黨近四年的「左右共治」，默克爾領導的基民盟再次取得勝利，議席較上屆增加，與同屬中間偏右的[自由民主黨合組聯合政府](../Page/自由民主黨_\(德國\).md "wikilink")。

[Dilma_Rousseff_e_Angela_Merkel_2012.jpg](https://zh.wikipedia.org/wiki/File:Dilma_Rousseff_e_Angela_Merkel_2012.jpg "fig:Dilma_Rousseff_e_Angela_Merkel_2012.jpg")，在[G-20峰會在墨西哥](../Page/2012年二十国集团洛斯卡沃斯峰会.md "wikilink")，2012年\]\]
[Merkel_cropped.jpg](https://zh.wikipedia.org/wiki/File:Merkel_cropped.jpg "fig:Merkel_cropped.jpg")
[2013年德國聯邦議院選舉](../Page/2013年德國聯邦議院選舉.md "wikilink")，由於中間偏右盟友自由民主黨未能跨越5%的得票門檻被逐出國會，基民盟再次與社民黨近四年的「左右共治」，組建[大聯合政府](../Page/大聯合政府.md "wikilink")。

2016年11月20日默克尔宣布将谋求第四度连任总理一职。\[17\]

2018年3月14日，在歷經長達171天的協商後，梅克爾以364票贊成、315票反對、9票棄權以及21張無效票的結果確定四連霸。\[18\]

默克尔支持接纳难民的政策招致国内强烈反弹。2018年10月，德国执政党基民盟在巴伐利亚州和黑森州议会选举中接连遭遇失败，默克尔宣布放弃竞选基民盟党主席一职。2021年第四度总理任期结束后不再寻求连任\[19\]\[20\]。

### 外交政策

#### 中华人民共和國

默克爾重視發展與中华人民共和國的關係，但在人權問題上曾保持強硬的立場。2007年9月23日，默克爾在总理官邸会见[西藏流亡政府精神領袖](../Page/西藏流亡政府.md "wikilink")[第十四世達賴喇嘛表示支持西藏文化自治](../Page/丹增嘉措.md "wikilink")，是首次正式會見達賴。这次会面引发[中華人民共和国政府的强烈反弹](../Page/中華人民共和国政府.md "wikilink")，德国外交部长[施泰因迈尔亦对此做出批评](../Page/施泰因迈尔.md "wikilink")。\[21\]默克尔在接受德國媒體採訪時回应說「我是德國總理，我有權決定在哪里和誰見面。不會因為對華貿易關係而在原則問題上妥協讓步。」（Entscheide
selbst, wen ich
treffe）。默克爾於2008年3月[藏區流血衝突發生之際即表示未來還會再接待到訪的達賴喇嘛](../Page/2008年藏區騷亂.md "wikilink")\[22\]，她其後更表明不會出席[北京奧運](../Page/北京奧運.md "wikilink")，成為第一個宣佈不出席的大國領導人\[23\]。

默克尔自就任总理以来，截至2018年6月，先后访华11次，到访过的城市有北京、上海、南京、西安、成都、合肥、沈阳等，她也是任内访华次数最多的西方国家领导人。在2015年10月的访问中，默克尔和中华人民共和國总理李克强共同提出，德国的[工业4.0计划将与](../Page/工业4.0.md "wikilink")[中国制造2025规划实现战略对接](../Page/中国制造2025.md "wikilink")\[24\]。

#### 土耳其

在2005年競選時，梅克爾就公開她的看法（在拜訪[伊斯坦堡時她也有表示](../Page/伊斯坦堡.md "wikilink")），[土耳其其實無法被](../Page/土耳其.md "wikilink")[歐盟接受成為成員國](../Page/歐盟.md "wikilink")。換句話說，她偏好的是「特殊的伙伴關係」。當她就任總理並進入歐盟後，她對這個問題就相當棘手。之後土耳其總理[埃尔多安在](../Page/雷杰普·塔伊普·埃尔多安.md "wikilink")2008年2月到德國向在德的土耳其人發表演說時警告他們不要被其它文化[同化](../Page/同化.md "wikilink")，並表示「這種文化融合是[反人類罪行](../Page/反人類罪行.md "wikilink")」\[25\]，引起默克爾的異議\[26\]。

#### 以色列

梅克爾一直至今都對德國參與[聯合國在南](../Page/聯合國.md "wikilink")[黎巴嫩解放運動的衝突事件](../Page/黎巴嫩.md "wikilink")，抱持保留態度。[以色列總理](../Page/以色列總理.md "wikilink")[奥尔默特在接受](../Page/奥尔默特.md "wikilink")[南德日報訪問時](../Page/南德日報.md "wikilink")，對德軍參戰的辯護是：「我已經告知梅克爾總理，我們跟德軍在南黎巴嫩一點問題都沒有。」現在梅克爾領導下，德國是世界上對以色列最為友善的國家之一。

2008年3月18日，梅克爾在[以色列國會發表演說](../Page/以色列國會.md "wikilink")，並以[希伯來語開場](../Page/希伯來語.md "wikilink")。她強調德國對以色列的歷史責任；確保[猶太人的國家安全](../Page/猶太人.md "wikilink")，毫無疑問是德國的責任。梅克爾是第一個在以色列國會前發表演說的德国政府首脑。

### 糧食問題

默克爾認為包括印度及中國在內的發展中國家，當地人民不斷改善及提高原有的飲食水平，是導致2008年上半年世界糧價飆漲的主要原因，並否認大量農作物被用於製造生質燃料為價格上升的原因。梅克爾說﹕“如果他們（印度人）突然比從前多吃一顿飯，又如果一億中國人突然開始喝牛奶，當然，我們的奶量必然有所缺減，其他方面（糧食）亦然。”\[27\]

### 軍事衝突

在[伊拉克戰爭中](../Page/伊拉克戰爭.md "wikilink")，梅克爾表示她同情[美國的伊拉克政策](../Page/美國.md "wikilink")。在面對軍事衝突方面，她表示必須非常理性地看待這個問題，比如當[北約參與](../Page/北約.md "wikilink")[科索沃戰爭時](../Page/科索沃戰爭.md "wikilink")，藉以比較德國歷史發展的過程，她說：「回頭看看過去的歷史，自由才是我們致力追求的善举，並且我們不惜一切要做的是，避免戰爭再度來臨....回頭看看過去的歷史，誤解、激進的[法西斯主義所帶來的只有災難](../Page/法西斯主義.md "wikilink")，運用暴力的結果，帶來的只有更大的不幸—這個結果是不可避免的，因此我們要盡力避免這種惡行。...在科索夫戰爭中，也要盡量避免暴力『意願同盟』（coalition
of the willing，2003年美國進軍伊拉克結合的同盟）所帶來的巨大災禍。」

### 經濟與社會政策

梅克爾在2000年底就試圖規劃「新社會市場經濟」。這個計畫就是要提供一個社會市場經濟的穩定概念。在1999年[格哈特·施羅德內閣中就已經出現這樣的概念](../Page/格哈特·施羅德.md "wikilink")。而到2001年8月27日在當時CDU委員會裡面，梅克爾主席擬定了這個討論方案，並在該年12月的聯邦議會日時，基民盟在[德勒斯登宣示這份計畫](../Page/德勒斯登.md "wikilink")，並且成為基民盟施政計畫的一環。

### 家庭政策

作為女性與年輕人的聯邦總理，出身自[東德的梅克爾相當在意女性](../Page/東德.md "wikilink")[就業率](../Page/就業率.md "wikilink")、[生育率等問題](../Page/生育率.md "wikilink")。在[東德與](../Page/東德.md "wikilink")[西德之間](../Page/西德.md "wikilink")，存在著不同阻礙生育的法律狀況，這也是後來她進行法律統一改革的原因。她政策要點之一就是在性別平權法（Gleichberechtigungsgesetz
(1993/94)）內強化女性就業條件。同時關於兒童與青年協助法（Kinder- und
Jugendhilfegeset）的修訂，也讓0歲以上的兒童可以享有[幼稚園的法律保障](../Page/幼稚園.md "wikilink")，這是她最大的成果。2017年6月，[德國同性婚姻法案在她對自己所屬的執政黨公開呼籲良知投票下](../Page/德國同性婚姻.md "wikilink")，[德國國會終於通過德國同性婚姻法制化](../Page/德國國會.md "wikilink")，儘管她個人堅持一夫一妻制，但依然尊重此項人權法案。\[28\]

### 環境政策

早於1995年4月時，梅克爾在[柏林任聯合國氣候委員會的第一屆環境部長](../Page/柏林.md "wikilink")。在這段时期內，她致力於減少國際[溫室效應](../Page/溫室效應.md "wikilink")。1997年在[京都議定書簽署之後](../Page/京都議定書.md "wikilink")，梅克爾就開始投入減碳工作。梅克爾是一個關注核心能源運用的支持者，比如如何從關鍵資源中獲得電力。在她的領導下，致力於減少[核廢料](../Page/核廢料.md "wikilink")。1998年5月，在德法邊界出現違法處理核廢料事件，梅克爾也因出現監督不當，受到反對黨批評而辭職。然而她證明了自己的對核能經濟的能力與責任感。1997年的官方宣告裡面，也看得到每年要持續針對特殊能源增稅，比如石油、天然氣與電力（[生態稅](../Page/生態稅.md "wikilink")）。1998年在她担任环境部部长期间曾在[《科学》杂志上撰文阐述科学在可持续发展中的角色](../Page/科学_\(期刊\).md "wikilink")。\[29\]

### 对斯诺登事件的立场

[爱德华·斯诺登事件導致](../Page/爱德华·斯诺登.md "wikilink")[稜鏡計劃曝光后](../Page/稜鏡計劃.md "wikilink")，美國被指監聽默克尔，而美国情报机构可能已经监听现任德国总理默克尔的电话长达十余年。默克尔强烈谴责间谍行动，要求[美国国家安全局立即停止监听行为](../Page/美国国家安全局.md "wikilink")\[30\]\[31\]，並指出如果事件继续酝酿下去，会影响[德美關係](../Page/德美關係.md "wikilink")。德国《明镜》周刊26日最先披露了上述消息，美国媒体纷纷跟进。有关报道的情报来源似乎仍出自美国揭秘者、美情报机构前雇员斯诺登。报道指默克尔的手机号码早在2002年就被列入美国国家安全局的特别监控当中，直到2014年6月美国总统奥巴马访问德国时，该号码仍未从NSA的监控中删除。\[32\]

## 爭議

### 對歐洲的政策

德國政府對希臘等國的撙節政策引起希臘及西班牙等[歐豬五國的抗議](../Page/歐豬五國.md "wikilink")，並且促使希臘的[激進左翼聯盟以及西班牙](../Page/激進左翼聯盟.md "wikilink")[我們可以黨的崛起](../Page/我們可以黨.md "wikilink")、以及國內的[德國另類選擇興起](../Page/德國另類選擇.md "wikilink")，間接促使歐盟部分地區例如[加泰隆尼亞想要](../Page/加泰隆尼亞.md "wikilink")[脫離西班牙](../Page/加泰隆尼亞獨立運動.md "wikilink")。由[阿列克西斯·齊普拉斯領導的希臘政府在](../Page/阿列克西斯·齊普拉斯.md "wikilink")2015年時更放話要追討與當時債務金額相近的二戰賠償高達2787億歐元。\[33\]

著名的美国政治学家[法蘭西斯·福山於](../Page/法蘭西斯·福山.md "wikilink")2016年在接受英國媒體采访时批評默克爾在處理歐洲事務時只關注德國民意，讓[民粹主義在歐洲滋長](../Page/民粹主義.md "wikilink")。福山認為对欧洲来说，民粹主義的興起是比[伊斯兰国更严重的威胁](../Page/伊斯兰国.md "wikilink")\[34\]。

### 難民政策

[何清涟認為欧洲各国因为默克尔](../Page/何清涟.md "wikilink")2015年9月初宣布的接收难民无上限政策而陷入难民带来的[强奸](../Page/强奸.md "wikilink")、[杀人](../Page/杀人.md "wikilink")、[抢劫](../Page/抢劫.md "wikilink")、[偷盗](../Page/偷盗.md "wikilink")、[贩毒与](../Page/贩毒.md "wikilink")[恐怖袭击之中](../Page/恐怖袭击.md "wikilink")\[35\]，何清涟批評默克爾無視[穆斯林移民不易融入歐洲社會的現實](../Page/穆斯林.md "wikilink")，讓大量穆斯林難民湧入德國定居，預言德國的未來將會被默克爾的難民政策毀掉\[36\]。

2017年1月，美國候任總統[唐纳德·川普批評默克爾在](../Page/唐纳德·川普.md "wikilink")[接收難民政策上犯下](../Page/歐洲難民危機.md "wikilink")「災難性錯誤」\[37\]。

## 参考文献

## 外部链接

  - [个人网站](http://www.angela-merkel.de/)

  -
  - [德国总理官方主页](http://www.bundeskanzlerin.de/)

  - Ruth Elkins，《[獨立報](../Page/獨立報.md "wikilink")》（*The
    Independent*），2005年6月19日，["安格拉·默克爾:
    铁姑娘"](http://news.independent.co.uk/people/profiles/story.jsp?story=647953)

  - [德国首位女总理在议会宣誓就职](http://news.bbc.co.uk/chinese/simp/hi/newsid_4450000/newsid_4459000/4459084.stm)，BBC

## 参见

  - [默克尔第一内阁](../Page/默克尔第一内阁.md "wikilink")
  - [梅克爾菱形](../Page/梅克爾菱形.md "wikilink")
  - [量子化学](../Page/量子化学.md "wikilink")

{{-}}

[\*](../Category/安格拉·默克尔.md "wikilink")
[Category:德国总理](../Category/德国总理.md "wikilink")
[Category:女性总理](../Category/女性总理.md "wikilink")
[Category:德国女性政府官员](../Category/德国女性政府官员.md "wikilink")
[Category:德國基督教民主聯盟黨員](../Category/德國基督教民主聯盟黨員.md "wikilink")
[Category:德國物理化學家](../Category/德國物理化學家.md "wikilink")
[Category:波蘭裔德國人](../Category/波蘭裔德國人.md "wikilink")
[Category:女性化學家](../Category/女性化學家.md "wikilink")
[Category:萊比錫大學校友](../Category/萊比錫大學校友.md "wikilink")
[Category:時代年度風雲人物](../Category/時代年度風雲人物.md "wikilink")
[Category:时代百大人物](../Category/时代百大人物.md "wikilink")
[Category:德國信義宗教徒](../Category/德國信義宗教徒.md "wikilink")
[Category:漢堡人](../Category/漢堡人.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:查理曼獎得主](../Category/查理曼獎得主.md "wikilink")
[Category:歐洲理事會主席](../Category/歐洲理事會主席.md "wikilink")

1.  [默克尔连任总理](http://www.apdnews.com/news/55686.html) ，亚太日报，2013年12月19日

2.

3.  }

4.   cited in  and listed in the Catalogue of the Deutsche
    Nationalbibliothek under subject code 30 (Chemistry)

5.

6.
7.
8.
9.

10.

11.
12.
13.
14.
15.
16.
17.

18. [Angela Merkel elected to fourth term as German
    chancellor](http://www.dw.com/en/angela-merkel-elected-to-fourth-term-as-german-chancellor/a-42966453)

19. [德国的默克尔时代已看到终点](http://www.xinhuanet.com/world/2018-10/30/c_129981697.htm)

20. [默克尔放弃连任，“铁娘子”为何能执政十几年？](https://www.yicai.com/news/100049052.html)

21.

22. [從默克爾對華外交中吸取“教訓”](http://cnnews.chosun.com/site/data/html_dir/2007/12/17/20071217000026.html)
    , [朝鮮日報](../Page/朝鮮日報.md "wikilink"), 2007.12.17

23. [德國總理表明不出席京奧](http://hk.apple.nextmedia.com/international/art/20080330/10925574)

24.

25.

26.

27.

28. [德國同性婚姻合法化
    梅克爾坦誠投反對票](http://m.cna.com.tw/news/firstnews/201706300263.aspx)，中央通訊社，2017-6-30

29.

30. ["NSA spying row: bugging friends is unacceptable, warn
    Germans"](http://www.guardian.co.uk/world/2013/jul/01/nsa-spying-allegations-germany-us-france)[卫报](../Page/卫报.md "wikilink")

31. [默克尔：谁都不能随意监控其他人](http://v.ifeng.com/news/world/201307/d11e41de-9708-47fb-af79-a358a322586d.shtml)

32. [美国情报机构或已监听默克尔电话长达十余年 2013年10月27日
    来源：中国新闻网](http://www.chinanews.com/gj/2013/10-27/5428402.shtml)

33.

34.

35.

36.

37.