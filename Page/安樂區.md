**安樂區**是[中華民國](../Page/中華民國.md "wikilink")[臺灣省](../Page/臺灣省.md "wikilink")[基隆市的一個](../Page/基隆市.md "wikilink")[行政區](../Page/市轄區.md "wikilink")，位於全市西北方，為基隆市人口最多的[行政區](../Page/市轄區.md "wikilink")。

## 歷史

安樂區涵蓋[清治時期的大武崙](../Page/台灣清治時期.md "wikilink")-{庄}-、蚵殼港-{庄}-、港口-{庄}-等地區。[日治](../Page/台灣日治時期.md "wikilink")[昭和](../Page/昭和.md "wikilink")16年（1941年），該區內改制為觀音町、寶町、西町、大武崙[大字等](../Page/大字_\(行政區劃\).md "wikilink")。1942年各[町及大字改置](../Page/町.md "wikilink")[區會](../Page/區會.md "wikilink")，而後合併為第四區。[二戰後](../Page/台灣戰後時期.md "wikilink")，於[民國](../Page/民國紀年.md "wikilink")35年（1946年）改稱為安樂區。
1980年，[基隆市政府於](../Page/基隆市政府.md "wikilink")[七堵區](../Page/七堵區.md "wikilink")[鶯歌石興建國民住宅](../Page/鶯歌石_\(基隆市\).md "wikilink")（即今安樂社區），[鶯歌石及附近的](../Page/鶯歌石_\(基隆市\).md "wikilink")[港口地區人口激增](../Page/港口_\(基隆市\).md "wikilink")。1982年11月[安樂區行政大樓於](../Page/安樂區.md "wikilink")[七堵區](../Page/七堵區.md "wikilink")[鶯歌石竣工](../Page/鶯歌石_\(基隆市\).md "wikilink")，1988年，將原[七堵區的](../Page/七堵區.md "wikilink")[鶯歌石及](../Page/鶯歌石_\(基隆市\).md "wikilink")[港口兩地區劃歸](../Page/港口_\(基隆市\).md "wikilink")[安樂區管轄](../Page/安樂區.md "wikilink")。區公所則延至該年3月1日始遷入辦公，[安樂區圖書館才同時開館營運](../Page/安樂區.md "wikilink")。

## 人口

## 歷任區長列表

| 區長姓名 | 區長任期                     | 備註 |
| ---- | ------------------------ | -- |
| 林慶福  | 1946年1月 - 1950年9月        |    |
| 高維新  | 1950年10月 - 1951年         |    |
| 楊再生  | 1951年 - 1954年            |    |
| 林慶福  | 1954年 - 1966年9月          |    |
| 劉亮夫  | 1966年9月 - 1972年4月        |    |
| 洪連成  | 1972年4月 - 1982年10月       |    |
| 陳資平  | 1982年10月 - 1986年9月1日     |    |
| 鄭炳煥  | 1987年2月16日 - 1990年2月28日  |    |
| 許庭郡  | 1990年4月23日 - 1992年2月27日  |    |
| 許清坤  | 1992年2月27日 - 1998年6月30日  |    |
| 蔡神恭  | 1998年7月1日 - 2001年12月16日  |    |
| 郭高標  | 2002年3月19日 - 2005年6月13日  |    |
| 項文龍  | 2005年6月14日 - 2013年10月31日 |    |
| 馬福能  | 2013年11月1日 - 2016年1月16日  |    |
| 駱文章  | 2016年3月11日 - 現任          |    |

## 行政區

  - 七賢里、干城-{里}-、內寮里、四維里、壯觀里、[長樂里](../Page/長樂里_\(基隆市\).md "wikilink")、新西里、樂一里、鶯歌里
  - 三民里、中崙里、西川里、外寮里、定邦里、武崙里、新崙里、興寮里
  - 六合里、五福里、永康里、[安和里](../Page/安和里_\(基隆市\).md "wikilink")、定國里、慈仁里、嘉仁里、鶯安里

## 教育

### 高級中學

  - [基隆市立安樂高級中學](../Page/基隆市立安樂高級中學.md "wikilink")

### 國民中學

  - [基隆市立建德國民中學](http://www.jdjh.kl.edu.tw/)
  - [基隆市立武崙國民中學](../Page/基隆市立武崙國民中學.md "wikilink")

### 國民小學

  - [基隆市立安樂國民小學](http://www.alps.kl.edu.tw/)
  - [基隆市立西定國民小學](http://ms1.sdes.kl.edu.tw/)
  - [基隆市立長樂國民小學](http://www.clps.kl.edu.tw/)
  - [基隆市立隆聖國民小學](http://www.lsps.kl.edu.tw/)
  - [基隆市立武崙國民小學](http://www.wlps.kl.edu.tw/)
  - [基隆市立建德國民小學](http://www.jdps.kl.edu.tw/)

## 交通

### 公路

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號（中山高速公路）](../Page/中山高速公路.md "wikilink")
      - [基隆交流道](../Page/基隆交流道.md "wikilink")
  - [TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號（福爾摩沙高速公路）](../Page/福爾摩沙高速公路.md "wikilink")
      - [基金交流道](../Page/基金交流道.md "wikilink")
  - [TW_PHW62.svg](https://zh.wikipedia.org/wiki/File:TW_PHW62.svg "fig:TW_PHW62.svg")[台62線](../Page/台62線.md "wikilink")（東西向快速公路
    萬里－瑞濱）
      - [安樂端](../Page/安樂端.md "wikilink")
  - [TW_PHW2.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2.svg "fig:TW_PHW2.svg")[台2線](../Page/台2線.md "wikilink")：[基金公路](../Page/基金公路.md "wikilink")
      - [TW_PHW2f.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2f.svg "fig:TW_PHW2f.svg")[台2己](../Page/台2線#己線.md "wikilink")：港西聯外快速公路

## 郵局

  - [中華郵政](../Page/中華郵政.md "wikilink")[基隆樂利郵局(基隆18支)](https://www.post.gov.tw/post/internet/I_location/index_post.jsp?prsb_no=001118-6)
      - 地址:基隆市安樂區樂利三街261號
      - 郵遞區號:20447
  - [中華郵政](../Page/中華郵政.md "wikilink")[基隆安樂路郵局(基隆21支)](https://www.post.gov.tw/post/internet/I_location/index_post.jsp?prsb_no=001121-5)
      - 地址:基隆市安樂區安樂路二段132號
      - 郵遞區號:20448
  - [中華郵政](../Page/中華郵政.md "wikilink")[基隆大武崙郵局(基隆26支)](https://www.post.gov.tw/post/internet/I_location/index_post.jsp?prsb_no=001126-3)
      - 地址:基隆市安樂區基金一路131-24號
      - 郵遞區號:20445

## 旅遊

[外木山_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:外木山_-_panoramio.jpg "fig:外木山_-_panoramio.jpg")

  - [大武崙砲台](../Page/大武崙砲台.md "wikilink")
  - 外木山海岸
    ([白沙灘](../Page/白沙灘.md "wikilink")、[澳底漁港](../Page/澳底漁港.md "wikilink"))
  - [情人湖](../Page/情人湖.md "wikilink")
  - 十方大覺寺
  - [老大公廟](../Page/老大公廟.md "wikilink")
  - [慈雲寺](../Page/慈雲寺.md "wikilink")

## 外部連結

  - [安樂區公所](http://www.klal.gov.tw/)

[\*](../Category/安樂區.md "wikilink")
[Category:基隆市行政區劃](../Category/基隆市行政區劃.md "wikilink")