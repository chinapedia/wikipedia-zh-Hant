**安氏中喙鯨**（[學名](../Page/學名.md "wikilink")：*Mesoplodon
bowdoini*）旧称**安竹氏喙鲸**，又称**突齿喙鲸**，**波多因氏喙鲸**和**高顶喙鲸**。其已知的研究資料極少，可能在[南極聚合帶](../Page/南極聚合帶.md "wikilink")（Antarctic
Convergence）北方呈環極區分布。在其分布範圍內有35次以上的發現記錄，但全部都是擱淺的個體，大部分在[紐西蘭與](../Page/紐西蘭.md "wikilink")[澳洲發現](../Page/澳洲.md "wikilink")。命名者[羅伊·查普曼·安德魯斯曾擔任](../Page/羅伊·查普曼·安德魯斯.md "wikilink")[美國自然歷史博物館館長](../Page/美國自然歷史博物館.md "wikilink")，他在任內以此[博物館的委託者與捐獻者George](../Page/博物館.md "wikilink")
S. Bowdoin之名為此[物種命名](../Page/物種.md "wikilink")。

## 基本資料

其他俗名：

  - 英：**Bowdoin's Beaked Whale、Deepcrest Beaked Whale**
  - 法：***MÉSOPLODON DE ANDREW、MÉSOPLODON DE BOWDOIN***
  - 西：***BALLENA DE PICO DE ANDREW、ZIFIO DE ANDREW***

出生時身長體重：1.6m；體重未知

最大身長體重紀錄：雄性至少4.5m、雌性4.9m；體重未知

壽命：未知

## 形态特徵

安氏中喙鯨的體型粗壯，[額隆平坦](../Page/額隆.md "wikilink")，嘴喙短而粗，巨大的下顎在中段形成明顯的拱起。背鰭小，呈[三角形](../Page/三角形.md "wikilink")，尖端圓鈍，位於背部中央後方。成年雄鯨下顎可見2顆外露的巨大[牙齒](../Page/牙齒.md "wikilink")，側面寬闊但相當扁平，位於距下顎尖端約20[公分處](../Page/公分.md "wikilink")。

安氏中喙鯨的體色普遍呈暗灰至黑色，僅嘴喙前端與下顎為白色，部分個體在眼睛前方有淺色斑塊。成年雄鯨背部有淺灰色的**「馬鞍」**（**saddle**，為一橫跨背部中線、外觀大致呈[馬鞍狀的斑紋](../Page/馬鞍.md "wikilink")，常會延伸至身體側面。），由噴氣孔後方延伸至背鰭。一般認為雌鯨的嘴喙顏色比雄鯨來得深，背部呈石板一般的灰色，腹部與側腹則是較淺的灰白色。成年雄鯨的體表常有長條直線狀傷痕，可能是其他雄鯨造成。無論雄鯨或雌鯨皆有因[雪茄鮫](../Page/雪茄鮫.md "wikilink")（cookiecutter
shark）攻擊留下的橢圓形傷痕。

## 分布

安氏中喙鯨僅分布於[南半球](../Page/南半球.md "wikilink")，可能在[溫帶](../Page/溫帶.md "wikilink")[緯度](../Page/緯度.md "wikilink")（南緯32度至南緯54度30分之間）海域呈環極區分布。擱淺記錄已知在澳洲南部、紐西蘭、[麥加利群島](../Page/麥加利群島.md "wikilink")（Macquarie
Islands）、[福克蘭群島](../Page/福克蘭群島.md "wikilink")、與[垂斯坦昆哈](../Page/垂斯坦昆哈.md "wikilink")（Tristan
da
Chunha）。在南[印度洋的](../Page/印度洋.md "wikilink")[克革倫群島](../Page/凯尔盖朗群岛.md "wikilink")（Kerguelen
Islands）與[祕魯的發現記錄可能是其他中喙鯨的誤認](../Page/祕魯.md "wikilink")。

## 習性、生殖、食性

由於沒有可信的海中目擊記錄與群體擱淺，科學家對其社會結構、習性或生殖、覓食的習慣幾乎完全不明。位於紐西蘭海域的族群，其繁殖季似乎自夏季至秋季。

## 現狀

安氏中喙鯨的數量不明。未曾有被捕殺的記錄，也沒有因漁業活動而造成死亡的跡象。

## 参考文献

  - *Pieter A. Folken, Randall R. Reeves, etc. / illustrated by Pieter
    A. Folkens, Guide to MARINE MAMMALS of the World, Alfred A. Knopf,
    2002: p286-287. ISBN 0-375-41141-0*
  - *Mark Carwardine / illustrated by Martin CammDorling, 《DORLING
    KINDERSLEY HANDBOOKS: WHALES, DOLPHINS AND PORPOISES》, Dorling
    Kindersley, 1995: p116-117. ISBN 0-7513-2781-6*
  - ''James G. Mead, 《Beaked Whales of the Genus *Mesoplodon*》, edited
    by Sam H. Ridgway and Sir Richard Harrison, F.R.S. 《Handbook of
    Marine Mammals, Volume 4: River Dolphins and the Larger Toothed
    Whales》, Academic Press, 1989: p349-430. ISBN 0-12-588504-0''

[Category:喙鲸属](../Category/喙鲸属.md "wikilink")