**安田
美和**是日本的女性聲優。[青二Production所屬](../Page/青二Production.md "wikilink")，[兵庫縣出身](../Page/兵庫縣.md "wikilink")。

## 演出作品

### 電視動畫

  - [袖珍女侍小梅](../Page/袖珍女侍小梅.md "wikilink")（花枝小弟）

  - [夢幻天女](../Page/夢幻天女.md "wikilink")（護士、子供）

  - [學園愛麗絲](../Page/學園愛麗絲.md "wikilink")（乃木流架）　

  - [機動新撰組 萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")（青龍）

  - [Kanon](../Page/Kanon.md "wikilink")（相沢祐一（少年時代）、栞之母）

  - [捍衛者](../Page/捍衛者.md "wikilink")（服務台的姐姐、女子A）

  - [∀鋼彈](../Page/∀鋼彈.md "wikilink")（侍女B）

  -
  - [真仙魔大戰](../Page/真仙魔大戰.md "wikilink")（お助けさん）

  - [貧窮姊妹物語](../Page/貧窮姊妹物語.md "wikilink")（看護師）

  - [幻影死神](../Page/幻影死神.md "wikilink")（小島茜）

  - [蟲孽](../Page/蟲孽.md "wikilink")（）

  - [彩夢芭蕾](../Page/彩夢芭蕾.md "wikilink") 第16話（4個女學生之一）

  - [彩夢芭蕾](../Page/彩夢芭蕾.md "wikilink") 第20話（女學生A）

### OVA

  - [袖珍女侍マイ](../Page/袖珍女侍マイ.md "wikilink")（花枝小弟）
  - [銀河少女警察](../Page/銀河少女警察.md "wikilink")（アナウンス）

### 遊戲

  - 學園愛麗絲（乃木流架）

  - [機動新撰組 萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")（青龍）

  - KOF MAXIMUM IMPACT系列（）

  - BLADE STORM 百年戰爭（）

  -
  -
  -
  -
  - [勇者之光](../Page/勇者之光.md "wikilink")（娣雅・坦格莉）

  - [英雄傳說VI「空之軌跡」3rd](../Page/英雄傳說VI「空之軌跡」3rd.md "wikilink")（噬身之蛇的盟主）

### 廣播劇CD

  - [英雄傳說VI「空之軌跡」](../Page/英雄傳說VI「空之軌跡」.md "wikilink")－盟主

  -
  -
## 外部連結

  - [事務所公開簡歷](https://web.archive.org/web/20090417024539/http://www.aoni.co.jp/actress/ya/yasuda-miwa.html)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")
[Category:青二Production](../Category/青二Production.md "wikilink")