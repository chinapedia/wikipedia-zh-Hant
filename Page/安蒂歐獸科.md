**安蒂歐獸科**（學名：Anteosauridae）又稱**前龍科**、**前衛龍科**、**安泰龍科**，是[獸孔目](../Page/獸孔目.md "wikilink")[恐頭獸亞目的一科或亞科](../Page/恐頭獸亞目.md "wikilink")，是群生存於中[二疊紀的大型肉食性](../Page/二疊紀.md "wikilink")[恐頭獸類](../Page/恐頭獸類.md "wikilink")，化石發現於[俄羅斯](../Page/俄羅斯.md "wikilink")（[長蜥獸](../Page/長蜥獸.md "wikilink")、[巨型獸](../Page/巨型獸.md "wikilink")）與[南非](../Page/南非.md "wikilink")（[安蒂歐獸](../Page/安蒂歐獸.md "wikilink")）。

牠們的特徵是大型尖狀的[門齒與](../Page/門齒.md "wikilink")[犬齒](../Page/犬齒.md "wikilink")、匙狀的[後犬齒](../Page/後犬齒.md "wikilink")、邊緣往外突起的[前上頜骨](../Page/前上頜骨.md "wikilink")，所以嘴的前部曲線往上，還有長而結實的下頜。
[Doliosauriscus_DB.jpg](https://zh.wikipedia.org/wiki/File:Doliosauriscus_DB.jpg "fig:Doliosauriscus_DB.jpg")的想像圖\]\]
安蒂歐獸科與更原始的[巨形獸科](../Page/巨形獸科.md "wikilink")（Brithopodidae）的差別，在於下頜兩側的大型突起物；這大概是用於物種內的打鬥行為。*Doliosauriscus*與[安蒂歐獸的突起物非常明顯](../Page/安蒂歐獸.md "wikilink")，而且這部份的骨頭非常厚且多溝紋。相同的特徵可在[草食性的](../Page/草食性.md "wikilink")[貘頭獸發現](../Page/貘頭獸.md "wikilink")，這被推測是這些動物會有以頭互相撞擊的行為。

這些動物是[二疊紀中期以前的最大型掠食動物](../Page/二疊紀.md "wikilink")，成年個體的頭骨長達80公分長，遠比[麗齒獸類大](../Page/麗齒獸類.md "wikilink")。

## 參考資料

  - Herbert Barghusen, ­ 1975. A review of fighting adaptation in
    dinocephalians (Reptilia, Therapsida). *Paleobiology* 12:95–311.
  - Lieuwe Dirk Boonstra 1963, Diversity within the South African
    Dinocephalia. South African Journal of Science 59: 196-206.
  - \----- 1969, "The Fauna of the Tapincephalus Zone (Beaufort Beds of
    the Karoo)," Annals of the South African Museum 56 (1) 1-73, pp.
    35-38
  - Robert L. Carroll, 1988, *Vertebrate Paleontology and Evolution*, WH
    Freeman & Co.
  - James A. Hopson and Barghusen, H.R., 1986, An analysis of therapsid
    relationships in Nicholas Hotton III, Paul D. MacLean, Jan J. Roth
    and E. Carol Roth, *The Ecology and Biology of Mammal-like
    Reptiles*, Smithsonian Institute Press, pp. 83-106
  - Gillian King, 1988, "Anomodontia" Part 17 C, *Encyclopedia of
    Paleoherpetology*, Gutsav Fischer Verlag, Stuttgart and New York,

## 外部連結

  - [Palaeos](https://web.archive.org/web/20080414095012/http://www.palaeos.com/Vertebrates/Units/400Therapsida/500.html)

[\*](../Category/恐頭獸亞目.md "wikilink")