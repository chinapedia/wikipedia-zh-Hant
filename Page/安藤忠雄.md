[Azuma_house.JPG](https://zh.wikipedia.org/wiki/File:Azuma_house.JPG "fig:Azuma_house.JPG")，1976年\]\]
[Hyogo_prefectural_museum_of_art08s3200.jpg](https://zh.wikipedia.org/wiki/File:Hyogo_prefectural_museum_of_art08s3200.jpg "fig:Hyogo_prefectural_museum_of_art08s3200.jpg")
[Tokyo,_Japan_(43696647564).jpg](https://zh.wikipedia.org/wiki/File:Tokyo,_Japan_\(43696647564\).jpg "fig:Tokyo,_Japan_(43696647564).jpg")開館10周年，進行有史以來規模最大的「安藤忠雄展–
挑戰–」作品展，圖為模仿著名的[光之教會](../Page/光之教會.md "wikilink")\]\]
**安藤忠雄**（，），[日本](../Page/日本.md "wikilink")[建築師](../Page/建築師.md "wikilink")，1995年[普利兹克奖得主](../Page/普利兹克奖.md "wikilink")，[東京大學名譽教授](../Page/東京大學.md "wikilink")。21世紀臨調特別顧問，[東日本大震災復興構想會議議長代理](../Page/東日本大震災.md "wikilink")，[大阪府與](../Page/大阪府.md "wikilink")[大阪市特別顧問](../Page/大阪市.md "wikilink")。

## 生平

安藤忠雄甚有傳奇性，在成為建築師前，曾當過職業[拳手](../Page/拳手.md "wikilink")（成績：23戰13勝3敗7平手），其後在沒有經過正統訓練下成為專業的建築師。

安藤忠雄在[大阪府立城東工業高校畢業後](../Page/大阪府.md "wikilink")，前往世界各地旅行，並自學建築。1969年創立安藤忠雄建築研究所。1976年完成位於[大阪府的](../Page/大阪府.md "wikilink")[住吉長屋](../Page/住吉長屋.md "wikilink")，是兩層高的[混凝土住宅](../Page/混凝土.md "wikilink")，已顯現其設計風格。其後獲得日本建築學會賞。1980年代參與[關西周邊地區](../Page/近畿地方.md "wikilink")（[神戶北野](../Page/神戶.md "wikilink")、[大阪](../Page/大阪.md "wikilink")[心齋橋](../Page/心齋橋.md "wikilink")）的商業建築設計，1990年代以後，參與公共建築、美術館建築等大型計劃。

接連發表了以[清水混凝土建造的住宅和商業建築](../Page/清水混凝土.md "wikilink")，引起風潮和討論，名聲也開始快速累積，從博物館、娛樂設施、宗教設施、辦公室等，作品的領域寬廣，通常都是大型規模的建築。但也有人認為失去了安藤早期的小型建築特有的魅力。

1995年，安藤忠雄獲得建築界最高榮譽[普利兹克奖](../Page/普利兹克奖.md "wikilink")，他把10萬[美元獎金捐贈予](../Page/美元.md "wikilink")1995年[神戶大地震後的孤兒](../Page/神戶大地震.md "wikilink")。

## 簡歷

安藤出生於[兵庫縣](../Page/兵庫縣.md "wikilink")，父姓北山，但出生後便過繼到外公家（位於大阪）。利用[拳擊比賽贏得的獎金](../Page/拳擊.md "wikilink")，前往[美國](../Page/美國.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[亞洲旅行](../Page/亞洲.md "wikilink")，也順便觀察各地獨特的建築。那個時候，他的攝影作品被使用在建築師[路易·康的作品集中](../Page/路易·康.md "wikilink")。

安藤忠雄是[雙胞胎中的哥哥](../Page/雙胞胎.md "wikilink")。雙胞胎中的弟弟是北山孝雄，在東京開設北山創造研究所，經營企業經營顧問、商品設計。3兄弟中最小的弟弟是建築師北山孝二郎（因為與美國建築師[彼得·艾森曼的合作而知名](../Page/彼得·艾森曼.md "wikilink")）。最初的建築作品—（、玫瑰花園；位在[神戶市](../Page/神戶市.md "wikilink")[生田區](../Page/生田區.md "wikilink")）是在1977年時，與弟弟孝雄所屬的浜野商品研究所一起共同合作。

安藤忠雄並未受過正規的建築教育，僅在建築公司工作過一小段時間。在高中畢業後，參加了研究班（，是一種由大學教授創立的研究班，屬於大學教育的一種），Semi
Mode研究班是由已故的長澤節所創立，被稱作是「傳說中的美術學校」，畢業生包括[飯野和好](../Page/飯野和好.md "wikilink")、[金子功](../Page/金子功.md "wikilink")、[寺門孝之](../Page/寺門孝之.md "wikilink")、[山本耀司等人](../Page/山本耀司.md "wikilink")，畢業生大多都是目前都在業界第一線活躍的創意工作者。之後獨自利用各種渠道學習了室內設計和製圖等技巧。在成為建築師之前，曾經擔任過以關西為中心，許多茶館或咖啡廳的室內設計。

1969年在[大阪成立安藤忠雄建築研究所](../Page/大阪.md "wikilink")，設計了許多個人住宅。其中位在大阪的「[住吉的長屋](../Page/住吉的長屋.md "wikilink")（）」獲得很高的評價，大規模的公共建築到小型的個人住宅作品，多次得到[日本建築學會獎的肯定](../Page/日本建築學會.md "wikilink")。此後安藤確立了自己以[清水混凝土和幾何形狀為主的個人風格](../Page/清水混凝土.md "wikilink")，也得到世界的良好評價。1980年代在[關西周邊](../Page/近畿地方.md "wikilink")（特別是神戶、北野町、大阪心齋橋一帶）設計了許多商業設施、寺廟、教會等。1990年代之後公共建築、美術館，以及海外的建築設計案開始增加。

2017年，東京國立新美術館舉行《安藤忠雄展──挑戰》個人展，除了把他的故事娓娓道來外，展區會分為六大主題，分別是：開端／居住、光、空間留白、在地性、活化歷史建築和培育。主辦單位收集了安藤忠雄多年來，二百多個作品的建築草稿、設計圖、照片等\[1\]。

## 經歷

  - 1987年 - 擔任[耶魯大學的客座教授](../Page/耶魯大學.md "wikilink")
  - 1988年 - 擔任[哥倫比亞大學的客座教授](../Page/哥倫比亞大學.md "wikilink")
  - 1989年 - 擔任[哈佛大學的客座教授](../Page/哈佛大學.md "wikilink")
  - 1997年 - 擔任[東京大學工學部教授](../Page/東京大學.md "wikilink")
  - 2003年 - 從[東京大學退休](../Page/東京大學.md "wikilink")，轉任名譽教授
  - 2005年 - 獲得[東京大學的終身特別榮譽教授](../Page/東京大學.md "wikilink")
  - 2005年 -
    擔任[加州](../Page/加州.md "wikilink")[柏克萊大學的講座教授](../Page/柏克萊大學.md "wikilink")

## 獎項

  - 1979年 - 日本建築學會賞（[住吉的長屋](../Page/住吉的長屋.md "wikilink")）
  - 1983年 - 日本文化設計賞（六甲集合住宅）
  - 1985年 - 芬蘭建築師協會阿爾瓦‧阿爾托（Alvar Aalto）獎
  - 1986年 - 藝術選獎文部大臣賞新人賞（中山邸）
  - 1985年 - 毎日設計賞
  - 1987年 - 毎日藝術賞（六甲教会）
  - 1988年 - 第13回吉田五十八賞（城戸崎邸）
  - 1989年 - 法國建築學院獎賞
  - 1990年 - 大阪藝術賞
  - 1991年 - 美國建築師協會（AIA）榮譽會員、美國科學院和文學藝術研究所Arnold W. Brunner紀念獎
  - 1992年 - 第1屆卡爾斯伯格（Carlsberg）建築獎（丹麥）
  - 1993年 - 皇立英國建築師協會（RIBA）榮譽會員、日本藝術院賞
  - 1994年 - 第26回日本藝術大賞（大阪飛鳥博物館）
  - 1995年 -
    1995年[普利茲克獎](../Page/普利茲克獎.md "wikilink")、1994年度朝日賞、第7回國際設計獎、[法國藝術及文學勳章](../Page/法國藝術及文學勳章.md "wikilink")（騎士級）
  - 1996年 -
    第8回[高松宮殿下紀念世界文化獎](../Page/高松宮殿下紀念世界文化獎.md "wikilink")、第1屆國際教會建築獎
  - 1997年 -
    德國建築師協會名譽會員、皇立英國建築師協會皇家金獎章、第4回大阪吉瓦尼斯獎、[法國藝術及文學勳章](../Page/法國藝術及文學勳章.md "wikilink")（提升至軍官級）
  - 2002年 -
    [美国建筑师学会金奖](../Page/美国建筑师学会金奖.md "wikilink")、[京都獎思想](../Page/京都獎.md "wikilink")・藝術部門
  - 2005年 - 國際建築師聯盟金牌（UIA金牌）

## 作品

### 日本

  - 富島邸（[大阪府](../Page/大阪府.md "wikilink")，1973年）
  - [住吉的長屋](../Page/住吉的長屋.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")，1976年）
  - 小篠邸（[兵庫縣](../Page/兵庫縣.md "wikilink")[芦屋市](../Page/芦屋市.md "wikilink")，1981年）
  - 六甲集合住宅
    I（[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")，1983年）
  - The Times I（[京都府](../Page/京都.md "wikilink")，1983年）
  - Festival（[沖繩縣](../Page/沖繩縣.md "wikilink")，1984年）
  - [六甲教會](../Page/六甲教會.md "wikilink")（[兵庫縣](../Page/兵庫縣.md "wikilink")，1986年）
  - 城戶崎邸（[東京都](../Page/東京都.md "wikilink")，1986年）
  - Guest House OLD/NEW六甲（[兵庫縣](../Page/兵庫縣.md "wikilink")，1987年）
  - [水之教會](../Page/水之教會.md "wikilink")（[北海道](../Page/北海道.md "wikilink")，1988年）
  - [光之教會](../Page/光之教會.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")[茨木市](../Page/茨木市.md "wikilink")，1989年）
  - [Collezione商業中心大廈](../Page/Collezione商業中心大廈.md "wikilink")（[東京](../Page/東京.md "wikilink")[南青山](../Page/南青山.md "wikilink")，1989年）
  - 萊卡（Raika）總公司大樓 （[大阪府](../Page/大阪府.md "wikilink")，1989年）
  - 兵庫縣立兒童館（[兵庫縣](../Page/兵庫縣.md "wikilink")，1989年）
  - 姬路文學館（[兵庫縣](../Page/兵庫縣.md "wikilink")，1991年）
  - Time II（[京都府](../Page/京都府.md "wikilink")，1991年）
  - [本福寺水御堂](../Page/本福寺水御堂.md "wikilink")（[兵庫縣](../Page/兵庫縣.md "wikilink")，1991年）
  - 熊本縣立装飾古墳館（[熊本縣](../Page/熊本縣.md "wikilink")，1992年）
  - 星之子館（[兵庫縣](../Page/兵庫縣.md "wikilink")，1992年）
  - 直島當代美術館（[香川縣直島町](../Page/香川縣.md "wikilink")，1992年）
  - 六甲集合住宅
    II（[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")，1993年）
  - 兵庫縣立看護大學（[兵庫縣](../Page/兵庫縣.md "wikilink")，1993年）
  - 香北市立金津小學（[石川縣](../Page/石川縣.md "wikilink")[香北市](../Page/香北市.md "wikilink")，1993年）
  - 大阪府立飛鳥博物館（[大阪府](../Page/大阪府.md "wikilink")，1994年）
  - 天保山三得利博物館（[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")，1994年）
  - 兵庫縣立木殿堂（[兵庫縣](../Page/兵庫縣.md "wikilink")，1994年）
  - 京都府立陶板名畫之庭（[京都府](../Page/京都府.md "wikilink")，1994年）
  - 成羽町美術館（[岡山縣](../Page/岡山縣.md "wikilink")，1994年）
  - 直島當代美術館 擴建工程（[香川縣直島町](../Page/香川縣.md "wikilink")，1995年）
  - 小海高原美術館（[長野縣](../Page/長野縣.md "wikilink")[南佐久郡](../Page/南佐久郡.md "wikilink")，1995年）
  - 大山崎山荘美術館（[京都府](../Page/京都府.md "wikilink")，1996年）
  - 姫路市立文學資料館南館（[兵庫縣](../Page/兵庫縣.md "wikilink")[姬路市](../Page/姬路市.md "wikilink")，1996年）
  - TOTO研究所（[淡路市](../Page/淡路市.md "wikilink")，1997年）
  - 西宮市貝類館（[西宮市](../Page/西宮市.md "wikilink")，1999年）
  - [淡路夢舞台](../Page/淡路夢舞台.md "wikilink")（[淡路市](../Page/淡路市.md "wikilink")，2000年）
  - [南岳山光明寺](../Page/南岳山光明寺.md "wikilink")（[愛媛縣](../Page/愛媛縣.md "wikilink")[西條市](../Page/西條市.md "wikilink")，2000年）
  - [奇跡之星的植物館](../Page/奇跡之星的植物館.md "wikilink")（[淡路市](../Page/淡路市.md "wikilink")，2000年）
  - [4m×4m的家](../Page/4m×4m的家.md "wikilink")（[2001\~2003](../Page/2001~2003.md "wikilink")）
  - 兵庫縣立美術館 藝術館（[神戶市](../Page/神戶市.md "wikilink")，2001年）
  - 司馬遼太郎記念館（[大阪府](../Page/大阪府.md "wikilink")[東大阪市](../Page/東大阪市.md "wikilink")，2001年）
  - 大阪府立狹山池博物館（[大阪府](../Page/大阪府.md "wikilink")[狹山市](../Page/狹山市.md "wikilink")，2001年）
  - [環球影城車站](../Page/環球影城車站.md "wikilink")（JR西日本櫻島線）（[大阪市](../Page/大阪市.md "wikilink")[此花區](../Page/此花區.md "wikilink")，2001年）
  - 國際兒童圖書館（[東京都](../Page/東京都.md "wikilink")[台東区](../Page/台東区.md "wikilink")，2002年）
  - 石川縣西田幾多郎記念哲學館（[石川縣香北市](../Page/石川縣.md "wikilink")，2002年）
  - 尾道市立美術館（[廣島縣](../Page/廣島縣.md "wikilink")[尾道市](../Page/尾道市.md "wikilink")，2003年）
  - 賀市立錦城中學（[石川縣](../Page/石川縣.md "wikilink")[加賀市](../Page/加賀市.md "wikilink")，2003年）
  - [東京美術館](../Page/東京美術館.md "wikilink")、城市之屋仙川（[東京都](../Page/東京都.md "wikilink")[調布市](../Page/調布市.md "wikilink")，2004年）
  - 地中美術館（[香川縣直島町](../Page/香川縣.md "wikilink")，2004年）
  - [表参道之丘](../Page/表参道之丘.md "wikilink")（[東京](../Page/東京.md "wikilink")，2006年1月）
  - 新銀橋（[大阪市北區](../Page/大阪市.md "wikilink")，2006年）
  - 神奈川秦野市太岳院重建 2007年
  - 坂上之雲美術館（愛媛縣松山市，2007年）
  - 21_21 DESIGN SIGHT（東京都港区，2007年）
  - 李禹煥美術館（香川県直島町，2010年）
  - 秋田縣立美術館（秋田縣，2012年）
  - ANDO MUSEUM（香川縣直島町，2013年）

### 西班牙

  - 西班牙[萬國博覽會](../Page/萬國博覽會.md "wikilink")
    日本政府館（[西班牙](../Page/西班牙.md "wikilink")[塞維爾](../Page/塞維爾.md "wikilink")，1992年）

### 德國

  - Vitra Seminar House（[德國](../Page/德國.md "wikilink")，1993年）
  - [藍爵基金會美術館Langen
    Foundation](../Page/藍爵基金會美術館Langen_Foundation.md "wikilink")（[德國](../Page/德國.md "wikilink")，2004年）

### 法國

  - UNESCO總部
    冥想空間（[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")，1995年）

### 威尼斯

  - [威尼斯皮諾現代美術館之葛拉喜館（Palazzo
    Grassi）](../Page/威尼斯皮諾現代美術館之葛拉喜館（Palazzo_Grassi）.md "wikilink")（2004年）

### 美國

  - Eychaner/Lee House（[美國](../Page/美國.md "wikilink")，1997年）
  - 普立茲美術館（[美國](../Page/美國.md "wikilink")，2000年）
  - Fort
    Worth現代美術館（[美國](../Page/美國.md "wikilink")[德州](../Page/德州.md "wikilink")，2002年）
  - 馬里布的住宅 I
    （[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")，2015年）
  - 152 Elizabeth, New York New York——進行中的計畫
  - 前時裝設計師[湯姆·福特的房屋](../Page/湯姆·福特.md "wikilink")、馬廄以及陵墓（[美國](../Page/美國.md "wikilink")[新墨西哥州](../Page/新墨西哥州.md "wikilink")）——進行中的計畫
  - 的克拉克藝術研究所擴建（美國[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")）——進行中的計畫

### 義大利

  - FABRICA （班尼頓藝術學校）（[義大利](../Page/義大利.md "wikilink")，1997年）
  - 亞曼尼劇場（[義大利](../Page/義大利.md "wikilink")，2001年）

### 中華民國（台灣）

  - [亞洲現代美術館](../Page/亞洲現代美術館.md "wikilink")（[亞洲大學](../Page/亞洲大學_\(台灣\).md "wikilink")）（[台灣](../Page/台灣.md "wikilink")
    [台中市](../Page/台中市.md "wikilink")
    [霧峰區](../Page/霧峰區.md "wikilink")，2013年）
  - [國立交通大學人文藝術館及建築館](../Page/國立交通大學.md "wikilink")（[台灣](../Page/台灣.md "wikilink")
    [新竹市](../Page/新竹市.md "wikilink")）——（已停工）
  - [龍巖人本櫻花墓園](../Page/龍巖人本.md "wikilink")，又名「光之殿堂」（台灣
    [新北市](../Page/新北市.md "wikilink")
    [三芝區](../Page/三芝區.md "wikilink")）——施工中
  - [大地教會](../Page/大地教會.md "wikilink")（台灣
    [澳底](../Page/澳底.md "wikilink")）——進行中的計畫
  - [山之教堂](../Page/山之教堂.md "wikilink") (台灣
    [嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉](../Page/阿里山鄉.md "wikilink")[樂野村](../Page/樂野村.md "wikilink"))
    ——進行中的計畫

### 中華人民共和國

  - 上海國際設計中心（[中國](../Page/中華人民共和國.md "wikilink")[上海市](../Page/上海市.md "wikilink")（已完成））——進行中的計畫
  - 杭州万科·良渚文化村藝術中心（已完成）
  - 上海保利大劇院（已完成）
  - 震旦美術館（上海市）

### 韓國

  - 済州島石之門 風之門（濟州島，2008年）
  - Museum SAN（[原州市](../Page/原州市.md "wikilink")，2012年）
  - Bonte Museum（濟州島，2012年）
  - 森之教會（驪州市，2015年）

### 阿拉伯

  - [海事博物館](../Page/海事博物館.md "wikilink") [Maritime
    Museum](../Page/Maritime_Museum.md "wikilink")
    [阿拉伯聯合大公國](../Page/阿拉伯聯合大公國.md "wikilink")[阿布達比](../Page/阿布達比.md "wikilink")——進行中的計畫

<File:413>, Taiwan, 台中市霧峰區南柳里 - panoramio (3).jpg|台中亞洲大學亞洲現代美術館，2013年
<File:Church> of Light.JPG|茨木春日丘教會，大阪府，1989年(普立茲克建築獎獲獎作品)
<File:Galleria> akka.jpg|[Galleria
akka](../Page/Galleria_akka.md "wikilink"),[大阪市](../Page/大阪市.md "wikilink")，1988年
<File:Pabellon_de_japon_expo_92.jpg>|[西班牙萬國博覽會日本政府館](../Page/西班牙萬國博覽會.md "wikilink")，1992年
[File:La_Collezione_1.jpg|安藤忠雄設計，位在東京](File:La_Collezione_1.jpg%7C安藤忠雄設計，位在東京)[青山的](../Page/青山_\(東京都港區\).md "wikilink")[La
Collezione](../Page/La_Collezione.md "wikilink") <File:Westin> Awaji
Island Hotel 06.jpg|淡路威斯汀飯店（[淡路夢舞台](../Page/淡路夢舞台.md "wikilink")）
[File:Westin_Awaji_Island_Hotel_02.jpg|淡路威斯汀飯店（淡路夢舞台百段苑](File:Westin_Awaji_Island_Hotel_02.jpg%7C淡路威斯汀飯店（淡路夢舞台百段苑)）
<File:Fort> Worth Texas Modern Art Museum
2003.jpg|[沃斯堡現代美術館](../Page/沃斯堡現代美術館.md "wikilink")（Modern
Art Museum of Fort Worth）

## 相關書籍

### 中文

  - 《閱讀安藤忠雄－建築創作作品論述》- 王建國著，2000/08，田園城市（ISBN 957-8440-97-9）
  - 《建築家的20歲年代》- 東京大學工學部建築學科、安藤忠雄研究室，2003/09，田園城市（ISBN 986-7705-18-1）
  - 《日本建築奇想與異人觀察》- 李清志著，2003/01，田園城市（ISBN 957-0406-91-7）
  - 《設計的未來考古學》- 商品學研究會編著，2003/01，田園城市（ISBN 957-0406-86-0）
  - 《都市地球學：日本三大建築家的都市論集》- 原廣司、槙文彥、黑川紀章著，2004/11，田園城市（ISBN 986-7705-55-6）
  - 《嬉遊城市光影間：歐洲六國建築之旅》- 謝國鐘著，2005/06，木馬文化（ISBN 986-7475-51-8）
  - 《建築大師談建築大師》- 蘇珊‧葛雷著，2005/08，木馬文化（ISBN 986-7475-58-5）
  - 《建築桂冠─普立茲克建築大師》- 施植明、黃健敏著，2005/09，木馬文化（ISBN 986-7475-67-4）
  - 《安藤忠雄的建築迷宮》- 李清志著，2007/04，大塊文化（ISBN 978-986-7059-74-1）

<!-- end list -->

  - 《安藤忠雄的都市徬徨》- 安藤忠雄著，2002/05，田園城市（ISBN 957-0406-60-7），譯自《安藤忠雄の都市彷徨》
  - 《安藤忠雄论建筑》 - 安藤忠雄著，2003/01，中国建筑工业出版社（ISBN
    978-7-112-05370-4），譯自《建築を語る》
  - 《安藤忠雄连战连败》 - 安藤忠雄著，2005/05，中国建筑工业出版社（ISBN
    978-7-112-07068-8），譯自《連戦連敗》
  - 《在建築中發現夢想：安藤忠雄談建築》 - 安藤忠雄著，2009/04，如果出版社（ISBN
    978-986-6702-31-0），譯自《建築に夢をみた》
  - 《建築家安藤忠雄》 - 安藤忠雄著，2009/10，商周出版（ISBN 986-6369-35-8）
  - 《安藤忠雄：我的人生履歷書》 - 安藤忠雄著，2012/11，聯經出版公司（ISBN 9789570840995 ）

### 日文

  - 《360映像で見る現代建築 安藤忠雄》<small>（360度看現代建築）</small>- 日経BP出版センター
  - 《連戦連敗》<small>（屢戰屢敗）</small>- 安藤忠雄著，2001/09/03，東京大学出版会
  - 《建築家という生き方―27人が語る仕事とこだわり》<small>（建築家的生存方式―27人談論工作和感想）</small>-
    安藤忠雄著，日経アーキテクチュア，2001/08，日経BP社
  - 《安藤忠雄》- Philip Jodidio著，2001/06，TASCHEN JAPAN
  - 《大工道具から世界が見える―建築・民俗・歴史そして文化》<small>（從木工用具看世界―建築・民俗・歴史以及文化）</small>-
    西和夫著，2001/04，五月書房
  - 《旅―インド・トルコ・沖縄》<small>（旅行―印度、土耳古、沖繩）</small>- 安藤忠雄著，2001/03，星雲社
  - 《安藤忠雄の美術館・博物館》<small>（安藤忠雄的美術館、博物館）</small>- 2001/02，美術出版社
  - 《光の教会―安藤忠雄の現場》<small>（光的教會―安藤忠雄的現場）</small>- 平松剛著，2000/12，建築資料研究社
  - 《淡路夢舞台―千年庭園の記録》<small>（淡路夢舞台―千年庭園的記錄）</small>- 安藤忠雄著，2000/05，新建築社
  - 《建築を語る》<small>（話說建築）</small>- 安藤忠雄著，1999/06，東京大学出版会
  - 《建築家たちの20代》<small>（建築家們的20歲年代）</small>-
    東京大学工学部建築学科安藤忠雄研究室著，1999/04，TOTO出版
  - 《直島コンテンポラリーアートミュージアム》<small>（直島現代美術館）</small>-
    安藤忠雄、三宅理一著，1996/09，鹿島出版会
  - 《アンドウ―安藤忠雄・建築家の発想と仕事》<small>（UNDOU―安藤忠雄・建築家的思考和工作）</small>-
    松葉一清著，1996/09，講談社
  - 《家》- 安藤忠雄著，1996/07，住まいの図書館出版局
  - 《現代デザインを学ぶ人のために》<small>（現代學習設計的人的理由）</small>- 嶋田厚著，1996/06，世界思想社
  - 《安藤忠雄の夢構想―震災復興と大阪湾ベイエリアプロジェクト》<small>（安藤忠雄的夢構想―震災復原與大阪灣區域計畫）</small>-
    安藤忠雄著，1995/10，朝日新聞社
  - 《サントリーミュージアム天保山》<small>（天保山三得利博物館）</small>- 安藤忠雄、三宅理一著，1995/10，鹿島出版会
  - 《壁の探究―安藤忠雄論》<small>（壁的研究―安藤忠雄論）</small>- 古山正雄著，1994/11，鹿島出版会
  - 《安藤忠雄ディテール集》<small>（安藤忠雄建築製圖集）</small>-
    安藤忠雄、二川幸夫著，1994/07，A.D.A.Edita Tokyo
  - 《安藤忠雄3》- 安藤忠雄著，SD編集部，1993/11，鹿島出版会
  - 《安藤忠雄の都市彷徨》<small>（安藤忠雄的都市焦慮）</small>- 安藤忠雄著，1992/05，マガジンハウス
  - 《安藤忠雄（1981‐1989）》- SD編輯部，1990/12，鹿島出版会
  - 《安藤忠雄―挑発する箱》<small>（安藤忠雄―煽動挑撥的箱子</small>）- 日本の建築家編集部著，1986/01，丸善
  - 《交感スルデザイン》- 安藤忠雄著，1985/09，六耀社
  - 《安藤忠雄のディテール―原図集 六甲の集合住宅・住吉の長屋》<small>（安藤忠雄建築製圖集
    六甲的集合住宅、住吉的長屋）</small>-
    安藤忠雄著，1984/01，彰国社
  - 《安藤忠雄》- 安藤忠雄著，SD編集部，1982/03，鹿島出版会

## 参考资料

## 外部連結

  - [安藤忠雄 Tadao Ando](http://www.tadao-ando.com/)
  - [台灣自由時報：與建築哲人安藤忠雄的空間對話](http://www.epochtimes.com/b5/5/3/6/n838756.htm)
  - [Great
    Buildings](http://www.greatbuildings.com/architects/Tadao_Ando.html)
  - [archINFORM資料庫：安藤忠雄](http://www.archinform.net/arch/474.htm)
  - [五十嵐太郎攝影集 Photo
    Archives](http://tenplusone.inax.co.jp/archive/ando/ando.html)
  - [安藤忠雄的網頁
    建築列表/連結](http://www.geocities.co.jp/Milano-Cat/2188/special03.html)
  - [建築檔案：安藤忠雄](https://web.archive.org/web/20061027130139/http://www.kbookmark.com/archfile/ando/)
  - [安藤忠雄（Tadao
    Ando）日本全国](https://web.archive.org/web/20060427205628/http://www.ken2-jp.com/kenken.cgi?category=97&tiiki=00)
  - [安藤忠雄亞洲大學現代美術館，落成紀念文題字](http://www.biosmonthly.com/contactd.php?id=4012)
  - [安藤忠雄作品图集](http://photo.zhulong.com/renwu/detail77.html)

[Category:日本建筑师](../Category/日本建筑师.md "wikilink")
[Category:普利兹克奖得主](../Category/普利兹克奖得主.md "wikilink")
[Category:國立交通大學榮譽博士](../Category/國立交通大學榮譽博士.md "wikilink")
[Category:法國榮譽軍團勳章持有人](../Category/法國榮譽軍團勳章持有人.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:東京大學名譽教授](../Category/東京大學名譽教授.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:大阪艺术大学校友](../Category/大阪艺术大学校友.md "wikilink")
[Category:高松宫殿下纪念世界文化奖获得者](../Category/高松宫殿下纪念世界文化奖获得者.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")

1.  [【安藤忠雄迷注意】東京個展　展出過百住宅模型](http://bkb.mpweekly.com/cu0004/20170912-51194)