**安德斯·福格·拉斯穆森**（**Anders Fogh
Rasmussen**，）[丹麦](../Page/丹麦.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[丹麦自由党人](../Page/丹麦自由党.md "wikilink")，[北约前秘书长](../Page/北约.md "wikilink")，2001年－2009年曾任[丹麦首相](../Page/丹麦首相列表.md "wikilink")。

## 生平

拉斯穆森毕业于丹麦[奥胡斯大学](../Page/奥胡斯大学.md "wikilink")，获[经济学科学硕士学位](../Page/经济学.md "wikilink")。

## 政治生涯

1978年首次当选[丹麥国会议员](../Page/丹麥国会.md "wikilink")，1985年当选为自由党副主席，1987年－1992年任税务大臣。1990年12月-1993年1月任经济和税务大臣。

1998年3月15日当选自由党主席，成為黨主席後把自由黨轉向右翼，並與其他政黨組成中間偏右聯盟。2001年11月27日，由[丹麥社會民主黨組成的中間偏左聯合政府在大选中失利](../Page/丹麥社會民主黨.md "wikilink")，社會民主黨更失去77年來的第一大黨地位。拉斯穆森其後宣布成立由他出任首相，自由黨及保守人民黨組成的中間偏右少数派联合政府，並獲[右派民粹主義的](../Page/右派民粹主義.md "wikilink")[丹麥人民党支持信任動議](../Page/丹麥人民党.md "wikilink")。2005年及2007年領導中間偏右聯盟成功連任。

在2009年4月4日召开的[北约](../Page/北约.md "wikilink")[斯特拉斯堡会议上](../Page/斯特拉斯堡.md "wikilink")，拉斯穆森被任命为第12任[北大西洋公约组织秘书长](../Page/北大西洋公约组织.md "wikilink")，从8月1日起接替离职的[夏侯雅伯出任](../Page/夏侯雅伯.md "wikilink")[北约秘书长](../Page/北约.md "wikilink")。\[1\]\[2\]他將於2014年10月卸任。

## 姓氏

雖然拉斯穆森與前任[波爾·尼魯普·拉斯穆森和繼任人](../Page/波爾·尼魯普·拉斯穆森.md "wikilink")[拉爾斯·勒克·拉斯穆森的姓氏相同](../Page/拉爾斯·勒克·拉斯穆森.md "wikilink")，但他與二人並無親屬關係。[拉斯穆森是丹麥第九常見的姓氏](../Page/拉斯穆森.md "wikilink")。\[3\]

## 参考文献

{{-}}

[Category:利比亞內戰人物](../Category/利比亞內戰人物.md "wikilink")
[Category:北大西洋公約組織秘書長](../Category/北大西洋公約組織秘書長.md "wikilink")
[Category:丹麦首相](../Category/丹麦首相.md "wikilink")
[Category:丹麥自由黨黨員](../Category/丹麥自由黨黨員.md "wikilink")
[Category:丹麥經濟學家](../Category/丹麥經濟學家.md "wikilink")
[R](../Category/奥胡斯大学校友.md "wikilink")
[Category:丹麥信義宗教徒](../Category/丹麥信義宗教徒.md "wikilink")
[Category:歐洲理事會主席](../Category/歐洲理事會主席.md "wikilink")

1.
2.
3.  丹麥統計局：http://www.dst.dk/Statistik/Navne/pop/2009.aspx