**安赫爾瀑布**（，[pemón語](../Page/pemón語.md "wikilink")：**Kerepakupay
Vená**或**Parakupá
Vená**），又譯**安琪爾瀑布**，有時依字義譯為**天使瀑布**，但不合語源；又名**丘倫梅鲁瀑布**，位于[南美洲委内瑞拉](../Page/南美洲.md "wikilink")[玻利瓦爾州圭亞那](../Page/玻利瓦爾州.md "wikilink")[高原的](../Page/高原.md "wikilink")[卡罗尼河支流](../Page/卡罗尼河.md "wikilink")[丘倫河上](../Page/丘倫河.md "wikilink")，藏身于的[委内瑞拉与](../Page/委内瑞拉.md "wikilink")[圭亞那的高原密林深處](../Page/圭亞那.md "wikilink")。

安赫尔瀑布是世界上最高的[瀑布](../Page/瀑布.md "wikilink")，當水從瀑布上流下，在落地之前會先蒸發掉，到底部又再次凝結成雨，是世上難得的奇蹟之一。丘倫河水从平頂高原[奥揚特普伊山](../Page/奥揚特普伊山.md "wikilink")（[Auyan-tepui](../Page/:en:Auyan-tepui.md "wikilink")）直流而下，宽150米，總落差979米（3212英尺），以離丘倫河谷地172米的分結晶岩平台为界，瀑布分为兩级，而最長一級瀑布高807米（2648英尺）。

安赫尔瀑布曾出现在电影《[恐龙](../Page/恐龙_\(电影\).md "wikilink")》中。在電影《[-{zh-cn:飞屋环游记;zh-tw:天外奇蹟;zh-hk:沖天救兵}-](../Page/飞屋环游记.md "wikilink")》中，“天堂瀑布”也取材于此。\[1\]

## 歷史

  - 1935年，西班牙人[卡多纳首次發现了原本只有本地](../Page/卡多纳.md "wikilink")[印第安人才知曉的丘倫梅魯瀑布](../Page/印第安人.md "wikilink")。
  - 1937年，[美国探险家](../Page/美国.md "wikilink")[詹姆斯·安赫爾在空中对瀑布進行考察時](../Page/詹姆斯·安赫爾.md "wikilink")，因一次不成功的降落緊急迫降在山頂。之後，在食物短缺的情况下，他在荒野中擕妻子徒步跋涉11天才到達最近的村鎮，被當地人傳為奇蹟。1956年，安赫爾在巴拿馬死于一次飛行事故。遵照他的遺願，安赫爾的兒子將其骨灰撒在了此瀑布中，以後，委内瑞拉也将瀑布命名為「安赫爾」。

## 参考文献

## 外部链接

  -
[Category:玻利瓦爾州地理](../Category/玻利瓦爾州地理.md "wikilink")
[Category:奧里諾科河](../Category/奧里諾科河.md "wikilink")
[Category:委內瑞拉瀑布](../Category/委內瑞拉瀑布.md "wikilink")
[Category:委內瑞拉世界遺產](../Category/委內瑞拉世界遺產.md "wikilink")
[Category:委内瑞拉地理的世界之最](../Category/委内瑞拉地理的世界之最.md "wikilink")
[Category:水体之最](../Category/水体之最.md "wikilink")

1.  [飛屋環遊記--一部讓人類重新審視自己的電影](http://www.mtime.com/group/disney/discussion/630653/)
    － 時光網