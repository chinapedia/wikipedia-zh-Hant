[Locatie_Andamanse_Zee.PNG](https://zh.wikipedia.org/wiki/File:Locatie_Andamanse_Zee.PNG "fig:Locatie_Andamanse_Zee.PNG")
[Andaman_sea.png](https://zh.wikipedia.org/wiki/File:Andaman_sea.png "fig:Andaman_sea.png")
**安達曼海**（Andaman Sea；; , ），又名**緬甸海**\[1\]（Burma Sea；,
），是一個[孟加拉灣東南部的](../Page/孟加拉灣.md "wikilink")[水體](../Page/水體.md "wikilink")，位於[印度洋東北](../Page/印度洋.md "wikilink")，南北長約1100公里，東西寬約600公里，[面積約](../Page/面積.md "wikilink")。安達曼海的西面是[印度的](../Page/印度.md "wikilink")[安達曼群島](../Page/安達曼群島.md "wikilink")，東面是[馬來半島](../Page/馬來半島.md "wikilink")，北依[泰國和](../Page/泰國.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")[伊洛瓦底江三角洲](../Page/伊洛瓦底江.md "wikilink")，東南面與著名的[麻六甲海峽相連](../Page/麻六甲海峽.md "wikilink")，是[南中国海與](../Page/南中国海.md "wikilink")[印度洋間的重要水道](../Page/印度洋.md "wikilink")。亦於2017年6月7日發生空難，導致29人死亡，93人失蹤

安達曼海各處水深差距極大，但是只有不到5%的海域水深超過，最深處在靠近安達曼群島的一系列海底深谷，水深超過\[2\]。表面水溫冬季為27.5攝氏度，夏季為30攝氏度，波動很小。

在安達曼海海底沿南北走向有一道地震斷層，在其西邊是[緬甸板塊](../Page/緬甸板塊.md "wikilink")，東邊是[巽他板塊](../Page/巽他板塊.md "wikilink")。當地的許多島嶼是火山島，不過目前只有一座島上還有火山活動。

安達曼海傳統上是沿岸各國漁業及貨物運輸的交通要道，海域內的[珊瑚礁與島嶼也是熱門的旅遊地點](../Page/珊瑚礁.md "wikilink")。2004年12月26日發的[印度洋大地震引起的嚴重](../Page/2004年印度洋大地震.md "wikilink")[海嘯](../Page/海嘯.md "wikilink")，對當地的漁業及旅遊業的基礎建設造成嚴重破壞。

## 參考資料

[Category:印度洋陆缘海](../Category/印度洋陆缘海.md "wikilink")
[Category:緬甸海域](../Category/緬甸海域.md "wikilink")
[Category:印度海域](../Category/印度海域.md "wikilink")
[Category:泰國海域](../Category/泰國海域.md "wikilink")
[Category:印度尼西亞海域](../Category/印度尼西亞海域.md "wikilink")
[Category:緬甸-泰國邊界](../Category/緬甸-泰國邊界.md "wikilink")
[Category:印度-緬甸邊界](../Category/印度-緬甸邊界.md "wikilink")

1.
2.