**安里積千代**（），出生於[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[座間味村的日本](../Page/座間味村.md "wikilink")[政治家](../Page/政治家.md "wikilink")，曾擔任[美國治理琉球時期](../Page/美國治理琉球時期.md "wikilink")[八重山群島政府](../Page/群島政府_\(琉球\)#八重山群島政府.md "wikilink")[知事](../Page/知事.md "wikilink")，[琉球政府](../Page/琉球政府.md "wikilink")[立法院](../Page/立法院_\(琉球政府\).md "wikilink")[議員](../Page/議員.md "wikilink")，以及日本[眾議院議員](../Page/眾議院.md "wikilink")。。

## 生平

  - 1928年（[昭和](../Page/昭和.md "wikilink")3年）：畢業於[日本大學法學部](../Page/日本大學.md "wikilink")。
  - 1931年：前往[台灣](../Page/台灣.md "wikilink")。
  - 1935年：擔任[台南市議會議員](../Page/台南市.md "wikilink")。
  - 1950年：[美國民政府於](../Page/琉球列島美國民政府.md "wikilink")[琉球列島成立各](../Page/琉球列島.md "wikilink")[群島政府](../Page/群島政府_\(琉球\).md "wikilink")，安里積千代參加八重山群島政府知事選舉，並擊敗原擔任[八重山民政府知事的](../Page/八重山民政府.md "wikilink")[吉野高善成為新任知事](../Page/吉野高善.md "wikilink")。
  - 1952年：美國民政府廢除群島政府改設置琉球政府，安里積千代參加立法院議員選舉並當選。
  - 1955年：當選沖繩社會大眾黨委員長。
  - 1970年：當選眾議院議員，後持續二任期，任期中加入民社黨。

[Category:日本眾議院議員
1969–1972](../Category/日本眾議院議員_1969–1972.md "wikilink")
[Category:日本眾議院議員
1972–1976](../Category/日本眾議院議員_1972–1976.md "wikilink")
[Category:美國統治琉球時期人物](../Category/美國統治琉球時期人物.md "wikilink")
[Category:琉球族政治人物](../Category/琉球族政治人物.md "wikilink")
[Category:台灣日治時期政治人物](../Category/台灣日治時期政治人物.md "wikilink")
[Category:日本大學校友](../Category/日本大學校友.md "wikilink")
[Category:沖繩縣選出日本眾議院議員](../Category/沖繩縣選出日本眾議院議員.md "wikilink")