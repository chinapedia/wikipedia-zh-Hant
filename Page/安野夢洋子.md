**安野夢洋子**（）是日本漫畫家。[khara工作室董事](../Page/khara工作室.md "wikilink")。出生于[東京都](../Page/東京都.md "wikilink")[杉並區](../Page/杉並區.md "wikilink")，成长于[东京都](../Page/东京都.md "wikilink")[多摩市](../Page/多摩市.md "wikilink")。O型血。毕业于
（现为）。高中三年級時出道，陸續推出多部作品，當中有些被拍成動畫或連續劇。丈夫是著名動畫導演[庵野秀明](../Page/庵野秀明.md "wikilink")。

## 经历

2012年4月6日，繼其夫庵野秀明的名字被用作[小行星命名后](../Page/小行星9081.md "wikilink")，安野梦洋子的名字也被用于命名火星与木星轨道间的一颗小行星（编号300082，，）。
\[1\] \[2\]。

## 漫畫作品

  - 《超感電少女莫娜》（）
      - 1994年連載
  - 《TRUMPS\!》（）
      - 1994年連載
  - 《Peek a boo\!》（）
      - 1995年連載
  - 《HAPPY MANIA‧戀愛暴走族》（）
      - 1995年連載，1998年拍成連續劇
  - 《旋轉木馬中的傑麗》（）
      - 1997年連載
  - 《外星戀曲》（）
      - 1997年連載
  - 《》（）
      - 1997年連載
  - 《Chasing Amy》（）
      - 1998年連載
  - 《愛情大師 X》（）
      - 1998年連載
  - 《天使幻音》（）
      - 1999年連載
  - 《變色龍軍團》（）
      - 1999年連載
  - 《JELLY BEANS 荳蔻奇夢》（）
      - 1999年連載
  - 《Tundra Blue Ice》（）
      - 2000年連載
  - 《花與蜜蜂》（）
      - 2000年連載
  - 《寶貝 G》（）
      - 2001年連載
  - 《[惡女花魁](../Page/惡女花魁.md "wikilink")》（）
      - 2001年連載
      - 港譯《櫻花夢》台譯《煙花夢》
  - 《[魔女的考驗](../Page/魔女的考驗.md "wikilink")》（）
      - 2003年連載
      - 港譯《魔界女王候補生》
      - 曾經在第29回[講談社漫畫賞兒童部門得獎](../Page/講談社漫畫賞.md "wikilink")
  - 《工作狂人》（）
      - 2004年連載
  - 《[監督不行屆](../Page/監督不行屆.md "wikilink")》（）
      - 2005年連載
      - 台釋《監督脫線日記》
  - 《櫻花夢》（港譯，原名《》）
  - 《小不點》
      - 2008年 朝日新聞 朝日新聞出版

## 其它作品

  - 《美人畫報》

## 外部連結

  - [安野モヨコ
    蜂蜜](https://web.archive.org/web/20060128083316/http://www.annomoyoco.com/)

  - [安野モヨコ Anno Moyoco Official website
    蜂蜜](http://www.pink.ne.jp/~moyoco/profile/index.html)

  - [jp.yahoo安野夢洋子特集(揭載期間：2006年2月1日～3月15日)](http://books.yahoo.co.jp/special/annomoyoco/)

  - [找寻属于自己的原创
    安野Moyoco](http://asianbeat.com/cs/selection/archive_20080608_144942.html)

  -
  -
## 参考资料

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[安野夢洋子](../Category/安野夢洋子.md "wikilink")
[Category:日本女性漫畫家](../Category/日本女性漫畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.
2.