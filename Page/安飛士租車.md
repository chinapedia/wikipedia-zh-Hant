**安飛士租車公司**（）是一家屬於[汽車租賃的](../Page/汽車租賃.md "wikilink")[跨國企業](../Page/跨國企業.md "wikilink")，母公司為[美國](../Page/美國.md "wikilink")[安飛士·巴吉集團](../Page/安飛士·巴吉集團.md "wikilink")（Avis
Budget Group, Inc.）。[創辦人](../Page/創辦人.md "wikilink")（Warren
Avis）於1946年在[密西根州](../Page/密歇根州.md "wikilink")[底特律的](../Page/底特律.md "wikilink")（Willow
Run
Airport）以8萬5千[美元創立](../Page/美元.md "wikilink")，這是全球第一個將據點設於機場的租車公司\[1\]。如今，安飛士租車公司成為全球第一大汽車租賃公司，超越了[赫茲租車](../Page/赫茲租車.md "wikilink")\[2\]。

## 公司沿革

1946年華倫·安飛士（Warren Avis）於美國密西根州底特律的威洛魯恩機場（Willow Run
Airport）以8萬5千[美元創立安飛士租車公司](../Page/美元.md "wikilink")，稍後並在[邁阿密機場成立另一個據點](../Page/邁亞密國際機場.md "wikilink")。公司業務拓展得很快，1953年已經在[歐洲](../Page/歐洲.md "wikilink")、[加拿大與](../Page/加拿大.md "wikilink")[墨西哥等國家地區設置據點](../Page/墨西哥.md "wikilink")。不過1954年華倫·安飛士以8百萬美元將其股權出售給[波士頓的金融家理查](../Page/波士頓.md "wikilink")·羅比（Richard
S. Robie）\[3\]。後來該公司經過數度轉手易主，直到2006年正式更名成**安飛士租車系統有限公司**（Avis Rent A Car
System, LLC），並隸屬於[安飛士·巴吉集團](../Page/安飛士·巴吉集團.md "wikilink")（Avis Budget
Group, Inc.）旗下。安飛士租車公司在美國境內設有1,249個據點，在全世界其他國家地區則設有約2,900個據點。

該公司的[廣告標語為](../Page/廣告標語.md "wikilink")「We Try
Harder」，成形於1963年，中譯成「再接再厲」\[4\]。

### 在華人地區的發展

2002年[安飛士·巴吉集團與](../Page/安飛士·巴吉集團.md "wikilink")[上海汽車集團股份有限公司所屬的上海汽車工業銷售有限公司各出資](../Page/上汽集团.md "wikilink")50%，組建了「安吉汽車租賃有限公司」，正式將安飛士汽車引進[中國市場](../Page/中國.md "wikilink")；目前該公司在該地約40個各級[城市已建立約](../Page/城市.md "wikilink")180個據點、8千餘輛車的規模。在[香港](../Page/香港.md "wikilink")，則已設立2個服務據點。

2012年6月美國[安飛士租車宣佈在臺灣成立分公司](../Page/安飛士租車.md "wikilink")，開始營運租車業務\[5\]目前在[新竹縣](../Page/新竹縣.md "wikilink")[竹北市設立分公司](../Page/竹北市.md "wikilink")，並於全臺設有超過30個服務據點\[6\]。

## 內部連結

  - [安飛士·巴吉集團](../Page/安飛士·巴吉集團.md "wikilink")
  - [巴吉租車](../Page/巴吉租車.md "wikilink")

## 參考資料

## 外部連結

  - [美國官方網站](http://www.avis.com/)
  - [臺灣官方網站](http://www.avis-taiwan.com/)
  - [中國官方網站](http://www.avis.cn/)
  - [香港官方網站](http://www.avis.com.hk/)

[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:美國公司](../Category/美國公司.md "wikilink")
[Category:汽車租賃公司](../Category/汽車租賃公司.md "wikilink")
[Category:1946年成立的公司](../Category/1946年成立的公司.md "wikilink")

1.  參看[Car-Rental Pioneer Avis Dies
    at 92](http://www.npr.org/templates/story/story.php?storyId=9814473)，第一段。
2.  參看[Car-Rental Pioneer Avis Dies
    at 92](http://www.npr.org/templates/story/story.php?storyId=9814473)，第四段。
3.  參看[Avis: Historical
    Chronology](http://www.avis.com/car-rental/content/display.ac?navId=T6M21S03)，第四行。
4.  參看[AVIS介紹](http://www.avis.tw/about.aspx) ，第二段。
5.  參看[加碼投資臺灣，Avis Taiwan簽署投資意向書](http://news.u-car.com.tw/17136.html)。
6.  請見[美國在台協會攜手AVIS國際租車共同行銷](http://b2b.travelrich.com.tw/subject02/subject02_detail.aspx?Second_classification_id=32&Subject_id=14648)
    。