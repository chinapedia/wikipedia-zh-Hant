**宋以朗**（Roland
Soong，），香港知名[博客](../Page/博客.md "wikilink")**東南西北**（zonaeuropa.com）的創立人。他是統計學博士，曾任全球第二大統計公司KMR的顧問，父親[宋淇](../Page/宋淇.md "wikilink")，[筆名林以亮](../Page/筆名.md "wikilink")，是著名《[紅樓夢](../Page/紅樓夢.md "wikilink")》翻譯家之一，曾任電懋製片，拍過梁醒波主演的《南北和》。母親[鄺文美](../Page/鄺文美.md "wikilink")，曾在[美國新聞處工作](../Page/美國新聞處.md "wikilink")，以方馨一名，翻譯文學作品。宋以朗現擁有[張愛玲小說的版權](../Page/張愛玲.md "wikilink")。

宋以朗近年製作東南西北博客，引述及翻譯比較世界各地的新聞，受到廣泛注目。　

## 生平

宋以朗1949年生於[上海](../Page/上海.md "wikilink")，當時其父宋淇在[燕京大學念](../Page/燕京大學.md "wikilink")[比較文學](../Page/比較文學.md "wikilink")，母[鄺文美則在](../Page/鄺文美.md "wikilink")[上海聖約翰大學念文學](../Page/上海聖約翰大學.md "wikilink")，在上海時經營賣藥和出入口等生意。宋以朗出生4個星期，[中國大陸](../Page/中國大陸.md "wikilink")[赤化在即](../Page/赤化.md "wikilink")，宋氏一家逃難到[香港](../Page/香港.md "wikilink")。宋淇來港後，遇到銀行倒閉，家財盡失。

父母二人50年代曾出外打工，靠翻譯小說賺稿費。其間，宋淇認了[鄒文懷](../Page/鄒文懷.md "wikilink")、[胡金銓等上海人](../Page/胡金銓.md "wikilink")，加入電影圈，10年間先後任職[懋業公司](../Page/懋業.md "wikilink")、[邵氏和](../Page/邵氏.md "wikilink")[嘉禾](../Page/嘉禾.md "wikilink")，拍國語片賣埠，隨後又翻譯《[紅樓夢](../Page/紅樓夢.md "wikilink")》。

宋以朗曾就讀[喇沙書院](../Page/喇沙書院.md "wikilink")，1967年[六七暴動時](../Page/六七暴動.md "wikilink")，宋父擔心宋以朗姐弟倆被牽連，把他們送出國。1978年於[美國](../Page/美國.md "wikilink")[紐約州立大學石溪分校博士畢業](../Page/紐約州立大學石溪分校.md "wikilink")，在美國生活了30年，03年回港，創立東南西北博客。

## 網站內容

該英文網站的內容，主要譯自中港兩地的論壇文章、博客內容和新聞報道，再比較國際通訊社的內容，網站本身對新聞內容並不提供鮮明的預設立場。

[2005年太石村罷免事件發生時](../Page/2005年太石村罷免事件.md "wikilink")，[英國](../Page/英國.md "wikilink")《[衛報](../Page/衛報.md "wikilink")》記者Benjamin
Joffe
Walt說協助他入村採訪的人被打到頸骨折斷\[1\]，但《南方日報》、《燕南論壇》在百里州醫院找到被打傷的呂邦列，證實他沒大礙，頸骨也不曾折斷。這些消息經東南西北翻譯及發布後，引起廣泛注目，衛報後來對誇張的新聞情節發表公開道歉。

近年東南西北不時向國際社會展示中國民族主義抬頭的現況。該博客常用的標題如「某某已成為賣國賊」（Someone is now a
traitor)，講述中國[憤青對](../Page/憤青.md "wikilink")[劉德華身穿日本服裝](../Page/劉德華.md "wikilink")、[章子怡結交外國男友時的反應](../Page/章子怡.md "wikilink")。

## 參考

<references />

:\* 明報：上海幫第二代的告白 2006-10-01

:\* 壹週刊：天外有天宋以朗 2005-12-08

[S](../Category/網誌.md "wikilink") [S](../Category/香港人.md "wikilink")
[Y](../Category/宋姓.md "wikilink") [S](../Category/上海人.md "wikilink")
[S](../Category/喇沙書院校友.md "wikilink")

1.  [衛報：Chinese activist vows to continue, despite
    beating](http://www.guardian.co.uk/china/story/0,7369,1589974,00.html)
    2005年10月12日更新