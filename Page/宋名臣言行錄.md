《**宋名臣言行錄**》共七十五卷，由[南宋](../Page/南宋.md "wikilink")[朱熹](../Page/朱熹.md "wikilink")、[李幼武所編](../Page/李幼武.md "wikilink")，朱熹撰前集十卷，後集十四卷，共二十四卷。李幼武撰續集八卷、別集二十六卷、外集十七卷，共五十一卷。該書彙編了散見於筆記、碑傳、行狀中的宋代重要人物的事蹟，共收入北宋以及南宋人物225人。今印行的前集，其原書名為《五朝名臣言行錄》，後集為《三朝名臣言行錄》，二書並稱為《八朝名臣言行錄》。若不包含附傳的人物，收入的北宋人物共97人，前集收入55人，後集收入42人。

## 成書經過

《八朝名臣言行錄》初稿完成於南宋孝宗乾道八年（1172年），不久刻書於[建陽](../Page/建陽.md "wikilink")\[1\]。
朱熹於本書自敘：「予讀近代文集及記事之書，觀其所載國朝名臣言行之迹，多有補於世教者。然以其散出而無統也，既莫究其始終表裏之全，而又汨於虛浮怪誕之說，予常病之。於是掇取其要，聚為此錄，以便記覽。尚恨書籍不備，多所遺闕，嗣有所得，當續書之。」故朱熹編此書，其目的在於整理散亂而不統一的史料，將北宋名臣的事迹中可信的部分保留下來，而非摻入一家之言，故書中不見他個人的評論。該書的作用，除了作為可信的史料外，亦可作為後世君臣的借鏡。

## 編輯體例

  - 收入的名臣根據其侍奉的北宋各朝依序排列下來，該書一共有八朝，朱熹先後分編五朝與三朝。五朝為宋太祖、太宗、真宗、仁宗和英宗，三朝為神宗、哲宗和徽宗，北宋九朝獨缺欽宗這一朝。
  - 此書名雖然取名為《八朝名臣言行錄》，但其實也順便收羅了不少北宋皇帝的言行，以及旁錄了一些小人、奸臣的言行，有正面也有負面，皆可以做為對後人的警惕。
  - 對於所有名臣事迹所摘錄的史料，朱熹皆會一一註明原始出處。而引用來源的作者大多在當時為北宋朝廷任官的名臣，故所記載之事多為親眼所見、親耳所聞。如司馬光著《涑水記聞》、曾鞏《隆平集》。
  - 《八朝名臣言行錄》所附註文，除引自他書提供補充說明外，兼有資料比對、校勘訛誤之功。正式成書前，註文曾歷多次更動、增補，部分為朱熹與門人討論後所定。
  - 《宋名臣言行錄》的前集、後集，為李衡校勘、刪節《八朝名臣言行錄》後的版本，雖然體例變動不大，但正文、註文卻大幅度縮減，甚至誤校，亦有註文混入正文的情形\[2\]。
  - 南宋以後，不斷有人模仿《八朝名臣言行錄》的體例。如元．蘇天爵《元朝名臣事略》\[3\]、清．沈佳《明儒言行錄》\[4\]、明．徐咸《近代名臣言行錄》、清．朱桓《歷代名臣言行錄》等。

## 流傳版本

  - 四庫本、文海本，為李衡大量刪節朱熹《八朝名臣言行錄》後的校正本，李幼武據此本連同自輯的三種著作合刻進去，名為《皇朝名臣言行錄》（宋亡後稱《宋名臣言行錄》），成書之後此版本逐漸取代原本在民間廣為流傳。
  - 四部叢刊影印宋本，宋末已不多見，為朱熹《八朝名臣言行錄》的原本\[5\]。雖然有極少部分缺葉，但可從四庫本、文海本補入。故四部叢刊本為現今新校標點本主要依據的版本，是目前最佳的版本。

## 爭議

  - 後世對編撰宗旨的解讀不一：該書所收錄的人物為多數為當時的名臣，但有若干人物在後世較具爭議性，被認為不該收入進去，如王安石、趙普、呂惠卿等人行事風格接近小人，則受到《四庫全書總目》提要的質疑。\[6\]\[7\]然而，有些學者認為朱熹編此書著重的是嚴謹的史學態度，而「有補於世教」乃其次，況且這些人物對國家社稷並非毫無建樹，且對當時的政壇具有相當的影響力，亦符合名臣的條件，故被收入書中並沒有不合理。\[8\]\[9\]
  - 成書之後的第二年，即受到友人呂祖謙的責難，乃為先人呂夷簡勸宋仁宗廢郭后一事爭辨，認為朱熹所根據的史料來源是有問題的書。朱熹回信表示會再修訂內容\[10\]。私下卻對人表示曾見過《涑水記聞》原稿，所以相信沒有被司馬光的家人補入，所以堅持保留這段史料而不刪去。\[11\]

## 人物列表

### 前集

  - [趙普](../Page/趙普.md "wikilink")
  - [曹彬](../Page/曹彬.md "wikilink")
  - [范質](../Page/范質.md "wikilink")
  - [竇儀](../Page/竇儀.md "wikilink")
  - [李昉](../Page/李昉.md "wikilink")
  - [呂蒙正](../Page/呂蒙正.md "wikilink")
  - [張齊賢](../Page/張齊賢.md "wikilink")
  - [呂端](../Page/呂端.md "wikilink")
  - [錢若水](../Page/錢若水.md "wikilink")
  - [李沆](../Page/李沆.md "wikilink")
  - [王旦](../Page/王旦.md "wikilink")
  - [向敏中](../Page/向敏中.md "wikilink")
  - [陳恕](../Page/陳恕.md "wikilink")
  - [張詠](../Page/張詠.md "wikilink")
  - [馬知節](../Page/馬知節.md "wikilink")
  - [曹瑋](../Page/曹瑋.md "wikilink")
  - [畢士安](../Page/畢士安.md "wikilink")
  - [寇準](../Page/寇準.md "wikilink")
  - [高瓊](../Page/高瓊.md "wikilink")
  - [楊億](../Page/楊億.md "wikilink")
  - [王曙](../Page/王曙.md "wikilink")
  - [王曾](../Page/王曾.md "wikilink")
  - [李迪](../Page/李迪.md "wikilink")
  - [魯宗道](../Page/魯宗道.md "wikilink")
  - [薛奎](../Page/薛奎.md "wikilink")
  - [蔡齊](../Page/蔡齊.md "wikilink")
  - [呂夷簡](../Page/呂夷簡.md "wikilink")
  - [陳堯佐](../Page/陳堯佐.md "wikilink")
  - [晏殊](../Page/晏殊.md "wikilink")
  - [宋庠](../Page/宋庠.md "wikilink")
  - [韓億](../Page/韓億.md "wikilink")
  - [程琳](../Page/程琳_\(北宋\).md "wikilink")
  - [杜衍](../Page/杜衍.md "wikilink")
  - [范仲淹](../Page/范仲淹.md "wikilink")
  - \-{[种世衡](../Page/种世衡.md "wikilink")}-
  - [龐籍](../Page/龐籍.md "wikilink")
  - [狄青](../Page/狄青.md "wikilink")
  - [吳育](../Page/吳育.md "wikilink")
  - [王堯臣](../Page/王堯臣.md "wikilink")
  - [包拯](../Page/包拯.md "wikilink")
  - [王德用](../Page/王德用.md "wikilink")
  - [田錫](../Page/田錫.md "wikilink")
  - [王禹偁](../Page/王禹偁.md "wikilink")
  - [孫奭](../Page/孫奭.md "wikilink")
  - [李及](../Page/李及.md "wikilink")
  - [孔道輔](../Page/孔道輔.md "wikilink")
  - [尹洙](../Page/尹洙.md "wikilink")
  - [余靖](../Page/余靖.md "wikilink")
  - [王質](../Page/王質.md "wikilink")
  - [孫甫](../Page/孫甫.md "wikilink")
  - [陳摶](../Page/陳摶.md "wikilink")
      - [穆脩](../Page/穆脩.md "wikilink")
      - [李之才](../Page/李之才.md "wikilink")
      - \-{[种放](../Page/种放.md "wikilink")}-
      - [魏野](../Page/魏野.md "wikilink")
      - [林逋](../Page/林逋.md "wikilink")
  - [胡瑗](../Page/胡瑗.md "wikilink")
  - [孫復](../Page/孫復.md "wikilink")
  - [石介](../Page/石介.md "wikilink")
  - [蘇洵](../Page/蘇洵.md "wikilink")

### 後集

  - [韓琦](../Page/韓琦.md "wikilink")
  - [富弼](../Page/富弼.md "wikilink")
  - [歐陽脩](../Page/歐陽脩.md "wikilink")
  - [文彥博](../Page/文彥博.md "wikilink")
  - [趙概](../Page/趙概.md "wikilink")
  - [吳奎](../Page/吳奎.md "wikilink")
  - [張方平](../Page/張方平.md "wikilink")
  - [胡宿](../Page/胡宿.md "wikilink")
  - [蔡襄](../Page/蔡襄.md "wikilink")
  - [王素](../Page/王素.md "wikilink")
  - [劉敞](../Page/劉敞.md "wikilink")
  - [唐介](../Page/唐介.md "wikilink")
  - [趙抃](../Page/趙抃.md "wikilink")
  - [呂誨](../Page/呂誨.md "wikilink")
  - [彭思永](../Page/彭思永.md "wikilink")
  - [范鎮](../Page/范鎮.md "wikilink")
  - [曾公亮](../Page/曾公亮.md "wikilink")
  - [王安石](../Page/王安石.md "wikilink")
  - [司馬光](../Page/司馬光.md "wikilink")
      - [司馬康](../Page/司馬康.md "wikilink")
  - [呂公著](../Page/呂公著.md "wikilink")
      - [呂希哲](../Page/呂希哲.md "wikilink")
  - [曾鞏](../Page/曾鞏.md "wikilink")
  - [曾肇](../Page/曾肇.md "wikilink")
  - [蘇軾](../Page/蘇軾.md "wikilink")
  - [蘇轍](../Page/蘇轍.md "wikilink")
  - [韓絳](../Page/韓絳.md "wikilink")
  - [韓維](../Page/韓維.md "wikilink")
  - [傅堯俞](../Page/傅堯俞.md "wikilink")
  - [彭汝礪](../Page/彭汝礪.md "wikilink")
  - [范純仁](../Page/范純仁.md "wikilink")
  - [王存](../Page/王存.md "wikilink")
  - [蘇頌](../Page/蘇頌.md "wikilink")
  - [劉摯](../Page/劉摯.md "wikilink")
  - [王巖叟](../Page/王巖叟.md "wikilink")
  - [劉安世](../Page/劉安世.md "wikilink")
  - [范祖禹](../Page/范祖禹.md "wikilink")
  - [鄒浩](../Page/鄒浩.md "wikilink")
  - [陳瓘](../Page/陳瓘.md "wikilink")
  - [邵雍](../Page/邵雍.md "wikilink")
  - [陳襄](../Page/陳襄.md "wikilink")
  - [劉恕](../Page/劉恕.md "wikilink")
  - [徐積](../Page/徐積.md "wikilink")
  - [陳師道](../Page/陳師道.md "wikilink")

### 續集

  - [黃庭堅](../Page/黃庭堅.md "wikilink")、[任伯雨](../Page/任伯雨.md "wikilink")、[江公望](../Page/江公望.md "wikilink")、[豐稷](../Page/豐稷.md "wikilink")、[陳過庭](../Page/陳過庭.md "wikilink")、[陳師錫](../Page/陳師錫.md "wikilink")
  - [吳敏](../Page/吳敏.md "wikilink")、[曹輔](../Page/曹輔.md "wikilink")、[孫傳](../Page/孫傳.md "wikilink")、[許份](../Page/許份.md "wikilink")、[錢即](../Page/錢即.md "wikilink")、[种師道](../Page/种師道.md "wikilink")
  - [傅察](../Page/傅察.md "wikilink")、[劉韐](../Page/劉韐.md "wikilink")、[程振](../Page/程振.md "wikilink")、[李若水](../Page/李若水.md "wikilink")
  - [歐陽珣](../Page/歐陽珣.md "wikilink")、[宇文虛中](../Page/宇文虛中.md "wikilink")
  - [洪皓](../Page/洪皓.md "wikilink")、[張邵](../Page/張邵.md "wikilink")、[朱弁](../Page/朱弁.md "wikilink")
  - [張叔夜](../Page/張叔夜.md "wikilink")、[鄭驤](../Page/鄭驤.md "wikilink")、[張克戩](../Page/張克戩.md "wikilink")、[向子韶](../Page/向子韶.md "wikilink")
  - [孫昭遠](../Page/孫昭遠.md "wikilink")、[郭永](../Page/郭永.md "wikilink")、[楊邦乂](../Page/楊邦乂.md "wikilink")
  - [呂祉](../Page/呂祉.md "wikilink")

### 別集

  - [李邴](../Page/李邴.md "wikilink")、[權邦彥](../Page/權邦彥.md "wikilink")、[張守](../Page/張守.md "wikilink")
  - [陳康伯](../Page/陳康伯.md "wikilink")、[范宗尹](../Page/范宗尹.md "wikilink")、[朱倬](../Page/朱倬.md "wikilink")
  - [張燾](../Page/張燾.md "wikilink")、[鄭瑴](../Page/鄭瑴.md "wikilink")、[滕康](../Page/滕康.md "wikilink")、[王庶](../Page/王庶.md "wikilink")、[沈與永](../Page/沈與永.md "wikilink")、[汪徹](../Page/汪徹.md "wikilink")、[周麟之](../Page/周麟之.md "wikilink")
  - [葉夢得](../Page/葉夢得.md "wikilink")、[程瑀](../Page/程瑀.md "wikilink")、[王大寶](../Page/王大寶.md "wikilink")
  - [廖剛](../Page/廖剛.md "wikilink")、[胡舜陟](../Page/胡舜陟.md "wikilink")、[衛膚敏](../Page/衛膚敏.md "wikilink")、[陳公輔](../Page/陳公輔.md "wikilink")、[陳戩](../Page/陳戩.md "wikilink")
  - [張闡](../Page/張闡.md "wikilink")、[王縉](../Page/王縉.md "wikilink")、[杜莘老](../Page/杜莘老.md "wikilink")、[黃龜年](../Page/黃龜年.md "wikilink")、[辛次膺](../Page/辛次膺.md "wikilink")
  - [汪藻](../Page/汪藻.md "wikilink")、[綦崇禮](../Page/綦崇禮.md "wikilink")、[呂本中](../Page/呂本中.md "wikilink")
  - [王居正](../Page/王居正.md "wikilink")、[胡寅](../Page/胡寅.md "wikilink")、[潘良貴](../Page/潘良貴.md "wikilink")
  - [吴玠](../Page/吴玠.md "wikilink")、[吳璘](../Page/吳璘.md "wikilink")
  - [周葵](../Page/周葵.md "wikilink")、[王庭珪](../Page/王庭珪.md "wikilink")、[范如圭](../Page/范如圭.md "wikilink")、[翁蒙之](../Page/翁蒙之.md "wikilink")
  - [向子諲](../Page/向子諲.md "wikilink")、[向子忞](../Page/向子忞.md "wikilink")、[陳規](../Page/陳規.md "wikilink")
  - [趙密](../Page/趙密.md "wikilink")、[王德](../Page/王德.md "wikilink")、[張子蓋](../Page/張子蓋.md "wikilink")、[李寶](../Page/李寶.md "wikilink")
  - [李彥仙](../Page/李彥仙.md "wikilink")、[趙立](../Page/趙立.md "wikilink")、[魏勝](../Page/魏勝.md "wikilink")
  - [李綱](../Page/李綱.md "wikilink")
  - [呂頤浩](../Page/呂頤浩.md "wikilink")、[朱勝非](../Page/朱勝非.md "wikilink")
  - [張浚](../Page/張浚.md "wikilink")
  - [趙鼎](../Page/趙鼎.md "wikilink")
  - [宗澤](../Page/宗澤.md "wikilink")
  - [楊沂中](../Page/楊沂中.md "wikilink")、[韓世忠](../Page/韓世忠.md "wikilink")
  - [劉光世](../Page/劉光世.md "wikilink")、[張俊](../Page/張俊.md "wikilink")
  - [岳飛](../Page/岳飛.md "wikilink")
  - [張九成](../Page/張九成.md "wikilink")、[晏敦復](../Page/晏敦復.md "wikilink")
  - [劉錡](../Page/劉錡.md "wikilink")
  - [李顯宗](../Page/李顯宗.md "wikilink")
  - [劉子羽](../Page/劉子羽.md "wikilink")
  - [胡銓](../Page/胡銓.md "wikilink")

### 外集

  - [周敦頤](../Page/周敦頤.md "wikilink")
  - [程顥](../Page/程顥.md "wikilink")
  - [程頤](../Page/程頤.md "wikilink")
  - [張載](../Page/張載.md "wikilink")、[張戩](../Page/張戩.md "wikilink")
  - [邵雍](../Page/邵雍.md "wikilink")
  - [呂希哲](../Page/呂希哲.md "wikilink")、[朱光庭](../Page/朱光庭.md "wikilink")、[劉絢](../Page/劉絢.md "wikilink")、[李籲](../Page/李籲.md "wikilink")、[呂大鈞](../Page/呂大鈞.md "wikilink")、[呂大臨](../Page/呂大臨.md "wikilink")、[呂大忠](../Page/呂大忠.md "wikilink")、[蘇昞](../Page/蘇昞.md "wikilink")
  - [謝良佐](../Page/謝良佐.md "wikilink")、[游酢](../Page/游酢.md "wikilink")
  - [楊時](../Page/楊時.md "wikilink")、[劉安節](../Page/劉安節.md "wikilink")
  - [尹焞](../Page/尹焞.md "wikilink")、[張繹](../Page/張繹.md "wikilink")、[馬伸](../Page/馬伸.md "wikilink")、[孟厚](../Page/孟厚.md "wikilink")、[侯仲良](../Page/侯仲良.md "wikilink")、[周行己](../Page/周行己.md "wikilink")、[王蘋](../Page/王蘋.md "wikilink")、[李郁](../Page/李郁.md "wikilink")
  - [胡安國](../Page/胡安國.md "wikilink")
  - [胡宏](../Page/胡宏_\(理学家\).md "wikilink")、[胡憲](../Page/胡憲.md "wikilink")、[劉子翬](../Page/劉子翬.md "wikilink")、[劉勉之](../Page/劉勉之.md "wikilink")、[李侗](../Page/李侗.md "wikilink")、[朱松](../Page/朱松_\(宋朝\).md "wikilink")
  - [朱熹](../Page/朱熹.md "wikilink")、[張拭](../Page/張拭.md "wikilink")
  - [呂祖謙](../Page/呂祖謙.md "wikilink")
  - [魏挺之](../Page/魏挺之.md "wikilink")、[劉清之](../Page/劉清之.md "wikilink")
  - [陸九齡](../Page/陸九齡.md "wikilink")、[陸九淵](../Page/陸九淵.md "wikilink")
  - [陳亮](../Page/陳亮.md "wikilink")
  - [蔡元定](../Page/蔡元定.md "wikilink")、[蔡沈](../Page/蔡沈.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [《宋名臣言行錄》四庫全書影印本](http://ctext.org/library.pl?if=gb&res=837) -
    [中國哲學書電子化計劃](../Page/中國哲學書電子化計劃.md "wikilink")

[Category:宋朝史书](../Category/宋朝史书.md "wikilink")
[Category:史部傳記類](../Category/史部傳記類.md "wikilink")
[Category:南宋典籍](../Category/南宋典籍.md "wikilink")

1.  據清王懋竑撰《朱子年譜》卷二，當時朱熹已43歲。但由於整理匆促，體例稱不上完善，故呂祖謙見到該書後，來信指出一些缺失，並詢問朱熹是否有討論的意願。後來兩人相見，進一步討論細節後，最後涉及呂祖謙先人事跡的真偽問題，導致兩人之間發生不愉快。
2.  參照鄭騫《朱熹八朝名臣言行錄的原本與刪節本》。
3.  《四庫全書總目》提要云：「蓋仿朱子《名臣言行錄》例，而始末較詳。又兼仿杜大珪《名臣碑傳琬琰集》例，但有所棄取，不盡錄全篇耳。」
4.  《四庫全書總目》提要云：「是編仿朱子《五朝名臣言行錄》之例，編次有明一代儒者。各徵引諸書述其行事，亦間摘其語錄附之。」
5.  《四部叢刊初編書錄》提要云：「世行《名臣言行錄》皆與李幼武續錄並為一書，陳均《編年備要》引用書名即然；是朱子單行之本，宋季已罕傳矣。惟《直齋書錄》載《八朝名臣言行錄》二十四卷，為著錄家所僅見。取校洪瑩仿宋刊本，方知刪削甚多，則此洵為朱子原書也。」
6.  清．紀昀總纂《欽定四庫全書史部七．傳記類三．宋名臣言行錄》提要：「乃編中所錄，如趙普之陰險、王安石之堅僻、呂惠卿之奸詐、與韓范諸人並列，莫詳其旨。」
7.  四庫提要的說法有誤，書中是有提及呂惠卿的一些事迹，但並沒有為他獨立出一篇，故不算與韓范諸人並列。韓應指韓琦，范應指范仲淹。
8.  參照王德毅《朱熹五朝、三朝名臣言行錄的史料價值》
9.  參照李偉國：《朱熹〈名臣言行錄〉八百年歷史公案》
10. 《四庫全書總目》：「《晦菴集》中亦有《與祖謙書》曰：『《名臣言行錄》一書，亦當時草草為之。其間自知尚多謬誤，編次亦無法，初不成文字。因看得為訂正，示及為幸』」
11. 《朱子語類．卷一百三十．本朝四》：「涑水記聞，呂家子弟力辨，以為非溫公書。蓋其中有記呂文靖公數事，如殺郭后等。某嘗見范太史之孫某說，親收得溫公手寫藁本，安得為非溫公書！某編《八朝言行錄》，呂伯恭兄弟亦來辨。為子孫者只得分雪，然必欲天下之人從己，則不能也。」