**宋善美**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")。以1996年[SBS](../Page/SBS.md "wikilink")
Super Elite
Model第二名出道。2006年6月29日，與電影美術導演[高佑石舉行結婚典禮](../Page/高佑石.md "wikilink")，至2015年4月生下女兒。丈夫因幫外公追討被表哥一家非法移轉的財產，在2017年8月21日早上遭買兇殺害，送醫不治身故。

## 演出作品

### 電視劇

  - 1997年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《天橋風雲》
  - 1999年：SBS《[順風婦產科](../Page/順風婦產科.md "wikilink")》
  - 1999年：[KBS](../Page/韓國放送公社.md "wikilink")《魔法之城》
  - 2002年：KBS《[敢愛](../Page/敢愛.md "wikilink")》
  - 2004年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[玫瑰戰爭](../Page/玫瑰戰爭_\(電視劇\).md "wikilink")》
  - 2004年：KBS《[說不出的愛](../Page/說不出的愛.md "wikilink")》飾演 宋雅莉
  - 2005年：MBC《[秘密男女](../Page/秘密男女.md "wikilink")》飾演 鄭雅美
  - 2006年：SBS《[突然有一天](../Page/突然有一天.md "wikilink")》
  - 2006年：MBC《[白色巨塔](../Page/白色巨塔_\(韓國電視劇\).md "wikilink")》飾演 李允珍
  - 2007年：KBS《[媳婦的全盛時代](../Page/媳婦的全盛時代.md "wikilink")》飾演 車秀賢
  - 2009年：SBS《[綠色馬車](../Page/綠色馬車.md "wikilink")》飾演 韓智苑
  - 2009年：[tvN](../Page/tvN.md "wikilink")《[丈夫死了](../Page/丈夫死了.md "wikilink")》飾演
    吳多靜
  - 2010年：MBC《[蒲公英家族](../Page/蒲公英家族.md "wikilink")》飾演 朴智媛
  - 2010年：MBC《[個人取向](../Page/個人取向.md "wikilink")》飾演 新娘（客串）
  - 2010年：SBS《[人生多美麗](../Page/人生多美麗.md "wikilink")》飾演 宋那妍（客串）
  - 2011年：KBS《[烏鵲橋兄弟](../Page/烏鵲橋兄弟.md "wikilink")》飾演 南-{于}-蔚
  - 2012年：MBC《[黃金時刻](../Page/黃金時刻.md "wikilink")》飾演 申銀婭
  - 2012年：SBS《[清潭洞愛麗絲](../Page/清潭洞愛麗絲.md "wikilink")》（客串）
  - 2013年：[JTBC](../Page/JTBC.md "wikilink")《[宮中殘酷史－花的戰爭](../Page/宮中殘酷史－花的戰爭.md "wikilink")》飾演
    [愍懷嬪姜氏](../Page/愍懷嬪.md "wikilink")
  - 2013年：KBS《Drama Special－她們的完美一天》飾演 鄭守雅
  - 2013年：MBC《[韓國小姐](../Page/韓國小姐_\(電視劇\).md "wikilink")》飾演 高花靜
  - 2016年：tvN《[記憶](../Page/記憶_\(電視劇\).md "wikilink")》飾演 韓貞媛
  - 2017年：MBC《[歸來的福丹芝](../Page/歸來的福丹芝.md "wikilink")》 飾演 朴熙珍
  - 2017年：MBC《[守望者](../Page/守望者_\(電視劇\).md "wikilink")》飾演 蔡惠善

### 電影

  - 1998年：《[Art Museum By The
    Zoo](../Page/Art_Museum_By_The_Zoo.md "wikilink")》
  - 2001年：《[頭師父一體（野蠻師生）](../Page/頭師父一體.md "wikilink")》飾演 英文老師
  - 2002年：《[怪癖神偷](../Page/怪癖神偷.md "wikilink")》
  - 2003年：《[The Scent Of Love](../Page/The_Scent_Of_Love.md "wikilink")》
  - 2003年：《[Silver Knife](../Page/Silver_Knife.md "wikilink")》
  - 2004年：《[Mokpo The Harbor](../Page/Mokpo_The_Harbor.md "wikilink")》
  - 2004年：《[Liar](../Page/Liar.md "wikilink")》
  - 2006年：《[Woman On The
    Beach](../Page/Woman_On_The_Beach.md "wikilink")》
  - 2011年：《[北村方向](../Page/北村方向.md "wikilink")》
  - 2017年：《[獨自在海邊的夜晚](../Page/獨自在海邊的夜晚.md "wikilink")》

## 外部連結

  - [EPG](https://web.archive.org/web/20070830142609/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=673)


[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[S](../Category/忠清南道出身人物.md "wikilink")
[S](../Category/宋姓.md "wikilink")