**宋存壽**（），[台灣](../Page/台灣.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")，[江蘇省](../Page/江蘇省.md "wikilink")[江都縣人](../Page/江都縣.md "wikilink")。於1949年到達[香港](../Page/香港.md "wikilink")，就讀於[香港文化專科學校](../Page/香港文化專科學校.md "wikilink")，與[胡金銓及](../Page/胡金銓.md "wikilink")[李翰祥熟識](../Page/李翰祥.md "wikilink")。1955年，進入[四維影片公司工作](../Page/四維影片公司.md "wikilink")，擔任編劇，曾編寫由[羅維執導的](../Page/羅維.md "wikilink")《[多情河](../Page/多情河.md "wikilink")》電影劇本。1956年轉入[邵氏電影公司](../Page/邵氏.md "wikilink")，擔任編劇與副導演等職。1963年來[台](../Page/台灣.md "wikilink")，與[李翰祥合組](../Page/李翰祥.md "wikilink")[國聯電影公司](../Page/國聯電影公司.md "wikilink")。1966年首次執導第一部電影作品《[天之驕女](../Page/天之驕女.md "wikilink")》。而隔年的《[破曉時分](../Page/破曉時分.md "wikilink")》就受到極大的好評，有「文人導演」之譽\[1\]。他1972年與[趙瑛瑛結婚](../Page/趙瑛瑛.md "wikilink")，並於1971年與[郁正春和](../Page/郁正春.md "wikilink")[楊偉雄合組](../Page/楊偉雄.md "wikilink")[八十年代電影公司](../Page/八十年代電影公司.md "wikilink")，並持續執導電影。1982年的《[老師‧斯卡也答](../Page/老師‧斯卡也答.md "wikilink")》是他最後一部作品。

他的電影作品中，被公認最好的有《[破曉時分](../Page/破曉時分.md "wikilink")》、《[母親三十歲](../Page/母親三十歲.md "wikilink")》及《[窗外](../Page/窗外.md "wikilink")》，其中《[窗外](../Page/窗外.md "wikilink")》更是女演員[林青霞的第一部電影作品](../Page/林青霞.md "wikilink")\[2\]。其作品《[秋霞](../Page/秋霞.md "wikilink")》則是女歌手[陳秋霞的第一部電影作品](../Page/陳秋霞.md "wikilink")，也因此作品獲得[第14屆金馬獎的](../Page/金馬獎.md "wikilink")[最佳女主角獎](../Page/金馬獎獎項列表.md "wikilink")\[3\]。他在2001年3月於[法國](../Page/法國.md "wikilink")[杜維爾亞洲影展](../Page/杜維爾亞洲影展.md "wikilink")（Festival
du film asiatique de
Deauville）中獲得[金蓮獎](../Page/金蓮獎.md "wikilink")（終身成就獎），並於同年12月獲得[第38屆金馬獎的](../Page/金馬獎.md "wikilink")「[終身成就特別獎](../Page/金馬獎獎項列表.md "wikilink")」\[4\]。

宋存壽於2008年5月27日下午3時30分病逝於[台北](../Page/台北.md "wikilink")，享年78歲\[5\]。

## 導演作品

### 電影

  - 《[天之驕女](../Page/天之驕女.md "wikilink")》1966
  - 《[破曉時分](../Page/破曉時分.md "wikilink")》1968
  - 《[鐵娘子](../Page/鐵娘子.md "wikilink")》1969
  - 《[庭院深深](../Page/庭院深深.md "wikilink")》1971
  - 《[母親三十歲](../Page/母親三十歲.md "wikilink")》1972
  - 《[心蘭的故事](../Page/心蘭的故事.md "wikilink")》1972
  - 《[輕煙](../Page/輕煙.md "wikilink")》1972
  - 《[窗外](../Page/窗外.md "wikilink")》1973
  - 《[早熟](../Page/早熟.md "wikilink")》1974
  - 《[古鏡幽魂](../Page/古鏡幽魂.md "wikilink")》1974
  - 《[夜半怪談](../Page/夜半怪談.md "wikilink")》1974
  - 《[女記者](../Page/女記者.md "wikilink")》1974
  - 《[晨星](../Page/晨星.md "wikilink")》1975
  - 《[水雲](../Page/水雲.md "wikilink")》1975
  - 《[秋霞](../Page/秋霞.md "wikilink")》1976
  - 《[花非花](../Page/花非花.md "wikilink")》1978
  - 《[此情可問天](../Page/此情可問天_\(台灣電影\).md "wikilink")》1978
  - 《[留下一片相思](../Page/留下一片相思.md "wikilink")》（又名《[綠色山莊](../Page/綠色山莊.md "wikilink")》）1978
  - 《[鸚鵡傳奇](../Page/鸚鵡傳奇.md "wikilink")》1978
  - 《[第二道彩虹](../Page/第二道彩虹.md "wikilink")》1979
  - 《[落花流水春去也](../Page/落花流水春去也.md "wikilink")》1979
  - 《[我歌我泣](../Page/我歌我泣.md "wikilink")》1980
  - 《[候鳥之愛](../Page/候鳥之愛.md "wikilink")》1980
  - 《[走不完的路](../Page/走不完的路.md "wikilink")》（又名《[瀟瀟秋水寒](../Page/瀟瀟秋水寒.md "wikilink")》）1980
  - 《[翹家的女孩](../Page/翹家的女孩.md "wikilink")》1982
  - 《[老師‧斯卡也答](../Page/老師‧斯卡也答.md "wikilink")》1982

### 電視劇

  - 中視《[名星劇場](../Page/名星劇場.md "wikilink")——[銀女](../Page/銀女.md "wikilink")》1989（外景導演）\[6\]

## 編劇作品

  - 《[一夜風流](../Page/一夜風流.md "wikilink")》1958
  - 《[歌迷小姐](../Page/歌迷小姐.md "wikilink")》1959
  - 《[自由戀愛](../Page/自由戀愛.md "wikilink")》1960
  - 《[茶山情歌](../Page/茶山情歌.md "wikilink")》1960
  - 《[義犬遊俠](../Page/義犬遊俠.md "wikilink")》1963
  - 《[狀元及第](../Page/狀元及第.md "wikilink")》1964

## 參考文獻與注釋

## 外部連結

  - [宋存壽](http://movie.cca.gov.tw/People/Content.asp?ID=3) 於
    [台灣電影筆記](https://web.archive.org/web/20061119220636/http://movie.cca.gov.tw/)

  -
  - [宋存壽](http://www.dianying.com/ft/person/SongCunshou) 於
    [中文電影資料庫](http://www.dianying.com/ft/)

  -
  -
  -
  - [宋存壽](http://www.hkcinemagic.com/en/people.asp?id=3166)

[Category:金馬獎終身成就獎得主](../Category/金馬獎終身成就獎得主.md "wikilink")
[Category:中華民國電影導演](../Category/中華民國電影導演.md "wikilink")
[Category:江蘇裔台灣人](../Category/江蘇裔台灣人.md "wikilink")
[Category:揚州人](../Category/揚州人.md "wikilink")
[C存](../Category/宋姓.md "wikilink")

1.
2.
3.
4.  [台灣電影筆記](http://movie.cca.gov.tw/People/Content.asp?ID=3)
5.
6.  舞伶 文，-{沈}-鑫南
    攝影，〈「名星劇場」推-{出}-「銀女」深受好評！〉，《掃描線周刊》第174期（[中視文化公司](../Page/中視文化公司.md "wikilink")1989年5月5日-{出}-版）第20至21頁。