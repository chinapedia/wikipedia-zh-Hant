[Sung_Fei-ju_couple.jpg](https://zh.wikipedia.org/wiki/File:Sung_Fei-ju_couple.jpg "fig:Sung_Fei-ju_couple.jpg")合照\]\]

**宋斐如**（），原名**宋文瑞**，[台灣](../Page/台灣.md "wikilink")[臺南二層行塗庫](../Page/臺南.md "wikilink")（今[臺南市](../Page/臺南市.md "wikilink")[仁德區](../Page/仁德區.md "wikilink")）人，曾任[新聞記者](../Page/新聞記者.md "wikilink")、[臺灣省行政長官公署教育處副處長](../Page/臺灣省行政長官公署.md "wikilink")，在[二二八事件中遇害](../Page/二二八事件.md "wikilink")。

## 生平

[台北高等學校畢業後到大陆学习](../Page/台北高等學校.md "wikilink")，在[馬幼漁先生門下學習](../Page/馬幼漁.md "wikilink")，與[洪炎秋同屆考進](../Page/洪炎秋.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[預科](../Page/預科.md "wikilink")；之後進入北京大學經濟系就讀，改用名「斐如」。

1924年，宋斐如開始主編雜誌《少年臺灣》；北京大學畢業後，宋斐如曾在[馮玉祥處講學](../Page/馮玉祥.md "wikilink")。[中國抗日戰爭期間](../Page/中國抗日戰爭.md "wikilink")，宋斐如曾在[廣州](../Page/廣州.md "wikilink")[孫科旗下工作](../Page/孫科.md "wikilink")，后擔任[中蘇友好協會幹事](../Page/中蘇友好協會.md "wikilink")，1942年任[中國國民黨台灣省黨部](../Page/中國國民黨.md "wikilink")（由在[重慶的台籍黨員組成](../Page/重慶.md "wikilink")）幹訓班教育長。

1945年[日本投降](../Page/日本投降.md "wikilink")，宋斐如回到台灣，被任命為[台灣省行政長官公署教育處副處長](../Page/台灣省行政長官公署.md "wikilink")，是[二戰後初期長官公署高層官員中極少數的台灣人](../Page/二戰.md "wikilink")（台籍人士先暫安排文教新聞職缺，層級較高的另有3位：《[台灣新生報](../Page/台灣新生報.md "wikilink")》社長[李萬居](../Page/李萬居.md "wikilink")、[台灣廣播電台台長](../Page/台灣廣播電台.md "wikilink")[林忠](../Page/林忠.md "wikilink")、[參議](../Page/參議.md "wikilink")[楊友濂](../Page/楊友濂.md "wikilink")、[宋李林都是隨國府回台的台灣人](../Page/宋李林.md "wikilink")，[楊雲萍是好友](../Page/楊雲萍.md "wikilink")[許壽裳力保的和國府沒有淵源的留日台人](../Page/許壽裳.md "wikilink")）。

1945年12月，宋斐如與[鄭明祿](../Page/鄭明祿.md "wikilink")、[蘇新](../Page/蘇新.md "wikilink")（《[人民導報](../Page/人民導報.md "wikilink")》總編輯，活躍[台共分子](../Page/台共.md "wikilink")）等人創辦《人民導報》，宋斐如並擔任社長。1946年，《人民導報》刊登有關[國共和談敏感文章](../Page/國共和談.md "wikilink")、批評陳儀施政等，引發[台灣省行政長官](../Page/台灣省行政長官.md "wikilink")[陳儀不滿](../Page/陳儀.md "wikilink")、要求宋斐如辭去社長職。1947年2月22日，陳儀罷黜了宋斐如教育處副處長一職。二二八事件爆發期間，宋斐如並未從事武力抗爭，卻在3月9日國府軍入[台北市後名列當局](../Page/台北市.md "wikilink")「叛亂首要人犯」，成為首批被捕人士之一，最後被殺。

## 家屬

宋斐如之妻[區嚴華](../Page/區嚴華.md "wikilink")，到台灣後曾任職台灣省行政長官公署、[台灣省政府法制室](../Page/台灣省政府.md "wikilink")，1950年因幫助前《人民導報》總主筆陳文彬舉家逃往[香港而被捕](../Page/香港.md "wikilink")，次年被槍決於[馬場町](../Page/馬場町.md "wikilink")（今[台北市](../Page/台北市.md "wikilink")[萬華區](../Page/萬華區.md "wikilink")[青年公園一帶地區](../Page/青年公園.md "wikilink")）。

[Category:二二八事件受難者](../Category/二二八事件受難者.md "wikilink")
[Category:臺灣省行政長官公署人物](../Category/臺灣省行政長官公署人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中華民國編輯](../Category/中華民國編輯.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:半山](../Category/半山.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:仁德人](../Category/仁德人.md "wikilink")
[F斐](../Category/宋姓.md "wikilink")
[Category:被國民政府處決的台灣人](../Category/被國民政府處決的台灣人.md "wikilink")