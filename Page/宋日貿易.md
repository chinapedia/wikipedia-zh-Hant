**宋日貿易**，是指[宋朝](../Page/宋朝.md "wikilink")（[南宋為多](../Page/南宋.md "wikilink")）與日本兩國之間的所實行商義交易活動。宋日貿易也是日本自從[晚唐停止向中國派遣](../Page/晚唐.md "wikilink")[遣唐使以後重新開始的中日交流](../Page/遣唐使.md "wikilink")（西元十至十三世紀）。在同一個時期的發生的貿易活動也包括中國與[朝鮮半島之間的貿易活動與日本與](../Page/朝鮮半島.md "wikilink")[高麗](../Page/高麗.md "wikilink")（[王氏高麗](../Page/王氏高麗.md "wikilink")）間的[日朝貿易貿易活動等](../Page/日朝貿易.md "wikilink")。

宋日貿易主要的發生時間為日本的[平安時代中期至](../Page/平安時代.md "wikilink")[鎌倉時代中期](../Page/鎌倉時代.md "wikilink")。[越前國的](../Page/越前國.md "wikilink")[敦賀和擁有眾多中國人所居住的](../Page/敦賀.md "wikilink")[博多都是當時重要的貿易據點](../Page/博多.md "wikilink")。

## 歷史

宋朝成立後不久為振興貿易在各地設置[市舶司](../Page/市舶司.md "wikilink")，與[高麗](../Page/高麗.md "wikilink")、[日本](../Page/日本.md "wikilink")、[南洋和](../Page/南洋.md "wikilink")[南亞等發展貿易](../Page/南亞.md "wikilink")。日本則在[大宰府監督下進行貿易](../Page/大宰府.md "wikilink")，建有鴻臚館（接待、交易的機關）。但中日間並無正式的外交貿易，僅限於私人貿易，宋商多行至博多與越前敦賀作交易。

越前守[平忠盛得到](../Page/平忠盛.md "wikilink")[肥前國神崎莊的領地後獨自進行交易](../Page/肥前國.md "wikilink")，將外來的貨品進獻給院（執政的太上皇），獲得近臣的地位。[平氏政權成立後](../Page/平氏.md "wikilink")，平氏輸出其控制下的[伊勢出產的銀](../Page/伊勢.md "wikilink")。[平治之亂前的](../Page/平治之亂.md "wikilink")1158年，平氏首領[平清盛在博多築建首個人工港](../Page/平清盛.md "wikilink")，大力發展貿易，並驅逐寺院與神社等勢力，掌握了[瀨戶內海的航路](../Page/瀨戶內海.md "wikilink")。平家與皇族參拜[嚴島神社的航路也成了日宋間貿易的航路](../Page/嚴島神社.md "wikilink")。1173年，擴建[福原的外港大輪田泊](../Page/福原.md "wikilink")（今[神戶港](../Page/神戶港.md "wikilink")），三月，與宋朝建立正式國交，施行振興貿易的國策。事實上日本在此之前根本無「通貨」可以膨脹或緊縮。

平氏政權滅亡之後的鎌倉時代，中日之間無正式邦交，但[鎌倉幕府認可民間貿易](../Page/鎌倉幕府.md "wikilink")，甚至也向[博多派遣商船](../Page/博多.md "wikilink")。直至南宋末期，一直有貿易往來。[武士階層信仰的](../Page/武士.md "wikilink")[禪宗受掌權的北條得宗家保護](../Page/禪宗.md "wikilink")，民間的[僧人可乘貿易船渡日](../Page/僧人.md "wikilink")。

日本方面亦有渡來[宋朝](../Page/宋朝.md "wikilink")（始於[北宋](../Page/北宋.md "wikilink")）的商人、僧侶。中國方面主要據點為[明州](../Page/明州.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")[寧波](../Page/寧波.md "wikilink")）。

[南宋滅亡後](../Page/南宋.md "wikilink")[元日之間也有貿易往來](../Page/元朝.md "wikilink")，但相關史料匱乏，原因可能是中國商人難以居於日本。

## 貿易品

輸入日本的貨物有宋錢、陶瓷器、絹織品、書籍與文房四寶、香料與藥品、繪畫等美術品、工藝品等。日本輸出的貨物有銅、硫磺等礦物、西部所產的木材羅板、[日本刀等工藝品等](../Page/日本刀.md "wikilink")。輸入日本的宋錢對日本的貨幣使用起了推動作用，而佛經的輸入對鎌倉時期的佛教變革（淨土思想的普及與禪宗的傳入等）有一定影響。

## 參考文獻

  - [宋史](../Page/宋史.md "wikilink") 列傳第二百五十

## 參見

  - [明日貿易](../Page/明日貿易.md "wikilink")
  - [朝貢體系](../Page/朝貢體系.md "wikilink")
  - [遣隋使](../Page/遣隋使.md "wikilink")
  - [遣唐使](../Page/遣唐使.md "wikilink")
  - [遣明使](../Page/遣明使.md "wikilink")
  - [冊封](../Page/冊封.md "wikilink")

[Category:宋朝经济](../Category/宋朝经济.md "wikilink")
[Category:中日關係史](../Category/中日關係史.md "wikilink")
[Category:中国对外贸易史](../Category/中国对外贸易史.md "wikilink")
[Category:日本对外贸易史](../Category/日本对外贸易史.md "wikilink")
[Category:平安時代外交](../Category/平安時代外交.md "wikilink")
[Category:鎌倉時代外交](../Category/鎌倉時代外交.md "wikilink")