**宋洪武**（），[河南](../Page/河南.md "wikilink")[滑县人](../Page/滑县.md "wikilink")。1971年2月参加工作，1983年12月加入[中国共产党](../Page/中国共产党.md "wikilink")。[河南大学中文系毕业](../Page/河南大学.md "wikilink")，[中共中央党校](../Page/中共中央党校.md "wikilink")[在职研究生学历](../Page/在职研究生.md "wikilink")。

## 早年经历

宋洪武早年曾在[河南](../Page/河南.md "wikilink")[鹤壁市当过](../Page/鹤壁市.md "wikilink")[知青](../Page/知青.md "wikilink")、煤矿工人；1975年获推荐，进入[河南大学中文系学习](../Page/河南大学.md "wikilink")，成为“工农兵学员”。毕业后，曾长期在[共青团河南省委工作](../Page/共青团.md "wikilink")；曾历任研究室秘书、副主任，团省委常委、研究室副主任、办公室副主任、主任。

## 仕途

1987年，宋洪武转到[河南省地方任职](../Page/河南省.md "wikilink")；历任[栾川县副书记](../Page/栾川.md "wikilink")、纪委书记，[偃师县委书记](../Page/偃师.md "wikilink")（1993年县改市，任[偃师市](../Page/偃师市.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")）；1994年，任[中共河南省委](../Page/中共河南省委.md "wikilink")[周口地委委员](../Page/周口.md "wikilink")、组织部部长；1996年，升任地委副书记、[行政公署专员](../Page/行政公署.md "wikilink")。

1998年调[陕西省工作](../Page/陕西省.md "wikilink")，任[安康地委副书记](../Page/安康.md "wikilink")、行署专员；2000年地改市后，出任首任[安康市](../Page/安康市.md "wikilink")[市长](../Page/市长.md "wikilink")；2001年，升任[中共](../Page/中共.md "wikilink")[安康](../Page/安康.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")；2002年8月，转任中共[咸阳市委书记](../Page/咸阳.md "wikilink")；2004年4月起，兼任[咸阳市](../Page/咸阳市.md "wikilink")[人大常委会主任](../Page/人大常委会.md "wikilink")；2005年8月，升任[中共陕西省委常委](../Page/中共陕西省委.md "wikilink")、兼省政法委书记\[1\]；2006年10月，授[副总警监](../Page/副总警监.md "wikilink")[警衔](../Page/中国警阶#中华人民共和国.md "wikilink")\[2\]。2012年1月，转任[陕西省人大常委会副主任](../Page/陕西省人大常委会.md "wikilink")\[3\]；当年5月起，兼任省人大党组副书记；2013年1月，在省十二届人大一次会议上连任。2016年1月，辞去[陕西省人大常委会副主任职务](../Page/陕西省人大常委会.md "wikilink")\[4\]。

## 参考资料

## 外部链接

  - [宋洪武简历](http://district.ce.cn/newarea/sddy/201302/01/t20130201_24087577_1.shtml)
    中国经济网 2014-05-14

{{-}}
[category:滑县人](../Page/category:滑县人.md "wikilink")
[H洪](../Page/category:宋姓.md "wikilink")

[Category:第九届全国人大代表](../Category/第九届全国人大代表.md "wikilink")
[Category:副总警监](../Category/副总警监.md "wikilink")
[Category:河南大学校友](../Category/河南大学校友.md "wikilink")
[Category:工农兵学员](../Category/工农兵学员.md "wikilink")
[Category:中共陕西省委常委](../Category/中共陕西省委常委.md "wikilink")
[Category:中共陕西省委政法委书记](../Category/中共陕西省委政法委书记.md "wikilink")
[Category:陕西省人大常委会副主任](../Category/陕西省人大常委会副主任.md "wikilink")
[Category:中共安康市委书记](../Category/中共安康市委书记.md "wikilink")
[Category:中共咸阳市委书记](../Category/中共咸阳市委书记.md "wikilink")
[Category:中华人民共和国安康市市长](../Category/中华人民共和国安康市市长.md "wikilink")

1.  [《陕西日报》2005年8月31日新闻](http://www.sxdaily.com.cn/data/bsyw/20050831_8705808_12.htm)
2.  [陕西政法委书记获授副总警监警衔
    周永康颁证书](http://gov.ce.cn/zhongyang/zyzj/200610/30/t20061030_9190726.shtml)
3.  [宋洪武、胡悦当选陕西省人大常委会副主任](http://news.hsw.cn/system/2012/01/16/051217424.shtml)
4.