**宋璟**（），[邢州](../Page/邢州.md "wikilink")[南和人](../Page/南和.md "wikilink")，為[唐玄宗](../Page/唐玄宗.md "wikilink")[開元初期的著名宰相](../Page/開元.md "wikilink")。封廣平郡公，謚文貞。

## 生平

### 武后時期

宋璟在17歲時[進士及第](../Page/進士及第.md "wikilink")，可謂年少得志。武后執政後，宋璟因為率性剛正而被重用，逐步由[鳳閣舍人](../Page/鳳閣舍人.md "wikilink")（即中書舍人）升遷至[御史中丞](../Page/御史中丞.md "wikilink")。武則天的寵臣[張昌宗因為私自向相士詢問運程](../Page/張昌宗.md "wikilink")，違反了宮規，宋璟奏請追究，但是武則天則特旨赦免，並命令張昌宗及[張易之兩兄弟到他的住所謝罪](../Page/張易之.md "wikilink")，宋璟卻拒而不見。宋璟因此與他們結怨，二張屢次欲借故中傷他，但卻不成功。

### 中宗、睿宗時期

李唐復興後，在[中宗時](../Page/唐中宗.md "wikilink")，宋璟擔任[黃門侍郎](../Page/黃門侍郎.md "wikilink")。當時宋璟得罪了當權的[武三思](../Page/武三思.md "wikilink")，因此受到排擠，被外調為[貝州](../Page/貝州.md "wikilink")[刺史](../Page/刺史.md "wikilink")。其後因為[韋-{后}-叛亂](../Page/韋后.md "wikilink")，中宗被殺，[睿宗子](../Page/唐睿宗.md "wikilink")[李隆基平叛](../Page/李隆基.md "wikilink")，
睿宗繼位。宋璟重新被起用，睿宗將他由[洛州長史調為](../Page/洛州.md "wikilink")[吏部尚書](../Page/吏部尚書.md "wikilink")、[同中書門下三品](../Page/同中書門下三品.md "wikilink")，執掌朝政，這是他首度為相。在這期間，他一改朝廷用人惟親的惡習，提出了用人“雖資高考深，非才者不取”的準則，並且不顧當時擁有極大權勢的[太平公主等人的反對及阻饒](../Page/太平公主.md "wikilink")，罷去昏庸的官員達數千人，並請太平公主出居東都[洛陽](../Page/洛陽.md "wikilink")，以防太平公主謀反，但因此得罪了太平公主，被其中傷，因此反而被罷相，貶為[楚州刺史](../Page/楚州.md "wikilink")。

### 玄宗開元時期

其後[太子李隆基討平太平公主的叛亂](../Page/太子.md "wikilink")，即位為唐玄宗，宋璟被調為[廣州](../Page/廣州.md "wikilink")[都督](../Page/都督.md "wikilink")。這時宋璟仍專注改善民生，教導百姓以磚瓦蓋屋取代簡陋的茅屋及草屋，以減少火災出現的可能\[1\]。716年，他被調返[京師](../Page/長安.md "wikilink")，任[刑部尚書](../Page/刑部尚書.md "wikilink")，後來[姚崇因事退隱](../Page/姚崇.md "wikilink")，他獲姚崇推薦，再度為相。這時，宋璟再度提出“雖資高考深，非才者不取”的準則。他為防針奸佞小人私下在皇帝耳邊進讒言，提出百官奏事，必定要有[諫官](../Page/諫官.md "wikilink")、[史官在旁的規定](../Page/史官.md "wikilink")。而玄宗亦十分器重宋璟，並以對待老師的禮節對待他，因此宋璟提出的具建設性的提議，通常均被採納。亦因如此，當時朝廷一改以往用人惟親的惡習，並減少了奸佞小人誣諂好人的情況，使開元初期的政局十分清明。宋璟前後為相四年，在720年，他因為壓制犯法官員的申訴，並嚴禁黑錢的流通，得罪了不少權貴，因此被降為[開府儀同三司](../Page/開府儀同三司.md "wikilink")，再度罷相。729年，再度拜為[尚書右丞相](../Page/尚書右丞相.md "wikilink")。733年，以年老為由退休，“仍令全给禄奉”，隱居洛陽。开元二十五年（737年）十一月十九日去世於东都明教里私第，享年74歲。

## 子孙

### 子

1.  [宋復](../Page/宋復.md "wikilink")，[同州司功參軍](../Page/同州.md "wikilink")
2.  [宋昇](../Page/宋昇.md "wikilink")，太僕少卿
3.  [宋尚](../Page/宋尚.md "wikilink")，漢東太守。生[宋寔](../Page/宋寔.md "wikilink")
4.  [宋渾](../Page/宋渾.md "wikilink")，太子左諭德
5.  [宋恕](../Page/宋恕.md "wikilink")，都官郎中。生[宋袞](../Page/宋袞.md "wikilink")，太常丞
6.  [宋延](../Page/宋延.md "wikilink")，[太原少尹](../Page/太原.md "wikilink")
7.  [宋華](../Page/宋華.md "wikilink")，尉氏令。生[宋儼](../Page/宋儼.md "wikilink")，[蘇州刺史](../Page/蘇州.md "wikilink")；[宋佶](../Page/宋佶.md "wikilink")，河南尉；[宋倚](../Page/宋倚.md "wikilink")，[虢州長史](../Page/虢州.md "wikilink")
8.  [宋衡](../Page/宋衡.md "wikilink")，河西節度行軍司馬、檢校左散騎常侍。生[宋倓](../Page/宋倓.md "wikilink")

### 曾孙

  - [宋渤](../Page/宋渤.md "wikilink")。曾孫[宋堅](../Page/宋堅.md "wikilink")，[太樂令](../Page/太樂令.md "wikilink")

### 七世孙

  - [宋處秀](../Page/宋處秀.md "wikilink")，大理正

## 宋璟碑

天寶八年（749年），宋璟第四子御史中丞[宋渾請求](../Page/宋渾.md "wikilink")[顏真卿為其父立碑](../Page/顏真卿.md "wikilink")。不久，御史[吉溫出於私怨陷害宋渾](../Page/吉溫.md "wikilink")，謫賀州，建碑之事「緣此中止」。大历七年（772年）九月二十五日，宋璟之孙苏州刺史[宋俨为](../Page/宋俨.md "wikilink")“追念祖父德业”建宋璟碑，立于宋璟墓前。由颜真卿撰写碑文，邢州刺史[封演](../Page/封演.md "wikilink")“购他山之石，曳以百牛，潺刻字之工”。不久颜真卿認為前文疏漏，又重新撰文，打算重新补刻，卻因宋璟八子[宋衡被吐蕃所俘而作罷](../Page/宋衡.md "wikilink")，大历十二年（777年）十一月，唐蕃二國和好，宋衡被遣返。大历十三年（778年）春，重新补刻于碑之左侧。

## 評價

史書評宋璟「璟善守文以持正」，又評他「勸諫上皇言語切」，可見宋璟是一位敢於犯顏直諫的賢相。後人亦有「前稱[房](../Page/房玄齡.md "wikilink")、[杜](../Page/杜如晦.md "wikilink")，後稱[姚](../Page/姚崇.md "wikilink")、宋」之說。因為他與姚崇合力開創了開元盛世，因此被世人合稱為“姚宋”。

## 成語關聯

[五代](../Page/五代.md "wikilink")[王仁裕著](../Page/王仁裕.md "wikilink")《[開元天寶遺事](../Page/開元天寶遺事.md "wikilink")‧有腳陽春》說宋璟體恤民情，朝野一致讚美。時人稱宋璟為“有腳陽春”，因其所到之處，如春天之萬物，受到和煦的陽光照耀，充滿生機。

## 注釋

## 相关链接

  - [國學網站 — 成語大全(下) 之 1](http://www.guoxue.com/chengyu/cyx_001.htm)
  - [成語故事 有腳陽春](http://www.epochtimes.com/b5/4/2/5/n461232.htm)
  - [唐代名相宋璟与南屿江口村](https://web.archive.org/web/20060601194518/http://fz.fj-archives.org.cn/m_listData.asp?CatalogCode=1057&FlowId=23497)

[J](../Category/广平宋氏.md "wikilink")
[Category:南和县人](../Category/南和县人.md "wikilink")
[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")
[Category:唐朝進士](../Category/唐朝進士.md "wikilink")
[Category:唐朝诗人](../Category/唐朝诗人.md "wikilink")
[Category:谥文贞](../Category/谥文贞.md "wikilink")
[Category:唐朝楚州刺史](../Category/唐朝楚州刺史.md "wikilink")
[Category:唐朝贝州刺史](../Category/唐朝贝州刺史.md "wikilink")
[Category:唐朝刑部尚书](../Category/唐朝刑部尚书.md "wikilink")
[Category:唐朝吏部尚书](../Category/唐朝吏部尚书.md "wikilink")
[Category:同中书门下三品](../Category/同中书门下三品.md "wikilink")
[Category:唐朝尚书右丞](../Category/唐朝尚书右丞.md "wikilink")
[Category:唐朝开府仪同三司](../Category/唐朝开府仪同三司.md "wikilink")
[Category:唐朝黄门侍郎](../Category/唐朝黄门侍郎.md "wikilink")
[Category:唐朝御史中丞](../Category/唐朝御史中丞.md "wikilink")

1.  《新唐書》卷一二六《盧奂傳》載：“天寶初，爲南海太守。南海兼水陸都會，物産環怪。前守劉巨麟、彭果皆以贜敗，故以奂代之。污吏斂手，中人之市舶者亦不敢干其法，遠俗爲安。時謂自開元後四十年，治廣有清節者，宋璟、李朝隱、奂三人而已。”