在[交換代數中](../Page/交換代數.md "wikilink")，可以探討一個交換環 \(R\) 本身，或一個
\(R\)-模對一[理想](../Page/理想_\(環論\).md "wikilink") \(I \subset R\)
的完備性。由於完備環有較容易處理的性質，**完備化**是研究[交換環的基本工具](../Page/交換環.md "wikilink")。

幾何上，交換環的完備化對應到一個閉子[概形的](../Page/概形.md "wikilink")[形式鄰域](../Page/形式事概形.md "wikilink")。

## *I*-進拓撲

對於一個交換環 \(R\) 及其理想 \(I\)（通常取為[極大理想](../Page/極大理想.md "wikilink")），可以藉著取
\(I^n \; (n  \in \N)\) 為零元素的開鄰域，賦予 \(R\)
相應的拓撲結構，使之成為對加法的[拓撲群](../Page/拓撲群.md "wikilink")。這種拓撲稱為
**\(I\)-進拓撲**。

對於一個 \(R\)-模 \(M\)，同樣可考慮零元素的開鄰域 \(I^n M\)，由此得到 \(M\) 上的 \(I\)-進拓撲。

## 完備化及其性質

模 \(M\) 對 \(I \subset R\)
的**完備化**定義為[射影極限](../Page/極限_\(範疇論\).md "wikilink")：

  -
    \(\hat{M} := \varprojlim_n M/I^n M\)

正如其名，\(\hat{M}\) 對其 \(I\)-進拓撲是[完備的](../Page/完備性.md "wikilink")。對於固定的
\(I \subset R\)，\(M \mapsto \hat{M}\) 是從 \(R\)-模範疇（態射為模同態）到 \(I\)-進拓撲
\(R\)-模（態射為連續同態）的[函子](../Page/函子.md "wikilink")；透過自然同態
\(M \to \hat{M}\)，它是與之反向的遺忘函子的左[伴隨函子](../Page/伴隨函子.md "wikilink")，因而是[右正合的](../Page/正合函子.md "wikilink")。

對於[諾特環](../Page/諾特環.md "wikilink")，\(\hat{R}\)
是[平坦的](../Page/平坦模.md "wikilink") \(R\)-模。此時，對任何有限生成
\(R\)-模 \(M\)，自然態射 \(\hat{M} \to M \otimes_R \hat{R}\) 是個同構。綜上所述，對於諾特環
\(R\)上的有限生成 \(R\)-模，完備化是個[正合函子](../Page/正合函子.md "wikilink")。

此外，完備化也可以用[柯西序列構造](../Page/柯西序列.md "wikilink")，得到的對象是自然同構的。

## 例子

  - [p進整數是](../Page/p進數.md "wikilink") \(\Z\) 對 \(p\Z\) 的完備化。
  - 形式冪級數環 \(k[[X_1,_/ldots,_X_n|X_1, \ldots, X_n]]\) 是多項式環
    \(k[X_1, \ldots, X_n]\) 對 \((X_1, \ldots, X_n)\) 的完備化。

## 文獻

  - David Eisenbud, *Commutative algebra. With a view toward algebraic
    geometry*. Graduate Texts in Mathematics, 150. Springer-Verlag, New
    York, 1995. xvi+785 pp. ISBN 0-387-94268-8; ISBN 0-387-94269-6

[U](../Category/交換代數.md "wikilink") [U](../Category/模論.md "wikilink")
[Category:环论](../Category/环论.md "wikilink")