在[数学中](../Page/数学.md "wikilink")，**完全格**是在其中所有子集都有[上确界](../Page/上确界.md "wikilink")（并）和[下确界](../Page/下确界.md "wikilink")（交）的[偏序集](../Page/偏序集.md "wikilink")。完全格出现于数学和[计算机科学的很多应用中](../Page/计算机科学.md "wikilink")。作为[格的特殊实例](../Page/格_\(数学\).md "wikilink")，在[序理论和](../Page/序理论.md "wikilink")[泛代数中都有所研究](../Page/泛代数.md "wikilink")。

完全格一定不能混淆于[完全偏序](../Page/完全偏序.md "wikilink")（*cpo*），它构成严格的更加一般的一个偏序集合类别。更特殊的完全格是[完全布尔代数和](../Page/完全布尔代数.md "wikilink")[完全Heyting代数](../Page/完全Heyting代数.md "wikilink")（*locale*）。

## 形式定义

[偏序集合](../Page/偏序集合.md "wikilink")(*L*,
≤)是完全格，如果*L*的所有[子集](../Page/子集.md "wikilink")*A*在(*L*,
≤)中都有[最大下界](../Page/最大下界.md "wikilink")（下确界，交）和[最小上界](../Page/最小上界.md "wikilink")（上确界，并）二者。它们被表示为：

  -
    \(\bigwedge\)*A*（交）和\(\bigvee\)*A*（并）。

注意在*A*是[空集的特殊情况下](../Page/空集.md "wikilink")，L的任何元素都是空集的上界和下界，*A*的交将是*L*的[最大元素](../Page/最大元素.md "wikilink")。类似的，空集的并生成[最小元素](../Page/最小元素.md "wikilink")。因为定义还确保了二元交和并的存在，完全格因为形成了特殊种类的[有界格](../Page/有界格.md "wikilink")。

上述定义的更多蕴涵在关于序理论中[完备性性质的文章中讨论](../Page/完备性_\(序理论\).md "wikilink")。

## 例子

  - 给定集合的[幂集](../Page/幂集.md "wikilink")，按[包含排序](../Page/子集.md "wikilink")。上确界给出自这些子集的[并集而下确界给出自这些子集的](../Page/并集.md "wikilink")[交集](../Page/交集.md "wikilink")。
  - [单位区间](../Page/区间.md "wikilink")\[0,1\]和[扩展的实数轴](../Page/扩展的实数轴.md "wikilink")，通过平常的全序和普通的[上确界和](../Page/上确界.md "wikilink")[下确界](../Page/下确界.md "wikilink")。实际上，全序集合（带有它的[序拓扑](../Page/序拓扑.md "wikilink")）作为[拓扑空间是](../Page/拓扑空间.md "wikilink")[紧致的](../Page/紧致空间.md "wikilink")，如果它作为一个格是完全的。
  - 非负[整数按](../Page/整数.md "wikilink")[整除排序](../Page/整除.md "wikilink")。这个格最小元是1，因为它可以被任何其他数整除。可能令人惊奇的是，最大元是0，因为它可以被任何数整除。有限集合的上确界给出自[最小公倍數而下确界给出自](../Page/最小公倍數.md "wikilink")[最大公约数](../Page/最大公约数.md "wikilink")。对于无限集合，上确界将总是0而下确界可以大于1。例如，所有偶数的集合有2作为最大公约数。如果从这个结构中去掉0它仍是格但不再是完全的。
  - 任何给定群的子群在包含关系下。（尽管这里的[下确界是平常的集合论交集](../Page/下确界.md "wikilink")，但子群的集合的[上确界是子群的集合论并集所生成的子群](../Page/上确界.md "wikilink")，而不是集合论并集自身）。如果*e*是*G*的单位元，则平凡的群{*e*}是*G*的[极小子群](../Page/极小元.md "wikilink")。而[极大子群是群](../Page/极大元.md "wikilink")*G*自身。
  - [模的子模按包含排序](../Page/模.md "wikilink")。上确界给出自子模的和而下确界给出自交集。
  - 环的[理想子环按包含排序](../Page/理想子环.md "wikilink")。上确界给出自理想子环的和而下确界给出自交集。
  - [拓扑空间的开集按包含排序](../Page/拓扑空间.md "wikilink")。上确界给出自开集的并而下确界给出自交集的[内部](../Page/内部_\(数学\).md "wikilink")。
  - [实数或](../Page/实数.md "wikilink")[复数的](../Page/复数.md "wikilink")[向量空间的](../Page/向量空间.md "wikilink")[凸集按包含排序](../Page/凸集.md "wikilink")。下确界给出自凸集的交集而上确界给出自并集的[凸包](../Page/凸包.md "wikilink")。
  - 在集合上[拓扑按包含排序](../Page/拓扑空间.md "wikilink")。下确界给出自拓扑的交集，而上确界给出自拓扑的并集所生成的拓扑。
  - 在集合上的所有[传递关系的格](../Page/传递关系.md "wikilink")。
  - [多重集的子多重集的格](../Page/多重集.md "wikilink")。
  - 在集合上的所有[等价关系的格](../Page/等价关系.md "wikilink")；等价关系\~被认为比≈更小（或"更细"），如果*x*\~*y*总是蕴涵*x*≈*y*。

## 参见

  - [完全布尔代数](../Page/完全布尔代数.md "wikilink")
  - [完全Heyting代数](../Page/完全Heyting代数.md "wikilink")

[U](../Category/格理论.md "wikilink")