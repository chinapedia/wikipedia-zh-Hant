**宏碁股份有限公司**（英語：Acer
Incorporated，簡稱：**宏碁**，縮寫：）是源自[臺灣的](../Page/臺灣.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[科技公司](../Page/科技公司.md "wikilink")，成立於1976年，其產品包括[個人電腦](../Page/個人電腦.md "wikilink")、[電競產品和虛擬實境裝置等](../Page/电子竞技.md "wikilink")。根據IDC
2018年第三季度的統計，宏碁是全球第4大電腦製造商，亦為[Windows系統個人電腦品牌中的第](../Page/Microsoft_Windows.md "wikilink")4大製造商。\[1\]2018台灣品牌價值4.06億美金，名列第9。

## 名稱

宏碁讀作「ㄐㄧ」（jī），碁原為棋的異體字，本有讀音「ㄑㄧˊ」（qí）和「ㄐㄧ」（jī）兩者，由於此企業表示“碁”是引申為根基、根底堅固之意，故“碁”此處讀作ㄐㄧ（jī）

創立時，使用「Multitech」品牌十餘年之久。1987年，宏碁集團董事長[施振榮獨排眾議](../Page/施振榮.md "wikilink")，認為企業在長期發展中為了不同的使命與任務，應該修改不合時宜的名字，而決定放棄「Multitech」這個價值兩千萬美元的品牌。施振榮發現「Multitech」這個名字字母太長很難記，而且容易與其他高科技公司產生重複商標的情形，於是決定重新設計新的品牌，誓言把這個品牌塑造為世界知名品牌。第一次變更中，品牌改為「AceR」，商標左半部是一個「箭」的標誌，代表衝力與速度，右半部是一顆閃亮的鑽石，代表堅實與價值，這個商標使用了13年。

為了擺脫電腦硬體硬梆梆的形象，宏碁電腦使用13年之久的企業識別系統在2001年3月8日正式改為「acer」，其中英文字母大寫的A改為英文小寫的「a」，「e」以流線型圖像字體塑造宏碁在網路時代的全新形象。因為「AceR」商標給外界的印象多是有稜有角的硬體，為了符合網路時代全新形象，於是決定改為流線型圖像字體，再創網路時代新王國。

2011年4月11日，宏碁集團發表新聞稿，再次更新企業識別商標，將原來「acer」商標的線條做了局部修正，並採用青綠色字體。

這個名字是當年運用電腦從四萬多個名字中篩選，源於拉丁字，代表鮮明的、活潑的、有洞察力的、敏銳的與有活力的，「Acer」源於「Ace」的語根，代表著極優秀的人物，這些特質都是宏碁所追求的，而且在一般的字母排序名列前茅，增加品牌能見度。

## 歷史

[Acer_Building_in_Oriental_Science_Park_entrance_20160305.jpg](https://zh.wikipedia.org/wiki/File:Acer_Building_in_Oriental_Science_Park_entrance_20160305.jpg "fig:Acer_Building_in_Oriental_Science_Park_entrance_20160305.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Acer_Liquid_E600.png "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Acer_Iconia_W5.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Acer.aspire-522.amd-fusion.ubuntu_1c555_7117.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:2008Computex_Acer_Aspire_G_Predator.jpg "fig:缩略图")

  - 1976年，宏碁電腦公司在台北市成立，成立初期登記資本額為新台幣100萬元，當時以"微處理機的園丁"自許，從事技術諮詢、進出口貿易，推廣微處理機應用。
  - 1977年，成立宏碁美國分公司。
  - 1978年，成立「宏亞微處理機研習中心」。
  - 1980年，宏碁推出台灣與全球第一個自行設計天龍中文終端機，榮獲「行政院院長獎」。
  - 1981年，宏碁在[新竹科學工業園區設立工廠](../Page/新竹科學工業園區.md "wikilink")，並推出[小教授一號](../Page/小教授一號.md "wikilink")。
  - 1982年，推出台灣與全球第一部中文家用電腦[小教授二號](../Page/小教授二號.md "wikilink")。
  - 1984年，推出第四代中文電腦[天龍DCS
    570](../Page/天龍DCS_570.md "wikilink")」及推出16位元個人電腦。成立宏碁日本分公司。
  - 1985年，成立宏碁德國分公司。
  - 1986年，宏碁領先[IBM](../Page/IBM.md "wikilink")，開發成功推出32位元個人電腦。宏碁公司創業10週年。
  - 1987年，宏碁自我品牌Multitech名稱更名為Acer。
  - 1988年，宏碁公司在台灣股票市場正式上市。(股票代號:2353)
  - 1995年，宏碁推出Aspire家用電腦。
  - 1996年，宏碁公司創業20週年。
  - 1997年，宏碁公司併購[德州儀器筆記型電腦事業部門](../Page/德州儀器.md "wikilink")
  - 1998年，宏碁贊助[1998年亞洲運動會](../Page/1998年亞洲運動會.md "wikilink")，提供該屆大會所需個人電腦，寫下全球以個人電腦支援大型國際體育賽事首例。
  - 2000年，宏碁陷入虧損，[施振榮展開品牌與代工分家策略](../Page/施振榮.md "wikilink")，將宏碁集團切割成「宏碁集團」、「明基電通集團」、「緯創集團」等「泛宏碁集團」，被施振榮形容為「兄弟登山，各自努力」。
  - 2001年，[王振堂擔任宏碁總經理](../Page/王振堂.md "wikilink")；[蔣凡可·蘭奇擔任宏碁歐洲區總經理](../Page/蔣凡可·蘭奇.md "wikilink")，期間大力發展筆記型電腦，成為歐洲第一大筆記型電腦品牌。
  - 2006年，宏碁集團創業30週年。
  - 2008年，宏碁的品牌定位策略定調，由旗艦品牌「Acer」擔綱以新技術、效能為主的市場；「eMachines」則會延續既有的低價入門級市場，同時這兩者的行銷範圍將遍及全球；「Gateway」和「Packard
    Bell」主打類似[蘋果公司的時尚潮流路線](../Page/蘋果公司.md "wikilink")：「Packard
    Bell」只會在歐洲市場銷售；「Gateway」則在已有基礎的美洲以外，將擴展到亞太市場。
  - 2010年，宏碁成為全球第二大個人電腦(PC)廠商，僅次於[惠普](../Page/惠普.md "wikilink")。
  - 2011年3月，宏碁召開臨時董事會，王振堂在會中宣布解除[蘭奇的執行長職位](../Page/蔣凡可·蘭奇.md "wikilink")，引發外資疑慮，宏碁股價連續下跌。王振堂希望投入更多資源在智能手機與平板電腦。[蘭奇認為桌上型電腦和筆記型電腦重要性高於手機和平板電腦](../Page/蔣凡可·蘭奇.md "wikilink")。而宏碁仍單一擁抱[微軟陣營的平台在全部產品](../Page/微軟.md "wikilink")。微軟行動裝置領域難敵[谷歌的](../Page/谷歌.md "wikilink")[Android和](../Page/Android.md "wikilink")[蘋果公司的](../Page/蘋果公司.md "wikilink")[iOS](../Page/iOS.md "wikilink")，谷歌、蘋果興起，宏碁應變不及，連串錯誤判斷造成3年後宏碁幾乎全產品線潰敗。
  - 2011年4月，王振堂宣布[翁建仁接任宏碁全球總裁](../Page/翁建仁.md "wikilink")。
  - 2012年，宏碁成為[2012年倫敦奧運會全球贊助商](../Page/2012年倫敦奧運會.md "wikilink")\[2\]。
  - 2013年11月，宏碁公布第3季淨損131.2億元，每股淨損4.82元，王振堂與翁建仁雙雙請辭下台。宏碁精簡全球人力7%，成立變革委員會，施振榮任召集人，公司進入史上第三次變革。
  - 2013年12月，宏碁董事會通過延攬台積電全球業務暨行銷資深副總[陳俊聖](../Page/陳俊聖.md "wikilink")，出任全球總裁暨執行長。
  - 2013年12月，宏碁董事會決議增提13億元材料庫存損失，去年全年大虧205.79億元，每股虧損7.56元。
  - 2014年，宏碁2014第三季出貨量年成長率全球第一。
  - 2014年8月，宏碁第二季合併營收813.37億元，營業淨利6.62億元，稅後淨利4.85億元，EPS 0.18元。
  - 2015年，宏碁宣布，2014年第4季在全世界許多國家所售的桌上型電腦和筆記型電腦都獲得不少好成績：智利消費型筆電市場穩定成長，市占率達25.1%；台灣市場也持續布建，從去年9月市占率不到19%一路成長至12月的23.9%，首季市占率也將挑戰25%。宏碁宣布完成現金增資案，發行3億股、共募集新台幣54億元資金，增資股預計11日撥入投資人帳戶。這是宏碁2000年底宣布分家以來首次辦理現金增資，除了公開承銷及保留供員工認購各3000萬股外，其餘由原股東以每股可認購0.086股比率認購，共計發行3億股。
  - 2016年，宏碁集團創業40週年。
  - 2016年4月，宏碁宣布旗下Predator電競子品牌機種可支援4種VR裝置，在[紐約](../Page/紐約.md "wikilink")[記者會](../Page/記者會.md "wikilink")，進行相關發表。\[3\]
  - 2016年4月，宏碁舉辦2016全球記者會 以next@acer為主題
    發表多款產品並介紹新事業發展：以新的智雲體（BeingWare）作為軟體、硬體和雲端服務的策略主軸，推出能創造更多整合價值的新時代產品，董事會決議通過將設立100%持股的「新事業投資控股公司」，區分電腦「核心產品事業」、以及負責BYOC自建雲、手機、與穿戴裝置的「新事業」，核心產品事業掌管產品如筆記型電腦、桌上型電腦、平板電腦、數位顯示產品、伺服器產品等與相關研發與全球營運；新事業掌管自建雲BYOC、手機與穿戴裝置等智慧設備、以及研發部門。核心事業的整合收斂，將更聚焦並持續優化營運資源與商業模式，相關組織調整將於7月上路。
  - 2016年6月，宏碁及瑞典商Starbreeze共同宣布將設立合資公司，發展StarVR虛擬實境頭戴式顯示器。
  - 2016年10月，宏碁計畫於2017年第一季擴大VR產品布局，推出支援Windows Holographic平台虛擬實境頭戴式顯示器。
  - 2016年11月，宏碁第三季合併營收585.93億元，稅後淨利2.49億元，毛利率達9.5％。
  - 2016年11月，宏碁董事會今日通過，投資一千萬美元於IMAX 虛擬實境內容基金（IMAX VR Content
    Fund），希望藉此推動更多高品質、且更具臨場感的虛擬實境內容發展。
  - 2016年12月，宏碁進行無形資產減損，此次減損金額為63.4億元，係歸屬於自建雲智慧產品事業相關無形資產減損61.9億元；另一項目為Gateway、Packard
    Bell品牌商標權之價值評估的減損約1.5億元。
  - 2017年4月，宏碁於紐約「next@acer」全球記者會中，推出2017年「返校季」新品，強調其冷卻技術：電競筆電、二合一筆電以及All-in-One系列電腦。加上首度公開展示的Windows
    Mixed Reality混合實境體驗與領先業界的解析度及視野的StarVR。
  - 2017年6月，宏碁臨時董事會通過決議，將由陳俊聖執行長接任董事長、並繼續兼任執行長一職，黃少華先生將卸任董事長職位、留任為宏碁公司董事會成員。
  - 2017年8月，宏碁第二季合併營收536.04億元，營業淨利9.92億元，淨利率達1.8%，為六年來的歷史新高。
  - 2017年8月，宏碁於德國柏林舉行2017 [IFA](../Page/柏林國際廣播展.md "wikilink")
    展前記者會推出5大類別的新產品：超窄邊框All-in-One一體成形桌機、Predator電競系列、筆記型電腦、智慧生活應用、寵物智慧穿戴裝置等共13款新品。
  - 2017年11月，宏碁第3季營運合併營收605.79億元，稅後淨利14.49億元，創27季新高，相較去年同期跳升近5倍。
  - 2018年5月，宏碁美国电竞市场市场占有率28%，市场排名第一，巴西市场电竞笔电则占有46%，稳坐龙头。
  - 2018年8月，宏碁於德國柏林舉行IFA大型新品記者會，介紹宏碁最新VR頭盔、最輕薄筆記型電腦、無邊框AIO（一體成型電腦）、玩家極致夢想的電競椅等。
  - 2018年12月，宏碁旗下VR事業[StarVR宏星技術](../Page/StarVR.md "wikilink")（6681）終止興櫃，因近兩年營運出現虧損，而且主要海外股東瑞典遊戲商[Starbreeze於月初申請破產重整](../Page/Starbreeze.md "wikilink")，目前暫停StarVR的開發計畫，而這個計畫才不到20天就宣告停擺。

## 收購

  - 1987年，宏碁收購康點電腦公司。
  - 1990年，宏碁收購高圖斯公司。
  - 1997年，宏碁收購德州儀器筆記型電腦事業群。
  - 2007年，宏碁收購eMachines、Gateway和Packard Bell。
  - 2008年，宏碁收購倚天資訊股份有限公司。
  - 2010年，宏碁收購方正科技集團股份有限公司電腦業務。
  - 2016年，宏碁通過三項收購案和兩項合資案，包括收購智慧車錶品牌Xplova、專攻寵物互動監控的波寶創新科技、開發「停車大聲公」App的鷹諾；合資案則攜手瑞典知名遊戲商Starbreeze、長輩平板服務grandPad。
  - 在2016年6月份，台湾企业宏碁宣布向星风工作室（Starbreeze）投资900万美元，并同时成立合资公司宏星技术负责StarVR头显的研发设计、制造、营销及销售。合资公司成立之初注册资本为2500万美元，双方约定分别持股50%，目前宏碁和星风工作室也已分别注资1000万美元到合资公司，而这次双方决定剩余500万美元注册资本将全部由宏碁缴纳，宏碁持股66.67％

## 獎項

  - 2006年，2項產品榮獲日本優良設計獎，分別是筆記型電腦Ferrari 1000與Ferrari
    5000；2項產品榮獲德國紅點設計大獎，分別是桌上型電腦Aspire
    E與數碼相機CU-6530；2項產品榮獲德國iF設計大獎，分別是筆記型電腦TravelMate
    8200與顯示器5；連續七年榮獲讀者文摘最佳品牌。

  - 2007年，桌上型電腦捷威Gateway One榮獲美國數位生活消費電子展最佳個人電腦獎暨最佳產品獎；桌上型電腦Packard
    BelliXtreme榮獲Janus工業設計獎；智慧導航PDA C500榮獲德國iF設計大獎；桌上型電腦Aspire
    L320榮獲台北國際電腦展Best Choice獎；桌上型電腦Aspire L350榮獲德國紅點設計大獎。

  - 2008年，筆記型電腦Aspire Gemstone Blue榮獲德國iF設計大獎；筆記型電腦Aspire
    One榮獲日本優良設計獎；筆記型電腦Aspire Gemstone
    Blue榮獲台北國際電腦展Best Choice獎及創新設計獎；Packard Bell榮獲歐洲Plus
    X最佳創新品牌獎；筆記型電腦Gateway
    P-6831FX榮獲美國消費性電子展最佳產品獎；連續十年獲選最受亞洲消費者信賴的個人電腦品牌

  - 2009年，筆記型電腦Aspire Z5000榮獲2009年台北國際電腦展Best
    Choice獎；投影機K-10榮台北國際電腦展綠色資訊Best
    Choice獎；筆記型電腦Aspire 3935榮獲台北國際電腦創新設計金質獎。

  - 2010年，4項產品榮獲日本優良設計獎，分別是筆記型電腦Aspire 8943G，筆記型電腦Gateway EC39，智能手機Acer
    Liquid Metal與顯示器Gateway FHD 2303L；桌上型電腦Gateway
    FX榮獲美國消費性電子協會電腦硬體類創新設計獎；4項產品榮獲台北國際電腦展創新設計獎，分別是筆記型電腦Aspire
    8943G，筆記型電腦Aspire Timeline 3820 T/TG，桌上型電腦Aspire X與筆記型電腦Gateway
    EC39C，Aspire 8943G更榮獲金質獎；2項產品榮獲台北國際電腦展最佳產品獎，分別是筆記型電腦Aspire
    8943G與投影機K11；顯示器S243HL榮獲德國iF設計大獎；2項產品榮獲美國消費性電子協會電腦硬體類創新設計獎，分別是筆記型電腦Aspire
    5738DG 3D與桌上型電腦Aspire Z5610 AIO。

  - 2011年，2項產品榮獲2011年台北國際電腦展的最佳產品獎，分別是智能手機Iconia Smart與桌上型電腦Revo
    100；6項產品榮獲台北國際電腦展創新設計獎，分別是平板電腦Iconia Tab A50，智能手機Iconia
    Smart，雙螢幕觸控筆記型電腦Iconia，筆記型電腦Aspire Ethos 8951G ，桌上型電腦Revo
    100與桌上型電腦Aspire Z5801；5項產品榮獲德國紅點設計大獎，分別是桌上型電腦Revo
    100，桌上型電腦Aspire Z5801，筆記型電腦Aspire Ethos
    8950G，投影機C20及雙螢幕平板電腦Iconia；筆記型電腦Iconia獲選拉斯維加斯北美消費性電子展最受喜愛的產品；4項產品榮獲德國iF設計大獎，分別是筆記型電腦Aspire
    8943G，筆記型電腦Packard Bell EasyNote Butterfly，顯示器Gateway FHD
    2303L與投影機Acer C20

  - 2012年，2項產品榮獲美國消費性電子協會電腦硬體類創新設計獎，分別是筆記型電腦Aspire S3
    Ultrabook與桌上型電腦Aspire Z7871。

  - 2013年，筆記型電腦Aspire R7榮獲德國紅點設計大獎，得獎產品包括：美型超薄筆電Aspire S
    13、可翻轉觸控筆電Aspire R 15、取自積木靈感的迷你PC - Revo
    Build、曲面螢幕XR342CK、全球首款可支援abPBX
    全功能的多媒體觸控式桌上型電話abTouchPhone、商用筆電TravelMate
    P6系列，以及體積輕巧、方便擴充的Veriton N系列商用桌機。

  - 2014年，4項產品榮獲德國iF產品設計大獎，分別是筆記型電腦TravelMate P645、智能手機Liquid
    Z5、投影機K137與筆記型電腦Aspire R7。

  - 2016年，宏碁七項產品榮獲2016年Red Dot設計獎

  - 2016年，宏碁八項產品榮獲2016年台北國際電腦展創新設計獎殊榮，包括：迷你PC Revo Build、Aspire One
    11筆電、Acer XR342CK曲面螢幕、Acer RT240Y窄邊框纖薄美型螢幕、短焦LED投影機Acer
    K138STi 、雷射超短焦投影機Predator Z850以及Liquid Jade 2手機和Liquid Jade Primo
    Desktop Kit。

  - 2016年，3項產品榮獲美國消費性電子展CES創新獎：Predator Z301CT電競顯示器，獲電腦周邊（Computer
    Peripherals）產品最高榮譽-最佳創新獎（Best of Innovation award）；全新可翻轉筆電Spin
    7，及超輕薄筆電Swift 7，獲得評審青睞，在電腦硬體與零組件（Computer Hardware and
    Components）產品中亦雙雙入選創新獎項。

  -
[缩略图](https://zh.wikipedia.org/wiki/File:Acer_Veriton_20170520.jpg "fig:缩略图")

## 旗下產品

宏碁營運分為兩大事業單位：一是新核心事業（New Core
Business），致力於個人電腦相關的IT產品的研發、行銷、銷售與服務；二是價值創新事業（New
Value Creation Business），包含BYOC雲端服務事業單位、以及電子化事業服務單位。

### 硬體

[缩略图](https://zh.wikipedia.org/wiki/File:North_Korea_-_Luxury_goods_\(5382066018\).jpg "fig:缩略图")被拍攝到的宏碁電腦\]\]

#### 手機

##### beTouch

  - [Acer beTouch E100](../Page/Acer_beTouch_E100.md "wikilink")
  - [Acer beTouch E101](../Page/Acer_beTouch_E101.md "wikilink")
  - [Acer beTouch E110](../Page/Acer_beTouch_E110.md "wikilink")
  - [Acer beTouch E120](../Page/Acer_beTouch_E120.md "wikilink")
  - [Acer beTouch E130](../Page/Acer_beTouch_E130.md "wikilink")
  - [Acer beTouch E140](../Page/Acer_beTouch_E140.md "wikilink")
  - [Acer beTouch E200](../Page/Acer_beTouch_E200.md "wikilink")
  - [Acer beTouch E400](../Page/Acer_beTouch_E400.md "wikilink")

##### Liquid

  - [Acer Liquid](../Page/Acer_Liquid.md "wikilink")
  - [Acer Liquid Jade](../Page/Acer_Liquid_Jade.md "wikilink")
  - [Acer Liquid Jade S](../Page/Acer_Liquid_Jade_S.md "wikilink")
  - [Acer Liquid Jade Z](../Page/Acer_Liquid_Jade_Z.md "wikilink")
  - [Acer Liquid Jade
    Primo](../Page/Acer_Liquid_Jade_Primo.md "wikilink")
  - [Acer Liquid C1](../Page/Acer_Liquid_C1.md "wikilink")
  - [Acer Liquid E](../Page/Acer_Liquid_E.md "wikilink")
  - [Acer Liquid E1](../Page/Acer_Liquid_E1.md "wikilink")
  - [Acer Liquid E2](../Page/Acer_Liquid_E2.md "wikilink")
  - [Acer Liquid E3](../Page/Acer_Liquid_E3.md "wikilink")
  - [Acer Liquid E600](../Page/Acer_Liquid_E600.md "wikilink")
  - [Acer Liquid E700](../Page/Acer_Liquid_E700.md "wikilink")
  - [Acer Liquid Gallant](../Page/Acer_Liquid_Gallant.md "wikilink")
  - [Acer Liquid Gallant S
    Duo](../Page/Acer_Liquid_Gallant_S_Duo.md "wikilink")
  - [Acer Liquid Glow](../Page/Acer_Liquid_Glow.md "wikilink")
  - [Acer Liquid M220](../Page/Acer_Liquid_M220.md "wikilink")
  - [Acer Liquid M320](../Page/Acer_Liquid_M320.md "wikilink")
  - [Acer Liquid M330](../Page/Acer_Liquid_M330.md "wikilink")
  - [Acer Liquid Metal](../Page/Acer_Liquid_Metal.md "wikilink")
  - [Acer Liquid Mini](../Page/Acer_Liquid_Mini.md "wikilink")
  - [Acer Liquid S1](../Page/Acer_Liquid_S1.md "wikilink")
  - [Acer Liquid S2](../Page/Acer_Liquid_S2.md "wikilink")
  - [Acer Liquid X1](../Page/Acer_Liquid_X1.md "wikilink")
  - [Acer Liquid X2](../Page/Acer_Liquid_X2.md "wikilink")
  - [Acer Liquid Z2](../Page/Acer_Liquid_Z2.md "wikilink")
  - [Acer Liquid Z3](../Page/Acer_Liquid_Z3.md "wikilink")
  - [Acer Liquid Z3S](../Page/Acer_Liquid_Z3S.md "wikilink")
  - [Acer Liquid Z4](../Page/Acer_Liquid_Z4.md "wikilink")
  - [Acer Liquid Z5](../Page/Acer_Liquid_Z5.md "wikilink")
  - [Acer Liquid Z110](../Page/Acer_Liquid_Z110.md "wikilink")
  - [Acer Liquid Z200](../Page/Acer_Liquid_Z200.md "wikilink")
  - [Acer Liquid Z220](../Page/Acer_Liquid_Z220.md "wikilink")
  - [Acer Liquid Z320](../Page/Acer_Liquid_Z320.md "wikilink")
  - [Acer Liquid Z330](../Page/Acer_Liquid_Z330.md "wikilink")
  - [Acer Liquid Z410](../Page/Acer_Liquid_Z410.md "wikilink")
  - [Acer Liquid Z500](../Page/Acer_Liquid_Z500.md "wikilink")
  - [Acer Liquid Z520](../Page/Acer_Liquid_Z520.md "wikilink")
  - [Acer Liquid Z530](../Page/Acer_Liquid_Z530.md "wikilink")
  - [Acer Liquid Z530S](../Page/Acer_Liquid_Z530S.md "wikilink")
  - [Acer Liquid Z630](../Page/Acer_Liquid_Z630.md "wikilink")
  - [Acer Liquid Z630S](../Page/Acer_Liquid_Z630S.md "wikilink")

##### neoTouch

  - [Acer neoTouch S200](../Page/Acer_neoTouch_S200.md "wikilink")
  - [Acer neoTouch P300](../Page/Acer_neoTouch_P300.md "wikilink")
  - [Acer neoTouch P400](../Page/Acer_neoTouch_P400.md "wikilink")

##### 其他

  - [Acer A800](../Page/Acer_A800.md "wikilink")
  - [Acer Allegro](../Page/Acer_Allegro.md "wikilink")
  - [Acer AK330](../Page/Acer_AK330.md "wikilink")
  - [Acer AT390](../Page/Acer_AT390.md "wikilink")
  - [Acer CloudMobile S500](../Page/Acer_CloudMobile_S500.md "wikilink")
  - [Acer Iconia Smart](../Page/Acer_Iconia_Smart.md "wikilink")
  - [Acer Jade Primo](../Page/Acer_Jade_Primo.md "wikilink")
  - [Acer Stream](../Page/Acer_Stream.md "wikilink")

#### 平板電腦

  - [Acer Aspire](../Page/Acer_Aspire.md "wikilink")
  - [Acer Iconia](../Page/Acer_Iconia.md "wikilink")
  - [Acer Predator 8](../Page/Acer_Predator_8.md "wikilink")
  - [Acer Tab 7](../Page/Acer_Tab_7.md "wikilink")

#### 筆記型電腦

  - [Acer Aspire](../Page/Acer_Aspire.md "wikilink")
  - [Acer Swift](../Page/Acer_Swift.md "wikilink")
  - [Acer Chromebook](../Page/Acer_Chromebook.md "wikilink")
  - [Acer Extensa](../Page/Acer_Extensa.md "wikilink")
  - [Acer Gemstone](../Page/Acer_Gemstone.md "wikilink")
  - [Acer Predator](../Page/Acer_Predator.md "wikilink")
  - [Acer Nitro](../Page/Acer_Nitro.md "wikilink")
  - [Acer Predator 6](../Page/Acer_Predator_6.md "wikilink")
  - [Acer TravelMate](../Page/Acer_TravelMate.md "wikilink")
  - [Acer One](../Page/Acer_One.md "wikilink")
  - [Acer Predator 21X](../Page/Acer_Predator_21X.md "wikilink")
  - [Acer Predator 17X](../Page/Acer_Predator_17X.md "wikilink")
  - [Acer Predator G9](../Page/Acer_Predator_G9.md "wikilink")
  - [Acer Predator Triton
    700](../Page/Acer_Predator_Triton_700.md "wikilink")
  - [Acer Predator Triton
    900](../Page/Acer_Predator_Triton_900.md "wikilink")
  - [Acer Predator helios
    500](../Page/Acer_Predator_helios_500.md "wikilink")
  - [Acer Predator helios
    300](../Page/Acer_Predator_helios_300.md "wikilink")
  - [Acer Predator helios
    700](../Page/Acer_Predator_helios_700.md "wikilink")

#### 桌上型電腦

  - [Acer Aspire](../Page/Acer_Aspire.md "wikilink")
  - [Acer Chromebox](../Page/Acer_Chromebox.md "wikilink")
  - [Acer Predator](../Page/Acer_Predator.md "wikilink")
  - [Acer Revo](../Page/Acer_Revo.md "wikilink")
  - [Acer Veriton](../Page/Acer_Veriton.md "wikilink")

**显示器**

Acer Predator X27

Acer Predator XB27HU

Acer Predator X34P

Acer Predator Z35P

Acer PE320QK Pro

**投影仪**

acer H7850

acer Predator Z650

acer D620D

acer K138ST

acer V7500

acer H7550ST

#### 智能配件

  - [Acer Liquid Leap](../Page/Acer_Liquid_Leap.md "wikilink")
  - [Acer Liquid Leap+](../Page/Acer_Liquid_Leap+.md "wikilink")
  - [Acer Liquid Leap
    Active](../Page/Acer_Liquid_Leap_Active.md "wikilink")
  - [Acer Liquid Leap
    Curve](../Page/Acer_Liquid_Leap_Curve.md "wikilink")
  - [Acer Liquid Leap Fit](../Page/Acer_Liquid_Leap_Fit.md "wikilink")

#### 伺服器

  - [Acer Altos C100 F3](../Page/Acer_Altos_C100_F3.md "wikilink")

  - [Acer Altos R360 F3](../Page/Acer_Altos_R360_F3.md "wikilink")

  - [Acer Altos R380 F3](../Page/Acer_Altos_R380_F3.md "wikilink")

  - [Acer Altos T110 F4](../Page/Acer_Altos_T110_F4.md "wikilink")

  - [Acer AT350 F3](../Page/Acer_AT350_F3.md "wikilink")

  - [Acer AW2000h F3](../Page/Acer_AW2000h_F3.md "wikilink")

  - Altos R320

  - Altos R360

  - Altos R380

  - Altos R480

  - Altos R680

  - Altos T110

  - Altos T310

  - Altos W2000h-W370h

  - Altos W2200h-W670h

  - AR580

  - AR780

  - AT350

  - AW2000h-AW170h

  - AW2000h-AW370h

  - AW2000h-W170h F2

  - TBD AW2000h-W370h

  - Veriton P10

  - Veriton P130

  -
  - Veriton P

  - Veriton P30

  - Veriton P330

**外设**

Acer Predator pwm510

## 参考文献

## 外部連結

  - [宏碁全球網站](http://www.acer.com/)

  - [宏碁集團](http://www.acer-group.com/)

  -
  -
  -
[宏碁](../Category/宏碁.md "wikilink") [A](../Category/台灣電子公司.md "wikilink")
[A](../Category/台灣品牌.md "wikilink")
[A](../Category/顯示科技公司.md "wikilink")
[A](../Category/行動電話製造商.md "wikilink")
[A](../Category/總部位於臺北市松山區的工商業機構.md "wikilink")
[A](../Category/總部位於新北市的工商業機構.md "wikilink")
[A](../Category/汐止區.md "wikilink")
[A](../Category/1976年成立的公司.md "wikilink")
[A](../Category/1976年台灣建立.md "wikilink")

1.  \[[https://www.idc.com/getdoc.jsp?containerId=prUS43734318\]IDC调研](https://www.idc.com/getdoc.jsp?containerId=prUS43734318%5DIDC调研)
    2018 PC market
2.  [London 2012 Olympic Games partners | The people delivering the
    Games |
    London 2012](http://www.london2012.com/about-us/the-people-delivering-the-games/london-2012-olympic-games-partners.php)

3.  [宏碁陳俊聖領軍　電競機支援4類VR裝置](http://www.appledaily.com.tw/realtimenews/article/finance/20160422/844341/)[蘋果日報](../Page/蘋果日報.md "wikilink")