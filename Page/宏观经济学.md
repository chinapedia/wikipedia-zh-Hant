**宏觀經濟學**（，來自希臘語前綴makro-意為“大”+經濟學），是指用[国民收入](../Page/国民收入.md "wikilink")、经济整体的[投资和](../Page/投资.md "wikilink")[消费等总体性的统计概念来分析经济运行规律的一个](../Page/消费.md "wikilink")[经济学领域](../Page/经济学.md "wikilink")。宏观经济学是相对于古典的[微观经济学而言的](../Page/微观经济学.md "wikilink")。

宏观经济学是[约翰·梅纳德·凯恩斯的](../Page/约翰·梅纳德·凯恩斯.md "wikilink")《[就业、利息和货币通论](../Page/就业、利息和货币通论.md "wikilink")》发表以来快速发展起来的一个经济学分支。

## 历史简介

[亞當·史密斯於其著作](../Page/亞當·史密斯.md "wikilink")《[國富論](../Page/國富論.md "wikilink")》中即已討論一個國家經濟成長的原因與條件。而一直到20世纪初期的主流經濟學家，對於的討論多著重於經濟成長的方面。

在1929年至1939年間，歐美自由經濟國家發生了嚴重的經濟衰退，這現象被稱為[經濟-{}-大恐慌或](../Page/经济大萧条.md "wikilink")[大蕭條](../Page/经济大萧条.md "wikilink")，這經濟大蕭條期間，[失業嚴重](../Page/失業.md "wikilink")，產量又下降。

在經濟大蕭條之前，當時主流學者不認為經濟衰退是一個嚴重且長久的現象。這些所謂的[古典經濟學的經濟學家延續著](../Page/古典經濟學.md "wikilink")[自由市場經濟的觀點](../Page/自由市場經濟.md "wikilink")，認為自由市場的[自由價格機制如同一隻看不見的手](../Page/自由價格機制.md "wikilink")，可使經濟自動[充分就業](../Page/充分就業.md "wikilink")。否定了[有效需求不足與](../Page/有效需求.md "wikilink")[失業嚴重同時存在的可能](../Page/失業.md "wikilink")，其看法以法國經濟學家[讓-巴蒂斯特·賽伊為代表](../Page/讓-巴蒂斯特·賽伊.md "wikilink")，他認為供給可以創造出同額的需求，而此看法被後人稱為[賽伊定律](../Page/賽伊定律.md "wikilink")。

但上述的看法卻與大蕭條時期的經濟狀況不符合。經濟大蕭條持續了十年，嚴重的失業現象未曾消失，是故在此背景之下，[凱因斯在](../Page/凱因斯.md "wikilink")1936年發表了《[就业、利息和货币通论](../Page/就業、利息與貨幣的一般理論.md "wikilink")》或簡稱為《通论》，而這也代表了現代總體經濟學的開始。凱因斯認為政府應在經濟衰退時提出各種政策以刺激需求，進而達到減緩失業與恢復經濟繁榮的目的。

到了1970年代，新的總體經濟問題又產生了。在1974年至1975年及1980年至1982年期間，歐美各國陷入[二次大戰之後最嚴重的經濟衰退](../Page/二次大戰.md "wikilink")，因[石油危機](../Page/石油危機.md "wikilink")、高通貨膨脹及高失業率產生的問題。逐漸地，凱因斯的理論受到其他理論的挑戰。

這些理論包含了以[米爾頓·傅利曼為代表的](../Page/米爾頓·傅利曼.md "wikilink")[貨幣學派和以](../Page/貨幣學派.md "wikilink")[小羅伯特·盧卡斯與](../Page/小羅伯特·盧卡斯.md "wikilink")[托馬斯·薩金特為代表的](../Page/托馬斯·薩金特.md "wikilink")[新興古典學派的理論](../Page/新興古典學派.md "wikilink")。而米爾頓·傅利曼與小羅伯特·盧卡斯分別於1976年和1995年因此理論而獲得了[諾貝爾經濟學獎](../Page/諾貝爾經濟學獎.md "wikilink")。

## 研究对象

宏观经济学要研究的问题是一个国家既有的各种生产[资源](../Page/资源.md "wikilink")（如[劳动力](../Page/劳动力.md "wikilink")，[土地](../Page/土地.md "wikilink")，[自然资源以及](../Page/自然资源.md "wikilink")[资本](../Page/资本.md "wikilink")）**实际上**会有多少被投入于各生产部门，并且研究投入后所产生的各种现象，以及研究这些现象背后的原因和规律。具体来讲，有以下三大研究问题。

### 经济周期问题

**经济周期问题**有时候也被称为[经济周期理论或者](../Page/经济周期理论.md "wikilink")[经济危机理论](../Page/经济危机.md "wikilink")。其研究的主题是为什么一些国家的[国民收入在其长期增长趋势中会出现周期性的上下波动现象](../Page/国民收入.md "wikilink")。

### 经济增长问题

**经济增长问题**也被称为[经济成长理论或](../Page/经济成长.md "wikilink")[经济发展理论](../Page/经济发展.md "wikilink")。其讨论的主要问题是在特定国家的历史发展过程中，制约和促进国民收入的主要因素和规律。从宏观经济学的角度讲，拉动一个国家或经济体增长的有三大要素，分别是[投资](../Page/投资.md "wikilink")、[消费](../Page/消费.md "wikilink")、[出口](../Page/出口.md "wikilink")，俗称“三架马车”。研究经济增长问题的著名经济学家包括[罗伯特·索洛](../Page/罗伯特·索洛.md "wikilink")，[保罗·罗默等](../Page/保罗·罗默.md "wikilink")，其中罗伯特·索洛获得了1987年诺贝尔经济学奖。

#### 经济增长的概念

#### 经济增长理论

#### 世界的经济增长

[缩略图](https://zh.wikipedia.org/wiki/File:2000-2011中国GDP增长率.jpg "fig:缩略图")

#### 经济增长的国际比较（巴西、俄罗斯、印度、中国）

[缩略图](https://zh.wikipedia.org/wiki/File:金砖四国GDP增长率.jpg "fig:缩略图")

#### 世界经济的未来

### 国民收入和就业问题

**国民收入和就业问题**有时也被称为[国民收入决定理论](../Page/国民收入决定理论.md "wikilink")、[就业理论或](../Page/就业理论.md "wikilink")[失业理论](../Page/失业理论.md "wikilink")。具体来讲就是研究一个时期国民收入的总量和就业量（或失业量）是怎样决定的。

## 宏观经济学诸学派

  - [传统凯恩斯宏观经济学](../Page/传统凯恩斯宏观经济学.md "wikilink")
  - [後凱恩斯學派](../Page/後凱恩斯學派.md "wikilink")
  - [供给学派](../Page/供给学派.md "wikilink")
  - [新古典綜合學派](../Page/新古典綜合學派.md "wikilink")
  - [貨幣學派](../Page/貨幣學派.md "wikilink")
  - [理性预期宏观经济学](../Page/理性预期宏观经济学.md "wikilink")
  - [新古典派宏观经济学](../Page/新古典派宏观经济学.md "wikilink")
  - [新凱恩斯學派](../Page/新凱恩斯學派.md "wikilink")
  - [宏观宏观经济学](../Page/宏观宏观经济学.md "wikilink")

## 宏观经济学中的一些概念

  - [挤出效应](../Page/挤出效应.md "wikilink")
  - [国内生产总值](../Page/国内生产总值.md "wikilink")（GDP）
  - [國民生產總值](../Page/國民生產總值.md "wikilink")（GNP）
  - [物价指数](../Page/物价指数.md "wikilink")
  - [失业率](../Page/失业率.md "wikilink")
  - [菲利普斯曲线](../Page/菲利普斯曲线.md "wikilink")
  - [产权](../Page/产权.md "wikilink")
  - [货币](../Page/货币.md "wikilink")
  - [庇古效应](../Page/庇古效应.md "wikilink")
  - [边际消费倾向](../Page/边际消费倾向.md "wikilink")
  - [凯恩斯效应](../Page/凯恩斯效应.md "wikilink")
  - [無條件基本收入](../Page/無條件基本收入.md "wikilink")
  - [軍事凱因斯主義](../Page/軍事凱因斯主義.md "wikilink")
  - [消費函數](../Page/消費函數.md "wikilink")
  - [通货膨胀](../Page/通货膨胀.md "wikilink")
  - [财政政策](../Page/财政政策.md "wikilink")
  - [货币政策](../Page/货币政策.md "wikilink")

## 代表人物

  - [傅利曼](../Page/傅利曼.md "wikilink")
  - [顧志耐](../Page/顧志耐.md "wikilink")(被稱為國民經濟學之父)
  - [曼昆](../Page/曼昆.md "wikilink")

## 参考文献

  - ［英］[约翰·梅纳德·凯恩斯](../Page/约翰·梅纳德·凯恩斯.md "wikilink") 著，高鸿业
    重译《就业、利息和货币通论》（[汉译世界学术名著丛书](../Page/汉译世界学术名著丛书.md "wikilink")）[商务印书馆](../Page/商务印书馆.md "wikilink")，1999。
  - ［加］迈克尔·帕金 著，张军 等 译《宏观经济学》 第8版
    [人民邮电出版社](../Page/人民邮电出版社.md "wikilink")，2011。
  - Robert M. Solow, "A Contribution to the Theory of Economic Growth",
    *The Quarterly Journal of Economics*, Vol. 70, No. 1 (Feb., 1956),
    pp. 65–94

## 參見

  -
{{-}}

[Category:经济学分支](../Category/经济学分支.md "wikilink")
[宏观经济学](../Category/宏观经济学.md "wikilink")