**宗主教**（；），[东正教在](../Page/东正教.md "wikilink")[中文传统上习惯翻译为](../Page/中文.md "wikilink")**牧首**，是實施[主教制度的](../Page/主教.md "wikilink")[基督教](../Page/基督教.md "wikilink")[宗派的一種](../Page/基督教派系.md "wikilink")[神職人員職稱](../Page/神職人員.md "wikilink")。

最初，宗主教是一位作为[家父](../Page/家父_\(pater_familias\).md "wikilink")（[pater
familias](../Page/pater_familias.md "wikilink")）对某个扩展家庭行使[专制的权力的男人](../Page/独裁.md "wikilink")。该种由年长男性对家庭进行支配的体系被称为[父权](../Page/父权.md "wikilink")（patriarchy）（Πατριάρχης）。这是一个[希腊文词汇](../Page/希腊文.md "wikilink")，由πατήρ
([pater](../Page/pater.md "wikilink")，意为“父亲”)和ἄρχων
([archon](../Page/archon.md "wikilink")，意为领导leader,首领chief，统治者ruler，国王king，等等)

[亚伯拉罕](../Page/亚伯拉罕.md "wikilink")，[以撒](../Page/以撒.md "wikilink")，以及[雅各被提及为三位](../Page/雅各.md "wikilink")[以色列人的](../Page/以色列人.md "wikilink")[族长](../Page/族长_\(圣经\).md "wikilink")（patriarch,天主教中文译译法，或为“圣祖”），而他们生活的时期即被称为“[族长时代](../Page/族长时代.md "wikilink")”（Patriarchal
Age）。它最初是在[圣经的](../Page/圣经.md "wikilink")[七十士譯本中获得了其宗教上的含义](../Page/七十士譯本.md "wikilink")\[1\]。

该词主要具有特殊的教会的多种含义。特别是，[东正教会](../Page/东正教会.md "wikilink")、[罗马天主教会](../Page/罗马天主教会.md "wikilink")（高于[大总主教](../Page/大总主教.md "wikilink")（Major
Archbishop）和[首席主教](../Page/首席主教.md "wikilink")（primate））、[东方亚述教会中最高等级的](../Page/东方亚述教会.md "wikilink")[主教均称为](../Page/主教.md "wikilink")“patriarchs”。此位patriarch的办公地以及教会的征役（由一个或更多的教省组成，尽管在他自己的（总）教区之外他经常没有可实施之司法权）被称为“[宗主教区](../Page/宗主教区.md "wikilink")”（patriarchate）。从历史上说，宗主教可能经常是一个充当[行政长官](../Page/行政长官.md "wikilink")（Ethnarch）的合理人选，后者可在一个信奉其他教义的国家或帝国内代表由其宗教团体所形成的社区（如在[奥斯曼帝国内的基督徒](../Page/奥斯曼帝国.md "wikilink")）。

宗主教是[早期基督教在一些主要城市的](../Page/早期基督教.md "wikilink")[主教的称号](../Page/主教.md "wikilink")，他们的威望和权力比一般的主教要高；其中[罗马](../Page/罗马.md "wikilink")、[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")、[亚历山大港](../Page/亚历山大港.md "wikilink")、[安条克](../Page/安条克.md "wikilink")、[耶路撒冷等五個首要主教所領導的](../Page/耶路撒冷.md "wikilink")[教會](../Page/教會.md "wikilink")，又合稱為「[五大宗主教區](../Page/五大宗主教區.md "wikilink")」（Pentarchy；亦稱為「五牧首聯合治理教會」）。当[罗马天主教会和](../Page/罗马天主教会.md "wikilink")[东正教会分裂时](../Page/东正教会.md "wikilink")，罗马的宗主教成为天主教的[教宗](../Page/教宗.md "wikilink")，而君士坦丁堡的宗主教成为东正教普世宗主教。现今在世界许多地方都可以发现一至数名宗主教/牧首，特别是在历史形成的宗教中心更是如此，比如[亚历山大宗主教](../Page/亚历山大宗主教.md "wikilink")。

## 东方正教会的牧首

[Bartolomew_I.jpg](https://zh.wikipedia.org/wiki/File:Bartolomew_I.jpg "fig:Bartolomew_I.jpg")

并非全部15个[东正教自主教会的领袖的称号都是宗主教](../Page/东正教自主教会.md "wikilink")。实际上使用这一称号的自主教会领袖有9个，其他自主教会领袖的称号是[都主教或](../Page/都主教.md "wikilink")[總主教](../Page/總主教.md "wikilink")。

使用宗主教称号的东正教自主教会领袖实际上都是独立、平权的，然而按照“历史荣誉”排序如下（名称为当前正教中文全称）：

1.  [君士坦丁堡，新罗马總主教和普世牧首](../Page/君士坦丁堡普世牧首.md "wikilink")
2.  [亚历山大和全非洲牧首](../Page/亞歷山大宗主教.md "wikilink")
3.  [大安提阿和全东方希腊教会牧首](../Page/大安提阿和全东方希腊教会牧首.md "wikilink")
4.  [耶路撒冷圣城和全巴勒斯坦牧首](../Page/耶路撒冷牧首.md "wikilink")
5.  [莫斯科及全俄羅斯牧首](../Page/莫斯科及全俄羅斯牧首.md "wikilink")
6.  [全喬治亞大公長牧首](../Page/全喬治亞大公長牧首.md "wikilink")
7.  [塞尔维亚牧首](../Page/塞尔维亚牧首.md "wikilink")
8.  [卡帕多细亚—恺撒城的继承者，翁格罗—弗拉克人的都主教，布加勒斯特的總主教，全罗马尼亚牧首](../Page/全罗马尼亚牧首.md "wikilink")
9.  [全保加利亚牧首](../Page/全保加利亚牧首.md "wikilink")

这9个牧首中，有4个是古代传流下来，而另外5个是晚近才确认的：

  - 古代宗主教区（最初也包括[罗马宗主教区](../Page/圣座.md "wikilink")）：
      - [君士坦丁堡普世宗主教](../Page/君士坦丁堡普世牧首.md "wikilink")（Ecumenical
        Patriarch of
        Constantinople）（东正教中文译名：[君士坦丁堡普世牧首](../Page/君士坦丁堡普世牧首.md "wikilink")）（领导[君士坦丁堡正教会Orthodox](../Page/君士坦丁堡正教会.md "wikilink")
        Church of
        Constantinople，并且是[东正教](../Page/东正教.md "wikilink")（Eastern
        Orthodoxy）精神领袖）
      - [亚历山大和全非洲宗主教](../Page/希腊正教亚历山大牧首列表.md "wikilink")（Patriarch of
        Alexandria and All
        Africa）（东正教中文译名：[亚历山大和全非洲牧首](../Page/亚历山大和全非洲牧首.md "wikilink")）（领导[亚历山大希腊正教会Greek](../Page/亚历山大希腊正教会.md "wikilink")
        Orthodox Church of Alexandria）
      - [安条克宗主教](../Page/希腊安条克牧首列表.md "wikilink")（Patriarch of
        Antioch）（东正教中文译名：[安条克牧首](../Page/安条克牧首.md "wikilink")）（领导在[近东的](../Page/近东.md "wikilink")[安条克和全东方希腊正教会](../Page/安条克正教会.md "wikilink")）
      - [耶路撒冷宗主教](../Page/耶路撒冷牧首.md "wikilink")（Patriarch of
        Jerusalem）（东正教中文译名：[耶路撒冷牧首](../Page/耶路撒冷牧首.md "wikilink")）（领导在[以色列](../Page/以色列.md "wikilink")、[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")、[约旦和全](../Page/约旦.md "wikilink")[阿拉伯的](../Page/阿拉伯.md "wikilink")[耶路撒冷和圣锡安希腊正教会](../Page/耶路撒冷正教会.md "wikilink")）

<!-- end list -->

  - 在五大宗主教区建立之后的五个晚出的牧首区，根据其被君士坦丁堡普世牧首确认为宗主教区的时间先后排列如下：
      - [全保加利亚牧首](../Page/保加利亚正教会牧首列表.md "wikilink")（Patriarch of All
        Bulgaria）（领导[保加利亚的](../Page/保加利亚.md "wikilink")[保加利亚正教会Bulgarian](../Page/保加利亚正教会.md "wikilink")
        Orthodox Church），927年被确认为牧首区。\[2\]
      - [全格鲁吉亚大公長牧首](../Page/全格鲁吉亚大公長牧首.md "wikilink")（Catholicos-Patriarch
        of All
        Georgia）（领导[格鲁吉亚的](../Page/格鲁吉亚.md "wikilink")[格鲁吉亚正教会](../Page/格鲁吉亚正教会.md "wikilink")），1008年被确认为Catholicosate（即Patriarchate，牧首区）\[3\]
      - [塞尔维亚牧首](../Page/塞尔维亚牧首.md "wikilink")（Patriarch of
        Serbia）（领导[塞尔维亚](../Page/塞尔维亚.md "wikilink")（以及原[南斯拉夫](../Page/南斯拉夫.md "wikilink")）的[塞尔维亚正教会Serbian](../Page/塞尔维亚正教会.md "wikilink")
        Orthodox Church），1375年被确认为牧首区\[4\]
      - [莫斯科和全俄罗斯牧首](../Page/莫斯科都主教与牧首列表.md "wikilink")（Patriarch of
        Moscow and All
        Russia）（领导[俄罗斯的](../Page/俄罗斯.md "wikilink")[俄罗斯正教会](../Page/俄罗斯正教会.md "wikilink")），1589年被确认为牧首区\[5\]
      - [全罗马尼亚牧首](../Page/全罗马尼亚牧首列表.md "wikilink")（Patriarch of All
        Romania）（领导[罗马尼亚的](../Page/罗马尼亚.md "wikilink")[罗马尼亚正教会](../Page/罗马尼亚正教会.md "wikilink")），1925年被确认为牧首区\[6\]

## 正教教团之外的东方牧首

  - [莫斯科和全俄罗斯牧首](../Page/俄罗斯古正教会.md "wikilink")（Patriarch of Moscow and
    All Russia）（领导[俄罗斯古正教会Russian](../Page/俄罗斯古正教会.md "wikilink")
    Old-Orthodox Church）
  - [基辅牧首](../Page/乌克兰正教会·基辅宗主教圣统.md "wikilink")（Patriarch of
    Kiev）（领导[乌克兰正教会·基辅宗主教圣统Ukrainian](../Page/乌克兰正教会·基辅宗主教圣统.md "wikilink")
    Orthodox Church - Kiev Patriarchate）
  - [教会法定乌克兰独立正教会基辅和全罗斯](../Page/教会法定乌克兰独立正教会.md "wikilink")-乌克兰牧首（Patriarch
    of Kyiv and All Rus-Ukraine of the Ukrainian Autocephalous Orthodox
    Church Canonical）
  - [安提阿大公宗徒教会牧首](../Page/安提阿大公宗徒教会.md "wikilink")（Patriarch of the
    Catholic Apostolic Church of Antioch）
  - [美国正统大公教会牧首](../Page/美国正统大公教会.md "wikilink")（Patriarch of the
    American Orthodox Catholic Church）
  - [耶路撒冷会牧首](../Page/耶路撒冷会.md "wikilink")（Patriarch of the Assembly of
    Jerusalem）

## 东方正统教会宗主教

[Chuck_Kennedy_-_The_Official_White_House_Photostream_-_P060409CK-0199_(pd).jpg](https://zh.wikipedia.org/wiki/File:Chuck_Kennedy_-_The_Official_White_House_Photostream_-_P060409CK-0199_\(pd\).jpg "fig:Chuck_Kennedy_-_The_Official_White_House_Photostream_-_P060409CK-0199_(pd).jpg")（HH
Pope Shenouda
III），第117任亚历山大教宗和全非洲牧首，在[聖馬爾谷的主教宝座](../Page/聖馬爾谷.md "wikilink")（Apostolic
Throne of St Mark）上\]\]

  - [亚历山大教宗和全非洲牧首](../Page/亞歷山大科普特正教教宗列表.md "wikilink")（Pope of
    Alexandria and Patriarch of All
    Africa）（领导在[埃及](../Page/埃及.md "wikilink")、全[非洲的](../Page/非洲.md "wikilink")[亚历山大科普特正教会Coptic](../Page/亚历山大科普特正教会.md "wikilink")
    Orthodox Church of
    Alexandria，也是[东方正统教会Oriental](../Page/东方正统教会.md "wikilink")
    Orthodoxy的精神领袖）
  - [安提阿和全东方牧首](../Page/安提阿叙利亚正教牧首列表.md "wikilink")（Patriarch of Antioch
    and All the
    East）（[近东的](../Page/近东.md "wikilink")[安提阿叙利亚正教会Syriac](../Page/叙利亚正教会.md "wikilink")
    Orthodox Church of
    Antioch的领袖，以及[普世叙利亚正教会至高领袖Supreme](../Page/叙利亚正教会.md "wikilink")
    Leader of the Universal Syriac Orthodox Churc）
      - [印度大公](../Page/玛兰卡雅格派叙利亚正教会.md "wikilink")（Catholicos of
        India）（领导[印度的](../Page/印度.md "wikilink")[玛兰卡雅格派叙利亚正教会Malankara](../Page/玛兰卡雅格派叙利亚正教会.md "wikilink")
        Jacobite Syriac Orthodox Church）
  - [埃奇米亚金，亚美尼亚和全亚美尼亚人大公](../Page/亚美尼亚大公列表.md "wikilink")（Catholicos of
    Etchmiadzin, Armenia and of All
    Armenians）和[亚美尼亚宗徒教会至高牧首](../Page/亚美尼亚宗徒教会.md "wikilink")（Supreme
    Patriarch of the Armenian Apostolic
    Church）（领导[亚美尼亚宗徒教会Armenian](../Page/亚美尼亚宗徒教会.md "wikilink")
    Apostolic Church）
      - [西利西亚大公](../Page/西利西亚大公.md "wikilink")（Catholicos of
        Cilicia）（领导[山省Antelias](../Page/山省.md "wikilink")，[黎巴嫩以及](../Page/黎巴嫩.md "wikilink")[中东的](../Page/中东.md "wikilink")[西利西亚家族亚美尼亚宗徒教会](../Page/亚美尼亚宗徒教会.md "wikilink")（Armenian
        Apostolic Church of the House of Cilicia）
      - [亚美尼亚人的君士坦丁堡牧首](../Page/君士坦丁堡亚美尼亚牧首列表.md "wikilink")（Patriarch
        of Constantinople for the
        Armenians）（在[土耳其](../Page/土耳其.md "wikilink")）
      - [亚美尼亚人的耶路撒冷和圣锡安牧首](../Page/耶路撒冷亚美尼亚牧首列表.md "wikilink")（Patriarch
        of Jerusalem and of Holy Zion for the
        Armenian）（在[以色列](../Page/以色列.md "wikilink")，[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")，[约旦](../Page/约旦.md "wikilink")，和[波斯湾](../Page/波斯湾.md "wikilink")）
  - [东方大公](../Page/东方大公.md "wikilink")（Catholicos of the
    East）（领导[印度的](../Page/印度.md "wikilink")[印度正教会Indian](../Page/印度正教会.md "wikilink")
    Orthodox Church）
  - [阿克苏姆总主教和全埃塞额比亚大公牧首](../Page/埃塞俄比亚阿布纳斯列表.md "wikilink")（Archbishop
    of Axum and Patriarch Catholicos of All
    Ethiopia）（领导[埃塞俄比亚的](../Page/埃塞俄比亚.md "wikilink")[埃塞俄比亚正统台瓦西多教会Ethiopian](../Page/埃塞俄比亚正统台瓦西多教会.md "wikilink")
    Orthodox Tewahedo Church）
  - [阿斯马拉总主教和全厄立特里亚牧首](../Page/厄立特里亚牧首列表.md "wikilink")（Archbishop of
    Asmara and Patriarch of All
    Eritrea）（领导[厄立特里亚的](../Page/厄立特里亚.md "wikilink")[厄立特里亚正统台瓦西多教会Eritrean](../Page/厄立特里亚正统台瓦西多教会.md "wikilink")
    Orthodox Tewahedo Church）

## 东方教会的牧首

  - [塞琉西亞-泰西封大公牧首](../Page/巴比倫牧首列表.md "wikilink")（Catholicos-Patriarch
    of
    Seleucia-Ctesiphon）（[近東地區](../Page/近東.md "wikilink")[東方亞述教會Assyrian](../Page/東方亞述教會.md "wikilink")
    Church of the East的領袖）

## 东方古代教会的牧首

  - 大公牧首（Catholicos-Patriarch）（[近東地區](../Page/近東.md "wikilink")[东方古代教会Ancient](../Page/东方古代教会.md "wikilink")
    Church of the
    East的领袖。该教会于1964年由[东方亚述教会分出](../Page/东方亚述教会.md "wikilink")）

## 天主教會的宗主教

作為[五大宗主教區之一](../Page/五大宗主教區.md "wikilink")，教宗的羅馬宗主教區是唯一一個[西羅馬帝國內的宗主教區](../Page/西羅馬帝國.md "wikilink")。粗略地說，它與如今[拉丁禮領土之界限相同](../Page/拉丁禮.md "wikilink")。在過去，教宗使用“[西方宗主教](../Page/西方宗主教.md "wikilink")”（Patriarch
of the
West）這一稱號。但是，該頭銜於2006年被從[教廷頒布的有關出版物中去掉了](../Page/教廷.md "wikilink")。
\[7\]

### 拉丁禮宗主教

  - [拉丁禮耶路撒冷宗主教](../Page/拉丁禮耶路撒冷宗主教列表.md "wikilink")（Latin Patriarch of
    Jerusalem）
  - [威尼斯宗主教](../Page/威尼斯宗主教列表.md "wikilink")（Patriarch of Venice）
  - [里斯本宗主教](../Page/里斯本宗主教列表.md "wikilink")（Patriarch of Lisbon）
  - [東印度宗主教](../Page/東印度宗主教列表.md "wikilink")（Patriarch of the East
    Indies）是一個領銜宗主教位，兼[果阿和達曼總主教](../Page/果阿和達曼總主教.md "wikilink")（Archbishop
    of Goa and Daman）

### 東方禮天主教會宗主教

[BISHOP-gregorio-III-laham.JPG](https://zh.wikipedia.org/wiki/File:BISHOP-gregorio-III-laham.JPG "fig:BISHOP-gregorio-III-laham.JPG")（Gregory
III Laham），屬[默基特希臘禮天主教會](../Page/默基特希臘禮天主教會.md "wikilink")（Melkite Greek
Catholic Church）\]\]

**[東儀天主教會](../Page/東儀天主教會.md "wikilink")**或稱**東方禮天主教會**的宗主教承認[教宗在教會中的最高地位](../Page/教宗.md "wikilink")，但擁有遠比一般天主教主教大的自治權。他們的地位大致相當於[樞機](../Page/樞機.md "wikilink")。在21世紀，東儀天主教會在東方管理著天主教東方宗主教轄區。這些宗主教分別是：

  - [科普特禮天主教亞歷山大宗主教](../Page/科普特禮天主教亞歷山大宗主教列表.md "wikilink")（Coptic
    Catholic Patriarch of
    Alexandria）領導[科普特禮天主教會](../Page/科普特禮天主教會.md "wikilink")（Coptic
    Catholic Church）\[8\]
  - [敘利亞禮天主教安提阿宗主教](../Page/敘利亞禮天主教安提阿宗主教列表.md "wikilink")（Syrian
    Catholic Patriarch of
    Antioch）領導[敘利亞禮天主教會](../Page/敘利亞禮天主教會.md "wikilink")（Syrian
    Catholic Church）\[9\]
  - [默基特希臘禮天主教安提阿宗主教](../Page/默基特希臘禮天主教安提阿宗主教列表.md "wikilink")（Melkite
    Greek Catholic Patriarch of
    Antioch）領導[默基特希臘禮天主教會](../Page/默基特希臘禮天主教會.md "wikilink")（
    Melkite Greek Catholic
    Church）\[10\]該職兼兩個領銜宗主教位，它們都在[五大宗主教區中東部](../Page/五大宗主教區.md "wikilink")（Middle
    Eastern Pentarchy）：
      - [默基特禮天主教亞歷山大宗主教](../Page/默基特希臘禮天主教會.md "wikilink")（Melkite
        Catholic Patriarch of
        Alexandria）（在[埃及](../Page/埃及.md "wikilink")）
      - [默基特禮天主教耶路撒冷宗主教](../Page/默基特希臘禮天主教會.md "wikilink")（Melkite
        Catholic Patriarchs of
        Jerusalem）（在[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")/[以色列](../Page/以色列.md "wikilink")）
  - [馬龍尼禮安提阿宗主教](../Page/馬龍尼禮安提阿宗主教列表.md "wikilink")（Maronite Patriarch
    of Antioch）領導[馬龍尼禮教會](../Page/馬龍尼禮教會.md "wikilink")（Maronite
    Church）\[11\]
  - [加色丁禮天主教巴比倫宗主教](../Page/加色丁禮天主教巴比倫宗主教列表.md "wikilink")（Chaldean
    Catholic Patriarch of
    Babylonia）領導[加色丁禮天主教會](../Page/加色丁禮天主教會.md "wikilink")（Chaldean
    Catholic Church）\[12\]
  - [亞美尼亞禮天主教西利西亞宗主教](../Page/亞美尼亞禮天主教西利西亞宗主教列表.md "wikilink")（Arm​​enian
    Catholic Patriarch of
    Cilicia）領導[亞美尼亞禮天主教會](../Page/亞美尼亞禮天主教會.md "wikilink")（Arm​​enian
    Catholic Church）\[13\]

### 歷史上羅馬天主教會的宗主教

[西方宗主教](../Page/西方宗主教.md "wikilink")（Patriarch of the
West）傳統上稱[羅馬主教即](../Page/羅馬主教.md "wikilink")[教宗為西方宗主教](../Page/教宗.md "wikilink")，但教宗[本篤十六世於](../Page/本篤十六世.md "wikilink")2006年廢除此稱號

  - [西印度宗主教](../Page/西印度宗主教列表.md "wikilink")（Patriarch of the West
    Indies）一個領銜宗主教位，自1963年起懸空
  - [拉丁禮安提阿宗主教](../Page/拉丁禮安提阿宗主教列表.md "wikilink")（Latin Patriarch of
    Antioch）1964年廢除
  - [拉丁禮亞歷山大宗主教](../Page/拉丁禮亞歷山大宗主教列表.md "wikilink")（Latin Patriarch of
    Alexandria）1964年廢除
  - [阿奎萊亞宗主教](../Page/拉丁禮阿奎萊亞宗主教列表.md "wikilink")（Latin Patriarch of
    Aquileia）1752年解除
  - [迦太基宗主教](../Page/拉丁禮迦太基宗主教列表.md "wikilink")（Latin Patriarch of
    Carthage）7世紀後主要是領銜
  - [君士坦丁堡宗主教](../Page/拉丁禮君士坦丁堡宗主教列表.md "wikilink")（Latin Patriarch of
    Constantinople）1964年廢除
  - [格雷多宗主教](../Page/格雷多宗主教宗主教列表.md "wikilink")（Patriarch of
    Grado）1451年與“卡斯泰洛和威尼斯主教”（Bishopric of Castello and
    Venice）合併組成[天主教威尼斯總教區](../Page/天主教威尼斯總教區.md "wikilink")

## 其他基督宗教宗主教

  - [巴西宗徒国家教会宗主教](../Page/巴西宗徒国家教会.md "wikilink")（The Patriarch of the
    Catholic Apostolic National Church of Brazil）
  - 国际[神恩主教教会团宗主教](../Page/神恩主教教会.md "wikilink")（The Patriarch of the
    International Communion of the Charismatic Episcopal Church）

## 耶稣基督后期圣徒教会（LDS Church）

据[耶稣基督后期圣徒教会](../Page/耶稣基督后期圣徒教会.md "wikilink")（The Church of Jesus
Christ of Latter-day
Saints）称，[patriarch](../Page/教长_\(摩门教\).md "wikilink")（教长）是在[麦基洗德祭司](../Page/麦基洗德祭司.md "wikilink")（Melchizedek
Priesthood）中已被任命为教长职者。该词被视为与[evangelist](../Page/布道员_\(摩门教\).md "wikilink")（布道员）为同义词。教长的一个基本责任是给予[教长祝福](../Page/教长祝福.md "wikilink")（Patriarchal
blessing），一如[旧约中](../Page/旧约.md "wikilink")[雅各](../Page/雅各.md "wikilink")（Jacob）为其十二个儿子所做的一样。教长通常被分配到每个[教区](../Page/教区_\(摩门教\).md "wikilink")（stake）并终身保有该头衔。

## 参考文献

{{-}}

[牧首](../Category/牧首.md "wikilink")
[Category:基督教頭銜](../Category/基督教頭銜.md "wikilink")
[Category:东仪天主教会头衔](../Category/东仪天主教会头衔.md "wikilink")

1.
2.  [Catholic Near East Welfare Association, a Papal agency for
    humanitarian and pastoral
    support](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=20&IndexView=toc)
    .
3.  [Catholic Near East Welfare Association, a Papal agency for
    humanitarian and pastoral
    support](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=21&IndexView=toc)
    .
4.  [Catholic Near East Welfare Association, a Papal agency for
    humanitarian and pastoral
    support](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=18&IndexView=toc)
    .
5.  [Catholic Near East Welfare Association, a Papal agency for
    humanitarian and pastoral
    support](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=17&IndexView=toc)
    .
6.  [Catholic Near East Welfare Association, a Papal agency for
    humanitarian and pastoral
    support](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=19&IndexView=toc)
    .
7.
8.  Ronald Roberson, CSP (2006). ["The Coptic Catholic
    Church"](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=63&IndexView=toc),
    ''The Eastern Christian Churches – A Brief Survey (6th edition) ''.
9.  \[http:
    //www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=65\&IndexView=toc "The
    Syrian Catholic Church"\], *Ibid*
10. ["The Melkite Catholic
    Church"](http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=68&IndexView=toc),
    *Ibid*
11. [www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=56\&IndexView=toc "The
    Maronite Catholic Church"](http://), *Ibid*
12. \[
    <http://www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=59&IndexView=toc>
    "The Chaldean Catholic Church"\], *Ibid*
13. [www.cnewa.org/ecc-bodypg-us.aspx?eccpageID=59\&IndexView=toc "The
    Armenian Catholic Church"](http://), *Ibid*