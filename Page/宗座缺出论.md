**宗座缺出论**（，由[拉丁语](../Page/拉丁语.md "wikilink")
而来，意即[宗座從缺](../Page/宗座從缺.md "wikilink")）是一种不承认召集[第二次梵蒂冈大公会议的](../Page/第二次梵蒂冈大公会议.md "wikilink")[教宗](../Page/教宗.md "wikilink")[若望二十三世以来](../Page/若望二十三世.md "wikilink")，在[梵蒂冈领导](../Page/梵蒂冈.md "wikilink")[天主教會之诸位教宗的理论](../Page/天主教會.md "wikilink")。「[宗座從缺](../Page/宗座從缺.md "wikilink")」是[天主教用语](../Page/天主教.md "wikilink")，指一位教宗逝世或[辭職後](../Page/教宗辭職.md "wikilink")，在[教宗选举选出继任者之前教宗职位的空缺状态](../Page/教宗选举.md "wikilink")。宗座缺出论者宣称，由于教会中[异端滋生](../Page/异端.md "wikilink")，现今领导[教廷的教宗并非合法教宗](../Page/教廷.md "wikilink")，自1958年[庇护十二世逝世之后](../Page/庇护十二世.md "wikilink")，该职位就一直空缺。

总的来说，宗座缺出论者视自己为\[1\]\[2\]，他们反对第二次梵蒂冈大公会议所推行的改革，以及允许用当地语言的[弥撒仪式取代传统的使用](../Page/弥撒.md "wikilink")[拉丁语的](../Page/拉丁语.md "wikilink")[脱利腾弥撒](../Page/脱利腾弥撒.md "wikilink")。他们认为，在20世纪后半葉执掌梵蒂冈的人，推行了此类变革，因而都是[异端](../Page/异端.md "wikilink")。另一些传统人士则承认自[庇护十二世起的所有教宗](../Page/庇护十二世.md "wikilink")，尽管他们个人采信了许多无耻的异教观念，还仍然是真正的教宗，因为他们都没有动用他们的[教宗不能错误性](../Page/教宗无误论.md "wikilink")（这种权利的动用十分罕见）来宣扬异端邪说，而所有的天主徒也都认为教宗不可能这么做。

宗座缺出论者的观点部分基于[第一次梵蒂冈大公会议发布的](../Page/第一次梵蒂冈大公会议.md "wikilink")[教宗不能错误性决议](../Page/教宗无误论.md "wikilink")。他们的理论是如果教宗宣扬异端，那么他就不具备不能错误性，因而就称不上是教宗了。另一方面，如果某个教宗接受了异端，即使没有公开地承認，其地位亦將受到質疑。

一些宗座缺出论者团体会推选出一位自己的教宗取替他们反对的教宗，在他们看来这终结了[宗座从缺](../Page/宗座从缺.md "wikilink")，这些新教宗被普遍认同为[对立教宗](../Page/对立教宗.md "wikilink")\[3\]。

## 参考文献

{{-}}

[Category:圣座](../Category/圣座.md "wikilink")
[Category:天主教相关争议](../Category/天主教相关争议.md "wikilink")

1.  .
2.  .
3.  [George D. Chryssides, *Historical Dictionary of New Religious
    Movements* (Rowman & Littlefield 2011 978-0-81087967-6), p.
    99](https://books.google.com/books?id=LgvvaMgTsZ0C&pg=PT75&dq=%22Victor+von+Pentz%22&hl=en&sa=X&ei=2FSPUbfuF-bD7AbczYH4BA&redir_esc=y)