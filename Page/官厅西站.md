**官厅西站**（**Guanting West Railway
Station**）是[丰沙铁路上的一个小火车站](../Page/丰沙铁路.md "wikilink")。该站位于[河北省](../Page/河北省.md "wikilink")[张家口市](../Page/张家口市.md "wikilink")[怀来县](../Page/怀来县.md "wikilink")[官厅镇境内](../Page/官厅镇_\(怀来县\).md "wikilink")，距离[北京站](../Page/北京站.md "wikilink")102[公里](../Page/公里.md "wikilink")，[沙城站](../Page/沙城站.md "wikilink")19公里。车站等级为四等站，建于1972年，为下行车站。

## 参见

  - [丰沙铁路车站列表](../Page/丰沙铁路车站列表.md "wikilink")

## 邻近车站

## 参考资料

[Category:张家口市铁路车站](../Category/张家口市铁路车站.md "wikilink")
[Category:怀来县](../Category/怀来县.md "wikilink")
[Category:北京铁路局](../Category/北京铁路局.md "wikilink")
[Category:中国铁路四等站](../Category/中国铁路四等站.md "wikilink")
[Category:1972年启用的铁路车站](../Category/1972年启用的铁路车站.md "wikilink")