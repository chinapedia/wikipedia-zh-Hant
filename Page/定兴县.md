**定兴县**在[河北省中部](../Page/河北省.md "wikilink")，[南拒马河](../Page/南拒马河.md "wikilink")、[易水河等河流经境内](../Page/易水河.md "wikilink")，是[保定市下辖的一个县](../Page/保定.md "wikilink")。

县政府驻[定兴镇通兴东路](../Page/定兴镇.md "wikilink")120号。

## 历史沿革

[金](../Page/金朝.md "wikilink")[大定六年以](../Page/大定.md "wikilink")[范阳县黄村置](../Page/涿州.md "wikilink")，属[涿州](../Page/涿州.md "wikilink")；[元属](../Page/元朝.md "wikilink")[保定路](../Page/保定路.md "wikilink")[易州](../Page/易州.md "wikilink")；[明](../Page/明朝.md "wikilink")[清均属](../Page/清朝.md "wikilink")[保定府](../Page/保定府.md "wikilink")；现属[河北省](../Page/河北省.md "wikilink")[保定市](../Page/保定市.md "wikilink")。

## 行政区划

下辖7个[镇](../Page/行政建制镇.md "wikilink")、9个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 參考資料

  - [河北定興縣努力實現從文化大縣向文化名縣的突破](https://web.archive.org/web/20160304131511/http://nongye.ce.cn/2012/dfkb_0530/4659.html)

## 官方网站

  - [定兴县人民政府](http://www.dingxing.gov.cn/)

[定兴县](../Page/category:定兴县.md "wikilink")
[县](../Page/category:保定区县市.md "wikilink")
[保定](../Page/category:河北省县份.md "wikilink")