在[經典力學裏](../Page/經典力學.md "wikilink")，如果一個系統的所有[約束都是](../Page/約束.md "wikilink")**定常約束**（scleronomous
constraint），則稱此系統為**定常系統**（scleronomous
system）。定常約束顯性地不含時間。假若約束顯性地含時間，則稱此約束為[非定常約束](../Page/非定常系統.md "wikilink")。

## 應用

  -
    主要項目：[廣義速度](../Page/廣義速度.md "wikilink")

在三維空間裏，一個質量為\(m\)、速度為\(\mathbf{v}\)的粒子的[動能是](../Page/動能.md "wikilink")

\[T =\frac{1}{2}m v^2\]。

速度是位置\(\mathbf{r}\)對於時間\(t\)的導數。應用[偏微分連鎖律](../Page/複合函數求導法則.md "wikilink")，可以得到

\[\mathbf{v}=\frac{d\mathbf{r}}{dt}=\sum_i\ \frac{\partial \mathbf{r}}{dq_i}\dot{q}_i+\frac{\partial \mathbf{r}}{dt}\]；

其中，\(q_i\)是第\(i\)個廣義坐標，\(\dot{q}_i\)是對應的廣義速度。

所以，

\[T =\frac{1}{2}m\sum_i\ \left(\frac{\partial \mathbf{r}}{\partial q_i}\dot{q}_i+\frac{\partial \mathbf{r}}{\partial t}\right)^2\]。

將方程式展開\[1\]，動能可以分為三個項目表示：

\[T =T_0+T_1+T_2\]；

其中，

\[T_0=\frac{1}{2}m\left(\frac{\partial \mathbf{r}}{\partial t}\right)^2\]，

\[T_1=\sum_i\ m\frac{\partial \mathbf{r}}{\partial t}\cdot \frac{\partial \mathbf{r}}{\partial q_i}\dot{q}_i\]，

\[T_2=\sum_{i,j}\ \frac{1}{2}m\frac{\partial \mathbf{r}}{\partial q_i}\cdot \frac{\partial \mathbf{r}}{\partial q_j}\dot{q}_i\dot{q}_j,\!\]。

\(T_0\)、\(T_1\)、\(T_2\)分別為廣義速度\(\dot{q}_i\)的0次、1次、2次[齊次函數](../Page/齊次函數.md "wikilink")。如果這系統是定常系統，位置不顯性地含時間，\(\frac{\partial \mathbf{r}}{\partial t}=0\)，則只有\(T_2\)不等於零。所以，\(T =T_2\)，動能是廣義速度的2次齊次函數。

## 實例1：單擺

[SimplePendulum01.svg](https://zh.wikipedia.org/wiki/File:SimplePendulum01.svg "fig:SimplePendulum01.svg")
如右圖所示，[單擺是由一個擺錘與一條繩子組成的簡單機械](../Page/單擺.md "wikilink")；繩子的上端固定，下端繫著擺錘。由於這繩子是無法伸縮的，繩子的長度是常數。所以，這系統是定常系統；它遵守定常約束

  -
    \(\sqrt{x^2+y^2} - L=0\)；

其中，\((x,\ y)\)是擺錘的位置，\(L\)是擺長。

## 實例2：受驅擺

[Pendulum02.JPG](https://zh.wikipedia.org/wiki/File:Pendulum02.JPG "fig:Pendulum02.JPG")
參考右圖，假設一個單擺的繩子上端受到[簡諧運動的驅動](../Page/簡諧運動.md "wikilink")：

\[x_t=x_0\cos\omega t\]；
這裏，\(x_0\)是[振幅](../Page/振幅.md "wikilink")，\(\omega\)是[角頻率](../Page/角頻率.md "wikilink")，\(t\)是[時間](../Page/時間.md "wikilink")。

由於無法伸縮繩子的長度是常數，擺錘與繩子上端的直線距離保持不變。但是，因為單擺的繩子上端受到[簡諧運動的驅動](../Page/簡諧運動.md "wikilink")，這個受驅擺系統是非定常系統；它遵守非定常約束

\[\sqrt{(x - x_0\cos\omega t)^2+y^2} - L=0\]。

## 參閱

  -
    [拉格朗日力學](../Page/拉格朗日力學.md "wikilink")
    [完整系統](../Page/完整系統.md "wikilink")
    [非定常系統](../Page/非定常系統.md "wikilink")
    [單演系統](../Page/單演系統.md "wikilink")
    [保守系統](../Page/保守系統.md "wikilink")

## 參考文獻

[de:Skleronom](../Page/de:Skleronom.md "wikilink")

[D](../Category/力學.md "wikilink") [D](../Category/經典力學.md "wikilink")
[D](../Category/拉格朗日力學.md "wikilink")
[D](../Category/哈密頓力學.md "wikilink")
[Category:物理学系统](../Category/物理学系统.md "wikilink")

1.