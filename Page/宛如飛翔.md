是[NHK於](../Page/NHK.md "wikilink")1990年1月7日～12月9日播出的第二十八部[大河劇](../Page/NHK大河劇.md "wikilink")。以[薩摩藩為中心](../Page/薩摩藩.md "wikilink")，積極描寫從幕末到明治維新時的歷史故事，改編自[司馬遼太郎的同名小說](../Page/司馬遼太郎.md "wikilink")。

故事裡，[西鄉隆盛與](../Page/西鄉隆盛.md "wikilink")[大久保利通兩人在](../Page/大久保利通.md "wikilink")[島津齊彬的庇護之下嶄露頭角](../Page/島津齊彬.md "wikilink")，撼動了薩摩藩。憑著兩人的力量，不但結束了江戶幕府，並且一同參與[明治維新](../Page/明治維新.md "wikilink")，但在新政府內，兩人的意見卻互相不同。最後西鄉下野並於[西南戰爭時戰死](../Page/西南戰爭.md "wikilink")，大久保則遭到盜賊襲擊而殞命。原作《宛如飛翔》是一部描寫由征韓論爭到西南戰爭時期的作品，本劇的第二部就是這個部份。而電視劇第一部則是編劇小山內美江子由《宛如飛翔》的部份內容再參照同為司馬遼太郎原作的《[龍馬來了](../Page/龍馬來了.md "wikilink")》與《[最後的將軍](../Page/最後的將軍.md "wikilink")》情節所綜合編成的劇本。第一部、第二部的旁白都使用鹿兒島方言。　

與前三年的作品比起來，本劇收視率略差一點；但是以[幕末](../Page/幕末.md "wikilink")[維新的題材而言](../Page/明治維新.md "wikilink")，本劇的收視率可謂相當不錯。由於本劇的內容並不輕薄，因此得到了很高的評價。

## 工作人員

  - 原作・・・[司馬遼太郎](../Page/司馬遼太郎.md "wikilink")
  - 劇本・・・[小山內美江子](../Page/小山內美江子.md "wikilink")
  - 音樂・・・[一柳慧](../Page/一柳慧.md "wikilink")
  - 演奏・・・東京交響樂團
  - 主題音樂演奏・・・NHK交響樂團
  - 主題音樂指揮・・・秋山和慶
  - 題字・・・司馬遼太郎
  - 協力・・・[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")
  - 監修・・・小西四郎
  - 時代考証・・・原口泉
  - 風俗考証・・・小野一成
  - 衣裝考証・・・小泉清子
  - 所作指導・・・猿若清方
  - 殺陣・・・林邦史朗
  - 製作・・・吉村文孝
  - 導演・・・平山武之、望月良雄、木田幸紀、小松隆一、菅康弘
  - 旁白・・・草野大悟（第一部）、田中裕子（第二部）

## 出演

### 西鄉家・大久保家

  - [西鄉隆盛](../Page/西鄉隆盛.md "wikilink")：[西田敏行](../Page/西田敏行.md "wikilink")
  - [大久保利通](../Page/大久保利通.md "wikilink")：[鹿賀丈史](../Page/鹿賀丈史.md "wikilink")
  - 西鄉吉兵衛：[坂上二郎](../Page/坂上二郎.md "wikilink")　（隆盛之父）
  - 西鄉龍右衛門：[濱村純](../Page/濱村純.md "wikilink")　（隆盛祖父）
  - 西鄉政：[富士真奈美](../Page/富士真奈美.md "wikilink")　（隆盛之母）
  - 西鄉君：[大路三千緒](../Page/大路三千緒.md "wikilink")　（隆盛祖母）
  - 西鄉糸：[田中裕子](../Page/田中裕子.md "wikilink")　（隆盛正室・繼妻）
  - 西鄉俊：[南果步](../Page/南果步.md "wikilink")　（隆盛正室・前妻）
  - 愛加那：[石田惠理](../Page/石田惠理.md "wikilink")　（隆盛側室）
  - 西鄉吉二郎：[村田雄浩](../Page/村田雄浩.md "wikilink")　（隆盛之弟）
  - 西鄉小兵衛：[武田佑介](../Page/武田佑介.md "wikilink")→[岩下謙人](../Page/岩下謙人.md "wikilink")→[金山一彥](../Page/金山一彥.md "wikilink")　（隆盛之弟）
  - [西鄉從道](../Page/西鄉從道.md "wikilink")：[根本卓哉](../Page/根本卓哉.md "wikilink")→[高橋守](../Page/高橋守.md "wikilink")→[星孝行](../Page/星孝行.md "wikilink")→[緒形直人](../Page/緒形直人.md "wikilink")　（隆盛之弟）
  - 西鄉琴：[酒井法子](../Page/酒井法子.md "wikilink")　（隆盛之妹）
  - 西鄉清：[國生小百合](../Page/國生小百合.md "wikilink")　（從道之妻）
  - 西鄉園：[星野博美](../Page/星野博美.md "wikilink")
  - 西鄉松：[長谷川真弓](../Page/長谷川真弓.md "wikilink")
  - [西鄉菊次郎](../Page/西鄉菊次郎.md "wikilink")：[若菜大輔](../Page/若菜大輔.md "wikilink")→[六浦誠](../Page/六浦誠.md "wikilink")
  - 西鄉菊子：[茅野佐智恵](../Page/茅野佐智恵.md "wikilink")
  - 西鄉須磨：[奧田圭子](../Page/奧田圭子.md "wikilink")
  - 西鄉美津：[八木澤一惠](../Page/八木澤一惠.md "wikilink")→[廣瀨珠美](../Page/廣瀨珠美.md "wikilink")
  - 西鄉勇袈裟：[早津翔太](../Page/早津翔太.md "wikilink")→[長谷川步](../Page/長谷川步.md "wikilink")
  - 西鄉午次郎：[高橋壹岐](../Page/高橋壹岐.md "wikilink")
  - 西鄉隆：[近藤繪麻](../Page/近藤繪麻.md "wikilink")
  - 西鄉安：[田京惠](../Page/田京惠.md "wikilink")
  - 大久保福：[八木昌子](../Page/八木昌子.md "wikilink")　（利通之母）
  - 大久保利世：[北村和夫](../Page/北村和夫.md "wikilink")　（利通之父）
  - 大久保滿壽：[賀來千香子](../Page/賀來千香子.md "wikilink")　（利通之妻）
  - 大久保喜智：[吉川十和子](../Page/吉川十和子.md "wikilink")　（利通之妹）
  - 大久保伸熊（[牧野伸顯](../Page/牧野伸顯.md "wikilink")）：[大西良和](../Page/大西良和.md "wikilink")→[三浦龍也](../Page/三浦龍也.md "wikilink")
  - 大久保須磨：[矢澤美紀](../Page/矢澤美紀.md "wikilink")
  - 大久保理代：[桂川冬子](../Page/桂川冬子.md "wikilink")

### 島津一門・家臣

  - [島津齊興](../Page/島津齊興.md "wikilink")：[江見俊太郎](../Page/江見俊太郎.md "wikilink")　（薩摩藩第10代藩主）
  - [島津齊彬](../Page/島津齊彬.md "wikilink")：[加山雄三](../Page/加山雄三.md "wikilink")　（薩摩藩11代藩主）
  - [島津久光](../Page/島津久光.md "wikilink")：[高橋英樹](../Page/高橋英樹_\(演員\).md "wikilink")　（齊彬庶弟）
  - [島津忠義](../Page/島津忠義.md "wikilink")：[藤原秀樹](../Page/藤原秀樹.md "wikilink")→[川名康浩](../Page/川名康浩.md "wikilink")　（久光長男）
  - 島津敬子・篤姬（[天璋院](../Page/天璋院.md "wikilink")）：[富司純子](../Page/富司純子.md "wikilink")　（島津齊彬養女・德川家定正室）
  - 幾島：[樹木希林](../Page/樹木希林.md "wikilink")　（篤姬的侍女）
  - 阿由羅：[草笛光子](../Page/草笛光子.md "wikilink")　（[島津齊興的側室](../Page/島津齊興.md "wikilink")）
  - 喜久：[田中好子](../Page/田中好子.md "wikilink")　
    （[島津齊彬的側室](../Page/島津齊彬.md "wikilink")）
  - [小松帶刀](../Page/小松帶刀.md "wikilink")：[大橋吾郎](../Page/大橋吾郎.md "wikilink")　（薩摩藩・家老）
  - [大山綱良](../Page/大山綱良.md "wikilink")：[蟹江敬三](../Page/蟹江敬三.md "wikilink")　（鹿兒島縣的大參事、縣令）
  - [大山巖](../Page/大山巖.md "wikilink")：[坂上忍](../Page/坂上忍.md "wikilink")　（西鄉隆盛的從兄）
  - [村田新八](../Page/村田新八.md "wikilink")：[益岡徹](../Page/益岡徹.md "wikilink")
  - [有馬新七](../Page/有馬新七.md "wikilink")：[内藤剛志](../Page/内藤剛志.md "wikilink")
  - [海江田信義](../Page/海江田信義.md "wikilink")：[佐野史郎](../Page/佐野史郎.md "wikilink")
  - [伊地知正治](../Page/伊地知正治.md "wikilink")：[安藤一夫](../Page/安藤一夫.md "wikilink")
  - [吉井友實](../Page/吉井友實.md "wikilink")：[福田勝洋](../Page/福田勝洋.md "wikilink")
  - [樺山三圓](../Page/樺山三圓.md "wikilink")：[吉岡祐一](../Page/吉岡祐一.md "wikilink")
  - [伊藤才藏](../Page/伊藤才藏.md "wikilink")：[草見潤平](../Page/草見潤平.md "wikilink")
  - [谷村愛之助](../Page/谷村愛之助.md "wikilink")：[潮哲也](../Page/潮哲也.md "wikilink")
  - [森山新藏](../Page/森山新藏.md "wikilink")：[東野英心](../Page/東野英心.md "wikilink")
  - [關勇助](../Page/關勇助.md "wikilink")：[坂口芳貞](../Page/坂口芳貞.md "wikilink")
  - [土持政照](../Page/土持政照.md "wikilink")：[光石研](../Page/光石研.md "wikilink")
  - [月照](../Page/月照.md "wikilink")：[野村萬之丞](../Page/野村萬之丞.md "wikilink")
  - [川路利良](../Page/川路利良.md "wikilink")：[鹽野谷正幸](../Page/鹽野谷正幸.md "wikilink")
  - [桐野利秋](../Page/桐野利秋.md "wikilink")：[杉本哲太](../Page/杉本哲太.md "wikilink")
  - [中原尚雄](../Page/中原尚雄.md "wikilink")：[渡邊一惠](../Page/渡邊一惠.md "wikilink")
  - [別府晉介](../Page/別府晉介.md "wikilink")：[黑田隆哉](../Page/黑田隆哉.md "wikilink")
  - [篠原國幹](../Page/篠原國幹.md "wikilink")：[西田靜志郎](../Page/西田靜志郎.md "wikilink")
  - [海老原穆](../Page/海老原穆.md "wikilink")：[草野大悟](../Page/草野大悟.md "wikilink")
  - 調所笑左衛門（[調所廣鄉](../Page/調所廣鄉.md "wikilink")）：[高品格](../Page/高品格.md "wikilink")

### 江戶幕府與相關人士

  - [德川家慶](../Page/德川家慶.md "wikilink")：[加藤治](../Page/加藤治.md "wikilink")　（江戶幕府第12代將軍）
  - [德川家定](../Page/德川家定.md "wikilink")：[上杉祥三](../Page/上杉祥三.md "wikilink")　（江戶幕府第13代將軍）
  - [德川家茂](../Page/德川家茂.md "wikilink")：[三宅喜章](../Page/三宅喜章.md "wikilink")→[小林正則](../Page/小林正則.md "wikilink")→[若菜孝史](../Page/若菜孝史.md "wikilink")　（江戶幕府第14代將軍）
  - [和宮](../Page/和宮.md "wikilink")：[鈴木京香](../Page/鈴木京香.md "wikilink")　（德川家茂正室）
  - [德川慶喜](../Page/德川慶喜.md "wikilink")：[三田村邦彥](../Page/三田村邦彥.md "wikilink")　（江戶幕府第15代將軍）
  - [井伊直弼](../Page/井伊直弼.md "wikilink")：[神山繁](../Page/神山繁.md "wikilink")　（江戶幕府大老）
  - [松平春嶽](../Page/松平春嶽.md "wikilink")：[磯部勉](../Page/磯部勉.md "wikilink")　（越前藩主）
  - [松平容保](../Page/松平容保.md "wikilink")：[若松武](../Page/若松武.md "wikilink")　（會津藩主）
  - [松平定敬](../Page/松平定敬.md "wikilink")：[真崎理](../Page/真崎理.md "wikilink")　（桑名藩主・松平容保兄弟）
  - [德川慶勝](../Page/德川慶勝.md "wikilink")：[板倉佳司](../Page/板倉佳司.md "wikilink")→[三上真一郎](../Page/三上真一郎.md "wikilink")　（尾張藩主・松平容保兄弟）
  - [德川慶篤](../Page/德川慶篤.md "wikilink")：[高野光平](../Page/高野光平.md "wikilink")　（水戶藩主）
  - [山内容堂](../Page/山内容堂.md "wikilink")：[嵐圭史](../Page/嵐圭史.md "wikilink")　（土佐藩主）
  - [伊達宗城](../Page/伊達宗城.md "wikilink")：[北村總一朗](../Page/北村總一朗.md "wikilink")　（宇和島藩主）
  - [淺野長勳](../Page/淺野長勳.md "wikilink")：[清水明彥](../Page/清水明彥.md "wikilink")　（廣島藩主）
  - [長野主膳](../Page/長野主膳.md "wikilink")：[伊藤孝雄](../Page/伊藤孝雄.md "wikilink")　（大老・井伊直弼之参謀）
  - [阿部正弘](../Page/阿部正弘.md "wikilink")：[若林豪](../Page/若林豪.md "wikilink")　（江戶幕府老中）
  - [堀田正睦](../Page/堀田正睦.md "wikilink")：[井上孝雄](../Page/井上孝雄.md "wikilink")　（江戶幕府老中）
  - [板倉勝靜](../Page/板倉勝靜.md "wikilink")：[津村鷹志](../Page/津村鷹志.md "wikilink")　（江戶幕府老中）
  - [水野忠精](../Page/水野忠精.md "wikilink")：[大林丈史](../Page/大林丈史.md "wikilink")　（江戶幕府老中）
  - [間部詮勝](../Page/間部詮勝.md "wikilink")：[石坂重二](../Page/石坂重二.md "wikilink")　（江戶幕府老中）
  - [勝海舟](../Page/勝海舟.md "wikilink")：[林隆三](../Page/林隆三.md "wikilink")　（江戶幕府陸軍總裁）
  - [川路聖謨](../Page/川路聖謨.md "wikilink")：[伏見哲夫](../Page/伏見哲夫.md "wikilink")　（江戶幕府勘定奉行）
  - [岩瀨忠震](../Page/岩瀨忠震.md "wikilink")：[酒井鄉博](../Page/酒井鄉博.md "wikilink")　（江戶幕府外國奉行）

### 各藩藩士與相關人士

  - [藤田東湖](../Page/藤田東湖.md "wikilink")：[大山克巳](../Page/大山克巳.md "wikilink")　（水戶藩士）
  - [安島帶刀](../Page/安島帶刀.md "wikilink")：[平井武](../Page/平井武.md "wikilink")　（水戶藩家老）
  - [橋本左内](../Page/橋本左内.md "wikilink")：[篠井英介](../Page/篠井英介.md "wikilink")　（越前藩士）
  - [坂本龍馬](../Page/坂本龍馬.md "wikilink")：[佐藤浩市](../Page/佐藤浩市.md "wikilink")　（土佐藩浪士）
  - [阿龍](../Page/阿龍.md "wikilink")：[洞口依子](../Page/洞口依子.md "wikilink")　（龍馬之妻）　
  - [中岡慎太郎](../Page/中岡慎太郎.md "wikilink")：[古山忠良](../Page/古山忠良.md "wikilink")　（土佐藩士）
  - [桂小五郎](../Page/桂小五郎.md "wikilink")（[木戶孝允](../Page/木戶孝允.md "wikilink")）：[田中健](../Page/田中健.md "wikilink")　（長州藩士）
  - [木戶松子](../Page/木戶松子.md "wikilink")（[幾松](../Page/幾松.md "wikilink")）：[景山仁美](../Page/景山仁美.md "wikilink")　（孝允之妻）
  - [品川彌二郎](../Page/品川彌二郎.md "wikilink")：[廣田高志](../Page/廣田高志.md "wikilink")　（長州藩士）
  - [大隈重信](../Page/大隈重信.md "wikilink")：[石丸謙二郎](../Page/石丸謙二郎.md "wikilink")　（佐賀藩士）
  - [板垣退助](../Page/板垣退助.md "wikilink")：[齊藤洋介](../Page/齊藤洋介.md "wikilink")　（土佐藩士）
  - [江藤新平](../Page/江藤新平.md "wikilink")：[隆大介](../Page/隆大介.md "wikilink")　（佐賀藩士）
  - [伊藤博文](../Page/伊藤博文.md "wikilink")：[小倉久寛](../Page/小倉久寛.md "wikilink")　（長州藩士）
  - [山縣有朋](../Page/山縣有朋.md "wikilink")：[新井康弘](../Page/新井康弘.md "wikilink")　（長州藩士）
  - [井上馨](../Page/井上馨.md "wikilink")：[長谷川初範](../Page/長谷川初範.md "wikilink")　（長州藩士）
  - [大村益次郎](../Page/大村益次郎.md "wikilink")：[平田滿](../Page/平田滿.md "wikilink")　（長州藩士）
  - [平野國臣](../Page/平野國臣.md "wikilink")：[野崎海太郎](../Page/野崎海太郎.md "wikilink")　（福岡藩士）
  - [副島種臣](../Page/副島種臣.md "wikilink")：[坂部文昭](../Page/坂部文昭.md "wikilink")　（佐賀藩士）
  - [大木喬任](../Page/大木喬任.md "wikilink")：[町田幸夫](../Page/町田幸夫.md "wikilink")　（佐賀藩士）
  - [兒玉源太郎](../Page/兒玉源太郎.md "wikilink")：[光岡湧太郎](../Page/光岡湧太郎.md "wikilink")　（長州藩士）
  - [后藤象二郎](../Page/后藤象二郎.md "wikilink")：[高橋幸兵](../Page/高橋幸兵.md "wikilink")　（土佐藩士）

### 朝廷相關人士

  - [岩倉具視](../Page/岩倉具視.md "wikilink")：[小林稔侍](../Page/小林稔侍.md "wikilink")
  - [三条實美](../Page/三条實美.md "wikilink")：[角野卓造](../Page/角野卓造.md "wikilink")
  - 中川宮（[久邇宮](../Page/久邇宮.md "wikilink")）：[三木敏彥](../Page/三木敏彥.md "wikilink")
  - [中山忠能](../Page/中山忠能.md "wikilink")：[松村彥次郎](../Page/松村彥次郎.md "wikilink")
  - [近衛忠凞](../Page/近衛忠凞.md "wikilink")：[柳生博](../Page/柳生博.md "wikilink")
  - [正親町三条實愛](../Page/正親町三条實愛.md "wikilink")：[沼田爆](../Page/沼田爆.md "wikilink")
  - [大原重德](../Page/大原重德.md "wikilink")：[庄司永建](../Page/庄司永建.md "wikilink")

### 其他

  - [新門辰五郎](../Page/新門辰五郎.md "wikilink")：[三木則平](../Page/三木則平.md "wikilink")
  - [約翰萬次郎](../Page/約翰萬次郎.md "wikilink")：[中西良太](../Page/中西良太.md "wikilink")
  - [恩斯特・薩道](../Page/恩斯特・薩道.md "wikilink")：[ゴダン・ジャンリュック](../Page/ゴダン・ジャンリュック.md "wikilink")
  - [湯森・赫禮士](../Page/湯森・赫禮士.md "wikilink")：[喬・葛雷伊斯](../Page/喬・葛雷伊斯.md "wikilink")
  - [亨利・休斯肯](../Page/亨利・休斯肯.md "wikilink")：[アンドレ・ケイザース](../Page/アンドレ・ケイザース.md "wikilink")
  - 阿房：[萬田久子](../Page/萬田久子.md "wikilink")
  - 龜：[飯田照子](../Page/飯田照子.md "wikilink")
  - 蘆名千繪：[有森也實](../Page/有森也實.md "wikilink")　（矢崎八郎太的戀人）
  - 蘆名千草：[南條玲子](../Page/南條玲子.md "wikilink")
  - 山城屋和助：[藤堂新二](../Page/藤堂新二.md "wikilink")
  - 梅乃家五郎八：[桂三木助](../Page/桂三木助.md "wikilink")
  - 川口雪篷：[龍雷太](../Page/龍雷太.md "wikilink")
  - 十藏：[奥村公延](../Page/奥村公延.md "wikilink")
  - 熊吉：[車丸吉](../Page/車丸吉.md "wikilink")
  - 金太：[段田安則](../Page/段田安則.md "wikilink")

＊矢崎八郎太(以[宮崎八郎做為原型](../Page/宮崎八郎.md "wikilink"))：[堤真一](../Page/堤真一.md "wikilink")

## 幕後花絮

  - 本作參考的原作很多，雖然主要是司馬遼太郎的《**宛如飛翔**》，但也參考了司馬其他的幕末作品，長篇小說如《[龍馬來了](../Page/龍馬來了.md "wikilink")》（坂本龍馬的故事）、《[花神](../Page/花神.md "wikilink")》（大村益次郎的故事），短篇小說如《[最後的將軍](../Page/最後的將軍.md "wikilink")》（德川慶喜的故事）、《[狐馬](../Page/狐馬.md "wikilink")》（島津久光的故事）、《[醉侯](../Page/醉侯.md "wikilink")》（山內容堂的故事），不過基本上還是劇作家[小山內美江子的原創作品](../Page/小山內美江子.md "wikilink")，比如原作裡面，沒有[川路利良](../Page/川路利良.md "wikilink")。

<!-- end list -->

  - 在原作裡面，西鄉與大久保等人的印象大多是沉默的薩摩人模樣，但劇作家小山內表示，沉默的人怎麼演電視劇？於是賦予了他們比較激情的台詞。這一點原作者司馬遼太郎是理解的。

<!-- end list -->

  - 第一部與第二部的旁白全部都使用鹿兒島方言（第一部的旁白者是鹿兒島出身）。演員只要是薩摩方的，都是說鹿兒島方言，並在劇中偶爾會對地方用語出現字幕解說。劇中使用的方言已經比較接近標準語了，實際的鹿兒島方言是更難的。

<!-- end list -->

  - 演出的主要演員都跟歷史人物相當接近，比如西田敏行跟當時有名的西鄉肖像畫很接近，且西田在演出時還特別增重，為了表現西鄉魁偉的身材，還在衣服裡增加了東西。另外，演出大久保的鹿賀，也被大久保的子孫稱其相當接近實像。

<!-- end list -->

  - 兩部的片頭不太一樣，第一部開頭是火山爆發，第二部是大海。

<!-- end list -->

  - 音樂採現代音樂，是大河劇中少見的風格，展現幕末到明治初期的渾沌感。

<!-- end list -->

  - 跟劇名一樣，裡面經常出現「與其哭泣，不如飛翔」（）的台詞，這是司馬遼太郎對薩摩人賦予的特殊印象。本作中的片頭，也是司馬本人的揮毫。

<!-- end list -->

  - 在2011年大河劇「[江～公主們的戰國～](../Page/江～公主們的戰國～.md "wikilink")」開播前，NHK製作過特別節目「看見大河劇50作」。裡面訪問到男主角西田敏行。西田敏行表示，本劇的特色是奔跑與跳躍，因為題目是飛翔，所以跳躍要像飛起來一樣。另外他表示，在選角時，很驚訝大久保可以找到像鹿賀這麼適合的人，而且在第二部御前會議的爭執中，鹿賀是含著淚水與他爭執的。他認為鹿賀相當到位。

## 總集編

1.  「青雲之志」（第一部・前編）
2.  「完成維新」（第一部・後編）
3.  「兩雄對決」 （第二部・前編）
4.  「往明日飛翔」（第二部・後編）

[Category:1990年日本電視劇集](../Category/1990年日本電視劇集.md "wikilink")
[Category:司馬遼太郎小說改編電視劇](../Category/司馬遼太郎小說改編電視劇.md "wikilink")
[Category:幕末背景電視劇](../Category/幕末背景電視劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:西鄉隆盛題材作品](../Category/西鄉隆盛題材作品.md "wikilink")
[Category:鹿兒島縣背景作品](../Category/鹿兒島縣背景作品.md "wikilink")