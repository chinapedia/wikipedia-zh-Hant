**宜昌市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖北省下辖的](../Page/湖北省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，湖北省政府确立的省域副中心城市，位于鄂西南，是省内首个被评为“[全国文明城市](../Page/全国文明城市.md "wikilink")”的城市。宜昌古称夷陵，扼[长江三峡东口](../Page/长江三峡.md "wikilink")，是[长江](../Page/长江.md "wikilink")[葛洲坝水电站和](../Page/葛洲坝水电站.md "wikilink")[三峡水电站的所在地](../Page/三峡水电站.md "wikilink")，长江上、中游分界地。宜昌被誉为世界水电之都，中国动力心脏，巴楚文化发源地。宜昌是[中国CFO论坛永久会址所在地](../Page/中国CFO论坛.md "wikilink")。\[1\]

## 历史

宜昌位于湖北省西部地区，地处长江中上游结合部，渝鄂湘三省市交汇地，为历代兵家必争之地，历史悠久，文化灿烂，有2400多年的历史。宜昌，古称西陵，是西陵部落分布的中心地区。距今约20万年前，清江流域就有“长阳人”在活动。二十纪50年代以来，在宜昌市点军区李家河、紫阳河一带发掘出新石器时代后期遗址，以及白庙子、清水滩、中堡岛、小溪口等数十处古遗址和文物，充分证明早在五、六千年前，我们的祖先就在这片土地上繁衍生息。

到夏商时，宜昌为古“荆州之域”,春秋战国为楚地，史称“楚之西塞”，那时宜昌就建有城邑。楚顷襄王二十一年（公元前278年）秦将白起“攻楚，拔郢，烧夷陵”，夷陵之名始见于史，使宜昌有文字可考的历史达2284年。

三国时，吴黄武元年（222年），改夷陵为西陵郡，也称宜都郡。

晋太康年间（280-289年），改置为夷陵县；最迟在305年，分夷陵西境（在长江南岸的黄牛岩至黑岩之间）另置一县时命名“宜昌”（今宜昌市西北三斗坪镇一带），寓意在于祈福分境置县宜于国运昌盛，“宜昌”之名便始见于史。

南北朝时宋、齐皆与晋同。梁改宜都郡为宜州，西魏改为拓州，北周改为硖州。

隋大业三年（607年）改硖州为夷陵郡，辖夷陵、夷道、长杨、远安四县，夷陵县为郡治，隶属荆州都督府。

唐初，改夷陵郡为硖州，领上述四县，属山南东道。天宝初又改为夷陵郡。乾元元年（758年）复改硖州，辖原四县，仍属山南东道。

五代时，硖州与荆州、归州为南平国。

北宋复称硖州，属荆湖北路，仍辖原夷陵四县。元丰年间（1078～1085年）改“硖”为“峡”。

元至元十七年（1280年）升峡州为峡州路，领原四县，属河南行省荆湖北道。

明初改峡州路为峡州府。洪武九年（1376年），改峡州为夷陵州，领宜都、长阳、远安三县，治所夷陵，隶属湖广布政使司荆州府上荆南道。

清顺治四年（1647），夷陵州隶属荆州府。顺治五年，改“夷陵”为“彝陵”。雍正十三年（1735年），升彝陵州为宜昌府，改彝陵县为东湖县并为宜昌府治所，领东湖、兴山、巴东、长阳、长乐5县及归州、鹤峰2州，隶属荆宜施道。宜都、枝江、当阳、远安4县属荆州府。

1876年，中英签订《[烟台条约](../Page/烟台条约.md "wikilink")》，宜昌开放为通商城市。次年，宜昌设立海关，正式对外开放。

1911年10月10日，[辛亥革命在](../Page/辛亥革命.md "wikilink")[武昌首義](../Page/武昌首義.md "wikilink")。10月13日在宜昌的[川漢鐵路工人發起繼義](../Page/川漢鐵路.md "wikilink")，成為[武漢以外首個響應革命的地方](../Page/武漢.md "wikilink")。

1940年5月，[侵華日軍發起](../Page/侵華日軍.md "wikilink")[棗宜會戰](../Page/棗宜會戰.md "wikilink")，於6月12日攻陷宜昌，雖15日由[中華民国國軍短暫收復](../Page/中華民国國軍.md "wikilink")，但旋即再度易手，宜昌被日軍佔領至1945年[日本投降](../Page/日本投降.md "wikilink")。

20世纪70-80年代，[葛洲坝的兴建使得宜昌实现了由一个小城市向中等城市的转变](../Page/葛洲坝.md "wikilink")。

1994年开始建设的[三峡工程则使宜昌開始由中等城市向大城市的跨越](../Page/三峡工程.md "wikilink")。

## 地理

由于特殊的地理位置，宜昌处于地理上的第二阶梯的[巫山山脉和第三阶梯的](../Page/巫山.md "wikilink")[江汉平原的交汇处](../Page/江汉平原.md "wikilink")，紧扼[四川盆地的东大门](../Page/四川盆地.md "wikilink")，地势险要，自古以来就是兵家必争之地。

宜昌雨量充沛，温暖湿润，适宜多种生物生长。境内森林覆盖率达49%，木材蓄积量240万立方米，居[湖北之首](../Page/湖北.md "wikilink")。在五峰后河等地，至今还保存有[第四纪冰川期遗留下来的](../Page/第四纪.md "wikilink")[原始森林群落](../Page/原始森林.md "wikilink")37万亩多，建有大老岭、柴埠溪、玉泉寺、龙门河、清江等六个[国家森林公园和后河一个国家级](../Page/国家森林公园.md "wikilink")[自然保护区](../Page/自然保护区.md "wikilink")，有20多种[珍稀动物活跃其中](../Page/珍稀动物.md "wikilink")，3000多种高等珍惜植物在那里生长。尤其是五峰后河560余亩原始森林中的[珙桐等稀有珍贵树种](../Page/珙桐.md "wikilink")，是距今1亿多年前的[第四纪冰川期植物物种的幸存者](../Page/第四纪.md "wikilink")，被称为世界植物的“[活化石](../Page/活化石.md "wikilink")”。此外，宜昌的[柑橘](../Page/柑橘.md "wikilink")、茶叶、香菇、蚕茧等土特产品享誉海内外。

### 位置

宜昌市位于[北纬](../Page/北纬.md "wikilink")29度56分～31度34分，[东经](../Page/东经.md "wikilink")110度15分～112度04分，地处[长江上游](../Page/长江.md "wikilink")、中游分界处，闻名世界的[长江三峡的终点](../Page/长江三峡.md "wikilink"),[鄂](../Page/鄂.md "wikilink")、[渝](../Page/渝.md "wikilink")、[湘三省市交汇地](../Page/湘.md "wikilink")。宜昌“上控巴蜀，下引荆襄”，素以“三峡门户、川鄂咽喉”著称。自古以来，宜昌就是鄂西、湘西北和川（渝）东一带重要的物资集散地和交通要冲。

### 气候

宜昌四季分明，春秋较长。属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")。年平均水量为992.1—1404.1毫米之间。雨水丰沛，多在[夏季](../Page/夏季.md "wikilink")，较长的降水过程都发生在6—7月份，雨热同季，全年积温较高，无霜期较长，年平均气温为13.1℃—18℃，但随着海拔高度上升而递减，每上升100米降低0.6℃。7月平均气温24.1℃—28.8℃，[元月平均气温](../Page/元月.md "wikilink")1.7℃—6.5℃。极端最高气温41.4℃（1969年8月2日），最低气温-9.8℃（1977年1月30日）。其中三峡河谷及[清江](../Page/清江.md "wikilink")、香溪河谷地带，由于高山对峙，下有流水，故在600米以下存在逆温层，冬季较暖和，极端最低气温小于-7℃的机会只有5%，是得天独厚的[柑桔生产基地](../Page/柑桔.md "wikilink")。

Metasequoia glyptostroboides (YiChang,2017).jpg|三峡植物园中的水杉林 宜昌新气象塔
01.jpg|宜昌新气象塔

### 地貌

宜昌位于[湖北省西南部](../Page/湖北省.md "wikilink")，地处长江上游与中游的结合部、鄂西[秦巴山脉和](../Page/秦巴山脉.md "wikilink")[武陵山脉向](../Page/武陵山脉.md "wikilink")[江汉平原的过渡地带](../Page/江汉平原.md "wikilink")，地势西高东底，地貌复杂多样，境内有[山地](../Page/山地.md "wikilink")、[平原](../Page/平原.md "wikilink")、[丘陵](../Page/丘陵.md "wikilink")，在市域总面积中，山区占69%，丘陵占21%，平原占10%，大致构成“七山、二丘、一平”\[2\]
的格局。

### 河流

流经宜昌的主要[河流有](../Page/河流.md "wikilink")：[长江](../Page/长江.md "wikilink")、[清江](../Page/清江.md "wikilink")、[沮漳河](../Page/沮漳河.md "wikilink")、[黄柏河](../Page/黄柏河.md "wikilink")、[柏临河](../Page/柏临河.md "wikilink")、[香溪河等](../Page/香溪河.md "wikilink")。境内河流以长江为主脉，河流多、密度大、水量丰富，年平均总水量4741.4亿立方米，市境内长度大于10公里的河流有99条，其中集水面积在50平方公里以上的河流有64条，总长3793公里，总集水面积占全市的83.9%。

Yichang skyline
2.jpg|流经宜昌主城区的[长江](../Page/长江.md "wikilink")，拍摄于[夷陵长江大桥](../Page/夷陵长江大桥.md "wikilink")。
Qing river in Changyang 04.jpg|[清江](../Page/清江.md "wikilink") Guanzhuang
Reservoir.jpg|宜昌市饮用水源保护区：官庄水库。

丁家坝大桥上游的黄柏河 02.jpg|黄柏河

Gezhouba Bridge.JPG|葛洲坝三江

### 矿产

市域内已探明矿种53种，大矿床14处。其中[磷矿储量](../Page/磷.md "wikilink")12亿吨，占全国的17%、全省的70%，是全国六大磷矿区之一。[石墨已探明储量](../Page/石墨.md "wikilink")1685万吨，是全世界四大优质石墨矿床之一。铁矿储量17亿吨，占全省的31%。其他如煤矿、[重晶石](../Page/重晶石.md "wikilink")、[花岗石](../Page/花岗石.md "wikilink")、金、银、钨等矿藏，都具有较大的工业开采价值。

## 政治

### 现任领导

[yichang_government.JPG](https://zh.wikipedia.org/wiki/File:yichang_government.JPG "fig:yichang_government.JPG")\]\]

<table>
<caption>宜昌市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党宜昌市委员会.md" title="wikilink">中国共产党<br />
宜昌市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/宜昌市人民代表大会.md" title="wikilink">宜昌市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/宜昌市人民政府.md" title="wikilink">宜昌市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议宜昌市委员会.md" title="wikilink">中国人民政治协商会议<br />
宜昌市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/周霁.md" title="wikilink">周霁</a>[3]</p></td>
<td><p><a href="../Page/张家胜_(1968年).md" title="wikilink">张家胜</a>[4]</p></td>
<td><p><a href="../Page/宋文豹.md" title="wikilink">宋文豹</a>[5]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省</a><a href="../Page/襄阳市.md" title="wikilink">襄阳市</a></p></td>
<td><p>湖北省<a href="../Page/江陵县.md" title="wikilink">江陵县</a></p></td>
<td><p>湖北省<a href="../Page/秭归县.md" title="wikilink">秭归县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年6月</p></td>
<td><p>2017年1月</p></td>
<td></td>
</tr>
</tbody>
</table>

### 行政区划

全市共辖5个[市辖区](../Page/市辖区.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")、2个[自治县](../Page/自治县.md "wikilink")，代管3个[县级市](../Page/县级市.md "wikilink")。总面积21227平方公里，市辖区面积4249平方公里，2013年底建成区面积约130平方公里，2014年年底达153平方公里。

  - 市辖区：[西陵区](../Page/西陵区.md "wikilink")、[伍家岗区](../Page/伍家岗区.md "wikilink")、[点军区](../Page/点军区.md "wikilink")、[猇亭区](../Page/猇亭区.md "wikilink")、[夷陵区](../Page/夷陵区.md "wikilink")
  - 县级市：[宜都市](../Page/宜都市.md "wikilink")、[当阳市](../Page/当阳市.md "wikilink")、[枝江市](../Page/枝江市.md "wikilink")
  - 县：[远安县](../Page/远安县.md "wikilink")、[兴山县](../Page/兴山县.md "wikilink")、[秭归县](../Page/秭归县.md "wikilink")
  - 自治县：[长阳土家族自治县](../Page/长阳土家族自治县.md "wikilink")、[五峰土家族自治县](../Page/五峰土家族自治县.md "wikilink")

此外，[宜昌高新技术产业开发区为宜昌市设立的](../Page/宜昌高新技术产业开发区.md "wikilink")[国家级高新技术产业开发区](../Page/国家级高新技术产业开发区.md "wikilink")。

该市现状[建成区主要集中在西陵区](../Page/建成区.md "wikilink")、伍家岗区、猇亭区、点军区的沿江一带以及夷陵区的小溪塔街办、龙泉镇、鸦鹊岭镇和三峡坝区。同2007年相比，该市2008年的建成区面积比2007年增长3平方千米，2008年新增的建成区主要为高新区深圳工业园和发展大道唐家湾片区。2014年新增建成区则集中在高新区生物产业园片区、猇亭区七里新村片区、宜昌新区伍家岗江北及点军江南片区。

<table>
<thead>
<tr class="header">
<th><p><strong>宜昌市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>420500</p></td>
</tr>
<tr class="odd">
<td><p>420502</p></td>
</tr>
<tr class="even">
<td><p>420503</p></td>
</tr>
<tr class="odd">
<td><p>420504</p></td>
</tr>
<tr class="even">
<td><p>420505</p></td>
</tr>
<tr class="odd">
<td><p>420506</p></td>
</tr>
<tr class="even">
<td><p>420525</p></td>
</tr>
<tr class="odd">
<td><p>420526</p></td>
</tr>
<tr class="even">
<td><p>420527</p></td>
</tr>
<tr class="odd">
<td><p>420528</p></td>
</tr>
<tr class="even">
<td><p>420529</p></td>
</tr>
<tr class="odd">
<td><p>420581</p></td>
</tr>
<tr class="even">
<td><p>420582</p></td>
</tr>
<tr class="odd">
<td><p>420583</p></td>
</tr>
<tr class="even">
<td><p>注：西陵区数字包含宜昌高新技术产业开发区所辖3街道。</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

[宜昌规划展览馆沙盘.jpg](https://zh.wikipedia.org/wiki/File:宜昌规划展览馆沙盘.jpg "fig:宜昌规划展览馆沙盘.jpg")

## 人口

<table>
<caption><strong>宜昌市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>宜昌市</p></td>
<td><p>4059686</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>西陵区</p></td>
<td><p>512074</p></td>
<td><p>12.61</p></td>
</tr>
<tr class="even">
<td><p>伍家岗区</p></td>
<td><p>214194</p></td>
<td><p>5.28</p></td>
</tr>
<tr class="odd">
<td><p>点军区</p></td>
<td><p>103696</p></td>
<td><p>2.55</p></td>
</tr>
<tr class="even">
<td><p>猇亭区</p></td>
<td><p>61230</p></td>
<td><p>1.51</p></td>
</tr>
<tr class="odd">
<td><p>夷陵区</p></td>
<td><p>520186</p></td>
<td><p>12.81</p></td>
</tr>
<tr class="even">
<td><p>远安县</p></td>
<td><p>184532</p></td>
<td><p>4.55</p></td>
</tr>
<tr class="odd">
<td><p>兴山县</p></td>
<td><p>170630</p></td>
<td><p>4.20</p></td>
</tr>
<tr class="even">
<td><p>秭归县</p></td>
<td><p>367107</p></td>
<td><p>9.04</p></td>
</tr>
<tr class="odd">
<td><p>长阳土家族自治县</p></td>
<td><p>388228</p></td>
<td><p>9.56</p></td>
</tr>
<tr class="even">
<td><p>五峰土家族自治县</p></td>
<td><p>188923</p></td>
<td><p>4.65</p></td>
</tr>
<tr class="odd">
<td><p>宜都市</p></td>
<td><p>384598</p></td>
<td><p>9.47</p></td>
</tr>
<tr class="even">
<td><p>当阳市</p></td>
<td><p>468293</p></td>
<td><p>11.54</p></td>
</tr>
<tr class="odd">
<td><p>枝江市</p></td>
<td><p>495995</p></td>
<td><p>12.22</p></td>
</tr>
<tr class="even">
<td><p>注：西陵区的常住人口数据中包含宜昌高新技术产业开发区85361人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年中国[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")4059686人\[9\]。其中，男性为2080389人，占总人口的51.25%；女性为1979297人，占总人口的48.75%。总人口性别比（以女性为100）为105.11。0－14岁的人口为441349人，占总人口的10.87%；15－64岁的人口为3183622人，占总人口的78.42%；65岁及以上的人口为434715人，占总人口的10.71%。

而[中南财经政法大学的课题组成员](../Page/中南财经政法大学.md "wikilink")2015年8月在宜昌市进行了一次关于人口问题的调研，调查发现宜昌地区的生育率多年来持续偏低，从2000年到2014年总体的生育水平一直处于下降趋势\[10\]。目前宜昌市育龄女性人均子女人数已不足1人。在中国当局宣布全面开放[二胎政策后](../Page/二胎.md "wikilink")，宜昌市民、周边地区居民的生育意愿要远低于预期水平，并没有出现生育率的回升。2016年9月18日，宜昌市卫生和计划生育委员会在其网站上发表了一篇名为《关于实施“全面两孩”政策致市直机关事业单位全体共产党员共青团员的公开信》的文章，呼吁“年轻的同志要从我做起，年老的同志要教育和督促自己的子女”、“引导[群众负责任](../Page/群众.md "wikilink")、有计划地生育第二个子女”\[11\]。此事件再次引发网民对中国当局[独生子女政策和目前](../Page/独生子女.md "wikilink")[二胎政策的讨论](../Page/二胎.md "wikilink")。
”

## 经济

[Yichang_wanda_plaza_indoor_walking_street.jpg](https://zh.wikipedia.org/wiki/File:Yichang_wanda_plaza_indoor_walking_street.jpg "fig:Yichang_wanda_plaza_indoor_walking_street.jpg")室内步行街\]\]
改革开放以来，宜昌经济快速发展，综合实力日益增强。2014年，全市生产总值达到3132.21亿元，工业已初步形成了以水电、载电体、化工、食品医药、新型建材等优势产业为主导的产业格局。宜昌投资环境不断优化，全方位对外开放格局已经形成。此外，宜昌还是中部地区唯一一个常住人口大于户籍人口的非省会城市。
'''

| 指标名称（单位）        | 经济数据    |
| --------------- | ------- |
| 地区生产总值（亿元）      | 3709.36 |
| 人均地区生产总值（元）     | 89978   |
| 固定资产投资（亿元）      | 3191.15 |
| 社会消费品零售总额（亿元）   | 1240.33 |
| 财政总收入（亿元）       | 386.15  |
| 地方一般公共预算收入（亿元）  | 300.04  |
| 地方一般公共预算支出（亿元）  | 538.63  |
| 金融机构本外币存款余额（亿元） | 3130.03 |

**<small>2016年宜昌主要经济指标一览表</small>**\[12\]

2016年全市实现生产总值3709.36亿元，比上年增长8.8%。分产业看，第一产业增加值398.89亿元，增长3.9%；第二产业增加值2122.74亿元，增长8.8%；第三产业增加值1187.73亿元，增长10.4%。三次产业结构由上年的10.9:58.7:30.4变化为10.8:57.2:32.0，第三产业占比较上年提高1.6个百分点。按常住人口计算，人均地区生产总值89978元，增长9.2%，较上年增加7618元。\[13\]

### 工业

宜昌独特的[地理位置和丰富的](../Page/地理.md "wikilink")[水电资源](../Page/水电.md "wikilink")，决定了宜昌工业发展的重点是[载电体工业](../Page/载电体.md "wikilink")，即以市场为导向，立足宜昌水电能源优势，依靠科技进步和技术创新，发展以高耗能、大耗水、环保型为特征的载电体工业。形成以输配电、电子元器件、[磷化工](../Page/磷.md "wikilink")、[有机化工](../Page/有机.md "wikilink")、[生物及](../Page/生物.md "wikilink")[医药](../Page/医药.md "wikilink")、新型建材等为支柱产业的载电体工业体系。为鼓励产业聚集，设有点军载电体工业试验区、猇亭台商工业园和宜昌[国家级高新技术产业开发区等](../Page/国家级高新技术产业开发区.md "wikilink")。

目前，宜昌市载电体产业发展迅速，形成了重点发展的六大支柱产业：

1.  电子信息基础材料及机电设备制造业
2.  化学工业
3.  生物工程及新医药行业
4.  建材、新材料行业
5.  冶金工业
6.  纺织工业

#### 中央企业

  - [中国长江三峡集团公司](../Page/中国长江三峡集团公司.md "wikilink")
  - [中国葛洲坝集团公司](../Page/中国葛洲坝集团公司.md "wikilink")
  - [长江电力](../Page/长江电力.md "wikilink")
  - [中国核工业第二二建设有限公司](../Page/中国核工业第二二建设有限公司.md "wikilink")

#### 重工业

  - 宜昌船舶柴油机有限公司（四〇三厂）
  - [湖北宜化集团有限责任公司](../Page/宜化集团.md "wikilink")
  - [湖北兴发化工集团股份有限公司](../Page/湖北兴发化工集团股份有限公司.md "wikilink")
  - [宜昌叉车厂](../Page/宜昌叉车厂.md "wikilink")
  - [红旗线缆](../Page/红旗线缆.md "wikilink")

#### 轻工业

  - [宜昌太平鸟服饰有限公司](../Page/宜昌太平鸟服饰有限公司.md "wikilink")
  - [安琪酵母股份有限公司](../Page/安琪酵母股份有限公司.md "wikilink")
  - [湖北枝江酒业股份有限公司](../Page/湖北枝江酒业股份有限公司.md "wikilink")
  - [湖北稻花香酒业股份有限公司](../Page/湖北稻花香酒业股份有限公司.md "wikilink")
  - [宜都市东阳光实业发展有限公司](../Page/宜都市东阳光实业发展有限公司.md "wikilink")

### 中心城区四大商圈

夷陵广场喷泉.jpg|夷陵广场商圈：国贸大厦、宜昌商场（右）、卓越广场（远处双子楼）
Yichangyilingguangchang.jpg|夷陵广场商圈：丹尼斯广场
Yichangjunyaodasha.jpg|夷陵广场商圈：均瑶广场（已荒废）\[14\] Yichang
Commercial pedestrian street.jpg|解放路商圈 Yichangwandaplaza.jpg|九码头商圈：万达广场
宜昌水悦城.jpg|三环广场商圈：水悦城 山水华庭.jpg|伍家岗区建筑群

  - 夷陵广场商圈（也作铁路坝商圈）：位于市中心，以国贸集团（四大主力卖场），CBD购物中心（包括沃尔玛超市）和大洋百货为代表，是宜昌市的核心商圈，无论是知名度还是消费能力都稳居宜昌首位。
  - 解放路商圈：以商业步行街为代表，是原宜昌市最繁华地带，但自夷陵广场建成后，中心地位受到严重动摇，目前屈居第二。步行街重点突出巴楚文化和三峡特色，但因为规划落后，所以商业气氛较差。
  - 九码头商圈：以万达广场为代表，面对三峡游客中心。地处宜昌港码头地带，来往旅客多，对九码头经济起到一定促进作用。
  - 三环广场商圈：以水悦城为代表，位于东山开发区，多个建成／在建商场，吸引了多个国际品牌入驻。

### 其他商圈

  - 葛洲坝商圈：以华祥商业中心为代表，处于330、403、827（曾为中国国家级重点项目工程编号，现为地名的代词）和[三峡大学黄金交汇地带](../Page/三峡大学.md "wikilink")，人流量巨大，未来极有可能成为宜昌市又一大型商圈
  - 五一广场商圈：处于伍家岗腹地，以[宜昌东站和五一广场为中心](../Page/宜昌东站.md "wikilink")，随着国贸香山福久源的建成，未来极有可能成为宜昌市又一大型商圈。
  - 晓溪塔商圈：以小溪塔为中心，包括长江市场等，是夷陵区的区中心。在大宜昌地区，有一定的竞争力。
  - 猇亭商圈：以“盛世天下”为代表，国贸集团已经决定进入，未来猇亭区的商业会得到一定发展
  - 点军商圈：以朱市街为中心（包括五龙），重点发展农业，商业(第三产业)发展水平低，辐射能力弱，客流量较小，发展不成熟。宜昌[至喜长江大桥目前已经开通](../Page/至喜长江大桥.md "wikilink")，该地区今后可能会迅猛发展。

## 交通

今天的宜昌，已经形成公路主骨架、铁路大动脉、水运大通道、空中大走廊、港站大联运的交通新格局，成为中西部结合的重要交通枢纽和现代物流中心。目前宜昌城区已建成BRT（快速公交系统）一期。

### 水运

长江黄金水道自西向东横贯宜昌全境，万吨级船队西通[重庆](../Page/重庆.md "wikilink")，东达[上海](../Page/上海.md "wikilink")，依托长江黄金水道重点建设的宜昌港成为三峡航运中转中心。

宜昌境内航道里程达678.5[公里](../Page/公里.md "wikilink")，其中[长江干线](../Page/长江.md "wikilink")232公里，[内河里程](../Page/内河.md "wikilink")446.44公里。

过江通道7个，港口年吞吐量万吨以上的有21个，其中年吞吐量20万吨以上的有13个，码头泊位354个。

拥有营运客船289艘，货船402艘，水路完成旅客发送量266万人次，货运量2209万吨；港口客货吞吐量分别为186.8万人次和3332万吨。

|        |        |
| ------ | ------ |
| 宜昌-上海  | 1751公里 |
| 宜昌-江阴  | 1563公里 |
| 宜昌-涪陵  | 528公里  |
| 宜昌-南通  | 1623公里 |
| 宜昌-万州  | 321公里  |
| 宜昌-镇江  | 1446公里 |
| 宜昌-张家港 | 1581公里 |
| 宜昌-重庆  | 648公里  |
| 宜昌-南京  | 1359公里 |

**宜昌通往国内主要城市的里程**

### 公路和桥梁

[宜昌汽车客运中心站.jpg](https://zh.wikipedia.org/wiki/File:宜昌汽车客运中心站.jpg "fig:宜昌汽车客运中心站.jpg")
[宜昌至喜长江大桥.jpg](https://zh.wikipedia.org/wiki/File:宜昌至喜长江大桥.jpg "fig:宜昌至喜长江大桥.jpg")\]\]
[沪蓉高速（G42）](../Page/沪蓉高速公路.md "wikilink")、[沪渝高速（G50）横接东西](../Page/沪渝高速公路.md "wikilink")，[呼北高速（G59）纵贯南北](../Page/呼北高速公路.md "wikilink")，七座[长江大桥跨越大江两岸](../Page/长江大桥.md "wikilink")，岳宜高速、三峡高速、翻坝高速、318国道、209国道等形成公路主骨架。

2009年底，市境内[高速公路](../Page/高速公路.md "wikilink")224公里。公路通车里程24862.06公里，其中[国道](../Page/国道.md "wikilink")277公里、[省道](../Page/省道.md "wikilink")1467公里、[县道](../Page/县道.md "wikilink")1314公里。全市100%的乡镇通油路、84.4%的[行政村通等级公路](../Page/行政村.md "wikilink")，公路密度117.92公里/每百平方公里。公路客、货运站88个，拥有营运客车4186辆，货车23993辆。年完成客运量7848万人次，货运量463万吨。城镇拥有公交运营车辆821台，营运路线72条，出租车2795台，年客运量18350.9万人次。

|          |            |
| -------- | ---------- |
| 宜昌-武汉    | 325公里全程高速  |
| 宜昌-南昌    | 680公里全程高速  |
| 宜昌-长沙    | 415公里全程高速  |
| 宜昌-上海    | 1400公里全程高速 |
| 宜昌-重庆-贵阳 | 1300公里全程高速 |

**宜昌通达国内主要城市的里程**

### 铁路

[Yichang_East_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:Yichang_East_Railway_Station.jpg "fig:Yichang_East_Railway_Station.jpg")
[焦柳铁路](../Page/焦柳铁路.md "wikilink")、[宜万铁路](../Page/宜万铁路.md "wikilink")、[汉宜铁路等中国重点交通线在宜昌交会](../Page/汉宜铁路.md "wikilink")。形成了宜昌的铁路大动脉。[沪汉蓉客运专线的最后一段](../Page/沪汉蓉客运专线.md "wikilink")[渝利铁路于](../Page/渝利铁路.md "wikilink")2013年12月28日建成通车后，，宜昌已经成为连接[成都](../Page/成都.md "wikilink")，[重庆](../Page/重庆.md "wikilink")，[武汉](../Page/武汉.md "wikilink")，[南京和](../Page/南京.md "wikilink")[上海的重要纽带](../Page/上海.md "wikilink")。
宜昌现拥有两个火车站：[宜昌站和](../Page/宜昌站.md "wikilink")[宜昌东站](../Page/宜昌东站.md "wikilink")。目前，宜昌绝大多数火车都在东站发出。
2014年底，全市铁路里程481.6公里，年发送旅客971万人次，目前经宜昌东站的列车数每天180对，位居全省前列。

|                                         |        |
| --------------------------------------- | ------ |
| 宜昌东-[北京西](../Page/北京西站.md "wikilink")   | 1421公里 |
| 宜昌东-[上海虹橋](../Page/上海虹桥站.md "wikilink") | 1121公里 |
| 宜昌东-[廣州](../Page/广州站.md "wikilink")     | 1245公里 |
| 宜昌东-杭州东                                 | 1066公里 |
| 宜昌东-重庆北                                 | 776公里  |
| 宜昌东-成都                                  | 908公里  |
| 宜昌东-[武漢](../Page/武汉站.md "wikilink")     | 291公里  |
| 宜昌东-西安                                  | 1005公里 |

**宜昌通达国内主要城市的铁路里程**

### 航空

[Sanxia_airport.jpg](https://zh.wikipedia.org/wiki/File:Sanxia_airport.jpg "fig:Sanxia_airport.jpg")
拥有4D级（规划4E级）机场一座，全国五十七个对外开放航空口岸之一。
[三峡机场是](../Page/宜昌三峡机场.md "wikilink")[湖北省第二大](../Page/湖北省.md "wikilink")[航空港](../Page/机场.md "wikilink")，地處[猇亭區東部](../Page/猇亭區.md "wikilink")，舊名黄龍寺機場。服务区域覆盖鄂西各城市。已开通国内30余个大中城市的航班。2016年，宜昌三峡机场完成旅客吞吐量153.6万人次，飞机起降14324架次，实现货邮行运输10988.7吨；起降架次、旅客吞吐量同比去年实现22%以上的增长，货邮行实现10%以上增长。2016年国际航班旅客吞吐量达到3.16万人次，平均客座率76.9%，其中韩国籍旅客1.74万人次，占比55%。[三峡机场已成为三峡地区最大的民用机场](../Page/三峡机场.md "wikilink")，步入国内中型机场行列。2014年6月,被SKYTRAX评为四星级机场。

| 年份   | 旅客吞吐量（人）  | 增长率    | 货邮行吞吐量（吨） | 增长率    |
| ---- | --------- | ------ | --------- | ------ |
| 2007 | 538,112   | 8.04%  | 2,408.6   | 22.52％ |
| 2008 | 523,234   | \-2.8% | 2,676.0   | 11.1%  |
| 2009 | 657,507   | 25.7%  | 3,498.4   | 30.7%  |
| 2010 | 724,121   | 10.1%  | 3,184.7   | \-9.0% |
| 2011 | 778,004   | 7.4%   | 3769.3    | 18.36% |
| 2012 | 901,366   | 15.9%  | 3,996     | 6%     |
| 2013 | 900,076   | \-0.1% | 4,628     | 15.8%  |
| 2014 | 1,127,093 | 25.2%  | 4,294.0   | \-7.2% |
| 2015 | 1,244,275 | 10.4%  | 3,608.7   | \-16%  |
| 2016 | 1,535,707 | 23.4%  | 3,806.8   | 5.5%   |

**宜昌三峡机场历年吞吐量**

### 市內交通

[缩略图](https://zh.wikipedia.org/wiki/File:宜昌BRT_车辆.JPG "fig:缩略图") 參見:

  - [宜昌公交](../Page/宜昌公交.md "wikilink")

<!-- end list -->

  - [宜昌出租车和](../Page/宜昌出租车.md "wikilink")[网约车](../Page/宜昌网约车.md "wikilink")

<!-- end list -->

  - [宜昌快速公交](../Page/宜昌BRT.md "wikilink")
  - [宜昌軌道交通](../Page/宜昌軌道交通.md "wikilink")

<!-- end list -->

  - [宜昌轮渡](../Page/宜昌轮渡.md "wikilink")

## 文化

宜昌历史悠久，文化资源丰富，曾经是巴楚文化的重要发祥地。这里钟灵毓秀、人杰地灵，先有远古“蚕母娘娘”嫘祖，后曾孕育出世界历史文化名人屈原、民族和亲使者王昭君以及闻名中外的著名学者杨守敬等诸多先贤名流，历代著名文人，如李白、杜甫、白居易、欧阳修、苏轼、陆游等，也多会游览至此。他们游览西陵山水所留下的胜迹，陶醉西陵风光所写下的诗文，为宜昌增添了宝贵的文化财富。

### 方言

宜昌主要方言为[宜昌话](../Page/宜昌话.md "wikilink")，今天的宜昌话跟[四川](../Page/四川.md "wikilink")、[云南](../Page/云南.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[广西西北部](../Page/广西.md "wikilink")、[湖南西北角这一大片地区的](../Page/湖南.md "wikilink")[汉语](../Page/汉语.md "wikilink")[方言同属于官话方言的](../Page/方言.md "wikilink")[西南官话](../Page/西南官话.md "wikilink")，宜昌话属于西南官话成渝片。由于地理的毗邻，[川东地区的](../Page/川东.md "wikilink")[四川话和宜昌话尤为接近](../Page/四川话.md "wikilink")。但受普通話推廣的影響，加上宜昌話與普通話本身就比較相似，越來越多的宜昌人的方言变得不地道，即“方言的調，國語的音和詞”，方言的傳統用語大量消失。

### 艺术

### 影视作品

  - 2012年在中国大陆播出的电视剧《[山楂树之恋](../Page/山楂树之恋.md "wikilink")》是在宜昌市[夷陵区分乡镇百里荒风景区拍摄的](../Page/夷陵区.md "wikilink")。
  - 国共抗日题材电视剧《[宜昌保卫战](../Page/宜昌保卫战.md "wikilink")》于2015年5月在宜昌市多个地点开始拍摄，在[西陵区的滨江公园大门处搭建起了再现宜昌老城区旧貌的场景和舞台](../Page/西陵区.md "wikilink")（现已拆除）。该电视剧的主要演员有[陈小春](../Page/陈小春.md "wikilink")、[童蕾](../Page/童蕾.md "wikilink")、[徐佳等](../Page/徐佳.md "wikilink")。\[15\]这部电视剧于2016年9月19日在[中国中央电视台电视剧频道首播](../Page/中国中央电视台电视剧频道.md "wikilink")。

### 长阳巴山舞

[巴山舞是宜昌普及最广泛的民间舞蹈](../Page/巴山舞.md "wikilink")。巴山舞是[20世纪80年代兴起的一种新型的群众自娱性的集体舞蹈](../Page/20世纪80年代.md "wikilink")。它是由[土家族的民间古老的](../Page/土家族.md "wikilink")“[跳丧舞](../Page/跳丧舞.md "wikilink")”（也叫“跳撒叶尔嗬”）经长阳覃发池等民间[舞蹈工作者收集整理](../Page/舞蹈.md "wikilink")，改革创新而发展起来。它一经问世便深受青睐。身体上下颤动和胯部左右摇摆是巴山舞的主要特点。21世纪初，特别是01年～05年在宜昌市各个广场、学校的体育课上都能见到巴山舞。
2005年，长阳巴山舞被[国家体育总局授予](../Page/国家体育总局.md "wikilink")“全国优秀全民健身项目一等奖”\[16\]\[17\]。

### 饮食

凉虾一碗.png|宜昌街头巷尾常见的特色小吃——[凉虾](../Page/凉虾.md "wikilink")
萝卜饺子.jpg|[萝卜饺子](../Page/萝卜饺子.md "wikilink")

宜昌地处[重庆和](../Page/重庆.md "wikilink")[武汉之间](../Page/武汉.md "wikilink")，口味接近四川盆地，喜食辣味，但與其相比，少了[花椒的麻味](../Page/花椒.md "wikilink")，而多了甜味，相对武汉来说，口味则偏辣、偏酸。宜昌与[武汉相同](../Page/武汉.md "wikilink")，也把吃早餐俗称为“过早”。宜昌著名的小吃有[凉虾](../Page/凉虾.md "wikilink")、[萝卜饺子](../Page/萝卜饺子.md "wikilink")、[顶顶糕](../Page/顶顶糕.md "wikilink")、金箍条、[榨广椒](../Page/榨广椒.md "wikilink")、[苕酥](../Page/苕酥.md "wikilink")、[赤花籽](../Page/赤花籽.md "wikilink")、懒豆花等。\[18\]\[19\]\[20\]\[21\]\[22\]\[23\]\[24\]\[25\]

### 媒体

|                                                                                |     |                                                          |
| ------------------------------------------------------------------------------ | --- | -------------------------------------------------------- |
| 名称                                                                             | 类型  | 备注                                                       |
| [三峡日报](../Page/三峡日报.md "wikilink")、[三峡商报](../Page/三峡商报.md "wikilink")          | 报纸  | [三峡日报传媒集团](../Page/三峡日报传媒集团.md "wikilink")，全市覆盖          |
| [三峡晚报](../Page/三峡晚报.md "wikilink")、《[楚天都市报](../Page/楚天都市报.md "wikilink")·宜昌新闻》 | 报纸  | [湖北日报传媒集团](../Page/湖北日报传媒集团.md "wikilink")，全市覆盖          |
| [宜昌三峡电视台](../Page/宜昌三峡电视台.md "wikilink")                                       | 电视台 | [宜昌三峡广播电视总台](../Page/宜昌三峡广播电视总台.md "wikilink")，全市覆盖，4个频道 |
| [宜昌人民广播电台](../Page/宜昌人民广播电台.md "wikilink")                                     | 电台  | [宜昌三峡广播电视总台](../Page/宜昌三峡广播电视总台.md "wikilink")，全市覆盖，3个频率 |
| [三峡周刊](../Page/三峡周刊.md "wikilink")                                             | 杂志  | [宜昌三峡广播电视总台](../Page/宜昌三峡广播电视总台.md "wikilink")           |
| [葛洲坝有线电视台](../Page/葛洲坝有线电视台.md "wikilink")                                     | 电视台 | 隶属葛洲坝集团，葛洲坝辖区有线覆盖，1个频道                                   |

#### 报纸

宜昌有本地报纸《三峡日报》《三峡商报》，还有湖北省报地方版《[楚天都市报](../Page/楚天都市报.md "wikilink")·宜昌新闻》和湖北省第一份省级地市晚报《三峡晚报》。其中《三峡晚报》是宜昌市发行量最大的报纸，《三峡日报》为中共宜昌市委机关报。

#### 广播

宜昌的广播媒体有宜昌人民广播电台。宜昌可以听到[中央人民广播电台](../Page/中央人民广播电台.md "wikilink")[中国之声](../Page/中国之声.md "wikilink")、湖北新闻综合广播、楚天卫星台、宜昌新闻综合频率、宜昌交通音乐频率、宜昌汽车频率。

#### 电视

宜昌的电视媒体有[宜昌三峡电视台](../Page/宜昌三峡电视台.md "wikilink")。旗下有宜昌三峡电视台综合频道、公共频道、移动频道和旅游商务频道。另外，在宜昌市葛洲坝的管辖地区，也有“[葛洲坝有线电视台](../Page/葛洲坝有线电视台.md "wikilink")”。

## 旅游

宜昌集雄山与大川、历史古迹与现代工程、自然风光风光集峡、山、水、洞于一域，与人文景观与一体，拥有各类旅游资源601处，其中世界级3处、国家级30处、省级40处。随着[三峡工程的建成](../Page/三峡工程.md "wikilink")，“高峡平湖”的出现，将形成一批世界级的[旅游品牌](../Page/旅游.md "wikilink")。
著名的[葛洲坝枢纽工程和宜昌市区的万家灯火交相辉映](../Page/葛洲坝.md "wikilink")，黄陵庙、三游洞、龙泉洞、下牢溪、桃花村、金狮洞等景观错落其间，险峻的[西陵峡似游龙串连其中](../Page/西陵峡.md "wikilink")。以诗人[屈原](../Page/屈原.md "wikilink")、美人[王昭君](../Page/王昭君.md "wikilink")、圣人[关羽](../Page/关羽.md "wikilink")、学者[杨守敬为代表的古代名人文化](../Page/杨守敬.md "wikilink")；以巴人遗风、土家风情为代表的民俗文化；以[三国古战场](../Page/三国.md "wikilink")、三游洞为代表的历史遗迹和以大老岭、柴埠溪[国家森林公园等为代表的生态旅游胜地](../Page/国家森林公园.md "wikilink")，构成了“金色三峡，银色大坝，绿色宜昌”的[旅游城市形象](../Page/旅游城市.md "wikilink")。

### 宜昌市旅游宣传口号

愛上宜昌

### 宜昌旅游风景区

宜昌共有国家4A级以上景区11家（其中三峡大坝旅游区、三峡人家风景区、清江画廊为5A级），居湖北省首位\[26\]\[27\] 。

|                                                      |
| :--------------------------------------------------: |
|              **宜昌市国家级风景名胜区及国家级非物质文化遗产**              |
|                          等级                          |
| **[中国国家5A级旅游景区](../Page/中国国家5A级旅游景区.md "wikilink")** |
| **[中国国家4A级旅游景区](../Page/中国国家4A级旅游景区.md "wikilink")** |
|    [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")    |
|    [国家级非物质文化遗产](../Page/国家级非物质文化遗产.md "wikilink")    |

<File:Dreischluchtendamm> hauptwall
2006.jpg|thumb|260px|right|位于[夷陵区](../Page/夷陵区.md "wikilink")[三斗坪镇的](../Page/三斗坪镇.md "wikilink")[三峡大坝](../Page/三峡大坝.md "wikilink")
<File:Yiling>, Yichang, Hubei, China - panoramio (5).jpg|thumb|三峡人家龙进溪
[file:宜昌三峡大瀑布.jpg|thumb](file:宜昌三峡大瀑布.jpg%7Cthumb)|[三峡大瀑布](../Page/三峡大瀑布.md "wikilink")，又名白果树瀑布
[File:Sanyoushiren.jpg|thumb|200px|right](File:Sanyoushiren.jpg%7Cthumb%7C200px%7Cright)|[三游洞诗人](../Page/三游洞.md "wikilink")——[白居易](../Page/白居易.md "wikilink")、[白行简和](../Page/白行简.md "wikilink")[元稹](../Page/元稹.md "wikilink")
Sandouping-Huangling-Miao-4893.jpg|黄陵庙
Yichang-Xiakou-Scenic-area-bridge-4835.jpg|西陵峡口风景区 The 10th China
yichang three gorges international tourist festival flower floats
parade.JPG|宜昌三峡旅游节 Yichang Museum.jpg|老博物馆 Yichang new Museum.jpg|新博物馆
Yichang Planning Exhibition Hall.jpg|规划展览馆 中国三峡柑橘博物馆.jpg|三峡柑橘博物馆
Zhenjiang Pavilion （Yichang） 03.jpg|镇江阁
三闾大夫屈原.jpg|[三闾大夫](../Page/三闾大夫.md "wikilink")[屈原雕像](../Page/屈原.md "wikilink")

### 宜昌古八景

1.  **东山图画**因东山寺、古白龙神井、绿萝溪、竹林腊梅等构成如诗如画之景，曾引得陆游、欧阳修等众多文人吟咏。始建于唐代的东山寺在东山之巅，现烈士陵园偏南，即现湖心亭北，与古白龙神井隔塘相望。寺内有揽胜楼、东山草堂、铁因庵。与江南葛道山（现磨基山）对峙。登揽胜楼，可遥望江流东去、俯看黛瓦街市，犹如图画之中，寺毁于抗战中；又从城中望东山，暮鼓晨钟，竹木苍翠；东山日出之时为景物最美，旧宜昌县府衙门门额上刻有“东山旭日”，为宜昌旧八景之首。
2.  **西陵形胜**又名：西塞晚霞。西塞即西陵山庙（现复建成嫘祖庙），原为前后坪的分水岭，住有道士，毁于抗战中。
3.  **赤矶钓艇** 原在葛洲坝与西坝之间（又有称现西坝三江口），有巨形红色矶石露于江心，故名赤矶。向为渔舟泊于此，百年前被洪流冲消。
4.  **黄牛棹歌**黄陵庙在黄牛岩山麓，庙在本市上游九十里的江南岸，高岩之上一大片石形，状似躬耕农夫赶牛扶犁，故名黄牛岩。黄牛岩在西陵峡中，帆棹浆橹，上下唱号子，名曰棹歌。古人唱云：朝见黄牛，暮见黄牛，三朝三暮，黄牛如故。
5.  **三游雨霁**三游洞在南津关北岸、下牢溪口山腰，唐白居易、白行简、元稹等三人首游此洞并题诗后而得名。林寒涧肃，风景极致，尤以雨后新晴，江山如洗，清静秀丽，分外好看。
6.  **五陇烟收**五陇是指江南岸丘陵的五个山包（峰）；烟收坝（也名烟脂坝）为五龙下游江边的一个小长岛，晨雾一收，江山顿晰，美伦美奂，故名五陇烟收。
7.  **雅台明月**尔雅街内（现镇江阁斜对面、市建行上、市外国语小学下的一个小型汽修洗车场），原有尔雅台，其筑台之黄土，传说是从中原开封运来，为晋代郭璞注《尔雅》处。台前面有石砌月亮池，现已毁尽。
8.  **灵洞仙湫**即旧时宜昌旱年求雨祈神处，河西龙王洞（现点军联棚乡石门洞），距市约三十余里，洞中建有庙宇，内有龙潭，传说水深千尺，龙王居潭底龙宫，统领水族，兴云布雨。每遇天旱，官民头戴柳枝，脚穿草鞋，手执神香前往灵洞求雨。\[28\]

### 国际旅游节

有[中国宜昌三峡国际旅游节](../Page/中国宜昌三峡国际旅游节.md "wikilink")，2000年为第一届，每年9月底举办。2010年与[重庆市的](../Page/重庆市.md "wikilink")[中国重庆三峡国际旅游节合并为](../Page/中国重庆三峡国际旅游节.md "wikilink")[中国三峡国际旅游节](../Page/中国三峡国际旅游节.md "wikilink")。
2011年第二届中国三峡国际旅游节在宜昌成功举办。2014年9月，第四届中国长江三峡国际旅游节在宜昌成功举办。

### 国际民间艺术节

2013年9月14日至25日，由中国文联和湖北省人民政府主办、湖北省文联和宜昌市人民政府承办的第九届中国国际民间艺术节在宜昌举行。此届民间艺术节以“发展民间艺术，促进友谊和平”为主题，吸引了来自世界五大洲13个国家的艺术团体共302位艺术家参加，提高了宜昌的知名度\[29\]。

### 国际钢琴节

2013年9月15日至21日，第三届中国宜昌长江钢琴音乐节在湖北省宜昌市隆重举行。本届钢琴节是由中国音乐家协会、宜昌市人民政府主办，中国音乐家协会大型活动办公室、中国音乐家协会高校音乐联盟、柏斯音乐集团承办\[30\]。

### 主要公园、广场

[Yichang_skyline.jpg](https://zh.wikipedia.org/wiki/File:Yichang_skyline.jpg "fig:Yichang_skyline.jpg")[Night_view_of_Yichang.jpg](https://zh.wikipedia.org/wiki/File:Night_view_of_Yichang.jpg "fig:Night_view_of_Yichang.jpg")

moji mountain.JPG|磨基山 夷陵广场.jpg|夷陵广场 宜昌五一广场2.jpg|五一广场 双亭广场 02.jpg|滨江公园
Morning Exercises- Yichang - panoramio.jpg|双亭广场 Tianran Pagoda 3.jpg|天然塔
文佛山1 - panoramio.jpg|文佛山 求雨台公园.jpg|求雨台公园 Metasequoia glyptostroboides
(YiChang,2017(02)).jpg|三峡植物园 三峡森林野生动物世界.jpg|三峡森林野生动物世界 俯瞰小溪塔.jpg|小溪塔森林公园
绿萝路植物园_04.jpg|绿萝路植物园 六泉湖公园.jpg|六泉湖广场

宜昌市内主要[公园有东山公园](../Page/公园.md "wikilink")、儿童公园\[31\]、滨江公园、世界和平公园、磨基山森林公园、小溪塔森林公园、三峡植物园、三峡森林野生动物世界、绿萝路植物园、东辰体育公园、柏临河湿地公园等。主要[广场有夷陵广场](../Page/广场.md "wikilink")、五一广场、双亭广场、六泉湖广场\[32\]\[33\]。

## 社会

### 教育

#### 高校列表

[三峡大学水电楼.jpg](https://zh.wikipedia.org/wiki/File:三峡大学水电楼.jpg "fig:三峡大学水电楼.jpg")

  - [缩略图](https://zh.wikipedia.org/wiki/File:湖北三峡职业技术学院校园_02.jpg "fig:缩略图")[三峡大学](../Page/三峡大学.md "wikilink")（省部共建，本）
  - [湖北三峡职业技术学院](../Page/湖北三峡职业技术学院.md "wikilink")（专）
  - [三峡电力职业学院](../Page/三峡电力职业学院.md "wikilink")（专）
  - [宜昌市广播电视大学](../Page/宜昌市广播电视大学.md "wikilink")（专）
  - [三峡旅游职业技术学院](../Page/三峡旅游职业技术学院.md "wikilink")（专）
  - [缩略图](https://zh.wikipedia.org/wiki/File:三峡电力职业学院西门.jpg "fig:缩略图")[三峡大学科技学院](../Page/三峡大学科技学院.md "wikilink")（民本独）

#### 市内公办学校

##### 中学列表

[缩略图](https://zh.wikipedia.org/wiki/File:夷陵中学正门.jpg "fig:缩略图")
[宜昌一中坦荡石.jpg](https://zh.wikipedia.org/wiki/File:宜昌一中坦荡石.jpg "fig:宜昌一中坦荡石.jpg")

市内西陵、伍家、点军、猇亭、高新五区：

  - [夷陵中学](../Page/夷陵中学.md "wikilink")
  - [宜昌市第一中学](../Page/宜昌市第一中学.md "wikilink")
  - [宜昌市第二中学](../Page/宜昌市第二中学.md "wikilink")
  - [宜昌市三峡艺术高中](../Page/宜昌市三峡艺术高中.md "wikilink")（原市二中艺术特长班）
  - [宜昌市第三中学](../Page/宜昌市第三中学.md "wikilink")
  - [宜昌市第四中学](../Page/宜昌市第四中学.md "wikilink")
  - [宜昌市第五中学](../Page/宜昌市第五中学.md "wikilink")
  - [宜昌市第六中学](../Page/宜昌市第六中学.md "wikilink")
  - [宜昌市第七中学](../Page/宜昌市第七中学.md "wikilink")
  - [宜昌市第八中学](../Page/宜昌市第八中学.md "wikilink")（增名：三峡大学附属中学）
  - [宜昌市第九中学](../Page/宜昌市第九中学.md "wikilink")
  - [宜昌市第十中学](../Page/宜昌市第十中学.md "wikilink")（增名：宜昌市[雷锋中学](../Page/雷锋.md "wikilink")）
  - [宜昌市第十一中学](../Page/宜昌市第十一中学.md "wikilink")
  - [宜昌市第十二中学](../Page/宜昌市第十二中学.md "wikilink")（增名：宜昌市外国语学校）
  - [宜昌市第十三中学](../Page/宜昌市第十三中学.md "wikilink")（增名：宜昌市[田家炳高级中学](../Page/田家炳.md "wikilink")）
  - [宜昌市第十四中学](../Page/宜昌市第十四中学.md "wikilink")
  - [宜昌市第十五中学](../Page/宜昌市第十五中学.md "wikilink")
  - [宜昌市第十六中学](../Page/宜昌市第十六中学.md "wikilink")
  - [宜昌市第十七中学](../Page/宜昌市第十七中学.md "wikilink")（原花艳中学）
  - [宜昌市第十八中学](../Page/宜昌市第十八中学.md "wikilink")
  - [宜昌市第十九中学](../Page/宜昌市第十九中学.md "wikilink")（筹备建设）\[34\]
  - [宜昌市第二十中学](../Page/宜昌市第二十中学.md "wikilink")（原葛洲坝六中
    葛洲坝高级中学）（增名：宜昌市葛洲坝中学）
  - [宜昌市第二十一中学](../Page/宜昌市第二十一中学.md "wikilink")（原葛洲坝五中 葛洲坝西陵中学）
  - [宜昌市第二十二中学](../Page/宜昌市第二十二中学.md "wikilink")（原葛洲坝一中 葛洲坝外国语学校）
  - [宜昌市第二十三中学](../Page/宜昌市第二十三中学.md "wikilink")（原葛洲坝七中 葛洲坝三江中学）
  - [宜昌市第二十四中学](../Page/宜昌市第二十四中学.md "wikilink")（原葛洲坝三中 葛洲坝明珠中学）
  - [宜昌市第二十五中学](../Page/宜昌市第二十五中学.md "wikilink")（十六中住宿部）（原葛洲坝四中 葛洲坝高峡中学）
  - [宜昌市第二十六中学](../Page/宜昌市第二十六中学.md "wikilink")（原猇亭中学）
  - [宜昌市第二十七中学](../Page/宜昌市第二十七中学.md "wikilink")（原点军一中）
  - [宜昌市第二十八中学](../Page/宜昌市第二十八中学.md "wikilink")（原点军四中）
  - [宜昌市第二十九中学](../Page/宜昌市第二十九中学.md "wikilink")（原点军五中）
  - [宜昌市第三十中学](../Page/宜昌市第三十中学.md "wikilink")（从[宜昌外校初中部分离出来](../Page/宜昌外校.md "wikilink")，2016年9月已正式投入运营）\[35\]
  - [宜昌市第三十一中学](../Page/宜昌市第三十一中学.md "wikilink")（增名：宜昌市东山中学，曾拟名宜昌市绿萝路中学）\[36\]

高新区托管区域：

  - [白洋中学](../Page/白洋中学.md "wikilink")
  - [雅畈中学](../Page/雅畈中学.md "wikilink")
  - [土门初中](../Page/土门初中.md "wikilink")

##### 其他

  - [宜昌市特殊教育学校](../Page/宜昌市特殊教育学校.md "wikilink")
  - [宜昌市机电工程学校](../Page/宜昌市机电工程学校.md "wikilink")【原宜昌市机电工程学校和夷陵区职业教育中心合并组建】
  - [宜昌市三峡中等专业学校](../Page/宜昌市三峡中等专业学校.md "wikilink")【原宜昌城市建设学校、宜昌市水利电力学校、宜昌市艺术学校、葛洲坝旅游学校（原葛洲坝二中）和宜昌市商业学校合并组建】
  - [湖北三峡技师学院](../Page/湖北三峡技师学院.md "wikilink")【原宜昌市工业技工学校（国家重点）、宜昌市第一技工学校（国家重点）、宜昌市机电技工学校和宜昌市纺织技工学校合并组建】
  - [宜昌市广播电视大学](../Page/宜昌市广播电视大学.md "wikilink")

### 医疗卫生

#### 宜昌市主要公立医院

宜昌市中心人民医院综合楼.jpg|市中心医院 The first hospital of yichang.jpg|市一医院
三峡大学仁和医院.jpg|三峡大学仁和医院 Yichang Yiling Hospital.jpg|夷陵医院

  - [宜昌市中心人民医院](../Page/宜昌市中心人民医院.md "wikilink")（三峡大学第一临床医学院
    三峡大学附属中心人民医院）【另含江南院区（点军区人民医院）、坝区分院（三峡坝区急救中心）】
  - [宜昌市第一人民医院](../Page/宜昌市第一人民医院.md "wikilink")（三峡大学人民医院）【另含康复分院（又名宜昌市康复医院）】
  - [宜昌市第二人民医院](../Page/宜昌市第二人民医院.md "wikilink")（三峡大学第二人民医院）【另含肿瘤院区（又名宜昌市肿瘤医院）】
  - [宜昌市第三人民医院](../Page/宜昌市第三人民医院.md "wikilink")（湖北三峡职业技术学院临床医学院）
  - [三峡大学仁和医院](../Page/三峡大学仁和医院.md "wikilink")（三峡大学第二临床医学院）【另含仁济院区、枝江铁路医院】
  - [葛洲坝集团中心医院](../Page/葛洲坝集团中心医院.md "wikilink")（三峡大学第三临床医学院
    武汉大学中南医院三峡医院）【另含三峡院区（原葛洲坝集团三峡医院）】
  - [宜昌市中医医院](../Page/宜昌市中医医院.md "wikilink")（三峡大学中医临床医学院 三峡大学中医医院
    湖北中医药大学附属宜昌医院）
  - [宜昌市妇幼保健院](../Page/宜昌市妇幼保健院.md "wikilink")（三峡大学妇女儿童临床医学院）
  - [宜昌市儿童医院](../Page/宜昌市儿童医院.md "wikilink") （在建）
  - [湖北三峡职业技术学院附属医院](../Page/湖北三峡职业技术学院附属医院.md "wikilink")（原宜昌市卫生学校附属医院）
  - [宜昌市优抚医院](../Page/宜昌市优抚医院.md "wikilink")（宜昌市精神卫生中心）
  - [宜昌市疾病预防控制中心](../Page/宜昌市疾病预防控制中心.md "wikilink")（三峡大学公共卫生研究中心
    宜昌市健康管理中心）【设东山工作区、一马路工作区、杨岔路工作区】
  - [宜昌市中心血站](../Page/宜昌市中心血站.md "wikilink")
  - [宜昌市第五人民医院](../Page/宜昌市第五人民医院.md "wikilink")（又名宜昌市血防医院，原猇亭区人民医院）
  - [宜昌市夷陵医院](../Page/宜昌市夷陵医院.md "wikilink")（原宜昌县人民医院）

## 宗教

佛教、道教、天主教、基督新教和伊斯兰教五种宗教在宜昌都有活动场所和信教群众。全市有市级爱国宗教团体4个，分别是市天主教爱国会，市基督教“两会”（市基督教协会、市基督教“三自”爱国运动委员会），市佛教协会，城区清真寺暨回族民管会，已批准登记的宗教活动场所56处。有宗教教职人员133人，其中天主教20人、基督新教18人、佛教56人、道教37人、伊斯兰教2人。宗教教职人员中，有省政协常委1人、市人大代表1人、市政协委员7人。全市有信教群众4万余人，其中佛教约1.3万人，道教0.55万人，天主教0.64万人，基督新教1.2万人，伊斯兰0.4万人。\[37\]

  - 佛教：古佛寺、[玉泉寺](../Page/玉泉寺_\(当阳市\).md "wikilink")
  - 道教：长阳中武当道观、远安鸣凤山道观
  - 基督新教：圣雅各堂
  - 天主教：圣方济各主教座堂 [宜昌教区](../Page/天主教宜昌教区.md "wikilink")

St. Francis of Assisi Cathedral,Yichang3.jpg|宜昌天主教堂

当阳玉泉寺.jpg|玉泉寺 古慈寺 (宜昌市) 02.jpg|古慈寺 观音禅寺 (小溪塔街道).jpg|观音禅寺 Saint James's
Church,Yichang.jpg|宜昌基督新教禮拜堂

## 城市荣誉

  - 2000年，宜昌荣膺第二批[中国优秀旅游城市](../Page/中国优秀旅游城市.md "wikilink")。
  - 2003年，湖北省政府首次明确将宜昌确定为省域副中心城市，要求其更好地发挥对[鄂西南地区的辐射带动作用](../Page/鄂西南.md "wikilink")。
  - 2005年，宜昌市荣获[建设部颁发的第八批](../Page/中华人民共和国住房和城乡建设部.md "wikilink")[国家园林城市](../Page/国家园林城市.md "wikilink")。
  - 2006年6月，宜昌被评为浙商（省外）最佳投资城市。\[38\]
  - 2007年8月，宜昌市荣膺“中部最佳投资城市”称号。\[39\]
  - 2008年，宜昌市入围2008年[中国综合实力百强城市名单](../Page/中国综合实力百强城市.md "wikilink")。\[40\]
  - 2008年11月，宜昌市被[全国爱卫会命名为](../Page/全国爱国卫生运动委员会.md "wikilink")“[国家卫生城市](../Page/国家卫生城市.md "wikilink")”\[41\]，也是湖北省第一个获此殊荣的城市\[42\]。　　
  - 2009年12月28日，宜昌市被[中华见义勇为基金会授予](../Page/中华见义勇为基金会.md "wikilink")“全国见义勇为城市奖”，是湖北省唯一获此殊荣的城市\[43\]。
  - 2010年1月，宜昌被[国家人口计生委命名为](../Page/中华人民共和国国家人口和计划生育委员会.md "wikilink")“[全国首批人口计划生育综合改革示范市](../Page/全国首批人口计划生育综合改革示范市.md "wikilink")”\[44\]\[45\]。
  - 2010年2月9日，宜昌市被[国家环境保护部授予](../Page/中华人民共和国环境保护部.md "wikilink")[国家环境保护模范城市称号](../Page/国家环境保护模范城市.md "wikilink")\[46\]，也是湖北省唯一获此殊荣的城市。
  - 2010年1月18日，宜昌市获得[第六届中华宝钢环境奖城镇环境类优秀奖](../Page/第六届中华宝钢环境奖.md "wikilink")\[47\]。
  - 2011年12月20日，宜昌市荣获[全国文明城市称号](../Page/全国文明城市.md "wikilink")，亦是湖北省第一个获此殊荣的城市\[48\]\[49\]。
  - 2011年7月30日，宜昌市荣获“2011中国最具幸福感城市”和“中国最佳投资城市”\[50\]
  - 2012年5月31日，宜昌市被[中央社会管理综合治理委员会授予](../Page/中央社会管理综合治理委员会.md "wikilink")“2009——2012年度全国社会管理综合治理优秀市”（长安杯）。\[51\]
  - 2012年7月9日，宜昌市被[全国绿化委员会](../Page/全国绿化委员会.md "wikilink")、[国家林业局授予](../Page/国家林业局.md "wikilink")“[国家森林城市](../Page/国家森林城市.md "wikilink")”称号。\[52\]

## 友好城市

1.  [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")[梅斯](../Page/梅斯.md "wikilink")（[法国](../Page/法国.md "wikilink")
    1991年1月28日）
2.  [Flag_of_Sweden.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sweden.svg "fig:Flag_of_Sweden.svg")[塞德港](../Page/塞德港.md "wikilink")（[瑞典](../Page/瑞典.md "wikilink")
    1994年10月22日）
3.  [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg")[路德维希堡县](../Page/路德维希堡县.md "wikilink")（[德国](../Page/德国.md "wikilink")
    1995年5月3日）
4.  [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg")[扎波罗热](../Page/扎波罗热.md "wikilink")（[乌克兰](../Page/乌克兰.md "wikilink")
    1997年10月16日）
5.  [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")[瓦朗谢讷](../Page/瓦朗谢讷.md "wikilink")（法国
    1998年4月23日）
6.  [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg")[柏崎市](../Page/柏崎市.md "wikilink")（[日本](../Page/日本.md "wikilink")
    1998年11月）
7.  [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg")[查尔斯顿](../Page/查尔斯顿.md "wikilink")（[澳大利亚](../Page/澳大利亚.md "wikilink")
    2001年8月10日）
8.  [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg")[伊瓜苏](../Page/伊瓜苏.md "wikilink")（[巴西](../Page/巴西.md "wikilink")
    2006年7月24日）
9.  [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")[华盛顿县
    (俄勒冈州)](../Page/华盛顿县_\(俄勒冈州\).md "wikilink")（[美国](../Page/美国.md "wikilink")
    2006年8月31日）
10. [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg")[四日市](../Page/四日市.md "wikilink")（日本
    2011年5月）

## 外部連結

<div class="references-small">

  - 官方

<!-- end list -->

  - [中国·宜昌网](http://www.yichang.gov.cn/)

<!-- end list -->

  - 旅游

<!-- end list -->

  - [宜昌市旅游局](http://lyw.yichang.gov.cn/)

<!-- end list -->

  - 媒体

<!-- end list -->

  - [三峡宜昌网——中共宜昌市委机关报三峡日报网站](http://www.cn3x.com.cn/)
  - [三峡广电传媒网](https://web.archive.org/web/20100207193605/http://www.sxgdtv.com/)（宜昌三峡广播电视总台）
  - [三峡新闻网](http://www.sxxw.net/)（湖北日报传媒集团三峡分社）

<!-- end list -->

  - 地图

<!-- end list -->

  - [Google地图 - 宜昌市](https://www.google.com/maps/place/Yichang)

## 注释

<div style="font-size: small; -moz-column-count:2; column-count:2;">

<references group="注" />

</div>

## 资料来源

  - 网络参考

<div style="font-size: small; -moz-column-count:2; column-count:2;">

<references group="参考" />

</div>

  - 参考文献

{{-}}

[\*](../Category/宜昌.md "wikilink")
[Category:湖北地级市](../Category/湖北地级市.md "wikilink")
[Category:长江沿岸城市](../Category/长江沿岸城市.md "wikilink")
[Category:国家园林城市](../Category/国家园林城市.md "wikilink")
[鄂](../Category/国家卫生城市.md "wikilink")
[3](../Category/全国文明城市.md "wikilink")
[Category:国家环境保护模范城市](../Category/国家环境保护模范城市.md "wikilink")
[Category:国家森林城市](../Category/国家森林城市.md "wikilink")

1.  [中国CFO论坛永久会址宜昌启动](http://www.hubei.gov.cn/zwgk/szsmlm/shzqb/201608/t20160827_889827.shtml)
2.  七成山地、二成丘陵、一成平原
3.
4.
5.
6.
7.
8.
9.
10. [【宜昌号召生二孩】湖北宜昌号召生二孩：面临怎样的人口形势？(3)](http://www.southmoney.com/redianxinwen/201609/749240_3.html)
11. [澎湃新闻：宜昌公开信号召生二孩，希望公职人员做表率、形成生育小气候](http://www.thepaper.cn/newsDetail_forward_1531980)
12.
13. <http://www.yichang.gov.cn/content-14819-952909-1.html>
14.
15. [演员章宇昂参演抗战大戏《最后的国门》](http://mt.sohu.com/20150519/n413354624.shtml)
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29. [第九届中国国际民间艺术节概况](http://yc3j.cn3x.com.cn/content/2013-08/23/content_443924.htm)

30. [第三届中国宜昌长江钢琴音乐节概况](http://yc3j.cn3x.com.cn/content/2013-08/23/content_444002.htm)

31. [儿童公园](http://www.yichang.gov.cn/art/2008/8/11/art_6101_86524.html)
32. [双亭广场](http://www.yichang.gov.cn/art/2008/8/11/art_6101_86521.html)
33. [夷陵广场](http://www.yichang.gov.cn/art/2008/5/30/art_6101_79619.html)
34.
35.
36.
37.
38. [湖北宜昌成为浙商省外最佳投资城市](http://news.sohu.com/20060603/n243545147.shtml)，搜狐新闻
39. [宜昌市荣膺“中部最佳投资城市”称号](http://www.cn3x.com.cn/gb/yc/2007-8/20/063835729.html)
40. 排名77位，属第三集团第二等（第53位到第89位，500亿以上）
41. [中华人民共和国卫生部网站](../Page/中华人民共和国卫生部.md "wikilink")（2008年11月17日）：[全国爱卫会关于命名湖北省宜昌市为国家卫生城市的决定](http://www.moh.gov.cn/sofpro/cms/previewjspfile/zwgkzt/cms_0000000000000000131_tpl.jsp?requestCode=38337&CategoryID=3962)
42. [楚天都市报多媒体报网站](../Page/楚天都市报.md "wikilink")（2008年11月26日）：[宜昌荣膺国家卫生城市
    武汉黄石襄樊冲关未果](http://ctdsb.cnhubei.com/html/ctdsb/20081126/ctdsb555731.html)

43.
44. 中国·宜昌(2010年2月6日)：[宜昌入选全国首批计生综合改革示范市](http://www.yichang.gov.cn/art/2010/2/6/art_164_221092.html)
45. 湖北新闻网(2010年2月5日)：[宜昌跻身全国首批人口计划生育综合改革示范市](http://www.hb.chinanews.com.cn/news/2010/0205/46630.html)
46.
47.
48.
49.
50.
51.
52.