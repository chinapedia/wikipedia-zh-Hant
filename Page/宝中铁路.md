**宝中铁路**是中國西北的一條[鐵路線](../Page/中華人民共和國鐵路運輸.md "wikilink")：东南边的起点是[陕西省的](../Page/陕西省.md "wikilink")[宝鸡](../Page/宝鸡.md "wikilink")，经过[甘肃](../Page/甘肃.md "wikilink")[平涼市](../Page/平涼市.md "wikilink")，最後后西北边的终点是[宁夏](../Page/宁夏.md "wikilink")[中卫市的](../Page/中卫市.md "wikilink")[镇罗堡](../Page/镇罗堡.md "wikilink")，全长498.19公里\[1\]。該鐵路1990年開始興建\[2\]，1996年開始通車營運。其中在陕西境内归[西安铁路局管辖](../Page/西安铁路局.md "wikilink")，在甘肃和宁夏境内归[兰州铁路局管辖](../Page/兰州铁路局.md "wikilink")。宝中铁路为单线电气化铁路，途径[中卫](../Page/中卫.md "wikilink")，[同心](../Page/同心.md "wikilink")，[固原](../Page/固原.md "wikilink")，[平凉](../Page/平凉.md "wikilink")，[华庭](../Page/华亭县.md "wikilink")，[陇县](../Page/陇县.md "wikilink")，[千阳](../Page/千阳.md "wikilink")，[宝鸡](../Page/宝鸡.md "wikilink")。是连接[陕西](../Page/陕西.md "wikilink")，[甘肃](../Page/甘肃.md "wikilink")，[宁夏的咽喉要道](../Page/宁夏.md "wikilink")。

## 与其它铁路线的连接

  - [宝鸡站](../Page/宝鸡站.md "wikilink")：[宝成铁路](../Page/宝成铁路.md "wikilink")
  - [卧龙寺站](../Page/卧龙寺站.md "wikilink")：[陇海铁路](../Page/陇海铁路.md "wikilink")
  - [平凉南站](../Page/平凉南站.md "wikilink")：[西平铁路](../Page/西平铁路.md "wikilink")
  - [安口窑站](../Page/安口窑站.md "wikilink")：安口南线
  - [柳家庄站](../Page/柳家庄站.md "wikilink")：[包兰铁路](../Page/包兰铁路.md "wikilink")
  - [中卫站](../Page/中卫站.md "wikilink")：[太中银铁路](../Page/太中银铁路.md "wikilink")

## 参考资料

<div class="references-small">

<references />

</div>

[Category:陕西铁路](../Category/陕西铁路.md "wikilink")
[Category:甘肃铁路](../Category/甘肃铁路.md "wikilink")
[Category:宁夏回族自治区铁路线](../Category/宁夏回族自治区铁路线.md "wikilink")
[Category:中国铁路线](../Category/中国铁路线.md "wikilink")

1.  [—一碗鸡蛋面（宁夏新闻网）](http://www.nxnews.net/785/2008-9-17/22@327721.htm)
2.  [宁夏交通概况](http://www.nx.xinhuanet.com/hy/2009-06/08/content_16749002.htm)