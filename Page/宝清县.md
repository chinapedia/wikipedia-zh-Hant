**宝清县**在[黑龙江省东部](../Page/黑龙江省.md "wikilink")、[挠力河上游](../Page/挠力河.md "wikilink")，是[双鸭山市下辖的一个县](../Page/双鸭山市.md "wikilink")。1916年设县，因县城南边的宝清河得名，“宝清”在满语的意思为“猴子”。

## 行政区划

下辖6个镇、4个乡、13个[类似乡级单位](../Page/类似乡级单位.md "wikilink")：\[1\]。

## 旅游

第四批全国重点烈士纪念建筑物保护单位：[珍宝岛革命烈士陵园](../Page/珍宝岛.md "wikilink")

## 参考资料

## 外部链接

  - [宝清县人民政府网](http://www.hlbaoqing.gov.cn/)

[\*](../Category/宝清县.md "wikilink")
[双鸭山](../Category/黑龙江省县份.md "wikilink")

1.