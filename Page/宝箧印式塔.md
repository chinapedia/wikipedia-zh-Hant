[Houkyouintou_English.gif](https://zh.wikipedia.org/wiki/File:Houkyouintou_English.gif "fig:Houkyouintou_English.gif")
**宝箧印式塔**（，译自日语“”）也叫**宝箧印塔**、**金涂塔**、**阿育王塔**，是一类实心[塔](../Page/塔.md "wikilink")。佛经中“宝箧印”全名“宝箧印陀罗尼”，指宝箧印真言咒，供奉它的塔就是宝箧印塔。

## 概论

宝箧印式塔造型平面四方，由塔基、塔身、塔檐与塔刹四部分组成，其中塔檐多在塔身上四角向上翻挑，形象地被称为“山花蕉叶”。这种形制是由古[印度的](../Page/印度.md "wikilink")“[窣堵坡](../Page/窣堵坡.md "wikilink")”发展而来，由印度人传入[尼泊尔再传入中国](../Page/尼泊尔.md "wikilink")，并在[东亚](../Page/东亚.md "wikilink")、[东南亚也有所发展](../Page/东南亚.md "wikilink")。

宝箧印式塔的用途最早用以供奉[宝箧印陀罗尼及](../Page/宝箧印陀罗尼.md "wikilink")《[宝箧印陀罗尼经](../Page/宝箧印陀罗尼经.md "wikilink")》，或可供奉佛[舍利](../Page/舍利.md "wikilink")，因经上说将本经书或[陀罗尼咒置於塔中](../Page/陀罗尼.md "wikilink")，此塔就是九十九百千萬[俱胝](../Page/俱胝.md "wikilink")[如來](../Page/如來.md "wikilink")[窣堵坡](../Page/窣堵坡.md "wikilink")，爲一切如来所[加持护佑](../Page/加持.md "wikilink")。後来逐渐成为一种纪念性的结构，在[藏传佛教中还有小型](../Page/藏传佛教.md "wikilink")**宝箧印塔**，可供於佛龛前，造型大者则可作为寺庙中的[浮图塔](../Page/浮图塔.md "wikilink")。

在[中国](../Page/中国.md "wikilink")，宝箧印式塔在[三国时代就开始建造](../Page/三国.md "wikilink")，北魏云冈石窟、隋唐时南响山石窟、敦煌是壁画中，都有这种塔的造型。[五代时期](../Page/五代.md "wikilink")，因吴越国王钱弘俶仿[阿育王造塔八万四千](../Page/阿育王.md "wikilink")，使宝箧印式塔有了广泛的发展，也保存下大量遗迹，有的甚至在那时就流传到了[日本](../Page/日本.md "wikilink")。以後[宋](../Page/宋.md "wikilink")、[元](../Page/元.md "wikilink")、[明](../Page/明.md "wikilink")、[清各代也建造了大量的宝箧印式塔](../Page/清.md "wikilink")。在中国分佈在长江以南，北方很少见。

## 宝箧印式塔举例

  - [新疆](../Page/新疆.md "wikilink")[吐鲁番交河古城的一百零一塔群](../Page/吐鲁番.md "wikilink")（唐）
  - 日本细川护立侯处的宝箧印塔（五代）
  - 福建泉州开元寺位于山顶的宝箧印塔是目前发现的最大的宝箧印式塔，开元寺内另有多座宝箧印式塔
  - 浙江舟山普陀山的[多宝塔是现存的建于元代最大的宝箧印式塔](../Page/普陀山多宝塔.md "wikilink")
  - [北京西山](../Page/北京.md "wikilink")[八大处](../Page/八大处.md "wikilink")[灵光寺中的宝箧印式塔](../Page/灵光寺.md "wikilink")。
  - [山西](../Page/山西.md "wikilink")[代县](../Page/代县.md "wikilink")[阿育王塔](../Page/代县阿育王塔.md "wikilink")（元）
  - [山西](../Page/山西.md "wikilink")[晋源](../Page/晋源.md "wikilink")[阿育王塔](../Page/晋源阿育王塔.md "wikilink")（明）

<File:taizita.jpg>|[普陀山多宝塔](../Page/普陀山多宝塔.md "wikilink") <File:HuiAn> -
Luoyang Bridge - P1240001.JPG|福建省泉州市惠安县洛阳桥桥身中部的宝箧印经式塔“月光菩萨塔”

[category:宝箧印式塔](../Page/category:宝箧印式塔.md "wikilink")

[Category:佛教建筑物](../Category/佛教建筑物.md "wikilink")