[Fourier_Series.svg](https://zh.wikipedia.org/wiki/File:Fourier_Series.svg "fig:Fourier_Series.svg")的[傅立葉級數的前四項](../Page/傅立葉級數.md "wikilink")。傅立葉級數是實分析的一項重要工具\]\]

**實分析**（，也称作**实变函数论**，）或**實數分析**是處理[實數及實函數的](../Page/實數.md "wikilink")[數學分析](../Page/數學分析.md "wikilink")。專門實數[函數及](../Page/函數.md "wikilink")[數列的解析特性](../Page/數列.md "wikilink")，包括實數數列的[極限](../Page/極限_\(數列\).md "wikilink")，實函數的[微分及](../Page/微分.md "wikilink")[積分](../Page/積分.md "wikilink")、[連續性](../Page/連續性.md "wikilink")，[光滑性以及其他相關性質](../Page/光滑函数.md "wikilink")。

實分析常以基礎[集合論](../Page/集合論.md "wikilink")，函數概念定義等等開始。

## 內容

### 實數的構造

有許多種將[實數定義為](../Page/實數.md "wikilink")[有序域的方式](../Page/有序域.md "wikilink")。合成的作法會提供許多實數的[公理](../Page/公理.md "wikilink")，將實數變成完備有序[域](../Page/域_\(數學\).md "wikilink")。在一般[集合论的公理下](../Page/集合论.md "wikilink")，可以證明這些公理都是[明確的](../Page/范畴论.md "wikilink")，也就是說有一個公理的[模型](../Page/模型论.md "wikilink")，任兩個模型都是[同构的](../Page/同构.md "wikilink")。這些模型中需要有一個有明確的定義，而大部份的模型都可以用實數為有序域時的基本性質來得到。

### 實數的有序性

實數有許多重要的特性是和數學中[格的定義有關](../Page/格_\(数学\).md "wikilink")，這些性質也是複數所沒有的。其中最重要的是，實數形成[有序域](../Page/有序域.md "wikilink")
，實數的有序滿足反對稱性、傳遞性及完全性，屬於[全序关系](../Page/全序关系.md "wikilink")，而且實數有[最小上限屬性](../Page/最小上限屬性.md "wikilink")。實數中的[偏序关系帶來了實變分析中許多重要的定理](../Page/偏序关系.md "wikilink")，例如[单调收敛定理](../Page/单调收敛定理.md "wikilink")、[介值定理及](../Page/介值定理.md "wikilink")[中值定理](../Page/中值定理.md "wikilink")。

在實變分析中這些定理只針對實數，不過許多的結果可以應用在其他的。特別是許多[泛函分析及](../Page/泛函分析.md "wikilink")中的概念是來自實數中概念的擴展，這類的擴展包括及的理論。也有數學家考慮複數數列的實部及虛部，例如[算子數列的](../Page/算子.md "wikilink")。

### 序列

序列是一個[定義域為](../Page/定義域.md "wikilink")[可數](../Page/可數.md "wikilink")[全序集合的](../Page/全序关系.md "wikilink")[函数](../Page/函数.md "wikilink")，多半會讓定義域是[自然數或是所有整數](../Page/自然數.md "wikilink")\[1\]。例如，一個實數的序列為以下定義的映射\(a:\mathbb{N}\to\mathbb{R},\ n\mapsto a_n\)，常會表示為\((a_n)=(a_n)_{n\in\mathbb{N}}=(a_1, a_2, a_3, \cdots)\)。若一序列會慢慢的接近一個[极限](../Page/极限_\(数学\).md "wikilink")（也就是存在\(\lim_{n\to\infty}a_n\)
），稱此序列為**收斂**，否則則稱此序列為**發散**。

### 極限

極限是指[函数或](../Page/函数.md "wikilink")[序列在其輸入接近一定值時](../Page/序列.md "wikilink")，其輸出數值所接近的特定定值\[2\]。極限是[微积分学及廣義](../Page/微积分学.md "wikilink")[数学分析的基礎](../Page/数学分析.md "wikilink")，[連續函數](../Page/連續函數_\(拓撲學\).md "wikilink")、[导数及](../Page/导数.md "wikilink")[积分也是利用極限來定義](../Page/积分.md "wikilink")。

### 連續函數

若[函数的輸入及輸出值都是](../Page/函数.md "wikilink")[实数](../Page/实数.md "wikilink")，可以表示成[笛卡儿坐标系上的](../Page/笛卡儿坐标系.md "wikilink")[图形](../Page/函数图形.md "wikilink")。粗略來說，若函数图形是一條連續未分割的[曲线](../Page/曲线.md "wikilink")，其中沒有「洞」或是「斷點」，函數即為連續函數。

針對上述粗略的定義，在數學上有許多嚴謹的定義。這些定義彼此是[等价的](../Page/等价关系.md "wikilink")，因此會用最簡單而方便的定義來確認一個函數是否是連續，在以下的定義中

\[f\colon I \rightarrow \mathbf R.\]
是一個定義在實數\(\boldsymbol{R}\)以內[子集的函數](../Page/子集.md "wikilink")，子集*I*稱為函數*f*的定義域。子集*I*的一些可能選擇包括\(I=\boldsymbol{R}\)（所有實數）、以下的[開區間](../Page/開區間.md "wikilink")

\[I = (a, b) = \{x \in \mathbf R \,|\, a < x < b \},\]
或[閉區間](../Page/閉區間.md "wikilink")

\[I = [a, b] = \{x \in \mathbf R \,|\, a \leq x \leq b \}.\]
因此\(a\)及\(b\)是實數。

一致连续是連續函數中，比連續函數更強的性質。若*X*和*Y*是[實數子集](../Page/實數.md "wikilink")，函數\(f:X\rightarrow Y\)為[一致连续的條件是針對所有大於](../Page/一致连续.md "wikilink")0的實數\(\varepsilon\)，存在一實數\(\delta >0\)，使得針對所有的\(x,y\in X,\left \vert x-y \right \vert <\delta\)即表示\(\implies \left \vert f(x)-f(y) \right \vert <\varepsilon\)。

一致连续和每一點連续的差異在一致连续時，\(\delta\)值只和\(\varepsilon\)值有關，和該值在定義域中的位置無關。一般情況下，連續不意味著均勻連續。

### 級數

給定一無窮[序列](../Page/序列.md "wikilink")
\((a_n)\)，即可定義相關的級數為\(a_1+a_2+a_3+\cdots=\sum_{n\in\mathbb{N}}a_n\)，有時會簡稱為\(\sum a_n\)。級數的部份和\(\sum a_n\)為\(s_n=\sum_{j=1}^n a_j\)。級數\(\sum a_n\)收斂的條件是部份和的數列\((s_n)\)收斂，否則級數即稱為發散。收斂級數的和\(s=\sum_{n=1}^\infty a_n\)定義為\(s=\lim_{n\to\infty}s_n\).

[等比数列的和就是一個收斂級數](../Page/等比数列.md "wikilink")，也是[芝诺悖论的基礎](../Page/芝诺悖论.md "wikilink")：

\[\sum_{n=1}^\infty \frac{1}{2^n} = \frac{1}{2}+ \frac{1}{4}+ \frac{1}{8}+\cdots=1\].

以下的[調和級數即為發散級數](../Page/調和級數.md "wikilink")：

\[\sum_{n=1}^\infty\frac{1}{n}=1+\frac{1}{2}+\frac{1}{3}+\cdots=\infty\].

（此處\(=\infty\)不是嚴謹的表示方式，只是表示部份和會無限制的成長）

### 微分

函數\(f\)在\(a\)位置的[導數為以下的](../Page/導數.md "wikilink")[函數極限](../Page/函數極限.md "wikilink")

\[f'(a)=\lim_{h\to 0}\frac{f(a+h)-f(a)}{h}\]

若導數在所有位置都存在，稱函數為可微分，可以再繼續計算函數的高階導數。

也可以將函數依其微分分類來區分。分類\(C^0\)包括所有連續函數，分類\(C^1\)包括所有導數連續的[可微函数](../Page/可微函数.md "wikilink")，這類函數稱為「連續可微」。分類\(C^1\)是指其導數在分類\(C^1\)中的函數。一般來說，分類\(C^k\)可以用[递归方式定義](../Page/递归.md "wikilink")，定義方式是宣告分類\(C^0\)是所有的連續函數，而分類*\(C^k\)*（\(k\)為正整數）是所有可微，而且其導數為\(C^{k-1}\)的函數。而分類*\(C^k\)*包括在分類\(C^{k-1}\)中，對所有的正整數*\(k\)*都成立
。分類\(C^\infty\)是所有*\(C^k\)*的交集，其中*\(k\)*為所有的非負整數。\(C^\omega\)包括所有的[解析函数](../Page/解析函数.md "wikilink")，是分類\(C^\infty\)的嚴格子集。

### 積分

#### 黎曼積分

黎曼積分定義函數的[黎曼和](../Page/黎曼和.md "wikilink")，對應為一個區間內的標記分區（tagged
partitions）。令\([a,b]\)為實數下的封閉[區間](../Page/區間.md "wikilink")，則在區間\([a,b]\)內的標記分區為有限數列

\[a = x_0 \le t_1 \le x_1 \le t_2 \le x_2 \le \cdots \le x_{n-1} \le t_n \le x_n = b . \,\!\]

將區間\([a,b]\)分隔為\(n\)個下標為\(i\)子區間\([x_{i-1},x_i]\)，每一個用不同的點\(t_i\in [x_{i-1},x_i]\)來標記。函數f對應標記分區的黎曼和定義為

\[\sum_{i=1}^{n} f(t_i) \Delta_i ;\]

則和的每一項都是長方形的面積，其高為函數在給定子區間內，標示點的數值，寬和子區間的寬相等。令\(\Delta_i=x_i-x_{i-1}\)為子區間*\(i\)*的寬，則標記分區的網格為長子區間中最寬區間的寬度\(\mathrm{max}_{i=1\ldots n}\Delta_i\)。函數\(f\)在區間\([a,b]\)內的黎曼積分等於\(S\)若：

  -
    對所有\(\varepsilon >0\)，存在\(\delta>0\)使得，對於任何有標示，且網格小於\(\delta\)的區間\([a,b]\)，以下的式子成立
    \[\left| S - \sum_{i=1}^{n} f(t_i)\Delta_i \right| < \varepsilon.\]

若選定的標示都是每個區間內函數的最大值（或最小值），黎曼積分就會成為上（或下）[达布和](../Page/达布积分.md "wikilink")，因此黎曼積分和[达布积分有緊密的關係](../Page/达布积分.md "wikilink")。

#### 勒貝格積分

勒貝格積分是一種積分概念，可以將積分延伸到更大範圍的函數，同時也拓展函數的[定义域](../Page/定义域.md "wikilink")。

### 分布

分布或是[广义函数是一種將](../Page/广义函数.md "wikilink")[函数擴展後產生的概念](../Page/函数.md "wikilink")。透過分布可以針對一些在傳統定義下其導數不存在的函數進行[微分](../Page/微分.md "wikilink")（例如[单位阶跃函数](../Page/单位阶跃函数.md "wikilink")）。而任何[局部可积函数都一定會有广义函数下的導數](../Page/局部可积函数.md "wikilink")。

### 和複變分析的關係

实变函数论是[数学分析的一部份](../Page/数学分析.md "wikilink")，探討像數列及其極限、連續性、函數的[导数及](../Page/导数.md "wikilink")[积分](../Page/积分.md "wikilink")。實變分析專注在[实数](../Page/实数.md "wikilink")，多半會包括正負[無窮大以形成](../Page/無窮大.md "wikilink")[擴展實軸](../Page/擴展實軸.md "wikilink")。實變分析和研究[复数對應性質的](../Page/复数_\(数学\).md "wikilink")[複分析緊密相關](../Page/複分析.md "wikilink")。在複分析中，很自然的會對[全純函數定義](../Page/全純函數.md "wikilink")[导数](../Page/导数.md "wikilink")，全純函數有許多有用的性質，包括多次可微、可以用[幂级数表示](../Page/幂级数.md "wikilink")，而且滿足[柯西積分公式](../Page/柯西積分公式.md "wikilink")。

實變分析中也很自然的去考慮[可微](../Page/可微.md "wikilink")、[光滑函數或](../Page/光滑函數.md "wikilink")[调和函数](../Page/调和函数.md "wikilink")，這些也常常用到，不過仍少了一些複變中全純函數中有力的性質。而且[代数基本定理若以複數表示時會比較簡單](../Page/代数基本定理.md "wikilink")。

複變中[解析函数理論的技巧也可以用在實變分析](../Page/解析函数.md "wikilink")，例如應用[留数定理來計算實變函數的](../Page/留数定理.md "wikilink")[定積分](../Page/定積分.md "wikilink")。

## 重要結果

實分析的重要結果包括[波爾查諾－魏爾斯特拉斯定理](../Page/波爾查諾－魏爾斯特拉斯定理.md "wikilink")、[海涅－博雷尔定理](../Page/海涅－博雷尔定理.md "wikilink")、[介值定理](../Page/介值定理.md "wikilink")、[中值定理](../Page/中值定理.md "wikilink")、[微积分基本定理及](../Page/微积分基本定理.md "wikilink")[单调收敛定理](../Page/单调收敛定理.md "wikilink")。

實分析的許多概念可以擴展到廣義的[度量空间](../Page/度量空间.md "wikilink")，包括[巴拿赫空间及](../Page/巴拿赫空间.md "wikilink")[希尔伯特空间](../Page/希尔伯特空间.md "wikilink")。

## 相關條目

  -
  - [时标微积分](../Page/时标微积分.md "wikilink")

  -
  -
  - [複分析](../Page/複分析.md "wikilink")

## 参考资料

[实分析](../Category/实分析.md "wikilink")

1.
2.