**實時**可參考：

  - 實時系統，它應對時間性的事件或信號。這些系統包括：
      - [實時計算](../Page/實時計算.md "wikilink")，研究的電腦系統受到實時約束

      - [實時操作系統](../Page/實時操作系統.md "wikilink")，多任務操作系統為實時應用

      - [實時圖像運算](../Page/實時圖像運算.md "wikilink")，生產和分析的實時圖像

      - [實時磁盤加密](../Page/實時磁盤加密.md "wikilink")，加密的數據，因為它的寫入磁盤。也被稱為“即時加密”（OTFE）

      - [實時Java](../Page/實時Java.md "wikilink")，技術開發的實時系統中的Java編程語言

      - Real-time Blackhole List, the first system to use [DNS
        blacklist](../Page/DNSBL.md "wikilink") technology

      - [實時傳輸協議及](../Page/實時傳輸協議.md "wikilink")[實時信息傳遞議定書](../Page/實時信息傳遞議定書.md "wikilink"),
        protocols for delivering audio and video over a computer network

      - [實時串流協議and](../Page/實時串流協議.md "wikilink")
        [實時控制協議](../Page/實時控制協議.md "wikilink"),
        protocols for controlling streaming media over a computer
        network

      - [實時保護](../Page/實時保護.md "wikilink"), automatic protection
        provided by computer security systems

      - [即時詮譯](../Page/即時詮譯.md "wikilink"), transcription of spoken
        words within a few seconds of their being spoken

      - （或稱「真實時境」），指影視作品的一種[敘述方式](../Page/敘事學.md "wikilink")，劇情進行的時間等同於真實經過的時間。

      - [即時戰略](../Page/即時戰略.md "wikilink"), a computer game genre of
        strategic war games

      - [實時戰術](../Page/實時戰術.md "wikilink"), a computer game genre of
        tactical war games

      - [實時聚合酶鏈反應](../Page/實時聚合酶鏈反應.md "wikilink"), a laboratory
        technique for amplification of DNA that provides quantitative
        results during the process

      - [實時商業智能](../Page/實時商業智能.md "wikilink"), the process of
        delivering information about business operations without any
        latency

      - [即時支付結算](../Page/即時支付結算.md "wikilink"), an online system for
        settling transactions between financial institutions
  - [Realtime Worlds](../Page/Realtime_Worlds.md "wikilink"), a
    BAFTA-winning Scottish game developer
  - [Real-Time
    Innovations](../Page/Real-Time_Innovations.md "wikilink"), an
    American software company
  - [實時組件](../Page/實時組件.md "wikilink"), distributor of [electronic
    components](../Page/electronic_components.md "wikilink")
  - [實時Cmix](../Page/實時Cmix.md "wikilink"), a music programming language
  - [實時時鐘](../Page/實時時鐘.md "wikilink")，一個可計算準確時間的電子計時器
  - [實時動態](../Page/實時動態.md "wikilink"), a satellite navigation technique
  - Real time, a synonym for
    [presentism](../Page/presentism_\(philosophy_of_time\).md "wikilink")
    in philosophy of time
  - [實時遊戲軟件](../Page/實時遊戲軟件.md "wikilink"), a former British video game
    developer
  - [Realtime (quartet)](../Page/Realtime_\(quartet\).md "wikilink"), a
    barbershop quartet
  - [*Real Time*](../Page/Real_Time_\(Doctor_Who\).md "wikilink"), a
    webcast based on the television programme *Doctor Who*
  - 《[與標馬艾的即時](../Page/與標馬艾的即時.md "wikilink")》，[HBO上的清談節目](../Page/HBO.md "wikilink")
  - "Fox Real Time", a headline update segment on the *[Fox News
    Live](../Page/Fox_News_Live.md "wikilink")* television program
  - [RealTime (computer
    tool)](../Page/RealTime_\(computer_tool\).md "wikilink"), a news
    reader and screen saver produced by
    [RealNetworks](../Page/RealNetworks.md "wikilink")
  - [Real Time (C:Real
    album)](../Page/Real_Time_\(C:Real_album\).md "wikilink") - album by
    Greek Group: C:Real
  - [實時廣播](../Page/實時廣播.md "wikilink"), a radio show on CBC 2

## 參見

  - [時間](../Page/時間.md "wikilink")