**实时传输协议**（****或简写****）是一个[网络传输协议](../Page/网络传输协议.md "wikilink")，它是由IETF的多媒体传输工作小组1996年在RFC
1889中公布的。

[国际电信联盟ITU](../Page/国际电信联盟.md "wikilink")-T也发布了自己的RTP文档，作为H.225.0，但是后来当IETF发布了关于它的稳定的标准RFC后就被取消了。它作为[因特网标准在RFC](../Page/因特网.md "wikilink")
3550（该文档的旧版本是RFC 1889）有详细说明。RFC 3551（STD 65，旧版本是RFC
1890）详细描述了使用最小控制的音频和视频会议。

RTP协议详细说明了在[互联网上传递音频和视频的标准数据包格式](../Page/互联网.md "wikilink")。它一开始被设计为一个[多播协议](../Page/多播.md "wikilink")，但后来被用在很多[单播应用中](../Page/单播.md "wikilink")。RTP协议常用于[流媒体系统](../Page/流媒体.md "wikilink")（配合RTSP协议），视频会议和[一键通](../Page/一键通.md "wikilink")（）系统（配合H.323或SIP），使它成为[IP电话产业的技术基础](../Page/IP电话.md "wikilink")。RTP协议和RTP控制协议[RTCP一起使用](../Page/RTCP.md "wikilink")，而且它是建立在[UDP协议上的](../Page/UDP协议.md "wikilink")。

## 封包結構

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>+位元</p></th>
<th><p>0-1</p></th>
<th><p>2</p></th>
<th><p>3</p></th>
<th><p>4-7</p></th>
<th><p>8</p></th>
<th><p>9-15</p></th>
<th><p>16-31</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0</p></td>
<td><p>Ver.</p></td>
<td><p>P</p></td>
<td><p>X</p></td>
<td><p>CC</p></td>
<td><p>M</p></td>
<td><p>PT</p></td>
<td><p>序號</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>Timestamp</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>64</p></td>
<td><p>SSRC identifier</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>96</p></td>
<td><p>... CSRC identifiers ...</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>96+（CC×32）</p></td>
<td><p>Additional header (optional), indicates length "AHL"</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>96+（CC×32）<br />
+（X×(AHL+16）)</p></td>
<td><p> <br />
Data<br />
 </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

**Ver**.（2 bits）是目前協定的版本號碼，目前版號是2。

**P**（1 bit）是用於RTP封包（packet）結束點的預留空間，視封包是否需要多餘的填塞空間。

**X**（1 bit）是否在使用延伸空間於封包之中。

**CC**（4 bits）包含了CSRC數目用於修正標頭（fixed header）。

**M**（1 bit）是用於應用等級以及其原型（profile）的定義。如果不為零表示目前的資料有特別的程式解譯。

**PT**（7 bits）是指payload的格式並決定將如何去由應用程式加以解譯。

**SSRC**是同步化來源。

## 参见

  - [Real time control protocol](../Page/RTCP.md "wikilink")（RTCP）
  - [Real Time Streaming Protocol (RTSP)](../Page/RTSP.md "wikilink")
  - [Secure Real-time Transport
    Protocol](../Page/Secure_Real-time_Transport_Protocol.md "wikilink")
  - [Stream Control Transmission
    Protocol](../Page/Stream_Control_Transmission_Protocol.md "wikilink")
  - [ZRTP](../Page/ZRTP.md "wikilink")
  - [Real time
    communications](../Page/Real_time_communications.md "wikilink")

## 参考書目

  - RTP: Audio and Video for the Internet by Colin Perkins, ISBN
    0672322498

[Category:串流](../Category/串流.md "wikilink")
[Category:应用层协议](../Category/应用层协议.md "wikilink")