**客家菜**，主要指[客家人的飲食](../Page/客家人.md "wikilink")，因[客家地区主体分布于闽粤赣交界处](../Page/客家地区.md "wikilink")，故又称**东江菜**、**闽西菜**、**赣南菜**。因為客家族群多遷徙且居住[華中](../Page/華中.md "wikilink")、[華南](../Page/華南.md "wikilink")[丘陵的丘陵山地地區](../Page/丘陵.md "wikilink")，遷徙行程中只能以[鹽保存食物](../Page/食盐.md "wikilink")，勞動出汗多亦需補充[鹽分以維持體力](../Page/食盐.md "wikilink")，因此飲食傾向多油多鹹的重口味菜，並且好用各式處理過的醃製菜類（如[酸菜](../Page/酸菜.md "wikilink")、[福菜](../Page/福菜.md "wikilink")、[梅乾菜](../Page/梅乾菜.md "wikilink")、大叶品种的[芥菜](../Page/芥菜.md "wikilink")）作為食材入菜。不過目前已有偏向少油少鹹的改良式客家菜。

客家人勤儉刻苦，平時省吃儉用，只在年節與朔望祭拜祖先神明，或是婚喪喜慶宴客才會宰殺牲畜。為了不浪費食材，極講究妥善運用牲畜之各個部位作成佳餚。演變至今，已形成「四炆四炒」的八道宴客標準菜色。

所謂的「[炆](../Page/炆.md "wikilink")」，是指大鍋烹煮、持久保溫。典型的「四炆」係指[酸菜](../Page/酸菜.md "wikilink")（或[鹹菜](../Page/鹹菜.md "wikilink")）炆[豬肚](../Page/豬肚.md "wikilink")、炆[爌肉](../Page/爌肉.md "wikilink")、[排骨炆](../Page/排骨.md "wikilink")[菜頭](../Page/菜頭.md "wikilink")、肥湯炆[筍乾四道菜](../Page/筍乾.md "wikilink")；而典型的「四[炒](../Page/炒.md "wikilink")」係指客家炒肉（又稱客家小炒）、[豬腸炒薑絲](../Page/豬腸炒薑絲.md "wikilink")、鴨血（亦有用豬肚者）炒[韭菜](../Page/韭菜.md "wikilink")、[豬肺](../Page/豬肺.md "wikilink")[鳳梨炒](../Page/鳳梨.md "wikilink")[木耳](../Page/木耳.md "wikilink")（俗稱鹹酸甜）四道菜\[1\]。

## 著名客家菜

[File:Yong_tau_foo_sweet.jpg|客家釀豆腐](File:Yong_tau_foo_sweet.jpg%7C客家釀豆腐)
[File:Cuisine_of_China_0074.JPG|梅菜扣肉](File:Cuisine_of_China_0074.JPG%7C梅菜扣肉)
[File:HofanUpClose.jpg|客家粄條](File:HofanUpClose.jpg%7C客家粄條)
[File:HK_Vegetarian_Poonchoi.JPG|盆菜](File:HK_Vegetarian_Poonchoi.JPG%7C盆菜)
[File:%E7%B1%B3%E7%AF%A9%E7%9B%AE.JPG|米篩目](File:%E7%B1%B3%E7%AF%A9%E7%9B%AE.JPG%7C米篩目)
[File:Lei_cha.jpg|擂茶](File:Lei_cha.jpg%7C擂茶)
[File:Fishball_beefball.jpg|牛丸篩目](File:Fishball_beefball.jpg%7C牛丸篩目)

  - [豬肚酸菜湯](../Page/豬肚酸菜湯.md "wikilink")
  - [客家炒肉](../Page/客家炒肉.md "wikilink")
  - [梅菜扣肉](../Page/梅菜扣肉.md "wikilink")
  - [客家釀豆腐](../Page/客家釀豆腐.md "wikilink")
  - [客家鹹鸡](../Page/客家鹹鸡.md "wikilink")
  - [清炖鸡](../Page/清炖鸡.md "wikilink")
  - [煎酿蛋角](../Page/煎酿蛋角.md "wikilink")
  - [釀苦瓜](../Page/釀苦瓜.md "wikilink")
  - [鸡炒酒](../Page/鸡炒酒.md "wikilink")
  - [黄酒鸡](../Page/黄酒鸡.md "wikilink")
  - [東江鹽焗雞](../Page/東江鹽焗雞.md "wikilink")
  - [客家小炒](../Page/客家小炒.md "wikilink")
  - [客家粽](../Page/客家粽.md "wikilink")
  - [浮水大鯇魚](../Page/浮水大鯇魚.md "wikilink")
  - [紅炆肉](../Page/紅炆肉.md "wikilink")
  - [水晶豆沙扣肉](../Page/水晶豆沙扣肉.md "wikilink")
  - [炒子鴨](../Page/炒子鴨.md "wikilink")
  - [酵飯](../Page/酵飯.md "wikilink")
  - [筍飯](../Page/筍飯.md "wikilink")
  - [珍珠粉](../Page/珍珠粉.md "wikilink")
  - [憶子飯](../Page/憶子飯.md "wikilink")
  - [鴨鬆羮](../Page/鴨鬆羮.md "wikilink")
  - [黃飯](../Page/黃飯.md "wikilink")
  - [鍋叾飯](../Page/鍋叾飯.md "wikilink")

## 其他客家菜

  - [牛肉丸](../Page/牛肉丸.md "wikilink")
  - [猪肉丸](../Page/猪肉丸.md "wikilink")
  - [牛筋丸](../Page/牛筋丸.md "wikilink")
  - [鱼丸](../Page/鱼丸.md "wikilink")
  - [酿苦瓜](../Page/酿苦瓜.md "wikilink")

<!-- end list -->

  - [酿青椒](../Page/酿青椒.md "wikilink")
  - [客家生魚膾](../Page/生魚片.md "wikilink")
  - [白切雞](../Page/白切雞.md "wikilink")
  - [客家鹹雞](../Page/客家鹹雞.md "wikilink")
  - [子姜炒鸭](../Page/子姜炒鸭.md "wikilink")

<!-- end list -->

  - [薑絲炒大腸](../Page/薑絲炒大腸.md "wikilink")
  - [粄條](../Page/粄條.md "wikilink")
  - [苦瓜炒鹹蛋](../Page/苦瓜炒鹹蛋.md "wikilink")
  - [封肉](../Page/封肉.md "wikilink")
  - [菜脯](../Page/菜脯.md "wikilink")

<!-- end list -->

  - [鹹豬肉](../Page/鹹豬肉.md "wikilink")
  - [盆菜](../Page/盆菜.md "wikilink")
  - [算盤子](../Page/算盤子.md "wikilink")
  - [客家炸肉](../Page/客家炸肉.md "wikilink")
  - [豬腳醋](../Page/豬腳醋.md "wikilink")

<!-- end list -->

  - [梅干扣肉](../Page/梅干扣肉.md "wikilink")
  - [梅菜扣肉](../Page/梅菜扣肉.md "wikilink")
  - [香芋扣肉](../Page/香芋扣肉.md "wikilink")
  - [焖乳狗](../Page/焖乳狗.md "wikilink")
  - [乳狗褒](../Page/乳狗褒.md "wikilink")

<!-- end list -->

  - [炸大腸](../Page/炸大腸.md "wikilink")
  - [東江豆腐煲](../Page/東江豆腐煲.md "wikilink")
  - [炖肉饼](../Page/炖肉饼.md "wikilink")
  - [酿腐皮卷](../Page/酿腐皮卷.md "wikilink")

## 小吃

  - [擂茶](../Page/擂茶.md "wikilink")（三生湯）
  - [金桔醬](../Page/金桔醬.md "wikilink")
  - [客家黄酒](../Page/客家黄酒.md "wikilink")
  - [艾粄](../Page/艾粄.md "wikilink")

<!-- end list -->

  - [茶粄](../Page/茶粄.md "wikilink")
  - [甜粄](../Page/甜粄.md "wikilink")
  - [淹麵](../Page/淹麵.md "wikilink")
  - [簸箕粄](../Page/簸箕粄.md "wikilink")

<!-- end list -->

  - [芋卵粄](../Page/芋卵粄.md "wikilink")
  - [发粄](../Page/发粄.md "wikilink")
  - [老鼠粉](../Page/老鼠粉.md "wikilink")
  - [萝卜粄](../Page/萝卜粄.md "wikilink")

<!-- end list -->

  - [仙人粄](../Page/仙人粄.md "wikilink")
  - [三及第汤](../Page/三及第汤.md "wikilink")
  - [萝卜丸](../Page/萝卜丸.md "wikilink")
  - [盐焗凤爪](../Page/盐焗凤爪.md "wikilink")

<!-- end list -->

  - [寧興藥糕](../Page/寧興藥糕.md "wikilink")
  - [灸糭](../Page/灸糭.md "wikilink")
  - [蓼花](../Page/蓼花.md "wikilink")
  - [煎芋丸](../Page/煎芋丸.md "wikilink")

<!-- end list -->

  - [白渡牛肉乾](../Page/白渡牛肉乾.md "wikilink")
  - [雲片糕](../Page/雲片糕.md "wikilink")
  - [牛汶水](../Page/牛汶水.md "wikilink")

## 参考文献

## 参见

  - [粤菜](../Page/粤菜.md "wikilink")

[客家菜](../Category/客家菜.md "wikilink")
[Category:菜系](../Category/菜系.md "wikilink")

1.