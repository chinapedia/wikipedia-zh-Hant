**客雅溪**是[臺灣北部的河流](../Page/臺灣北部.md "wikilink")，屬於中央管區域排水，原名**隙仔溪**\[1\]，主流長約24公里，流域涵蓋[新竹縣](../Page/新竹縣.md "wikilink")[寶山鄉與](../Page/寶山鄉_\(台灣\).md "wikilink")[新竹市](../Page/新竹市.md "wikilink")。客雅溪發源於[竹東丘陵](../Page/竹東丘陵.md "wikilink")，流經[山湖](../Page/山湖村.md "wikilink")、[寶山](../Page/寶山村.md "wikilink")、[大崎](../Page/大崎村.md "wikilink")、[雙溪四個村](../Page/雙溪村.md "wikilink")，經過[青草湖](../Page/青草湖_\(新竹\).md "wikilink")，進入[新竹平原](../Page/新竹平原.md "wikilink")，最後注入[臺灣海峽](../Page/臺灣海峽.md "wikilink")。流域面積約4560公頃\[2\]。

## 客雅溪水系主要河川

  - **客雅溪**：新竹市、新竹縣寶山鄉
      - [油車溝溪](../Page/油車溝溪.md "wikilink")：新竹市

## 参考文献

[category:新竹市河川](../Page/category:新竹市河川.md "wikilink")
[category:新竹縣河川](../Page/category:新竹縣河川.md "wikilink")

[客雅溪水系](../Category/客雅溪水系.md "wikilink")

1.  中華民國內政部 臺灣地區地名查詢系統 客雅溪，
2.  經濟部水利署第二河川局，http://www.wra02.gov.tw/riverplan6.asp