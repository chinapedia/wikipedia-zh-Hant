**宣武靈皇后**（），胡氏，名字已失考，小说《[北史演義](../Page/北史演義.md "wikilink")》稱**胡仙真**，[安定郡](../Page/安定郡.md "wikilink")[临泾县](../Page/临泾县.md "wikilink")（今[宁夏回族自治区](../Page/宁夏回族自治区.md "wikilink")[固原市](../Page/固原市.md "wikilink")[彭阳县](../Page/彭阳县.md "wikilink")）人，司徒[胡國珍的长女](../Page/胡國珍.md "wikilink")，母亲秦太上君[皇甫氏](../Page/皇甫氏.md "wikilink")。為[魏宣武帝元恪妃](../Page/魏宣武帝.md "wikilink")、[魏孝明帝元詡生母](../Page/魏孝明帝.md "wikilink")。史書上多稱之**靈太后**。

## 生平

[胡國珍的妹妹](../Page/胡国珍.md "wikilink")[胡僧芝是名尼姑](../Page/胡僧芝.md "wikilink")，經常出入宮內，後來她見自己的侄女姿色豔美，便引薦胡氏入宮。胡氏入宮後被封为[世妇](../Page/世妇.md "wikilink")。

當時的北魏[後宮實行](../Page/後宮.md "wikilink")[子貴母死制度](../Page/子貴母死.md "wikilink")，因而宮中諸[嬪妃及宮人皆祈願](../Page/嬪妃.md "wikilink")，不願生下皇子，胡氏看見這種情況後，不今感嘆：「一國之君怎可無子？我怎能為一己之私而不生育皇子，這樣皇室血統不就要斷絕了嗎？」因此她被寵幸之後，並不像其餘妃嬪祈禱生下[公主](../Page/公主.md "wikilink")，相反的，胡氏相當期望自己生下的是[皇子](../Page/皇子.md "wikilink")。[永平三年](../Page/永平_\(北魏\).md "wikilink")（510年），胡氏生下兒子[元詡](../Page/魏孝明帝.md "wikilink")。晉為[充華](../Page/充華.md "wikilink")（北魏后宮嬪級的最低一等）。宣武帝废除了子贵母死制度，胡氏因而没有被[赐死](../Page/赐死.md "wikilink")。

六歲的[元詡登基為](../Page/魏孝明帝.md "wikilink")[北魏孝明帝後](../Page/元詡.md "wikilink")，尊嫡母宣武帝[皇后](../Page/皇后.md "wikilink")[高英為皇太后](../Page/高英.md "wikilink")、生母胡氏為[皇太妃](../Page/皇太妃.md "wikilink")，之後高太后失势出家为尼，孝明帝晉封胡太妃為[皇太后](../Page/皇太后.md "wikilink")，胡太后又因皇帝年幼而[臨朝聽政](../Page/臨朝稱制.md "wikilink")。她的妹夫江阳王世子[元乂也因而进入权力中心](../Page/元乂.md "wikilink")。

胡太后掌政初期北魏國勢發展蓬勃，但佞佛教，浪费國家財產。宣武帝死後，胡太后耐不住寂寞，在宮中包養了[情夫如清河王](../Page/情夫.md "wikilink")[元怿等](../Page/元怿.md "wikilink")，后来又不断培养和发掘新的情夫，而这几位情夫，酿下了数次宫廷政变的祸根。她最早和[楊白花通姦](../Page/楊白花.md "wikilink")，後來和元懌通姦。宦官[刘腾和元乂发动政变杀死元怿](../Page/劉騰.md "wikilink")，废黜囚禁胡太后于宣光殿，宫门昼夜长闭，内外隔绝，刘腾亲自掌管宫门钥匙，孝明帝也见不到她，她只能等候送饭，衣食短缺，不免饥寒。后刘腾死，元乂放松警惕，她才被释放，向孝明帝哭诉母子不得相见之情；又和[郑俨](../Page/郑俨.md "wikilink")、[李神轨等人淫亂](../Page/李神轨.md "wikilink")，后来又和孝明帝等合谋处死元乂及其弟[元爪](../Page/元爪.md "wikilink")。但因元乂系自己妹夫，她还是追赠元乂为江阳王、侍中、骠骑大将军、仪同三司、尚书令、冀州刺史。

[孝明帝長大後](../Page/魏孝明帝.md "wikilink")，漸不滿母親胡太后把持朝政，[孝昌四年](../Page/孝昌_\(年号\).md "wikilink")（528年），暗召[爾朱榮領兵入洛陽協助他奪權](../Page/爾朱榮.md "wikilink")，但爾朱榮還未到洛陽，[孝明帝突然駕崩](../Page/魏孝明帝.md "wikilink")（傳因胡太后得知孝明帝間召爾朱榮，將他鳩殺），無子嗣繼位，再加上政局動盪，胡太后因而將孝明帝[獨女元氏偽稱皇子](../Page/元氏_\(魏孝明帝女\).md "wikilink")，使其登基為帝。待人心已安，又將[元氏廢位](../Page/元氏_\(魏孝明帝女\).md "wikilink")，改立孝明帝堂姪[元釗為帝](../Page/元釗.md "wikilink")，世稱北魏幼主。

瞬時間的廢立使天下震驚，將領[爾朱榮疑有詐](../Page/爾朱榮.md "wikilink")，遂帶兵討伐，15日後佔領京師[洛陽](../Page/洛陽.md "wikilink")，[元釗及胡太后被俘](../Page/元釗.md "wikilink")。爾朱榮將幼主元釗和胡太后押送至[黃河](../Page/黃河.md "wikilink")。胡太后在爾朱榮面前極盡能言之事，但爾朱榮充耳不聞，下令將幼主元釗和胡太后沉入黃河溺斃，另立[元子攸即位](../Page/魏孝庄帝.md "wikilink")，是為孝莊帝。

胡太后死後，她的妹妹[胡玄辉将尸体收葬于双灵寺](../Page/胡玄辉.md "wikilink")。三年后[高欢攻灭尔朱氏](../Page/高欢.md "wikilink")，以皇后之礼改葬之，追諡**靈太后**，或稱**宣武靈皇后**。

## 参考资料

[H胡](../Category/中国女性摄政者.md "wikilink")
[H胡](../Category/北魏妃嬪.md "wikilink")
[H胡](../Category/北魏皇太后.md "wikilink")
[Category:河阴之变遇害者](../Category/河阴之变遇害者.md "wikilink")
[Category:溺死者](../Category/溺死者.md "wikilink")
[Category:安定胡氏](../Category/安定胡氏.md "wikilink")
[Category:諡靈](../Category/諡靈.md "wikilink")