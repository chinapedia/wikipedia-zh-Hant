**宣靖陵**()位於[大韓民國](../Page/大韓民國.md "wikilink")[首爾](../Page/首爾.md "wikilink")[江南區的三成](../Page/江南區_\(首爾\).md "wikilink")2洞，[COEX的西側](../Page/COEX.md "wikilink")、鄰近[首爾地鐵的](../Page/首爾地鐵.md "wikilink")[宣陵站和未來的](../Page/宣陵站.md "wikilink")[三陵站](../Page/三陵站.md "wikilink")，首爾的新世界酒店和華美達酒店亦在附近，是首爾的著名觀光點。它是[朝鮮王朝两位君主和一位王后的陵墓所在](../Page/朝鮮王朝.md "wikilink")。

## 宣陵

宣陵是[朝鮮成宗的陵墓](../Page/朝鮮成宗.md "wikilink")。建于[光海君元年](../Page/光海君.md "wikilink")（1495年）。

## 貞顯王后墓

貞顯王后墓位於宣陵旁邊、是成宗的王后[贞显王后的陵墓](../Page/贞显王后.md "wikilink")。

## 靖陵

靖陵，[朝鮮中宗的陵墓](../Page/朝鮮中宗.md "wikilink")，位於公園的東側。建于[明宗十七年](../Page/朝鮮明宗.md "wikilink")（1562年）

## 三陵公園

三陵公園位於宣靖陵的中央，在宣陵、貞顯王后墓與靖陵之間。

## 参观信息

售票時間：09:00-17:30（冬季:09:00-16:30） 觀覽時間：09:00-18:00（冬季:09:00-17:00）

## 參見

  - [朝鮮成宗](../Page/朝鮮成宗.md "wikilink")
  - [朝鮮中宗](../Page/朝鮮中宗.md "wikilink")
  - [(影片)](https://www.youtube.com/watch?v=C8vd4quls-w&feature=player_embedded%7C首爾王陵)

[Category:朝鲜王朝王室墓葬](../Category/朝鲜王朝王室墓葬.md "wikilink")
[Category:首尔墓葬](../Category/首尔墓葬.md "wikilink")