**憲法**，又稱**宪章**、**根本法**\[1\]或**憲制文件**，是一个[主權国家](../Page/主權国家.md "wikilink")、[政治實體或](../Page/政治實體.md "wikilink")[地區](../Page/地區.md "wikilink")、[自治地区](../Page/自治.md "wikilink")、[聯邦制](../Page/聯邦制.md "wikilink")[國家的](../Page/國家.md "wikilink")[联邦州](../Page/联邦州.md "wikilink")或[國際組織及其成員](../Page/國際組織.md "wikilink")的最基本[法律](../Page/法律.md "wikilink")\[2\]。與憲法擁有同樣地位的還有[基本法](../Page/基本法.md "wikilink")。

憲法通常規定一個國家的社會制度、國家制度、國家機構、公民的基本權利和義務等\[3\]，但不一定包含以上全部內容，例如會逐漸增加憲法內容的英國不成文憲法。憲法定義國家[政體及政府運作方式](../Page/政體.md "wikilink")，以及[法律訂定的方式](../Page/法律.md "wikilink")。憲法在一個國家之全部法律中具有最高權威和最大效力，是制定其他法律之依據\[4\]。有些憲法（特別是成文憲法）會限制政府的權力，其方式是訂定一些政府權力的運作範圍，例如人民的[基本權](../Page/基本權.md "wikilink")，例如[美国宪法就是這類的憲法](../Page/美国宪法.md "wikilink")。憲法之制定和修改，一般須經過特定之程序\[5\]。考量美國憲法與美國黑奴曾經同時並存，認定憲法並非民主國家特有的法律種類，以憲法的原文"Constitution"作為思考的起點，憲法的定義其實是國家基本結構的意思，漢朝的[約法三章亦能歸類為憲法](../Page/约法三章.md "wikilink")。

[Washington_Constitutional_Convention_1787.jpg](https://zh.wikipedia.org/wiki/File:Washington_Constitutional_Convention_1787.jpg "fig:Washington_Constitutional_Convention_1787.jpg")\]\]
[印度憲法是世界所有](../Page/印度憲法.md "wikilink")[主权国家中](../Page/主权国家.md "wikilink")，篇幅最長的成文憲法\[6\]，共有444條，分為22章\[7\]\[8\]，12份附表及118個修正案，若翻譯為[印度英語有](../Page/印度英語.md "wikilink")117,369字\[9\]。[美國憲法是篇幅最短的成文憲法](../Page/美國憲法.md "wikilink")，共有7條，27個修正案，合計4,400字\[10\]。

## 名称

宪法在[拉丁文裡写做](../Page/拉丁文.md "wikilink")「」，為[構造或體制翻譯而來](../Page/構造.md "wikilink")。

中文的「憲法」一詞很早就出現於[春秋時期](../Page/春秋時期.md "wikilink")（公元前770年—公元前476年），[左丘明編撰的](../Page/左丘明.md "wikilink")《[國語](../Page/國語_\(書\).md "wikilink")·晉語九》：「賞善罰奸，國之憲法也」。然而，现代的「憲法」的概念是从西方传入。

屬於憲法或憲制性文件的[法律](../Page/法律.md "wikilink")，不一定在正式名稱中有「憲法」的字樣。除了「憲法」的稱呼外，還有「[基本法](../Page/基本法.md "wikilink")」等其他稱呼，例如《[香港特別行政區基本法](../Page/香港特別行政區基本法.md "wikilink")》，在[台灣日治時期有效的憲制性文件](../Page/台灣日治時期.md "wikilink")《[六三法](../Page/六三法.md "wikilink")》，正式名稱是「應於臺灣法令施行相關之法律」。

《[德國聯邦基本法](../Page/德國基本法.md "wikilink")》在德國未制定憲法之前，具有憲法地位。\[11\]

[孫中山認為](../Page/孫中山.md "wikilink")，所謂憲法，就是將政權分以部分，各司其事。\[12\]

## 起源

[Magna_Carta_(British_Library_Cotton_MS_Augustus_II.106).jpg](https://zh.wikipedia.org/wiki/File:Magna_Carta_\(British_Library_Cotton_MS_Augustus_II.106\).jpg "fig:Magna_Carta_(British_Library_Cotton_MS_Augustus_II.106).jpg")主要是規範統治者權力如何行使的固有意義憲法\]\]
從傳統的[國家學來看](../Page/國家學.md "wikilink")，憲法的發展可以分為三個階段：

### [警察國家時期](../Page/警察國家.md "wikilink")

國家與社會並未真正區分，統治者作為國家的代表，與代表人民生活環境的社會之間的關係如同[家父長](../Page/家父長.md "wikilink")，統治者擁有至高權力，國家可以為人民等，規範以上事項之憲法即為「固有意義的憲法」。

### 自由法治國家時期

此一時期由於思想的啟蒙、中產階級興起，國家與社會逐漸區分開來，此一時期的思想認為社會先於國家存在，且基於[私法自治](../Page/私法自治.md "wikilink")（即[契約自由原則](../Page/契約自由.md "wikilink")）而自發性形成，國家是為了使社會運作完善而產生的，因此政府對於社會的干預越小越好，透過[天賦人權](../Page/天賦人權.md "wikilink")、[議會制度](../Page/議會制度.md "wikilink")、[司法制度的確立](../Page/司法制度.md "wikilink")，國家間接使社會運作順利，人民權利透過間接的方式受到憲法的保障，所以又稱為「形式法治國」。資本主義國家之憲法，出現於資產階級革命勝利後\[13\]。在此一時期，行政法開始出現，[依法行政](../Page/依法行政.md "wikilink")、[法律保留](../Page/法律保留.md "wikilink")、[特別權力關係等概念逐漸出現](../Page/特別權力關係.md "wikilink")。

### 社會法治國家時期

鑒於前一時期國家任務範圍狹隘，在私法自治及契約自由等前提之下，經濟力強大的社團或財團造成市場壟斷，對於人民權利侵害過鉅，因此認為國家與社會的關係並非完全重疊，也非完全分立的二元，而是應有適度的混合，國家形成社會秩序同時，也要對人民權利予以最低的保障，強調人民基本權利可直接以憲法為保障根據，並且憲法應加入基本國策，以補充性原則保障人民福利。此一時期，不只國家，社會的一般人民也要遵守憲法對於基本權的保障規定，但隨著時間轉移，因而使國家修改宪法的可能性增加。

## 特性

从理论上讲，宪法的效力高于本国其他法律和法规。憲法反映階級力量對比關係\[14\]。但在现实裡，宪法并不是在所有国家中都具有权威性。在不同時代和類型之國家，憲法之形式和內容有所不同，但都是統治階級意志之表現，是實現其階級專政之重要工具\[15\]。为保证宪法的权威性，需要一套相应的体系来确保宪法没有被违背。这套体系称之为[宪法審查制度](../Page/違憲審查.md "wikilink")。在现代民主国家，由于宪法審查制度的实施，一条法规如果和宪法相牴觸，便會失效。而在非民主国家，宪法的最高效力经常不能得到有效的维护，以至于宪法成为一纸空文。

使一条和宪法牴觸的法规失效的方法有很多种，端視不同宪法審查制度而異，可以事前审查，也可以事后审查。即使获得通过，嗣後被撤销，或在审理的时候不被法院采纳，也可能造成法規無效。这条体系最早由奥地利的法律学家[凯尔孙最先提出](../Page/凯尔孙.md "wikilink")。依据这个的理论，法律和法规以及宪法构成一个金字塔。宪法位于塔顶，拥有最高权威；而法律由立法机关通过，其效力仅次于宪法；而法规是由行政机关颁布，它的效力最低，因此位于金字塔底。因此一条法规不能违背高于它的法律和宪法，否则它可能會失效（除非它背離的法律違背了憲法）。同样的，一条法律不能和宪法相牴觸，否则它也會失其效力。

現代概念中的憲法是[公民与](../Page/公民.md "wikilink")[国家的契约](../Page/国家.md "wikilink")，它在国家的法律体系中拥有最高的地位，因此它是国家的根本法，拥有最高的法律效力。宪法规定的事项主要有国家政治架构，政府组成与职能，权力制衡模式和公民的权利等。有些国家的宪法还规定了公民的义务，但大多[宪法学学者认为](../Page/宪法学.md "wikilink")，宪法规定公民的义务，不仅没有必要，而且难以实行。\[16\]宪法最为重要的意义在于它是一部权利宣言书。《[美国宪法](../Page/美国宪法.md "wikilink")》是这一表述的最佳注解。但對權利的列舉式規範並非憲法的絕對要件，美國憲法在訂立之初並無權利條款，[法国第五共和的憲法也未明列權利條款](../Page/法国.md "wikilink")，但這都無損於它們是有效憲法規範的事實。

## 分類

憲法可以沒有明文規定，隨著歷史的發展，习惯形成的。例如[英國憲法就是典型的](../Page/英國憲法.md "wikilink")[不成文憲法](../Page/不成文憲法.md "wikilink")\[17\]，該憲法不是一部單一的法律，而是由包括《[大憲章](../Page/大憲章.md "wikilink")》、《[英國權利法案](../Page/英國權利法案.md "wikilink")》、大量國會法案和相關法律，再加上很多習慣、判例累積組成。英國資產階級在與封建貴族之鬥爭與妥協中，先後通過或確認一些法律、慣例，逐漸形成憲法體系，即不成文憲法\[18\]。到18世紀[美國和](../Page/美國.md "wikilink")[法國資產階級革命取得勝利後](../Page/法國.md "wikilink")，制定[成文憲法](../Page/成文憲法.md "wikilink")\[19\]。

亦有學者依據憲法內容而分為「憲章」與「憲律」\[20\]；憲章為制憲者制訂憲法時的核心理念，例如美國憲法裡的聯邦國、中華民國憲法裡的五權分立、各國憲法裡出現的各種人權保障等如是；憲律則為制憲者依據憲章、當時制憲時空以及背景所做的思構，例如中華民國憲法裡有關邊疆地區人民的生存、發展、參政等規定如是。

宪法是一个与[主权紧密相连的概念](../Page/主权.md "wikilink")。主权是指不受任何限制，也不从属于其他权力的权力。因此，只有国家才拥有主权。在目前阶段，欧盟依然是一个独立国家联合体。它的最高规范性文件不是“宪法”。其成员国把部分国家主权交给欧盟（如軍事指揮權），但各成员国地位平等并拥有退出欧盟的权力。

[香港是一个属于](../Page/香港.md "wikilink")[中华人民共和国的](../Page/中华人民共和国.md "wikilink")“[特别行政区](../Page/特别行政区.md "wikilink")”，其權力來源為中英共同簽署的《[中英聯合聲明](../Page/中英聯合聲明.md "wikilink")》。而香港有人称《[基本法](../Page/中華人民共和國香港特別行政區基本法.md "wikilink")》为“小宪法”，其中隐含意义即是基本法的條文及法律地位與中國憲法相對，一如[歐洲聯盟的](../Page/歐洲聯盟.md "wikilink")《[歐盟憲法](../Page/歐盟憲法.md "wikilink")》是建基于国家的授权。

## 成文憲法與不成文憲法的比較

### 成文憲法的優點

  - 具有清楚的明文上準則
  - 習慣法和判例僅供參考用
  - 大致上屬於**「剛性憲法」** （意指修憲門檻較高，且程序繁瑣，修改較為困難之憲法）

其他：成文法的祖源是[古羅馬時代的](../Page/古羅馬.md "wikilink")「[十二銅表法](../Page/十二銅表法.md "wikilink")」\[21\]，故此有成文憲法的國家基本上都是屬於[歐陸法系](../Page/歐陸法系.md "wikilink")（或稱為[大陸法系](../Page/大陸法系.md "wikilink")）。

### 成文憲法的缺點

  - 因法條規定明確，致使法條易凝滯不變，需靠修法方能更動。
  - 因習慣法和判例僅供參考用，所以法條近乎不近人情。

### 不成文憲法的優點

  - 因法例眾多，故特重判例
  - 重視[習慣法和判例](../Page/習慣法.md "wikilink")，而習慣法和判例隨時可新增，故每年至少有數十個習慣法和判例新增至法典內
  - 大致上屬於**「柔性憲法」** （意指相對於剛性憲法，修憲門檻較低、程序簡便，修改較為容易之憲法）

其他：不成文法的祖源是[征服者](../Page/征服者威廉.md "wikilink")[諾曼從古](../Page/诺曼王朝.md "wikilink")[法國諾曼地區帶去](../Page/法國.md "wikilink")，並在[英國發揚光大](../Page/英國.md "wikilink")，所於此法系被通稱為**「[英美法系](../Page/英美法系.md "wikilink")」**。([海洋法系](../Page/海洋法系.md "wikilink"))

### 不成文憲法的缺點

  - 法條規定不明確，重視歷史判例和名法官論文或是報告，所以繁雜、不易簡單明確

## 注释

## 参考文献

## 参见

  - [宪法学](../Page/宪法学.md "wikilink")

  - [宪政](../Page/宪政.md "wikilink")

  - [行憲](../Page/行憲.md "wikilink")

  - [憲法法院](../Page/憲法法院.md "wikilink")

  - [違憲審查及](../Page/違憲審查.md "wikilink")[司法覆核](../Page/司法覆核.md "wikilink")

  - [基本法](../Page/基本法.md "wikilink")

  - [宗座憲令](../Page/宗座憲令.md "wikilink")

  -
  - [宪政主义](../Page/宪政主义.md "wikilink")

  -
  - 和

  - [法律哲学](../Page/法律哲学.md "wikilink")

  - [法治](../Page/法治.md "wikilink")

<!-- end list -->

  - 一些國家的憲法

<!-- end list -->

  -   - 《[钦定宪法大纲](../Page/钦定宪法大纲.md "wikilink")》（废除）
      - 《[宪法重大信条十九条](../Page/宪法重大信条十九条.md "wikilink")》（废除）

  -   - 《[中华民国临时政府组织大纲](../Page/中华民国临时政府组织大纲.md "wikilink")》（废除）
      - 《[中華民國臨時約法](../Page/中華民國臨時約法.md "wikilink")》（废除）
      - 《[中华民国约法](../Page/中华民国约法.md "wikilink")》（废除）
      - 《[中华民国宪法](../Page/1923年中华民国宪法.md "wikilink")》（1923年版）（废除）
      - 《[中华民国训政时期约法](../Page/中华民国训政时期约法.md "wikilink")》（废除）
      - 《[中華民國憲法](../Page/中華民國憲法.md "wikilink")》

  -   - 《[中國人民政治協商會議共同綱領](../Page/中國人民政治協商會議共同綱領.md "wikilink")》
      - 《[中华人民共和国宪法](../Page/中华人民共和国宪法.md "wikilink")》

  - [宪法](../Page/日本国宪法.md "wikilink")

  - [宪法](../Page/馬來西亞憲法.md "wikilink")

  - [宪法](../Page/美國憲法.md "wikilink")

  - [宪法](../Page/法国宪法.md "wikilink")

  - [宪法](../Page/英国宪法.md "wikilink")

  - [基本法](../Page/德意志联邦共和国基本法.md "wikilink")

  - [宪法](../Page/印度宪法.md "wikilink")

  - [宪法](../Page/爱尔兰宪法.md "wikilink")

  -
  - [欧盟宪法](../Page/欧盟宪法.md "wikilink")

  - [里斯本条约](../Page/里斯本条约.md "wikilink")

  - [联合国宪章](../Page/联合国宪章.md "wikilink")

## 外部連結

  - 陳弘毅：[〈基督教傳統與西方現代憲政的起源〉](http://www.aisixiang.com/data/67222.html)（2013）
  - 陳弘毅：[〈亞洲國家憲政發展的道路初探〉](http://www.aisixiang.com/data/65204.html)（2013）

<!-- end list -->

  - [Constitute](http://constituteproject.org), an indexed and
    searchable database of all constitutions in force
  - [Dictionary of the History of
    Ideas](https://web.archive.org/web/20060623044421/http://etext.lib.virginia.edu/cgi-local/DHI/dhi.cgi?id=dv1-61)
    Constitutionalism
  - [*Constitutional
    Law*](https://web.archive.org/web/20060502181213/http://www.staff.amu.edu.pl/%7Ewroblew/html/en_pr_konst.html),
    "Constitutions, bibliography, links"
  - [*International Constitutional
    Law*:](http://www.servat.unibe.ch/icl/) English translations of
    various national constitutions
  - [*constitutions of countries of the European
    Union*](https://web.archive.org/web/20180503094037/http://www.constitution.eu/)
  - [United Nations Rule of Law:
    Constitution-making](https://web.archive.org/web/20091204190502/http://unrol.org/article.aspx?article_id=31),
    on the relationship between constitution-making, the rule of law and
    the United Nations.
  - [*Democracy in Ancient
    India*](http://www.infinityfoundation.com/ECITdemocracyindiaframeset.htm)
    by Steve Muhlberger of Nipissing University
  - [Report on the British constitution and proposed European
    constitution by Professor John McEldowney, University of
    Warwick](https://web.archive.org/web/20051221073913/http://www.parliament.the-stationery-office.co.uk/pa/ld200203/ldselect/ldconst/168/16809.htm#note92)
    Submitted as written evidence to House of Lords Select Committee on
    Constitution, published to the public on 15 October 2003.

{{-}}

[宪法](../Category/宪法.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.

2.  *The [New Oxford American
    Dictionary](../Page/New_Oxford_American_Dictionary.md "wikilink")*,
    Second Edn., [Erin McKean](../Page/Erin_McKean.md "wikilink")
    (editor), 2051 pages, May 2005, Oxford University Press, ISBN
    978-0-19-517077-1.

3.
4.
5.
6.

7.

8.

9.

10.

11. 联邦德国基本法（GG）第146条 基本法的适用期
    本基本法在德国自由统一之后适用所有德国人民，德国人民以自由意志制定通过的**宪法**实施之日，本基本法失效。

12. 孫中山：《在廣東省教育會的演說》，1921年4月4日，刊《廣東群報》，1921年4月4、5、8、13、14日

13.
14.
15.
16.

17.

18.
19.
20.

21.