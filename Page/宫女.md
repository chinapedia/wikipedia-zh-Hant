[Court_Ladies_of_the_Tang.jpg](https://zh.wikipedia.org/wiki/File:Court_Ladies_of_the_Tang.jpg "fig:Court_Ladies_of_the_Tang.jpg")[永泰公主墓室](../Page/永泰公主.md "wikilink")[壁畫的](../Page/壁畫.md "wikilink")[宮女](../Page/宮女.md "wikilink")\]\]
**宫女**，也称**宫人**，狭义上指[君主及其家庭的](../Page/君主.md "wikilink")[女僕或](../Page/女僕.md "wikilink")[侍女](../Page/侍女.md "wikilink")，广义上指君主[后宫包括](../Page/后宫.md "wikilink")[妃嫔的所有女性](../Page/妃嫔.md "wikilink")。較為高級的宮女則稱為[女官](../Page/女官.md "wikilink")。

在不同的国家、朝代，宫女主要的职能是为君主及其家庭的做杂役，以及为皇家提供[音乐](../Page/音乐.md "wikilink")、[舞蹈等娱乐](../Page/舞蹈.md "wikilink")。在君主專制政體中，宫女被视为[君主的准妾室](../Page/君主妾室.md "wikilink")。宫女被君主临幸后，一般会进行[册封](../Page/册封.md "wikilink")，获得妃嫔的[位号](../Page/位号.md "wikilink")。[中国历史上](../Page/中国历史.md "wikilink")，亦有宫女被皇帝临幸后，雖被視為皇帝的妾室，但未受册封為妃嬪的例子，如明朝的[戴银娘](../Page/戴银娘.md "wikilink")、[胡氏等人](../Page/胡侍御_\(明神宗\).md "wikilink")。有些更沒有妾室地位，只維持宮中僕役身份。

宫女的来源可能是强制，例如奴隶、战俘、罪犯或其眷属，或[君主从民间选女](../Page/宮女采選.md "wikilink")。也有一些情况下是自愿的，如明代宣宗起秀女多選幼女，養之教之，以禮教陰禮御婢。

## 中国

年華老去的宮女，可以[退宮](../Page/退宮.md "wikilink")。帝王登基時，往往都會把宮人放出民間。在唐代，有些宮女退宮後會進入道觀修道。\[1\]
明代洪武朝即欲規範退宮制度，洪武廿二年曾下誥敕令其外有家者，或五載或六載，歸其父母，聽從婚嫁。除新帝登基，常以災異大放宮女，若太長未放宮女，儒臣必以陰陽之理規勸皇帝，以陰氣怨氣不滯留宮中，加上紫禁城空間最多就容納不到3000宮女，放年長宮女進年輕的宮女，少有皇帝不允的。

## 朝鲜

[李氏朝鮮每年从](../Page/李氏朝鮮.md "wikilink")[兩班](../Page/兩班.md "wikilink")、[中人](../Page/中人.md "wikilink")、[常民](../Page/常民.md "wikilink")、[白丁阶层的女孩中挑选宫女](../Page/白丁.md "wikilink")。入宫后为見習的小宫女，小宫女通過考試後成為正式的宫女，稱為[內人](../Page/內人.md "wikilink")，资深宫女通過考核可升为尚宫。最高为[提调尚宫](../Page/提调尚宫.md "wikilink")，掌管一座[宫殿的事务](../Page/宫殿.md "wikilink")。宫女在年老体衰的时候，或者主人去世守丧3年后可以退宫。退宫的宫女不得婚嫁。

在朝鮮王朝實錄中，可能為了稱呼方便，廣義的將嬪妃與宮女都稱為宮人。

## 土耳其

[奥斯曼帝国的宫女来源为](../Page/奥斯曼帝国.md "wikilink")[女奴或自愿](../Page/女奴.md "wikilink")。宫女一般是在后宫做杂役，通常由[太后管辖](../Page/太后.md "wikilink")。非常美丽或有才艺的宫女，可能会被训练之后引见给[蘇丹](../Page/蘇丹_\(稱謂\).md "wikilink")，得到苏丹宠幸后，有機會升为[嫔妃](../Page/嫔妃.md "wikilink")。

## 註釋

[G](../Category/傳統女性職業.md "wikilink") [G](../Category/後宮.md "wikilink")
[Category:宮廷人員](../Category/宮廷人員.md "wikilink")

1.  李豐楙：《憂與遊：六朝隋唐遊仙詩論集》（臺北：臺灣學生書局，1996），頁325。