**宮崎
勝**（）是[日本的](../Page/日本.md "wikilink")[漫畫原作者](../Page/漫畫.md "wikilink")，其他笔名：鐘田正太郎、金田正太郎、宮崎二郎、写楽麿、宮崎博文、宮崎克、観月昴。擅長推理、[時代劇和紀錄片等廣泛故事內容為執筆題材](../Page/時代劇.md "wikilink")。其中，「[怪醫黑傑克創作秘話〜從手塚治虫的工作室開始〜](../Page/:ja:ブラック・ジャック創作秘話〜手塚治虫の仕事場から〜.md "wikilink")」獲選「[這本漫畫真厲害！](../Page/這本漫畫真厲害！.md "wikilink")2012年」男生編第一名。

## 作品

### 連載中

#### 宮崎克（）名義

  - （《Big Comic Original
    增刊號》－**連載中**，[小學館](../Page/小學館.md "wikilink")）作畫：[青木朋](../Page/:ja:青木朋.md "wikilink")

#### 觀月昴（）名義

  - 不動（《[週刊漫画ゴラク](../Page/:ja:週刊漫画ゴラク.md "wikilink")》，既刊3巻，[日本文藝社](../Page/:ja:日本文芸社.md "wikilink")）《G
    COMICS版》，作畫：奥道則

  - 新宿（《》，既刊3卷，日本文藝社）作畫：奥道則

### 連載完結

#### 宮崎勝（）名義

  - （《[月刊Fresh
    Jump](../Page/:ja:フレッシュジャンプ.md "wikilink")》，全1卷，[集英社](../Page/集英社.md "wikilink")）作畫：

  - 100（《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》，全9巻／豪華版、[FC軟體](../Page/FC游戏列表.md "wikilink")，[講談社](../Page/講談社.md "wikilink")）作畫：[石垣ゆうき](../Page/:ja:石垣ゆうき.md "wikilink")

  - [あいつはアインシュタイン](../Page/:ja:あいつはアインシュタイン.md "wikilink")（《週刊少年Magazine》，全3卷，講談社）作畫：石垣

  - [METAL FINISH](../Page/:ja:METAL_FINISH.md "wikilink")
    （《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》，全2卷，集英社）作畫：[鶴岡伸壽](../Page/:ja:鶴岡伸寿.md "wikilink")

  - [力人伝説
    -鬼を継ぐもの-](../Page/:ja:力人伝説_-鬼を継ぐもの-.md "wikilink")（《週刊少年Jump》全3巻，集英社）作畫：[小畑健](../Page/小畑健.md "wikilink")

  - [ザ・ドラえもんズ スペシャル](../Page/:ja:ザ・ドラえもんズ_スペシャル.md "wikilink")
    （[《小學5年生》《小學6年生》](../Page/:ja:小学館の学年別学習雑誌.md "wikilink")，全12巻，[小學館](../Page/小學館.md "wikilink")）作畫：三谷幸廣

  - BLADERS （《小學5年生》，全3巻，小學館）作畫：印照

#### 鐘田正太郎（）名義

  - 名探偵Mr.（《週刊少年Magazine》，全5卷，講談社）作画：[宇野比呂士](../Page/宇野比呂士.md "wikilink")

#### 金田正太郎（）名義

  - （《[Big Comic
    Original](../Page/Big_Comic_Original.md "wikilink")》，全2卷，小學館）作畫：

  - 雷神孫市（原案協力）（《[Play
    Comic](../Page/:ja:プレイコミック.md "wikilink")》，全2卷，[秋田書店](../Page/秋田書店.md "wikilink")）作畫：[貞安圭](../Page/:ja:さだやす圭.md "wikilink")

#### 宮崎二郎（）名義

  - BOY（《[週刊少年Sunday
    增刊號](../Page/:ja:週刊少年サンデーS.md "wikilink")》，全3卷，小學館）作畫：風巻弦

#### 寫楽麿（）名義

  - [傀儡師左近](../Page/傀儡師左近.md "wikilink")，原名《》（《週刊少年Jump》全4巻、《[JUMP
    COMICS](../Page/:ja:ジャンプ・コミックス.md "wikilink")》文庫版全3巻，後發行混音版、CD
    Book及製成電視動畫）作畫：[小畑健](../Page/小畑健.md "wikilink")。台灣中文版由[東立出版社正式授權](../Page/東立出版社.md "wikilink")。

  - 犯罪心理搜查官 小説（二見書房）

  - UNIVERSAL NUTS 《PS・世嘉土星（電玩軟體）》（Lay-up）

#### 宮崎克（）名義

  - 松田優作物語（《[Young
    Champion](../Page/:ja:ヤングチャンピオン.md "wikilink")》，全6卷+00卷，秋田書店）作畫：[高岩吉浩](../Page/:ja:高岩ヨシヒロ.md "wikilink")

  - 探偵帳（《Nighty
    Judy》，全1卷，小學館）作畫：[佐柄きょうこ](../Page/:ja:佐柄きょうこ.md "wikilink")

  - 新撰組默示錄（《Young Champion》，全7卷，秋田書店）
    作畫：[乾良彦](../Page/:ja:乾良彦.md "wikilink")

  - 歐人K（《[Business
    Jump](../Page/Business_Jump.md "wikilink")》，全1巻，集英社）作畫：[あだちつよし](../Page/:ja:プロジェクト:漫画家/日本の漫画家_あ行.md "wikilink")

  - （《Play
    Comic》，2卷～**繼續中**，秋田書店）作畫：[ケン月影](../Page/:ja:ケン月影.md "wikilink")

  - （《Play
    Comic》，1卷～**繼續中**，秋田書店）作畫：[高岩吉浩](../Page/:ja:高岩ヨシヒロ.md "wikilink")

  - [怪醫黑傑克創作秘話〜從手塚治虫的工作室開始〜](../Page/:ja:ブラック・ジャック創作秘話〜手塚治虫の仕事場から〜.md "wikilink")（《[週刊少年Champion](../Page/週刊少年Champion.md "wikilink")》、《[別冊少年Chapion](../Page/:ja:別冊少年チャンピオン.md "wikilink")》全5卷，秋田書店。2013年9月24日播放電視劇特別篇）作畫：[吉本浩二](../Page/:ja:吉本浩二.md "wikilink")

  - （《[週刊漫画ゴラク](../Page/:ja:週刊漫画ゴラク.md "wikilink")》，全1卷，[日本文藝社](../Page/:ja:日本文芸社.md "wikilink")）作畫：[藤原芳秀](../Page/藤原芳秀.md "wikilink")

  - RYO（《[Young
    King](../Page/:ja:ヤングキング.md "wikilink")》，[少年畫報社](../Page/少年畫報社.md "wikilink")）作畫：[乾良彦](../Page/:ja:乾良彦.md "wikilink")

  - 動物探偵推理日誌（《[月刊少年Rival](../Page/:ja:月刊少年ライバル.md "wikilink")》，全4卷，[講談社](../Page/講談社.md "wikilink")）作畫：左藤圭右

  - 怪奇道（《[新耳袋](../Page/:ja:新耳袋.md "wikilink")》，Home社）作畫：

#### 宮崎博文（）名義

  - 暗闇をぶっとばせ\!（《週刊少年Jump》，全2卷，集英社）作畫：[今泉伸二](../Page/:ja:今泉伸二.md "wikilink")

#### 觀月昴（）名義

  - Dr.（《》，日本文藝社）作畫：奥道則

[Category:漫畫原作者](../Category/漫畫原作者.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")