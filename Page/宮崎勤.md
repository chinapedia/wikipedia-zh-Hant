，也被称为**御宅族杀手**和**幼女杀手**，是日本的一个[连环杀手](../Page/连环杀手.md "wikilink")，[宫崎勤事件的凶手](../Page/宫崎勤事件.md "wikilink")。他在祖父去世的三个月后、自己26岁生日的第二天开始了杀人计划。
1988年8月到1989年6月间先后[绑架](../Page/绑架.md "wikilink")、伤害及杀害四名年纪介乎4至7岁的[女童](../Page/女童.md "wikilink")，并通过家族势力掩盖罪行，然后拍摄裸照[猥亵](../Page/猥亵.md "wikilink")、[奸尸](../Page/奸尸.md "wikilink")、食尸、[饮血](../Page/饮血.md "wikilink")，罪行發現19年后，被[法務大臣](../Page/法務大臣.md "wikilink")[鳩山邦夫批准执行死刑](../Page/鳩山邦夫.md "wikilink")。后来他被证实有[恋尸癖等爱好](../Page/恋尸癖.md "wikilink")。\[1\]\[2\]

## 遇害人

  - 今野真理：4岁
  - 古泽正美：7岁
  - 难波绘梨香：4岁
  - 野本绫子：5岁

## 参考资料

[Category:被日本处决的死刑犯](../Category/被日本处决的死刑犯.md "wikilink")
[Category:日本殘疾人](../Category/日本殘疾人.md "wikilink")
[Category:御宅族](../Category/御宅族.md "wikilink")
[Category:东京都出身人物](../Category/东京都出身人物.md "wikilink")
[Category:1962年出生](../Category/1962年出生.md "wikilink")
[Category:2008年逝世](../Category/2008年逝世.md "wikilink")
[Category:东京工艺大学短期大学部校友](../Category/东京工艺大学短期大学部校友.md "wikilink")
[Category:連環殺手](../Category/連環殺手.md "wikilink")
[Category:食人](../Category/食人.md "wikilink")
[Category:日本性罪犯](../Category/日本性罪犯.md "wikilink")
[Category:被處決的日本人](../Category/被處決的日本人.md "wikilink")

1.
2.