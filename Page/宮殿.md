[Forbidden_city_07.jpg](https://zh.wikipedia.org/wiki/File:Forbidden_city_07.jpg "fig:Forbidden_city_07.jpg")\]\]
[Caserta_jardín_44.JPG](https://zh.wikipedia.org/wiki/File:Caserta_jardín_44.JPG "fig:Caserta_jardín_44.JPG")\]\]

**宮殿**通常指[君主居住](../Page/君主.md "wikilink")、使用的房屋，可由一座或多座房屋構成。在中文中最初是[君主](../Page/君主.md "wikilink")（[皇帝](../Page/皇帝.md "wikilink")、[王](../Page/王.md "wikilink")）所用的建筑物的专称，一般称举行礼仪和办公用的主体建筑物为“殿”，而称生活起居的部分为“宫”\[1\]。[西方国家中](../Page/西方国家.md "wikilink")，Palace泛指[君主](../Page/君主.md "wikilink")、[贵族](../Page/贵族.md "wikilink")、[主教或重要](../Page/主教.md "wikilink")[公衆人物所居住的](../Page/重要人物.md "wikilink")[房屋](../Page/房屋.md "wikilink")。

在國家[首都的主要宮殿是全國的](../Page/首都.md "wikilink")[權力中心與象徵](../Page/權力.md "wikilink")，如[故宮](../Page/故宮.md "wikilink")。宮殿的象徵意義引申為[政治或](../Page/政治.md "wikilink")[宗教的代號](../Page/宗教.md "wikilink")，如[克里姆林宮和](../Page/克里姆林宮.md "wikilink")[白宮](../Page/白宮.md "wikilink")。宮殿根據各國的環境情況而不同，如歐洲的宮殿不少被設計成[城堡](../Page/城堡.md "wikilink")、[堡壘形式](../Page/防禦工事.md "wikilink")，亞洲的宮殿有些設計成[寺廟](../Page/寺廟.md "wikilink")、[塔狀的形式](../Page/塔.md "wikilink")。

中國的宮殿則比較豐富，包括主要行政中心－皇宮，還有、[行宮](../Page/行宮.md "wikilink")、[花園等讓](../Page/花園.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")、[皇族](../Page/皇族.md "wikilink")、[貴族](../Page/貴族.md "wikilink")、[官員休息或行樂的地方](../Page/官員.md "wikilink")，都是大形的[園林建築群](../Page/園林.md "wikilink")。宫殿的范围进一步扩展，就是由诸多宫殿组成的[宫城](../Page/故宫_\(消歧义\).md "wikilink")。更一步扩展为[皇城](../Page/皇城.md "wikilink")。[中国传统建筑中](../Page/中国传统建筑.md "wikilink")，建筑物依照所有者身份遵循严格的等级制度，宫殿的级别是最高的。

## 地點

大多數的皇宮都建在首都，中國歷代的王朝以及各個地方政權都分別建造有自己的皇宮，又因为各朝都建有[行宫](../Page/行宫.md "wikilink")、离宫等建筑，因此宫殿遍布中國各地，例如[西安](../Page/西安.md "wikilink")、[洛阳](../Page/洛阳.md "wikilink")、[南京](../Page/南京.md "wikilink")、[北京](../Page/北京.md "wikilink")、[開封](../Page/開封.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[瀋陽](../Page/瀋陽.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[成都](../Page/成都.md "wikilink")、[承德等地](../Page/承德.md "wikilink")，[拉萨的](../Page/拉萨.md "wikilink")[布達拉宮亦作為西藏王宮](../Page/布達拉宮.md "wikilink")。

## 著名宫殿

### 亞洲

[150px](../Page/檔案:Palace_of_Versailles.jpg.md "wikilink")

  - [西安](../Page/西安.md "wikilink")：[阿房宫](../Page/阿房宫.md "wikilink")、[未央宫](../Page/未央宫.md "wikilink")、[长乐宫](../Page/长乐宫.md "wikilink")、[建章宫](../Page/建章宫.md "wikilink")、[大明宫](../Page/大明宫.md "wikilink")、[太极宫](../Page/太极宫.md "wikilink")、[兴庆宫](../Page/兴庆宫.md "wikilink")
  - [咸阳](../Page/咸阳.md "wikilink"): [咸阳宫](../Page/咸阳宫.md "wikilink")
  - [洛阳](../Page/洛阳.md "wikilink")：[南宫](../Page/南宫.md "wikilink")、[北宫](../Page/北宫.md "wikilink")、[上阳宫](../Page/上阳宫.md "wikilink")
  - [南京](../Page/南京.md "wikilink")：[六朝](../Page/六朝.md "wikilink")[台城](../Page/台城.md "wikilink")、[明故宫](../Page/明故宫.md "wikilink")
  - [北京](../Page/北京.md "wikilink")：[紫禁城](../Page/紫禁城.md "wikilink")（[故宫](../Page/故宫.md "wikilink")）、[北海公園](../Page/北海公園.md "wikilink")、[中南海](../Page/中南海.md "wikilink")、[南苑](../Page/南苑.md "wikilink")、[圆明园](../Page/圆明园.md "wikilink")、[颐和园](../Page/颐和园.md "wikilink")、[香山](../Page/香山_\(北京\).md "wikilink")[静宜园](../Page/静宜园.md "wikilink")、[玉泉山](../Page/玉泉山.md "wikilink")[静明园](../Page/静明园.md "wikilink")
  - [承德](../Page/承德.md "wikilink")：[避暑山庄](../Page/避暑山庄.md "wikilink")
  - [长春](../Page/长春.md "wikilink")：[伪满皇宫](../Page/伪满皇宫.md "wikilink")
  - [拉萨](../Page/拉萨.md "wikilink")：[布达拉宫](../Page/布达拉宫.md "wikilink")、[罗布林卡](../Page/罗布林卡.md "wikilink")
  - [沈阳](../Page/沈阳.md "wikilink")：[沈阳故宫](../Page/沈阳故宫.md "wikilink")
  - [正蓝旗](../Page/正蓝旗.md "wikilink")：[元上都](../Page/元上都.md "wikilink")
  - [烏蘭巴托](../Page/烏蘭巴托.md "wikilink")：[博克多汗冬宮](../Page/博克多汗冬宮.md "wikilink")
  - [開京](../Page/開京.md "wikilink")：[滿月臺](../Page/滿月臺.md "wikilink")
  - [首尔](../Page/首尔.md "wikilink")：[景福宫](../Page/景福宫.md "wikilink")
  - [曼谷](../Page/曼谷.md "wikilink")：[大王宫](../Page/曼谷大皇宫.md "wikilink")
  - [台南](../Page/台南.md "wikilink")：[寧靖王府](../Page/大天后宮.md "wikilink")、[安平古堡](../Page/安平古堡.md "wikilink")、[赤崁樓](../Page/赤崁樓.md "wikilink")
  - [顺化](../Page/顺化.md "wikilink")：[顺化皇城](../Page/顺化皇城.md "wikilink")
  - [东京](../Page/东京.md "wikilink")：[江戶城](../Page/江戶城.md "wikilink")、[皇居](../Page/皇居.md "wikilink")、[赤坂御用地](../Page/w:ja:赤坂御用地.md "wikilink")、[濱離宮](../Page/濱離宮恩賜庭園.md "wikilink")
  - [大阪](../Page/大阪.md "wikilink")：[大坂城](../Page/大坂城.md "wikilink")
  - [奈良](../Page/奈良.md "wikilink")：[飛鳥淨御原宮](../Page/飛鳥淨御原宮.md "wikilink")、[平城宮](../Page/平城宮.md "wikilink")
  - [京都](../Page/京都.md "wikilink")：[平安京](../Page/平安京.md "wikilink")、[京都御所](../Page/京都御所.md "wikilink")、[仙洞御所](../Page/仙洞御所.md "wikilink")、[修學院離宮](../Page/修學院離宮.md "wikilink")、[桂離宮](../Page/桂離宮.md "wikilink")、[二条城](../Page/二条城.md "wikilink")、[聚樂第](../Page/聚樂第.md "wikilink")、[花之御所](../Page/花之御所.md "wikilink")、[北山第](../Page/鹿苑寺.md "wikilink")
  - [德里](../Page/德里.md "wikilink")：[红堡](../Page/红堡.md "wikilink")

### 亞洲以外

  - [圣彼得堡](../Page/圣彼得堡.md "wikilink")：[冬宫](../Page/冬宫.md "wikilink")、[夏宫](../Page/夏宫.md "wikilink")
  - [莫斯科](../Page/莫斯科.md "wikilink")：[克里姆林宫](../Page/克里姆林宫.md "wikilink")
  - [巴黎](../Page/巴黎.md "wikilink")：[卢浮宫](../Page/卢浮宫.md "wikilink")、[杜伊勒里宫](../Page/杜伊勒里宫.md "wikilink")、[凡尔赛宫](../Page/凡尔赛宫.md "wikilink")、[爱丽舍宫](../Page/爱丽舍宫.md "wikilink")
  - [伦敦](../Page/伦敦.md "wikilink")：[伦敦塔](../Page/伦敦塔.md "wikilink")、[威斯敏斯特宫](../Page/威斯敏斯特宫.md "wikilink")、[白金汉宫](../Page/白金汉宫.md "wikilink")、[圣詹姆斯宫](../Page/圣詹姆斯宫.md "wikilink")
  - [柏林](../Page/柏林.md "wikilink")：[柏林城市宫](../Page/柏林城市宫.md "wikilink")
  - [维也纳](../Page/维也纳.md "wikilink")：[美泉宫](../Page/美泉宫.md "wikilink")
  - [罗马](../Page/罗马.md "wikilink")：[奎里纳尔宫](../Page/奎里纳尔宫.md "wikilink")、[梵蒂冈宫](../Page/梵蒂冈宫.md "wikilink")、[拉特朗宫](../Page/拉特朗宫.md "wikilink")
  - [伊斯坦布尔](../Page/伊斯坦布尔.md "wikilink")：[君士坦丁堡大皇宮](../Page/君士坦丁堡大皇宮.md "wikilink")、[托卡比皇宮](../Page/托卡比皇宮.md "wikilink")

## 参考文献

## 参见

  - [宫殿列表](../Page/宫殿列表.md "wikilink")
  - [城堡](../Page/城堡.md "wikilink")
  - [官邸](../Page/官邸.md "wikilink")

[hi:राजमहल](../Page/hi:राजमहल.md "wikilink")
[sr:Дворац](../Page/sr:Дворац.md "wikilink")

[宮殿](../Category/宮殿.md "wikilink")

1.