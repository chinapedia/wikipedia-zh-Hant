**宮迫博之**（Miyasako
Hiroyuki，）為[日本的](../Page/日本.md "wikilink")[搞笑藝人](../Page/搞笑藝人.md "wikilink")、[演員](../Page/演員.md "wikilink")、主持人等全方位藝人，所屬[吉本興業](../Page/吉本興業.md "wikilink")。
与[螢原徹組成](../Page/螢原徹.md "wikilink")[雨後敢死隊](../Page/雨後敢死隊.md "wikilink")，担当裝傻角色。[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[西淀川区出身](../Page/西淀川区.md "wikilink")。[吉本综合艺能学院](../Page/吉本综合艺能学院.md "wikilink")（大阪NSC）7期生。[血型B型](../Page/血型.md "wikilink")。[西淀川公害病患者](../Page/西淀川公害病.md "wikilink")（宮迫本人談）。
身為搞笑藝人與演員之外，並与[山口智充組成乐團](../Page/山口智充.md "wikilink")「[くず](../Page/くず.md "wikilink")(屑)」進行音樂活動。

## 演出作品

### 综艺节目

  - [水10\!](../Page/水10!.md "wikilink")[一晩R\&R](../Page/一晩R&R.md "wikilink")（[富士電視台](../Page/富士電視台.md "wikilink")）
  - [信长](../Page/信长.md "wikilink")（[中部日本放送](../Page/中部日本放送.md "wikilink")、[名古屋市地方](../Page/名古屋市.md "wikilink")）
  - [Ametalk](../Page/毒舌纠察队.md "wikilink")[(アメトーーク)](../Page/\(アメトーーク\).md "wikilink")([朝日電視台](../Page/朝日電視台.md "wikilink"))
  - [綜藝之神](../Page/綜藝之神.md "wikilink")(以紫SHIKIBU身份演出)([日本電視台](../Page/日本電視台.md "wikilink"))

### 电视剧

  - [美人](../Page/美人.md "wikilink")／屋代久志（[TBS](../Page/TBS.md "wikilink")）
  - [北條時宗 (大河劇)](../Page/北條時宗_\(大河劇\).md "wikilink")）／北條義宗
  - [大和拜金女](../Page/大和拜金女.md "wikilink")／ツネさん (客串)
  - [HERO (日本電視劇)](../Page/HERO_\(日本電視劇\).md "wikilink")／宮川雅史 (客串)
  - [急診室大醫生](../Page/急診室大醫生.md "wikilink")／馬場武藏
  - [急診室大醫生](../Page/急診室大醫生.md "wikilink") 新春SP／馬場武藏
  - [反乱のボヤージュ](../Page/反乱のボヤージュ.md "wikilink")／立花
  - [婚外戀愛](../Page/婚外戀愛.md "wikilink")／小野寺孝
  - [春浪漫](../Page/春浪漫.md "wikilink")／中川貫介
  - [東京愛情電影](../Page/東京愛情電影.md "wikilink")／千葉吉成
  - [老公當家](../Page/老公當家.md "wikilink")／杉尾優介
  - [愛上無賴男](../Page/愛上無賴男.md "wikilink")／一ツ橋信
  - [龍馬傳](../Page/龍馬傳.md "wikilink")）／平井收二郎
  - [絕對零度](../Page/絕對零度.md "wikilink")／塚本圭吾
  - [絕對零度](../Page/絕對零度.md "wikilink") SP／塚本圭吾
  - [夜行摩天輪](../Page/夜行摩天輪.md "wikilink")／遠藤啓介
  - [真相大白](../Page/真相大白.md "wikilink")／奥貫徹

### 電影

  - [岸和田少年愚連隊](../Page/岸和田少年愚連隊.md "wikilink")（初出演作品）
  - 蛇苺（初主演作品）
  - [13階段](../Page/13階段.md "wikilink")
  - [下妻物語](../Page/下妻物語.md "wikilink")
  - [再造人卡辛](../Page/再造人卡辛.md "wikilink")(CASSHERN)
  - [东京塔](../Page/东京塔.md "wikilink")（2005年公映）
  - [姑獲鳥之夏](../Page/姑獲鳥之夏.md "wikilink")（2005年夏天公映）
  - [妖怪大战争](../Page/妖怪大战争.md "wikilink")（2005年8月公映）
  - [20世紀少年](../Page/20世紀少年.md "wikilink")（2008年公映）

### 電視廣告

  - House食品「爽快的叹息」（2004年11月至2005年1月）

### 遊戲

  - [人中之龍3](../Page/人中之龍3.md "wikilink")（2009年）：神田強
  - [人中之龍6](../Page/人中之龍6.md "wikilink")（2016年）：南雲剛

### 配音

  - [大怪獸格鬥THE MOVIE
    超銀河傳說](../Page/大怪獸格鬥THE_MOVIE_超銀河傳說.md "wikilink")(2009年)：超人力霸王貝利亞
  - [凡赫辛](../Page/凡赫辛.md "wikilink")(2004年)：瑪莉希卡※劇場公開版
  - [超人特攻隊](../Page/超人特攻隊.md "wikilink")(2004年)：辛拉登
  - [疾風禁區](../Page/疾風禁區.md "wikilink")(2006年)：席丹
  - \[復仇者聯盟\]\]（2012年）：鷹眼

## 外部連結

  - [吉本興業官方網站](http://www.yoshimoto.co.jp/)
  - [大公網：飲醉裸照被公開
    宮迫博之表震驚](https://web.archive.org/web/20050328232259/http://www.takungpao.com/news/2005-3-28/UL-381814.htm)

[Category:吉本興業所屬搞笑藝人](../Category/吉本興業所屬搞笑藝人.md "wikilink")
[Category:日本男性搞笑藝人](../Category/日本男性搞笑藝人.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:每日電影獎最佳新人得主](../Category/每日電影獎最佳新人得主.md "wikilink")
[Category:橫濱電影節最佳新人獎得主](../Category/橫濱電影節最佳新人獎得主.md "wikilink")
[Category:報知電影獎最佳男配角得主](../Category/報知電影獎最佳男配角得主.md "wikilink")