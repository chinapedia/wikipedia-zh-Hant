[Dining_table_for_two.jpg](https://zh.wikipedia.org/wiki/File:Dining_table_for_two.jpg "fig:Dining_table_for_two.jpg")
[Furniture_1.jpg](https://zh.wikipedia.org/wiki/File:Furniture_1.jpg "fig:Furniture_1.jpg")设计的扶手椅和桌子\]\]
[Chinese_style_furniture_in_Bang_Pa_In_Chinese_style_palace.JPG](https://zh.wikipedia.org/wiki/File:Chinese_style_furniture_in_Bang_Pa_In_Chinese_style_palace.JPG "fig:Chinese_style_furniture_in_Bang_Pa_In_Chinese_style_palace.JPG")

**家具**，又称**家私**（也可寫成**-{傢}-具**或**-{傢}-私**，可能因此而常被人[誤寫成](../Page/錯別字.md "wikilink")“-{傢}-俱”或“-{傢}-俬”），是日常生活和[社会活动中为起居](../Page/社会.md "wikilink")，或[工作方便而配备的用具](../Page/工作.md "wikilink")，传统的家具都是独立于房屋主体结构之外可以移动的，现代很多家具已成为[建筑的一个组成部分](../Page/建筑.md "wikilink")。

狭义的家具只包括具有坐卧、凭倚、贮藏、间隔等功能的器具，而广义上的家具包括各种所有的家庭用具，如[家用电器](../Page/家用电器.md "wikilink")、[灯具等](../Page/灯.md "wikilink")。

## 功能分类

按功能家具可分为：

  - [桌](../Page/桌.md "wikilink")，包括[茶几](../Page/茶几.md "wikilink")、[写字台](../Page/写字台.md "wikilink")、[餐桌等](../Page/桌子.md "wikilink")
  - 坐卧用具，包括[椅子](../Page/椅.md "wikilink")、[沙发](../Page/沙发.md "wikilink")、[床等](../Page/床.md "wikilink")
  - 储藏家具，包括[书架](../Page/书架.md "wikilink")、床头柜、[衣橱](../Page/衣橱.md "wikilink")、电视柜、[梳妝台等](../Page/梳妝台.md "wikilink")
  - 厨房家具，包括[厨柜](../Page/厨柜.md "wikilink")、厨具等
  - 辅助家具，包括[窗簾](../Page/窗簾.md "wikilink")、[地毯等](../Page/地毯.md "wikilink")

按设计风格可分为：现代家具、中式古典家具、欧式古典家具、美式家具、在地家具（指當地生產的家具，特別指無名的工匠製造，不論是實踐傳統或追求最新式的原型，皆仔細地按照最初的原型做幅度的修改，簡化其形式，並使用當地材料和技術\[1\]）等。

按所用材料可分为：实木家具、板木结合家具、板式家具、钢木家具、复合板式家具、[石材家具](../Page/石.md "wikilink")、软体家具、[藤编家具](../Page/藤.md "wikilink")、[竹编家具](../Page/竹.md "wikilink")、和其他人造材材料制成的家具（如[玻璃家具](../Page/玻璃.md "wikilink")、不锈钢家具、[塑料家具等](../Page/塑料.md "wikilink")）。

## 家具設計

**家具设计**是[艺术和](../Page/艺术.md "wikilink")[技术的结合](../Page/技术.md "wikilink")，反映了当代[产品设计的发展](../Page/产品设计.md "wikilink")，偏重于艺术。从第一把量产的Thonet椅开始，优秀的[设计师会和](../Page/设计师.md "wikilink")[制造商共同合作来完成设计](../Page/制造商.md "wikilink")。家具设计既是制造商制造技术的体现，通过设计师的试验，表达美感的，在两方面的努力下就诞生了一件好的家具。

## 訂制家具

訂制家具，是指專為特定房間大小而造的家具。需要專人上門度尺和安裝的。有別於傳統的成品家具。訂制家具在中國的主要城市，如北京和上海等非常流行，而在香港則稱為「訂造傢俬」。流行的原因由於這些地方的地價非常高，所以每件傢具都要求有非常高的收納能力。一般訂制家具的設計會以入牆形式為主，即櫃體會與櫃體旁的天花和牆身無縫連接。這些家具一般以板式家具為主。常用到的板材有夾板、刨花板和纖維板等等。飾面的處理也非常多變，包裝焗漆、木皮、防火板和三聚氰胺浸漬膠膜紙等等。\[2\]

## 家具国家标准

  - 《家具国家标准》GB/T8324-1995
  - 《木家具通用技术条件》国家行业标准QB/T1951.1-94《木家具质量检验及质量评定》\[3\]
  - 《室内装饰装修材料-人造板及其制品中的甲醛释放限量》GB18584-2001
  - 《软体家具.沙发》B/T1952.1-2003
  - 《室内装饰装修材料溶合型木器涂料中有害物质限量》GB18581-2009
  - 《室内装饰装修材料胶粘剂中有害物质限量》GB 18583-2008
  - 《室内装饰装修材料木家具中有害物质限量》GB18584-2001

## 参考文献

[Category:家具](../Category/家具.md "wikilink")
[Category:木工](../Category/木工.md "wikilink")

1.  [藝術與建築索引典—在地家具](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300263334)
     於2011年3月17日查閱
2.
3.