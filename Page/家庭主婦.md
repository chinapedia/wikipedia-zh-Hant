[Good_housekeeping_1908_08_a.jpg](https://zh.wikipedia.org/wiki/File:Good_housekeeping_1908_08_a.jpg "fig:Good_housekeeping_1908_08_a.jpg")
**家庭主婦**（[注音](../Page/注音.md "wikilink")：ㄐㄧㄚ　ㄊㄧㄥˊ　ㄓㄨˇ　ㄈㄨˋ，英語：Housewife），簡稱**主婦**，是已婚[婦女從事的一種崗位](../Page/婦女.md "wikilink")，指全職照顧家庭、不外出工作的婦女。服務對象是[家庭的成員](../Page/家庭.md "wikilink")，包括[丈夫](../Page/丈夫.md "wikilink")、[兒](../Page/兒子.md "wikilink")[女](../Page/女兒.md "wikilink")，與[夫家成員同住的還要照顧夫家的成員](../Page/夫家.md "wikilink")。如由[男性](../Page/男性.md "wikilink")（丈夫）在家負責這些工作，由[妻子及其他家庭成員出外賺錢養家](../Page/妻子.md "wikilink")，則稱為[家庭主夫](../Page/家庭主夫.md "wikilink")。

## 劳动价值

虽然家庭主妇无劳动报酬，但以此種身分所付出的[勞動不應該是無償的](../Page/勞動.md "wikilink")，因為請傭人打掃煮飯幫傭也是有給職。而且各地区法律，包括婚姻法也有保障主婦的權益，例如[遺產分配權](../Page/遺產.md "wikilink")、[離婚婦女權益等](../Page/離婚.md "wikilink")。但各地社会对家庭主妇的劳动价值认可度不一。中国大陆普遍否家庭主妇的劳动价值。2010年，[全国政协委员](../Page/全国政协委员.md "wikilink")[张晓梅提出](../Page/张晓梅_\(1963年\).md "wikilink")“建议家务劳动工资化，切实保障女性权益”的提案\[1\]。虽有评论认为它因提出“承认家务劳动的价值”而成为当年[两会上最有价值的提案之一](../Page/两会.md "wikilink")，但除了普遍的质疑外，提案更引来了舆论无尽的嘲讽和挖苦\[2\]。

## 历史

在欧洲于19世纪进入[工业化社会之前](../Page/工业化.md "wikilink")，人类社会普遍处于[农业社会](../Page/农业社会.md "wikilink")。各地已婚女性以家庭主妇为业，兼以家庭为单位[从事农业](../Page/农民.md "wikilink")，是社会的主流。进入[工业社会后](../Page/工业社会.md "wikilink")，未婚女性外出工作被普遍接受，但已婚女性仍以家庭主妇为业。以美国为例，1890年，[联邦调查局所列出的](../Page/联邦调查局.md "wikilink")369种行业中，除9种外，皆有女性从业者。1900年时，美国女性的社会劳动参与率为20%，但女性婚后就业率仅5%，1910年时为10.7%。20世纪初至第二次世界大战结束期间，一战结束和工业化的加速，使越来越多的欧美女性接受教育，并走出家庭，从事社会职业。经过1930年代的[经济大萧条](../Page/经济大萧条.md "wikilink")，美国女性就业率从1930年的24.3%上升至1940年的25.4%。已婚女性就业率由1930年28.8%上升至1940年35%\[3\]。在二战期间，由于成年男性被大规模的征兵，更直接促使女性就业人数激增。

二战结束，男性回归工作岗位，女性就业率下降。另一方面，1950年代之后的经济繁荣，在欧洲和美国产生了更为保守的价值观。以美国为例，在1950年代，美国女性接受高等教育的目地，并非为职业前景，而是借此获得更好的婚姻。1950年代美国婚后女性就业率仅为11%，甚至少于1920年代。在1960年代、1970年代伴随着带有浓烈地[左翼色彩的](../Page/左翼.md "wikilink")[民权运动而出现的的女权主义运动](../Page/民权运动.md "wikilink")，带来了[美国和](../Page/美国.md "wikilink")[欧洲女性就业率的大幅度上升](../Page/欧洲.md "wikilink")。\[4\]\[5\]经历了女权运动高潮后，1978年美国已婚女性就业率上升到50%\[6\]。

1997年时，美国女性就业率达到61%。而在21世纪初头几年的经济繁荣期时，认同“返回家庭相夫教子”的“选择女权主义”回潮\[7\]。已婚女性就业率曾经回落到54%，\[8\]在2008年底[金融海嘯爆发后](../Page/金融海嘯.md "wikilink")，因为生活压力增大所迫，美国已婚女性就业率重新上扬，\[9\]
在2010年上升到69%左右，与在[改革开放](../Page/改革开放.md "wikilink")30多年后婚后女性就业率下降到77%的中国大陆已相差不远。\[10\]
\[11\]\[12\]

2008年底开始的[金融海嘯对男性为主要劳动力的](../Page/金融海嘯.md "wikilink")[建筑业](../Page/建筑业.md "wikilink")、[运输业损害较大](../Page/运输业.md "wikilink")，对女性为主要劳动力的[护理](../Page/护理.md "wikilink")、[清洁等工作损害较小](../Page/清洁.md "wikilink")，2010年美国劳动力市场一度出现女性总人数接近甚至超过男性总人数的可能\[13\]，但最终随着经济复苏和工作机会转好，美国男性重新巩固了就业总人数的多数优势。\[14\]

根据[英国广播公司](../Page/英国广播公司.md "wikilink")（[BBC](../Page/BBC.md "wikilink")）的调查数据，截止2013年1月止对[发达国家女性婚后就业率的统计](../Page/发达国家.md "wikilink")，在[北欧的](../Page/北欧.md "wikilink")[瑞典和](../Page/瑞典.md "wikilink")[芬兰](../Page/芬兰.md "wikilink")，有[6岁以下孩子却不辞职继续工作的妇女比例高达](../Page/6岁.md "wikilink")76％（有[18岁以下孩子不辞职坚持工作的妇女比例高达](../Page/18岁.md "wikilink")82％），[美国为](../Page/美国.md "wikilink")61％（有18岁以下孩子却不辞职坚持工作的妇女比例达70％），[英国为](../Page/英国.md "wikilink")55％，[德国为](../Page/德国.md "wikilink")53％。相比之下，[日本只有](../Page/日本.md "wikilink")34％。\[15\]

与在女性社会地位和[人权纪录方面表现较为恶劣的](../Page/人权.md "wikilink")[沙特阿拉伯等](../Page/沙特阿拉伯.md "wikilink")[海湾国家相比](../Page/海湾.md "wikilink")，\[16\]相对而言，[穆阿迈尔·卡扎菲领导下的](../Page/穆阿迈尔·卡扎菲.md "wikilink")[利比亚和](../Page/利比亚.md "wikilink")[萨达姆·侯赛因领导下的](../Page/萨达姆·侯赛因.md "wikilink")[伊拉克拥有](../Page/伊拉克.md "wikilink")[阿拉伯世界中较好的女性境遇与政策](../Page/阿拉伯世界.md "wikilink")，家庭主妇比例在40%到50%左右，在[中东地区属较低水平](../Page/中东地区.md "wikilink")，低于[沙特阿拉伯等](../Page/沙特阿拉伯.md "wikilink")[海湾国家](../Page/海湾.md "wikilink")。\[17\]

1990年之前，[北韓政府要求每一位健康的男性必须到](../Page/北韓.md "wikilink")[国有企业工作](../Page/国有.md "wikilink")，但允许约30%处于就业年龄阶段的已婚女性留在家里做全职[家庭主妇](../Page/家庭主妇.md "wikilink")，70%的朝鲜女性婚后继续工作（婚后女性就业率高于同地区的[日本](../Page/日本.md "wikilink")、[韩国和](../Page/韩国.md "wikilink")[台湾](../Page/台湾.md "wikilink")，但低于[苏联](../Page/苏联.md "wikilink")、[中国以及](../Page/中国.md "wikilink")[瑞典等](../Page/瑞典.md "wikilink")[北欧国家](../Page/北欧.md "wikilink")，与如今的美国相仿。\[18\]）\[19\]隨著1990年代朝鲜經濟崩塌，有的原本选择留在家中的婦女出外工作或從事小買賣而成為家庭的經濟支柱。現時，北韓市场中的商贩至少有四分之三是女性。[平壤甚至流传着一则笑话](../Page/平壤.md "wikilink")：[丈夫与宠物狗有何共同之处](../Page/丈夫.md "wikilink")？答案是：都不工作也不赚钱，但都很可爱，待在家里，还能吓跑[小偷](../Page/小偷.md "wikilink")\[20\]。

## 参看

  - [師奶](../Page/師奶.md "wikilink")
  - [家務](../Page/家務.md "wikilink") / [家政](../Page/家政.md "wikilink")
  - [家傭](../Page/家傭.md "wikilink")
  - [灰色經濟](../Page/灰色經濟.md "wikilink")
  - [零家務](../Page/零家務.md "wikilink")
  - [職業婦女](../Page/職業婦女.md "wikilink")

## 参考资料

[Category:家務勞動](../Category/家務勞動.md "wikilink")
[Category:家庭](../Category/家庭.md "wikilink")
[Category:女性职业](../Category/女性职业.md "wikilink")

1.
2.
3.
4.  [《焦作师范高等专科学校学报》2012年第2期，杨俊霞：简评美国女权主义运动的发展](http://www.cqvip.com/QK/85435A/201202/42514300.html)
5.  [1959年访问苏联的美国副总统尼克松与赫鲁晓夫进行“厨房辩论”：尼克松称美国的家庭主妇比苏联结婚后继续工作的妇女幸福](http://www.doc88.com/p-57886790766.html)
6.  [近几十年美国黑人、女性和非基督教徒社会地位的改善，究竟是谁之功](http://www.360doc.com/content/12/0715/23/865028_224437446.shtml)
7.  [贝蒂·弗里丹：她的一生和女权运动的未来](http://www.gmw.cn/01ds/2006-02/08/content_370856.htm)
8.  [新浪网读书频道2005-12-12：家庭主妇在美国回潮](http://book.sina.com.cn/nzt/soc/mgsrhpyjyd/28.shtml)

9.  [搜狐新闻，2008年5月9日：美国就业市场呈现“阴进阳退”趋势](http://news.sohu.com/20080509/n256757528.shtml)
10. [译言网：中国女人将统治世界？](http://article.yeeyan.org/view/91830/133129)
11. [（英文） ”By the end of 2010,both men and women say their employers
    have not done enough to accommodate a world where 70 percent of
    American families have a working mom.“
    “2010年底，无论男人还是女人都说自己的雇主们还没有做好准备迎接全美国70%的家庭有一个工作中的母亲的事实。”](http://www.npr.org/templates/story/story.php?storyId=113939266)
12. [美国就业与人口保障局数据（英文）](http://www.bls.gov/news.release/famee.nr0.htm)
13. [国际在线：美国女性就业人口将首次超过男性，2009年2月7日](http://gb.cri.cn/19224/2009/02/07/2225s2418324.htm)
14. [美国中文网：美国经济复苏男性夺走多数工作机会，2012年1月2日](http://www.sinovision.net/portal.php?mod=view&aid=200162)
15. [央视网：英国广播公司调查称日本女性婚后继续工作率为发达国家中最低](http://news.cntv.cn/2013/03/22/ARTI1363945606315673.shtml)
16. [人们都在说卡扎菲因为独裁而失去利比亚，可难道跟美国打利比亚的全都民主？](http://bbs.tianya.cn/post-worldlook-379255-1.shtml)
17. [周视天下：阿拉伯民族复兴社会主义的衰败](http://www.zhoushitianxia.com/2/Html/2012-8-28/1404.html)

18. [译言网：中国女人将统治世界？](http://article.yeeyan.org/view/91830/133129)
19. [第一金融网：韩国国民大学美国籍教授兰科夫(Andrei
    Lankov)：朝鲜妇女穿裤子（当家）](http://www.afinance.cn/new/xzgd/201004/266255.html)
20. [第一金融网：韩国国民大学美国籍教授兰科夫(Andrei
    Lankov)：朝鲜妇女穿裤子（当家）](http://www.afinance.cn/new/xzgd/201004/266255.html)