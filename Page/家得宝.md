**家得寶**（[英語](../Page/英語.md "wikilink")：**The Home
Depot**；）是[美國一家家庭裝飾品與建材的](../Page/美國.md "wikilink")[零售商](../Page/零售.md "wikilink")，總部設於[喬治亞州](../Page/喬治亞州.md "wikilink")[科布縣](../Page/科布縣.md "wikilink")[維寧斯市](../Page/維寧斯.md "wikilink")。家得寶僱用超過355,000名員工，經營2,164家大型商場，分店遍及[美國](../Page/美國.md "wikilink")（包括50個[州份](../Page/美國州份.md "wikilink")、[哥倫比亞特區](../Page/哥倫比亞特區.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[處女群島和](../Page/美屬處女群島.md "wikilink")[關島](../Page/關島.md "wikilink")）、[加拿大](../Page/加拿大.md "wikilink")（10個[省份](../Page/加拿大行政區劃.md "wikilink")）、[墨西哥及](../Page/墨西哥.md "wikilink")[中國](../Page/中華人民共和國.md "wikilink")。

2006年，家得寶的銷售額達908億[美元](../Page/美元.md "wikilink")。儘管錄得10%的收入增長，家得寶在2007年《[財富](../Page/財富_\(雜誌\).md "wikilink")》雜誌[財富500大企業名錄中下跌了三位](../Page/Fortune_500.md "wikilink")（第17位），2005年與2006年的排名分別是第13位和第14位。

## 歷史

1978年，[伯尼·馬庫斯](../Page/伯尼·馬庫斯.md "wikilink")（Bernie
Marcus）和[阿瑟·布蘭克](../Page/阿瑟·布蘭克.md "wikilink")（Arthur
Blank）被[Handy
Dan公司解僱](../Page/Handy_Dan.md "wikilink")，其後兩人於[美國](../Page/美國.md "wikilink")[喬治亞州](../Page/喬治亞州.md "wikilink")[亞特蘭大創立家得寶](../Page/亞特蘭大.md "wikilink")。家得寶發展得很快，1996年前已獲得超過10億[美元的全年銷售額](../Page/美元.md "wikilink")。1997年，家得寶將業務拓展至[智利與](../Page/智利.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")，並在這些國家的[景氣時期取得前所未見的成功](../Page/景氣.md "wikilink")。但是，由於受到傳統上奉行[社會主義經濟](../Page/社會主義.md "wikilink")（不同於[墨西哥的高度](../Page/墨西哥.md "wikilink")[資本主義導向經濟](../Page/資本主義.md "wikilink")）的國家的工會影響，家得寶希望將相關業務撤離，避免與當地政府發生[利益衝突](../Page/利益衝突.md "wikilink")。至今，家得寶仍然活躍於[南美洲](../Page/南美洲.md "wikilink")，而2006年在[中國開設的](../Page/中國.md "wikilink")12家新分店更成為家得寶的新焦點，因為中國的業務帶來更理想的业绩。\[1\]
2000年，馬庫斯和伯朗克相繼退休，[羅伯特·納德利](../Page/羅伯特·納德利.md "wikilink")（Robert
Nardelli）被委任為新[主席](../Page/主席.md "wikilink")、[董事長兼](../Page/董事長.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")。納德利其後於2007年1月由[弗蘭克·布萊克](../Page/弗蘭克·布萊克.md "wikilink")（Frank
Blake）接任。 <ref name="Corporate news release">{{ cite web

`|url         = `<http://ir.homedepot.com/ReleaseDetail.cfm?ReleaseID=224078>
`|publisher   = 家得寶`
`|title       = 消息發佈`
`|date        = 2007年1月3日`
`|deadurl     = yes`
`|archiveurl  = `<https://web.archive.org/web/20070502072638/http://ir.homedepot.com/ReleaseDetail.cfm?ReleaseID=224078>
`|archivedate = 2007年5月2日`

}}</ref>

## 現況

家得寶[董事局的現任成員包括](../Page/董事局.md "wikilink")[布倫尼曼](../Page/布倫尼曼.md "wikilink")（Greg
Brenneman）、[理察·布朗](../Page/理察·布朗.md "wikilink")（Richard H.
Brown）、[求约翰·克伦德宁](../Page/求约翰·克伦德宁.md "wikilink")（John
Clendenin）、[克劳迪奥·冈萨雷斯](../Page/克劳迪奥·冈萨雷斯.md "wikilink")（Claudio
González）、[米利奇·哈特](../Page/米利奇·哈特.md "wikilink")（Milledge
Hart）、Bonnie Hill、、、[肯·蘭格恩](../Page/肯·蘭格恩.md "wikilink")（Ken
Langone）及[湯姆·里奇](../Page/湯姆·里奇.md "wikilink")（Tom Ridge）。

2007年1月2日，家得寶與[羅伯特·納德利達成協議](../Page/羅伯特·納德利.md "wikilink")，維德利終告卸下為期六年的[行政總裁一職](../Page/行政總裁.md "wikilink")。但是，由於他五年來以高壓手段管理公司，而其薪酬福利（撇除高級職員優先認股權津貼）又處於12,370萬[美元的極高水平](../Page/美元.md "wikilink")，加上，家得寶的股票比競爭對手[Lowe's表現不濟](../Page/Lowe's.md "wikilink")，他的離任可謂與外界的控訴不無關係。另外，他的2.1億美元離職金亦被批評為[黃金降落傘](../Page/黃金降落傘.md "wikilink")，因為他在家得寶股價下滑時賣出股份。\[2\]
\[3\]納德利的繼任者是[弗蘭克·布萊克](../Page/弗蘭克·布萊克.md "wikilink")，他曾經擔任家得寶的董事局副主席兼執行副總裁。一眾[股東消除了對納德利離任的疑慮](../Page/股東.md "wikilink")，但轉向質疑弗蘭克·布萊克能否勝任新任行政總裁一職，把龐大的零售業務妥善地管理。\[4\]
\[5\]

## 經營方式

[Home_Depot.png](https://zh.wikipedia.org/wiki/File:Home_Depot.png "fig:Home_Depot.png")本土[48州的分店分佈](../Page/美國州份.md "wikilink")\]\]
家得寶旗下的分店佔地廣闊，平均有105,000[平方呎](../Page/平方呎.md "wikilink")（9,755平方米），這種大型零售店的設計可確保庫存充足。

### 廣告與標語

家得寶以[淺橙色](../Page/橙色.md "wikilink")（PMS 165，CMYK
60M100Y）作為公司顏色，並用於招牌、裝備和員工服裝。自2003年起，家得寶以「你做得到，我們幫得到」（You
can do it. We can
help）為[廣告標語](../Page/廣告.md "wikilink")。在過去25年，家得寶曾先後採用「家得寶，低價只是開始」（The
Home Depot, Low prices are just the
beginning，1990年代初期）、「在家得宝，感觉就像在家里一样」（When
you're at the Home Depot, You'll feel right at
home，1990年代後期），以及「家得寶：改善家居的首選！」（The
Home Depot: First In Home Improvement\!，1999年-2003年）招徠顧客。

### 品牌

家得寶擁有數個獨家品牌，包括[Hampton
Bay](https://web.archive.org/web/20081029040954/http://www.hamptonbay.com/)（照明、-{吊}-扇與天井家具）、[Pegasus](http://www.pegasusinfo.com/)（廚具與浴具）、[Glacier
Bay](http://www.glacierbayinfo.com/)（水龍頭與浴缸）、[Husky](http://www.huskytools.com/)（工具）、[Vigoro](http://www.vigoro.com/)（肥料）、[Ryobi](http://www.ryobitools.com/)（電源工具）、[Millstead](http://www.millsteadinfo.com)、[Mill's
Pride](http://www.millspride.com)、Workforce、[Thomasville
cabinetry](http://www.thomasvillecabinetry.com/)、[Behr](http://www.behr.com)（油漆）及
[Ralph Lauren
paint](http://rlhome.polo.com/rlhome/products/paint/default.asp)。

家得寶亦擁有連鎖高端家庭裝飾及用具商店[EXPO Design
Center](http://expo.com/)。2006年，家得寶收購了[Hughes
Supply](../Page/Hughes_Supply.md "wikilink")，並將其同化為[HD
Supply](http://www.hdsupplyinc.com/)的服務承包商。2005年9月，在高端線上照明商店[Paces
Trading
Company推出不久](../Page/Paces_Trading_Company.md "wikilink")，家得寶主管部門隨即開設另一家高端線上家具商店[10
Crescent
Lane](../Page/10_Crescent_Lane.md "wikilink")。2006年中旬，家得寶收購[Home
Decorators
Collection](../Page/Home_Decorators_Collection.md "wikilink")，並將其納入主管部門，成為附加品牌。

### 燃料中心

自2006年起，家得寶開始在旟下的一些分店開設燃料中心，首批設於[美國](../Page/美國.md "wikilink")[田納西州Hermitage](../Page/田納西州.md "wikilink")、Brentwood兩市及[喬治亞州Acworth市的然料中心預計每年可賺取](../Page/喬治亞州.md "wikilink")500萬-700萬[美元](../Page/美元.md "wikilink")。燃料中心主要售賣[啤酒](../Page/啤酒.md "wikilink")、[熟食](../Page/熟食.md "wikilink")、[零食](../Page/零食.md "wikilink")，並在個別位置提供[柴油](../Page/柴油.md "wikilink")，方便駕駛者為車輛入油。另外，小卡車可享用燃料中心全車清洗服務。

## 國際業務

### 加拿大

家得寶加拿大是家得寶的[加拿大分公司](../Page/加拿大.md "wikilink")，也是加拿大最重要的家庭裝飾品[零售商之一](../Page/零售商.md "wikilink")。家得寶加拿大旗下約有150家分店，在當地僱用超過26,000名員工。家得寶加拿大透過電子工具，如線上與型錄銷售，在加拿大所有省份與[努那福特地區](../Page/努那福特.md "wikilink")、[西北地區及](../Page/西北地區_\(加拿大\).md "wikilink")[育空地區設立分店](../Page/育空地區.md "wikilink")。

家得寶加拿大是由[Aikenhead
Hardware併購組成](../Page/Aikenhead_Hardware.md "wikilink")。家得寶的管理層一直有志於壓倒其最大的競爭對手──[Rona](../Page/Rona.md "wikilink")，該公司旗下的分店數目是家得寶的四倍有多。但是，Rona公司許多分店的面積是比家得寶的小得多，況且，以大型商場以言，家得寶是有過之而無不及的。同時，家得寶亦面對來自[Lowe's的競爭](../Page/Lowe's.md "wikilink")，該公司於2007年把業務遷進加拿大市場；Lowe公司的首批加拿大商店將會設於[安大略省](../Page/安大略省.md "wikilink")。

### 墨西哥

2001年，家得寶進軍[墨西哥](../Page/墨西哥.md "wikilink")，至今已成為墨西哥最大的零售商之一，旗下營運超過50家分店，僱用超過6,600員工。邊境城鎮的家得寶吸引不少美國顾客，他們在蒂华纳、墨西卡利、华雷斯城、新拉雷多和马塔莫罗斯等地用[美元購買類似的家庭裝飾品](../Page/美元.md "wikilink")。2006年，家得寶展開新計劃，讓墨西哥員工以墨西哥國民和拉丁美洲人的「客工」身分，到美國的家得寶方便、合法地工作。

### 中國

2006年12月，家得寶宣佈併購[中國家居建材](../Page/中國.md "wikilink")[超市](../Page/超市.md "wikilink")[家世界](../Page/家世界.md "wikilink")\[6\]，藉以短時間內進軍中國市場。目前十家分店設於[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[西安和](../Page/西安.md "wikilink")[鄭州四個城市](../Page/鄭州.md "wikilink")。

2012年9月14日，家得宝宣布关闭在中的所有门店，全线退出中国市场。其原因在於對中國DIY文化的誤判，儘管有同樣販售組裝產品的生產商，如IKEA等很常見，但是若組裝時間長或過於複雜不易，與美國人又買工具組裝相反，中國人偏好取得已經完成的產品，顧客會希望直接由專人推薦傢俱安裝，但組裝好的美式家具又較大而不適合公寓和搬運等不受青睞，因而家得宝在中國市場的總銷量很差，此失誤造成公司損失1.6億美元。

2013年重新以漆料專賣店方式回歸，目前已經拓展了6家專賣店。

### 其他

家得寶沒明確表示欲進軍其他國家，但有傳聞指公司未來會把業務拓展至[歐洲](../Page/歐洲.md "wikilink")（如[英國](../Page/英國.md "wikilink")）和[東亞](../Page/東亞.md "wikilink")（如[日本](../Page/日本.md "wikilink")）。家得寶像其他大型[零售公司](../Page/零售.md "wikilink")（40年來，[沃爾瑪是零售業的先驅者](../Page/沃爾瑪.md "wikilink")）一樣，貫徹強硬的無工會政策。然而，在一些州份或國家，家得寶對工會表示一定的尊重。1997-2002年，家得寶在[南美洲擁有九家分店](../Page/南美洲.md "wikilink")，但由於地區經濟衰退、[左翼政府干預和工會影響](../Page/左翼.md "wikilink")，在2003年前已停止擴張業務。

## 主要贊助

[HomeDepotStorefront.JPG](https://zh.wikipedia.org/wiki/File:HomeDepotStorefront.JPG "fig:HomeDepotStorefront.JPG")[紐約家得寶的舊店面設計](../Page/紐約.md "wikilink")\]\]
[Homedepotcalifornia.jpg](https://zh.wikipedia.org/wiki/File:Homedepotcalifornia.jpg "fig:Homedepotcalifornia.jpg")

自1991年起，家得寶擔任體育運動的大贊助商，為[美國與](../Page/美國.md "wikilink")[加拿大兩國的](../Page/加拿大.md "wikilink")[奧運隊伍提供贊助](../Page/奧林匹克運動會.md "wikilink")，並且開辦計劃，協助運動員適應各項訓練與比賽。儘管家得寶仍然支援加拿大奧運選手，但是該公司對加拿大奧運隊的贊助已經在2005年終止。

2002年2月，身為家得寶創辦人之一的[阿瑟·布蘭克購得](../Page/阿瑟·布蘭克.md "wikilink")[美國](../Page/美國.md "wikilink")[-{zh-hans:国家橄榄球大联盟;zh-hk:國家欖球聯盟;zh-tw:國家美式足球聯盟;}-的](../Page/國家美式足球聯盟.md "wikilink")[亞特蘭大獵鷹球隊的經銷權](../Page/亞特蘭大獵鷹.md "wikilink")。家得寶也是2002年、2005年兩屆美國[全國賽車聯合會](../Page/全國賽車聯合會.md "wikilink")（National
Association for Stock Car Auto
Racing，NASCAR）冠軍──車隊的主要贊助商。賽車手[托尼·斯圖沃特](../Page/托尼·斯圖沃特.md "wikilink")（Tony
Stewart）駕駛名為「家得寶」的20號[雪佛蘭](../Page/雪佛蘭.md "wikilink")。

家得寶是[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[卡森市的](../Page/卡森.md "wikilink")[家得寶中心球場的冠名贊助商](../Page/家得寶中心球場.md "wikilink")、[美國職業足球大聯盟的](../Page/美國職業足球大聯盟.md "wikilink")[洛杉磯銀河和](../Page/洛杉磯銀河.md "wikilink")[美國芝華士](../Page/美國芝華士.md "wikilink")、[洛杉磯激流隊](../Page/洛杉磯激流隊.md "wikilink")（Los
Angeles Riptide，[長柄曲棍球大聯盟](../Page/長柄曲棍球大聯盟.md "wikilink")；Major League
Lacrosse），以及許多重要體育盛事的所在地。

2007年1月，家得寶成為美國[-{zh-hans:国家橄榄球大联盟;zh-hk:國家欖球聯盟;zh-tw:國家美式足球聯盟;}-的官方家庭裝飾品贊助商](../Page/國家美式足球聯盟.md "wikilink")。

值得一提的是，家得宝寶除了赞助一系列体育赛事，还为社区家庭给予回报。他们每月的第一个周末，在全美几千家店面为社区孩子提供小木工作坊活动(build
and grow
workshop)。至今[已经坚持了二十年](http://mp.weixin.qq.com/s/1Y5TM8rWDfuN9Jb2ibvYcA)[1](https://web.archive.org/web/20070121174052/http://ir.homedepot.com/ReleaseDetail.cfm?ReleaseID=224939)

## 參考

## 外部連結

  - [家得宝中国](https://web.archive.org/web/20100324093137/http://www.homedepot.com.cn/)
  - [10 Crescent Lane](http://www.10crescentlane.com/)
  - [Bernie
    Marcus](http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-1920&sug=y)

  - [Expo Design Center](http://www.expo.com/)
  - [Home Decorators Collection](http://www.homedecorators.com/)
  - [家得寶加拿大](http://www.homedepot.ca/)
  - [家得寶墨西哥](http://homedepot.com.mx)
  - [家得寶美國](http://www.homedepot.com)
  - [家得寶美國](https://web.archive.org/web/20070513040315/http://homedepotespanol.com/hdus/esus/index.shtml)
  - [家得寶](https://web.archive.org/web/20070116080446/http://corporate.homedepot.com/wps/portal/)（集團網站）
  - [家得寶](http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-1886&sug=y)（學術文章）
  - [家得寶在一些社區不會受歡迎](https://web.archive.org/web/20170913130326/http://no2homedepot.com/)
  - [Home Depot stock
    quote](https://web.archive.org/web/20070926234420/http://www.gstock.com/s=hd)

  - [Hughes Supply](http://www.hughessupply.com/)
  - [Paces Trading Company](http://www.pacestradingcompany.com/)
  - [RIDGID Tools](http://www.ridgid.com/)
  - [Build and grow workshop
    小木工作坊](http://mp.weixin.qq.com/s/1Y5TM8rWDfuN9Jb2ibvYcA)(微信文章)

[H](../Category/道瓊斯工業平均指數成份股.md "wikilink")
[H](../Category/標準普爾500指數成分股.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:零售商](../Category/零售商.md "wikilink")
[H](../Category/量販店.md "wikilink")

1.  {{ cite web
    |url=<http://www.forbes.com/2002/03/21/0321homedepot.html>
    |publisher=[《福布斯》](http://Forbes.com) |title=Home Depot Finds The
    World A Small Place |date=2002年3月21日 }}

2.
3.  {{ cite web
    |url=<http://www.businessweek.com/bwdaily/dnflash/content/jan2007/db20070103_536329.htm>
    |publisher=《商業週刊》 |title=Home Depot's Surprising Choice for CEO
    |date=2007年1月4日 }}

4.
5.  {{ cite web
    |url=<http://www.forbes.com/markets/2007/01/03/hd-nardelli-update-markets-equity-cx_rs_0103hd.html>
    |publisher=[《福布斯》](http://Forbes.com) |title=Nardelli Bails On Home
    Depot |date=2007年1月3日 }}

6.  <http://www.prnewswire.com/mnr/homedepot/26373/>