**家豬**（[学名](../Page/学名.md "wikilink")：**）是[野猪被](../Page/野猪.md "wikilink")[人类驯化后所形成的](../Page/人类.md "wikilink")[亚种](../Page/亚种.md "wikilink")，[獠牙較野猪短](../Page/獠牙.md "wikilink")，是人類的[家畜之一](../Page/家畜.md "wikilink")，目的是從豬身上取得[豬肉](../Page/豬肉.md "wikilink")、[豬腳等食材](../Page/豬腳.md "wikilink")。人類蓄養家豬的[歷史相當悠久](../Page/歷史.md "wikilink")，不過至16世紀才被人类带入[美洲](../Page/美洲.md "wikilink")，中國飼養的豬即是人類最早馴養的豬的直系後代\[1\]。20世紀後期，家豬發展培育達到成熟。豢養區域，多為生產[穀物或](../Page/穀物.md "wikilink")[玉米的農產產地](../Page/玉米.md "wikilink")。

世界各地飼養家豬的方式多為[圈養](../Page/圈養.md "wikilink")，例如[台灣的蓄豬業](../Page/台灣.md "wikilink")。20世紀中期以前，此地區家豬多食加熱過的[廚餘或](../Page/廚餘.md "wikilink")[蕃薯葉](../Page/蕃薯葉.md "wikilink")，且為個別小規模蓄養，不過現今以企業化大型經營為主，飼料部分也多改成專用飼料。根據[世界糧食組織的統計](../Page/世界糧食組織.md "wikilink")\[2\]，在2010年底，全世界家豬約為9.65亿頭。其中，[中國境內的家豬為](../Page/中國.md "wikilink")4.76亿頭，佔總數49%，為全世界第一；第二名則為[美國](../Page/美國.md "wikilink")，為6.49千万隻。

在[台灣部分區域](../Page/台灣.md "wikilink")，如[客家地區以及](../Page/客家地區.md "wikilink")[新北市](../Page/新北市.md "wikilink")[三峽區的](../Page/三峽區.md "wikilink")[清水祖師廟仍有](../Page/三峽長福巖.md "wikilink")[神豬競賽活動](../Page/神豬.md "wikilink")，為地方上傳統文化\[3\]。

## 生物学特征

[Little_pigs_are_eating.jpg](https://zh.wikipedia.org/wiki/File:Little_pigs_are_eating.jpg "fig:Little_pigs_are_eating.jpg")
[Saitama_Domestic_Pigs_In_Pasture_1.jpg](https://zh.wikipedia.org/wiki/File:Saitama_Domestic_Pigs_In_Pasture_1.jpg "fig:Saitama_Domestic_Pigs_In_Pasture_1.jpg")
[Hog_confinement_barn_interior.jpg](https://zh.wikipedia.org/wiki/File:Hog_confinement_barn_interior.jpg "fig:Hog_confinement_barn_interior.jpg")
家猪的[染色体数量为](../Page/染色体.md "wikilink")19對38條\[4\]。猪的毛色有很多种，白黑色，如白色、全黑色、白带猪、花猪、棕色或红色及污白毛等。毛色的不同主要由黑色素等一类物质所决定。

豬體肥肢短，性温驯，适应力强，易饲养，繁殖快。猪出生后5-12个月可以配种，[妊娠期约为](../Page/妊娠.md "wikilink")114天左右（333记忆法，3个月3周零3天）。豬的平均[寿命为](../Page/寿命.md "wikilink")15年，但一般養豬場的豬隻多在5個月時、90公斤左右宰殺供人食用，就算是農家兼養的餵餿水的豬，也頂多在一年左右就宰殺，所以食用的豬隻都算是小豬。

猪体表无[汗腺](../Page/汗腺.md "wikilink")，在腹部有类似汗腺的[腺体](../Page/腺体.md "wikilink")，降低[体温的主要方法是将泥巴涂满全身](../Page/体温.md "wikilink")\[5\]。

英國《[每日郵報](../Page/每日郵報.md "wikilink")》稱豬的用途達185項——從糖果和洗髮水製造，到麵包、啤酒、子彈。\[6\]

猪的行為和人或狗比較接近，和牛羊等動物有比較大的差別。在很多方面，猪是介於[肉食性動物和高度進化的](../Page/肉食性.md "wikilink")[偶蹄目動物之間](../Page/偶蹄目.md "wikilink")\[7\]。家猪一般會尋找同伴，和同伴蜷縮在一起，維持身體的接觸，但很少會自然的形成一大群。

## 繁殖与育种

家猪的繁殖主要通过[自然交配和](../Page/自然交配.md "wikilink")[人工授精的方式进行](../Page/猪人工授精.md "wikilink")。

### 家豬的品種

豬隻的馴養，一般認為是全球各地分別將當地[野豬馴化](../Page/野豬.md "wikilink")，經過長達數千年歷史的雜交，豬隻品系繁多，光[中國就有數十種](../Page/中國.md "wikilink")。[十八世紀](../Page/十八世紀.md "wikilink")，[歐洲人引進中國豬品系到歐陸](../Page/歐洲.md "wikilink")，與當地野豬馴化後裔交配，培養出高換肉率的現代肉豬，成為世界主流肉豬品種，佔台灣豬隻95%以上的白豬，就屬此一系列。

目前，全世界范围有超过400个猪种。其中，[中国大陆拥有地方猪品种](../Page/中国大陆.md "wikilink")64个，为各國之首。包括[东北民猪](../Page/东北民猪.md "wikilink")、[西北八眉猪](../Page/西北八眉猪.md "wikilink")、[黄淮海黑猪](../Page/黄淮海黑猪.md "wikilink")、[汉江黑猪](../Page/汉江黑猪.md "wikilink")、[沂蒙黑猪](../Page/沂蒙黑猪.md "wikilink")、[两广小花猪](../Page/两广小花猪.md "wikilink")、[粤东黑猪](../Page/粤东黑猪.md "wikilink")、[海南猪](../Page/海南猪.md "wikilink")、[滇南小耳猪](../Page/滇南小耳猪.md "wikilink")、[蓝塘猪](../Page/蓝塘猪.md "wikilink")、[香猪](../Page/香猪.md "wikilink")、[隆林猪](../Page/隆林猪.md "wikilink")、[槐猪](../Page/槐猪.md "wikilink")、[五指山猪](../Page/五指山猪.md "wikilink")、[宁乡猪](../Page/宁乡猪.md "wikilink")、[华中两头乌猪](../Page/华中两头乌猪.md "wikilink")、[湘西黑猪](../Page/湘西黑猪.md "wikilink")、[大围子猪](../Page/大围子猪.md "wikilink")、[大花白猪](../Page/大花白猪.md "wikilink")、[金华猪](../Page/金华猪.md "wikilink")、[龙游乌猪](../Page/龙游乌猪.md "wikilink")、[闽北花猪](../Page/闽北花猪.md "wikilink")、[嵊县花猪](../Page/嵊县花猪.md "wikilink")、[乐平猪](../Page/乐平猪.md "wikilink")、[杭猪](../Page/杭猪.md "wikilink")、[赣中南花猪](../Page/赣中南花猪.md "wikilink")、[玉江猪](../Page/玉江猪.md "wikilink")、[武夷黑猪](../Page/武夷黑猪.md "wikilink")、[清平猪](../Page/清平猪.md "wikilink")、[南阳黑猪](../Page/南阳黑猪.md "wikilink")、[皖浙花猪](../Page/皖浙花猪.md "wikilink")、[莆田猪](../Page/莆田猪.md "wikilink")、[福州黑猪](../Page/福州黑猪.md "wikilink")、[太湖猪](../Page/太湖猪.md "wikilink")、[姜曲海猪](../Page/姜曲海猪.md "wikilink")、[东串猪](../Page/东串猪.md "wikilink")、[虹桥猪](../Page/虹桥猪.md "wikilink")、[圩猪](../Page/圩猪.md "wikilink")、[阳新猪](../Page/阳新猪.md "wikilink")、[内江猪](../Page/内江猪.md "wikilink")、[荣昌猪](../Page/荣昌猪.md "wikilink")、[成华猪](../Page/成华猪.md "wikilink")、[雅南猪](../Page/雅南猪.md "wikilink")、[湖川山地猪](../Page/湖川山地猪.md "wikilink")、[乌金猪](../Page/乌金猪.md "wikilink")、[关岭猪](../Page/关岭猪.md "wikilink")、[藏猪等](../Page/藏猪.md "wikilink")。培育品种有20多个，包括[哈尔滨白猪](../Page/哈尔滨白猪.md "wikilink")、[东北花猪](../Page/东北花猪.md "wikilink")、[新淮猪](../Page/新淮猪.md "wikilink")、[北京黑猪](../Page/北京黑猪.md "wikilink")、[上海白猪](../Page/上海白猪.md "wikilink")、[伊犁白猪](../Page/伊犁白猪.md "wikilink")、[赣州白猪](../Page/赣州白猪.md "wikilink")、[汉中白猪](../Page/汉中白猪.md "wikilink")、[三江白猪](../Page/三江白猪.md "wikilink")、[新金猪](../Page/新金猪.md "wikilink")、[北京花猪](../Page/北京花猪.md "wikilink")、[泛农花猪](../Page/泛农花猪.md "wikilink")、[山西黑猪等](../Page/山西黑猪.md "wikilink")。引进品种有巴克夏猪、中约克夏猪、大约克夏猪、苏白猪、克米洛夫猪、长白猪等。

目前在[美国只有](../Page/美国.md "wikilink")9个[品种登记在册](../Page/品种.md "wikilink")：[巴克夏猪](../Page/巴克夏猪.md "wikilink")、[切斯特白猪](../Page/切斯特白猪.md "wikilink")、[杜洛克猪](../Page/杜洛克猪.md "wikilink")、[漢普夏猪](../Page/漢普夏猪.md "wikilink")、[海福特猪](../Page/海福特猪.md "wikilink")、[長白猪](../Page/長白猪.md "wikilink")、[波中猪](../Page/波中猪.md "wikilink")、[花斑猪](../Page/花斑猪.md "wikilink")、[約克夏猪](../Page/約克夏猪.md "wikilink")。\[8\]

在[台灣的豬隻大體分為三大品系](../Page/台灣.md "wikilink")：

  - 台灣外來種品系
      - 大規模飼養的[肉豬](../Page/肉豬.md "wikilink")、[白豬](../Page/白豬.md "wikilink")，包括[約克夏豬](../Page/約克夏豬.md "wikilink")、[藍瑞斯豬](../Page/藍瑞斯豬.md "wikilink")、[杜洛克豬等](../Page/杜洛克豬.md "wikilink")。現在所飼養的白肉豬多是以上三品種[雜交產生](../Page/雜交.md "wikilink")，稱為L(藍瑞斯豬)Y(約克夏豬)D(杜洛克豬)豬。

<!-- end list -->

  - 台灣原生種品系
      - 台灣家豬的原生種品系有，包括現存的[桃園豬](../Page/桃園豬.md "wikilink")、[頂雙溪豬](../Page/頂雙溪豬.md "wikilink")、[美濃豬](../Page/美濃豬.md "wikilink")、[梅山豬和](../Page/梅山豬.md "wikilink")[蘭嶼小耳豬等三品系](../Page/蘭嶼小耳豬.md "wikilink")；另外，早期台灣原住民飼養，消失或滅絕的品系有「小耳豬」、「小型長鼻豬」、「大型長鼻豬」等。
      - 在桃竹苖一帶[客家農家兼養的桃園黑豬](../Page/客家.md "wikilink")（面部皮皺），現在被列為國家保種動物之一。
      - [台灣原生種或在來種的](../Page/台灣原生種.md "wikilink")[蘭嶼小耳豬](../Page/蘭嶼小耳豬.md "wikilink")，據說[荷蘭領台時](../Page/荷蘭.md "wikilink")，台灣的豬隻都屬此一品系，但目前十分珍稀，現在被列為國家保種動物之一。

### 养猪生产

[猪生产学是](../Page/猪生产学.md "wikilink")[动物科学中的一个重要分支](../Page/动物科学.md "wikilink")，随着目前现代[农业的发展](../Page/农业.md "wikilink")，目前养猪生产正朝着[集约化和](../Page/集约化.md "wikilink")[机械化的方向发展](../Page/机械化.md "wikilink")。

## 用途

人類飼養家豬主要是為了可食用的[豬肉](../Page/豬肉.md "wikilink")。其他從豬而來的食品有[香腸](../Page/香腸.md "wikilink")、[火腿](../Page/火腿.md "wikilink")、[培根](../Page/培根.md "wikilink")、[煙肉](../Page/煙肉.md "wikilink")、等。像豬肝、[豬大腸](../Page/豬大腸.md "wikilink")、豬血等也會作為食物。不過[猶太教及](../Page/猶太教.md "wikilink")[伊斯蘭教認為豬對人來說是不潔的](../Page/伊斯蘭教.md "wikilink")(unclean
to you)，故禁食豬肉。

人類曾以作為食物，不過取得不易，因此沒有商業化的生產。[托斯卡纳的Porcorino](../Page/托斯卡纳.md "wikilink")[乳酪是用豬奶作的](../Page/乳酪.md "wikilink")。

除了供食用和作为宠物，猪在人类生命科学和生物科学中的特殊价值。在所有动物中，猪的生理、生化、解剖、代谢、骨骼发育、心血管系统、消化系统等与人类最为接近，因此是医学研究（例如[动物模型](../Page/动物模型.md "wikilink")）和人类[异种器官移植的最佳供体](../Page/异种器官移植.md "wikilink")，也是药理、药效等制药方面的理想[实验动物](../Page/实验动物.md "wikilink")，猪基因组学、蛋白质组学、猪基因操作等技术的发展\[9\]，已经为制备[人源化转基因猪和人类](../Page/人源化转基因猪.md "wikilink")[疾病模型提供了条件](../Page/疾病模型.md "wikilink")。尤其是
[小型猪](../Page/小型猪.md "wikilink")([Mini
Pig](../Page/Mini_Pig.md "wikilink"))，在形态学、解剖学、生理学、生物、生化、饮食结构、营养代谢、染色体结构、基因结构和[基因组序列](../Page/基因组序列.md "wikilink")\[10\]等方面，都与人类具有较高的相似性。其中中国农业科学院北京畜牧兽医研究所原研究员[冯书堂教授主持的](../Page/冯书堂.md "wikilink")[五指山小型猪近交系研究已经产生第二十五代](../Page/五指山小型猪近交系.md "wikilink")[近交系小型猪](../Page/近交系.md "wikilink")（F25），其[近交系数达到](../Page/近交系数.md "wikilink")0.99519，而且已经进入以生命科学和医学研究为主的产业化开发阶段\[11\]。

### 娛樂及文學中的豬

、[小猪宝贝](../Page/小猪宝贝.md "wikilink")、[粉紅豬小妹及](../Page/粉紅豬小妹.md "wikilink")都是電影或電視中豬的例子。像[三隻小豬](../Page/三隻小豬.md "wikilink")、[小熊維尼中的](../Page/小熊維尼.md "wikilink")、《[夏洛特的网](../Page/夏洛特的网.md "wikilink")》中的韋柏、《[西遊記](../Page/西遊記.md "wikilink")》中的[猪八戒](../Page/猪八戒.md "wikilink")、《[动物庄园](../Page/动物庄园.md "wikilink")》中的豬都是文學中著名豬的例子。

## 文化

### 猪的别名与汉字

[Piero_di_Cosimo_025.jpg](https://zh.wikipedia.org/wiki/File:Piero_di_Cosimo_025.jpg "fig:Piero_di_Cosimo_025.jpg")畫像，[皮埃洛·迪·科西莫](../Page/皮埃洛·迪·科西莫.md "wikilink")1480年的作品\]\]
[KCityParkPig.JPG](https://zh.wikipedia.org/wiki/File:KCityParkPig.JPG "fig:KCityParkPig.JPG")[九龍寨城公園十二生肖像之豬](../Page/九龍寨城公園.md "wikilink")\]\]

  - 最初表示猪的[汉字是](../Page/汉字.md "wikilink")[象形字](../Page/象形字.md "wikilink")“**豕**”（粵音：ci2（始）；[拼音](../Page/拼音.md "wikilink")：）。
  - “**-{豬}-**”字则是後来造的[形声字](../Page/形声字.md "wikilink")，从豕者聲。而中國大陸的[简化字将左边表示猪的](../Page/简化字.md "wikilink")“豕”旁改变为“犭”，作“-{猪}-”。
  - 象形字“**彘**”本义指野猪，下方的“[矢](../Page/矢.md "wikilink")”字和两边的符号表示[箭射入了野猪](../Page/箭.md "wikilink")。\[12\]
  - “**豚**”字在“豕”左边添加[肉月旁](../Page/肉部.md "wikilink")，本义指专门为屠杀吃肉而蓄养的小猪，[日語主要以此字稱呼家豬](../Page/日語.md "wikilink")，日語中的「豬」專指野豬。
  - 猪在中国家庭有着很重要的地位。从[金文到](../Page/金文.md "wikilink")[楷書的](../Page/楷書.md "wikilink")“**家**”字中，宝盖头的下面均为“**豕**”。由此一般意见认为在中国古代，无豬是不成家的。但有人也指岀，偏偏在[甲骨文的](../Page/甲骨文.md "wikilink")“家”字中，屋子下面的图像明確表示是一头公猪，并不是普通的“豕”字，而是「豖」字，肚皮下很清晰地添加了一个表示雄性[生殖器的符号](../Page/生殖器.md "wikilink")。
  - 《[尔雅](../Page/尔雅.md "wikilink")·释兽》记载有：“彘，豬也。其子曰豚。一歲曰豵。”，所以**彘**、**豬**、**豚**、**豵**都是指猪。

### 民俗學

逢年過節，到了豬年，民間有豬年吉祥話如下：
福相如豬、諸事順利、豬年吉祥、豬事大吉、諸事吉祥、豬入門，百福臻、金豬獻吉、金豬賀歲、金豬獻瑞、竹報平安，豬肥人富、春花百朵開，珠豬引金福、吉祥如豬、喜從豬來、豬豬平安、諸事如意、朱帨迎祥、豬年好運、金豬頌春、豬年旺旺來、豬年到，好運到。

### 比喻

  - 在汉语中，“猪”、“猪头”、“猪头炳”、“[猪头三](../Page/猪头三.md "wikilink")”有时用来比喻、指称一些想法或做法被认为较为愚蠢的人。
  - 在汉语中，“[小猪](../Page/小猪.md "wikilink")”“猪猪”“傻猪”有时用来称呼可爱、亲密的人。
  - 在漢語中，「豬」、「肥豬」常被用來指或身形[肥胖的人](../Page/肥胖.md "wikilink")。
  - 豬有時候代表好色，台灣流行用“[豬哥](../Page/豬哥.md "wikilink")”（台語意为“用于配种的公猪”）一語代表好色的男性，「母豬」指好色的女性。《[左傳](../Page/左傳.md "wikilink")》定公十四年，[衛夫人](../Page/衛國.md "wikilink")[南子與](../Page/南子.md "wikilink")[宋朝淫亂](../Page/宋朝_\(人物\).md "wikilink")，“野人歌之曰：‘既定爾婁豬，盍歸吾艾豭？’”《[太平廣記](../Page/太平廣記.md "wikilink")》卷二一六《張璟藏》條引《[朝野僉載](../Page/朝野僉載.md "wikilink")》云：“准相書：豬視者淫。”
  - 中國畫史上，有“豬不入畫”之俗\[13\]。1934年[徐悲鴻為豬年畫一幅豬](../Page/徐悲鴻.md "wikilink")。
  - [南齊](../Page/南齊.md "wikilink")[卞彬](../Page/卞彬.md "wikilink")《禽兽决录》曰：“羊性淫而狠，猪性卑而率，鵝性頑而傲，狗性险而出，皆指斥当时贵势。羊淫狠谓[吕文显](../Page/吕文显.md "wikilink")，猪卑率谓[朱隆之](../Page/朱隆之.md "wikilink")，鵝頑傲谓[潘敞](../Page/潘敞.md "wikilink")，狗险出谓[吕文庶也](../Page/吕文庶.md "wikilink")。”这也是成语[猪卑狗险的出处](../Page/猪卑狗险.md "wikilink")。

### 宗教中的豬

  - 在[古埃及](../Page/古埃及.md "wikilink")，豬會被人和太陽神[荷魯斯的對敵](../Page/荷魯斯.md "wikilink")[賽特聯想在一起](../Page/賽特.md "wikilink")。但當賽特在埃及人中不再受歡迎之後，養豬的人都被禁止進入廟宇。
  - 在[印度教](../Page/印度教.md "wikilink")，保護神[毗濕奴曾化身為](../Page/毗濕奴.md "wikilink")[野豬去拯救地球](../Page/野豬.md "wikilink")，打敗了潛入海底的[惡魔](../Page/惡魔.md "wikilink")。
  - 在[佛教](../Page/佛教.md "wikilink")、[道教](../Page/道教.md "wikilink")，星辰之神[摩利支天乘](../Page/摩利支天.md "wikilink")[豕或七豕所拉之車](../Page/豕.md "wikilink")。
  - 在[古希臘](../Page/古希臘.md "wikilink")，母豬適合作為[得墨忒耳的](../Page/得墨忒耳.md "wikilink")[祭品](../Page/犧牲.md "wikilink")，由於得墨忒爾是古代最大的女神，所以有祂自己喜愛的動物。而[厄琉息斯秘儀的開始都會獻祭一隻豬](../Page/厄琉息斯秘儀.md "wikilink")。
  - [猶太教和](../Page/猶太教.md "wikilink")[伊斯蘭教的教義中認為豬是](../Page/伊斯蘭教.md "wikilink")[不淨動物](../Page/不淨動物.md "wikilink")，因此禁止信徒食用任何的豬肉及其加工製品。[基督復臨安息日會和其他某些基督宗派也都認為豬肉是不淨食物](../Page/基督復臨安息日會.md "wikilink")。曾經發生台灣雇主強迫要求[印尼勞工吃](../Page/印尼.md "wikilink")[豬肉](../Page/豬肉.md "wikilink")，不吃則扣[薪水](../Page/薪水.md "wikilink")，僱主因而被判刑八個月，引起國際人權及伊斯蘭教團體的注意。而東亞地區的郵局在豬年時也會提醒民眾不可以郵寄貼有豬圖案的郵票或信件、賀卡到伊斯蘭教為主的國家，以免觸犯其忌諱。
  - 在[天主教](../Page/天主教.md "wikilink")，[東正教和其他天主教團體](../Page/東正教.md "wikilink")，會將豬和[聖安東尼聯想在一起](../Page/聖安東尼.md "wikilink")，因為聖安東尼是養豬的人的[主保聖人](../Page/主保聖人.md "wikilink")。
  - 《[聖經](../Page/聖經.md "wikilink")》[舊約](../Page/舊約.md "wikilink")「[利未記](../Page/利未記.md "wikilink")」第11章7節指出，“豬─因為蹄分兩瓣、卻不[倒嚼](../Page/倒嚼.md "wikilink")，就與你們不潔淨。”
  - 豬是[亞洲十二](../Page/亞洲.md "wikilink")[生肖中排名第十二](../Page/生肖.md "wikilink")，對應地支中的[亥](../Page/亥.md "wikilink")。
  - 扫墓时，除了会带上香烛、纸帛等祭品外，还会抬只[乳猪去祭祖](../Page/乳猪.md "wikilink")，[烧乳猪在](../Page/烧乳猪.md "wikilink")[广东已有超过二千年的历史](../Page/广东.md "wikilink")。在广东传统习俗中，烧猪或烧乳猪是各隆重场合的常用品。无论是新店开张、新剧开拍或是[清明](../Page/清明.md "wikilink")[祭祖](../Page/祭祖.md "wikilink")，都可常见抬出整只烧猪或烧乳猪作为祭祀用品。切烧猪有时更会是仪式的一部份，待仪式完成后，烧猪便会分给各参予者分享。清明分食乳猪等烧味食品，讲究的是“红皮赤壮”意头，寓意祖先保佑子孙身体强壮安健。

## 相關條目

  - [豬肉](../Page/豬肉.md "wikilink")
  - [豬腳](../Page/豬腳.md "wikilink")
  - [山豬](../Page/山豬.md "wikilink")
  - [紅豬](../Page/紅豬.md "wikilink")
  - [粉紅豬小妹](../Page/粉紅豬小妹.md "wikilink")
  - [烤乳豬](../Page/烤乳豬.md "wikilink")
  - [猪八戒](../Page/猪八戒.md "wikilink")
  - [三隻小豬](../Page/三隻小豬.md "wikilink")
  - [符合教規的食物 (猶太教)](../Page/符合教規的食物_\(猶太教\).md "wikilink")
  - [符合教規的食物 (伊斯蘭教)](../Page/符合教規的食物_\(伊斯蘭教\).md "wikilink")
  - [国家级畜禽遗传资源保护名录](../Page/国家级畜禽遗传资源保护名录.md "wikilink")
  - [飲食禁忌](../Page/飲食禁忌.md "wikilink")
  - [精耕细作](../Page/精耕细作.md "wikilink")
  - [粗放農業](../Page/粗放農業.md "wikilink")

## 参考文献

## 外部連結

  - [台灣畜產種原知識庫](http://agrkb.angrin.tlri.gov.tw/)

  - [中華民國養豬協會](http://www.swineroc.com.tw/index.aspx)

  - [《中国猪品种志》品种名录](https://web.archive.org/web/20160311194749/http://zzpig.com/article/showarticle.asp?articleid=51)

  - [British Pig Association](http://www.britishpigs.org.uk)

  - [Factory farming pigs - animal welfare
    site](http://www.goveg.com/factoryFarming_pigs.asp)

  - [The process of pig
    slaughtery](http://www.hyfoma.com/en/content/food-branches-processing-manufacturing/meat-fish-shrimps/pig-slaughtering/)

[domestica](../Category/豬.md "wikilink")
[DP](../Category/生肖.md "wikilink")
[\*](../Category/家豬.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [历史悠久的中国猪](http://songshuhui.net/archives/37482.html)
2.  *[Live
    Animals](http://faostat.fao.org/site/573/DesktopDefault.aspx?PageID=573#ancor)*.(2012,
    August 7). Provided October 5, 2012 from database FAOSTAT.
3.
4.
5.
6.
7.
8.
9.  <http://xuewen.cnki.net/CJFD-GWXK200712021.html>
10. *GigaScience* 2012, 1:16,
11. <http://www.caas.cn/ysxw/gnhz1/256256.shtml>
12. [“猪”字命名新探](http://wxs.swu.edu.cn/de/upimg/soft/30_120301234053.pdf)
13. 《幽夢影》引龔半千曰：“物之不可入画者，猪也，阿堵物也，恶少年也。”，見《[谈艺录](../Page/谈艺录.md "wikilink")》第三五九页