這是一個關於**[家貓品種的列表](../Page/貓.md "wikilink")**，列出被（The International Cat
Association）所認定註冊的[純種貓和](../Page/純種貓.md "wikilink")[雜交出的家貓品種](../Page/雜交.md "wikilink")\[1\]。

## 品種

<table>
<thead>
<tr class="header">
<th><p>品種</p></th>
<th><p>原產地</p></th>
<th><p>起源</p></th>
<th><p>體型</p></th>
<th><p>毛髮</p></th>
<th><p>顏色/花紋</p></th>
<th><p>圖片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿比西尼亞貓.md" title="wikilink">阿比西尼亞貓</a></p></td>
<td><p><a href="../Page/埃塞俄比亞.md" title="wikilink">埃塞俄比亞</a></p></td>
<td><p>天然</p></td>
<td><p>東方</p></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gustav_chocolate.jpg" title="fig:Gustav_chocolate.jpg">Gustav_chocolate.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛琴海貓.md" title="wikilink">愛琴海貓</a></p></td>
<td><p><a href="../Page/希臘.md" title="wikilink">希臘</a></p></td>
<td><p>天然</p></td>
<td><p>標準</p></td>
<td><p>半長</p></td>
<td><p>雙色或三色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aegean_cat.jpg" title="fig:Aegean_cat.jpg">Aegean_cat.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳大利亞.md" title="wikilink">澳大利亞</a></p></td>
<td><p>雜交</p></td>
<td><p>中等</p></td>
<td><p>短</p></td>
<td><p>虎斑</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Australian_Mist.jpg" title="fig:Australian_Mist.jpg">Australian_Mist.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td><p>突變</p></td>
<td><p>中等</p></td>
<td><p>長/短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Polydactylcat.jpg" title="fig:Polydactylcat.jpg">Polydactylcat.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td><p>Semi-Cobby</p></td>
<td><p>中</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TICO_(8).jpg" title="fig:TICO_(8).jpg">TICO_(8).jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美國反耳貓.md" title="wikilink">美國反耳貓</a></p></td>
<td><p>美國</p></td>
<td><p>突變</p></td>
<td><p>Cobby</p></td>
<td><p>長/短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:American_curl_2.jpg" title="fig:American_curl_2.jpg">American_curl_2.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美国短毛猫.md" title="wikilink">美国短毛猫</a></p></td>
<td><p>美國</p></td>
<td><p>天然</p></td>
<td><p>中等</p></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:American_Shorthair.jpg" title="fig:American_Shorthair.jpg">American_Shorthair.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>美國</p></td>
<td><p>突變<br />
（<a href="../Page/美國短毛貓.md" title="wikilink">美國短毛貓的突變種</a>）</p></td>
<td></td>
<td><p>卷毛</p></td>
<td><p>All but colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:American_Wirehair_-_CFF_cat_show_Heinola_2008-05-04_IMG_8721.JPG" title="fig:American_Wirehair_-_CFF_cat_show_Heinola_2008-05-04_IMG_8721.JPG">American_Wirehair_-_CFF_cat_show_Heinola_2008-05-04_IMG_8721.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a></p></td>
<td></td>
<td></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:BrownVarientAsianCat.JPG" title="fig:BrownVarientAsianCat.JPG">BrownVarientAsianCat.JPG</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>英國</p></td>
<td><p>雜交</p></td>
<td></td>
<td><p>半長</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tiffanie_at_cat_show.jpg" title="fig:Tiffanie_at_cat_show.jpg">Tiffanie_at_cat_show.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td><p>Oriental</p></td>
<td><p>長</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Oskar.jpg" title="fig:Oskar.jpg">Oskar.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>雜交<br />
（<a href="../Page/斯芬克斯貓.md" title="wikilink">斯芬克斯貓與</a><a href="../Page/曼切堪貓.md" title="wikilink">曼切堪貓的混種</a>）</p></td>
<td></td>
<td><p>hairless or with fuzzy down</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孟加拉貓.md" title="wikilink">孟加拉貓</a></p></td>
<td><p>美國</p></td>
<td><p>| 雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted/Marbled</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:BengalCat_Stella.jpg" title="fig:BengalCat_Stella.jpg">BengalCat_Stella.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>[2]</p></td>
<td><p><a href="../Page/缅甸.md" title="wikilink">缅甸</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>長</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Birman2.jpg" title="fig:Birman2.jpg">Birman2.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孟買貓.md" title="wikilink">孟買貓</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/緬甸貓.md" title="wikilink">緬甸貓與</a><a href="../Page/美國短毛貓.md" title="wikilink">美國短毛貓的混種</a>）</p></td>
<td><p>中等</p></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bombay_cat.jpg" title="fig:Bombay_cat.jpg">Bombay_cat.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gato_pelo_curto_brasileiro.JPG" title="fig:Gato_pelo_curto_brasileiro.JPG">Gato_pelo_curto_brasileiro.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英国短毛猫.md" title="wikilink">英国短毛猫</a></p></td>
<td><p>英國</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Britishblue.jpg" title="fig:Britishblue.jpg">Britishblue.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>英國</p></td>
<td></td>
<td></td>
<td><p>长</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:British_Longhair_-_Black_Silver_Shaded.jpg" title="fig:British_Longhair_-_Black_Silver_Shaded.jpg">British_Longhair_-_Black_Silver_Shaded.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/緬甸貓.md" title="wikilink">緬甸貓</a></p></td>
<td><p><a href="../Page/缅甸.md" title="wikilink">缅甸和</a><a href="../Page/泰国.md" title="wikilink">泰国</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Blissandlucky11.jpg" title="fig:Blissandlucky11.jpg">Blissandlucky11.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>英國</p></td>
<td><p>雜交<br />
（<a href="../Page/波斯貓.md" title="wikilink">波斯貓與</a><a href="../Page/緬甸貓.md" title="wikilink">緬甸貓的混種</a>）</p></td>
<td></td>
<td><p>短/长</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Male_Burmilla_cat.jpeg" title="fig:Male_Burmilla_cat.jpeg">Male_Burmilla_cat.jpeg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三色貓.md" title="wikilink">三色貓</a></p></td>
<td></td>
<td><p>天然</p></td>
<td></td>
<td><p>短/长</p></td>
<td><p>三色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Calico_cat_-_Phoebe.jpg" title="fig:Calico_cat_-_Phoebe.jpg">Calico_cat_-_Phoebe.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加州閃亮貓.md" title="wikilink">加州閃亮貓</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/阿比西尼亞貓.md" title="wikilink">阿比西尼亞貓</a>、<a href="../Page/美國短毛貓.md" title="wikilink">美國短毛貓與</a><a href="../Page/英國短毛貓.md" title="wikilink">英國短毛貓的混種</a>）</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Star_Spangled_Cat.jpg" title="fig:Star_Spangled_Cat.jpg">Star_Spangled_Cat.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（亞洲貓與<a href="../Page/緬甸貓.md" title="wikilink">緬甸貓的混種</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tiffany3.jpg" title="fig:Tiffany3.jpg">Tiffany3.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡特爾貓.md" title="wikilink">卡特爾貓</a></p></td>
<td><p><a href="../Page/法国.md" title="wikilink">法国</a></p></td>
<td><p>天然</p></td>
<td><p>Cobby</p></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Abbaye_fev2006_003.jpg" title="fig:Abbaye_fev2006_003.jpg">Abbaye_fev2006_003.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>[3]</p></td>
<td><p>法国</p></td>
<td><p>雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Ticked</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chausiecatexample.jpg" title="fig:Chausiecatexample.jpg">Chausiecatexample.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Cheetoh.md" title="wikilink">Cheetoh</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（孟加拉猫與<a href="../Page/欧西猫.md" title="wikilink">欧西猫的混种</a>）</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p>|</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重點色短毛貓.md" title="wikilink">重點色短毛貓</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>短</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Colorpoint_Shorthair.jpg" title="fig:Colorpoint_Shorthair.jpg">Colorpoint_Shorthair.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>[4]</p></td>
<td><p>英國</p></td>
<td><p>突變</p></td>
<td></td>
<td><p>卷毛</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:BebopsLilacPrince.JPG" title="fig:BebopsLilacPrince.JPG">BebopsLilacPrince.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/马恩岛.md" title="wikilink">马恩岛</a></p></td>
<td><p>天然/突變<br />
（<a href="../Page/馬恩島貓.md" title="wikilink">馬恩島貓的突變種</a>）</p></td>
<td></td>
<td><p>長</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cymric_-_Norwegian_forest_cat_presentation_show_Kotka_2009-02-01_IMG_0687.JPG" title="fig:Cymric_-_Norwegian_forest_cat_presentation_show_Kotka_2009-02-01_IMG_0687.JPG">Cymric_-_Norwegian_forest_cat_presentation_show_Kotka_2009-02-01_IMG_0687.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Cyprus_Shorthair.md" title="wikilink">Cyprus Shorthair</a></p></td>
<td><p><a href="../Page/塞浦路斯.md" title="wikilink">塞浦路斯</a></p></td>
<td><p>天然</p></td>
<td><p>Svelte</p></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:CyprusShorthair.jpg" title="fig:CyprusShorthair.jpg">CyprusShorthair.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>[5]</p></td>
<td><p>英國</p></td>
<td><p>突變</p></td>
<td><p>东方</p></td>
<td><p>卷毛</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Devon_Rex_Izzy.jpg" title="fig:Devon_Rex_Izzy.jpg">Devon_Rex_Izzy.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/短毛家貓.md" title="wikilink">短毛家貓</a></p></td>
<td></td>
<td><p>雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tabby-cat-domestic-shorthair-balthazar.jpg" title="fig:Tabby-cat-domestic-shorthair-balthazar.jpg">Tabby-cat-domestic-shorthair-balthazar.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/俄罗斯.md" title="wikilink">俄罗斯</a></p></td>
<td></td>
<td></td>
<td><p>無毛</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cat_don_sphinx.JPG" title="fig:Cat_don_sphinx.JPG">Cat_don_sphinx.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃及猫.md" title="wikilink">埃及猫</a></p></td>
<td><p>埃及</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>斑點</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Egy_mau.jpg" title="fig:Egy_mau.jpg">Egy_mau.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/瑞典.md" title="wikilink">瑞典</a>-<a href="../Page/意大利.md" title="wikilink">意大利</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:European_shorthair_procumbent_Quincy.jpg" title="fig:European_shorthair_procumbent_Quincy.jpg">European_shorthair_procumbent_Quincy.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/异国短毛猫.md" title="wikilink">异国短毛猫</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/美國短毛貓.md" title="wikilink">美國短毛貓與</a><a href="../Page/波斯貓.md" title="wikilink">波斯貓的混種</a>）</p></td>
<td><p>Cobby</p></td>
<td><p>短</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cream_tabby_exotic_cat.jpg" title="fig:Cream_tabby_exotic_cat.jpg">Cream_tabby_exotic_cat.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/德国.md" title="wikilink">德国</a></p></td>
<td><p>突變</p></td>
<td></td>
<td><p>卷毛</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:German_Rex_Emi.jpg" title="fig:German_Rex_Emi.jpg">German_Rex_Emi.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>英國</p></td>
<td></td>
<td></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p>|<a href="https://zh.wikipedia.org/wiki/File:Havana_Brown_-_choco.jpg" title="fig:Havana_Brown_-_choco.jpg">Havana_Brown_-_choco.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喜馬拉雅貓.md" title="wikilink">喜馬拉雅貓</a></p></td>
<td><p>英國</p></td>
<td><p>雜交<br />
（<a href="../Page/暹羅貓.md" title="wikilink">暹羅貓與</a><a href="../Page/波斯貓.md" title="wikilink">波斯貓的混種</a>）</p></td>
<td><p>Cobby</p></td>
<td><p>長</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chocolate_Himlayan.jpg" title="fig:Chocolate_Himlayan.jpg">Chocolate_Himlayan.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本短尾猫.md" title="wikilink">日本短尾猫</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
<td><p>天然</p></td>
<td><p>中等</p></td>
<td><p>短/长</p></td>
<td><p>All but colorpoint and ticked</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:JapaneseBobtailBlueEyedMi-ke.JPG" title="fig:JapaneseBobtailBlueEyedMi-ke.JPG">JapaneseBobtailBlueEyedMi-ke.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>雜交</p></td>
<td><p>东方</p></td>
<td><p>短/长</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Javanese_cat.jpg" title="fig:Javanese_cat.jpg">Javanese_cat.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>泰国</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>單色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Veda,chat-adulte-mâle-race-korat.JPG" title="fig:Veda,chat-adulte-mâle-race-korat.JPG">Veda,chat-adulte-mâle-race-korat.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>俄罗斯</p></td>
<td><p>天然</p></td>
<td><p>Semi-Cobby</p></td>
<td><p>短/长</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kurilian_bobtail.JPG" title="fig:Kurilian_bobtail.JPG">Kurilian_bobtail.JPG</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>美國</p></td>
<td><p>突變</p></td>
<td><p>中等</p></td>
<td><p>卷毛</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Laperm_LH_red_tabby.jpg" title="fig:Laperm_LH_red_tabby.jpg">Laperm_LH_red_tabby.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/緬因貓.md" title="wikilink">緬因貓</a></p></td>
<td><p>美國</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>長</p></td>
<td><p>All but colorpoint and ticked</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Maine_Coon_female.jpg" title="fig:Maine_Coon_female.jpg">Maine_Coon_female.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬恩島貓.md" title="wikilink">馬恩島貓</a></p></td>
<td><p><a href="../Page/马恩岛.md" title="wikilink">马恩岛</a></p></td>
<td><p>天然/突變</p></td>
<td></td>
<td><p>短/长</p></td>
<td><p>All but colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Japanese_Bobtail_looking_like_Manx.jpg" title="fig:Japanese_Bobtail_looking_like_Manx.jpg">Japanese_Bobtail_looking_like_Manx.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/明斯钦猫.md" title="wikilink">明斯钦猫</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/曼切堪貓.md" title="wikilink">曼切堪貓與</a><a href="../Page/斯芬克斯貓.md" title="wikilink">斯芬克斯貓的混種</a>）</p></td>
<td><p>Semi-Cobby</p></td>
<td><p>短/無毛</p></td>
<td><p>所有</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼切堪貓.md" title="wikilink">曼切堪貓</a></p></td>
<td><p>美國</p></td>
<td><p>突變</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Longhairedmunchkin.jpg" title="fig:Longhairedmunchkin.jpg">Longhairedmunchkin.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td></td>
<td></td>
<td><p>半長</p></td>
<td><p>Solid</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nebelung_Male,_Aleksandr_van_Song_de_Chine.JPG" title="fig:Nebelung_Male,_Aleksandr_van_Song_de_Chine.JPG">Nebelung_Male,_Aleksandr_van_Song_de_Chine.JPG</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>长或短</p></td>
<td><p>varied</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/挪威森林貓.md" title="wikilink">挪威森林貓</a></p></td>
<td><p><a href="../Page/挪威.md" title="wikilink">挪威</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>長</p></td>
<td><p>All but colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Norskskogkatt_Evita_3.JPG" title="fig:Norskskogkatt_Evita_3.JPG">Norskskogkatt_Evita_3.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐西貓.md" title="wikilink">歐西貓</a></p></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/阿比西尼亞貓.md" title="wikilink">阿比西尼亞貓與</a><a href="../Page/暹羅貓.md" title="wikilink">暹羅貓的混種</a>）</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ocicat-Charan.jpg" title="fig:Ocicat-Charan.jpg">Ocicat-Charan.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>美國</p></td>
<td><p>突變</p></td>
<td></td>
<td><p>卷毛</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>雙色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Oriental_shorthair_20070130_caroline.jpg" title="fig:Oriental_shorthair_20070130_caroline.jpg">Oriental_shorthair_20070130_caroline.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東方短毛貓.md" title="wikilink">東方短毛貓</a></p></td>
<td></td>
<td></td>
<td><p>东方</p></td>
<td><p>短</p></td>
<td><p>All but colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Oriental_Shorthair_Blue_Eyed_White_cat_(juvenile).jpg" title="fig:Oriental_Shorthair_Blue_Eyed_White_cat_(juvenile).jpg">Oriental_Shorthair_Blue_Eyed_White_cat_(juvenile).jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>东方</p></td>
<td><p>半長</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:OLH-GIP_Divan_Cesar.jpg" title="fig:OLH-GIP_Divan_Cesar.jpg">OLH-GIP_Divan_Cesar.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波斯貓.md" title="wikilink">波斯貓</a></p></td>
<td><p><a href="../Page/大伊朗.md" title="wikilink">大伊朗</a></p></td>
<td></td>
<td><p>Cobby</p></td>
<td><p>長</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Persialainen.jpg" title="fig:Persialainen.jpg">Persialainen.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>俄罗斯</p></td>
<td><p>雜交<br />
（顿斯科伊猫與<a href="../Page/東方短毛貓.md" title="wikilink">東方短毛貓的混種</a>）</p></td>
<td><p>Stocky</p></td>
<td><p>無毛</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tamila_the_lilac_tabby_Peterbald_cat.jpg" title="fig:Tamila_the_lilac_tabby_Peterbald_cat.jpg">Tamila_the_lilac_tabby_Peterbald_cat.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>美國</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Jarnac_Bepacific_feb07.jpg" title="fig:Jarnac_Bepacific_feb07.jpg">Jarnac_Bepacific_feb07.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td><p>Cobby</p></td>
<td><p>長</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ragamuffin.jpg" title="fig:Ragamuffin.jpg">Ragamuffin.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布偶貓.md" title="wikilink">布偶貓</a></p></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td><p>Cobby</p></td>
<td><p>長</p></td>
<td><p>Colorpoint/Mitted/Bicolor</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ragdoll_from_Gatil_Ragbelas.jpg" title="fig:Ragdoll_from_Gatil_Ragbelas.jpg">Ragdoll_from_Gatil_Ragbelas.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/俄羅斯藍貓.md" title="wikilink">俄羅斯藍貓</a></p></td>
<td><p>俄罗斯</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Solid</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Russian_Blue_001.gif" title="fig:Russian_Blue_001.gif">Russian_Blue_001.gif</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Russian_White,_Black_and_Tabby.md" title="wikilink">Russian White, Black and Tabby</a></p></td>
<td><p>澳大利亚</p></td>
<td><p>雜交</p></td>
<td></td>
<td><p>短</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇格蘭摺耳貓.md" title="wikilink">蘇格蘭摺耳貓</a></p></td>
<td><p><a href="../Page/蘇格蘭.md" title="wikilink">蘇格蘭</a></p></td>
<td><p>天然(基因缺陷)</p></td>
<td><p>Cobby</p></td>
<td><p>短/长</p></td>
<td><p>All</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Scottishfold.jpg" title="fig:Scottishfold.jpg">Scottishfold.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>[6]</p></td>
<td><p>美國</p></td>
<td><p>突變/雜交</p></td>
<td></td>
<td><p>Rex (短/長)</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Selkirk_Rex.jpg" title="fig:Selkirk_Rex.jpg">Selkirk_Rex.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>美國</p></td>
<td><p>雜交<br />
（<a href="../Page/孟加拉貓.md" title="wikilink">孟加拉貓與</a><a href="../Page/東方短毛貓.md" title="wikilink">東方短毛貓的混種</a>）</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Serengetimalecat.jpg" title="fig:Serengetimalecat.jpg">Serengetimalecat.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暹罗猫.md" title="wikilink">暹罗猫</a></p></td>
<td><p>泰国</p></td>
<td><p>天然</p></td>
<td><p>Oriental</p></td>
<td><p>短</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Siam_lilacpoint.jpg" title="fig:Siam_lilacpoint.jpg">Siam_lilacpoint.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西伯利亚猫.md" title="wikilink">西伯利亚猫</a></p></td>
<td><p>俄罗斯</p></td>
<td><p>天然</p></td>
<td><p>Semi-Cobby</p></td>
<td><p>半長</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Siberiancat.jpg" title="fig:Siberiancat.jpg">Siberiancat.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡貓.md" title="wikilink">新加坡貓</a></p></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Ticked</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Raffles_singapura_cat.jpg" title="fig:Raffles_singapura_cat.jpg">Raffles_singapura_cat.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雪鞋貓.md" title="wikilink">雪鞋貓</a></p></td>
<td><p>美國</p></td>
<td><p>雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Snowshoe_(cat).JPG" title="fig:Snowshoe_(cat).JPG">Snowshoe_(cat).JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/肯尼亚.md" title="wikilink">肯尼亚</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Classic tabby with ticking</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sokoke_dalili.jpg" title="fig:Sokoke_dalili.jpg">Sokoke_dalili.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/索馬利貓.md" title="wikilink">索馬利貓</a></p></td>
<td><p>美國</p></td>
<td><p>突變<br />
（<a href="../Page/阿比西尼亞貓.md" title="wikilink">阿比西尼亞貓的突變種</a>）</p></td>
<td></td>
<td><p>長</p></td>
<td><p>Ticked</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Blue_Somali_kitten_age_3_months.jpg" title="fig:Blue_Somali_kitten_age_3_months.jpg">Blue_Somali_kitten_age_3_months.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯芬克斯貓.md" title="wikilink">斯芬克斯貓</a></p></td>
<td><p>加拿大/美国</p></td>
<td><p>突變</p></td>
<td><p>Stocky</p></td>
<td><p>無毛</p></td>
<td><p>所有</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sphinx2_July_2006.jpg" title="fig:Sphinx2_July_2006.jpg">Sphinx2_July_2006.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Thai_(cat).md" title="wikilink">Thai</a>/ Old-style Siamese</p></td>
<td><p>泰国</p></td>
<td><p>天然</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Mimbi3.JPG" title="fig:Mimbi3.JPG">Mimbi3.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a></p></td>
<td><p>雜交<br />
（<a href="../Page/暹羅貓.md" title="wikilink">暹羅貓與</a><a href="../Page/緬甸貓.md" title="wikilink">緬甸貓的混種</a>）</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Colourpoint/Mink/Solid</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tonkinese.gif" title="fig:Tonkinese.gif">Tonkinese.gif</a></p></td>
</tr>
<tr class="even">
<td><p>[7]</p></td>
<td></td>
<td><p>雜交<br />
</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Toyger_-_Cornish_Rex_presentation_show_Riihimäki_2008-11-16_IMG_0101.JPG" title="fig:Toyger_-_Cornish_Rex_presentation_show_Riihimäki_2008-11-16_IMG_0101.JPG">Toyger_-_Cornish_Rex_presentation_show_Riihimäki_2008-11-16_IMG_0101.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/土耳其安哥拉貓.md" title="wikilink">土耳其安哥拉貓</a></p></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>半長</p></td>
<td><p>All but colorpoint</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Turkse_angora.jpg" title="fig:Turkse_angora.jpg">Turkse_angora.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/土耳其梵貓.md" title="wikilink">土耳其梵貓</a></p></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
<td><p>天然</p></td>
<td></td>
<td><p>半長</p></td>
<td><p>Van</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Turkish_Van_Example2.jpg" title="fig:Turkish_Van_Example2.jpg">Turkish_Van_Example2.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/乌克兰.md" title="wikilink">乌克兰</a></p></td>
<td></td>
<td></td>
<td><p>無毛</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ukrainian_Levkoy_cat.jpg" title="fig:Ukrainian_Levkoy_cat.jpg">Ukrainian_Levkoy_cat.jpg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>俄罗斯</p></td>
<td><p>天然雜交</p></td>
<td></td>
<td><p>短</p></td>
<td><p>Spotted</p></td>
<td><p>|-Ukunna Maine United States Natural Any speckeld</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 另見

  - [貓隻顏色](../Page/貓隻顏色.md "wikilink")
  - [犬種列表](../Page/犬種列表.md "wikilink")

## 參考

[\*](../Category/貓品種.md "wikilink")

1.  [貓迷擠爆TICA國際貓展一睹稀有品種貓　米克斯參賽爭美](http://www.ettoday.net/news/20140224/328043.htm)《ETtoday》2014年02月24日
2.  [伯曼貓
    BIRMAN](http://hd.stheadline.com/home/home_content.asp?contid=1804&srctype=p)
    《頭條日報》
3.  [我的野獸貓](http://hd.stheadline.com/culture/culture_content.asp?contid=800&srctype=g)《頭條日報》
4.  [康沃耳帝王貓 CORNISH
    REX](http://hd.stheadline.com/home/home_content.asp?contid=1801&srctype=p)
    《頭條日報》
5.  [德文帝王貓 DEVON
    REX](http://hd.stheadline.com/home/home_content.asp?contid=1801&srctype=p)
    《頭條日報》
6.  [Selkirk Rex
    披着羊毛的貓](http://orientaldaily.on.cc/cnt/lifestyle/20110324/00323_001.html)《東方日報》2011年3月24日
7.  [「玩具虎貓」抓傷女店員](http://orientaldaily.on.cc/cnt/news/20100709/00176_038.html)《東方日報》2010年7月9日