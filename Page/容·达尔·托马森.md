**容·达尔·托马森**（**Jon Dahl
Tomasson**，），一名已退役的[丹麥職業](../Page/丹麥.md "wikilink")[足球運動員](../Page/足球.md "wikilink")，司職[前鋒及](../Page/前鋒_\(足球\).md "wikilink")[攻擊中場](../Page/攻擊中場.md "wikilink")。現為[荷蘭球會](../Page/荷蘭.md "wikilink")[SBV精英的助教](../Page/SBV精英.md "wikilink")。他成名於荷蘭球會[飛燕諾](../Page/飛燕諾.md "wikilink")，2002年協助奪得[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")，翌年轉投意大利的[AC米蘭](../Page/AC米蘭.md "wikilink")，並且贏得[2003年歐洲聯賽冠軍盃冠軍](../Page/2002年-2003年歐洲聯賽冠軍盃.md "wikilink")。他亦是2002年及2004年[丹麥足球先生得主](../Page/丹麥足球先生.md "wikilink")。

湯馬臣目前為[丹麥國家足球隊上陣](../Page/丹麥國家足球隊.md "wikilink")112場國際賽射入52球，和二十世紀初名將尼爾遜（Poul
Nielsen）一同成為國家隊歷史上最多入球的球員。他曾代表丹麥出戰[2002年世界盃足球賽](../Page/2002年世界盃足球賽.md "wikilink")，並且取得四個入球。在[2010年世界盃足球賽中射入該屆賽事丹麥最後一個入球](../Page/2010年世界盃足球賽.md "wikilink")。另外亦出席過兩屆[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")。

## 球員生涯

### 早年時期

出生於[哥本哈根的湯馬臣初次接觸足球](../Page/哥本哈根.md "wikilink")，在於五歲時加入Solrød
BK少年隊，九歲時再轉到另一間較大的球會Køge
BK。湯馬臣直至16歲時才正式被獲得重用，初時他為球隊出戰低組別賽事及[丹麥乙組足球聯賽](../Page/丹麥乙組足球聯賽.md "wikilink")，1992年至1994年的三年間，Køge
BK由原本只是位列丹麥第五級別聯賽躍升上為丹麥第三級別聯賽，在這三年間湯馬臣一共上陣55場射入38球，寂寂無名的他因而獲得荷蘭的[海倫芬賞識](../Page/SC海倫芬.md "wikilink")，於1994年12月以18歲之齡加盟該支甲組聯賽球會。效力首季他為球隊上陣30場聯賽射入14球，翌年他再替[海倫芬射入](../Page/SC海倫芬.md "wikilink")18球，只在短短兩年他的表現非常優異，不但使湯馬臣首次獲得召入[丹麥國家足球隊](../Page/丹麥國家足球隊.md "wikilink")，亦吸引多間大球會的興趣，最終英超球會[紐卡素於](../Page/紐卡素.md "wikilink")1997年以二百五十萬英鎊成功購入湯馬臣。

### 加盟喜鵲

在[紐卡素時](../Page/紐卡素.md "wikilink")，湯馬臣由原本擅長所踢的攻擊中場被轉為前鋒球員，在首場聯賽主場迎戰[錫菲聯](../Page/錫菲聯.md "wikilink")，他迅即為球隊貢獻一個助攻，協助[紐卡素以二比一擊敗對方](../Page/紐卡素.md "wikilink")，可惜湯馬臣在後期發現體力所限加上未能成功融入球隊\[1\]\[2\]，只能在23場聯賽射入3個入球，而球隊在當屆聯賽取得第十六名，幾乎要降班收場，這時處於球員生涯最低潮的湯馬臣被迫令他落選丹麥在[1998年世界盃足球賽大軍名單](../Page/1998年世界盃足球賽.md "wikilink")。

### 重返荷蘭

經過一年不如意的英超生活後，1998年7月決定重返荷蘭，加盟當地勁旅[飛燕諾](../Page/飛燕諾.md "wikilink")。在效力首季他再度改踢攻擊中場之下，\[3\]成功地重拾效力[海倫芬的風采](../Page/SC海倫芬.md "wikilink")，不但為[飛燕諾重奪失落六年的](../Page/飛燕諾.md "wikilink")[荷蘭甲組足球聯賽冠軍](../Page/荷蘭甲組足球聯賽.md "wikilink")，及後更以三比二擊敗[阿積士第二度贏得](../Page/阿積士.md "wikilink")[荷蘭超級盃冠軍](../Page/荷蘭超級盃.md "wikilink")，湯馬臣因此獲被重召入[丹麥國家足球隊](../Page/丹麥國家足球隊.md "wikilink")，在[2000年歐洲國家盃外圍賽中的](../Page/2000年歐洲國家盃外圍賽.md "wikilink")7場比賽中射入6球，令他迅速地在國家隊建立一個重要的位置，最終丹麥在附加賽階段，兩回合以八比零擊敗以色列出線[2000年歐洲國家盃決賽週](../Page/2000年歐洲國家盃.md "wikilink")，其中湯馬臣八球中射入三球而成為出線功臣。在[2000年歐洲國家盃決賽週分組賽中](../Page/2000年歐洲國家盃.md "wikilink")，丹麥不幸地處於有「死亡之組」的D組（同組對手為實力相當不俗的法國、荷蘭及捷克），最後丹麥在一球不入兼三戰全敗完成賽事。

### 歐洲足協盃

2002年，是湯馬臣職業生涯最成功的一年。飛燕諾在歐聯分組賽以第三名而轉戰歐洲足協盃，湯馬臣在賽事中帶領球隊先後戰勝德國的[弗賴堡](../Page/弗賴堡體育俱樂部.md "wikilink")、蘇格蘭的[格拉斯哥流浪](../Page/格拉斯哥流浪.md "wikilink")、荷蘭的[PSV燕豪芬以及意大利的](../Page/PSV燕豪芬.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")，繼1970年後第二度晉級[歐洲足協盃決賽](../Page/歐洲足協盃.md "wikilink")。由於決賽地點早己決定於[飛燕諾的主場](../Page/飛燕諾.md "wikilink")—[飛燕諾球場](../Page/飛燕諾球場.md "wikilink")（Feijenoord
Stadion）舉行，因此令[飛燕諾以主場身份出戰](../Page/飛燕諾.md "wikilink")，決賽階段湯馬臣於五十分鐘建功為球隊將比數三比一，飛燕諾最終以三比二擊敗對手[多蒙特](../Page/多蒙特.md "wikilink")，湯馬臣則成為比賽的最佳球員。\[4\]2002年暑假，湯馬臣與飛燕諾的合約到期，隨著歐洲足協盃的成功得到意大利勁旅[AC米蘭以自由轉會邀請加盟](../Page/AC米蘭.md "wikilink")。同時湯馬臣獲[丹麥國家足球隊入選出席](../Page/丹麥國家足球隊.md "wikilink")[2002年世界盃足球賽](../Page/2002年世界盃足球賽.md "wikilink")，賽事中他更取得四個入球。

### AC米蘭

在效力首個球季，由於[AC米蘭擁有](../Page/AC米蘭.md "wikilink")[舍甫琴科及](../Page/安德烈·舍甫琴科.md "wikilink")[里瓦尔多在陣](../Page/里瓦尔多.md "wikilink")，托马森大多數淪為球隊後備，出場機會少之又少，但他在[歐洲聯賽冠軍盃取得三個入球](../Page/歐洲聯賽冠軍盃.md "wikilink")，更壓倒[尤文图斯而贏得賽事冠軍](../Page/尤文图斯.md "wikilink")。翌年[AC米蘭獲得](../Page/AC米蘭.md "wikilink")[意大利超級盃冠軍](../Page/意大利超級盃.md "wikilink")，[里瓦尔多的離隊令托马森獲得多番的上陣機會](../Page/里瓦尔多.md "wikilink")，並且射入12球協助球隊成功衛冕[意甲聯賽冠軍](../Page/意甲.md "wikilink")。隨後托马森代表丹麥出席[2004年歐洲國家盃](../Page/2004年歐洲國家盃.md "wikilink")，在3場分組賽比賽中貢獻3個入球，從而入選[賽事最佳陣容](../Page/2004年歐洲足球錦標賽.md "wikilink")。

2004年至05年球季，托马森雖然在[AC米蘭仍然未獲重用](../Page/AC米蘭.md "wikilink")，但表現總算理想，2005年[歐洲聯賽冠軍盃決賽迎戰](../Page/歐洲聯賽冠軍盃.md "wikilink")[利物浦](../Page/利物浦足球會.md "wikilink")，互射十二碼階段中他成功在第三輪射入，可惜[AC米蘭其中兩球十二碼被對方門將](../Page/AC米蘭.md "wikilink")[杜德克撲出](../Page/耶日·杜德克.md "wikilink")，終而失落歐冠杯冠軍。2005年7月，前意大利國家隊射手[维耶里由](../Page/克里斯蒂安·维耶里.md "wikilink")[國際米蘭轉投](../Page/國際米蘭.md "wikilink")[AC米蘭](../Page/AC米蘭.md "wikilink")，托马森自知難以繼續取得上陣，決定離開意大利加盟[德甲球會](../Page/德甲.md "wikilink")[史特加](../Page/史特加.md "wikilink")。

在[史特加](../Page/史特加.md "wikilink")，托马森能夠夥拍丹麥國家隊隊友[格伦夏尔](../Page/耶斯佩尔·格伦夏尔.md "wikilink")（Jesper
Grønkjær）迎戰2005年至06年德甲聯賽，但在該季他沒有為球隊帶來成功的球季，最終[史特加在聯賽只能取得第九名](../Page/史特加.md "wikilink")，即使托马森為求突破自己而奮力為球隊創造機會\[5\]。2006年暑假，[英冠球會](../Page/英冠.md "wikilink")[伯明翰有意收購托马森](../Page/伯明翰足球會.md "wikilink")，不過他沒有因表現欠佳而選擇離開[史特加](../Page/史特加.md "wikilink")。

### 登陸四大聯賽

2007年1月24日，湯馬臣獲被外借[西班牙甲組聯賽球會](../Page/西甲.md "wikilink")[維拉利爾](../Page/維拉利爾.md "wikilink")，以代替受傷多月的土耳其前鋒[尼赫治](../Page/尼赫治.md "wikilink")（Nihat
Kahveci），\[6\]他在面對[皇家馬德里時後備入替首度上陣](../Page/皇家馬德里.md "wikilink")\[7\]
，令他成為第五位先後效力過歐洲四大頂級聯賽（包括[西班牙甲組聯賽](../Page/西甲.md "wikilink")、[意大利甲組聯賽](../Page/意甲.md "wikilink")、[英格蘭超級聯賽及](../Page/英超.md "wikilink")[德國甲組聯賽](../Page/德甲.md "wikilink")）球會的球員，之前能夠達成該項紀錄的四名球員包括[樸比斯古](../Page/樸比斯古.md "wikilink")（Gheorghe
Popescu），[列度斯奧](../Page/列度斯奧.md "wikilink")（Florin
Răducioiu）、[-{zh-hans:沙维尔;zh-hk:沙維亞;zh-tw:沙維爾;}-](../Page/阿贝尔·沙维尔.md "wikilink")（Abel
Xavier）及[禾美](../Page/禾美.md "wikilink")（Pierre Wome）。\[8\]

## 職業數據

以下是湯馬臣至2009/10球季為止的職業數據\[9\]︰

<table>
<thead>
<tr class="header">
<th><p>賽季</p></th>
<th><p>球會</p></th>
<th><p>上陣</p></th>
<th><p>入球</p></th>
<th><p>聯賽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/BK科格.md" title="wikilink">BK科格</a></p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/BK科格.md" title="wikilink">BK科格</a></p></td>
<td><p>20</p></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/BK科格.md" title="wikilink">BK科格</a></p></td>
<td><p>33</p></td>
<td><p>27</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994/95</p></td>
<td><p><a href="../Page/SC海倫芬.md" title="wikilink">海倫芬</a></p></td>
<td><p>16</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>1995/96</p></td>
<td><p><a href="../Page/SC海倫芬.md" title="wikilink">海倫芬</a></p></td>
<td><p>30</p></td>
<td><p>14</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>1996/97</p></td>
<td><p><a href="../Page/SC海倫芬.md" title="wikilink">海倫芬</a></p></td>
<td><p>32</p></td>
<td><p>18</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>1997/98</p></td>
<td><p><a href="../Page/纽卡斯尔联足球俱乐部.md" title="wikilink">紐卡素</a></p></td>
<td><p>23</p></td>
<td><p>3</p></td>
<td><p><a href="../Page/英格蘭超級聯賽.md" title="wikilink">英格蘭超級聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>1998/99</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>33</p></td>
<td><p>13</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>1999/00</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>28</p></td>
<td><p>10</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2000/01</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>31</p></td>
<td><p>15</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2001/02</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>30</p></td>
<td><p>17</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2002/03</p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>20</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/意大利甲組聯賽.md" title="wikilink">意大利甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2003/04</p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>26</p></td>
<td><p>12</p></td>
<td><p><a href="../Page/意大利甲組聯賽.md" title="wikilink">意大利甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2004/05</p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>30</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/意大利甲組聯賽.md" title="wikilink">意大利甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2005/06</p></td>
<td><p><a href="../Page/史特加.md" title="wikilink">史特加</a></p></td>
<td><p>26</p></td>
<td><p>8</p></td>
<td><p><a href="../Page/德國甲組聯賽.md" title="wikilink">德國甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2006/07</p></td>
<td><p><a href="../Page/史特加.md" title="wikilink">史特加</a></p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p><a href="../Page/德國甲組聯賽.md" title="wikilink">德國甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2006/07</p></td>
<td><p><a href="../Page/比利亞雷阿爾足球俱樂部.md" title="wikilink">比利亞雷阿爾</a></p></td>
<td><p>11</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/西班牙甲組聯賽.md" title="wikilink">西班牙甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2007/08</p></td>
<td><p><a href="../Page/比利亞雷阿爾足球俱樂部.md" title="wikilink">比利亞雷阿爾</a></p></td>
<td><p>25</p></td>
<td><p>3</p></td>
<td><p><a href="../Page/西班牙甲組聯賽.md" title="wikilink">西班牙甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2008/09</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>13</p></td>
<td><p>9</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="even">
<td><p>2009/10</p></td>
<td><p><a href="../Page/飛燕諾.md" title="wikilink">飛燕諾</a></p></td>
<td><p>24</p></td>
<td><p>11</p></td>
<td><p><a href="../Page/荷蘭甲組聯賽.md" title="wikilink">荷蘭甲組聯賽</a></p></td>
</tr>
<tr class="odd">
<td><p>TOTAL</p></td>
<td><p>457</p></td>
<td><p>189</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 國際賽入球

  -
    *以下紀錄湯馬臣截至2010年6月24日的代表丹麥出戰國際賽時所射的入球。*

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>地點</p></th>
<th><p>對手</p></th>
<th><p>入球</p></th>
<th><p>比數</p></th>
<th><p>賽事性質</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1999年6月9日</p></td>
<td><p><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a><a href="../Page/利物浦.md" title="wikilink">利物浦</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-0</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1999年9月4日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>2-1</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1999年9月8日</p></td>
<td><p><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/拿玻里.md" title="wikilink">拿玻里</a></p></td>
<td></td>
<td><p>3-2</p></td>
<td><p>3-2</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>1999年11月13日</p></td>
<td><p><a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/特拉維夫.md" title="wikilink">特拉維夫</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>5-0</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>1999年11月13日</p></td>
<td><p>以色列特拉維夫</p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>5-0</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1999年11月17日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>3-0</p></td>
<td><p>2000年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2000年3月29日</p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/萊里亞.md" title="wikilink">萊里亞</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>1-2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2000年6月3日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2000年9月2日</p></td>
<td><p><a href="../Page/冰島.md" title="wikilink">冰島</a><a href="../Page/雷克亞維克.md" title="wikilink">雷克亞維克</a></p></td>
<td></td>
<td><p>1-1</p></td>
<td><p>2-1</p></td>
<td><p>2002年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2001年5月25日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>3-0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2001年6月2日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>2-1</p></td>
<td><p>2002年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2001年9月5日</p></td>
<td><p><a href="../Page/保加利亞.md" title="wikilink">保加利亞</a><a href="../Page/索菲亞.md" title="wikilink">索菲亞</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-0</p></td>
<td><p>2002年世界盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2001年9月5日</p></td>
<td><p>保加利亞索菲亞</p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-0</p></td>
<td><p>2002年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2002年4月17日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>3-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2002年5月17日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>2002年6月1日</p></td>
<td><p><a href="../Page/南韓.md" title="wikilink">南韓</a><a href="../Page/蔚山.md" title="wikilink">蔚山</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-1</p></td>
<td><p><a href="../Page/2002年世界盃.md" title="wikilink">2002年世界盃</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>2002年6月1日</p></td>
<td><p>南韓蔚山</p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-1</p></td>
<td><p>2002年世界盃</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>2002年6月6日</p></td>
<td><p>南韓<a href="../Page/大邱.md" title="wikilink">大邱</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>1-1</p></td>
<td><p>2002年世界盃</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>2002年6月11日</p></td>
<td><p>南韓<a href="../Page/仁川.md" title="wikilink">仁川</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-0</p></td>
<td><p>2002年世界盃</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>2002年9月7日</p></td>
<td><p><a href="../Page/挪威.md" title="wikilink">挪威</a><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>2004年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>2002年9月7日</p></td>
<td><p>挪威奧斯陸</p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>2-2</p></td>
<td><p>2004年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>2002年10月12日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-0</p></td>
<td><p>2004年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>2002年11月20日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>2003年2月12日</p></td>
<td><p><a href="../Page/埃及.md" title="wikilink">埃及</a><a href="../Page/開羅.md" title="wikilink">開羅</a></p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>4-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>2003年3月29日</p></td>
<td><p><a href="../Page/羅馬尼亞.md" title="wikilink">羅馬尼亞</a><a href="../Page/布加勒斯特.md" title="wikilink">布加勒斯特</a></p></td>
<td></td>
<td><p>3-2</p></td>
<td><p>5-2</p></td>
<td><p>2004年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>2003年9月10日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>2004年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>2003年11月16日</p></td>
<td><p><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a><a href="../Page/曼徹斯特.md" title="wikilink">曼徹斯特</a></p></td>
<td></td>
<td><p>3-2</p></td>
<td><p>3-2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>2004年5月30日</p></td>
<td><p><a href="../Page/愛沙尼亞.md" title="wikilink">愛沙尼亞</a><a href="../Page/塔林.md" title="wikilink">塔林</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>2004年6月18日</p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/布拉加.md" title="wikilink">布拉加</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-0</p></td>
<td><p><a href="../Page/2004年歐洲國家盃.md" title="wikilink">2004年歐洲國家盃</a></p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>2004年6月22日</p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/波圖.md" title="wikilink">波圖</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>2004年歐洲國家盃</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p>2004年6月22日</p></td>
<td><p>葡萄牙波圖</p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>2-2</p></td>
<td><p>2004年歐洲國家盃</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>2004年10月9日</p></td>
<td><p><a href="../Page/阿爾巴尼亞.md" title="wikilink">阿爾巴尼亞</a><a href="../Page/地拉那.md" title="wikilink">地拉那</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-0</p></td>
<td><p><a href="../Page/2006年世界盃足球賽外圍賽-歐洲區.md" title="wikilink">2006年世界盃外圍賽</a></p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p>2004年10月13日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>1-1</p></td>
<td><p>2006年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p>2004年11月17日</p></td>
<td><p><a href="../Page/格魯吉亞.md" title="wikilink">格魯吉亞</a><a href="../Page/第比利斯.md" title="wikilink">第比利斯</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-2</p></td>
<td><p>2006年世界盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p>2004年11月17日</p></td>
<td><p>格魯吉亞第比利斯</p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>2-2</p></td>
<td><p>2006年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p>2005年8月17日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>4-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p>2005年9月7日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>4-1</p></td>
<td><p>6-1</p></td>
<td><p><a href="../Page/2006年世界盃足球賽外圍賽-歐洲區.md" title="wikilink">2006年世界盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p>2005年10月12日</p></td>
<td><p><a href="../Page/哈薩克.md" title="wikilink">哈薩克</a><a href="../Page/阿拉木圖.md" title="wikilink">阿拉木圖</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-1</p></td>
<td><p>2006年世界盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>2006年5月27日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/奧胡斯.md" title="wikilink">奧胡斯</a></p></td>
<td></td>
<td><p>1-1</p></td>
<td><p>1-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>2006年9月1日</p></td>
<td><p>丹麥<a href="../Page/邦比.md" title="wikilink">邦比</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>4-2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p>2006年9月6日</p></td>
<td><p><a href="../Page/冰島.md" title="wikilink">冰島</a><a href="../Page/雷克亞維克.md" title="wikilink">雷克亞維克</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>2-0</p></td>
<td><p><a href="../Page/2008年歐洲國家盃外圍賽.md" title="wikilink">2008年歐洲國家盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p>2006年10月11日</p></td>
<td><p><a href="../Page/列支敦斯登.md" title="wikilink">列支敦斯登</a><a href="../Page/瓦都茲.md" title="wikilink">瓦都茲</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>4-0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p>2006年10月11日</p></td>
<td><p>列支敦斯登瓦都茲</p></td>
<td></td>
<td><p>4-0</p></td>
<td><p>4-0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p>2007年2月6日</p></td>
<td><p><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>3-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td><p>2007年2月6日</p></td>
<td><p>英格蘭倫敦</p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>3-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td><p>2007年6月2日</p></td>
<td><p><a href="../Page/丹麥.md" title="wikilink">丹麥</a><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-3</p></td>
<td><p><a href="../Page/2008年歐洲國家盃外圍賽球證遇襲事件.md" title="wikilink">賽事中斷</a></p></td>
<td><p><a href="../Page/2008年歐洲國家盃外圍賽.md" title="wikilink">2008年歐洲國家盃外圍賽</a></p></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p>2007年9月12日</p></td>
<td><p>丹麥<a href="../Page/奧胡斯.md" title="wikilink">奧胡斯</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>4-0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p>2007年10月13日</p></td>
<td><p>丹麥<a href="../Page/奧胡斯.md" title="wikilink">奧胡斯</a></p></td>
<td></td>
<td><p>1-2</p></td>
<td><p>1-3</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p>2007年10月17日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>3-1</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td><p>2007年11月21日</p></td>
<td><p>丹麥<a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>3-0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>51</p></td>
<td><p>2007年2月6日</p></td>
<td><p><a href="../Page/斯洛文尼亞.md" title="wikilink">斯洛文尼亞</a><a href="../Page/采列.md" title="wikilink">采列</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>52</p></td>
<td><p>2010年6月24日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/羅斯頓堡.md" title="wikilink">羅斯頓堡</a></p></td>
<td></td>
<td><p>1-2</p></td>
<td><p>1-3</p></td>
<td><p><a href="../Page/2010年世界盃.md" title="wikilink">2010年世界盃</a></p></td>
</tr>
</tbody>
</table>

## 榮譽

本土賽事

  - 1998年至99年
    **[荷甲聯賽冠軍](../Page/荷甲.md "wikilink")**([飛燕諾時代](../Page/飛燕諾.md "wikilink"))
  - 1998年至99年
    **[荷蘭超級盃](../Page/荷蘭超級盃.md "wikilink")**([飛燕諾時代](../Page/飛燕諾.md "wikilink"))
  - 2002年至03年
    **[意大利杯](../Page/意大利杯.md "wikilink")**([AC米蘭時代](../Page/AC米蘭.md "wikilink"))
  - 2003年至04年
    **[意甲聯賽冠軍](../Page/意甲.md "wikilink")**([AC米蘭時代](../Page/AC米蘭.md "wikilink"))
  - 2003年至04年
    **[意大利超級杯](../Page/意大利超級杯.md "wikilink")**([AC米蘭時代](../Page/AC米蘭.md "wikilink"))
  - 2006年至07年
    **[德甲聯賽冠軍](../Page/德甲.md "wikilink")**([史特加時代](../Page/史特加.md "wikilink"))

歐洲賽

  - 2001年至02年
    **[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")**([飛燕諾時代](../Page/飛燕諾.md "wikilink"))
  - 2002年至03年
    **[歐洲聯賽冠軍杯](../Page/歐洲聯賽冠軍杯.md "wikilink")**([AC米蘭時代](../Page/AC米蘭.md "wikilink"))

個人獎項

  - 1994年**[丹麥最佳年青球員](../Page/丹麥最佳年青球員.md "wikilink")**(19歲以下)\[10\]
  - 1996年**[荷蘭最佳年青球員](../Page/荷蘭最佳年青球員.md "wikilink")(告魯夫獎)**(21歲以下)\[11\]
  - 2002年及2004年[丹麥足球先生](../Page/丹麥足球先生.md "wikilink")\[12\]
  - [2002年世界盃銅靴獎](../Page/2002年世界盃.md "wikilink")(4球)
  - [2004年歐洲國家盃最佳陣容](../Page/2004年歐洲國家盃.md "wikilink")\[13\]

## 資料來源

<div class="references-small">

<references/>

</div>

## 外部連結

  - [丹麥國家隊有關資料](http://www.dbu.dk/landshold/landsholdsdatabasen/LBasePlayerInfo.aspx?playerid=1571)
  - [史特加有關資料](http://www.vfb.de/teams/profil_frame.php?sp_id=155&langcok=deu)
  - [德國生涯有關資料](http://www.fussballdaten.de/spieler/tomassonjondahl/)

[Category:丹麥足球運動員](../Category/丹麥足球運動員.md "wikilink")
[Category:丹麥國家足球隊球員](../Category/丹麥國家足球隊球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:海倫芬球員](../Category/海倫芬球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:史特加球員](../Category/史特加球員.md "wikilink")
[Category:維拉利爾球員](../Category/維拉利爾球員.md "wikilink")
[Category:丹超球員](../Category/丹超球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")

1.  [WORLD CUP | Squad | Jon Dahl
    Tomasson](http://news.bbc.co.uk/sport3/worldcup2002/hi/team_pages/denmark/squad/newsid_1779000/1779851.stm),
    *[BBC Sport](../Page/BBC_Sport.md "wikilink")*, April 8, 2002

2.  Kristine Wilkens, "Jon Dahl tilbage på vant plads", *[Berlingske
    Tidende](../Page/Berlingske_Tidende.md "wikilink")*, June 20, 1998

3.
4.  [Feyenoord boost
    Dutch](http://news.bbc.co.uk/sport2/hi/football/uefa_cup/1976438.stm),
    *[BBC體育](../Page/BBC.md "wikilink")*, 2002年5月8日

5.  [Tomasson hands Trapattoni a
    lifeline](http://soccernet.espn.go.com/news/story?id=346507&cc=3888),
    *[Soccernet](../Page/Soccernet.md "wikilink")*, 2005年10月21日

6.

7.

8.  [Striker Tomasson heading to Villareal on
    loan（湯馬臣外借至維拉利爾）](http://soccernet.espn.go.com/news/story?id=404501&cc=3888),
    *[Soccernet](../Page/Soccernet.md "wikilink")*, January 24, 2007

9.  [Voetbal International - Jon Dahl Tomasson
    profile](http://www.vi.nl/Spelers/Speler/Jon-Dahl-Tomasson.htm),
    accessed on 3 Aug 2010

10. [DBU 丹麥足協 年度最佳年青球員](http://www.dbu.dk/page.aspx?id=318)

11. [荷蘭最佳球員獎](http://www.rsssf.com/miscellaneous/nedpoy.html)

12. [丹麥球員協會 年度最佳球員](http://www.spillerforeningen.dk/viewpage.jsp?id=227)


13. [2004年歐洲國家盃
    最佳陣容](https://web.archive.org/web/20040707012523/http://www.euro2004.com/News/Kind%3D1/newsId%3D205742.html)