**容城县**是[河北省](../Page/河北省.md "wikilink")[保定市下辖的一个县](../Page/保定.md "wikilink")。县政府驻[容城镇古城路](../Page/容城镇.md "wikilink")46号。

## 历史沿革

[汉置](../Page/汉朝.md "wikilink")**范阳县**，属[涿郡](../Page/涿郡.md "wikilink")；[曹魏改属](../Page/曹魏.md "wikilink")[范阳郡](../Page/范阳郡.md "wikilink")；[隋更名为](../Page/隋朝.md "wikilink")**遒县**\[1\]；[唐](../Page/唐朝.md "wikilink")“[圣历二年](../Page/圣历.md "wikilink")，[契丹入寇](../Page/契丹.md "wikilink")，固守得全，因改名**全忠县**”\[2\]。[天宝元年改为](../Page/天宝_\(唐朝\).md "wikilink")**容城县**，属[易州](../Page/易州.md "wikilink")。[宋](../Page/宋朝.md "wikilink")[辽之际为边境](../Page/辽朝.md "wikilink")，北部属辽，侨治于新城县，南部属宋，属[雄州](../Page/雄州_\(河北\).md "wikilink")。[金合并南北二县](../Page/金朝.md "wikilink")，仍属雄州；[元因之](../Page/元朝.md "wikilink")；[明](../Page/明朝.md "wikilink")[清皆属](../Page/清朝.md "wikilink")[保定府](../Page/保定府.md "wikilink")；现属[保定市](../Page/保定市.md "wikilink")。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 注释

<references/>

## 参看

  - [雄安新区](../Page/雄安新区.md "wikilink")（[雄县](../Page/雄县.md "wikilink")、[安新县](../Page/安新县.md "wikilink")）
  - [白洋淀站](../Page/白洋淀站.md "wikilink")

## 官方网站

  - [容城门户](http://www.071700.net/)
  - [容城网](https://web.archive.org/web/20120825231609/http://www.rongchengwang.com/)
  - [容城论坛](https://web.archive.org/web/20120825222939/http://www.rongchengwang.com/club)

[容城县](../Category/容城县.md "wikilink")
[县](../Category/保定区县市.md "wikilink")
[保定](../Category/河北省县份.md "wikilink")

1.  隋书 地理志
2.  旧唐书 地理志