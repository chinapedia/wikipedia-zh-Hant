**宾格**（,
，又称：直接受格）表示一个[动词直接宾语的](../Page/动词.md "wikilink")[名词或一个](../Page/名词.md "wikilink")[前置词的](../Page/前置词.md "wikilink")[宾语](../Page/宾语.md "wikilink")。

几乎所有的语言都有宾格，但通过名词[变格表达宾格的语言比较少](../Page/变格.md "wikilink")，其中包括[拉丁语](../Page/拉丁语.md "wikilink")、[希腊语](../Page/希腊语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[俄语](../Page/俄语.md "wikilink")、[芬兰语](../Page/芬兰语.md "wikilink")、[维吾尔语](../Page/维吾尔语.md "wikilink")、[蒙古语](../Page/蒙古语.md "wikilink")、[满语](../Page/满语.md "wikilink")、[土耳其语](../Page/土耳其语.md "wikilink")、[冰岛语](../Page/冰岛语.md "wikilink")、古典[阿拉伯语](../Page/阿拉伯语.md "wikilink")、[波兰语](../Page/波兰语.md "wikilink")、[匈牙利语](../Page/匈牙利语.md "wikilink")、[捷克语](../Page/捷克语.md "wikilink")、[斯洛伐克语](../Page/斯洛伐克语.md "wikilink")、[克罗地亚语](../Page/克罗地亚语.md "wikilink")、[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")、[阿尔巴尼亚语](../Page/阿尔巴尼亚语.md "wikilink")、[羌语](../Page/羌语.md "wikilink")、[锡伯语](../Page/锡伯语.md "wikilink")、[鄂伦春语](../Page/鄂伦春语.md "wikilink")、[赫哲语](../Page/赫哲语.md "wikilink")、[韩语和](../Page/韩语.md "wikilink")[藏语](../Page/藏语.md "wikilink")（通过变化[施动者为](../Page/施动者.md "wikilink")[具格而间接体现宾格](../Page/具格.md "wikilink")）等等。[世界语也用名词变格表达宾格](../Page/世界语.md "wikilink")。

不通过变格体现宾格的语言，则会使用[虚词和](../Page/虚词.md "wikilink")[语序来表达这个意义](../Page/语序.md "wikilink")。例如：[漢語](../Page/漢語.md "wikilink"),
[英语](../Page/英语.md "wikilink")、[荷兰语](../Page/荷兰语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[意大利语](../Page/意大利语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[泰语](../Page/泰语.md "wikilink")、[越南语等等](../Page/越南语.md "wikilink")。

例如，名词词尾变化较少的[英语](../Page/英语.md "wikilink")，在少数一些[代词中存在宾格](../Page/代词.md "wikilink")（例如**whom**是**who**的宾格形式；**him**是**he**的宾格形式）。但是在英文的名词中，主格和宾格形式完全一致，没有任何变化，因此必须通过“SVO”的结构，区分主语和宾语。德语或拉丁语就不需要这样。

舉例來說，「The hunter kills the tiger.（猎人杀死老虎。）」與「The tiger kills the
hunter.（老虎杀死猎人。）」此二句中，主语及宾语完全是由语序的正反区别的（和汉语一致），因此这两句的主宾相反是因为语序相反造成的。

以德語舉例，「Der Jäger tötet den Tiger.」與「Der Tiger tötet den
Jäger.」（語意同上）此二句中，主语及宾语是由名词的变格（在此二句中体现在[冠词上](../Page/冠词.md "wikilink")）区别的，因此这两句的主宾相反不是因为语序相反造成的，而是名词的格位不同造成的。因为在德语中，將这两句话颠倒，意思也完全不变（和汉语相反）：「Den
Tiger tötet der Jäger.」與「Den Jäger tötet der Tiger.」这两句话的意思分别和前述两句话意义相同。

在英語裡，who / whom和he / him在不仅仅是主格和宾格的关系，也是主格和与格的关系（例如：「I gave him the
present.」）；在[古英语中有很明显的区分](../Page/古英语.md "wikilink")，**him**是与格，**hine**是宾格，这个双重性使得很多學習英语的学生没有意识到英语中宾格和与格的不同。

## 参看

  - [主格](../Page/主格.md "wikilink")
  - [与格](../Page/与格.md "wikilink")
  - [离格](../Page/离格.md "wikilink")
  - [呼格](../Page/呼格.md "wikilink")
  - [属格](../Page/属格.md "wikilink")
  - [主动格](../Page/主动格.md "wikilink")

## 外部連結

  - Russian Accusative:
    [1](http://www.russianlessons.net/grammar/nouns_accusative.php),
    [2](https://web.archive.org/web/20100326044807/http://www.russian-plus.com/Cases/accusative-case.html),
    [3](http://www.russian-resources.info/links.aspx/grammar/nouns/acc),
    [4](https://web.archive.org/web/20160304063335/http://www.learnrussian.net/learn-russian-accusative-case.html)
  - [German Accusative
    Case](http://www.deutsched.com/Grammar/Lessons/0203accusative.php)
    Grammar lesson covering the accusative case in the German language
  - [Arabic case endings](http://arabic.tripod.com/CaseSigns.htm)

[Category:语法格](../Category/语法格.md "wikilink")