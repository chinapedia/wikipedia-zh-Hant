[Dresden-Guentzstrassel.jpg](https://zh.wikipedia.org/wiki/File:Dresden-Guentzstrassel.jpg "fig:Dresden-Guentzstrassel.jpg")的高級職員宿舍\]\]
[Lighthouse_Wien_2.jpg](https://zh.wikipedia.org/wiki/File:Lighthouse_Wien_2.jpg "fig:Lighthouse_Wien_2.jpg")
[Montague_Burton_Residences_Kitchen.jpg](https://zh.wikipedia.org/wiki/File:Montague_Burton_Residences_Kitchen.jpg "fig:Montague_Burton_Residences_Kitchen.jpg")一間宿舍的公用廚房\]\]
**宿舍**，是一种供[学生](../Page/学生.md "wikilink")、[职工或](../Page/职工.md "wikilink")[组织成员居住的](../Page/组织_\(社会学\).md "wikilink")[房屋](../Page/房屋.md "wikilink")，居住人數從單人多至十几人不等。宿舍的設備從只有基本的床舖及共用的衛浴設備，到有[廚房可供住宿者自行煮食](../Page/廚房.md "wikilink")、有[電視機及運動器材不等](../Page/電視機.md "wikilink")。不過，[高級職員的宿舍又與](../Page/高級職員.md "wikilink")[豪宅無異](../Page/豪宅.md "wikilink")。前者是寄宿學生或職員的福利，宿生間是同輩職工，非[親屬](../Page/親屬.md "wikilink")[父母等關係](../Page/父母.md "wikilink")，宿舍主持或管理人員稱為「舍監」。
而高級職員的宿舍，容許[企業管理層及其](../Page/企業.md "wikilink")[家庭成員同住](../Page/家庭.md "wikilink")，甚至有私家[游泳池等設施](../Page/游泳池.md "wikilink")，[生活及](../Page/生活.md "wikilink")[消費全包](../Page/消費.md "wikilink")，視乎[僱傭合約條款](../Page/勞務合約.md "wikilink")、[勞動法等而定](../Page/勞動法.md "wikilink")。

平常的宿舍有守則，傳統宿舍受到[男女授受不親等保守觀念影響](../Page/男女授受不親.md "wikilink")，通常[男生](../Page/男生.md "wikilink")、[女生](../Page/女生.md "wikilink")，大多數宿舍為男女分棟或同棟不同樓層，有些宿舍為男女同宿，舍員要参與自治。甚至如[大學生](../Page/大學生.md "wikilink")，以自己居住的宿舍名稱為榮，不時舉行各種舍際[競賽](../Page/競賽.md "wikilink")。

相對於[建築宿舍供宿生居住](../Page/建築.md "wikilink")，[大學或者](../Page/大學.md "wikilink")[組織可以提供津貼](../Page/组织_\(社会学\).md "wikilink")，協助有居住需求的成員，在私人[房地產](../Page/房地產.md "wikilink")[市場](../Page/市場.md "wikilink")[租屋住](../Page/出租.md "wikilink")。在[資本](../Page/資本.md "wikilink")[投資及快速反應上](../Page/投資.md "wikilink")，住宿津貼比較宿舍供應更加靈活。另外也有與校外建商或屋主合作，承租或價購房屋再供給宿生居住，以降低自行興建所需費用及土地空間。

## 類型

### 單性別宿舍

在[大學宿舍中](../Page/大學.md "wikilink")，宿生的室友通常皆為同一[性別](../Page/性別.md "wikilink")，有些大學的宿舍是男女同棟分層，有些是男女分別居於不同棟。有些院校規定任何時間均不得帶異性回宿舍，有些則規定異性可於訪客時間內進入宿舍，但不得留宿。

[中國大陸除了大學有提供宿舍外](../Page/中國大陸.md "wikilink")，不少中、小學均有為居住於距離學校較遠的學生提供宿舍，一般為多人共用一房間，因此每名學生可以有多至十名室友。

[日本與](../Page/日本.md "wikilink")[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國大陸不同](../Page/中國大陸.md "wikilink")，學院宿生多住單人房，與室友同住的較少。

[香港不少大學宿舍均實行男女同棟分層制](../Page/香港.md "wikilink")，一般不禁止異性訪客，但一般規定異性不得留宿。

### 混合性別宿舍

[美国大学的住宿方式是多种多样](../Page/美国.md "wikilink")。除了单性别、男女同宿、单人间外，还在男女双方都同意的方式下，允许双方在同屋居住。

## 另見

  - [物業管理](../Page/物業管理.md "wikilink")
  - [保安](../Page/保安.md "wikilink")
  - [光華寮](../Page/光華寮.md "wikilink")
  - [順利紀律部隊宿舍](../Page/順利紀律部隊宿舍.md "wikilink")

## 外部連結

  - [中途宿舍](https://web.archive.org/web/20070927190247/http://www.swd.gov.hk/tc/index/site_pubsvc/page_rehab/sub_listofserv/id_halfwayhou/)

[Category:住宅](../Category/住宅.md "wikilink")
[Category:福利](../Category/福利.md "wikilink")