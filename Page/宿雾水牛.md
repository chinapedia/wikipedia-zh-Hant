**宿雾水牛**（[学名](../Page/学名.md "wikilink")：**）是在[菲律賓發現的](../Page/菲律賓.md "wikilink")[化石](../Page/化石.md "wikilink")[倭水牛](../Page/倭水牛.md "wikilink")，於2006年首次被描述。

## 解剖及形態

宿雾水牛最大的特徵是其細小的體型。其他同期的[水牛肩高約](../Page/水牛.md "wikilink")6呎及重2000磅，但宿雾水牛只有2.5呎高及重350磅，比其他的[倭水牛](../Page/倭水牛.md "wikilink")（如[民都洛水牛](../Page/民都洛水牛.md "wikilink")）都要小。\[1\]

該[化石標本估計屬於](../Page/化石.md "wikilink")[更新世或](../Page/更新世.md "wikilink")[全新世](../Page/全新世.md "wikilink")。\[2\]

## 演化歷史

宿雾水牛的[化石是在](../Page/化石.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")[宿霧開掘隧道時發現](../Page/宿霧.md "wikilink")。\[3\]該化石已捐贈與[美國的](../Page/美國.md "wikilink")[菲爾德自然歷史博物館](../Page/菲爾德自然歷史博物館.md "wikilink")，但卻接近50年的時間沒有被進一步分析。

## 參考

## 外部連結

  - [1](http://www.terradaily.com/reports/New_Dwarf_Buffalo_Discovered_By_Chance_In_The_Philippines_999.html)

[cebuensis](../Category/水牛屬.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")
[Category:更新世哺乳类](../Category/更新世哺乳类.md "wikilink")

1.

2.
3.