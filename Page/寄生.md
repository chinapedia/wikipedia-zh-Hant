[Cimex_lectularius.jpg](https://zh.wikipedia.org/wiki/File:Cimex_lectularius.jpg "fig:Cimex_lectularius.jpg")：[人類身上的吸血蟲](../Page/人類.md "wikilink")\]\]

**寄生**是指一种生物生于另一种生物的体内或体表，并从后者摄取养分以维持生活的现象。前者称[寄生物](../Page/寄生物.md "wikilink")，后者称[宿主](../Page/宿主.md "wikilink")。

寄生物若寄住在宿主體內，稱為內寄生，例如[鉤蟲寄生在](../Page/鉤蟲.md "wikilink")[動物的](../Page/動物.md "wikilink")[消化道](../Page/消化道.md "wikilink")；而那些生活在表面的稱為外寄生，例如[蚊子和造成](../Page/蚊.md "wikilink")[足癬](../Page/足癬.md "wikilink")（俗稱：香港腳）的[黴菌](../Page/黴菌.md "wikilink")、吸取其他植物養分的[菟絲子](../Page/菟絲子.md "wikilink")；若一個寄生物會殺死宿主的，便稱為擬寄生物；另外有一種寄生形式稱為竊取性寄生，寄生物偷取宿主所捕捉的或是準備好的食物。

在定義上必須特別注意「獲利」和「被害」在寄生的關係是種族性的、血統性的，並非個體性的，因此如果一個生物體由於被感染，造成身體變得較為強壯的狀況，卻失去[生殖能力](../Page/生殖.md "wikilink")（例如被[扁蟲寄生的](../Page/扁蟲.md "wikilink")[蛇類](../Page/蛇.md "wikilink")）在演化的觀點上這種生物體是被傷害的，也因此稱做被寄生物。

許多內寄生物尋找宿主是透過被動的方式達成，例如一種人類[小腸內](../Page/小腸.md "wikilink")[寄生虫](../Page/寄生虫.md "wikilink")，稱做[線蟲Ascaris](../Page/線蟲.md "wikilink")
lumbricoides，牠從宿主的消化道排出到外在環境，必須仰賴其他人，因為衛生不良而不慎攝入。另一方面，外寄生物在這方面大多有更好的方式找尋宿主上身，例如一些水生的[蛭](../Page/蛭.md "wikilink")，在附著上宿主之前會先感應移動狀況，並且透過散發的體溫和[化學訊息來確認目標物](../Page/化學.md "wikilink")。

寄生物的宿主通常也演化出良好的防禦機制：[植物會製造毒素來殘害寄生](../Page/植物.md "wikilink")[真菌和](../Page/真菌.md "wikilink")[細菌](../Page/細菌.md "wikilink")，當然對草食性動物也有害；[脊椎動物的](../Page/脊椎動物.md "wikilink")[免疫系統可以透過體液對多數的寄生物攻擊](../Page/免疫.md "wikilink")。許多寄生物，特別是[微生物](../Page/微生物.md "wikilink")，為此更演化出可以適應特定宿主物種的能力，在這樣特定的互動中，這兩種生物會共同演化出相對穩定的關係，這種狀況下，[宿主就不會太快或是根本不會被殺死](../Page/宿主.md "wikilink")，因為在演化上宿主的對抗也會對寄生物造成威脅，但是別忘了有一種寄生物是會殺死宿主的，那就是先前提到過的[擬寄生物](../Page/拟寄生物.md "wikilink")（如[寄生蜂](../Page/寄生蜂下目.md "wikilink")）。

有時候寄生物的研究可以幫忙解決[系統分類學上的問題](../Page/系統分類學.md "wikilink")，例如過去[生物學家對於](../Page/生物學家.md "wikilink")[紅鶴究竟和](../Page/紅鶴.md "wikilink")[鴨](../Page/鴨.md "wikilink")、[雁類還是跟](../Page/雁.md "wikilink")[鸛鳥類血緣關係較為親近](../Page/鸛鳥.md "wikilink")，在過去一直有很多的爭議，但是由於發現紅鶴和鴨、雁類有共同的寄生物，目前一般傾向認為這兩者的血緣關係比鸛鳥類更親近。

## 以生物种类分类

  - [病毒](../Page/病毒.md "wikilink")：因为[病毒生存和结构的特殊性](../Page/病毒.md "wikilink")，基本上所有[病毒都应属于营](../Page/病毒.md "wikilink")**寄生**生活，常见的比如[人类乳头瘤病毒](../Page/人类乳头瘤病毒.md "wikilink")（HPV）、烟草花叶病毒等等。
  - [细菌](../Page/细菌.md "wikilink")：营**寄生**生活的[细菌也很普遍](../Page/细菌.md "wikilink")，比如营内寄生（endoparasitic）的[大肠杆菌](../Page/大肠杆菌.md "wikilink")（*E.
    coli*）和营外寄生的[表皮葡萄球菌](../Page/表皮葡萄球菌.md "wikilink")（*Staphylococcus
    epidermis*）。
  - [植物](../Page/植物.md "wikilink")：营**寄生**的[植物不算少数](../Page/植物.md "wikilink")，比如专门吸取[寄主](../Page/寄主.md "wikilink")[植物水分](../Page/植物.md "wikilink")、自我[光合作用的](../Page/光合作用.md "wikilink")[槲寄生](../Page/槲寄生.md "wikilink")（Mistletoe）和[菟丝子](../Page/菟丝子.md "wikilink")（*Cuscuta
    sp.*）。有些植物以寄生[真菌的方式取得養分](../Page/真菌.md "wikilink")，稱為[菌異營植物](../Page/菌異營.md "wikilink")。

## 參見

  - [寄生植物](../Page/寄生植物.md "wikilink")
  - [共生](../Page/共生.md "wikilink")

[学](../Category/寄生虫学.md "wikilink")
[Category:共生](../Category/共生.md "wikilink")
[Category:寄生](../Category/寄生.md "wikilink")