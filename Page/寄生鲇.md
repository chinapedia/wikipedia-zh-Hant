**寄生鲇**（学名*Vandellia*）是一种生长在[南美洲](../Page/南美洲.md "wikilink")[亚马逊河流域的的](../Page/亚马逊河.md "wikilink")[鱼](../Page/鱼.md "wikilink")，属于[鲇形目](../Page/鲇形目.md "wikilink")，[毛鼻鲇科](../Page/毛鼻鲇科.md "wikilink")。它可能是世界上最小的[脊椎动物](../Page/脊椎动物.md "wikilink")，成体小于10毫米，体色透明，无鳞，体形细长，因此很难在水中被发现。其[鳃盖上有成束棘刺](../Page/鳃.md "wikilink")，以[寄生方式生存](../Page/寄生.md "wikilink")：通常钻入大鱼的鳃中，用棘钩住，吸食鱼[血](../Page/血.md "wikilink")，如果有人在水中裸泳，会被其钻入[尿道](../Page/尿道.md "wikilink")。而一旦发生，只能通过[手术取出](../Page/手术.md "wikilink")，有时如果来不及救治会致人于死。而会钻入尿道的原因，据说是因为人[尿中有类似鱼鳃中的](../Page/尿.md "wikilink")[化学物质](../Page/化学物质.md "wikilink")（[氨及](../Page/氨.md "wikilink")[尿素分別是鰓及尿道的蛋白質代謝物](../Page/尿素.md "wikilink")，其代謝產物的來源、性質、作用都非常相似），从而吸引了寄生鲇。寄生鲇的名声令當地人聞之色变，程度有时甚至超过[食人鱼](../Page/食人鱼.md "wikilink")\[1\]。

## 種

  - *[Vandellia balzanii](../Page/Vandellia_balzanii.md "wikilink")*
  - [貝氏寄生鯰](../Page/貝氏寄生鯰.md "wikilink")（*Vandellia beccarii*）
  - [卷鬚寄生鯰](../Page/卷鬚寄生鯰.md "wikilink")（*Vandellia cirrhosa*）
  - *[Vandellia plazaii](../Page/Vandellia_plazaii.md "wikilink")*
  - [亞馬遜河寄生鯰](../Page/亞馬遜河寄生鯰.md "wikilink")（*Vandellia sanguinea*）

## 参考资料

<span style="font-size:smaller;">

<references/>

</span>

[th:ปลากังจีรู](../Page/th:ปลากังจีรู.md "wikilink")

[寄生鯰屬](../Category/寄生鯰屬.md "wikilink")
[Category:吸血](../Category/吸血.md "wikilink")

1.