**密西根上半島** （），簡稱**上半島** （、），又稱**上密西根**
（），更通俗的稱呼為「[橋以北的土地](../Page/麥基諾橋.md "wikilink")」
（the land "above the
Bridge"），是構成[美國](../Page/美國.md "wikilink")[密西根州兩塊陸地之一](../Page/密西根州.md "wikilink")，東為[聖瑪麗河](../Page/聖瑪麗河.md "wikilink")，西為[威斯康辛州](../Page/威斯康辛州.md "wikilink")，北為[蘇必略湖](../Page/蘇必略湖.md "wikilink")，南為[休倫湖和](../Page/休倫湖.md "wikilink")[密西根湖](../Page/密西根湖.md "wikilink")。

上半島面積42,610平方公里，約為全州面積的三分之一，但人口只佔全州3%
（328,000），也是全美國唯一[芬蘭裔佔多數的地區](../Page/芬蘭.md "wikilink")
（當地人被稱為Yoopers）。\[1\]

由於氣候因素，農業不發達，而以[林業和](../Page/林業.md "wikilink")[礦業為經濟支柱](../Page/礦業.md "wikilink")。

## 历史

上密歇根半岛从公元9世纪开始有人类居住。

据推测，法国探险家艾蒂·布鲁勒（Étienne
Brûlé）于1620年前后到达该地区，成为最早到达这一地区的欧洲人，之後上密歇根半岛成为法国殖民地。

1763年，法国在[七年战争中战败](../Page/七年战争.md "wikilink")，被迫将该领地割让给英国，[美国独立战争胜利后](../Page/美国独立战争.md "wikilink")，上密歇根半岛成为美国领土。

## 参考文献

[Category:密西根州地理](../Category/密西根州地理.md "wikilink")
[Category:美国半岛](../Category/美国半岛.md "wikilink")
[Category:密歇根州各地区](../Category/密歇根州各地区.md "wikilink")

1.