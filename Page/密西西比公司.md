**密西西比公司**（）是一間18世紀時的[法國公司](../Page/法國.md "wikilink")，它當時主要目的是在隸屬於法國的[北美地區](../Page/北美洲.md "wikilink")[密西西比河流域](../Page/密西西比河.md "wikilink")，從事貿易和開發等等的商業活動，該公司的經營狀況極差但是股價卻暴漲到發行價格的40倍以上，是近代三大[泡沫經濟事件之一](../Page/泡沫經濟.md "wikilink")。

1717年八月，由一名叫做[約翰·羅的商人所買下](../Page/約翰·羅_\(經濟學者\).md "wikilink")，當時法國政府承諾給他25年的壟斷經營權。密西西比公司先後以其雄厚的財力，發展出了[法蘭西東印度公司和](../Page/法國東印度公司.md "wikilink")[法蘭西銀行](../Page/法蘭西銀行.md "wikilink")。在這段時間之內，該公司的股票由500[里弗尔漲到](../Page/里弗尔.md "wikilink")15000[里弗尔](../Page/里弗尔.md "wikilink")。\[1\]但是在1720年夏天時，投資人對這間公司的信心大減，結果一年之內股票價格跌回500[里弗尔](../Page/里弗尔.md "wikilink")。

## 参见

  - [鬱金香狂熱](../Page/鬱金香狂熱.md "wikilink")
  - [南海泡沫事件](../Page/南海泡沫事件.md "wikilink")
  - [泡沫经济](../Page/泡沫经济.md "wikilink")

## 注释

<div class="references-small">

<references />

</div>

## 外部链接

  - ["The Compagnie des
    Indes"](https://web.archive.org/web/20071225184121/http://www.booneshares.com/Indes.htm)
    -（By Howard Shakespeare）
  - ["Famous First Bubbles - Mississippi
    Bubble"](https://web.archive.org/web/20051129012820/http://www.few.eur.nl/few/people/smant/m-economics/johnlaw.htm)
  - ["Learning from past investment
    manias"](https://web.archive.org/web/20041205025919/http://www.ameinfo.com/news/Detailed/16533.html)
    -（AME Info FN）
  - ["The South Sea Bubble and Law's Mississippi
    Scheme"](https://web.archive.org/web/20060404145341/http://www.dailyreckoning.com/home.cfm?loc=%2Fbody_headline.cfm&qs=id%3D3480)
    -（from The Daily Reckoning）

[Category:法国公司](../Category/法国公司.md "wikilink")
[Category:经济史](../Category/经济史.md "wikilink")
[Category:法蘭西殖民帝國](../Category/法蘭西殖民帝國.md "wikilink")
[Category:泡沫經濟](../Category/泡沫經濟.md "wikilink")
[Category:1684年新法蘭西建立](../Category/1684年新法蘭西建立.md "wikilink")
[Category:1770年新法蘭西廢除](../Category/1770年新法蘭西廢除.md "wikilink")

1.  [里弗尔](../Page/里弗尔.md "wikilink")
    （Livre）是一種法國的貨幣單位，在1795年前法國都是使用此貨幣單位。