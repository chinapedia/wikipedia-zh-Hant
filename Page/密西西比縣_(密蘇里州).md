**密西西比县**（**Mississippi County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州東南的一個縣](../Page/密蘇里州.md "wikilink")，東隔[密西西比河與](../Page/密西西比河.md "wikilink")[肯塔基州相望](../Page/肯塔基州.md "wikilink")。面積1,111平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,427人。縣治[查爾斯頓](../Page/查爾斯頓_\(密蘇里州\).md "wikilink")
(Charleston)。

成立於1845年2月14日。本縣來自密西西比河。

[M](../Category/密苏里州行政区划.md "wikilink")