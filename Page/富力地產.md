**廣州富力地產股份有限公司**()成立於1994年，是[廣州主要的房地產開發公司之一](../Page/廣州.md "wikilink")，業務主要在廣州、[北京](../Page/北京.md "wikilink")、[上海及](../Page/上海.md "wikilink")[天津等等十三個](../Page/天津.md "wikilink")[中國核心城市開發房地產項目](../Page/中國.md "wikilink")。集團亦從事提供其他房地產相關配套服務，包括房地產管理及房地產代理及中介服務。集團[總裁為](../Page/總裁.md "wikilink")[張力](../Page/张力_\(富力地产\).md "wikilink")。

## 簡介

富力地產由[李思廉成立於](../Page/李思廉.md "wikilink")1994年，到了2005年7月在[香港交易所主板上市](../Page/香港交易所.md "wikilink")。曾經成為[恆生中國企業指數成分股之一](../Page/恆生中國企業指數.md "wikilink")，於2011年被剔除。

2009年12月，富力地產聯同[碧桂園及](../Page/碧桂園.md "wikilink")[雅居樂地產同組的財團以](../Page/雅居樂地產.md "wikilink")255億元[人民幣投得廣州](../Page/人民幣.md "wikilink")[番禺亞運城的地皮](../Page/番禺亞運城.md "wikilink")\[1\]。

2010年4月，富力地產以70.5億元人民幣投得[天津](../Page/天津.md "wikilink")[津南區咸水沽鎮一幅地皮](../Page/津南區.md "wikilink")，創下天津單一項目樓盤的體量紀錄和最高地價紀錄\[2\]。

2011年6月，富力地產收購因為財政困難瀕臨解散的[中甲球會](../Page/中甲.md "wikilink")[深圳鳳凰足球俱樂部](../Page/深圳鳳凰足球俱樂部.md "wikilink")，並且改名[廣州富力足球俱樂部](../Page/廣州富力足球俱樂部.md "wikilink")\[3\]。

2013年12月2日富力地產以約45億馬來西亞幣（約108.1億港元）向馬來西亞柔佛依不拉欣蘇丹皇室（Sultan Ibrahim
Johor）收購位於柔佛州新山地區的六幅地塊，地盤面積約116英畝，估計待開發的物業可售樓面面積達350萬平方米。2014年6月21日富力地产正式推介这个发展项目并命名为
Princess Cove Tanjung Puteri。

2017年7月19日
[萬達集團](../Page/萬達集團.md "wikilink")，[融創中國及](../Page/融創中國.md "wikilink")[富力地產在](../Page/富力地產.md "wikilink")[北京召開合作發布會](../Page/北京.md "wikilink")，公告出售資產細節，其中，[富力地產以](../Page/富力地產.md "wikilink")199.06億人民幣收購[萬達集團旗下](../Page/萬達集團.md "wikilink")76家酒店項目全部股權及煙台萬達文華酒店７０％權益，７７家酒店資產淨值預期為不少於331.76億元，包括兩間未開業酒店的估計成本，擁有總建築面積約為328.6萬平方米及合共２３２０２間客房，從[萬達手中以較低價格接手](../Page/萬達.md "wikilink")77家萬達酒店後，[富力地產將持有超過](../Page/富力地產.md "wikilink")100家酒店，成為全球最大的五星級酒店業主，77家酒店，2016年收入為53億元，盈利8億多

## 外部链接

  - [富力地產官方网页](http://www.rfchina.com/)

## 參考

[Category:富力地產](../Category/富力地產.md "wikilink")
[Category:香港上市地產公司](../Category/香港上市地產公司.md "wikilink")
[Category:广州上市公司](../Category/广州上市公司.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:前恒生中國企業指數成份股](../Category/前恒生中國企業指數成份股.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:中國H股](../Category/中國H股.md "wikilink")
[Category:1994年成立的公司](../Category/1994年成立的公司.md "wikilink")

1.  [富力、碧桂園、雅居樂聯手
    奪廣州亞運城地](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20091223&sec_id=15307&subsec=15320&art_id=13553318)
    蘋果日報，2009年12月23日
2.  [富力70億奪天津地王](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100430&sec_id=15307&subsec=15320&art_id=13982074)
    蘋果日報，2010年4月30日
3.  [富力地产低调接手深圳凤凰
    望学恒大模式入主足球](http://sports.sina.com.cn/b/2011-06-25/08575632297.shtml)新浪網