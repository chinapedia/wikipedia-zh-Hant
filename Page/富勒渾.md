**富勒渾**（；\[1\]），[蒙古鑲白旗人](../Page/蒙古鑲白旗.md "wikilink")。富勒渾初自[舉人](../Page/舉人.md "wikilink")，後授為[內閣中書舍人](../Page/內閣.md "wikilink")，累遷[戶郎郎中](../Page/戶郎郎中.md "wikilink")。除此之外，於[乾隆二十年](../Page/乾隆.md "wikilink")（1755年）至[乾隆二十三年](../Page/乾隆.md "wikilink")（1758年）期間，富勒渾亦曾擔任不同省份的[按察使](../Page/按察使.md "wikilink")、[布政使和](../Page/布政使.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")。後接替[孫士毅代理](../Page/孫士毅.md "wikilink")[兩廣總督](../Page/兩廣總督.md "wikilink")。

## 擔任官職沿革

  - 1755年2月，由[熱河道](../Page/熱河道.md "wikilink")[監察御史授予](../Page/監察御史.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[按察使一職](../Page/按察使.md "wikilink")。
  - 1756年3月24日，調任[浙江省](../Page/浙江省.md "wikilink")[布政使](../Page/布政使.md "wikilink")。
  - 1757年8月16日，調任[湖北省](../Page/湖北省.md "wikilink")[布政使](../Page/布政使.md "wikilink")。同年10月調任[湖南省](../Page/湖南省.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")。
  - 1758年5月，[湖南省](../Page/湖南省.md "wikilink")[巡撫一職解職](../Page/巡撫.md "wikilink")。
  - 1785年9月1日，任兩廣總督。
  - 1786年5月23日，兩廣總督一職解職。

## 參考文獻

{{-}}

[Category:清朝浙江按察使](../Category/清朝浙江按察使.md "wikilink")
[Category:清朝浙江布政使](../Category/清朝浙江布政使.md "wikilink")
[Category:清朝湖北布政使](../Category/清朝湖北布政使.md "wikilink")
[Category:清朝湖南巡撫](../Category/清朝湖南巡撫.md "wikilink")
[F](../Category/清朝两广總督.md "wikilink")
[F](../Category/蒙古镶白旗人.md "wikilink")
[Category:清朝工部尚書](../Category/清朝工部尚書.md "wikilink")

1.  满语意思是“恩惠”。（安双成，《满汉大辞典》，1065页）