[Ming_Nga_Court.jpg](https://zh.wikipedia.org/wiki/File:Ming_Nga_Court.jpg "fig:Ming_Nga_Court.jpg")

[Fu_Shin_Shopping_Centre_ground_floor.jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Shopping_Centre_ground_floor.jpg "fig:Fu_Shin_Shopping_Centre_ground_floor.jpg")
[Fu_Shin_Market_(2).jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Market_\(2\).jpg "fig:Fu_Shin_Market_(2).jpg")
[Fu_Shin_Estate_cooked_food_stalls.jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Estate_cooked_food_stalls.jpg "fig:Fu_Shin_Estate_cooked_food_stalls.jpg")
[Fu_Shin_Sports_Centre.jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Sports_Centre.jpg "fig:Fu_Shin_Sports_Centre.jpg")
[Sam_Shui_Natives_Association_Huen_King_Wing_School_(revised).jpg](https://zh.wikipedia.org/wiki/File:Sam_Shui_Natives_Association_Huen_King_Wing_School_\(revised\).jpg "fig:Sam_Shui_Natives_Association_Huen_King_Wing_School_(revised).jpg")
**富善邨**（[英文](../Page/英文.md "wikilink")：**Fu Shin
Estate**）是[香港公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[大埔新市鎮東部的](../Page/大埔新市鎮.md "wikilink")[大埔海](../Page/大埔海.md "wikilink")[填海區](../Page/填海.md "wikilink")，約於1985年落成入伙，鄰近[新興花園](../Page/新興花園.md "wikilink")，樓宇壽命大約為30年。2005年，富善邨為[租者置其屋計劃第六期](../Page/租者置其屋計劃.md "wikilink")(乙)中，其中一個出售的屋邨。

**明雅苑**（[英文](../Page/英文.md "wikilink")：**Ming Nga
Court**）是[香港房屋委員會的](../Page/香港房屋委員會.md "wikilink")[居者有其屋計劃屋苑之一](../Page/居者有其屋計劃.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[大埔新市鎮](../Page/大埔新市鎮.md "wikilink")。

**怡雅苑**（[英文](../Page/英文.md "wikilink")：**Yee Nga
Court**）是[香港房屋委員會的](../Page/香港房屋委員會.md "wikilink")[居者有其屋計劃屋苑之一](../Page/居者有其屋計劃.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[大埔新市鎮](../Page/大埔新市鎮.md "wikilink")。

## 屋邨資料

### 樓宇

富善邨共有六座Y1及Y2型設計住宅大廈，明雅苑共有三座風車型大廈，怡雅苑共有五座新十字型大廈，分別為：

| 屋邨/屋苑   | 樓宇名稱                             | 樓宇類型                               | 落成年份 |
| ------- | -------------------------------- | ---------------------------------- | ---- |
| 富善邨     | 善群樓                              | [Y1型](../Page/Y1型.md "wikilink")   | 1985 |
| 善鄰樓     |                                  |                                    |      |
| 善景樓     | [Y2型](../Page/Y2型.md "wikilink") |                                    |      |
| 善美樓     |                                  |                                    |      |
| 善雅樓     |                                  |                                    |      |
| 善翠樓     |                                  |                                    |      |
| 明雅苑     | 明凱閣                              | 風車型                                | 1985 |
| 明欣閣     |                                  |                                    |      |
| 明昌閣     |                                  |                                    |      |
| 怡雅苑     | 怡禮閣 （A座）                         | [新十字型](../Page/新十字型.md "wikilink") | 1993 |
| 怡順閣（B座） |                                  |                                    |      |
| 怡厚閣（C座） |                                  |                                    |      |
| 怡達閣（D座） |                                  |                                    |      |
| 怡亮閣（E座） |                                  |                                    |      |

## 教育及福利設施

富善邨內設有三間中學，兩間小學和兩間幼稚園：

### 中學

  - [救恩書院](../Page/救恩書院.md "wikilink")，位於善景樓旁邊
  - [孔教學院大成何郭佩珍中學](../Page/孔教學院大成何郭佩珍中學.md "wikilink")，位於善景樓旁邊
  - [香港道教聯合會圓玄學院第二中學](../Page/香港道教聯合會圓玄學院第二中學.md "wikilink")，在富善邨東南面

### 小學

  - [三水同鄉會禤景榮學校](../Page/三水同鄉會禤景榮學校.md "wikilink")，在明雅苑旁邊
  - [港九街坊婦女會孫方中小學](../Page/港九街坊婦女會孫方中小學.md "wikilink")，在富善邨東面，鄰近完善路和完善公園

<!-- end list -->

  - *已結束*

<!-- end list -->

  - [中華基督教會基正小學](../Page/中華基督教會基正小學.md "wikilink")，位於善景樓旁邊（已停辦）

### 幼稚園

  - [基督教香港崇真會安仁幼兒學校](http://www.ttmssd.org/Child/OY/oy.php)（1976年創辦）（位於富善邨）
  - [香港道教聯合會圓玄幼稚園（富善邨）](http://www.yuenyuenkg.edu.hk/fushan/)（1986年創辦）（位於善美樓地下）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 基明幼稚園（1986年創辦）（位於善鄰樓地下）

### 特殊幼兒中心

  - [協康會雷瑞德夫人中心](https://www.heephong.org/center/detail/alice-louey-centre)（位於善翠樓地下）

### 綜合青少年服務中心

  - [路德會賽馬會富善綜合服務中心](http://www.t2lutheran.org.hk)（位於善群樓地下）

### 長者鄰舍中心

  - [中華傳道會恩光長者鄰舍中心](http://www.cnecglnec.org)（位於善雅樓地下104至105室）

### 護理安老院

  - 嗇色園主辦可善護理安老院（位於善景樓地下及2樓）

## 公用設施

富善邨內設有商場、街市、停車場，另有室內體育館。

  - [富善商場](../Page/富善商場.md "wikilink")

Fu Shin Shopping Centre ground floor (2).jpg|富善商場地下（2） Fu Shin Shopping
Centre ground floor (3).jpg|富善商場地下（3） Fu Shin Shopping Centre 1st
floor.jpg|富善商場1樓 Fu Shin Shopping Centre 1st floor (2).jpg|富善商場1樓（2） Fu
Shin Shopping Centre 1st floor (3).jpg|富善商場1樓（3） Fu Shin Shopping Centre
1st floor (4).jpg|富善商場1樓（4） Fu Shin Shopping Centre
(interior).JPG|富善商場1樓（2014年） Fu Shin Shopping Centre 2nd floor
in October 2016.jpg|富善商場2樓 Fu Shin Shopping Centre 2nd floor in October
2016 (2).jpg|富善商場2樓（2）

  - 富善街市

Fu Shin Market (3).jpg|富善街市（2） Fu Shin Market (4).jpg|富善街市（3） Fu Shin
Market (5).jpg|富善街市（4） Fu Shin Market (6).jpg|富善街市（5） Fu Shin Market
under partial renovation in October 2016.jpg|富善街市局部翻新工程 Fu Shin
Market.JPG|富善街市（2014年）

## 附近環境

### 屋苑

[Fu_Shin_Estate_Open_Space_2009.jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Estate_Open_Space_2009.jpg "fig:Fu_Shin_Estate_Open_Space_2009.jpg")
[Fu_Shin_Estate_Fountain_2009.jpg](https://zh.wikipedia.org/wiki/File:Fu_Shin_Estate_Fountain_2009.jpg "fig:Fu_Shin_Estate_Fountain_2009.jpg")
[HK_Fu_Shin_EstateBusTerminal.JPG](https://zh.wikipedia.org/wiki/File:HK_Fu_Shin_EstateBusTerminal.JPG "fig:HK_Fu_Shin_EstateBusTerminal.JPG")
富善邨南方貼近[明雅苑](../Page/明雅苑.md "wikilink")，西北面鄰接[怡雅苑](../Page/怡雅苑.md "wikilink")，西面為[新興花園](../Page/新興花園.md "wikilink")、[大埔廣場和](../Page/大埔廣場.md "wikilink")[大埔中心](../Page/大埔中心.md "wikilink")。南面的[廣福邨](../Page/廣福邨.md "wikilink")、北面的[富亨邨和西北面的](../Page/富亨邨.md "wikilink")[大元邨距離富善邨都是約](../Page/大元邨.md "wikilink")
15
分鐘步行路程。富善邨由[新昌管理服務有限公司作](../Page/新昌管理服務有限公司.md "wikilink")[物業管理](../Page/物業管理.md "wikilink")，[升降機由](../Page/升降機.md "wikilink")[蒂森克虜伯電梯有限公司維修](../Page/蒂森克虜伯.md "wikilink")。但由於在2008年10月發生升降機罕見下墮意外後，於2010年舉行業主大會投票，決定由2010年3月1日起，更換另一個承辦商[奧的斯電梯有限公司接手管理](../Page/奧的斯電梯公司.md "wikilink")，同時將所有原裝[通力升降機更換為奧的斯升降機](../Page/通力.md "wikilink")。（參見[1](http://news.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/928710/1.html)）

### 休憩設施

  - [完善公園](../Page/完善公園.md "wikilink")
  - [大埔海濱公園](../Page/大埔海濱公園.md "wikilink")

<!-- end list -->

  -
    兩者皆位於富善邨東南面

<!-- end list -->

  - 富善邨遊憩設施

Fu Shin Estate playground.jpg|兒童遊樂場 Fu Shin Estate playground
(2).jpg|兒童遊樂場（2） Fu Shin Estate playground (3).jpg|兒童遊樂場（3） Fu
Shin Estate playground (4).jpg|兒童遊樂場（4） Fu Shin Estate badminton
court.jpg|羽毛球場 Fu Shin Estate table tennis play area.jpg|乒乓球場 Fu Shin
Estate stretch pole.jpg|伸展柱

  - 明雅苑設施

Ming Nga Court playground.jpg|兒童遊樂場 Ming Nga Court stretch pole.jpg|伸展柱

  - 怡雅苑設施

Yee Nga Court fountain.jpg|噴水池 Yee Nga Court playground.jpg|兒童遊樂場 Yee
Nga Court table tennis play area.jpg|乒乓球場 Yee Nga Court pebble walking
trail.jpg|卵石路步行徑 Yee Nga Court covered walkway.jpg|怡雅苑有蓋行人道

### 學校

富善邨附近學校林立，除了邨內數間中小學（詳見本頁上方），附近另有：

#### 中學

  - [迦密柏雨中學](../Page/迦密柏雨中學.md "wikilink")
  - [中華聖潔會靈風中學](../Page/中華聖潔會靈風中學.md "wikilink")

#### 小學

  - [大埔循道衛理小學](../Page/大埔循道衛理小學.md "wikilink")

## 圖庫

HK_TaiPoWaterfrontPark_Overview.JPG|從[白石角](../Page/白石角.md "wikilink")[單車徑眺望富善邨及](../Page/單車徑.md "wikilink")[大埔海濱公園](../Page/大埔海濱公園.md "wikilink")
Ming Nga Court renovation works.JPG|明雅苑翻新工程

## 途經的公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [富善邨巴士總站](../Page/富善邨巴士總站.md "wikilink")/[安埔路](../Page/安埔路.md "wikilink")

<!-- end list -->

  - [汀角路](../Page/汀角路.md "wikilink")

<!-- end list -->

  - [南運路](../Page/南運路.md "wikilink")

<!-- end list -->

  - [完善路](../Page/完善路.md "wikilink")

</div>

</div>

## 區議員

  - [關永業](../Page/關永業.md "wikilink")
  - [任啟邦](../Page/任啟邦.md "wikilink")

## 重大事件

  - 2008年10月25日，善雅樓L32升降機從14樓墜落地下，無人受傷。\[1\]\[2\]

## 另見

  - [富善街](../Page/富善街.md "wikilink")

## 外部連結

[房委會：富善邨簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2740)

  - [房委會：明雅苑簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3068)
  - [房委會：怡雅苑簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3076)

## 參考

  - [香港公共屋邨列表](../Page/香港公共屋邨列表.md "wikilink")
  - [香港居者有其屋列表](../Page/香港居者有其屋列表.md "wikilink")

[en:Public housing estates in Tai Po\#Fu Shin
Estate](../Page/en:Public_housing_estates_in_Tai_Po#Fu_Shin_Estate.md "wikilink")
[en:Public housing estates in Tai Po\#Ming Nga
Court](../Page/en:Public_housing_estates_in_Tai_Po#Ming_Nga_Court.md "wikilink")
[en:Public housing estates in Tai Po\#Yee Nga
Court](../Page/en:Public_housing_estates_in_Tai_Po#Yee_Nga_Court.md "wikilink")

[Category:大埔 (香港)](../Category/大埔_\(香港\).md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.
2.