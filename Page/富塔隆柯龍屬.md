**富塔隆柯龍屬**（[學名](../Page/學名.md "wikilink")：*Futalognkosaurus*）是[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，是[草食性的恐龍](../Page/草食性.md "wikilink")\[1\]，嗅覺敏銳。生存於[上白堊紀的](../Page/上白堊紀.md "wikilink")[科尼亞克階](../Page/科尼亞克階.md "wikilink")，約8700萬年前。在發現富塔隆柯龍的位點附近，亦發現有[魚類及](../Page/魚類.md "wikilink")[樹葉的化石](../Page/樹葉.md "wikilink")，可見在當時的[巴塔哥尼亞是一個](../Page/巴塔哥尼亞.md "wikilink")[熱帶氣候的地方](../Page/熱帶.md "wikilink")。

## 發現與命名

[Futalognkosaurus_dukei.jpg](https://zh.wikipedia.org/wiki/File:Futalognkosaurus_dukei.jpg "fig:Futalognkosaurus_dukei.jpg")
[Futalognkosaurus.JPG](https://zh.wikipedia.org/wiki/File:Futalognkosaurus.JPG "fig:Futalognkosaurus.JPG")
富塔隆柯龍的[化石是在](../Page/化石.md "wikilink")2000年在[阿根廷](../Page/阿根廷.md "wikilink")[內烏肯省發現](../Page/內烏肯省.md "wikilink")，並在2007年才正式被發表、命名。學名是來自當地的[馬普切語](../Page/馬普切語.md "wikilink")，意思是「巨大的首領[蜥蜴](../Page/蜥蜴.md "wikilink")」\[2\]。[模式種是](../Page/模式種.md "wikilink")**杜氏富塔隆柯杜克龍**（*F.
dukei*），身長估計約32-34米長。牠的化石有三組標本，可建構出整體[骨骼的](../Page/骨骼.md "wikilink")70%，被認為是目前最為完整的巨型恐龍。

## 分類

在Jorge
Calvo等人的[系統發生學分析中](../Page/系統發生學.md "wikilink")，發現富塔隆柯龍是屬於[泰坦巨龍科](../Page/泰坦巨龍科.md "wikilink")（現等於[岩盔龍類演化支](../Page/岩盔龍類.md "wikilink")），並與[門多薩龍](../Page/門多薩龍.md "wikilink")（*Mendozasaurus*）是近親。他們從而成立了一個包括富塔隆柯龍及門多薩龍，並牠們的共同祖先及後代的新[演化支](../Page/演化支.md "wikilink")，稱為[隆柯龍類](../Page/隆柯龍類.md "wikilink")（Lognkosauria）\[3\]。他們也發現了[馬拉威龍是這個演化支的姊妹](../Page/馬拉威龍.md "wikilink")[分類單元](../Page/分類單元.md "wikilink")。[普爾塔龍也被認為屬於隆柯龍類](../Page/普爾塔龍.md "wikilink")，生存年代較晚，也是種巨型恐龍\[4\]。除了富塔隆柯龍外，在同一地點上亦發現了其他的[動物群](../Page/動物群.md "wikilink")，包括兩類未被命名的[蜥腳下目](../Page/蜥腳下目.md "wikilink")、[大盜龍](../Page/大盜龍.md "wikilink")、[半鳥及](../Page/半鳥.md "wikilink")[側頸龜亞目的標本](../Page/側頸龜亞目.md "wikilink")。

## 參考資料

## 外部連結

  - [Dinosaur Mailing List](http://dml.cmnh.org/2004Aug/msg00291.html)
  - [Fósiles hallados en el Centro Paleontológico Lago
    Barreales](http://www.proyectodino.com.ar)
  - [BBC新聞報導](http://news.bbc.co.uk/2/hi/science/nature/7046223.stm)
  - [富塔隆柯龍的電腦圖像](https://web.archive.org/web/20071017044715/http://ap.google.com/article/ALeqM5iGJds4L0q44m7IWynfx7Mgr0NXeAD8S9TC9G0)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:泰坦巨龍類](../Category/泰坦巨龍類.md "wikilink")

1.

2.

3.
4.