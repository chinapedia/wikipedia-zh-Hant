**富士市**（）是[靜岡縣東部的城市](../Page/靜岡縣.md "wikilink")，[施行時特例市](../Page/施行時特例市.md "wikilink")。人口數居于靜岡縣內第三位。也是富士都市圈（約40万人）的行政、商業、工業中心。[日本製紙](../Page/日本製紙.md "wikilink")（舊大昭和製紙）、[王子製紙等很多造紙工廠都設立于此](../Page/王子製紙.md "wikilink")，在戰后的經濟高度成長期，很多造紙工廠將廢水傾倒入海中，加上空氣污染，引發嚴重的社會問題。

## 交通

### 鐵道

  - 中央車站：[富士站](../Page/富士站.md "wikilink")
  - [JR東海](../Page/東海旅客鐵道.md "wikilink")
      - [東海道本線](../Page/東海道本線.md "wikilink")：東田子之浦站－吉原站－富士站－富士川站
      - [身延線](../Page/身延線.md "wikilink")：富士站－柚木站－竪堀站－入山瀨站－富士根站－
      - [東海道新幹線](../Page/東海道新幹線.md "wikilink")：[新富士站](../Page/新富士站.md "wikilink")
  - [岳南鐵道](../Page/岳南鐵道.md "wikilink")：全線

## 地理

富士市位于日本最高的山富士山以南，面向[駿河灣](../Page/駿河灣.md "wikilink")。市內[海岸線長](../Page/海岸線.md "wikilink")10公里。市域東西寬17.9km、南北長27.5km。據2006年統計，一年之內中有192日可看見[富士山](../Page/富士山.md "wikilink")（全部116日、一部份76日），且富士市是唯一可眺望到富士山最大之[寄生火山](../Page/寄生火山.md "wikilink")——[寶永山的地方](../Page/寶永山.md "wikilink")，因此當地文宣及居民所描繪的富士山會有多一角\[1\]。

### 相鄰的自治體

  - [静岡市](../Page/静岡市.md "wikilink")[清水区](../Page/清水区_\(日本\).md "wikilink")
  - [富士宮市](../Page/富士宮市.md "wikilink")
  - 裾野市
  - [御殿場市](../Page/御殿場市.md "wikilink")
  - [沼津市](../Page/沼津市.md "wikilink")
  - 芝川町

[Mount_Fuji_and_Shinkansen_N700.jpg](https://zh.wikipedia.org/wiki/File:Mount_Fuji_and_Shinkansen_N700.jpg "fig:Mount_Fuji_and_Shinkansen_N700.jpg")的[新幹線列車](../Page/新幹線.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Fuji_city_from_Ashitaka_Mountains.jpg "fig:缩略图")

## 姊妹或友好城市

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>國家</p></th>
<th><p>締結日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/嘉興市.md" title="wikilink">嘉興</a>（<a href="../Page/浙江省.md" title="wikilink">浙江省</a>）</p></td>
<td></td>
<td><p>1989年1月13日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐申賽德_(加利福尼亞州).md" title="wikilink">歐申賽德</a>（<a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a>）</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [富士市公所](http://www.city.fuji.shizuoka.jp/)

[\*](../Category/富士市.md "wikilink")

1.  [富士山怎麼多一角？富士市民：這是全日本獨有的驕傲](https://travel.ettoday.net/article/415744.htm)