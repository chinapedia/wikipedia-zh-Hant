**富士見Fantasia文庫**（，），是[日本](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")[KADOKAWA旗下](../Page/KADOKAWA.md "wikilink")[富士見書房發行的](../Page/富士見書房.md "wikilink")[輕小說](../Page/輕小說.md "wikilink")[文庫](../Page/文庫.md "wikilink")。1988年和《[月刊Dragon
Magazine](../Page/月刊Dragon_Magazine.md "wikilink")》一同創刊，主要讀者為10多歲的國高中生，從創刊當初便發行了眾多以[奇幻世界為舞台的](../Page/奇幻.md "wikilink")[小說](../Page/小說.md "wikilink")。現在和[電擊文庫](../Page/電擊文庫.md "wikilink")、[角川Sneaker文庫並列為](../Page/角川Sneaker文庫.md "wikilink")[角川集團的輕小說主力文庫](../Page/角川集團.md "wikilink")。發行的第一部作品是[田中芳樹著](../Page/田中芳樹.md "wikilink")。

雖然以「Fantasia」為名，但題材並不侷限於奇幻類型，亦出版許多以現代與其他世界為舞台的書籍。

由富士見書房主辦的[Fantasia長篇小說大賞](../Page/Fantasia長篇小說大賞.md "wikilink")，得獎作品由本文庫發行。

## 作品一覽

### [台灣角川](../Page/台灣角川.md "wikilink")

  - [驚爆危機](../Page/驚爆危機.md "wikilink")（[賀東招二](../Page/賀東招二.md "wikilink")/[四季童子](../Page/四季童子.md "wikilink")）
  - [風之聖痕](../Page/風之聖痕.md "wikilink")（[山門敬弘](../Page/山門敬弘.md "wikilink")/[納都花丸](../Page/納都花丸.md "wikilink")）
  - [BLACK BLOOD
    BROTHERS](../Page/BLACK_BLOOD_BROTHERS.md "wikilink")（/[草河遊也](../Page/草河遊也.md "wikilink")）
  - [黃昏色的詠使](../Page/黃昏色的詠使.md "wikilink")（[細音啓](../Page/細音啓.md "wikilink")/[竹岡美穗](../Page/竹岡美穗.md "wikilink")）
  - [The
    Third](../Page/The_Third.md "wikilink")（[星野亮](../Page/星野亮.md "wikilink")/）
  - [節哀唷♥二之宮同學](../Page/節哀唷♥二之宮同學.md "wikilink")（[鈴木大輔](../Page/鈴木大輔.md "wikilink")/[高苗京鈴](../Page/高苗京鈴.md "wikilink")）
  - [鋼殼都市雷吉歐斯](../Page/鋼殼都市雷吉歐斯.md "wikilink")（/[深遊](../Page/深遊.md "wikilink")）
  - [特甲少女
    焱之精靈](../Page/特甲少女_焱之精靈.md "wikilink")（[冲方丁](../Page/冲方丁.md "wikilink")/）
  - [特甲少女
    惡戲之猋](../Page/特甲少女_惡戲之猋.md "wikilink")（冲方丁/[白亞右月](../Page/白亞右月.md "wikilink")）
  - [EME
    BLACK](../Page/EME_\(輕小說\).md "wikilink")（[瀧川武司](../Page/瀧川武司.md "wikilink")/[尾崎弘宣](../Page/尾崎弘宣.md "wikilink")）
  - [EME BLUE](../Page/EME_\(輕小說\).md "wikilink")（瀧川武司/尾崎弘宣）
  - [EME RED](../Page/EME_\(輕小說\).md "wikilink")（瀧川武司/尾崎弘宣）
  - [抱歉囉♥二之宮同學](../Page/抱歉囉♥二之宮同學.md "wikilink")（[鈴木大輔](../Page/鈴木大輔.md "wikilink")/[高苗京鈴](../Page/高苗京鈴.md "wikilink")）
  - [碧陽學園學生會議事錄](../Page/碧陽學園學生會議事錄.md "wikilink")（[葵關南](../Page/葵關南.md "wikilink")/[狗神煌](../Page/狗神煌.md "wikilink")）
  - [天魔黑兔](../Page/天魔黑兔.md "wikilink")（[鏡貴也](../Page/鏡貴也.md "wikilink")/[榎宮祐](../Page/榎宮祐.md "wikilink")）
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（[木村心一](../Page/木村心一.md "wikilink")/、）
  - [冰結鏡界的伊甸](../Page/冰結鏡界的伊甸.md "wikilink")（細音啓/）
  - [神不在的星期天](../Page/神不在的星期天.md "wikilink")（[入江君人](../Page/入江君人.md "wikilink")/[茨乃](../Page/茨乃.md "wikilink")）
  - [美少女死神還我H之魂](../Page/美少女死神還我H之魂.md "wikilink")（/）
  - [惡魔高校D×D](../Page/惡魔高校D×D.md "wikilink")（[石踏一榮](../Page/石踏一榮.md "wikilink")/）
  - [棺姬嘉依卡](../Page/棺姬嘉依卡.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")/）
  - [想變成宅女，就讓我當現充！](../Page/想變成宅女，就讓我當現充！.md "wikilink")（[村上凜](../Page/村上凜.md "wikilink")/）
  - [約會大作戰](../Page/約會大作戰.md "wikilink")（[橘公司](../Page/橘公司.md "wikilink")/）
  - [鳩子與我的愛情喜劇](../Page/鳩子與我的愛情喜劇.md "wikilink")（[鈴木大輔](../Page/鈴木大輔.md "wikilink")/[nauribon](../Page/nauribon.md "wikilink")）
  - [我的狐姬主人](../Page/我的狐姬主人.md "wikilink")（/[p19](../Page/p19.md "wikilink")）
  - [不起眼女主角培育法](../Page/不起眼女主角培育法.md "wikilink")（[丸戶史明](../Page/丸戶史明.md "wikilink")/[深崎暮人](../Page/深崎暮人.md "wikilink")）
  - [不完全神性機關伊莉斯](../Page/不完全神性機關伊莉斯.md "wikilink")（[細音啓](../Page/細音啓.md "wikilink")/）
  - [刺客守則](../Page/刺客守則.md "wikilink")（/）

### [尖端出版](../Page/尖端出版.md "wikilink")

  - [魔法戰士李維](../Page/魔法戰士李維.md "wikilink")（[水野良](../Page/水野良.md "wikilink")/[士貴智志](../Page/士貴智志.md "wikilink")→[橫田守](../Page/橫田守.md "wikilink")）

### [奇幻基地](../Page/奇幻基地.md "wikilink")

  - [廢棄公主](../Page/廢棄公主.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")/[安曇雪伸](../Page/安曇雪伸.md "wikilink")）
  - [傳說的勇者的傳說](../Page/傳說的勇者的傳說.md "wikilink")（[鏡貴也](../Page/鏡貴也.md "wikilink")/）
  - [秀逗魔導士](../Page/秀逗魔導士.md "wikilink")（[神坂一](../Page/神坂一.md "wikilink")/，台灣角川、臺灣東販代理漫畫版）

### [台灣東販](../Page/台灣東販.md "wikilink")

  - [天地無用\!
    魎皇鬼](../Page/天地無用!.md "wikilink")（[長谷川菜穗子](../Page/長谷川菜穗子.md "wikilink")）

### [東立出版社](../Page/東立出版社.md "wikilink")

  - [SH@PPLE](../Page/SH@PPLE.md "wikilink")（[竹岡葉月](../Page/竹岡葉月.md "wikilink")/）

  - [煉獄神盾](../Page/煉獄神盾.md "wikilink")（[貴子潤一郎](../Page/貴子潤一郎.md "wikilink")/）

  - [卡納克的軌跡](../Page/卡納克的軌跡.md "wikilink")（[上總朋大](../Page/上總朋大.md "wikilink")/）

  - [中之下！](../Page/中之下！.md "wikilink")（/）

  - [東京闇鴉](../Page/東京闇鴉.md "wikilink")（/）

  - [L
    詐欺師佛雷特蘭德的華麗傳說](../Page/L_詐欺師佛雷特蘭德的華麗傳說.md "wikilink")（[坂照鉄平](../Page/坂照鉄平.md "wikilink")/[水城葵](../Page/水城葵.md "wikilink")→[水谷悠珠](../Page/水谷悠珠.md "wikilink")）

  - [Re:笨蛋也能拯救世界？](../Page/Re：笨蛋也能拯救世界？.md "wikilink")（[柳實冬貴](../Page/柳實冬貴.md "wikilink")/）

  - [尼特族吸血鬼·江藤](../Page/尼特族吸血鬼·江藤.md "wikilink")（[鈴木大輔](../Page/鈴木大輔.md "wikilink")/[空中幼彩](../Page/空中幼彩.md "wikilink")）

  - [夏海紗音與不可思議的世界](../Page/夏海紗音與不可思議的世界.md "wikilink")（/）

  - [彼岸花綻放之夜](../Page/彼岸花綻放之夜.md "wikilink")（/）

  - [RISING×RYDEEN
    異能對決](../Page/RISING×RYDEEN_異能對決.md "wikilink")（[初美陽一](../Page/初美陽一.md "wikilink")/）

  - [當不成勇者的我，只好認真找工作了。](../Page/當不成勇者的我，只好認真找工作了。.md "wikilink")（[左京潤](../Page/左京潤.md "wikilink")/[戌角柾](../Page/戌角柾.md "wikilink")）

  - [絕對服從少女](../Page/絕對服從少女.md "wikilink")（[春日秋人](../Page/春日秋人.md "wikilink")/[樹人](../Page/樹人.md "wikilink")）

  - [SKYWORLD蒼穹境界](../Page/SKYWORLD蒼穹境界.md "wikilink")（/[武藤此史](../Page/武藤此史.md "wikilink")）

  - [冰雪少女](../Page/冰雪少女.md "wikilink")（[山田有](../Page/山田有.md "wikilink")/[狐印](../Page/狐印.md "wikilink")）

  - [勇者凜的傳說](../Page/勇者凜的傳說.md "wikilink")（[琴平稜](../Page/琴平稜.md "wikilink")/[karory](../Page/karory.md "wikilink")）

  - [銀之十字架與吸血姬](../Page/銀之十字架與吸血姬.md "wikilink")（/）

  - （/[水野良](../Page/水野良.md "wikilink")/[春日歩](../Page/春日歩.md "wikilink")）

  - [不正經的魔術講師與禁忌教典](../Page/不正經的魔術講師與禁忌教典.md "wikilink")（[羊太郎](../Page/羊太郎.md "wikilink")/[三嶋くろね](../Page/三嶋くろね.md "wikilink")）

  - [对魔导学园35试验小队](../Page/对魔导学园35试验小队.md "wikilink")

### [四季國際](../Page/春天出版社.md "wikilink")

  - [期間限定妹妹](../Page/期間限定妹妹.md "wikilink")（/[Anmi](../Page/Anmi.md "wikilink")）

### [天闻角川](../Page/天闻角川.md "wikilink")

  - [碧阳学园学生会议事录](../Page/碧阳学园学生会议事录.md "wikilink")（[葵关南](../Page/葵关南.md "wikilink")/[狗神煌](../Page/狗神煌.md "wikilink")）
  - [魔装少女就是本少爷！](../Page/魔装少女就是本少爷！.md "wikilink")（[木村心一](../Page/木村心一.md "wikilink")/[小舞一](../Page/小舞一.md "wikilink")、[梦璃凛](../Page/梦璃凛.md "wikilink")）
  - [约会大作战](../Page/约会大作战.md "wikilink")（[橘公司](../Page/橘公司.md "wikilink")/[Tsunako](../Page/Tsunako.md "wikilink")）
  - [神不在的星期天](../Page/神不在的星期天.md "wikilink")（[入江君人](../Page/入江君人.md "wikilink")/[茨乃](../Page/茨乃.md "wikilink")）
  - [想变成宅女，就让我当现充](../Page/想變成宅女，就讓我當現充！.md "wikilink")（[村上凛](../Page/村上凛.md "wikilink")/）
  - [东京暗鸦](../Page/东京暗鸦.md "wikilink")（[字野耕平](../Page/字野耕平.md "wikilink")/[澄兵](../Page/澄兵.md "wikilink")）

### 未代理

  - [偶像大師 XENOGLOSSIA](../Page/偶像大師_XENOGLOSSIA.md "wikilink")
    ～絆～（[涼風涼](../Page/涼風涼.md "wikilink")）

  - [機械女神J](../Page/機械女神.md "wikilink")（[赤堀悟](../Page/赤堀悟.md "wikilink")/，台灣東販代理漫畫版）

  - [機甲兵團
    J-PHOENIX](../Page/機甲兵團_J-PHOENIX.md "wikilink")（作：，案：，著：[唯野条太郎](../Page/唯野条太郎.md "wikilink")//[石野聰](../Page/石野聰.md "wikilink")）

  - [氣象精靈記](../Page/氣象精靈記.md "wikilink")（[清水文化](../Page/清水文化.md "wikilink")/[七瀨葵](../Page/七瀨葵.md "wikilink")）

  - [機動警察](../Page/機動警察.md "wikilink")（[伊藤和典](../Page/伊藤和典.md "wikilink")、[橫手美智子](../Page/橫手美智子.md "wikilink")，大然文化代理漫畫版）

  - [銀河天使](../Page/銀河天使.md "wikilink")（[水野良](../Page/水野良.md "wikilink")、，台灣角川代理漫畫版）

  - [死神與巧克力百匯](../Page/死神與巧克力百匯.md "wikilink")（[花凰神也](../Page/花凰神也.md "wikilink")、，台灣角川代理漫畫版）

  - [京四郎與永遠的空
    -前奏曲-](../Page/京四郎與永遠的空.md "wikilink")（[植竹須美男](../Page/植竹須美男.md "wikilink")/[介錯](../Page/介錯_\(漫画家\).md "wikilink")，台灣角川代理漫畫版）

  - [聖槍修女](../Page/聖槍修女.md "wikilink")（[富永浩史](../Page/富永浩史.md "wikilink")/，東立出版社代理漫畫版）

  - [櫻花大戰系列](../Page/櫻花大戰系列.md "wikilink")（[赤堀悟](../Page/赤堀悟.md "wikilink")、/[奧田萬里ほか](../Page/奧田萬里.md "wikilink")）

  - （[雜賀禮史](../Page/雜賀禮史.md "wikilink")/）

  - [星方遊撃隊エンジェル・リンクス](../Page/星方天使.md "wikilink")（[伊吹秀明](../Page/伊吹秀明.md "wikilink")/[幡池裕行](../Page/幡池裕行.md "wikilink")）

  - [創聖的亞庫艾里翁](../Page/創聖的亞庫艾里翁.md "wikilink")（作:[河森正治](../Page/河森正治.md "wikilink"),著:[橘圭](../Page/橘圭.md "wikilink")/[金田榮路](../Page/金田榮路.md "wikilink")，台灣角川代理漫畫版）

  - [宇宙戰艦山本洋子](../Page/宇宙戰艦山本洋子.md "wikilink")（[庄司卓](../Page/庄司卓.md "wikilink")/[赤石澤貴士](../Page/赤石澤貴士.md "wikilink")，台灣東販代理漫畫版）

  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")（作:，著:[大河內一樓](../Page/大河內一樓.md "wikilink")/，，台灣角川代理漫畫版）

  - [Di Gi Charat Fantasy](../Page/Di_Gi_Charat.md "wikilink")（/，）

  - [火魅子炎戰記](../Page/火魅子傳.md "wikilink")（[舞阪洸](../Page/舞阪洸.md "wikilink")/[大暮維人](../Page/大暮維人.md "wikilink")、）

  - [火魅子傳](../Page/火魅子傳.md "wikilink")（舞阪洸/）

  - [魔法使的條件
    太陽與風的坡道](../Page/魔法使的條件_太陽與風的坡道.md "wikilink")（[山田典枝](../Page/山田典枝.md "wikilink")/，台灣角川代理漫畫版）

  - [魔術士歐菲](../Page/魔術士歐菲.md "wikilink")（[秋田禎信](../Page/秋田禎信.md "wikilink")/[草河遊也](../Page/草河遊也.md "wikilink")，東立出版社代理漫畫版）\[1\]

  - [愛的魔法](../Page/愛的魔法.md "wikilink")（[築地俊彥](../Page/築地俊彥.md "wikilink")/[駒都英二](../Page/駒都英二.md "wikilink")，台灣角川代理漫畫版）

  - [無責任艦長](../Page/無責任艦長.md "wikilink")（[吉岡平](../Page/吉岡平.md "wikilink")/[都築和彥](../Page/都築和彥.md "wikilink")→[平田智浩](../Page/平田智浩.md "wikilink")）

  - [無敵王TRI-ZENON](../Page/無敵王TRI-ZENON.md "wikilink")（[長谷川勝己](../Page/長谷川勝己.md "wikilink")//[花田十輝](../Page/花田十輝.md "wikilink")/[日下弘文](../Page/日下弘文.md "wikilink")）

  - [六門天外](../Page/六門天外.md "wikilink")（[黑田和人](../Page/黑田和人.md "wikilink")/[赤堀悟](../Page/赤堀悟.md "wikilink")/[長谷川勝己](../Page/長谷川勝己.md "wikilink")[安田均](../Page/安田均.md "wikilink")/[松本嵩春](../Page/松本嵩春.md "wikilink")，東立出版社代理漫畫版）

  - [火箭女孩](../Page/火箭女孩.md "wikilink")（[野尻抱介](../Page/野尻抱介.md "wikilink")/）

  - [失落的宇宙](../Page/失落的宇宙.md "wikilink")（[神坂一](../Page/神坂一.md "wikilink")/[義仲翔子](../Page/義仲翔子.md "wikilink")）

  - [公主+天國](../Page/公主+天國.md "wikilink")（[風見周](../Page/風見周.md "wikilink")/、[睦月](../Page/睦月.md "wikilink")）

## 相關項目

  - [月刊Dragon Magazine](../Page/月刊Dragon_Magazine.md "wikilink")
  - [富士見Dragon Book](../Page/富士見Dragon_Book.md "wikilink")
  - [Light Novel Award](../Page/Light_Novel_Award.md "wikilink")
      - [富士見Mystery文庫](../Page/富士見Mystery文庫.md "wikilink")
      - [角川Sneaker文庫](../Page/角川Sneaker文庫.md "wikilink")
      - [電擊文庫](../Page/電擊文庫.md "wikilink")
      - [Fami通文庫](../Page/Fami通文庫.md "wikilink")
  - [輕小說作家列表](../Page/輕小說作家列表.md "wikilink")

## 注释

## 外部連結

  - [富士見Fantasia文庫官方網站](http://www.fujimishobo.co.jp/novel/fantasia.php)

  -
[F](../Category/富士見書房.md "wikilink")
[\*](../Category/富士見Fantasia文庫.md "wikilink")
[Category:1988年日本建立](../Category/1988年日本建立.md "wikilink")

1.  日本版權已移籍至[TO Books](../Page/TO_Books.md "wikilink")。