**富士通公司**（[日語](../Page/日语.md "wikilink")：富士通株式会社，英語：Fujitsu
Limited）是一家源自於[日本的綜合跨國電子製造公司與資訊科技](../Page/日本.md "wikilink")(ICT)服務公司。總部位於東京，擁有100個以上的海外據點與16萬名以上的優秀員工，提供世界各地的客戶最佳的專業ICT服務。致力於研發、製造與銷售各類通訊系統、資訊處理系統與電子產品([半導體、](../Page/半導體.md "wikilink")[超級電腦](../Page/超級電腦.md "wikilink")、[個人電腦](../Page/個人電腦.md "wikilink")、[伺服器](../Page/伺服器.md "wikilink")）及相關服務，為日本第一大、世界領先的資訊科技公司。

## 歷史

1935年，「富士通信機製造」成立，為富士電機製造株式會社（今[富士電機控股株式會社](../Page/富士電機.md "wikilink")）的一個分支，依次成為[古河電氣工業](../Page/古河電氣工業.md "wikilink")（Furukawa
Electric Company）與[西門子公司](../Page/西門子公司.md "wikilink")（Siemens
AG）的合資企業。儘管與[古河財閥有關聯](../Page/古河財閥.md "wikilink")，富士通避開了[盟軍](../Page/同盟國_\(第二次世界大戰\).md "wikilink")[佔領時期的分割解散](../Page/同盟國軍事佔領日本.md "wikilink")，業務大部分未受損害。

1954年，富士通硏製出日本第一台電腦FACOM 100；七年後，它的晶狀管兄弟機FACOM
222加入競爭。1967年，公司的名字正式改為縮寫Fujitsū（富士通）。

標語“The possibilities are
infinite”在主要廣告中的商標後方可見，在Fujitsu一詞中J及I字上方可找到一個小型的商標，該小型商標的橫向八字與[無限大的符號相似](../Page/無限大.md "wikilink")。

今日，富士通雇用了大約158,000名員工以及在其[子公司中雇用了](../Page/子公司.md "wikilink")500名員工，以合資[富士通西門子](../Page/富士通-西門子電腦公司.md "wikilink")（1999年成立）的形式恢復與西門子活躍的合作關係，該公司為[歐洲最大的](../Page/歐洲.md "wikilink")[資訊科技供應商](../Page/資訊科技.md "wikilink")，富士通及西門子各擁有一半控制權。富士通視[IBM為國際市場上的主要對手](../Page/IBM.md "wikilink")，[NEC為國內的對手](../Page/日本電氣.md "wikilink")。[英國的International](../Page/英國.md "wikilink")
Computers
Ltd（ICL）與[美國的Amdahl及Rapidigm皆為](../Page/美國.md "wikilink")「富士通服務」的品牌。

2010年10月1日設立的合資公司富士通[東芝行動通訊](../Page/東芝.md "wikilink")（Fujitsu Toshiba
Mobile Communications
Limited），富士通持有80.1%股權主導合資公司營運，東芝則持有剩餘的19.9%股權\[1\]。

2012年4月3日**富士通**向[東芝收購富士通](../Page/東芝.md "wikilink")[東芝行動通訊餘下的](../Page/東芝.md "wikilink")19.9%股份，成為獨資企業，並且已經在4月1日起正式將公司名稱更換為富士通行動通訊股份有限公司（Fujitsu
Mobile Communications Limited）。

2012年10月1日，富士通子公司富士通半導體（Fujitsu Semiconductor）和
Panasonic系統LSI部門正式宣佈將把旗下的LSI（大型積體電路）設計、研發部門與後者合併，建立全新的合資公司。

2014年7月31日，富士通半导体（Fujitsu
Semiconductor）和[安森美半导体](../Page/安森美半导体.md "wikilink")（ON
Semiconductor）宣布战略合作协议（[晶圆代工服务协议及安森美半导体收购](../Page/晶圆代工.md "wikilink")8英寸会津若松晶圆厂10%股份协议。\[2\]

2015年12月24日，富士通分拆 PC 及行動手機業務，分別成立 Fujitsu Client Computing Limited 及
Fujitsu Connected Technologies 2 家全資子公司。

2017年11月2日，富士通宣佈與[聯想集團](../Page/联想集团.md "wikilink")（Lenovo）與日本政策投資銀行（Development
Bank of Japan Inc.)
建立戰略合作關係，期望提升聯想和富士通的個人電腦業務在全球市場的競爭力。\[3\]聯想集團將作價178.5億日圓收購富士通子公司Fujitsu
Client Computing
Limited（FCCL）的51%股權，再視乎FCCL到2020年的表現，額外提供25.5億至127.5億日圓的收購費用。日本政策投資銀行則投放25億日圓收購該公司5%的股份。交易預料在2018年第一季完成。\[4\]

Shiodome_City_Center_2012.JPG|東京總部
NTT_docomo_F-10A_photo_b.jpg|docomo F-10A手機 Fujitsu Siemens Primergy
Servers.jpeg| Primergy 伺服器 2008Computex Fujitsu-Siemens Amilo Notebook
XA3530.jpg|XA3530筆電 FujitsuSiemens-Celvin-1.jpg|富士西門子桌上電腦 Pocket LOOX
600.jpg|LOOX 600掌上機 Conditionneur d'air à Velleron 2.JPG|富士通空調
FDK製の富士通乾電池 FDK Battery.JPG|富士通乾電池 Fujitsu AOpen DSD 46-1P
at Gangqian Station 20130928.jpg|公共場所用DSD Fujitsu DL3300 dot matrix
printer.JPG|DL3300點陣印表機 3point5 inch MO cartridge.jpg|富士通磁光碟

## 海外公司

  - 香港商富士通電子亞太有限公司臺灣分公司
      - [台北市信義區市民大道](../Page/台北市.md "wikilink")6段288號8F之7（[松山車站](../Page/松山車站_\(台灣\).md "wikilink")[潤泰松山車站大樓B棟](../Page/潤泰松山車站大樓.md "wikilink")）
  - 台灣富士通股份有限公司
      - 台北市[中正區中華路一段](../Page/中正區_\(臺北市\).md "wikilink")39號19F
  - 台灣富士電化股份有限公司
      - 台北市中正區重慶南路一段57號8F之4
  - 台灣富士通科技服務股份有限公司
      - [新北市](../Page/新北市.md "wikilink")[中和區板南路](../Page/中和區.md "wikilink")653號7F
  - 富晶通科技股份有限公司
      - [桃園市](../Page/桃園市.md "wikilink")[龜山區](../Page/龜山區.md "wikilink")（[華亞科技園區](../Page/華亞科技園區.md "wikilink")）華亞三路50號3F
  - 台灣富士通將軍國際股份有限公司
      - [台中市](../Page/台中市.md "wikilink")[北屯區崇德路](../Page/北屯區.md "wikilink")2段416號4F之1

## 参考文献

## 外部連結

  - [台灣富士通](http://www.fujitsu.com/tw/)

  - [Fujitsu
    Taiwan](https://www.facebook.com/fujitsu.taiwan/)的Facebook專頁

  -
  -
  - [Fujitsu全球網站](http://www.fujitsu.com/global/)

  - [公司歷史](http://pr.fujitsu.com/en/profile/history/hist1.html)

  - [富士通的沿革日文網頁](http://jp.fujitsu.com/about/corporate/history/)

  - [日本富士通](http://www.fujitsu.com/jp/)

  -
  -
  -
  - [富士通中國](http://www.fujitsu.com/cn/)

## 参见

  - [Lifebook](../Page/Lifebook.md "wikilink")
  - [川崎前鋒](../Page/川崎前鋒.md "wikilink")
  - [FM Towns](../Page/FM_Towns.md "wikilink")
  - [富士通杯世界職業圍棋錦標賽](../Page/富士通杯世界職業圍棋錦標賽.md "wikilink")
  - [全球二十大半导体厂商](../Page/全球二十大半导体厂商.md "wikilink")

[富士通](../Category/富士通.md "wikilink")
[Category:1935年日本建立](../Category/1935年日本建立.md "wikilink")
[Category:云计算提供商](../Category/云计算提供商.md "wikilink")
[Category:東京證券交易所上市公司](../Category/東京證券交易所上市公司.md "wikilink")
[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:電腦儲存媒體公司](../Category/電腦儲存媒體公司.md "wikilink")
[Category:日本軍事工業](../Category/日本軍事工業.md "wikilink")
[Category:显示科技公司](../Category/显示科技公司.md "wikilink")
[Category:1935年成立的电子公司](../Category/1935年成立的电子公司.md "wikilink")
[Category:日本電子公司](../Category/日本電子公司.md "wikilink")
[Category:古河集團](../Category/古河集團.md "wikilink")
[Category:信息技术咨询公司](../Category/信息技术咨询公司.md "wikilink")
[Category:暖通空调制造公司](../Category/暖通空调制造公司.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:东京制造公司](../Category/东京制造公司.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:上网本制造商](../Category/上网本制造商.md "wikilink")
[Category:销售点公司](../Category/销售点公司.md "wikilink")
[Category:半導體公司](../Category/半導體公司.md "wikilink")
[Category:日本半导体公司](../Category/日本半导体公司.md "wikilink")
[Category:东京软件公司](../Category/东京软件公司.md "wikilink")
[Category:日本科技公司](../Category/日本科技公司.md "wikilink")
[Category:东京电信公司](../Category/东京电信公司.md "wikilink")

1.
2.  <http://jp.fujitsu.com/group/fsl/en/release/20140731.html>
3.
4.