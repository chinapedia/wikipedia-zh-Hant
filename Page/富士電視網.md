**富士電視網**（，，簡稱：**FNS**），是[日本一個以](../Page/日本.md "wikilink")[富士電視台和](../Page/富士電視台.md "wikilink")[關西電視台為核心的](../Page/關西電視台.md "wikilink")[電視聯播網](../Page/電視聯播網.md "wikilink")，又名**富士電視系**（），主要提供[新聞節目以外的](../Page/新聞節目.md "wikilink")[電視節目聯播服務](../Page/電視節目.md "wikilink")。目前共有28個加盟台。

## 加盟局

<table style="width:70%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 6%" />
<col style="width: 13%" />
<col style="width: 38%" />
</colgroup>
<thead>
<tr class="header">
<th><p>放送地區</p></th>
<th><p>簡稱/頻道</p></th>
<th><p>電視台名稱</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p><strong>uhb 8</strong></p></td>
<td><p><a href="../Page/北海道文化放送.md" title="wikilink">北海道文化放送</a></p></td>
<td><p><small>1972年4月開播。<br />
</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岩手縣.md" title="wikilink">岩手縣</a></p></td>
<td><p><strong>mit 8</strong></p></td>
<td><p><a href="../Page/岩手可愛電視台.md" title="wikilink">岩手可愛電視台</a></p></td>
<td><p><small>1991年4月開播。</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p><strong>OX 8</strong></p></td>
<td><p><a href="../Page/仙台放送.md" title="wikilink">仙台放送</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a></p></td>
<td><p><strong>AKT 8</strong></p></td>
<td><p><a href="../Page/秋田電視台.md" title="wikilink">秋田電視台</a></p></td>
<td><p><small>1969年12月開播。</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><strong>SAY 8</strong></p></td>
<td><p><a href="../Page/櫻桃電視台.md" title="wikilink">櫻桃電視台</a></p></td>
<td><p><small>1997年4月開播。<br />
1970年4月（開播）自1993年3月<a href="../Page/山形電視台.md" title="wikilink">山形電視台是加盟台</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td><p><strong>FTV 8</strong></p></td>
<td><p><a href="../Page/福島電視台.md" title="wikilink">福島電視台</a></p></td>
<td><p><small>1963年4月開播。<br />
1971年10月加入，1983年4月從<a href="../Page/日本新聞網_(TBS).md" title="wikilink">JNN轉為</a><a href="../Page/富士新聞網.md" title="wikilink">FNN</a></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/關東廣域圈.md" title="wikilink">關東廣域圈</a></p></td>
<td><p><strong>CX 8</strong></p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><small><a href="../Page/核心局.md" title="wikilink">核心局</a></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><strong>NST 8</strong></p></td>
<td><p><a href="../Page/新潟綜合電視台.md" title="wikilink">新潟綜合電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><strong>NBS 8</strong></p></td>
<td><p><a href="../Page/長野放送.md" title="wikilink">長野放送</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靜岡縣.md" title="wikilink">靜岡縣</a></p></td>
<td><p><strong>SUT 8</strong></p></td>
<td><p><a href="../Page/靜岡電視台.md" title="wikilink">靜岡電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中京廣域圈.md" title="wikilink">中京廣域圈</a></p></td>
<td><p><strong>THK 1</strong></p></td>
<td><p><a href="../Page/東海電視台.md" title="wikilink">東海電視台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><strong>BBT 8</strong></p></td>
<td><p><a href="../Page/富山電視台.md" title="wikilink">富山電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td><p><strong>ITC 8</strong></p></td>
<td><p><a href="../Page/石川電視台.md" title="wikilink">石川電視台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p><strong>FTB 8</strong></p></td>
<td><p><a href="../Page/福井電視台.md" title="wikilink">福井電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/近畿廣域圏.md" title="wikilink">近畿廣域圏</a></p></td>
<td><p><strong>KTV 8</strong></p></td>
<td><p><a href="../Page/關西電視台.md" title="wikilink">關西電視台</a></p></td>
<td><p><small><a href="../Page/準核心局.md" title="wikilink">準核心局</a><br />
2007年4月，因《<a href="../Page/發掘！真有其事大百科.md" title="wikilink">發掘！真有其事大百科</a>Ⅱ》造假事件而被<a href="../Page/民放聯.md" title="wikilink">民放聯除名</a>。（后恢复资格）</small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鳥取縣.md" title="wikilink">鳥取縣</a></p></td>
<td><p><strong>TSK 8</strong></p></td>
<td><p><a href="../Page/山陰中央電視台.md" title="wikilink">山陰中央電視台</a></p></td>
<td><p><small>1970年4月開播。</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/島根縣.md" title="wikilink">島根縣</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岡山縣.md" title="wikilink">岡山縣</a></p></td>
<td><p><strong>OHK 8</strong></p></td>
<td><p><a href="../Page/岡山放送.md" title="wikilink">岡山放送</a></p></td>
<td><p><small>1969年4月1日開播，當天加盟FNS</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廣島縣.md" title="wikilink">廣島縣</a></p></td>
<td><p><strong>TSS 8</strong></p></td>
<td><p><a href="../Page/新廣島電視台.md" title="wikilink">新廣島電視台</a></p></td>
<td><p><small>1975年10月1日開播，當天就加盟FNS和<a href="../Page/富士新聞網.md" title="wikilink">FNN</a></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛媛縣.md" title="wikilink">愛媛縣</a></p></td>
<td><p><strong>EBC 8</strong></p></td>
<td><p><a href="../Page/愛媛電視台.md" title="wikilink">愛媛電視台</a></p></td>
<td><p><small>1969年12月開播。</small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高知縣.md" title="wikilink">高知縣</a></p></td>
<td><p><strong>KSS 8</strong></p></td>
<td><p><a href="../Page/高知SunSun電視台.md" title="wikilink">高知SunSun電視台</a></p></td>
<td><p><small>1997年4月開播。</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td><p><strong>TNC 8</strong></p></td>
<td><p><a href="../Page/西日本電視台.md" title="wikilink">西日本電視台</a></p></td>
<td><p><small>1958年8月28日開播。</small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td><p><strong>STS 3</strong></p></td>
<td><p><a href="../Page/佐賀電視台.md" title="wikilink">佐賀電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td><p><strong>KTN 8</strong></p></td>
<td><p><a href="../Page/長崎電視台.md" title="wikilink">長崎電視台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a></p></td>
<td><p><strong>TKU 8</strong></p></td>
<td><p><a href="../Page/熊本電視台.md" title="wikilink">熊本電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大分縣.md" title="wikilink">大分縣</a></p></td>
<td><p><strong>TOS 4</strong></p></td>
<td><p><a href="../Page/大分電視台.md" title="wikilink">大分電視台</a></p></td>
<td><p><small>1970年4月開播。同時加入<a href="../Page/日本電視台.md" title="wikilink">日本電視台聯播網</a></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p><strong>UMK 3</strong></p></td>
<td><p><a href="../Page/宮崎電視台.md" title="wikilink">宮崎電視台</a></p></td>
<td><p><small>1970年4月開播。同時加入<a href="../Page/日本電視台.md" title="wikilink">日本電視台和</a><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台聯播網</a></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><strong>KTS 8</strong></p></td>
<td><p><a href="../Page/鹿兒島電視台.md" title="wikilink">鹿兒島電視台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖繩縣.md" title="wikilink">沖繩縣</a></p></td>
<td><p><strong>OTV 8</strong></p></td>
<td><p><a href="../Page/沖繩電視台.md" title="wikilink">沖繩電視台</a></p></td>
<td><p><small>正式加盟日期為1972年5月15日</small></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[FNS](../Category/日本地面電視.md "wikilink")
[Category:富士电视台](../Category/富士电视台.md "wikilink")
[Category:電視網](../Category/電視網.md "wikilink")
[Category:1969年建立](../Category/1969年建立.md "wikilink")
[日](../Category/日本公司列表.md "wikilink")