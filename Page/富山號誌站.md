[TRA_Fushan_station.JPG](https://zh.wikipedia.org/wiki/File:TRA_Fushan_station.JPG "fig:TRA_Fushan_station.JPG")
[fushanStation.jpg](https://zh.wikipedia.org/wiki/File:fushanStation.jpg "fig:fushanStation.jpg")
**富山號誌站**位於[臺灣](../Page/臺灣.md "wikilink")[臺東縣](../Page/臺東縣.md "wikilink")[大武鄉](../Page/大武鄉_\(台灣\).md "wikilink")，曾為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[南迴線的鐵路號誌站](../Page/南迴線.md "wikilink")。

## 車站構造

  - 岸式月台兩座。
  - 因通車後運量不大，車次不多，海側軌道在南迴線通車後不久即拆除，僅保留一股通過線。

## 利用狀況

  - 目前已廢除。

## 車站周邊

  - 富山部落舊址（[八八水災後已遷大武國小舊址](../Page/八八水災.md "wikilink")，並正名[斯卡拉比部落](../Page/斯卡拉比部落.md "wikilink")）
  - [太平洋](../Page/太平洋.md "wikilink")。
  - 距[TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[台9線](../Page/台9線.md "wikilink")426.5公里岔路口約
    250 公尺。

## 公車資訊

由號誌站聯絡道路走到台9線路口即有站牌，站牌為富山

  - [鼎東客運](../Page/鼎東客運.md "wikilink")(山線)：
      - 8132 台東－安朔國小
      - 8135 台東－大南－安朔
      - 8136 台東－豐源－安朔
      - 8137 台東－大南－尚武
      - 8156 安朔－大竹
      - 8158 金崙－安朔

## 歷史

  - 原計畫為[號誌站](../Page/號誌站.md "wikilink")，但1991年[南迴線通車時並未實際使用](../Page/南迴線.md "wikilink")\[1\]。
  - [屏東車站曾短暫販賣過至此站之普通](../Page/屏東車站.md "wikilink")、快車通用[名片式車票](../Page/埃德蒙森式鐵路車票.md "wikilink")，被[鐵道迷視為珍品](../Page/鐵道迷.md "wikilink")。
  - 台鐵曾安排郵輪式列車於富山站臨停觀光\[2\]。

## 鄰近車站

## 參考

[廢](../Category/南迴線車站.md "wikilink")
[Category:台東縣交通](../Category/台東縣交通.md "wikilink")
[南](../Category/台灣鐵路廢站.md "wikilink")
[Category:台灣鐵路號誌站](../Category/台灣鐵路號誌站.md "wikilink")

1.
2.