[Cosco_Tower_2011.jpg](https://zh.wikipedia.org/wiki/File:Cosco_Tower_2011.jpg "fig:Cosco_Tower_2011.jpg")\]\]
**富昌金融集團**（英文：**Fulbright Financial
Group**）成立於1999年，總部設在香港上環[中遠大廈](../Page/中遠大廈.md "wikilink")。屬於大型金融集團，旗下全資子公司包括[富昌證券有限公司](../Page/富昌證券.md "wikilink")、[富昌期貨有限公司](../Page/富昌期貨.md "wikilink")、[富昌金業有限公司及](../Page/富昌金業.md "wikilink")[富昌財務有限公司](../Page/富昌財務.md "wikilink")。

目前集團主席為[郭俊偉先生](../Page/郭俊偉.md "wikilink")。

[香港交易所經紀牌照號碼為](../Page/香港交易所.md "wikilink")5130, 5131, 5135, 5137, 5138,
5139, 6078, 6079, 9000, 9001, 9004, 9005, 9007, 9008, 9009, 9015, 9016,
9018, 9019, 9070, 9071, 9072, 9076, 9077, 9079, 9081, 9083, 9084, 9085,
9086, 9087, 9089, 9094, 9095, 9097, 9098, 9126, 9127, 9128。\[1\]

## 子公司

  - [富昌證券有限公司](../Page/富昌證券.md "wikilink")
  - [富昌期貨有限公司](../Page/富昌期貨.md "wikilink")
  - [富昌金業有限公司](../Page/富昌金業.md "wikilink")

## 外部链接

  - [富昌金融](http://www.fulbright.com.hk)

[Category:投资银行](../Category/投资银行.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:香港金融公司](../Category/香港金融公司.md "wikilink")
[Category:1999年成立的公司](../Category/1999年成立的公司.md "wikilink")
[Category:郭英成家族](../Category/郭英成家族.md "wikilink")

1.  <https://www.hkex.com.hk/chi/plw/SearchParticipant_c.aspx>