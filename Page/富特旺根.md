[Furtwangen_Schwarzwald_-_Black_forest.jpg](https://zh.wikipedia.org/wiki/File:Furtwangen_Schwarzwald_-_Black_forest.jpg "fig:Furtwangen_Schwarzwald_-_Black_forest.jpg")
**黑林山区富特旺根**（Furtwangen im
Schwarzwald），通称**富特旺根**，是位於[德國西南部](../Page/德國.md "wikilink")[黑森林地區的一個小城市](../Page/黑森林.md "wikilink")，隸屬於[巴登-符腾堡州內的](../Page/巴登-符腾堡州.md "wikilink")[黑林山-巴尔县](../Page/黑林山-巴尔县.md "wikilink")。总面积82.57平方公里，总人口9250人，其中男性4824人，女性4426人（2011年12月31日），人口密度112人/平方公里。

富特旺根位於黑森林地區的中部，[弗赖堡東面約](../Page/弗赖堡_\(巴登-符腾堡州\).md "wikilink")27[公里處](../Page/公里.md "wikilink")。富特旺根的[海拔高度介乎](../Page/海拔.md "wikilink")850[米至](../Page/米_\(單位\).md "wikilink")1,150米之間，是巴登-符腾堡州海拔最高的城市，也是德國海拔第二高的城市。一條稱為[布雷格河的小溪](../Page/布雷格河.md "wikilink")，自圍繞富特旺根的山區出發，穿越富特旺根內城向東流動。布雷河是匯集成[多瑙河的兩條小溪的其中之一](../Page/多瑙河.md "wikilink")。

截至2006年12月31日，富特旺根有9463名居民。自1873年起，富特旺根擁有被稱為城市的權利。

市內有一所高等院校，[富特旺根应用科学大学](../Page/富特旺根应用科学大学.md "wikilink")。該大学擁有[微電子學](../Page/微電子學.md "wikilink")、精密機械、[電腦科學和其他的學院](../Page/電腦科學.md "wikilink")。學院的其中一個入口旁邊是1852年創設的德國鐘錶博物館（Deutsches
Uhrenmuseum），展出過去數世紀至現代的[鐘錶產品](../Page/鐘錶.md "wikilink")。

## 参考

## 外部連結

  - [富特旺根政府官方網頁](http://www.furtwangen.de) （德文）
  - [德國鐘錶博物館官方網頁](http://www.deutsches-uhrenmuseum.de/) （德文、英文、法文）

[F](../Category/巴登-符腾堡州市镇.md "wikilink")