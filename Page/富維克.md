**富維克**是一個天然[礦泉水品牌](../Page/礦泉水.md "wikilink")，水源來自[法國](../Page/法國.md "wikilink")[中央高原圓頂火山](../Page/中央高原.md "wikilink")[多姆山正北面的](../Page/多姆山.md "wikilink")[奧弗涅地區火山自然公園](../Page/奧弗涅地區火山自然公園.md "wikilink")。該礦泉水內含的獨特礦物來自公元[前5760年鄰近](../Page/前六千年.md "wikilink")[火山的最後一次爆發](../Page/火山.md "wikilink")；以[地質學角度來說非常近期](../Page/地質學.md "wikilink")。

1922年**富維克**[鎮首次利用在古雷山谷的溫泉水](../Page/市鎮_\(法國\).md "wikilink")，1938年首支富維克樽裝水面世，現今富維克礦泉水已國際知名。生產由兩家裝瓶廠負責，是富維克鎮的主要雇主。

富維克每年生產超過10億瓶樽裝水，其中超過50%會運送到全球60多個國家銷售。

1993年富維克被[達能集團收購](../Page/達能.md "wikilink")。

富維克聲稱非常注重[環境並帶領對環境有益的新改革](../Page/自然環境.md "wikilink")，1997年富維克開始使用可[回收再用的物料](../Page/回收.md "wikilink")[聚對苯二甲酸乙二酯來製造瓶子](../Page/聚對苯二甲酸乙二酯.md "wikilink")。

富維克同時有在歐洲出產一系列名為Touch of
Fruit的果汁飲品，共有四種口味(檸檬及酸橙、芒果、草苺與最近推出的熱帶味)，2005年入口到北美並以*Volvic
natural Lemon*與*Volvic natural Orange*品牌出售。

富维克其他产品线包括 Volvic Juiced (添加水果浓缩汁）以及Volvic Sparkling （果味气泡水，类似于Touch of
Fruit)。

## 外部連結

  - [富維克英國網站](http://www.volvic.co.uk/)
  - [富維克北美網站](http://www.volvic-na.com/)

[Category:達能品牌](../Category/達能品牌.md "wikilink")
[Category:瓶裝水品牌](../Category/瓶裝水品牌.md "wikilink")
[Category:礦泉水](../Category/礦泉水.md "wikilink")
[Category:法國品牌](../Category/法國品牌.md "wikilink")