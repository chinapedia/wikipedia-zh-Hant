[Regal_Hong_Kong_Hotel.jpg](https://zh.wikipedia.org/wiki/File:Regal_Hong_Kong_Hotel.jpg "fig:Regal_Hong_Kong_Hotel.jpg")
**富豪酒店國際控股有限公司**（）是一家在[香港交易所上市的](../Page/香港交易所.md "wikilink")[酒店公司](../Page/酒店.md "wikilink")。主要業務是持有與管理酒店、物業發展及投資，及其他投資（包括金融票據及有價證券之投資及買賣）。

富豪酒店國際控股，曾在1979年，由[鷹君集團有限公司分拆成立](../Page/鷹君集團有限公司.md "wikilink")，現在於[百慕達註冊](../Page/百慕達.md "wikilink")，以及由[世紀城市國際控股有限公司旗下公司](../Page/世紀城市國際控股有限公司.md "wikilink")，[主席是](../Page/主席.md "wikilink")[羅旭瑞](../Page/羅旭瑞.md "wikilink")。
\[1\]

## 酒店

### [香港](../Page/香港.md "wikilink")

  - [富豪機場酒店](../Page/富豪機場酒店.md "wikilink")
  - [富豪香港酒店](../Page/富豪香港酒店.md "wikilink")
  - [富豪九龍酒店](../Page/富豪九龍酒店.md "wikilink")
  - [富豪東方酒店](../Page/富豪東方酒店.md "wikilink")
  - [麗豪酒店](../Page/麗豪酒店.md "wikilink")
  - [富薈炮台山酒店](../Page/富薈炮台山酒店.md "wikilink")
  - [富薈馬頭圍酒店](../Page/富薈馬頭圍酒店.md "wikilink")
  - [富薈上環酒店](../Page/富薈上環酒店.md "wikilink")
  - [富薈灣仔酒店](../Page/富薈灣仔酒店.md "wikilink")

<!-- end list -->

  - 2017年2月3日富豪酒店以21億8890萬元
    中標香港國際機場SKYCITY[航天城第A](../Page/航天城.md "wikilink")1a號，佔地7萬1580平方呎，許可建築面積36萬2746平方呎，將發展成一座提供超過1000間客房及套房的酒店項目。

### [中國](../Page/中國.md "wikilink")

  - [上海](../Page/上海.md "wikilink")[富豪環球東亞酒店](../Page/富豪環球東亞酒店.md "wikilink")
  - [上海](../Page/上海.md "wikilink")[富豪東亞酒店](../Page/富豪東亞酒店.md "wikilink")
  - [上海](../Page/上海.md "wikilink")[富豪金豐酒店](../Page/富豪金豐酒店.md "wikilink")
  - [山東](../Page/山東.md "wikilink")[德州富豪康博酒店](../Page/德州富豪康博酒店.md "wikilink")（於2010年開業）
  - [富豪新都酒店](../Page/富豪新都酒店.md "wikilink")
  - 西安機場富豪酒店
  - [廣東](../Page/廣東.md "wikilink")[佛山金融城富豪酒店](../Page/佛山金融城富豪酒店.md "wikilink")

## 相關

  - [富豪產業信託](../Page/富豪產業信託.md "wikilink")
  - [世紀城市國際控股](../Page/世紀城市國際控股.md "wikilink")
  - [百利保控股](../Page/百利保控股.md "wikilink")

## 外部連結

  - [富豪國際酒店集團](http://www.regalhotel.com/)

## 参考文献

<div class="references-small">

<references />

</div>

[分類:羅鷹石家族](../Page/分類:羅鷹石家族.md "wikilink")

[Category:香港酒店連鎖集團](../Category/香港酒店連鎖集團.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市酒店公司](../Category/香港上市酒店公司.md "wikilink")
[Category:旅館品牌](../Category/旅館品牌.md "wikilink")
[Category:1979年成立的公司](../Category/1979年成立的公司.md "wikilink")

1.  [**富豪酒店國際控股**投資者資料](http://info.regalhotel.com/c_main.htm)