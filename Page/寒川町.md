**寒川町**（）位於[日本](../Page/日本.md "wikilink")[神奈川县中部](../Page/神奈川县.md "wikilink")。

## 地理

位於相模川下流東側，大概是平原台地也能看。

  - 河川:
    [相模川](../Page/相模川.md "wikilink")、[目久尻川](../Page/目久尻川.md "wikilink")、[小出川](../Page/小出川.md "wikilink")

## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [相模線](../Page/相模線.md "wikilink")：[寒川站](../Page/寒川站.md "wikilink") -
    [宮山站](../Page/宮山站.md "wikilink") - [倉見站](../Page/倉見站.md "wikilink")

## 主要观光点

[Cetral_Park_in_Samukawa.jpg](https://zh.wikipedia.org/wiki/File:Cetral_Park_in_Samukawa.jpg "fig:Cetral_Park_in_Samukawa.jpg")

  - [相模国一之宮](../Page/相模国.md "wikilink")[寒川神社](../Page/寒川神社.md "wikilink")
  - [相模国一之宮](../Page/相模国.md "wikilink")[八幡大神](../Page/八幡大神.md "wikilink")
  - 梶原景時館跡