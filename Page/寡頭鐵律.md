**寡頭鐵律**（Iron law of
oligarchy）是一項[政治學和](../Page/政治學.md "wikilink")[社會學理論](../Page/社會學.md "wikilink")，1911年由[羅伯特·米契爾斯在其出版发表的](../Page/羅伯特·米契爾斯.md "wikilink")[Political
Parties](https://en.wikipedia.org/wiki/Political_Parties_\(book\))一书中针对[代议民主制提出的其致命缺陷是不稳定会退化成](../Page/代议民主制.md "wikilink")[寡头政治](../Page/寡头政治.md "wikilink")，研究[政黨和](../Page/政黨.md "wikilink")[人群組織都會提及](../Page/人群組織.md "wikilink")。

## 內容

[羅伯特·米契爾斯以觀察歐洲各](../Page/羅伯特·米契爾斯.md "wikilink")[社會主義](../Page/社會主義.md "wikilink")[政黨](../Page/政黨.md "wikilink")（尤其是[德國社會民主黨](../Page/德國社會民主黨.md "wikilink")）的黨內生態，提出下列幾項觀點：

  - [組織不論多民主](../Page/组织_\(社会学\).md "wikilink")，最後決定權都只在少數幾個人手中。
  - 組織越大，[权力](../Page/权力.md "wikilink")[核心離](../Page/核心.md "wikilink")[群眾越遠](../Page/大众.md "wikilink")。
  - 領導人產生方法越來越不[民主](../Page/民主.md "wikilink")。
  - 領導人越來越執著權力。
  - 領導階層生活方式[布爾喬亞化](../Page/布爾喬亞.md "wikilink")，和一般民眾脫鉤。
  - 領導階層思想行為保守化。

## 反驳

1943年，瑞士历史学家Adolf Gasser出版发表了“Gemeindefreiheit als Rettung
Europas”一书，通过研究全球多个已经长期形成稳定的代议制民主的国家，指出符合以下特点的代议民主制会打破寡头铁律而变得相对稳定：

  - 社会必须以自下而上的体制建立。推论：社会必须由公民建立，这些公民是自由民并且持有武器可以保卫自身的合法利益。 Society has
    to be built up from bottom to top. As a consequence, society is
    built up by people, who are free and have the power to defend
    themselves with weapons.
  - 这些自由公民加入并组合成为本地社区。这些本地社区是独立的，包括财政独立，自行定义政治游戏规则不受他人驱使。These free
    people join or form local communities. These local communities are
    independent, which includes financial independence, and they are
    free to determine their own rules.
  - 同理类推，本地社区加入并组合成为更高级的政治单元，如市和邦(美国的邦通常中译为州)。 Local communities join
    together into a higher unit e.g. a municipal, a state.
  - 本地社区、本市、本邦和国家级联邦之间不存在类似倒扣树的政治上的下级隶属上级的等级关系。There is no hierarchical
    bureaucracy.
  - 不同社区、不同市、不同邦之间有竞争关系，自由公民可以自由迁徙选择其所在社区、市或邦来挑选自己看得上的地方赋予的政治权利和税率等。There
    is competition between these local communities e.g. on services
    delivered or on taxes.

## 參見

  - [團體迷思](../Page/團體迷思.md "wikilink")
  - [組織病象](../Page/組織病象.md "wikilink")

[Category:社會學理論](../Category/社會學理論.md "wikilink")
[Category:政治學](../Category/政治學.md "wikilink")