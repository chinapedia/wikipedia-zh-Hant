**實驗室設備**是指在[實驗室裡工作人士用的各種各樣的](../Page/實驗室.md "wikilink")[工具和](../Page/工具.md "wikilink")[設備](../Page/設備.md "wikilink")。實驗室設備一般是用作進行實驗或作為測量，和收集資料。**實驗室設備**依實驗室的種類不同，而有不同的設備。

## 化學實驗室

這些工具包括常見的[本生燈](../Page/本生燈.md "wikilink")，[顯微鏡](../Page/顯微鏡.md "wikilink")，還有一些專業設備如[分光光度計和](../Page/分光光度計.md "wikilink")[熱量計等](../Page/熱量計.md "wikilink")。除此之外还有试管，量筒，烧杯。

## 醫學實驗室

[醫學實驗室中使用的工具](../Page/醫學實驗室中使用的工具.md "wikilink")

## 電子電路實驗室

\[1\]

  - 電源供應器（直流、交流）
  - [示波器](../Page/示波器.md "wikilink")
  - [訊號產生器](../Page/訊號產生器.md "wikilink")
  - 數位電錶
  - [面包板](../Page/面包板.md "wikilink")

## 材料實驗室

根據實驗的目的與被實驗的材料之特性，而有不同的設備：

1.  土木工程之材料實驗室\[2\]
2.  奈米材料實驗室\[3\]
3.  磁性材料實驗室\[4\]

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [實驗室設備](https://web.archive.org/web/20021017004400/http://www.geocities.com/~chemfun/unit1/labequipment/equipment.html)


[實驗室設備](../Category/實驗室設備.md "wikilink")

1.  [國立彰化師範大學電信工程研究所](http://eedept.ncue.edu.tw/telecommunications/) -
    [實驗室設備](http://eedept.ncue.edu.tw/telecommunications/picture-2.html)
2.  [臺北科技大學土木與防災研究所 材料實驗室](http://www.ce.ntut.edu.tw/bin/home.php) -
    [實驗室設備](http://www.ce.ntut.edu.tw/files/11-1046-1953.php)
3.  [大同大學 材料工程學系
    奈米材料研究室](http://www.mse.ttu.edu.tw/content/labs/nanolab/)
     -
    [實驗室設備](http://www.mse.ttu.edu.tw/content/labs/nanolab/chinese/facility1.html)

4.  [台大材料科學與工程學研究所 磁性材料實驗室](http://www.mse.ntu.edu.tw/~mag/index.html) -
    [實驗室設備](http://www.mse.ntu.edu.tw/~mag/photo.htm)