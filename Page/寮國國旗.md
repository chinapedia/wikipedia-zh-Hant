现行**老撾國旗**于1975年共产主义者上台后启用，由[紅色](../Page/紅色.md "wikilink")、[藍色和](../Page/藍色.md "wikilink")[白色组成](../Page/白色.md "wikilink")，藍色代表老挝富饒美麗的國土，以及老挝人民熱愛和平安寧的生活。[紅色象徵革命](../Page/紅色.md "wikilink")，表明老挝人民不惜以鮮血為代價捍衛國家尊嚴。[白色圓形象徵老撾人民在](../Page/白色.md "wikilink")[老挝人民革命党的領導下團結一致以及國家光明的未來](../Page/老挝人民革命党.md "wikilink")。白色圓形也代表滿月，置於藍條之上，象徵皎潔明月高懸於[湄公河的上空](../Page/湄公河.md "wikilink")。

[Flag_of_Laos.jpg](https://zh.wikipedia.org/wiki/File:Flag_of_Laos.jpg "fig:Flag_of_Laos.jpg")

## 历史国旗

老挝保护国国旗是在老挝法国殖民统治时期使用的一面国旗。左上角的法国国旗象征着法国与老挝保护国的联系。红色的旗底象征着人民的[血液](../Page/血液.md "wikilink")。图像为三只大象站在撑有阳伞的五阶台座上。大象象征着尊贵、智慧、吉祥，并作为一种交通工具。同时，三只大象还象征着老挝历史上的三个古国：[万象](../Page/万象.md "wikilink")、[琅勃拉邦和](../Page/琅勃拉邦.md "wikilink")[川圹王国](../Page/川圹王国.md "wikilink")。位于大象之上的伞也有其确切的含义，在佛教的宇宙哲学中，须弥山（Mt.
Meru）为世界的中心，此伞象征须弥山，代表了老挝的国民信仰。大象所站之台基象征着老挝王国的法律。

老挝王国国旗，在老挝君主制政权时期使用的一面国旗，去掉了旗帜左上角的法国国旗。

<File:Flag> of the Kingdom of Luang Phrabang
(1707-1893).svg|[琅勃拉邦王国](../Page/琅勃拉邦王国.md "wikilink")（1707-1893）
Flag of French Laos.svg| [老挝保护国](../Page/老挝保护国.md "wikilink")
（1893-1952） Flag of Laos (1952-1975).svg|
[老挝王国](../Page/老挝王国.md "wikilink") (1952-1975) Flag of
Laos.svg|[老挝人民民主共和国](../Page/老挝人民民主共和国.md "wikilink")（1975至今）

### 其他旗帜

Flag of LPRP.svg|[老挝人民革命党党旗](../Page/老挝人民革命党.md "wikilink") Flag of
LPRYU.svg|[老挝人民革命青年联盟盟旗](../Page/老挝人民革命青年联盟.md "wikilink") Flag of
Laos.svg|[老挝人民军军旗](../Page/老挝人民军.md "wikilink") Royal Standard of the
Kingdom of Laos.svg|老挝王国军旗（1952-1975）

### 老挝历史上王国旗帜

<File:Flag> of the Kingdom of Vientiane (1707 -
1828).svg|[万象王国](../Page/万象王国.md "wikilink") (1707–1828)
<File:Flag> of the Kingdom of Luang Phrabang
(1707-1893).svg|[琅勃拉邦王国](../Page/琅勃拉邦王国.md "wikilink")
(1707–1893) <File:Flag> of the Kingdom of Champasak
(1713-1947).svg|[占巴塞王国](../Page/占巴塞王国.md "wikilink") (1707–1946)

### 相似的旗帜

<File:Belizean> Air Force
Roundel.svg|[伯利兹空军旗帜，与老挝国旗相似](../Page/伯利兹国旗.md "wikilink")

## 參見

  - [寮國](../Page/寮國.md "wikilink")
  - [國旗](../Page/國旗.md "wikilink")
  - [國徽](../Page/國徽.md "wikilink")
  - [老挝国徽](../Page/老挝国徽.md "wikilink")

[Category:寮國](../Category/寮國.md "wikilink")
[L](../Category/國旗.md "wikilink")
[Category:亞洲國旗](../Category/亞洲國旗.md "wikilink")
[Category:1975年面世的旗幟](../Category/1975年面世的旗幟.md "wikilink")