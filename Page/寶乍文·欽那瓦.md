**寶乍文·欽那瓦**（，[RTGS](../Page/皇家泰语转写通用系统.md "wikilink")：Photchaman
Chinnawat，），本名**寶乍文·達瑪蓬**(，[RTGS](../Page/皇家泰语转写通用系统.md "wikilink")：Photchaman
Damaphong），生於[泰國北部富有華商之家](../Page/泰國.md "wikilink")，是前[泰國首相](../Page/泰國首相.md "wikilink")[塔克辛·欽那瓦的前妻](../Page/塔克辛·欽那瓦.md "wikilink")。

婚前（1980年代），他信致力與警察局事業，九十年代又同時致力於創業與其政治生涯，據信寶乍文在[新信通公司](../Page/新信通公司.md "wikilink")（[Shin
Corp](../Page/:en:Shin_Corp.md "wikilink")）發展成為泰國第一大電訊公司的道路上中起過極其巨大的作用。

## 學業

曾就讀與[美國](../Page/美國.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[東肯塔基大學](../Page/東肯塔基大學.md "wikilink")，與後來的丈夫塔克辛是校友。獲得該校[文學碩士學位](../Page/文學碩士.md "wikilink")。

## 家庭

1976年回泰國與[塔克辛·欽那瓦結婚](../Page/塔克辛·欽那瓦.md "wikilink")(20歲)，現育有一子及二女。長子：[盼東泰](../Page/攀通泰·西那瓦.md "wikilink")([พานทองแท้
ชินวัตร](../Page/:th:พานทองแท้_ชินวัตร.md "wikilink")/[Panthongtae
Shinawatra](../Page/:en:Panthongtae_Shinawatra.md "wikilink"))，出生於1979年12月2日，現為泰國商人，二女：[萍通塔·西那瓦](../Page/萍通塔·西那瓦.md "wikilink")（Pinthongta
Shinawatra）與三女：[贝东丹·西那瓦](../Page/贝东丹·西那瓦.md "wikilink")（Paetongtarn
Shinawatra）均在[英國攻讀碩士學位](../Page/英國.md "wikilink")。

2008年，塔克辛夫婦流亡香港期間，在泰國領事館辦理了離婚手續。

## 法律糾紛

2003年，寶乍文從[泰國銀行所控制的](../Page/泰國銀行.md "wikilink")[金融機構發展基金](../Page/金融機構發展基金.md "wikilink")（FIDF；Finance
Institute Development
Fund）一次負債資產拍賣中，斥資二千多萬[美金](../Page/美金.md "wikilink")，購入[曼谷](../Page/曼谷.md "wikilink")[叻猜拉披色路繁華地段上的一塊國有土地](../Page/叻猜拉披色路.md "wikilink")\[1\]\[2\]。2006年8月，她因為財產申報不實、[利益衝突與](../Page/利益衝突.md "wikilink")[瀆職等罪名](../Page/瀆職.md "wikilink")，與[塔克辛同遭](../Page/塔克辛.md "wikilink")[泰國最高法院](../Page/泰國最高法院.md "wikilink")[通緝](../Page/通緝.md "wikilink")；若這三項「罪名」皆成立，她將被處以最高28年的[有期徒刑](../Page/有期徒刑.md "wikilink")。2008年1月8日，她搭乘[泰國航空班機](../Page/泰國航空.md "wikilink")，從[香港返回](../Page/香港.md "wikilink")[泰國](../Page/泰國.md "wikilink")，準備參加[泰王](../Page/泰王.md "wikilink")[蒲美蓬胞姊的](../Page/蒲美蓬.md "wikilink")[葬禮](../Page/葬禮.md "wikilink")，降落[曼谷素萬那普國際機場時遭](../Page/曼谷素萬那普國際機場.md "wikilink")[警方](../Page/警方.md "wikilink")[逮捕](../Page/逮捕.md "wikilink")，隨後以600萬[泰銖](../Page/泰銖.md "wikilink")[交保](../Page/交保.md "wikilink")。\[3\]

2006年9月19日深夜，曼谷發生[軍事政變](../Page/2006年泰國軍事政變.md "wikilink")，塔克辛被迫下野，然而寶乍文多數時間待在泰國照理生意及籌備一些法律事務。政變叛軍領導集團聲稱，寶乍文在該交易案中獲得[泰國財政部內幕消息與](../Page/泰國財政部.md "wikilink")「優先照顧」\[4\]\[5\]，而且該交易因她的丈夫當時是泰國首相而被誣陷違反泰國「反貪污法」。然而，監控那塊土地拍賣過程的財政部及央行則指出，該交易案完全按照法律程序進行。\[6\]

2008年7月31日，叛軍政府控制的法院認為寶乍文[逃稅罪名](../Page/逃稅.md "wikilink")「成立」，判監3年。[1](http://hk.news.yahoo.com/article/080731/4/7gdv.html)

## 請參照

  - [塔克辛·钦那瓦](../Page/塔克辛·钦那瓦.md "wikilink")
  - [東肯塔基大學](../Page/東肯塔基大學.md "wikilink")
    ([EKU](../Page/:en:Eastern_Kentucky_University.md "wikilink"))

## 參考資料

<references />

## 外部連結

  - [寶乍文夫人生平介紹](https://web.archive.org/web/20070716101249/http://www.onopen.com/2006/01/464)
    (泰文)
  - [寶乍文夫人 --
    一個甚有影響力的女人](http://www.positioningmag.com/magazine/details.aspx?id=50111)
    (泰文政治雜誌《Positioning Magazine》述評)

[Category:泰國華人](../Category/泰國華人.md "wikilink")
[Category:東肯塔基大學校友](../Category/東肯塔基大學校友.md "wikilink")
[Category:政治犯](../Category/政治犯.md "wikilink")

1.  [《泰國新聞社》報導](http://etna.mcot.net/query.php?nid=30080)
2.  [《美國之音》報導](http://voanews.com/english/2007-06-21-voa13.cfm)
3.  陳怡妏 綜合外電報導，〈泰涉貪前總理妻 返國被捕〉，[蘋果日報
    (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink")，2008年1月9日
4.  [《聯合早報》2007年6月19日報導](http://www.zaobao.com/yx/yx070619_508.html)
5.  [《英國廣播公司》2007年6月20日報導](http://news.bbc.co.uk/2/hi/in_depth/6224928.stm)
6.  [曼谷《國民報》（The Nation）2006年11月2日文，泰國銀行堅持交易是完全合法的-Pojaman purchase
    lawful:
    BOT](http://nationmultimedia.com/2006/11/02/politics/politics_30017804.php)