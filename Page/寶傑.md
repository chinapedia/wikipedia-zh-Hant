[Poljot.gif](https://zh.wikipedia.org/wiki/File:Poljot.gif "fig:Poljot.gif")
**寶傑**（，意為「飛行」），[俄羅斯鐘錶品牌](../Page/俄羅斯.md "wikilink")，由[莫斯科第一鐘錶廠生產](../Page/莫斯科第一鐘錶廠.md "wikilink")。[莫斯科第一鐘錶廠是](../Page/莫斯科第一鐘錶廠.md "wikilink")[蘇聯規模最大](../Page/蘇聯.md "wikilink")，專以生產高精密度的機械鐘錶，產品包括：機械[計時碼錶](../Page/計時碼錶.md "wikilink")、超薄[自動機械錶](../Page/自動機械錶.md "wikilink")、超薄手錶及航海鐘等。

寶傑的手錶使用[莫斯科第一鐘錶廠自產的手錶機芯為主](../Page/莫斯科第一鐘錶廠.md "wikilink")，亦採用[閃電牌的](../Page/閃電牌.md "wikilink")[懷錶機芯作為手錶使用](../Page/懷錶.md "wikilink")。但近年慢慢開始使用進口機芯，包括有瑞士[ETA的](../Page/ETA.md "wikilink")7750,
2824及2836等。

## 名稱的由來

1961年[蘇聯人](../Page/蘇聯.md "wikilink")[加加林成為世界第一個](../Page/加加林.md "wikilink")[太空人](../Page/太空人.md "wikilink")，而他升空時配帶的[手錶](../Page/手錶.md "wikilink")，就是[莫斯科第一鐘錶廠生產的](../Page/莫斯科第一鐘錶廠.md "wikilink")[Shturmanskie](../Page/Shturmanskie.md "wikilink")，因此[莫斯科第一鐘錶廠成為了世界第一所有能力生產](../Page/莫斯科第一鐘錶廠.md "wikilink")[航天手錶的廠商](../Page/航天手錶.md "wikilink")；自此以後，[莫斯科第一鐘錶廠生產的](../Page/莫斯科第一鐘錶廠.md "wikilink")[手錶](../Page/手錶.md "wikilink")，大部份都是以寶傑為品牌。

## 現在的寶傑

自1991年[蘇聯瓦解以後](../Page/蘇聯.md "wikilink")，寶傑分別由兩所不同的工廠所生產：分別有[烏克蘭的](../Page/烏克蘭.md "wikilink")[基輔鐘錶廠](../Page/基輔鐘錶廠.md "wikilink")（俗稱烏克蘭寶傑）及[莫斯科第一鐘錶廠](../Page/莫斯科第一鐘錶廠.md "wikilink")。但於2004年，[莫斯科第一鐘錶廠被分拆出售](../Page/莫斯科第一鐘錶廠.md "wikilink")，例如[俄羅斯的鐘錶生產商](../Page/俄羅斯.md "wikilink")[Maktime收購了](../Page/Maktime.md "wikilink")[3133系列](../Page/3133.md "wikilink")[計時碼錶機芯的生產設備](../Page/計時碼錶.md "wikilink")，[奇斯托波爾鐘錶廠亦收購了其](../Page/東方牌.md "wikilink")[2612](../Page/2612.md "wikilink")[鬧錶機芯的生產工具](../Page/鬧錶.md "wikilink")，自此[莫斯科第一鐘錶廠亦正式結束其長達](../Page/莫斯科第一鐘錶廠.md "wikilink")70多年的歷史。

[莫斯科第一鐘錶廠的結束並不代表寶傑的結束](../Page/莫斯科第一鐘錶廠.md "wikilink")，現時寶傑的手錶仍然在繼續生產。

但早於在2002年，寶傑的員工成立了[Volmax公司](../Page/Volmax.md "wikilink")，取得了Shturmanskie、Buran及Avaitor等品牌，繼續生產使用3133等機芯的手錶。

另外在德國，也有名為[Poljot
International的](../Page/Poljot_International.md "wikilink")[手錶品牌](../Page/手錶.md "wikilink")。[Poljot
International與寶傑本身並無任何關係](../Page/Poljot_International.md "wikilink")，但是其手錶的機芯有部份是使用寶傑及[閃電牌的俄製機芯](../Page/閃電牌.md "wikilink")。

## 外部連結

  - [寶傑的官方網頁](http://www.poljot.ru)
  - [Volmax的官方網頁（Shturmanskie、Buran及Avaitor）](https://web.archive.org/web/20121206142003/http://aviatorwatch.ru/)

[Category:手表品牌](../Category/手表品牌.md "wikilink")
[Category:俄罗斯品牌](../Category/俄罗斯品牌.md "wikilink")