**寶勝國際（控股）有限公司**（）為設立於中國的運動服裝和鞋類產品的經銷和零售商\[1\]，通路品牌名稱為「勝道」及「YYsports」。原爲[裕元工業集團旗下的零售業務](../Page/裕元集團.md "wikilink")，在2008年6月6日分拆獨立上市，當時招股定價為3.05港元\[2\]。其發展重心是高端品牌代理及零售，目前約80%的銷售收入來自[耐克和](../Page/耐克.md "wikilink")[阿迪達斯](../Page/阿迪達斯.md "wikilink")，約10%來自[匡威](../Page/匡威.md "wikilink")、[彪馬和](../Page/彪馬.md "wikilink")[銳步](../Page/銳步.md "wikilink")，其餘來自其他運動和休閒服飾品牌，例如[李寧](../Page/李宁有限公司.md "wikilink")、[北面](../Page/The_North_Face.md "wikilink")、[探路者和](../Page/探路者.md "wikilink")[添柏岚等](../Page/添柏岚.md "wikilink")。

2018年1月，裕元工業（集團）主要股東[寶成工業向寶勝國際提出私有化計劃](../Page/寶成工業.md "wikilink")，作價為每股2.03港元。\[3\]2018年4月9日，該計劃由於未能於法院會議上獲得通過批准，因此失效。\[4\]

## 参考

## 外部連結

  - [寶勝國際官網](http://www.pousheng.com/)

[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:中國體育用品公司](../Category/中國體育用品公司.md "wikilink")
[Category:寶成國際集團](../Category/寶成國際集團.md "wikilink")
[Category:中国鞋业公司](../Category/中国鞋业公司.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")
[Category:中國零售商](../Category/中國零售商.md "wikilink")

1.
2.
3.
4.