[Gem.pebbles.800pix.jpg](https://zh.wikipedia.org/wiki/File:Gem.pebbles.800pix.jpg "fig:Gem.pebbles.800pix.jpg")、[赤鐵礦](../Page/赤鐵礦.md "wikilink")、[矽孔雀石](../Page/矽孔雀石.md "wikilink")、[虎眼石](../Page/虎眼石.md "wikilink")
第2排：[石英](../Page/石英.md "wikilink")、[電氣石](../Page/電氣石.md "wikilink")、[紅玉髓](../Page/紅玉髓.md "wikilink")、[黃鐵礦](../Page/黃鐵礦.md "wikilink")、[舒俱徠石](../Page/杉石.md "wikilink")
第3排：[孔雀石](../Page/孔雀石.md "wikilink")、[芙蓉石](../Page/芙蓉石.md "wikilink")、[黑曜石](../Page/黑曜石.md "wikilink")、[紅寶石](../Page/紅寶石.md "wikilink")、[苔瑪瑙](../Page/苔瑪瑙.md "wikilink")
第4排：[碧玉](../Page/賈斯珀.md "wikilink")、[紫水晶](../Page/紫水晶.md "wikilink")、紋[瑪瑙](../Page/瑪瑙.md "wikilink")、[青金石](../Page/青金石.md "wikilink")\]\]

**寶石**是被切割、擦潤、抛光的[礦物](../Page/礦物.md "wikilink")、[石料](../Page/石料.md "wikilink")、[土化物體](../Page/土化.md "wikilink")，為收集品、收藏品或穿著裝飾之用。而作为宝石的[矿物](../Page/矿物.md "wikilink")，一般[颜色鲜艳柔和](../Page/颜色.md "wikilink")，光泽和花纹美观，结构均匀，折光率强，硬度较大，[化学成分稳定](../Page/化学.md "wikilink")。

人类从自古以来就将宝石加工作为饰物。寶石最重要的珍貴之處是其完美的外表，所以被擦傷後喪失價值。

也有些脆弱或軟的寶石只能收集，不能穿著，像許多[博物館徵求的](../Page/博物館.md "wikilink")[菱錳礦](../Page/菱錳礦.md "wikilink")。

## 分类

目前世界上3000多种[矿物中](../Page/矿物.md "wikilink")，有230种以上可以作为宝石，但应用最广的一般只有20-30种。世界宝石资源主要分布在[亚洲南部](../Page/亚洲和太平洋地区世界遗产列表.md "wikilink")、[非洲南部](../Page/非洲.md "wikilink")、[美洲](../Page/美洲.md "wikilink")、[澳洲以及](../Page/澳洲.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")。

商人分寶石為寶石（貴重寶石）和半寶石（次貴重寶石）。只有五種貴重寶石，1968年法國定義前四種為貴重寶石\[1\]，餘稱精緻寶石：

  - [鑽石](../Page/鑽石.md "wikilink")
    :最晚名列貴重寶石的礦物，四大文明古國和14世紀以前歐洲均不承認鑽石或金鋼石為寶物
  - [藍寶石](../Page/藍寶石.md "wikilink")
  - [紅寶石](../Page/紅寶石.md "wikilink")
  - [祖母綠](../Page/祖母綠.md "wikilink")
  - [金綠玉](../Page/金綠玉.md "wikilink")（亞歷山大變色石）

半寶石數量多，二十多種常見，包括：

  - [瑪瑙](../Page/瑪瑙.md "wikilink")
  - [紫水晶](../Page/紫水晶.md "wikilink")
  - [綠柱石](../Page/綠柱石.md "wikilink")（海藍寶石）
  - [黃水晶](../Page/黃水晶.md "wikilink")
  - [石榴石](../Page/石榴石.md "wikilink")

<!-- end list -->

  - [橄欖石](../Page/橄欖石.md "wikilink")
  - [蛋白石](../Page/蛋白石.md "wikilink")
  - [尖晶石](../Page/尖晶石.md "wikilink")
  - [坦桑石](../Page/坦桑石.md "wikilink")
  - [虎眼石](../Page/虎眼石.md "wikilink")

<!-- end list -->

  - [綠松石](../Page/綠松石.md "wikilink")
  - [黃玉](../Page/黃玉.md "wikilink")（黃晶）
  - [電氣石](../Page/電氣石.md "wikilink")（碧玺）
  - [鋯石](../Page/鋯石.md "wikilink")

嚴格上不算做宝石的，即不属于矿物的有机体，但也作为珠宝的还有：

  - [琥珀](../Page/琥珀.md "wikilink")14世紀前 歐洲最尊貴的寶石
  - [煤玉](../Page/煤玉.md "wikilink")
  - [珍珠](../Page/珍珠.md "wikilink")
  - [珊瑚](../Page/珊瑚.md "wikilink")
  - [象牙](../Page/象牙.md "wikilink")

在[寶石學中](../Page/寶石學.md "wikilink")，寶石是由以下特徵分類：

  - 構造；例如，[鑽石是完全](../Page/鑽石.md "wikilink")[碳](../Page/碳.md "wikilink")；[紅寶石是](../Page/紅寶石.md "wikilink")[氧化鋁](../Page/氧化鋁.md "wikilink")。（參見[元素](../Page/元素.md "wikilink")）
  - [結晶系統](../Page/結晶.md "wikilink")：例如，立方晶系、三方晶系、單斜晶系。
  - 晶體習性：例如，八面體結晶。

同一種寶石有不同變化，例如：

  - [紅寶石和不同顏色的](../Page/紅寶石.md "wikilink")[藍寶石都是](../Page/藍寶石.md "wikilink")[剛玉變化而来的](../Page/剛玉.md "wikilink")。
  - [金綠玉有金綠玉貓眼石](../Page/金綠玉.md "wikilink")，亞歷山大變色石

此外，在[中国传统非常重视](../Page/中国.md "wikilink")[玉](../Page/玉.md "wikilink")，通常分为软玉（[昆仑山是主要产地](../Page/昆仑山.md "wikilink")）和硬玉（[翡翠](../Page/翡翠.md "wikilink")，主要产于[缅甸](../Page/缅甸.md "wikilink")），所谓“黄金有价玉无价”。

## 決定寶石價格的因素

對於寶石的評鑑會以4C作為標準。\[2\]

1.  克拉（carat）
2.  淨度（clarity）
3.  顏色（color）
4.  車工（cut）


而影響寶石行情的外來因素也包含：產地因素、國際經濟局勢、新式處理或合成寶的問世共三項。

## 种类

天然宝石名称\[3\]

|                                    |                       |      |
| ---------------------------------- | --------------------- | ---- |
| 天然宝石基本名称                           | 英文名称                  | 矿物名称 |
| [钻石](../Page/钻石.md "wikilink")     | Diamond               | 金刚石  |
| [红宝石](../Page/红宝石.md "wikilink")   | Ruby                  | 刚玉   |
| [蓝宝石](../Page/蓝宝石.md "wikilink")   | Sapphire              | 剛玉   |
| [金绿宝石](../Page/金绿宝石.md "wikilink") | Chrysoberyl           | 金绿宝石 |
| [猫眼](../Page/猫眼.md "wikilink")     | Chrysoberyl cat’s-eye | 金绿宝石 |
| [变石](../Page/变石.md "wikilink")     | Alexandrite           |      |
| [变石猫眼](../Page/变石猫眼.md "wikilink") | Alexandrite cat’s-eye |      |
| [祖母绿](../Page/祖母绿.md "wikilink")   | Emerald               | 绿柱石  |
| [海蓝宝石](../Page/海蓝宝石.md "wikilink") | Aquamarine            | 綠柱石  |
| [绿柱石](../Page/绿柱石.md "wikilink")   | Beryl                 | 綠柱石  |
| [碧玺](../Page/碧玺.md "wikilink")     | Tourmaline            | 电气石  |
| [尖晶石](../Page/尖晶石.md "wikilink")   | Spinel                | 尖晶石  |
| [锆石](../Page/锆石.md "wikilink")     | Zircon                | 锆石   |
| [托帕石](../Page/托帕石.md "wikilink")   | Topaz                 | 黄玉   |
| [橄榄石](../Page/橄榄石.md "wikilink")   | Peridot               | 橄榄石  |
| [石榴石](../Page/石榴石.md "wikilink")   | Garnet                | 石榴石  |
| [镁铝榴石](../Page/镁铝榴石.md "wikilink") | Pyrope                | 镁铝榴石 |
| [铁铝榴石](../Page/铁铝榴石.md "wikilink") | Almandite             | 铁铝榴石 |
| [锰铝榴石](../Page/锰铝榴石.md "wikilink") | Spessartite           | 锰铝榴石 |
| [钙铝榴石](../Page/钙铝榴石.md "wikilink") | Grossularite          | 钙铝榴石 |
| [钙铁榴石](../Page/钙铁榴石.md "wikilink") | Andradite             | 钙铁榴石 |
| [翠榴石](../Page/翠榴石.md "wikilink")   | Demantoid             | 翠榴石  |
| [黑榴石](../Page/黑榴石.md "wikilink")   | Melanite              | 黑榴石  |
| [钙铬榴石](../Page/钙铬榴石.md "wikilink") | Uvarovite             | 钙铬榴石 |
| [石英](../Page/石英.md "wikilink")     | Quartz                | 石英   |
| [水晶](../Page/水晶.md "wikilink")     | Rock crystal          | 石英   |
| [紫晶](../Page/紫晶.md "wikilink")     | Amethyst              | 石英   |
| [黄晶](../Page/黄晶.md "wikilink")     | Citrine               | 石英   |
| [烟晶](../Page/烟晶.md "wikilink")     | Smoky quartz          | 石英   |
| [绿水晶](../Page/绿水晶.md "wikilink")   | Green quartz          | 石英   |
| [芙蓉石](../Page/芙蓉石.md "wikilink")   | Rose quartz           | 石英   |
| [长石](../Page/长石.md "wikilink")     | Feldspar              | 长石   |
| [月光石](../Page/月光石.md "wikilink")   | Moonstone             | 正长石  |
| [天河石](../Page/天河石.md "wikilink")   | Amazonyte             | 微斜长石 |
| [日光石](../Page/日光石.md "wikilink")   | Sunstone              | 奥长石  |
| [拉长石](../Page/拉长石.md "wikilink")   | Labradorite           | 拉长石  |
| [方柱石](../Page/方柱石.md "wikilink")   | Scapolite             | 方柱石  |
| [柱晶石](../Page/柱晶石.md "wikilink")   | Kounerupine           | 柱晶石  |
| [黝帘石](../Page/黝帘石.md "wikilink")   | Zoisite               | 黝帘石  |
| [坦桑石](../Page/坦桑石.md "wikilink")   | Tanzanite             |      |
| [绿帘石](../Page/绿帘石.md "wikilink")   | Epidote               | 绿帘石  |
| [堇青石](../Page/堇青石.md "wikilink")   | Iolite                | 堇青石  |
| [榍石](../Page/榍石.md "wikilink")     | Sphene                | 榍石   |
| [磷灰石](../Page/磷灰石.md "wikilink")   | Apatite               | 磷灰石  |
| [辉石](../Page/辉石.md "wikilink")     | Pyroxene              | 辉石   |
| [透辉石](../Page/透辉石.md "wikilink")   | Diopside              | 透辉石  |
| [普通辉石](../Page/普通辉石.md "wikilink") | Augite                | 普通辉石 |
| [顽火辉石](../Page/顽火辉石.md "wikilink") | Enstatite             | 顽火辉石 |
| [锂辉石](../Page/锂辉石.md "wikilink")   | Spodumene             | 锂辉石  |
| [红柱石](../Page/红柱石.md "wikilink")   | Andalusite            | 红柱石  |
| [空晶石](../Page/空晶石.md "wikilink")   | Chiastolite           |      |
| [矽线石](../Page/矽线石.md "wikilink")   | Sillimanite           | 矽线石  |
| [蓝晶石](../Page/蓝晶石.md "wikilink")   | Kyanite               | 蓝晶石  |
| [鱼眼石](../Page/鱼眼石.md "wikilink")   | Apophyllite           | 鱼眼石  |
| [青金石](../Page/青金石.md "wikilink")   | Lazurite              | 方鈉石  |
| [符山石](../Page/符山石.md "wikilink")   | Idocrase              | 符山石  |
| [硼铝镁石](../Page/硼铝镁石.md "wikilink") | Sinhalite             | 硼铝镁石 |
| [塔菲石](../Page/塔菲石.md "wikilink")   | Taaffeite             | 塔菲石  |
| [蓝锥矿](../Page/蓝锥矿.md "wikilink")   | Benitoite             | 蓝锥矿  |
| [重晶石](../Page/重晶石.md "wikilink")   | Barite                | 重晶石  |
| [天青石](../Page/天青石.md "wikilink")   | Celestite             | 天青石  |
| [方解石](../Page/方解石.md "wikilink")   | Calcite               | 方解石  |
| [冰洲石](../Page/冰洲石.md "wikilink")   | Iceland spar          |      |
| [斧石](../Page/斧石.md "wikilink")     | Axinite               | 斧石   |
| [锡石](../Page/锡石.md "wikilink")     | Cassiterite           | 锡石   |
| [磷铝锂石](../Page/磷铝锂石.md "wikilink") | Amblygonite           | 磷铝锂石 |
| [透视石](../Page/透视石.md "wikilink")   | Dioptase              | 透视石  |
| [蓝柱石](../Page/蓝柱石.md "wikilink")   | Euclase               | 蓝柱石  |
| [磷铝钠石](../Page/磷铝钠石.md "wikilink") | Brazilianite          | 磷铝钠石 |
| [赛黄晶](../Page/赛黄晶.md "wikilink")   | Danburite             | 赛黄晶  |
| [硅铍石](../Page/硅铍石.md "wikilink")   | Phenakite             | 硅铍石  |
| [綠松石](../Page/綠松石.md "wikilink")   | turquoise             | 綠松石  |

## 合成寶石及人造寶石

有些合成寶石被用来仿冒或偽裝為其他的較珍貴寶石，例如[二氧化鋯石是一種合成的](../Page/二氧化鋯石.md "wikilink")[鑽石仿冒品](../Page/鑽石.md "wikilink")。這些仿冒品外表和[反光和真正的寶石看起來一樣](../Page/反光.md "wikilink")，但是沒有它們的[化學和](../Page/化學.md "wikilink")[物理特色](../Page/物理.md "wikilink")。有的[人工寶石是和自然界的宝石成分相同](../Page/人工寶石.md "wikilink")，例如人工合成的[鑽石](../Page/鑽石.md "wikilink")、[紅寶石](../Page/紅寶石.md "wikilink")、[藍寶石](../Page/藍寶石.md "wikilink")、[綠寶石](../Page/綠寶石.md "wikilink")。合成寶石中[剛玉](../Page/剛玉.md "wikilink")（包括紅寶石和藍寶石）較常見，比天然的便宜；

## 参考资料

## 外部链接

  - [《皇家宝石书籍》](http://www.wdl.org/zh/item/2839)从1554年，是一个阿拉伯语手稿众多宝石中大篇幅谈
  - [宝石鉴赏专著](http://www.wdl.org/zh/item/4256)从17世纪开始，在阿拉伯语中被称为“最详细，最完整的论文在石头上的中世纪和它们的属性是一本书。”

[\*](../Category/宝石.md "wikilink") [B](../Category/礦物學.md "wikilink")

1.  [*Precious
    Stones*](https://books.google.com/books?id=crc7ZtRHItgC&pg=PA1), Max
    Bauer, p.2
2.
3.  GB/T16552－2003 “珠宝玉石名称”