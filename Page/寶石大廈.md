**寶石大廈**（，重建前：）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣市地段](../Page/荃灣.md "wikilink")364號，荃灣[沙咀道](../Page/沙咀道.md "wikilink")328號，1996年落成，由鍾華楠建築設計事務所設計，金門建築有限公司承建。其前身「**寶石樓**」是出租屋邨，有三座大廈，於1964年落成，1993年開始拆卸重建，重建工程於1996年完成，重建後的寶石大廈同樣是三座大廈，當中第1座及第2座作為[住宅發售計劃項目](../Page/住宅發售計劃.md "wikilink")，間格分為兩房兩廳及三房兩廳連套房兩種，並隨居屋計劃第十七期乙一併出售。

第3座的1至3樓作為年長者居住單位用途，單位面積分6款，而4樓則為[隔火層及平台](../Page/隔火層.md "wikilink")，而5至25樓則作為乙類出租屋邨用途，並採用私人樓宇常用的三梯八伙設計，出租單位主要分三款間格，全部附有梗房（固定房間）間格，廳房設有[窗台](../Page/窗台.md "wikilink")。

## 命名

寶石大廈在早期稱為「荃灣區第二期」；[荃灣新市鎮內居民及來往荃灣區的](../Page/荃灣新市鎮.md "wikilink")[公共小巴路線顯示則俗稱為](../Page/公共小巴.md "wikilink")「寶石樓」。而寶石大廈的名稱由來，根據房協年報，因為此邨鄰近安置因興建[石壁水塘而需拆遷的](../Page/石壁水塘.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[石壁居民而建的](../Page/石壁_\(香港\).md "wikilink")[石碧新村](../Page/石碧新村.md "wikilink")，所以以一「石」字作為邨名
。

## 屋邨資料

### 樓宇

由鍾華楠建築設計事務所設計

| 樓宇名稱      | 落成年份 |
| --------- | ---- |
| 第1座(出售大廈) | 1996 |
| 第2座(出售大廈) |      |
| 第3座(出租大廈) |      |

### 歷代樓宇

由周耀年李禮之建築師工程師事務所設計。

| 樓宇名稱 | 落成年份 | 拆卸年份 |
| ---- | ---- | ---- |
| 琥珀樓  | 1964 | 1993 |
| 鑽石樓  |      |      |
| 翡翠樓  |      |      |

### 出租單位(第3座)面積及編配標凖

|       |                   |      |      |      |           |                   |
| ----- | ----------------- | ---- | ---- | ---- | --------- | ----------------- |
| 單位款式  | 單位                | 單位型號 | 單位間隔 | 編配標凖 | 建築面積      | 實用面積              |
| 小型單位  | 5樓至25樓---H室       | A型   | 一房兩廳 | 2-3人 | 約22.80平方米 | 13.166平方米         |
| 中型單位  | 5樓至25樓---B,C,F及G室 | B1型  | 兩房兩廳 | 3-5人 | 約33.20平方米 | 23.336平方米         |
| 中型單位  | 5樓至25樓---D及E室     | B型   | 兩房兩廳 | 3-5人 | 約33.37平方米 | 23.556平方米         |
| 大型單位  | 5樓至25樓---A室       | C型   | 三房兩廳 | 5-6人 | 約49.71平方米 | 38.482平方米         |
| 年長者單位 | 1樓至3樓---所有單位      | E型   | 開放式  | 1-2人 | 約16-23平方米 | 9.52平方米-15.688平方米 |

## 教育

### 幼稚園

  - [藍如溪盛成皿教育基金邊耀良幼稚園](http://www.alvseducationfund.org.hk/quanwan/home.php)（1997年創辦）
  - [思博幼稚園](http://swindon.edu.hk)（2013年創辦）

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[荃灣站](../Page/荃灣站.md "wikilink")(步行15分鐘)

<!-- end list -->

  - [沙咀道](../Page/沙咀道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港公共小巴.md "wikilink")

<!-- end list -->

  - 荃灣至[旺角](../Page/旺角.md "wikilink")/[佐敦道線](../Page/佐敦道.md "wikilink")
    (24小時服務)\[1\]

<!-- end list -->

  - [鹹田街](../Page/鹹田街.md "wikilink")/[河背街](../Page/河背街.md "wikilink")

<!-- end list -->

  - [楊屋道](../Page/楊屋道.md "wikilink")

<!-- end list -->

  - [聯仁街](../Page/聯仁街.md "wikilink")

</div>

</div>

## 外部連結

  - [寶石大廈舊照](http://media10.dropshots.com/photos/531863/20110320/124915.jpg)
  - [寶石大廈](https://web.archive.org/web/20080618120954/http://www.hkhs.com/chi/business/16.asp?contentid=1&estid=16)
  - [建設及建築物 － 房協屋村(二)](http://www.hk-place.com/view.php?id=226)

[en:Public housing estates in Tsuen Wan\#Bo Shek
Mansion](../Page/en:Public_housing_estates_in_Tsuen_Wan#Bo_Shek_Mansion.md "wikilink")

[Category:荃灣](../Category/荃灣.md "wikilink")
[Category:住宅發售計劃屋苑](../Category/住宅發售計劃屋苑.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  [旺角朗豪坊　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn47.html)