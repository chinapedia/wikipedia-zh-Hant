**寶蛺蝶屬**（[學名](../Page/學名.md "wikilink")：*Boloria*）是[蛺蝶科](../Page/蛺蝶科.md "wikilink")[釉蛺蝶亞科](../Page/釉蛺蝶亞科.md "wikilink")[彩蛺蝶族裡的一個](../Page/彩蛺蝶族.md "wikilink")[屬](../Page/屬.md "wikilink")。[物種分佈於](../Page/物種.md "wikilink")[全北界](../Page/全北界.md "wikilink")\[1\]。

## 物種

**鉑蛺蝶亞屬**

  - [鉑蛺蝶](../Page/鉑蛺蝶.md "wikilink") *Boloria eunomia*

**寶蛺蝶亞屬**

  - *Boloria caucasica*
  - [膝寶蛺蝶](../Page/膝寶蛺蝶.md "wikilink") *Boloria generator*
  - *Boloria patina*
  - [細寶蛺蝶](../Page/細寶蛺蝶.md "wikilink") *Boloria sifanica*
  - [乖寶蛺蝶](../Page/乖寶蛺蝶.md "wikilink") *Boloria graeca*
  - [龍女寶蛺蝶](../Page/龍女寶蛺蝶.md "wikilink") *Boloria pales*
  - *Boloria altaica*
  - *Boloria alaskensis*
  - [水寶蛺蝶](../Page/水寶蛺蝶.md "wikilink") *Boloria aquilonaris*
  - [洛神寶蛺蝶](../Page/洛神寶蛺蝶.md "wikilink") *Boloria napaea*
  - *Boloria banghaasi*
  - *Boloria eupales*
  - [福潤寶蛺蝶](../Page/福潤寶蛺蝶.md "wikilink") *Boloria frigidalis*
  - *Boloria purpurea*
  - *Boloria pyrenesmiscens*
  - *Boloria roddi*
  - *Boloria sipora*

**珍蛺蝶亞屬**

  - [嚴珍蛺蝶](../Page/嚴珍蛺蝶.md "wikilink") *Boloria iphigenia*
  - [埃珍蛺蝶](../Page/埃珍蛺蝶.md "wikilink") *Boloria erda*
  - [白珍蛺蝶](../Page/白珍蛺蝶.md "wikilink") *Boloria alberta*
  - [興佛珍蛺蝶](../Page/興佛珍蛺蝶.md "wikilink") *Boloria polaris*
  - [阿珍蛺蝶](../Page/阿珍蛺蝶.md "wikilink") *Boloria astarte*
      - 迪珍蛺蝶 *B. a. distincta*
  - [美珍蛺蝶](../Page/美珍蛺蝶.md "wikilink") *Boloria matrreevi*
  - [林珍蛺蝶](../Page/林珍蛺蝶.md "wikilink") *Boloria tritonia*
  - [艾魯珍蛺蝶](../Page/艾魯珍蛺蝶.md "wikilink") *Boloria erubescens* = 中亞珍蛺蝶
    *B. hegemone*
  - [傑珍蛺蝶](../Page/傑珍蛺蝶.md "wikilink") *Boloria jerdoni*
  - [長毛珍蛺蝶](../Page/長毛珍蛺蝶.md "wikilink") *Boloria frigga*
  - *Boloria befiona*
  - [瑰珍蛺蝶](../Page/瑰珍蛺蝶.md "wikilink") *Boloria kriemhild*
  - [艾珍蛺蝶](../Page/艾珍蛺蝶.md "wikilink") *Boloria epithore*
  - [夷珍蛺蝶](../Page/夷珍蛺蝶.md "wikilink") *Boloria improba*
      - 殊脛珍蛺蝶 *C. i. acrocnema*
  - [北冷珍蛺蝶](../Page/北冷珍蛺蝶.md "wikilink") *Boloria selene*
  - [佩珍蛺蝶](../Page/佩珍蛺蝶.md "wikilink") *Boloria perryi*
  - [通珍蛺蝶](../Page/通珍蛺蝶.md "wikilink") *Boloria thore*
  - [珍蛺蝶](../Page/珍蛺蝶.md "wikilink") *Boloria gong*
  - [女神珍蛺蝶](../Page/女神珍蛺蝶.md "wikilink") *Boloria dia*
  - [卵珍蛺蝶](../Page/卵珍蛺蝶.md "wikilink") *Boloria euphrosyne*
  - [佛珍蛺蝶](../Page/佛珍蛺蝶.md "wikilink") *Boloria freija*
  - [無佛珍蛺蝶](../Page/無佛珍蛺蝶.md "wikilink") *Boloria natazhati*
  - [西冷珍蛺蝶](../Page/西冷珍蛺蝶.md "wikilink") *Boloria selenis*
  - [北國珍蛺蝶](../Page/北國珍蛺蝶.md "wikilink") *Boloria oscarus*
  - [提珍蛺蝶](../Page/提珍蛺蝶.md "wikilink") *Boloria titania*
  - [安格爾珍蛺蝶](../Page/安格爾珍蛺蝶.md "wikilink") *Boloria angarensis*
      - 黑珍蛺蝶 *B. a. hakutozana*
  - [豹珍蛺蝶](../Page/豹珍蛺蝶.md "wikilink") *Boloria chariclea*

## 參考文獻

<references/>

  - Pelham, J.P. 2008. A catalogue of the butterflies of the United
    States and Canada with a complete bibliography of the descriptive
    and systematic literature. Journal of Research on the Lepidoptera
    40: xiii + 1-658.

  - Simonsen, T. J. 2005. Boloria phylogeny (Lepidoptera: Nymphalidae):
    tentatively reconstructed on the basis of male and female genitalic
    morphology. Systematic Entomology 30:653-665.

  - Simonsen, T. J. 2006. Fritillary phylogeny, classification and
    larval hostplants: reconstructed mainly on the basis of male and
    female genitalic morphology (Lepidoptera: Nymphalidae: Argynnini).
    Biological Journal of the Linnean Society 89: 627-673.

  - Simonsen, T. J., N. Wahlberg, A. V. Z. Brower, and R. de Jong. 2006.
    Morphology, molecules and Fritillaries: approaching a stable
    phylogeny for Argynnini (Lepidoptera: Nymphalidae). Insect
    Systematics & Evolution 37: 405-418.

  - Simonsen, T.J., Wahlberg, N., Warren, A.D.,Sperling, F.A.H. 2010.
    The evolutionary history of Boloria (Lepidoptera: Nymphalidae):
    phylogeny, zoogeography and larval-foodplant relationships.
    Systematics and Biodiversity 8: 513-529.

  -
[Category:豹蛺蝶族](../Category/豹蛺蝶族.md "wikilink")
[\*](../Category/寶蛺蝶屬.md "wikilink")

1.  Simonsen, Thomas, Niklas Wahlberg, and Andrew V. Z. Brower. 2011.
    Boloria Moore 1900. Clossiana Reuss 1920 currently viewed as a
    subgenus of Boloria, Proclossiana Reuss 1926 currently viewed as a
    subgenus of Boloria. Version 20 March 2011 (under construction).
    <http://tolweb.org/Boloria/70413/2011.03.20> in The Tree of Life Web
    Project, <http://tolweb.org/>