[HK_PreciousBloodPrimarySchool.JPG](https://zh.wikipedia.org/wiki/File:HK_PreciousBloodPrimarySchool.JPG "fig:HK_PreciousBloodPrimarySchool.JPG")
**寶血小學**（**Precious Blood Primary
School**）是[香港一所](../Page/香港.md "wikilink")[政府津貼全日制](../Page/香港政府.md "wikilink")[小學](../Page/小學.md "wikilink")，位於[灣仔區](../Page/灣仔區.md "wikilink")[跑馬地](../Page/跑馬地.md "wikilink")[成和道](../Page/成和道.md "wikilink")，成立於1945年，前身是德貞第二女子中學，由[寶血女修會主辦](../Page/寶血女修會.md "wikilink")，於2003年轉為全日制。

## 辦學宗旨

以兒童為中心，藉教育傳播福音。培養自學、思考、解難、創新的能力。締造融和友愛的氣氛，讓兒童在被肯定的環境中成長；並致力邁向教育多元化的新領域。尤重品德的陶成，塑造六育兼備的兒童，以達全人教育的目標。

## 學校設施

寶血小學校舍有18個[課室](../Page/課室.md "wikilink")，當中設有[禮堂](../Page/禮堂.md "wikilink")。全校裝置[冷氣](../Page/冷氣.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、[電視](../Page/電視.md "wikilink")、[投影機及](../Page/投影機.md "wikilink")[無線網絡](../Page/無線網絡.md "wikilink")。設施包括：

  - 運動：

:\*[籃球場](../Page/籃球場.md "wikilink")

:\*乒乓球場

:\*兩個有蓋操場

  - 文化：

:\*圖書室

:\*音樂室

:\*英語室

:\*電腦室

  - 特別室：

:\*會客室

:\*多用途室

:\*學生輔導室

  - 其他：

:\*校車服務

:\*家長資源中心

## 班級結構

  - 全校小一至小六，共18班
  - 班名按次序：
      - 信{a}
      - 望{b}
      - 愛{c}
  - 每級三班

## 重點或未來發展項目

重點以戲劇發展學生創意，參加幼聯小學英語教學計劃，提升學生英語水平；推行普通話教中文，發展數學校本課程；全面加強中、英、數的教學效能。

## 學生活動

寶血小學開辦不同的制服團隊，如小女童軍（港島東跑馬地分區）兩隊、小童軍〈港島地域灣仔區1482旅〉、幼童軍（港島地域灣仔區1482旅）美樂笛，合唱團，西樂組、基督小先鋒等。以循環課節上課，並在每循環周舉行興趣小組，提供多樣化的活動。

## 交通

  - [電車](../Page/香港電車.md "wikilink")

<!-- end list -->

  - [城巴](../Page/城巴.md "wikilink"): 1、1P、8X、19、117
  - [九巴](../Page/九龍巴士.md "wikilink"): 117
  - [香港島專線小巴](../Page/香港小巴.md "wikilink"): 30

## 著名校友

  - [陳思齊](../Page/陳思齊.md "wikilink")：香港女藝人
  - [蔡卓妍](../Page/蔡卓妍.md "wikilink")：香港女歌手
  - [蔡思貝](../Page/蔡思貝.md "wikilink")：香港女藝人
  - [劉惠鳴](../Page/劉惠鳴.md "wikilink")：香港粵劇演員\[1\]
  - [梁鳳儀](../Page/梁鳳儀_\(企業家\).md "wikilink")：[勤+緣媒體創辦人](../Page/勤+緣媒體.md "wikilink")，作家\[2\]
  - [施幸余](../Page/施幸余.md "wikilink")：香港女子游泳運動員\[3\]

## 參見條目

  - [海怡寶血小學](../Page/海怡寶血小學.md "wikilink")
  - [華富邨寶血小學](../Page/華富邨寶血小學.md "wikilink")
  - [寶血女子中學](../Page/寶血女子中學.md "wikilink")

## 參考來源

## 外部連結

  - [寶血小學官方網頁](http://www.preciousbloodhv.edu.hk/)
  - [寶血小學校友會官方網頁](http://www.pbalumni.org/)
  - [香港教育城《小學概覽2007》-
    寶血小學](http://psp.proj.hkedcity.net/2007/chi/detail_sch_basic.php?sch_id=559)

[Category:灣仔區小學](../Category/灣仔區小學.md "wikilink")
[Category:跑馬地](../Category/跑馬地.md "wikilink")
[Category:香港天主教學校](../Category/香港天主教學校.md "wikilink")
[Category:1945年創建的教育機構](../Category/1945年創建的教育機構.md "wikilink")

1.  [劉惠鳴個人介紹](http://www.lauwaiming.hk/intro.php)
2.
3.  [五旬節靳茂生小學《Sport Star 7
    施幸余》，2005年](http://www.pgms.edu.hk/teacher/pe/2005PE/picture/HongKongSport/SportStar007.htm)