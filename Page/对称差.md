[Venn0110.svg](https://zh.wikipedia.org/wiki/File:Venn0110.svg "fig:Venn0110.svg")。對稱差標為紅色。\]\]
[数学上](../Page/数学.md "wikilink")，两个[集合的](../Page/集合.md "wikilink")**对称差**是只属于其中一个集合，而不属于另一个集合的元素组成的集合。
[集合论中的这个运算相当于](../Page/集合论.md "wikilink")[布尔逻辑中的](../Page/布尔逻辑.md "wikilink")[异或运算](../Page/异或.md "wikilink")。

集合\(A\)和\(B\)的对称差通常表示为\(A \operatorname{\triangle} B\)，对称差的符号在有些图论书籍中也使用\(\oplus\)符号来表示。例如：集合\(\{1, 2, 3\}\)和\(\{3, 4\}\)的对称差为\(\{1, 2, 4\}\)。所有学生的集合和所有女性的集合的对称差为所有男性学生和所有女性非学生组成的集合。

## 定义

对称差是集合间的运算，两个集合\(A\)和\(B\)，其对称差\(A \operatorname{\triangle} B\)有几种等价的定义方式：

1.  \(A \operatorname{\triangle} B = (A - B) \cup (B - A)\)
2.  \(A \operatorname{\triangle} B = (A \cup B) - (A \cap B)\)

## 性质

对称差运算的主要性质包括：

  - [交换律](../Page/交换律.md "wikilink")
    \(A \operatorname{\triangle} B = B \operatorname{\triangle} A\)
  - [结合律](../Page/结合律.md "wikilink")
    \((A \operatorname{\triangle} B) \operatorname{\triangle} C = A \operatorname{\triangle} (B \operatorname{\triangle} C)\)
  - [单位元](../Page/单位元.md "wikilink")
    \(\varnothing \operatorname{\triangle} A = A\)（空集是单位元）
  - [逆元](../Page/逆元.md "wikilink")
    \(A \operatorname{\triangle} A = \varnothing\)
  - [分配律](../Page/分配律.md "wikilink")
    \(A \cap (B \operatorname{\triangle} C) = (A \cap B) \operatorname{\triangle} (A \cap C)\)
    注意：
    \[A \operatorname{\triangle} (B \cap C) \neq (A \operatorname{\triangle} B) \cap (A \operatorname{\triangle} C)\]
    \[A \cup (B \operatorname{\triangle} C) \neq (A \cup B) \operatorname{\triangle} (A \cup C)\]
    \[A \operatorname{\triangle} (B \cup C) \neq (A \operatorname{\triangle} B) \cup (A \operatorname{\triangle} C)\]

### [布尔环](../Page/布尔环.md "wikilink")

以对称差作为加法，交集为乘法，任何集合\(X\)的幂集\(\mathcal{P}(X)\)构成一个布尔环，并可以诱导一个同构的[布尔代数](../Page/布尔代数.md "wikilink")。

综上可得，采用对称差运算，任意集合\(X\)的[幂集是](../Page/幂集.md "wikilink")[阿贝尔群](../Page/阿贝尔群.md "wikilink")。由于该群中所有元素都是其自身的负元，这个群实际上是[二元域](../Page/有限域.md "wikilink")\(Z_{2}\)上的[向量空间](../Page/向量空间.md "wikilink")。若\(X\)有限，则以其为元素的[单元素集合构成这个向量空间的](../Page/单元素集合.md "wikilink")[基](../Page/基.md "wikilink")，那么向量空间的[维数等于](../Page/维数.md "wikilink")\(X\)的元素个数。这种构造方法用于[图论](../Page/图论.md "wikilink")，可定义图的[圈空间](../Page/圈空间.md "wikilink")。

对称差满足的恒等式有：

\[A \operatorname{\triangle} \varnothing = A\]

\[A \operatorname{\triangle} A = \varnothing\]

\[A \operatorname{\triangle} B = B \operatorname{\triangle} A\]

\[(A \operatorname{\triangle} B) \operatorname{\triangle} C = A \operatorname{\triangle} (B \operatorname{\triangle} C)\]

\[A \operatorname{\triangle} B = A \operatorname{\triangle} C \Rightarrow B = C\]

## 与逻辑和布尔代数的关系

或者用异或运算（\(\oplus\)）表示：

\[A \operatorname{\triangle} B = \{x \mid (x \in A) \oplus (x \in B)\}\]

对称差可以在任意[布尔代数中定义](../Page/布尔代数.md "wikilink")，写作：

\[x \operatorname{\triangle} y = (x \lor y) \land \neg (x \land y) = (x \land \neg y) \lor (y \land \neg x)\]

## 参考

  - [朴素集合论](../Page/朴素集合论.md "wikilink")
  - [并集](../Page/并集.md "wikilink")
  - [交集](../Page/交集.md "wikilink")
  - [补集](../Page/补集.md "wikilink")
  - [不交并](../Page/不交并.md "wikilink")

[D](../Category/抽象代数.md "wikilink")
[D](../Category/集合論基本概念.md "wikilink")
[D](../Category/二元運算.md "wikilink")