**对称性（）**是现代[物理学中的一个核心概念](../Page/物理学.md "wikilink")，系统从一个状态到另一个状态，如果这两个状态等价，则说系统对这一变换是对称的。或者说给系统一个“操作”，如果系统从一个状态变到另一个等价的状态，则说系统对这一操作是对称的。它是指一个理论的[拉格朗日量或](../Page/拉格朗日量.md "wikilink")[运动方程在某些变量的变化下的不变性](../Page/运动方程.md "wikilink")。如果这些变量随时空变化，而[拉格朗日量或](../Page/拉格朗日量.md "wikilink")[运动方程仍舊不變](../Page/运动方程.md "wikilink")，則称此性質為为「局域对称性」（），反之，若这些变量不随时空变化，則称此性質为「整体对称性」（）。物理学中最简单的对称性例子是[牛顿运动方程的](../Page/牛顿第二定律.md "wikilink")[伽利略变换不变性和](../Page/伽利略变换.md "wikilink")[麦克斯韦方程的](../Page/麦克斯韦方程.md "wikilink")[洛伦兹变换不变性和相位不变性](../Page/洛伦兹变换.md "wikilink")。\[1\]

数学上，这些对称性由[群论来表述](../Page/群论.md "wikilink")。上述例子中的群分别对应着[伽利略群](../Page/伽利略变化.md "wikilink")，[洛伦兹群和](../Page/洛伦兹变化.md "wikilink")\(U(1)\)群。对称群为[连续群和](../Page/连续群.md "wikilink")[分立群的情形分别被称为](../Page/分立群.md "wikilink")「连续对称性」（)和「離散對稱性」（）。[德国](../Page/德国.md "wikilink")[数学家](../Page/数学家.md "wikilink")[外尔](../Page/赫尔曼·外尔.md "wikilink")（）是把这套数学方法运用于物理学中并意识到规范对称重要性的第一人。1950年代[杨振宁和](../Page/杨振宁.md "wikilink")[米尔斯意识到规范对称性可以完全决定一个理论的](../Page/罗伯特·米尔斯.md "wikilink")[拉格朗日量的形式](../Page/拉格朗日量.md "wikilink")，并构造了核作用的\(SU(2)\)规范理论。从此，规范对称性被大量应用于[量子场论和](../Page/量子场论.md "wikilink")[粒子物理模型中](../Page/粒子物理.md "wikilink")。在粒子物理的[标准模型中](../Page/标准模型.md "wikilink")，[强相互作用](../Page/强相互作用.md "wikilink")，[弱相互作用和](../Page/弱相互作用.md "wikilink")[电磁相互作用的规范群分别为](../Page/电磁相互作用.md "wikilink")\(SU(3)\)，\(SU(2)\)和\(U(1)\)。除此之外，其他群也被理论物理学家广泛地应用，如[大统一模型中的](../Page/大统一理论.md "wikilink")\(SU(5)\)，\(SO(10)\)和\(E_6\)群，[超弦理论中的](../Page/超弦理论.md "wikilink")\(SO(32)\)和\(E_8\times E_8\)群。

整体对称性在[粒子物理和](../Page/粒子物理.md "wikilink")[量子场论的发展中也起着非常重要的角色](../Page/量子场论.md "wikilink")，如[强相互作用的](../Page/强相互作用.md "wikilink")[手征对称性](../Page/手征对称性.md "wikilink")。规范和整体[对称性破缺是](../Page/对称性破缺.md "wikilink")[粒子物理學和](../Page/粒子物理學.md "wikilink")[凝聚体物理学的重要概念](../Page/凝聚体物理学.md "wikilink")。

## 守恆定律與對稱性的關係

物理系統的每一個對稱性都有相對的[守恒定律](../Page/守恒定律.md "wikilink")。[諾特定理就是概括這關係的重要定理](../Page/諾特定理.md "wikilink")。它指出物理系統包含的每一個對稱性都代表此系统有某相對的物理量守恒。反過來說：物理系統有某守恒性質就代表它帶其相對的對稱性。例如，空間位移對稱造成[動量守恒](../Page/動量守恒.md "wikilink")，而時間平移對稱造成[能量守恒](../Page/能量守恒.md "wikilink")。

以下列表總結各對稱和相對的守恒量：

<table>
<thead>
<tr class="header">
<th><p>類型</p></th>
<th><p><a href="../Page/不變量.md" title="wikilink">不變性</a></p></th>
<th><p><a href="../Page/守恆定律.md" title="wikilink">守恒量</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Proper orthochronous<br />
<a href="../Page/洛伦兹协变性.md" title="wikilink">洛伦兹协变性</a></p></td>
<td><p>時間平移<br />
  <SMALL>(<a href="../Page/同質性_(物理學).md" title="wikilink">時間同質性</a>)</SMALL></p></td>
<td><p><a href="../Page/能量守恒.md" title="wikilink">能量</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/平移不變性.md" title="wikilink">空間平移</a><br />
  <SMALL>(<a href="../Page/同質性_(物理學).md" title="wikilink">空間同質性</a>)</SMALL></p></td>
<td><p><a href="../Page/動量守恒.md" title="wikilink">直線動量</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/旋轉不變性.md" title="wikilink">空間旋轉</a><br />
  <SMALL>(<a href="../Page/各向同性.md" title="wikilink">各向同性</a>)</SMALL></p></td>
<td><p><a href="../Page/角動量守恒.md" title="wikilink">角動量</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/分立對稱.md" title="wikilink">分立對稱</a></p></td>
<td><p>P,座標倒置</p></td>
<td><p><a href="../Page/宇稱不守恆.md" title="wikilink">空間宇稱（鏡像對稱）</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>C, <a href="../Page/反粒子共軛.md" title="wikilink">反粒子共軛</a></p></td>
<td><p><a href="../Page/CP破壞.md" title="wikilink">電荷宇稱</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>T,時間反演</p></td>
<td><p><a href="../Page/时间反演对称性.md" title="wikilink">時間宇稱</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/CPT對稱.md" title="wikilink">CPT</a></p></td>
<td><p>product of parities</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/內部對稱.md" title="wikilink">內部對稱</a>（不取決於<br />
<a href="../Page/時空.md" title="wikilink">時空</a><a href="../Page/座標.md" title="wikilink">座標</a>）</p></td>
<td><p><a href="../Page/圓群.md" title="wikilink">U (1)</a> <a href="../Page/規范場論.md" title="wikilink">規范轉換</a></p></td>
<td><p><a href="../Page/電荷.md" title="wikilink">電荷數</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>U (1) 規范轉換</p></td>
<td><p><a href="../Page/輕子數.md" title="wikilink">輕子數</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>U (1) 規范轉換</p></td>
<td><p><a href="../Page/超荷.md" title="wikilink">超荷</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>U (1)<sub>Y</sub> 規范轉換</p></td>
<td><p><a href="../Page/弱超荷.md" title="wikilink">弱超荷</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>U(2) [U(1)x<a href="../Page/特殊酉群.md" title="wikilink">SU (2)</a>]</p></td>
<td><p><a href="../Page/电弱交互作用.md" title="wikilink">电弱交互作用</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>SU(2) 規范轉換</p></td>
<td><p><a href="../Page/同位旋.md" title="wikilink">同位旋</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>SU (2)<sub>L</sub> 規范轉換</p></td>
<td><p><a href="../Page/弱同位旋.md" title="wikilink">弱同位旋</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>PxSU（2）</p></td>
<td><p><a href="../Page/G-parity.md" title="wikilink">G-parity</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/特殊酉群.md" title="wikilink">SU(3)</a> "卷繞數"</p></td>
<td><p><a href="../Page/重子數.md" title="wikilink">重子數</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>SU(3) 規范轉換</p></td>
<td><p><a href="../Page/夸克_色.md" title="wikilink">夸克 色</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>SU (3)（approximate）</p></td>
<td><p><a href="../Page/味_(粒子物理学).md" title="wikilink">夸克 味</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>S((U2)xU(3))<br />
U (1)xSU (2)xSU (3)</p></td>
<td><p><a href="../Page/標準模型.md" title="wikilink">標準模型</a></p></td>
</tr>
</tbody>
</table>

## 參閱

  - [手徵對稱性破缺](../Page/手徵對稱性破缺.md "wikilink")
  - [明顯對稱性破缺](../Page/明顯對稱性破缺.md "wikilink")

## 参考

[D](../Page/category:量子场论.md "wikilink")
[D](../Page/category:粒子物理学.md "wikilink")

[Category:对称](../Category/对称.md "wikilink")

1.