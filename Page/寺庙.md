[Xiantong_Temple2.JPG](https://zh.wikipedia.org/wiki/File:Xiantong_Temple2.JPG "fig:Xiantong_Temple2.JPG")\]\]
[Temple_of_People's_Liberation_Army.JPG](https://zh.wikipedia.org/wiki/File:Temple_of_People's_Liberation_Army.JPG "fig:Temple_of_People's_Liberation_Army.JPG")[福建省](../Page/福建省.md "wikilink")[惠安县](../Page/惠安县.md "wikilink")[崇武镇的](../Page/崇武镇.md "wikilink")[解放军庙供奉的是](../Page/解放军庙.md "wikilink")27名[解放军](../Page/解放军.md "wikilink")，匾額寫“天下第一奇庙”\]\]
**寺庙**，即[寺院](../Page/寺院.md "wikilink")（佛寺）與**庙宇**的合称，有時統稱為**廟**、**廟宇**，又稱**宮廟**\[1\]，一般專指東亞傳統宗教供奉[神灵](../Page/神灵.md "wikilink")、[神话或](../Page/神话.md "wikilink")[传说人物](../Page/传说.md "wikilink")、历代贤哲、历史著名人物的房屋式祭祀建筑，實際上不限於佛寺與廟宇，還包括[祠](../Page/祠.md "wikilink")、[道觀等](../Page/道觀.md "wikilink")。功能包括[祭祀以及讓信眾祈求](../Page/祭祀.md "wikilink")[庇佑的寺廟](../Page/庇佑.md "wikilink")。一般所稱寺廟不包括祭祀祖先的[家廟](../Page/家廟.md "wikilink")，但一些名人的家廟由於亦受宗族以外的信眾參拜，故同時有著寺廟的性質。

由於東亞傳統上常有不同宗教互相影響、習合的情況，如佛道雙修、[三教合流](../Page/三教合流.md "wikilink")、[神佛習合等](../Page/神佛習合.md "wikilink")，加上與民間信仰、[巫覡宗教合流](../Page/巫覡宗教.md "wikilink")，因此寺廟的名稱、性質多變，甚至同一間寺廟同時具備多種性質。

## 建築

除了一些單體式且不設前院的寺廟外，一般都有一個獨立的[門為入口](../Page/門_\(建築物\).md "wikilink")，如[神道坊](../Page/神道坊.md "wikilink")（寺廟[牌坊](../Page/牌坊.md "wikilink")）、[山門](../Page/山門.md "wikilink")、[鳥居等](../Page/鳥居.md "wikilink")，也代表進入[結界範圍](../Page/結界.md "wikilink")。門之後為[神道](../Page/神道_\(道路\).md "wikilink")，通往參拜之處。

寺廟的主體建築稱為[殿](../Page/殿.md "wikilink")，「殿」本指帝王居所，後來也指供佛寺神的屋宇，有些則稱為堂。一些規模較大的寺廟有多個不同的殿或堂。

有些寺廟周圍會有，本身有守護寺廟的神聖意義，也有些是作為[風水林](../Page/風水林.md "wikilink")。

## 類型與名稱

[儒教的寺廟包括祭祀](../Page/儒教.md "wikilink")[孔子的](../Page/孔子.md "wikilink")[孔廟](../Page/孔廟.md "wikilink")，以及祭祀聖賢、烈士的[祠](../Page/祠.md "wikilink")，如[忠烈祠](../Page/忠烈祠.md "wikilink")、[名宦祠等](../Page/名宦祠.md "wikilink")。但一些祭祀先賢的祠又被民眾視為祈福消災的神明，與民間信仰的結合，變成民間寺廟。

[寺院是](../Page/寺院.md "wikilink")[佛教僧侶修行](../Page/佛教.md "wikilink")、禮佛的屋宇，「寺」原係朝廷辦公官署，如[太常寺](../Page/太常寺.md "wikilink")、[大理寺等](../Page/大理寺.md "wikilink")，「院」原為官廨別稱。佛教傳入中國後，中國方將僧人居所也稱為寺或院，並隨著[漢傳佛教傳播](../Page/漢傳佛教.md "wikilink")，影響到[日本](../Page/日本.md "wikilink")、[朝鮮](../Page/朝鮮.md "wikilink")、[越南](../Page/越南.md "wikilink")、[琉球](../Page/琉球.md "wikilink")；[伊斯蘭教之禮拜堂也以寺稱之為](../Page/伊斯蘭教.md "wikilink")「[清真寺](../Page/清真寺.md "wikilink")」。[庵或](../Page/庵.md "wikilink")[庵堂本來是指結草為屋](../Page/庵堂.md "wikilink")，後指僧尼供佛的屋舍，如地藏庵，又常常專指僅有[女性僧人](../Page/女性.md "wikilink")（[比丘尼](../Page/比丘尼.md "wikilink")）居住、修行的場所。[巖本來指位於山窟或依山崖處據險而築的佛寺](../Page/巖仔.md "wikilink")，[洞係道教先真居住之地](../Page/洞.md "wikilink")，如「洞天福地」，後來在[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[越南等地](../Page/越南.md "wikilink")，因為佛教的世俗化，以及與民間信仰合流，成為廟的代名詞之一。

「觀」原指宮殿的高大門闕，後來用以指道士修道的居所，即道觀，也作為供奉道教先真的建築物，如「三清觀」、「[玄妙觀](../Page/玄妙觀.md "wikilink")」。一些規模較大的道觀會以「宮」命名。兩者又合稱「宮觀」。

民間信仰中的寺廟命名多元，按照規模，傳統上只有一些主神神格為帝、-{后}-、妃或[王爺級的寺廟才會用原指帝王居所的](../Page/王爺信仰.md "wikilink")「宮」、「殿」命名。例如祭祀[媽祖的](../Page/媽祖.md "wikilink")[媽祖廟](../Page/媽祖廟.md "wikilink")，由於媽祖先後在[元代](../Page/元代.md "wikilink")、[清代獲](../Page/清代.md "wikilink")[皇帝冊封為](../Page/皇帝.md "wikilink")「天妃」、「天后」，於是媽祖廟可命名為[天妃宮](../Page/天妃宮.md "wikilink")、[天后宮](../Page/天后宮.md "wikilink")；[關羽被尊為](../Page/關羽.md "wikilink")「協天大帝」、「伏魔大帝」，民間尊為「關帝」，於是一些[關帝廟也命名為](../Page/關帝廟.md "wikilink")「協天宮」；供奉[玄天上帝的廟宇有命名](../Page/玄天上帝.md "wikilink")「北極殿」、「玄帝殿」。而神格崇高的神明，其廟宇也會稱為「宮」，如[香港](../Page/香港.md "wikilink")、[澳門一些由非佛教徒開設的](../Page/澳門.md "wikilink")[觀音廟名為](../Page/觀音廟.md "wikilink")「水月宮」。

「府」原指百官所居之處，部份廟宇也以「府」命名，如[馬祖](../Page/馬祖.md "wikilink")[北竿鄉](../Page/北竿鄉.md "wikilink")「玉封蕭王府」(祀[蕭府太傅](../Page/蕭府太傅.md "wikilink"))、[雲林](../Page/雲林.md "wikilink")[水林鄉](../Page/水林鄉.md "wikilink")「水林通天府」(祀[溫府千歲](../Page/溫府千歲.md "wikilink"))等。

「」是紀念先賢的建築物，如祭祀祖先的「宗祠」、祭祀國家先烈的「忠烈祠」，供奉[土地公的廟宇也有作](../Page/土地公.md "wikilink")「土地祠」、「福德祠」。

「堂」是古代官吏審案的地方，也指屋舍的正廳，作為廟宇使用如「慈惠堂」、「勸化堂」，亦有作「某某佛堂」、「某某聖堂」等。

「壇」是舉行祭祀的高臺，如天壇、地壇；其中「天壇」也成為常見祭祀天公廟的名稱，如「[臺南天壇](../Page/臺南天壇.md "wikilink")」。另外，一般私人設立的神壇也常稱「某某壇」。

日本[神道教的寺廟稱為](../Page/神道教.md "wikilink")[神社](../Page/神社.md "wikilink")，其中祭祀[日本皇室祖先神](../Page/日本皇室.md "wikilink")、[日本天皇](../Page/日本天皇.md "wikilink")、作為[宗廟](../Page/宗廟.md "wikilink")，以及對於[大和平定有非常功績的特定神祇的神社稱為](../Page/大和.md "wikilink")[神宮](../Page/神宮.md "wikilink")。

越南傳統信仰中的寺廟，除了與中國相同的類型外，村社裡還有一種稱為的寺廟，供奉村社保護神。

琉球神道的祭祀場所稱為[御嶽](../Page/御嶽.md "wikilink")，其中一些屬於寺廟形式。

## 参见

  - [宗教场所](../Page/宗教场所.md "wikilink")
  - [大廟](../Page/大廟.md "wikilink")

## 外部連結

  - [中華民國內政部全國宗教資訊網](http://religion.moi.gov.tw/)
  - 宋光宇：〈[寺庙经营与社会变迁——以台北市碧山岩开漳圣王庙为例](http://www.nssd.org/articles/article_read.aspx?id=670499006)〉.
  - [香港的廟宇](https://web.archive.org/web/20120415133930/http://www.openlife.com.hk/%E9%A6%99%E6%B8%AF%E5%BB%9F%E5%AE%87%E5%90%8D%E5%86%8A)

## 參考來源

[寺廟](../Category/寺廟.md "wikilink")
[Category:宗教建築物](../Category/宗教建築物.md "wikilink")
[Category:東亞傳統祭祀建築物](../Category/東亞傳統祭祀建築物.md "wikilink")

1.