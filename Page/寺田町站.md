[Teradacho-station-manybicycles.jpg](https://zh.wikipedia.org/wiki/File:Teradacho-station-manybicycles.jpg "fig:Teradacho-station-manybicycles.jpg")
[Terada_station.jpg](https://zh.wikipedia.org/wiki/File:Terada_station.jpg "fig:Terada_station.jpg")
**寺田町站**（）是[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[大阪環狀線的車站](../Page/大阪環狀線.md "wikilink")。

## 歷史

  - 1932年7月16日：以城東線沿線車站之姿啟用。
  - 1961年4月25日：城東線改稱「大阪環狀線」。
  - 1987年4月1日：[日本國鐵分割民營化](../Page/日本國鐵分割民營化.md "wikilink")，本站劃歸JR西日本。

## 車站結構

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[高架車站](../Page/高架車站.md "wikilink")，由於沒有[轉轍器與絕對信號機](../Page/轉轍器.md "wikilink")，被分類為停留所。閘口有南口與北口2個。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>行先</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/大阪環狀線.md" title="wikilink">大阪環狀線</a></p></td>
<td><p>內環</p></td>
<td><p><a href="../Page/鶴橋站.md" title="wikilink">鶴橋</a>、<a href="../Page/京橋站_(大阪府).md" title="wikilink">京橋方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>外環</p></td>
<td><p><a href="../Page/天王寺站.md" title="wikilink">天王寺</a>、<a href="../Page/新今宮站.md" title="wikilink">新今宮方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 巴士路線

全為[大阪市營巴士](../Page/大阪市營巴士.md "wikilink")。

### 「寺田町站前」停留所

  - 9：[出戶巴士總站](../Page/出戶站.md "wikilink") 行、 行
  - 11：[北巽巴士總站](../Page/北巽站.md "wikilink") 行、 行
  - 13：[地下鐵今里](../Page/今里站_\(大阪市營地下鐵\).md "wikilink") 行、 行
  - 30：地下鐵動物園前 行、北巽巴士總站 行

## 相鄰車站

  - 西日本旅客鐵道

    大阪環狀線

      -

        大和路快速、區間快速、關空快速、紀州路快速、快速、直通快速、普通

          -
            [天王寺](../Page/天王寺站.md "wikilink")（JR-O01）－**寺田町（JR-O02）**－[桃谷](../Page/桃谷站.md "wikilink")（JR-O03）

## 外部連結

  -
[Radachou](../Category/日本鐵路車站_Te.md "wikilink")
[Category:大阪環狀線車站](../Category/大阪環狀線車站.md "wikilink")
[Category:天王寺區鐵路車站](../Category/天王寺區鐵路車站.md "wikilink")
[Category:1932年启用的铁路车站](../Category/1932年启用的铁路车站.md "wikilink")