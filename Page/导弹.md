[Peacekeeper_missile.jpg](https://zh.wikipedia.org/wiki/File:Peacekeeper_missile.jpg "fig:Peacekeeper_missile.jpg")[和平衛士洲際飛彈](../Page/和平衛士.md "wikilink")\]\]
[AIM-9_Sidewinder.jpg](https://zh.wikipedia.org/wiki/File:AIM-9_Sidewinder.jpg "fig:AIM-9_Sidewinder.jpg")[海軍](../Page/海軍.md "wikilink")[航空母艦上的地勤正在搬運](../Page/航空母艦.md "wikilink")[AIM-9響尾蛇式紅外線空對空](../Page/AIM-9響尾蛇飛彈.md "wikilink")-{zh-hans:导弹;
zh-hant:飛彈;}-\]\]
**飛彈**（）是本身有[動力](../Page/動力.md "wikilink")、有[誘導能力且在](../Page/精确制导武器.md "wikilink")[空氣或是](../Page/空氣.md "wikilink")[太空中移動的](../Page/太空.md "wikilink")[彈藥](../Page/彈藥.md "wikilink")。有誘導能力但没有動力的彈藥稱作[導引炸彈](../Page/導引炸彈.md "wikilink")，有動力但是沒有誘導能力的彈藥稱作[火箭彈](../Page/火箭彈.md "wikilink")。

在导弹的[制导的分類上通常有兩類](../Page/导弹制导.md "wikilink")，一種是訊號傳送媒體的不同，如：[有線導引](../Page/有線導引.md "wikilink")、[雷達導引](../Page/雷達導引.md "wikilink")、[紅外線導引](../Page/紅外線導引.md "wikilink")、[雷射導引](../Page/雷射導引.md "wikilink")、[電視導引等](../Page/電視導引.md "wikilink")。另外一種分類是飛彈的制导方式的不同，如：[慣性導引](../Page/慣性導引.md "wikilink")、[乘波導引](../Page/乘波導引.md "wikilink")、[主動雷達導引和](../Page/主動雷達導引.md "wikilink")[指揮至瞄準線導引等](../Page/指揮至瞄準線導引.md "wikilink")。

按照导弹的作用分類可以简单地分为[战略导弹和](../Page/战略.md "wikilink")[战术导弹](../Page/战术.md "wikilink")。

飛彈的分類原則是由兩個部分所構成：發射載具的特性與攻擊目標的性質。發射載具的特性包括：空射，面射，潛射，艦射等。攻擊的目標性質包括：對空，對面，對潛，對艦。把這兩項原則合併在一起就是目前最常見的各類飛彈的分類系統。雖然發射載具是飛彈分類的一項原則，不過同一種飛彈往往可以在簡單的改裝下自不同的載具上發射，因此許多飛彈往往會在不同的類別當中重複出現。譬如說[魚叉反艦飛彈可以自](../Page/AGM-84魚叉反艦飛彈.md "wikilink")[潛艇](../Page/潛艇.md "wikilink")、[水面艦艇與飛機上發射](../Page/水面艦艇.md "wikilink")，因此它會分別出現在**潛射反艦飛彈**、**艦射反艦飛彈**以及**空射反艦飛彈**當中。

## 詞源

飛彈的英文「Missile」的一般意義即是「被投射出來的物體」的意思，其範圍包括石子、彈頭、箭矢等等\[1\]\[2\]；在1945年之後，「Missile」才開始專門指有導引系統的火箭，即現在的飛彈意義\[3\]。

## 导弹的种类

### 面對面飛彈

**面對面飛彈**代表發射的位置和攻擊的目標都是在陸地(地上或地下)或是水中(水上或水下)，這一類的飛彈還包括：

#### [弹道导弹](../Page/弹道导弹.md "wikilink")

##### 按射程分类

  - [洲际弹道导弹](../Page/洲际弹道导弹.md "wikilink")（ICBM :
    **I**nter**c**ontinental **B**allistic **M**issile），射程为6000公里以上。
  - [远程弹道导弹](../Page/远程弹道导弹.md "wikilink")（IRBM :
    **I**ntermediate-**R**ange **B**allistic
    **M**issile），射程2000公里～6000公里。
  - [中程弹道导弹](../Page/中程弹道导弹.md "wikilink")（MRBM：**M**edium-**R**ange**B**allistic**M**issile），射程800公里～1600公里。
  - [短程弹道导弹](../Page/短程弹道导弹.md "wikilink")（SRBM : **S**hort-**R**ange
    **B**allistic **M**issile），射程約800公里以下。

##### 按发射平台

  - 陆基弹道导弹
  - [潜射弹道导弹](../Page/潜射弹道导弹.md "wikilink")（SLBM :
    **S**ubmarine-**L**aunched **B**allistic **M**issile）

#### [巡弋飛彈](../Page/巡弋飛彈.md "wikilink")

巡弋飛彈是依靠喷气发动机的推力和弹翼的气动升力，主要以巡航状态在大气层内飞行的导弹，曾被称为飞航式导弹。它可从空中、地面、地下、水面或水下發射，攻击地面、地下、水面固定目標或移動目標。

  - [MGM-1C屠牛士](../Page/MGM-1C.md "wikilink")（Matador）戰術巡弋飛彈
  - [PJ-10](../Page/PJ-10.md "wikilink")[布拉莫斯飛彈](../Page/布拉莫斯飛彈.md "wikilink")：（BrahMos）超音速巡弋飛彈
  - [BGM-109战斧](../Page/戰斧巡弋飛彈.md "wikilink")（Tomahawk）巡弋飛彈
  - [雄風二E巡弋飛彈](../Page/雄風二E巡弋飛彈.md "wikilink")
  - [雲峰飛彈](../Page/雲峰飛彈.md "wikilink")
  - [萬劍機場聯合遙攻武器](../Page/萬劍機場聯合遙攻武器.md "wikilink")
  - [长剑-10导弹](../Page/长剑-10导弹.md "wikilink")

#### [反艦飛彈](../Page/反艦飛彈.md "wikilink")

  - [远程反艦飛彈](../Page/远程反艦飛彈.md "wikilink")
  - [MM-38、MM-40飛鱼](../Page/飛魚反艦飛彈.md "wikilink")（Exocet）反艦飛彈（陸射型或艦射型）
  - [SM-39飛魚](../Page/飛魚反艦飛彈.md "wikilink")（Expect）反艦飛彈（潛射型）
  - [AM-39飛魚](../Page/飛魚反艦飛彈.md "wikilink")（Exocet）反艦飛彈（空射型）
  - [AGM-84魚叉](../Page/魚叉反艦飛彈.md "wikilink")（Harpoon）反艦飛彈（空射型）
  - [RGM-84魚叉](../Page/魚叉反艦飛彈.md "wikilink")（Harpoon）反艦飛彈（陸射/艦射型）
  - [UGM-84魚叉](../Page/魚叉反艦飛彈.md "wikilink")（Harpoon）反艦飛彈（潛射型）
  - [雄風一型反艦飛彈](../Page/雄風一型反艦飛彈.md "wikilink")（艦射型）
  - [雄風二型反艦飛彈](../Page/雄風二型反艦飛彈.md "wikilink")（陸射/艦射/空射型）
  - [雄風三型反艦飛彈](../Page/雄風三型反艦飛彈.md "wikilink")（陸射/艦射型）
  - [SS-N-19花崗岩](../Page/P-700导弹.md "wikilink")（Shipwreck）反艦飛彈
  - [SS-N-22日炙](../Page/SS-N-22导弹.md "wikilink")（Sunburn）反艦飛彈
  - [SS-N-26寶石](../Page/SS-N-26导弹.md "wikilink")（Yakhont）反艦飛彈
  - [88式陸基反艦飛彈](../Page/88式陸基反艦飛彈.md "wikilink")

#### [反潜飛彈](../Page/反潜飛彈.md "wikilink")

  - [RUR-5阿斯洛克](../Page/阿斯洛克反潜导弹.md "wikilink")(ASROC)

#### [反裝甲飛彈](../Page/反裝甲飛彈.md "wikilink")

  - [BGM-71拖式](../Page/拖式飛彈.md "wikilink")(TOW)
  - [Milan米蘭](../Page/米蘭飛彈.md "wikilink")(MiIan)
  - [FGM-148標槍](../Page/標槍飛彈.md "wikilink")(Javelin)
  - [AGM-114地獄火](../Page/地獄火飛彈.md "wikilink")(Hellfire)
  - [9M133短號](../Page/短號飛彈.md "wikilink")(Kornet)
  - [昆吾飛彈](../Page/昆吾飛彈.md "wikilink")

### [面對空飛彈或是](../Page/面對空飛彈.md "wikilink")[防空飛彈](../Page/防空飛彈.md "wikilink")

**面對空飛彈**泛指由陸上或是艦上（潛艇上的非常少見，可以歸類到艦上發射）的對空飛彈。這類飛彈包括：
[Romanian_MIM-23_HAWK.jpg](https://zh.wikipedia.org/wiki/File:Romanian_MIM-23_HAWK.jpg "fig:Romanian_MIM-23_HAWK.jpg")\]\]
[RIM-8_Talos_launched.JPEG](https://zh.wikipedia.org/wiki/File:RIM-8_Talos_launched.JPEG "fig:RIM-8_Talos_launched.JPEG")防空飛彈\]\]

#### 反彈道飛彈

用來擊落彈道飛彈的飛彈，一般使用核彈頭，利用核彈強大的爆炸震波摧毀目標，並且可加大有效殺傷半徑，以彌補高速飛行下過大的誤差。由於美蘇簽定了反彈道飛彈條約，雙方只能在首都附近佈署，數量不足使其失去研發的意義，當年的反彈道飛彈已經停止研發。但是由於美國展開了戰區飛彈防禦（TMD）與國家飛彈防禦（NMD）的計劃，新一波的反彈道飛彈研發風潮也有展開的可能。而現在的反彈道飛彈發展趨勢，是使用較沒有核污染危險的傳統彈頭，以近距離爆炸的破片，或是以直接撞擊的方式擊毀目標。

#### 地對空飛彈

地對空飛彈還可以區分為短程的點防禦防空飛彈、中長程的區域防空飛彈與輕便的肩射防空飛彈。

請參考[面對空飛彈中的飛彈列表](../Page/面對空飛彈.md "wikilink")。

#### 艦對空飛彈

與地對空飛彈類似，但是裝設在船艦上，用來保護船艦與艦隊。艦對空飛彈還可以分成近距離的點防禦防空飛彈以及中長程的區域防空飛彈。

請參考[面對空飛彈中的飛彈列表](../Page/面對空飛彈.md "wikilink")。

  - [RIM-66飛彈](../Page/RIM-66飛彈.md "wikilink")
  - [RIM-161標準三型飛彈](../Page/RIM-161標準三型飛彈.md "wikilink")
  - [闪电-8导弹](../Page/闪电-8导弹.md "wikilink")

### [空對空飛彈](../Page/空對空飛彈.md "wikilink")

請參考[空對空飛彈當中的各國飛彈列表](../Page/空對空飛彈.md "wikilink")。

#### [反衛星飛彈](../Page/反衛星飛彈.md "wikilink")

用來擊毀敵人的[人造衛星](../Page/人造衛星.md "wikilink")，通常有地面發射和飛機發射兩種，由於研發成本高昂以及國際間對[反衛星飛彈研發計畫抱持負面態度](../Page/反衛星飛彈.md "wikilink")，目前沒有任何國家公開表示正在進行研發或者是有現役的系統，但是，[美國與](../Page/美國.md "wikilink")[中國大陸此前都進行過地面飛彈摧毀太空衛星的實驗](../Page/中國大陸.md "wikilink")，並且都獲得了成功。

  - [ASM-135](../Page/ASM-135.md "wikilink")

### 空對面飛彈

[空對面飛彈是泛稱](../Page/空對面飛彈.md "wikilink"):從空中載具發射攻擊地表上的目標。

**對面**又可分成對地與對海，因為海面與地面的目標型態有些許不同，不過有些飛彈還是可以兼顧兩種目標。

#### 空对地飛彈

  - [AGM-12犢牛犬](../Page/AGM-12飛彈.md "wikilink")（Bullpup）空對地飛彈
  - [AGM-65小牛](../Page/AGM-65小牛飛彈.md "wikilink")（Maverick）式空對地飛彈

#### 空射反戰車飛彈

  - [AGM-114地獄火式](../Page/AGM-114地獄火飛彈.md "wikilink")（Hellfire）反戰車飛彈

#### [空對艦飛彈](../Page/反艦飛彈.md "wikilink")

  - [AM-39飛鱼](../Page/AM-39.md "wikilink")（Exocet）反艦飛彈
  - [AGM-84魚叉式](../Page/AGM-84魚叉反艦飛彈.md "wikilink")（Harpoon）反艦飛彈
  - [雄風二型反艦飛彈](../Page/雄風二型反艦飛彈.md "wikilink")（陸射/艦射/空射型）
  - [AGM-119企鵝式](../Page/AGM-119企鵝反艦飛彈.md "wikilink")（Penguin）反艦飛彈
  - [Kh-31氪式](../Page/Kh-31导弹.md "wikilink")（Krypton）反艦飛彈
  - [Kh-41蚊式](../Page/Kh-41导弹.md "wikilink")（Moskit）反艦飛彈
  - [鷹擊8反艦導彈](../Page/鷹擊8反艦導彈.md "wikilink")
  - [AS-1狗窝](../Page/AS-1.md "wikilink")（Kennel）空对地导弹
  - [AS-2鳟鱼](../Page/AS-2.md "wikilink")（Kipper）空对地导弹
  - [AS-3袋鼠](../Page/AS-3.md "wikilink")（kangaroo）空对地导弹
  - [AS-4厨房](../Page/AS-4.md "wikilink")（Kitchen）空对地导弹
  - [AS-5鲑鱼](../Page/AS-5.md "wikilink")（Kelt）空对地导弹

#### [反辐射导弹](../Page/反辐射导弹.md "wikilink")

反輻射飛彈是專門攻擊發射無線電訊號源的目標，其中又以針對[雷達為大宗](../Page/雷達.md "wikilink")。

  - [AGM-45百舌鳥飛彈](../Page/AGM-45百舌鳥飛彈.md "wikilink")
  - [AGM-78標準飛彈](../Page/AGM-78標準飛彈.md "wikilink")
  - [AGM-88高速反輻射飛彈](../Page/AGM-88.md "wikilink")（HARM）
  - [AGM-122佩枪飛彈](../Page/AGM-122佩枪飛彈.md "wikilink")
  - [AGM-136沉默彩虹](../Page/AGM-136.md "wikilink")（Tacit Rainbow）飛彈
  - [天劍二A反輻射飛彈](../Page/天劍二型飛彈.md "wikilink")

#### [空射巡弋飛彈](../Page/空射巡弋飛彈.md "wikilink")

現在已經發展出由空中發射的巡弋飛彈,它最初是用來增加一些較低速的轟炸機的生存機率:例如

  - [AGM-86飛彈](../Page/AGM-86飛彈.md "wikilink")
  - [AGM-158聯合空對地距外飛彈](../Page/AGM-158聯合空對地距外飛彈.md "wikilink")
  - [金牛座KEPD 350飛彈](../Page/金牛座KEPD_350飛彈.md "wikilink")

#### [超音速飛彈](../Page/超音速飛彈.md "wikilink")

俄國展示了最新研發的匕首高超音速飛彈，總統普亭稱其能在反彈道飛彈防禦系統周圍以10倍音速的速度飛行。10倍音速約為時速1.2萬公里

## 参阅

  - [-{zh-hans:宙斯盾; zh-hant:神盾;}-戰鬥系統](../Page/神盾戰鬥系統.md "wikilink")
  - [飛彈導引](../Page/飛彈導引.md "wikilink")
  - [電子作戰](../Page/電子作戰.md "wikilink")
  - [雷達](../Page/雷達.md "wikilink")
  - [火器](../Page/火器.md "wikilink")

## 参考文献

{{-}}

[Category:军用飞行器类型](../Category/军用飞行器类型.md "wikilink")
[Category:武器类型](../Category/武器类型.md "wikilink")
[导弹](../Category/导弹.md "wikilink")
[Category:爆炸物](../Category/爆炸物.md "wikilink")

1.  <http://dictionary.reference.com/browse/missile>
2.  [Guardian newspaper: "Emmanuel Eboué pelted with missiles while
    playing for
    Galatasaray"](http://www.guardian.co.uk/football/2011/nov/21/emmanuel-eboue-galatasaray-besiktas)
    Example of ordinary English usage. In this case the missiles were
    bottles and cigarette lighters
3.  [missile](http://www.etymonline.com/index.php?term=missile&allowed_in_frame=0)