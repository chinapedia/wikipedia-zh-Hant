[Type022.png](https://zh.wikipedia.org/wiki/File:Type022.png "fig:Type022.png")\]\]
[FACG-74_in_No.13_Pier_Right_Front_View_20130504.jpg](https://zh.wikipedia.org/wiki/File:FACG-74_in_No.13_Pier_Right_Front_View_20130504.jpg "fig:FACG-74_in_No.13_Pier_Right_Front_View_20130504.jpg")[光華六號飛彈快艇](../Page/光華六號飛彈快艇.md "wikilink")\]\]
[Norwegian_missile_patrol_craft_KNM_Skjold_(P_690)_(2_Nov_2001).jpg](https://zh.wikipedia.org/wiki/File:Norwegian_missile_patrol_craft_KNM_Skjold_\(P_690\)_\(2_Nov_2001\).jpg "fig:Norwegian_missile_patrol_craft_KNM_Skjold_(P_690)_(2_Nov_2001).jpg")\]\]

**飛彈快艇**是以[反艦导弹为主要武器](../Page/反艦导弹.md "wikilink")，用于近海作战的小型战斗[舰艇](../Page/舰艇.md "wikilink")。除了執行攻击任務以外，也可担负巡逻、警戒、反潜、布雷等其他任务，已經取代過去[魚雷快艇在水面作戰中的角色](../Page/魚雷快艇.md "wikilink")。

导弹快艇吨位小，航速高，机动灵活，[排水量通常为数十吨至数百吨](../Page/排水量.md "wikilink")，航行速度30到40节，有的可达50节，续航能力500－3000海里。艇上装有反艦导弹2至8枚，有些快艇還加裝20至76毫米口徑舰炮，噸位較大的快艇還可能包含[鱼雷](../Page/鱼雷.md "wikilink")、[水雷](../Page/水雷.md "wikilink")、[深水炸弹和](../Page/深水炸弹.md "wikilink")[舰对空导弹等](../Page/面對空飛彈.md "wikilink")。搭配的感測系統有搜索、探测、武器控制、通信导航、[电子作戰等](../Page/电子作戰.md "wikilink")。

## 歷史

飛彈快艇第一次使用是在1963年10月20日，[埃及海軍兩艘](../Page/埃及海軍.md "wikilink")[蘇聯提供的](../Page/蘇聯.md "wikilink")[科馬級飛彈快艇以](../Page/科馬級飛彈快艇.md "wikilink")[冥河反艦飛彈擊沉](../Page/冥河反艦飛彈.md "wikilink")[以色列的](../Page/以色列.md "wikilink")[艾略特號驅逐艦](../Page/艾略特號驅逐艦.md "wikilink")。自此以后飛彈快艇在局部战争中得到广泛运用，战果显赫，为越来越多的国家所重视。以色列後來吸取了此一戰役的慘痛教訓，發展出了[毒蜂級飛彈快艇與其搭載的](../Page/海鷗級飛彈快艇.md "wikilink")[天使飛彈](../Page/天使飛彈.md "wikilink")，並於[贖罪日戰爭成功反擊埃及海軍](../Page/贖罪日戰爭.md "wikilink")。

近年[低可偵測性技術開始大幅應用在艦艇上](../Page/匿蹤.md "wikilink")，新開發的飛彈快艇多數也開始採用斜角設計與吸收雷達波材料。由於飛彈快艇重視速度，因此不少開始採用[雙體船](../Page/雙體船.md "wikilink")、[氣墊船](../Page/氣墊船.md "wikilink")、[水翼船](../Page/水翼船.md "wikilink")……等高速船船體設計。

國際上的主流飛彈快艇，包含了[中國人民解放軍海軍的](../Page/中國人民解放軍海軍.md "wikilink")[22型导弹艇](../Page/22型导弹艇.md "wikilink")、[中華民國海軍的](../Page/中華民國海軍.md "wikilink")[光華六號飛彈快艇](../Page/光華六號飛彈快艇.md "wikilink")、[挪威海軍的](../Page/挪威軍隊.md "wikilink")[盾牌級飛彈快艇](../Page/盾牌級飛彈快艇.md "wikilink")、[美國海軍的](../Page/美國海軍.md "wikilink")[飛馬級水翼船](../Page/飛馬級水翼船.md "wikilink")、[蘇聯海軍的](../Page/蘇聯海軍.md "wikilink")[科馬級飛彈快艇](../Page/科馬級飛彈快艇.md "wikilink")、[奧薩級飛彈快艇](../Page/奧薩級飛彈快艇.md "wikilink")、[毒蜘蛛級護衛艦等等](../Page/毒蜘蛛級護衛艦.md "wikilink")。

## 外部連結

  - [Saar](https://web.archive.org/web/20071116060136/http://israeli-weapons.com/weapons/naval/saar/Saar.html)

[Category:军舰](../Category/军舰.md "wikilink")