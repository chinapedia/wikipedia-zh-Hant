在[数学](../Page/数学.md "wikilink")，特别是[点集拓扑学中](../Page/点集拓扑学.md "wikilink")，[拓扑空间的子集](../Page/拓扑空间.md "wikilink")\(S\)的**导集**（**导出集合**）是\(S\)的所有[极限点的集合](../Page/极限点.md "wikilink")。它通常記为
\(S'\)。

这个概念是[格奥尔格·康托尔在](../Page/格奥尔格·康托尔.md "wikilink")1872年引入的，他开发[集合论很大程度上就是为了研究在](../Page/集合论.md "wikilink")[实直线上的导出集合](../Page/实直线.md "wikilink")。

## 导集公理

导集是[拓扑学的基础概念之一](../Page/拓扑学.md "wikilink")，可以用来定义[拓扑空间](../Page/拓扑空间.md "wikilink")。
给定集合\(X\)，考慮一個定義在\(X\)的[冪集](../Page/冪集.md "wikilink")\(\mathcal{P}(X)\)上的运算\(d: \mathcal{P}(X) \to \mathcal{P}(X)\)，若\(d\)满足以下**导集公理**，則稱\(d\)為**導集運算**：

  - **D<sub>1</sub>**：\(d(\empty) = \empty\)
  - **D<sub>2</sub>**：\(d(d(A)) \subseteq d(A) \cup A\)
  - **D<sub>3</sub>**：\(\forall x \in X,\ d(A) = d(A - \{x\})\)
  - **D<sub>4</sub>**：\(d(A\cup B) = d(A)\cup d(B)\)

\(d(A)\)稱為\(A\)的**導來集**。

从导集出发可以定义各种拓扑的基础概念：

  - [闭集](../Page/闭集.md "wikilink")：\(X\)的子集\(A\)是闭集，当且仅当\(d(A) \subseteq A\)。（从此处可以看到和闭集公理的等价性，从而可以等价地定义拓扑空间。）
  - [同胚](../Page/同胚.md "wikilink")：拓扑空间\(T_1(X_1,\tau_1)\)、\(T_1(X_2,\tau_2)\)同胚，当且仅当存在[双射](../Page/双射.md "wikilink")\(f: \mathcal{P}(X_1) \to \mathcal{P}(X_2)\)，使得\(\forall A \subseteq X_1,\ f(d(A)) = d(f(A))\)。

## 相关概念

  - 聚点
    \(d(A)\)中的点称为\(A\)的**聚点**。

## 性质

  - \(S,T\subseteq X\)，若\(S\cap T=\empty\)，\(S\cap d(T)=\empty\)，\(d(S)\cap T=\empty\)。则称\(S\)和\(T\)是[分离的](../Page/分离集合.md "wikilink")。（注意：\(d(S)\cap d(T)\)不一定为\(\empty\)）。
  - 集合\(S\)被定义为**[完美](../Page/完美集合.md "wikilink")**的，如果\(S=d(S)\)。等价地说，完美集合是没有[孤点的](../Page/孤点.md "wikilink")[闭集](../Page/闭集.md "wikilink")。完美集合又称为完备集合。
  - **[Cantor-Bendixson定理](../Page/Cantor-Bendixson定理.md "wikilink")**声称任何[波兰空间都可以写为可数集合和完美集合的的并集](../Page/波兰空间.md "wikilink")。因为任何波兰空间的\(G_\delta\)子集都再次是波兰空间，这个定理还证明了任何波兰空间的\(G_\delta\)子集都是可数集合和完美集合的并集。
  - 拓扑空间\(X\)是[T<sub>1</sub>
    空间](../Page/T1空间.md "wikilink")，当且仅当\(\forall x\in X,\ d(\{x\}) = \empty\)。

## 引用

  -
  - [Sierpiński, Wacław F.](../Page/Wacław_Sierpiński.md "wikilink");
    translated by [Krieger, C.
    Cecilia](../Page/Cecilia_Krieger.md "wikilink") (1952). *General
    Topology*. [University of
    Toronto](../Page/University_of_Toronto.md "wikilink") Press.

## 参见

  - [极限点](../Page/极限点.md "wikilink")
  - [导出代数](../Page/导出代数.md "wikilink")

[D](../Category/点集拓扑学.md "wikilink")