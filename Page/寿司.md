[Plateau_de_sushis_au_restaurant_japonais_Au_soleil_levant_de_Colmar_en_2013_2.jpg](https://zh.wikipedia.org/wiki/File:Plateau_de_sushis_au_restaurant_japonais_Au_soleil_levant_de_Colmar_en_2013_2.jpg "fig:Plateau_de_sushis_au_restaurant_japonais_Au_soleil_levant_de_Colmar_en_2013_2.jpg")
**寿司**（）是一種[日本料理](../Page/日本料理.md "wikilink")，主要材料是用[醋](../Page/醋.md "wikilink")、[糖](../Page/糖.md "wikilink")、[鹽調味過](../Page/食盐.md "wikilink")，以肥小而稍帶甜味的日本[珍珠米所煮成的](../Page/粳稻.md "wikilink")[醋飯](../Page/醋飯.md "wikilink")，降溫後加上[魚肉](../Page/魚肉.md "wikilink")、[海鮮](../Page/海鮮.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、[雞蛋或其他](../Page/雞蛋.md "wikilink")[肉類等作配料](../Page/肉類.md "wikilink")。醋飯原為一種保存食物的方法：以[醋酸令食物的](../Page/醋酸.md "wikilink")[酸鹼值下降](../Page/酸鹼值.md "wikilink")，抑制[微生物生長](../Page/微生物.md "wikilink")，以减缓食物[腐敗](../Page/腐敗.md "wikilink")\[1\]。

壽司既可以作为[小吃也可以作正餐](../Page/小吃.md "wikilink")，配料可以是生的、也可以是熟的，又或者是醃過的。視乎配料的不同，不同壽司的价格、檔次差距甚大。

壽司除了是日本人喜愛的食物之外\[2\]，在日本以外地區也十分流行，世界各地[迴轉壽司式的壽司店與包裝壽司也不胜其数](../Page/迴轉壽司.md "wikilink")。

## 種類

[Flower_Sushi.jpg](https://zh.wikipedia.org/wiki/File:Flower_Sushi.jpg "fig:Flower_Sushi.jpg")
[Sushi.png](https://zh.wikipedia.org/wiki/File:Sushi.png "fig:Sushi.png")
[Sasazushi.jpg](https://zh.wikipedia.org/wiki/File:Sasazushi.jpg "fig:Sasazushi.jpg")
[Golden_Maki_Rainbow_Roll_sushi.jpg](https://zh.wikipedia.org/wiki/File:Golden_Maki_Rainbow_Roll_sushi.jpg "fig:Golden_Maki_Rainbow_Roll_sushi.jpg")

  - **熟壽司（）**：以[鹽](../Page/食鹽.md "wikilink")、米飯[醃漬魚肉](../Page/醃.md "wikilink")，以[乳酸菌](../Page/乳酸菌.md "wikilink")[發酵後做成](../Page/發酵.md "wikilink")，可長久保存，但具有濃烈氣味。[滋賀縣](../Page/滋賀縣.md "wikilink")[琵琶湖市町村的傳統食品](../Page/琵琶湖.md "wikilink")「[鯽魚熟壽司](../Page/鯽魚.md "wikilink")」、[和歌山縣的](../Page/和歌山縣.md "wikilink")[鯖魚熟壽司和](../Page/鯖魚.md "wikilink")[秋刀魚熟壽司屬此](../Page/秋刀魚.md "wikilink")\[3\]。
  - **[卷壽司](../Page/卷壽司.md "wikilink")**：在小竹簾上面铺一层[海苔](../Page/海苔.md "wikilink")（紫菜），再铺一层米飯，中間放上配料，卷起来成一長卷，然後切成小段。
      - **太-{卷}-**，是直徑比較粗的一種-{卷}-壽司，通常有數種配料。
      - **細-{卷}-**，顧名思義，就是比較幼細的，通常只含一種配料。如鐵火卷。
      - **手-{卷}-**，把壽司捲成圓錐體狀（類似冰淇淋[甜筒](../Page/甜筒.md "wikilink")），比較難用筷子夹，所以通常用手拿著吃。
      - **裏-{卷}-**，反過來用海苔裹着最中心的配料，再裹米飯。最外面灑一層或有[芝麻](../Page/芝麻.md "wikilink")、[魚卵](../Page/魚卵.md "wikilink")、[蟹卵等](../Page/蟹卵.md "wikilink")。
      - **軍艦-{卷}-**，米飯用海苔裹成橢圓形狀，配料放上面。專門用於鮭魚子、海膽等較難捏製的食材。
      - **素-{卷}-**，僅用[醋飯與海苔捲成壽司卷](../Page/醋飯.md "wikilink")，較常見美食家用來品嘗店家純粹的醋飯味道。
  - **笹壽司（）**，又稱做木條壽司或一夜壽司，主要流行於日本[關西](../Page/近畿地方.md "wikilink")，是用長型小木箱（押箱）輔助製作壽司。製作者先把配料鋪在押箱的最底層，再放上米飯，然後用力把箱的蓋子壓下去。作成的壽司會變成四方形，最後切成一口塊。
  - **握壽司（）**，（日本[江戶時代文化](../Page/江戶時代.md "wikilink")、文政年間興起）製作者用手把米飯捏成一口大小，在配料上塗上一層[山葵](../Page/山葵.md "wikilink")，最後將米飯與配料結合，用食、中指壓製。視配料種類之不同，有時會用一塊海苔把兩者縛在一起。在日本，若不加說明的話“壽司”一詞多是指握壽司。
  - **[豆皮壽司](../Page/豆皮壽司.md "wikilink")**，用配料裝着米飯。常見配料是油炸[豆腐皮](../Page/豆腐.md "wikilink")、煎[雞蛋](../Page/雞蛋.md "wikilink")、[高麗菜](../Page/高麗菜.md "wikilink")（椰菜）、滷香菇、筍子、胡蘿蔔等。
  - **散壽司（）**與之前所描述的壽司稍有不同。
      - **江戶前散壽司（）**，常見於[關東地區](../Page/關東.md "wikilink")，配料灑在盛在碗裏的米飯上。
      - **五目散壽司（）**，常見於[關西地區](../Page/近畿地方.md "wikilink")，配料拌進盛在碗中的米飯里。

## 常見配料

  - [生魚片](../Page/生魚片.md "wikilink")：[鮭魚](../Page/鮭魚.md "wikilink")（三文魚）、[鮪魚](../Page/鮪魚.md "wikilink")（金槍魚、吞拿魚）、[鰤魚](../Page/鰤魚.md "wikilink")（黃尾魚）、[鯛魚](../Page/鯛.md "wikilink")、[鰹魚](../Page/鰹魚.md "wikilink")、[鯖魚](../Page/鯖魚.md "wikilink")、[鯽魚](../Page/鯽魚.md "wikilink")、-{A|zh-hans:[针鱼](../Page/针鱼.md "wikilink");zh-hant:[鱵魚](../Page/鱵魚.md "wikilink");}-、[旗魚](../Page/旗魚.md "wikilink")、[比目魚](../Page/比目魚.md "wikilink")、[鰈魚](../Page/鰈魚.md "wikilink")、[河豚](../Page/河豚.md "wikilink")、[竹莢魚](../Page/竹莢魚.md "wikilink")、[烏魚](../Page/烏魚.md "wikilink")
  - 各類[海鮮](../Page/海鮮.md "wikilink")：[烏賊](../Page/烏賊.md "wikilink")（墨魚）、[八爪魚](../Page/八爪魚.md "wikilink")、[螃蟹](../Page/螃蟹.md "wikilink")、[蝦](../Page/蝦.md "wikilink")、[蝦蛄](../Page/蝦蛄.md "wikilink")、[鰻魚](../Page/鰻魚.md "wikilink")、[星鰻](../Page/穴子魚.md "wikilink")、[魚卵](../Page/魚卵.md "wikilink")、[海膽](../Page/海膽.md "wikilink")、[北寄貝](../Page/北寄貝.md "wikilink")、[鮭魚卵](../Page/鮭魚卵.md "wikilink")、[飛魚卵](../Page/飛魚卵.md "wikilink")、[蝦卵等](../Page/蝦卵.md "wikilink")
  - 果菜：醃[蘿蔔](../Page/蘿蔔.md "wikilink")、醃[梅子](../Page/梅子.md "wikilink")、[納豆](../Page/納豆.md "wikilink")、[鱷梨](../Page/鱷梨.md "wikilink")（牛油果）、[黃瓜](../Page/黃瓜.md "wikilink")（青瓜）、[炸豆腐](../Page/炸豆腐.md "wikilink")、[豆皮](../Page/豆皮.md "wikilink")、醃[蘿蔔](../Page/蘿蔔.md "wikilink")、[紫蘇](../Page/紫蘇.md "wikilink")、[瓠瓜](../Page/瓠瓜.md "wikilink")、[香菇](../Page/香菇.md "wikilink")
  - [紅肉](../Page/紅肉.md "wikilink")：[牛肉](../Page/牛肉.md "wikilink")、[馬肉](../Page/馬肉.md "wikilink")、[火腿](../Page/火腿.md "wikilink")、[鯨魚](../Page/鯨魚.md "wikilink")
  - 其他：煎[雞蛋](../Page/雞蛋.md "wikilink")、生[鵪鶉蛋](../Page/鵪鶉.md "wikilink")

## 常見佐料

  - 壽司[醬油](../Page/醬油.md "wikilink")
  - [山葵](../Page/山葵.md "wikilink")
  - 紫[薑](../Page/薑.md "wikilink")
  - [紫蘇葉](../Page/紫蘇.md "wikilink")
  - [味醂](../Page/味醂.md "wikilink")
  - [蘿蔔](../Page/蘿蔔.md "wikilink")

## 字源與歷史

[Hiroshige_Bowl_of_Sushi.jpg](https://zh.wikipedia.org/wiki/File:Hiroshige_Bowl_of_Sushi.jpg "fig:Hiroshige_Bowl_of_Sushi.jpg")
壽司的畫\]\]
[Flickr_-_cyclonebill_-_Sushi_(3).jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_cyclonebill_-_Sushi_\(3\).jpg "fig:Flickr_-_cyclonebill_-_Sushi_(3).jpg")
 壽司起源於東南亞的湄公河沿岸，在八世紀左右引入日本。

### 鮨、鮓之字源

壽司亦作「[鮨](../Page/wikt:鮨.md "wikilink")」，鮨字首先出現於前3世紀至前4世紀，原意是攪碎的魚肉\[4\]。在日本，「鮨」字最早出現於718年的《[養老律令](../Page/養老律令.md "wikilink")》中，當中提及國民要繳付「」，不過這個「鮨」指的是甚麼已無從稽考。

壽司的另一寫法為「[鮓](../Page/wikt:鮓.md "wikilink")」，鮓字出現於2世紀，原意是一種用鹽、米等醃製，讓魚肉發酵後剁碎，煮熟後進食的食物\[5\]。

### 日本的壽司

**[迴轉壽司](../Page/迴轉壽司.md "wikilink")**是壽司餐廳的一種。師傅把製作好的壽司放在盤子後擺在運輸帶上，運輸帶圍繞餐廳的坐檯而行。顧客雖然可以要求師傅個別製作壽司，不過大部分還是於運輸帶上挑選，另外在一般的迴轉壽司餐廳中，會提供[杏仁豆腐](../Page/杏仁豆腐.md "wikilink")、[布丁](../Page/布丁.md "wikilink")、[羊羹](../Page/羊羹.md "wikilink"),花生等傳統壽司店不提供的中西日式甜點。

## 衛生問題

壽司常以[生魚片做為材料](../Page/生魚片.md "wikilink")，如處理不當
就可能有[病菌和](../Page/病菌.md "wikilink")[寄生蟲](../Page/寄生蟲.md "wikilink")，尤其是[線蟲](../Page/線蟲動物門.md "wikilink")\[6\]。因此壽司對於食材與處理過程
要求較高。

## 圖庫

Image:Shrimpnigiri.jpg|[海老握寿司](../Page/海老.md "wikilink") Image:Salmon
nigiri.jpg|鲑鱼握寿司(香港稱為三文魚握壽司) Image:Tuna Sushi.jpg| 鮪魚寿司 Image:Salmon
sushi.jpg|鲑鱼卷 Image:Kakinohazusi.jpg|柿叶寿司 Image:Chakin-sushi2.JPG|茶巾寿司
Image:Sushi plate (盛り合わせ).jpg|盛り合わせ Image:Ikura gunkan maki sushi by
sfllaw in Toronto.jpg|軍艦卷 Image:Sasazushi.jpg|笹寿司
Image:Unagi-Sushi.jpg|鳗鱼寿司 Image:Sashimi for sale.JPG|东京超市里的握寿司
Image:Assorted sushi.png|什锦寿司 Image:Assorted Western sushi
(盛り合わせ).jpg|西式什锦寿司 Image:Sushi1.jpg|加州卷
Image:Spicytunahandroll.jpg|辣[金槍魚手卷](../Page/金槍魚.md "wikilink")（）
Image:Spicyshrimproll.jpg|海老軍艦壽司 Image:Salmon and avocado uramaki with
smelt egg topping, avocado norimaki.jpg|蔬菜壽司 Image:Spam
musubi.jpg|[午餐肉壽司](../Page/午餐肉.md "wikilink")
[SPAM_musubi.jpg](https://zh.wikipedia.org/wiki/File:SPAM_musubi.jpg "fig:SPAM_musubi.jpg")\]\]
Image:Inari-zushi.jpg|[-{zh-cn:腐皮; zh-tw:豆皮;
zh-hk:腐皮;}-壽司](../Page/豆皮壽司.md "wikilink")
Image:Vegetarian sushi rolls.jpg|蛋乳素食壽司
Image:Sushi_bento.jpg|壽司[便當](../Page/便當.md "wikilink")
<File:Assortment> of sushi, May 2010.jpg|壽司

## 另见

  - [紫菜包饭](../Page/紫菜包饭.md "wikilink")，韩国料理，类似卷寿司

## 參考

<references />

## 外部連結

[壽司](../Category/壽司.md "wikilink") [S](../Category/日語借詞.md "wikilink")

1.
2.  <http://zh.cn.nikkei.com/columnviewpoint/tearoom/25603-2017-06-19-04-53-20.html>
    日本人愛上餃子、也有科學性的理由
3.
4.  《[爾雅](../Page/爾雅.md "wikilink")·釋器》：肉謂之羹，魚謂之鮨。
5.  [劉熙](../Page/劉熙.md "wikilink")《[釋名](../Page/釋名.md "wikilink")·-{卷}-二·釋飲食第十三》：鮓滓也，以鹽米釀之加葅，熟而食之也。
6.  [刺身測試：逾半吞拿魚甲基汞超標 1款吞拿魚檢出寄生蟲及蟲卵
    另有1款三文魚含寄生蟲](https://www.consumer.org.hk/ws_chi/news/press/510/raw-salmon-tuna.html).
    [消費者委員會_(香港)](../Page/消費者委員會_\(香港\).md "wikilink"). \[2019-04-15\]