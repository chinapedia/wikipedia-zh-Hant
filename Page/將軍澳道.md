[Tseung_Kwan_O_Road_(south_end).JPG](https://zh.wikipedia.org/wiki/File:Tseung_Kwan_O_Road_\(south_end\).JPG "fig:Tseung_Kwan_O_Road_(south_end).JPG")
**將軍澳道**（）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[觀塘區的一條道路](../Page/觀塘區.md "wikilink")，為進出[將軍澳隧道及](../Page/將軍澳隧道.md "wikilink")[東區海底隧道的其中一段重要行車道路](../Page/東區海底隧道.md "wikilink")，連接[將軍澳](../Page/將軍澳新市鎮.md "wikilink")、[藍田](../Page/藍田_\(香港\).md "wikilink")、東區海底隧道及[觀塘幹道之一](../Page/觀塘.md "wikilink")，為雙線雙程分隔道路。屬[7號幹線一部份](../Page/7號幹線.md "wikilink")，車速限制每小時70公里。該路段是位於將軍澳隧道的[秀茂坪收費處前](../Page/秀茂坪.md "wikilink")，連接了[觀塘道](../Page/觀塘道.md "wikilink")、[鯉魚門道](../Page/鯉魚門道.md "wikilink")、[偉發道](../Page/偉發道.md "wikilink")。另外，不經將軍澳隧道的[秀茂坪道及](../Page/秀茂坪道.md "wikilink")[連德道也連接這條幹道](../Page/連德道.md "wikilink")，來往[藍田的](../Page/藍田_\(香港\).md "wikilink")[紅色小巴路線也途經將軍澳道](../Page/紅色小巴.md "wikilink")。

## 歷史

將軍澳道於1970年代時已經規劃，在[將軍澳隧道通車前](../Page/將軍澳隧道.md "wikilink")，此路曾經是一條[死路](../Page/死路.md "wikilink")。

由於道路噪音問題，[路政署從](../Page/路政署.md "wikilink")2007年9月起進行隔音屏障建造工程，於2009年底完工，工程耗資1.37億港元，由新福港〈土木〉有限公司承接興建。\[1\]

## 出口位置

| [將軍澳道](../Page/將軍澳道.md "wikilink") [链接=<https://zh.wikipedia.org/wiki/File:HK_Route7.svg>](https://zh.wikipedia.org/wiki/File:HK_Route7.svg "fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route7.svg")                                                                                                              |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 東行（將軍澳方向）                                                                                                                                                                                                                                                                                                           |
| 連接將軍澳隧道/將軍澳隧道公路[链接=<https://zh.wikipedia.org/wiki/File:HK_Route7.svg>](https://zh.wikipedia.org/wiki/File:HK_Route7.svg "fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route7.svg")                                                                                                                                  |
| *將軍澳道終點*[链接=<https://zh.wikipedia.org/wiki/File:HK_Route7.svg>](https://zh.wikipedia.org/wiki/File:HK_Route7.svg "fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route7.svg")                                                                                                                                         |
| [秀茂坪](../Page/秀茂坪.md "wikilink")、[藍田](../Page/藍田_\(香港\).md "wikilink")、[將軍澳](../Page/將軍澳新市鎮.md "wikilink")（經[寶琳路](../Page/寶琳路.md "wikilink")）、[大上托](../Page/大上托.md "wikilink") [秀茂坪道](../Page/秀茂坪道.md "wikilink")、連德道                                                                                               |
| 不適用                                                                                                                                                                                                                                                                                                                 |
| 不適用                                                                                                                                                                                                                                                                                                                 |
| 不適用                                                                                                                                                                                                                                                                                                                 |
| [*將軍澳道*](../Page/將軍澳道.md "wikilink")*起點*[链接=<https://zh.wikipedia.org/wiki/File:HK_Route7.svg>](https://zh.wikipedia.org/wiki/File:HK_Route7.svg "fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route7.svg")                                                                                                         |
| 連接[鯉魚門道](../Page/鯉魚門道.md "wikilink")[链接=<https://zh.wikipedia.org/wiki/File:HK_Route7.svg>](https://zh.wikipedia.org/wiki/File:HK_Route7.svg "fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route7.svg")、[觀塘繞道](../Page/觀塘繞道.md "wikilink")[23x23像素](https://zh.wikipedia.org/wiki/File:HK_Route2.svg "fig:23x23像素") |

## 途經的公共交通服務

<div class="NavFrame collapsed" style="background-color: #FFFF00;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFF00;margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 外部連結

  - [香港地方](http://www.hk-place.com/db.php?post=d006022)
  - [路政署](https://web.archive.org/web/20080501111005/http://www.hyd.gov.hk/chi/major/majorworks/tko/index.htm)

## 參考

<references/>

[Category:觀塘區街道](../Category/觀塘區街道.md "wikilink")
[Category:將軍澳](../Category/將軍澳.md "wikilink")
[Category:秀茂坪](../Category/秀茂坪.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")

1.  [將軍澳道加建隔音屏障工程](http://www.hyd.gov.hk/chi/major/majorworks/tko/index.htm)