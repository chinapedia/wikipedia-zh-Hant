**尊尼获加**（）又譯**-{zh-hans:约翰走路; zh-hk:約翰走路;
zh-tw:尊尼獲加;}-**，是[世界著名的](../Page/世界.md "wikilink")[苏格兰威士忌品牌](../Page/苏格兰威士忌.md "wikilink")，有150多[年的](../Page/年.md "wikilink")[歷史](../Page/歷史.md "wikilink")，由[帝亚吉欧在](../Page/帝亚吉欧.md "wikilink")[英國](../Page/英國.md "wikilink")[基尔马诺克的釀酒廠酿造](../Page/基尔马诺克.md "wikilink")。

## 商標的演變

尊尼獲加的[商標本來是一個](../Page/商標.md "wikilink")[走路中的](../Page/走路.md "wikilink")[英國](../Page/英國.md "wikilink")[紳士](../Page/紳士.md "wikilink")，但到了2000年代初期，為使[商品更](../Page/商品.md "wikilink")[國際化](../Page/國際化.md "wikilink")，所以把原來清楚的人像變成一個抽象形體，去除了原來商標人物的[種族特色](../Page/種族.md "wikilink")。並且由原本的向左邊行走，轉變成向右邊行走。

直至2015年，再在原本的商標人物增加細節，令商標人物看來更加像一個人物。

## 等級區分

約翰走路（）以調和威士忌聞名於世界，其中大多數產品並沒有標示年分，而使用約翰走路酒廠本身的規則，由等級低到高分別為：紅牌(red label)
\< 黑牌(black label) \< 綠牌(green label) \< 金牌(gold label) \< 藍牌(blue
label)，而特殊版本則獨立看待。\[1\]

## 外部链接

  - [Johnnie Walker官方网站](http://www.johnniewalker.com)

## 參考資料

[Category:威士忌](../Category/威士忌.md "wikilink")
[Category:帝亞吉歐品牌](../Category/帝亞吉歐品牌.md "wikilink")
[Category:1865年面世](../Category/1865年面世.md "wikilink")

1.