**尊龍**（****，本名**吳國良**，），知名華裔美籍[演員](../Page/演員.md "wikilink")。曾因演出《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》中[溥儀一角而廣為人知](../Page/溥儀.md "wikilink")，並因此而獲得[金球奬提名](../Page/金球奬.md "wikilink")。

## 生平

尊龍在[香港出生](../Page/香港.md "wikilink")，幼為孤兒，被放在一個籃子裡，由一名沒結婚的上海殘障女子收養。雖然幼年時過著很貧困的生活，然而尊龍有感養育之恩，故此其後雖移居他國，亦一直有照顧養母，直至終老。

尊龍十歲左右被送入[粉菊花的春秋戲劇學院學習京劇](../Page/粉菊花.md "wikilink")、舞蹈及武術。十七歲移居[美國](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")，刻苦學習演藝。其後尊龍到[紐約](../Page/紐約.md "wikilink")，為經理人黃美玉發掘，開始演藝事業。

1984年，尊龍首次在美國電影《[冰人四萬年](../Page/冰人四萬年.md "wikilink")》中扮演沒有對白的原始人。1985年，尊龍演出有辱華爭議的電影《龍年》後開始嶄露頭角。1987年，憑著電影《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》而聲名大噪，被海外媒體稱為「演藝國度的哲學家皇帝」。1993年主演《蝴蝶君》。

尊龍近年主要在[亞洲發展](../Page/亞洲.md "wikilink")。

## 個人榮譽

  - 1981年，尊龍憑舞台劇《Fresh Off the
    Boat》（F.O.B）獲得[奧比獎最佳男主角](../Page/奧比獎.md "wikilink")
  - 1982年，尊龍在舞台劇《The Dance And The Railroad》獲得奧比獎最佳男主角，同時包辦編舞和作曲工作
  - 1986年，尊龍憑《龍年》獲得[金球獎最佳男配角提名](../Page/金球獎.md "wikilink")
  - 1988年，尊龍憑《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》獲得[金球獎最佳男主角提名](../Page/金球獎.md "wikilink")　
  - 1989年，尊龍憑《輝煌時代》獲得美國[獨立精神獎最佳男配角提名](../Page/獨立精神獎.md "wikilink")
  - 1990年，尊龍被《[時人](../Page/時人.md "wikilink")》（People
    Magazine）評為50位最美麗人物之一
  - 1997年，尊龍在Capri Film Festival中獲得終身成就獎
  - 2001年，尊龍紐約[美洲華人博物館傑出成就獎](../Page/美洲華人博物館.md "wikilink")

## 作品

### 電影作品

  - 《[金剛 (1976年電影)](../Page/金剛_\(1976年電影\).md "wikilink")》(1976年)　飾演
    中國廚師
  - 《Americathon》 (1979)
  - 《[冰人四萬年](https://www.imdb.com/title/tt0087452/)》
    [Iceman](https://en.wikipedia.org/wiki/Iceman_\(1984_film\)) (1984)
  - 《龍年》 Year of the Dragon (1985)
  - 《[龍在天涯](http://www.imdb.com/title/tt0092946/?ref_=nm_flmg_act_13)》Echoes
    of Paradise(1987)
  - 《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》The Last Emperor
    (1987)　飾演 [愛新覺羅溥儀](../Page/愛新覺羅溥儀.md "wikilink")
  - 《[輝煌時代](http://www.imdb.com/title/tt0095649/?ref_=nm_flmg_act_11)》
    The Moderns (1988)
  - 《[龍在中國](http://www.imdb.com/title/tt0102895/?ref_=nm_flmg_act_10)》
    Shadow of China (1991)
  - 《上海一九二〇》 Shanghai 1920 (1991)
  - 《[蝴蝶君](../Page/蝴蝶君_\(电影\).md "wikilink")》 M，Butterfly (1993)　飾演
    [宋麗伶](../Page/宋麗伶.md "wikilink")
  - 《[影子](https://www.imdb.com/title/tt0111143/)》 [The
    Shadow](../Page/魅影奇俠.md "wikilink") (1994)
  - 《黑色追殺令》 The Hunted (1995)
  - 《熱血最強》 Hot Blood Is the Strongest (1997)
  - 《[尖峰時刻2](../Page/尖峰時刻2.md "wikilink")》 Rush Hour 2 (2001)
  - 《自娛自樂》 Bamboo Shoot (2004)
  - 《影月情缘》 Paper Moon Affair (2005)
  - 《玩命對戰》 War(2007)

### 電視劇作品

  - The Great Wall of Chinatown (1976)
  - The Blue Knight (1 episode, 1976)
  - Kate Bliss and the Ticker Tape Kid (1978)
  - Sword of Justice (1978)
  - The Destructors (1978)
  - Separate Ways (1 episode, 1979)
  - Eight Is Enough (1 episode, 1979)
  - Letter to One Bradford (1979)
  - Kate Bliss and the Ticker Tape Kid (1978)
  - 《乾隆與香妃》(2004)飾演 [乾隆](../Page/乾隆.md "wikilink")
  - 《[康熙微服私访记](../Page/康熙微服私访记.md "wikilink")》第五部(2006) 飾演
    [康熙](../Page/康熙.md "wikilink")

### 舞台劇作品

  - 1980－Fresh Off the Boat－Actor (Steve) Los Angeles, CA 　　
  - 1981－Fresh Off the Boat－Choreographer New York, Shakespeare
    Festival, Public Theater, NYC 　　
  - 1981－The Dance and the Railroad－Director, Choreographer, Actor New
    Federal Theatre, NYC 　　
  - 1982－The Dance and the Railroad－Actor Cricket Theatre, Minneapolis,
    MN 　　
  - 1983－Sound and Beauty－Director, Actor New York, Shakespeare
    Festival, Public Theater, NYC

### 音樂專輯

  - 1991年－ Coming To My Own（英文專輯）
  - 1998年－ Night Day （國語專輯） 　　
  - 1996年-Rabbit Ears Storybook Collection: The Five Chinese Brothers
    （Narrator） 　　
  - 單曲︰God Will Never Give You Up

[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:美国华裔演员](../Category/美国华裔演员.md "wikilink")
[K](../Category/吳姓.md "wikilink")
[Category:移民美國的香港人](../Category/移民美國的香港人.md "wikilink")
[Category:華人電影演員](../Category/華人電影演員.md "wikilink")