[hypnero.png](https://zh.wikipedia.org/wiki/File:hypnero.png "fig:hypnero.png")
《**尋愛綺夢**》（[意大利語](../Page/意大利語.md "wikilink")：**Hypnerotomachia
Poliphili**）是本印刷於[文藝復興時期](../Page/文藝復興.md "wikilink")，被認為是歷史上一本不尋常的書籍。

此書由[阿尔杜斯·马努提乌斯於](../Page/阿尔杜斯·马努提乌斯.md "wikilink")1499年12月在[威尼斯印刷](../Page/威尼斯.md "wikilink")。此書作者不明，但有人發現將書內每一章的第一個字母抽出排列後會變成「POLIAM
FRATER FRANCISCVS COLVMNA
PERAMAVIT」，即「[弗朗切斯科·科隆納弟兄深愛著寶莉拉](../Page/弗朗切斯科·科隆納.md "wikilink")」。學者們認為此書是獻給建築師[阿爾伯蒂](../Page/萊昂·巴蒂斯塔·阿爾伯蒂.md "wikilink")，[佛羅倫斯的君主](../Page/佛羅倫斯.md "wikilink")[洛伦佐·德·美第奇及](../Page/洛伦佐·德·美第奇.md "wikilink")[阿爾杜斯·馬努提烏斯](../Page/阿爾杜斯·馬努提烏斯.md "wikilink")。

此書以怪異的[由拉丁文衍生的](../Page/拉丁語.md "wikilink")[意大利文所寫成](../Page/義大利語.md "wikilink")，並充塞大量沒有說明的，由拉丁文及[希臘文](../Page/希臘語.md "wikilink")[詞根所創造出來的字詞](../Page/詞根.md "wikilink")。此書內亦有出現意大利文，[阿拉伯文及](../Page/阿拉伯語.md "wikilink")[希伯來文的詞彙](../Page/希伯來語.md "wikilink")。柯羅納甚至在現有詞彙的意思不足以準確地表達其所想時自創新語言（這種新語言包含並非真實的[古埃及象形文字](../Page/聖書體.md "wikilink")）。這故事詳盡地記述主角普力菲羅（Poliphilo，其名字意思為「很多事物的愛人」。希臘文中，Polu解作「很多」，Philos解作「被愛的」）漫遊於具古典田園風味的夢境中，去尋找他的愛人寶莉拉（Polia，解作「很多事物」）。作者的風格是詳盡地去形容各樣事物，並不吝惜地使用各種最高級的詞語。

這本書由於是世上最美麗的印刷書籍之一，所以長期以來為人們所追捧。這書亦以[字體排印的高質素及清晰度聞名](../Page/字體排印學.md "wikilink")，此種羅馬字體乃出自[弗朗西斯科·格利分](../Page/弗朗西斯科·格利分.md "wikilink")（Francesco
Griffo）的手筆。這書內有174幅精美的[木版畫](../Page/木刻.md "wikilink")，用來表示普力菲羅在夢中遇到的風景，建築物及人物。這些木版畫也許是全書最好的部份：精緻及令人回味，它們用硬直及華麗的線條風格來描畫出普力菲羅冒險的場景，及作者狂想下的建築物特徵。這些版畫有趣的地方在於它們表現了文藝復興時期人們眼中的[希臘及](../Page/希臘.md "wikilink")[羅馬古代歷史中的](../Page/羅馬.md "wikilink")[美學](../Page/美學.md "wikilink")。

[心理學家](../Page/心理學家.md "wikilink")[榮格曾誇獎此書中的夢境影像預示了他的](../Page/榮格.md "wikilink")[原型理論](../Page/原型.md "wikilink")。書中的版畫風格亦深深地影響了十九世紀後期的英國插圖畫家，例如是[奧伯利·比亞茲萊](../Page/奧伯利·比亞茲萊.md "wikilink")、[沃爾特·克蘭](../Page/沃爾特·克蘭.md "wikilink")（Walter
Crane）及貝爾（Robert Anning Bell）。

《尋愛綺夢》的部份文字曾於1592年時被R. D.譯成[英文版](../Page/英語.md "wikilink")，「R.
D.」被認為是[羅賓·單寧頓](../Page/羅賓·單寧頓.md "wikilink")（Robert
Dallington），他給了此書一個英文名字：《夢中愛的衝突》（The Strife of Love in a
Dream）。[互聯網檔案館則有此版本的真本](../Page/互聯網檔案館.md "wikilink")。

完整的英文譯本於1999年（約原著出版五百年後）由[泰晤士及戴德生出版社](../Page/泰晤士及戴德生出版社.md "wikilink")（Thames
& Hudson）出版。翻譯人為音樂家[祖士連·葛榮](../Page/祖士連·葛榮.md "wikilink")（Joscelyn
Godwin），字體則使用了[Monotype公司的字體普力菲利斯](../Page/Monotype.md "wikilink")（Poliphilus），這種字體是改造自格利分所創作的字體。在2005年2月時英文小型平裝本出版。由於翻譯上的困難，英文譯本採用了標準的現代英語，並捨棄原著中大量借來或作者自創的詞彙，因此，這譯本只令部份對此書好奇的普羅大眾而非嚴肅的學者們感興趣。

## 圖片

<File:Hypne2pg.jpg> <File:Hypneroto2.jpg>
<File:HypnerotomachiaPoliphili0018.jpg>

## 尋愛綺夢於以下小說中被提及

  - [弗朗索瓦·拉伯雷](../Page/弗朗索瓦·拉伯雷.md "wikilink")：《[巨人傳](../Page/巨人傳.md "wikilink")》
  - Alberto Pérez-Gómez：*Polyphilo: or The Dark Forest Revisited - An
    Erotic Epiphany of Architecture*
  - [阿圖洛·貝雷茲-雷維特](../Page/阿圖洛·貝雷茲-雷維特.md "wikilink")：*The Club Dumas*
  - John Crowley：*Love and Sleep*
  - [伊恩·柯德威](../Page/伊恩·柯德威.md "wikilink")（Ian
    Caldwell）及[達斯汀·湯瑪遜](../Page/達斯汀·湯瑪遜.md "wikilink")（Dustin
    Thomason）：《[四的法則](../Page/四的法則.md "wikilink")》

## 參見

  - Thames & Hudson (1999). *Hypnerotomachia Poliphili, the Strife of
    Love in a Dream*. ISBN 0-500-01942-8, a modern English translation.
  - Lefaivre, Liane: ''Leon Battista Alberti's *Hypnerotomachia
    Poliphili* : Re-cognizing the architectural body in the early
    Italian Renaissance''. Cambridge, Mass. \[u.a.\]: MIT Press 1997.
    ISBN 0-262-12204-9.
  - Schmeiser, Leonhard: *Das Werk des Druckers. Untersuchungen zum
    Buch* Hypnerotomachia Poliphili. Maria Enzersdorf: Edition Roesner
    2003. ISBN 3-902300-10-8, Austrian philosopher argues for Aldus
    Manutius' authorship.
  - Perez-Gomez, Alberto: *Polyphilo or The Dark Forest Revisited - An
    Erotic Epiphany of Architecture.* Cambridge, Mass.: MIT Press 1992.
    ISBN 0-262-16129-X, Introduction by Alberto Perez-Gomez.

## 外部連結

  - [電子版尋愛綺夢](http://mitpress.mit.edu/e-books/HP/index.htm)
    位於[麻省理工學院的電子版本](../Page/麻省理工學院.md "wikilink")
  - [完整的電子版副本](https://web.archive.org/web/20061005105228/http://www.liberliber.it/biblioteca/c/colonna/index.htm)
    意大利文章項目[Progetto
    Manuzio所提供](../Page/:en:Progetto_Manuzio.md "wikilink")
  - [一份詳細的文章](http://special.lib.gla.ac.uk/exhibns/month/feb2004.html)
    由[格拉斯哥大學圖書館所提供](../Page/格拉斯哥大學.md "wikilink")，此圖書館亦收藏了一本尋愛綺夢
  - [尋愛綺夢](http://www.gutenberg.org/files/18459)
    作者法蘭契斯科·柯羅納，由[古騰堡計劃所提供](../Page/古騰堡計劃.md "wikilink")

[Category:1499年](../Category/1499年.md "wikilink")
[Category:義大利文學](../Category/義大利文學.md "wikilink")