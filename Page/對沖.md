**對沖**（又稱**避險**、**套期保值**，），是指對於國際間從事商品買賣的[洋行](../Page/洋行.md "wikilink")、[進出口貿易商](../Page/国际贸易.md "wikilink")，以及在國際間從事[投资的人士](../Page/投资.md "wikilink")，若預期未來將收付一定金額的[外汇](../Page/外汇.md "wikilink")，為避免因變動產生的損失（參見[交易風險](../Page/汇率风险.md "wikilink")），可利用[遠期外匯交易以規避](../Page/遠期外匯交易.md "wikilink")[风险](../Page/风险.md "wikilink")，這種避險的操作方式，稱為[遠期避險或遠期](../Page/遠期避險.md "wikilink")**對沖**。\[1\]它是一種在減低商業風險的同時仍然能在投資中獲利的手法。

對沖在[外汇市场中最為常見](../Page/外汇市场.md "wikilink")，著意避開單線買賣的風險。所謂單線買賣，就是看好某一種[貨幣就做](../Page/貨幣.md "wikilink")[买空](../Page/买空.md "wikilink")（或稱揸倉），看淡某一種貨幣，就做[沽空](../Page/沽空.md "wikilink")（空倉）。如果判斷正確，所獲利潤自然多；但如果判斷錯誤，損失亦會比沒有進行「對沖」時來得大。

所謂對沖，就是同一時間買入一外幣，做買空。另外亦要沽出另外一種貨幣，即沽空。理論上，買空一種貨幣和沽空一種貨幣，要銀碼一樣，才算是真正的對沖盤，否則兩邊大小不一樣就做不到對沖的功能。

這樣做的原因，是世界外匯市場都以[美元做計算單位](../Page/美元.md "wikilink")。所有外幣的升跌都以美元作為相對的匯價。美元強，即外幣弱；外幣強，則美元弱。美元的升跌影響所有外幣的升跌。所以，若看好一種貨幣，但要減低風險，就需要同時沽出一種看淡的貨幣。買入強勢貨幣，沽出弱勢貨幣，如果估計正確，美元弱，所買入的強勢貨幣就會上升；即使估計錯誤，美元強，買入的貨幣也不會跌太多。沽空了的弱勢貨幣卻跌得重，做成蝕少賺多，整體來說仍可獲利。

## 對沖的例子

### 九零年代

1990年初，中東[伊拉克戰爭完結](../Page/伊拉克戰爭.md "wikilink")，[美国成為戰勝國](../Page/美国.md "wikilink")，美元價格亦穩步上升，走勢強勁，兌所有外匯都上升，當時只有[日元仍是強勢貨幣](../Page/日元.md "wikilink")。當時[柏林圍牆倒下不久](../Page/柏林圍牆.md "wikilink")，[德国剛](../Page/德国.md "wikilink")[統一](../Page/兩德統一.md "wikilink")，[東德經濟差令德國受拖累](../Page/東德.md "wikilink")，經濟有隱憂。[苏联政局不穩](../Page/苏联.md "wikilink")，[戈尔巴乔夫地位受到動搖](../Page/米哈伊尔·谢尔盖耶维奇·戈尔巴乔夫.md "wikilink")。[英国當時經濟亦差](../Page/英国.md "wikilink")，不斷減息，而[保守黨又受到](../Page/保守黨_\(英國\).md "wikilink")[工黨的挑戰](../Page/工党_\(英国\).md "wikilink")，所以英磅亦偏弱。[瑞士法郎在戰後作為戰爭避難所的吸引力大減](../Page/瑞士法郎.md "wikilink")，亦成為弱勢貨幣。

假如在當時買外匯，長沽英磅、[馬克](../Page/馬克.md "wikilink")、瑞士法郎，同時買入日元，就會賺大錢。當美元升時，所有外幣都跌，只有日元跌得最少，其他外幣都大跌；當美元回軟，其他外幣升幅少，日元卻會大升。無論如何，只要在當時的市場這樣做對沖，都會獲利。

### 亞洲金融危機

1997年，[索羅斯的](../Page/乔治·索罗斯.md "wikilink")[量子基金大量拋售](../Page/量子基金.md "wikilink")[泰銖](../Page/泰銖.md "wikilink")，買入其他貨幣，泰國股市下跌，泰國政府無法再維持[聯繫匯率](../Page/联系汇率制.md "wikilink")，經濟損失慘重。而該基金卻大大獲利。除了泰國，[香港等以聯繫匯率為維持貨幣價格的國家與地區都遭受](../Page/香港.md "wikilink")[对冲基金的挑戰](../Page/对冲基金.md "wikilink")。港府把息率大幅調高，[隔夜拆息達](../Page/隔夜拆息.md "wikilink")300%，更動用[外匯儲備](../Page/外匯儲備.md "wikilink")1200億港元大量購入港股。最後擊退炒家。

## 其他市場的對沖

對沖的原理並不限於外匯市場，不過在投資方面，較常用於外匯市場，這個原理亦適用於[黃金](../Page/金.md "wikilink")、[期货和](../Page/期货.md "wikilink")[期指市場](../Page/股票指數期貨.md "wikilink")。

### 期权對沖

  - [风险反转](../Page/风险反转.md "wikilink")
  - [Delta中性](../Page/Delta中性.md "wikilink")

## 可对冲的风险

  - [商品风险](../Page/商品风险.md "wikilink")
  - [信用風險](../Page/信用風險.md "wikilink")
  - [汇率风险](../Page/汇率风险.md "wikilink")
  - [利率風險](../Page/利率風險.md "wikilink")
  - [股权风险](../Page/股权风险.md "wikilink")
  - [波动风险](../Page/波动风险.md "wikilink")

## 参见

  - [对冲基金](../Page/对冲基金.md "wikilink")
  - [金融衍生工具](../Page/金融衍生工具.md "wikilink")
  - [利息風險](../Page/利息風險.md "wikilink")
  - [汇率风险](../Page/汇率风险.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
[H](../Category/金融衍生工具.md "wikilink")
[Category:金融术语](../Category/金融术语.md "wikilink")
[Category:商業](../Category/商業.md "wikilink")

1.  [財金商業科技辭典：Hedge避險](http://www.andrewlo.idv.tw/dictionary_H/dictionary_hedge.htm)