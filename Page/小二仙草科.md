**小二仙草科**（[学名](../Page/学名.md "wikilink")：）為多年生沉水或挺水之雙子葉植物，共6屬，約有120種，全世界分布。其中，以[澳洲地區最廣](../Page/澳洲.md "wikilink")。在[中國](../Page/中國.md "wikilink")、[台灣該科植物共有](../Page/台灣.md "wikilink")2屬，如[聚藻](../Page/聚藻.md "wikilink")。於兩地的該植物，多生長在[溝渠](../Page/溝渠.md "wikilink")，池塘，[水田](../Page/水田.md "wikilink")。平常以沉水狀態呈現，不過枯水期，則會呈現挺水狀。

## 分類

小二仙草科在不同的植物分類系統內，被分類在不同的目裡。

  - [克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink") - 小二仙草目。
  - [APG II植物分類系統](../Page/被子植物APG_II分类法_\(修订版\).md "wikilink") - 虎耳草目。

## 屬

  - *Glischrocaryon*
  - *Gonocarpus*
  - [小二仙草屬](../Page/小二仙草屬.md "wikilink")（*Haloragis*）
  - *Haloragodendron*
  - *Laurembergia*
  - *Meziella*
  - [狐尾藻屬](../Page/狐尾藻屬.md "wikilink")（*Myriophyllum*）
  - *Proserpinaca*

[\*](../Category/小二仙草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")