**小力加布龍屬**（[學名](../Page/學名.md "wikilink")：*Ligabueino*）意為「力加布的小東西」，是[阿貝力龍超科](../Page/阿貝力龍超科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[下白堊紀的](../Page/下白堊紀.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")[巴塔哥尼亞](../Page/巴塔哥尼亞.md "wikilink")。

屬名是以[義大利](../Page/義大利.md "wikilink")[醫生力加布](../Page/醫生.md "wikilink")（Giancario
Ligabue）命名。小力加布龍是體型最小的阿貝力龍超科，也是最小型的[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍之一](../Page/恐龍.md "wikilink")，只有70公分長。根據脊椎的癒合狀況，這個化石是個未成年個體；而最初新聞報導是發現成年個體\[1\]。小力加布龍曾被歸類於[西北阿根廷龍科](../Page/西北阿根廷龍科.md "wikilink")，但化石過於破碎，無法確定是否屬於西北阿根廷龍科。在2011的一項研究，提出小力加布龍只能確定屬於[阿貝力龍超科](../Page/阿貝力龍超科.md "wikilink")，無法做進一步的分類\[2\]。

小力加布龍的[化石破碎](../Page/化石.md "wikilink")，包括：[股骨](../Page/股骨.md "wikilink")、[腸骨](../Page/腸骨.md "wikilink")、[恥骨](../Page/恥骨.md "wikilink")、[趾骨](../Page/趾骨.md "wikilink")，以及[頸椎](../Page/頸椎.md "wikilink")、[背椎及](../Page/背椎.md "wikilink")[尾椎的神經弓](../Page/尾椎.md "wikilink")。

## 參考資料

  - [小力加布龍](http://web.me.com/dinoruss/de_4/5a650c6.htm)

[Category:阿貝力龍超科](../Category/阿貝力龍超科.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.  Carrano, M.T., Loewen, M.A. and Sertic, J.J.W. (2011). "New
    Materials of Masiakasaurus knopfleri Sampson, Carrano, and Forster,
    2001, and Implications for the Morphology of the Noasauridae
    (Theropoda: Ceratosauria). Smithsonian Contributions to
    Paleobiology, **95**: 53pp.

2.