**小古貓屬**（[学名](../Page/学名.md "wikilink")：），又名**細齒獸**，是一屬已[滅絕的](../Page/滅絕.md "wikilink")[細齒獸類](../Page/細齒獸類.md "wikilink")，生存於6500-4200萬年前的[古新世晚期](../Page/古新世.md "wikilink")。牠們代表了現今[食肉目的祖先](../Page/食肉目.md "wikilink")，

小古貓有五爪，大小像[鼬](../Page/鼬.md "wikilink")，生活在[北美洲及](../Page/北美洲.md "wikilink")[歐洲大陸](../Page/歐洲.md "wikilink")。牠們保有[肉齒目的一些原始特徵](../Page/肉齒目.md "wikilink")，如低的[頭顱骨](../Page/頭顱骨.md "wikilink")、幼長的身體、長尾巴及腳短。[牙齒數目仍保持在](../Page/牙齒.md "wikilink")44顆，但在[演化歷程中這個數量有一些減少及一些牙齒亦縮小了](../Page/演化.md "wikilink")。後肢較前肢長，[盆骨在形狀及結構上很像](../Page/盆骨.md "wikilink")[狗的](../Page/狗.md "wikilink")，[脊骨上亦有一些獨有的特徵](../Page/脊骨.md "wikilink")。牠們的爪可以伸縮，[關節靈活適合攀樹](../Page/關節.md "wikilink")，且是[雙目視覺的](../Page/雙目視覺.md "wikilink")。小古貓的[腦部比肉齒目的較大](../Page/腦部.md "wikilink")，而從腦部與身體大小比例的增長可見牠們在智能上亦增長了。就像其他早期的肉食性動物，牠們適合樹上的生活，而四肢及關節則像現今的肉食性動物。小古貓可能是非常靈活的[森林棲息者](../Page/森林.md "wikilink")，獵食細小的動物如哺乳動物、[爬行動物及](../Page/爬行動物.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，並且會吃[蛋及](../Page/蛋.md "wikilink")[生果](../Page/生果.md "wikilink")，故小古貓是[雜食性的](../Page/雜食性.md "wikilink")。

## 參考

[Category:小古貓屬](../Category/小古貓屬.md "wikilink")
[Category:始新世哺乳類](../Category/始新世哺乳類.md "wikilink")