《**小姐与流浪汉**》（），是一部由[華特迪士尼於](../Page/華特迪士尼影業.md "wikilink")1955年製作並發行的[動畫](../Page/動畫.md "wikilink")[電影](../Page/電影.md "wikilink")。它的上映日期是1955年6月22日（香港則於1956年9月20日上映），為第15部[華特迪士尼經典動畫長片](../Page/迪士尼经典动画长片.md "wikilink")。

这部影片的女主角是一只富裕家庭所养的美國可卡犬，叫做小姐
（Lady）。而男主角是一条[流浪狗](../Page/流浪狗.md "wikilink")（外型極似[雪納瑞](../Page/雪納瑞.md "wikilink")），叫流氓
（Tramp）。本片DVD已经分别由[中录德加拉](../Page/中录德加拉.md "wikilink")，[博伟在](../Page/博伟.md "wikilink")[中国大陆](../Page/中国大陆.md "wikilink")，[台湾发行](../Page/台湾.md "wikilink")。本片电影原声带已经由[滚石在台湾发行](../Page/滚石.md "wikilink")。續集《[-{zh-cn:小姐与流浪汉2：狗儿逃家记;zh-tw:小姐與流氓2：狗兒逃家記;zh-hk:小姐與流氓2：狗兒逃家記}-](../Page/小姐与流浪汉2：狗儿逃家记.md "wikilink")》由[迪士尼則是於](../Page/迪士尼.md "wikilink")2001年以[錄影帶首映的方式上映的](../Page/錄影帶首映.md "wikilink")。

## 配音員

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英語</p></td>
<td><p>國語</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/姜先誠.md" title="wikilink">姜先誠</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/佟紹宗.md" title="wikilink">佟紹宗</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/李香生.md" title="wikilink">李香生</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>比爾·鮑康</p></td>
<td><p><a href="../Page/陳明陽.md" title="wikilink">陳明陽</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>李·米勒</p></td>
<td><p><a href="../Page/徐健春.md" title="wikilink">徐健春</a></p></td>
</tr>
<tr class="odd">
<td><p>李·米勒</p></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李映淑.md" title="wikilink">李映淑</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/王景平.md" title="wikilink">王景平</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李香生.md" title="wikilink">李香生</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 配音本地化

本片的中文配音版本進行了本地化，因此在該版本裡出現了許多不屬於英文地區的東方地區的稱呼，例如麗滴兩個主人的名字改成中式人名、片中所在的[新英格蘭地區變成](../Page/新英格蘭.md "wikilink")「[台北](../Page/台北.md "wikilink")」、出現「咱」、「俺」、「[四川菜館](../Page/四川.md "wikilink")」、「[上海菜館](../Page/上海.md "wikilink")」、「[豬血湯](../Page/豬血.md "wikilink")」、「[滷肉飯](../Page/滷肉飯.md "wikilink")」等詞彙，餐館人員角色也帶有中國各省的口音。

## 電影原聲帶

## 外部連結

  -
  -
  -
  -
  -
[Category:1955年電影](../Category/1955年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1950年代劇情片](../Category/1950年代劇情片.md "wikilink")
[Category:美國浪漫劇情片](../Category/美國浪漫劇情片.md "wikilink")
[Category:家庭片](../Category/家庭片.md "wikilink")
[Category:美國動畫電影](../Category/美國動畫電影.md "wikilink")
[Category:狗主角故事](../Category/狗主角故事.md "wikilink")
[Category:迪士尼經典動畫長片電影](../Category/迪士尼經典動畫長片電影.md "wikilink")
[Category:身分差異戀情題材電影](../Category/身分差異戀情題材電影.md "wikilink")
[Category:華特迪士尼影業電影](../Category/華特迪士尼影業電影.md "wikilink")
[Category:動物權利相關電影](../Category/動物權利相關電影.md "wikilink")