《**小孩不笨**》（英文名：），由[新加坡的](../Page/新加坡.md "wikilink")[新傳媒星霖電影公司於](../Page/新傳媒星霖電影公司.md "wikilink")2002年發行的电影。这部电影主要是教育小孩，並由[新加坡著名電影人](../Page/新加坡.md "wikilink")[梁智強自編自導自演的作品](../Page/梁智強.md "wikilink")。演員包括：[向雲](../Page/向雲.md "wikilink")、[劉謙益](../Page/劉謙益.md "wikilink")、[黄志强](../Page/黄志强.md "wikilink")、[傅麗玲](../Page/傅麗玲.md "wikilink")、[秦顺薏](../Page/秦顺薏.md "wikilink")、[李創銳](../Page/李創銳.md "wikilink")、[陳雪兒](../Page/陳雪兒.md "wikilink")
及
[鄭咸斌](../Page/鄭咸斌.md "wikilink")。2002年2月《小孩不笨》於新加坡上映時曾連續拿下四週票房冠軍，同時創下380多萬新加坡元票房記錄，並獲得2003年[第22屆香港電影金像獎最佳亞洲電影的提名](../Page/2003年香港電影金像獎.md "wikilink")，但最後敗給韓國電影《[我的野蠻女友](../Page/我的野蠻女友.md "wikilink")》。

《小孩不笨》是一部社會諷刺喜劇片，故事講述[新加坡教育制度弊病](../Page/新加坡教育.md "wikilink")，包括[能力分班](../Page/能力分班.md "wikilink")、新加坡華語問題等。故事透過三個小孩讓觀眾看出新加坡政府的教育政策如何影響社會上每個人的生活。

在此電影中用到的語言有：[華語](../Page/華語.md "wikilink")（普通話）、[英語及新加坡當地盛行的漢語方言](../Page/英語.md "wikilink")—[新加坡福建話](../Page/新加坡福建話.md "wikilink")（閩台片閩南語），在劇中都有出現過。此電影在同樣使用閩台片閩南語的[臺灣也是相當受歡迎的電影](../Page/臺灣.md "wikilink")，電視台時有重播。

這部電影後來也改編成同名電視劇《[小孩不笨](../Page/小孩不笨_\(電視劇\).md "wikilink")》，是由電影原班人馬演出。而後來電影人[梁智強也推出了漫畫版](../Page/梁智強.md "wikilink")。

## 故事角色

| 角色                       | 演員                                                    |
| ------------------------ | ----------------------------------------------------- |
| 劉先生（Mr. Liu）             | [梁智強](../Page/梁智強.md "wikilink")（Jack Neo）            |
| 劉太太（Mrs. Liu）            | [向雲](../Page/向雲.md "wikilink")                        |
| 邱先生（Mr. Khoo）            | [劉謙益](../Page/劉謙益.md "wikilink")（Richard Low）         |
| 邱太太（Mrs. Khoo）           | [陳美廩](../Page/陳美廩.md "wikilink")（Selena Tan）          |
| 邱達利（Terry Khoo）          | [黃柏儒](../Page/黃柏儒.md "wikilink")（Huang Po Ju）         |
| 洪文福（Ang Boon Hock）       | [洪賜健](../Page/洪賜健.md "wikilink")（Joshua Ang Ser Kian） |
| 劉國彬（Liu Kok Pin）         | [李創銳](../Page/李創銳.md "wikilink")（Shawn Lee）           |
| 邱欣蓮（Selena Khoo）         | [陳雪兒](../Page/陳雪兒.md "wikilink")                      |
| 李老師（Ms. Lee）             | [溫淑如](../Page/溫淑如.md "wikilink")（Wen Su-Ru）           |
| 洪忠明（Ang Tiong Meng）      | [鄭咸斌](../Page/鄭咸斌.md "wikilink")（Zheng Xian Bin）      |
| 忠明母（Tiong Meng's Mother） | [林桂葉](../Page/林桂葉.md "wikilink")（Lim Kwee Hiok）       |
| 文福母（Boon Hock's Mother）  | [王翠瑩](../Page/王翠瑩.md "wikilink")（Wong Choi Yeng）      |
| 林老師（Ms. Lim）             | [王舒儀](../Page/王舒儀.md "wikilink")（Sally Ong）           |
| 朱老師（Ms. Choo）            | [王慧瑩](../Page/王慧瑩.md "wikilink")（Wang Wai Ying）       |
| 華文老師（Chinese Teacher）    | [徐冰](../Page/徐冰.md "wikilink")（Chee Bing）             |
| 校長（Principal）            | [潘皇升](../Page/潘皇升.md "wikilink")（Phua Chor Yong）      |
| John                     | [Harlow Russe](../Page/Harlow_Russe.md "wikilink")    |
| 康先生（Mr Kang）             | [黃定賢](../Page/黃定賢.md "wikilink")（Winston Huang）       |
| 中國經理（China Manager）      | [鄭世童](../Page/鄭世童.md "wikilink")（Zheng Shi Tong）      |
| Ben                      | [梁榮耀](../Page/梁榮耀.md "wikilink")（Hossan Leong）        |
| 綁匪（Kidnapper）            | [李國煌](../Page/李國煌.md "wikilink")（Mark Lee）            |
| 訓導主任（Discipline Master）  | [莫小玲](../Page/莫小玲.md "wikilink")（Patricia Mok）        |

## 獎項

  - [第二十二屆香港電影金像獎](../Page/2003年香港電影金像獎.md "wikilink")
      - 最佳亞洲電影提名

## 參考資料

  - [Media Corp Raintree Pictures. Raintree Pictures， cited 26
    January 2006. Available from World Wide
    Web](https://web.archive.org/web/20061104051910/http://www.mediacorpraintree.com/ins/ins_prodnotes.htm).

  - [Wikipedia. 2006. I Not Stupid. np: Wikipedia, the Free
    Encyclopedia, cited 26 January 2006. Available from World Wide
    Web](http://en.wikipedia.org/wiki/I_not_stupid).

  -
  -
[Category:小孩不笨系列](../Category/小孩不笨系列.md "wikilink")
[Category:新加坡電影作品](../Category/新加坡電影作品.md "wikilink")
[Category:2002年電影](../Category/2002年電影.md "wikilink")
[Category:讽刺类电影](../Category/讽刺类电影.md "wikilink")
[Category:喜劇劇情片](../Category/喜劇劇情片.md "wikilink")
[Category:小學背景電影](../Category/小學背景電影.md "wikilink")