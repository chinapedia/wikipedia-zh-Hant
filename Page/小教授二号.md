[Mpf-II.jpg](https://zh.wikipedia.org/wiki/File:Mpf-II.jpg "fig:Mpf-II.jpg")
**小教授二号**（Microprofessor II，MPF
II）在1982年上市发表。它是[宏碁](../Page/宏碁.md "wikilink")（当时英文名为Multitech）的第二种品牌电脑产品，也是一种最早的[苹果电脑](../Page/苹果电脑.md "wikilink")[复制机](../Page/Apple_II复制机列表.md "wikilink")。它看起来并不像大部份其他个人电脑。它的机箱是长方形的塑膠盒，正面下方嵌着小型的[软胶键盘](../Page/软胶键盘.md "wikilink")。

在1983年，小教授二号在[英国贩售](../Page/英国.md "wikilink")，含[税零售价是](../Page/增值税.md "wikilink")269[英鎊](../Page/英鎊.md "wikilink")。

MPF II一个重要的功能是它的[中文培基](../Page/中文培基.md "wikilink")。它是一种基于[Applesoft
BASIC中文化的](../Page/Applesoft_BASIC.md "wikilink")[BASIC语言](../Page/BASIC.md "wikilink")。

## 与Apple II的不同

小教授二号并不完全与[Apple II兼容](../Page/Apple_II.md "wikilink")。

小教授二号没有Apple
II的文字模式。所有的文字都是由软件"画"在屏幕上，而不是经由硬件产生。这在当时是使电脑能显示中文的唯一最具经济效益的方式，因为当时基于硬件的中文产生器要价上百美元。

如同Apple II，小教授二号有两个图形缓冲器，但第二个缓冲器在位址A000H，而Apple II的在4000H。

MPF-II的键盘介面相当简单，是由一个8位输出口及一个直接连结至键盘矩阵的输入口组成。它没有苹果的摇杆介面，取而代之的是键盘方向键与两个功能键。

## 技术信息

  - [中央處理器](../Page/中央處理器.md "wikilink")：[MOS
    6502處理器](../Page/MOS_6502.md "wikilink")
  - [時脈](../Page/時脈.md "wikilink")：1 [MHz](../Page/MHz.md "wikilink")
  - [内存](../Page/内存.md "wikilink")：64 [KB](../Page/KB.md "wikilink")
  - [ROM](../Page/ROM.md "wikilink")：16
    [KB](../Page/KB.md "wikilink")（其中12KB是[BASIC编译器](../Page/BASIC.md "wikilink")）
  - 文字模式：40×24（使用图形模式）
  - 图像模式：280×192
  - 颜色：8色
  - 音效：单声道1位元音效
  - 连接埠：键盘、打印机、扩充埠、录音带输入/输出口、电视输出口。
  - 可选[周边设备](../Page/周边设备.md "wikilink")：55键完全尺寸键盘、[软驱](../Page/软驱.md "wikilink")、[加热与](../Page/加热打印机.md "wikilink")[点矩阵打印机](../Page/点矩阵打印机.md "wikilink")、[摇杆](../Page/摇杆.md "wikilink")。
  - 电源供应：外置[电源供应器](../Page/电源供应器.md "wikilink")，5.12[伏特](../Page/伏特.md "wikilink")

## 相关条目

  - [小教授一号](../Page/小教授一号.md "wikilink")
  - [小教授三号](../Page/小教授三号.md "wikilink")

## 外部链接

  - [MPF
    II](http://www.old-computers.com/museum/computer.asp?st=1&c=276) at
    the Old Computer Museum

[Category:早期電腦](../Category/早期電腦.md "wikilink")
[Category:個人電腦](../Category/個人電腦.md "wikilink")