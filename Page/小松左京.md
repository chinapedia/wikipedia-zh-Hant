**小松左京**（），本名**小松實**，是[日本最著名的](../Page/日本.md "wikilink")[科幻小說](../Page/科幻小說.md "wikilink")[作家之一](../Page/作家.md "wikilink")\[1\]，號稱「日本科幻界的推土機」，出生於[大阪府](../Page/大阪府.md "wikilink")。小松左京與[星新一](../Page/星新一.md "wikilink")、[筒井康隆合稱為日本科幻小說](../Page/筒井康隆.md "wikilink")「[御三家](../Page/御三家.md "wikilink")」。

## 生平

### 早期

小松左京的[祖先是](../Page/祖先.md "wikilink")[阿波](../Page/阿波國.md "wikilink")（[德島](../Page/德島縣.md "wikilink")）小松至（[千葉](../Page/千葉縣.md "wikilink")）[外房附近活動的](../Page/房總半島.md "wikilink")[漁夫家族](../Page/漁夫.md "wikilink")\[2\]。父親曾於[明治藥学專門学校](../Page/明治藥学專門学校.md "wikilink")（現在的[明治藥科大学](../Page/明治藥科大学.md "wikilink")）就讀，後來與東京漢方藥店店主的女兒結婚\[3\]\[4\]。小松左京的父母住在[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[西區](../Page/西區_\(大阪市\).md "wikilink")，育有五男一女，而小松左京則是次男\[5\]。他在4歳時搬到[兵庫縣](../Page/兵庫縣.md "wikilink")[西宮市](../Page/西宮市.md "wikilink")，隨後於[尼崎及西宮度過童年時期](../Page/尼崎市.md "wikilink")\[6\]。小松左京的哥哥在[京都大学專攻冶金工學](../Page/京都大学.md "wikilink")，後來擔任[三洋電機的技術人員](../Page/三洋電機.md "wikilink")、[戰争爆發後藉由研讀科學書籍來教授小松左京科學知識](../Page/戰争.md "wikilink")\[7\]。他也告訴小松左京轟炸廣島的新型炸彈為[原子彈](../Page/原子彈.md "wikilink")\[8\]。

他在少年時代因為身體虛弱，對於[體育沒有興趣](../Page/體育.md "wikilink")，但熱中於[歌](../Page/歌.md "wikilink")、[漫畫及](../Page/漫畫.md "wikilink")[電影](../Page/電影.md "wikilink")。小松左京也在大阪盛行的[文樂中學習](../Page/文樂.md "wikilink")[古典](../Page/古典.md "wikilink")[藝術相關知識](../Page/藝術.md "wikilink")\[9\]。

他在1943年進入[第一神戶中学校就讀](../Page/兵庫縣立神戶高等學校.md "wikilink")，並曾加入柔道部（戰爭結束後，[柔道遭到禁止](../Page/柔道.md "wikilink")，於是轉入橄欖球）\[10\]。小松左京在[第二次世界大戰結束時才](../Page/第二次世界大戰.md "wikilink")14歲，無法加入軍隊。他感覺自己背負存活下來的責任，成為将来創作[科幻小說的契機](../Page/科幻小說.md "wikilink")\[11\]。

### 戰後

小松左京在戰爭結束後曾接觸到[詩人](../Page/詩人.md "wikilink")[但丁的](../Page/但丁.md "wikilink")《[神曲](../Page/神曲.md "wikilink")》，並受到啟發，成為之後創作[科幻小說的起點](../Page/科幻小說.md "wikilink")。

他在1948年從中学畢業，進入[第三高等学校就讀](../Page/第三高等学校.md "wikilink")\[12\]。小松左京於[京都大學學習](../Page/京都大學.md "wikilink")[文學](../Page/文學.md "wikilink")，專攻[義大利文學](../Page/義大利文學.md "wikilink")\[13\]\[14\]。他在大学中参加同人誌『京大作家集團』活動，[高橋和巳](../Page/高橋和巳.md "wikilink")、[三浦浩等人交流文學](../Page/三浦浩.md "wikilink")。當時他熱中於[安部公房的作品](../Page/安部公房.md "wikilink")\[15\]。

他曾參加[日本共產黨](../Page/日本共產黨.md "wikilink")，從事政治活動[山村工作隊](../Page/山村工作隊.md "wikilink")。他因為對投下原子彈的[美國感到反感](../Page/美國.md "wikilink")，所以加入高唱「反戰平和」的共產黨，信奉[共産主義思想](../Page/共産主義.md "wikilink")\[16\]。後來他因[蘇聯開始研究原子彈](../Page/蘇聯.md "wikilink")，對共産黨活動感到質疑，後來離開共産黨\[17\]。

小松左京在提出義大利[劇作家](../Page/劇作家.md "wikilink")[路伊吉·皮兰德娄論文後](../Page/路伊吉·皮兰德娄.md "wikilink")、於1954年大学畢業。後來他在進入[産経新聞](../Page/産経新聞.md "wikilink")，開始擔任推理小說評論工作\[18\]。

他於1958年結婚，但是生活清苦。他後來根據當時大阪的[阿帕契族](../Page/阿帕契族.md "wikilink")、創作長篇小說《[日本阿帕契族](../Page/日本阿帕契族.md "wikilink")》\[19\]。

### 作家生涯

1961年、他參加[早川書房主辦的第](../Page/早川書房.md "wikilink")1屆科幻小説競賽，以《[地球和平](../Page/地球和平.md "wikilink")》（地には平和を）獲得努力賞\[20\]。《地球和平》後來未於《SF雜誌》連載，而是在SF同人誌『[宇宙塵](../Page/宇宙塵_\(同人誌\).md "wikilink")』刊登\[21\]。

1963年，小松左京加入剛成立的[日本SF作家俱樂部](../Page/日本SF作家俱樂部.md "wikilink")（1980年至1983年的會長是他與[星新一](../Page/星新一.md "wikilink")、[矢野徹](../Page/矢野徹.md "wikilink")），開始頻繁與科幻小說作家互相交流。

1963年，他在《[全讀物](../Page/全讀物.md "wikilink")》刊登「紙か髪か」，獲得[吉田健一](../Page/吉田健一.md "wikilink")、[扇谷正造等人的稱讚](../Page/扇谷正造.md "wikilink")。同年，短編集《地には平和を》發行，並進入1963年度下半期[直木賞決選](../Page/直木賞.md "wikilink")。小松左京對[未来學抱持興趣](../Page/未来學.md "wikilink")，於是在1968年参加「日本未来學會」成立\[22\]。

1970年，[國際科幻小說評論會召開](../Page/國際科幻小說評論會.md "wikilink")，作家[亞瑟·查理斯·克拉克](../Page/亞瑟·查理斯·克拉克.md "wikilink")、[茱迪絲‧梅洛](../Page/茱迪絲‧梅洛.md "wikilink")（Judith
Merril）、[費德列克·波爾](../Page/費德列克·波爾.md "wikilink")（Frederik
Pohl）、[布萊恩·威爾遜·艾爾迪斯](../Page/布萊恩·威爾遜·艾爾迪斯.md "wikilink")（Brian
Wilson
Aldiss）等都曾参與該會議。他在1973年推出代表作《[日本沉沒](../Page/日本沉沒.md "wikilink")》，想像[日本列島因地殼大變動而沉入海底](../Page/日本列島.md "wikilink")，轟動日本社會，銷出四百萬冊；此書獲得第27回[日本推理作家協會賞](../Page/日本推理作家協會賞.md "wikilink")，並由[東寶公司改編成電影](../Page/東寶.md "wikilink")。

1980年、他擔任[日本SF作家俱樂部会長](../Page/日本SF作家俱樂部.md "wikilink")、盡力推動[徳間書店設立](../Page/徳間書店.md "wikilink")[日本SF大賞](../Page/日本SF大賞.md "wikilink")。第1屆得獎作品為[堀晃的](../Page/堀晃.md "wikilink")[硬科幻短編集](../Page/硬科幻.md "wikilink")《太陽風交点》。1985年，小松左京又以《[首都消失](../Page/首都消失.md "wikilink")》描述[東京被濃](../Page/東京.md "wikilink")[霧包圍阻隔外界的想像災難](../Page/霧.md "wikilink")，獲得第6回[日本科幻小說大賞](../Page/日本科幻小說大賞.md "wikilink")（[日本SF大賞](../Page/日本SF大賞.md "wikilink")）\[23\]。

2000年，[角川春樹事務所主辦的](../Page/角川春樹事務所.md "wikilink")[小松左京賞設立](../Page/小松左京賞.md "wikilink")、小松左京擔任評審委員（2009年退出）。2001年，同人誌『[小松左京雜誌](../Page/小松左京雜誌.md "wikilink")』開辦，毎集開頭文張連載小松與著名人物的談話。2006年，《日本沉沒》再度改編為電影，由[草彅剛主演](../Page/草彅剛.md "wikilink")。小松左京於2011年7月26日因[肺炎於大阪府](../Page/肺炎.md "wikilink")[箕面市病逝](../Page/箕面市.md "wikilink")\[24\]\[25\]\[26\]。

## 紀念

1993年，[小林隆男發現](../Page/小林隆男.md "wikilink")[6983小行星](../Page/6983小行星.md "wikilink")，後來於2002年命名為小松左京。

## 作品

  - 小松左京作品集 全5卷 [JustSystems](../Page/JustSystems.md "wikilink")、1995年 -
    1996年
  - 小松左京全集 全56卷 城西国际大学出版会、2006年 -

### 小说

  - 1963年：《[大地和平](../Page/大地和平.md "wikilink")》（短篇）
  - 1964年：《[復活之日](../Page/復活之日.md "wikilink")》
  - 1966年：《[超能密諜](../Page/超能密諜.md "wikilink")》
  - 1966年：《[無盡漂流的終點](../Page/無盡漂流的終點.md "wikilink")》
  - 1972年：《[繼承者是誰？](../Page/繼承者是誰？.md "wikilink")》
  - 1973年：《[結晶星團](../Page/結晶星團.md "wikilink")》（短篇）
  - 1973年：《[祖先大人萬歳](../Page/祖先大人萬歳.md "wikilink")》
  - 1973年：《[日本沉沒](../Page/日本沉沒.md "wikilink")》
  - 1978年：《[戈耳狄俄斯之结](../Page/戈耳狄俄斯之结.md "wikilink")》
  - 1982年：《[木星再見](../Page/木星再見.md "wikilink")》
  - 1985年：《[首都消失](../Page/首都消失.md "wikilink")》
  - 2006年：《[日本沉沒・第二部](../Page/日本沉沒・第二部.md "wikilink")》（與[谷甲州共著](../Page/谷甲州.md "wikilink")）

### 戏剧

  - 狐と宇宙人－戯曲集（德间書店、1990年）
    -SF[狂言](../Page/狂言.md "wikilink")。受[茂山千之丞之托所写](../Page/茂山千之丞.md "wikilink")\[27\]\[28\]。

### 评论、对话、随笔

  - 地図の思想 講談社、1965年
  - 探検の思想 講談社、1966年
  - 未来図の世界 講談社、1966年
  - 未来怪獣宇宙 講談社、1967年
  - 未来の思想 文明の進化と人類 中公新書、1967年

### 漫画

  - イワンの馬鹿（不二書房、1949年）モリ・ミノル名義
  - 大地底海（不二書房、1949年）モリ・ミノル名義
  - ぼくらの地球（我们的地球）（不二書房、1949年）モリ・ミノル名義
  - 幻の小松左京=モリ・ミノル漫画全集 全4巻（小学館、2002年）

### 电视

  - [宇宙人ピピ](../Page/宇宙人ピピ.md "wikilink")
    （1965年、[NHK](../Page/日本放送協会.md "wikilink")、实拍+动画合成作品的原作）
  - [日本沉沒](../Page/日本沉沒.md "wikilink")（電視版，1974年）

### 電影

  - [日本沉没](../Page/日本沉没.md "wikilink")（1973年版、2006年版，[東宝](../Page/東宝.md "wikilink")）
  - [超能密諜](../Page/超能密諜.md "wikilink")（1974年，東宝）
  - [復活之日](../Page/復活之日.md "wikilink")（1980年，[角川書店](../Page/角川書店.md "wikilink")、[TBS](../Page/TBSテレビ.md "wikilink")（發行：東宝））
  - [木星再見](../Page/木星再見.md "wikilink")（1984年，東宝、株式会社イオ）
  - [首都消失](../Page/首都消失.md "wikilink")（1987年，[関西テレビ](../Page/関西テレビ放送.md "wikilink")、[大映](../Page/大映.md "wikilink")、[德间书店](../Page/德间书店.md "wikilink")（發行：東宝））

### 广播

## 參考資料

## 外部連結

  - [小松左京ホームページ](https://web.archive.org/web/20061016044120/http://www.nacos.com/komatsu/)
    官方網站

  - [List of his translated
    works](http://www.iocorp.co.jp/translated.htm)

  - [Tribute him by Aritsune Toyoda, translated by Leslie
    Furlong](http://www.nippon2007.us/GOH/Toyoda_tribute_to_Sakyo_Komatsu.php)

  -
  -
  - [日本沈没](https://web.archive.org/web/20071011014151/http://www.nc06.jp/)
    2006年版電影官方網站

[Category:日本推理小說作家](../Category/日本推理小說作家.md "wikilink")
[Category:日本推理作家協會獎獲得者](../Category/日本推理作家協會獎獲得者.md "wikilink")
[Category:日本科幻小說家](../Category/日本科幻小說家.md "wikilink")
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:京都大學校友](../Category/京都大學校友.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:前日本共產黨黨員](../Category/前日本共產黨黨員.md "wikilink")

1.  Komatsu topped in the writers ranking of All-Time Best survey of S-F
    Magazine in 2006. ()

2.  『週刊サンケイ』1982年3月18日号 pp.23-25

3.
4.  『小松左京自伝』pp.9-10、253、412

5.
6.
7.  『小松左京自伝』p.17

8.  『小松左京自伝』pp.22-23

9.  『小松左京自伝』pp.11-14

10. 『小松左京自伝』pp.19-21

11. 『小松左京自伝』p.27

12. 『小松左京自伝』pp.32-36

13.

14. 『小松左京自伝』pp.31-32

15. 『小松左京自伝』pp.39-46

16. 当時の活動は、事前に待ち合わせ日時を決めて集団でアジテーションを叫びながら街中を練り歩くというものだった。

17. 『小松左京自伝』pp.36-43

18. 『小松左京自伝』pp.46-52、56-58

19. 『小松左京自伝』pp.53-55

20. *The New York Times* obituary, "[Sci-fi writer got the continental
    drift](http://www.smh.com.au/national/obituaries/scifi-writer-got-the-continental-drift-20110821-1j4lh.html)"
    August 22, 2011 via Sydney Morning Herald

21. 『小松左京自伝』pp.59-62

22. 『小松左京自伝』pp.66-70

23.

24.

25. [小松左京さんが死去
    「日本沈没」など](http://www.47news.jp/CN/201107/CN2011072801000573.html)
    [共同](../Page/共同通信社.md "wikilink")（[47NEWS](../Page/47NEWS.md "wikilink")）、2011年7月28日閲覧

26. [「日本沈没」の小松左京さん死去](http://hochi.yomiuri.co.jp/topics/news/20110728-OHT1T00228.htm)
     スポーツ報知 2011年7月28日閲覧

27. 《小松左京自伝》pp.87-88

28.