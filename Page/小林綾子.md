**小林綾子**（）是[日本女演員](../Page/日本.md "wikilink")，出身於[東京都](../Page/東京都.md "wikilink")。

她在1983年度播出的[NHK](../Page/日本放送協會.md "wikilink")
[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")《[阿信](../Page/阿信_\(电视剧\).md "wikilink")》「童年篇」裡飾演女主角阿信，演技優異，因電視劇在日本以及許多國家播出，受到相當矚目。2013年於《[阿信(電影)](../Page/阿信_\(電影\).md "wikilink")》飾演八代美乃（阿信姊妹淘加代的母親）。畢業於[立命館大學](../Page/立命館大學.md "wikilink")[文學部英美文学學系](../Page/文學部.md "wikilink")，在大學時代曾經參加過[奧克拉荷馬大學的海外語文研修](../Page/奧克拉荷馬大學.md "wikilink")。

小林綾子是在1999年6月跟大她4歲的建築師結婚，但已於2010年離婚\[1\]。

## 主要出演作品

### 電視劇

  - NHK《[阿信](../Page/阿信.md "wikilink")》
  - NHK《[命](../Page/命_\(大河劇\).md "wikilink")》
  - NHK《[遠山金四郎與女鼠小僧](../Page/遠山金四郎與女鼠小僧.md "wikilink")》系列
  - [TBS](../Page/東京放送.md "wikilink")《[冷暖人間](../Page/冷暖人間.md "wikilink")》系列
    飾本間由紀
  - [富士電視台火曜時代劇](../Page/富士電視台.md "wikilink")《[劍客生涯](../Page/劍客生涯.md "wikilink")》（系列作　[藤田真主演](../Page/藤田真.md "wikilink")）的準主角
  - [朝日電視台特別單元劇](../Page/朝日電視台.md "wikilink")《[台灣歌姬·鄧麗君](../Page/台灣歌姬·鄧麗君.md "wikilink")》
    飾「林女士」（[鄧麗君香港經紀人](../Page/鄧麗君.md "wikilink")）

### 電視劇以外的電視節目

  - [NHK教育頻道](../Page/NHK教育頻道.md "wikilink")《[趣味悠悠](../Page/趣味悠悠.md "wikilink")》

## 參考來源

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本兒童演員](../Category/日本兒童演員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:NHK晨間連續劇主演演員](../Category/NHK晨間連續劇主演演員.md "wikilink")
[Category:立命館大學校友](../Category/立命館大學校友.md "wikilink")
[Category:奧克拉荷馬大學校友](../Category/奧克拉荷馬大學校友.md "wikilink")

1.  \[<http://www.nikkansports.com/entertainment/news/f-et-tp0-20130723-1161648.html>　「おしん」小林綾子が離婚していた\]
    （[日刊スポーツ](../Page/日刊體育.md "wikilink") ）