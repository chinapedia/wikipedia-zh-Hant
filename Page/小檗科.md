**小檗科**\[1\]，或寫作「小蘗科」，包括15个[属大约](../Page/属.md "wikilink")570[种植物](../Page/种.md "wikilink")\[2\]，其中大部分（450种）都是[小檗属的](../Page/小檗属.md "wikilink")[植物](../Page/植物.md "wikilink")，其中既有[木本植物也有多年宿根的](../Page/木本植物.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，有[乔木也有](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")。主要分布北半球[溫帶地區與](../Page/溫帶地區.md "wikilink")[亞熱帶的](../Page/亞熱帶.md "wikilink")[山區](../Page/山地气候.md "wikilink")\[3\]。

中國本科植物有11屬，303種(其中207種是[特有種](../Page/特有種.md "wikilink")，1種為[引進栽培](../Page/栽培種.md "wikilink")，又有17種為仍未充分研究者)\[4\]。

## 属

下列具中名者為產於中國的屬。

  - *[Achlys](../Page/Achlys.md "wikilink")*
  - [小檗属](../Page/小檗属.md "wikilink") *Berberis*
  - *[Bongardia](../Page/Bongardia.md "wikilink")*
  - [红毛七属](../Page/红毛七属.md "wikilink") *Caulophyllum*
  - [山荷叶属](../Page/山荷叶属.md "wikilink") *Diphylleia*
  - [八角莲属](../Page/八角莲属.md "wikilink") *Dysosma*
  - [淫羊藿属](../Page/淫羊藿属.md "wikilink") *Epimedium*
  - [牡丹草属](../Page/牡丹草属.md "wikilink") *Gymnospermium*
  - [鲜黄连属](../Page/鲜黄连属.md "wikilink") *Jeffersonia*
  - [囊果草属](../Page/囊果草属.md "wikilink") *Leontice*
  - [十大功劳属](../Page/十大功劳属.md "wikilink") *Mahonia*
  - [南天竹属](../Page/南天竹属.md "wikilink") *Nandina*
  - [鬼臼属](../Page/鬼臼属.md "wikilink") *Podophyllum*
  - *[Ranzania](../Page/Ranzania.md "wikilink")*
  - *[Vancouveria](../Page/Vancouveria.md "wikilink")*

2003年的[APG II
分类法也将小檗科放入](../Page/APG_II_分类法.md "wikilink")[毛茛目中](../Page/毛茛目.md "wikilink")，在[真双子叶植物分支之下](../Page/真双子叶植物分支.md "wikilink")；但[克朗奎斯特分类法将毛茛目放到](../Page/克朗奎斯特分类法.md "wikilink")[木兰亚纲之下](../Page/木兰亚纲.md "wikilink")。其中[十大功劳属和小檗属的亲缘关系最近](../Page/十大功劳属.md "wikilink")，两个属的植物之间可以杂交。以前的分类法有另外分出南天竹科、囊果草科和鬼臼科的。

## 注释

<references />

## 外部链接

  - 在L. Watson 和 M.J. Dallwitz (1992年 )的
    [《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[小檗科](http://delta-intkey.com/angio/www/berberid.htm)，
    [囊果草科](http://delta-intkey.com/angio/www/leontica.htm)，
    [南天竹科](http://delta-intkey.com/angio/www/nandinac.htm)，
    [鬼臼科](http://delta-intkey.com/angio/www/podophyl.htm)
  - [小檗科链接](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Berberidaceae)
  - [NCBI分类法中的小檗科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=41773&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)
  - [北美植物中的小檗科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10100)

[\*](../Category/小檗科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.

2.  [FOC Vol. 19
    Page 714](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=10100)，中國植物志(英文版)，稱本科植物有17個屬，約650種。

3.
4.