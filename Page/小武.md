《**小武**》是[中国导演](../Page/中国.md "wikilink")[贾樟柯出品的一部独立制片的](../Page/贾樟柯.md "wikilink")[电影](../Page/电影.md "wikilink")，为[北京电影学院学生作业作品](../Page/北京电影学院.md "wikilink")。1998年2月18日发行。影片在贾樟柯的家乡[山西](../Page/山西.md "wikilink")[汾阳的小县城中拍摄](../Page/汾阳.md "wikilink")，大部分次要演员都是当地人，使用[晋语汾阳方言对白](../Page/晋语.md "wikilink")。主人公小武的演员[王宏伟是](../Page/王宏伟.md "wikilink")[河南人](../Page/河南.md "wikilink")，在影片中使用河南[安阳方言](../Page/安阳.md "wikilink")。

## 情节概要

影片通过[扒手小武的友情](../Page/扒手.md "wikilink")、爱情、亲情的遭遇，描述了一个边缘人的窘况。

小武（[王宏伟饰](../Page/王宏伟.md "wikilink")）是一个小县城里文质彬彬的扒手，自称是“干手艺活儿的”。他从前的黑道朋友（[郝鸿建饰](../Page/郝鸿建.md "wikilink")）转行贩卖假烟而成了成功的“企业家”，结婚也不邀请小武。小武心情烦闷，找[卡拉OK的](../Page/卡拉OK.md "wikilink")[三陪女胡梅梅](../Page/性工作者.md "wikilink")（[左百韬饰](../Page/左百韬.md "wikilink")）聊天，逐渐堕入爱河。但在他准备送给她一个定情戒指时她却和一个富人去了[太原](../Page/太原.md "wikilink")。于是小武回到农村家中，把戒指送给了母亲。而他母亲之后把戒指转送了没[过门的二儿媳](../Page/结婚.md "wikilink")。小武和父母吵了一架，回到县城，在一次行窃时被捕。小武被手铐拷在路边，受到许多路人的围观，影片结束。

## 其他

在贾樟柯2002年的电影《[任逍遥](../Page/任逍遥.md "wikilink")》中，仍然由王宏伟饰演的小武这个角色再次出现，这次他是一个在[大同市的成功的小偷](../Page/大同市.md "wikilink")，借给斌斌一笔钱。

## 获奖

  - 获第48届[柏林国际电影节青年论坛首奖](../Page/柏林国际电影节.md "wikilink")：沃尔夫冈 斯道奖
  - 获第48届柏林国际电影节亚洲电影促进联盟奖
  - 获第20届法国南特三大洲电影节最佳影片：金热气球奖
  - 获第17届[温哥华国际电影节](../Page/温哥华国际电影节.md "wikilink")：龙虎奖
  - 获第3届[釜山国际电影节](../Page/釜山国际电影节.md "wikilink")：新潮流奖
  - 获比利时电影资料馆98年度大奖：黄金时代奖
  - 获第42届[旧金山国际电影节首奖](../Page/旧金山国际电影节.md "wikilink")：SKYY奖
  - 获1999意大利[亚的里亚国际电影节最佳影片奖](../Page/亚的里亚国际电影节.md "wikilink")

## 外部链接

  -
  -
[Category:贾樟柯电影](../Category/贾樟柯电影.md "wikilink")
[X小](../Category/1998年电影.md "wikilink")
[X小](../Category/中國大陸禁片.md "wikilink")
[X小](../Category/中国电影作品.md "wikilink")