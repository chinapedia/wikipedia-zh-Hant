**小沢征悦**（），[日本男](../Page/日本.md "wikilink")[演员](../Page/演员.md "wikilink")。[美国](../Page/美国.md "wikilink")[旧金山出生](../Page/旧金山.md "wikilink")，為著名指揮家[小沢征爾與模特及女演员](../Page/小泽征尔.md "wikilink")[入江美樹之子](../Page/入江美樹.md "wikilink")，女性散文家[小沢征良之弟](../Page/小沢征良.md "wikilink")。

## 主要出演作品

### 电视剧

  - [大河劇](../Page/大河劇.md "wikilink")（[NHK](../Page/日本放送協会.md "wikilink")）
      - [徳川慶喜](../Page/德川慶喜_\(大河劇\).md "wikilink")（1998年、[沖田總司](../Page/沖田總司.md "wikilink")）
      - [義経](../Page/義經_\(大河劇\).md "wikilink")（2005年、[源義仲](../Page/源義仲.md "wikilink")）
      - [篤姫](../Page/篤姬_\(電視劇\).md "wikilink")
        （2008年、[西鄉隆盛](../Page/西鄉隆盛.md "wikilink")）　
      - [坂上之雲](../Page/坂上之雲.md "wikilink") （2009年 -
        2011年特别历史剧、[夏目漱石](../Page/夏目漱石.md "wikilink")）
  - 最後のサムライ河井継之助 （1999年、[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - 乳房 THE BREAST（2000年、[中部日本放送](../Page/中部日本放送.md "wikilink")）
  - サスペンス喜劇　瓜二つ（2002年、[北海道放送](../Page/北海道放送.md "wikilink")・[TBS電視台](../Page/TBS電視台.md "wikilink")）
  - [命运之恋](../Page/命运之恋.md "wikilink")（2002年、TBS電視台・[韓国MBC](../Page/文化廣播_\(韓國\).md "wikilink")）
  - [さくら](../Page/さくら_\(朝ドラ\).md "wikilink")（2002年、NHK、桂木慶介（体育教師））
  - ひまわりさん
  - 人情届けます 江戸・娘飛脚（2003年、NHK、清太郎）
  - [シェエラザード〜海底に眠る永遠の愛〜](../Page/シェエラザード_\(小説\).md "wikilink")（2004年、NHK）
  - [HTBスペシャルドラマ](../Page/HTBスペシャルドラマ.md "wikilink")
    うみのほたる（2005年、[北海道電視台](../Page/北海道電視台.md "wikilink")）
  - [女人一代記](../Page/女人一代記.md "wikilink")（2005年、[富士電視台](../Page/富士電視台.md "wikilink")、[内藤法美](../Page/内藤法美.md "wikilink")（[越路吹雪之夫](../Page/越路吹雪.md "wikilink")））
  - [里見八犬傳](../Page/里見八犬傳_\(2006年電視劇\).md "wikilink")（2006年、TBS電視台、犬山道節忠與）
  - [美味求婚](../Page/美味求婚.md "wikilink")（2006年、TBS電視台）
  - [ちいさこべ](../Page/ちいさこべ.md "wikilink")（2006年、NHK）
  - [堀部安兵衛](../Page/堀部安兵衛_\(正月時代劇\).md "wikilink")（2007年、NHK、堀部安兵衛）
  - 2夜連续冬季特别剧[李香蘭](../Page/李香蘭_\(2007年電視劇\).md "wikilink")（2007年、[北海道電視台](../Page/北海道電視台.md "wikilink")、[兒玉英水](../Page/兒玉英水.md "wikilink")）
  - [天國與地獄](../Page/天國與地獄_\(電視\).md "wikilink")（2007年、朝日電視台）
  - [ドラマW](../Page/ドラマW.md "wikilink")「[6時間後に君は死ぬ](../Page/6時間後に君は死ぬ.md "wikilink")」（2008年9月28日、[WOWOW](../Page/WOWOW.md "wikilink")、手塚祐介）
  - [Panasonic特別劇](../Page/Panasonic.md "wikilink")「[あるがままの君でいて](../Page/あるがままの君でいて.md "wikilink")」（2008年11月、TBS電視台）
  - [テレビ朝日50周年ドラマスペシャル
    警官の血](../Page/警官の血#テレビドラマ.md "wikilink")（2009年2月8日、朝日電視台、早瀬勇作）
  - 金曜プレステージ・[木枯し紋次郎](../Page/木枯し紋次郎.md "wikilink") （2009年、フジテレビ、小判鮫の金蔵）
  - [わたしが子どもだったころスペシャル](../Page/わたしが子どもだったころ.md "wikilink")「指揮者
    小澤征爾」（2009年6月3日、[NHK-BShi](../Page/NHKデジタル衛星ハイビジョン.md "wikilink")、再現ドラマ部分の[小澤開作](../Page/小澤開作.md "wikilink")）
  - [アンタッチャブル〜事件記者・鳴海遼子〜](../Page/アンタッチャブル〜事件記者・鳴海遼子〜.md "wikilink")（2009年、[朝日放送](../Page/朝日放送.md "wikilink")・朝日電視台）-　鳴海洸至
  - [シスター](../Page/シスター_\(テレビドラマ\).md "wikilink")（2009年12月11・18日、NHK）-　成瀬生馬　
  - [蒼穹之昴](../Page/苍穹之昴_\(电视剧\).md "wikilink")（2010年、NHK-BShi）-　岡圭之介
  - [福岡発ドラマスペシャル](../Page/福岡発地域ドラマ.md "wikilink")
    「母（かか）さんへ」（2009年12月11日、[NHK九州沖縄](../Page/NHK九州沖縄.md "wikilink")
    / 2010年2月11日、NHK総合）
  - [必殺仕事人2010](../Page/必殺仕事人2010.md "wikilink")（2010年7月10日、テレビ朝日）－　風間右京乃助役
  - [LADY](../Page/LADY_\(电视剧\).md "wikilink") (2011年1月7日 - TBS電視台) -
    藤堂雄一郎
  - [TEAM -警视厅特别犯罪搜查本部-](../Page/TEAM_-警视厅特别犯罪搜查本部-.md "wikilink")
    (2014年4月16日 - 朝日电视台) - 佐久晋吾
  - [Criminal Minds: Beyond
    Borders](../Page/Criminal_Minds:_Beyond_Borders.md "wikilink")(2016年4月6日)
  - [神探夏洛克小姐](../Page/神探夏洛克小姐.md "wikilink")（2018年4月、HBO Asia X Hulu ) -
    雙葉健人

### 電影

  - [豚の報い](../Page/豚の報い.md "wikilink")（1999年）
  - [ざわざわ下北沢](../Page/ざわざわ下北沢.md "wikilink")（2000年）
  - [東京マリーゴールド](../Page/東京マリーゴールド.md "wikilink")（2001年）
  - [ホタル](../Page/ホタル_\(映画\).md "wikilink")（2001年）
  - [釣りバカ日誌13
    ハマちゃん危機一髪\!](../Page/釣りバカ日誌13_ハマちゃん危機一髪!.md "wikilink")（2002年）
  - [ほたるの星](../Page/ほたるの星.md "wikilink")（2003年）
  - [隱劍鬼爪](../Page/隱劍鬼爪.md "wikilink")（2004年）
  - [雪に願うこと](../Page/雪に願うこと.md "wikilink")（2006年）
  - [犯人に告ぐ](../Page/犯人に告ぐ.md "wikilink")（2007年）
  - [クライマーズ・ハイ](../Page/クライマーズ・ハイ.md "wikilink")（2008年）安西燐太郎（安西耿一郎之子・成長後）
  - [劔岳 点の記](../Page/劔岳_点の記.md "wikilink")（2009年）玉井大尉（主人公の直属の上官）
  - [BALLAD 名もなき恋のうた](../Page/BALLAD_名もなき恋のうた.md "wikilink")（2009年）安長
  - [わたし出すわ](../Page/わたし出すわ.md "wikilink")（2009年）保利満
  - ワカラナイ WHERE ARE YOU?（2009年）
  - 相棒 -劇場版II- 警視庁占拠\! 特命係の一番長い夜（2010年）八重樫哲也（警視庁人質籠城事件の犯人）
  - [腦男](../Page/腦男.md "wikilink")(2013年)
  - [HOT ROAD](../Page/Hot_Road_\(電影\).md "wikilink")（2014年）
  - [神劍闖江湖
    傳說的最終篇](../Page/神劍闖江湖.md "wikilink")（2014年）[伊藤博文](../Page/伊藤博文.md "wikilink")
  - [王牌大騙局](../Page/王牌大騙局.md "wikilink")(2015年) (主廚)

### CM

  - [三菱自動車](../Page/三菱自動車.md "wikilink")　[コルト](../Page/コルト.md "wikilink")（女性ドライブ篇）2003年1月～　（[志村享子と共演](../Page/志村享子.md "wikilink")）
  - 三菱自動車　コルト（デザイン篇）2003年1月～　（志村享子と共演）
  - 三菱自動車　コルト（安全篇）（環境篇）（フリーチョイス篇すべて）2002年11月～
      -
        （ナレーションのみの出演）
  - UHA味覚糖 さけるグミVSなが～いさけるグミ 2017年7月～ （伊藤梨沙子と共演）

### 解说

  - [歴史秘話ヒストリア](../Page/歴史秘話ヒストリア.md "wikilink")([NHK大阪](../Page/NHK大阪.md "wikilink"))2008年7月23日放送分「幕末
    殿様たちの恋・夢・涙」 ([島津齊彬編担当](../Page/島津齊彬.md "wikilink"))

[Category:日本男演員](../Category/日本男演員.md "wikilink")