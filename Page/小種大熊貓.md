**小種大熊貓**（[学名](../Page/学名.md "wikilink")：），又名**小種貓熊**、**小型大熊貓**，是現今[大熊貓已知最早的祖先](../Page/大熊貓.md "wikilink")。牠們長約1米，現今的大熊貓則可長超過1.5米。從牠們[牙齒的磨損情況可知牠們是吃](../Page/牙齒.md "wikilink")[竹為生的](../Page/竹.md "wikilink")。最先的[化石是在](../Page/化石.md "wikilink")[中國發現的](../Page/中國.md "wikilink")[頭顱骨](../Page/頭顱骨.md "wikilink")，估計有200萬年歷史。

## 參考文献

[Category:大熊貓屬](../Category/大熊貓屬.md "wikilink")