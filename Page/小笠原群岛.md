**小笠原群岛**（，）是[日本在](../Page/日本.md "wikilink")[西太平洋的一個](../Page/西太平洋.md "wikilink")[群岛](../Page/群岛.md "wikilink")，位于[东京以南](../Page/东京.md "wikilink")1,000多[公里](../Page/公里.md "wikilink")，行政區劃属[东京都](../Page/东京都.md "wikilink")[小笠原村管辖](../Page/小笠原村.md "wikilink")。群岛由30多个小岛组成，其中著名的有[父岛](../Page/父岛.md "wikilink")、[母岛和](../Page/母岛.md "wikilink")[硫磺岛等島嶼](../Page/硫磺岛.md "wikilink")，總面积104.41平方公里。另外，距離東京市區1,848公里遠、日本實際管轄領土最東的[南鸟岛](../Page/南鸟岛.md "wikilink")，也属小笠原群岛的一部分。

## 历史

1830年，曾有一些[美国人在岛上开展过殖民](../Page/美国.md "wikilink")，但是没有成功。1875年，[英国将这些岛屿的所有权转交给日本](../Page/英国.md "wikilink")。1880年，日本在该群岛建立居民点并建立统治。[二战期间](../Page/二战.md "wikilink")，美国军队和日本军队为争夺小笠原群岛展开连番激战，美国前总统[乔治·H·W·布什驾驶的](../Page/乔治·H·W·布什.md "wikilink")[魚雷轟炸機曾在](../Page/魚雷轟炸機.md "wikilink")[父島附近海域被击落](../Page/父島.md "wikilink")，险些丧命。至於[硫磺岛战役則更是名留青史](../Page/硫磺岛战役.md "wikilink")。

1945年，[美国海军占领该群岛](../Page/美国海军.md "wikilink")，并强行迁离了所有日本居民。1968年，基于[小笠原归还协定](../Page/日本国与美利坚合众国关于南方诸岛和其他岛屿的协定.md "wikilink")，该群岛被归还给日本，原先被迁离的居民又回到了岛上。

2011年，小笠原群岛列入[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")[世界遗产名录](../Page/世界遗产.md "wikilink")\[1\]。

## 島嶼

  - [聟島列島](../Page/聟島列島.md "wikilink")：[聟島](../Page/聟島.md "wikilink")、[嫁島](../Page/嫁島.md "wikilink")、[媒島](../Page/媒島.md "wikilink")、[北之島](../Page/北之島.md "wikilink")
  - [父島列島](../Page/父島列島.md "wikilink")：[父島](../Page/父島.md "wikilink")、[兄島](../Page/兄島.md "wikilink")、[弟島](../Page/弟島.md "wikilink")
  - [母岛列岛](../Page/母岛列岛.md "wikilink")：[母島](../Page/母島.md "wikilink")、[姊島](../Page/姊島.md "wikilink")、[妹島](../Page/妹島.md "wikilink")
  - [西之岛](../Page/西之岛_\(小笠原群岛\).md "wikilink")
  - [火山列島](../Page/火山列岛_\(日本\).md "wikilink")：[北硫磺島](../Page/北硫磺島.md "wikilink")、[硫磺島](../Page/硫磺島.md "wikilink")、[南硫磺島](../Page/南硫磺島.md "wikilink")
  - [南鸟岛](../Page/南鸟岛.md "wikilink")、[冲之鸟礁](../Page/冲之鸟礁.md "wikilink")

其中僅父島和母島有一般居民居住，硫磺島有[自衛隊駐紮](../Page/自衛隊.md "wikilink")，南鳥島則有自衛隊、[氣象廳和](../Page/氣象廳.md "wikilink")[海上保安廳人員駐紮](../Page/海上保安廳.md "wikilink")。

## 氣候

小笠原群岛属[亚热带](../Page/亚热带.md "wikilink")[海洋性气候](../Page/海洋性气候.md "wikilink")，年降水量1200至1600毫米。

## 世界遗产

## 参考资料

[小笠原群島](../Category/小笠原群島.md "wikilink")
[Category:小笠原村](../Category/小笠原村.md "wikilink")
[Category:東京都島嶼](../Category/東京都島嶼.md "wikilink")
[Category:日本世界遗产](../Category/日本世界遗产.md "wikilink")
[Category:以人名命名的地名](../Category/以人名命名的地名.md "wikilink")

1.