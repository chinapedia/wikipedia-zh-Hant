**小约瑟夫·帕特里克·“乔”·肯尼迪**（，），[美国海軍](../Page/美国海軍.md "wikilink")[飞行员](../Page/飞行员.md "wikilink")，[上尉](../Page/上尉.md "wikilink")，[二戰](../Page/二戰.md "wikilink")[軍官](../Page/軍官.md "wikilink")。

小约瑟夫是[老约瑟夫·P·肯尼迪與](../Page/老约瑟夫·P·肯尼迪.md "wikilink")[罗丝·伊麗莎白·肯尼迪的長子](../Page/罗丝·肯尼迪.md "wikilink")、[美國第](../Page/美國.md "wikilink")35任[總統](../Page/美國總統.md "wikilink")[約翰·甘迺迪及前](../Page/約翰·甘迺迪.md "wikilink")[美國司法部長](../Page/美國司法部長.md "wikilink")[羅伯特·甘迺迪和](../Page/羅伯特·甘迺迪.md "wikilink")[美國](../Page/美國.md "wikilink")[聯邦參議員](../Page/聯邦參議員.md "wikilink")[愛德華·甘迺迪的哥哥](../Page/愛德華·甘迺迪.md "wikilink")，[肯尼迪家族的第三代成员](../Page/肯尼迪家族.md "wikilink")，1944年8月12日在[第二次世界大战中执行任务时在](../Page/第二次世界大战.md "wikilink")[英国](../Page/英国.md "wikilink")[萨福克郡坠机身亡](../Page/萨福克郡.md "wikilink")，得年29歲。

[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:肯尼迪家族](../Category/肯尼迪家族.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:美國海軍軍官](../Category/美國海軍軍官.md "wikilink")
[Category:美國海軍飛行員](../Category/美國海軍飛行員.md "wikilink")
[Category:美国天主教徒](../Category/美国天主教徒.md "wikilink")
[Category:哈佛大学校友](../Category/哈佛大学校友.md "wikilink")
[Category:伦敦政治经济学院校友](../Category/伦敦政治经济学院校友.md "wikilink")
[Category:紫心勳章獲得者](../Category/紫心勳章獲得者.md "wikilink")
[Category:马萨诸塞州人](../Category/马萨诸塞州人.md "wikilink")