[Lantau_Island_reclamation.png](https://zh.wikipedia.org/wiki/File:Lantau_Island_reclamation.png "fig:Lantau_Island_reclamation.png")及[赤鱲角填海範圍](../Page/赤鱲角.md "wikilink")\]\]
[201611_Near_the_Siu_Ho_Wan_Depot.jpg](https://zh.wikipedia.org/wiki/File:201611_Near_the_Siu_Ho_Wan_Depot.jpg "fig:201611_Near_the_Siu_Ho_Wan_Depot.jpg")[小蠔灣車廠](../Page/小蠔灣車廠.md "wikilink")\]\]
[Siu_Ho_Wan_Government_Maintenance_Depot_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Siu_Ho_Wan_Government_Maintenance_Depot_\(Hong_Kong\).jpg "fig:Siu_Ho_Wan_Government_Maintenance_Depot_(Hong_Kong).jpg")\]\]

**小蠔**（），或作**小濠**，是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[大嶼山北部的一個地方](../Page/大嶼山.md "wikilink")，位於[陰澳之西](../Page/陰澳.md "wikilink")，[大蠔之東](../Page/大蠔.md "wikilink")，[香港行政區劃上屬於](../Page/香港行政區劃.md "wikilink")[離島區](../Page/離島區.md "wikilink")，但是所有公共交通工具上的車廠屬於[荃灣區](../Page/荃灣區.md "wikilink")；其對出在填海前有一個名爲**小蠔灣**（）的[海灣](../Page/海灣.md "wikilink")。

## 歷史

1989年，[香港政府公佈](../Page/香港殖民地時期#香港政府.md "wikilink")《[香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")》，當中包括在小蠔北部穿過的[北大嶼山公路](../Page/北大嶼山公路.md "wikilink")，及將小蠔納入為發展範圍的[北大嶼山新市鎮](../Page/北大嶼山新市鎮.md "wikilink")。地下鐵路公司（今[港鐵公司](../Page/港鐵公司.md "wikilink")）當時公佈[東涌綫的早期方案](../Page/東涌綫.md "wikilink")，亦計劃在小蠔興建車廠、[小蠔站及上蓋物業](../Page/小蠔站.md "wikilink")。為了興建北大嶼山公路及機場快線的車廠，小蠔灣被填海。其後由於保育以及節省開支考慮，[香港政府縮減北大嶼山新市鎮的發展規模](../Page/香港政府.md "wikilink")，優先發展[東涌](../Page/東涌.md "wikilink")，故此在小蠔的發展擱置。目前該處有[港鐵](../Page/港鐵.md "wikilink")[小蠔灣車廠](../Page/小蠔灣車廠.md "wikilink")、[龍運及](../Page/龍運巴士小蠔灣車廠.md "wikilink")[城巴](../Page/城巴.md "wikilink")[巴士廠](../Page/巴士廠.md "wikilink")、廢物轉運站、污水處理廠、濾水廠、廚餘回收中心、車輛扣留中心及[愉景灣隧道等建築物](../Page/愉景灣隧道.md "wikilink")，甚少居民。

就香港政府計劃於小蠔灣填海100至150公頃興建[物流園](../Page/物流園.md "wikilink")，惟填海範圍與[中華白海豚出沒的深水區域重疊](../Page/中華白海豚.md "wikilink")，環境諮詢委員會建議縮減填海範圍，否則應該擱置計劃。就此，[發展局局長](../Page/發展局局長.md "wikilink")[陳茂波表示](../Page/陳茂波.md "wikilink")，考慮在小蠔灣填海時，[發展局已經提出研究中華白海豚的生活環境](../Page/發展局.md "wikilink")，因此定必尊重科學研究結果，故此同意收窄填海範圍；他表示會研究小蠔灣車廠上蓋及在其範圍發展房屋用途\[1\]。

## 特色

  - 附近有行山徑，前往[大東山](../Page/大東山.md "wikilink")。
  - 附近的[白芒村](../Page/白芒村.md "wikilink")，有大量史前古物出土，具歷史價值。

## 交通

  - [新大嶼山巴士](../Page/新大嶼山巴士.md "wikilink")：**36**
    [東涌鐵路站](../Page/東涌站_\(東涌綫\).md "wikilink")-[翔東路](../Page/翔東路.md "wikilink")-小蠔灣（每天固定班次4-5班）

## 區議會議席分佈

由於小蠔現時主要用作[車廠用途](../Page/車廠.md "wikilink")，理論上沒有常住人口，而整個小蠔都劃入[大嶼山選區](../Page/大嶼山_\(選區\).md "wikilink")。

## 参考资料

## 外部链接

  - [港鐵小蠔灣車廠](../Page/港鐵小蠔灣車廠.md "wikilink")

{{-}}  {{-}}

[Category:大嶼山](../Category/大嶼山.md "wikilink")

1.  [陳茂波籲理性討論大嶼山發展](http://orientaldaily.on.cc/cnt/news/20150322/00176_010.html)
    《東方日報》 2015年3月22日