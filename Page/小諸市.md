**小諸市**（）是位於[日本](../Page/日本.md "wikilink")[長野縣東部的一個市](../Page/長野縣.md "wikilink")，這裡在江戶時代之前曾經是個繁華的[城下町](../Page/城下町.md "wikilink")（[戰國時代](../Page/戰國_\(日本\).md "wikilink")、[江戶時代以](../Page/江戶時代.md "wikilink")[大名所居住的城為中心而繁華的市街](../Page/大名.md "wikilink")）。

## 地理環境

**小諸市**標高679m，以小諸市役所的位置來說，在東經138度25分45秒、北緯36度19分26秒的地方，南方有[淺間山廣大的傾斜面](../Page/淺間山.md "wikilink")，市區中央有[千曲川流過](../Page/千曲川.md "wikilink")，整體是在一個[高原上的都市](../Page/高原.md "wikilink")。

東西方向的寬度約12.8公里，南北長約15.4公里，整體面積為98.66平方公里，和首都東京的直線距離大約為150公里左右。

## 歷史沿革

這裡曾經挖掘到許多[繩文時代以及](../Page/繩文時代.md "wikilink")[彌生時代的文化遺跡](../Page/彌生時代.md "wikilink")，同時這裡的農耕、畜牧以及法令制度等都相當的先進，古時此處為官道（重要道路）重要的地方，設有傳馬所（驛站），人馬往來自然而形成的一個集市。

[日本戰國時代](../Page/戰國_\(日本\).md "wikilink")[武田信玄在天文](../Page/武田信玄.md "wikilink")12年攻佔，為了控管東[信州命](../Page/信州.md "wikilink")[山本勘助與](../Page/山本勘助.md "wikilink")[馬場信房在此築城建造了](../Page/馬場信房.md "wikilink")[小諸城](../Page/小諸城.md "wikilink")（現懷古園址），據說目前遺留的城跡是[信玄的軍師](../Page/武田信玄.md "wikilink")[山本勘助的勢力範圍](../Page/山本勘助.md "wikilink")。[江戶時代之後在此設](../Page/江戶時代.md "wikilink")[藩](../Page/藩.md "wikilink")，並有[小諸藩廳](../Page/藩.md "wikilink")（相當於我們現在的地方行政機關），[元祿](../Page/元祿.md "wikilink")15年（1702年），[牧野康重以](../Page/牧野康重.md "wikilink")15,000[石米入主小諸城成為初代小諸藩主](../Page/石.md "wikilink")，一直到[明治時代都由牧野一氏世代子孫擔任小諸城主](../Page/明治時代.md "wikilink")。

在古道街道這裡屬於[中山道](../Page/中山道.md "wikilink")、[甲州街道以及北國街道交匯的重要所在](../Page/甲州街道.md "wikilink")。由於物資交流的盛行，所以這裡是個以典型商業都市而繁華的地方。進入[明治時代之後](../Page/明治時代.md "wikilink")**小諸**更是[批發商聚集之處](../Page/批發.md "wikilink")，成為縣內、外重要的[商業發展之地以及文化振興交流之處](../Page/商業.md "wikilink")。

昭和29年4月1日（1954年）由於北佐久郡小諸町、三岡村、南大井村、北大井村、大里村、川邊村的一町五村[合併而改為市制](../Page/市町村合併.md "wikilink")，成為今天的**小諸市**。

## 交通

[Otome_Station_20060405.jpg](https://zh.wikipedia.org/wiki/File:Otome_Station_20060405.jpg "fig:Otome_Station_20060405.jpg")[長野縣小諸市乙女驛](../Page/長野縣.md "wikilink")（車站）內站牌，是個無人車站，連結乙女湖公園\]\]

### 鐵道

小諸市境內有以下主要鐵道與車站

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")、[小海線](../Page/小海線.md "wikilink")

<!-- end list -->

  -

      -
        小諸車站、東小諸車站、乙女車站、三岡車站、美里車站

<!-- end list -->

  - [信濃鐵道](../Page/信濃鐵道.md "wikilink")（、shinanotetudou）

<!-- end list -->

  -

      -
        小諸車站、平原車站

### 道路

以下為小諸市內主要幹道

  - [上信越自動車道](../Page/上信越自動車道.md "wikilink") - 小諸交流道
  - 國道18號
  - 國道141號
  - 國道142號

## 知名古蹟與觀光景點

  - 懷古園（原[小諸城址](../Page/小諸城.md "wikilink")）
  - 小諸市動物園
  - 藤村紀念館
  - 布引觀音釋尊寺
  - 寺之浦石器時代住居遺跡（、teranourasekkijidaijyuukyoato）
  - 小諸布引草莓園（、komoronunobikiichigoen）
  - 淺間2000公園（、Asama 2000 Park、滑雪場）

## 姐妹城市

  - \-
    [岐阜縣](../Page/岐阜縣.md "wikilink")[中津川市](../Page/中津川市.md "wikilink")（1973年）

  - \-
    [神奈川縣](../Page/神奈川縣.md "wikilink")[大磯町](../Page/大磯町.md "wikilink")（1973年）

  - \-
    [富山縣](../Page/富山縣.md "wikilink")[滑川市](../Page/滑川市.md "wikilink")（1974年）

## 參見

[長野縣](../Page/長野縣.md "wikilink")、[戰國
(日本)](../Page/戰國_\(日本\).md "wikilink")、[武田信玄](../Page/武田信玄.md "wikilink")

[千曲川](../Page/千曲川.md "wikilink")、[淺間山](../Page/淺間山.md "wikilink")、[藩](../Page/藩.md "wikilink")、[山本勘助](../Page/山本勘助.md "wikilink")

## 參考資料

  - [小諸市役所官方網站](http://webarchive.loc.gov/all/20020914050105/http%3A//www.city.komoro.nagano.jp/)（日文）
  - [小諸市觀光協會](http://www.kanko.komoro.org/)（日文）
  - [Asama 2000 Park](http://www.asama2000.com/)（日文）
  - [小諸城](https://web.archive.org/web/20070208234210/http://joe.ifdef.jp/nagano/010komoro.htm)（日文）