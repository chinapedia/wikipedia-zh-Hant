**小貴族龍屬**（[學名](../Page/學名.md "wikilink")：*Kritosaurus*），又名**克里托龍**，是[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[上白堊紀的](../Page/上白堊紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，約7300萬年前。小貴族龍身長10公尺，體重2到3公噸，牠的[化石並不齊全](../Page/化石.md "wikilink")，但在分類歷史上卻很重要。牠的學名意思是「分離的[蜥蜴](../Page/蜥蜴.md "wikilink")」，但卻有時因牠的[鷹鈎鼻而誤譯為](../Page/鷹鈎鼻.md "wikilink")「[貴族蜥蜴](../Page/貴族.md "wikilink")」\[1\]，最初標本被發現時，鼻部是關節脫落的碎片，因此原先被重建成扁平的。牠的分類歷史很曲折，涉及了[格里芬龍](../Page/格里芬龍.md "wikilink")、[阿納薩齊龍及](../Page/阿納薩齊龍.md "wikilink")[納秀畢吐龍](../Page/納秀畢吐龍.md "wikilink")，這個分類的混亂狀況需要發現更多小貴族龍的化石才能解決。除了缺乏化石外，牠亦一直被認為是格里芬龍的[異名](../Page/異名.md "wikilink")，故牠要到1990年代才出現在書本之上。

## 描述

小貴族龍的[化石只有部分](../Page/化石.md "wikilink")[頭顱骨及下頜](../Page/頭顱骨.md "wikilink")，並一些未描述的顱後骨骼\[2\]。口鼻部的大部分及[前上頜骨都遺失了](../Page/前上頜骨.md "wikilink")，但在2000年代早期利用頭顱骨碎片的重組，發現牠的[眼睛前有一個冠](../Page/眼睛.md "wikilink")\[3\]，但冠的形狀卻不清楚。頭顱骨約長87厘米（從上頜前端至連接下頜的[方骨底](../Page/方骨.md "wikilink")）\[4\]。小貴族龍的可能[獨有衍徵包括](../Page/獨有衍徵.md "wikilink")：[前齒骨沒有類似](../Page/前齒骨.md "wikilink")[牙齒的紋路](../Page/牙齒.md "wikilink")、下頜的前段大幅地向下彎、及呈[長方形的厚重](../Page/長方形.md "wikilink")[上頜骨](../Page/上頜骨.md "wikilink")\[5\]。如果小貴族龍與[阿納薩齊龍或](../Page/阿納薩齊龍.md "wikilink")[納秀畢吐龍是相同的動物](../Page/納秀畢吐龍.md "wikilink")，牠眼睛前有隆起的頭冠，延伸至兩眼之間，但沒有到眼睛之後。

## 分類

小貴族龍是屬於[櫛龍亞科](../Page/櫛龍亞科.md "wikilink")，這是一類頭部扁平或有實心頭冠的[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")。由於對小貴族龍的所知甚少，所以仍未清楚牠的最近親。納秀畢吐龍及[南方小貴族龍](../Page/南方小貴族龍.md "wikilink")（*K.
australis*）似乎非常相似，在最近的鴨嘴龍科的[系統發生學中](../Page/系統發生學.md "wikilink")，納秀畢吐龍、南方小貴族龍、[櫛龍形成了一個分支](../Page/櫛龍.md "wikilink")。但在同一份研究中，小貴族龍被標明為獨立的屬，也同時被列為[疑名](../Page/疑名.md "wikilink")\[6\]。[格里芬龍的年代較小貴族龍的遠](../Page/格里芬龍.md "wikilink")，且更為原始，而牠們之間雖然有地理及時間上的分野，牠們的顱後骨骼卻很相似\[7\]。

## 發現及歷史

[Kritosaurus_Brown_original.jpg](https://zh.wikipedia.org/wiki/File:Kritosaurus_Brown_original.jpg "fig:Kritosaurus_Brown_original.jpg")重建的小貴族龍頭部\]\]
在1904年，[巴納姆·布郎](../Page/巴納姆·布郎.md "wikilink")（Barnum
Brown）在繼續前一次的發掘工作時，在[美國](../Page/美國.md "wikilink")[新墨西哥州](../Page/新墨西哥州.md "wikilink")[聖胡安縣近](../Page/聖胡安縣_\(新墨西哥州\).md "wikilink")[白楊山發現了小貴族龍的](../Page/白楊山.md "wikilink")[正模標本](../Page/正模標本.md "wikilink")\[8\]。他最初並不確定是屬於哪一個[地層](../Page/地層.md "wikilink")，但要到1916年才能確定那是[科特蘭地層的De](../Page/科特蘭地層.md "wikilink")-na-zin段，年代為[坎帕階晚期](../Page/坎帕階.md "wikilink")\[9\]\[10\]。在發現時，大部分[頭顱骨的前部分被侵蝕或破碎了](../Page/頭顱骨.md "wikilink")，而布郎參考[大鴨龍而重組這個部分](../Page/大鴨龍.md "wikilink")，並留下許多碎片\[11\]。他雖然發現這重建與碎片之間有些分別，但卻歸因於壓碎時造成的\[12\]。他最初想取名為*Nectosaurus*，但卻發現這個名字已被使用。這個名字是因Jan
Versluys在較早前探訪了布郎後不小心洩露的\[13\]。布郎最後保留了[種小名](../Page/種小名.md "wikilink")，並將牠命名為[納瓦霍小貴族龍](../Page/納瓦霍小貴族龍.md "wikilink")（*K.
navajovius*）。

在1914年，[加拿大的](../Page/加拿大.md "wikilink")[格里芬龍的發現](../Page/格里芬龍.md "wikilink")，改變了布郎對小貴族龍鼻端的想法\[14\]。他重新檢查了那些碎片，並將小貴族龍的頭冠重組成鼻部拱起狀，類似格里芬龍\[15\]。他並將格里芬龍列為小貴族龍的[異名](../Page/異名.md "wikilink")\[16\]，並得到[查爾斯·懷特尼·吉爾摩爾](../Page/查爾斯·懷特尼·吉爾摩爾.md "wikilink")（Charles
W.
Gilmore）的支持\[17\]。這個異名由1920年代一直被沿用，當時[威廉·帕克斯](../Page/威廉·帕克斯.md "wikilink")（William
Parks）更將[加拿大的](../Page/加拿大.md "wikilink")[物種命名為內彎手小貴族龍](../Page/物種.md "wikilink")（*K.
incurvimanus*）\[18\]；內彎手格里芬龍，目前被認為是著名格里芬龍的[異名](../Page/異名.md "wikilink")\[19\]。在[理查·史旺·魯爾](../Page/理查·史旺·魯爾.md "wikilink")（Richard
Swann Lull）與[尼爾達·賴特](../Page/尼爾達·賴特.md "wikilink")（Nelda
Wright）在1942年的具影響力[北美洲](../Page/北美洲.md "wikilink")[鴨嘴龍科論文中](../Page/鴨嘴龍科.md "wikilink")，小貴族龍、格里芬龍是同種動物成為標準用法\[20\]。自此到1990年，小貴族龍至少包含了[模式種的納瓦霍小貴族龍](../Page/模式種.md "wikilink")、內彎手小貴族龍、及著名克里托龍（*K.
notabilis*，原為格里芬龍的模式種）。而魯爾及賴特更將[蒙大拿州](../Page/蒙大拿州.md "wikilink")[朱迪斯河組發現的只有](../Page/朱迪斯河組.md "wikilink")[齒骨的](../Page/齒骨.md "wikilink")[短頭鴨嘴龍](../Page/鴨嘴龍.md "wikilink")（*H.
breviceps*）\[21\]編入小貴族龍屬中\[22\]，但現時這個分類已不被接受\[23\]\[24\]。

於1970年代末及1980年代初，[鴨嘴龍被牽扯入小貴族龍與格里芬龍是否為異名的討論中](../Page/鴨嘴龍.md "wikilink")\[25\]\[26\]。[大衛·諾曼](../Page/大衛·諾曼.md "wikilink")（David
B.
Norman）的《恐龍插畫百科全書》中，將格里芬龍的加拿大化石稱為小貴族龍，但卻誤將內彎手小貴族龍稱為鴨嘴龍\[27\]。於1984年，[阿根廷](../Page/阿根廷.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（José
Bonaparte）將從阿根廷[巴塔哥尼亞](../Page/巴塔哥尼亞.md "wikilink")[內格羅河的](../Page/內格羅河.md "wikilink")[洛斯阿拉密托斯組發現的鴨嘴龍科骨頭命名為](../Page/洛斯阿拉密托斯組.md "wikilink")[南方小貴族龍](../Page/南方小貴族龍.md "wikilink")（*K.
australis*），該地層的年代為晚[白堊紀的晚](../Page/白堊紀.md "wikilink")[坎潘階到早](../Page/坎潘階.md "wikilink")[馬斯垂克階](../Page/馬斯垂克階.md "wikilink")\[28\]。但這個物種可能不是屬於小貴族龍\[29\]\[30\]。根據最新研究，南方小貴族龍是[獨孤龍的異名](../Page/獨孤龍.md "wikilink")\[31\]。

[Kritosaurus_Sabinosaurio.jpg](https://zh.wikipedia.org/wiki/File:Kritosaurus_Sabinosaurio.jpg "fig:Kritosaurus_Sabinosaurio.jpg")
在1990年，[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）及[大衛·威顯穆沛](../Page/大衛·威顯穆沛.md "wikilink")（David
B.
Weishampel）再度將格里芬龍從小貴族龍中分出來，並指出格里芬龍的部分頭顱骨有一些不確定的地方。霍納於1992年描述了兩個從新墨西哥州發現的頭顱骨，稱是屬於小貴族龍的，並顯示了與格里芬龍的分別\[32\]。但翌年，[阿德里安·亨特](../Page/阿德里安·亨特.md "wikilink")（Adrian
Hunt）及[斯潘塞·盧卡斯](../Page/斯潘塞·盧卡斯.md "wikilink")（Spencer G.
Lucas）將這兩個頭顱骨各自置放在自己的屬中，建立[阿納薩齊龍及](../Page/阿納薩齊龍.md "wikilink")[納秀畢吐龍兩個新屬](../Page/納秀畢吐龍.md "wikilink")\[33\]。目前最少有兩份文獻支持這個不同屬的分類法\[34\]\[35\]，但這並非得到所有學者的認同\[36\]。

小貴族龍的地理範圍遍佈於北美洲。從[德克薩斯州的](../Page/德克薩斯州.md "wikilink")[阿古哈組](../Page/阿古哈組.md "wikilink")（Aguja
Formation）發現了包括頭顱骨的骨頭，該地屬於[坎潘階晚期](../Page/坎潘階.md "wikilink")\[37\]\[38\]。另外最近描述的[墨西哥](../Page/墨西哥.md "wikilink")[科阿韋拉州化石可能是一個新的物種](../Page/科阿韋拉州.md "wikilink")，體型約是納瓦霍小貴族龍的1.2倍，並有明顯彎曲的[坐骨](../Page/坐骨.md "wikilink")。這頭動物可能是北美洲的最大型鴨嘴龍亞科物種。但不幸的是牠的鼻骨並非完整\[39\]。

## 古生態學

小貴族龍是發現於[科特蘭地層的De](../Page/科特蘭地層.md "wikilink")-na-zin段。這地層可追溯至[上白堊紀的](../Page/上白堊紀.md "wikilink")[坎帕階末期](../Page/坎帕階.md "wikilink")（即7400萬至7000萬年前）\[40\]，當地還發現其他幾類[恐龍](../Page/恐龍.md "wikilink")，如[阿拉摩龍](../Page/阿拉摩龍.md "wikilink")、[副櫛龍的一個](../Page/副櫛龍.md "wikilink")[物種](../Page/物種.md "wikilink")、[五角龍](../Page/五角龍.md "wikilink")、[結節頭龍](../Page/結節頭龍.md "wikilink")、[蜥鳥盜龍及未命名的](../Page/蜥鳥盜龍.md "wikilink")[暴龍科](../Page/暴龍科.md "wikilink")\[41\]。科特蘭地層被認為過去是[河流](../Page/河流.md "wikilink")[氾濫平原](../Page/氾濫平原.md "wikilink")，在[西部內陸水道退卻後出現](../Page/西部內陸水道.md "wikilink")。[松科是主要的](../Page/松科.md "wikilink")[植物](../Page/植物.md "wikilink")，而[開角龍亞科較](../Page/開角龍亞科.md "wikilink")[鴨嘴龍科普遍](../Page/鴨嘴龍科.md "wikilink")\[42\]。[副櫛龍](../Page/副櫛龍.md "wikilink")、小貴族龍、[角龍類的](../Page/角龍類.md "wikilink")[五角龍的化石都發現於北美洲北部](../Page/五角龍.md "wikilink")，沒有在北美洲南部發現化石，這可能代表白堊紀晚期的北美洲恐龍相有高度地域性，同屬恐龍可能只生存於特定範圍地區\[43\]。

## 古生物學

[Kritosaurus.jpg](https://zh.wikipedia.org/wiki/File:Kritosaurus.jpg "fig:Kritosaurus.jpg")

### 食性

小貴族龍是雙足或四足的[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，以多種的[植物為食糧](../Page/植物.md "wikilink")。牠的[頭顱骨有特別的](../Page/頭顱骨.md "wikilink")[關節](../Page/關節.md "wikilink")，可以容許磨碾的動作，牠的[牙齒會不斷的替換](../Page/牙齒.md "wikilink")，且保有達超過幾百顆牙齒，但只有少量是持續使用的。牠用寬的喙嘴來咬斷[植物](../Page/植物.md "wikilink")，並以類似面頰的[器官來裝載食物](../Page/器官.md "wikilink")。牠可以食用由地面至約四米高度的植物\[44\]。若牠是獨立的屬，牠如何與同期的納秀畢吐龍分隔食物資源，則不得而知。

### 鼻冠

小貴族龍的鼻冠，不論真正的形狀如何，可能有多種的社交功能，如性徵、物種特徵、或社會地位等\[45\]。它可能有膨胀的氣囊，以製造[視覺或](../Page/視覺.md "wikilink")[聽覺的訊號](../Page/聽覺.md "wikilink")\[46\]。

## 大眾文化

在1910年代至1990年期間，小貴族龍與格里芬龍的[異名化](../Page/異名.md "wikilink")，使對原有小貴族龍的化石出現了一些誤解。由於格里芬龍的化石較為完整，很多在20年代到90年代的小貴族龍討論，實際只是適用於格里芬龍的\[47\]\[48\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [*Kritosaurus*](http://web.me.com/dinoruss/de_4/5a7b0d6.htm) The
    Dinosaur Encyclopaedia
  - [Hadrosaurinae](https://web.archive.org/web/20090808194559/http://www.thescelosaurus.com/hadrosaurinae.htm)
    Thescelosaurus
  - [Hadrosaurinae](https://web.archive.org/web/20071029224404/http://www.fmnh.helsinki.fi/users/haaramo/metazoa/Deuterostoma/chordata/Archosauria/Ornithischia/Hadrosaurinae.htm)
    Mikko\`s Phylogeny Archive

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:櫛龍亞科](../Category/櫛龍亞科.md "wikilink")

1.

2.

3.

4.

5.
6.
7.
8.

9.

10.

11.
12.

13.

14.

15.
16.

17.
18.

19.

20.

21.

22.
23.
24.

25.

26.

27.

28.

29.

30.
31.

32.

33.

34.

35.
36.
37.

38.

39.
40.
41.

42.

43. Lehman, T. M., 2001, Late Cretaceous dinosaur provinciality: In:
    Mesozoic Vertebrate Life, edited by Tanke, D. H., and Carpenter, K.,
    Indiana University Press, pp. 310-328.

44.
45.
46.

47.
48.