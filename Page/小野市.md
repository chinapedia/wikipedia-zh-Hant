**小野市**（）是位于[日本](../Page/日本.md "wikilink")[兵庫縣中南部的城市](../Page/兵庫縣.md "wikilink")。[算盤為本市特有的傳統工藝產品](../Page/算盤_\(東亞\).md "wikilink")，稱為「」，為日本算盤的主要產地。

## 歷史

在[江戶時代初期](../Page/江戶時代.md "wikilink")，為[西條藩的領地](../Page/西條藩.md "wikilink")，1636年一柳直盛過世後，將領地分成三份，其中小野所在的[加東郡及](../Page/加東郡.md "wikilink")[伊予國](../Page/伊予國.md "wikilink")[宇摩郡交給次子](../Page/宇摩郡.md "wikilink")，由於最初一柳直家將[陣屋設於位於伊予的](../Page/陣屋.md "wikilink")[江之川](../Page/川之江市.md "wikilink")，因此名為[川之江藩](../Page/川之江藩.md "wikilink")，但隔年一柳直家及家臣即遷移至小野，小野成為實質上的藩廳。

1642年一柳直家過世時，由於未有子嗣，緊急招了女婿來繼承一柳家，但此舉違反了幕府當時禁止的規定，因此位於伊予的領地被沒收，僅留下小野一地，藩名也隨之變成[小野藩](../Page/小野藩.md "wikilink")，直到幕府結束此地都屬於小野藩的領地。1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，曾短暫設置小野縣，但旋即被整併為姬路縣，後又成為[飾磨縣](../Page/飾磨縣.md "wikilink")，1876年再次被整併到[兵庫縣](../Page/兵庫縣.md "wikilink")。

1889年日本實施[町村制](../Page/町村制.md "wikilink")，設置了**小野村**，隸屬加東郡，1915年升格為****；1954年，與同屬加東郡的、、、、[合併為](../Page/市町村合併.md "wikilink")**小野市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1926年</p></th>
<th><p>1926年-1944年</p></th>
<th><p>1944年-1954年</p></th>
<th><p>1954年-1989年</p></th>
<th><p>1989年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下東條村</p></td>
<td><p>1954年12月1日<br />
小野市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>河合村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>來住村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>市場村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小野村</p></td>
<td><p>1915年5月1日<br />
小野町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）

      - [加古川線](../Page/加古川線.md "wikilink")：（[加古川市](../Page/加古川市.md "wikilink")）
        -  -  - [粟生車站](../Page/粟生車站.md "wikilink") -  -  -
        （[加東市](../Page/加東市.md "wikilink")→）

  - [神戶電鐵](../Page/神戶電鐵.md "wikilink")

      - [粟生線](../Page/粟生線.md "wikilink")：（←[三木市](../Page/三木市.md "wikilink")）
        -  -  -  -  - 粟生車站

  -   - ：粟生車站 - （[加西市](../Page/加西市.md "wikilink")）

<File:Onomachi> Station 01.jpg|小野町車站 <File:Ao> Station.jpg|粟生車站
<File:Kawainishi> Station.jpg|河合西車站
[File:KB-Kashiyama.jpg|樫山車站](File:KB-Kashiyama.jpg%7C樫山車站)
<File:Ono> Sta Hyogo01n3200.jpg|小野車站
[File:KB-Hata.jpg|葉多車站](File:KB-Hata.jpg%7C葉多車站)

### 公路

  - 高速公路

[山陽自動車道從南側與](../Page/山陽自動車道.md "wikilink")[三木市的邊界處通過](../Page/三木市.md "wikilink")，此處設有[三木小野交流道](../Page/三木小野IC.md "wikilink")。

  - 一般國道

<!-- end list -->

  -
## 觀光資源

[Jodoji_Ono_Hyogo.JPG](https://zh.wikipedia.org/wiki/File:Jodoji_Ono_Hyogo.JPG "fig:Jodoji_Ono_Hyogo.JPG")
[Himawarinooka_Park.JPG](https://zh.wikipedia.org/wiki/File:Himawarinooka_Park.JPG "fig:Himawarinooka_Park.JPG")
[City_Museum_Ono_Hyogo02n4272.jpg](https://zh.wikipedia.org/wiki/File:City_Museum_Ono_Hyogo02n4272.jpg "fig:City_Museum_Ono_Hyogo02n4272.jpg")

  - [浄土寺](../Page/浄土寺_\(小野市\).md "wikilink")

  -
  -
  -
  -
  -
## 姊妹、友好城市

### 海外

  - [林賽](../Page/林賽_\(加利福尼亞州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[圖萊里縣](../Page/圖萊里縣.md "wikilink")）\[1\]

## 本地出身之名人

  - [上田三四二](../Page/上田三四二.md "wikilink")：歌人、小説家、文藝評論家。
  - [堀井富太郎](../Page/堀井富太郎.md "wikilink")：大日本帝國陸軍中將。

## 參考資料

## 相關條目

  - [西條藩](../Page/西條藩.md "wikilink")

  - [川之江藩](../Page/川之江藩.md "wikilink")

  - [小野藩](../Page/小野藩.md "wikilink")

  -
  - [加東郡](../Page/加東郡.md "wikilink")

## 外部連結

  -

  -

<!-- end list -->

1.