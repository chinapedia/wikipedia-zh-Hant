**小野田寬郎**（），是一位生於現在的[日本](../Page/日本.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")[海南市](../Page/海南市.md "wikilink")、曾長期擔任[軍人且階級為](../Page/軍人.md "wikilink")[少尉的人物](../Page/少尉.md "wikilink")。[第二次世界大戰時被](../Page/第二次世界大戰.md "wikilink")[大日本帝國陸軍徵召入伍](../Page/大日本帝國陸軍.md "wikilink")，1944年11月被派遣至[菲律賓盧邦島擔任守備任務](../Page/菲律賓.md "wikilink")。[美軍攻佔盧邦島後](../Page/美軍.md "wikilink")，與三名同僚躲入叢林中進行[游擊戰](../Page/游擊戰.md "wikilink")，直到1974年3月10日才向菲律賓警方投降。

## 生平

### 早年

小野田寬郎1922年3月19日出生于日本和歌山縣海南市。1939年3月到“[田島洋行](../Page/田島洋行.md "wikilink")”的[武漢分店工作](../Page/武漢.md "wikilink")。1942年12月被徵召入[和歌山](../Page/和歌山.md "wikilink")[步兵第61聯隊](../Page/步兵第61聯隊.md "wikilink")，後分配到[步兵第218聯隊](../Page/步兵第218聯隊.md "wikilink")。1943年9月成爲甲種幹部候補生，1944年1月進入[久留米第一種](../Page/久留米.md "wikilink")[日本](../Page/日本.md "wikilink")[陸軍預備士官學校步兵科](../Page/陸軍預備士官學校.md "wikilink")，8月畢業後成爲士官勤務見習士官，9月進入日本[陸軍中野學校二俁分校接受游擊戰訓練](../Page/陸軍中野學校.md "wikilink")，11月畢業後被派往菲律賓。派遣地是菲律賓[盧邦島](../Page/盧邦島.md "wikilink")，準備在美軍登陸後開展游擊戰。

### 遁入叢林

#### 前期

1944年11月，小野田被派至菲律賓的盧邦島，此時日軍在太平洋的局勢已經岌岌可危。12月17日，第8师团的师团长[横山静雄中将命令小野田率領部屬在島上展開游擊作戰](../Page/横山静雄.md "wikilink")，並對小野田說\[1\]：

1945年2月28日，美軍在盧邦島登陸，日軍大部分不是戰死便是投降。小野田與[伍長島田庄一](../Page/伍長.md "wikilink")、[上等兵小塚金七](../Page/上等兵.md "wikilink")、[一等兵赤津勇一三人一起逃入叢林](../Page/一等兵.md "wikilink")，繼續頑抗。

1945年8月15日，[日本宣布無條件投降](../Page/日本投降.md "wikilink")。美軍派遣日本降軍赴[太平洋各島勸降](../Page/太平洋.md "wikilink")，並空投大量的傳單。小野田認定這是美軍的計謀，絲毫不為所動。每天清晨，小野田都會帶著三名同胞爬上山崗，對著東昇的旭日敬禮。他們不斷地移動自己的位置，並偷竊當地居民的菜果、獵捕居民的家禽來充飢，喝河水、雨水解渴。他们甚至将保存晒干的香蕉充当干粮，以便维持一定的热量。但是他们无法猎取太多的食物，因为枪声会暴露他的战斗位置。大雨傾盆之時，他們用自己的身體護住[步槍](../Page/步槍.md "wikilink")、[地雷及](../Page/地雷.md "wikilink")[炸藥](../Page/炸藥.md "wikilink")，但他们得彼此警惕，尽量保持清醒，以免在睡觉时因体温过低而死亡。在如此惡劣的環境之下，小野田依然沒有忘記長官交付他的任務─「游擊戰」。每個月，他們都會襲擊軍車，槍殺司機，奪取物資；有時，他們也會出現在村落，射殺當地農民。

1949年，赤津受不了惡劣的環境與身心的煎熬脫隊，隔年6月向[菲律賓警察](../Page/菲律賓警察.md "wikilink")「投降」，菲律賓與美軍因此掌握了潛伏中三人的身分。赤津在投降之後，也參與了勸降的行列；小野田等人依舊不為所動，每天早上依然爬上山頭，向旭日敬禮，等待援軍的到來。

1952年，[菲律賓政府不斷地將小野田親人的家書以及日本當時的報紙散播在叢林之中](../Page/菲律賓政府.md "wikilink")，希望他們三人能夠早日投降。然而小野田始終認為這是美軍的計謀，因為他認為，如果日本真的投降，那他的長官谷口一定會告訴他「任務取消」。

1953年6月, 岛田在一次行动中被当地渔民射伤大腿。虽然后来复原，但在隔年5月7日，島田在一次與菲律賓警方的衝突中被打死。

#### 後期

雖然小野田確實閱讀了勸降用的傳單、報紙與家書，但他自行編造出世界觀加以解釋，他認為「[日本本土雖遭美軍佔領](../Page/日本本土.md "wikilink")，但日軍仍在[满洲進行抗戰](../Page/满洲地区.md "wikilink")」，並相信援軍終將來到。他將[朝鲜战争解讀為满洲日軍開始反擊](../Page/朝鲜战争.md "wikilink")，活動地附近美軍空軍基地[越戰期間的頻繁飛機起降則是日軍重返](../Page/越戰.md "wikilink")[南太平洋之故](../Page/南太平洋.md "wikilink")；勸降用的報紙上日本現代化的現況，更讓他相信變得富強的日本不可能戰敗。潛伏晚期小野田還會利用偷來的收音機收聽日本的[賽馬比賽](../Page/賽馬.md "wikilink")，與最後的战友小塚猜測勝負以為消遣，並不如一般想像中的與世隔絕。

1972年10月，小野田在附近的村莊埋設了剩下的最後一枚[地雷](../Page/地雷.md "wikilink")；因爲生銹，地雷沒有爆炸。1972年10月9日，菲律賓警察部隊得到當地農民的報告，在盧邦島發現了兩個舊日本軍人在山崗上燒稻草。菲律賓警察部隊索特上士等三人連忙趕到現場，兩方爆發槍戰；結果小塚身中兩槍，其中一枪穿越心脏导致其当场毙命、身邊扔著保養良好的[38式步槍](../Page/三八式步枪.md "wikilink")，而小野田則逃入叢林。日本投降27年後，日本士兵的死亡引起了[日本政府的高度重視](../Page/日本政府.md "wikilink")。日本馬上派人到緬甸、馬來西亞和菲律賓尋找藏在森林中的日軍士兵。並留下報紙、雜誌，還有小塚在日本的喪禮等消息給小野田。孤身一人的小野田仍決心繼續游擊九年。他甚至计划以死突击雷达基地。

1974年2月20日，小野田在叢林中遇到專程前來尋找他的日籍自由[探險家](../Page/探險家.md "wikilink")[鈴木紀夫](../Page/鈴木紀夫.md "wikilink")。鈴木告訴小野田，戰爭真的已經結束了；但小野田堅持必須有谷口义美少佐的命令才願意投降，同時要親自將20餘年來保存良好的軍刀交給[天皇](../Page/天皇.md "wikilink")。鈴木回國後，幾經波折，找到了谷口少佐，原来谷口已经改名并成了书商。並請谷口書寫一份要求小野田投降的命令。1974年3月9日，小野田接到了来到当地的谷口親自宣布的投降命令；隔天，已经52歲的小野田身著已經破爛的日本軍服，翻過整個山頭，來到了警察局，放下肩上的[38式步槍](../Page/三八式步枪.md "wikilink")，說：「我是陸軍少尉小野田寬郎，我奉上級的命令向你們投降。」

小野田在29年的戰鬥當中，一共造成了130名以上的菲律賓人死傷，除了少數[軍人](../Page/軍人.md "wikilink")、[警察外](../Page/警察.md "wikilink")，還有大量[平民](../Page/平民.md "wikilink")，许多菲律宾人主张把小野田关进监狱，并绳之以法。但是，由于日本政府的斡旋，後來獲得了[菲律賓獨裁者](../Page/菲律賓.md "wikilink")[費迪南德·馬可仕的](../Page/費迪南德·馬可仕.md "wikilink")[赦免](../Page/赦免.md "wikilink")，并允许已经52岁的小野田返回日本。

1974年3月12日，他与铃木纪夫和谷口义美一起回到日本。

### 晚年

小野田回國後，拒絕接受日本政府給予他的100萬[日圓補償金](../Page/日圓.md "wikilink")，後來他將這筆款項捐給了[靖國神社](../Page/靖國神社.md "wikilink")。之後，他也婉拒了天皇召見，理由是「天皇陛下說不定會低著頭跟我說『對不起，辛苦你了』吧？我不希望發生這種事情」（もし、今の陛下に会ったら、陛下が私に頭を下げてしまうだろう。『申し訳なかった、ご苦労さん』と。それをさせたくない）。此外，他還去祭拜了當年與他並肩作戰，卻不幸陣亡的小塚與島田。

在他生活几十年来，他就一直活在1944年。纵使世界已经不断的改变，他始终活在二次大战的年代里不能自拔。当他回到了现代世界的日本，面对社会的变迁时，他完全无法理解，特别對於日本[新憲法中對於軍事行動的限制相當不滿](../Page/日本國憲法.md "wikilink")，並且對戰後的日本社會相當不能適應；\[2\]半年後，他[移民至](../Page/移民.md "wikilink")[巴西定居](../Page/巴西.md "wikilink")，並經營農場有成。之後，他有感於時下日本年輕人好勇鬥狠，便以「為了祖國而希望培養出健全的日本國民」為號召，創立了「小野田自然塾」，於假期指導青少年[野外求生技巧](../Page/野外求生.md "wikilink")，經常往返日巴兩國。

小野田是個典型的受[軍國主義教育的日本軍人](../Page/軍國主義.md "wikilink")，晚年他經常參與許多右翼組織舉辦的[愛國主義活動](../Page/愛國主義.md "wikilink")。每每聽見日本軍歌時，他總是會激動地直流眼淚。當他接受無數次媒體的訪問，當他被問到如何看待上百名傷亡的無辜農民與破碎的家庭時，他堅決地認為，他身處於作戰之中，不必為這些平民的死亡負責。他一貫的口吻是，「軍人就是服從命令，在不違反[國際法的狀況下](../Page/國際法.md "wikilink")，我沒有責任」。但是他在1996年捐出了1万美金给曾經游擊29年的[盧邦島的学校当奖学金](../Page/盧邦島.md "wikilink")。

菲律賓前第一夫人[艾美黛·馬可斯回憶說](../Page/艾美黛·馬可斯.md "wikilink")：「我在小野田投降後不久同他谈了话。他好長時間没有明白究竟發生了什么。當我們告訴他戰爭早在1945年已經結束時，他都驚呆了。他問：『日本怎麼會敗？我幹嘛要像愛護嬰兒一樣愛護槍？』他坐在那裡，失聲痛哭。」

2014年1月16日，小野田於91歲時因[肺炎引起](../Page/肺炎.md "wikilink")[心臟衰竭並發症於東京](../Page/心臟衰竭.md "wikilink")[聖路加國際醫院過世](../Page/聖路加國際醫院.md "wikilink")。\[3\]

## 著書

  - 《戰鬥、求生、盧邦島30年》『戦った、生きた、ルバン島30年』 (小野田寛郎\[他\],
    [講談社](../Page/講談社.md "wikilink"), 1974年)
  - 『遥かに祖国を語る』 (小野田寛郎,[酒卷和男](../Page/酒卷和男.md "wikilink"),
    [時事通信社](../Page/時事通信社.md "wikilink"), 1977年)
  - 《我的巴西人生》『わがブラジル人生』 ([講談社](../Page/講談社.md "wikilink"), 1982年) ISBN
    4-06-145914-7
  - 『子どもは野性だ』 ([学習研究社](../Page/学習研究社.md "wikilink"), 1984年) ISBN
    4-05-101464-9 (『鈴木健二のお父さん子どもに野性を贈ろう』と同じISBN)
  - 『子どもは風の子、自然の子-『ジャングルおじさん』の自然流子育て』 ([講談社](../Page/講談社.md "wikilink"),
    1987年) ISBN 4-06-203382-8
  - 『たった一人の30年戦争』 ([東京新聞出版局](../Page/東京新聞.md "wikilink"), 1995年) ISBN
    4-8083-0535-6
  - 《我回憶中的盧邦島》『わが回想のルバング島』 ([朝日新聞社](../Page/朝日新聞社.md "wikilink"), 1995年)
    ISBN 4-02-261109-X
  - 『極限で私を支えたもの』 ([山田村教育委員会](../Page/山田村教育委員会.md "wikilink"), 1997年)
  - 『小野田寛郎-わがルバン島の30年戦争 (人間の記録 (109))』
    ([日本圖書中心](../Page/日本圖書中心.md "wikilink"),
    1999年) ISBN 4-8205-5769-6
  - 『講演・シンポジウム記録集-平成11年』 ([靖國神社崇敬奉贊会](../Page/靖國神社崇敬奉贊会.md "wikilink"),
    1999年)
  - 『生きる』 (小野田寛郎\[他\],
    [富山縣民生涯学習カレッジ](../Page/富山縣民生涯学習カレッジ.md "wikilink"),
    2001年)
  - 『智慧の実を食べよう。』([糸井重里編著](../Page/糸井重里.md "wikilink"),
    [ぴあ](../Page/ぴあ.md "wikilink"), 2004年) ISBN 4-8356-0903-4
  - 『君たち、どうする?』 ([新潮社](../Page/新潮社.md "wikilink"), 2004年) ISBN
    4-10-471301-5
  - 『だから日本人よ、靖国へ行こう』 (小野田寛郎,[中條高德](../Page/中條高德.md "wikilink"),
    [ワック](../Page/ワック.md "wikilink"), 2006年) ISBN 4-89831-091-5
  - 『魚は水人は人の中-今だからこそ伝えたい師小野田寛郎のことば』
    ([原充男著](../Page/原充男.md "wikilink"),小野田寛郎述.
    [清流出版](../Page/清流出版.md "wikilink"), 2007年) ISBN
    978-4-86029-161-7
  - 『ルバング島戦後30年の戦いと靖国神社への思い ([まほろばシリーズ](../Page/まほろば.md "wikilink") 2)』
    ([明成社](../Page/明成社.md "wikilink"), 2007年) ISBN 978-4-944219-57-5

## 參見

  - [殘留日本兵](../Page/殘留日本兵.md "wikilink")
  - [史尼育唔](../Page/史尼育唔.md "wikilink")（李光輝）
  - [上野石之助](../Page/上野石之助.md "wikilink")
  - [橫井庄一](../Page/橫井庄一.md "wikilink")

## 註釋

<div class="references-small">

<references group="注"/>

</div>

## 註釋

## 參考文獻

  - [小野田種次郎](../Page/小野田種次郎.md "wikilink") 『ルバングの譜(ウタ)―寛郎を捜しつづけて30年』
    潮出版社、1974。(父親の手記)
  - ―― (「小野田凡二」名義) 『回想のルバング―寛郎を待った三十年』 浪曼、1974。(同上。「凡二」は俳号)
  - [小野田町枝](../Page/小野田町枝.md "wikilink")
    『私は戦友になれたかしら―小野田寛郎とブラジルに命をかけた30年』
    (夫人の手記) ISBN 4-86029-013-5
  - [鈴木紀夫](../Page/鈴木紀夫.md "wikilink") 『大放浪―小野田少尉発見の旅』 (発見者の手記) ISBN
    4-02-261116-2
      - 1974年、文藝春秋刊の文庫化。
  - ―― 「小野田少尉発見の旅」(『「文藝春秋」にみる昭和史 第3巻』ISBN 4-16-362650-6 に収録)
  - [津田信](../Page/津田信.md "wikilink") 『幻想の英雄―小野田少尉との三ヵ月』 圖書出版社、1977。
  - [戶井十月著](../Page/戶井十月.md "wikilink") 『小野田寛郎の終わらない戦い』（新潮社,2005年）ISBN
    978-4-10-403104-7

## 外部連結

  - [尋找最後的皇軍？還是尋找軍國主義的活化石？](http://www.cuhkacs.org/~benng/Bo-Blog/read.php?169)
  - [In March of 1974, Hiroo Onoda surrendered.--the 45th
    phot0](http://www.theatlantic.com/photo/2011/10/world-war-ii-after-the-war/100180/#img01)

[740310](../Page/category:殘留日本兵.md "wikilink")
[category:死於心臟衰竭的人](../Page/category:死於心臟衰竭的人.md "wikilink")

[Category:日本陆军軍人](../Category/日本陆军軍人.md "wikilink")
[Category:和歌山縣出身人物](../Category/和歌山縣出身人物.md "wikilink")

1.  p57 小野田寛郎の終わらない戦い
2.  他對戰後的日本社會不能適應的程度甚至看見洗衣机等家用电器就令他害怕，而喷气式飞机和电视更把他吓得心惊肉跳。
3.  环球网综合报道,"“二战最后投降兵”去世
    战后在1974年才投降"[1](http://world.huanqiu.com/exclusive/2014-01/4765366.html),2014-01-17
    11:29,[环球网](../Page/环球网.md "wikilink").