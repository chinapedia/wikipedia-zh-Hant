**小鍵琴**（；；；），也称**擊弦鍵琴**或**翼琴**，是早期鍵盤樂器的一種,
[15世紀初便已出現](../Page/15世紀.md "wikilink")。曾經盛行於[歐洲的](../Page/歐洲.md "wikilink")[鍵盤樂器](../Page/鍵盤樂器.md "wikilink")、[擊弦樂器](../Page/擊弦樂器.md "wikilink"),
音色貼近[結他](../Page/結他.md "wikilink")。[霍恩博斯特爾-薩克斯分類法](../Page/霍恩博斯特爾-薩克斯分類法.md "wikilink")(H-S
分類)屬於314.122-4-8;
即是由[鍵盤通過擊打](../Page/鍵盤.md "wikilink")[琴弦](../Page/琴弦.md "wikilink")[切線的](../Page/切線.md "wikilink")[弦鳴樂器](../Page/弦鳴樂器.md "wikilink")。

现代[钢琴发明以前](../Page/钢琴.md "wikilink")，与“[大键琴](../Page/大键琴.md "wikilink")”（拨弦键琴）同是欧洲[音乐生活中普遍使用的键盘类乐器](../Page/音乐.md "wikilink")。小鍵琴主要流行在[中世紀的晚期](../Page/中世紀.md "wikilink"),
[文藝復興時期](../Page/文藝復興.md "wikilink"),
[巴洛克時期以及](../Page/巴洛克時期.md "wikilink")[古典時期](../Page/古典時期.md "wikilink")。以往它主要用作練習樂器和作曲的輔助,
而不是作公開表演。由於[古鋼琴的出現](../Page/古鋼琴.md "wikilink"),
到了[18世紀末](../Page/18世紀.md "wikilink"),
[大鍵琴逐漸式微](../Page/大鍵琴.md "wikilink")。反而,
小鍵琴卻普遍的成為了個人的樂器,
此現象一直持續到[19世紀](../Page/19世紀.md "wikilink");
尤其是活躍在[北歐國家的地區](../Page/北歐國家.md "wikilink")。[丹麥浪漫作曲家](../Page/丹麥.md "wikilink")[卡爾·尼爾森](../Page/卡爾·尼爾森.md "wikilink"),
甚至在當時, 都用過小鍵琴來作曲\[1\]。

古典時期音樂家[莫札特的小鍵琴](../Page/莫札特.md "wikilink"),
由[胡桃木製成](../Page/胡桃.md "wikilink"),
該琴擁有5個8度[音階](../Page/音階.md "wikilink")。莫扎特並不會利用他自己的小鍵琴作公開演奏,
而是平時主要用它在家裡練習和作曲。根據他的妻子康斯坦茲·莫扎特貼在小鍵琴上的手寫小[筆記證明](../Page/筆記.md "wikilink"),
莫扎特的確僅僅用了五個月的時間, 利用他的私人小鍵琴這件樂器, 完成了他最後的作品,
如[魔笛](../Page/魔笛.md "wikilink") (The Magic
Flute)、[提圖斯的慈悲](../Page/提圖斯.md "wikilink") (La clemenza di
Tito) 以及[安魂曲](../Page/安魂曲.md "wikilink")
(Requiem)\[2\]。另外，小鍵琴亦是巴洛克時期音樂家[約翰·塞巴斯蒂安·巴哈最喜歡的樂器](../Page/約翰·塞巴斯蒂安·巴哈.md "wikilink")，程度更遠高於他自己的[大鍵琴](../Page/大鍵琴.md "wikilink")。不論他用來練習或者[模仿使用](../Page/模仿.md "wikilink")，巴哈都認為，小鍵琴是他[學習音樂和透過音樂來表達他最細緻](../Page/學習.md "wikilink")[感情的最佳樂器](../Page/感情.md "wikilink")\[3\]。

## 名稱由來

小鍵琴的英文 clavichord 的 clavi 和 chord
其實是來自於[拉丁文單詞](../Page/拉丁文.md "wikilink")
clavis，意思是“琴鍵”（與更常見的 clavus 相關，意思是金屬平釘，桿等）和
chorda（來自[希臘語](../Page/希臘語.md "wikilink"):
χορδή），意思是弦樂，特別用來指樂器。在其他[歐洲語言中都使用了類似的名稱](../Page/歐洲語言.md "wikilink")（義大利語：clavicordio，clavicordo;
[法語](../Page/法語.md "wikilink"): clavicorde;
[德語](../Page/德語.md "wikilink"): Klavichord; 拉丁語：clavicordium;
[葡萄牙語](../Page/葡萄牙語.md "wikilink")：clavicórdio;
[西班牙語](../Page/西班牙語.md "wikilink")：clavicordio）。許多語言的另一個名稱來自拉丁語
manus，意思是指“手”（義大利語：manicordo; 法語: manicorde，manicordion;
西班牙語：manicordio，manucordio）。其他名稱指的是單弦式性質（義大利語：monacordo 或
monocordo;
西班牙語：monacordio）的擊打獨立琴弦的小鍵琴。[意大利語也使用了](../Page/意大利語.md "wikilink")
sordino，通常都是指安靜的聲音（sordino 通常指靜音)\[4\]。

## 歷史

[Detail_of_case_decoration_on_Clavichord_by_Philip_Jacob_Specken,_N33932_The_Swedish_Museum_of_Performing_Arts_04.jpg](https://zh.wikipedia.org/wiki/File:Detail_of_case_decoration_on_Clavichord_by_Philip_Jacob_Specken,_N33932_The_Swedish_Museum_of_Performing_Arts_04.jpg "fig:Detail_of_case_decoration_on_Clavichord_by_Philip_Jacob_Specken,_N33932_The_Swedish_Museum_of_Performing_Arts_04.jpg")[德累斯頓出生的菲利普](../Page/德累斯頓.md "wikilink")·雅各·斯帕肯
(Philip Jacob Specken 1680/90 - 1762),
是當時[瑞典著名的小鍵琴製造商](../Page/瑞典.md "wikilink")。圖中是其中一部由他製造的小鍵琴;
外殼繪畫上裝飾的圖畫。該部小鍵琴現存放在瑞典[斯德哥爾摩的表演藝術博物館內](../Page/斯德哥爾摩.md "wikilink")

</center>

\]\]
[Komponierzimmer_Bachhaus.jpg](https://zh.wikipedia.org/wiki/File:Komponierzimmer_Bachhaus.jpg "fig:Komponierzimmer_Bachhaus.jpg")所展出的音樂家[約翰·塞巴斯蒂安·巴哈的私人小鍵琴](../Page/約翰·塞巴斯蒂安·巴哈.md "wikilink")（圖中右下方)

</center>

\]\] 小鍵琴是最古老的鍵盤樂器之一,
大約在15世紀初出現。它是從[12世紀透過單絃琴](../Page/12世紀.md "wikilink")
(monochord)\[5\]慢慢發展至多絃琴 (polychord with
bridge)\[6\]而成。亦即是說，小鍵琴就是將多絃琴加上鍵盤而演變成的。

### 14世紀－19世紀

"Clavichord"一詞最早出現在Eberhard Cers
ne作於[1404年的一首名為](../Page/1404年.md "wikilink")《Der
Minne regal戀詩歌手的規則》的[詩歌之中](../Page/詩歌.md "wikilink")。
在[英格蘭](../Page/英格蘭.md "wikilink")，最早提到小鍵琴之一的地方；出現在[亨利七世的妻子](../Page/亨利七世.md "wikilink")[約克的伊麗莎白](../Page/約克的伊麗莎白.md "wikilink")[女王的私人開支中](../Page/女王.md "wikilink")。當中在1502年8月的一篇文章中提到：

> 【Item. The same day, Hugh Denys for money by him delivered to a
> stranger that gave the queen a payre of clavycordes. In crowns form
> his reward iiii libres.】\[7\]

而關於小鍵琴的最早圖畫，據說是在那不勒斯的創作的一幅壁畫之中，當時大約為[1435年](../Page/1435年.md "wikilink")。大約在[1440年](../Page/1440年.md "wikilink")，阿勞特的手稿及[德國音樂家菲爾東的論著](../Page/德國.md "wikilink")《音樂精義》裏，有小鍵琴結構圖示和說明。現存的最早的小鍵琴製造於[1537年](../Page/1537年.md "wikilink")，現放存在[紐約大都會博物館內](../Page/紐約大都會博物館.md "wikilink")；而另一個小鍵琴則是在[1543年](../Page/1543年.md "wikilink")，由多美尼科製造。現放存在[萊比錫](../Page/萊比錫.md "wikilink")、卡爾·[馬克思大學的樂器博物館之內](../Page/馬克思.md "wikilink")。在流行使用[風琴的時代](../Page/風琴.md "wikilink")；風琴師一般都是使用踏板大鍵琴
(英：pedal harpsichord) 和踏板小鍵琴 (英：pedal clavichord) 作為練習的樂器。

最早的小鍵琴於[1404年便已經出現](../Page/1404年.md "wikilink")。現今所保存的最早製造的小鍵琴，是由
Domenico da Pesaro (1533-1575)
於[1543年製造](../Page/1543年.md "wikilink")。[1601年](../Page/1601年.md "wikilink")1月24日,
意大利耶穌會傳教士[利瑪竇](../Page/利瑪竇.md "wikilink")（Matteo
Ricci）去到[北京](../Page/北京.md "wikilink"),
帶著他多年積累的禮物向[萬曆皇帝獻禮](../Page/萬曆皇帝.md "wikilink")。這些禮物包括機械[鐘錶](../Page/鐘錶.md "wikilink"),
[宗教物品和小鍵琴這種樂器](../Page/宗教.md "wikilink")\[8\]。[利瑪竇的同工](../Page/利瑪竇.md "wikilink")[龐迪我](../Page/龐迪我.md "wikilink")
(Diego de Pantoja 1571-1618)
曾經亦跟隨利瑪竇去到當時的[大明](../Page/明朝.md "wikilink"),
教導[明神宗彈奏小鍵琴](../Page/明神宗.md "wikilink")\[9\]。

最初期的小鍵琴, 只是使用一根或兩根琴絃。當鍵琴的切槌敲在琴絃上，依振動的長短而形成高低音。這種小鍵琴也被稱為琴格小鍵琴 (英：fretted
clavichord)\[10\]。由於兩個或四個一組相鄰的音；通常共同敲打在一個琴絃上，因此鄰近幾個的音，就無法同時彈奏了。[18世紀早期所製造的小鍵琴](../Page/18世紀.md "wikilink")，通常都是琴格小鍵琴。後來；由於技術不斷進步，小鍵琴發展到每個音都擁有自己的琴絃，稱為無琴格的小鍵琴
(英：unfretted
clavichord)\[11\]。[15世紀初小鍵琴都是始於](../Page/15世紀.md "wikilink")4個8度音階範圍，但到[18世紀就增加到](../Page/18世紀.md "wikilink")5個8度或更高的[音階](../Page/音階.md "wikilink")。到了[16世紀至](../Page/16世紀.md "wikilink")[18世紀](../Page/18世紀.md "wikilink")，小鍵琴在[歐洲才慢慢地普及起來](../Page/歐洲.md "wikilink")。其主要盛行在[德語的國家](../Page/德語.md "wikilink")，[斯堪的納維亞半島以及](../Page/斯堪的納維亞.md "wikilink")[伊比利亞半島](../Page/伊比利亞.md "wikilink")。由於鍵盤樂器演奏漸漸被[古鋼琴及現代](../Page/古鋼琴.md "wikilink")[鋼琴所取代](../Page/鋼琴.md "wikilink")，到了[1850年之後](../Page/1850年.md "wikilink")，小鍵琴才幾乎被人完全遺忘。

### 20世紀－現在

小鍵琴到了今天主要由[文藝復興](../Page/文藝復興.md "wikilink")，[巴洛克和](../Page/巴洛克.md "wikilink")[古典音樂愛好者演奏](../Page/古典音樂.md "wikilink")。時至今日，小鍵琴在各地有不斷復興之勢\[12\],
除了吸引了不少對西洋古樂研究者的興趣和高度關注之外,
它們還吸引了許多感興趣的買家，並在全球各地大量翻新和製造\[13\]。現在，全世界各地也成立了許多小鍵琴協會。而在過去，已有大約400多種有關小鍵琴樂器的錄音問世。[20世紀最著名的小鍵琴音樂家](../Page/20世紀.md "wikilink")，當數[克利斯朵夫·霍格伍德](../Page/克利斯朵夫·霍格伍德.md "wikilink")
(Christopher Hogwood, 1941-2014),
[1992年他成為了](../Page/1992年.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦皇家音樂學院古樂教授](../Page/倫敦皇家音樂學院.md "wikilink")；此外，還有來自英國[倫敦國王學院的音樂教授瑟斯頓](../Page/倫敦國王學院.md "wikilink")·達特
(Thurston Dart, 1921-1971)\[14\]。
[Clavinet_d6.jpg](https://zh.wikipedia.org/wiki/File:Clavinet_d6.jpg "fig:Clavinet_d6.jpg")
小鍵琴也以其他類型的音樂而受到關注，其形式為電子小鍵琴(clavinet)，它使用磁性[拾音器來產生及放大信號](../Page/拾音器.md "wikilink")\[15\]。[史提夫·汪達](../Page/史提夫·汪達.md "wikilink")（Stevie
Wonder）在他的許多歌曲中都使用了電子小鍵琴\[16\]，例如“Superstition”和“Higher
Ground”。電子小鍵琴通過具有吉他踏板效果的樂器放大器來演奏，這通常與[20世紀](../Page/20世紀.md "wikilink")[70年代的](../Page/70年代.md "wikilink")[放克](../Page/放克.md "wikilink")[迪斯科](../Page/迪斯科.md "wikilink")[搖滾舞曲相關聯](../Page/搖滾.md "wikilink")\[17\]。[盖伊·西格斯沃思](../Page/盖伊·西格斯沃思.md "wikilink")（Guy
Sigsworth）在[碧玉](../Page/碧玉_\(歌手\).md "wikilink")（Björk）的現代舞台上演奏了小鍵琴，特別是在“All
Is Full of
Love”的錄音室的錄音中。碧玉還在她[2007年專輯](../Page/2007年.md "wikilink")“Volta”的歌曲“My
Juvenile”中大量使用甚至演奏這種特有的樂器。[多莉·艾莫絲](../Page/多莉·艾莫絲.md "wikilink")（Tori
Amos）在她的專輯“Boys for Pele”中的“Little Amsterdam”以及在2007年專輯“American Doll
Posse”中的歌曲“Smokey Joe”也使用小鍵琴這種樂器。
多莉·艾莫絲還在她[2004年錄製的](../Page/2004年.md "wikilink")“Not
David Bowie”中使用了電子小鍵琴，這是她[2006年的](../Page/2006年.md "wikilink")“A
Piano：The
Collection”[套裝的一部分](../Page/Boxset.md "wikilink")。[1976年](../Page/1976年.md "wikilink")，奧斯卡·彼得森（Oscar
Peterson 1925-2007）在小鍵琴上演奏波吉和貝絲（Porgy And
Bess）的歌曲。[凱斯·傑瑞](../Page/凱斯·傑瑞.md "wikilink")（Keith
Jarrett）還錄製了一張名為“Book of Ways”(1986)
的專輯，其中他演奏了一系列的小鍵琴的[即興創作](../Page/音樂即興.md "wikilink")。[披頭士樂隊的](../Page/披頭士樂隊.md "wikilink")“For
No One”(1966)
以[保羅·麥卡特尼演奏小鍵琴為特色](../Page/保羅·麥卡特尼.md "wikilink")。而里克·威克曼（Rick
Wakeman）在“地球中心之旅”（Journey to the Earth of the Earth）專輯中的“戰鬥”（The
Battle）中亦用了電子小鍵琴來演奏。

## 小鍵琴結構

### 鍵琴外形

[Clavicorde_pedalier.jpg](https://zh.wikipedia.org/wiki/File:Clavicorde_pedalier.jpg "fig:Clavicorde_pedalier.jpg")所製造的腳踏式小鍵琴

</center>

\]\]
小鍵琴的好處是盒狀、琴身[體積細小](../Page/體積.md "wikilink")，對必須經常前往不同的地方演出的演奏者來說，攜帶和修理都很方便。小鍵琴的一般外形是一個帶有鍵盤的長方形盒子，底部由四隻腳撐起來。但有些是沒有腳的，可以放在桌上彈奏。
雖然小鍵琴通常是單排鍵盤的手動樂器，但也有踏板式的小鍵琴 (pedal
clavichord)，以提供多個鍵盤來彈奏。由於踏板式小鍵琴增加了一個踏板琴弦，即是其中多了一個用於低音的踏板鍵盤，因而變相也可以提供個人用來當作練習[風琴了](../Page/風琴.md "wikilink")\[18\]。

關於腳踏式的小鍵琴(見右上第一幅圖)，一位[荷蘭學者斯佩爾斯特拉](../Page/荷蘭.md "wikilink")·喬爾
(Speerstra Joel) 在[2004年](../Page/2004年.md "wikilink")\[19\]提出了一個有趣的案例,
即巴哈的“8小前奏曲和賦格曲”, 現在被認為是偽作,
實際上可能是真的。鍵盤樂器的作曲似乎不適合管風琴，但斯佩爾斯特拉·喬爾卻認為它們在踏板式的小鍵琴上是慣用的。正如斯佩爾斯特拉·喬爾和彼得·威廉斯
(Peter Williams)
在[2003年的時候也指出的那樣](../Page/2003年.md "wikilink")\[20\]，巴哈的6個管風琴奏鳴曲之三重奏（BWV
525-530）的鍵盤部分的音域很少低於高音C調，因此演奏者可以在一個手動的踏板小鍵琴上演奏，將左手向下移動8度，而這個做法亦是18世紀的演奏習慣。

### 基礎結構

[Clavichord_action.svg](https://zh.wikipedia.org/wiki/File:Clavichord_action.svg "fig:Clavichord_action.svg")
[W2509_JocelyneCuiller_Clavicorde_FolleJournee_1fev17_0263_flou.jpg](https://zh.wikipedia.org/wiki/File:W2509_JocelyneCuiller_Clavicorde_FolleJournee_1fev17_0263_flou.jpg "fig:W2509_JocelyneCuiller_Clavicorde_FolleJournee_1fev17_0263_flou.jpg")
小鍵琴是根據測弦器的原理發展而成的。其發聲的結構主要由琴碼、弦軸、琴弦、齒軌、楔槌、全弦固定製音結、銷釘、琴鍵、平衡軌等部分組成。其中，全弦固定製音結是小鍵琴的製音裝置。這種製音結是用布條編織並固定在每一根琴弦上。由於製音結的作用，使得小鍵琴無論怎樣擊弦，琴弦都不是全段都發音的。小鍵琴內部將一組琴弦用琴馬橫拉於共鳴箱上，琴身的左下側裝有一套鍵盤，鍵盤通過一組機械裝置連接一組裝有金屬頭的鍵子。

#### 比較圖表

小鍵琴的基礎結構分為有琴格和無琴格的兩種結構。它們彼此都各有優缺點(見下表)。

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>兩種不同琴弦結構的優點與缺點:</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>優點</p></td>
<td><p>缺點</p></td>
</tr>
<tr class="even">
<td><p>小鍵琴的琴弦結構主要分為:</p></td>
<td><p>有琴格 (Fretted)</p></td>
</tr>
<tr class="odd">
<td><p>無琴格 (Unfretted)</p></td>
<td><p><br />
1.) 音域更廣[21]<br />
2.) 可以完全按照樂譜來彈奏而不用擔心有不好的音符出現<br />
3.) 聲音更甜美，音調更平和</p></td>
</tr>
</tbody>
</table>

### 聲音效果

[Range_of_clavichord.png](https://zh.wikipedia.org/wiki/File:Range_of_clavichord.png "fig:Range_of_clavichord.png")
[Tangenty.JPG](https://zh.wikipedia.org/wiki/File:Tangenty.JPG "fig:Tangenty.JPG")
小鍵琴的琴弦從左側的掛鉤導軌橫向延伸到右側的調弦軸。琴弦當中都是經過彎曲的木橋。鍵盤是帶有小金屬黃銅切線的槓桿,
其形狀和尺寸類似於位於遠端的平頭螺絲刀的頭部。琴弦一般都是由黃銅製成,
或者黃銅和鐵的組合，通常排列成對\[22\]。按下琴鍵時，切線撞擊上方琴弦。只要按住琴鍵，它就會與琴弦保持接觸。每個音可以通過更猛或更柔和的方式來改變，並且透過改變對弦的切線的力（稱為Bebung\[23\]）也可以影響[音高](../Page/音高.md "wikilink")。當放開琴鍵時，切線失去與琴弦的接觸，並且琴弦的振動會立即被阻尼布條所壓靜\[24\]。

小鍵琴的彈奏動作在所有鍵盤樂器中是比較獨特的, 因為彈奏動作的一部分不但啟動聲音的振動, 同時也限定振動弦的端點,
從而確定其[音高](../Page/音高.md "wikilink")。演奏者按動琴鍵;
小鍵琴的鍵子便抬起用銅楔槌擊打琴弦發出聲音。由於演奏者的手指與聲音產生之間具有這種緊密的接觸,
小鍵琴因而成為演奏者最貼心的鍵盤樂器。儘管小鍵琴有許多或者甚至是嚴重的限制;
例如包括是彈奏出極低的[音量](../Page/音量.md "wikilink"),
但是它還是具有相當的表現力。小鍵琴和鋼琴不同的是, 小鍵琴楔槌不是立刻離開琴弦, 而是保持在弦上的壓力。這樣, 玩家的手指能夠保持在弦上的壓力,
控制小鍵琴琴鍵的持續時間和音量, 直到手指離開琴鍵,
這樣便能產生出某些微妙的[音調膨脹的變化效果以及一種獨特的擊弦](../Page/音調.md "wikilink")[顫音](../Page/顫音.md "wikilink")。

#### 優點

小鍵琴可以用手指控制[音量](../Page/音量.md "wikilink"),
並且可以產生出細微的[音色變化的效果](../Page/音色.md "wikilink"),
所以它不但[織體優美柔和](../Page/織體_\(音樂\).md "wikilink")、富有歌唱性,
而且[音質清脆古朴](../Page/音質.md "wikilink")、精緻獨特而帶有韻味。由於小鍵琴產生的音量較低,
故便於作曲家在家中勤於練習、寫曲或模仿演奏而不會打擾他人。

#### 缺點

小鍵琴的缺點是[音量較弱](../Page/音量.md "wikilink") (約僅在 pp【極弱】∼ mp【中弱】之間)\[25\],
[音色不太明亮](../Page/音色.md "wikilink"), 厚度頗差,
[共鳴度低](../Page/共鳴.md "wikilink"), 表現力因此顯得單薄。而且小鍵琴琴鍵較少,
音域比現代鋼琴至少少兩個8度; 一般只有4個8度,
所以[音域顯得狹窄](../Page/音域.md "wikilink")、變化較少。同時,
小鍵琴本身受到[機械結構的限制](../Page/機械.md "wikilink"),
演奏快速和[節奏複雜的樂句會有一定程度上的困難](../Page/節奏.md "wikilink"),
所以它一般只適合在家中練習或在小型[演奏會上使用](../Page/演奏會.md "wikilink")。由於功能性不強,
音量小且不及大鍵琴\[26\],
小鍵琴後來在18世紀[大鍵琴盛行的年代逐漸被取代了](../Page/大鍵琴.md "wikilink")。

## 小鍵琴曲目

大約在[1400年至](../Page/1400年.md "wikilink")[1800年期間為](../Page/1800年.md "wikilink")[大鍵琴和](../Page/大鍵琴.md "wikilink")[管風琴寫的大部分音樂的曲目](../Page/管風琴.md "wikilink"),
其實都可以在小鍵琴上演奏。不過, 它音量不夠大，除了為柔和的巴洛克長笛或獨唱歌手提供伴奏之外; 難以參與室內樂演奏。
[約翰·塞巴斯蒂安·巴哈的兒子](../Page/約翰·塞巴斯蒂安·巴哈.md "wikilink")[卡爾·飛利浦·愛馬努埃爾·巴哈是該樂器的忠實支持者](../Page/卡爾·飛利浦·愛馬努埃爾·巴哈.md "wikilink"),
他與大多數[德國同時代人都將其視為中央鍵盤樂器](../Page/德國.md "wikilink"),
用於表演、教學、作曲和練習。在普魯士宮廷擔任樂師之前，[C.
P. E. 巴哈亦早已擁有擁有小鍵琴了](../Page/卡爾·飛利浦·愛馬努埃爾·巴哈.md "wikilink")。[C. P. E.
巴哈高度重視即興表演能力](../Page/卡爾·飛利浦·愛馬努埃爾·巴哈.md "wikilink")。這也給當時著名的音樂理論家查爾·斯伯尼博士(Charles
Burney 1726-1814),
於[1772年一次前往](../Page/1772年.md "wikilink")[漢堡訪問巴赫時](../Page/漢堡.md "wikilink")，見到巴赫對小鍵琴的精湛運用和掌握,
留下了深刻的印象。就在巴赫去世前幾年, 即[1781年](../Page/1781年.md "wikilink"),
他將他心愛的小鍵琴獻給了一位來自庫蘭 (Kurland)
的年輕貴族\[27\]。雖然有琴格的小鍵琴演奏某一些曲目比較困難, 但是一些學者卻認為,
在巴哈的[平均律鍵盤曲集演奏之中](../Page/平均律鍵盤曲集.md "wikilink"),
這種小鍵琴其實是可以克服這種困難的 (Loucks（1992))\[28\]。

在最近的小鍵琴灌錄之中，[克利斯朵夫·霍格伍德](../Page/克利斯朵夫·霍格伍德.md "wikilink")（巴哈的秘密\[29\],
韓德爾\[30\]的秘密和莫扎特的秘密）的錄音開闢了新的天地。霍格伍德指出，這些著名的作曲家，通常都會在家中秘密地演奏小鍵琴。在[英格蘭](../Page/英格蘭.md "wikilink")，作曲家赫伯特·豪威爾斯
(Herbert Howells
1892-1983）為小鍵琴（蘭伯特的小鍵琴和豪威爾斯的小鍵琴\[31\]）寫了兩個重要的作品集，而斯蒂芬·道奇森
(Stephen Dodgson 1924-2013）則為小鍵琴這種樂器，寫了兩個組曲\[32\]。

## 小鍵琴其他圖集

Image:N145765 - Klavikord - Daniel Stråhle - 1738 - foto Sofi
Sykfont.jpg|

<center>

一部來自[瑞典於](../Page/瑞典.md "wikilink")[1738年由Daniel](../Page/1738年.md "wikilink")
Stråhle製造的小鍵琴, 現存於[斯德哥爾摩音樂與劇院博物館內](../Page/斯德哥爾摩.md "wikilink")

</center>

Image:Clavicorde_Lépante.JPG|

<center>

一部來自於[義大利於](../Page/義大利.md "wikilink")[16世紀製造的小鍵琴](../Page/16世紀.md "wikilink")，現存於[巴黎音樂博物館內](../Page/巴黎.md "wikilink")

</center>

Image:Bundfreies Clavichord-Museum für Kunst und Gewerbe
Hamburg-1904.708.tif|

<center>

一部來自德國於[1760年製造的小鍵琴](../Page/1760年.md "wikilink"),
現存於[漢堡工藝美術博物館內](../Page/漢堡工藝美術館.md "wikilink")

</center>

Image:Clavichord_Hieronymus_Albrecht_Hass_1742.jpg|

<center>

一部來自德國於[1742年由Hieronymus](../Page/1742年.md "wikilink") Albrecht
Hass製造的小鍵琴, 現存於漢堡工藝美術博物館內

</center>

Image:Clavichord-JA Haas 007.jpg|

<center>

一部於[2005年製造的無琴格](../Page/2005年.md "wikilink")(unfretted),
具有5個8度音階的普通小鍵琴。(木材用料:[桃花心木](../Page/桃花心木.md "wikilink"),
[楊樹和](../Page/楊樹.md "wikilink")[樺木](../Page/樺木.md "wikilink"))

</center>

Image:Clavicord.jpg|

<center>

一部複製[1670年由以色列](../Page/1670年.md "wikilink")·蓋林格(Israel
Gellinger)製造的琴格(fretted)的小鍵琴

</center>

Image:122_Museu_de_la_Música.jpg|

<center>

顯示其中一部小鍵琴的琴弦構造, 該部小鍵琴現存於[巴塞羅那音樂博物館內](../Page/巴塞羅那.md "wikilink")

</center>

## 参见

  - [大鍵琴](../Page/大鍵琴.md "wikilink")
  - [古鋼琴](../Page/古鋼琴.md "wikilink")
  - 現代[鋼琴](../Page/鋼琴.md "wikilink")

## 參考文獻

  -
  -

  -
  -
  -
  -
  -
  -
## 外部鏈接

**小鍵琴協會：**

  - [英國小鍵琴協會](http://www.clavichord.org.uk/)

  - [波士頓小鍵琴協會](http://www.bostonclavichord.org/)

  - [德國小鍵琴協會](http://www.clavichord.info/index.html)

  - [瑞士小鍵琴協會](https://www.clavichordgesellschaft.ch/)

**小鍵琴製作**

  - [如何製作小鍵琴及其步驟](http://dustyfeet.com/mykeyboardbaby1.html)

**相關樂器珍藏**

  - [美國](../Page/美國.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[大都會藝術博物館所珍藏歷史悠久的小鍵琴](../Page/大都會藝術博物館.md "wikilink"):
      - [1763年 Christian Kintzing
        所製造的小鍵琴](https://www.metmuseum.org/art/collection/search/503271)
        來自[德國](../Page/德國.md "wikilink")[新維德市](../Page/新維德.md "wikilink")

      - [1765年 John Christopher Jesse
        所製造的小鍵琴](http://www.metmuseum.org/works_of_art/collection_database/musical_instruments/clavichord_john_christopher_jesse/objectview.aspx?page=1&sort=0&sortdir=asc&keyword=clavichord&fp=1&dd1=18&dd2=0&vw=1&collID=18&OID=180013539&vT=1)
        來自德國[哈伯斯塔特市](../Page/哈伯斯塔特.md "wikilink")

      - [18世紀 School of Johann Heinrich Gräbner
        所製造的小鍵琴](http://www.metmuseum.org/works_of_art/collection_database/musical_instruments/clavichord/objectview.aspx?page=1&sort=0&sortdir=asc&keyword=clavichord&fp=1&dd1=18&dd2=0&vw=1&collID=18&OID=180015702&vT=1)
        來自德國[德勒斯登市](../Page/德勒斯登.md "wikilink")
  - 位於[英國](../Page/英國.md "wikilink")[薩里郡](../Page/薩里郡.md "wikilink") The
    Cobbe Collection Trust in association with National Trust 所珍藏的的小鍵琴:
      - [非常罕有的1750-1775年西班牙或葡萄牙製造的小鍵琴](http://www.cobbecollection.co.uk/collection/spanish-or-portuguese-clavichord/)
        據說[多梅尼科·斯卡拉蒂尤其對該等樂器演奏會非常熟悉](../Page/多梅尼科·斯卡拉蒂.md "wikilink")

      - [1784年由 Christian Gotthelf
        Hoffmann](http://www.cobbecollection.co.uk/collection/11-clavichord/)
        在[德國的](../Page/德國.md "wikilink")[羅內堡市所製造的最具有撒克遜風格的小鍵琴](../Page/羅內堡.md "wikilink")

**其他小鍵琴相關介紹**

  - [小鍵琴的介紹 Bernard
    Brauchli](http://assets.cambridge.org/97805216/19899/excerpt/9780521619899_excerpt.pdf)，[劍橋大學出版社](../Page/劍橋大學.md "wikilink")

  - [有踏板的小鍵琴](http://www.fmil.org/instruments.php?instrument=clavecins-pedalier)，Yves
    Rechsteiner, 2001年

  - [小鍵琴的基本結構介紹](http://heinrich-schuetz-haus.de/instrumente_schuetzzeit/clavichord.php)，Heinrich
    Schütz

  - [小鍵琴國際](http://www.clavichordgenootschap.nl/) (Het Nederlands
    Clavichord Genootschap & Clavichord International)

**小鍵琴試聽**

  - [樂器:
    紐珀特小鍵琴](https://www.pianoteq.com/audio/kivir/Pachelbel%20-%20Chaconne%20in%20F%20Minor%20-%20Noa%20Leigh%20Kleisen.mp3)
    [恰空F小調](../Page/恰空.md "wikilink"), J. Pachelbel，作曲 Noa Leigh Kleisen
    演奏

  - [樂器:
    紐珀特小鍵琴](https://www.pianoteq.com/audio/kivir/Guy-Sigsworth_Italian-Concerto-Slow-Movement.mp3)
    [意大利慢動作](../Page/意大利.md "wikilink")[協奏曲](../Page/協奏曲.md "wikilink"),
    G. Sigsworth 作曲，Guy Sigsworth 演奏

  - [樂器:
    紐珀特小鍵琴](https://www.pianoteq.com/audio/kivir/Piet-DeRidder_Let-Them-Eat-Clavicake.mp3)
    Let Them Eat Clavicake, Piet De Ridder 作曲 ，Piet De Ridder 演奏

## 註釋

[Category:鍵盤樂器](../Category/鍵盤樂器.md "wikilink")
[Category:擊弦樂器](../Category/擊弦樂器.md "wikilink")
[Category:弦鳴樂器](../Category/弦鳴樂器.md "wikilink")

1.  小鍵琴是什麼

2.  莫扎特的小鍵琴返回維也納故居

3.

4.  Edwin M. Ripin; et al. *Clavichord* In Deane L. Root. Grove Music
    Online. Oxford Music Online. Oxford University Press

5.  單絃琴介紹

6.  多絃琴介紹

7.  Brinsmead, Edgar *History of the Pianoforte* London, 1879. pp. 90–91

8.  小鍵琴於明代中末期傳入

9.  塞萬提斯學院結束了文化年活動, 以紀念西班牙與中國的關係

10. 有琴格的小鍵琴

11. 無琴格的小鍵琴

12. Howard Schott *Early Music: The Clavichord Revival, 1800-1960* Vol.
    32, No. 4 (Nov., 2004), pp.595-603 Published by Oxford University
    Press

13. 小鍵琴製造商

14. Reviewed Works: *The Clavichord Collection, I: Bach: French Suites &
    Purcell by J. S. Bach*, Henry Purcell, Thurston Dart; *The
    Clavichord Collection, II: J. J. Froberger & Early English Pieces*
    by J. J. Froberger, William Croft. Review by: Howard Schott *Early
    Music* Vol. 27, No. 4, Luca Marenzio (1553/4-99) (Nov., 1999), pp.
    678-680 Published by Oxford University Press

15. 現代電子小鍵琴的發展

16.
17.
18. 弗里德里希·格里彭克爾（Friedrich Griepenkerl）於1844年討論到音樂家巴哈的風琴作品書 (Organ works
    of J.S. Bach); 該書第一卷的前言內提到巴哈使用踏板小鍵琴作為練習樂器; 參見

19. Speerstra, Joel (2004) *Bach and the Pedal Clavichord: an Organist's
    Guide* University of Rochester Press, ISBN 1-58046-135-2

20. Williams, Peter (2003) *The Organ Music of J.S. Bach* (2nd ed.),
    Cambridge University Press, pp. 4–6, ISBN 0-521-89115-9

21.
22. 琴弦排列成對

23.
24.
25. 在五線譜中常見的聲音強度的統一標示, 從弱至強依次序為:
    ppp【超弱】＜pp【極弱】＜p【弱】＜mp【中弱】＜mf【中強】＜f【強】＜ff【極強】＜fff【超強】

26.
27. 卡爾·飛利浦·愛馬努埃爾·巴哈與小鍵琴

28. Loucks, Richard (1992) *Performance Practice Review* Was the
    'Well-Tempered Clavier' performable on a fretted clavichord? 5/1
    pp.44–89

29. 有關光碟灌錄的曲目

30. 韓德爾與小鍵琴

31. 有關光碟灌錄的曲目

32. 有關斯蒂芬·道奇森曲目