**小霸尖山**為[台灣知名山峰](../Page/台灣.md "wikilink")，也是[台灣百岳之一](../Page/台灣百岳.md "wikilink")，排名第36。小霸尖山3,360公尺，屬於[雪山山脈](../Page/雪山山脈.md "wikilink")，[行政區劃屬於](../Page/台灣行政區劃.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")。小霸尖山東北東方有[大霸尖山](../Page/大霸尖山.md "wikilink")，北邊連接[伊澤山](../Page/伊澤山.md "wikilink")。其特色為山型模樣狀似猴子。

## 參考文獻

  - 楊建夫，《台灣的山脈》，2001年，臺北，遠足文化公司

## 相關條目

  - [雪霸國家公園](../Page/雪霸國家公園.md "wikilink")
  - [大霸尖山](../Page/大霸尖山.md "wikilink")
  - [聖稜線](../Page/聖稜線.md "wikilink")
  - [登山](../Page/登山.md "wikilink")

## 外部連結

  - [大霸/聖稜線段](https://web.archive.org/web/20100123021625/http://www1.forest.gov.tw/nt/TR_G_01.aspx)
    - 國家步道系統
  - [中華民國山岳協會](http://www.mountaineering.org.tw/) - 最初為「台灣山岳會」
  - [聖稜線](https://web.archive.org/web/20070926212836/http://csm01.csu.edu.tw/0150/49002134/index-12.htm)
    - 連峰照片
  - [雪山山脈的聖稜、南稜與西南稜線](https://web.archive.org/web/20070926212842/http://csm01.csu.edu.tw/0150/49002134/index-11-1.htm)
    - 連峰照片
  - [山情點滴─山與人的故事](https://web.archive.org/web/20080127144602/http://www.tces.chc.edu.tw/alan/taiwan/g/g1/g16.htm)

[Category:苗栗縣山峰](../Category/苗栗縣山峰.md "wikilink")
[Category:台灣百岳](../Category/台灣百岳.md "wikilink")
[Category:雪霸國家公園](../Category/雪霸國家公園.md "wikilink")