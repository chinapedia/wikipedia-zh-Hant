**小額貸款券**（[德文](../Page/德文.md "wikilink")：**Darlehnskassenschein**）是[德意志帝國時期貸幣](../Page/德意志帝國.md "wikilink")，由小額貸款處發行，用以解決因戰爭而缺乏金屬鑄造輔幣的問題。小額貸款券並非[法定貨幣](../Page/法定貨幣.md "wikilink")，但廣泛的在德意志帝國境內流通。

## 沿革

1914年，[歐洲](../Page/歐洲.md "wikilink")[大陸展開](../Page/大陸.md "wikilink")[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")。德意志帝國於[戰爭開始時](../Page/戰爭.md "wikilink")，[經濟亦進入戰時經濟體制](../Page/經濟.md "wikilink")，一切[金屬](../Page/金屬.md "wikilink")、[稀有金屬均被視為](../Page/稀有金屬.md "wikilink")，而另一方面國家得發行更多的[通貨](../Page/通貨.md "wikilink")(currency)去支付更多的軍事採購，國家[黃金與](../Page/黃金.md "wikilink")[外匯漸漸乾涸](../Page/外匯.md "wikilink")，在上述情況下，設立了[小額貸款處](../Page/小額貸款處.md "wikilink")(Darlehnskassen)。經過一段冗長的準備，設立法令在1914年八月四日通過，這個新機關以存放貨物與債券為質押而釋出貸款，這此貸款以類似鈔券的樣式被發行，也就是所謂的「小額貸款券」。
[5mrak.jpg](https://zh.wikipedia.org/wiki/File:5mrak.jpg "fig:5mrak.jpg")
這些標準化的票據有著對黃金的固定匯率，而且可以被任何一邦的[公務機關所接受](../Page/公務機關.md "wikilink")，那些需要借貸的人們一如使用實際鈔券一般使用小額貸款券。是此，雖然這票據不算是法定貨幣，他們仍然廣泛的流通而且自由的與法定貨幣[兌換](../Page/兌換.md "wikilink")，因為他們的[面額均不算大](../Page/面額.md "wikilink")。特別是面額1、2、5[馬克的小額貸款券](../Page/馬克.md "wikilink")，大量的被使用。他們取代了[硬幣](../Page/硬幣.md "wikilink")，而硬幣則是被帝國銀行自流通貨幣中取出，或是被人民與國家機關所收藏。自治地區或是國家機關從小額貸款處取得大量的貸款，做為質押性的則是與國家機關的[契約](../Page/契約.md "wikilink")：小額貸款處得以先行發行，而實際上這些新發行的鈔券則是由國家保證在未來的一個時間點上得以兌現。

### 1917年小額貸款券五馬克

  - 鈔券尺寸：12.5cm × 8cm。

<!-- end list -->

  - 正面字樣：Darlehnskassenschein, Fünf Mark, Berlin, den 1. August 1917,
    Reichsschuldenverwaltung.
    ([中文](../Page/中文.md "wikilink")：小額貸款券，五馬克，[柏林](../Page/柏林.md "wikilink")1917年八月一日，國家債務管理處。)下方有管理處主管的草簽。Wer
    Darlehnskassenschein nachmacht oder verfalscht oder nachgemachte
    oder verfalschte sich verschafft und in Verkehr bringt, wird mit
    Zuchthaus nicht unter zwei Jahren
    bestraft.(中文：不論是變造、[偽造](../Page/偽造.md "wikilink")，或是提供變造或協助流通者，將被處二年以上[有期徒刑](../Page/有期徒刑.md "wikilink")。)

<!-- end list -->

  - 背面字樣：Darlehnskassenschein, Fünf Mark,
    鋼印內文字：Reichsschuldenverwaltung, 5 (中文：小額貸款券，五馬克，國家債務管理處)

<!-- end list -->

  - 介紹：鈔券正面為淡紫色，左上方有[帝國](../Page/帝國.md "wikilink")[國徽](../Page/國徽.md "wikilink")，[山雕頭頂](../Page/山雕.md "wikilink")[皇冠](../Page/皇冠.md "wikilink")，身帶類似[嘉德勳章](../Page/嘉德勳章.md "wikilink")(Order
    of
    Garter)的徽飾。右方有少女半身像，頭帶麥繐，象徵[農業](../Page/農業.md "wikilink")。鈔券背面有繁複的底紋裝飾，中央有[橡樹葉襯托的皇冠](../Page/橡樹.md "wikilink")，左右方各有國家債物管理處的鋼章，左邊字體陽刻，右邊字體陰刻。

<!-- end list -->

  - 防偽方式：鈔券本身佈有流雲狀[浮水印](../Page/浮水印.md "wikilink")，背面有繁複網狀底紋，中央有紅色防偽[纖維通過](../Page/纖維.md "wikilink")。

## 退場

最初，發行貸款的總數被小額代款處限制在十五億馬克，但是這個限制早在1916年時已經超過，到了1918年為止，成千上萬在市面上流通的小額貸款券總數達到101億馬克。當在一次世界大戰之中，大量的[熱錢在流通而且穩定的成長](../Page/熱錢.md "wikilink")，而戰後對於生活物資的需求度下降，換句話說，就是到了戰時經濟體制與戰後[經濟危機該出現的時候了](../Page/經濟危機.md "wikilink")。所以鈔券的價值正快速的貶值，到了1923年5月，所有面額的小額貸款券全失去其價值，而正式的官方作廢則是在1924年的4月30日。

[Category:紙幣](../Category/紙幣.md "wikilink")
[Category:德意志帝国](../Category/德意志帝国.md "wikilink")
[Category:已废止的德国货币](../Category/已废止的德国货币.md "wikilink")