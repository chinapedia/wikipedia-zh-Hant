**小駝獸**（屬名：*Oligokyphus*）是種先進[草食性](../Page/草食性.md "wikilink")[犬齒獸類](../Page/犬齒獸類.md "wikilink")，生存於晚[三疊紀到晚](../Page/三疊紀.md "wikilink")[侏儸紀](../Page/侏儸紀.md "wikilink")。小駝獸起初被認為是早期[哺乳類](../Page/哺乳類.md "wikilink")，現在被分類於[獸孔目](../Page/獸孔目.md "wikilink")，因為小駝獸仍保有退化的[方骨](../Page/方骨.md "wikilink")-[方軛骨關節](../Page/方軛骨.md "wikilink")，不是哺乳類的下頜附著方式。

[category:真犬齒獸下目](../Page/category:真犬齒獸下目.md "wikilink")

[Category:三疊紀合弓類](../Category/三疊紀合弓類.md "wikilink")
[Category:侏羅紀合弓類](../Category/侏羅紀合弓類.md "wikilink")