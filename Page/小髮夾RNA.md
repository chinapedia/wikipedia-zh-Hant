**小髮夾RNA**（，[缩写](../Page/缩写.md "wikilink")）是一種形成[急轉彎](../Page/急轉彎.md "wikilink")（hairpin
turn）結構的[RNA序列](../Page/RNA.md "wikilink")，可以經由[RNA干擾](../Page/RNA干擾.md "wikilink")（RNAi）使[基因表現](../Page/基因表現.md "wikilink")[沉默化](../Page/基因沉默.md "wikilink")。shRNA可利用[載體導入細胞當中](../Page/載體.md "wikilink")，並藉由U6[啟動子來確保shRNA的表現](../Page/啟動子.md "wikilink")。另外，shRNA可經由切割轉變成為[siRNA](../Page/siRNA.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
## 参见

  - [RNA干扰](../Page/RNA干扰.md "wikilink")
  - [小干擾RNA](../Page/小干擾RNA.md "wikilink")

{{-}}

[Category:RNA](../Category/RNA.md "wikilink")
[Category:RNA干扰](../Category/RNA干扰.md "wikilink")