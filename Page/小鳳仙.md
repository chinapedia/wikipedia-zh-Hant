**小鳳仙**（），原名**朱筱鳳**，後改名為**張鳳雲**、**張洗非**。原籍[浙江](../Page/浙江.md "wikilink")[錢塘](../Page/錢塘.md "wikilink")，[中華民國大陸時期名妓](../Page/中華民國大陸時期.md "wikilink")。

## 生平

[小凤仙.jpg](https://zh.wikipedia.org/wiki/File:小凤仙.jpg "fig:小凤仙.jpg")
小鳳仙为[偏房所生](../Page/偏房.md "wikilink")，[光緒年間](../Page/光緒.md "wikilink")，舉家流寓[湖南](../Page/湖南.md "wikilink")[湘潭](../Page/湘潭.md "wikilink")，[父親因經商傾家蕩產](../Page/父親.md "wikilink")。小鳳仙被賣到[妓院](../Page/妓院.md "wikilink")，輾轉至[北京](../Page/北京.md "wikilink")[八大胡同](../Page/八大胡同.md "wikilink")「陝西巷雲吉班」，“流寓袁啞，墮入妓籍，隸屬陝西巷雲和班，相貌乏過中姿”“粗通翰墨，喜綴歌詞，又生成一雙[慧眼](../Page/慧眼.md "wikilink")，能辨別[狎客才華](../Page/嫖客.md "wikilink")”\[1\]。

小凤仙与[蔡鍔之间流传着许多不实的传言](../Page/蔡鍔.md "wikilink")。如：“在[袁世凱稱帝前夕](../Page/袁世凱.md "wikilink")，小凤仙助蔡鍔逃離[北平](../Page/北平.md "wikilink")”，实际协助者为蔡锷侧室潘蕙英。小凤仙本人既不知情，也未参与蔡锷逃走之事；1916年11月8日，蔡锷病逝於[日本](../Page/日本.md "wikilink")[福冈大学医院后](../Page/福冈大学.md "wikilink")，各种报刊随即刊登了一些托名小凤仙的挽联、悼文，实际上都是各地好事者所撰写，与小凤仙本人并无关系；此外，在各种文艺作品中，蔡锷和小凤仙被虚构并演绎成一段爱情故事的主角\[2\]。

晚年活動不詳，傳聞先是嫁給[東北軍的一個](../Page/東北軍.md "wikilink")[師長](../Page/師長.md "wikilink")，移居[瀋陽](../Page/瀋陽.md "wikilink")\[3\]，後化名張洗非，委身於鍋爐工人李振海，又一說嫁給陳姓廚師\[4\]，又說她得[梅蘭芳之助](../Page/梅蘭芳.md "wikilink")，到東北人民政府機關學校當保健員\[5\]。

## 影视形象

  - [胡蝶](../Page/胡蝶.md "wikilink")：1932年電影《[自由之花](../Page/自由之花.md "wikilink")》
  - [李麗華](../Page/李麗華.md "wikilink")：1953年電影《小鳳仙》
  - [殷巧兒](../Page/殷巧兒.md "wikilink")：1973年[香港](../Page/香港.md "wikilink")[無綫電視劇](../Page/無綫.md "wikilink")《[小鳳仙](../Page/小鳳仙_\(電視劇\).md "wikilink")》
  - [李璇](../Page/李璇.md "wikilink")：1974年[臺灣](../Page/臺灣.md "wikilink")[華視電視劇](../Page/華視.md "wikilink")《[小鳳仙與蔡松坡](../Page/小鳳仙與蔡松坡.md "wikilink")》
  - [何莉莉](../Page/何莉莉.md "wikilink")：1974年[邵氏電影](../Page/邵氏電影.md "wikilink")《[五大漢](../Page/五大漢.md "wikilink")》（[鮑學禮導演執導](../Page/鮑學禮.md "wikilink")）
  - [狄波拉](../Page/狄波拉.md "wikilink")：1976年[香港單元電視劇](../Page/香港.md "wikilink")《[近代豪俠傳](../Page/近代豪俠傳.md "wikilink")》之《小鳳仙》
  - [許秀年](../Page/許秀年.md "wikilink")：1981年[臺灣單元歌仔戲](../Page/臺灣.md "wikilink")《[傳奇故事](../Page/傳奇故事.md "wikilink")》之《[蔡松坡与小鳳仙](../Page/蔡松坡与小鳳仙.md "wikilink")》
  - [張瑜](../Page/張瑜_\(演員\).md "wikilink")：1980年代初，[中國大陸電影](../Page/中國大陸.md "wikilink")《[知音](../Page/知音.md "wikilink")》
  - [邱于庭](../Page/邱于庭.md "wikilink")：1985年[台湾](../Page/台湾.md "wikilink")[中視電視劇](../Page/中視.md "wikilink")《[大將軍與小鳳仙](../Page/大將軍與小鳳仙.md "wikilink")》
  - [梁珮玲](../Page/梁珮玲.md "wikilink")：1990年[香港](../Page/香港.md "wikilink")[無綫電視劇](../Page/無綫.md "wikilink")《[螳螂小子](../Page/螳螂小子.md "wikilink")》
  - [劉曉慶](../Page/劉曉慶.md "wikilink")：1998年[中國大陸電視劇](../Page/中國大陸.md "wikilink")《[逃之戀](../Page/逃之戀.md "wikilink")》
  - [蔣勤勤](../Page/蔣勤勤.md "wikilink")、[舒暢](../Page/舒暢.md "wikilink")：1998年[中國大陸電視劇](../Page/中國大陸.md "wikilink")《[小鳳仙的故事](../Page/小鳳仙的故事.md "wikilink")》
  - [周海媚](../Page/周海媚.md "wikilink")：2009年[香港](../Page/香港.md "wikilink")[無綫電視劇](../Page/電視廣播有限公司.md "wikilink")《[蔡鍔與小鳳仙](../Page/蔡鍔與小鳳仙.md "wikilink")》
  - [AngelaBaby](../Page/AngelaBaby.md "wikilink")：2011年[中國電影](../Page/中國.md "wikilink")《[建黨偉業](../Page/建黨偉業.md "wikilink")》
  - [王力可](../Page/王力可.md "wikilink")：2011年中国大陆电视剧《[护国军魂传奇](../Page/护国军魂传奇.md "wikilink")》
  - [姚笛](../Page/姚笛_\(演員\).md "wikilink")：2011年中国大陆电视剧《[护国大将军](../Page/护国大将军_\(电视剧\).md "wikilink")》
  - [孙格](../Page/孙格.md "wikilink")：2011年中国大陆电视剧《[辛亥革命](../Page/辛亥革命_\(電視劇\).md "wikilink")》

## 注釋

<div class="references-small">

<references />

</div>

## 參考文獻

  - 《中國歷代名女人之謎》

[X](../Category/護國戰爭人物.md "wikilink") [X](../Category/杭州人.md "wikilink")
[X](../Category/中國傳統娼妓女樂.md "wikilink")
[\~](../Category/朱姓.md "wikilink")
[\~](../Category/張姓.md "wikilink")

1.  《民初史略》
2.  [蔡锷逸事](../Page/:蔡锷#.E9.80.B8.E4.BA.8B.md "wikilink")
3.
4.  [民国名妓小凤仙历尽沧桑
    解放前夕嫁给一锅炉工](http://history.qihoo.com/article/q6719994,2daa49,s5172_34730.html)
5.