**小鼻綠鸚嘴魚**，又名**鈍頭鸚哥魚**，俗名鸚哥，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[鸚哥魚科的其中一](../Page/鸚哥魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[太平洋區](../Page/太平洋.md "wikilink")，包括[台灣](../Page/台灣.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[中國](../Page/中國.md "wikilink")[南海](../Page/南海.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[羅得豪島](../Page/羅得豪島.md "wikilink")、[拉帕島](../Page/拉帕島.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[夏威夷群島等海域](../Page/夏威夷群島.md "wikilink")。

## 深度

水深2至35公尺。

## 特徵

本魚體延長而略側扁。幼魚體黑具數條較寬的[白色平行縱紋](../Page/白色.md "wikilink")，尾鰭[黑色不透明](../Page/黑色.md "wikilink")，截型尾鰭。雄魚額部突出，使吻部呈陡直狀；雌魚則略隆起而使頭背部幾成直線。約20公分的魚體，體暗灰帶[棕](../Page/棕色.md "wikilink")[綠色](../Page/綠色.md "wikilink")，且魚體越大[藍綠色調越明顯](../Page/藍綠色.md "wikilink")。較大魚體的雄魚魚嘴具鮮豔的綠色斑紋。背鰭硬棘9枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條9枚。體長可達55公分。

## 生態

幼魚通常單獨生活於較淺的珊瑚礁區。成魚則通常成群的洄游於珊瑚礁區離岸較遠的一端。

## 經濟利用

食用魚類，適合[清蒸](../Page/清蒸.md "wikilink")、[紅燒或用溫火燒烤食用之](../Page/紅燒.md "wikilink")。曾有熱帶魚毒的中毒報告，故須注意。另外可作為[觀賞魚](../Page/觀賞魚.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[microrhinos](../Category/綠鸚嘴魚屬.md "wikilink")