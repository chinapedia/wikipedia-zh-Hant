《**少年小樹之歌**》（*The Education of Little
Tree*），是[艾薩·卡特化名佛瑞斯特](../Page/艾薩·卡特.md "wikilink")·卡特（Forrest
Carter）所創作的一部[回憶錄式](../Page/回憶錄.md "wikilink")[小說](../Page/小說.md "wikilink")。\[1\]該作品描繪了[主角幼時與](../Page/主角.md "wikilink")[祖父母在](../Page/祖父母.md "wikilink")[1930年代前後於](../Page/1930年代.md "wikilink")[美國東部的](../Page/美國東部.md "wikilink")[生活故事](../Page/生活.md "wikilink")，並藉之深刻闡述[人類與](../Page/人類.md "wikilink")[大自然之間的互動關係](../Page/大自然.md "wikilink")。\[2\]\[3\]\[4\]\[5\]

該作品曾在1997年被改編翻拍為[電影](../Page/電影.md "wikilink")。\[6\]

## 參考

[Category:美國小說](../Category/美國小說.md "wikilink")
[Category:1976年美國小說](../Category/1976年美國小說.md "wikilink")

1.
2.
3.
4.
5.
6.