《**少年英雄方世玉**》（[英文](../Page/英文.md "wikilink")：**Young Hero Fong Sai
Yuk**）是1999年[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台與](../Page/本港台.md "wikilink")[三立都會台外購劇](../Page/三立都會台.md "wikilink")，[飛騰電影公司製作](../Page/飛騰電影公司.md "wikilink")，[張衛健](../Page/張衛健.md "wikilink")、[恬妞](../Page/恬妞.md "wikilink")、[何美鈿](../Page/何美鈿.md "wikilink")、[樊少皇](../Page/樊少皇.md "wikilink")、[李婷宜等主演](../Page/李婷宜.md "wikilink")。是描述[方世玉從整天闖禍的少年](../Page/方世玉.md "wikilink")，和摯友[洪熙官](../Page/洪熙官.md "wikilink")、-{[胡惠乾](../Page/胡惠乾.md "wikilink")}-及摯愛[凌小小](../Page/凌小小.md "wikilink")，如何大敗[雷老虎](../Page/雷老虎.md "wikilink")、智取[李巴山](../Page/李巴山.md "wikilink")、最後打敗武林魔頭[白眉道長拯救武林](../Page/白眉道人.md "wikilink")。

该剧当年一经播出，在两岸三地刮起收视狂潮，更在香港亚视播出后，收视击败无线，且超过之前《还珠格格》在港创下的16%左右的收视结果，创亚视史上最高收视纪录。

## 故事簡介

一名整天闖禍的少年方世玉，誤傷雷老虎之姪子[雷人王](../Page/雷人王.md "wikilink")。雷老虎強迫方世玉和其決鬥報仇，沒想到方世玉打敗雷老虎。雷老虎因此傷重不治，其妻李小環找其父李巴山出面復仇。方世玉裝死避往[少林](../Page/少林.md "wikilink")，勤練武功，最後被發現。李巴山殺上少林，和方世玉在梅花樁上決鬥。決鬥時，李小環暗算方世玉不成，還因此間接害死父親，不能報仇，只能抱着屍首離去。

然後，李小環去武當山找師叔幫忙報仇。後來武當掌門[白眉道長卻想藉此機會](../Page/白眉道長.md "wikilink")，找藉口一舉征服少林，想要讓武當從此成為武林第一大派。白眉道長殺上少林，死傷慘重。方世玉等人逃出少林。後來因緣際會，[嚴詠春自創](../Page/嚴詠春.md "wikilink")[詠春拳](../Page/詠春拳.md "wikilink")，[洪熙官也自創](../Page/洪熙官.md "wikilink")[洪拳](../Page/洪拳.md "wikilink")，方世玉更習得了絕世武功，最後大敗白眉，拯救武林，也完成了傳說。

## 演員表

### 主要演員

  - [張衛健](../Page/張衛健.md "wikilink") 飾 [方世玉](../Page/方世玉.md "wikilink")
  - [何美鈿](../Page/何美鈿.md "wikilink") 飾 [凌小小](../Page/凌小小.md "wikilink")
  - [樊少皇](../Page/樊少皇.md "wikilink") 飾 [洪熙官](../Page/洪熙官.md "wikilink")
  - [李婷宜](../Page/李婷宜.md "wikilink") 飾
    [嚴詠春](../Page/嚴詠春.md "wikilink")（香港配音
    [曾秀清](../Page/曾秀清.md "wikilink")）
  - [恬　妞](../Page/恬妞.md "wikilink") 飾 [苗翠花](../Page/苗翠花.md "wikilink")
  - [張振寰](../Page/張振寰.md "wikilink") 飾 [方　德](../Page/方德.md "wikilink")
  - [黃一飛](../Page/黃一飛.md "wikilink") 飾 [苗　顯](../Page/苗顯.md "wikilink")
  - [鄭國霖](../Page/鄭國霖.md "wikilink") 飾
    -{[胡惠乾](../Page/胡惠乾.md "wikilink")}-（香港配音
    [潘文柏](../Page/潘文柏.md "wikilink")）
  - [謝　娜](../Page/谢娜.md "wikilink") 飾 小　麗（香港配音
    [黃玉娟](../Page/黃玉娟.md "wikilink")）

### 次要演員

  - [鄭佩佩](../Page/鄭佩佩.md "wikilink") 飾 [五　梅](../Page/五枚.md "wikilink")
  - [沈孟生](../Page/沈孟生.md "wikilink") 飾
    [三　德](../Page/三德.md "wikilink")（香港配音
    [黃志成](../Page/黃志成.md "wikilink")）
  - [冼灝英](../Page/冼灝英.md "wikilink") 飾 至　能
  - [于立文](../Page/于立文.md "wikilink") 飾
    [至　善](../Page/至善.md "wikilink")（香港配音
    [黄志明](../Page/黃志明_\(香港\).md "wikilink")）
  - [王衛國](../Page/王衛國.md "wikilink") 飾
    [嚴　湛](../Page/嚴湛.md "wikilink")（香港配音
    [盧　雄](../Page/盧雄.md "wikilink")）
  - [劉大成](../Page/劉大成.md "wikilink") 飾 關夫子
  - [盧星宇](../Page/盧星宇.md "wikilink") 飾
    [童千斤](../Page/童千斤.md "wikilink")（香港配音
    [雷　霆](../Page/雷霆.md "wikilink")）
  - [卓　凡](../Page/卓凡.md "wikilink") 飾 [雷人王](../Page/雷人王.md "wikilink")
  - [趙　劍](../Page/趙劍.md "wikilink") 飾
    [雷老虎](../Page/雷老虎.md "wikilink")（香港配音
    [黄志明](../Page/黃志明_\(香港\).md "wikilink")）
  - [商天娥](../Page/商天娥.md "wikilink") 飾 [李小環](../Page/李小環.md "wikilink")
  - [李海興](../Page/李海興.md "wikilink") 飾
    [李巴山](../Page/李巴山.md "wikilink")（香港配音
    [陆颂愚](../Page/陆颂愚.md "wikilink")）
  - [黃海冰](../Page/黃海冰.md "wikilink") 飾 仇萬千
  - [陳鴻烈](../Page/陳鴻烈.md "wikilink") 飾
    [白眉道長](../Page/白眉道人.md "wikilink")
  - [張春仲](../Page/張春仲.md "wikilink") 飾 [冯道德](../Page/冯道德.md "wikilink")
  - 飾 [肥褔](../Page/肥褔.md "wikilink")（香港配音
    [曾志偉](../Page/曾志偉.md "wikilink")）

## 原声專輯

原声CD由[环星唱片于](../Page/环星唱片.md "wikilink")1999年发行，作曲与编曲为香港音乐人[张兆鸿](../Page/张兆鸿.md "wikilink")。

1.  醉凡塵 (粵語) - [葉振棠](../Page/葉振棠.md "wikilink")
2.  心的守候 -
    [樊少皇](../Page/樊少皇.md "wikilink")、[李婷宜](../Page/李婷宜.md "wikilink")
3.  媽媽我愛你 -
    [麦子杰](../Page/麦子杰.md "wikilink")、[黎亚](../Page/黎亚.md "wikilink")
4.  今夜我想醉 - [黎骏](../Page/黎骏.md "wikilink")
5.  少年夢 - [麦子杰](../Page/麦子杰.md "wikilink")
6.  醉凡塵(電視版) - [葉振棠](../Page/葉振棠.md "wikilink")
7.  醉凡塵(音樂版)
8.  心的守候(音樂版)
9.  媽媽我愛你(音樂版)
10. 今夜我想醉(音樂版)

## 参考资料

<references/>

[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[Category:1999年亞視電視劇集](../Category/1999年亞視電視劇集.md "wikilink")
[S少](../Category/1999年台灣電視劇集.md "wikilink")
[S少](../Category/三立都會台戲劇節目.md "wikilink")
[S少](../Category/武俠劇.md "wikilink")
[S少](../Category/康熙時期背景電視劇.md "wikilink")
[S少](../Category/廣東省背景電視劇.md "wikilink")