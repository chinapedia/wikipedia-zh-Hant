《**少年陰陽師**》是[結城光流著](../Page/結城光流.md "wikilink")、[淺木櫻插畫](../Page/淺木櫻.md "wikilink")，由[角川書店出版的](../Page/角川書店.md "wikilink")[奇幻系列小說](../Page/奇幻小说.md "wikilink")（[輕小說](../Page/輕小說.md "wikilink")）。

## 故事簡介

故事時代背景是[平安时代](../Page/平安时代.md "wikilink")。故事主角是曠世陰陽師[安倍晴明最小的孫子安倍昌浩](../Page/安倍晴明.md "wikilink")，與一隻搭檔修行，最終成為一代優秀陰陽師的故事。

## 登場角色

## 用語

  - [平安京](../Page/平安京.md "wikilink")
    原本的都城長岡京於公元七八四年設在長岡京市（現在的京都府向日市）附近，但是因為陸續發生不祥的怪事，於是[桓武天皇在公元七九四年遷都到平安京](../Page/桓武天皇.md "wikilink")。
  - 元服禮
    這是日本男子的成年儀式，是根據中國古代的弱冠禮而來。舉行元服禮的時間大約在十一歲起至二十歲之間，不過愈早完成對未來愈有利，所以昌浩十三歲才進行算晚了。
  - 陰陽寮
    [陰陽寮是日本古代掌管占卜](../Page/陰陽寮.md "wikilink")、天文、時刻、曆法的機構，並且負責朝廷的祭祀典儀，可以說是古代的科學、天文研究中心。陰陽寮內設有陰陽師、陰陽博士、陰陽生等不同的職位，像昌浩就是還在見習的菜鳥陰陽生。總之一般人都把他們叫作陰陽師。
  - 內覽
    就是可以預先閱讀上奏給天皇的奏摺的官員。
  - 對屋
    日本古代貴族的房子有一二種主要結構：「寢殿」是房子中最主要的建築，「對屋」是供眷屬居住的部分，「渡殿」則是連接寢殿與對屋的走廊。
  - 右大辨
    管理兵部、刑部、大藏、宮內這四個部門的官職
  - 藏人頭
    專門負責管理皇室文書與文具的官職。
  - 安倍家的結界
    安倍家位於京都的鬼門的位置，且為了保護與京都龍脈相連的龍穴的責任，因此設有強力保護的結界。在昌浩三歲時遭到妖異(其實是風音的法術)的襲擊，因此晴明將結界擴大至安倍家全境。要進入結界必須有安倍家的人的同意，否則妖怪只要碰觸就會消滅。
  -
    十二神將是不能傷害人類，也不能殺害人類的。他們作為神的同時又隸屬於人類、被人類的角色束縛著，那是因為人類創造了十二神將，對於神將們來說人類就像他們的父母。如果他們殺害人類的話就等於殺害自己的父母，對於神將的內心也會造成無比的傷害。
  -
    十二神將雖然身為神族，比人類強壯，但也並非不死的永生存在。一旦死亡，之前所有的記憶與形態將會消失，但是因為身為神族，十二神將就算死了也能馬上復活，帶著全新的靈魂重生，但外表、性情都會與之前的完全不同(如同人類的投胎轉世)。
  -
    軻遇突智是日本神話中的火神。在其出生時就燒死了自己的母親[伊邪那美](../Page/伊奘冉尊.md "wikilink")（邪念耶），而生氣的父親[伊邪那歧命將其以](../Page/伊奘諾尊.md "wikilink")[十拳劍斬成三段](../Page/十握劍.md "wikilink")，其中之一就形成了高龗神，因此高龗神才會擁有弒神之炎。而在古書紀中，軻遇突智又稱火之迦具土神、火之夜芸速男神、火之炫毘古神；而在[日本書紀中又名火產靈神](../Page/日本書紀.md "wikilink")，是相當有名的火神。
  - 火焰之刃
    朱雀的大刀和弒神之炎混合而成的劍，昌浩就是利用此劍殺死被黃泉屍鬼附身的紅蓮（騰蛇）。
  - 天珠
    天狐的生命，是死後天狐的心臟，擁有強大的力量，能夠化解天狐及其他異形（例如窮奇）的詛咒或是延續生命。
  - 魑魅
    在都城裡出現的烏鴉與狼型的幻妖，類似陰陽師的式，往沒有生命的東西注入法術，使其像生物一樣動起來。比如把普通的紙片做成鳥兒的形狀飛出去，或者把剪成人形的紙幻化成人等等。是被稱作魑魅的使役。魑魅沒有意識。它們只會聽從創造自己的主人的命令，只能使用主人注入的力量。
  - 八咫鏡
    三神器之一，[天照大神所使用的鏡子](../Page/天照大神.md "wikilink")，目前真品供奉在伊勢齋宮，而仿造品則放在皇宮裡的溫明殿聖地供奉。
  - 夢殿
    據說是在夢中的「另一個世界」存在著神明與亡者。玉依篇中在此替昌浩治療心之傷的岦齋稱此為「通往陰陽道的幽暗世界（）。乃是夢中的另一個世界。是神與死者居住的地方」。

## 小說

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>日本發行日期</p></th>
<th><p>日文標題</p></th>
<th><p>台灣發行日期</p></th>
<th><p>中文標題（括號內為另一譯名）</p></th>
<th><p>篇名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一卷</p></td>
<td><p>2001年12月26日</p></td>
<td></td>
<td><p>2006年7月31日</p></td>
<td><p>異邦的妖影（探尋異邦的妖影）</p></td>
<td><p>窮奇篇</p></td>
</tr>
<tr class="even">
<td><p>第二卷</p></td>
<td><p>2002年4月27日</p></td>
<td></td>
<td><p>2006年12月11日</p></td>
<td><p>黑暗的咒縛（粉碎黑暗的呪縛）[1]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三卷</p></td>
<td><p>2002年7月31日</p></td>
<td></td>
<td><p>2007年4月23日</p></td>
<td><p>鏡子的牢籠（打破鏡之牢籠）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四卷</p></td>
<td><p>2002年10月31日</p></td>
<td></td>
<td><p>2007年8月6日</p></td>
<td><p>災禍之鎖（解放災禍的鎖鏈）</p></td>
<td><p>風音篇</p></td>
</tr>
<tr class="odd">
<td><p>第五卷</p></td>
<td><p>2003年1月30日</p></td>
<td></td>
<td><p>2007年11月5日</p></td>
<td><p>雪花之夢（沉眠於雪花中）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第六卷</p></td>
<td><p>2003年4月25日</p></td>
<td></td>
<td><p>2008年2月1日</p></td>
<td><p>黃泉之風（追逐黃泉的異風）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第七卷</p></td>
<td><p>2003年8月1日</p></td>
<td></td>
<td><p>2008年5月12日</p></td>
<td><p>火焰之刃（琢磨清澈的火焰之刃）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第八卷</p></td>
<td><p>2003年9月30日</p></td>
<td></td>
<td><p>2008年7月8日</p></td>
<td><p>夢的鎮魂歌（平靜的鎮魂歌）</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="odd">
<td><p>第九卷</p></td>
<td><p>2004年1月30日</p></td>
<td></td>
<td><p>2008年9月1日</p></td>
<td><p>真紅之空（翱翔於緋紅的天空）</p></td>
<td><p>天狐篇</p></td>
</tr>
<tr class="even">
<td><p>第十卷</p></td>
<td><p>2004年5月29日</p></td>
<td></td>
<td><p>2008年11月3日</p></td>
<td><p>光之導引（指示光之方向）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第十一卷</p></td>
<td><p>2004年9月30日</p></td>
<td></td>
<td><p>2009年1月5日</p></td>
<td><p>冥夜之帳（劃破黑夜的帳幕）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第十二卷</p></td>
<td><p>2004年12月28日</p></td>
<td></td>
<td><p>2009年3月9日</p></td>
<td><p>羅剎之腕（掙脫羅刹之手）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第十三卷</p></td>
<td><p>2005年6月30日</p></td>
<td></td>
<td><p>2009年5月12日</p></td>
<td><p>虛無之命（扭轉無常的命運）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第十四卷</p></td>
<td><p>2005年9月30日</p></td>
<td></td>
<td><p>2009年7月13日</p></td>
<td><p>竹姬綺緣（仿若嫩竹之輝夜姬）</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="odd">
<td><p>第十五卷</p></td>
<td><p>2006年1月1日</p></td>
<td></td>
<td><p>2009年9月15日</p></td>
<td><p>蒼古之魂（喚醒遠古的靈魂）</p></td>
<td><p>珂神篇</p></td>
</tr>
<tr class="even">
<td><p>第十六卷</p></td>
<td><p>2006年7月1日</p></td>
<td></td>
<td><p>2009年11月16日</p></td>
<td><p>玄妙之絆（緊握靈妙之牽絆）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第十七卷</p></td>
<td><p>2006年10月1日</p></td>
<td></td>
<td><p>2009年12月28日</p></td>
<td><p>真相之聲（傾聽告知真實之聲）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第十八卷</p></td>
<td><p>2007年2月1日</p></td>
<td></td>
<td><p>2010年3月1日</p></td>
<td><p>嘆息之雨（橫掃歎息之陰雨）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第十九卷</p></td>
<td><p>2007年5月1日</p></td>
<td></td>
<td><p>2010年5月31日</p></td>
<td><p>歸天之翼（重返天空之翼）</p></td>
<td><p>外傳</p></td>
</tr>
<tr class="even">
<td><p>第二十卷</p></td>
<td><p>2007年7月1日</p></td>
<td></td>
<td><p>2010年7月5日</p></td>
<td><p>無盡之誓（銘記無法實現之誓言）</p></td>
<td><p>珂神篇</p></td>
</tr>
<tr class="odd">
<td><p>第二十一卷</p></td>
<td><p>2007年10月1日</p></td>
<td></td>
<td><p>2010年9月13日</p></td>
<td><p>幽幽玄情（綿綿愁緒無處訴）</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="even">
<td><p>第二十二卷</p></td>
<td><p>2008年2月1日</p></td>
<td></td>
<td><p>2010年11月8日</p></td>
<td><p>無懼之心（拋開一切恐懼）</p></td>
<td><p>玉依篇</p></td>
</tr>
<tr class="odd">
<td><p>第二十三卷</p></td>
<td><p>2008年6月1日</p></td>
<td></td>
<td><p>2011年1月3日</p></td>
<td><p>憂愁之波（彷徨與憂愁的波瀾）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第二十四卷</p></td>
<td><p>2008年8月1日</p></td>
<td></td>
<td><p>2011年3月7日</p></td>
<td><p>寂靜之瞬（沉眠於剎那的寂靜）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第二十五卷</p></td>
<td><p>2008年10月1日</p></td>
<td></td>
<td><p>2011年7月11日</p></td>
<td><p>失迷之途（尋找迷途的彼端）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第二十六卷</p></td>
<td><p>2009年2月1日</p></td>
<td></td>
<td><p>2011年11月7日</p></td>
<td><p>彼方之敵（眺望彼方之時）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第二十七卷</p></td>
<td><p>2009年6月1日</p></td>
<td></td>
<td><p>2012年3月5日</p></td>
<td><p>狂風之劍（吹下暴風之劍）</p></td>
<td><p>颯峰篇</p></td>
</tr>
<tr class="even">
<td><p>第二十八卷</p></td>
<td><p>2009年10月1日</p></td>
<td></td>
<td><p>2012年7月9日</p></td>
<td><p>真心之願（結起所有祈禱之線）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第二十九卷</p></td>
<td><p>2010年1月1日</p></td>
<td></td>
<td><p>2012年11月12日</p></td>
<td><p>消散之印（消去斑駁之印）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第三十卷</p></td>
<td><p>2010年4月1日</p></td>
<td></td>
<td><p>2013年1月14日</p></td>
<td><p>玄天之渦（衝擊千尺之渦）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三十一卷</p></td>
<td><p>2010年7月1日</p></td>
<td></td>
<td><p>2013年5月6日</p></td>
<td><p>神威之舞（跳躍的笛之音）</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="even">
<td><p>第三十二卷</p></td>
<td><p>2010年9月1日</p></td>
<td></td>
<td><p>2013年7月15日</p></td>
<td><p>夕暮之花（傍晚散落之花）</p></td>
<td><p>竹籠眼篇</p></td>
</tr>
<tr class="odd">
<td><p>第三十三卷</p></td>
<td><p>2011年2月1日</p></td>
<td></td>
<td><p>2013年9月9日</p></td>
<td><p>微光潛行（依稀可見之光）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第三十四卷</p></td>
<td><p>2011年6月1日</p></td>
<td></td>
<td><p>2013年11月18日</p></td>
<td><p>破暗之明（黑暗中的短暫光明）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三十五卷</p></td>
<td><p>2011年10月1日</p></td>
<td></td>
<td><p>2013年12月30日</p></td>
<td><p>心願之證（心願成真的證明）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第三十六卷</p></td>
<td><p>2012年2月1日</p></td>
<td></td>
<td><p>2014年3月10日</p></td>
<td><p>朝雪之約（在降雪早晨的約定）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三十七卷</p></td>
<td><p>2012年7月1日</p></td>
<td></td>
<td><p>2014年7月14日</p></td>
<td><p>落櫻之禱（凋謝櫻花的希望）</p></td>
<td><p>尸櫻篇</p></td>
</tr>
<tr class="even">
<td><p>第三十八卷</p></td>
<td><p>2012年10月1日</p></td>
<td></td>
<td><p>2014年9月9日</p></td>
<td><p>蜷曲之滴（傾洩而出的滴珠）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三十九卷</p></td>
<td><p>2013年3月1日</p></td>
<td></td>
<td><p>2015年1月12日</p></td>
<td><p>妖花之塚（雨後被蔽的塵埃）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四十卷</p></td>
<td><p>2013年6月1日</p></td>
<td></td>
<td><p>2015年5月11日</p></td>
<td><p>顫慄之瞳（存於眼中的顫慄）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第四十一卷</p></td>
<td><p>2013年10月1日</p></td>
<td></td>
<td><p>2015年7月20日</p></td>
<td><p>傷逝之櫻（遺落成片的悲傷日子）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四十二卷</p></td>
<td><p>2014年4月1日</p></td>
<td></td>
<td><p>2015年10月19日</p></td>
<td><p>浮生幻夢（稍縱即逝的夢境）</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="odd">
<td><p>第四十三卷</p></td>
<td><p>2014年7月1日</p></td>
<td></td>
<td><p>2016年1月11日</p></td>
<td><p>召喚之音（招來騷亂之聲飛舞）</p></td>
<td><p>道敷篇</p></td>
</tr>
<tr class="even">
<td><p>第四十四卷</p></td>
<td><p>2014年10月1日</p></td>
<td></td>
<td><p>2016年4月11日</p></td>
<td><p>凝聚之牆（凝聚於牆內的騷動）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第四十五卷</p></td>
<td><p>2015年3月1日</p></td>
<td></td>
<td><p>2016年7月18日</p></td>
<td><p>虛假之門（再次關閉虛假）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四十六卷</p></td>
<td><p>2015年7月1日</p></td>
<td></td>
<td><p>2016年10月17日</p></td>
<td><p>朽木之陰</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第四十七卷</p></td>
<td><p>2015年11月1日</p></td>
<td></td>
<td><p>2017年1月9日</p></td>
<td><p>替身之翅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四十八卷</p></td>
<td><p>2016年7月1日</p></td>
<td></td>
<td><p>2017年6月12日</p></td>
<td><p>真情之守</p></td>
<td><p>番外篇</p></td>
</tr>
<tr class="odd">
<td><p>第四十九卷</p></td>
<td><p>2016年8月1日</p></td>
<td></td>
<td><p>2017年10月16日</p></td>
<td><p>終命之日</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="even">
<td><p>第五十卷</p></td>
<td><p>2016年11月1日</p></td>
<td></td>
<td><p>2018年11月12日</p></td>
<td><p>似遠還近</p></td>
<td><p>厳霊編</p></td>
</tr>
<tr class="odd">
<td><p>第五十一卷</p></td>
<td><p>2017年4月1日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第五十二卷</p></td>
<td><p>2017年10月1日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 外傳

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>日本發行日期</p></th>
<th><p>日文標題</p></th>
<th><p>台灣發行日期</p></th>
<th><p>中文標題</p></th>
<th><p>系列</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>外傳</p></td>
<td><p>2010年7月30日</p></td>
<td></td>
<td><p>2013年3月11日</p></td>
<td><p>大陰陽師 安倍晴明：我將顛覆天命</p></td>
<td><p>陰陽師・安倍晴明</p></td>
</tr>
<tr class="even">
<td><p>外傳</p></td>
<td><p>2013年2月27日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>外傳</p></td>
<td><p>2015年2月27日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>外傳</p></td>
<td><p>2016年2月25日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 電視動畫

從2006年10月5日起放映預定。製作公司是[STUDIO
DEEN](../Page/STUDIO_DEEN.md "wikilink")，共26話。

台灣ANIMAX在播放時以台灣國語配音與日文原音分時段播放，但在中文配音版中，於某些昌浩快速念咒的場合使用日文原音而不做配音。

香港ANIMAX在播放時以粵日雙語雙聲道播放。

### 製作人員

  - 原作 - [結城光流](../Page/結城光流.md "wikilink")

  - 企劃 - 及川武、上玉利純宏、川村明廣、湯淺昭博

  - 監督 - [森邦宏](../Page/森邦宏.md "wikilink")

  - 系列構成 - [淺川美也](../Page/淺川美也.md "wikilink")

  - 人物原案 - [淺木櫻](../Page/淺木櫻.md "wikilink")

  - 人物設定 -

  - 道具設計 - 小坂知

  - 總作畫監督 - 堀越久美子

  - 作畫監督補佐 - 清水博明、江森真理子、山崎輝彥、菊永千里

  - 特殊演技 - 水村良男

  - 原畫 - [陸演隊](../Page/陸演隊.md "wikilink")、、、、、、紅組、、[MOOK
    DLE](../Page/MOOK_DLE.md "wikilink")、、AI、Wish、[TAMA
    PRODUCTION](../Page/TAMA_PRODUCTION.md "wikilink")、[ECHO](../Page/ECHO.md "wikilink")、火鳥動畫、（Drop）、HONG
    YING

  - 動畫檢查 - 澤村享、粕川智美、赤堀隆一、鈴木裕輔、辻浩樹、海保仁美、深見沙和、菅原美智代

  - 美術監督 - 小山俊久（[Production-ai](../Page/Production-ai.md "wikilink")）

  - 美術擔當 - 菱沼康範

  - 色彩設計 - 北爪英子

  - 色指定・仕上檢查 - 大須賀隆純、北爪英子、後藤恵子、永野綾香、田村智美、大谷和也

  - 完成 - 、津茂谷知里、美馬真理子、完甘幸隆、永野綾香、大谷和也、ECHO、火鳥動畫、飛龍動畫

  - 撮影監督 - 川口正幸

  - 撮影 -

      - \- 川口正幸、坂本彬惠、下崎昭、周藤智敬、越山麻彥、濱尾繁光

      - 神山茂男、阿部照男、伏見真一、佐藤賢伸、宮城千賀子、佐藤慎也（第7．10話）

      - \- 鎌田克明、青木孝司、澤田浩司、藤田智史、齋藤仁、大井勝利、小西康平、堀野大輔（第21・24話）

  - 3D - （馬場就大・池野直幸）※第14話

  - 特殊效果 - 上原將一、遠藤剛彥・山中彰（第7話）、中西徳則（第21．24話）

  - 編輯 - 松村正宏

  - 錄像編集 - [東京現像所](../Page/東京現像所.md "wikilink")（金高明宏）

  - 編集助手 - 三田沙彌佳

  - 錄音監督 - [本山哲](../Page/本山哲.md "wikilink")

  - 錄音制作 -

  - 錄音制作擔當 - 兼塚忠幸

  - 效果 - 高梨繪美（[eNa](../Page/eNa.md "wikilink")）

  - 錄音工作室 - 、

  - 調整 - 大石幸平（T\&T）

  - 錄音助手 - 吉原裕美（T\&T）

  - 音樂 - [中川孝](../Page/中川孝.md "wikilink")

  - 音樂制作 - [Frontier Works](../Page/Frontier_Works.md "wikilink")、

  - 音樂製片人 - 吉川明、矢部敦志、川村和義

  - 標誌設計 - 石阪嘉康

  - \- 田村烈

  - \-

  - 設定制作 - 米岡仁司

  - 制作合作 - （第7・10話）、AI（第21・24話）

  - 合作 - （岡山智子、野崎智子、西川千裕）

  - \- 伊藤將彥、黑岩裕之

  - 媒體擔當 - 須藤清美

  - 宣傳合作 - 西山洋介

  - 助手製片人 - 鶴卷苑美、岡村武真

  - 製片人 - 、西川路健太、小川泰之、日向泰隆

  - 動畫製片人 - 浦崎宣光

  - 動畫製作 - [STUDIO DEEN](../Page/STUDIO_DEEN.md "wikilink")

  - 製作 - 少年陰陽師製作委員會

### 主題曲

  - 片頭曲「」
    作詞 -  / 作曲、合唱 -  / 編曲 - [Ｈ∧Ｌ](../Page/Ｈ∧Ｌ.md "wikilink") / 歌 -
    [引田香織](../Page/引田香織.md "wikilink")
  - 片尾曲「」
    作詞、作曲 - 廣田由佳 / 編曲 - 松浦晃久 / 歌 - [木氏沙織](../Page/木氏沙織.md "wikilink")
  - 最終話片尾曲「」
    作詞 - U-ka / 作曲 - 榎本太己 / 編曲 - rhythmic bird / 歌 -
    安倍昌浩（[甲斐田雪](../Page/甲斐田雪.md "wikilink")）

### 播放電視台

<table>
<thead>
<tr class="header">
<th><p>播放地區</p></th>
<th><p>電視台</p></th>
<th><p>播放期間</p></th>
<th><p>播放時間（<a href="../Page/UTC+9.md" title="wikilink">UTC+9</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/近畿廣域圏.md" title="wikilink">近畿廣域圏</a></p></td>
<td><p><a href="../Page/關西電視台.md" title="wikilink">關西電視台</a></p></td>
<td><p>2006年10月3日 - 2007年3月27日</p></td>
<td><p>星期一 26:15 - 26:45<br />
→星期二 26:30 - 27:00</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a></p></td>
<td><p><a href="../Page/千葉電視台.md" title="wikilink">千葉電視台</a></p></td>
<td><p>2006年10月4日 - 2007年3月26日</p></td>
<td><p>星期三 25:30 - 26:00</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/埼玉縣.md" title="wikilink">埼玉縣</a></p></td>
<td><p><a href="../Page/埼玉電視台.md" title="wikilink">埼玉電視台</a></p></td>
<td><p>星期三 26:00 - 26:30</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><p><a href="../Page/神奈川電視台.md" title="wikilink">tvk</a></p></td>
<td><p>2006年10月7日 - 2007年3月31日</p></td>
<td><p>星期六 25:00 - 25:30</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中京廣域圏.md" title="wikilink">中京廣域圏</a></p></td>
<td><p><a href="../Page/東海電視台.md" title="wikilink">東海電視台</a></p></td>
<td><p>2006年11月2日 - 2007年5月10日</p></td>
<td><p>星期四 27:27 - 27:57</p></td>
</tr>
<tr class="even">
<td><p>全日本</p></td>
<td><p><a href="../Page/AT-X.md" title="wikilink">AT-X</a></p></td>
<td><p>2007年1月19日 - 7月13日</p></td>
<td><p>星期五 10:00 - 10:30</p></td>
</tr>
</tbody>
</table>

### 各話標題

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>窮奇篇</dt>

</dl>
<ol>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td><dl>
<dt>風音篇</dt>

</dl>
<ol start="13">
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
</tr>
</tbody>
</table>

## CD

  - 廣播劇CD
      - 少年陰陽師 窮奇篇 廣播劇CD 全3卷（原作1～3卷）
      - 少年陰陽師 風音篇 廣播劇CD 全4卷（原作4～7卷）
      - 少年陰陽師 天狐篇 廣播劇CD 第1～3卷（原作9～11卷）
      - 少年陰陽師 番外篇 廣播劇CD 第1卷（原作8卷）
  - 音樂CD
      - 少年陰陽師 原聲音樂＆廣播劇CD 第1卷
      - 少年陰陽師 角色歌曲 「」春
      - 少年陰陽師 角色歌曲 「」夏
      - 少年陰陽師 角色歌曲 「」秋
      - 少年陰陽師 角色歌曲 「」冬
      - 少年陰陽師 角色歌曲 花鳥風月～殘月～
      - 少年陰陽師 角色歌曲 花鳥風月～白夜～

## 相關

  - [安倍晴明](../Page/安倍晴明.md "wikilink")
  - [陰陽師](../Page/陰陽師.md "wikilink")

## 外部連結

  - [動畫官方網站](http://www.seimeinomago.net/web/)

  - [結城光流官方網站『狹霧殿』](http://www.yuki-mitsuru.com/)

  - [廣播節目網站](https://web.archive.org/web/20070901222020/http://www.animate.tv/digital/web_radio/detail_072.html)

  - [皇冠中文版小說官方網站](http://www.crown.com.tw/shounenonmyouji/index.htm)

  - [木棉花動畫官方網站](http://www.e-muse.com.tw/property/seimeinomago/introduction/20070611/)

## 參考資料

<references/>

[Category:未完結作品](../Category/未完結作品.md "wikilink")
[Category:角川Beans文庫](../Category/角川Beans文庫.md "wikilink")
[Category:日本奇幻小說](../Category/日本奇幻小說.md "wikilink")
[Category:妖怪題材作品](../Category/妖怪題材作品.md "wikilink")
[Category:平安時代背景作品](../Category/平安時代背景作品.md "wikilink")
[Category:驅魔題材作品](../Category/驅魔題材作品.md "wikilink")
[Category:阴阳道题材作品](../Category/阴阳道题材作品.md "wikilink")
[Category:廣播劇CD](../Category/廣播劇CD.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:奇幻漫畫](../Category/奇幻漫畫.md "wikilink")
[Category:2006年UHF動畫](../Category/2006年UHF動畫.md "wikilink")
[Category:奇幻動畫](../Category/奇幻動畫.md "wikilink")
[Category:八大電視外購動畫](../Category/八大電視外購動畫.md "wikilink")
[Category:輕小說改編動畫](../Category/輕小說改編動畫.md "wikilink")
[Category:月刊Asuka](../Category/月刊Asuka.md "wikilink")

1.  ISBN 978-957-33-2286-3