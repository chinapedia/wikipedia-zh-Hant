**尖孢鐮刀菌**（*Fusarium
oxysporum*）是[镰刀菌属的一種](../Page/镰刀菌属.md "wikilink")[真菌](../Page/真菌.md "wikilink")。部份尖孢鐮刀菌被用來製作[納米材料](../Page/納米材料.md "wikilink")（尤其是納米銀顆粒）。

## 古巴專化型

**尖孢鐮刀菌古巴專化型**（*Fusarium oxysporum* f.sp.
*cubense*，又名**香蕉鐮刀菌**）是會感染某種[香蕉引起](../Page/香蕉.md "wikilink")[黃葉病的](../Page/黃葉病.md "wikilink")[病原體](../Page/病原體.md "wikilink")，曾經使[大米七](../Page/大米七.md "wikilink")（）這種香蕉絕種。

曾有「香蕉王國」美譽\[1\][高雄縣](../Page/高雄縣.md "wikilink")[旗山鎮地區](../Page/旗山區.md "wikilink")。於1967年就曾因黃葉病而使[台灣](../Page/台灣.md "wikilink")[香蕉外銷受到極大的打擊](../Page/香蕉.md "wikilink")。\[2\]\[3\]

此菌的四個小種會侵害不同[栽培品種的香蕉](../Page/栽培品種.md "wikilink")：

  - 1號小種能感染AAA種的大米七，造成20世紀上半葉的黃葉病疫症。另外1號小種也感染AAB種的*Musa* 'Pome'、*Musa*
    'Silk'和ABB種的*Musa* 'Pisang Awak'。
  - 2號小種感染ABB種的*Musa* 'Bluggoe'及其近親。
  - 3號小種感染[赫蕉屬的植物](../Page/赫蕉屬.md "wikilink")。
  - 4號小種感染AAA種的矮把[香芽蕉](../Page/香芽蕉.md "wikilink")（*Musa* 'Dwarf
    Cavendish'），以及1、2號小種的宿主。\[4\]\[5\]

## 參考文獻

<div class="references-small">

<references />

</div>

[分類:植物病原體與疾病](../Page/分類:植物病原體與疾病.md "wikilink")

[Category:鐮刀菌屬](../Category/鐮刀菌屬.md "wikilink")

1.  [旗山香蕉王國](http://www.fast.org.tw/s02/left02.htm)
2.  吳田泉，1993，《臺灣農業史》，自立晚報社
3.  林純美，1989，《香蕉王國興亡史—吳振瑞悲情二十年》
4.  Crop Protection Compendium 2005 Edition. Fusarium oxysporum f.sp.
    cubense (Panama disease of banana). (CAB International: Wallingford,
    UK).
5.  Ploetz RC & Pegg KG (2000). Fusarium wilt. In Jones DR (ed.)
    Diseases of banana, abaca and enset. (CABI Publishing: Wallingford,
    UK). p.143-159.