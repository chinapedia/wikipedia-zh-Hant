**尖沙咀站**（）為[港鐵](../Page/港鐵.md "wikilink")[荃灣綫的一個車站](../Page/荃灣綫.md "wikilink")，也是荃灣綫穿越[維多利亞港至](../Page/維多利亞港.md "wikilink")[香港島前最後一個車站](../Page/香港島.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[油尖旺區](../Page/油尖旺區.md "wikilink")[尖沙嘴](../Page/尖沙嘴.md "wikilink")[彌敦道地底](../Page/彌敦道.md "wikilink")，於1979年12月16日啟用，承建商是[西松建設株式會社](../Page/西松建設.md "wikilink")。

尖沙咀站設有[行人隧道系統連接周邊地區及](../Page/行人隧道.md "wikilink")[尖東站](../Page/尖東站.md "wikilink")，供乘客轉乘[西鐵綫](../Page/西鐵綫.md "wikilink")；但由於該系統為非收費區，乘客不能以一張單程車票在此站轉乘；若以[八達通轉乘需要先出閘](../Page/八達通.md "wikilink")、後入閘。位於半島中心和海港城的[港鐵特惠站](../Page/港鐵特惠站.md "wikilink")，為使用成人八達通卡的乘客提供港幣2元車費回贈。

## 車站設計

### 樓層

尖沙咀站共有2層，地面為出入口，大堂位於L1層\[1\]，荃灣綫月台則位於L2層。\[2\]

<table>
<tbody>
<tr class="odd">
<td><p><strong>U1</strong></p></td>
<td><p>九龍公園</p></td>
<td><p>A1出入口升降機、九龍公園</p></td>
</tr>
<tr class="even">
<td><p><strong>G</strong></p></td>
<td><p>地面</p></td>
<td><p>A－E出入口</p></td>
</tr>
<tr class="odd">
<td><p><strong>L1</strong></p></td>
<td><p>大堂</p></td>
<td><p>D3、H及R出入口、客務中心、商店、自助服務、「會員服務站」、免費Wi-Fi熱點</p></td>
</tr>
<tr class="even">
<td><p>行人隧道系統</p></td>
<td><p>連接尖東站出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>L2</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>島式月台，開右車門</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>荃灣綫往<a href="../Page/中環站.md" title="wikilink">中環</a><small>（<a href="../Page/金鐘站.md" title="wikilink">金鐘</a>）</small></p></td>
<td></td>
</tr>
</tbody>
</table>

### 大堂

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Tsim Sha Tsui Station 2019 01 part1.jpg | caption1 =
2019年翻新中的大堂（收費區外） | image2 = Tsim Sha Tsui Station 2019 01
part2.jpg | caption2 = 大堂（收費區內） | image3 = Tsim Sha Tsui Station
Concourse Shops 201506.jpg | caption3 = 商店 }}
尖沙咀站的大堂位於L1層，為了方便乘客進出，收費區位於大堂的南北兩方，四周被行人通道及商店包圍，因此商店主要是在大堂非收費區，收費區內設有多組[扶手電梯及](../Page/電動扶梯.md "wikilink")[樓梯往返月台](../Page/樓梯.md "wikilink")\[3\]。

#### 商店及自助服務

現時，尖沙咀站內提供不同類型的商店供乘客購物或進食，例如有[便利店](../Page/便利商店.md "wikilink")、[麵包糕餅店](../Page/麵包糕餅店.md "wikilink")、[咖啡店](../Page/咖啡店.md "wikilink")、[書店](../Page/書店.md "wikilink")、[花店](../Page/花店.md "wikilink")、[銀行及](../Page/銀行.md "wikilink")[速遞等](../Page/速遞.md "wikilink")\[4\]\[5\]。由於港鐵的收費區是不能進食，因此大部份[食肆均不會設在收費區內](../Page/食肆.md "wikilink")。

此外，尖沙咀站也有不少自助服務設施供乘客使用，包括[自動櫃員機](../Page/自動櫃員機.md "wikilink")、[自動售賣機](../Page/自動販賣機.md "wikilink")、自動照相機等\[6\]。車站內亦設有[香港郵政](../Page/香港郵政.md "wikilink")[郵箱方便乘客在車站內投寄](../Page/郵箱.md "wikilink")[郵件](../Page/郵件.md "wikilink")\[7\]、「iCentre」免費上網服務設施供乘客瀏覽[萬維網的內容](../Page/全球資訊網.md "wikilink")\[8\]，以及「[會員服務站](../Page/會員服務站.md "wikilink")」，供乘客登記為港鐵友禮會會員、查閱獎分及換領獎賞\[9\]。

[Tsim_Sha_Tsui_Station_2017_09_part5.jpg](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_Station_2017_09_part5.jpg "fig:Tsim_Sha_Tsui_Station_2017_09_part5.jpg")

### 月台

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Tsim Sha Tsui Station 2019 01 part6.jpg | caption1 =
1號月台（往荃灣方向） | image2 = Tsim Sha Tsui Station 2019 01
part5.jpg | caption2 = 2號月台（往中環方向） }}
車站設有2個裝有[月台幕門的月台](../Page/月台幕門.md "wikilink")，以[島式月台排列](../Page/島式月台.md "wikilink")。另外，尖沙咀站至[佐敦站之間設有](../Page/佐敦站.md "wikilink")[渡綫](../Page/渡綫.md "wikilink")。當海底隧道或荃灣綫位於香港島的[金鐘站有故障時](../Page/金鐘站.md "wikilink")，列車將會暫停過海服務，並以尖沙咀站作為臨時終點站。

<File:Platform> of Tsim Sha Tsui Station
(20181007140405).jpg|由月台至大堂的扶手電梯及樓梯通道（2018年）

[Tsim_Sha_Tsui_Station_2017_09_part2.jpg](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_Station_2017_09_part2.jpg "fig:Tsim_Sha_Tsui_Station_2017_09_part2.jpg")
[Tsim_Sha_Tsui_Station_2017_09_part3.jpg](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_Station_2017_09_part3.jpg "fig:Tsim_Sha_Tsui_Station_2017_09_part3.jpg")

### 出入口

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Entrance and exit A1 of Tsim Sha Tsui Station after
renovation.jpg | caption1 = 重建後的A1出入口（初時斜道設於近海防道一邊） | image2 = Tsim Sha
Tsui Station Exit E 2018.jpg | caption2 = E出入口 | image3 =
Tsim_Sha_Tsui_Station_Exit_H_201809.jpg | caption3 = H出入口 | image4
= MTR TST (11).JPG | caption4 = C及R出入口 | image5 = MTR TST (10).JPG |
caption5 = 重建後尚未啟用的D1出入口，後方為已重新啟用的D2出入口 | image6 = MTR TST (9).JPG |
caption6 = 連接[K11商場地庫的D](../Page/K11_\(香港\).md "wikilink")3出口 | image7 =
MTR TST ETS.JPG | caption7 = 連接尖沙咀站及尖東站的南面通道，現時出入口編號已取消 }}
尖沙咀站設有12個出入口，主要連接[尖沙咀區內的購物中心](../Page/尖沙咀.md "wikilink")、酒店及文娛康樂設施等。由於車站在[彌敦道地底](../Page/彌敦道.md "wikilink")，因此大部份出入口均鄰近彌敦道及尖沙咀區內的街道。其中原稱F、G的兩個出入口與[尖東站相連](../Page/尖東站.md "wikilink")，乘客可透過該出入口前往尖東站及行人隧道系統各出入口\[10\]，而H及R出入口則連接車站旁的[國際廣場地庫](../Page/國際廣場.md "wikilink")，其中R出入口的編號是遷就尖東站出入口編號而編定的，因此亦成為目前整個港鐵系統中採用最大英文字母的出入口編號。

港鐵於2013年2月至4月中期間為尖沙咀站及尖東站進行導向指示改善工程，其中F出入口和G出入口由於是連接尖東站的通道，港鐵於2013年4月11日取消上述兩個出入口編號。

2014年，港鐵為位處於旅遊區的尖沙咀站A1出入口全新設計，將出入口化身由玻璃建造的巨型水晶屋，透過善用自然光源節省能源，達致環境保護效果，而且透明外表亦更容易融入周遭環境；計劃亦包括一部連接大堂及地面的升降機以及扶手電梯，方便乘客進出車站，其中升降機可以直達[九龍公園入口層](../Page/九龍公園.md "wikilink")。工程於同年1月15日開始\[11\]，於2016年5月7日竣工\[12\]。A1出入口改建後原本於海防道設有無障礙通道，但臨時出入口拆卸工程於2016年底完成後，此無障礙通道轉移至靠近九龍公園出入口。工程期間，連接A1和A2出入口之間，位於彌敦道和車站大堂之間的地下行人通道臨時封閉，現已解封。

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>指示</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[13]</p></td>
<td><p><a href="../Page/九龍公園.md" title="wikilink">九龍公園</a></p></td>
<td><p>九龍公園、<a href="../Page/廣東道.md" title="wikilink">廣東道</a>、<a href="../Page/中國客運碼頭.md" title="wikilink">中國客運碼頭</a>、<a href="../Page/中港城.md" title="wikilink">中港城</a>、<a href="../Page/海防道.md" title="wikilink">海防道</a>、<a href="../Page/漢口道_(香港).md" title="wikilink">漢口道</a>、<a href="../Page/海港城.md" title="wikilink">海港城</a>、<a href="../Page/衛生教育展覽及資料中心.md" title="wikilink">衛生教育展覽中心</a>、<a href="../Page/香港文物探知館.md" title="wikilink">香港文物探知館</a>、<a href="../Page/九龍清真寺.md" title="wikilink">九龍清真寺</a>、<a href="../Page/栢麗購物大道.md" title="wikilink">栢麗購物大道</a>、<a href="../Page/新港中心.md" title="wikilink">新港中心</a>、<a href="../Page/皇家太平洋酒店.md" title="wikilink">皇家太平洋酒店</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/堪富利士道.md" title="wikilink">堪富利士道</a></p></td>
<td><p>晉逸精品酒店尖沙咀、<a href="../Page/加拿分道.md" title="wikilink">加拿分道</a>、昌興大廈、格蘭中心、粵海酒店、<a href="../Page/赫德道.md" title="wikilink">赫德道</a>、<a href="../Page/寶勒巷.md" title="wikilink">寶勒巷</a>、堪富利士道、堪富利廣場、<a href="../Page/河內道.md" title="wikilink">河內道</a>、文遜大廈、<a href="../Page/職業性失聰補償管理局.md" title="wikilink">職業性失聰補償管理局</a>、時財商業大廈</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/金馬倫道.md" title="wikilink">金馬倫道</a></p></td>
<td><p><a href="../Page/古物古蹟辦事處.md" title="wikilink">古物古蹟辦事處</a>、尖沙咀皇悅酒店、<a href="../Page/加連威老道.md" title="wikilink">加連威老道</a>、尖沙咀滙豐大廈、<a href="../Page/入境事務處.md" title="wikilink">入境事務處</a>、<a href="../Page/香港君怡酒店.md" title="wikilink">君怡酒店</a>、<a href="../Page/金巴利道.md" title="wikilink">金巴利道</a>、<a href="../Page/彌敦道.md" title="wikilink">彌敦道</a>、<a href="../Page/諾士佛臺.md" title="wikilink">諾士佛臺</a>、<a href="../Page/美麗華商場.md" title="wikilink">美麗華商場</a>、The Luxe Manor、<a href="../Page/The_Mira_Hong_Kong.md" title="wikilink">The Mira Hong Kong</a>、Mira Mall、<a href="../Page/The_ONE.md" title="wikilink">The ONE</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>金馬倫里、金馬倫道、加拿芬廣場、<a href="../Page/漆咸道.md" title="wikilink">漆咸道南</a>、厚福街、<a href="../Page/香港中文大學專業進修學院.md" title="wikilink">中大專業進修學院</a>（安年）、香港基督教服務處、<a href="../Page/香港歷史博物館.md" title="wikilink">香港歷史博物館</a>、<a href="../Page/香港科學館.md" title="wikilink">香港科學館</a>、<a href="../Page/百樂酒店.md" title="wikilink">百樂酒店</a>、九龍華美達酒店、仕德福山景酒店、<a href="../Page/尖沙咀東.md" title="wikilink">尖沙咀東部</a>（近金馬倫道）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/北京道.md" title="wikilink">北京道</a></p></td>
<td><p><a href="../Page/亞士厘道.md" title="wikilink">亞士厘道</a>、廣東道、消委會諮詢及資源中心、<a href="../Page/香港工業總會.md" title="wikilink">工業總會簽證處</a>、漢口道、亞太中心、<a href="../Page/樂道.md" title="wikilink">樂道</a>、<a href="../Page/北京道.md" title="wikilink">北京道</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海防道、九龍清真寺、九龍公園、彌敦道</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/加拿分道.md" title="wikilink">加拿分道</a></p></td>
<td><p>（為配合興建新行人隧道，此出口暫停使用）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/重慶大廈.md" title="wikilink">重慶大廈</a>、CKE購物商場、帝國酒店、美麗都大廈、<a href="../Page/K11_(香港).md" title="wikilink">K11購物藝術館</a>、<a href="../Page/麼地道.md" title="wikilink">麼地道</a>、彌敦道、<a href="../Page/喜來登酒店.md" title="wikilink">喜來登酒店</a>、<a href="../Page/活方商場.md" title="wikilink">活方商場</a>、碧仙桃路、加拿分道、<a href="../Page/赫德道.md" title="wikilink">赫德道</a>、<a href="../Page/河內道.md" title="wikilink">河內道</a>、<a href="../Page/香港總商會.md" title="wikilink">香港總商會簽證部</a>、<a href="../Page/寶勒巷.md" title="wikilink">寶勒巷</a>、豪峰軒</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>K11購物藝術館地庫</p></td>
<td><p>K11購物藝術館</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/半島酒店.md" title="wikilink">半島酒店</a></p></td>
<td><p>彩星中心、半島酒店、<a href="../Page/九龍酒店.md" title="wikilink">九龍酒店</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/國際廣場.md" title="wikilink">國際廣場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰接車站

尖沙咀站是荃灣綫的中途站，乘客可在此乘坐往[中環或](../Page/中環站.md "wikilink")[荃灣的列車](../Page/荃灣站.md "wikilink")，往返[香港島](../Page/香港島.md "wikilink")、九龍及[新界等地](../Page/新界.md "wikilink")。另外，乘客可於該站經隧道往[尖東站轉乘](../Page/尖東站.md "wikilink")[西鐵綫列車](../Page/西鐵綫.md "wikilink")。

## 利用狀況

[Tsim_Sha_Tsui_Station_peak_hour_congestion.JPG](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_Station_peak_hour_congestion.JPG "fig:Tsim_Sha_Tsui_Station_peak_hour_congestion.JPG")
[MTR_TST_(8).JPG](https://zh.wikipedia.org/wiki/File:MTR_TST_\(8\).JPG "fig:MTR_TST_(8).JPG")
尖沙咀站是港鐵\[14\]系統中最繁忙和人流量最高的車站，高峰期每小時人流可達4萬人次\[15\]，每天人流更有大約26萬人次。而車站位於[彌敦道地底](../Page/彌敦道.md "wikilink")，鄰近多個著名旅遊景點、大型購物商場、酒店和商業大廈，也有地下通道連接鄰近商場，因此每天有不少[上班族和遊客進出車站](../Page/上班族.md "wikilink")，也是[荃灣綫](../Page/荃灣綫.md "wikilink")16個車站中最多人出入閘的車站。

車站設兩條行人隧道連接[尖東站](../Page/尖東站.md "wikilink")，方便乘客前往[尖東一帶或經](../Page/尖東.md "wikilink")[西鐵綫前往其他地區](../Page/西鐵綫.md "wikilink")，或先乘西鐵綫至[紅磡轉乘](../Page/紅磡站.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")。而西鐵綫（屯門—紅磡）及東鐵綫（上水／烏溪沙—尖東）的[全月通加強版亦包括此站](../Page/全月通加強版.md "wikilink")，故吸引乘客使用此站從而進一步提升人流。

2004年[尖東站啟用後](../Page/尖東站.md "wikilink")，每日早上繁忙時間往[中環方向](../Page/中環站.md "wikilink")，乘客更要等候2至3班車才能登車（月台車頭位置極難登車，因為該處處理[尖東站轉乘人流](../Page/尖東站.md "wikilink")，車尾位置則明顯較易登車）。至於每日下午繁忙時間往[荃灣方向](../Page/荃灣站.md "wikilink")，因在前站[金鐘站擠上車廂的乘客甚多](../Page/金鐘站.md "wikilink")，即使當中部分乘客會在本站下車，在本站上車的乘客仍要等候2至3班車才能登車（月台車頭位置較易登車）。同樣在下午繁忙時間，部份從港島區出發的乘客或選用本站到尖東站轉乘西鐵綫到紅磡站再轉乘[東鐵綫往](../Page/東鐵綫.md "wikilink")[新界東](../Page/新界東.md "wikilink")，以避開[旺角站及](../Page/旺角站.md "wikilink")[九龍塘站的人潮及在](../Page/九龍塘站.md "wikilink")[紅磡站選擇座位](../Page/紅磡站.md "wikilink")。

在平日下班時間，B1及B2出口會實施人潮管制措施，B1出口將改為出站出口，而B2出口將改為進站入口。

每逢[平安夜](../Page/平安夜.md "wikilink")、[除夕](../Page/除夕.md "wikilink")、[年初一](../Page/年初一.md "wikilink")[花車巡遊及煙花匯演舉行的日子](../Page/花車巡遊.md "wikilink")，該站人流會更高，可能會實施人潮管制措施。

## 接駁交通

由於尖沙咀站位於繁忙的[彌敦道地底](../Page/彌敦道.md "wikilink")，加上處於已發展的[商業中心區內](../Page/商業中心區.md "wikilink")，車站四周欠缺大型[公共運輸交匯處](../Page/公共運輸交匯處.md "wikilink")，大部份的[巴士及](../Page/香港巴士.md "wikilink")[小巴主要是以中途站方式途經尖沙咀站](../Page/香港小巴.md "wikilink")，或是於[尖沙咀天星碼頭或](../Page/尖沙咀天星碼頭公共運輸交匯處.md "wikilink")[麼地道設總站](../Page/麼地道.md "wikilink")。

乘客亦可步行至[中國客運碼頭轉乘渡輪前往](../Page/中國客運碼頭.md "wikilink")[澳門及](../Page/澳門.md "wikilink")[珠江三角洲等地](../Page/珠江三角洲.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>接駁交通列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{西鐵綫色彩}}">█</font><a href="../Page/西鐵綫.md" title="wikilink">西鐵綫</a>：<a href="../Page/尖東站.md" title="wikilink">尖東站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl>
<dl>
<dt><a href="../Page/渡輪.md" title="wikilink">渡輪</a></dt>

</dl>
<ul>
<li><a href="../Page/天星小輪.md" title="wikilink">天星小輪</a>：
<ul>
<li><a href="../Page/尖沙咀天星碼頭.md" title="wikilink">尖沙咀</a> ↔︎ <a href="../Page/中環碼頭.md" title="wikilink">中環</a></li>
<li>尖沙咀 ↔︎ <a href="../Page/灣仔碼頭.md" title="wikilink">灣仔</a></li>
</ul></li>
<li><a href="../Page/新世界第一渡輪.md" title="wikilink">新世界第一渡輪</a>：
<ul>
<li><a href="../Page/中國客運碼頭.md" title="wikilink">中國客運碼頭</a> ↔︎ <a href="../Page/外港碼頭.md" title="wikilink">澳門</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 與尖東站轉乘

尖沙咀站設有[行人隧道系統連接周邊地區及西鐵綫尖東站](../Page/行人隧道.md "wikilink")。但由於兩鐵合併前九廣鐵路公司與香港政府有協議，要將連接兩個站的行人隧道開放予公眾使用，不得劃為收費區，因此分隔兩站的[閘機不會被拆除](../Page/驗票閘門.md "wikilink")。

使用[八達通或](../Page/八達通.md "wikilink")[港鐵都會票者於](../Page/港鐵都會票.md "wikilink")30分鐘內在此兩站轉綫，會被視作一程車程，因此如從其他車站到達該站並由[尖東站返回尖沙咀站者則會按港鐵的](../Page/尖東站.md "wikilink")「車票發出條件」被徵收原站出入超過20分鐘的費用（殘疾人士或長者除外）。而從該站出發超過150分鐘則會被收取相等於當時港鐵最高車費作為附加費。

另外使用單程票的乘客則需要先出閘，然後再另購車票入閘（兩程車費會高於其他轉車站轉綫）。

## 歷史

### 車站命名

尖沙咀的正字和舊稱實為**尖沙嘴**，需要注意的是「嘴」為名詞而「咀」則為動詞，因此舊稱更能反映出周邊地形特徵\[16\]。然而，當時香港的地下鐵路公司將本站定名為「尖沙咀站」，此次命名對鄰近地方往後發展影響深遠，「尖沙咀」甚至已取代了原來「尖沙嘴」的舊稱，並廣見於商業地址、大廈名稱（如[尖沙咀中心](../Page/尖沙咀中心.md "wikilink")）和公共設施。不過個别媒體(如[蘋果日報](../Page/蘋果日報.md "wikilink"))依然會以正字「尖沙嘴」、「尖沙嘴站」稱之。

### 修正早期系統

尖沙咀站於1979年12月16日啟用，當時為修正早期系統的南行終點站。1980年2月12日，[地鐵](../Page/香港地鐵.md "wikilink")\[17\]過海段（尖沙咀－[中環](../Page/中環站.md "wikilink")）啟用，尖沙咀站便成為過海前的中途站。

### 設置永久商舖

1987年，地鐵公司在車站大堂進行重修工程，以設置永久舖位。\[18\]

### 加建E出入口

1989年12月，尖沙咀站擴建大堂南端、增設[扶手電梯並加建E出入口](../Page/扶手電梯.md "wikilink")，以改善該站的擠塞情況。\[19\]

### 加建尖東站轉乘通道及改建A2出入口

為配合九廣東鐵尖沙咀支線，尖沙咀站於2002年開始擴建，工程由熊谷組株式會社承辦。當中包括：\[20\]

  - 擴建大堂南端，以容納因接駁[麼地道行人隧道而需要遷移的機房](../Page/麼地道.md "wikilink")；
  - 於大堂南端興建行人隧道連接[中間道行人隧道](../Page/中間道.md "wikilink")；
  - 改動大堂佈局，在站內提供一條通道，並成為尖沙咀區行人隧道系統的一部份；
  - 重建A2出入口，安裝一條扶手電梯連接地面及A1與A2出入口之間通道，以及兩條扶手電梯連接通道及大堂；
  - 增設一條扶手電梯及一部升降機連接大堂及月台；
  - 重新改裝車站控制室並提升功能。

A2出入口原先經A1出入口通道連接大堂。改建後經新建扶手電梯直接連接大堂，獨立於A1出入口。A1與A2出入口之間通道隨之封閉，改建為警崗，但警崗隨後搬遷至中間道行人隧道附近，A1與A2出入口之間再度重開通道，以便作行人隧道使用。

2004年10月24日，連接尖沙咀站和尖東站的麼地道行人隧道啟用，當時稱為尖沙咀站G出口，尖沙咀站成為地鐵\[21\]和九廣東鐵繼[九龍塘站後的第二個轉乘站](../Page/九龍塘站_\(香港\).md "wikilink")。

2005年4月4日，連接大堂南端的F出入口的中間道行人隧道啟用，方便乘客利用中間道行人隧道轉乘東鐵綫\[22\]或前往尖沙咀南面主要建築物。此外，在興建F出入口工程期間，同時把車站大堂南端擴充，有助解決E出入口車站通道擠迫的問題。

2007年12月2日，隨著[兩鐵合併](../Page/兩鐵合併.md "wikilink")，九廣東鐵更名為東鐵綫\[23\]，由當天起乘客在尖沙咀站及[尖東站轉乘](../Page/尖東站.md "wikilink")，可享有轉綫車費調減，但分隔兩站的[閘機不會被拆除](../Page/閘機.md "wikilink")，使用[八達通或](../Page/八達通.md "wikilink")[港鐵都會票轉乘者於](../Page/港鐵都會票.md "wikilink")30分鐘內轉綫，會被分別視作一程車費或不扣除額外車程；而使用單程票的乘客則需要先出閘，然後再另購車票入閘（兩程車費會高於其他轉車站轉綫）。

2009年8月16日，隨著[九龍南綫通車](../Page/九龍南綫.md "wikilink")，尖沙咀站成為荃灣綫和[西鐵綫繼](../Page/西鐵綫.md "wikilink")[美孚站後的第二個轉乘站](../Page/美孚站.md "wikilink")。

MTR Tsim Sha Tsui Station Exit G.jpg | 尖沙咀站G出口（現已取消出口編號） MTR ETS TST.JPG
| 尖沙咀站大堂及麼地道行人隧道的交界處（現已取消出口編號）

### 加建國際廣場出入口

[地鐵公司](../Page/香港鐵路有限公司.md "wikilink")\[24\]與發展商合作興建新行人隧道連接於2006年拆卸重建的[香港凱悅酒店地底](../Page/香港凱悅酒店.md "wikilink")，大堂分別有H同R兩個出入口，直接接駁重建後的[國際廣場地底](../Page/國際廣場.md "wikilink")，在2009年12月17日啟用。\[25\]

### 改建A1出入口

港鐵公司於2012年年尾將A1出入口大規模改建，增設兩條扶手電梯及一部升降機。新建的升降機除連接車站大堂及地面外，亦可直達[九龍公園入口](../Page/九龍公園.md "wikilink")\[26\]。新出入口已於2016年5月7日啟用，工程期間的臨時出入口隨即封閉。受工程影響，3棵樹需被砍除，港鐵將重新栽種4棵新樹。\[27\]\[28\]

MTR Tsim Sha Tsui Station Exit A1 200907.jpg | 重建前的A1出入口 Entrance and
exit A1 of Tsim Sha Tsui Station under renovation in July
2015.JPG|A1出入口翻新工程（2015年7月） Entrance and exit A1 of Tsim Sha
Tsui Station under renovation in February 2016.jpg|A1出入口翻新工程（2016年2月）

### 更換升降機

往來大堂及月台升降機於2017年6月19日暫停使用，以更換為較新型號，並於2018年3月底重投服務\[29\]。

### 加建加拿分道行人隧道

[Tsim_Sha_Tsui_Exit_D_after_modification_2019.jpg](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_Exit_D_after_modification_2019.jpg "fig:Tsim_Sha_Tsui_Exit_D_after_modification_2019.jpg")
港鐵公司為疏導尖沙咀站南端大堂、麼地道行人隧道交匯處（前稱G出入口）及[加拿分道路面的人流](../Page/加拿分道.md "wikilink")，計劃在D出入口位置加建一條長約80米的行人隧道，增設D3出入口連接[K11購物藝術館地庫二層](../Page/K11_\(香港\).md "wikilink")，及同時重建D1及D2出入口，以提升其外觀設計，亦建造一條上行的扶手電梯連接D1/D2出入口。\[30\]工程已於2012年4月27日刊憲，2015年4月起展開所有相關工程，行人隧道連同新的D3出口已於2018年11月30日啟用。\[31\]

HK TST Carnarvon Road D2 MTR Exit EMPaST.jpg | 重建前的D2出入口

### 以時序記錄事故列表

#### 2014年

  - 2014年3月30日，因港鐵坐享巨額盈利，但年年加票價，結果令多個團體到尖沙咀站抗議及遞交請願信，要求港鐵重新計算載客量，增加班次，並促請政府檢討[可加可減機制](../Page/可加可減.md "wikilink")\[32\]。

#### 2015年

  - 5月9日晚上11時20分，近H出口一部「單程票售票機」被乘客發現冒煙，通知職員後報警。職員並打開售票機以滅火筒將煙撲熄，火警中無人受傷，亦毋須疏散。消防員調查，初步懷疑電路板過熱冒煙，並無可疑\[33\]。

<!-- end list -->

  - 5月11日，尖沙咀站A1出入口改建地盤發生工業意外，一輛挖泥機突然翻側，一名57歲釘板工人疑走避不及被壓傷腳，雙腳有多處骨折，送院搶救後留醫接受治療\[34\]。

#### 2017年

  - 2月10日晚上約7時15分，一列荃灣綫往荃灣列車（A113/A192）在金鐘站往尖沙咀站的隧道內被一名60歲乘客張錦輝[縱火](../Page/縱火.md "wikilink")，車長決定將列車駛至尖沙咀站1號月台，車廂內至少19名乘客被火燒傷，4人危殆，縱火者延至5月14日因器官衰竭不治。事發後港鐵疏散所有於尖沙咀站內的乘客，並緊急封鎖車站。荃灣線一度不停尖沙咀站，直至2017年2月11日早上才完全回復正常\[35\]。

<!-- end list -->

  - 2017年9月21日7時許，一名14歲女童乘搭港鐵時，疑遭一名姓[張](../Page/張.md "wikilink")(41歲)男子非禮\[36\]。

<!-- end list -->

  - 2017年11月1日，一名59歲外籍男子疑在尖沙咀站車廂內非禮一名27歲女子，後被帶署扣查\[37\]。

#### 2018年

  - 7月19日10時許，64歲[陳姓](../Page/陳.md "wikilink")[老人涉嫌在月台上非禮一名](../Page/老人.md "wikilink")22歲女子，經警方初步調查後帶走\[38\]。

<!-- end list -->

  - 7月28日，中國大陸學生月台便溺。身穿印有「China」[紋章制服的疑似中國大陸學生團](../Page/紋章.md "wikilink")，有人脫光下身在該站月台上大便。該[自由行學團老師向在場人士指他](../Page/自由行.md "wikilink")，無法控制情況，他管不了\[39\]。

<!-- end list -->

  - 8月10日，警方追捕中國籍男色狼。29歲[港女在金鐘站遭](../Page/港女.md "wikilink")31歲[林姓中國男子接觸了身體](../Page/林.md "wikilink")，[香港警察在尖沙咀站捉住該男子](../Page/香港警察.md "wikilink")，並以涉嫌[非禮](../Page/非禮.md "wikilink")，案件交由油尖警區[刑事調查隊跟進](../Page/刑事調查隊.md "wikilink")\[40\]。

<!-- end list -->

  - 8月11日，扶手電梯嚴重破毀事故。傍晚6時許，站內一條運行中的扶手電梯，突然發出巨響，隨即零件四散，梯級變形，梯梳彈出，現未知傷亡人數\[41\]。

## 未來發展

### 加建北面大堂和出入口

地鐵公司\[42\]於2007年6月27日宣佈進一步擴建尖沙咀站和增建出入口的計劃。計劃原建議將在尖沙咀站的月台北端機房的位置，以暗挖技術開闢一條全長200米、闊6米的新通道，在通道中央開設面積約500[平方米的新大堂](../Page/平方米.md "wikilink")。新大堂將接駁三個新出入口Q1、Q2、Q3，其中包括早前計劃連接東英大廈的出入口，以及連接[美麗華酒店及美麗華商場地庫的新出入口](../Page/美麗華酒店.md "wikilink")。

根據2010年6月30日的最新計劃，港鐵公司將會於現有尖沙咀站北端起，興建共長180米隧道，連接新建的衛星大堂及新Q出入口。施工期間，將分階段封閉[彌敦道的其中一條行車線](../Page/彌敦道.md "wikilink")。工程原預計於2010年11月20日動工，預計最快於2014年2月完工。但是該工程因不明原因而擱置。\[43\]\[44\]\[45\]

## 參考資料

## 外部連結

  - [港鐵公司－尖沙咀站／尖東站街道圖](http://www.mtr.com.hk/archive/ch/services/maps/tst.pdf)
  - [港鐵公司－尖沙咀站列車服務時間](http://www.mtr.com.hk/jplanner/images/timetables/tst.pdf)

[Category:尖沙咀](../Category/尖沙咀.md "wikilink")
[Category:前香港地鐵車站](../Category/前香港地鐵車站.md "wikilink")
[Category:1979年啟用的鐵路車站](../Category/1979年啟用的鐵路車站.md "wikilink")
[Category:荃灣綫車站](../Category/荃灣綫車站.md "wikilink")
[Category:油尖旺區鐵路車站](../Category/油尖旺區鐵路車站.md "wikilink")

1.

2.

3.
4.
5.
6.
7.

8.

9.

10.
11. [尖沙咀「水晶屋」](http://orientaldaily.on.cc/cnt/news/20140116/00176_062.html)
    《東方日後》 2014年1月16日

12. [港鐵尖沙咀站A1出入口化身全新「水晶屋」　荔景站新建升降機令出入更方便](http://www.mtr.com.hk/archive/corporate/en/press_release/PR-16-039-C.pdf)，[港鐵公司](../Page/港鐵公司.md "wikilink")，2016年5月6日

13. 升降機另可停靠U1層（[九龍公園入口](../Page/九龍公園.md "wikilink")）

14. 在[兩鐵合併前](../Page/兩鐵合併.md "wikilink")，港鐵及[港鐵公司的前稱分別為地鐵](../Page/香港鐵路有限公司.md "wikilink")（地下鐵路）及地鐵公司。

15.

16. [文匯報《正字正音》：「尖沙嘴」作「尖沙咀」
    原來大不同](http://paper.wenweipo.com/2012/11/06/ED1211060016.htm)

17.
18. 地鐵公司年報 1987年

19. 地鐵公司年報 1990年

20.

21.
22. 在兩鐵合併前，東鐵綫的前稱為**九廣東鐵**或簡稱**東鐵**，由九廣鐵路公司營運。

23.
24.
25.

26.

27.

28.

29.

30.

31. [港鐵尖沙咀站新設行人隧道及出入口　為市民帶來方便](http://www.mtr.com.hk/archive/corporate/en/press_release/PR-18-102-C.pdf)，2018年11月30日，港鐵公司

32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.

44.

45.