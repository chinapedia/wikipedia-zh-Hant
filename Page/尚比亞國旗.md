**尚比亞國旗**是一面纵横比例2：3，底部为[綠色](../Page/綠色.md "wikilink")
（象徵[自然資源](../Page/自然資源.md "wikilink")）的旗幟，右下角從左到右有紅
（象徵爭取[自由](../Page/自由.md "wikilink")）、黑
（象徵[人民](../Page/人民.md "wikilink")）、橙
（象徵[礦產資源](../Page/礦物.md "wikilink")）三條直條。其上有一兩翼舒張的雄鷹，象徵赞比亚[人民拥有克服困難的勇气和决心](../Page/人民.md "wikilink")。\[1\]
本旗採用於1964年10月24日，1996年10月17日稍作修改，将国旗底部的绿色深度改浅。

|                                                                                                                                                              |                                                                                                                              |                                                                                                                                              |                                                                                                                                              |
| :----------------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------: |
| [Flag_of_the_President_of_Zambia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_President_of_Zambia.svg "fig:Flag_of_the_President_of_Zambia.svg") | [Flag_of_Zambia_Police.png](https://zh.wikipedia.org/wiki/File:Flag_of_Zambia_Police.png "fig:Flag_of_Zambia_Police.png") | [Air_Force_Ensign_of_Zambia.svg](https://zh.wikipedia.org/wiki/File:Air_Force_Ensign_of_Zambia.svg "fig:Air_Force_Ensign_of_Zambia.svg") | [Civil_Air_Ensign_of_Zambia.svg](https://zh.wikipedia.org/wiki/File:Civil_Air_Ensign_of_Zambia.svg "fig:Civil_Air_Ensign_of_Zambia.svg") |
|                                                            [尚比亞總統旗幟](../Page/尚比亞總統.md "wikilink")                                                            |                                             [尚比亞警察旗幟](../Page/尚比亞.md "wikilink")                                             |                                                                   尚比亞空軍旗幟                                                                    |                                                                   尚比亞民航旗幟                                                                    |
|                                                                                                                                                              |                                                                                                                              |                                                                                                                                              |                                                                                                                                              |

### 历史国旗

#### 殖民地时期

#### 共和国时期

## 參考資料

[Category:尚比亞](../Category/尚比亞.md "wikilink")
[Z](../Category/國旗.md "wikilink")
[Category:1964年面世的旗幟](../Category/1964年面世的旗幟.md "wikilink")

1.  [Zambia at Flags of the World](http://flagspot.net/flags/zm.html)