**尚福林**（），男，汉族，[山东](../Page/山东.md "wikilink")[济南人](../Page/济南.md "wikilink")。北京财贸学院金融专业毕业，西南财经大学金融学博士。曾任[中国人民银行副行长](../Page/中国人民银行.md "wikilink")、[中国农业银行行长](../Page/中国农业银行.md "wikilink")、[中国证监会主席](../Page/中国证监会.md "wikilink")、[中国银监会主席](../Page/中国银监会.md "wikilink")、中国人民银行货币政策委员会委员。中共第十六屆中央候补委員，第十七、十八届中央委員。

## 生平

1951年出生在中国[济南](../Page/济南.md "wikilink")，1969年至1973年在军队服役，期间于1971年7月加入[中国共产党](../Page/中国共产党.md "wikilink")。1973年至1978年在[中国人民银行](../Page/中国人民银行.md "wikilink")[北京市](../Page/北京市.md "wikilink")[樱桃园分理处工作](../Page/樱桃园.md "wikilink")。1978年至1982年在[北京财贸学院金融专业学习](../Page/北京财贸学院.md "wikilink")。1993年考入[西南财经大学攻读](../Page/西南财经大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")，师从金融学家[曾康霖教授](../Page/曾康霖.md "wikilink")。1982年至1990年在中国人民银行总行综合计划司工作，历任副处长、处长。1994年任中国人民银行行长助理，1996年4月升至副行长，负责分管货币政策及支付清算工作。1997年7月任[中国人民银行货币政策委员会委员](../Page/中国人民银行货币政策委员会.md "wikilink")。2000年2月出任[中国农业银行行长](../Page/中国农业银行.md "wikilink")。2002年12月27日，接替[周小川出任](../Page/周小川.md "wikilink")[中国证监会主席](../Page/中国证监会.md "wikilink")。2011年10月29日，接替[刘明康转任](../Page/刘明康.md "wikilink")[中国银监会主席](../Page/中国银监会.md "wikilink")。2017年2月24日，卸任银监会主席，将到[全国政协经济委员会任职](../Page/全国政协.md "wikilink")。\[1\]2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[2\]。

## 参考文献

## 外部链接

  - [尚福林简历](http://www.gov.cn/misc/2005-07/20/content_16345.htm)　[中国政府网](../Page/中国政府网.md "wikilink")

{{-}}

[Category:中国银行业监督管理委员会主席](../Category/中国银行业监督管理委员会主席.md "wikilink")
[Category:中国证券监督管理委员会主席](../Category/中国证券监督管理委员会主席.md "wikilink")
[Category:中国人民银行副行长](../Category/中国人民银行副行长.md "wikilink")
[Category:中国人民银行行长助理](../Category/中国人民银行行长助理.md "wikilink")
[Category:中国人民银行货币政策委员会委员](../Category/中国人民银行货币政策委员会委员.md "wikilink")
[Category:中国农业银行行长](../Category/中国农业银行行长.md "wikilink")
[Category:中国共产党第十六届中央委员会候补委员](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中国共产党党员
(1971年入党)](../Category/中国共产党党员_\(1971年入党\).md "wikilink")
[Category:首都经济贸易大学校友](../Category/首都经济贸易大学校友.md "wikilink")
[Category:西南财经大学校友](../Category/西南财经大学校友.md "wikilink")
[Category:济南人](../Category/济南人.md "wikilink")
[F福林](../Category/尚姓.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.
2.