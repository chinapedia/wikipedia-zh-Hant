**尤代·乔普拉**（英語：**Uday
Chopra**，），亦被译作**烏代·曹帕拉**。他是一名[印度演員](../Page/印度.md "wikilink")。

他是著名的印度[導演](../Page/導演.md "wikilink")[雅什·曹帕拉](../Page/雅什·曹帕拉.md "wikilink")（Yash
Chopra）的兒子， 他的哥哥[阿迪提亞·喬普拉也是一個出名的導演](../Page/阿迪提亞·喬普拉.md "wikilink")。

## 出演電影

  - *[Dhoom 3](../Page/Dhoom_3.md "wikilink")* (2008年)...演阿里探員（Ali）
  - *[Dhoom 2](../Page/Dhoom_2.md "wikilink")* (2006年) ... 演阿里探員
  - *[Neal N Nikki](../Page/Neal_N_Nikki.md "wikilink")* (2005年) ...
    Neal
  - *[Dhoom](../Page/Dhoom.md "wikilink")* (2004年) ... 演阿里探員
  - *[Charas](../Page/Charas.md "wikilink")* (2004年) ... Fishy
  - *[Kal Ho Naa Ho](../Page/Kal_Ho_Naa_Ho.md "wikilink")* (2003年)
    ...Day 6 GUY on the bike special apearnce
  - *[Supari](../Page/Supari.md "wikilink")* (2003年) ... Chirkut
  - *[Mujhse Dosti
    Karoge\!](../Page/Mujhse_Dosti_Karoge!.md "wikilink")* (2002年) ...
    Rohan
  - *[Mere Yaar Ki Shaadi
    Hai](../Page/Mere_Yaar_Ki_Shaadi_Hai.md "wikilink")* (2002年) ...
    Sanju
  - *[Mohabbatein](../Page/Mohabbatein.md "wikilink")* (2000年) ...
    Vikram

## 外部連結

  -
[C](../Category/1973年出生.md "wikilink")
[C](../Category/在世人物.md "wikilink")
[Category:印度電影男演員](../Category/印度電影男演員.md "wikilink")