**尤泽比乌兹·斯莫拉雷克**（**Euzebiusz "Ebi" Smolarek**，
，），[波蘭足球運動員](../Page/波蘭.md "wikilink")，司職前鋒，效力[波蘭足球甲級聯賽球會](../Page/波蘭足球甲級聯賽.md "wikilink")[乔治罗尼亚](../Page/乔治罗尼亚比亚韦斯托克足球俱乐部.md "wikilink")。

出生於波蘭的[罗兹市](../Page/罗兹.md "wikilink")，父親是前足球員，曾出席過1982年和1986年世界杯。由於父親曾在球員時代在荷蘭打滾並擔任教練，施莫拿歷克受荷蘭足球影響較深，出道時是在[荷蘭班霸](../Page/荷蘭.md "wikilink")[飛燕諾展開](../Page/飛燕諾.md "wikilink")，一效力便是五年。2004年轉到鄰國[德國加盟](../Page/德國.md "wikilink")[多蒙特](../Page/多特蒙德足球俱樂部.md "wikilink")，兩季間上陣81場入25球。他也是波蘭國家隊核心，曾出席[2006年世界杯](../Page/2006年世界杯.md "wikilink")，可惜首圈出局。

2007年8月24日，施莫拿歷克與[西甲球會](../Page/西甲.md "wikilink")[桑坦德競技簽約](../Page/桑坦德競技.md "wikilink")，轉會費據報是480萬[歐元](../Page/歐元.md "wikilink")。2008年8月29日獲外借到[英超球隊](../Page/英超.md "wikilink")[保頓一個球季](../Page/博尔顿足球俱乐部.md "wikilink")，如表現理想，更可優先買斷<small>\[1\]</small>。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [尤泽比乌兹·斯莫拉雷克](http://www.90minut.pl/kariera.php?id=3917)
    (90minut.pl)

  - [尤泽比乌兹·斯莫拉雷克](http://www.ebismolarek.com/) (ebismolarek.com)

[Category:波蘭足球運動員](../Category/波蘭足球運動員.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:多蒙特球員](../Category/多蒙特球員.md "wikilink")
[Category:桑坦德球員](../Category/桑坦德球員.md "wikilink")
[Category:保頓球員](../Category/保頓球員.md "wikilink")
[Category:卡華拿球員](../Category/卡華拿球員.md "wikilink")
[Category:ADO海牙球員](../Category/ADO海牙球員.md "wikilink")
[Category:喬治羅尼亞球員](../Category/喬治羅尼亞球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")

1.  [Bolton bring in Smolarek on
    loan](http://news.bbc.co.uk/sport2/hi/football/teams/b/bolton_wanderers/7589307.stm)