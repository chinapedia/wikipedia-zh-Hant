**尤金·艾萨克·迈耶**（，）是一位[美国金融家](../Page/美国.md "wikilink")。

## 生平

1875年出生在[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[洛杉矶](../Page/洛杉矶.md "wikilink")，曾任《[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")》出版人、[美国联邦储备委员会主席和](../Page/美国联邦储备委员会主席.md "wikilink")[世界银行行长](../Page/世界银行行长.md "wikilink")。

## 外部链接

  - [Biography of Eugene Meyer
    (website)](http://web.worldbank.org/WBSITE/EXTERNAL/EXTABOUTUS/EXTARCHIVES/0,,contentMDK:20487099~pagePK:36726~piPK:437378~theSitePK:29506,00.html)
  - [Statements and Speeches of Eugene
    Meyer](https://fraser.stlouisfed.org/title/3775)
  - [Selections from the Eugene Meyer Papers relating to the Federal
    Reserve](https://fraser.stlouisfed.org/archival/4951), from the
    Library of Congress

[Category:美国联邦储备委员会主席](../Category/美国联邦储备委员会主席.md "wikilink")
[Category:世界银行行长](../Category/世界银行行长.md "wikilink")
[Category:美国投资者](../Category/美国投资者.md "wikilink")
[Category:美国金融家](../Category/美国金融家.md "wikilink")
[Category:法国裔美国人](../Category/法国裔美国人.md "wikilink")
[Category:奥地利裔美国人](../Category/奥地利裔美国人.md "wikilink")
[Category:美国犹太人](../Category/美国犹太人.md "wikilink")
[Category:洛杉矶人](../Category/洛杉矶人.md "wikilink")
[Category:美国企业家](../Category/美国企业家.md "wikilink")
[Category:耶鲁大学校友](../Category/耶鲁大学校友.md "wikilink")
[Category:华盛顿哥伦比亚特区共和党人](../Category/华盛顿哥伦比亚特区共和党人.md "wikilink")
[Category:华盛顿邮报人物](../Category/华盛顿邮报人物.md "wikilink")