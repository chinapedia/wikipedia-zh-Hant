**尤金·保羅·維格納**（，）原名**維格納·帕爾·耶諾**（），[匈牙利](../Page/匈牙利.md "wikilink")-[美国理論](../Page/美国.md "wikilink")[物理學家及](../Page/物理學家.md "wikilink")[數學家](../Page/數學家.md "wikilink")，奠定了[量子力學](../Page/量子力學.md "wikilink")[對稱性的理論基礎](../Page/對稱性_\(物理學\).md "wikilink")，在[原子核結構的研究上有重要貢獻](../Page/原子核.md "wikilink")。\[1\]
他在純數學領域也有許多重要工作，許多[數學定理以其命名](../Page/定理.md "wikilink")。其中[維格納定理是](../Page/維格納定理.md "wikilink")[量子力學數學表述的重要基石](../Page/量子力學的數學表述.md "wikilink")。維格納首先發現了核反應器中的[氙-135帶有毒性](../Page/氙.md "wikilink")，這也是為何這種毒性有時被稱作「維格納毒性」。

1963年，由於「在[原子核和基本粒子物理理論上的貢獻](../Page/原子核.md "wikilink")，尤其是基本對稱原理的發現與應用」，維格納和[瑪麗亞·格佩特-梅耶](../Page/瑪麗亞·格佩特-梅耶.md "wikilink")、[約翰內斯·延森一同獲得](../Page/約翰內斯·延森.md "wikilink")[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")。\[2\]

## 早年與求學

[Heisenberg,W._Wigner,E._1928.jpg](https://zh.wikipedia.org/wiki/File:Heisenberg,W._Wigner,E._1928.jpg "fig:Heisenberg,W._Wigner,E._1928.jpg")和尤金·维格纳（1928年）。\]\]

維格納·帕爾·耶諾1902年出生於[奥匈帝国](../Page/奥匈帝国.md "wikilink")[布達佩斯的一個](../Page/布達佩斯.md "wikilink")[猶太中產家庭](../Page/猶太.md "wikilink")。他的雙親是製皮工人，有一位姊姊、一位妹妹。這位妹妹後來嫁給了[英國物理學家](../Page/英國.md "wikilink")[保羅·狄拉克](../Page/保羅·狄拉克.md "wikilink")。九歲以前維格納在家裡由家庭教師指導，他這段期間培養了對數學問題的興趣。十一歲時被診斷出肺結核，被父母送到[奧地利的療養院](../Page/奧地利.md "wikilink")，直到六個星期後發現為誤診。

1920年維格納進入[布達佩斯科技經濟大學就讀](../Page/布達佩斯科技經濟大學.md "wikilink")。然而他對那裡提供的課程並不滿意，因此1921年轉入[柏林的一間工程學院](../Page/柏林.md "wikilink")（現在的[柏林工業大學](../Page/柏林工業大學.md "wikilink")）就讀，學習[化學工程](../Page/化學工程.md "wikilink")。
另外他也常參加[德國物理學會在星期三下午的學術討論會](../Page/德國物理學會.md "wikilink")。這個聚會聚集了如[馬克斯·普朗克](../Page/馬克斯·普朗克.md "wikilink")、[馬克斯·馮·勞厄](../Page/馬克斯·馮·勞厄.md "wikilink")、、[維爾納·海森堡](../Page/維爾納·海森堡.md "wikilink")、[瓦爾特·能斯特](../Page/瓦爾特·能斯特.md "wikilink")、[沃爾夫岡·包立](../Page/沃爾夫岡·包立.md "wikilink")、和[阿爾伯特·愛因斯坦等許多知名學者](../Page/阿爾伯特·愛因斯坦.md "wikilink")。
隨後維格納進入威廉皇帝物理化學和電化學研究所（現在的），並在那遇上了他的指導教授[麥可·波拉尼](../Page/迈克·波拉尼.md "wikilink")。在其指導下完成了學位論文「分子的形成與分解」（Bildung
und Zerfall von Molekülen）。

## 中年

維格納完成學業回到布達佩斯，並且在他父親的製革廠工作。但在1926年在其導師波拉尼的推薦下，他開始擔任威廉皇帝研究所（今[馬克斯·普朗克物理學研究所](../Page/馬克斯·普朗克物理學研究所.md "wikilink")）物理學家的助手，協助魏森伯格在[X射線](../Page/X射線.md "wikilink")[晶體學的研究](../Page/晶體學.md "wikilink")。六個月的助手工作後，維格納在底下工作。維格納也開始學習[量子力學](../Page/量子力學.md "wikilink")，接觸[埃爾溫·薛丁格的論文](../Page/埃爾溫·薛丁格.md "wikilink")，並深入研究[群論](../Page/群論.md "wikilink")。收到了[阿諾·索末菲的邀請](../Page/阿諾·索末菲.md "wikilink")，維格納前往[哥廷根擔任數學家](../Page/哥廷根.md "wikilink")[大衛·希爾伯特的助手](../Page/大衛·希爾伯特.md "wikilink")。然而希爾伯特已將研究的重心轉移到邏輯，對此感到失望的維格納只能獨立研究。他建立了量子力學中對稱性的理論基礎，在1927年寫下了。\[3\]他和數學家[赫爾曼·外爾將數學中的群論帶進了量子力學](../Page/赫爾曼·外爾.md "wikilink")。外爾寫了一本著作《群論與量子力學》（Group
Theory and Quantum
Mechanics，1928年），然而這本書對當時年輕的物理學者而言並不容易理解。而維格納的《群論與其在原子光譜量子力學的應用》（Group
Theory and Its Application to the Quantum Mechanics of Atomic
Spectra，1931年）讓群論被更多人所瞭解。

此時維格納已經在物理學界受到廣泛的注意。1930年，[普林斯頓大學邀請維格納作一年的學術訪問](../Page/普林斯頓大學.md "wikilink")，當時也一起邀請了數學家[馮·諾伊曼](../Page/約翰·馮·諾伊曼.md "wikilink")。在這之前兩人就已經合作了三篇文章。他們分別把名字改成尤金和約翰。一年過後，普林斯頓大學提供了一份為期五年的合約，一年中半年的時間在普林斯頓做學術研究。在這期間[納粹在德國迅速崛起](../Page/納粹主義.md "wikilink")。1934年，維格納將他的妹妹曼琪（Manci）介紹給了物理學家[保羅·狄拉克](../Page/保羅·狄拉克.md "wikilink")。曼琪在1937年嫁給了狄拉克。1936合約到期後，普林斯頓並未繼續聘用維格納。經由介紹，他在[威斯康辛大學找到新的職位](../Page/威斯康辛大學麥迪遜分校.md "wikilink")，並在那認識了他的第一任妻子愛蜜莉亞·法蘭克（Amelia
Frank）。愛蜜莉亞是威斯康辛大學物理系的學生，然而她卻在1937年匆匆離開人世。傷心的維格納在1938年回到普林斯頓任職。維格納於1937年歸化美國籍，並將雙親帶到美國定居。

## 曼哈頓計劃

[EugeneWignerAlvinWeinberg.jpg](https://zh.wikipedia.org/wiki/File:EugeneWignerAlvinWeinberg.jpg "fig:EugeneWignerAlvinWeinberg.jpg")。\]\]

1939年8月2日，維格納將[利奧·西拉德介紹給愛因斯坦](../Page/利奧·西拉德.md "wikilink")，促成了歷史上知名的[愛因斯坦—西拉德信](../Page/愛因斯坦—西拉德信.md "wikilink")。這封寫給美國總統[羅斯福的信件促使美國啟動研發](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")[原子彈的](../Page/原子彈.md "wikilink")[曼哈頓計劃](../Page/曼哈頓計劃.md "wikilink")。

1941年7月4日，維格納和他的第二任妻子瑪麗·安妮特·惠勒（Mary Annette
Wheeler）結婚。瑪麗是[瓦薩學院的物理學教授](../Page/瓦薩學院.md "wikilink")，1932年畢業於[耶魯大學](../Page/耶魯大學.md "wikilink")。他們的婚姻一直到瑪麗於1977年過世。\[4\]兩人育有兩個孩子。\[5\]

曼哈頓計劃期間維格納領導了一個團隊，成員包括[阿爾文·溫伯格](../Page/阿爾文·溫伯格.md "wikilink")、[凱瑟琳·威](../Page/凱瑟琳·威.md "wikilink")、蓋爾·楊、和。他們的任務是設計讓[鈾產生衰變的核子反應爐](../Page/鈾.md "wikilink")。在當時，反應爐設計還只停留於於紙上作業，從未真正被建造出來。1942年7月，維格納選擇了具有[石墨](../Page/石墨.md "wikilink")[中子減速劑和水冷系統的設計](../Page/中子減速劑.md "wikilink")。
1942年12月2日，位於[芝加哥大學原子反應爐](../Page/芝加哥大學.md "wikilink")[芝加哥1號堆成功進行了歷史上第一次人為的](../Page/芝加哥1號堆.md "wikilink")[核連鎖反應](../Page/核連鎖反應.md "wikilink")，維格納也參與了這次的實驗。\[6\]維格納未曾後悔投入曼哈頓計劃，有時甚至希望原子彈能早一年被創造出來。

## 戰後和晚年

1945年維格納接受了柯林頓實驗室（現在的[橡樹嶺國家實驗室](../Page/橡樹嶺國家實驗室.md "wikilink")）的研究指導一職。當1947年新成立的[美國原子能委員會接掌實驗室的營運後](../Page/美國原子能委員會.md "wikilink")，維格納憂心許多技術決定都將取決於[華府](../Page/華府.md "wikilink")。\[7\]
他也看見戰爭時期作為警衛的軍隊仍持續著駐守，作為「愛管閒事的監督」干擾著研究。\[8\]
有感於被邊緣化的角色，維格納在1947年離開橡樹嶺回到普林斯頓大學。在這之後的許多年裡他仍擔任顧問。
\[9\]
在戰後維格納服務於許多政府機構，包括1947到1951年於[國家標準技術研究所](../Page/國家標準技術研究所.md "wikilink")、1951到1954年於[美國國家科學研究委員會數學部門](../Page/美國國家科學研究委員會.md "wikilink")、[國家科學基金會物理部門](../Page/國家科學基金會.md "wikilink")、以及1952到1957年和1959到1964年於美國原子能委員會中的顧問委員會。

1963年，維格納獲頒[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")。他個人聲稱從未預料到會獲獎：「我從沒料想到我會在沒做壞事的情況下登上報紙。」在此之前他也獲得了1958年的[恩里科·費米獎](../Page/恩里科·費米獎.md "wikilink")，以及之後獲得1969年的[美國國家科學獎章](../Page/美國國家科學獎章.md "wikilink")。1968年出席了[約西亞·吉布斯講座](../Page/約西亞·吉布斯.md "wikilink")。\[10\]\[11\]

1992年，以九十歲的高齡，與傳記作家合作出版了傳記《亂世學人——維格納自傳》（*The Recollections of Eugene P.
Wigner*）。三年後維格納在[新泽西州](../Page/新泽西州.md "wikilink")[普林斯顿過世](../Page/普林斯顿_\(新泽西州\).md "wikilink")。

## 参考资料

## 參考書目

  -
## 外部链接

  -
  -
  - [诺贝尔官方网站-尤金·维格纳简介](http://nobelprize.org/nobel_prizes/physics/laureates/1963/wigner-bio.html)

[Category:匈牙利物理学家](../Category/匈牙利物理学家.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:恩里科·費米獎獲獎者](../Category/恩里科·費米獎獲獎者.md "wikilink")
[Category:柏林工業大學校友](../Category/柏林工業大學校友.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:马克斯·普朗克奖章获得者](../Category/马克斯·普朗克奖章获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦奖获得者](../Category/阿尔伯特·爱因斯坦奖获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")
[Category:橡树岭国家实验室人物](../Category/橡树岭国家实验室人物.md "wikilink")

1.  Wightman, A.S. (1995) *[Eugene Paul
    Wigner 1902–1995](http://www.ams.org/notices/199507/wigner.pdf)*,
    Notices of the American Mathematical Society **42**(7), 769–771.

2.

3.

4.

5.

6.

7.

8.

9.
10. [Josiah Willard Gibbs
    Lectures](http://www.ams.org/meetings/lectures/meet-gibbs-lect)

11.