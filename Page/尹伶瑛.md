**尹伶瑛**（**Yin Ling-
ying**，）是[中華民國](../Page/中華民國.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，[臺灣外省人第二代](../Page/臺灣外省人.md "wikilink")，[原籍](../Page/籍貫.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")。尹出生於[臺灣](../Page/臺灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")，自[國立中正大學社會福利研究所取得](../Page/國立中正大學.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")，[凱達格蘭學校第一期](../Page/凱達格蘭學校.md "wikilink")「女性公共事務領導班」結業，前任[臺灣團結聯盟籍](../Page/臺灣團結聯盟.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")；並曾任[雲林縣議員](../Page/雲林縣議員.md "wikilink")、「雲林縣阿扁之友會」副總會長、「水噹噹婦女聯盟」斗六店館長、雲林縣[林內鄉湖本村村長](../Page/林內鄉.md "wikilink")、「雲林縣社區希望聯盟」理事、「雲林縣歷史建築協會」理事、「雲林縣生命線協會」理事長等職。[2012年立法委員選舉](../Page/2012年立法委員選舉.md "wikilink")，尹名列[民主進步黨](../Page/民主進步黨.md "wikilink")[全國不分區立委提名名單](../Page/全國不分區.md "wikilink")，惟並未當選。

## 簡歷

尹伶瑛曾任[雲林縣議員](../Page/雲林縣議員.md "wikilink")，積極參與[生態與](../Page/生態.md "wikilink")[環保議題](../Page/環保.md "wikilink")。尹曾在雲林縣議員任內揭發[林內鄉](../Page/林內鄉.md "wikilink")[焚化爐弊案](../Page/焚化爐.md "wikilink")\[1\]。而由林內鄉湖本村村長任內，直到擔任立法委員，大力呼籲搶救當地的珍貴稀有[保育類動物](../Page/臺灣保育物種列表.md "wikilink")[八色鳥](../Page/八色鳥.md "wikilink")，爭取將雲林國有林班地劃設公告為「重要棲息環境」\[2\]\[3\]，反對開放[進口](../Page/進口.md "wikilink")[中國大陸砂石](../Page/中國大陸.md "wikilink")，打響知名度，被稱為「鳥仔瑛」\[4\]。

[2004年立法委員選舉中](../Page/2004年中華民國立法委員選舉.md "wikilink")，尹代表台聯當選雲林縣立委。但在2007年，台聯黨內有人質疑[百萬人民倒扁運動](../Page/百萬人民倒扁運動.md "wikilink")[紅衫軍涉入黨務](../Page/紅衫軍.md "wikilink")、台聯背離原本理想及與民進黨走太近，而遭開除黨籍；為了聲援遭開除黨籍的[廖本煙及](../Page/廖本煙.md "wikilink")[黃宗源](../Page/黃宗源.md "wikilink")，尹質疑台聯黨中央引進紅衫軍以及未按黨綱處理開除黨務，與[台北市立委](../Page/台北市.md "wikilink")[黃適卓共同聲援](../Page/黃適卓.md "wikilink")。之後，尹與台聯雲林縣黨部主委等一百零一位雲林黨員公開宣布[退黨](../Page/退黨.md "wikilink")。尹與黃退出台聯之後的一週內，台聯[民代陸續退黨](../Page/民意代表.md "wikilink")，形成台聯創黨以來最大規模的退黨潮。

退出台聯之後，尹變成[無黨籍](../Page/無黨籍.md "wikilink")。2007年11月15日上午，[民主進步黨](../Page/民主進步黨.md "wikilink")（民進黨）立法院黨團總召[柯建銘](../Page/柯建銘.md "wikilink")、幹事長[王拓與書記長](../Page/王拓.md "wikilink")[王幸男共同迎接廖本煙](../Page/王幸男.md "wikilink")、黃宗源、尹伶瑛與黃適卓加入民進黨立法院黨團；尹在該場[記者會中說](../Page/記者會.md "wikilink")，她與參選雲林縣第二選區立委的民進黨雲林縣議員[劉建國有](../Page/劉建國.md "wikilink")「重覆選區」問題，呼籲劉趕緊在該日中午前簽署由[民進黨秘書長](../Page/民進黨秘書長.md "wikilink")[卓榮泰](../Page/卓榮泰.md "wikilink")[傳真送來的](../Page/傳真.md "wikilink")[民調同意書](../Page/民調.md "wikilink")，以民調決定誰代表[泛綠角逐雲林縣第二選區立委](../Page/泛綠.md "wikilink")；尹還說，只要劉在同意書上簽名，她就馬上入黨。2007年11月26日11時，尹夫[廖健良及長子](../Page/廖健良.md "wikilink")[廖貫廷至民進黨雲林縣黨部辦理入黨](../Page/廖貫廷.md "wikilink")，廖健良入黨申請表填寫的介紹人是當時[行政院長](../Page/行政院長.md "wikilink")[張俊雄](../Page/張俊雄.md "wikilink")；並未入黨的尹批評，劉建國不僅不願簽署民調同意書，反而要求她先入黨，意圖害她因「入黨未滿一年」被依黨章取消提名參選資格，這是「自稱正班綠營的人士把民調當作一場遊戲，難怪黨內初選前後頻頻傳出[買票與民調作假傳聞](../Page/買票.md "wikilink")」。\[5\]

2008年1月2日，尹在雲林縣第二選區公辦政見發表會中演講時，對劉建國說「過去大家都知道你有[黑道背景](../Page/黑道.md "wikilink")」等語，並手持[雲林地方法院](../Page/雲林地方法院.md "wikilink")[判決書稱劉曾在](../Page/判決書.md "wikilink")2001年涉及[妨害自由](../Page/妨害自由.md "wikilink")、違反《[槍砲彈藥刀械管制條例](../Page/槍砲彈藥刀械管制條例.md "wikilink")》等案件。2008年1月7日上午，前民進黨立委[林國華](../Page/林國華_\(臺灣政治人物\).md "wikilink")、[林中禮及民進黨](../Page/林中禮.md "wikilink")[雲林縣議會黨團總召集人](../Page/雲林縣議會.md "wikilink")[李建昇主持劉建國競選總部記者會](../Page/李建昇.md "wikilink")；他們批評，尹連續謾罵民進黨、中傷劉，民進黨和劉一直忍讓，尹卻變本加厲，因此他們決定發起連署要求尹退選；同日，尹支持者在尹競選總部扮演「討債集團」，手持[球棒砸毀一部](../Page/球棒.md "wikilink")「阿國牌嘛仔台」（阿國牌[賭博性](../Page/賭博.md "wikilink")[大型電玩](../Page/大型電玩.md "wikilink")），諷刺劉是[黑道](../Page/黑道.md "wikilink")；尹競選總部僱用[吊車](../Page/吊車.md "wikilink")，拉起寫著「呣通投黑道」（不要投票給黑道）五字的[帆布](../Page/帆布.md "wikilink")；同日，陳姓[農民在尹競選總部指控](../Page/農民.md "wikilink")，2006年，其[農田](../Page/農田.md "wikilink")[電纜遭竊](../Page/電纜.md "wikilink")，竊賊被[逮捕](../Page/逮捕.md "wikilink")，時任雲林縣議員的劉卻[關說](../Page/關說.md "wikilink")[警方釋放竊賊](../Page/警方.md "wikilink")。劉競選總部強調，一定會控告尹陳二人。\[6\]\[7\]

2008年1月8日，[雲林縣長](../Page/雲林縣長.md "wikilink")[蘇治芬與](../Page/蘇治芬.md "wikilink")[古坑鄉鄉長](../Page/古坑鄉.md "wikilink")[林慧如召開](../Page/林慧如.md "wikilink")「不要再傷害所有的台灣媽媽」[記者會聲援劉建國](../Page/記者會.md "wikilink")，呼籲尹不要為了選舉私利而扭曲了人的基本價值；蘇並說：「如果劉建國是黑道，那我就退出政壇。」尹則召開記者會哭訴，她自從參選以來一再遭打壓，但她絕不屈服、更不會因而退選，劉應承認過去參與黑道所造成之社會負面影響、並向社會道歉以獲得重生，「如果劉建國不是黑道，我尹伶瑛退出政壇。」\[8\]\[9\]

2008年1月9日，劉建國拆除尹的「呣通投黑道」帆布，批尹是「加重誹謗連續犯」；同日23時，尹欲再度掛上「呣通投黑道」帆布，劉欲拆除，雙方發生肢體衝突，尹的長子廖貫廷被打到[腦震盪](../Page/腦震盪.md "wikilink")，劉的林姓-{志}-工被打到眼部出血。劉先稱尹把林打到眼部出血，後在2008年1月10日凌晨率領劉的支持者在現場[靜坐抗議尹](../Page/靜坐_\(示威\).md "wikilink")。2008年1月10日上午，[民進黨主席](../Page/民進黨主席.md "wikilink")[陳水扁在雲林縣](../Page/陳水扁.md "wikilink")[斗南鎮德化堂前要求](../Page/斗南鎮.md "wikilink")[選民集中](../Page/選民.md "wikilink")[投票給最有可能勝選的劉](../Page/投票.md "wikilink")，不能「分票」（分散投票）給尹；尹及其支持者在德化堂外拉起「呣通投黑道」帆布，尹本人也向陳高喊「呣通投黑道」，諷刺劉有黑道背景，呼籲陳切勿為黑道助選；尹與廖貫廷至[雲林地檢署](../Page/雲林地檢署.md "wikilink")，按鈴控告劉毆打她們母子，並控告[雲林縣選舉委員會總幹事](../Page/雲林縣選舉委員會.md "wikilink")[丁彥哲違反](../Page/丁彥哲.md "wikilink")《[公職人員選舉罷免法](../Page/公職人員選舉罷免法.md "wikilink")》；同日中午，劉競選總部召開記者會，指控尹競選總部的重要幹部有黑道[議員](../Page/議員.md "wikilink")、開[電玩店的](../Page/電玩.md "wikilink")[市民代表](../Page/市民代表.md "wikilink")、自稱「[張（榮味）系人馬](../Page/張榮味.md "wikilink")」的鎮長等，質疑尹收受前雲林縣長[張榮味的好處而成為](../Page/張榮味.md "wikilink")[國民黨雲林縣第二選區立委候選人](../Page/國民黨.md "wikilink")[張碩文的打手](../Page/張碩文.md "wikilink")。\[10\]
劉至雲林地檢署控告尹\[11\]。開票結果，尹劉二人皆落選。2009年3月3日，雲林地方法院一審判決\[12\]，尹被判[有期徒刑四個月](../Page/有期徒刑.md "wikilink")；劉稱司法還他清白，他不是黑道，所以尹被判有罪\[13\]。尹不服一審判決，向[台南高分院提起上訴](../Page/台南高分院.md "wikilink")。2009年8月11日，台南高分院二審判決，尹無罪；二審裁判書\[14\]
稱，尹指控劉有黑道背景與涉及妨害自由、違反《槍砲彈藥刀械管制條例》等案件，並非全然無據或憑空捏造。

2011年6月29日，民進黨第14屆中央執行委員會（中執會）第15次會議通過[第八屆全國不分區立法委員候選人名單](../Page/2012年中華民國立法委員選舉.md "wikilink")，尹伶瑛名列第31順位，與第10順位的[段宜康同為](../Page/段宜康.md "wikilink")[外省籍代表](../Page/外省籍.md "wikilink")。選舉結果，尹落選。

## 政治

### 2004年立法委員選舉

| 2004年雲林縣選舉區立法委員選舉結果 |
| ------------------- |
| 應選6席                |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |
| 5                   |
| 6                   |
| 7                   |
| 8                   |
| 9                   |
| 10                  |
| 11                  |
| 12                  |
| 13                  |
| 14                  |
| 15                  |
| 16                  |
| 17                  |
| 18                  |

### 2008年第七屆區域立法委員選舉

  - 雲林縣第二選區（[崙背鄉](../Page/崙背鄉.md "wikilink")、[二崙鄉](../Page/二崙鄉.md "wikilink")、[西螺鎮](../Page/西螺鎮.md "wikilink")、[莿桐鄉](../Page/莿桐鄉.md "wikilink")、[林內鄉](../Page/林內鄉.md "wikilink")、[斗六市](../Page/斗六市.md "wikilink")、[大埤鄉](../Page/大埤鄉.md "wikilink")、[斗南鎮](../Page/斗南鎮.md "wikilink")、[古坑鄉](../Page/古坑鄉.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>尹伶瑛</p></td>
<td><p>-</p></td>
<td><p>17,282</p></td>
<td><p>10.73%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/張碩文.md" title="wikilink">張碩文</a></p></td>
<td></td>
<td><p>79,138</p></td>
<td><p>49.11%</p></td>
<td><div align="center">
<p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/黃國華.md" title="wikilink">黃國華</a></p></td>
<td></td>
<td><p>499</p></td>
<td><p>0.31%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/張昆煌.md" title="wikilink">張昆煌</a></p></td>
<td><p><a href="../Page/大道慈悲濟世黨.md" title="wikilink">大道慈悲濟世黨</a></p></td>
<td><p>454</p></td>
<td><p>0.28%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/廖珪如.md" title="wikilink">廖珪如</a></p></td>
<td></td>
<td><p>2,055</p></td>
<td><p>1.28%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/劉建國.md" title="wikilink">劉建國</a></p></td>
<td></td>
<td><p>61,703</p></td>
<td><p>38.29%</p></td>
<td></td>
</tr>
</tbody>
</table>

## 資料來源

<references/>

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00012&stage=6)
  - [尹伶瑛官方部落格](https://web.archive.org/web/20080112182145/http://tw.myblog.yahoo.com/gogo101-gogo101/)

[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第15屆雲林縣議員](../Category/第15屆雲林縣議員.md "wikilink")
[Category:前台灣團結聯盟黨員](../Category/前台灣團結聯盟黨員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:臺灣女性政治人物](../Category/臺灣女性政治人物.md "wikilink")
[Category:凱達格蘭學校校友](../Category/凱達格蘭學校校友.md "wikilink")
[Category:國立中正大學校友](../Category/國立中正大學校友.md "wikilink")
[Category:雲林人](../Category/雲林人.md "wikilink")
[Category:湖南裔台灣人](../Category/湖南裔台灣人.md "wikilink")
[Category:花蓮人](../Category/花蓮人.md "wikilink")
[L伶](../Category/尹姓.md "wikilink")

1.  [敬請連署「反對暴力威脅，支持環保議員尹伶瑛」](http://news.e-info.org.tw/against/2004/ag04082401.htm)

2.  [從八色鳥看台灣生態保育的困境](http://www.ecotour.org.tw/discuss.asp?OP=SHOWCONTENT&RECID=86)

3.  [保育八色鳥
    擬公告棲息環境](http://www.ecc.ntnu.edu.tw/~ecc/modules/news/print.php?storyid=14)
4.  魯永明，〈尹伶瑛 鬥-{志}-旺盛
    背水一戰〉，《[聯合報](../Page/聯合報.md "wikilink")》2007年11月24日C2版。
5.  尹伶瑛競選總部，〈新聞稿：尹伶瑛夫廖健良入民進黨〉，2007年11月26日。
6.  地方中心記者 連線報導，〈雲林泛綠鬩牆 總部宣傳車對嗆〉，《聯合報》2008年1月8日。
7.  鐘武達 雲林報導，[〈尹伶瑛、劉建國
    隔街互嗆火力猛〉](http://forums.chinatimes.com/report/vote2007/allnews/20080108062.htm)，《[中國時報](../Page/中國時報.md "wikilink")》2008年1月8日。
8.  蔡青峰
    雲林報導，[〈拚立委／「我兒非黑道」劉建國七旬老母助選哭昏〉](http://www.nownews.com/2008/01/08/91-2213967.htm)，《[東森新聞報](../Page/東森新聞報.md "wikilink")》2008年1月8日。
9.  陳金北
    雲林報導，[〈拚立委／黑道指控　劉建國槓上尹伶瑛　老母暈倒送醫〉](http://www.nownews.com/2008/01/09/123-2214122.htm)，《東森新聞報》2008年1月9日。
10. [〈雲林兩派
    毆人互咬〉](http://tw.nextmedia.com/applenews/article/art_id/30160608/IssueID/20080111)，《[蘋果日報
    (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink")》2008年1月11日。
11. 起訴案號：臺灣雲林地方法院檢察署97年度選偵字第216號
12. 起訴案號：臺灣雲林地方法院97年度訴字第1322號
13. 〈尹伶瑛、劉建國黑道爭論將再上演〉，《[民眾日報](../Page/民眾日報.md "wikilink")》2009年8月13日。
14. 裁判字號：台灣高等法院台南分院刑事判決98年度上訴字第426號。二審裁判書中，「甲」指尹伶瑛，「戊」指劉建國。