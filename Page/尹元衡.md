**尹元衡**（****，），[字](../Page/表字.md "wikilink")**彥平**，[朝鮮王朝政治人物](../Page/朝鮮王朝.md "wikilink")，字**彦平**（언평），[本貫](../Page/本貫.md "wikilink")[坡平尹氏](../Page/坡平尹氏.md "wikilink")，[父親為](../Page/父親.md "wikilink")[尹之任](../Page/尹之任.md "wikilink")，[母親李氏為](../Page/母親.md "wikilink")[李德崇之女](../Page/李德崇.md "wikilink")。他是[文定王后的弟弟](../Page/文定王后.md "wikilink")，是[小尹派的代表人物](../Page/小尹派.md "wikilink")。

## 生平

尹元衡是[尹之任的第五子](../Page/尹之任.md "wikilink")。尹元衡於1528年[小科及第](../Page/小科.md "wikilink")，為[承候官](../Page/承候官.md "wikilink")，[朝鮮中宗在位時](../Page/朝鮮中宗.md "wikilink")，尹元衡的[小尹派支持文定王后之子](../Page/小尹派.md "wikilink")[慶原大君](../Page/慶原大君.md "wikilink")，與支持[章敬王后所生的](../Page/章敬王后.md "wikilink")[世子李峼](../Page/朝鮮仁宗.md "wikilink")、以[尹任為首的](../Page/尹任.md "wikilink")[大尹派對立](../Page/大尹派.md "wikilink")。中宗死後，世子即位，是為[仁宗](../Page/朝鮮仁宗.md "wikilink")，但不到一年就去世，由其異母弟慶原大君繼位，是為[明宗](../Page/朝鮮明宗.md "wikilink")，此時發生[乙巳士禍](../Page/乙巳士禍.md "wikilink")，尹元衡等人立大功，被封二等衛社功臣，因明宗年幼故由文定王后[垂簾聽政](../Page/垂簾聽政.md "wikilink")，尹元衡勢力正大，在其[正室金氏](../Page/正室.md "wikilink")（[金安老之](../Page/金安老.md "wikilink")[姪女](../Page/姪女.md "wikilink")）死後，尹元衡官拜[領議政](../Page/領議政.md "wikilink")，其妾[鄭蘭貞因為幫助小尹派](../Page/鄭蘭貞.md "wikilink")，獲文定王后破例[扶正為尹元衡的正妻](../Page/扶正.md "wikilink")，封正一品[貞敬夫人](../Page/貞敬夫人.md "wikilink")，明宗十五年二月賜封瑞原[府院君](../Page/府院君.md "wikilink")。文定王后在1565年過世，明宗受到朝臣的逼迫，清算[舅父尹元衡](../Page/舅父.md "wikilink")，其官職、封爵一併追奪，把尹元衡、鄭蘭貞[夫婦流放](../Page/夫婦.md "wikilink")。鄭蘭貞服毒自盡，尹元衡本人亦飲恨而終。\[1\]\[2\]

## 相关影视作品及饰演者

  - 《[朝鮮王朝五百年](../Page/朝鮮王朝五百年.md "wikilink")》—《》：1984年[MBC電視劇](../Page/文化广播_\(韓國\).md "wikilink")，[漢仁守](../Page/漢仁守.md "wikilink")
    饰。
  - 《》：1996年[SBS電視劇](../Page/SBS_\(韓國\).md "wikilink")，[朴根瀅](../Page/朴根瀅.md "wikilink")
    饰。
  - 《[女人天下](../Page/女人天下.md "wikilink")》：2001年[SBS電視劇](../Page/SBS_\(韓國\).md "wikilink")，[李德華](../Page/李德华.md "wikilink")
    饰。
  - 《[不滅的李舜臣](../Page/不滅的李舜臣.md "wikilink")》:2004年[KBS電視劇](../Page/韓國放送公社.md "wikilink")，
    饰。
  - 《[獄中花](../Page/獄中花.md "wikilink")》:2016年[MBC電視劇](../Page/文化广播_\(韓國\).md "wikilink")，[鄭俊鎬](../Page/鄭俊鎬.md "wikilink")
    饰。

## 附註

## 參見

  - [尹元老](../Page/尹元老.md "wikilink")
  - [尹任](../Page/尹任.md "wikilink")
  - [尹之任](../Page/尹之任.md "wikilink")
  - [李戡](../Page/李戡_\(朝鮮人\).md "wikilink")
  - [李梁](../Page/李梁_\(朝鮮人\).md "wikilink")

## 參考資料

  - [윤원형(尹元衡)](http://koreandb.empas.com/koreandb/history/bang/bang_info.html?n=2856&di=l)
  - [尹元衡:Daum](http://enc.daum.net/dic100/contents.do?query1=b17a2117a)

[Category:領議政](../Category/領議政.md "wikilink")
[Category:朝鮮王朝外戚](../Category/朝鮮王朝外戚.md "wikilink")
[Category:自殺政治人物](../Category/自殺政治人物.md "wikilink")
[Category:朝鲜王朝自杀人物](../Category/朝鲜王朝自杀人物.md "wikilink")
[Category:京畿道出身人物](../Category/京畿道出身人物.md "wikilink")
[Won](../Category/坡平尹氏.md "wikilink")
[Won](../Category/尹姓.md "wikilink")

1.  [《明宗實錄》31卷，明宗20年（1565年）11月13日紀錄二](http://sillok.history.go.kr/id/kma_12011013_002)
2.  [《明宗實錄》31卷，明宗20年（1565年）11月18日紀錄五](http://sillok.history.go.kr/id/kma_12011018_005)