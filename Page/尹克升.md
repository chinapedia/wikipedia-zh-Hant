**尹克升**（），河北[通县](../Page/通县.md "wikilink")（今属[北京](../Page/北京.md "wikilink")）人。1949年3月参加革命工作，1953年6月加入中国共产党。1964年毕业于北京石油学院开发系油田开采专业。曾任中共青海省委书记，為[中華人民共和國](../Page/中華人民共和國.md "wikilink")[青海省的第十任](../Page/青海省.md "wikilink")[領導人](../Page/領導人.md "wikilink")。中共第十二届（1985年9月增选）、十三届、十四届中央委员。第八届、第九届全国人大代表，第九届全国人大常委。

1953年加入[中国共产党](../Page/中国共产党.md "wikilink")。1964年毕业于北京石油学院开发系，大学文化。历任青海石油管理局科研所副所长、局长，中共青海省委常委，青海省副省长，1985年任[中共青海省委书记](../Page/中共青海省委.md "wikilink")。1997年2月至1998年4月任[中共中央直属机关工委副书记](../Page/中共中央直属机关工委.md "wikilink")（正部长级）。1998年3月当选九届全国人大常委、全国人大民族委员会副主任委员。中共第十二届、十三届、十四届中央委员。2011年5月19日，尹克升因病在北京逝世，享年80岁\[1\]。

## 参考资料

{{-}}

[Category:第九屆全國人大代表](../Category/第九屆全國人大代表.md "wikilink")
[Category:青海省全國人民代表大會代表](../Category/青海省全國人民代表大會代表.md "wikilink")
[Category:中華人民共和國青海省副省長](../Category/中華人民共和國青海省副省長.md "wikilink")
[Category:中國共產黨第十四屆中央委員會委員](../Category/中國共產黨第十四屆中央委員會委員.md "wikilink")
[Category:中國共產黨第十三屆中央委員會委員](../Category/中國共產黨第十三屆中央委員會委員.md "wikilink")
[Category:中國共產黨第十二屆中央委員會委員](../Category/中國共產黨第十二屆中央委員會委員.md "wikilink")
[Category:中共青海省委書記](../Category/中共青海省委書記.md "wikilink")
[Category:中共青海省委常委](../Category/中共青海省委常委.md "wikilink")
[Category:中國共產黨黨員](../Category/中國共產黨黨員.md "wikilink")
[Category:北京石油学院校友](../Category/北京石油学院校友.md "wikilink")
[Category:通州人](../Category/通州人.md "wikilink")
[Ke克升](../Category/尹姓.md "wikilink")

1.  [青海省委原书记尹克升同志逝世](http://news.xinhuanet.com/politics/2011-05/24/c_121453318.htm)，新华网，2011年5月24日。