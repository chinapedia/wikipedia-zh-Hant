**尼什**（[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")：****或****；[拉丁语](../Page/拉丁语.md "wikilink")：****；[希腊语](../Page/希腊语.md "wikilink")：****）是[塞爾維亞的一座城市](../Page/塞爾維亞.md "wikilink")，在[中塞爾維亞範圍內](../Page/中塞爾維亞.md "wikilink")，尼什也是塞爾維亞南部最大的城市。在塞爾維亞國內，尼什是僅次于首都[貝爾格萊德](../Page/貝爾格萊德.md "wikilink")、[諾維薩德的第三大都市](../Page/諾維薩德.md "wikilink")。據2011年的人口普查，市區人口有192,208人\[1\]。包含郊外在內，全市有人口260,237\[2\]人。尼什的面積是597㎢，市區部份分為4個區，加上郊外的[尼什卡巴尼亞](../Page/尼什卡巴尼亞.md "wikilink")，共分為五個區。郊外分為68個地區。尼什是[尼沙瓦州的行政中心都市](../Page/尼沙瓦州.md "wikilink")。

尼什是[欧洲和](../Page/欧洲.md "wikilink")[巴尔干半岛最古老的城市之一](../Page/巴尔干半岛.md "wikilink")，在古代就被认为是连接东西方世界的重要通道\[3\]。早在铁器时代，古代巴尔干[色雷斯人就定居在这里](../Page/色雷斯人.md "wikilink")。色雷斯人部族多利巴利人在凱爾特人來到巴爾幹之前就居住在這里。公元前279年，凱爾特人來到這裡，凱爾特人部族之一[斯科迪斯克人開始統治這裡](../Page/斯科迪斯克人.md "wikilink")。在公元前75年，尼什市區及其附近地區被羅馬人征服。古羅馬在1世紀建設了[米利塔里亞大道](../Page/米利塔里亞大道.md "wikilink")。尼什在當時的名稱是納伊斯斯（Naissus），是當時的重要都市。尼什也是[君士坦丁堡的創建者](../Page/君士坦丁堡.md "wikilink")、羅馬帝國最初的基督教徒[羅馬皇帝](../Page/羅馬皇帝.md "wikilink")[君士坦丁一世的誕生地](../Page/君士坦丁一世.md "wikilink")\[4\]，此外也是[君士坦丁三世及](../Page/君士坦丁三世.md "wikilink")[查士丁一世的誕生地](../Page/查士丁一世.md "wikilink")。尼什郊區還有塞爾維亞最古老的教會。

今日的尼什是塞爾維亞最重要的產業和教育中心之一，是電器產業及機械技術、紡織、香煙產業的中心。2013年，尼什舉辦了紀念[米蘭勅令](../Page/米蘭勅令_\(君士坦丁\).md "wikilink")1,700年的活動\[5\]。

## 地勢

尼什位於尼沙瓦和流域的東側，靠近尼沙瓦河和[南摩拉瓦河的交匯處](../Page/南摩拉瓦河.md "wikilink")。在市中心地區，市中央廣場附近的海拔高度是194m。市內最高地點是[斯瓦山](../Page/斯瓦山.md "wikilink")，海抜1,523m，最低地点是尼沙瓦河匯流處，海抜173m。

尼什位於[大陸性氣候地區](../Page/大陸性氣候.md "wikilink")。年平均氣溫是11.2℃。最熱的月份是7月，平均氣溫是21.2℃。最冷的月份是1月，平均氣溫是0.2℃。年平均降水量是567.25mm，全年降水日數是123日，積雪日數是45日。

## 歷史

### 古代

在尼什發現過[新石器時代住宅的遺跡](../Page/新石器時代.md "wikilink")。尼什及其附近地區的歷史可追溯至[公元前5000年至](../Page/公元前5000年.md "wikilink")[公元前2000年](../Page/公元前2000年.md "wikilink")，其中最引人注目的遺跡是胡姆斯卡丘卡（*Humska
Čuka*）\[6\]。在鐵器時代時期，色雷斯人開始出現在這裡。色雷斯人部族[多利巴利人居住在尼什附近地區](../Page/多利巴利人.md "wikilink")。尼什在公元前424年時在史書中被提及。公元前279年，凱爾特人侵入巴爾幹半島。凱爾特系的斯科迪斯克人征服了多利巴利人，尼什在當時的名稱則是納維索斯（**Navissos**）\[7\]。

### 羅馬時代

[Липадаријум_10.jpg](https://zh.wikipedia.org/wiki/File:Липадаријум_10.jpg "fig:Липадаријум_10.jpg")
在公元前175-168年，羅馬征服了巴爾幹半島。在這一時期，納維索斯改名為納伊斯斯（Naissus，也稱納伊索斯，*Naissos*）。曾是作戰的基地。納伊斯斯首次被記錄在羅馬文書中是在2世紀初期，[托勒密在他的著作](../Page/托勒密.md "wikilink")《地理學
(書籍)|地理學》中認為納伊斯斯是值得關注的地區\[8\]。當時城市是戰略要地，也是上[默西亞的駐屯地和商業都市](../Page/默西亞_\(羅馬行省\).md "wikilink")\[9\]。[古羅馬在](../Page/古羅馬.md "wikilink")1世紀初期建設了[米利塔利斯大道](../Page/米利塔利斯大道.md "wikilink")，納伊斯斯是其中重要城市之一。前往萊什、[塞爾迪加](../Page/索菲亞.md "wikilink")、辛吉度納姆、拉蒂亞里亞、[塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")（途徑斯庫皮）的五條街道在納伊斯斯交叉\[10\]。

[Byzantinischer_Mosaizist_um_1000_002.jpg](https://zh.wikipedia.org/wiki/File:Byzantinischer_Mosaizist_um_1000_002.jpg "fig:Byzantinischer_Mosaizist_um_1000_002.jpg")內[君士坦丁一世的馬賽克c](../Page/君士坦丁一世.md "wikilink").
1000\]\]
[三世紀危機期間中的](../Page/三世紀危機.md "wikilink")268年，羅馬皇帝頻繁交替，[哥特人開始大規模入侵](../Page/哥特人.md "wikilink")。哥特人勢力蹂躪了[色雷斯和](../Page/色雷斯.md "wikilink")[馬其頓](../Page/馬其頓行省.md "wikilink")、[默西亞](../Page/默西亞_\(羅馬行省\).md "wikilink")、[潘諾尼亞](../Page/潘諾尼亞.md "wikilink")。之後，[克勞狄二世就任皇帝](../Page/克勞狄二世.md "wikilink")。在三世紀各戰役中最血腥的戰役之一[納伊斯斯之戰中戰勝了哥特人聯軍](../Page/納伊斯斯之戰.md "wikilink")。哥特人聯軍中有3萬至5萬人這這裡戰死。272年，君士坦丁一世在納伊斯斯出生。君士坦丁一世新設了達契亞·地中海行省，首府就是納伊斯斯。

[Medijana_mozaik.jpg](https://zh.wikipedia.org/wiki/File:Medijana_mozaik.jpg "fig:Medijana_mozaik.jpg")
皇帝的邸宅位於尼什郊外的梅迪亞那，是重要的遺跡。君士坦丁一世也一度在這裡居住。在[尤利安皇帝在位期間](../Page/尤利安.md "wikilink")，加固了納伊斯斯的城牆，城市更加繁榮。443年，匈人國王[阿提拉攻擊納伊斯斯](../Page/阿提拉.md "wikilink")，城市遭到了嚴重的破壊。納伊斯斯被匈人支配之後，市民遭到了屠殺。在戰爭數年之後，市外的河岸都被匈人屠殺的納伊斯斯市民的遺骸所覆蓋。[查士丁尼王朝的創始者](../Page/查士丁尼王朝.md "wikilink")[查士丁一世在](../Page/查士丁一世.md "wikilink")450年出生于納伊斯斯。其外甥[查士丁尼一世復興了納伊斯斯](../Page/查士丁尼一世.md "wikilink")。查士丁尼設置了查士丁尼主教座。[普羅克匹厄斯也曾提及過納伊斯斯](../Page/普羅克匹厄斯.md "wikilink")\[11\]。

### 中世紀

[Crusaders_attacking_Niš,_1096.jpg](https://zh.wikipedia.org/wiki/File:Crusaders_attacking_Niš,_1096.jpg "fig:Crusaders_attacking_Niš,_1096.jpg")時的1096年尼什圍城戰\]\]
6世紀後期，斯拉夫人和阿瓦爾人開始大規模民族移動。在6世紀至7世紀之間，斯拉夫人部族曾8次試圖攻打尼什。在6世紀至7世紀之間，斯拉夫人部族曾8次試圖攻打尼什。551年，斯拉夫人穿过尼什，最初曾試圖到達塞薩洛尼基但最後只到達達爾馬提亞\[12\]\[13\]。580年代，[南斯拉夫人部族之一](../Page/南斯拉夫人.md "wikilink")[斯庫拉夫尼人征服了塞爾維亞和希臘北部地區](../Page/斯庫拉夫尼人.md "wikilink")\[14\]。最後的攻撃是在615年，斯拉夫人掌握了城市，羅馬人和羅馬化的色雷斯人及達契亞人要麼逃跑，要麼被同化。785年[君士坦丁六世征服了尼什](../Page/君士坦丁六世.md "wikilink")。842年，隨著[狄奧菲洛的死去](../Page/狄奧菲洛.md "wikilink")，這一地區被[保加爾人支配](../Page/保加爾人.md "wikilink")\[15\]。

1018年，[巴西爾二世成立了塞爾曼軍事管區](../Page/巴西爾二世.md "wikilink")，佔領了保加利亞和塞爾維亞，尼什是當時的三大正式都市的其中之一。君士坦丁·博丁于1072年發動了叛亂，博丁曾攻佔尼什，但在後來被抓捕\[16\]。在[平民十字軍時期的](../Page/平民十字軍.md "wikilink")1096年7月3日，[隱者皮埃爾在尼什和拜占庭的軍隊發生衝突](../Page/隱者皮埃爾.md "wikilink")，其自身勢力折損了四分之一，但仍然行軍至[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")\[17\]。

1155年，尼什被贈送被德薩公\[18\]。1162年，拜占庭皇帝[曼努埃爾一世在尼什和](../Page/曼努埃爾一世_\(拜占庭\).md "wikilink")[斯特凡·曼尼亞一世會面](../Page/斯特凡·曼尼亞一世.md "wikilink")。斯特凡·曼尼亞一世因戰勝拜占庭，得到了[萊斯科瓦茨和尼什](../Page/萊斯科瓦茨.md "wikilink")\[19\]。1188年，尼什成為斯特凡·曼尼亞一世的塞爾維亞王國的首都\[20\]，1189年7月22日曼尼亞一世在尼什接納了神聖羅馬帝國皇帝[腓特烈一世派來的](../Page/腓特烈一世_\(神聖羅馬帝國\).md "wikilink")10萬人十字軍戰士\[21\]。1202年，在伏干2世（Vukan
Nemanjić of
Serbia）統治時期，尼什曾擁有特別地位\[22\]。1203年，[卡洛揚曾佔領尼什](../Page/卡洛揚_\(保加利亞\).md "wikilink")\[23\]，但之後斯特凡二世又再次統治尼什地區。

1375年，經過25天的包圍之後，尼什首次被奧斯曼帝國統治。在之後1389年的[科索沃戰役中](../Page/科索沃戰役.md "wikilink")，塞爾維亞的勢力被削弱，在之後70年間成為半獨立的國家。

1443年，尼什被贈給盧丹寧（Ludanjin），城市又回到塞爾維亞統治。1443年，在尼什附近，匈牙利王國的[匈雅提·亞諾什和塞爾維亞君主杜拉德](../Page/匈雅提·亞諾什.md "wikilink")·布蘭科維奇的軍隊和奧斯曼之間爆發了尼什戰役，奧斯曼軍隊被擊退。之後直到再次被奧斯曼攻佔為止，尼什都是自由都市。

### 近代

尼什在1448年再次屈服于奧斯曼帝國，之後的241年里，尼什都由奧斯曼統治。奧斯曼時期，尼什是尼什桑扎克及尼什州的首府\[24\]
。尼什要塞修建于奧斯曼時期，至今日依然保存良好，是巴爾幹地區保存狀態最好的要塞之一。1689年，奧地利軍隊和奧斯曼之間爆發尼什戰役，翌年奧斯曼又奪回尼什。1737年，[俄土戰爭爆發](../Page/俄土戰爭_\(1735年-1739年\).md "wikilink")，在戰爭中奧地利曾一度佔領尼什。1739年年戰爭結束后尼什又再次由奧斯曼統治。

### 19世紀至第二次世界大戰

[Monument_du_Mont_Cegar.jpg](https://zh.wikipedia.org/wiki/File:Monument_du_Mont_Cegar.jpg "fig:Monument_du_Mont_Cegar.jpg")
[Bubanj-Pesnice_sa_amfiteatra_2.jpg](https://zh.wikipedia.org/wiki/File:Bubanj-Pesnice_sa_amfiteatra_2.jpg "fig:Bubanj-Pesnice_sa_amfiteatra_2.jpg")
尼什在塞爾維亞-土耳其戰爭之後得到解放。解放尼什的戰役是在1877年12月29日開始的。塞爾維亞軍隊在1878年1月11日進入尼什，尼什成為塞爾維亞的一部份。尼什在解放之后急速近代化。

1879年，尼什設立了圖書館。尼什最初的旅館「Europe」建于1879年。首個醫院則在1881年開始使用。市政廳建設于1882年至1887年期間，Kosta
Čendaš印刷廠則設立于1883年。1884年，尼什最早的報紙「Niški Vesnik」創刊。1884年，約萬·阿佩爾（Jovan
Apel）創建了啤酒廠。1884年，尼什還修建了鐵路并建設了車站，1884年8月8日，一班自貝爾格萊德出發的列車到達尼什，這是最早的到達尼什的列車。1885年以來，尼什是[東方特快的終點站](../Page/東方特快.md "wikilink")，直到1888年鐵路開業至保加利亞的[索菲亞為止](../Page/索菲亞.md "wikilink")。1887年，米哈伊洛·迪米奇（Mihailo
Dimić）設立了國立劇場。1897年，米塔·利斯蒂茨設立了紡織品工廠*Nitex*。1905年，納德茲達·彼得羅維茨（Nadežda
Petrović）在尼什開設了美術館。首次上演電影則是在1897年。首個電影院則開始于1905年。1908年，在尼沙瓦河上修建了水力發電站，是塞爾維亞最大的水力發電站。1912年，在多盧巴菲爾德修建了機場。1912年12月29日，首次有飛機在尼什着陸。1913年，開設了市博物館，收藏考古學及民俗學相關藏品和藝術作品。

[第一次巴爾幹戰爭時期](../Page/第一次巴爾幹戰爭.md "wikilink")，塞爾維亞對奧斯曼帝國軍事作戰的司令部就設在尼什。第一次世界大戰時期，尼什曾是戰時首都，政府機關和議會都設在這裡，直到1915年11月塞爾維亞被[同盟國征服為止](../Page/同盟國.md "wikilink")。隨著塞薩洛尼基戰線的展開，塞爾維亞軍元帥[佩塔爾·波約維奇在](../Page/佩塔爾·波約維奇.md "wikilink")1918年10月12日解放尼什。尼什在戰後最初的數年是戰後復興期。1930年11月，開設運行有軌電車（市電）。1930年，國營航空開設了[貝爾格萊德](../Page/貝爾格萊德.md "wikilink")-**尼什**-[斯科普里](../Page/斯科普里.md "wikilink")-[塞薩洛尼基之間的航線](../Page/塞薩洛尼基.md "wikilink")。第二次世界大戰時期，德國佔領了城市，南斯拉夫最初的納粹的強制收容所設在尼什，收容了30,000人，其中有10,000人在布巴尼（Bubanj）之丘被射殺。1942年2月12日，有147人收容者從這裡逃走。1944年，尼什受遭到了同盟國的猛烈空襲\[25\]。1944年10月，尼什被[南斯拉夫遊擊隊和蘇聯軍隊解放](../Page/南斯拉夫人民民族解放軍及黨衛隊.md "wikilink")。

### 現代

1996年，尼什是塞爾維亞最早抵抗[斯洛博丹·米洛舍維奇支配的都市](../Page/斯洛博丹·米洛舍維奇.md "wikilink")。同年在尼什當地舉行的選舉中獲勝的民主在野黨聯合曾經持續抵抗88天，拒絕將權力交給米洛舍維奇的政黨。尼什首位通過民主方式選出的市長是佐蘭·日夫科維奇（Zoran
Živković）。1999年5月7日，尼什遭到了[北約的](../Page/北約.md "wikilink")[空襲](../Page/北约轰炸南斯拉夫.md "wikilink")，有15名市民犧牲。\[26\]

## 經濟

尼什在南斯拉夫時代開始得到開發。1981年，尼什的GDP是南斯拉夫平均的110%\[27\]。尼什不僅是行政上的中心，也自古以來就是塞爾維亞南部交通網重要的要衝。現在也是塞爾維亞重要的產業中心，除了香煙產業之外，還有電氣機械産業和建設、機械技術、紡織、貴金屬、食品產業、皮革等多種產業。尼什的香煙工廠創業于1930年，位於市內的[茨爾維尼-克爾斯特](../Page/茨爾維尼-克爾斯特.md "wikilink")，生產香煙和捲烟、香煙器具、過濾嘴等香煙關聯產品。在1995年還設置了研究所，進行新產品的開發。在民營化的過程中，尼什香煙工廠在2003年8月被[菲利普·莫里斯公司收購](../Page/菲利普·莫里斯公司.md "wikilink")。菲利普·莫里斯公司投資了5億8000萬歐元，這是2003年內外國企業對塞爾維亞進行的投資中規模最大的單筆投資。

## 交通

[Niš_railway_station_2.JPG](https://zh.wikipedia.org/wiki/File:Niš_railway_station_2.JPG "fig:Niš_railway_station_2.JPG")
[Bus_station_niš.jpg](https://zh.wikipedia.org/wiki/File:Bus_station_niš.jpg "fig:Bus_station_niš.jpg")

尼什位於[摩拉瓦河流域北部和](../Page/摩拉瓦河.md "wikilink")[瓦爾達爾河流域南部](../Page/瓦爾達爾河.md "wikilink")，地理位置重要，位於連接[希臘和](../Page/希臘.md "wikilink")[中歐主要交通走廊的途中](../Page/中歐.md "wikilink")。連接索菲亞和伊斯坦布爾的走廊也通過這裡。尼什的地理位置使得其在歷史上在這一地區佔有重要位置。最初利用其地理優勢的是羅馬帝國。羅馬帝國修建米利塔里亞大道，其北至辛吉度納姆（現在的貝爾格萊德），南東至君士坦丁堡（現在的伊斯坦布爾）。

在今天，尼什科通過[歐洲高速公路](../Page/歐洲高速公路.md "wikilink")[E75號線往北至](../Page/歐洲E75公路.md "wikilink")[貝爾格萊德及](../Page/貝爾格萊德.md "wikilink")[中歐](../Page/中歐.md "wikilink")、往南至[斯科普里](../Page/斯科普里.md "wikilink")、[塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")、[雅典](../Page/雅典.md "wikilink")。還可通過[E80號線自尼什前往索菲亞及伊斯坦布爾](../Page/歐洲E80公路.md "wikilink")，以及更遠的安那托利亞和中東，也可往西至普里什蒂納河黑山、亞得里亞海。[E771號線在前往](../Page/歐洲E771公路.md "wikilink")[扎那查爾](../Page/扎那查爾.md "wikilink")、[克拉多沃](../Page/克拉多沃.md "wikilink")、[羅馬尼亞的](../Page/羅馬尼亞.md "wikilink")[德羅貝塔-塞維林堡](../Page/德羅貝塔-塞維林堡.md "wikilink")。尼什也是鐵路交通的要衝。[君士坦丁大帝機場](../Page/君士坦丁大帝機場.md "wikilink")（機場代碼：INI，塞爾維亞語：、英語:Niš
Constantine the Great Airport）是塞爾維亞第二重要的機場，建設于1910年。

市內的公共交通機關由13條巴士路線構成。在1930年至1958年期間，尼什曾有有軌電車運行\[28\]。

## 國際關係

### 姊妹都市

據尼什市政廳的官方網站，尼什和以下都市是姊妹都市\[29\]。

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/斯科普里.md" title="wikilink">斯科普里</a>, <a href="../Page/馬其頓共和國.md" title="wikilink">馬其頓共和國</a></p></li>
<li><p><a href="../Page/卡桑德拉.md" title="wikilink">卡桑德拉</a>, <a href="../Page/希臘.md" title="wikilink">希臘</a>[30]</p></li>
<li><p><a href="../Page/斯巴達_(自治市).md" title="wikilink">斯巴達</a>, <a href="../Page/希臘.md" title="wikilink">希臘</a>[31]</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/格利法扎.md" title="wikilink">格利法扎</a>, <a href="../Page/希臘.md" title="wikilink">希臘</a>[32]</p></li>
<li><p><a href="../Page/馬奧斯.md" title="wikilink">馬奧斯</a>, <a href="../Page/希臘.md" title="wikilink">希臘</a>[33]</p></li>
<li><p><a href="../Page/阿利莫斯.md" title="wikilink">阿利莫斯</a>, <a href="../Page/希臘.md" title="wikilink">希臘</a>[34]</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/大特爾諾沃.md" title="wikilink">大特爾諾沃</a>, <a href="../Page/保加利亞.md" title="wikilink">保加利亞</a>[35]</p></li>
<li><p><a href="../Page/科希策.md" title="wikilink">科希策</a>, <a href="../Page/斯洛伐克.md" title="wikilink">斯洛伐克</a>[36][37]</p></li>
<li><p><a href="../Page/庫爾斯克.md" title="wikilink">庫爾斯克</a>, <a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a>[38]</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/薩爾特達爾.md" title="wikilink">薩爾特達爾</a>, <a href="../Page/挪威.md" title="wikilink">挪威</a>[39]</p></li>
<li><p><a href="../Page/巴德洪布爾格福爾德爾赫黑.md" title="wikilink">巴德洪布爾格福爾德爾赫黑</a>, <a href="../Page/德國.md" title="wikilink">德國</a>[40]</p></li>
<li><p><a href="../Page/克拉科夫.md" title="wikilink">克拉科夫</a>, <a href="../Page/波蘭.md" title="wikilink">波蘭</a>[41]</p></li>
</ul></td>
</tr>
</tbody>
</table>

### 友好都市

  - [維也納](../Page/維也納.md "wikilink"),
    [奧地利](../Page/奧地利.md "wikilink")\[42\]

  - [格拉茨](../Page/格拉茨.md "wikilink"),
    [奧地利](../Page/奧地利.md "wikilink")\[43\]

  - [巴塞羅那](../Page/巴塞羅那.md "wikilink"),
    [西班牙](../Page/西班牙.md "wikilink")\[44\]

  - [哥倫布](../Page/哥倫布_\(俄亥俄州\).md "wikilink"),
    [美國](../Page/美國.md "wikilink")\[45\]

## 注释

## 外部链接

  - [尼什官方网站](http://www.ni.rs/)
  - [尼什地图和指南](https://web.archive.org/web/20070910191751/http://www.southserbia.com/)

[N](../Category/希腊城邦.md "wikilink")

1.

2.  [1](http://pod2.stat.gov.rs/ObjavljenePublikacije/Popis2011/Knjiga20.pdf)

3.  [Arabic
    UPI.com](http://www.metimes.com/storyview.php?StoryID=20070515-082637-6667r)


4.  [New Advent Catholic encyclopedia: Constantine the
    Great](http://www.newadvent.org/cathen/04295c.htm)

5.

6.  Stone Pages,
    [002763](http://www.stonepages.com/news/archives/002763.html)

7.  [Nis,Britanica](http://www.britannica.com/EBchecked/topic/415944/Nis)

8.  [The provincial at Rome: and, Rome and the
    Balkans 80BC-AD14](http://books.google.com/books?id=D5IxWxCgFFwC&pg=PA207)

9.  [BALCANICA
    XXXVII](http://www.balkaninstitut.com/pdf/izdanja/balcanica/Balcanica%20XXXVII%20\(2006\).pdf)

10.
11. [p. 238](http://books.google.com/?id=3J96wSxMaeYC&pg=PA238)

12. BG III 40

13. [The Slavs in the 6th century North
    Illyricum](http://www.rastko.rs/arheologija/delo/13047)

14. The New Cambridge Medieval History: c. 500-c. 700, p. 539

15. *Encyclopædia Britannica: a new survey of universal knowledge,
    Volume [20](http://books.google.com/?id=Jh9GAQAAIAAJ)*, p. 341: "the
    eastern provinces (Branichevo, Morava, Timok, Vardar, Podrimlye)
    were occupied by the Bulgars."

16. *Byzantium's Balkan frontier*, p. 142

17. *The great migrations in the East and South East of Europe from the
    ninth to the thirteenth century*, p. 146, [Google Books
    link](http://books.google.com/?id=9tZoAAAAMAAJ)

18. *The Late Medieval Balkans*, p. 4

19.
20. *The Late Medieval Balkans*, p. 7

21. *The Late Medieval Balkans*, p. 24

22. *The Late Medieval Balkans*, p. 48

23. *The Late Medieval Balkans*, p. 54

24.

25. [Serbs were not specially chosen as
    targets](http://www.danas.rs/20040417/vikend3.html) ,
    [Danas](../Page/Danas.md "wikilink")

26.

27.

28. [arhivnis.co.rs](http://www.arhivnis.co.rs/cirilica/idelatnost/br%201/cpksaobsrbije.htm)


29.

30.
31.
32.
33.
34.
35.
36.
37.

38.
39.
40.
41.
42.
43.
44.
45.