**尼克·李森**（**Nick
Leeson**，1967年2月25日—），曾任[英國](../Page/英國.md "wikilink")[霸菱银行投資交易員](../Page/霸菱银行.md "wikilink")。他的交易失利導致英國歷史最悠久的投資銀行——霸菱银行倒閉。

李森從1992年開始隱瞞虧損，偽造買賣紀錄，造假交易，僅私下留存粗略的真實交易回報。事件始於1995年1月16日，李森在[新加坡和](../Page/新加坡.md "wikilink")[東京交易市場進行](../Page/東京.md "wikilink")[衍生性金融商品投機交易](../Page/衍生性金融商品.md "wikilink")，賣出跨式（又稱賣出鞍式,，short
straddle，[選擇權交易策略的一種](../Page/選擇權.md "wikilink")）賭股價指數不會大幅波動。然而，在1月17日發生的[阪神大地震重創日本經濟](../Page/阪神大地震.md "wikilink")，也使得日本股市大跌，而李森的交易也隨之遭殃。李森試圖補回他的損失，做了一系列風險越來越高的投資決策，賭[日經會停止下跌且快速回升](../Page/日經平均股票價格.md "wikilink")；但又再度碰壁，使虧損越積越多。最後，銀行發現李森的交易累積了高達14億美金的損失，也就是銀行資本額的兩倍。因此導致銀行倒閉。

李森逃到[馬來西亞](../Page/馬來西亞.md "wikilink")，[泰國](../Page/泰國.md "wikilink")，最終在[德國被逮捕](../Page/德國.md "wikilink")，並且於1995年11月20日被[引渡到](../Page/引渡.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")。雖然李森在1月16日的交易行為有得到授權，但他仍因欺瞞上級交易風險及損失程度而被起訴。有些評論家認為銀行自我審核制度的缺陷和風險管理的陋習佔了主要因素，新加坡的官方報告也指出，銀行管理的疏失是主要關鍵，主管早已知悉李森用來作假的帳號。
判決結果他必須在新加坡的[樟宜監獄服刑六年半](../Page/樟宜監獄.md "wikilink")。李森在1999年出獄，並被診斷出罹患[結腸癌](../Page/結腸癌.md "wikilink")。

1996年，李森在服刑時發行了他的個人傳記《》，書中詳細描述他的所作所為。這本書後來在1999年被拍成同名電影（台灣上映時片名改譯為《Ａ錢大玩家》），由[伊旺·迈奎格和](../Page/伊旺·迈奎格.md "wikilink")主演。

尼克·李森現居于[愛爾蘭西部的](../Page/愛爾蘭.md "wikilink")[哥爾威郡的](../Page/戈尔韦郡.md "wikilink")。雖然此前在獄時離了婚，現在的他已經再婚且成為餐後演說的一般觀眾。他在2005年4月被任命為[加爾威聯足球俱樂部的商務經理](../Page/加爾威聯足球俱樂部.md "wikilink")，並在2011年2月時離職。他仍在操作股市，但僅使用個人資本。

2005年6月23日，尼克·李森推出新書*Back from the Brink: Coping with
Stress*，再續了《Ａ錢大玩家》后尼克·李森的故事。它訴說了尼克·李森完整的個人故事。其中與頂尖心理學家的深度對談顯示了連續不斷的壓力如何影響了尼克·李森以及世間大眾的心理和生理健康。

## 作品

  - 《Ａ錢大玩家》（*Rogue Trader*） ISBN 0-7515-1708-9
  - ''Back from the Brink: Coping with Stress '' (2005) - ISBN
    0-7535-1075-8

## 外部連結

  - [Nick Leeson 的官方網站](http://www.nickleeson.com)
  - [霸菱大混亂](http://www.riskglossary.com/articles/barings_debacle.htm)
    *(更多細節)*
  - [From Rogue Trader to Star
    Survivor](http://www.iconmag.co.uk/nick_leeson.htm) *(尼克·李森與癌症奮戰細節)*
  - [Leeson's legacy lives on in
    Singapore](http://news.bbc.co.uk/2/hi/business/4288271.stm)

[Category:新加坡囚犯及被拘留者](../Category/新加坡囚犯及被拘留者.md "wikilink")
[Category:英国人](../Category/英国人.md "wikilink")
[Category:金融家](../Category/金融家.md "wikilink")
[T](../Category/1967年出生.md "wikilink")
[Category:經濟犯罪](../Category/經濟犯罪.md "wikilink")