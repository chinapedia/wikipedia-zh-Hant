**尼克羅龍**（屬名：*Nicrosaurus*）是[植龍目的一屬](../Page/植龍目.md "wikilink")。牠們的外表與生活方式可能類似[鱷魚](../Page/鱷魚.md "wikilink")，但牠們與鱷魚並非近親，而是[平行演化的結果](../Page/平行演化.md "wikilink")。尼克羅龍與鱷魚的主要差異在於鼻孔的位置，尼克羅龍的鼻孔位在前額，而鱷魚的鼻孔則位在口鼻部末端。
[Nicrosaurus1Zittel.jpg](https://zh.wikipedia.org/wiki/File:Nicrosaurus1Zittel.jpg "fig:Nicrosaurus1Zittel.jpg")
根據2002年的研究，尼克羅龍、[迷鱷](../Page/迷鱷.md "wikilink")、[雷東達龍](../Page/雷東達龍.md "wikilink")、[偽帕拉鱷都屬於](../Page/偽帕拉鱷.md "wikilink")[偽帕拉鱷亞科](../Page/偽帕拉鱷亞科.md "wikilink")\[1\]。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:植龍目](../Category/植龍目.md "wikilink")

1.  Hungerbühler A. 2002. The Late Triassic phytosaur *Mystriosuchus
    westphali*, with a revision of the genus. *Palaeontology* **45**
    (2): 377-418