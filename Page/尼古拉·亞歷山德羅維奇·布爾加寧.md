[Bundesarchiv_Bild_183-29921-0001,_Bulganin,_Nikolai_Alexandrowitsch.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-29921-0001,_Bulganin,_Nikolai_Alexandrowitsch.jpg "fig:Bundesarchiv_Bild_183-29921-0001,_Bulganin,_Nikolai_Alexandrowitsch.jpg")
**尼古拉·亚历山德罗维奇·布尔加宁**（，）[苏联政治家](../Page/苏联.md "wikilink")、国务活动家，曾任[苏联部长会议主席](../Page/苏联部长会议主席.md "wikilink")（名义上的[政府首脑](../Page/政府首脑.md "wikilink")，1955年2月8日-1958年3月27日）\[1\]。

## 生平

布尔加宁生于[下诺夫哥罗德](../Page/下诺夫哥罗德.md "wikilink")，1917年加入[布尔什维克](../Page/苏联共产党.md "wikilink")。1918年被安排到[契卡工作](../Page/契卡.md "wikilink")。[内战结束后在](../Page/俄国内战.md "wikilink")[最高国民经济委员会工作](../Page/最高国民经济委员会.md "wikilink")；1927年\~1931年，他负责管理[莫斯科的电力供应](../Page/莫斯科.md "wikilink")。1931年\~1937年，任莫斯科市[苏维埃执行委员会主席](../Page/苏维埃.md "wikilink")。

1934年，[苏共十七大选举布尔加宁为](../Page/苏联共产党第十七次全国代表大会.md "wikilink")[中央委员会候补委员](../Page/苏联共产党中央委员会.md "wikilink")。作为[斯大林的支持者](../Page/斯大林.md "wikilink")，他在斯大林发动的[大清洗中没有遇害](../Page/大清洗.md "wikilink")，反而获得提升。1937年，布尔加宁成为中央委员会正式委员，1938年成为[苏联人民委员会副主席](../Page/苏联人民委员会.md "wikilink")，兼任[苏联国家银行行长](../Page/苏联国家银行.md "wikilink")。

在[第二次世界大战中](../Page/第二次世界大战.md "wikilink")，布尔加宁活跃于苏联政府和军界，但从未担任过前线指挥官。[卫国战争开始后](../Page/卫国战争.md "wikilink")，布尔加宁从1941年开始先后担任[西方面军](../Page/西方面军.md "wikilink")、[波罗的海沿岸第2方面军和](../Page/波罗的海沿岸第2方面军.md "wikilink")[白俄罗斯第1方面军的军事委员会委员](../Page/白俄罗斯第1方面军.md "wikilink")。1944年成为[苏联国防委员会成员](../Page/苏联国防委员会.md "wikilink")。二战结束后，布尔加宁于1946年被任命为苏联武装力量部副部长，并被晋升为[元帅](../Page/苏联元帅.md "wikilink")。1948年起成为[苏联共产党中央政治局委员](../Page/苏联共产党中央政治局.md "wikilink")。

1953年斯大林逝世，布尔加宁在苏共内部夺权斗争中支持[赫鲁晓夫](../Page/赫鲁晓夫.md "wikilink")。1955年，他接替[马林科夫任部长会议主席](../Page/马林科夫.md "wikilink")，同年被授予[社会主义劳动英雄称号](../Page/社会主义劳动英雄.md "wikilink")。布尔加宁长期是赫鲁晓夫的政治盟友，但到1958年，由于怀疑赫鲁晓夫的自由化政策，他被强迫引退。不久他被派遣到[斯塔夫罗波尔当](../Page/斯塔夫罗波尔.md "wikilink")[地区经济委员会主席](../Page/地区经济委员会.md "wikilink")，最后在1960年2月正式退休。

布尔加宁于1975年2月24日逝世于莫斯科，葬于莫斯科[新圣女公墓](../Page/新圣女公墓.md "wikilink")\[2\]。

## 参考资料

<references/>

[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:蘇聯元帥](../Category/蘇聯元帥.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:蘇聯部長會議主席](../Category/蘇聯部長會議主席.md "wikilink")
[Category:蘇聯國防部長](../Category/蘇聯國防部長.md "wikilink")
[Category:蘇聯國家銀行行長](../Category/蘇聯國家銀行行長.md "wikilink")
[Category:莫斯科市長](../Category/莫斯科市長.md "wikilink")
[Category:苏联政治人物](../Category/苏联政治人物.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink")
[Category:安葬於新聖女公墓者](../Category/安葬於新聖女公墓者.md "wikilink")
[Category:下諾夫哥羅德人](../Category/下諾夫哥羅德人.md "wikilink")
[Category:老布尔什维克](../Category/老布尔什维克.md "wikilink")
[Category:俄国社会民主工党成员](../Category/俄国社会民主工党成员.md "wikilink")

1.
2.