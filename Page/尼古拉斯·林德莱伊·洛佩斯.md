**尼古拉斯·林德莱伊·洛佩斯**（**Nicolás Lindley
López**，1908年11月16日—1995年2月3日），生於[利馬](../Page/利馬.md "wikilink")，[武官](../Page/武官.md "wikilink")，[祕魯军政府第二任总统](../Page/祕魯军政府.md "wikilink")。

## 生平

林德莱伊生於[利馬一個上流家庭](../Page/利馬.md "wikilink")。他父親擁有[英國](../Page/英國.md "wikilink")[血統](../Page/血統.md "wikilink")。他於一間秘魯英人學校
- [聖安德烈斯學院](../Page/聖安德烈斯學院.md "wikilink")（Colegio San
Andrés）完成學業。1926年，他進入了[Chorrillos
軍事學院並於](../Page/Chorrillos_Military_School.md "wikilink")1930年取得[博士學位](../Page/博士.md "wikilink")。林德莱伊隨後加入軍隊並獲得不俗的仕途，他於1960年成為了秘魯軍的[陆军总司令](../Page/陆军总司令.md "wikilink")。

1962年7月，林德莱伊與祕魯军政府第一任总统[里卡多·佩雷斯·戈多伊發動了一場](../Page/里卡多·佩雷斯·戈多伊.md "wikilink")[軍事政變](../Page/軍事政變.md "wikilink")，推翻了民選總統[曼努埃爾·普拉多·烏加特切並建立起軍政府](../Page/曼努埃爾·普拉多·烏加特切.md "wikilink")。起初是由里卡多·佩雷斯·戈多伊任[臨時總統而林德莱伊則任](../Page/臨時總統.md "wikilink")[總理](../Page/秘魯總理.md "wikilink")，目的是為了組織一場新的[選舉並將權力交接到新選出來的政府](../Page/選舉.md "wikilink")。然而，由於里卡多·佩雷斯·戈多伊對權位的留戀，有違當初的共識。於是林德莱伊在1963年3月3日將之推翻並暫任總統，直至同年7月28日[費爾南多·貝朗德·特里獲選成為新總統](../Page/費爾南多·貝朗德·特里.md "wikilink")。

1964年至1975年，林德莱伊成為了秘鲁驻[西班牙大使](../Page/西班牙.md "wikilink")。任期結束後他選擇退休並一直留在西班牙，直至去世。

{{-}}

[Category:秘魯總統](../Category/秘魯總統.md "wikilink")
[Category:秘魯將軍](../Category/秘魯將軍.md "wikilink")