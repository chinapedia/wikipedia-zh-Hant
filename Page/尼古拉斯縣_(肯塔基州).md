**尼古拉斯縣**（**Nicholas County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州北部的一個縣](../Page/肯塔基州.md "wikilink")。面積510平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口6,813人。縣治[卡萊爾](../Page/卡萊爾_\(肯塔基州\).md "wikilink")
(Carisle)。

成立於1799年，縣政府成立於1800年6月1日。本縣紀念[肯塔基州州憲法起草者之一](../Page/肯塔基州州憲法.md "wikilink")、有「州憲之父」之稱的[喬治·尼古拉斯](../Page/喬治·尼古拉斯.md "wikilink")（George
Nicholas）。

[N](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")