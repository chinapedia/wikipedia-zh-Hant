**尼古萊·格里戈里耶維奇·魯賓斯坦**（，），[俄羅斯](../Page/俄羅斯.md "wikilink")[鋼琴家及](../Page/鋼琴家.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。他是[安東·魯賓斯坦的弟弟](../Page/安東·魯賓斯坦.md "wikilink")，也是[彼得·伊里奇·柴可夫斯基的好友](../Page/彼得·伊里奇·柴可夫斯基.md "wikilink")。

尼古萊·魯賓斯坦出生於[莫斯科的一個](../Page/莫斯科.md "wikilink")[猶太家庭中](../Page/猶太.md "wikilink")，父親當時開了一家小工廠，小時候跟隨著母親學[鋼琴](../Page/鋼琴.md "wikilink")，大一點時，則與他哥哥一樣受教於亞歷山大·維洛營（Alexander
Villoing）。在1840年代時，他與他哥哥被母親帶往[柏林](../Page/柏林.md "wikilink")，受教於Siegfried
Dehn，並因此受到[孟德爾頌與](../Page/孟德爾頌.md "wikilink")[邁耶貝爾的賞識與資助](../Page/邁耶貝爾.md "wikilink")。1866年9月1日，他成為了[莫斯科音樂學院的創辦人](../Page/莫斯科音樂學院.md "wikilink")。在當時，他被認為是同時代最偉大的鋼琴家之一，直到近來才被他哥哥安東的光環逐漸掩蓋。儘管如此，他的鋼琴演奏與安東狂放式的演奏風格有很大的不同；尼古萊較堅持古典主義的演奏風格，類似於[克拉拉·舒曼](../Page/克拉拉·舒曼.md "wikilink")，而與[李斯特·費倫茨差別較大](../Page/李斯特·費倫茨.md "wikilink")。

在莫斯科任職期間，他曾邀請柴可夫斯基為他寫了著名的《[第一號鋼琴協奏曲](../Page/第一號鋼琴協奏曲_\(柴可夫斯基\).md "wikilink")》。根據柴可夫斯基的信件描述，魯賓斯坦對於這首作品印象不佳，並希望他重寫，但他拒絕了。因此最後這首作品由鋼琴家[漢斯·馮·彪羅做了世界首演](../Page/漢斯·馮·彪羅.md "wikilink")。儘管如此，在魯賓斯坦於[巴黎去世之後](../Page/巴黎.md "wikilink")，柴可夫斯基為紀念他而寫了《[a小調鋼琴三重奏](../Page/鋼琴三重奏_\(柴可夫斯基\).md "wikilink")》。

尼古萊·魯賓斯坦在作曲方面也有不錯的成就。他最著名的作品為《g小調[塔朗泰拉](../Page/塔朗泰拉.md "wikilink")》以及《舒曼主題[幻想曲](../Page/幻想曲.md "wikilink")》，皆為鋼琴獨奏。

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

## 外部連結

  - [Note Regarding Dedication of Tchaikovsky's 1st Symphony to
    Rubinstein](https://web.archive.org/web/20071129202113/http://www.sonoraproductions.com/s022584.html)
  - [L.A Philharmonic reference to Rubinstein and Tchaikovsky's String
    quartet
    No. 1](https://web.archive.org/web/20050215053602/http://www.laphil.org/resources/piece_detail.cfm?id=1226)

[Rubinstein](../Category/俄罗斯作曲家.md "wikilink")
[Rubinstein](../Category/俄羅斯鋼琴家.md "wikilink")
[Rubinstein](../Category/俄國猶太人.md "wikilink")
[Rubinstein](../Category/莫斯科人.md "wikilink")
[Rubinstein](../Category/浪漫主义作曲家.md "wikilink")
[Rubinstein](../Category/犹太音乐家.md "wikilink")