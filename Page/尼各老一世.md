[教宗](../Page/教宗.md "wikilink")[聖](../Page/天主教聖人.md "wikilink")**尼各老一世**（，於858年4月24日—867年11月13日岀任教宗。尼各老一世宣稱，在有關信仰和道德的領域，教宗對所有的基督徒應該擁有宗主權的權威，甚至涵括皇室成員。\[1\]為教宗地位及權勢立下堅固的基礎。

## 譯名列表

  - 尼各老：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)
  - 尼古拉：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=nicholas&mode=3)、《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作尼古拉。

[N](../Category/教宗.md "wikilink") [N](../Category/羅馬人.md "wikilink")
[N](../Category/基督教聖人.md "wikilink")
[N](../Category/867年逝世.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  [Durant, Will](../Page/Will_Durant.md "wikilink"). The Age of Faith.
    New York: Simon and Schuster. 1972. Chapter XXI: Christianity in
    Conflict 529–1085. p. 517–551