[教宗](../Page/教宗.md "wikilink")**尼古拉五世**（，），原名Tomaso
Parentucelli，1447年3月6日—1455年3月24日在位。

## 譯名列表

  - 尼各老：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm) 作尼各老。
  - 尼閣：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)
    作尼閣。
  - 尼古拉：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=nicholas&mode=3)、《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作尼古拉。

[N](../Category/15世纪意大利人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")