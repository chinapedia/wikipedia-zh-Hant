**尼康D2X**是[尼康于](../Page/尼康.md "wikilink")2004年9月16日推出的一款1240万[像素的](../Page/像素.md "wikilink")[数码单镜反光相机](../Page/数码单镜反光相机.md "wikilink")。

## 尼康D2Xs

**尼康
D2Xs**是一部[尼康生產的](../Page/尼康.md "wikilink")[數位單鏡反光相機](../Page/數位單鏡反光相機.md "wikilink")，於2006年6月推出市面，像素為1240萬像素，是當時的高像素專業級[CMOS](../Page/CMOS.md "wikilink")[數位單鏡反光相機](../Page/數位單鏡反光相機.md "wikilink")，具有11個對焦點。

### 規格

  - [感光元件使用](../Page/感光元件.md "wikilink")[CMOS](../Page/CMOS.md "wikilink")，有效像素為1240萬[像素](../Page/像素.md "wikilink")，最大的[解像度是](../Page/解像度.md "wikilink")4,288
    x 2,848
  - 對焦系統使用尼康Nikon Multi-CAM2000 ，支援AF和AF-F對焦模式。
  - 支援三種[曝光模式](../Page/曝光.md "wikilink")，[光圈先決](../Page/光圈先決.md "wikilink")（A）、[快門先決](../Page/快門先決.md "wikilink")（S）、手動曝光（M）。
  - [快門速度為](../Page/快門.md "wikilink")1/8000至30秒，每秒2-9幅連拍，支援B快門，感光度支援範圍由ISO
    100至ISO 800，並能在增幅模式下達到HI-0.3、HI-0.5、HI-0.7、HI-1、HI-2。
  - 2.5[吋](../Page/吋.md "wikilink")（230,000像素）低溫多晶矽[TFT](../Page/TFT.md "wikilink")[LCD](../Page/LCD.md "wikilink")，顯示屏連光度調控熒光幕，視野率為100%。
  - 使用CF Type I/II
  - 機身重量為1070[克](../Page/克.md "wikilink")，體積為158x150x86[公釐](../Page/公釐.md "wikilink")。

[Category:數位單眼相機](../Category/數位單眼相機.md "wikilink")
[Category:尼康相機](../Category/尼康相機.md "wikilink")
[Category:2004年面世的相機](../Category/2004年面世的相機.md "wikilink")