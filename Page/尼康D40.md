**尼康D40**（**Nikon
D40**）是市面上最为便宜的由[尼康生产的入门级](../Page/尼康.md "wikilink")[单反相机](../Page/单反.md "wikilink")，
该机于2006年11月16日发布。出于节约成本的考虑，一些[尼康
D50具有的特性被简化](../Page/尼康_D50.md "wikilink")。套机的价格为500美元左右(包含镜头AF-s
18-55/3.5-5.6 ED G Dx II)。2007年3月6日, D40x发布，像素由D40的600万提升为1000万。

D40的市场定位很特殊。它的价格和特性都低于市面上竞争对手的同类产品，如[佳能EOS](../Page/佳能.md "wikilink")
400D/Digital Rebel XTi，Pentax K110D, Olympus
E-400,但是相对其它非单反数码相机它的价格非常的有竞争力。伴随D40的问世，一款新的配套镜头AF-S
DX Zoom-Nikkor 18-55mm f/3.5-5.6G ED
II和一款小型外置闪光灯，具备i-TTL特性的SB-400(闪光指数21)也于同时发布。

## 与D50的区别

  - 改进

<!-- end list -->

  - 尺寸更大（對角2.5英寸），具有更高分辨率的显示屏
  - 更高的感光度（由原本的[ISO](../Page/ISO.md "wikilink") 1600提升到ISO 3200）
  - 機身本體重量更轻，体积更小。
  - 增添机内图像处理功能，包含D-Lighting（自動動態範圍亮度補償）、红眼校正、裁切、单色设置、滤镜、缩略图等。
  - 可依照用戶需求設定實際功能的Func按钮。
  - 可顯示三色RGB直方图（相對於原本的单色（照度）直方图）。

<!-- end list -->

  - 简化

<!-- end list -->

  - 用于驱动AF镜头的機身內對焦马达被取消，轉而仰賴鏡頭內的對焦馬達來驅動鏡組。因此在D40上，只有AF-S和AF-I（Internal
    Focus Motor）的鏡頭款式可以自动对焦。
  - 3个[对焦点](../Page/对焦.md "wikilink")（D50为5个）。
  - 机顶LCD被取消，拍摄参数由机背显示屏显示。
  - 直接调节影像檔案格式大小、[白平衡和](../Page/白平衡.md "wikilink")[感光度的功能被取消](../Page/感光度.md "wikilink")。

## 參考資料

<references />

  - [List of Nikon compatible lenses with integrated
    autofocus-motor](../Page/:en:List_of_Nikon_compatible_lenses_with_integrated_autofocus-motor.md "wikilink")

[Category:尼康相機](../Category/尼康相機.md "wikilink")
[Category:数码单反相机](../Category/数码单反相机.md "wikilink")
[Category:2006年面世的相機](../Category/2006年面世的相機.md "wikilink")