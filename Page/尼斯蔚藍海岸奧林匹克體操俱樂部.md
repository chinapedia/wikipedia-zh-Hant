[Allianzcoupdenvoi.jpg](https://zh.wikipedia.org/wiki/File:Allianzcoupdenvoi.jpg "fig:Allianzcoupdenvoi.jpg")
**尼斯蔚藍海岸奧林匹克體操俱樂部**（，，簡稱**OGC
Nice**或更簡單的**Nice**）是一家位於[法國南部](../Page/法國.md "wikilink")[地中海沿岸](../Page/地中海.md "wikilink")[滨海阿尔卑斯省](../Page/滨海阿尔卑斯省.md "wikilink")[行政中心](../Page/行政中心.md "wikilink")[尼斯的足球俱樂部](../Page/尼斯.md "wikilink")，為[法国足球甲级联赛的球隊之一](../Page/法国足球甲级联赛.md "wikilink")，成立於1904年。1927年到2013年球队的主场是[雷伊市政體育場](../Page/雷伊市政體育場.md "wikilink")（Stade
du
Ray，2013年9月OGC尼斯正式启用新球场[安聯里維耶拉球場](../Page/安聯里維耶拉球場.md "wikilink")（Allianz
Riviera）。

## 球會榮譽

  - [法国足球甲级联赛](../Page/法国足球甲级联赛.md "wikilink")
      - 冠軍（4）：1951年、1952年、1956年、1959年
      - 亞軍（3）：1968年、1973年、1976年
  - [法国足球乙级联赛](../Page/法国足球乙级联赛.md "wikilink")
      - 冠軍（4）：1948年、1965年、1970年、1994年
      - 亞軍（1）：1984年
  - [法國盃](../Page/法國盃.md "wikilink")
      - 冠軍（3）：1952年、1954年、1997年
      - 亞軍（1）：1978年
  - [聯賽盃](../Page/法國聯賽盃.md "wikilink")
      - 亞軍（1）：2006年
  - [拉丁盃](../Page/拉丁盃.md "wikilink")
      - 亞軍（1）：1952年

      -
  - 2012国际足联U20 世界杯冠军 
  - 2015欧洲U17 青年足球锦标赛冠军 
  - 2016欧洲U19 青年足球锦标赛冠军

## 資料來源

## 外部連結

  -
[N](../Category/法國足球俱樂部.md "wikilink")
[Category:尼斯](../Category/尼斯.md "wikilink")
[Category:1904年建立的足球俱樂部](../Category/1904年建立的足球俱樂部.md "wikilink")