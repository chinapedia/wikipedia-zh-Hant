**尼日利亞足球甲級聯賽**（**Nigeria Premier
League**）是[尼日利亚国内最高等级的足球联赛](../Page/尼日利亚.md "wikilink")。在2004年NPL取代了原先的尼日利亚联赛，分为1-A和1-B两组，并有降级制度。

## 參賽球隊

2018–19年尼日利亞足球甲級聯賽參賽隊伍共有 24 支。

| 中文名稱                                        | 英文名稱                       | 所在城市                                    | 上季成績              |
| ------------------------------------------- | -------------------------- | --------------------------------------- | ----------------- |
| [洛比星隊](../Page/洛比之星足球俱樂部.md "wikilink")     | Lobi Stars F.C.            | [馬庫爾迪](../Page/馬庫爾迪.md "wikilink")      | 第 1 位             |
| [阿克瓦聯](../Page/阿克瓦聯足球會.md "wikilink")       | Akwa United F.C.           | [烏約](../Page/烏約.md "wikilink")          | 第 2 位             |
| [卡諾支柱](../Page/卡諾支柱足球會.md "wikilink")       | Kano Pillars F.C.          | [卡諾](../Page/卡諾.md "wikilink")          | 第 3 位             |
| [安耶巴](../Page/安耶巴國際足球會.md "wikilink")       | Enyimba International F.C. | [阿巴](../Page/阿巴_\(尼日利亞\).md "wikilink") | 第 4 位             |
| [高原聯](../Page/高原聯足球會.md "wikilink")         | Plateau United F.C.        | [喬斯](../Page/喬斯.md "wikilink")          | 第 5 位             |
| [尼日爾龍捲風](../Page/尼日爾龍捲風足球俱樂部.md "wikilink") | Niger Tornadoes F.C.       | [明納](../Page/明納.md "wikilink")          | 第 6 位             |
| [卡齊納聯](../Page/卡齊納聯足球會.md "wikilink")       | Katsina United F.C.        | [卡齊納](../Page/卡齊納.md "wikilink")        | 第 7 位             |
| [阿比亞勇士](../Page/阿比亞勇士足球會.md "wikilink")     | Abia Warriors F.C.         | [烏穆阿希亞](../Page/烏穆阿希亞.md "wikilink")    | 第 8 位             |
| [河流聯](../Page/河流聯足球會.md "wikilink")         | Rivers United F.C.         | [哈科特港](../Page/哈科特港.md "wikilink")      | 第 9 位             |
| [埃努古流浪](../Page/埃努古流浪者足球俱樂部.md "wikilink")  | Enugu Rangers              | [埃努古](../Page/埃努古.md "wikilink")        | 第 10 位            |
| [MFM FC](../Page/MFM足球會.md "wikilink")      | MFM F.C.                   | [拉哥斯](../Page/拉哥斯.md "wikilink")        | 第 11 位            |
| [納沙拉瓦聯](../Page/納薩拉瓦聯足球俱樂部.md "wikilink")   | Nasarawa United F.C.       | [拉菲亞](../Page/拉菲亞.md "wikilink")        | 第 12 位            |
| [卡內米勇士](../Page/卡內米勇士足球會.md "wikilink")     | El-Kanemi Warriors F.C.    | [邁杜古里](../Page/邁杜古里.md "wikilink")      | 第 13 位            |
| [維基旅行者](../Page/維吉旅行者足球俱樂部.md "wikilink")   | Wikki Tourists F.C.        | [包奇](../Page/包奇.md "wikilink")          | 第 14 位            |
| [卡華拉聯](../Page/卡華拉聯足球會.md "wikilink")       | Kwara United F.C.          | [伊洛林](../Page/伊洛林.md "wikilink")        | 第 15 位            |
| [高勞](../Page/高勞足球會.md "wikilink")           | Go Round F.C.              | [奧莫庫](../Page/奧莫庫.md "wikilink")        | 第 16 位            |
| [烏巴赫](../Page/烏巴赫足球會.md "wikilink")         | Ifeanyi Ubah F.C.          | [內維](../Page/內維.md "wikilink")          | 第 17 位            |
| [陽光星](../Page/陽光星足球會.md "wikilink")         | Sunshine Stars F.C.        | [阿庫雷](../Page/阿庫雷.md "wikilink")        | 第 18 位            |
| [尤比沙漠之星](../Page/尤比沙漠之星體育會.md "wikilink")   | Yobe Desert Stars          | [伊巴丹](../Page/伊巴丹.md "wikilink")        | 第 19 位            |
| [哈特蘭德](../Page/哈特蘭德足球俱樂部.md "wikilink")     | Heartland F.C.             | [奧韋里](../Page/奧韋里.md "wikilink")        | 第 20 位            |
|                                             |                            |                                         | 乙組，第 1 位          |
|                                             |                            |                                         | 乙組，第 2 位          |
|                                             |                            |                                         | 乙組，第 3 位          |
|                                             |                            |                                         | 乙組，第 4 位 \<\!--|- |
| [岡貝聯](../Page/貢貝聯足球俱樂部.md "wikilink")       | Gombe United F.C.          | [貢貝](../Page/貢貝.md "wikilink")          | 乙組，第 1 位          |
| [利蒙星隊](../Page/利蒙星足球會.md "wikilink")        | Remo Stars F.C.            | [伊杰布奧德](../Page/伊杰布奧德.md "wikilink")    | 乙組，第 2 位          |
| [ABS FC](../Page/ABS足球會.md "wikilink")      | ABS F.C.                   | [伊洛林](../Page/伊洛林.md "wikilink")        | 乙組，第 4 位--\>      |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 球會成績

| 球會                                          | 冠軍次數 | 冠軍年份                                     |
| ------------------------------------------- | ---- | ---------------------------------------- |
| [安耶巴](../Page/安耶巴國際足球會.md "wikilink")       | 7    | 2001, 2002, 2003, 2005, 2007, 2010, 2015 |
| [埃努古流浪](../Page/埃努古流浪者足球俱樂部.md "wikilink")  | 7    | 1971, 1974, 1975, 1981, 1982, 1984, 2016 |
| [艾雲恩活國民隊](../Page/哈特蘭德足球俱樂部.md "wikilink")  | 5    | 1987, 1988, 1989, 1990, 1993             |
| [流星](../Page/流星體育會.md "wikilink")           | 5    | 1976, 1980, 1983, 1995, 1998             |
| [卡諾支柱](../Page/卡諾支柱足球會.md "wikilink")       | 4    | 2008, 2012, 2013, 2014                   |
| [杜芬斯](../Page/海豚足球俱樂部.md "wikilink")        | 3    | 1997, 2004, 2011                         |
| [賓度爾燕梳](../Page/班德爾保險足球俱樂部.md "wikilink")   | 2    | 1973, 1979                               |
| [朱利亞斯貝格爾](../Page/布迪治足球會.md "wikilink")     | 2    | 1991, 2000                               |
| [米格提噴射機](../Page/米格提噴射機足球會.md "wikilink")   | 1    | 1972                                     |
| [拉卡流浪](../Page/拉卡流浪足球會.md "wikilink")       | 1    | 1978                                     |
| [新尼日利亞銀行](../Page/新尼日利亞銀行足球會.md "wikilink") | 1    | 1985                                     |
| [利雲迪斯聯](../Page/利雲迪斯聯.md "wikilink")        | 1    | 1986                                     |
| [文具店隊](../Page/文具店足球會.md "wikilink")        | 1    | 1992                                     |
| [BCC雄獅](../Page/BCC雄獅足球俱樂部.md "wikilink")   | 1    | 1994                                     |
| [烏杜基聯](../Page/烏杜基聯足球會.md "wikilink")       | 1    | 1996                                     |
| [洛比星隊](../Page/洛比之星足球俱樂部.md "wikilink")     | 1    | 1999                                     |
| [海洋男孩](../Page/海洋男孩足球俱樂部.md "wikilink")     | 1    | 2006                                     |
| [巴耶爾薩聯](../Page/巴耶爾薩聯足球俱樂部.md "wikilink")   | 1    | 2008–09                                  |
| [高原聯](../Page/高原聯足球會.md "wikilink")         | 1    | 2017                                     |

## 參考

## 外部連結

  - [1](https://web.archive.org/web/20070311093045/http://nigeriasports.com/2461)
    - Nigeria Sports
  - [RSSSF competition
    history](http://www.rsssf.com/tablesn/nigchamp.html)

[非](../Category/國家頂級足球聯賽.md "wikilink")
[Category:尼日利亞足球](../Category/尼日利亞足球.md "wikilink")
[Nigeria](../Category/非洲各國足球聯賽.md "wikilink")

1.