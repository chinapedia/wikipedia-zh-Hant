[Nigerian-Spam-Letter.gif](https://zh.wikipedia.org/wiki/File:Nigerian-Spam-Letter.gif "fig:Nigerian-Spam-Letter.gif")

**尼日利亞騙徒**（又稱為**奈及利亞騙局**或**419騙局**）是國際騙徒以[尼日利亞為名而設的騙局](../Page/尼日利亞.md "wikilink")，與此國家並無直接關係，一直都是各國[警方大為頭痛的問題](../Page/警方.md "wikilink")。尼日利亞由於地域廣闊，加上豐富的[石油及](../Page/石油.md "wikilink")[礦產資源](../Page/礦產.md "wikilink")，使尼日利亞在[非洲各國當中的](../Page/非洲.md "wikilink")[經濟一向比較富裕](../Page/經濟.md "wikilink")。然而，由於其嚴重的[貪污問題以及周邊國家的連年](../Page/貪污.md "wikilink")[內戰](../Page/內戰.md "wikilink")，使不少富有人家都希望透過第三者作出資產轉移，從而保障自己的財富。然而，這卻正成為了騙徒藉以在世界各地行騙的方式。

隨着[科技日新月異](../Page/科技.md "wikilink")，騙徒行騙的方式亦不斷更新。另一方面，亦由於不少人對尼日利亞人的防備加強，這些騙徒開始以鄰近國家（如[多哥等地](../Page/多哥.md "wikilink")）的名義繼續行騙活動。

## 行騙手法

### 假美金

從1960年代開始至1980年代，不少尼日利亞騙徒都會在世界各大城市，特別是經濟正在起飛的[東亞地區的](../Page/東亞.md "wikilink")[酒店裡尋找獵物](../Page/酒店.md "wikilink")。最初他們會聲稱身上的[美金已用盡](../Page/美金.md "wikilink")，但還有一批經特別處理的美金。由於這些美金是非法從尼日利亞轉移，所以被塗上特製的黑油，以避過[海關的檢查](../Page/海關.md "wikilink")。他們手上通常都會有一張至數張經特別處理過的美金作示範，並在受害者面前用特別藥水清洗美金，以證明自己所言不假。之後，他們會看受害者要求，指若受害者願意替他作兌換，他願意以一個較低的匯率來兌換這一批“美金”。基於貪小便宜的心態下，有不少人都會跌入圈套，而用當地的現鈔換來一大疊不能還原的廢紙。有不少受害者更需要額外向騙徒付款購買該種特製的“清潔藥水”。

### 虛假交易

騙徒會聲稱希望跟你購物，但需要你把貨物送往尼日利亞某地。對方會用[支票或](../Page/支票.md "wikilink")[信用卡來付款](../Page/信用卡.md "wikilink")。但當貨物送抵目的地以後，你才發現支票不能兌現，或對方使用虛假的信用卡來購物。

### 支票輪

騙徒會利用[時區的差別](../Page/時區.md "wikilink")，以及各地處理支票所需的時間差，利用一張可以兌現的支票提供保證金作交易，但在交易完成後從戶口撤走資金，使支票不能兌現。

### 虛假遺產

聲稱某人（或你的某個遠方[親戚](../Page/親戚.md "wikilink")）逝世，而對方需要你提供你的個人資料及[銀行戶口號碼](../Page/銀行.md "wikilink")，以便把死者的[遺產存入你的戶口裡](../Page/遺產.md "wikilink")。有時，他們甚至會聲稱要借用你的戶口來作資產轉移，並承諾會以所轉移的資產的某個比例作為報酬。

## 新發展

[ChineseFraud-Sample.png](https://zh.wikipedia.org/wiki/File:ChineseFraud-Sample.png "fig:ChineseFraud-Sample.png")職員的虛假電郵\]\]
基於[亞洲近年的經濟起飛](../Page/亞洲.md "wikilink")，不少騙徒把目標從[歐美人士轉移到東亞地區](../Page/歐美.md "wikilink")。這些騙徒假冒當地的銀行或[律師行職員](../Page/律師行.md "wikilink")，並宣稱有一筆為數可觀的遺產等他們來申領；但其實跟以前一樣，仍然是一個騙局。

### 網路交友詐騙

受害者多是在使用網路交友時遭詐騙，受害者透過網路與騙徒（通常都是使用假造的身份資訊，例如假裝是歐洲的白領階層，或是某國的貴族顯要）交往多時後，詐騙分子利用各種藉口誘騙受害者將金錢匯往他國，且受害者大都是具有高教育背景之女性。此類詐騙中，騙徒通常都是以被害人只需匯出一筆金額較小的匯款，就可以換得鉅額利益作為誘餌讓被害者上鉤（例如要求被害人匯款參與高獲利的投資，或是先得替一筆金額極大的匯款代墊操作費用之類理由），由於騙徒與被害者之間往往還有感情上的關連，導致許多被害者縱使被騙，也堅持不承認自己已經遭到詐騙。\[1\]

## 其他

尼日利亞騙徒透過[垃圾電郵不斷轟炸一般用戶的](../Page/垃圾電郵.md "wikilink")[電郵信箱](../Page/電郵.md "wikilink")，然而其信件內容亦為不少人帶來歡樂。有人特意為這些電郵開設了一個網站，站內的所有錄音都是利用[饒舌的形式來朗讀這些垃圾電郵的內容](../Page/饒舌.md "wikilink")。此外，為向這批撰寫電郵的人士表示敬意，[搞笑諾貝爾獎還特地把](../Page/搞笑諾貝爾獎.md "wikilink")2005年度的文學獎頒發給他們，以表揚他們「利用短小精悍的文字，為我們提供娛樂」。

## 参见

  - [台湾诈骗集团](../Page/台湾诈骗集团.md "wikilink")
  - [網絡釣魚](../Page/钓鱼式攻击.md "wikilink")
  - [垃圾電郵](../Page/垃圾電郵.md "wikilink")

## 參考文獻

## 外部連結

  - [尼日利亞虛假交易電郵展覽館](http://www.potifos.com/fraud/)

<!-- end list -->

  - [Case falls apart](http://news.bbc.co.uk/2/hi/africa/3909233.stm)
  - [The 419 Coalition](http://home.rica.net/alphae/419coal/)
  - [Scamorama presents The Lads From Lagos](http://www.scamorama.com)

<!-- end list -->

  - [How to become rich
    dotcom](https://web.archive.org/web/20050601023505/http://home.arcor.de/ktdykes/wafrica.htm)
  - [U.S. State Department
    Information](http://www.state.gov/www/regions/africa/naffpub.pdf)
  - [419eater.com](http://www.419eater.com/)
  - [419 Legal (South African Police Services)](http://www.419legal.org)
  - [Artists Against 419](http://www.aa419.org/)
  - [Urban Legends Collected
    Scams](http://www.snopes2.com/inboxer/scams/nigeria.htm)
  - [Nigerian Criminal
    Code](http://www.nigeria-law.org/Criminal%20Code%20Act-Part%20VI%20%20to%20the%20end.htm)
  - [The varieties and history of advance fee
    scams](http://samvak.tripod.com/nigerianscam.html)

<!-- end list -->

  - [P-P-P-Powerbook web browser friendly
    edition](http://www.newssocket.com/id/231/)

<!-- end list -->

  - [Scampatrol Victim Support](http://www.scampatrol.org/)
  - [US Secret Service Public Awareness
    Advisory](https://web.archive.org/web/20020802151417/http://www.secretservice.gov/alert419.shtml)

[Category:詐騙](../Category/詐騙.md "wikilink")
[Category:骗局](../Category/骗局.md "wikilink")
[Category:搞笑诺贝尔奖得主](../Category/搞笑诺贝尔奖得主.md "wikilink")
[Category:网络安全](../Category/网络安全.md "wikilink")

1.