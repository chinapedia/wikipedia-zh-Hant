**尼日尔共和国**（）
通稱**尼日**，是[西非](../Page/西非.md "wikilink")[内陆国家之一](../Page/内陆国家.md "wikilink")，因[尼日尔河得名](../Page/尼日尔河.md "wikilink")，首都[尼亚美](../Page/尼亚美.md "wikilink")。东临[乍得](../Page/乍得.md "wikilink")，南界[尼日利亚](../Page/尼日利亚.md "wikilink")、[贝宁](../Page/贝宁.md "wikilink")，西部与[布基纳法索和](../Page/布基纳法索.md "wikilink")[马里毗邻](../Page/马里.md "wikilink")，北部与[阿尔及利亚接壤](../Page/阿尔及利亚.md "wikilink")，东北与[利比亚交界](../Page/利比亚.md "wikilink")。边境总长5500公里。面积126.76万平方公里，是世界上[最低度開發國家](../Page/最低度開發國家.md "wikilink")，亦是[人類發展指數排名最低的國家](../Page/人類發展指數.md "wikilink")(根據2017年所統計的資料)。

## 历史

公元8至16世纪，在尼日尔河中上游地区先后出现过三个强大国家：[加纳帝国](../Page/加纳帝国.md "wikilink")、[马里帝国和](../Page/马里帝国.md "wikilink")[桑海帝国](../Page/桑海帝国.md "wikilink")。11至18世纪在[乍得湖地区兴起了](../Page/乍得湖.md "wikilink")[博尔努帝国](../Page/博尔努帝国.md "wikilink")。加纳王国和马里帝国的统治中心虽然不在尼日尔，但通过贸易往来而对该地区产生了很大影响。而尼日尔的西部和中部都曾经是桑海帝国的一部分。

1897年～1900年被[法国佔领](../Page/法蘭西第三共和國.md "wikilink")，1904年成為法國殖民地（法屬尼日軍管地），1940年代并入[法屬西非](../Page/法屬西非.md "wikilink")。

1960年8月3日獨立，[哈馬尼·迪奧-{里}-成為首任總統](../Page/哈马尼·迪奥里.md "wikilink")。1974年4月，武裝部隊參謀長[賽義尼·孔切發動政變奪權](../Page/賽義尼·孔切.md "wikilink")。孔切一直掌權至1987年11月去世，由[阿-{里}-·賽義布繼任](../Page/阿里·賽義布.md "wikilink")。1993年3月，[馬哈曼·奥斯曼在該國首次民主總統選舉中勝出](../Page/馬哈曼·奥斯曼.md "wikilink")，不過他的政府在1996年1月被軍人發動政變推翻。[坦賈·馬馬杜在](../Page/坦賈·馬馬杜.md "wikilink")1999年11月當選總統，2004年12月成功連任，後於2010年2月18日被軍隊發動政變推翻。\[1\]

## 政治

實行半總統制。總統為國家元首、行政首腦和武裝部隊統帥，通過兩輪多數選舉產生，任期5年，可連選連任一次。即將任滿兩屆總統任期的坦賈在2009年提出修改憲法，更改連任限制，引起爭議。

總理為政府首腦，由總統根據議會多數黨團的提名人選任命，對議會負責。

  - 2004年當選的總統[坦贾·马马杜在](../Page/坦贾·马马杜.md "wikilink")2010年2月的军事政变被推翻。

## 地理

[Fachi-Bilma-Dünen.jpg](https://zh.wikipedia.org/wiki/File:Fachi-Bilma-Dünen.jpg "fig:Fachi-Bilma-Dünen.jpg")\]\]
[Niger_Army_322nd_Parachute_Regiment.jpg](https://zh.wikipedia.org/wiki/File:Niger_Army_322nd_Parachute_Regiment.jpg "fig:Niger_Army_322nd_Parachute_Regiment.jpg")
[To_market.jpg](https://zh.wikipedia.org/wiki/File:To_market.jpg "fig:To_market.jpg")的雜貨店\]\]
[Niamey_Niger.png](https://zh.wikipedia.org/wiki/File:Niamey_Niger.png "fig:Niamey_Niger.png")
尼日領土面積126.7萬平方公里。国土纵横的范围，从北纬11°37′延伸到23°33′，经度上东经0°6′伸展到16°。

### 地形

尼日尔全境地势呈北高南低。按地形特征大致可分为三部分：

北部是[艾尔高原及周围群山](../Page/艾尔高原.md "wikilink")，地势较高，海拔700～1000米。其中位于[塔泽尔扎伊特山区的](../Page/塔泽尔扎伊特.md "wikilink")[格雷本山海拔](../Page/格雷本山.md "wikilink")1997米，是尼日尔的最高点。

中部是[萨赫勒地区](../Page/萨赫勒.md "wikilink")，是[半沙漠草原区](../Page/半沙漠草原区.md "wikilink")，海拔500～800米。

南部是[平原区](../Page/平原.md "wikilink")，地势较平坦，海拔200～300米。

### 河流与湖泊

[尼日尔河是最主要的河流](../Page/尼日尔河.md "wikilink")，另东部還有一条[科马杜古-约贝河](../Page/科马杜古-约贝河.md "wikilink")，流入[乍得湖](../Page/乍得湖.md "wikilink")。此外还有一些[时令河](../Page/时令河.md "wikilink")。总体来说，尼日尔水资源比较贫乏。

### 气候

尼日尔气候属于[干旱热带性](../Page/干旱热带性.md "wikilink")，终年炎热少雨。平均气温摄氏30 °C。雨季从6月到9月，旱季从10月到第二年5月。降水量由西往东逐渐递减。

## 居民与宗教

### 人口

2011年估計尼日尔全国人口1574萬。人口密度为每平方公里5人。人口主要集中在[尼亚美及其周边地区](../Page/尼亚美.md "wikilink")。人口结构相对年轻，65岁以上的老人占总人口的2%。

### 民族

全國有5個主要部族：[豪薩族](../Page/豪薩族.md "wikilink")（佔全國人口56%）、[哲爾馬-桑海族](../Page/哲爾馬-桑海族.md "wikilink")（22%）、[富拉尼人](../Page/富拉尼人.md "wikilink")（8.5%）、[圖阿雷格族](../Page/圖阿雷格族.md "wikilink")（8%）和[卡努里族](../Page/卡努里族.md "wikilink")（4%）。

### 语言

官方語言為[法語](../Page/法語.md "wikilink")，但日常使用的人不多。各部族均有自己的語言，[豪薩語可在全國大部分地區通用](../Page/豪薩語.md "wikilink")。

### 宗教信仰

尼日尔是一个世俗国家，施行政教分离制度，该制度由2010年的宪法所保障，并且规定未来的修订不得改变尼日尔是世俗国家的状态。同时，宪法也保障宗教自由。从10世纪开始广泛传播的伊斯兰教深刻影响了尼日尔的文化和人群。超過99%的居民信奉[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，當中約90%是[遜尼派](../Page/遜尼派.md "wikilink")，約5%是[什葉派](../Page/什葉派.md "wikilink")；其餘居民信奉原始宗教、[基督教等](../Page/基督教.md "wikilink")。

## 经济

### 經濟產業

农业是尼日尔最基本的经济生产部门。但由于长期干旱，粮食长时间不能自给。畜牧业是尼日尔的经济支柱之一，在出口贸易中仅次于铀矿业，在国内生产总值中占15%～20%，占外汇收入的50%。此外尼日爾是鈾元素出口國之一，尼日尔的铀矿发现于20世纪50年代。现已探明储量为21万吨，占世界总储量的11%，居世界第五位。1992年，尼日尔成为仅次于[加拿大](../Page/加拿大.md "wikilink")、[澳大利亚和](../Page/澳大利亚.md "wikilink")[美国的世界第四大产铀国](../Page/美国.md "wikilink")。此外尼日尔的采金业近年来发展迅速。主要产金地是[萨米拉](../Page/萨米拉.md "wikilink")（Samira）和[利比里](../Page/利比里.md "wikilink")（Libiri）。並且尼日尔有比较丰富的[磷酸盐](../Page/磷酸盐.md "wikilink")，储量达12.5亿吨，居世界第四位。此外，尼日尔的[煤炭和](../Page/煤炭.md "wikilink")[石油资源也比较重要](../Page/石油.md "wikilink")。

### 發展程度

尼日爾的發展十分落後，甚至比其他非洲國家還要落後。根據聯合國發布的2017年人類發展指數，尼日爾排行最後，而人均收入亦十分低，在2018年尼日爾人均收入只有約1200美元(購買力平價)。有超過40%的人口每日生活低於1.9美元。

## 行政区划

[Nigerienne_Departments_in_Roman_script_and_in_Chinese_scriptzh.png](https://zh.wikipedia.org/wiki/File:Nigerienne_Departments_in_Roman_script_and_in_Chinese_scriptzh.png "fig:Nigerienne_Departments_in_Roman_script_and_in_Chinese_scriptzh.png")
全國分為7個大區及1個大區級市。

1.  [阿加德茲省](../Page/阿加德茲省.md "wikilink")（）
2.  [迪法省](../Page/迪法省.md "wikilink")（）
3.  [多索省](../Page/多索省.md "wikilink")（）
4.  [馬拉迪省](../Page/馬拉迪省.md "wikilink")（）
5.  [塔瓦省](../Page/塔瓦省.md "wikilink")（）
6.  [蒂拉貝里省](../Page/蒂拉貝里省.md "wikilink")（）
7.  [津德爾省](../Page/津德爾省.md "wikilink")（）
8.  [尼亚美市](../Page/尼亚美.md "wikilink")（，首都）

## 重要城镇

  - [賈多](../Page/賈多.md "wikilink")（）
  - [耶盖巴](../Page/耶盖巴.md "wikilink")（）
  - [比尔马](../Page/比尔马.md "wikilink")（）
  - [阿加德茲](../Page/阿加德茲.md "wikilink")（）
  - [恩吉格米](../Page/恩吉格米.md "wikilink")（）
  - [津德尔](../Page/津德尔.md "wikilink")（）
  - [马拉迪](../Page/马拉迪.md "wikilink")（）
  - [塔努特](../Page/塔努特.md "wikilink")（）
  - [塔瓦](../Page/塔瓦.md "wikilink")（）
  - [尼亚美](../Page/尼亚美.md "wikilink")（）国都

## 外交

尼日尔1963年7月7日与[中华民国建交](../Page/中华民国.md "wikilink")，1974年7月20日改同[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")，1992年7月30日尼日尔再同中华民国复交四年，直至1996年8月19日尼日尔同中华人民共和国二度复交。

## 教育

尼日尔教育水準十分落后，成人识字率只有16.5%，妇女更低，只有7%。

目前尼日尔有两所大学，一所是[尼亚美阿卜杜·穆穆尼大学](../Page/尼亚美阿卜杜·穆穆尼大学.md "wikilink")，另一所是设在[萨伊地区的](../Page/萨伊.md "wikilink")[萨伊伊斯兰大学](../Page/萨伊伊斯兰大学.md "wikilink")。前者是国立大学，后者是[伊斯兰会议组织开设的](../Page/伊斯兰会议组织.md "wikilink")。

## 參考資料

## 外部連結

  - [尼日尔地图](http://www.freeworldmaps.net/africa/niger/map.html)



[\*](../Category/尼日.md "wikilink")
[Category:西非](../Category/西非.md "wikilink")
[Category:前法國殖民地](../Category/前法國殖民地.md "wikilink")
[Category:非洲国家](../Category/非洲国家.md "wikilink")
[Category:伊斯兰会议组织成员国](../Category/伊斯兰会议组织成员国.md "wikilink")
[Category:內陸國家](../Category/內陸國家.md "wikilink")
[Category:共和國](../Category/共和國.md "wikilink")
[Category:法語國家地區](../Category/法語國家地區.md "wikilink")
[Category:1960年建立的國家或政權](../Category/1960年建立的國家或政權.md "wikilink")

1.  [《尼-{日}-尔发生兵变总统被扣》](http://www.bbc.co.uk/zhongwen/simp/world/2010/02/100218_niger_coup.shtml)，BBC中文網，2010年2月18日。