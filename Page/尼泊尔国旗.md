**尼泊爾國旗**為[尼泊爾的](../Page/尼泊爾.md "wikilink")[國旗](../Page/國旗.md "wikilink")，而其造型為兩個[直角三角形所組成](../Page/直角三角形.md "wikilink")，是目前世界上唯一的三角型國旗，也是唯一縱大於横的國家旗幟。

## 設計

[月亮與](../Page/月亮.md "wikilink")[太陽代表尼泊爾為](../Page/太陽.md "wikilink")[印度教的國家](../Page/印度教.md "wikilink")，也象徵國家能與日月共長久。

《尼泊爾聯邦民主共和國憲法》的第5條附表1中，詳細地描述了如何繪製該國的國旗。\[1\]

## 历史国旗

Flag_of_Nepal_(19th_century-1962).svg|19世紀–1962年以前

## 引用來源

## 外部連結

  - [國旗歷史網-尼泊爾](http://www.flags.idv.tw/tama/wfh/pg-as-3.htm#303)

## 參見

  - [尼泊爾](../Page/尼泊爾.md "wikilink")

{{-}}

[Category:尼泊尔国家象征](../Category/尼泊尔国家象征.md "wikilink")
[Nepal](../Category/亞洲國旗.md "wikilink")
[Category:1962年面世的旗幟](../Category/1962年面世的旗幟.md "wikilink")

1.  [Constitution of the Kingdom of Nepal, Article 5,
    Schedule 1](http://www.servat.unibe.ch/icl/np01000_.html)