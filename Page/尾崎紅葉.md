**尾崎紅葉**（，即[慶應三年](../Page/慶應.md "wikilink")12月16日－[明治三十六年](../Page/明治.md "wikilink")），[日本](../Page/日本.md "wikilink")[小說家](../Page/小說家.md "wikilink")。本名：德太郎。別號「緣山」、「半可通人」、「十千萬堂」。

## 生平

1868年出生於[江戶](../Page/江戶.md "wikilink")（現今[東京](../Page/東京.md "wikilink")）。[東京帝國大學](../Page/東京帝國大學.md "wikilink")（現今[東京大學](../Page/東京大學.md "wikilink")）國文科中退。

[明治](../Page/明治.md "wikilink")18年（1885年），與山田美妙、硯友社發行文學[同人誌](../Page/同人誌.md "wikilink")《[我樂多文庫](../Page/我樂多文庫.md "wikilink")》。[明治](../Page/明治.md "wikilink")30年（1897年）連載《[金色夜叉](../Page/金色夜叉.md "wikilink")》，可惜此作未完成，作者就去世了，此作成為未完成之作。尾崎紅葉是[泉鏡花](../Page/泉鏡花.md "wikilink")、[小栗風葉](../Page/小栗風葉.md "wikilink")、[柳川春葉](../Page/柳川春葉.md "wikilink")、[德田秋聲的老師](../Page/德田秋聲.md "wikilink")。

## 年譜

  - [慶應](../Page/慶應.md "wikilink")３年（1867年），出生於江戶芝中門前町（現今[東京](../Page/東京.md "wikilink")[濱松町](../Page/濱松町.md "wikilink")）
  - [明治](../Page/明治.md "wikilink")16年（1883年），進入舊制第一高等學校（[東京大學前身](../Page/東京大學.md "wikilink")）
  - [明治](../Page/明治.md "wikilink")18年（1885年）
      - 2月、與[山田美妙](../Page/山田美妙.md "wikilink")、[石橋思案](../Page/石橋思案.md "wikilink")、[丸岡九華等人組成文學結社](../Page/丸岡九華.md "wikilink")「硯友社」。
      - 5月、發行「[我樂多文庫](../Page/我樂多文庫.md "wikilink")」。
  - [明治](../Page/明治.md "wikilink")20年（1887年）4月、入[東京女子専門学校擔任漢學教師](../Page/東京女子専門学校.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")22年（1889年）
      - 4月、出版『二人比丘尼色懺悔』。
      - 12月、進入[讀賣新聞社工作](../Page/讀賣新聞.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")23年（1890年）自[帝国大学退学](../Page/帝国大学.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")24年（1891年）3月10日、與樺島喜久結婚。
  - [明治](../Page/明治.md "wikilink")25年（1892年）3月、「三人妻」在『[讀賣新聞](../Page/讀賣新聞.md "wikilink")』連載。
  - [明治](../Page/明治.md "wikilink")26年（1893年）
      - 1月10日、長男弓之助誕生（早逝）。
      - 6月、「心の闇」在『[讀賣新聞](../Page/讀賣新聞.md "wikilink")』連載。
  - [明治](../Page/明治.md "wikilink")27年（1894年）
      - 2月3日、長女藤枝誕生。
      - 同21日、父親惣蔵去世。
  - [明治](../Page/明治.md "wikilink")29年（1896年）
      - 2月、「多情多恨」在『[讀賣新聞](../Page/讀賣新聞.md "wikilink")』連載。
      - 3月10日、次女彌生出生。
  - [明治](../Page/明治.md "wikilink")30年（1897年）1月、「[金色夜叉](../Page/金色夜叉.md "wikilink")」在『[讀賣新聞](../Page/讀賣新聞.md "wikilink")』連載。
  - [明治](../Page/明治.md "wikilink")32年（1899年）、健康を害する。6月に塩原、7月から8月にかけて新潟へ赴く。
  - [明治](../Page/明治.md "wikilink")33年（1900年）3月26日、三女三千代誕生。
  - [明治](../Page/明治.md "wikilink")34年（1901年）
      - 5月、赴修善寺療養身體。
      - 同20日、次男夏彥誕生。
  - [明治](../Page/明治.md "wikilink")35年（1902年）、辭去在[讀賣新聞社的工作](../Page/讀賣新聞社.md "wikilink")。同年進入[二六新報工作](../Page/二六新報.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")36年（1903年）10月30日、在[牛込區](../Page/牛込區.md "wikilink")[橫井町的自宅中死去](../Page/橫井町.md "wikilink")。

## 主要作品

  - [二人比丘尼色懺悔](../Page/二人比丘尼色懺悔.md "wikilink")
  - [伽羅枕](../Page/伽羅枕.md "wikilink")
  - [多情多恨](../Page/多情多恨.md "wikilink")
  - [青葡萄](../Page/青葡萄.md "wikilink")
  - [金色夜叉](../Page/金色夜叉.md "wikilink")

## 資料來源

  - [尾崎紅葉](http://ja.wikipedia.org/wiki/%E5%B0%BE%E5%B4%8E%E7%B4%85%E8%91%89#.E9.96.80.E4.B8.8B.E7.94.9F)

## 外部連結

  - [日本史上第一本同人誌](http://tw.knowledge.yahoo.com/question/question?qid=1105060711763)

[Category:日本文學家](../Category/日本文學家.md "wikilink")