**尾張國**（），[日本古代的](../Page/日本.md "wikilink")[令制國之一](../Page/令制國.md "wikilink")，屬[東海道](../Page/東海道.md "wikilink")，亦稱“尾州”（），其國領約為現在[愛知縣的西部](../Page/愛知縣.md "wikilink")。

## 郡

  - [知多郡](../Page/知多郡.md "wikilink")
  - [愛知郡](../Page/愛知郡_\(愛知縣\).md "wikilink")
  - [春日井郡](../Page/春日井郡.md "wikilink")（[東春日井郡](../Page/東春日井郡.md "wikilink")、[西春日井郡](../Page/西春日井郡.md "wikilink")）
  - [丹羽郡](../Page/丹羽郡.md "wikilink")
  - [葉栗郡](../Page/葉栗郡.md "wikilink")
  - [中島郡](../Page/中島郡.md "wikilink")
  - [海東郡](../Page/海東郡.md "wikilink")
  - [海西郡](../Page/海西郡.md "wikilink")

## 人物

### 尾張守

  - [藤原真川](../Page/藤原真川.md "wikilink")（从五位下）：[延历](../Page/延历.md "wikilink")25年（806年）任官
  - [多入鹿](../Page/多入鹿.md "wikilink")（从五位下）：[大同](../Page/大同_\(日本\).md "wikilink")2年（807年）任官
  - [平群真常](../Page/平群真常.md "wikilink")（从五位上）：[大同](../Page/大同_\(日本\).md "wikilink")3年（808年）任官
  - [秋篠安人](../Page/秋篠安人.md "wikilink")（从四位上）：[大同](../Page/大同_\(日本\).md "wikilink")5年（810年）任官
  - [紀田上](../Page/紀田上.md "wikilink")（从四位下）：[大同](../Page/大同_\(日本\).md "wikilink")5年（810年）任官
  - [安倍犬養](../Page/安倍犬養.md "wikilink")（从五位下）：[弘仁元年](../Page/弘仁.md "wikilink")（809年）任官
  - [三原弟平](../Page/三原弟平.md "wikilink")（从五位上）：[弘仁](../Page/弘仁.md "wikilink")4年（812年）任官
  - [滋野家訳](../Page/滋野家訳.md "wikilink")（从五位下）：[弘仁](../Page/弘仁.md "wikilink")6年（814年）任官
  - [伴氏上](../Page/伴氏上.md "wikilink")（从五位下）：[天長](../Page/天長.md "wikilink")2年（825年）任官
  - [路年継](../Page/路年継.md "wikilink")（从四位下）：[天長](../Page/天長.md "wikilink")3年（826年）任官
  - [藤原助](../Page/藤原助.md "wikilink")（从四位下）：[承和](../Page/承和_\(日本\).md "wikilink")4年（837年）任官
  - [橘氏人](../Page/橘氏人.md "wikilink")（从四位上）：[承和](../Page/承和_\(日本\).md "wikilink")8年（841年）任官
  - [源弘](../Page/源弘.md "wikilink")（从四位下）：[承和](../Page/承和_\(日本\).md "wikilink")12年（845年）任官
  - [源定](../Page/源定.md "wikilink")（从三位）：[承和](../Page/承和_\(日本\).md "wikilink")15年（848年）任官
  - [滋野貞主](../Page/滋野貞主.md "wikilink")（从四位下）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")2年（849年）任官
  - [源定](../Page/源定.md "wikilink")（从三位）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")3年（850年）任官
  - [南淵年名](../Page/南淵年名.md "wikilink")（从五位下）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")3年（850年）任官
  - [滋野貞主](../Page/滋野貞主.md "wikilink")（从四位下）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")3年（850年）任官
  - [橘数岑](../Page/橘数岑.md "wikilink")（从五位下）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")3年（850年）任官
  - [源定](../Page/源定.md "wikilink")（从三位）：[嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")4年（85１年）任官
      - [豊階安人](../Page/豊階安人.md "wikilink")（从五位上）：[仁寿](../Page/仁寿.md "wikilink")4年（854年）任官（权守）
  - [藤原宗善](../Page/藤原宗善.md "wikilink")（从五位上）：[齐衡](../Page/齐衡.md "wikilink")2年（855年）任官
  - [飯高永雄](../Page/飯高永雄.md "wikilink")（从五位下）：[天安](../Page/天安_\(日本\).md "wikilink")2年（858年）任官
      - [藤原常永](../Page/藤原常永.md "wikilink")（从五位上）：[天安](../Page/天安_\(日本\).md "wikilink")2年（858年）任官（权守）
  - [安倍房上](../Page/安倍房上.md "wikilink")（从五位下）：[貞観](../Page/贞观_\(日本\).md "wikilink")4年（862年）任官
  - [吉備全継](../Page/吉備全継.md "wikilink")（从五位下）：[貞観](../Page/贞观_\(日本\).md "wikilink")9年（867年）任官
      - [家原氏主](../Page/家原氏主.md "wikilink")（从五位上）：[貞観](../Page/贞观_\(日本\).md "wikilink")10年（868年）任官（权守）
  - [南淵興世](../Page/南淵興世.md "wikilink")（从五位下）：[貞観](../Page/贞观_\(日本\).md "wikilink")13年（871年）任官
  - [藤原村椙](../Page/藤原村椙.md "wikilink")（从五位下）：[貞観](../Page/贞观_\(日本\).md "wikilink")16年（874年）任官
      - [藤原高藤](../Page/藤原高藤.md "wikilink")（从五位上）：[元慶](../Page/元慶.md "wikilink")3年（879年）任官（权守）
  - [藤原高藤](../Page/藤原高藤.md "wikilink")（从五位上）：[元慶](../Page/元慶.md "wikilink")5年（881年）任官
  - [清原常岑](../Page/清原常岑.md "wikilink")（从五位下）：[元慶](../Page/元慶.md "wikilink")8年（884年）任官
  - [源有](../Page/源有.md "wikilink")（从四位下）：[仁和](../Page/仁和.md "wikilink")3年（887年）任官
      - [藤原定方](../Page/藤原定方.md "wikilink")（从五位下）：[宽平](../Page/宽平.md "wikilink")8年（896年）任官（权守）
      - [平伊望](../Page/平伊望.md "wikilink")（从五位下）：[昌泰](../Page/昌泰.md "wikilink")3年（900年）任官（权守）
  - [橘秘樹](../Page/橘秘樹.md "wikilink")：[延長](../Page/延長_\(醍醐天皇\).md "wikilink")9年（931年）任官
  - [藤原共理](../Page/藤原共理.md "wikilink")：[天慶](../Page/天慶.md "wikilink")2年（939年）任官
  - [源宗海](../Page/源宗海.md "wikilink")：[天慶](../Page/天慶.md "wikilink")2年（939年）任官
  - [藤原興方](../Page/藤原興方.md "wikilink")（从五位下）：[天慶](../Page/天慶.md "wikilink")6年（942年）任官
  - [藤原為輔](../Page/藤原為輔.md "wikilink")（从五位下）：[天历](../Page/天历.md "wikilink")9年（955年）任官
  - [藤原文正](../Page/藤原文正.md "wikilink")：[天德](../Page/天德_\(村上天皇\).md "wikilink")3年（959年）任官
  - [藤原守平](../Page/藤原守平.md "wikilink")（从五位上）：[应和](../Page/应和.md "wikilink")3年（963年）任官
  - [橘桓平](../Page/橘桓平.md "wikilink")（从五位上）：[康保](../Page/康保.md "wikilink")4年（967年）任官
      - [源忠清](../Page/源忠清.md "wikilink")（正四位上）：[天禄](../Page/天禄.md "wikilink")2年（971年）任官（权守）
  - [藤原永頼](../Page/藤原永頼.md "wikilink")：[天延](../Page/天延.md "wikilink")2年（975年）任官
      - [菅原文時](../Page/菅原文時.md "wikilink")（正四位下）：[天元](../Page/天元_\(圓融天皇\).md "wikilink")3年（980年）任官（权守）
      - [菅原公任](../Page/菅原公任.md "wikilink")（从四位上）：[永观](../Page/永观.md "wikilink")2年（984年）任官（权守）
  - [藤原元命](../Page/藤原元命.md "wikilink")：[宽和](../Page/宽和.md "wikilink")2年（986年）任官
      - [藤原道綱](../Page/藤原道綱.md "wikilink")（从三位）：[永延](../Page/永延.md "wikilink")2年（988年）任官（权守）
  - [藤原文信](../Page/藤原文信.md "wikilink")：[永延](../Page/永延.md "wikilink")3年（989年）
      - [大江匡衡](../Page/大江匡衡.md "wikilink")（从五位上）：[正历](../Page/正历.md "wikilink")3年（992年）任官（权守）
  - [藤原里兼](../Page/藤原里兼.md "wikilink")（正四位下）：[長德](../Page/長德.md "wikilink")2年（996年）任官
  - [藤原知光](../Page/藤原知光.md "wikilink")：[長德](../Page/長德.md "wikilink")3年（997年）任官
  - [大江匡衡](../Page/大江匡衡.md "wikilink")（从四位下）：[長保](../Page/長保.md "wikilink")3年（1001年）任官
      - [藤原正光](../Page/藤原正光.md "wikilink")（正四位下）：[長保](../Page/長保.md "wikilink")5年（1003年）任官（权守）
      - [藤原実成](../Page/藤原実成.md "wikilink")（从四位下）：[長保](../Page/長保.md "wikilink")6年（1004年）任官（权守）
  - [藤原中清](../Page/藤原中清.md "wikilink")：[宽弘](../Page/宽弘.md "wikilink")2年（1005年）任官
  - [大江匡衡](../Page/大江匡衡.md "wikilink")（从四位下）：[宽弘](../Page/宽弘.md "wikilink")6年（1009年）任官
  - [藤原知光](../Page/藤原知光.md "wikilink")：[宽弘](../Page/宽弘.md "wikilink")7年（1010年）任官
  - [橘経国](../Page/橘経国.md "wikilink")：[長和](../Page/長和.md "wikilink")4年（1015年）任官
      - [藤原良経](../Page/藤原良経.md "wikilink")：[宽仁](../Page/宽仁.md "wikilink")2年（1018年）任官（权守）
  - [藤原惟貞](../Page/藤原惟貞.md "wikilink")：[宽仁](../Page/宽仁.md "wikilink")3年（1019年）任官
  - [源則理](../Page/源則理.md "wikilink")：[治安](../Page/治安_\(後一條天皇\).md "wikilink")3年（1023年）任官
  - [平惟忠](../Page/平惟忠.md "wikilink")：[長元](../Page/長元.md "wikilink")2年（1029年）任官
  - [源実基](../Page/源実基.md "wikilink")：[長元](../Page/長元.md "wikilink")8年（1035年）任官
  - [藤原範永](../Page/藤原範永.md "wikilink")：[長元](../Page/長元.md "wikilink")10年（1037年）任官
  - [橘俊綱](../Page/橘俊綱.md "wikilink")：[長久](../Page/長久.md "wikilink")3年（1042年）任官
  - [藤原公基](../Page/藤原公基.md "wikilink")（从五位上）：[宽德](../Page/宽德.md "wikilink")2年（1045年）任官
  - [藤原時房](../Page/藤原時房.md "wikilink")：[康平](../Page/康平.md "wikilink")3年（1060年）任官
  - [平定家](../Page/平定家.md "wikilink")：[治历元年](../Page/治历.md "wikilink")（1065年）任官
      - [源李宗](../Page/源李宗.md "wikilink")：[治历](../Page/治历.md "wikilink")4年（1068年）任官（权守）
  - [藤原憲房](../Page/藤原憲房.md "wikilink")：[治历](../Page/治历.md "wikilink")5年（1069年）任官
      - [賀茂道平](../Page/賀茂道平.md "wikilink")：[延久](../Page/延久.md "wikilink")2年（1070年）任官（权守）
  - [藤原惟経](../Page/藤原惟経.md "wikilink")：[延久](../Page/延久.md "wikilink")5年（1073年）任官
  - [平忠盛](../Page/平忠盛.md "wikilink")
  - [平重衡](../Page/平重衡.md "wikilink")
  - [坊門忠清](../Page/坊門忠清.md "wikilink")
  - [名越時章](../Page/名越時章.md "wikilink")
  - [名越高家](../Page/名越高家.md "wikilink")

### 守護

#### 鎌倉幕府

  - 1195年 - ? : [小野成綱](../Page/小野成綱.md "wikilink")
  - ? - 1221年 : [小野盛綱](../Page/小野盛綱.md "wikilink")
  - 1240年 - ? : [中条家平](../Page/中条家平.md "wikilink")
  - 1252年 - ? : [中条頼平](../Page/中条頼平.md "wikilink")
  - 1290年 - ? : [中条景長](../Page/中条景長.md "wikilink")
  - 1314年 - ? : 北条氏一門（名越氏?）
  - ? - 1333年 : [北条宗教](../Page/北条宗教.md "wikilink")

#### 室町幕府

  - 1336年 - 1338年 : [中条秀長](../Page/中条秀長.md "wikilink")
  - 1339年 - 1340年 : [高師泰](../Page/高師泰.md "wikilink")
  - 1351年 - 1387年 : [土岐頼康](../Page/土岐頼康.md "wikilink")
  - 1388年 - 1391年 : [土岐満貞](../Page/土岐満貞.md "wikilink")
  - 1391年 - ? : [一色詮範](../Page/一色詮範.md "wikilink")
  - 1392年 - ? : [畠山深秋](../Page/畠山深秋.md "wikilink")
  - 1393年 - 1398年 : [今川仲秋](../Page/今川仲秋.md "wikilink")
  - 1398年 : [今川氏兼](../Page/今川氏兼.md "wikilink")
  - 1398年 - ? : [畠山基国](../Page/畠山基国.md "wikilink")
  - 1400年 - 1418年 : [斯波義重](../Page/斯波義重.md "wikilink")
  - 1424年 - 1433年 : [斯波義淳](../Page/斯波義淳.md "wikilink")
  - 1433年 - 1436年 : [斯波義郷](../Page/斯波義郷.md "wikilink")
  - 1436年 - 1452年 : [斯波義健](../Page/斯波義健.md "wikilink")
  - 1452年 - 1460年 : [斯波義敏](../Page/斯波義敏.md "wikilink")
  - 1460年 - 1461年 : [斯波義寛](../Page/斯波義寛.md "wikilink")
  - 1461年 - 1466年 : [斯波義廉](../Page/斯波義廉.md "wikilink")
  - 1466年 : 斯波義敏
  - 1466年 - 1468年 : 斯波義廉
  - 1468年 - 1499年 : 斯波義寛
  - 1511年 - 1517年 : [斯波義達](../Page/斯波義達.md "wikilink")
  - 1536年 - 1554年 : [斯波義統](../Page/斯波義統.md "wikilink")
  - 1554年 - 1556年 : [斯波義銀](../Page/斯波義銀.md "wikilink")

### 戦国時代

  - 戰國大名
      - [斯波氏](../Page/斯波氏.md "wikilink")（尾張守護）
      - [織田氏](../Page/織田氏.md "wikilink")（尾張守護代）
  - 織田政権大名
      - [池田恒興](../Page/池田恒興.md "wikilink")（[犬山城](../Page/犬山城.md "wikilink")）：1万貫。1570年
        - 1580年（移封至攝津）
  - 豐臣政権大名
      - [織田信雄](../Page/織田信雄.md "wikilink")（[清洲城](../Page/清洲城.md "wikilink")）：尾張・伊勢・伊賀110万石、1582年
        - 1590年（改易）
      - [豐臣秀次](../Page/豐臣秀次.md "wikilink")（[清洲城](../Page/清洲城.md "wikilink")）：尾張・伊勢100万石、1590年
        - 1595年（切腹、改易）
      - [福島正則](../Page/福島正則.md "wikilink")（[清洲城](../Page/清洲城.md "wikilink")）：尾張国内24万石、1595年
        - 1600年（移封至安芸[廣島藩](../Page/廣島藩.md "wikilink")，49万8,200石）
      - [一柳直盛](../Page/一柳直盛.md "wikilink")（[黒田城](../Page/黒田城_\(尾張国\).md "wikilink")）：3万5千石、1595年
        - 1600年（移封至伊勢[神戸藩](../Page/神戸藩.md "wikilink")，5万石）
      - [石川貞清](../Page/石川貞清.md "wikilink")（[犬山城](../Page/犬山城.md "wikilink")）：1万2千石、1595年
        - 1600年（改易）

### 武家官位尾張守

#### 江戸時代以前

  - [斯波氏武衛家](../Page/斯波氏.md "wikilink")（尾張足利氏）
      - [足利家氏](../Page/足利家氏.md "wikilink")（斯波家氏）：斯波氏初代。鎌倉時代中期武将
      - [斯波宗家](../Page/斯波宗家.md "wikilink")：2代当主。鎌倉時代中期武将。
      - [斯波宗氏](../Page/斯波宗氏.md "wikilink")：3代当主。鎌倉時代中期至後期武将
      - [斯波高経](../Page/斯波高経.md "wikilink")：4代当主。室町幕府越前・若狹・越中守護
      - [斯波義淳](../Page/斯波義淳.md "wikilink")：7代当主。室町幕府[管領](../Page/管領.md "wikilink")、越前・尾張・遠江守護
  - 室町幕府守護[畠山氏](../Page/畠山氏.md "wikilink")（尾州家）
      - [畠山満家](../Page/畠山満家.md "wikilink")：本宗家10代当主。室町幕府管領、河內・紀伊・越中・伊勢・山城守護
      - [畠山持富](../Page/畠山持富.md "wikilink")：本宗家11代・尾州家初代当主。室町幕府管領、紀伊・河內・越中守護
      - [畠山政長](../Page/畠山政長.md "wikilink")：本宗家14代・尾州家3代当主。室町幕府管領、紀伊・河內・越中・山城守護
      - [畠山尚順](../Page/畠山尚順.md "wikilink")：本宗家16代・尾州家4代当主。紀伊・河內・越中守護
      - [畠山稙長](../Page/畠山稙長.md "wikilink")：本宗家17代・尾州家5代当主。紀伊・河內・越中守護
      - [畠山政国](../Page/畠山政国.md "wikilink")：本宗家19代・尾州家7代当主。紀伊・河內守護。
      - [畠山高政](../Page/畠山高政.md "wikilink")：本宗家21代・尾州家9代当主。紀伊・河內守護
  - 周防国・長門国守護・戰国大名大內氏譜代重臣陶氏
      - [陶弘護](../Page/陶弘護.md "wikilink")：8代当主。室町時代武将。周防・筑前守護代
      - [陶興房](../Page/陶興房.md "wikilink")：9代当主。戰国時代武将。周防守護代
      - [陶晴賢](../Page/陶晴賢.md "wikilink")（陶隆房）：第10代当主。戰国時代武将。周防守護代。握有大內氏實權。
  - 其他
      - [高師泰](../Page/高師泰.md "wikilink")：南北朝時代武将。[高師直兄](../Page/高師直.md "wikilink")
      - [新発田長敦](../Page/新発田長敦.md "wikilink")：戰国時代武将。[上杉謙信七手組大将其中一人](../Page/上杉謙信.md "wikilink")
      - [織田信長](../Page/織田信長.md "wikilink")：攻陷美濃後、自稱尾張守（之前的身份是上總介）
      - [松田憲秀](../Page/松田憲秀.md "wikilink")：[小田原北条氏家老](../Page/後北条氏.md "wikilink")。[小田原征伐之際](../Page/小田原征伐.md "wikilink")、企圖充當豐臣軍內應

## 相關項目

  - [日本令制國列表](../Page/日本令制國列表.md "wikilink")

[Category:令制國](../Category/令制國.md "wikilink")