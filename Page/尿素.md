**尿素**是由[碳](../Page/碳.md "wikilink")、[氮](../Page/氮.md "wikilink")、[氧和](../Page/氧.md "wikilink")[氢组成的](../Page/氢.md "wikilink")[有机化合物](../Page/有机化合物.md "wikilink")，又稱**脲**（與尿同音）。其化学式为
、 或 ，分子质量60，[国际非专利药品名称为](../Page/国际非专利药品名称.md "wikilink")
（碳酰胺）。外观是无色晶体或粉末，是动物蛋白质代谢后的产物，通常用作植物的氮肥。

尿素在[肝合成](../Page/肝.md "wikilink")，是[哺乳类动物排出的体内含氮代谢物](../Page/哺乳类动物.md "wikilink")。這代謝过程称为[尿素循环](../Page/尿素循环.md "wikilink")。

尿素是第一种以[人工合成无机物质而得到的有机化合物](../Page/维勒尿素合成.md "wikilink")。[活力论從此被推翻](../Page/活力论.md "wikilink")。

## 发现

1773年，（）发现尿素。1828年，[弗里德里希·维勒首次使用无机物质](../Page/弗里德里希·维勒.md "wikilink")[氰酸钾与](../Page/氰酸钾.md "wikilink")[硫酸铵人工合成了尿素](../Page/硫酸铵.md "wikilink")。本来他打算合成[氰酸铵](../Page/氰酸铵.md "wikilink")（，NH<sub>4</sub>NCO），卻得到了尿素。

  -
    [Urea_Synthesis_Woehler.png](https://zh.wikipedia.org/wiki/File:Urea_Synthesis_Woehler.png "fig:Urea_Synthesis_Woehler.png")

由此证明了[活力论是错误的](../Page/活力论.md "wikilink")，事实上开辟了[有机化学](../Page/有机化学.md "wikilink")。活力论认为[无机物与](../Page/无机物.md "wikilink")[有机物有根本性差異](../Page/有机物.md "wikilink")，所以无机物无法變成有机物。[哺乳动物](../Page/哺乳动物.md "wikilink")、[两栖动物和一些](../Page/两栖动物.md "wikilink")[鱼的](../Page/鱼.md "wikilink")[尿中含有尿素](../Page/尿.md "wikilink")；[鸟和](../Page/鸟.md "wikilink")[爬行动物排放的是](../Page/爬行动物.md "wikilink")[尿酸](../Page/尿酸.md "wikilink")，因為其氮代谢过程使用的水量比较少。

## 生理

尿素在[肝脏产生后融入](../Page/肝脏.md "wikilink")[血液](../Page/血液.md "wikilink")（人体内的浓度在每升2.5至7.5微摩尔/升之间），最后通过[肾脏由](../Page/肾脏.md "wikilink")[尿排出](../Page/尿.md "wikilink")。少量尿素由[汗排出](../Page/汗.md "wikilink")。

生物以[二氧化碳](../Page/二氧化碳.md "wikilink")、[水](../Page/水.md "wikilink")、[天冬氨酸和](../Page/天冬氨酸.md "wikilink")[氨等化学物質合成尿素](../Page/氨.md "wikilink")。促使尿素合成的[代謝途徑是一種](../Page/代謝途徑.md "wikilink")[合成代谢](../Page/合成代谢.md "wikilink")，叫做[尿素循环](../Page/尿素循环.md "wikilink")。此过程耗费能量，卻很必要。因为氨有毒，且是常见的[新陈代谢产物](../Page/新陈代谢.md "wikilink")，必须被消除。肝脏在合成尿素時，需要[N-乙酰谷氨酸作為调节](../Page/N-乙酰谷氨酸.md "wikilink")。

含氮废物具有毒性，产生自[蛋白质和](../Page/蛋白质.md "wikilink")[氨基酸的](../Page/氨基酸.md "wikilink")[分解代谢过程](../Page/分解代谢.md "wikilink")。大多数生物必须再处理之。海生生物通常直接以氨的形式排入[海水](../Page/海水.md "wikilink")。陆地生物则转化氨为尿素或[尿酸再排出](../Page/尿酸.md "wikilink")。鸟和爬行动物通常排泄尿酸，其它动物（如[哺乳动物](../Page/哺乳动物.md "wikilink")）则是尿素。例外如，水生的[蝌蚪排泄氨](../Page/蝌蚪.md "wikilink")，但在其[蜕变过程转为排泄尿素](../Page/蜕变.md "wikilink")；[大麥町狗主要排泄尿酸](../Page/大麥町.md "wikilink")，不是尿素，因為其尿素循环中的一个转换酶的基因坏了。

哺乳动物以肝脏中的一个循环反应产生尿素。这循环最早在1932年被提出，其反应起点是氨的分解。1940年代澄清[瓜氨酸和](../Page/瓜氨酸.md "wikilink")[精氨基琥珀酸的作用后](../Page/精氨基琥珀酸.md "wikilink")，它已完全被理解。在這循环中，来自氨和
L-天冬氨酸的[氨基被转换为尿素](../Page/氨基.md "wikilink")，起中介作用的是
L-[鳥氨酸](../Page/鳥氨酸.md "wikilink")、[瓜氨酸](../Page/瓜氨酸.md "wikilink")、L-[精氨酸](../Page/精氨酸.md "wikilink")-[琥珀酸和](../Page/琥珀酸.md "wikilink")
L-精氨酸。

尿素循环是[哺乳动物和](../Page/哺乳动物.md "wikilink")[两栖动物排泄含氮代谢废物的主要途径](../Page/两栖动物.md "wikilink")。但別種生物亦然，如[鸟類](../Page/鸟類.md "wikilink")、[无脊椎动物](../Page/无脊椎动物.md "wikilink")、[昆虫](../Page/昆虫.md "wikilink")、[植物](../Page/植物.md "wikilink")、[酵母](../Page/酵母.md "wikilink")、[真菌和](../Page/真菌.md "wikilink")[微生物](../Page/微生物.md "wikilink")。

尿素對生物基本是废物，但仍有正面價值。比如，[肾小管裡的尿素被引入](../Page/肾小管.md "wikilink")[肾皮质以提高其渗透浓度](../Page/肾皮质.md "wikilink")，促使水份从肾小管[渗透回身體再利用](../Page/渗透.md "wikilink")。

## 生产

全世界每年工业的尿素产量约为十亿吨。中国目前尿素产能在6400万吨，年产量约5700万吨。商业尿素是通过氨与二氧化碳的反应生产的，成品尿素可以为药片状、颗粒状、片状、晶体或者溶液。尿素一般以药片或者颗粒的形式上市。

90%以上的生产的尿素被用作[肥料](../Page/肥料.md "wikilink")。在所有的一般使用的估计氮肥料中尿素的含氮量最高（46.4%），因此相对而言其每氮营养的运输费最低。尿素在水中的可溶性非常高，因此非常适合被加在可溶的肥料中。

### 生产反应

商用尿素的原料是氨与二氧化碳。後者在以焦炭或[烃](../Page/烃.md "wikilink")（如天然气和石油）為原料生产氨的过程中，会大量产生。尿素因此直接从这些原料中就产生了。

尿素生产是一个平衡的化學反应，其反应物不完全成为反应结果。生产过程、設定的反应条件、如何处理未转化的反应物，皆可能不同。由於使用大量的反应物，未反应的反应物可用以生产其它产品，（如[硝酸铵或](../Page/硝酸铵.md "wikilink")[硫酸铵](../Page/硫酸铵.md "wikilink")），也可回收再投入反应。

实际的合成反应普遍认为应是在液相中分如下两大步完成的。

第一步是過量的[液氨與](../Page/液氨.md "wikilink")[乾冰反應為](../Page/乾冰.md "wikilink")[氨基甲酸铵](../Page/氨基甲酸铵.md "wikilink")。由於是可逆的放热反应，反應需要帶走熱能的設備。

  -
    2NH<sub>3</sub> + CO<sub>2</sub> ↔ H<sub>2</sub>N-COONH<sub>4</sub>
    + 28千卡

第二步是加熱氨基甲酸铵為尿素；這步是一个可逆的吸热反应。反應需要帶走水分的設備。

  -
    H<sub>2</sub>N-COONH<sub>4</sub> ↔ (NH<sub>2</sub>)<sub>2</sub>CO +
    H<sub>2</sub>O - 3.6千卡

尿素的反应总式为：

  -
    2NH<sub>3</sub>+CO<sub>2</sub> →
    CO（NH<sub>2</sub>）<sub>2</sub>+H<sub>2</sub>O

表達在该公式的生产反应總體上为一个可逆的放热反应。

尿素在生产中通常伴随着副产品，如[甲醇](../Page/甲醇.md "wikilink")，也可用来制造下游产品，如[三聚氰胺等](../Page/三聚氰胺.md "wikilink")。

## 应用

[1966-01_1965年吴泾化工厂尿素厂2.jpg](https://zh.wikipedia.org/wiki/File:1966-01_1965年吴泾化工厂尿素厂2.jpg "fig:1966-01_1965年吴泾化工厂尿素厂2.jpg")
尿素在商业上，可作為：

  - 特殊[塑料的原料](../Page/塑料.md "wikilink")，尤其是[尿素甲醛樹脂](../Page/尿素甲醛樹脂.md "wikilink")
  - 某些胶類的原料
  - [肥料和](../Page/肥料.md "wikilink")[饲料的成分](../Page/饲料.md "wikilink")
  - 取代防冻的盐撒在街道，优点是不使金属腐蚀
  - 加强[香烟的氣味](../Page/香烟.md "wikilink")
  - 賦予工业生产的[椒鹽卷餅棕色](../Page/椒鹽卷餅.md "wikilink")
  - 某些洗发剂、清洁剂的成分
  - 急救用制冷包的成分，因為尿素与水的反應會吸热
  - 处理柴油机、发动机、热力发电厂的废气，尤其可降低其中氮氧化物的含量
  - 催雨剂的成分〈配合盐〉
  - 过去用来分离石蜡，因為尿素能形成包合物
  - 耐火材料
  - 環保引擎燃料的成分
  - [美白牙齿产品的成分](../Page/美白牙齿.md "wikilink")
  - 為化學肥料

### 饲料添加剂

人类粮食资源與[蛋白质的短缺](../Page/蛋白质.md "wikilink")，也造成饲料工业一大难题。業者積極尋找[蛋白质的新來源](../Page/蛋白质.md "wikilink")，並擴及[蛋白質以外的氮來源](../Page/蛋白質.md "wikilink")，例如含氮量高的尿素。

1897年，Waesk 等人提出**反刍动物**能转化非蛋白質氮为菌体蛋白質的想法。1949年，C. J. Watson
等人餵食[绵羊含有N](../Page/绵羊.md "wikilink")15标记的尿素胶囊，4天后在[绵羊](../Page/绵羊.md "wikilink")[血液](../Page/血液.md "wikilink")、[肝脏](../Page/肝脏.md "wikilink")、[肾脏中檢驗出含有N](../Page/肾脏.md "wikilink")15的蛋白质。這证实了反刍动物可以利用非蛋白質氮。同年
J. K. Looli
等人以尿素當作唯一氮源餵食[绵羊](../Page/绵羊.md "wikilink")，发现[绵羊能夠正氮平衡](../Page/绵羊.md "wikilink")，表明绵羊瘤胃裡的微生物能利用尿素合成其生长所需的10种**必需氨基酸**。自此，尿素及尿素化合物成為反刍动物的饲料添加剂了。

### 实验室应用

尿素能非常有效的使蛋白质[变性](../Page/变性_\(生物化学\).md "wikilink")，尤其能非常有效地破坏非共价键结合的蛋白质。这特点可以提高某些蛋白质的可溶性，其浓度可达10[摩尔体积](../Page/摩尔体积.md "wikilink")。尿素也可用来制造硝酸尿素。

### 医学应用

[皮肤科以含有尿素的某些药剂来提高](../Page/皮肤科.md "wikilink")[皮肤的湿度](../Page/皮肤.md "wikilink")。非手术摘除的[指甲使用的](../Page/指甲.md "wikilink")[封闭敷料中](../Page/封闭敷料.md "wikilink")，含有40%的尿素。

测试[幽門螺桿菌存在的](../Page/幽門螺桿菌.md "wikilink")[碳-14-呼气试验](../Page/碳-14-呼气试验.md "wikilink")，使用了含有[碳14或](../Page/碳14.md "wikilink")[碳13标记的尿素](../Page/碳13.md "wikilink")。因為幽門螺桿菌的尿素酶使用尿素来製造氨，以提高其周边胃里的pH值。同样原理也可测试生活在动物胃中的类似细菌。

### 纺织工业

尿素是纺织工业在染色和印刷時的重要辅助剂，能提高颜料可溶性，並使纺织品染色后保持一定的湿度。

## 脲

脲是[化合物](../Page/化合物.md "wikilink")，含有[官能团](../Page/官能团.md "wikilink")。这官能团的[羰基帶著两个有机的](../Page/羰基.md "wikilink")[氨基](../Page/氨基.md "wikilink")。实验室中，[光气可与此二氨基反应](../Page/光气.md "wikilink")。脲類化合物包括[过氧化脲](../Page/过氧化脲.md "wikilink")、[尿囊素](../Page/尿囊素.md "wikilink")、[乙内酰脲](../Page/乙内酰脲.md "wikilink")。脲与[缩二脲非常接近](../Page/缩二脲.md "wikilink")。脲的化學结构与[酰胺](../Page/酰胺.md "wikilink")、[氨基甲酸](../Page/氨基甲酸.md "wikilink")、[双偶氮化合物](../Page/双偶氮化合物.md "wikilink")、[碳二亚胺等接近](../Page/碳二亚胺.md "wikilink")。

## 其他

  - [硫脲](../Page/硫脲.md "wikilink")
  - [硫酰胺](../Page/硫酰胺.md "wikilink")

## 外部链接

  - [中国尿素网](https://web.archive.org/web/20070428022624/http://www.fert.cn/fenlei/F010401/)
  - [中国种植业大观·肥料卷·尿素](http://www.ndcnc.gov.cn/datalib/2003/AgroKnowledge/DL/DL-456082)

[氮素代谢](../Category/氮素代谢.md "wikilink") [脲](../Category/脲.md "wikilink")
[N](../Category/官能团.md "wikilink")
[Category:一碳有机物](../Category/一碳有机物.md "wikilink")
[Category:排泄](../Category/排泄.md "wikilink")
[Category:土壤改良物](../Category/土壤改良物.md "wikilink")
[Category:化學肥料](../Category/化學肥料.md "wikilink")
[Category:尿素循环](../Category/尿素循环.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")
[Category:天然保濕因子](../Category/天然保濕因子.md "wikilink")