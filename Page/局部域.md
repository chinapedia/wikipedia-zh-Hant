在**數學上**，**局部域**是一類特別的[域](../Page/域.md "wikilink")，它有非平凡的[絕對值](../Page/絕對值.md "wikilink")，此絕對值賦予的拓撲是[局部緊的](../Page/局部緊.md "wikilink")。局部域可粗分為兩類：一種的絕對值滿足阿基米德性質（稱作**阿基米德局部域**），另一種的絕對值不滿足阿基米德性質（稱作**非阿基米德局部域**）。在[數論中](../Page/數論.md "wikilink")，[數域的](../Page/數域.md "wikilink")[完備化給出局部域的典型例子](../Page/完備化.md "wikilink")。

## 非阿基米德局部域

設\(F\)為非阿基米德局部域，而\(|\cdot|\)為其絕對值。關鍵在下述對象：

  - 閉單位球：\(\{a\in F: |a|\leq 1\}\)，或其**整數環**\(\mathcal{O}\)，這是個[緊集](../Page/緊集.md "wikilink")。
  - 整數環裡的**單位元素**：\(\mathcal{O}^\times = \{a\in F: |a|= 1\}\)
  - 開單位球：\(\{a\in F: |a|< 1\}\)，這同時是其整數環裡唯一的[極大理想](../Page/極大理想.md "wikilink")，也記作\(\mathfrak{m}\)。

上述對象與[賦值環的構造相呼應](../Page/賦值環.md "wikilink")；事實上，可證明必存在實數\(0 < c < 1\)及[離散賦值](../Page/賦值.md "wikilink")\(v: F^\times \rightarrow \mathbb{Z}\)，使得

\[\forall a \in F \; c^{v(a)}=|a|\].

可取唯一的\(c\)使得\(v\)為滿射，稱之為**正規化賦值**。

從此引出非阿基米德局部域的另一個等價定義：一個域\(F\)，帶離散賦值\(v: F^\times \rightarrow \mathbb{Z}\)，使得\(F\)成為完備的拓撲域，而且剩餘域有限。

這類局部域的行為可由[局部類域論描述](../Page/局部類域論.md "wikilink")。

## 分類

局部域的完整分類如次：

  - \(\mathbb R, \mathbb C\)。這些是阿基米德局部域。
  - [p進數域](../Page/p進數.md "wikilink")\(\mathbb{Q}_p\)的有限擴張。這些是特徵為零的非阿基米德局部域。
  - \(\mathbb{F}_q((T))\)的有限擴張（其中\(\mathbb{F}_q\)表有q個元素的[有限域](../Page/有限域.md "wikilink")）。這些是特徵非零的非阿基米德局部域。

## 文獻

  - Milne, James, [**Algebraic Number
    Theory**](http://www.jmilne.org/math/CourseNotes/math676.html).

  -
[J](../Category/域論.md "wikilink") [J](../Category/代數數論.md "wikilink")