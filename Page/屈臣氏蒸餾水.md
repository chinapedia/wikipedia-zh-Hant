[Watsons_Water_Centre.jpg](https://zh.wikipedia.org/wiki/File:Watsons_Water_Centre.jpg "fig:Watsons_Water_Centre.jpg")
**屈臣氏蒸餾水**是[香港](../Page/香港.md "wikilink")[屈臣氏集團和長江和記實業旗下的業務之一在香港生產及銷售](../Page/屈臣氏集團.md "wikilink")[蒸餾水](../Page/蒸餾水.md "wikilink")，成為和記黃埔有限公司全資附屬機構，總部設於新界東區之大埔工業邨，同樣亦是法國甘露生産商之亞洲總部。目前由[屈臣氏實業負責管理](../Page/屈臣氏實業.md "wikilink")。屈臣氏於辦公室推出商用裝蒸餾水首創[塑膠零售瓶裝蒸餾水](../Page/聚对苯二甲酸乙二酯.md "wikilink")。

屈臣氏蒸餾水在香港、中國大陸，以至東南亞地區均廣受歡迎，生產廠房遍佈香港、[北京](../Page/北京.md "wikilink")、[上海及](../Page/上海.md "wikilink")[廣州](../Page/廣州.md "wikilink")，同時為[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞及](../Page/馬來西亞.md "wikilink")[台灣等國家地區提供及銷售蒸餾水](../Page/台灣.md "wikilink")。2001
年屈臣氏推出「加礦」蒸餾水，並於 2002 年推出全新瓶裝設計。

## 名稱

屈臣氏蒸餾水於1990年代末之前，一直將「蒸餾水」錯寫成「蒸溜水」，後來在社會壓力下才改正過來。

## 贊助

[Andy_Wallace.jpg](https://zh.wikipedia.org/wiki/File:Andy_Wallace.jpg "fig:Andy_Wallace.jpg")
[No_29_Franz_Engstler_BMW_at_Lisboa.JPG](https://zh.wikipedia.org/wiki/File:No_29_Franz_Engstler_BMW_at_Lisboa.JPG "fig:No_29_Franz_Engstler_BMW_at_Lisboa.JPG")
自1985年，屈臣氏蒸餾水多次贊助[澳門大賽車](../Page/澳門大賽車.md "wikilink")，當中為屈臣氏蒸餾水奪得冠軍的包括1985年東望洋大賽白加利（Gianfranco
Brancatelli）、1986年三級方程式華萊士（Andy Wallace）、1987年三級方程式當連尼（Martin
Donnelly）和1989年東望洋大賽夏菲（Tim Harvey）。

於2000年代，由屈臣氏蒸餾水贊助的[鄧肯·許士文四度奪得東望洋大賽冠軍](../Page/鄧肯·許士文.md "wikilink")。

於2014年10月，由屈臣氏蒸餾水贊助的《[香港哈爾濱冰雪節](../Page/香港哈爾濱冰雪節.md "wikilink")2014》為香港首届的冰雪節做指定蒸餾水贊助商。

## 參見

  - [屈臣氏集團](../Page/屈臣氏集團.md "wikilink")
      - [屈臣氏實業](../Page/屈臣氏實業.md "wikilink")
          - [碧泉](../Page/碧泉.md "wikilink")
          - [沙示](../Page/沙示.md "wikilink")
  - [2008年屈臣氏蒸餾水工潮](../Page/2008年屈臣氏蒸餾水工潮.md "wikilink")

## 參考資料

[屈臣氏蒸餾水 公司簡介](http://www.watsons-water.com/zh/watsons_water.html)

## 外部链接

  - [屈臣氏蒸餾水](http://www.watsons-water.com/)

[Category:香港食品製造商](../Category/香港食品製造商.md "wikilink")
[Category:瓶裝水品牌](../Category/瓶裝水品牌.md "wikilink")
[Category:屈臣氏集團](../Category/屈臣氏集團.md "wikilink")
[Category:1903年成立的公司](../Category/1903年成立的公司.md "wikilink")