《**屋塔房小貓**》（，又译《**閣樓男女**》），是[韓國一套從](../Page/大韓民國.md "wikilink")[网络小说改編成](../Page/网络小说.md "wikilink")[电视剧的作品](../Page/韩国电视剧.md "wikilink")，由[MBC製播](../Page/MBC.md "wikilink")。故事講述了一對在同一間屋內“同居”的男女的故事。[中国中央电视台曾在](../Page/中国中央电视台.md "wikilink")2005年寒假播出中文版，譯名為《阁楼-{}-男女》。

本作品的兩位主角[鄭多彬及](../Page/鄭多彬_\(1980年\).md "wikilink")[金來沅都因為這套電視劇而得到](../Page/金來沅.md "wikilink")2003年度[MBC演技大賞](../Page/MBC演技大賞.md "wikilink")：鄭多彬得到“新人賞”，而金來沅得到了“最優秀賞”及“人氣賞”。

## 演員陣容

  - [金来沅](../Page/金来沅.md "wikilink") 饰 **李庆民**
  - [鄭多彬](../Page/鄭多彬_\(1980年\).md "wikilink") 饰 **蓝静恩**
  - [崔贞允](../Page/崔贞允.md "wikilink") 饰 **罗惠莲**
  - [李贤宇](../Page/李贤宇.md "wikilink") 饰 **刘东俊**

## 其他搭配歌曲

  - 台灣[緯來戲劇台版本](../Page/緯來戲劇台.md "wikilink")
      - 片頭曲：[何以奇](../Page/何以奇.md "wikilink")《別疼我》
      - 片尾曲：[何以奇](../Page/何以奇.md "wikilink")《圓》

## 外部連結

  - [韓國MBC官方網站](http://www.imbc.com/broad/tv/drama/cat/)

[Category:網路小說](../Category/網路小說.md "wikilink")
[Category:MBC月火連續劇](../Category/MBC月火連續劇.md "wikilink")
[Category:2003年韓國電視劇集](../Category/2003年韓國電視劇集.md "wikilink")
[Category:緯來電視外購韓劇](../Category/緯來電視外購韓劇.md "wikilink")
[Category:韓國小說改編韓國電視劇](../Category/韓國小說改編韓國電視劇.md "wikilink")
[Category:韩国网络小说改编电视剧](../Category/韩国网络小说改编电视剧.md "wikilink")
[Category:韓國偶像劇](../Category/韓國偶像劇.md "wikilink")
[Category:公寓背景電視劇](../Category/公寓背景電視劇.md "wikilink")