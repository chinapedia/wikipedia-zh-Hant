  - 海外地區旗幟為[自治區](../Page/自治區.md "wikilink")、[屬地](../Page/屬地.md "wikilink")、海外省與海外領地，附上所在地理位置。
  - 關於各國行政區劃分與命名請參見[各國行政區劃](../Category/各國行政區劃.md "wikilink")。
  - 其他旗幟請參見**[旗幟列表](../Page/旗幟列表.md "wikilink")**。

## 澳大利亞 Australian

國家介紹:[澳大利亞](../Page/澳大利亞.md "wikilink")

|                                                                                                                 |
| :-------------------------------------------------------------------------------------------------------------- |
| **澳大利亞海外領地**                                                                                                    |
| [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") |
| [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") |

|-
|[Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg")||[豪勛爵島](../Page/豪勛爵島.md "wikilink")
屬於[新南威爾斯州](../Page/新南威爾斯州.md "wikilink")
Lord Howe Island
(南太平洋)|||[Flag_of_Lord_Howe_Island.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Lord_Howe_Island.svg "fig:Flag_of_Lord_Howe_Island.svg")
(非正式) |} |}

## 智利 Chile

國家介紹:[智利](../Page/智利.md "wikilink")

**智利海外領地**  |-
|[Flag_of_Rapa_Nui,_Chile.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Rapa_Nui,_Chile.svg "fig:Flag_of_Rapa_Nui,_Chile.svg")||[復活節島](../Page/復活節島.md "wikilink")
Easter Island |}

## 丹麥 Denmark

國家介紹：[丹麥](../Page/丹麥.md "wikilink")

**丹麥自治領地**

<table style="width:30%;">
<colgroup>
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Faroe_Islands.svg" title="fig:Flag_of_the_Faroe_Islands.svg">Flag_of_the_Faroe_Islands.svg</a></p></td>
</tr>
</tbody>
</table>

|}

## 法國 France

國家介紹：[法國](../Page/法國.md "wikilink")

|                                                                                                                                       |
| :------------------------------------------------------------------------------------------------------------------------------------ |
| **法國海外屬國**                                                                                                                            |
| [Flag_of_French_Polynesia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_French_Polynesia.svg "fig:Flag_of_French_Polynesia.svg") |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")                                |

| valign=top align=left width=35% bgcolor=\#E6E6FA | **法屬玻里尼西亞各島嶼**  |-
|[Bandera_de_les_illes_Australs.png](https://zh.wikipedia.org/wiki/File:Bandera_de_les_illes_Australs.png "fig:Bandera_de_les_illes_Australs.png")||[南方群島](../Page/南方群島.md "wikilink")
Austral Islands
|[Flag_of_the_Gambier_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Gambier_Islands.svg "fig:Flag_of_the_Gambier_Islands.svg")||[甘比爾群島](../Page/甘比爾群島.md "wikilink")
Gambier Islands |-
|[Flag_of_Marquesas_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Marquesas_Islands.svg "fig:Flag_of_Marquesas_Islands.svg")||[馬克薩斯群島](../Page/馬克薩斯群島.md "wikilink")
Marquesas Islands
|[Missing_flag.png](https://zh.wikipedia.org/wiki/File:Missing_flag.png "fig:Missing_flag.png")||[社會群島](../Page/社會群島.md "wikilink")
Society Islands |-
|[Flag_of_Tuamotu_Archipelago.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Tuamotu_Archipelago.svg "fig:Flag_of_Tuamotu_Archipelago.svg")||[土木土群島](../Page/土阿莫土群岛.md "wikilink")
Tuamotu Archipelago |} **新喀里多尼亞各省**  |-
|[Bandera_Province_Nord.png](https://zh.wikipedia.org/wiki/File:Bandera_Province_Nord.png "fig:Bandera_Province_Nord.png")||[北方省](../Page/北方省.md "wikilink")
North Province
|[Bandera_Loyauté.png](https://zh.wikipedia.org/wiki/File:Bandera_Loyauté.png "fig:Bandera_Loyauté.png")||[洛亞蒂省](../Page/洛亞蒂省.md "wikilink")
Loyalty Islands |-
|[Bandera_Province_Sud.png](https://zh.wikipedia.org/wiki/File:Bandera_Province_Sud.png "fig:Bandera_Province_Sud.png")||[南方省](../Page/南方省.md "wikilink")
South Province |} |}

|                                                                                                        |
| :----------------------------------------------------------------------------------------------------- |
| **法國海外省**                                                                                              |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") |

| valign=top align=left width=35% bgcolor=\#E6E6FA | **法國海外領地**  |-
|[Flag_of_the_French_Southern_and_Antarctic_Lands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_French_Southern_and_Antarctic_Lands.svg "fig:Flag_of_the_French_Southern_and_Antarctic_Lands.svg")||[法屬南方和南極洲領地](../Page/法屬南方和南極洲領地.md "wikilink")
FSAT
(印度洋)
|[Flag_of_Wallis_and_Futuna.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Wallis_and_Futuna.svg "fig:Flag_of_Wallis_and_Futuna.svg")||[瓦利斯及富圖納群島](../Page/瓦利斯及富圖納群島.md "wikilink")
Wallis & Futuna Islands
(南太平洋) |-
|[Flag_of_Saint-Pierre_and_Miquelon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Saint-Pierre_and_Miquelon.svg "fig:Flag_of_Saint-Pierre_and_Miquelon.svg")||[聖皮耶與密克隆群島](../Page/聖皮耶與密克隆群島.md "wikilink")
Saint-Pierre and Miquelon
(北大西洋) |} **法國海外集合領地**  |-
|[Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")||[馬約特島](../Page/馬約特.md "wikilink")
FSAT
(印度洋)||[Flag_of_Mayotte_(local).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Mayotte_\(local\).svg "fig:Flag_of_Mayotte_(local).svg")
(非正式) |} |}

## 荷蘭 Netherlands

國家介紹：[荷蘭](../Page/荷蘭.md "wikilink")

**荷蘭自治區**
注：[阿魯巴](../Page/阿魯巴.md "wikilink")、[波內赫與](../Page/波內赫.md "wikilink")[库拉索合稱ABC島嶼](../Page/库拉索.md "wikilink")。

<table style="width:2%;">
<colgroup>
<col style="width: 1%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Aruba.svg" title="fig:Flag_of_Aruba.svg">Flag_of_Aruba.svg</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands_Antilles.svg" title="fig:Flag_of_the_Netherlands_Antilles.svg">Flag_of_the_Netherlands_Antilles.svg</a></p></td>
</tr>
</tbody>
</table>

| valign=top align=left width=30% bgcolor=\#E6E6FA | **-{
zh-hans:荷屬安的列斯;zh-hant:荷屬安地列斯}-各島嶼**  |-
|[Flag_of_Bonaire.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bonaire.svg "fig:Flag_of_Bonaire.svg")||[波內赫](../Page/波內赫.md "wikilink")
Bonaire
|[Flag_of_Curaçao.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Curaçao.svg "fig:Flag_of_Curaçao.svg")||[库拉索](../Page/库拉索.md "wikilink")
Curaçao |-
|[Flag_of_Saba.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Saba.svg "fig:Flag_of_Saba.svg")||[薩巴島](../Page/薩巴島.md "wikilink")
Saba
|[Flag_of_Sint_Eustatius.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sint_Eustatius.svg "fig:Flag_of_Sint_Eustatius.svg")||[聖尤斯特歇斯](../Page/聖尤斯特歇斯.md "wikilink")
Sint Eustatius
|[Flag_of_Sint_Maarten.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sint_Maarten.svg "fig:Flag_of_Sint_Maarten.svg")||[聖馬丁島](../Page/聖馬丁島.md "wikilink")
Sint Maarten |} |}

## 紐西蘭 New Zealand

國家介紹:[紐西蘭](../Page/紐西蘭.md "wikilink")

|                                                                                                                        |
| :--------------------------------------------------------------------------------------------------------------------- |
| **紐西蘭海外領地**                                                                                                            |
| [Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg") |

| valign=top align=left width=45% bgcolor=\#E6E6FA | **紐西蘭自由結合區
(Territory in Free Association)**  |-
|[Flag_of_the_Cook_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Cook_Islands.svg "fig:Flag_of_the_Cook_Islands.svg")||[庫克群島](../Page/庫克群島.md "wikilink")
Cook Islands
(南太平洋) |-
|[Flag_of_Niue.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Niue.svg "fig:Flag_of_Niue.svg")||[紐埃島](../Page/紐埃.md "wikilink")
Niue
(南太平洋) |-
|[Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg")||[托克勞群島](../Page/托克劳.md "wikilink")
Tokelau
(南太平洋)|||[Flag_of_Tokelau_%28local%29.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Tokelau_%28local%29.svg "fig:Flag_of_Tokelau_%28local%29.svg")
(非正式) |} |}

## 挪威 Norway

國家介紹：[挪威](../Page/挪威.md "wikilink")

**挪威海外領地**

<table style="width:3%;">
<colgroup>
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg" title="fig:Flag_of_Norway.svg">Flag_of_Norway.svg</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg" title="fig:Flag_of_Norway.svg">Flag_of_Norway.svg</a></p></td>
</tr>
</tbody>
</table>

| valign=top align=left width=40% bgcolor=\#E6E6FA | **挪威南極領地**  |-
|[Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")||[布威島](../Page/布威島.md "wikilink")
Bouvet Island
(南冰洋)
|[Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")||[毛德皇后地](../Page/毛德皇后地.md "wikilink")(主權凍結)
Queen Maud Land
(南極洲) |-
|[Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")||[彼得一世島](../Page/彼得一世島.md "wikilink")
Peter I Island
(南冰洋) |} |}

## 葡萄牙 Portugal

國家介紹：[葡萄牙](../Page/葡萄牙.md "wikilink")

**葡萄牙自治區**

<table style="width:6%;">
<colgroup>
<col style="width: 6%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Azores.svg" title="fig:Flag_of_the_Azores.svg">Flag_of_the_Azores.svg</a></p></td>
</tr>
</tbody>
</table>

|}

## 英國 United Kingdom

國家介紹：[英國](../Page/英國.md "wikilink")

|                                                                                                                                                                                                |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **英國海外領地**                                                                                                                                                                                     |
| [Flag_of_Anguilla.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Anguilla.svg "fig:Flag_of_Anguilla.svg")                                                                                   |
| [Flag_of_the_British_Indian_Ocean_Territory.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_British_Indian_Ocean_Territory.svg "fig:Flag_of_the_British_Indian_Ocean_Territory.svg") |
| [Flag_of_the_Falkland_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Falkland_Islands.svg "fig:Flag_of_the_Falkland_Islands.svg")                                             |
| [Flag_of_Saint_Helena.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Saint_Helena.svg "fig:Flag_of_Saint_Helena.svg")                                                                      |
| [Flag_of_the_Turks_and_Caicos_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Turks_and_Caicos_Islands.svg "fig:Flag_of_the_Turks_and_Caicos_Islands.svg")                   |

|}

|                                                                                                                 |
| :-------------------------------------------------------------------------------------------------------------- |
| **英國直屬自治領地**                                                                                                    |
| [Flag_of_Gibraltar.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Gibraltar.svg "fig:Flag_of_Gibraltar.svg") |

[海峽群島](../Page/海峽群島.md "wikilink") Channel Islands (英吉利海峽，包括澤西行政區與根息行政區)
 |-
|[Flag_of_Jersey.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Jersey.svg "fig:Flag_of_Jersey.svg")||[澤西島](../Page/澤西島.md "wikilink")
Jersey
|[Flag_of_Guernsey.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Guernsey.svg "fig:Flag_of_Guernsey.svg")||[根息島](../Page/根西岛.md "wikilink")
Guernsey
|[Flag_of_Alderney.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Alderney.svg "fig:Flag_of_Alderney.svg")||[奧爾德尼島](../Page/奧爾德尼島.md "wikilink")
Alderney |-
|[Flag_of_Sark.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sark.svg "fig:Flag_of_Sark.svg")||[薩克島](../Page/薩克島.md "wikilink")
Sark
|[Flag_of_Herm.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Herm.svg "fig:Flag_of_Herm.svg")||[赫姆島](../Page/赫姆島.md "wikilink")
Herm | |} |}

|                                                                                                           |
| :-------------------------------------------------------------------------------------------------------- |
| **英國直屬殖民地**                                                                                               |
| [Flag_of_Bermuda.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bermuda.svg "fig:Flag_of_Bermuda.svg") |

| valign=top align=left width=50% bgcolor=\#E6E6FA | **英國特殊領地**  |-
|[Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")||[賽普勒斯英屬基地區](../Page/賽普勒斯英屬基地區.md "wikilink")
U.K. Sovereign Base Areas in Cyprus
(賽普勒斯) |} |}

## 美國 United State of America

國家介紹:[美國](../Page/美國.md "wikilink")

|                                                                                                                                                            |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **美國海外無建制領地**                                                                                                                                              |
| [Flag_of_American_Samoa.svg](https://zh.wikipedia.org/wiki/File:Flag_of_American_Samoa.svg "fig:Flag_of_American_Samoa.svg")                            |
| [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")                  |
| [Flag_of_Navassa_Island_(local).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Navassa_Island_\(local\).svg "fig:Flag_of_Navassa_Island_(local).svg") |

|}

|                                                                                                                                                         |
| :------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **美國海外建制領地**                                                                                                                                            |
| [Flag_of_Palmyra_Atoll_(local).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Palmyra_Atoll_\(local\).svg "fig:Flag_of_Palmyra_Atoll_(local).svg") |

| valign=top align=left width=30% bgcolor=\#E6E6FA | **美國自由聯邦領地**  |-
|[Flag_of_the_Northern_Mariana_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Northern_Mariana_Islands.svg "fig:Flag_of_the_Northern_Mariana_Islands.svg")||[北馬利安納群島](../Page/北马里亚纳群岛.md "wikilink")
Northern Mariana Islands
(西太平洋)
|[Flag_of_Puerto_Rico.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Puerto_Rico.svg "fig:Flag_of_Puerto_Rico.svg")||[波多黎各](../Page/波多黎各.md "wikilink")
Puerto Rico
(加勒比海) |} |}

## 相關條目

  - [旗幟列表](../Page/旗幟列表.md "wikilink")

[Category:旗幟](../Category/旗幟.md "wikilink")
[Category:旗幟列表](../Category/旗幟列表.md "wikilink")