[Tuen_Mun_Law_Court_201309.jpg](https://zh.wikipedia.org/wiki/File:Tuen_Mun_Law_Court_201309.jpg "fig:Tuen_Mun_Law_Court_201309.jpg")

**屯門裁判法院**（）是[香港](../Page/香港.md "wikilink")[屯門區及](../Page/屯門區.md "wikilink")[元朗區的最初級刑事法院](../Page/元朗區.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[屯喜路](../Page/屯喜路.md "wikilink")1號屯門法院大樓內，於1987年9月由時任英國大法官[夏永善男爵](../Page/夏永善.md "wikilink")（Michael
Havers）和首席按察司[羅弼時爵士主持揭幕](../Page/羅弼時.md "wikilink")。\[1\]

## 圖集

<File:HK> Tuen Mun Law Courts Tuen Hing Road.JPG|屯門裁判法院 <File:Tuen> Mun
Law Courts Building (Hong Kong).jpg|屯門法院大樓的北面外貌，外牆正進行維修工程（攝於2012年5月）

## 軼事

2011年有報章報導屯門法院大樓外牆維修工程破壞了約15至20個[小白腰雨燕鳥巢](../Page/小白腰雨燕.md "wikilink")，[香港觀鳥會向](../Page/香港觀鳥會.md "wikilink")[申訴專員公署投訴](../Page/申訴專員公署.md "wikilink")，指負責工程的[建築署及司法機構涉嫌行政失當](../Page/建築署.md "wikilink")\[2\]\[3\]。　

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [輕鐵](../Page/香港輕鐵.md "wikilink")：[市中心站](../Page/市中心站_\(香港\).md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 資料來源

## 外部連結

  - [香港裁判法院](http://www.judiciary.gov.hk/tc/crt_services/pphlt/html/mag.htm)

## 參見

  - [香港裁判法院](../Page/香港裁判法院.md "wikilink")

{{-}}

[Category:香港裁判法院](../Category/香港裁判法院.md "wikilink")
[Category:屯門市中心](../Category/屯門市中心.md "wikilink")

1.
2.
3.