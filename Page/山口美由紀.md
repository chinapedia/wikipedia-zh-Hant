**山口
美由紀**\[1\]，日本女性[漫畫家](../Page/漫畫家.md "wikilink")。[福岡縣](../Page/福岡縣.md "wikilink")[北九州市](../Page/北九州市.md "wikilink")[八幡西區出身](../Page/八幡西區.md "wikilink")。

## 作品

  - [勁爆四人組](../Page/勁爆四人組.md "wikilink")、原名《》（1985-1988年）
  - [青春三口組](../Page/青春三口組.md "wikilink")、全4冊、原名《》（1988-1989年）
  - [公主症候群](../Page/公主症候群.md "wikilink")、全1冊、原名《》（短編集1989年）
  - [女孩的季節](../Page/女孩的季節.md "wikilink")、全1冊、原名《》（短編集1989年）
  - [愛情音樂盒](../Page/愛情音樂盒.md "wikilink")、全1冊、原名《》（1990年）
  - [魔法花園](../Page/魔法花園.md "wikilink")、全5冊、原名《》（1990-1992年）
  - [魔法妖精](../Page/魔法妖精.md "wikilink")、全6冊、原名《》（1992-1994年）
  - [晨光之約](../Page/晨光之約.md "wikilink")、全9冊、原名《》（1995-1999年）
  - [太陽公公的世界地圖](../Page/太陽公公的世界地圖.md "wikilink")、全1冊、原名《》（短編集1996年）
  - 全1冊、原名《》（短編集1997年）
  - [魔幻龍騎士](../Page/魔幻龍騎士.md "wikilink")、全1冊、原名《》（2000年）
  - [夢幻櫻](../Page/夢幻櫻.md "wikilink")、全1冊、原名《》（2000年）
  - [舞池飯店](../Page/舞池飯店.md "wikilink")、全1冊、原名《》
  - [春告小町](../Page/春告小町.md "wikilink")、全4冊、原名《》
  - [半熟魔法使](../Page/半熟魔法使.md "wikilink")、全1冊、原名《》（2002年）
  - [火箭魔法使](../Page/火箭魔法使.md "wikilink")、全1冊、原名《》（2003年）
  - [天空聖龍](../Page/天空聖龍.md "wikilink")、全9冊、原名《》
  - [海盜們的樂園](../Page/海盜們的樂園.md "wikilink")、2冊待續、原名《》

## 参考文献

<div class="references-small">

<references />

</div>

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[Category:北九州市出身人物](../Category/北九州市出身人物.md "wikilink")

1.  [](http://www.hakusensha.co.jp/artist/profile.php?artistname=%8ER%8C%FB%94%FC%97R%8BI&artistname2=%82%E2%82%DC%82%AE%82%BF%82%DD%82%E4%82%AB&keyword=%82%E2&home=%95%9F%89%AA%8C%A7&birthday=6%8C%8E29%93%FA&bloodtype=A%8C%5E&debut=1983%94N%81u%89%D4%82%C6%82%E4%82%DF%81v9%8C%8E%91%E5%91%9D%8A%A7%8D%86%81w%82%B7%82%AB%82%E1%82%F1%82%BE%82%E9%81%9A%83%89%83%89%83o%83C%81x)