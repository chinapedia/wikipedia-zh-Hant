**山地-多瑙县**（Alb-Donau-Kreis）是[德国](../Page/德国.md "wikilink")[巴登-符腾堡州的一个县](../Page/巴登-符腾堡州.md "wikilink")，隶属于[蒂宾根行政区](../Page/蒂宾根行政区.md "wikilink")，首府[乌尔姆](../Page/乌尔姆.md "wikilink")。

## 地理

山地-多瑙县北面与[格平根县和](../Page/格平根县.md "wikilink")[海登海姆县](../Page/海登海姆县.md "wikilink")，东面与[巴伐利亚州的](../Page/巴伐利亚州.md "wikilink")[金茨堡县](../Page/金茨堡县.md "wikilink")、[新乌尔姆县和巴登](../Page/新乌尔姆县.md "wikilink")-符腾堡州的直辖市[乌尔姆](../Page/乌尔姆.md "wikilink")，南面与[比伯拉赫县](../Page/比伯拉赫县.md "wikilink")，西面与[罗伊特林根县相邻](../Page/罗伊特林根县.md "wikilink")。

## 县徽

山地-多瑙县县徽的底色是银色，一只黑色的双头老鹰，老鹰的胸前有一块盾牌，盾牌的左侧是金色底色上的三只黑色鹿角，盾牌的右侧是红白相间的条纹。老鹰代表[乌尔姆的](../Page/乌尔姆.md "wikilink")[帝国自由城市身份](../Page/帝国自由城市.md "wikilink")（山地-多瑙县县徽沿用自以前的乌尔姆县县徽，1973年乌尔姆县、Ehingen县与[比伯拉赫县和Münsingen县的一部分城镇合并为山地](../Page/比伯拉赫县.md "wikilink")-多瑙县），鹿角代表县内原本属于[符腾堡的城镇和](../Page/符腾堡.md "wikilink")1803年后归属于符腾堡的城镇，红白相间的条纹则代表Burgau的藩侯家族和Berg的伯爵家族。山地-多瑙县的县徽启用于1975年，同样的图案此前曾是乌尔姆县的县徽。

## 参考文献

<div class="references-small">

  - *Das Land Baden-Württemberg* – Amtliche Beschreibung nach Kreisen
    und Gemeinden (in acht Bänden). Hrsg. von der Landesarchivdirektion
    Baden-Württemberg. Band VII: Regierungsbezirk Tübingen. Kohlhammer,
    Stuttgart 1978, ISBN 3-17-004807-4

</div>

## 外部連結

  - [Official website](http://www.alb-donau-kreis.de)

[A](../Category/巴登-符腾堡州的县.md "wikilink")