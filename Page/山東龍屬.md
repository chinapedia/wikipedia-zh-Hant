**山東龍屬**（屬名：*Shantungosaurus*）意為“山東[蜥蜴](../Page/蜥蜴.md "wikilink")”，是種頭部平坦，没有冠饰的[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，归类于[栉龙亚科](../Page/栉龙亚科.md "wikilink")\[1\]。發現於[中國](../Page/中國.md "wikilink")[山東半島的](../Page/山東.md "wikilink")[王氏群](../Page/王氏群.md "wikilink")，地質年代屬於晚[白堊紀](../Page/白堊紀.md "wikilink")。

## 敘述

[Largestornithopods_scale.png](https://zh.wikipedia.org/wiki/File:Largestornithopods_scale.png "fig:Largestornithopods_scale.png")、[鴨龍](../Page/鴨龍.md "wikilink")([连接埃德蒙顿龙](../Page/连接埃德蒙顿龙.md "wikilink"))、[帝王埃德蒙頓龍](../Page/帝王埃德蒙頓龍.md "wikilink")\]\]
山東龍是目前已知最大、最長的鴨嘴龍科恐龍之一。一個中等個體的複合骨骸保存於[北京](../Page/北京.md "wikilink")[中國地質學院](../Page/中國地質學院.md "wikilink")，該[正模標本身長](../Page/正模標本.md "wikilink")14.72公尺\[2\]，頭顱骨長1.63公尺\[3\]。山東龍重量估計約達16噸\[4\]。山東龍具有長尾巴，可能使身體的重心維持在臀部\[5\]。根據複合材料而重建的[諸城龍骨架模型](../Page/諸城龍.md "wikilink")，身長可達16.6公尺。目前諸城龍被視為根山東龍是同種動物，使得山東龍成為已知最大型的[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")\[6\]。

如同所有鴨嘴龍類，山東龍的喙狀嘴缺乏牙齒，但牠們頜部有1,500顆咀嚼用牙齒。山東龍鼻孔附近有個由寬鬆垂下物所覆蓋的洞，可能用來發出聲音。

## 發現與種

[Laika_ac_Dino_Kingdom_2012_(7882291466).jpg](https://zh.wikipedia.org/wiki/File:Laika_ac_Dino_Kingdom_2012_\(7882291466\).jpg "fig:Laika_ac_Dino_Kingdom_2012_(7882291466).jpg")
化石在1964年發現於[山東省](../Page/山東省.md "wikilink")[諸城市](../Page/諸城市.md "wikilink")，目前已發現超過5個不完整骨骸。中國古生物學家[徐星根據目前已發現化石](../Page/徐星.md "wikilink")，發現山東龍與[埃德蒙頓龍有許多共同特徵](../Page/埃德蒙頓龍.md "wikilink")，兩者可能形成一個[演化支](../Page/演化支.md "wikilink")。

在2007年被敘述、命名的[巨大諸城龍](../Page/諸城龍.md "wikilink")（*Zhuchengosaurus
maximus*），化石來自於數個個體，包含頭顱骨、四肢骨頭、脊椎，也是發現於山東省諸城市\[7\]。在2011年，研究發現山東龍、諸城龍其實是同種動物，代表不同生長階段\[8\]

## 照片

<File:Shantungosaurus> 2008 09 07.jpg|山東龍的骨架 -
[山东省博物馆](../Page/山东省博物馆.md "wikilink")
[File:Shantungosaurus.jpg|山東龍的骨架](File:Shantungosaurus.jpg%7C山東龍的骨架) -
山東省博物館

## 參考資料

  -
  - 157\.

## 外部連結

  - [See entry on *Shantungosaurus* at DinoData (registration required,
    free)](http://www.dinodata.org/index.php)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:櫛龍亞科](../Category/櫛龍亞科.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.  Ji, Y., Wang, X., Liu, Y., and Ji, Q. (2011). "Systematics, behavior
    and living environment of *Shantungosaurus giganteus* (Dinosauria:
    Hadrosauridae)." *Acta Geologica Sinica (English Edition)*,
    **85**(1): 58-65.