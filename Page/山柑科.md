**山柑科**也叫[白花菜科](../Page/白花菜科.md "wikilink")，共有25[属约](../Page/属.md "wikilink")650[种](../Page/种.md "wikilink")，广泛分布在全球暖[温带和](../Page/温带.md "wikilink")[热带地区](../Page/热带.md "wikilink")，[中国有](../Page/中国.md "wikilink")4属约20餘種，分布南方各地。

本科[植物为](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")、[灌木](../Page/灌木.md "wikilink")、[藤本](../Page/藤本.md "wikilink")，少数为[草本](../Page/草本.md "wikilink")，和[醉蝶花科相似](../Page/醉蝶花科.md "wikilink")。

1981年的[克朗奎斯特分类法将山柑科列在](../Page/克朗奎斯特分类法.md "wikilink")[白花菜目中](../Page/白花菜目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法和](../Page/APG_分类法.md "wikilink")2003年经过修订的[APG II
分类法将其和](../Page/APG_II_分类法.md "wikilink")[醉蝶花科都列在](../Page/醉蝶花科.md "wikilink")[十字花科中](../Page/十字花科.md "wikilink")，但2006年8月22日新修订的结果是和[醉蝶花科都单独分出成为独立的](../Page/醉蝶花科.md "wikilink")[科](../Page/科.md "wikilink")，放到[十字花目中](../Page/十字花目.md "wikilink")。

## 属

  - *[Apophyllum](../Page/Apophyllum.md "wikilink")*
  - *[Bachmannia](../Page/Bachmannia.md "wikilink")*
  - *[Belencita](../Page/Belencita.md "wikilink")*
  - [节蒴木属](../Page/节蒴木属.md "wikilink")
    *Borthwickia*(或分类在[节蒴木科](../Page/节蒴木科.md "wikilink"))
  - *[Boscia](../Page/Boscia.md "wikilink")*
  - *[Buchholzia](../Page/Buchholzia.md "wikilink")*
  - *[Cadaba](../Page/Cadaba.md "wikilink")*
  - [山柑属](../Page/山柑属.md "wikilink") *Capparis*
  - *[Cladostemon](../Page/Cladostemon.md "wikilink")*
  - [鱼木属](../Page/鱼木属.md "wikilink") *Crateva*
  - *[Dactylaena](../Page/Dactylaena.md "wikilink")*
  - *[Euadenia](../Page/Euadenia.md "wikilink")*
  - *[Forchhammeria](../Page/Forchhammeria.md "wikilink")*
  - *[Maerua](../Page/Maerua.md "wikilink")*
  - *[Morisonia](../Page/Morisonia.md "wikilink")*
  - *[Neothorelia](../Page/Neothorelia.md "wikilink")*
  - *[Podandrogyne](../Page/Podandrogyne.md "wikilink")*
  - *[Poilanedora](../Page/Poilanedora.md "wikilink")*
  - *[Ritchiea](../Page/Ritchiea.md "wikilink")*
  - *[Steriphoma](../Page/Steriphoma.md "wikilink")*
  - [斑果藤属](../Page/斑果藤属.md "wikilink") *Stixis*
  - *[Thilachium](../Page/Thilachium.md "wikilink")*
  - *[Tirania](../Page/Tirania.md "wikilink")*

### 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[山柑科](https://web.archive.org/web/20061101100756/http://delta-intkey.com/angio/www/capparid.htm)
  - [APG网站中的山柑科](http://www.mobot.org/MOBOT/Research/APWeb/orders/Brassicalesweb.htm#Capparoideae)
  - [NCBI中的山柑科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=301453)

[\*](../Category/山柑科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")