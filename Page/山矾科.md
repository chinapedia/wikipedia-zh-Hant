**山矾科**，又稱**灰木科**，是[開花植物](../Page/開花植物.md "wikilink")[杜鹃花目的一個科](../Page/杜鹃花目.md "wikilink")，有兩[属](../Page/属_\(生物\).md "wikilink")、约320[种](../Page/种_\(生物\).md "wikilink")，广泛分布在[亚洲](../Page/亚洲.md "wikilink")、[澳洲和](../Page/澳洲.md "wikilink")[美洲的](../Page/美洲.md "wikilink")[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[中国约有](../Page/中国.md "wikilink")130余种，主要生长在[长江以南的地区](../Page/长江.md "wikilink")。

本科[植物都是](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，有落叶的也有常绿的，单[叶](../Page/叶.md "wikilink")，互生；[花两性](../Page/花.md "wikilink")，辐射对称；果实为核果或浆果。

## 分類及物種

本科有兩屬：

  - [山矾属](../Page/山矾属.md "wikilink")
    *[Symplocos](../Page/Symplocos.md "wikilink")*
  - *[Cordyloblaste](../Page/Cordyloblaste.md "wikilink")*
    <small>(Brand) [Hatus.](../Page/初島住彥.md "wikilink")</small>

山矾屬的主要品种有[白檀](../Page/白檀.md "wikilink")（*S.
paniculata*）、[山矾](../Page/山矾.md "wikilink")（*S.
caudata*）、[老鼠矢](../Page/老鼠矢.md "wikilink")（*S.
stellaris*）等。种子可榨油做工业用，[木材细腻](../Page/木材.md "wikilink")，可用于细木工，[叶可提炼](../Page/叶.md "wikilink")[黄色染料](../Page/黄色.md "wikilink")，有的[树种适宜做行道树或绿化用树](../Page/树.md "wikilink")。

根據1981年的[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")，本科原是[木蘭綱](../Page/木蘭綱.md "wikilink")（）
[柿樹目](../Page/柿樹目.md "wikilink")（）\[1\]\[2\]之下五個科之一。1998年根据[基因](../Page/基因.md "wikilink")[亲缘关系分类的](../Page/亲缘关系分类.md "wikilink")[APG
分类法合并到](../Page/APG_分类法.md "wikilink")[杜鹃花目之下](../Page/杜鹃花目.md "wikilink")，2003年经过修订的[APG
II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。

本科過往一直都是[單屬科植物](../Category/單屬科植物.md "wikilink")，2009年被加入*[Cordyloblaste](../Page/Cordyloblaste.md "wikilink")*屬，不再屬科。

## 参考文献

## 外部連結

  -
[山矾科](../Category/山矾科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.
2.