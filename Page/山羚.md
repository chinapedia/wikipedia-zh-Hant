**山羚**（學名*Oreotragus
oreotragus*），[荷蘭語為](../Page/荷蘭語.md "wikilink")「山岩的跳躍者」，是一種住在[好望角至](../Page/好望角.md "wikilink")[東非及](../Page/東非.md "wikilink")[衣索匹亞的細少的](../Page/衣索匹亞.md "wikilink")[非洲](../Page/非洲.md "wikilink")[羚羊](../Page/羚羊.md "wikilink")。

[Oreotragus_oreotragus_(2).jpg](https://zh.wikipedia.org/wiki/File:Oreotragus_oreotragus_\(2\).jpg "fig:Oreotragus_oreotragus_(2).jpg")
山羚肩高只有58厘米，是相對於其他羚羊來說是較細少的[動物](../Page/動物.md "wikilink")。只有雄性有脆弱的角，一般長20-25厘米。牠們以蹄尖站立。

山羚的毛皮厚而密，有著橄欖形狀的小斑點。牠們的毛皮使牠們能在生活的石間混和，難讓人發現。

山羚是草食性動物，以石間[植物為糧食](../Page/植物.md "wikilink")。牠們不用喝水，這是由於多汁的植物提供了牠們生存所需的[水份](../Page/水.md "wikilink")。

山羚於九月至一月間交配。[妊娠期為](../Page/妊娠期.md "wikilink")214日。

[Category:山羚屬](../Category/山羚屬.md "wikilink")