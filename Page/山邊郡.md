**山邊郡**（）為[奈良縣轄下的](../Page/奈良縣.md "wikilink")[郡](../Page/郡.md "wikilink")。人口4,853人、面積66.56
km²。（2004年） 只有一村。

  - [山添村](../Page/山添村.md "wikilink")（やまぞえむら）

## 沿革

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1945年</p></th>
<th><p>1946年 - 1955年</p></th>
<th><p>1956年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>山邊村</p></td>
<td><p>1893年9月26日<br />
町制改稱<br />
丹波市町</p></td>
<td><p>1954年4月1日<br />
天理市</p></td>
<td><p>天理市</p></td>
<td><p>天理市</p></td>
<td><p><a href="../Page/天理市.md" title="wikilink">天理市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>二階堂村</p></td>
<td><p>二階堂村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>朝和村</p></td>
<td><p>朝和村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>福住村</p></td>
<td><p>福住村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>針別所村</p></td>
<td><p>針別所村</p></td>
<td><p>1955年1月1日<br />
都祁村</p></td>
<td><p>都祁村</p></td>
<td><p>2005年4月1日<br />
被併入奈良市</p></td>
<td><p><a href="../Page/奈良市.md" title="wikilink">奈良市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>都介野村</p></td>
<td><p>都介野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>豐原村</p></td>
<td><p>豐原村</p></td>
<td><p>豐原村</p></td>
<td><p>1956年9月30日<br />
山添村</p></td>
<td><p>山添村</p></td>
<td><p>山添村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>波多野村</p></td>
<td><p>波多野村</p></td>
<td><p>波多野村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/添上郡.md" title="wikilink">添上郡</a><br />
東山村</p></td>
<td><p>添上郡<br />
東山村</p></td>
<td><p>添上郡<br />
東山村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東里村</p></td>
<td><p>東里村</p></td>
<td><p>1955年2月11日<br />
<a href="../Page/宇陀郡.md" title="wikilink">宇陀郡</a><br />
<a href="../Page/室生村.md" title="wikilink">室生村的一部</a></p></td>
<td></td>
<td><p>2006年1月1日<br />
合併市制</p></td>
<td><p><a href="../Page/宇陀市.md" title="wikilink">宇陀市</a></p></td>
</tr>
</tbody>
</table>