**山陽小野田市**（）是位於[山口縣西南部的市](../Page/山口縣.md "wikilink")。於2005年3月22日，由[小野田市與](../Page/小野田市.md "wikilink")[厚狭郡](../Page/厚狭郡.md "wikilink")[山陽町](../Page/山陽町_\(山口縣\).md "wikilink")[合併而成](../Page/市町村合併.md "wikilink")，城市名稱直接取合併前兩行政區的名字相連而成。同時屬於[宇部都市圈和](../Page/宇部都市圈.md "wikilink")[關門都市圈的一部份](../Page/關門都市圈.md "wikilink")。

市內產業為以[水泥為中心的工業為主](../Page/水泥.md "wikilink")，屬於北九州工業地帶、瀨戶內工業地區的一部份。

## 地形

  - [山](../Page/山.md "wikilink")：[龍王山](../Page/龍王山_\(山陽小野田市\).md "wikilink")、[松嶽山](../Page/松嶽山.md "wikilink")
  - [河川](../Page/河川.md "wikilink")：[有帆川](../Page/有帆川.md "wikilink")、[厚狭川](../Page/厚狭川.md "wikilink")
  - [湖沼](../Page/湖沼.md "wikilink")：[江汐湖](../Page/江汐湖.md "wikilink")

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[厚狹郡高千帆村](../Page/厚狹郡.md "wikilink")、須惠村、厚西村、[出合村](../Page/出合村.md "wikilink")、生田村。
  - 1918年10月1日：厚西村改制並改名為[厚狹町](../Page/厚狹町.md "wikilink")。\[1\]
  - 1920年4月3日：須惠村改制並改名為[小野田町](../Page/小野田町_\(山口縣\).md "wikilink")。\[2\]
  - 1929年4月1日：出合村被併入厚狹町。
  - 1938年4月1日：高千帆村改制為[高千帆町](../Page/高千帆町.md "wikilink")。
  - 1940年11月3日：小野田町與高千帆町[合併為](../Page/市町村合併.md "wikilink")[小野田市](../Page/小野田市.md "wikilink")。
  - 1948年6月1日：生田村改制並改名為[埴生町](../Page/埴生町.md "wikilink")。
  - 1950年8月27日：舊高千帆町針對是否脫離小野田市獨立成町舉辦[公民投票](../Page/公民投票.md "wikilink")，結果為反對者占多數。
  - 1956年9月30日：[厚狹町與](../Page/厚狹町.md "wikilink")[埴生町合併為](../Page/埴生町.md "wikilink")[山陽町](../Page/山陽町_\(山口縣\).md "wikilink")。
  - 1957年：小野田市與山陽町曾針對是否合併舉辦公民投票，結過未獲得通過。
  - 2004年1月18日：小野田市與山陽町針對[宇部市](../Page/宇部市.md "wikilink")、小野田市、[楠町](../Page/楠町_\(山口縣\).md "wikilink")、山陽町合併舉辦公民投票，結果反對占多數。
  - 2005年3月22日：小野田市與山陽町合併為**山陽小野田市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>西須惠村</p></td>
<td><p>須惠村</p></td>
<td><p>1920年4月3日<br />
改制並改名為小野田町</p></td>
<td><p>1940年11月3日<br />
小野田市</p></td>
<td><p>2005年3月22日<br />
山陽小野田市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高畑村</p></td>
<td><p>高千帆村</p></td>
<td><p>1938年4月1日<br />
町制<br />
高千帆町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東高泊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西高泊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>千崎村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>有帆村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>厚狹村</p></td>
<td><p>厚西村</p></td>
<td><p>1918年10月1日<br />
改制並改名為厚狹町</p></td>
<td><p>厚狭町</p></td>
<td><p>1956年9月30日<br />
山陽町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>郡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鴨庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>山野井村</p></td>
<td><p>出合村</p></td>
<td><p>1929年4月1日<br />
併入厚狹町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>埴生村</p></td>
<td><p>生田村</p></td>
<td><p>1948年6月1日<br />
改制並改名為埴生町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>津布田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：[厚狹車站](../Page/厚狹車站.md "wikilink")
      - [山陽本線](../Page/山陽本線.md "wikilink")：[小野田車站](../Page/小野田車站.md "wikilink")
        - 厚狹車站 - [埴生車站](../Page/埴生車站.md "wikilink")
      - [小野田線](../Page/小野田線.md "wikilink")：[雀田車站](../Page/雀田車站.md "wikilink")
        - [小野田港車站](../Page/小野田港車站.md "wikilink") -
        [南小野田車站](../Page/南小野田車站.md "wikilink") -
        [南中川車站](../Page/南中川車站.md "wikilink") -
        [目出車站](../Page/目出車站.md "wikilink") - 小野田車站
          - 本山支線：雀田車站 - [濱河內車站](../Page/濱河內車站.md "wikilink") -
            [長門本山車站](../Page/長門本山車站.md "wikilink")
      - [美禰線](../Page/美禰線.md "wikilink")：厚狹車站 -
        [湯之峠車站](../Page/湯之峠車站.md "wikilink")

|                                                                                                         |                                                                                                                 |
| ------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| [Asa_stn.jpg](https://zh.wikipedia.org/wiki/File:Asa_stn.jpg "fig:Asa_stn.jpg")                        | [Habu_Station_2010.JPG](https://zh.wikipedia.org/wiki/File:Habu_Station_2010.JPG "fig:Habu_Station_2010.JPG") |
| [JR-Suzumeda-sta.jpg](https://zh.wikipedia.org/wiki/File:JR-Suzumeda-sta.jpg "fig:JR-Suzumeda-sta.jpg") | [Nagato-motoyama.jpg](https://zh.wikipedia.org/wiki/File:Nagato-motoyama.jpg "fig:Nagato-motoyama.jpg")         |

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [山陽自動車道](../Page/山陽自動車道.md "wikilink")：[小野田交流道](../Page/小野田交流道.md "wikilink")
    - [周防灘休息區](../Page/周防灘休息區.md "wikilink") -
    [埴生交流道](../Page/埴生交流道.md "wikilink")

### 港口

  - [小野田港](../Page/小野田港.md "wikilink")：為[中國電力](../Page/中國電力.md "wikilink")[新小野田發電所輸入](../Page/新小野田發電所.md "wikilink")[煤炭的主要管道](../Page/煤炭.md "wikilink")。

## 觀光景點

  - 濱五挺唐樋（[围垦遺跡](../Page/围垦.md "wikilink")）
  - 小野田水泥德利窯
  - 江汐湖溫泉
  - 湯之峠溫泉
  - 王喜溫泉、糸根溫泉

## 學校

[Yamaguchitokyorikauniv.jpg](https://zh.wikipedia.org/wiki/File:Yamaguchitokyorikauniv.jpg "fig:Yamaguchitokyorikauniv.jpg")

### 大學

  - [山口東京理科大學](../Page/山口東京理科大學.md "wikilink")
  - [放送大學山口學習中心](../Page/放送大學.md "wikilink")

### 高等學校

  - [山口縣立厚狹高等學校](../Page/山口縣立厚狹高等學校.md "wikilink")
  - [山口縣立小野田高等學校](../Page/山口縣立小野田高等學校.md "wikilink")
  - [山口縣立小野田工業高等學校](../Page/山口縣立小野田工業高等學校.md "wikilink")
  - [沙勿略高等學校](../Page/沙勿略高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [秩父市](../Page/秩父市.md "wikilink")（[埼玉縣](../Page/埼玉縣.md "wikilink")）
      -
        於1996年5月20日與小野田市締結為姊妹都市。\[3\]

### 海外

  - [摩頓灣](../Page/摩頓灣區政府.md "wikilink")（[澳大利亞](../Page/澳大利亞.md "wikilink")[昆士蘭州](../Page/昆士蘭州.md "wikilink")）

      -
        1992年8月18日小野田市與Redcliffe締結為姊妹都市，2008年3月15日Redcliffe合併為摩頓灣區政府。\[4\]

## 本地出身之名人

  - [青木周藏](../Page/青木周藏.md "wikilink")：明治時期的日本[外務大臣](../Page/外務大臣.md "wikilink")
  - [池野雅博](../Page/池野雅博.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [門田博光](../Page/門田博光.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [來島又兵衛](../Page/來島又兵衛.md "wikilink")：[江戶時代末期](../Page/江戶時代.md "wikilink")[長州藩武士](../Page/長州藩.md "wikilink")，曾參加[禁門之變](../Page/禁門之變.md "wikilink")。
  - [藏川洋平](../Page/藏川洋平.md "wikilink")：[職業足球選手](../Page/職業足球.md "wikilink")
  - [嶋村かおり](../Page/嶋村かおり.md "wikilink")：[女演員](../Page/女演員.md "wikilink")
  - [藤田ミノル](../Page/藤田ミノル.md "wikilink")：[職業摔角選手](../Page/職業摔角.md "wikilink")
  - [村中李衣](../Page/村中李衣.md "wikilink")：兒童文學作家、[繪本作家](../Page/繪本作家.md "wikilink")
  - [やきそばかおる](../Page/やきそばかおる.md "wikilink")：作家、動物攝影師
  - [長谷川謹介](../Page/長谷川謹介.md "wikilink")：明治時期的日本[鐵道官僚](../Page/鐵道.md "wikilink")、工程師，建設[台灣](../Page/台灣.md "wikilink")[縱貫鐵路的總工程師](../Page/縱貫鐵路.md "wikilink")

## 參考資料

## 外部連結

  - [山陽小野田觀光協會](http://sanyoonoda-kanko.com/)

<!-- end list -->

1.

2.

3.

4.