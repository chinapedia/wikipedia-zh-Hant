，一般稱為**FC岐阜**，是位於[岐阜縣](../Page/岐阜縣.md "wikilink")[岐阜市的足球會](../Page/岐阜市.md "wikilink")。在奪得[日本足球聯賽季軍後](../Page/日本足球聯賽.md "wikilink")，球會得到升級資格並會在2008年的[日本職業足球乙級聯賽中作賽](../Page/2008年日本職業足球聯賽.md "wikilink")。球隊的主色為綠色。

## 球會歷史

球隊於2001年創立並於2007年升級至[日本足球聯賽作賽](../Page/日本足球聯賽.md "wikilink")。在2007年賽季，FC岐阜以第三名的成績獲得升級資格。2007年12月3日，[日本職業足球聯賽終於承認其升級資格](../Page/日本職業足球聯賽.md "wikilink")。

## 隊徽

隊徽的設計是代表岐阜縣。上方的山脈是代表北方的山脈。花是岐阜縣花。
綠、藍、紅的三道條紋，代表縣內的[木曾三川](../Page/木曾三川.md "wikilink")。至於外貌似頭盔的圖像是代表德川家在[關原之戰取得勝利](../Page/關原之戰.md "wikilink")。

## 球員名單

## 外部連結

  - [F.C. Gifu Official Website](http://www.fc-gifu.com/)

|

[Category:日本足球俱樂部](../Category/日本足球俱樂部.md "wikilink")
[Category:岐阜市](../Category/岐阜市.md "wikilink")
[Category:2001年建立的足球會](../Category/2001年建立的足球會.md "wikilink")