[thumb](../Page/file:Da_Nang_view_from_top_of_Son_Tra.jpg.md "wikilink")
**港市**（），本名“**沱㶞**”，华文世界习作“**峴港**”或“**蜆港**”，位於[越南中部](../Page/越南.md "wikilink")，古都[順化的附近](../Page/順化.md "wikilink")，地理上屬[南中部](../Page/南中部.md "wikilink")。峴港是越南第四大城市，僅次于[胡志明市](../Page/胡志明市.md "wikilink")、[河内市和](../Page/河内市.md "wikilink")[海防市](../Page/海防市.md "wikilink")，是越南中部最大及重要的城市之一。峴港市瀕臨[南中國海](../Page/南中國海.md "wikilink")、下轄六郡兩縣；距離北方的河內市764公里、南方的胡志明市964公里。

## 譯名

峴港本名為**沱㶞**。在越南漢文古籍和法屬以後的國語字資料中，峴港的正式名稱一直是沱㶞。法國人則將沱㶞的法文名稱改作“土倫港”。二戰結束後，[香港媒體曾將峴港譯作](../Page/香港.md "wikilink")“大南”，而越南華人華僑將沱㶞譯作“峴港”\[1\]和“蜆港”\[2\]。

## 歷史

“沱㶞”之名最早见于1555年的《[烏州近錄](../Page/烏州近錄.md "wikilink")》，在该书的第五卷记载：“松江祠：祠在思榮縣思客海門，並在廣南陀㶞海門。”

16世紀時，作為[廣南阮主首都順化的外港會安](../Page/廣南國.md "wikilink")，是一個小漁村，作為與周邊地區貿易的港口，曾與日本商人進行交易（南蠻貿易）。

1858年8月，法軍在[拿破崙三世的命令下登陸並佔領](../Page/拿破崙三世.md "wikilink")，成為[殖民地](../Page/殖民地.md "wikilink")，並改名為「**土伦港**」（），但越南语汉字仍写作“沱㶞”。

1888年10月，[法屬印度支那總督府以土伦港為總督直轄地](../Page/法屬印度支那.md "wikilink")，是越南的第三大城市（僅次於西貢市和河內市）。

1945年，越盟發動八月革命。為紀念反法人士[蔡璠](../Page/蔡璠.md "wikilink")，曾一度改名“蔡璠市”，後恢復沱㶞之名。

在[越南共和國時代](../Page/越南共和國.md "wikilink")，峴港市主城区属于直辖市[岘港市社](../Page/岘港市社.md "wikilink")，和荣县和城郊属于[廣南省](../Page/廣南省.md "wikilink")。1976年，[越南南方共和国将广南省](../Page/越南南方共和国.md "wikilink")、[广沱省和岘港市合并为](../Page/广沱省.md "wikilink")[廣南-峴港省](../Page/廣南-峴港省.md "wikilink")，岘港市为该省的省辖市。

1996年11月6日，廣南-峴港省分拆成[廣南省和峴港](../Page/廣南省.md "wikilink")[直辖市](../Page/直辖市.md "wikilink")\[3\]。新设立的岘港直辖市包括原岘港市和[和荣县](../Page/和荣县.md "wikilink")、[黄沙县](../Page/黄沙县.md "wikilink")1市2县，并重新划分为海洲郡、莲沼郡、清溪郡、五行山郡、山茶郡、和荣县和黄沙县5郡2县。

2003年7月15日，岘港市被越南政府评定为第一级都市\[4\]。

2005年8月5日，增设锦荔郡\[5\]。

## 行政區劃

峴港市分為6個郡和2個縣。

  - [海洲郡](../Page/海洲郡.md "wikilink")（Quận Hải Châu）
  - [清溪郡](../Page/清溪郡.md "wikilink")（Quận Thanh Khê）
  - [山茶郡](../Page/山茶郡.md "wikilink")（Quận Sơn Trà）
  - [五行山郡](../Page/五行山郡.md "wikilink")（Quận Ngũ Hành Sơn）
  - [蓮沼郡](../Page/蓮沼郡.md "wikilink")（Quận Liên Chiểu）
  - [錦荔郡](../Page/錦荔郡.md "wikilink")（Quận Cẩm Lệ）
  - [和榮縣](../Page/和榮縣.md "wikilink")（Huyện Hòa Vang）
  - [黃沙縣](../Page/黃沙縣.md "wikilink")（Huyện Hoàng
    Sa)，争议领土，無实际控制權，為越南所虛設的縣份之一。

## 文化遺蹟

岘港市擁有眾多歷史文化遺蹟，如[占婆博物館](../Page/占婆.md "wikilink")，[高台教寺廟](../Page/高台教.md "wikilink")，等等。

## 氣候

## 教育

[One_view_of_Bach_Dang_street,_Da_Nang,_Vietnam_-_Indochina_Riverside.jpg](https://zh.wikipedia.org/wiki/File:One_view_of_Bach_Dang_street,_Da_Nang,_Vietnam_-_Indochina_Riverside.jpg "fig:One_view_of_Bach_Dang_street,_Da_Nang,_Vietnam_-_Indochina_Riverside.jpg")

  - 峴港大學
      - 百科大學
      - 經濟大學
      - 師範大學
      - 外語大學
      - 工業大學
  - 維新民立大學
  - 峴港建築大學

## 經濟

岘港市以工業為主，出口海鮮、藤製家具、家用品、衣物等。近期往高科技產業發展，故設立[峴港高科技園區](../Page/峴港高科技園區.md "wikilink")。

## 交通

峴港位於[東西經濟走廊的末端](../Page/東西經濟走廊.md "wikilink")。

[峴港國際機場位於市中心](../Page/峴港國際機場.md "wikilink")，是越南第三個[國際機場](../Page/國際機場.md "wikilink")。

## 旅游

岘港是越南中部重要交通枢纽，半径100平方公里之内范围内可直达锦南岛（Cẩm
Nam）、[占婆岛](../Page/占婆岛.md "wikilink")、（Bà
Nà）、、山茶半岛、[美山遗址](../Page/美山圣地.md "wikilink")、[顺化皇城](../Page/顺化皇城.md "wikilink")、[会安古镇等著名景点](../Page/會安市.md "wikilink")。

## 注释

## 外部链接

  - [岘港市政府电子信息门户网站](http://www.danang.gov.vn/)

  -
[峴港](../Category/峴港.md "wikilink") [Đà
Nẵng](../Category/越南直轄市.md "wikilink")
[Category:越南港口](../Category/越南港口.md "wikilink")
[Category:南中國海沿海城市](../Category/南中國海沿海城市.md "wikilink")
[Category:越南省份](../Category/越南省份.md "wikilink")

1.  《越南地圖集》（又名《越綿遼地圖集》），越南南圻中華總商會出版，1949年。
2.  見於二戰時越南華文資料。
3.  [国会决议：广南-岘港省恢复设立直辖市岘港市和广南省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-chia-va-dieu-chinh-dia-gioi-hanh-chinh-tinh-40091.aspx)
4.  [145/2003/QĐ-TTg：评定岘港市为第一级都市](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-145-2003-QD-TTg-cong-nhan-thanh-pho-Da-Nang-la-do-thi-loai-I/51143/noi-dung.aspx)
5.  [102/2005/NĐ-CP：岘港市增设锦荔郡](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-102-2005-ND-CP-thanh-lap-phuong-xa-thuoc-quan-thanh-khe-huyen-Hoa-Vang-thanh-lap-quan-Cam-Le-thanh-pho-Da-Nang-2725.aspx)