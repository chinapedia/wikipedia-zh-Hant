**岚皋路站**位于[上海](../Page/上海.md "wikilink")[普陀区](../Page/普陀区.md "wikilink")[岚皋路](../Page/岚皋路.md "wikilink")[銅川路](../Page/銅川路.md "wikilink")，为[上海轨道交通7号线的地下车站](../Page/上海轨道交通7号线.md "wikilink")，于2009年12月5日启用。

## 周边

## 車站設計

7号线车站为地下[岛式站台](../Page/岛式站台.md "wikilink")。

## 公交换乘

40、112、112区间、117、165、206、224、321、561、724、737、738、744、822、830、846、858、859、876、909、923、951、北安线区间、沪嘉专线（往成都北路方向单向停靠，下客站不上客）、沪唐专线、新嘉专线

## 车站出口

[Langao_Road_Station_2_Exit.JPG](https://zh.wikipedia.org/wiki/File:Langao_Road_Station_2_Exit.JPG "fig:Langao_Road_Station_2_Exit.JPG")

  - 1号口：岚皋路东侧，铜川路南
  - 2号口：岚皋路东侧，1号口南
  - 3号口：铜川路南侧，岚皋路西

[Category:2009年启用的铁路车站](../Category/2009年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")