[Okazakijo5.JPG](https://zh.wikipedia.org/wiki/File:Okazakijo5.JPG "fig:Okazakijo5.JPG")
[Okazakijo4.JPG](https://zh.wikipedia.org/wiki/File:Okazakijo4.JPG "fig:Okazakijo4.JPG")
[Okazakijo1.JPG](https://zh.wikipedia.org/wiki/File:Okazakijo1.JPG "fig:Okazakijo1.JPG")

**岡崎城**（）是位於[日本](../Page/日本.md "wikilink")[愛知縣](../Page/愛知縣.md "wikilink")[岡崎市的一座城堡](../Page/岡崎市.md "wikilink")。[德川家康於此地出生](../Page/德川家康.md "wikilink")。别名为**龙城**。

建於1452年，[戰國時代到](../Page/日本戰國時代.md "wikilink")[安土桃山時代为松平氏所有](../Page/安土桃山時代.md "wikilink")，[江戶時代是](../Page/江戶時代.md "wikilink")[岡崎藩藩廳所在地](../Page/岡崎藩.md "wikilink")。

最初城名写作“冈奇城”（）。此外，冈崎城是[享禄年间以来的名称](../Page/享禄.md "wikilink")，在此之前名为菅生乡\[1\]。

## 相關條目

  - [日本100名城](../Page/日本100名城.md "wikilink")
  - [日本城堡列表](../Page/日本城堡列表.md "wikilink")

## 外部連結

  - [三河武士のやかた家康](https://web.archive.org/web/20071117103657/http://www.city.okazaki.aichi.jp/museum/ka161.htm)

[Category:愛知縣城堡](../Category/愛知縣城堡.md "wikilink")
[Category:室町時代建築](../Category/室町時代建築.md "wikilink")
[Category:西鄉氏](../Category/西鄉氏.md "wikilink")
[Category:大草松平氏](../Category/大草松平氏.md "wikilink")
[Category:安祥松平氏](../Category/安祥松平氏.md "wikilink")
[Category:今川氏](../Category/今川氏.md "wikilink")
[Category:德川家康](../Category/德川家康.md "wikilink")
[Category:田中氏](../Category/田中氏.md "wikilink")
[Category:本多氏](../Category/本多氏.md "wikilink")
[Category:水野氏](../Category/水野氏.md "wikilink")
[Category:松井松平氏](../Category/松井松平氏.md "wikilink")
[Category:岡崎藩](../Category/岡崎藩.md "wikilink")
[Category:1959年完工建築物](../Category/1959年完工建築物.md "wikilink")
[Category:日本史跡](../Category/日本史跡.md "wikilink")

1.