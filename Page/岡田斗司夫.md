**岡田斗司夫**，[血型O型](../Page/血型.md "wikilink")，生於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")，自稱為Otaking（御宅之王），[大阪電氣通信大學](../Page/大阪電氣通信大學.md "wikilink")（電信大學）肄業。曾任[GAINAX公司](../Page/GAINAX.md "wikilink")[CEO社長](../Page/CEO.md "wikilink")、[東京大學講師等](../Page/東京大學.md "wikilink")；目前為「」會員、非小說類文學作家。

## 簡歷

  - 1980年在大阪舉辦的「第四次SF Show」，透過此事的出展機會，於1981年設立SF精品的專門店「General
    Products」。
  - 1984年，他和[庵野秀明等同好成立動畫製作公司](../Page/庵野秀明.md "wikilink")「[GAINAX](../Page/GAINAX.md "wikilink")」，並擔任CEO社長。有眾多的作品都出自於岡田之手，例如動畫《[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")》、《[勇往直前](../Page/勇往直前_\(動畫\).md "wikilink")》、《[海底兩萬浬](../Page/海底兩萬浬.md "wikilink")》，以及電玩遊戲《[美少女夢工廠](../Page/美少女夢工廠.md "wikilink")》等。他在1992年辭職，其原因仍不明，不過岡田目前仍為GAINAX的最大[股東之一](../Page/股東.md "wikilink")。
  - 1994年10月至1995年3月間，他擔任東京大學教養學部（通識教育學院）的特別講師，以次文化的「多媒體概論」為題材講授，1996年至1997間則以「御宅文化論」講授。也在立教大學講課。
  - 1995年與[唐澤俊一](../Page/唐澤俊一.md "wikilink")、[眠田直組成御宅藝人團體](../Page/眠田直.md "wikilink")「オタクアミーゴス」。
  - 1997年由於自己的演講與寫作活動而設立「Otaking（御宅之王）株式會社」，擔任CEO。
  - 2003年擔任美國[麻省理工學院的講師](../Page/麻省理工學院.md "wikilink")。
  - 2005年擔任大阪藝術大學藝術學系的角色造型學科的客座教授。
  - 2010年成立OTAKINGex組織
  - 2011年設立「CLOUDCITY株式會社」
  - 2015年「愛人目録」流出

他也在[NHK衛星第2頻道不定時播出之](../Page/NHK衛星第2頻道.md "wikilink")『BS
Manga夜談』擔任主要來賓，而且也在自己企劃的『BS Anime夜談』中擔任主持人。
岡田有被稱為「御宅領導人」及「御宅教主」。

「General
Products」製作以戰隊人物為腳本的業餘電影『[愛國戰隊大日本](../Page/愛國戰隊大日本.md "wikilink")』，同時激怒了右派、左派的人。

他曾在新宿「Loft Plus One」的座談會上指出「御宅族已死」，引起相當大的迴響。

目前仍活躍在部落格網站「プチ落語家『吉祥亭滿月』」中。

## 對御宅的分類

在「阿宅，你已經死了！」一書中提出御宅已死的說法；指出御宅是成人的特化（兒童發展為成人，成人發展為御宅）．
並將御宅分類為：御宅原人、御宅貴族主義（第一世代）、御宅菁英主義（第二世代）、御宅感受主義（第三世代）

## 主要著作

  - 我們的洗腦社會（）（1995年） ISBN 4022612444
  - 御宅學入門（）（1996年） ISBN 4102900195
  - 東大御宅學講座（）（1997年） ISBN 4062082926
  - 國際御宅大學 1998年 來自最前線的研究報告（）（1998年） ISBN 4334971822
  - 失落的未來（）（2000年） ISBN 4620314501
  - フロン 結婚生活・19の絶対法則（2001年） ISBN 4907727194
  - 日本御宅大賞（）（2003年） ISBN 4594039006
  - 戀愛自由市場主義宣言（）（2003年） ISBN 4821108429
  - プチクリ 好き=才能（2005年） ISBN 4344010825
  - 征服世界是可能的嗎？（）
      - 中文譯本：[時報文化](../Page/時報文化.md "wikilink")，2008年5月 ISBN
        978-957-13-4832-2
      - 日文原著：[筑摩書房](../Page/筑摩書房.md "wikilink")，2007年6月 ISBN 4480687629
  - 別為多出來的體重抓狂──絕不復胖！筆記瘦身法（）
      - 中文譯本：方智出版社，2008年9月 ISBN 978-986-175-122-1
      - 日文原著：[新潮社](../Page/新潮社.md "wikilink")，2007年8月 ISBN 4106102277
  - 阿宅，你已經死了！（）
      - 中文譯本：時報文化，2009年8月 ISBN 978-957-13-5065-3
      - 日文原著：新潮社，2008年4月 ISBN 9784106102585
  - 脱デブ（2008年）ISBN 9784789733236
  - レコーディング・ダイエット「公式」手帖（2009年）ISBN 9784796669757
  - 遺言（2010年）ISBN 9784480864055

## 東京大學「御宅文化論」的與談者一覽

  - ロト（[冰川龍介](../Page/冰川龍介.md "wikilink")）
  - [フレデリック・ショット](../Page/フレデリック・ショット.md "wikilink")
  - [皆神龍太郎](../Page/皆神龍太郎.md "wikilink")
  - [志水一夫](../Page/志水一夫.md "wikilink")
  - [村上隆](../Page/村上隆.md "wikilink")
  - [村崎百郎](../Page/村崎百郎.md "wikilink")
  - [青木光恵](../Page/青木光恵.md "wikilink")
  - [兵頭二十八](../Page/兵頭二十八.md "wikilink")
  - [唐澤俊一](../Page/唐澤俊一.md "wikilink")
  - [小林善紀](../Page/小林善紀.md "wikilink")
  - [淺羽通明](../Page/淺羽通明.md "wikilink")

## 外部連結

  - [岡田斗司夫的プチクリ日記](http://putikuri.way-nifty.com/blog/index.html)（Blog）
  - [OTAKING SPACE
    PORT](https://web.archive.org/web/20070313194319/http://www.netcity.or.jp/OTAKU/okada/index.html)（官方網站）

[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:御宅族](../Category/御宅族.md "wikilink")
[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:GAINAX人物](../Category/GAINAX人物.md "wikilink")