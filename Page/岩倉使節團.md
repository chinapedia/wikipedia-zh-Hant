[Iwakura_mission.jpg](https://zh.wikipedia.org/wiki/File:Iwakura_mission.jpg "fig:Iwakura_mission.jpg")、[山口尚芳](../Page/山口尚芳.md "wikilink")、[岩倉具視](../Page/岩倉具視.md "wikilink")、[伊藤博文](../Page/伊藤博文.md "wikilink")、[大久保利通](../Page/大久保利通.md "wikilink")\]\]
**岩仓使节团**（（いわくらしせつだん）），是[明治](../Page/明治.md "wikilink")4年11月12日（1871年12月23日）至明治6年（1873年9月13日）期间由日本政府派遣至美國及歐洲諸國訪察之使節團。使节团正使为[岩倉具視](../Page/岩倉具視.md "wikilink")，由政府官员、留学生等共107人组成。岩倉使節團並不是日本第一次向西方派遣使節團。在其之前，日本派遣過、[文久遣欧使節和](../Page/文久遣欧使節.md "wikilink")[横浜鎖港談判使節團](../Page/横浜鎖港談判使節團.md "wikilink")。

## 简介

使节团于明治4年（1871年）11月12日（旧历）从[横滨港登上海轮](../Page/横滨港.md "wikilink")，东渡[太平洋](../Page/太平洋.md "wikilink")，在美国[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[三藩市登陆](../Page/三藩市.md "wikilink")。此后，他们横跨美国大陆，访问了美国首都[华盛顿市](../Page/华盛顿市.md "wikilink")。使节团在美国累计停留长达8个月左右。

结束对美国的访问后，使节团渡过大西洋，访问了欧洲诸国。他们先后访问游历了[英国](../Page/英国.md "wikilink")（4个月）、[法国](../Page/法国.md "wikilink")（2个月）、[比利时](../Page/比利时.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[德国](../Page/德国.md "wikilink")（3周）、[俄国](../Page/俄国.md "wikilink")（2周）、[丹麦](../Page/丹麦.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")（参观了当时的[维也纳万国博览会](../Page/维也纳万国博览会.md "wikilink")）、[瑞士等十二国](../Page/瑞士.md "wikilink")。归国途中，他们从[地中海穿过](../Page/地中海.md "wikilink")[苏伊士运河](../Page/苏伊士运河.md "wikilink")，并经[红海](../Page/红海.md "wikilink")，拜访了沿途多个欧洲殖民地和亚洲城市（[斯里兰卡](../Page/斯里兰卡.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[西贡](../Page/西贡.md "wikilink")、[香港](../Page/香港.md "wikilink")、[上海等](../Page/上海.md "wikilink")），但停留时间都比较短。

使团回到日本的时间比原定计划大大推迟，历经1年10个月后，才于明治6年（1873年）9月13日回到了出发地的横滨港。当时，留守政府内关于出征朝鲜问题发生了[征韩论论战](../Page/征韩论.md "wikilink")，而在使节团回国后便发生了[明治六年政变](../Page/明治六年政变.md "wikilink")。

本来根据[大隈重信的计划](../Page/大隈重信.md "wikilink")，日本政府仅打算派遣小规模的使节团，但最后出于政治方面的考虑，使节团规模逐步扩大。政府的多位高官作为使节团成员长期在国外访问，这一做法在当时也非常罕见，但使节团与西方文明及思想的密切接触给政府首脑积累了宝贵的经验，因此此次使节团的周游列国得到了后世高度的评价。其中的留学生也在回国后活跃在政治、经济、教育、文化等各个领域，对日本的文明开化做出了重大的贡献。但另一方面，使节团企图越权修改条约导致的与留守政府的摩擦、访问时间的大幅延长、木户与大久保之间的冲突等政治问题，也使得此次使节团遭到了一定的非议。

访问期间，使节团大多数成员都剪发并着洋装，但岩仓保留了日式发型和和服的装扮。他的形象也刊载于美国报纸的插图上。访问之初，岩仓颇以日本传统文化为傲，但不久留学美国的儿子[岩仓具定劝说他](../Page/岩仓具定.md "wikilink")“不要以未开化的形象使国家受辱”，岩仓正使遂被说服，在[芝加哥决定改为西式发型](../Page/芝加哥.md "wikilink")，此后不久也改穿洋装。

## 使节团的目的

1.  对签订了条约的诸国进行友好访问，向对方元首递交国书
2.  为了修改[江户时代后期与西方诸强签订的](../Page/江户时代.md "wikilink")[不平等条约](../Page/不平等条约.md "wikilink")（史称[条约改正](../Page/条约改正.md "wikilink")）而进行预备性的谈判
3.  调查西洋文明

使节团最主要的目的是与各国增进友谊亲善以及考察欧美诸强的文化国情等，同时也带着在访问时试探各国对修改条约的态度这一使命。明治政府从明治初年开始，就一直试图修改旧幕府与各国之间签订的不平等条约，加上1872年7月1日（明治5年6月26日）也是与欧美十五国订立的修好条约的续签日期，各国同意在提前一年通知的情况下可以修改条约。明治政府企图利用这一绝好的机会修改或废除不平等条约\[1\]。

## 歷史意義

在1871年12月到1873年9月的一年九個月期間，來自明治政府工、農、礦、金融、文教、軍事、治安部門的數十名官員乘坐輪船和火車，考察了15個歐美國家。岩倉使節團離開日本的時候，日本基本上仍是一個封閉的國家，日本人對世界所知甚少。但是，這群官員考察了各國的[工廠](../Page/工廠.md "wikilink")、[礦山](../Page/礦山.md "wikilink")、[博物館](../Page/博物館.md "wikilink")、[公園](../Page/公園.md "wikilink")、[股票交易所](../Page/股票交易所.md "wikilink")、[鐵路](../Page/鐵路.md "wikilink")、[農場和](../Page/農場.md "wikilink")[造船廠](../Page/造船廠.md "wikilink")，令他們認識到，當時的日本不但需要引進新技術，更要引進新的組織和思維方式，唯有如此，方能將日本改造為現代國家。此次出訪，不僅讓使節團成員意識到日本與先進國家相比落後很多，也對如何進行變革形成了共識。使節團成員們不因所見所聞而沮喪，反而在回國後充滿幹勁，並迫不及待向海外派出更多的使節團進行更細微的考察。

## 使节团成员

使节46名、随员18名、留学生43名。使节主要以[萨摩](../Page/萨摩藩.md "wikilink")、[长州兩藩的官僚为主](../Page/長州藩.md "wikilink")，而书记官等也从旧幕府官员中选拔。

### 使节

[Genichiro_Fukuchi_3.jpg](https://zh.wikipedia.org/wiki/File:Genichiro_Fukuchi_3.jpg "fig:Genichiro_Fukuchi_3.jpg")\]\]
[Tadasu_Hayashi.png](https://zh.wikipedia.org/wiki/File:Tadasu_Hayashi.png "fig:Tadasu_Hayashi.png")
[Kume_Kunitake.jpg](https://zh.wikipedia.org/wiki/File:Kume_Kunitake.jpg "fig:Kume_Kunitake.jpg")》的[久米邦武](../Page/久米邦武.md "wikilink")\]\]
[Shinpachi_Murata.jpg](https://zh.wikipedia.org/wiki/File:Shinpachi_Murata.jpg "fig:Shinpachi_Murata.jpg")的[村田新八](../Page/村田新八.md "wikilink")\]\]

  - 特命全权大使

<!-- end list -->

  - [岩倉具視](../Page/岩倉具視.md "wikilink")(時任[右大臣](../Page/右大臣.md "wikilink"))

<!-- end list -->

  - 副使

<!-- end list -->

  - [木戶孝允](../Page/木戶孝允.md "wikilink")
    ([桂小五郎](../Page/桂小五郎.md "wikilink")，時任**參議**)
  - [大久保利通](../Page/大久保利通.md "wikilink")
    (時任[大藏卿](../Page/大藏卿.md "wikilink"))
  - [伊藤博文](../Page/伊藤博文.md "wikilink")
    (時任[工部大輔](../Page/工部大輔.md "wikilink"))
  - [山口尚芳](../Page/山口尚芳.md "wikilink")
    (時任[外務少輔](../Page/外務少輔.md "wikilink"))

<!-- end list -->

  - 一等书记官

<!-- end list -->

  - [田辺太一](../Page/田辺太一.md "wikilink")
  - [何礼之](../Page/何礼之.md "wikilink")
  - [福地源一郎](../Page/福地源一郎.md "wikilink")

<!-- end list -->

  - 二等书记官

<!-- end list -->

  - [渡辺洪基](../Page/渡辺洪基.md "wikilink")
  - [小松済治](../Page/小松済治.md "wikilink")
  - [林董三郎](../Page/林董.md "wikilink")
  - [長野桂次郎](../Page/長野桂次郎.md "wikilink")（立石斧次郎）

<!-- end list -->

  - 三等书记官

<!-- end list -->

  - [川路寛堂](../Page/川路寛堂.md "wikilink")

<!-- end list -->

  - 四等书记官

<!-- end list -->

  - [安藤太郎](../Page/安藤太郎_\(外交官\).md "wikilink")
  - [池田政懋](../Page/池田政懋.md "wikilink")

<!-- end list -->

  - 大使随行

<!-- end list -->

  - [久米邦武](../Page/久米邦武.md "wikilink")
  - [中山信彬](../Page/中山信彬.md "wikilink")
  - [内海忠勝](../Page/内海忠勝.md "wikilink")
  - [野村靖](../Page/野村靖.md "wikilink")
  - [五辻安仲](../Page/五辻安仲.md "wikilink")

<!-- end list -->

  - 理事官

<!-- end list -->

  - [田中光顕](../Page/田中光顕.md "wikilink")
  - [東久世通禧](../Page/東久世通禧.md "wikilink")
  - [山田顕義](../Page/山田顕義.md "wikilink")
  - [佐佐木高行](../Page/佐々木高行.md "wikilink")
  - [田中不二麿](../Page/田中不二麿.md "wikilink")
  - [肥田為良](../Page/肥田浜五郎.md "wikilink")

<!-- end list -->

  - 随行

<!-- end list -->

  - [村田新八](../Page/村田新八.md "wikilink")
  - [原田一道](../Page/原田一道.md "wikilink")
  - [長與專齋](../Page/長與專齋.md "wikilink")
  - [安場保和](../Page/安場保和.md "wikilink")
  - [若山儀一](../Page/若山儀一.md "wikilink")
  - [阿部潜](../Page/阿部潜.md "wikilink")
  - [沖守固](../Page/沖守固.md "wikilink")
  - [富田命保](../Page/富田命保.md "wikilink")
  - [杉山一成](../Page/杉山一成.md "wikilink")
  - [吉雄永昌](../Page/吉雄永昌.md "wikilink")
  - [中島永元](../Page/中島永元.md "wikilink")
  - [近藤鎮三](../Page/近藤鎮三.md "wikilink")
  - [今村和郎](../Page/今村和郎.md "wikilink")
  - [内村公平](../Page/内村公平.md "wikilink")
  - [大島高任](../Page/大島高任.md "wikilink")
  - [瓜生震](../Page/瓜生震.md "wikilink")
  - [岡内重俊](../Page/岡内重俊.md "wikilink")
  - [中野健明](../Page/中野健明.md "wikilink")
  - [平賀義質](../Page/平賀義質.md "wikilink")
  - [長野文炳](../Page/長野文炳.md "wikilink")

### 留学生

[Kaneko_Kentaro_1-1.jpg](https://zh.wikipedia.org/wiki/File:Kaneko_Kentaro_1-1.jpg "fig:Kaneko_Kentaro_1-1.jpg")
[First_female_study-abroad_students.jpg](https://zh.wikipedia.org/wiki/File:First_female_study-abroad_students.jpg "fig:First_female_study-abroad_students.jpg")

  - 留学英国学生

<!-- end list -->

  - [中江兆民](../Page/中江兆民.md "wikilink")
  - [鍋島直大](../Page/鍋島直大.md "wikilink")
  - [前田利嗣](../Page/前田利嗣.md "wikilink")
  - [毛利元敏](../Page/毛利元敏.md "wikilink")

<!-- end list -->

  - 留学英国、法国学生

<!-- end list -->

  - [前田利同](../Page/前田利同.md "wikilink")

<!-- end list -->

  - 留学美国学生

<!-- end list -->

  - [金子堅太郎](../Page/金子堅太郎.md "wikilink")
  - [團琢磨](../Page/團琢磨.md "wikilink")
  - [牧野伸顕](../Page/牧野伸顕.md "wikilink")
  - [黒田長知](../Page/黒田長知.md "wikilink")
  - [鳥居忠文](../Page/鳥居忠文.md "wikilink")
  - [津田梅子](../Page/津田梅子.md "wikilink")
  - [山川捨松](../Page/山川捨松.md "wikilink")
  - [永井繁子](../Page/永井繁子.md "wikilink")
  - [吉川重吉](../Page/吉川重吉.md "wikilink")
  - [木戸孝正](../Page/木戸孝正.md "wikilink")
  - [日下義雄](../Page/日下義雄.md "wikilink")
  - [山脇正勝](../Page/山脇正勝.md "wikilink")
  - [高木貞作](../Page/高木貞作.md "wikilink")

<!-- end list -->

  - 留学德国学生

<!-- end list -->

  - [平田東助](../Page/平田東助.md "wikilink")

<!-- end list -->

  - 留学俄国学生

<!-- end list -->

  - [清水谷公考](../Page/清水谷公考.md "wikilink")
  - [万里小路正秀](../Page/万里小路正秀.md "wikilink")

<!-- end list -->

  - 留学

<!-- end list -->

  - [大村純熈](../Page/大村純熈.md "wikilink")
  - [朝永甚次郎](../Page/朝永甚次郎.md "wikilink")
  - [長岡治三郎](../Page/長岡治三郎.md "wikilink")
  - [大鳥圭介](../Page/大鳥圭介.md "wikilink")
  - [坊城俊章](../Page/坊城俊章.md "wikilink")
  - [武者小路実世](../Page/武者小路実世.md "wikilink")
  - [錦小路頼言](../Page/錦小路頼言.md "wikilink")

### 随员

[Jo_Niijima.jpg](https://zh.wikipedia.org/wiki/File:Jo_Niijima.jpg "fig:Jo_Niijima.jpg")，毕生从事基督教传教的[新島襄](../Page/新島襄.md "wikilink")\]\]

  - 随员

<!-- end list -->

  - [新島襄](../Page/新島襄.md "wikilink")（翻译）
  - [岩倉具綱](../Page/岩倉具綱.md "wikilink")
  - [大久保利和](../Page/大久保利和.md "wikilink")
  - [牧野伸顕](../Page/牧野伸顕.md "wikilink")
  - [山縣伊三郎](../Page/山縣伊三郎.md "wikilink")
  - [山口俊太郎](../Page/山口俊太郎.md "wikilink")
  - [吉井幸輔](../Page/吉井友実.md "wikilink")
  - [吉原重俊](../Page/吉原重俊.md "wikilink")（大原令之助）
  - [畠山義成](../Page/畠山義成.md "wikilink")（杉浦弘蔵）

等。

## 相关条目

  - [日本历史年表](../Page/日本历史年表.md "wikilink")
  - [明治人物列表](../Page/明治人物列表.md "wikilink")
  - [殖産興業](../Page/殖産興業.md "wikilink")
  - [富国強兵](../Page/富国強兵.md "wikilink")

## 参考文献

  - [久米邦武编著](../Page/久米邦武.md "wikilink")　《米欧回覧実記》
    （明治11年刊，全100巻、博聞社　1878年）

:\*※復刻版 『特命全権大使米欧回覧実記』（全5巻 宗高書房　1975年）

  - [田中彰校注](../Page/田中彰_\(歴史学者\).md "wikilink")　『米欧回覧実記』　[岩波文庫](../Page/岩波文庫.md "wikilink")
    全5巻、初版1977-82年／※単行判5巻組、[岩波書店](../Page/岩波書店.md "wikilink")、1985年
  - 水澤周訳注　『現代語訳　特命全権大使 米欧回覧実記』
    5巻組：[慶應義塾大学出版会](../Page/慶應義塾大学出版会.md "wikilink")、2005年　　
    　
      - 新版[選書判](../Page/選書.md "wikilink")　『現代語訳　特命全権大使　米欧回覧実記』　全5巻＋別冊総索引、2008年

<!-- end list -->

  -
    '''〔专业性著作・単著〕　

<!-- end list -->

  - 久米美術館編 『岩倉使節団関係　久米邦武文書.3』　[吉川弘文館](../Page/吉川弘文館.md "wikilink")、2001年
      - 久米美術館編 『特命全権大使　「米欧回覧実記」銅板画集』 1985年
      - 久米美術館編 『銅鐫にみる文明のフォルム　<small>「米欧回覧実記」挿絵銅版画とその時代展」資料集</small>』
        2006年
  - 田中彰　『岩倉使節団の歴史的研究』　[岩波書店](../Page/岩波書店.md "wikilink")　2002年
  - [イアン・ニッシュ編](../Page/イアン・ニッシュ.md "wikilink")
    、[麻田貞雄ほか訳](../Page/麻田貞雄.md "wikilink")　『欧米から見た岩倉使節団』
    　[ミネルヴァ書房](../Page/ミネルヴァ書房.md "wikilink") \[MINERVA日本史ライブラリー12\]
    　2002年
  - [芳賀徹編](../Page/芳賀徹.md "wikilink")　『岩倉使節団の比較文化史的研究』　思文閣出版　2003年
  - 米欧回覧の会編　『岩倉使節団の再発見』　思文閣出版、2003年
  - 米欧亜回覧の会編　『世界の中の日本の役割を考える　<small>岩倉使節団を出発点として</small>』
    　芳賀徹・[松本健一](../Page/松本健一.md "wikilink")・[齋藤希史ほか多数](../Page/齋藤希史.md "wikilink")、[慶應義塾大学出版会](../Page/慶應義塾大学出版会.md "wikilink")、2009年
  - 田中彰・高田誠二編著　『「米欧回覧実記」の学際的研究』　[北海道大学図書刊行会](../Page/北海道大学.md "wikilink")、1993年
  - [西川長夫](../Page/西川長夫.md "wikilink")・松宮秀治編
    　『「米欧回覧実記」を読む　1870年代の世界と日本』　法律文化社　1995年
  - 岩倉翔子編著　『岩倉使節団とイタリア』　[京都大学学術出版会](../Page/京都大学学術出版会.md "wikilink")　1997年

<!-- end list -->

  -
    '''〔一般向けの書籍〕　　

<!-- end list -->

  - [田中彰](../Page/田中彰_\(歴史学者\).md "wikilink")　『岩倉使節団「米欧回覧実記」』（[講談社現代新書](../Page/講談社現代新書.md "wikilink")　1977年／[岩波現代文庫](../Page/岩波現代文庫.md "wikilink")　2002年）
      - 『[明治維新と西洋文明](../Page/明治維新.md "wikilink")　岩倉使節団は何を見たか』（[岩波新書](../Page/岩波新書.md "wikilink")　2003年）
      - 『「脱亜」の明治維新　岩倉使節団を追う旅から』（[日本放送出版協会](../Page/日本放送出版協会.md "wikilink")［[NHKブックス](../Page/日本放送協会.md "wikilink")］　1984年、[オンデマンド版](../Page/オンデマンド.md "wikilink")　2003年）
  - [宮永孝](../Page/宮永孝.md "wikilink")　『アメリカの岩倉使節団』（[筑摩書房](../Page/筑摩書房.md "wikilink")［ちくまライブラリー20］　1992年）
      - 『白い崖の国をたずねて　岩倉使節団の旅　[木戸孝允のみたイギリス](../Page/木戸孝允.md "wikilink")』（[集英社](../Page/集英社.md "wikilink")　1997年）
  - [萩原延壽](../Page/萩原延壽.md "wikilink")　『岩倉使節団－遠い崖　[アーネスト・サトウ日記抄](../Page/アーネスト・サトウ.md "wikilink")9』　（[朝日新聞社](../Page/朝日新聞社.md "wikilink")　2000年／[朝日文庫](../Page/朝日新聞出版.md "wikilink")　2008年）
  - [泉三郎](../Page/泉三郎.md "wikilink")　『誇り高き日本人　国の命運を背負った岩倉使節団の物語』　（[PHP研究所](../Page/PHP研究所.md "wikilink")　2008年／「岩倉使節団　誇り高き男たちの物語」
    祥伝社黄金文庫　2012年）
      - 『岩倉使節団という冒険』（[文春新書](../Page/文春新書.md "wikilink")　2004年）
      - 『堂々たる日本人　この国のかたちを創った岩倉使節団「米欧回覧」の旅』
        （[祥伝社](../Page/祥伝社.md "wikilink")　1996年／祥伝社黄金文庫、2004年）
      - 『写真・絵図で甦る堂々たる日本人　この国のかたちを創った岩倉使節団「米欧回覧」の旅』　（祥伝社　2001年）
  - [高田誠二](../Page/高田誠二.md "wikilink")　『維新の科学精神
    　「米欧回覧実記」の見た産業技術』　（[朝日選書](../Page/朝日選書.md "wikilink")　1995年）
  - 高田誠二　『久米邦武　史学の眼鏡で浮世の景を』　（〈日本評伝選〉[ミネルヴァ書房](../Page/ミネルヴァ書房.md "wikilink")　2007年）
  - 芳賀徹ほか　『NHK市民大学　岩倉使節団の西洋見聞　<small>～「米欧回覧実記」を読む～</small>』
    （[日本放送出版協会](../Page/日本放送出版協会.md "wikilink")　1990年）

## 脚注

<references />

## 外部链接

  - [国立国会図書館 憲政資料室
    岩倉使節団文書（MF：国立公文書館蔵）](https://web.archive.org/web/20080612212025/http://www.ndl.go.jp/jp/data/kensei_shiryo/kensei/iwakurashisetsudannmonnjo.html)
  - [特命全権大使米欧回覧実記 /
    久米邦武編，博聞社，明治11年10月](http://iss.ndl.go.jp/api/openurl?ndl_jpno=40006071)（[近代数据图书馆](../Page/近代数据图书馆.md "wikilink")）

[Category:明治時代](../Category/明治時代.md "wikilink")
[Category:明治维新](../Category/明治维新.md "wikilink")
[Category:战前日本外交](../Category/战前日本外交.md "wikilink")

1.