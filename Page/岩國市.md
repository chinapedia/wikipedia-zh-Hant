**岩國市**（）是位於[日本](../Page/日本.md "wikilink")[中國地方](../Page/中國地方.md "wikilink")[山口縣最東端的城市](../Page/山口縣.md "wikilink")，以為界與[廣島縣相接](../Page/廣島縣.md "wikilink")。

[錦帶橋為轄內最具代表性的景點](../Page/錦帶橋.md "wikilink")，轄內也設有[岩國飛行場](../Page/岩國飛行場.md "wikilink")，為日本[海上自衛隊與](../Page/海上自衛隊.md "wikilink")[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")[駐日部隊共用的軍事基地](../Page/駐日美軍.md "wikilink")。

由於距離[廣島市僅](../Page/廣島市.md "wikilink")35公里且鐵公路交通便利，因此在經濟上與廣島的關係遠較山口縣內其他城市為緊密，有時候也會被戲稱為[廣島縣岩國市](../Page/廣島縣.md "wikilink")。

## 歷史

岩國地區過去是[山陽道的交通要衝](../Page/山陽道.md "wikilink")，在[室町時代是由](../Page/室町時代.md "wikilink")[岩國氏和](../Page/岩國氏.md "wikilink")[廣中氏所統治](../Page/廣中氏.md "wikilink")，在[戰國時代後成為](../Page/戰國時代_\(日本\).md "wikilink")[毛利氏的領地](../Page/毛利氏.md "wikilink")，[江戶時代則成為毛利氏](../Page/江戶時代.md "wikilink")
親族[吉川氏的領地](../Page/吉川氏.md "wikilink")，江戶時代結束後，一度短暫設立為[岩國藩](../Page/岩國藩.md "wikilink")，但在1871年實施[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，改設為[岩國縣](../Page/岩國縣.md "wikilink")，但同年又被併入山口縣。

由於地理鄰近軍事重鎮廣島市、[吳市和](../Page/吳市.md "wikilink")[江田島市](../Page/江田島市.md "wikilink")，因此在昭和時代設置了[日本陸軍燃料廠](../Page/日本陸軍.md "wikilink")、海軍潛水艇訓練基地和海軍岩國航空隊。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[玖珂郡](../Page/玖珂郡.md "wikilink")**岩國町**、[橫山村](../Page/橫山村.md "wikilink")、麻里布村、[川下村](../Page/川下村.md "wikilink")、[愛宕村](../Page/愛宕村.md "wikilink")、[灘村](../Page/灘村.md "wikilink")、[小瀨川村](../Page/小瀨川村.md "wikilink")、[御庄村](../Page/御庄村.md "wikilink")、[北河內村](../Page/北河內村.md "wikilink")、[南河內村](../Page/南河內村.md "wikilink")、[師木野村](../Page/師木野村.md "wikilink")、[通津村](../Page/通津村.md "wikilink")、由宇村、[神代村](../Page/神代村.md "wikilink")、玖珂村、[本鄉村](../Page/本鄉村_\(山口縣\).md "wikilink")、高森村、[祖生村](../Page/祖生村.md "wikilink")、[米川村](../Page/米川村.md "wikilink")、[川越村](../Page/川越村.md "wikilink")、廣瀨村、[深須村](../Page/深須村.md "wikilink")、[高根村](../Page/高根村.md "wikilink")、[河波村](../Page/河波村.md "wikilink")、[桑根村](../Page/桑根村.md "wikilink")、[秋中村](../Page/秋中村.md "wikilink")、[賀見畑村](../Page/賀見畑村.md "wikilink")、[涉前村](../Page/涉前村.md "wikilink")、[藤谷村](../Page/藤谷村.md "wikilink")。
  - 1904年1月1日：涉前村和藤谷村合併為坂上村。
  - 1905年4月1日：岩國町與橫山村合併為新設置的岩國町。
  - 1899年4月1日：小瀨川村被分割為[小瀨村和和木村](../Page/小瀨村.md "wikilink")（後來的[和木町](../Page/和木町.md "wikilink")）兩村。
  - 1911年7月1日：河波村的部分地區被併入本鄉村，其餘地區則獨立改名為[河山村](../Page/河山村.md "wikilink")。
  - 1916年6月1日：[藤河村從御庄村分村](../Page/藤河村.md "wikilink")。
  - 1924年8月1日：
      - 玖珂村改制為[玖珂町](../Page/玖珂町.md "wikilink")。
      - 高森村改制為[高森町](../Page/高森町_\(山口縣\).md "wikilink")。
  - 1926年2月11日：由宇村改制為[由宇町](../Page/由宇町.md "wikilink")。
  - 1928年4月29日：麻里布村改制為[麻里布町](../Page/麻里布町.md "wikilink")。
  - 1940年4月1日：岩國町、麻里布町、川下村、愛宕村、灘村[合併為](../Page/市町村合併.md "wikilink")**岩國市**。
  - 1940年11月3日：廣瀨村改制為[廣瀨町](../Page/廣瀨町.md "wikilink")。
  - 1955年4月1日：
      - 小瀨村、藤河村、御庄村、北河內村、南河內村、師木野村和通津村被併入岩國市。
      - 神代村的部分地區與鳴門村合併為大畠村（現為[柳井市的一部分](../Page/柳井市.md "wikilink")），其餘地區被併入由宇町。
      - 高森町、祖生村、米川村、川越村合併為[周東町](../Page/周東町.md "wikilink")。
      - 廣瀨町、深須村、高根村合併為[錦町](../Page/錦町_\(山口縣\).md "wikilink")。
      - 秋中村、賀見畑村合併為美和町。
  - 1955年7月20日：河山村和桑根村合併為美川村。
  - 1956年9月30日：美和町和坂上村合併為[美和町](../Page/美和町_\(山口縣\).md "wikilink")。
  - 1959年4月1日：美川村改制為[美川町](../Page/美川町_\(山口縣\).md "wikilink")。
  - 2006年3月20日：岩國市與由宇町、玖珂町、本鄉村、周東町、錦町、美川町、美和町[合併為新的](../Page/市町村合併.md "wikilink")**岩國市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>小瀨川村</p></td>
<td><p>1899年4月1日<br />
和木村</p></td>
<td><p>1973年4月1日<br />
改制為和木町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1899年4月1日<br />
小瀨村</p></td>
<td><p>1955年4月1日<br />
合併為岩國市</p></td>
<td><p>2006年3月20日<br />
合併為岩國市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岩國町</p></td>
<td><p>1905年4月1日<br />
合併為岩國町</p></td>
<td><p>1940年4月1日<br />
岩國市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>橫山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>麻裡布村</p></td>
<td><p>1928年4月29日<br />
改制為麻裡布町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>川下村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>愛宕村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>灘村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>御庄村</p></td>
<td><p>御庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1916年6月1日<br />
藤河村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北河內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南河內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>師木野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>通津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>玖珂村</p></td>
<td><p>1924年8月1日<br />
玖珂町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>本鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高森村</p></td>
<td><p>1924年8月1日<br />
高森町</p></td>
<td><p>1955年4月1日<br />
周東町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>祖生村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>米川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>川越村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>廣瀨村</p></td>
<td><p>1940年11月3日<br />
改制為廣瀨町</p></td>
<td><p>1955年4月1日<br />
錦町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>深須村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高根村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>河波村</p></td>
<td><p>1911年7月1日<br />
改名為河山村</p></td>
<td><p>1955年7月20日<br />
合併為美川村。</p></td>
<td><p>1959年4月1日<br />
改制為美川町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>桑根村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>秋中村</p></td>
<td><p>1955年4月1日<br />
美和町</p></td>
<td><p>1956年9月30日<br />
合併為美和町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>賀見畑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>涉前村</p></td>
<td><p>1904年1月1日<br />
坂上村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>藤谷村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>由宇村</p></td>
<td><p>1926年2月11日<br />
改制為由宇町</p></td>
<td><p><br />
由宇町<br />
<br />
</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神代村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1955年4月1日<br />
大畠村</p></td>
<td><p>1971年4月1日<br />
改制為大畠町。</p></td>
<td><p>2005年2月21日<br />
合併為柳井市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：[新岩國車站](../Page/新岩國車站.md "wikilink")
      - [山陽本線](../Page/山陽本線.md "wikilink")：[岩國車站](../Page/岩國車站.md "wikilink")
        - [南岩國車站](../Page/南岩國車站.md "wikilink") -
        [藤生車站](../Page/藤生車站.md "wikilink") -
        [通津車站](../Page/通津車站.md "wikilink") -
        [由宇車站](../Page/由宇車站.md "wikilink") -
        [神代車站](../Page/神代車站_\(山口縣\).md "wikilink")
      - [岩德線](../Page/岩德線.md "wikilink")：岩國車站 -
        [西岩國車站](../Page/西岩國車站.md "wikilink") -
        [川西車站](../Page/川西車站_\(山口縣\).md "wikilink") -
        [柱野車站](../Page/柱野車站.md "wikilink") -
        [欽明路車站](../Page/欽明路車站.md "wikilink") -
        [玖珂車站](../Page/玖珂車站.md "wikilink") -
        [周防高森車站](../Page/周防高森車站.md "wikilink") -
        [米川車站](../Page/米川車站.md "wikilink")
  - [錦川鐵道](../Page/錦川鐵道.md "wikilink")
      - [錦川清流線](../Page/錦川清流線.md "wikilink")：[川西車站](../Page/川西車站_\(山口縣\).md "wikilink")
        - [御庄車站](../Page/御庄車站.md "wikilink") -
        [守內瘡神車站](../Page/守內瘡神車站.md "wikilink") -
        [南河內車站](../Page/南河內車站.md "wikilink") -
        [行波車站](../Page/行波車站.md "wikilink") -
        [北河內車站](../Page/北河內車站_\(山口縣\).md "wikilink") -
        [椋野車站](../Page/椋野車站.md "wikilink") -
        [南桑車站](../Page/南桑車站.md "wikilink") -
        [根笠車站](../Page/根笠車站.md "wikilink") -
        [河山車站](../Page/河山車站.md "wikilink") -
        [柳瀨車站](../Page/柳瀨車站.md "wikilink") -
        [錦町車站](../Page/錦町車站.md "wikilink")

### 港口

  - [岩國港](../Page/岩國港.md "wikilink")

### 纜車

  - [岩國城纜車](../Page/岩國城纜車.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [山陽自動車道](../Page/山陽自動車道.md "wikilink")：[岩國交流道](../Page/岩國交流道.md "wikilink")
    - [玖珂休息區](../Page/玖珂休息區.md "wikilink") -
    [玖珂交流道](../Page/玖珂交流道.md "wikilink")
  - [岩國大竹道路](../Page/岩國大竹道路.md "wikilink")（興建中）：[室之木交流道](../Page/室之木交流道.md "wikilink")
    - [山手交流道](../Page/山手交流道.md "wikilink")
  - [中國自動車道](../Page/中國自動車道.md "wikilink")：[深谷休息區](../Page/深谷休息區.md "wikilink")

## 觀光資源

[Iwakuni_castle_05-03.jpg](https://zh.wikipedia.org/wiki/File:Iwakuni_castle_05-03.jpg "fig:Iwakuni_castle_05-03.jpg")
[20100724_Iwakuni_5235.jpg](https://zh.wikipedia.org/wiki/File:20100724_Iwakuni_5235.jpg "fig:20100724_Iwakuni_5235.jpg")
[USMC-091018-M-1298M-810.jpg](https://zh.wikipedia.org/wiki/File:USMC-091018-M-1298M-810.jpg "fig:USMC-091018-M-1298M-810.jpg")

  - 岩國地區

<!-- end list -->

  - [白崎八幡宮](../Page/白崎八幡宮.md "wikilink")
  - [錦帶橋](../Page/錦帶橋.md "wikilink")
  - [岩國城](../Page/岩國城.md "wikilink")
  - [櫻井戶](../Page/櫻井戶.md "wikilink")（[名水百選](../Page/名水百選.md "wikilink")）
  - [通津海水浴場](../Page/通津海水浴場.md "wikilink")
  - [岩國徴古館](../Page/岩國徴古館.md "wikilink")
  - [岩國美術館+柏原Collection](../Page/岩國美術館+柏原Collection.md "wikilink")

<!-- end list -->

  - 錦地區

<!-- end list -->

  - 羅漢山
  - 寂地峽
  - [雙津峽溫泉](../Page/雙津峽溫泉.md "wikilink")
  - [深谷峽溫泉](../Page/深谷峽溫泉.md "wikilink")

<!-- end list -->

  - 美和地域

<!-- end list -->

  - [彌榮峽](../Page/彌榮峽.md "wikilink")
  - 彌榮湖（[彌榮水庫](../Page/彌榮水庫.md "wikilink")）

### 行事·活動

  - 錦帶橋祭（4月29日）
  - 日美親善日（5月5日）美軍[岩國基地對外開放](../Page/岩國基地.md "wikilink")、軍用機展示、飛行表演等
  - 錦川[鵜鶘捕魚操演](../Page/鵜鶘.md "wikilink")（6月1日〜8月31日
    [錦帶橋附近的錦川](../Page/錦帶橋.md "wikilink")）
  - 彌榮湖Sport Festival（7月最後週六、週日）
  - 錦川水之祭典（8月第一週六）錦帯橋週邊的煙火大會
  - 周防祖生的柱松行事（8月中旬）周東町祖生地區一帶 日本重要無形民俗文化財
  - 岩国行波神舞（10月中旬）行波地區 日本重要無形民俗文化財
  - 岩国祭（10月第三週六、週日）[岩國車站前一帶](../Page/岩國車站.md "wikilink")
  - Yu you Festa（10月）由宇町週邊
  - 鞍掛城祭（11月）玖珂町週邊
  - 三樂祭（11月）美和町一帶
  - 周東食肉Fair（11月最後週日）周東町一帶

## 教育

### 短期大學

  - [岩國短期大學](../Page/岩國短期大學.md "wikilink")

### 高等學校

  - [山口縣立岩國高等學校](../Page/山口縣立岩國高等學校.md "wikilink")
      - 坂上分校
      - 廣瀨分校
  - [山口縣立岩國綜合高等學校](../Page/山口縣立岩國綜合高等學校.md "wikilink")
  - [山口縣立岩國商業高等學校](../Page/山口縣立岩國商業高等學校.md "wikilink")
  - [山口縣立岩國工業高等學校](../Page/山口縣立岩國工業高等學校.md "wikilink")
  - [山口縣立高森高等學校](../Page/山口縣立高森高等學校.md "wikilink")
  - [高水高等學校](../Page/高水高等學校.md "wikilink")

## 媒体

  - [山口新闻岩国支局](../Page/山口新闻.md "wikilink")

## 姊妹、友好都市

### 日本

  - [鳥取市](../Page/鳥取市.md "wikilink")（[鳥取縣](../Page/鳥取縣.md "wikilink")）

### 海外

  - [埃弗里特](../Page/埃弗里特_\(華盛頓州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[斯諾霍米什郡](../Page/斯諾霍米什郡_\(華盛頓州\).md "wikilink")）

  - [容迪亞伊](../Page/容迪亞伊.md "wikilink")（[巴西](../Page/巴西.md "wikilink")[聖保羅州](../Page/聖保羅州.md "wikilink")）

  - [太倉市](../Page/太倉市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[蘇州市](../Page/蘇州市.md "wikilink")）

## 本地出身之名人

  - [蘆岡俊明](../Page/蘆岡俊明.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [岩川隆](../Page/岩川隆.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [岩瀨成子](../Page/岩瀨成子.md "wikilink")：[兒童文學作家](../Page/兒童文學.md "wikilink")
  - [宇津本直紀](../Page/宇津本直紀.md "wikilink")：前[DEEN成員](../Page/DEEN.md "wikilink")、[音樂製作人](../Page/音樂製作人.md "wikilink")
  - [宇野千代](../Page/宇野千代.md "wikilink")：作家
  - [江木翼](../Page/江木翼.md "wikilink")：前[貴族院議員](../Page/貴族院.md "wikilink")、曾任司法大臣、鐵道大臣
  - [大友柳太朗](../Page/大友柳太朗.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [岡本信人](../Page/岡本信人.md "wikilink")：俳優
  - [河上肇](../Page/河上肇.md "wikilink")：經濟學者
  - [魁傑將晃](../Page/魁傑將晃.md "wikilink")：前[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")
  - [栗栖赳夫](../Page/栗栖赳夫.md "wikilink")：前[大藏大臣](../Page/大藏大臣.md "wikilink")
  - [琴岩國武士](../Page/琴岩國武士.md "wikilink")：前大相撲力士
  - [佐古正人](../Page/佐古正人.md "wikilink")：俳優、[配音員](../Page/配音員.md "wikilink")
  - [重宗雄三](../Page/重宗雄三.md "wikilink")：前[日本參議院議長](../Page/日本參議院.md "wikilink")
  - [難波圭一](../Page/難波圭一.md "wikilink")：配音員
  - [田島直人](../Page/田島直人.md "wikilink")：[田徑選手](../Page/田徑.md "wikilink")、[1936年夏季奧林匹克運動會三級跳金牌](../Page/1936年夏季奧林匹克運動會.md "wikilink")
  - [田中稻城](../Page/田中稻城.md "wikilink")：帝國圖書館（現在的[國立國會圖書館](../Page/國立國會圖書館.md "wikilink")）第一任館長
  - [田中穗積](../Page/田中穗積_\(作曲家\).md "wikilink")：作曲家
  - [玉乃世履](../Page/玉乃世履.md "wikilink")：第一任[大審院長](../Page/大審院.md "wikilink")
  - [西村茂生](../Page/西村茂生.md "wikilink")：曾任岩國市市長、眾議院議員
  - [長谷川好道](../Page/長谷川好道.md "wikilink")：前日本陸軍元帥
  - [平岡秀夫](../Page/平岡秀夫.md "wikilink")：日本眾議院議員
  - [弘兼憲史](../Page/弘兼憲史.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
      - [島耕作](../Page/島耕作.md "wikilink")：弘兼憲史所創造出來的一個漫畫人物
  - [福田良彥](../Page/福田良彥.md "wikilink")：岩國市市長、曾任日本眾議院議員
  - [藤井かすみ](../Page/藤井かすみ.md "wikilink")：女子[職業高爾夫選手](../Page/職業高爾夫.md "wikilink")
  - [藤岡市助](../Page/藤岡市助.md "wikilink")：[東芝創辦人](../Page/東芝.md "wikilink")
  - [藤重政孝](../Page/藤重政孝.md "wikilink")：歌手、俳優
  - [藤谷光信](../Page/藤谷光信.md "wikilink")：參議院議員
  - [又野誠治](../Page/又野誠治.md "wikilink")：俳優
  - [松林和雄](../Page/松林和雄.md "wikilink")：前職業棒球選手
  - [松林慎司](../Page/松林慎司.md "wikilink")：俳優
  - [真柴あずき](../Page/真柴あずき.md "wikilink")：編劇
  - [三上真司](../Page/三上真司.md "wikilink")：遊戲製作人，被稱為「惡靈古堡之父」
  - [森慎二](../Page/森慎二.md "wikilink")：職業棒球選手
  - [山口瑠美](../Page/山口瑠美.md "wikilink")：演歌歌手
  - [吉田矢健治](../Page/吉田矢健治.md "wikilink")：作曲家
  - [石田ニコル](../Page/石田ニコル.md "wikilink")：模特兒

## 相關條目

  - [岩國海軍陸戰隊航空基地](../Page/岩國海軍陸戰隊航空基地.md "wikilink")（岩國飛行場）

## 外部連結

  - [岩國市觀光協會](http://www.iwakuni-kanko.jp/)