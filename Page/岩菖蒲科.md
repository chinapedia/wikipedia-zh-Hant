**岩菖蒲科**只有3[属](../Page/属.md "wikilink")27[种](../Page/种.md "wikilink")，主要分布在北半球寒冷区域，[中国有](../Page/中国.md "wikilink")1属分布在西北。

本科[植物以前一直没有被大多数分类学家承认为一个](../Page/植物.md "wikilink")[科](../Page/科.md "wikilink")。只是1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其单独分为一个科](../Page/APG_分类法.md "wikilink")，放在[泽泻目下](../Page/泽泻目.md "wikilink")，此后到2016年的[APG
IV依然维持此分类](../Page/APG_IV.md "wikilink")。

## 系统发生学

本科属于泽泻目，

## 参考文献

## 外部链接

  - [APG II
    网站中的岩菖蒲科](http://www.mobot.org/MOBOT/Research/APweb/orders/alismatalesweb.htm#Tofieldiaceae)
  - [《北美植物》的岩菖蒲科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=133373)
  - [NCBI中的岩菖蒲科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=85243&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的岩菖蒲科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Tofieldiaceae)

[\*](../Category/岩菖蒲科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")