**峄城区**是[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[枣庄市所辖的一个](../Page/枣庄市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。总面积为627.6平方千米，2001年人口为36万。

## 行政区划

峄城区辖2个街道、5个镇：[坛山街道](../Page/坛山街道.md "wikilink")、[吴林街道](../Page/吴林街道.md "wikilink")、[榴园镇](../Page/榴园镇.md "wikilink")、[底阁镇](../Page/底阁镇.md "wikilink")、[阴平镇](../Page/阴平镇.md "wikilink")、[古邵镇](../Page/古邵镇.md "wikilink")、[峨山镇](../Page/峨山镇.md "wikilink")。

## 交通

  - [206国道](../Page/206国道.md "wikilink")
  - [104国道](../Page/104国道.md "wikilink")

## 风景名胜

  - [冠世榴园](../Page/冠世榴园.md "wikilink")
  - [万福园](../Page/万福园.md "wikilink")
  - [匡衡墓](../Page/匡衡墓.md "wikilink")

## 外部链接

  - [枣庄市峄城区商务网站](https://web.archive.org/web/20060212120109/http://yicheng.mofcom.gov.cn/)

[峄城区](../Category/峄城区.md "wikilink") [区](../Category/枣庄区市.md "wikilink")
[枣庄](../Category/山东市辖区.md "wikilink")