**峇里九人組**（Bali
Nine），指9名於2005年在[印尼](../Page/印尼.md "wikilink")[峇里島被捕的](../Page/峇里島.md "wikilink")[澳洲青年](../Page/澳大利亚公民.md "wikilink")，被控從印尼偷運8.3公斤（18磅）[海洛英到澳洲](../Page/海洛英.md "wikilink")，該批毒品市值約400萬[澳元](../Page/澳大利亚元.md "wikilink")（合310萬[美元](../Page/美元.md "wikilink")）。印尼當局透過澳洲當局提供之線報，把該九名青年拘捕，其中集團主腦[陳子维](../Page/陳子维.md "wikilink")（Andrew
Chan）及蘇庫馬朗（Myuran Sukumaran）兩人被判處死刑；其餘七人包括陳思毅（Si Yi Chen）、蘇蓋傑（Michael
Czugaj）、蕾妮勞倫絲（Renae Lawrence）、阮晉德青（Tan Duc Thanh Nguyen）、諾曼（Matthew
Norman）、拉斯（Scott Rush）及史提芬斯（Martin Stephens）各人均被判監。

2006年2月13日，首兩名受審的被告勞倫絲及拉斯被判終身監禁，至翌日的審訊中，被告蘇蓋傑及史提芬斯同被判囚終身，集團頭目陳子維及蘇庫馬朗則判處槍斃，也是登巴沙地區法院首次作出的死刑判決。最後三名被告諾曼、陳思毅及阮晉德青於2月15日被判終身監禁。判決結果透過澳洲的SKY
News直播。

至4月26日，五名被告上訴得直，獲減為20年有期徒刑，三名被告上訴遭駁回，包括兩名死刑犯，而最後一名被告選擇不上訴。

9月6日，當地最高法院發言人Djoko
Sarwoko稱，控方已提出上訴，並宣判加重部分被告刑期。案件相信已於8月完成審理，但峇里法院官員尚未得知結果。

至2008年3月6日，有消息指其中三人（諾曼、陳思毅及阮晉德青）獲得減刑，由死刑改判終身監禁，但減刑消息並未正式對外公佈，僅透過法庭內部證實法官決定減刑的消息。\[1\]

2015年4月29日，印尼當局處決了陳子维和蘇庫馬朗。\[2\]

## 被捕之前

警方不清楚兩個分別來自悉尼及布里斯本的集團如何聯繫，但能確認他們前往印尼前的行動。他們不少均受僱於聘用近9,000名員工的跨國膳食公司怡樂食（Eurest
Australia），被告諾曼、勞倫絲、史提芬斯及陳子维皆為怡樂食員工，為悉尼板球場提供到會服務，當中陳子維為公司主管之一。

拉斯和蘇蓋傑聲稱他們於布里斯本一間[卡拉OK聚會時](../Page/卡拉OK.md "wikilink")，為阮晉德青所招攬，警方懷擬阮扮演販毒計劃中的財政角色。拉斯稱半年前一次釣魚時認識阮，曾與阮一同前往悉尼出席一生日會，阮當時介紹拉斯予蘇庫馬朗認識。多日後拉斯與友人蘇蓋傑再度前往悉尼，組織每兩人一組前往印尼的計劃，但他們兩人未有啟程出發。

蕾妮曾前往峇里三次，分別為2004年10月16日、12月5日及翌年4月6日，諾曼的出發時間則為2004年12月5日、翌年1月19日及4月6日。蘇庫馬朗、陳子维及阮則各自兩次前往峇里兩次，其餘四人，陳思毅、史提芬斯、蘇蓋傑及拉斯則於2005年4月17日出發前往印尼，並於同日被捕。

## 印尼被捕

蕾妮、陳子维、蘇庫馬朗及阮於4月首先抵達印尼，並於Hard
Rock酒店下榻。警方得悉他們會長時間於房間逗留，至4月16日，即被捕前一天，他們四人再度會面，警方認為他們是就販毒計劃作最後一次簡介。

澳洲警方向印尼當局提供可疑人士資料，包括姓名、護照號碼及他們可能的毒品交易渠道。印尼警方已向該組織進行監視達一星期，直至他們被捕。警方相信，22歲[泰國女子Cherry](../Page/泰國.md "wikilink")
Likit
Bannakorn向陳子維提供毒品海洛英，並已於他們被捕後一天離開峇里，期間曾於泰[馬邊境被扣留](../Page/馬來西亞.md "wikilink")，其後被釋放。

## 判決

九名被告的判決結果如下：（中文譯名全為音譯）

<table style="width:77%;">
<colgroup>
<col style="width: 18%" />
<col style="width: 13%" />
<col style="width: 9%" />
<col style="width: 36%" />
</colgroup>
<thead>
<tr class="header">
<th><p>被告</p></th>
<th><p>來自</p></th>
<th><p>判決結果</p></th>
<th><p>詳情</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/陳子維.md" title="wikilink">陳子維</a><br />
(Andrew Chan)</p></td>
<td><p><a href="../Page/新南威爾士州.md" title="wikilink">新南威爾士州</a></p></td>
<td><p>死刑</p></td>
<td><p>2006年2月14日於丹帕沙地方法院被判處死刑<br />
2006年4月26日遭峇里高等法院駁回上訴，維持原判<br />
2010年8月13日向丹帕沙地方法院提出司法覆核<br />
2011年5月10日遭印尼最高法院駁回上訴，維持原判[3][4]<br />
2015年1月17日遭印尼總統維多多駁回特赦請求<br />
2015年2月4日遭丹帕沙地方法院駁回司法覆核<br />
2015年4月29日被槍決[5]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Si_Yi_Chen.md" title="wikilink">陳思毅</a><br />
(Si Yi Chen)</p></td>
<td><p><a href="../Page/悉尼.md" title="wikilink">悉尼</a></p></td>
<td><p>終身監禁</p></td>
<td><p>2月15日判處終身監禁<br />
上訴得直改判20年有期徒刑<br />
控方提出上訴，最高法院改判死刑<br />
辯方提出上訴，最高法院改判終身監禁</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:en:Michael_Czugaj.md" title="wikilink">蘇蓋傑</a><br />
(Michael Czugaj)</p></td>
<td><p><a href="../Page/昆士蘭州.md" title="wikilink">昆士蘭州</a></p></td>
<td><p>終身監禁</p></td>
<td><p>2月14日判處終身監禁<br />
上訴得直改判20年有期徒刑<br />
控方提出上訴，最高法院改判終身監禁</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Tan_Duc_Thanh_Nguyen.md" title="wikilink">阮晉德青</a><br />
(Tan Duc Thanh Nguyen)</p></td>
<td><p><a href="../Page/布里斯班.md" title="wikilink">布里斯班</a></p></td>
<td><p>終身監禁</p></td>
<td><p>2月15日判處終身監禁<br />
上訴得直改判20年有期徒刑<br />
控方提出上訴，最高法院改判死刑<br />
辯方提出上訴，最高法院改判終身監禁<br />
2018年5月19日因胃癌死于监狱[6]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:en:Matthew_Norman.md" title="wikilink">諾曼</a><br />
(Matthew Norman)</p></td>
<td><p><a href="../Page/悉尼.md" title="wikilink">悉尼</a></p></td>
<td><p>終身監禁</p></td>
<td><p>登帕沙區域法院於2006年2月15日判處被告終身監禁<br />
上訴得直，峇里高等法院於2006年9月6日改判被告20年有期徒刑<br />
控方提出上訴，最高法院於2006年9月6日改判被告死刑<br />
辯方提出上訴，最高法院於2008年3月6日改判被告終身監禁<br />
現於克羅伯坎監獄服刑</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Scott_Rush.md" title="wikilink">史葛·拉斯</a><br />
(Scott Rush)</p></td>
<td><p><a href="../Page/昆士蘭州.md" title="wikilink">昆士蘭州</a></p></td>
<td><p>18年</p></td>
<td><p>登帕沙區域法院於2006年2月13日判處被告終身監禁<br />
控方提出上訴，峇里高等法院於2006年9月6日改判被告死刑<br />
辯方提出上訴，最高法院於2011年5月10日改判被告終身監禁，峇里高等法院於同年7月15日把刑期減至18年<br />
現於克羅伯坎監獄服刑，預計2029年出獄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:en:Martin_Stephens.md" title="wikilink">馬丁·史提芬斯</a><br />
(Martin Stephens)</p></td>
<td><p><a href="../Page/新南威爾士州.md" title="wikilink">新南威爾士州</a></p></td>
<td><p>終身監禁</p></td>
<td><p>2月14日判處終身監禁<br />
上訴駁回維持原判，緩期執行</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Myuran_Sukumaran.md" title="wikilink">繆蘭·蘇庫馬朗</a><br />
(Myuran Sukumaran)</p></td>
<td><p><a href="../Page/新南威爾士州.md" title="wikilink">新南威爾士州</a></p></td>
<td><p>死刑</p></td>
<td><p>2月14日判處死刑</b r>上訴駁回維持原判<br />
2015年2月4日遭丹帕沙地方法院駁回司法覆核<br />
2015年4月29日遭槍決[7]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:en:Renae_Lawrence.md" title="wikilink">蕾妮·勞倫斯</a>（女）<br />
(Renae Lawrence)</p></td>
<td><p><a href="../Page/新南威爾士州.md" title="wikilink">新南威爾士州</a></p></td>
<td><p>20年</p></td>
<td><p>2月13日判處終身監禁<br />
上訴得直改判20年有期徒刑[8]<br />
2008年印尼獨立日，獲扣減刑期4個月<br />
現於克羅伯坎監獄服刑，預計2026年出獄</p></td>
</tr>
</tbody>
</table>

根據印尼1999年的總統法令174號第九條，如果終身監禁者於服刑期首五年內的表現良好，可申請減為最少15年的有期徒刑。

## 上訴

峇里九人組有多種途徑提出[上訴](../Page/上訴.md "wikilink")，辯方律師需於判決七日內提出。而犯人向印尼總統申請[特赦則不設時限](../Page/特赦.md "wikilink")，但需在犯人認罪以及並非干犯[販毒罪行的情況下方可獲考慮](../Page/販毒.md "wikilink")。除被告拉斯申請總統特赦外，其餘八名被告均就判決提出上訴，其中五人上訴得直獲減為20年有期徒刑。

## 其他

被告人Michael Czugaj的兄長Richard也於2007年3月24日干犯搶劫及傷人罪，判刑18個月。\[9\]

## 参考文献

[Category:犯罪組織](../Category/犯罪組織.md "wikilink")
[Category:澳洲罪案](../Category/澳洲罪案.md "wikilink")
[Category:印尼罪案](../Category/印尼罪案.md "wikilink")
[Category:巴厘省](../Category/巴厘省.md "wikilink")
[Category:海洛因](../Category/海洛因.md "wikilink")
[Category:名數9](../Category/名數9.md "wikilink")
[Category:人物合稱](../Category/人物合稱.md "wikilink")
[Category:澳大利亚罪犯](../Category/澳大利亚罪犯.md "wikilink")
[Category:毒贩](../Category/毒贩.md "wikilink")

1.

2.

3.

4.

5.
6.

7.
8.

9.  <http://www.news.com.au/heraldsun/story/0,21985,23820825-5005961,00.html>