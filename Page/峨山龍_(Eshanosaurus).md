**峨山龍**（[學名](../Page/學名.md "wikilink")：**）是[鐮刀龍超科](../Page/鐮刀龍超科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")。因為目前只發現一個部分下頜骨頭，所以對牠的研究甚少。牠們的[化石被發現於](../Page/化石.md "wikilink")[中國](../Page/中國.md "wikilink")，年代屬於[侏羅紀早期](../Page/侏羅紀.md "wikilink")。

## 發現與命名

在1971年，趙喜進在中國[雲南省](../Page/雲南省.md "wikilink")[峨山彝族自治县發現一些恐龍化石](../Page/峨山彝族自治县.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**出口峨山龍**（*E.
deguchiianus*），是由[徐星等人於](../Page/徐星_\(古生物學家\).md "wikilink")2001年描述、命名。[模式標本](../Page/模式標本.md "wikilink")（編號IVPP
V11579）是一個部分下頜骨頭與牙齒，發現於中國雲南省的[下祿豐組](../Page/下祿豐組.md "wikilink")，地質年代維侏儸紀早期的[赫塘階](../Page/赫塘階.md "wikilink")，約1億9600萬年前。這個標本目前存放於[北京](../Page/北京.md "wikilink")[中國科學院](../Page/中國科學院.md "wikilink")[古脊椎動物與古人類研究所](../Page/古脊椎動物與古人類研究所.md "wikilink")\[1\]。

## 分類

徐星等人根據數個下頜與牙齒的[共有衍徵](../Page/共有衍徵.md "wikilink")，將峨山龍分類在[鐮刀龍超科之下](../Page/鐮刀龍超科.md "wikilink")。這使得峨山龍成為目前已知最早的[虛骨龍類](../Page/虛骨龍類.md "wikilink")，並且是年代早於[始祖鳥的衍化](../Page/始祖鳥.md "wikilink")[手盜龍類](../Page/手盜龍類.md "wikilink")，比已知最早的鐮刀龍類[北票龍還早](../Page/北票龍.md "wikilink")6,000萬年，例如[鑄鐮龍](../Page/鑄鐮龍.md "wikilink")、[北票龍](../Page/北票龍.md "wikilink")。

根據徐星等人的未公佈[親緣分支分類法分析](../Page/親緣分支分類法.md "wikilink")，他們認為峨山龍是種鐮刀龍類恐龍\[2\]。如果峨山龍屬於鐮刀龍類，將造成虛骨龍類的化石斷層，所以有許多科學家質疑峨山龍的歸類。在2001年，[詹姆士·柯克蘭](../Page/詹姆士·柯克蘭.md "wikilink")（James
Kirkland）與D.G.
Wolfe敘述、命名[懶爪龍時](../Page/懶爪龍.md "wikilink")，曾根據牙齒形狀而認為峨山龍應屬於[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，或是[蜥腳形亞目的基礎物種](../Page/蜥腳形亞目.md "wikilink")\[3\]。徐星等人認為這是因為牠們的化石被發現於眾多的[祿豐龍化石之下](../Page/祿豐龍.md "wikilink")，祿豐龍屬於原蜥腳下目，原蜥腳下目的化石非常類似鐮刀龍類。因此，徐星等人仍認為峨山龍是種侏儸紀早期的鐮刀龍類，提出牠們與鐮刀龍類之間有數個共有特徵，因此不認為牠們屬於原蜥腳類恐龍\[4\]。

在2009年，[保羅·巴雷特](../Page/保羅·巴雷特.md "wikilink")（Paul M.
Barrett）重新研究峨山龍的模式標本，排除峨山龍屬於[原蜥腳下目的可能性](../Page/原蜥腳下目.md "wikilink")。巴雷特的結論是峨山龍有六個鐮刀龍類的特徵，屬於鐮刀龍類，與徐星等人的最初研究相同。峨山龍的歸類對於恐龍的演化有者重大影響，使虛骨龍類的化石紀錄中斷了數千萬年\[5\]。

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - <https://web.archive.org/web/20090727131406/http://www.thescelosaurus.com/dinosauromorpha.htm>
  - <https://web.archive.org/web/20070715004249/http://www.dinosauria.com/dml/names/dinoe.htm>

[Category:鐮刀龍超科](../Category/鐮刀龍超科.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")

1.  Xu, X., Zhao, X. and Clark, J.M. (2001). "A new therizinosaur from
    the Lower Jurassic Lower Lufeng Formation of Yunnan, China."
    *Journal of Vertebrate Paleontology*, **21**(3): 477–483.

2.
3.  Kirkland, J.I. and Wolfe, D.G. (2001). "First definitive
    therizinosaurid (Dinosauria: Theropoda) from North America."
    *Journal of Vertebrate Paleontology*, **21**(3): 410-414.

4.
5.  Barrett, P.M. (2009). "The affinities of the enigmatic dinosaur
    *Eshanosaurus deguchiianus* from the Early Jurassic of Yunnan
    Province, People's Republic of China." *Palaeontology*, **52**(4):
    681-688. doi: 10.1111/j.1475-4983.2009.00887.x.