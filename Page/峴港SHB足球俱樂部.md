**峴港SHB足球俱樂部**（）
是[越南足球俱乐部](../Page/越南.md "wikilink")，位于[岘港](../Page/岘港.md "wikilink")。俱乐部创建于1976年，属于越南足球联赛中的顶级联赛球队，球队主体育场为支朗体育场（San
Chi Lang）。

俱乐部在2006赛季最近一次闯入过[亚洲冠军联赛](../Page/亚洲冠军联赛.md "wikilink")，尽管表现只能称得上惨败（包括平记录的0-15惨败给日本联赛球队[大阪飞脚](../Page/大阪飞脚.md "wikilink")），最终在和[全北现代](../Page/全北现代.md "wikilink")、[大阪飞脚](../Page/大阪飞脚.md "wikilink")、[大连实德小组赛中](../Page/大连实德.md "wikilink")6战全败垫底被淘汰。

[Da_Nang.gif](https://zh.wikipedia.org/wiki/File:Da_Nang.gif "fig:Da_Nang.gif")

## 球隊榮譽

  - **[越南足球聯賽](../Page/越南足球聯賽.md "wikilink")**: 2 (1992, 2009)
  - **[越南盃](../Page/越南盃.md "wikilink")**: 2 (1993, 2009)

## 亞足協賽事成績

  - **[亞足聯冠軍聯賽](../Page/亞足聯冠軍聯賽.md "wikilink")**: 2次

<!-- end list -->

  -

      -
        [2006](../Page/2006年亞足聯冠軍聯賽.md "wikilink"): 小組賽
        [2010](../Page/2010年亞足聯冠軍聯賽.md "wikilink"): *外圍賽出局*

<!-- end list -->

  - **[亞洲球會錦標賽](../Page/亞足聯冠軍聯賽.md "wikilink")**: 1次

<!-- end list -->

  -

      -
        [1994](../Page/:en:Asian_Club_Championship_1994.md "wikilink"):
        *外圍賽出局*

<!-- end list -->

  - **[亞洲盃賽冠軍盃](../Page/亞洲盃賽冠軍盃.md "wikilink")**: 1次

<!-- end list -->

  -

      -
        [1994/95](../Page/:en:Asian_Cup_Winners_Cup_1995.md "wikilink"):
        第二輪
        [1992/93](../Page/:en:Asian_Cup_Winners_Cup_1993.md "wikilink"):
        半決賽

<!-- end list -->

  - **[亞洲足協盃](../Page/亞洲足協盃.md "wikilink")**: 1次

<!-- end list -->

  -

      -
        [2010](../Page/2010年亞洲足協盃.md "wikilink"): 半準決賽

## 球員名單

<table style="width:24%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 1%" />
<col style="width: 6%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="#FFFF00">號碼</p></th>
<th><p><font color="#FFFF00">國籍</p></th>
<th><p><font color="#FFFF00">球員名字</p></th>
<th><p><font color="#FFFF00">出生日期</p></th>
<th><p><font color="#FFFF00">加盟年份</p></th>
<th><p><font color="#FFFF00">前屬球會</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><font color="#FFFF00">守門員</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><a href="../Page/范文凱.md" title="wikilink">范文凱</a>（Phạm Văn Khải）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>13</strong></p></td>
<td></td>
<td><p><a href="../Page/阮清平.md" title="wikilink">阮清平</a>（Nguyễn Thanh Bình）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>25</strong></p></td>
<td></td>
<td><p><a href="../Page/黎文興（足球員）.md" title="wikilink">黎文興</a>（Lê Văn Hưng）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><font color="#FFFF00">後衛</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2</strong></p></td>
<td></td>
<td><p><a href="../Page/高強（足球員）.md" title="wikilink">高強</a>（Cao Cường）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p><a href="../Page/武輝全.md" title="wikilink">武輝全</a>（Võ Huy Toàn）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/武黃廣.md" title="wikilink">武黃廣</a>（Võ Hoàng Quãng）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><a href="../Page/周黎福永.md" title="wikilink">周黎福永</a>（Châu Lê Phước Vĩnh）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>15</strong></p></td>
<td></td>
<td><p><a href="../Page/陳海林.md" title="wikilink">陳海林</a>（Trần Hải Lâm）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><a href="../Page/華倫迪治.md" title="wikilink">華倫迪治</a>（Adrian Valentić）</p></td>
<td><p>1987年8月17日</p></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/:en:NK_Rudeš.md" title="wikilink">NK Rudeš</a></p></td>
</tr>
<tr class="even">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/潘維嵐.md" title="wikilink">潘維嵐</a>（Phan Duy Lam）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>26</strong></p></td>
<td></td>
<td><p><a href="../Page/阮晉田.md" title="wikilink">阮晉田</a>（Nguyễn Tấn Điền）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><font color="#FFFF00">中場</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>4</strong></p></td>
<td></td>
<td><p><a href="../Page/陳英科.md" title="wikilink">陳英科</a>（Trần Anh Khoa）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><a href="../Page/阮明方.md" title="wikilink">阮明方</a>（Nguyễn Minh Phương）</p></td>
<td><p>1980年7月5日</p></td>
<td><p>2010年</p></td>
<td><p><a href="../Page/隆安同心足球俱樂部.md" title="wikilink">隆安同心</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>11</strong></p></td>
<td></td>
<td><p><a href="../Page/江陳郭新.md" title="wikilink">江陳郭新</a>（Giang Trần Quách Tân）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><a href="../Page/黃明心.md" title="wikilink">黃明心</a>（Hoàng Minh Tâm）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/范元沙.md" title="wikilink">范元沙</a>（Phạm Nguyên Sa）</p></td>
<td><p>1989年12月15日</p></td>
<td><p>2009年</p></td>
<td><p>青年軍</p></td>
</tr>
<tr class="even">
<td><p><strong>17</strong></p></td>
<td></td>
<td><p><a href="../Page/阮武峰.md" title="wikilink">阮武峰</a>（Nguyễn Vũ Phong）</p></td>
<td><p>1985年2月6日</p></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/平陽足球俱樂部.md" title="wikilink">平陽</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>19</strong></p></td>
<td></td>
<td><p><a href="../Page/潘清興.md" title="wikilink">潘清興</a>（Phan Thanh Hưng）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>22</strong></p></td>
<td></td>
<td><p><a href="../Page/段雄山.md" title="wikilink">段雄山</a>（Đoàn Hùng Sơn）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p><a href="../Page/阮清創.md" title="wikilink">阮清創</a>（Nguyễn Thanh Sang）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>32</strong></p></td>
<td></td>
<td><p><a href="../Page/黃國英.md" title="wikilink">黃國英</a>（Huỳnh Quốc Anh）</p></td>
<td><p>1985年1月13日</p></td>
<td><p>2005年</p></td>
<td><p>青年軍</p></td>
</tr>
<tr class="odd">
<td><p><font color="#FFFF00">前鋒</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>8</strong></p></td>
<td></td>
<td><p><a href="../Page/潘德禮.md" title="wikilink">潘德禮</a>（Phan Đức Lễ）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>9</strong></p></td>
<td></td>
<td><p><a href="../Page/阮文盛.md" title="wikilink">阮文盛</a>（Nguyễn Văn Thạnh）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><a href="../Page/何明俊.md" title="wikilink">何明俊</a>（Hà Minh Tuấn）</p></td>
<td><p>1991年1月1日</p></td>
<td><p>2010年</p></td>
<td><p>青年軍</p></td>
</tr>
<tr class="odd">
<td><p><strong>24</strong></p></td>
<td></td>
<td><p><a href="../Page/馬迪亞斯.md" title="wikilink">馬迪亞斯</a>（Matias Jose Zbrun）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>34</strong></p></td>
<td></td>
<td><p><a href="../Page/馬里安奴.md" title="wikilink">馬里安奴</a>（Jhonatan Mariano Bernardo）</p></td>
<td><p>1988年11月7日</p></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/普雷紹夫足球俱樂部.md" title="wikilink">普雷紹夫</a></p></td>
</tr>
</tbody>
</table>

## 外部链接

  - [Da Nang page - AFC Champions League
    site](http://www.afcchampionsleague.com/en/tournament/teams.asp?cid=1309&sqid=1362&tbl=Y)
  - [Da Nang page -
    Vietnamnet.com](https://web.archive.org/web/20070606032118/http://thethao.vietnamnet.vn/bongda/clb_vn/2004/10/334662/)

[Category:越南足球俱乐部](../Category/越南足球俱乐部.md "wikilink")