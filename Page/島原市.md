**島原市**（）為[日本](../Page/日本.md "wikilink")[長崎縣東南部](../Page/長崎縣.md "wikilink")[島原半島上的](../Page/島原半島.md "wikilink")[城市](../Page/城市.md "wikilink")，為島原半島的主要都市。

## 地理

位于[島原半島東端](../Page/島原半島.md "wikilink")，西側為雲仙市，南側為南島原市。轄區西部有海拔1,483公尺的[雲仙岳平成新山](../Page/雲仙岳.md "wikilink")，東側面向[有明海](../Page/有明海.md "wikilink")。人口主要集中在東部海岸。

  - 河川：水無川、中尾川
  - 山：[雲仙岳](../Page/雲仙岳.md "wikilink")（眉山・平成新山）
  - 湖沼：白土湖

### 相鄰的行政區劃

  - [雲仙市](../Page/雲仙市.md "wikilink")
  - [南島原市](../Page/南島原市.md "wikilink")

## 歷史

[Shimabara_Castle_Tower_20090906.jpg](https://zh.wikipedia.org/wiki/File:Shimabara_Castle_Tower_20090906.jpg "fig:Shimabara_Castle_Tower_20090906.jpg")\]\]
[江戶時代初期的](../Page/江戶時代.md "wikilink")1618年，[松倉重政花了七年的時間建築了](../Page/松倉重政.md "wikilink")[島原城](../Page/島原城.md "wikilink")，此後便以[島原藩的](../Page/島原藩.md "wikilink")[城下町形式開始發展](../Page/城下町.md "wikilink")\[1\]；但在1637年也因為第二代領主[松倉勝家對待人民過於殘暴](../Page/松倉勝家.md "wikilink")，引發[島原之亂](../Page/島原之亂.md "wikilink")，造成此地區嚴重的破壞，甚至大量的居民死亡。直到繼任的領主[高力忠房推動移民政策之後才開始恢復](../Page/高力忠房.md "wikilink")。

[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，伊度成為[島原縣的](../Page/島原縣.md "wikilink")[縣廳所在地](../Page/縣廳所在地.md "wikilink")，直到島原縣被併入[長崎縣](../Page/長崎縣.md "wikilink")。

位於島原半島中央的[雲仙普賢岳為著名的](../Page/雲仙普賢岳.md "wikilink")[活火山](../Page/活火山.md "wikilink")，曾在1792年爆發產山崩，土石落入海中後引起[海嘯](../Page/海嘯.md "wikilink")，海嘯在[有明海沿岸的](../Page/有明海.md "wikilink")[肥後國](../Page/肥後國.md "wikilink")、[天草群島](../Page/天草群島.md "wikilink")、島原半島造成共約一萬五千人的重大傷亡，為日本歷史最重大的火山災害。

近年在1990年11月17日也一度爆發，並在隔年的6月3日產生[火山碎屑流](../Page/火山碎屑流.md "wikilink")，造成43人死亡，其中包括前來進行研究的火山學者。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[南高來郡島原町](../Page/南高來郡.md "wikilink")、島原村、湊町、安中村、杉谷村、三會村、大三東村、湯江村。
  - 1924年4月1日：島原町、[島原村](../Page/島原村.md "wikilink")、[湊町](../Page/湊町_\(長崎縣\).md "wikilink")[合併為新設置的](../Page/市町村合併.md "wikilink")[島原町](../Page/島原町.md "wikilink")。
  - 1940年4月1日：**島原町**、[安中村](../Page/安中村.md "wikilink")、[杉谷村合併為](../Page/杉谷村.md "wikilink")**島原市**。
  - 1955年4月1日：
      - [三會村被併入島原市](../Page/三會村.md "wikilink")。
      - [大三東村](../Page/大三東村.md "wikilink")、[湯江村合併為有明村](../Page/湯江村.md "wikilink")。
  - 1961年11月3日：有明村改制為[有明町](../Page/有明町_\(長崎縣\).md "wikilink")。
  - 2006年1月1日：有明町被併入島原市。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1959年</p></th>
<th><p>1960年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>島原町</p></td>
<td><p>1924年4月1日<br />
島原町</p></td>
<td><p>1940年4月1日<br />
島原市</p></td>
<td><p>島原市</p></td>
<td><p>島原市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>湊町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>島原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>杉谷村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>安中村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>三會村</p></td>
<td><p>1955年4月1日<br />
併入島原市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大三東村</p></td>
<td><p>1955年4月1日<br />
有明村</p></td>
<td><p>1961年11月3日<br />
有明町</p></td>
<td><p>2006年1月1日<br />
併入島原市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>湯江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [島原鐵道](../Page/島原鐵道.md "wikilink")
      - [島原鐵道線](../Page/島原鐵道線.md "wikilink")：[島鐵湯江車站](../Page/島鐵湯江車站.md "wikilink")
        - [大三東車站](../Page/大三東車站.md "wikilink") -
        [松尾町車站](../Page/松尾町車站.md "wikilink") -
        [三會車站](../Page/三會車站.md "wikilink") -
        [島原車站](../Page/島原車站.md "wikilink") -
        [島鐵本社前車站](../Page/島鐵本社前車站.md "wikilink") -
        [南島原車站](../Page/南島原車站.md "wikilink") -
        [島原外港車站](../Page/島原外港車站.md "wikilink")

島原鐵道線原還有繼續往南延伸至[南島原市](../Page/南島原市.md "wikilink")，但已於2008年4月1日停駛；停駛路段位於轄區內的車站則包括[秩父浦車站與](../Page/秩父浦車站.md "wikilink")[安德車站](../Page/安德車站.md "wikilink")。

### 渡船

[島原港亦有渡輪航班可往來下列港口](../Page/島原港.md "wikilink")。

  - [熊本港](../Page/熊本港.md "wikilink")（[熊本縣](../Page/熊本縣.md "wikilink")[熊本市](../Page/熊本市.md "wikilink")）
  - [三池港](../Page/三池港.md "wikilink")（[福岡縣](../Page/福岡縣.md "wikilink")[大牟田市](../Page/大牟田市.md "wikilink")）

## 觀光景點

[140321_The_old_samurai_residence_town_Shimabara_Nagasaki_pref_Japan00s3.jpg](https://zh.wikipedia.org/wiki/File:140321_The_old_samurai_residence_town_Shimabara_Nagasaki_pref_Japan00s3.jpg "fig:140321_The_old_samurai_residence_town_Shimabara_Nagasaki_pref_Japan00s3.jpg")

  - [島原城](../Page/島原城.md "wikilink")
  - [武家屋敷](../Page/武家屋敷.md "wikilink")
  - 島原湧水群：[名水百選之一](../Page/名水百選.md "wikilink")
  - [雲仙岳災害記念館](../Page/雲仙岳災害記念館.md "wikilink")
  - [九十九島](../Page/九十九島_\(島原市\).md "wikilink")
  - [島原溫泉](../Page/島原溫泉.md "wikilink")

## 教育

### 高等學校

  - [長崎縣立島原高等學校](../Page/長崎縣立島原高等學校.md "wikilink")
  - [長崎縣立島原農業高等學校](../Page/長崎縣立島原農業高等學校.md "wikilink")
  - [長崎縣立島原工業高等學校](../Page/長崎縣立島原工業高等學校.md "wikilink")
  - [長崎縣立島原商業高等學校](../Page/長崎縣立島原商業高等學校.md "wikilink")
  - [島原中央高等學校](../Page/島原中央高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [豐後高田市](../Page/豐後高田市.md "wikilink")（[大分縣](../Page/大分縣.md "wikilink")）：於1969年4月25日締結為兄弟都市。
  - [福知山市](../Page/福知山市.md "wikilink")（[京都府](../Page/京都府.md "wikilink")）：於
    1983年3月1日締結為姊妹都市。

## 本地出身之名人

  - [柴田保光](../Page/柴田保光.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [仲町貞子](../Page/仲町貞子.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [中村孝明](../Page/中村孝明.md "wikilink")：廚師
  - [百武裕司](../Page/百武裕司.md "wikilink")：業餘天文愛好者，因於1996年發現著名的[百武二號彗星而成名](../Page/百武二號彗星.md "wikilink")。
  - [丸尾末廣](../Page/丸尾末廣.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [森崎東](../Page/森崎東.md "wikilink")：[電影導演](../Page/電影導演.md "wikilink")
  - [宮崎香蓮](../Page/宮崎香蓮.md "wikilink")：藝人
  - [鐘江管一](../Page/鐘江管一.md "wikilink")：前島原市長，任內發生雲仙普賢岳[火山碎屑流災害](../Page/火山碎屑流.md "wikilink")，致力於島原市之重建。
  - [村田重治](../Page/村田重治.md "wikilink")：[日本海軍軍人](../Page/日本海軍.md "wikilink")

## 參考資料

## 外部連結

  - [島原地域合併協議會](https://web.archive.org/web/20081215105809/http://www.city.shimabara.lg.jp/gappei/kyogikai/index.htm)

  - [島原溫泉觀光協會](http://www.shimabaraonsen.com/)

  - [島原商工會議所](http://www.shimabara-cci.or.jp/)

  - [島原雲仙農業協同組合](http://www.ja-shimabaraunzen.or.jp/)

  - [島原是商店街聯盟](http://www.motenasu.com/)

<!-- end list -->

1.