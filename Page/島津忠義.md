**島津忠義**（）。[薩摩藩第](../Page/薩摩藩.md "wikilink")12代（最後）藩主、[島津氏第](../Page/島津氏.md "wikilink")29代當主。為[薩摩藩第](../Page/薩摩藩.md "wikilink")11代藩主[島津齊彬其弟](../Page/島津齊彬.md "wikilink")[島津久光長男](../Page/島津久光.md "wikilink")。官位[從一位](../Page/從一位.md "wikilink")、大隅守。勳等勳一等。

幼名**壯之助**，通稱**又次郎**。初名**忠德**，後來與將軍[德川家茂偏諱而賜名為](../Page/德川家茂.md "wikilink")**茂久**，在維新後的[慶應](../Page/慶應.md "wikilink")4年1月16日改名為「**忠義**」。

## 簡歷

為伯父[島津齊彬的養子](../Page/島津齊彬.md "wikilink")，1858年[齊彬去世後成為藩主](../Page/島津齊彬.md "wikilink")。不過當時藩政實權由祖父[齊興](../Page/島津齊興.md "wikilink")、生父[久光](../Page/島津久光.md "wikilink")、重臣[西鄉隆盛以及](../Page/西鄉隆盛.md "wikilink")[大久保利通所掌握](../Page/大久保利通.md "wikilink")，忠義本身毫無權力可言。

1858年2月7日，在[江戶城謁見](../Page/江戶城.md "wikilink")[德川家茂](../Page/德川家茂.md "wikilink")，與家茂偏諱而改名茂久。1859年2月，總稱[從四位下](../Page/從四位.md "wikilink")[左近衛少將叙任](../Page/左近衛少將.md "wikilink")[修理大夫](../Page/修理大夫.md "wikilink")。

明治維新後與[長州藩](../Page/長州藩.md "wikilink")、[土佐藩](../Page/土佐藩.md "wikilink")、[佐賀藩一同推行](../Page/佐賀藩.md "wikilink")[版籍奉還](../Page/版籍奉還.md "wikilink")。雖然名義上是薩摩藩知事，不過實際上的藩務是由[西鄉隆盛管理](../Page/西鄉隆盛.md "wikilink")。

1871年[廢藩置縣後被封為](../Page/廢藩置縣.md "wikilink")[公爵](../Page/公爵.md "wikilink")。此後在政府的命令下居住於[東京](../Page/東京.md "wikilink")，1888年時得到政府許可後返鄉回到[鹿兒島](../Page/鹿兒島.md "wikilink")。

忠義相當擅長[犬追物與](../Page/犬追物.md "wikilink")[馬術](../Page/馬術.md "wikilink")，對攝影與[煙火製作也很有興趣](../Page/煙火.md "wikilink")，擁有許多嗜好的人。1889年2月11日大日本帝國憲法公布當日，[德國](../Page/德國.md "wikilink")[醫學家](../Page/醫學家.md "wikilink")[埃爾溫·貝爾茲的日記裡](../Page/埃爾溫·貝爾茲.md "wikilink")，記載著他對忠義穿著洋服及留鬍子的樣子十分驚訝。從這一點可以看得出忠義對於西洋文化的造詣頗深。

1897年12月26日，以58歳之齡在[鹿兒島市逝世](../Page/鹿兒島市.md "wikilink")。死後追授[勳一等旭日桐花大綬章](../Page/勳一等旭日桐花大綬章.md "wikilink")，翌年1月9日舉行國葬。

現在忠義的墓碑仍保存於[尚古集成館內](../Page/尚古集成館.md "wikilink")，探勝園有忠義的銅像，墓地則是在[島津氏歷代的常安御墓](../Page/島津氏.md "wikilink")。

## 家系

  - 父：[島津久光](../Page/島津久光.md "wikilink")
  - 母：島津千百子（[重富島津家當主](../Page/重富島津家.md "wikilink")・[島津忠公長女](../Page/島津忠公.md "wikilink")）
  - 養父：[島津齊彬](../Page/島津齊彬.md "wikilink")
  - 正室：暐子（1851年 - 1869年，[島津齊彬三女](../Page/島津齊彬.md "wikilink")）
      - 女:房子（1869年 - 1871年）
  - 繼室：寧子（1853年 -
    1879年，[島津齊彬五女](../Page/島津齊彬.md "wikilink")、[近衛忠熙養女](../Page/近衛忠熙.md "wikilink")）
      - 男：忠寶（1879年生，誕生後3個月夭折）
  - 繼室：棲子（- 1886年，[板倉勝達次女](../Page/板倉勝達.md "wikilink")）
  - 側室：山崎壽滿子（1850年 - 1927年）
      - 長女・清子（1871年 - 1919年，[黑田長成室](../Page/黑田長成.md "wikilink")）
      - 次女・充子（1873年 - 1958年，[池田詮政室](../Page/池田詮政室.md "wikilink") →
        [松平直亮室](../Page/松平直亮.md "wikilink")）
      - 三女・[常子](../Page/菊麿王妃常子.md "wikilink")（1874年 -
        1938年，[山階宮菊麿王妃](../Page/山階宮菊麿王.md "wikilink")）
      - 四女・知子（1875年 - 1953年，[德川達孝室](../Page/德川達孝.md "wikilink")）
      - 五女・貞子（1878年 - 1974年，[久松定謨室](../Page/久松定謨.md "wikilink")）
      - 七女・[俔子](../Page/邦彥王妃俔子.md "wikilink")（1879年 -
        1956年，[久邇宮邦彦王妃](../Page/久邇宮邦彦王.md "wikilink")、[香淳皇后母](../Page/香淳皇后.md "wikilink")）
      - 九女・正子（1885年 - 1963年、[德川家正室](../Page/德川家正.md "wikilink")）
      - 長男・忠重（1886年 - 1968年）

<!-- end list -->

  - 側室：菱刈久（ - 1960年）
      - 次男・忠備（1891年 - 1928年、分家[男爵](../Page/男爵.md "wikilink")）
      - 三男・忠弘（1892年 - 1922年、分家[男爵](../Page/男爵.md "wikilink")）
      - 四男・久範（1894年 -
        1944年、[島津忠麿](../Page/島津忠麿.md "wikilink")（舊[佐土原藩主家](../Page/佐土原藩.md "wikilink")）養子、[伯爵](../Page/伯爵.md "wikilink")）
      - 五男・康久（1895年 - 1972年）
      - 十女・為子（1897年 - 1989年、[德川賴貞室](../Page/德川賴貞.md "wikilink")）

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**島津忠義** |-    |-

[Category:貴族院公爵議員](../Category/貴族院公爵議員.md "wikilink")
[Category:奧州島津氏](../Category/奧州島津氏.md "wikilink")
[Tadayoshi](../Category/重富島津氏.md "wikilink")
[Category:薩摩國出身人物](../Category/薩摩國出身人物.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")
[Category:薩摩藩](../Category/薩摩藩.md "wikilink")
[Category:幕末大名](../Category/幕末大名.md "wikilink")
[Category:黃綬褒章獲得者](../Category/黃綬褒章獲得者.md "wikilink")