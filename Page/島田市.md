**島田市**（）為[靜岡縣中部的市](../Page/靜岡縣.md "wikilink")。位于大井川沿岸，過去是[志太郡及](../Page/志太郡.md "wikilink")[榛原郡](../Page/榛原郡.md "wikilink")。在過去是東海道島田宿的宿場。

## 地理

  - 位于靜岡縣中部的[志太平原](../Page/志太平原.md "wikilink")。
  - [河](../Page/河.md "wikilink")：[大井川](../Page/大井川.md "wikilink")、大津谷川、伊太谷川、伊久美川、湯日川、大代川、[栃山川](../Page/栃山川.md "wikilink")。

## 行政

  - 市長：櫻井勝郎（2005年5月～ 1任）

## 外部連結

  - [島田市](http://www.city.shimada.shizuoka.jp/)

  -