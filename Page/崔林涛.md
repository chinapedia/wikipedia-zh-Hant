**崔林涛**（），[汉族](../Page/汉族.md "wikilink")，[陕西](../Page/陕西.md "wikilink")[千阳人](../Page/千阳.md "wikilink")；[中共](../Page/中共.md "wikilink")[陕西省委党校大专学历](../Page/陕西.md "wikilink")。1966年4月加入[中国共产党](../Page/中国共产党.md "wikilink")，是[中共第十五届中央候补委员](../Page/中共.md "wikilink")。曾任[中共陕西省委副书记](../Page/中共陕西省委.md "wikilink")、[西安](../Page/西安.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")，[陕西省](../Page/陕西省.md "wikilink")[人大常委会副主任](../Page/人大常委会.md "wikilink")、代主任等职。

## 经历

作为陕西人，崔林涛也一直工作在[陕西](../Page/陕西.md "wikilink")。他曾先后在第三机械工业部秦岭公司（即后来的，航空部秦岭公司），以及[陕西省国防科学技术工业办公室任职](../Page/陕西省.md "wikilink")。1988年8月，转任地方，出任[中共](../Page/中共.md "wikilink")[西安市委副书记](../Page/西安.md "wikilink")；1990年8月起，代理[西安市](../Page/西安市.md "wikilink")[市长](../Page/市长.md "wikilink")；次年4月，正式当选市长；1995年1月，晋升[中共陕西省委常委](../Page/中共陕西省委.md "wikilink")、[西安](../Page/西安.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")，并兼任[西安市](../Page/西安市.md "wikilink")[人大常委会主任](../Page/人大常委会.md "wikilink")；2002年1月，转任[陕西省](../Page/陕西省.md "wikilink")[人大常委会副主任](../Page/人大常委会.md "wikilink")、党组副书记。
崔林涛在西安工作多年，但热衷形象工程，曾提出著名的八大工程。务实作风稍欠。

2007年7月，在[李建国被宣布调](../Page/李建国.md "wikilink")[山东任职之后](../Page/山东.md "wikilink")，[陕西省](../Page/陕西省.md "wikilink")[人大常委会主任的位置出缺](../Page/人大常委会.md "wikilink")；年逾[花甲](../Page/花甲.md "wikilink")，已到正部级官员退休年龄的崔林涛，被推举为代理主任；直到2008年1月任期结束。

## 参考文献

  - [崔林涛简历](http://news.xinhuanet.com/ziliao/2007-07/30/content_6449996.htm)
    新华网

[Category:中华人民共和国西安市市长](../Category/中华人民共和国西安市市长.md "wikilink")
[C崔](../Category/中国共产党第十五届中央委员会候补委员.md "wikilink")
[C崔](../Category/宝鸡人.md "wikilink")
[L林](../Category/崔姓.md "wikilink")
[Category:中共西安市委书记](../Category/中共西安市委书记.md "wikilink")
[Category:陕西省人大常委会主任](../Category/陕西省人大常委会主任.md "wikilink")
[Category:中共陕西省委常委](../Category/中共陕西省委常委.md "wikilink")
[Category:陕西省人大常委会副主任](../Category/陕西省人大常委会副主任.md "wikilink")