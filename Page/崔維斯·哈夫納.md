**崔維斯·李·哈夫納**（**Travis Lee
Hafner**、）是[美國職棒大聯盟球員](../Page/美國職棒大聯盟.md "wikilink")，目前效力[紐約洋基](../Page/紐約洋基.md "wikilink")。綽號是**Pronk**，這個綽號是2001年春季集訓期間被前隊友[Bill
Selby所起的名字](../Page/Bill_Selby.md "wikilink")。有時亦會被稱為**Donkey**，當他上壘的時候就會被球迷稱呼此稱號\[1\]。

## 球員生涯

### 德州遊騎兵

哈夫納在1996年的選秀會中被[德州遊騎兵在第](../Page/德州遊騎兵.md "wikilink")31輪選中，2002年8月6日首次在大聯盟比賽亮相。2002年冬季遊騎兵隊將哈夫納和[Aaron
Myette換來了](../Page/Aaron_Myette.md "wikilink")[捕手印地安人隊](../Page/捕手.md "wikilink")[Einar
Diaz及投手](../Page/Einar_Diaz.md "wikilink")[萊恩·德瑞斯](../Page/萊恩·德瑞斯.md "wikilink")。

### 克里夫蘭印地安人

2003年哈夫納在印地安人隊以[一壘手及](../Page/一壘手.md "wikilink")[指定打擊登場](../Page/指定打擊.md "wikilink")。8月14日，哈夫納完成了[完全打擊的創舉](../Page/完全打擊.md "wikilink")，自1978年的[Andre
Thornton以來第二位造出完全安打的印地安人球員](../Page/Andre_Thornton.md "wikilink")\[2\]。

2004年球季哈夫納成為了印地安人隊指定打擊的球員。哈夫納在以下數據排在聯盟十名之內，[上壘率](../Page/上壘率.md "wikilink")（.410、第三）、[長打率](../Page/長打率.md "wikilink")（.583、第四）、[二壘安打](../Page/二壘安打.md "wikilink")（41、第六）、[長打](../Page/長打_\(棒球\).md "wikilink")（72、第7）、打點（109、第9）、打擊率（.311、第10）。此外他亦打出28支全壘打（[美國聯盟第](../Page/美國聯盟.md "wikilink")16）及取得96分（美聯第20）。除了8月只有.274以外，每個月平均打擊率均超過.300。7月造出.360打擊率、8支全壘打及28個打點。4月12日對[明尼蘇達雙城的比賽中](../Page/明尼蘇達雙城.md "wikilink")，打出生涯第一支[滿壘全壘打](../Page/滿壘全壘打.md "wikilink")\[3\]。

2005年初，哈夫納獲球隊簽署一份為期三年的合約，另外球隊有一年選擇權在2008年是否讓他留隊\[4\]。他的進攻能力仍在這個球季在數據上保持前列。上壘率（.408、第3）、長打率（.595、第3）、二壘安打（42、第5）、保送（79、第7）、長打（75、第8）、打擊率（.305、第9）、全壘打（33、第9）及打點（108、第9），此外亦為球隊取得94分。6月，他當選為美國聯盟[每月最佳球員](../Page/每月最佳球員.md "wikilink")，在24場比賽當中，打擊率是.345、10支二壘打及8支全壘打以及29個打點。\[5\]。在7月第一個星期的打擊率是.480，4支全壘打及12個打點，獲選為每週最佳球員\[6\]。7月16日，被[芝加哥白襪隊的](../Page/芝加哥白襪.md "wikilink")[馬克·柏利所投出的球擊中臉部](../Page/馬克·柏利.md "wikilink")，7月26日被登錄在15天傷兵名單之上\[7\]。8月4日重返球場上陣。在他剩下來的54場比賽當中，取得15支全壘打、45個打點、打擊率為.296。9月18日至24日，哈夫納在6場比賽中，每場均取得1支全壘打，這個連續記錄是印地安人隊第二最長記錄\[8\]。球季結束後，[全美棒球記者協會](../Page/全美棒球記者協會.md "wikilink")（BBWAA）當選他為印地安人隊最有價值球員\[9\]。另外，在美國聯盟[最有價值球員投票結果是第五](../Page/美國職棒大聯盟最有價值球員.md "wikilink")\[10\]
。

[Travis_Hafner_intentionally_walked.jpg](https://zh.wikipedia.org/wiki/File:Travis_Hafner_intentionally_walked.jpg "fig:Travis_Hafner_intentionally_walked.jpg")的比賽中獲得故意四壞球保送的哈夫納。\]\]
2006年7月7日的比賽，哈夫納打出一支滿壘全壘打。這是大聯盟歷史上第一位在[明星賽舉行前](../Page/美國職棒大聯盟明星賽.md "wikilink")，季內擊出5支滿壘全壘打球員\[11\]。8月，以13支全壘打、30個打點及.346打擊率成為美國聯盟8月最佳球員\[12\]。9月1日被德州遊騎兵隊投手[C·J·威爾森擊手](../Page/C·J·威爾森.md "wikilink")，餘下比賽被登錄到傷兵名單之上。9月9日，進行X-光檢驗後，證實右手出現骨折\[13\]。

2007年，哈夫納表現稍為下滑。球季打擊率只有.266，較為季的.306及2005年.305為低。不過仍然擊出24支全壘打及100個打點，連續4個球季超過100個打點數\[14\]，在明星賽休息期間，哈夫納獲印地安人隊的新合約，合共4年，至2012年截止\[15\]。

### 紐約洋基

## 参考文献

## 外部連結

  - [The Pronk
    Shift](https://web.archive.org/web/20070426124037/http://www.armchairgm.com/index.php?title=The_Pronk_Shift)

{{-}}

[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:德州遊騎兵隊球員](../Category/德州遊騎兵隊球員.md "wikilink")
[Category:克里夫蘭印地安人隊球員](../Category/克里夫蘭印地安人隊球員.md "wikilink")
[Category:紐約洋基隊球員](../Category/紐約洋基隊球員.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14. [Travis Hafner
    Profile](http://sports.yahoo.com/mlb/players/6980;_ylt=ApB3A0zjEEj9Tq6z2b9jvv2FCLcF)
    sports.yahoo.com
15. [Tribe, Hafner ink $57M, 4 -year
    extension](http://www.mlive.com/sportsflash/topstories/index.ssf?/base/sports-19/1184285662205450.xml&storylist=)
     mlive.com