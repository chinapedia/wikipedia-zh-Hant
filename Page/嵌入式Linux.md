[Smart_phone.jpg](https://zh.wikipedia.org/wiki/File:Smart_phone.jpg "fig:Smart_phone.jpg"),是一个基于Linux
[Android的](../Page/Android.md "wikilink")[智能手机](../Page/智能手机.md "wikilink")。\]\]

**嵌入式Linux**（）是一類[嵌入式作業系統的概稱](../Page/嵌入式作業系統.md "wikilink")，這類型的[作業系統皆以](../Page/作業系統.md "wikilink")[Linux內核為基礎](../Page/Linux內核.md "wikilink")，被設計來使用於[嵌入式裝置](../Page/嵌入式裝置.md "wikilink")。這類作業系統被廣泛地使用在[行動電話](../Page/行動電話.md "wikilink")、[個人數位助理](../Page/個人數位助理.md "wikilink")（PDA）、媒體播放器以及眾多消費性電子裝置中。

在過去，嵌入式應用通常使用專用的[組合語言程式碼](../Page/組合語言.md "wikilink")。開發者必須撰寫所有的硬體驅動程式以及介面。

自從Linux出現之後，以自由軟體為主的核心與公用程式可被放進嵌入式裝置有限的硬體資源中。典型的嵌入式Linux安裝大概需要2百萬位元組（2M
Byte）的系統記憶體。

嵌入式Linux與其他嵌入式作業系統的比較如下：

  - 開放源碼
  - 所需容量小（最小的安裝大約需要2MB）
  - 不需版權費用
  - 成熟與穩定（經歷這些年的發展與使用）
  - 良好的支援

[V9.jpg](https://zh.wikipedia.org/wiki/File:V9.jpg "fig:V9.jpg"),使用embedded
Linux\]\]
[Sharp_Zaurus.jpg](https://zh.wikipedia.org/wiki/File:Sharp_Zaurus.jpg "fig:Sharp_Zaurus.jpg")和[OPIE](../Page/:en:OPIE_user_interface.md "wikilink")\]\]

## 使用Linux的行動電話

### 原生的嵌入式Linux

  - [摩托罗拉mobile](../Page/摩托罗拉.md "wikilink")
    telephones,一般而言是基於[MontaVista](../Page/MontaVista.md "wikilink")
    Linux,包括[RAZR²
    V8](../Page/:en:Motorola_RAZR².md "wikilink")、[:en:Motorola RAZR²
    V8](../Page/:en:Motorola_RAZR²_V8.md "wikilink"), [ROKR
    E2](../Page/:en:Motorola_ROKR_E2.md "wikilink")、[Motorola
    E6](../Page/Motorola_E6.md "wikilink")、[A780](../Page/:en:Motorola_A780.md "wikilink")、[E680](../Page/:en:Motorola_E680.md "wikilink"),
    E680i, [A910](../Page/:en:Motorola_A910.md "wikilink"),
    [A1200](../Page/:en:Motorola_A1200.md "wikilink"), A1210, A1600,
    A1800,[U9](../Page/:en:Motorola_U9.md "wikilink"),
    [E8及其他](../Page/:en:Motorola_E8.md "wikilink")
  - [Openmoko](../Page/:en:Openmoko.md "wikilink")'s [Neo
    1973或](../Page/:en:Neo_1973.md "wikilink")[Freerunner](../Page/:en:Neo_Freerunner.md "wikilink")
  - [诺基亚的](../Page/诺基亚.md "wikilink")[Maemo](../Page/Maemo.md "wikilink")、[MeeGo](../Page/MeeGo.md "wikilink")
  - [Google的](../Page/Google.md "wikilink")[Android](../Page/Android.md "wikilink")
  - [惠普公司的](../Page/惠普公司.md "wikilink")[webOS](../Page/webOS.md "wikilink")
  - [Amazon.com的](../Page/Amazon.com.md "wikilink")[金讀](../Page/金讀.md "wikilink")
  - [Linksys](../Page/Linksys.md "wikilink")
    [WRT54G版本](../Page/WRT54G.md "wikilink")4或更早
  - the Panasonic P901i
  - NEC N901ic telephones
  - Philips [LPC3180](../Page/:en:LPC3180.md "wikilink")
  - UTstarcom [DV007](../Page/:en:DV007.md "wikilink")
  - [gumstix](../Page/:en:gumstix.md "wikilink") basix, connex and
    verdex

### 廠商

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/愛可信.md" title="wikilink">愛可信</a></li>
<li><a href="../Page/LynuxWorks.md" title="wikilink">LynuxWorks</a></li>
<li><a href="../Page/MontaVista.md" title="wikilink">MontaVista</a></li>
<li><a href="../Page/EzHomeTech.md" title="wikilink">EzHomeTech</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/SYSGO.md" title="wikilink">SYSGO</a></li>
<li><a href="../Page/TimeSys.md" title="wikilink">TimeSys</a></li>
<li><a href="../Page/Wind_River_Systems.md" title="wikilink">Wind River Systems</a></li>
</ul></td>
</tr>
</tbody>
</table>

## Small single board computers（SBC）運行Linux

## 參考文獻與外部連結

[Category:Linux](../Category/Linux.md "wikilink")
[Category:嵌入式操作系统](../Category/嵌入式操作系统.md "wikilink")
[嵌入式Linux](../Category/嵌入式Linux.md "wikilink")