**川波县**（**Trimble County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州北部的一個縣](../Page/肯塔基州.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[印第安納州相望](../Page/印第安納州.md "wikilink")。面積405平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口8,125人。縣治[貝德福德](../Page/貝德福德_\(肯塔基州\).md "wikilink")（Bedford）。

成立於1837年2月9日。縣名紀念[美國最高法院法官](../Page/美國最高法院.md "wikilink")[羅伯特·川波](../Page/羅伯特·川波.md "wikilink")
（Robert Trimble）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[T](../Category/肯塔基州行政區劃.md "wikilink")
[Category:路易維爾都會區](../Category/路易維爾都會區.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.