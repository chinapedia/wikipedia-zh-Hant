**川田
紳司**是[日本男性](../Page/日本.md "wikilink")[声優](../Page/声優.md "wikilink")。[栃木縣出身](../Page/栃木縣.md "wikilink")。

屬日本藝能事務所[賢Production公司](../Page/賢Production.md "wikilink")。

## 參與作品

### 電視動畫

**2000年**

  - （哉生優）

  - [真·女神轉生 惡魔之子](../Page/真·女神轉生_惡魔之子.md "wikilink")（スフィンクス）

  - [第一神拳](../Page/第一神拳.md "wikilink")（音羽ジム練習生、同級生、学生們）

**2001年**

  - [少年足球](../Page/少年足球.md "wikilink")（馬場 他）

  - [妙妙魔法屋](../Page/妙妙魔法屋.md "wikilink")（鏡ヘナモン 他）

  - （セルゲイ・IV・スニーク）

  - [通靈王](../Page/通靈王.md "wikilink")（シルバーシールド）

  - [電腦冒險記](../Page/電腦冒險記.md "wikilink")（ドラグオン）

  - （後藤史彦）

  - [宇宙人形17](../Page/宇宙人形17.md "wikilink")（トオル）

  - [神奇寶貝](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")（ミナキ）

  - （ミナキ）

  - [魔力女管家](../Page/魔力女管家.md "wikilink")（水谷くん、帆風湊）

  - [RUN=DIM](../Page/RUN=DIM.md "wikilink")（野島優）

**2002年**

  - [水瓶戰記](../Page/水瓶戰記.md "wikilink")（ディレクター）
  - [火影忍者](../Page/火影忍者.md "wikilink")（[油女志乃](../Page/油女志乃.md "wikilink")）
  - [迷糊天使](../Page/迷糊天使.md "wikilink")（客人、警官、司機、老爺爺A）

**2003年**

  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")（清兵衛）

  - [神魂合體](../Page/神魂合體.md "wikilink")（森本、ククラチョフ）

  - （中原卓也）

  - [.hack//黃昏的腕輪傳說](../Page/.hack/黃昏的腕輪傳說.md "wikilink")（大輔）

  - [鼻毛真拳](../Page/鼻毛真拳.md "wikilink")（ソニック、コブテン、パピットマン）

  - [爱的魔法](../Page/爱的魔法.md "wikilink")（浮氣光洋）

  - [ロックマンエグゼAXESS](../Page/ロックマンエグゼAXESS.md "wikilink")（コールドマン）

  - [驚爆危機？校園篇](../Page/驚爆危機.md "wikilink")（柳田）

**2004年**

  - [アガサ・クリスティーの名探偵ポワロとマープル](../Page/アガサ・クリスティーの名探偵ポワロとマープル.md "wikilink")（ルパート）
  - [攻壳机动队 S.A.C. 2nd
    GIG](../Page/攻壳机动队_S.A.C._2nd_GIG.md "wikilink")（イズミ）
  - [サムライチャンプルー](../Page/サムライチャンプルー.md "wikilink")（手下）
  - [校园迷糊大王](../Page/校园迷糊大王.md "wikilink")（花井春樹）
  - [超重神グラヴィオンZwei](../Page/超重神グラヴィオン.md "wikilink")（ジョゼ）
  - [ニニンがシノブ伝](../Page/ニニンがシノブ伝.md "wikilink")（忍者）
  - [光と水のダフネ](../Page/光と水のダフネ.md "wikilink")（チャン）
  - [游戏王GX](../Page/游戏王GX.md "wikilink")（猪爪誠）
  - [ロックマンエグゼStream](../Page/ロックマンエグゼStream.md "wikilink")（コールドマン）

**2005年**

  - [ガンパレード・オーケストラ](../Page/ガンパレード・オーケストラ_\(アニメ\).md "wikilink")（上田虎雄）
  - [スターシップ・オペレーターズ](../Page/スターシップ・オペレーターズ.md "wikilink")（榊原コウキ）
  - [ツバサ・クロニクル](../Page/ツバサ-RESERVoir_CHRoNiCLE-.md "wikilink")（自警団員）
  - [ディノブレイカー](../Page/ディノブレイカー.md "wikilink")（マルコ・ロッカ）
  - [蟲師](../Page/蟲師.md "wikilink")（塊）
  - [MAJOR 2nd season](../Page/メジャー_\(アニメ\).md "wikilink")（香取）
  - [MÄR -メルヘヴン-](../Page/MÄR#アニメ\(メルヘヴン\).md "wikilink")（イアン）
  - [雪の女王](../Page/雪の女王_\(NHKアニメ\).md "wikilink")（ギルバート）
  - [ロックマンエグゼBEAST](../Page/ロックマンエグゼBEAST.md "wikilink")（ダイブマン）

**2006年**

  - [犬神！](../Page/犬神！.md "wikilink")（桑秋貝）
  - [くじびきアンバランス](../Page/くじびきアンバランス.md "wikilink")（鏑木有也）
  - [校園迷糊大王 二學期](../Page/校園迷糊大王.md "wikilink")（花井春樹）
  - [砂沙美☆魔法少女クラブ](../Page/砂沙美☆魔法少女クラブ.md "wikilink")（殿部先生）
  - [地上最强新娘](../Page/地上最强新娘.md "wikilink")（虎金井天礼、アナウンサー）
  - [となグラ\!](../Page/となグラ!.md "wikilink")（町田洸介）
  - [パンプキン・シザーズ](../Page/パンプキン・シザーズ.md "wikilink")（ブルーノ）
  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（中村剛太）
  - [FLAG](../Page/FLAG_\(アニメ\).md "wikilink")（一柳信）
  - [×××HOLiC](../Page/×××HOLiC.md "wikilink")（男）
  - [妖怪人間ベム](../Page/妖怪人間ベム.md "wikilink")(2006年) （ゴア）
  - [LEMON ANGEL
    PROJECT](../Page/LEMON_ANGEL_PROJECT.md "wikilink")（片桐仁）
  - ロックマンエグゼBEAST+（ダイブマン）
  - [ワンワンセレプー それゆけ\!徹之進](../Page/ワンワンセレプー_それゆけ!徹之進.md "wikilink")（桜の青年）

**2007年**

  - [星界死者之书](../Page/星界死者之书.md "wikilink")（猪尾亜久里）
  - [GO！纯情水泳社！](../Page/GO！纯情水泳社！.md "wikilink")（所沢先生）
  - [青空下的约定～欢迎来到鸫寮～](../Page/青空下的约定#动画.md "wikilink")（三田村隆史）
  - [守护甜心！](../Page/守护甜心！.md "wikilink")（撫子爸爸 ）
  - [神曲奏界](../Page/神曲奏界.md "wikilink")（ナトリ）
  - [精灵守护者](../Page/守护者系列#电视动画.md "wikilink")（ユン、気の民、無人 他）
  - [火影忍者疾风传](../Page/火影忍者疾风传.md "wikilink")（[油女志乃](../Page/油女志乃.md "wikilink")）
  - [交響情人夢](../Page/交響情人夢#动画.md "wikilink")（峰龍太郎）
  - [蓝龙](../Page/蓝龙#TV动画.md "wikilink")（シュナイダー）
  - [地球防卫少年](../Page/地球防卫少年.md "wikilink")（関政光）
  - [南家三姐妹](../Page/南家三姐妹.md "wikilink")（長男、男性教師）
  - [MAJOR 3rd season](../Page/MAJOR_3rd_season.md "wikilink")（香取）

**2008年**

  - [狂乱家族日記](../Page/狂乱家族日記.md "wikilink")（蓋柏克‧凱薩）
  - [库洛洛军曹](../Page/库洛洛军曹.md "wikilink")（ラビリン人）
  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")（ヤス）
  - [鉄腕巴迪 DECODE](../Page/鉄腕巴迪.md "wikilink")（テュート（サトル））
  - [デュエル・マスターズ クロス](../Page/デュエル・マスターズ_\(漫画\).md "wikilink")（太田Q、手下）
  - [ペンギンの問題](../Page/ペンギンの問題.md "wikilink")（岡本ポール 、よしお、飼育員、不良B）
  - [药师寺凉子怪奇事件簿](../Page/药师寺凉子怪奇事件簿.md "wikilink")（山岸）

**2009年**

  - [大正野球娘](../Page/大正野球娘.md "wikilink")（高原伴睦）
  - [青花](../Page/青花_\(漫畫\).md "wikilink")（奧平忍）
  - [Viper's Creed](../Page/Viper's_Creed.md "wikilink")（Rudra）

**2010年**

  - [交響情人夢 Finale](../Page/交響情人夢_\(動畫\).md "wikilink")（峰龍太郎）
  - [刀語](../Page/刀語.md "wikilink")（真庭川獺）
  - [爆漫王](../Page/爆漫王.md "wikilink")（瓶子吉久）

**2011年**

  - [偶像大師 (動畫)](../Page/偶像大師_\(動畫\).md "wikilink")（音響監督）
  - [白兔玩偶](../Page/白兔玩偶.md "wikilink")（辰巳先生）
  - [爆漫王。2](../Page/爆漫王。.md "wikilink")（瓶子吉久）
  - [卡片戰鬥先導者](../Page/卡片戰鬥先導者.md "wikilink")（美南ハルミ）
  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（ダラリオ・ヘイワード）
  - [美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink")（ヨハネス）
  - [花開物語](../Page/花開物語.md "wikilink")（サバゲーマーB）
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")(獵人)（キリコ息子）
  - ペンギンの問題DX?（岡本ポール、森シュナイダー）
  - [迴轉企鵝罐](../Page/迴轉企鵝罐.md "wikilink")（真砂子之父 / 冠葉之實父）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（山本英司）
  - [食夢者瑪莉](../Page/食夢者瑪莉.md "wikilink")（グリッチョ）

**2012年**

  - [輪迴的拉格朗日](../Page/輪迴的拉格朗日.md "wikilink")（ジェイムズ・ロウ）
  - [在盛夏等待](../Page/在盛夏等待.md "wikilink")（小倉聰）
  - [爆漫王。3](../Page/爆漫王。.md "wikilink")（瓶子吉久、Kenchan）

**2013年**

  - [進擊的巨人](../Page/進擊的巨人.md "wikilink")（歐魯·波札德）
  - [噬血狂襲](../Page/噬血狂襲.md "wikilink")（摩怪）

**2014年**

  - [來自風平浪靜的明天](../Page/來自風平浪靜的明天.md "wikilink")（三橋悟）
  - [天雷爭霸：復仇者聯盟](../Page/天雷爭霸：復仇者聯盟.md "wikilink")（彼得·帕克/蜘蛛人）
  - [JOJO的奇妙冒險 星塵鬥士](../Page/JOJO的奇妙冒險.md "wikilink")（Rubber Soul）
  - [黑色子彈](../Page/黑色子彈.md "wikilink")（我堂英彥）
  - [前進吧！登山少女 Second Season](../Page/前進吧！登山少女.md "wikilink")（店員）
  - [飆速宅男](../Page/飆速宅男.md "wikilink") GRANDE ROAD（大粒健士）

**2015年**

  - [食戟之靈](../Page/食戟之靈.md "wikilink")（小西寬一、富田友哉）
  - [银魂°](../Page/銀魂_\(動畫\).md "wikilink")（宇宙海賊・千鳥第二師團團長）
  - [下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink")（遠藤正志）
  - [進擊！巨人中學校](../Page/進擊的巨人.md "wikilink")（歐魯·波札德）
  - [終結的熾天使 名古屋決戰篇](../Page/終結的熾天使.md "wikilink")（優一郎的父親）
  - [金田一少年之事件簿R 第2期](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（佐久羅京）

**2016年**

  - [Active
    Raid－機動強襲室第八課－](../Page/Active_Raid－機動強襲室第八係－.md "wikilink")（岬大介）
  - [從前有座靈劍山](../Page/從前有座靈劍山.md "wikilink")（志峰真人）
  - [超時空要塞Δ](../Page/超時空要塞Δ.md "wikilink")（查克·馬斯坦古）
  - [食戟之靈](../Page/食戟之靈.md "wikilink") 貳之皿（富田友哉、三田村衛）
  - [WWW.WORKING\!\!](../Page/WORKING!!_\(網路漫畫\).md "wikilink")（進藤的父親）
  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（水口慎吾 \#838）

**2017年**

  - [龍的牙醫](../Page/龍的牙醫.md "wikilink")
  - [不正經的魔術講師與禁忌教典](../Page/不正經的魔術講師與禁忌教典.md "wikilink")（哈雷·亞斯頓）
  - [火影新世代BORUTO -NARUTO NEXT
    GENERATIONS-](../Page/火影忍者.md "wikilink")（油女志乃）
  - [來自深淵](../Page/來自深淵_\(漫畫\).md "wikilink")（工作人員）
  - [咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣.md "wikilink")（德里達）
  - 食戟之靈 餐之皿（小西寬一）
  - [魔法使的新娘](../Page/魔法使的新娘.md "wikilink")（羽鳥夕輝）

**2018年**

  - [霸穹 封神演義](../Page/封神演義_\(漫畫\).md "wikilink")（張天君）
  - [博多豚骨拉麵團](../Page/博多豚骨拉麵團.md "wikilink")（進來）
  - [錢進球場](../Page/錢進球場.md "wikilink")（北村）
  - [刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")（篠原）
  - [Hisone與Masotan](../Page/Hisone與Masotan.md "wikilink")（前澤真吾）
  - [Banana Fish](../Page/Banana_Fish.md "wikilink")（**伊部俊一**\[1\]）
  - [GRAND BLUE碧藍之海](../Page/GRAND_BLUE碧藍之海.md "wikilink")（古手川登志夫、指導者）
  - [後街女孩](../Page/後街女孩.md "wikilink")（**喬治**）
  - [叛逆性百萬亞瑟王](../Page/叛逆性百萬亞瑟王.md "wikilink")（桑達）

**2019年**

  - [路人超能100 II](../Page/路人超能100.md "wikilink")（惡靈）
  - [川柳少女](../Page/川柳少女.md "wikilink")（數學教師）

### OVA

  - [夜勤病棟貳](../Page/夜勤病棟.md "wikilink")（九羽原總一郎）
  - [圣斗士星矢 THE LOST CANVAS
    冥王神话](../Page/圣斗士星矢_THE_LOST_CANVAS_冥王神话.md "wikilink")（死神
    塔纳托斯）

**2015年**

  - [絕命制裁X](../Page/絕命制裁X.md "wikilink")（犬鳴慎一郎）
  - [噬血狂襲 女武神的王國篇 後篇](../Page/噬血狂襲.md "wikilink")（摩怪）

**2016年**

  - [食戟之靈](../Page/食戟之靈.md "wikilink")「巧的下町合戰」（小西寬一）
  - 食戟之靈「暑假的繪里奈」（小西寬一）

**2018年**

  - 噬血狂襲III（摩怪）

### 游戏

  - \[PC\]蔷薇树上蔷薇开（**金子光伸**）
      - \[PS2\]蔷薇树上蔷薇开 -Das Versprechen-
      - \[PC\]蔷薇树上蔷薇开 fan disc
  - \[PS2\]死亡连接（**尼古拉**）
  - \[PC\]苍天之彼方（太星）
  - \[DS\]在黑暗的尽头等待着你（神子元直树）
  - \[PC\]（水先圭二）
  - \[PC\]KoiGIG ～DEVIL×ANGEL～（青柳功一）
  - \[PC\]Brothers～恋爱吧哥哥大人～（**春日由鹰**）
  - \[PC\]Brothers～再爱一次哥哥大人～
  - \[Pc\]ピヨたん～ハウスキーパーはCuteな探偵～ （长谷川雅治）
  - \[PC\]鳥籠婚姻（真行寺優人）
  - \[PS4\][審判之眼：死神的遺言](../Page/審判之眼：死神的遺言.md "wikilink") （大久保新平）

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:栃木縣出身人物](../Category/栃木縣出身人物.md "wikilink")
[Category:賢Production](../Category/賢Production.md "wikilink")

1.