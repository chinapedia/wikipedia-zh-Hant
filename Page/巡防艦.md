[French_frigate_La_Fayette_(F-710).jpg](https://zh.wikipedia.org/wiki/File:French_frigate_La_Fayette_\(F-710\).jpg "fig:French_frigate_La_Fayette_(F-710).jpg")\]\]
[HMAS_Darwin_(FFG_04).jpg](https://zh.wikipedia.org/wiki/File:HMAS_Darwin_\(FFG_04\).jpg "fig:HMAS_Darwin_(FFG_04).jpg")的
(Adelaide Class Frigate)四號艦— (HMAS
Darwin)。此級船艦的設計主要取自美國海軍的[派里級巡防艦](../Page/派里級巡防艦.md "wikilink")
(Oliver Hazard Perry class)。\]\]
[ROCN_cheng_kung_class_PFG2-1105_and_PFG2-1101_20050624.jpg](https://zh.wikipedia.org/wiki/File:ROCN_cheng_kung_class_PFG2-1105_and_PFG2-1101_20050624.jpg "fig:ROCN_cheng_kung_class_PFG2-1105_and_PFG2-1101_20050624.jpg")的[成功級巡防艦](../Page/成功級巡防艦.md "wikilink")，也是派里級巡防艦的海外授權製造艦級之一。\]\]
[2013_10_17_MKS_180_Beispiel_IMG_1681_P_b_P_D.JPG](https://zh.wikipedia.org/wiki/File:2013_10_17_MKS_180_Beispiel_IMG_1681_P_b_P_D.JPG "fig:2013_10_17_MKS_180_Beispiel_IMG_1681_P_b_P_D.JPG")計劃中的MKS
180巡防艦（F126）\]\]
**巡防艦**（，在英美西方海軍中是稱比護衛艦大一倍的艦船，但苏军和中國人民解放軍繼續稱为[護衛艦](../Page/護衛艦#苏俄海军与中國人民解放軍海軍的分級.md "wikilink")），是指在近岸海区与其他兵力合同或单独地执行歼灭敌方轻型舰船，或在遠洋为大型攻擊舰船担負警戒和护卫任务的[水面舰艇](../Page/水面舰艇.md "wikilink")，是[綠水海軍中屬於近岸戰鬥用途的最大船艦](../Page/綠水海軍.md "wikilink")，或[藍水海軍遠洋分派守衛任務的中型艦艇](../Page/藍水海軍.md "wikilink")，噸位上僅次於航行大洋的[驅逐艦](../Page/驅逐艦.md "wikilink")，大約在2000-4000噸左右。

在任務角色上，巡防艦最初是為船團護衛而設計，避免運輸船艦遭到[潛水艇](../Page/潛水艇.md "wikilink")、[潛艦的襲擾](../Page/潛艦.md "wikilink")，因此特別強調反潛作戰能力，如今也成為一般中等國家的海軍作戰主力。雖然常見於近岸，但為對付海盜，也會航行遠洋。此外，在[美國海軍以](../Page/美國海軍.md "wikilink")[航空母艦為主的艦隊中](../Page/航空母艦.md "wikilink")，巡防艦也擔任艦隊最外圍的防禦工作，包括攔截來襲的威脅（多為飛行物，如[飛彈](../Page/飛彈.md "wikilink")、[飛機](../Page/飛機.md "wikilink")、[直升機](../Page/直升機.md "wikilink")、[無人航空載具](../Page/無人航空載具.md "wikilink")），以及各種救援任務。

## 歷史

巡防舰一词来自英文frigate，而这个单词来自法文frégate，意大利文fregatta，是中世纪地中海上的一种帆桨兼用的快船galleass的别称，并成为16至17世纪出现的[快速帆船的称呼](../Page/快速帆船.md "wikilink")。

海军的第一艘称为frigate的军舰是1646年下水的、380吨的“康斯坦·沃里克”号，它的船体模仿一艘被英军捕获的、在敦刻尔克建造的法国掠私船。到1677年，木质帆船依据所搭载的火炮数量开始划分等级。英国海军大臣[佩皮斯就为帆船订出六个等级](../Page/佩皮斯.md "wikilink")：

| 帆船等级 | 炮甲板层数 | 装炮数量    |
| ---- | ----- | ------- |
| 一等舰  | 三层    | 100门以上  |
| 二等舰  | 三层    | 84-100门 |
| 三等舰  | 二层至三层 | 64-84门  |
| 四等舰  | 二层    | 50-64门  |
| 五等舰  | 单层    | 32-50门  |
| 六等舰  | 单层    | 32门以下   |

当时舰船的火炮排列在两舷，在海战中双方帆船排成单纵队，用舷侧的火炮轰击对方。从一等到四等舰就是担任这种任务的[战列舰](../Page/风帆战列舰.md "wikilink")（ships
of the
line）。如[特拉法加海战中](../Page/特拉法加海战.md "wikilink")，英国地中海舰队司令[纳尔逊的旗舰](../Page/纳尔逊.md "wikilink")“”就是一艘造价10万英镑的一等舰。由于造价昂贵，[英国舰队中的一等舰不多](../Page/英国.md "wikilink")，而数量最多的主力舰还是74门炮的三等舰，其造价约6万英镑。而造价2.6万[英镑的四等舰主要担任英国海外巡航分舰队的旗舰](../Page/英镑.md "wikilink")。攻击力不够的五等、六等舰不能在阵列中决战，但它们轻捷快速，主要从事袭击敌手运输线，侦察监视，情报或命令的传送，护送商船和要急送等任务，被称为frigate。此时的frigate，被译为巡航舰或快速舰相对准确些。

然而，還有等级之外更小的帆船被成为corvette（[護衛艦](../Page/護衛艦.md "wikilink")）或sloop-of-war（炮舰）。

[二次世界大戰時](../Page/二次世界大戰.md "wikilink")，由於[德國企圖用大量的潛水艇來封鎖](../Page/德國.md "wikilink")[英國的海上](../Page/英國.md "wikilink")[補給線](../Page/補給線.md "wikilink")，使其無法獲得[美國的軍用物資接濟](../Page/美國.md "wikilink")，而英國則是要盡可能突破此一封鎖，當時德國[潛艇時常襲擊往來於英美間的船隻](../Page/潛艇.md "wikilink")，任何的軍艦、民船一律擊沉，以此來斷絕英國可能的海上外援，然英國也為了保護運送軍用物資的船隻而派出隨行的[驅逐艦](../Page/驅逐艦.md "wikilink")，一旦發現有德國潛艇則護航的英國驅逐艦便會對潛艇展開攻擊，或至少驅趕，避免運送物資的船團受襲，以順利將物資運抵。

不過，英國並沒有足夠數目的驅逐艦來護衛大量往返的運資船團，且驅逐艦的武裝火力強大，用其來對付潛艇似乎也大材小用，所以英國決定開發一種各項規格設計都較驅逐艦簡易的軍艦，且能專精於對付潛艇及船團護衛任務，此即是巡防艦的由來。

美國海軍當初則把此種功能的艦艇稱為護航驅逐艦(destroyer
escort，艦號標示DE)，在1970年代，為了和北約各國溝通方便，也改分級為巡防艦(frigate,
標示FF)。過去的大日本帝國海軍則是以哨戒艇或[海防艦擔任此種任務](../Page/海防艦.md "wikilink")，有些人把[松級驅逐艦歸類為這種](../Page/松級驅逐艦.md "wikilink")，並不算完全正確的。

## 噸位

一般而言現代化的巡防艦，其標準排水量多在1,600噸以上，以下多為巡邏艦、護衛艦，現在化的主流巡防艦多在3,000噸以上，5,000噸以下，5,000噸以上則多為[驅逐艦](../Page/驅逐艦.md "wikilink")。不過這並沒有具體明確的界定，也有許多現代化的驅逐艦位在4,000噸的量級，此外[第二次世界大戰期間有許多驅逐艦僅有](../Page/第二次世界大戰.md "wikilink")2,000多噸，加上法國不使用驅逐艦一稱，因此也有偏高噸位的法國戰艦依然稱為「巡防艦」，而不能一概而論。

[日本海上自衛隊因為受到](../Page/日本海上自衛隊.md "wikilink")[戰後憲法的限制不得組織攻擊性武力](../Page/日本國憲法.md "wikilink")，因此並未設置包括巡防艦在內的各式軍艦艦級，而是将其所有千吨级以上的水面武裝舰艇一律命名為「[護衛艦](../Page/護衛艦_\(日本\).md "wikilink")」以強調其防衛性武力的定位。

| 國際概略分法     |
| ---------- |
| 1000噸以下    |
| 1000至2000噸 |
| 2000至4000噸 |
| 4000至8000噸 |
| 8000噸以上    |

## 現代巡防艦列表

### 二戰後

**歐洲**

  - ：

<!-- end list -->

  - ：



[花月级巡防舰](../Page/花月级巡防舰.md "wikilink")

  - ：



[布蘭登堡級巡防艦](../Page/布蘭登堡級巡防艦.md "wikilink")

  - ：

[海德拉级巡防舰](../Page/海德拉级巡防舰.md "wikilink")
[埃利级巡防舰](../Page/埃利级巡防舰.md "wikilink")

  - ：



[狼级巡防舰](../Page/狼级巡防舰.md "wikilink")
[西北风级巡防舰](../Page/西北风级巡防舰.md "wikilink")

  - ：

<!-- end list -->

  - ：




[特朗普級巡防艦](../Page/特朗普級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：



[瓦斯科·達伽馬級巡防艦](../Page/瓦斯科·達伽馬級巡防艦.md "wikilink")

  - ：

[巴利亞里级巡防舰](../Page/巴利亞里级巡防舰.md "wikilink")

  - ：

[15型护卫舰](../Page/15型护卫舰.md "wikilink")
[16型护卫舰](../Page/16型护卫舰.md "wikilink")







[亚马逊级巡防舰](../Page/21型巡防舰.md "wikilink")
[阔劍级巡防舰](../Page/22型巡防舰.md "wikilink")
[公爵级巡防舰](../Page/23型巡防舰.md "wikilink")

**亞洲**

  - ：


[班加班德胡級巡防艦](../Page/班加班德胡級巡防艦.md "wikilink")

  - ：

[安字级巡防舰](../Page/安字级巡防舰.md "wikilink")
[太字级巡防舰](../Page/太字级巡防舰.md "wikilink")
[山字级巡防舰](../Page/山字级巡防舰.md "wikilink")
[成功級巡防艦](../Page/成功級巡防艦.md "wikilink")
[濟陽級巡防艦](../Page/濟陽級巡防艦.md "wikilink")

  - :

[053H型导弹护卫舰](../Page/053H型导弹护卫舰.md "wikilink")
[053H1型导弹护卫舰](../Page/053H1型导弹护卫舰.md "wikilink")
[053H1Q型导弹护卫舰](../Page/053H1Q型导弹护卫舰.md "wikilink")
[053H2型导弹护卫舰](../Page/053H2型导弹护卫舰.md "wikilink")
[053H2G型导弹护卫舰](../Page/053H2G型导弹护卫舰.md "wikilink")
[053H1G型导弹护卫舰](../Page/053H1G型导弹护卫舰.md "wikilink")
[053H3型导弹护卫舰](../Page/053H3型导弹护卫舰.md "wikilink")

  - ：





  - ：

<!-- end list -->

  - ：



  - ：

[曙號護衛艦 (初代)](../Page/曙號護衛艦_\(初代\).md "wikilink")


[若葉號護衛艦](../Page/若葉號護衛艦.md "wikilink")


[石狩號護衛艦](../Page/石狩號護衛艦.md "wikilink")
[夕張級護衛艦](../Page/夕張級護衛艦.md "wikilink")
[阿武隈级护卫舰](../Page/阿武隈级护卫舰.md "wikilink")

  - ：

[蔚山級巡防艦](../Page/蔚山級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：




[琉球级巡防舰](../Page/琉球级巡防舰.md "wikilink")

  - ：

<!-- end list -->

  - ：

<!-- end list -->

  - ：


[葛雷戈里奧·德爾·皮拉爾級巡防艦](../Page/葛雷戈里奧·德爾·皮拉爾級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：


[昭披耶級巡防艦](../Page/昭披耶級巡防艦.md "wikilink")

  - ：




  - ：


[里加级护卫舰](../Page/里加级护卫舰.md "wikilink")
[别佳级护卫舰](../Page/别佳级护卫舰.md "wikilink")
[米尔卡级护卫舰](../Page/米尔卡级护卫舰.md "wikilink")
[科尼级护卫舰](../Page/科尼级护卫舰.md "wikilink")
[克里瓦克级护卫舰](../Page/风暴海燕级护卫舰.md "wikilink")
[不惧级护卫舰](../Page/不惧级护卫舰.md "wikilink")

**非洲**

  - ：

<!-- end list -->

  - ：

[花月級巡防艦](../Page/花月級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：

[勇敢級巡防艦](../Page/勇敢級巡防艦.md "wikilink")

**美洲（盎格魯+拉丁）**

  - ：

[哈利法克斯级巡防舰](../Page/哈利法克斯级巡防舰.md "wikilink")

  - ：




[諾克斯級巡防艦](../Page/諾克斯級巡防艦.md "wikilink")
[派里級巡防艦](../Page/派里級巡防艦.md "wikilink")

  - ：

（阿根廷歸類為驅逐艦）

  - ：

<!-- end list -->

  - ：

<!-- end list -->

  - ：



  - :

（與智利海軍同型）

  - :

**大洋洲**

  - ：

[阿德萊德級巡防艦](../Page/阿德萊德級巡防艦.md "wikilink")
[安扎克级巡防舰](../Page/安扎克级巡防舰.md "wikilink")

  - ：

[安扎克级巡防舰](../Page/安扎克级巡防舰.md "wikilink")

### 匿蹤巡防艦

**歐洲**

  - ：

[伊萬·休特菲爾德級巡防艦](../Page/伊萬·休特菲爾德級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：

[拉法葉級巡防艦](../Page/拉法葉級巡防艦.md "wikilink")

  - ：

[薩克森級巡防艦](../Page/薩克森級巡防艦.md "wikilink")
[巴登-符騰堡級巡防艦](../Page/巴登-符騰堡級巡防艦.md "wikilink")
F126

  - ：

[城市級巡防艦](../Page/26型巡防艦.md "wikilink")
[31型巡防艦](../Page/31型巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：

[七省級巡防艦](../Page/七省級巡防艦.md "wikilink")

  - ：

[阿爾瓦羅·巴贊級巡防艦](../Page/阿爾瓦羅·巴贊級巡防艦.md "wikilink")
**聯合研製**

  - [歐洲多用途巡防艦](../Page/歐洲多用途巡防艦.md "wikilink")/

  - /

**亞洲**

  - ：

[康定級巡防艦](../Page/康定級巡防艦.md "wikilink")
[新一代巡防艦](../Page/新一代巡防艦.md "wikilink")

  - ：

[054型导弹护卫舰](../Page/054型导弹护卫舰.md "wikilink")
[054A型导弹护卫舰](../Page/054A型导弹护卫舰.md "wikilink")

  - ：

[拉登·艾迪·瑪爾塔迪納塔級巡防艦](../Page/拉登·艾迪·瑪爾塔迪納塔級巡防艦.md "wikilink")

  - ：

[塔尔瓦级巡防舰](../Page/塔尔瓦级巡防舰.md "wikilink")
[什瓦利克级巡防舰](../Page/什瓦利克级巡防舰.md "wikilink")

  - ：

[仁川級巡防艦](../Page/仁川級巡防艦.md "wikilink")
[大邱級巡防艦](../Page/大邱級巡防艦.md "wikilink")

  - ：

<!-- end list -->

  - ：

[馬哈拉惹里拉級巡防艦](../Page/馬哈拉惹里拉級巡防艦.md "wikilink")

  - ：

[江喜陀级巡防舰](../Page/江喜陀级巡防舰.md "wikilink")

  - ：

[何塞·黎剎級巡防艦](../Page/何塞·黎剎級巡防艦.md "wikilink")

  - ：

[利雅德級巡防艦](../Page/拉法叶级巡防舰.md "wikilink")

  - ：

[可畏级巡防舰](../Page/可畏级巡防舰.md "wikilink")

  - ：

[蒲美蓬·阿杜德級巡防艦](../Page/蒲美蓬·阿杜德級巡防艦.md "wikilink")

  - ：



  - ：


[格里戈洛维奇海军上将级护卫舰](../Page/11356M型护卫舰.md "wikilink")
[戈尔什科夫级护卫舰](../Page/22350型护卫舰.md "wikilink")
[獵豹級護衛艦](../Page/獵豹級護衛艦.md "wikilink")

**非洲**

  - ：

[穆罕默德六世級巡防艦](../Page/欧洲多用途巡防舰.md "wikilink")

  - ：

[勇敢級巡防艦](../Page/勇敢級巡防艦.md "wikilink")

**美洲（盎格魯+拉丁）**

  - ：

<!-- end list -->

  - ：

[獨立級濱海戰鬥艦](../Page/獨立級濱海戰鬥艦.md "wikilink")
[自由级濒海战斗舰](../Page/自由级濒海战斗舰.md "wikilink")

  - ：

  - ：

[瓦伊凯里级巡防舰](../Page/瓦伊凯里级巡防舰.md "wikilink")

**大洋洲**：

  - ：

[獵人級巡防艦](../Page/獵人級巡防艦.md "wikilink")

## 参考文献

## 参见

  - [護衛航空母艦](../Page/護衛航空母艦.md "wikilink")
  - [驅逐艦](../Page/驅逐艦.md "wikilink")
  - [护卫舰](../Page/护卫舰.md "wikilink")
  - [Q船](../Page/Q艇.md "wikilink")

{{-}}

[Category:军舰类型](../Category/军舰类型.md "wikilink")
[巡防艦](../Category/巡防艦.md "wikilink")