**工业标准结构**（**Industry Standard Architecture**，简称ISA）。

## 历史

ISA在1981年誕生，並作為IBM
PC的8位系统，1983年，ISA被升级作為XT總線體系。後來16位的ISA總線在1984年發布。由於ISA設計出來的目的是為了連接擴展卡和[主板](../Page/主板.md "wikilink")，因此ISA的協議同樣允許總線控制，儘管-{只}-有前16MB的内存可以直接訪問。8位的ISA總線頻率为4.77MHz，而16位的工作在8MHz。ISA接口同樣出現在一些非IBM
PC（包括兼容機）上，比如短命的[AT\&T的Hobbit還有後來基於](../Page/AT&T.md "wikilink")[PowerPC的BeBox](../Page/PowerPC.md "wikilink")。

1987年，[IBM試圖以他们所擁有的](../Page/IBM.md "wikilink")“微通道體系架構體系”（Micro Channel
Architecture，簡稱[MCA](../Page/MCA.md "wikilink")）取代ISA，並重新取得對計算機架構和市場上的控制權。MCA總線比ISA更先進，但並不兼容ISA。為了繼續控制架構上和市場上的控制權，電腦生產商以“延伸工業標準體系架構”（Extended
Industry Standard
Architecture，簡稱[EISA](../Page/EISA.md "wikilink")），以及後来的“VESA本地總線”（[VESA](../Page/VESA.md "wikilink")
Local
Bus，簡稱[VLB](../Page/VLB.md "wikilink")）做出還擊。事實上，由於组成[VESA組織的生產商已经有能力生產MCA設備](../Page/VESA.md "wikilink")，所以最初VESA打算在VLB中利用MCA的一些部分。EISA和VLB都兼容ISA標準的擴展。

基於ISA的計算機的用户不得不了解一些關於硬件的特殊知識來升级硬件系统。在那個时候，支持“隨插即用”（Plug-n-Play）技術的設備非常罕有。用户在添加新設備的时候不得不配置2到3個項目，比如[IRQ](../Page/IRQ.md "wikilink")（中斷請求）、[I/O地址](../Page/I/O地址.md "wikilink")（输出／輸入地址）、[DMA信道](../Page/DMA.md "wikilink")，才能正常使用新設備。MCA架構會幫用戶完成這些設定，而後来的[PCI總線實際上整合了MCA的这些想法](../Page/PCI.md "wikilink")（儘管[PCI更多特點是直接繼承自EISA](../Page/PCI.md "wikilink")）。

這個配置上的缺點最终導致了“ISA隨插即用”系统的誕生。通過對硬件的一些改造，使硬件、系统BIOS和操作系统自動處理這些繁瑣的细節。但實際上，ISA隨插即用的缺陷卻成為了一个令人頭痛的問題，而且没有得到廣泛的支持直到ISA结束其使命。

PCI是第一個在物理展上整合了ISA、MCA、EISA優點的擴展接口，並且它的出現直接地擠壓了ISA在主板上的地位。起初，主板上依然是ISA占主流地位，但已經出現了PCI槽了。到了20世纪90年代中葉，兩種插槽已經在主板上平分秋色了，而ISA插槽很快就在消费PC市場上成為了少數派。微软的PC
97规范更劝说ISA插槽应该完全被除去，尽管当时的系统架构依然需要ISA存在于一些内部发育不良的管线去操作[软驱](../Page/软驱.md "wikilink")、[串口](../Page/串口.md "wikilink")、等等。ISA接口在随後的幾年裡依然存在，甚至看见AGP接口的誕生，之后遗留在主板上的ISA接口也退出历史了。

值得注意的是，PCI插槽反转的话与ISA是很相似的——PCI卡本来是颠倒插入的，允许ISA和PCI连接器在主板上挤在一起。两个连接器一次-{只}-有一个连接器能正常工作，但这已虑及更大的适应性。

## 8位ISA（XT总线架构）

**XT总线架构**（即8位ISA）是基于[Intel](../Page/Intel.md "wikilink")[8086和](../Page/8086.md "wikilink")[8088的](../Page/8088.md "wikilink")[IBM](../Page/IBM.md "wikilink")
[PC](../Page/PC.md "wikilink")／[XT上采用的](../Page/XT.md "wikilink")8位ISA总线。

[ISA_mouse_adapter.JPG](https://zh.wikipedia.org/wiki/File:ISA_mouse_adapter.JPG "fig:ISA_mouse_adapter.JPG")

XT总线有4条DMA通道，这些通道中有3条连接到其他扩展槽中。在这三条通道中，正常情况下又有两条分配到特定的机器功能。

| DMA通道 | 扩展性 | 常规功能                                     |
| ----- | --- | ---------------------------------------- |
| 0     | No  | 动态[RAM更新](../Page/隨機存取記憶體.md "wikilink") |
| 1     | Yes | 扩展卡                                      |
| 2     | Yes | [软驱控制器](../Page/軟碟控制器.md "wikilink")     |
| 3     | Yes | [硬盘控制器](../Page/硬盘.md "wikilink")        |

## 16位ISA（AT总线架构）

**AT总线架构**（即16位ISA）在基于Intel
[8086的](../Page/Intel_80286.md "wikilink")[IBM
PC/AT中开始使用](../Page/IBM_PC/AT.md "wikilink")。

## 技术资料

**8位ISA（XT）架构**

[XT_Bus_pins.svg](https://zh.wikipedia.org/wiki/File:XT_Bus_pins.svg "fig:XT_Bus_pins.svg")

|      |                                |
| ---- | ------------------------------ |
| 带宽   | [8位](../Page/8位.md "wikilink") |
| 兼容   | 8位ISA                          |
| 针脚   | 62                             |
| 工作电压 | \+5 V, -5 V, +12 V, -12 V      |
| 时钟频率 | 4.77 MHz                       |

<div style="clear: both">

</div>

**16位ISA**

[ISA_Bus_pins.svg](https://zh.wikipedia.org/wiki/File:ISA_Bus_pins.svg "fig:ISA_Bus_pins.svg")

|      |                                        |
| ---- | -------------------------------------- |
| 带宽   | [16-bit](../Page/16-bit.md "wikilink") |
| 兼容   | 8 bit ISA, 16 bit ISA                  |
| 针脚   | 98                                     |
| 工作电压 | \+5 V, -5 V, +12 V, -12 V              |
| 时钟频率 | 8.33 MHz                               |

## 当前应用

除了一些特殊工业使用以外，ISA已经不再使用了，而且现在的[主板都不带ISA接口](../Page/主板.md "wikilink")。甚至在一些设备要用上ISA时，系统生产商也不对消费者提及“ISA总线”这个被遗忘的术语，而称呼它为“**旧式总线（Legacy
Bus）**”。

尽管ISA已经几乎没人使用了，但以它为基础的其他总线依然被应用。[PC/104](../Page/PC/104.md "wikilink")，一种派生自ISA的扩展接口，目前仍被用于工业和嵌入式系统，这种接口利用与ISA相同的信号传输线连接不同的连接器。[LPC总线在现在的一些主板上取代ISA总线](../Page/LPC总线.md "wikilink")，连接一些老式的I/O设备；尽管物理层上与传统的ISA有区别，但是一般软件都会把LPC看成是ISA，因此一些ISA的缺陷依然存在，比如16MB的DMA寻址极限。

## 参见

  - [EISA](../Page/EISA.md "wikilink")
  - [MCA](../Page/Micro_Channel_architecture.md "wikilink")
  - [NuBus](../Page/NuBus.md "wikilink")
  - [VLB](../Page/VLB.md "wikilink")
  - [PCI](../Page/外设组件互连标准.md "wikilink")
  - [AGP](../Page/AGP.md "wikilink")
  - [PCI-E](../Page/PCI_Express.md "wikilink")
  - [Amiga Zorro II](../Page/Amiga_Zorro_II.md "wikilink")
  - [PC/104](../Page/PC/104.md "wikilink")
  - [LPC](../Page/LPC匯流排.md "wikilink")
  - [Switched fabric](../Page/Switched_fabric.md "wikilink")
  - [電腦裝置頻寬列表](../Page/電腦裝置頻寬列表.md "wikilink")
  - [CompactPCI](../Page/CompactPCI.md "wikilink")
  - [PC卡](../Page/PC卡.md "wikilink")
  - [USB](../Page/USB.md "wikilink")
  - [Legacy port](../Page/Legacy_port.md "wikilink")
  - [Backplane](../Page/Backplane.md "wikilink")

[Category:计算机总线](../Category/计算机总线.md "wikilink")
[Category:IBM_PC兼容机](../Category/IBM_PC兼容机.md "wikilink")
[Category:已被淘汰的電腦硬體](../Category/已被淘汰的電腦硬體.md "wikilink")