[Young_girl_dressed_in_soldier_attire_at_the_arrival_of_the_press_plane_in_Peking,_China_-_NARA_-_194411.tif](https://zh.wikipedia.org/wiki/File:Young_girl_dressed_in_soldier_attire_at_the_arrival_of_the_press_plane_in_Peking,_China_-_NARA_-_194411.tif "fig:Young_girl_dressed_in_soldier_attire_at_the_arrival_of_the_press_plane_in_Peking,_China_-_NARA_-_194411.tif")
**工农兵**是**[工人](../Page/工人.md "wikilink")**、**[农民](../Page/农民.md "wikilink")**和**[士兵](../Page/士兵.md "wikilink")**三个词的合称。

## 使用的历史

此词早在[大革命时期即已经出现](../Page/大革命时期.md "wikilink")，比如利用[日本](../Page/日本.md "wikilink")[学堂乐歌](../Page/学堂乐歌.md "wikilink")《[学生宿舍的旧吊桶](../Page/学生宿舍的旧吊桶.md "wikilink")》重新填词的著名的革命歌曲《[工农兵联合起来](../Page/工农兵联合起来.md "wikilink")》，其首句即为：“工农兵联合起来！”

大革命失败后，[中国共产党在中国各地进行武装割据](../Page/中国共产党.md "wikilink")，成立[苏区](../Page/苏区.md "wikilink")，“工农兵”一词被频繁使用，如“工农兵[苏维埃](../Page/苏维埃.md "wikilink")”、“工农兵代表大会”等在各地出现。该词在[中华民国国内其他地区也被](../Page/中华民国.md "wikilink")[左派人士使用](../Page/左派.md "wikilink")，有时还加上学生、商人，形成“[工农兵学商](../Page/工农兵学商.md "wikilink")”这样的简称。如[诗人](../Page/诗人.md "wikilink")[周钢鸣和](../Page/周钢鸣.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")[孙慎](../Page/孙慎.md "wikilink")1936年初创作的《[救亡进行曲](../Page/救亡进行曲.md "wikilink")》，其首句即为：“工农兵学商，
一齐来救亡。”

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，1950年代至1970年代多次的[政治运动中](../Page/政治运动.md "wikilink")，这三个阶层被认为是最彻底的[无产阶级](../Page/无产阶级.md "wikilink")，也是[社会主义国家最具有](../Page/社会主义国家.md "wikilink")“[革命先进性](../Page/革命.md "wikilink")”的阶层。在[政治运动的](../Page/政治运动.md "wikilink")[宣传中](../Page/宣传.md "wikilink")，工农兵常作为正面褒扬的角色出现，和[地富反坏右相对立](../Page/地富反坏右.md "wikilink")，因而成为习惯用语。但是在「[文化大革命](../Page/文化大革命.md "wikilink")」后，由于中华人民共和国国内大规模政治运动逐渐停止，[阶级斗争不再被提及](../Page/阶级斗争.md "wikilink")，整个国家转向经济建设，故该词使用频率也大幅降低。

## 工農兵學員

[POWs_captived_by_Chinese_Communist_troops(1946).jpg](https://zh.wikipedia.org/wiki/File:POWs_captived_by_Chinese_Communist_troops\(1946\).jpg "fig:POWs_captived_by_Chinese_Communist_troops(1946).jpg")時期俘虜國軍的工农兵\]\]
「文化大革命」時期，在[毛澤東的指示下](../Page/毛澤東.md "wikilink")，[北京大學](../Page/北京大學.md "wikilink")、[清華大學等](../Page/清华大学_\(北京\).md "wikilink")[高等院校專門招收具有](../Page/高等院校.md "wikilink")[中學以上教育程度的工農兵子女入學](../Page/中學.md "wikilink")，該批學生後來被稱為「工農兵學員」。

## 当代工农兵形象的使用

由于**工农兵**一词及带有其形象的宣传画已经成为[中华人民共和国](../Page/中华人民共和国.md "wikilink")1950年代至1970年代这一历史时期的象征和标志物之一，故其在当代[商业等方面的使用也引起了大众和媒体对当今社会的道德观念等问题给予关注并进行探讨](../Page/商业.md "wikilink")。

比如2007年5月8日《[北京晨報](../Page/北京晨報.md "wikilink")》報導，[北京市](../Page/北京市.md "wikilink")[大兴区](../Page/大兴区.md "wikilink")[麋鹿苑有一張貼在](../Page/麋鹿苑.md "wikilink")[公共廁所牆上的](../Page/公共廁所.md "wikilink")[宣传画](../Page/宣传画.md "wikilink")，由三名濃眉大眼、精神十足的工農兵緊握拳頭，喊著一語雙關的口號：「同志們……-{沖}-（-{衝}-）啊！」藉此鼓勵民眾在如廁之後記得沖水。\[1\]

还有的商家利用工农兵形象作为商业广告。如苏州一茶楼广告、\[2\]北京某药店宣传单\[3\]等即利用工农兵的形象并引起了争议。

## 参考文献

## 参见

  - [苏维埃](../Page/苏维埃.md "wikilink")
  - [中国民兵](../Page/中国民兵.md "wikilink")
  - [後備軍事動員](../Page/後備軍事動員.md "wikilink")

[Category:文革時期特定人群稱謂](../Category/文革時期特定人群稱謂.md "wikilink")
[Category:中国人物合称](../Category/中国人物合称.md "wikilink")

1.  [北京公厕工农兵宣传画提醒便后冲水引争议，北京晨报2007年5月8日](http://news.163.com/07/0508/01/3DUDPJ2N00011229.html)
2.  [李俊锋，苏州一茶楼广告恶搞工农兵形象，人民网2008年7月29日](http://pic.people.com.cn/GB/7575591.html)
3.  [药店宣传单恶搞工农兵形象引争议，北京晨报 2007年12月24日](http://news.cctv.com/society/20071224/103593.shtml)