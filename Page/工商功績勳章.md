**工商功績勳章**（）是[澳門勳章、獎章和獎狀制度第二級別的榮譽](../Page/澳門勳章、獎章和獎狀制度.md "wikilink")，用以獎勵在工商業有卓越和傑出表現的人士或實體，又或對工商業發展作出貢獻的人士或實體。

## 授勳名單

  - 2001年

<!-- end list -->

  - [林金城](../Page/林金城.md "wikilink")
  - [梁維特](../Page/梁維特.md "wikilink")
  - [賀一誠](../Page/賀一誠.md "wikilink")

<!-- end list -->

  - 2002年

<!-- end list -->

  - [何華添](../Page/何華添.md "wikilink")
  - [林潤垣](../Page/林潤垣.md "wikilink")
  - [馮志強](../Page/馮志強_\(澳門\).md "wikilink")
  - [澳門中華總商會](../Page/澳門中華總商會.md "wikilink")

<!-- end list -->

  - 2003年

<!-- end list -->

  - [張立群](../Page/張立群_\(澳門\).md "wikilink")
  - [馮嘉鍌](../Page/馮嘉鍌.md "wikilink")
  - [黃國勝](../Page/黃國勝.md "wikilink")
  - [蕭志偉](../Page/蕭志偉.md "wikilink")

<!-- end list -->

  - 2004年

<!-- end list -->

  - [賀定一](../Page/賀定一.md "wikilink")
  - [黃志成](../Page/黃志成.md "wikilink")
  - [劉永誠](../Page/劉永誠.md "wikilink")

<!-- end list -->

  - 2005年

<!-- end list -->

  - [何桂鈴](../Page/何桂鈴.md "wikilink")
  - [冼志耀](../Page/冼志耀.md "wikilink")
  - [崔煜林](../Page/崔煜林.md "wikilink")
  - [蕭婉儀](../Page/蕭婉儀.md "wikilink")

<!-- end list -->

  - 2006年

<!-- end list -->

  - 山度士
  - [馬有禮](../Page/馬有禮.md "wikilink")
  - [莫均益](../Page/莫均益.md "wikilink")
  - [蕭婉儀](../Page/蕭婉儀.md "wikilink")

<!-- end list -->

  - 2007年

<!-- end list -->

  - [王守基](../Page/王守基.md "wikilink")
  - [高開賢](../Page/高開賢.md "wikilink")
  - [蘇鈺龍](../Page/蘇鈺龍.md "wikilink")

<!-- end list -->

  - 2008年

<!-- end list -->

  - [曾志揮](../Page/曾志揮.md "wikilink")
  - [劉藝良](../Page/劉藝良.md "wikilink")
  - [黎振強](../Page/黎振強.md "wikilink")

<!-- end list -->

  - 2009年

<!-- end list -->

  - [何富強](../Page/何富強.md "wikilink")
  - [余健楚](../Page/余健楚.md "wikilink")
  - [吳皆妍](../Page/吳皆妍.md "wikilink")
  - [徐偉坤](../Page/徐偉坤.md "wikilink")
  - [梁安琪](../Page/梁安琪_\(澳門\).md "wikilink")

<!-- end list -->

  - 2010年

<!-- end list -->

  - [王彬成](../Page/王彬成.md "wikilink")
  - [勞灼榮](../Page/勞灼榮.md "wikilink")
  - [鄧君明](../Page/鄧君明.md "wikilink")
  - [中國銀行股份有限公司澳門分行](../Page/中國銀行股份有限公司澳門分行.md "wikilink")

<!-- end list -->

  - 2011年

<!-- end list -->

  - [馬志毅](../Page/馬志毅.md "wikilink")
  - [梁蔭沖](../Page/梁蔭沖.md "wikilink")
  - [大西洋銀行股份有限公司](../Page/大西洋銀行股份有限公司.md "wikilink")

<!-- end list -->

  - 2012年

<!-- end list -->

  - [鄭志強](../Page/鄭志強_\(澳門\).md "wikilink")
  - [何佩芬](../Page/何佩芬.md "wikilink")
  - [謝思訓](../Page/謝思訓.md "wikilink")
  - [曾旭明](../Page/曾旭明.md "wikilink")

<!-- end list -->

  - 2013年

<!-- end list -->

  - [張樂田](../Page/張樂田.md "wikilink")
  - [李子豐](../Page/李子豐.md "wikilink")
  - [司徒荻林](../Page/司徒荻林.md "wikilink")

<!-- end list -->

  - 2014年

<!-- end list -->

  - [吳在權](../Page/吳在權.md "wikilink")
  - [葉啟明](../Page/葉啟明.md "wikilink")
  - [袁松山](../Page/袁松山.md "wikilink")
  - [何敬麟](../Page/何敬麟.md "wikilink")

<!-- end list -->

  - 2015年

<!-- end list -->

  - [何　慶](../Page/何慶.md "wikilink")
  - [澳門銀行公會](../Page/澳門銀行公會.md "wikilink")
  - [南光（集團）有限公司](../Page/南光集團.md "wikilink")
  - [何榮祥](../Page/何榮祥.md "wikilink")

<!-- end list -->

  - 2016年

<!-- end list -->

  - [何玉棠](../Page/何玉棠.md "wikilink")
  - [羅德禮繼承有限公司](../Page/羅德禮繼承有限公司.md "wikilink")

<!-- end list -->

  - 2017年

<!-- end list -->

  - [大豐銀行股份有限公司](../Page/大豐銀行.md "wikilink")
  - [岐關車路有限公司](../Page/岐關車路有限公司.md "wikilink")
  - [馮信堅](../Page/馮信堅.md "wikilink")

<!-- end list -->

  - 2018年

<!-- end list -->

  - [莫志偉](../Page/莫志偉.md "wikilink")
  - [葉紹文](../Page/葉紹文.md "wikilink")
  - [黃健中](../Page/黃健中.md "wikilink")

[Category:澳門頒授勳章、獎章和獎狀制度](../Category/澳門頒授勳章、獎章和獎狀制度.md "wikilink")
[Category:2001年建立的獎項](../Category/2001年建立的獎項.md "wikilink")