[Wolfsburg_VW-Werk.jpg](https://zh.wikipedia.org/wiki/File:Wolfsburg_VW-Werk.jpg "fig:Wolfsburg_VW-Werk.jpg")
[Airacobra_P39_Assembly_LOC_02902u.jpg](https://zh.wikipedia.org/wiki/File:Airacobra_P39_Assembly_LOC_02902u.jpg "fig:Airacobra_P39_Assembly_LOC_02902u.jpg")
[Adolf_Friedrich_Erdmann_von_Menzel_021.jpg](https://zh.wikipedia.org/wiki/File:Adolf_Friedrich_Erdmann_von_Menzel_021.jpg "fig:Adolf_Friedrich_Erdmann_von_Menzel_021.jpg")所畫[工業革命初期的工廠油畫](../Page/工業革命.md "wikilink")\]\]

[City_Factory.jpg](https://zh.wikipedia.org/wiki/File:City_Factory.jpg "fig:City_Factory.jpg")的多層工廠\]\]

**工廠**（有時又作**工場**）又稱**製造廠**，是一所用以生產[貨物的大型](../Page/貨物.md "wikilink")[工業樓宇](../Page/工業.md "wikilink")。大部分工廠皆設有以大型機器或設備構成的[生產線](../Page/生產線.md "wikilink")。

## 早期的工廠

### 中國

普遍相信古代中国首先出现工厂。据《[周礼](../Page/周礼.md "wikilink")》记载官营和民营的作坊、工场、小型工厂在周朝（771-221
BC）就出现了。到宋代（960-1279
AD），商品流通进一步发展，甚至出现了世界最早的[纸币](../Page/纸币.md "wikilink")。为适应商品市场的需要，出现了更多的民营小型工厂，如[瓷器](../Page/瓷器.md "wikilink")、[冶炼](../Page/冶炼.md "wikilink")、[造纸](../Page/造纸.md "wikilink")、[印刷](../Page/印刷.md "wikilink")、[造船等专业工厂](../Page/造船.md "wikilink")。而宋朝官方也官营的[兵器工厂](../Page/兵器工厂.md "wikilink")，如北宋初期的[弓弩院](../Page/弓弩院.md "wikilink")、[造箭院各有工匠一千多人](../Page/造箭院.md "wikilink")。

### 阿拉伯

### 歐洲

歐洲史上雖然在[古羅馬時代已經出現類似工廠的組織](../Page/古羅馬.md "wikilink")，但是真正出現工廠這個英文單词的时间是西元1104年，地点是[意大利](../Page/意大利.md "wikilink")[威尼斯的](../Page/威尼斯.md "wikilink")[造船廠](../Page/造船廠.md "wikilink")。工廠出現的時間比[工業革命早上了數百年](../Page/工業革命.md "wikilink")。在造船廠中船隻以[裝配線的形式在生產線上由組件裝嵌而成](../Page/裝配線.md "wikilink")。一隻船需用約一天建造，而工廠全盛時期曾有超過16,000名員工。

[英國](../Page/英國.md "wikilink")[伯明翰的](../Page/伯明翰.md "wikilink")[蘇豪製造廠](../Page/蘇豪製造廠.md "wikilink")（Soho
Manufactory，1761至1990年）被公認為全球首座現代工廠。

## 工廠發展

最初的工廠（例如在18世紀建於[英國](../Page/英國.md "wikilink")[殖民地的工廠](../Page/殖民地.md "wikilink")）並沒有大型的自動化機器。那時的工廠純粹是讓一大群從事[手工業](../Page/手工業.md "wikilink")（如[紡織業](../Page/紡織業.md "wikilink")）的[工人聚集起來](../Page/工人.md "wikilink")，一起進行生產。這種做法令工序易於管理，而[原料也能更有效地分配](../Page/原料.md "wikilink")。

直到動力織布機和[蒸汽機等機械發明後](../Page/蒸汽機.md "wikilink")，開始出現以機器生產的工廠。機械和可替換的零件使生產較有效率和減少廢品。工厂的大量出现，是[工业革命的特征](../Page/工业革命.md "wikilink")。

[亨利·福特在](../Page/亨利·福特.md "wikilink")20世紀初創立了[流水線生產模式](../Page/量產.md "wikilink")，使工廠的發展又向前邁進了一步。這種模式的特點是每組工人只負責某產品生產的一項工序。它大大地減低了生產成本，且為後來的[消費時代奠下了基礎](../Page/消費時代.md "wikilink")。

20世紀中後期，兩項新的技術為工廠的發展提供了新的出路：

  - 先進[統計學原理及方法來達成產品的](../Page/統計學.md "wikilink")[品質管理](../Page/品質管理.md "wikilink")。該技術進一步提高了生產的成本效率以及產品質量，此理論最先由美國人[愛德華茲·戴明提出](../Page/愛德華茲·戴明.md "wikilink")，但是並未在本國受到重視，而是在他協助重建的[日本開始推行](../Page/日本.md "wikilink")，日本戰後重建的成功經驗讓世界各國開始重視此理論的運用，並引導出[成本-效果比較研究這門學問](../Page/成本-效果比較研究.md "wikilink")。
  - 1970年代後[機器人的廣泛使用](../Page/機器人.md "wikilink")。電腦控制的機械能快捷且準確地執行一些簡單的工序，有效減低人為錯誤帶來的影響，除此之外機器可全天候運作更加強了工廠生產效率以及勞工成本。

一些人推測未來的工廠將包括[快速成型技術及](../Page/快速成型技術.md "wikilink")[納米科技甚至是地球](../Page/納米科技.md "wikilink")[軌道上之](../Page/軌道.md "wikilink")[零重力設施](../Page/零重力.md "wikilink")。

## 工廠選址

在[大眾運輸普及之前](../Page/大眾運輸.md "wikilink")，工廠需要設置於工人密集之處。故早期的工廠多設於[城市之中](../Page/城市.md "wikilink")，甚至有隨著工廠建設而產生城市發展的事。工廠之間亦傾向於互相集結──很多時候一所工廠的成品或[廢物能成為另一所工廠的](../Page/廢物.md "wikilink")[原料](../Page/原料.md "wikilink")。

後來，[運河和](../Page/運河.md "wikilink")[鐵路等運輸網絡隨著工廠的繁榮而不斷擴大](../Page/鐵路.md "wikilink")，工廠亦開始建於就近廉價[能源](../Page/能源.md "wikilink")、[原料或](../Page/原料.md "wikilink")[市場之地](../Page/市場.md "wikilink")。運輸網絡完善也是工廠選址的考量之一：曾有工廠建於四野無人但交通連接方便之地，結果成功發展並獲得豐厚利潤。

由於工廠一般會產生[污染](../Page/污染.md "wikilink")，近年很多工廠都改建於經專門規劃的[市郊地區](../Page/市郊.md "wikilink")，工人則於[住宅區及工廠之間](../Page/住宅區.md "wikilink")[通勤](../Page/通勤.md "wikilink")。

[全球化使工廠選址更具彈性](../Page/全球化.md "wikilink")。一些[發展中國家](../Page/發展中國家.md "wikilink")（如[中國](../Page/中國.md "wikilink")）便成立[經濟特區](../Page/經濟特區.md "wikilink")，在區內發展工廠設施以振興工業。[外判等理念亦使在極少工業化的地區開設工廠變得更為可行](../Page/外判.md "wikilink")。

## 工廠管理

工廠的生產過程對[管理技術的需求非常高](../Page/管理.md "wikilink")，使很多管理理論產生出來。一般而言，工廠管理涉及將低、中、高技術工人以及主管、經理等人士作分級管理。

## 參閱

  - [工業工程](../Page/工業工程.md "wikilink")
  - [生產管理](../Page/生產管理.md "wikilink")
  - [工業園區](../Page/工業園區.md "wikilink")
  - [工業革命](../Page/工業革命.md "wikilink")
  - [製造業](../Page/製造業.md "wikilink")
  - [自動化](../Page/自動化.md "wikilink")
  - [勞工](../Page/勞工.md "wikilink")
  - [家庭代工](../Page/家庭代工.md "wikilink")

[Category:产业](../Category/产业.md "wikilink")
[Category:工业建筑](../Category/工业建筑.md "wikilink")