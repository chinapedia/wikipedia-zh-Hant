공조 |hanja=工部{{\`}}工曺 |rr=Gongbu{{\`}}Gongjo |mr=Kongpu{{\`}}Kongcho
|koreantext= |vietnamesetitle= |vietnamese=Bộ Công{{\`}}Công bộ |chunom=
|hantu= |hannom=部工{{\`}}工部 |vietnamesetext= |mongoltitle= |mongol=
|kiril= |mishi= |monglatin= |manchutitle= |manchu= ᠸᡝᡳᠯᡝᡵᡝ ᠵᡠᡵᡤᠠᠨ
|mollendorff= weilere jurgan }}

**工部**為[中國古代](../Page/中國古代.md "wikilink")[官署名](../Page/官署.md "wikilink")，[六部之一](../Page/六部.md "wikilink")。其長官為[工部尚書](../Page/工部尚書.md "wikilink")。

受中國文化影響，[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[高麗王朝也設有工部](../Page/高麗王朝.md "wikilink")，其長官為工部尚書。至[朝鮮王朝時期](../Page/朝鮮王朝.md "wikilink")，改稱工曹，長官改稱工曹判書。受中國文化影響，阮朝設有六部，其中也有工部。

## 歷史

[周代属冬官事](../Page/周代.md "wikilink")，[秦](../Page/秦朝.md "wikilink")[汉属](../Page/汉朝.md "wikilink")[少府](../Page/少府.md "wikilink")。[曹魏自少府分置水部曹](../Page/曹魏.md "wikilink")，隶[尚书台](../Page/尚书台.md "wikilink")，掌水利工程，兼领航运之政。[晋置屯田曹](../Page/晋朝.md "wikilink")、起部曹，掌农垦（军垦由[屯田中郎将领之](../Page/屯田中郎将.md "wikilink")）和水利事业。[南北朝时期](../Page/南北朝.md "wikilink")，[南朝沿置不改](../Page/南朝.md "wikilink")，[北朝损益不定](../Page/北朝.md "wikilink")。[隋朝将前述诸曹合并置为部](../Page/隋朝.md "wikilink")，掌管各项工程、工匠、屯田、水利、交通等事，沿用[北周工部的名称](../Page/北周.md "wikilink")，列为[尚书省](../Page/尚书省.md "wikilink")[六部之一](../Page/六部.md "wikilink")。

[唐朝工部长官称工部尚书](../Page/唐朝.md "wikilink")，下置工部侍郎一人，后代相沿不改。一開始工部下屬有工部、屯田、虞部、水部四個司。工部为工部头司，掌营建之政令与工部庶务；屯田司掌天下田垦；虞部司掌山川水泽之利；水部司掌水利。各司的名稱偶有更動。惟[宋代使职盛行](../Page/宋代.md "wikilink")，工部职务为诸使所夺。[明朝初](../Page/明朝.md "wikilink")，工部下设总部、屯部、虞部和水部四属部，[洪武二十六年](../Page/洪武.md "wikilink")，改尚书二十四部为[二十四清吏司](../Page/二十四清吏司.md "wikilink")，工部各属部分别改为营缮清吏司、虞衡清吏司、都水清吏司和屯田清吏司，职掌、设官仍前。

清末“[仿行宪政](../Page/仿行宪政.md "wikilink")”后，清政府设立[农工商部](../Page/农工商部.md "wikilink")、[邮传部承接工部职责](../Page/邮传部.md "wikilink")，工部遂废。

## 参考文献

  - 《[新唐书](../Page/新唐书.md "wikilink")·百官志一》：“工部，[尚书一人](../Page/尚书.md "wikilink")，正三品；[侍郎一人](../Page/侍郎.md "wikilink")，正四品下……其属有四：一曰工部，二曰屯田，三曰虞部，四曰水部。”
  - 《[通典](../Page/通典.md "wikilink")·[职官五](../Page/职官.md "wikilink")·工部尚书》：“《[周礼](../Page/周礼.md "wikilink")·冬官》其属有[考工](../Page/考工.md "wikilink")，掌百工之事，曰国有六职，百工是其一焉……至隋乃有工部尚书，统工部、屯田二曹，盖因工部后周之名，兼前代起部之职。”

## 参见

  - [将作监](../Page/将作监.md "wikilink")、[都水监](../Page/都水监.md "wikilink")、[军器监](../Page/军器监.md "wikilink")
  - [少府](../Page/少府.md "wikilink")、[内务府](../Page/内务府.md "wikilink")

{{-}}

[6](../Category/六部.md "wikilink") [工部](../Category/工部.md "wikilink")
[Category:建设部门](../Category/建设部门.md "wikilink")
[Category:交通部门](../Category/交通部门.md "wikilink")
[Category:水利部门](../Category/水利部门.md "wikilink")
[Category:军事工业部门](../Category/军事工业部门.md "wikilink")