**工黨**（[荷蘭语](../Page/荷蘭语.md "wikilink")：Partij van de
Arbeid，缩写为**PvdA**）是[荷蘭一個](../Page/荷蘭.md "wikilink")[中間偏左](../Page/中間偏左.md "wikilink")[社會民主主義的政黨](../Page/社會民主主義.md "wikilink")。它也是[社會黨國際的成員團體](../Page/社會黨國際.md "wikilink")。

荷蘭工黨成立於1946年2月9日，由三個政黨合併而成，成立動機在克服荷蘭政治長久以來的山頭主義。工黨自建黨至今一直在議會兩院擁有較多席次。

工黨以社會民主主義為宗旨，重視社會、政治與經濟方面的平等。該黨曾於1977年的綱領中曾有將重要的工業、銀行及保險業國有化的訴求，但在2005年1月29日的新綱領中，則被「安全而有尊嚴的生活」、「人人皆能完全參與社會」等口號取代。

目前工黨是荷蘭的第二大政黨，[2012年荷蘭大選後與右翼的](../Page/2012年荷蘭大選.md "wikilink")[自由民主人民黨組建聯合政府](../Page/自由民主人民黨.md "wikilink")。

荷蘭PVDA在17年二院選舉慘敗，因為受到VVD影響導致大多數青中年人不再相信PVDA的黨策，只剩下二院的九個票席。

在地方政党和国家政党的共同努力下，工党在2018年九月的预测大选中重新赢得了人民群众的信任和理解，并让广大荷兰群众了解到了以自民党为首的右派党派不能够帮荷兰成立更好的社会路程、所以工党增加四位选票，并且华人在工党内也有突出的表现，以Alafate
Yeersheng（Vincent）为首青少年工党组织领袖开始一步一步增进华人对荷兰政坛的理解与参与性。

## 歷史

### 1945–1965

[Drees,_W._-_SFA002019221.jpg](https://zh.wikipedia.org/wiki/File:Drees,_W._-_SFA002019221.jpg "fig:Drees,_W._-_SFA002019221.jpg")
1946年2月9日，社會民主工人黨（Social Democratic Workers' Party,
SDAP）、左派自由主義少數政黨自由思想民主聯盟（Free-thinking
Democratic League,
VDB）和小型[基督教左派政黨](../Page/基督教左派.md "wikilink")[基督教民主聯盟](../Page/基督教民主聯盟_\(荷蘭\).md "wikilink")（Christian
Democratic Union,
CDU）合併成立工黨。另外還包括天主教二戰抵抗團體「Christofoor」人士、[新教政黨基督教歷史聯盟](../Page/新教.md "wikilink")（Christian
Historical Union, CHU）、反革命黨（Anti Revolutionary Party, ARP）等黨派。

工黨創黨人希望成立一個廣泛政黨，打破支柱化（pillarisation）這個歷史傳痛。他們希望建立一個稱為「突破」（Doorbraak）的新政治制度。工黨結合[社會主義理想與](../Page/社會主義.md "wikilink")[自由主義](../Page/自由主義.md "wikilink")、宗教、[人文主義思想](../Page/人文主義.md "wikilink")。然而，工黨無法打破支柱化。1948年，以前自由思想民主聯盟領袖彼得·奧德（Pieter
Oud）為首的部分自由派成員，因不滿工黨的社會主義路線而退黨。他們組成右翼[保守自由主義的](../Page/保守自由主義.md "wikilink")[自由民主人民黨](../Page/自由民主人民黨.md "wikilink")。

1946年至1958年，工黨與[基民黨](../Page/基督教民主聯盟_\(荷蘭\).md "wikilink")（Catholic
People's Party, KVP）及其他政黨組成執政聯盟。兩黨在議會取得多數。

1948年起，威廉·德里斯（Willem
Drees）出任首相。在他領導下，荷蘭自戰爭中恢復，開始建立[福利國家制度](../Page/福利國家.md "wikilink")，期間[印尼於](../Page/印尼.md "wikilink")1949年12月脫離獨立。

1958年內閣危機後，威廉·德里斯下台結束十年執政，工黨被基民黨取代。其後工黨一直擔任反對黨直至1965年。

### 1965–1989

[Joop_den_Uyl_1975.jpg](https://zh.wikipedia.org/wiki/File:Joop_den_Uyl_1975.jpg "fig:Joop_den_Uyl_1975.jpg")，1966至1986年政黨領袖\]\]
1965年，天主教人民黨、反革命黨、基督教歷史聯盟和自由民主人民黨間的衝突讓執政聯盟無法繼續合作。其中三個認信主義（Confessionalism）基督教政黨轉向工黨。四黨組成卡爾斯內閣。卡爾斯內閣內部仍衝突不斷，最後因經濟政策而垮台。

與此同時，年輕一代試圖掌控工黨。自稱新左派的年輕黨員改變了工黨。新左派希望改革工黨：他們主張應轉向[新社會運動](../Page/新社會運動.md "wikilink")，採納他們的反議會策略與議題，如女性運動、[環保和](../Page/環保.md "wikilink")[第三世界發展](../Page/第三世界.md "wikilink")。卡爾斯內閣垮台後，工黨黨大會決議不與天主教人民黨和其新教盟友執政。做為對日益壯大新左派的回應，以德里斯之子小威廉·德里斯為首的老一輩中間派黨員組成新右派。1970年，他們另外成立政黨民主社會黨'70（Democratic
Socialists '70, DS70）。

在新左派帶領下，工黨開始採取支柱化策略，以議會多數爭取入閣。為了組成內閣，工黨與[社會自由主義的](../Page/社會自由主義.md "wikilink")[民主66](../Page/民主66.md "wikilink")、進步基督教主義的激進政治黨結盟。聯盟稱為進步協議（PAK）。1971年和1972年大選，三黨承諾在激進共同政綱下組成內閣。1971年，他們未能組閣，而前工黨黨員組成的民主社會黨'70加入第一屆比舒維爾內閣。

1972年大選，無論是工黨陣營或是基民黨陣營都無法取得多數。兩陣營被迫合作。工黨領袖[約普·登厄伊爾就任首相](../Page/約普·登厄伊爾.md "wikilink")。該內閣為大中間派的超議會內閣，包括三組進步主義政黨與基民黨、反革命黨。內閣試圖激進改革政府、經濟，在任內通過多項進步主義社會改革，如大幅增加福利支出、津貼物價指數化和符合生活成本的最低工資。\[1\]\[2\]

然而，內閣依然面臨經濟衰退與個人和意識形態的衝突。其中，首相登厄伊爾與基民黨籍副首相[德里斯·范阿赫特的關係惡劣](../Page/德里斯·范阿赫特.md "wikilink")。衝突在1977年大選前爆發，內閣垮台。1977年大選，工黨取得多數。但登厄伊爾與范阿赫特的衝突使得雙方無法合作。在經歷漫長的組閣協商後，[基民黨](../Page/基督教民主黨_\(荷蘭\).md "wikilink")、三個基督教民主主義政黨（天主教人民黨、基督教歷史聯盟、反革命黨）與自由民主人民黨組成右翼聯合政府。工黨成為反對黨。

1981年大選，執政右翼基民黨為首的聯合政府失去議會多數。基民黨仍維持第一大黨地位，但被迫與工黨和民主66合作（激進政治黨因敗選退出執政聯盟），組織大聯合政府。范阿赫特出任新內閣首相，登厄伊爾就任副首相一職。惟兩人之間的衝突和意見分歧導致新內閣在成立後數個月便宣告瓦解。基民黨在1982年大選重新取得多數，並在1986年大選中維持優勢，直至1989年前一直與中右翼[民主66組織聯合政府](../Page/民主66.md "wikilink")。處於在野的工黨開始改革。1986年登厄伊爾退出政壇，指定前[工會領袖](../Page/工會.md "wikilink")[維姆·科克繼任](../Page/維姆·科克.md "wikilink")。

### 1989年至今

由於工黨放棄過去激進的左翼路線，走較為中間的道路，使工黨在1989年後對政府內閣的組成有了主導權。工黨曾多次參與聯合政府，其中最長的一段時期是1989年至2002年。工黨在此後選舉中的表現並不理想，得票數屈居[基督教民主黨之後](../Page/基督教民主黨_\(荷蘭\).md "wikilink")，此外部份票源也遭較激進，比工黨更為左傾的[社會黨瓜分](../Page/社會黨_\(荷蘭\).md "wikilink")，2007年至2010年加入[巴爾克嫩德的聯合政府](../Page/巴爾克嫩德.md "wikilink")。

1989年至2002年，2007年至2010年以及2012年至今，多次主導組建聯合政府。目前工黨是荷蘭的第二大政黨，[2012年荷蘭大選後與右翼的](../Page/2012年荷蘭大選.md "wikilink")[自由民主人民黨組建聯合政府](../Page/自由民主人民黨.md "wikilink")。

[2017年大選](../Page/2017年荷蘭大選.md "wikilink")，工黨慘敗，在[國會二院中的議席由選前的](../Page/國會二院.md "wikilink")38席跌至僅9席。

## 意識形態與議題

工黨作為[社會民主主義政黨](../Page/社會民主主義.md "wikilink")，承諾建立[福利國家](../Page/福利國家.md "wikilink")。1970年代，工黨的黨綱更為激進且增加新議題，如婦女自由、環保與第三世界發展。1990年代，該黨政綱逐漸偏向中間路線，較為和緩，包括改革福利國家制度與公營企業私有化。2005年，該黨採用新黨綱表達中間偏左的政治立場。其核心議題有[就業](../Page/就業.md "wikilink")；[社會保險和](../Page/社會保險.md "wikilink")[福利](../Page/福利.md "wikilink")；公共[教育](../Page/教育.md "wikilink")、[公共安全與](../Page/公共安全.md "wikilink")[健保的投資](../Page/健保.md "wikilink")。

## 注釋

## 外部連結

  - [Website PvdA](http://www.pvda.nl/)
  - [PvdA Europa](http://www.pvda-europa.nl/)
  - [Beginselprogramma 1977](http://www.rug.nl/dnpp/politiekePartijen/pvda/beginselProgrammas/begProg77.pdf)
  - [Beginselprogramma 2005](http://www.rug.nl/dnpp/politiekePartijen/pvda/beginselProgrammas/begProg05.pdf)

[Category:社會民主主義政黨](../Category/社會民主主義政黨.md "wikilink")
[Category:荷蘭政黨](../Category/荷蘭政黨.md "wikilink")
[Category:社會黨國際](../Category/社會黨國際.md "wikilink")
[Category:工黨](../Category/工黨.md "wikilink")
[Category:歐洲社會黨成員](../Category/歐洲社會黨成員.md "wikilink")
[Category:1946年建立的政黨](../Category/1946年建立的政黨.md "wikilink")

1.  <http://books.google.co.uk/booksid=ZIoKqEYDUUC&pg=PA59&dq=joop+den+uyl+social+reforms&hl=en#v=onepage&q&f=false>
2.  <http://books.google.co.uk/booksid=w2exQnhFNyYC&pg=PA235&dq=joop+den+uyl+reforms+social+security&hl=en#v=onepage&q=joop%20den%20uyl%20reforms%20social%20security&f=false>