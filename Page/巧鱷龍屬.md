**巧鱷龍**（[學名](../Page/學名.md "wikilink")：*Compsosuchus*）是[阿貝力龍科](../Page/阿貝力龍科.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[上白堊紀的](../Page/上白堊紀.md "wikilink")[印度](../Page/印度.md "wikilink")。巧鱷龍是由[休尼](../Page/休尼.md "wikilink")（Friedrich
von Huene）及Matley於1933年所描述、命名的。[模式種是](../Page/模式種.md "wikilink")*C.
solus*。此屬一般都被認為是[可疑名稱](../Page/可疑名稱.md "wikilink")。

## 外部連結

  - [恐龍博物館——巧鱷龍](https://web.archive.org/web/20070606115233/http://www.dinosaur.net.cn/Museum/Compsosuchus.htm)
  - [*Compsosuchus* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/de_4/5a64e41.htm) at Dino
    Russ's Lair

[Category:阿貝力龍科](../Category/阿貝力龍科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:印度與馬達加斯加恐龍](../Category/印度與馬達加斯加恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")