[Penny_Porsche_6.JPG](https://zh.wikipedia.org/wiki/File:Penny_Porsche_6.JPG "fig:Penny_Porsche_6.JPG")[Charcoal_drawing_large_breasted_woman.jpg](https://zh.wikipedia.org/wiki/File:Charcoal_drawing_large_breasted_woman.jpg "fig:Charcoal_drawing_large_breasted_woman.jpg")[素描](../Page/素描.md "wikilink")\]\]
**巨乳**又稱**豪乳**、**爆乳**、**大奶**、**巨奶**、**大波**、**波霸**、**海咪咪**、**大咪咪**，用來形容擁有巨大的[乳房的](../Page/乳房.md "wikilink")[女性](../Page/女性.md "wikilink")。

## 概述

對於乳房或罩杯必須要豐滿到什麼程度才能稱之為巨乳，並沒有一個絕對的定義。但凡其乳房大於同年齡、同族群的平均值者，都可以在相對上視為巨乳。以日本My
Navi Woman所做的調查，有34%的人將E罩杯以上視為巨乳、33%的人視D罩杯以上為巨乳\[1\]。

## 生理學角度

過大的乳房除了對女性造成不便之外（例如[肩膀酸痛](../Page/肩.md "wikilink")），還可能對生命構成危險。這有可能就是所謂的“[巨乳症](../Page/巨乳症.md "wikilink")”。因為過大的乳房會造成[呼吸困難](../Page/呼吸困難.md "wikilink")。不過一般可以通过[縮胸手術](../Page/縮胸.md "wikilink")，從而減低[壓力](../Page/壓力.md "wikilink")。

## 文化

部分[男性對巨乳有所](../Page/男性.md "wikilink")[情結](../Page/情結.md "wikilink")（可稱為「巨乳控」或「巨乳星人」），所以亦有女性利用[隆胸手術而造成巨乳](../Page/隆胸.md "wikilink")。而傳媒亦產生「[童顏巨乳](../Page/童顏巨乳.md "wikilink")」一詞，該詞彙源自[日本](../Page/日本.md "wikilink")[動漫社群](../Page/動漫.md "wikilink")，指擁有[娃娃臉但乳房巨大的年輕女性](../Page/娃娃臉.md "wikilink")，與中文環境所稱的「天使臉孔，魔鬼身材」類似。近年來，許多[日本漫畫常以童顏巨乳的](../Page/日本漫畫.md "wikilink")[美少女作為賣點](../Page/美少女.md "wikilink")，吸引年輕一代的讀者。

## 注釋

## 參見

  - [平胸](../Page/平胸.md "wikilink")
  - [人工美女](../Page/人工美女.md "wikilink")
  - [乳交](../Page/乳交.md "wikilink")
  - [隆胸](../Page/隆胸.md "wikilink")
  - [恋乳](../Page/恋乳.md "wikilink")
  - [乳搖](../Page/乳搖.md "wikilink")
  - [波霸餐廳](../Page/波霸餐廳.md "wikilink")
  - [巨乳症](../Page/巨乳症.md "wikilink")

[日](../Category/流行語.md "wikilink") [女](../Category/性俗語.md "wikilink")
[\*](../Category/乳房.md "wikilink") [B](../Category/萌属性.md "wikilink")
[日](../Category/戀胸.md "wikilink") [姿](../Category/色情.md "wikilink")

1.  [巨乳だと感じるブラジャーのカップ「Eカップ　34.1%」](http://woman.mynavi.jp/article/140126-17/)
    ，My Navi Woman。