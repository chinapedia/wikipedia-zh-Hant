**巨口魚科**（[学名](../Page/学名.md "wikilink")：），是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[巨口魚目的其中一科](../Page/巨口魚目.md "wikilink")。

## 分類

**巨口魚科**下分27個屬，如下：

## 星衫魚亞科（Astronesthinae）

### 星衫魚屬（*Astronesthes*）

  - [大西洋星衫魚](../Page/大西洋星衫魚.md "wikilink")（*Astronesthes atlanticus*）
  - [雙葉星衫魚](../Page/雙葉星衫魚.md "wikilink")（*Astronesthes bilobatus*）
  - [鮑氏星衫魚](../Page/鮑氏星衫魚.md "wikilink")（*Astronesthes boulengeri*）
  - [尾燈星衫魚](../Page/尾燈星衫魚.md "wikilink")（*Astronesthes caulophorus*）
  - [金星衫魚](../Page/金星衫魚.md "wikilink")（*Astronesthes chrysophekadion*）
  - [藍黑星衫魚](../Page/藍黑星衫魚.md "wikilink")（*Astronesthes cyaneus*）
  - （*Astronesthes decoratus*）
  - （*Astronesthes dupliglandis*）
  - （*Astronesthes exsul*）
  - [費氏星衫魚](../Page/費氏星衫魚.md "wikilink")（*Astronesthes fedorovi*）
  - [台灣星衫魚](../Page/台灣星衫魚.md "wikilink")（*Astronesthes formosana*）
  - （*Astronesthes galapagensis*）
  - [大西洋星衫魚](../Page/大西洋星衫魚.md "wikilink")（*Astronesthes gemmifer*）
  - [吉布斯氏星衫魚](../Page/吉布斯氏星衫魚.md "wikilink")（*Astronesthes gibbsi*）
  - （*Astronesthes gudrunae*）
  - （*Astronesthes haplophos*）
  - （*Astronesthes ijimai*）
  - （*Astronesthes illuminatus*）
  - [印度星衫魚](../Page/印度星衫魚.md "wikilink")（*Astronesthes indicus*）
  - [印度太平洋星衫魚](../Page/印度太平洋星衫魚.md "wikilink")（*Astronesthes
    indopacifica*）
  - （*Astronesthes karsteni*）
  - [克氏星衫魚](../Page/克氏星衫魚.md "wikilink")（*Astronesthes kreffti*）
  - （*Astronesthes lamellosus*）
  - （*Astronesthes lampara*）
  - [白鬚星衫魚](../Page/白鬚星衫魚.md "wikilink")（*Astronesthes leucopogon*）
  - [亮頰星衫魚](../Page/亮頰星衫魚.md "wikilink")（*Astronesthes lucibucca*）
  - [螢光星衫魚](../Page/螢光星衫魚.md "wikilink")（*Astronesthes lucifer*）
  - （*Astronesthes luetkeni*）
  - [狼星衫魚](../Page/狼星衫魚.md "wikilink")（*Astronesthes lupina*）
  - [大鬚星衫魚](../Page/大鬚星衫魚.md "wikilink")（*Astronesthes macropogon*）
  - [馬丁星衫魚](../Page/馬丁星衫魚.md "wikilink")（*Astronesthes martensii*）
  - [小鬚星衫魚](../Page/小鬚星衫魚.md "wikilink")（*Astronesthes micropogon*）
  - [新鬚星衫魚](../Page/新鬚星衫魚.md "wikilink")（*Astronesthes neopogon*）
  - [黑星衫魚](../Page/黑星衫魚.md "wikilink")（*Astronesthes niger*）
  - [淺黑星衫魚](../Page/淺黑星衫魚.md "wikilink")（*Astronesthes nigroides*）
  - （*Astronesthes oligoa*）
  - [冷星衫魚](../Page/冷星衫魚.md "wikilink")（*Astronesthes psychrolutes*）
  - [太平洋星衫魚](../Page/太平洋星衫魚.md "wikilink")（*Astronesthes quasiindicus*）
  - [李氏星衫魚](../Page/李氏星衫魚.md "wikilink")（*Astronesthes richardsoni*）
  - [似星衫魚](../Page/似星衫魚.md "wikilink")（*Astronesthes similus*）
  - （*Astronesthes spatulifer*）
  - [絲球星衫魚](../Page/絲球星衫魚.md "wikilink")（*Astronesthes splendidus*）
  - （*Astronesthes tanibe*）
  - （*Astronesthes tatyanae*）
  - [朱氏星衫魚](../Page/朱氏星衫魚.md "wikilink")（*Astronesthes tchuvasovi*）
  - [三絲星衫魚](../Page/三絲星衫魚.md "wikilink")（*Astronesthes trifibulatus*）
  - [澤氏星衫魚](../Page/澤氏星衫魚.md "wikilink")（*Astronesthes zetgibbsi*）
  - （*Astronesthes zharovi*）

### 掠食巨口魚屬（*Borostomias*）

  - [北大西洋掠食巨口魚](../Page/北大西洋掠食巨口魚.md "wikilink")（*Borostomias
    abyssorum*）
  - [南極掠食巨口魚](../Page/南極掠食巨口魚.md "wikilink")（*Borostomias antarcticus*）
  - [掠食巨口魚](../Page/掠食巨口魚.md "wikilink")（*Borostomias elucens*）
  - [單絲掠食巨口魚](../Page/單絲掠食巨口魚.md "wikilink")（*Borostomias mononema*）
  - [太平洋掠食巨口魚](../Page/太平洋掠食巨口魚.md "wikilink")（*Borostomias pacificus*）
  - [巴拿馬掠食巨口魚](../Page/巴拿馬掠食巨口魚.md "wikilink")（*Borostomias panamensis*）

### 真芒巨口魚屬（*Eupogonesthes*）

  - [真芒巨口魚](../Page/真芒巨口魚.md "wikilink")（*Eupogonesthes xenicus*）

### 異星杉魚屬（*Heterophotus*）

  - [蛇口異星杉魚](../Page/蛇口異星杉魚.md "wikilink")（*Heterophotus ophistoma*）

### 新衫魚屬（*Neonesthes*）

  - [新衫魚](../Page/新衫魚.md "wikilink")（*Neonesthes capensis*）
  - [小頭新衫魚](../Page/小頭新衫魚.md "wikilink")（*Neonesthes microcephalus*）

### 細杉魚屬（*Rhadinesthes*）

  - [細杉魚](../Page/細杉魚.md "wikilink")（*Rhadinesthes decimus*）

## 蝰魚亞科（Chauliodontinae）

### 蝰魚屬（*Chauliodus*）

  - [鬚蝰魚](../Page/鬚蝰魚.md "wikilink")（*Chauliodus barbatus*）
  - [達納蝰魚](../Page/達納蝰魚.md "wikilink")（*Chauliodus danae*）
  - （*Chauliodus dentatus*）
  - [馬康氏蝰魚](../Page/馬康氏蝰魚.md "wikilink")（*Chauliodus macouni*）
  - [長牙蝰魚](../Page/長牙蝰魚.md "wikilink")（*Chauliodus minimus*）
  - [黑蝰魚](../Page/黑蝰魚.md "wikilink")（*Chauliodus pammelas*）
  - [施氏蝰魚](../Page/施氏蝰魚.md "wikilink")（*Chauliodus schmidti*）
  - [蝰魚](../Page/蝰魚.md "wikilink")（*Chauliodus sloani*）
  - [瓦氏蝰魚](../Page/瓦氏蝰魚.md "wikilink")（*Chauliodus vasnetzovi*）

## 奇棘魚亞科（Idiacanthinae）

### 奇棘魚屬（*Idiacanthus*）

  - [穴口奇棘魚](../Page/穴口奇棘魚.md "wikilink")（*Idiacanthus antrostomus*）
  - [大西洋奇棘魚](../Page/大西洋奇棘魚.md "wikilink")（*Idiacanthus atlanticus*）
  - [奇棘魚](../Page/奇棘魚.md "wikilink")（*Idiacanthus fasciola*）

## 柔骨魚亞科（Malacosteinae）

### 奇巨口魚屬（*Aristostomias*）

  - [格氏奇巨口魚](../Page/格氏奇巨口魚.md "wikilink")（*Aristostomias grimaldii*）
  - [黑奇巨口魚](../Page/黑奇巨口魚.md "wikilink")（*Aristostomias lunifer*）
  - [多指奇巨口魚](../Page/多指奇巨口魚.md "wikilink")（*Aristostomias polydactylus*）
  - [閃亮奇巨口魚](../Page/閃亮奇巨口魚.md "wikilink")（*Aristostomias scintillans*）
  - [蒂氏奇巨口魚](../Page/蒂氏奇巨口魚.md "wikilink")（*Aristostomias tittmanni*）
  - [異域奇巨口魚](../Page/異域奇巨口魚.md "wikilink")（*Aristostomias xenostoma*）

### 柔骨魚屬（*Malacosteus*）

  - （*Malacosteus australis*）
  - [黑柔骨魚](../Page/黑柔骨魚.md "wikilink")（*Malacosteus niger*）

### 光巨口魚屬（*Photostomias*）

  - （*Photostomias atrox*）
  - （*Photostomias goodyeari*）
  - [格氏光巨口鱼](../Page/格氏光巨口鱼.md "wikilink")（*Photostomias guernei*）
  - （*Photostomias liemi*）
  - （*Photostomias lucingens*）
  - （*Photostomias tantillux*）

## 黑巨口魚亞科（Melanostomiinae）

### 深巨口魚屬（*Bathophilus*）

  - [無鬚深巨口魚](../Page/無鬚深巨口魚.md "wikilink")（*Bathophilus abarbatus*）
  - （*Bathophilus altipinnis*）
  - [黑體深巨口魚](../Page/黑體深巨口魚.md "wikilink")（*Bathophilus ater*）
  - [高腹深巨口魚](../Page/高腹深巨口魚.md "wikilink")（*Bathophilus brevis*）
  - [岐鰭深巨口魚](../Page/岐鰭深巨口魚.md "wikilink")（*Bathophilus digitatus*）
  - [長絲深巨口魚](../Page/長絲深巨口魚.md "wikilink")（*Bathophilus filifer*）
  - [弗氏深巨口魚](../Page/弗氏深巨口魚.md "wikilink")（*Bathophilus flemingi*）
  - [印度深巨口魚](../Page/印度深巨口魚.md "wikilink")（*Bathophilus indicus*）
  - [畸深巨口魚](../Page/畸深巨口魚.md "wikilink")（*Bathophilus irregularis*）
  - [四絲深巨口魚](../Page/四絲深巨口魚.md "wikilink")（*Bathophilus kingi*）
  - [長羽深巨口魚](../Page/長羽深巨口魚.md "wikilink")（*Bathophilus longipinnis*）
  - [絲鬚深巨口魚](../Page/絲鬚深巨口魚.md "wikilink")（*Bathophilus nigerrimus*）
  - [少鰭深巨口魚](../Page/少鰭深巨口魚.md "wikilink")（*Bathophilus pawneei*）
  - （*Bathophilus proximus*）
  - [裂鰭深巨口魚](../Page/裂鰭深巨口魚.md "wikilink")（*Bathophilus schizochirus*）
  - [維氏深巨口魚](../Page/維氏深巨口魚.md "wikilink")（*Bathophilus vaillanti*）

### 肢巨口魚屬（*Chirostomias*）

  - [肢巨口魚](../Page/肢巨口魚.md "wikilink")（*Chirostomias pliopterus*）

### 刺巨口魚屬（*Echiostoma*）

  - [單鬚刺巨口魚](../Page/單鬚刺巨口魚.md "wikilink")（*Echiostoma barbatum*）

### 真巨口魚屬（*Eustomias*）

  - [瑪瑙真巨口魚](../Page/瑪瑙真巨口魚.md "wikilink")（*Eustomias achirus*）
  - （*Eustomias acinosus*）
  - [大西洋真巨口魚](../Page/大西洋真巨口魚.md "wikilink")（*Eustomias aequatorialis*）
  - （*Eustomias albibulbus*）
  - [聯球真巨口魚](../Page/聯球真巨口魚.md "wikilink")（*Eustomias appositus*）
  - [樹球真巨口魚](../Page/樹球真巨口魚.md "wikilink")（*Eustomias arborifer*）
  - [澳洲真巨口魚](../Page/澳洲真巨口魚.md "wikilink")（*Eustomias australensis*）
  - （*Eustomias austratlanticus*）
  - （*Eustomias bertelseni*）
  - （*Eustomias bibulboides*）
  - [雙球真巨口魚](../Page/雙球真巨口魚.md "wikilink")（*Eustomias bibulbosus*）
  - [歧鬚真巨口魚](../Page/歧鬚真巨口魚.md "wikilink")（*Eustomias bifilis*）
  - [比氏真巨口魚](../Page/比氏真巨口魚.md "wikilink")（*Eustomias bigelowi*）
  - [雙緣真巨口魚](../Page/雙緣真巨口魚.md "wikilink")（*Eustomias bimargaritatus*）
  - （*Eustomias bimargaritoides*）
  - [賓氏真巨口魚](../Page/賓氏真巨口魚.md "wikilink")（*Eustomias binghami*）
  - [雙瘤真巨口魚](../Page/雙瘤真巨口魚.md "wikilink")（*Eustomias bituberatus*）
  - （*Eustomias borealis*）
  - [布氏真巨口魚](../Page/布氏真巨口魚.md "wikilink")（*Eustomias braueri*）
  - [短髭真巨口魚](../Page/短髭真巨口魚.md "wikilink")（*Eustomias brevibarbatus*）
  - （*Eustomias bulbiramis*）
  - [球鬚真巨口魚](../Page/球鬚真巨口魚.md "wikilink")（*Eustomias bulbornatus*）
  - [瘤鬚真巨口魚](../Page/瘤鬚真巨口魚.md "wikilink")（*Eustomias cancriensis*）
  - （*Eustomias cirritus*）
  - （*Eustomias contiguus*）
  - （*Eustomias crossotus*）
  - （*Eustomias crucis*）
  - （*Eustomias cryptobulbus*）
  - （*Eustomias curtatus*）
  - （*Eustomias curtifilis*）
  - （*Eustomias danae*）
  - （*Eustomias decoratus*）
  - [棍狀真巨口魚](../Page/棍狀真巨口魚.md "wikilink")（*Eustomias dendriticus*）
  - （*Eustomias deofamiliaris*）
  - （*Eustomias digitatus*）
  - （*Eustomias dinema*）
  - （*Eustomias dispar*）
  - [疑真巨口魚](../Page/疑真巨口魚.md "wikilink")（*Eustomias dubius*）
  - （*Eustomias elongatus*）
  - [內鬚真巨口魚](../Page/內鬚真巨口魚.md "wikilink")（*Eustomias enbarbatus*）
  - [叉鬚真巨口魚](../Page/叉鬚真巨口魚.md "wikilink")（*Eustomias filifer*）
  - [絲鬚真巨口魚](../Page/絲鬚真巨口魚.md "wikilink")（*Eustomias fissibarbis*）
  - （*Eustomias flagellifer*）
  - [叉球真巨口魚](../Page/叉球真巨口魚.md "wikilink")（*Eustomias furcifer*）
  - [吉氏真巨口魚](../Page/吉氏真巨口魚.md "wikilink")（*Eustomias gibbsi*）
  - [鬚球真巨口魚](../Page/鬚球真巨口魚.md "wikilink")（*Eustomias grandibulbus*）
  - （*Eustomias hulleyi*）
  - [下裸真巨口魚](../Page/下裸真巨口魚.md "wikilink")（*Eustomias hypopsilus*）
  - （*Eustomias ignotus*）
  - （*Eustomias inconstans*）
  - [佛得角真巨口魚](../Page/佛得角真巨口魚.md "wikilink")（*Eustomias insularum*）
  - [中長真巨口魚](../Page/中長真巨口魚.md "wikilink")（*Eustomias intermedius*）
  - （*Eustomias interruptus*）
  - [艾氏真巨口魚](../Page/艾氏真巨口魚.md "wikilink")（*Eustomias ioani*）
  - （*Eustomias jimcraddocki*）
  - [克氏真巨口魚](../Page/克氏真巨口魚.md "wikilink")（*Eustomias kreffti*）
  - （*Eustomias lanceolatus*）
  - [細真巨口魚](../Page/細真巨口魚.md "wikilink")（*Eustomias leptobolus*）
  - [滑鰭真巨口魚](../Page/滑鰭真巨口魚.md "wikilink")（*Eustomias lipochirus*）
  - [長鬚真巨口魚](../Page/長鬚真巨口魚.md "wikilink")（*Eustomias longibarba*）
  - [大絲真巨口魚](../Page/大絲真巨口魚.md "wikilink")（*Eustomias macronema*）
  - [大眼真巨口魚](../Page/大眼真巨口魚.md "wikilink")（*Eustomias macrophthalmus*）
  - [大尾真巨口魚](../Page/大尾真巨口魚.md "wikilink")（*Eustomias macrurus*）
  - （*Eustomias magnificus*）
  - （*Eustomias medusa*）
  - [黑線真巨口魚](../Page/黑線真巨口魚.md "wikilink")（*Eustomias melanonema*）
  - [黑點真巨口魚](../Page/黑點真巨口魚.md "wikilink")（*Eustomias melanostigma*）
  - （*Eustomias melanostigmoides*）
  - （*Eustomias mesostenus*）
  - （*Eustomias metamelas*）
  - [小星真巨口魚](../Page/小星真巨口魚.md "wikilink")（*Eustomias micraster*）
  - （*Eustomias micropterygius*）
  - （*Eustomias minimus*）
  - （*Eustomias monoclonoides*）
  - （*Eustomias monoclonus*）
  - （*Eustomias monodactylus*）
  - [多線真巨口魚](../Page/多線真巨口魚.md "wikilink")（*Eustomias multifilis*）
  - [暗色真巨口魚](../Page/暗色真巨口魚.md "wikilink")（*Eustomias obscurus*）
  - [東方真巨口魚](../Page/東方真巨口魚.md "wikilink")（*Eustomias orientalis*）
  - （*Eustomias pacificus*）
  - （*Eustomias parini*）
  - （*Eustomias parri*）
  - [展鰭真巨口魚](../Page/展鰭真巨口魚.md "wikilink")（*Eustomias patulus*）
  - （*Eustomias paucifilis*）
  - （*Eustomias paxtoni*）
  - （*Eustomias perplexus*）
  - （*Eustomias pinnatus*）
  - [多星真巨口魚](../Page/多星真巨口魚.md "wikilink")（*Eustomias polyaster*）
  - （*Eustomias posti*）
  - （*Eustomias precarius*）
  - （*Eustomias problematicus*）
  - [梨形真巨口魚](../Page/梨形真巨口魚.md "wikilink")（*Eustomias pyrifer*）
  - （*Eustomias quadrifilis*）
  - （*Eustomias radicifilis*）
  - [塞氏真巨口魚](../Page/塞氏真巨口魚.md "wikilink")（*Eustomias satterleei*）
  - （*Eustomias schiffi*）
  - [史氏真巨口魚](../Page/史氏真巨口魚.md "wikilink")（*Eustomias schmidti*）
  - （*Eustomias silvescens*）
  - （*Eustomias similis*）
  - [簡真巨口魚](../Page/簡真巨口魚.md "wikilink")（*Eustomias simplex*）
  - [圓球真巨口魚](../Page/圓球真巨口魚.md "wikilink")（*Eustomias spherulifer*）
  - （*Eustomias suluensis*）
  - [特氏真巨口魚](../Page/特氏真巨口魚.md "wikilink")（*Eustomias tenisoni*）
  - [四線真巨口魚](../Page/四線真巨口魚.md "wikilink")（*Eustomias tetranema*）
  - （*Eustomias teuthidopsis*）
  - [長絲真巨口魚](../Page/長絲真巨口魚.md "wikilink")（*Eustomias tomentosis*）
  - [屈氏真巨口魚](../Page/屈氏真巨口魚.md "wikilink")（*Eustomias trewavasae*）
  - [三歧真巨口魚](../Page/三歧真巨口魚.md "wikilink")（*Eustomias triramis*）
  - （*Eustomias uniramis*）
  - [雜色真巨口魚](../Page/雜色真巨口魚.md "wikilink")（*Eustomias variabilis*）
  - [維氏真巨口魚](../Page/維氏真巨口魚.md "wikilink")（*Eustomias vitiazi*）
  - （*Eustomias vulgaris*）
  - [伍氏真巨口魚](../Page/伍氏真巨口魚.md "wikilink")（*Eustomias woollardi*）
  - [加勒比海真巨口魚](../Page/加勒比海真巨口魚.md "wikilink")（*Eustomias xenobolus*）

### 鞭鬚巨口魚屬（*Flagellostomias*）

  - [波氏鞭鬚巨口魚](../Page/波氏鞭鬚巨口魚.md "wikilink")（*Flagellostomias boureei*）

### 裸巨口魚屬（*Grammatostomias*）

  - [圓裸巨口魚](../Page/圓裸巨口魚.md "wikilink")（*Grammatostomias circularis*）
  - [裸巨口魚](../Page/裸巨口魚.md "wikilink")（*Grammatostomias dentatus*）
  - [鞭鬚裸巨口魚](../Page/鞭鬚裸巨口魚.md "wikilink")（*Grammatostomias
    flagellibarba*）

### 纖巨口魚屬（*Leptostomias*）

  - （*Leptostomias analis*）
  - （*Leptostomias bermudensis*）
  - [雙葉纖巨口魚](../Page/雙葉纖巨口魚.md "wikilink")（*Leptostomias bilobatus*）
  - [絲鬚纖巨口魚](../Page/絲鬚纖巨口魚.md "wikilink")（*Leptostomias gladiator*）
  - [纖巨口魚](../Page/纖巨口魚.md "wikilink")（*Leptostomias gracilis*）
  - [單莖纖巨口魚](../Page/單莖纖巨口魚.md "wikilink")（*Leptostomias haplocaulus*）
  - （*Leptostomias leptobolus*）
  - [長鬚纖巨口魚](../Page/長鬚纖巨口魚.md "wikilink")（*Leptostomias longibarba*）
  - [大絲纖巨口魚](../Page/大絲纖巨口魚.md "wikilink")（*Leptostomias macronema*）
  - [大鬚纖巨口魚](../Page/大鬚纖巨口魚.md "wikilink")（*Leptostomias macropogon*）
  - [多紋纖巨口魚](../Page/多紋纖巨口魚.md "wikilink")（*Leptostomias multifilis*）
  - [強壯纖巨口魚](../Page/強壯纖巨口魚.md "wikilink")（*Leptostomias robustus*）

### 黑巨口魚屬（*Melanostomias*）

  - [巴氏黑巨口魚](../Page/巴氏黑巨口魚.md "wikilink")（*Melanostomias bartonbeani*）
  - [雙光黑巨口魚](../Page/雙光黑巨口魚.md "wikilink")（*Melanostomias biseriatus*）
  - [呂宋黑巨口魚](../Page/呂宋黑巨口魚.md "wikilink")（*Melanostomias globulifer*）
  - [加勒比黑巨口魚](../Page/加勒比黑巨口魚.md "wikilink")（*Melanostomias
    macrophotus*）
  - [烏鬚黑巨口魚](../Page/烏鬚黑巨口魚.md "wikilink")（*Melanostomias melanopogon*）
  - [大眼黑巨口魚](../Page/大眼黑巨口魚.md "wikilink")（*Melanostomias melanops*）
  - [暗色黑巨口魚](../Page/暗色黑巨口魚.md "wikilink")（*Melanostomias niger*）
  - （*Melanostomias nigroaxialis*）
  - [少燈黑巨口魚](../Page/少燈黑巨口魚.md "wikilink")（*Melanostomias
    paucilaternatus*）
  - [少紋黑巨口魚](../Page/少紋黑巨口魚.md "wikilink")（*Melanostomias pauciradius*）
  - [絲鬚黑巨口魚](../Page/絲鬚黑巨口魚.md "wikilink")（*Melanostomias pollicifer*）
  - （*Melanostomias stewarti*）
  - [黑鬚黑巨口魚](../Page/黑鬚黑巨口魚.md "wikilink")（*Melanostomias tentaculatus*）
  - [瓦氏黑巨口魚](../Page/瓦氏黑巨口魚.md "wikilink")（*Melanostomias valdiviae*）
  - （*Melanostomias vierecki*）

### 強牙巨口魚屬（*Odontostomias*）

  - [強牙巨口魚](../Page/強牙巨口魚.md "wikilink")（*Odontostomias masticopogon*）
  - [小鬚強牙巨口魚](../Page/小鬚強牙巨口魚.md "wikilink")（*Odontostomias micropogon*）

### 脂巨口魚屬（*Opostomias*）

  - [小鰭脂巨口魚](../Page/小鰭脂巨口魚.md "wikilink")（*Opostomias micripnus*）
  - [脂巨口魚](../Page/脂巨口魚.md "wikilink")（*Opostomias mitsuii*）

### 厚巨口魚屬（*Pachystomias*）

  - [厚巨口魚](../Page/厚巨口魚.md "wikilink")（*Pachystomias microdon*）

### 袋巨口魚屬（*Photonectes*）

  - [白鰭袋巨口魚](../Page/白鰭袋巨口魚.md "wikilink")（*Photonectes albipennis*）
  - （*Photonectes albipennis*）
  - （*Photonectes barnetti*）
  - [勃氏袋巨口魚](../Page/勃氏袋巨口魚.md "wikilink")（*Photonectes braueri*）
  - [暗藍袋巨口魚](../Page/暗藍袋巨口魚.md "wikilink")（*Photonectes caerulescens*）
  - （*Photonectes coffea*）
  - （*Photonectes corynodes*）
  - [雙線袋巨口魚](../Page/雙線袋巨口魚.md "wikilink")（*Photonectes dinema*）
  - [細袋巨口魚](../Page/細袋巨口魚.md "wikilink")（*Photonectes gracilis*）
  - [白點袋巨口魚](../Page/白點袋巨口魚.md "wikilink")（*Photonectes leucospilus*）
  - [黑鰭袋巨口魚](../Page/黑鰭袋巨口魚.md "wikilink")（*Photonectes margarita*）
  - [奇異袋巨口魚](../Page/奇異袋巨口魚.md "wikilink")（*Photonectes mirabilis*）
  - （*Photonectes munificus*）
  - [球鬚袋巨口魚](../Page/球鬚袋巨口魚.md "wikilink")（*Photonectes parvimanus*）
  - [葉鬚袋巨口魚](../Page/葉鬚袋巨口魚.md "wikilink")（*Photonectes phyllopogon*）

### 箭口魚屬（*Tactostoma*）

  - [長鰭箭口魚](../Page/長鰭箭口魚.md "wikilink")（*Tactostoma macropus*）

### 纓光魚屬（*Thysanactis*）

  - [纓光魚](../Page/纓光魚.md "wikilink")（*Thysanactis dentex*）

### 三線巨口魚屬（*Trigonolampa*）

  - [三線巨口魚](../Page/三線巨口魚.md "wikilink")（*Trigonolampa miriceps*）

## 巨口魚亞科（Stomiinae）

### 巨口魚屬（*Stomias*）

  - [贡氏巨口鱼](../Page/贡氏巨口鱼.md "wikilink")（*Stomias affinis*）
  - [腹灯巨口鱼](../Page/腹灯巨口鱼.md "wikilink")（*Stomias atriventer*）
  - [发光巨口鱼](../Page/发光巨口鱼.md "wikilink")（*Stomias boa boa*）
  - （*Stomias boa colubrinus*）
  - [蛇形巨口魚](../Page/蛇形巨口魚.md "wikilink")（*Stomias boa ferox*）
  - [短鬚巨口魚](../Page/短鬚巨口魚.md "wikilink")（*Stomias brevibarbatus*）
  - （*Stomias danae*）
  - [细巨口魚](../Page/细巨口魚.md "wikilink")（*Stomias gracilis*）
  - [美丽巨口魚](../Page/美丽巨口魚.md "wikilink")（*Stomias lampropeltis*）
  - [长须巨口鱼](../Page/长须巨口鱼.md "wikilink")（*Stomias longibarbatus*）
  - [星雲巨口魚](../Page/星雲巨口魚.md "wikilink")（*Stomias nebulosus*）

[\*](../Category/巨口魚科.md "wikilink")