**巨腳龍屬**（[學名](../Page/學名.md "wikilink")：*Barapasaurus*），又名**巴拉帕龍**或**巨腿龍**，是[火山齒龍科下的一個](../Page/火山齒龍科.md "wikilink")[屬](../Page/屬.md "wikilink")。屬名是由[孟加拉語的](../Page/孟加拉語.md "wikilink")「巨腿」與[古希臘語的](../Page/古希臘語.md "wikilink")「蜥蜴」而來，因牠的大型腿骨。種名則是以[印度詩人](../Page/印度.md "wikilink")、哲學家[羅賓德拉納特·泰戈爾為名](../Page/羅賓德拉納特·泰戈爾.md "wikilink")。

## 描述

巨腳龍是已知最早的[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍之一](../Page/恐龍.md "wikilink")，估計生活於[早侏羅紀的](../Page/侏羅紀.md "wikilink")[托阿爾階](../Page/托阿爾階.md "wikilink")，距今約1億8960萬到1億7650萬年前。這一點可以從牠的不獨特形態中得到引證。後期的蜥腳下目如[腕龍](../Page/腕龍.md "wikilink")，已發展出牠們獨有的[生態位及攝食策略](../Page/生態位.md "wikilink")。例如，後期的蜥腳下目已發展出空心的[脊椎](../Page/脊椎.md "wikilink")，作為減輕體重的方法。而巨腳龍脊椎卻是幾乎實心的，只有少許空心的早期跡象。

巨腳龍長約18米，體重約48[公噸](../Page/公噸.md "wikilink")。牠的[臀部高約](../Page/臀部.md "wikilink")5.5米。

## 生活環境

如同其他[蜥腳下目恐龍](../Page/蜥腳下目.md "wikilink")，巨腳龍是[草食性動物](../Page/草食性.md "wikilink")。但是，到目前為止都沒有發現[頭顱骨](../Page/頭顱骨.md "wikilink")，所以不能確定牠的實際食性。目前已發現一些個別的[牙齒](../Page/牙齒.md "wikilink")，但都不足以作出確定。

雖然巨腳龍的[化石是於](../Page/化石.md "wikilink")[印度被發現](../Page/印度.md "wikilink")，但卻很像其他在[東非發現的標本](../Page/東非.md "wikilink")。由此估計，在[早侏羅紀時](../Page/侏羅紀.md "wikilink")，這兩個土地應該是連接在一起，或是不久後分離的。

## 分類

巨腳龍被分類在[火山齒龍科下](../Page/火山齒龍科.md "wikilink")，而非[鯨龍科](../Page/鯨龍科.md "wikilink")。這個分類並不確定，因為未有足夠的骨骼研究。而將牠分類在此科的原因，是牠擁有火山齒龍科特徵的狹窄[薦骨](../Page/薦骨.md "wikilink")。[背椎的](../Page/背椎.md "wikilink")[神經弓高於](../Page/神經弓.md "wikilink")[椎孔](../Page/椎孔.md "wikilink")，是種[鯨龍科的特徵](../Page/鯨龍科.md "wikilink")。

目前已知的唯一[種是泰氏巨腳龍](../Page/種.md "wikilink")（*B. tagorsi*）。

## 發現

巨腳龍的骨頭首先於1960年在[印度被發現](../Page/印度.md "wikilink")。但是要到1975年才作出詳細描述，並命名了[模式種](../Page/模式種.md "wikilink")。自此，有五個骨骼標本在印度南部的[戈達瓦里河河谷被發現](../Page/戈達瓦里河.md "wikilink")，但是沒有發現任何[頭顱骨或腳掌](../Page/頭顱骨.md "wikilink")，使得巨腳龍成為了解最多的[侏羅紀](../Page/侏羅紀.md "wikilink")[蜥腳下目](../Page/蜥腳下目.md "wikilink")，不過只有少數的文獻記述有關巨腳龍的資料。

## 外部連結

  - [巨腿龍基本數據](https://web.archive.org/web/20070813164835/http://www.dinoruss.com/de_4/5a6c125.htm)
  - [*Barapasaurus*](http://dinosaurs.about.com/od/herbivorousdinosaurs/p/barapasaurus.htm)
  - [*Barapasaurus*](http://animals.howstuffworks.com/dinosaurs/barapasaurus.htm)

[Category:蜥腳下目](../Category/蜥腳下目.md "wikilink")
[Category:印度與馬達加斯加恐龍](../Category/印度與馬達加斯加恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")