[Megantereon_cultridens.jpg](https://zh.wikipedia.org/wiki/File:Megantereon_cultridens.jpg "fig:Megantereon_cultridens.jpg")**巨頦虎**（*Megantereon*），又名**巨劍劍齒虎**或**巨劍齒虎**，是古代的[劍齒虎](../Page/劍齒虎.md "wikilink")，[斯劍虎的祖先](../Page/斯劍虎.md "wikilink")。

## 化石分佈

巨頦虎的[化石碎片是在](../Page/化石.md "wikilink")[非洲](../Page/非洲.md "wikilink")、[歐亞大陸及](../Page/歐亞大陸.md "wikilink")[北美洲發現](../Page/北美洲.md "wikilink")。巨頦虎唯一完整的[骨骼是在](../Page/骨骼.md "wikilink")[法國發現](../Page/法國.md "wikilink")。估計巨頦虎是生存於300-90萬年前。

## 生理學

巨頦虎的體型像現今的[美洲豹](../Page/美洲豹.md "wikilink")，大小如大型的[豹](../Page/豹.md "wikilink")。牠們的前肢粗壯，而下前肢有如[獅的大小](../Page/獅.md "wikilink")。牠們的[頸部](../Page/頸部.md "wikilink")[肌肉強壯](../Page/肌肉.md "wikilink")，一咬可以致命。

## 掠食者

巨頦虎並非以其劍齒來咬死獵物，因為其劍齒並不夠堅硬。牠有可能會咬傷獵物，待獵流[血致死](../Page/血.md "wikilink")。不過這樣牠需要保護獵物，免受其他[掠食者搶奪](../Page/掠食者.md "wikilink")，但其保護策略就不得面知。現時一般認為巨頦虎會像其他劍齒虎般利用劍齒來割破獵物[頸部大部份](../Page/頸部.md "wikilink")[神經及](../Page/神經.md "wikilink")[血管](../Page/血管.md "wikilink")。但是當獵物掙扎時，巨頦虎的劍齒仍有破損的風險。\[1\]

## 物種

巨頦虎的[物種數量並不清楚](../Page/物種.md "wikilink")，因為大部份的[化石都只是碎片](../Page/化石.md "wikilink")。一些學者指巨頦虎真正的物種數量是較以下的少：\[2\].

  - [泥河灣巨頦虎](../Page/泥河灣巨頦虎.md "wikilink")（*M. nihowanensis*）
  - *M. cultridens*
  - *M. whitei*：可能是*M. cultridens*的[異名](../Page/異名.md "wikilink")
  - *M. gracile*：可能是*M. cultridens*的異名
  - *M. eurynodon*
  - [巨頦巨頦虎](../Page/巨頦巨頦虎.md "wikilink")（*M. megantereon*）
  - *M. vakhshensis*
  - *M. ekidoit*
  - [藍田巨頦虎](../Page/藍田巨頦虎.md "wikilink")（*M. lantianensis*）

## 參考

[\*](../Category/巨頦虎屬.md "wikilink")
[Category:上新世哺乳類](../Category/上新世哺乳類.md "wikilink")
[M](../Category/更新世哺乳类.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")
[Category:非洲史前哺乳動物](../Category/非洲史前哺乳動物.md "wikilink")
[Category:北美洲史前哺乳動物](../Category/北美洲史前哺乳動物.md "wikilink")

1.
2.