**巨齒鱷屬**（屬名：*Teratosaurus*，意為「怪物蜥蜴」），是[勞氏鱷目的一屬](../Page/勞氏鱷目.md "wikilink")，生存於[三疊紀](../Page/三疊紀.md "wikilink")[諾利階的](../Page/諾利階.md "wikilink")[德國](../Page/德國.md "wikilink")，化石在Stubensandstein地區的Löwenstein地層發現。巨齒鱷的[模式標本是由](../Page/模式標本.md "wikilink")[克莉斯汀·艾瑞克·赫爾曼·汪邁爾](../Page/克莉斯汀·艾瑞克·赫爾曼·汪邁爾.md "wikilink")（Christian
Erich Hermann von
Meyer）所敘述，他根據一個附有牙齒的左[上頜骨](../Page/上頜骨.md "wikilink")，宣稱該化石與[箭齒龍並非同一種動物](../Page/箭齒龍.md "wikilink")。

之後的研究人員，例如[德國的休尼博士](../Page/德國.md "wikilink")、[亨利·費爾費爾德·奧斯本](../Page/亨利·費爾費爾德·奧斯本.md "wikilink")（Henry
Fairfield Osborn）、以及[埃德溫·爾伯特](../Page/埃德溫·爾伯特.md "wikilink")（Edwin H.
Colbert），將[原蜥腳下目的](../Page/原蜥腳下目.md "wikilink")[埃弗拉士龍的顱後部份歸類於巨齒鱷](../Page/埃弗拉士龍.md "wikilink")，使得埃弗拉士龍被認為是種非常原始的[獸腳亞目恐龍](../Page/獸腳亞目.md "wikilink")。許多20世紀的大眾書籍遵循這個錯誤，將巨齒鱷敘述成最早的大型、二足、[肉食性恐龍](../Page/肉食性.md "wikilink")，並獵食[原蜥腳下目恐龍](../Page/原蜥腳下目.md "wikilink")。巨齒鱷被許多研究人員認為是[侏儸紀肉食性恐龍的](../Page/侏儸紀.md "wikilink")[三疊紀祖先](../Page/三疊紀.md "wikilink")。

在1985年與1986年，[彼得·加爾冬](../Page/彼得·加爾冬.md "wikilink")（Peter
Galton）與[麥可·班頓](../Page/麥可·班頓.md "wikilink")（Michael
Benton）分別指出巨齒鱷其實屬於[勞氏鱷目](../Page/勞氏鱷目.md "wikilink")，是種大型掠食性[主龍類](../Page/主龍類.md "wikilink")，與恐龍共同生存於晚[三疊紀](../Page/三疊紀.md "wikilink")\[1\]\[2\]。

在2005年由Tomasz Sulej命名的西里西亞巨齒鱷（*T.
silesiacus*）\[3\]，在2009年由[斯蒂芬·布魯薩特](../Page/斯蒂芬·布魯薩特.md "wikilink")（Stephen
L.
Brusatte）建立為新屬[波羅尼鱷](../Page/波羅尼鱷.md "wikilink")（*Polonosuchus*）\[4\]。

## 參考資料

<div class="references-small">

<references>

</references>

  - [On the Classification of the Dinosauria with Observations on the
    Dinosauria of the
    Trias](http://aleph0.clarku.edu/huxley/SM3/ClDino.html) - *Quarterly
    Journal of the Geological Society* (1870) Scientific Memoirs III

</div>

## 外部連結

  - [Palaeos Mesozoic -
    Norian](https://web.archive.org/web/20070311011626/http://www.palaeos.com/Mesozoic/Triassic/Norian.4.htm)
  - [Rauisuchia Translation and Pronunciation
    Guide](https://web.archive.org/web/20060315213816/http://www.dinosauria.com/dml/names/rau.htm)
    by Ben Creisler

[Category:勞氏鱷目](../Category/勞氏鱷目.md "wikilink")

1.  Galton, P. M. (1985). "The poposaurid thecodontian *Teratosaurus
    suevicus* von Meyer, plus referred specimens mostly based on
    prosauropod dinosaurs". *Stuttgarter Beitrage zur Naturkunde*, B,
    **116**: 1-29.
2.  Benton, M.J. (1986). "The late Triassic reptile *Teratosaurus* - a
    rauisuchian, not a dinosaur". *Palaeontology* **29**: 293-301.
3.  Sulej, T. (2005). "A new rauisuchian reptile (Diapsida: Archosauria)
    from the Late Triassic of Poland." *Journal of Vertebrate
    Paleontology*, **25**(1):78-86.
4.