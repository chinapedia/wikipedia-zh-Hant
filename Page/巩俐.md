**巩俐**（），生于中國[辽宁省](../Page/辽宁省.md "wikilink")[沈阳市](../Page/沈阳市.md "wikilink")，於2008年入籍新加坡，國際著名[电影女](../Page/电影.md "wikilink")[演员](../Page/演员.md "wikilink")。曾获第49届[威尼斯国际电影节最佳女演员](../Page/威尼斯国际电影节.md "wikilink")、两届[中国电影金鸡奖最佳女主角](../Page/中国电影金鸡奖.md "wikilink")、[第26屆香港電影金像獎最佳女主角等多个海内外演技大奖](../Page/第26屆香港電影金像獎.md "wikilink")。鞏俐有豐富的評委經驗，她擔任過第50屆坎城影展評審、柏林影展評審團主席、威尼斯影展評審團主席、東京影展評審團主席、以及金馬獎評審團主席等。

## 生平

[Gong_Li_Cannes.jpg](https://zh.wikipedia.org/wiki/File:Gong_Li_Cannes.jpg "fig:Gong_Li_Cannes.jpg")

<center>

\]\]
巩俐出生于中国[沈阳市](../Page/沈阳市.md "wikilink")，是家中五个孩子中最小的一个。其父曾在[辽宁大学教书](../Page/辽宁大学.md "wikilink")，于[文化大革命前期调入](../Page/文化大革命.md "wikilink")[山东大学教书](../Page/山东大学.md "wikilink")，并举家迁往山东[济南](../Page/济南.md "wikilink")\[1\]。母亲是国企财务人员。四个哥哥姐姐均是教师。1985年从[济南二中毕业后考入](../Page/济南第二中学.md "wikilink")[中央戏剧学院表演系](../Page/中央戏剧学院.md "wikilink")，毕业后留校任话剧研究所演员。1988年，她与首次担任导演的[张艺谋合作](../Page/张艺谋.md "wikilink")，在影片《[红高粱](../Page/红高粱_\(电影\).md "wikilink")》中扮演女主角九儿，影片获得中国首个国际电影节[金熊奖](../Page/金熊奖.md "wikilink")。随着《红高粱》的一炮打响，巩俐也被海内外广为关注，成为首位登上《时代周刊》封面的华人明星。此后的两年间，巩俐又主演了两部张艺谋执导的影片《[菊豆](../Page/菊豆.md "wikilink")》和《[大红灯笼高高挂](../Page/大红灯笼高高挂.md "wikilink")》，都饰演了中国社会深受压迫的女性形象。因出色的演技，屡获国内外最佳女主角奖项。

1989年，巩俐第一次与[香港电影人合作](../Page/香港.md "wikilink")，偕同[张艺谋主演电影](../Page/张艺谋.md "wikilink")《[秦俑](../Page/秦俑.md "wikilink")》。《[秦俑](../Page/秦俑.md "wikilink")》是[大陆和香港电影人第一次跨地区的合作](../Page/大陆.md "wikilink")，两地精英尽出，制作团队庞大。憑藉在《秦俑》中的表演，鞏俐第一次提名[香港電影金像獎最佳女主角](../Page/香港電影金像獎.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Gong_Li_Andie_MacDowell_1998.jpg "fig:缩略图")，與美國女星[安迪·麥杜維合影](../Page/安迪·麥杜維.md "wikilink")\]\]
1992年与张艺谋合作的影片《[秋菊打官司](../Page/秋菊打官司.md "wikilink")》确立了巩俐在中国影坛的地位。秋菊这个朴实却执著的农村妇女形象，不仅使巩俐荣膺“[金鸡獎](../Page/金鸡獎.md "wikilink")”、“[百花獎](../Page/百花獎.md "wikilink")”双料影后，而且在第49届[威尼斯国际电影节上获得最佳女演员奖](../Page/威尼斯国际电影节.md "wikilink")；这是大陆女演员首次荣获国际大奖。之后三年与张艺谋合作的影片还有《[活着](../Page/活着_\(电影\).md "wikilink")》、《[摇啊摇，摇到外婆桥](../Page/摇啊摇，摇到外婆桥.md "wikilink")》。

鞏俐於2006年出演张艺谋執導的《[滿城盡帶黃金甲](../Page/滿城盡帶黃金甲.md "wikilink")》，這是十一年後的再度合作\[2\]，凭借此片获得[第26屆香港電影金像獎最佳女主角](../Page/第26屆香港電影金像獎.md "wikilink")。张艺谋与巩俐在事业和感情上都曾是亲密无间的伴侣，一系列的合作使这对导演和演员达到了两人事业的巅峰。然而在交往8年、合作過7部电影之后，两人最终还是分手。到了90年代中期，随着与张艺谋的分手，她也开始转换套路，开始饰演旧时代的城市女性和古代人物，例如《[画魂](../Page/画魂.md "wikilink")》、《[霸王别姬](../Page/霸王別姬_\(電影\).md "wikilink")》、《[漂亮妈妈](../Page/漂亮妈妈.md "wikilink")》、《[周渔的火车](../Page/周渔的火车.md "wikilink")》等等。

巩俐随后与英美烟草公司香港总裁黄和祥结婚，並於2008年11月8日入籍新加坡\[3\]，但兩人於2010年離婚\[4\]\[5\]。最近几年主要在[美国发展](../Page/美国.md "wikilink")。2014年以[張藝謀執導的](../Page/張藝謀.md "wikilink")《[歸來](../Page/歸來.md "wikilink")》入圍[第51屆金馬獎最佳女主角](../Page/第51屆金馬獎.md "wikilink")。\[6\]\[7\]2017年，巩俐被發現跟法國電子音樂家[让-米歇尔·雅尔拍拖](../Page/让-米歇尔·雅尔.md "wikilink")。第51屆金馬獎，錯失后冠的鞏俐離台後透過經紀人傳話，直說自己很喜歡台灣，未來會再來，但絕對不會再出席電影節，稱一點意義也沒有。2018年，在金馬執委會主席李安的邀約下，鞏俐擔任[第55屆金馬獎評審團主席](../Page/第55屆金馬獎.md "wikilink")。李安表示，自己擔任過第50屆金馬獎評審團主席，深刻了解這既是榮譽更是件苦差事，認為鞏俐不僅在華語影壇舉足輕重，亦有豐富的國際經驗，是相當合適的人選。

## 國際聲譽

1988年，鞏俐首次當女主角的電影——《红高粱》夺得在中國影史上的首個柏林影展金熊獎，這之後她便开始受到國際矚目。她隨後與該片導演張藝謀合作的影片如獲得[奧斯卡最佳外語片提名的](../Page/奧斯卡最佳外語片.md "wikilink")《菊豆》、《大紅燈籠高高掛》等讓她成為了西方世界知名度最高的華人女星。1992年的《秋菊打官司》更讓鞏俐成為中國大陸首獲威尼斯影展最佳女演員獎
(Volpi Cup)。隔年她憑《霸王別姬》成為首個獲紐約影評人獎的華人演員（最佳女配角）。

隨著1988年的《红高粱》，1992年的《秋菊打官司》及1993年的《霸王別姬》，鞏俐成為首位所參與的電影分別奪得歐洲三大影展最佳電影獎的華人女演員。

由於在國際影壇的知名度和影響力，鞏俐先後擔任第50屆[柏林國際電影節評委會主席](../Page/柏林國際電影節.md "wikilink")\[8\]、第59屆[威尼斯國際電影節評委會主席](../Page/威尼斯國際電影節.md "wikilink")\[9\]、第16屆[東京國際電影節評委會主席](../Page/東京國際電影節.md "wikilink")\[10\]，這個記錄迄今沒有華人女星打破。

淡出幾年之後，鞏俐以《[藝伎回憶錄](../Page/藝伎回憶錄.md "wikilink")》回歸大銀幕，以影片中初桃一角獲得美國[國家評論协会最佳女配角](../Page/國家評論协会.md "wikilink")，並主演了《[沉默的羔羊前傳](../Page/沉默的羔羊前傳.md "wikilink")》、《[邁阿密風雲](../Page/邁阿密風雲.md "wikilink")》、《[谍海风云](../Page/谍海风云.md "wikilink")》等好萊塢影片。

此外，鞏俐還獲得[聯合國教科文組織頒發的](../Page/聯合國教科文組織.md "wikilink")“促進和平藝術家”的稱號\[11\]，和[法蘭西藝術與文學勳章最高等級的](../Page/法蘭西藝術與文學勳章.md "wikilink")「[高等騎士勳位](../Page/法蘭西藝術與文學勳章.md "wikilink")」\[12\]。

## 影视作品

### 电影

<table>
<tbody>
<tr class="odd">
<td><p><strong>上映年份</strong></p></td>
<td><p><strong>片名</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>导演</strong></p></td>
<td><p><strong>合作演员</strong></p></td>
<td><p>'''奖项/备注</p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/红高粱_(电影).md" title="wikilink">红高梁</a></p></td>
<td><p>九儿</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/姜文.md" title="wikilink">姜文</a></p></td>
<td><p><a href="../Page/金熊奖.md" title="wikilink">金熊奖</a></p></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/一代妖后.md" title="wikilink">一代妖后</a></p></td>
<td><p>桂连</p></td>
<td><p><a href="../Page/李翰祥.md" title="wikilink">李翰祥</a></p></td>
<td><p><a href="../Page/刘晓庆.md" title="wikilink">刘晓庆</a>、<a href="../Page/陈道明.md" title="wikilink">陈道明</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開心巨無霸.md" title="wikilink">開心巨無霸</a></p></td>
<td></td>
<td><p><a href="../Page/陳欣健.md" title="wikilink">陳欣健</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/代号美洲豹.md" title="wikilink">代号美洲豹</a></p></td>
<td><p>阿丽</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td></td>
<td><p><a href="../Page/百花奖.md" title="wikilink">百花奖最佳女配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/秦俑.md" title="wikilink">秦俑</a></p></td>
<td><p>韩冬儿</p></td>
<td><p><a href="../Page/程小东.md" title="wikilink">程小东</a></p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p>提名–香港電影金像獎最佳女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/菊豆.md" title="wikilink">菊豆</a></p></td>
<td><p>菊豆</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/李保田.md" title="wikilink">李保田</a></p></td>
<td><p>提名-<a href="../Page/奥斯卡最佳外语片.md" title="wikilink">奥斯卡最佳外语片</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/賭俠2之上海灘賭聖.md" title="wikilink">賭俠2之上海灘賭聖</a></p></td>
<td><p>如仙/如夢</p></td>
<td><p><a href="../Page/王晶.md" title="wikilink">王晶</a></p></td>
<td><p><a href="../Page/周星馳.md" title="wikilink">周星馳</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大红灯笼高高挂.md" title="wikilink">大红灯笼高高挂</a></p></td>
<td><p>颂莲</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/何赛飞.md" title="wikilink">何赛飞</a>、<a href="../Page/曹翠芬.md" title="wikilink">曹翠芬</a>、<a href="../Page/马精武.md" title="wikilink">马精武</a></p></td>
<td><p><a href="../Page/百花奖.md" title="wikilink">百花奖最佳女主角</a><br />
提名-<a href="../Page/奥斯卡最佳外语片.md" title="wikilink">奥斯卡最佳外语片</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/豪門夜宴.md" title="wikilink">豪門夜宴</a></p></td>
<td><p>女服务员</p></td>
<td><p><a href="../Page/张同祖.md" title="wikilink">张同祖</a>、<a href="../Page/张坚庭.md" title="wikilink">张坚庭</a></p></td>
<td><p>群星</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/秋菊打官司.md" title="wikilink">秋菊打官司</a></p></td>
<td><p>秋菊</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/雷恪生.md" title="wikilink">雷恪生</a>、<a href="../Page/刘佩琦.md" title="wikilink">刘佩琦</a></p></td>
<td><p><a href="../Page/金鸡奖.md" title="wikilink">金鸡奖最佳女主角</a><br />
第49届<a href="../Page/威尼斯国际电影节.md" title="wikilink">威尼斯国际电影节最佳女演员</a><br />
<a href="../Page/金凤凰奖.md" title="wikilink">金凤凰奖最佳女演员</a><br />
<a href="../Page/金狮奖.md" title="wikilink">金狮奖</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夢醒時分.md" title="wikilink">夢醒時分</a></p></td>
<td><p>马莉</p></td>
<td><p><a href="../Page/张艾嘉.md" title="wikilink">张艾嘉</a></p></td>
<td><p><a href="../Page/钟镇涛.md" title="wikilink">钟镇涛</a>、<a href="../Page/林俊贤.md" title="wikilink">林俊贤</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/霸王别姬_(電影).md" title="wikilink">霸王别姬</a></p></td>
<td><p>菊仙</p></td>
<td><p><a href="../Page/陈凯歌.md" title="wikilink">陈凯歌</a></p></td>
<td><p><a href="../Page/张丰毅.md" title="wikilink">张丰毅</a>、<a href="../Page/张国荣.md" title="wikilink">张国荣</a></p></td>
<td><p><a href="../Page/纽约影评人协会奖.md" title="wikilink">纽约影评人协会奖最佳女配角</a><br />
<a href="../Page/金棕榈奖.md" title="wikilink">金棕榈奖</a><br />
<a href="../Page/金球奖最佳外语片.md" title="wikilink">金球奖最佳外语片</a><br />
<a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳外语片</a><br />
提名-<a href="../Page/奥斯卡最佳外语片.md" title="wikilink">奥斯卡最佳外语片</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/唐伯虎點秋香.md" title="wikilink">唐伯虎點秋香</a></p></td>
<td><p>秋香</p></td>
<td><p><a href="../Page/李力持.md" title="wikilink">李力持</a></p></td>
<td><p><a href="../Page/周星驰.md" title="wikilink">周星驰</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/新天龍八部之天山童姥.md" title="wikilink">新天龍八部之天山童姥</a></p></td>
<td><p>巫行云(天山童姥)</p></td>
<td><p><a href="../Page/钱永强.md" title="wikilink">钱永强</a></p></td>
<td><p><a href="../Page/林青霞.md" title="wikilink">林青霞</a>、<a href="../Page/张敏.md" title="wikilink">张敏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/画魂.md" title="wikilink">画魂</a></p></td>
<td><p>潘玉良</p></td>
<td><p><a href="../Page/黄蜀芹.md" title="wikilink">黄蜀芹</a></p></td>
<td><p><a href="../Page/尔冬升.md" title="wikilink">尔冬升</a>、<a href="../Page/达式常.md" title="wikilink">达式常</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/活着_(電影).md" title="wikilink">活着</a></p></td>
<td><p>家珍</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/葛优.md" title="wikilink">葛优</a></p></td>
<td><p><a href="../Page/戛纳电影节.md" title="wikilink">戛纳电影节评审团大奖</a><br />
<a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳外语片</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西楚霸王_(電影).md" title="wikilink">西楚霸王</a></p></td>
<td><p><a href="../Page/吕后.md" title="wikilink">吕后</a></p></td>
<td><p><a href="../Page/冼杞然.md" title="wikilink">冼杞然</a>、<a href="../Page/卫翰韬.md" title="wikilink">卫翰韬</a></p></td>
<td><p><a href="../Page/吕良伟.md" title="wikilink">吕良伟</a>、<a href="../Page/关之琳.md" title="wikilink">关之琳</a>、<a href="../Page/张丰毅.md" title="wikilink">张丰毅</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/摇啊摇，摇到外婆桥.md" title="wikilink">摇啊摇，摇到外婆桥</a></p></td>
<td><p>小金宝</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/王啸晓.md" title="wikilink">王啸晓</a>、<a href="../Page/李雪健.md" title="wikilink">李雪健</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/风月.md" title="wikilink">风月</a></p></td>
<td><p>龐如意</p></td>
<td><p><a href="../Page/陈凯歌.md" title="wikilink">陈凯歌</a></p></td>
<td><p><a href="../Page/张国荣.md" title="wikilink">张国荣</a></p></td>
<td><p>提名–<a href="../Page/香港電影金像獎最佳女主角.md" title="wikilink">香港電影金像獎最佳女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/:en:Chinese_Box.md" title="wikilink">中國匣</a></p></td>
<td><p>Vivian</p></td>
<td></td>
<td><p>許冠文、<a href="../Page/張曼玉.md" title="wikilink">张曼玉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/荆轲刺秦王_(电影).md" title="wikilink">荆柯刺秦王</a></p></td>
<td><p>赵姬</p></td>
<td><p><a href="../Page/陈凯歌.md" title="wikilink">陈凯歌</a></p></td>
<td><p><a href="../Page/李雪健.md" title="wikilink">李雪健</a>、<a href="../Page/张丰毅.md" title="wikilink">张丰毅</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p>孙丽英</p></td>
<td><p><a href="../Page/孙周.md" title="wikilink">孙周</a></p></td>
<td><p><a href="../Page/高炘.md" title="wikilink">高炘</a>、<a href="../Page/施京明.md" title="wikilink">施京明</a>、<a href="../Page/乐秀清.md" title="wikilink">乐秀清</a></p></td>
<td><p><a href="../Page/金鸡奖.md" title="wikilink">金鸡奖最佳女主角</a><br />
第24届<a href="../Page/蒙特利尔国际电影节.md" title="wikilink">蒙特利尔国际电影节最佳女主角奖</a><br />
<a href="../Page/金鳳凰獎.md" title="wikilink">金鳳凰獎最佳女演员</a><br />
第24届大眾電影百花獎最佳女主角<br />
第9屆<a href="../Page/上海影評人協會.md" title="wikilink">上海影評人協會最佳女演員</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/周渔的火车.md" title="wikilink">周渔的火车</a></p></td>
<td><p>周渔</p></td>
<td><p><a href="../Page/孙周.md" title="wikilink">孙周</a></p></td>
<td><p><a href="../Page/梁家辉.md" title="wikilink">梁家辉</a>、<a href="../Page/孙红雷.md" title="wikilink">孙红雷</a></p></td>
<td><p>第10屆<a href="../Page/北京大學生電影節.md" title="wikilink">北京大學生電影節最受歡迎女演員</a></p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/2046_(電影).md" title="wikilink">2046</a></p></td>
<td><p>苏丽珍</p></td>
<td><p><a href="../Page/王家卫.md" title="wikilink">王家卫</a></p></td>
<td><p><a href="../Page/梁朝偉.md" title="wikilink">梁朝伟</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:en:Eros_(film).md" title="wikilink">爱神</a></p></td>
<td><p>Miss Hua</p></td>
<td><p><a href="../Page/米开朗基罗·安东尼奥尼.md" title="wikilink">米开朗基罗·安东尼奥尼</a></p></td>
<td><p><a href="../Page/小勞勃·道尼.md" title="wikilink">小勞勃·道尼</a>、<a href="../Page/張震_(演員).md" title="wikilink">张震</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/艺伎回忆录.md" title="wikilink">艺伎回忆录</a></p></td>
<td><p>初桃</p></td>
<td><p><a href="../Page/罗伯·马歇尔.md" title="wikilink">罗伯·马歇尔</a></p></td>
<td><p><a href="../Page/章子怡.md" title="wikilink">章子怡</a>、<a href="../Page/渡边谦.md" title="wikilink">渡边谦</a>、<a href="../Page/杨紫琼.md" title="wikilink">杨紫琼</a></p></td>
<td><p><a href="../Page/國家評論协會.md" title="wikilink">國家評論协會最佳女配角</a><br />
提名–美國國際傳媒學院<a href="../Page/衛星獎.md" title="wikilink">衛星獎最佳女配角</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/:en:Miami_Vice_(film).md" title="wikilink">迈阿密风暴</a></p></td>
<td><p>伊莎贝拉</p></td>
<td><p><a href="../Page/迈克尔·曼.md" title="wikilink">迈克尔·曼</a></p></td>
<td><p><a href="../Page/柯林·法瑞尔.md" title="wikilink">柯林·法瑞尔</a>、<a href="../Page/杰米·福克斯.md" title="wikilink">杰米·福克斯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/满城尽带黄金甲.md" title="wikilink">满城尽带黄金甲</a></p></td>
<td><p>王后</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/周润发.md" title="wikilink">周润发</a>、<a href="../Page/周杰伦.md" title="wikilink">周杰伦</a>、<a href="../Page/刘烨.md" title="wikilink">刘烨</a></p></td>
<td><p><a href="../Page/第26屆香港電影金像獎.md" title="wikilink">第26屆香港電影金像獎最佳女主角</a><br />
第13屆<a href="../Page/香港電影評論學會大獎.md" title="wikilink">香港電影評論學會大獎最佳女演員</a><br />
第12屆香港電影金紫荊獎 最佳女主角<br />
提名–<a href="../Page/第一届亚洲电影大奖.md" title="wikilink">第一届亚洲电影大奖最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/人魔崛起.md" title="wikilink">人魔崛起</a>/<a href="../Page/:en:Hannibal_Rising_(film).md" title="wikilink">沉默的羔羊前传之揭开罪幕</a></p></td>
<td><p>紫夫人</p></td>
<td><p><a href="../Page/Peter_Webber.md" title="wikilink">Peter Webber</a></p></td>
<td><p><a href="../Page/加斯帕德·尤利尔.md" title="wikilink">加斯帕德·尤利尔</a>、<a href="../Page/瑞斯·伊凡斯.md" title="wikilink">瑞斯·伊凡斯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/諜海風雲.md" title="wikilink">諜海風雲</a></p></td>
<td><p>安娜</p></td>
<td><p><a href="../Page/米凯尔·哈弗斯特罗姆.md" title="wikilink">米凯尔·哈弗斯特罗姆</a></p></td>
<td><p><a href="../Page/约翰·库萨克.md" title="wikilink">约翰·库萨克</a>、<a href="../Page/周润发.md" title="wikilink">周润发</a>、<a href="../Page/渡边谦.md" title="wikilink">渡边谦</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/我知女人心.md" title="wikilink">我知女人心</a></p></td>
<td><p>李仪龙</p></td>
<td><p><a href="../Page/陈大明.md" title="wikilink">陈大明</a></p></td>
<td><p><a href="../Page/刘德华.md" title="wikilink">刘德华</a>、<a href="../Page/李成儒.md" title="wikilink">李成儒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/归来_(电影).md" title="wikilink">归来</a></p></td>
<td><p>冯婉瑜</p></td>
<td><p><a href="../Page/张艺谋.md" title="wikilink">张艺谋</a></p></td>
<td><p><a href="../Page/陈道明.md" title="wikilink">陈道明</a>、<a href="../Page/张慧雯.md" title="wikilink">张慧雯</a></p></td>
<td><p><a href="../Page/长春电影节.md" title="wikilink">长春电影节最佳女主角</a><br />
中国电影导演协会2014年度表彰评选年度女演员<br />
提名-<a href="../Page/第51届金马奖.md" title="wikilink">第51届金马奖最佳女主角</a><br />
提名-<a href="../Page/第16届华表奖.md" title="wikilink">第16届华表奖优秀女演员奖</a></p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/西游记之孙悟空三打白骨精.md" title="wikilink">西游记之孙悟空三打白骨精</a></p></td>
<td><p><a href="../Page/白骨精.md" title="wikilink">白骨精</a></p></td>
<td><p><a href="../Page/郑保瑞.md" title="wikilink">郑保瑞</a></p></td>
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a>、<a href="../Page/冯绍峰.md" title="wikilink">冯绍峰</a>、<a href="../Page/小沈阳.md" title="wikilink">小沈阳</a>、<a href="../Page/罗仲谦.md" title="wikilink">罗仲谦</a></p></td>
<td><p><a href="../Page/3D电影.md" title="wikilink">3D电影</a>，首次饰演<a href="../Page/反派.md" title="wikilink">反派</a></p></td>
</tr>
<tr class="odd">
<td><p>待上映</p></td>
<td><p><a href="../Page/蘭心大劇院.md" title="wikilink">蘭心大劇院</a></p></td>
<td></td>
<td><p><a href="../Page/娄烨.md" title="wikilink">娄烨</a></p></td>
<td><p><a href="../Page/赵又廷.md" title="wikilink">赵又廷</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>待上映</p></td>
<td><p><a href="../Page/花木蘭_(2020年電影).md" title="wikilink">花木蘭</a></p></td>
<td><p>巫女</p></td>
<td><p><a href="../Page/妮基·卡歐.md" title="wikilink">妮基·卡歐</a></p></td>
<td><p><a href="../Page/劉亦菲.md" title="wikilink">劉亦菲</a>、<a href="../Page/甄子丹.md" title="wikilink">甄子丹</a>、<a href="../Page/李連杰.md" title="wikilink">李連杰</a></p></td>
<td><p>[13]</p></td>
</tr>
</tbody>
</table>

### 配音

  - 2007年，配音电影《[蓝莓之夜](../Page/蓝莓之夜.md "wikilink")》

## 广告代言

  - [Piaget](http://www.piaget.com) [伯爵珠宝腕表](http://www.piaget.com.cn)
  - [曲美](../Page/曲美.md "wikilink")
  - Loreal Paris[巴黎欧莱雅](../Page/巴黎欧莱雅.md "wikilink")
  - [美的电器](../Page/美的.md "wikilink")
  - 大阳摩托车
  - Chopard Jewelry萧邦珠宝、Chopard Watches萧邦腕表
  - 雅雀腕表
  - OSIM[傲胜](../Page/傲胜.md "wikilink")
  - 嘉伦饼干
  - 野力干红葡萄酒

## 獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>劇名</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/代號美洲豹.md" title="wikilink">代號美洲豹</a></p></td>
<td><p><a href="../Page/大眾電影百花奖.md" title="wikilink">大眾電影百花奖</a></p></td>
<td><p><a href="../Page/大眾電影百花獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/古今大战秦俑情.md" title="wikilink">古今大战秦俑情</a></p></td>
<td><p><a href="../Page/1991年香港電影金像獎.md" title="wikilink">金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/大紅燈籠高高掛.md" title="wikilink">大紅燈籠高高掛</a></p></td>
<td><p><a href="../Page/意大利電影金像獎.md" title="wikilink">意大利電影金像獎</a></p></td>
<td><p>最佳外國女演員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/秋菊打官司.md" title="wikilink">秋菊打官司</a></p></td>
<td><p><a href="../Page/威尼斯電影節.md" title="wikilink">威尼斯電影節</a></p></td>
<td><p><a href="../Page/最佳演员奖_(威尼斯电影节).md" title="wikilink">最佳演员奖</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/秋菊打官司.md" title="wikilink">秋菊打官司</a></p></td>
<td><p><a href="../Page/第13届中国电影金鸡奖.md" title="wikilink">金雞獎</a></p></td>
<td><p><a href="../Page/中国电影金鸡奖最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/秋菊打官司.md" title="wikilink">秋菊打官司</a></p></td>
<td><p><a href="../Page/中國電影金鳳凰獎.md" title="wikilink">中國電影金鳳凰獎</a></p></td>
<td><p>表演學會獎</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/秋菊打官司.md" title="wikilink">秋菊打官司</a></p></td>
<td><p>日本影評人大獎</p></td>
<td><p>最佳外語片女主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/大紅燈籠高高掛.md" title="wikilink">大紅燈籠高高掛</a></p></td>
<td><p><a href="../Page/大眾電影百花奖.md" title="wikilink">大眾電影百花奖</a></p></td>
<td><p><a href="../Page/大眾電影百花獎最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/菊豆.md" title="wikilink">菊豆</a></p></td>
<td><p>第1届保加利亞瓦爾那國際電影節</p></td>
<td><p>最佳女演員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/霸王別姬_(電影).md" title="wikilink">霸王別姬</a></p></td>
<td><p><a href="../Page/紐約影評人協會獎.md" title="wikilink">紐約影評人協會獎</a></p></td>
<td><p>最佳女配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/活着_(電影).md" title="wikilink">活着</a></p></td>
<td><p>Chlotrudis Awards</p></td>
<td><p>最佳女演員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/風月.md" title="wikilink">風月</a></p></td>
<td><p><a href="../Page/1997年香港電影金像獎.md" title="wikilink">金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p><a href="../Page/第20届中国电影金鸡奖.md" title="wikilink">金雞獎</a></p></td>
<td><p><a href="../Page/中国电影金鸡奖最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p><a href="../Page/蒙特利爾世界電影節.md" title="wikilink">蒙特利爾世界電影節</a></p></td>
<td><p>最佳女主角[14]</p></td>
<td><p><br />
(與<a href="../Page/Isabelle_Huppert.md" title="wikilink">Isabelle Huppert共享</a>)</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p>第10届上海影評人協會</p></td>
<td><p>最佳女演員[15]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p><a href="../Page/中國電影金鳳凰獎.md" title="wikilink">中國電影金鳳凰獎</a></p></td>
<td><p>表演學會獎</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/漂亮妈妈.md" title="wikilink">漂亮妈妈</a></p></td>
<td><p><a href="../Page/大眾電影百花奖.md" title="wikilink">大眾電影百花奖</a></p></td>
<td><p><a href="../Page/大眾電影百花獎最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/周漁的火車.md" title="wikilink">周漁的火車</a></p></td>
<td><p><a href="../Page/北京大學生電影節.md" title="wikilink">北京大學生電影節</a></p></td>
<td><p>最受歡迎女演員</p></td>
<td><p><br />
(與<a href="../Page/周迅.md" title="wikilink">周迅共享</a>)</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/藝伎回憶錄.md" title="wikilink">藝伎回憶錄</a></p></td>
<td><p><a href="../Page/國家評論協會.md" title="wikilink">國家評論協會</a></p></td>
<td><p>最佳女配角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/藝伎回憶錄.md" title="wikilink">藝伎回憶錄</a></p></td>
<td><p><a href="../Page/衛星獎.md" title="wikilink">衛星獎</a></p></td>
<td><p>最佳女配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/滿城盡帶黃金甲.md" title="wikilink">滿城盡帶黃金甲</a></p></td>
<td><p><a href="../Page/香港電影評論學會大獎.md" title="wikilink">香港電影評論學會大獎</a></p></td>
<td><p><a href="../Page/香港電影評論學會大獎.md" title="wikilink">最佳女演員</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/滿城盡帶黃金甲.md" title="wikilink">滿城盡帶黃金甲</a></p></td>
<td><p><a href="../Page/第1屆亞洲電影大獎.md" title="wikilink">亞洲電影大獎</a></p></td>
<td><p>最佳女演員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/滿城盡帶黃金甲.md" title="wikilink">滿城盡帶黃金甲</a></p></td>
<td><p><a href="../Page/2007年香港電影金像獎.md" title="wikilink">金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/滿城盡帶黃金甲.md" title="wikilink">滿城盡帶黃金甲</a></p></td>
<td><p><a href="../Page/2007年香港電影金紫荊獎.md" title="wikilink">金紫荊獎</a></p></td>
<td><p>最佳女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/归来_(电影).md" title="wikilink">歸來</a></p></td>
<td><p><a href="../Page/長春電影節.md" title="wikilink">長春電影節</a></p></td>
<td><p>最佳女主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/归来_(电影).md" title="wikilink">歸來</a></p></td>
<td><p><a href="../Page/第51屆金馬獎.md" title="wikilink">第51屆金馬獎</a></p></td>
<td><p>最佳女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/归来_(电影).md" title="wikilink">歸來</a></p></td>
<td><p><a href="../Page/中國電影導演協會年度表彰評選.md" title="wikilink">中國電影導演協會2014年度表彰評選</a></p></td>
<td><p>年度女演員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/归来_(电影).md" title="wikilink">歸來</a></p></td>
<td><p><a href="../Page/第16届华表奖.md" title="wikilink">第16届华表奖</a></p></td>
<td><p>优秀女演员</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部链接

  -
  - [巩俐](http://www.dianying.com/jt/person.php?personid=GongLi) 在中文电影资料库


  -
  -
  -
[L俐](../Category/巩姓.md "wikilink")
[Category:沈阳人](../Category/沈阳人.md "wikilink")
[Category:济南人](../Category/济南人.md "wikilink")
[Category:归化新加坡公民的中华人民共和国人](../Category/归化新加坡公民的中华人民共和国人.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")
[Category:21世纪演员](../Category/21世纪演员.md "wikilink")
[Category:華人電影演員](../Category/華人電影演員.md "wikilink")
[Category:中国電影女演员](../Category/中国電影女演员.md "wikilink")
[Category:辽宁演员](../Category/辽宁演员.md "wikilink")
[Category:山东演员](../Category/山东演员.md "wikilink")
[Category:新加坡电影演员](../Category/新加坡电影演员.md "wikilink")
[Category:中央戏剧学院校友](../Category/中央戏剧学院校友.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:威尼斯影展最佳女演员奖获得者‎](../Category/威尼斯影展最佳女演员奖获得者‎.md "wikilink")
[Category:香港電影評論學會大獎最佳女演員得主](../Category/香港電影評論學會大獎最佳女演員得主.md "wikilink")
[Category:香港電影金紫荊獎最佳女主角得主](../Category/香港電影金紫荊獎最佳女主角得主.md "wikilink")
[Category:威尼斯电影节评审团主席](../Category/威尼斯电影节评审团主席.md "wikilink")
[Category:东京电影节评审团主席](../Category/东京电影节评审团主席.md "wikilink")
[Category:柏林电影节评审团主席](../Category/柏林电影节评审团主席.md "wikilink")
[Category:福布斯中国年度前十名人](../Category/福布斯中国年度前十名人.md "wikilink")

1.
2.  [再合作
    张艺谋巩俐十年一别刮目相看](http://ent.china.com/zh_cn/movie/news/205/20060922/13643566.html)
     中華網電影頻道 2006年9月22日
3.  [巩俐宣誓成为新加坡公民](http://ent.sina.com.cn/s/m/p/2008-11-10/03032242790.shtml)
4.  [鞏俐黃和祥離婚內幕曝光 傳兩人有孩子](https://read01.com/NoG7AG.html)
5.  [鞏俐老公 揭秘鞏俐與老公離婚的真因是什麼](https://read01.com/mx5RJK.html)
6.  [金馬獎入圍戰力分析](http://www.cna.com.tw/project/2014_goldenhorse/51nominate.html)，中央社，2014年11月12日
7.  [金馬獎2014完整入圍名單出爐](http://www.cna.com.tw/news/firstnews/201410015006-1.aspx)，中央社，2014年10月1日
8.  [巩俐将担任第50届柏林国际电影节评委会主席](http://www.chinanews.com.cn/1999-12-22/26/12841.html)
    中國新聞網 1999年12月22日
9.  [第59届威尼斯电影节
    巩俐主席只讲汉语](http://www.hljdaily.com.cn/xw_whyl/system/2002/09/03/000364788.shtml)
     黑龍江日報 2002年9月03日
10. [巩俐任东京国际电影节评审团主席(图)](http://ent.sina.com.cn/2003-09-19/0949203248.html)
    廣州日報 2003年9月19日
11. [巩俐被任命为联合国教科文组织“促进和平艺术家”](http://www.people.com.cn/GB/channel2/702/20000511/60718.html)
    人民網 2000年5月11日
12. [巴黎欧莱雅代言人巩俐
    授勋法国文学艺术最高荣誉](http://eladies.sina.com.cn/beauty/liangli/2010/0505/1148989576.shtml)
    新浪網 2010年5月5日
13.
14. [AWARDS OF THE MONTREAL WORLD FILM FESTIVAL
    - 2000](http://www.ffm-montreal.org/en/awards/17-awards-of-the-montreal-world-film-festival-2000.html)
15. [上海影评人奖十佳影片昨颁奖](http://ent.sina.com.cn/film/chinese/2000-09-12/16333.html)新浪娱乐
    2000年9月12日