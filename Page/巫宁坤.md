**巫宁坤**（巫甯坤，）[中國](../Page/中國.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[扬州人](../Page/扬州.md "wikilink")，[翻译家](../Page/翻译家.md "wikilink")，英美文学研究专家。

## 生平

[抗日战争期间](../Page/抗日战争.md "wikilink")，就读于[昆明](../Page/昆明.md "wikilink")[西南联合大学外文系](../Page/西南联合大学.md "wikilink")。1946年9月，成为[美国](../Page/美国.md "wikilink")[印第安纳州的](../Page/印第安纳州.md "wikilink")唯一的外国学生。后在[芝加哥大学研究院攻读](../Page/芝加哥大学.md "wikilink")[文学批评](../Page/文学批评.md "wikilink")。1951年受北京[燕京大学校长](../Page/燕京大学.md "wikilink")[陆志韦邀请](../Page/陆志韦.md "wikilink")，放弃攻读博士学位，回国从事英语教学。临行前，室友[李政道为其整理行李](../Page/李政道.md "wikilink")\[1\]。

1951年新年，应北京[燕京大学校长](../Page/燕京大学.md "wikilink")[陆志韦邀请](../Page/陆志韦.md "wikilink")，并附以及中华人民共和国的政务院欢迎信后，便回国任教。此后先后在北京[燕京大学](../Page/燕京大学.md "wikilink")、天津[南开大学](../Page/南开大学.md "wikilink")、北京[国际关系学院等校任英美文学教授](../Page/国际关系学院.md "wikilink")。巫宁坤曾经在《英语世界》发表一些英文散文，诗歌和翻译，包括他英译的方励之的《重访卡普里》。巫宁坤中英文俱佳。[黄灿然認為他翻译为中文的有些诗歌译文质量优于](../Page/黄灿然.md "wikilink")[余光中的翻译](../Page/余光中.md "wikilink")\[2\]。著有英文回忆录《A
Single
Tear》，后出版中文本，名《[一滴泪](../Page/一滴泪.md "wikilink")》。作为翻译家，他的著名译作有《[了不起的盖茨比](../Page/了不起的盖茨比.md "wikilink")》和《[白求恩传](../Page/白求恩传.md "wikilink")》。

1954年起，开始受到批评批判，后一度恢复工作。燕京大学被拆分后，被分配到南开大学。在南开任教期间与李怡楷结婚。1957年“[双百运动](../Page/双百运动.md "wikilink")”时因批评时政，在[反右运动时被打倒](../Page/反右运动.md "wikilink")，1958年4月17日下午，被送到了北京南城[陶然亭公园附近的](../Page/陶然亭公园.md "wikilink")“[半步桥监狱](../Page/半步桥监狱.md "wikilink")”接受[劳教](../Page/劳教.md "wikilink")。1958年6月，被送往[北大荒兴凯湖劳改农场劳动改造](../Page/北大荒.md "wikilink")。1960年10月26日，被转移至[河北省](../Page/河北省.md "wikilink")[清河农场的三分场](../Page/清河农场.md "wikilink")（又称宁河农场）。1961年6月30日保外就医，1962年前往[安徽大学英语专业任教](../Page/安徽大学.md "wikilink")。[文革开始后继续受到各种批斗](../Page/文革.md "wikilink")、批判。1970年4月底，出了“[牛棚](../Page/牛棚_\(文革用语\).md "wikilink")”的巫宁坤又被取消了职工待遇，被[下放到生产队劳动](../Page/下放.md "wikilink")。[林彪叛逃事件后](../Page/林彪叛逃事件.md "wikilink")，1973年落实知识分子政策再一次提上日程，经省革委会文教副主任杨效椿和[安徽师范大学校长魏心一的努力](../Page/安徽师范大学.md "wikilink")，巫宁坤于1974年一月底，被调到位于[芜湖的](../Page/芜湖.md "wikilink")[安徽师范大学任教](../Page/安徽师范大学.md "wikilink")。1979年5月奉命回北京已改称[国际关系学院的原单位办理](../Page/国际关系学院.md "wikilink")“右派”改正。1980年代，偕夫人和儿女离开中国，重返美国定居。

## 参考注释

## 外部链接

  - [巫宁坤文集](https://web.archive.org/web/20100425064114/http://www.boxun.com/hero/wunk/)
  - [巫宁坤《一滴泪》](https://www.bannedbook.org/download/downfile.php?id=416/)

[Category:扬州人](../Category/扬州人.md "wikilink")
[Category:中华人民共和国翻译家](../Category/中华人民共和国翻译家.md "wikilink")
[Category:右派分子](../Category/右派分子.md "wikilink")
[Category:国立西南联合大学校友](../Category/国立西南联合大学校友.md "wikilink")
[Category:燕京大学教师](../Category/燕京大学教师.md "wikilink")
[Category:南开大学教师](../Category/南开大学教师.md "wikilink")
[Category:安徽大学教师](../Category/安徽大学教师.md "wikilink")
[Category:安徽师范大学教授](../Category/安徽师范大学教授.md "wikilink")
[Category:文革被迫害学者](../Category/文革被迫害学者.md "wikilink")

1.
2.  参考黄灿然《译诗中的现代敏感》