**差館街**是[香港多條街道的名稱](../Page/香港.md "wikilink")。“[差館](../Page/差館.md "wikilink")”一詞沿自[差人](../Page/差人.md "wikilink")，因[中國歷史上](../Page/中國歷史.md "wikilink")，地方上的[治安](../Page/治安.md "wikilink")[管理](../Page/管理.md "wikilink")[機構為](../Page/機構.md "wikilink")[衙門](../Page/衙門.md "wikilink")，執行差事者名[衙門差役](../Page/衙門差役.md "wikilink")，及後演變成[警察的俗稱](../Page/警察.md "wikilink")，由此得名。因香港各區[警署的落成](../Page/警署.md "wikilink")，警署附近的街道被[居民](../Page/居民.md "wikilink")[約定俗成為差館街](../Page/約定俗成.md "wikilink")，後來被[香港政府認可](../Page/香港政府.md "wikilink")。及後，為免不同地區使用相同的街道名稱，產生混淆，政府於1909年刊登憲報將九龍區之街道名稱更改，以[上海](../Page/上海.md "wikilink")、[大沽和](../Page/大沽.md "wikilink")[福州取名](../Page/福州.md "wikilink")，這些皆為當時[中國對外主要的通商](../Page/中國.md "wikilink")[港口](../Page/港口.md "wikilink")。

[香港島範圍共有](../Page/香港島.md "wikilink")1條街道：

  - **[普義街](../Page/普義街.md "wikilink")**，位於[上環](../Page/上環.md "wikilink")[太平山區](../Page/太平山區.md "wikilink")（[維多利亞城第三區](../Page/維多利亞城.md "wikilink")），因第一代[半山警署](../Page/半山警署.md "wikilink")（八號差館）於1845年座落該區而取名差館街，該區於1894年發生嚴重[鼠疫](../Page/鼠疫.md "wikilink")，香港政府為了確保改善疫情，遂把該區所有樓宇清拆，包括當時的半山警署，警署被遷至現[醫院道及](../Page/醫院道.md "wikilink")[高街之間位置](../Page/高街.md "wikilink")，而太平山區重建工程於1898年左右完成，街名亦因而改為現時的普義街。不過，與差館街相連並向半山方向延伸的一條斜路[差館上街卻被保留下來](../Page/差館上街.md "wikilink")，現時分為上下兩段，被[卜公花園所分隔](../Page/卜公花園.md "wikilink")。下段由[荷里活道至卜公花園](../Page/荷里活道.md "wikilink")，上段由[普慶坊至](../Page/普慶坊.md "wikilink")[醫院道](../Page/醫院道.md "wikilink")。

[九龍範圍共有](../Page/九龍.md "wikilink")3條街道：

  - **[上海街](../Page/上海街.md "wikilink")**，位於[油麻地及](../Page/油麻地.md "wikilink")[旺角一帶](../Page/旺角.md "wikilink")，因地處該區的第一代[油麻地警署](../Page/油麻地警署.md "wikilink")（1873年啟用）而得名，差館街後來易名為上海街，警署在1922年遷至[廣東道](../Page/廣東道.md "wikilink")，2016年遷至[友翔道](../Page/友翔道.md "wikilink")。

<!-- end list -->

  - **[大沽街](../Page/大沽街.md "wikilink")**，位於[紅磡](../Page/紅磡.md "wikilink")，第一代[紅磡警署於](../Page/紅磡警署.md "wikilink")1872年座落於該處，差館街後來易名為大沽街，及後警署亦作遷移。惟附近的一條橫街[差館里則得到保留名稱](../Page/差館里.md "wikilink")。

<!-- end list -->

  - **[通州街](../Page/通州街.md "wikilink")**，位於[大角咀](../Page/大角咀.md "wikilink")，早年警署座落於該區[福全鄉](../Page/福全鄉.md "wikilink")，差館街後來易名為福州街，及後再改名為現時的[通州街](../Page/通州街.md "wikilink")，並延長至[長沙灣一帶](../Page/長沙灣.md "wikilink")，今天該區最近距離的警署為[深水埗警署](../Page/深水埗警署.md "wikilink")。

## 相關街道

  - [警署徑](../Page/警署徑.md "wikilink")，位於[離島區](../Page/離島區.md "wikilink")[長洲](../Page/長洲_\(香港\).md "wikilink")，因[長洲警署而得名](../Page/長洲警署.md "wikilink")。

## 參考文獻

<div class="references-small">

<references />

  -

</div>

[分類:香港街道](../Page/分類:香港街道.md "wikilink")
[分類:香港警察](../Page/分類:香港警察.md "wikilink")
[分類:香港文化](../Page/分類:香港文化.md "wikilink")
[分類:香港歷史](../Page/分類:香港歷史.md "wikilink")