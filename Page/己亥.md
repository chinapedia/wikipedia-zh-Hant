**己亥**为[干支之一](../Page/干支.md "wikilink")，顺序为第36个。前一位是[戊戌](../Page/戊戌.md "wikilink")，后一位是[庚子](../Page/庚子.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之己屬陰之土](../Page/天干.md "wikilink")，[地支之亥屬陰之水](../Page/地支.md "wikilink")，是土尅水相尅。

## 己亥年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")36年称“**己亥年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘39，或年份數減3，除以10的餘數是6，除以12的餘數是0，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“己亥年”：

<table>
<caption><strong>己亥年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li>39年</li>
<li>99年</li>
<li>159年</li>
<li>219年</li>
<li>279年</li>
<li>339年</li>
<li>399年</li>
<li>459年</li>
<li>519年</li>
<li>579年</li>
<li>639年</li>
<li>699年</li>
<li>759年</li>
<li>819年</li>
<li>879年</li>
<li>939年</li>
<li>999年</li>
</ul></td>
<td><ul>
<li>1059年</li>
<li>1119年</li>
<li>1179年</li>
<li>1239年</li>
<li>1299年</li>
<li>1359年</li>
<li>1419年</li>
<li>1479年</li>
<li>1539年</li>
<li>1599年</li>
<li>1659年</li>
<li>1719年</li>
<li>1779年</li>
<li>1839年</li>
<li>1899年</li>
<li>1959年</li>
</ul></td>
<td><ul>
<li>2019年</li>
<li>2079年</li>
<li>2139年</li>
<li>2199年</li>
<li>2259年</li>
<li>2319年</li>
<li>2379年</li>
<li>2439年</li>
<li>2499年</li>
<li>2559年</li>
<li>2619年</li>
<li>2679年</li>
<li>2739年</li>
<li>2799年</li>
<li>2859年</li>
<li>2919年</li>
<li>2979年</li>
</ul></td>
</tr>
</tbody>
</table>

## 己亥月

天干丙年和辛年，[立冬到](../Page/立冬.md "wikilink")[大雪的時間段](../Page/大雪.md "wikilink")，就是**己亥月**：

  - ……
  - 1976年11月立冬到12月大雪
  - 1981年11月立冬到12月大雪
  - 1986年11月立冬到12月大雪
  - 1991年11月立冬到12月大雪
  - 1996年11月立冬到12月大雪
  - 2001年11月立冬到12月大雪
  - 2006年11月立冬到12月大雪
  - ……

## 己亥日

## 己亥時

天干丙日和辛日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）21時到23時，就是**己亥時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/己亥年.md "wikilink")