[HK_Central_己連拿利_Glenealy_rainy_day_June-2010.JPG](https://zh.wikipedia.org/wiki/File:HK_Central_己連拿利_Glenealy_rainy_day_June-2010.JPG "fig:HK_Central_己連拿利_Glenealy_rainy_day_June-2010.JPG")之一段\]\]
[Hong_Kong_Central_Glenealy_Road_Sign.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Central_Glenealy_Road_Sign.jpg "fig:Hong_Kong_Central_Glenealy_Road_Sign.jpg")
[Glenealy_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Glenealy_\(Hong_Kong\).jpg "fig:Glenealy_(Hong_Kong).jpg")。\]\]
**己連拿利**（）同時是[香港的街道和地方](../Page/香港.md "wikilink")，地名全名為**己連拿利谷**（），位於[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[中環的](../Page/中環.md "wikilink")[政府山以西](../Page/政府山.md "wikilink")、[畢打山以東](../Page/畢打山.md "wikilink")。

己連拿利早期曾名為**鐵崗**及**義律谷**。義律谷改名，與[義律去職有所關係](../Page/義律.md "wikilink")。\[1\]鐵崗名字的由來是來自[干德道的鐵缸](../Page/干德道.md "wikilink")。己連拿利之前名為忌連拿利，但因忌有「忌諱」的意思，1970年左右改為己連拿利。\[2\]

## 道路位置

而己連拿利這街道自為**己連拿利谷**起分為三段，向南伸延至山上的[干德道](../Page/干德道.md "wikilink")。

北段自[雲咸街](../Page/雲咸街.md "wikilink")、[下亞厘畢道交界](../Page/下亞厘畢道.md "wikilink")，[藝穗會的所在地起](../Page/藝穗會.md "wikilink")，至[堅道及](../Page/堅道.md "wikilink")[上亞厘畢道相遇交架](../Page/上亞厘畢道.md "wikilink")，此處有一行人隧道及公眾洗手間（己連拿利公廁）。

而中段自穿過行人隧道起為行車路，將[堅道](../Page/堅道.md "wikilink")、[上亞厘畢道及](../Page/上亞厘畢道.md "wikilink")[亞畢諾道交界](../Page/亞畢諾道.md "wikilink")，上山南行至、[香港明愛大廈西餐廳入口](../Page/香港明愛大廈.md "wikilink")、[香港動植物公園西邊入口](../Page/香港動植物公園.md "wikilink")[高主教書院地下入口及](../Page/高主教書院.md "wikilink")[香港聖母無原罪主教座堂停車場連接](../Page/香港聖母無原罪主教座堂.md "wikilink")，再向南行則是一段只可以行人的小山路，有山水流下。

南段則在[羅便臣花園以南](../Page/羅便臣花園.md "wikilink")、[香雪道以北之間](../Page/香雪道.md "wikilink")，由[行車天橋及斜路構成](../Page/行車天橋.md "wikilink")，稱為**己連拿利天橋**（），由小山路及樓梯開始駁上行車天橋，之後由行車斜路上[干德道及](../Page/干德道.md "wikilink")[香雪道交界](../Page/香雪道.md "wikilink")。該段可給車輛由[羅便臣道連接上](../Page/羅便臣道.md "wikilink")[干德道](../Page/干德道.md "wikilink")，於1979年12月13日由時任立法局議員[鄧蓮如主持啟用儀式](../Page/鄧蓮如.md "wikilink")。

## 道路名稱

該道路其英文名「Glenealy」是香港少數沒有英文後綴，以一個英文單字作為路名的道路，其他類似個案包括[金鐘的](../Page/金鐘.md "wikilink")[金鐘道](../Page/金鐘道.md "wikilink")（Queensway）、[荔枝角](../Page/荔枝角.md "wikilink")[美孚新邨的](../Page/美孚新邨.md "wikilink")[百老匯街](../Page/百老匯街.md "wikilink")（Broadway）及[堅尼地城的](../Page/堅尼地城.md "wikilink")[士美菲路](../Page/士美菲路.md "wikilink")（Smithfield），而己連拿利更是香港唯一同時沒有中文及英文後綴的道路。

## 相關參見

  - 己連拿小學

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:中環街道](../Category/中環街道.md "wikilink")
[Category:香港谷地](../Category/香港谷地.md "wikilink")

1.
2.  [探古尋源：細數殖民時期不吉利街道譯名變遷](http://news.tvb.com/story/5573a9eb6db28ce653000001/598f86d8e603833d7cd1bc54/)
    無線新聞，2017年8月13日