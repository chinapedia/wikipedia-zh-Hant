**巴**（符号bar）、**毫巴**（符号mbar）都是表示壓强的[单位](../Page/单位.md "wikilink")。它们不属于[國際單位制或](../Page/國際單位制.md "wikilink")[厘米-克-秒制](../Page/厘米-克-秒制.md "wikilink")，但接受與國際單位並用。因为巴與大氣壓力相似，所以被廣泛用於描述壓力。它在[歐洲聯盟國家裡律法上被承認](../Page/歐洲聯盟.md "wikilink")。\[1\]

## 定义

巴和毫巴被定义为：

  - 1 bar（巴） = 100,000 Pa（[帕](../Page/帕.md "wikilink")） = 100 kPa（千帕） =
    1,000,000 dyn/cm<sup>2</sup>（[达因](../Page/达因.md "wikilink")/平方厘米）
  - 1 mbar（毫巴） = 0.001 bar（巴） = 0.1 kPa（千帕） = 1 hPa（百帕） =
    1,000 dyn/cm²（达因/平方厘米）

（帕斯卡等于一[牛頓每](../Page/牛頓_\(單位\).md "wikilink")[平方米](../Page/平方米.md "wikilink")）

## 换算

  - 1 bar = 100 kN/m<sup>2</sup> ≈ 1.0197 kg/cm<sup>2</sup>
  - 1 mbar = 100 Pa = 1 hPa = 0.1 kPa

## 起源

“巴”源自（baros），意思是[重量](../Page/重量.md "wikilink")。巴的正式符号是“bar”，旧符号“b”已废弃不用，同样地，毫巴的现行符号是“mbar”，旧符号“mb”也已废弃不用。

巴和毫巴的概念由英国气象学家先生于1909年发明，于1929年为国际所接受。

## 讨论

[气压通常以毫巴作单位](../Page/气压.md "wikilink")，如[标准大气压被定义为](../Page/标准大气压.md "wikilink")1013.25毫巴（[百帕](../Page/百帕.md "wikilink")），等于1.01325巴。长期以来，世界各地的气象学者即使用毫巴作为测量大气压的众多单位之一。这导致国际单位[帕花了一段时间才为人们所广泛采用](../Page/帕.md "wikilink")。毫巴现在仍然被广泛使用，虽然各国官方都渐渐过渡到其实数字是一样的国际单位：hPa（百帕）。但因为在其他领域里基本上很少会用到表示壹佰的“hecto”前缀所以通常会用“千帕”代替。如加拿大的[天气预报中](../Page/天气预报.md "wikilink")，就通常使用kPa（[千帕](../Page/千帕.md "wikilink")）作为单位，也作cbar（厘巴）。\[2\]

今日，毫巴通常在提及[飓风或龙卷风暴时使用](../Page/飓风.md "wikilink")，因为更低的中心气压即意味着更高的[风速和更大的威力](../Page/飓风等级.md "wikilink")。

“mb”的[统一码符号为](../Page/统一码.md "wikilink")“”（U+33D4）。"bar"的统一码则是“”（U+3374）。

## 参考文献

## 参见

  - [压强](../Page/压强.md "wikilink")
  - [帕斯卡](../Page/帕斯卡.md "wikilink")

## 外部链接

  - [官方国际单位网站： Table 8. Non-SI units accepted for use with the
    SI](https://web.archive.org/web/20080821211324/http://www.bipm.org/en/si/si_brochure/chapter4/table8.html)


  -
  -
  -
[Category:气压单位](../Category/气压单位.md "wikilink")

1.  英國標準 BS 350:2004 *Conversion Factors for Units*
2.  [Weather - Environment
    Canada](http://www.weatheroffice.gc.ca/canada_e.html)