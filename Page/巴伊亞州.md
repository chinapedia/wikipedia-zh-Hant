**巴伊亚**（）是[巴西的](../Page/巴西.md "wikilink")[26个州之一](../Page/巴西行政区划.md "wikilink")，地处东北部。面积565,733平方千米，首府是[萨尔瓦多](../Page/萨尔瓦多_\(巴西\).md "wikilink")。巴伊亚北部是[阿拉戈阿斯州](../Page/阿拉戈阿斯.md "wikilink")、[塞尔希培州](../Page/塞尔希培州.md "wikilink")、[伯南布哥州和](../Page/伯南布哥州.md "wikilink")[皮奥伊州](../Page/皮奥伊州.md "wikilink")，东部是[大西洋](../Page/大西洋.md "wikilink")，西部有[戈亚斯州和](../Page/戈亚斯州.md "wikilink")[托坎廷斯州](../Page/托坎廷斯州.md "wikilink")，南部有[米纳斯吉拉斯州和](../Page/米纳斯吉拉斯州.md "wikilink")[聖埃斯皮里圖州](../Page/聖埃斯皮里圖州.md "wikilink")。

2015年估計人口有15,203,934人\[1\]。

## 地理

巴伊亚州的地区分为三部分：

  - 大西洋沿海森林区（）
  - 环巴伊亚湾区 （）
  - 巴伊亚内地高原区（）

巴伊亚的主要大城市計有：[萨尔瓦多](../Page/萨尔瓦多_\(巴西\).md "wikilink")、[费拉迪圣安娜](../Page/费拉迪圣安娜.md "wikilink")、[维多利亚](../Page/维多利亚-达孔基斯塔.md "wikilink")、[伊塔布纳](../Page/伊塔布纳.md "wikilink")
、[伊利乌斯](../Page/伊利乌斯.md "wikilink")、[茹阿泽鲁等](../Page/茹阿泽鲁.md "wikilink")。

较大的河流有[圣弗朗西斯科河](../Page/圣弗朗西斯科河.md "wikilink")、[巴拉瓜苏河](../Page/巴拉瓜苏河.md "wikilink")
（）、[热基蒂尼奥尼亚河](../Page/热基蒂尼奥尼亚河.md "wikilink")（）、[伊达皮苦鲁河](../Page/伊达皮苦鲁河.md "wikilink")（）、
[卡皮瓦里河](../Page/卡皮瓦里河.md "wikilink")（）和。

该地属于[热带干湿季气候](../Page/热带干湿季气候.md "wikilink")，州内长期干旱。

## 经济

巴伊亚的经济以[农业为基础](../Page/农业.md "wikilink")。[农作物主要有](../Page/农作物.md "wikilink")[甘蔗](../Page/甘蔗.md "wikilink")、[木薯](../Page/木薯.md "wikilink")、[大豆](../Page/大豆.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[可可豆](../Page/可可豆.md "wikilink")、[椰子等](../Page/椰子.md "wikilink")。巴伊亚是巴西出口可可豆最重要的地区。工业有[化工](../Page/化工.md "wikilink")、[食品](../Page/食品.md "wikilink")、矿产，也有[石油和](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")。[旅游业对巴伊亚的经济发展越来越重要](../Page/旅游业.md "wikilink")。沿海地区有很多漂亮的沙滩，而名胜古迹也不少。巴伊亚已经成为了巴西旅游业最重要的地方之一。

## 历史

[葡萄牙探险者](../Page/葡萄牙.md "wikilink")[卡布劳](../Page/卡布劳.md "wikilink")1500年来到今天的巴伊亚[塞古羅港就发现了巴西](../Page/塞古羅港.md "wikilink")。于是从1534年开始，就有移民从欧州来到巴西。巴伊亚第一个省长Tomé
de Souza建立了萨尔瓦多，它成为巴西的第一个首都，也是政治、管理、宗教中心。同时Tomé de
Souza开始贩卖[非洲](../Page/非洲.md "wikilink")[奴隶](../Page/奴隶.md "wikilink")，开发甘蔗生产。17世纪，[荷兰和](../Page/荷兰.md "wikilink")[法国侵掠巴伊亚](../Page/法国.md "wikilink")，1624年荷兰占领萨尔瓦多，但一年以后就解放了。

1822年，巴西宣布独立，但巴伊亚在1823年7月2日之前仍被葡萄牙占领，所以巴伊亚是加入巴西联邦的最后一州。甚至到今天，7月2日仍是巴伊亚的独立节。由于巴伊亚人要求更多的自主权，因此有了几次暴力的争执。

## 文化

由于历史的原因，巴伊亚是巴西受非洲影響最大的地方，是非巴文化中心。[约鲁巴式的](../Page/约鲁巴.md "wikilink")宗教，武术是受非洲影响的典型例子。巴伊亚还有[印地安人](../Page/印地安.md "wikilink")，
比如，住在本州内地，大西洋沿海岸南部。

巴伊亚也因音乐而闻名，[多利瓦·卡伊米](../Page/多利瓦·卡伊米.md "wikilink")（），[乔安·
吉布多](../Page/乔安·_吉布多.md "wikilink")（），[吉布多·吉尔](../Page/吉布多·吉尔.md "wikilink")（），[卡耶塔诺·维罗索](../Page/卡耶塔诺·维罗索.md "wikilink")
（），[玛丽亚·贝登妮亚](../Page/玛丽亚·贝登妮亚.md "wikilink")（），[达妮埃拉·玛克丽等音乐家都是巴伊亚人](../Page/达妮埃拉·玛克丽.md "wikilink")，还有
欧罗敦（）， [Ara Ketu](../Page/Ara_Ketu.md "wikilink")， [Ilê
Aiyê等乐团也来自巴伊亚](../Page/Ilê_Aiyê.md "wikilink")。文学方面，巴西20世纪最著名的文学家，[若热·阿马多](../Page/若热·阿马多.md "wikilink")（），是巴伊亚[伊利乌斯人](../Page/伊利乌斯.md "wikilink")。

## 參考文獻

[Category:巴西行政區劃](../Category/巴西行政區劃.md "wikilink")
[Category:1823年建立的一級行政區](../Category/1823年建立的一級行政區.md "wikilink")
[巴西](../Category/咖啡豆產地.md "wikilink")

1.