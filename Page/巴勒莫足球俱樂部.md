**巴勒莫足球俱樂部**（****）乃一家位於[意大利](../Page/意大利.md "wikilink")[西西里島首府](../Page/西西里島.md "wikilink")[巴勒莫市的足球俱樂部](../Page/巴勒莫市.md "wikilink")，現時在[意大利足球乙級聯賽](../Page/意大利足球乙級聯賽.md "wikilink")
聯賽作賽，球隊制服為粉紅色。

它成立於1900年，主場是[巴爾貝拉球場](../Page/巴爾貝拉球場.md "wikilink")，能夠容納 36,980
人。巴勒莫曾於2004–05年升上甲組，曾在聯賽作客摸和[國際米蘭](../Page/國際米蘭.md "wikilink")1-1，又於主場勝[祖雲達斯](../Page/祖雲達斯.md "wikilink")，於2006–07球季參加[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")。

## 歷史

## 主場球場

球场位于西西利岛西南部

## 主要成就

  - [意大利杯亞軍](../Page/意大利杯.md "wikilink")：1973–74、1978–79
  - [意乙冠軍](../Page/意大利乙組足球聯賽.md "wikilink")：1931–32、1947–48、1967–68、2003–04、2013–14
  - [意丙冠軍](../Page/意丙.md "wikilink")：1941–42、1984–85、1992–93、2000–01

## 著名球員

  - [-{zh-hans:法比奥·格罗索; zh-hant:格羅素;}-](../Page/法比奥·格罗索.md "wikilink")
  - [巴沙利](../Page/巴沙利.md "wikilink")
  - [基斯頓·薩卡度](../Page/基斯頓·薩卡度.md "wikilink")
  - [-{zh-hans:卢卡·托尼; zh-hant:東尼;}-](../Page/卢卡·托尼.md "wikilink")
  - [-{zh-hans:保罗·迪巴拉; zh-hant:保羅·戴巴拿;}-](../Page/保罗·迪巴拉.md "wikilink")

## 參見

  - [西西里德比](../Page/西西里德比.md "wikilink")

## 參考資料

## 外部連結

  - [巴勒莫官方網站](http://www.ilpalermocalcio.it)

[分類:1900年建立的足球俱樂部](../Page/分類:1900年建立的足球俱樂部.md "wikilink")

[P](../Category/意大利足球俱樂部.md "wikilink")
[P](../Category/巴勒莫.md "wikilink")