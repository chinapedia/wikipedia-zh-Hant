**巴塔哥尼亞**（）是一家主营[户外服装的公司](../Page/户外.md "wikilink")，由[伊馮·喬伊納德](../Page/伊馮·喬伊納德.md "wikilink")（[Yvon
Chouinard](../Page/:en:Yvon_Chouinard.md "wikilink")）于1972年创建，总部位于[美国](../Page/美国.md "wikilink")[加利福尼亚](../Page/加利福尼亚.md "wikilink")[文圖拉](../Page/文圖拉.md "wikilink")。该公司参与了几个环保运动，具有社会责任心。

巴塔哥亞将其1%的销售额或者10%的[利润](../Page/利润.md "wikilink")（取更多者），捐献给[环保组织](../Page/环保主义者.md "wikilink")，并共同创建了"1%
For the Planet"的商业联盟。自1985年开展以来，巴塔哥尼亞已经向1000多个环保组织，共计捐献了7400万美元\[1\]。。

巴塔哥尼亞经常将环保运动的宣传放在其产品目录、广告中，这些运动包括preventing oil drilling in the Alaska
Wildlife
Refuge（阻止[阿拉斯加原油钻井破坏野生动物庇护所](../Page/阿拉斯加.md "wikilink")）、"Ocean
As A Wilderness"（保留海洋荒野）、"Don't Dam Patagonia"（拒绝Patagonia水坝）。

## 外部链接

  - [Patagonia公司官网](http://www.patagonia.com/)
  - [1% For the Planet](http://www.onepercentfortheplanet.org/)

[Category:美国公司](../Category/美国公司.md "wikilink")
[Category:戶外服裝品牌](../Category/戶外服裝品牌.md "wikilink")
[Category:1972年成立的公司](../Category/1972年成立的公司.md "wikilink")

1.