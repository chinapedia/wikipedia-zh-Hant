**巴尼奥-迪罗马涅**（），是[意大利](../Page/意大利.md "wikilink")[费利-切塞纳省的一个](../Page/费利-切塞纳省.md "wikilink")[市镇](../Page/市镇_\(意大利\).md "wikilink")。总面积233.44平方公里，人口6191人，人口密度26.5人/平方公里（2009年）。ISTAT代码为040001。

## 友好城市

  - [瑞士](../Page/瑞士.md "wikilink")[拉珀斯维尔](../Page/拉珀斯维尔.md "wikilink")

## 外部链接

  - [www.bagnodiromagnaturismo.it/](http://www.bagnodiromagnaturismo.it/)

[B](../Category/弗利-切塞納省市鎮.md "wikilink")
[Category:弗利-切塞納省市鎮](../Category/弗利-切塞納省市鎮.md "wikilink")