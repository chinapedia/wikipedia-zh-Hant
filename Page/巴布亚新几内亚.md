<File:Papua> New Guinea (orthographic projection).svg{{\!}}285 |國土面積數據 =
462,840 |面積排名 = 53 |水域率百分比 = 2 |國家首都名稱 =
[莫爾茲比港](../Page/莫爾茲比港.md "wikilink") |中央政府所在地 =
|最大行政區名稱 = |最大城市名稱 = [莫爾茲比港](../Page/莫爾茲比港.md "wikilink") |最高點 =
|最長河流 = |最大湖泊 = |海岸線 = 5,152[公里](../Page/公里.md "wikilink") |國鳥 =
[極樂鳥](../Page/極樂鳥.md "wikilink") |時區名稱 = |時區 = +10 |結束時區 = |夏時制 =
|結束夏時制時區 = |人口估計年份 = 2011年 |總人口數據 = 7,059,653 |人口排名 = 104 |人口密度數據 = 15
|人口密度排名 = 201 |電壓 = |插座 = |頻率 = |官方語言 =
[英語](../Page/英語.md "wikilink")、[巴布亞皮欽語](../Page/巴布亞皮欽語.md "wikilink")、[希里摩图语](../Page/希里摩图语.md "wikilink")
|官方文字 =
[英語](../Page/英語.md "wikilink")、[巴布亞皮欽語](../Page/巴布亞皮欽語.md "wikilink")、[希里摩图语](../Page/希里摩图语.md "wikilink")
|民族 =
[巴佈亞人](../Page/巴佈亞人.md "wikilink")、[馬來人](../Page/馬來人.md "wikilink")、其他[少數族群](../Page/少數族群.md "wikilink")
|主要宗教 = |曆法 = |主要節日 = |道路通行方向 = 靠左行駛 |國家元首職稱 = 女王 |現任國家元首名字 = 伊莉莎白二世
|政府代表職稱 = 總督 |現任政府代表名字 = 鮑伯·達達|Bob Dadae |政府首腦職稱 = 總理 |現任政府首腦名字 =
彼得·奧尼爾 |政治制度一 = |行政首腦職稱 = |現任行政首腦名字 = |立法首腦職稱 = |現任立法首腦名字 = |司法首腦職稱 =
|現任司法首腦名字 = |考試首腦職稱 = |現任考試首腦名字 = |監察首腦職稱 = |現任監察首腦名字 = |政治制度二 =
|現任上議院立法首腦名字 = |現任下議院立法首腦名字 = |政治制度三 = |其他首腦一 = |其他首腦職稱一 =
|現任其他首腦名字一 = |其他首腦二 = |其他首腦職稱二 = |現任其他首腦名字二 = |其他首腦三 = |他首腦職稱三 =
|現任其他首腦名字三 = |國家憲法 = |國家結構形式 = |國家政權
=[巴布亚新几内亚国民议会](../Page/巴布亚新几内亚国民议会.md "wikilink")
|政治體制 =
[君主立憲制](../Page/君主立憲制.md "wikilink")、[議會制](../Page/議會制.md "wikilink")
|法律體系 = |國內生產總值購買力平價估計年份 = 2016年 |國內生產總值購買力平價數據 =
213.13億[美元](../Page/美元.md "wikilink")\[1\] |國內生產總值購買力平價數據排名 =
141 |人均國內生產總值購買力平價數據 = 2,694美元\[2\] |人均國內生產總值購買力平價數據排名 = 154 |國內生產總值估計年份
= 2016年 |國內生產總值數據 = 156.15億美元\[3\] |國內生產總值數據排名 = 111 |人均國內生產總值數據 =
1,973美元\[4\] |人均國內生產總值數據排名 = 132 |人類發展指數年份 = 2017年 |人類發展指數 =
0.544\[5\] |人類發展指數排名 = 153 |人類發展指數等級 =
<span style="color:#990000">低</span> |中央銀行 =
[巴布亞紐幾內亞銀行](../Page/巴布亞紐幾內亞銀行.md "wikilink")
|貨幣單位 = [巴布亚新几内亚基那](../Page/巴布亚新几内亚基那.md "wikilink") |貨幣代碼 = PGK |貨幣符號 =
|吉尼係數 = |立國日期 = 1975年9月16日 |立國事件 = 自獨立 |立國事件日期 = 1975年9月16日 |國家象徵 =
[极乐鸟](../Page/极乐鸟.md "wikilink") |國家代碼 = PNG |國際域名縮寫 =
[.pg](../Page/.pg.md "wikilink") |國際電話區號 = +675 |現役軍人數 = |現役軍人數排名 =
|現役軍人數年份 = |備註 = |}}

**巴布亚新几内亚独立国**（），通称**巴布亚新几内亚**（Papua New
Guinea；[巴布亚皮钦语](../Page/巴布亚皮钦语.md "wikilink")：Papua
Niugini；[希里摩图语](../Page/希里摩图语.md "wikilink")：Papua Niu
Gini），简称**巴新**，是位於[太平洋西南部的一個](../Page/太平洋.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")[國家](../Page/國家.md "wikilink")，主要涵蓋[新幾內亞島東半部](../Page/新幾內亞島.md "wikilink")，西鄰[印度尼西亞的](../Page/印度尼西亞.md "wikilink")[巴布亞省](../Page/巴布亞省.md "wikilink")，南部和東部分別與[澳洲和](../Page/澳洲.md "wikilink")[所羅門群島隔海相望](../Page/所羅門群島.md "wikilink")。

## 历史

公元前8000年新幾內亞高地已有人定居。1511年[葡萄牙人發現新幾內亞島](../Page/葡萄牙.md "wikilink")。直至19世紀初，西方對此島了解不多。

[thumb](../Page/file:Territorios_coloniales_de_Nueva_Guinea.PNG.md "wikilink")

1884年英、德相繼瓜分新幾內亞島東半部及附近島嶼。1906年英屬新幾內亞交[澳洲聯邦管理](../Page/澳洲聯邦.md "wikilink")，改稱澳屬巴布亞領地。

[Australian_infantry_and_armour_at_Buna_(AWM_014008).jpg](https://zh.wikipedia.org/wiki/File:Australian_infantry_and_armour_at_Buna_\(AWM_014008\).jpg "fig:Australian_infantry_and_armour_at_Buna_(AWM_014008).jpg")[澳洲軍隊攻擊](../Page/澳大利亞國防軍.md "wikilink")[日軍陣地](../Page/日軍.md "wikilink")。\]\]1919年一战结束后，澳军佔領[德属新几内亚](../Page/德属新几内亚.md "wikilink")。1920年12月17日[國際聯盟決定委託澳管理](../Page/國際聯盟.md "wikilink")；[二战中新幾內亞一度為日軍佔領](../Page/二战.md "wikilink")，戰後聯合國委托澳繼續管理德屬部分，1949年澳將原英屬和德屬兩部分合併為一個行政單位，稱「巴布亞紐幾內亞領地」。

1973年12月1日實行內部自治。1975年9月16日脫離[澳洲獨立](../Page/澳洲.md "wikilink")，成為[英聯邦成員國](../Page/英聯邦王國.md "wikilink")。

## 地理

[Papua_New_Guinea_map.png](https://zh.wikipedia.org/wiki/File:Papua_New_Guinea_map.png "fig:Papua_New_Guinea_map.png")
[Goroka.JPG](https://zh.wikipedia.org/wiki/File:Goroka.JPG "fig:Goroka.JPG")
[GorokaMarket.jpg](https://zh.wikipedia.org/wiki/File:GorokaMarket.jpg "fig:GorokaMarket.jpg")
[Huli_Wigman_visit_Cooktown,_Australia_2005.jpg](https://zh.wikipedia.org/wiki/File:Huli_Wigman_visit_Cooktown,_Australia_2005.jpg "fig:Huli_Wigman_visit_Cooktown,_Australia_2005.jpg")

巴布亞紐幾內亞面積462,840平方公里，其国土包括所有島嶼在内位於[赤道至南纬](../Page/赤道.md "wikilink")12度，東經140度至160度之间。

巴布亞新幾內亞為[赤道多雨氣候區](../Page/赤道多雨氣候.md "wikilink")，氣候炎熱且降雨量大，形成一個個[熱帶雨林](../Page/熱帶雨林.md "wikilink")，其國土多被山和[熱帶雨林覆蓋](../Page/熱帶雨林.md "wikilink")。因巴布亞新幾內亞是熱帶雨林為主，所以在生物种类上相當豐富。植物方面都主要是一些高達數十米的闊葉樹木，而動物方面則主要以鳥類和有袋類動物為主。[极乐鸟是巴布亚新几内亚特有的鸟类](../Page/极乐鸟.md "wikilink")，被绘在其国徽中。

## 行政區劃

[Papua_new_guinea_provinces_(numbers)_2012.png](https://zh.wikipedia.org/wiki/File:Papua_new_guinea_provinces_\(numbers\)_2012.png "fig:Papua_new_guinea_provinces_(numbers)_2012.png")
[Papua_New_Guinea_in_the_World.svg](https://zh.wikipedia.org/wiki/File:Papua_New_Guinea_in_the_World.svg "fig:Papua_New_Guinea_in_the_World.svg")

巴布亞新幾內亞分為四個[大區](../Page/巴布亞新幾內亞大區列表.md "wikilink")（Region），雖然這並不是正式的行政區劃，但在政府，商業，體育和其他活動的許多方面都非常重要。

  - 巴布亞新幾內亞共有22個省級行政區：20個省、1個自治區（[布干維爾自治區](../Page/布干維爾自治區.md "wikilink")）以及[國家首都區](../Page/國家首都區_\(巴布亞紐幾內亞\).md "wikilink")。每個省可分為一個或多個區（District），一個區又分為一個或多個地方行政區域。

首都：莫尔兹比港

1.  [中央省](../Page/中央省_\(巴布亞紐幾內亞\).md "wikilink")（Central）
2.  [欽布省](../Page/欽布省.md "wikilink")（Chimbu）
3.  [東高地省](../Page/東高地省.md "wikilink")（Eastern Highlands）
4.  [東新不列顛省](../Page/東新不列顛省.md "wikilink")（East New Britain）
5.  [東塞皮克省](../Page/東塞皮克省.md "wikilink")（East Sepik）
6.  [恩加省](../Page/恩加省.md "wikilink")（Enga）
7.  [海灣省](../Page/海灣省.md "wikilink")（Gulf）
8.  [馬當省](../Page/馬當省.md "wikilink")（Madang）
9.  [馬努斯省](../Page/馬努斯省.md "wikilink")（Manus）
10. [米爾恩灣省](../Page/米爾恩灣省.md "wikilink")（Milne Bay）
11. [莫雷貝省](../Page/莫雷貝省.md "wikilink")（Morobe）
12. [新愛爾蘭省](../Page/新愛爾蘭省.md "wikilink")（New Ireland）
13. [北部省](../Page/北部省.md "wikilink")（Oro）Northern
14. [布干維爾自治区](../Page/布干維爾自治区.md "wikilink")（Autonomous Region of
    Bougainville）
15. [南高地省](../Page/南高地省.md "wikilink")（Southern Highlands）
16. [西部省](../Page/西部省_\(巴布亞紐幾內亞\).md "wikilink")（Western）
17. [西高地省](../Page/西高地省.md "wikilink")（Western Highlands）
18. [西新不列顛省](../Page/西新不列顛省.md "wikilink")（West New Britain）
19. [桑道恩省](../Page/桑道恩省.md "wikilink")（Sandaun）West Sepik
20. [國家首都區](../Page/国家首都区_\(巴布亚新几内亚\).md "wikilink")（National Capital
    District）
21. [赫拉省](../Page/赫拉省.md "wikilink")（Hela）
22. [吉瓦卡省](../Page/吉瓦卡省.md "wikilink")（Jiwaka）

## 政治

巴布亚新几内亚是[議會民主制的](../Page/議會民主制.md "wikilink")[君主立憲國家](../Page/君主立憲.md "wikilink")，国家元首是女王[伊丽莎白二世](../Page/伊丽莎白二世.md "wikilink")，女王不在巴布亚新几内亚時由總督代爲行使元首職責。巴布亚新几内亚立法機構為一院制議會，政府首腦是總理大臣，由議會内多數黨的領袖擔任。議會的111個議席中有22席為21個省和首都區的22名地方首長的官守議席。

巴布亞新幾內亞有40多個[政黨](../Page/政黨.md "wikilink")，其中比較具有影響力的政黨有國民聯盟黨、[人民行動黨](../Page/人民行動黨_\(巴布亞紐幾內亞\).md "wikilink")、聯合資源黨、聯合黨、潘古黨、人民全國代表大會黨、人民民主運動黨、人民勞動黨、[人民進步黨](../Page/人民進步黨_\(巴布亞紐幾內亞\).md "wikilink")、[巴布亞新幾內亞黨及新一代黨等](../Page/巴布亞新幾內亞黨.md "wikilink")。

巴布亚新几内亚是[英联邦王國](../Page/英联邦王國.md "wikilink")。

## 生態

[Papua_New_Guinea_(5986599443).jpg](https://zh.wikipedia.org/wiki/File:Papua_New_Guinea_\(5986599443\).jpg "fig:Papua_New_Guinea_(5986599443).jpg")。\]\]

[Fly_River.PNG](https://zh.wikipedia.org/wiki/File:Fly_River.PNG "fig:Fly_River.PNG")。\]\]

巴布亞紐幾內亞的一部分是澳大拉西亞生態區，其中還包括澳大利亞，新西蘭，印尼東部，和一些太平洋群島，包括所羅門群島和瓦努阿圖。

## 氣候

巴布亞紐幾內亞為[赤道氣候](../Page/赤道氣候.md "wikilink")，其特點是一年四季高溫，高濕度。平地年平均氣溫約24℃，溫度隨高度升高迅速降低，冬季有暴雨。

|                                                                                                                                                                                                                                                                 |     |     |     |     |    |    |    |    |    |    |     |     |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --- | --- | --- | --- | -- | -- | -- | -- | -- | -- | --- | --- |
| [月份](../Page/月份.md "wikilink")                                                                                                                                                                                                                                  | 一月  | 二月  | 三月  | 四月  | 五月 | 六月 | 七月 | 八月 | 九月 | 十月 | 十一月 | 十二月 |
| 最低溫 (°C)                                                                                                                                                                                                                                                        | 23  | 22  | 22  | 22  | 22 | 22 | 21 | 21 | 22 | 22 | 22  | 23  |
| 最高溫 (°C)                                                                                                                                                                                                                                                        | 31  | 31  | 31  | 31  | 30 | 30 | 29 | 30 | 30 | 31 | 32  | 32  |
| 平均溫 (°C)                                                                                                                                                                                                                                                        | 28  | 28  | 27  | 27  | 27 | 26 | 26 | 26 | 26 | 27 | 28  | 28  |
| 雨量（mm）                                                                                                                                                                                                                                                          | 179 | 196 | 190 | 120 | 65 | 39 | 26 | 25 | 33 | 35 | 55  | 121 |
| <small>Sources : [CapAustral](https://web.archive.org/web/20120513201902/http://capaustral.com/guide_voyage_papouasie_nouvelle_guinee.php), [www.studentsoftheworld.info](http://www.studentsoftheworld.info/informations_pays.php?Pays=PNG&Opt=climat)</small> |     |     |     |     |    |    |    |    |    |    |     |     |

## 經濟

[Papua_New_Guinea_Export_Treemap.png](https://zh.wikipedia.org/wiki/File:Papua_New_Guinea_Export_Treemap.png "fig:Papua_New_Guinea_Export_Treemap.png")
[OkTediMine.jpg](https://zh.wikipedia.org/wiki/File:OkTediMine.jpg "fig:OkTediMine.jpg")

巴布亞新幾內亞擁有豐富的自然資源，包括礦產和可再生資源，如森林，海洋（包括全球主要剩餘[金槍魚資源的很大一部分](../Page/金槍魚.md "wikilink")），以及農業的某些部分，人民所得約為2,084美元（2015年估計）。

巴布亞新幾內亞主要是以農業為主，據2003年資料顯示，農業佔巴布亞新幾內亞經濟34%。主要出口有礦產和農產[咖啡](../Page/咖啡.md "wikilink")、[可可](../Page/可可.md "wikilink")、[椰乾](../Page/椰乾.md "wikilink")、[棕油](../Page/棕油.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")、木材及海產等。

亚太经合组织领导人非正式会议，简称亚太经合峰会（英语：THE APEC ECONOMIC LEADERS'
MEETING）。举办次数：第26次。举办年份：2018年。举办日：11月12日-11月18日。举办国：巴布亚新几内亚。举办都市：莫尔兹比港

## 人口

巴布亚新几内亚在2014年人口估计为6,552,730，人口中位年龄是22.4岁。人口增长率是1.84%，出生率为24.89‰，死亡率为6.53‰。城市人口占13%。新生儿的死亡率是每千人39.67。人口出生时的平均预期寿命是66.85岁。人口识字率是62.4%。没有读写能力的人中妇女占了大部分。\[6\]

巴布亞紐幾內亞是在世界上最多元化的國家之一。\[7\]
巴布亞紐幾內亞的土著族群有數百個。大部分被稱為[巴布亞人](../Page/巴布亞人.md "wikilink")。他們的祖先在幾萬年前，來到新幾內亞地區。很多偏遠的部落與外界仍然只有輕微的接觸。其余的土著居民为[南岛民族](../Page/南岛民族.md "wikilink")，在少于四千年前迁移至该地区。

原住民的祖先在不到4萬年前抵達該地區。也有許多世界其他地方的居民，包括[中國](../Page/中國.md "wikilink")，[歐洲](../Page/歐洲.md "wikilink")，[澳洲](../Page/澳洲.md "wikilink")，[菲律賓](../Page/菲律賓.md "wikilink")，[波利尼西亞和](../Page/波利尼西亞.md "wikilink")[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")。在1975年，巴布亞[獨立](../Page/獨立.md "wikilink")，有40,000名外籍人（主要是澳大利亞和中國）在巴布亞紐幾內亞。\[8\]

## 文化

### 體育

體育是巴布亞紐幾內亞文化很重要的一部分，[聯盟式橄欖球是最受歡迎的體育項目](../Page/聯盟式橄欖球.md "wikilink")\[9\]。

## 教育

國家教授語言為[英語](../Page/英語.md "wikilink")，但巴布亞新幾內亞有大約850種不同的[方言](../Page/方言.md "wikilink")。

位于首都[莫爾茲比港](../Page/莫爾茲比港.md "wikilink")。[Port_Moresby_parliament_building_front,_by_Steve_Shattuck.jpg](https://zh.wikipedia.org/wiki/File:Port_Moresby_parliament_building_front,_by_Steve_Shattuck.jpg "fig:Port_Moresby_parliament_building_front,_by_Steve_Shattuck.jpg")

## 交通

因其多山崎嶇地形，國內陸路交通十分不便。首都[莫爾茲比港并無公路通往任何國內城市](../Page/莫爾茲比港.md "wikilink")，亦無鐵路連接其他城市。公路也僅有少數由外國捐贈的公共交通車輛行駛。

空運乃國內最重要運輸方式，於2007年全國有578座[機場](../Page/機場.md "wikilink")。其次是水運，全國有[港口](../Page/港口.md "wikilink")50多個。

## 参考资料

## 外部連結

  - [巴布亞紐幾內亞政府網站](https://web.archive.org/web/20081011090909/http://www.pm.gov.pg/)

  - [國家統計局](http://www.nso.gov.pg/)

  -
  -
  - [Papua New
    Guinea](https://web.archive.org/web/20111006235947/http://ucblibraries.colorado.edu/govpubs/for/png.htm)
    at *UCB Libraries GovPubs*

  -
  -
[B巴](../Category/大洋洲國家.md "wikilink")
[B巴](../Category/亚洲岛国.md "wikilink")
[\*](../Category/巴布亞新幾內亞.md "wikilink")
[Category:新几内亚](../Category/新几内亚.md "wikilink")
[Category:大洋洲岛国](../Category/大洋洲岛国.md "wikilink")
[P](../Category/君主立憲國.md "wikilink")
[Category:前英国殖民地](../Category/前英国殖民地.md "wikilink")
[Category:前德国殖民地](../Category/前德国殖民地.md "wikilink")

1.

2.
3.
4.
5.

6.  [CIA世界知识手册-巴纽](https://www.cia.gov/library/publications/the-world-factbook/geos/pp.html)

7.

8.  "[Papua New
    Guinea](http://www.britannica.com/EBchecked/topic/442191/Papua-New-Guinea)".
    *Encyclopædia Britannica* Online.

9.