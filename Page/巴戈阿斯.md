**巴戈阿斯**（[古波斯楔形文字](../Page/古波斯楔形文字.md "wikilink")：
，，），[波斯](../Page/波斯.md "wikilink")[阿契美尼德王朝的知名](../Page/阿契美尼德王朝.md "wikilink")[宦官](../Page/宦官.md "wikilink")、[维齐尔](../Page/维齐尔.md "wikilink")（[宰相](../Page/宰相.md "wikilink")），可能是[埃及人](../Page/埃及人.md "wikilink")。他幫助[亞他薛西斯三世剷除宗室](../Page/亞他薛西斯三世.md "wikilink")，掌管朝政，成為[维齐尔](../Page/维齐尔.md "wikilink")。西元前338年，他更毒殺亞他薛西斯三世，扶植其子[亞他薛西斯四世繼續掌握權力](../Page/亞他薛西斯四世.md "wikilink")。因阿爾塞斯不服，巴戈阿斯仍重施故技，將其殺害，轉而擁立[大流士三世](../Page/大流士三世.md "wikilink")，大流士三世[即位後](../Page/即位.md "wikilink")，立即將他[賜死](../Page/賜死.md "wikilink")。

## 传记

巴戈阿斯在成为[维齐尔之前是一名](../Page/维齐尔.md "wikilink")[宦官](../Page/宦官.md "wikilink")。在这一职位上，他和罗德岛的佣兵将领[门托耳结盟](../Page/罗德岛的门托耳.md "wikilink")，在他的帮助下，再一次使埃及成为了波斯帝国的一个[行省](../Page/行省.md "wikilink")（大概是前342年）。门托耳成为了沿海省份的将军，镇压了埃及叛乱并把希腊佣兵带给国王,
与此同时，巴戈阿斯管理着波斯总督们（satrap），在阿达薛西斯三世的统治末期，他权倾朝野，成为了波斯帝国真正的主人(Diod. xvi.
50; cf. Didymus, *Comm. in Demosth. Phil.* vi. 5)

亞他薛西斯四世是[亞他薛西斯三世和](../Page/亞他薛西斯三世.md "wikilink")[阿托莎的幼子](../Page/阿托撒\(阿尔塔薛西斯三世\).md "wikilink")，不具备继承波斯王位的可能。前338年，当巴戈阿斯失去了[亞他薛西斯三世的恩宠](../Page/亞他薛西斯三世.md "wikilink")，他谋杀了波斯王及其几乎所有的家人之后，亞他薛西斯四世出人意料的登基称王。巴戈阿斯为了保住权力，扶持了他认为易于控制的阿尔塞斯。在他执政的两年里，阿尔塞斯只是一个傀儡，巴戈阿斯在幕后操纵一切。最终，出于对巴戈阿斯的不满或者受到宫廷贵族的教唆，他们认为巴戈阿斯是如此无礼，[阿尔塞斯开始策划除掉巴戈阿斯](../Page/阿尔塞斯.md "wikilink")。然而，巴戈阿斯为了保住自己的权力，再一次先下手为强，杀害了**亞他薛西斯四世**。巴戈阿斯又扶持了一个阿尔塞斯的堂兄弟，即[大流士三世](../Page/大流士三世.md "wikilink")，作为新的波斯王。

当大流士三世试图摆脱巴戈阿斯支配的时候，巴戈阿斯又想[毒死他](../Page/毒死.md "wikilink")，但是大流士事先得知，并用武力脅迫巴戈阿斯[自鴆](../Page/自鴆.md "wikilink")(Diod.
xvii. 5; Johann. Antioch, p. 38, 39 ed. Müller; Arrian ii. 14. 5; Curt.
vi. 4. 10).\[1\]
之后的一则故事称，巴戈阿斯是一个埃及人，他杀死了[亞他薛西斯三世](../Page/亞他薛西斯三世.md "wikilink")，起因是波斯王杀死了神圣的[阿匹斯](../Page/阿匹斯.md "wikilink")(Aelian,
*Var. Hist.* vi. 8), 这个缺乏历史依据。\[2\]

巴戈阿斯的家在[苏萨](../Page/苏萨.md "wikilink")，有着巨额财产，被[亚历山大大帝赠送给了](../Page/亚历山大大帝.md "wikilink")[帕曼纽](../Page/帕曼纽.md "wikilink")；在巴比伦的花园，有最上乘品种的棕榈(palms)，这些是通过[泰奥弗拉斯托斯知道的](../Page/泰奥弗拉斯托斯.md "wikilink")。
(*Hist. Plant*, ii. 6; Plin. *Nat. Hist.* xiii. 41).\[3\]
[普鲁塔克提到](../Page/普鲁塔克.md "wikilink")，在一份亚历山大大帝给大流士的信中，愤怒的声称巴戈阿斯是谋害其父亲[腓力二世的凶手之一](../Page/腓力二世_（马其顿）.md "wikilink")。

在中提到巴戈阿斯是耶胡德[长官](../Page/长官.md "wikilink")(governor of Yehud)，\[4\]
并且是解决象岛犹太神庙的权威。巴戈阿斯，随同**德莱哈**（Delaiah）——撒玛利亚长官, 对此作出答复,
正如备忘录所记:
"牢记**巴国海**（Bagohi）和**德莱哈**（Delaiah）告诉我的，他们说，记住:在耶博的城堡里，你可以在之前谈论天堂之主的祭祀神殿
... 在这个地方重建它就像它以前一样..."\[5\]\[6\]

巴戈阿斯一个古代小说[Aethiopica中的角色](../Page/Aethiopica.md "wikilink"),
作者赫利奥多罗斯（Heliodorus）。在小说中,
他被描述成一个波斯帝国孟菲斯总督的可靠宦官。在一系列事件后,
他被埃塞俄比亚国王俘获并成为埃塞俄比亚宫廷中的一个仆人。\[7\]

## 注释

[Category:埃及歷史](../Category/埃及歷史.md "wikilink")
[Category:宦官](../Category/宦官.md "wikilink")

1.
2.
3.
4.   See also Porten, Bezalel, et al. *The Elephantine Papyri in
    English: Three Millennia of Cross-Cultural Continuity and Change.*
    Leiden: Brill, 1996.

5.  'Memorandum: You may speak before Arsham about the sacrificial
    temple of the God of heaven which is in the fortress of Yeb... to
    build it on its site as it was formerly..."

6.  Bezalel Porten; Ada Yardeni, *Textbook of Aramaic Documents from
    Ancient Egypt 1*. Jerusalem 1986, Letters, 76 (=TAD A4.9). The
    Aramaic text of this document is accessible through

7.