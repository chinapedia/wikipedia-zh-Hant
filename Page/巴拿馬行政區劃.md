**巴拿馬共和國**分為10省 （provincias）、3省級原住民自治區 （comarcas
indígenas，簡稱comarcas）和2市級 （corregimiento）原住民自治區。在省市之間有区
（Distritos）：

| 省份                                         | [首府](../Page/首府.md "wikilink")               |
| ------------------------------------------ | -------------------------------------------- |
| [博卡斯德爾托羅省](../Page/博卡斯德爾托羅省.md "wikilink") | [博卡斯德爾托羅](../Page/博卡斯德爾托羅.md "wikilink")     |
| [奇里基省](../Page/奇里基省.md "wikilink")         | [戴維](../Page/戴維_\(巴拿馬\).md "wikilink")       |
| [科克萊省](../Page/科克萊省.md "wikilink")         | [佩諾諾梅](../Page/佩諾諾梅.md "wikilink")           |
| [科隆省](../Page/科隆省_\(巴拿馬\).md "wikilink")   | [科隆](../Page/科隆_\(巴拿馬\).md "wikilink")       |
| [達連省](../Page/達連省.md "wikilink")           | [拉帕爾馬](../Page/拉帕爾馬_\(巴拿馬\).md "wikilink")   |
| [埃雷拉省](../Page/埃雷拉省.md "wikilink")         | [奇特雷](../Page/奇特雷.md "wikilink")             |
| [洛斯桑托斯省](../Page/洛斯桑托斯省.md "wikilink")     | [拉斯塔布拉斯](../Page/拉斯塔布拉斯.md "wikilink")       |
| [巴拿馬省](../Page/巴拿馬省.md "wikilink")         | [巴拿馬城](../Page/巴拿馬城.md "wikilink")           |
| [貝拉瓜斯省](../Page/貝拉瓜斯省.md "wikilink")       | [貝拉瓜斯的聖地亞哥](../Page/貝拉瓜斯的聖地亞哥.md "wikilink") |

| 原住民自治區（省级）                                   | 首府                                     |
| -------------------------------------------- | -------------------------------------- |
| [恩贝拉-沃内安特区](../Page/恩贝拉-沃内安特区.md "wikilink") | [烏尼翁喬科](../Page/烏尼翁喬科.md "wikilink")   |
| [库纳雅拉特区](../Page/库纳雅拉特区.md "wikilink")       | [埃爾波韋尼爾](../Page/埃爾波韋尼爾.md "wikilink") |
| [恩戈贝-布格雷特区](../Page/恩戈贝-布格雷特区.md "wikilink") | [奇奇卡](../Page/奇奇卡.md "wikilink")       |

| 原住民自治區（次省级）                                    | 所屬省份 |
| ---------------------------------------------- | ---- |
| [库纳-德马顿干迪特区](../Page/库纳-德马顿干迪特区.md "wikilink") | 巴拿馬省 |
| [库纳-德瓦尔干迪特区](../Page/库纳-德瓦尔干迪特区.md "wikilink") | 達連省  |

[\*](../Category/巴拿馬行政區劃.md "wikilink")