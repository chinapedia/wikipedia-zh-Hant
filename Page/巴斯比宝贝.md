**巴斯比宝贝**（）一般指[英格兰](../Page/英格兰.md "wikilink")[曼联足球俱乐部在](../Page/曼徹斯特聯足球俱樂部.md "wikilink")1950年代由俱乐部[青訓系統培养出的一批优秀的年青](../Page/青訓系統.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，由于当时俱乐部的主帅是[苏格兰人](../Page/苏格兰.md "wikilink")[马特·巴斯比](../Page/马特·巴斯比.md "wikilink")，因而得名。巴斯比宝贝在1955/56年和1956/57年赛季取得[英格兰顶级联赛二连冠](../Page/英格蘭足球聯賽.md "wikilink")，而平均年龄只有21-22岁，因而名声大噪、前程无限。

但是1958年2月6日，曼联队在参加欧洲比赛后返回途中遭遇“[慕尼黑空难](../Page/慕尼黑空难.md "wikilink")”，7名巴斯比宝贝当场遇难。当时保持[英格兰队最年轻球员记录](../Page/英格兰足球代表队.md "wikilink")（此纪录直到1998年才被[迈克尔·欧文打破](../Page/迈克尔·欧文.md "wikilink")）的[邓肯·爱德华兹重伤](../Page/邓肯·爱德华兹.md "wikilink")，在医院救治15天后无效死亡。主帅巴斯比也重伤，在医院救治数月后才终于康复。

巴斯比宝贝中幸存的主力球员有当时年仅20岁的[鲍比·查尔顿](../Page/鲍比·查尔顿.md "wikilink")，他虽然在事故中受到头部严重创伤，但是仍然成功康复重回球场，不但在[1966年協助](../Page/1966年世界盃足球賽.md "wikilink")[英格兰以東道主身份首次奪得](../Page/英格兰足球代表队.md "wikilink")[世界盃冠軍](../Page/世界盃足球賽.md "wikilink")，并且成为[曼联历史上迄今为止进球数第](../Page/曼徹斯特聯足球俱樂部.md "wikilink")2的传奇球星。另一位稍年长的幸存者[比尔·福尔克斯也继续在曼联效力了](../Page/比尔·福尔克斯.md "wikilink")12年，是后防线核心。

曼联在慕尼黑空难后培养或收购的球员，如[乔治·贝斯特](../Page/乔治·贝斯特.md "wikilink")、[丹尼斯·劳等](../Page/丹尼斯·劳.md "wikilink")，虽然也是巴斯比主教练手下的重要队员，但是不属于“巴斯比宝贝”。这个名称也成为后人对于在慕尼黑遇难的那些青年球员的爱称。

## 巴斯比宝贝在慕尼黑空难的死难者名单

  - [乔夫·本特](../Page/乔夫·本特.md "wikilink")（Geoff Bent）
  - [罗杰·伯恩](../Page/罗杰·伯恩.md "wikilink")（Roger Byrne）
  - [埃迪·科尔曼](../Page/埃迪·科尔曼.md "wikilink")（Eddie Colman）
  - [邓肯·爱德华兹](../Page/邓肯·爱德华兹.md "wikilink")（Duncan Edwards）
  - [马克·琼斯](../Page/马克·琼斯.md "wikilink")（Mark Jones）
  - [大卫·佩格](../Page/大卫·佩格.md "wikilink")（David Pegg）
  - [汤米·泰勒](../Page/汤米·泰勒.md "wikilink")（Tommy Taylor）
  - [比利·维兰](../Page/比利·维兰.md "wikilink")（Liam 'Billy' Whelan）

## 參見

  - [曼聯](../Page/曼徹斯特聯足球俱樂部.md "wikilink")
  - [慕尼黑空難](../Page/慕尼黑空難.md "wikilink")
  - [費爵爺弟子](../Page/費爵爺弟子.md "wikilink")

[B](../Category/曼聯足球俱樂部.md "wikilink")
[Category:英格蘭足球名人堂](../Category/英格蘭足球名人堂.md "wikilink")