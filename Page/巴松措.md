**巴松措**（），也称**错高湖**，位于[中国](../Page/中国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[工布江达县东部](../Page/工布江达县.md "wikilink")，是一座[冰川](../Page/冰川.md "wikilink")[堰塞湖](../Page/堰塞湖.md "wikilink")，湖面海拔3,484米，東西長13.8公里，南北平均寬1.85公里，面积25.5平方公里，最大水深60米。\[1\]巴松措四周高山環繞，湖體坐落在[由扎拉弄巴冰川和](../Page/由扎拉弄巴冰川.md "wikilink")[鍾錯弄巴冰川相匯而塑造成的](../Page/鍾錯弄巴冰川.md "wikilink")“U”型槽谷中，由[終磧壟堵塞而形成](../Page/終磧壟.md "wikilink")。巴松湖景色秀丽，是西藏知名景点，目前属于[国家5A级旅游景区](../Page/国家5A级旅游景区.md "wikilink")。

湖西南岸有一岛[扎西岛](../Page/扎西岛.md "wikilink")，島上有[寧瑪派寺廟](../Page/寧瑪派.md "wikilink")[措宗寺](../Page/措宗寺.md "wikilink")。寺廟門前存有生殖崇拜的木刻。据当地人称巴松措最大水深160米。因为是[圣湖](../Page/圣湖.md "wikilink")，所以水面上没有一片树叶。

## 参考文献

[Category:堰塞湖](../Category/堰塞湖.md "wikilink")
[Category:林芝湖泊](../Category/林芝湖泊.md "wikilink")
[Category:工布江达县](../Category/工布江达县.md "wikilink")

1.