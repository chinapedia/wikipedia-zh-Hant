**巴林**是波斯湾一个岛屿国家。巴林历史可以追溯到远古时期。

## 遠古時代

  - [迪爾穆恩文化](../Page/迪爾穆恩.md "wikilink")
  - 阿瓦勒文化

## 中古時代

4世纪，巴林成为[波斯](../Page/波斯.md "wikilink")[萨珊王朝的一部分](../Page/萨珊王朝.md "wikilink")，首次见于史籍。

7世纪，皈依[伊斯兰教](../Page/伊斯兰教.md "wikilink")，并入[阿拉伯帝国](../Page/阿拉伯帝国.md "wikilink")。

894年，[什叶派一支](../Page/什叶派.md "wikilink")[伊斯玛仪派占据巴林](../Page/伊斯玛仪派.md "wikilink")，900年，建立[卡尔马特国家](../Page/卡尔马特.md "wikilink")。

899年～1077年，**卡尔马特派王朝**，一个宗教军事集团，该派的首领称伊玛目，为宗教、行政和军事最高领袖，由6名执政官及6名助理组成政务委员会领导。

11世纪中叶，[逊尼派推翻卡尔马特](../Page/逊尼派.md "wikilink")，占领巴林。1235年，巴林被波斯占领，1253年，[乌斯福尔家族起义](../Page/乌斯福尔.md "wikilink")，宣布巴林独立。

巴努歐蓋伊勒部落建立了**烏斯庫里德王朝（[:en:Usfurids](../Page/:en:Usfurids.md "wikilink")）**。

不知名的部落建立了**乌尤尼德王朝([:en:Uyunids](../Page/:en:Uyunids.md "wikilink"))**。

14世紀，屬於**巴尼馬利克部落**或**巴努歐蓋伊勒部落**的[賈爾萬人](../Page/賈爾萬人.md "wikilink")[賈爾萬一世在](../Page/賈爾萬一世.md "wikilink")[蓋提夫建立的](../Page/蓋提夫.md "wikilink")**[贾尔万王朝](../Page/贾尔万王朝.md "wikilink")**，一個統治巴林的什葉派王朝。[賈爾萬王朝是](../Page/賈爾萬王朝.md "wikilink")[忽里模子的](../Page/忽里模子.md "wikilink")[附庸國](../Page/附庸國.md "wikilink")。賈爾萬人在14世紀逐退了盤據在巴士拉的蒙塔菲克（Muntafiq）部落領袖賽義德·伊本·蒙加米斯，因此得勢。

15世紀至16世紀，[贾布里德王朝](../Page/贾布里德王朝.md "wikilink")。

[穆克林·伊本·扎米爾](../Page/穆克林·伊本·扎米爾.md "wikilink")，[巴林](../Page/巴林.md "wikilink")[賈布里德王朝最後一位統治者](../Page/賈布里德王朝.md "wikilink")。

## 葡占时期

16世纪初期，随着达迦马的航海，葡萄牙开始在印度洋进行扩张。1485年，葡萄牙人第一次进入海湾地区。

1514年，[葡萄牙人来到巴林](../Page/葡萄牙.md "wikilink")，并占领之，兴建[麦纳麦](../Page/麦纳麦.md "wikilink")。統治巴林近百年。

## 波斯統治時期

1602年，波斯击败葡萄牙占领军，重新占领巴林。

18世纪，阿富汗人侵入伊朗导致萨菲王朝几近崩溃。在由此引发的权力真空时期，阿曼于1717年入侵巴林，结束了波斯长达100多年的统治。

1783年，[谢赫·艾哈迈德击败波斯](../Page/谢赫·艾哈迈德.md "wikilink")，1797年在巴林建立[哈利法王朝统治](../Page/哈利法王朝.md "wikilink")。

## 英國統治時期

1820年，阿勒哈利法部落重新执政的巴林并且与[英国签订条约](../Page/英国.md "wikilink")。这是一系列条约的开始，之后巴林和英国签订和平友好的永久休战协定。这个协定在1892年和1951年进行进一步修改。此后巴林逐渐成为英国势力范围。1871年，巴林成为英国[保护国](../Page/保护国.md "wikilink")。

## 发现石油

1932年，巴林石油公司发现巴林蕴藏大量[石油](../Page/石油.md "wikilink")。巴林迅速进入现代化。之后[伊朗和英国开始争夺巴林](../Page/伊朗.md "wikilink")。1957年，英国声明巴林是其保护的独立酋长国，而伊朗则宣布巴林是其第十四个省。1968年，英国宣布从巴林撤军，并试图将巴林同[卡塔尔和](../Page/卡塔尔.md "wikilink")[特鲁西尔阿曼组成联邦](../Page/阿联酋.md "wikilink")，未果。此后，[联合国派出调查团](../Page/联合国.md "wikilink")，并于1970年发布了调查结果，认为多数巴林人倾向于独立。当年5月，伊朗接受此结果，承认巴林独立。

  - 民族联合委员会
  - 巴林三月起義

## 独立的巴林

1971年8月14日，巴林正式独立，废除与英国签订的一切不平等条约，定国名为“巴林国”，并将君主称号改为[埃米尔](../Page/埃米尔.md "wikilink")。

  - 巴林國家安全法
  - 1990年代巴林起義

2006年11月25日，巴林举行议会和市政选举。\[1\]

  - [2011年巴林反政府示威](../Page/2011年巴林反政府示威活動.md "wikilink")

## 参考资料

[\*](../Category/巴林歷史.md "wikilink")
[Category:亞洲各國歷史](../Category/亞洲各國歷史.md "wikilink")

1.  [1](http://news.xinhuanet.com/world/2006-11/26/content_5374932.htm)