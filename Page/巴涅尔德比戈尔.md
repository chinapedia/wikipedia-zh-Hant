**巴涅尔德比戈尔**（）是位于[法国](../Page/法国.md "wikilink")[奥克西塔尼大区](../Page/奥克西塔尼大区.md "wikilink")[上比利牛斯省的一个](../Page/上比利牛斯省.md "wikilink")[市镇](../Page/市镇_\(法国\).md "wikilink")，位于[阿杜尔河畔](../Page/阿杜尔河.md "wikilink")。该市镇也是上比利牛斯省的一个副省会，下辖[巴涅爾-德比戈爾區](../Page/巴涅爾-德比戈爾區.md "wikilink")（Arrondissement
de Bagnères-de-Bigorre）。该市镇总面积125.86平方公里，2009年时的人口为8040人。\[1\]

## 人口

巴涅尔德比戈尔人口变化图示
[Population_-_Municipality_code_65059.svg](https://zh.wikipedia.org/wiki/File:Population_-_Municipality_code_65059.svg "fig:Population_-_Municipality_code_65059.svg")

## 参见

  - [上比利牛斯省市镇列表](../Page/上比利牛斯省市镇列表.md "wikilink")

## 参考文献

[B](../Category/上比利牛斯省市镇.md "wikilink")

1.  [法国INSEE人口数据-2009年](http://www.insee.fr/fr/ppp/bases-de-donnees/recensement/populations-legales/france-departements.asp?annee=2009)