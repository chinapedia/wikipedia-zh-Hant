**巴西國徽**啟用於1889年11月19日，即聯邦共和國成立四天以後。

呈圓形，底為光芒，成20角星形。環以[咖啡和](../Page/咖啡.md "wikilink")[煙草枝條](../Page/煙草.md "wikilink")，兩者皆為該國主要的農作物。中間的圓形繪有[南十字星座](../Page/南十字星座.md "wikilink")，為27顆代表26個州和[聯邦區的星星圍繞](../Page/聯邦區_\(巴西\).md "wikilink")。外加鑲紅黃邊的黃綠兩色五角星。

底部的藍色飾帶寫上了巴西聯邦共和國 （República Federativa do
Brasil）的字樣，以下為共和國成立的日期：1889年11月15日。中間有一柄劍，銀色。護手藍色鑲黃邊。柄腳紅色，飾以一黃色五角星。

## 历代国徽

### 王国时代

### 帝国时代

### 共和国时代

[Category:巴西国家象征](../Category/巴西国家象征.md "wikilink")
[B](../Category/國徽.md "wikilink")