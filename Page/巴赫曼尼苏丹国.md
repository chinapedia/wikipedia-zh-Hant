**巴赫曼尼苏丹國**（1347年-1518年 [泰卢固语](../Page/泰卢固语.md "wikilink")：బహుమనీ
సామ్రాజ్యము）بہمانی
سلطانت)是处于[印度](../Page/印度.md "wikilink")[德干高原的古代](../Page/德干高原.md "wikilink")[苏丹国](../Page/苏丹国.md "wikilink")。

1347年，[阿富汗君主](../Page/阿富汗.md "wikilink")[阿拉丁·哈桑·巴赫曼沙阿摆脱了](../Page/阿拉丁·哈桑·巴赫曼沙阿.md "wikilink")[德里苏丹國的束缚](../Page/德里苏丹國.md "wikilink")，建都[古尔伯加](../Page/古尔伯加.md "wikilink")。1425年移都[比德尔](../Page/比德尔.md "wikilink")。1518年巴赫曼尼苏丹國的统治瓦解，取而代之的是五个独立的苏丹國，所谓的[德干苏丹國](../Page/德干苏丹國.md "wikilink")：[比德尔](../Page/比德尔.md "wikilink")、[比贾布尔](../Page/比贾布尔.md "wikilink")、[艾哈迈德讷格尔](../Page/艾哈迈德讷格尔.md "wikilink")、[畢拉尔以及](../Page/畢拉尔.md "wikilink")[果尔贡德](../Page/果尔贡德.md "wikilink")。五个苏丹國纷争不休，战乱不断。最后[莫卧儿入侵](../Page/莫卧儿帝国.md "wikilink")，各苏丹國逐一沦陷。

## 历代苏丹

  - [阿拉丁·哈桑·巴赫曼沙阿](../Page/阿拉丁·哈桑·巴赫曼沙阿.md "wikilink") 1347年 - 1358年
  - [穆罕默德沙阿一世](../Page/穆罕默德沙阿一世.md "wikilink") 1358年 - 1375年
  - [阿拉丁·穆贾希德沙阿](../Page/阿拉丁·穆贾希德沙阿.md "wikilink") 1375年 - 1378年
  - [达乌德沙阿](../Page/达乌德沙阿.md "wikilink") 1378年
  - [穆罕默德沙阿二世](../Page/穆罕默德沙阿二世.md "wikilink") 1378年 - 1397年
  - [吉亚特赫·乌德-丁](../Page/吉亚特赫·乌德-丁.md "wikilink") 1397年
  - [沙姆斯·乌德-丁](../Page/沙姆斯·乌德-丁.md "wikilink") 1397年
  - [塔杰·乌德-丁·菲鲁兹沙阿](../Page/塔杰·乌德-丁·菲鲁兹沙阿.md "wikilink") 1397年 - 1422年
  - [艾哈迈德沙阿一世](../Page/艾哈迈德沙阿一世.md "wikilink") 1422年 - 1436年
  - [艾哈迈德沙阿二世](../Page/艾哈迈德沙阿二世.md "wikilink")
    [1436](../Page/1436.md "wikilink") - 1458年
  - [阿拉丁·胡马雍·扎利姆沙阿](../Page/阿拉丁·胡马雍·扎利姆沙阿.md "wikilink") 1458年 - 1461年
  - [尼扎姆沙阿](../Page/尼扎姆沙阿.md "wikilink") 1461年 - 1463年
  - [穆罕默德沙阿三世](../Page/穆罕默德沙阿三世.md "wikilink") 1463年 - 1482年
  - [穆罕默德沙阿四世](../Page/穆罕默德沙阿四世.md "wikilink") 1482年 - 1518年
  - [艾哈迈德沙阿三世](../Page/艾哈迈德沙阿三世.md "wikilink") 1518年 - 1521年
  - [阿拉丁沙阿](../Page/阿拉丁沙阿.md "wikilink") 1521年 - 1522年
  - [瓦利-阿拉沙阿](../Page/瓦利-阿拉沙阿.md "wikilink") 1522年 - 1525年
  - [卡利姆-阿拉沙阿](../Page/卡利姆-阿拉沙阿.md "wikilink") 1525年 - 1527年

## 参见

  - [德里蘇丹國](../Page/德里蘇丹國.md "wikilink")
  - [德干蘇丹國](../Page/德干蘇丹國.md "wikilink")

[Category:印度帝国及王国](../Category/印度帝国及王国.md "wikilink")
[Category:卡纳塔克邦](../Category/卡纳塔克邦.md "wikilink")