**巴达赫尚省**（，），又譯**巴達克山**，位於[阿富汗東北角](../Page/阿富汗.md "wikilink")，包括[瓦罕走廊地区](../Page/瓦罕走廊.md "wikilink")，與[塔哈爾省](../Page/塔哈爾省.md "wikilink")、[潘傑希爾省](../Page/潘傑希爾省.md "wikilink")、[努爾斯坦省等省份及](../Page/努爾斯坦省.md "wikilink")[塔吉克](../Page/塔吉克.md "wikilink")、[中國和](../Page/中華人民共和國.md "wikilink")[巴基斯坦控制下的](../Page/巴基斯坦.md "wikilink")[克什米尔相鄰](../Page/克什米尔.md "wikilink")，是[巴达克山地区的一部分](../Page/巴达克山.md "wikilink")。中华民国政府曾經宣称[瓦罕走廊应该属于](../Page/瓦罕走廊.md "wikilink")[中华民国版图](../Page/中华民国版图.md "wikilink")，属[中华民国新疆省的一部分](../Page/中华民国新疆省.md "wikilink")（详见[中华民国行政区划](../Page/中华民国行政区划.md "wikilink")）。

## 地理

[Wakhan,_Badakhshan.jpg](https://zh.wikipedia.org/wiki/File:Wakhan,_Badakhshan.jpg "fig:Wakhan,_Badakhshan.jpg")
[Afghanistan_Badakhshan_numbered_gray.PNG](https://zh.wikipedia.org/wiki/File:Afghanistan_Badakhshan_numbered_gray.PNG "fig:Afghanistan_Badakhshan_numbered_gray.PNG")

巴达赫尚主要在北面和东面与[塔吉克斯坦接壤](../Page/塔吉克斯坦.md "wikilink")，东面的一个长条被称作[瓦罕走廊](../Page/瓦罕走廊.md "wikilink")，这里与[巴基斯坦北部接壤](../Page/巴基斯坦.md "wikilink")，在它的最东边，瓦罕走廊的另外一端与[中国接壤](../Page/中華人民共和國.md "wikilink")。整个省的总面积为44,059[平方公里](../Page/平方公里.md "wikilink")，大部分是[兴都库什山和](../Page/兴都库什.md "wikilink")[帕米尔山脉](../Page/帕米尔.md "wikilink")。该省共分为13个行政区。

## 历史

巴达赫尚是古代[丝绸之路的一个中转站](../Page/丝绸之路.md "wikilink")，巴达赫尚的名字是萨珊王朝起的，名字源于一个词「badaxš」，一个萨珊王朝的官方头衔，这个名字的后缀
-ān，则表示这个区域属于一个拥有badaxš头衔的人\[1\]（类似[德黑兰](../Page/德黑兰.md "wikilink")，[阿塞拜疆和](../Page/阿塞拜疆.md "wikilink")[伊斯法罕](../Page/伊斯法罕.md "wikilink")）。

巴达赫尚和[潘傑希爾省是仅有的两个在](../Page/潘傑希爾省.md "wikilink")[塔利班扩张中未能控制的省份](../Page/塔利班.md "wikilink")。但是在战争期间，Mawlawi
Shariqi建立了一个非塔利班的伊斯兰酋长国，同时临近的[努尔斯坦省发生了伊斯兰革命](../Page/努尔斯坦省.md "wikilink")。巴达赫尚本地人，阿富汗前总统[布爾漢努丁·拉巴尼和](../Page/布爾漢努丁·拉巴尼.md "wikilink")[艾哈迈德·沙阿·马苏德是反塔利班的](../Page/艾哈迈德·沙阿·马苏德.md "wikilink")[北方联盟在塔利班巅峰时期的最后的剩余](../Page/北方聯盟_\(阿富汗\).md "wikilink")。巴达赫尚即将陷落的时候，[美国用其](../Page/美国.md "wikilink")[空军力量和大量的援助让北方联盟恢复了对该省的控制](../Page/空军.md "wikilink")。

该省目前的总督是孟什·阿卜杜拉·马吉德，之前一任是赛义德·阿敏·塔里克。在塔利班失败之后，中国对该地区表现出了巨大的兴趣，帮助重建了道路和基础设施。

## 经济

[Kuchi_caravan_in_Badakhshan,_Afghanistan.jpg](https://zh.wikipedia.org/wiki/File:Kuchi_caravan_in_Badakhshan,_Afghanistan.jpg "fig:Kuchi_caravan_in_Badakhshan,_Afghanistan.jpg")
尽管拥有巨大的矿藏，巴达赫尚仍然是全世界最贫穷的地区之一。种植[罂粟是该省唯一实际的收入](../Page/罂粟.md "wikilink")，而且，由于缺乏基本的医疗设施，和很多难以接近的区域，以及极为严酷的冬季，这里有着全世界最高的[产妇](../Page/产妇.md "wikilink")[死亡率](../Page/死亡率.md "wikilink")。

巴达赫尚的[天青石从几个世纪之前就开始开采](../Page/天青石.md "wikilink")，在古代它曾经是世界上最大，最著名的天青石产地\[2\]\[3\]
。巴达赫尚最近几年的很多采矿活动都是为了开采天青石。这些开采活动被用来支持北方联盟的部队，在北方联盟之前，这些天青石被用来支持反对[苏联的圣战者](../Page/苏联.md "wikilink")。最近的一些地质勘探表明，这一区域有其他宝石矿藏，其中包括[红宝石和绿宝石](../Page/红宝石.md "wikilink")。这些矿藏的开发成了振兴该地区经济的关键。

## 首府

[Feyzabad_international.jpg](https://zh.wikipedia.org/wiki/File:Feyzabad_international.jpg "fig:Feyzabad_international.jpg")

[法扎巴德](../Page/法扎巴德_\(阿富汗\).md "wikilink")（）位于[阿富汗东北部](../Page/阿富汗.md "wikilink")[科克恰河畔](../Page/科克恰河.md "wikilink")，是巴达赫尚省的首府。人口大约有50,000。他是东北阿富汗和帕米尔地区主要的经济和政治中心，在冬季这座城市有时候会因为大雪而与外界隔离。1979年，这里曾经是一个阿富汗游击队抵制苏联入侵的重点地区，但是在1980年法扎巴德落入苏联人手中之后，成为了一座主要的要塞城市。

## 人口和语言

该省人口估计有823,000，他们主要讲[塔吉克语](../Page/塔吉克语.md "wikilink")，同时当地也有一些其他的印度伊朗语系的其他语言。当地有一些[柯尔克孜族](../Page/柯尔克孜族.md "wikilink")。还有一些[游牧半游牧状态的](../Page/游牧.md "wikilink")[乌兹别克族人和](../Page/乌兹别克族.md "wikilink")[普什图族人他们随着季节变化做长距离的迁徙](../Page/普什图族.md "wikilink")。本地居民之中大部分是[逊尼派穆斯林](../Page/逊尼派.md "wikilink")，但是大部分帕米尔人是[伊斯玛仪派](../Page/伊斯玛仪.md "wikilink")。

该地区历史人口估计如下：\[4\]

  - 2006: 823,000\[5\]
  - 2004: 725,700
  - 2000: 923,144（[橡树岭国家实验室](../Page/橡树岭国家实验室.md "wikilink")）
  - 1998: 924,747（橡树岭国家实验室）
  - 1997: 663,700
  - 1991: 615,156
  - 1990: 554,374（[美国国际开发署](../Page/美国国际开发署.md "wikilink")）
  - 1979: 497,798

## 名人

  - [布尔汉努丁·拉巴尼](../Page/布尔汉努丁·拉巴尼.md "wikilink")——阿富汗前总统

## 参考文献

## 参见

  - [巴達克山](../Page/巴達克山.md "wikilink")
  - [戈爾諾-巴達赫尚自治州](../Page/戈爾諾-巴達赫尚自治州.md "wikilink")

{{-}}

[Category:阿富汗行政區劃](../Category/阿富汗行政區劃.md "wikilink")

1.
2.  Deer, William A.; Howie, Robert A, and Zussman, Joseph (1963) "Lapis
    lazuli" *Rock-Forming Minerals* Longman, London,
    [OCLC 61975619](http://www.worldcat.org/oclc/61975619)
3.  Lapis lazuli was also found in the Urals Mountains in Russia. Deer
    *et al.* above
4.  [Afghanistan Geographic & Thematic
    Layers](http://www.fao.org/afghanistan/)
5.