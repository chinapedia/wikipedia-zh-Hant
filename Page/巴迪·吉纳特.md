**哈里·爱德华·“巴迪”·吉纳特**（，），生于[宾西法尼亚州新金斯顿](../Page/宾西法尼亚州.md "wikilink")（New
Kensington），美国职业篮球运动员和教练。

吉纳特被广泛的认定为1938年至1948年间最出色的篮球运动员，曾四次当选[NBL全明星赛](../Page/国家篮球联盟.md "wikilink")，带领[希伯根红人队](../Page/希伯根红人队.md "wikilink")
(Sheboygan Redskins)和[韦恩堡活塞队](../Page/底特律活塞队.md "wikilink") (Fort Wayne
Pistons)夺取了1943，1944和1945年的NBL冠军头衔。吉纳特还帮助[巴尔的摩子弹队夺取了](../Page/巴尔的摩子弹队_\(最初的\).md "wikilink")1947年[ABL的冠军](../Page/美国篮球联赛.md "wikilink")。

尽管吉纳特的篮球职业生涯都是在[NBA创建前的早期联赛中度过的](../Page/NBA.md "wikilink")，他还是以球员兼教练身份效力[BAA联盟](../Page/NBA.md "wikilink")[巴尔的摩子弹队三年](../Page/巴爾的摩子彈_\(最初的\).md "wikilink")。1948年，BAA联盟季后赛中吉纳特成为历史上第一位夺得职业联赛冠军的球员兼教练。在他球员生涯结束后，他执教了最初的子弹队一个多赛季。随后他成为了NCAA大学篮球的[乔治城大学四个赛季](../Page/乔治城大学.md "wikilink")。

吉纳特随后两次执教现代的子弹队，一次是一个赛季还有一次是临时过渡。他后来还执教过[ABA的](../Page/美国篮球协会.md "wikilink")[匹兹堡风笛手队](../Page/匹兹堡风笛手队.md "wikilink")
(Pittsburgh Pipers)。

1994年，吉纳特在[宾西法尼亚州华盛顿市的华盛顿和杰斐逊学院](../Page/宾西法尼亚州.md "wikilink")
(Washington and Jefferson
College)的典礼上荣载[篮球名人堂](../Page/篮球名人堂.md "wikilink")。

1998年逝世于[新罕布什尔州纳舒厄](../Page/新罕布什尔州.md "wikilink")（Nashua）。

## 参考资料

## 外部链接

  - [Basketball Hall of Fame: Buddy
    Jeanette](https://web.archive.org/web/20070312010905/http://www.hoophall.com/halloffamers/Jeannette.htm)

[Category:美国篮球教练](../Category/美国篮球教练.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:篮球名人堂成员](../Category/篮球名人堂成员.md "wikilink")
[Category:韦恩堡活塞队球员](../Category/韦恩堡活塞队球员.md "wikilink")
[Category:巴尔的摩子弹队 (1944-1954)
球员](../Category/巴尔的摩子弹队_\(1944-1954\)_球员.md "wikilink")
[Category:巴尔的摩子弹队 (1944-1954)
教练](../Category/巴尔的摩子弹队_\(1944-1954\)_教练.md "wikilink")
[Category:匹兹堡风笛手队教练](../Category/匹兹堡风笛手队教练.md "wikilink")
[Category:巴尔的摩子弹队教练](../Category/巴尔的摩子弹队教练.md "wikilink")
[Category:希博伊根红人队球员](../Category/希博伊根红人队球员.md "wikilink")