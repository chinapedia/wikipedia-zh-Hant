**巴里縣**（**Barry County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州西南部的一個縣](../Page/密蘇里州.md "wikilink")，南鄰[阿肯色州](../Page/阿肯色州.md "wikilink")。面積2,048平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口34,010人。縣治[卡斯維爾](../Page/卡斯維爾_\(密蘇里州\).md "wikilink")
(Cassville)。

成立於1835年1月5日。縣名紀念在[安德魯·傑克遜和](../Page/安德魯·傑克遜.md "wikilink")[馬丁·范布倫時期任](../Page/馬丁·范布倫.md "wikilink")[郵政部長的](../Page/美國郵政部長.md "wikilink")[威廉·泰勒·巴里](../Page/威廉·泰勒·巴里.md "wikilink")
(William Taylor Barry)。

[B](../Category/密蘇里州行政區劃.md "wikilink")