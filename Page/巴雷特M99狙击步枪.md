**巴雷特M99**（，又名****）是[巴雷特在](../Page/巴雷特.md "wikilink")1999年推出的[狙击步枪](../Page/狙击步枪.md "wikilink")，發射[12.7×99毫米](../Page/12.7×99mm_NATO.md "wikilink")[北約](../Page/北大西洋公约组织.md "wikilink")[口徑](../Page/口徑.md "wikilink")（.50
BMG）及[子彈](../Page/子彈.md "wikilink")。

## 設計及歷史

巴雷特在推出大口徑的[M82及](../Page/巴雷特M82狙擊步槍.md "wikilink")[巴雷特M95後](../Page/巴雷特M95狙擊步槍.md "wikilink")，為了再提高精確度及降低長度，以M95為基礎設計出一種[犢牛式結構](../Page/犢牛式.md "wikilink")、[旋轉後拉式槍機](../Page/手動槍機.md "wikilink")、內置彈倉只可放一發子彈的[狙擊步槍](../Page/狙擊步槍.md "wikilink")。M99的槍口仍然裝有高效能的雙室[槍口制退器](../Page/砲口制動器.md "wikilink")，但槍管沒有刻上線坑，亦沒有預設瞄準鏡，只有在[機匣頂部設有的](../Page/機匣.md "wikilink")[戰術導軌上安裝瞄準鏡](../Page/戰術導軌.md "wikilink")，[兩腳架裝在機匣前端的底部](../Page/兩腳架.md "wikilink")。巴雷特M99有[12.7×99毫米NATO](../Page/12.7×99mm_NATO.md "wikilink")（.50
BMG）及兩種口徑，亦提供四種不同槍管選擇，包括[12.7×99毫米NATO](../Page/12.7×99mm_NATO.md "wikilink")（.50
BMG）的25、29、32寸槍管版本，及的32寸槍管版本\[1\]\[2\]。

巴雷特為了令M99達到極高精度，因此結構比較簡單，[彈倉只可放一發子彈而且不設](../Page/彈倉.md "wikilink")[彈匣](../Page/彈匣.md "wikilink")，在軍事用途缺乏競爭力，所以現時主要是向民用市場及[執法部門發售](../Page/警察.md "wikilink")，在美國一些禁止民間擁有.50
BMG的州份（例如[加州](../Page/加利福尼亚州.md "wikilink")）只會發售.416 Barrett口徑版本。

## 使用國

  -   - [阿爾巴尼亞武裝部隊](../Page/阿爾巴尼亞武裝部隊.md "wikilink")\[3\]

  -   - [孟加拉陸軍](../Page/孟加拉陸軍.md "wikilink")\[4\]

  -   - [荷蘭皇家海軍陸戰隊](../Page/荷蘭皇家海軍陸戰隊.md "wikilink")\[5\]

  -   - 地方警察部門

## 流行文化

### [电影](../Page/电影.md "wikilink")

  - 2015年—《[战狼](../Page/战狼.md "wikilink")》：被雇佣兵组织成员「刺客」在开篇抢夺敏登（[倪大红饰](../Page/倪大红.md "wikilink")）时所使用，但在武器特辑中被错误的介绍为[巴雷特M82A1](../Page/巴雷特M82狙击步枪.md "wikilink")[狙击步枪](../Page/狙击步枪.md "wikilink")。

### [电子游戏](../Page/电子游戏.md "wikilink")

  - 2006年—《[-{zh-hans:幽灵行动：尖峰战士;zh-hk:幽靈行動：尖峰戰士;zh-tw:火線獵殺：先進戰士;}-](../Page/火線獵殺：先進戰士.md "wikilink")》：命名为“SR
    M99”
  - 2007年—《[-{zh-hans:幽灵行动：尖峰战士 2;zh-hk:幽靈行動：尖峰戰士 2;zh-tw:火線獵殺：先進戰士
    2;}-](../Page/火線獵殺：先進戰士2.md "wikilink")》：命名为“M99 Sniper”
  - 2015年—《[殺戮空間2](../Page/殺戮空間2.md "wikilink")》

## 相關條目

  - [巴雷特M82狙擊步槍](../Page/巴雷特M82狙擊步槍.md "wikilink")
  - [巴雷特M90狙擊步槍](../Page/巴雷特M90狙擊步槍.md "wikilink")
  - [巴雷特M95狙擊步槍](../Page/巴雷特M95狙擊步槍.md "wikilink")
  - [巴雷特XM109狙擊榴彈發射器](../Page/巴雷特XM109狙擊榴彈發射器.md "wikilink")
  - [巴雷特XM500狙擊步槍](../Page/巴雷特XM500狙擊步槍.md "wikilink")

## 資料來源

<div class="references-small">

<references />

</div>

## 外部链接

  - —[Official Barrett M99
    page](http://www.barrettrifles.com/rifle_99.aspx)

      - [Operator's
        manual](http://www.barrettrifles.com/pdf/Manual-99.pdf)

  - —[Video demo of the
    M99](https://web.archive.org/web/20070824181938/http://www.gunslot.com/guns/barrett-m99-big-shot)

  - —[M99影片](https://web.archive.org/web/20070824181938/http://www.gunslot.com/guns/barrett-m99-big-shot)

  - —[Modern Firearms—Barrett Model M99 and
    M99-1](http://world.guns.ru/sniper/large-caliber-sniper-rifles/usa/barrett-m99-e.html)

  - —[D Boy Gun World（巴雷特
    M99）](http://firearmsworld.net/usa/barrett/m99/m99.htm)

[Category:栓動式步槍](../Category/栓動式步槍.md "wikilink")
[Category:狙击步枪](../Category/狙击步枪.md "wikilink")
[Category:反器材步槍](../Category/反器材步槍.md "wikilink")
[Category:美國狙擊步槍](../Category/美國狙擊步槍.md "wikilink")
[Category:巴雷特槍械](../Category/巴雷特槍械.md "wikilink")
[Category:12.7×99毫米槍械](../Category/12.7×99毫米槍械.md "wikilink")

1.  [巴雷特官方頁面-M99資料](http://www.barrettrifles.com/rifle_99.aspx)
2.
3.  <http://illyria.proboards.com/thread/23520/albanian-military-photos#page=3>
4.  <http://www.bdmilitary.com/index.php?option=com_content&view=article&id=216&Itemid=95>
5.