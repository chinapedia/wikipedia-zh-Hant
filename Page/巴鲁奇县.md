[Map_GujDist_CentralEast.png](https://zh.wikipedia.org/wiki/File:Map_GujDist_CentralEast.png "fig:Map_GujDist_CentralEast.png")中东部地区\]\]

**巴鲁奇县**（Bharuch
District）为[印度](../Page/印度.md "wikilink")[古吉拉特邦南部](../Page/古吉拉特邦.md "wikilink")[辖县](../Page/县.md "wikilink")，划分为[安克尔西瓦](../Page/安克尔西瓦.md "wikilink")（[Ankleshwar](../Page/:en:Ankleshwar.md "wikilink")）、[汉索特](../Page/汉索特.md "wikilink")（[Hansot](../Page/:en:Hansot.md "wikilink")）、[贾姆布沙尔](../Page/贾姆布沙尔.md "wikilink")（[Jambusar](../Page/:en:Jambusar.md "wikilink")）、[吉哈加迪亚](../Page/吉哈加迪亚.md "wikilink")（[Jhagadia](../Page/:en:Jhagadia.md "wikilink")）、[潘诺里](../Page/潘诺里.md "wikilink")（[Panoli](../Page/:en:Panoli.md "wikilink")）和[瓦格拉](../Page/瓦格拉.md "wikilink")（[Vagra](../Page/:en:Vagra.md "wikilink")）等六大[次区](../Page/次区_\(南亚\).md "wikilink")（[Taluka](../Page/:en:Tehsil.md "wikilink")）。全县面积9,308平方公里，人口1,370,656人（2001年，含[巴鲁奇市人口](../Page/巴鲁奇.md "wikilink")），其中城镇人口25.72％。

巴鲁奇县拥有众多[穆斯林人口](../Page/穆斯林.md "wikilink")，主要分布于[巴鲁奇市周边的](../Page/巴鲁奇.md "wikilink")[村庄和](../Page/村庄.md "wikilink")[集镇](../Page/集镇.md "wikilink")。以前的村庄和巴鲁奇主城区人口并不多，巴鲁奇人一直寻求着绿色牧场环绕的城市，穆斯林村庄才渐渐增多。巴鲁奇人或被称作窝荷罗人（Vohro），很多人足迹遍及从[中东](../Page/中东.md "wikilink")、[欧洲到](../Page/欧洲.md "wikilink")[美洲乃至世界各地](../Page/美洲.md "wikilink")。[英国是巴鲁奇人的首选地](../Page/英国.md "wikilink")，也是诸多巴鲁奇人的第二故乡；英国的巴鲁奇人主要分布于[曼彻斯特的](../Page/曼彻斯特.md "wikilink")[布莱克本](../Page/布莱克本.md "wikilink")、[博尔顿](../Page/博尔顿.md "wikilink")、[杜斯布里](../Page/杜斯布里.md "wikilink")（[Dewsbury](../Page/:en:Dewsbury.md "wikilink")）以及[伦敦东部](../Page/伦敦.md "wikilink")。

[明朝史籍称作](../Page/明朝.md "wikilink")**八可意**，“剌泥而外，有数国，……曰八可意，…永乐中，尝遣使朝贡”。在今印度古吉拉特邦东南部沿海一带巴鲁奇县附近。\[1\]\[2\]\[3\]\[4\]\[5\]

## 参考文献

[Category:古吉拉特邦行政区划](../Category/古吉拉特邦行政区划.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")》卷326剌泥
2.  《象胥录》卷5
3.  《[续通典](../Page/续通典.md "wikilink")》卷149
4.  《[明续文献通考](../Page/续文献通考.md "wikilink")》卷237
5.  《皇舆考》卷12