**巴黎大學**（），是世界上历史最悠久的大学之一，其授课历史可以追溯到12世纪中叶。大学分别在1200年和1215年获得法王[腓力二世和教宗](../Page/腓力二世.md "wikilink")[英诺森三世的官方认可](../Page/英诺森三世.md "wikilink")\[1\]。1257年，大学的第一个学院机构[索邦學院成立](../Page/索邦學院.md "wikilink")，索邦(Sorbonne)成为大学的代称。1793年[法国大革命時巴黎大学遭到解散](../Page/法国大革命.md "wikilink")，1896年重建，1968年再被拆分成13所獨立大學；2019年再由巴黎第五第七大學合併為巴黎大學，繼承其名稱。

截止2018年10月，巴黎大学的校友、教职工及研究人员中，共有[50位诺贝奖得主](../Page/各大學諾貝爾獎得主列表.md "wikilink")、[16位菲尔兹奖得主](../Page/各大学菲尔兹奖得主列表.md "wikilink")。

## 歷史

最初是於12世紀時，在巴黎塞納河上的[西堤島](../Page/西堤島.md "wikilink")，針對當時來自[法蘭西](../Page/法蘭西.md "wikilink")、[皮卡第](../Page/皮卡第.md "wikilink")、[诺曼底與](../Page/诺曼底.md "wikilink")[英格蘭四個國家之年輕人教授](../Page/英格蘭.md "wikilink")[神學](../Page/神學.md "wikilink")、[法律](../Page/法律.md "wikilink")、[醫學及](../Page/醫學.md "wikilink")[藝術四種領域](../Page/藝術.md "wikilink")。而后，於1257年，[法國國王](../Page/法國國王.md "wikilink")[路易九世身旁之](../Page/路易九世.md "wikilink")[神父](../Page/神父.md "wikilink")[羅貝爾·德·索邦在](../Page/羅貝爾·德·索邦.md "wikilink")[圣女日南斐法山上成立](../Page/圣女日南斐法山.md "wikilink")[索邦學院](../Page/索邦學院.md "wikilink")，后逐渐发展为巴黎大学。

1968年法国学生运动发动之后，巴黎大学被拆分成13座独立的大学（因为教育改革大学合并的原因，12所在2018年1月1日起，2019年1月1日预计为11所），至今沿用“索邦”称谓的大学为
索邦大学（Sorbonne
Université，文学院直接继承者巴黎四大大学(paris-sorbonne)和理学院直接继承者巴黎六大（pierre
et marie curie）合并而来），巴黎第一大学（先贤祠-索邦大学）、巴黎第三大学（新索邦大学）。

### 拆分

1968年法国[学生运动发动之后](../Page/五月风暴.md "wikilink")，大学被拆分成13座独立的大學，巴黎大学从此不复存在。

  - [巴黎第一大學](../Page/巴黎第一大學.md "wikilink")（Université Paris I - Panthéon
    Sorbonne）
  - [巴黎第二大學](../Page/巴黎第二大學.md "wikilink")（Université Paris II -
    Panthéon Assas）
  - [巴黎第三大學](../Page/巴黎第三大學.md "wikilink")（Université Paris III -
    Sorbonne Nouvelle）
  - [巴黎第四大學](../Page/巴黎第四大學.md "wikilink")（Université Paris IV - Paris
    Sorbonne）
  - [巴黎第五大學](../Page/巴黎第五大學.md "wikilink")（Université Paris V -
    Descartes）
  - [巴黎第六大學](../Page/巴黎第六大學.md "wikilink")（Université Paris VI - Pierre
    et Marie Curie）
  - [巴黎第七大學](../Page/巴黎第七大學.md "wikilink")（Université Paris VII -
    Denis-Diderot）
  - [巴黎第八大學](../Page/巴黎第八大學.md "wikilink")（Université Paris VIII -
    Vincennes Saint Denis）
  - [巴黎第九大學](../Page/巴黎第九大學.md "wikilink")（Université Paris IX -
    Paris-Dauphine）
  - [巴黎第十大學](../Page/巴黎第十大學.md "wikilink")（Université Paris X - Paris
    Ouest Nanterre La Défense）
  - [巴黎第十一大學](../Page/巴黎第十一大學.md "wikilink")（Université Paris XI - Paris
    Sud）
  - [巴黎第十二大學](../Page/巴黎第十二大學.md "wikilink")（Université Paris XII -
    Paris Est Créteil Val de Marne）
  - [巴黎第十三大學](../Page/巴黎第十三大學.md "wikilink")（Université Paris XIII -
    Paris Nord）

### 合并

21世纪法国教育改革以来，13所巴黎大学的分支学校中最著名的几所学校（Prestigious Universities in
Paris）开始通过合并建立跨学科世界级大学，
以争取卓越大学（IDEX，类似中国[985工程](../Page/985工程.md "wikilink")、双1流、德国的卓越大学计划、與臺灣[邁向頂尖大學計畫](../Page/邁向頂尖大學計畫.md "wikilink")）称号和拨款。

2018年1月1日，[巴黎第四大學](../Page/巴黎第四大學.md "wikilink")（Université Paris IV -
Paris
Sorbonne）－舊巴黎大學文學院的直接繼承者，與[巴黎第六大學](../Page/巴黎第六大學.md "wikilink")（Université
Paris VI - Pierre et Marie
Curie）－舊巴黎大學理學院的直接繼承者，合併為「[索邦大學](../Page/索邦大學.md "wikilink")」
(l'Université Sorbonne Université)。

2019年，**[巴黎第五大學](../Page/巴黎第五大學.md "wikilink")**（Université Paris V -
Descartes）与 **[巴黎第七大學](../Page/巴黎第七大學.md "wikilink")**（Université Paris
VII -
Denis-Diderot）合并为**[巴黎大学](../Page/巴黎大学_\(2019年成立\).md "wikilink")**。

[巴黎第九大學](../Page/巴黎第九大學.md "wikilink")（Université Paris IX -
Paris-Dauphine）已合并进入[巴黎科學人文藝術大学](../Page/巴黎文理研究大学.md "wikilink")（Université
de recherche
Paris-Sciences-et-Lettres）,预计同[巴黎高等師範學校](../Page/巴黎高等師範學校.md "wikilink")(ENS
Paris)等[師範學校一樣](../Page/師範學校_\(法式\).md "wikilink")，從2018年起将只颁发PSL的文凭。

[巴黎第十一大學](../Page/巴黎第十一大學.md "wikilink")（Université Paris XI - Paris
Sud）加入[大学与院校共同体](../Page/大学与院校共同体.md "wikilink")[巴黎-萨克雷大学](../Page/巴黎-萨克雷大学.md "wikilink")（Université
Paris-Saclay)。

## 制度

巴黎大學在過去為**教授的大學**(masters' university)，其有別於學生的大學(students'
university)，由教師掌管校務，教師組成的學院(Faculties)具其影響力，學院教授選出的大學校長（亦稱Rector）可制訂規例管理校內所有學生團體\[2\]。

### 特權

12世紀末至13世紀初，巴黎大學已獲得免稅及免服兵役等特權。在1231年，時[羅馬教皇格雷戈里九世](../Page/羅馬教皇.md "wikilink")（Gregory
IX）政頒布了一連串法令予巴黎大學特權，包括\[3\]：

(1)司法自治權：內部可設有特別法庭，校長和教授享有對本校成員訴訟案件的裁決權；

(2)制度校服、上課時間及地點、宗教儀式、居住問題等權力；

(3)審定教師資格權和學位授予權；

(4)罷教、罷課、遷校的自由等。

## 著名校友

  - [瑪麗亞·居里](../Page/瑪麗亞·居里.md "wikilink")，物理學者，1903年[諾貝爾物理獎得主](../Page/諾貝爾物理獎.md "wikilink")，1911年[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")。
  - [皮埃爾·居里](../Page/皮埃爾·居里.md "wikilink")，物理學者，1903年[諾貝爾物理獎得主](../Page/諾貝爾物理獎.md "wikilink")。
  - [加布里埃爾·李普曼](../Page/加布里埃爾·李普曼.md "wikilink")，物理學者，1908年[諾貝爾物理獎得主](../Page/諾貝爾物理獎.md "wikilink")。
  - [讓·佩蘭](../Page/讓·佩蘭.md "wikilink")，物理學者，1926年[諾貝爾物理獎得主](../Page/諾貝爾物理獎.md "wikilink")。
  - [路易-維克多-皮埃爾-雷蒙·第七代德布羅意公爵](../Page/路易·德布羅意.md "wikilink")，物理學者，1929年[諾貝爾物理獎得主](../Page/諾貝爾物理獎.md "wikilink")。
  - [弗雷德里克·约里奥-居里](../Page/弗雷德里克·约里奥-居里.md "wikilink")，物理學者，1935年[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")。
  - [吉尔·德勒兹](../Page/吉尔·德勒兹.md "wikilink")，[法国](../Page/法国.md "wikilink")[后现代主义哲学家](../Page/后现代主义.md "wikilink")。
  - [伊伦·约里奥-居里](../Page/伊伦·约里奥-居里.md "wikilink")，物理學者，1935年[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")。
  - [儒爾斯·亨利·龐加萊](../Page/龐加萊.md "wikilink")，物理、數學家。
  - [西蒙·波娃](../Page/西蒙·波娃.md "wikilink")，存在主義作家。
  - [蔣勳](../Page/蔣勳.md "wikilink")，中華民國文學家、藝術家、教授、教師、美學家。
  - [鍾期榮](../Page/鍾期榮.md "wikilink")，中國首位女法官，香港樹仁大學校長。
  - [胡鴻烈](../Page/胡鴻烈.md "wikilink")，大律師，香港樹仁大學校監。
  - [刘佛年](../Page/刘佛年.md "wikilink")，教育家，[华东师范大学校长](../Page/华东师范大学.md "wikilink")。
  - [盧毓駿](../Page/盧毓駿.md "wikilink")，建築師，1958年代表[中華民國出席聯合國招開之](../Page/中華民國.md "wikilink")「亞洲及遠東都市化及工業化區域計畫研討會」。創辦[中國文化大學建築及都市設計學系](../Page/中國文化大學.md "wikilink")。

## 参考文献

[巴黎高等教育](../Category/巴黎高等教育.md "wikilink")
[Category:中世纪大学](../Category/中世纪大学.md "wikilink")
[Category:12世紀創建的教育機構](../Category/12世紀創建的教育機構.md "wikilink")
[Category:法国教育史](../Category/法国教育史.md "wikilink")
[Category:1968年廢除](../Category/1968年廢除.md "wikilink")

1.  Haskins, C. H.: *The Rise of Universities*, page 292. Henry Holt and
    Company, 1923.
2.  [中世紀大學 Medieval
    University](http://terms.naer.edu.tw/detail/1302315/) 方永泉 2000年12月
    國家教育研究院 台灣
3.  [論格雷戈里九世授予巴黎大學的特權與限制](http://www.cultus.hk/his5532/tutorials/paris/CHAN%20SIN%20LING.htm)
    陳倩鈴