**1814年巴黎和約**（）是在1814年5月30日[法國與](../Page/法兰西第一帝国.md "wikilink")[第六次反法同盟所簽訂的的和約](../Page/第六次反法同盟.md "wikilink")。

## 背景

[法國](../Page/法兰西第一帝国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")[拿破崙在對](../Page/拿破仑·波拿巴.md "wikilink")[第六次反法同盟戰敗後](../Page/第六次反法同盟.md "wikilink")，[俄羅斯和](../Page/俄罗斯帝国.md "wikilink")[普魯士軍隊在同年](../Page/普魯士.md "wikilink")3月30日攻陷巴黎，迫使拿破崙承認戰敗並於3月31日退位，於是在[英國支持下](../Page/英国.md "wikilink")，由[沙皇](../Page/沙皇.md "wikilink")[亞歷山大一世主導下](../Page/亚历山大一世_\(俄国\).md "wikilink")[波旁王朝重新成為法國的合法政府及復辟為正統王朝](../Page/波旁王朝.md "wikilink")，由[路易十六之兄弟](../Page/路易十六.md "wikilink")[路易十八登上皇位](../Page/路易十八.md "wikilink")，採[君主立憲制](../Page/君主立宪制.md "wikilink")，列強與法國皇室於1814年5月30日簽訂和約，恢復正常關係；條約對法國作出寬大優待，藉以撫慰法國人民及加強復辟後波旁王室的聲望。

## 內容

1.  法國保持1792年1月1日以前的疆域，較革命爆發前為多
2.  列強承認波旁王朝為正統，法國承認[荷蘭](../Page/荷兰.md "wikilink")、[德意志各邦國](../Page/德意志.md "wikilink")、[義大利各邦國及](../Page/意大利.md "wikilink")[瑞士獨立](../Page/瑞士.md "wikilink")
3.  英國須交還某些法國的[殖民地](../Page/殖民地.md "wikilink")，英國繼續擁有[毛里裘斯及](../Page/毛里裘斯.md "wikilink")[馬爾他等海外據點](../Page/马耳他.md "wikilink")
4.  法國不需向列強支付任何賠償
5.  拿破崙自願流放至[厄爾巴島](../Page/厄尔巴岛.md "wikilink")，可保留[法蘭西帝國](../Page/法蘭西帝國.md "wikilink")[皇帝稱號](../Page/皇帝.md "wikilink")，且每年收取法國政府二百萬[法郎](../Page/法郎.md "wikilink")

## 參見

  - [法國大革命](../Page/法国大革命.md "wikilink")
  - [拿破崙戰爭](../Page/拿破崙戰爭.md "wikilink")
  - [拿破崙](../Page/拿破仑·波拿巴.md "wikilink")
  - [第六次反法同盟](../Page/第六次反法同盟.md "wikilink")
  - [歐洲協調](../Page/歐洲協調.md "wikilink")
  - [儲蒙條約](../Page/休蒙條約.md "wikilink")
  - [1815年巴黎和約](../Page/1815年巴黎和約.md "wikilink")
  - [巴黎條約](../Page/巴黎条约.md "wikilink")

[Category:19世纪欧洲条约](../Category/19世纪欧洲条约.md "wikilink")
[Category:法蘭西第一帝國](../Category/法蘭西第一帝國.md "wikilink")
[Category:法國條約](../Category/法國條約.md "wikilink")
[Category:俄罗斯帝国条约](../Category/俄罗斯帝国条约.md "wikilink")
[Category:英國條約](../Category/英國條約.md "wikilink")
[Category:普魯士條約](../Category/普魯士條約.md "wikilink")
[Category:奥地利帝国条约](../Category/奥地利帝国条约.md "wikilink")
[Category:歐洲協調](../Category/歐洲協調.md "wikilink")
[Category:巴黎政治史](../Category/巴黎政治史.md "wikilink")
[Category:1814年欧洲](../Category/1814年欧洲.md "wikilink")