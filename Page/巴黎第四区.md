**巴黎第四区**（）是[法国首都](../Page/法国.md "wikilink")[巴黎市的](../Page/巴黎.md "wikilink")20个[区之一](../Page/巴黎区份.md "wikilink")。

该区位于[塞纳河的右岸](../Page/塞纳河.md "wikilink")，西部与[第一区接邻](../Page/巴黎第一区.md "wikilink")；北部毗邻[第三区](../Page/巴黎第三区.md "wikilink")，东面是[十一区和](../Page/巴黎十一区.md "wikilink")[十二区](../Page/巴黎十二区.md "wikilink")，塞纳河以南则是[第五区](../Page/巴黎第五区.md "wikilink")。

第四区拥有[文艺复兴时期的](../Page/文艺复兴.md "wikilink")[巴黎市政厅](../Page/巴黎市政厅.md "wikilink")，以及文艺复兴式的[孚日广场](../Page/孚日广场.md "wikilink")（Place
des
Vosges）、完全现代的[龐畢度中心和中世纪风格的](../Page/蓬皮杜中心.md "wikilink")[玛莱区南部](../Page/玛莱区.md "wikilink")，今天以巴黎的同性恋区著称（玛莱区北面较安静的部分属于[第三区](../Page/巴黎第三区.md "wikilink")）。[西岱岛的东部](../Page/西岱岛.md "wikilink")（包括[巴黎圣母院](../Page/巴黎圣母院.md "wikilink")）以及[圣路易岛也属于第四区](../Page/圣路易岛.md "wikilink")。

## 地理

[缩略图](https://zh.wikipedia.org/wiki/File:Paris_4th.png "fig:缩略图")
该区土地面积1.601 km²，是巴黎市第三小的区。

## 历史

公元前1世纪时，[西岱岛上已经有人居住](../Page/西岱岛.md "wikilink")，居民是[高卢的Parisii部落](../Page/高卢.md "wikilink")。公元5世纪，右岸开始形成居民点。自19世纪以来，[玛莱区聚集了众多的](../Page/玛莱区.md "wikilink")[犹太人](../Page/犹太人.md "wikilink")，[蔷薇街](../Page/蔷薇街.md "wikilink")（Rue
des Rosiers）位于犹太区的核心，有不少适应犹太教戒律的
餐馆。1990年代以来，同性恋文化影响到该区，在市政厅附近开设了许多酒吧、咖啡馆。

## 人口

该区人口的峰值出现在1860年区划调整以前。1999年，该区人口为30,675人，提供工作岗位41,424个。

### 历史上的人口

<table class="wikitable">

<th>

年份

<th>

人口

<th>

密度
（人/km²）

</tr>

<tr>

<td>

1861年 <small>（峰值）</small>

<td>

108,520

<td>

67,783

<tr>

<td>

1872年

<td>

95,003

<td>

59,377

<tr>

<td>

1954年

<td>

70,944

<td>

41,638

<tr>

<td>

1962年

<td>

61,670

<td>

38,520

<tr>

<td>

1968年

<td>

54,029

<td>

33,747

<tr>

<td>

1975年

<td>

40,466

<td>

25,275

<tr>

<td>

1982年

<td>

33,990

<td>

21,230

<tr>

<td>

1990年

<td>

32,226

<td>

20,129

<tr>

<td>

1999年

<td>

30,675

<td>

19,160

<tr>

<td>

2005年

<td>

28,600

<td>

17,864

</table>

## 名胜

  - [市政厅巴扎百货公司](../Page/市政厅巴扎.md "wikilink")（Bazar de l'Hôtel de Ville）
  - 貝蒂（Berthillon）冰淇淋店
  - 兵工厂图书馆（Bibliothèque de l'Arsenal）
  - [龐畢度中心](../Page/蓬皮杜中心.md "wikilink")（Centre Georges Pompidou）
  - [巴黎主宫医院](../Page/巴黎主宫医院.md "wikilink")（Hôtel-Dieu）
  - [桑斯宫邸](../Page/桑斯宫邸.md "wikilink")（Hôtel de Sens）
  - [旭丽宫邸](../Page/旭丽宫邸.md "wikilink")（Hôtel de Sully）
  - [巴黎市政厅](../Page/巴黎市政厅.md "wikilink")（Hôtel de Ville）
  - [玛莱区](../Page/玛莱区.md "wikilink")（Le Marais）
  - [蔷薇街](../Page/蔷薇街.md "wikilink")（Rue des Rosiers）
  - 查理曼中学（Lycée Charlemagne）
  - [巴黎圣母院](../Page/巴黎圣母院.md "wikilink")（Cathédrale Notre Dame de Paris）
  - [圣礼拜堂](../Page/圣礼拜堂.md "wikilink")（La Sainte-Chapelle）
  - 警察总局（Prefecture de Police）
  - [圣雅各伯塔](../Page/圣雅各伯塔.md "wikilink")（Saint-Jacques Tower）
  - [圣杰维圣波蝶教堂](../Page/圣杰维圣波蝶教堂.md "wikilink")
  - [圣保禄圣路易教堂](../Page/圣保禄圣路易教堂.md "wikilink")
  - [岛上的圣路易教堂](../Page/岛上的圣路易教堂.md "wikilink")
  - 过去的[圣殿塔堡垒及监狱](../Page/圣殿塔.md "wikilink")
  - [Temple du Marais](../Page/Temple_du_Marais.md "wikilink")

### 街道与广场

  - [巴士底广场](../Page/巴士底广场.md "wikilink")（部分位于[十一区和](../Page/巴黎十一区.md "wikilink")[十二区](../Page/巴黎十二区.md "wikilink"))，包括[七月圆柱](../Page/七月圆柱.md "wikilink")（*Colonne
    de juillet*）
  - [市政厅广场](../Page/市政厅广场_\(巴黎\).md "wikilink"),原Place de Grève
  - [里窝利路](../Page/里窝利路.md "wikilink")（部分位于[第一区](../Page/巴黎第一区.md "wikilink")）
  - [孚日广场](../Page/孚日广场.md "wikilink")（部分位于[第三区](../Page/巴黎第三区.md "wikilink")）

## 外部链接

  -
[04](../Category/巴黎区份.md "wikilink")