**市光路站**位于[上海](../Page/上海.md "wikilink")[杨浦区](../Page/杨浦区.md "wikilink")[中原路](../Page/中原路.md "wikilink")[市光路](../Page/市光路.md "wikilink")，为[上海轨道交通8号线的地下](../Page/上海轨道交通8号线.md "wikilink")[侧式车站及起始站](../Page/侧式站台.md "wikilink")，于2007年12月29日启用。
[Shiguang_Road_Station_Exit_3.jpg](https://zh.wikipedia.org/wiki/File:Shiguang_Road_Station_Exit_3.jpg "fig:Shiguang_Road_Station_Exit_3.jpg")
[ShiguangRoadStation.JPG](https://zh.wikipedia.org/wiki/File:ShiguangRoadStation.JPG "fig:ShiguangRoadStation.JPG")

## 公交换乘

137、537、538、577、726、758、813、817、842、870

## 车站出口

1、2号口：中原路东侧，市光路北，开鲁路南（自北向南） 3、4号口：中原路东侧，市光路北，开鲁路南（自南向北）

## 邻近车站

[Category:杨浦区地铁车站](../Category/杨浦区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")