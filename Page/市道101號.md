**市道101號
三芝－淡水**，北起[新北市](../Page/新北市.md "wikilink")[三芝區](../Page/三芝區.md "wikilink")[台二線外環道東端](../Page/台二線.md "wikilink")，南至新北市[淡水區淡水國小前](../Page/淡水區.md "wikilink")，全長17.173公里（公路總局資料）。
有一條支線

## 行經行政區域、里程數

### 主線

  - [新北市](../Page/新北市.md "wikilink")

<!-- end list -->

  - [三芝區](../Page/三芝區.md "wikilink")：三芝外環道東端0.0k（**起點**，交叉）→北新莊8.175k（左[TW_CHW101a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW101a.svg "fig:TW_CHW101a.svg")市道101甲線岔路）
  - [淡水區](../Page/淡水區.md "wikilink")：瀾尾埔登輝大道口15.450k（岔路）→淡水國小前17.173k（**終點**，岔路）

### 支線

#### 甲線

[TW_CHW101a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW101a.svg "fig:TW_CHW101a.svg")
由新北市三芝區北新莊通往[陽明山](../Page/陽明山.md "wikilink")[小油坑](../Page/小油坑.md "wikilink")[陽金公路](../Page/陽金公路.md "wikilink")（）的道路，又稱「巴拉卡公路」（巴拉卡、百拉卡、百六戛），長10.740公里。該路為1952年由國軍工兵部隊歷經半年修築而成之戰備道路。由於路面狹窄且彎道、坡度亦大，本路已禁止甲類大客車通行。該路沿線有于右任墓、大屯自然公園、二子坪遊客中心（大屯山登山口）等。

  - [新北市](../Page/新北市.md "wikilink")

<!-- end list -->

  - [三芝區](../Page/三芝區.md "wikilink")：北新莊0.0k（**起點**，岔路）→陽明山國家公園界碑→車埕→巴拉卡（百六戛）
  - [淡水區](../Page/淡水區.md "wikilink")：百六戛地區→于右任墓園
  - 三芝區：于右任墓園→大屯自然公園、二子坪→市界10.740k（鞍部氣象站，**終點**）

## 沿革

  - 1966年縣道101號路線為金山-陽明山，全長23.800K。後改編為台2甲線。
  - 1976年由縣道102甲線改編為縣道101號，路線名為三芝-淡水，全長16.679K。

<!-- end list -->

  - 甲線

<!-- end list -->

  - 1966年路線名為北新莊-竹子湖，全長12.576K，為當時北14改編。
  - 2001年終點縮至新北、臺北市界，路線名亦改為北新莊-北市界。

## 沿線風景區

  - [大屯自然公園](../Page/大屯自然公園.md "wikilink")
  - [于右任墓園](../Page/于右任.md "wikilink")
  - [大屯山主峰](../Page/大屯山.md "wikilink")

## 相關連結

[Category:台灣市道](../Category/台灣市道.md "wikilink")
[Category:新北市道路](../Category/新北市道路.md "wikilink")
[Category:台北市道路](../Category/台北市道路.md "wikilink")