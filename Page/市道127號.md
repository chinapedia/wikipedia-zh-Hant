**市道127號
大雅－霧峰**，北起[臺中市](../Page/臺中市.md "wikilink")[大雅區四塊厝](../Page/大雅區.md "wikilink")，南至臺中市[霧峰區](../Page/霧峰區.md "wikilink")，全長共計25.012公里（公路總局資料）。

## 行經行政區域

  - 全線均位於[臺中市境內](../Page/臺中市.md "wikilink")

<!-- end list -->

  - [大雅區](../Page/大雅區.md "wikilink")：四塊厝0.0k（**起點**，岔路）
  - [西屯區](../Page/西屯區.md "wikilink")：廣福環中路口（橋下道路）→[朝馬](../Page/朝馬.md "wikilink")5.623k（岔路）
  - [南屯區](../Page/南屯區.md "wikilink")：南屯老街8.882k（岔路）
  - [烏日區](../Page/烏日區.md "wikilink")：烏日13.29k（岔路）→喀哩20.1k（左[環中路八段](../Page/環中路.md "wikilink")，可接[烏日交流道](../Page/烏日交流道.md "wikilink")）
  - [霧峰區](../Page/霧峰區.md "wikilink")：四塊厝21.55k（中投公路橋下北側可接共同[中投交流道](../Page/中投交流道.md "wikilink")）→霧峰25.012k（**終點**，岔路）

## 沿線風景

  - [僑光科技大學](../Page/僑光科技大學.md "wikilink")
  - [逢甲夜市](../Page/逢甲夜市.md "wikilink")
  - [黎明新村](../Page/黎明新村.md "wikilink")
  - [行政院中部聯合服務中心](../Page/行政院中部聯合服務中心.md "wikilink")
  - [國立公共資訊圖書館黎明分館](../Page/國立公共資訊圖書館.md "wikilink")
  - [南屯老街](../Page/南屯老街.md "wikilink")
  - [南屯文昌廟](../Page/南屯文昌廟.md "wikilink")
  - [南屯萬和宮](../Page/萬和宮_\(台中市\).md "wikilink")
  - [南屯楓樹社區](../Page/楓樹里_\(臺中市\).md "wikilink")
  - [臺灣菸酒公司烏日酒廠](../Page/臺灣菸酒公司.md "wikilink")
  - 烏日區公所
  - 四德二號埤柳樹公園（烏日區、霧峰區交界處）
  - 霧峰工業區
  - 北柳里平溪河生態區
  - [霧峰樹仁商圈](../Page/霧峰樹仁商圈.md "wikilink")

## 交流道

  - [TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[烏日交流道](../Page/烏日交流道.md "wikilink")
  - [TW_PHW74.svg](https://zh.wikipedia.org/wiki/File:TW_PHW74.svg "fig:TW_PHW74.svg")[溪南交流道](../Page/溪南交流道.md "wikilink")（計劃中）

## 註釋

## 參考資料

[Category:台灣市道](../Category/台灣市道.md "wikilink")
[Category:台中市道路](../Category/台中市道路.md "wikilink")
[路](../Category/西屯區.md "wikilink") [路](../Category/霧峰區.md "wikilink")