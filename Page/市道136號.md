**市道136號**是位於台灣中部的[市道](../Page/市道.md "wikilink")，西起[臺中市](../Page/臺中市.md "wikilink")[龍井區](../Page/龍井區.md "wikilink")，東至[南投縣](../Page/南投縣.md "wikilink")[國姓鄉龜溝](../Page/國姓鄉.md "wikilink")，全長共計49.666公里\[1\]。原起點為[梧棲區台中港](../Page/梧棲區.md "wikilink")，，故起點修正至[龍井區後壁崙](../Page/龍井區.md "wikilink")\[2\]。

## 行經行政區域

（劃橫線為已解編路段）

  - [臺中市](../Page/臺中市.md "wikilink")

<!-- end list -->

  - <s>[梧棲區](../Page/梧棲區.md "wikilink")：中二路（台中港區）、[臨港路口](../Page/臨港路.md "wikilink")0.0k（岔路）→安良港（[龍井交流道](../Page/台61線#交流道.md "wikilink")）</s>
  - <s>[龍井區](../Page/龍井區.md "wikilink")：向上路（原港南路）、[中華路口](../Page/中華路_\(台中市海線\).md "wikilink")（岔路）[縱貫鐵路](../Page/縱貫線_\(鐵路\).md "wikilink")[海線跨越橋](../Page/海岸線_\(臺鐵\).md "wikilink")</s>
  - <s>[沙鹿區](../Page/沙鹿區.md "wikilink")：鹿港寮（區道中65線岔路）→[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[龍井交流道](../Page/龍井交流道_\(國道3號\).md "wikilink")</s>
  - [龍井區](../Page/龍井區.md "wikilink")：後壁崙0k，中興路（**起點**，接特五號道路）
  - [大肚區](../Page/大肚區.md "wikilink")：蔗廍
  - [南屯區](../Page/南屯區.md "wikilink")：知高-{庄}-，[五權西路](../Page/五權西路.md "wikilink")、[忠勇路口](../Page/忠勇路.md "wikilink")6.9k（岔路）→[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[南屯交流道](../Page/南屯交流道.md "wikilink")7.3k→[南屯交流道](../Page/台74線#交流道列表.md "wikilink")7.8k→南屯老街9.7k（岔路）
  - [南區與](../Page/南區_\(臺中市\).md "wikilink")[西區區界](../Page/西區_\(臺中市\).md "wikilink")：
  - [西區](../Page/西區_\(臺中市\).md "wikilink")：[南屯路](../Page/南屯路.md "wikilink")、[五權路口](../Page/五權路_\(台中市區\).md "wikilink")13.2k（岔路）→自由林森路口（[台中地方法院](../Page/台中地方法院.md "wikilink")）
  - [南區](../Page/南區_\(臺中市\).md "wikilink")：[台中林氏宗廟](../Page/台中林氏宗廟.md "wikilink")→[台中文化創意園區](../Page/台中文化創意園區.md "wikilink")→民意街
  - [東區](../Page/東區_\(臺中市\).md "wikilink")：[台中後火車站](../Page/臺中車站.md "wikilink")→[新時代購物中心](../Page/新時代購物中心.md "wikilink")→[振興路](../Page/振興路_\(台中市\).md "wikilink")、[建成路口](../Page/建成路_\(台中市\).md "wikilink")17.3k（岔路）→[東門橋](../Page/東門橋.md "wikilink")→[大里溪太平橋](../Page/大里溪.md "wikilink")
  - [太平區](../Page/太平區_\(臺中市\).md "wikilink")：太平→山腳21.8k（與共線起點）→[一江橋東端](../Page/一江橋.md "wikilink")22.5k（與共線終點））→頭汴坑24.613k（區道中99）線→中埔29.25k（蝙蝠洞，區道中100線）→鹿食水40.756k（中投縣市界）

<!-- end list -->

  - [南投縣](../Page/南投縣.md "wikilink")

<!-- end list -->

  - [國姓鄉](../Page/國姓鄉.md "wikilink")：龜溝49.666k（**終點**，岔路）

## 沿線風景

  - [南屯老街](../Page/南屯老街.md "wikilink")
  - [太平蝙蝠洞](../Page/太平蝙蝠洞.md "wikilink")
  - [赤崁頂](../Page/赤崁頂.md "wikilink")
  - [九九峰](../Page/九九峰.md "wikilink")

## 休閒活動

**市道136號**東段，起自[台中市](../Page/台中市.md "wikilink")[太平區一江橋](../Page/太平區_\(台灣\).md "wikilink")，終至赤崁頂，為[臺灣中部著名之自行車訓練道](../Page/臺灣中部.md "wikilink")。長約15公里，最高坡度12%。例假日有大量車友在此訓練，赤崁頂也有小吃販賣車為車友服務。

## 交流道

  - <s>[TW_PHW61.svg](https://zh.wikipedia.org/wiki/File:TW_PHW61.svg "fig:TW_PHW61.svg")[龍井交流道](../Page/龍井交流道_\(台61線\).md "wikilink")</s>
  - <s>[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[龍井交流道](../Page/龍井交流道_\(國道3號\).md "wikilink")</s>
  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[南屯交流道](../Page/南屯交流道.md "wikilink")
  - [TW_PHW74.svg](https://zh.wikipedia.org/wiki/File:TW_PHW74.svg "fig:TW_PHW74.svg")[南屯二交流道](../Page/南屯二交流道.md "wikilink")
  - [TW_PHW74.svg](https://zh.wikipedia.org/wiki/File:TW_PHW74.svg "fig:TW_PHW74.svg")[大里一交流道](../Page/大里一交流道_\(台74線\).md "wikilink")

## 參見

  - [向上路](../Page/向上路_\(台中市\).md "wikilink")
  - [五權西路](../Page/五權西路.md "wikilink")
  - [南屯路](../Page/南屯路.md "wikilink")
  - [復興路](../Page/復興路_\(台中市\).md "wikilink")
  - [振興路](../Page/振興路_\(台中市\).md "wikilink")
  - [太平路](../Page/太平路_\(太平區\).md "wikilink")
  - [東平路](../Page/東平路_\(太平區\).md "wikilink")

## 参考文獻

## 外部連結

[Category:台灣市道](../Category/台灣市道.md "wikilink")
[Category:台中市道路](../Category/台中市道路.md "wikilink")
[Category:南投縣道路](../Category/南投縣道路.md "wikilink")

1.
2.  交通部於2004年3月16日公告原縣道一三六線0K+000-11k+694路段（
    0K+000-8K+000屬未開闢路段）解編、原8K+000-11k+694（龍井區中沙路）路段移交地方政府管養