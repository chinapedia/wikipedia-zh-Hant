'''市道165號 後-{庄}-－官田
'''，是位於[臺灣](../Page/臺灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")、[臺南市兩縣市之間的南北向市](../Page/臺南市.md "wikilink")（縣）道。北起[嘉義縣](../Page/嘉義縣.md "wikilink")[中埔鄉後](../Page/中埔鄉.md "wikilink")-{庄}-，南至[臺南市](../Page/臺南市.md "wikilink")[官田區](../Page/官田區.md "wikilink")，全長共計37.107公里（[公路總局資料](../Page/交通部公路總局.md "wikilink")）。

## 行經行政區域

  - [嘉義縣](../Page/嘉義縣.md "wikilink")

<!-- end list -->

  - [中埔鄉](../Page/中埔鄉.md "wikilink")：後-{庄}-0.0k（岔路）→嘉義交流道2.4k（）
  - [水上鄉](../Page/水上鄉.md "wikilink")：中-{庄}-4.442k（前終點岔路/本線行左岔）→紅毛寮5.454k（嘉南縣市界）

<!-- end list -->

  - [臺南市](../Page/臺南市.md "wikilink")

<!-- end list -->

  - [白河區](../Page/白河區.md "wikilink")：內角 7.334k（鄉道南86線）→
    富民三民路口13.3k（右終點岔路）→富民中正路口13.68k（與共線起點）→富民中山路口（與共線終點）
  - [東山區](../Page/東山區_\(臺南市\).md "wikilink")：東山17.032k（鄉道南100線岔路）→北馬19.36k→東河20.312k
  - [柳營區](../Page/柳營區.md "wikilink")：果毅後26.971k（[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")柳營交流道）
  - [六甲區](../Page/六甲區.md "wikilink")：六甲30.875k（）
  - [官田區](../Page/官田區.md "wikilink")：烏山頭32.866k（左）→中脅34.884k→官田37.107k（岔路）

## 歷史沿革

  - 1961年公告**縣道165號
    嘉義－官田**，總長40.369公里，北起嘉義台1線262k+882處，南至官田台1線301k+728處，另有一支線**縣道165甲
    後-{庄}-－中埔** ，總長7.620公里，西起後庄165線3k+878，東至中埔台3線304k+330。
  - 1976年165線0k\~3k+878併入[台18線](../Page/台18線.md "wikilink")，改稱後-{庄}-－官田，甲線廢止，後-{庄}-－頂六併入台18線，頂六-中埔改編為嘉135鄉道。
  - 2013年起縣道165號在[台南市境內路段改制為](../Page/臺南市.md "wikilink")**市道165號**；嘉義縣部份則不變。

## 交流道

  -   - [柳營交流道](../Page/柳營交流道.md "wikilink")（322k）

  -   - [嘉義交流道](../Page/台82線.md "wikilink")（32k）

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:台灣市道](../Category/台灣市道.md "wikilink")
[Category:嘉義縣道路](../Category/嘉義縣道路.md "wikilink")
[Category:台南市道路](../Category/台南市道路.md "wikilink")