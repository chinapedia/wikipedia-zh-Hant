**布卢姆茨伯里**（**Bloomsbury**），是[伦敦市區內的一个地區](../Page/伦敦.md "wikilink")，行政上屬於[卡姆登區的管轄範圍](../Page/卡姆登區.md "wikilink")，位在[內倫敦的西北角](../Page/內倫敦.md "wikilink")。位於[尤斯頓路和](../Page/尤斯頓路.md "wikilink")[霍本之間](../Page/霍本.md "wikilink")。17至18世紀間由[貝德福德公爵開發成時尚住宅區](../Page/貝德福德公爵.md "wikilink")。因為當地有大量[花園廣場](../Page/花園廣場.md "wikilink")、\[1\]
學府和醫療機構而出名。雖然布盧姆茨伯里不是倫敦第一個有廣場的地區，但1660年建造的[布盧姆茨伯里廣場卻是第一個命名為](../Page/布盧姆茨伯里廣場.md "wikilink")“廣場”（Square）的地點。\[2\]

[倫敦大學的許多學院都位於此地](../Page/倫敦大學.md "wikilink")，其中包括[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")、[伯貝克學院](../Page/倫敦大學伯貝克學院.md "wikilink")、[倫敦衛生與熱帶醫學院和](../Page/倫敦衛生與熱帶醫學院.md "wikilink")[亞非學院](../Page/倫敦大學亞非學院.md "wikilink")。醫療機構則有[英國醫學協會](../Page/英國醫學協會.md "wikilink")、[大奧蒙德街醫院](../Page/大奧蒙德街醫院.md "wikilink")、[國家神經病學和神經外科醫院](../Page/國家神經病學和神經外科醫院.md "wikilink")、[大學學院醫院和](../Page/大學學院醫院.md "wikilink")[皇家倫敦整合醫學醫院](../Page/皇家倫敦整合醫學醫院.md "wikilink")。此外的著名地點還有[大英博物館和](../Page/大英博物館.md "wikilink")[皇家戲劇藝術學院](../Page/皇家戲劇藝術學院.md "wikilink")。

## 參考文獻

## 擴展閱讀

  -
## 外部連結

  -
[Category:倫敦前民政教區](../Category/倫敦前民政教區.md "wikilink")
[Category:倫敦地區](../Category/倫敦地區.md "wikilink")

1.  [Guide to London
    Squares](http://www.gardenvisit.com/landscape/london/lguide/london-squares.htm)
    . Retrieved 8 March 2007.
2.  [The London
    Encyclopaedia](../Page/The_London_Encyclopaedia.md "wikilink"),
    Edited by Ben Weinreb and Christopher Hibbert. Macmillan London Ltd
    1983