[Buji_Checkpoint.JPG](https://zh.wikipedia.org/wiki/File:Buji_Checkpoint.JPG "fig:Buji_Checkpoint.JPG")
**布吉街道**位於[中國南部](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[深圳市東北的](../Page/深圳市.md "wikilink")[龍崗區](../Page/龍崗區.md "wikilink")，緊鄰[羅湖區](../Page/羅湖區.md "wikilink")。

## 歷史

布吉街道原為[布吉鎮的一部份](../Page/布吉鎮.md "wikilink")，為[客家人之聚居地](../Page/客家人.md "wikilink")，“布吉”一名由“莆隔”讹变而来\[1\]：[清朝时期](../Page/清朝.md "wikilink")，在如今广深铁路布吉段西侧有一个名叫“莆隔村”的村落，属[新安县管辖](../Page/寶安縣.md "wikilink")。由于[客家话中](../Page/客家话.md "wikilink")“莆”与“布”的发音十分相似，自清代中叶以后，莆隔村便渐被当地人称为“布隔村”。咸丰二年（1852年），当地人在莆隔村南面建起一个墟市，名叫“丰和墟”，并逐渐成为商品、农贸产品的集散地。1911年，广九铁路华段在丰和墟设置了一个车站，并命名为“[布吉站](../Page/深圳东站.md "wikilink")”，自此“布隔”遂称之为“布吉”\[2\]\[3\]。1979年，布吉第一家製衣工廠設立，自此發展成一個工業區。在1995年，布吉工业总产值為1297亿元[人民幣](../Page/人民幣.md "wikilink")。

2004年8月26日，布吉鎮被分拆為布吉、[-{坂田}-](../Page/坂田街道.md "wikilink")、[南湾](../Page/南湾街道_\(深圳市\).md "wikilink")（由南岭和沙湾组成）三个街道办。

## 地理

辖区总面积30.89平方公里，常住总人口75.37万，其中户籍人口7.13万人。

## 經濟

在改革開放早期，布吉鎮是深圳市主要的工業區之一。由於離羅湖區僅4公里左右，大多數在[深圳經濟特區工作的人](../Page/深圳經濟特區.md "wikilink")，都是居於布吉，使布吉成為深圳其中最主要的住宅區之一。目前辖区内有[中海怡翠山莊](../Page/中海怡翠山莊.md "wikilink")、茂业城帝景峰、大世纪花园、大世纪杰座、东方半岛、丽湖、[信义假日名城](../Page/信义假日名城.md "wikilink")、阳光、[桂芳園等数十个大型屋苑](../Page/桂芳園.md "wikilink")。

另外還有一個[大芬村](../Page/大芬村.md "wikilink")，專門出售油畫等相關產品。此村是在20世紀80年代末，由一位香港畫商黃江發起。目前有一萬多外來人口從事油畫批發。

## 交通

### 道路

  - [深惠路](../Page/深惠路.md "wikilink")：連接羅湖區之間。
  - [布龙路](../Page/布龙路.md "wikilink")：连接[龙华区](../Page/龙华区_\(深圳市\).md "wikilink")
  - [吉华路](../Page/吉华路.md "wikilink")（布龙公路旧线）：连接[龙华区](../Page/龙华区_\(深圳市\).md "wikilink")
  - [西环路](../Page/西环路.md "wikilink")
  - [清平高速](../Page/清平高速.md "wikilink")
  - [粤宝路](../Page/粤宝路.md "wikilink")
  - [中兴路](../Page/中兴路.md "wikilink")
  - [布李路](../Page/布李路.md "wikilink")

### 鐵路

  - [深圳地鐵龍崗線](../Page/深圳地鐵龍崗線.md "wikilink")：布吉站
  - [深圳地鐵環中線](../Page/深圳地鐵環中線.md "wikilink")：布吉站
  - [廣深鐵路](../Page/廣深鐵路.md "wikilink")：[深圳东站](../Page/深圳东站.md "wikilink")

## 參考文獻

## 外部連結

  - [龙岗政府在线：布吉街道辦](https://web.archive.org/web/20071005040105/http://www.lg.gov.cn/web/GovOpen/departinfo.aspx?depId=138)

[Category:龙岗区行政区划](../Category/龙岗区行政区划.md "wikilink")
[龙岗区](../Category/深圳街道_\(行政区划\).md "wikilink")

1.
2.
3.