<table>
<thead>
<tr class="header">
<th><p>布哈拉州<br />
</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>colspan=2; bgcolor= |<a href="https://zh.wikipedia.org/wiki/File:Buxoro_Viloyati_in_Uzbekistan.svg" title="fig:Buxoro_Viloyati_in_Uzbekistan.svg">Buxoro_Viloyati_in_Uzbekistan.svg</a></p></td>
</tr>
<tr class="even">
<td><p>基本統計資料</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/首府.md" title="wikilink">首府</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面積.md" title="wikilink">面積</a> :</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>(2005):<br />
 • <a href="../Page/人口密度.md" title="wikilink">密度</a> :</p></td>
</tr>
<tr class="even">
<td><p>主要<a href="../Page/民族.md" title="wikilink">民族</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ISO_3166-2.md" title="wikilink">行政區代碼</a>:</p></td>
</tr>
</tbody>
</table>

**布哈拉州**（[烏茲別克語](../Page/烏茲別克語.md "wikilink")：）是[烏茲別克十二個州份之一](../Page/烏茲別克.md "wikilink")。它涵蓋39,400平方公里，有人口1,384,700。布哈拉州下轄11個农业区，[首府設於](../Page/首府.md "wikilink")[布哈拉市](../Page/布哈拉.md "wikilink")。

## 地理位置

布哈拉州位於[烏茲別克中部](../Page/烏茲別克.md "wikilink")。它與以下州份或國家相連（從北開始逆時針）：[納沃伊州](../Page/納沃伊州.md "wikilink")、[卡拉卡爾帕克斯坦自治共和國](../Page/卡拉卡爾帕克斯坦自治共和國.md "wikilink")、[花拉子模州](../Page/花拉子模州.md "wikilink")、[土庫曼](../Page/土庫曼.md "wikilink")、[卡什卡達里亞州](../Page/卡什卡達里亞州.md "wikilink")。

## 行政区划

布哈拉州分为11个[区](../Page/行政区.md "wikilink")，如下：

1.  [阿拉特区](../Page/阿拉特区.md "wikilink")（）
2.  [布哈拉区](../Page/布哈拉区.md "wikilink")（）
3.  [瓦布肯特区](../Page/瓦布肯特区.md "wikilink")（）
4.  [吉日杜万区](../Page/吉日杜万区.md "wikilink")（）
5.  [占达尔区](../Page/占达尔区.md "wikilink")（）
6.  [卡甘区](../Page/卡甘区.md "wikilink")（）
7.  [卡拉乌尔巴扎尔区](../Page/卡拉乌尔巴扎尔区.md "wikilink")（）
8.  [卡拉库尔区](../Page/卡拉库尔区.md "wikilink")（）
9.  [佩什昆区](../Page/佩什昆区.md "wikilink")（）
10. [罗米坦区](../Page/罗米坦区.md "wikilink")（）
11. [沙菲尔坎区](../Page/沙菲尔坎区.md "wikilink")（）

## 重要城镇

  - [布哈拉市](../Page/布哈拉.md "wikilink")（）
  - [阿拉特市](../Page/阿拉特.md "wikilink")（）
  - [卡拉科尔市](../Page/卡拉科尔.md "wikilink")（）
  - [加拉西亚市](../Page/加拉西亚.md "wikilink")（）
  - [加兹利市](../Page/加兹利.md "wikilink")（）
  - [吉日杜万市](../Page/吉日杜万.md "wikilink")（）
  - [卡甘市](../Page/卡甘.md "wikilink")（）
  - [罗米坦市](../Page/罗米坦.md "wikilink")（）
  - [沙夫里坎市](../Page/沙夫里坎.md "wikilink")（）
  - [瓦布肯特市](../Page/瓦布肯特.md "wikilink")（）

## 經濟

布哈拉州有著豐富的[花岡石](../Page/花岡石.md "wikilink")、[石灰和](../Page/石灰.md "wikilink")[硫磺儲藏](../Page/硫磺.md "wikilink")。工業以[棉花和](../Page/棉花.md "wikilink")[布料生產為主](../Page/布料.md "wikilink")。另外，布哈拉州亦蘊藏著[天然氣和](../Page/天然氣.md "wikilink")[石油等資源](../Page/石油.md "wikilink")。[烏茲別克最大的](../Page/烏茲別克.md "wikilink")[石油提煉廠正坐落在布哈拉州內](../Page/石油提煉.md "wikilink")。

## 参见

  - [卡拉库尔绵羊](../Page/卡拉库尔绵羊.md "wikilink")：得名于该州城市[卡拉库尔的一种绵羊](../Page/卡拉庫爾_\(烏茲別克\).md "wikilink")

## 參考

  - [布哈拉州简介](http://www.zy.gov.cn/news.asp?newsid=11147) 中亚科技经济信息网

[Category:烏茲別克行政區劃](../Category/烏茲別克行政區劃.md "wikilink")