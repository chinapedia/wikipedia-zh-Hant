**布基农省**（**Bukidnon**）是[菲律賓的一個內陸](../Page/菲律賓.md "wikilink")[省份](../Page/菲律賓省份.md "wikilink")，位於[北民答那峨](../Page/北民答那峨.md "wikilink")[政區](../Page/菲律賓政區.md "wikilink")，首府為[馬拉巴拉市](../Page/馬拉巴拉市.md "wikilink")（Malaybalay
City）。布基农省從北順時針相鄰於[東米撒米斯省](../Page/東米撒米斯省.md "wikilink")（Misamis
Oriental）、[南阿古桑省](../Page/南阿古桑省.md "wikilink")（Agusan del
Sur）、[北達沃省](../Page/北達沃省.md "wikilink")（Davao del
Norte）、[哥打巴托省](../Page/哥打巴托省.md "wikilink")（Cotabato）、[南拉瑙省](../Page/南拉瑙省.md "wikilink")（Lanao
del Sur）及[北拉瑙省](../Page/北拉瑙省.md "wikilink")（Lanao del Norte）。

[B](../Category/菲律賓省份.md "wikilink")