**可滿足性**（英語：**Satisfiability**）是用來解決給定的[真值方程式](../Page/真值.md "wikilink")，是否存在一组变量赋值，使問題为可满足。布尔可滿足性問題（**Boolean
satisfiability
problem**；**SAT)**）屬於[決定性問題](../Page/決定性問題.md "wikilink")，也是第一个被证明屬於[NP完全的问题](../Page/NP完全.md "wikilink")。此問題在[電腦科學上許多領域的皆相當重要](../Page/電腦科學.md "wikilink")，包括[電腦科學基礎理論](../Page/電腦科學基礎理論.md "wikilink")、[演算法](../Page/演算法.md "wikilink")、[人工智慧](../Page/人工智慧.md "wikilink")、[硬體設計等等](../Page/硬體設計.md "wikilink")。

## 直观描述

  - 对于一个确定的逻辑电路，是否存在一种输入使得输出为真。

## 外部連結

SAT Solvers:

  - [Chaff](http://www.princeton.edu/~chaff/)
  - [HyperSAT](http://www.cs.ubc.ca/~babic/index_hypersat.htm)
  - [Spear](http://www.cs.ubc.ca/~babic/index_spear.htm)
  - [The MiniSAT
    Solver](http://www.cs.chalmers.se/Cs/Research/FormalMethods/MiniSat/MiniSat.html)
  - [UBCSAT](https://web.archive.org/web/20070804183143/http://www.satlib.org/ubcsat/)

Conferences/Publications:

  - [SAT 2007: Tenth International Conference on Theory and Applications
    of Satisfiability Testing](http://sat07.ecs.soton.ac.uk/)
  - [Journal on Satisfiability, Boolean Modeling and
    Computation](https://web.archive.org/web/20060219180520/http://jsat.ewi.tudelft.nl/)
  - [Survey
    Propagation](https://web.archive.org/web/20060210192113/http://www.ictp.trieste.it/~zecchina/SP/)

Benchmarks:

  - [Forced Satisfiable SAT
    Benchmarks](http://www.nlsde.buaa.edu.cn/~kexu/benchmarks/benchmarks.htm)
  - [IBM Formal Verification SAT
    Benchmarks](https://web.archive.org/web/20070717105815/http://www.haifa.il.ibm.com/projects/verification/RB_Homepage/bmcbenchmarks.html)
  - [SATLIB](http://www.satlib.org)
  - [Software Verification
    Benchmarks](http://www.cs.ubc.ca/~babic/index_benchmarks.htm)

SAT solving in general:

  - <http://www.satlive.org>
  - <http://www.satisfiability.org>
  - [Sat4j](http://www.sat4j.org/)

[Category:计算机逻辑](../Category/计算机逻辑.md "wikilink")
[Category:形式方法](../Category/形式方法.md "wikilink")
[Category:NP完全问题](../Category/NP完全问题.md "wikilink")