**布尔津县**（[维吾尔语](../Page/维吾尔语.md "wikilink")：بۇرچىن
ناھىيىسى）是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[阿勒泰地区下辖的一个](../Page/阿勒泰地区.md "wikilink")[县](../Page/县.md "wikilink")。面积10362平方千米，人口7万。邮政编码836600。县人民政府驻[布尔津镇](../Page/布尔津镇.md "wikilink")。

县城西北有著名旅游景点[喀纳斯和](../Page/喀纳斯.md "wikilink")[喀纳斯湖](../Page/喀纳斯湖.md "wikilink")。

## 沿革

民國三年（1914年）由[承化縣析置布爾津河設治局](../Page/承化縣.md "wikilink")，以瀕於[布尔津河而名](../Page/布尔津河.md "wikilink")，民國八年（1919年）升為縣。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")：

。

## 地理狀況

## 参考文献

{{-}}

[布尔津县](../Category/布尔津县.md "wikilink")
[县](../Category/阿勒泰县市.md "wikilink")
[阿勒泰](../Category/新疆县份.md "wikilink")