**布尔萨**（[土耳其语](../Page/土耳其语.md "wikilink")：****），古稱**普魯薩**（），位于[土耳其西北部](../Page/土耳其.md "wikilink")，是该国第四大[城市](../Page/城市.md "wikilink")，也是[布尔萨省的首府](../Page/布尔萨省.md "wikilink")，人口超過200万。布尔萨以[滑雪胜地](../Page/滑雪.md "wikilink")、[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")[苏丹陵墓和肥沃的平原而闻名](../Page/苏丹_\(称谓\).md "wikilink")，因其[公园及](../Page/公园.md "wikilink")[花园之多而享有](../Page/花园.md "wikilink")（即“绿色布尔萨”）的美称，同时也是许多著名的土耳其食品的发源地。

## 历史

布尔萨曾是奥斯曼帝国的首都（1326年-1365年），也是[丝绸之路的临近西方终点的主要城市](../Page/丝绸之路.md "wikilink")。

## 经济

布尔萨是土耳其的汽车工业中心，[菲亚特和](../Page/菲亚特.md "wikilink")[雷诺在此均设有](../Page/雷诺汽车.md "wikilink")[工厂](../Page/工厂.md "wikilink")，在[工业区内也有](../Page/工业区.md "wikilink")[紡織品与](../Page/紡織品.md "wikilink")[食品工业工厂](../Page/食品工业.md "wikilink")，包括[可口可乐](../Page/可口可乐.md "wikilink")、[百事可樂](../Page/百事可樂.md "wikilink")、和许多[罐裝食品公司的工厂](../Page/罐裝食品.md "wikilink")。布尔萨是一个[旅游业发达的城市](../Page/旅游业.md "wikilink")，而附近的[烏魯達山有土耳其最大的滑雪场之一](../Page/烏魯達山.md "wikilink")。

[Bursa,_Zafer_Plaza_shopping_center.jpg](https://zh.wikipedia.org/wiki/File:Bursa,_Zafer_Plaza_shopping_center.jpg "fig:Bursa,_Zafer_Plaza_shopping_center.jpg")

## 教育

[乌鲁达大学校园位于布尔萨](../Page/乌鲁达大学.md "wikilink")，该校为土耳其有名的[大学之一](../Page/大学.md "wikilink")。

## 体育

[布尔萨阿塔土克体育场](../Page/布尔萨阿塔土克体育场.md "wikilink")（）是一个多种用途的运动场地，可容纳1.97万名观众，建于1979年，一直作为[布尔萨体育](../Page/布尔萨体育.md "wikilink")（）的主场。布尔萨足球队又称“绿鳄队”（），参与[土耳其足球超级联赛](../Page/土耳其足球超级联赛.md "wikilink")，于1986年夺得[土耳其杯](../Page/土耳其足球杯.md "wikilink")。

布尔萨将申请举办2018年[冬季奥运会](../Page/冬季奥运会.md "wikilink")。

## 文化

[Iskender_kebab.jpg](https://zh.wikipedia.org/wiki/File:Iskender_kebab.jpg "fig:Iskender_kebab.jpg")

### 美食

  - [栗子](../Page/栗子.md "wikilink")[甜品](../Page/甜品.md "wikilink")
  - [水蜜桃](../Page/桃.md "wikilink")
  - “布尔萨[卡巴](../Page/卡巴_\(食物\).md "wikilink")”，一种[烤肉](../Page/烤肉.md "wikilink")

## 旅游

[Uludag_mountain.jpg](https://zh.wikipedia.org/wiki/File:Uludag_mountain.jpg "fig:Uludag_mountain.jpg")

布尔萨旅游景点：

  - [乌鲁达山国家公园](../Page/乌鲁达山国家公园.md "wikilink")
  - [盖姆利克](../Page/盖姆利克.md "wikilink")[温泉](../Page/温泉.md "wikilink")
  - 古城墙
  - [伊兹尼克大墓地](../Page/伊兹尼克.md "wikilink")
  - [清真寺与](../Page/清真寺.md "wikilink")[教堂](../Page/教堂.md "wikilink")
  - 布尔萨考古博物馆

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a> <a href="../Page/德国.md" title="wikilink">德国</a> <a href="../Page/达姆施塔特.md" title="wikilink">达姆施塔特</a>（1971年7月7日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Bosnia_and_Herzegovina.svg" title="fig:Flag_of_Bosnia_and_Herzegovina.svg">Flag_of_Bosnia_and_Herzegovina.svg</a> <a href="../Page/波斯尼亚和黑塞哥维那.md" title="wikilink">波斯尼亚和黑塞哥维那</a><a href="../Page/萨拉热窝.md" title="wikilink">萨拉热窝</a>（1972年6月30日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Pakistan.svg" title="fig:Flag_of_Pakistan.svg">Flag_of_Pakistan.svg</a> <a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦</a><a href="../Page/木尔坦.md" title="wikilink">木尔坦</a>（1975年2月24日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg" title="fig:Flag_of_Finland.svg">Flag_of_Finland.svg</a> <a href="../Page/芬兰.md" title="wikilink">芬兰</a><a href="../Page/奥卢.md" title="wikilink">奥卢</a>（1979年7月7日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg" title="fig:Flag_of_the_United_States.svg">Flag_of_the_United_States.svg</a> <a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/蒂芬_(俄亥俄州).md" title="wikilink">蒂芬</a>（1983年5月）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Tunisia.svg" title="fig:Flag_of_Tunisia.svg">Flag_of_Tunisia.svg</a> <a href="../Page/突尼西亞.md" title="wikilink">突尼西亞</a><a href="../Page/凯鲁万.md" title="wikilink">凯鲁万</a>（1987年12月26日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Turkey.svg" title="fig:Flag_of_Turkey.svg">Flag_of_Turkey.svg</a> <a href="../Page/土耳其.md" title="wikilink">土耳其</a><a href="../Page/代尼兹利.md" title="wikilink">代尼兹利</a>（1988年2月4日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Turkish_Republic_of_Northern_Cyprus.svg" title="fig:Flag_of_the_Turkish_Republic_of_Northern_Cyprus.svg">Flag_of_the_Turkish_Republic_of_Northern_Cyprus.svg</a> <a href="../Page/北塞浦路斯土耳其共和国.md" title="wikilink">北塞浦路斯土耳其共和国</a><a href="../Page/尼科西亚.md" title="wikilink">尼科西亚</a>（1990年12月17日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_People&#39;s_Republic_of_China.svg" title="fig:Flag_of_the_People&#39;s_Republic_of_China.svg">Flag_of_the_People's_Republic_of_China.svg</a> <a href="../Page/中华人民共和国.md" title="wikilink">中华人民共和国</a><a href="../Page/鞍山市.md" title="wikilink">鞍山市</a>（1991年11月8日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Macedonia.svg" title="fig:Flag_of_Macedonia.svg">Flag_of_Macedonia.svg</a> <a href="../Page/马其顿.md" title="wikilink">马其顿</a><a href="../Page/比托拉.md" title="wikilink">比托拉</a>（1996年11月3日）</li>
</ul></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Israel.svg" title="fig:Flag_of_Israel.svg">Flag_of_Israel.svg</a> <a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/赫茨利亞.md" title="wikilink">赫茨利亞</a>（1997年8月15日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Moldova.svg" title="fig:Flag_of_Moldova.svg">Flag_of_Moldova.svg</a> <a href="../Page/摩尔多瓦.md" title="wikilink">摩尔多瓦</a><a href="../Page/恰德尔隆加.md" title="wikilink">恰德尔隆加</a>（1997年9月23日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kazakhstan.svg" title="fig:Flag_of_Kazakhstan.svg">Flag_of_Kazakhstan.svg</a> <a href="../Page/哈萨克斯坦.md" title="wikilink">哈萨克斯坦</a><a href="../Page/克孜勒奥尔达.md" title="wikilink">克孜勒奥尔达</a>（1997年10月4日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Algeria.svg" title="fig:Flag_of_Algeria.svg">Flag_of_Algeria.svg</a> <a href="../Page/阿尔及利亚.md" title="wikilink">阿尔及利亚</a><a href="../Page/穆阿斯凱爾.md" title="wikilink">穆阿斯凱爾</a>（1998年5月25日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a> <a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/库爾姆巴赫.md" title="wikilink">库爾姆巴赫</a>（1998年10月29日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Bulgaria.svg" title="fig:Flag_of_Bulgaria.svg">Flag_of_Bulgaria.svg</a> <a href="../Page/保加利亚.md" title="wikilink">保加利亚</a><a href="../Page/普列文.md" title="wikilink">普列文</a>（1998年10月30日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Bulgaria.svg" title="fig:Flag_of_Bulgaria.svg">Flag_of_Bulgaria.svg</a> <a href="../Page/保加利亚.md" title="wikilink">保加利亚</a><a href="../Page/普罗夫迪夫.md" title="wikilink">普罗夫迪夫</a>（1998年12月3日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Albania.svg" title="fig:Flag_of_Albania.svg">Flag_of_Albania.svg</a> <a href="../Page/阿尔巴尼亚.md" title="wikilink">阿尔巴尼亚</a><a href="../Page/地拉那.md" title="wikilink">地拉那</a>（1998年12月15日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Slovakia.svg" title="fig:Flag_of_Slovakia.svg">Flag_of_Slovakia.svg</a> <a href="../Page/斯洛伐克.md" title="wikilink">斯洛伐克</a><a href="../Page/科希策.md" title="wikilink">科希策</a>（2000年3月29日）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg" title="fig:Flag_of_Ukraine.svg">Flag_of_Ukraine.svg</a> <a href="../Page/乌克兰.md" title="wikilink">乌克兰</a><a href="../Page/文尼察.md" title="wikilink">文尼察</a>（2004年3月8日）</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [布尔萨市官方网站](http://www.bursa-bld.gov.tr/)
  - [乌鲁达大学](http://www.uludag.edu.tr/)
  - [布尔萨的2018年奥运会网](http://www.bursa2018.com/)

[Category:土耳其城市](../Category/土耳其城市.md "wikilink")
[Category:土耳其世界遺產](../Category/土耳其世界遺產.md "wikilink")