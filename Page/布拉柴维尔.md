[Kindepuisbrazza.jpg](https://zh.wikipedia.org/wiki/File:Kindepuisbrazza.jpg "fig:Kindepuisbrazza.jpg")所分隔\]\]

**布拉柴维尔**（），简称**布拉柴**，位于[刚果共和国南部](../Page/刚果共和国.md "wikilink")[刚果河畔的](../Page/刚果河.md "wikilink")[斯坦利湖](../Page/斯坦利湖.md "wikilink")北岸，与[刚果民主共和国首都](../Page/刚果民主共和国.md "wikilink")[金沙萨隔河相望](../Page/金沙萨.md "wikilink")，是[刚果共和国的首都和最大城市](../Page/刚果共和国.md "wikilink")。2005年人口1,174,000人。建立于1880年10月3日，取代了在殖民地时期之前的城镇恩库纳（Nkuna），主要部落是姆皮拉（Mpila）和姆法（Mfa）或姆福阿（Mfoa）。名字来源于[皮埃尔·萨沃尼昂·德·布拉柴](../Page/皮埃尔·萨沃尼昂·德·布拉柴.md "wikilink")。

## 地理

布拉柴维尔对岸（即[刚果河左岸](../Page/刚果河.md "wikilink")）是[刚果民主共和国首都](../Page/刚果民主共和国.md "wikilink")[金沙萨](../Page/金沙萨.md "wikilink")。两座首都规模差距悬殊，金沙萨2009年人口超过一千万。\[1\]为区别刚果河两岸的两个以“刚果”为国名的国家，刚果共和国（首都布拉柴维尔）被称为“刚果（布）”或“原法属刚果”，刚果民主共和国（1971年至1999年改名[扎伊尔](../Page/扎伊尔.md "wikilink")，首都金沙萨）被称为“刚果（金）”或“原比属刚果”。两国首都相距5到6公里，仅次于[罗马和](../Page/罗马.md "wikilink")[梵蒂冈之间的距离](../Page/梵蒂冈.md "wikilink")，也是全世界唯一一对隔河相望并能互相看见的首都。

布拉柴维尔位于[赤道以南](../Page/赤道.md "wikilink")，距[大西洋](../Page/大西洋.md "wikilink")506公里。市区地形较为平坦，海拔317米，周围是广阔的稀树草原。

布拉柴维尔聚集了全国三分之一的人口和全国40%的非农业人口。行政上是刚果（布）一个独立市镇，为[普尔区包围](../Page/普尔区.md "wikilink")。

## 历史

这一地区几个世纪以来都是非洲要地。在拉里人（）的语言中，姆福阿和姆皮拉地区被称为马武拉（Mavula），即“发财的地方”，这个名称至今仍为拉里人使用；但虽然这种语言的使用者数量占布拉柴维尔第一名，却并未在宪法中列为国语。不过，1880年时，拉里人已经不在这一地区居住，取而代之的是泰凯（[Téké](../Page/巴特克人.md "wikilink")）移民，他们称这里为恩库纳（Nkuna）。

布拉柴维尔始建于1880年9月10日，由[法籍](../Page/法国.md "wikilink")[意大利探险家](../Page/意大利.md "wikilink")[皮埃尔·萨沃尼昂·德·布拉柴建立](../Page/皮埃尔·萨沃尼昂·德·布拉柴.md "wikilink")；这一天，当地的马科科（Makoko）国王与之签署协议，将其王国置于[法国的保护之下](../Page/法国.md "wikilink")；四年后，为与河对岸比利时人兴建的利奥波德维尔（今金沙萨）竞争，城市开始兴建。1880年10月至1882年5月，这里由塞内加尔人马拉米纳·卡马拉（Malamine
Camara）率领的小部队占领，以避免比利时侵占。

法国对这一地区的正式占领始于1884年的[柏林会议](../Page/柏林会议.md "wikilink")，这里成为[法属刚果和后来的](../Page/法属刚果.md "wikilink")[法属赤道非洲](../Page/法属赤道非洲.md "wikilink")（包括[加蓬](../Page/加蓬.md "wikilink")、[中非和](../Page/中非.md "wikilink")[乍得的联邦](../Page/乍得.md "wikilink")）首都。1911年升格为市镇，1912年建立市政厅，1929年在时任市长安东内提（Antonetti）将军时期首次拥有了城市规划。1924年，[刚果大洋铁路投入使用](../Page/刚果大洋铁路.md "wikilink")，将布拉柴维尔与[黑角连接起来](../Page/黑角.md "wikilink")。

1940年，布拉柴维尔成为[自由法国的象征性首都](../Page/自由法国.md "wikilink")。1944年，[戴高乐的](../Page/戴高乐.md "wikilink")[自由法国与法属非洲殖民地代表在布拉柴维尔举行了一次会议](../Page/自由法国.md "wikilink")，这次会议决定重定二战后法国及其非洲殖民地的关系，黑非洲摆脱殖民统治的思想萌发。1944年和1958年，戴高乐在布拉柴发表讲话，成为法属黑非洲独立的前奏。

20世纪60年代之前，城市分为欧洲人区（市中心）和非洲人区（波多-波多、巴刚果和马凯莱凯莱）。1962年至1963年修建新市政厅。1980年，布拉柴维尔脱离[普尔区](../Page/普尔区.md "wikilink")，获得区级地位。

这座城市经历过多次刚果（布）国内、[刚果（金）和](../Page/刚果（金）.md "wikilink")[安哥拉之间的多次武装冲突](../Page/安哥拉.md "wikilink")。20世纪90年代的[刚果内战中](../Page/刚果共和国内战.md "wikilink")，这里发生了血腥大屠杀，数千人死亡，数十万人成为难民。

2012年3月4日發生[爆炸事件](../Page/2012年布拉薩維爾爆炸.md "wikilink")，事故造成至少200人死亡。

## 气候

布拉柴维尔位于[热带干湿季气候带](../Page/热带干湿季气候.md "wikilink")，雨季从10月至次年5月，其余时间为旱季。七、八月是最干旱的季节，无明显降水。全年温差不大。

## 行政区划

[Memorial_de_Brazza.jpg](https://zh.wikipedia.org/wiki/File:Memorial_de_Brazza.jpg "fig:Memorial_de_Brazza.jpg")
布拉柴维尔分为以下七个区：

### 马凯莱凯莱（Makélékélé）

布拉柴第一区，人口最多、面积最大，位于市区南部外围，朱埃河畔。现任区长毛里斯·毛雷尔·基文祖（Maurice Maurel
Kiwoundzou）。

[世界卫生组织非洲区域总部位于朱埃河畔](../Page/世界卫生组织.md "wikilink")。

### 巴刚果（Bacongo）

最早的区之一，拥有全市最大的市场——le Marché Total。该区街道以法国名人命名。三法郎大街（L'Avenue des 3
Francs）因被殖民者付给移殖民的贡金而得名，距今已有上百年历史。戴高乐曾于二战期间住在本区，其别墅“Case de
Gaulle”由罗歇·埃雷尔（Roger
Erell，1907-1986）设计，位于刚果河畔，现为法国驻刚果（布）大使官邸。区内还有罗塞尔圣母院等[教堂](../Page/教堂.md "wikilink")，均是法国-刚果风格建筑的精品。

巴刚果也是20世纪90年代众多冲突事件的中心，1993-1997年的刚果内战中经历严重破坏和屠杀，导致族群同质化。

现任区长为雷蒙·库巴（Raymond Kouba）。

### 波多-波多（Poto-Poto）

第三区，也是最老的区之一，建于1900年左右，原为沼泽和奥基拉村（Okila）。

刚果圣安娜大教堂建于1949年，位于1944年兴建的菲利克斯·埃布埃体育场附近，二者均为罗歇·埃雷尔设计，是城市象征性建筑。

现任区长雅克·埃利翁。

### 穆恩加利（Moungali）

第四区，商业气氛浓厚，聚集众多酒吧、迪厅。现任区长塞莱斯蒂娜·库阿库阿（Célestine Kouakoua）。

### 乌恩泽（Ouenzé）

第五区，区名来自[林加拉语](../Page/林加拉语.md "wikilink")，意为市场。现任区长马塞尔·加农哥（Marcel
Ganongo）。

### 塔朗加伊（Talangaï）

第六区，位于北郊，人口居全市第二。区名在林加拉语中意为“看我”（“tala”意为“看”，“ngaï”意为“我”）。区内混杂着危房和权贵的别墅。

现任区长普利瓦·恩德凯（Privat Ndeke）。

### 姆菲卢（Mfilou）

第七区，位于西北郊。[帕斯卡尔·利苏巴时期是其民兵控制区](../Page/帕斯卡尔·利苏巴.md "wikilink")。

现任区长阿尔贝尔·桑巴（Albert Samba）。

## 交通

[TaxisBrazzavilleByMarcel.jpg](https://zh.wikipedia.org/wiki/File:TaxisBrazzavilleByMarcel.jpg "fig:TaxisBrazzavilleByMarcel.jpg")
布拉柴是全国两条主干公路（布拉柴-[黑角](../Page/黑角.md "wikilink")、布拉柴-韦索）的起点，距刚果第二大城市[黑角的道路距离为](../Page/黑角.md "wikilink")512公里，也是[刚果大洋铁路终点站](../Page/刚果大洋铁路.md "wikilink")，设有火车站。这里也是重要的[河港](../Page/河港.md "wikilink")，拥有通往[金沙萨和途径](../Page/金沙萨.md "wikilink")[因普丰多至](../Page/因普丰多.md "wikilink")[班吉的渡船](../Page/班吉.md "wikilink")，但由于下游朱埃河汇入刚果河处有[李文斯頓瀑布](../Page/李文斯頓瀑布.md "wikilink")，因此无法航行至[大西洋](../Page/大西洋.md "wikilink")。

布拉柴拥有[马亚-马亚国际机场](../Page/马亚-马亚国际机场.md "wikilink")，机场[IATA代码为BZV](../Page/国际航空运输协会航空公司代码.md "wikilink")。目前开通至法国、肯尼亚、埃塞俄比亚、喀麦隆、贝宁、安哥拉、南非等地的定期航线。

市内主要交通方式为小客车和[出租车](../Page/出租车.md "wikilink")，车况参差不齐、普遍较差，大部分采用上白下绿的本市统一涂装。小客车由私人运营，无固定线路及班次，日本车占压倒多数，主要是[丰田考斯特和](../Page/丰田考斯特.md "wikilink")[丰田Hiace](../Page/丰田Hiace.md "wikilink")。出租车单程价格视距离在700到1500[中非法郎间浮动](../Page/中非法郎.md "wikilink")（约合1到2[欧元](../Page/欧元.md "wikilink")），无计价设备，也有拼客的出租车。出租车约占全市机动车数量的70%。市内不少道路路况较差。

## 教育

马里安·恩古瓦比大学是全国唯一一所大学。

## 排名

根据Mercer对215个城市的[生活质量调查](http://www.finfacts.ie/irishfinancenews/article_1016548.shtml)，2009年布拉柴维尔排名第211，仅高于[乍得](../Page/乍得.md "wikilink")[恩贾梅纳](../Page/恩贾梅纳.md "wikilink")、[中非共和国](../Page/中非共和国.md "wikilink")[班吉和](../Page/班吉.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")[巴格达](../Page/巴格达.md "wikilink")；2008年排名第213，2007年排名为214。

## 友好城市

  - [德国](../Page/德国.md "wikilink")[德累斯顿](../Page/德累斯顿.md "wikilink")

  - [美国](../Page/美国.md "wikilink")[奧拉西](../Page/奧拉西_\(堪萨斯州\).md "wikilink")

  - [美国](../Page/美国.md "wikilink")[华盛顿哥伦比亚特区](../Page/华盛顿哥伦比亚特区.md "wikilink")

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[威海](../Page/威海.md "wikilink")

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[长沙](../Page/长沙.md "wikilink")

## 注释

## 参考文献

### 引用

### 来源

  - G. Balandier, *Sociologie des Brazzavilles noires*, Fo. Nationale
    des Sciences Politiques, rééd. 1985;
  - R. Bazanguissa-Ganga, *Les Voies du politique au Congo*, Ed.
    Karthala, 1997;
  - G. Houlet, *Guide bleu Afrique centrale*, Ed. Hachette, 1962;
  - P. M. Martin, *Loisirs et société à Brazzaville pendant l'ère
    coloniale*, Ed. Karthala, 2005, 308 P.;
  - M. Petringa, *Brazza, A Life for Africa*, Ed. AuthorHouse, 2006.
  - M. Soret, *Histoire du Congo-Brazzaville*, Ed. Berger-Levrault,
    1978;
  - B. Toulier, *Brazzaville la Verte*, Images du Patrimoine n°62,
    Paris, 1996, 48 p.;
  - R. Frey, *Livre d'or du Centenaire de Brazzaville*, Brazzaville,
    1980, 354p.
  - Chavannes, Charles de.（1929）《Le Sergent Sénégalais Malamine》*Annales
    de l’Académie des Sciences Coloniales*，3:159-187卷
  - Petringa, Maria.（2006）《Brazza, A Life for Africa》（2006） ISBN
    978-1-4259-1198-0
  - Tiepolo, M.（1996）《Cities》中的《City Profile:
    Brazzaville》13卷，pp. 117–124
  - H. Brisset-Guibert, Hervé（2007）*Histoire de Brazzaville identité
    coloniale identité nationale*，Université de
    Poitiers，1988，257页；《Brazzaville petit guide
    historique》，网站www.presidence.cg（总统府）

## 外部链接

  - [BRAKIN, the fusion city of Brazzaville and
    Kinshasa](http://www.fusion-cities.par-darmstadt.de/cities/brakin/)，城市分析讨论和向量地图，TU
    Darmstadt，2009

  - [Maria Petringa's 1997 biographical article on Savorgnan de
    Brazza](https://web.archive.org/web/20061108125758/http://www.harvardmag.com/jf97/vita.html)，描述了肇造布拉柴维尔的事件

  - [IZF -
    布拉柴维尔地图](https://web.archive.org/web/20070928021601/http://www.izf.net/izf/documentation/cartes/centreVille/brazzaville.htm)

  -
{{-}}

[Brazzaville](../Category/刚果共和国城市.md "wikilink")
[Brazzaville](../Category/非洲首都.md "wikilink")

1.