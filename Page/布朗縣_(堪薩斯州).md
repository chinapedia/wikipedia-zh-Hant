**布朗县**（**Brown County,
Kansas**，簡稱**BR**）是位於[美國](../Page/美國.md "wikilink")[堪薩斯州東北部的一個縣](../Page/堪薩斯州.md "wikilink")，北界[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")。面積1,482平方公里。根據[2000年美國人口普查](../Page/2000年美國人口普查.md "wikilink")，共有人口10,724人。縣治[海華沙](../Page/海華沙_\(明尼蘇達州\).md "wikilink")
(Hiawatha)。

成立於1855年8月25日，縣名紀念代表[密西西比州的](../Page/密西西比州.md "wikilink")[眾議員](../Page/美國眾議院.md "wikilink")[阿爾伯·G·布朗](../Page/阿爾伯·G·布朗.md "wikilink")
(Albert G. Browne)。

[Brown](../Category/堪萨斯州行政区划.md "wikilink")
[布朗縣_(堪薩斯州)](../Category/布朗縣_\(堪薩斯州\).md "wikilink")