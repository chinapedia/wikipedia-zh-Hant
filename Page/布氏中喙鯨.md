**布氏中喙鯨**或**柏氏中喙鯨**（學名*Mesoplodon
densirostris*），旧称**布兰氏喙鲸**，也名**钝喙鲸**，**大西洋喙鲸**，**热带喙鲸**和**瘤齿喙鲸**\[1\]，似乎是中喙鯨屬中分布最廣泛的[物種](../Page/物種.md "wikilink")，也是本屬唯一已有確實海中觀測研究資料者。已知經常傳出目擊記錄的地點至少有三處：[夏威夷](../Page/夏威夷.md "wikilink")[歐胡島外海](../Page/歐胡島.md "wikilink")、南[太平洋](../Page/太平洋.md "wikilink")[社會群島外海](../Page/社會群島.md "wikilink")、與[巴哈馬東北部](../Page/巴哈馬.md "wikilink")。其種名源自[拉丁文的](../Page/拉丁文.md "wikilink")**''densus***（意為「**厚**」或「**密**」）與***rostrum**''（意為「**嘴喙**」）。

## 基本資料：

**其他俗名：**

  - 英：**Dense Beaked Whale、Atlantic Beaked Whale、Tropical Beaked Whale**
  - 法：***Baleine à bec de Blainville***
  - 西：***BALLENA DE PICO DE BLAINVILLE、ZIFIO DE BLAINVILLE***

**出生時身長體重：**2m、60kg

**最大身長體重紀錄：**雄-4.4m、800kg以上；雌-4.6m、至少1,000kg

**壽命：**未知

## 外型特徵

柏氏中喙鯨的體型寬厚而粗壯，背鰭小，呈[三角形或鐮刀形](../Page/三角形.md "wikilink")，大約位於身長2/3處。嘴喙長度中等，[額隆外觀上較小而扁平](../Page/額隆.md "wikilink")。嘴部曲線特殊，先是水平往後延伸，至中段急劇高起呈一圓弧狀。成年雄鯨在下顎隆起處有2顆大型[牙齒](../Page/牙齒.md "wikilink")，末端略微突出，會往前傾斜越過上顎。牙齒上常附著有成串的[鵝頸藤壺](../Page/鵝頸藤壺.md "wikilink")。

背部與身體側面皆為深藍灰色，至腹部轉為淺灰色。頭部可能呈淺褐色，上脣與下顎邊緣為淺灰色。牠們身上常有許多橢圓形傷痕，特別是在[生殖器附近](../Page/生殖器.md "wikilink")，可能是遭[雪茄鮫咬傷](../Page/雪茄鮫.md "wikilink")。成年雄鯨身上常有白色長條擦傷痕跡。

## 分布

柏氏中喙鯨廣泛分布於各大洋[熱帶與暖](../Page/熱帶.md "wikilink")[溫帶海域](../Page/溫帶.md "wikilink")，偶爾會進入高[緯度海域](../Page/緯度.md "wikilink")，原因可能與[暖流有關](../Page/暖流.md "wikilink")，例如北[大西洋的](../Page/大西洋.md "wikilink")[墨西哥灣流](../Page/墨西哥灣流.md "wikilink")（Gulf
Stream）與[非洲南部的](../Page/非洲.md "wikilink")[厄加勒斯洋流](../Page/厄加勒斯洋流.md "wikilink")（Agulhas
Current）。已知的擱淺記錄有：

  - 北大西洋：[加拿大的](../Page/加拿大.md "wikilink")[諾瓦斯科西亞](../Page/諾瓦斯科西亞.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[英國](../Page/英國.md "wikilink")、[美國東岸](../Page/美國.md "wikilink")、[巴哈馬](../Page/巴哈馬.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[法國](../Page/法國.md "wikilink")。
  - 北[太平洋](../Page/太平洋.md "wikilink")：[夏威夷](../Page/夏威夷.md "wikilink")、[日本](../Page/日本.md "wikilink")、[加州南部](../Page/加州.md "wikilink")、[台灣](../Page/台灣.md "wikilink")。
  - 南大西洋：[巴西](../Page/巴西.md "wikilink")、[南非](../Page/南非.md "wikilink")。
  - 南太平洋：[智利中部](../Page/智利.md "wikilink")、[澳洲的](../Page/澳洲.md "wikilink")[塔斯馬尼亞州](../Page/塔斯馬尼亞州.md "wikilink")、[紐西蘭北部](../Page/紐西蘭.md "wikilink")。

尚無證據顯示牠們有季節性的移動或遷徙。柏氏中喙鯨經常在500至1,000[公尺深的大陸坡或深層](../Page/公尺.md "wikilink")[海溝出現](../Page/海溝.md "wikilink")。

## 習性、生殖、食性

柏氏中喙鯨多以3至7頭的小群體出現。通常會緩慢浮至海面，先是嘴喙突出水面，接著出現不明顯的噴氣，以15-20秒的間隔浮出水面數次後，會進行20-45分鐘的深潛。牠們主要以[魷魚和小型](../Page/魷魚.md "wikilink")[魚類為食](../Page/魚類.md "wikilink")。對其生殖狀況了解甚少。

## 現狀

柏氏中喙鯨偶爾會被捕殺，但從未成為主要捕鯨對象。2000年3月在巴哈馬有數頭個體擱淺，確實原因不明，由於該海域不久前曾舉行過[海軍演習](../Page/海軍.md "wikilink")，部分學者認為可能與聽覺系統受損有關。

## 保护

  - [中国国家重点保护动物等级](../Page/中國國家重點保護野生動物名錄.md "wikilink")：二级

## 注释

## 參考書目

  - *Pieter A. Folken, Randall R. Reeves, etc. / illustrated by Pieter
    A. Folkens，《Guide to MARINE MAMMALS of the World》，Alfred A. Knopf,
    2002: p294-295. ISBN 0-375-41141-0*
  - *Mark Carwardine / illustrated by Martin CammDorling，《DORLING
    KINDERSLEY HANDBOOKS: WHALES, DOLPHINS AND PORPOISES》，Dorling
    Kindersley, 1995: p120-121. ISBN 0-7513-2781-6*
  - ''James G. Mead，《Beaked Whales of the Genus *Mesoplodon*》，edited by
    Sam H. Ridgway and Sir Richard Harrison, F.R.S. 《Handbook of Marine
    Mammals, Volume 4: River Dolphins and the Larger Toothed
    Whales》，Academic Press, 1989: p349-430. ISBN 0-12-588504-0''

[Category:喙鲸属](../Category/喙鲸属.md "wikilink")

1.