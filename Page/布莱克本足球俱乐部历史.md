**[-{zh-hans:布莱克本足球俱乐部;
zh-hant:布力般流浪足球會;}-](../Page/布莱克本足球俱乐部.md "wikilink")**（**Blackburn
Rovers Football
Club**）是一支位於蘭開郡的[布莱克本市的](../Page/布莱克本.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")-{zh-hans:足球俱乐部;
zh-hant:足球會;}-。成立於1875年，是1888年足球聯盟成立的創始成員。自1890年開始以[伊活公園球場為主場](../Page/伊活公園球場.md "wikilink")。球會格言
*"Arte et labore"* 解作 "利用技巧與勞力"。

## 早年時期

### 創立球會

[Leaflet_advert_for_blackburn_rovers_match-1887.jpg](https://zh.wikipedia.org/wiki/File:Leaflet_advert_for_blackburn_rovers_match-1887.jpg "fig:Leaflet_advert_for_blackburn_rovers_match-1887.jpg")的宣傳單張\]\]
布莱克本於1875年11月5日由兩位[舒茲伯利官立學校](../Page/Shrewsbury_School.md "wikilink")（Shrewsbury
School）舊生[約翰劉易士](../Page/John_Lewis.md "wikilink")（John
Lewis）及[亞瑟康士坦丁](../Page/Arthur_Constantine.md "wikilink")（Arthur
Constantine）倡議組成。由於部份始創成員非富則貴，而且人脈網絡廣闊，使布力般流浪能從同時期成立的多支球隊中脫穎而出。

當時布力般流浪沒有比賽球場及門票收入，唯一的收入來自會員的捐款，首季共收到2[鎊](../Page/英鎊.md "wikilink")8[先令](../Page/先令.md "wikilink")。布力般流浪在1876-77年的賽季在市西部[奧士赫特](../Page/Oozehead.md "wikilink")（Oozehead）租用一片農地成為主場，在比賽前還需用木板和草皮舖平球場中央的小池塘才能比賽，但布力般流浪可從中收取門票，整季收入共6先令6[便士](../Page/便士.md "wikilink")！有部份比賽會移師[派盛頓木球場](../Page/Pleasington_Cricket_Ground.md "wikilink")（Pleasington
Cricket
Ground）舉行。最終布力般流浪租用[東蘭開郡木球會](../Page/East_Lancashire_Cricket_Club.md "wikilink")（East
Lancashire Cricket
Club）的[亞歷山卓美度士](../Page/Alexandra_Meadows.md "wikilink")（Alexandra
Meadows）為正式主場。首場比賽對當時薄有名望的[柏德烈菲士圖](../Page/Partick_Thistle.md "wikilink")（Partick
Thistle），布力般以2-1獲勝。1878年9月28日[蘭開郡足球聯會](../Page/Lancashire_Football_Association.md "wikilink")（Lancashire
Football
Association）成立，布力般成為23支始創成員球會之一。翌年11月1日首次參戰[足總杯](../Page/英格兰足总杯.md "wikilink")，以5-1大破[泰恩](../Page/Tyne_Association_F.C..md "wikilink")（Tyne），但最後在第三圈被[-{zh-hans:诺丁汉森林;
zh-hant:諾定咸森林;}-以](../Page/诺丁汉森林足球俱乐部.md "wikilink")6-0大敗而遭淘汰。

1880年由於布力般流浪在會員缺席比賽時採用非布力般本地的球員作為替補，違反蘭開郡足球聯會的原則而引發連串的爭議。在1881年賽季開始時，一名球員[費治蘇達](../Page/Fergie_Suter.md "wikilink")（Fergie
Suter）從蘭開郡足球聯會之一的[達爾文](../Page/Darwen.md "wikilink")（Darwen）轉投布力般流浪時引起了球員職業化的軒然大波，達爾文指控布力般流浪提供厚酬誘使蘇達轉會，但蘇達本身亦是放棄石匠本業從[蘇格蘭加入達爾文的](../Page/蘇格蘭.md "wikilink")，業餘與職業球員的分界模糊不清。自始兩支球隊的比賽場內場外都是糾纏不清，不巧地兩隊在[蘭開郡杯](../Page/Lancashire_Cup.md "wikilink")（Lancashire
Cup）第四圈對壘，兩隊拒絕同意訂定比賽日期，蘭開郡足球聯會一怒之下，將兩隊同時逐出此項杯賽。類似的爭議直到五年後（1885年）通過職業球員法例才停止。

在1881-82年賽季布力般流浪繼續租用亞歷山卓美度士作為主場，但作為當地領導的球隊實在需要擁有自己的球場布力般流浪在[連名頓街](../Page/Leamington_Street.md "wikilink")（Leamington
Street）租用一塊土地，花費500鎊修建可容600至700觀眾的看台，入口豎立一度有球會名字的大拱門布力般流浪終於擁有自己的球場。

### 冠軍球隊

1882年3月25日布力般流浪成功殺入[足總杯決賽對](../Page/足總杯.md "wikilink")[伊東學院舊生隊](../Page/Old_Etonians.md "wikilink")（Old
Etonians），成為首支地區球隊晉身決賽，但不幸以0-1落敗而回。翌季（1882-83年）更在第二圈以0-1敗於死對頭達爾文被淘汰，最後由本地敵對球隊[-{zh-hans:布莱克本奥林匹克;
zh-hant:布力般奧林匹克;}-](../Page/Blackburn_Olympic.md "wikilink")（Blackburn
Olympic）贏得足總杯成為首支奪冠的地區球隊。布力般流浪在1884年3月29日終嘗足總杯冠軍的滋味，於[堅靈頓橢圓球場](../Page/Kennington_Oval.md "wikilink")（Kennington
Oval）以2-1擊敗[蘇格蘭球隊](../Page/蘇格蘭.md "wikilink")[格拉斯哥昆士柏](../Page/Queen's_Park.md "wikilink")（Queen's
Park）。兩支球隊再在翌季一同晉身決賽，布力般流浪以2-0成功衛冕。再接再厲，在1886年的決賽以2-0擊敗[-{zh-hans:西布罗姆维奇;
zh-hant:西布朗;}-](../Page/西布罗姆维奇足球俱乐部.md "wikilink")，布力般流浪吃出大三元。此次史無前例的足總杯三連冠，獲授予銀盾作紀念。

1888年3月2日[-{zh-hans:阿斯顿维拉;
zh-hant:阿士東維拉;}-的委員](../Page/阿士東維拉足球俱樂部.md "wikilink")[威廉麥奎格](../Page/William_McGregor.md "wikilink")（William
McGregor）發信給包括布力般流浪在內的5間球會，建議以12支頂級球隊組織包括主客制的足球聯賽。1888年3月22日在[倫敦的](../Page/倫敦.md "wikilink")[安達頓酒店](../Page/Anderton_Hotel.md "wikilink")（Anderton
Hotel）舉行會議，布力般流浪的[約翰畢華圖](../Page/John_Birtwistle.md "wikilink")（John
Birtwistle）代表列席，會議導致[足球聯盟](../Page/英格兰足球联赛.md "wikilink")（Football
League）的成立，而布力般流浪成為創始成員之一。布力般流浪在首屆聯賽排名第四位，主場保持不敗。1890年3月29日，布力般流浪第四度奪得足總杯冠軍，在堅靈頓橢圓球場以6-1懸殊的比數大破[-{zh-hans:谢周三;
zh-hant:錫周三;}-](../Page/谢周三足球俱乐部.md "wikilink")。

### 伊活公園

[伊活公園](../Page/:en:Ewood_Park.md "wikilink")（Ewood
Park）由當地四個商人發起在1882年修建而成，用作舉辦一般運動項目。布力般流浪在1890年收購伊活公園，再花多1,000鎊增加設施使其達到足球比賽標準，自始伊活公園便成布力般流浪的主場，共同渡過超過一世紀（或許更長久）的歲月。伊活公園球場首場比賽在1890年9月13日對[艾靈頓](../Page/Accrington.md "wikilink")，踢成0-0和局收場。1890-91年賽季布力般流浪以3-1擊敗[-{zh-hans:诺茨郡;
zh-hant:諾士郡;}-第五度贏得足總杯冠軍](../Page/諾士郡足球會.md "wikilink")，同時亦標誌著球會轉向衰退，一段長久的貧乏時期緊接而來。1896-97年賽季原本需要降級的布力般流浪因聯賽增加隊數而得以保留甲組席位，本季見證[卜金普頓](../Page/Bob_Crompton.md "wikilink")（Bob
Crompton）的加盟，此後50年金普頓分別以球員及領隊身份與球會緊密連繫。十九世紀的最末數年並無為布力般流浪帶來榮譽，反而有數次僥倖的逃離降級險境。

## 廿世紀初及中期

### 兩次大戰

1912年，布力般流浪才首次取得聯賽冠軍，並於兩年（1914年）後再獲殊榮，隨後一季布力般流浪獲得季軍，而[世界大戰亦告展開](../Page/第一次世界大戰.md "wikilink")。戰後聯賽在1919年重開，布力般流浪未能找到戰前偉大球員的替補，只能在榜尾掙扎。在1922年2月布力般流浪委任[積奇卡爾](../Page/Jack_Carr.md "wikilink")（Jack
Carr）成為首位全職領隊，但成績仍無改善。

直到在前布力般流浪及英格蘭國家隊名將金普頓帶領下，戰績逐年提升，其首三年聯賽排名分別是第十二位、第七位及第六位。布力般於1928年在[足總杯先後淘汰](../Page/英格兰足总杯.md "wikilink")[-{zh-hans:纽卡斯尔联;
zh-hant:紐卡素;}-](../Page/纽卡斯尔联足球俱乐部.md "wikilink")、[埃克塞特城及](../Page/埃克塞特城足球俱乐部.md "wikilink")[-{zh-hans:韦尔港;
zh-hant:維爾港;}-而晉身半準決賽主場對](../Page/韦尔港足球俱乐部.md "wikilink")[曼聯](../Page/曼聯.md "wikilink")，以2-0獲勝，下一輪為作客對[-{zh-hans:阿森纳;
zh-hant:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink")，布力般流浪被普遍看淡，故只有3,000名球迷南下支持，布力般流浪爆冷以1-0取勝殺入決賽，對手為被譽為當時[英格蘭最佳球隊的](../Page/英格蘭.md "wikilink")[哈德斯菲爾德](../Page/哈德斯菲尔德足球俱乐部.md "wikilink")，但布力般流浪並沒有被嚇倒，在決賽以3-1取勝，第六度奪得足總杯冠軍。此後四十年間布力般流浪一直在甲乙組間浮沉。

金普頓在1931年2月因球員反抗其嚴厲管治而離開連續服務了34年的布力般流浪。他們在1936年終於降級乙組。布力般流浪在乙組的生涯並不好過，大將星散後，再降一級的機會比回升甲組更大。布力般流浪迫得在1938年4月2日重召金普頓為領隊，在協助球隊成功護級後，金普頓重整球隊，補充數名球員後於1938-39年賽季壓倒[-{zh-hans:谢菲尔德联;
zh-hant:錫菲聯;}-及](../Page/谢菲尔德联足球俱乐部.md "wikilink")[-{zh-hans:谢周三;
zh-hant:錫周三;}-奪得冠軍回升甲組](../Page/谢周三足球俱乐部.md "wikilink")。在甲組只比賽了三輪便因[二次大戰展開而停頓](../Page/第二次世界大戰.md "wikilink")。大戰期間金普頓在督導一場對[-{zh-hans:伯恩利;
zh-hant:般尼;}-的比賽後數小時暈倒去世](../Page/伯恩利足球俱乐部.md "wikilink")，結束其與布力般流浪不可分割的一生，被尊稱為「布力般流浪先生」（*Mr
Blackburn*）。

### 戰後衰落

[第二次大戰後足球聯賽在](../Page/第二次世界大戰.md "wikilink")1946-47年重開，前[-{zh-hans:阿森纳;
zh-hant:阿仙奴;}-名將](../Page/阿森纳足球俱乐部.md "wikilink")[艾迪夏葛特](../Page/Eddie_Hapgood.md "wikilink")（Eddie
Hapgood）獲委任為布力般流浪領隊。與[第一次大戰後的境況不同](../Page/第一次世界大戰.md "wikilink")，布力般流浪不再是富有及具影響力的球隊，財政緊絀令球隊重建工作倍感困難，現時的球隊由1939年升級功臣和一群在戰時出道的年青球員組成。雖然開季時四戰三勝，但布力般流浪的成績逐漸下滑，在[聖誕節前後護級形勢吃緊](../Page/聖誕節.md "wikilink")，布力般流浪作出史無前例的瘋狂大收購：共用2萬6仟鎊購入[澤克華亞](../Page/Jock_Weir.md "wikilink")（Jock
Weir）、[積克奧基斯](../Page/Jack_Oakes.md "wikilink")（Jack
Oakes）及[法蘭斯麥哥尼根](../Page/Francis_McGorrighan.md "wikilink")（Francis
McGorrighan）。夏葛特經常否認所收購的球員是他的選擇，但新球員仍發揮實力協助布力般流浪成功脫離降級危機。董事局與夏葛特的緊張關係最終令到夏葛特在1947年2月19日辭職。繼任人[韋爾史葛](../Page/Will_Scott.md "wikilink")（Will
Scott）因健康問題而迅即被[積奇布頓](../Page/Jack_Bruton.md "wikilink")（Jack
Bruton）取代。1946-47年賽季成績低下但翌年更加惡劣，球隊不斷變動及欠缺經驗，季末10輪比賽只能取得1勝，布力般流浪無可避免地降級乙組。在最後一輪比賽，布力般流浪派出一位年青球員處子登場：布力般流浪歷來最大的球員之一的[比爾艾卡斯尼](../Page/Bill_Eckersley.md "wikilink")（Bill
Eckersley）。

在乙組的頭兩季布力般流浪只列於中游，在1950-51年成績改善為第六位。翌年布力般流浪在[足總杯有不俗的表現](../Page/英格兰足总杯.md "wikilink")，在半準決賽主場3-1擊敗[-{zh-hans:伯恩利;
zh-hant:般尼;}-](../Page/伯恩利足球俱乐部.md "wikilink")，在-{zh-hans:半决赛;
zh-hant:準決賽;}-挑戰[-{zh-hans:纽卡斯尔联;
zh-hant:紐卡素;}-](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，在[希斯堡球場悶和](../Page/Hillsborough.md "wikilink")0-0後，重賽在完場前五分鐘，布力般流浪被球證判罰一個有爭議的-{zh-hans:点球;
zh-hant:十二碼;}-而以1-2飲恨出局。前[曼聯名將](../Page/曼聯.md "wikilink")[尊尼卡尼](../Page/Johnny_Carey.md "wikilink")（Johnny
Carey）在1953年被任命為領隊，是布力般流浪「黃金歲月」來臨的預告。雖然曾作出數項精明的收購如[法蘭慕尼](../Page/Frank_Mooney.md "wikilink")（Frank
Mooney）及購回[波比蘭頓](../Page/Bobby_Langton.md "wikilink")（Bobby
Langton）等，但卡尼最大的本事是逼出球員的潛能，令到球員大幅改善。卡尼強調進攻足球，旗下球員[湯美碧斯](../Page/Tommy_Briggs.md "wikilink")（Tommy
Briggs）連續四季分別射入32、33、30及32球。布力般流浪連續兩年只能排第三及第六位而無緣升級，球迷蜂擁而至觀看卡尼的布力般流浪比賽，在低沉了長時間後，布力般流浪的前境一片美好！

### 黃金歲月

再連續兩年名列第四位而與升級擦身而過，年青隊員如[朗尼克萊頓](../Page/Ronnie_Clayton.md "wikilink")（Ronnie
Clayton）、[拜恩杜勒斯](../Page/Bryan_Douglas.md "wikilink")（Bryan
Douglas）、[萊維農](../Page/Roy_Vernon.md "wikilink")（Roy
Vernon）及[彼得杜冰](../Page/Peter_Dobing.md "wikilink")（Peter
Dobing）表現日漸成熟，加以購入的球員如[麥活士](../Page/Matt_Woods.md "wikilink")（Matt
Woods）及[艾利麥李歐德](../Page/Ally_McLeod.md "wikilink")（Ally
McLeod）等令到布力般流浪實力逐漸加強。在1957-58年賽季開季表現慢熱，但從九月尾開始一直挑戰升級席位。在[足總杯方面](../Page/英格兰足总杯.md "wikilink")，布力般流浪接連淘汰[-{zh-hans:埃弗顿;
zh-hant:愛華頓;}-及](../Page/埃弗顿足球俱乐部.md "wikilink")[利物浦而殺入準決賽](../Page/利物浦足球俱乐部.md "wikilink")，在[緬因路球場對](../Page/緬因路球場.md "wikilink")[-{zh-hans:博尔顿;
zh-hant:保頓;}-](../Page/博尔顿足球俱乐部.md "wikilink")，不幸以1-2落敗出局。布力般流浪收拾心情全力爭取升級，四月取得5連勝，在最後一輪比賽壓過[-{zh-hans:查尔顿竞技;
zh-hant:查爾頓;}-獲得亞軍而與](../Page/查尔顿竞技足球俱乐部.md "wikilink")[-{zh-hans:西汉姆联;
zh-hant:韋斯咸;}-雙雙攜手升級甲組](../Page/西汉姆联足球俱乐部.md "wikilink")。

升級首季（1958-59年）穩守中游，但翌季（1959-60年）只能剛好壓著[-{zh-hans:利兹联;
zh-hant:列斯聯;}-列第十七位避免降級](../Page/利兹联足球俱乐部.md "wikilink")。但布力般流浪在[足總杯有截然不同的表現](../Page/英格兰足总杯.md "wikilink")，首兩圈分別淘汰[-{zh-hans:桑德兰;
zh-hant:新特蘭;}-及](../Page/桑德兰足球俱乐部.md "wikilink")[黑池](../Page/黑池足球俱乐部.md "wikilink")，第五圈作客對[-{zh-hans:托特纳姆热刺;
zh-hant:熱刺;}-以](../Page/托特纳姆足球俱乐部.md "wikilink")3-1勝出。第六圈需作客對鄰近死敵[伯恩利](../Page/伯恩利足球俱乐部.md "wikilink")，當般尼以3-0遙遙領先時，[足總杯的路看似走到盡頭](../Page/英格兰足总杯.md "wikilink")，此時布力般流浪獲得一個具爭議的-{zh-hans:点球;
zh-hant:十二碼;}-，由杜勒斯射入，1分鐘之後杜冰再入一球追成2-3接近的比數，布力般流浪傾力反撲，終在完場前4分鐘由[米克麥格夫](../Page/Mick_McGrath.md "wikilink")（Mick
McGrath）追成平手。重賽回到伊活公園主場以2-0輕鬆獲勝晉級。-{zh-hans:半决赛;
zh-hant:準決賽;}-再一次在[緬因路球場上演](../Page/緬因路球場.md "wikilink")，共有7萬4仟觀眾見證布力般流浪憑[戴力杜根](../Page/Derek_Dougan.md "wikilink")（Derek
Dougan）的兩個入球擊敗[-{zh-hans:谢周三;
zh-hant:錫周三;}-](../Page/谢周三足球俱乐部.md "wikilink")，在缺席三十年後，再次晉身[足總杯決賽](../Page/英格兰足总杯.md "wikilink")，對手為強大的[狼隊](../Page/伍尔弗汉普顿流浪足球俱乐部.md "wikilink")。但這場決賽布力般流浪卻被場外的紛爭所困擾：布力般流浪的忠實球迷購買不到決賽門票，傳言有球員將配票在黑市出售；杜根在決賽前夕提出轉會要求等……布力般流浪整隊籠罩在不安的情緒內。1960年5月1日的決賽場上布力般流浪大失水準，在41分鐘被[巴利史杜拔](../Page/Barry_Stobart.md "wikilink")（Barry
Stobart）一個幸運入球先開紀錄，兩分鐘後右後衛[戴夫韋倫](../Page/Dave_Whelan.md "wikilink")（Dave
Whelan，現時[-{zh-hans:维甘竞技;
zh-hant:韋根;}-的班主](../Page/维甘竞技足球俱乐部.md "wikilink")）斷腳離場，由於當年並沒有替補球員的賽例，餘下時間布力般流浪只能以十人應戰，下半場[諾曼迪尼](../Page/Norman_Deeley.md "wikilink")（Norman
Deeley）再添兩球，布力般流浪以0-3大敗而回。

[積奇馬素爾](../Page/Jack_Marshall.md "wikilink")（Jack
Marshall）在1960年6月接任領隊，1960-61年賽季布力般流浪聯賽排第八位。除著球員薪金上限在1961年季末解除，布力般流浪一類的「城鎮級」（*town*）球隊發覺越來越難與一些「城市級」（*city*）大球會在薪金上競爭，雖然大部份球員接納球會的薪酬條件，但杜冰與杜根因談不攏而離隊他投。然而新人相繼湧現，在1962-63賽季，由後衛改踢中鋒的[費特畢克林](../Page/Fred_Pickering.md "wikilink")（Fred
Pickering）在36場比賽射入23球，[米克英倫](../Page/Mike_England.md "wikilink")（Mike
England）亦取代活士成為正選，再加從面臨倒閉的[艾靈頓史丹尼](../Page/Accrington_Stanley.md "wikilink")「拯救」回來的[米克費格遜](../Page/Mike_Ferguson.md "wikilink")（Mike
Ferguson）。布力般流浪維持在甲組上游多年，1962-63年十一位、1963-64年第七位及1964-65年第十位。

## 70及80年代: 更多挫敗

### 降級掙扎

1965-66年球季由於布力般市內爆發[小兒麻痺症](../Page/小兒麻痺症.md "wikilink")，布力般流浪比其他球隊較遲展開球季，一球未踢已排於榜尾。傷患和缺態導致不穩定的演出，整季只取得八場勝仗，順理成章於季末降級乙組。部份球員在球隊降級後離隊：米克英倫以9萬5仟鎊轉投[-{zh-hans:托特纳姆热刺;
zh-hant:熱刺;}-](../Page/托特纳姆足球俱乐部.md "wikilink")，[約翰拜隆](../Page/John_Byrom.md "wikilink")（John
Byrom）投奔[-{zh-hans:博尔顿;
zh-hant:保頓;}-](../Page/博尔顿足球俱乐部.md "wikilink")，而[費特艾爾斯](../Page/Fred_Else.md "wikilink")（Fred
Else）則自由轉會。只從[卡迪夫城以](../Page/卡迪夫城足球俱乐部.md "wikilink")4萬鎊購入[威爾斯國腳](../Page/威爾斯足球代表隊.md "wikilink")[巴利豪爾](../Page/Barrie_Hole.md "wikilink")（Barrie
Hole）補充實力。在乙組首季（1966-67年）開季三連勝而成為升級熱門，布力般流浪再簽入[約翰康納利](../Page/John_Connelly.md "wikilink")（John
Connelly）增強實力。前布力般流浪球員[艾迪奎爾尼](../Page/Eddie_Quigley.md "wikilink")（Eddie
Quigley）在1967年2月1日接手任教。不幸在[復活節的週末主場敗於](../Page/復活節.md "wikilink")[-{zh-hans:考文垂;
zh-hant:高雲地利;}-而宣佈無緣升級](../Page/考文垂足球俱乐部.md "wikilink")，最終排名不錯的第四位。在1967-68年及1969-70年賽季布力般流浪只排在不過不失的第八位。但在這兩季之間的1968-69年賽季布力般流浪成績急跌至第十九位，當季杜勒斯及克萊頓同告退役，費格遜及豪爾他投。在1969年一月布力般流浪急忙重召卡尼回巢，先將財務困難放在一傍，在1969-70年球季引進多位新血，共用了高達10萬鎊購入[堅禮頓](../Page/Ken_Knighton.md "wikilink")（Ken
Knighton）、[阿倫亨特](../Page/Alan_Hunter.md "wikilink")（Alan
Hunter）及[白賴仁希路](../Page/Brian_Hill.md "wikilink")（Brian
Hill），但將[基夫紐頓](../Page/Keith_Newton.md "wikilink")（Keith
Newton）賣給[-{zh-hans:埃弗顿;
zh-hant:愛華頓;}-以舒緩財政壓力](../Page/埃弗顿足球俱乐部.md "wikilink")。在1970-71年賽季布力般流浪以破球會紀錄的6萬鎊購入[占美卡亞](../Page/Jimmy_Kerr.md "wikilink")（Jimmy
Kerr），但只作賽11場便不幸因傷退役。由於嚴重財赤，迫得以6萬鎊將禮頓賣給[-{zh-hans:赫尔城;
zh-hant:侯城;}-](../Page/赫尔城足球俱乐部.md "wikilink")。布力般流浪成績下滑，最終在季末靜靜地降下丙組。

在丙組初期由[堅科菲](../Page/Ken_Furphy.md "wikilink")（Ken
Furphy）帶領，分別取得第十位及翌年差點升級的第三位，科菲在1973年尾離任轉投[-{zh-hans:谢菲尔德联;
zh-hant:錫菲聯;}-](../Page/谢菲尔德联足球俱乐部.md "wikilink")。[哥頓李爾](../Page/Gordon_Lee.md "wikilink")（Gordon
Lee）在1974年一月接任，當季（1973-74年）只能排在第十八位。李爾在1974年季後補充數名球員，在1974-75年賽季開季首戰以2比1作客擊敗[-{zh-hans:格林斯比镇;
zh-hant:甘士比;}-](../Page/格林斯比镇足球俱乐部.md "wikilink")，在榜首帶領至新年才作客1-2負於[普利茅斯而被後者取代領頭羊的位置](../Page/普利茅斯足球俱乐部.md "wikilink")。兩週後布力般流浪主場再戰普利茅夫，早段已以0-2落後兼射失一球-{zh-hans:点球;
zh-hant:十二碼;}-，布力般流浪並沒有氣餒，半場前攀回一球，下半場布力般流浪攻勢浪接浪，回敬四球成5-2完場。在最後一輪比賽主場對[-{zh-hans:雷克斯汉姆;
zh-hant:域斯咸;}-只要不輸多過五球便能完成霸業](../Page/雷克斯汉姆足球俱乐部.md "wikilink")，結果0-0，共2萬1仟球迷在場慶祝回升乙組。

### 禿鷹空降

有「禿鷹」之稱的[占史勿夫](../Page/Jim_Smith.md "wikilink")（Jim
Smith）在1975-76年賽季接手剛回升乙組的布力般流浪，經過兩季的磨合在1977-78年賽季有力挑戰升級，但表現不穩定成致命傷，在季末的八輪比賽只取得三分，最後只能排第五位，史勿夫亦決定轉投[-{zh-hans:伯明翰城;
zh-hant:伯明翰城;}-爭取更隹的前途](../Page/伯明翰足球俱乐部.md "wikilink")。布力般流浪在翌季更換了兩位領隊亦末能阻止降級的厄運。布力般流浪任命[侯活簡度](../Page/Howard_Kendall.md "wikilink")（Howard
Kendall）為首位領隊兼球員，在1979-80賽季經季初的慢熱表現後，自1月12日以2-1戰勝[-{zh-hans:格林斯比镇;
zh-hant:甘士比;}-起](../Page/格林斯比镇足球俱乐部.md "wikilink")，在接著的十四輪比賽取得十三勝的輝煌戰果，最終取得第二位回升乙組。自1980年獲得丙組聯賽亞軍後，布力般流浪一直保持在[英格蘭聯賽的上層組別](../Page/英格兰足球联赛.md "wikilink")。

在1980年夏季簡度轉投[-{zh-hans:埃弗顿;
zh-hant:愛華頓;}-](../Page/埃弗顿足球俱乐部.md "wikilink")，布力般流浪以[波比撒士頓](../Page/Bobby_Saxton.md "wikilink")（Bobby
Saxton）取代其領隊職務，但由於財政緊絀，未能作出重大收購增強實力，經兩季穩守中游後，1983-84年賽季提升至第六位。1984-85年賽季布力般流浪在[聖誕節前排在榜首](../Page/聖誕節.md "wikilink")，一如以往，球隊狀態在下半季急跌而失去升級機會。撒士頓堅拒引進新血的態度獲得董事局的支持，在1985-86年這支年華老去的球隊只能在最後一輪比賽以3-1擊敗[-{zh-hans:格林斯比镇;
zh-hant:甘士比;}-才避免降級](../Page/格林斯比镇足球俱乐部.md "wikilink")。到1986-87年賽季，布力般流浪在聖誕節時排於榜末，董事局才老大不願意地中止撒士頓的職務。暫代領隊[東尼柏堅斯](../Page/Tony_Parkes.md "wikilink")（Tony
Parkes）將球員狀態提升令新任領隊[唐麥基](../Page/Don_Mackay.md "wikilink")（Don
Mackay）接手後更易改善球隊的戰績，最終取得第十二位。柏堅斯在往後的日子亦曾多次在危急關頭扭轉形勢，扮演「雷霆救兵」的角色。在同年布力般流浪在[足總會員杯有令人驚喜的演出](../Page/Full_Members'_Cup.md "wikilink")，先在-{zh-hans:半决赛;
zh-hant:準決賽;}-淘汰[-{zh-hans:切尔西;
zh-hant:車路士;}-](../Page/切尔西足球俱乐部.md "wikilink")，進而在溫布萊的決賽憑[亨特利的入球以](../Page/Colin_Hendry.md "wikilink")1-0殺敗[-{zh-hans:查尔顿竞技;
zh-hant:查爾頓;}-奪得冠軍](../Page/查尔顿竞技足球俱乐部.md "wikilink")。

麥基的首個完整球季（1987-88年）以第五位作結，而麥基的新收購如[阿治波特](../Page/Steve_Archibald.md "wikilink")（Steve
Archibald）、[雅迪尼斯](../Page/Ossie_Ardiles.md "wikilink")（Ossie
Ardiles）及[法蘭史塔保頓](../Page/Frank_Stapleton.md "wikilink")（Frank
Stapleton）等亦改善了球會的形象及重燃球迷的希望。在1988-89年賽季布力般流浪獲得升級的機會，晉身最後一年採用主客兩回合賽制的升級附加賽決賽，但負於[水晶宮而無緣升級](../Page/水晶宫足球俱乐部.md "wikilink")，翌年(1989-90年)再在附加賽-{zh-hans:半决赛;
zh-hant:準決賽;}-出局。但在來季布力般流浪被本地鋼鐵廠東主及終身支持者[積奇獲加](../Page/Jack_Walker.md "wikilink")（Jack
Walker）收購。

## 90年代: 積奇獲加革命

### 勇闖英超

獲加收購布力般流浪的賽季（1990-91年）只能在乙組排於令人失望的第十九位，但這位新東主已準備好數以百萬鎊的資金在來季收購新球員。在1991-92賽季展開時仍任用唐麥基為領隊，但他很快便辭職讓路給[肯尼·达格利什](../Page/肯尼·达格利什.md "wikilink")。杜格利殊剛在數月前以壓力為理由離開[利物浦](../Page/利物浦足球俱乐部.md "wikilink")，在其六年領隊任期內共奪得五項重要錦標。杜格利殊作出數項收購以加強實力，包括[黑池的](../Page/黑池足球俱乐部.md "wikilink")[阿倫胡禮](../Page/Alan_Wright.md "wikilink")（Alan
Wright），從[曼城回巢的](../Page/曼城.md "wikilink")[哥連亨特利](../Page/Colin_Hendry.md "wikilink")（Colin
Hendry）及首個用七位數字轉會費從[-{zh-hans:埃弗顿;
zh-hant:愛華頓;}-收購的](../Page/埃弗顿足球俱乐部.md "wikilink")[米克紐維爾](../Page/Mike_Newell.md "wikilink")（Mike
Newell）。布力般流浪在十二月中時一度高據第三位，升級似乎唾手可得。球會主席[比爾霍士](../Page/Bill_Fox.md "wikilink")（Bill
Fox）不幸在這時逝世。在新年後紐維爾因傷缺陣，布力般流浪繼後災難性地連敗六場。在賽季最後一輪比賽，以3-1擊敗[-{zh-hans:普利茅斯;
zh-hant:普利茅夫;}-而排名第六位](../Page/普利茅斯足球俱乐部.md "wikilink")，剛好搭上升級附加賽的尾班車。附加賽首輪兩回合分別主場4-2及作客1-2，累計5-4淘汰[-{zh-hans:德比郡;
zh-hant:打比郡;}-](../Page/德比郡足球俱乐部.md "wikilink")。附加賽決賽對手是[莱斯特城](../Page/莱斯特城足球俱乐部.md "wikilink")，季尾傷愈復出的紐維爾在上半場射入一球-{zh-hans:点球;
zh-hant:十二碼;}-，1-0的賽果維持到完場，將布力般流浪送上新成立的[英格蘭超級聯賽](../Page/英格蘭超級聯賽.md "wikilink")，成為創始成員之一。

### 英超盟主

在1992年布力般流浪大洒金錢而成為頭條新聞，以破英格蘭紀錄350萬鎊收購[-{zh-hans:南安普顿;
zh-hant:修咸頓;}-及](../Page/南安普顿足球俱乐部.md "wikilink")[英格蘭](../Page/英格兰足球代表队.md "wikilink")22歲的前鋒[阿兰·希勒](../Page/舒利亞.md "wikilink")，其他貴價收購包括[格林美拿索斯](../Page/Graeme_Le_Saux.md "wikilink")（Graeme
Le Saux）、[萊韋格利](../Page/Roy_Wegerle.md "wikilink")（Roy
Wegerle）及[添·舒活](../Page/添·舒活.md "wikilink")（Tim
Sherwood）等。在1992-93年首個超聯賽季布力般流浪整季向著冠軍目標前進，唯最終只能獲得第四位，當年尚未可取得[歐洲足協杯的參賽資格](../Page/歐洲足協杯.md "wikilink")，但以一隊缺席頂級聯賽長達30年的球隊來說，成績已算不俗。翌季（1993-94年）再加入[史超活納利](../Page/Stuart_Ripley.md "wikilink")（Stuart
Ripley）及[奇雲格拉查](../Page/Kevin_Gallagher.md "wikilink")（Kevin
Gallagher）兩員猛將，聯賽僅次於[曼聯而登上次席](../Page/曼聯.md "wikilink")。布力般流浪再展金錢魔力，以新紀錄的500萬鎊從[诺里奇城購入年僅](../Page/诺里奇城足球俱乐部.md "wikilink")21歲的前鋒[基斯瑟頓](../Page/Chris_Sutton.md "wikilink")（Chris
Sutton），與舒利亞組成黃金搭檔，合稱「[SAS](../Page/特种空勤团.md "wikilink")」。1994-95年賽季布力般流浪雖然在三大盃賽（[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")、[足總杯及](../Page/英格兰足总杯.md "wikilink")[聯賽杯](../Page/英格兰联赛杯.md "wikilink")）早早出局，但更能集中火力應付超聯賽事，整季以些微分數壓倒[曼聯直至最後一輪比賽](../Page/曼聯.md "wikilink")，布力般流浪以1-2負於[利物浦](../Page/利物浦足球俱乐部.md "wikilink")，以為功虧一簣之際，消息傅來同時比賽的[曼聯作客只能與](../Page/曼聯.md "wikilink")[-{zh-hans:西汉姆联;
zh-hant:韋斯咸;}-踢成](../Page/西汉姆联足球俱乐部.md "wikilink")1-1平手，自1914年來頂級聯賽錦標終於重回布力般，亦是超聯成立後的第二支冠軍球隊。獲加的美夢成真：在購入布力般流浪短短五年間，將一支在乙組苦苦掙扎的球隊一躍而成超聯新盟主。

### 力不從心

杜格利殊在奪冠後晉升為足球總監，將領隊職務移交給副手[雷·夏福特](../Page/Ray_Harford.md "wikilink")（Ray
Harford）接掌。1995-96年賽季開季成績極差，輸了頭三輪聯賽及在[歐聯分組賽出局](../Page/歐聯.md "wikilink")，加上主力球員如拿索斯及瑟頓等需長期養傷，情況並不樂觀。幸好舒利亞保持隹態，成為首位連續三季取得30個入球以上的球員，布力般流浪在餘下賽季成績改善，以第七位結束，僅僅錯過[歐洲足協杯的參賽資格](../Page/歐洲足協杯.md "wikilink")。在1996年夏季舒利亞以破世界紀錄的1,500萬鎊轉會費改投其家鄉球會[-{zh-hans:纽卡斯尔联;
zh-hant:紐卡素;}-](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，而布力般流浪一直未能找到合適的替補球員。1996-97年賽季開季可說是一個惡夢，布力般流浪在頭十輪聯賽未嘗勝果，夏福特在十月尾布力般流浪排在榜末時宣佈辭職。在奪得冠軍的兩年後降級似成定局，但暫代領隊[東尼·柏堅斯扭轉敗局](../Page/Tony_Parkes.md "wikilink")，布力般流浪最終排第十三位，力保超聯一席位。

1997年夏季[霍奇森](../Page/罗伊·霍奇森.md "wikilink")（Roy
Hodgson）獲提名為新領隊，鶴臣曾在1994年率領[瑞士進軍](../Page/瑞士國家足球隊.md "wikilink")[世界盃決賽週](../Page/世界盃.md "wikilink")。首季即取得[歐洲足協杯參賽席位](../Page/歐洲足協杯.md "wikilink")。但在翌季12月布力般流浪仍在超聯榜末掙扎，鶴臣隨即被革退。[曼聯副領隊](../Page/曼聯.md "wikilink")[白賴仁·傑特](../Page/布莱恩·基德.md "wikilink")（Brian
Kidd）走馬上任，但不能阻止布力般流浪降級的命運，諷刺地決定降級的一戰在季末第二輪比賽，布力般流浪在主場只能以0-0戰平傑特的舊球會[曼聯](../Page/曼聯.md "wikilink")，宣佈護級失敗。

## 千禧年代

### 重回英超

1999-2000年賽季對布力般流浪而言是一個困難的考驗。在開季前成為升級熱門，但在十月時只處於降級安全線上，傑特被革退，甲隊教練柏堅斯再次被任命為暫代領隊，處理職務直到翌年三月[格雷姆·索内斯正式接任為新領隊才結束](../Page/格雷姆·索内斯.md "wikilink")。積奇獲加不幸於2000-01年賽季開始前去世，布力般流浪上下決定極力爭取升級來紀念這位大恩人。最終在季末成為甲組聯賽亞軍，獲得升級重回[超級聯賽](../Page/英格蘭足球超級聯賽.md "wikilink")。

回到超聯首季（2001-02年）排名第十位，但奪得布力般流浪歷史首次的[聯賽杯冠軍](../Page/英格兰联赛杯.md "wikilink")。在桑拿士帶領下布力般流浪在2002-03年賽季繼續進步，取得第六位而連續兩年晉身[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")。好景不常，接著的2003-04年賽季急跌至第15位，桑拿士被受壓力。在翌季（2004-05年）初[-{zh-hans:纽卡斯尔联;
zh-hant:紐卡素;}-辭退笠臣爵士後即向桑拿士招手](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，桑拿士匆匆過檔。當時的[威爾斯領隊](../Page/威爾斯足球代表隊.md "wikilink")[-{zh-hans:马克·休斯;
zh-hant:馬克曉士;}-成為新領隊](../Page/马克·休斯.md "wikilink")，曉士曾在數季前以球員身份效力布力般流浪協助球隊升級及奪得聯賽杯。曉士在該季帶領布力般流浪以第15名完成以及打進足總杯4強(敗給該屆冠軍阿仙奴),
力保布力般流浪的超聯席位。

在曉士領軍下，布力般一直保持在英超聯賽榜中游位置。至2008年，曉士轉投[曼城](../Page/曼城.md "wikilink")，由[保羅‧恩斯接任領隊一職](../Page/保羅‧恩斯.md "wikilink")，可惜恩斯領軍下的布力般竟連續十一場不勝，加上與球員不和，在同年12月被會方辭退，改聘前[保頓領隊](../Page/保頓.md "wikilink")[艾拿泰斯領軍](../Page/艾拿泰斯.md "wikilink")。艾拿泰斯領軍下，布力般於[2009年至2010年英格蘭超級聯賽取得第十位](../Page/2009年至2010年英格蘭超級聯賽.md "wikilink")，並打入足總盃四強。

2010年11月，布力般被一印度財團入主，新東家將艾拿泰斯辭退，改任[堅恩為領隊](../Page/堅恩.md "wikilink")。

### 再次降落英冠

[2010/11球季](../Page/2010年至2011年英格蘭超級聯賽.md "wikilink")，布力般一度捲入降班漩渦，但最後護級成功，但在之後的[2011/12球季](../Page/2011年至2012年英格蘭超級聯賽.md "wikilink")，布力般表現比上一季更差，最終以英超第十九名結束球季，2012/13球季降落英冠作賽。布力般流浪在英冠角逐了五個球季仍未取得返回英超的機會，成為唯一一支在低級別聯賽角逐的前英超聯冠軍球隊。

### 降落英甲及回升英冠

2016/17年度球季，布力般流浪長期位處於英冠榜末降班區內，護級形勢嚴竣。在2017年4月29日第45輪聯賽，布力般流浪主場擊敗[阿士東維拉](../Page/阿士東維拉.md "wikilink")，追至與聯賽榜倒數第四名的[諾定咸森林同分](../Page/諾定咸森林足球會.md "wikilink")，但得失球差稍為落後對手。最後一輪聯賽，布力般流浪雖作客以3-1擊敗[賓福特](../Page/賓福特足球會.md "wikilink")，但諾定咸森林亦在主場以3-0大勝[葉士域治](../Page/葉士域治足球會.md "wikilink")，結果布力般流浪以少兩個得失球差不敵同分的諾定咸森林，只得英冠第廿二名，來屆降班英甲。

2017/18年度球季，布力般流浪取得英甲亞軍，來屆回升英冠。

## 聯賽歷史

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 外部連結

  - [布力般流浪官方網站](http://www.rovers.co.uk)

[Category:布力般流浪足球會](../Category/布力般流浪足球會.md "wikilink")