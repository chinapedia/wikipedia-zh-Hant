**布莱恩·基德**（，），是一名[英格兰前](../Page/英格兰.md "wikilink")[职业](../Page/职业.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，司職[前鋒](../Page/前鋒_\(足球\).md "wikilink")，曾经效力于[曼联](../Page/曼联.md "wikilink")、[阿仙奴](../Page/阿森纳足球俱乐部.md "wikilink")、[曼城](../Page/曼彻斯特城足球俱乐部.md "wikilink")、[愛華頓等多家顶级联赛球队](../Page/埃弗顿足球俱乐部.md "wikilink")，他於曼聯贏得[1968年歐洲冠軍盃的決賽射入](../Page/1968年欧洲冠军杯决赛.md "wikilink")1球，當日是他的19歲[生日](../Page/生日.md "wikilink")。退役後擔任[教練](../Page/教練.md "wikilink")，在[曼联出任](../Page/曼联.md "wikilink")[亚历克斯·弗格森的助理教练长达](../Page/亚历克斯·弗格森.md "wikilink")12年，并培养出了[瑞恩·吉格斯](../Page/瑞恩·吉格斯.md "wikilink")、[保罗·斯科尔斯](../Page/保罗·斯科尔斯.md "wikilink")、[大卫·贝克汉姆等多名著名球员](../Page/大卫·贝克汉姆.md "wikilink")。

## 外部連結

  -
  -
  - [Football Association – Brian Kidd
    profile](http://www.thefa.com/England/SeniorTeam/NewsAndFeatures/Postings/2003/01/37369.htm)

[Category:英格兰足球运动员](../Category/英格兰足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:曼联球员](../Category/曼联球员.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:曼城球員](../Category/曼城球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:保頓球員](../Category/保頓球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英格兰足球领队](../Category/英格兰足球领队.md "wikilink")
[Category:普雷斯頓領隊](../Category/普雷斯頓領隊.md "wikilink")
[Category:布力般流浪領隊](../Category/布力般流浪領隊.md "wikilink")