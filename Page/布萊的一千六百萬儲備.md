[Blair's_16_Million_Reserve.jpg](https://zh.wikipedia.org/wiki/File:Blair's_16_Million_Reserve.jpg "fig:Blair's_16_Million_Reserve.jpg")
**布萊的一千六百萬儲備**（）是[美國](../Page/美國.md "wikilink")[新澤西州的一家食品公司](../Page/新澤西州.md "wikilink")[布萊的醬料與小吃所限量生產的](../Page/布萊的醬料與小吃.md "wikilink")[辣椒醬產品](../Page/辣椒醬.md "wikilink")，由該公司創始人布萊·拉扎爾（）所調製出，是現今世界上最辣的辣椒醬，辣味強度相當於1600萬[斯科維爾單位](../Page/斯科維爾單位.md "wikilink")，此即為「一千六百萬儲備」的名字由來。此辣椒醬比[塔巴斯科辣椒醬還辣](../Page/塔巴斯科辣椒醬.md "wikilink")6,400倍；比全球最辣的辣椒[龍之氣息辣](../Page/龍之氣息.md "wikilink")6倍。部分醫療專家相信，布萊的一千六百萬儲備辣椒醬能把[哮喘病人置諸死地](../Page/哮喘病.md "wikilink")\[1\]。

布萊的一千六百萬儲備辣椒醬以精緻的瓶子盛載無色[辣椒素結晶](../Page/辣椒素.md "wikilink")，瓶口以[蠟封合](../Page/蠟.md "wikilink")，並附有警告字句封條。它比[墨西哥的](../Page/墨西哥.md "wikilink")[沙維那亞伯內洛紅辣椒還要辣上](../Page/沙維那亞伯內洛紅辣椒.md "wikilink")30倍\[2\]。布萊表示，提煉出重一磅辣椒素必須使用數以噸計的[辣椒](../Page/辣椒.md "wikilink")，所以就算要將一千六百萬儲備對外流銷，都只能限量發售。布萊爾的一千六百萬儲備有999瓶，最初市價為200[美元](../Page/美元.md "wikilink")\[3\]。

布萊表示，他曾經把一顆鹽花大小的結晶放在自己的舌頭上，該部份立即受損、「感覺就像是被鐵鎚打到一樣」、並且在兩天後才痊癒\[4\]。

## 參考資料

## 外部連結

  - [布萊的一千六百萬儲備（Chilli
    World）](http://www.chilliworld.com/SP6.asp?p_id=63)
  - [辣度排行榜（Chilli
    World）](http://www.chilliworld.com/FactFile/Scoville_Scale.asp)

[en:Blair's Sauces and Snacks\#16 Million
Reserve](../Page/en:Blair's_Sauces_and_Snacks#16_Million_Reserve.md "wikilink")

[Category:辣椒酱](../Category/辣椒酱.md "wikilink")
[Category:美國食品](../Category/美國食品.md "wikilink")
[Category:美國的世界之最](../Category/美國的世界之最.md "wikilink")

1.  [World's Hottest Hot Sauce - Blair's 16 Million
    Reserve](http://www.thegreenhead.com/2007/07/blairs-16-million-reserve-worlds-hottest-hot-sauce.php)


2.
3.
4.  [Blair's 16 Million Product
    Review](http://www.hotsauceblog.com/hotsaucearchives/blairs-16-million-product-review/)