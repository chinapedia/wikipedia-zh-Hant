[Po_Toi_O_Clear_Water_Bay_Golf_Course_2014.jpg](https://zh.wikipedia.org/wiki/File:Po_Toi_O_Clear_Water_Bay_Golf_Course_2014.jpg "fig:Po_Toi_O_Clear_Water_Bay_Golf_Course_2014.jpg")養殖場及[清水灣鄉村俱樂部高爾夫球場](../Page/清水灣鄉村俱樂部高爾夫球場.md "wikilink")\]\]
[Po_Toi_O_1.jpg](https://zh.wikipedia.org/wiki/File:Po_Toi_O_1.jpg "fig:Po_Toi_O_1.jpg")[田下山南脊眺望布袋澳](../Page/田下山.md "wikilink")\]\]
**布袋澳**（）是[香港](../Page/香港.md "wikilink")[新界的一個](../Page/新界.md "wikilink")[海灣](../Page/海灣.md "wikilink")，位於[清水灣半島的南部](../Page/清水灣半島.md "wikilink")（以及[清水灣](../Page/清水灣.md "wikilink")[海灣東南偏南](../Page/海灣.md "wikilink")），田下山及[清水灣郊野公園的東面](../Page/清水灣郊野公園.md "wikilink")，[大廟灣及](../Page/大廟灣.md "wikilink")[佛堂門天后廟](../Page/佛堂門天后廟.md "wikilink")（大廟）的北面，[清水灣的南面](../Page/清水灣.md "wikilink")，[清水灣鄉村俱樂部高爾夫球場的西面](../Page/清水灣鄉村俱樂部.md "wikilink")。布袋澳海灣的入口在北部，入口的東面和西面分別為[石尾頭和](../Page/石尾頭.md "wikilink")[下角頭](../Page/下角頭.md "wikilink")。布袋灣內有魚排養殖場，鄰近有[大坳門路及](../Page/大坳門路.md "wikilink")[布袋澳村路可以到達](../Page/布袋澳村路.md "wikilink")[布袋澳村](../Page/布袋澳村.md "wikilink")，此村村民多以[高](../Page/高.md "wikilink")、[張](../Page/張.md "wikilink")、[劉](../Page/劉.md "wikilink")、[鄧氏](../Page/鄧.md "wikilink")[客家](../Page/客家.md "wikilink")[原居民](../Page/原居民.md "wikilink")，經營[海鮮生意及](../Page/海鮮.md "wikilink")[本地旅遊為業](../Page/香港旅遊.md "wikilink")。

布袋澳三面環山，北西出海口較窄，就像布袋一樣，布袋澳因而得名。

## 交通

[大坳門路是前往布袋澳的必經之路](../Page/大坳門路.md "wikilink")，可在[銀影路轉入](../Page/銀影路.md "wikilink")[清水灣道後走入](../Page/清水灣道.md "wikilink")。

  -   - 寶琳站位置：寶琳港鐵站A2出口[新都城地面樓層](../Page/新都城.md "wikilink")[巴士總站右旁](../Page/巴士總站.md "wikilink")
      - 布袋澳位置：[布袋澳村路](../Page/布袋澳村路.md "wikilink")[翡翠別墅入口前面](../Page/翡翠別墅.md "wikilink")
      - 平日班次約為30分鐘一班，而假日19:30前每15分鐘一班。
      - （尾班車往[寶琳為](../Page/寶琳.md "wikilink")23:30，而[寶琳往布袋澳尾班車於](../Page/寶琳.md "wikilink")21:30）

<!-- end list -->

  - [**<span style="color: #00B14F;">新界的士</span>**可行走至](../Page/香港的士.md "wikilink")[坑口鐵路站](../Page/坑口鐵路站.md "wikilink")
  - [旅遊巴士](../Page/旅遊巴士.md "wikilink")
  - [遊艇](../Page/遊艇.md "wikilink")

## 設施

  - 位於布袋澳村路盡頭，小巴總站，[翡翠別墅入口前面設有供張貼有傷殘人士車輛停泊許可證之汽車停車場乙個](../Page/翡翠別墅.md "wikilink")，在小巴總站上客位置旁邊。
  - 旱廁變為[沖水式廁所](../Page/沖水式廁所.md "wikilink")——[市政總署及](../Page/市政總署.md "wikilink")[區域市政局遺產被翻新](../Page/區域市政局.md "wikilink")（編號：SK-25，SK代表[西貢](../Page/西貢.md "wikilink")），男女廁各設有座廁[馬桶乙座](../Page/馬桶.md "wikilink")、[電子](../Page/電子.md "wikilink")[感應](../Page/感應.md "wikilink")[水龍頭](../Page/水龍頭.md "wikilink")、乾手機，男女廁格比例：二比三；位於已荒廢[鄉村學校之下](../Page/鄉村學校.md "wikilink")，需要經過一系列海鮮檔位旁邊主要小徑前往。

## 區議會議席分佈

布袋澳屬[鄉村範圍](../Page/鄉村.md "wikilink")，人口不多，所以跟隨[清水灣半島其他地方組合為](../Page/清水灣半島.md "wikilink")[坑口東選區](../Page/坑口東.md "wikilink")，選出一名區議員。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>布袋澳</strong>全部範圍</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [布袋澳地圖](http://www.centamap.com/gc/centamaplocation.aspx?x=848201&y=814938&sx=848201.68784&sy=814938.37008&z=6)

[Category:清水灣半島](../Category/清水灣半島.md "wikilink")
[Category:西貢區鄉村](../Category/西貢區鄉村.md "wikilink")