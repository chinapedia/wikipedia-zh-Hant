**布賴恩縣**（**Bryan County,
Oklahoma**）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州東南部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，南以[雷德河為界與](../Page/雷德河_\(密西西比河\).md "wikilink")[德克薩斯州相望](../Page/德克薩斯州.md "wikilink")。面積2,443平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口36,534人。縣治[杜蘭特](../Page/杜蘭特_\(奧克拉荷馬州\).md "wikilink")（Durant）。

成立於1907年7月16日，縣名紀念三度被提名為[民主黨](../Page/民主黨_\(美國\).md "wikilink")[總統候選人](../Page/美國總統.md "wikilink")[威廉·詹寧斯·布萊恩](../Page/威廉·詹寧斯·布萊恩.md "wikilink")。


[B](../Category/俄克拉何马州行政区划.md "wikilink")