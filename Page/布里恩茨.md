**布里恩茨**（[德语](../Page/德语.md "wikilink")：****，发音：\[briənt͡s\]）是[瑞士](../Page/瑞士.md "wikilink")[伯恩州](../Page/伯恩州.md "wikilink")[因特拉肯区的一座城市](../Page/因特拉肯区.md "wikilink")，位處[布里恩茨湖的北面](../Page/布里恩茨湖.md "wikilink")。因其木刻遠近馳名，以木刻小鎮聞名。有證據顯示[阿拉曼人早在](../Page/阿拉曼人.md "wikilink")7世紀時已經在此落地生根，而至1146年布里恩茨第一次被記錄在案。

## 友好城市

  - [瑞士Brinzauls](../Page/瑞士.md "wikilink")

  - [保加利亚](../Page/保加利亚.md "wikilink")[特里亚夫纳](../Page/特里亚夫纳.md "wikilink")

  - [日本](../Page/日本.md "wikilink")[岛田市](../Page/岛田市.md "wikilink")

## 外部連結

  - [官方網站](https://web.archive.org/web/20080307002828/http://www.brienz.ch/web/)

[B](../Page/category:伯恩州的市镇.md "wikilink")