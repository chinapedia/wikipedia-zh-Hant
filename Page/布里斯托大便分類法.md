**-{布里斯托}-大便分類法**（Bristol Stool
Scale）是一種為了[醫學上的需求而設計的分類法](../Page/醫學.md "wikilink")，它將人類的[大便分為七類](../Page/大便.md "wikilink")。設計者為[-{布里斯托}-大學](../Page/布里斯托大學.md "wikilink")（University
of Bristol）的希頓（Heaton）和路易斯（Lewis），首篇於1997年發表在《北歐腸胃病學雜誌》（Scandinavian
Journal of
Gastroenterology）上。因為大便的形狀和其待在[大腸內的時間有關](../Page/大腸.md "wikilink")，所以可以用它來判斷食物經過大腸所需的時間。

分類法：

  - 第一型：一顆顆硬球（很難通过）
  - 第二型：香腸狀，但表面凹凸
  - 第三型：香腸狀，但表面有裂痕
  - 第四型：像香腸或蛇一樣，且表面很光滑
  - 第五型：断边光滑的柔软块状（容易通过）
  - 第六型：粗边蓬松块，糊状大便
  - 第七型：水状，无固体块（完全液体）

第一型和第二型表示有[便秘](../Page/便秘.md "wikilink")；第三型和第四型是理想的便形，尤其第四型是最容易[排便的形狀](../Page/排便.md "wikilink")；第五至第七型則有[腹瀉的可能](../Page/腹瀉.md "wikilink")。

## 参考资料

  - [Constipation Management and Nurse Prescribing: The importance of
    developing a concordant approach
    PDF](http://www.constipationadvice.co.uk/further-resources/pam-campbell-constipation-management.pdf)

  - [Faecal incontinence and constipation
    PDF](https://web.archive.org/web/20070724142601/http://www.rila.co.uk/issues/full/005/2004/v6n3/p99-108.pdf)

## 外部链接

  - [The Bristol Stool Scale from
    Medscape.com](http://www.medscape.com/pages/sites/infosite/zelnorm/article-diagnosis)

  - [Information from Solvay
    Pharmaceuticals](https://web.archive.org/web/20160304211707/http://www.solvay-ibs.com/index.html?source=http%3A%2F%2Fwww.solvay-ibs.com%2Fconsumer%2Fabout%2Fdiagnose.html)

  - [Childhood
    Constipation](https://web.archive.org/web/20060705161640/http://www.childhoodconstipation.com/)

[Category:生理學](../Category/生理學.md "wikilink")
[Category:医学](../Category/医学.md "wikilink")
[Category:检验医学](../Category/检验医学.md "wikilink")