**布里斯班河**（**Brisbane
River**）為[澳大利亚](../Page/澳大利亚.md "wikilink")[昆士兰州流经府城](../Page/昆士兰州.md "wikilink")[布里斯班](../Page/布里斯班.md "wikilink")[市中心](../Page/布里斯本中央商業區.md "wikilink")，並於[摩顿湾入海的一條重要河流](../Page/摩顿湾.md "wikilink")。

布里斯班河在上游被[Wivenhoe大坝所拦截](../Page/Wivenhoe大坝.md "wikilink")，形成了[Wivenhoe湖](../Page/Wivenhoe湖.md "wikilink")，乃目前[布里斯班市的主要水源](../Page/布里斯班.md "wikilink")。此河名源於1823年探险家[约翰·奧克斯力](../Page/约翰·奧克斯力.md "wikilink")（John
Oxley），以當年[新南威尔士州](../Page/新南威尔士州.md "wikilink")[州长](../Page/州长.md "wikilink")－布里斯本爵士（Sir
Thomas Brisbane）之名而命之。

府城颇具觀光特色的[城市猫摆渡於](../Page/CityCat.md "wikilink")[中央商業區沿河兩旁](../Page/中央商務區.md "wikilink")，服务旅客往來。[布里斯本港则管理著从海湾进入布里斯班河的大型貨輪](../Page/布里斯本港.md "wikilink")。

## 历史

[BrisbaneRiver02_gobeirne.jpg](https://zh.wikipedia.org/wiki/File:BrisbaneRiver02_gobeirne.jpg "fig:BrisbaneRiver02_gobeirne.jpg")
在[欧洲人到来之前](../Page/欧洲人.md "wikilink")，布里斯班河就是[澳大利亞原住民](../Page/澳大利亞原住民.md "wikilink")[Turrbal族人的重要生計来源](../Page/Turrbal.md "wikilink")。[Turrbal人素以捕鱼为生](../Page/Turrbal.md "wikilink")，同时这条河在他们的精神和娱乐活动方面也含有重要意义。

## 桥梁

目前布里斯班河上有13座桥，包括历史上著名的[故事桥和收费的](../Page/故事桥.md "wikilink")[Sir Leo
Hielscher Bridges](../Page/:en:Sir_Leo_Hielscher_Bridges.md "wikilink")。

## 參見

  - [Brisbane River Stage](../Page/Brisbane_River_Stage.md "wikilink")
  - [Pinkenba Wharf](../Page/Pinkenba_Wharf.md "wikilink")

## 参考文献

### 引用

### 来源

  - Department of Harbours and Marine 1986 "Harbours and Marine *Port
    and Harbour Development in Queensland from 1824 to 1985*" pp. 25
    Queensland Government Department of Harbours and Marine
  - J. G. Steele 1975 *Brisbane Town in Convict Days 1824-42* pp. 118
    University of Queensland Press
  - <cite id="refLonghurst">Longhurst, Robert; William Douglas. *The
    Brisbane River: A pictoral history*. Brisbane, W.D. Incorporated
    Pty. Ltd.</cite>
  - <cite id="refMcLeod1990">McLeod, G Roderick (1990). "Some aspects of
    the History of the Brisbane River". In Peter Davie, Errol Stock,
    Darryl Low Choy (Ed.), *The Brisbane River: a source-book for the
    future*, Australian Littoral Society in association with the
    [Queensland Museum](../Page/Queensland_Museum.md "wikilink").</cite>

## 外部連結

  - [布里斯本河的歷史地圖](https://web.archive.org/web/20060107153021/http://www.ourindooroopilly.com/brisbane-river-map.html)
  - [History of Brisbane
    flooding](http://www.bom.gov.au/hydro/flood/qld/fld_history/brisbane_history.shtml)
    — [Bureau of
    Meteorology](../Page/Bureau_of_Meteorology_\(Australia\).md "wikilink")
    official website
  - [布里斯本河邊步道](http://www.brisbane.qld.gov.au/BCC:STANDARD:1576302065:pc=PC_1216)
  - [Murray
    Cod](https://web.archive.org/web/20071010105433/http://www.nativefish.asn.au/cod.html)

[Category:布里斯班](../Category/布里斯班.md "wikilink")
[Category:昆士兰州河流](../Category/昆士兰州河流.md "wikilink")