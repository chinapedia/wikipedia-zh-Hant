**布雷肯里奇縣**（**Breckinridge County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州北部的一個縣](../Page/肯塔基州.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[印地安納州相望](../Page/印地安納州.md "wikilink")。面積1,483平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口18,648人。縣治[哈丁斯堡](../Page/哈丁斯堡_\(肯塔基州\).md "wikilink")
（Hardinsburg）。

縣政府成立於1799年12月9日。\[1\]縣名紀念[聯邦參議員](../Page/美國參議院.md "wikilink")、[司法部長](../Page/美國司法部長.md "wikilink")[約翰·布雷肯里奇](../Page/約翰·布雷肯里奇_\(1760-1806\).md "wikilink")
（John Breckinridge）。本縣為一[禁酒的縣](../Page/禁酒.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/肯塔基州行政區劃.md "wikilink")

1.  <http://www.breckinridgecounty.net/ourheritage.html>