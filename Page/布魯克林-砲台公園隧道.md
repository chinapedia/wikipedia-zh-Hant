**布魯克林-砲台公園隧道**（Brooklyn-Battery
Tunnel）是一座位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市的收費](../Page/紐約市.md "wikilink")[隧道](../Page/隧道.md "wikilink")，穿越[東河底下連接](../Page/東河_\(紐約\).md "wikilink")[曼哈頓與](../Page/曼哈頓.md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")。隧道在1950年通車，其結構是由兩管隧道構成雙向總共四線道，的長度使得布魯克林-砲台公園隧道成為[北美洲最長的水下隧道](../Page/北美洲.md "wikilink")。過去曾載有紐約州27A公路，現今載有478號[州際公路](../Page/州際公路.md "wikilink")。

隧道名稱中的「砲台」（即英文的Battery）意指曼哈頓島的最南端，過去這塊地方在紐約市早期時曾經是部署防守重炮的軍事要地。紐約市府擁有布魯克林-砲台公園隧道的所有權，但是由[紐約大都會運輸署的附屬機構](../Page/紐約大都會運輸署.md "wikilink")－[三區橋樑暨隧道管理局所營運管理](../Page/三區橋樑暨隧道管理局.md "wikilink")。

## 歷史

### 建造歷程

最初[三區橋樑管理局](../Page/三區橋樑管理局.md "wikilink")（即三區橋樑暨隧道管理局的前身）的局長[羅伯特·莫斯屢次嘗試著將興建隧道的提案改為在同址以興建橋樑替代](../Page/羅伯特·莫斯.md "wikilink")，雖然許多人反對興建橋樑，因為擔心橋樑的興建會破壞曼哈頓下城[城市天際線的景致](../Page/城市天際線.md "wikilink")，而且[砲台公園也會因為橋樑連絡交流道龐大的佔地面積](../Page/砲台公園.md "wikilink")，而被縮減成極小的大小，以及也會需要摧毀當初位於[柯林頓堡](../Page/柯林頓堡.md "wikilink")（[Clinton
Castle](../Page/:en:Clinton_Castle.md "wikilink")）的紐約水族館。最後是由[美國總統](../Page/美國總統.md "wikilink")[富蘭克林·D·羅斯福經由軍事國防管道下達一道命令](../Page/富蘭克林·D·羅斯福.md "wikilink")，以在該址興建橋樑會對[布魯克林海軍碼頭](../Page/布魯克林造船廠.md "wikilink")（[Brooklyn
Navy
Yard](../Page/:en:Brooklyn_Navy_Yard.md "wikilink")）通往外海造成阻礙並對國家安全構成危險為由，最後才得以恢復隧道的工程。然而這個理由實際上並不成立，因為在當時[曼哈頓大橋與](../Page/曼哈頓大橋.md "wikilink")[布魯克林大橋早已位於布魯克林海軍碼頭與紐約外海之間](../Page/布魯克林大橋.md "wikilink")。

隧道的設計者為[歐雷·辛史塔得](../Page/歐雷·辛史塔得.md "wikilink")（[Ole
Singstad](../Page/:en:Ole_Singstad.md "wikilink")）並在1940年10月開始興建，然而在興建的途中因為[二次世界大戰的爆發而暫止工程](../Page/二次世界大戰.md "wikilink")。二戰結束之後莫斯所管的三區橋樑管理局與紐約隧道管理局（The
Tunnel
Authority）合併成為[三區橋梁暨隧道管理局](../Page/三區橋梁暨隧道管理局.md "wikilink")，而布魯克林-砲台公園隧道的興建工程也被三區橋梁暨隧道管理局所接管，而興建工程亦在1945年底恢復，並在1950年完工通車。

### 現代

紐約州議會在2010年12月8日時決議將布魯克林－砲台公園隧道改名為休·凱利隧道，以紀念前紐約州長休·凱利（[Hugh
Carey](../Page/:en:Hugh_Carey.md "wikilink")）\[1\]
，並於2012年10月22日正式更名為休·L·凱利隧道\[2\]。

因為[颶風珊迪來襲](../Page/颶風珊迪.md "wikilink")，隧道在2012年10月29日淹水\[3\]，被迫暫時關閉修復至今。
现阶段工作日的晚9点至早6点，以及周末的全天，一侧的隧道被关闭，另一侧则改为双向车道。另一条位于中城的皇后隧道同样因为飓风的影响维修至今。纽约市极慢的维修效率给纽约市的通勤族以及司机造成极大的不便和痛苦。
完工之日遥遥无期，以致纽约市州长库默下命令要求纽约市的桥梁隧道维修必须在2017年夏天之前完工。

## 收費

小客車的費率為$8.50雙向收費，而使用[電子收費](../Page/電子收費_\(道路系統\).md "wikilink")[E-ZPass則有](../Page/E-ZPass.md "wikilink")50分錢的優惠；機車的費率為$2，E-ZPass使用者則有25分錢的優惠。

## 相關電影

在1997年上映的電影《[MIB星際戰警](../Page/MIB星際戰警.md "wikilink")》中，布魯克林-砲台公園隧道在曼哈頓端的通氣塔建築就是電影中MIB的機密外星入出境管理大廈以及MIB星際戰警的總部。

## 478號州際公路

[I-478_(long).svg](https://zh.wikipedia.org/wiki/File:I-478_\(long\).svg "fig:I-478_(long).svg")

**478號[州際公路](../Page/州際公路.md "wikilink")**的全長即為布魯克林-砲台公園隧道的全段以及其交流道。I-478的南端點為[278號州際公路](../Page/278號州際公路.md "wikilink")，其北端點為[紐約州9A公路](../Page/紐約州9A公路.md "wikilink")（即[西側快速道路](../Page/西側快速道路_\(紐約市\).md "wikilink")－[West
Side
Highway](../Page/:en:West_Side_Highway.md "wikilink")）。原本I-478計畫繼續往北經由取消的地下[西道快速道路連接](../Page/西道快速道路.md "wikilink")[荷蘭隧道接往](../Page/荷蘭隧道.md "wikilink")[78號州際公路](../Page/78號州際公路.md "wikilink")。

其他曾經提案過使用I-478編號的路線包括：

  - [下曼哈頓快速道路](../Page/下曼哈頓快速道路.md "wikilink")（[Lower Manhattan
    Expressway](../Page/:en:Lower_Manhattan_Expressway.md "wikilink")）的分支用以連接[曼哈頓大橋](../Page/曼哈頓大橋.md "wikilink")（1971年）
  - [大中央公園大道位於](../Page/大中央公園大道.md "wikilink")[I-278與](../Page/I-278.md "wikilink")[I-678之間](../Page/I-678.md "wikilink")（1971年）

在I-478編號於1971年分給西道快速道路計畫之前，西道快速道路是計畫為[I-695](../Page/I-695_\(紐約州\).md "wikilink")，用以延[亨利哈德遜公園大道往北在](../Page/亨利哈德遜公園大道.md "wikilink")[喬治華盛頓大橋與](../Page/喬治華盛頓大橋.md "wikilink")[95號州際公路連接](../Page/95號州際公路#紐約州、紐澤西州、賓夕法尼亞州.md "wikilink")。

## 參考資料

  - [砲台公園](../Page/砲台公園.md "wikilink")
  - [Caro, Robert A.](../Page/:en:Robert_Caro.md "wikilink"), *[The
    Power Broker: Robert Moses and the Fall of New
    York](../Page/:en:The_Power_Broker.md "wikilink")*, New York: Knopf,
    1974. ISBN 0-394-72024-5
  - [nycroads.com about Brooklyn-Battery
    Tunnel](http://www.nycroads.com/crossings/brooklyn-battery/)

### 注釋

<div class="references-small">

<references />

</div>

[Category:紐約市隧道](../Category/紐約市隧道.md "wikilink")
[Category:78号州际公路](../Category/78号州际公路.md "wikilink")
[Category:州際公路系統](../Category/州際公路系統.md "wikilink")

1.
2.
3.