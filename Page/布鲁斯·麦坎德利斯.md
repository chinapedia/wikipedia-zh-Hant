**布鲁斯·麦坎德利斯二世**（，）是[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过以及任务。他在1984年的STS-41-B任务中完成人类历史上首次无系绳太空行走。\[1\]

## 參考資料

## 外部链接

  - [美国国家航空航天局网站的麦克坎德雷斯介绍](https://web.archive.org/web/20070430121741/http://www.jsc.nasa.gov/Bios/htmlbios/mccandless-b.html)

[Bruce_McCandless_II_during_EVA_in_1984.jpg](https://zh.wikipedia.org/wiki/File:Bruce_McCandless_II_during_EVA_in_1984.jpg "fig:Bruce_McCandless_II_during_EVA_in_1984.jpg")
[BruceMcCandlessByPhilKonstantin.jpg](https://zh.wikipedia.org/wiki/File:BruceMcCandlessByPhilKonstantin.jpg "fig:BruceMcCandlessByPhilKonstantin.jpg")


[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")
[Category:美国宇航员](../Category/美国宇航员.md "wikilink")
[Category:阿波罗计划](../Category/阿波罗计划.md "wikilink")
[Category:第五组宇航员](../Category/第五组宇航员.md "wikilink")
[Category:美國海軍學院校友](../Category/美國海軍學院校友.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")

1.