[Shilpa_Shetty.jpg](https://zh.wikipedia.org/wiki/File:Shilpa_Shetty.jpg "fig:Shilpa_Shetty.jpg")

**希爾柏·謝迪**（Shilpa
Shetty，），[印度](../Page/印度.md "wikilink")[寶萊塢](../Page/寶萊塢.md "wikilink")[电影演员](../Page/电影演员.md "wikilink")，1993年拍攝了首部電影。

## 名作

### 成名作

  - [阿格](../Page/阿格_\(印度電影\).md "wikilink")（1994年）──被提名[印度電影觀眾獎](../Page/印度電影觀眾獎.md "wikilink")：最佳女配角。

### 近期

  - [都市浮生錄](../Page/都市浮生錄_\(印度電影\).md "wikilink")（2007年）

## 得獎項

### 實得

  - 2007年: 英國電視節目──*[Big Brother
    Celebrity](../Page/Big_Brother_Celebrity.md "wikilink")* ──
    獎金：三十六萬七千五百[英鎊](../Page/英鎊.md "wikilink")
    (£367,500 GBP)
  - 2007年: [IIFA Special Award for Global
    Impact](../Page/IIFA_Special_Award_for_Global_Impact.md "wikilink").
  - 2007年: **Silver Star Award** for her outstanding contribution to
    humanitarian causes.\[1\]
  - 2007年: Rajiv Gandhi National Quality Award.\[2\]
  - 2005年: AAHOA Award for *Phir Milenge*.\[3\]
  - 2005年: Indian "Diva of the Year" by SaharaOne Television viewers.
    [1](https://web.archive.org/web/20071012162922/http://nowrunning.com/news/slideshow.asp?newsID=2560)
  - 2004年: Giant International Award for *[Phir
    Milenge](../Page/Phir_Milenge.md "wikilink")*.\[4\]
  - 1998年: [Bollywood Movie Award - Best Supporting
    Actress](../Page/Bollywood_Movie_Award_-_Best_Supporting_Actress.md "wikilink")
    for *Pardesi Babu*

### 提名

  - 1994年: [Filmfare Best Supporting Actress
    Award](../Page/Filmfare_Best_Supporting_Actress_Award.md "wikilink")
    for *[Baazigar](../Page/Baazigar.md "wikilink")*
  - 2001年: [IIFA Best Actress
    Award](../Page/IIFA_Best_Actress_Award.md "wikilink") for
    *[Dhadkan](../Page/Dhadkan.md "wikilink")*
  - 2003年: [Filmfare Best Supporting Actress
    Award](../Page/Filmfare_Best_Supporting_Actress_Award.md "wikilink")
    for *[Rishtey](../Page/Rishtey.md "wikilink")*
  - 2003年: [Star Screen Award Best
    Comedian](../Page/Star_Screen_Award_Best_Comedian.md "wikilink") for
    *Rishtey*
  - 2005年: [Star Screen Award Best
    Actress](../Page/Star_Screen_Award_Best_Actress.md "wikilink") for
    *[Phir Milenge](../Page/Phir_Milenge.md "wikilink")*
  - 2005年: [Filmfare Best Actress
    Award](../Page/Filmfare_Best_Actress_Award.md "wikilink") for *Phir
    Milenge*
  - 2005年: [Zee Cine Award Best Actor-
    Female](../Page/Zee_Cine_Award_Best_Actor-_Female.md "wikilink") for
    *Phir Milenge*
  - 2005年: [IIFA Best Actress
    Award](../Page/IIFA_Best_Actress_Award.md "wikilink") for *Phir
    Milenge*
  - 2005年: [Bollywood Movie Award - Best
    Actress](../Page/Bollywood_Movie_Award_-_Best_Actress.md "wikilink")
    for *Phir Milenge*

## 電影演出

| 年份   | 片名                                                                                         | 角色名                   | 片用語言 | 備註                                                                                                         |
| ---- | ------------------------------------------------------------------------------------------ | --------------------- | ---- | ---------------------------------------------------------------------------------------------------------- |
| 1993 | *[Baazigar](../Page/Baazigar.md "wikilink")*                                               | Seema Chopra          | 印地語  | 提名, [Filmfare Best Debut Award](../Page/Filmfare_Best_Debut_Award.md "wikilink")                           |
| 1994 | *[Aao Pyaar Karen](../Page/Aao_Pyaar_Karen.md "wikilink")*                                 | Chhaya                | 印地語  |                                                                                                            |
| 1994 | *[Main Khiladi Tu Anari](../Page/Main_Khiladi_Tu_Anari.md "wikilink")*                     | Mona/Basanti          | 印地語  |                                                                                                            |
| 1994 | *[Aag](../Page/Aag_\(1994_film\).md "wikilink")*                                           | Bijli                 | 印地語  |                                                                                                            |
| 1995 | ''[Hathkadi](../Page/Hathkadi.md "wikilink")                                               | Neha                  | 印地語  |                                                                                                            |
| 1996 | *[Mr. Romeo](../Page/Mr._Romeo.md "wikilink")*                                             | Shilpa                | 泰米爾語 | Dubbed into Hindi and Telugu as *Mr. Romeo*                                                                |
| 1996 | *[Chhote Sarkar](../Page/Chhote_Sarkar.md "wikilink")*                                     | Seema                 | 印地語  |                                                                                                            |
| 1996 | *[Himmat](../Page/Himmat.md "wikilink")*                                                   | Nisha                 | 印地語  |                                                                                                            |
| 1996 | *[Sahasa Veerudu Sagara Kanya](../Page/Sahasa_Veerudu_Sagara_Kanya.md "wikilink")*         | Sona                  | 泰盧固語 | Dubbed into Hindi as *Saagar Kanya*                                                                        |
| 1997 | *[Dus](../Page/Dus_\(1997_film\).md "wikilink")*                                           | Journalist            | 印地語  | Incomplete role                                                                                            |
| 1997 | *[Gambler](../Page/Gambler_\(film\).md "wikilink")*                                        | Ritu                  | 印地語  |                                                                                                            |
| 1997 | *[Prithvi](../Page/Prithvi_\(film\).md "wikilink")*                                        | Neha                  | 印地語  |                                                                                                            |
| 1997 | *[Insaaf](../Page/Insaaf.md "wikilink")*                                                   | Divya                 | 印地語  |                                                                                                            |
| 1997 | *[Zameer: The Awakening of a Soul](../Page/Zameer:_The_Awakening_of_a_Soul.md "wikilink")* | Roma Khurana          | 印地語  |                                                                                                            |
| 1997 | *[Auzaar](../Page/Auzaar.md "wikilink")*                                                   | Prathna Thakur        | 印地語  |                                                                                                            |
| 1997 | *[Veedeva Dani Babu](../Page/Veedeva_Dani_Babu.md "wikilink")*                             | Nandhana              | 泰盧固語 |                                                                                                            |
| 1998 | *[Pardesi Babu](../Page/Pardesi_Babu.md "wikilink")*                                       | Chinni Malhotra       | 印地語  |                                                                                                            |
| 1998 | *[Aakrosh](../Page/Aakrosh.md "wikilink")*                                                 | Komal                 | 印地語  |                                                                                                            |
| 1999 | *[Jaanwar](../Page/Jaanwar.md "wikilink")*                                                 | Mamta                 | 印地語  |                                                                                                            |
| 1999 | *[Shool](../Page/Shool.md "wikilink")*                                                     | Special appearance    | 印地語  | [Item number](../Page/Item_number.md "wikilink")                                                           |
| 1999 | *[Lal Baadshah](../Page/Lal_Baadshah.md "wikilink")*                                       | Lawyer's daughter     | 印地語  |                                                                                                            |
| 2000 | *[Azad](../Page/Azad_\(film\).md "wikilink")*                                              | Kanaka Mahalakshmi    | 泰盧固語 |                                                                                                            |
| 2000 | *[Dhadkan](../Page/Dhadkan.md "wikilink")*                                                 | Anjali                | 印地語  |                                                                                                            |
| 2000 | *[Tarkieb](../Page/Tarkieb.md "wikilink")*                                                 | Preeti Sharma         | 印地語  |                                                                                                            |
| 2000 | *[Khushi](../Page/Khushi.md "wikilink")*                                                   | Macarena              | 泰米爾語 |                                                                                                            |
| 2000 | *[Jung](../Page/Jung_\(film\).md "wikilink")*                                              | Tara                  | 印地語  |                                                                                                            |
| 2001 | *[Indian](../Page/Indian_\(2001_film\).md "wikilink")*                                     | Anjali Rajshekar Azad | 印地語  |                                                                                                            |
| 2001 | *[Bhalevadivi Basu](../Page/Bhalevadivi_Basu.md "wikilink")*                               | Shilpa                | 泰盧固語 | Dubbed into Hindi as *Sherni Ka Shikaar*                                                                   |
| 2001 | *[Maduve Agona Baa](../Page/Maduve_Agona_Baa.md "wikilink")*                               | Preeti                | 坎拿大語 |                                                                                                            |
| 2001 | *[Preethsod Thappa](../Page/Preethsod_Thappa.md "wikilink")*                               | Preethi               | 坎拿大語 |                                                                                                            |
| 2002 | *[Karz](../Page/Karz_\(film_2002\).md "wikilink")*                                         | Sapna                 | 印地語  |                                                                                                            |
| 2002 | *[Rishtey](../Page/Rishtey.md "wikilink")*                                                 | Vaijanti              | 印地語  | 提名, [Filmfare Best Supporting Actress Award](../Page/Filmfare_Best_Supporting_Actress_Award.md "wikilink") |
| 2002 | *[Hathyar](../Page/Hathyar_\(film\).md "wikilink")*                                        | Gauri Shivalkar       | 印地語  |                                                                                                            |
| 2002 | *[Chor Machaaye Shor](../Page/Chor_Machaaye_Shor.md "wikilink")*                           | Kaajal                | 印地語  |                                                                                                            |
| 2002 | *[Badhaai Ho Badhaai](../Page/Badhaai_Ho_Badhaai.md "wikilink")*                           | Radha/Banto Betty     | 印地語  |                                                                                                            |
| 2002 | *[Junoon](../Page/Junoon_\(film\).md "wikilink")*                                          |                       | 印地語  |                                                                                                            |
| 2003 | *[Ondagona Baa](../Page/Ondagona_Baa.md "wikilink")*                                       | Belli                 | 坎拿大語 |                                                                                                            |
| 2003 | *[Darna Mana Hai](../Page/Darna_Mana_Hai.md "wikilink")*                                   | Gayathri              | 印地語  |                                                                                                            |
| 2004 | *[Phir Milenge](../Page/Phir_Milenge.md "wikilink")*                                       | Tamanna Sahani        | 印地語  | 提名, [Filmfare Best Actress Award](../Page/Filmfare_Best_Actress_Award.md "wikilink")                       |
| 2004 | *[Garv: Pride and Honour](../Page/Garv:_Pride_and_Honour.md "wikilink")*                   | Jannat                | 印地語  |                                                                                                            |
| 2005 | *[Dus](../Page/Dus.md "wikilink")*                                                         | Aditi                 | 印地語  |                                                                                                            |
| 2005 | *[Fareb](../Page/Fareb.md "wikilink")*                                                     | Neha                  | 印地語  |                                                                                                            |
| 2005 | *[Khamosh: Khauff Ki Raat](../Page/Khamosh:_Khauff_Ki_Raat.md "wikilink")*                 | Sonia                 | 印地語  |                                                                                                            |
| 2005 | *[Auto Shankar](../Page/Auto_Shankar_\(film\).md "wikilink")*                              | Money Lender          | 坎拿大語 |                                                                                                            |
| 2006 | *[Shaadi Karke Phas Gaya Yaar](../Page/Shaadi_Karke_Phas_Gaya_Yaar.md "wikilink")*         | Ahana                 | 印地語  |                                                                                                            |
| 2007 | *[Life In A... Metro](../Page/Life_In_A..._Metro.md "wikilink")*                           | Shikha                | 印地語  |                                                                                                            |
| 2007 | *[Apne](../Page/Apne.md "wikilink")*                                                       | Simran                | 印地語  |                                                                                                            |
| 2007 | *[Om Shanti Om](../Page/珊蒂別傳.md "wikilink")*                                               | Special Appearance    | 印地語  |                                                                                                            |
| 2007 | *Cash*                                                                                     |                       | 印地語  | Special appearance in the song "Mind Blowing Mahiya"                                                       |
| 2008 | *Dostana*                                                                                  |                       | 印地語  | Special appearance in the song "Shut Up & Bounce"                                                          |
| 2010 | *The Desire*                                                                               | Gautami               |      |                                                                                                            |
| 2014 | *Dishkiyaoon*                                                                              |                       | 印地語  | Special appearance in the song "Tu Mere Type Ka Nahi Hai"                                                  |
|      |                                                                                            |                       |      |                                                                                                            |

## 外部連結

  -
  - [BBC
    News](http://news.bbc.co.uk/1/hi/entertainment/6272561.stm)上的个人资料

  - [Scandal with American actor Gere at AIDS awareness
    gathering](http://www.cnn.com/2007/SHOWBIZ/Movies/04/26/gere.arrest.reut/index.html?eref=rss_topstories)（CNN文章）

  - [Interview with Shilpa on
    WHO.com](https://web.archive.org/web/20080118044244/http://www.who.com/who/magazine/article/0,19636,7401070521-1619685,00.html)

## 參考

[S](../Category/印度电影女演员.md "wikilink")
[S](../Category/圖魯人.md "wikilink")
[Category:孟买女演员](../Category/孟买女演员.md "wikilink")

1.

2.

3.
4.