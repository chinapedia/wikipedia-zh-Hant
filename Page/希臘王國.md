**希臘王國**（），是希臘脫離[鄂圖曼土耳其帝國後](../Page/鄂圖曼土耳其帝國.md "wikilink")，在俄、英、法三國主導下，於1832年召開的[倫敦會議中所建立](../Page/1832年倫敦會議.md "wikilink")，目的是取代原先的[希臘第一共和國](../Page/希臘第一共和國.md "wikilink")。並於隨後簽訂的《君士坦丁堡條約》中獲得承認。1924年，君主制一度被廢，改為第二共和；隨後又於1935年恢復。1941年，[轴心国占领了希腊并扶植了傀儡政权](../Page/轴心国.md "wikilink")[希腊国](../Page/希腊国.md "wikilink")。1944年，希腊王国复辟。1967年，希腊军方发动政变建立[右翼政权](../Page/希臘軍政府時期.md "wikilink")。广义上，直至1974年軍政府垮台、希臘第三共和國成立，希臘王國才正式結束。

## 維特爾斯巴赫王朝

[Prinz_Otto_von_Bayern_Koenig_von_Griechenland_1833.jpg](https://zh.wikipedia.org/wiki/File:Prinz_Otto_von_Bayern_Koenig_von_Griechenland_1833.jpg "fig:Prinz_Otto_von_Bayern_Koenig_von_Griechenland_1833.jpg")\]\]

[希臘人於](../Page/希臘人.md "wikilink")1821年開始反抗[鄂圖曼土耳其](../Page/鄂圖曼土耳其.md "wikilink")，並一直激戰至1829年；1828年由[愛奧尼斯·卡波季斯第亞斯帶領](../Page/愛奧尼斯·卡波季斯第亞斯.md "wikilink")[希臘邁向獨立](../Page/希臘.md "wikilink")，同時當選為[共和國的首任](../Page/共和國.md "wikilink")[總統](../Page/總統.md "wikilink")。然而他卻在1831年遭到[暗殺身亡](../Page/暗殺.md "wikilink")。此後全國陷入了一片混亂；列強決定介入並計劃成立一個[王國](../Page/王國.md "wikilink")。在[1832年倫敦會議中](../Page/1832年倫敦會議.md "wikilink")，英、法、俄三國指派[巴伐利亞](../Page/巴伐利亞.md "wikilink")[維特爾斯巴赫王朝](../Page/維特爾斯巴赫王朝.md "wikilink")、年僅十七歲的奧托王子為[希臘王國的第一位君主](../Page/希臘王國.md "wikilink")，是為[奧托一世](../Page/奧托一世_\(希臘\).md "wikilink")，並規定希臘王國與[巴伐利亞王國永遠不許合併](../Page/巴伐利亞王國.md "wikilink")。由於當時奧托一世尚未成年，於是就由三位[輔政大臣執政](../Page/輔政大臣.md "wikilink")。到了1835年，奧托一世才親政。

## 九月三日革命

1843年，一般大眾普遍對[奧托一世及巴伐利亞王室的專斷行為感到不滿](../Page/奧托一世.md "wikilink")，而開始要求[制憲](../Page/制憲.md "wikilink")、改採[君主立憲](../Page/君主立憲.md "wikilink")。奧托拒絕。於是同年的9月3日，一支步兵團在Dimitri
Kalergis上校，以及備受尊敬的指揮官Yannis
Makriyannis的帶領下包圍了皇宮。揚言倘若國王不制憲，就不會解散。奧托迫於壓力，只得答應。召開[國民大會以回應眾人的請求](../Page/國民大會.md "wikilink")。

結果1844年的首相[約安尼斯·科萊提斯推翻了憲法的自由條文](../Page/約安尼斯·科萊提斯.md "wikilink")，與國王共同建立一個「議會獨裁制」。於是1843年胜利的不是立宪主义者，而是独立战争时的领袖。希腊从巴伐利亚人的专制统治换为希腊人的獨裁统治。他们都没有执行民主的原则。這是因為民主自由的價值觀與精神從未在希臘人心中普及或建立，希臘仍是東方色彩濃重的農業國度，而非西歐進步的工業社會。

## 石勒蘇益格-荷爾斯泰因-宗德堡-格呂克斯堡王朝

[King_George's_Portrait_by_Georgios_Iakovidis.jpg](https://zh.wikipedia.org/wiki/File:King_George's_Portrait_by_Georgios_Iakovidis.jpg "fig:King_George's_Portrait_by_Georgios_Iakovidis.jpg")\]\]

奧托一世在1862年被罷黜後，由來自[丹麥的威廉親王即位](../Page/丹麥.md "wikilink")，是為喬治一世。他一共在位五十年。在這段期間，希臘的版圖大為擴張（在他加冕後不久，英國便把[愛奧尼亞群島割給希臘](../Page/愛奧尼亞群島.md "wikilink")）；同時，在經濟方面也有大幅成長。1913年，就在希臘即將於[第一次巴爾幹戰爭獲勝之際](../Page/第一次巴爾幹戰爭.md "wikilink")，他在[塞薩洛尼基遇刺身亡](../Page/塞薩洛尼基.md "wikilink")。

喬治一世死後，由其子[康斯坦丁一世繼位](../Page/康斯坦丁一世_\(希臘\).md "wikilink")。他在[德國受過教育](../Page/德國.md "wikilink")，並且還娶了德皇[腓特烈三世的女兒](../Page/腓特烈三世_\(德國\).md "wikilink")[蘇菲亞](../Page/索菲_\(普魯士公主\).md "wikilink")。因此也算是德國人，而拒絕首相[埃莱夫塞里奥斯·韦尼泽洛斯參與](../Page/埃莱夫塞里奥斯·韦尼泽洛斯.md "wikilink")[三國協約的建議](../Page/三國協約.md "wikilink")。使希臘一開始在[第一次世界大戰中保持中立](../Page/第一次世界大戰.md "wikilink")。

然而，此舉引起部分人士的反對，雙方意見相左。各自在雅典與[塞薩洛尼基成立政府](../Page/塞薩洛尼基.md "wikilink")，造成分裂的局面。1917年希臘還是加入了三國協約陣營；而康斯坦丁一世則被迫讓位給他的兒子亞歷山大。戰後，希臘獲得[小亞細亞的一些土地作為補償](../Page/小亞細亞.md "wikilink")。

亞歷山大國王於1920年被猴子咬傷而逝世，由康斯坦丁一世重新繼承王位。但稍後發生的[第二次希土戰爭中希臘軍隊慘敗](../Page/第二次希土戰爭.md "wikilink")，致使他又被罷黜下臺。不久於[西西里島的流放地逝世](../Page/西西里島.md "wikilink")。

其後，喬治二世繼位。1924年，他因第二共和的成立而離位。1935年，[喬治·康迪利斯將軍發動](../Page/喬治·康迪利斯.md "wikilink")[政變](../Page/政變.md "wikilink")，將共和政府推翻；並舉辦公民投票恢復君主制，喬治二世復辟。1936年，[愛奧尼斯·美塔薩克斯發動政變](../Page/愛奧尼斯·美塔薩克斯.md "wikilink")，建立[八月四日體制](../Page/八月四日體制.md "wikilink")。1941年德軍入侵希臘後他與全體王室成員流亡到埃及。於1946年回國復位，直到1947年去世。

喬治二世的繼承人為保羅一世，他在位至1964年。而其子康斯坦丁二世則在1967年12月為上校團驅逐，並由上校團所指派的攝政王取而代之。1973年，在軍政府所主導的公投下，廢黜王政。同年6月，[喬治·帕帕多普洛斯出任總統](../Page/喬治·帕帕多普洛斯.md "wikilink")。

軍政府垮台後，康斯坦丁二世返國，但是他並未恢復王位，另外一次公民投票結果，有69%的民眾贊成終止君主制度。希臘王國至此宣告滅亡，走入歷史。

## 君主

自奧托一世到康斯坦丁二世，共歷任七位國王。

## 参考文献

## 参见

  - [希臘近代史](../Page/希臘近代史.md "wikilink")
  - [希腊共和国](../Page/希腊共和国.md "wikilink")

[分類:前聯合國會員國](../Page/分類:前聯合國會員國.md "wikilink")

[\*](../Category/希腊王国.md "wikilink")
[GK](../Category/希腊历史政权.md "wikilink")
[GK](../Category/欧洲历史上的王国.md "wikilink")
[G](../Category/1832年建立的國家或政權.md "wikilink")
[G](../Category/1974年終結的國家或政權.md "wikilink")