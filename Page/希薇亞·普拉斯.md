**希薇亞·普拉斯**（英语：**Sylvia
Plath**，），生于[美国](../Page/美国.md "wikilink")[波士顿](../Page/波士顿.md "wikilink")[牙买加平原区](../Page/:en:Jamaica_Plain.md "wikilink")，[兒童作家出身的美國天才詩人](../Page/兒童文學.md "wikilink")、小說家及[短篇故事作家](../Page/短篇故事.md "wikilink")。

## 简介

普拉斯生於1932年10月27日的波士頓牙買加平原社區。母親Aurelia Schober Plath (1906–1994)為奧地利裔,
父親Otto Plath
(1885–1940)為徳國血統，身為波士頓大學的生物學教授及昆蟲學家。有弟弟Warren出生於1935年4月27日。
[Grave_of_Sylvia_Plath_-_geograph.org.uk_-_412470.jpg](https://zh.wikipedia.org/wiki/File:Grave_of_Sylvia_Plath_-_geograph.org.uk_-_412470.jpg "fig:Grave_of_Sylvia_Plath_-_geograph.org.uk_-_412470.jpg")
除了較為著名的詩作外，普拉斯也以筆名Victoria
Lucas創作了[傳記小說](../Page/傳記小說.md "wikilink")《[瓶中美人](../Page/:en:The_Bell_Jar.md "wikilink")》，書中主角，聰慧、有抱負的[史密斯學院學生艾瑟](../Page/史密斯學院.md "wikilink")·葛林伍德，便是普拉斯的化身，後於紐約時尚雜誌實習期間，展開了一段精神崩潰的歷程，正如同普拉斯在《Mademoiselle》雜誌工作時，經歷心理掙扎、自殺未遂的過程。

普拉斯与女性文学天才[安妮·塞克斯頓并称](../Page/安妮·塞克斯頓.md "wikilink")，被公認是[自白詩重要的推動者之一](../Page/自白詩.md "wikilink")。

## 作品

### 诗歌集

  - 《普拉斯的精华等》（*[The Colossus and Other
    Poems](../Page/:en:The_Colossus_and_Other_Poems.md "wikilink")*，1960年）
  - 《普拉斯的美人鱼》（*[Ariel](../Page/:en:Ariel_\(book\).md "wikilink")*，1965年）
  - 《三位女子的心灵独白》（''Three Women: A Monologue for Three Voices ''，1968年）
  - 《渡湖》（*Crossing the Water*，1971年）
  - 《冬之树》（*Winter Trees*，1971年）
  - 《诗集》（*The Collected Poems*，1981年）
  - 《普拉斯精选》（*Selected Poems*，1985年）
  - 《普拉斯：诗歌天地》（*Plath: Poems*，1998年）
  - 《普拉斯的书海》（*Sylvia Plath Reads*，2000年）

### 散文及小说

  - 《瓶中美人》（*[The Bell
    Jar](../Page/:en:The_Bell_Jar.md "wikilink")*，1963年）
      - 普拉斯的半自传体小说，笔名 Victoria Lucas。
  - 《普拉斯的家书》（*[Letters Home: Correspondence
    1950–1963](../Page/:en:Letters_Home:_Correspondence_1950–1963.md "wikilink")*，1975年）
  - 《约翰尼·派尼克与梦想天堂》（*[Johnny Panic and the Bible of
    Dreams](../Page/:en:Johnny_Panic_and_the_Bible_of_Dreams.md "wikilink")*，1977年）
  - 《普拉斯的旅程》（''The Journals of Sylvia Plath ''，1982年）
  - 《魔镜》（*The Magic Mirror*，1989年）
  - 《完整版的普拉斯旅程》（*The Unabridged Journals of Sylvia Plath*，2000年）

### 儿童文学

  - 《枕边童话》（*The Bed Book*，1976年）
  - 《不合时宜》（*The It-Doesn't-Matter-Suit*，1996年）
  - 《普拉斯的童话选集》（*Collected Children's Stories*，2001年）
  - 《切丽夫人的厨房》（*Mrs. Cherry's Kitchen*，2001年）

## 參見

  - [自白詩](../Page/自白詩.md "wikilink")
  - [安妮·塞克斯顿](../Page/安妮·塞克斯顿.md "wikilink")：自白诗的另一位女性文学天才，也是普拉斯的挚友，最终也是抑郁症失控自杀早逝（45岁，煤气自杀）
  - [普拉斯文学风格效应](../Page/:en:Sylvia_Plath_effect.md "wikilink")：心理学大师对于文学家抑郁症及其相关效应的分析研究。
  - Peter K. Steinberg - [A celebration, this is: A website for Sylvia
    Plath](http://www.sylviaplath.info/)

[Category:美国诗人](../Category/美国诗人.md "wikilink")
[Category:美國散文家](../Category/美國散文家.md "wikilink")
[Category:美国小说家](../Category/美国小说家.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:兒童文学作家](../Category/兒童文学作家.md "wikilink")
[Category:女性小說家](../Category/女性小說家.md "wikilink")
[Category:女性詩人](../Category/女性詩人.md "wikilink")
[Category:女性自殺者](../Category/女性自殺者.md "wikilink")
[Category:自殺作家](../Category/自殺作家.md "wikilink")
[Category:自杀诗人](../Category/自杀诗人.md "wikilink")
[Category:女性主義者](../Category/女性主義者.md "wikilink")
[Category:普利茲獎獲獎者](../Category/普利茲獎獲獎者.md "wikilink")
[Category:史密斯學院校友](../Category/史密斯學院校友.md "wikilink")
[Category:劍橋大學紐納姆學院校友](../Category/劍橋大學紐納姆學院校友.md "wikilink")
[Category:波士頓人](../Category/波士頓人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:奧地利裔美國人](../Category/奧地利裔美國人.md "wikilink")
[Category:在英國的美國人](../Category/在英國的美國人.md "wikilink")
[Category:情感障碍患者](../Category/情感障碍患者.md "wikilink")