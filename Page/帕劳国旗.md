**[帛琉國旗](../Page/帛琉.md "wikilink")**是一面雙色旗幟。天藍色底象徵[海洋](../Page/海洋.md "wikilink")，中間偏左的方向為一黃色圓形，代表了[月亮](../Page/月亮.md "wikilink")。

本旗啟用於1981年1月1日。帛琉獨立前曾使用過[聯合國旗](../Page/聯合國旗.md "wikilink")、[美國國旗和](../Page/美國國旗.md "wikilink")[日本国旗](../Page/日本国旗.md "wikilink")。

一些日本人猜想帕劳国旗的设计刻意模仿了日本国旗，但设计者John Blau Skebong否认了这一说法。\[1\]

## 歷史上使用過的旗幟

<File:Flag> of Spain (1785-1873 and
1875-1931).svg|[西班牙帝國](../Page/西班牙帝國.md "wikilink")[西屬東印度](../Page/西屬東印度.md "wikilink")
1785-1873 及 1875-1931 <File:Flag> of the German Empire.svg|
[德意志殖民帝國](../Page/德意志殖民帝國.md "wikilink") 1899–1914
Merchant flag of Japan (1870).svg|[大日本帝國](../Page/大日本帝國.md "wikilink")
1914–1944 <File:US> flag 48
stars.svg|[美國](../Page/美國.md "wikilink")[國際聯盟託管地](../Page/國際聯盟託管地.md "wikilink")
1944–1959 <File:US> flag 49
stars.svg|[美國](../Page/美國.md "wikilink")[聯合國託管地](../Page/聯合國.md "wikilink")
1959–1960 <File:Flag> of the United States (Pantone).svg|
[太平洋群島託管地](../Page/太平洋群島託管地.md "wikilink")1960–1994
<File:Flag> of the Trust Territory of the Pacific
Islands.svg|[太平洋群島託管地](../Page/太平洋群島託管地.md "wikilink")
1965–1981 <File:Flag> of the United
Nations.svg|[太平洋群島託管地](../Page/太平洋群島託管地.md "wikilink")、[聯合國旗](../Page/聯合國旗.md "wikilink")
1947–1965

## 外部链接

  - [官方的解說](https://web.archive.org/web/20060815161815/http://www.palauconcon2.com/_doc/Palau%20flag%20description.pdf)

[P](../Category/国旗.md "wikilink")
[Category:帛琉](../Category/帛琉.md "wikilink")
[Category:1981年面世的旗幟](../Category/1981年面世的旗幟.md "wikilink")

1.  [パラオ国旗の作者との対話](http://blog.canpan.info/fukiura/archive/7313).