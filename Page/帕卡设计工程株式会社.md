**帕卡设计工程株式会社**（英文：Parker Engineering
Co.,Ltd.）是一家[日本涂装设备制造公司](../Page/日本.md "wikilink")，总部位于日本[东京](../Page/东京.md "wikilink")[中央区](../Page/中央區_\(東京都\).md "wikilink")[日本桥](../Page/日本橋_\(東京\).md "wikilink")。由日本帕卡濑精株式会社（[Nihon
Parkerizing
Co.,Ltd.](../Page/Nihon_Parkerizing_Co.,Ltd..md "wikilink")）全额出资。

## 概要

**主要业务**

  - 金属表面处理设备和涂装设备的设计·制造·安装·施工及售后服务
  - 经营各种过滤器
  - 设计、制造和开发环保类型涂装设备

## 沿革

  - 1951年：成立东和通商株式会社。
    荣获喷射式磷酸盐处理装置技术(Spra-bonde equipment)专利
    开始销售日本帕卡濑精株式会社([Nihon Parkerizing
    Co.,Ltd.](../Page/Nihon_Parkerizing_Co.,Ltd..md "wikilink"))生产的药品。在这期间、把发展重心转移到了喷射式磷酸盐处理装置(Spra-bonde
    equipment)的设计制造上。
  - 1960年：加盟日本帕卡濑精集团公司([Nihon Parkerizing
    Co.,Ltd.](../Page/Nihon_Parkerizing_Co.,Ltd..md "wikilink"))旗下，开始从事以喷射式磷酸盐处理装置(Spra-bonde
    equipment)为代表的各种涂装设备、[洗涤设备](../Page/洗涤.md "wikilink")、[干燥设备的](../Page/干燥.md "wikilink")[设计和](../Page/设计.md "wikilink")[制造](../Page/制造.md "wikilink")。
    更名为新东和通商工业株式会社。
    引进欧美技术，主要业务从涂装前处理装置拓展到综合涂装机械设备的制造。
    与日本帕卡濑精株式会社名古屋工厂合并，更名为帕卡产业株式会社。
  - 1986年：更名为帕卡设计工程株式会社，沿用至今
  - 1987年：在美国[伊利诺州设立PE](../Page/伊利诺州.md "wikilink") OF
    AMERICA,INC.(1999年转让给Trutec Industries,Inc.)
    在[台湾](../Page/台湾.md "wikilink")・[台北市成立台湾派可股份有限公司](../Page/台北市.md "wikilink")

<!-- end list -->

  - 1988年：在[泰国](../Page/泰国.md "wikilink")・[曼谷成立分公司](../Page/曼谷.md "wikilink")
    PARKER ENGINEERING（THAILAND）CO.,LTD.
  - 1995年：在[中国](../Page/中国.md "wikilink")・[上海设立办事处](../Page/上海.md "wikilink")。
  - 1996年：在[印度尼西亚](../Page/印度尼西亚.md "wikilink")・[雅加达设立分公司](../Page/雅加达.md "wikilink")
    PT.PARKER ENGINEERING INDONESIA
  - 1997年：在[泰国](../Page/泰国.md "wikilink")・[曼谷成立第二家分公司](../Page/曼谷.md "wikilink")
    PET TRADING CO., LTD.
  - 2002年：在[中国](../Page/中国.md "wikilink")・[上海成立帕柯工业设备](../Page/上海.md "wikilink")(上海)有限公司
  - 2005年：在[印度](../Page/印度.md "wikilink")・[哈里亚纳邦州](../Page/哈里亚纳邦.md "wikilink")[Gurgaon市设立分公司](../Page/Gurgaon.md "wikilink")
    PARKER ENGINEERING(INDIA)PVT.LTD
  - 2006年：台湾派可股份有限公司与中日金属化工有限公司合并，后者为续存公司。

## 外部链接

  - [パーカーエンジニアリング株式会社](http://www.parker-eng.co.jp/)
  - [日本パーカライジング株式会社](http://www.parker.co.jp/)

[Category:中央區公司 (東京都)](../Category/中央區公司_\(東京都\).md "wikilink")