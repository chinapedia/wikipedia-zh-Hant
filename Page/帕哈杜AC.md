**帕哈杜竞技俱乐部**（**Paradou AC**）
是位于[阿尔及利亚首都](../Page/阿尔及利亚.md "wikilink")[阿尔及尔的足球俱乐部](../Page/阿尔及尔.md "wikilink")。球队的主体育场为奥哈·马哈马迪体育场(Omar
Hammadi Stadium)。

俱乐部在1994年从海德拉竞技青年队中分离出的球队，俱乐部队服色为蓝色和黄色。球队的荣誉包括2004/05赛季的阿尔及利亚第二级联赛冠军。

## 球队荣誉

  - 阿尔及利亚国家锦标赛乙组 :
      - 2005年

## 外部链接

  - [Official Site](http://www.paradouac.com)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")