**帕尔杜比采**（[捷克语](../Page/捷克语.md "wikilink")：****，德语：Pardubitz，[国际音标](../Page/国际音标.md "wikilink")：）是[捷克共和国中部的一个城市](../Page/捷克共和国.md "wikilink")，[帕尔杜比采州首府](../Page/帕尔杜比采州.md "wikilink")，位于[布拉格以东](../Page/布拉格.md "wikilink")104千米的[易北河畔](../Page/易北河.md "wikilink")。帕尔杜比采是一个工业城市，[塑膠炸藥](../Page/塑膠炸藥.md "wikilink")[Semtex制造商Synthesia](../Page/Semtex.md "wikilink")、[台湾](../Page/台湾.md "wikilink")[电子专业制造服务厂商](../Page/电子专业制造服务.md "wikilink")[富士康公司都有在当地设厂](../Page/富士康.md "wikilink")。
[Foxconn_Pardubice.JPG](https://zh.wikipedia.org/wiki/File:Foxconn_Pardubice.JPG "fig:Foxconn_Pardubice.JPG")

## 友好城市

  - [荷兰](../Page/荷兰.md "wikilink")[杜廷赫姆](../Page/杜廷赫姆.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")[梅拉诺](../Page/梅拉诺.md "wikilink")

  - [保加利亚](../Page/保加利亚.md "wikilink")[佩尔尼克](../Page/佩尔尼克.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[舍纳贝克](../Page/舍纳贝克.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[塞尔布](../Page/塞尔布.md "wikilink")

  - [斯洛文尼亚](../Page/斯洛文尼亚.md "wikilink")[塞扎纳](../Page/塞扎纳.md "wikilink")

  - [瑞典](../Page/瑞典.md "wikilink")[谢莱夫特奥](../Page/谢莱夫特奥.md "wikilink")

  - [斯洛伐克](../Page/斯洛伐克.md "wikilink")[上塔特拉](../Page/上塔特拉.md "wikilink")

  - [比利时](../Page/比利时.md "wikilink")[瓦勒海姆](../Page/瓦勒海姆.md "wikilink")

## 外部链接

  - [帕尔杜比采官方网站](https://web.archive.org/web/20070928090120/http://www.pardubice.cz/xbuilder.php?xb1=ENPceCity)
  - [帕尔杜比采大学](http://upce.cz/)

[P](../Category/捷克城市.md "wikilink")