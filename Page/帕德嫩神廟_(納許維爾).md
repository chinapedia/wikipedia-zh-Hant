纳什维尔的帕特农**神廟**（**Parthenon**）是一座对[古希腊](../Page/古希腊.md "wikilink")[帕德嫩神廟的完整](../Page/帕德嫩神廟.md "wikilink")1:1複製建筑，坐落于[美國](../Page/美國.md "wikilink")[田纳西州第二大城市](../Page/田纳西州.md "wikilink")[纳什维尔](../Page/纳什维尔_\(田纳西州\).md "wikilink")，建於1897年，当年是建筑群的一部分。\[1\]

## 历史

在1897年該地舉辦一場博覽會，為了讓納許維爾配上“南方雅典”這個稱號，有人號召來興建這建築物。當時，為了博覽會，建了一系列仿古的建築物。然而其中只有這座帕德嫩神廟是講求正確復原的再製品，也是唯一保留至今的。

當初原始的建材是：[石膏](../Page/石膏.md "wikilink")、[木頭](../Page/木.md "wikilink")、[磚頭](../Page/磚.md "wikilink")，之後於1920年代在相同的基礎上以[混凝土重建](../Page/混凝土.md "wikilink")。

## 现今

今日，帕德嫩神廟被當作一間藝術博物館，為[納許維爾城郊外的](../Page/納許維爾.md "wikilink")的核心建築。

## 復原的雅典娜巨像

[缩略图](https://zh.wikipedia.org/wiki/File:Athena_Parthenos_LeQuire.jpg "fig:缩略图")
雕刻家在1990年，重製了這尊[雅典娜神像](../Page/雅典娜·帕德嫩.md "wikilink")（希臘帕德嫩神廟原來的[雅典娜神像早已毀於數百年前的入侵者](../Page/雅典娜.md "wikilink")）是該館近年來為人矚目的焦點。

這個巨大雕像，是雅典原物的全尺寸再製品，這個雅典娜的雕像，立基於對古代消失的原始神像的細心學術考證研究：

  - 雅典娜身穿胸甲、頭戴頭盔
  - 左手扶一盾，在身體與盾間有一隻大蛇
  - 右手捧勝利女神的雕像，有42呎高

這座雕像完成後，為了儘可能逼近原物，還上了彩漆、全身鍍上總重約八磅的金箔。不過，如此復原古雅典人當年可能看到的神廟內景，比起[雅典衛城原址殘破的帕德嫩神廟是不是較好的呈現方式](../Page/雅典衛城.md "wikilink")？目前還是未定論的。

## 外部連結

  - [Parthenon website](http://www.nashville.gov/parthenon/)
  - \[<http://memory.loc.gov/cgi-bin/query/S?ammem/pan:@field(SUBJ+@od1(Parthenon)>)
    Panoramic photograph of the Parthenon published in 1909\]

[Category:田納西州建築物](../Category/田納西州建築物.md "wikilink")
[Category:美国艺术博物馆](../Category/美国艺术博物馆.md "wikilink")
[Category:田納西州文化](../Category/田納西州文化.md "wikilink")
[Category:纳什维尔](../Category/纳什维尔.md "wikilink")

1.