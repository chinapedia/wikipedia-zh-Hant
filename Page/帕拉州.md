**帕拉**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：****，）是[巴西北部的一个](../Page/巴西.md "wikilink")[州](../Page/巴西行政区划.md "wikilink")，首府[贝伦
(巴西)](../Page/贝伦_\(巴西\).md "wikilink")。与帕拉州相邻的巴西州份有[阿马帕州](../Page/阿马帕州.md "wikilink")、[马拉尼昂州](../Page/马拉尼昂州.md "wikilink")、[托坎廷斯州](../Page/托坎廷斯州.md "wikilink")、[南马托格罗索州](../Page/南马托格罗索州.md "wikilink")、[亚马孙州和](../Page/亚马孙_\(巴西州份\).md "wikilink")[罗赖马州](../Page/罗赖马州.md "wikilink")。
该州与圭亚那，苏里南接壤

## 外部链接

  - [帕拉州政府网站](http://www.pa.gov.br)

[\*](../Category/帕拉州.md "wikilink")
[Category:巴西州份](../Category/巴西州份.md "wikilink")