**帕米爾高原戰斗**（也稱**2007年新疆搜捕行動**），發生于[2007年](../Page/2007年.md "wikilink")[1月5日](../Page/1月5日.md "wikilink")，[中國](../Page/中國.md "wikilink")[新疆](../Page/新疆.md "wikilink")[公安機關在南疆](../Page/公安機關.md "wikilink")[阿克陶县](../Page/阿克陶县.md "wikilink")——[帕米爾高原臨近](../Page/帕米爾高原.md "wikilink")[阿富汗和](../Page/阿富汗.md "wikilink")[巴基斯坦邊境上突擊摧毀一個](../Page/巴基斯坦.md "wikilink")[東突厥斯坦伊斯蘭運動](../Page/東突厥斯坦伊斯蘭運動.md "wikilink")「武裝分子」訓練營。\[1\]

據新疆公安厅新闻发言人巴燕说，在1月5日的搜捕过程中，这伙「暴力恐怖分子」进行武裝抵抗，公安部隊进行还击。戰斗中，一名新疆公安民警（黃強）陣亡，一名公安民警受伤。公安部隊击毙東伊運成員18名，捕获17名，缴获自制手雷22枚，半成品手雷1500多枚。\[2\]\[3\]\[4\]

流亡海外的維吾爾人領袖紛紛質疑這次突擊行動的動機。著名維吾爾人權活動家[熱比婭·卡德爾要求聯合國成立獨立調查委員會調查這一事件](../Page/熱比婭·卡德爾.md "wikilink")，并重申新疆境內是存在反對中國政府的維吾爾組織，但不是恐怖組織。\[5\][世界維吾爾大會主席Alim](../Page/世界維吾爾大會.md "wikilink")
Seytoff譴責中方始終未有拿出任何證據證明「營地」與[恐怖主義有關](../Page/恐怖主義.md "wikilink")。對此，新疆公安部反恐局副局长赵永琛反復強調該訓練營存在恐怖威脅。\[6\]\[7\]
[世界維吾爾代表大會議長](../Page/世界維吾爾代表大會.md "wikilink")[熱比婭·卡德爾](../Page/熱比婭·卡德爾.md "wikilink")\[8\]\[9\]\[10\]\[11\]要求“聯合國成立獨立調查委員會調查這一事件，并重申新疆境內是存在反對中國政府的維吾爾組織，但不是恐怖組織”。但其言论或带有分裂国家目的，煽动民粹等企图。

## 参考文献

{{-}}

[Category:2007年中国事件](../Category/2007年中国事件.md "wikilink")
[Category:2000年代新疆](../Category/2000年代新疆.md "wikilink")
[Category:新疆反恐行动](../Category/新疆反恐行动.md "wikilink")
[Category:新疆维吾尔自治区公安厅](../Category/新疆维吾尔自治区公安厅.md "wikilink")
[Category:東突厥斯坦獨立運動](../Category/東突厥斯坦獨立運動.md "wikilink")
[Category:克孜勒苏历史](../Category/克孜勒苏历史.md "wikilink")
[Category:阿克陶县](../Category/阿克陶县.md "wikilink")
[Category:2007年1月](../Category/2007年1月.md "wikilink")

1.  [UN urged to probe killing of Chinese
    Muslims](http://www.thenews.com.pk/daily_detail.asp?id=38523) The
    News

2.  [新疆公安机关捣毁一恐怖分子训练基地](http://news.xinhuanet.com/legal/2007-01/08/content_5580446.htm)
    [中新社](../Page/中新社.md "wikilink")

3.
4.  [China 'anti-terror' raid
    kills 18](http://news.bbc.co.uk/2/hi/asia-pacific/6241073.stm)
    [BBC](../Page/BBC.md "wikilink")

5.  [热比娅:中国突袭东突营地令人怀疑](http://news.bbc.co.uk/chinese/simp/hi/newsid_6250000/newsid_6250100/6250105.stm)
    [英國廣播公司](../Page/英國廣播公司.md "wikilink")

6.
7.  [China crushes Xinjiang \`terror
    camp'](http://www.taipeitimes.com/News/world/archives/2007/01/10/2003344141)
    臺北時報

8.  [諾貝爾和平獎熱比婭獲邀觀禮](http://news.rti.org.tw/index_newsContent.aspx?nid=270605)
    , [中央廣播電臺](../Page/中央廣播電臺.md "wikilink") Radio Taiwan International,
    2010/12/3

9.  [疆獨鬥士熱比婭
    獲諾貝爾和平獎提名](http://www.libertytimes.com.tw/2006/new/sep/12/today-int4.htm)
    , [自由時報](../Page/自由時報.md "wikilink"), 2006年9月12日

10. [Fighting for her peoples’ rights: Rebiya Kadeer visits
    Australia](http://www.amnesty.org.au/china/comments/9233),
    [國際特赦組織](../Page/國際特赦組織.md "wikilink"), February 2008

11. [Visitor Kadeer Calls For Action to Help Uyghur
    People](http://tech.mit.edu/V127/N27/kadeer.html), The Tech - MIT's
    Newspaper, June 8, 2007