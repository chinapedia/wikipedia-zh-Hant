**帖木兒帝國**（1370年－1507年）是[渴石地区的](../Page/渴石.md "wikilink")[突厥化](../Page/突厥化.md "wikilink")[蒙古人](../Page/蒙古人.md "wikilink")[巴鲁剌思氏之貴族](../Page/巴鲁剌思氏.md "wikilink")[帖木兒](../Page/帖木兒.md "wikilink")（1370年－1405年在位）所創之帝國。

建國者帖木兒本為渴石地区的突厥巴鲁剌思氏的[埃米尔](../Page/埃米尔.md "wikilink")，后来迎娶了[东察合台汗国王室](../Page/东察合台汗国.md "wikilink")[黑的儿火者的女儿为妃](../Page/黑的儿火者.md "wikilink")。由於是[黑的兒火者](../Page/黑的兒火者.md "wikilink")，在人們眼中是[成吉思汗的後裔](../Page/成吉思汗.md "wikilink")，所以帖木兒本人获得了政权的合法性。在文化上，帖木兒家族信仰[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，除武功顯赫，還特別注重文教建設。在首都撒馬爾罕創立[大學](../Page/大學.md "wikilink")，漸取代[巴格達的文化地位](../Page/巴格達.md "wikilink")，成為[穆斯林的學術中心](../Page/穆斯林.md "wikilink")。

帖木兒帝國征服了东察合台汗国、[河中地区](../Page/河中地区.md "wikilink")、[花剌子模](../Page/花剌子模.md "wikilink")、[美索不达米亚](../Page/美索不达米亚.md "wikilink")、[小亚细亚](../Page/小亚细亚.md "wikilink")、高加索和[大伊朗地区](../Page/大伊朗.md "wikilink")，並與[奧斯曼帝國交戰](../Page/奧斯曼帝國.md "wikilink")，企圖復興[蒙古帝国](../Page/蒙古帝国.md "wikilink")，但最後因帖木儿於東征[中国](../Page/中国.md "wikilink")[明朝的期間](../Page/明朝.md "wikilink")（1404年11月27日－1405年2月18日）逝世而被迫中斷。帝國的鼎盛時期橫亙從[小亚细亚到](../Page/小亚细亚.md "wikilink")[印度](../Page/印度.md "wikilink")[德里的](../Page/德里.md "wikilink")[西亞](../Page/西亞.md "wikilink")、[中亞和](../Page/中亞.md "wikilink")[南亞和](../Page/南亞.md "wikilink")[费尔干纳盆地](../Page/费尔干纳盆地.md "wikilink")，北起[锡尔河和](../Page/锡尔河.md "wikilink")[咸海](../Page/咸海.md "wikilink")，南及[阿拉伯海和](../Page/阿拉伯海.md "wikilink")[波斯湾的大帝国](../Page/波斯湾.md "wikilink")。

首都原為[撒馬爾罕](../Page/撒馬爾罕.md "wikilink")，其四子[沙哈鲁即位后遷都到今](../Page/沙哈鲁.md "wikilink")[阿富汗西北部的](../Page/阿富汗.md "wikilink")[赫拉特](../Page/赫拉特.md "wikilink")。在統治期間，統治者死亡後王位爭奪的情況相當頻繁，[卜撒因曾短暫統一](../Page/卜撒因.md "wikilink")，但死後國家又處於分裂狀態。1506年，帝國亡於突厥[烏茲別克人部落](../Page/烏茲別克人.md "wikilink")。其後裔[巴卑尔轉而去](../Page/巴卑尔.md "wikilink")[印度次大陆開創了](../Page/印度次大陆.md "wikilink")[蒙兀兒帝国](../Page/蒙兀兒帝国.md "wikilink")，至第六代皇帝[奧朗则布去世后开始衰敗](../Page/奧朗则布.md "wikilink")。1857年，末任皇帝[巴哈杜爾沙二世被](../Page/巴哈杜爾沙二世.md "wikilink")[東印度公司罷黜](../Page/東印度公司.md "wikilink")，蒙兀兒帝國滅亡。帖木兒帝國和蒙兀兒帝国皆由[帖木儿王朝统治](../Page/帖木儿王朝.md "wikilink")。

## 历任君主

  - [帖木兒](../Page/帖木兒.md "wikilink")：1370年－1405年在位
  - [皮儿·马黑麻](../Page/皮儿·马黑麻.md "wikilink")：1405年－1407年在位；[哈利勒蘇丹](../Page/哈利勒蘇丹.md "wikilink")：1405年－1409年在位（和[皮儿·马黑麻争位并于](../Page/皮儿·马黑麻.md "wikilink")1407年击败对手。）
  - [沙哈鲁](../Page/沙哈鲁.md "wikilink")：1409年－1447年在位
  - [兀鲁伯](../Page/兀鲁伯.md "wikilink")：1447年－1449年在位（統治帝国大部）；[穆罕默德本拜宋豁兒](../Page/穆罕默德本拜宋豁兒.md "wikilink")：1447年－1451年在位（与[兀鲁伯争位](../Page/兀鲁伯.md "wikilink")，統治[法爾斯](../Page/法爾斯.md "wikilink")）
  - [阿不都·剌迪甫](../Page/阿不都·剌迪甫.md "wikilink")：1449年－1450年在位（統治[河中](../Page/河中.md "wikilink")）；[阿布勒－卡西姆·巴布爾·本·拜宋豁兒](../Page/阿布勒－卡西姆·巴布爾·本·拜宋豁兒.md "wikilink")：1449年－1457年在位（統治[赫拉特](../Page/赫拉特.md "wikilink")）
  - [阿卜杜拉·米爾扎](../Page/阿卜杜拉·米爾扎.md "wikilink")：1450年－1451年在位（統治[河中](../Page/河中.md "wikilink")）；[米兒咱·沙·馬赫穆德](../Page/米兒咱·沙·馬赫穆德.md "wikilink")：1457年在位（統治[赫拉特](../Page/赫拉特.md "wikilink")）
  - [卜撒因](../Page/卜撒因.md "wikilink")：1451年－1469年在位（統治[河中](../Page/河中.md "wikilink")），1459年后亦统治[赫拉特](../Page/赫拉特.md "wikilink")；[易卜拉欣·米尔扎](../Page/易卜拉欣·米尔扎.md "wikilink")：1457年－1459年在位（統治[赫拉特](../Page/赫拉特.md "wikilink")）
  - ***河中分裂***：[阿赫馬德米兒咱](../Page/阿赫馬德米兒咱.md "wikilink")：1469年—1494年在位（統治[撒馬爾罕](../Page/撒馬爾罕.md "wikilink")、[布哈拉](../Page/布哈拉.md "wikilink")、[希萨尔](../Page/希萨尔.md "wikilink")）、[烏瑪爾沙赫米兒咱二世](../Page/烏瑪爾沙赫米兒咱二世.md "wikilink")：1469年—1494年在位（統治[費爾干納谷地](../Page/費爾干納谷地.md "wikilink")）、[馬赫穆德·米爾扎](../Page/馬赫穆德·米爾扎.md "wikilink")：1469年—1495年在位（統治[巴尔赫](../Page/巴尔赫.md "wikilink")）、[兀鲁伯·米爾扎二世](../Page/兀鲁伯·米爾扎二世.md "wikilink")：1469年—1502年在位（統治[喀布爾](../Page/喀布爾.md "wikilink")）；[忽辛·拜哈拉](../Page/忽辛·拜哈拉.md "wikilink")：1469年－1470年及1470年－1506年两度在位（統治赫拉特）
  - ***河中分裂***：[苏丹·拜孙豁儿·米尔扎](../Page/苏丹·拜孙豁儿·米尔扎.md "wikilink")：1495年—1497年在位（統治[撒馬爾罕](../Page/撒馬爾罕.md "wikilink")）、[苏丹·阿里](../Page/苏丹·阿里.md "wikilink")：1495年-1497年在位（統治[布哈拉](../Page/布哈拉.md "wikilink")）、[苏丹·马苏德·米尔扎](../Page/苏丹·马苏德·米尔扎.md "wikilink")：1495年-？在位（統治[希萨尔](../Page/希萨尔.md "wikilink")）、[巴布尔](../Page/巴布尔.md "wikilink")：1494年—1497年在位（統治[費爾干納谷地](../Page/費爾干納谷地.md "wikilink")）、[库斯洛·沙伊赫](../Page/库斯洛·沙伊赫.md "wikilink")（篡位者）：？—1503年在位（統治[巴尔赫](../Page/巴尔赫.md "wikilink")）、[玛齐姆·别吉](../Page/玛齐姆·别吉.md "wikilink")（篡位者）：？—1504年在位（統治[喀布爾](../Page/喀布爾.md "wikilink")）；[雅迪格爾·穆罕默德](../Page/雅迪格爾·穆罕默德.md "wikilink")：1470年（統治赫拉特）
  - ***河中分裂***：[苏丹·阿里](../Page/苏丹·阿里.md "wikilink")：1497年-1500年在位（統治[撒馬爾罕和](../Page/撒馬爾罕.md "wikilink")[布哈拉](../Page/布哈拉.md "wikilink")）、[贾罕吉尔·米尔扎二世](../Page/贾罕吉尔·米尔扎二世.md "wikilink")（苏丹·艾哈迈德·塔布尔的傀儡）：1497年-？在位（統治[費爾干納谷地](../Page/費爾干納谷地.md "wikilink")）、[巴布尔](../Page/巴布尔.md "wikilink")：1503年—1504年在位（統治[巴尔赫和](../Page/巴尔赫.md "wikilink")[喀布爾](../Page/喀布爾.md "wikilink")）；[巴迪·匝曼与](../Page/巴迪·匝曼.md "wikilink")[穆扎法尔·忽辛共治](../Page/穆扎法尔·忽辛.md "wikilink")：1506年－1507年在位（統治赫拉特）
  - [乌兹别克统治者](../Page/乌兹别克.md "wikilink")[穆罕默德·昔班尼](../Page/穆罕默德·昔班尼.md "wikilink")：1500年-1501年統治[撒馬爾罕](../Page/撒馬爾罕.md "wikilink")、[布哈拉](../Page/布哈拉.md "wikilink")、[希萨尔](../Page/希萨尔.md "wikilink")，1503年-1504年吞并[費爾干納谷地和](../Page/費爾干納谷地.md "wikilink")[巴尔赫](../Page/巴尔赫.md "wikilink")，统一[河中](../Page/河中.md "wikilink")；[巴布尔](../Page/巴布尔.md "wikilink")：1504年—1511年在位（仅统治[喀布爾](../Page/喀布爾.md "wikilink")）
  - [巴布尔](../Page/巴布尔.md "wikilink")：1511年—1512年在位（短暂统一），1512年再次被乌兹别克人击败。

## 文化

帖木儿帝国的官方语言是[突厥语](../Page/突厥语.md "wikilink")，民间语言是[波斯语和](../Page/波斯语.md "wikilink")[阿拉伯語等等](../Page/阿拉伯語.md "wikilink")。

## 参考文献

{{-}}

[帖木儿帝国](../Category/帖木儿帝国.md "wikilink")
[Category:突厥](../Category/突厥.md "wikilink")
[Category:中亞歷史](../Category/中亞歷史.md "wikilink")
[Category:伊朗历史](../Category/伊朗历史.md "wikilink")