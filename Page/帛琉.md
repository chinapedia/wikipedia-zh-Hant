**帕劳共和国**（；[帕勞語](../Page/帕勞語.md "wikilink")：Beluu er a
Belau；），通稱**帕劳**（，旧称或），也称**-{zh-cn:帛琉;zh-hk:帕勞;zh-tw:帕勞;zh-mo:帕勞}-**，是位于西[太平洋的](../Page/太平洋.md "wikilink")[岛嶼国家](../Page/島嶼國家.md "wikilink")。全国有约340座岛屿，属于[密克罗尼西亚群岛中](../Page/密克罗尼西亚群岛.md "wikilink")[加罗林群岛的西链](../Page/加罗林群岛.md "wikilink")，总面积为466平方公里\[1\]。人口最多的岛屿是[科罗尔](../Page/科羅爾.md "wikilink")。首都[恩吉鲁穆德位于临近岛屿](../Page/恩吉鲁穆德.md "wikilink")[巴伯尔道布岛](../Page/巴伯尔道布岛.md "wikilink")，隶属于[梅莱凯奥克州](../Page/梅莱凯奥克州.md "wikilink")。帕劳的海上邻国包括[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[菲律宾和](../Page/菲律宾.md "wikilink")[密克罗尼西亚联邦](../Page/密克罗尼西亚联邦.md "wikilink")。

约3,000年以前，来自菲律宾的移民最先在这里定居；直到约900年以前，当地人种都属于[尼格利陀人](../Page/尼格利陀人.md "wikilink")。群岛在16世纪初被欧洲人发现，1574年成为[西属东印度群岛的一部分](../Page/西屬東印度群島.md "wikilink")。1898年，西班牙在[美西战争中被击败](../Page/美西战争.md "wikilink")；1899年，根据德国－西班牙条约，群岛被出售予[德意志帝国](../Page/德意志帝國.md "wikilink")，隶属于[德属新几内亚](../Page/德屬新幾內亞.md "wikilink")。[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，帕劳被[日本帝国海军占领](../Page/大日本帝國海軍.md "wikilink")，随后被[国际联盟归入日治](../Page/國際聯盟.md "wikilink")[南洋群岛](../Page/南洋群岛.md "wikilink")。[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，作为[马里亚纳群岛及帕劳战事的一部分](../Page/馬里亞納群島及帛琉戰事.md "wikilink")，美军与日军在此进行过多次小规模战斗，包括主要的[佩莱利乌战役](../Page/佩莱利乌战役.md "wikilink")。1947年，帕劳同其它几个[太平洋群岛一起成为美属](../Page/太平洋三大島群.md "wikilink")[太平洋群岛托管地的一部分](../Page/太平洋群島託管地.md "wikilink")。1979年，帕劳公投反对加入密克罗尼西亚联邦；根据与美国之间的[自由联合协定](../Page/自由联合协定.md "wikilink")，帕劳在1994年完全独立。

## 歷史

1783年，[葡萄牙人發現帛琉](../Page/葡萄牙人.md "wikilink")，但不久後被[西班牙人統治](../Page/西班牙人.md "wikilink")。1899年由[西班牙售予](../Page/西班牙.md "wikilink")[德国](../Page/德国.md "wikilink")。[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，被[日本以](../Page/日本.md "wikilink")[C類託管地進行託管](../Page/國際聯盟託管地#第三等託管地.md "wikilink")。[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")，被盟軍攻佔。戰後於1947年由[聯合國授權](../Page/聯合國.md "wikilink")[美國以](../Page/美國.md "wikilink")[太平洋群島託管地進行託管](../Page/太平洋群島託管地.md "wikilink")，1981年獲准成立自治政府，稱為「帕劳共和國」，並制定禁止貯藏及攜入[核武之](../Page/核武.md "wikilink")「非核憲法」。

1986年美國以15年內分期提供4億5千萬美元經濟援助及准許其獨立為條件與帛琉簽訂為期50年之「[自由聯合協定](../Page/自由聯合.md "wikilink")」（Compact
of Free
Association），依據該協定帛琉享有內政自治權並得與他國政府、區域暨國際組織簽訂條約及協定，涉外事務需與美方進行協商，國防亦由[美軍協防](../Page/美軍.md "wikilink")，美方還有權派遣[核子軍艦及](../Page/核子動力潛艇.md "wikilink")[軍機至帕劳領域](../Page/軍機.md "wikilink")，且無義務承認或否認在帕劳部署核武；此一條件有違帛琉憲法中「非核條款」，故要求帛琉暫時凍結此一憲法條款。此協定先後7次遭到帕劳公民投票否決，直至1993年11月第8次公民投票始獲通過，帕劳遂於1994年10月1日正式獨立。

## 史前遺址

最早期的帛琉人可能來自[波里尼西亞和](../Page/波里尼西亞.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。從該地各家族的起源傳說可以看出，帛琉人可能混雜了[美拉尼西亞](../Page/美拉尼西亞.md "wikilink")、[密克羅尼西亞和波里尼西亞的人種](../Page/密克羅尼西亞群島.md "wikilink")。因此帛琉人在嚴謹的定義上，並非歸屬於密克羅尼西亞人（[南島民族](../Page/南島民族.md "wikilink")）。

這個島群早期被認為是「黑色群島」，在[澳大利亚在線圖書館](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0001780)中可以找到早期的地圖和村落的描繪，也可以找到早期[科羅州大酋長Ibedul有著](../Page/科羅州.md "wikilink")[紋身和耳洞裝飾的照片](../Page/紋身.md "wikilink")。

近年的研究中，從[<sup>14</sup>C的定年結果和出土的考古證據](../Page/碳-14.md "wikilink")，把這個島群的歷史帶到了新的面向。在島上發現的墓葬遺址顯示，帛琉應該是整個[大洋洲最早具有喪葬儀式的地方](../Page/大洋洲.md "wikilink")。但是對於帛琉人何時定居在此有兩種說法，一個說法是西元前2500年，另一個則是西元前1000年。

在爪哇，這個遠古時期跟帛琉有貿易往來關係的地區，曾經發現了[佛羅勒斯人](../Page/佛羅勒斯人.md "wikilink")。2007年，有考古學者在帛琉的洛克群島發現了疑似佛羅勒斯人的矮小體型的人骨遺留\[2\]。這個發現可能會推翻以往的假設，但是目前這個研究發現仍有爭議。

幾千年來，帛琉有著發達的[母系社會](../Page/母系社會.md "wikilink")，一般認為是從爪哇遷移來的族群所帶來的制度。傳統上，土地、貨幣、跟頭銜都由母系傳承。氏族所有地，由女性的族長掌管並傳給第一個女兒。但是當代也有實行[父系傳承制度](../Page/父系社會.md "wikilink")，這是在日本殖民帛琉時所傳入該社會。在二次大戰期間，[日本政府試圖把土地重新分配](../Page/日本政府.md "wikilink")，轉為個人所有權，並且也意圖革除舊有的制度。現在這些法律的衝突仍在許多家族中上演。

## 政治

### 行政

帛琉總統為國家及內閣組織的最高元首，每四年由帛琉人民選舉產生。

現任總統為[-{zh-cn:小汤米·雷门格绍;zh-tw:湯米·雷蒙傑索}-](../Page/湯米·雷蒙傑索.md "wikilink")，於2017年1月起擔任第4任期帛琉總統，任期4年。

內閣（2017-2021）設8部，分別為國務部、財政部、基建工商部、社區暨文化部、教育部、衛生部、司法部及自然資源環境暨觀光部，部長由總統任命並須經參議院同意。

### 立法

[帕劳国会实行](../Page/帕劳国会.md "wikilink")[兩院制](../Page/兩院制.md "wikilink")，稱為*Olbiil
Era Kelulau*，分參（Senate）、眾（House of Delegates）兩院。

參議員共13名，按人口比例由全國選區（Senatorial
District）選出，眾議員共16名，由16個行政區各選出1名，兩院議員任期均為4年。

現為第十屆（2017-2021），於2016年11月1日改選，於2017年1月19日就職。

## 外交

帛琉共和国现已與79個國家及政治體建立[外交关系](../Page/外交关系.md "wikilink")，详见[帕劳建交列表](../Page/帕劳建交列表.md "wikilink")。並在[美國華盛頓與紐約](../Page/美國.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[菲律賓等地設立](../Page/菲律賓.md "wikilink")[外交代表機構](../Page/外交代表機構.md "wikilink")；[中華民國](../Page/中華民國.md "wikilink")、[日本及](../Page/日本.md "wikilink")[美國也在帛琉設館](../Page/美國.md "wikilink")，並派駐大使。

### 與日本關係

帛琉共和国與日本關係密切，第一次世界大戰期間，帛琉曾被[日本帝國海軍占領](../Page/日本帝國海軍.md "wikilink")，隨後被國際聯盟歸入日治[南洋群島](../Page/南洋群島.md "wikilink")。[日本戰敗](../Page/日本戰敗.md "wikilink")，帛琉獨立後，曾選出日裔的[中村國雄任該國總統](../Page/中村國雄.md "wikilink")。

### 與中華民國關係

帛琉共和國與中華民國政府的建交，引發中華人民共和國施壓，發出旅遊禁令。2017年12月30日帛琉總統表示，目前並不打算改變和台灣的外交關係，在外交上將會有自己的決定。

### 與中華人民共和國關係

兩國現時無邦交。中國公民前往帛琉可以通過[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[首爾](../Page/首爾.md "wikilink")、[台北等中轉站去帛琉](../Page/台北.md "wikilink")。目前中國政府已經發布旅遊禁令禁止所有中國旅行團前往帛琉旅遊，但[自由行旅客則不受影響](../Page/自由行.md "wikilink")\[3\]。

## 行政區劃

[States_of_Palau.jpg](https://zh.wikipedia.org/wiki/File:States_of_Palau.jpg "fig:States_of_Palau.jpg")
[Koror-palau-downtown20071219.jpg](https://zh.wikipedia.org/wiki/File:Koror-palau-downtown20071219.jpg "fig:Koror-palau-downtown20071219.jpg")

帛琉全國共分為16個州。除了最大的主島群分成14州之外，餘下的堡礁以南北再分為2州。這16個州名稱如下：（以英文字母排列）

## 地理

[Orthographic_projection_centred_over_Palau.png](https://zh.wikipedia.org/wiki/File:Orthographic_projection_centred_over_Palau.png "fig:Orthographic_projection_centred_over_Palau.png")
[Palau-map.png](https://zh.wikipedia.org/wiki/File:Palau-map.png "fig:Palau-map.png")

帛琉由一個大[堡礁和無數小島和較少堡礁構成](../Page/堡礁.md "wikilink")。主島本身其實也由多個小堡礁構成，較大的有[安加爾](../Page/安加爾.md "wikilink")、Babeldaob（Babelthuap）、Koror、Peleliu。亦因為當地有大量珊瑚，故其有大量是由海洋生物的屍體沉降累積而成的石灰岩，甚至成為其附近[雅浦島](../Page/雅浦島.md "wikilink")，[雅浦島石幣的制作原料](../Page/雅浦島石幣.md "wikilink")。全國有三分之二人口在舊都[科羅爾居住](../Page/科羅爾.md "wikilink")。主島群以北是Ngeruangel和Kayangel。主島以西是Rock
Islands，無人居住。西南部距離主島群約600公里的西南群島，也是帛琉的一部份。

以下是帛琉和其他地區的距離：

  - ：7,200公里

  - ：11,200公里

  - ：4,000公里

  - ：2,200公里

  - ：4,000公里

  - ：880公里

  - ：650公里

  - ：600公里

### 氣候

帛琉的氣候屬[熱帶雨林氣候](../Page/熱帶型雨林氣候.md "wikilink")，全年平均溫度約28.9℃。全年皆為雨季，平均全年降雨量為380厘米，平均濕度為82%。雖然降雨主要集中在7月到10月，但這段期間也經常陽光普照。帛琉偏離颱風主要路徑範圍，因此鮮少受到[熱帶氣旋影響](../Page/熱帶氣旋.md "wikilink")，僅有2013年[颱風海燕以極高強度直襲帛琉](../Page/颱風海燕_\(2013年\).md "wikilink")，所幸未在當地造成嚴重傷亡。

## 生态

在帛琉，热带雨林覆盖大多数的岛屿，有黑檀木、孟加拉榕树、面包树、椰子树、露兜树等，这些热带雨林形成绿意盎然的树海。同时帛琉还有野生红树林与热带大草原生态可以探访。

帛琉海底生物形形色色，场面壮观包括超过1,500种的多样鱼类、700种的珊瑚和海葵。其他值得一看的景观尚包括巨型蛤蜊、海龟、蝠魟鱼、灰礁鲨、海蛇、海牛等。靠近陆地之处，有入海口附近的鳄鱼、大蜥蝪。此外，尚可看到数十种鸟类、果蝠在洛克群岛的聚居地、在Angaur岛的猿猴、一些无毒的蛇类、小型蜥蝪及其他昆虫，而且在帛琉的陆地上，没有任何有毒的陆生动物。

帛琉政府指定了洛克群岛中70个无人居住的岛屿作为海洋生态保护区，禁止民众进入，以充分保护海龟与海鸟的生态环境。

## 經濟

[旅遊業是經濟的支柱](../Page/旅遊業.md "wikilink")，除此之外還有農業和漁業。在2000-2001財政年度，約有共50,000外國人來帛琉經商或觀光。政府是本國最主要的雇主，但國家經濟很大程度上依賴美國的經濟援助。儘管如此，人均國民收入是[菲律賓和大多數](../Page/菲律宾.md "wikilink")[密克羅尼西亞國家的兩倍](../Page/密克罗尼西亚群岛.md "wikilink")。帛琉的旅遊市場正在傾向於吸引更多來自[東亞的遊客](../Page/東亞.md "wikilink")，並希望外國投資可以幫助他們興建更完備的基礎設施。

## 人口

當地居民約20,000人，其中大部分為帛琉土著，屬[密克羅尼西亞人](../Page/密克罗尼西亚.md "wikilink")。少數族裔有來自菲律賓為主的亞洲人和少量歐洲人。三分之二的居民信奉[基督教](../Page/基督教.md "wikilink")，當中[天主教徒佔大多數](../Page/天主教.md "wikilink")，其餘的信奉其他[新教派系](../Page/新教.md "wikilink")。除了基督教外，當地一種叫*Modekngei*的地方宗教也十分流行。

當地官方語言為[英語和](../Page/英語.md "wikilink")[帛琉語](../Page/帛琉語.md "wikilink")。其中三個州有地方語言作為自己的官方語言。而在安加爾島上，[日語曾经是官方語言](../Page/日語.md "wikilink")。目前安加爾全岛人口80人，所有人讲英语。

## 交通

[Japan_Palau_Friendship_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Japan_Palau_Friendship_Bridge.jpg "fig:Japan_Palau_Friendship_Bridge.jpg")

帛琉全國共有3個機場，分別位於主島、貝里琉和安加爾島上。其中[帛琉國際機場為唯一的國際機場](../Page/帛琉國際機場.md "wikilink")，位於主島南部，埃拉伊州境内。
[帛琉太平洋航空](../Page/帛琉太平洋航空.md "wikilink")、[大韓航空](../Page/大韓航空.md "wikilink")、[達美航空和](../Page/達美航空.md "wikilink")[聯合航空等提供連接香港](../Page/聯合航空.md "wikilink")、東京、首爾、台北、高雄及關島等航班。

## 節日及假日

<TABLE class="wikitable" align=center cellpadding=2 cellspacing=0>

<TR>

<TH style="background: #efefef;">

日期

<TH style="background: #efefef;">

中文名称

<TH style="background: #efefef;">

当地名称

<TR>

<TD>

1月1日

<TD>

[新年](../Page/新年.md "wikilink")

<TD>

<TR>

<TD>

3月15日

<TD>

[青年节](../Page/青年节.md "wikilink")

<TD>

<TR>

<TD>

5月5日

<TD>

[老人节](../Page/老人节.md "wikilink")

<TD>

<TR>

<TD>

6月1日

<TD>

总统日

<TD>

<TR>

<TD>

7月9日

<TD>

[宪法日](../Page/宪法日.md "wikilink")

<TD>

<TR>

<TD>

9月的第一个星期一

<TD>

[劳动节](../Page/劳动节.md "wikilink")

<TD>

<TR>

<TD>

10月1日

<TD>

[独立节](../Page/独立节.md "wikilink")

<TD>

<TR>

<TD>

10月24日

<TD>

[联合国日](../Page/联合国日.md "wikilink")

<TD>

<TR>

<TD>

11月的最后一个星期四

<TD>

[感恩节](../Page/感恩节.md "wikilink")

<TD>

<TR>

<TD>

12月25日

<TD>

[圣诞节](../Page/圣诞节.md "wikilink")

<TD>

</TABLE>

## 二战历史

[First_wave_of_LVTs_moves_toward_the_invasion_beaches_-_Peleliu.jpg](https://zh.wikipedia.org/wiki/File:First_wave_of_LVTs_moves_toward_the_invasion_beaches_-_Peleliu.jpg "fig:First_wave_of_LVTs_moves_toward_the_invasion_beaches_-_Peleliu.jpg")\]\]

## 其他主題

  - [帛琉通訊](../Page/帛琉通訊.md "wikilink")
  - [帛琉交通](../Page/帛琉交通.md "wikilink")
  - [帛琉軍事](../Page/帛琉軍事.md "wikilink")
  - [帛琉外交](../Page/帛琉外交.md "wikilink")

## 注释

## 參考文獻

## 外部連結

  - [帛琉政府網站](https://www.palaugov.pw)

  - [帛琉政府觀光局](http://palaugov.pw/bureau-of-tourism/)

  -

  -

  -

## 參见

  - [帛琉太平洋航空](../Page/帛琉太平洋航空.md "wikilink")
  - [海洋師團](../Page/海洋師團.md "wikilink")
  - [聯繫邦](../Page/聯繫邦.md "wikilink")

{{-}}

[帛琉](../Category/帛琉.md "wikilink")

1.   Palau National Government |accessdate=2017-05-09 |website =
    palaugov.pw |language = en-US }}
2.  <http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0001780>
3.  <http://www.mafengwo.cn/gonglve/ziyouxing/172114.html>