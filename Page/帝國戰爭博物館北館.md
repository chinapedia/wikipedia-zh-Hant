[IWMN_the_big_picture.JPG](https://zh.wikipedia.org/wiki/File:IWMN_the_big_picture.JPG "fig:IWMN_the_big_picture.JPG")
**曼彻斯特帝国战争博物馆北馆**（Imperial War Museum
North）是一所位于[英国](../Page/英国.md "wikilink")[大曼彻斯特地区](../Page/曼彻斯特.md "wikilink")[特拉福德的](../Page/特拉福德.md "wikilink")[战争纪念](../Page/战争.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")，是[解构主义建筑的代表作品之一](../Page/解构主义建筑.md "wikilink")。曾获得2003年[英国营造业建筑奖](../Page/英国营造业建筑奖.md "wikilink")，常简称为**IWMN**，有时又译做**英国皇家战争博物馆北部分馆**。

曼彻斯特帝国战争博物馆在2002年7月5日开馆，设计师是[丹尼爾·李伯斯金](../Page/丹尼爾·李伯斯金.md "wikilink")，整个建造花费约为2800万[英镑](../Page/英镑.md "wikilink")。负责公司为[麦卡宾爵士建筑公司和英国营建和环境顾问公司](../Page/麦卡宾爵士建筑公司.md "wikilink")[奥雅纳](../Page/奥雅纳.md "wikilink")。

## 建筑

该馆的建筑远观时如同三片弧型碎片的组成物，两片躺于地上，一片竖起。李伯斯金曾在[回忆录上这样写着](../Page/回忆录.md "wikilink")：“我绞尽脑汁想传达这座建筑物的本质及意图展现的东西。这建筑与大英帝国无关，也与战争无关，而是关乎面对全球冲突永无止境的本质。我脑中出现一个地球散成碎片的意象，就在那时，我知道这座建筑应该长什么模样了。（[来源](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=rtpe\(SellItems\)&id=PE0322&p=excerpt&exid=36968)）”

近观或走入整个建筑造型，则其具有复杂的几何形组合，大角度倾斜的天花板和地板，垂直线也几乎都是倾斜的，只有几条直线。设计师表示这样的建筑概念来自于受过战争创伤、身体或[心理上造成障碍的一种隐喻](../Page/心理.md "wikilink")。

此博物馆的特色展览是｢大图｣（The Big
Picture）；每一小时，博物馆内的灯光会变暗，所有的墙壁会投射出关于战争的图片、引文和播放重要事件的录音，在大厅造成回声。这个特色展览设计的目的是使观者完全体会“在战争中失去勇气的感觉”。

## 现况

据博物馆网站声称，今日这座战争博物馆已成为英国讨论话题最高的博物馆之一，其参访人次已达1百万人。该馆的展出自1914年以来英国人民经历战争的历史为主，是[帝国战争博物馆的第五座分馆](../Page/帝国战争博物馆.md "wikilink")，同时也是在英国东南部以外的第一座战争博物馆。2004年，此馆是年度最具有吸引力的观光景点银奖得主，也是欧洲博物馆年度特别奖得主，同一年，英国皇家协会[史特灵建筑奖曾将此博物馆纳入决选名单](../Page/史特灵建筑奖.md "wikilink")，最后首奖颁给了[圣玛丽斧街30号](../Page/圣玛丽斧街30号.md "wikilink")。2006年，曼彻斯特帝国战争博物馆被授与曼彻斯特旅游业年度重大观光景点的头衔。

此馆免费开放入馆，馆内皆为[无障碍空间](../Page/无障碍空间.md "wikilink")，每日开放时间为早上10点到下午6点。（11月到隔年2月时，闭馆时间提早一小时为下五5点）

## 相关图片

<center>

[File:ImperialWarMuseumNorth02.jpg|此塔内部几为中空](File:ImperialWarMuseumNorth02.jpg%7C此塔内部几为中空)
[File:LibeskindSpaceFrameTower.jpg|塔内结构](File:LibeskindSpaceFrameTower.jpg%7C塔内结构)
<File:IWMN> canal entrance.JPG|入口 <File:IWMN> interior air
shard.JPG|支称结构的细节 <File:IWMN> detail.JPG|近看博物馆外观的细节

</center>

## 参考文献

## 外部链接

  - [英国皇家战争博物馆北部分馆](https://web.archive.org/web/20050131172550/http://north.iwm.org.uk/)
    – 官方网站 – 英文

[Category:英国博物馆](../Category/英国博物馆.md "wikilink")
[Category:解構主義建築](../Category/解構主義建築.md "wikilink")
[Category:2002年完工建築物](../Category/2002年完工建築物.md "wikilink")
[Category:军事博物馆](../Category/军事博物馆.md "wikilink")
[Category:丹尼爾·里伯斯金設計的建築物](../Category/丹尼爾·里伯斯金設計的建築物.md "wikilink")