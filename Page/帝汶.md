**帝汶**為[馬來群島南端的島嶼](../Page/馬來群島.md "wikilink")，屬於[南洋群島的一部分](../Page/南洋群島.md "wikilink")。東部為獨立國家[東帝汶](../Page/東帝汶.md "wikilink")，西部為[印度尼西亞](../Page/印度尼西亞.md "wikilink")[東努沙登加拉省的一部份](../Page/東努沙登加拉省.md "wikilink")（又稱[西帝汶](../Page/西帝汶.md "wikilink")）。

帝汶岛是[檀香的重要产地](../Page/檀香.md "wikilink")，很早就被[中国古籍提到](../Page/中国.md "wikilink")。《[诸番志](../Page/诸番志.md "wikilink")》称其为“底勿”，《[岛夷志略](../Page/岛夷志略.md "wikilink")》作“古里地闷”（“古”为“吉”之误，“吉里”意为“山”），《[星槎胜览](../Page/星槎胜览.md "wikilink")》作“吉里地闷”，《[东西洋考](../Page/东西洋考.md "wikilink")》译作“迟闷”和“池闷”，《[海岛逸志](../Page/海岛逸志.md "wikilink")》译作“知汶”，《[海录](../Page/海录.md "wikilink")》译作“地问”\[1\]。

## 相關條目

  - [葡萄牙殖民地](../Page/葡萄牙殖民地.md "wikilink")
  - [荷蘭王國](../Page/荷蘭王國.md "wikilink")
  - [荷蘭東印度公司](../Page/荷蘭東印度公司.md "wikilink")
  - [大帝汶](../Page/大帝汶.md "wikilink")

## 参考资料

[Category:印度洋島嶼](../Category/印度洋島嶼.md "wikilink")
[Category:小巽他群岛](../Category/小巽他群岛.md "wikilink")
[Category:印尼岛屿](../Category/印尼岛屿.md "wikilink")
[Category:東帝汶島嶼](../Category/東帝汶島嶼.md "wikilink")
[Category:跨国岛屿](../Category/跨国岛屿.md "wikilink")
[Category:分裂地區](../Category/分裂地區.md "wikilink")

1.