[Locatie_Timorzee.PNG](https://zh.wikipedia.org/wiki/File:Locatie_Timorzee.PNG "fig:Locatie_Timorzee.PNG")
**帝汶海**（[英語](../Page/英語.md "wikilink")：**Timor Sea**，，
）位于[帝汶岛与](../Page/帝汶岛.md "wikilink")[澳大利亚西北部之间](../Page/澳大利亚.md "wikilink")，西面是浩瀚的[印度洋](../Page/印度洋.md "wikilink")，东接[阿拉弗拉海](../Page/阿拉弗拉海.md "wikilink")。帝汶海南北宽约550公里，面积约61万平方公里，海中央有[博拿巴海盆](../Page/博拿巴海盆.md "wikilink")
，海盆东北有一狭谷与[帝汶海槽相连](../Page/帝汶海槽.md "wikilink")，平均海深约406米，最深处有3200米。帝汶海蕴藏有丰富的油气资源，是重要的[石油产区](../Page/石油.md "wikilink")。

這一片海域有許多的珊瑚礁，無人島和驚人的油氣儲備。

現在這裡被2009年的石油洩漏所污染，其影響可能會持續25年\[1\]。這裡也是印尼蘇拉威西省[望加錫人採集海參的傳統地方](../Page/望加錫人.md "wikilink")。

## 參考資料

<references/>

[Category:印度洋陆缘海](../Category/印度洋陆缘海.md "wikilink")
[Category:印度尼西亞海域](../Category/印度尼西亞海域.md "wikilink")
[Category:東帝汶海域](../Category/東帝汶海域.md "wikilink")
[Category:澳大利亞海域](../Category/澳大利亞海域.md "wikilink")
[Category:澳大利亞-印度尼西亞邊界](../Category/澳大利亞-印度尼西亞邊界.md "wikilink")
[Category:澳大利亞-東帝汶邊界](../Category/澳大利亞-東帝汶邊界.md "wikilink")
[Category:印度尼西亞-東帝汶邊界](../Category/印度尼西亞-東帝汶邊界.md "wikilink")

1.