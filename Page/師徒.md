**師徒**是指**師父**與**徒弟**的關係；師父也作**[師傅](../Page/師傅.md "wikilink")**是[前輩](../Page/長輩.md "wikilink")，是[學問](../Page/學問.md "wikilink")、[武功](../Page/武術.md "wikilink")、[技術](../Page/技術.md "wikilink")、[工藝的傳授者](../Page/工藝.md "wikilink")；而徒弟是[學生是晚輩](../Page/學生.md "wikilink")。

受[儒家文化影響](../Page/儒家文化.md "wikilink")，[東亞傳統師徒關係往往是要求尊師重道](../Page/東亞.md "wikilink")，而師傅和徒弟一起[生活](../Page/生活.md "wikilink")，師徒之間往往形式一種有如[父母和](../Page/父母.md "wikilink")[子女的關係](../Page/子女.md "wikilink")，這種關係是終身的，所謂「一日為師，終身為父」，而傳統拜師儀式也是[結誼儀式之一](../Page/結誼.md "wikilink")。當徒弟的不像[學生上學一般](../Page/學生.md "wikilink")，每天未必都有新的[知識傳授](../Page/知識.md "wikilink")，甚至跟隨師傅幾年，也只是常[童工](../Page/童工.md "wikilink")、[傭人而已](../Page/傭人.md "wikilink")；但他們並不像學生那樣需要按時交[學費](../Page/學費.md "wikilink")，而是要為師傅工作，同時師父會供應徒弟的日常生活所需。

隨著[學校制度與](../Page/學校.md "wikilink")[免費教育普及](../Page/免費教育.md "wikilink")，師徒制在現代社會已經較少見，但一些傳統藝術、文化的傳承如[戲曲](../Page/戲曲.md "wikilink")、[武術等以及](../Page/武術.md "wikilink")[宗教](../Page/宗教.md "wikilink")[修行](../Page/修行.md "wikilink")（如[佛教](../Page/佛教.md "wikilink")、[道教等](../Page/道教.md "wikilink")）或某些特定行業如[大律師仍有師徒制](../Page/大律師.md "wikilink")。

## 著名的师徒关系

  - [苏格拉底](../Page/苏格拉底.md "wikilink") →
    [柏拉图](../Page/柏拉图.md "wikilink") →
    [亚里士多德](../Page/亚里士多德.md "wikilink") →
    [亚历山大大帝](../Page/亚历山大大帝.md "wikilink")
  - [弗洛伊德](../Page/弗洛伊德.md "wikilink") → [荣格](../Page/荣格.md "wikilink")
  - [萨尔·汗](../Page/萨尔·汗.md "wikilink") →
    [比尔·盖茨](../Page/比尔·盖茨.md "wikilink")
  - [本杰明·格雷厄姆](../Page/本杰明·格雷厄姆.md "wikilink") →
    [沃伦·巴菲特](../Page/沃伦·巴菲特.md "wikilink")
  - [孔子](../Page/孔子.md "wikilink") →
    [颜回](../Page/颜回.md "wikilink")、[子路](../Page/子路.md "wikilink")、[子贡](../Page/子贡.md "wikilink")、[子夏](../Page/子夏.md "wikilink")、[有若](../Page/有若.md "wikilink")……（<small>参见：**[孔子的三千弟子和七十二贤人](../Page/孔子#三千弟子和七十二贤人.md "wikilink")**及**[孔子弟子列表](../Page/孔子弟子列表.md "wikilink")**</small>）
  - [荀子](../Page/荀子.md "wikilink") →
    [韩非子](../Page/韩非子.md "wikilink")、[李斯](../Page/李斯.md "wikilink")、[贾谊](../Page/贾谊.md "wikilink")
  - [鬼谷子](../Page/鬼谷子.md "wikilink") →
    [张仪](../Page/张仪.md "wikilink")、[苏秦](../Page/苏秦.md "wikilink")、[孙膑](../Page/孙膑.md "wikilink")、[庞涓](../Page/庞涓.md "wikilink")、[毛遂](../Page/毛遂.md "wikilink")、[徐福](../Page/徐福.md "wikilink")
  - [温庭筠](../Page/温庭筠.md "wikilink") → [鱼玄机](../Page/鱼玄机.md "wikilink")
  - [康有为](../Page/康有为.md "wikilink") → [梁启超](../Page/梁启超.md "wikilink")
    → [徐志摩](../Page/徐志摩.md "wikilink")
  - [唐僧](../Page/唐僧.md "wikilink") →
    [孙悟空](../Page/孙悟空.md "wikilink")、[猪八戒](../Page/猪八戒.md "wikilink")、[沙僧](../Page/沙僧.md "wikilink")
  - [梁赞](../Page/梁赞.md "wikilink") →
    [陈华顺](../Page/陈华顺.md "wikilink")、[叶问](../Page/叶问.md "wikilink")、[李小龙](../Page/李小龙.md "wikilink")
  - [楚原](../Page/楚原.md "wikilink") → [馮淬帆](../Page/馮淬帆.md "wikilink")
  - [于占元](../Page/于占元.md "wikilink") →
    『[七小福](../Page/七小福.md "wikilink")』[元龍](../Page/元龍.md "wikilink")、[元樓](../Page/元樓.md "wikilink")、[元彪](../Page/元彪.md "wikilink")、[元奎](../Page/元奎.md "wikilink")、[元華](../Page/元華.md "wikilink")、[元泰](../Page/元泰.md "wikilink")、[元武等人](../Page/元武.md "wikilink")。

## 相關

  - [門徒](../Page/門徒.md "wikilink")
  - [道統](../Page/道統.md "wikilink")
  - [學徒制度](../Page/學徒.md "wikilink")：[在職訓練的一種](../Page/在職訓練.md "wikilink")，但晚近的師徒制度含意已跳脫學徒制的觀點，特別是採導師制（mentoring）的概念時，其與傳統學徒制度的意涵-在職訓練-差異更大。

[Category:教育](../Category/教育.md "wikilink")
[Category:東亞傳統](../Category/東亞傳統.md "wikilink")