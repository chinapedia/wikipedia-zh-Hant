在[計算複雜度理論中](../Page/計算複雜度理論.md "wikilink")，**常數時間**是一种[时间复杂度](../Page/时间复杂度.md "wikilink")，它表示某个[算法求出解答的时间在固定范围内](../Page/算法.md "wikilink")，而不依照問題輸入資料大小变化。

常數時間記為\(O(1)\)（采用[大O符號](../Page/大O符號.md "wikilink")）。数字1可以替换为任意常数。

舉例：

  -
    想在「存取[陣列上的元素](../Page/陣列.md "wikilink")」的問題上達到常數時間，只要以元素的-{zh-hant:序位;zh-hans:序号}-存取即可。
    然而「在陣列上搜索最小值」並不是一個常數時間問題，因為这需要掃描陣列上的每一個元素以寻找最小值及其位置，一般需要\(O(n)\)次访问。

## 參考文獻

### 書籍

  -
  -
  -
  -
  -
  -
  -
  -
### 研究報告

  -
  -
  -
  -
## 参见

  - [多項式時間](../Page/多項式時間.md "wikilink")
  - [線性時間](../Page/線性時間.md "wikilink")
  - [指數時間](../Page/指數時間.md "wikilink")
  - [計算複雜度理論](../Page/計算複雜度理論.md "wikilink")
  - [P/NP問題](../Page/P/NP問題.md "wikilink")
  - [NP](../Page/NP_\(複雜度\).md "wikilink")
  - [演算法](../Page/演算法.md "wikilink")
  - [大O符號](../Page/大O符號.md "wikilink")

[Category:計算複雜性理論](../Category/計算複雜性理論.md "wikilink")