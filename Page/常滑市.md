**常滑市**（）是[日本](../Page/日本.md "wikilink")[爱知县的一个市](../Page/爱知县.md "wikilink")，建于1954年4月1日。

2005年2月17日启用的[中部国际机场就位于常滑市](../Page/中部国际机场.md "wikilink")。

## 外部链接

  - [<http://www.city.tokoname.aichi.jp>](http://www.city.tokoname.aichi.jp/)

  - [維客旅行上的](../Page/維客旅行.md "wikilink")[常滑市](http://wikitravel.org/ja/常滑市)

  -
  - \[//maps.google.com/maps?q=Tokoname 谷歌地圖\]