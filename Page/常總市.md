**常總市**（）是位於[茨城縣西南部的一](../Page/茨城縣.md "wikilink")[市](../Page/市.md "wikilink")。設立於2006年1月1日，由水海道市與[石下町編入合併誕生](../Page/石下町.md "wikilink")，同時改名。

## 历史

  - 1889年（明治22年）4月1日 -
    町村制施行，劃分為[豐田郡水海道町](../Page/豐田郡_\(下總國\).md "wikilink")、三妻村、五箇村、大生村、石下村、豐田村、[岡田郡菅原村](../Page/岡田郡.md "wikilink")、大花羽村、岡田村、飯沼村、[北相馬郡坂手村](../Page/北相馬郡.md "wikilink")、內守谷村、菅生村。
  - 1896年（明治29年）3月29日 -
    [豐田郡](../Page/豐田郡_\(下總國\).md "wikilink")、[岡田郡及](../Page/岡田郡.md "wikilink")[結城郡統合成新的](../Page/結城郡.md "wikilink")[結城郡](../Page/結城郡.md "wikilink")。
  - 1897年（明治30年）8月24日 - 結城郡石下村施行町制。

## 經濟

### 工業區

#### 大生郷工業區

  - 日立Maxell
  - TOSTEM
  - ITOKI
  - YKK AP
  - SMC等

#### 內守谷工業區

  - 森永乳業
  - 王子千代田集裝箱
  - SK化研
  - 日本EXCEED
  - 全農HIPAC等

## 教育施設

### 高等學校

  - 茨城縣立水海道第一高等學校
  - 茨城縣立水海道第二高等學校
  - 茨城縣立石下高等學校

### 中學校

  - [常總市立水海道中學校](https://web.archive.org/web/20070927212240/http://kaichu.web5.jp/)
  - [常總市立水海道西中學校](https://web.archive.org/web/20071115215236/http://kaisei-jh.com/)
  - [常總市立鬼怒中學校](https://web.archive.org/web/20070807140002/http://academic4.plala.or.jp/kinujh/)
  - 常總市立石下中學校
  - 常總市立石下西中學校

### 小學校

<table>
<colgroup>
<col style="width: 0%" />
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071225092623/http://www3.ocn.ne.jp/~kaisho/">常總市立水海道小學校</a></li>
<li><a href="https://web.archive.org/web/20071110225121/http://www15.ocn.ne.jp/~oonosyo/">常總市立大生小學校</a></li>
<li><a href="https://web.archive.org/web/20071120080944/http://www1.ocn.ne.jp/~gokasyo/">常總市立五箇小學校</a></li>
<li><a href="https://web.archive.org/web/20080412055204/http://www16.ocn.ne.jp/~mituma/">常總市立三妻小學校</a></li>
<li><a href="https://web.archive.org/web/20080412055610/http://www15.ocn.ne.jp/~ohanawa/">常總市立大花羽中學校</a></li>
<li><a href="https://web.archive.org/web/20080412060027/http://www11.ocn.ne.jp/~sugahara/">常總市立菅原小學校</a></li>
<li><a href="https://web.archive.org/web/20080225201135/http://www15.ocn.ne.jp/~toyosyo/">常總市立豐岡小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071124013549/http://www.fureai.or.jp/~kensei/">常總市立絹西小學校</a></li>
<li><a href="https://web.archive.org/web/20080412060031/http://www16.ocn.ne.jp/~sugao/">常總市立菅生小學校</a></li>
<li>常總市立石下小學校</li>
<li>常總市立豐田小學校</li>
<li>常總市立玉小學校</li>
<li>常總市立岡田小學校</li>
<li>常總市立飯沼小學校</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 當地名人

  - [羽田美智子](../Page/羽田美智子.md "wikilink") - 女優、旧水海道市出身
  - [岡田忠之](../Page/岡田忠之.md "wikilink") - 自行車賽選手、旧水海道市出身
  - [長塚節](../Page/長塚節.md "wikilink") -
    [歌人](../Page/歌人.md "wikilink")、旧国生村出身
  - [鈴木桂治](../Page/鈴木桂治.md "wikilink") -
    [2004年夏季奥林匹克运动会](../Page/2004年夏季奥林匹克运动会.md "wikilink")[柔道男子](../Page/柔道.md "wikilink")100Kg以上級金牌得主、旧石下町出身
  - [小谷野敦](../Page/小谷野敦.md "wikilink") - 文学者、旧水海道市出身
  - [市村則紀](../Page/市村則紀.md "wikilink") - 職業棒球選手、旧水海道市出身