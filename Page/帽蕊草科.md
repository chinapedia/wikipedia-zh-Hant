**帽蕊草科**\[1\]又名[帽蕊花科或](../Page/帽蕊花科.md "wikilink")[奴草科](../Page/奴草科.md "wikilink")\[2\]，只有1[属](../Page/属.md "wikilink")2[种](../Page/种.md "wikilink")，生长在[东南亚以及](../Page/东南亚.md "wikilink")[中美洲的](../Page/中美洲.md "wikilink")[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[中国有一种](../Page/中国.md "wikilink")，生长在南方。\[3\]

本科[植物是](../Page/植物.md "wikilink")[寄生植物](../Page/寄生植物.md "wikilink")，常寄生在[壳斗科植物上面](../Page/壳斗科.md "wikilink")，[茎非常短](../Page/茎.md "wikilink")，只有3.5-6.5厘米；[叶蜕化为鳞片状](../Page/叶.md "wikilink")，对生；茎顶部只生有一朵[花](../Page/花.md "wikilink")，[雄蕊围绕](../Page/雄蕊.md "wikilink")[雌蕊群合生](../Page/雌蕊.md "wikilink")；[果实为](../Page/果实.md "wikilink")[蒴果或](../Page/蒴果.md "wikilink")[浆果状](../Page/浆果.md "wikilink")，内含许多微小的[种子](../Page/种子.md "wikilink")。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[大花草目中](../Page/大花草目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法发现其和](../Page/APG_分类法.md "wikilink")[杜鹃花目中许多种类的](../Page/杜鹃花目.md "wikilink")[基因相似](../Page/基因.md "wikilink")，但无法分在任何[目中](../Page/目.md "wikilink")，属于系属不清，2003年经过修订的[APG
II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。2009年的[APG
III将其放进](../Page/APG_III.md "wikilink")[杜鹃花目](../Page/杜鹃花目.md "wikilink")。

## 注釋

<references />

## 參考文獻

  - Huang, Shumei （黃淑美）& M. G. Gilbert. 2003.
    [Rafflesiaceae](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=10756).
    In: Flora of China Editorial Committee. Flora of China. 5: 270.
    Science Press & Missouri Botanical Garden Press, Beijing & St.
    Louis.
  - YANG, YUEN-PO and LU, SHENG-YOU（楊遠波&呂勝由）. 1996. [43.
    RAFFLESIACEAE](http://tai2.ntu.edu.tw/PlantInfo/genus.php?genus=Mitrastemon&family=Rafflesiaceae).
    Flora of Taiwan, second edition (台灣植物誌第二版). 2: 652.

## 外部链接

  - [APWeb中的帽蕊草科](http://www.mobot.org/MOBOT/Research/APWeb/orders/ericalesweb.htm#Mitrastemonaceae)
  - [NCBI中的帽蕊草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=91826)

[\*](../Category/帽蕊草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  [帽蕊草屬](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=200083)，2003年出版的《Flora
    of China》乃因舊習把本屬置於大花草科之下，稱**帽蕊草屬**。

2.  [《臺灣植物資訊整合查詢系統》](http://tai2.ntu.edu.tw/PlantInfo/genus.php?genus=Mitrastemon&family=Rafflesiaceae)，1996年出版的第二版《臺灣植物誌》（英文版）列為**奴草屬**，屬於大花草科。。

3.