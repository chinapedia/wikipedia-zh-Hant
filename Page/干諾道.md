[Connaught_Road_Central_Near_Sheung_Wan_2015.jpg](https://zh.wikipedia.org/wiki/File:Connaught_Road_Central_Near_Sheung_Wan_2015.jpg "fig:Connaught_Road_Central_Near_Sheung_Wan_2015.jpg")
[Connaught_Road_Central_201406.jpg](https://zh.wikipedia.org/wiki/File:Connaught_Road_Central_201406.jpg "fig:Connaught_Road_Central_201406.jpg")外一段\]\]
[Connaught_Road_Central_Tunnel_201412.jpg](https://zh.wikipedia.org/wiki/File:Connaught_Road_Central_Tunnel_201412.jpg "fig:Connaught_Road_Central_Tunnel_201412.jpg")期間的干諾道中畢打街隧道\]\]
[Connaught_Road_Central_near_Infinitus_Plaza.JPG](https://zh.wikipedia.org/wiki/File:Connaught_Road_Central_near_Infinitus_Plaza.JPG "fig:Connaught_Road_Central_near_Infinitus_Plaza.JPG")及[海港政府大樓一段](../Page/海港政府大樓.md "wikilink")，亦為干諾道西天橋之起點。\]\]
[Connaught_Road_Central_near_Exchange_Square_at_night.jpg](https://zh.wikipedia.org/wiki/File:Connaught_Road_Central_near_Exchange_Square_at_night.jpg "fig:Connaught_Road_Central_near_Exchange_Square_at_night.jpg")巴士站。（2015年）\]\]

**干諾道中**（）及**干諾道西**（）是[香港島區的其中一組主幹道](../Page/香港島.md "wikilink")，位於[中西區沿岸](../Page/中西區_\(香港\).md "wikilink")，連接[堅尼地城](../Page/堅尼地城.md "wikilink")[城西道及](../Page/城西道.md "wikilink")[金鐘](../Page/金鐘.md "wikilink")[夏慤道](../Page/夏慤道.md "wikilink")，穿越[中環心臟地帶](../Page/中環.md "wikilink")。全線為3線至4線雙程分隔[道路](../Page/道路.md "wikilink")，部份路段為[行車天橋及地下](../Page/行車天橋.md "wikilink")[行車隧道](../Page/行車隧道.md "wikilink")。干諾道中曾經屬於[香港4號幹線的一部份](../Page/香港4號幹線.md "wikilink")，而干諾道西天橋現時仍屬於[香港4號幹線的一部份](../Page/香港4號幹線.md "wikilink")，干諾道西天橋雖不是快速公路，但車速限制為每小時80公里，是香港島限速最寬鬆的道路，比同屬4號幹線的[東區走廊](../Page/東區走廊.md "wikilink")——香港島唯一一條快速公路的限速（每小時70公里）更寬鬆。

## 歷史

[香港政府於](../Page/香港殖民地時期#香港政府.md "wikilink")1889年起於[中環進行](../Page/中環.md "wikilink")[海旁填海計劃](../Page/海旁填海計劃.md "wikilink")。1890年，[英國](../Page/英國.md "wikilink")[王子](../Page/王子.md "wikilink")[干諾公爵夫婦訪問](../Page/阿瑟王子_\(康諾和斯特拉森公爵\).md "wikilink")[香港](../Page/香港.md "wikilink")。為紀念此事，當時香港護督[傅林明](../Page/傅林明.md "wikilink")（署理當時離港渡假的[香港總督](../Page/香港總督.md "wikilink")[德輔](../Page/德輔.md "wikilink")）決定將在中環填海區的新海旁道，以[維多利亞女皇的第三子](../Page/維多利亞女皇.md "wikilink")[干諾公爵](../Page/干諾公爵.md "wikilink")（Prince
Arthur William Patrick Albert, Duke of Connaught and
Strathearn）命名為**干諾道**，並於1903年落成通車。其後，干諾道被分為兩部份：干諾道中東起[金鐘](../Page/金鐘.md "wikilink")[夏慤道](../Page/夏慤道.md "wikilink")，西至[上環](../Page/上環.md "wikilink")[安泰街](../Page/安泰街.md "wikilink")；干諾道西東起上環安泰街，西至[石塘咀](../Page/石塘咀.md "wikilink")[山道](../Page/山道.md "wikilink")，在西區填海工程完成後，再伸延到[城西道](../Page/城西道.md "wikilink")。[香港日治時期](../Page/香港日治時期.md "wikilink")，干諾道曾經被改名為**住吉通**。

道路於落成時本來全為地面道路。後來於1980年代，為疏導日益擠塞的交通，政府在八十年代末實行「干諾道改善工程計劃」，改善干諾道為無[交通燈阻隔的主幹道路](../Page/交通號誌.md "wikilink")，連接港島東西兩端。**[林士街天橋](../Page/林士街.md "wikilink")**（小巴界因其貼近[信德中心而俗稱](../Page/信德中心.md "wikilink")「**信德橋**」）和**[畢打街隧道](../Page/畢打街.md "wikilink")**於1986年興建，令車輛能避開地面繁忙路段。1997年3月**干諾道西天橋**（坊間對應[東區走廊而俗稱](../Page/東區走廊.md "wikilink")「**西區走廊**」）落成通車，以便車輛快速地由[中環經](../Page/中環.md "wikilink")[3號幹線](../Page/香港3號幹線.md "wikilink")[西區海底隧道前往](../Page/西區海底隧道.md "wikilink")[香港國際機場](../Page/香港國際機場.md "wikilink")、[屯門及](../Page/屯門.md "wikilink")[元朗](../Page/元朗.md "wikilink")。干諾道西天橋接駁林士街天橋，目前通往[堅尼地城](../Page/堅尼地城.md "wikilink")[城西道](../Page/城西道.md "wikilink")，並設有預留位連接計劃中的**4號幹線**堅尼地城至[香港仔段的公路](../Page/香港仔.md "wikilink")。1998年，為配合通往[港鐵](../Page/港鐵.md "wikilink")[香港站附近的](../Page/香港站.md "wikilink")[民祥街的地下行車隧道](../Page/民祥街.md "wikilink")，干諾道中近[海港政府大樓的東行線取消](../Page/海港政府大樓.md "wikilink")，車輛改道新建的[民吉街](../Page/民吉街.md "wikilink")，而西行線則需重新定線繞過民祥街隧道的出口。另外干諾道中往金鐘方向在交易廣場對開路段亦被重新劃線，由干諾道西天橋落橋入干諾道中，只-{准}-直入畢打街隧道；前往中環之車輛必須繞經民寶街（[國際金融中心一帶](../Page/國際金融中心_\(香港\).md "wikilink")），或者由金鐘[紅棉路繞回](../Page/紅棉路.md "wikilink")[皇后大道中一帶](../Page/皇后大道中.md "wikilink")。

2019年[中環灣仔繞道通車後](../Page/中環灣仔繞道.md "wikilink")，干諾道中天橋除[林士街天橋以外不再成為](../Page/林士街天橋.md "wikilink")4號幹線一部分，而[3號幹線終點位於干諾道西](../Page/香港3號幹線.md "wikilink")。

## 沿路著名地點

  - 干諾道中

<!-- end list -->

  - [友邦金融中心](../Page/友邦金融中心.md "wikilink")
  - [香港會所大廈](../Page/香港會所大廈.md "wikilink")
  - [中國建設銀行大廈](../Page/中國建設銀行大廈.md "wikilink")
  - [香港大會堂](../Page/香港大會堂.md "wikilink")
  - [皇后像廣場](../Page/皇后像廣場.md "wikilink")
  - [怡和大廈](../Page/怡和大廈.md "wikilink")
  - [香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")
  - [聖佐治大廈](../Page/聖佐治大廈.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")
  - [環球大廈](../Page/環球大廈.md "wikilink")
  - [中環站](../Page/中環站.md "wikilink")
  - [永安集團大廈](../Page/永安集團大廈.md "wikilink")
  - [李寶樁大廈](../Page/李寶樁.md "wikilink")
  - [永安中心](../Page/永安中心.md "wikilink")
  - [南豐大廈](../Page/南豐集團.md "wikilink")
  - [交易廣場](../Page/交易廣場.md "wikilink")
  - [國際金融中心一期](../Page/國際金融中心_\(香港\).md "wikilink")
  - [恒生銀行總行大廈](../Page/恒生銀行總行大廈.md "wikilink")
  - [華懋廣場II期](../Page/華懋.md "wikilink")
  - [中國農業銀行大廈](../Page/中國農業銀行大廈.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[港島線](../Page/港島線.md "wikilink")[上環站](../Page/上環站.md "wikilink")
  - [無限極廣場](../Page/無限極廣場.md "wikilink")
  - [中保集團大廈](../Page/中保集團大廈.md "wikilink")
  - [海港政府大樓](../Page/海港政府大樓.md "wikilink")
  - [信德中心](../Page/信德中心.md "wikilink")

<!-- end list -->

  - 干諾道西

<!-- end list -->

  - [西港城](../Page/西港城.md "wikilink")
  - [維港峰](../Page/維港峰.md "wikilink")
  - [中山紀念公園](../Page/中山紀念公園.md "wikilink")
  - [西區海底隧道港島出入口](../Page/西區海底隧道.md "wikilink")
  - [香港萬怡酒店](../Page/香港萬怡酒店.md "wikilink")
  - [港島太平洋酒店](../Page/港島太平洋酒店.md "wikilink")
  - [中區警區總部](../Page/中區警區總部.md "wikilink")(前稱海傍警署)
  - [中央人民政府駐香港特別行政區聯絡辦公室](../Page/中央人民政府駐香港特別行政區聯絡辦公室.md "wikilink")/[西港中心](../Page/西港中心.md "wikilink")
  - [均益大廈第二和第三期](../Page/均益大廈.md "wikilink")
  - [高樂花園第一至第三座](../Page/高樂花園.md "wikilink")
  - [香港商業中心](../Page/香港商業中心.md "wikilink")
  - [香港電車](../Page/香港電車.md "wikilink")[屈地街總站](../Page/屈地街總站.md "wikilink")

<File:Hong> Kong Club Building.jpg|[香港會大廈](../Page/香港會.md "wikilink")
<File:Exchange> Square 3 2016.jpg|[交易廣場第三座](../Page/交易廣場.md "wikilink")
<File:50> Connaught Road Central
201108.jpg|[中國農業銀行大廈](../Page/中國農業銀行大廈.md "wikilink")
<File:Western> Market Overview
201008.jpg|[西港城](../Page/西港城.md "wikilink") <File:LOCPG> HK
2012.JPG|[中央人民政府駐香港特別行政區聯絡辦公室](../Page/中央人民政府駐香港特別行政區聯絡辦公室.md "wikilink")

另外，干諾道以東連接[夏慤道](../Page/夏慤道.md "wikilink")，並沒有「干諾道東」\[1\]。

## 途經的公共交通服務

現時大部份途經干諾道西的巴士路線皆取道地面路段，故以下只列出途經干諾道西天橋的巴士路線。

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參考資料

## 參見

  - [德輔道](../Page/德輔道.md "wikilink")
  - [皇后大道](../Page/皇后大道.md "wikilink")

[Category:香港主要幹道](../Category/香港主要幹道.md "wikilink")
[Category:香港4號幹線](../Category/香港4號幹線.md "wikilink")
[Category:曾經的香港4號幹線路段](../Category/曾經的香港4號幹線路段.md "wikilink")
[Category:中西區 (香港)](../Category/中西區_\(香港\).md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")

1.