[Suzakumon_Heijokyo1.jpg](https://zh.wikipedia.org/wiki/File:Suzakumon_Heijokyo1.jpg "fig:Suzakumon_Heijokyo1.jpg")

**平城宮**（）是[日本於](../Page/日本.md "wikilink")710年遷都[奈良後建設于](../Page/奈良.md "wikilink")[平城京](../Page/平城京.md "wikilink")[大内中的](../Page/皇城.md "wikilink")[宮室](../Page/宮殿.md "wikilink")。位于[平城京的北端](../Page/平城京.md "wikilink")。由舉行禮儀儀式的[朝堂院和官員辦公的官衙](../Page/朝堂院.md "wikilink")（[役所](../Page/役所.md "wikilink")）組成。[遷都](../Page/遷都.md "wikilink")[平安京後](../Page/平安京.md "wikilink")，該宮被放棄，變為農地。在[明治時代](../Page/明治.md "wikilink")，當時的建築史家[關野貞于田野中發現了一塊高地](../Page/關野貞.md "wikilink")，后被證實就是大極殿的遺址。**平城宮跡**（）于1922年被[日本政府指定為](../Page/日本政府.md "wikilink")[史跡](../Page/史跡.md "wikilink")（後指定為[特別史跡](../Page/特別史跡.md "wikilink")）。根據遺跡的發掘成果，朱雀門（1998年竣工）和庭園的復原工程已經完工。并在2010年完成[大極殿的復原工程](../Page/大極殿.md "wikilink")。在1998年12月，以「[古都奈良的文化財](../Page/古都奈良的文化財.md "wikilink")」而與[東大寺等建筑共同列入](../Page/東大寺.md "wikilink")[世界遺產名單](../Page/世界遺產.md "wikilink")，也是日本第一個登入世界遺產的考古遺跡。

## 平城宮跡設施

[Daigokuden_16-9.jpg](https://zh.wikipedia.org/wiki/File:Daigokuden_16-9.jpg "fig:Daigokuden_16-9.jpg")
[Toin_Teien.jpg](https://zh.wikipedia.org/wiki/File:Toin_Teien.jpg "fig:Toin_Teien.jpg")

  - 第一次大極殿復原（2010年）完成。
  - 第二次大極殿基壇復原。
  - [朱雀門復原](../Page/朱雀門.md "wikilink")（1998年）完成。
  - [朱雀大路廣場](../Page/朱雀大路.md "wikilink")
      - [棚田嘉十郎銅像](../Page/棚田嘉十郎.md "wikilink")
  - [兵部省](../Page/兵部省#兵部省_\(律令制\).md "wikilink")、[式部省](../Page/式部省.md "wikilink")－基壇、礎石復原。
  - 平城宮跡資料館－全體模型、大極殿院模型等。
  - 遺構展示館－建在發掘遺跡之上、見學可能。
  - 平城京歷史館－遣唐使船復原展示。
  - [宮內省](../Page/宮內省.md "wikilink")－建物群、[築地復原](../Page/築地塀.md "wikilink")。
  - 東院庭園－庭園、樓閣等復原。日本[特別名勝](../Page/特別名勝.md "wikilink")。

## 關聯條目

  - [藤原京](../Page/藤原京.md "wikilink")
  - [平城京](../Page/平城京.md "wikilink")
  - [平城遷都1300年記念事業](../Page/平城遷都1300年記念事業.md "wikilink")
  - [日本建築史](../Page/日本建築史.md "wikilink")
  - [京奈和自動車道](../Page/京奈和自動車道.md "wikilink")
  - [關西文化學術研究都市](../Page/關西文化學術研究都市.md "wikilink")

## 外部連結

  - [平城宮跡資料館](http://www.nabunken.go.jp/chinese/heijo.html)
  - [奈良世界遺産市民Netwoek](https://web.archive.org/web/20070917073803/http://www3.ocn.ne.jp/~nsih2001/)

[Category:日本世界遺產](../Category/日本世界遺產.md "wikilink")
[Category:奈良市](../Category/奈良市.md "wikilink")
[Category:奈良縣史跡](../Category/奈良縣史跡.md "wikilink")
[Category:日本宫殿](../Category/日本宫殿.md "wikilink")
[Category:特別史跡](../Category/特別史跡.md "wikilink")
[Category:日本國指定名勝](../Category/日本國指定名勝.md "wikilink")