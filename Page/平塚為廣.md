**平塚為廣**（1556年? -
1600年10月21日）是日本[戰國時代以及](../Page/战国_\(日本\).md "wikilink")[安土桃山時代的](../Page/安土桃山時代.md "wikilink")[武將](../Page/武將.md "wikilink")。[官位為](../Page/官位.md "wikilink")[從五位下](../Page/從五位下.md "wikilink")[因幡守](../Page/因幡国.md "wikilink")。弟越中守久賀。

## 生平

父親為三郎入道無心。原為三浦氏一族，於獲封平塚鄉時，趁機改姓平塚｡通稱平九郎､因幡守｡

秀吉於中國出陣時，因被秀吉訓斥成為浪人，平塚藤藏於播磨之戰時，於黑田孝高的陣營效力，後因討取高倉山城主福原助就再度仕於秀吉，此時平塚藤藏改名為廣。
最初跟隨[明智光秀](../Page/明智光秀.md "wikilink")。[本能寺之變自稱曾參與對](../Page/本能寺之變.md "wikilink")[織田信長襲擊](../Page/織田信長.md "wikilink")。後來出任[豐臣秀吉家臣](../Page/豐臣秀吉.md "wikilink")（馬廻眾），其後參加[小牧·長久手之戰和](../Page/小牧·長久手之戰.md "wikilink")[小田原征伐獲得了功績](../Page/小田原征伐.md "wikilink")。1592年起開始協助秀吉[文祿・慶長之役](../Page/文祿・慶長之役.md "wikilink")，在[肥前](../Page/肥前国.md "wikilink")[名護屋城駐守](../Page/名護屋城.md "wikilink")。文祿4年（1595年）7月，由於對秀吉的長期效忠，最後擁有8000石的領地。慶長3年（1598年）秀吉舉辦醍醐花會時，擔任護衛。

秀吉死後繼續為[豐臣秀賴效力](../Page/豐臣秀賴.md "wikilink")，1600年被分封[美濃垂井](../Page/美濃國.md "wikilink")1万2000石。同年在[關原之戰前](../Page/關原之戰.md "wikilink")、為勸諫計劃討伐[德川家康而出兵的](../Page/德川家康.md "wikilink")[石田三成](../Page/石田三成.md "wikilink")，與大谷吉繼在[佐和山城共同遊說](../Page/佐和山城.md "wikilink")，後來吉繼加入了西軍。其後参與攻擊[伏見城](../Page/伏見城.md "wikilink")。在9月14日大谷吉繼命令[戶田勝成和為廣共同查探](../Page/戶田勝成.md "wikilink")[小早川秀秋動向](../Page/小早川秀秋.md "wikilink")，秀秋看似有意背叛，因此下命進行暗殺，不過秀秋事前已經察覺到暗殺行動，所以沒有成功。9月15日在關原之戰為大谷吉繼所屬奮戰，將山内一豊的家臣樫井太兵衛及小早川秀秋的家臣横田小半介等討死。不過由於[脇坂安治等人的背叛下](../Page/脇坂安治.md "wikilink")，以及藤堂隊、京極隊的攻擊而支持不住，最終自殺身亡。死前，命人將取得的敵將頭顱及絕命詩句送到大谷吉繼身邊。

## 家族

此外，嫡男[左馬助為景參加](../Page/左馬助為景.md "wikilink")[大坂夏之陣](../Page/大坂夏之陣.md "wikilink")，於若江之戰死。

## 外部連結

  - [平塚為広の碑](https://archive.is/20130427061141/http://n-hp.com/navigate/public/mu8/bin/view.rbz?cd=91)
    関ケ原町地域振興課

[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:三浦氏](../Category/三浦氏.md "wikilink")
[Category:日本战争身亡者](../Category/日本战争身亡者.md "wikilink")
[Category:1600年逝世](../Category/1600年逝世.md "wikilink")