**平岡祐太**（
），出生於[廣島縣](../Page/廣島縣.md "wikilink")[廣島市](../Page/廣島市.md "wikilink")\[1\]\[2\]\[3\]，[山口縣](../Page/山口縣.md "wikilink")[玖珂郡](../Page/玖珂郡.md "wikilink")[和木町出身](../Page/和木町.md "wikilink")\[4\]。[日本](../Page/日本.md "wikilink")[男演員](../Page/男演員.md "wikilink")，所屬[經紀公司為](../Page/經紀公司.md "wikilink")[Amuse](../Page/Amuse.md "wikilink")。

## 略歷

  - 2002年：獲得第15回「[JUNON SUPER
    BOY](../Page/JUNON_SUPER_BOY.md "wikilink")」大賞
  - 2003年：以『』一戲出道
  - 2005年：以『[搖擺女孩](../Page/搖擺女孩.md "wikilink")』獲得第28回「[日本電影學院獎](../Page/日本電影學院獎.md "wikilink")」新人獎
  - 2006年：10月起一年間連續出演四部電視劇，其中『[東京鐵塔：老媽和我，有時還有老爸](../Page/東京鐵塔：老媽和我，有時還有老爸.md "wikilink")』、『[求婚大作戰](../Page/求婚大作戰.md "wikilink")』、『[First
    Kiss](../Page/First_Kiss_\(電視劇\).md "wikilink")』三部為月九
  - 2009年4月：『[醫界神手](../Page/醫界神手.md "wikilink")』電視劇初主演
  - 2010年：『[龍馬傳](../Page/龍馬傳.md "wikilink")』[大河劇初出演](../Page/大河劇.md "wikilink")
  - 2010年7月：首次擔任遊戲配音員『』
  - 2013年11月16日：於台灣[九份舉行首次海外粉絲見面會](../Page/九份.md "wikilink")\[5\]
  - 2014年：『[熱海戀歌](../Page/熱海戀歌.md "wikilink")』[台灣電視劇初出演](../Page/臺灣電視劇.md "wikilink")
  - 2016年：『[童裝小姐](../Page/別嬪小姐.md "wikilink")』[晨間劇初出演](../Page/連續電視小說.md "wikilink")

## 人物

  - 身高：178公分
  - 血型：O型
  - 技藝：作曲、吉他、鋼琴、足球
  - 興趣：電影欣賞、音樂欣賞、攝影
  - 家中四名兄弟之長男，最年下的兩個弟弟是雙胞胎
  - 13歲開始學習吉他，高中三年級時轉學到東京
  - 戲路廣泛，與[戶田惠梨香](../Page/戶田惠梨香.md "wikilink")、[榮倉奈奈多次共演](../Page/榮倉奈奈.md "wikilink")

## 作品

### 電影

<table>
<thead>
<tr class="header">
<th><p>日期</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004/09/11 上映  2008/07/19 首播</p></td>
<td><p><a href="../Page/搖擺女孩.md" title="wikilink">搖擺女孩</a></p></td>
<td><p>中村拓雄</p></td>
<td><p>第28回「日本電影學院獎」新人獎</p></td>
</tr>
<tr class="even">
<td><p>2004/10/30 上映  2005/04/08 上映</p></td>
<td><p><a href="../Page/現在，很想見你#電影.md" title="wikilink">現在，很想見你</a></p></td>
<td><p>秋穗佑司</p></td>
<td><p>角色年齡為18歲時</p></td>
</tr>
<tr class="odd">
<td><p>2005/09/03 上映  2005/11/04 上映</p></td>
<td><p><a href="../Page/NANA_(電影).md" title="wikilink">NANA</a></p></td>
<td><p>遠藤章司</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005/10/29 上映</p></td>
<td></td>
<td><p>アキ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006/04/22 上映</p></td>
<td><p>{{Link-ja|Check It Out, Yo!|チェケラッチョ</p></td>
<td><p>}}</p></td>
<td><p>玉城哲雄</p></td>
</tr>
<tr class="even">
<td><p>2006/06/10 上映  2010/12/10 上映</p></td>
<td><p><a href="../Page/圈套_(電視劇)#電影版.md" title="wikilink">圈套劇場版2</a></p></td>
<td><p>青沼和彦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006/08/12 上映  2007/01/05 上映</p></td>
<td></td>
<td><p>田中秀俊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006/11/03 上映</p></td>
<td></td>
<td><p>王子様第5位</p></td>
<td><p>友情客串</p></td>
</tr>
<tr class="odd">
<td><p>2007/01/20 上映  2007/06/01 上映</p></td>
<td><p><a href="../Page/妹妹戀人#電影.md" title="wikilink">妹妹戀人</a></p></td>
<td><p>矢野立芳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007/01/27 上映</p></td>
<td></td>
<td><p>中原直</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007/03/10 上映</p></td>
<td></td>
<td><p>高野悟</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009/03/20 上映  2009/05/27 上映</p></td>
<td></td>
<td><p>浩平</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009/06/13 上映  2011/02/20 首播</p></td>
<td></td>
<td><p>坪田誠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010/08/21 上映  2012/05/04 發行</p></td>
<td><p><a href="../Page/青春恐怖箱.md" title="wikilink">青春恐怖箱</a></p></td>
<td><p>越前魔太郎</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012/01/28 上映  2012/06/01 上映</p></td>
<td></td>
<td><p>山下時生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012/09/15 上映</p></td>
<td></td>
<td><p>江田晴彦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013/10/12 上映</p></td>
<td></td>
<td><p>新志[6]</p></td>
<td><p>主演</p></td>
</tr>
<tr class="even">
<td><p>2016/01/16 上映</p></td>
<td></td>
<td><p>中村和典</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016/02/20 上映</p></td>
<td><p><a href="http://noranekofilm.com/koitoonchi/">戀與音痴的方程式</a></p></td>
<td><p>小川和幸</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016/11/25 上映</p></td>
<td></td>
<td><p>劇團青年</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>首播日期</p></th>
<th><p>播出頻道</p></th>
<th><p>劇名</p></th>
<th><p>飾演</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2003/10/13</p></td>
<td><p><a href="../Page/讀賣電視台.md" title="wikilink">讀賣電視台</a></p></td>
<td></td>
<td><p>織原健治</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004/10/05<br />
2006/04/11</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a><br />
<a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/菜鳥老師來報到.md" title="wikilink">菜鳥老師來報到</a></p></td>
<td><p>高杉順平</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005/02/11</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p>無聲的青空</p></td>
<td><p>門脇孝司</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005/04/20</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
<td><p>柳明夫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005/08/19－20</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/五個撲水的少年#水男孩特別篇.md" title="wikilink">水男孩 2005夏</a></p></td>
<td><p>名嘉尾悟</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005/10/17 2006/07/14</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/危險傻大姊.md" title="wikilink">危險傻大姊</a></p></td>
<td><p>中村拓未</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006/08/22</p>
<p>2012/08/30</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/毛骨悚然撞鬼經驗.md" title="wikilink">毛骨悚然撞鬼經驗</a>2006年特別篇 不可思議的時間</p></td>
<td><p>白鳥大吾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006/08/28</p></td>
<td><p><a href="../Page/毎日放送.md" title="wikilink">毎日放送</a> <a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td></td>
<td><p>杉山傳命</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006/09/14</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td></td>
<td><p>栗本洋輔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006/10/14</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a></p></td>
<td><p><a href="../Page/唯一的愛.md" title="wikilink">唯一的愛</a></p></td>
<td><p>大澤亞裕太</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007/01/08 2008/04/28</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/JET綜合台.md" title="wikilink">JET綜合台</a></p></td>
<td><p><a href="../Page/東京鐵塔：我的母親父親.md" title="wikilink">東京鐵塔：我的母親父親</a></p></td>
<td><p>鳴澤一</p></td>
<td><p><a href="../Page/月九.md" title="wikilink">月九</a></p></td>
</tr>
<tr class="even">
<td><p>2007/03/20－21</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
<td><p>谷脇高士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007/04/16 2007/08/07</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/求婚大作戰.md" title="wikilink">求婚大作戰</a></p></td>
<td><p>榎戶幹雄</p></td>
<td><p><a href="../Page/月九.md" title="wikilink">月九</a></p></td>
</tr>
<tr class="even">
<td><p>2007/07/09 2008/06/02</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/First_Kiss_(電視劇).md" title="wikilink">First Kiss</a></p></td>
<td><p>結城秋生</p></td>
<td><p><a href="../Page/月九.md" title="wikilink">月九</a></p></td>
</tr>
<tr class="odd">
<td><p>2008/01/17 2009/07/23</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a> <a href="../Page/JET綜合台.md" title="wikilink">JET綜合台</a></p></td>
<td><p>[[最喜歡你</p></td>
<td><p>柚子的育兒日記|最喜歡你</p></td>
<td><p>柚子的育兒日記]]</p></td>
</tr>
<tr class="even">
<td><p>2008/03/25 2008/08/26－27</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/求婚大作戰.md" title="wikilink">求婚大作戰SP</a></p></td>
<td><p>榎戶幹雄</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008/03/29</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
<td><p>酒保</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008/05/16</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
<td><p>喜多嶋拓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008/07/01 2009/04/23</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/怪獸家長_(電視劇).md" title="wikilink">怪獸家長</a></p></td>
<td><p>望月道夫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008/09/23</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/世界奇妙物語.md" title="wikilink">世界奇妙物語</a>2008年秋季特別篇 引領長龍的刑警</p></td>
<td><p>鈴木正繼</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008/10/11</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
<td><p>酒保</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008/11/01</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a></p></td>
<td></td>
<td><p>たかあき</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009/01/05</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/惡魔的手毬歌.md" title="wikilink">惡魔的手毬歌</a></p></td>
<td><p>青池歌名雄</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009/01/21 2009/07/22</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/破案天才奇娜.md" title="wikilink">破案天才奇娜</a></p></td>
<td><p>山崎尊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009/04/11 2013/11/18</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/天生妙手.md" title="wikilink">天生妙手</a></p></td>
<td><p>真東輝</p></td>
<td><p>初主演</p></td>
</tr>
<tr class="even">
<td><p>2009/09/23</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a></p></td>
<td></td>
<td><p>村西純</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009/12/20</p></td>
<td><p><a href="../Page/WOWOW.md" title="wikilink">WOWOW</a></p></td>
<td><p><a href="http://www.wowow.co.jp/dramaw/top/suitei.html">一應的推定</a></p></td>
<td><p>竹内善之</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010/01/09</p></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK</a></p></td>
<td></td>
<td><p>深堂由良</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010/01/03 2010/11/12</p></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/龍馬傳.md" title="wikilink">龍馬傳</a>　</p></td>
<td><p>陸奧陽之助</p></td>
<td><p><a href="../Page/大河劇.md" title="wikilink">大河劇初演</a>　</p></td>
</tr>
<tr class="even">
<td><p>2011/01/07 2012/10/03</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/LADY_(电视剧).md" title="wikilink">LADY</a></p></td>
<td><p>新堀圭祐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011/01/17</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p></p></td>
<td><p>濱田貴司</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011/10/17 2012/08/01</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/我無法戀愛的理由.md" title="wikilink">我無法戀愛的理由</a></p></td>
<td><p>山本正</p></td>
<td><p><a href="../Page/月九.md" title="wikilink">月九</a></p></td>
</tr>
<tr class="odd">
<td><p>2012/03/17</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td></td>
<td><p>織田信行</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012/04/21</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/未來日記-ANOTHER:WORLD-.md" title="wikilink">未來日記-ANOTHER:WORLD-</a></p></td>
<td><p>奧田陽介</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012/05/06、06/17 2012/08/06、08/17</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/自閉天才ATARU.md" title="wikilink">自閉天才ATARU</a></p></td>
<td><p>公原卓郎</p></td>
<td><p>第4集、第10集</p></td>
</tr>
<tr class="even">
<td><p>2012/08/23 2013/11/18</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/公視3台.md" title="wikilink">公視3台</a></p></td>
<td><p><a href="../Page/東野圭吾推理劇場.md" title="wikilink">東野圭吾推理劇場</a> 白色兇器</p></td>
<td><p>森田壽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012/09/22</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td><p><a href="../Page/澪之料理帖#朝日電視台版.md" title="wikilink">澪之料理帖</a> 第1部</p></td>
<td><p>永田源齊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012/09/30</p></td>
<td><p><a href="../Page/NHK_BS_Premium.md" title="wikilink">NHK BS Premium</a></p></td>
<td></td>
<td><p>鳴海久志</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012/10/01</p></td>
<td></td>
<td></td>
<td><p>友鷹耀介</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012/10/14 2014/09/02</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/TOKYO機場～東京機場管制保安部～.md" title="wikilink">TOKYO機場～東京機場管制保安部～</a></p></td>
<td><p>本上圭介</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012/12/02</p></td>
<td></td>
<td></td>
<td><p>友鷹耀介</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013/01/02 2014/07/06</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td></td>
<td><p>真田和幸</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013/01/26</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/殺人草莓夜.md" title="wikilink">殺人草莓夜</a> After The Invisible Rain</p></td>
<td><p>辻内眞人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013/02/02－03</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td></td>
<td><p>渡良一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013/04/12</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td></td>
<td><p>隼人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013/05/18 2017/02/18</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a> <a href="../Page/WakuWaku_Japan.md" title="wikilink">WakuWaku Japan</a></p></td>
<td><p><a href="../Page/Specialist.md" title="wikilink">Specialist</a> 1</p></td>
<td><p>堀川耕平</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013/06/23</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td></td>
<td><p>織田信行</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013/09/19</p></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK</a></p></td>
<td></td>
<td><p>勘蔵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013/10/13 2013/10/19</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/A.I.人工智慧男友.md" title="wikilink">A.I.人工智慧男友</a></p></td>
<td><p>角城元</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014/03/07 2015/10/03</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a> <a href="../Page/BS12_TwellV.md" title="wikilink">BS12 TwellV</a></p></td>
<td><p><a href="../Page/熱海戀歌.md" title="wikilink">熱海戀歌</a></p></td>
<td><p>白原正彰 （唏哩嘩啦桑）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014/03/08 2017/02/19</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a> <a href="../Page/WakuWaku_Japan.md" title="wikilink">WakuWaku Japan</a></p></td>
<td><p><a href="../Page/Specialist.md" title="wikilink">Specialist</a> 2</p></td>
<td><p>堀川耕平</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014/06/11、06/18 2014/08/29、09/09</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/花咲舞無法沉默.md" title="wikilink">花咲舞無法沉默</a></p></td>
<td><p>伊丹清一郎</p></td>
<td><p>第9集、第10集</p></td>
</tr>
<tr class="odd">
<td><p>2014/06/08</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td><p><a href="../Page/澪之料理帖#朝日電視台版.md" title="wikilink">澪之料理帖</a> 第2部</p></td>
<td><p>永田源齊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014/07/04</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p><a href="../Page/家族狩獵.md" title="wikilink">家族狩獵</a></p></td>
<td><p>椎村榮作</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014/09/02</p></td>
<td><p><a href="../Page/NHK_BS_Premium.md" title="wikilink">NHK BS Premium</a></p></td>
<td></td>
<td><p>結城健太郎</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014/10/16－12/18 2015/01/15－19</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/我妹是惡魔.md" title="wikilink">我妹是惡魔</a></p></td>
<td><p>吉村達也</p></td>
<td><p>第1集－第3集</p></td>
</tr>
<tr class="odd">
<td><p>2014/12/24</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td></td>
<td><p>岩澤隆太郎</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015/02/28 2017/02/25</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a> <a href="../Page/WakuWaku_Japan.md" title="wikilink">WakuWaku Japan</a></p></td>
<td><p><a href="../Page/Specialist.md" title="wikilink">Specialist</a> 3</p></td>
<td><p>堀川耕平</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015/06/11</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p><a href="../Page/脫黑～不再當黑社會～.md" title="wikilink">脫黑～不再當黑社會～</a></p></td>
<td><p>丸橋</p></td>
<td><p>第9集</p></td>
</tr>
<tr class="even">
<td><p>2015/10/10</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/Teddy_Go!.md" title="wikilink">Teddy Go!</a></p></td>
<td><p>冬野唯志</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015/12/12 2017/02/26</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a> <a href="../Page/朝日電視台.md" title="wikilink">WakuWaku Japan</a></p></td>
<td><p><a href="../Page/Specialist.md" title="wikilink">Specialist</a> 4</p></td>
<td><p>堀川耕平</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016/06/02</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td><p><a href="../Page/早子老師，要結婚是真的嗎？.md" title="wikilink">早子老師，要結婚是真的嗎？</a></p></td>
<td><p>香川優介</p></td>
<td><p>第7集</p></td>
</tr>
<tr class="odd">
<td><p>2016/01/14 2017/03/01</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a> <a href="../Page/朝日電視台.md" title="wikilink">WakuWaku Japan</a></p></td>
<td><p><a href="../Page/Specialist.md" title="wikilink">Specialist</a></p></td>
<td><p>堀川耕平</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016/10/03 2017/04/24</p></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/別嬪小姐.md" title="wikilink">別嬪小姐</a></p></td>
<td><p>村田昭一</p></td>
<td><p><a href="../Page/連續電視小說.md" title="wikilink">晨間劇初演</a></p></td>
</tr>
<tr class="odd">
<td><p>2017/01/18 2017/01/21</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a> <a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><p><a href="../Page/東京白日夢女.md" title="wikilink">東京白日夢女</a></p></td>
<td><p>鮫島涼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017/04/29</p></td>
<td><p><a href="../Page/NHK_BS_Premium.md" title="wikilink">NHK BS Premium</a></p></td>
<td><p><a href="../Page/別嬪小姐#其他影集.md" title="wikilink">別嬪小姐</a> 特別篇～戀愛中的百貨公司</p></td>
<td><p>村田昭一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017/07/05</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td><p><a href="http://www.tv-tokyo.co.jp/kensho_sousa/">檢證搜查</a></p></td>
<td><p>皆川慶一郎</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017/10/30</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p>新・淺見光彥系列 <a href="http://www.tbs.co.jp/getsuyou-meisaku/20171030/">漂泊的樂人 越後〜沼津・哀傷的殺人者</a></p></td>
<td><p><a href="../Page/淺見光彥.md" title="wikilink">淺見光彥</a></p></td>
<td><p>主演</p></td>
</tr>
<tr class="odd">
<td><p>2018/01/26</p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td></td>
<td><p>春田龍馬</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018/02/16</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td></td>
<td><p>北原祐介</p></td>
<td><p>第5集</p></td>
</tr>
<tr class="odd">
<td><p>2018/02/17</p></td>
<td><p><a href="../Page/NHK_BS_Premium.md" title="wikilink">NHK BS Premium</a></p></td>
<td></td>
<td><p>榊田宗栄</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018/02/26</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p>新・淺見光彥系列 <a href="http://www.tbs.co.jp/getsuyou-meisaku/20180226/">後鳥羽傳説殺人事件</a></p></td>
<td><p><a href="../Page/淺見光彥.md" title="wikilink">淺見光彥</a></p></td>
<td><p>主演</p></td>
</tr>
<tr class="odd">
<td><p>2018/05/21</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p>新・淺見光彥系列 <a href="http://www.tbs.co.jp/getsuyou-meisaku/20180521/">平家傳說殺人事件</a></p></td>
<td><p><a href="../Page/淺見光彥.md" title="wikilink">淺見光彥</a></p></td>
<td><p>主演</p></td>
</tr>
<tr class="even">
<td><p>2018/07/02</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td><p><a href="../Page/松本清張.md" title="wikilink">松本清張特別企畫</a> <a href="http://www.tv-tokyo.co.jp/hanzainokaiso/">犯罪的回送</a></p></td>
<td><p>岡本和也</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018/07/20</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td></td>
<td><p>金田一修</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018/07/30</p></td>
<td><p><a href="../Page/毎日放送.md" title="wikilink">毎日放送</a> <a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td></td>
<td><p>神崎拓也</p></td>
<td></td>
</tr>
</tbody>
</table>

### 網路劇

  - タケル
    新人捜査官ファイル（2009年1月21日－2009年3月18日、[日本電視台](../Page/日本電視台.md "wikilink")）：主演・山崎尊\[7\]

  - Oh\! my PROPOSE（2010年6月26日－9月30日）：島田優作

  - （2011年12月25日－2012年3月11日，BeeTV）：伊藤亮介

  - [進撃的巨人 ATTACK ON TITAN
    反撃的狼煙](../Page/進擊的巨人#真人改編.md "wikilink")（2015年8月15日，）：伊祖魯\[8\]

  - [不能犯](https://pc.video.dmkt-sp.jp/ft/s0005109)
    第3話、第4話（2018年1月5日、12日配信，／2018年1月30日、2月6日放送，[關西電視台](../Page/關西電視台.md "wikilink")）：安藤貴哉

### 舞台劇

  - [相對的浮世繪](http://www.g2produce.com/other/soutaiteki/)（2010年3月18日－4月11日）：岬達朗

  - （2011年6月17日－7月9日）：由良市太郎

  - （2011年11月5日－20日）：姫川圖書之助

  - 朗讀劇（2012年3月8日）：安迪

  - [現代能楽集Ⅷ『道玄坂綺譚』](https://setagaya-pt.jp/performances/20151108-6564-6.html)（2015年11月8日－21日）：キイチ／深草貴一郎（一人分飾兩角）

### 廣播劇

  - Season 7（2011年2月6日－27日，[FM東京](../Page/FM東京.md "wikilink")）：松原蓮

### 遊戲配音

  - （2010年7月22日發售）：芳川樹

  - （2010年9月16日發售）：芳川樹

### 綜藝節目

  - （2005年12月26日－2006年5月1日配信，）

  - [夢丘residence](https://web.archive.org/web/20080311051727/http://www.m-on.jp/yumereji/hiraoka/index.html)（2007年4月4日－2008年2月26日，）：星期三主持人

### 雜誌

  - PHaT PHOTO「ひらおかんさつ日記」（2006年10月20日発売号より連載）
  - 月刊ZATOO「平岡祐太『ON THE REAL WORLD』」（2009年 VOL.1 創刊号）

### 廣告

  - [必勝客](../Page/必勝客.md "wikilink")（2004年7月10日－）
  - [寶礦力水得](../Page/寶礦力水得.md "wikilink") -
    與[綾瀨遙共演](../Page/綾瀨遙.md "wikilink")
      - 「走る春」篇（2005年3月5日－）
      - 「見上げる夏」篇（2005年6月18日－）
      - 「メモと昇降口」篇（2005年12月2日－）
  - [小學館](../Page/小學館.md "wikilink")「Telepal f」（2005年9月25日－）
  - [瑞穗銀行](../Page/瑞穗銀行.md "wikilink")「みずほマイレージクラブ」新鮮人篇・店頭海報（2006年2月20日－）
  - [富維克](../Page/富維克.md "wikilink")（2006年12月21日－）
  - [英會話AEON](../Page/AEON_\(會話教室\).md "wikilink")（2007年1月－）
  - [NTT Communications](../Page/日本電信電話.md "wikilink")（2009年6月20日－）
      - 「コムくん登場篇（あんしんナンバー）」（2009年6月20日－）
      - 「香りを届けること篇（香り通信）」（2009年8月29日－）
      - 「外出しなくちゃ篇（OCNモバイル）」（2009年9月24日－）
      - 「見守る篇（ＯＣＮキッズケア）」（2009年12月12日－）
      - 「ペットに○○篇（ワンにゃんバー）」（2010年2月11日－）
  - [P\&G](../Page/P&G.md "wikilink")「ボールド」<ref>



</ref>

  -   - ときめき・お洗濯
        巴士篇（2009年7月15日－2010年1月31日）：與[大塚寧寧共演](../Page/大塚寧寧.md "wikilink")
      - ときめき・お洗濯 公園篇（2010年3月10日－）：與共演

  - [三得利](../Page/三得利.md "wikilink")「インテリゲン」インテリゲンの唄（2010年7月6日－）：與共演

  - 「HYDRO-TECH BLACK COLLECTION」商品代言（2012年3月22日－）<ref>

</ref>

  - [SMIRNOFF](../Page/斯米諾伏特加.md "wikilink")（2012年5月7日－）\[9\]

      - 「平岡祐太 音楽とSMIRNOFF TIME」（2012年5月7日－）
      - 「好きな音楽と酔う」篇（2013年4月19日－）

  - [花王](../Page/花王.md "wikilink")「トイレマジックリン
    消臭・洗浄スプレーAROMA」（2013年9月12日－）：與共演

  - [花王](../Page/花王.md "wikilink")「リセッシュリセッシュ・除菌EXデオドラントパワー」（2014年2月－4月）

  - 「サッポロ一番 塩とんこつラーメン」塩顔イケメン篇（2015年1月23日－）

  - 「10周年感謝祭」（2015年3月16日－）<ref>



</ref>

  - GISELe HOMMe 品牌代言（2015年3月28日－）
  - [雪印メグミルク](../Page/雪印乳業.md "wikilink")
    「重ねドルチェ」-{大人女子}-の贅沢なひと時プレゼントキャンペーン<ref>


</ref>

  -
    應募期間：2017年4月1日－5月31日
    活動日期：2017年6月25日

<!-- end list -->

  - 「平岡祐太のロイヒ博士モザイクポスター もらえる\!キャンペーン」\[10\]

<!-- end list -->

  -
    應募期間：2017年2月15日－3月31日
    活動期間：東京・新宿駅（2017年4月10日－16日）／大阪・藤井寺駅（2017年4月14日－20日）

<!-- end list -->

  - 「ながら運転防止啓発動画」（2017年7月7日－10月26日）

  - 「[午後紅茶](../Page/午後紅茶.md "wikilink")×[Pocky](../Page/Pocky.md "wikilink")」合作企劃第4彈（2018年2月20日－）<ref>

</ref>

  -
    〜スマートフォン限定スペシャル動画〜 平岡祐太さんと、ハワイデートが楽しめる！360°ムービー

### 旁白

  - Messi TV（2011年3月16日－2011年4月13日，[WOWOW](../Page/WOWOW.md "wikilink")）
  - [岩合光昭的貓步走世界](../Page/岩合光昭.md "wikilink")「[蘇連多和](../Page/蘇連多.md "wikilink")[卡布里島](../Page/卡布里島.md "wikilink")」（2012年8月8日，[NHK
    BS Premium](../Page/NHK_BS_Premium.md "wikilink")）
  - [岩合光昭的貓步走世界](../Page/岩合光昭.md "wikilink")「總集篇～[地中海的貓](../Page/地中海.md "wikilink")～」（2013年1月5日，[NHK
    BS Premium](../Page/NHK_BS_Premium.md "wikilink")）

### 活動

  - MERRY X'MAS
    PARTY（2005年12月23日，[新宿KOMA劇場](../Page/新宿KOMA劇場.md "wikilink")）
  - AMUSE PRESENTS「バラエティショー2006～真冬のどんちゃん騒ぎ～」（2006年12月28日，天王洲銀河劇場）

### PV

  - [Mr.Children](../Page/Mr.Children.md "wikilink")「未來」（2005年）
  - [Ms.OOJA](../Page/Ms.OOJA.md "wikilink")「30」（2013年）\[11\]\[12\]

### 其他

  - [Nexon](../Page/Nexon.md "wikilink")「わたしの浮島自慢コンテスト」特別審査員（2012年12月26日－2013年1月14日）<ref>


</ref>

  - [The Rolling
    Stones](../Page/滚石乐队.md "wikilink")「平岡祐太２０１３コラボTシャツ」聯名設計T恤（2013年5月13日－）\[13\]
  - [LACOSTE](../Page/拉科斯特_\(品牌\).md "wikilink")「LACOSTE BEAUTIFUL
    AWARDS featuring LESLIE KEE」ACTOR AWARD（2014年）\[14\]\[15\]
  - 一般社團法人山口縣觀光連盟 [おいでませ山口へ](http://www.oidemase.or.jp/)
    [觀光手冊特集「平岡祐太と巡る維新の舞台」](http://www.oidemase.or.jp/yamaguchi-dc/images/pdf/eg_201709-12.pdf)（2017年8月發行）
  - [NHK](../Page/日本廣播協會.md "wikilink")[「⇒２０２０
    レスリー・キーがつなぐポートレートメッセージ」](http://www.nhk.or.jp/tokyo2020/change/messages/main/58.html)（2017年10月27日）

## 參考資料

<references />

## 外部連結

  - [YUTA HIRAOKA OFFICIAL WEBSITE](http://www.hiraokayuta.com/)

  -
  - [Amuse官網個人介紹](http://artist.amuse.co.jp/artist/hiraoka_yuta/)

[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")
[Category:山口縣出身人物](../Category/山口縣出身人物.md "wikilink")
[Category:JUNON BOY](../Category/JUNON_BOY.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本電視演員](../Category/日本電視演員.md "wikilink")
[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:日本舞台劇演員](../Category/日本舞台劇演員.md "wikilink")
[Category:日本電影學院獎新人獎得主](../Category/日本電影學院獎新人獎得主.md "wikilink")
[Category:Amuse所屬藝人](../Category/Amuse所屬藝人.md "wikilink")

1.  [平岡祐太 - 略歴・フィルモグラフィー
    -KINENOTE（キネノート）](http://www.kinenote.com/main/public/cinema/person.aspx?person_id=197130)
2.  [男の履歴書　平岡祐太｜インライフ](http://www.inlifeweb.com/reports/report_4132.html)
3.
4.
5.  [Amuse Group - Taiwan -
    貼文](https://www.facebook.com/amusetaiwan/posts/553206978065988)
6.  [北野武《壞孩子》開拍續集
    平岡祐太苦練肌肉](http://news.cri.cn/gb/27564/2013/07/19/3465s4188162.htm)
7.  [タケル 新人捜査官ファイル｜日本テレビ](http://www.ntv.co.jp/kiina/spinoff.html)
8.  [巨人進擊電視 石原里美學尖叫](http://ent.ltn.com.tw/news/paper/869452)
9.
10.
11.
12.
13.
14.
15.