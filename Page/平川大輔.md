**平川
大輔**（）是日本的男性[聲優](../Page/聲優.md "wikilink")、[歌手](../Page/歌手.md "wikilink")、[旁白](../Page/旁白.md "wikilink")。自由身。出身於[新潟縣](../Page/新潟縣.md "wikilink")。[血型為A型](../Page/血型.md "wikilink")。身高167cm，體重52kg。

## 概要

### 來歷

  - 勝田聲優學院11期畢業生。同期的聲優有、[小西克幸](../Page/小西克幸.md "wikilink")、[倉田雅世](../Page/倉田雅世.md "wikilink")、[萩原惠美子](../Page/萩原惠美子.md "wikilink")，當時隸屬於。
  - 出道作是在飛機上放映的『[西藏七年](../Page/西藏七年.md "wikilink")』中的僧侶等等的無名角色吹替，早期主要是參與海外作品吹替，直到2004年正式參與動畫配音，為[巖窟王法蘭茲](../Page/巖窟王.md "wikilink")·德皮利男爵一角進行配音演繹，有更多機會為動畫配音。

<!-- end list -->

  - 自魔戒系列電影以來，是好萊塢明星[奧蘭多·布魯的日語吹替](../Page/奧蘭多·布魯.md "wikilink")，近年也是韓國明星[張根碩的吹替](../Page/張根碩.md "wikilink")。主要是少年到青年的演繹，或者是圍繞着主角的成年人角色的擔當。其他代表作有『[我的裘可妹妹](../Page/我的裘可妹妹.md "wikilink")』的川越春馬、動畫『[淘氣小親親](../Page/淘氣小親親.md "wikilink")』中的入江直樹、『[新安琪莉可](../Page/新安琪莉可.md "wikilink")』系列的貝爾納、『[超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink")』的、『[School
    Days](../Page/School_Days.md "wikilink")』的伊藤誠等等。他同時也是台灣偶像劇『[惡作劇之吻](../Page/惡作劇之吻.md "wikilink")』中入江直樹的日語配音。

### 特色

  - 聲音的範圍相當的廣，從像是『ロビーとケロビー』中的タナカ那樣的高音來演出。還有『[神鬼奇航](../Page/神鬼奇航.md "wikilink")』中威爾那樣的善良青年、『[School
    Days](../Page/School_Days.md "wikilink")』中優柔寡斷的伊藤誠、『淘氣小親親』中入江直樹型的天才帥哥，或是用低沉聲音演出冷酷型在[超級機器人大戰Z中登場的怪人ジ](../Page/超級機器人大戰Z.md "wikilink")・エーデル等等多方面的表現，FANS對此表示「相當尊敬」。

### 人物

  - 因為喜歡和親戚或是朋友的孩子玩，從前曾經想要成為幼稚園老師。但是有一天突然有了「我要成為聲優」的念頭。理由是「如果在像動畫卡通那樣子的兒童節目中演出的話，那就可以帶給很多小朋友快樂了。所以，同樣是演出的工作，但像是演員或舞台劇之類的工作卻沒有考慮過。」至於成為聲優的動機，則曾表示過「正在思考中」（出自ENTERBRAIN出版「B's
    LOG」2006年3月號採訪內容P.111『声王子』）。
  - 自認無能（ヘタレ），在[小林由美子的廣播節目](../Page/小林由美子.md "wikilink")「我們都很無能呢」中偶然提到而討論熱烈。似乎組成了一個都是無能的「無能同盟」（人數等詳細不明），在某DRAMA
    CD中的free talk中曾邀請[近藤隆加入](../Page/近藤隆.md "wikilink")。
  - 2008年10月以前從沒有搭過飛機，曾在『[蟲之歌](../Page/蟲之歌.md "wikilink")』9話預告中曾說出「為了環遊世界首先我會著手於取得護照這件事」。而在與[岸尾大輔一起主持的WEB](../Page/岸尾大輔.md "wikilink")
    radio『シュガービーンズ放送局』17回中發表「已經取得護照了」的消息。2006年末本來要去韓國但因為工作而作罷，直到2008年11月參加在韓國舉辦的見面會，實現了第一次出國、第一次坐飛機的夢想。
  - 在『新安琪莉可Abyss
    〜歡迎來到向陽邸〜』中決定了暱稱，由於被同是主持人的[小野大輔喊做](../Page/小野大輔.md "wikilink")**お兄ちゃん**（哥哥），而聽眾們也跟著這樣稱呼他，因而決定下來。

<!-- end list -->

  - 曾經屬於Media Force旗下的聲優，但公司相關的事務所Dream
    Force在2014年2月23日宣佈破產；在1月末與旗下聲優解約，自此平川大輔變回自由身。

## 參與作品

※擔任主角者角色名以粗體表示

### 電視動畫

**1998年〜2003年**

  - [夢幻天女](../Page/夢幻天女.md "wikilink")（芳塚廣和）
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（男子）
  - [天才小魚郎](../Page/天才小魚郎.md "wikilink")（運動員）

**2004年**

  - [御伽草子](../Page/御伽草子.md "wikilink")（檢非違使）
  - [巖窟王](../Page/巖窟王.md "wikilink")（法蘭茲·德皮利男爵）
  - KURAU Phantom Memory （エド）
  - [攻殼機動隊 S.A.C. 2nd
    GIG](../Page/攻殼機動隊_S.A.C._2nd_GIG.md "wikilink")（警官、招慰難民）
  - [BECK](../Page/BECK.md "wikilink")（椎木祐介）

**2005年**

  - [韋駄天翔](../Page/韋駄天翔.md "wikilink")（神崎聖也）
  - [我的太太是魔法少女](../Page/我的太太是魔法少女.md "wikilink")（翔）
  - [SHUFFLE\!](../Page/SHUFFLE!.md "wikilink")（瀧澤巨規）
  - [變形金剛 銀河原力](../Page/變形金剛_銀河原力.md "wikilink")（戰士艾克西里昂、武裝戰士艾克西凱撒）
  - [風人物語](../Page/風人物語.md "wikilink")（電影中的男性）

**2006年**

  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（赤羽隼人）
  - [死亡代理人](../Page/死亡代理人.md "wikilink")（オートレイヴA）
  - [童話槍手小紅帽](../Page/童話槍手小紅帽.md "wikilink")（漢賽爾）
  - [大長今](../Page/大長今_\(動畫\).md "wikilink")（中宗、刺客）
  - [校園迷糊大王 二學期](../Page/校園迷糊大王.md "wikilink")（田中一也）
  - [未來都市NO.6](../Page/未來都市NO.6.md "wikilink")（山勢）（第2集）
  - [我的裘可妹妹](../Page/我的裘可妹妹.md "wikilink")（**川越春馬**）

**2007年**

  - [英國戀物語艾瑪 第二幕](../Page/艾瑪_\(漫畫\).md "wikilink")（）
  - [SHUFFLE\! Memories](../Page/SHUFFLE!.md "wikilink") （）
  - [結界師](../Page/結界師.md "wikilink")（八王子君也）
  - [School Days](../Page/School_Days.md "wikilink")（**伊藤誠**）
  - [Saint Beast](../Page/Saint_Beast.md "wikilink") ～光陰敘事詩天使譚～（）
  - [零之使魔 ～雙月的騎士～](../Page/零之使魔.md "wikilink")（朱利歐·薩切雷）
  - [奔向地球](../Page/奔向地球.md "wikilink")（客人）
  - [蟲之歌](../Page/蟲之歌.md "wikilink")（**土師圭吾**）

**2008年**

  - [死後文](../Page/死後文.md "wikilink")（森下俊輔）
  - [淘氣小親親](../Page/淘氣小親親.md "wikilink")（**入江直樹**）
  - [新·安琪莉可](../Page/新·安琪莉可.md "wikilink") （**貝爾納**）
  - 新·安琪莉可-Second Age-（**貝爾納**）
  - [零之使魔 ～三美姬的輪舞～](../Page/零之使魔.md "wikilink")（朱利歐·薩切雷）
  - [記憶女神的女兒們](../Page/記憶女神的女兒們.md "wikilink")（前埜 輝紀）
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink") （空（））
  - [藥師寺涼子之怪奇事件簿](../Page/藥師寺涼子之怪奇事件簿.md "wikilink")（野長瀬一馬）

**2009年**

  - [香格里拉 (科幻小說)](../Page/香格里拉_\(科幻小說\).md "wikilink")（秦總一郎）
  - [BLEACH](../Page/BLEACH.md "wikilink")（千本櫻）
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（オウ）
  - [旋風管家 第二季](../Page/旋風管家.md "wikilink")（加賀北斗）

**2010年**

  - [守護貓娘緋鞠](../Page/守護貓娘緋鞠.md "wikilink")（**天河優人**）
  - [SD高達三國傳BraveBattleWarriors](../Page/SD高達三國傳BraveBattleWarriors.md "wikilink")（**周瑜百式**）
  - [學園默示錄](../Page/學園默示錄.md "wikilink")（田島）
  - [百花繚亂 SAMURAI
    GIRLS](../Page/百花繚亂_SAMURAI_GIRLS.md "wikilink")（**柳生宗朗**）
  - [Starry☆Sky](../Page/Starry☆Sky.md "wikilink")（**青空颯斗**）

**2011年**

  - [境界線上的地平線](../Page/境界線上的地平線.md "wikilink")（野挽、念治）
  - [與殿下一起
    ～眼罩的野望～](../Page/與殿下一起_～眼罩的野望～.md "wikilink")（[香宗我部親泰](../Page/香宗我部親泰.md "wikilink")、[明智光秀](../Page/明智光秀.md "wikilink")）
  - [遊戲王ZEXAL](../Page/遊戲王ZEXAL.md "wikilink")（德魯貝）

**2012年**

  - [零之使魔F](../Page/零之使魔.md "wikilink")（朱力歐·切薩雷）
  - [緋色的碎片](../Page/緋色的碎片.md "wikilink")（**大蛇卓**）
  - [冰菓](../Page/冰菓.md "wikilink")（料理研究會副部長）
  - [遊戲王ZEXAL II](../Page/遊戲王ZEXAL_II.md "wikilink")（德魯貝）

**2013年**

  - [魔王勇者](../Page/魔王勇者.md "wikilink")（冬寂王）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（練白雄）
  - [黑色嘉年華](../Page/黑色嘉年華.md "wikilink")（**燭**）
  - [宇宙戰艦大和號2199](../Page/宇宙戰艦大和號2199.md "wikilink")（筱原弘树）※2012年起于电影院先行上映
  - [百花繚亂 SAMURAI
    BRIDE](../Page/百花繚亂_SAMURAI_GIRLS.md "wikilink")（**柳生宗朗**）
  - [旋風管家！Cuties](../Page/旋風管家_\(動畫\).md "wikilink")（加賀北斗）
  - [BROTHERS
    CONFLICT](../Page/BROTHERS_CONFLICT.md "wikilink")（**朝日奈右京**）
  - [Free\!](../Page/Free!.md "wikilink")（**-{龍崎怜}-**）
  - [DIABOLIK LOVERS](../Page/DIABOLIK_LOVERS.md "wikilink")（**逆卷禮人**）

**2014年**

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**桃太郎**）
  - [WIZARD
    BARRISTERS～弁魔士賽希爾](../Page/WIZARD_BARRISTERS～弁魔士賽希爾.md "wikilink")（教師）
  - [JOJO的奇妙冒險 星塵鬥士](../Page/JOJO的奇妙冒險.md "wikilink")（**花京院典明**）
  - [Free\!-Eternal Summer-](../Page/Free!.md "wikilink")（**-{龍崎怜}-**）
  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（矢野）
  - [閃爍的青春](../Page/閃爍的青春.md "wikilink")（**田中阳一**）
  - [LOVE STAGE\!\!](../Page/LOVE_STAGE!!.md "wikilink")（**相樂玲**）
  - [偶像學園3](../Page/偶像學園_\(動畫\).md "wikilink")（四葉春）

**2015年**

  - [元氣少女緣結神◎](../Page/元氣少女緣結神.md "wikilink")（翠郎）
  - [新妹魔王的契約者](../Page/新妹魔王的契約者.md "wikilink")（斯波恭一）
  - [JOJO的奇妙冒險 星塵鬥士](../Page/JOJO的奇妙冒險.md "wikilink")（**花京院典明**）
  - [ALDNOAH.ZERO 第2季](../Page/ALDNOAH.ZERO.md "wikilink")（哈克萊特）
  - [雨色可可](../Page/雨色可可.md "wikilink")（**古賀紫苑**）\[1\]
  - [其實我是](../Page/其實我是.md "wikilink")（岡田）
  - [魔鬼戀人 MORE,BLOOD](../Page/魔鬼戀人.md "wikilink")（**逆卷禮人**）\[2\]
  - [雨色可可 Rainy color歡迎您的光臨！](../Page/雨色可可.md "wikilink")（**古賀紫苑**）\[3\]
  - [高校星歌劇](../Page/高校星歌劇.md "wikilink")（柊翼）
  - [與魔共舞](../Page/與魔共舞_\(日本\).md "wikilink")（**棗坂四季**）
  - [櫻子小姐腳下埋著屍體](../Page/櫻子小姐腳下埋著屍體.md "wikilink")（藤岡毅）
  - [終結的熾天使 名古屋決戰篇](../Page/終結的熾天使.md "wikilink")（**岩咲秀作**、隊長）

**2016年**

  - [疾走王子Alternative](../Page/疾走王子.md "wikilink")（黛靜馬）
  - [亞人](../Page/亞人_\(漫畫\).md "wikilink")（**田中功次**）
  - [偶像學園STARS！](../Page/偶像學園STARS！.md "wikilink")（諸星輝）
  - [烙印勇士](../Page/烙印勇士.md "wikilink")（傑洛姆）
  - [半田君傳說](../Page/元氣囝仔.md "wikilink")（永正惣一）
  - [美男高校地球防衛部LOVE！LOVE](../Page/美男高校地球防衛部LOVE！.md "wikilink")！（巡屋圓太郎）
  - [終末的伊澤塔](../Page/終末的伊澤塔.md "wikilink")（艾利奧特）
  - 雨色可可 in Hawaii（古賀紫苑）
  - 亞人 第二期（**田中功次**）

**2017年**

  - [TRICKSTER
    －來自江戶川亂步「少年偵探團」－](../Page/TRICKSTER_－來自江戶川亂步「少年偵探團」－.md "wikilink")（小林昭文）

  - [風夏](../Page/風夏.md "wikilink")（尚志）

  - [KiraKira☆光之美少女 A La
    Mode](../Page/KiraKira☆光之美少女_A_La_Mode.md "wikilink")（艾利西奧）

  - （織田） \[4\]

  - [高校星歌劇 第2期](../Page/高校星歌剧.md "wikilink")（柊翼）

  - [青春歌舞伎](../Page/青春歌舞伎.md "wikilink")（八卷屋）

  - [在地下城尋求邂逅是否搞錯了什麼](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink") 外傳
    劍姬神聖譚（艾絲父親）

  - [潔癖男子青山！](../Page/潔癖男子青山！.md "wikilink")（門-{松}-了）

  - [梵諦岡奇蹟調查官](../Page/梵諦岡奇蹟調查官.md "wikilink")（マギー神父）

  - [將國戡亂記](../Page/將國戡亂記.md "wikilink")（卡瓦哈爾）

  - [Infini-T Force](../Page/Infini-T_Force.md "wikilink")（達米安·格雷）

  - [Code:Realize
    ～創世的公主～](../Page/Code:Realize_～創世的公主～.md "wikilink")（**聖日耳曼**）

  - 鬼燈的冷徹 第貳期（**桃太郎**）

  - [泥鯨之子們在沙地上歌唱](../Page/泥鯨之子們在沙地上歌唱.md "wikilink")（阿拉夫尼）

**2018年**

  - [伊藤潤二驚選集](../Page/伊藤潤二驚選集.md "wikilink")（山田、誠）
  - [一人之下 羅天大醮篇](../Page/一人之下.md "wikilink")（**諸葛青**）
  - [霸穹 封神演義](../Page/封神演義_\(漫畫\).md "wikilink")（太乙真人）
  - [博多豚骨拉麵團](../Page/博多豚骨拉麵團.md "wikilink")（佐伯）
  - [妖怪旅館營業中](../Page/妖怪旅館營業中.md "wikilink")（時彥）
  - 鬼燈的冷徹 第貳期 其之貳（**桃太郎**）
  - [千銃士](../Page/千銃士.md "wikilink")（エフ）
  - [音樂少女](../Page/音樂少女.md "wikilink")（池橋大輝\[5\]）
  - [OVERLORD III](../Page/OVERLORD_\(小說\).md "wikilink")（羅伯戴克·戈爾特隆）
  - [Free\!-Dive to the
    Future-](../Page/Free!.md "wikilink")（**-{龍崎怜}-**）
  - [付喪神出租中](../Page/付喪神出租中.md "wikilink")（**五位**\[6\]）
  - [茜色少女](../Page/茜色少女.md "wikilink")（橘田藏人\[7\]）
  - [CONCEPTION](../Page/CONCEPTION_產子救世錄.md "wikilink")（納爾西斯特斯\[8\]）

**2019年**

  - [家有女友](../Page/家有女友.md "wikilink")（萩原柊）

### OVA

**2004年**

  - [HUNTER×HUNTER](../Page/HUNTER×HUNTER.md "wikilink") OVA
    G・I（阿本加聶、初代果列奴）
  - [TALES OF PHANTASIA](../Page/TALES_OF_PHANTASIA.md "wikilink") THE
    ANIMATION（ ）

**2005年**

  -
**2004年**

  - （****）

**2008年**

  - （**伊藤誠**）

**2009年**

  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/THE_LOST_CANVAS_冥王神話_\(動畫\).md "wikilink")（**水瓶座
    迪捷爾**）

**2010年**

  - [恋爱暴君](../Page/恋爱暴君.md "wikilink")（ヒロト)
  - [與殿下一起](../Page/與殿下一起.md "wikilink")（明智光秀）
  - [眼鏡女友](../Page/眼鏡女友.md "wikilink")（**宮口孝史**）

**2011年**

  - [心之國的愛麗絲](../Page/心之國的愛麗絲.md "wikilink")（**Ace**）
  - [這個男子，能與宇宙人作戰](../Page/這個男子，能與宇宙人作戰.md "wikilink")（**シロ**）

**2012年**

  - [探索者的目標](../Page/探索者的目標.md "wikilink")（コウ）

**2013年**

  - [偷窺孔](../Page/偷窺孔.md "wikilink")（**城戶龍彥**）
  - [黒と金の开かない键](../Page/黒と金の开かない键.md "wikilink")（绀野千纮）
  - [「PEACE
    MAKER铁」油小路篇](../Page/「PEACE_MAKER铁」油小路篇.md "wikilink")（伊东甲子太郎）

**2014年**

  - [LOVE STAGE\!\!](../Page/LOVE_STAGE!!.md "wikilink")（**相樂玲**）
  - [眷戀你的溫柔](../Page/眷戀你的溫柔.md "wikilink")（**葉月**）
  - [这個男子，正為石化所惱。](../Page/这個男子，正為石化所惱。.md "wikilink")（**穗仁原鉱也**）

**2015年**

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**桃太郎**）
  - [探索者的羽翼](../Page/探索者的羽翼.md "wikilink")（コウ）

**2016年**

  - [高校星歌劇](../Page/高校星歌劇.md "wikilink") 第1期 OVA1（柊翼） \[9\]
  - 高校星歌劇 第1期 OVA2（柊翼） \[10\]
  - [亞人](../Page/亞人_\(漫畫\).md "wikilink")（**田中功次**）※漫畫第9卷附OAD限定版

**2017年**

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink") OAD4（**桃太郎**）

**2018年**

  - [Free\!－Dive to the
    Future－第](../Page/Free!.md "wikilink")0話（-{龍崎怜}-）

### 動畫電影

**2005年**

  - [劇場版xxxHOLiC仲夏夜之夢](../Page/×××HOLiC#劇場版.md "wikilink")（男子C）

**2008年**

  - [空中殺手The Sky Crawlers](../Page/空中杀手#剧场版动画.md "wikilink")（湯田川亞伊豆·合原）

**2011年**

  - [心之国的爱丽丝～Wonderful Wonder
    World～](../Page/心之国的爱丽丝～Wonderful_Wonder_World～.md "wikilink")（Ace）

**2012年**

  - [神秘之法](../Page/神秘之法.md "wikilink")（**塔塔卡塔尔·奇拉**）

**2013年**

  - [大怪兽RUSH ULTRA FRONTIER DINO-TANK
    hunting](../Page/大怪兽RUSH_ULTRA_FRONTIER_DINO-TANK_hunting.md "wikilink")（**巴尔坦巴特拉·巴雷鲁**）

**2014年**

  - [剧场版 TIGER & BUNNY -The
    Rising-](../Page/TIGER_&_BUNNY.md "wikilink")（VIRGIL DINGFELDER /
    アンドリュー・スコット）

**2015年**

  - [亞人 -衝動-](../Page/亞人_\(漫畫\).md "wikilink")（**田中功次**）

**2017年**

  - [劇場版 Free\!-Timeless
    Medley-](../Page/Free!.md "wikilink")（**-{龍崎怜}-**）
  - 特別版 Free\!-Take Your Marks-（**-{龍崎怜}-**）

### 遊戲

  - [光速蒙面俠21球場上最強的戰士們](../Page/光速蒙面俠21.md "wikilink")：赤羽隼人
  - [SD鋼彈G世代SPIRITS](../Page/SD鋼彈G世代.md "wikilink")：ハリソン・マディン
  - きみスタ〜きみとスタディ〜：清宮雅貴
  - [王國之心II](../Page/王國之心.md "wikilink")：威爾·杜納
  - [閃靈二人組 裏新宿最強BATTLE](../Page/閃靈二人組.md "wikilink")：マンジ兄弟の兄
  - [白騎士物語](../Page/白騎士物語.md "wikilink")：シーザー
  - [超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink")：**ジ・エーデル・ベルナル**
  - [School Days L×H](../Page/School_Days.md "wikilink")：**伊藤誠**
  - iアプリ テイルズオブブレイカー：ユーテキ、ザウバー
  - 天地の門2 武双伝：リジュ・ロウ
  - [古墓奇兵3](../Page/古墓奇兵.md "wikilink")
  - とらぶるふぉうちゅんCOMPANY☆はぴCURE：主人公の父親
  - [BALDR FORCE EXE](../Page/BALDR_FORCE.md "wikilink")：柏木洋介
  - 愛神餐館2 特別版：ディル・ベルガモット（※特別版同封スペシャルCD「セルリー編」（PS2版）・「ネメシア編」（X-BOX版）に出演）
  - ピリオド：漣雄一
  - FEVER7 SANKYO公式パチンコシミュレーション：ゲージろう
      - FEVER8 SANKYO公式パチンコシミュレーション：ゲージろう
  - マリア2 受胎告知の謎：水谷敦
  - 武士神槍斬
  - [刺客教條：梟雄](../Page/刺客教條：梟雄.md "wikilink")： '''雅各·弗來 '''
  - [魔戒](../Page/魔戒首部曲：魔戒現身.md "wikilink")
    系列：**[勒苟拉斯](../Page/勒苟拉斯.md "wikilink")**
  - 花降樓シリーズ(1)君も知らない邪戀の果てに：綺蝶

<!-- end list -->

1.  花降樓シリーズ(2)愛で痴れる夜の純情：綺蝶
2.  花降樓シリーズ(4)婀娜めく華、手摺られる罪：綺蝶

<!-- end list -->

  - \[PS2/PC\]：**樱川鹰士**
      - \[PSP\]乙女的恋革命 携带版
  - 愛麗絲系列：**心之骑士艾斯**
      - \[PC/PS2/PSP\]
      - \[PC/PS2\]三葉草之國的爱丽丝
      - \[PC\]小丑之国的爱丽丝
      - \[PC\]纪念日之国的爱丽丝
      - \[PSP\]玩具匣之国的爱丽丝
      - \[PSP\]鑽石之国的愛丽丝
  - \[PC\]：**城崎焔**、弗朗茨
  - \[DS\][好逑少女系列](../Page/好逑少女.md "wikilink")：**巢鸭薰**
      - 好逑少女 夏色射手
      - 好逑少女2 两个翠！？
  - \[PC\][神无之鸟](../Page/神无之鸟.md "wikilink")：主、斑鳩
  - \[PC\][鬼畜眼鏡 KICHIKU
    MEGANE](../Page/鬼畜眼鏡_KICHIKU_MEGANE.md "wikilink")：**佐伯克哉**
      - \[PC\]鬼畜眼鏡R KICHIKU MEGANE R
  - \[PC\][初音島Girl's
    Symphony](../Page/初音島Girl's_Symphony.md "wikilink")：**古城孝明**
      - \[PSP\]初音岛Girl's Symphony 携带版
  - \[PC\]：**恶魔乌列尔/乌鲁鲁**
  - \[PS2\][咎狗之血 True Blood](../Page/咎狗之血.md "wikilink")：
  - \[PS2\][新·安琪莉可](../Page/新·安琪莉可.md "wikilink") ：**貝爾納**
      - \[PS2\]新·安琪莉可 全语音版
      - \[PSP\]新·安琪莉可 Special
  - [绯色的碎片系列](../Page/绯色的碎片.md "wikilink")：**大蛇卓**
      - \[DS\]绯色的碎片 DS
      - \[PSP\]绯色的碎片 携带版
      - \[PS2\]绯色的碎片 爱藏版
      - \[PS2\]绯色的碎片 ～在那天空下～
      - \[PS2\]翡翠之雫 绯色的碎片2
      - \[PS2\]真·翡翠之雫 绯色的碎片2
      - \[PSP\]真·翡翠之雫 绯色的碎片2 携带版
      - \[PS2\]苍黑之楔 绯色的碎片3
      - \[PSP\]苍黑之楔 绯色的碎片3 携带版
      - \[PS2\]苍黑之楔 绯色的碎片3
      - \[PSP\]苍黑之楔 绯色的碎片3 迎向明天 携带版
      - \[PS2\]绯色的碎片 新玉依姬传承：**大蛇凌**
      - \[PSP\]绯色的碎片 新玉依姬传承 携带版：**大蛇凌**
  - \[PC\]：**山崎充範**
      - \[PS2\]
  - \[PC\]蝶之毒華之鎖：瑞人（みずひと）
  - \[PC\][Starry☆Sky ～in
    Winter～](../Page/Starry☆Sky.md "wikilink")：**青空颯斗**
      - \[PSP\]Starry☆Sky ～in Winter～ 携带版
      - \[PC\]Starry☆Sky ～after Winter～
  - \[PC/PS2\]：**贾斯汀·罗比拉蒂**
      - \[PC\]绯红王室
  - \[PSP\]绝对迷宫格林 七把钥匙与乐园的少女：**威廉·格林**
  - \[PS2\]：桂小五郎
  - \[PC\]GARNET CRADLE：**樱泽辉一郎**
      - \[PC\]GARNET CRADLE sugary sparkle
  - \[DS\]：拉尔夫·格雷萨
  - \[PC\]Happy☆Magic\!：**日向绀**
  - \[PS2\]星色礼物：**鸣泷宗哉**
      - \[PSP\]星色礼物 携带版
  - \[PSP\]十三支演義 〜偃月三國伝〜：[袁紹](../Page/袁紹.md "wikilink")
  - \[PSP\]DIABOLIK LOVERS ：**逆巻ライト**
  - [英雄傳說 軌跡系列](../Page/英雄傳說_軌跡系列.md "wikilink")：曹・李、路法斯・亞爾巴雷亞
      - \[PS VITA\] [英雄傳說 零之軌跡
        Evolution](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 閃之軌跡](../Page/英雄傳說_閃之軌跡.md "wikilink")
  - [PC裏語](../Page/PC.md "wikilink")
    薄櫻鬼：[中冈慎太郎](../Page/中冈慎太郎.md "wikilink")
  - \[PSP\][宵夜森林的公主](../Page/宵夜森林的公主.md "wikilink")：**米利亞姆**
  - [白貓Project](../Page/白貓Project.md "wikilink")：オズワルド
  - \[Android/ios\][夢王國與沉睡的100位王子殿下](../Page/夢王國與沉睡的100位王子殿下.md "wikilink")：瘋帽子
  - \[Android/ios\][在茜色世界與君詠唱](../Page/在茜色世界與君詠唱.md "wikilink")：江戶川亂步
  - \[Android/ios\][戰刻夜想曲](../Page/戰刻夜想曲.md "wikilink")：毛利隆元
  - \[Android/ios\] 格林筆記：魅影
  - \[Android/ios\] 陰陽師：荒
  - 戀與製作人：許墨
  - 夢色卡司：朱道岳

### 人偶剧

**2016年**

  - [Thunderbolt Fantasy
    東離劍遊紀](../Page/Thunderbolt_Fantasy_東離劍遊紀.md "wikilink")（丹衡）

### 吹替

**電視/電影/廣告**

<table>
<thead>
<tr class="header">
<th><p>作品年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>演員</p></th>
<th><p>媒介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/雷神奇俠.md" title="wikilink">雷神奇俠</a></p></td>
<td><p>rowspan="3|<strong>Loki</strong></p></td>
<td><p><a href="../Page/湯·希丹斯頓.md" title="wikilink">湯·希丹斯頓</a></p></td>
<td><p>公映/影碟版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/復仇者聯盟.md" title="wikilink">復仇者聯盟</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/雷神奇俠2：黑暗世界.md" title="wikilink">雷神奇俠2：黑暗世界</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/積架.md" title="wikilink">Jaguar XE係列汽車廣告</a></p></td>
<td><p><strong>湯姆·希德斯頓</strong></p></td>
<td><p>積架公司宣傳廣告</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2015</strong></p></td>
<td><p><a href="../Page/腥紅山莊.md" title="wikilink">腥紅山莊</a></p></td>
<td><p><strong>Thomas Sharpe</strong></p></td>
<td><p>影碟版本</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/摩天樓_(2015年電影).md" title="wikilink">魔天豪廷</a></p></td>
<td><p><strong>Robert Laing</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2016</strong></p></td>
<td></td>
<td><p><strong>Hank Williams</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2017</strong></p></td>
<td><p><a href="../Page/雷神奇俠3：諸神黃昏.md" title="wikilink">雷神奇俠3：諸神黃昏</a></p></td>
<td><p><strong>洛基</strong></p></td>
<td><p>公映/影碟版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2018</strong></p></td>
<td><p><a href="../Page/復仇者聯盟3.md" title="wikilink">復仇者聯盟3</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/魔戒首部曲：魔戒現身.md" title="wikilink">魔戒首部曲：魔戒現身</a></p></td>
<td><p><strong>勒苟拉斯</strong></p></td>
<td><p><a href="../Page/奧蘭度·布林.md" title="wikilink">奧蘭度·布林</a></p></td>
<td><p>公映/影碟版本</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑鷹計劃.md" title="wikilink">黑鷹計劃</a></p></td>
<td><p>陶德·布拉朋</p></td>
<td><p>東京電視台版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002</strong></p></td>
<td><p><a href="../Page/魔戒二部曲：雙城奇謀.md" title="wikilink">魔戒二部曲：雙城奇謀</a></p></td>
<td><p><strong>勒苟拉斯</strong></p></td>
<td><p>公映/影碟版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2003</strong></p></td>
<td><p><a href="../Page/魔戒三部曲：王者再臨.md" title="wikilink">魔戒三部曲：王者再臨</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔盜王決戰鬼盜船.md" title="wikilink">魔盜王決戰鬼盜船</a></p></td>
<td><p><strong>威爾·杜納</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/特洛伊：木馬屠城.md" title="wikilink">特洛伊：木馬屠城</a></p></td>
<td><p><strong>帕里斯</strong></p></td>
<td><p>公映/錄像帶/影碟版本<br />
東京電視台版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/伊麗莎白小鎮.md" title="wikilink">伊麗莎白小鎮</a></p></td>
<td><p><strong>Drew Baylor/德魯·拜倫</strong></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/血染天堂.md" title="wikilink">血染天堂</a></p></td>
<td><p>蕭</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加勒比海盜：決戰魔盜王.md" title="wikilink">加勒比海盜：決戰魔盜王</a></p></td>
<td><p><strong>威爾·杜納</strong></p></td>
<td><p>公映/影碟版本</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/加勒比海盜：魔盜王終極之戰.md" title="wikilink">加勒比海盜：魔盜王終極之戰</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/凶心仁術.md" title="wikilink">凶心仁術</a></p></td>
<td><p><strong>馬田·比克醫生</strong></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/3D劍客聯盟：雲端之戰.md" title="wikilink">3D劍客聯盟：雲端之戰</a></p></td>
<td><p>白金漢公爵</p></td>
<td><p>東京電視台版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/哈比人：荒谷魔龍.md" title="wikilink">哈比人：荒谷魔龍</a></p></td>
<td><p><strong>勒苟拉斯</strong></p></td>
<td><p>公映/影碟版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/哈比人：五軍之戰.md" title="wikilink">哈比人：五軍之戰</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/祖魯_(2013年电影).md" title="wikilink">祖魯</a></p></td>
<td><p><strong>Brian Epkeen</strong></p></td>
<td><p>影碟版本</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/原來是美男_(韓國電視劇).md" title="wikilink">原來是美男</a></p></td>
<td><p><strong>黃泰京</strong></p></td>
<td><p><a href="../Page/張根碩.md" title="wikilink">張根碩</a></p></td>
<td><p>DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/瑪莉外宿中.md" title="wikilink">瑪莉外宿中</a></p></td>
<td><p><strong>姜無缺</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2011</strong></p></td>
<td></td>
<td><p><strong>姜仁浩</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/愛情雨.md" title="wikilink">愛情雨</a></p></td>
<td><p><strong>徐仁河</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/漂亮男人.md" title="wikilink">漂亮男人</a></p></td>
<td><p><strong>獨孤馬特</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002</strong></p></td>
<td></td>
<td><p>David</p></td>
<td><p><a href="../Page/尹子維.md" title="wikilink">尹子維</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2003</strong></p></td>
<td><p><a href="../Page/古墓奇兵系列.md" title="wikilink">古墓奇兵2</a></p></td>
<td><p>Xien</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/新警察故事.md" title="wikilink">新警察故事</a></p></td>
<td><p>Fire</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/寒戰.md" title="wikilink">寒戰</a></p></td>
<td><p>Man/杜文</p></td>
<td><p>影碟版本</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/風暴.md" title="wikilink">風暴</a></p></td>
<td><p>高飛</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1992</strong></p></td>
<td><p><a href="../Page/吸血殭屍：驚情四百年.md" title="wikilink">吸血殭屍：驚情四百年</a></p></td>
<td><p>乔纳森·哈克</p></td>
<td><p><a href="../Page/奇洛·李維斯.md" title="wikilink">奇洛·李維斯</a></p></td>
<td><p>15週年影碟版本</p></td>
</tr>
<tr class="even">
<td><p><strong>1997</strong></p></td>
<td><p><a href="../Page/西藏七年.md" title="wikilink">西藏七年</a></p></td>
<td><p>無名角色等等</p></td>
<td><p>-</p></td>
<td><p>航空公司版本</p></td>
</tr>
<tr class="odd">
<td><p><strong>2002</strong></p></td>
<td></td>
<td><p><strong>Soldier Serra</strong></p></td>
<td></td>
<td><p>錄像帶/DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2004</strong></p></td>
<td></td>
<td><p><strong>倪峰</strong></p></td>
<td><p><a href="../Page/陳冠希.md" title="wikilink">陳冠希</a></p></td>
<td><p>DVD版本</p></td>
</tr>
<tr class="odd">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/惡作劇之吻.md" title="wikilink">惡作劇之吻</a></p></td>
<td><p><strong>江直樹</strong></p></td>
<td><p><a href="../Page/鄭元暢.md" title="wikilink">鄭元暢</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/惡作劇2吻.md" title="wikilink">惡作劇2吻</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無極.md" title="wikilink">無極</a></p></td>
<td><p><strong>無歡</strong></p></td>
<td><p><a href="../Page/謝霆鋒.md" title="wikilink">謝霆鋒</a></p></td>
<td><p>朝日電視台</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2008</strong></p></td>
<td><p><a href="../Page/霜花店.md" title="wikilink">霜花店</a></p></td>
<td><p>韓裴</p></td>
<td><p><a href="../Page/林周煥.md" title="wikilink">林周煥</a></p></td>
<td><p>DVD版本</p></td>
</tr>
<tr class="odd">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/一日一生.md" title="wikilink">一日一生</a></p></td>
<td><p>成年的亨利·惠勒</p></td>
<td><p><a href="../Page/杜比·麥奎爾.md" title="wikilink">杜比·麥奎爾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/創：光速戰記.md" title="wikilink">創：光速戰記</a></p></td>
<td><p><strong>山姆·費林</strong></p></td>
<td><p><a href="../Page/蓋瑞特·荷德倫.md" title="wikilink">蓋瑞特·荷德倫</a></p></td>
<td><p>公映/影碟版本</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/華爾街：金錢萬歲.md" title="wikilink">華爾街：金錢萬歲</a></p></td>
<td><p><strong>雅各</strong></p></td>
<td><p><a href="../Page/西亞·李畢福.md" title="wikilink">西亞·李畢福</a></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong>文生·費羅</strong></p></td>
<td><p><a href="../Page/山姆·萊利.md" title="wikilink">山姆·萊利</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/惡作劇之吻_(韓國電視劇).md" title="wikilink">惡作劇之吻（韓國版）</a></p></td>
<td><p><strong>白勝祖</strong></p></td>
<td><p><a href="../Page/金賢重.md" title="wikilink">金賢重</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td></td>
<td><p><strong>Michael Kovak</strong></p></td>
<td><p><a href="../Page/科林·奧多霍諾.md" title="wikilink">科林·奧多霍諾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2015</strong></p></td>
<td><p><a href="../Page/英國恐怖故事.md" title="wikilink">英國恐怖故事(S1)</a></p></td>
<td><p><strong><a href="../Page/道林格雷.md" title="wikilink">道林格雷</a></strong></p></td>
<td><p><a href="../Page/瑞夫·卡尼.md" title="wikilink">瑞夫·卡尼</a></p></td>
<td><p>WOWOW頻道/DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2016</strong></p></td>
<td><p><a href="../Page/英國恐怖故事.md" title="wikilink">英國恐怖故事(S2)</a></p></td>
<td><p><strong>道林格雷</strong></p></td>
<td><p><a href="../Page/瑞夫·卡尼.md" title="wikilink">瑞夫·卡尼</a></p></td>
<td><p>WOWOW頻道/DVD版本</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [平川大輔 Official Blog](http://ameblo.jp/daisuke-hirakawa/)
  - [Lantis介紹頁](http://lantis.jp/artist.php?id=1d798f5ce599a261ddc501d2adff837f)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.