**平度市**，位于[山东省](../Page/山东.md "wikilink")[山东半岛中部](../Page/山东半岛.md "wikilink")，[胶莱河的东岸](../Page/胶莱河.md "wikilink")。平度市为[中国肉类生产第六大县市](../Page/中国肉类生产百强县列表.md "wikilink")，[青岛市的卫星城](../Page/青岛市.md "wikilink")，也是山东省面积最大的[县级行政区](../Page/县级行政区.md "wikilink")，管辖5街道12镇。

## 历史

[西周时属](../Page/西周.md "wikilink")[莱国](../Page/莱国.md "wikilink")，[东周时属](../Page/东周.md "wikilink")[齐国](../Page/齐国.md "wikilink")。[西汉置](../Page/西汉.md "wikilink")[平度县](../Page/平度县.md "wikilink")，位于今市境北，[东汉时废置](../Page/东汉.md "wikilink")。而今市境南则属[即墨县地](../Page/即墨.md "wikilink")，为宗室胶东王的驻地，县治设于今平度城东南方向，是[南北朝以前](../Page/南北朝.md "wikilink")[山东半岛上的最大城市](../Page/山东半岛.md "wikilink")。至[北齐析置长广县](../Page/北齐.md "wikilink")，[隋改称胶水县](../Page/隋.md "wikilink")。[明胶水县省入平度州](../Page/明.md "wikilink")。1913年废州改县。曾于1941年至1944年间析置出平南、平西、平东三县，但又分别在1953年至1956年间合并回平度。1989年平度撤县设市，目前隶属于[青岛市管辖](../Page/青岛.md "wikilink")。

## 地理

平度境内地形以平原为主，东北部多低山丘陵。全市年平均气温11℃，年均降水量677mm。

## 经济

平度是青岛的卫星城，所处的山东半岛又是中国经济最发达、气候最温和的地域之一，所以平度无论是工业还是农业，都发达，曾被多次列入全国综合实力百强县，[兩田制便起始於此](../Page/兩田制.md "wikilink")。矿产资源主要有[黄金](../Page/黄金.md "wikilink")、[石墨](../Page/石墨.md "wikilink")、[滑石](../Page/滑石.md "wikilink")、[大理石等](../Page/大理石.md "wikilink")，其中滑石生产规模位居中国第一，黄金、石墨产量也位居山东省前列。农业以种植[小麦](../Page/小麦.md "wikilink")、[花生](../Page/花生.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[棉花为主](../Page/棉花.md "wikilink")，是中国重要的油料、粮食、花生产区。工业有[电子](../Page/电子.md "wikilink")、[食品](../Page/食品.md "wikilink")、采矿、[化工](../Page/化工.md "wikilink")、[纺织等诸多门类](../Page/纺织.md "wikilink")，[青岛啤酒公司](../Page/青岛啤酒.md "wikilink")、[海尔公司](../Page/海尔.md "wikilink")、[海信公司等青岛本地知名企业和多家](../Page/海信.md "wikilink")[韩国跨国公司都在平度拥有巨额投资](../Page/韩国.md "wikilink")。

## 文化和旅游

市境北部与[莱州市交界处的](../Page/莱州.md "wikilink")[天柱山上有](../Page/天柱山.md "wikilink")[北魏](../Page/北魏.md "wikilink")[郑道昭](../Page/郑道昭.md "wikilink")、[郑述祖父子手书的](../Page/郑述祖.md "wikilink")[摩崖石刻](../Page/摩崖石刻.md "wikilink")，价值颇高。其中“[荥阳郑文公之碑](../Page/荥阳.md "wikilink")”更是完美地融合了[魏碑和](../Page/魏碑.md "wikilink")[楷书两种](../Page/楷书.md "wikilink")[书法字体而备受推崇](../Page/书法.md "wikilink")。附近的大泽山也是一处著名风景区，风光秀丽，并且盛产“大泽山[葡萄](../Page/葡萄.md "wikilink")”，远近闻名。

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[即墨故城遗址](../Page/即墨故城遗址.md "wikilink")

[平度市](../Category/平度市.md "wikilink")
[鲁](../Category/中国大城市.md "wikilink")