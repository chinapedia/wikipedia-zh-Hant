**葛然朗巴·平措汪杰**（，），简称**平措汪杰**、**平汪**，汉名**闵志成**，[西康省](../Page/西康省.md "wikilink")（今[四川省](../Page/四川省.md "wikilink")[甘孜藏族自治州](../Page/甘孜藏族自治州.md "wikilink")）[巴塘县人](../Page/巴塘县.md "wikilink")，[藏族](../Page/藏族.md "wikilink")[学者](../Page/学者.md "wikilink")。1940年12月参加革命工作，1949年8月加入[中国共产党](../Page/中国共产党.md "wikilink")。

## 生平

### 民国时期

1922年，藏曆10月25日，平汪出生于[西康](../Page/西康.md "wikilink")[巴塘](../Page/巴塘.md "wikilink")，是家里的长子，有五位弟妹。

1935年，跟随舅父来到[南京准备上学](../Page/南京.md "wikilink")。次年就读于[中央政治学校附设的](../Page/中央政治学校.md "wikilink")[蒙藏学校](../Page/蒙藏学校.md "wikilink")，[蒋介石亲自任该校校长](../Page/蒋介石.md "wikilink")。1937年，[抗日战争爆发](../Page/中國抗日戰爭.md "wikilink")，学校经[安徽](../Page/安徽.md "wikilink")[九华山后](../Page/九华山.md "wikilink")，再迁至[湖南](../Page/湖南.md "wikilink")[芷江](../Page/芷江.md "wikilink")，撰写见闻《武汉之行》，发表于《芷江日报》。对左翼报刊、人士有所接触。1938年，学校再迁至[重庆](../Page/重庆.md "wikilink")，阅读了一些马列著作，倾向于[列宁](../Page/列宁.md "wikilink")《[论民族自决权](../Page/论民族自决权.md "wikilink")》的观点。第二年，平汪和[根曲扎西](../Page/根曲扎西.md "wikilink")、[昂旺格桑](../Page/昂旺格桑.md "wikilink")、[喜饶](../Page/喜饶.md "wikilink")、[马甲顿珠等同学组建了](../Page/马甲顿珠.md "wikilink")“藏族共产主义运动小组”，平汪任书记。通过[八路军驻重庆办事处秘密联系](../Page/八路军.md "wikilink")，还以该小组的名义，向[斯大林](../Page/斯大林.md "wikilink")、[毛泽东致信](../Page/毛泽东.md "wikilink")。平汪还将《[国际歌](../Page/国际歌.md "wikilink")》、《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》、《[游击队之歌](../Page/游击队之歌.md "wikilink")》翻译成[藏文](../Page/藏文.md "wikilink")。1940年，因为领导学生闹学潮，平汪被学校开除。此后，在[叶剑英的资助下](../Page/叶剑英.md "wikilink")，平汪等人购买了许多马列著作，返回藏区。

1942年，平汪在西康首府[康定](../Page/康定縣.md "wikilink")，组织成立“星火社”，参加成员包括[昂旺](../Page/昂旺.md "wikilink")、[刀登](../Page/刀登.md "wikilink")、[扎堆](../Page/扎堆.md "wikilink")、[曾却扎等](../Page/曾却扎.md "wikilink")。后被[国民党察觉](../Page/国民党.md "wikilink")，平汪和刀登前往[昌都](../Page/昌都地區.md "wikilink")，并与昌都总督[宇妥·扎西顿珠交流](../Page/宇妥·扎西顿珠.md "wikilink")。平汪和刀登前往[拉萨](../Page/拉萨.md "wikilink")，并组建[雪域共产主义青年同盟](../Page/雪域共产主义青年同盟.md "wikilink")（后被称作西藏共产党）。他们试图通过[索康·旺钦格勒噶伦](../Page/索康·旺钦格勒.md "wikilink")，说服[噶厦政府进行改革](../Page/噶厦.md "wikilink")，未果。一些马列著作被运至拉萨，平汪等人将《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》、《[游击队之歌](../Page/游击队之歌.md "wikilink")》、《[到敌人后方去](../Page/到敌人后方去.md "wikilink")》、《[延安颂](../Page/延安颂.md "wikilink")》、《[黄河颂](../Page/黄河颂.md "wikilink")》、《[黄水谣](../Page/黄水谣.md "wikilink")》、《[在太行山上](../Page/在太行山上.md "wikilink")》、《[青年颂](../Page/青年颂.md "wikilink")》等歌曲翻译成藏文，并重译了《国际歌》。1944年，平汪前往[印度](../Page/印度.md "wikilink")，与[印共](../Page/印度共产党.md "wikilink")[觉底士医生](../Page/觉底士.md "wikilink")、会面，并结识了《[西藏鏡報](../Page/西藏鏡報.md "wikilink")》主编[塔欽](../Page/塔欽.md "wikilink")。平汪告訴塔欽在康區組織游擊隊的計劃，塔欽表示支持，但是平汪把《西藏鏡報》移到拉薩出版以協助宣傳的建議被拒。\[1\]平汪透過塔欽向英國尋求軍事援助無果，英國反將其電報轉交噶厦。\[2\]1945年，平汪来到[云南](../Page/云南.md "wikilink")[德钦](../Page/德钦.md "wikilink")，组建“东藏人民自治同盟”，后被当地政府通缉。1947年，平汪又来到昌都，总督宇妥·扎西顿珠资助其前往拉萨。1949年，平汪等人因涉嫌“中共秘密工作人员”，被噶厦政府驱逐出境，平汪扬言“还会回来”。同年9月，平汪等人经印度，来到[昆明](../Page/昆明.md "wikilink")，并与当地中共组织取得联系，并正式转为中共党员。后平汪返回巴塘，组建"中共康藏边地工作委员会"、"东藏民主青年同盟"，平汪担任书记。

### 1949年后

1950年，平汪被任命为进藏南路部队党委副书记、民运部部长。10月，[解放军攻占昌都](../Page/中國人民解放軍.md "wikilink")，平汪任新成立的“中共昌都分工委”副主任。1951年4月，平汪陪同[阿沛·阿旺晋美抵达](../Page/阿沛·阿旺晋美.md "wikilink")[北京](../Page/北京.md "wikilink")，并列席了北京和噶厦政府的谈判，期间担任翻译工作。5月23日，《[17条协议](../Page/17条协议.md "wikilink")》签定。返回昌都后，平汪任先遣部队党委委员，9月9日，先遣部队进入拉萨，平汪被部分藏人称为“引红汉人进藏”的“红藏人”。在此后成立的中共西藏工委的八位委员中，平汪是唯一的藏族。1952年，成立编审委员会，平汪任主任。1953年，平汪陪同西藏[佛教代表团赴京](../Page/佛教.md "wikilink")，此后留在了北京，担任[中央民委政法司副司长](../Page/中央民委.md "wikilink")、兼任[民族出版社副总编辑等职](../Page/民族出版社.md "wikilink")。1954年，[毛泽东接见](../Page/毛泽东.md "wikilink")[达赖喇嘛](../Page/十四世达赖喇嘛.md "wikilink")、[班禅喇嘛](../Page/十世班禅.md "wikilink")，平汪担任翻译。9月，当选第一届全国人大代表，并担任《[宪法](../Page/中華人民共和國憲法.md "wikilink")》藏文翻译组组长。1955年，平汪主动要求进入[中央党校学习马列主義](../Page/中共中央党校.md "wikilink")。1956年，[陈毅率中央代表团进藏](../Page/陈毅.md "wikilink")，平汪任特别顾问，并担任翻译工作。[西藏自治区筹备委员会成立后](../Page/西藏自治区筹备委员会.md "wikilink")，平汪任筹委会委员、副秘书长，并任中共西藏工委委员、筹委会办公厅党组书记、西藏工委统战部副部长等职。

1957年，[德格·格桑旺堆在中央民委西藏小组提出把](../Page/德格·格桑旺堆.md "wikilink")[德格划到昌都](../Page/德格.md "wikilink")。次年，平汪被调离西藏，人大代表的职务也被免去，由於《西藏鏡報》出版了他改寫的歌詞，呼籲藏人向漢人復仇，他被认为“有地方民族主义思想”\[3\]，除了德格划分问题，他进藏托运的列宁《论民族自决权》也被认为是其过错。[1959年西藏发生騷亂](../Page/1959年藏區騷亂.md "wikilink")。第二年，平汪以“反革命嫌疑”罪入狱，关押于[秦城监狱的单人牢房](../Page/秦城监狱.md "wikilink")，并受到酷刑审讯。在狱中他研读[马克思](../Page/马克思.md "wikilink")、[列宁](../Page/列宁.md "wikilink")、[毛泽东](../Page/毛泽东.md "wikilink")、[黑格尔等人的著作](../Page/黑格尔.md "wikilink")，并用稻草、铁丝作为笔，用省下来的手纸作为稿纸
，来撰写文字。由于长期隔绝，他的口语能力退化。期间，平汪的父亲、妻子被迫害致死，其长子被关押六年。另外，他组建的“中共康藏边地工委”和“东藏民主青年同盟”被定为反革命组织，其中六人被迫害致死。

### 1978年后

1978年4月，56岁的平汪获释出狱，但其手稿等未能带出。后经平汪要求重审，终于获得平反。1980年，在北京[中南海受到](../Page/中南海.md "wikilink")[胡耀邦接见](../Page/胡耀邦.md "wikilink")。同年，平汪撰写的《对修改宪法有关民族部分的几点意见》\[4\]、《平汪与旅外藏胞回国代表的谈话纪要》\[5\]，引起关于民族问题原则的争论。1986年，平汪与[茨丹央珍结婚](../Page/茨丹央珍.md "wikilink")。平汪历任第五、六、七届[全国人大常委会委员](../Page/全国人大常委会.md "wikilink")、[全國人大民族委員會副主任委员](../Page/全國人大民族委員會.md "wikilink")，第八届全国人大民委顾问。1990年，平汪的哲学著作《辩证法新探》出版。1994年，其第二部哲学著作《月球存有液态》出版。1997年，平汪主编的《中国民族自治州投资指南》、《中国民族法制论文集》出版。1998年，[江泽民在](../Page/江泽民.md "wikilink")[北戴河接见了平汪夫妇](../Page/北戴河.md "wikilink")。2004年以来，平汪数次致信[胡锦涛](../Page/胡锦涛.md "wikilink")，建议中央与达赖喇嘛进行和谈，以解决西藏问题。\[6\]

2014年3月30日，他在[北京醫院去世](../Page/北京醫院.md "wikilink")，享壽92歲\[7\]。

## 回憶錄

平措旺傑在自傳中回憶道：1954年[第十四世達賴喇嘛和](../Page/第十四世達賴喇嘛.md "wikilink")[第十世班禪喇嘛訪問北京期間](../Page/第十世班禪喇嘛.md "wikilink")，毛澤東曾親自到達賴喇嘛住地拜訪。有一次，毛澤東特地問到，西藏是否有自己的[國旗](../Page/西藏國旗.md "wikilink")（National
Flag）。毛澤東說，以後應該讓中國的自治區都能夠打出自己的國旗，西藏打出西藏的國旗，內蒙打出內蒙的國旗。平措旺傑並表示：陳毅元帥“在一次黨內的高干座談會上講話時說︰
應該向中央建議，將[拉薩作為首府](../Page/拉薩.md "wikilink")，以西藏自治區為主，並把被分割在鄰省的[川](../Page/四川.md "wikilink")、[康](../Page/西康.md "wikilink")、[滇](../Page/雲南.md "wikilink")、[甘](../Page/甘肅.md "wikilink")、[青的所有東部](../Page/青海.md "wikilink")[藏區](../Page/藏區.md "wikilink")，建立一個統一的藏族自治區，這既照顧了[藏人的普遍心愿](../Page/藏人.md "wikilink")，對藏族發展有利，又對漢、藏的親密團結、國家的統一和鞏固都有極大好處。”
這是后來西藏問題中“大西藏自治區”的最早出處。中央有了達賴喇嘛方面的合作，就可以考慮把所有藏人生活的藏地，包括川青甘滇的藏區都統一到西藏自治區。\[8\]

## 参考文献

<references/>

## 外部链接

  - [葛然朗巴·平措汪杰生平年表](https://web.archive.org/web/20081120231310/http://people.tibetcul.com/dangdai/xyxz/200411/1293.html)
  - [A Tibetan Revolutionary: The Political Life and Times of Bapa
    Phüntso
    Wangye](http://www.ucpress.edu/books/pages/9933.php)，平措汪杰的英文传记
  - [王力雄：平措汪杰的追求](http://woeser.middle-way.net/?action=show&id=49)
  - [毁于希望和猜忌——读A Tibetan
    Revolutionary](http://blog.sina.com.cn/s/blog_4184b6ce0100brf7.html)

[Category:藏族学者](../Category/藏族学者.md "wikilink")
[Category:第一届全国人大代表](../Category/第一届全国人大代表.md "wikilink")
[Category:第五届全国人大常委会委员](../Category/第五届全国人大常委会委员.md "wikilink")
[Category:第六届全国人大常委会委员](../Category/第六届全国人大常委会委员.md "wikilink")
[Category:第七届全国人大常委会委员](../Category/第七届全国人大常委会委员.md "wikilink")
[Category:西康省全國人民代表大會代表](../Category/西康省全國人民代表大會代表.md "wikilink")
[Category:藏族全國人大代表](../Category/藏族全國人大代表.md "wikilink")
[Category:西藏自治區籌備委員會委員](../Category/西藏自治區籌備委員會委員.md "wikilink")
[Category:藏族中國共產黨黨員](../Category/藏族中國共產黨黨員.md "wikilink")
[Category:中国共产党党员
(1949年10月前入党)](../Category/中国共产党党员_\(1949年10月前入党\).md "wikilink")
[Category:巴塘人](../Category/巴塘人.md "wikilink")

1.
2.

3.

4.  [对修改宪法有关民族部分的几点意见](http://gangjanba.middle-way.net/article/2007/0128/article_10.html)


5.  [应达赖喇嘛派来的代表团要求我与他们谈话纪要](http://gangjanba.middle-way.net/article/2007/0206/article_58.html)


6.  [写给胡锦涛的信，2004年](http://gangjanba.middle-way.net/article/2007/0128/article_9.html)
    、[谈话后的信，2005年](http://gangjanba.middle-way.net/article/2007/0206/article_59.html)
    、[资深藏族学者平汪据报上书胡锦涛](http://news.bbc.co.uk/chinese/simp/hi/newsid_6420000/newsid_6426600/6426673.stm)、[平措汪杰：谈彻底解决西藏问题的必要性](http://www.savetibet.org/cn/tibetans/newsitem.php?id=2)


7.  [中共前西藏高官逝世
    曾呼籲讓達賴返藏](http://www.bbc.co.uk/zhongwen/trad/china/2014/03/140330_tibet_phuntso_wangye_death.shtml)

8.  [五十年前的西藏之爭](https://www.tibet.org.tw/com_detail.php?com_id=353)