**平方-{米}-**，又稱為「**平方公-{}-尺**」（符號為**m<sup>2</sup>**）是[面積的](../Page/面積.md "wikilink")[公制單位](../Page/公制.md "wikilink")，其定義是「在一[平面上](../Page/平面.md "wikilink")，邊長為一[公尺的](../Page/公尺.md "wikilink")[正方形之面積](../Page/正方形.md "wikilink")」。中國大陆在表示房间面积等时又常简称为“-{**平米**}-”或“**平**”。

## 單位轉換

<table>
<tbody>
<tr class="odd">
<td><p>1平方-{zh-hans:米; zh-hant:公尺;}-（1m²）等於：　　　<br />
* 0.000001<a href="../Page/平方公里.md" title="wikilink">km²</a></p>
<ul>
<li>10000<a href="../Page/平方厘米.md" title="wikilink">cm²</a></li>
<li>0.0001<a href="../Page/公頃.md" title="wikilink">公頃</a></li>
<li>0.01<a href="../Page/公畝.md" title="wikilink">公畝</a></li>
<li>0.0002471<a href="../Page/英畝.md" title="wikilink">英畝</a></li>
<li>0.3025<a href="../Page/坪.md" title="wikilink">坪</a></li>
<li>1.196<a href="../Page/平方碼.md" title="wikilink">平方碼</a></li>
<li>10.7584<a href="../Page/平方英尺.md" title="wikilink">平方英尺</a></li>
<li>1550<a href="../Page/平方英寸.md" title="wikilink">平方英寸</a></li>
<li>0.00010031786225<a href="../Page/甲.md" title="wikilink">甲</a></li>
</ul></td>
<td><p><br />
（1km²=1000000m²）<br />
（1cm²=0.0001m²）<br />
（1公頃=10000m²）<br />
（1公畝=100m²）<br />
（1英畝=4046.86m²）<br />
（1坪=3.3058m²）<br />
（1平方碼=0.836120m²）<br />
（1平方英尺=0.092951m²）<br />
（1平方英寸=0.000645m²）<br />
（1甲=9969.17m²）<br />
</p></td>
</tr>
</tbody>
</table>

## 参看

  - [国际单位](../Page/国际单位.md "wikilink") -
    [长度单位](../Page/长度单位.md "wikilink") -
    [时间长度比较](../Page/时间长度比较.md "wikilink") -
    [数量级](../Page/数量级.md "wikilink") -
    [单位转换](../Page/单位转换.md "wikilink")
  - [平方-{zh-hans:千米; zh-hant:公里;}-](../Page/平方千米.md "wikilink") -
    [公頃](../Page/公頃.md "wikilink") - [公畝](../Page/公畝.md "wikilink")
    - **平方-{zh-hans:米; zh-hant:公尺;}-** - [平方-{zh-hans:厘米;
    zh-hant:公分;}-](../Page/平方厘米.md "wikilink") - [平方-{zh-hans:毫米;
    zh-hant:公釐;}-](../Page/平方毫米.md "wikilink")

## 外部链接

  - [面积单位转换计算器](https://web.archive.org/web/20030409073955/http://www.sjz.net.cn/bangbsh/htm/dulian/area.htm)
  - [轉換平方米的其他單位](http://unit-converter.org/en/area/m%25C2%25B2.html)

[Category:面積單位](../Category/面積單位.md "wikilink")
[Category:国际单位制导出单位](../Category/国际单位制导出单位.md "wikilink")