[York_central_hall.jpg](https://zh.wikipedia.org/wiki/File:York_central_hall.jpg "fig:York_central_hall.jpg")中央大厅是平板玻璃建筑设计的实例\]\]

**平板玻璃大学**（**plate glass
university**）是指[英国在](../Page/英国.md "wikilink")1960年代关于[高等教育的](../Page/高等教育.md "wikilink")《[罗宾斯报告](../Page/罗宾斯报告.md "wikilink")》发表以后成立的大学。这个名称是来源于它们的[现代建筑设计](../Page/现代建筑.md "wikilink")，在[钢或](../Page/钢.md "wikilink")[混凝土结构中广泛使用平板玻璃](../Page/混凝土.md "wikilink")，与以维多利亚建筑风格为主的[红砖大学和更古老的](../Page/红砖大学.md "wikilink")[古典大学形成鲜明对照](../Page/古典大学.md "wikilink")。

过去，[新大学是平板玻璃大学的同义词](../Page/新大学.md "wikilink")，不过从1992年以后，新大学倾向于指1992后大学（Post-1992
university，主要是过去的[理工學院polytechnic升格](../Page/理工學院.md "wikilink")）。

## 平板玻璃大学列表

  - [阿斯顿大学](../Page/阿斯顿大学.md "wikilink")（Aston University）
  - [巴斯大学](../Page/巴斯大学.md "wikilink")（University of Bath）
  - [布鲁内尔大学](../Page/布鲁内尔大学.md "wikilink")（Brunel University）
  - [倫敦城市大學](../Page/倫敦城市大學.md "wikilink")（City University, London）
  - [东安格利亚大学](../Page/东安格利亚大学.md "wikilink")（University of East Anglia）
  - [艾塞克斯大学](../Page/艾塞克斯大学.md "wikilink")（University of Essex）
  - [赫瑞瓦特大学](../Page/赫瑞瓦特大学.md "wikilink")（Heriot-Watt University）
  - [肯特大学](../Page/肯特大学.md "wikilink")（University of Kent）
  - [兰卡斯特大学](../Page/兰卡斯特大学.md "wikilink")（Lancaster University）
  - [罗浮堡大学](../Page/罗浮堡大学.md "wikilink")（Loughborough University）
  - [索尔福德大学](../Page/索尔福德大学.md "wikilink")（University of Salford）
  - [斯特灵大学](../Page/斯特灵大学.md "wikilink")（University of Stirling）
  - [史崔克莱德大学](../Page/史崔克莱德大学.md "wikilink")（University of Strathclyde）
  - [萨里大学](../Page/萨里大学.md "wikilink")（University of Surrey）
  - [萨塞克斯大学](../Page/萨塞克斯大学.md "wikilink")（University of Sussex）
  - [华威大学](../Page/华威大学.md "wikilink")（University of Warwick）
  - [阿尔斯特大学](../Page/阿尔斯特大学.md "wikilink")（University of Ulster）
  - [约克大学](../Page/英國約克大學.md "wikilink")（University of York）

## 参考文献

## 外部链接

  - [Guardian article featuring the term *plate glass
    university*](http://www.guardian.co.uk/comment/story/0,3604,727427,00.html)

## 参见

  - [砂岩學府](../Page/砂岩學府.md "wikilink")
  - [校園大學](../Page/校園大學.md "wikilink")

{{-}}

[Category:英国大学类型](../Category/英国大学类型.md "wikilink")