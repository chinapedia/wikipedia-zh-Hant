**平湖市**，是由[中华人民共和国](../Page/中华人民共和国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[嘉兴市代管的一个](../Page/嘉兴市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，位于浙江省北部。

## 历史

[明](../Page/明朝.md "wikilink")[宣德五年](../Page/宣德.md "wikilink")（1430年）自[海盐县析置](../Page/海盐县.md "wikilink")“**平湖县**”，1991年撤平湖县设平湖市至今。

近代中国著名的[艺术家](../Page/艺术家.md "wikilink")（[音乐](../Page/音乐.md "wikilink")、[书法](../Page/书法.md "wikilink")、[绘画](../Page/绘画.md "wikilink")、[篆刻](../Page/篆刻.md "wikilink")、[诗歌](../Page/诗歌.md "wikilink")）、[教育家](../Page/教育家.md "wikilink")、[僧侣](../Page/僧侣.md "wikilink")[李叔同](../Page/李叔同.md "wikilink")（弘一法师）的母亲是平湖人，市中心现有规模宏大的李叔同纪念馆。此外，还有莫氏庄园、九龙山、天妃宫炮台等景点。

[乍浦镇有](../Page/乍浦镇.md "wikilink")[第一次鸦片战争古炮台遗址](../Page/第一次鸦片战争.md "wikilink")。

## 地理

平湖地处[杭嘉湖平原](../Page/杭嘉湖平原.md "wikilink")，境内地势低平、河网密布，在海上还有[王盘山等岛屿](../Page/王盘山.md "wikilink")。平湖属江南古陆外缘杭州湾凹陷，为一冲积平原。境内地势平坦，平均海拔2.8米，除东南沿海有呈带状分布的20座低丘和11座岛礁共4.89平方公里外，余为大片平原。陆域面积537平方公里，海域面积1086平方公里，海岸线长27公里；耕地面积47万亩。

农业盛产粮油和[水产品](../Page/水产品.md "wikilink")，有“金平湖”之称。主产[水稻](../Page/水稻.md "wikilink")、[小麦](../Page/小麦.md "wikilink")、[油菜](../Page/油菜.md "wikilink")、[对虾](../Page/对虾.md "wikilink")、[禽蛋等](../Page/禽蛋.md "wikilink")，特产[糟蛋和](../Page/糟蛋.md "wikilink")[西瓜](../Page/西瓜.md "wikilink")。由于靠近[上海](../Page/上海.md "wikilink")，平湖也因此吸引了许多跨国企业前来投资。工业有[服装](../Page/服装.md "wikilink")、[箱包](../Page/箱包.md "wikilink")、[机电](../Page/机电.md "wikilink")、[造纸等门类](../Page/造纸.md "wikilink")，其中服装制造业产值极高，出口额位居中国第一。曾多次名列全国综合实力百强县。

位于市境西南部的[乍浦港是](../Page/乍浦港.md "wikilink")[杭州湾畔重要的深水港](../Page/杭州湾.md "wikilink")，历史悠久，[唐代即设港务机构](../Page/唐.md "wikilink")，目前以货物转运为主。其旁建有装机容量达300万千瓦的[嘉兴发电厂](../Page/嘉兴发电厂.md "wikilink")，为中国巨型火力发电厂之一。

### 气候

属北亚热带季风气候，年平均气温16度左右，年平均日照时数在2000小时左右，年平均降水量1170毫米左右，全年无霜期225天左右。

## 行政区划

下辖3个街道、6个镇：

  - 街道：钟埭街道、当湖街道、曹桥街道；
  - 镇：乍浦镇、新埭镇、新仓镇、独山港镇（原黄姑镇和全塘镇合并而成）\[1\]、广陈镇、林埭镇。

## 经济

平湖市经济较为发达，为全国最具实力的[县市之一](../Page/县级行政区.md "wikilink")，其综合竞争能力[2010年位居浙江省县（市）的第十三位，全国百强县（市）的第四十五位](../Page/中国百强县#县域经济论坛评价.md "wikilink")，2017年[GDP总量](../Page/国内生产总值.md "wikilink")605.8亿元\[2\]。2017年12月14日，平湖市位居《中国工业百强县（市）、百强区发展报告》第67位\[3\]。

## 旅游

  - [莫氏庄园](../Page/莫氏庄园.md "wikilink")
  - [乍浦炮台](../Page/乍浦炮台.md "wikilink")
  - [九龙山](../Page/九龙山.md "wikilink")
  - [东湖](../Page/东湖.md "wikilink")

## 友好城市

<table>
<thead>
<tr class="header">
<th><p>国家</p></th>
<th><p>城市</p></th>
<th><p>结交时间</p></th>
<th><p>国家</p></th>
<th><p>城市</p></th>
<th><p>结交时间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/东台市.md" title="wikilink">东台市</a></p></td>
<td><p>2007年3月</p></td>
<td></td>
<td><p><a href="../Page/涡阳县.md" title="wikilink">涡阳县</a></p></td>
<td><p>2013年6月</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/森尼韦尔.md" title="wikilink">森尼韦尔</a></p></td>
<td><p>2002年12月</p></td>
<td></td>
<td><p><a href="../Page/府中市.md" title="wikilink">府中市</a></p></td>
<td><p>2004年5月</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/小浜市.md" title="wikilink">小浜市</a></p></td>
<td><p>2006年4月</p></td>
<td></td>
<td><p><a href="../Page/沃森维尔_(加利福尼亚州).md" title="wikilink">沃森维尔 (加利福尼亚州)</a></p></td>
<td><p>2006年8月</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/兰讷斯.md" title="wikilink">兰讷斯</a></p></td>
<td><p>2018年3月</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

{{-}}

[嘉兴市](../Category/浙江省县级市.md "wikilink")
[平湖市](../Category/平湖市.md "wikilink")
[市](../Category/嘉兴区县市.md "wikilink")
[P](../Category/東海沿海城市.md "wikilink")
[浙](../Category/中国中等城市.md "wikilink")

1.
2.
3.