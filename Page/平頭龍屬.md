**平頭龍屬**（屬名：*Homalocephale*）屬於[平頭龍科](../Page/平頭龍科.md "wikilink")，生存於晚[白堊紀的](../Page/白堊紀.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")，約8000萬年前。平頭龍是種[草食性恐龍](../Page/草食性.md "wikilink")，身長約1.8公尺。平頭龍由Teresa
Maryańska與Halszka Osmólska在1974年命名。[模式種是](../Page/模式種.md "wikilink")*H.
calathocercos*，屬名在[希臘文的意思為](../Page/希臘文.md "wikilink")「平坦的頭部」\[1\]\[2\]。根據2010年的最新研究，平頭龍可能是[傾頭龍的一個](../Page/傾頭龍.md "wikilink")[次異名](../Page/次異名.md "wikilink")。

## 敘述

平頭龍與其他的[厚頭龍類不同](../Page/厚頭龍類.md "wikilink")，牠們的[頭顱骨平坦](../Page/頭顱骨.md "wikilink")、呈楔形。顱頂相當厚，可能用來在戰鬥或抵抗掠食動物時緩衝撞擊\[3\]。平頭龍也因牠們寬廣的[臀部著名](../Page/臀部.md "wikilink")，使得有些[古生物學家認為寬廣臀部是用來直接生下幼體](../Page/古生物學家.md "wikilink")，其他科學家則認為它們是用來緩衝撞擊\[4\]。平頭龍擁有長腿，顯示牠們能以快速奔跑。

[模式種是](../Page/模式種.md "wikilink")*H.
calathocercos*，是根據一個不完整的頭顱骨，與顱後的骨骼所命名。該標本具有大型的[上顳孔](../Page/上顳孔.md "wikilink")（Supratemporal
fenestrae）、一個明顯的[額頂縫](../Page/額頂縫.md "wikilink")（Frontoparietal
suture）、位置低且長的[下顳孔](../Page/下顳孔.md "wikilink")（Infratemporal
fenestrae）、以及大而圓形的眼窩。[鱗骨背側的裝飾物呈圓形](../Page/鱗骨.md "wikilink")，而後側與側面的裝飾物呈結狀。該標本被認為是個成年個體，但頭部骨頭的縫卻是分開的，頭部平坦，這些是許多厚頭龍類的未成年特徵。

在2010年，[尼克·朗里奇](../Page/尼克·朗里奇.md "wikilink")（Nick Longrich
）等人提出顱頂平坦的厚頭龍類，其實是圓顱頂的厚頭龍類的幼年個體。稍早在2009年，也有類似的研究發現。尼克·朗里奇等人提出，平頭龍其實是[傾頭龍的幼年個體或亞成年個體](../Page/傾頭龍.md "wikilink")\[5\]。

## 大眾文化

平頭龍出現在[維旺迪環球](../Page/維旺迪環球.md "wikilink")（Vivendi
Universal）的電玩遊戲《[侏儸紀公園：基因計劃](../Page/侏儸紀公園：基因計劃.md "wikilink")》（*Jurassic
Park: Operation Genesis*）。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

  - *Fantastic Facts About Dinosaurs* (ISBN 978-0-7525-3166-3)

## 外部連結

  - <https://web.archive.org/web/20061006225059/http://www.dino-nakasato.org/en/special97/Homa-e.html>
  - <https://web.archive.org/web/20070518001124/http://www.leute.server.de/frankmuster/H/Homalocephale.htm>

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:厚頭龍下目](../Category/厚頭龍下目.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")

1.  Holtz, Thomas R. Jr. (2008) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages*
    [Supplementary
    Information](http://www.geol.umd.edu/~tholtz/dinoappendix/DinoappendixSummer2008.pdf)
2.
3.
4.
5.  Longrich, N.R., Sankey, J. and Tanke, D. (2010). "*Texacephale
    langstoni*, a new genus of pachycephalosaurid (Dinosauria:
    Ornithischia) from the upper Campanian Aguja Formation, southern
    Texas, USA." *Cretaceous Research*, .