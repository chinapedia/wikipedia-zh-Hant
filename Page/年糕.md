[Niangao2.JPG](https://zh.wikipedia.org/wiki/File:Niangao2.JPG "fig:Niangao2.JPG")餡料的年糕\]\]
[File-Guangdong_Nianguo.jpg](https://zh.wikipedia.org/wiki/File:File-Guangdong_Nianguo.jpg "fig:File-Guangdong_Nianguo.jpg")
[Guangdong_Nian_cake.jpg](https://zh.wikipedia.org/wiki/File:Guangdong_Nian_cake.jpg "fig:Guangdong_Nian_cake.jpg")[煎年糕](../Page/煎.md "wikilink")\]\]
**年糕**，[客家語稱為](../Page/客家語.md "wikilink")**甜粄**，[閩南語稱](../Page/閩南語.md "wikilink")**甜粿**，是流行於[東亞新年的一種](../Page/東亞新年.md "wikilink")[傳統美食和賀年食品](../Page/传統.md "wikilink")，流行於很多地方，例如：[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡等華人地區](../Page/新加坡.md "wikilink")，中文裡年糕與“年高”諧音，有年年昇高的意思。年糕早期是在[年夜用來](../Page/年夜.md "wikilink")[祭祀](../Page/祭祀.md "wikilink")[神灵及](../Page/神灵.md "wikilink")[供奉](../Page/供奉.md "wikilink")[祖先](../Page/祖先.md "wikilink")，其後漸漸成為一種[農曆新年](../Page/農曆新年_\(華人\).md "wikilink")[食品](../Page/食品.md "wikilink")。

## 歷史

過年吃年糕的習俗，據傳從[周代開始](../Page/周代.md "wikilink")。由於禾穀成熟一次稱為一年，所以後世過年吃年糕，就含有祝賀五穀豐登的意思了。吃年糕還有取“年高”長壽之意。
也有年年長高長大的意思。

## 相關傳說

在[江浙一帶流傳著一則與年糕有關的傳說](../Page/江浙.md "wikilink")。據傳於[春秋時代時](../Page/春秋時代.md "wikilink")，[吳國](../Page/吳國.md "wikilink")[大夫](../Page/大夫.md "wikilink")[伍子胥](../Page/伍子胥.md "wikilink")，被吳王[夫差賜劍自刎而死](../Page/夫差.md "wikilink")。傳說伍子胥死前囑咐親信：「我死後，如果國家有難，民眾缺糧，你們到象門城牆挖地三尺，就可以得到食糧。」越王[勾踐聞知伍子胥遭賜死的消息後](../Page/勾踐.md "wikilink")，就攻打吳國雪恥。夫差連吃敗仗，都城被越兵圍困，城中糧盡援絕，軍民紛紛餓死。此時，伍子胥的親信按他生前囑咐，去象門挖地三尺，挖到伍子胥以江米粉蒸制後壓製成的“城磚”。從此以後，每逢過年，當地家家戶戶都要蒸製像城磚樣的江米年糕，以奉祀伍子胥的功績。因為是在過年時製作的食品，就把它叫作年糕，沿襲至今

## 種類

年糕通常用[糯米磨粉製成](../Page/糯米.md "wikilink")，在中國不同地區製作的年糕各有分別；年糕傳至週邊國家後，也依其飲食習慣而延伸出不同的製作方法及風味。

### 北方年糕

[北方的年糕可蒸或炸](../Page/北方.md "wikilink")，以甜味為主。[北京年糕包括江米或黄米製成的](../Page/北京.md "wikilink")[红棗年糕](../Page/红棗.md "wikilink")、百果年糕及白年糕。[山西則習慣以黄米粉](../Page/山西.md "wikilink")（[晋语称之为](../Page/晋语.md "wikilink")[糕麵](../Page/糕麵.md "wikilink")）製作年糕，以油炸，並可以豆沙或棗泥作餡。[河北習慣在年糕中加入小枣](../Page/河北.md "wikilink")、小红豆及[綠豆等一起蒸食](../Page/綠豆.md "wikilink")，[山東年糕以红棗及黄米蒸成](../Page/山東.md "wikilink")。[東北年糕則主要由粘高粱米加上豆類蒸製而成](../Page/東北.md "wikilink")。

### 江南年糕

[长江下游一帶的年糕呈白色](../Page/长江下游.md "wikilink")，本身屬淡味，由[粳米跟](../Page/粳米.md "wikilink")[糯米混合製成](../Page/糯米.md "wikilink")，糯米較多成品較軟，粳米較多成品較硬。可蒸、炸、切片炒或煮湯。常见做法有[雪菜](../Page/雪菜.md "wikilink")[肉丝汤年糕](../Page/肉丝.md "wikilink")、[荠菜炒年糕等](../Page/荠菜.md "wikilink")。上海的排骨年糕也别具特色。江南部份農村用腳踏的方法製年糕，並名為「腳踏糕」。\[1\]

### 福建年糕

[福州](../Page/福州.md "wikilink")、[宁德与](../Page/宁德.md "wikilink")[莆田地区的](../Page/莆田.md "wikilink")[白粿类似于台灣常见的年糕](../Page/白粿.md "wikilink")，此外还有[芋粿](../Page/芋粿.md "wikilink")、[九重粿](../Page/九重粿.md "wikilink")、粳米、白八粿、糖粿等等，福州人只把味甜的糖粿称为年糕，并有红糖粿、白糖粿等分类。

福建年糕是将糯米粉和米粉加白糖或红糖，有加入[花生](../Page/花生.md "wikilink")、红枣、红豆等，而后和成面团，再放在年糕叶上蒸熟。

[閩南地區的](../Page/閩南.md "wikilink")[福建年糕即](../Page/福建.md "wikilink")[粘糕](../Page/粘糕.md "wikilink")，為天然琥珀色，主要為年節祭祀及送禮用，由糯米、芋頭製成，通常切片煎熟食用，亦可裹蛋汁、或[太白粉或](../Page/太白粉.md "wikilink")[番薯粉煎食](../Page/番薯粉.md "wikilink")。

### 廣東年糕

[Hong_Kong_coconut_milk_cake.JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_coconut_milk_cake.JPG "fig:Hong_Kong_coconut_milk_cake.JPG")
[廣東及](../Page/廣東.md "wikilink")[港澳的年糕是橙紅色](../Page/港澳.md "wikilink")[黏糕](../Page/黏糕.md "wikilink")，屬甜味，表面有油光，主要由[糯米粉](../Page/糯米粉.md "wikilink")、[片糖和](../Page/片糖.md "wikilink")[花生油混合後蒸煮而成](../Page/花生油.md "wikilink")，通常食用前先切片再用慢火煎軟，也可以將年糕沾蛋漿煎至金黃色食用。

#### 香港年糕

香港有一種「椰汁年糕」，加入[椰汁和椰子水](../Page/椰汁.md "wikilink")，及使用白糖取代片糖，呈現乳白色的外觀\[2\]，也由此發展出[錦鯉造型及比喻](../Page/錦鯉.md "wikilink")「鯉躍龍門」的「錦鯉年糕」\[3\]\[4\]。另外，[蘿蔔糕和](../Page/蘿蔔糕.md "wikilink")[芋頭糕也會被作為賀年的年糕](../Page/芋頭糕.md "wikilink")。

### 湖南年糕

又称[糍粑](../Page/糍粑.md "wikilink")。将[糯米用水浸泡一天以上](../Page/糯米.md "wikilink")，将水濾乾之後蒸熟。将蒸好的糍粑放在石臼裡面，用大木槌等工具舂烂至胶状。然后将这团胶状物放入洒上糯米粉的器皿中揉搓，有些地區由两人进行，一个拿着木槌站着敲打，另外一个蹲着用手将糍粑揉成团状。每打一下揉一次。用模具制成各种形状或直接制成饼状或团状，其大小会依据具体的用途而定。放在通风干燥的地方晾干。待要食用时，可以油炸，水煮或直接用火烤熟\[5\]。

### [蜑家年糕](../Page/蜑家.md "wikilink")

[蜑家](../Page/蜑家.md "wikilink")[艇戶也有喫年糕的習俗](../Page/艇戶.md "wikilink")，名為「九層糕」，以粘米糊，拌入葱花、[芝麻](../Page/芝麻.md "wikilink")、花生碎、豬肉粒、鷄肉糕，以[五香粉調味](../Page/五香粉.md "wikilink")，每蒸一層加一層味料，至第九層為止，故稱。\[6\]

### 臺式年糕

[臺灣漢人有七成以上](../Page/臺灣漢人.md "wikilink")[祖籍來自](../Page/祖籍.md "wikilink")[閩南](../Page/閩南.md "wikilink")，故臺灣的傳統年糕，基本上與閩南相同，[臺灣話俗稱](../Page/臺灣閩南語.md "wikilink")**甜粿**（即[黏糕](../Page/黏糕.md "wikilink")），[客家語俗稱](../Page/臺灣客家語.md "wikilink")**甜粄**，臺式年糕多為赤色、棕色或琥珀色的，主要為[除夕](../Page/除夕.md "wikilink")[祭祖與](../Page/祭祖.md "wikilink")[過年](../Page/過年.md "wikilink")[祭天](../Page/祭天.md "wikilink")、[接神用](../Page/接神.md "wikilink")。

[臺灣北部的傳統年糕是將糯米先磨成米漿放入米袋壓乾](../Page/臺灣北部.md "wikilink")，再切片放入大鼎和花生、紅糖或是[紅豆等材料蒸熟](../Page/紅豆.md "wikilink")，待熟後再以長竹片拉起放入容器，口感柔軟細緻。

[臺北流行各種年糕](../Page/臺北.md "wikilink")，受[臺灣戰後時期來臺的](../Page/臺灣戰後時期.md "wikilink")[外省人影響](../Page/外省人.md "wikilink")，年糕的款式較為多樣，甚至有商家主打「[眷村年糕](../Page/眷村.md "wikilink")」，或者加料的各種傳統年糕，如花生年糕、黑糖年糕及紅豆年糕等。亦常見[閩東](../Page/閩東.md "wikilink")[福州移民所製造的福州年糕加入豬肉及芋頭還有花生製成](../Page/福州.md "wikilink")，為天然琥珀色，通常切片煎熟、蒸熟食用，亦可裹蛋汁、或[太白粉或番薯粉煎食](../Page/太白粉.md "wikilink")。

[中臺灣](../Page/中臺灣.md "wikilink")、[南臺灣則流行早期](../Page/南臺灣.md "wikilink")[閩南風格的年糕](../Page/閩南.md "wikilink")，以小火將糯米煮至糊狀加[砂糖](../Page/砂糖.md "wikilink")，放冷即可，煎後食用，較有嚼勁。

臺灣部分地區，送人年糕，意味著對方正在服喪。受禮的人，只要回贈一個一元[硬幣](../Page/硬幣.md "wikilink")，就能避開這惱人的送禮禁忌。

### 日式年糕

[日本年糕又稱](../Page/日本.md "wikilink")[鏡餅](../Page/鏡餅.md "wikilink")。日本亦流行在慶祝[新年](../Page/新年.md "wikilink")（[明治維新起日本新年改為](../Page/明治維新.md "wikilink")[陽曆元旦](../Page/陽曆.md "wikilink")）時吃年糕，新年早上會煮成[年糕湯食用](../Page/年糕湯.md "wikilink")。日式年糕是白色淡而無味，傳統的的方式是把飯團放在大木桶中，用鎚子不斷擊打而製成。

### 韓式年糕

[Korean.food-Tteok.mandu.guk-01.jpg](https://zh.wikipedia.org/wiki/File:Korean.food-Tteok.mandu.guk-01.jpg "fig:Korean.food-Tteok.mandu.guk-01.jpg")
韓國的年糕是[朝鮮打糕的一種](../Page/朝鮮打糕.md "wikilink")，與中國寧波年糕相仿，但烹調方式不同，通常是煮[年糕湯和炒](../Page/年糕湯.md "wikilink")**泡菜**時吃。韓國年糕也常與[苦椒醬一起煮成](../Page/苦椒醬.md "wikilink")[辣炒年糕作為小吃](../Page/辣炒年糕.md "wikilink")。

### 琉球年糕

[琉球人新年會吃一種叫](../Page/琉球人.md "wikilink")「ナントゥンスー」的糕點\[7\]。琉球群島以黑糖著名，因此琉球人新年時會在年糕中加入黑糖，它和臺式年糕有些相似。

## 參考資料

## 外部連接

  - [異「糕」膽大—不同地區年糕的探討](http://www.shs.edu.tw/works/essay/2018/03/2018033023020731.pdf)

{{-}}

[年糕](../Category/年糕.md "wikilink")
[Category:糯米食品](../Category/糯米食品.md "wikilink")
[Category:新春習俗](../Category/新春習俗.md "wikilink")

1.  [腳踏糕
    （人民網）](http://www.people.com.cn/BIG5/paper447/14059/1254249.html)
2.
3.
4.
5.  [打糍粑](http://www.hb.xinhuanet.com/2007zfwq/2007-08/09/content_10813172.htm)

6.
7.  [ナントゥンスー](http://okinawanstyle.seesaa.net/article/35273641.html)