《**幹**

## 參考文獻

## 外部連結

[Stealthisfilm.PNG](https://zh.wikipedia.org/wiki/File:Stealthisfilm.PNG "fig:Stealthisfilm.PNG")

  - [《幹走電影》官方網站](http://www.stealthisfilm.com/)

  - [但唐謨](../Page/但唐謨.md "wikilink")，〈[分享是人類的珍寶：《幹走電影》第一部和第二部](http://drugstore73.blogspot.com/2008/04/blog-post_27.html)〉

  - [《幹走電影》](http://video.google.com/videoplay?docid=-9198224502900953337)於[Google
    Video](../Page/Google_Video.md "wikilink")

  - [《幹走電影》系列的計畫與戰略](http://stealthisfilm.wikidot.com/)

  - [海盜灣收藏的.torrent檔案](http://thepiratebay.org/blog/38)

  - 〈[《幹走電影》：瑞典盜版運動的紀錄片](https://web.archive.org/web/20060831025721/http://www.boingboing.net/2006/08/23/steal_this_movie_doc.html)〉（Steal
    This Movie: documentary on Swedish piracy
    movement）於[波音波音](../Page/波音波音.md "wikilink")

[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:檔案分享](../Category/檔案分享.md "wikilink")
[Category:紀錄片](../Category/紀錄片.md "wikilink")
[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:网络行动主义](../Category/网络行动主义.md "wikilink")