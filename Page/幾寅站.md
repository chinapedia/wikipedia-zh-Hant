[Ikutora_stn_from_platform.jpg](https://zh.wikipedia.org/wiki/File:Ikutora_stn_from_platform.jpg "fig:Ikutora_stn_from_platform.jpg")
**幾寅站**（），位於[北海道](../Page/北海道.md "wikilink")[空知郡](../Page/空知郡.md "wikilink")[南富良野町字幾寅](../Page/南富良野町.md "wikilink")，屬於[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")（JR北海道）[根室本線的](../Page/根室本線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。車站編號為**T36**。電報略號為**トラ**。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「yuk-turasi-pet」，意思為有鹿溯流而上的河川\[1\]。

## 車站構造

  - 1面1線的[側式月台的](../Page/側式月台.md "wikilink")[無人車站](../Page/招呼站.md "wikilink")。
  - 木造站舎於電影《[鐵道員](../Page/鐵道員.md "wikilink")》拍攝時被改裝成[石炭輸送路線終端站](../Page/石炭.md "wikilink")「**幌舞站**」。
  - 候車室有電影拍攝相關的照片等展示，站房旁有電影拍攝時用的Kiha40型柴油客車展示。

## 車站周邊

幾寅的市街地、商店與住宅並立。

  - [北海道道465號金山幾寅停車場線](../Page/北海道道465號金山幾寅停車場線.md "wikilink")
  - [國道38號](../Page/國道38號.md "wikilink")
  - [富良野警察署幾寅派出所](../Page/富良野警察署.md "wikilink")
  - 南富良野町公所
  - 幾寅[郵局](../Page/郵局.md "wikilink")（併設[日本郵政富良野分店幾寅配送據點](../Page/日本郵政.md "wikilink")）
  - [旭川信用金庫南富良野分店](../Page/旭川信用金庫.md "wikilink")
  - 富良野農業協同組合（JA富良野）南富良野分所
  - 北海道南富良野高等學校。
  - 南富良野滑雪場（）
  - 道路車站「南富良野」（）：[北之道路車站](../Page/北之道路車站.md "wikilink")（）是一個遍及北海道各地的公路[休息站系統](../Page/休息站.md "wikilink")。
  - [南富良野町營循環巴士](../Page/南富良野町.md "wikilink")「農協前」、[占冠村營巴士](../Page/占冠村.md "wikilink")「幾寅車站前」站

## 歷史

  - 1902年（[明治](../Page/明治.md "wikilink")35年）12月6日 -
    以[北海道官設鐵道十勝線的車站開業](../Page/北海道官設鐵道.md "wikilink")。
  - 1905年（明治38年）4月1日 - 移交給[官設鐵道](../Page/日本國有鐵道.md "wikilink")。
  - 1982年（[昭和](../Page/昭和.md "wikilink")57年）11月15日 - 貨運服務廢止。
  - 1984年（昭和59年）2月1日 - 廢除行李處理。車站簡易委託化。
  - 1987年（昭和62年）4月1日 -
    在[國鐵分割民營化後由JR北海道承繼](../Page/國鐵分割民營化.md "wikilink")。
  - 1994年（[平成](../Page/平成.md "wikilink")6年）4月1日 -
    由[北海道旅客鐵道釧路分社改為](../Page/北海道旅客鐵道釧路分社.md "wikilink")[北海道旅客鐵道總社管轄](../Page/北海道旅客鐵道總社.md "wikilink")。
  - 2003年（平成15年）4月1日 - 簡易委託廢除，完全無人化。
  - 2016年（平成28年）8月31日 - 由於受到颱風影響，車站暫時停止營運。

## 相鄰車站

  - 北海道旅客鐵道（JR北海道）

    根室本線

      -
        [東鹿越](../Page/東鹿越站.md "wikilink")（T35）－**幾寅（T36）**－[落合](../Page/落合站_\(北海道\).md "wikilink")（T37）

`  *由於受到熱帶風暴蒲公英及獅子山影響,東鹿越(T35) - 新得(K23)之間停止營運,並以臨時巴士代替營運`

## 參見

  - [根室本線](../Page/根室本線.md "wikilink")
  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

## 外部連結

  - [幾寅站](http://www.mapion.co.jp/station/ST20031/)

  - [參觀全部車站之旅](http://www.tsuchibuta.com/jr-hokkaido/nemuroline/16ikutora/16ikutora.htm)

[Category:上川管內鐵路車站](../Category/上川管內鐵路車站.md "wikilink")
[Category:根室本線車站](../Category/根室本線車站.md "wikilink")
[Kutora](../Category/日本鐵路車站_I.md "wikilink")
[Category:1902年啟用的鐵路車站](../Category/1902年啟用的鐵路車站.md "wikilink")
[Category:南富良野町](../Category/南富良野町.md "wikilink")
[Category:北海道官設鐵道車站](../Category/北海道官設鐵道車站.md "wikilink")

1.