**广元市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[四川省下辖的](../Page/四川省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于四川省北部。东邻[巴中市](../Page/巴中市.md "wikilink")，南接[南充市](../Page/南充市.md "wikilink")，西连[绵阳市](../Page/绵阳市.md "wikilink")，北与[甘肃省](../Page/甘肃省.md "wikilink")[陇南市和](../Page/陇南市.md "wikilink")[陕西省](../Page/陕西省.md "wikilink")[汉中市接壤](../Page/汉中市.md "wikilink")。地处[四川盆地北部边缘](../Page/四川盆地.md "wikilink")，位于西安、成都、重庆、兰州四地的中心。[米仓山](../Page/米仓山.md "wikilink")、[大巴山南麓](../Page/大巴山.md "wikilink")，[嘉陵江上游](../Page/嘉陵江.md "wikilink")，川、陕、甘三省结合部。素有“[川北门户](../Page/川北.md "wikilink")”之称，是一座山水园林型现代化新兴城市。全市总面积1.63万平方公里，人口263万。

广元已有2300多年的建城历史。自古为入川的重要通道，是苴国故地，入蜀要塞，三国重镇，一代女皇武则天的故里，川陕革命根据地的重要组成部分，四川省旅游热点城市。广元获得的殊荣有：农科教结合示范区、国家园林城市、国家卫生城市、中国优秀旅游城市、新型工业化基地、中国温泉之乡。

## 历史

[秦](../Page/秦国.md "wikilink")[惠文王更元九年](../Page/惠文王.md "wikilink")（前316年），秦国兼并[苴国](../Page/苴国.md "wikilink")，在境内置[葭萌县](../Page/葭萌县.md "wikilink")。[秦代及](../Page/秦代.md "wikilink")[两汉沿袭葭萌县建制](../Page/两汉.md "wikilink")。[东汉](../Page/东汉.md "wikilink")[建安二十二年](../Page/建安.md "wikilink")（217年），[刘备改葭萌县为](../Page/刘备.md "wikilink")[汉寿县](../Page/汉寿县.md "wikilink")。[西晋改为](../Page/西晋.md "wikilink")[晋寿县](../Page/晋寿县.md "wikilink")。[东晋](../Page/东晋.md "wikilink")[太元十五年](../Page/太元.md "wikilink")（390年），析晋寿县置[兴安县](../Page/兴安县.md "wikilink")，治所在今广元城区，并置[晋寿郡](../Page/晋寿郡.md "wikilink")，晋寿县改为[益昌县](../Page/益昌县.md "wikilink")，郡县均治今[昭化镇](../Page/昭化镇.md "wikilink")。[南朝梁](../Page/南朝梁.md "wikilink")[大同年间置](../Page/大同_\(梁\).md "wikilink")[黎州](../Page/黎州.md "wikilink")。[西魏废帝三年](../Page/西魏.md "wikilink")（554年）改黎州为[利州](../Page/利州.md "wikilink")。

[隋](../Page/隋朝.md "wikilink")[开皇初废晋寿郡](../Page/开皇.md "wikilink")，开皇十八年（598年）改兴安县为[绵谷县](../Page/绵谷县.md "wikilink")。[大业初改利州为](../Page/大业.md "wikilink")[义城郡](../Page/义城郡.md "wikilink")，治绵谷县。[唐](../Page/唐朝.md "wikilink")[武德元年](../Page/武德.md "wikilink")（618年）改义城郡为利州，属[山南西道](../Page/山南西道.md "wikilink")。[宋代利州属](../Page/宋代.md "wikilink")[利州路](../Page/利州路.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[开宝五年](../Page/开宝.md "wikilink")（972年），[宋太祖改益昌县为](../Page/宋太祖.md "wikilink")[昭化县](../Page/昭化县.md "wikilink")，取“昭示皇恩，以化万民”之意。[元](../Page/元朝.md "wikilink")[至元十四年](../Page/至元.md "wikilink")（1277年）改置[广元路](../Page/广元路.md "wikilink")，改绵谷县为[广元县](../Page/广元县.md "wikilink")，《[大明一统志](../Page/大明一统志.md "wikilink")》记：“在《易》曰广大配天地，在《春秋》谓一为元，以示张大之意。”至元二十四年（1287年），省葭萌县入昭化县。[明](../Page/明朝.md "wikilink")[洪武四年](../Page/洪武.md "wikilink")（1371年）改广元路为[广元府](../Page/广元府.md "wikilink")；洪武九年（1376年）降府为州；二十二年（1389年）再降为广元县，属[保宁府](../Page/保宁府.md "wikilink")，省绵谷县。[清代因明制](../Page/清代.md "wikilink")。[民国二年](../Page/民国.md "wikilink")（1913年），广元县属[川北道](../Page/川北道.md "wikilink")，1914年属[嘉陵道](../Page/嘉陵道.md "wikilink")。1935年，[中国工农红军在广元等](../Page/中国工农红军.md "wikilink")6县建立[苏维埃政府](../Page/苏维埃.md "wikilink")。民国二十四年（1935年）置四川省第十四行政督察区，广元县属之。
[1964-11_1964年_四川广元.jpg](https://zh.wikipedia.org/wiki/File:1964-11_1964年_四川广元.jpg "fig:1964-11_1964年_四川广元.jpg")
[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，广元县属[川北行署区](../Page/川北行署区.md "wikilink")[剑阁专区](../Page/剑阁专区.md "wikilink")，专员公署驻广元县。1952年改为四川省[广元专区](../Page/广元专区.md "wikilink")。1953年广元专区省入[绵阳专区](../Page/绵阳专区.md "wikilink")。1959年3月，撤销昭化县，并入广元县。1968年绵阳专区改为[绵阳地区](../Page/绵阳地区.md "wikilink")。1985年2月，撤销广元县，设立地级广元市；原广元县改设[广元市市中区](../Page/广元市市中区.md "wikilink")；原属绵阳地区的[旺苍](../Page/旺苍.md "wikilink")、[青川](../Page/青川.md "wikilink")、[剑阁](../Page/剑阁.md "wikilink")3县划入。9月，划入[南充地区的](../Page/南充地区.md "wikilink")[苍溪县](../Page/苍溪县.md "wikilink")。1989年5月，从市中区析置[元坝](../Page/元坝.md "wikilink")、[朝天两区](../Page/朝天.md "wikilink")。2007年3月，市中区更名为[利州区](../Page/利州区.md "wikilink")。2008年5月12日发生[汶川大地震](../Page/汶川大地震.md "wikilink")，下属的青川县是极重灾区县，整个县城成为废墟。2013年4月，元坝区更名为[昭化区](../Page/昭化区.md "wikilink")。

## 地理

## 政治

### 现任领导

<table>
<caption>广元市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党广元市委员会.md" title="wikilink">中国共产党<br />
广元市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/广元市人民代表大会.md" title="wikilink">广元市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/广元市人民政府.md" title="wikilink">广元市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议广元市委员会.md" title="wikilink">中国人民政治协商会议<br />
广元市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/王菲_(1963年).md" title="wikilink">王菲</a>[1]</p></td>
<td><p><a href="../Page/邓光志.md" title="wikilink">邓光志</a>[2]</p></td>
<td><p><a href="../Page/邹自景.md" title="wikilink">邹自景</a>[3]</p></td>
<td><p><a href="../Page/杨凯_(1962年).md" title="wikilink">杨凯</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/藏族.md" title="wikilink">藏族</a></p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/巴中市.md" title="wikilink">巴中市</a></p></td>
<td><p>四川省<a href="../Page/剑阁县.md" title="wikilink">剑阁县</a></p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/文安县.md" title="wikilink">文安县</a></p></td>
<td><p>四川省<a href="../Page/小金县.md" title="wikilink">小金县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年2月</p></td>
<td><p>2016年9月</p></td>
<td><p>2016年3月</p></td>
<td><p>2016年9月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市辖3个[市辖区](../Page/市辖区.md "wikilink")、4个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[利州区](../Page/利州区.md "wikilink")、[昭化区](../Page/昭化区.md "wikilink")、[朝天区](../Page/朝天区.md "wikilink")
  - 县：[旺苍县](../Page/旺苍县.md "wikilink")、[青川县](../Page/青川县.md "wikilink")、[剑阁县](../Page/剑阁县.md "wikilink")、[苍溪县](../Page/苍溪县.md "wikilink")

此外，位于青川县境内的[唐家河国家级自然保护区为四川省林业厅直属的副县级事业单位](../Page/唐家河国家级自然保护区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>广元市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>510800</p></td>
</tr>
<tr class="odd">
<td><p>510802</p></td>
</tr>
<tr class="even">
<td><p>510811</p></td>
</tr>
<tr class="odd">
<td><p>510812</p></td>
</tr>
<tr class="even">
<td><p>510821</p></td>
</tr>
<tr class="odd">
<td><p>510822</p></td>
</tr>
<tr class="even">
<td><p>510823</p></td>
</tr>
<tr class="odd">
<td><p>510824</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>广元市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>广元市</p></td>
<td><p>2484122</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>利州区</p></td>
<td><p>516424</p></td>
<td><p>20.79</p></td>
</tr>
<tr class="even">
<td><p>昭化区</p></td>
<td><p>168489</p></td>
<td><p>6.78</p></td>
</tr>
<tr class="odd">
<td><p>朝天区</p></td>
<td><p>174333</p></td>
<td><p>7.02</p></td>
</tr>
<tr class="even">
<td><p>旺苍县</p></td>
<td><p>385787</p></td>
<td><p>15.53</p></td>
</tr>
<tr class="odd">
<td><p>青川县</p></td>
<td><p>222253</p></td>
<td><p>8.95</p></td>
</tr>
<tr class="even">
<td><p>剑阁县</p></td>
<td><p>457656</p></td>
<td><p>18.42</p></td>
</tr>
<tr class="odd">
<td><p>苍溪县</p></td>
<td><p>559180</p></td>
<td><p>22.51</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")2484123人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共减少578418人，减少18.89%。其中，男性人口为1251499人，占50.38%；女性人口为1232624人，占49.62%。总人口性别比（以女性为100）为101.53。0－14岁人口为412846人，占16.62%；15－64岁人口为1794394人，占72.23%；65岁及以上人口为276883人，占11.15%。[汉族人口为](../Page/汉族.md "wikilink")2474052人，占99.60%；各[少数民族人口为](../Page/少数民族.md "wikilink")10071人，占0.40%。

2017年末全市户籍人口302.62万人。其中，女性147.36万人，男性155.25万人，分别占总人口的48.7%和51.3%；城镇人口70.92万人，乡村人口231.70万人，分别占总人口的23.4%和76.6%。年末全市常住人口266.00万人。其中，城镇人口116.99万人，乡村人口149.01万人。常住人口城镇化率43.98%，比上年提高1.58个百分点。人口出生率10.37%，死亡率6.25%，自然增长率4.12%。\[9\]

## 交通

[宝成铁路與](../Page/宝成铁路.md "wikilink")[西成客運專線纵贯广元南北](../Page/西成客運專線.md "wikilink")，與貫穿東南與西北的[兰渝鐵路形成十字交匯](../Page/兰渝鐵路.md "wikilink")，[108国道](../Page/108国道.md "wikilink")、[212国道在境内交汇](../Page/212国道.md "wikilink")。[绵广高速公路已经全线贯通](../Page/绵广高速公路.md "wikilink")，广元至[巴中高速公路](../Page/巴中.md "wikilink")，广元至[陕西](../Page/陕西.md "wikilink")[宁强高速公路](../Page/宁强.md "wikilink")、广元至[南充高速公路](../Page/南充.md "wikilink")、广元至[甘肃高速公路](../Page/甘肃.md "wikilink")、成都经广元至[西安高速铁路均在建设之中](../Page/西安.md "wikilink")。[广元机场有直飞](../Page/广元机场.md "wikilink")[北京](../Page/北京.md "wikilink")、[广州](../Page/广州.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[上海等地的多条航线](../Page/上海.md "wikilink")。[嘉陵江水运可达重庆](../Page/嘉陵江.md "wikilink")。

## 旅游资源

广元境内有国家级的自然保护区9处，省级自然保护区18处，国家级文物保护单位8处，省级文物保护单位17处，旅游资源密集。著名旅游景点有：

  - [唐家河国家级自然保护区](../Page/唐家河.md "wikilink")，位于青川县
  - [东河口地震遗址公园](../Page/东河口地震遗址公园.md "wikilink")，位于青川县
  - [皇泽寺](../Page/皇泽寺.md "wikilink")，位于利州区
  - [剑门关](../Page/剑门关.md "wikilink")，位于剑阁县
  - [千佛崖](../Page/千佛崖.md "wikilink")、位于利州区
  - [天台山森林公园](../Page/天台山森林公园.md "wikilink")，现已更名为天曌山森林公园
  - [昭化古城](../Page/昭化古城.md "wikilink")，位于[昭化区](../Page/昭化区.md "wikilink")[昭化镇](../Page/昭化镇.md "wikilink")
  - [明月峡古栈道](../Page/明月峡古栈道.md "wikilink")
  - [米仓山森林公园](../Page/米仓山森林公园.md "wikilink")

## 参考资料

[Category:广元](../Category/广元.md "wikilink")
[Category:四川地级市](../Category/四川地级市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.