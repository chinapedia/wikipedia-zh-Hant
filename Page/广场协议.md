[USD-JPY_(Plaza_Accord).svg](https://zh.wikipedia.org/wiki/File:USD-JPY_\(Plaza_Accord\).svg "fig:USD-JPY_(Plaza_Accord).svg")
[JPY_EER.png](https://zh.wikipedia.org/wiki/File:JPY_EER.png "fig:JPY_EER.png")

《**广场协议**》（，简称：）是[美國](../Page/美國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[英国](../Page/英国.md "wikilink")、[法国及](../Page/法国.md "wikilink")[西德](../Page/联邦德国.md "wikilink")5个工业[发达国家](../Page/发达国家.md "wikilink")[財政部長和](../Page/財政部長.md "wikilink")[央行行长於美國](../Page/央行.md "wikilink")[纽约的](../Page/纽约.md "wikilink")[广场饭店会晤後](../Page/广场饭店.md "wikilink")，在1985年9月22日签署的協議。目的在联合干预[外汇市场](../Page/外汇市场.md "wikilink")，使[美元对](../Page/美元.md "wikilink")[日元及](../Page/日元.md "wikilink")[德國馬克等主要货币有秩序性地下调](../Page/德國馬克.md "wikilink")，以解决美国巨额[贸易赤字](../Page/贸易赤字.md "wikilink")，从而导致日元大幅[升值](../Page/升值.md "wikilink")。

《广场协议》签订后，上述五国开始联合干预[外汇市场](../Page/外汇市场.md "wikilink")，在国际外汇市场大量抛售美元，继而形成市场[投资者的抛售狂潮](../Page/投资者.md "wikilink")，导致美元持续大幅度[贬值](../Page/貶值.md "wikilink")\[1\]。1985年9月，美元兑日元在1美元兑250日元上下波动，协议签订后不到3个月的时间里，日元兑美元迅速升值到1美元兑200日元左右，升幅20%。1988年与1985年相比，主要货币对美元的升值幅度大约分别为：日元86.1%、德国马克70.5%、法国[法郎](../Page/法國法郎.md "wikilink")50.8%、意大利[里拉](../Page/意大利里拉.md "wikilink")46.7%、英国[英镑](../Page/英镑.md "wikilink")37.2%、加拿大[元近](../Page/加拿大元.md "wikilink")11%。

作为1980年代的世界第二大经济体（1978年时超过了[苏联](../Page/苏联.md "wikilink")），日本亲眼目睹其由经济起飞后快速窜升之势，逐渐演变为缓慢增长、停止增长乃至严重[衰退](../Page/经济衰退.md "wikilink")\[2\]，到了1990年代中期又经历货币快速贬值（时称“抛售日本”），从此一蹶不振，[泡沫经济破裂](../Page/日本泡沫经济.md "wikilink")、崩盘，經過二十餘年仍未恢复元气。

1987年2月，[七大主要工业国政府签定](../Page/八大工業國組織.md "wikilink")《[卢浮宫协议](../Page/卢浮宫协议.md "wikilink")》，《广场协议》被取代。

## 另見

  - [日元升值蕭條](../Page/日元升值蕭條.md "wikilink")
  - [日本经济](../Page/日本经济.md "wikilink")
  - [日本泡沫经济](../Page/日本泡沫经济.md "wikilink")
  - [失去的十年](../Page/失去的十年.md "wikilink")

## 參考資料

## 外部連結

  - [Announcement the Ministers of Finance and Central Bank Governors of
    France, Germany, Japan, the United Kingdom, and the United States
    (Plaza Accord)](http://www.g8.utoronto.ca/finance/fm850922.htm)
  - [U.S. Treasury - Exchange Stabilization Fund, Intervention
    Operations 1985-90](https://web.archive.org/web/20101112234118/http://www.treas.gov/offices/international-affairs/esf/history/#IO8590)
  - [Plaza Agreement, ANZ Financial Dictionary from Language of Money by
    Edna
    Carew](http://www.anz.com/edna/dictionary.asp?action=content&content=plaza_agreement)

[Category:昭和時代戰後經濟](../Category/昭和時代戰後經濟.md "wikilink")
[Category:1985年美國](../Category/1985年美國.md "wikilink")
[Category:1985年日本](../Category/1985年日本.md "wikilink")
[Category:1985年法國](../Category/1985年法國.md "wikilink")
[Category:1985年英國](../Category/1985年英國.md "wikilink")
[Category:1985年西德](../Category/1985年西德.md "wikilink")
[Category:1985年9月](../Category/1985年9月.md "wikilink")

1.
2.