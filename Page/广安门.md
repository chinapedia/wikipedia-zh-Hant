[Beijing_Guanganmen_1910.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Guanganmen_1910.jpg "fig:Beijing_Guanganmen_1910.jpg")
**广安门**又称**广宁门**、**张仪门**、**彰仪门**，建于[明代](../Page/明代.md "wikilink")[嘉靖年间](../Page/嘉靖.md "wikilink")，是[北京老城](../Page/北京.md "wikilink")20座城门中的一座，位于外城西垣正中偏北。是进出京城的主要通道，在清朝[乾隆年间](../Page/乾隆.md "wikilink")，[昆山](../Page/昆山.md "wikilink")[顾森所写的](../Page/顾森.md "wikilink")《燕京记》中是这样提到广安门的：“外城七门，面向西者广宁门，西行三十里[卢沟桥](../Page/卢沟桥.md "wikilink")，过桥四十里即是[良乡县](../Page/良乡.md "wikilink")，为各省陆路进京之咽喉。”

广安门城楼已经不复存在，原高两层，是重檐[歇山顶](../Page/歇山顶.md "wikilink")[三滴水楼阁式建筑](../Page/三滴水.md "wikilink")，灰筒瓦绿[琉璃瓦剪边顶](../Page/琉璃瓦.md "wikilink")，面阔三间，通宽13.8米；进深一间，进深6米；高17.6米；城楼和城台的总高度为26米，瓮城为圆角方形，宽39米，深34米。民间流传的《里程歌》里这么描述广安门城楼：“彰仪门城楼九丈高，[小井](../Page/小井.md "wikilink")[大井卢沟桥](../Page/大井.md "wikilink")……”

## 历史

广安门[明代称广宁门](../Page/明代.md "wikilink")，又名彰义门（该门与[金中都彰义门在同一轴线上](../Page/金中都.md "wikilink")）。清朝[道光年间为避](../Page/道光.md "wikilink")[清宣宗旻宁之](../Page/清宣宗.md "wikilink")[讳改为现名](../Page/避讳.md "wikilink")。原规制与[广渠门相同](../Page/广渠门.md "wikilink")，[乾隆三十一年](../Page/乾隆.md "wikilink")，以该门为南方各省进京的主要通路，故提高城门规格，仿[永定门城楼加以改建](../Page/永定门.md "wikilink")。

由于广安门是各省陆路进京的必经之路，因此广安门内的彰仪门大街（即今天的广安门内大街）在清朝时期是比较繁华的，有“一进彰仪门，银子碰倒人”的说法。[雍正年间](../Page/雍正.md "wikilink")，因为皇帝打算在河北修建皇陵，雍正帝下令从广安门到[宛平城修筑石板路](../Page/宛平.md "wikilink")。广安门到小井村的路段长1500丈，共花费白银八万两，平均每尺长的道路用去白银五两三钱三分，因此有“一尺道路五两三”的说法。这条道路的修通对广安门地区的发展起到很好的促进作用。
[Jztl-4.jpg](https://zh.wikipedia.org/wiki/File:Jztl-4.jpg "fig:Jztl-4.jpg")起点车站——广安门车站\]\]

在北京与各地之间的铁路、公路修通之后，广安门的重要地位逐渐下降，至1949年[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，从城门到菜市口大街并没有二层楼房，这条大街也并非是商铺相连的大街。1956年，广安门被以“年久失修、阻碍交通”的理由拆除。

2001年修成通车的[两广路](../Page/两广路.md "wikilink")，使广安门一带的经济得到了促进，正逐渐成为北京重要的商业中心之一。

[Category:北京外城城门](../Category/北京外城城门.md "wikilink")