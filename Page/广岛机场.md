**廣島機場**（），（）是一座位於[日本](../Page/日本.md "wikilink")[廣島縣](../Page/廣島縣.md "wikilink")[三原市的民用機場](../Page/三原市.md "wikilink")，也是日本[中國地方最大的機場](../Page/中國地方.md "wikilink")。

## 歷史

  - 1993年：新廣島機場開始使用。位於[廣島市的舊廣島機場成為廣島縣政府管理的](../Page/廣島市.md "wikilink")「[廣島西機場](../Page/廣島西機場.md "wikilink")」。
  - 1994年：新廣島機場更名為廣島機場。
  - 2001年：跑道由2500公尺延長至3000公尺。

## 航空公司與航點

## 意外事件

  - 2015年4月14日，[韓亞航空OZ162班機](../Page/韓亞航空162號班機事故.md "wikilink")（機型空中巴士A320-200，機身編號HL7762），執行韓國仁川飛往日本廣島，實施28跑道落地，飛機進場高度過低，撞上跑道前端儀表著陸系統的天線，最後停止於跑道旁邊的草地上。該機左機翼和左發動機嚴重受損，側起落架機輪也受損。機上共28名人員受傷。\[1\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [廣島機場](http://www.hij.airport.jp/)

{{-}}

[Category:中國地方機場](../Category/中國地方機場.md "wikilink")
[Category:廣島縣交通](../Category/廣島縣交通.md "wikilink")
[Category:1993年启用的机场](../Category/1993年启用的机场.md "wikilink")

1.