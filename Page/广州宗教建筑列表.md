以下是**[广州市](../Page/广州市.md "wikilink")**各**[宗教](../Page/宗教.md "wikilink")**的[建筑列表](../Page/建筑.md "wikilink")。

## 佛教

  - [光孝寺](../Page/光孝寺_\(广州\).md "wikilink")
  - [六榕寺](../Page/六榕寺.md "wikilink")
  - [华林寺](../Page/华林寺_\(广州\).md "wikilink")
  - [海幢寺](../Page/海幢寺.md "wikilink")
  - [大佛寺](../Page/大佛寺_\(广州\).md "wikilink")
  - [萝峰寺](../Page/萝峰寺.md "wikilink")
  - 能仁寺
  - 无着庵
  - 陶轮学社
  - [东山庙](../Page/东山庙.md "wikilink")（已不存）
  - 万寿寺

其中，光孝寺、六榕寺、华林寺、海幢寺并称广州四大名寺。
光孝寺、六榕寺、华林寺、海幢寺与大佛寺并称广州五大丛林。

## 道教

### 现存的道观

  - [三元宫](../Page/三元宫_\(广州\).md "wikilink")
  - [纯阳观](../Page/純陽觀_\(廣州\).md "wikilink")
  - [五仙观](../Page/五仙观.md "wikilink")
  - 白云仙馆 (现不作宗教用途)
  - [仁威庙](../Page/仁威庙.md "wikilink")
  - [城隍庙](../Page/广州都城隍庙.md "wikilink")
  - [南海神庙](../Page/南海神庙.md "wikilink")
  - [杨箕玉虚宫](../Page/杨箕玉虚宫.md "wikilink")
  - 黄大仙祠
  - [南沙天后宫](../Page/南沙天后宫.md "wikilink")
  - [廣東圓玄道觀](../Page/廣東圓玄道觀.md "wikilink")
  - 五岳殿

### 已湮没的宫观

  - 元妙观
  - 应元宫
  - 修元精舍
  - 云泉山馆
  - 碧虚观
  - 郑仙翁祠\[1\]

## 伊斯兰教

  - [怀圣寺](../Page/怀圣寺.md "wikilink")
  - 东营寺
  - 南胜寺（已不存）
  - [濠畔寺](../Page/濠畔街清真寺.md "wikilink")
  - ~~东郊寺~~（已拆除）
  - [清真先贤古墓](../Page/先贤清真寺.md "wikilink")

## 基督宗教

### 天主教

  - [石室耶稣圣心堂](../Page/石室圣心大教堂.md "wikilink")（[天主教广州总教区](../Page/天主教广州总教区.md "wikilink")[主教座堂](../Page/主教座堂.md "wikilink")）
  - [沙面露德圣母堂](../Page/沙面露德圣母堂.md "wikilink")
  - 河南宝岗圣母圣心堂
  - 东山圣五伤方济各堂
  - 已不存：小北圣依纳爵堂、淘金坑主佑天主堂、中山四路圣阿纳堂、西华路西山圣母堂

### 新教

  - [东山堂](../Page/东山浸信会堂.md "wikilink")
  - [救主堂](../Page/广州救主堂.md "wikilink")
  - [锡安堂](../Page/广州锡安堂.md "wikilink")
  - [光孝堂](../Page/光孝堂.md "wikilink")
  - [十甫堂](../Page/十甫堂.md "wikilink")
  - [芳村堂](../Page/芳村堂.md "wikilink")
  - [洪德堂](../Page/洪德堂.md "wikilink")
  - [沙面堂](../Page/沙面堂.md "wikilink")
  - 沙河堂
  - 现不作宗教用途：[西村堂](../Page/西村堂.md "wikilink")、[仁济堂](../Page/仁济堂.md "wikilink")、[信义会芳村堂](../Page/信义会芳村堂.md "wikilink")
  - [天河堂](../Page/天河堂.md "wikilink")
  - [逢源堂](../Page/逢源堂.md "wikilink")（已不存）
  - 已拆除：~~[惠爱堂](../Page/惠爱堂.md "wikilink")~~、~~[万善堂](../Page/万善堂.md "wikilink")~~、~~[小港堂](../Page/小港堂.md "wikilink")~~、~~[东石浸信会](../Page/东石浸信会.md "wikilink")~~

## 市级重点文物保护单位

[市级重点文物保护单位](../Page/广州市各级文物保护单位列表#广州市文物保护单位.md "wikilink")21处，佛教：[柯子岭六榕寺和尚墓塔群](../Page/柯子岭六榕寺和尚墓塔群.md "wikilink")、[华林寺罗汉堂及](../Page/华林寺_\(广州\)#五百罗汉堂.md "wikilink")[祖师墓地](../Page/华林寺祖师墓群.md "wikilink")、[海幢寺大雄宝殿塔殿](../Page/海幢寺.md "wikilink")、金花古庙；道教：[三元宫](../Page/三元宫_\(广州\).md "wikilink")、[纯阳观](../Page/纯阳观_\(广州\).md "wikilink")、[城隍庙](../Page/广州都城隍庙.md "wikilink")、[茶塘村洪圣古庙](../Page/茶塘村洪圣古庙.md "wikilink")、盘古神坛、[卧云庐](../Page/卧云庐.md "wikilink")、圣母宫庙；伊斯兰教：[濠畔街清真寺](../Page/濠畔街清真寺.md "wikilink")、[回教坟场](../Page/回教坟场.md "wikilink")；基督新教：[东山浸信会旧址](../Page/基督教东山堂.md "wikilink")、[光孝堂](../Page/光孝堂.md "wikilink")、[洪德堂旧址](../Page/洪德堂.md "wikilink")、[仁济堂](../Page/仁济堂.md "wikilink")、圣经学院旧址；拜火教：[巴斯教徒墓地](../Page/巴斯教徒墓地.md "wikilink")、[波斯楼](../Page/波斯楼.md "wikilink")

## 参考文献

[Category:中国宗教场所列表](../Category/中国宗教场所列表.md "wikilink")
[Category:广东文化列表](../Category/广东文化列表.md "wikilink")
[Category:廣州建築物列表](../Category/廣州建築物列表.md "wikilink")
[Category:廣州旅遊相關列表](../Category/廣州旅遊相關列表.md "wikilink")
[\*](../Category/广州宗教建筑.md "wikilink")

1.