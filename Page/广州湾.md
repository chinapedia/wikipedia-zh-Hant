廣東省 |flag_p1 = Flag of the Qing Dynasty (1889-1912).svg |s1 = 湛江市
|flag_s1 = Flag of the Republic of China.svg |image_flag =
Ministre-DOMTOM.svg |flag = 法屬印度支那國旗 |image_coat = Emblem of the
Gouvernement général de l'Indochine.svg |symbol = |symbol_type = 總督府徽章
|image_map = KouangTcheouWang.JPG |image_map2 = Location Guangzhou
Wan.png |image_map2_caption = 廣州灣在法屬印度支那的位置 |capital =
[白瓦特城](../Page/湛江.md "wikilink") |common_languages =

  - [法語](../Page/法語.md "wikilink") (官方)
  - [粵語](../Page/粵語.md "wikilink")
  - [雷州话](../Page/雷州话.md "wikilink")

|title_leader = [總督](../Page/法屬印度支那總督.md "wikilink") |leader1 =
[法屬印度支那總督](../Page/法屬印度支那總督.md "wikilink") |title_deputy
= |deputy1 = |era = 新帝國主義 |stat_year1 = 1899年 |stat_area1 = 1300
|stat_year2 = 1911年 |stat_pop2 = 189000 |stat_year3 = 1935
|stat_pop3 = 209000 |currency =
[法屬印度支那元](../Page/法屬印度支那元.md "wikilink") }}
[Liangguang_circa_1900_en.png](https://zh.wikipedia.org/wiki/File:Liangguang_circa_1900_en.png "fig:Liangguang_circa_1900_en.png")地域之位置，約1900年\]\]
**广州湾**（，又譯作**Kwang Chow
Wan**、**Kwang-Chou-Wan**等）是位于中國廣東[雷州半島東北的地區](../Page/雷州半島.md "wikilink")，今屬[湛江市](../Page/湛江市.md "wikilink")。廣州灣在1899年11月由于[法國与](../Page/法蘭西第三共和國.md "wikilink")[清政府签订的](../Page/清政府.md "wikilink")《[中法互订广州湾租界条约](../Page/广州湾租界条约.md "wikilink")》成为[租借地](../Page/租借地.md "wikilink")，直到1943年被日本佔領並在1945年日本投降後由法國歸還中國。廣州灣租借地是歷史上[法國在中國的最大的殖民地或租借地](../Page/法國.md "wikilink")，總面積1300平方公里，比香港新界還要大一點點，範圍約等於今[湛江市城区](../Page/湛江市.md "wikilink")。

一道海灣深入廣州灣租借地，以東原屬於高州府吳川縣，以西則屬雷州府遂溪縣。下轄兩個主要城區，分別為行政中心西營（Fort
Bayard，今霞山區，早期曾設在海灣東岸的麻斜），與商業中心赤坎。此外，還有多個墟鎮中心和較小港埠。至今湛江仍是[香港與](../Page/香港.md "wikilink")[中南半島之間吞吐量最大的深水良港](../Page/印度支那.md "wikilink")。\[1\]

“广州湾”此名称形成于明清时期，据考证因南三岛有“广州湾”村坊而得名。

## 歷史

1899年11月[法国与清政府签订](../Page/法国.md "wikilink")《[中法互订广州湾租界条约](../Page/中法互订广州湾租界条约.md "wikilink")》，将[遂溪](../Page/遂溪县.md "wikilink")、[吴川两县属部分陆地](../Page/吴川县.md "wikilink")、岛屿以及两县间麻斜海湾（今[湛江港](../Page/湛江港.md "wikilink")）划为法国[租界](../Page/租界.md "wikilink")，统称“广州湾”，划入[法属印度支那联邦范围](../Page/法属印度支那.md "wikilink")，派专员（Commissaire）就任，设广州湾行政总公使署，受法国[北圻統使节制](../Page/北圻統使.md "wikilink")，为当时法属印支六使之一（余五者分别为[北圻统使](../Page/北圻统使.md "wikilink")、[中圻钦使](../Page/中圻钦使.md "wikilink")、[南圻统督](../Page/南圻统督.md "wikilink")、和）。

1943年2月，为[日军所占](../Page/日军.md "wikilink")，曾建立短暂日伪政权。1945年，[日本二戰投降后](../Page/日本二戰投降.md "wikilink")，經中法交涉，10月19日由[中华民国政府收回](../Page/中华民国政府.md "wikilink")。1946年1月以广州湾范围划设市治，定名[湛江市](../Page/湛江市.md "wikilink")。

## 建筑

  - [广州湾法国公使署旧址](../Page/广州湾法国公使署旧址.md "wikilink")

## 参考文献

## 外部連結

{{-}}

[Category:租借地](../Category/租借地.md "wikilink")
[Category:湛江历史](../Category/湛江历史.md "wikilink")
[Category:广东行政区划史](../Category/广东行政区划史.md "wikilink")
[Category:前法國殖民地](../Category/前法國殖民地.md "wikilink")
[Category:城邦](../Category/城邦.md "wikilink")

1.  [香港今昔：被遺忘的海岸廣州灣](http://www.nanzaozhinan.com/tc/zhuan-ti/825/xiang-gang-jin-xi-bei-yi-wang-de-hai-an-guang-zhou-wan)，南早中文網
    2014-03-12。