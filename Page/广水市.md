**广水市**位于[中国](../Page/中华人民共和国.md "wikilink")[湖北省东北部](../Page/湖北省.md "wikilink")，是[随州市代管的一个](../Page/随州市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。有鄂北门户和“湖北书法之乡”之称。原为应山县。广水市标为火凤。

## 历史沿革

[南朝宋析随县置](../Page/南朝宋.md "wikilink")**[永阳县](../Page/永阳县.md "wikilink")**，为建县之始，隶[司州](../Page/司州.md "wikilink")[随阳郡](../Page/随阳郡.md "wikilink")。[南梁](../Page/南梁.md "wikilink")[大同二年](../Page/大同_\(南梁\).md "wikilink")（536年）置[应州](../Page/应州_\(南梁\).md "wikilink"),
又置[平靖郡领永阳县](../Page/平靖郡.md "wikilink")。

[隋朝](../Page/隋朝.md "wikilink")[开皇三年](../Page/开皇.md "wikilink")（583年）省平靖郡，由应州领[平靖县和永阳县](../Page/平靖县.md "wikilink")。开皇十八年（598年），改永阳县为**[应山县](../Page/應山縣.md "wikilink")**，这是“应山”一名的最早来历。该县得名应为市中心有一山名为应山，后讹传为印山，即今日市区中心印台山。
[大业二年](../Page/大业_\(年号\).md "wikilink")（606年）废应州，将平靖县并入应山县，隶属[安陆郡](../Page/安陆郡.md "wikilink")。

[唐朝](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年）复置应州，领应山县、[礼山县](../Page/礼山县_\(隋朝\).md "wikilink")；武德八年（625年）又废应州，将礼山县并入应山县，属[淮南道](../Page/淮南道.md "wikilink")[安州](../Page/安州_\(西魏\).md "wikilink")。

[北宋属](../Page/北宋.md "wikilink")[荆湖北路](../Page/荆湖北路.md "wikilink")[德安府](../Page/德安府.md "wikilink")，[开宝年间废吉阳县](../Page/开宝.md "wikilink")，吉阳山以北并入应山县。[元朝隶](../Page/元朝.md "wikilink")[随州](../Page/随州_\(古代\).md "wikilink")，并属荆湖北路德安府。[明](../Page/明朝.md "wikilink")[清两朝延其制](../Page/清朝.md "wikilink")，惟[雍正七年后直属](../Page/雍正.md "wikilink")[德安府](../Page/德安府.md "wikilink")，不再隶属随州。

民国时隶属变动频繁，先后直属于[湖北省](../Page/湖北省_\(中華民國\).md "wikilink")、属[江汉道](../Page/江汉道.md "wikilink")、复直属湖北省、属湖北省第五行政督察区、属第三行政督察区。

1949年3月30日，中共政权占领应山县，此后属[孝感专区](../Page/孝感专区.md "wikilink")。1959年改属[武汉市](../Page/武汉市.md "wikilink")。1961年复属孝感专区。1988年12月撤销应山县，设广水市，以下辖广水镇（现[广水街道](../Page/广水街道.md "wikilink")，位于[京广铁路上](../Page/京广铁路.md "wikilink")）命名，但县治不变。2000年7月15日，广水市改由随州市代管。\[1\]

## 地理

广水市位于湖北省北部偏东，北与[河南省](../Page/河南省.md "wikilink")[信阳市毗邻](../Page/信阳市.md "wikilink")，东邻[大悟县](../Page/大悟县.md "wikilink")，南接[孝昌县](../Page/孝昌县.md "wikilink")、[安陆市](../Page/安陆市.md "wikilink")，西连[随州市](../Page/随州市.md "wikilink")[曾都区](../Page/曾都区.md "wikilink")。东西宽57.5公里，南北长78.7公里，面积2647平方公里。

广水地处[桐柏山脉南麓](../Page/桐柏山.md "wikilink")、[大别山脉西端](../Page/大别山.md "wikilink"),
属低山丘陵地带。最高处[大贵山海拨](../Page/大贵山.md "wikilink")907.8米，最低处平林的河床海拔仅37米。\[2\]

## 区划人口

现辖3个街道办事处、10镇、4乡以及中华山、大贵寺、花山3个国营林场、三潭风景区和广水经济开发区。

2000年, 全市年末总人口为879216人。\[3\]

该区域主要语言是汉语方言西南官话。 该区域在太平天国叛乱后有部分人口移居至安徽，主要分布于宁国市大部及周边郎溪县、广德县、宣城市、泾县。
宁国市大部及周边郎溪县、广德县、宣城市、泾县方言和应山方言基本一致。

## 交通

  - [京广铁路经市境东部武胜关](../Page/京广铁路.md "wikilink")、广办、杨寨，设[广水站](../Page/广水站.md "wikilink")
  - [汉丹铁路经市境西部长岭](../Page/汉丹铁路.md "wikilink")、马坪两镇，1980年代设马坪站、长岭站、平林站，后汉丹线提速，撤销三站。
  - [107国道经市境东部武胜关](../Page/107国道.md "wikilink")、广办、杨寨，到[武汉](../Page/武汉.md "wikilink")[天河国际机场车程](../Page/天河国际机场.md "wikilink")1小时
  - [316国道经本市西部长岭镇](../Page/316国道.md "wikilink")、马坪镇穿镇而过；长岭镇距武汉150公里、距襄阳180公里。
  - S28高速（麻竹）贯穿市境东西，东与G4（京珠高速）相接、西与G70（汉十高速）相接，横跨107、316两条国道，有杨寨、广水（市府所在地应山）、关庙三个出入口。

## 经济

2008年国民生产总值106.42亿元，比上年增长14.3%。

现有中型企业6家，小型企业154家。以食品加工、冶金铸造、建材、医药化工、造纸包装、通用设备制造、纺织服装为支柱产业。\[4\]

## 教育

  - 广水市第一高级中学
  - 广水市第二高级中学
  - 广水市第三高级中学
  - 广水市第四高级中学
  - 广水市实验高级中学
  - 广水市育才高级中学
  - 广水文华高级中学
  - 广水市南关高级中学
  - 广水市实验中学
  - 广水市外国语学校
  - 广水市广场中学
  - 广水市广才中学
  - 广水市实验小学

广水市吴店镇中心中学

## 名胜古迹

  - [三潭风景区](../Page/三潭风景区.md "wikilink")
  - [大贵寺国家森林公园](../Page/大贵寺国家森林公园.md "wikilink")
  - [中华山国家森林公园](../Page/中华山国家森林公园.md "wikilink")
  - [高峰寺水库](../Page/高峰寺水库.md "wikilink")
  - [徐家河水库](../Page/徐家河水库.md "wikilink")
  - [鸡公山](../Page/鸡公山.md "wikilink")
  - 黑龙潭瀑布群
  - [黑洞湾水库](../Page/黑洞湾水库.md "wikilink")
  - 九斗沟瀑布
  - [花山水库](../Page/花山水库.md "wikilink")
  - [飞沙河水库](../Page/飞沙河水库.md "wikilink")
  - [许家冲水库](../Page/许家冲水库.md "wikilink")
  - 观音寺遗址
  - [金鸡河](../Page/金鸡河.md "wikilink")
  - 将军寨
  - 平康寨
  - [龙兴沟水库](../Page/龙兴沟水库.md "wikilink")
  - 杨涟墓
  - 龙兴寺遗址
  - [霞家河水库](../Page/霞家河水库.md "wikilink")
  - 空山寺
  - 古仙人洞
  - 宝林寺遗址
  - [武胜关](../Page/武胜关.md "wikilink")
  - [黄土关](../Page/黄土关.md "wikilink")
  - [平靖关](../Page/平靖关.md "wikilink")

## 名人

  - [连舜宾](../Page/连舜宾.md "wikilink")
  - [郑獬](../Page/郑獬.md "wikilink")
  - [连南夫](../Page/连南夫.md "wikilink")
  - [李庭芝](../Page/李庭芝.md "wikilink")
  - [颜木](../Page/颜木.md "wikilink")
  - [杨涟](../Page/杨涟.md "wikilink")
  - [洪起元](../Page/洪起元.md "wikilink")
  - [左绍佐](../Page/左绍佐.md "wikilink")
  - [聂洗](../Page/聂洗.md "wikilink")
  - [程瑞霖](../Page/程瑞霖.md "wikilink")
  - [何子述](../Page/何子述.md "wikilink")
  - [邹家尤](../Page/邹家尤.md "wikilink")

## 外部链接

  - [广水市人民政府网](http://www.zggs.gov.cn/)
  - [广水搜房网](http://www.nihaogs.com/)
  - [广水市第一中学](https://web.archive.org/web/20170101162346/http://www.hbgsyz.com/)

## 参考文献

[广水市](../Category/广水市.md "wikilink")
[市](../Category/随州区县市.md "wikilink")
[随州市](../Category/湖北县级市.md "wikilink")
[鄂](../Category/中国小城市.md "wikilink")

1.
2.  <http://www.zggs.gov.cn/2005-07/29/cms241348article.shtml>
3.
4.