**广运**（1034年八月-1036年）是[西夏景宗](../Page/西夏.md "wikilink")[李元昊的](../Page/李元昊.md "wikilink")[年號](../Page/年號.md "wikilink")，共計3年。

## 纪年

| 广运                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1034年                          | 1035年                          | 1036年                          |
| [干支](../Page/干支纪年.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[广运年号](../Page/广运.md "wikilink")
  - 同期存在的其他政权年号
      - [景祐](../Page/景祐.md "wikilink")（1034年至1038年十一月）：宋—宋仁宗趙禎之年號
      - [正治](../Page/正治_\(段素真\).md "wikilink")（1027年至1041年）：[大理](../Page/大理國.md "wikilink")—[段素真之年號](../Page/段素真.md "wikilink")
      - [天成](../Page/天成_\(李佛瑪\).md "wikilink")（1028年至1034年）：李朝—[李佛瑪之年號](../Page/李佛瑪.md "wikilink")
      - [通瑞](../Page/通瑞.md "wikilink")（1034年至1039年）：李朝—李佛瑪之年號
      - [重熙](../Page/重熙.md "wikilink")（1032年十一月至1055年八月）：契丹—遼興宗耶律宗真之年號
      - [長元](../Page/長元.md "wikilink")（1028年七月二十五日至1037年四月二十一日）：日本後一條天皇與[後朱雀天皇年号](../Page/後朱雀天皇.md "wikilink")

[Category:西夏年号](../Category/西夏年号.md "wikilink")
[Category:11世纪中国年号](../Category/11世纪中国年号.md "wikilink")
[Category:1030年代中国政治](../Category/1030年代中国政治.md "wikilink")