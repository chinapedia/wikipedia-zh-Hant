**庐州府**是[明](../Page/明朝.md "wikilink")、[清两代的一个](../Page/清朝.md "wikilink")[府](../Page/府_\(行政区划\).md "wikilink")，位于[长江下游北岸](../Page/长江.md "wikilink")、[巢湖流域](../Page/巢湖.md "wikilink")，辖区大致相当于今日安徽省的[合肥市及](../Page/合肥市.md "wikilink")[巢湖市部分地区](../Page/巢湖市.md "wikilink")。

[元朝时](../Page/元朝.md "wikilink")，为[庐州路](../Page/庐州路.md "wikilink")。[至正二十四年](../Page/至正.md "wikilink")（[龙凤十年](../Page/龍鳳_\(韓林兒\).md "wikilink")，1364年），[朱元璋政权改为庐州府](../Page/朱元璋.md "wikilink")。[明朝](../Page/明朝.md "wikilink")，庐州府属[南直隶](../Page/南直隶.md "wikilink")，领二州：[无为州](../Page/无为州.md "wikilink")、[六安州](../Page/六安州.md "wikilink")，六县：[合肥县](../Page/合肥县.md "wikilink")、[舒城县](../Page/舒城县.md "wikilink")、[庐江县](../Page/庐江县.md "wikilink")、[巢县](../Page/巢县.md "wikilink")（属无为州）、[英山县](../Page/英山县.md "wikilink")（属六安州）、[霍山县](../Page/霍山县.md "wikilink")（属六安州）。[洪武二十六年](../Page/洪武.md "wikilink")（1393年），编户四万八千七百二十，口三十六万七千二百。[弘治四年](../Page/弘治_\(明朝\).md "wikilink")（1491年），户三万六千五百四十八，口四十八万六千五百四十九。[万历六年](../Page/万历.md "wikilink")（1578年），户四万七千三百七十三，口六十二万二千六百九十八。

[清朝初期属](../Page/清朝.md "wikilink")[江南省](../Page/江南省.md "wikilink")，分省后属于[安徽省](../Page/安徽省.md "wikilink")。下辖四县：合肥县（首县）、舒城县、庐江县、巢县，以及一个[散州](../Page/散州.md "wikilink")：无为州。清代，庐州府与本省的[安庆府](../Page/安庆府.md "wikilink")、[凤阳府](../Page/凤阳府.md "wikilink")、[滁州直隶州](../Page/滁州直隶州.md "wikilink")、[和州直隶州](../Page/和州直隶州.md "wikilink")、[六安直隶州相邻](../Page/六安直隶州.md "wikilink")，南面隔长江与本省[太平府](../Page/太平府_\(安徽省\).md "wikilink")、[池州府相望](../Page/池州府.md "wikilink")。1912年（民国元年），撤废庐州府。

## 参考资料

  - 《[明史](../Page/明史.md "wikilink")·志第十六·地理一》

[Category:明朝的府](../Category/明朝的府.md "wikilink")
[Category:清朝的府](../Category/清朝的府.md "wikilink")
[Category:安徽的府](../Category/安徽的府.md "wikilink")
[Category:合肥行政区划史](../Category/合肥行政区划史.md "wikilink")
[Category:六安行政区划史](../Category/六安行政区划史.md "wikilink")
[Category:芜湖行政区划史](../Category/芜湖行政区划史.md "wikilink")
[Category:1364年建立的行政区划](../Category/1364年建立的行政区划.md "wikilink")
[Category:1912年废除的行政区划](../Category/1912年废除的行政区划.md "wikilink")