[Hip_roof.svg](https://zh.wikipedia.org/wiki/File:Hip_roof.svg "fig:Hip_roof.svg")

**殿顶**，古稱**房**、**阿殿**，[宋朝称](../Page/宋朝.md "wikilink")**庑殿**或**四阿顶**，清朝称“庑殿”或“**五脊殿**”，[日語稱](../Page/日語.md "wikilink")**寄棟造**（[假名](../Page/假名.md "wikilink")：）是[中国](../Page/中国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[朝鮮古代](../Page/朝鮮半島.md "wikilink")[建筑的一种屋顶样式](../Page/建筑.md "wikilink")。在中國是各屋顶样式中等级最高的，高于[歇山式](../Page/歇山式.md "wikilink")。明清時只有皇家和[孔子殿堂才可以使用](../Page/孔子.md "wikilink")。唐朝時和[日本也見於](../Page/日本.md "wikilink")[佛寺建築](../Page/佛寺.md "wikilink")。但在[福建沿海地區和](../Page/福建.md "wikilink")[琉球的民居為了防風而採用廡殿頂](../Page/琉球.md "wikilink")。

庑殿顶是“四出水”的五脊四坡式，由一条[正脊和四条](../Page/正脊.md "wikilink")[垂脊](../Page/垂脊.md "wikilink")（一说[戗脊](../Page/戗脊.md "wikilink")）共五脊组成，因此又称五脊殿。由于屋顶有四面斜坡，故又称四阿顶。

庑殿顶又分为单檐和重檐两种，所谓重檐，就是在上述屋顶之下，四角各加一条短檐，形成第二檐。[北京故宫的](../Page/故宮博物院.md "wikilink")[太和殿就是重檐庑殿顶](../Page/太和殿.md "wikilink")，而故宫的[英华殿](../Page/英华殿.md "wikilink")、[弘义阁](../Page/弘义阁.md "wikilink")、[体仁阁则为单檐庑殿顶](../Page/体仁阁.md "wikilink")。

## 历史

庑殿顶出现的很早，甚至早于[歇山顶](../Page/歇山顶.md "wikilink")。在[殷商的](../Page/商朝.md "wikilink")[甲骨文](../Page/甲骨文.md "wikilink")、[周朝的](../Page/周朝.md "wikilink")[青铜器](../Page/青铜器.md "wikilink")、[汉朝画像石与](../Page/汉朝.md "wikilink")[明器](../Page/明器.md "wikilink")、[北朝石窟中都可发现庑殿顶](../Page/北朝.md "wikilink")。汉朝的[阙楼和](../Page/阙楼.md "wikilink")[唐朝的](../Page/唐朝.md "wikilink")[佛光寺大殿是现存最早的庑殿顶建筑](../Page/佛光寺_\(五台\)#大殿.md "wikilink")。

后来庑殿顶成为中国古代建筑中最高等级的屋顶形式，常用於皇家建築以及大型寺院、宮觀。清朝时期，庑殿顶只能用于皇家和[孔子的宫殿](../Page/孔子.md "wikilink")，其中又以重檐庑殿顶最为尊贵。但[閩東沿海地區的民居因為防風需要也會用平緩的廡殿頂](../Page/閩東.md "wikilink")。

Image:Fengguo_Temple_2.JPG|遼代[奉國寺采用單檐廡殿頂](../Page/奉國寺.md "wikilink")
Image:Datong Shanhua Si 2013.08.29
12-28-45.jpg|金代[善化寺三聖殿採用單檐廡殿頂](../Page/善化寺.md "wikilink")
Image:Beiyue_Temple_8.jpg|元代[北嶽廟採用重檐廡殿頂](../Page/北嶽廟.md "wikilink")
<File:Suzhou> Wenmiao 2015.04.23 15-51-48.jpg
|[蘇州文廟大成殿採用重檐庑殿顶](../Page/蘇州文廟.md "wikilink")
<File:Quanzhou> Wenmiao
20120229-14.jpg|[泉州府文廟大成殿採用重檐單層庑殿顶](../Page/泉州府文廟.md "wikilink")
图像:罗布林卡 luobulinka （dalai lama summer palace） -
panoramio.jpg|[罗布林卡的康松司伦有庑殿屋顶](../Page/罗布林卡.md "wikilink")

中國大陸以外的地方古建築，例如[日本的](../Page/日本.md "wikilink")[東大寺](../Page/東大寺.md "wikilink")[大佛殿](../Page/大佛殿.md "wikilink")、[正倉院](../Page/正倉院.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[景福宮的城樓](../Page/景福宮_\(朝鮮\).md "wikilink")。在日本，廡殿頂多見於[東日本](../Page/東日本.md "wikilink")，又有「東屋」的別稱。

在琉球與越南，歇山頂就是最高等級的官式建築，卽便牌坊也應用歇山頂，四坡頂衹見於民居建築中。琉球傳統住宅采用一種稱爲**別当型**的四坡頂，非常常見。越南則衹有山地少數民族民居（如哈尼族民居）及近代的越法合璧、越美合璧建築存在四坡頂。

此外還有仿古建築例如[台北](../Page/台北.md "wikilink")[國家戲劇院與](../Page/國家戲劇院.md "wikilink")[大直忠烈祠](../Page/大直忠烈祠.md "wikilink")，[香港仿唐建築](../Page/香港.md "wikilink")[志蓮淨苑的](../Page/志蓮淨苑.md "wikilink")[大雄殿等](../Page/大雄寶殿.md "wikilink")。

Image:Amarbayasgalant_Monastery_-_panoramio.jpg|[蒙古國](../Page/蒙古國.md "wikilink")[色楞格省](../Page/色楞格省.md "wikilink")[慶寧寺主殿從外側看類似](../Page/慶寧寺_\(蒙古國\).md "wikilink")[盝頂建築](../Page/盝頂.md "wikilink")，但實爲四棟廡殿頂組屋包住一棟歇山頂内閣
<File:Gyeonbokgung-Gate-Guards.jpg>|[韓國](../Page/韓國.md "wikilink")[景福宮的重檐庑殿顶](../Page/景福宮_\(朝鮮\).md "wikilink")[城樓](../Page/城樓.md "wikilink")
<File:Cổng> chùa Nam
Nhã.jpg|廡殿頂在越南等級不高，且極其罕見。位於越南南方原屬[下柬埔寨](../Page/下柬埔寨.md "wikilink")，在建立時該地區建築樣式已經受到法國影響。
<File:Shikinaen>
20040118-2.jpg|[琉球](../Page/琉球.md "wikilink")[識名園四坡頂御殿](../Page/識名園.md "wikilink")
<File:Nakamura> House
Kitanakagusuku01n3104.jpg|琉球[賀氏中村傢住宅中有別当型風格的組屋](../Page/中村家住宅_\(沖繩縣\).md "wikilink")
<File:Gangoji03n4592.jpg>|[日本](../Page/日本.md "wikilink")[元興寺極乐坊](../Page/元興寺.md "wikilink")
[File:Todaiji18s3200.jpg|日本](File:Todaiji18s3200.jpg%7C日本)[東大寺的大佛殿採用重檐庑殿顶](../Page/東大寺.md "wikilink")

## 脚注

[Category:中国传统屋顶](../Category/中国传统屋顶.md "wikilink")
[Category:東亞傳統屋頂](../Category/東亞傳統屋頂.md "wikilink")