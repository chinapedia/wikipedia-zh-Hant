**库仑**（）是[电量的单位](../Page/电量.md "wikilink")，符号为\(\mathrm{C}\)。若导线中载有1[安培的穩定](../Page/安培.md "wikilink")[電流](../Page/電流.md "wikilink")，则在1秒内通过导线横截面积的[电量为](../Page/电量.md "wikilink")1库仑。\[1\]\[2\]

库仑不是[國際單位制基本單位](../Page/國際單位制基本單位.md "wikilink")，而是[國際單位制導出單位](../Page/國際單位制導出單位.md "wikilink")。1库仑=1[安培](../Page/安培.md "wikilink")·秒（\(1\mathrm{C}=1\mathrm{A}\cdot\mathrm{s}\)）。一个[电子所带电荷量](../Page/电子.md "wikilink")\(e=-1.6021892\times10^{-19}\)库仑，即1库仑相当于\(6.24146\times10^{18}\)个电子（约\(1.04\times10^{-5}\mathrm{mol}\)电子）所带的电荷量。

\[1\ \mathrm{C} = 1\ \mathrm{A} \times 1\ \mathrm{s}\]

此单位是为纪念物理学家[查尔斯·奥古斯丁·库仑而命名的](../Page/查尔斯·奥古斯丁·库仑.md "wikilink")。

1库仑亦可定義為：

\[1\ \mathrm{C} = 1\ \mathrm{V} \times 1\ \mathrm{F}\]

  -

      -
        註 F = [Farad](../Page/法拉.md "wikilink")

## 参考文献

## 参见

  - [库仑定律](../Page/库仑定律.md "wikilink")
  - [库仑爆炸](../Page/库仑爆炸.md "wikilink")
  - [安培](../Page/安培.md "wikilink")
  - [电磁学](../Page/电磁学.md "wikilink")
  - [电容](../Page/电容.md "wikilink")
  - [电荷](../Page/电荷.md "wikilink")

<!-- end list -->

  - [法拉第常数](../Page/法拉第常数.md "wikilink")

{{-}}

[K](../Category/电磁学.md "wikilink") [K](../Category/电学单位.md "wikilink")
[K](../Category/电荷单位.md "wikilink")
[K](../Category/国际单位制导出单位.md "wikilink")

1.  物理.高中下册.理科班和实验班用.张大同
2.  物理奥林匹克竞赛教程.电磁学篇 程稼夫 中国科技大学出版社