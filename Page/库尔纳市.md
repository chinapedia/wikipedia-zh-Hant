**库尔纳**（[孟加拉语](../Page/孟加拉语.md "wikilink")：****）是[孟加拉国的第三大城市](../Page/孟加拉国.md "wikilink")，[库尔纳专区首府和](../Page/庫爾納專區.md "wikilink")[库尔纳县行政驻地](../Page/库尔纳县.md "wikilink")，2007年估计人口为140万。

该城位于孟加拉国首都[达卡之西南方约](../Page/达卡.md "wikilink")333千米，是主要的工商业中心。

## 參考文獻

[Category:孟加拉国城市](../Category/孟加拉国城市.md "wikilink")