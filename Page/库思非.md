<table>
<tbody>
<tr class="odd">
<td><p>{{Infobox Person</p></td>
<td><p>name = 库思非</p></td>
<td><p>image = <a href="https://zh.wikipedia.org/wiki/File:Carl_Frederick_Kupfer.jpg" title="fig:Carl_Frederick_Kupfer.jpg">Carl_Frederick_Kupfer.jpg</a></p></td>
<td><p>caption =</p></td>
<td><p>birth_date = 1852年6月8日</p></td>
<td><p>birth_place = <a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/图林根州.md" title="wikilink">图林根州</a><a href="../Page/格赖茨县.md" title="wikilink">格赖茨县</a></p></td>
<td><p>death_date = 1925年11月20日</p></td>
<td><p>death_place = <a href="../Page/中国.md" title="wikilink">中国</a><a href="../Page/汉口.md" title="wikilink">汉口</a></p></td>
<td><p>occupation = <a href="../Page/美以美会.md" title="wikilink">美以美会传教士</a><br />
<a href="../Page/九江同文中学.md" title="wikilink">九江同文中学校长</a></p></td>
<td><p>parents =</p>
<p><code> }}</code></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**库思非**（，，），[美以美会德裔年议会来华传教的代表性人物之一](../Page/美以美会.md "wikilink")。1852年，他出生于现[德国](../Page/德国.md "wikilink")[图林根州](../Page/图林根州.md "wikilink")[格赖茨县的](../Page/格赖茨县.md "wikilink")，幼年时随父母移民[美国](../Page/美国.md "wikilink")，在现[西弗吉尼亚州的](../Page/西弗吉尼亚州.md "wikilink")[惠灵长大](../Page/惠灵_\(西弗吉尼亚州\).md "wikilink")\[1\]。1881年，他毕业于面向德裔美国人的，与莉迪亚·克里尔（Lydia
Krill）结婚\[2\]。同年，他受美以美会差遣，来到[江西](../Page/江西.md "wikilink")[九江](../Page/九江.md "wikilink")，担任九江同文书院校长。他于1885年左右将学校迁至南门附近，即学校现址。1888年后，他又赴同属[美以美会华中年议会范围的](../Page/美以美会差会#华中年议会（CENTRAL_CHINA_CONFERENCE）.md "wikilink")[镇江](../Page/镇江.md "wikilink")、[南京等地做传教工作](../Page/南京.md "wikilink")。1896年他从[雪城大学获得博士学位](../Page/雪城大学.md "wikilink")\[3\]。

1901年，为纪念德裔卫理宗领袖，同文书院更名为南伟烈大学（William Nast
College，中文中也称同文大学），库思非再次出任学校校长。学校增设高等课程，开始时有两年大学课程，1905年时已具备完整的大学课程，但并未达到真正的大学水准。1918年，在[第一次世界大战的背景下](../Page/第一次世界大战.md "wikilink")，库思非被控有同情德国而不忠于美国的行为，教会命令其退休\[4\]，在南伟烈大学工作的其他德裔人士亦被迫离开。在学校管理层更迭之后，学校的高等教育被逐步减少，1920年时已无高等课程，但大学之名保留至1927年，之后，校名改为[同文中学](../Page/九江同文中学.md "wikilink")（William
Nast Academy）。

库思非1921年又返回中国，1925年在[汉口的一家医院去世](../Page/汉口.md "wikilink")，葬在九江\[5\]。

库思非在牯岭上拥有112号、113号、118号、159号4幢毗邻的别墅。1929年－1934年间，分别都卖给了中国人。

## 参考文献

[K](../Category/在华基督教传教士.md "wikilink")
[K](../Category/美國傳教士.md "wikilink")
[K](../Category/美以美会在华传教士.md "wikilink")

1.

2.

3.
4.

5.