**應用心理學**的基本前提是運用[心理學的原則及理論來克服其他領域中實際的問題](../Page/心理學.md "wikilink")，例如[管理學](../Page/管理學.md "wikilink")、[產品設計](../Page/產品設計.md "wikilink")、[人因工程學](../Page/人因工程學.md "wikilink")、[營養學](../Page/營養學.md "wikilink")、[法律及](../Page/法律.md "wikilink")[臨床藥物](../Page/醫學.md "wikilink").
應用心理學不但包含以下領域:[工業與組織心理學](../Page/工業與組織心理學.md "wikilink")、[法庭心理學](../Page/法庭.md "wikilink"),
[人因工程學](../Page/人因工程學.md "wikilink")，也包含了[教育心理學](../Page/教育心理學.md "wikilink")、[運動心理學及](../Page/運動.md "wikilink")[社群意識](../Page/社群意識.md "wikilink")。另外，一些心理學的專門領域也有應用心理學的分支(例如:應用社會心理學，應用認知心理學)。

應用心理學的奠基者，[雨果·明斯特堡](../Page/Hugo_Münsterberg.md "wikilink")。和1800年代晚期許多胸懷大志的心理學家一樣，這位德國學者前往美國，起初是為了學習哲學。明斯特堡對心理學的領域有多方興趣，例如:
[目的心理學](../Page/目的心理學.md "wikilink")、[社會心理學以及法庭心理學](../Page/社會心理學.md "wikilink")。1907年，他撰寫了有關證詞的法律觀點、自白以及法庭程序的一些雜誌文章，最後集結成書
- On the Witness
Stand。接下來幾年，應用心理學派進入了哈佛心理實驗室。九年內，他以英文撰寫了八本書，將心理學應用於教育、工業效率、管理以及教學。雨果·明斯特堡的貢獻使他成為應用心理學的創始者和奠基人。

## 工業與組織心理學

[工業與組織心理學主要是分別員工](../Page/工業與組織心理學.md "wikilink")、顧客及消費者的心理層級，包含招聘、從眾多應徵者中選用員工，並包含對員工的訓練、績效評價、工作滿意度、[工作](../Page/工作.md "wikilink")[行為](../Page/行為.md "wikilink")、工作壓力及[管理](../Page/管理.md "wikilink")。

## 法庭及法律心理學

法庭心理學及[法律心理學均是對法律問題及其議題有關的心理學方法與原則的應用](../Page/法律.md "wikilink")。典型地來說，法庭心理學與對個人的臨床分析及特定心理法律問題的評估相關。法律心理學則是在法律問題或議題上運用心理學的原則或方法相關。除了應用面外，法律心理學亦包含法律對人類心理過程行為的理論研究或經驗研究。

法庭心理學的使用可以回溯到1800年代晚期，兩名內科醫師從調查開膛手傑克的凶案現場，解析兇手的人格特質並運用線索進入他的內心世界。十年後，這種調查方式被稱作人格側寫，一名探員運用其他犯人的人格側寫來調查「山姆之子」行為的原由，從人格側寫衍生出的家庭、教育以及行為有關的問題。這是心理學的另一種形式，也是讓FBI探員預防謀殺案的發生並找出兇手的方法。利用此心理學研究，我們可以將兇手分成兩種類型:非組織型，代表犯罪者並非有計畫的犯案；及組織型，犯罪已有預謀。人格在剖析兇手的心理中扮演了重要的角色。

## 人因學

[人因工程學是研究](../Page/人因工程學.md "wikilink")**認知**及**心理過程**如何影響人與環境中物件的相互作用。研究人因學的目標是為了了解人類心理過程和行為的極限與偏差。

## 其他領域

  - [社區心理學](../Page/社區心理學.md "wikilink")
  - [臨床心理學](../Page/臨床心理學.md "wikilink")
  - [諮商心理學](../Page/輔導.md "wikilink")
  - [生態心理學](../Page/生態心理學.md "wikilink")
  - [教育心理學](../Page/教育心理學.md "wikilink")
  - [環境心理學](../Page/環境心理學.md "wikilink")
  - [健康心理學](../Page/健康心理學.md "wikilink")
  - [運動心理學](../Page/運動心理學.md "wikilink")
  - [學校心理學](../Page/學校心理學.md "wikilink")
  - [交通心理學](../Page/交通心理學.md "wikilink")
  - [工業與組織心理學](../Page/工業與組織心理學.md "wikilink")

## 其他

  - [人因工程學](../Page/人因工程學.md "wikilink")
  - [Information design](../Page/Information_design.md "wikilink")
  - [Knowledge
    visualization](../Page/Knowledge_visualization.md "wikilink")
  - [交通心理學](../Page/交通心理學.md "wikilink")
  - [校園輔導老師](../Page/校園輔導老師.md "wikilink")
  - [教育心理學](../Page/教育心理學.md "wikilink")
  - [School psychologist](../Page/School_psychologist.md "wikilink")

[Category:應用心理學](../Category/應用心理學.md "wikilink")
[Category:心理學](../Category/心理學.md "wikilink")