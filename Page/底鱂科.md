**底鱂科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯉齒目的其中一](../Page/鯉齒目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分類

**底鱂科**下分4個屬，如下：

### 小孔鱂屬(*Adinia*)

  - [異域小孔鱂](../Page/異域小孔鱂.md "wikilink")(*Adinia xenica*)

### 底鱂屬(*Fundulus*)

  - [白線底鱂](../Page/白線底鱂.md "wikilink")(*Fundulus albolineatus*)
  - [百慕達底鱂](../Page/百慕達底鱂.md "wikilink")(*Fundulus bermudae*)
  - [阿拉巴馬底鱂](../Page/阿拉巴馬底鱂.md "wikilink")(*Fundulus bifax*)
  - [布氏底鱂](../Page/布氏底鱂.md "wikilink")(*Fundulus blairae*)
  - [北方底鱂](../Page/北方底鱂.md "wikilink")(*Fundulus catenatus*)
  - [金色底鱂](../Page/金色底鱂.md "wikilink")(*Fundulus chrysotus*)
  - [環帶底鱂](../Page/環帶底鱂.md "wikilink")(*Fundulus cingulatus*)
  - [沼澤底鱂](../Page/沼澤底鱂.md "wikilink")(*Fundulus confluentus*)
  - [秀體底鱂](../Page/秀體底鱂.md "wikilink")(*Fundulus diaphanus diaphanus*)
  - [月尾底鱂](../Page/月尾底鱂.md "wikilink")(*Fundulus diaphanus menona*)
  - [異底鱂](../Page/異底鱂.md "wikilink")(*Fundulus dispar*)
  - [東部底鱂](../Page/東部底鱂.md "wikilink")(*Fundulus escambiae*)
  - [寬底鱂](../Page/寬底鱂.md "wikilink")(*Fundulus euryzonus*)
  - [大底鱂](../Page/大底鱂.md "wikilink")(*Fundulus grandis*)
  - [偉底鱂](../Page/偉底鱂.md "wikilink")(*Fundulus grandissimus*)
  - [加拿大底鱂](../Page/加拿大底鱂.md "wikilink")(*Fundulus heteroclitus*)
  - [金氏底鱂](../Page/金氏底鱂.md "wikilink")(*Fundulus jenkinsi*)
  - [柔底鱂](../Page/柔底鱂.md "wikilink")(*Fundulus jullisia*)
  - [平原底鱂](../Page/平原底鱂.md "wikilink")(*Fundulus kansae*)
  - [側邊底鱂](../Page/側邊底鱂.md "wikilink")(*Fundulus lima*)
  - [線紋底鱂](../Page/線紋底鱂.md "wikilink")(*Fundulus lineolatus*)
  - [斑鰭底鱂](../Page/斑鰭底鱂.md "wikilink")(*Fundulus luciae*)
  - [條帶底鱂](../Page/條帶底鱂.md "wikilink")(*Fundulus majalis*)
  - [黑紋底鱂](../Page/黑紋底鱂.md "wikilink")(*Fundulus notatus*)
  - [諾特氏底鱂](../Page/諾特氏底鱂.md "wikilink")(*Fundulus nottii*)
  - [黑點底鱂](../Page/黑點底鱂.md "wikilink")(*Fundulus olivaceus*)
  - [小翼底鱂](../Page/小翼底鱂.md "wikilink")(*Fundulus parvipinnis*)
  - [瑪瑙底鱂](../Page/瑪瑙底鱂.md "wikilink")(*Fundulus persemilis*)
  - [菲氏底鱂](../Page/菲氏底鱂.md "wikilink")(*Fundulus philpisteri*)
  - [海灣底鱂](../Page/海灣底鱂.md "wikilink")(*Fundulus pulvereus*)
  - [拉氏底鱂](../Page/拉氏底鱂.md "wikilink")(*Fundulus rathbuni*)
  - [殘嘴底鱂](../Page/殘嘴底鱂.md "wikilink")(*Fundulus relictus*)
  - [紅額底鱂](../Page/紅額底鱂.md "wikilink")(*Fundulus rubrifrons*)
  - [寶蓮底鱂](../Page/寶蓮底鱂.md "wikilink")(*Fundulus saguanus*)
  - [半帶底鱂](../Page/半帶底鱂.md "wikilink")(*Fundulus sciadicus*)
  - [小口底鱂](../Page/小口底鱂.md "wikilink")(*Fundulus seminolis*)
  - [長吻底鱂](../Page/長吻底鱂.md "wikilink")(*Fundulus similis*)
  - [星斑底鱂](../Page/星斑底鱂.md "wikilink")(*Fundulus stellifer*)
  - [瓦卡馬底鱂](../Page/瓦卡馬底鱂.md "wikilink")(*Fundulus waccamensis*)
  - [斑馬底鱂](../Page/斑馬底鱂.md "wikilink")(*Fundulus zebrinus*)

### 小眼底鱂屬(*Leptolucania*)

  - [小眼底鱂](../Page/小眼底鱂.md "wikilink")(*Leptolucania ommata*)

### 盧氏鱂屬(*Lucania*)

  - [藍鰭盧氏鱂](../Page/藍鰭盧氏鱂.md "wikilink")(*Lucania goodei*)
  - [中間盧氏鱂](../Page/中間盧氏鱂.md "wikilink")(*Lucania interioris*)
  - [雨點盧氏鱂](../Page/雨點盧氏鱂.md "wikilink")(*Lucania parva*)

## 參考資料庫

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/底鱂科.md "wikilink")