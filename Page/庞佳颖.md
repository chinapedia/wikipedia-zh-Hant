**庞佳颖**，[上海人](../Page/上海.md "wikilink")，[中国](../Page/中国.md "wikilink")[游泳](../Page/游泳.md "wikilink")[运动员](../Page/运动员.md "wikilink")。

## 生平

庞佳颖1991年入上海闸北区少体校，教练是张艳君。1996年进入上海市体育运动学校，教练是周振怡。1998年进入上海市体育运动技术学院，教练是潘佳章。2002年入选国家游泳队，教练是潘佳章。

庞佳颖第一次參加奧運會是在[2004年的雅典奥运会](../Page/2004年夏季奥林匹克运动会.md "wikilink")。她在女子4×200米自由泳接力比赛中和[徐妍玮](../Page/徐妍玮.md "wikilink")、[杨雨](../Page/杨雨.md "wikilink")、[朱颖文合作夺得银牌](../Page/朱颖文.md "wikilink")。\[1\]

[2006年多哈亚运会](../Page/2006年亚洲运动会.md "wikilink")，庞佳颖一共夺得四枚金牌，包括游泳女子200米自由泳、4×100米自由泳接力、4×200米自由泳接力和4×100米混合泳接力。

另外，龐佳穎亦在[2008北京奧運會的游泳比赛奪得一面銀牌和兩面銅牌](../Page/2008年夏季奥林匹克运动会.md "wikilink")，包括4×200米自由泳接力（与[杨雨](../Page/杨雨.md "wikilink")、[朱倩蔚](../Page/朱倩蔚.md "wikilink")、[谭淼](../Page/谭淼.md "wikilink")、[湯景之合作](../Page/湯景之.md "wikilink")）、女子200米自由泳、4×100米混合泳接力（与[趙菁](../Page/趙菁.md "wikilink")、[徐田龍子](../Page/徐田龍子.md "wikilink")、[孫曄](../Page/孫曄.md "wikilink")、[周雅菲合作](../Page/周雅菲.md "wikilink")），是中國代表團在該屆奧運會的其中一位最多獎牌的中國選手。在女子100米自由泳準決賽中，雖然取得最佳成績，但却因偷步而被取消資格。

2012年，庞佳颖代表[中国出戰](../Page/2012年夏季奥林匹克运动会中国代表团.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦舉行的](../Page/倫敦.md "wikilink")[奥林匹克运动会](../Page/2012年夏季奥林匹克运动会.md "wikilink")[游泳比賽](../Page/2012年夏季奧林匹克運動會游泳比賽.md "wikilink")[女子4×200米自由泳接力賽事](../Page/2012年夏季奧林匹克運動會女子4×200米自由泳接力比賽.md "wikilink")，并获得第四名。

## 參考資料

## 外部链接

  - [多哈亞運四科游泳冠军庞佳颖访谈实录](https://web.archive.org/web/20070310225947/http://swimming.sport.org.cn/home/gndt/2006-12-11/105611.html)

[category:2006年亚洲运动会金牌得主](../Page/category:2006年亚洲运动会金牌得主.md "wikilink")
[Jiaying](../Page/category:庞姓.md "wikilink")

[Category:上海籍游泳运动员](../Category/上海籍游泳运动员.md "wikilink")
[Category:中国奥运游泳运动员](../Category/中国奥运游泳运动员.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會游泳運動員](../Category/2004年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會游泳運動員](../Category/2008年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2012年夏季奧林匹克運動會游泳運動員](../Category/2012年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2006年亞洲運動會游泳運動員](../Category/2006年亞洲運動會游泳運動員.md "wikilink")
[Category:奥林匹克运动会游泳铜牌得主](../Category/奥林匹克运动会游泳铜牌得主.md "wikilink")
[Category:亞洲運動會游泳獎牌得主](../Category/亞洲運動會游泳獎牌得主.md "wikilink")
[Category:2006年亞洲運動會銀牌得主](../Category/2006年亞洲運動會銀牌得主.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:世界短道游泳錦標賽獎牌得主](../Category/世界短道游泳錦標賽獎牌得主.md "wikilink")
[Category:女子自由式游泳運動員](../Category/女子自由式游泳運動員.md "wikilink")

1.  [2004年夏季奥林匹克运动会女子4×200米自由泳接力比赛结果](http://sportsillustrated.cnn.com/olympics/2004/schedules/162043ByEvent.html)，于2008年2月14日查阅。