**座間市**（）是位於[日本](../Page/日本.md "wikilink")[神奈川縣中心的](../Page/神奈川縣.md "wikilink")[都市](../Page/都市.md "wikilink")。

## 概要

座間由[八王子街道的](../Page/八王子街道.md "wikilink")[宿場發展而成為聚落](../Page/宿場.md "wikilink")。[太平洋戰爭期間](../Page/太平洋戰爭.md "wikilink")，[日本軍在此設有](../Page/日本軍.md "wikilink")[高座海軍工廠和](../Page/高座海軍工廠.md "wikilink")[陸軍士官學校](../Page/日本陸軍士官學校.md "wikilink")，軍都的色彩強烈。戰後，[美國駐日陸軍總部](../Page/美國駐日陸軍.md "wikilink")[座間基地和](../Page/座間基地.md "wikilink")[日本陸上自衛隊的](../Page/日本陸上自衛隊.md "wikilink")[座間駐屯地位於本市和附近的](../Page/座間駐屯地.md "wikilink")[相模原市](../Page/相模原市.md "wikilink")。

## 交通

### 鐵路

  - [小田急電鐵](../Page/小田急電鐵.md "wikilink")

<!-- end list -->

  - [Odakyu_odawara_logo.svg](https://zh.wikipedia.org/wiki/File:Odakyu_odawara_logo.svg "fig:Odakyu_odawara_logo.svg")
    [小田原線](../Page/小田原線.md "wikilink")：[相武台前站](../Page/相武台前站.md "wikilink")
    - [座間站](../Page/座間站.md "wikilink")

<!-- end list -->

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [相模線](../Page/相模線.md "wikilink")：[入谷站](../Page/入谷站_\(神奈川縣\).md "wikilink")

<!-- end list -->

  - [相模鐵道](../Page/相模鐵道.md "wikilink")

<!-- end list -->

  - [本線](../Page/相鐵本線.md "wikilink")：通過市域南端但不設站。[相模野站的所在地不是](../Page/相模野站.md "wikilink")「座間市相模野」而是「[海老名市](../Page/海老名市.md "wikilink")」。
  - 除相模野站外，徒步還可以使用[柏台站](../Page/柏台站.md "wikilink")、[小田急相模原站](../Page/小田急相模原站.md "wikilink")、[相武台下站](../Page/相武台下站.md "wikilink")、[南林間站等](../Page/南林間站.md "wikilink")。

## 外部連結

[分類:座間市](../Page/分類:座間市.md "wikilink")

[Category:高座郡](../Category/高座郡.md "wikilink")