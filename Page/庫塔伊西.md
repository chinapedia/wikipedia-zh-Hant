**庫塔伊西**
（[格鲁吉亚语](../Page/格鲁吉亚语.md "wikilink")：）僅次於[第比利斯及](../Page/第比利斯.md "wikilink")[巴統](../Page/巴統.md "wikilink")，是[格魯吉亞第三大城市](../Page/格魯吉亞.md "wikilink")、[伊梅列季亞州首府](../Page/伊梅列季亞州.md "wikilink")。位於該國西部[里奧尼河畔](../Page/里奧尼河.md "wikilink")，距離首都[第比利斯](../Page/第比利斯.md "wikilink")211[公里](../Page/公里.md "wikilink")。面積70[平方公里](../Page/平方公里.md "wikilink")，2002年人口185,965人。

## 歷史

是[科爾基斯的首都](../Page/科爾基斯.md "wikilink")，也是[格魯吉亞王國的首都](../Page/格魯吉亞王國.md "wikilink")
（975－1122年）、伊梅列季亞王國首都。1810年併入[沙俄](../Page/沙俄.md "wikilink")。[巴格拉特大教堂和](../Page/巴格拉特大教堂.md "wikilink")[格拉特修道院於](../Page/格拉特修道院.md "wikilink")1994年被列入[世界文化遺產](../Page/世界文化遺產.md "wikilink")。

## 友好城市

  - [美国](../Page/美国.md "wikilink")[密苏里州哥伦比亚](../Page/哥伦比亚_\(密苏里州\).md "wikilink")

  - [阿塞拜疆](../Page/阿塞拜疆.md "wikilink")[占贾](../Page/占贾.md "wikilink")

  - [英国](../Page/英国.md "wikilink")[纽波特](../Page/纽波特.md "wikilink")

  - [西班牙](../Page/西班牙.md "wikilink")[维多利亚](../Page/维多利亚_\(西班牙\).md "wikilink")

  - [希腊](../Page/希腊.md "wikilink")[尼凯阿](../Page/尼凯阿_\(阿提卡大区\).md "wikilink")

  - [波兰](../Page/波兰.md "wikilink")[波兹南](../Page/波兹南.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[图拉](../Page/圖拉_\(圖拉州\).md "wikilink")

  - [保加利亚](../Page/保加利亚.md "wikilink")[普罗夫迪夫](../Page/普罗夫迪夫.md "wikilink")

  - [以色列](../Page/以色列.md "wikilink")[阿什凯隆](../Page/阿什凯隆.md "wikilink")

  - [伊朗](../Page/伊朗.md "wikilink")[拉什特](../Page/拉什特.md "wikilink")

  - [土耳其](../Page/土耳其.md "wikilink")[萨姆松](../Page/萨姆松.md "wikilink")

  - [土耳其](../Page/土耳其.md "wikilink")[卡尔斯](../Page/卡尔斯.md "wikilink")

  - [亚美尼亚](../Page/亚美尼亚.md "wikilink")[久姆里](../Page/久姆里.md "wikilink")

  - [中国](../Page/中国.md "wikilink")[天津](../Page/天津.md "wikilink")

  - [中国](../Page/中国.md "wikilink")[新华区](../Page/新华区.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[里昂](../Page/里昂.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[巴约讷](../Page/巴约讷.md "wikilink")

  - [乌克兰](../Page/乌克兰.md "wikilink")[顿涅茨克](../Page/顿涅茨克.md "wikilink")

  - [乌克兰](../Page/乌克兰.md "wikilink")[哈尔科夫](../Page/哈尔科夫.md "wikilink")

  - [乌克兰](../Page/乌克兰.md "wikilink")[利沃夫](../Page/利沃夫.md "wikilink")

[K](../Category/格鲁吉亚城市.md "wikilink")