[Skull_of_Kubanochoerus_gigas.JPG](https://zh.wikipedia.org/wiki/File:Skull_of_Kubanochoerus_gigas.JPG "fig:Skull_of_Kubanochoerus_gigas.JPG")
[Kubanochoerus_skeleton_at_the_Beijing_Museum_of_Natural_History.jpg](https://zh.wikipedia.org/wiki/File:Kubanochoerus_skeleton_at_the_Beijing_Museum_of_Natural_History.jpg "fig:Kubanochoerus_skeleton_at_the_Beijing_Museum_of_Natural_History.jpg")\]\]
**庫斑豬**（*Kubanochoerus*）是一[屬大型及長角的](../Page/屬.md "wikilink")[豬](../Page/豬.md "wikilink")，生存於[中新世的](../Page/中新世.md "wikilink")[歐亞大陸與](../Page/歐亞大陸.md "wikilink")[非洲](../Page/非洲.md "wikilink")。其下最大的[物種是巨庫斑豬](../Page/物種.md "wikilink")，肩高達
 及重
。牠們的[頭部很特別](../Page/頭部.md "wikilink")，[眼睛上有細小的角](../Page/眼睛.md "wikilink")，雄性的前額更有大角。估計雄性會以這角來互相比拼。牠們在中新世末[滅絕](../Page/滅絕.md "wikilink")。

最初被認為是一種庫斑豬，名為*K.
massai*\[1\]。牠的[頭顱骨上有庫斑豬眼睛上的小角](../Page/頭顱骨.md "wikilink")，但卻沒有前額的角。最初以為牠是雌性的庫斑豬，但現已被重置在自己的屬中。

## 參考文獻

[Category:庫斑豬屬](../Category/庫斑豬屬.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")
[Category:中新世哺乳類](../Category/中新世哺乳類.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")
[Category:非洲史前哺乳動物](../Category/非洲史前哺乳動物.md "wikilink")

1.  Bishop LC (2010) Suoidea. In: Werdelin L, Sanders WJ, editors.
    Cenozoic Mammals of Africa. Berkeley: University of California
    Press. pp. 821–842.