**庭州**，又名**別失八里**（**Bechbaliq**或Beshbalik）、**别石把**、**亦力把力**，是[中亚古城](../Page/中亚.md "wikilink")，原为[车师后部王庭所在](../Page/车师.md "wikilink")，因为该王庭由五座城市组成，故称为**别失八里**，意为“五城之地”，城址位于今[吉木萨尔之北](../Page/吉木萨尔.md "wikilink")。

## 历史

[汉宣帝](../Page/汉宣帝.md "wikilink")[元康四年](../Page/元康.md "wikilink")（前62年），汉朝在当地设[戊己校尉](../Page/戊己校尉.md "wikilink")，[东汉时](../Page/东汉.md "wikilink")[耿恭在此](../Page/耿恭.md "wikilink")[屯垦](../Page/屯垦.md "wikilink")。3世纪后，中国陷入内乱，放弃该地区，[高车](../Page/高车.md "wikilink")、[柔然](../Page/柔然.md "wikilink")、[铁勒](../Page/铁勒.md "wikilink")、[突厥相继占领该地](../Page/突厥.md "wikilink")。

7世纪[唐朝重新统一中国之后](../Page/唐朝.md "wikilink")，[唐太宗于](../Page/唐太宗.md "wikilink")[贞观十四年](../Page/贞观_\(唐太宗\).md "wikilink")（640年）派遣[侯君集征讨](../Page/侯君集.md "wikilink")[高昌和](../Page/高昌.md "wikilink")[西突厥](../Page/西突厥.md "wikilink")。贞观二十年（646年），西突厥可汗[阿史那贺鲁内附唐朝](../Page/阿史那贺鲁.md "wikilink")，唐朝政府遂在当地设立**庭州**。[武则天](../Page/武则天.md "wikilink")[长安](../Page/长安_\(年号\).md "wikilink")（702年），又在庭州设立[北庭大都护府](../Page/北庭大都护府.md "wikilink")，统管整个[天山以北的](../Page/天山.md "wikilink")[西域地区](../Page/西域.md "wikilink")。[唐德宗](../Page/唐德宗.md "wikilink")[贞元六年](../Page/贞元_\(唐德宗\).md "wikilink")（790年），庭州被[吐蕃攻占](../Page/吐蕃.md "wikilink")，中国势力再一次退出当地。此后，[葛逻禄](../Page/葛逻禄.md "wikilink")、吐蕃和[回鹘对该地进行了长时间的争夺](../Page/回鹘.md "wikilink")，800年，回鹘最终获胜，在高昌建立[回鹘汗国](../Page/回鹘.md "wikilink")，以北庭为其陪都。[耶律大石建立](../Page/耶律大石.md "wikilink")[西辽之后](../Page/西辽.md "wikilink")，臣服了高昌汗国。

[元太祖十四年](../Page/元太祖.md "wikilink")（1219年）[耶律楚材随](../Page/耶律楚材.md "wikilink")[成吉思汗西征](../Page/成吉思汗.md "wikilink")，经过的別失八里、[轮台](../Page/轮台.md "wikilink")、[高昌](../Page/高昌.md "wikilink")、[-{于}-阗等地](../Page/于阗.md "wikilink")\[1\]，也即《[元史](../Page/元史.md "wikilink")》中，大将阿剌瓦而思攻破的回鹘城别失八里，轮台、高昌、-{于}-阗等地\[2\]。1251年蒙古在此设立了**别失八里等处[行尚书省](../Page/行省.md "wikilink")**。元宪宗[蒙哥死后](../Page/蒙哥.md "wikilink")，[阿里不哥一度以此处为基地之一与其兄](../Page/阿里不哥.md "wikilink")[忽必烈争夺蒙古大汗之位](../Page/忽必烈.md "wikilink")。[元朝](../Page/元朝.md "wikilink")[至元元年](../Page/至元.md "wikilink")（1264年）阿里不哥投降，该地遂为中央政府控制。但是至元十三年（1276年）[昔里吉发动叛乱](../Page/昔里吉.md "wikilink")，别失八里行省大部地区被窝阔台后王[海都占领](../Page/海都.md "wikilink")。元朝平定[南宋残部的反抗后](../Page/南宋.md "wikilink")，于至元二十年（1283年）在别失八里设[宣慰司](../Page/宣慰司.md "wikilink")，派[万户](../Page/万户.md "wikilink")[綦公直为宣慰使](../Page/綦公直.md "wikilink")，率大批新附军进驻实行[屯田](../Page/屯田.md "wikilink")\[3\]。然而到了至元二十三年（1286年），[察合台汗国后王](../Page/察合台汗国.md "wikilink")[笃哇占领该地](../Page/笃哇.md "wikilink")，[元贞二年](../Page/元贞.md "wikilink")（1295年），元朝再次设立北庭都元帅府。此后经过长时间的争夺，确立了察合台汗国对当地的统治。

[明](../Page/明.md "wikilink")[洪武二十三年](../Page/洪武.md "wikilink")（1390年），察合台后王（即[东察合台汗国](../Page/东察合台汗国.md "wikilink")）[黑的儿火者遣使贡方物](../Page/黑的儿火者.md "wikilink")。[永乐四年](../Page/永乐_\(明成祖\).md "wikilink")（1406年），后王[沙迷查干遣使贡玉璞等方物](../Page/沙迷查干.md "wikilink")。永樂七年，[明成祖封](../Page/明成祖.md "wikilink")[瓦剌](../Page/瓦剌.md "wikilink")[馬哈木為順寧王](../Page/馬哈木.md "wikilink")。永乐十一年（1413年），吏部员外郎[陈诚出使西域](../Page/陈诚_\(明朝\).md "wikilink")，曾访问别失八里\[4\]。

1417年察合台[歪思汗弑杀叔叔别失八里国王](../Page/歪思汗.md "wikilink")[纳黑失只罕](../Page/纳黑失只罕.md "wikilink")，自立为王，号[亦力把力](../Page/亦力把力.md "wikilink")。[宣德二年](../Page/宣德.md "wikilink")（1427年），歪思遣使入贡，[明宣宗赐歪思金刀甲胄](../Page/明宣宗.md "wikilink")。[正统后亦力把力朝贡不绝](../Page/正统_\(年号\).md "wikilink")\[5\]。

[清代光绪二十八年](../Page/清代.md "wikilink")（1902年）设孚远县。目前新疆[昌吉州以庭州为本区的代称](../Page/昌吉州.md "wikilink")。

## 注释

## 参考文献

[Category:新疆古地名](../Category/新疆古地名.md "wikilink")
[Category:西域](../Category/西域.md "wikilink")
[Category:昌吉](../Category/昌吉.md "wikilink")

1.  耶律楚材《[西游录](../Page/西游录.md "wikilink")》
2.  元史列传第十
3.  （明）严从简著 《[殊域周咨录](../Page/殊域周咨录.md "wikilink")·亦力把力》余思黎点校 中华书局 ISBN
    71010006078
4.  陈诚著《[西域番国志](../Page/西域番国志.md "wikilink")·别失八里》 周连宽校注 2000 中华书局 ISBN
    7-101-02058-5/K
5.  （明）罗曰褧 《[咸宾录](../Page/咸宾录.md "wikilink")·亦力把力》