《**庭院深深**》為[台灣作家](../Page/台灣.md "wikilink")[瓊瑤於](../Page/瓊瑤.md "wikilink")1969年所出版的[言情小說](../Page/言情小說.md "wikilink")，此作並曾多次被改編為[電影與](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")。

## 概要

本作品無論在原著[小說及改編](../Page/小說.md "wikilink")[電影](../Page/電影.md "wikilink")，都得到相當的成功，這本書意味瓊瑤小說進入了一個更深的階段。書名是出自於[宋朝著名詞人](../Page/宋朝.md "wikilink")[歐陽修的](../Page/歐陽修.md "wikilink")《[蝶戀花](../Page/蝶戀花.md "wikilink")》裡的其中一句：「庭院深深深幾許，楊柳堆堙，簾幕無重數。」意為暗示女主角章含煙的孤身獨處，且有心事深沉、怨恨莫訴之感，這對於故事中章含煙的心情可謂貼切非常。並且也是瓊瑤小說的一種新的嘗試，本書承接了瓊瑤前作《彩雲飛》的結構，由兩段大章節「廢墟之魂」及「灰姑娘」所組成，然而它的風格有別於瓊瑤之前的作品，使全書的劇情發展充滿懸念。

## 故事大綱

### 第一部「廢墟之魂」

1939年。故事由女主角方絲縈在「含煙山莊」廢墟中，與男主角柏霈文的巧遇揭開了序幕。而後擔任小學老師的方絲縈受柏霈文所邀，成為他女兒的家教並住進了柏家。前半部中不單著力描寫柏霈文前妻章含煙的鬼魂，以及「含煙山莊」的鬧鬼傳聞，而且著力描寫瞎眼的柏霈文，使得上篇充滿著神秘感，在女主角方絲縈那種既陌生又好像充滿了回憶的酸楚感覺中，讀者不禁充滿了疑問，甚至於懷疑方絲縈就是含煙，而正是這一種懸疑，吸引了讀者不斷地追索下去。

### 第二部「灰姑娘」

方絲縈的真實身份終於掀開，原來她就是當年被視為已死的章含煙，但為什麼她要離開柏霈文，而且還改名換姓？故事自此引出了章含煙與柏霈文舊日的愛情故事，以及後來的「章含煙之死」，第一部「廢墟之魂」中所鋪陳的懸念，在這時才真相大白。

### 第三部「暴風雨後」

方絲縈與跟柏霈文破鏡重圓，就成了作品中的第二個高潮。

事實上《庭院深深》一書的確是充滿引誘力，它營造懸念能夠給引讀者不斷的深究下去，事實上在瓊瑤為數眾多的作品中，《庭院深深》確實是其中一部極具吸引力之作，於1969年所寫出的這部作品，意味著瓊瑤小說在形式上有了更新的突破。

## 改編作品

### 台灣版電影

，1971年，導演[宋存壽](../Page/宋存壽.md "wikilink")，[楊群](../Page/楊群.md "wikilink")（飾柏霈文）和[歸亞蕾](../Page/歸亞蕾.md "wikilink")（飾章含煙）主演。\[1\]

#### 獲獎

  - [第9屆金馬獎](../Page/第9屆金馬獎.md "wikilink")

|- | rowspan="2"|

<center>

1971 |[王戎](../Page/王戎_\(演員\).md "wikilink")
|[最佳男配角](../Page/金馬獎最佳男配角.md "wikilink") | |-
|庭院深深 |優等劇情片 |

#### 外部連結

  -
  -
  -
### 大陸版電影

1989年，導演[史蜀君](../Page/史蜀君.md "wikilink")，演員有[金夢](../Page/金夢.md "wikilink")、[尤勇](../Page/尤勇.md "wikilink")、[宋佳](../Page/宋佳.md "wikilink")、[焦晃等](../Page/焦晃.md "wikilink")。\[2\]

### 電視劇

  - 台灣[台視](../Page/台視.md "wikilink")，1974年8月5日～16日，週一至週五21:25-21:50（全10集），[吳桓](../Page/吳桓_\(臺灣\).md "wikilink")（飾柏霈文）、[華真真](../Page/華真真.md "wikilink")（飾章含煙）主演。
  - 台灣[華視](../Page/華視.md "wikilink")，1987年8點（全40集），[劉立立導演](../Page/劉立立.md "wikilink")，[秦漢](../Page/秦漢_\(演員\).md "wikilink")、[劉雪華](../Page/劉雪華.md "wikilink")、[谷音](../Page/谷音.md "wikilink")、[徐乃麟](../Page/徐乃麟.md "wikilink")、[趙永馨](../Page/趙永馨.md "wikilink")、[李麗鳳](../Page/李麗鳳.md "wikilink")、[朱慧珍](../Page/朱慧珍.md "wikilink")、[李天柱](../Page/李天柱.md "wikilink")、[李又麟](../Page/李又麟.md "wikilink")、[林在培及](../Page/林在培.md "wikilink")[范鴻軒](../Page/范鴻軒.md "wikilink")、[余晨華主演](../Page/余晨華.md "wikilink")。\[3\]

## 參考資料

[1](../Page/category:1970年代台灣電影作品.md "wikilink")
[category:台灣愛情片](../Page/category:台灣愛情片.md "wikilink")
[T庭](../Page/category:中國電影作品.md "wikilink")

[T庭](../Category/改編成電影的台灣小說.md "wikilink")
[T庭](../Category/1987年台灣電視劇集.md "wikilink")
[T庭](../Category/華視電視劇.md "wikilink")
[Category:瓊瑤小說改編電視劇](../Category/瓊瑤小說改編電視劇.md "wikilink")
[Category:瓊瑤小說改編電影](../Category/瓊瑤小說改編電影.md "wikilink")
[Category:瓊瑤愛情小說](../Category/瓊瑤愛情小說.md "wikilink")

1.  [庭院深深(1971)](http://www.dianying.com/ft/title/tys1971).中文電影資料庫
2.  [庭院深深 (1989)](http://www.dianying.com/ft/title/tys1989).中文電影資料庫
3.  [庭院深深 (1987)](http://www.dianying.com/ft/title/tys1987).中文電影資料庫