**康保县**是[中国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[张家口市下辖的一个县](../Page/张家口市.md "wikilink")，位於张家口市西北部，邻接[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")，俗称“**坝上高原**”。

1923年，由[张北和](../Page/张北.md "wikilink")[商都两县析置康保招垦设治局](../Page/商都.md "wikilink")。1925年，改為康保县，以境内的[康保泊得名](../Page/康保泊.md "wikilink")。畜牧业发达，盛产[马](../Page/马.md "wikilink")、[牛](../Page/牛.md "wikilink")、[骡和](../Page/骡.md "wikilink")[羊等](../Page/羊.md "wikilink")。农产有[莜麦](../Page/莜麦.md "wikilink")、[马铃薯和](../Page/马铃薯.md "wikilink")[小麦等](../Page/小麦.md "wikilink")。[工业有肉类和皮毛加工](../Page/工业.md "wikilink")、煤炭、建材及酿酒等。名胜有[金界壕及](../Page/金界壕.md "wikilink")[南天门峡等](../Page/南天门峡.md "wikilink")。

## 行政区划

下辖7个[镇](../Page/行政建制镇.md "wikilink")、8个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [中国张家口康保](http://www.zjkkb.gov.cn/)

[康保县](../Page/category:康保县.md "wikilink")
[县](../Page/category:张家口区县.md "wikilink")
[张家口](../Page/category:河北省县份.md "wikilink")
[冀](../Page/category:国家级贫困县.md "wikilink")