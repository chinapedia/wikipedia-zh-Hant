**康姓**是[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》排第88位，[中国大陆第](../Page/中国大陆.md "wikilink")92大姓（2006年统计）。

## 現代人物

  - [康寧祥](../Page/康寧祥.md "wikilink")：現任[總統府資政](../Page/總統府資政.md "wikilink")，曾任[臺北市議員](../Page/臺北市議員.md "wikilink")、[立法委員](../Page/立法委員.md "wikilink")、[國大代表](../Page/國大代表.md "wikilink")、[監察委員](../Page/監察委員.md "wikilink")、[國防部副部長等](../Page/國防部.md "wikilink")。
  - [康世儒](../Page/康世儒.md "wikilink")：第7屆中華民國[立法委員](../Page/立法委員.md "wikilink")，曾任[苗栗縣](../Page/苗栗縣.md "wikilink")[竹南鎮第十四屆鎮長](../Page/竹南鎮.md "wikilink")、苗栗縣議員、竹南鎮民代表。
  - [康晋榮](../Page/康晋榮.md "wikilink")(康康)：台灣節目主持人和歌手。
  - [康禎庭](../Page/康禎庭.md "wikilink")：[第四屆《超級星光大道](../Page/超級星光大道_\(第四屆\).md "wikilink")》歌唱選秀節目中奪得殿軍。
  - [康思婷](../Page/康思婷.md "wikilink")：[快樂星期天節目中校園歌喉戰歌唱選秀節目第二屆冠軍](../Page/快樂星期天.md "wikilink")。
  - [康日新](../Page/康日新.md "wikilink")：[中国核工业集团公司党组书记](../Page/中国核工业集团公司.md "wikilink")、总经理
  - [康華](../Page/康華.md "wikilink")：香港藝人
  - [康子妮](../Page/康子妮.md "wikilink")：香港藝人
  - [康瑟琪](../Page/康瑟琪.md "wikilink")：韓國女子組合[Red
    Velvet成員](../Page/Red_Velvet.md "wikilink")
  - [康美娜](../Page/康美娜.md "wikilink")：韓國女子組合[gu9udan成員](../Page/gu9udan.md "wikilink")
  - [康京和](../Page/康京和.md "wikilink")：韓國外交部长

## 歷史名人

  - [安祿山](../Page/安祿山.md "wikilink")：[唐朝政治人物](../Page/唐朝.md "wikilink")，本姓康，因母改嫁突厥番官安延偃，遂改姓安姓。來自[粟特人的](../Page/粟特.md "wikilink")[昭武九姓](../Page/昭武九姓.md "wikilink")。
  - [康海](../Page/康海.md "wikilink")：[明朝作家](../Page/明朝.md "wikilink")，前七子之一，提倡復古擬古。
  - [康有為](../Page/康有為.md "wikilink")：[清末](../Page/清.md "wikilink")[民初政治家](../Page/民初.md "wikilink")、思想家、教育家，戊戌變法領袖之一。
  - [康廣仁](../Page/康廣仁.md "wikilink")：[戊戌變法領袖康有為胞弟](../Page/戊戌變法.md "wikilink")，「戊戌六君子」之一。
  - [康世恩](../Page/康世恩.md "wikilink")：中國政治人物。
  - [康克清](../Page/康克清.md "wikilink")：中國政治人物。

## 源流

  - 出自[姬姓](../Page/姬姓.md "wikilink")，[衛康叔本來封於康邑](../Page/衛康叔.md "wikilink")，[周公東征後](../Page/周公東征.md "wikilink")，遷徙至[衛國](../Page/衛國.md "wikilink")，子孫後以康為姓。
  - 出自[子姓](../Page/子姓.md "wikilink")，[宋康王子孫以康為姓](../Page/宋康王.md "wikilink")。
  - 出自[羋姓](../Page/羋姓.md "wikilink")，[楚康王子孫以康為姓](../Page/楚康王.md "wikilink")。
  - 出自[匡姓](../Page/匡姓.md "wikilink")，宋太祖名匡胤，爲避諱，改匡姓爲[主姓](../Page/主姓.md "wikilink")。政和間，又以主非人臣姓氏，故又改主姓爲康姓。\[1\]
  - 出自[滿族](../Page/滿族.md "wikilink")、[蒙古族](../Page/蒙古族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[苗族](../Page/苗族.md "wikilink")、[瑶族等](../Page/瑶族.md "wikilink")[少数民族](../Page/少数民族.md "wikilink")[漢化改氏](../Page/漢化.md "wikilink")。
  - 来自中亚的[昭武九姓之一](../Page/昭武九姓.md "wikilink")。
  - 臺灣康姓以台南市新化區為大宗，有大批居民姓康。

## 参考

<references />

[\*](../Category/康姓.md "wikilink") [K康](../Category/漢字姓氏.md "wikilink")
[康](../Category/朝鮮語姓氏.md "wikilink")

1.