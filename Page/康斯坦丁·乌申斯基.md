[Ushinsky.jpg](https://zh.wikipedia.org/wiki/File:Ushinsky.jpg "fig:Ushinsky.jpg")
**康斯坦丁·乌申斯基**（，-）是一位[俄国教育家](../Page/俄国.md "wikilink")，俄国科学[教育学的创建者](../Page/教育学.md "wikilink")\[1\]
\[2\].

## 生平

，康斯坦丁·乌申斯基出生在[图拉市的一个退休陆军](../Page/图拉.md "wikilink")[中校D](../Page/中校.md "wikilink").K.
乌申斯基\[3\]的家庭。不久他们移居到乌克兰诺夫哥罗德-谢维尔斯克城附近，他父亲在那里担任uyezd judge
\[4\]。1840年，乌申斯基进入[莫斯科大学法律系](../Page/莫斯科大学.md "wikilink")，1844年毕业\[5\]\[6\]。1846年-1849年担任[雅罗斯拉夫尔高等法政学校](../Page/雅罗斯拉夫尔.md "wikilink")
教授，但后来由于自由主义思想而被迫离开\[7\]\[8\]。

乌申斯基失业以后，依靠为杂志Sovremennik和Biblioteka dlya
Chteniya写稿为生。一年半以后，乌申斯基在内务部宗教事务司找到一个科长助理的职位，虽然这个职业让他非常厌烦\[9\]。

1854年，乌申斯基成为加特钦斯克孤儿院的教师，1855年-1859年担任孤儿院学监\[10\]。在那里，他在2个已有20多年无人动过的书柜中发现了[裴斯泰洛齐](../Page/裴斯泰洛齐.md "wikilink")

，康斯坦丁·乌申斯基在[敖德萨去世](../Page/敖德萨.md "wikilink")，安葬在[基辅](../Page/基辅.md "wikilink")\[11\]。

## 著作

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Children Tales by Konstantin
    Ushiksny](http://peskar.net.ru/index_a.php?id_sst=176&&id_sec=22&&id_cat=7)


[Category:俄罗斯教育家](../Category/俄罗斯教育家.md "wikilink")
[Category:1824年出生](../Category/1824年出生.md "wikilink")
[Category:1871年逝世](../Category/1871年逝世.md "wikilink")

1.  [*Ushinsky* article](http://dic.academic.ru/dic.nsf/brokgauz/21746)
    in [Brockhaus and Efron Encyclopedic
    Dictionary](../Page/Brockhaus_and_Efron_Encyclopedic_Dictionary.md "wikilink")


2.  [*Ushinsky*
    article](http://www.cultinfo.ru/fulltext/1/001/008/114/995.htm) by
    [Eduard Dneprov](../Page/Eduard_Dneprov.md "wikilink") in *[Great
    Soviet
    Encyclopedia](../Page/Great_Soviet_Encyclopedia.md "wikilink")*

3.  [Konstantin
    Ushinsky](http://school76.yar.ru/menu/work/virtualnew/zam_lyudi.htm)


4.  [*The teacher of Russian
    teachers*](http://www.mifp.ru/pedagogika/Ibk/5/ushin.htm)  official
    site of [Moscow University of Industry and
    Finance](../Page/Moscow_University_of_Industry_and_Finance.md "wikilink")

5.
6.
7.
8.
9.
10.
11.