**康明斯**（Cummins Inc.
），[臺灣鐵路管理局譯為](../Page/臺灣鐵路管理局.md "wikilink")**固敏式**，是[美國一家](../Page/美國.md "wikilink")[柴油引擎及](../Page/柴油引擎.md "wikilink")[氣體燃料引擎的開發及生產商](../Page/氣體燃料引擎.md "wikilink")。

## 历史

於1919年成立，在多個國家均設有分公司。2012年1月1日[兰博文正式接替退休的](../Page/兰博文.md "wikilink")[苏志强担任康明斯董事长](../Page/苏志强.md "wikilink")。\[1\]

## 引擎產品

  - A系列
  - B系列 / ISB - 5.9L
  - C系列 / ISC - 8.3L
  - L系列 / ISL - 8.9L : 在[亞歷山大丹尼士Enviro
    500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")（[歐盟四/五代](../Page/歐洲汽車尾氣排放標準.md "wikilink")）上使用
  - M系列 / ISM - 10.8L :
    在[丹尼士三叉戟三型](../Page/丹尼士三叉戟三型.md "wikilink")，[亞歷山大丹尼士Enviro
    500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")（歐盟三代）、[Gillig
    Phantom](https://en.m.wikipedia.org/wiki/Gillig_Phantom)等車輛上使用
  - X系列 / X - 12L & 15L
  - ISF - 3.8L
  - ISX - 12L
  - ISX - 15L

註：「IS」是Interactive System的縮寫

## 参考来源

## 外部連結

  - [康明斯總部](http://www.cummins.com)
  - [康明斯中国](http://www.cummins.com.cn)
  - [台灣康明斯](http://www.cummins.com.tw)

[康明斯](../Category/康明斯.md "wikilink")
[Category:总部在印第安纳州的跨国公司](../Category/总部在印第安纳州的跨国公司.md "wikilink")
[Category:美国汽车公司](../Category/美国汽车公司.md "wikilink")
[Category:汽車零件供應商](../Category/汽車零件供應商.md "wikilink")
[Category:發動機製造商](../Category/發動機製造商.md "wikilink")
[Category:1919年美國建立](../Category/1919年美國建立.md "wikilink")
[Category:1919年成立的公司](../Category/1919年成立的公司.md "wikilink")

1.  [康明斯董事长年底换届
    官兰博文2012年接任](http://auto.ifeng.com/news/parts/20110722/647206.shtml)，凤凰新闻。