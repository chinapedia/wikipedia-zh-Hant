[Mt._Parker_Lodge_(full_view_and_better_contrast).jpg](https://zh.wikipedia.org/wiki/File:Mt._Parker_Lodge_\(full_view_and_better_contrast\).jpg "fig:Mt._Parker_Lodge_(full_view_and_better_contrast).jpg")
 **康景花園**（**Mt. Parker
Lodge**），是一個在[港島東的](../Page/港島東.md "wikilink")[私人屋苑](../Page/私人屋苑.md "wikilink")，由[恆基兆業及](../Page/恆基兆業.md "wikilink")[恆隆地產發展](../Page/恆隆地產.md "wikilink")，由關吳黃建築師事務所設計，[怡高物業顧問管理](../Page/怡高物業顧問.md "wikilink")。康景花園位於[鰂魚涌](../Page/鰂魚涌.md "wikilink")[英皇道](../Page/英皇道.md "wikilink")1030號以南、[基利路](../Page/基利路.md "wikilink")[南豐新邨以西及](../Page/南豐新邨.md "wikilink")[大潭郊野公園以東的內街](../Page/大潭郊野公園.md "wikilink")，鄰近東面還有其他大型屋苑[太古城及](../Page/太古城.md "wikilink")[康怡花園等](../Page/康怡花園.md "wikilink")。康景花園在1989年建成，分A至E座，共有5幢，單位[建築面積約在](../Page/建築面積.md "wikilink")500至1000方呎，有[業主立案法團](../Page/業主立案法團.md "wikilink")。

## 歷史

康景花園地盤前身是太古三號鰂魚涌[水塘](../Page/水塘.md "wikilink")。

## 設施

  - [兒童遊樂場](../Page/兒童遊樂場.md "wikilink")
  - [游泳池](../Page/游泳池.md "wikilink")
  - [專線小巴](../Page/專線小巴.md "wikilink")

## 交通設施

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [港島線](../Page/港島線.md "wikilink")：[太古站](../Page/太古站.md "wikilink")

<!-- end list -->

  - [祐民街](../Page/祐民街.md "wikilink")

<!-- end list -->

  - 康柏徑

</div>

</div>

## 外部連線

  - [香港島東的康景花園地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=840270&cy=816064&zm=4&mx=839986&my=815851&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[Category:東區私人屋苑](../Category/東區私人屋苑.md "wikilink")
[Category:鰂魚涌](../Category/鰂魚涌.md "wikilink")
[Category:恒基兆業物業](../Category/恒基兆業物業.md "wikilink")
[Category:恒隆地產物業](../Category/恒隆地產物業.md "wikilink")