**康莊道**（）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[紅磡灣的一條主幹道](../Page/紅磡灣.md "wikilink")，大部份路段屬[1號幹線的一部份](../Page/1號幹線.md "wikilink")，全長約1[公里](../Page/公里.md "wikilink")。幹線部份即紅磡交匯處至紅磡海底隧道九龍入口一段，主要作為[紅磡海底隧道的引道](../Page/海底隧道_\(香港\).md "wikilink")，連接隧道入口與[公主道](../Page/公主道.md "wikilink")、[漆咸道北及](../Page/漆咸道北.md "wikilink")[漆咸道南](../Page/漆咸道南.md "wikilink")，為4線雙程分隔道路，車速限制每小時70公里；而非幹線部份則為2線單程北行（南行屬於梳士巴利道），與紅磡海底隧道收費廣場平行，連接[梳士巴利道與](../Page/梳士巴利道.md "wikilink")[科學館道交界](../Page/科學館道.md "wikilink")，車速限制為每小時50公里。

## 歷史

康莊道的前身為通往香港工業專門學院（今[香港理工大學](../Page/香港理工大學.md "wikilink")）的**工專通道**，建於1960年代，定線與康莊道相若,又稱為安發街，連接舊紅磡碼頭與漆咸道。為興建通往海底隧道的道路，工專通道及附近土地被填高，並於上面興建了康莊道。

## 現況

康莊道需要應付由三個方向（公主道、[東九龍走廊及](../Page/東九龍走廊.md "wikilink")[西九龍走廊](../Page/西九龍走廊.md "wikilink")）進入香港海底隧道的車流，但由於香港海底隧道的行車流量已經飽和，令車龍無法進入隧道而積聚在康莊道和隧道收費廣場外，使四線行車的康莊道由早到晚都異常擠塞，車輛的行車速度經常遠低於最高限速70公里/小時。

<File:Hong> Chong Road at night.jpg|康莊道夜景 <File:Hong> Chong Road view
2015.jpg|康莊道交通非常繁忙

## 沿路地點

  - [香港消防處總部大樓](../Page/香港消防處總部大樓.md "wikilink")
  - [尖東消防局](../Page/尖東消防局.md "wikilink")
  - [香港理工大學](../Page/香港理工大學.md "wikilink")

## 參考資料

<references />

[Category:紅磡灣](../Category/紅磡灣.md "wikilink")
[Category:香港1號幹線](../Category/香港1號幹線.md "wikilink")
[Category:香港主要幹道](../Category/香港主要幹道.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")