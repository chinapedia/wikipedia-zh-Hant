**廉江市**（[邮政式拼音](../Page/邮政式拼音.md "wikilink")：）是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[湛江市下辖的一个](../Page/湛江市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。位于广东省西南部，[雷州半岛北部](../Page/雷州半岛.md "wikilink")，北与[广西接壤](../Page/广西.md "wikilink")，东于广东省[化州市毗邻](../Page/化州市.md "wikilink")，濒临[北部湾](../Page/北部湾.md "wikilink")，地域总面积2835平方公里。

## 歷史

[唐高祖](../Page/唐高祖.md "wikilink")[武德](../Page/武德.md "wikilink")5年（622年）建石城縣，[唐玄宗](../Page/唐玄宗.md "wikilink")[天宝元年](../Page/天宝.md "wikilink")（742年）改稱廉江縣。其後曾設羅州府，1915年復置廉江縣。1993年撤縣設市。

## 语言

廉江主要有白话（[粵語高陽片](../Page/粵語.md "wikilink")）、涯话（[客家](../Page/客家.md "wikilink")）、雷州话（[闽南语系](../Page/闽南语系.md "wikilink")）三大方言。

## 行政区划

现辖：

  - 3街道：[罗州街道](../Page/罗州街道.md "wikilink") ·
    [城南街道](../Page/城南街道.md "wikilink") ·
    [城北街道](../Page/城北街道.md "wikilink")
  - 18镇:[石城镇](../Page/石城镇.md "wikilink") ·
    [新民镇](../Page/新民镇.md "wikilink") ·
    [吉水镇](../Page/吉水镇.md "wikilink") ·
    [河唇镇](../Page/河唇镇.md "wikilink") ·
    [石角镇](../Page/石角镇.md "wikilink") ·
    [良垌镇](../Page/良垌镇.md "wikilink") ·
    [横山镇](../Page/横山镇.md "wikilink") ·
    [安铺镇](../Page/安铺镇.md "wikilink") ·
    [营仔镇](../Page/营仔镇.md "wikilink") ·
    [青平镇](../Page/青平镇.md "wikilink") ·
    [车板镇](../Page/车板镇.md "wikilink") ·
    [高桥镇](../Page/高桥镇.md "wikilink") ·
    [石岭镇](../Page/石岭镇.md "wikilink") ·
    [雅塘镇](../Page/雅塘镇.md "wikilink") ·
    [石颈镇](../Page/石颈镇.md "wikilink") ·
    [长山镇](../Page/长山镇.md "wikilink") ·
    [塘蓬镇](../Page/塘蓬镇.md "wikilink") ·
    [和寮镇](../Page/和寮镇.md "wikilink")

## 地理

廉江市北部為山地，中部為丘陵，南與西南部為平原階地。

## 氣候

一月份均溫15.2℃，七月份均溫28.4℃，年均降水量1,758毫米。

## 自然資源

  - 礦藏：[金](../Page/金.md "wikilink")、[銀](../Page/銀.md "wikilink")、[磷](../Page/磷.md "wikilink")、[鉬](../Page/鉬.md "wikilink")、[銻](../Page/銻.md "wikilink")、[鉛](../Page/鉛.md "wikilink")、[鋅](../Page/鋅.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[瓷土](../Page/瓷土.md "wikilink")、[花崗岩](../Page/花崗岩.md "wikilink")、[滑石等](../Page/滑石.md "wikilink")。

<!-- end list -->

  - 植物：[夜合花](../Page/夜合花.md "wikilink")、[白玉蘭](../Page/白玉蘭.md "wikilink")、[海棠](../Page/海棠.md "wikilink")、[月桂](../Page/月桂.md "wikilink")、[緬茄等](../Page/緬茄.md "wikilink")。

<!-- end list -->

  - 野生動物：[果子狸](../Page/果子狸.md "wikilink")、[黃猄](../Page/黃猄.md "wikilink")、[穿山甲](../Page/穿山甲.md "wikilink")、[水獺](../Page/水獺.md "wikilink")、[水鴨](../Page/水鴨.md "wikilink")、[禾花雀](../Page/黃胸鵐.md "wikilink")、[南蛇](../Page/南蛇.md "wikilink")。

## 經濟

2003年，廉江市实现生产总值74.05亿元，[工业主导地位增强](../Page/工业.md "wikilink")。

  - 第一产业增加值25.74亿元，增长2.5%，第一产业增加值比重下降4.7个百分点
  - 第二产业增加值20.43亿元，增长15.8%；第二产业增加值比重上升4.3个百分点
  - 第三产业增加值 27.88亿元，增长10.4%，第三产业增加值上升0.4个百分点。
  - 三次产业结构比重为34.8:27.6:37.6。

<!-- end list -->

  - 工业：九洲江工业基地、龙头沙港海洋产业园、饲料厂、市中药厂、雅塘糖业有限公司、建安公司、二轻安铺炮竹厂。
  - 農產：[糖蔗](../Page/糖蔗.md "wikilink")、[紅江橙](../Page/紅江橙.md "wikilink")、[荔枝](../Page/荔枝.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")。
  - 水產：[對蝦](../Page/對蝦.md "wikilink")
  - 畜產：[生豬](../Page/生豬.md "wikilink")、[北京鴨](../Page/北京鴨.md "wikilink")、[三黃雞等](../Page/三黃雞.md "wikilink")。

## 文化

  - [廉江回龙寺](../Page/廉江回龙寺.md "wikilink")，中共廉江第一个党支部廉江县农民协会旧址在此。
  - [谭氏宗祠](../Page/谭氏宗祠_\(廉江市\).md "wikilink")
  - [羅州故城遺址](../Page/羅州故城遺址.md "wikilink")，位於[廉江市河唇鎮西南](../Page/廉江市.md "wikilink")2.5公里的龍湖村東側山坡。

## 教育

[廉江中学](../Page/廉江中学.md "wikilink")，在廉江市的一所[重點中學](../Page/重點中學.md "wikilink")；

[廣東文理職業學院](../Page/廣東文理職業學院.md "wikilink")，是[廣東省所有](../Page/廣東省.md "wikilink")[縣級市當中唯一的高等院校](../Page/縣級市.md "wikilink")；

## 知名人士

  - [黎楨](../Page/黎楨.md "wikilink")，清朝廉潔官吏，廉江史上唯一的進士。
  - [江瑔](../Page/江瑔.md "wikilink")，中国民主革命家，[中华民国政治人物](../Page/中华民国.md "wikilink")、小说家。[南社成员](../Page/南社.md "wikilink")。\[1\]
  - [吕良伟](../Page/吕良伟.md "wikilink")，祖籍廉江，越南華裔，香港演員；
  - [勞麗詩](../Page/勞麗詩.md "wikilink")，中國跳水女運動員；2004年雅典奧運跳水冠軍。
  - [黄典元](../Page/黄典元.md "wikilink")，字伯谟，二十世纪50年代广东著名经济金融专家、教授；
  - [關澤恩](../Page/關澤恩.md "wikilink")，抗日戰爭烈士。
  - [袁超](../Page/袁超.md "wikilink")，[第十一届全国人民代表大会广东地区代表](../Page/第十一届全国人民代表大会.md "wikilink")\[2\]

## 参考文献

## 外部連結

  - [廉江市人民政府官方網](http://www.lianjiang.gov.cn/)
  - [廉江地情網](https://web.archive.org/web/20170418081652/http://www.gd-info.gov.cn/shtml/ljs//index.shtml)

## 其他

  - [廉江灵山战斗](../Page/廉江灵山战斗.md "wikilink")

[市](../Page/category:湛江区县市.md "wikilink")

[廉江市](../Category/廉江市.md "wikilink")
[湛江市](../Category/广东县级市.md "wikilink")
[粤](../Category/太平洋沿海城市.md "wikilink")

1.
2.