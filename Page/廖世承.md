[Liao_Shicheng.jpg](https://zh.wikipedia.org/wiki/File:Liao_Shicheng.jpg "fig:Liao_Shicheng.jpg")

**廖世承**（），字**茂如**。教育家。[江苏](../Page/江苏.md "wikilink")[嘉定](../Page/嘉定.md "wikilink")（今[上海嘉定](../Page/上海.md "wikilink")）人。

## 生平

1915年毕业于[北京](../Page/北京.md "wikilink")[清华学校](../Page/清华学校.md "wikilink")（今[清华大学](../Page/清华大学.md "wikilink")）高等科，1919年获[布朗大学教育心理学博士](../Page/布朗大学.md "wikilink")。同年回国，执教于[南京高等师范学校与](../Page/南京高等师范学校.md "wikilink")[国立东南大学](../Page/国立东南大学.md "wikilink")（後更名[國立中央大學](../Page/國立中央大學.md "wikilink")、[南京大學](../Page/南京大學.md "wikilink")），任教育科（今[南京师范大学](../Page/南京师范大学.md "wikilink")）教授暨附属中学主任。1927年后任光华大学副校长兼附属中学主任，其间在抗战时期任湖南国立师范学院（现[湖南师范大学](../Page/湖南师范大学.md "wikilink")）院长。1949年，任[光华大学校长](../Page/光华大学.md "wikilink")。

1951年起历任[华东师范大学筹备委员会常务委员](../Page/华东师范大学.md "wikilink")、副校长，后任上海第一师范学院（后与上海第二师范学院合并为上海师范学院）院长、[上海师范学院一级教授](../Page/上海师范学院.md "wikilink")、院长等职。

## 事略

1919年回国后，廖世承和[徐养秋](../Page/徐养秋.md "wikilink")、[陈鹤琴](../Page/陈鹤琴.md "wikilink")、[孟宪承](../Page/孟宪承.md "wikilink")、[俞子夷](../Page/俞子夷.md "wikilink")、[程其保等人汇集在南京高师和东南大学教育科](../Page/程其保.md "wikilink")，成为[郭秉文](../Page/郭秉文.md "wikilink")、[陶行知倡导的中国教育科学化的试验先驱人物](../Page/陶行知.md "wikilink")。廖世承任[南京高师与东南大学附属中学主任](../Page/南京师范大学附属中学.md "wikilink")，开展教学改革试验，使附中成为中国现代中等教育实验的中心。当时的[国立东南大学附属中学](../Page/国立东南大学附属中学.md "wikilink")(今[南师附中](../Page/南师附中.md "wikilink"))，为全国最有声望和报考最踊跃的中学，师资水准之高、阵容之强在中国中学史上堪称空前绝后，培养出了[李国鼎](../Page/李国鼎.md "wikilink")、[周鸿经](../Page/周鸿经.md "wikilink")、[巴金](../Page/巴金.md "wikilink")、[胡风](../Page/胡风.md "wikilink")、[屈伯川](../Page/屈伯川.md "wikilink")、[徐克勤](../Page/徐克勤.md "wikilink")、[周同庆](../Page/周同庆.md "wikilink")、[余纪忠](../Page/余纪忠.md "wikilink")、[孙元良](../Page/孙元良.md "wikilink")、[刘晓等众多各界杰出人士](../Page/刘晓.md "wikilink")。廖世承在东大附中主持的教育试验成果，为[壬戌学制的中学教育学制提供了范本](../Page/壬戌学制.md "wikilink")。
　　　　　

## 著作

  - 《教育心理学》
  - 《智力测验法》
  - 《测验概要》 (与陈鹤琴合著)

[L廖](../Page/category:清华大学校友.md "wikilink")

[L廖](../Category/中國教育家.md "wikilink")
[L廖](../Category/上海人.md "wikilink")
[S世](../Category/廖姓.md "wikilink")
[L](../Category/國立清華大学校友.md "wikilink")
[師](../Category/國立中央大學教授.md "wikilink")
[Category:南京大學教授](../Category/南京大學教授.md "wikilink")
[Category:南京师范大学校友](../Category/南京师范大学校友.md "wikilink")
[Category:光華大學教授](../Category/光華大學教授.md "wikilink")
[Category:布朗大學校友](../Category/布朗大學校友.md "wikilink")
[Category:上海师范学院院长](../Category/上海师范学院院长.md "wikilink")
[Category:上海师范学院教授](../Category/上海师范学院教授.md "wikilink")
[Category:光华大学校长](../Category/光华大学校长.md "wikilink")