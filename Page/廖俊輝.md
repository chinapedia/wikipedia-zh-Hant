**廖俊輝**（**Liu Chun Fai
Raymond**，），生於[香港](../Page/香港.md "wikilink")，已退役[香港足球運動員](../Page/香港.md "wikilink")，[香港足球教練](../Page/香港.md "wikilink")，司職[守門員](../Page/守門員.md "wikilink")，外號「清高輝」，是1980年代[香港其中一位最出色的](../Page/香港.md "wikilink")[門將](../Page/門將.md "wikilink")，現時持有[亞洲足協A級教練牌照及](../Page/亞洲足球協會.md "wikilink")[守門員訓練員資格](../Page/亞洲足球協會.md "wikilink")。現時擔任[香港足球代表隊助教](../Page/香港足球代表隊.md "wikilink")。

## 球員生涯

廖俊輝生於[香港](../Page/香港.md "wikilink")，早年在1970年畢業於[聖公會基德小學下午校](../Page/聖公會基德小學.md "wikilink")（第7屆小六），與香港著名播音員、前[香港電台中文台台長](../Page/香港電台.md "wikilink")[馮偉棠為同級同學](../Page/馮偉棠.md "wikilink")\[1\]。他出身於[愉園青年軍](../Page/愉園體育會.md "wikilink")，曾效力過[駒騰](../Page/宏輝駒騰.md "wikilink")、[加山](../Page/加山.md "wikilink")、[海蜂](../Page/海蜂足球隊.md "wikilink")、[東方](../Page/東方足球隊.md "wikilink")、[俠士](../Page/俠士足球隊.md "wikilink")、[精工](../Page/精工足球隊.md "wikilink")\[2\]、[美青](../Page/美孚青年足球隊.md "wikilink")、[麗新及](../Page/保濟足球隊.md "wikilink")[星島多支甲組球隊](../Page/星島體育會.md "wikilink")，亦曾代表[香港南征北討](../Page/香港足球代表隊.md "wikilink")，[1985年世界盃外圍賽小組冠軍](../Page/1986年世界盃足球賽.md "wikilink")[香港隊門將](../Page/香港足球代表隊.md "wikilink")，1981年獲選[香港足球明星選舉最佳十一人](../Page/香港足球明星選舉.md "wikilink")。1985年廖俊輝在球會表現出色，亦是[香港代表隊的正選主力](../Page/香港足球代表隊.md "wikilink")，不過於季尾一場無關重要的聯賽中仍然搏到盡，結果意外令韌帶斷裂，無法為[香港隊披甲參與](../Page/香港足球代表隊.md "wikilink")「[五一九](../Page/五一九事件.md "wikilink")」經典大戰。1992年掛靴，退役後專注青訓工作。

## 教練生涯

1992年掛靴，退役後專注青訓工作，曾任教[香港09](../Page/香港09.md "wikilink")、[油尖旺](../Page/油尖旺足球隊.md "wikilink")、[足球隊青苗及綠基拔尖](../Page/黃大仙足球隊.md "wikilink")。2004年從[加拿大回流返港開始](../Page/加拿大.md "wikilink")，其後加盟甲組球隊[公民出任守門員訓練員](../Page/公民足球隊.md "wikilink")，2006年獲提升為教練，在一場聯賽中，[公民於開賽僅](../Page/公民足球隊.md "wikilink")30分鐘內便輸2球，廖俊輝認為繼續下去只有重傷收場，結果於30分鐘便決定換人及改變踢法，最終球隊成功賽和對手。翌季帶領[公民奪得](../Page/公民足球隊.md "wikilink")[甲組聯賽亞軍及](../Page/2007–08年香港甲組足球聯賽.md "wikilink")[足總盃冠軍](../Page/香港足總盃.md "wikilink")，成為[2007–08年度最佳教練](../Page/香港足球明星選舉.md "wikilink")。2008年夏天，廖俊輝應邀「上山」加盟[南華](../Page/南華足球隊.md "wikilink")，擔任守門員教練。同年9月，[南華主教練](../Page/南華足球隊.md "wikilink")[曾偉忠以健康理由呈辭](../Page/曾偉忠.md "wikilink")，由廖俊輝接任主教練，為[南華捧走聯賽冠軍](../Page/南華足球隊.md "wikilink")，其後擔任[南華副領隊](../Page/南華足球隊.md "wikilink")。2011年6月，廖俊輝辭去[南華副領隊一職](../Page/南華足球隊.md "wikilink")，接受[香港隊的邀請](../Page/香港足球代表隊.md "wikilink")，成為新任的[香港總教練](../Page/香港足球代表隊.md "wikilink")。在任期間，廖俊輝帶領[香港隊贏得](../Page/香港足球代表隊.md "wikilink")[2011年龍騰盃冠軍及](../Page/2011年龍騰盃國際足球邀請賽.md "wikilink")[第34屆省港盃冠軍](../Page/第34屆省港盃.md "wikilink")。\[3\]

2014年9月，廖俊輝帶領[香港U23隊贏](../Page/香港奧運足球代表隊.md "wikilink")[第70屆港澳埠際賽](../Page/港澳埠際足球賽.md "wikilink")。\[4\]2015年11月，再次帶領[香港B隊贏](../Page/香港足球代表隊.md "wikilink")[第71屆港澳埠際賽](../Page/港澳埠際足球賽.md "wikilink")。\[5\]2017年1月，今屆省港盃賽事[香港隊派出](../Page/香港足球代表隊.md "wikilink")[香港B隊迎戰](../Page/香港足球代表隊.md "wikilink")，賽前更爆發退隊潮有，不少主力退隊，被看低一線的情況下，首回合以2–3僅負[廣東](../Page/省港盃.md "wikilink")，次回合以1–1賽和對方，雖然最終[香港兩回合計以總比數](../Page/香港足球代表隊.md "wikilink")3–4落敗，連續四屆無緣[第39屆省港盃冠軍](../Page/第39屆省港盃.md "wikilink")，但廖俊輝帶領球員打出驚喜，已贏盡球迷掌聲。\[6\]

## 個人生活

在云云名帥當中，廖俊輝最欣賞現今[曼城教練](../Page/曼徹斯特城足球會.md "wikilink")[哥迪奧拿](../Page/佩普·哥迪奧拿.md "wikilink")，認為這位橫掃[西甲及](../Page/西甲.md "wikilink")[德甲的冠軍級主帥充滿足球智慧](../Page/德甲.md "wikilink")。\[7\]

## 個人榮譽

  - [總督盃](../Page/總督盃.md "wikilink") 最佳防守球員（1979–80、1980–81年度）
  - [香港足球明星選舉](../Page/香港足球明星選舉.md "wikilink") 最佳十一人（1980–81年度）
  - [香港足球明星選舉](../Page/香港足球明星選舉.md "wikilink") 最佳教練（2007–08年度）

## 金句

  - 「前面無殺手，後面無防守」

## 參考資料

## 外部連結

  - [功臣教練拒稱神奇讚合作佳](http://hk.sports.yahoo.com/080518/42/2u6gb.html)
  - [南華足球隊官方網頁資料](http://www.southchinafc.com/template?series=4&article=3387)

[C](../Category/廖姓.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:駒騰球員](../Category/駒騰球員.md "wikilink")
[Category:加山球員](../Category/加山球員.md "wikilink")
[Category:海蜂球員](../Category/海蜂球員.md "wikilink")
[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:俠士球員](../Category/俠士球員.md "wikilink")
[Category:精工球員](../Category/精工球員.md "wikilink")
[Category:美青球員](../Category/美青球員.md "wikilink")
[Category:麗新球員](../Category/麗新球員.md "wikilink")
[Category:星島球員](../Category/星島球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:香港足球主教練](../Category/香港足球主教練.md "wikilink")
[Category:公民教練](../Category/公民教練.md "wikilink")
[Category:南華教練](../Category/南華教練.md "wikilink")
[Category:香港足球代表隊教練](../Category/香港足球代表隊教練.md "wikilink")

1.  [1970年聖公會基德小學下午校第7屆小六，紅圈者為廖俊輝](https://drive.google.com/file/d/1mLG2e53WsOvZ20w8L6dtWa51RV1eGRh3/view?usp=sharing)
2.  [精工球員名單](http://albertchh.sinaman.com/seiko_main.htm)
3.  [教頭廖俊輝約滿決「下山」](http://news.singtao.ca/vancouver/2013-06-21/sports1371799953d4556389.html)
    [星島日報](../Page/星島日報.md "wikilink") 2013-06-21
4.  [港U23蟬聯港澳埠際賽廖俊輝唔收貨](http://football.on.cc/cnt/news/newc/20140907/fbnewc0124x0.html)
    2014/09/07
5.  [香港B隊備戰港澳埠際賽](http://hk.apple.nextmedia.com/sports/art/20151110/19366634)
    [蘋果日報](../Page/蘋果日報.md "wikilink") 2015年11月10日
6.  [省港盃　港隊力戰失盃有晒交代](http://hk.on.cc/hk/bkn/cnt/sport/20170104/bkn-20170104215635240-0104_00882_001.html?eventsection=hk_sport&eventid=4028828d4d9a0d6f014dc43e82ec5983)【on.cc東網】
    2017年1月4日
7.  [【第12人】介紹番！新任港隊行政經理廖俊輝](http://hk.on.cc/hk/bkn/cnt/sport/20170202/bkn-20170202000504458-0202_00882_001.html)
    【on.cc東網】2017年02月02日