**廖大林**（），雲林縣人，為[中華民國前立法委員](../Page/中華民國.md "wikilink")，[民進黨籍](../Page/民進黨.md "wikilink")。

## 學歷

  - [西螺文昌國小畢業](../Page/西螺鎮.md "wikilink")
  - [西螺初中畢業](../Page/西螺初中.md "wikilink")
  - [台北師範學校畢業](../Page/台北師範學校.md "wikilink")
  - [台灣大學經濟系畢業](../Page/台灣大學.md "wikilink")
  - [文化大學經濟研究所碩士](../Page/文化大學.md "wikilink")
  - 美國[德州農工大學](../Page/德州農工大學.md "wikilink")[統計學博士](../Page/統計學.md "wikilink")

## 經歷

  - [文化大學商學院院長](../Page/文化大學.md "wikilink")
  - [企業管理研究所所長](../Page/企業管理研究所.md "wikilink")
  - [東吳大學會計研究所兼任教授](../Page/東吳大學.md "wikilink")
  - [美國聯邦政府高級官員兼高級統計師](../Page/美國.md "wikilink")
  - [美國多所大學教職與研究](../Page/美國.md "wikilink")
  - [德州](../Page/德州.md "wikilink")[台灣同鄉會首任會長](../Page/台灣.md "wikilink")
  - [華盛頓](../Page/華盛頓.md "wikilink")[台灣人權會首任會長](../Page/台灣.md "wikilink")
  - [雲林農權會會長](../Page/雲林.md "wikilink")

## 政治

### 2001年立法委員選舉

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>黨籍</p></th>
<th><p>姓名</p></th>
<th><p>得票數</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>陳劍松</p></td>
<td><p>35,612</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p><a href="../Page/高孟定.md" title="wikilink">高孟定</a></p></td>
<td><p>33,961</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>廖大林</p></td>
<td><p>22,613</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p><a href="../Page/蘇治芬.md" title="wikilink">蘇治芬</a></p></td>
<td><p>58,960</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p><a href="../Page/林國華.md" title="wikilink">林國華</a></p></td>
<td><p>37,231</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p><a href="../Page/曾蔡美佐.md" title="wikilink">曾蔡美佐</a></p></td>
<td><p>38,012</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>蔡桓生</p></td>
<td><p>1,232</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p><a href="../Page/侯惠仙.md" title="wikilink">侯惠仙</a></p></td>
<td><p>27,353</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p><a href="../Page/許舒博.md" title="wikilink">許舒博</a></p></td>
<td><p>31,886</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p><a href="../Page/林明義.md" title="wikilink">林明義</a></p></td>
<td><p>29,530</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>何振盛</p></td>
<td><p>1,183</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>李建昇</p></td>
<td><p>20,851</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

## 外部連結

[立法院全球資訊網](http://www.ly.gov.tw/03_leg/0301_main/tornado_leg_intro/legIntro.action?lgno=00123&stage=3)

[Category:第2屆中華民國立法委員](../Category/第2屆中華民國立法委員.md "wikilink")
[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:東吳大學教授](../Category/東吳大學教授.md "wikilink")
[Category:德州農工大學校友](../Category/德州農工大學校友.md "wikilink")
[Category:國立臺灣大學社會科學院校友](../Category/國立臺灣大學社會科學院校友.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")
[Category:臺灣省立臺北師範學校校友](../Category/臺灣省立臺北師範學校校友.md "wikilink")
[Category:西螺人](../Category/西螺人.md "wikilink")
[Da大](../Category/張廖姓.md "wikilink")