**廣平大學**是位於[越南](../Page/越南.md "wikilink")[廣平省](../Page/廣平省.md "wikilink")[洞海市的一所大学](../Page/洞海市.md "wikilink")，由廣平師範高等院校和廣平經濟高等院校于2006年合并而成。

## 科系

廣平大學現有經濟、師範（数学、物理、化學、生物學、文學、歷史、地理、英語）、法律等专业。

## 參見

  - [越南大學列表](../Page/越南大學列表.md "wikilink")

## 外部链接

  - [廣平大學](http://www.quangbinhuni.edu.vn/)

[Category:越南大學](../Category/越南大學.md "wikilink")
[Category:廣平省](../Category/廣平省.md "wikilink")
[Category:2006年創建的教育機構](../Category/2006年創建的教育機構.md "wikilink")