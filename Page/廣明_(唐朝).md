**廣明**（880年正月至881年七月）是[唐僖宗的年號](../Page/唐僖宗.md "wikilink")，共计2年。

## 大事記

广明元年十二月（881年1月），唐朝起义军首领[黄巢攻入长安](../Page/黄巢.md "wikilink")，[唐僖宗逃难四川](../Page/唐僖宗.md "wikilink")。

## 出生

## 逝世

## 紀年

| 廣明                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 880年                           | 881年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他使用[廣明年號的政權](../Page/廣明.md "wikilink")
  - 同期存在的其他政权年号
      - [王霸](../Page/王霸.md "wikilink")（878年二月至880十一月）：[唐朝時期領袖](../Page/唐朝.md "wikilink")[黄巢之年號](../Page/黄巢.md "wikilink")
      - [金統](../Page/金統.md "wikilink")（880年十二月至884六月）：唐朝時期領袖黄巢之年號
      - [元慶](../Page/元慶.md "wikilink")（877年四月十六日至885年二月二十一日）：[平安時代](../Page/平安時代.md "wikilink")[陽成天皇](../Page/陽成天皇.md "wikilink")、[光孝天皇之年號](../Page/光孝天皇.md "wikilink")
      - [貞明](../Page/貞明_\(隆舜\).md "wikilink")（878年起）：[南詔領袖](../Page/南詔.md "wikilink")[隆舜之年號](../Page/隆舜.md "wikilink")
      - [承智](../Page/承智.md "wikilink")：南詔領袖隆舜之年號
      - [大同](../Page/大同_\(隆舜\).md "wikilink")（至888年）：南詔領袖隆舜之年號

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1998年3月，ISBN 4639007116

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:880年代中国政治](../Category/880年代中国政治.md "wikilink")
[Category:880年](../Category/880年.md "wikilink")
[Category:881年](../Category/881年.md "wikilink")