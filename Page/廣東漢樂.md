**廣東漢樂**，又稱作**客家音樂**、**客家漢樂**、**外江弦**、**儒家樂**、**漢調音樂**等，是流行於[客家人的一種民間器樂](../Page/客家.md "wikilink")，主要分布於[中國](../Page/中國.md "wikilink")[廣東東部](../Page/廣東.md "wikilink")[客家地區](../Page/客家地區.md "wikilink")，此外並及於[江西](../Page/江西.md "wikilink")、[福建](../Page/福建.md "wikilink")、[广西](../Page/广西.md "wikilink")、[海南以及](../Page/海南.md "wikilink")[臺灣與](../Page/臺灣.md "wikilink")[東南亞華人之間](../Page/東南亞.md "wikilink")。廣東漢樂被認為是中原地區居民南遷時，將中原與廣東地區音樂結合發展而成，因其在數百年間傳承少有變化，有「中州古樂的活化石」之稱2006年，被[中國文化部列入國家級非物質文化遺產第一批保護名錄](../Page/中國文化部.md "wikilink")。

廣東漢樂分佈在廣東梅州、汕頭、韶關、惠陽等地區，又以梅州市大埔縣為代表，舊稱客家音樂、外江弦、儒家樂、漢調音樂等。據查是古代漢民由中原南遷時帶入的，有"中州古樂"之稱，在大埔流傳至少有五百年以上的歷史。廣東漢樂保留了原有中原音樂的特點，並與大埔當地的民間音樂（如打八音、中軍班音樂）等相融合，同時又吸納了潮樂（如大鑼鼓）的一些成分，已成為廣東三大樂種之一。
在長期的流傳中，漢樂有過種種稱謂。如"國樂"、"中州古韻"、"客家音樂"、"外江弦"、"漢調音樂"、"鑼鼓吹"、"打八音"等。為了正本清源，一九六二年第一屆羊城音樂會上，經眾多音樂家研討，正式定名為"廣東漢樂"。

## 廣東漢樂的演出形式和內容

廣東漢樂可以包括幾種演出形式

  - [和弦索](../Page/和弦索.md "wikilink")：為[絲竹樂形式](../Page/絲竹樂.md "wikilink")，由[頭弦領奏](../Page/頭弦.md "wikilink")，搭配[月琴](../Page/月琴.md "wikilink")、[琵琶](../Page/琵琶.md "wikilink")、[椰胡](../Page/椰胡.md "wikilink")、[角胡](../Page/角胡.md "wikilink")、[三弦](../Page/三弦.md "wikilink")、[笛子等樂器](../Page/笛子.md "wikilink")。
  - 鑼鼓吹：為[鑼鼓樂形式](../Page/鑼鼓樂.md "wikilink")，由[嗩吶主奏](../Page/嗩吶.md "wikilink")，配合[蘇鑼](../Page/蘇鑼.md "wikilink")、[小鑼](../Page/小鑼.md "wikilink")、[鈸](../Page/鈸.md "wikilink")、[碗鑼](../Page/碗鑼.md "wikilink")、[乳鑼](../Page/乳鑼.md "wikilink")、[梆子](../Page/梆子.md "wikilink")、[搖板等打擊樂器](../Page/搖板.md "wikilink")。
  - [清樂](../Page/清樂.md "wikilink")：由[箏](../Page/箏.md "wikilink")、[琵琶](../Page/琵琶.md "wikilink")、[椰胡三種樂器合奏](../Page/椰胡.md "wikilink")，又稱作「三件頭」。
  - [中軍班](../Page/中軍班.md "wikilink")：是一種在[興寧](../Page/興寧.md "wikilink")、[梅縣](../Page/梅縣.md "wikilink")、[大埔和福建西部的職業樂班](../Page/大埔縣.md "wikilink")，主要在婚喪喜慶等場合吹奏。其演出內容包括古曲大樂、民間小調，以及以嗩吶[咔奏](../Page/咔奏.md "wikilink")[廣東漢劇的唱腔等](../Page/廣東漢劇.md "wikilink")。

廣東漢樂著名的曲子有《翡翠登潭》、《出水蓮》、《懷古》等。

## 參考資料

1.  《中國音樂詞典》（臺北：丹青圖書公司），590頁。

## 相關條目

  - [客家八音](../Page/客家八音.md "wikilink")
  - [客語流行音樂](../Page/客語流行音樂.md "wikilink")

[Category:客家音樂](../Category/客家音樂.md "wikilink")
[Category:广东音乐](../Category/广东音乐.md "wikilink")