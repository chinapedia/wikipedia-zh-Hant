[Hong_Pak_Court_(full_view,_sky_blue_version_and_better_contrast).jpg](https://zh.wikipedia.org/wiki/File:Hong_Pak_Court_\(full_view,_sky_blue_version_and_better_contrast\).jpg "fig:Hong_Pak_Court_(full_view,_sky_blue_version_and_better_contrast).jpg")
[Hong_Shui_Court_(better_contrast).jpg](https://zh.wikipedia.org/wiki/File:Hong_Shui_Court_\(better_contrast\).jpg "fig:Hong_Shui_Court_(better_contrast).jpg")
[Kwong_Tin_Shopping_Centre_2nd_floor.jpg](https://zh.wikipedia.org/wiki/File:Kwong_Tin_Shopping_Centre_2nd_floor.jpg "fig:Kwong_Tin_Shopping_Centre_2nd_floor.jpg")\]\]
[Kwong_Tin_Market_(1).jpg](https://zh.wikipedia.org/wiki/File:Kwong_Tin_Market_\(1\).jpg "fig:Kwong_Tin_Market_(1).jpg")
[Kwong_Tin_Estate_Children_Play_Area.jpg](https://zh.wikipedia.org/wiki/File:Kwong_Tin_Estate_Children_Play_Area.jpg "fig:Kwong_Tin_Estate_Children_Play_Area.jpg")
[Hong_Pak_Court_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Hong_Pak_Court_Covered_Walkway.jpg "fig:Hong_Pak_Court_Covered_Walkway.jpg")
[Lam_Tin_(South)_Sports_Centre.jpg](https://zh.wikipedia.org/wiki/File:Lam_Tin_\(South\)_Sports_Centre.jpg "fig:Lam_Tin_(South)_Sports_Centre.jpg")
[Hong_Pak_Court_Fitness_and_Children_Play_Area.jpg](https://zh.wikipedia.org/wiki/File:Hong_Pak_Court_Fitness_and_Children_Play_Area.jpg "fig:Hong_Pak_Court_Fitness_and_Children_Play_Area.jpg")
[Hong_Pak_Court_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Hong_Pak_Court_Car_Park.jpg "fig:Hong_Pak_Court_Car_Park.jpg")
**廣田邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")[藍田](../Page/藍田_\(香港\).md "wikilink")[碧雲道](../Page/碧雲道.md "wikilink")160－172號，於1992年落成入伙。

**康栢苑**（）及**康瑞苑**（）均是[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，在廣田邨旁邊，分別在1993年及1999年落成，有七座及一座樓宇。

## 屋邨資料

### 廣田邨

| 樓宇名稱 | 地址      | 樓宇類型                               | 落成年份 |
| ---- | ------- | ---------------------------------- | ---- |
| 廣靖樓  | 碧雲道172號 | [和諧三型](../Page/和諧三型.md "wikilink") | 1992 |
| 廣軒樓  | 碧雲道166號 | [和諧一型](../Page/和諧一型.md "wikilink") | 1993 |
| 廣雅樓  | 碧雲道162號 |                                    |      |
| 廣逸樓  | 碧雲道160號 |                                    |      |

### 康栢苑

| 樓宇名稱\[1\] | 地址      | 樓宇類型                               | 落成年份 |
| --------- | ------- | ---------------------------------- | ---- |
| 松栢閣       | 碧雲道139號 | [新十字型](../Page/新十字型.md "wikilink") | 1993 |
| 龍栢閣       |         |                                    |      |
| 祥栢閣       |         |                                    |      |
| 瑞栢閣       |         |                                    |      |
| 欣栢閣       |         |                                    |      |
| 榮栢閣       |         |                                    |      |
| 金栢閣       |         |                                    |      |
|           |         |                                    |      |

### 康瑞苑

| 樓宇名稱\[2\] | 地址      | 樓宇類型                               | 落成年份 |
| --------- | ------- | ---------------------------------- | ---- |
| 康瑞苑       | 碧雲道133號 | [新十字型](../Page/新十字型.md "wikilink") | 1999 |
|           |         |                                    |      |

## 設施

  - [藍田（南）體育館](../Page/藍田（南）體育館.md "wikilink")

### 廣田商場

廣田商場樓高3層，面積97,600呎，於1992年建成。商場租戶包括連鎖食肆、超級市場及便利店。而停車場共有53個車位。

領展於2015年10月出售廣田商場後，新業主「友文投資」於2016年4月向租用商場2樓的5間非牟利機構更新租約，徵收優惠租金外的一筆高昂管理費，加租近85%。房委會指已發信提醒商場業主只可向有關機構收取優惠租金，不得收取管理費或其他費用。\[3\]

  - [郵筒](../Page/郵筒.md "wikilink")
  - [街市](../Page/街市.md "wikilink")
  - [熟食中心](../Page/熟食中心.md "wikilink")
  - [私家診所](../Page/私家診所.md "wikilink")
  - [順豐速遞](../Page/順豐速遞.md "wikilink")
  - [大家樂](../Page/大家樂.md "wikilink")
  - [日本城](../Page/日本城.md "wikilink")
  - [惠康超級市場](../Page/惠康超級市場.md "wikilink")
  - [補習社](../Page/補習社.md "wikilink")
  - [白普理廣田社區健康中心](../Page/白普理廣田社區健康中心.md "wikilink")
  - 社會福利署藍田綜合家庭服務中心
  - [博愛醫院陳徐鳳蘭幼稚園](https://web.archive.org/web/20160811223613/http://pohchfl.icampus.hk/webapp/)（1993年創辦）
  - [7-11](../Page/7-11.md "wikilink")
  - [759阿信屋](../Page/759阿信屋.md "wikilink")
  - [金翡翠海鮮酒家](../Page/金翡翠海鮮酒家.md "wikilink")
  - [廣德中西藥行](../Page/廣德中西藥行.md "wikilink")
  - [水電工程公司](../Page/水電工程公司.md "wikilink")
  - [理髮店](../Page/理髮店.md "wikilink")
  - [跌打醫館](../Page/跌打醫館.md "wikilink")
  - [小童群益會](../Page/小童群益會.md "wikilink")
  - [房屋署](../Page/房屋署.md "wikilink")

Kwong Tin Shopping Centre Ground
Level.jpg|廣田商場地下餅店及[759阿信屋](../Page/759阿信屋.md "wikilink")
Kwong Tin Shopping Centre.JPG|廣田商場1樓鞋店 Kwong Tin Shopping Centre Level
1.jpg|廣田商場1樓[日本城及](../Page/日本城.md "wikilink")[大家樂](../Page/大家樂.md "wikilink")
Kwong Tin Market (2).jpg|廣田街市食肆

### 廣靖樓

  - [廣靖樓互助委員會](../Page/廣靖樓互助委員會.md "wikilink")

### 廣雅樓及廣軒樓

  - [保良局蕭明紀念護老院](../Page/保良局蕭明紀念護老院.md "wikilink")
  - [廣雅樓互助委員會](../Page/廣雅樓互助委員會.md "wikilink")
  - [廣軒樓互助委員會](../Page/廣軒樓互助委員會.md "wikilink")

### 廣逸樓

  - [藍田街坊褔利會藍田會所](../Page/藍田街坊褔利會藍田會所.md "wikilink")
  - [柯創盛議員辦事處](../Page/柯創盛.md "wikilink")
  - [廣逸樓互助委員會](../Page/廣逸樓互助委員會.md "wikilink")

### 康栢苑及停車場大樓

  - [聖公會慈光堂聖匠幼稚園](https://web.archive.org/web/20160814071620/http://skhklcopc.edu.hk/hc/home.htm)（1973年創辦）

<!-- end list -->

  - [康栢苑業主立案法團](../Page/康栢苑業主立案法團.md "wikilink")

### 康瑞苑及停車場大樓

  - [康瑞苑業主立案法團](../Page/康瑞苑業主立案法團.md "wikilink")
  - [何啟明議員辦事處](../Page/何啟明_\(工聯會\).md "wikilink")

## 圖片集

Kwong Tin Estate.jpg|廣田邨[和諧一型大廈](../Page/和諧式大廈#和諧一型.md "wikilink") Kwong
Ching House.jpg|廣田邨[和諧三型大廈](../Page/和諧式大廈#和諧三型.md "wikilink") Hong Pak
Court.jpg|康栢苑的樓宇以綠色為外牆色彩 Hong Shui Court 2012.JPG|眺望康瑞苑與康栢苑 Kwong Tin
Estate Fitness Area.jpg|廣田邨健體區 Kwong Tin Estate Pebble Walking
Trail.jpg|廣田邨卵石路步行徑

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/藍田（廣田邨）巴士總站.md" title="wikilink">藍田（廣田邨）巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/碧雲道.md" title="wikilink">碧雲道</a></dt>

</dl>
<dl>
<dt>鄰近<a href="../Page/高翔苑.md" title="wikilink">高翔苑</a>/<a href="../Page/高怡邨.md" title="wikilink">高怡邨</a>/<a href="../Page/高俊苑.md" title="wikilink">高俊苑</a></dt>

</dl>
<ul>
<li><a href="../Page/港鐵.md" title="wikilink">港鐵</a><a href="../Page/油塘站.md" title="wikilink">油塘站A</a>1出口</li>
</ul></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  - [房委會廣田邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-13_4953,00.html)
  - [廣田商場](http://www.thelinkreit.com/TC/leasing/Pages/Property-Locator-Details.aspx?pid=141)
  - [藍田街坊褔利會藍田會所](https://web.archive.org/web/20130801040700/http://www.kaifong-lamtin.org.hk/elderly/index.htm)
  - [陳徐鳳蘭幼稚園
    幼兒中心](http://www.pokoi.org.hk/tc/services_education2_detail.aspx?id=48)
  - [廣逸樓互助委員會](https://www.facebook.com/pages/%E5%BB%A3%E9%80%B8%E6%A8%93%E4%BA%92%E5%8A%A9%E5%A7%94%E5%93%A1%E6%9C%83-Kwong-Yat-House-Mutual-Aid-Committe/292896437396991)
  - [柯創盛議員辦事處](https://www.facebook.com/OrChongShing.Office)
  - [康栢苑業主立案法團](http://www.hongpakcourt.com/)
  - [聖公會慈光堂聖匠幼稚園](http://www.skhklchc.edu.hk/)
  - [何啟明議員辦事處](http://www.hkaiming.hk)

[en:Public housing estates in Lam Tin\#Kwong Tin
Estate](../Page/en:Public_housing_estates_in_Lam_Tin#Kwong_Tin_Estate.md "wikilink")

[Category:藍田 (香港)](../Category/藍田_\(香港\).md "wikilink")

1.  [康栢苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-13_5661,00.html)

2.  [康瑞苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-13_5763,00.html)

3.  [向5社福機構開刀　廣田商場收額外管理費
    on.cc東網 2016年3月14日](http://hk.on.cc/hk/bkn/cnt/news/20160314/bkn-20160314172228269-0314_00822_001.html)