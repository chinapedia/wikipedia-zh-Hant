[拉格朗日力學與](../Page/拉格朗日力學.md "wikilink")[哈密頓力學時常涉及](../Page/哈密頓力學.md "wikilink")**廣義動量**。這是因為採用廣義坐標有許多優點。而廣義動量是正則共軛於廣義坐標的[物理量](../Page/物理量.md "wikilink")，又稱為**共軛動量**。

假設一個物理系統的[廣義坐標是](../Page/廣義坐標.md "wikilink")
\((q_1,\ q_2,\ q_3,\ \dots,\ q_N)\,\!\)
，則[廣義速度為](../Page/廣義速度.md "wikilink")
\((\dot{q}_1,\ \dot{q}_2,\ \dot{q}_3,\ \dots,\ \dot{q}_N)\,\!\) 。表示廣義動量為
\((p_1,\ p_2,\ p_3,\ \dots,\ p_N)\,\!\)
。定義廣義動量為[拉格朗日量](../Page/拉格朗日量.md "wikilink")
\(\mathcal{L}\,\!\) 隨廣義速度的導數：

\[p_{k} \ \stackrel{\mathrm{def}}{=}\  \frac{\partial \mathcal{L}}{\partial\dot q_{k}}\,\!\]
；

## 廣義動量守恆定律

如果一個物理系統是[單演系統與](../Page/單演系統.md "wikilink")[完整系統](../Page/完整系統.md "wikilink")，那麼，[哈密頓原理保證](../Page/哈密頓原理.md "wikilink")[拉格朗日方程式的成立](../Page/拉格朗日方程式.md "wikilink")：

\[\frac{d}{dt}\left(\frac{\partial \mathcal{L}}{\partial \dot{q}_k}\right) - \frac{\partial \mathcal{L}}{\partial q_k}=0\,\!\]
。

假若，\(\mathcal{L}\,\!\) 不顯含廣義坐標 \(q_{k}\,\!\) ：

  -
    \(\frac{\partial \mathcal{L}}{\partial q_{k}}=0\,\!\) ，

則廣義動量 \(p_{k}\,\!\) 是常數。在此種狀況，坐標 \(q_{k}\,\!\)
稱為**循環坐標**，或**可略坐標**。舉例而言，如果我們用[圓柱坐標](../Page/圓柱坐標系.md "wikilink")
\((r,\ \theta,\ h)\,\!\) 來描述一個粒子的運動，而 \(\mathcal{L}\,\!\) 與
\(\theta\,\!\) 無關，則廣義動量是守恆的[角動量](../Page/角動量.md "wikilink")。

## 參閱

  - [拉格朗日力學](../Page/拉格朗日力學.md "wikilink")
  - [哈密頓力學](../Page/哈密頓力學.md "wikilink")
  - [廣義力](../Page/廣義力.md "wikilink")
  - [正則變換](../Page/正則變換.md "wikilink")

[G](../Category/力學.md "wikilink") [G](../Category/經典力學.md "wikilink")
[G](../Category/拉格朗日力學.md "wikilink")
[G](../Category/哈密頓力學.md "wikilink")