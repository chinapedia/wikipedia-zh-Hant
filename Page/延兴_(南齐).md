**延兴**（494年七月—十月）是[南朝齊皇帝](../Page/南朝齊.md "wikilink")[萧昭文的](../Page/萧昭文.md "wikilink")[年號](../Page/年號.md "wikilink")，共數月。

## 大事记

## 出生

## 逝世

## 纪年

| 延兴                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 494年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[延兴年號的政權](../Page/延兴.md "wikilink")
  - 同期存在的其他政权年号
      - [太和](../Page/太和_\(北魏孝文帝\).md "wikilink")（477年正月-499年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝文帝元宏年号](../Page/北魏孝文帝.md "wikilink")
      - [太安](../Page/太安_\(柔然\).md "wikilink")（492年-505年）：[柔然政权候其伏代库者可汗](../Page/柔然.md "wikilink")[那盖年号](../Page/那盖.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南朝齊年號](../Category/南朝齊年號.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")