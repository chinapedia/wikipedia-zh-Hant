**延美珠**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。

## 演出作品

### 電視劇

  - 2006年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[戀人](../Page/戀人_\(2006年電視劇\).md "wikilink")》飾演
    崔允
  - 2007年：[KBS](../Page/韓國放送公社.md "wikilink")《[你好！小姐](../Page/你好！小姐.md "wikilink")》
  - 2009年：KBS《[傻瓜](../Page/傻瓜_\(電視劇\).md "wikilink")》
  - 2009年：SBS《[妻子回來了](../Page/妻子回來了.md "wikilink")》
  - 2009年：KBS《[天下無敵李平岡](../Page/天下無敵李平岡.md "wikilink")》
  - 2011年：KBS《[相信愛情](../Page/相信愛情.md "wikilink")》
  - 2011年：SBS《[太陽的新娘](../Page/太陽的新娘.md "wikilink")》飾演 李藝蓮
  - 2013年：[JTBC](../Page/JTBC.md "wikilink")《[宮中殘酷史－花的戰爭](../Page/宮中殘酷史－花的戰爭.md "wikilink")》飾演
    承恩尚宮李氏
  - 2014年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[命中注定我愛你](../Page/命中注定我愛你_\(韓國電視劇\).md "wikilink")》飾演
    Miss金
  - 2017年：KBS《[我男人的秘密](../Page/我男人的秘密.md "wikilink")》飾演 奇黛拉

### MV

  - 2007年：Black Pearl《喜歡怎麼辦》
  - 2007年：Black Pearl《結果…還是你》
  - 2009年：慧玲《為什麼和我分手》

## 外部連結

  - [EPG](https://web.archive.org/web/20070331205049/http://epg.epg.co.kr/star/profile/index.asp?actor_id=15009)

  - [官方網站](http://www.atreemedia.com/menu20/menu3.html)

[Y](../Category/韓國電視演員.md "wikilink")