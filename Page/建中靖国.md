**建中靖国**（1101年）是[宋徽宗赵佶的](../Page/宋徽宗.md "wikilink")[年号](../Page/年号.md "wikilink")。[北宋使用这个年号共](../Page/北宋.md "wikilink")1年，“建中”意即“建立中正（不偏不倚）之道”、“靖国”指消除[党爭](../Page/党爭.md "wikilink")，使[社稷安定](../Page/社稷.md "wikilink")。

## 纪年

| 建中靖国                             | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1101年                          |
| [干支](../Page/干支纪年.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [明开](../Page/明开.md "wikilink")（1097年至1102年在位）：大理—段正淳之年號
      - [壽昌](../Page/壽昌.md "wikilink")（1095年正月至1101年正月）：遼—遼道宗耶律洪基之年號
      - [乾統](../Page/乾統.md "wikilink")（1101年二月至1110年十二月）：遼—[遼天祚帝耶律延禧之年號](../Page/遼天祚帝.md "wikilink")
      - [康和](../Page/康和.md "wikilink")（1099年八月二十八日至1104年二月十日）：日本[堀河天皇年号](../Page/堀河天皇.md "wikilink")
      - [龍符元化](../Page/龍符元化.md "wikilink")（1101年至1109年）：李朝—李乾德之年號
      - [貞觀](../Page/貞觀_\(西夏\).md "wikilink")（1101年正月至1113年十二月）：西夏—夏崇宗李乾順之年號

[Category:北宋年号](../Category/北宋年号.md "wikilink")
[Category:12世纪中国年号](../Category/12世纪中国年号.md "wikilink")
[Category:1100年代中国政治](../Category/1100年代中国政治.md "wikilink")
[Category:1101年](../Category/1101年.md "wikilink")