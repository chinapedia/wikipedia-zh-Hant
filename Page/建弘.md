**建弘**（420年正月—428年五月）是[十六國時期](../Page/十六國.md "wikilink")[西秦政權](../Page/西秦.md "wikilink")，太祖文昭王[乞伏熾磐的](../Page/乞伏熾磐.md "wikilink")[年號](../Page/年號.md "wikilink")，共計8年餘。

## 纪年

| 建弘                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 420年                           | 421年                           | 422年                           | 423年                           | 424年                           | 425年                           | 426年                           | 427年                           | 428年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元熙](../Page/元熙_\(晋恭帝\).md "wikilink")（419年正月-420年六月）：東晉皇帝[晋恭帝司马德文的年号](../Page/晋恭帝.md "wikilink")
      - [永初](../Page/永初_\(刘裕\).md "wikilink")（420年六月—423年十二月）：[南朝宋皇帝宋武帝](../Page/南朝宋.md "wikilink")[刘裕的年号](../Page/刘裕.md "wikilink")
      - [景平](../Page/景平.md "wikilink")（423年正月—424年八月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋少帝刘义符的年号](../Page/宋少帝.md "wikilink")
      - [元嘉](../Page/元嘉_\(南朝宋\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [太平](../Page/太平_\(北燕\).md "wikilink")（409年十月-430年十二月）：[北燕政权](../Page/北燕.md "wikilink")[冯跋年号](../Page/冯跋.md "wikilink")
      - [真兴](../Page/真兴.md "wikilink")（419年二月-425年七月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [承光](../Page/承光_\(夏国\).md "wikilink")（425年八月-428年二月）：[夏国政权](../Page/夏国.md "wikilink")[赫连昌年号](../Page/赫连昌.md "wikilink")
      - [勝光](../Page/胜光.md "wikilink")（428年二月-431年六月）：[夏国政权](../Page/夏国.md "wikilink")[赫连定年号](../Page/赫连定.md "wikilink")
      - [嘉兴](../Page/嘉兴_\(西凉\).md "wikilink")（417年二月-420年七月）：[西凉政权](../Page/西凉.md "wikilink")[李歆年号](../Page/李歆.md "wikilink")
      - [永建](../Page/永建_\(西凉\).md "wikilink")（420年十月-421年三月）：[西凉政权](../Page/西凉.md "wikilink")[李恂年号](../Page/李恂.md "wikilink")
      - [永安](../Page/永安_\(北凉\).md "wikilink")（401年六月-412年十月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [玄始](../Page/玄始.md "wikilink")（412年十一月-428年）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [泰常](../Page/泰常.md "wikilink")（416年四月-423年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏明元帝拓跋嗣年号](../Page/北魏明元帝.md "wikilink")
      - [始光](../Page/始光.md "wikilink")（424年正月-428年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [神䴥](../Page/神䴥.md "wikilink")（428年二月-431年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:西秦年号](../Category/西秦年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")