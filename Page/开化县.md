**开化县**是[中國](../Page/中國.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[衢州市下辖的一个县](../Page/衢州.md "wikilink")，位于[浙江省的最西部](../Page/浙江省.md "wikilink")，[钱塘江源头](../Page/钱塘江.md "wikilink")。面积2224平方千米，人口34万。邮政编码324300。县人民政府驻城关镇解放街54号。森林覆盖率达79.2%，是全国和浙江省林业重点县。

## 历史沿革

春秋时期属越国，战国时期属楚。 秦朝，属会稽郡太末县。

东汉初平三年（192），分太末置新安县（今衢江区），今开化地域为新安县的一部分。
建安二十三年（218），孙权分新安置定阳县（今常山县），开化属定阳县。
唐朝咸亨五年（674），置常山县，今开化地域属常山县。

[北宋](../Page/北宋.md "wikilink")[乾德四年](../Page/乾德_\(北宋\).md "wikilink")（966年），[吴越王](../Page/吴越国.md "wikilink")[钱弘俶分](../Page/钱弘俶.md "wikilink")[常山县开源](../Page/常山县.md "wikilink")、崇化、金水、玉田、石门、龙山、云台七乡置开化场，“开化”由开源、崇化二乡得名。[太平兴国六年](../Page/太平兴国.md "wikilink")（981年），升开化场为开化县，属[衢州](../Page/衢州.md "wikilink")。元、明、清三代相沿未变。

民国初年，属金华道。 民国16年（1927）废除道制，直属浙江省。 民国24年（1935）年，设衢州行政督察专员公署，开化属之。

1949年5月4日，开化县解放，属衢州专区。 1955年，衢州专区撤消，开化县属建德专区。 1958年12月，建德专区撤消，开化改属金华专区。
1985年5月，撤消金华专区分置金华市，衢州两省辖市，开化县属衢州市至今。

## 行政区划

下辖8个镇、6个乡：

  - 镇：[华埠镇](../Page/华埠镇.md "wikilink")、[马金镇](../Page/马金镇.md "wikilink")、[池淮镇](../Page/池淮镇.md "wikilink")、[村头镇](../Page/村头镇.md "wikilink")、[桐村镇](../Page/桐村镇.md "wikilink")、[杨林镇](../Page/杨林镇_\(开化县\).md "wikilink")、[苏庄镇和](../Page/苏庄镇.md "wikilink")[齐溪镇](../Page/齐溪镇.md "wikilink")。
  - 乡：[中村乡](../Page/中村乡_\(开化县\).md "wikilink")、[音坑乡](../Page/音坑乡.md "wikilink")、[林山乡](../Page/林山乡_\(开化县\).md "wikilink")、[长虹乡](../Page/长虹乡.md "wikilink")、[何田乡](../Page/何田乡.md "wikilink")、[大溪边乡](../Page/大溪边乡.md "wikilink")

## 交通

[201712_Kaihua_Station.jpg](https://zh.wikipedia.org/wiki/File:201712_Kaihua_Station.jpg "fig:201712_Kaihua_Station.jpg")[开化站](../Page/开化站.md "wikilink")\]\]

  - [205国道](../Page/205国道.md "wikilink")
  - G3高速（[京台高速公路](../Page/京台高速公路.md "wikilink")）黄衢南高速
  - [杭新景高速公路](../Page/杭新景高速公路.md "wikilink")
  - [衢九鐵路](../Page/衢九鐵路.md "wikilink")

## 风景名胜

  - [钱江源森林公园](../Page/钱江源森林公园.md "wikilink")
  - [古田山国家级自然保护区](../Page/古田山国家级自然保护区.md "wikilink")
  - 圣潭沟风景区
  - 南华山风景区
  - 南湖岛景区
  - 玉屏公园

## 历代名人

  - [詹旭刚](../Page/詹旭刚.md "wikilink")
  - [吴光韶](../Page/吴光韶.md "wikilink")，开化杀牛坑村人，民国总统府人事四科科长，[蘭封會戰任](../Page/蘭封會戰.md "wikilink")[桂永清部作战处长](../Page/桂永清.md "wikilink")。1967年逝于南京。著书《戎马遗痕》、《战时警察》等。
  - [吕公良](../Page/吕公良.md "wikilink")，民国中将,抗日战死于[许昌](../Page/许昌.md "wikilink")。

[Category:中国国家级生态市区县](../Category/中国国家级生态市区县.md "wikilink")
[开化县](../Category/开化县.md "wikilink")
[县](../Category/衢州区县市.md "wikilink")
[衢州市](../Category/浙江省县份.md "wikilink")