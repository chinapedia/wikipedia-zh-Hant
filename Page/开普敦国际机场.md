**開普敦國際機場**（）位于[南非](../Page/南非.md "wikilink")[开普敦](../Page/开普敦.md "wikilink")，是仅次于[约翰内斯堡国际机场的南非第二大机场](../Page/约翰内斯堡国际机场.md "wikilink")，也是[南非航空公司的枢纽机场](../Page/南非航空公司.md "wikilink")。在1990年代之前，机场使用的名称是**丹尼尔·弗朗索瓦·马兰机场**，以纪念前总理[丹尼尔·弗朗索瓦·马兰](../Page/丹尼尔·马兰.md "wikilink")。

## 航空公司與航點列表

**國內線**  **國際線**

[Category:南非機場](../Category/南非機場.md "wikilink")
[Category:開普敦建築物](../Category/開普敦建築物.md "wikilink")
[M](../Category/曾冠以人名的机场.md "wikilink")