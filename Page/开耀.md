**开耀**（681年九月—682年二月）是[唐高宗李治的年号](../Page/唐高宗.md "wikilink")。共计1年余。因太子李显嫡长子[李重润出生](../Page/李重润.md "wikilink")，唐高宗甚为喜悦，等到孙子满月，[大赦天下](../Page/大赦.md "wikilink")，改元为永淳。

## 大事记

## 出生

  - [李重润](../Page/李重润.md "wikilink")

## 逝世

## 纪年

| 开耀                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 681年                           | 682年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:680年代中国政治](../Category/680年代中国政治.md "wikilink")
[Category:681年](../Category/681年.md "wikilink")
[Category:682年](../Category/682年.md "wikilink")