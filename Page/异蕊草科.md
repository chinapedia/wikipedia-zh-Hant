**异蕊草科**共有15[属约](../Page/属.md "wikilink")180[种](../Page/种.md "wikilink")，主要分布在[东南亚](../Page/东南亚.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[美洲和](../Page/美洲.md "wikilink")[太平洋岛屿等区域](../Page/太平洋.md "wikilink")。

1981年的[克朗奎斯特分类法将其分入](../Page/克朗奎斯特分类法.md "wikilink")[百合科](../Page/百合科.md "wikilink")；1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将这些](../Page/APG_分类法.md "wikilink")[植物单独分出一个](../Page/植物.md "wikilink")[科](../Page/科.md "wikilink")，放在[天门冬目中](../Page/天门冬目.md "wikilink")；2003年经过修订的[APG
II
分类法认为可以选择性地和](../Page/APG_II_分类法.md "wikilink")[天门冬科合并](../Page/天门冬科.md "wikilink")。

## 部分属

  - [棘籽属](../Page/棘籽属.md "wikilink") *Acanthocarpus* Lehm.
  - [龙舌百合属](../Page/龙舌百合属.md "wikilink") *Arthropodium* R.Br.
  - *[Chamaescilla](../Page/Chamaescilla.md "wikilink")* F.Muell. ex
    Benth.
  - [朱蕉属](../Page/朱蕉属.md "wikilink") *Cordyline* Comm. ex R.Br.
  - *[Eustrephus](../Page/Eustrephus.md "wikilink")* R.Br.
  - *[Laxmannia](../Page/Laxmannia.md "wikilink")* R.Br.
  - *[Murchisonia](../Page/Murchisonia.md "wikilink")* Brittan
  - *[Sowerbaea](../Page/Sowerbaea.md "wikilink")* Sm.
  - [异蕊草属](../Page/异蕊草属.md "wikilink") *Thysanotus* R.Br.
  - *[Trichopetalum](../Page/Trichopetalum.md "wikilink")* Lindl.

## 外部链接

  - [NCBI分类中的异蕊草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=178389&lvl=3&lin=f&keep=1&srchmode=1&unlock)

[Category:异蕊草科](../Category/异蕊草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")