**弓斑石斑鱼**（[学名](../Page/学名.md "wikilink")：），又名**吊橋石斑魚**，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[鮨科的一個](../Page/鮨科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、中[太平洋等海域](../Page/太平洋.md "wikilink")。

## 深度

水深80至250公尺。

## 特徵

本魚體呈[黃褐色](../Page/黃褐色.md "wikilink")，無斑點。頭長約為體長1/3。眼小，短於吻長。口大；上下頜前端具小犬齒，兩側齒細尖，下頜約2列。體側有若干[深褐色斑塊](../Page/深褐色.md "wikilink")，或連接成縱帶，或不規則分布其上。各鰭顏色較體色稍黃，但均無斑點或斑塊。背鰭有硬棘11枚、軟棘14至15枚；臀鰭有硬棘3枚、軟條8枚，其中第二、第三硬棘約略等長。側線有孔鱗片數58至64枚。體長可達90公分。

## 生態

本魚主要棲息在水深100多公尺的深水礁石海岸，肉食性，以其他[魚類為食](../Page/魚類.md "wikilink")。

## 經濟利用

棲地較深，非重要的[食用魚](../Page/食用魚.md "wikilink")，肉味平庸，頭部宜煮沙鍋魚頭或[清湯](../Page/清湯.md "wikilink")，富含膠質。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[EM](../Category/IUCN無危物種.md "wikilink")
[EM](../Category/食用鱼.md "wikilink")
[morrhua](../Category/石斑魚屬.md "wikilink")