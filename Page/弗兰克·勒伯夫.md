**-{zh-hans:弗兰克·勒伯夫; zh-hk:法蘭·拿保夫;}-**(****，1968年1月22日 -
)，乃一名演員、體育評論員，曾為[法國職業足球員](../Page/法國.md "wikilink")，司職後衛，乃[1998年世界杯冠軍隊成員](../Page/1998年世界杯.md "wikilink")。

## 生平

勒伯夫出身於法國低組別聯賽球會，出道早年主要都是效力一些實力較次的[法甲下游球隊](../Page/法甲.md "wikilink")，如[勒阿弗爾和](../Page/Stade_Lavallois_Mayenne_FC.md "wikilink")[斯特拉斯堡](../Page/斯特拉斯堡足球俱乐部.md "wikilink")。直到1996年他轉投[英超球會](../Page/英超.md "wikilink")[車路士後](../Page/車路士.md "wikilink")，拿保夫才廣為人熟悉。他在車路士時雖然並沒有贏得英超聯賽冠軍，但也有冠軍取得，那是兩屆足總杯冠軍、一屆聯賽杯冠軍和1998年[歐洲杯賽冠軍杯冠軍](../Page/歐洲杯賽冠軍杯.md "wikilink")。

拿保夫於1998年世界杯入選世界杯大營，乃當屆冠軍隊成員之一，並於決賽上過陣。效力車路士期間，外界對於拿保夫的印象並不是在場上表現，而是在社交界方面，頻頻出席社交界活動，予人高高在上形象。

拿保夫於2001年離開車路士加盟[法甲球會](../Page/法甲.md "wikilink")[馬賽](../Page/马赛足球俱乐部.md "wikilink")，隨後轉投[卡塔爾聯賽](../Page/卡塔爾.md "wikilink")，於2005年宣告退役。

## 榮譽

  - 英格蘭足總杯：1997、2000
  - 歐洲杯賽冠軍杯：1998
  - 世界杯冠軍：1998
  - 歐洲國家杯冠軍：2000

[Category:1968年出生](../Category/1968年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:斯特拉斯堡球員](../Category/斯特拉斯堡球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:艾薩德球員](../Category/艾薩德球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:馬賽人](../Category/馬賽人.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2001年洲際國家盃球員](../Category/2001年洲際國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:洲際國家盃冠軍隊球員](../Category/洲際國家盃冠軍隊球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")