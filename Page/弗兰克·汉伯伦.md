**弗兰克·汉伯伦**（；\[1\]\[2\]），[美国大学](../Page/美国.md "wikilink")[篮球运动员和职业篮球教练](../Page/篮球.md "wikilink")，身高1.90米。

## NCAA球员生涯

汉伯伦在1960年代晚期的大学时期是[NCAA](../Page/NCAA.md "wikilink")[雪城大学的](../Page/雪城大学.md "wikilink")[后卫](../Page/后卫.md "wikilink")，1969年成为球队的队长。他在球场上的招牌动作是离地幅度很小的跳投，[罰球準度也有一定的水準](../Page/罰球_\(籃球\).md "wikilink")，但因為受傷的緣故，在三年级和四年级时逐渐失去了在球队中的先發位置。

## NBA教练生涯

大学毕业后，作为大学时期的杰出球员，汉伯伦很快成为了[NBA联盟](../Page/NBA.md "wikilink")[圣迭戈/休斯敦火箭队的球探](../Page/休斯敦火箭队.md "wikilink")。前后在NBA和[ABA联盟工作了](../Page/ABA.md "wikilink")35年，在[丹佛火箭队](../Page/丹佛金塊.md "wikilink")（1972-77）、[堪萨斯城国王队](../Page/堪萨斯城国王队.md "wikilink")（1978-1987）、[密尔沃基雄鹿队](../Page/密尔沃基雄鹿队.md "wikilink")（1988-1996）、[芝加哥公牛队](../Page/芝加哥公牛队.md "wikilink")（1996-1998）和[洛杉矶湖人队](../Page/洛杉矶湖人队.md "wikilink")（1999-至今）担任过助理教练的职务，并且曾赢得过1997年和1998年公牛队的[总冠军及](../Page/NBA總冠軍.md "wikilink")2000年、2001年和2002年湖人队的总冠军。

汉伯伦还曾两度成为NBA球队的临时过渡教练，分别是的密尔沃基雄鹿队和的洛杉矶湖人队，在雄鹿隊的期间共取得了23胜42负的执教成绩，湖人时期是10胜29负。此后他还多次出任NBA球队的助理教练一职，现为洛杉矶湖人队的助理教练。

## 參考資料

## 外部链接

  - [NBA.com coach
    profile](http://www.nba.com/coachfile/frank_hamblen/index.html?nav=page)
  - [OrangeHoops: Frank
    Hamblen](http://www.orangehoops.org/FHamblen.htm)
  - [BasketballReference: Frank
    Hamblen](http://www.basketball-reference.com/coaches/hamblfr99c.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:美国篮球教练](../Category/美国篮球教练.md "wikilink")
[Category:洛杉矶湖人队教练](../Category/洛杉矶湖人队教练.md "wikilink")
[Category:密尔沃基雄鹿队教练](../Category/密尔沃基雄鹿队教练.md "wikilink")

1.
2.  [Former NBA coach, Terre Haute native Frank Hamblen dies
    at 70](http://www.tribstar.com/news/former-nba-coach-terre-haute-native-frank-hamblen-dies/article_82469df4-a610-11e7-90fa-a3f675975187.html)