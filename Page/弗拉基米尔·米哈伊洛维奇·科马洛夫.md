[Soviet_Union-1964-stamp-Vladimir_Mikhailovich_Komarov.jpg](https://zh.wikipedia.org/wiki/File:Soviet_Union-1964-stamp-Vladimir_Mikhailovich_Komarov.jpg "fig:Soviet_Union-1964-stamp-Vladimir_Mikhailovich_Komarov.jpg")

**弗拉基米尔·米哈伊洛维奇·科马洛夫**（[俄语](../Page/俄语.md "wikilink"):****，）是一名[前苏联](../Page/前苏联.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")。他是第一名因载人航天遇难的苏联宇航员，也是第一个多次进入太空的苏联宇航员。

## 生平

科马洛夫生于[前苏联](../Page/前苏联.md "wikilink")（现[俄罗斯](../Page/俄罗斯.md "wikilink")）[莫斯科市](../Page/莫斯科.md "wikilink")。

科马洛夫于1960年入选苏联第一组宇航员，并曾作为[帕维尔·波波维奇的替补队员参与了](../Page/帕维尔·波波维奇.md "wikilink")[东方4号任务](../Page/东方4号.md "wikilink")。他本人第一次进入太空是在[上升1号任务](../Page/上升1号.md "wikilink")，而在他的第二次飞行任务[联盟1号中](../Page/联盟1号.md "wikilink")，他因飞船[降落伞故障而死于飞船坠毁](../Page/降落伞.md "wikilink")。

坠毁前，苏联总理[柯西金曾告诉科马洛夫](../Page/柯西金.md "wikilink")“祖国将以他为骄傲”。尽管有许多流言称科马洛夫死前一直在诅咒飞船设计者和飞行控制员，但据美国[国家安全局在](../Page/国家安全局.md "wikilink")[伊斯坦布尔的监听站报告称](../Page/伊斯坦布尔.md "wikilink")：科马洛夫的回复无法听清\[1\]。

科马洛夫的妻子是瓦伦缇娜·雅科夫勒夫纳·凯瑟尤娃，并有两个孩子：耶夫基尼和艾琳娜。

## 荣誉

1971年发现的[小行星1836科马洛夫以科马洛夫命名](../Page/小行星1836.md "wikilink")，同样以他名字命名的还有一个[月球上的](../Page/月球.md "wikilink")[环形山](../Page/环形山.md "wikilink")。受宇航员和[小行星的启发](../Page/小行星.md "wikilink")，作曲家[贝雷特·迪恩创作了一支名为](../Page/贝雷特·迪恩.md "wikilink")'科马洛夫的陨落'的[交响乐](../Page/交响乐.md "wikilink")，这支曲子于2006年由[西蒙·拉特尔指挥演出](../Page/西蒙·拉特尔.md "wikilink")，收录[EMI古典音乐专辑系列古斯塔夫](../Page/EMI.md "wikilink")·霍尔斯特的[行星组曲](../Page/行星组曲.md "wikilink")。

在众多荣誉中还包含一个于1969年建立在[卢布尔雅那的弗拉基米尔](../Page/卢布尔雅那.md "wikilink")·米哈伊洛维奇·科马洛夫宇航火箭技术俱乐部
（ARK）。

[国际宇航联合会颁发的](../Page/国际宇航联合会.md "wikilink")*科马洛夫奖*是以他的名字命名的。

## 参见

  - [阿波罗1号](../Page/阿波罗1号.md "wikilink")

## 参考资料

## 外部链接

  - [ARK Vladimir M. Komarov](http://www2.arnes.si/~ljarkvmk5/)
  - [Komarov](https://web.archive.org/web/20040815072907/http://www.astronautix.com/astros/komarov.htm)详细传记[Encyclopedia
    Astronautica](http://www.astronautix.com)
  - [BBC "On this day" 1967: Russian cosmonaut dies in space
    crash](http://news.bbc.co.uk/onthisday/hi/dates/stories/april/24/newsid_2523000/2523019.stm)

[Category:苏联英雄](../Category/苏联英雄.md "wikilink")
[Category:蘇聯太空人](../Category/蘇聯太空人.md "wikilink")
[Category:蘇聯空軍軍官](../Category/蘇聯空軍軍官.md "wikilink")
[Category:蘇聯空難身亡者](../Category/蘇聯空難身亡者.md "wikilink")
[Category:莫斯科人](../Category/莫斯科人.md "wikilink")
[Category:安葬於克里姆林宮紅場墓園者](../Category/安葬於克里姆林宮紅場墓園者.md "wikilink")

1.