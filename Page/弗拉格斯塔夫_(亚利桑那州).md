**弗拉格斯塔夫**（；莫哈維語：*Kinłání*），又譯**旗手市**，是[美國](../Page/美國.md "wikilink")[亞利桑那州的一座城市](../Page/亞利桑那州.md "wikilink")，位於[科羅拉多高原南緣](../Page/科羅拉多高原.md "wikilink")。传说是很久以前为纪念一群支持美利坚星条旗人物，人们直接把一棵大彬树就地去枝叶，升上大旗。这旗杆（树干）附近就称为旗手镇。\[1\]2012年人口67,468\[2\]。该市为[可可尼諾縣縣治](../Page/可可尼諾縣.md "wikilink")。

是[北亞利桑那大學和](../Page/北亞利桑那大學.md "wikilink")[羅威爾天文台的所在地](../Page/羅威爾天文台.md "wikilink")。2001年10月，[國際暗天協會將旗桿市認證為世界第一個](../Page/國際暗天協會.md "wikilink")「國際暗天社區」（International
Dark-Sky Community, IDSC）\[3\]，本城亦因此獲得「暗天之城」的綽號。\[4\]

## 姐妹城市\[5\]

  - （[台灣](../Page/台灣.md "wikilink")）[新北市](../Page/新北市.md "wikilink")[新店區](../Page/新店區.md "wikilink")

  - [巴爾瑙爾](../Page/巴爾瑙爾.md "wikilink")

  - [藍山市](../Page/藍山市.md "wikilink")

  - [曼薩尼約](../Page/曼薩尼約.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:亞利桑那州城市](../Category/亞利桑那州城市.md "wikilink") [Category:可可尼諾縣城市
(亞利桑那州)](../Category/可可尼諾縣城市_\(亞利桑那州\).md "wikilink")
[Category:亞利桑那州縣城](../Category/亞利桑那州縣城.md "wikilink")
[Category:1876年建立的聚居地](../Category/1876年建立的聚居地.md "wikilink")

1.  "[Biotic Communities of the Colorado
    Plateau](http://www.cpluhna.nau.edu/Biota/ponderosa_forest.htm) ."
    *[Northern Arizona
    University](../Page/Northern_Arizona_University.md "wikilink").*
    Retrieved on March 2, 2007.
2.  "[Flagstaff city, Arizona: Population
    Finder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&_name=Flagstaff&_state=04000US04&_county=Flagstaff&_cityTown=Flagstaff&_zip=&_sse=on&_lang=en&pctxt=fph)."
    *[United States Census
    Bureau](../Page/United_States_Census_Bureau.md "wikilink").*
    Retrieved on July 18, 2007.
3.
4.
5.  [Flagstaff Sister
    Cities](http://www.flagstaffsistercities.net/flagsiscities.htm)