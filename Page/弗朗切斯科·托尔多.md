**弗朗切斯科·托尔多**（**Francesco
Toldo**，），退役[意大利](../Page/意大利.md "wikilink")[足球運動員](../Page/足球.md "wikilink")，司職[守門員](../Page/守門員.md "wikilink")，退役前效力[意甲班霸球會](../Page/意大利甲組足球聯賽.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")。

## 俱樂部生涯

托度原本是[AC米蘭青年軍成員](../Page/AC米蘭.md "wikilink")，但未曾替[AC米蘭一隊上陣](../Page/AC米蘭.md "wikilink")。之前曾效力一些低組別球會，自1993年開始效力費倫天拿，差不多達十年。2001年他以1800萬鎊轉會費加盟國際米蘭。2002年10月21日，托度在最後時刻打進一球，幫助國際米蘭主場1比1戰平[祖雲達斯](../Page/祖雲達斯.md "wikilink")\[1\]。2005年7月，[儒利奧·塞薩爾加盟國際米蘭](../Page/儒利奧·塞薩爾.md "wikilink")，托度成為球隊後備球員。2009年3月3日，托度和國際米蘭續約至2011年，這意味著他將會在國際米蘭退休\[2\]。其後在2010年7月7日，托度宣布不再上陣，轉到足球學校任教練。

## 國家隊生涯

托度於1995年10月8日對陣[克羅地亞時開始入選國家隊](../Page/克羅地亞.md "wikilink")，當時因為[安杰洛·佩鲁齐受傷而且比賽期間](../Page/安杰洛·佩鲁齐.md "wikilink")[布基拿了紅牌而得到上陣機會](../Page/布基.md "wikilink")。[2000年歐洲國家盃他代替當時受傷的](../Page/2000年歐洲國家盃.md "wikilink")[保方擔任](../Page/保方.md "wikilink")[意大利正選守門員](../Page/意大利國家足球隊.md "wikilink")，出奇地在四強賽對陣[荷蘭中三救十二碼成功](../Page/荷蘭國家足球隊.md "wikilink")，帶領國家隊闖入決賽。

托度自[2004年歐洲國家盃後已退出國家隊](../Page/2004年歐洲國家盃.md "wikilink")，他表示希望抽空陪伴家人，無意重返國家隊。

托度能力不差，甚至比起歐洲其他國家的守門員更為出色，但奈何保方比托度更為出眾，令他在國家隊少有上場機會。可謂最生不逢時的出色門將。

## 參考資料

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:義大利國家足球隊球員](../Category/義大利國家足球隊球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")

1.
2.