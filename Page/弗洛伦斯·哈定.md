**弗洛伦斯·梅布尔·“弗洛西”·克林·哈丁**（**Florence Mabel "Flossie" Kling
Harding**，），[美國第](../Page/美國.md "wikilink")29任[总统](../Page/美国总统.md "wikilink")[沃伦·哈定的妻子](../Page/沃伦·哈定.md "wikilink")，[美国前第一夫人](../Page/美国第一夫人.md "wikilink")（1921年-1923年）。

本名弗洛伦斯·梅布尔·“弗洛西”·克林（Florence Mabel "Flossie"
Kling），年輕時與鄰家男生尤金·德沃尔夫（Eugene
deWolfe）[私奔後](../Page/私奔.md "wikilink")，曾隨丈夫姓德沃尔夫，但兩人一起不久已以離婚收場。

[Category:美国第一夫人](../Category/美国第一夫人.md "wikilink")
[Category:俄亥俄州共和党人](../Category/俄亥俄州共和党人.md "wikilink")