**弗里德利布·費迪南德·龍格**（，），[德國](../Page/德國.md "wikilink")[分析化學家](../Page/分析化學.md "wikilink")。1819年在[歌德的鼓励下](../Page/歌德.md "wikilink")，首次提煉了[咖啡因](../Page/咖啡因.md "wikilink")。他也確認了[顛茄提取物的](../Page/顛茄.md "wikilink")[瞳孔放大](../Page/瞳孔放大.md "wikilink")（散瞳）效果，並發現了第一種[煤焦油染料](../Page/煤焦油.md "wikilink")（[苯胺藍](../Page/苯胺藍.md "wikilink")）。

## 參考資料

  - Weinberg BA, Bealer BK. The world of caffeine. New York & London:
    Routledge, 2001. .

  -
  -
## 延伸閱讀

  - Anft, Berthold, with R. E. Oesper, trans. (1955) "Friedlieb
    Ferdinand Runge: A forgotten chemist of the nineteenth century,"
    *Journal of Chemical Education*, **32** : 566–574.
  - Anft, Bertold, *Friedlieb Ferdinand Runge: sein Leben und sein Werk*
    \[Friedlieb Ferdinand Runge: his life and his work\] (Berlin,
    Germany: Dr. Emil Ebering, 1937).

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")
[Category:布雷斯勞大學教師](../Category/布雷斯勞大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:耶拿大學校友](../Category/耶拿大學校友.md "wikilink")
[Category:漢堡人](../Category/漢堡人.md "wikilink")