**弗里德里希·克里斯蒂安·阿尔贝特·利奥波德·阿诺·西尔维斯特·马卡里乌斯**（Friedrich Christian Albert
Leopold Anno Sylvester
Macarius，），[萨克森公爵](../Page/萨克森.md "wikilink")，迈森藩侯，[韦廷家族阿贝丁系族长](../Page/韦廷王朝.md "wikilink")。1893年12月31日生于[萨克森首都](../Page/萨克森.md "wikilink")[德累斯顿](../Page/德累斯顿.md "wikilink")，[萨克森国王](../Page/萨克森统治者列表.md "wikilink")[弗里德里希·奥古斯特三世与](../Page/弗里德里希·奥古斯特三世_\(萨克森国王\).md "wikilink")[托斯卡纳公主路易丝的次子](../Page/托斯卡纳.md "wikilink")。

父亲弗里德里希·奥古斯特三世在1918年被推翻，于1932年去世。由于长兄[格奥尔格已于](../Page/格奥尔格_\(萨克森王储\).md "wikilink")1923年放弃王位继承权，他成为[萨克森王位继承人](../Page/萨克森统治者列表.md "wikilink")，[韦廷家族阿贝丁系族长](../Page/韦廷王朝.md "wikilink")（Chef
der albertinischen Linie des Hauses Wettin），称迈森藩侯弗里德里希·克里斯蒂安（Friedrich
Christian Markgraf zu Meißen）。

1923年，他与[图恩和塔克西斯亲王](../Page/图恩和塔克西斯.md "wikilink")[阿尔贝特之女伊丽莎白](../Page/阿尔贝特一世_\(图恩和塔克西斯\).md "wikilink")·海伦（Elisabeth
Helene）结婚，有二子三女。

弗里德里希·克里斯蒂安王子于1968年8月9日在[瑞士去世](../Page/瑞士.md "wikilink")，终年75岁。长子[玛利亚·埃曼努埃尔成为家族族长](../Page/玛利亚·埃曼努埃尔.md "wikilink")。

[Category:萨克森王室](../Category/萨克森王室.md "wikilink")
[Category:德国君主儿子](../Category/德国君主儿子.md "wikilink")
[Category:王位覬覦者](../Category/王位覬覦者.md "wikilink")
[Category:死在瑞士的外国人](../Category/死在瑞士的外国人.md "wikilink")
[Category:德勒斯登人](../Category/德勒斯登人.md "wikilink")