**弗里西語**（Frysk、Frasch、Fresk或Friisk，中文又譯**弗里斯語**、**弗里斯蘭語**、**夫里斯蘭語**）是[荷蘭及](../Page/荷蘭.md "wikilink")[德國内靠近](../Page/德國.md "wikilink")[北海南部處一族人所使用的](../Page/北海_\(大西洋\).md "wikilink")[語言](../Page/語言.md "wikilink")，使用人數大約在40万左右。弗里西語屬於[日耳曼語族](../Page/日耳曼語族.md "wikilink")，族人在北歐歷史上曾十分活躍，在[維京時代作為](../Page/維京人.md "wikilink")[商人和](../Page/商人.md "wikilink")[海盜很有名](../Page/海盜.md "wikilink")。

下分三支，[西弗里西語](../Page/西弗里西語.md "wikilink")（West Lauwers
Frisian，本地語Frysk）、[東弗里西語](../Page/東弗里西語.md "wikilink")（[沙特弗里西語](../Page/沙特弗里西語.md "wikilink")，本地語Seeltersk）和[北弗里西語](../Page/北弗里西語.md "wikilink")。北弗里西語下又分幾種獨特的口語形式。

## 分佈

大多數使用者居住在[荷蘭的](../Page/荷蘭.md "wikilink")[菲士蘭省](../Page/菲士蘭省.md "wikilink")，約35万人。也有越來越多的居住在此省的其他荷蘭人能夠掌握這種語言。[德國内](../Page/德國.md "wikilink")，有大約2000名使用者居住在[下薩克森州的](../Page/下薩克森州.md "wikilink")[薩特蘭地區](../Page/薩特蘭.md "wikilink")。由於此地區周圍多是沼澤，也保護了此語言沒有受到低地德語和高地德語的衝擊。

德国[石勒苏益格-荷尔斯泰因州的](../Page/石勒苏益格-荷尔斯泰因州.md "wikilink")[北弗里斯兰地區内](../Page/北弗里斯蘭縣.md "wikilink")，也有約一萬名使用者仍在運用一些[方言](../Page/方言.md "wikilink")。

弗里西語在欧盟作為“少數民族語言”受到保護；在荷蘭是兩种官方語言之一。

## 發展

古弗里西語和[古英語極為相像](../Page/古英語.md "wikilink")，而且傳統上也視作除[蘇格蘭語外](../Page/低地苏格兰语.md "wikilink")，和英語最接近的語言。中世紀發生的[北海日耳曼語鼻音消失現象標誌着英語和弗里西語等](../Page/北海日耳曼語鼻音消失法則.md "wikilink")[盎格鲁-弗里西语组語言跟其他](../Page/盎格鲁-弗里西语组.md "wikilink")[西日耳曼語的分離](../Page/西日耳曼語.md "wikilink")。不過這種相似是基於早期書面語來判斷的，經過數百年的發展，二者已經互相差距很遠。如今二者之間完全不能互相交流，其中一部分原因是弗里西語受到低地法蘭克語（如荷蘭語）和低地德語的影響。

英语之所以和弗里西语相近，就是因为盎格鲁-弗里西语组的二部落文化语言上的相近（当然是与薩克遜等其他部落的西日耳曼语相比较而言）。西日耳曼语支除卻了[意第緒語組及](../Page/意第緒語組.md "wikilink")，可以分为[盎格鲁-弗里西语组](../Page/盎格鲁-弗里西语组.md "wikilink")、
[高地德語組](../Page/高地德語.md "wikilink")、[低地德語組](../Page/低地德語.md "wikilink")。高地德語組又分为上德语、今日的标准德语（以中古高地德语为标准发展起来的）、[意第绪语](../Page/意第绪语.md "wikilink")、
[高地法蘭克語](../Page/高地法蘭克語.md "wikilink")；低地德語組分为
[低地法蘭克語](../Page/低地法蘭克語.md "wikilink")，包括荷兰语及其诸方言、低地德語組，包括低地撒克遜語和[下德語](../Page/下德語.md "wikilink")。

政治上弗里斯兰地區是荷蘭的一部分，因此弗里西語也和荷蘭語越來越近。原來荷蘭北部西弗里斯兰地區的方言已經滅絕，但一些荷蘭方言仍然帶有很深的西弗里西語的痕跡。

已經確認的最早的弗里西語書面文件出現在大約9世紀；可能在之前也使用過[如尼文字](../Page/如尼文字.md "wikilink")。

## 參考文獻

  - [Omniglot links to various Frisian
    resources](http://www.omniglot.com/writing/frisian.htm)
  - [Tresoar - Historical and Literary Centre of Friesland Province of
    the Netherlands](http://www.tresoar.nl/)

## 外部連結

  - [弗英字典](https://web.archive.org/web/20050405213727/http://www.websters-online-dictionary.org/definition/Frisian-english/)

  - [弗里西語介紹](https://web.archive.org/web/20050412180706/http://lamar.colostate.edu/~eric13/frisian.shtml)

  - [Frisian academy for people, language and
    culture](http://www.fa.knaw.nl/)

  - [The Frisian foundation](http://www.ferring-stiftung.org/)

  - [Ferring Stiftung, a foundation from North
    Frisia](http://www.ferring-stiftung.org/)

  - [West-Frisian-English
    dictionary](http://www.dicts.info/dictionary.php?l1=English&l2=Frisian)

  - [\[PDF](http://scans.library.utoronto.ca/pdf/2/14/frisianlanguagel00heweuoft/frisianlanguagel00heweuoft.pdf)Hewett,
    Waterman Thomas, The Frisian language and literature\]

  - ['Hover & Hear' West Frisian
    pronunciations](http://www.languagesandpeoples.com/Eng/Direct/Germanic/SglLgWFrisianGrou.htm),
    and compare with equivalents in English and other Germanic
    languages.

  -  

  - [Radio in West Frisian](http://www.omropfryslan.nl/)

  - [Radio news in North Frisian](http://friiskforiining.podspot.de/)

[Category:西日耳曼语支](../Category/西日耳曼语支.md "wikilink")
[Category:荷蘭語言](../Category/荷蘭語言.md "wikilink")