**弗雷德里克·巴兰坦**（**Frederick Ballantyne**，1936年7月5日 -
），[圣迈克尔和圣乔治大十字勋章](../Page/圣迈克尔和圣乔治大十字勋章.md "wikilink")，现任[圣文森特和格林纳丁斯总督](../Page/圣文森特和格林纳丁斯总督.md "wikilink")。

[Category:1936年出生](../Category/1936年出生.md "wikilink")
[Category:圣文森特和格林纳丁斯总督](../Category/圣文森特和格林纳丁斯总督.md "wikilink")
[Category:現任國家領導人](../Category/現任國家領導人.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:聖米迦勒及聖喬治勳章爵級大十字勳章持有人](../Category/聖米迦勒及聖喬治勳章爵級大十字勳章持有人.md "wikilink")