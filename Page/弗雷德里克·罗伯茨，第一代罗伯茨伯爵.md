[Lord_roberts_of_kandahar.jpg](https://zh.wikipedia.org/wiki/File:Lord_roberts_of_kandahar.jpg "fig:Lord_roberts_of_kandahar.jpg")
**弗雷德里克·斯雷·罗伯茨，第一代羅伯茨伯爵**（**Frederick Sleigh Roberts, 1st Earl
Roberts**，），VC, KG, KP, GCB, OM, GCSI, GCIE, KStJ, VD,
PC，是一位[英国陆军元帅](../Page/英国陆军元帅.md "wikilink")，英国著名的军事家，[维多利亚女王时代的最成功的指挥官当中的一个](../Page/维多利亚女王.md "wikilink")。先后参加了[阿富汗战争](../Page/阿富汗战争.md "wikilink")，[布尔战争](../Page/布尔战争.md "wikilink")，并曾经担任英国陆军总司令。由于功勋卓著被授予[维多利亚十字勋章和](../Page/维多利亚十字勋章.md "wikilink")[嘉德勋章](../Page/嘉德勋章.md "wikilink")。

## 生平

1832年出生于[印度的](../Page/印度.md "wikilink")[坎普尔](../Page/坎普尔.md "wikilink")，父亲是[亚伯拉罕·罗伯茨将军](../Page/亚伯拉罕·罗伯茨.md "wikilink")。\[1\]罗伯茨先后在[伊顿公学](../Page/伊顿公学.md "wikilink")、[桑赫斯特皇家军事学院和阿第斯康比](../Page/桑赫斯特皇家军事学院.md "wikilink")（Addiscombe，设在阿第斯康比的东印度公司辖下的军事学院）接受过教育。在进入英国的印度军队之前是[孟加拉炮兵少尉](../Page/孟加拉.md "wikilink")。他参加镇压印度叛乱，于1858年1月被授予维多利亚十字勋章。
[Field_Marshall_Lord_Roberts.jpg](https://zh.wikipedia.org/wiki/File:Field_Marshall_Lord_Roberts.jpg "fig:Field_Marshall_Lord_Roberts.jpg")

## 参考来源

## 外部链接

  -
  -
  - [Lord Roberts' British
    Honours](https://web.archive.org/web/20110715084506/http://www.pinetreeweb.com/roberts-honours.htm)

  - [Account of Earl Roberts'
    funeral](http://www.garenewing.co.uk/angloafghanwar/articles/roberts_funeral.php)

  - [Frederick Roberts and the long road to
    Kandahar](http://www.historicaleye.com/Roberts.html)

[Category:英国陆军元帥](../Category/英国陆军元帥.md "wikilink")
[Category:聯合王國伯爵](../Category/聯合王國伯爵.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:伊顿公学校友](../Category/伊顿公学校友.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:安葬于圣保罗座堂者](../Category/安葬于圣保罗座堂者.md "wikilink")

1.