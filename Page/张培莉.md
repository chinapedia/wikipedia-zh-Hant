**张培莉**（1941年—），[温家宝夫人](../Page/温家宝.md "wikilink")，籍贯浙江[嘉兴](../Page/嘉兴.md "wikilink")[海盐县](../Page/海盐县.md "wikilink")，生于[甘肃](../Page/甘肃.md "wikilink")[兰州](../Page/兰州.md "wikilink")。

张培莉曾就读于兰州大学地质地理系毕业，后为中国著名地质学家；现为中国珠宝协会副主席、中国宝玉石鉴定委员会主任、[国家珠宝玉石质量监督检验中心主任等职务](../Page/国家珠宝玉石质量监督检验中心.md "wikilink")\[1\]。

## 温家宝夫人

张培莉比[温家宝大一岁](../Page/温家宝.md "wikilink")，两人最初相识于[温家宝在](../Page/温家宝.md "wikilink")[甘肃工作期间](../Page/甘肃.md "wikilink")。

[温家宝夫妇育有一子](../Page/温家宝.md "wikilink")[温云松](../Page/温云松.md "wikilink")，儿媳杨小萌；及一女[温如春](../Page/温如春.md "wikilink")，女婿刘春航。

## 奢华珠宝新闻

2007年11月2日，境外媒体[中国时报报道温家宝妻子张培莉酷爱珠宝](../Page/中国时报.md "wikilink")，曾向台湾珠宝商买珠宝，一出手就是单价6524万元新台币一条的宝石项链（合人民币1400万元）。\[2\]台灣中天新聞亦有類似报道，视频链接，但該次報導模糊隱諱。\[3\]

对此，温家宝无明确回应。与[温家宝平日苦心维持的](../Page/温家宝.md "wikilink")“一件羽绒服穿十年、视察民间时候穿平价旅游鞋”的节俭朴素形象相比，张培莉显然奢华享受，因此有些人挪揄温家宝是“影帝”。

## 参见

  - [温家宝](../Page/温家宝.md "wikilink")、[温云松](../Page/温云松.md "wikilink")
  - [温家宝家族财富被曝光事件](../Page/温家宝家族财富被曝光事件.md "wikilink")

## 参考资料

{{-}}

[category:在世人物](../Page/category:在世人物.md "wikilink")
[Z](../Page/category:兰州人.md "wikilink")
[Z](../Page/category:海盐人.md "wikilink")
[Z](../Page/category:中华人民共和国领导人配偶.md "wikilink")

[Z](../Category/1941年出生.md "wikilink")
[Category:温家宝家族](../Category/温家宝家族.md "wikilink")
[Z](../Category/中华人民共和国国务院总理夫人.md "wikilink")
[P](../Category/張姓.md "wikilink")

1.  [温夫人张培莉](http://www.kexuejia.com/news/news_view.asp?newsid=1333)
2.  [中国时报：温家宝夫人爱珠宝出手破千万
    台北：中国时报， 2007-11-2](http://society.dwnews.com/big5/news/2007-11-03/3437827.html)

3.  [台湾中天新闻报道视频](http://www.youtube.com/watch?v=aHmnzqAgimE)