**张家口市**（[汉语拼音](../Page/汉语拼音.md "wikilink")：Zhāngjiākǒu
Shì，[邮政式拼音](../Page/邮政式拼音.md "wikilink")：Kalgan\[1\]）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省下辖的](../Page/河北省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于河北省西北部，跨东经113度50分至116度30分、北纬39度30分至42度10分，南北最大距离近300公里、东西最大距离约228公里，是[京包线上重要的城市之一](../Page/京包铁路.md "wikilink")，也是[京张高速公路的终点](../Page/京张高速公路.md "wikilink")，是河北与[内蒙古的交通要冲](../Page/内蒙古自治区.md "wikilink")，是连通中国西北、[蒙古和](../Page/蒙古.md "wikilink")[北京的重要通道和货物集散地](../Page/北京.md "wikilink")、军事要地与陆路商埠。著名的[大境门是](../Page/大境门.md "wikilink")[长城的主要关口](../Page/长城.md "wikilink")。

张家口自古以来就是“屏翰神京”的军事重镇，是兵家必争之地，俗称京都的“北大门”。在经济上，曾是中国北方汉蒙贸易的商业都会，是冀、京、晋、内蒙古的交通枢纽和物资集散地，有“陆路商埠”之称。中国[改革开放后](../Page/改革开放.md "wikilink")，随着开放型经济的发展，张家口成为沟通中原与北疆，连接东部京津经济区，环渤海经济区与中西部资源产区的重要纽带\[2\]。

2015年7月31日，在[馬來西亞首都](../Page/馬來西亞.md "wikilink")[吉隆坡舉行的第](../Page/吉隆坡.md "wikilink")128屆[國際奧委會全體會議上](../Page/国际奥林匹克委员会.md "wikilink")，奧委會決定由[北京和張家口共同承辦](../Page/北京市.md "wikilink")2022年第二十四屆[冬季奧林匹克運動會](../Page/冬季奥林匹克运动会.md "wikilink")\[3\]。

## 名称来源

[Kalgan_1698.jpg](https://zh.wikipedia.org/wiki/File:Kalgan_1698.jpg "fig:Kalgan_1698.jpg")张家口旧名张垣，别称[塞外山城](../Page/塞外.md "wikilink")、武城。

张家口在历史上以Kalgan（）广为欧洲人所知，一直到20世纪中叶。\[4\]。该名字来源于它的[蒙古语名称](../Page/蒙古语.md "wikilink")
[链接=<https://en.wikipedia.org/wiki/File:Cighulaltu_qaghalgha.svg>](https://zh.wikipedia.org/wiki/File:Cighulaltu_qaghalgha.svg "fig:链接=https://en.wikipedia.org/wiki/File:Cighulaltu_qaghalgha.svg")（Chuulalt
Khaalga）或简写
[链接=<https://en.wikipedia.org/wiki/File:Qaghalghan.svg>](https://zh.wikipedia.org/wiki/File:Qaghalghan.svg "fig:链接=https://en.wikipedia.org/wiki/File:Qaghalghan.svg")（Khaalgan），意为大门（在长城上的）。在[历史上中国东北地区](../Page/满洲.md "wikilink")，又以[满语名](../Page/满语.md "wikilink")
[链接=<https://en.wikipedia.org/wiki/File:Imiyangga_Jase.svg>](https://zh.wikipedia.org/wiki/File:Imiyangga_Jase.svg "fig:链接=https://en.wikipedia.org/wiki/File:Imiyangga_Jase.svg")（Imiyangga
Jase）闻名。

张家口的发源地是现位于桥西区的堡子里一带，这里的发展是整个张家口逐步繁荣的历史见证。而张家口名称的由来也和这里有密切的关系。
堡子里（亦称下堡），[明代时属京师宣府镇](../Page/明代.md "wikilink")，为万全右卫地。[宣德四年](../Page/宣德.md "wikilink")（公元1429年），指挥使张文始筑城堡，名张家堡。张家堡高三丈三尺，方四里十三步，东南各开一门，东曰“永镇门”，南曰“承恩门”。[嘉靖八年](../Page/嘉靖.md "wikilink")（公元1529年）守备[张珍在北城墙开一小门](../Page/张珍.md "wikilink")，曰“小北门”，因门小如口，又由张珍开筑，所以称“张家口”。

## 历史

公元前约2500年[黄帝与](../Page/黄帝.md "wikilink")[炎帝为争做部落霸主发生矛盾并爆发冲突](../Page/炎帝.md "wikilink")，在[阪泉](../Page/阪泉.md "wikilink")（今[怀来](../Page/怀来.md "wikilink")）一带大战，炎帝败，史称[阪泉之战](../Page/阪泉之战.md "wikilink")。阪泉之战后不久，[有熊氏](../Page/有熊氏.md "wikilink")[黄帝与](../Page/黄帝.md "wikilink")[神农氏](../Page/神农氏.md "wikilink")[炎帝两族联合](../Page/炎帝.md "wikilink")，同[蚩尤](../Page/蚩尤.md "wikilink")[九黎族在](../Page/九黎族.md "wikilink")[涿鹿](../Page/涿鹿.md "wikilink")（今河北省[涿鹿县](../Page/涿鹿县.md "wikilink")）进行的一次大规模战争，史称[涿鹿之战](../Page/涿鹿之战.md "wikilink")。

[春秋时北为](../Page/春秋.md "wikilink")[匈奴与](../Page/匈奴.md "wikilink")[东胡居住地](../Page/东胡.md "wikilink")，南部分属[燕国](../Page/燕国.md "wikilink")、[代国](../Page/代_\(春秋\).md "wikilink")，公元前475年，代国为[赵襄子所灭](../Page/赵襄子.md "wikilink")。公元前228年，秦将王翦破赵军，攻占邯郸，赵国灭亡。赵国公子[嘉逃到代](../Page/代王嘉.md "wikilink")（今蔚县东北），自立为代王。代王嘉联合燕王喜抵抗秦军，燕代联军由燕太子丹统领，败于易水。前222年，秦将王贲攻辽东，掳燕王喜，燕国灭亡，王贲转攻代郡，掳代王嘉，赵彻底灭亡。[秦朝时南部改属](../Page/秦朝.md "wikilink")[代郡](../Page/代郡.md "wikilink")、[上谷郡](../Page/上谷郡.md "wikilink")。[汉朝时大部分属](../Page/汉朝.md "wikilink")[幽州地界](../Page/幽州.md "wikilink")，小部分属[乌桓](../Page/乌桓.md "wikilink")、[匈奴](../Page/匈奴.md "wikilink")、[鲜卑](../Page/鲜卑.md "wikilink")。

[北魏太和年间](../Page/北魏.md "wikilink")（477年至499年）设御夷（今[赤城](../Page/赤城.md "wikilink")、[沽源一带](../Page/沽源.md "wikilink")）、怀荒（今[张北一带](../Page/张北.md "wikilink")）、柔玄（今[尚义一带](../Page/尚义.md "wikilink")）等六镇，以防止[柔然入侵](../Page/柔然.md "wikilink")，保卫北魏首都[平城安全](../Page/平城.md "wikilink")。[北魏正光四年](../Page/北魏.md "wikilink")（523年），怀荒镇（今张北）民众在最高首长于景不愿发放粮食赈济饥荒后将其杀害，并发起反抗运动，[六镇爆发起义](../Page/六镇起义.md "wikilink")，[关陇](../Page/關隴集團.md "wikilink")、[河北等地各族民众纷纷起兵响应](../Page/河北.md "wikilink")，北魏统治濒临崩溃。边镇豪强乘机扩充实力，其中以[尔朱荣实力最强](../Page/尔朱荣.md "wikilink")。西魏废帝元年正月（552年）[土门发兵攻击](../Page/土门.md "wikilink")[柔然](../Page/柔然.md "wikilink")，大破柔然军于怀荒镇北面，[阿那瓌因而自杀](../Page/阿那瓌.md "wikilink")。

[隋朝时东为](../Page/隋朝.md "wikilink")[涿郡](../Page/涿郡.md "wikilink")，西属[雁门郡](../Page/雁门郡.md "wikilink")。[唐朝时北属](../Page/唐朝.md "wikilink")[突厥地](../Page/突厥.md "wikilink")，[桑干都督府](../Page/桑干都督府.md "wikilink")，南多属[河北道](../Page/河北道.md "wikilink")[妫州](../Page/妫州.md "wikilink")、[新州](../Page/新州.md "wikilink")、少属[河北道](../Page/河北道.md "wikilink")[蔚州](../Page/蔚州.md "wikilink")。

[五代十国](../Page/五代十国.md "wikilink")[后晋天福元年](../Page/后晋.md "wikilink")（936年）后晋首任皇帝[石敬瑭割让](../Page/石敬瑭.md "wikilink")[燕云十六州于](../Page/燕云十六州.md "wikilink")[辽](../Page/辽.md "wikilink")，包括新（今河北涿鹿）、妫（原属河北[怀来](../Page/怀来.md "wikilink")，今为[官厅水库库区](../Page/官厅水库.md "wikilink")）、武（今河北[宣化](../Page/宣化.md "wikilink")）、蔚（今河北[蔚县](../Page/蔚县.md "wikilink")）州，对之后的[宋朝打开了门户](../Page/宋朝.md "wikilink")，对[长江以北地区造成威胁](../Page/长江以北.md "wikilink")。[北宋时](../Page/北宋.md "wikilink")，皆属[辽之](../Page/辽朝.md "wikilink")[西京道](../Page/西京道.md "wikilink")，分为[武州](../Page/武州.md "wikilink")、[蔚州](../Page/蔚州.md "wikilink")、[奉圣州](../Page/奉圣州.md "wikilink")、[归化州](../Page/归化州.md "wikilink")、[儒州](../Page/儒州.md "wikilink")、[妫州地](../Page/妫州.md "wikilink")。辽天禄四年（951年）10月，辽世宗耶律阮在协助北汉攻打后周时，驻扎在火神淀（今宣化西），被属下耶律察割杀死，即[火神淀之乱](../Page/火神淀之乱.md "wikilink")。辽[萧太后](../Page/萧太后.md "wikilink")、辽圣宗[耶律隆绪时期在张家口地区建上](../Page/耶律隆绪.md "wikilink")、中、下三个花园，今[下花园因此而得名](../Page/下花园.md "wikilink")。[南宋时皆属](../Page/南宋.md "wikilink")[金之](../Page/金朝.md "wikilink")[西京路](../Page/西京路.md "wikilink")。

[`Bataille_entre_mongols_&_chinois_(1211).jpeg`](https://zh.wikipedia.org/wiki/File:Bataille_entre_mongols_&_chinois_\(1211\).jpeg "fig:Bataille_entre_mongols_&_chinois_(1211).jpeg")

宋元时期，1211年8月，蒙古与金朝在野狐岭（今张家口万全西北）爆发[决战](../Page/野狐岭战役.md "wikilink")，1213年在怀来再次发生战争，加速了金朝的灭亡。[大蒙古国](../Page/大蒙古国.md "wikilink")1251年，可汗[蒙哥之弟](../Page/蒙哥.md "wikilink")[忽必烈在](../Page/忽必烈.md "wikilink")[金莲川](../Page/金莲川.md "wikilink")（今沽源境内）建立[金莲川幕府](../Page/金莲川幕府.md "wikilink")，招揽了包括核心人物[刘秉忠在内的大批](../Page/刘秉忠.md "wikilink")[汉族谋士](../Page/汉族.md "wikilink")，为其出谋划策。[元朝时皆属](../Page/元朝.md "wikilink")[中书省](../Page/中书省.md "wikilink")[上都路](../Page/上都路.md "wikilink")[宣德府](../Page/宣德府.md "wikilink")，西北部属[兴和路](../Page/兴和路.md "wikilink")(治今[张北](../Page/张北.md "wikilink"))。[元朝武宗](../Page/元朝.md "wikilink")[大德十一年](../Page/大德.md "wikilink")（1307年）元世祖忽必烈的孙子[元武宗海山建立](../Page/元武宗.md "wikilink")[元中都](../Page/元中都.md "wikilink")（今张北县馒头营乡），与当时[元大都](../Page/元大都.md "wikilink")（即今北京）、[元上都](../Page/元上都.md "wikilink")（今[内蒙古](../Page/内蒙古.md "wikilink")[正蓝旗
(内蒙古)东](../Page/正蓝旗_\(内蒙古\).md "wikilink")）齐名。

[明朝时除蔚县一带属于山西](../Page/明朝.md "wikilink")[大同府外](../Page/大同府.md "wikilink")，其它皆属[京师](../Page/京师.md "wikilink")（治[顺天府](../Page/顺天府.md "wikilink")，今北京市），分为[延庆州](../Page/延庆州.md "wikilink")、[保安州](../Page/保安州.md "wikilink")、[云州](../Page/云州.md "wikilink")、蔚州及[万全都指挥使司十二卫](../Page/万全都指挥使司.md "wikilink")、所地。[明朝宣德四年](../Page/明朝.md "wikilink")（1429年）筑张家口堡，属京师[宣府镇](../Page/宣府镇.md "wikilink")。[明正统十四年](../Page/明朝.md "wikilink")（1449年）[蒙古](../Page/蒙古.md "wikilink")[瓦剌部落太师](../Page/瓦剌部落.md "wikilink")[也先部大举攻明](../Page/也先.md "wikilink")。宦官[王振](../Page/王振.md "wikilink")（蔚县人）不顾朝臣反对，怂恿英宗[朱祁镇御驾亲征](../Page/朱祁镇.md "wikilink")，七月朱祁镇率军50万与百名文武官员亲征，出[居庸关](../Page/居庸关.md "wikilink")。由于准备仓促，途中军粮不继，军心不稳，又有传言说前方已经战败，王振因此惊慌，命大军撤出大同。王振本想经[紫荆关](../Page/紫荆关.md "wikilink")（今河北[易县西北](../Page/易县.md "wikilink")）让英宗到蔚州，而匆忙改变行军路线。行四十里后，又怕大军过境损坏家乡庄稼，急令军队转道宣府（今河北宣化），在8月14日到达[土木堡](../Page/土木堡.md "wikilink")（河北怀来县东南），离怀来城仅20里，王振以辎重军车没能到达，下令就地宿营。土木堡地势高，无泉缺水，其南十五里处有河，被瓦剌军占据，后瓦剌诈和，趁机发动攻势，明军只得仓促应战，一败涂地。[明英宗被俘](../Page/土木之變.md "wikilink")，王振被护卫将军[樊忠所杀](../Page/樊忠.md "wikilink")。

[清朝时北属](../Page/清朝.md "wikilink")[口北三厅](../Page/口北三厅.md "wikilink")（多伦诺尔厅、独石口厅、张家口厅），大部属张家口厅。南部属宣化府（治今[宣化](../Page/宣化.md "wikilink")）。1860年，《[中俄北京条约](../Page/中俄北京条约.md "wikilink")》中，张家口与[库伦](../Page/乌兰巴托.md "wikilink")、[喀什噶尔一同被开放为商埠](../Page/喀什市.md "wikilink")。清朝光绪二十六年（1900年）内蒙古中部[天主教教区发生了教徒被杀事件](../Page/天主教.md "wikilink")，除崇礼西湾子主教座堂避难的5000多教友外，绝大部分（3200多人）被杀。清朝宣统元年（1909年）8月，由[詹天佑主持设计的自](../Page/詹天佑.md "wikilink")[北京至张家口的](../Page/北京.md "wikilink")[京张铁路建成并于](../Page/京张铁路.md "wikilink")11月通车，京张铁路是是中国首条不使用外国资金及人员，由中国人自行勘测、设计、施工完成，投入营运的铁路。

[Inner_Mongolian_People's_Revolutionary_Party.fondation.png](https://zh.wikipedia.org/wiki/File:Inner_Mongolian_People's_Revolutionary_Party.fondation.png "fig:Inner_Mongolian_People's_Revolutionary_Party.fondation.png")及[共产国际代表合影](../Page/共产国际.md "wikilink")。前排左起：[金永昌](../Page/金永昌.md "wikilink")、[福明泰](../Page/福明泰.md "wikilink")、[郭道甫](../Page/郭道甫.md "wikilink")、[白云梯](../Page/白云梯.md "wikilink")、[乐景涛](../Page/乐景涛.md "wikilink")、[包悦卿](../Page/包悦卿.md "wikilink")、[李丹山](../Page/李丹山.md "wikilink")；后排左一为[宝音鄂木合](../Page/宝音鄂木合.md "wikilink")（笔名齐庆毕力格图，[蒙古革命青年联盟中央委员会书记](../Page/蒙古革命青年联盟.md "wikilink")），左二为[丹巴道尔吉](../Page/策伦奥齐尔·丹巴道尔吉.md "wikilink")（[蒙古人民革命党中央委员会委员长](../Page/蒙古人民革命党.md "wikilink")），左三为[奥齐罗夫](../Page/奥齐罗夫.md "wikilink")（共产国际代表）。\[5\]\[6\]\[7\]\]\]

1912年[中華民国时属宣化府万全县](../Page/中華民国.md "wikilink")。民国二年（1913年）属[直隶省](../Page/直隶省.md "wikilink")[察哈尔特别区兴和道和口北道](../Page/察哈尔特别区.md "wikilink")。民国九年（1920年），[美国在张家口设立](../Page/美国.md "wikilink")[美国驻张家口领事馆](../Page/美国驻张家口领事馆.md "wikilink")（1927年被撤销），索克斌（Samuel
Sokobin）担任领事。民国十七年（1928年）设[察哈尔省](../Page/察哈尔省.md "wikilink")，张家口为[省会](../Page/省会.md "wikilink")，到1952年止。1925年10月13日，[内蒙古人民革命党第一次代表大会在张家口召开](../Page/内蒙古人民革命党.md "wikilink")。内蒙古各盟、部、旗约100多名代表出席大会，一些蒙古族青年列席大会；[共产国际驻内蒙古代表](../Page/共产国际.md "wikilink")[奥齐罗夫](../Page/奥齐罗夫.md "wikilink")、[中国国民党代表](../Page/中国国民党.md "wikilink")[李烈钧](../Page/李烈钧.md "wikilink")（中国国民党中央执行委员）、[中国共产党代表](../Page/中国共产党.md "wikilink")[王仲一](../Page/王仲一.md "wikilink")（中共张家口地委组织委员）、[江浩](../Page/江浩.md "wikilink")（中共张家口地委宣传委员）、[冯玉祥的](../Page/冯玉祥.md "wikilink")[国民军代表](../Page/国民军.md "wikilink")[张之江](../Page/张之江.md "wikilink")（[察哈尔都统](../Page/察哈尔都统.md "wikilink")）、[蒙古人民革命党代表](../Page/蒙古人民革命党.md "wikilink")[丹巴道尔吉](../Page/策伦奥齐尔·丹巴道尔吉.md "wikilink")（[蒙古人民革命党中央委员会委员长](../Page/蒙古人民革命党.md "wikilink")）等人出席大会。11月，中共早期领导人[李大钊在](../Page/李大钊.md "wikilink")[张家口主持召开农工兵大同盟成立大会](../Page/张家口.md "wikilink")，来自热河、[察哈尔](../Page/察哈尔.md "wikilink")、[绥远各地的](../Page/绥远.md "wikilink")200多名代表与会。大会选举李大钊、[韩麟符](../Page/韩麟符.md "wikilink")、[赵世炎](../Page/赵世炎.md "wikilink")、[李裕智](../Page/李裕智.md "wikilink")、[王仲青](../Page/王仲青.md "wikilink")、[陈镜湖](../Page/陈镜湖.md "wikilink")、[郑丕烈为大同盟执行委员](../Page/郑丕烈.md "wikilink")，李大钊任书记，韩麟符、赵世炎任副书记。\[8\]\[9\]\[10\]1927年10月国民革命期间，[阎锡山攻打奉系](../Page/阎锡山.md "wikilink")，爆发[下花园战斗](../Page/下花园战斗.md "wikilink")，并占领张家口，后在奉系的反攻下，退守雁门、蔚县、井径\[11\]。

民国二十二年（1933年）[长城战役之后](../Page/长城战役.md "wikilink")，[日军越过](../Page/日军.md "wikilink")[长城](../Page/长城.md "wikilink")，向[华北渗透](../Page/华北.md "wikilink")，并于该年春占领察哈尔部分地区。5月26日，在[中国共产党的推动下](../Page/中国共产党.md "wikilink")，由[冯玉祥](../Page/冯玉祥.md "wikilink")、[吉鸿昌](../Page/吉鸿昌.md "wikilink")、[方振武等中国将领在](../Page/方振武.md "wikilink")[察哈尔组织了人数约](../Page/察哈尔.md "wikilink")10万的[察哈尔民众抗日同盟军](../Page/察哈尔民众抗日同盟军.md "wikilink")，冯玉祥任总司令。6月22日，同盟军开始向察哈尔和[热河的日军发动进攻](../Page/热河.md "wikilink")，7月12日，同盟军在吉鸿昌的指挥下攻占重镇[多伦](../Page/多伦.md "wikilink")，并将日军全部逐出察哈尔。此为[九一八事变以来中国军队首次收复失地](../Page/九一八事变.md "wikilink")。此后，日军与[满洲国军对多伦进行反扑](../Page/满洲国.md "wikilink")，同时，[南京国民政府以实现军令统一为由](../Page/南京国民政府.md "wikilink")，也派遣军队进逼张家口，威胁同盟军的后方，同盟军被迫解散。民国二十六年（1937年）8月，日本全面[侵华战争爆发后](../Page/侵华战争.md "wikilink")，爆发了[张家口战斗](../Page/张家口战斗.md "wikilink")。8月，日本组成[察哈尔派遣兵团](../Page/察哈尔派遣兵团.md "wikilink")，[东条英机任司令官](../Page/东条英机.md "wikilink")，驻地原为多伦，后迁至张北。
9月，日本人在以张家口为中心，包括察哈尔南部10县的地区，成立[察南自治政府](../Page/察南自治政府.md "wikilink")，[于品卿为主席](../Page/于品卿.md "wikilink")。民国二十八年（1939年）初[蒙疆联合自治政府成立时](../Page/蒙疆联合自治政府.md "wikilink")，首都迁至张家口，并改为直属蒙疆的张家口特别市。同时为[晋察冀边区政府所在地](../Page/晋察冀边区.md "wikilink")，察哈尔省会。1945年8月，蒙疆联合自治政府瓦解，于品卿被八路军逮捕。12月，[晋察冀边区组成特别法庭在张家口市邮政总局院审判于品卿](../Page/晋察冀边区.md "wikilink")，判处其死刑并执行。年底，[华北联合大学教育学院随中共军队进入张家口](../Page/华北联合大学.md "wikilink")，随即恢复原有的三个学院。1946年又成立外国语学院，下设俄文、英文两系，[成仿吾任校长](../Page/成仿吾.md "wikilink")，[周扬任副校长](../Page/周扬_\(学者\).md "wikilink")，[张如心任教务长](../Page/张如心.md "wikilink")。华北文工团、延安鲁艺工作团于12月合并到华北联大，简称华大文工团。联大设立了文艺、法政、教育、外语四个学院。1946年10月，国军攻占张家口，华北联大等学校被迫提前于9月撤出。

[Han_Guangsen_and_Cui_Jinglan.jpg](https://zh.wikipedia.org/wiki/File:Han_Guangsen_and_Cui_Jinglan.jpg "fig:Han_Guangsen_and_Cui_Jinglan.jpg")张家口市市长[韩广森](../Page/韩广森.md "wikilink")（右）、副市长[崔景岚](../Page/崔景岚.md "wikilink")（左）。\]\]
民国三十七年（1948年）11月，国军华北剿匪总司令[傅作义以](../Page/傅作义.md "wikilink")1个军8个师驻守张家口。11月29日，[解放军](../Page/解放军.md "wikilink")[华北军区第3兵团在](../Page/华北军区第3兵团.md "wikilink")[杨成武司令员的指挥下向张家口地区守军发起攻击](../Page/杨成武.md "wikilink")，形成对张家口包围之势。傅作义急令在[北平的第](../Page/北平.md "wikilink")35军（欠1个师）及怀来（今怀来镇）的第104军第258师连夜驰援张家口。12月5日，解放军[东北野战军在北平以北](../Page/东北野战军.md "wikilink")[密云歼灭国军十三军一个师](../Page/密云.md "wikilink")。傅作义误以为东北野战军要直取北平，因而，又急令三十五军回师东撤，返回北平。12月7日撤至怀来县，受到[华北野战军强烈阻击](../Page/华北野战军.md "wikilink")，当夜退守[新保安城](../Page/新保安.md "wikilink")（属张家口怀来县）内。35军被围后，曾数次突围，但均被击退。军长[郭景云感到突围无望](../Page/郭景云.md "wikilink")，便转而在城内日夜修筑工事，企图固守待援。12月21日华北第2兵团连同东北野战军一部对新保安发动进攻。12月22日，解放军攻克新保安，占领怀来县全境，此时驻守张家口的[国军陷入绝境并开始突围](../Page/国军.md "wikilink")，而解放军已经占领了周边地区所有高地，并已构筑起坚固的工事。国军突围受阻。12月24日，解放军占领张家口。\[12\]

1949年10月至11月间，爆发[鼠疫](../Page/1949年张家口鼠疫.md "wikilink")。1952年12月察哈尔省建制撤销，察南专区、察北专区合并后称张家口专区，划归[河北省](../Page/河北省.md "wikilink")，张家口市为河北省[省辖市](../Page/省辖市.md "wikilink")，并为[专区治所](../Page/专区.md "wikilink")。1955年宣化市划入。1958年5月张家口市改属张家口专区。1959年5月撤销张家口专区，所辖各县划归张家口市。1961年5月复置张家口专区，张家口市及所属各县隶属之。1967年12月，张家口专区改称张家口地区，辖张家口市，县属不变。1981年9月，[中国人民解放军](../Page/中国人民解放军.md "wikilink")[北京军区在张家口进行了一次大规模军事演习](../Page/北京军区.md "wikilink")，即[华北大演习](../Page/华北大演习.md "wikilink")。[胡耀邦](../Page/胡耀邦.md "wikilink")、[邓小平](../Page/邓小平.md "wikilink")、[赵紫阳](../Page/赵紫阳.md "wikilink")、[李先念](../Page/李先念.md "wikilink")、[华国锋参加](../Page/华国锋.md "wikilink")。演习结束后在[张家口机场举行阅兵](../Page/张家口机场.md "wikilink")。1983年宣化县划入，1983年11月，张家口市改为[河北省省辖市](../Page/河北省.md "wikilink")。1993年7月1日，张家口地区和张家口市合并，称张家口市。1995年5月9日，中华人民共和国[国务院正式批准张家口市对外开放](../Page/国务院.md "wikilink")（即开始在张家口适用[改革开放政策](../Page/改革开放.md "wikilink")），相对于其他城市开放较晚。\[13\]

1998年1月10日中午11时52分左右，张北地区一带发生里氏6.2级强烈地震。此次地震涉及张北、尚义、康保、万全四县19个乡、200多个行政村，以及北京、天津、内蒙古等地。造成49人死亡、362人重伤、11077人轻伤，10多万间房屋倒塌，4.4万人无家可归，直接经济损失8.36亿元。\[14\]

2013年11月5日，张家口市与北京市联合申请2022年冠名为[2022北京冬奥会的冬季奥林匹克运动会](../Page/北京申办2022年冬奥会计划.md "wikilink")，2015年7月底申请成功。

2016年1月，张家口市部分行政区划调整获国务院批复，撤销宣化县、宣化区，设立新的宣化区，以原宣化县、宣化区的行政区域为新的宣化区的行政区域，政府驻地在原宣化区人民政府驻地；撤销万全县、崇礼县，设立万全区、崇礼区，管辖范围和政府驻地不变。

## 地理

[Shijiazhuang_in_Hebei.png](https://zh.wikipedia.org/wiki/File:Shijiazhuang_in_Hebei.png "fig:Shijiazhuang_in_Hebei.png")西北部，紧邻[北京市](../Page/北京市.md "wikilink")，历史上由于其特殊的地理位置而一直被称为北京的北大门。\]\]
张家口市处[华北平原与](../Page/华北平原.md "wikilink")[内蒙古高原的过渡地带](../Page/内蒙古高原.md "wikilink")[冀西北山间盆地](../Page/冀西北山间盆地.md "wikilink")，北靠[坝上高原](../Page/坝上高原.md "wikilink")，南为[洋河谷地](../Page/洋河.md "wikilink")，地势西北高、东南低。以[大马群山分水岭为界](../Page/大马群山.md "wikilink")，分北部坝上、南部坝下两个自然地理区域。属中[温带季风气候](../Page/温带季风气候.md "wikilink")，年均降水量406毫米，年均温10.3
℃。

位于[蔚县](../Page/蔚县.md "wikilink")、[涿鹿交界处的](../Page/涿鹿.md "wikilink")[小五台山平均海拔](../Page/小五台山.md "wikilink")2000米，其中主峰东台最高，达2882米，为河北省最高峰。

主城区地处[清水河两岸](../Page/清水河_\(张家口河流\).md "wikilink")，东、西、北三面环山，只有南面是小面积平原，[长城从北面山上蜿蜒而过](../Page/长城.md "wikilink")，平均海拔725米。在城区内就可看到，主城区由于地形原因呈南北狭长状分布。

由于气候原因，城区四周的山上曾经很少有绿色植物覆盖，经过市民多年的齐心努力，现已建立了数个森林公园及众多的景点，风景怡人。

## 政治

### 现任领导

<table>
<caption>张家口市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党张家口市委员会.md" title="wikilink">中国共产党<br />
张家口市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/张家口市人民代表大会.md" title="wikilink">张家口市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/张家口市人民政府.md" title="wikilink">张家口市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议张家口市委员会.md" title="wikilink">中国人民政治协商会议<br />
张家口市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/回建.md" title="wikilink">回建</a>[15]</p></td>
<td><p><a href="../Page/李青春.md" title="wikilink">李青春</a>[16]</p></td>
<td><p><a href="../Page/武卫东_(1969年).md" title="wikilink">武卫东</a>[17]</p></td>
<td><p><a href="../Page/王江_(1960年).md" title="wikilink">王江</a>[18]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/回族.md" title="wikilink">回族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/沧县.md" title="wikilink">沧县</a></p></td>
<td><p>河北省<a href="../Page/阳原县.md" title="wikilink">阳原县</a></p></td>
<td><p>河北省<a href="../Page/平山县.md" title="wikilink">平山县</a></p></td>
<td><p>河北省<a href="../Page/万全县.md" title="wikilink">万全县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年2月</p></td>
<td><p>2017年4月</p></td>
<td><p>2016年11月</p></td>
<td><p>2017年4月</p></td>
</tr>
</tbody>
</table>

### 行政区划

张家口市现下辖6个[市辖区](../Page/市辖区.md "wikilink")、10个[县](../Page/县_\(中华人民共和国\).md "wikilink")，市人民政府驻纬二路市府西路，原通泰大厦。

  - 市辖区：[桥西区](../Page/桥西区_\(张家口市\).md "wikilink")、[桥东区](../Page/桥东区_\(张家口市\).md "wikilink")、[宣化区](../Page/宣化区.md "wikilink")、[下花园区](../Page/下花园区.md "wikilink")、[万全区](../Page/万全区.md "wikilink")、[崇礼区](../Page/崇礼区.md "wikilink")
  - 县：[张北县](../Page/张北县.md "wikilink")、[康保县](../Page/康保县.md "wikilink")、[沽源县](../Page/沽源县.md "wikilink")、[尚义县](../Page/尚义县.md "wikilink")、[蔚县](../Page/蔚县.md "wikilink")、[阳原县](../Page/阳原县.md "wikilink")、[怀安县](../Page/怀安县.md "wikilink")、[怀来县](../Page/怀来县.md "wikilink")、[涿鹿县](../Page/涿鹿县.md "wikilink")、[赤城县](../Page/赤城县.md "wikilink")

此外，张家口市设立以下3个行政管理区：[张家口经济开发区](../Page/张家口经济开发区.md "wikilink")、[塞北管理区](../Page/塞北管理区.md "wikilink")、[察北管理区](../Page/察北管理区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>张家口市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[19]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>130700</p></td>
</tr>
<tr class="odd">
<td><p>130702</p></td>
</tr>
<tr class="even">
<td><p>130703</p></td>
</tr>
<tr class="odd">
<td><p>130705</p></td>
</tr>
<tr class="even">
<td><p>130706</p></td>
</tr>
<tr class="odd">
<td><p>130708</p></td>
</tr>
<tr class="even">
<td><p>130709</p></td>
</tr>
<tr class="odd">
<td><p>130722</p></td>
</tr>
<tr class="even">
<td><p>130723</p></td>
</tr>
<tr class="odd">
<td><p>130724</p></td>
</tr>
<tr class="even">
<td><p>130725</p></td>
</tr>
<tr class="odd">
<td><p>130726</p></td>
</tr>
<tr class="even">
<td><p>130727</p></td>
</tr>
<tr class="odd">
<td><p>130728</p></td>
</tr>
<tr class="even">
<td><p>130730</p></td>
</tr>
<tr class="odd">
<td><p>130731</p></td>
</tr>
<tr class="even">
<td><p>130732</p></td>
</tr>
<tr class="odd">
<td><p>注：张家口经济开发区所辖2街道4镇分別包含在桥东区、桥西区及宣化区；张北县数字包含察北管理区所辖1镇1乡；沽源县数字包含塞北管理区。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>张家口市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[20]（2010年11月）</p></th>
<th><p>户籍人口[21]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>张家口市</p></td>
<td><p>4345485</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>桥东区</p></td>
<td><p>339372</p></td>
<td><p>7.81</p></td>
</tr>
<tr class="even">
<td><p>桥西区</p></td>
<td><p>287900</p></td>
<td><p>6.63</p></td>
</tr>
<tr class="odd">
<td><p>宣化区</p></td>
<td><p>370569</p></td>
<td><p>8.53</p></td>
</tr>
<tr class="even">
<td><p>下花园区</p></td>
<td><p>62764</p></td>
<td><p>1.44</p></td>
</tr>
<tr class="odd">
<td><p>宣化县</p></td>
<td><p>273506</p></td>
<td><p>6.29</p></td>
</tr>
<tr class="even">
<td><p>张北县</p></td>
<td><p>318669</p></td>
<td><p>7.33</p></td>
</tr>
<tr class="odd">
<td><p>康保县</p></td>
<td><p>204975</p></td>
<td><p>4.72</p></td>
</tr>
<tr class="even">
<td><p>沽源县</p></td>
<td><p>174619</p></td>
<td><p>4.02</p></td>
</tr>
<tr class="odd">
<td><p>尚义县</p></td>
<td><p>151639</p></td>
<td><p>3.49</p></td>
</tr>
<tr class="even">
<td><p>蔚　县</p></td>
<td><p>450236</p></td>
<td><p>10.36</p></td>
</tr>
<tr class="odd">
<td><p>阳原县</p></td>
<td><p>258086</p></td>
<td><p>5.94</p></td>
</tr>
<tr class="even">
<td><p>怀安县</p></td>
<td><p>210914</p></td>
<td><p>4.85</p></td>
</tr>
<tr class="odd">
<td><p>万全县</p></td>
<td><p>211706</p></td>
<td><p>4.87</p></td>
</tr>
<tr class="even">
<td><p>怀来县</p></td>
<td><p>352307</p></td>
<td><p>8.11</p></td>
</tr>
<tr class="odd">
<td><p>涿鹿县</p></td>
<td><p>333932</p></td>
<td><p>7.68</p></td>
</tr>
<tr class="even">
<td><p>赤城县</p></td>
<td><p>238169</p></td>
<td><p>5.48</p></td>
</tr>
<tr class="odd">
<td><p>崇礼县</p></td>
<td><p>106122</p></td>
<td><p>2.44</p></td>
</tr>
<tr class="even">
<td><p>注：常住人口中，张家口经济开发区184042人未有单独列出；张北县的数据中包含察北管理区20270人；沽源县的数据中包含塞北管理区6360人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年的[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")4345491人\[22\]，同[第五次全国人口普查的](../Page/第五次全国人口普查.md "wikilink")4238308人相比，十年共增加107183人，增长2.53%。年平均增长率为0.25%。其中，男性人口为2225429人，占51.21%；女性人口为2120062人，占48.79%。总人口性别比（以女性为100）为104.97。0－14岁人口为691707人，占15.92%；15－64岁人口为3204272人，占73.74%；65岁及以上人口为449512人，占10.34%。

### 民族

张家口在历史上就一直是北方少数民族与汉族杂居交往、贸易流通、长期融会的重要之地。民族构成以[汉族为主](../Page/汉族.md "wikilink")，此外还有[回族](../Page/回族.md "wikilink")、[满族](../Page/满族.md "wikilink")、[蒙古族](../Page/蒙古族.md "wikilink")、[藏族](../Page/藏族.md "wikilink")、[朝鲜族](../Page/朝鲜族.md "wikilink")、[白族](../Page/白族.md "wikilink")、[维吾尔族](../Page/维吾尔族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[黎族](../Page/黎族.md "wikilink")、[壮族](../Page/壮族.md "wikilink")、[侗族](../Page/侗族.md "wikilink")、[布依族](../Page/布依族.md "wikilink")、[高山族](../Page/高山族.md "wikilink")、[土家族](../Page/土家族.md "wikilink")、[苗族](../Page/苗族.md "wikilink")、[哈萨克族](../Page/哈萨克族.md "wikilink")、[达斡尔族](../Page/达斡尔族.md "wikilink")、[俄罗斯族](../Page/俄罗斯族.md "wikilink")、[瑶族](../Page/瑶族.md "wikilink")、[哈尼族](../Page/哈尼族.md "wikilink")、[傣族](../Page/傣族.md "wikilink")、[畲族](../Page/畲族.md "wikilink")、[拉祜族](../Page/拉祜族.md "wikilink")、[锡伯族](../Page/锡伯族.md "wikilink")、[鄂温克族等](../Page/鄂温克族.md "wikilink")26个少数民族。少数民族人口共计7万余人，其中人数超过万人的有回族和满族。少数民族分布在市区和各县，呈大分散、小聚居的特点。全市现共有2个民族乡，91民族村。


<table>
<thead>
<tr class="header">
<th></th>
<th><p>民族乡、民族村</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>桥东区</p></td>
<td><p>老鸦庄乡下小站满汉联合村</p></td>
</tr>
<tr class="even">
<td><p>桥西区</p></td>
<td><p>沈家屯乡闫家屯满汉联合村</p></td>
</tr>
<tr class="odd">
<td><p>宣化区</p></td>
<td><p>河子西乡河子西满汉联合村、河子西乡旧李宅满汉联合村、河子西乡肖家坊满汉联合村、河子西乡沙圪哒满汉联合村、春光乡庙底回汉联合村。</p></td>
</tr>
<tr class="even">
<td><p>崇礼区</p></td>
<td><p>石嘴子乡六间房蒙汉联合村、石嘴子乡下四杆旗蒙汉联合村、石嘴子乡摆察蒙汉联合村、西湾子镇二道沟蒙汉联合村、西湾子镇上三道沟蒙汉联合村、西湾子镇瓦夭蒙汉联合村、西湾子镇黄土嘴蒙汉联合村、西湾子镇东南沟蒙汉联合村、高家营镇乌拉哈达蒙汉联合村、高家营镇高家营蒙汉联合村、高家营镇南地蒙汉联合村、石夭子乡下耗庆蒙汉联合村、场地乡下新营蒙汉联合村、红旗营乡东红旗营蒙汉联合村、五十家乡察汗陀罗蒙汉联合村、五十家乡麻地营蒙汉联合村、大水泉乡小水泉蒙汉联合村。</p></td>
</tr>
<tr class="odd">
<td><p>赤城县</p></td>
<td><p>上斗营乡下斗营回汉联合村、茨营子乡碾子湾蒙汉联合村、大海陀乡东山庙回汉联合村、独石口乡青羊沟满族村、独石口乡杨家夭回族村、东万口乡官路房蒙汉联合村、东万口乡塘子营蒙汉联合村、东卯乡头道营满汉联合村、东卯乡三道营满汉联合村。</p></td>
</tr>
<tr class="even">
<td><p>涿鹿县</p></td>
<td><p>西二堡乡牛家场满汉联合村、西二堡乡白塔寺满汉联合村、西二堡乡西二堡满汉联合村、西二堡乡杨堡满汉联合村、大堡镇回汉联合村、东小庄乡保庄满汉联合村、涿鹿镇东关回汉联合村、涿鹿镇周堡满汉联合村。</p></td>
</tr>
<tr class="odd">
<td><p>怀来县</p></td>
<td><p><strong>怀来县王家楼回族乡</strong>、沙城镇一街回汉联合村、沙城镇五街回汉联合村、沙城镇六街回汉联合村、沙城镇八街回汉联合村、沙城镇南园回族村、新保安镇前进街一街回汉联合村、新保安镇民主街一街回汉联合村、新保安镇新兴街一街回汉联合村、王家楼回族乡王家楼一街回汉联合村、王家楼回族乡麻峪口一街回汉联合村、王家楼回族乡晏庄回族村、王家楼回族乡茨山回族村、大黄庄乡朱官屯满汉联合村、古家夭乡甘汲梁一街回汉联合村。</p></td>
</tr>
<tr class="even">
<td><p>沽源县</p></td>
<td><p><strong>沽源县大二号回族乡</strong>、常铁炉乡学堂营满汉联合村、常铁炉乡菜园满汉联合村、常铁炉乡温铁炉满汉联合村、常铁炉乡官厂满汉联合村、长梁乡大宏山满汉联合村、长梁乡大石满汉联合村、二道沟乡迷地沟满汉联合村、二道沟乡大梁底满汉联合村、闪电河乡土井子满汉联合村、闪电河乡石头坑满汉联合村、闪电河乡前水泉满汉联合村、丰元店乡蒙古营蒙汉联合村、丰元店乡二道沟回汉联合村、小厂乡棠梨沟满汉联合村大二号回族乡大二号回汉联合村。</p></td>
</tr>
<tr class="odd">
<td><p>尚义县</p></td>
<td><p>七甲乡三合澄回汉联合村、七甲乡三排地回族村、七甲乡南洼回族村、炕塄乡西伙房回汉联合村、八道沟乡大东山回汉联合村、哈拉沟乡王油房回汉联合村，后石庄井乡四台蒙古营蒙古族村、大营盘乡五台蒙古营蒙古族村、套里庄乡西城夭回汉联合村。</p></td>
</tr>
<tr class="even">
<td><p>张北县</p></td>
<td><p>公会镇新义回族村、公会镇汽车桥回族村、公会镇黄石崖东友回族村、公会镇西号回族村、公会镇南号回族村、大河乡石盖梁回汉联合村、大河乡大河回汉联合村、大河乡狼窝沟回族村、海流图乡三台蒙古营蒙汉联合村。</p></td>
</tr>
<tr class="odd">
<td><p>康保县</p></td>
<td><p>处长地乡回族村、处长地乡四号回族村、李家地乡马家营回族村。</p></td>
</tr>
</tbody>
</table>

## 宗教

全市现有[天主教](../Page/天主教.md "wikilink")、[基督教](../Page/基督教.md "wikilink")、[伊斯兰教](../Page/伊斯兰教.md "wikilink")、[佛教](../Page/佛教.md "wikilink")、[道教五种](../Page/道教.md "wikilink")[宗教](../Page/宗教.md "wikilink")。信教群众12万余人，占全市总人口的2.5%左右，其中尤以天主教（六万余人）和伊斯兰教（近四万人）信众为最多。现有教职人员二百余人。有[天主教爱国会](../Page/天主教爱国会.md "wikilink")、[天主教教务委员会](../Page/天主教教务委员会.md "wikilink")、[基督教三自爱国运动委员会](../Page/基督教三自爱国运动委员会.md "wikilink")、[基督教教务委员会](../Page/基督教教务委员会.md "wikilink")、[伊斯兰教协会](../Page/伊斯兰教协会.md "wikilink")、[佛教协会筹备组等六个宗教组织](../Page/佛教协会筹备组.md "wikilink")。已开放宗教活动场所173处，其中天主教123处，基督教11处，伊斯兰教34处，佛教3处，道教2处，比较著名的宗教场所（含遗迹）有[杨家坪圣母神慰院](../Page/杨家坪圣母神慰院.md "wikilink")、[若瑟总修院](../Page/若瑟总修院.md "wikilink")、[天主教宣化教区](../Page/天主教宣化教区.md "wikilink")、[天主教西湾子教区和](../Page/天主教西湾子教区.md "wikilink")[新华街清真寺等](../Page/新华街清真寺.md "wikilink")，**[杨家坪圣母神慰院](../Page/杨家坪圣母神慰院.md "wikilink")**是[天主教内最严谨的隐修会](../Page/天主教.md "wikilink")——[严规熙笃会](../Page/严规熙笃会.md "wikilink")（Trappist）于1883年在张家口附近太行山区建立的该会亚洲第一座隐修院，1947年解散。

## 经济

|                                                                                                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [20060416050427.jpg](https://zh.wikipedia.org/wiki/File:20060416050427.jpg "fig:20060416050427.jpg")[人民大会堂式建筑](../Page/人民大会堂.md "wikilink")，张家口市的地理坐标点，1999年经大规模改造扩建，更名为文化广场，顶部原有“毛泽东思想胜利万岁”后改为“建设开放繁荣文明的张家口”,市民活动的主要场所之一。\]\] |

该市
六区，十县中有10个国家级贫困县（区），分别为：康保县、张北县、阳原县、赤城县、沽源县、怀安县、崇礼区、尚义县、蔚县、万全区、(涿鹿县赵家蓬区)
；除市区（桥西区，桥东区）外，仅宣化区（旧宣化府）、蔚县 （旧蔚州）、怀来县（邻接北京市）3县（区）经济略发达；但近几年张北县经济发展也很快。

  - [农业](../Page/农业.md "wikilink")：产[玉米](../Page/玉米.md "wikilink")、[高粱](../Page/高粱.md "wikilink")、[谷子](../Page/谷子.md "wikilink")、[小麦](../Page/小麦.md "wikilink")、[莜麦](../Page/莜麦.md "wikilink")、[葵花](../Page/葵花.md "wikilink")、[菜籽](../Page/菜籽.md "wikilink")、[蚕豆](../Page/蚕豆.md "wikilink")、[豌豆](../Page/豌豆.md "wikilink")、[芸豆](../Page/芸豆.md "wikilink")、[绿豆](../Page/绿豆.md "wikilink")、薯类（含[马铃薯](../Page/马铃薯.md "wikilink")）等作物及各种蔬菜。
  - [工业](../Page/工业.md "wikilink")：以轻纺、机械、冶金、电力、食品、化工为主。毛皮、制革与毛麻纺工业较发达，素称皮都，产品称口羔、口皮。

## 资源

全市土地总面积5529万亩，[桑干河](../Page/桑干河.md "wikilink")、[洋河](../Page/洋河.md "wikilink")、[潮白河等河流流经境内](../Page/潮白河.md "wikilink")，沿河形成的[河川盆地土质肥沃](../Page/河川盆地.md "wikilink")，灌溉条件好，有利于发展农业和水果业，是全市[粮食和](../Page/粮食.md "wikilink")[水果的主产区](../Page/水果.md "wikilink")。

已发现各类矿产83种，已探明储量的矿种33种。一些大宗矿产如[煤](../Page/煤.md "wikilink")、[铁](../Page/铁.md "wikilink")、[金](../Page/金.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[膨润土](../Page/膨润土.md "wikilink")、[沸石](../Page/沸石.md "wikilink")、[石墨](../Page/石墨.md "wikilink")、[磷矿等均在河北省占有重要地位](../Page/磷.md "wikilink")。

水资源总量21.91亿立方米，人均水资源占有量487立方米。

现有林地面积1059万亩，森林覆盖率20.4%，[三北防护林工程穿越市境](../Page/三北防护林.md "wikilink")。

现有野生陆生植物120[科](../Page/科.md "wikilink")513[属](../Page/属.md "wikilink")、1343种。

## 特产

张家口有著名的三宝：山药（“药”读音yek，入声）、[莜面](../Page/莜面.md "wikilink")、大皮袄。“山药”是张家口人对[土豆的惯称](../Page/土豆.md "wikilink")，而并非药材[山药](../Page/山药.md "wikilink")，此称呼广泛使用在晋语的大包和张呼片地区，同时晋语和官话的过渡区比如[延庆](../Page/延庆县.md "wikilink")，也使用；“莜面”指的是莜麦（裸燕麦）粉，是张家口人喜欢的主要食品之一，延庆人也常食用；“大皮袄”是指张家口地区由于海拔较高，冬季气候寒冷，所以出产的皮衣很有名，张家口是中国著名的“皮都”之一。

此外，张家口的特产还有[宣化牛奶葡萄](../Page/宣化牛奶葡萄.md "wikilink")、[口蘑](../Page/口蘑.md "wikilink")、[萤石等](../Page/萤石.md "wikilink")。

## 交通

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Zhan_Tianyou_statue_on_Zhangjiakou_South_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:Zhan_Tianyou_statue_on_Zhangjiakou_South_Railway_Station.jpg "fig:Zhan_Tianyou_statue_on_Zhangjiakou_South_Railway_Station.jpg")广场外屹立的铜像：[詹天佑](../Page/詹天佑.md "wikilink")。他主持修建了[京张铁路](../Page/京张铁路.md "wikilink")。该铁路连接[北京](../Page/北京.md "wikilink")[丰台](../Page/丰台.md "wikilink")，经[居庸关](../Page/居庸关.md "wikilink")、[沙城](../Page/沙城.md "wikilink")、[宣化至](../Page/宣化.md "wikilink")[河北张家口](../Page/河北.md "wikilink")，全长约201.2千米，于1909年建成，是中国首条不使用外国资金及人员，由中国人自行完成，投入营运的[铁路](../Page/铁路.md "wikilink")。\]\] |

在主城区南部9公里处设有[张家口宁远机场](../Page/张家口宁远机场.md "wikilink")，为国内支线机场。

张家口市境内有[京包铁路](../Page/京包铁路.md "wikilink")（[京張鐵路为其中一段](../Page/京張鐵路.md "wikilink")）、[丰沙铁路](../Page/丰沙铁路.md "wikilink")、[沙蔚铁路](../Page/沙蔚铁路.md "wikilink")、[大秦铁路](../Page/大秦铁路.md "wikilink")，另外[宣庞铁路](../Page/宣庞铁路.md "wikilink")、[宣烟铁路通矿区](../Page/宣烟铁路.md "wikilink")。[张集铁路亦于](../Page/张集铁路.md "wikilink")2011年4月28日开通。[京张城际铁路原预计于](../Page/京张城际铁路.md "wikilink")2009年8月左右开工建设，经过多次延期后，于2016年上半年开工，预计将于2019年竣工。近期规划还有[张唐铁路](../Page/张唐铁路.md "wikilink")（张家口到[唐山](../Page/唐山.md "wikilink")[曹妃甸](../Page/曹妃甸.md "wikilink")）（建设中）、[大张客运专线](../Page/大张客运专线.md "wikilink")（张家口至[山西](../Page/山西.md "wikilink")[大同](../Page/大同市.md "wikilink")）（建设中）、[张准铁路](../Page/张准铁路.md "wikilink")（规划中）、[呼张客运专线](../Page/呼张客运专线.md "wikilink")（张家口至[内蒙古](../Page/内蒙古.md "wikilink")[呼和浩特](../Page/呼和浩特.md "wikilink")）（建设中）。现铁路通车总里程580多公里。

公路方面，有[109国道](../Page/109国道.md "wikilink")、[110国道](../Page/110国道.md "wikilink")、[112国道](../Page/112国道.md "wikilink")、[207国道](../Page/207国道.md "wikilink")、[京张高速公路](../Page/京张高速公路.md "wikilink")、[宣大高速公路](../Page/宣大高速公路.md "wikilink")、[丹拉高速公路](../Page/丹拉高速公路.md "wikilink")（[丹东至](../Page/丹东.md "wikilink")[拉萨](../Page/拉萨.md "wikilink")[国道主干线](../Page/国道.md "wikilink")）、[京藏高速公路](../Page/京藏高速公路.md "wikilink")、[京化高速公路](../Page/京化高速公路.md "wikilink")、[张石高速公路以及](../Page/张石高速公路.md "wikilink")19条省级公路穿越市境，[张承高速公路](../Page/张承高速公路.md "wikilink")（通行段至崇礼北收费站，崇礼北至承德为施工段）、[张涿高速公路](../Page/张涿高速公路.md "wikilink")（2013年12月31日张涿高速全线贯通）。截止2007年年中全市公路通车里程达到1.95万公里，位居河北省第一。\[23\]

城区现有公共交通线路30条，并已开通发往[河北省内](../Page/河北省.md "wikilink")、省外以及本市各县的多条长途客运线路。

## 媒体

|                                                                                                                       |
| --------------------------------------------------------------------------------------------------------------------- |
| [Zhangjiakou_TV_Logo.jpg](https://zh.wikipedia.org/wiki/File:Zhangjiakou_TV_Logo.jpg "fig:Zhangjiakou_TV_Logo.jpg") |

张家口市内媒体涵盖了电视、广播、报纸等众多形式，主要媒体包括：

  - [张家口电视台](../Page/张家口电视台.md "wikilink")
      - 张家口电视一台（新闻综合频道）
      - 张家口电视二台（综合影视频道）
      - 张家口电视三台（综合文艺频道）
  - [张家口日报](../Page/张家口日报.md "wikilink")
  - 张家口晚报
  - 张家口广播电视报
  - 张家口广播电台
      - 张家口广播电台一台（新闻综合广播频道）调频107.4兆赫，中波1566千赫
      - 张家口广播电台二台（综合文艺广播频道）调频100兆赫，中波900千赫
      - 张家口广播电台三台（交通生活广播频道）调频98.6兆赫

## 名胜古迹

[Dajingmen_04_July_2,2010.jpg](https://zh.wikipedia.org/wiki/File:Dajingmen_04_July_2,2010.jpg "fig:Dajingmen_04_July_2,2010.jpg")
张家口古遗址多，分布面广。境内存有[战国](../Page/战国.md "wikilink")、[秦](../Page/秦.md "wikilink")、[北魏](../Page/北魏.md "wikilink")、[北齐](../Page/北齐.md "wikilink")、[金](../Page/金国.md "wikilink")、[明](../Page/明.md "wikilink")6个时期10段[长城](../Page/长城.md "wikilink")，总长约2500千米，延伸市区和12个县。古长城的“大境门”是长城的重要关隘之一。阳原县“[泥河湾遗址](../Page/泥河湾遗址.md "wikilink")”是[旧石器时代的遗址](../Page/旧石器时代.md "wikilink")，有“世界标准地层”。[涿鹿](../Page/涿鹿.md "wikilink")[矾山是](../Page/矾山.md "wikilink")“[黄帝战](../Page/黄帝.md "wikilink")[蚩尤](../Page/蚩尤.md "wikilink")”的古战场，[黄帝城](../Page/黄帝城.md "wikilink")、黄帝泉、轩辕湖、黄帝祠、蚩尤三寨、蚩尤泉、上下七旗、釜山、桥山、蚩尤坟等许多历史古迹犹存。

张家口自古以来为兵家必争之地，市属各县留有许多古城址。著名的有[蔚县](../Page/蔚县.md "wikilink")[代王城](../Page/代王城遗址.md "wikilink")，是古时诸侯国之一的[代国都城](../Page/代_\(春秋\).md "wikilink")；[沽源县](../Page/沽源县.md "wikilink")[小宏城](../Page/小宏城.md "wikilink")，[北魏时为御夷故城](../Page/北魏.md "wikilink")；赤城县境内的雄关独石口，有“朔方天险”、“上谷咽喉”之称；宣化城汉属[上谷郡地](../Page/上谷郡.md "wikilink")，[唐末五代属](../Page/唐.md "wikilink")[武州](../Page/武州.md "wikilink")，[辽](../Page/辽.md "wikilink")、[金](../Page/金国.md "wikilink")、[元为州府治](../Page/元朝.md "wikilink")，明代为九边之一；[万全城](../Page/万全城.md "wikilink")、[鸡鸣驿城](../Page/鸡鸣驿城.md "wikilink")，部分砖墙保存尚好。

张家口境内古墓葬很多。有蔚县代王城汉墓群，怀安县汉墓群（分布于3个乡6个村境内）为西汉古墓群，万全区[老龙湾汉墓群](../Page/老龙湾.md "wikilink")，宣化辽壁画墓，赤城有明代名将[杨洪墓](../Page/杨洪_\(明朝\).md "wikilink")。\[24\]

全市共有2座省级历史文化名城、1座中国历史文化名镇、1座中国历史文化名村和多处全国及省级文物保护单位。

  - 省级历史文化名城：[宣化](../Page/宣化.md "wikilink")、[蔚县](../Page/蔚县.md "wikilink")
  - 中国历史文化名镇：[蔚县暖泉镇](../Page/蔚县.md "wikilink")
  - 中国历史文化名村：[怀来县鸡鸣驿乡鸡鸣驿村](../Page/怀来县.md "wikilink")
    （参见：[鸡鸣驿](../Page/鸡鸣驿.md "wikilink")）

此外，还有[大境门](../Page/大境门.md "wikilink")、[清远楼](../Page/清远楼.md "wikilink")、[镇朔楼](../Page/镇朔楼.md "wikilink")、[水母宫](../Page/水母宫.md "wikilink")、[赐儿山](../Page/赐儿山.md "wikilink")[云泉寺](../Page/云泉寺.md "wikilink")、[张世卿壁画墓](../Page/张世卿壁画墓.md "wikilink")、[泥河湾遗址](../Page/泥河湾遗址.md "wikilink")、[土木堡等名胜古迹](../Page/土木堡.md "wikilink")。

## 长远规划

张家口市是[京津冀城市群中的一个重要组成部分](../Page/京津冀城市群.md "wikilink")。2004年2月2日，[国务院批准了](../Page/国务院.md "wikilink")《张家口市城市总体规划（2000－2020年）》
[Zhang_Jiakou_Expressway.jpg](https://zh.wikipedia.org/wiki/File:Zhang_Jiakou_Expressway.jpg "fig:Zhang_Jiakou_Expressway.jpg")（图中红色）开工建设，这一工程的建成将缓解城区道路通车压力，改善通行条件。\]\]
未来的张家口市域内规划期末，本市城镇等级规模结构分为四级，首级市域中心城市为张家口市区（[桥东区](../Page/桥东区_\(张家口市\).md "wikilink")、[桥西区](../Page/桥西区_\(张家口市\).md "wikilink")）；第二级为5个中小城市，即沙城（[怀来县](../Page/怀来县.md "wikilink")）、张北、蔚州（今[蔚县](../Page/蔚县.md "wikilink")）3个中等城市，[涿鹿](../Page/涿鹿.md "wikilink")、[阳原](../Page/阳原.md "wikilink")2个小城市；第三级为柴沟堡、西湾子等7个县城，第四级为84个建制镇。

依据城市现状布局及未来城市空间趋势，主城区划分为7个片区：

市中心划分为**城北片区**、**红旗楼片区**、**南站片区**、**铁路南片区**。

[宣化城区划分为](../Page/宣化.md "wikilink")**古城片区**、**新城片区**。

[下花园城区分为](../Page/下花园.md "wikilink")**下花园片区**。

城市主中心位于市中心区的纬二路和纬三路一带，形成行政办公，文化娱乐，金融贸易为主的新区中心。两个次中心分别位于[宣化城区和](../Page/宣化.md "wikilink")[下花园城区](../Page/下花园.md "wikilink")，形成金融贸易、文化娱乐中心。

市中心区将沿[清水河两岸富有变化的建筑](../Page/清水河_\(张家口河流\).md "wikilink")、景观结点和沿河绿带相串联，形成既有整体感，又有节奏感的城市滨河景观风貌。沿八角台向北经[赐儿山](../Page/赐儿山.md "wikilink")、[水母宫](../Page/水母宫.md "wikilink")、[东太平山](../Page/东太平山.md "wikilink")、[西太平山](../Page/西太平山.md "wikilink")，构成自然景观风貌，与沿河休闲轴遥相呼应。

城区北端的[大境门是扼守京都的北大门](../Page/大境门.md "wikilink")，在这里，以大境门为主体，借助山势河流，开辟广场，增设绿地，无疑是“跨越雄关”的一大胜景。

主城区将选择合适地段结合张家口的山川风貌、地域文化及社会经济特征设置城市八景，即：

**跨越雄关**、**时代牧歌**、**鱼水情深**、**华夏之尊**、**沃野莜香**、**故里情怀**、**北国逢春**、**城河浸月**。

随着城市规划的日益扩大，张家口、宣化两地逐步趋于一体，形成“片”状布局，下花园为相对独立片区。

到2020年城市人口为110.4万人，到2050年将达到180万人左右。21世纪中叶，张家口将成为物资环境和生态环境高度协调，景观优美，各类基础设施和服务设施完善，富于文明的现代化城市。

## 教育

### 高等院校

  - [张家口学院](../Page/张家口学院.md "wikilink")
  - [河北北方学院](../Page/河北北方学院.md "wikilink")
  - [河北建筑工程学院](http://www.hebiace.edu.cn/)
  - [张家口职业技术学院](http://www.zhz.cn/)
  - [北方机电工业学校](http://sf.edude.net/hebei/bfjdg/index.htm)

### 中学

  - [张家口市第一中学](../Page/张家口一中.md "wikilink")
  - [张家口市第二中学](../Page/张家口市第二中学.md "wikilink")
    <http://www.zjk2z.com/index.htm>
  - [张家口市第三中学](../Page/张家口市第三中学.md "wikilink")
  - [张家口市第四中学](../Page/张家口市第四中学.md "wikilink")
  - [张家口市第五中学](../Page/张家口市第五中学.md "wikilink")
  - [张家口市第六中学](../Page/张家口市第六中学.md "wikilink")
  - [张家口市第七中学](../Page/张家口市第七中学.md "wikilink")
  - [张家口市第八中学](../Page/张家口市第八中学.md "wikilink")
  - [张家口市第九中学](../Page/张家口市第九中学.md "wikilink")
  - [张家口市第十中学](../Page/张家口市第十中学.md "wikilink")
  - [张家口市第十二中学](../Page/张家口市第十二中学.md "wikilink")
  - [张家口市第十九中学](../Page/张家口市第十九中学.md "wikilink")
  - [张家口市第二十中学](../Page/张家口市第二十中学.md "wikilink")
  - [张家口市第二十一中学](../Page/张家口市第二十一中学.md "wikilink")
  - [张家口市第一职业中学](../Page/张家口市第一职业中学.md "wikilink")
  - [张家口市创新国际中学（私立）](../Page/张家口市创新国际中学（私立）.md "wikilink")
  - [张家口市东方中学](../Page/张家口市东方中学.md "wikilink")
  - 张家口市立正博高级学校

### 小学

  - 张家口市宝丰街小学
  - 张家口市书院巷小学（[抡才书院](../Page/抡才书院.md "wikilink")）
  - 张家口市下东营小学
  - 张家口市卫华小学
  - 张家口市五一路小学
  - 张家口市蒙古营小学
  - 张家口市宝善街小学
  - 张家口市铁路斜街小学
  - 张家口市桥东区老鸦庄镇高庙小学
  - 张家口市利民小学
  - 张家口市北新村小学
  - 张家口市逸夫小学
  - 张家口市大境门小学
  - 张家口市南小区小学

## 友好城市

  - [特伦蒂诺-上阿迪杰大区](../Page/特伦蒂诺-上阿迪杰.md "wikilink")[波尔扎诺自治省](../Page/波尔扎诺自治省.md "wikilink")（2007年11月23日缔结）

  - [黑龙江省](../Page/黑龙江省.md "wikilink")[伊春市](../Page/伊春市.md "wikilink")（2012年3月1日缔结）

  - [湖南省](../Page/湖南省.md "wikilink")[株洲市](../Page/株洲市.md "wikilink")（2014年9月22日缔结）

  - [北京市](../Page/北京市.md "wikilink")[海淀区](../Page/海淀区.md "wikilink")（2016年3月17日缔结）

  - [萨瓦省](../Page/萨瓦省.md "wikilink")[尚贝里](../Page/尚贝里.md "wikilink")（2017年2月15日缔结）

  - [派亚特海梅区](../Page/派亚特海梅区.md "wikilink")[拉赫蒂](../Page/拉赫蒂.md "wikilink")（2018年11月25日缔结）

## 其它信息

  - 2005年4月底，张家口市正式向[国家体育总局递交了申办](../Page/国家体育总局.md "wikilink")2012年第12届[全国冬季运动会的申请](../Page/中华人民共和国全国冬季运动会.md "wikilink")。这是[河北省首次申办全国最高规格的综合性运动会](../Page/河北省.md "wikilink")，也是东三省之外的省份首次申办冬运会。
  - 著名[电影](../Page/电影.md "wikilink")[导演](../Page/导演.md "wikilink")[张艺谋于西元](../Page/张艺谋.md "wikilink")1999年拍摄的影片《[一个都不能少](../Page/一个都不能少.md "wikilink")》，就是在张家口市[赤城县](../Page/赤城县.md "wikilink")[镇宁堡乡](../Page/镇宁堡乡.md "wikilink")**水泉村**拍摄完成的。
  - 在[金庸小说](../Page/金庸.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》中，主人公[郭靖和](../Page/郭靖.md "wikilink")[黄蓉就是在张家口相遇的](../Page/黄蓉.md "wikilink")。

## 参考文献

## 外部链接

  - [中国·张家口（张家口政府公众信息网）](http://www.zjk.gov.cn/)
  - [张家口新闻网](http://www.zjknews.com/)
  - [张家口热线](http://www.zjknews.com/)

{{-}}

[張家口](../Category/張家口.md "wikilink")
[Category:河北地级市](../Category/河北地级市.md "wikilink")
[冀](../Category/中国大城市.md "wikilink")
[Category:京津冀城市群](../Category/京津冀城市群.md "wikilink")

1.  [中国地名英文新旧拼法比照（汉语拼音、邮政式拼音）](http://hi.baidu.com/ttfz/blog/item/a54201d7460e47d7a144dfd7.html)

2.  《张家口地理》 海南出版社

3.

4.

5.  二木博史，丹巴道尔吉政权对内蒙古革命的援助，乌力吉套格套 译，蒙古学信息2002年01期

6.  [神秘失踪的革命者，内蒙古日报，2011年8月25日](http://szb.northnews.cn/nmgrb/html/2011-08/25/content_856737.htm)


7.  內蒙古革命史画册:1919-1949，呼和浩特：內蒙古人民出版社，2002年，第28页

8.  [韩麟符追烈始末，塞外赤峰，2013-12-05](http://www.swcf.cn/wh/2013-12/05/content_52847.htm)

9.  [胡广志，北方英杰韩麟符（二），建平新闻网，2015-11-20](http://www.jpxww.com/news_xx.asp?id=6291)

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23. [我市公路通车里程跃居全省第一
    张家口新闻网](http://www.zjknews.com/show_news.asp?newsid=91088)


24.