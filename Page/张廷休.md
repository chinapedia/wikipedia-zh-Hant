**張廷休**（），历史学家，教育家。

1924年毕业于[国立东南大学历史系](../Page/国立东南大学.md "wikilink")。曾于国民政府[教育部](../Page/中華民國教育部.md "wikilink")、河南省政府等处任职。1942年筹建国立[贵州大学](../Page/贵州大学.md "wikilink")，出任校长。1949年到[台湾](../Page/台湾.md "wikilink")，曾主持正中书局。

[C張](../Page/category:中華民國教育家.md "wikilink")
[文C張](../Page/category:中央大学校友.md "wikilink")
[C張](../Page/category:南京大学校友.md "wikilink")

[C張](../Category/中華民國歷史學家.md "wikilink")
[文](../Category/國立中央大學校友.md "wikilink")
[Category:第2屆中華民國考試委員](../Category/第2屆中華民國考試委員.md "wikilink")
[Category:第3屆中華民國考試委員](../Category/第3屆中華民國考試委員.md "wikilink")
[Category:倫敦大學校友](../Category/倫敦大學校友.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:安顺人](../Category/安顺人.md "wikilink")
[Category:台灣戰後貴州移民](../Category/台灣戰後貴州移民.md "wikilink")