**张志贤**（英文名：，），祖籍广东[饶平县](../Page/饶平县.md "wikilink")[樟溪镇锡坑村](../Page/樟溪镇.md "wikilink")，生于[新加坡](../Page/新加坡.md "wikilink")。新加坡副總理兼内政部长、国安部长，[人民行动党第二助理秘书长](../Page/人民行动党.md "wikilink")。1973年荣获[新加坡总统奖学金及](../Page/新加坡总统奖学金.md "wikilink")[新加坡武装部队奖学金赴英国](../Page/新加坡武装部队奖学金.md "wikilink")[曼彻斯特大学深造](../Page/曼彻斯特大学.md "wikilink")。1977年,他继续在[倫敦帝國學院深造获得理科硕士](../Page/倫敦帝國學院.md "wikilink")。1986年获美国[哈佛大学](../Page/哈佛大学.md "wikilink")[肯尼迪行政管理学院公共行政学硕士学位](../Page/肯尼迪行政管理学院.md "wikilink")。1991年升[准将还担任了新加坡共和国海军](../Page/准将.md "wikilink")[总长](../Page/总长.md "wikilink")。同年辞去军职当选为马林百列集选区国会议员，后在1997年,
2001年 及2006年的大选中继续当选白沙－榜鹅集选区国会议员。
1995年4月任代环境部长兼国防部高级政务部长。1996年1月，任环境部长兼国防部第二部长。1997年到2003年,
任教育部长兼国防部第二部长。 2003年8月任国防部长。2009年4月任副总理兼国防部长。2011年5月任副总理兼内政部长、国安部长。

## 家庭

张志贤也是当年[孙中山在新加坡创立](../Page/孙中山.md "wikilink")[同盟会新加坡分会会长](../Page/同盟会.md "wikilink")[张永福的曾侄孙](../Page/张永福.md "wikilink")（其弟张华丹之曾孙）、新加坡先贤[林义顺的表侄孙](../Page/林义顺.md "wikilink")（其舅张华丹之曾孙）。

张志贤育有一个女儿和一个儿子。

[Category:新加坡國會議員](../Category/新加坡國會議員.md "wikilink")
[Category:新加坡國防部長](../Category/新加坡國防部長.md "wikilink")
[Category:饶平人](../Category/饶平人.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")
[Category:曼徹斯特大學校友](../Category/曼徹斯特大學校友.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:新加坡華人](../Category/新加坡華人.md "wikilink")