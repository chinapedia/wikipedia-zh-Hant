**张月姣**（
），女，[中国](../Page/中国.md "wikilink")[吉林人](../Page/吉林.md "wikilink")；法学硕士，教授；1985年取得[中国](../Page/中国.md "wikilink")[律师资格](../Page/律师.md "wikilink")。现任，[世界贸易组织常设上诉机构](../Page/世界贸易组织.md "wikilink")“七人小组”成员，[北京君合律师事务所执业律师](../Page/北京.md "wikilink")、资深顾问，[西非开发银行董事](../Page/西非开发银行.md "wikilink")（已离任），中国国际经济法学会副会长，[中国国际贸易仲裁委员会仲裁员](../Page/中国国际贸易仲裁委员会.md "wikilink")，[汕头大学法学院](../Page/汕头大学.md "wikilink")、[北京师范大学珠海分校法律与行政学院教授](../Page/北京师范大学珠海分校.md "wikilink")。

## 生平

张月姣在“[文革](../Page/文革.md "wikilink")”前毕业于[北京师范大学第二附属中学](../Page/北京师范大学第二附属中学.md "wikilink")，后被派赴[法国留学](../Page/法国.md "wikilink")。1968年获[法国汉纳大学学士学位](../Page/法国.md "wikilink")。回国后，张月姣进入在四川东方汽轮机厂工作；1972年，调入国家机械工业部技术经济研究所工作；其间曾赴[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[法国](../Page/法国.md "wikilink")、[德国工作](../Page/德国.md "wikilink")，精通[英语](../Page/英语.md "wikilink")、[法语](../Page/法语.md "wikilink")。1978年回国；先后在国家进出口委员会和国家外国投资委员会任职；其间曾被赴[美国](../Page/美国.md "wikilink")[乔治登法学院](../Page/乔治登法学院.md "wikilink")、[哥伦比亚大学法学院学习](../Page/哥伦比亚大学.md "wikilink")，并获得法学硕士学位。

1982年至1984年间，张月姣曾在[世界银行法律部担任法律顾问](../Page/世界银行.md "wikilink")。回国后，她历任[对外经济贸易部条约法律司副处长](../Page/中华人民共和国对外经济贸易部.md "wikilink")、处长、经济法副研究员，中国国际经济贸易仲裁委员会仲裁员，[北京大学法学院兼职教授等职](../Page/北京大学.md "wikilink")。1987年至1999年间，张月姣曾被选为国际统一私法协会理事、国际发展法学院董事；并参与制订了“国际融资租赁公约”，“商事合同通则”等。1990年，她升任对[对外经贸部条约法律司副司长](../Page/中华人民共和国对外经济贸易部.md "wikilink")、司长，是[中国在国际经济法领域中的高级专家](../Page/中国.md "wikilink")。在担任条约法律司司长期间，作为中美知识产权谈判代表、以及中国入关谈判的法律顾问，在[吴仪领导下积极参与了中美知识产权谈判的工作](../Page/吴仪.md "wikilink")\[1\]。

1997年，53岁的张月姣再次赴[美国求学](../Page/美国.md "wikilink")，后在[哥伦比亚大学法学院完成](../Page/哥伦比亚大学.md "wikilink")
JSD学业，并教授中国法律。自1998年起，张月姣一直在[亚洲开发银行任职](../Page/亚洲开发银行.md "wikilink")；历任助理法律总顾问、东亚局副局长、嵋公局副局长、上诉委员会主席、欧洲局局长等职；2005年1月，退休。不久，在[中国人民银行行长](../Page/中国人民银行.md "wikilink")[周小川和](../Page/周小川.md "wikilink")[商务部部长](../Page/中华人民共和国商务部.md "wikilink")[薄熙来的推荐下](../Page/薄熙来.md "wikilink")，张月姣又“披挂上阵”——出任[中国在](../Page/中国.md "wikilink")[西非开发银行的首任董事](../Page/西非开发银行.md "wikilink")（已于2007年卸任）\[2\]；同时，她还是[中国人民银行的参事](../Page/中国人民银行.md "wikilink")\[3\]。

2007年11月27日，[世界贸易组织通过任命](../Page/世界贸易组织.md "wikilink")，任命张月姣等四人，成为其常设上诉机构“七人小组”中的一员。四年的任期由2008年6月开始\[4\]。这使她成为[世界贸易组织最高仲裁机构的首位中国籍](../Page/世界贸易组织.md "wikilink")“大法官”。自2012年6月，開始其另一四年的任期。

2012年获颁北京师范大学-香港浸会大学联合国际学院荣誉院士荣衔。\[5\]

## 工作领域

张月姣有着长期国际贸易、[金融和投资法律的经验](../Page/金融.md "wikilink")，又曾在中国的技术经济情报所工作。在[北京君合律师事务所](../Page/北京.md "wikilink")，她主要受理争议解决、知识产权和外贸等方面的案件\[6\]。

## 学术成就

张月姣曾发表学术论文200余篇；参与或主持起草过中国许多重要的涉及外经贸的法律法规。如，《民法通则》、《涉外经济合同法》、《公司法》、《外贸法》、《反倾销条例》、《反补贴条例》、《专利法》、《商标法》、《版权法》和《计算机软件保护条例》等。

## 参考文献

[Category:吉林律师](../Category/吉林律师.md "wikilink")
[Category:中华人民共和国政府官员](../Category/中华人民共和国政府官员.md "wikilink")
[Category:中华人民共和国女律师](../Category/中华人民共和国女律师.md "wikilink")
[Category:北京師範大學教授](../Category/北京師範大學教授.md "wikilink")
[Category:汕頭大學教授](../Category/汕頭大學教授.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Yue月](../Category/张姓.md "wikilink")
[Category:改革先锋称号获得者](../Category/改革先锋称号获得者.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")

1.  [国家外贸部条约法律司前司长张月姣女士受聘为我院教授](http://www.bnusgl.com/News/CollegeNews/200502/20050228030434.htm)
2.  [央行行长周小川的西非特使](http://lgj.mofcom.gov.cn/aarticle/c/200612/20061204176548.html)
    《21世纪经济报道》 滕晓萌
3.  [中国人民银行参事张月姣](http://finance.sina.com.cn/hy/20061219/18053178460.shtml)
    新浪网财经
4.  [WTO appoints four new Appellate Body
    members](http://www.wto.org/english/news_e/pres07_e/pr501_e.htm)
    世界贸易组织网站
5.
6.  [张月姣](http://www.findalawyer.cn/lawyers/profile/tourism,%20hotel/beijing/dongcheng/132_2_17_183564/lawyer/%E5%BC%A0%E6%9C%88%E5%A7%A3.php?sd=725)
    励德·爱思唯尔律师搜索网