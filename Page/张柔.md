**张柔**（），字德刚，[金末](../Page/金朝.md "wikilink")[元初](../Page/元朝.md "wikilink")[易州定兴](../Page/易州.md "wikilink")（[辽朝时期属于](../Page/辽朝.md "wikilink")[南京道管辖](../Page/南京道.md "wikilink")，[金朝时期属于](../Page/金朝.md "wikilink")[中都路管辖](../Page/中都路.md "wikilink")，今属[河北](../Page/河北.md "wikilink")）人，[元朝名将](../Page/元朝.md "wikilink")。

## 生平

世代務農。金末[金宣宗下诏各地人民聚众结寨自保抵抗蒙古](../Page/金宣宗.md "wikilink")，被任为定兴令，[苗道润很赏识张柔](../Page/苗道润.md "wikilink")，加昭毅大將軍，委以重任。官至中都留守兼知大兴府事。苗道潤為其副使[賈瑀所殺](../Page/賈瑀.md "wikilink")。

### 降蒙

1218年蒙古军突袭紫荆口，张柔兵败投降蒙古。张柔知苗道潤已死，誓師復仇，攻下雄、易、安、保诸州。贾瑀盘踞孔山抵抗，因汲水被断，贾瑀最後投降，张柔剖其心以祭苗道润。

[元太宗五年](../Page/元太宗.md "wikilink")（1233年）从蒙古军参加攻金[汴京](../Page/汴京.md "wikilink")（今开封）之役，取《[金实录](../Page/金实录.md "wikilink")》而归；1234年破蔡州（今汝南）之战负伤，以功升至万户；后屡次攻宋，曾从忽必烈围攻鄂州(今武昌)，[世祖时封蔡国公](../Page/世祖.md "wikilink")。

[中统二年](../Page/中统.md "wikilink")（1261年）以年老致仕，封安肃公。[至元三年](../Page/至元_\(元世祖\).md "wikilink")（1266年），又起判行工部事，营建[元大都](../Page/元大都.md "wikilink")。至元四年（1267年）正月，封[蔡国公](../Page/蔡国公.md "wikilink").

[至元五年六月二十九日](../Page/至元_\(元世祖\).md "wikilink")（1268年8月9日）\[1\]，张柔病逝，终年七十九岁。

[元世祖赠推忠宣力翊运功臣](../Page/元世祖.md "wikilink")、[太师](../Page/太师.md "wikilink")、[开府仪同三司](../Page/开府仪同三司.md "wikilink")、上柱国，谥武康。[延祐五年](../Page/延祐.md "wikilink")（1318年），[元仁宗加封](../Page/元仁宗.md "wikilink")[汝南王](../Page/汝南王.md "wikilink")，谥忠武。

## 家庭

有子十一人，张弘略、张弘範官职最显赫，第九子即灭[南宋之](../Page/宋朝.md "wikilink")[元朝镇国大将军](../Page/元朝.md "wikilink")[张弘範](../Page/张弘範.md "wikilink")。

## 参考资料

  - [《元史》卷一百四十七《张柔传》](http://www.guoxue.com/shibu/24shi/yuanshi/yuas_147.htm)
  - [《新元史》卷一百三十九《张柔传》](http://www.guoxue123.com/shibu/0101/00xys/138.htm)

## 注释

[Z](../Category/金朝人.md "wikilink") [Z](../Category/元朝人.md "wikilink")
[Z](../Category/定兴人.md "wikilink") [柔](../Category/张姓.md "wikilink")
[Category:谥武康](../Category/谥武康.md "wikilink")
[Category:谥忠武](../Category/谥忠武.md "wikilink")

1.  《新元史》卷八《世祖本纪二》记载：至元五年六月，“己酉，蔡国公张柔卒”。至元五年六月己酉日是至元五年六月二十九日。