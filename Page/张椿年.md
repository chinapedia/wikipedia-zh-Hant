**张椿年**出生于[江苏](../Page/江苏.md "wikilink")[溧阳](../Page/溧阳.md "wikilink")，毕业于[苏联](../Page/苏联.md "wikilink")[列宁格勒大学](../Page/列宁格勒大学.md "wikilink")，为[中国社会科学院世界历史研究所研究员](../Page/中国社会科学院.md "wikilink")，[国家社会科学基金历史学科组组长](../Page/国家社会科学基金.md "wikilink")。\[1\]

## 作品

### 独著

  - 《从信仰到理性：意大利人文主义研究》，[方志出版社](../Page/方志出版社.md "wikilink")，2007年，ISBN
    9787802380233

### 合著

  - 《第二次世界大战史》，[人民出版社](../Page/人民出版社.md "wikilink")，[朱贵生](../Page/朱贵生.md "wikilink")，[王振德](../Page/王振德.md "wikilink")，张椿年，2005年，ISBN
    9787010049922

## 参考文献

## 外部链接

[Category: 1931年出生](../Category/_1931年出生.md "wikilink")
[C椿](../Category/_张姓.md "wikilink") [Category:
列宁格勒大学校友](../Category/_列宁格勒大学校友.md "wikilink")
[Category: 溧阳人](../Category/_溧阳人.md "wikilink")

1.