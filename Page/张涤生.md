**张涤生**（\[1\]\[2\]），中国医学家，[中国工程院院士](../Page/中国工程院院士.md "wikilink")，中国[整复外科的重要创始人](../Page/整复外科.md "wikilink")，被称为“中国[整形外科之父](../Page/整形外科.md "wikilink")”。曾是[上海交通大学医学院终身教授](../Page/上海交通大学医学院.md "wikilink")、[博士生导师](../Page/博士生导师.md "wikilink")，[上海市第九人民医院院长](../Page/上海市第九人民医院.md "wikilink")、整复外科主任，[上海市整复外科研究所所长](../Page/上海市整复外科研究所.md "wikilink")、名誉所长。\[3\]

## 生平

1916年6月12日生于[江苏省](../Page/江苏省.md "wikilink")[无锡县](../Page/无锡县.md "wikilink")，父亲是银行[会计](../Page/会计.md "wikilink")，幼年时随父在[长春](../Page/长春.md "wikilink")、[北京](../Page/北京.md "wikilink")、[天津等地生活](../Page/天津.md "wikilink")。后回到无锡念初中，1932年就读于私立无锡中学。

1935年考入[国立中央大学医学院](../Page/國立中央大學_\(南京\).md "wikilink")（今[第四军医大学](../Page/第四军医大学.md "wikilink")）。1937年[抗日战争爆发](../Page/抗日战争.md "wikilink")，随中大医学院西迁[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")[华西坝](../Page/华西坝.md "wikilink")。1941年毕业于国立中央大学医学院，获[医学学士学位](../Page/医学学士.md "wikilink")。毕业后，在[贵阳任](../Page/贵阳.md "wikilink")“[中国红十字会救护总队部](../Page/中国红十字会.md "wikilink")”医师，从事治疗抗战伤员的工作。\[4\]

抗战胜利后，1946年至1948年，在[美国](../Page/美国.md "wikilink")[宾夕法尼亚大学学习整形外科](../Page/宾夕法尼亚大学.md "wikilink")，师从现代整形外科先驱\[5\]。

1948年回国，在美国教授来华举办的最早的整形外科进修班担任教员。1949年，在[上海](../Page/上海.md "wikilink")[国立同济大学医学院任教](../Page/华中科技大学同济医学院.md "wikilink")。1950年，参加朝鲜战争中国医疗手术队，在长春成立了中国第一个[烧伤整形中心](../Page/烧伤整形.md "wikilink")。

1956年，任上海[广慈医院](../Page/广慈医院.md "wikilink")（后更名为瑞金医院）[口腔颌面外科主任](../Page/口腔颌面外科.md "wikilink")，参与[抢救丘财康](../Page/抢救丘财康.md "wikilink")，1964年担任[整形外科主任](../Page/整形外科.md "wikilink")。1966年随科迁到上海[第九人民医院](../Page/上海交通大学医学院附属第九人民医院.md "wikilink")，先后担任[上海第二医科大学附属](../Page/上海第二医科大学.md "wikilink")[第九人民医院整复外科主任](../Page/上海交通大学医学院附属第九人民医院.md "wikilink")、第九人民医院院长；创办[上海市整复外科研究所](../Page/上海市整复外科研究所.md "wikilink")，任所长。他主持的上海整复外科研究所，曾被国际权威整形外科专家称为“可同世界上任何一个同类专业中心媲美”\[6\]。

1996年，当选为[中国工程院医药卫生学部院士](../Page/中国工程院.md "wikilink")\[7\]。

## 学术成就

张涤生创立了“烘绑疗法”概念，开创了中国淋巴学科先河。他是中国第一个应用显微外科技术进行科学实验的人，1964年开始做吻合小血管游离皮瓣的动物实验，1970年代初期逐步将显微外科应用于临床，是中国显微外科的创始人之一。\[8\]

张涤生将[显微外科与](../Page/显微外科.md "wikilink")[整形外科有机的结合](../Page/整形外科.md "wikilink")，突破传统“整形外科”的观念，倡导把外形的修整与功能的恢复有机地统一起来，形成了以外形修整与功能恢复有机统一的“整复外科”新概念，从而使传统的整形外科得到空前的发展。
他认为，“整”是指对人体组织器官缺损[畸形在形态上进行修整](../Page/畸形.md "wikilink")；“复”则是使病人在生理功能上进行最大限度的恢复。在张涤生的倡导下，整复外科已突破了以往“颌面整形”和“美容”的范畴，扩大了治疗范围，处理范围涉及各种先天或创伤的四肢畸形、颅颌面畸形以及烧伤畸形。\[9\]

## 著作

  - 主编：

<!-- end list -->

  - 《整复外科学》
  - 《修复重建外科学》
  - 《实用美容外科学》
  - 《颅面外科学》
  - 《整形外科手术图解》
  - 《张涤生整复外科学》

## 学术兼职

  - [中国康复医学会修复重建外科委员会主任委员](../Page/中国康复医学会.md "wikilink")
  - [中华医学会整形外科学会副主任委员](../Page/中华医学会.md "wikilink")
  - 国际显微外科大会主席
  - 《[美国整形外科学报](../Page/美国整形外科学报.md "wikilink")》特邀主编

## 注释

## 参考文献

## 外部链接

  - [上海交通大学 » 师资队伍 » 两院院士 » 中国工程院院士 »
    张涤生](https://web.archive.org/web/20111028114740/http://www.sjtu.edu.cn/info/news/content_02/1447.htm)
  - [上海第九人民医院 \> 院士风采 \>
    张涤生院士](http://www.9hospital.com/default.php?mod=article&do=detail&tid=334889&fid=11225)

{{-}}

[Category:上海交通大学教师](../Category/上海交通大学教师.md "wikilink")
[Category:上海交通大学医学院附属第九人民医院医生](../Category/上海交通大学医学院附属第九人民医院医生.md "wikilink")
[Category:中国工程院医药卫生学部院士](../Category/中国工程院医药卫生学部院士.md "wikilink")
[Category:中华人民共和国医学家](../Category/中华人民共和国医学家.md "wikilink")
[Category:中华人民共和国外科医生](../Category/中华人民共和国外科医生.md "wikilink")
[Category:中华民国军医](../Category/中华民国军医.md "wikilink")
[医](../Category/国立中央大学校友.md "wikilink")
[医](../Category/中央大学校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[Category:中国人民解放军空军军医大学校友](../Category/中国人民解放军空军军医大学校友.md "wikilink")
[Category:賓夕法尼亞大學校友](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:同济大学教授](../Category/同济大学教授.md "wikilink")
[Category:无锡人](../Category/无锡人.md "wikilink")
[Di滌生](../Category/張姓.md "wikilink")

1.

2.

3.

4.
5.
6.

7.
8.
9.