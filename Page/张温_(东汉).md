**張温**（），字**伯慎**。[東漢末期](../Page/東漢.md "wikilink")[南阳郡](../Page/南阳郡.md "wikilink")[穰县](../Page/穰县.md "wikilink")（今河南省[邓州市](../Page/邓州市.md "wikilink")）人，位至[三公](../Page/三公.md "wikilink")，曾經為[董卓](../Page/董卓.md "wikilink")、[孫堅](../Page/孫堅.md "wikilink")、[陶謙等人的上司](../Page/陶謙.md "wikilink")。

## 生平

### 京中為官

張温曾受[曹操祖父](../Page/曹操.md "wikilink")[曹騰所提拔](../Page/曹騰.md "wikilink")。[中平元年](../Page/中平.md "wikilink")（184年），張溫當時為[大司農](../Page/大司農.md "wikilink")，受封為[司空](../Page/司空.md "wikilink")。同年張溫舉薦[討虜校尉](../Page/討虜校尉.md "wikilink")[蓋勳為](../Page/蓋勳.md "wikilink")[京兆尹](../Page/京兆尹.md "wikilink")。

### 奉命討賊

中平二年（185年），為[車騎將軍](../Page/車騎將軍.md "wikilink")，與副將[執金吾](../Page/執金吾.md "wikilink")[袁滂討伐胡人](../Page/袁滂.md "wikilink")[北宮伯玉](../Page/北宮伯玉.md "wikilink")。當時[董卓為](../Page/董卓.md "wikilink")[破虜將軍](../Page/破虜將軍.md "wikilink")、[蕩寇將軍](../Page/蕩寇將軍.md "wikilink")[周慎](../Page/周慎.md "wikilink")、別駕從事[孫堅與議郎](../Page/孫堅.md "wikilink")[陶謙皆被攬至張溫帳下](../Page/陶謙.md "wikilink")。張溫領兵十餘萬駐紮[美陽](../Page/美陽.md "wikilink")，與[邊章](../Page/邊章.md "wikilink")、[韓遂等會戰](../Page/韓遂.md "wikilink")，初期戰勝後派出周慎、董卓等追擊，但因不聽孫堅建言而無法取得有利形勢，後來戰況陷入膠著的局面而撤兵，班師回朝後陶謙亦不滿張溫的無能多次私下批評抱怨。

### 位至三公

中平三年（186年），於[長安拜張溫為](../Page/長安.md "wikilink")[太尉](../Page/太尉.md "wikilink")，成為第一位不在朝廷的三公，有傳言張溫以行賄方式得到此位。同年張溫受徵召回京師。

### 事業滑落

中平四年（187年），張溫因未能平亂被罷免。[初平二年](../Page/初平.md "wikilink")（191年），當時張溫為[衛尉](../Page/衛尉.md "wikilink")，並不與董卓結交，為董卓所怨恨，所以董卓誣蔑張溫與[袁術私通](../Page/袁術.md "wikilink")，於市街上被拷打致死。

## 演義記載

登場於第八回「王司徒巧使連環計
董太師大鬧鳳儀亭」，董卓於長安宴請百官時，因[呂布接獲袁術與張溫私通書信](../Page/呂布.md "wikilink")，乃登堂密告董卓；董卓獲報後，立刻命令呂布將張溫從宴席中拖出斬首，並將首級呈上，百官見之俱感顫慄，唯董卓一人飲食談笑自若。

## 部下

  - [董卓](../Page/董卓.md "wikilink")：為[破虜將軍](../Page/破虜將軍.md "wikilink")，一同出征邊章等人。
  - [陶謙](../Page/陶謙.md "wikilink")：為[參軍](../Page/參軍.md "wikilink")，認為張温才能不及[皇甫嵩而輕視張温](../Page/皇甫嵩.md "wikilink")。
  - [孫堅](../Page/孫堅.md "wikilink")：為[參軍](../Page/參軍.md "wikilink")，認為張温應殺怠慢軍機的董卓以立軍威，不為張温採納。
  - [張純](../Page/張純_\(漢靈帝\).md "wikilink")：張純自薦為將討伐邊章不為張温採納，心生不憤而叛亂。

## 評價

  - [晉](../Page/晉.md "wikilink")[傅玄曰](../Page/傅玄.md "wikilink")：「溫有傑才。」
  - [東漢](../Page/東漢.md "wikilink")[劉陶曰](../Page/劉陶.md "wikilink")：「將軍張溫，天性精勇。」

## 參考資料

  - 《[三國志](../Page/三國志.md "wikilink")·[魏書一·武帝紀](../Page/s:三國志/卷01.md "wikilink")》
  - 《[三國志](../Page/三國志.md "wikilink")·[魏書六·董二袁劉傳](../Page/s:三國志/卷06.md "wikilink")》
  - 《[三國志](../Page/三國志.md "wikilink")·[魏書八·二公孫陶四張傳](../Page/s:三國志/卷08.md "wikilink")》
  - 《[三國志](../Page/三國志.md "wikilink")·[吳書一·孫破虜討逆傳](../Page/s:三國志/卷46.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[卷八·孝靈帝紀第八](../Page/s:後漢書/卷8.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[後漢書卷五十二·崔駰列傳·第四十二](../Page/s:後漢書/卷52.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[後漢書卷五十七·杜欒劉李劉謝列傳·第四十七](../Page/s:後漢書/卷57.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[後漢書卷五十八·虞傅蓋臧列傳·第四十八](../Page/s:後漢書/卷58.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[後漢書卷七十二·董卓列傳·第六十二](../Page/s:後漢書/卷72.md "wikilink")》
  - 《[後漢書](../Page/後漢書.md "wikilink")·[卷七十三·劉虞公孫瓚陶謙列傳第六十三](../Page/s:後漢書/卷73.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十](../Page/s:資治通鑑/卷058.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十二](../Page/s:資治通鑑/卷060.md "wikilink")》

[Category:东汉太尉](../Category/东汉太尉.md "wikilink")
[Category:东汉司空](../Category/东汉司空.md "wikilink")
[Category:东汉将军](../Category/东汉将军.md "wikilink")
[Category:东汉被杀害人物](../Category/东汉被杀害人物.md "wikilink")
[W温](../Category/張姓.md "wikilink")
[Category:邓州人](../Category/邓州人.md "wikilink")