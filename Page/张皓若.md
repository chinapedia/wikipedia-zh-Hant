**張皓若**()，[河南](../Page/河南.md "wikilink")[焦作人](../Page/焦作.md "wikilink")，[清华大学化工系石油专业毕业](../Page/清华大学.md "wikilink")。1950年加入[中國共產黨](../Page/中國共產黨.md "wikilink")。曾任[國内貿易部首任部長](../Page/中華人民共和國國內貿易部.md "wikilink")，[四川省](../Page/四川省.md "wikilink")[省长](../Page/省长.md "wikilink")（1988年1月－1993年2月）。\[1\]

他是[河南大学校长](../Page/河南大学.md "wikilink")、河南省政协副主席[张仲鲁的三子](../Page/张仲鲁.md "wikilink")。

## 生平

1952年大学毕业后，被分配到西北石油管理局工作；1953年，在石油部俄文训练班学习；1954年6月，赴[蘇聯巴庫煉油廠實習](../Page/蘇聯.md "wikilink")。1956年回国后，参与筹建[蘭州煉油廠](../Page/蘭州煉油廠.md "wikilink")，并一直在该厂工作20年；历任兰炼筹建处技术员、科长、工程师、车间主任、分厂厂长、炼油厂副总工程师。\[2\]

1978年，张皓若调入[石油工业部](../Page/中华人民共和国石油工业部.md "wikilink")，先后担任生產司處長、煉油化工組副組長；此后，历任[化工部計劃司司長](../Page/中华人民共和国化学工业部.md "wikilink")、國家能源委員會科技局局長、[中国石化总公司副總經理等职](../Page/中国石化集团.md "wikilink")。1986年，出任[對外經濟貿易部副部長](../Page/對外經濟貿易部.md "wikilink")；1988年外放[四川](../Page/四川.md "wikilink")，任[省委副書記](../Page/省委书记.md "wikilink")、兼省長；1992年，再次回到中央，任[輕工業部副部長](../Page/輕工業部.md "wikilink")、黨組副書記；1993年，被任命为新组建的國内貿易部首任部長；1995年，转任[國家經濟體制改革委員會副主任](../Page/國家經濟體制改革委員會.md "wikilink")、黨組書記。1998年3月，当选第九屆[全國人大常委](../Page/全國人大.md "wikilink")、環境與資源保護委員會副主任委員。\[3\]

2004年3月27日在[北京逝世](../Page/北京.md "wikilink")，享年72歲。\[4\]

## 参考文献

[Category:中华人民共和国四川省省长](../Category/中华人民共和国四川省省长.md "wikilink")
[Category:国务院已撤销各部部长](../Category/国务院已撤销各部部长.md "wikilink")
[Category:第七届全国人大代表](../Category/第七届全国人大代表.md "wikilink")
[Category:第八届全国人大代表](../Category/第八届全国人大代表.md "wikilink")
[Category:第九届全国人大常委会委员](../Category/第九届全国人大常委会委员.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink") [Category:中国共产党党员
(1950年入党)](../Category/中国共产党党员_\(1950年入党\).md "wikilink")
[Category:焦作人](../Category/焦作人.md "wikilink")
[H](../Category/张姓.md "wikilink")
[Category:国家经济体制改革委员会副主任](../Category/国家经济体制改革委员会副主任.md "wikilink")
[Category:重庆市南开中学校友](../Category/重庆市南开中学校友.md "wikilink")

1.

2.
3.
4.