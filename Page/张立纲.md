**张立纲**（，），[物理学家](../Page/物理学家.md "wikilink")。原籍[吉林](../Page/吉林.md "wikilink")[永吉](../Page/永吉.md "wikilink")，出生于[河南](../Page/河南.md "wikilink")[开封](../Page/开封.md "wikilink")。张立纲先生乃为著名华裔物理学家、半导体物理、材料科学与器件等多科性交叉形成的前沿领域——半导体量子阱、超晶格的主要奠基人之一。

## 生平

1949年來到[臺灣](../Page/臺灣.md "wikilink")，在此長大。1957年自[國立臺灣大學電機工程系毕业後](../Page/國立臺灣大學.md "wikilink")，赴美留學，1961年，取得[南卡羅來納大學](../Page/南卡羅來納大學.md "wikilink")（電子/電機工程系）碩士學位。1963年，获[斯坦福大学](../Page/斯坦福大学.md "wikilink")（固態電子/電機工程系）博士学位。

1963年\~1968年及1969年\~1975年在[美國國際商業機器公司任研究員](../Page/美國.md "wikilink")，1968年\~1969年任美國麻省理工學院電機系副教授，1975年\~1993年則擔任美國國際商業機器公司研究部經理一職。後於[香港科技大学創校](../Page/香港科技大学.md "wikilink")，任理学院院长（1993年至1998年）、副校长(1998年至2000年)。[美国国家科学院院士](../Page/美国国家科学院.md "wikilink")、[工程院院士](../Page/美国国家工程院.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[中央研究院第](../Page/中央研究院.md "wikilink")20屆(數理科學組)院士\[1\]，[中国科学院外籍院士](../Page/中国科学院外籍院士.md "wikilink")。

## 家庭

张立纲父亲[张莘夫](../Page/张莘夫.md "wikilink")，工程师，1946年在东北被中共和苏俄杀害。\[2\]

张立纲女兒[张彤禾](../Page/张彤禾.md "wikilink") (Leslie T.
Chang)，美籍華裔作家。女婿[彼得·海斯勒](../Page/彼得·海斯勒.md "wikilink")，美国作家。\[3\]

## 参考资料

[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:半導體物理學家](../Category/半導體物理學家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:美国国家工程院院士](../Category/美国国家工程院院士.md "wikilink")
[Category:美國物理學會院士](../Category/美國物理學會院士.md "wikilink")
[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:美籍华裔科学家](../Category/美籍华裔科学家.md "wikilink")
[Category:國立臺灣大學電機資訊學院校友](../Category/國立臺灣大學電機資訊學院校友.md "wikilink")
[C](../Category/史丹佛大學校友.md "wikilink")
[C](../Category/南卡羅萊納大學校友.md "wikilink")
[Category:开封人](../Category/开封人.md "wikilink")
[L立](../Category/张姓.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:IEEE Fellow](../Category/IEEE_Fellow.md "wikilink")
[Category:香港科技大学副校长](../Category/香港科技大学副校长.md "wikilink")

1.  [張立綱 逝世院士一覽表](https://academicians.sinica.edu.tw/index.php?func=1-D)
    中央研究院
2.
3.