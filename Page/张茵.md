**张茵**
，[中国人](../Page/中国.md "wikilink")，出生于[广东](../Page/广东.md "wikilink")[韶關](../Page/韶關.md "wikilink")，祖籍[黑龍江省](../Page/黑龍江省.md "wikilink")[鸡西市](../Page/鸡西市.md "wikilink")，[中国女](../Page/中国.md "wikilink")[企业家](../Page/企业家.md "wikilink")，[玖龙造纸有限公司董事长](../Page/玖龙造纸.md "wikilink")，2006年其家族財富總值270亿元[人民幣](../Page/人民幣.md "wikilink")，目前是[中國大陸十大富翁之一](../Page/中國大陸.md "wikilink")。

## 生平

  - 1976年，文化大革命結束後，張茵則從事會計工作。
  - 1982年，张茵离开韶關，到[深圳發展](../Page/深圳.md "wikilink")。
  - 1985年，她带着3万元到[香港创业](../Page/香港.md "wikilink")，投身废纸回收行业，受到黑社會打壓發展不順。
  - 1988年，在[广东](../Page/广东.md "wikilink")[东莞建立了自己的独资工厂](../Page/东莞.md "wikilink")。
  - 1990年，[劉名中张茵夫婦把事业重心由巴西](../Page/劉名中.md "wikilink")、香港遷往[美国](../Page/美国.md "wikilink")，成立美国中南有限公司，初由精通多國語言的劉名中任CEO。
  - 1996年，美商中南控股在[广东](../Page/广东.md "wikilink")[东莞投资](../Page/东莞.md "wikilink")1.1亿美元成立子公司玖龙纸业，用废纸造纸。
  - 2006年3月，玖龙纸业在香港上市，募集资金34亿港元。
  - 2015年1月23日，張茵以8505.07萬元，實用呎價35736元向信和置業購入大埔白石角逸瓏灣II期第6座17樓A室，實用面積2380方呎，平台面積2772方呎，另外張成飛(張茵胞弟)以1.878242億元，平均呎價18717元購入大埔白石角逸瓏灣II期第6座10樓A室、11樓B室、12樓A室、12樓B室、15樓A室、15樓B室，涉及總樓面面積約10035方呎連同該家族其他成員，包括張茵丈夫、玖紙副董事長兼行政總裁劉名中，以及張茵兒子劉晉嵩等人，合共以接近3.7億元購入該盤10個單位，以總面積17485方呎計，呎價約21148元。

2006年上市后，玖龙纸业股票价格不断飚升，半年间张茵的财富也因此激增175亿港元，成為百富榜的第一名，並超出第二名，即2005年首富[国美电器主席](../Page/国美电器.md "wikilink")[黄光裕](../Page/黄光裕.md "wikilink")70亿元。而她在[福布斯中國富豪榜則排名第](../Page/福布斯中國富豪榜.md "wikilink")5位。

## 相關內部链接

  - [玖龙造纸](../Page/玖龙造纸.md "wikilink")

## 資料來源

  - [中国第一位女首富张茵简介](http://www.55www.com/renwu/qiyejia/zhangyin.htm)
  - [张茵简介](http://finance.sina.com.cn/wealthperson/2004-04-29/199.html)
  - [张茵简介](http://money.163.com/special/00252NCL/zhangyin.html)

[Category:中国首席执行官](../Category/中国首席执行官.md "wikilink")
[Category:中華人民共和國女企業家](../Category/中華人民共和國女企業家.md "wikilink")
[Category:中華人民共和國億萬富豪](../Category/中華人民共和國億萬富豪.md "wikilink")
[Category:女性億萬富豪](../Category/女性億萬富豪.md "wikilink")
[Category:女性全国政协委员](../Category/女性全国政协委员.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十屆全國政協委員](../Category/第十屆全國政協委員.md "wikilink")
[Category:鸡西人](../Category/鸡西人.md "wikilink")
[Category:韶關人](../Category/韶關人.md "wikilink")
[Y茵](../Category/张姓.md "wikilink")