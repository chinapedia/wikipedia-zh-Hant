**张议潮**（又作**义潮**）（），[唐朝沙州敦煌郡](../Page/唐朝.md "wikilink")（今[甘肃](../Page/甘肃.md "wikilink")[敦煌](../Page/敦煌.md "wikilink")）人，首任[归义军节度使](../Page/归义军.md "wikilink")。

张议潮是沙州本地的豪族出身。他于848年与洪辩、安景达等人在沙州起事并很快推翻[吐蕃的统治](../Page/吐蕃.md "wikilink")，之后在851年，他派遣使者前往[长安向](../Page/长安.md "wikilink")[唐朝表示臣服](../Page/唐朝.md "wikilink")。张议潮因此被封为沙州防御使。同年冬季，他又发兵控制沙州旁[陇右道的十州并派自己的兄长张议泽带十一州图籍再次到长安面见](../Page/陇右道.md "wikilink")[唐宣宗](../Page/唐宣宗.md "wikilink")。唐朝于是在沙州设置[归义军并让张议潮担任节度使兼十一州观察使](../Page/归义军.md "wikilink")。张议潮此后与[吐蕃和](../Page/吐蕃.md "wikilink")[回鹘多次交战](../Page/回鹘.md "wikilink")，互有胜负。867年张议潮前往长安并被封为右神武统军，而归义军则由他的侄子[张淮深负责](../Page/张淮深.md "wikilink")。872年，张议潮去世。

张议潮的事迹在[旧唐书](../Page/旧唐书.md "wikilink")、[新唐书与](../Page/新唐书.md "wikilink")[资治通鉴上记载不多且存在错误](../Page/资治通鉴.md "wikilink")。目前对他生平的较为详细的记载主要来自于敦煌[莫高窟出土的写本与其他文物](../Page/莫高窟.md "wikilink")。
[Dunhuang_Zhang_Yichao_army.jpg](https://zh.wikipedia.org/wiki/File:Dunhuang_Zhang_Yichao_army.jpg "fig:Dunhuang_Zhang_Yichao_army.jpg")第156窟\]\]

## 早年

有关张议潮早年的记载并不多，但其家族是地方望族。张氏起源于清河，之后在[东汉时迁到](../Page/东汉.md "wikilink")[敦煌郡](../Page/敦煌郡.md "wikilink")\[1\]。张议潮的父亲张谦逸曾在[吐蕃担任沙州都督一职](../Page/吐蕃.md "wikilink")\[2\]。同时敦煌望族之间普遍崇尚[佛教](../Page/佛教.md "wikilink")\[3\]。莫高窟156窟居信是

张议潮曾跟随吐蕃僧人法成学习[吐蕃文并取吐蕃名](../Page/藏文.md "wikilink")“赞热”\[4\]。

## 沙州举事

自8世纪初，吐蕃攻占沙州地区并设立节儿作为当地长官\[5\]。节儿以下官员是吐蕃人与当地汉人兼用\[6\]。但吐蕃的统治并非稳固，在张议潮举事前，有多次试图推翻吐蕃统治的当地汉人举事\[7\]。842年后，由于[吐蕃分裂时期](../Page/吐蕃陷入内战状态.md "wikilink")，导致自称“宰相”的[论恐热与鄯州](../Page/论恐热.md "wikilink")[尚婢婢在](../Page/尚婢婢.md "wikilink")[河西地区不断发生冲突](../Page/河西地区.md "wikilink")。沙州也进入一种无政府状态。

## 參見

  - [歸義軍](../Page/歸義軍.md "wikilink")

## 参考文獻

  - [向达](../Page/向达.md "wikilink")《罗叔言<补唐书张议潮传>补正》

[category:酒泉人](../Page/category:酒泉人.md "wikilink")

[Category:唐朝軍事人物](../Category/唐朝軍事人物.md "wikilink")
[Category:唐朝官员](../Category/唐朝官员.md "wikilink")
[Y](../Category/张姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.