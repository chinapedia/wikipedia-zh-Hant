**张鼎丞**（），又写为**张鼎亟**，原名**张福仁**，[福建](../Page/福建.md "wikilink")[永定县](../Page/永定县.md "wikilink")[金砂乡](../Page/金砂乡.md "wikilink")[西湖寨村人](../Page/西湖寨村.md "wikilink")，1927年加入[中国共产党](../Page/中国共产党.md "wikilink")。第一任[中华人民共和国最高人民检察院检察长](../Page/中华人民共和国最高人民检察院.md "wikilink")。

张鼎丞于1928年领导[永定暴动](../Page/永定暴动.md "wikilink")，曾担任[中华苏维埃共和国中央执行委员会委员兼土地人民委员](../Page/中华苏维埃共和国.md "wikilink")、[福建省苏维埃政府主席](../Page/福建省苏维埃政府.md "wikilink")，红军[长征后](../Page/长征.md "wikilink")，奉命留守苏区坚持游击战争。在[抗战期间](../Page/抗战.md "wikilink")，担任[新四军第二支队司令员](../Page/新四军.md "wikilink")。[皖南事变后](../Page/皖南事变.md "wikilink")，任新四军第七师师长、[中共中央党校二部主任](../Page/中共中央党校.md "wikilink")。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，任华中军区司令员、[中共中央华东局常委兼组织部部长](../Page/中共中央华东局.md "wikilink")。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，任[中共福建省委书记](../Page/中共福建省委.md "wikilink")、[中华人民共和国最高人民检察院检察长](../Page/中华人民共和国最高人民检察院.md "wikilink")。在[文革期间受迫害](../Page/文革.md "wikilink")，后当选[全国人大常委会副委员长](../Page/全国人大常委会副委员长.md "wikilink")。1981年因病逝世。

## 生平

### 早期革命生涯

张鼎丞早年曾就读于本村私塾和初级小学，后到邻近的丰稔市作新高级小学读书。1924年秋，回乡担任[金砂公学校长](../Page/金砂公学.md "wikilink")。1926年6月，张鼎丞到[大埔县青溪保灵寺小学当教员](../Page/大埔县.md "wikilink")，结识共产党员[饶龙光](../Page/饶龙光.md "wikilink")，接受共产主义思想\[1\]。

1927年5月，张鼎丞参加[大埔暴动](../Page/大埔暴动.md "wikilink")，不久加入[中国共产党](../Page/中国共产党.md "wikilink")\[2\]。7月，他受党组织的派遣回到金砂，秘密发展党员，不久建立中共溪南支部。11月当选中共[永定县委县委委员](../Page/永定县.md "wikilink")。1928年6月，中共永定县委召开党员代表会议，决定领导全县农民武装暴动，推举张鼎丞任暴动总指挥\[3\]。7月他和[邓子恢从起义工农中挑选出数百人](../Page/邓子恢.md "wikilink")，建立起闽西第一个工农红军营，他任营长，邓任党代表。1929年5月，[毛泽东](../Page/毛泽东.md "wikilink")、[朱德率领](../Page/朱德.md "wikilink")[红四军入闽](../Page/红四军.md "wikilink")，张鼎丞带领永定农民武装向当地驻军和民团发起攻击，策应红四军作战。25日协同红四军占领永定城，成立永定县革命委员会，他出任县革委会主席。6月，他被任命为红四军第四纵队党代表。7月下旬，成立[中共闽西特委](../Page/中共闽西特委.md "wikilink")，张鼎丞当选为军委书记。12月，张鼎丞参加[古田会议后](../Page/古田会议.md "wikilink")，率部转战赣南、粤东北，反击闽粤赣三省国军的“会剿”\[4\]。1930年3月，张鼎丞任闽西苏维埃政府执行委员会委员，后又任闽粤赣边军事委员会委员和[彭](../Page/彭湃.md "wikilink")[杨军事学校政治委员等职](../Page/杨殷.md "wikilink")\[5\]。1930年6月，率领部队回到闽西。

1931年11月，张鼎丞当选为[中华苏维埃共和国中央执行委员和临时中央工农民主政府土地委员](../Page/中华苏维埃共和国.md "wikilink")\[6\]。1932年3月，又当选为[福建省苏维埃政府主席](../Page/福建省苏维埃政府.md "wikilink")。在他主持领导下，制订了各项法令、条例，建立了法制，健全了各级苏维埃政府的工作制度。1933年秋，因抵制批判“[罗明路线](../Page/罗明路线.md "wikilink")”，而遭到严厉批判，被撤销福建省苏维埃政府主席职务\[7\]，调任[中华苏维埃共和国粮食部副部长](../Page/中华苏维埃共和国粮食部.md "wikilink")\[8\]。

1934年10月，红军长征后，张鼎丞被中央分局派回福建，到[杭](../Page/上杭县.md "wikilink")、[永边开展游击战争](../Page/永定县.md "wikilink")。1935年夏，张鼎丞担任[闽西南军政委员会主席](../Page/闽西南军政委员会.md "wikilink")，同[邓子恢](../Page/邓子恢.md "wikilink")、[谭震林等坚持了南方三年游击战争](../Page/谭震林.md "wikilink")\[9\]。西安事变发生，张鼎丞同闽西南的国民政府当局谈判，建立了闽西南抗日义勇军第一支队。1937年，任中共闽粤赣边省委书记\[10\]。
[Chen_Yi_in_1947.jpg](https://zh.wikipedia.org/wiki/File:Chen_Yi_in_1947.jpg "fig:Chen_Yi_in_1947.jpg")（坦克上中）与华中军区司令员[张鼎丞](../Page/张鼎丞.md "wikilink")（坦克前左）、山东军区副司令员[张云逸](../Page/张云逸.md "wikilink")（坦克前右）在缴获的坦克前留影\]\]

### 抗日战争与第二次国共内战时期

[抗战爆发之后](../Page/抗战.md "wikilink")，在赣闽活动的中共游击部队与国军进行谈判，并获得成功。1938年3月1日，张鼎丞任[新四军第二支队司令员兼中共东南局委员](../Page/新四军.md "wikilink")\[11\]，部队北上。7月，张鼎丞率第2支队主力进入苏皖边，开展[横山战斗](../Page/横山战斗.md "wikilink")、[奇袭官陡门作战等](../Page/奇袭官陡门.md "wikilink")。1939年5月，与[周恩来等经](../Page/周恩来.md "wikilink")[重庆](../Page/重庆.md "wikilink")、[西安到](../Page/西安.md "wikilink")[延安](../Page/延安.md "wikilink")，在[中共中央党校高级班学习](../Page/中共中央党校.md "wikilink")\[12\]。1941年，[皖南事变后](../Page/皖南事变.md "wikilink")，张鼎丞在延安被任命为新四军第七师师长，以后又遥兼皖江军区司令员。1943年，张鼎丞任中央党校二部主任\[13\]。1945年4月，参加[中共七大](../Page/中共七大.md "wikilink")，当选为中央委员\[14\]。

抗战胜利之后，张鼎丞返回华东。1945年10月，他经[粟裕推荐担任华中军区司令员](../Page/粟裕.md "wikilink")\[15\]。[第二次国共内战爆发后](../Page/第二次国共内战.md "wikilink")，他同邓子恢等组织对[苏中战役的供应](../Page/苏中战役.md "wikilink")。由于陈毅指挥的[山东野战军战绩不彰](../Page/山东野战军.md "wikilink")，张鼎丞同邓子恢、[曾山在](../Page/曾山.md "wikilink")1946年10月4日密电中共中央，反对陈毅的指挥，要求陈毅、粟裕、谭震林统一指挥部队\[16\]。此后，参与组织了[涟水战役](../Page/涟水战役.md "wikilink")，12月参与组织了[宿北战役](../Page/宿北战役.md "wikilink")，此后转入山东作战。1949年1月，张鼎丞任[中共中央华东局常委兼组织部长](../Page/中共中央华东局.md "wikilink")\[17\]。1949年7月，张鼎丞与[叶飞南下福建](../Page/叶飞.md "wikilink")，任[中共福建省委书记](../Page/中共福建省委.md "wikilink")、省人民政府主席、省军区政委\[18\]。

### 中华人民共和国成立后

[Xiao_Jinguang_and_Zhou'enlai,_1957.jpg](https://zh.wikipedia.org/wiki/File:Xiao_Jinguang_and_Zhou'enlai,_1957.jpg "fig:Xiao_Jinguang_and_Zhou'enlai,_1957.jpg")、[蕭勁光在](../Page/蕭勁光.md "wikilink")[青岛检阅解放军海军舰艇部队](../Page/青岛.md "wikilink")\]\]

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，张鼎丞主持福建党政军全面工作，并组织福建[剿匪](../Page/剿匪.md "wikilink")。此外，他还担任[中共中央华东局第四书记](../Page/中共中央华东局.md "wikilink")，[华东行政委员会副主席兼](../Page/华东行政委员会.md "wikilink")[政法委员会主任](../Page/政法委员会.md "wikilink")。1953年8月，他担任[中共中央组织部第一副部长](../Page/中共中央组织部.md "wikilink")，次年代理部长。1954年，张鼎丞在[第一届全国人民代表大会第一次会议上当选为](../Page/第一届全国人民代表大会第一次会议.md "wikilink")[最高人民检察院检察长](../Page/最高人民检察院.md "wikilink")，之后长期担任此职，建立健全社会主义法制\[19\]。尽管军功卓著，但由于较早转地方工作，没有授军衔\[20\]。张鼎丞在中国共产党第八届、第九届、第十届、第十一届全国代表大会上，他都当选为中央委员。[文化大革命期间](../Page/文化大革命.md "wikilink")，遭受迫害。

1975年起，第四届和第五届全国人民代表大会第一次会议上，他连续当选为[全国人民代表大会常务委员会副委员长](../Page/全国人民代表大会常务委员会副委员长.md "wikilink")\[21\]。1980年8月，张鼎丞响应中共中央关于废除领导职务终身制的决定，请退全国人大常委会副委员长职务。1981年12月16日，张鼎丞在[北京逝世](../Page/北京.md "wikilink")\[22\]。

## 参考文献

{{-}}

[Category:中华人民共和国最高人民检察院检察长](../Category/中华人民共和国最高人民检察院检察长.md "wikilink")
[Category:第四届全国人大常委会副委员长](../Category/第四届全国人大常委会副委员长.md "wikilink")
[Category:第五届全国人大常委会副委员长](../Category/第五届全国人大常委会副委员长.md "wikilink")
[代](../Category/中共中央组织部部长.md "wikilink")
[Category:中共福建省委书记](../Category/中共福建省委书记.md "wikilink")
[主席](../Category/中华人民共和国福建省省长.md "wikilink")
[Category:福建省军区政治委员](../Category/福建省军区政治委员.md "wikilink")
[Category:新四军将领](../Category/新四军将领.md "wikilink")
[Category:中国共产党第七届中央委员会委员](../Category/中国共产党第七届中央委员会委员.md "wikilink")
[Category:中国共产党第七届中央监察委员会委员](../Category/中国共产党第七届中央监察委员会委员.md "wikilink")
[Category:中国共产党第八届中央委员会委员](../Category/中国共产党第八届中央委员会委员.md "wikilink")
[Category:中国共产党第九届中央委员会委员](../Category/中国共产党第九届中央委员会委员.md "wikilink")
[Category:中国共产党第十届中央委员会委员](../Category/中国共产党第十届中央委员会委员.md "wikilink")
[Category:中国共产党第十一届中央委员会委员](../Category/中国共产党第十一届中央委员会委员.md "wikilink")
[Category:中国共产党党员
(1927年入党)](../Category/中国共产党党员_\(1927年入党\).md "wikilink")
[Z张](../Category/永定人.md "wikilink") [D](../Category/张姓.md "wikilink")
[Category:中华苏维埃共和国中央执行委员会委员](../Category/中华苏维埃共和国中央执行委员会委员.md "wikilink")

1.

2.

3.

4.
5.
6.

7.

8.
9.

10.
11.

12.
13.
14.
15.

16.

17.
18.

19.
20.

21.

22.