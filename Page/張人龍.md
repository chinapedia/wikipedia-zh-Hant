**張人龍**（，），[新界](../Page/新界.md "wikilink")[上水人](../Page/上水.md "wikilink")，[香港商人](../Page/香港.md "wikilink")、前[立法局](../Page/立法局.md "wikilink")[議員](../Page/議員.md "wikilink")、前[北區區議會](../Page/北區區議會.md "wikilink")[議員](../Page/議員.md "wikilink")、[全國政協前委員](../Page/全國政協.md "wikilink")、前[區域市政局](../Page/區域市政局.md "wikilink")[主席](../Page/主席.md "wikilink")，[太平紳士](../Page/太平紳士.md "wikilink")，第16屆[新界鄉議局主席](../Page/新界鄉議局.md "wikilink")，[新界總商會創辦人](../Page/新界總商會.md "wikilink")。

## 簡歷

1946年畢業於[廣州](../Page/廣州.md "wikilink")[中山大學經濟系](../Page/中山大學.md "wikilink")，獲文學士學位。後來回[香港隨父營商](../Page/香港.md "wikilink")，曾任[香港期貨交易所](../Page/香港期貨交易所.md "wikilink")、[九廣鐵路公司董事和](../Page/九廣鐵路公司.md "wikilink")[新界鄉議局執行議員](../Page/新界鄉議局.md "wikilink")。1964年起歷任[新界鄉議局第十六屆主席](../Page/新界鄉議局.md "wikilink")、第二十一、二十二屆副主席。1981年獲委任為[立法局議員](../Page/立法局.md "wikilink")。1982年任[北區區議員](../Page/北區區議會.md "wikilink")。1985年任[臨時區域市政局主席](../Page/臨時區域市政局.md "wikilink")。1986年出任[區域市政局主席](../Page/區域市政局.md "wikilink")。1993年受聘為[港事顧問](../Page/港事顧問.md "wikilink")。1996年獲任[香港特別行政區第一屆政府推選委員會委員](../Page/香港特別行政區第一屆政府推選委員會.md "wikilink")。

## 榮譽

  - [太平紳士](../Page/太平紳士.md "wikilink")
  - [CBE勳銜](../Page/CBE.md "wikilink")
  - [OStJ勳銜](../Page/聖約翰勳章.md "wikilink")

## 家庭

  - 弟弟

<!-- end list -->

  - [張家駒](../Page/張家駒.md "wikilink")，三弟

<!-- end list -->

  - 張人龍有十五名兒子\[1\]

<!-- end list -->

  - [張亮聲](../Page/張亮聲.md "wikilink")，第三子，活躍於桌球界，花名「化功大師」，2013年7月15日跳樓自殺。\[2\]
  - [張震聲](../Page/張震聲.md "wikilink")，傅聲之哥哥
  - [張德熙](../Page/張德熙.md "wikilink")：第八子，第四十五屆[香港金銀業貿易場理事長](../Page/香港金銀業貿易場.md "wikilink")。\[3\]
  - [傅聲](../Page/傅聲.md "wikilink")（原名張景賢、又名張富聲）：第九名[兒子](../Page/兒子.md "wikilink")、[電影演員](../Page/電影演員.md "wikilink")，藝人[甄妮的亡夫](../Page/甄妮.md "wikilink")（已故）
  - [張展鵬](../Page/張展鵬.md "wikilink")，傅聲之弟弟
  - [張醒鐘](../Page/張醒鐘.md "wikilink")
  - [張樂清](../Page/張樂清.md "wikilink")
  - 張樹輝
  - [張良坤](../Page/張良坤.md "wikilink")
  - [張樹祥](../Page/張樹祥.md "wikilink")
  - [張樂森](../Page/張樂森.md "wikilink")（已歿）
  - [張德貴](../Page/張德貴.md "wikilink")
  - [張樹聲](../Page/張樹聲_\(香港商人\).md "wikilink")
  - [張樹榮](../Page/張樹榮.md "wikilink")
  - [張樹祥](../Page/張樹祥.md "wikilink")
  - [張耀祖](../Page/張耀祖.md "wikilink")

## 紀念

香港政府在[上水有一條道路以張人龍及上水鄉紳](../Page/上水.md "wikilink")[廖潤琛命名](../Page/廖潤琛.md "wikilink")，名為[龍琛路](../Page/龍琛路.md "wikilink")，表揚二人致力於[石湖墟](../Page/石湖墟.md "wikilink")1955年－1956年大火後的重建工作。

## 參考來源

[Category:全國政協委員](../Category/全國政協委員.md "wikilink")
[Category:前香港立法局議員](../Category/前香港立法局議員.md "wikilink")
[Category:前區域市政局議員](../Category/前區域市政局議員.md "wikilink")
[Category:前香港區議員](../Category/前香港區議員.md "wikilink")
[Category:鄉議局主席](../Category/鄉議局主席.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港交易所人物](../Category/香港交易所人物.md "wikilink")
[Category:香港新界原居民](../Category/香港新界原居民.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:寶安人](../Category/寶安人.md "wikilink")
[Y](../Category/張姓.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")

1.  [1](http://hk.apple.nextmedia.com/news/art/20130717/18337296)
2.  [2](http://hk.apple.nextmedia.com/realtime/breaking/20130715/51561119)
3.