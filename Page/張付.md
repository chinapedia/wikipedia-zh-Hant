**張付**（），[中國男子](../Page/中華人民共和國.md "wikilink")[射擊運動員](../Page/射擊.md "wikilink")，出生于[吉林](../Page/吉林.md "wikilink")[長春](../Page/長春.md "wikilink")。

## 生涯

張付是男子臥射和氣步槍運動員，他在1996年至1998年期間在長春軍事體育運動學校接受韓淑秋教練的射擊訓練。隨後的3年，他晉身至吉林隊，當時他的教練為劉志忠。2001年，他成為了國家隊成員，教練是馬軍。

入選國家隊當年的11月14日，他於[九運會中的男子個人氣步槍小項決賽中以](../Page/九運會.md "wikilink")0.6環的優勢為吉林奪得一面[金牌](../Page/金牌.md "wikilink")\[1\]。次年，他在[上海舉行的世界杯中以](../Page/上海.md "wikilink")599環的成績取得男子個人10米氣步槍小項的[冠軍](../Page/冠軍.md "wikilink")，是全國最佳紀錄\[2\]。同年的10月，他與[李杰和](../Page/李杰_\(1979年\).md "wikilink")[蔡亞林在](../Page/蔡亞林.md "wikilink")[釜山亞運以](../Page/2002年亞洲運動會.md "wikilink")1788環的成績奪得男子團體10米氣步槍小項的[金牌以外](../Page/金牌.md "wikilink")，更打破了世界紀錄\[3\]。

2005年，張付於[南韓的世界杯中的男子個人三姿射擊小項以總成績](../Page/南韓.md "wikilink")1264.8環贏得金牌。翌年的[多哈亞運](../Page/2006年亞洲運動會.md "wikilink")，他先後贏得兩金一[銅](../Page/銅牌.md "wikilink")，包括男子團體50米步槍米項銅牌、男子個人50米三姿步槍小項金牌及男子團體50米三姿步槍小項金牌。

## 資料來源

<div class="references-small">

<references />

</div>

[Z张](../Category/中國射擊運動員.md "wikilink")
[Z张](../Category/2006年亞洲運動會金牌得主.md "wikilink")
[Z张](../Category/长春人.md "wikilink") [F付](../Category/張姓.md "wikilink")

1.  [男子10米氣步槍蔡亞林失手
    吉林張付奪冠](http://www.sportsonline.com.cn/GB/channel21/549/2447/2454/20011114/47126.html)

2.  [最新射擊專案全國紀錄表](http://images.sport.org.cn/download/2006/06/12/1426224069.doc)
3.  [wiki體育資料庫](http://wiki.sports.163.com/stars/d/d04a33ecdf6e67a13bba17777e5f8f1f.html)