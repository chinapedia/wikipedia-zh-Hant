**张信**（），[字彦实](../Page/字.md "wikilink")，[号城甫](../Page/号.md "wikilink")，明朝状元，[浙江](../Page/浙江.md "wikilink")[宁波府](../Page/宁波府.md "wikilink")[定海县](../Page/定海县.md "wikilink")（现[舟山](../Page/舟山.md "wikilink")[定海区](../Page/定海区.md "wikilink")）人。

## 生平

[明太祖洪武二十七年](../Page/明太祖.md "wikilink")（1394年）中甲戌科[进士第一](../Page/进士.md "wikilink")（[状元](../Page/状元.md "wikilink")），授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")\[1\]，官至[侍读学士](../Page/侍读学士.md "wikilink")。“常直谏朝政得失，上下交赞。”洪武三十年（1397年）[会试](../Page/会试.md "wikilink")，[刘三吾任主考](../Page/刘三吾.md "wikilink")，所取进士均来自南方各省，引起北方士子不满，太祖命侍读张信、侍讲[戴彝](../Page/戴彝.md "wikilink")、春坊右赞善[王俊华](../Page/王俊华.md "wikilink")、[司宪](../Page/司宪.md "wikilink")、右司直郎张谦、司经局校书[严叔载](../Page/严叔载.md "wikilink")、正字[董贯](../Page/董贯.md "wikilink")、韩、安二王府长史[黄章](../Page/黄章.md "wikilink")、韩府纪善周衡、靖江府纪善[萧楫及](../Page/萧楫.md "wikilink")[陈叔恭首甲三人](../Page/陳䢿.md "wikilink")，每人审阅十卷，复审试卷。张信阅卷后，认为三吾所取无私，太祖震怒，以张信等人所撰汉武帝尽杀长安狱囚，暗讽太祖大起[胡惟庸案为由](../Page/胡惟庸.md "wikilink")，将张信等下刑部大狱拷讯，诸阅卷者并祭酒杨淞皆被判为胡惟庸一党，刘三吾、[白信蹈及司宪为](../Page/白信蹈.md "wikilink")[蓝玉一党](../Page/蓝玉.md "wikilink")，只戴彝被无罪释放，其余众人皆被处死，刘三吾戍边\[2\]。四月初二日，前次科举进士被取消，陈及探花[刘仕谔被御史弹劾](../Page/刘仕谔.md "wikilink")，连坐处死，榜眼[尹昌隆幸免](../Page/尹昌隆.md "wikilink")。六月初一，太祖以山东[韩克忠为状元](../Page/韩克忠.md "wikilink")，另取六十一人为新科进士。

## 参见

  - [明朝状元列表](../Page/明朝状元列表.md "wikilink")

## 参考文献

## 外部链接

  - [状元张信](http://www.cseac.com/Article_Show.asp?ArticleID=6778)

[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:定海人](../Category/定海人.md "wikilink")
[Category:舟山人](../Category/舟山人.md "wikilink")
[X](../Category/張姓.md "wikilink")

1.  《[明太祖實錄](../Page/明太祖實錄.md "wikilink")》（卷232）：“丙午以进士及第张信为翰林院修撰。”
2.  《[明史](../Page/明史.md "wikilink")》（卷137）：“三十年偕紀善白信蹈等主考會試。榜發，泰和宋琮第一，北士無預者。於是諸生言三吾等南人，私其鄉。帝怒，命侍講張信等覆閱，不稱旨。或言信等故以陋卷呈，三吾等實屬之。帝益怒，信蹈等論死，三吾以老戍邊，琮亦遣戍。帝親賜策問，更擢六十一人，皆北士。時謂之「南北榜」，又曰「春夏榜」云。”