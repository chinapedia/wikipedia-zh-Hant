**張元忭**（），字子藎，號陽和。[浙江](../Page/浙江.md "wikilink")[山陰縣](../Page/山陰縣.md "wikilink")（今屬[紹興市](../Page/紹興市.md "wikilink")）人，民籍，[明朝狀元](../Page/明朝.md "wikilink")、政治人物。

## 生平

浙江鄉試第六十八名。隆慶五年（1571年）一甲第一名進士（狀元）\[1\]\[2\]\[3\]。官至翰林侍讀、左諭德，有明一代修史屬翰苑諸臣，故稱張太史。萬曆己卯，教席[內書堂](../Page/內書堂.md "wikilink")，取《[中鑑錄](../Page/中鑑錄.md "wikilink")》諄諄誨之。卒於萬曆十六年（1588年）。天启初年，追谥**文恭**。

## 著作

在其父[张天复著作](../Page/張天復.md "wikilink")《皇舆考》的基础上増扩改编并成书了重要[地理学著作](../Page/地理学.md "wikilink")《[广皇舆考](../Page/广皇舆考.md "wikilink")》。有《不二齋文選》。與[孫鑛同撰](../Page/孫鑛.md "wikilink")《紹興府志》50卷。

## 家族

祖籍四川[綿竹](../Page/綿竹.md "wikilink")。曾祖父[張宗盛](../Page/張宗盛.md "wikilink")；祖父[張詔](../Page/張詔.md "wikilink")，贈吏部主事；父親[張天復](../Page/張天復.md "wikilink")，行太僕寺卿。母劉氏（封安人）\[4\]。有子[張汝霖](../Page/張汝霖_\(明朝\).md "wikilink")。

## 参考文献

  - [焦竑](../Page/焦竑.md "wikilink")，《奉直大夫左春坊左諭德兼翰林院侍讀張公元忭墓誌銘》，《國朝獻徵錄》1冊19卷
  - 錢明：〈[阳明后学张元忭的生平与著述](http://www.nssd.org/articles/article_read.aspx?id=669428124)〉

{{-}}

[Category:嘉靖三十七年戊午科舉人](../Category/嘉靖三十七年戊午科舉人.md "wikilink")
[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林院修撰](../Category/明朝翰林院修撰.md "wikilink")
[Category:明朝翰林院侍讀](../Category/明朝翰林院侍讀.md "wikilink")
[Category:明朝心學家](../Category/明朝心學家.md "wikilink")
[Category:紹興人](../Category/紹興人.md "wikilink")
[Y元](../Category/張姓.md "wikilink")
[Category:諡文恭](../Category/諡文恭.md "wikilink")

1.
2.
3.
4.