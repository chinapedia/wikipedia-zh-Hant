**張光直**（），生于北京。[考古學家](../Page/考古學家.md "wikilink")、[人類學家](../Page/人類學家.md "wikilink")，曾任[中央研究院副院長](../Page/中央研究院.md "wikilink")、[美國國家科學院院士](../Page/美國國家科學院.md "wikilink")，亦曾任教於[美國](../Page/美國.md "wikilink")[耶魯大學與](../Page/耶魯大學.md "wikilink")[哈佛大學](../Page/哈佛大學.md "wikilink")。

## 生平

張光直生於[北京](../Page/北京.md "wikilink")\[1\]。父親[張我軍是](../Page/張我軍.md "wikilink")[台灣新文學健將](../Page/台灣新文學.md "wikilink")、哥哥張光正於青少年時就到[蘇區參加革命](../Page/蘇區.md "wikilink")，是[中國共產黨黨員](../Page/中國共產黨.md "wikilink")。弟弟[張光誠和他一起從](../Page/張光誠.md "wikilink")[中國大陸回台](../Page/中國大陸.md "wikilink")，後來都去美國。

他和父母返台後，就讀[台北市](../Page/台北市.md "wikilink")[建國中學](../Page/建國中學.md "wikilink")，在左派校長[陳文彬和國文教師](../Page/陳文彬.md "wikilink")[羅鐵鷹的影響下](../Page/羅鐵鷹.md "wikilink")，接受了[左派思想](../Page/左派.md "wikilink")，並培養了文學的興趣。張光直在[白色恐怖](../Page/白色恐怖.md "wikilink")[四六事件中被捕](../Page/四六事件.md "wikilink")，被關押一年多，通過父亲的黨政關係才得釋放。

張光直從[國立台灣大學考古人類學系畢業後留學](../Page/國立台灣大學考古人類學系.md "wikilink")[美國](../Page/美國.md "wikilink")，於[哈佛大學師從](../Page/哈佛大學.md "wikilink")[戈登·藍道夫·威利](../Page/戈登·藍道夫·威利.md "wikilink")，取得[人類學](../Page/人類學.md "wikilink")[博士學位](../Page/博士.md "wikilink")。取得哈佛大學博士學位後張光直於[耶魯大學人類學系任教長達](../Page/耶魯大學.md "wikilink")16年，1977年回到母校哈佛人類學系任教，曾任該系系主任一職。

他在1994年應[李遠哲邀請回台灣](../Page/李遠哲.md "wikilink")，擔任[中央研究院副院長](../Page/中央研究院.md "wikilink")，1996年健康因素辭職，到美國休養，2001年因[帕金森氏症在](../Page/帕金森氏症.md "wikilink")[美國](../Page/美國.md "wikilink")[麻塞諸塞州去世](../Page/麻塞諸塞州.md "wikilink")\[2\]。

## 學術成就

張氏學術主要成就有二：

1.  在[聚落考古學](../Page/聚落考古學.md "wikilink")(settlement archaeology)有卓著的貢獻。
2.  將當代[文化人類學及](../Page/文化人類學.md "wikilink")[考古學的理論以及方法應用在](../Page/考古學.md "wikilink")[中國考古學與](../Page/中國考古學.md "wikilink")[臺灣考古學](../Page/臺灣考古學.md "wikilink")，並促成兩者與當代考古學發展的接軌。

其代表作《[古代中國考古](../Page/古代中國考古.md "wikilink")》（*The Archaeology of Ancient
China*) 曾修訂多次，其中最後一版提出中國文化的多元起源論，迄今仍然是中國考古學最主要的指導理論。

## 著作

  - *Rethinking Archaeology* (1967)

  - *Settlement Archaeology* (1968)

  - 《Fengpitou, Tapenkeng, and the Prehistory of Taiwan》, ISBN
    978-0-913516-06-5 (1969)

  - *Early Chinese Civilization: Anthropological Perspectives* (1976)

  - *Food in Chinese Culture: Anthropological and Historical
    Perspectives* (1977)

  - (Chang has essay in)

  - *The Formation of Chinese Civilization : an archaeological
    perspective* (2002)

  - 《Souvenirs de la patate douce : une adolescence taiwanaise》 (2005)

  - 《商周青銅器與銘文的綜合研究》 (1973)

  - 《張我軍詩文集》 (1975)

  - 《臺灣省濁水溪與大肚溪流域考古調查報告》 (1977)

  - 《中國青銅時代》 (1983)

  - 《考古學專題六講》 (1988)

  - 《九洲學刊選編(一)》 (1988)

  - 《中國青銅時代第二集》 (1990)

  - 《美術.神話與祭祀》 (1992)

  - 《考古人類學隨筆》 (1995年)，ISBN 978-957-08-1442-2

  - 《中國考古學論文集》 (1995)

  - 《蕃薯人的故事》 (1998)，ISBN 978-957-08-1757-7

  - 《古代中國考古學》 (2002)

  - 《商文明》 (2002)

  - 《張光直文學作品集 : 考古學家張光直文學作品集粹》 (2005)

## 受獎榮譽

  - [亞洲研究協會](../Page/亞洲研究協會.md "wikilink")(Association for Asian Studies
    (AAS)), 1996 Award for Distinguished Contributions to Asian
    Studies\[3\]

## 人物評價

[余英时](../Page/余英时.md "wikilink"):「他是一座没有爆发的火山，但是他的光和热已永远留在人间。」\[4\]

## 註釋

## 參考文獻

  - Ferrie, Helke, 1995, "A Conversation with K.C. Chang," *Current
    Anthropology*, 36(2): 307-325.
  - Keightley, David N., 2001, "Kwang-Chih Chang (1931–2001)," *The
    Journal of Asian Studies* 60(2): 619-621.

## 外部連結

  - [Short Biography with a link to K.C. Chang's complete
    bibliography](https://web.archive.org/web/20131224115124/http://www.bu.edu/asianarc/files/kcchang_publications.pdf)
  - [National Academy of Sciences Biographical
    Memoir](http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/chang-kwang-chih-1.pdf)

[Category:中央研究院副院長](../Category/中央研究院副院長.md "wikilink")
[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:台灣考古學家](../Category/台灣考古學家.md "wikilink")
[Category:台灣人類學家](../Category/台灣人類學家.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:耶魯大學教師](../Category/耶魯大學教師.md "wikilink")
[Category:台灣哈佛大學校友](../Category/台灣哈佛大學校友.md "wikilink")
[Category:國立臺灣大學文學院校友](../Category/國立臺灣大學文學院校友.md "wikilink")
[Category:臺北市立建國高級中學校友](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")
[Category:半山](../Category/半山.md "wikilink")
[Category:板橋人](../Category/板橋人.md "wikilink")
[Guang光](../Category/張姓.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")

1.  [圖書室簡介](http://www.nmp.gov.tw/learn/books/searching.php)
    ，國立臺灣史前文化博物館
2.
3.  Association for Asian Studies (AAS), [1996 Award for Distinguished
    Contributions to Asian
    Studies](http://www.aasianst.org/publications/distinguished.htm);
    retrieved 2011-06-06
4.