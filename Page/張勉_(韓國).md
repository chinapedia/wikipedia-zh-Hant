**張勉**（，），是[大韓民国](../Page/大韓民国.md "wikilink")[政治家](../Page/政治家.md "wikilink")、教育家，出生于[漢城府](../Page/漢城府.md "wikilink")（今[首爾](../Page/首爾.md "wikilink")）[鍾路區](../Page/鍾路區.md "wikilink")[積善洞](../Page/積善洞.md "wikilink")\[1\]，[本貫是](../Page/本貫.md "wikilink")[仁同張氏](../Page/仁同張氏.md "wikilink")，信仰[天主教](../Page/天主教.md "wikilink")。号**雲石**（），字**志兑**（），教名**若翰**（），\[2\]美國名若翰·勉·張（John
Myon
Chang）。其父是[張箕彬](../Page/張箕彬.md "wikilink")。弟[張勃是](../Page/張勃.md "wikilink")[漢城大学首任美術大学学長](../Page/漢城大学.md "wikilink")、[張剋是世界有名的航空工学学者](../Page/張剋.md "wikilink")。在[日據時期為教育家](../Page/朝鮮日治時期.md "wikilink")。[朝鮮光復後](../Page/光復節_\(朝鮮半島\).md "wikilink")，张勉在1948年-1950年任[韓國駐美大使](../Page/大韓民國駐美國大使館.md "wikilink")，1950年-1952年和1960年-1961年曾两次任[韩国总理](../Page/韩国总理.md "wikilink")；1956年-1960年任韓國最後一任[副總統](../Page/韓國總統#副總統.md "wikilink")，之後[副總統的職位廢除](../Page/韓國總統#副總統.md "wikilink")。其四子[張益為](../Page/張益.md "wikilink")[天主教春川教區主教](../Page/天主教春川教區.md "wikilink")。

## 生平

### 初記

父亲[张箕彬是一名税务官](../Page/张箕彬.md "wikilink")，母亲[黄卢夏](../Page/黄卢夏.md "wikilink")，是黄圣集的次女，張勉作为长男出生于朝鲜汉城，但因父亲张箕彬是京畿道仁川郡人，因此，另一說張勉的出生地是[仁川](../Page/仁川.md "wikilink")。父亲张箕彬的雅号太岩，张箕彬子女雅号的行列字**石**，所以張勉的雅号叫作**雲石**（운석）。

張勉1906年8月入读[仁川](../Page/仁川.md "wikilink")[圣堂附设博文小学校](../Page/圣堂.md "wikilink")，学习韩文，汉学，数学等，1910年入读博文学校高等科，1912年博文学校高等科毕业。1912年3月入读仁川寻常小学校六学年，1913年4月入读仁川寻常小学校高等科，1914年4月进入京畿道水原农林高等学校。

1916年6月張勉和[金玉胤结婚](../Page/金玉胤.md "wikilink")。1917年3月水原农业学院毕业。1919年张勉去美国进纽约曼哈顿大学留学。1925年作为[曼哈顿大学韩国人天主教徒代表赴梵蒂冈访问](../Page/曼哈顿大学.md "wikilink")，1927年毕业。随后回到朝鲜，在汉城曾任天主教青年联合会会长、东星商业学校教员、校长及启星国民学校校长等职。

### 教育活动

[Chang_Myon's_Ordo_Fratrum_Minorum_Conventualium_members.jpg](https://zh.wikipedia.org/wiki/File:Chang_Myon's_Ordo_Fratrum_Minorum_Conventualium_members.jpg "fig:Chang_Myon's_Ordo_Fratrum_Minorum_Conventualium_members.jpg")上学时的张勉（1921年）\]\]
1931年4月張勉在天主教徒的推荐下招聘为东星商业学校教师。1936年任东星商业学校校长，1937年4月12日兼任惠化洞天主教会附设惠化[幼稚园园长](../Page/幼稚园.md "wikilink")。1936年11月任启星国民初等学校第三任校长（至1942年4月），1944年8月再次兼任启星国民初等学校第五任校长（至1945年11月）\[3\]。

1945年8月[日本投降后](../Page/日本投降.md "wikilink")，9月[韩国民主党创党](../Page/韩国民主党.md "wikilink")，张勉的韩民党拒绝加入，1946年2月当选国会议员，进入[韩国政界](../Page/韩国.md "wikilink")，1948年5月10日当选[汉城钟路区的](../Page/汉城.md "wikilink")[大韩民国国会议员](../Page/大韩民国国会.md "wikilink")。在48年9月張勉代表[大韩民国赴](../Page/大韩民国.md "wikilink")[巴黎列席第](../Page/巴黎.md "wikilink")3次联合国大会。12月任韩国驻梵蒂冈特别大使，1948年美國曼哈頓大學授予他名譽法學博士学位。

### 政治活动

[주미_대한민국_대사_장면.png](https://zh.wikipedia.org/wiki/File:주미_대한민국_대사_장면.png "fig:주미_대한민국_대사_장면.png")
[South_Koream_vicepresident_Chang_Myon.png](https://zh.wikipedia.org/wiki/File:South_Koream_vicepresident_Chang_Myon.png "fig:South_Koream_vicepresident_Chang_Myon.png")
1949年任第一任[韩国驻美大使](../Page/韩国驻美大使.md "wikilink")，1950年[朝鲜战争时](../Page/朝鲜战争.md "wikilink")，张勉作为韩国驻美大使，在联合国安全理事会呼吁为[大韩民国的自由和民主](../Page/大韩民国.md "wikilink")，请求[联合国军入朝鲜對抗](../Page/联合国军.md "wikilink")[中国人民志愿军和](../Page/中国人民志愿军.md "wikilink")[朝鲜人民军](../Page/朝鲜人民军.md "wikilink")。1951年經任命成为[韩国总理](../Page/韩国总理.md "wikilink")，1952年4月张勉因参加[大韩民国总统竞选而遭李承晚免职](../Page/大韩民国总统.md "wikilink")。

1954年加入[护宪同志会](../Page/护宪同志会.md "wikilink")，1955年11月与[申翼熙](../Page/申翼熙.md "wikilink")，[尹潽善](../Page/尹潽善.md "wikilink")，[赵炳玉等人组建](../Page/赵炳玉.md "wikilink")[民主党](../Page/民主党_\(大韩民国1955年\).md "wikilink")，出任民主党最高委员和国会议员，1956年3月当选第四任韩国副总统。同年9月汉城举行民主党全党大会，开会時张勉遭到杀手的狙击受轻伤。

1959年再次当选民主党最高委员，1960年3月作为民主党总统候选人竞选总统，因“3.15非法选举”，自由党以非法手段，使李承晚以90%得票率第四次当选为总统，張勉落選。李承晚篡改宪法，企图实施长期独裁统治，执政自由党腐败到了极点。内政部和警察局成了负责政权选举的总部，恣意妄为，大肆以非法手段操纵选举。当时，在马山参加抗议示威的马山商业高中学生金朱烈因催泪弹打中头部而身亡，后来，人们发现他的尸体被扔进海中。这一事件成为了革命爆发的导火線，4月19日，以学生为主体的韩国民众发动了民主主义革命，大规模示威活动如同燎原之火迅速蔓延到全国，李承晚于4月26日宣布下台。

1960年[4·19學運後](../Page/4·19學運.md "wikilink")，5月张勉当选第五任韩国国会议员，8月[第二共和国成立](../Page/第二共和国.md "wikilink")，改為[內閣制](../Page/內閣制.md "wikilink")，张勉当选[国务总理](../Page/国务总理.md "wikilink")，他执权後试图保障国民的最大自由权限，但因缺乏领导能力导致社会无秩序混乱，加上韓國社會對共產主義及北韓的恐懼，1961年[朴正熙少將发动](../Page/朴正熙.md "wikilink")[5.16军事政变](../Page/5.16军事政变.md "wikilink")，張勉的[韩国总理职务被中止](../Page/韩国总理.md "wikilink")，結束僅9个月的民主黨政府。

政变後，朴正熙实行[政治活動净化法](../Page/政治活動净化法.md "wikilink")（1962年3月16日制定），禁止政党的政治活动，1962年將之關入监狱，释放后，活躍于宗教生活和在野民主党。

1966年，因[肝炎逝世](../Page/肝炎.md "wikilink")。

### 死後

他的遗体安葬在京畿道[抱川郡的天主教惠華公園](../Page/抱川市.md "wikilink")。

1999年8月先后由[金大中总统追授予](../Page/金大中.md "wikilink")[大韓民國建国功勞勳章和大韓民国章](../Page/大韓民國.md "wikilink")（一等奖勋）。

## 评价

韩国对张勉的评价较低。他作为国家领导人，没有抵抗军事政变，而是隐藏在漢城市内的女子修道院里，为人所诟病。

## 親日指控說

张勉被怀疑是[親日派](../Page/親日派.md "wikilink")。

2009年11月8日，「親日活動研究所」發表了《親日合作者全書》\[4\]，當中指後來任[天主教首爾總教區總主教的](../Page/天主教首爾總教區.md "wikilink")[盧基南自](../Page/盧基南.md "wikilink")1940年出任「天主教漢城教區全力支持日本參戰聯盟」的主席，而張為聯盟的議員。

## 著書

  - 自传：張勉博士回顧錄（한알의 밀이 죽지 않고는），雲石先生紀念出版委員會，作者：韓昌愚，1967年。

## 履历

  - 1919年 4月 龍山小神學校 講師
  - 1930年 4月1日 漢城 [東成商業高等學校](../Page/東成商業高等學校.md "wikilink") 教師
  - 1931年 [東成商業高等學校](../Page/東成商業高等學校.md "wikilink") 行政室長
  - 1936年11月9日 東成商業高等學校 校長
  - 1939年 4月 - 1942年 8月 第3代 啓星初等學校 校長
  - 1939年 9月 朝鮮天主教青年聯合會 會長
  - 1941年 日本名 **玉岡勉**
  - 1946年 [南朝鮮過渡立法議院](../Page/南朝鮮過渡立法議院.md "wikilink") 議員, 國立大學教 說立案,
    娼女制 廢止 製案
  - 1948年 9月 联合国大韓民國特派首席代表
  - 1949年
    首任[大韓民國駐](../Page/大韓民國.md "wikilink")[美國大使](../Page/美國.md "wikilink")
  - 1950年11月23日 第2任 [大韓民國總理](../Page/大韓民國總理.md "wikilink")
  - 1952年 4月 因病辞退
  - 1956年 大韓民國 第4任 副總統
  - 1960年4月23日 退任
  - 1960年8月18日 第8任 [大韓民國總理](../Page/大韓民國總理.md "wikilink")
  - 1961年5月16日 被推翻

## 妻子

  - 金允玉（1900年 - 1990年）
      - 張英，早死
      - [張震](../Page/張震_\(1927年\).md "wikilink")（장진, 1927年 - ，教授）
      - 張建
      - [張益](../Page/張益.md "wikilink")，為[天主教春川教區主教](../Page/天主教春川教區.md "wikilink")
      - [張純](../Page/張純.md "wikilink")（1935年 - ，教授）

## 相關條目

  - [许政](../Page/许政.md "wikilink")
  - [李承晩](../Page/李承晩.md "wikilink")
  - [金九](../Page/金九.md "wikilink")
  - [金性洙](../Page/金性洙.md "wikilink")
  - [尹致瑛](../Page/尹致瑛.md "wikilink")
  - [尹致昊](../Page/尹致昊.md "wikilink")
  - [尹潽善](../Page/尹潽善.md "wikilink")
  - [張俊河](../Page/張俊河.md "wikilink")
  - [盧基南](../Page/盧基南.md "wikilink")

## 网站

  - [雲石 張勉博士
    纪念會](https://web.archive.org/web/20110722132616/http://unsuk.kyunghee.ac.kr/)
  - [張勉：韓國國會](https://archive.is/20121205014529/http://www.rokps.or.kr/profile_result_ok.asp?num=87)
  - [張勉：Daum](http://enc.daum.net/dic100/contents.do?query1=b18j3022a)

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:韩国总理](../Category/韩国总理.md "wikilink")
[Category:韓國副總統](../Category/韓國副總統.md "wikilink")
[Category:被政變推翻的領導人](../Category/被政變推翻的領導人.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:朝鮮戰爭人物](../Category/朝鮮戰爭人物.md "wikilink")
[Category:朝鮮日佔時期人物](../Category/朝鮮日佔時期人物.md "wikilink")
[Category:韓國駐美國大使](../Category/韓國駐美國大使.md "wikilink")
[Category:韩国外交官](../Category/韩国外交官.md "wikilink")
[Category:韓國政治人物](../Category/韓國政治人物.md "wikilink")
[Category:韓國言論人物](../Category/韓國言論人物.md "wikilink")
[Category:韩国教育家](../Category/韩国教育家.md "wikilink")
[Category:韓國天主教徒](../Category/韓國天主教徒.md "wikilink")
[Category:韓國反共主義者](../Category/韓國反共主義者.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Category:仁川廣域市出身人物](../Category/仁川廣域市出身人物.md "wikilink")
[Category:曼哈頓學院校友](../Category/曼哈頓學院校友.md "wikilink")
[Category:韩国政治之最](../Category/韩国政治之最.md "wikilink")
[Category:仁同张氏](../Category/仁同张氏.md "wikilink")
[Myon](../Category/张姓.md "wikilink")

1.  一說仁川出身
2.  一名 張勉 若
3.  <http://www.gyeseong1882.es.kr/?doc=sub/m14.php>
4.