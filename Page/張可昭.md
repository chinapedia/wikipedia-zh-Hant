**張可昭**(Carl
Chang)是[华裔](../Page/华裔.md "wikilink")[美国计算机科学家](../Page/美国.md "wikilink")。他目前是[艾奥瓦州立大学](../Page/艾奥瓦州立大学.md "wikilink")[计算机科学系的主任](../Page/计算机科学.md "wikilink").

## 生平

張可昭曾获得[國立中央大學數學系學士](../Page/國立中央大學.md "wikilink")\[1\]，[西北大学计算机博士学位](../Page/西北大学_\(伊利诺州\).md "wikilink")。他曾在[GTE
Automatic
Electric和](../Page/GTE_Automatic_Electric.md "wikilink")[贝尔实验室工作](../Page/贝尔实验室.md "wikilink")。1984年，他加入了
[伊利諾大學芝加哥分校](../Page/伊利諾大學芝加哥分校.md "wikilink"),
担任国际软件工程中心的负责人。2001-2002年，他在[奧本大學工作](../Page/奧本大學.md "wikilink")。
2002年7月加入艾奥瓦州立大学.。2004年当选为[IEEE](../Page/IEEE.md "wikilink") [Computer
Society](../Page/Computer_Society.md "wikilink") 主席， [IEEE
Software杂志的主编](../Page/IEEE_Software.md "wikilink")
(1991年-1994年) and spearheaded the [Computing Curricula
2001](../Page/Computing_Curricula_2001.md "wikilink") (CC2001) project
jointly sponsored by the IEEE Computer Society, the
[ACM](../Page/Association_for_Computing_Machinery.md "wikilink"), and
the [National Science
Foundation](../Page/National_Science_Foundation.md "wikilink").
他是[IEEE和](../Page/IEEE.md "wikilink")[AAAS](../Page/AAAS.md "wikilink")
Fellow, and serves on the Advisory Council of the [Internet
Society](../Page/Internet_Society.md "wikilink") (ISOC).

张近来的主要研究兴趣包括软件工程，[Web服务等](../Page/Web服务.md "wikilink")。

## 參考資料

<references/>

## 外部链接

  - [个人主页](http://www.cs.iastate.edu/~chang)

[Category:美国计算机科学家](../Category/美国计算机科学家.md "wikilink")
[Category:伊利諾大學芝加哥分校教師](../Category/伊利諾大學芝加哥分校教師.md "wikilink")
[Category:奧本大學教師](../Category/奧本大學教師.md "wikilink")
[Category:愛荷華州立大學教師](../Category/愛荷華州立大學教師.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[理](../Category/國立中央大學校友.md "wikilink")
[Category:张姓](../Category/张姓.md "wikilink")

1.  [系友演講-張可昭教授座談](http://www.math.ncu.edu.tw/math/news/news_select_detail.php?news_id=1036)