**张圆圆**（英文名：，），[美籍华裔](../Page/美籍华裔.md "wikilink")[花样滑冰运动员](../Page/花样滑冰.md "wikilink")，她在2006-2007[国际滑冰联盟少年大奖赛中获得冠军](../Page/国际滑冰联盟.md "wikilink")，在2007世界少年花样滑冰冠军赛中获得亚军。

## 出生

张圆圆是[美籍华裔](../Page/美籍华裔.md "wikilink")，生于[波士顿](../Page/波士顿.md "wikilink")，在童年的时候迁居到[加利福尼亚](../Page/加利福尼亚.md "wikilink")。她的父母来自[中国](../Page/中华人民共和国.md "wikilink")[湖北](../Page/湖北.md "wikilink")[武汉](../Page/武汉.md "wikilink")，姐姐张阳洋生于中国，較张圆圆年长八岁，毕业于[麻省理工学院](../Page/麻省理工学院.md "wikilink")。张圆圆目前居住在[加州的Brea](../Page/加州.md "wikilink")。

张圆圆精通[钢琴演奏](../Page/钢琴.md "wikilink")，并且在所就读的中学交响乐团中担任首席[小提琴手](../Page/小提琴.md "wikilink")。在练习花样滑冰之前曾学习[芭蕾](../Page/芭蕾.md "wikilink")。

她在[Artesia的East](../Page/Artesia.md "wikilink") West Ice
Palace接受训练，该冰场属于[关颖珊](../Page/关颖珊.md "wikilink")，她的教练是曾经培养出[陈露的](../Page/陈露.md "wikilink")[李明珠](../Page/李明珠_\(1962年\).md "wikilink")。

## 花样滑冰生涯

张圆圆在5岁开始练习滑冰，在12岁的时候获得2006年全美花样滑冰冠军赛青年组第8名后开始引起外界关注。次年开始参加国际比赛，并在[国际滑冰联盟花样滑冰大奖赛少年组墨西哥站比赛中以创记录](../Page/国际滑冰联盟花样滑冰大奖赛.md "wikilink")\[1\]
的53分\[2\]的优势夺冠。而后以33分\[3\]的优势赢得了她的第二项国际比赛桂冠，接下来是20分的优势\[4\]
赢得的总决赛的桂冠。张圆圆以她独创的旋转，良好的乐感以及惊人的柔韧性而广为人知，在花样滑冰界被认为是可以与[萨莎·科恩和](../Page/萨莎·科恩.md "wikilink")[关颖珊相提并论的明日之星](../Page/关颖珊.md "wikilink")。\[5\]

在2007[全美花样滑冰冠军赛中](../Page/全美花样滑冰冠军赛.md "wikilink")，张圆圆位列她的好友[长洲未来之后获得亚军](../Page/長洲未来.md "wikilink")，并入选了世界少年队。而在2007[世界滑冰冠军赛少年组中](../Page/世界滑冰冠军赛少年组.md "wikilink")，张圆圆取得了冠军。

从2007-08赛季起，张圆圆开始参加成年组的比赛，
在2007[国际滑冰联盟花样滑冰大奖赛美国站](../Page/国际滑冰联盟花样滑冰大奖赛美国站.md "wikilink")
中获得铜牌。是次比赛，她的后仰旋转赢得了4级+3
的分数，这是一个滑冰选手从一个动作中能赢得的最高分数。\[6\]但是她的跳跃动作被有争议地扣掉了17分。她还参加了2007[中国杯](../Page/国际滑冰联盟花样滑冰大奖赛中国站.md "wikilink"),
这次她的争议动作被扣掉了稍少的分数，获得了银牌。
她在大奖赛系列赛中赢得了24分。在2007[大奖赛日本站之后](../Page/国际滑冰联盟花样滑冰大奖赛日本站.md "wikilink")，她入围了2007-2008[国际滑冰联盟花样滑冰大奖赛总决赛](../Page/国际滑冰联盟花样滑冰大奖赛总决赛.md "wikilink"),
并获得了短节目第四名以及总成绩亚军. 她是连续第四位在参加成年组比赛的第一年就入围大奖赛总决赛的少年组世界冠军。

因为参加了大奖赛美国站的比赛，张圆圆将不再参加Regionals的比赛。参加中国杯之后也不再参加Sectionals的比赛。她在2008[全美花样滑冰冠军赛中首次以成年选手的身份出场](../Page/全美花样滑冰冠军赛.md "wikilink")。因为年龄的关系，张圆圆在2009年前不能参加国际冰联冠军赛成年组的比赛。

## 动作创新

张圆圆以“珍珠”旋转而著称\[7\],
珍珠旋转是[后仰旋转与](../Page/后仰旋转.md "wikilink")[贝尔曼旋转的结合](../Page/贝尔曼旋转.md "wikilink")，选手的双手握住非支撑腿的冰刀，并将其举至齐腰高度。背部及头部向膝盖弯曲，之后非支撑腿伸至头部上方呈贝尔曼姿势。这使得这种旋转实质上是一种握脚后仰的贝尔曼姿势，因为根据规则，当足部经过头部并伸至头部的上后方时成为贝尔曼姿势。\[8\]
这是一个后仰旋转，因为头部直立，并且肩部后拉，背部弯向冰面。\[9\] 这个姿势已经成为了张圆圆的标志性动作。
张圆圆也做一种*超伸展的*[贝尔曼旋转](../Page/贝尔曼旋转.md "wikilink").
她是少数几个能完成这个动作的年轻选手.
腿向上拉直，背部在旋转中尽可能地弯曲。张圆圆在做这个动作的时候非支撑腿能够垂直向上，膝盖只有轻微的弯曲，使得这个动作看上去更象capital-I
position而不是通常的贝尔曼水滴姿势。这是比赛中最受观众欢迎的动作。

张圆圆也使用向前的.

## 节目编排

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p><a href="../Page/短節目.md" title="wikilink">短節目</a></p></th>
<th><p><a href="../Page/自由滑.md" title="wikilink">自由滑</a></p></th>
<th><p>表演节目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2009-2010</p></td>
<td><p>"Zigeunerweisen"<br />
<small> <a href="../Page/帕布罗·德·萨拉萨蒂.md" title="wikilink">帕布罗·德·萨拉萨蒂</a></small></p></td>
<td><p>"<a href="../Page/胡桃夹子.md" title="wikilink">胡桃夹子</a>"<br />
<small><a href="../Page/柴可夫斯基.md" title="wikilink">柴可夫斯基</a></small></p></td>
<td><p>有待公布 |-|-</p></td>
</tr>
<tr class="even">
<td><p>2007-2008</p></td>
<td><p>"Spanish Gypsy"<br />
<small> by <a href="../Page/Ray_DeTone.md" title="wikilink">Ray DeTone</a></small></p></td>
<td><p>"<a href="../Page/圣母经.md" title="wikilink">圣母经</a>"<br />
<small><a href="../Page/舒伯特.md" title="wikilink">舒伯特</a></small>"</p></td>
<td><p>"<a href="../Page/Born_to_Try.md" title="wikilink">Born to Try</a>"<br />
<small>by <a href="../Page/Delta_Goodrem.md" title="wikilink">Delta Goodrem</a></small></p>
<hr>
<p>"<a href="../Page/You_Raise_Me_Up.md" title="wikilink">You Raise Me Up</a>"<br />
<small> <a href="../Page/凯尔特女人.md" title="wikilink">凯尔特女人</a></small></p></td>
</tr>
<tr class="odd">
<td><p>2006-2007</p></td>
<td><p>"Olga" from <a href="../Page/Ladies_in_Lavender.md" title="wikilink">Ladies in Lavender</a> soundtrack</p></td>
<td><p>"Meditation" from "<a href="../Page/黛依丝.md" title="wikilink">黛依丝</a>"<br />
<small><a href="../Page/马斯内.md" title="wikilink">马斯内</a></small></p></td>
<td><p>"<a href="../Page/You_Raise_Me_Up.md" title="wikilink">You Raise Me Up</a>"<br />
<small> <a href="../Page/凯尔特女人.md" title="wikilink">凯尔特女人</a></small></p></td>
</tr>
<tr class="even">
<td><p>2005-2006</p></td>
<td><p>"<a href="../Page/亲爱的爸爸.md" title="wikilink">亲爱的爸爸</a>"<br />
<small><a href="../Page/普契尼.md" title="wikilink">普契尼</a></small></p></td>
<td><p>"Meditation" from "<a href="../Page/黛依丝.md" title="wikilink">黛依丝</a>"<br />
<small><a href="../Page/马斯内.md" title="wikilink">马斯内</a></small></p></td>
<td></td>
</tr>
</tbody>
</table>

## 重要比赛成绩

| 比赛                                                           | 2004-2005 | 2005-2006 | 2006-2007 | 2007-2008 | 2008-2009 |
| ------------------------------------------------------------ | --------- | --------- | --------- | --------- | --------- |
| 四大洲冠军赛                                                       |           |           |           |           | 第4        |
| 世界少年花样滑冰冠军赛                                                  |           |           | 冠军        | 亚军        | 亚军        |
| [美国花样滑冰冠军赛](../Page/美国花样滑冰冠军赛.md "wikilink")                 | 第4 N.     | 第8 J.     | 亚军 J.     | 第4        | 季军        |
| [大奖赛总决赛](../Page/国际滑冰联盟花样滑冰大奖赛总决赛.md "wikilink")             |           |           |           | 第4        |           |
| [国际滑冰联盟花样滑冰大奖赛法国站](../Page/国际滑冰联盟花样滑冰大奖赛法国站.md "wikilink")   |           |           |           |           | 季军        |
| [国际滑冰联盟花样滑冰大奖赛加拿大站](../Page/国际滑冰联盟花样滑冰大奖赛加拿大站.md "wikilink") |           |           |           |           | 第5        |
| [国际滑冰联盟花样滑冰大奖赛美国站](../Page/国际滑冰联盟花样滑冰大奖赛美国站.md "wikilink")   |           |           |           | 季军        |           |
| [中国杯](../Page/国际滑冰联盟花样滑冰大奖赛中国站.md "wikilink")                |           |           |           | 亚军        |           |
| 2006-2007 国际冰联少年大奖赛总决赛                                       |           |           | 冠军        |           |           |
| 2006-2007 国际冰联少年大奖赛墨西哥站                                      |           |           | 冠军        |           |           |
| 2006-2007 国际冰联少年大奖赛台北站                                       |           |           | 冠军        |           |           |
| Pacific Coast Sectionals                                     | 第4 N.     | 季军 J.     |           |           |           |
| Southwest Pacific Regionals                                  | 亚军 N.     | 亚军 J.     |           |           |           |

  - N = 新手组; J = 少年组

## 參考資料

## 導航

[Z](../Category/美国花样滑冰运动员.md "wikilink")
[Z](../Category/美国华裔运动员.md "wikilink")
[Z](../Category/麻薩諸塞州人.md "wikilink")
[Y圆圆](../Category/张姓.md "wikilink")

1.  <http://www.philly.com/philly/sports/high_school/pennsylvania/20071028_Effortlessly_gliding_to_top.html>
2.
3.
4.
5.
6.
7.
8.
9.