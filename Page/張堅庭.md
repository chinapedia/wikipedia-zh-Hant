**張堅庭**（**Alfred Cheung Kin
Ting**，），[香港知名](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")、[男演員](../Page/男演員.md "wikilink")、[編劇和](../Page/編劇.md "wikilink")[監製](../Page/監製.md "wikilink")，也曾擔任[電台及](../Page/電台.md "wikilink")[電視](../Page/電視.md "wikilink")[節目主持人](../Page/節目主持人.md "wikilink")。曾獲得1982年及1984年[香港電影金像獎最佳編劇](../Page/香港電影金像獎.md "wikilink")。2008獲[香港科技大學EMBA碩士學位](../Page/香港科技大學.md "wikilink")。妻子為[英皇集團主席](../Page/英皇集團.md "wikilink")[楊受成長女楊諾思](../Page/楊受成.md "wikilink")，有三子女。幼子是[傑志青年軍](../Page/傑志足球學校.md "wikilink")。外甥孫為[2012年度香港小姐競選冠軍](../Page/2012年度香港小姐競選.md "wikilink")[張名雅](../Page/張名雅.md "wikilink")。

## 生平

張堅庭於1956年在[廣東](../Page/廣東省.md "wikilink")[開平出生](../Page/開平市.md "wikilink")，3歲時隨家人來港\[1\]，曾就讀花地瑪英文學校（小學）、博允英文中學、[威靈頓英文中學](../Page/威靈頓英文中學.md "wikilink")，畢業於[香港浸會學院](../Page/香港浸會大學.md "wikilink")，其後於[香港中文大學進修電影文憑校外課程](../Page/香港中文大學.md "wikilink")。再遠赴[美國The](../Page/美國.md "wikilink")
New School For Social Research-Alfred Hitchcock
Studies深造電影，於1981年起參與編劇工作，1982年憑著《[胡越的故事](../Page/胡越的故事.md "wikilink")》，於1982年贏得[香港電影金像獎最佳編劇](../Page/香港電影金像獎.md "wikilink")；1983年自編自導《[表錯七日情](../Page/表錯七日情.md "wikilink")》，再獲得[香港電影金像獎最佳編劇](../Page/香港電影金像獎.md "wikilink")。

張堅庭參與製作之電影逾80部，一直以來都是以喜劇居多，並身兼電影導演、編劇、監製和演員各職，並獲出色成就。像1990年憑著《[表姐，你好嘢！](../Page/表姐，你好嘢！.md "wikilink")》，為主演的[鄭裕玲贏得](../Page/鄭裕玲.md "wikilink")[香港電影金像獎最佳女主角](../Page/香港電影金像獎.md "wikilink")；《[港督最後一個保鑣](../Page/港督最後一個保鑣.md "wikilink")》也獲得過香港電影金像獎多個提名。

自1990年代末張堅庭經常活躍於各類型的媒體，於1996年發展entertainment.com娛樂網站及飲食業包括越南菜館及表哥茶餐廳。近年他又發展戲劇訓練，透過戲劇訓練及角色扮演，學員可學習了解客人情緒及感受。張氏於2003年創辦「張堅庭戲劇工作坊」，曾為各大機構如稻香集團、康泰旅行社、香港郵政、電訊盈科、香港賽馬會、數碼通、太古可口可樂香港有限公司、香港迪士尼樂園、海洋公園等提供僱員訓練。2008年，張堅庭於[香港科技大學高級管理人員國際工商管理碩士課程](../Page/香港科技大學.md "wikilink")（IEMBA）畢業。

在[2007年香港行政長官選舉中](../Page/2007年香港行政長官選舉.md "wikilink")，曾指導泛民主派候選人[梁家傑表達技巧](../Page/梁家傑.md "wikilink")。

於2011-2012年度微笑企業大獎頒獎典禮擔任微笑大使，頒獎予港澳地區多個於微笑服務表現優異的企業。\[2\]

## 爭議

### 搶註泰昌餅家商標事件

2005年[泰昌餅家暫時結業期間](../Page/泰昌餅家.md "wikilink")，張堅庭曾與泰昌東主歐陽先生洽談合作，希望對方供應蛋撻給「表哥茶餐廳」(本身由張所經營)，但不果，由於泰昌經營多年來都未有到商標註冊處登記商標，於是張氏便單方面搶註泰昌商標。張表示因為自己是基督徒，做不到以註冊跟歐陽先生「講數」，於是開出條件要歐陽天閏捐款給宣明會，張便會
「物歸原主」，最終歐陽天閏捐款出五萬港元贖回商標使用權。事件引發香港網民激烈討論，有基督徒更質疑張氏此舉是否基督徒應有所為。有網民批評張堅庭這樣的行徑是「爛仔所為」，基督徒網民也質疑他逼人捐錢，並非基督徒應有的行為。\[3\]

### 助內地官員妻來港產子

早在2012年2月1日，在香港反蝗聲音高漲的時候，張堅庭已在明報發表一篇名為《星級爸爸﹕誰是蝗蟲？》的文章。同日，高登網民在[蘋果日報及](../Page/蘋果日報.md "wikilink")《爽報》刊登反對雙非孕婦來港產子的廣告。2月4日，張堅庭在其微博張貼了大罵[蘋果日報及其創辦人的留言](../Page/蘋果日報.md "wikilink")。留言內文批評他們為「冚x鏟的一群」，並指出有人在爭取政治資本而挑撥族群。2月中，張堅庭接受[蘋果日報專訪時透露](../Page/蘋果日報.md "wikilink")，他曾協助過一名大陸黨政機關官員妻子來港產子，此言一出更令網民嘩然，立即指他為「中介人」、「賣港賊」。\[4\]\[5\]\[6\]

## 電影

### 1980年代

|                                           |        |        |          |                                                                                                                                                                    |         |
| ----------------------------------------- | ------ | ------ | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------- |
| **片名**                                    | **年代** | **角色** | **職務**   | **合作**                                                                                                                                                             | **備註**  |
| [救世者](../Page/救世者.md "wikilink")          | 1980   |        | 編劇.演員    |                                                                                                                                                                    |         |
| [文仔的肥皂泡](../Page/文仔的肥皂泡.md "wikilink")    | 1981   |        | 編劇       |                                                                                                                                                                    |         |
| [父子情](../Page/父子情.md "wikilink")          | 1981   | 未演出    | 編劇       |                                                                                                                                                                    |         |
| [胡越的故事](../Page/胡越的故事.md "wikilink")      | 1981   | 未演出    | 編劇       |                                                                                                                                                                    |         |
| [馬騮過海](../Page/馬騮過海.md "wikilink")        | 1982   | 未演出    | 導演       |                                                                                                                                                                    |         |
| [血汗金錢](../Page/血汗金錢.md "wikilink")        | 1983   | 未演出    | 編劇       |                                                                                                                                                                    |         |
| [星際鈍胎](../Page/星際鈍胎.md "wikilink")        | 1983   |        | 演員       |                                                                                                                                                                    |         |
| [表錯七日情](../Page/表錯七日情.md "wikilink")      | 1983   |        | 導演.編劇.演員 |                                                                                                                                                                    |         |
| [城市之光](../Page/城市之光.md "wikilink")        | 1984   |        | 導演.編劇.演員 |                                                                                                                                                                    |         |
| [青蛙王子](../Page/青蛙王子_\(電影\).md "wikilink") | 1984   |        | 演員       |                                                                                                                                                                    |         |
| [三十處男](../Page/三十處男.md "wikilink")        | 1984   |        | 編劇.策劃.演員 | [鄭文雅](../Page/鄭文雅.md "wikilink")                                                                                                                                   |         |
| [緣份](../Page/緣份.md "wikilink")            | 1984   |        | 演員       |                                                                                                                                                                    |         |
| [花心蘿蔔](../Page/花心蘿蔔.md "wikilink")        | 1984   |        | 演員       |                                                                                                                                                                    |         |
| [再見七日情](../Page/再見七日情.md "wikilink")      | 1985   | 未演出    | 導演.編劇    |                                                                                                                                                                    |         |
| [鬼馬飛人](../Page/鬼馬飛人.md "wikilink")        | 1985   | 廣告導演   | 演員       | [鍾鎮濤](../Page/鍾鎮濤.md "wikilink")、[鄭則士](../Page/鄭則士.md "wikilink")                                                                                                  | 客串      |
| [天使出更](../Page/天使出更.md "wikilink")        | 1985   |        | 編劇.演員    |                                                                                                                                                                    |         |
| [平安夜](../Page/平安夜.md "wikilink")          | 1985   |        | 演員       |                                                                                                                                                                    |         |
| [夏日福星](../Page/夏日福星.md "wikilink")        | 1985   |        | 演員       |                                                                                                                                                                    |         |
| [替槍老豆](../Page/替槍老豆.md "wikilink")        | 1985   | 未演出    | 編劇       |                                                                                                                                                                    |         |
| [富貴列車](../Page/富貴列車_\(電影\).md "wikilink") | 1986   | 未演出    | 編劇       |                                                                                                                                                                    |         |
| [兩公婆八條心](../Page/兩公婆八條心.md "wikilink")    | 1986   |        | 導演.編劇.演員 | [葉童](../Page/葉童.md "wikilink")、[錢慧儀](../Page/錢慧儀.md "wikilink")                                                                                                    | 【雙生傷生】篇 |
| [玫瑰的故事](../Page/玫瑰的故事.md "wikilink")      | 1986   |        | 演員       |                                                                                                                                                                    |         |
| [最佳福星](../Page/最佳福星.md "wikilink")        | 1986   |        | 故事.演員    |                                                                                                                                                                    |         |
| [老娘夠騷](../Page/老娘夠騷.md "wikilink")        | 1986   |        | 演員       |                                                                                                                                                                    |         |
| [一屋兩妻](../Page/一屋兩妻.md "wikilink")        | 1987   | 未演出    | 編劇.策劃    |                                                                                                                                                                    |         |
| [標錯參](../Page/標錯參.md "wikilink")          | 1987   | 未演出    | 導演.編劇    |                                                                                                                                                                    | 台：綁錯票   |
| [亡命鴛鴦](../Page/亡命鴛鴦.md "wikilink")        | 1988   | 未演出    | 導演.編劇    |                                                                                                                                                                    |         |
| [一妻兩夫](../Page/一妻兩夫.md "wikilink")        | 1988   | 劇團演員   | 編劇.演員    |                                                                                                                                                                    | 客串      |
| [過埠新娘](../Page/過埠新娘.md "wikilink")        | 1988   |        | 導演.編劇.演員 |                                                                                                                                                                    |         |
| [鬼掹腳](../Page/鬼掹腳.md "wikilink")          | 1988   | 王小明    | 演員       | [高麗虹](../Page/高麗虹.md "wikilink")、[午馬](../Page/午馬.md "wikilink")、[陳友](../Page/陳友.md "wikilink")                                                                     | 台：鬼抓人   |
| [神探父子兵](../Page/神探父子兵.md "wikilink")      | 1988   |        | 演員       |                                                                                                                                                                    |         |
| [群鶯亂舞](../Page/群鶯亂舞.md "wikilink")        | 1988   |        | 演員       |                                                                                                                                                                    |         |
| [小男人周記](../Page/小男人周記.md "wikilink")      | 1989   |        | 演員       |                                                                                                                                                                    |         |
| [群龍戲鳳](../Page/群龍戲鳳.md "wikilink")        | 1989   | 珠寶店員   | 演員       | [孫越](../Page/孫越.md "wikilink")、[利智](../Page/利智.md "wikilink")                                                                                                      | 客串      |
| [再見王老五](../Page/再見王老五.md "wikilink")      | 1989   | 喜宴賓客   | 策劃.演員    |                                                                                                                                                                    | 客串      |
| [不脫襪的人](../Page/不脫襪的人.md "wikilink")      | 1989   | 未演出    | 策劃       |                                                                                                                                                                    |         |
| [求愛夜驚魂](../Page/求愛夜驚魂.md "wikilink")      | 1989   | 阿龍     | 編劇.策劃.演員 | [張曼玉](../Page/張曼玉.md "wikilink")、[吳君如](../Page/吳君如.md "wikilink")、[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")、[劉錫賢](../Page/劉錫賢.md "wikilink")、[胡楓](../Page/胡楓.md "wikilink") |         |
|                                           |        |        |          |                                                                                                                                                                    |         |

### 1990年代

<table>
<tbody>
<tr class="odd">
<td><p><strong>片名</strong></p></td>
<td><p><strong>年代</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>職務</strong></p></td>
<td><p><strong>合作</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/漫畫奇俠.md" title="wikilink">漫畫奇俠</a></p></td>
<td><p>1990</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/表姐，你好嘢！.md" title="wikilink">表姐，你好嘢！</a></p></td>
<td><p>1990</p></td>
<td><p>張寶勝</p></td>
<td><p>導演.編劇.演員</p></td>
<td><p><a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/梁家輝.md" title="wikilink">梁家輝</a>、<a href="../Page/林蛟.md" title="wikilink">林蛟</a>、<a href="../Page/方剛_(演員).md" title="wikilink">方剛</a> 、<a href="../Page/周文健.md" title="wikilink">周文健</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/搞怪大律師.md" title="wikilink">搞怪大律師</a></p></td>
<td><p>1990</p></td>
<td><p>張大狀</p></td>
<td><p>導演.編劇.演員</p></td>
<td><p>鄭裕玲、梁家輝、<a href="../Page/劉嘉玲.md" title="wikilink">劉嘉玲</a>、<a href="../Page/吳家麗.md" title="wikilink">吳家麗</a></p></td>
<td><p>臺：古惑大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/衰鬼撬牆腳.md" title="wikilink">衰鬼撬牆腳</a></p></td>
<td><p>1990</p></td>
<td><p>David</p></td>
<td><p>演員</p></td>
<td><p><a href="../Page/胡楓.md" title="wikilink">胡楓</a>、<a href="../Page/吳耀漢.md" title="wikilink">吳耀漢</a>、<a href="../Page/劉玉婷.md" title="wikilink">劉玉婷</a>、<a href="../Page/司馬燕.md" title="wikilink">司馬燕</a>、<a href="../Page/陳友.md" title="wikilink">陳友</a>、<a href="../Page/王文君.md" title="wikilink">王文君</a></p></td>
<td><p>臺：衰鬼要上床</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夜魔先生.md" title="wikilink">夜魔先生</a></p></td>
<td><p>1990</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我老婆唔係人.md" title="wikilink">我老婆唔係人</a></p></td>
<td><p>1991</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td><p>臺：我老婆不是人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/表姐，你好嘢！2.md" title="wikilink">表姐，你好嘢！2</a></p></td>
<td><p>1991</p></td>
<td><p>張寶勝</p></td>
<td><p>導演.編劇.演員</p></td>
<td><p>鄭裕玲、<a href="../Page/李子雄.md" title="wikilink">李子雄</a>、林蛟、周文健</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/豪門夜宴_(1991年電影).md" title="wikilink">豪門夜宴</a></p></td>
<td><p>1991</p></td>
<td><p>未演出</p></td>
<td><p>導演.編劇</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/與龍共舞.md" title="wikilink">與龍共舞</a></p></td>
<td><p>1991</p></td>
<td><p>鞏俐<br />
馬丁鞏</p></td>
<td><p>演員</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/張敏_(演員).md" title="wikilink">張敏</a>、<a href="../Page/葉德嫺.md" title="wikilink">葉德嫺</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/羅美薇.md" title="wikilink">羅美薇</a>、<a href="../Page/鮑漢琳.md" title="wikilink">鮑漢琳</a>、<a href="../Page/翁虹.md" title="wikilink">翁虹</a>、<a href="../Page/李香琴.md" title="wikilink">李香琴</a>、<a href="../Page/朱江.md" title="wikilink">朱江</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夜夜伴肥嬌.md" title="wikilink">夜夜伴肥嬌</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙龍會.md" title="wikilink">雙龍會</a></p></td>
<td><p>1992</p></td>
<td><p>黑幫老大</p></td>
<td><p>演員</p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成龍</a>、<a href="../Page/泰迪羅賓.md" title="wikilink">泰迪羅賓</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/逃學英雄傳.md" title="wikilink">逃學英雄傳</a></p></td>
<td><p>1992</p></td>
<td><p>侯瑞堅</p></td>
<td><p>演員</p></td>
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a>、張敏、<a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a>、吳孟達、<a href="../Page/陳惠敏_(演員).md" title="wikilink">陳惠敏</a>、<a href="../Page/黃炳耀_(編劇).md" title="wikilink">黃炳耀</a>、<a href="../Page/陳國新.md" title="wikilink">陳國新</a>、<a href="../Page/黃一山.md" title="wikilink">黃一山</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/太子爺出差.md" title="wikilink">太子爺出差</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>編劇.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/表姐，妳好嘢！3之大人駕到.md" title="wikilink">表姐，妳好嘢！3之大人駕到</a></p></td>
<td><p>1992</p></td>
<td><p>張寶勝</p></td>
<td><p>導演.編劇.演員</p></td>
<td><p>鄭裕玲、<a href="../Page/陳松勇.md" title="wikilink">陳松勇</a>、<a href="../Page/黃秋生.md" title="wikilink">黃秋生</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丐世英雄_(1992年電影).md" title="wikilink">丐世英雄</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>演員</p></td>
<td><p><a href="../Page/許冠文.md" title="wikilink">許冠文</a>、<a href="../Page/吳君如.md" title="wikilink">吳君如</a>、<a href="../Page/雷宇揚.md" title="wikilink">雷宇揚</a>、<a href="../Page/黎彼得.md" title="wikilink">黎彼得</a>、陳惠敏</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/家有喜事.md" title="wikilink">家有喜事</a></p></td>
<td><p>1992</p></td>
<td><p>神經病</p></td>
<td><p>客串</p></td>
<td><p><a href="../Page/張國榮.md" title="wikilink">張國榮</a>、<a href="../Page/黃百鳴.md" title="wikilink">黃百鳴</a>、<a href="../Page/周星馳.md" title="wikilink">周星馳</a>、<a href="../Page/張曼玉.md" title="wikilink">張曼玉</a>、<a href="../Page/毛舜筠.md" title="wikilink">毛舜筠</a>、<a href="../Page/吳君如.md" title="wikilink">吳君如</a>、李香琴、<a href="../Page/關海山.md" title="wikilink">關海山</a>、<a href="../Page/李麗珍.md" title="wikilink">李麗珍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/兩屋一妻.md" title="wikilink">兩屋一妻</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/細佬識講嘢.md" title="wikilink">細佬識講嘢</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>導演.編劇.監製.演員</p></td>
<td><p>陳惠敏</p></td>
<td><p>臺：色魔抓狂</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飛女正傳_(1992年電影).md" title="wikilink">飛女正傳</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>演員</p></td>
<td><p>張敏、吳孟達、<a href="../Page/刘玉翠.md" title="wikilink">刘玉翠</a>、<a href="../Page/楊麗青.md" title="wikilink">楊麗青</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏日情人.md" title="wikilink">夏日情人</a></p></td>
<td><p>1992</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神槍手與咖喱雞.md" title="wikilink">神槍手與咖喱雞</a></p></td>
<td><p>1992</p></td>
<td><p>張家長</p></td>
<td><p>演員</p></td>
<td><p><a href="../Page/張學友.md" title="wikilink">張學友</a>、<a href="../Page/李麗珍.md" title="wikilink">李麗珍</a>、<a href="../Page/林正英.md" title="wikilink">林正英</a>、<a href="../Page/董瑋.md" title="wikilink">董瑋</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盲女七十二小時.md" title="wikilink">盲女七十二小時</a></p></td>
<td><p>1993</p></td>
<td></td>
<td><p>監製.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富貴狂花.md" title="wikilink">富貴狂花</a></p></td>
<td><p>1993</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/表姐，你好嘢！4情不自禁.md" title="wikilink">表姐，你好嘢！4情不自禁</a></p></td>
<td><p>1994</p></td>
<td><p>張寶勝</p></td>
<td><p>導演.編劇.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴綠帽的女人.md" title="wikilink">戴綠帽的女人</a></p></td>
<td><p>1995</p></td>
<td></td>
<td><p>導演.編劇.監製.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/驚變.md" title="wikilink">驚變</a></p></td>
<td><p>1996</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人細鬼大.md" title="wikilink">人細鬼大</a></p></td>
<td><p>1996</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/港督最後一個保鑣.md" title="wikilink">港督最後一個保鑣</a></p></td>
<td><p>1996</p></td>
<td></td>
<td><p>導演</p></td>
<td><p><a href="../Page/葛文輝.md" title="wikilink">葛文輝</a>、邱淑貞、<a href="../Page/羅家英.md" title="wikilink">羅家英</a>、周文健、<a href="../Page/陳曼娜.md" title="wikilink">陳曼娜</a>、<a href="../Page/黃毓民.md" title="wikilink">黃毓民</a>、<a href="../Page/羅冠蘭.md" title="wikilink">羅冠蘭</a>、<a href="../Page/李純恩.md" title="wikilink">李純恩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/基佬四十.md" title="wikilink">基佬四十</a></p></td>
<td><p>1997</p></td>
<td></td>
<td><p>演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/97家有囍事.md" title="wikilink">97家有囍事</a></p></td>
<td><p>1997</p></td>
<td></td>
<td><p>導演.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/全職大盜.md" title="wikilink">全職大盜</a></p></td>
<td><p>1998</p></td>
<td><p>高明</p></td>
<td><p>導演.編劇.監製.演員</p></td>
<td><p>黃秋生</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/失業特工隊.md" title="wikilink">失業特工隊</a></p></td>
<td><p>1998</p></td>
<td></td>
<td><p>導演.編劇.監製.演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 2000年代

|                                            |        |        |          |                                                                   |        |
| ------------------------------------------ | ------ | ------ | -------- | ----------------------------------------------------------------- | ------ |
| **片名**                                     | **年代** | **角色** | **職務**   | **合作**                                                            | **備註** |
| [特務迷城](../Page/特務迷城.md "wikilink")         | 2001   |        | 演員       |                                                                   |        |
| [老夫子2001](../Page/老夫子2001.md "wikilink")   | 2001   | 校長     | 演員       | [老夫子](../Page/老夫子.md "wikilink")                                  |        |
| [殺科](../Page/殺科.md "wikilink")             | 2001   |        | 演員       |                                                                   |        |
| [風流家族](../Page/風流家族.md "wikilink")         | 2002   | 張堅庭    | 演員       |                                                                   |        |
| [反收數特遣隊](../Page/反收數特遣隊.md "wikilink")     | 2002   |        | 演員       |                                                                   |        |
| [金雞](../Page/金雞.md "wikilink")             | 2002   |        | 演員       | [吳君如](../Page/吳君如.md "wikilink")                                  |        |
| [賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink")   | 2003   |        | 演員       | [楊恭如](../Page/楊恭如.md "wikilink")                                  |        |
| [飛龍再生](../Page/飛龍再生.md "wikilink")         | 2003   |        | 編劇.監製.演員 | [成龍](../Page/成龍.md "wikilink")                                    |        |
| [少年阿虎](../Page/少年阿虎.md "wikilink")         | 2003   |        | 演員       |                                                                   |        |
| [大丈夫2](../Page/大丈夫2.md "wikilink")         | 2006   | 盧醫生    | 演員       |                                                                   |        |
| [合約情人](../Page/合約情人.md "wikilink")         | 2007   |        | 編劇.導演.演員 |                                                                   |        |
| [七天愛上你](../Page/七天愛上你.md "wikilink")       | 2009   |        | 編劇.導演.演員 | [賀軍翔](../Page/賀軍翔.md "wikilink")、[李小璐](../Page/李小璐.md "wikilink") |        |
| [72家租客](../Page/72家租客.md "wikilink")       | 2010   | 醫生     | 演員       | [曾志偉](../Page/曾志偉.md "wikilink")                                  |        |
| [我愛HK開心萬歲](../Page/我愛HK開心萬歲.md "wikilink") | 2011   | TVB導演  | 演員       |                                                                   |        |
| [Z風暴](../Page/Z風暴.md "wikilink")           | 2014   |        | 演員       | [古天樂](../Page/古天樂.md "wikilink")                                  |        |
| [贫穷富爸爸](../Page/贫穷富爸爸.md "wikilink")       | 2015   |        | 導演       |                                                                   |        |

## 電視劇

  - 2004 《[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")》飾 黃材俊
  - 1977
    [香港電台](../Page/香港電台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")《[小時候](../Page/小時候.md "wikilink")—爸爸打我》飾
    小學中文老師

## 電視主持

  - 《荔園小天地》麗的電視
  - 《香港傳奇》無線電視
  - 《向男講向女講》無線電視

## 獎項

### 香港電影金像獎

<table style="width:135%;">
<colgroup>
<col style="width: 54%" />
<col style="width: 15%" />
<col style="width: 8%" />
<col style="width: 9%" />
<col style="width: 48%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1982年</p></td>
<td><p><a href="../Page/第1屆香港電影金像獎.md" title="wikilink">第1屆香港電影金像獎</a></p></td>
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/胡越的故事.md" title="wikilink">胡越的故事</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/第3屆香港電影金像獎.md" title="wikilink">第3屆香港電影金像獎</a></p></td>
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/表錯七日情.md" title="wikilink">表錯七日情</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 金馬獎

<table style="width:135%;">
<colgroup>
<col style="width: 54%" />
<col style="width: 15%" />
<col style="width: 8%" />
<col style="width: 9%" />
<col style="width: 48%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/第27屆金馬獎.md" title="wikilink">第27屆金馬獎</a></p></td>
<td><p>最佳改编剧本</p></td>
<td><p><a href="../Page/表姐，妳好嘢！.md" title="wikilink">表姐，妳好嘢！</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 日本電影學院獎

<table style="width:135%;">
<colgroup>
<col style="width: 54%" />
<col style="width: 15%" />
<col style="width: 8%" />
<col style="width: 9%" />
<col style="width: 48%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/第23回日本電影學院獎.md" title="wikilink">第23回日本電影學院獎</a></p></td>
<td><p>话题奖-最具话题影片奖</p></td>
<td><p><a href="../Page/无问题.md" title="wikilink">无问题</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [張堅庭個人簡介](http://www.hk5sa.com/hk5sa/board.htm#alfredch)

  - 中文電影資料庫：[張堅庭](http://www.dianying.com/ft/person/ZhangJianting)

  -
  -
  -
  -
[Category:香港電影金像獎最佳編劇得主](../Category/香港電影金像獎最佳編劇得主.md "wikilink")
[Category:1955年出生](../Category/1955年出生.md "wikilink")
[Category:張姓](../Category/張姓.md "wikilink")
[Category:开平人](../Category/开平人.md "wikilink")
[Category:20世紀導演](../Category/20世紀導演.md "wikilink")
[Category:21世紀導演](../Category/21世紀導演.md "wikilink")
[Category:香港電影导演](../Category/香港電影导演.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港编剧](../Category/香港编剧.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港飲食界人士](../Category/香港飲食界人士.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")
[Category:香港科技大學校友](../Category/香港科技大學校友.md "wikilink")
[Category:威靈頓英文中學校友](../Category/威靈頓英文中學校友.md "wikilink")

1.  [反蝗反表姐　表哥你好 嘢](http://www.youtube.com/watch?v=UFUY6LBuW9c).蘋果動新聞 HK
    Apple Daily.2012-02-15
2.  [香港笑容指數全球排名尾3](https://www.facebook.com/notes/%E7%A5%9E%E7%A7%98%E9%A1%A7%E5%AE%A2-%E5%85%BC%E8%81%B7-mystery-shopper-part-time-job/%E5%BE%AE%E7%AC%91%E5%A0%B1%E5%91%8A%E5%8F%8A%E5%BE%AE%E7%AC%91%E4%BC%81%E6%A5%AD/10150710116517017/).「微笑報告」及「微笑企業」facebook
3.  [基督徒給著名餅家兩條路──結業或捐錢給宣明會](http://newsofreligion.blogspot.com/2005/12/blog-post_12.html).蘋果日報.2005-12-12
4.  [張堅庭:肥佬黎是全家鏟\!](https://forum.hkgolden.com/view.aspx?message=3532889)
    香港高登討論區
5.  [蘋果日報：張堅庭：文化差異勿變仇恨
    助內地官員妻來港產子](https://hk.news.appledaily.com/local/daily/article/20120215/16071392)
6.  [高登討論區：\[中介](https://forum.hkgolden.com/view.aspx?type=CA&message=3556032)
    張堅庭協助內地黨官妻子來港產子\]