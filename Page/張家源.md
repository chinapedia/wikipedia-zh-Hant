**張家源**（1980年9月21日—），台灣棒球選手，[中華職棒前](../Page/中華職棒.md "wikilink")[中信鯨隊球員](../Page/中信鯨.md "wikilink")，守備位置為[游擊手](../Page/游擊手.md "wikilink")。曾經於2005\~2007年效力於[統一獅](../Page/統一獅.md "wikilink")，2008年效力於[中信鯨](../Page/中信鯨.md "wikilink")。

## 經歷

  - [台北市福林國小少棒隊](../Page/台北市.md "wikilink")
  - [台北市](../Page/台北市.md "wikilink")[華興中學青少棒隊](../Page/華興中學.md "wikilink")
  - [台北市](../Page/台北市.md "wikilink")[華興中學青棒隊](../Page/華興中學.md "wikilink")
  - [輔仁大學棒球隊](../Page/輔仁大學.md "wikilink")（中興保全）
  - [中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")（2005年－2007年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")(2008－2008年11月11日)

## 特殊事蹟

  - 2008年10月10日，在[台北縣立新莊棒球場對上](../Page/台北縣立新莊棒球場.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")[小林亮寬擊出生涯第一支全壘打](../Page/小林亮寬.md "wikilink")，也是[中信鯨隊史最後一支全壘打](../Page/中信鯨.md "wikilink")。

## 外部連結

[C](../Category/張姓.md "wikilink") [C](../Category/在世人物.md "wikilink")
[C](../Category/1980年出生.md "wikilink")
[C](../Category/統一獅隊球員.md "wikilink")
[C](../Category/中信鯨隊球員.md "wikilink")
[C](../Category/台灣棒球選手.md "wikilink")
[C](../Category/輔仁大學校友.md "wikilink")