**張懋修**（），[字](../Page/表字.md "wikilink")**維時**，[號](../Page/號.md "wikilink")**斗樞**，[湖广](../Page/湖广承宣布政使司.md "wikilink")[江陵](../Page/江陵.md "wikilink")（今[湖北](../Page/湖北.md "wikilink")[江陵](../Page/江陵.md "wikilink")）人。[明朝政治人物](../Page/明朝.md "wikilink")，[首輔](../Page/首輔.md "wikilink")[張居正三子](../Page/張居正.md "wikilink")。

## 生平

[明神宗](../Page/明神宗.md "wikilink")[萬曆八年](../Page/萬曆.md "wikilink")（1580年）庚辰科以關節獲中[狀元](../Page/狀元.md "wikilink")\[1\]\[2\]\[3\]。[万历十二年](../Page/万历.md "wikilink")（1584年）四月，诏令查抄張居正家产，[司礼監](../Page/司礼監.md "wikilink")[太监](../Page/太监.md "wikilink")[张诚](../Page/张诚.md "wikilink")，[刑部](../Page/刑部.md "wikilink")[右侍郎](../Page/右侍郎.md "wikilink")[邱橓及](../Page/邱橓.md "wikilink")[锦衣卫](../Page/锦衣卫.md "wikilink")、[给事中等奉命前往](../Page/给事中.md "wikilink")。长兄[張敬修不堪拷问](../Page/張敬修_\(明\).md "wikilink")，自缢而死。二哥[張嗣修投井自殺未遂](../Page/張嗣修.md "wikilink")，又絕食又不死，被发配邊疆。[明思宗](../Page/明思宗.md "wikilink")[崇禎十二年](../Page/崇禎.md "wikilink")（1639年）懋修戍烟瘴地而死。[崇禎十三年](../Page/崇禎.md "wikilink")（1640年）下詔恢復張懋修官職\[4\]。

## 注釋

## 參考書目

  - 朱東潤：《張居正大傳》

[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:江陵人](../Category/江陵人.md "wikilink")
[M](../Category/張姓.md "wikilink")

1.
2.
3.
4.  《明史·张居正传》：「崇祯三年，[礼部](../Page/礼部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")[罗喻义等讼居正冤](../Page/罗喻义.md "wikilink")。帝令部议，复二荫及诰命。十三年，敬修孙[同敞请复武荫](../Page/張同敞.md "wikilink")，并复敬修官。帝授同敞[中书舍人](../Page/中书舍人.md "wikilink")，而下部议敬修事。」