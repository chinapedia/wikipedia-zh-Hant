**张树声**（），[字](../Page/表字.md "wikilink")**振轩**，安徽[合肥人](../Page/合肥.md "wikilink")，为[中国](../Page/中国.md "wikilink")[清末](../Page/清.md "wikilink")[淮军将领](../Page/淮军.md "wikilink")。

## 生平

张树声为[廪生出身](../Page/廪生.md "wikilink")；1853年（[咸丰三年](../Page/咸丰_\(年号\).md "wikilink")）在乡办团练，1862年（[同治元年](../Page/同治.md "wikilink")）随[李鸿章来到上海](../Page/李鸿章.md "wikilink")，与[刘铭传等分领淮军进攻太平军](../Page/刘铭传.md "wikilink")；后来参与镇压[捻军](../Page/捻军.md "wikilink")；1872年任江苏巡抚。光绪五年闰三月十三，由[贵州巡抚调任](../Page/贵州巡抚.md "wikilink")[廣西巡撫](../Page/廣西巡撫.md "wikilink")。光绪五年十一月十五（1879年12月27日），升任[两广总督](../Page/两广总督.md "wikilink")\[1\]。於1879年12月27日－1882年4月19日期間，奉旨接替[裕寬擔任](../Page/裕寬.md "wikilink")[兩廣總督](../Page/兩廣總督.md "wikilink")；全名為「總督兩廣等處地方提督軍務、糧饟兼巡撫事」的該官職，是兼轄[廣西地區的](../Page/廣西.md "wikilink")[廣東](../Page/廣東.md "wikilink")、廣西兩省之最高統治者，亦為清朝[封疆大吏之一](../Page/封疆大吏.md "wikilink")。

張樹聲又於1882年—1883年，擔任[直隶总督](../Page/直隶总督.md "wikilink")；1884年11月病逝于广州，谥靖达。

## 參考文獻

[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:清朝直隶总督](../Category/清朝直隶总督.md "wikilink")
[S樹](../Category/張姓.md "wikilink")
[Category:清朝廣西巡撫](../Category/清朝廣西巡撫.md "wikilink")
[Category:淮軍人物](../Category/淮軍人物.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")

1.