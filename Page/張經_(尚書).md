**張經**（），[字](../Page/表字.md "wikilink")**廷彝**，[號](../Page/號.md "wikilink")**半洲**，[福建](../Page/福建.md "wikilink")[侯官縣](../Page/侯官縣.md "wikilink")（今[福建](../Page/福建.md "wikilink")[福州市](../Page/福州.md "wikilink")）[洪塘鄉人](../Page/洪塘鄉_\(福州\).md "wikilink")，是明朝抗倭將領，為人剛正，得罪[嚴嵩](../Page/嚴嵩.md "wikilink")、[趙文華](../Page/趙文華.md "wikilink")、[胡宗憲等](../Page/胡宗憲.md "wikilink")，因而被[明世宗處死](../Page/明世宗.md "wikilink")，著有《半洲詩集》傳世。[穆宗時追諡](../Page/明穆宗.md "wikilink")**襄愍**。

## 生平

張經其父曾改[蔡姓](../Page/蔡姓.md "wikilink")，後復[張姓](../Page/張姓.md "wikilink")。[正德十二年](../Page/正德_\(明朝\).md "wikilink")（1517年）[進士](../Page/進士.md "wikilink")，任[浙江](../Page/浙江.md "wikilink")[嘉興](../Page/嘉興.md "wikilink")[知縣](../Page/知縣.md "wikilink")。

[嘉靖四年](../Page/嘉靖.md "wikilink")（1525年），入京任[戶部](../Page/戶部.md "wikilink")、[吏部給事中](../Page/吏部.md "wikilink")，曾上疏劾罷納賄的[兵部](../Page/兵部.md "wikilink")[尚書](../Page/尚書.md "wikilink")[金獻民](../Page/金獻民.md "wikilink")、匿災不報的[河南](../Page/河南.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")[潘塤](../Page/潘塤.md "wikilink")，並請撤除[錦衣衛](../Page/錦衣衛.md "wikilink")。

[嘉靖十六年](../Page/嘉靖.md "wikilink")（1537年）[總督兩廣軍務](../Page/兩廣總督.md "wikilink")。升為南京兵部左[侍郎](../Page/侍郎.md "wikilink")，又升兵部[尚書](../Page/尚書.md "wikilink")。

[嘉靖三十三年](../Page/嘉靖.md "wikilink")（1554年），[倭寇襲擾中國東南](../Page/倭寇.md "wikilink")，[南京兵部尚書張經任總督](../Page/南京兵部尚書.md "wikilink")[江南](../Page/江南.md "wikilink")、江北、[浙江](../Page/浙江.md "wikilink")、[山東](../Page/山東.md "wikilink")、[福建](../Page/福建.md "wikilink")、[湖廣諸軍](../Page/湖廣.md "wikilink")；張經每日選將練兵，徵調“狼兵”（[廣西壯族官兵](../Page/廣西.md "wikilink")），按兵不動。[明世宗大怒](../Page/明世宗.md "wikilink")，命張經限期進兵，並派兵部[侍郎](../Page/侍郎.md "wikilink")[趙文華至浙督師](../Page/趙文華.md "wikilink")，催張出兵。趙文華大怒，指示黨羽[胡宗憲疏劾張經](../Page/胡宗憲.md "wikilink")「糜餉殃民，畏賊失機」。

[嘉靖三十四年](../Page/嘉靖.md "wikilink")（1555年）四月二十日張經副總[俞大猷和參將](../Page/俞大猷.md "wikilink")[盧鏜](../Page/盧鏜.md "wikilink")、[湯克寬等](../Page/湯克寬.md "wikilink")，率水陸軍聯合抗擊，湯克寬率水師由中間出擊，俞大猷、盧鏜前後夾擊，在石塘灣、川沙洼（今[上海川沙縣](../Page/上海.md "wikilink")）、王江涇（今[嘉興縣北州里](../Page/嘉興縣.md "wikilink")）獲大勝，斬殺敵一千九百餘人，俘獲倭寇五千，史稱[王江涇大捷](../Page/王江涇大捷.md "wikilink")。

此時，明世宗卻以抗倭不力罪名，遣[錦衣衛捉拿張經](../Page/錦衣衛.md "wikilink")。《明史》記載[嚴嵩授意刑部尚書](../Page/嚴嵩.md "wikilink")[何鰲](../Page/何鰲.md "wikilink")，將張經、[李天寵擬定死罪](../Page/李天寵.md "wikilink")，“（严）嵩皆有力焉。”\[1\]，十月初一，張經與[湯克寬](../Page/湯克寬.md "wikilink")、[李天寵](../Page/李天寵.md "wikilink")、[楊繼盛等同斬於](../Page/楊繼盛.md "wikilink")[西市](../Page/西四牌樓.md "wikilink")\[2\]。时人多为其称冤。王世贞指出，张经之死与徐阶有很大关系。\[3\]

[趙文華將破倭之功據為已有](../Page/趙文華.md "wikilink")，至於[胡宗憲則連升三級](../Page/胡宗憲.md "wikilink")，本職由正七品的監察御史一跳而成為正四品的右僉都御史。

明穆宗[隆慶六年](../Page/隆慶.md "wikilink")（1572年），張經之孫[張懋爵上疏鳴冤](../Page/張懋爵.md "wikilink")，朝廷復其官職，諡**襄愍**。

## 注釋

  - 鄭樑生：〈[張經與王江涇之役──明嘉靖間之剿倭戰事研究](http://ccsdb.ncl.edu.tw/ccs/image/01_010_002_01_13.pdf)〉。

[Category:明朝嘉興縣知縣](../Category/明朝嘉興縣知縣.md "wikilink")
[Category:明朝給事中](../Category/明朝給事中.md "wikilink")
[Category:明朝兩廣總督](../Category/明朝兩廣總督.md "wikilink")
[Category:南京兵部尚書](../Category/南京兵部尚書.md "wikilink")
[Category:明朝被處決者](../Category/明朝被處決者.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[J](../Category/張姓.md "wikilink")
[Category:諡襄愍](../Category/諡襄愍.md "wikilink")

1.  《明史》卷308《严嵩传》
2.  《嘉靖东南平倭通录》记载，世宗在处死张经以后，曾詢问严嵩，張經是否冤死，严嵩回答说：“此事，臣昨问徐阶、吕本二臣，以乡邦被惨，闻见甚真，皆怨（张）经养寇损威，殃民糜饷，不逮问无以正法。”
3.  [王世贞指出](../Page/王世贞.md "wikilink")，徐阶家在松江华亭，“而是时倭事起，上以所蹂躏多（徐）阶乡，而（徐）阶又晓畅军事，以故数以询问（徐阶）……江南督臣张经素贵而汰，然老将能持重，守便宜，不轻与贼斗。而恶之者谓（张）经家在闽，故近贼，不欲击以市恩。而（徐）阶信之，数龁于上。其后（张）经破贼，卒不免于死。前后督臣杨宜、周珫斥，抚臣彭黯、屠大山、李天宠逮，（徐）阶有力焉。”（《献征录》卷16《大学士徐公阶传》）