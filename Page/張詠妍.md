**張詠妍**（，），前[香港商業電台](../Page/香港商業電台.md "wikilink")[叱吒903](../Page/叱吒903.md "wikilink")[節目主持](../Page/節目主持.md "wikilink")，現為全港首間
Kids and family agency - ohmykids.org 的聯合創辦人。

## 簡歷

她是[素食主義者](../Page/素食主義者.md "wikilink")。讀書時曾任兼職[模特兒](../Page/模特兒.md "wikilink")。[時裝設計系畢業後](../Page/時裝設計.md "wikilink")，曾任職時裝記者、[櫥窗設計及演員等工作](../Page/櫥窗.md "wikilink")。2005年參加「903發開口夢」[DJ選拔賽](../Page/DJ.md "wikilink")，後加入了商業電台。

2009年7月31日離開商台。其後繼續參與[舞台劇等演出](../Page/舞台劇.md "wikilink")。2013年她和朋友創辦全港首間
Kids and family agency - ohmykids.org，以創意推行相信及尊重孩子的理念。

## 家庭

2006年8月6日與商台903任唱片騎師的同事[細So](../Page/細So.md "wikilink")（蘇耀宗）結婚。

2009年年初，因性格不合，兩人低調協議離婚。翌年3月嫁給商台創作總監龐健章（即填詞人[林若寧](../Page/林若寧.md "wikilink")），兩人育有一對女兒。

## 曾參與之分享會

  - 【TEDxKowloon】 重構想像（2016年）

## 曾主持之電台節目

  - [903 發開口夢](../Page/903_發開口夢.md "wikilink")
  - [五天精華遊](../Page/五天精華遊.md "wikilink")
  - [903 傍住你放榜](../Page/903_傍住你放榜.md "wikilink")
  - [妹妹的情書](../Page/妹妹的情書.md "wikilink")
  - [沒寄出的那一封](../Page/沒寄出的那一封.md "wikilink")
  - [日出而息](../Page/日出而息.md "wikilink")
  - [妹妹妹妹](../Page/妹妹妹妹.md "wikilink")
  - [叱咤叱叱咤](../Page/叱咤叱叱咤.md "wikilink")

## 曾參與廣播劇

  - [地板對我說](../Page/地板對我說.md "wikilink")
    第一、二季　<span style="font-size:smaller;">飾</span>　Jill
  - [妹妹妹妹爆炸糖劇場](../Page/妹妹妹妹.md "wikilink")　<span style="font-size:smaller;">飾</span>　Cinderella
  - [黃金少年](../Page/黃金少年.md "wikilink")
      - Season 2　<span style="font-size:smaller;">飾</span>　陸欣
      - Season 3，4　<span style="font-size:smaller;">飾</span>　另一個陸欣
  - [戀愛樂園](../Page/戀愛樂園.md "wikilink")　<span style="font-size:smaller;">飾</span>　Phyllis
  - [來不及聽妳說愛我](../Page/來不及聽妳說愛我.md "wikilink")
  - [妹妹妹妹實況劇場](../Page/妹妹妹妹.md "wikilink")　<span style="font-size:smaller;">飾</span>　阿Wing
  - [森美幻想劇](../Page/森美幻想劇.md "wikilink")　<span style="font-size:smaller;">飾</span>　阿Wing
  - [最好的時光
    系列](../Page/最好的時光_\(廣播劇\).md "wikilink")　<span style="font-size:smaller;">飾</span>　余傲芝

## 曾參與之電視劇集

  - [赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")　<span style="font-size:smaller;">飾</span>　梅香華
  - [奇幻潮之收買佬](../Page/奇幻潮.md "wikilink")　<span style="font-size:smaller;">飾</span>　譚慧玲{Tammy}（羅敏莊妹妹）

## 曾參與之電視節目

  - [少年不識愁滋味](../Page/少年不識愁滋味.md "wikilink") - 成人禮
  - [醫學神探](../Page/醫學神探.md "wikilink") - 傷口勿浸水
  - [積金創未來](../Page/積金創未來.md "wikilink") - 魔法糕餅師的蛋糕

## 參與舞台劇

  - [拾香紀](../Page/拾香紀.md "wikilink")　<span style="font-size:smaller;">飾</span>　連拾香
  - 森美小儀歌劇團lokamochi mamiba　<span style="font-size:smaller;">飾</span>　管家
  - 賣火柴的女孩　<span style="font-size:smaller;">飾</span>　賣火柴的女孩
  - 小飛俠彼德潘　<span style="font-size:smaller;">飾</span>　Wendy
  - [森美金牌司儀 The Show Must Go
    Wrong](../Page/森美金牌司儀_The_Show_Must_Go_Wrong.md "wikilink")　<span style="font-size:smaller;">飾</span>　候選[香港小姐](../Page/香港小姐.md "wikilink")
  - [愈痛愈美麗](../Page/愈痛愈美麗.md "wikilink")（2008年10月）[1](http://www.trinitytheatre.org/lovelinessoftwinge/lovelinessoftwinge.html)　<span style="font-size:smaller;">飾</span>　[夢特嬌·全](../Page/夢特嬌·全.md "wikilink")
  - 愛麗絲夢醒時份　<span style="font-size:smaller;">飾</span>　愛麗絲
  - 二十出頭　<span style="font-size:smaller;">飾</span>　李綺玲

## 參與電影

  - [魔術男](../Page/魔術男.md "wikilink")(2007)　<span style="font-size:smaller;">飾</span>　Amy
  - [反斗狂奔](../Page/反斗狂奔.md "wikilink")（2006）
  - [AV](../Page/AV_\(電影\).md "wikilink")(2005)由[彭浩翔執導](../Page/彭浩翔.md "wikilink")。
  - [半醉人間](../Page/半醉人間.md "wikilink")（2005）
  - [甜美生活](../Page/甜美生活.md "wikilink")（2002）
  - [買兇拍人](../Page/買兇拍人.md "wikilink")（2001）　<span style="font-size:smaller;">飾</span>　雙鎗雄女友
  - [風的日夜](../Page/風的日夜.md "wikilink")（2000）[突破電影](../Page/突破電影.md "wikilink")　<span style="font-size:smaller;">飾</span>　Jola

## 參與音樂錄影帶

  - [陳曉東](../Page/陳曉東.md "wikilink") - 黑色領帶
  - [Bliss](../Page/Bliss.md "wikilink") - 失落奧斯卡
  - [周國賢](../Page/周國賢.md "wikilink") - 逃避
  - [古巨基](../Page/古巨基.md "wikilink") - 花灑

## 書籍作品

  -
  -
  -
  -
  -
## 參考文獻

<div class="references-small">

<references />

</div>

  - [推動「家人優先」，港親子企業准帶埋爸媽返工](https://hk.thenewslens.com/article/85093)
    2017年11月份《信報財經月刊》

## 外部連結

  -
  - [創辦的 kids and family agency](http://www.ohmykids.org/)

[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港素食主义者](../Category/香港素食主义者.md "wikilink")