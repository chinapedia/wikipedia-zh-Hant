**張賢智**（），[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")，目前效力於[桃園航空城棒球隊](../Page/桃園航空城棒球隊.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")，其胞兄為[統一7-ELEVEn獅隊投手](../Page/統一7-ELEVEn獅隊.md "wikilink")[徐余偉](../Page/徐余偉.md "wikilink")。

## 經歷

  - [花蓮縣光復國小少棒隊](../Page/花蓮縣.md "wikilink")
  - [花蓮縣光復國中青少棒隊](../Page/花蓮縣.md "wikilink")
  - [花蓮縣國光商工青棒隊](../Page/花蓮縣.md "wikilink")
  - [台北市強恕中學青棒隊](../Page/台北市.md "wikilink")
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - [誠泰COBRAS隊代訓球員](../Page/誠泰COBRAS.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[米迪亞暴龍隊](../Page/米迪亞暴龍.md "wikilink")
  - 桃園航空城棒球隊

## 職棒生涯成績

|       |                                            |    |    |    |    |    |     |     |     |      |     |       |      |
| ----- | ------------------------------------------ | -- | -- | -- | -- | -- | --- | --- | --- | ---- | --- | ----- | ---- |
| 年度    | 球隊                                         | 出賽 | 勝投 | 敗投 | 中繼 | 救援 | 四死  | 三振  | 被安打 | 被全壘打 | 責失  | 投球局數  | 防禦率  |
| 2006年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 34 | 12 | 10 | 0  | 3  | 45  | 96  | 171 | 14   | 60  | 165.2 | 3.26 |
| 2007年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 32 | 12 | 9  | 2  | 0  | 44  | 98  | 137 | 9    | 70  | 130.1 | 4.83 |
| 2008年 | [米迪亞暴龍](../Page/米迪亞暴龍.md "wikilink")       | 28 | 5  | 7  | 0  | 1  | 46  | 78  | 124 | 9    | 52  | 111.0 | 4.22 |
| 合計    | 3年                                         | 94 | 29 | 26 | 2  | 4  | 135 | 272 | 432 | 32   | 182 | 296.0 | 3.95 |

## 特殊事蹟

## 外部連結

[X](../Category/張姓.md "wikilink") [C](../Category/光復人.md "wikilink")
[C](../Category/阿美族人.md "wikilink")
[C](../Category/台灣棒球選手.md "wikilink")
[C](../Category/米迪亞暴龍隊球員.md "wikilink")
[C](../Category/誠泰COBRAS隊球員.md "wikilink")
[C](../Category/中華職棒單月最有價值球員獎得主.md "wikilink")