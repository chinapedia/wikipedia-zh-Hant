**張鳴岐**（），
[字](../Page/表字.md "wikilink")**坚白**，又作**健伯**，[斋号](../Page/斋号.md "wikilink")**韩斋**，中國[清朝](../Page/清朝.md "wikilink")[山东](../Page/山东.md "wikilink")[武定府](../Page/武定府_\(清朝\).md "wikilink")[海丰县人](../Page/无棣县.md "wikilink")，於1911年4月14日至11月8日间任[清朝末代](../Page/清朝.md "wikilink")[两广总督](../Page/两广总督.md "wikilink")。子[張銳](../Page/張銳.md "wikilink")、[張鎛](../Page/張鎛.md "wikilink")。

## 生平

张於[光绪二十年](../Page/光绪.md "wikilink")（1894年）中[举人](../Page/举人.md "wikilink")，后入[岑春煊之](../Page/岑春煊.md "wikilink")[幕府](../Page/幕府.md "wikilink")，受其器重。光绪三十年（1904年）随岑春煊入[广西镇压](../Page/广西.md "wikilink")[民变有功](../Page/民变.md "wikilink")，受广西[巡抚](../Page/巡抚.md "wikilink")[李经羲保举](../Page/李经羲.md "wikilink")，成为[太平思顺道](../Page/太平思顺道.md "wikilink")[道尹](../Page/道尹.md "wikilink")，次年又在岑春煊的保举下出任署理广西[布政使](../Page/布政使.md "wikilink")。光绪三十二年（1906年）年底，张又升任[广西布政使](../Page/广西布政使.md "wikilink")。光绪三十二年十一月十四，由广西布政使代，次年四月二十二（1907年6月2日），任[廣西巡撫](../Page/廣西巡撫.md "wikilink")\[1\]。宣统二年九月二十七（1910年10月29日），在[奕劻的支持下](../Page/奕劻.md "wikilink")，接替[袁树勋](../Page/袁树勋.md "wikilink")，[署理](../Page/署理.md "wikilink")[兩廣總督](../Page/兩廣總督.md "wikilink")，次年4月实授。1911年[辛亥革命](../Page/辛亥革命.md "wikilink")，曾被广东革命军立为[广东](../Page/广东.md "wikilink")[都督](../Page/都督.md "wikilink")，后逃到[日本](../Page/日本.md "wikilink")，由[胡汉民接替](../Page/胡汉民.md "wikilink")。

1912年[宣統退位后](../Page/宣統退位.md "wikilink")，出任[中華民國大總統](../Page/中華民國大總統.md "wikilink")[袁世凯高级顾问](../Page/袁世凯.md "wikilink")，1916年支持[袁世凯称帝](../Page/袁世凯称帝.md "wikilink")，被封为一等[伯爵](../Page/伯爵.md "wikilink")。[护国战争时隐居](../Page/护国战争.md "wikilink")[上海法租界](../Page/上海法租界.md "wikilink")，后又移居[天津意租界](../Page/天津意租界.md "wikilink")。[抗日战争期间与](../Page/抗日战争_\(中国\).md "wikilink")[日軍占领当局合作](../Page/日軍.md "wikilink")，任[华北政务委员会諮議委员](../Page/华北政务委员会.md "wikilink")。1945年8月15日[日本投降](../Page/日本投降.md "wikilink")，张於9月15日病死。

## 參考文獻

{{-}}

[Category:光緒二十年甲午科舉人](../Category/光緒二十年甲午科舉人.md "wikilink")
[Category:清朝两广总督](../Category/清朝两广总督.md "wikilink")
[Category:與大日本帝國合作的中國人](../Category/與大日本帝國合作的中國人.md "wikilink")
[Category:无棣人](../Category/无棣人.md "wikilink")
[M鸣岐](../Category/张姓.md "wikilink")

1.