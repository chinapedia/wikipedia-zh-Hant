**-{zh-tw:強納森;zh-cn:乔纳森;zh-hk:莊拿芬;}-·-{zh-cn:文森特;zh-tw:文森;zh-hk:雲遜;}-·“-{zh-cn:乔恩;
zh-hk:莊; zh-tw:強}-”·-{zh-cn:沃伊特; zh-hk:威;
zh-tw:沃特}-**（，），美國著名演技派演員，[動作片演員](../Page/動作片演員.md "wikilink")。多演反派角色，三次入圍提名美國[奧斯卡最佳男主角獎且得獎一次](../Page/奧斯卡最佳男主角獎.md "wikilink")；近年以《[不可能的任務](../Page/不可能的任務.md "wikilink")》裡狡滑的反派特工頭子再度贏得注視。其女兒[安潔莉娜·裘莉也為著名影星](../Page/安潔莉娜·裘莉.md "wikilink")。

強·沃特支持[共和黨](../Page/共和黨_\(美國\).md "wikilink")，曾表態支持[約翰·麥凱恩競選總統](../Page/約翰·麥凱恩.md "wikilink")。

## 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1969年</p></td>
<td><p>《<a href="../Page/午夜牛郎_(電影).md" title="wikilink">午夜牛郎</a>》</p></td>
<td><p>Joe Buck</p></td>
<td><p><a href="../Page/金球獎.md" title="wikilink">金球獎最佳男新人</a><br />
提名—<a href="../Page/奧斯卡最佳男主角獎.md" title="wikilink">奧斯卡最佳男主角獎</a><br />
提名—<a href="../Page/金球獎最佳戲劇類電影男主角.md" title="wikilink">金球獎最佳戲劇類電影男主角</a><br />
提名—<a href="../Page/英國影藝學院電影獎.md" title="wikilink">英國影藝學院電影獎英國本土傑出處女作</a></p></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td><p>《<a href="../Page/激流四勇士.md" title="wikilink">激流四勇士</a>》</p></td>
<td><p>Ed Gentry</p></td>
<td><p>提名—<a href="../Page/金球獎最佳戲劇類電影男主角.md" title="wikilink">金球獎最佳戲劇類電影男主角</a></p></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td><p>《<a href="../Page/歸返家園.md" title="wikilink">歸返家園</a>》</p></td>
<td><p>Luke Martin</p></td>
<td><p><a href="../Page/奧斯卡最佳男主角獎.md" title="wikilink">奧斯卡最佳男主角獎</a><br />
<a href="../Page/法國.md" title="wikilink">法國</a><a href="../Page/坎城影展.md" title="wikilink">坎城影展影帝</a></p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p>《<a href="../Page/滅.md" title="wikilink">滅</a>》</p></td>
<td><p>Oscar 'Manny' Manheim</p></td>
<td><p><a href="../Page/金球獎最佳戲劇類電影男主角.md" title="wikilink">金球獎最佳戲劇類電影男主角</a><br />
提名—<a href="../Page/奧斯卡最佳男主角獎.md" title="wikilink">奧斯卡最佳男主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p>《<a href="../Page/盜火線.md" title="wikilink">盜火線</a>》</p></td>
<td><p>內特</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/不可能的任務.md" title="wikilink">不可能的任務</a>》</p></td>
<td><p>反派特工頭子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/狂蟒之灾1.md" title="wikilink">大蟒蛇神出鬼沒</a>》</p></td>
<td><p>尋寶者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p>《<a href="../Page/全民公敵.md" title="wikilink">全民公敵</a>》</p></td>
<td><p>謀殺政敵的美國國家安全局主管</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/珍珠港.md" title="wikilink">珍珠港</a>》</p></td>
<td><p>美國總統 羅斯福</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/古墓丽影_(2001年电影).md" title="wikilink">古墓奇兵</a>》</p></td>
<td><p>女主角劳拉·克罗夫特（<a href="../Page/安吉丽娜·朱莉.md" title="wikilink">安潔莉娜·裘莉飾演</a>）的父親理查德·克罗夫特勋爵</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/威爾史密斯之叱吒風雲.md" title="wikilink">威爾史密斯之叱吒風雲</a>》</p></td>
<td><p>Howard Cosell</p></td>
<td><p>提名—<a href="../Page/奧斯卡最佳男配角獎.md" title="wikilink">奧斯卡最佳男配角獎</a><br />
提名—<a href="../Page/金球獎最佳電影男配角.md" title="wikilink">金球獎最佳電影男配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/別有洞天.md" title="wikilink">別有洞天</a>》</p></td>
<td><p>先生(Mr Sir)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/驚天奪寶.md" title="wikilink">國家寶藏</a>》</p></td>
<td><p>該片主角<a href="../Page/尼古拉斯·凯奇.md" title="wikilink">尼古拉斯·凯奇的父親Patrick</a> Henry Gates</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/光荣之路.md" title="wikilink">光荣之路</a>》</p></td>
<td><p>Adolph Rupp</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/九月黎明.md" title="wikilink">九月黎明</a>》</p></td>
<td><p>Jacob Samuelson</p></td>
<td><p>提名—<a href="../Page/金酸莓獎.md" title="wikilink">金酸莓獎最差男配角</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/變形金剛_(2007年電影).md" title="wikilink">變形金剛</a>》</p></td>
<td><p>美國國防部長</p></td>
<td><p>提名—<a href="../Page/金酸莓獎.md" title="wikilink">金酸莓獎最差男配角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/校園小天后.md" title="wikilink">校園小天后</a>》</p></td>
<td><p>Principal Dimly</p></td>
<td><p>提名—<a href="../Page/金酸莓獎.md" title="wikilink">金酸莓獎最差男配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/國家寶藏：古籍秘辛.md" title="wikilink">國家寶藏：古籍秘辛</a>》</p></td>
<td><p>該片主角<a href="../Page/尼古拉斯·凯奇.md" title="wikilink">尼古拉斯·凯奇的父親Patrick</a> Henry Gates</p></td>
<td><p>提名—<a href="../Page/金酸莓獎.md" title="wikilink">金酸莓獎最差男配角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/非法警戒.md" title="wikilink">非法警戒</a>》</p></td>
<td><p>Francis Tierney Sr.</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/大美國了沒?.md" title="wikilink">大美國了沒?</a>》</p></td>
<td><p><a href="../Page/喬治·華盛頓.md" title="wikilink">喬治·華盛頓</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/清道夫_(電視劇).md" title="wikilink">清道夫</a>》（電視劇）</p></td>
<td><p>米基·唐納文</p></td>
<td><p><a href="../Page/金球獎.md" title="wikilink">金球獎電視劇、迷你影集與電影電視最佳男配角</a><br />
提名—<a href="../Page/影評人票選電視獎.md" title="wikilink">影評人票選電視獎劇情類最佳男配角</a><br />
提名—<a href="../Page/衛星獎.md" title="wikilink">衛星獎電視劇、連續短劇與電視電影最佳男配角</a><br />
提名—<a href="../Page/黃金時段艾美獎.md" title="wikilink">黃金時段艾美獎劇情類最佳男配角</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/死路十條.md" title="wikilink">死路十條</a>》</p></td>
<td><p>神祕的聲音</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/黑暗王子德古拉.md" title="wikilink">黑暗王子德古拉</a>》</p></td>
<td><p>Leonardo Van Helsing</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/橄欖球傳奇.md" title="wikilink">橄欖球傳奇</a>》</p></td>
<td><p>教練Paul "Bear" Bryant</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>《<a href="../Page/怪獸與牠們的產地_(電影).md" title="wikilink">怪獸與牠們的產地</a>》</p></td>
<td><p>報業大亨Henry Shaw,Sr.</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 扩展阅读

  -
## 外部連結

  -
  -
  -
  -
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:斯洛伐克裔美國人](../Category/斯洛伐克裔美國人.md "wikilink")
[Category:英国电影学院奖得主](../Category/英国电影学院奖得主.md "wikilink")
[Category:奧斯卡最佳男主角獎獲獎演員](../Category/奧斯卡最佳男主角獎獲獎演員.md "wikilink")
[Category:金球獎最佳電影男主角獲得者](../Category/金球獎最佳電影男主角獲得者.md "wikilink")
[Category:金球獎最佳電視男配角獲得者](../Category/金球獎最佳電視男配角獲得者.md "wikilink")
[Category:美國天主教大學校友](../Category/美國天主教大學校友.md "wikilink")
[Category:纽约州共和党人](../Category/纽约州共和党人.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")