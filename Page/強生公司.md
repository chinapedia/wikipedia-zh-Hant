[JohnsonJohnson_HQ_building.jpg](https://zh.wikipedia.org/wiki/File:JohnsonJohnson_HQ_building.jpg "fig:JohnsonJohnson_HQ_building.jpg")[新布朗斯維克的總部大樓](../Page/新布朗斯維克.md "wikilink")。\]\]
[HKGrand_Centary_Place.jpg](https://zh.wikipedia.org/wiki/File:HKGrand_Centary_Place.jpg "fig:HKGrand_Centary_Place.jpg")的分公司設於[旺角](../Page/旺角.md "wikilink")[新世紀廣場辦公大樓和炮台山友邦廣場](../Page/新世紀廣場.md "wikilink")\]\]
 **強生公司**（**Johnson &
Johnson**）是[美國一家醫療保健產品](../Page/美國.md "wikilink")、醫療器材及藥廠的製造商，全球總部位於美國紐澤西州的新布朗斯維克。强生集團由全球超過250家的子公司組成，其產品銷售遍及170多個國家。

## 历史

強生成立於1886年，是[道瓊斯工業指數的成份股之一](../Page/道瓊斯工業指數.md "wikilink")，也是[財富500強的一員](../Page/財富500強.md "wikilink")。強生向來以擁有良好的名譽著稱，並在2008年霸榮雜誌的調查中獲選為最受尊崇的公司。然而，其聲譽近年來受到[產品召回](../Page/產品召回.md "wikilink")、藥物罰金及訴訟等因素影響而略有下滑。

強生公司是在1970至72年進入大中華市場，該公司在[香港曾使用](../Page/香港.md "wikilink")「**莊-{}-生**」作為中文名，在1995年年底改為「**強-{}-生**」，在中國亦使用「強-{}-生」作為官方名稱。在[台灣則稱為](../Page/台灣.md "wikilink")「**嬌-{}-生**」，在东南亚依则为「**莊生」**，並分成四大事業體：消費品（嬌-{}-生）、視力保健產品（嬌-{}-生安適優）、醫療器材（壯-{}-生）及藥廠（楊森）。

強生集團最廣為人知的為其醫療保健用品。集團旗下擁有多個品牌，包括：Tylenol[泰諾止痛錠](../Page/泰諾.md "wikilink")、Johnson's
Baby[强生婴儿](../Page/强生婴儿.md "wikilink")、Neutrogena[露得清](../Page/露得清.md "wikilink")、Clean
&
Clear[可伶可俐](../Page/可伶可俐.md "wikilink")、Regaine[落健](../Page/落健.md "wikilink")、Salvon[沙威隆](../Page/沙威隆.md "wikilink")、隱形眼鏡ACUVUE[安適優](../Page/安適優.md "wikilink")、OneTouch穩豪血糖機等。

2006年6月27日強生以166億美元（約1294.8億港元）現金，收購[輝瑞](../Page/輝瑞.md "wikilink")（
Pfizer）的個人護理用品業務。

2008年，強生為[北京奧運會的合作](../Page/2008年夏季奧林匹克運動會.md "wikilink")-{伙}-伴。

2012年6月12日強生以每股55.65法郎的现金，和價值103.35法郎的强生股票，斥資210億美元收購瑞士醫療設備生產商Synthes
Inc，Synthes將與強生旗下的DePuy公司合併成DePuy Synthes。

2009年3月，美國[安全化妆品运动组织的一份报告指出强生等公司的婴儿产品含有致癌物质](../Page/安全化妆品运动组织.md "wikilink")[甲醛和](../Page/甲醛.md "wikilink")[二恶烷](../Page/二恶烷.md "wikilink")，但在之後的檢驗中被證明沒有問題。

強生已於2012年12月與上海噯呵母嬰用品國際貿易有限公司簽訂合同並報商務部審批，以6.5億元收購噯呵100%股權。

2013年11月6日美國司法部宣布，美國強生公司將支付超過22億美元（約171億港元）罰款，了結對其違法銷售藥品和向醫生及藥商提供[回扣的刑事及民事指控](../Page/回扣.md "wikilink")。今次是美國醫療保健行業歷來第三大罰款，強生強調賠錢只是為免官司纏繞，對政府的指控不完全認同。

2016年9月17日，強生以43.25億美元現金收購[雅培眼部護理業務Abbott](../Page/雅培.md "wikilink")
Medical Optics。

2017年1月27日，強生以每股280美元現金，斥資300億美元收購歐洲最大生物製藥公司，瑞士[Actelion](../Page/Actelion.md "wikilink")
所有流通股份，其中Actelion的藥物開發和臨床研發業務將會分折成一家獨立公司R＆D
NewCo，在瑞士上市，強生持有新上市公司16%股權，并将通過可轉換票據獲得額外的16％的股權。

2017年2月15日美國Integra LifeSciences以現金10.5億美元（81.9億港元）收購強生旗下Codman
Neurosurgery業務分支，Codman主要是生產神經外科手術用與術後設備。

2017年7月24日，强生宣布称利用[马赛克疫苗制成的HIV疫苗临床试验中](../Page/马赛克疫苗.md "wikilink")，志愿者100%产生抗体。\[1\]

2018年10月24日，強生以每股收購價5900日圓，斥資2300億日圓（20.5億美元）現金收購日本化妝品企業Ci:z
Holdings所有在外流通股票，目前強生是Ci:z的第二大股東，通過其附屬公司持有19.9%股權

## 争议

2018年7月12日，美国[密苏里州法院判决](../Page/密蘇里州.md "wikilink")，强生必须对22位妇女使用该公司爽身粉而罹患[卵巢癌的原告](../Page/卵巢癌.md "wikilink")，支付创纪录的46.9亿美元赔偿金。\[2\]

2018年12月14日，[路透社披露强生蓄意隐藏旗下婴儿爽身粉产品潜藏致癌物石绵达](../Page/路透社.md "wikilink")47年，强生股价在当日惨跌10.04%，一日之间蒸发逾400亿美元，创下16年来最大单日跌幅。强生则强烈反驳路透社的报道，并宣称报导是一种"荒谬的阴谋论"。\[3\]

2019年2月14日，強生以34億美元現金收購外科手術機器人公司Auris Health

## 参考来源

## 外部連結

  - [美國強生](http://www.jnj.com)
  - [台灣嬌生（壯生醫療器材）](http://www.jjmt.com.tw/)
  - [嬌生嬰兒（台灣）](https://www.johnsonsbaby.com.tw/)
  - [中國強生](http://www.jnj.com.cn)
  - [香港強生](https://web.archive.org/web/20170112080854/https://www.jnj.com.hk/)
  - [強生中国医疗器材有限公司](http://www.jjmc.com.cn)
  - [西安杨森制药有限公司](http://www.xian-janssen.com.cn)

[强生公司](../Category/强生公司.md "wikilink")
[J](../Category/纽约证券交易所上市公司.md "wikilink")
[J](../Category/道瓊斯工業平均指數成份股.md "wikilink")
[J](../Category/標準普爾500指數成分股.md "wikilink")
[J](../Category/財富500強公司.md "wikilink")
[J](../Category/美國化學工業公司.md "wikilink")
[J](../Category/美国制药公司.md "wikilink")
[J](../Category/1886年成立的公司.md "wikilink")
[J](../Category/光學製造公司.md "wikilink")

1.
2.
3.