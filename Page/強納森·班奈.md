**強納森·班奈**（，）是一位[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")。

強納森·班奈出生於美国[俄亥俄州](../Page/俄亥俄州.md "wikilink")[托莱多](../Page/托莱多.md "wikilink")（Toledo）。他第一次演出是在他小学7年級，當時他住在[北卡羅來納州](../Page/北卡羅來納州.md "wikilink")。在那之後沒多久，他搬去了俄亥俄州[罗斯佛德](../Page/罗斯佛德.md "wikilink")（Rossford），並就讀於[罗斯佛德高中](../Page/罗斯佛德高中.md "wikilink")（Rossford
High
School）。他畢業於1999年，之後他繼續深造於[奥特本大学](../Page/奥特本大学.md "wikilink")（Otterbein
College），他的主修是 Theatre
Program。在他離開學院之後，他般去了紐約並繼續演戲。強納森班奈的銀幕處女作是獨立製片《Season
of
Youth》，他並以該片贏得2003年[棕櫚灘國際影展最佳男](../Page/棕櫚灘國際影展.md "wikilink")[主角獎](../Page/主角.md "wikilink")。

## 演出作品

### 影視

  - 《[法網遊龍](../Page/法網遊龍.md "wikilink")》（Law and Order: Special Victims
    Unit），在第4季第2集 "Deception" 中 (2002)
  - 《[年輕季節](../Page/年輕季節.md "wikilink")》（Season of Youth） (2003)
  - 《[辣妹過招](../Page/辣妹過招.md "wikilink")》（Mean Girls） (2004)
  - 《[辣妹校探](../Page/辣妹校探.md "wikilink")》（Veronica Mars） (两集，2004-2005)
  - 《[超人前傳](../Page/超人前傳.md "wikilink")》（Smallville）(2005)，在第85集 "Blank"
    中饰 Kevin Grady（第4季）
  - 《[24 笑](../Page/24_笑.md "wikilink")》（Cheaper by the Dozen 2）
    (2005)，饰 Bud McNulty
  - 《[賭城單身派對](../Page/賭城單身派對.md "wikilink")》（Bachelor Party Vegas）
    (2005)
  - 《[荒島尤物](../Page/荒島尤物.md "wikilink")》（Lovewrecked） (2006)
  - 《[The Dukes of Hazzard: The
    Beginning](../Page/The_Dukes_of_Hazzard:_The_Beginning.md "wikilink")》
    (2007) (V)

### 音樂錄影帶

  - 《Thank U, Next》－[亞莉安娜·格蘭德](../Page/亞莉安娜·格蘭德.md "wikilink")

## 外部連結

  -
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:奥特本大学校友](../Category/奥特本大学校友.md "wikilink")
[Category:俄亥俄州人](../Category/俄亥俄州人.md "wikilink")
[Category:美國男同性戀者](../Category/美國男同性戀者.md "wikilink")
[Category:男同性戀演員](../Category/男同性戀演員.md "wikilink")