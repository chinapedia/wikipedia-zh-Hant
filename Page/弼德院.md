**弼德院**，[中國](../Page/中國.md "wikilink")[清朝中央機構之一](../Page/清朝.md "wikilink")，創設於1911年5月8日（[宣統三年四月初十](../Page/宣統.md "wikilink")），是清朝末年為實行憲政而成立的“上備顧問，參議國務”的專職參議機構。為[皇帝親臨顧問國務的地方](../Page/皇帝.md "wikilink")，院內置院長、副院長各1人，顧問大臣32人，參議10人，秘書廳秘書長1人，初設院長為[陸潤庠](../Page/陸潤庠.md "wikilink")，[榮慶為副院長](../Page/榮慶.md "wikilink")。

## 职官

以下如无说明，为[宣統三年四月初十](../Page/宣統.md "wikilink")（1911年5月8日）任命：

  - 院长：[陸潤庠](../Page/陸潤庠.md "wikilink")（六月十五日（1911年7月10日）解职）、[榮慶](../Page/榮慶.md "wikilink")（六月十五日（1911年7月10日）任）、[奕劻](../Page/奕劻.md "wikilink")（九月十一日（1911年11月1日）任）
  - 副院长：[榮慶](../Page/榮慶.md "wikilink")（六月十五日（1911年7月10日）升院长）、[邹嘉来](../Page/邹嘉来.md "wikilink")（六月十五日（1911年7月10日）任）

以下为闰六月廿日（1911年8月14日）任命的[庆亲王内阁弼德院成员](../Page/庆亲王内阁.md "wikilink")：

  - 秘书长：[田智枚](../Page/田智枚.md "wikilink")
  - 顾问大臣：[贝子载振](../Page/载振.md "wikilink")、[陆润庠](../Page/陆润庠.md "wikilink")、[增祺](../Page/增祺.md "wikilink")、[陈宝琛](../Page/陈宝琛.md "wikilink")、[丁振铎](../Page/丁振铎.md "wikilink")、[姚锡光](../Page/姚锡光.md "wikilink")、[沈云沛](../Page/沈云沛.md "wikilink")、[誠勳](../Page/誠勳.md "wikilink")、[清锐](../Page/清锐.md "wikilink")、[朱祖谋](../Page/朱祖谋.md "wikilink")、[礼亲王世铎](../Page/世铎.md "wikilink")（[宗人府令兼任](../Page/宗人府令.md "wikilink")）、[奎俊](../Page/奎俊.md "wikilink")（[内务府大臣兼任](../Page/内务府大臣.md "wikilink")）、[继禄](../Page/继禄.md "wikilink")（内务府大臣兼任）；全体[国务大臣均兼充](../Page/国务大臣.md "wikilink")（[国务大臣见](../Page/国务大臣.md "wikilink")[庆亲王内阁](../Page/庆亲王内阁.md "wikilink")）。
  - 参议：[景瑗](../Page/景瑗.md "wikilink")、[施愚](../Page/施愚.md "wikilink")、[陈云诰](../Page/陈云诰.md "wikilink")、[恩华](../Page/恩华.md "wikilink")、[陶葆廉](../Page/陶葆廉.md "wikilink")、[张一麐](../Page/张一麐.md "wikilink")、[吴廷燮](../Page/吴廷燮.md "wikilink")（[法制院参议兼任](../Page/法制院.md "wikilink")）、[陈懋鼎](../Page/陈懋鼎.md "wikilink")（外务部右参议兼任）、[林灏深](../Page/林灏深.md "wikilink")（学部左参议兼任）、[诚璋](../Page/诚璋.md "wikilink")（农工商部左参议兼任）

以下为九月十一日（1911年11月1日）任命的[袁世凯内阁弼德院成员](../Page/袁世凯内阁.md "wikilink")：

  - 院长：[慶親王奕劻](../Page/奕劻.md "wikilink")
  - 顾问大臣：[那桐](../Page/那桐.md "wikilink")、[徐世昌](../Page/徐世昌.md "wikilink")、[荣庆](../Page/荣庆.md "wikilink")

以下为九月廿六日（1911年11月16日）任命的[袁世凯内阁弼德院成员](../Page/袁世凯内阁.md "wikilink")：

  - 顾问大臣：[绍昌](../Page/绍昌.md "wikilink")、[林绍年](../Page/林绍年.md "wikilink")、[陈邦瑞](../Page/陈邦瑞.md "wikilink")、[王垿](../Page/王垿.md "wikilink")、[吴郁生](../Page/吴郁生.md "wikilink")、[恩顺](../Page/恩顺.md "wikilink")（十二月廿日（1912年2月7日）死）\[1\]

## 参考文献

### 引用

### 来源

  - [趙爾巽等](../Page/趙爾巽.md "wikilink")
    撰：[清史稿](../Page/清史稿.md "wikilink")，[中華書局](../Page/中華書局.md "wikilink").

## 参见

  - [清朝中央國家機關列表](../Page/清朝中央國家機關列表.md "wikilink")

{{-}}

[弼德院](../Category/弼德院.md "wikilink")
[Category:1911年建立政府機構](../Category/1911年建立政府機構.md "wikilink")

1.  錢實甫編，[清代職官年表](../Page/清代職官年表.md "wikilink")，北京：中华书局，1980年，p.3094.