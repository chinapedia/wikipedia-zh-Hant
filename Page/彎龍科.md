**彎龍科**（Camptosauridae）是[鳥臀目](../Page/鳥臀目.md "wikilink")[鳥腳下目](../Page/鳥腳下目.md "wikilink")[禽龍類的一科](../Page/禽龍類.md "wikilink")，生存於[侏儸紀晚期到](../Page/侏儸紀.md "wikilink")[白堊紀早期](../Page/白堊紀.md "wikilink")，分布於現今的[北美洲與](../Page/北美洲.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。

## 敘述

相較於早期的[稜齒龍類](../Page/稜齒龍類.md "wikilink")，彎龍科的體型較大，行動較不靈活。頭部長而平坦，牙齒成葉狀，緊密排列，嘴部前部缺乏牙齒，具有喙嘴。彎龍科的前肢短而結實，後肢強壯。中間三跟手指與四個腳趾呈蹄狀。由化石足跡來判斷，牠們除了以四肢來步行外，亦能夠以雙足步行。

## 分類

彎龍科最早是由[奧塞內爾·查利斯·馬什](../Page/奧塞內爾·查利斯·馬什.md "wikilink")（Othniel Charles
Marsh）在1885年提出。根據目前的定義，包含[全異彎龍](../Page/彎龍.md "wikilink")，但不包含[沃克氏副櫛龍在內的最大演化支](../Page/副櫛龍.md "wikilink")。

## 外部連結

  - [彎龍科的定義歷史](https://web.archive.org/web/20080405234824/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=68)
    Taxon Search
  - [彎龍科的簡介](http://www.dinoruss.com/de_4/5c3da39.htm)

[\*](../Category/禽龍類.md "wikilink")