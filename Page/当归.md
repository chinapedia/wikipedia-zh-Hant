**当归**（[学名](../Page/学名.md "wikilink")：），属[伞形科的一种植物](../Page/伞形科.md "wikilink")。一般作为药用。

## 形态

多年生[草本植物](../Page/草本.md "wikilink")，高0.4～1米。茎直立，有纵直槽纹，无毛。二或三回三出式羽状复叶，小叶卵形，浅裂或有缺刻。开白色花，复伞状花序，顶生。矩圆形双悬果，侧棱有宽翅，边缘为淡紫色。花期6～7月，果期7～8月。\[1\]

## 分布

在中国分布于[甘肃](../Page/甘肃.md "wikilink")、[云南](../Page/云南.md "wikilink")、[四川](../Page/四川.md "wikilink")、[青海](../Page/青海.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[贵州等地](../Page/贵州.md "wikilink")，各地均有栽培，以甘肅[岷縣出產的最佳](../Page/岷縣.md "wikilink")\[2\]。当归的根可入药，是最常用的[中药之一](../Page/中药.md "wikilink")。

## 异名

当归一名首见于《[本经](../Page/本经.md "wikilink")》，又名薜、山蕲、白蕲（《[尔雅](../Page/尔雅.md "wikilink")》），文无（[崔豹](../Page/崔豹.md "wikilink")《[古今注](../Page/古今注.md "wikilink")》）。\[3\]

## 药用

当归的根可入药。一般需培育3年才可采收。药材为干燥的根，可分为3部：根头称“**归头**”，主根为“**归身**”，支根及根梢为“**归尾**”。岷县当归、[东当归](../Page/东当归.md "wikilink")、[粉绿当归和](../Page/粉绿当归.md "wikilink")[欧当归在部分地区也作当归入药](../Page/欧当归.md "wikilink")。\[4\]

### 成分

根部含[挥發油](../Page/挥發油.md "wikilink")，挥發油的主要成分有[亚丁基苯酞](../Page/亚丁基苯酞.md "wikilink")、[邻羧基苯正戊酮](../Page/邻羧基苯正戊酮.md "wikilink")、[二氢酞酐以及](../Page/二氢酞酐.md "wikilink")[维生素B<sub>12</sub>](../Page/钴胺素.md "wikilink")、[蔗糖](../Page/蔗糖.md "wikilink")、[维生素A](../Page/维生素A.md "wikilink")、[棕榈酸](../Page/棕榈酸.md "wikilink")、[硬脂酸](../Page/硬脂酸.md "wikilink")、[肉豆蔻酸](../Page/肉豆蔻酸.md "wikilink")、[不饱和油酸](../Page/不饱和油酸.md "wikilink")、[亚油酸](../Page/亚油酸.md "wikilink")、[β-谷甾醇等](../Page/β-谷甾醇.md "wikilink")。\[5\]

### 炮制方法

当归的[炮制方法是除去杂质](../Page/炮制.md "wikilink")，洗净，润透，切薄片，晒干或低温干燥。

[當歸酒的炮制方法是取净当归片](../Page/當歸酒.md "wikilink")，照酒炙法炒干。为类圆形或不规则薄片，切面有浅棕色环纹，质柔韧，深黄色，略有焦斑。味甘、微苦，香气浓厚，有酒香气。

### 性味归经

甘、辛，温。归肝、心、脾经。

### 功能与主治

补血活血，调经止痛，润肠通便。

用于血虚萎黄、眩晕心悸、[月经不调](../Page/月经不调.md "wikilink")、经闭痛经、虚寒腹痛、肠燥[便秘](../Page/便秘.md "wikilink")、风湿痹痛、跌扑损伤、痈疽疮疡。酒当归活血通经，用于经闭痛经、风湿痹痛、跌扑损伤。

应用于[方剂](../Page/方剂.md "wikilink")[四物汤](../Page/四物汤.md "wikilink")、[补中益气汤](../Page/补中益气汤.md "wikilink")、[当归芍药散](../Page/当归芍药散.md "wikilink")、[当归苦参丸](../Page/当归苦参丸.md "wikilink")、[当归补血汤](../Page/当归补血汤.md "wikilink")、[当归六黄汤等](../Page/当归六黄汤.md "wikilink")。

可作為溫和的鎮靜劑、緩瀉劑、利尿劑、抗痙攣劑和減輕疼痛、改善血液、強化生殖系統，幫助身體利用賀爾蒙。用來治療婦女疾病，如：熱潮紅和其他更年期的症狀、經前症候群及陰道乾燥。

### 毒副作用

《[神农本草经](../Page/神农本草经.md "wikilink")》等中医学典籍把当归列为无毒的上品药。当归含有[雌激素活性成分](../Page/雌激素.md "wikilink")，体外实验表明，当归能显著地刺激[乳腺](../Page/乳腺.md "wikilink")[癌细胞的增殖](../Page/癌细胞.md "wikilink")，因此[乳腺癌患者应避免用当归](../Page/乳腺癌.md "wikilink")“进补”。有报道称存在当归使男子乳房肥大，以及会使哺乳期妇女服用当归后母子均出现[高血压的案例](../Page/高血压.md "wikilink")。当归能与[抗凝血剂发生反应](../Page/抗凝血剂.md "wikilink")，增加出血的风险。\[6\]

## 历史典故

  - 太史当归：[曹操曾修书一封给](../Page/曹操.md "wikilink")[太史慈](../Page/太史慈.md "wikilink")，内中并无一字，只有一味中药，名为“当归”，告诉太史慈應當歸來。动之以情，曹操求贤之心溢然于上。

## 由來傳說

花蓮在地生產的新鮮當歸話說「當歸」的由來，民間有很多則傳說，其中有則很感人的故事，相傳在很久以前，有對十分恩愛的夫妻，過著快樂幸福的日子，但妻子不幸罹患重病，多年來各處求醫均無效，丈夫發誓要治好妻子的病，便親自到人跡罕至的深山裏採藥，臨行對妻子說，若經過三年未返，一定是死於他鄉，你便可以改嫁他人。時光飛逝，三年匆匆流過，丈夫果然未回家，妻子因生活所迫，只得改嫁他人。但世事難料，改嫁不久，前夫竟採得藥草歸來，妻子深覺愧對前夫，便服下前夫送來的草藥，意欲自盡謝罪，結果反而把病治好。後來，人們就把該草藥，取名為「當歸」。李時珍在《本草綱目》中，寫道：「當歸本非芹類，特以花葉似芹故得芹名，古人娶妻為嗣續也，當歸調血為女人要藥，有恩夫之意，故有當歸之名」。

## 参考文献

<div class="references-small">

<references />

  - [当归的药典标准](http://www.zeeming.com/wiki/当归的药典标准)

</div>

## 外部連結

  - [當歸
    Danggui](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00117)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [當歸
    Danggui](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00049)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [當歸 Dang
    Gui](http://libproject.hkbu.edu.hk/was40/outline?page=1&channelid=44273&searchword=%E7%95%B6%E6%AD%B8&sortfield=+name_chi_sort)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [全歸 Quan
    Gui](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00305)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [槁本內酯
    Z-ligustilide](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00426)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)
  - 張珣：〈[文化建構性別、身體與食物：以當歸為例](http://homepage.ntu.edu.tw/~anthro/download/journal/67-5.pdf)〉

[多草](../Category/藥用植物.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:可食用伞形科](../Category/可食用伞形科.md "wikilink")

1.
2.  [神農百草遊](../Page/神農百草遊.md "wikilink")，2017-01-08

3.  [江苏新医学院](../Page/江苏新医学院.md "wikilink").
    [中药大辞典](../Page/中药大辞典.md "wikilink").
    [上海科学技术出版社](../Page/上海科学技术出版社.md "wikilink").
    1986.5. ISBN 7-5323-0842-1

4.
5.
6.  [常用中成药的真相——当归养血丸](http://www.xys.org/xys/netters/Fang-Zhouzi/zhongyi/dangguiyangxuewan.txt)[方舟子](../Page/方舟子.md "wikilink")，[新语丝](../Page/新语丝.md "wikilink")，2008-12-27