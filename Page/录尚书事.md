**錄尚書事**是中國[東漢](../Page/東漢.md "wikilink")[章帝時開始設置的職位](../Page/漢章帝.md "wikilink")。不過早在[西漢](../Page/西漢.md "wikilink")[昭帝時期就已出現](../Page/漢昭帝.md "wikilink")**領尚書事**，[昭帝初繼位](../Page/漢昭帝.md "wikilink")，大將軍[霍光秉政](../Page/霍光.md "wikilink")，領尚書事
\[1\]。此處的領尚書事和錄尚書事同義。它不是一個獨立職位，需附加其他朝廷重要官職之下。如195年（[東漢獻帝興平](../Page/漢獻帝.md "wikilink")2年），[曹操官拜鎮東將軍](../Page/曹操.md "wikilink")、錄尚書事\[2\]，221年（[蜀漢建安](../Page/蜀漢.md "wikilink")26年、章武元年）蜀漢[諸葛亮官拜丞相](../Page/諸葛亮.md "wikilink")、錄尚書事\[3\]等。至東漢末年時，錄尚書事已是掌握實權重臣的必要條件。

## 發展

[西漢武帝時](../Page/漢武帝.md "wikilink")，[尚書開始經手天下章奏](../Page/尚書_\(官職\).md "wikilink")，歷經曲折，到[東漢光武帝時尚書業務已增加至分設六曹](../Page/漢光武帝.md "wikilink")，設尚書臺。東漢時，大臣中位階最高的[三公權力已受到尚書臺的嚴重影響](../Page/三公.md "wikilink")，僅剩執行政務的權力，而決策的空間大大受限（關於尚書與尚書臺歷史詳見條目[尚書
(官職)](../Page/尚書_\(官職\).md "wikilink")）。75年章帝即位時首度任命錄尚書事一職予重臣[牟融與](../Page/牟融.md "wikilink")[趙憙](../Page/趙憙.md "wikilink")。兼錄尚書事的重臣可以經手尚書臺業務，且地位高於尚書臺最高長官[尚書令](../Page/尚書令.md "wikilink")，等於將政務執行與決策的權力重新集於一身。東漢自[汉章帝之后多为幼帝临朝](../Page/汉章帝.md "wikilink")，常任命重臣為[太傅兼錄尚書事](../Page/太傅.md "wikilink")，如[和帝時的](../Page/漢和帝.md "wikilink")[鄧彪](../Page/鄧彪.md "wikilink")、[殤帝時的](../Page/漢殤帝.md "wikilink")[張禹](../Page/張禹_\(東漢\).md "wikilink")。朝廷重臣若要完全掌握行政實權，亦必須兼領此職。例如曹操安插重要屬下[荀彧為尚書令](../Page/荀彧.md "wikilink")，自己為錄尚書事；[劉備託孤時以尚書令](../Page/劉備.md "wikilink")[李嚴為錄尚書事的諸葛亮副手](../Page/李严_\(蜀汉\).md "wikilink")；蜀漢[蔣琬](../Page/蔣琬.md "wikilink")、[費禕作為政治最高決策官員時](../Page/費禕.md "wikilink")，均以尚書令兼錄尚書事。此後至[南北朝均有錄尚書事任命之記載](../Page/南北朝.md "wikilink")。\[4\]

## 知名擔任者

  - 東漢獻帝時（192年），董卓被刺殺後，司徒[王允兼錄尚書事](../Page/王允.md "wikilink")。
  - 東漢獻帝時（195年），曹操迎接獻帝後，獻帝封其為鎮東將軍、費亭侯，稍後封[假節鉞](../Page/符節.md "wikilink")、錄尚書事。
  - 劉備稱帝時（221年），諸葛亮受封丞相、錄尚書事、[假節](../Page/符節.md "wikilink")。
  - [西晉武帝時](../Page/晉武帝.md "wikilink")（27?年），[賈充封太尉](../Page/賈充.md "wikilink")、行太子太保、錄尚書事。
  - [司馬睿](../Page/司馬睿.md "wikilink")317年稱晉王，實際掌權者[王導拜右將軍](../Page/王導.md "wikilink")、揚州刺史、監江南諸軍事，遷驃騎將軍，加散騎常侍、都督中外諸軍、領中書監、錄尚書事、假節。
  - [東晉安帝時](../Page/晉安帝.md "wikilink")（402年），[桓玄掌握都城](../Page/桓玄.md "wikilink")，矯詔加封自己為總百揆、侍中、都督中外諸軍事、丞相、錄尚書事、揚州牧、領徐州刺史、假黃鉞、羽葆鼓吹、班劍二十人，置左右長史、司馬、從事中郎四人，甲杖二百人上殿。
  - 桓玄稱帝後，405年[劉裕擊敗桓軍](../Page/劉裕.md "wikilink")，迎回晉安帝。408年，經過多次推辭，劉裕接受錄尚書事任命，同時受封侍中、車騎將軍、開府儀同三司、揚州刺史。
  - [南朝齊時](../Page/南齊.md "wikilink")（501年12月），[蕭衍佔領首都健康](../Page/蕭衍.md "wikilink")，受中書監、都督揚南徐二州諸軍事、大司馬、錄尚書、驃騎大將軍、揚州刺史，封建安郡公，食邑萬戶，給班劍四十人。次年2月封梁公，3月封梁王，又過1個月接受齊帝禪位。
  - [南朝梁時](../Page/梁_\(南朝\).md "wikilink")（556年4月），[陳霸先擊退北齊進犯軍隊](../Page/陳霸先.md "wikilink")，8月受封丞相、錄尚書事、鎮纫大將軍，改（揚州）刺史為牧，進封義興郡公。第二年9月為陳公，10月封陳王，3日後接受梁帝禪位。\[5\]

<!-- end list -->

  - 曹魏

[陈群](../Page/陈群_\(三国\).md "wikilink")、[曹爽](../Page/曹爽.md "wikilink")、[司马懿](../Page/司马懿.md "wikilink")、[司马师](../Page/司马师.md "wikilink")、[司马昭](../Page/司马昭.md "wikilink")

  - 蜀汉

[诸葛亮](../Page/诸葛亮.md "wikilink")、[蔣琬](../Page/蔣琬.md "wikilink")、[費禕](../Page/費禕.md "wikilink")、[姜维](../Page/姜维.md "wikilink")

  - 东吴

[刘基](../Page/刘基.md "wikilink")、[滕胤](../Page/滕胤.md "wikilink")、[全尚](../Page/全尚.md "wikilink")、[濮阳兴](../Page/濮阳兴.md "wikilink")、[纪亮](../Page/纪亮.md "wikilink")、[滕牧](../Page/滕牧.md "wikilink")、[虞昺](../Page/虞昺.md "wikilink")、[李仁](../Page/李仁_\(三国\).md "wikilink")、[华融](../Page/华融.md "wikilink")

  - 西晋

[王沈](../Page/王沈_\(西晋政治人物\).md "wikilink")、[贾充](../Page/贾充.md "wikilink")、[司马亮](../Page/司马亮.md "wikilink")、[杨骏](../Page/杨骏.md "wikilink")、[司马泰](../Page/司马泰.md "wikilink")、[司马柬](../Page/司马柬.md "wikilink")、[卫瓘](../Page/卫瓘.md "wikilink")、[司马肜](../Page/司马肜.md "wikilink")、[王浑](../Page/王浑.md "wikilink")、[陈准](../Page/陈准.md "wikilink")、[司马颖](../Page/司马颖.md "wikilink")、[张方](../Page/张方.md "wikilink")、[司马越](../Page/司马越.md "wikilink")、[麴允](../Page/麴允.md "wikilink")、[索綝](../Page/索綝.md "wikilink")

  - 东晋

[王导](../Page/王导.md "wikilink")、[司马羕](../Page/司马羕.md "wikilink")、[荀组](../Page/荀组.md "wikilink")、[陆晔](../Page/陆晔.md "wikilink")、[荀崧](../Page/荀崧.md "wikilink")、[庾冰](../Page/庾冰.md "wikilink")、[何充](../Page/何充.md "wikilink")、[司马昱](../Page/司马昱.md "wikilink")、[蔡谟](../Page/蔡谟.md "wikilink")、[桓温](../Page/桓温.md "wikilink")、[谢安](../Page/谢安.md "wikilink")、[司马道子](../Page/司马道子.md "wikilink")、[司马元显](../Page/司马元显.md "wikilink")、[桓谦](../Page/桓谦.md "wikilink")、[王谧](../Page/王谧.md "wikilink")、[刘裕](../Page/刘裕.md "wikilink")

## 相關條目

  - [領尚書事](../Page/領尚書事.md "wikilink")
  - [省尚書事](../Page/省尚書事.md "wikilink")
  - [視尚書事](../Page/視尚書事.md "wikilink")
  - [平尚書事](../Page/平尚書事.md "wikilink")

## 注釋

<references group="註" />

## 參考資料

[\*](../Category/录尚书事.md "wikilink")
[Category:中国古代中央官制](../Category/中国古代中央官制.md "wikilink")

1.  [漢書·昭帝紀](../Page/s:漢書/卷07.md "wikilink")
2.  [三國志·魏書·武帝紀](../Page/s:三國志/卷01.md "wikilink")
3.  [三國志·蜀書·諸葛亮傳](../Page/s:三國志/卷35.md "wikilink")
4.  後漢書·[卷4](../Page/s:後漢書/卷4.md "wikilink")、[卷26](../Page/s:後漢書/卷26.md "wikilink")
5.  [太平御覽·職官部八](../Page/s:太平御覽/0210.md "wikilink")、後漢書、三國志、晉書、宋書、梁書、陳書