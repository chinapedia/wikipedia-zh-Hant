**形意拳**，又称**行意拳**、**心意拳**、**心意六合拳**，中国传统武术，与[太极拳](../Page/太极拳.md "wikilink")、[八卦掌](../Page/八卦掌.md "wikilink")、通臂拳齐名，同属[内家拳之中](../Page/内家拳.md "wikilink")，流行於中國北方，分成河北、山西、河南三個系統。打法多直行直进，与八卦之横走、太极之中定有显著之差别。

## 基本介绍

形意拳是中国三大著名内家拳拳种之一（形意、太极、八卦），與[少林拳位列中国四大名拳](../Page/少林拳.md "wikilink")。\[1\]

形意拳发源于山西太谷，山西形拳讲究功力，形鬆意紧，外形不拘一格，打法变幻多端的风格特点。在形意十二形中，重点突出所取动物的进攻技巧，不求形象但求意真的练法，五行中讲究金、木、水、火、土的内涵。\[2\]

## 拳术起源

形意拳尊[岳武穆为始祖](../Page/岳武穆.md "wikilink")，其起源可以追溯到清初山西[姬际可](../Page/姬际可.md "wikilink")。据说姬际可於[終南山得到岳武穆拳谱](../Page/終南山.md "wikilink")，精通六合枪法，後把大枪术化为拳法，创出此拳。姬际可门下，分成河南、山西、河北等不同派系，分化成不同的名字传承，包括心意六合拳、心意拳、形意拳等。\[3\]把心意拳授與少林寺武僧，少林寺稱之為“心意把”，成為該寺傳留最精拳術之一。

现代盛行的形意拳，定型於河北深州[李洛能](../Page/李洛能.md "wikilink")。他至山西，學習戴氏心意拳，之後發展出這門拳術，並定名為形意拳。其門徒眾多，主要分成山西、河北二個系統。李洛能之徒[郭雲深](../Page/郭雲深.md "wikilink")，以崩拳聞名，改良了五行拳的架子，創半步崩拳。

## 拳术特点

形意拳具有以下特点：

1.  简洁朴实，其动作大多直来直往，一屈一伸，节奏鲜明，朴实无华，富于自然之美。
2.  动作严密紧凑，"出手如钢锉，落手如钩竿”，“两肘不离肋，两手不离心”。发拳时，拧裹钻翻，与身法、步法紧密相合，周身上下好像拧绳一样，毫不松懈。
3.  沉着稳健，身正，步稳，“迈步如行犁，落脚如生根”，要求宽胸实腹，气沉丹田，刚而不僵，柔而不软，劲力舒展沉实。
4.  快速完整，形意拳要求“六合”，即心与意合，意与气合，气与力合，肩与胯合，肘与膝合，手与足合。动作强调上法上身，手脚齐到，一发即至，一寸为先。

拳谚有“起如风，落如箭，打倒还嫌慢”之说。形意拳讲究“三节”、“八要”。三节是：“梢节起，中节随，根节催”。从全身讲，头与上肢为梢节，
形意拳躯干为中节，下肢为根节；上肢以手为梢节，肘为中节，肩为根节；下肢则分为胯、膝、足三节。做到三节的要求，就能保证周身完整一体，内外合一。八要是：三顶（头上顶，有冲天之雄；手外顶，有推山之功；舌上顶，有吞象之容）、三扣（肩扣，则力气到肘；膝胯扣，则全身气凑；手足指掌扣，则周身力厚）、三圆（胸要圆、背要圆、虎口要圆）、三敏（心要敏，眼要敏，手要敏）、三抱（丹田抱，心意抱，两肋抱）、三垂（肩下垂，肘下垂，气下垂）、三曲（臂要曲，腿要曲，腕要曲）、三挺（颈要挺，脊要挺，膝要挺）。这样，就可保证身体各部姿势正确舒展。形意拳包含着丰富的技击理论和技术、战术内容，强调敢打必胜、勇往直前的战斗意识。拳谚说：“遇敌有主，临危不惧”。在战术思想上，主张快速突然，以我为主，交手时先发制人，“乘其无备而攻之，出其不意而击之”，“有意莫带形，带形必不赢”。在攻防技术上，提倡近打快攻，抢占有利位置，“眼要毒，心要奸，脚踏中门裆里钻”，“进即闪，闪即进，不必远求”。形意拳主张头、肩、肘、手、胯、膝、脚七法并用，处
形意拳处可发，“远了便上手，近了便加肘；远了用脚踢，近了便加膝”，并且要求虚实结合，知己知人，相机而行，不可拘使成法，做到“拳无拳，意无意，无意之中是真意”，方算上乘功夫。形意拳的技击理论有6项原则，即工（巧妙）、顺（自然）、勇（果断）、疾（快速、突然）、狠（不容情）、真（使敌难于逃脱），称为"六方之妙"。这6项原则对培养攻防意识，训练技击技术具有指导作用。

形意拳注重力量的训练。第1步功夫是“筑其基，壮其体，使骨体坚如铁石”，为技术提高打下良好基础，这称为“明劲功夫”。第2步要练“暗劲和化劲功夫”，要求周身完整，刚柔相济，精神贯注，形神合一，以意导体，以气发力。可见，形意拳对人体各项生理功能要求是相当高的。形意拳动作中正不倚，打法可刚可柔，不同体质的人都可从事锻炼，近年来医疗体育方面也已采用。

## 流派演化

明末清初姬际可创形意拳，传给山西曹继武：

曹继武传马学礼與李政 马学礼所传的河南形意拳一支，今称心意六合拳，一般只在回民中流传。

李政传戴龙邦及
之两名儿子文龙及文熊兄弟。戴隆邦傳戴隆邦之妻侄郭维汉。今称戴氏心意拳（尊曹继武为祖）。戴龙邦又传贾大俊、吕海根、河北人李洛能等少数外姓弟子，其中李洛能广收门徒，由此迎来形意拳在近代的大发展：
这一时期其中代表人物是李洛能的八大弟子：山西的车永宏、宋世荣、宋世德、李广亨四人；河北的郭云深、刘奇兰、刘晓兰、贺运亨；这八大弟子各有所长，北方形意拳逐步衍化为具备地方特色的山西形意拳，又称山西小架，与河北形意拳，又称河北大架。郭云深，创半步崩拳，将形意拳五行拳的
孙禄堂，师从李奎元，并得郭云深亲授，兼学程派八卦掌、武氏太极拳，会武学之大成，创孙氏太极拳。刘奇兰，传李存义，在李存义手上，河北形意拳逐渐壮大，成为形意拳的主流。李存义，传尚云祥，开创尚氏形意拳。传薛颠，创象形拳。此外，还有现代少林寺之心意把（类形意拳）以及四川梁平县一带流传之金家功。

形意八卦拳
相传郭云深与[八卦掌始祖董海川曾经论武相交](../Page/八卦掌.md "wikilink")，认为八卦掌与形意拳在拳理上有互补之功，不禁止门下相互学习，两门之间以兄弟相称，故习形意拳者多同时学习八卦掌，称为形意八卦拳。但考证历史，两派的交流，应该开始于李存义与程廷华，两人均在北京天津一带教拳，两人交情又好，所以河北形意拳门下多同时学习八卦掌，著名的代表人物为孙禄堂、尚云祥、程有龙，程有龙又传于何广、马德山等人，但是他们尚谨守门派之别。真正以形意八卦拳为名授徒，则是开始于河北张占魁一派。意拳
王芗斋，自称为郭云深弟子，自创意拳（晚年又改名为大成拳）。其功法虽脱胎于形意拳门下，但多加入王芗斋本人的心得。王芗斋本人认为此拳为拳技之大成，故名大成拳。自认不列名于形意门下，故许多形意拳门人也不认为此拳为形意拳之分支。五法八象（象形术）

薛颠自称受到灵空禅师传象形术，但有人考证象形术的拳理、心法与形意拳无异。象形术应该是薛颠以形意拳拳理为基础，结合个人体会，加上华佗五禽戏等内功而产生，可视为形意拳的支派。

此外，還有現代少林寺之[心意把](../Page/心意把.md "wikilink")（類形意拳）以及[四川](../Page/四川.md "wikilink")[梁平縣一帶流傳之](../Page/梁平.md "wikilink")[金家功](../Page/金家功.md "wikilink")。

## 拳譜文獻

  - [六合拳](../Page/六合拳.md "wikilink")
  - [岳武穆王拳經](../Page/岳武穆王拳經.md "wikilink")：內蛓有《[五行拳](../Page/五行拳.md "wikilink")》（與福建[白鶴拳之入門樁功五行拳僅同名](../Page/白鶴拳.md "wikilink")，沒有傳承上的關係）、《[連環拳](../Page/連環拳.md "wikilink")》、《十二形》（大勢）等。懷疑為**山西戴氏心意拳**後人托古作品。
  - [守洞塵技](../Page/守洞塵技.md "wikilink")
  - [解說形意拳經](../Page/解說形意拳經.md "wikilink")
  - [形意拳學](../Page/形意拳學.md "wikilink")
  - [形意拳術訣微](../Page/形意拳術訣微.md "wikilink")
  - [形意拳術講義](../Page/形意拳術講義.md "wikilink")
  - [形意拳秘法](../Page/形意拳秘法.md "wikilink")
  - [象形拳法真詮](../Page/象形拳法真詮.md "wikilink")

## 各派形意拳內容

**河南心意六合拳**，（馬學禮所傳，分為洛陽派、南陽派），單勢操練原始形式。保存原始之六合十形，加上當地查拳四拳八式。

  - 基本樁：龍吊膀
  - 單練及套路：單把、雙把、三把、四把（合稱四把拳）、踩式推手、古樹擺枝、迎風貫耳、鷹兒撲食、龍折身、虎抱頭、十形、四拳八式
  - 步法：雞形步、馬形步、螺旋步

**山西戴氏心意拳**（尊曹繼武為祖，創編五行拳，重道功中醫跌打術）

  - 基本樁：蹲猴勢
  - 單練及套路：**主拳**（[交際四把](../Page/交際四把.md "wikilink")、[六合四把](../Page/六合四把.md "wikilink")），**會意拳**（[五行拳](../Page/五行拳.md "wikilink")、[三拳](../Page/三拳.md "wikilink")、[七炮](../Page/七炮.md "wikilink")、[五膀](../Page/五膀.md "wikilink")），**象形拳**（[十大形](../Page/十大形.md "wikilink")、[七小形](../Page/七小形.md "wikilink")），**單練套路**（[螳螂閘勢](../Page/螳螂閘勢.md "wikilink")/閘勢捶、[聯珠把](../Page/聯珠把.md "wikilink")、[連環拳](../Page/連環拳.md "wikilink")）
  - 步法：[雞形步](../Page/雞形步.md "wikilink")
  - 對練：[九套環](../Page/九套環.md "wikilink")、[五行炮](../Page/五行炮.md "wikilink")、[安身炮](../Page/安身炮.md "wikilink")、[五花炮](../Page/五花炮.md "wikilink")

**河北形意拳**、**山西形意拳**（兩派均由李洛能所傳，重單勢操練及樁功）

  - 基本樁：三體式（又稱三才式、鷹捉式）
  - 步法：[雞形步](../Page/雞形步.md "wikilink")、[槐蟲步](../Page/槐蟲步.md "wikilink")
  - 單練及套路：五行拳、十二形、五行連環拳、鸡形四把、鹞形八式、十二洪捶、雜式捶、拾手藝拳、八字功拳、金剛八勢拳、拳出入洞拳、九套環拳、形意十七勢、盤根、形意散手等
  - 器械：五行剑、三才剑、纯阳剑、龙形剑、五行刀、五行棍、九州棍、五行枪、连环枪等
  - 對練：三花炮、五行生剋对练、八式对练、洪捶对练、安身炮对练、九套環、十六把、三才剑对练等

**賈大俊傳形意拳**

  - 賈大俊傳的形意拳主要承傳了戴氏所傳的“悟心拳”和“會意拳”部分，根據《形意拳秘法》圖片所示，賈氏所傳的五行拳中，劈、崩二拳與戴氏心意拳相同；而鑽、炮、橫就比較接近李洛能所傳的鑽、炮、橫三拳，其餘**十二形象**（十二形）及其他所練的功、技、法、式與戴氏原傳“心意拳”相同。該派傳與趙萬遠後，又增加了“九九八十一勁”練養功法及七腿、七膝、七肘等技法和三才六合七進等對練套路。

**五法八象（象形術）**

  - 起式：提水式（来源请求）
  - 內功：正身法、調息法、修心法
  - 單練：**五法**（飛、雲、搖、晃、旋）、**八象**（龍、虎、馬、牛、象、獅、熊、猿）
  - 套路：五法連珠（五法合一）、八象合演
  - 器械：除了靈空禪師所傳**佛門方便鏟**外，其餘的器械套路演示均與形意拳的器械無異。

## 五行拳

戴氏心意拳及形意拳之基礎動作。亦稱“母拳”。以單式動作為主。

根據1929年由[海上尚武進德會出版](../Page/海上尚武進德會.md "wikilink")，**姜容樵**編著的『形意母拳』一書的介紹說：所謂母拳者，即**五行拳**。相生相尅連環也。此為各形之基礎，故謂母拳。

**李洛能**與**郭雲深**時，僅五行連環十二形。

## 形意拳門下代表人物

<span style="text-decoration: underline;">始祖</span>
\*[姬際可](../Page/姬際可.md "wikilink")

<span style="text-decoration: underline;">[河南心意拳名人](../Page/河南.md "wikilink")</span>

  - [馬學禮](../Page/馬學禮.md "wikilink")
  - [馬三元](../Page/馬三元.md "wikilink")
  - [張志誠](../Page/張志誠.md "wikilink")
  - [張聚](../Page/張聚.md "wikilink")
  - [張海洲](../Page/張海洲.md "wikilink")
  - [李政](../Page/李禎.md "wikilink")
  - [吳呼連](../Page/吳呼連.md "wikilink")
  - [劉化鳥](../Page/劉化鳥.md "wikilink")
  - [唐大用](../Page/唐大用.md "wikilink")
  - [唐萬儀](../Page/唐萬儀.md "wikilink")
  - [買壯圖](../Page/買壯圖.md "wikilink")
  - [安大慶](../Page/安大慶.md "wikilink")
  - [袁鳯儀](../Page/袁鳯儀.md "wikilink")
  - [水觀蘭](../Page/水觀蘭.md "wikilink")
  - [水騰龍](../Page/水騰龍.md "wikilink")
  - [尚學禮](../Page/尚學禮.md "wikilink")
  - [楊殿卿](../Page/楊殿卿.md "wikilink")
  - [卢嵩高](../Page/卢嵩高.md "wikilink")
  - [宋國賓](../Page/宋國賓.md "wikilink")
  - [寶顯庭](../Page/寶顯庭.md "wikilink")
  - [解興邦](../Page/解興邦.md "wikilink")
  - [孫少甫](../Page/孫少甫.md "wikilink")

<span style="text-decoration: underline;">[山西戴氏心意拳名人](../Page/山西.md "wikilink")</span>
\*[曹繼武](../Page/曹繼武.md "wikilink")

  - [戴龍邦](../Page/戴龍邦.md "wikilink")
  - [戴大閭](../Page/戴大閭.md "wikilink")
  - [戴文熊](../Page/戴二閭.md "wikilink")（戴二閭）
  - [戴花奴](../Page/戴花奴.md "wikilink")
  - [戴良楝](../Page/戴良楝.md "wikilink")
  - [戴奎](../Page/戴奎.md "wikilink")
  - [高升禎](../Page/高升禎.md "wikilink")
  - [岳蘊忠](../Page/岳蘊忠.md "wikilink")
  - [馬二牛](../Page/馬二牛.md "wikilink")
  - [段鍚福](../Page/段鍚福.md "wikilink")
  - [段仙](../Page/段仙.md "wikilink")

<span style="text-decoration: underline;">[河北及](../Page/河北.md "wikilink")[山西形意拳名人](../Page/山西.md "wikilink")</span>

  - [李洛能](../Page/李洛能.md "wikilink")
  - [張樹德](../Page/張樹德.md "wikilink")
  - [劉奇蘭](../Page/劉奇蘭.md "wikilink")
  - [王福元](../Page/王福元.md "wikilink")
  - [王继武](../Page/王继武.md "wikilink")
  - [张宝杨](../Page/张宝杨.md "wikilink")
  - [車永宏](../Page/車永宏.md "wikilink")
  - [宋世榮](../Page/宋世榮.md "wikilink")
  - [宋世德](../Page/宋世德.md "wikilink")
  - [白西園](../Page/白西園.md "wikilink")
  - [李廣亨](../Page/李廣亨.md "wikilink")
  - [賀運亨](../Page/賀運亨.md "wikilink")
  - [劉曉蘭](../Page/劉曉蘭.md "wikilink")
  - [李太和](../Page/李太和.md "wikilink")
  - [郭雲深](../Page/郭雲深.md "wikilink")
  - [李存義](../Page/李存義_\(清朝\).md "wikilink")
  - [張占魁](../Page/張占魁.md "wikilink")
  - [孫福全](../Page/孫福全.md "wikilink")（即[孫祿堂](../Page/孫祿堂.md "wikilink")）
  - [尚雲祥](../Page/尚雲祥.md "wikilink")
  - [傅劍秋](../Page/傅劍秋.md "wikilink")
  - [郝恩光](../Page/郝恩光.md "wikilink")
  - [唐維祿](../Page/唐維祿.md "wikilink")
  - [薛顛](../Page/薛顛.md "wikilink")
  - [姜容樵](../Page/姜容樵.md "wikilink")
  - [宋虎臣](../Page/宋虎臣.md "wikilink")
  - [宋鐵麟](../Page/宋鐵麟.md "wikilink")
  - [李復禎](../Page/李復禎.md "wikilink")
  - [布學寬](../Page/布學寬.md "wikilink")
  - [劉儉](../Page/劉儉.md "wikilink")
  - [董秀升](../Page/董秀升.md "wikilink")
  - [王薌齋](../Page/王薌齋.md "wikilink")
  - [李仲軒](../Page/李仲軒.md "wikilink")
  - [李東園 (東垣)](../Page/李東園_\(東垣\).md "wikilink")

<span style="text-decoration: underline;">其它、待考</span>

  - [南山鄭氏](../Page/南山鄭氏.md "wikilink")
  - [郭維漢](../Page/郭維漢.md "wikilink")
  - [賈大俊](../Page/賈大俊.md "wikilink")
  - [金一旺](../Page/金一旺.md "wikilink")
  - [麻貴廷](../Page/麻貴廷.md "wikilink")

## 传承

  - 第一代
    [姬際可](../Page/姬際可.md "wikilink")：[明](../Page/明.md "wikilink")[万历三十年](../Page/万历.md "wikilink")（1602年）－[清](../Page/清.md "wikilink")[康熙二十二年](../Page/康熙.md "wikilink")（1683年）。字龍峰（一说隆豐，隆凤）。山西蒲州人。传人有曹继武，马学礼。
  - 第二代
    [曹繼武](../Page/曹繼武.md "wikilink")：[清](../Page/清.md "wikilink")[康熙四年](../Page/康熙.md "wikilink")（1655年）－？。名曰瑋，字继武。[安徽](../Page/安徽.md "wikilink")[贵池人](../Page/贵池.md "wikilink")。十余岁随姬際可学艺。后中武举状元，官至陝西靖遠總鎮都督。传人有[戴龍邦](../Page/戴龍邦.md "wikilink")。
    [馬學禮](../Page/馬學禮.md "wikilink")：回族，河南洛阳人。曾伪装成哑巴混入姬際可家偷学武艺，被姬发现后，嘉其志，授之以拳。马亦以曹继武为师。学成后回乡，在回民中创河南心意拳一脉。传人有[张志诚](../Page/张志诚.md "wikilink")，[马兴](../Page/马兴.md "wikilink")，[马三元等](../Page/马三元.md "wikilink")。
  - 第三代
    [戴龍邦](../Page/戴龍邦.md "wikilink")：[清](../Page/清.md "wikilink")[康熙五十二年](../Page/康熙.md "wikilink")（1713年）－[清](../Page/清.md "wikilink")[嘉庆七年](../Page/嘉庆.md "wikilink")（1802年）。[山西](../Page/山西.md "wikilink")[祁县人](../Page/祁县.md "wikilink")。[曹继武弟子](../Page/曹继武.md "wikilink")。创山西祁县戴氏心意拳一脉。传次子[戴二闾](../Page/戴二闾.md "wikilink")。
    [马三元](../Page/马三元.md "wikilink")：生卒年不详，回族。马学礼之侄。河南形意拳第二代传人。
    [马兴](../Page/马兴.md "wikilink")：河南心意拳第二代传人。
    [张志诚](../Page/张志诚.md "wikilink")：河南心意拳第二代传人。
  - 第四代
    [戴二闾](../Page/戴二闾.md "wikilink")：约[清](../Page/清.md "wikilink")[乾隆三十八年](../Page/乾隆.md "wikilink")（1773年）－[清](../Page/清.md "wikilink")[同治二年](../Page/同治.md "wikilink")（1863年）。本名文熊，字义熊。山西祁县人。戴龙邦次子。山西祁县戴氏形意拳第二代传人。传侄[戴良栋](../Page/戴良栋.md "wikilink")，[李洛能](../Page/李洛能.md "wikilink")。
  - 第五代
    [戴良栋](../Page/戴良栋.md "wikilink")：[清](../Page/清.md "wikilink")[道光四年](../Page/道光.md "wikilink")（1824年）－[民國四年](../Page/民國.md "wikilink")（1915年）。山西祁县人。[戴二闾侄](../Page/戴二闾.md "wikilink")。山西祁县戴氏形意拳第三代传人。
    [李洛能](../Page/李洛能.md "wikilink")：[清](../Page/清.md "wikilink")[嘉庆十三年](../Page/嘉庆.md "wikilink")（1808年）－[清](../Page/清.md "wikilink")[光绪十六年](../Page/光绪.md "wikilink")（1890年）。名飞羽，字能然。[河北](../Page/河北.md "wikilink")[深县人](../Page/深县.md "wikilink")。拜师戴二闾。后在心意六合拳基础上创形意拳。传人众多，知名者有车永宏、宋世荣、宋世德、李广亨，郭云深、刘奇兰、刘晓兰、贺运恒八人。
  - 第六代
    [车永宏](../Page/车永宏.md "wikilink")：[清](../Page/清.md "wikilink")[道光十三年](../Page/道光.md "wikilink")（1833年）－[民國三年](../Page/民國.md "wikilink")（1914年）。字毅斋。[山西](../Page/山西.md "wikilink")[太谷人](../Page/太谷.md "wikilink")。李洛能首徒。[咸丰六年](../Page/咸丰.md "wikilink")（1856年）拜师。创车氏形意一脉。传人众多，知名者有[李复贞](../Page/李复贞.md "wikilink")、[李发春](../Page/李发春.md "wikilink")、[白光普](../Page/白光普.md "wikilink")、[王凤翙](../Page/王凤翙.md "wikilink")、[武杰](../Page/武杰.md "wikilink")、[郭琨](../Page/郭琨.md "wikilink")、[樊永庆](../Page/樊永庆.md "wikilink")、[孟兴德](../Page/孟兴德.md "wikilink")、[布学宽](../Page/布学宽.md "wikilink")、[刘俭](../Page/刘俭.md "wikilink")，子[车兆俊](../Page/车兆俊.md "wikilink")、[车兆杰](../Page/车兆杰.md "wikilink")、[车兆烈等](../Page/车兆烈.md "wikilink")。
    [宋世荣](../Page/宋世荣.md "wikilink")：[清](../Page/清.md "wikilink")[道光二十九年](../Page/道光.md "wikilink")（1849年）－[民國十六年](../Page/民國.md "wikilink")（1927年）。字约斋。[北京](../Page/北京.md "wikilink")[大兴人](../Page/大兴.md "wikilink")。李洛能高徒。创宋氏形意一脉。传人有[贾蕴高](../Page/贾蕴高.md "wikilink")、[任尔祺](../Page/任尔祺.md "wikilink")、[王嗣昌](../Page/王嗣昌.md "wikilink")、[赵守钰](../Page/赵守钰.md "wikilink")、子[宋虎臣](../Page/宋虎臣.md "wikilink")、[宋青山](../Page/宋青山.md "wikilink")，侄[宋晏彪](../Page/宋晏彪.md "wikilink")、[宋铁麟等](../Page/宋铁麟.md "wikilink")。
    [宋世德](../Page/宋世德.md "wikilink")：[清](../Page/清.md "wikilink")[咸丰七年](../Page/咸丰.md "wikilink")（1857年）－[民國十年](../Page/民國.md "wikilink")（1921年）。字辅仁。北京大兴人。宋世荣弟，李洛能高徒。无传人。
    [李广亨](../Page/李广亨.md "wikilink")：[清](../Page/清.md "wikilink")[咸丰九年](../Page/咸丰.md "wikilink")（1859年）－[民國二十三年](../Page/民國.md "wikilink")（1934年）。[山西](../Page/山西.md "wikilink")[榆次人](../Page/榆次.md "wikilink")。李洛能高徒。传人有[牛利](../Page/牛利.md "wikilink")、[白解和](../Page/白解和.md "wikilink")、[郭崇善](../Page/郭崇善.md "wikilink")、[孟振武](../Page/孟振武.md "wikilink")、[田仰武](../Page/田仰武.md "wikilink")、[宁玉璋](../Page/宁玉璋.md "wikilink")、[李润清等](../Page/李润清.md "wikilink")。
    [贺运亨](../Page/贺运亨.md "wikilink")：生卒不详。李洛能高徒。传人不多。
    [刘晓兰](../Page/刘晓兰.md "wikilink")：[清](../Page/清.md "wikilink")[嘉庆二十四年](../Page/嘉庆.md "wikilink")（1819年）－[清](../Page/清.md "wikilink")[宣统元年](../Page/宣统.md "wikilink")（1909年）。[直隶](../Page/直隶.md "wikilink")[河间府人](../Page/河间府.md "wikilink")。李洛能高徒。河北形意拳主要传人。传人有[石振发](../Page/石振发.md "wikilink")、[刘维山](../Page/刘维山.md "wikilink")、[杨扶山](../Page/杨扶山.md "wikilink")、[刘轮山](../Page/刘轮山.md "wikilink")、[朱春芳](../Page/朱春芳.md "wikilink")、[李云山](../Page/李云山.md "wikilink")、[南云标](../Page/南云标.md "wikilink")、[王老者](../Page/王老者.md "wikilink")、[孙宝和](../Page/孙宝和.md "wikilink")、[张聚川](../Page/张聚川.md "wikilink")、[陈凤高等](../Page/陈凤高.md "wikilink")。
    [刘奇兰](../Page/刘奇兰.md "wikilink")：[清](../Page/清.md "wikilink")[嘉庆二十四年](../Page/嘉庆.md "wikilink")（1819年）－[清](../Page/清.md "wikilink")[光绪十五年](../Page/光绪.md "wikilink")（1889年）。[河北](../Page/河北.md "wikilink")[深县人](../Page/深县.md "wikilink")。李洛能高徒。传人有[李存义](../Page/李存義_\(清朝\).md "wikilink")、[张占魁](../Page/张占魁.md "wikilink")、[耿继善](../Page/耿继善.md "wikilink")、[周明泰](../Page/周明泰.md "wikilink")、[田静杰](../Page/田静杰.md "wikilink")、[刘凤春](../Page/刘凤春.md "wikilink")、[刘德宽](../Page/刘德宽.md "wikilink")，[王福元](../Page/王福元.md "wikilink")、子[刘文华等](../Page/刘文华.md "wikilink")。
    [郭云深](../Page/郭云深.md "wikilink")：[清](../Page/清.md "wikilink")[道光九年](../Page/道光.md "wikilink")（1829年）－[清](../Page/清.md "wikilink")[光绪二十六年](../Page/光绪.md "wikilink")（1900年）。字峪深。[河北](../Page/河北.md "wikilink")[深州人](../Page/深州.md "wikilink")。李洛能高徒。传人有[李奎元](../Page/李奎元.md "wikilink")、[钱砚堂](../Page/钱砚堂.md "wikilink")（[钱锡采子](../Page/钱锡采.md "wikilink")）、子[郭深](../Page/郭深.md "wikilink")、子[郭圆](../Page/郭圆.md "wikilink")、[许占鳌](../Page/许占鳌.md "wikilink")、[魏老率](../Page/魏老率.md "wikilink")、[李振山等](../Page/李振山.md "wikilink")。
  - 第七代
    [李存义](../Page/李存義_\(清朝\).md "wikilink")：[清](../Page/清.md "wikilink")[道光二十七年](../Page/道光.md "wikilink")（1847年）－[民國十年](../Page/民國.md "wikilink")（1921年）。原名存毅，字肃堂。後改名存義，字忠元。[河北](../Page/河北.md "wikilink")[深州人](../Page/深州.md "wikilink")。刘奇兰高徒。创中华武士会。弟子众多，知名者有黄柏年、郝恩光、尚云祥、马玉堂、李星阶、傅剑秋。陈泮岭，田鸿业，薛颠等。
    [张占魁](../Page/张占魁.md "wikilink")：[清](../Page/清.md "wikilink")[同治四年](../Page/同治.md "wikilink")（1865年）－[民國二十七年](../Page/民國.md "wikilink")（1938年）。字兆東，[河北](../Page/河北.md "wikilink")[河間人](../Page/河間.md "wikilink")。刘奇兰高徒。精通形意拳和八卦掌。创形意八卦掌。弟子极多且杂，知名者有韓慕俠、王俊臣、劉晉卿、裘稚和、李劍秋、趙道新、姜容樵、錢樹樵、張雨亭等。
  - 第八代
    [韩慕侠](../Page/韩慕侠.md "wikilink")：[清](../Page/清.md "wikilink")[光绪三年](../Page/光绪.md "wikilink")（1877年）－1969年。原名韩金镛。河北天津蘆北口人。张占魁徒。曾授周恩来武艺。
    [趙道新](../Page/趙道新.md "wikilink")：[清](../Page/清.md "wikilink")[光绪三十三年](../Page/光绪.md "wikilink")（1907年）－1990年。天津人。张占魁徒。
    [钱树樵](../Page/钱树樵.md "wikilink")：1894年－1972年。[河北](../Page/河北.md "wikilink")[河间人](../Page/河间.md "wikilink")。张占魁徒。弟子有张仁甫、宋庭顺、耿继义、潘荣弟、熊守年、黄万祥等。
    [姜容樵](../Page/姜容樵.md "wikilink")：1891年－1974年。字光武。[河北](../Page/河北.md "wikilink")[沧州人](../Page/沧州.md "wikilink")。从张占魁习形意拳，八卦掌。一生弟子，著作颇丰。
    [黄柏年](../Page/黄柏年.md "wikilink")：1880年－1954年。字介梓。[河北](../Page/河北.md "wikilink")[任邱人](../Page/任邱.md "wikilink")。

## 參見

  - [心意六合拳](../Page/心意六合拳.md "wikilink")

## 參考文獻

## 外部連結

  - [定格：山西形意拳](http://www.3jrx.com/staticFiles/198/2010-06-14/20100614125525.shtml)，三晋热线
  - [晉中形意拳](http://www.youtube.com/watch?v=4WOt7wXJEXU&feature=related)

[\*](../Category/形意拳.md "wikilink")
[X](../Category/中国非物质文化遗产.md "wikilink")
[Category:中國傳統武術](../Category/中國傳統武術.md "wikilink")

1.

2.
3.