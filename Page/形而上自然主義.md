**形而上自然主義**，[哲學學派之一](../Page/哲學.md "wikilink")，認為除了被自然科学研究的与我们所理解的物质世界相关的物、力、因以外凡是理論上不可能用自然[科學去理解的東西都不存在](../Page/科學.md "wikilink")。形而上自然主義认为所有与意识、心灵相关的概念都是指那些可被还原为、或依附于自然物、力、因的实体。而且，它否认任何超自然的物、力、因，比如诸种宗教中出现的事物、以及所有形式的[目的论](../Page/目的论.md "wikilink")。它认为所有“超自然”的物均能由纯粹自然的原因来解释。它不只是对现有科学的反映，亦能展望科学在未来能够发现什么。对于现实，形而上自然主义持一元论而不是二元论的态度。

## 深入阅读

### 支持

  - [Gary Drescher](../Page/Gary_Drescher.md "wikilink"), *Good and
    Real*, The MIT Press, 2006. \[ISBN 0-262-04233-9\]
  - [David Malet
    Armstrong](../Page/David_Malet_Armstrong.md "wikilink"), *A World of
    States of Affairs*, Cambridge: Cambridge University Press, 1997.
    \[ISBN 0-521-58064-1\]
  - [Mario Bunge](../Page/Mario_Bunge.md "wikilink"), 2006, *Chasing
    Reality: Strife over Realism*, University of Toronto Press. ISBN
    0-8020-9075-3 and 2001, *Scientific Realism: Selected Essays of
    Mario Bunge*, Prometheus Books. ISBN 1-57392-892-5
  - [Richard Carrier](../Page/Richard_Carrier.md "wikilink"), 2005,
    [Sense and Goodness without God: A Defense of Metaphysical
    Naturalism](../Page/Sense_and_Goodness_without_God:_A_Defense_of_Metaphysical_Naturalism.md "wikilink"),
    AuthorHouse. ISBN 1-4208-0293-3
  - Mario De Caro & David Macarthur (eds), 2004. *Naturalism in
    Question*. Cambridge, Mass: Harvard University Press. ISBN
    0-674-01295-X
  - [Daniel Dennett](../Page/Daniel_Dennett.md "wikilink"), 2003,
    *[Freedom Evolves](../Page/Freedom_Evolves.md "wikilink")*, Penguin.
    ISBN 0-14-200384-0 and 2006
  - [Andrew Melnyk](../Page/Andrew_Melnyk.md "wikilink"), 2003, *A
    Physicalist Manifesto: Thoroughly Modern Materialism*, Cambridge
    University Press. ISBN 0-521-82711-6
  - [David Mills](../Page/David_Mills_\(author\).md "wikilink"), 2004,
    *Atheist Universe: Why God Didn't Have A Thing To Do With It*,
    Xlibris. ISBN 1-4134-3481-9
  - [Jeffrey Poland](../Page/Jeffrey_Poland.md "wikilink"), 1994,
    *Physicalism: The Philosophical Foundations*, Oxford University
    Press. ISBN 0-19-824980-2

### 反对

  - [James Beilby](../Page/James_Beilby.md "wikilink"), ed., 2002,
    *Naturalism Defeated? Essays on Plantinga's Evolutionary Argument
    Against Naturalism*, Cornell University Press. ISBN 0-8014-8763-3
  - [William Lane Craig](../Page/William_Lane_Craig.md "wikilink") and
    [J.P. Moreland](../Page/J.P._Moreland.md "wikilink"), eds., 2000,
    *Naturalism: A Critical Analysis*, Routledge. ISBN 0-415-23524-3
  - Stewart Goetz and Charles Taliaferro, 2008, *Naturalism*, Eerdmans
    Publishing. ISBN 978-0-8028-0768-7
  - [Phillip E. Johnson](../Page/Phillip_E._Johnson.md "wikilink"),
    1998, *Reason in the Balance: The Case Against Naturalism in
    Science, Law & Education*, InterVarsity Press. ISBN 0-8308-1929-0
    and 2002, *The Wedge of Truth: Splitting the Foundations of
    Naturalism*, InterVarsity Press. ISBN 0-8308-2395-6
  - [C.S. Lewis](../Page/C.S._Lewis.md "wikilink"), ed., 1996,
    "Miracles", Harper Collins. ISBN 0-06-065301-9
  - [Michael Rea](../Page/Michael_Rea.md "wikilink"), 2004, *World
    without Design: The Ontological Consequences of Naturalism*, Oxford
    University Press. ISBN 0-19-924761-7
  - [Victor Reppert](../Page/Victor_Reppert.md "wikilink"), 2003, *C.S.
    Lewis's Dangerous Idea: In Defense of the Argument from Reason*,
    InterVarsity Press. ISBN 0-8308-2732-3
  - [Mark Steiner](../Page/Mark_Steiner.md "wikilink"), 2002, *The
    Applicability of Mathematics as a Philosophical Problem*, Harvard
    University Press. ISBN 0-674-00970-3

[Category:科學哲學](../Category/科學哲學.md "wikilink")
[Category:湧現](../Category/湧現.md "wikilink")