**彦根市**（）位于[日本](../Page/日本.md "wikilink")[近畿地方](../Page/近畿地方.md "wikilink")[滋贺县](../Page/滋贺县.md "wikilink")，是[琵琶湖东岸的最大城市与旅游观光城市](../Page/琵琶湖.md "wikilink")。城市是以1603年开始建设的[彥根城为中心逐渐发展起来的](../Page/彥根城.md "wikilink")，彥根城现在已成为著名的观光胜地。除了過去屬於舊和舊[愛知郡](../Page/愛知郡.md "wikilink")的地區外，市內大部分的區域過去屬於[犬上郡](../Page/犬上郡.md "wikilink")。為[日本國際鳥人錦標賽的舉辦地點](../Page/日本國際鳥人錦標賽.md "wikilink")。

## 地理

### 位置

  - 東端 ：北緯35度16分26.8秒
    東經136度21分5.3秒（武奈町）。[1](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.3515&latitude=35.274114)
  - 北端 ：北緯35度18分7.1秒 東經136度18分3.7秒（
    [名神高速道路在接近摺針峠處的隧道附近](../Page/名神高速道路.md "wikilink")）。[2](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.301028&latitude=35.301975)
      - 水域北端 ：北緯35度21分21.4秒
        東經136度8分54.8秒（多景島沖）。[3](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.148583&latitude=35.355972)
  - 西端 ：北緯35度13分1.7秒
    東經136度6分45.9秒（愛知川河口北岸）。[4](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.11275&latitude=35.217166)
      - 水域西端 ：北緯35度15分31.6秒
        東經136度5分1.7秒（愛知川河口沖）。[5](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.083806&latitude=35.258787)
  - 南端 ：北緯35度11分24.6秒
    東經136度11分30秒（服部町）。[6](http://watchizu.gsi.go.jp/watchizu.html?longitude=136.191694&latitude=35.190168)

### 氣候

雖被分類在屬於[日本海側氣候的滋賀縣北部](../Page/日本海側氣候.md "wikilink")，但因其鄰近滋賀縣中部，與滋賀縣北部其他地域相比，降雪量較少，未被指定為豪雪地帶。

又因此地觀測所鄰近琵琶湖，日溫差、年溫差也比滋賀縣内其他地區小。

## 交通

  - [JR西日本](../Page/JR西日本.md "wikilink")[東海道本線](../Page/東海道本線.md "wikilink")（琵琶湖線）
  - [近江鐵道](../Page/近江鐵道.md "wikilink")
  - [名神高速道路](../Page/名神高速道路.md "wikilink")

## 著名景點

### 以自然為主的景點

  - [琵琶湖](../Page/琵琶湖.md "wikilink")

  - ：為[琵琶湖國定公園第](../Page/琵琶湖國定公園.md "wikilink")1種特別地域中的一個。

  - ：「[名水百選](../Page/名水百選.md "wikilink")」選定的[泉水](../Page/泉水.md "wikilink")。

### 古蹟

  - [彥根城](../Page/彥根城.md "wikilink")：日本國寶，僅存的四座木造城堡之一

## 教育

大学

  - [滋贺大学](../Page/滋贺大学.md "wikilink")（国立）
  - 滋贺县立大学
  - 圣泉大学（私立）

## 象徵

## 友好城市

  - [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")[安娜堡](../Page/安娜堡.md "wikilink")，[美国](../Page/美国.md "wikilink")[密西根州](../Page/密西根州.md "wikilink")
  - [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg")[湘潭](../Page/湘潭.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")

[\*](../Category/彥根市.md "wikilink")