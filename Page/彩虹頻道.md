**彩虹頻道**（**Rainbow
Channel**，1994年—2005年）是[台灣電視史上第一家核准合法播映](../Page/台灣電視史.md "wikilink")[成人節目的](../Page/成人.md "wikilink")[有線電視頻道](../Page/有線電視.md "wikilink")，是[朝禾事業股份有限公司](../Page/朝禾事業.md "wikilink")（SHOVWA
ENTERPRISE CO.,
LTD.）旗下頻道之一。1994年3月，彩虹頻道取得頻道執照，24小時播放成人節目，與[星穎](../Page/星穎.md "wikilink")、[新東寶合稱有線電視](../Page/新東寶.md "wikilink")「三寶」。2005年8月3日，彩虹頻道被[行政院新聞局](../Page/行政院新聞局.md "wikilink")[撤銷執照](../Page/2005年台灣電視頻道換照爭議.md "wikilink")，勒令關閉。2008年3月，朝禾事業在[中華電信MOD推出](../Page/中華電信MOD.md "wikilink")「彩虹家族」套餐，宣告捲土重來。

但某部份有線電視業者有在重新簽約，在[DVB-C](../Page/DVB.md "wikilink")[機上盒上重新播映](../Page/機上盒.md "wikilink")。

## 所屬頻道位置

<table>
<thead>
<tr class="header">
<th><p>供應商</p></th>
<th><p>彩虹頻道</p></th>
<th><p>彩虹e台</p></th>
<th><p>彩虹電影台</p></th>
<th><p>K頻道</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中華電信MOD.md" title="wikilink">中華電信MOD</a></p></td>
<td></td>
<td><p>832</p></td>
<td><p>833</p></td>
<td><p>834</p></td>
</tr>
<tr class="even">
<td><p>凱擘大寬頻/台固</p></td>
<td><p>900</p></td>
<td><p>901</p></td>
<td><p>902</p></td>
<td><p>903</p></td>
</tr>
<tr class="odd">
<td><p>TBC台灣寬頻通訊</p></td>
<td><p>404</p></td>
<td><p>405</p></td>
<td><p>406</p></td>
<td><p>407</p></td>
</tr>
<tr class="even">
<td><p>中嘉bbTV</p></td>
<td><p>301</p></td>
<td><p>308</p></td>
<td><p>309</p></td>
<td><p>310</p></td>
</tr>
<tr class="odd">
<td><p>寶福/聯維</p></td>
<td><p>257</p></td>
<td><p>255</p></td>
<td><p>256</p></td>
<td><p>258</p></td>
</tr>
<tr class="even">
<td><p>全國數位</p></td>
<td><p>511</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>新北市/天外天</p></td>
<td><p>604</p></td>
<td><p>605</p></td>
<td><p>606</p></td>
<td><p>607</p></td>
</tr>
<tr class="even">
<td><p>大豐</p></td>
<td><p>311</p></td>
<td><p>312</p></td>
<td><p>314</p></td>
<td><p>313</p></td>
</tr>
<tr class="odd">
<td><p>台灣數位光訊</p></td>
<td><p>408</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三大</p></td>
<td><p>289</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大揚/新永安/南國</p></td>
<td></td>
<td><p>405</p></td>
<td><p>406</p></td>
<td><p>407</p></td>
</tr>
<tr class="even">
<td><p>北都</p></td>
<td><p>506</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:台灣電視台](../Category/台灣電視台.md "wikilink")
[Category:臺灣的性](../Category/臺灣的性.md "wikilink")
[Category:1994年成立的電視台或電視頻道](../Category/1994年成立的電視台或電視頻道.md "wikilink")
[Category:2005年停播的電視台或電視頻道](../Category/2005年停播的電視台或電視頻道.md "wikilink")
[Category:1994年台灣建立](../Category/1994年台灣建立.md "wikilink")
[Category:2005年台灣廢除](../Category/2005年台灣廢除.md "wikilink")