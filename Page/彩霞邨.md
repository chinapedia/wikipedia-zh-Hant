[Choi_Ha_Estate_Landscape_Area_2009.jpg](https://zh.wikipedia.org/wiki/File:Choi_Ha_Estate_Landscape_Area_2009.jpg "fig:Choi_Ha_Estate_Landscape_Area_2009.jpg")
[Choi_Ha_Estate_Children_Playground_2009.jpg](https://zh.wikipedia.org/wiki/File:Choi_Ha_Estate_Children_Playground_2009.jpg "fig:Choi_Ha_Estate_Children_Playground_2009.jpg")
**彩霞邨**（**Choi Ha
Estate**）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[觀塘區的](../Page/觀塘區.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[佐敦谷](../Page/佐敦谷.md "wikilink")，由[房屋委員會及](../Page/房屋委員會.md "wikilink")[房屋署發展](../Page/房屋署.md "wikilink")。屋邨的具體位置在[瑪利諾中學的對面](../Page/瑪利諾中學.md "wikilink")，鄰近[淘大花園](../Page/淘大花園.md "wikilink")，於1982年開始設計興建，於1989年至1990年落成入伙。

彩霞邨原址為一個[石礦場](../Page/石礦場.md "wikilink")，後來被納入公共房屋興建範圍，容納毗鄰[佐敦谷邨清拆計劃的居民](../Page/佐敦谷邨.md "wikilink")。由於當時附近有一條叫做[彩霞道的道路](../Page/彩霞道.md "wikilink")，所以建成公共屋邨後亦以該條街道作為屋邨的名字。及後因應長遠房屋政策，房委會將彩霞邨納入[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")，並於該計劃的第三期，2000年開始發售。

## 屋邨資料

### 樓宇

分為彩星樓，彩日樓，彩月樓三座，每座高34層

| 樓宇名稱 | 樓宇類型                             | 落成年份 |
| ---- | -------------------------------- | ---- |
| 彩星樓  | [Y3型](../Page/Y3型.md "wikilink") | 1989 |
| 彩日樓  | [Y4型](../Page/Y4型.md "wikilink") |      |
| 彩月樓  |                                  |      |

### 商舖及停車場

邨內有一座包含商舖及停車場的大廈，現由[領展負責管理](../Page/領展.md "wikilink")，建築樓面面積約2861[平方米](../Page/平方米.md "wikilink")。

## 教育及福利設施

### 幼稚園

  - [天主教彩霞邨潔心幼稚園](http://www.sihm.edu.hk/)（1990年創辦）

### 特殊幼兒中心

  - [協康會王石崇傑紀念中心](https://www.heephong.org/center/detail/mary-wong-centre)

## 圖片集

<File:Choi> Ha Estate (Hong Kong).jpg|彩霞邨彩日樓

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/彩霞道.md" title="wikilink">彩霞道</a>/<a href="../Page/彩禧路.md" title="wikilink">彩禧路</a></dt>

</dl>
<p><a href="../Page/九龍巴士13X線.md" title="wikilink">13X</a> 特別班次 <br />
</p>
<dl>
<dt><a href="../Page/香港公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>觀塘至<a href="../Page/彩盈邨.md" title="wikilink">彩盈邨線</a>[1]</li>
<li><a href="../Page/秀茂坪.md" title="wikilink">秀茂坪至</a><a href="../Page/土瓜灣.md" title="wikilink">土瓜灣</a>/佐敦道線[2]</li>
<li>秀茂坪至旺角線 (24小時服務)[3]</li>
</ul>
<dl>
<dt><a href="../Page/福淘街.md" title="wikilink">福淘街</a><br />
<a href="../Page/香港公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>觀塘/黃大仙至青山道線 (通宵服務)[4]</li>
<li>觀塘至美孚線[5]</li>
<li>觀塘至土瓜灣/佐敦道線 (24小時服務)[6]</li>
<li>牛頭角至旺角線[7]</li>
<li>牛頭角至土瓜灣線 (上午服務)[8]</li>
<li>觀塘至香港仔線 (下午服務)[9]</li>
<li>觀塘至西環線 (上午服務)[10]</li>
</ul>
<dl>
<dt><a href="../Page/牛頭角道.md" title="wikilink">牛頭角道</a></dt>

</dl>
<dl>
<dt><a href="../Page/觀塘道.md" title="wikilink">觀塘道</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>觀塘至<a href="../Page/青山道.md" title="wikilink">青山道線</a> (上午服務)[11]</li>
<li>觀塘/<a href="../Page/黃大仙.md" title="wikilink">黃大仙至青山道線</a> (通宵服務)[12]</li>
<li>觀塘至<a href="../Page/美孚.md" title="wikilink">美孚線</a>[13]</li>
<li>觀塘至<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣</a>/<a href="../Page/佐敦道.md" title="wikilink">佐敦道線</a> (24小時服務)[14]</li>
<li>觀塘至<a href="../Page/旺角.md" title="wikilink">旺角線</a> (24小時服務)[15]</li>
<li>牛頭角至旺角線[16]</li>
<li>牛頭角至佐敦道線 (上午服務)[17]</li>
<li><a href="../Page/紅磡.md" title="wikilink">紅磡至觀塘線</a> (24小時服務)[18]</li>
<li>觀塘至香港仔線 (下午服務)[19]</li>
<li>觀塘至西環線 (上午服務)[20]</li>
<li>銅鑼灣至觀塘線 (通宵服務)[21]</li>
<li>灣仔至觀塘線 (通宵服務)[22]</li>
</ul></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  - [房委會彩霞邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-13_4981,00.html)

[en:Public housing estates in Ngau Tau Kok and Kowloon Bay\#Choi Ha
Estate](../Page/en:Public_housing_estates_in_Ngau_Tau_Kok_and_Kowloon_Bay#Choi_Ha_Estate.md "wikilink")

[Category:佐敦谷](../Category/佐敦谷.md "wikilink")
[Category:領展商場及停車場](../Category/領展商場及停車場.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:以街道命名的公營房屋](../Category/以街道命名的公營房屋.md "wikilink")

1.  [觀塘瑞和街街市—牛頭角,
    觀塘瑞和街街市—彩盈邨](http://www.16seats.net/chi/rmb/r_k23.html)
2.  [秀茂坪—佐敦道北海街](http://www.16seats.net/chi/rmb/r_k95.html)
3.  [秀茂坪—旺角先達廣場](http://www.16seats.net/chi/rmb/r_k01.html)
4.  [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)
5.  [觀塘協和街—美孚](http://www.16seats.net/chi/rmb/r_k64.html)
6.  [觀塘同仁街—佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
7.  [牛頭角站—旺角登打士街](http://www.16seats.net/chi/rmb/r_k24.html)
8.  [牛頭角站—佐敦道吳松街](http://www.16seats.net/chi/rmb/r_k56.html)
9.  [香港仔湖北街—觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
10. [觀塘宜安街＞西環卑路乍街,
    西環修打蘭街＞觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
11. [觀塘協和街—青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
12. [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)
13. [觀塘協和街—美孚](http://www.16seats.net/chi/rmb/r_k64.html)
14. [觀塘同仁街—佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
15. [觀塘同仁街—旺角先達廣場／奧運站](http://www.16seats.net/chi/rmb/r_k31.html)
16. [牛頭角站—旺角登打士街](http://www.c16seats.net/chi/rmb/r_k24.html)
17. [牛頭角站—佐敦道吳松街](http://www.16seats.net/chi/rmb/r_k56.html)
18. [紅磡差館里＞牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_k15.html)
19. [香港仔湖北街—觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
20. [觀塘宜安街＞西環卑路乍街,
    西環修打蘭街＞觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
21. [銅鑼灣鵝頸橋—牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
22. [中環／灣仔＞牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)