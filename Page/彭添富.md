**彭添富**（**Peng
Tien-fu**，），[台灣](../Page/台灣.md "wikilink")[桃園市人](../Page/桃園市.md "wikilink")，台灣政治人物，曾連任兩屆[台灣省議員及立法委員](../Page/台灣省.md "wikilink")，2008年於桃園中壢選區立委落選後短暫擔任[行政院客家委員會副主任委員](../Page/行政院客家委員會.md "wikilink")，520政權交接後轉任[民主進步黨族群事務部主任等黨職](../Page/民主進步黨.md "wikilink")。2009年，以無黨籍違紀參選因時任立委[廖正井當選無效而遺留的海線立委缺額補選](../Page/廖正井.md "wikilink")，但投票前夕決定支持且最終勝選的前民進黨立委[郭榮宗](../Page/郭榮宗.md "wikilink")。2012年，再度回鍋參選立委，但最終因泛綠分裂，在平鎮龍潭選區敗給了前平鎮市長[陳萬得之妻](../Page/陳萬得.md "wikilink")[呂玉玲](../Page/呂玉玲.md "wikilink")，這也是他繼2008年挑戰三連霸失利後第三度參與立委選舉落敗。現任桃園果菜市場股份有限公司董事長。

## 選舉

### [2004年中華民國立法委員選舉](../Page/2004年中華民國立法委員選舉.md "wikilink")

| 2004年桃園縣選舉區立法委員選舉結果 |
| ------------------- |
| 應選13席               |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |
| 5                   |
| 6                   |
| 7                   |
| 8                   |
| 9                   |
| 10                  |
| 11                  |
| 12                  |
| 13                  |
| 14                  |
| 15                  |
| 16                  |
| 17                  |
| 18                  |
| 19                  |
| 20                  |
| 21                  |
| 22                  |
| 23                  |
| 24                  |
| 25                  |
| 26                  |
| 27                  |
| 28                  |

### [第七屆立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")

| 2008年[桃園縣第三選舉區](../Page/桃園市第三選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

### [第七屆立法委員補選](../Page/2010年桃園縣第二選舉區立法委員缺額補選.md "wikilink")

| 2010年[桃園縣第二選舉區](../Page/桃園市第二選舉區.md "wikilink")[立法委員補選選舉結果](../Page/立法委員.md "wikilink") |
| --------------------------------------------------------------------------------------- |
| 號次                                                                                      |
| 1                                                                                       |
| 2                                                                                       |
| 3                                                                                       |
| **選舉人數**                                                                                |
| **投票數**                                                                                 |
| **有效票**                                                                                 |
| **無效票**                                                                                 |
| **投票率**                                                                                 |

### [第八屆立法委員選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")

  - [桃園市第五選舉區總選舉人數](../Page/桃園市第五選舉區.md "wikilink")：235,906
  - 總票數（投票率）：177,866（75.40%）
  - 有效票（比率）：173,310（97.44%）
  - 無效票（比率）：4,556（2.56%）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/黃嘉華.md" title="wikilink">黃嘉華</a></p></td>
<td></td>
<td><p>717</p></td>
<td><p>0.41%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/黃國華.md" title="wikilink">黃國華</a></p></td>
<td></td>
<td><p>433</p></td>
<td><p>0.25%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/吳平娥.md" title="wikilink">吳平娥</a></p></td>
<td></td>
<td><p>15,779</p></td>
<td><p>9.10%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/劉邦鉉.md" title="wikilink">劉邦鉉</a></p></td>
<td></td>
<td><p>15,644</p></td>
<td><p>9.03%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/呂玉玲.md" title="wikilink">呂玉玲</a></p></td>
<td></td>
<td><p>78,504</p></td>
<td><p>45.30%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/陳振瑋.md" title="wikilink">陳振瑋</a></p></td>
<td></td>
<td><p>1,346</p></td>
<td><p>0.78%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>彭添富</p></td>
<td></td>
<td><p>60,887</p></td>
<td><p>35.13%</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 標示粗體者表競選連任之候選人

## 外部連結

  - [立法院](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00147&stage=5)

|- |colspan="3"
style="text-align:center;"|**[中華民國](../Page/中華民國.md "wikilink")[行政院](../Page/行政院.md "wikilink")**


[Category:中華民國行政院客家委員會副主任委員](../Category/中華民國行政院客家委員會副主任委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:臺灣省諮議長](../Category/臺灣省諮議長.md "wikilink")
[Category:台灣省諮議員](../Category/台灣省諮議員.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:第9屆臺灣省議員](../Category/第9屆臺灣省議員.md "wikilink")
[Category:第11屆桃園縣議員](../Category/第11屆桃園縣議員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:淡江大學校友](../Category/淡江大學校友.md "wikilink")
[Category:中華大學校友](../Category/中華大學校友.md "wikilink")
[Category:華夏科技大學校友](../Category/華夏科技大學校友.md "wikilink")
[Category:客家裔臺灣人](../Category/客家裔臺灣人.md "wikilink")
[Category:桃園市人](../Category/桃園市人.md "wikilink")
[T添富](../Category/彭姓.md "wikilink")