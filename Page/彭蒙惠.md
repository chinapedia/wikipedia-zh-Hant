**彭蒙惠**，原名**多丽丝·布鲁尔姆**（**Doris M.
Brougham**；），出生於[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")，2002年起為[中華民國永久居民](../Page/中華民國.md "wikilink")，是一位奉獻[臺灣的](../Page/臺灣.md "wikilink")[基督教](../Page/基督教.md "wikilink")[宣教士及](../Page/宣教士.md "wikilink")[教育家](../Page/教育家.md "wikilink")。

## 學歷

  - 1944年至1947年：美國華盛頓州西雅圖[辛普森聖經學院基督教教育](../Page/辛普森聖經學院.md "wikilink")[學士](../Page/學士.md "wikilink")
  - 1947年至1948年：美國[西雅圖華盛頓大學遠東研究學士](../Page/西雅圖華盛頓大學.md "wikilink")
  - 1948年至1949年：[中國內地會語言學校](../Page/中國內地會.md "wikilink")
  - 1953年：美國[西雅圖太平洋學院](../Page/西雅圖太平洋學院.md "wikilink")（Seattle Pacific
    College）遠東研究證書
  - 1986年：美國[加州](../Page/加州.md "wikilink")[太平洋西方大學](../Page/太平洋西方大學.md "wikilink")（Pacific
    Western
    University）傳播學士、美國加州[太平洋大學](../Page/太平洋大學.md "wikilink")（Pacific
    States University）榮譽[法學博士](../Page/法學.md "wikilink")
  - 1988年：美國[阿蘇薩太平洋大學](../Page/阿蘇薩太平洋大學.md "wikilink")（Azusa Pacific
    University）榮譽文學博士
  - 1991年：美國[西雅圖太平洋大學](../Page/西雅圖太平洋大學.md "wikilink")（Seattle Pacific
    University）榮譽文學博士
  - 2009年: 美國密西西比州傑克森市 Belhaven 學院榮譽博士

## 傳教與任教

彭蒙惠12歲時，立志要向[華人地區傳播](../Page/華人.md "wikilink")[福音](../Page/福音.md "wikilink")。1948年，彭蒙惠抵達[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")，後來因[中國內戰而到](../Page/國共內戰.md "wikilink")[香港](../Page/香港.md "wikilink")。1951年，來到[台灣](../Page/台灣.md "wikilink")[花蓮傳教](../Page/花蓮.md "wikilink")，擔任[台灣聖書學校](../Page/台灣聖書學校.md "wikilink")[教授](../Page/教授.md "wikilink")（至1956年止）。1953年至1957年，擔任[花蓮高中英文](../Page/花蓮高中.md "wikilink")[教師](../Page/教師.md "wikilink")。1963年至1973年，擔任[國立藝專](../Page/國立藝專.md "wikilink")[管樂教授](../Page/管樂.md "wikilink")。1973年至1983年，擔任[三軍大學](../Page/三軍大學.md "wikilink")[軍官英文班教授](../Page/軍官.md "wikilink")。1976年至1982年，擔任[中華航空公司空服人員英語進修班指導老師](../Page/中華航空公司.md "wikilink")。1977年，主持英語電視教學節目《世界英語》。1977年至1994年，擔任[實踐家專客座教授](../Page/實踐大學.md "wikilink")。1978年，擔任[中華民國國防部軍官外語學校教授](../Page/中華民國國防部.md "wikilink")。1980年至1985年，擔任[台北市政府高級主管英文班教授](../Page/台北市政府.md "wikilink")。1994年，彭蒙惠設立「彭蒙惠教育[獎學金](../Page/獎學金.md "wikilink")」。

## 多媒體教英語

1960年4月，彭蒙惠與[李恩祺夫婦](../Page/李恩祺.md "wikilink")（Leland & Dorothy
Haggerty）及[唐主謙](../Page/唐主謙.md "wikilink")[牧師夫婦共同在](../Page/牧師.md "wikilink")[台北市](../Page/台北市.md "wikilink")[中山北路二段創辦](../Page/中山北路.md "wikilink")[中華救世廣播團](../Page/中華救世廣播團.md "wikilink")（Overseas
Radio Inc.），也就是今日的救世傳播協會（ORTV；Overseas Radio & Television Inc.）的前身。

1962年，彭蒙惠創辦[英語教學](../Page/英語.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《空中英語文摘》，並且開始了[廣播英語教學](../Page/廣播.md "wikilink")。《空中英語文摘》歷經40年的改版、編輯，演化為《[空中英語教室](../Page/空中英語教室.md "wikilink")》（*Studio
Classroom*），由救世傳播協會旗下的空中英語教室文摘社發行，是台灣最普遍的英語教學雜誌之一；後來又發行了《大家說英語》（*Let's
Talk in English*）與《彭蒙惠英語》（*Advanced*）。除了廣播、電視教學節目外,也發展網際網路、APP 等多種數位媒體。

2000年代，彭蒙惠與洪善群製作[華視兒童英語教學節目](../Page/華視.md "wikilink")《兒童美語會話300句》。

## 獲獎紀錄

由於彭蒙惠對於台灣英語教育的貢獻，1996年，彭蒙惠榮獲台北市政府頒贈85年度台北市榮譽市民獎。2002年，彭蒙惠獲得[中華民國總統](../Page/中華民國總統.md "wikilink")[陳水扁頒贈](../Page/陳水扁.md "wikilink")「紫色大綬[景星勳章](../Page/景星勳章.md "wikilink")」，並成為首批獲得[中華民國外僑永久居留證的外國人](../Page/中華民國外僑永久居留證.md "wikilink")。2010年3月20日，彭蒙惠獲得[周大觀文教基金會](../Page/周大觀文教基金會.md "wikilink")[頒贈第13屆「全球熱愛生命獎章」](http://krt.101events.asia/home139d.html?p_p_id=62_INSTANCE_XaWA&p_p_action=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-2&p_p_col_pos=7&p_p_col_count=8&_62_INSTANCE_XaWA_struts_action=%2Fjournal_articles%2Fview&_62_INSTANCE_XaWA_groupId=10402&_62_INSTANCE_XaWA_articleId=1783523&_62_INSTANCE_XaWA_version=1.0)。2014
獲美華盛頓州西雅圖市頒布[四月二日為彭蒙惠日（Doris M. Brougham
Day）](http://www.chinatimes.com/realtimenews/20140624005859-260405)。2017
獲得[台南市頒發榮譽市民（Certificate of Honorary Citizenship of
Tainan）](http://www.cna.com.tw/news/aloc/201712270138-1.aspx)。

## 相關著作

  - 彭蒙惠/口述，李文茹/整理執筆：《愛是一生的堅持──彭蒙惠傳奇》（台北：彭蒙惠英語出版社，1998）

## 注釋

## 外部連結

  - [空中英語教室網站的彭蒙惠介紹](http://www.studioclassroom.com/h_doris_c.php)
  - [民視台灣演義：愛在台灣
    彭蒙惠故事（2012/03/25）](http://www.youtube.com/watch?v=D0mbfS1dJNc&list=PLCC9A19F17FD442BB&feature=plpp_play_all)
  - [捨棄親情與愛情　彭蒙惠一甲子的台灣情 - 風傳媒](http://www.storm.mg/article/38777)

[D](../Category/臺灣教育工作者.md "wikilink")
[D](../Category/台灣戰後時期美國籍傳教士.md "wikilink")
[D](../Category/女性傳教士.md "wikilink")
[D](../Category/美國新教徒.md "wikilink")
[D](../Category/獲頒授景星勳章者.md "wikilink")
[D](../Category/西雅圖人.md "wikilink")
[D](../Category/華盛頓大學校友.md "wikilink")
[Category:國立臺灣藝術大學教授](../Category/國立臺灣藝術大學教授.md "wikilink")
[Category:英語教師](../Category/英語教師.md "wikilink")
[Category:在台灣的美國人](../Category/在台灣的美國人.md "wikilink")