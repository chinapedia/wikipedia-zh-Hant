**影響200**，為2000年《[天下雜誌](../Page/天下雜誌.md "wikilink")》紀念出刊200期而製作的[臺灣人物專輯](../Page/臺灣.md "wikilink")，標舉**「影響200，飛躍2000」**，由台灣社會各領域人物中選出兩百位人士，記錄台灣歷史發展，《天下雜誌》一仍其**「打開歷史，走出未來」**的訴求希望能藉喚起社會共同記憶，凝聚共同情感。

## 採訪面向

影響200專題，以「世界」、「開拓」、「啟蒙」、「行動」、「美麗」、「自重」、「豐富」、「共生」等八個面向，選擇代表人物，進行採訪\[1\]。

## 所選人物

### 世界

  - [劉銘傳](../Page/劉銘傳.md "wikilink")：[清代](../Page/清代.md "wikilink")[台灣省首任](../Page/台灣省.md "wikilink")[巡撫](../Page/台灣巡撫.md "wikilink")
  - [尹仲容](../Page/尹仲容.md "wikilink")：前經濟部長、[美援會副主委](../Page/美援會.md "wikilink")、[台灣銀行董事長](../Page/台灣銀行.md "wikilink")
  - [陳永華](../Page/陳永華_\(明朝將領\).md "wikilink")：[明鄭](../Page/明鄭.md "wikilink")[參軍](../Page/參軍.md "wikilink")
  - [蔡志忠](../Page/蔡志忠.md "wikilink")：漫畫家
  - [張榮發](../Page/張榮發.md "wikilink")：[長榮集團創辦人](../Page/長榮集團.md "wikilink")、總裁
  - [宋作楠](../Page/宋作楠.md "wikilink")：[勤業會計師事務所創辦人](../Page/勤業會計師事務所.md "wikilink")
  - [朱 銘](../Page/朱銘.md "wikilink")：雕塑家
  - [段鍾沂](../Page/段鍾沂.md "wikilink")、[段鍾潭](../Page/段鍾潭.md "wikilink")：[滾石國際音樂經營者](../Page/滾石唱片.md "wikilink")
  - [後藤新平](../Page/後藤新平.md "wikilink")：[台灣日治時期](../Page/台灣日治時期.md "wikilink")[總督府](../Page/臺灣總督府.md "wikilink")[民政長官](../Page/民政長官.md "wikilink")
  - [施振榮](../Page/施振榮.md "wikilink")：[宏碁集團創辦人](../Page/宏碁集團.md "wikilink")
  - [俞國華](../Page/俞國華.md "wikilink")：前[中華民國中央銀行總裁](../Page/中華民國中央銀行.md "wikilink")、[行政院長](../Page/行政院長.md "wikilink")
  - [徐賢修](../Page/徐賢修.md "wikilink")：前[國立清華大學校長](../Page/國立清華大學.md "wikilink")、[行政院國家科學委員會主任委員](../Page/行政院國家科學委員會.md "wikilink")、工研院董事長，[新竹科學園區催生者](../Page/新竹科學園區.md "wikilink")
  - [劉大中](../Page/劉大中.md "wikilink")：[中央研究院院士](../Page/中央研究院.md "wikilink")、經濟學家，推動台灣賦稅改革
  - [嚴家淦](../Page/嚴家淦.md "wikilink")：前[行政院長](../Page/行政院長.md "wikilink")、[中華民國副總統](../Page/中華民國副總統.md "wikilink")、[總統](../Page/中華民國總統.md "wikilink")
  - [王惕吾](../Page/王惕吾.md "wikilink")：[聯合報創辦人](../Page/聯合報.md "wikilink")
  - [張忠謀](../Page/張忠謀.md "wikilink"):前[工研院董事長](../Page/工研院.md "wikilink")、[台積電創辦人](../Page/台積電.md "wikilink")
  - [莊
    嚴](../Page/莊嚴.md "wikilink"):[國立故宮博物院副院長](../Page/國立故宮博物院.md "wikilink")
  - [李國鼎](../Page/李國鼎.md "wikilink"):前[經濟部長](../Page/中華民國經濟部.md "wikilink")、財政部長，被譽為[台灣經濟奇蹟的重要推手](../Page/台灣經濟.md "wikilink")
  - [郭婉容](../Page/郭婉容.md "wikilink"):前[財政部長](../Page/中華民國財政部.md "wikilink")
  - [宋秩銘](../Page/宋秩銘.md "wikilink"):知名[廣告人](../Page/廣告.md "wikilink")，台灣[奧美總經理](../Page/奧美.md "wikilink")、奧美集團大中華區董事長
  - [徐小波](../Page/徐小波.md "wikilink"):前[理律法律事務所共同主持律師](../Page/理律法律事務所.md "wikilink")
  - [金唯信](../Page/金唯信.md "wikilink"):
  - [李連墀](../Page/李連墀.md "wikilink"):
  - [焦雄屏](../Page/焦雄屏.md "wikilink"):台灣著名[電影學者](../Page/電影.md "wikilink")、[電影製片人](../Page/電影製片.md "wikilink")
  - [德微臣](../Page/德微臣.md "wikilink"):
  - [林媽利](../Page/林媽利.md "wikilink"):台灣著名的外科醫師，被譽為「台灣血液之母」
  - [蔣中正](../Page/蔣中正.md "wikilink"):前[中華民國總統](../Page/中華民國總統.md "wikilink")
  - [羅益強](../Page/羅益強.md "wikilink"):

### 開拓

  - [王永慶](../Page/王永慶.md "wikilink"):[台塑集團創辦人](../Page/台塑集團.md "wikilink")，被譽為臺灣的「經營之神」
  - [廖一久](../Page/廖一久.md "wikilink"):台灣知名的水產養殖學者、中研院院士，被譽為「草蝦養殖之父」。
  - [趙耀東](../Page/趙耀東.md "wikilink"):[中鋼總經理](../Page/中鋼.md "wikilink")、董事長，[經濟部長](../Page/經濟部.md "wikilink")
  - [陳
    誠](../Page/陳誠.md "wikilink"):[行政院長](../Page/行政院.md "wikilink")、中華民國副總統
  - [歐晉德](../Page/歐晉德.md "wikilink"):[台北市副市長](../Page/台北市.md "wikilink")、[台灣高鐵執行長](../Page/台灣高鐵.md "wikilink")、董事長
  - [汪彝定](../Page/汪彝定.md "wikilink"):
  - [吳舜文](../Page/吳舜文.md "wikilink"):臺灣企業家，[中華汽車工業](../Page/中華汽車工業.md "wikilink")、[裕隆汽車董事長](../Page/裕隆汽車.md "wikilink")，素有「汽車皇后」封號
  - [吳光亮](../Page/吳光亮.md "wikilink"):清[光緒年間的](../Page/光緒.md "wikilink")[台灣鎮總兵](../Page/台灣鎮總兵.md "wikilink")
  - [吳
    沙](../Page/吳沙.md "wikilink"):清代開墾臺灣[宜蘭的先賢](../Page/宜蘭縣.md "wikilink")
  - [林長城](../Page/林長城.md "wikilink"):
  - [郁永河](../Page/郁永河.md "wikilink"):清代作家、在台官員
  - [呂秀蓮](../Page/呂秀蓮.md "wikilink"):前[桃園縣縣長](../Page/桃園市.md "wikilink")、中華民國首位女性副總統
  - [林挺生](../Page/林挺生.md "wikilink"):前[大同公司董事長](../Page/大同公司.md "wikilink")
  - [陳
    炘](../Page/陳炘.md "wikilink"):台灣金融界的先驅、[228事件受難者](../Page/二二八事件.md "wikilink")
  - [陳文郁](../Page/陳文郁.md "wikilink"):台灣「西瓜大王」
  - [陳拱北](../Page/陳拱北.md "wikilink"):前[連江縣縣長](../Page/連江縣.md "wikilink")、國大代表
  - [許世鉅](../Page/許世鉅.md "wikilink"):
  - [許博允](../Page/許博允.md "wikilink"):台灣音樂家
  - [顏雲年](../Page/顏雲年.md "wikilink"):日治時期的知名礦業家，出身[基隆顏家](../Page/基隆顏家.md "wikilink")。
  - [司徒達賢](../Page/司徒達賢.md "wikilink"):管理學者、[政治大學講座教授](../Page/政治大學.md "wikilink")
  - [胡定華](../Page/胡定華.md "wikilink"):
  - [高承恕](../Page/高承恕.md "wikilink"):
  - [鄭成功](../Page/鄭成功.md "wikilink"):明末抗清名將、民族英雄，[明鄭時期的開創者](../Page/明鄭時期.md "wikilink")
  - [張任飛](../Page/張任飛.md "wikilink"):
  - [羅漢腳](../Page/羅漢腳.md "wikilink"):清治時期的男性遊民。

### 啟蒙

  - [辛志平](../Page/辛志平.md "wikilink"):前[新竹中學校長](../Page/新竹中學.md "wikilink")，有「教育哲人」之稱譽。
  - [胡
    適](../Page/胡適.md "wikilink"):前[中央研究院院長](../Page/中央研究院.md "wikilink")
  - [聖嚴法師](../Page/聖嚴法師.md "wikilink"):[法鼓山創辦人](../Page/法鼓山.md "wikilink")
  - [孫
    文](../Page/孫文.md "wikilink"):[中國近代革命家](../Page/中國.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")[國父](../Page/國父.md "wikilink")
  - [蔣渭水](../Page/蔣渭水.md "wikilink"):[台灣日治時期反殖民運動的領導人之一](../Page/台灣日治時期.md "wikilink")，有「台灣的孫中山」之譽
  - [雷
    震](../Page/雷震.md "wikilink"):台灣政治人物、政論家，[白色恐怖受難者](../Page/白色恐怖.md "wikilink")
  - [郭雨新](../Page/郭雨新.md "wikilink"):前[臺灣省議員](../Page/臺灣省議員.md "wikilink")，是[黨外運動元老之一](../Page/黨外.md "wikilink")，被稱為「黨外祖師爺」。
  - [吳大猷](../Page/吳大猷.md "wikilink"):著名物理學家、前中研院院長
  - [吳健雄](../Page/吳健雄.md "wikilink"):著名女性物理學家、被稱為「中國[居里夫人](../Page/居里夫人.md "wikilink")」。
  - [孫立人](../Page/孫立人.md "wikilink"):中華民國[抗日名將](../Page/抗日.md "wikilink")、[陸軍總司令](../Page/中華民國陸軍.md "wikilink")，政治受難者
  - [俞大綱](../Page/俞大綱.md "wikilink"):中國戲曲專家。
  - [王大閎](../Page/王大閎.md "wikilink"):台北[國父紀念館的建築師](../Page/國立國父紀念館.md "wikilink")
  - [李
    喬](../Page/李喬.md "wikilink"):台灣劇作家、[國家文藝獎得主](../Page/國家文藝獎.md "wikilink")
  - [施
    敏](../Page/施敏.md "wikilink"):知名[半導體學者](../Page/半導體.md "wikilink")，中央研究院院士
  - [徐鍾珮](../Page/徐鍾珮.md "wikilink"):作家
  - [傅斯年](../Page/傅斯年.md "wikilink"):[國立台灣大學校長](../Page/國立台灣大學.md "wikilink")、中央研究院院士
  - [殷海光](../Page/殷海光.md "wikilink"):台灣[自由主義的啟蒙大師](../Page/自由主義.md "wikilink")，政治受難者。
  - [錢 穆](../Page/錢穆.md "wikilink"):中央研究院院士，知名歷史學家和教育家
  - [朱邦復](../Page/朱邦復.md "wikilink"):[倉頡輸入法發明人](../Page/倉頡輸入法.md "wikilink")，被[港](../Page/香港.md "wikilink")[台地區的華人譽為](../Page/台灣.md "wikilink")「[中文電腦之父](../Page/中文電腦.md "wikilink")」
  - [楊國樞](../Page/楊國樞.md "wikilink"):台灣[心理學家](../Page/心理學.md "wikilink")，中央研究院院士，[澄社創社社長](../Page/澄社.md "wikilink")。
  - [蔣 勳](../Page/蔣勳.md "wikilink"):台灣知名畫家
  - [張清吉](../Page/張清吉.md "wikilink"):台灣[出版家](../Page/出版.md "wikilink")，[志文出版社創辦人](../Page/志文出版社.md "wikilink")
  - [牟宗三](../Page/牟宗三.md "wikilink"):中華民國[哲學家](../Page/哲學.md "wikilink")、國立臺灣大學教授
  - [愛新覺羅·毓鋆](../Page/愛新覺羅·毓鋆.md "wikilink"):台灣著名[儒家學者](../Page/儒家.md "wikilink")
  - [江學珠](../Page/江學珠.md "wikilink"):台灣教育家、[北一女中校長](../Page/北一女中.md "wikilink")
  - [陳紹馨](../Page/陳紹馨.md "wikilink"):台灣第一位[社會學博士](../Page/社會學.md "wikilink")
  - [蔣夢麟](../Page/蔣夢麟.md "wikilink"):中華民國教育家，[北京大學校長](../Page/北京大學.md "wikilink")、[農復會主委](../Page/農復會.md "wikilink")
  - [林俊義](../Page/林俊義.md "wikilink"):台灣生物學家、前[環保署署長](../Page/環保署.md "wikilink")
  - [余紀忠](../Page/余紀忠.md "wikilink"):[中國時報創辦人](../Page/中國時報.md "wikilink")
  - [趙麗蓮](../Page/趙麗蓮.md "wikilink"):台灣電視節目主持人
  - [彭明敏](../Page/彭明敏.md "wikilink"):[臺灣獨立運動領袖之一](../Page/臺灣獨立運動.md "wikilink")，首位[民進黨](../Page/民進黨.md "wikilink")[總統候選人](../Page/1996年中華民國總統選舉.md "wikilink")
  - [曾虛白](../Page/曾虛白.md "wikilink"):[中央通訊社社長](../Page/中央通訊社.md "wikilink")

### 行動

  - [孫運璿](../Page/孫運璿.md "wikilink"):前經濟部長、行政院長，與李國鼎並稱為「台灣經濟的推手」。
  - [沈葆禎](../Page/沈葆禎.md "wikilink"):清朝[兩江總督](../Page/兩江總督.md "wikilink")
  - [巴爾頓](../Page/巴爾頓.md "wikilink"):台灣[自來水之父](../Page/自來水.md "wikilink")
  - [劉必稼](../Page/劉必稼.md "wikilink"):
  - [陳定南](../Page/陳定南.md "wikilink"):前[宜蘭縣長](../Page/宜蘭縣.md "wikilink")、[法務部長](../Page/中華民國法務部.md "wikilink")
  - [張曉春](../Page/張曉春.md "wikilink"):前台灣大學社會系教授、台灣勞動運動啟蒙者，有台灣勞工之父之稱
  - [李 秀](../Page/李秀.md "wikilink"):
  - [李鎮源](../Page/李鎮源.md "wikilink"):台灣中央研究院院士，知名的[藥理學家和台獨人士](../Page/藥理學.md "wikilink")。
  - [沈宗瀚](../Page/沈宗瀚.md "wikilink"):著名農業學者、農復會主委
  - [周碧瑟](../Page/周碧瑟.md "wikilink"):
  - [顧文魁](../Page/顧文魁.md "wikilink"):
  - [八田與一](../Page/八田與一.md "wikilink"):[烏山頭水庫的建造者](../Page/烏山頭水庫.md "wikilink")，有「[嘉南大圳之父](../Page/嘉南大圳.md "wikilink")」之稱
  - [許世賢](../Page/許世賢.md "wikilink"):臺灣第一位女博士、女性縣市長，前[嘉義市長](../Page/嘉義市.md "wikilink")
  - [俞大維](../Page/俞大維.md "wikilink"):前[國防部長](../Page/中華民國國防部.md "wikilink")
  - [彭婉如](../Page/彭婉如.md "wikilink"):臺灣[婦女運動重要參與者](../Page/婦女運動.md "wikilink")，死於命案，至今是臺灣尚未偵破的重大刑案之一。
  - [黃真生](../Page/黃真生.md "wikilink"):
  - [游錫堃](../Page/游錫堃.md "wikilink"):前宜蘭縣長、行政院長
  - [楊日松](../Page/楊日松.md "wikilink"):[法醫](../Page/法醫.md "wikilink")，被譽為「台灣[福爾摩斯](../Page/福爾摩斯.md "wikilink")」
  - [簡錦忠](../Page/簡錦忠.md "wikilink"):
  - [漢寶德](../Page/漢寶德.md "wikilink"):臺灣知名建築教育學者
  - [魏火曜](../Page/魏火曜.md "wikilink"):台灣醫學家，前[台大醫院院長](../Page/台大醫院.md "wikilink")、前[台大醫學院院長](../Page/台大醫學院.md "wikilink")
  - [蔣經國](../Page/蔣經國.md "wikilink"):前行政院長、中華民國總統

### 美麗

  - [林懷民](../Page/林懷民.md "wikilink"):[雲門舞集創辦人](../Page/雲門舞集.md "wikilink")
  - [黃土水](../Page/黃土水.md "wikilink"):雕刻家
  - [鹿野忠雄](../Page/鹿野忠雄.md "wikilink"):台灣日治時期的動物學家、人類學家
  - [白先勇](../Page/白先勇.md "wikilink"):作家
  - [余光中](../Page/余光中.md "wikilink"):作家
  - [朱南子](../Page/朱南子.md "wikilink"):
  - [江文也](../Page/江文也.md "wikilink"):台灣作曲家
  - [凌 波](../Page/凌波.md "wikilink"):香港著名電影演員
  - [楊三郎](../Page/楊三郎.md "wikilink"):
  - [李泰祥](../Page/李泰祥.md "wikilink"):台灣音樂家、國家文藝獎得主
  - [呂泉生](../Page/呂泉生.md "wikilink"):「台灣合唱之父」
  - [侯孝賢](../Page/侯孝賢.md "wikilink"):導演
  - [吳清友](../Page/吳清友.md "wikilink"):[誠品書店創辦人](../Page/誠品書店.md "wikilink")
  - [潘 冀](../Page/潘冀.md "wikilink"):建築師
  - [胡金銓](../Page/胡金銓.md "wikilink"):導演
  - [夏鑄九](../Page/夏鑄九.md "wikilink"):台灣建築學家
  - [張繼高](../Page/張繼高.md "wikilink"):
  - [劉其偉](../Page/劉其偉.md "wikilink"):知名畫家，有畫壇老頑童之稱
  - [張愛玲](../Page/張愛玲.md "wikilink"):作家
  - [楊 牧](../Page/楊牧.md "wikilink"):台灣著名詩人及散文作家

### 自重

  - [林獻堂](../Page/林獻堂.md "wikilink"):[台灣日治時期反殖民運動的領導人之一](../Page/台灣日治時期.md "wikilink")，有「台灣議會之父」之譽
  - [陳錦煌](../Page/陳錦煌.md "wikilink"):知名醫師、前[政務委員](../Page/政務委員.md "wikilink")
  - [陳來紅](../Page/陳來紅.md "wikilink"):
  - [陳正然](../Page/陳正然.md "wikilink"):
  - [嘉邑行善團](../Page/嘉邑行善團.md "wikilink"):於1965年在[嘉義成立的社會公益性團體](../Page/嘉義市.md "wikilink")。
  - [陶百川](../Page/陶百川.md "wikilink"):前[監察委員](../Page/監察委員.md "wikilink")、前總統府[國策顧問](../Page/國策顧問.md "wikilink")
  - [南方朔](../Page/南方朔.md "wikilink"):知名作家、詩人及政治評論家
  - [莫那魯道](../Page/莫那魯道.md "wikilink"):[霧社事件的領導人](../Page/霧社事件.md "wikilink")
  - [陳泰然](../Page/陳泰然.md "wikilink"):
  - [柏 楊](../Page/柏楊.md "wikilink"):知名作家、政治受難者
  - [陳志梧](../Page/陳志梧.md "wikilink"):
  - [林敏生](../Page/林敏生.md "wikilink"):
  - [陳其南](../Page/陳其南.md "wikilink"):前[文建會主委](../Page/文建會.md "wikilink")
  - [楊 逵](../Page/楊逵.md "wikilink"):臺灣著名小說家
  - [高玉樹](../Page/高玉樹.md "wikilink"):前[台北市長](../Page/台北市.md "wikilink")、[交通部長](../Page/中華民國交通部.md "wikilink")
  - [帝瓦伊‧撒耘](../Page/李來旺.md "wikilink"):
  - [許子秋](../Page/許子秋.md "wikilink"):
  - [林強](../Page/林強.md "wikilink"):台灣歌手、演員
  - [印順法師](../Page/印順法師.md "wikilink"):中國近代著名的佛教思想家
  - [殷之浩](../Page/殷之浩.md "wikilink"):台灣企業家、前高鐵董事長[殷琪之父](../Page/殷琪.md "wikilink")
  - [拓拔斯‧塔瑪匹瑪](../Page/田雅各.md "wikilink"):台灣醫師、作家，出身[布農族](../Page/布農族.md "wikilink")
  - [陳定信](../Page/陳定信.md "wikilink"):前台大醫學院院長、中央研究院院士
  - [李登輝](../Page/李登輝.md "wikilink"):前中華民國副總統、總統
  - [龍應台](../Page/龍應台.md "wikilink"):作家、前文建會主委、首任[文化部長](../Page/中華民國文化部.md "wikilink")

### 豐富

  - [紅葉少棒隊](../Page/紅葉少棒隊.md "wikilink"):
  - [梁實秋](../Page/梁實秋.md "wikilink"):中國近代著名的[散文家](../Page/散文.md "wikilink")、學者和[翻譯家](../Page/翻譯.md "wikilink")
  - [顧正秋](../Page/顧正秋.md "wikilink"):著名京劇演員、[國家文藝獎得主](../Page/國家文藝獎.md "wikilink")
  - [歐陽漪棻](../Page/歐陽漪棻.md "wikilink"):[中華民國空軍軍官](../Page/中華民國空軍.md "wikilink")、傳教士
  - [姚一葦](../Page/姚一葦.md "wikilink"):台灣著名劇作家、評論家、翻譯家與美學評論家
  - [高
    陽](../Page/高陽.md "wikilink"):著名作家、[小說家](../Page/小說.md "wikilink")、[紅學家](../Page/紅學.md "wikilink")
  - [瓊 瑤](../Page/瓊瑤.md "wikilink"):著名的小說作家、影視製作人。
  - [王井泉](../Page/王井泉.md "wikilink"):台灣日治時期文學家
  - [賴 和](../Page/賴和.md "wikilink"):作家、有「台灣現代文學之父」之譽
  - [漢聲雜誌](../Page/漢聲雜誌.md "wikilink"):
  - [黃俊雄](../Page/黃俊雄.md "wikilink"):[布袋戲操偶藝師](../Page/布袋戲.md "wikilink")
    、國家文藝獎得主
  - [林清富](../Page/林清富.md "wikilink"):
  - [連
    橫](../Page/連橫_\(人名\).md "wikilink"):《[台灣通史](../Page/台灣通史.md "wikilink")》作者、前副總統[連戰之祖父](../Page/連戰.md "wikilink")
  - [高清愿](../Page/高清愿.md "wikilink"):[統一企業創辦人](../Page/統一企業.md "wikilink")
  - [紀 政](../Page/紀政.md "wikilink"):著名的女性運動員、有「飛躍羚羊」之稱
  - [王日昇](../Page/王日昇.md "wikilink"):臺灣知名歌手、廣播[DJ](../Page/唱片騎師.md "wikilink")
  - [林衡道](../Page/林衡道.md "wikilink"):台灣著名歷史學家、出身[板橋林家](../Page/板橋林家.md "wikilink")
  - [傅培梅](../Page/傅培梅.md "wikilink"):台灣的知名廚師、烹飪節目製作人及主持人
  - [許文龍](../Page/許文龍.md "wikilink"):[奇美集團創辦人](../Page/奇美集團.md "wikilink")
  - [莊永明](../Page/莊永明.md "wikilink"):
  - [林海音](../Page/林海音.md "wikilink"):知名女性作家
  - [楊 弦](../Page/楊弦.md "wikilink"):台灣民謠歌手
  - [成舍我](../Page/成舍我.md "wikilink"):中國著名報人、教育家
  - [鄧麗君](../Page/鄧麗君.md "wikilink"):華人世界的著名女歌手
  - [許常惠](../Page/許常惠.md "wikilink"):台灣知名的音樂家及教育家
  - [張小燕](../Page/張小燕_\(1948年\).md "wikilink"):知名綜藝節目主持人
  - [張照堂](../Page/張照堂.md "wikilink"):知名攝影家，[金馬獎](../Page/金馬獎.md "wikilink")、[金鐘獎](../Page/金鐘獎.md "wikilink")、國家文藝獎得主
  - [楊麗花](../Page/楊麗花.md "wikilink"):台灣的知名[歌仔戲藝人](../Page/歌仔戲.md "wikilink")

### 共生

  - [證嚴法師](../Page/證嚴法師.md "wikilink"):[慈濟功德會創辦人](../Page/慈濟功德會.md "wikilink")
  - [鳥居龍藏](../Page/鳥居龍藏.md "wikilink"):台灣日治時期的考古學家、人類學家
  - [曹 瑾](../Page/曹瑾.md "wikilink"):清朝鳳山知縣、淡水同知，在臺期間有不少政績
  - [馬偕博士](../Page/馬偕.md "wikilink"):台灣清治時期的醫師、傳教士
  - [孫 越](../Page/孫越_\(台灣演員\).md "wikilink"):台灣知名演員
  - [張隆盛](../Page/張隆盛.md "wikilink"):
  - [史溫侯](../Page/史溫侯.md "wikilink"):台灣清治時期的[英國駐](../Page/英國.md "wikilink")[打狗領事](../Page/打狗.md "wikilink")
  - [崔 玖](../Page/崔玖.md "wikilink"):
  - [杜聰明](../Page/杜聰明.md "wikilink"):台灣史上首位醫學博士，[高雄醫學院](../Page/高雄醫學院.md "wikilink")（今高雄醫學大學）創辦人
  - [施明德](../Page/施明德.md "wikilink"):台灣戰後黨外領袖之一，前民進黨主席、政治受難者
  - [蘭大弼](../Page/蘭大弼.md "wikilink"):台灣著名的醫師、傳教士
  - [劉小如](../Page/劉小如.md "wikilink"):
  - [林義雄](../Page/林義雄.md "wikilink"):台灣戰後黨外領袖之一，前民進黨主席、政治受難者
  - [黑幼龍](../Page/黑幼龍.md "wikilink"):[卡內基訓練](../Page/戴爾·卡內基.md "wikilink")[大中華地區負責人](../Page/大中華地區.md "wikilink")
  - [陳映真](../Page/陳映真.md "wikilink"):作家
  - [胡台麗](../Page/胡台麗.md "wikilink"):
  - [陳玉峰](../Page/陳玉峰.md "wikilink"):
  - [黃春明](../Page/黃春明.md "wikilink"):台灣著名的文學作家、國家文藝獎得主
  - [媽 祖](../Page/媽祖.md "wikilink"):
  - [陳五福](../Page/陳五福.md "wikilink"):台灣著名眼科醫師、社會工作者，有「[噶瑪蘭的燭光](../Page/噶瑪蘭.md "wikilink")」之稱
  - [李遠哲](../Page/李遠哲.md "wikilink"):前[中央研究院院長](../Page/中央研究院.md "wikilink")、[諾貝爾獎得主](../Page/諾貝爾獎.md "wikilink")

## 參考文獻

[category:台灣人列表](../Page/category:台灣人列表.md "wikilink")

[Category:台灣文化](../Category/台灣文化.md "wikilink")
[人](../Category/臺灣中文書籍.md "wikilink")
[Category:台灣歷史書籍](../Category/台灣歷史書籍.md "wikilink")
[Category:2000年台灣](../Category/2000年台灣.md "wikilink")
[影響200](../Category/影響200.md "wikilink")

1.  《影響200》，天下雜誌出版，2006年，第2-8頁