[Trinidad_pitch_lake_ENG.png](https://zh.wikipedia.org/wiki/File:Trinidad_pitch_lake_ENG.png "fig:Trinidad_pitch_lake_ENG.png")

**彼奇湖**(**Pitch
Lake**)是一個奇特的[瀝青湖泊](../Page/瀝青.md "wikilink")，该湖泊位於[加勒比海的](../Page/加勒比海.md "wikilink")[特立尼达岛的西南方](../Page/特立尼达岛.md "wikilink")。這個湖面積大約有44
[公頃](../Page/公頃.md "wikilink")，報告寫說是75
[公尺深](../Page/公尺.md "wikilink")，但是鑽到90\~100公尺深仍然都是瀝青，所以深度沒人確定，湖中沥青存量達1200万[公噸](../Page/公噸.md "wikilink")。彼奇湖被認為為[旅遊勝地](../Page/旅遊勝地.md "wikilink")，每年吸引大約20,000位參觀者。它已經被比作"*壞停車場*"並且已經被叫為"*在[加勒比海裡的最醜陋的旅遊勝地](../Page/加勒比海.md "wikilink")*"。它也開採出最優質的道路建設瀝青。

彼奇湖的起源是與深[斷層有關](../Page/斷層.md "wikilink")
[加勒比板塊在下面的](../Page/加勒比板塊.md "wikilink")[隱沒帶與巴貝多弧有關](../Page/隱沒帶.md "wikilink")。這個湖沒被廣泛研究，但是據說這個湖是在兩個斷層的交界，讓沉積在深層的瀝青被擠壓上來。

在瀝青上活躍的細菌會在低壓瀝青裡產生石油。

## 記錄

## 外部連結

  - [千里達的彼奇湖](http://www.richard-seaman.com/Travel/TrinidadAndTobago/Trinidad/PitchLake/)在
    [www.richard-seaman.com](http://www.richard-seaman.com), 很好的照片。

[Category:千里達與托巴哥地理](../Category/千里達與托巴哥地理.md "wikilink")
[Category:沥青湖](../Category/沥青湖.md "wikilink")