**彼得·菲利普·凯里** （**Peter Philip
Carey**，），澳大利亚小说家。他是仅有的三位两次获得[布克奖的作家之一](../Page/布克奖.md "wikilink")（另兩位是[约翰·马克斯维尔·库切和](../Page/约翰·马克斯维尔·库切.md "wikilink")[希拉里·曼特爾](../Page/希拉里·曼特爾.md "wikilink")）。他也与人合作创作了[电影](../Page/电影.md "wikilink")[剧本](../Page/剧本.md "wikilink")《[直到世界尽头](../Page/直到世界尽头.md "wikilink")》。他目前是[纽约市立大学](../Page/纽约市立大学.md "wikilink")[亨特学院](../Page/亨特学院.md "wikilink")“Master
of Fine Arts in Creative Writing
program”项目的常务理事。主要作品有《[奥斯卡与露辛达](../Page/奥斯卡与露辛达.md "wikilink")》、《[凯利帮真史](../Page/凯利帮真史.md "wikilink")》等。

## 生平

彼得·凯利生于澳大利亚[维多利亚州](../Page/维多利亚州.md "wikilink")[贝处斯·马什](../Page/贝处斯·马什.md "wikilink")，双亲是汽车商。1948-1953年凯利在当地学校上学，毕业之前就转学到同州考瑞奥的吉隆语法学校读书。1961年，他被墨尔本的蒙纳士大学录取，学习化学和动物学，但因一次交通事故导致学业终端。

1962年-1967年他为墨尔本的很多广告公司工作过，期间结识了作家[巴里·奥克雷和](../Page/巴里·奥克雷.md "wikilink")[莫里斯·劳瑞](../Page/莫里斯·劳瑞.md "wikilink"),他们把最新的欧美小说介绍给他阅读。1964年他和Leigh
Weetman结婚。这一段时间里，他广泛的阅读，特别是[詹姆斯·乔伊斯](../Page/詹姆斯·乔伊斯.md "wikilink"),[塞缪尔·贝克特](../Page/塞缪尔·贝克特.md "wikilink")，[弗兰茨·卡夫卡和](../Page/弗兰茨·卡夫卡.md "wikilink")[威廉·福克纳的作品](../Page/威廉·福克纳.md "wikilink")，并于1964年开始自己的创作。1968年，他已经积累了一些未发表的手稿。60年代末，他到欧洲和中东旅行，1968年到达伦敦，继续自己的广告事业。1970年，他回到了澳大利亚，在墨尔本和悉尼从事广告行业。

1974年凯里发表了他的第一部短篇小说集《[历史上的胖子](../Page/历史上的胖子.md "wikilink")》(The Fat Man
In
History)。他的朋友[莫里斯·劳瑞在](../Page/莫里斯·劳瑞.md "wikilink")《[国家评论](../Page/国家评论.md "wikilink")》上为这篇小说集写了第一篇评论。同年他和妻子离婚，前往Balmain为格雷广告公司工作。1976年凯里前往昆士兰岛，打算在那里写作三周，然后在悉尼工作一周。这一段时间他完成了后来收入短篇小说集《战争罪恶》里面的大部分作品和他的第一部长篇小说《[幸福](../Page/幸福.md "wikilink")》（bliss）。1980年凯里自己開設广告公司。

## 作品

### 小说

  - *[歷史上的胖子](../Page/歷史上的胖子.md "wikilink")* (1974) 英文書名：The Fat Man in
    History.是作家第一部短篇小說集。
  - *[戰爭罪行](../Page/戰爭罪行.md "wikilink")* (1979) 英文書名：War Crimes:Short
    Stories.是作家第二部短篇小說集。
  - *[幸福](../Page/幸福.md "wikilink")* (1981) 英文書名：Bliss.是作家第一部長篇小說。
  - *[魔術師](../Page/魔術師.md "wikilink")* (1985) 英文書名：Illywhacker.
  - *[奥斯卡和露辛达](../Page/奥斯卡和露辛达.md "wikilink")* (1988) 英文書名： Oscar and
    Lucinda.
  - *[税务审查员](../Page/税务审查员.md "wikilink")* (1991) 英文書名：The Tax
    Inspector.
  - *[崔森‧史密斯不凡的生活](../Page/崔森‧史密斯不凡的生活.md "wikilink")* (1994) 英文書名：The
    Unusual Life of Tristan Smith.
  - *[短篇小說全集](../Page/短篇小說全集.md "wikilink")* (1994) 英文書名：Collected
    Stories.收錄作家過往所有的短篇小說。
  - *[黑獄來的陌生人](../Page/黑獄來的陌生人.md "wikilink")* (1997) 英文書名：Jack Maggs.
  - *[凯利帮真史](../Page/凯利帮真史.md "wikilink")* (2000) 英文書名：True History of
    the Kelly Gang.
  - *[我虛假的生活](../Page/我虛假的生活.md "wikilink")* (2003) 英文書名：My Life as a
    Fake.
  - *[偷窃，真实的故事](../Page/偷窃，真实的故事.md "wikilink")* (2006)

## 作品在台灣的出版

在台灣，譯作「[彼得·凯瑞](../Page/彼得·凯瑞.md "wikilink")」。

在出版方面，有以下的作品中譯發行：

  - 林尹星/譯，《奧斯卡與露辛達》，台北：允晨，1999年。
  - 彭倩文/譯，《黑獄來的陌生人》，台北：皇冠，1999年。
  - 李宛蓉/譯，《雪梨三十天》，台北：馬可孛羅，2005年。
  - 孟祥森/譯，《凱利幫》，台北：皇冠，2005年。

在研究方面，有：

  - 羅文君，〈重塑澳洲歷史：論彼得凱瑞《奧斯卡與露辛達》中的移民者身分認同和時空性〉，中興大學外國語文學系所碩士論文，2008。

在引介方面，到2011/3/30為止，使用「台灣期刊論文索引系統」僅找到一篇相關文章：鄭樹森/策畫、趙明、李志良/譯〈袋鼠原鄉—澳洲小說家Peter
Carey彼德‧凱里〉，發表在1991年4月號的《聯合文學》。

## 參考資料

  - 宋國誠，《從邊緣到中心：後殖民文學》，擎松出版。2004年12月初版一刷。

[Category:布克獎獲獎者](../Category/布克獎獲獎者.md "wikilink")
[Category:澳大利亚小说家](../Category/澳大利亚小说家.md "wikilink")
[Category:在美國的澳大利亞人](../Category/在美國的澳大利亞人.md "wikilink")
[Category:維多利亞州人](../Category/維多利亞州人.md "wikilink")
[Category:蒙纳士大学校友](../Category/蒙纳士大学校友.md "wikilink")