**彼得·呂姆克爾夫**（，），[德國著名戰後作家](../Page/德國.md "wikilink")。曾使用的筆名有：Leo
Doletzki、Leslie Maier、Johannes Fontara、Lyng、John Frieder、Hans-Werner
Weber、Harry Flieder、與Hans Hingst。

呂姆克爾夫1952年開始寫作，至1956年間固定於[漢堡的](../Page/漢堡.md "wikilink")「Zwischen den
Kriegen
（戰爭之間）」雜誌發表文章，與[威爾納·黎格爾](../Page/威爾納·黎格爾.md "wikilink")（）、[阿諾·施密特](../Page/阿諾·施密特.md "wikilink")（）與[庫爾特·希勒](../Page/庫爾特·希勒.md "wikilink")（），共同成為「Studentenkurier」（即今「konkret」）政論月刊1955年創刊的創刊作家。

## 主要獎項

  - 1979年，Erich Kästner Preis für Literatur der Erich Kästner
    Gesellschaft
  - 1986年，Arno Schmidt Preis
  - 1988年，Heinrich Heine Preis des Ministeriums für Kultur der DDR
  - 1993年，Georg Büchner Preis

[Category:德國作家](../Category/德國作家.md "wikilink")
[Category:德國詩人](../Category/德國詩人.md "wikilink")
[Category:德語作家](../Category/德語作家.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")