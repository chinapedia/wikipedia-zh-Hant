**彼得·葛雷格·阿奈特**（，）出生於[紐西蘭](../Page/紐西蘭.md "wikilink")，是紐西蘭裔[美國籍的](../Page/美國.md "wikilink")[戰地記者](../Page/記者.md "wikilink")。

他在1960年代[越戰起就開始採訪戰地新聞](../Page/越戰.md "wikilink")，因此在1966年得到[普立茲獎](../Page/普立茲獎.md "wikilink")；也曾在[八二三砲戰期間到](../Page/八二三砲戰.md "wikilink")[中華民國所管轄的](../Page/中華民國.md "wikilink")[金門及](../Page/金門.md "wikilink")[馬祖採訪](../Page/馬祖.md "wikilink")，當時是[美聯社所屬的記者](../Page/美聯社.md "wikilink")。

1983年時，「彼得·阿奈特」到[中美洲採訪](../Page/中美洲.md "wikilink")[聖地牙哥暴動及](../Page/聖地牙哥.md "wikilink")[薩爾瓦多暴動](../Page/薩爾瓦多.md "wikilink")。從此他從文字記者轉變為在[攝影機前報導的記者](../Page/攝影機.md "wikilink")。

在1990年的[波斯灣戰爭期間](../Page/波斯灣戰爭.md "wikilink")，彼得·阿奈特為美國的[有線電視新聞網](../Page/有線電視新聞網.md "wikilink")（CNN）採訪，而進入[伊拉克](../Page/伊拉克.md "wikilink")，並且成功訪問到伊拉克總統[海珊](../Page/海珊.md "wikilink")。但也因為採訪美軍轟炸過，宣稱是一座化學武器工廠的奶粉工廠，而引起美國國內的反彈。

1998年，他在有線電視新聞網（[CNN](../Page/CNN.md "wikilink")）及《[時代雜誌](../Page/時代雜誌.md "wikilink")》聯合製作的「*NewsStand*」節目中，揭發1970年代[越南戰爭期間](../Page/越南戰爭.md "wikilink")[美軍曾在](../Page/美軍.md "wikilink")[寮國的](../Page/寮國.md "wikilink")“”（Operation
Tailwind）中對[北越使用](../Page/北越.md "wikilink")[沙林毒氣](../Page/沙林.md "wikilink")，因此美國國防部向[有線電視新聞網](../Page/有線電視新聞網.md "wikilink")（CNN）施壓開除彼得·阿奈特，雖然CNN並未開除他，但之後他就轉往[NBC任職](../Page/NBC.md "wikilink")。（2013年7月14日，美國HBO電視劇[新闻编辑室將相關事件溶入第](../Page/新闻编辑室.md "wikilink")2季劇情中）

2003年，彼得·阿奈特為美國[國家廣播公司](../Page/國家廣播公司.md "wikilink")（NBC）、[MSNBC及](../Page/MSNBC.md "wikilink")[國家地理頻道進入伊拉克採訪](../Page/國家地理頻道.md "wikilink")[伊拉克戰爭](../Page/伊拉克戰爭.md "wikilink")，但因為接受[伊拉克國營電視台的專訪](../Page/伊拉克國營電視台.md "wikilink")，直言美軍的第一次軍事行動將會有挫折，而遭[國家廣播公司](../Page/國家廣播公司.md "wikilink")（NBC）及[國家地理頻道開除](../Page/國家地理頻道.md "wikilink")，但隨後馬上有[台灣的](../Page/台灣.md "wikilink")[TVBS](../Page/TVBS.md "wikilink")、英國《[鏡報](../Page/鏡報.md "wikilink")》、[比利時民營電視網等多國新聞媒體與他簽約持續採訪報導伊拉克戰爭的](../Page/比利時.md "wikilink")[新聞](../Page/新聞.md "wikilink")。

## 著作

  - *Live from the Battlefield: From Vietnam to Baghdad, 35 Years in the
    World's War Zones.*（1994年）（ISBN 0-68-4800365 ； ISBN
    978-068-480-036-3）
      - 《我從戰場歸來 : 一位戰地記者的回憶》中文版：譚天
        翻譯，麥田出版社出版，台灣[台北](../Page/台北.md "wikilink")，1996年（ISBN
        957-708-373-0 ； ISBN 978-957-708-373-9）。

## 外部連結

  - [台灣TVBS電視台在伊拉克戰爭期間與彼得·阿奈特簽約報導新聞](https://web.archive.org/web/20051218215826/http://www.gclub.com.tw/news/news_list.asp?no=kenneth20030407164632)
  - [CNN interview with Arnett, looking back at Operation Desert
    Storm](http://www.cnn.com/COMMUNITY/transcripts/2001/01/16/arnett/)
  - [Arnett on his Osama bin Laden
    interview](http://www.cnn.com/2001/COMMUNITY/12/05/gen.arnett.cnna/)
  - [National Geographic Fires Peter
    Arnett](http://news.nationalgeographic.com/news/2003/03/0331_030331_arnettfired.html)
  - [CNN transcript of Arnett's controversial remarks made
    in 2003](http://edition.cnn.com/2003/WORLD/meast/03/30/sprj.irq.arnett.transcript/)
  - [Try Arnett for treason, senator
    says](http://enquirer.com/editions/2003/04/02/loc_rail.bunning02.html)
    - article in the Cincinnati Enquirer
  - [Fired CNN journalist on dismissal of
    Arnett](http://www.wsws.org/articles/1999/apr1999/oliv-a22.shtml)
  - [CNN's "Tailwind" and Selective Media
    Retractions](https://web.archive.org/web/20051117220143/http://www.fair.org/extra/9808/tailwind.html)

[A](../Category/紐西蘭人.md "wikilink")
[Category:美國戰地記者](../Category/美國戰地記者.md "wikilink")
[Category:越南战争战地记者](../Category/越南战争战地记者.md "wikilink")