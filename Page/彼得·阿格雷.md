**彼得·阿格雷**（，），[美国医生和分子生物学家](../Page/美国.md "wikilink")。因对[细胞膜中的水通道的发现以及对离子通道的研究](../Page/细胞膜.md "wikilink")，与[罗德里克·麦金农一起获得了](../Page/罗德里克·麦金农.md "wikilink")2003年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")\[1\]。阿格雷早年毕业于，后进入[约翰·霍普金斯大学并获得医学博士学位](../Page/约翰·霍普金斯大学.md "wikilink")。阿格雷现任约翰·霍普金斯大学布隆伯格学院讲席教授及约翰·霍普金斯疟疾研究所（JHMRI）所长。

## 參見

  - [沼正作](../Page/沼正作.md "wikilink")
  - [罗德里克·麦金农](../Page/罗德里克·麦金农.md "wikilink")

## 参考资料

  - [诺贝尔官方网站彼得·阿格雷自传](http://nobelprize.org/nobel_prizes/chemistry/laureates/2003/agre-autobio.html)


[Category:美国化学家](../Category/美国化学家.md "wikilink")
[Category:美國醫學家](../Category/美國醫學家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:約翰霍普金斯大學教師](../Category/約翰霍普金斯大學教師.md "wikilink")
[Category:杜克大学教师](../Category/杜克大学教师.md "wikilink")
[Category:约翰·霍普金斯大学校友](../Category/约翰·霍普金斯大学校友.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:美國信義宗教徒](../Category/美國信義宗教徒.md "wikilink")
[Category:挪威裔美國人](../Category/挪威裔美國人.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:明尼蘇達州人](../Category/明尼蘇達州人.md "wikilink")
[Category:杰出鹰级童军奖得主](../Category/杰出鹰级童军奖得主.md "wikilink")

1.  <http://www.ch.ntu.edu.tw/nobel/2003.html>