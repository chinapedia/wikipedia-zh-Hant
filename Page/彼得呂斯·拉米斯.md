[Petrus_Ramus.jpg](https://zh.wikipedia.org/wiki/File:Petrus_Ramus.jpg "fig:Petrus_Ramus.jpg")
**彼得呂斯·拉米斯**（，）又名**皮埃爾·德拉拉梅**（）是[法國男](../Page/法國.md "wikilink")[人文主義學家](../Page/人文主義.md "wikilink")、[邏輯學家](../Page/邏輯.md "wikilink")、[哲學家](../Page/哲學家.md "wikilink")、[教育改革者](../Page/教育.md "wikilink")。他在1572年發生的[圣巴泰勒米大屠杀中被殺](../Page/圣巴泰勒米大屠杀.md "wikilink")。

## 傳記

彼得呂斯·拉米斯在[法國](../Page/法國.md "wikilink")[皮卡第大區出生](../Page/皮卡第大區.md "wikilink")，[貴族出身](../Page/貴族.md "wikilink")，但家境貧困。他[父親是](../Page/父親.md "wikilink")[農夫](../Page/農夫.md "wikilink")，曾祖父是燒[木炭工人](../Page/木炭.md "wikilink")。拉米斯在12歲時到[納瓦拉書院](../Page/納瓦拉書院.md "wikilink")（Collège
de
Navarre）當富裕[學生的](../Page/學生.md "wikilink")[僕人](../Page/僕人.md "wikilink")，雙手沒有停過，直至[夜晚才讀書](../Page/夜晚.md "wikilink")。

## 著作

[La_Ramée_-_Arithmeticae_libri_tres,_1557_-_106004.jpg](https://zh.wikipedia.org/wiki/File:La_Ramée_-_Arithmeticae_libri_tres,_1557_-_106004.jpg "fig:La_Ramée_-_Arithmeticae_libri_tres,_1557_-_106004.jpg")

  - 《Advertissement sur la réformation de l'université de Paris, au Roy,
    Paris》（1562年）
  - 《Aristotelicae Animadversiones》（1543年）
  - 《Arithmétique》（1555年）
  - 《Brutinae questiones》（1547年）
  - 《Commentariorum de religione
    christiana》（1576年在[法蘭克福出版](../Page/法蘭克福.md "wikilink")）
  - 《De moribus veterum
    Gallorum》（1559年在[巴黎初版](../Page/巴黎.md "wikilink")，1572年在[巴塞爾再版](../Page/巴塞爾.md "wikilink")）
  - 《De militia C.J. Cæsaris》
  - 《Dialectique》（1550年初版，1556年修訂重印）
  - 《Rhetoricae distinctiones in Quintilianum》（1549年）
  - 《Scolae physicae, metaphysicae, mathematicae》（1565年、1566年、1578年）
  - 《Three grammars: Grammatica latina》（1548年）
  - 《Three grammars:Grammatica Graeca》（1560年）
  - 《Three grammars:Grammaire Française》（1562年）

[category:法國哲學家](../Page/category:法國哲學家.md "wikilink")
[category:在法國被謀殺身亡者](../Page/category:在法國被謀殺身亡者.md "wikilink")

[Category:16世紀哲學家](../Category/16世紀哲學家.md "wikilink")