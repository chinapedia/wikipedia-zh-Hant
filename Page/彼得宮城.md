[Petergof_city_coa.png](https://zh.wikipedia.org/wiki/File:Petergof_city_coa.png "fig:Petergof_city_coa.png")
[PeterhofGrandCascade.JPG](https://zh.wikipedia.org/wiki/File:PeterhofGrandCascade.JPG "fig:PeterhofGrandCascade.JPG")\]\]
[Peterhof_Peter\&Pavel_temple.jpg](https://zh.wikipedia.org/wiki/File:Peterhof_Peter&Pavel_temple.jpg "fig:Peterhof_Peter&Pavel_temple.jpg")
[New_Peterhof_station_4.JPG](https://zh.wikipedia.org/wiki/File:New_Peterhof_station_4.JPG "fig:New_Peterhof_station_4.JPG")
[Peterhof_russia_01.jpg](https://zh.wikipedia.org/wiki/File:Peterhof_russia_01.jpg "fig:Peterhof_russia_01.jpg")
[Stariy_Peterhof_panorama.JPG](https://zh.wikipedia.org/wiki/File:Stariy_Peterhof_panorama.JPG "fig:Stariy_Peterhof_panorama.JPG")
**彼得宮城**（）是[俄羅斯](../Page/俄羅斯.md "wikilink")[聖彼得堡的](../Page/聖彼得堡.md "wikilink")[彼得宮区下轄的一個城市](../Page/彼得宮区.md "wikilink")，在[德語中意為](../Page/德語.md "wikilink")「彼得的宮殿」。彼得宮城位於聖彼得堡市中心以西約29km，北臨[芬蘭灣](../Page/芬蘭灣.md "wikilink")。2002年人口64,791人。

以噴泉景觀著名的[彼得霍夫宮位於本市](../Page/彼得霍夫宮.md "wikilink")。[聖彼得堡國立大學兩個校區之一亦位於本市](../Page/聖彼得堡國立大學.md "wikilink")。

## 歷史

1705年奉[彼得一世之命建城](../Page/彼得一世.md "wikilink")。1721年建有大理石加工廠，1801年建有寶石精加工廠，1932年建有建築石材廠。之後又建成了鐘錶工廠，以「Rocket」為品牌出貨。

[第二次世界大戰中的](../Page/第二次世界大戰.md "wikilink")1944年1月27日，德語的市名**Peterhof**被改成了**Petrodvoréts**（），[蘇聯解體之後依然如故](../Page/蘇聯.md "wikilink")。直至1997年恢復了原名**Peterhof**。

由於戰時成爲了前線，城市被毀壞。直至1990年代才結束復興。

## 經濟

現在市內大部份勞動者均為聖彼得堡市內的企業或組織的職工。

## 旅遊觀光

以下兩處均以「[聖彼得堡歷史中心及其週邊建築群](../Page/聖彼得堡歷史中心及其週邊建築群.md "wikilink")」登記成為[世界遺産](../Page/世界遺産.md "wikilink")。

### 彼得夏宮及其庭園

[彼得夏宮及其噴泉庭園為俄羅斯代表的觀光地之一](../Page/彼得夏宮.md "wikilink")。奉彼得一世之命，以當時最先進的技術建設而成，可發現[凡爾賽宮的影響](../Page/凡爾賽宮.md "wikilink")。

### 歷史地区

彼得宮城市內的留有大量歷史建築物。其代表如下：

  - \- 1904年竣工的教堂，高60m。從彼得夏宮的「上庭園」入口徒步走5分鐘可到。1935年關閉，1985年至1990年間修復完工。

  - \- 以1857年竣工的房屋為基礎的火車站。

## 科学・教育

聖彼得堡國立大學為中心的科学教育中心內，設有海軍無線技術研究所等。2005年7月23日，被認定為[俄羅斯聯邦科学都市中的首位](../Page/俄羅斯聯邦科学都市.md "wikilink")。

## 交通

### 航路

有[水翼船在](../Page/水翼船.md "wikilink")[埃爾米塔日博物館附近的碼頭及彼得夏宮](../Page/埃爾米塔日博物館.md "wikilink")「下庭園」的碼頭之間往返。航程35分鐘。僅限於[涅瓦河不結冰的時間](../Page/涅瓦河.md "wikilink")（約為5月至10月間）運行。因為「下庭園」需要購買門票，所以在此下船之後需要交付門票錢。

### 鐵路

市內有波羅地線（Baltic line）的、、三個火車站。

## 姐妹城市

  - [勒布朗-梅尼爾](../Page/勒布朗-梅尼爾.md "wikilink")

## 外部連結

  - [彼得宮城政府網站（俄語）](http://www.peterhof.ru/)
  - [QTVR提供的全景照片](http://www.360pano.eu/peterhof/)
  - [Wikimapia提供的地圖](http://www.wikimapia.org/#y=59890000&x=29900000&z=13&l=1&m=s&v=2)

[П](../Category/聖彼得堡下轄市鎮.md "wikilink")