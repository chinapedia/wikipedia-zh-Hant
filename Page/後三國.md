**後三國**（892年－936年）是[朝鮮歷史上的時代劃分之一](../Page/朝鮮.md "wikilink")，《[高麗史](../Page/高麗史.md "wikilink")》開篇即定後三國之說，後世史家多以[新羅](../Page/新羅.md "wikilink")、[後百濟](../Page/後百濟.md "wikilink")、[泰封](../Page/泰封.md "wikilink")（後高句麗）為後三國。始於892年農民出身的[甄萱起兵](../Page/甄萱.md "wikilink")，同一時期[新羅王族後裔](../Page/新羅.md "wikilink")[弓裔亦建立](../Page/弓裔.md "wikilink")[泰封國導致已衰弱的](../Page/泰封國.md "wikilink")[新羅分裂](../Page/新羅.md "wikilink")，終於936年[高麗再度統一](../Page/高麗_\(918年－1392年\).md "wikilink")[朝鮮半島](../Page/朝鮮半島.md "wikilink")。

## 歷史

新羅[景德王之子](../Page/景德王.md "wikilink")[惠恭王即位後](../Page/惠恭王.md "wikilink")，都城內頻繁發生暴動，780年惠恭王與妃嬪等被殺，[武烈王系血統斷絕](../Page/武烈王.md "wikilink")。之後連續發生篡位事件，宮廷紛亂。王位争奪戰中失敗的王族後裔[金憲昌於](../Page/金憲昌.md "wikilink")[熊川州獨立](../Page/熊川州.md "wikilink")，擁有海上勢力的將領[張保皐亦起兵](../Page/張保皐.md "wikilink")，介入了都城的王位争奪。起事雖為中央軍隊鎮壓，地方上仍發生農民暴動，新羅王朝統治能力逐漸弱化。

九世紀中期，此類暴動頻繁發生，地方豪族多有舉兵，號稱將軍、城主等，脫離新羅統治。其中甄萱、北原的[梁吉及其部下弓裔等為有力的勢力](../Page/梁吉.md "wikilink")。甄萱是[尚州出身的農民](../Page/尚州.md "wikilink")，在西南海建立軍功被升為將領。892年以完山（今[全州](../Page/全州.md "wikilink")）為根據地起兵，攻佔武珍州（今[光州](../Page/光州.md "wikilink")）獨立。後糾集周邊豪族以擴大勢力，900年自稱「後百濟王」，於朝鮮半島西南部建立「[後百濟](../Page/後百濟.md "wikilink")」。

弓裔是新羅王族後裔，891年起兵反新羅，隨梁吉於[江原道等地作戰](../Page/江原道.md "wikilink")。898年建都松岳（[開城](../Page/開城.md "wikilink")），901年稱王，904年立國號[摩震](../Page/摩震.md "wikilink")，911年改國號[泰封](../Page/泰封.md "wikilink")，史稱「[泰封國](../Page/泰封國.md "wikilink")」，中國史書《資治通鑒》中作「大封國」。

如此形成新羅、後百濟、泰封三國鼎立的局面。松岳豪族出身的[王建在弓裔麾下多建軍功](../Page/高麗太祖.md "wikilink")，918年推翻弓裔，以開城為都建立「高麗」並稱王（太祖）。王建繼續合併地方豪族增加勢力，935年吞併新羅，936年攻滅後百濟，重新統一朝鮮半島。

## 大事年表

  - 892年：[甄萱叛亂](../Page/甄萱.md "wikilink")，佔領[光州](../Page/光州.md "wikilink")。[弓裔加入梁吉叛軍](../Page/弓裔.md "wikilink")。
  - 900年：[甄萱稱](../Page/甄萱.md "wikilink")[後百濟王](../Page/後百濟.md "wikilink")。
  - 901年：[弓裔稱](../Page/弓裔.md "wikilink")[後高句麗王](../Page/後高句麗.md "wikilink")。
  - 904年：[弓裔立國號為](../Page/弓裔.md "wikilink")[摩震](../Page/摩震.md "wikilink")。
  - 905年：[弓裔改都](../Page/弓裔.md "wikilink")[弓裔都城](../Page/弓裔都城.md "wikilink")
    ( [鐵原郡](../Page/鐵原郡_\(南\).md "wikilink") ) 。
  - 911年：[弓裔改國號為](../Page/弓裔.md "wikilink")[泰封](../Page/泰封.md "wikilink")。
  - 917年：[朴彦昌稱](../Page/朴彦昌.md "wikilink")[後沙伐王](../Page/後沙伐.md "wikilink")。
  - 918年：[王建起兵反](../Page/高麗太祖.md "wikilink")[弓裔](../Page/弓裔.md "wikilink")，建立[高麗](../Page/高麗_\(918年－1392年\).md "wikilink")，建元[天授](../Page/天授.md "wikilink")。
  - 926年：[契丹攻滅](../Page/契丹.md "wikilink")[渤海國](../Page/渤海國.md "wikilink")，[渤海國](../Page/渤海國.md "wikilink")[太子](../Page/太子.md "wikilink")[大光顯率部下三十餘人歸順](../Page/大光顯.md "wikilink")[王建](../Page/高麗太祖.md "wikilink")。
  - 927年：[甄萱進攻](../Page/甄萱.md "wikilink")[新羅佔領](../Page/新羅.md "wikilink")[慶州](../Page/慶州.md "wikilink")，殺[新羅](../Page/新羅.md "wikilink")[景哀王](../Page/景哀王.md "wikilink")。
  - 929年：[甄萱進攻](../Page/甄萱.md "wikilink")[後沙伐佔領](../Page/後沙伐.md "wikilink")[尙州](../Page/尙州.md "wikilink")，殺[景明王五男](../Page/景明王.md "wikilink")[朴彦昌](../Page/朴彦昌.md "wikilink")。
  - 930年：[高敞之戰](../Page/高敞.md "wikilink")，[王建以責問](../Page/高麗太祖.md "wikilink")[甄萱殺](../Page/甄萱.md "wikilink")[景哀王為名發動戰爭](../Page/景哀王.md "wikilink")。
  - 934年：[洪城之戰](../Page/洪城.md "wikilink")，[後百濟國力大損](../Page/後百濟.md "wikilink")。
  - 935年：[甄萱指定第四子](../Page/甄萱.md "wikilink")[甄金剛為繼承者](../Page/甄金剛.md "wikilink")。[新羅向](../Page/新羅.md "wikilink")[高麗投降](../Page/高麗_\(918年－1392年\).md "wikilink")。
  - 936年3月：[甄萱與其妻](../Page/甄萱.md "wikilink")[古比及女婿](../Page/古比.md "wikilink")[朴英規等逃亡至](../Page/朴英規.md "wikilink")[高麗](../Page/高麗_\(918年－1392年\).md "wikilink")。
  - 936年8月：[黃山之戰](../Page/黃山.md "wikilink")，[王建在](../Page/高麗太祖.md "wikilink")[甄萱幫助下攻滅](../Page/甄萱.md "wikilink")[後百濟](../Page/後百濟.md "wikilink")，處罰叛亂策劃者能奐與[甄萱次子](../Page/甄萱.md "wikilink")[良劍等](../Page/良劍.md "wikilink")。[高麗](../Page/高麗_\(918年－1392年\).md "wikilink")[統一朝鮮半島](../Page/統一朝鮮半島.md "wikilink")。

[Category:朝鲜半岛朝代](../Category/朝鲜半岛朝代.md "wikilink")
[Category:後三國時代](../Category/後三國時代.md "wikilink")
[Category:朝鮮半島歷史](../Category/朝鮮半島歷史.md "wikilink")