**後藤
理沙**（），[日本女性](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。出身於[福岡縣](../Page/福岡縣.md "wikilink")[浮羽郡](../Page/浮羽郡.md "wikilink")[吉井町](../Page/吉井町_\(福岡縣\).md "wikilink")（今[浮羽市](../Page/浮羽市.md "wikilink")），為四姊妹中的-{么}-妹。

## 簡歷

1997年，後藤理沙在[小學館](../Page/小學館.md "wikilink")[青年漫畫雜誌](../Page/青年漫畫.md "wikilink")《Weekly
Young
Sunday》初次亮相。1998年，接拍[寶礦力水特的廣告一砲而紅](../Page/寶礦力水特.md "wikilink")，隨即獲得大量[電視連續劇](../Page/電視連續劇.md "wikilink")、[電影及](../Page/電影.md "wikilink")[廣告的演出](../Page/廣告.md "wikilink")。後藤理沙當時的所屬事務所是[FlaMme](../Page/FlaMme.md "wikilink")，公司將她定位為僅次於[廣末涼子及](../Page/廣末涼子.md "wikilink")[小雪的王牌女星](../Page/加藤小雪.md "wikilink")，但是人氣低迷紅不起來，於2002年宣佈引退。

2004年復出，加入JMO事務所。

2005年出版個人[寫真集](../Page/寫真集.md "wikilink")，但是人氣與定位僅為寫真女郎的程度，已難回電視圈成為一般藝人。

2009年因婚二度退出演藝圈。

2012年復出拍攝[全裸寫真集](../Page/全裸.md "wikilink")《LISA GOTO at NUDE》，但反應遠不如預期。

2013年進軍AV界成為AV女優。

2018年於節目上自曝離婚與26歲時整形的消息。

## 出演

### 電視劇

  - [超能少年2](../Page/超能少年2.md "wikilink")（サイコメトラーEIJI2，1999年，\[\[日本電視台|

日本電視\]\]）

  - [新宿暴走救急隊](../Page/新宿暴走救急隊.md "wikilink")（新宿暴走救急隊，2000年，\[\[日本電視台|

日本電視\]\]）

  - [本家之嫁](../Page/本家之嫁.md "wikilink")（本家のヨメ，2001年，[讀賣電視](../Page/讀賣電視台.md "wikilink")）
  - ～Xenos～東方神奇（Xenos，2007年，[東京電視](../Page/東京電視台.md "wikilink")）

### 電影

  - 《[玻璃之腦](../Page/玻璃之腦.md "wikilink")》（[ガラスの脳](http://www.jmdb.ne.jp/2000/dx000240.htm)，2000年，中田秀夫導演）主角
  - 《[伊藤潤二之](../Page/伊藤潤二.md "wikilink")[鬼卜怪談](../Page/鬼卜怪談.md "wikilink")》（[死びとの恋わずらい](http://www.jmdb.ne.jp/2001/dy000780.htm)，日2000年，台2007年，澁谷和行導演）主角
  - 《[野狼](http://ipac.hkfa.lcsd.gov.hk/ipac20/ipac.jsp?session=J168BO1408301.3703&profile=hkfa&uri=link=3100036@!120344@!3100024@!3100036&menu=search&submenu=basic_search&source=192.168.110.61@!horizon#focus)》（Color
    of Pain
    ～野狼～，2001年，梁德森導演）配角，合演有[黃浩然](../Page/黃浩然.md "wikilink")、[尹子維](../Page/尹子維.md "wikilink")、[李燦森](../Page/李燦森.md "wikilink")、[何超儀](../Page/何超儀.md "wikilink")
  - 《[不倫的秘密](../Page/不倫的秘密.md "wikilink")》（[ふぞろいな秘密](https://web.archive.org/web/20070818095826/http://www.huzoroinahimitsu.com/index.htm)，2007年，石原真理子導演）主角

### 廣告

  - [寶礦力水特](../Page/寶礦力水特.md "wikilink")
  - [羅森便利店](../Page/羅森_\(便利店\).md "wikilink")
  - TOPPO

### 寫真集

  - 《Go to R》（[集英社](../Page/集英社.md "wikilink")）
  - 《Risa》（[近代映画社](../Page/近代映画社.md "wikilink")）
  - 《Days》（[音樂專科社](../Page/音樂專科社.md "wikilink")）
  - 《reflect》（Wani Books）
  - 《TERRORS－アナザーストリー－》（[双葉社](../Page/双葉社.md "wikilink")）
  - 《from here to Reality》（[竹書房](../Page/竹書房.md "wikilink")）
  - 《W-trip》（Wani Books）
  - 《月刊後藤理沙》（[新潮社](../Page/新潮社.md "wikilink")）
  - 《LISA GOTO at NUDE》（[講談社](../Page/講談社.md "wikilink")）

### 錄影帶・DVD

  - 《from here to Reality》（竹書房）
  - 《ONE》（マーレ）

### 其他

  - [NHK教育台](../Page/NHK教育台.md "wikilink")『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』（飾演三六九（ミロク））

## 參考資料

## 外部連結

  - [JMO上的官方網站](https://web.archive.org/web/20071121230422/http://www.jmo.co.jp/profile/goto.htm)

[Category:福岡縣出身人物](../Category/福岡縣出身人物.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本AV女優](../Category/日本AV女優.md "wikilink")