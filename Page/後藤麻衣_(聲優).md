**後藤
麻衣**（），是[日本女性配音員](../Page/日本.md "wikilink")，出身於[北海道](../Page/北海道.md "wikilink")[帶廣市](../Page/帶廣市.md "wikilink")，目前隸屬[賢Production](../Page/賢Production.md "wikilink")。

身高148cm、體重43kg。[血型為](../Page/血型.md "wikilink")[B型](../Page/B型.md "wikilink")。[星座是](../Page/星座.md "wikilink")[獅子座](../Page/獅子座.md "wikilink")。

## 簡歷

  - 從帶廣北高中畢業後，前往專門學校東京アナウンス学院的放送声優科就讀。
  - 2009年3月，經紀公司由移籍到[賢Production](../Page/賢Production.md "wikilink")。
  - 沒有兄弟姊妹，是獨生女。
  - 擁有漢字檢定準二級、秘書檢定二級資格。
  - 喜歡的食物是[雞肉和](../Page/雞肉.md "wikilink")[蛋](../Page/蛋.md "wikilink")。另一方面，
    似乎不喜歡[香瓜](../Page/香瓜.md "wikilink")。
  - 不知道是不是常常看書的原因，很擅長[國文](../Page/國文.md "wikilink")，而對[數學很不擅長](../Page/數學.md "wikilink")。
  - 不管是本人還是他人都認為自己是[御宅族](../Page/御宅族.md "wikilink")。
  - 小學五年級的時候，聽了最喜歡的作品《[夢回綠園](../Page/夢回綠園.md "wikilink")》的廣播節目《這裡是綠林寮放送局》，而對[聲優這個工作有了興趣](../Page/聲優.md "wikilink")。
  - 擅長畫插圖，在動畫[乃木坂春香的秘密中擔任了次回預告的插畫](../Page/乃木坂春香的秘密.md "wikilink")。
  - 曾經有過[公寓的](../Page/公寓.md "wikilink")[水](../Page/水.md "wikilink")[電](../Page/電.md "wikilink")、[瓦斯被停掉的經驗](../Page/瓦斯.md "wikilink")。
  - 是廣播《[這裡是綠林寮放送局](../Page/夢回綠園.md "wikilink")》裡[岩田光央的](../Page/岩田光央.md "wikilink")[粉絲](../Page/粉絲.md "wikilink")。後藤在岩田於[釧路舉行的](../Page/釧路.md "wikilink")[握手會](../Page/握手會.md "wikilink")，向岩田傳達了自己想當聲優的事情，被岩田以「請加油」這樣鼓勵了。本人表示：「是個非常溫柔的人！」。
  - 後藤在部落格上表示，同名的寫真女星出馬競選[2009年東京都議會議員選舉時](../Page/2009年東京都議會議員選舉.md "wikilink")，被誤會是同一人，而收到「都議選請加油」的訊息。所屬事務所也收到雜誌取材的委託\[1\]。

## 主要參與作品

### 電視動畫

  - 主要角色以**粗體**顯示

<!-- end list -->

  - 2003年

<!-- end list -->

  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")（女孩、小孩）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [Happiness\!](../Page/Happiness!.md "wikilink")（**小日向桃**）
  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（**朝霧麻衣**）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [ef - a tale of
    memories.](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink")（羽山瑞希、廣野紘（幼年時期））
  - [逮捕令](../Page/逮捕令_\(動漫\).md "wikilink")（第四話的女性警官）
  - [七色★星露](../Page/七色★星露.md "wikilink")（**小雪**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [ef - a tale of
    melodies.](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink")（**羽山瑞希/未來**）
  - [鋼鐵新娘](../Page/鋼鐵新娘.md "wikilink")（小林玉子）
  - [乃木坂春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")（**乃木坂美夏**）※亦担当预告的绘图

<!-- end list -->

  - 2009年

<!-- end list -->

  - [11eyes](../Page/11eyes.md "wikilink")（**水奈瀨由佳**）
  - [K-ON\!](../Page/K-ON!.md "wikilink")（學生）
  - [真・戀姫†無雙](../Page/戀姬†無雙.md "wikilink")（**劉備**）
  - [乃木坂春香的秘密 Purezza♪](../Page/乃木坂春香的秘密.md "wikilink")（**乃木坂美夏**）
  - [鋼之鍊金術師 FULLMETAL
    ALCHEMIST](../Page/鋼之鍊金術師_FULLMETAL_ALCHEMIST.md "wikilink")（張梅）
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（マリリン）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [真・戀姫†無雙 ～乙女大亂～](../Page/戀姬†無雙.md "wikilink")（**劉備**）
  - [聖痕鍊金士](../Page/聖痕鍊金士.md "wikilink")（阿斯塔蒂）
  - [大小姐×執事！](../Page/大小姐×執事！.md "wikilink")（琵娜·司弗露姆克蘭·艾斯特）
  - [祝福的鐘聲](../Page/祝福的钟声.md "wikilink") （莉特絲·托魯蒂亞）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [純白交響曲](../Page/純白交響曲.md "wikilink")（**瓜生櫻乃**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [天使水果糖](../Page/天使水果糖.md "wikilink")（**忍**）
  - [銀之匙 Silver
    Spoon](../Page/銀之匙_Silver_Spoon.md "wikilink")（駒場二野、駒場三空）
  - [IS〈Infinite
    Stratos〉2](../Page/IS〈Infinite_Stratos〉.md "wikilink")（傑露西·布蘭科特）
  - [噗嗶啵～來自未來～](../Page/噗嗶啵～來自未來～.md "wikilink")（東禮子）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [BUDDY COMPLEX](../Page/BUDDY_COMPLEX.md "wikilink")（渡瀨翼）
  - [SONIANI -SUPER SONICO THE
    ANIMATION-](../Page/SONIANI_-SUPER_SONICO_THE_ANIMATION-.md "wikilink")（**富士見鈴**）
  - [銀之匙 Silver Spoon 第二季](../Page/銀之匙_Silver_Spoon.md "wikilink")（初中女子）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [任性High-Spec](../Page/任性High-Spec.md "wikilink")（**鳴海兔亞**）
  - [SHOW BY ROCK\!\!\#](../Page/SHOW_BY_ROCK!!.md "wikilink")（亞絲特莉亞爾）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [魔法律事務所](../Page/魔法律事務所.md "wikilink")（蘇菲姐）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [環戰公主](../Page/環戰公主.md "wikilink")（**妮娜·亞維琳**\[2\]）

### OVA

  - [ワイルド7](../Page/ワイルド7.md "wikilink")
  - [AIKa R-16:VIRGIN
    MISSION](../Page/AIKa_R-16:VIRGIN_MISSION.md "wikilink")（キョーコ）
  - [四娘物語](../Page/四娘物語.md "wikilink")（天地祭）
  - 異世界的聖機師物語（）
  - [灼眼的夏娜S](../Page/灼眼的夏娜.md "wikilink")（尾崎夕紀乃）

### 劇場版動畫

  - 魔法少年賈修：第101本魔法書（女性）

### 遊戲

  - [11eyes CrossOver](../Page/11eyes.md "wikilink")（**水奈瀨結花**）

  - [押忍！戰鬥！應援團](../Page/押忍！戰鬥！應援團.md "wikilink")（雨宮沙耶花）

  - [鋼鐵新娘！DS ～ヨメとメカと男と女～](../Page/鋼鐵新娘.md "wikilink")（小林玉子）

  - （南条時夢路）

  - （東雲こもも）

  - [電擊學園RPG：女神的十字架](../Page/電擊學園RPG：女神的十字架.md "wikilink")（乃木坂美夏）

  - [七色★星露 pure\!\!](../Page/七色★星露.md "wikilink")（**小雪** / 秋姬卡琳）

  - [narcissu 3rd -Die Dritte
    welt-](../Page/narcissu.md "wikilink")（チサト）

  - [乃木坂春香的秘密─開始了，角色扮演♥](../Page/乃木坂春香的秘密.md "wikilink")（**乃木坂美夏**）

  - [乃木坂春香的秘密 同人誌開始♥](../Page/乃木坂春香的秘密.md "wikilink")（**乃木坂美夏**）

  - Happiness\! Re:Lucks（**小日向すもも**）

  - [燃燒！熱血韻律魂
    押忍！戰鬥！應援團2](../Page/燃燒！熱血韻律魂_押忍！戰鬥！應援團2.md "wikilink")（雨宮沙耶花）

  - [夜明前的琉璃色 -Brighter than dawning
    blue-](../Page/夜明前的琉璃色.md "wikilink")（**朝霧麻衣**）

  - （**八重櫻**）

  - （導覽声音）

  - [魔塔大陸III](../Page/魔塔大陸.md "wikilink")（**咲**）

  - [天神乱漫 Happy Go Lucky！！](../Page/天神亂漫.md "wikilink")（**千歲佐奈**）

  - [Strawberry Nauts](../Page/Strawberry_Nauts.md "wikilink")（**日和橙子**）

  - [祝福的鐘声](../Page/祝福的钟声.md "wikilink") （莉特絲·托魯蒂亞）

  - [少女理論及其周邊](../Page/少女理論及其周邊.md "wikilink")（**梅麗爾‧琳祺**）

  - [少女理論及其之後的周邊](../Page/少女理論及其之後的周邊.md "wikilink")（**梅麗爾‧琳祺**）

### CD

  - [純白交響曲](../Page/純白交響曲.md "wikilink")（瓜生櫻乃）

## 註腳

<div class="references-small">

<references/>

</div>

## 外部連結

  - [株式會社 賢Production 公式網站](http://www.kenproduction.co.jp)
      - [賢Production
        後藤麻衣](https://web.archive.org/web/20130510001627/http://www.kenproduction.co.jp/member.php?mem=w66)（介紹頁）
  - [後藤麻衣blog「ごまぶろ。」](http://ameblo.jp/mai-goto/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:賢Production](../Category/賢Production.md "wikilink")

1.  [都議選｜後藤麻衣部落格「ごまぶろ。」by
    Ameba](http://ameblo.jp/mai-goto/entry-10294813625.html)
2.