**徐光启**（），[字](../Page/表字.md "wikilink")**子先**，[号](../Page/号.md "wikilink")**玄扈**，[圣名](../Page/圣名.md "wikilink")**保禄**（），谥**文定**。[南直隶](../Page/南直隶.md "wikilink")[松江府](../Page/松江府.md "wikilink")[上海县人](../Page/上海县.md "wikilink")，明朝末年儒学、[西学](../Page/西学.md "wikilink")、[天学](../Page/天学.md "wikilink")、数学、水利、农学、军事学等领域学者，科学家、思想家、政治家、军事家，[明朝天主教](../Page/明朝天主教.md "wikilink")[教友领袖](../Page/教友.md "wikilink")，于[崇祯朝官至](../Page/崇祯.md "wikilink")[礼部尚书兼](../Page/礼部尚书.md "wikilink")[文渊阁大学士](../Page/文渊阁大学士.md "wikilink")、[内阁次辅](../Page/明朝内阁.md "wikilink")，任上病逝于北京。

徐光启是[中西文化交流和](../Page/西学东渐.md "wikilink")[中国近代科学技术事业的先驱之一](../Page/中国科技史.md "wikilink")。在明末天下危亡之际，他忧国爱民、清廉勤政，倾心竭力以实学救国利民，在[西学东渐](../Page/西学东渐.md "wikilink")，引进西式[火器和发展明军炮兵抵御](../Page/火器.md "wikilink")[后金](../Page/后金.md "wikilink")，引种和推广番薯、良种水稻等高产抗逆作物等的过程中起了关键作用。

徐光启最著名的事迹之一是他与[利玛窦合作汉译](../Page/利玛窦.md "wikilink")[欧几里得](../Page/欧几里得.md "wikilink")《[几何原本](../Page/几何原本.md "wikilink")》前6卷，其中译定的一些重要术语沿用至今。此外，他亲自或组织他人与来华天主教传教士合作编译了其他一系列[汉文西书](../Page/汉文西书.md "wikilink")。徐光启是[百科全书式的人物](../Page/通才.md "wikilink")，其尽晚年心血的主要工作是编纂集中国古代农学之大成的《[农政全书](../Page/农政全书.md "wikilink")》和系统介绍西方古典天文学（主要是[第谷体系](../Page/第谷.md "wikilink")）理论和方法的《[崇祯历书](../Page/崇祯历书.md "wikilink")》等[百科全书式巨著](../Page/百科全书.md "wikilink")。徐光启还著有军事文集《[徐氏庖言](../Page/徐氏庖言.md "wikilink")》和数量可观的天主教传道护教文章。徐光启病逝后，[李之藻辑大量徐光启译著等成丛书](../Page/李之藻.md "wikilink")《[天学初函](../Page/天学初函.md "wikilink")》刊行。

在西学方面启蒙于[郭居静](../Page/郭居静.md "wikilink")、受教于主要合作者利玛窦，徐光启深感中国传统学术于逻辑的严重欠缺和中国数学的停滞落后，因而高度重视[演绎推理](../Page/演绎推理.md "wikilink")，以数学为着力点，倡导数学的研习、普及和应用；同时，他以理论指导实践，长期身体力行地进行天文、水利、农业等方面的科学实验和测量以及天文望远镜、西式火炮等的制造，归纳总结实践经验。徐光启生活在16世纪末、17世纪初，与[培根](../Page/培根.md "wikilink")、[伽利略](../Page/伽利略.md "wikilink")、[笛卡儿等西欧学术名家同时代且并驾齐驱](../Page/笛卡儿.md "wikilink")，在一些方面或有过之而无不及。在对待西学和[西方文明的态度问题上](../Page/西方文明.md "wikilink")，远早于且不同于清末[魏源](../Page/魏源_\(清朝\).md "wikilink")“[师夷长技以制夷](../Page/师夷长技以制夷.md "wikilink")”、[馮桂芬](../Page/馮桂芬.md "wikilink")“[中体西用](../Page/中体西用.md "wikilink")”等思想，徐光启不仅试图组织人才队伍在道理和技艺各层面虚心学习和利用西方优秀文明成果（“博求道艺之士，虚心扬榷，令彼三千年增修渐进之业，我岁月间拱受其成”\[1\]），还提出了逐步而全面地理解、融汇并超越（“欲求超胜，必须会通；会通之前，先须翻译。”\[2\]）的发展路线。

坚持[无神论立场](../Page/国家无神论.md "wikilink")、[独立自主自办教会原则且曾与](../Page/三自爱国教会.md "wikilink")[梵蒂冈](../Page/梵蒂冈.md "wikilink")[长期交恶的中共当局侧重于宣传其为](../Page/中華人民共和國－梵蒂岡關係.md "wikilink")“爱国科学家”。但近年来，中国内地学界和舆论已开始公开强调徐光启近乎“中国向西方寻找思想的第一人”\[3\]的历史地位和虔诚天主教徒的敏感身份。

徐光启希望以“修身事天”的基督教信仰匡救时弊，挽回世道人心，“补儒易佛”，改良中华文化。徐保禄阁老是[罗马大公教会](../Page/天主教会.md "wikilink")[汉传开教之初热忱而忠贞的教友领袖和护教士](../Page/明朝天主教.md "wikilink")，是[中国基督教史上影响力最大](../Page/中国基督教史.md "wikilink")、官位最高的奉教士大夫，被誉為中国天主教「[聖教三柱石](../Page/聖教三柱石.md "wikilink")」之首。1933年，徐光启被[天主教上海教区宣告为](../Page/天主教上海教区.md "wikilink")“[天主之-{zh-hans:仆;zh-hant:僕;}-](../Page/天主之仆.md "wikilink")”，启动列真福品案；2010年前后，利玛窦、徐光启列真福品案分别重启。

## 生平

[缩略图](https://zh.wikipedia.org/wiki/File:Ming_Xuguangqi_Guju.JPG "fig:缩略图")\]\]

徐氏祖居[苏州](../Page/苏州.md "wikilink")，以务农为业，后迁至上海。徐光启的祖父因经商而致富，及至父亲徐思诚家道中落，乃转务农。[嘉靖四十一年](../Page/嘉靖.md "wikilink")（1562年）徐光启出生于[太卿坊](../Page/太卿坊.md "wikilink")（今上海市[黄浦区](../Page/黄浦区.md "wikilink")[乔家路](../Page/乔家路.md "wikilink")）。

### 万历朝

少年时代的徐光启在[龙华寺读书](../Page/上海龙华寺.md "wikilink")。[万历九年](../Page/万历.md "wikilink")（1581年）应[金山卫试中](../Page/金山卫.md "wikilink")[秀才后](../Page/秀才.md "wikilink")，他在家乡教书，并娶本县处士吴小溪女儿为妻。

万历二十一年（1593年），徐光启赴[广东](../Page/广东.md "wikilink")[韶州任教](../Page/韶州.md "wikilink")，并结识了[耶稣会士](../Page/耶稣会.md "wikilink")[郭居静](../Page/郭居静.md "wikilink")（Lazzaro
Cattaneo）。二十四年（1596年）年转至[广西](../Page/广西.md "wikilink")[浔州任教](../Page/浔州.md "wikilink")。

万历二十五年（1597年），徐光启因考官[焦竑赏识而以](../Page/焦竑.md "wikilink")[顺天府](../Page/顺天府.md "wikilink")[解元中举](../Page/解元.md "wikilink")。次年[会试他未能考中](../Page/会试.md "wikilink")，便回到家乡教书。

[Ricci1.jpg](https://zh.wikipedia.org/wiki/File:Ricci1.jpg "fig:Ricci1.jpg")》中的插图：利玛窦和徐光启\]\]

万历二十八年（1600年），他赴南京拜见恩师焦竑，并首次与耶稣会士利玛窦晤面。三十一年（1603年），在南京由耶稣会士[罗如望](../Page/罗如望.md "wikilink")（Jean
de
Rocha）[受洗入天主教会](../Page/受洗.md "wikilink")，圣名为[保禄](../Page/保禄.md "wikilink")（Paulus）。

万历三十二年（1604年），徐光启中[进士](../Page/进士.md "wikilink")，考选[翰林院](../Page/翰林院.md "wikilink")[庶吉士](../Page/庶吉士.md "wikilink")。三十四年（1606年）他开始与利玛窦合作翻译《[几何原本](../Page/几何原本.md "wikilink")》前6卷，次年春翻译完毕并刻印刊行。翻译完毕《几何原本》后，他又根据利玛窦口述翻译了《测量法义》一书。

万历三十五年（1607年）三年，翰林馆期满告散，他被授予翰林院[检讨](../Page/检讨.md "wikilink")。同年徐光启的父亲在北京去世，他回乡[丁忧守制](../Page/丁忧.md "wikilink")。第二年邀请郭居静至上海传教，这成为天主教传入上海之始。守制期间，他整理定稿了《测量法义》，并将《测量法义》与《[周髀算经](../Page/周髀算经.md "wikilink")》、《[九章算术](../Page/九章算术.md "wikilink")》相互参照，整理编撰了《测量异同》；作《勾股义》一书，探讨[商高定理](../Page/商高定理.md "wikilink")；开辟双园、农庄别墅，进行农作物引种、耕作试验，作《甘薯疏》、《芜菁疏》、《吉贝疏》、《种棉花法》和《代园种竹图说》。

万历三十八年（1610年），徐光启回到北京，官复原职。因[钦天监推算](../Page/钦天监.md "wikilink")[日食不準](../Page/日食.md "wikilink")，他与传教士合作研究天文仪器，撰写了《简平仪说》、《平浑图说》、《日晷图说》和《夜晷图说》。四十年（1612年），他向耶稣会士[熊三拔](../Page/熊三拔.md "wikilink")（P.
Sabbathino de Ursis）学习西方水利，合译《泰西水法》6卷。

万历四十一年（1613年）初冬，因与朝中一些大臣意见不合，徐光启告病去职前往[天津](../Page/天津.md "wikilink")。他在[房山](../Page/房山.md "wikilink")、[涞水两县开渠种稻](../Page/涞水.md "wikilink")，进行各种农业实验，先后撰写了《宜垦令》、《农书草稿》（北耕录）等书，为《[农政全书](../Page/农政全书.md "wikilink")》的编写打下了基础。

万历四十四年（1616年），礼部侍郎[沈榷连上三道奏疏](../Page/沈榷.md "wikilink")，请求查办天主教传教士，史称“[南京教案](../Page/南京教案.md "wikilink")”。徐光启上《辩学章疏》为传教士辩护。同年徐光启回京复职，次年任[詹事府](../Page/詹事府.md "wikilink")[左春坊左](../Page/左春坊.md "wikilink")[赞善](../Page/赞善.md "wikilink")。不久病归天津，作《粪壅规则》。

万历四十六年（1618年），后金大汗[努尔哈赤发兵进犯关内](../Page/努尔哈赤.md "wikilink")，徐光启应召星夜入京。

万历四十七年（1619年）明军在[萨尔浒之战中战败](../Page/萨尔浒之战.md "wikilink")，他多次上疏请求练兵，擢升少詹事兼河南道御史，在通州督练新军。但由于军饷、器械供应困难，练兵计划并不顺利。

### 天启朝

[天启元年](../Page/天启_\(明朝\).md "wikilink")（1621年）三月，徐光启上疏回天津养病；六月辽阳失陷，他又奉召返京，力请使用[红夷大炮帮助守城](../Page/红夷大炮.md "wikilink")，但因与[兵部尚书崔景荣意见不合](../Page/兵部尚书.md "wikilink")，于十二月再次辞归。

天启三年（1624年），徐光启升任[礼部右](../Page/礼部.md "wikilink")[侍郎兼](../Page/侍郎.md "wikilink")[侍读学士等职](../Page/侍读学士.md "wikilink")。当时朝中[魏忠贤专权](../Page/魏忠贤.md "wikilink")，他不肯就任，次年便遭谗劾去职。徐光启回到上海，将积累多年的农业资料“系统地进行增广、审订、批点、编排”，编撰而成后来的《[农政全书](../Page/农政全书.md "wikilink")》。他同[毕方济](../Page/毕方济.md "wikilink")（P.
Franciscus
Sambiasi）一起合译了《[灵言蠡勺](../Page/灵言蠡勺.md "wikilink")》。还把自己关于军事方面的文章辑录成书，刻印刊行了《徐氏庖言》。

### 崇祯朝

[崇祯元年](../Page/崇祯.md "wikilink")（1628年），徐光启奉召回京，官复原职。二年（1629年），升[礼部左侍郎](../Page/礼部左侍郎.md "wikilink")。因钦天监推算日食失準，[崇祯帝同意由徐光启主持开局修曆](../Page/崇祯帝.md "wikilink")。同年，[皇太极率领兵数万人进逼京畿](../Page/皇太极.md "wikilink")，崇祯帝召集大臣平台议事，徐光启奏对战守之事。三年（1630年），升任[礼部尚书](../Page/礼部尚书.md "wikilink")。四年起，陆续进献曆書多卷，即《[崇禎曆書](../Page/崇禎曆書.md "wikilink")》。五年（1632年），以礼部尚书兼[东阁大学士](../Page/东阁大学士.md "wikilink")，入参机务。六年（1633年），加[太子太保](../Page/太子太保.md "wikilink")，兼[文渊阁大学士](../Page/文渊阁大学士.md "wikilink")；同年十月初七（11月8日），病逝于任上。[谥文定](../Page/谥号.md "wikilink")。崇祯十四年（1641年），归葬上海[徐家汇](../Page/徐家汇.md "wikilink")。

徐光启《[农政全书](../Page/农政全书.md "wikilink")》遗稿经[陈子龙修订](../Page/陈子龙.md "wikilink")，编成60卷，于崇祯十二年（1639年）刊行。

## 思想与著述

### 主要著作年表

（含编译、与他人合著、由他人整理出版的作品）

  - 1603年：《毛诗六贴讲义》4卷
  - 1607年：《[几何原本](../Page/几何原本.md "wikilink")》前6卷（与利玛窦合译）、《[测量法义](../Page/测量法义.md "wikilink")》（与利玛窦合译）
  - 1608年：《测量异同》、《甘薯疏》
  - 1609年：《勾股义》
  - 1612年：《[泰西水法](../Page/泰西水法.md "wikilink")》（与[熊三拔合译](../Page/熊三拔.md "wikilink")）
  - 1613年至1618年：《农书草稿》（《北耕录》）
  - 1614年：《定法平方算数》（疑为托名）2卷、《刻同文算指序》
  - 1619年：《考工记解》、《选练条格》
  - 1620年：《农遗杂疏》5卷
  - 1625年至1628年：《[农政全书](../Page/农政全书.md "wikilink")》60卷
  - 1627年：《[徐氏庖言](../Page/徐氏庖言.md "wikilink")》5卷
  - 1629年受命至1633年病逝：《[崇祯历书](../Page/崇祯历书.md "wikilink")》46种、137卷

### 经学、文学

  - 1603年：《毛诗六贴讲义》4卷

### 西学译著（汉文西书）

[Introduction_to_Astronomy_translated_by_Xu_Guangqi_and_edited_by_Li_Zhizao.jpg](https://zh.wikipedia.org/wiki/File:Introduction_to_Astronomy_translated_by_Xu_Guangqi_and_edited_by_Li_Zhizao.jpg "fig:Introduction_to_Astronomy_translated_by_Xu_Guangqi_and_edited_by_Li_Zhizao.jpg")辑，于崇祯元年（1628年）刊行的[丛书](../Page/丛书.md "wikilink")《[天学初函](../Page/天学初函.md "wikilink")》，其中收录大量徐光启译著。\]\]

  - 天启四年（1624年），[毕方济](../Page/毕方济.md "wikilink")、徐光启译[亚里士多德](../Page/亚里士多德.md "wikilink")《[论灵魂](../Page/论灵魂.md "wikilink")》为《灵言蠡勺》（后收入[李之藻辑](../Page/李之藻.md "wikilink")《[天学初函](../Page/天学初函.md "wikilink")》）。

### 天主教信仰及宗教问题

  - 圣诗

<!-- end list -->

  - 《大赞诗》（[诗经体](../Page/诗经.md "wikilink")）
  - 《獻心頌》
  - 《耶稣像赞》（Eulogy on the Portrait of Jesus）
  - 《圣母像赞》（Eulogy on the Portrait of the Virgin Mary）
  - 《正道题纲》（An Outline of the Way of Justice）
  - 《规诫箴赞》（Hymn of Admonition and Commandment）
  - 《[十诫箴赞](../Page/十诫.md "wikilink")》（An Admonitory Hymn on the Ten
    Commandments）
  - 《克罪七德箴赞》（An Admonitory Hymn of the Seven Virtues of Redemption）
  - 《[真福八端箴赞](../Page/真福八端.md "wikilink")》（A Hymn of Admonition on the
    Eight True Beatitudes）
  - 《哀矜十四端箴赞》（A Hymn of Admonition on the Fourteen Mercies）

<!-- end list -->

  - [护教奏疏](../Page/基督教辨惑學.md "wikilink")

<!-- end list -->

  - 1616年（万历四十四年），《辩学章疏》：因[南京教案而上疏](../Page/南京教案.md "wikilink")，为来华传教士辩护，论证“诸陪臣之学，真可以补益王化，左右儒术，救正佛法，裨益当朝。”，建议让天主教与佛教代表开辩论会、设立“特区”考察天主教对社会的影响等等。——[万历帝御批](../Page/万历帝.md "wikilink")：“知道了”。

<!-- end list -->

  - 批判[汉传佛教](../Page/汉传佛教.md "wikilink")、[道教及](../Page/道教.md "wikilink")[中国民间宗教中的迷信观念和习俗](../Page/中国民间宗教.md "wikilink")

<!-- end list -->

  - 《辟妄》
  - 《谘诹偶编》

<!-- end list -->

  - [景教碑考古](../Page/景教碑.md "wikilink")

<!-- end list -->

  - 《景教堂碑记》
  - 《铁十字箸》

<!-- end list -->

  - 其他与宗教信仰相关的散文

<!-- end list -->

  - 《答乡人书》（A Letter in Reply to a Countryman）
  - 《造物主垂像略说》（Short Explanation of the Hanging Picture of the Creator）
  - 《跋二十五言》（A postscript to the Twenty-Five Sayings）

### 数学

[Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg](https://zh.wikipedia.org/wiki/File:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg "fig:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg")〉序》\]\]

  - 1607年：《[几何原本](../Page/几何原本.md "wikilink")》前6卷（与利玛窦合译）

<!-- end list -->

  - 关于数学地位、功能的重要论述

<!-- end list -->

  - 《刻〈幾何原本〉序》：

<!-- end list -->

  - 《〈幾何原本〉杂议》：

<!-- end list -->

  - 1628年（崇祯元年），《条议历法修正岁差疏》：“盖凡物有形有质，莫不资与度数故耳”，提出“度数旁通十事”即治历、测量、音律、军事、理财、营建、机械、舆地、医药、计时，建议在“历局”内开展以数学为基础的多学科研究工作，该设想近似于开办“[科学院](../Page/科学院.md "wikilink")”。——[崇祯帝批准](../Page/崇祯帝.md "wikilink")：“度数旁通有关庶绩，一并分曹料理，该衙知道。”

### 测量学

  - 1607年：《[测量法义](../Page/测量法义.md "wikilink")》（与利玛窦合译）
  - 1608年：《测量异同》：比较中西测量学理论发展
  - 1609年：《勾股义》

### 中国数学史、科学技术史

  - 1614年：《刻〈[同文算指](../Page/同文算指.md "wikilink")〉序》：讨论了元明数学衰落的原因等。
  - 1619年：《〈[考工记](../Page/考工记.md "wikilink")〉解》

### 科研、治历战略

  - 1631年（崇祯四年）春，《历书总目表》：

### 天文、历法

  - 1629年受命至1633年病逝主持编修《[崇祯历书](../Page/崇祯历书.md "wikilink")》（共46种、137卷，1634年完成），其中系统介绍了当时西方的天文学、历法和三角学（包括平面三角和球面三角），堪称“欧洲[古典天文学百科全书](../Page/古典天文学.md "wikilink")”，成为此后清朝官方[天学的理论基础](../Page/天学.md "wikilink")。徐光启先后召请[耶稣会士](../Page/在华耶稣会士列表.md "wikilink")[龙华民](../Page/龙华民.md "wikilink")（Niccolo
    Longobardo，1565-1655）、[邓玉函](../Page/邓玉函.md "wikilink")（Johann
    Terrenz Schreck，1576-1630）、[汤若望](../Page/汤若望.md "wikilink")（Johann
    Adam Schall von
    Bell，1592-1666）和[罗雅谷](../Page/罗雅谷.md "wikilink")（Jacobus
    Rho，1592-1638）四人参与历局工作，大部分实际工作出于[汤若望和](../Page/汤若望.md "wikilink")[罗雅谷之手](../Page/罗雅谷.md "wikilink")。

### 军事理论、练兵、兵器

[Cook_Xu's_words.jpg](https://zh.wikipedia.org/wiki/File:Cook_Xu's_words.jpg "fig:Cook_Xu's_words.jpg")著作《[徐氏庖言](../Page/徐氏庖言.md "wikilink")》\]\]

  - 万历四十八年（1620年）二月起，徐光启受命在[北直隶](../Page/北直隶.md "wikilink")[通州](../Page/通州_\(北京\).md "wikilink")、[昌平等地督练](../Page/昌平.md "wikilink")“[新军](../Page/新军.md "wikilink")”，期间他撰写了《选练百字诀》、《选练条格》、《练艺条格》、《束伍条格》、《形名条格》（列阵方法）、《火攻要略》（火炮要略）、《制火药法》等等。徐光启撰写的这些“[条格](../Page/条格.md "wikilink")”是中国近代较早的一批军事[条令和](../Page/条令.md "wikilink")[法典](../Page/法典.md "wikilink")。《选练百字诀》和《选练条格》等体现了徐光启“选需实选，练需实练”的责实精神。

<!-- end list -->

  - 1627年，徐光启完成[军事学著作](../Page/军事学.md "wikilink")《[徐氏庖言](../Page/徐氏庖言.md "wikilink")》5卷。

### 国防、战略问题奏疏

[Letter_of_Xu_Quangxi_to_the_King_of_Portugal_in_Latin.jpg](https://zh.wikipedia.org/wiki/File:Letter_of_Xu_Quangxi_to_the_King_of_Portugal_in_Latin.jpg "fig:Letter_of_Xu_Quangxi_to_the_King_of_Portugal_in_Latin.jpg")的信\]\]

  - 《海防迂说》：谴责朝廷不救琉球、白救朝鲜，预见三百年后日本侵吞琉球、台湾、朝鲜；要求彻底解决日本问题，使日本“常为外藩”\[4\]
  - 《亟遣使臣监护朝鲜》：自荐派驻、监护、改革朝鲜国，抑制朝鲜离心倾向，联合朝鲜对付后金。
  - 《大征策》
  - 《器胜策》
  - 《服戎策》
  - 《拟上安边御虏疏》
  - 《敷陈末议以殄凶酋疏》
  - 《兵非选练决难战守疏》
  - 《辽左阽危已甚疏》
  - 《恭承新命谨陈急切事宜疏》

### 人口、经济

  - 《处置宗禄查核边饷议》：提出类似[马尔萨斯](../Page/马尔萨斯.md "wikilink")[人口论](../Page/人口论.md "wikilink")（同样不准确）的明朝宗室人口倍增模型。
  - 《拟缓举三殿及朝门工程疏》

### 农学、水利、荒政

[Nongzhengquanshu.jpg](https://zh.wikipedia.org/wiki/File:Nongzhengquanshu.jpg "fig:Nongzhengquanshu.jpg")大成的巨著《[农政全书](../Page/农政全书.md "wikilink")》（上海[嘉定孔庙展品](../Page/嘉定孔庙.md "wikilink")）\]\]

  - 万历三十三年（1605年）《漕河议》：水利规划著作，论及[漕运之利弊](../Page/漕运.md "wikilink")（指出“漕能使国贫”）及其改进之法、治理[黄河之法](../Page/黄河.md "wikilink")，并指出[水利工程中](../Page/水利工程.md "wikilink")[数学应用的重要性](../Page/应用数学.md "wikilink")。（文见《[明经世文编](../Page/明经世文编.md "wikilink")》卷四百九十一）
  - 1608年《甘藷疏》：中国最早关于[甘薯种植和加工利用的专著](../Page/甘薯.md "wikilink")，建议在各地推广甘薯。除《甘藷疏序》外已失传。
  - 《[芜菁疏](../Page/芜菁.md "wikilink")》
  - 《[吉贝疏](../Page/吉贝.md "wikilink")》
  - 《种[棉花法](../Page/棉花.md "wikilink")》
  - 《代园种竹图说》
  - 《除[蝗疏](../Page/蝗虫.md "wikilink")》24章，3万多字：统计[中国蝗灾史](../Page/中国蝗灾史.md "wikilink")，总结治蝗方法。
  - 1612年《[泰西水法](../Page/泰西水法.md "wikilink")》（与[熊三拔合译](../Page/熊三拔.md "wikilink")）
  - 1613年至1618年《农书草稿》（《北耕录》）
  - 1620年《农遗杂疏》5卷
  - 1625年至1628年《[农政全书](../Page/农政全书.md "wikilink")》，遗稿经[陈子龙修订](../Page/陈子龙.md "wikilink")，编成60卷，于崇祯十二年（1639年）刊行。
  - 1630年（崇祯三年）《旱田用水疏》：总结中国古代对各种水源的利用经验。

### 化学

  - 《造[强水法](../Page/强水.md "wikilink")》：关于[硝酸制法的手稿](../Page/硝酸.md "wikilink")，是中国最早记述[无机酸的作品](../Page/无机酸.md "wikilink")\[5\]。

## 评价

徐光启病逝后，[崇祯帝為之](../Page/崇祯帝.md "wikilink")「輟朝三日」，赐[谥](../Page/谥号.md "wikilink")[文定](../Page/文定.md "wikilink")，人称**徐文定公**。

[徐光启墓石牌坊上正中额题](../Page/徐光启墓.md "wikilink")“文武元勋”，右题“熙朝元辅”，左题“王佐儒宗”，两侧刻有[对联](../Page/对联.md "wikilink")：“治曆明农百世师经天纬地，出将入相一个臣奋武揆文”。

清初官修《[明史](../Page/明史.md "wikilink")·列传一百三十九·徐光启传》：“（[崇祯](../Page/崇祯.md "wikilink")）五年五月，以[本官兼](../Page/礼部尚书.md "wikilink")[东阁大学士](../Page/东阁大学士.md "wikilink")，入参机务，与[郑以伟并命](../Page/郑以伟.md "wikilink")。寻加[太子太保](../Page/太子太保.md "wikilink")，进[文渊阁](../Page/文渊阁.md "wikilink")。光启雅负经济才，有志用世。及柄用，年已老，值[周延儒](../Page/周延儒.md "wikilink")、[温体仁专政](../Page/温体仁.md "wikilink")，不能有所建白。明年十月卒。赠[少保](../Page/少保.md "wikilink")。[御史言光启盖棺之日](../Page/御史.md "wikilink")，囊无余赀，请优恤以愧贪墨者。帝纳之，乃[谥光启文定](../Page/谥.md "wikilink")。久之，帝念光启博学强识，索其家遗书。子骥入谢，进《[农政全书](../Page/农政全书.md "wikilink")》六十卷。诏令有司刊布。加赠[太保](../Page/太保.md "wikilink")，其孙为[中书舍人](../Page/中书舍人.md "wikilink")。”

徐光启为官清廉，《明史·徐光启传》有“蓋棺之日，囊無餘貲”的记载，《[罪惟录](../Page/罪惟录.md "wikilink")》说他“官邸萧然，敝衣数袭外，止著述手草尘束而已”。“

2011年，徐光启被譽為[中国天主教](../Page/中国天主教.md "wikilink")「[聖教三柱石](../Page/聖教三柱石.md "wikilink")」之首。[天主教上海教区和](../Page/天主教上海教区.md "wikilink")[利瑪竇家乡所在](../Page/利瑪竇.md "wikilink")[天主教教区已分别启动为二人宣福列圣作预备的调查审核程序](../Page/天主教教区.md "wikilink")，这一阶段他们的头衔是“[天主之僕](../Page/天主之僕.md "wikilink")”，多年后可能被[圣座宣布爲](../Page/圣座.md "wikilink")[真福者和](../Page/真福者.md "wikilink")[圣人](../Page/天主教聖人.md "wikilink")\[6\]。

《[東方早报](../Page/東方早报.md "wikilink")》载，徐光啟被誉为“四百多年来最杰出的[上海人](../Page/上海人.md "wikilink")”\[7\]。

《[解放日报](../Page/解放日报.md "wikilink")》载，徐光啟作为近现代上海的“人文始祖”、[海派文化的奠基人和标志性人物](../Page/海派文化.md "wikilink")，被称为“徐上海”、“上海文明的肇始者”等，他被认为是“第一位近代意义上的上海人”\[8\]。

## 後世影響

[光启公园和徐光启纪念馆](../Page/光启公园.md "wikilink")”现为[徐汇区](../Page/徐汇区.md "wikilink")[廉政教育基地](../Page/廉政.md "wikilink")。

## 主要社会关系

[Wife_of_Xu_Guangqi.jpg](https://zh.wikipedia.org/wiki/File:Wife_of_Xu_Guangqi.jpg "fig:Wife_of_Xu_Guangqi.jpg")
[Jesuites_en_chine.jpg](https://zh.wikipedia.org/wiki/File:Jesuites_en_chine.jpg "fig:Jesuites_en_chine.jpg")（下右）
和三位重要来华[耶稣会传教士](../Page/耶稣会.md "wikilink")：意大利神父[利玛窦](../Page/利玛窦.md "wikilink")（上左）、[神圣罗马帝国神父](../Page/神圣罗马帝国.md "wikilink")[汤若望](../Page/汤若望.md "wikilink")（上中）、比利时神父[南怀仁](../Page/南怀仁.md "wikilink")（上右）\]\]

### 家族

  - 曾祖父：徐珣
  - 祖父：徐绪
  - 祖母：尹氏
  - 父亲：徐思诚
  - 母亲：
  - 一妻：吴氏。徐光启作为[基督徒](../Page/基督徒.md "wikilink")（41岁时入天主教，参见[基督教婚姻观](../Page/基督教婚姻观.md "wikilink")），未置侧室（一说为一妻一妾，妾韩氏，待考）。
  - 独子：徐骥，娶妻顾氏，-{系}-上海首富[顾昌祚](../Page/顾昌祚.md "wikilink")（“顾半城”）之女。
  - 5个孙子
  - 4个孙女：
      - 长孙女教名福利济大
      - 次孙女教名甘第大（许徐甘弟大夫人，[许远度之妻](../Page/许远度.md "wikilink")、[许缵曾之母](../Page/许缵曾.md "wikilink")）
      - 四孙女教名玛尔第纳

徐光启的后裔[倪桂珍](../Page/倪桂珍.md "wikilink")，是近代中国影响巨大的[宋氏三姐妹的母亲](../Page/宋氏三姐妹.md "wikilink")。

### 良师

  - 会试阅卷选拔恩师：[焦竑](../Page/焦竑.md "wikilink")，明代王门后学[泰州学派代表人物](../Page/泰州学派.md "wikilink")。
  - 业师：[黄体仁](../Page/黄体仁.md "wikilink")，[上海县县城人](../Page/上海县.md "wikilink")，[王学学者](../Page/王学.md "wikilink")，曾在[龙华寺督教三年](../Page/龙华寺_\(上海\).md "wikilink")，时徐光启受教门下。万历三十二年，两人同登进士。就职京师时，[李运机使试馆职](../Page/李运机.md "wikilink")，体仁谢辞曰：“某老矣，不足辱此选，门人徐光启，博学而贤，用是才也。”请以自代。徐光启入[翰林院为](../Page/翰林院.md "wikilink")[庶吉士](../Page/庶吉士.md "wikilink")，体仁则为[刑部主事](../Page/刑部主事.md "wikilink")，时传为佳话。
  - [西学](../Page/西学.md "wikilink")[启蒙](../Page/启蒙.md "wikilink")[导师及主要](../Page/导师.md "wikilink")[神师](../Page/神师.md "wikilink")：[郭居靜](../Page/郭居靜.md "wikilink")、[利玛窦](../Page/利玛窦.md "wikilink")

### 上司

  - [明朝皇帝](../Page/明朝皇帝.md "wikilink")：[万历帝](../Page/万历帝.md "wikilink")
    → [泰昌帝](../Page/泰昌帝.md "wikilink") →
    [天启帝](../Page/天启帝.md "wikilink") →
    [崇祯帝](../Page/崇祯帝.md "wikilink")
  - 对徐光启和[天主教会友好的](../Page/天主教会.md "wikilink")[内阁首辅](../Page/内阁首辅.md "wikilink")：
      - [叶向高](../Page/叶向高.md "wikilink")，[东林党人](../Page/东林党.md "wikilink")，万历四十年至四十二年（1612年－1614年）、天啓元年至四年（1621年－1624年）任内阁首辅。
      - [刘宇亮](../Page/刘宇亮.md "wikilink")，崇祯十一年至十二年（1638年－1639年）任内阁首辅。亦曾任礼部尚书兼东阁大学士并受徐光启影响，后在家乡率众[领洗奉教](../Page/领洗.md "wikilink")。

### [同工](../Page/同工.md "wikilink")

  - [耶稣会传教士](../Page/耶稣会.md "wikilink")

<!-- end list -->

  - [郭居静](../Page/郭居静.md "wikilink")，意大利人，徐光启结识的第一位传教士，上海、[嘉定](../Page/嘉定直隸州.md "wikilink")、杭州开教者。
  - [利玛窦](../Page/利玛窦.md "wikilink")，意大利人，耶稣会在华第一任负责人，教授徐光启[数学](../Page/数学.md "wikilink")，与其合作翻译《几何原本》。
  - [龙华民](../Page/龙华民.md "wikilink")，意大利人，耶稣会在华第二任负责人，短期参与编修《崇祯历书》。
  - [毕方济](../Page/毕方济.md "wikilink")，意大利人，[松江开教者](../Page/松江府.md "wikilink")。
  - [熊三拔](../Page/熊三拔.md "wikilink")，意大利人。与徐光启合译《[泰西水法](../Page/泰西水法.md "wikilink")》。
  - [罗雅谷](../Page/罗雅谷.md "wikilink")，意大利人。
  - [艾儒略](../Page/艾儒略.md "wikilink")，意大利人，福建开教者。
  - [高一志](../Page/高一志.md "wikilink")，意大利人，山西开教者。
  - [阳玛诺](../Page/阳玛诺.md "wikilink")，[葡萄牙人](../Page/葡萄牙人.md "wikilink")。
  - [曾德昭](../Page/曾德昭.md "wikilink")，葡萄牙人。
  - [费奇观](../Page/费奇观.md "wikilink")，葡萄牙人。
  - [李马诺](../Page/李马诺.md "wikilink")，葡萄牙人，[澳门公学院长](../Page/澳门公学.md "wikilink")。
  - [黎勃劳](../Page/黎勃劳.md "wikilink")，葡萄牙人。
  - [庞迪峨](../Page/庞迪峨.md "wikilink")，[西班牙人](../Page/西班牙人.md "wikilink")。
  - [邓玉函](../Page/邓玉函.md "wikilink")，[神圣罗马帝国](../Page/神圣罗马帝国.md "wikilink")[德意志人](../Page/德意志人.md "wikilink")。
  - [汤若望](../Page/汤若望.md "wikilink")，神圣罗马帝国德意志人，协助徐光启编修《[崇祯历书](../Page/崇祯历书.md "wikilink")》。

<!-- end list -->

  - 中国[天主教徒](../Page/天主教徒.md "wikilink")

<!-- end list -->

  - “中国天主教四贤”
      - “[圣教三柱石](../Page/圣教三柱石.md "wikilink")”（以徐光启为首）
          - [李之藻](../Page/李之藻.md "wikilink")，与徐光启共任历局监督，先于徐光启病逝。
          - [杨廷筠](../Page/杨廷筠.md "wikilink")
      - [王徵](../Page/王徵_\(天启进士\).md "wikilink")，科学家，天启进士，历任直隶广平府推官、辽海监军道等职，因登州战事失利获罪充军，后遇赦回乡，购地归隐山林，终因不愿出仕李自成而绝食殉国。译著有《[奇器图说](../Page/遠西奇器圖說錄最.md "wikilink")》、《西洋音诀》、不少天主教教义著作。
  - 徐光启的门生：
      - [孙元化](../Page/孙元化.md "wikilink")，徐光启的学生，数学家、火器家。1627年[中国天主教](../Page/中国天主教.md "wikilink")[嘉定会议在孙宅举行](../Page/嘉定会议.md "wikilink")。官至[登萊巡撫](../Page/登萊巡撫.md "wikilink")，受[吴桥兵变牵连被处决](../Page/吴桥兵变.md "wikilink")。
      - [李天经](../Page/李天经.md "wikilink")，数学家、天文学家。接替徐光启主持[历局](../Page/历局.md "wikilink")。参与制造象限仪、纪限仪、天球仪、黄赤全仪、日晷、望远镜等十几种欧洲式天文仪器。
      - [韩霖](../Page/韩霖.md "wikilink")（1596-1649）字雨公，号寓庵居士，教名多默（Thomas），曾为入教而休妾（“学道特遣瑶姬”）\[9\]，学兵法于徐光启而“颇闻兵家绪余”、学[铳法于](../Page/铳.md "wikilink")[高则圣](../Page/高则圣.md "wikilink")\[10\]，崇祯九年编明末重要军事著作《[守圉全书](../Page/守圉全书.md "wikilink")》。

<!-- end list -->

  - 深受徐光启影响的同时期学者

<!-- end list -->

  - [陈子龙](../Page/陈子龙.md "wikilink")（[夏完淳的老师](../Page/夏完淳.md "wikilink")），整理定稿、校刻出版并题名《[农政全书](../Page/农政全书.md "wikilink")》，主编巨著《皇明经世文编》（含徐光启文集）

### 反对者

  - 反教[士大夫](../Page/士大夫.md "wikilink")

<!-- end list -->

  - [沈榷](../Page/沈榷.md "wikilink")，[南京教案发起者](../Page/南京教案.md "wikilink")，时任[禮部侍郎署](../Page/禮部侍郎.md "wikilink")[南京禮部尚書](../Page/南京禮部尚書.md "wikilink")。

## 墓地及纪念馆

[The_tomb_of_Xuguangqi.jpg](https://zh.wikipedia.org/wiki/File:The_tomb_of_Xuguangqi.jpg "fig:The_tomb_of_Xuguangqi.jpg")\]\]
[Xu_Guangqi_Memorial_Hall.jpg](https://zh.wikipedia.org/wiki/File:Xu_Guangqi_Memorial_Hall.jpg "fig:Xu_Guangqi_Memorial_Hall.jpg")

崇祯七年（1634年）以一品官规格赐域赐葬，派专使护灵柩归葬上海，因时局不靖，暂厝于[上海县城](../Page/上海老城厢.md "wikilink")[大南门外的](../Page/大南门_\(上海\).md "wikilink")[双园别墅](../Page/双园.md "wikilink")；崇祯十四年（1641年）营葬营葬于[松江府](../Page/松江府.md "wikilink")[上海县高昌乡二十八保六图西南隅](../Page/上海县.md "wikilink")（今[上海市](../Page/上海市.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[徐家汇](../Page/徐家汇.md "wikilink")[光启公园](../Page/光启公园.md "wikilink")）。墓地附近形成村落，后称为[徐家汇](../Page/徐家汇.md "wikilink")。

光绪二十九年（1903年），[天主教江南代牧区在徐光启](../Page/天主教江南代牧区.md "wikilink")[受洗三百周年之际](../Page/受洗.md "wikilink")，重修了墓地。1933年徐光启逝世三百周年，墓地又获重修。[抗日战争期间](../Page/抗日战争.md "wikilink")，墓地荒废成为菜畦。1957年，[上海市文化局重修徐光启墓](../Page/上海市文化局.md "wikilink")，复建十字架基台，并将其列为[上海市文物保护单位](../Page/上海市文物保护单位.md "wikilink")。“[文化大革命](../Page/文化大革命.md "wikilink")”期间，墓地的牌坊、华表遭到破坏。1978年，墓地重辟为南丹公园，此后陆续修建了一系列纪念物。1983年徐光启逝世350周年，南丹公园改名为[光启公园](../Page/光启公园.md "wikilink")。1988年，徐光启墓被国务院列为[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。2003年12月，徐光启墓修复工程竣工，恢复了1903年的墓制。

徐光启墓占地300平方米，高2.2米，为椭圆形大墓。墓地共十个墓穴，葬有徐光启、夫人吴氏和他的四个孙辈夫妇。墓前立有石碑、十字架，石人、石马、[华表和石](../Page/华表.md "wikilink")[牌坊](../Page/牌坊.md "wikilink")。墓碑为数学家[苏步青手书](../Page/苏步青.md "wikilink")“明徐光启之墓”。石牌坊刻有[对联](../Page/对联.md "wikilink")：“治历明农百世师经天纬地，出将入相一个臣奋武揆文”。墓东侧为碑廊，刻有徐光启手迹、[查继佐撰](../Page/查继佐.md "wikilink")《徐光启传》及[程十髮临摹的徐光启画像](../Page/程十髮.md "wikilink")。

上海现存最古老的民居[南春华堂原位于徐汇区](../Page/南春华堂.md "wikilink")[梅陇路南春华堂](../Page/梅陇路.md "wikilink")5号，2003年经抢救性保护搬迁至光启公园并辟为徐光启纪念馆。南春华堂建于明弘治末年至正德年间，距今已有500多年的历史。

## 相关文献

  - 《[崇祯历书](../Page/崇祯历书.md "wikilink")》相关

<!-- end list -->

  - 潘鼐汇
    辑：《崇祯历书（附[西洋新法历书增刊十种](../Page/西洋新法历书.md "wikilink")）》（上下册），上海古籍出版社2009年12月出版（定价：480.00元）
  - [汤若望等](../Page/汤若望.md "wikilink")
    著：《[西洋新法历书](../Page/西洋新法历书.md "wikilink")》，1645年刊本，故宫博物院图书馆藏。

<!-- end list -->

  - 徐光启文集

<!-- end list -->

  - 徐光启病重将亡时将其治国、治历、治兵等方面的疏稿分别辑成《纶扉奏草》、《清台奏草》、《兵事疏》。
  - ［明］抄本《徐文定公奏疏》
  - ［明］[陳子龍](../Page/陳子龍.md "wikilink")（1608－1647）等于崇禎十一年（1638年）編刻：《[皇明經世文編](../Page/皇明經世文編.md "wikilink")》第488-493卷（《徐文定公集》），收錄33篇文章，但曾刪節，非原貌。
  - ［清］[李杕](../Page/李杕.md "wikilink")（1840－1911）編：《徐文定公集》四卷，收錄27篇文章，光緒二十二年（1896）上海[慈母堂鉛印本](../Page/慈母堂.md "wikilink")
      - ［清］[徐允希](../Page/徐允希.md "wikilink")（徐光啟十一世孫）（在李杕編本的基礎上）增訂：《增訂徐文定公集》六卷（前五卷为徐光启的文章，第六卷為李之藻的文稿），宣統元年（1909年）上海慈母堂排印本
          - 1933年[徐宗澤](../Page/徐宗澤.md "wikilink")（徐光啟十二世孫）增補《增訂徐文定公集》成89篇，上海徐家匯天主堂印本
              - 1962年（徐光啟四百年誕辰紀念），[徐懋禧於臺北重印](../Page/徐懋禧.md "wikilink")《增訂徐文定公集》
  - 1963年 [王重民](../Page/王重民.md "wikilink")
    輯校：《徐光启集》十二卷，收錄文章204篇，詩14首（[上海古籍出版社](../Page/上海古籍出版社.md "wikilink")，1984年；臺北：[明文書局](../Page/明文書局.md "wikilink")，1986年）
  - 上海市文物保管委員會 主編：《徐光啟著譯集》（上海：上海古籍出版社，1983年）
  - （[复旦大学](../Page/复旦大学.md "wikilink")）[朱维铮](../Page/朱维铮.md "wikilink")、[李天纲](../Page/李天纲.md "wikilink")
    主编：《徐光启全集》（全十册），上海古籍出版社2010年12月1日第1版（页数：4732；定价：780.00元），ISBN
    978-7-5325-5752-3

<!-- end list -->

  - 徐光启传记

<!-- end list -->

  - ［明］刻印本《[崇禎曆書](../Page/崇禎曆書.md "wikilink")‧治曆緣起》
  - ［明末清初］[徐驥](../Page/徐驥.md "wikilink")（徐光启的独子）：《文定公行實》、《先文定公行述》（見《徐氏家譜》）
  - ［明末清初］比利时传教士、汉学家[柏应理](../Page/柏应理.md "wikilink")（Philippe Couplet,
    1623－1693）：《徐光启行略》
  - ［明末清初］[查继佐](../Page/查继佐.md "wikilink")：《[罪惟录](../Page/罪惟录.md "wikilink")》卷十一下《经济诸臣列传》：《徐光启传》
  - ［明末清初］[邹漪](../Page/邹漪.md "wikilink")：《[启祯野乘](../Page/启祯野乘.md "wikilink")·徐文定传》
  - ［清］[张廷玉等](../Page/张廷玉.md "wikilink")，官修：《钦定[明史](../Page/明史.md "wikilink")》
      - 《明史》卷251 [列传第](../Page/列传.md "wikilink")139：《徐光启传》
      - 《明史》卷31 志第七 历一
      - 《明史》卷25 志第一 天文一
  - 朱傳譽 主編：《徐光啟傳記資料（一）》（臺北：天一出版社，1979年）、《徐光啟傳記資料（二）》（臺北：天一出版社，1981年）

<!-- end list -->

  - 研究书籍

<!-- end list -->

  - [梁家勉](../Page/梁家勉.md "wikilink")　编：《徐光启年谱》，[上海古籍出版社](../Page/上海古籍出版社.md "wikilink")1981年4月出版。统一书号：11186.28
  - 梁家勉 原编，[李天纲](../Page/李天纲.md "wikilink")
    增补：《增补徐光启年谱》，上海古籍出版社2011年11月出版。ISBN
    978-7-5325-5746-2

<!-- end list -->

  - 托名“徐光启”著作

<!-- end list -->

  - ［清］托名“徐光启”：《[破迷](../Page/破迷.md "wikilink")》

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 梁家勉：《徐光启[年谱](../Page/年谱.md "wikilink")》，[上海古籍出版社](../Page/上海古籍出版社.md "wikilink")，1981年4月.
    [统一书号](../Page/统一书号.md "wikilink")：11186.28

## 扩展阅读

  - [江晓原](../Page/江晓原.md "wikilink")：〈[徐光启：有心报国，无力回天——极度的悲剧人物](http://blog.sina.com.cn/s/blog_485f2bc801000b3k.html)〉，原载2007年11月4日《[新民晚报](../Page/新民晚报.md "wikilink")》
  - 蔡孟翰：〈[徐光启曾想征服日本——21世纪的中日大战之三](https://web.archive.org/web/20150220175217/http://beta.dajia.qq.com/blog/408166067544143)〉，2014年6月27日发布于腾讯网《大家》
  - 矩阵：〈[徐光启的苦修](https://web.archive.org/web/20150221100030/http://www.shxhdj.cn/v1/contents/003_02_07/2013-8-27/266581.html)〉，2013年8月27日发布于中共徐汇区委党建网
  - 裴德生、朱鴻林：〈[徐光啟、李之藻、楊廷筠成為天主教徒試釋](https://www.polyu.edu.hk/cc/images/Article/prof_chu_articles/28.pdf)〉。
  - 徐光台：〈\[<http://readopac3.ncl.edu.tw/nclJournal/search/detail.jsp?sysId=0006407084&dtdId=000040&search_type=detail&la=ch&checked=&unchecked=0010006752406,0020006712697,0030006693676,0040006648254,0050006611549,0060006604642,0070006629745,0080006572134,0090006416529,0100006256394,0110006407084,0120006012154,0130006022450,0140005984490,0150005881411,0160005825951,0170005711085,0180003979024,0190005628147,0200005635068>,
    徐光啟演說《泰西水法．水法或問》(1612) 的歷史意義與影響\]〉

## 外部链接

  - 天主教相关

<!-- end list -->

  - [天主教上海教区](../Page/天主教上海教区.md "wikilink")：[徐光启与利玛窦专题](http://www.catholicsh.org/ZhuanLan/XuAndRicci/index.html)

<!-- end list -->

  - 徐光启思想相关

<!-- end list -->

  - [从“会通”到“超胜”：徐光启科学思想的历史价值与当代意义](http://shc2000.sjtu.edu.cn/0601/conghuit.htm)

<!-- end list -->

  - 徐光启著作相关

<!-- end list -->

  - [《农政全书》撰述过程及若干有关问题的探讨](http://www.guoxue.com/economics/ReadNews.asp?NewsID=2861&BigClassName=&BigClassID=21&SmallClassID=61&SpecialID=139)
  - [利玛窦和徐光启翻译《几何原本》的过程](http://shc2000.sjtu.edu.cn/0409/limad.htm)
  - [徐光启与《崇祯历书》](http://shc2000.sjtu.edu.cn/0512/xvguangq.htm)

## 参见

  - **[明朝科技](../Page/明朝科技.md "wikilink")**
      - [明朝农业科技史年表](../Page/明朝农业科技史年表.md "wikilink")
      - 明朝重要科技学者：[朱橚](../Page/朱橚.md "wikilink")、[朱載堉](../Page/朱載堉.md "wikilink")、[潘季驯](../Page/潘季驯.md "wikilink")、[方以智](../Page/方以智.md "wikilink")、[宋应星](../Page/宋应星.md "wikilink")、[李时珍](../Page/李时珍.md "wikilink")、[徐霞客](../Page/徐霞客.md "wikilink")
  - 清朝重要科技学者：[李善兰](../Page/李善兰.md "wikilink")
  - **徐光启与上海**：[徐光启墓](../Page/徐光启墓.md "wikilink")、[徐家汇](../Page/徐家汇.md "wikilink")、[徐家汇街道](../Page/徐家汇街道.md "wikilink")、[徐汇区](../Page/徐汇区.md "wikilink")、[上海人](../Page/上海人.md "wikilink")、[海派文化](../Page/海派文化.md "wikilink")、[上海城市精神](../Page/上海城市精神.md "wikilink")、[天主教上海教区](../Page/天主教上海教区.md "wikilink")、[光启社](../Page/光启社.md "wikilink")、[马相伯](../Page/马相伯.md "wikilink")、[倪桂珍](../Page/倪桂珍.md "wikilink")、[宋耀如](../Page/宋耀如.md "wikilink")、[宋庆龄](../Page/宋庆龄.md "wikilink")
  - [明朝解元列表](../Page/明朝解元列表.md "wikilink")

{{-}}

[徐光启](../Category/徐光启.md "wikilink")
[Category:徐光启家族](../Category/徐光启家族.md "wikilink")
[Category:萬曆二十五年丁酉科舉人](../Category/萬曆二十五年丁酉科舉人.md "wikilink")
[Category:明朝順天鄉試解元](../Category/明朝順天鄉試解元.md "wikilink")
[Category:明朝庶吉士](../Category/明朝庶吉士.md "wikilink")
[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝历局监督](../Category/明朝历局监督.md "wikilink")
[Category:明朝礼部左侍郎](../Category/明朝礼部左侍郎.md "wikilink")
[Category:明朝礼部右侍郎](../Category/明朝礼部右侍郎.md "wikilink")
[Category:明朝礼部尚书](../Category/明朝礼部尚书.md "wikilink")
[Category:明朝内阁次辅](../Category/明朝内阁次辅.md "wikilink")
[Category:明朝东阁大学士](../Category/明朝东阁大学士.md "wikilink")
[Category:明朝文渊阁大学士](../Category/明朝文渊阁大学士.md "wikilink")
[Category:明朝光禄大夫](../Category/明朝光禄大夫.md "wikilink")
[Category:明朝太子太保](../Category/明朝太子太保.md "wikilink")
[Category:明朝追赠少保](../Category/明朝追赠少保.md "wikilink")
[Category:明朝追赠太保](../Category/明朝追赠太保.md "wikilink")
[Category:通才](../Category/通才.md "wikilink")
[Category:明朝经学家](../Category/明朝经学家.md "wikilink")
[Category:明朝思想家](../Category/明朝思想家.md "wikilink")
[Category:明朝翻译家](../Category/明朝翻译家.md "wikilink")
[Category:16世纪数学家](../Category/16世纪数学家.md "wikilink")
[Category:17世纪数学家](../Category/17世纪数学家.md "wikilink")
[Category:明朝数学家](../Category/明朝数学家.md "wikilink")
[Category:明朝科学家](../Category/明朝科学家.md "wikilink")
[Category:明朝天文学家](../Category/明朝天文学家.md "wikilink")
[Category:明朝农学家](../Category/明朝农学家.md "wikilink")
[Category:明朝水利学家](../Category/明朝水利学家.md "wikilink")
[Category:明朝火器家](../Category/明朝火器家.md "wikilink")
[Category:明朝军事家](../Category/明朝军事家.md "wikilink")
[Category:中国军事学家](../Category/中国军事学家.md "wikilink")
[Category:基督教护教士](../Category/基督教护教士.md "wikilink")
[Category:天主之仆](../Category/天主之仆.md "wikilink")
[Category:明朝天主教人物](../Category/明朝天主教人物.md "wikilink")
[Category:上海天主教人物](../Category/上海天主教人物.md "wikilink")
[Category:上海科学家](../Category/上海科学家.md "wikilink")
[G光](../Category/徐姓.md "wikilink")
[Category:諡文定](../Category/諡文定.md "wikilink")
[Category:葬于上海](../Category/葬于上海.md "wikilink")

1.  徐光启《[简平仪说序](../Page/简平仪说.md "wikilink")》

2.  徐光启《历书总目表》

3.
4.  蔡孟翰：[徐光启曾想征服日本——21世纪的中日大战之三](http://beta.dajia.qq.com/blog/408166067544143)
    ，2014年6月27日发表于腾讯网《大家》

5.

6.  [徐光啟將被封聖
    梵蒂岡反應積極](http://www.bbc.co.uk/zhongwen/trad/china/2011/04/110415_vatican_xuguangqi.shtml)

7.  东方早报记者
    杜方舒：[徐光启：四百年来最杰出上海人](http://www.dfdaily.com/html/150/2012/4/25/782100.shtml)，2012年4月25日《[东方早报](../Page/东方早报.md "wikilink")》

8.  [徐光启：第一位近代意义上的“上海人”](http://guoqing.china.com.cn/2013-03/27/content_28375150.htm)，[中国网](../Page/中国网.md "wikilink")“中国国情”，原载于2013年3年27日《[解放日报](../Page/解放日报.md "wikilink")》

9.  [黄景昉](../Page/黄景昉.md "wikilink")《鹿鸠詠·韩雨公幽香谷詠》

10. [乾隆](../Page/乾隆.md "wikilink")（1736-1795）《直隶绛州志》