**徐冠华**（），出生于[上海](../Page/上海.md "wikilink")；[北京林业学院毕业](../Page/北京林业大学.md "wikilink")，遥感应用学专家，[中科院院士](../Page/中科院.md "wikilink")，[香港城市大学荣誉博士](../Page/香港城市大学.md "wikilink")。1984年9月加入[中国共产党](../Page/中国共产党.md "wikilink")，是[中共第十六届中央委员](../Page/中共.md "wikilink")。现任第十一届[全国政协常委](../Page/全国政协.md "wikilink")、教科文卫体委员会主任。曾任[中华人民共和国科学技术部部长](../Page/中华人民共和国科学技术部.md "wikilink")，[中国科学院副院长](../Page/中国科学院.md "wikilink")。

## 早年经历

徐冠华1959年进入[北京林业学院学习](../Page/北京林业大学.md "wikilink")。毕业后，他就被分配到中国林业科学院工作，历任研究实习员、教师、助理研究员、研究员、资源信息所所长等职。1979年至1981年间，徐冠华还曾被派往[瑞典](../Page/瑞典.md "wikilink")[斯德哥尔摩大学](../Page/斯德哥尔摩大学.md "wikilink")，从事遥感数字图象处理的研究工作。1992年，他当选为[中科院地学部学部委员](../Page/中科院.md "wikilink")；次年，被任命为[中科院遥感应用研究所所长](../Page/中科院.md "wikilink")；又是一年后，他晋升为[中科院副院长](../Page/中科院.md "wikilink")。

## 进入政界

1995年，徐冠华开始进入政府部门工作，先后出任[国家科学技术委员会副主任](../Page/中华人民共和国科学技术部.md "wikilink")，党组成员、党组副书记，[科学技术部副部长](../Page/科学技术部.md "wikilink")、党组副书记等职。2001年2月，他接替因任职年龄到届的[朱丽兰出任](../Page/朱丽兰.md "wikilink")[科技部部长](../Page/中华人民共和国科学技术部.md "wikilink")；并在十届[全国人大一次会议上产生的](../Page/全国人大.md "wikilink")[温家宝](../Page/温家宝.md "wikilink")“内阁”中，继续留任[科技部部长](../Page/中华人民共和国科学技术部.md "wikilink")。2007年4月，徐冠华也因任职年龄到届而离职。

在2008年3月的十一届[全国政协一次会议上](../Page/全国政协.md "wikilink")，徐冠华当选[全国政协常委](../Page/全国政协.md "wikilink")、教科文卫体委员会主任。

## 学术成就

徐冠华是[中华人民共和国遥感应用科学领域的专家](../Page/中华人民共和国.md "wikilink")，曾多次获得“有突出贡献的中青年专家”、“有突出贡献的留学回国人员”、“有突出贡献的科技人员”等荣誉称号。[1991年](../Page/1991年.md "wikilink")，他还被[人事部批准享受政府特殊津贴](../Page/人事部.md "wikilink")。以表彰他的学术成就及对社会的贡献，2006年11月15日，[香港城市大学颁授徐冠华荣誉博士](../Page/香港城市大学.md "wikilink")。[2009年6月](../Page/2009年6月.md "wikilink")，[香港中文大学委任徐冠华为该校太空与地球信息科学研究所伟伦太空与地球科学研究客座教授](../Page/香港中文大学.md "wikilink")。[2010年12月](../Page/2010年12月.md "wikilink")，徐冠华被香港中文大学授予荣誉博士学位。

## 参考文献

  - [徐冠华等获香港城市大学颁授荣誉博士学位](http://news.xinhuanet.com/tai_gang_ao/2006-11/15/content_5334589.htm)
  - [徐冠华等获香港中文大学颁授荣誉理学博士学位](http://www.cpr.cuhk.edu.hk/cong/?q=zh-hant/node/49)

{{-}}

[X徐](../Category/中华人民共和国科学技术部部长.md "wikilink")
[X徐](../Category/中国遥感地学家.md "wikilink")
[X徐](../Category/香港城市大學榮譽博士.md "wikilink")
[X徐](../Category/第十一届全国政协常务委员.md "wikilink")
[X徐](../Category/上海人.md "wikilink") [G](../Category/徐姓.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:北京林学院校友](../Category/北京林学院校友.md "wikilink")