[Xu_Siceng.jpg](https://zh.wikipedia.org/wiki/File:Xu_Siceng.jpg "fig:Xu_Siceng.jpg")
**徐嗣曾**（），[字](../Page/表字.md "wikilink")**宛東**，[浙江](../Page/浙江.md "wikilink")[海寧人](../Page/海寧.md "wikilink")，[清朝官員](../Page/清朝.md "wikilink")。

## 生平

本姓[楊](../Page/楊.md "wikilink")，[過繼爲](../Page/過繼.md "wikilink")[徐氏之後](../Page/徐氏.md "wikilink")。[乾隆二十八年](../Page/乾隆.md "wikilink")（1763年）癸未科[進士](../Page/進士.md "wikilink")。授[戶部](../Page/戶部.md "wikilink")[主事](../Page/主事.md "wikilink")，升[員外郎](../Page/員外郎.md "wikilink")、[郎中](../Page/郎中.md "wikilink")。[乾隆三十六年](../Page/乾隆.md "wikilink")（1771年）出督[陝西](../Page/陝西.md "wikilink")[學政](../Page/學政.md "wikilink")。[乾隆四十年](../Page/乾隆.md "wikilink")（1775年）任[雲南迤東道](../Page/雲南.md "wikilink")，歷改糧儲道、迤西道。[乾隆四十四年](../Page/乾隆.md "wikilink")（1779年）升[安徽](../Page/安徽.md "wikilink")[按察使](../Page/按察使.md "wikilink")，同年轉任[雲南](../Page/雲南.md "wikilink")[按察使](../Page/按察使.md "wikilink")。[乾隆四十七年](../Page/乾隆.md "wikilink")（1782年）任[福建](../Page/福建.md "wikilink")[布政使](../Page/布政使.md "wikilink")。[乾隆五十年](../Page/乾隆.md "wikilink")（1785年）升[福建巡撫](../Page/福建巡撫.md "wikilink")，期間曾短期署理[閩浙總督職務](../Page/閩浙總督.md "wikilink")。[乾隆五十五年](../Page/乾隆.md "wikilink")（1790年）卒。\[1\]

## 註釋

## 參考文獻

  - 《清史稿》
  - 《清國史館傳稿》5841號
  - 《福建省志》，福建省地方誌編纂委員會編，1992年。

## 外部連結

  - [徐嗣曾](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%AE%7D%B6%E0%B4%BF)
    中研院史語所

{{-}}

[Category:清朝陝西學政](../Category/清朝陝西學政.md "wikilink")
[Category:清朝安徽按察使](../Category/清朝安徽按察使.md "wikilink")
[Category:清朝雲南按察使](../Category/清朝雲南按察使.md "wikilink")
[Category:清朝福建巡抚](../Category/清朝福建巡抚.md "wikilink")
[Category:海寧人](../Category/海寧人.md "wikilink")
[S](../Category/徐姓.md "wikilink")

1.  《清史稿·卷三百三十二·列傳一百十九》：徐嗣曾，字宛东，实杨氏，出为徐氏后，浙江海宁人。乾隆二十八年进士，授户部主事。再迁郎中。四十年，授云南迤东道。累迁福建[布政使](../Page/布政使.md "wikilink")。五十年，擢巡抚。五十二年，[台湾民](../Page/台湾.md "wikilink")[林爽文为乱](../Page/林爽文事件.md "wikilink")，调[浙江兵](../Page/浙江.md "wikilink")，经[延平吉溪塘](../Page/延平區.md "wikilink")，兵有溺者，嗣曾坐不能督察，下吏议。乱既定，五十三年，命赴台湾勘建城垣，因命偕[福康安](../Page/福康安.md "wikilink")、[李侍尧按](../Page/李侍尧.md "wikilink")[柴大纪贪劣状](../Page/柴大纪.md "wikilink")，上责嗣曾平日缄默不言。寻疏言大纪废弛[行伍](../Page/軍隊.md "wikilink")，贪婪营私，事迹昭著。又奏：“抚恤被难流民，给银折米，福建旧例，石准银二两；今以米贵，请改为三两。”上以福康安奏晴雨及时，岁可丰收，仍令视旧例。偕福康安等奏清察积弊，筹酌善后诸事，均得旨允行。尝以台湾吏治废弛，不能早行觉察，自劾，上原之。命台湾建福康安、[海兰察](../Page/海兰察.md "wikilink")[生祠](../Page/生祠.md "wikilink")，以嗣曾并列。寻奏台湾海疆刁悍，治乱用严，民为盗及杀人者，役殃民，兵冒粮，及助战守义民或挟嫌害良，皆立置典刑，以是称上旨，嘉嗣曾不负任使。事觕定，命内渡，寻又命俟[总兵](../Page/总兵.md "wikilink")[奎林至乃行](../Page/奎林.md "wikilink")。[庄大田者](../Page/庄大田.md "wikilink")，与[爽文同乱](../Page/林爽文.md "wikilink")，坐诛，嗣曾捕得其子天畏及用事者黄天养送[京师](../Page/京师.md "wikilink")，又得[海盗](../Page/海盗.md "wikilink")，立诛之。五十四年，赐[孔雀翎](../Page/孔雀翎.md "wikilink")、大小[荷包](../Page/荷包.md "wikilink")。图像紫光阁。请入觐，未行，[安南](../Page/越南.md "wikilink")[阮光平据](../Page/阮光平.md "wikilink")[黎城](../Page/黎城.md "wikilink")，福康安督兵赴[广西](../Page/广西.md "wikilink")，嗣曾署[总督](../Page/总督.md "wikilink")。福康安濒行，奏福建文武废弛，宜大加惩创，上谕嗣曾振刷整顿。嗣曾奏许[琉球市](../Page/琉球.md "wikilink")[大黄](../Page/大黄.md "wikilink")，限三五百斤，谕不可因噎废食。又奏：“福建民多聚族而居，有为盗，责族正举首，教约有方，给顶带；盗但附从行劫未杀人拒捕，自首，拟[斩监候](../Page/斬首.md "wikilink")，三年发遣，免死。”上谕曰：“捕盗责在将吏。令族正举首，设将吏何用？族正皆土豪，假以事权，将何所不为？福建多盗，当严治。若行劫后尚许自首免死，何以示儆？二条俱属错谬。”
    五十五年，[高宗八旬万寿](../Page/高宗.md "wikilink")，台湾[生番头人请赴京祝嘏](../Page/生番.md "wikilink")，嗣曾以闻，命率诣[热河](../Page/热河.md "wikilink")[行在瞻觐](../Page/行在.md "wikilink")。十一月，回任，次[山东台庄](../Page/山东.md "wikilink")，病作，遂卒。