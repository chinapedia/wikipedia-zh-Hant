**徐子淇**（****，）， 昵称“千亿新抱”，
[香港出生](../Page/香港.md "wikilink")，[香港前](../Page/香港.md "wikilink")[模特兒及](../Page/模特兒.md "wikilink")[電影](../Page/電影.md "wikilink")[演員香港](../Page/演員.md "wikilink")[第二首富](../Page/富豪.md "wikilink")[李兆基二子](../Page/李兆基.md "wikilink")[李家誠之妻](../Page/李家誠.md "wikilink")。

徐子淇在[澳洲長大](../Page/澳洲.md "wikilink")，後與家人回港，就讀[南島中學](../Page/南島中學.md "wikilink")。14歲時，被時裝設計師[Pacino
Wan](../Page/Pacino_Wan.md "wikilink")[尹泰尉發掘](../Page/尹泰尉.md "wikilink")，在他的時裝表演中擔當模特兒，甜美面孔初露鋒芒。接著更加入了模特兒公司Elite成為模特兒。後被[導演](../Page/導演.md "wikilink")[陳嘉上邀請試鏡](../Page/陳嘉上.md "wikilink")，並於電影《[誤人子弟](../Page/誤人子弟.md "wikilink")》中飾演教師。

她先前往[英國](../Page/英國.md "wikilink")[倫敦大學的](../Page/倫敦大學.md "wikilink")[倫敦大學學院攻讀東歐研究與經濟學士課程](../Page/倫敦大學學院.md "wikilink")，2005年於[倫敦政治經濟學院完成性別與媒體碩士課程](../Page/倫敦政治經濟學院.md "wikilink")。

## 私人生活

  - 2000年4月，均為劇集《[新鮮人](../Page/新鮮人.md "wikilink")》演員的徐子淇和[洪天明傳出緋聞](../Page/洪天明.md "wikilink")。\[1\]
    二人多次被[記者發現](../Page/記者.md "wikilink")[拍拖和](../Page/戀愛.md "wikilink")[親嘴](../Page/親吻.md "wikilink")，但兩年來兩人從未正式承認戀情。有報道指徐母嫌棄洪天明，一直阻止二人發展，期間多次傳出兩人離離合合的消息。直到2002年，徐子淇從[英國返港放](../Page/英國.md "wikilink")[暑假](../Page/暑假.md "wikilink")，二人終承認分手。

<!-- end list -->

  - 2003年，她被發現與比她大一歲的Brian在街上擁抱，其後她默認了新戀情，更有指徐母很滿意Brian。不過戀情只維持了一年，徐子淇指二人長時間分隔異地，感情轉淡，分手使她感嘆息\[2\]。

<!-- end list -->

  - 香港名人[霍震霆之子](../Page/霍震霆.md "wikilink")[霍啟山在英國讀書時](../Page/霍啟山.md "wikilink")，曾與徐子淇拍拖一個月，二人經常在[酒吧出現](../Page/酒吧.md "wikilink")，不避嫌表現親暱，回港後亦有約會。\[3\]
    事後兩人否認戀情，謂只是在英國認識的好朋友\[4\]。

<!-- end list -->

  - 2004年7月，徐子淇結識[恆基兆業創辦人兼董事局主席](../Page/恆基兆業.md "wikilink")[李兆基之幼子](../Page/李兆基.md "wikilink")[李家誠](../Page/李家誠.md "wikilink")，雖然傳出兩人拍拖，但徐子淇極力否認，並指雙方只是朋友\[5\]。但消息傳出不足一周，徐子淇即在[中環開設時裝店Clover](../Page/中環.md "wikilink")。直至8月，二人在[海邊拖手漫步的照片曝光](../Page/海邊.md "wikilink")，徐才承認二人戀情，又指曾因報道而感到[壓力](../Page/壓力.md "wikilink")\[6\]。

<!-- end list -->

  - 2005年9月，報道指李不准徐再涉足娛樂界。10月再傳二人分手，但徐否認。2006年9月，[周刊傳出徐已](../Page/雜誌.md "wikilink")[懷孕](../Page/懷孕.md "wikilink")5個月。

<!-- end list -->

  - 2006年12月，傳媒曾報導徐子淇將與李家誠在[美國](../Page/美國.md "wikilink")[洛杉磯舉行婚禮](../Page/洛杉磯.md "wikilink")，二人不置可否。12月14日，徐子淇、李家誠及雙方家屬乘私人飛機抵達[澳洲](../Page/澳洲.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")。2006年12月15日，李兆基聯同徐之家長徐傳順和彭雪芳在各大[香港報紙](../Page/香港報紙.md "wikilink")，刊出李家誠與徐子淇婚禮公告，同日上午11時30分在澳洲[悉尼皇家植物園舉行婚禮](../Page/悉尼皇家植物園.md "wikilink")。

## 家庭及子女

徐子淇已婚，夫婿為恒基兆業董事局主席李兆基的次子[李家誠](../Page/李家誠.md "wikilink")，育有四名儿女，長女李晞彤（Leanna
Lee, 2007年出生)、次女李晞兒(Hayley Lee, 2009年出生)、三子李俊熹(Triston Lee,
2011年出生)。\[7\]\[8\]\[9\]
2015年9月27日，徐子淇於[Instagram宣佈懷有第四胎](../Page/Instagram.md "wikilink")。2015年10月初，誕下5.7磅次子李建熹。\[10\]\[11\]

## 電影

  - 1997年：《[誤人子弟](../Page/誤人子弟.md "wikilink")》
  - 2000年：《[順流逆流](../Page/順流逆流.md "wikilink")》
  - 2000年：《[愛我別走](../Page/愛我別走.md "wikilink")》
  - 2001年：《[靚女差館](../Page/靚女差館.md "wikilink")》
  - 2001年：《[最激之手](../Page/最激之手.md "wikilink")》
  - 2007年：《[那個傻瓜愛過你](../Page/那個傻瓜愛過你.md "wikilink")》

## 電視劇

  - 2004年：[無綫電視](../Page/無綫電視.md "wikilink")《[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")》
    飾 關莜男（Coco）
  - 2000年：[無綫電視](../Page/無綫電視.md "wikilink")《[新鮮人](../Page/新鮮人.md "wikilink")》
    飾 阮盈（Queenie）

## 參見

  - [李兆基家族](../Page/李兆基家族.md "wikilink")

## 資料來源

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:李兆基家族](../Category/李兆基家族.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:澳大利亞華人](../Category/澳大利亞華人.md "wikilink")
[Category:伦敦大学学院校友](../Category/伦敦大学学院校友.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:南島中學校友](../Category/南島中學校友.md "wikilink")
[chi](../Category/徐姓.md "wikilink")
[Category:社交名媛](../Category/社交名媛.md "wikilink")

1.  [重演「灰姑娘」
    徐子淇退出娛樂圈等待嫁入豪門](http://news.xinhuanet.com/ent/2005-12/13/content_3914328.htm)，新華網

2.
3.  [徐子淇霍啟山睇戲爆谷兩份食 讀者揭發密會
    當李家誠冇到](http://the-sun.orisun.com/channels/ent/20050721/20050720233937_0003.html)

4.  [霍啟山否認曾追徐子淇
    和章子怡只是朋友](http://news.eastday.com/eastday/news/news/node4945/node26243/userobject1ai379932.html)

5.
6.
7.  [渴望徐子淇得子繼香火，李兆基西安求神賜男孫。](http://enjoy.eastday.com/e/20070425/u1a2792547.html)

8.  [徐子淇又生女李兆基懶得慶祝](http://www.takungpao.com/news/09/05/22/images_0705-1086315.htm)


9.  [徐子淇誕 6磅仔繼續生](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20110622&sec_id=462&art_id=15363043)

10. [【夜半報喜】徐子淇深夜晒事業線宣佈：就快生！](http://hk.apple.nextmedia.com/enews/realtime/20150927/54250242)

11. [徐子淇深夜放相：Ready to
    POP！](http://hk.on.cc/hk/bkn/cnt/entertainment/20150927/bkn-20150927040404099-0927_00862_001.html)