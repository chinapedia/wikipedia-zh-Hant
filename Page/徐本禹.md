**徐本禹**（），[山东](../Page/山东.md "wikilink")[聊城人](../Page/聊城.md "wikilink")，[感动中国年度人物](../Page/感动中国.md "wikilink")，现任[湖北](../Page/湖北.md "wikilink")[秭归县委副书记](../Page/秭归.md "wikilink")。\[1\]出生於[山東](../Page/山東.md "wikilink")[聊城的一個貧窮農村家庭](../Page/聊城.md "wikilink")，1999-2003年就讀於[華中農業大學](../Page/華中農業大學.md "wikilink")。2003年考取[農業經濟管理專業](../Page/農業經濟管理專業.md "wikilink")[公費研究生](../Page/公費研究生.md "wikilink")，但沒有立即就讀，到[貴州省](../Page/貴州省.md "wikilink")[大方縣](../Page/大方縣.md "wikilink")[貓場鎮](../Page/貓場鎮.md "wikilink")[狗吊岩村](../Page/狗吊岩村.md "wikilink")[岩洞小學與](../Page/岩洞小學.md "wikilink")[大水鄉](../Page/大水鄉.md "wikilink")[大石村](../Page/大石村.md "wikilink")[大石小學](../Page/大石小學.md "wikilink")[支教](../Page/支教.md "wikilink")。徐本禹因[天涯社區的文章](../Page/天涯社區.md "wikilink")《两所山村小学和一个支教者》而被中國人所熟知，後獲選[中國中央電視台](../Page/中國中央電視台.md "wikilink")“[感动中国](../Page/感动中国.md "wikilink")·2004年度人物”。

作爲[中非合作論壇後中國派往](../Page/中非合作論壇.md "wikilink")[非洲的第一批志願者](../Page/非洲.md "wikilink")，也是中國首次往[津巴布韦選派志願者](../Page/津巴布韦.md "wikilink")。2007年1月24日，前往[津巴布韦進行](../Page/津巴布韦.md "wikilink")[漢語教學](../Page/漢語.md "wikilink")。同年10月，徐本禹作為代表出席在[北京召開的](../Page/北京.md "wikilink")[中共十七大](../Page/中共十七大.md "wikilink")。

## 参考资料

## 外部連結

  - [徐本禹在新浪網上的網志](http://blog.sina.com.cn/xubenyu)
  - [徐本禹：说说大学生对十七大期待](http://news.sohu.com/20071018/n252710799.shtml)
  - [两所乡村小学和一个支教者](http://www.tianya.cn/new/Publicforum/Content.asp?idWriter=0&Key=0&strItem=no04&idArticle=284603&flag=1)
  - [央视专访支教大学生徐本禹：为了大山里的渴望](http://news.sina.com.cn/c/2005-01-14/18115549848.shtml)

[Category:中共十七大代表](../Category/中共十七大代表.md "wikilink")
[Category:感动中国年度人物](../Category/感动中国年度人物.md "wikilink")
[Category:华中农业大学校友](../Category/华中农业大学校友.md "wikilink")
[Category:聊城人](../Category/聊城人.md "wikilink")
[B本禹](../Category/徐姓.md "wikilink")

1.