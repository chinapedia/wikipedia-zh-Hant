**徐樾**（），[字](../Page/表字.md "wikilink")**子直**，[號](../Page/號.md "wikilink")**波石**，[江西](../Page/江西.md "wikilink")[貴溪縣人](../Page/貴溪縣.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")，嘉靖進士，官至[雲南左](../Page/雲南.md "wikilink")[布政使](../Page/布政使.md "wikilink")，在[沅江土酋之亂中遇難](../Page/沅江.md "wikilink")。

## 生平

[嘉靖十一年](../Page/嘉靖.md "wikilink")（1532年）成進士，任[禮部](../Page/禮部.md "wikilink")[郎中](../Page/郎中.md "wikilink")。[嘉靖十八年](../Page/嘉靖.md "wikilink")（1539年）擢升为[福建](../Page/福建.md "wikilink")[参议](../Page/参议.md "wikilink")。[嘉靖二十三年](../Page/嘉靖.md "wikilink")（1544年）以副使督学[贵州](../Page/贵州.md "wikilink")。

[嘉靖二十五年](../Page/嘉靖.md "wikilink")（1546年）[云南](../Page/云南.md "wikilink")[元江府](../Page/元江府.md "wikilink")[知府那宪被叔父那鉴弑杀](../Page/知府.md "wikilink")，那鉴发动叛乱。[明廷调集兵马](../Page/明廷.md "wikilink")，兵分五路进行清剿，叛军诈降。[嘉靖三十一年](../Page/嘉靖.md "wikilink")（1552年）徐樾擢升为[云南](../Page/云南.md "wikilink")[左布政使](../Page/左布政使.md "wikilink")，押着军饷到达南羡（今[元江县境内](../Page/元江县.md "wikilink")），逢那氏詐降，那鉴以埋伏[大象以及兵马杀出](../Page/大象.md "wikilink")，徐樾“以布政使請兵督戰而死”\[1\]\[2\]，《[明史](../Page/明史.md "wikilink")》載：“土官高鵠當元江之變布政司徐樾遇害，奮身赴救，死之。”其弟子[顏鈞尋其骸骨](../Page/顏鈞.md "wikilink")，歸葬於心齋墓庵。[云南巡抚](../Page/云南巡抚.md "wikilink")[赵炳然上报朝廷](../Page/赵炳然.md "wikilink")，追赠为[光禄寺卿](../Page/光禄寺卿.md "wikilink")。

## 家族

其父徐灌，生子徐棐、徐樾。娶[御史](../Page/御史.md "wikilink")[詹昇之女](../Page/詹昇.md "wikilink")。

## 著作

徐樾少從[王守仁學](../Page/王守仁.md "wikilink")[心學](../Page/心學.md "wikilink")，[嘉靖七年](../Page/嘉靖.md "wikilink")（1528年）又受業於[王艮](../Page/王艮.md "wikilink")\[3\]。著有《日省仁學錄》。[孙应鳌整理他的遗稿编为](../Page/孙应鳌.md "wikilink")《徐子直集》。徐樾未生育，以兄长徐棐的幼子徐罃为子。

## 參考文獻

[Category:明朝禮部郎中](../Category/明朝禮部郎中.md "wikilink")
[Category:明朝福建布政使司參議](../Category/明朝福建布政使司參議.md "wikilink")
[Category:明朝貴州按察使司副使](../Category/明朝貴州按察使司副使.md "wikilink")
[Category:明朝雲南布政使](../Category/明朝雲南布政使.md "wikilink")
[Category:明朝戰爭身亡者](../Category/明朝戰爭身亡者.md "wikilink")
[Category:明朝心學家](../Category/明朝心學家.md "wikilink")
[Category:贵溪人](../Category/贵溪人.md "wikilink")
[Y](../Category/徐姓.md "wikilink")

1.  《漢籍電子文獻資料庫-明人傳記資料索引》4940：徐樾，字子直，號波石，貴溪人。初從王守仁遊，復受業于王艮，得其傳。擧嘉靖十一年進士，歷官雲南左布政，死沅江土酋之難。有波石集。
2.  《明儒学案》：布政徐波石先生樾
    徐樾字子直，号波石，贵溪人。嘉靖十一年进士。历官部郎，出任臬藩。三十一年，陞云南左布政使。元江府土舍那鑑，弑其知府那宪，攻劫州县，朝议讨之。总兵沐朝弼、巡抚石简会师，分五哨进勦。那鑑遣经历张惟至监军佥事王养浩所伪降，养浩疑不敢往。先生以督饷至军，慨然请行。至元江府南门外，鑑不出迎。先生呵问，伏兵起而害之。姚安土官高鹄力救，亦战殁。我兵连岁攻之不克。会鑑死，诸酋愿纳象赎罪，世宗厌兵，遂允之。时人为之语曰：“可怜二品承宣使，只值元江象八条。”伤罪人之不得也。
3.  《明儒学案》：布政徐波石先生樾
    先生少与夏相才名相亚，得事阳明，继而卒业心斋之门。先生操存过苦，常与心斋步月下，刻刻简默，心斋厉声曰：“天地不交否？”又一夕至小渠，心斋跃过，顾谓先生曰：“何多拟议也？”先生过渠，顿然若失，既而叹曰：“从前孤负此翁，为某费却许多气力。”先生谓：“六合也者，心之郛廓；四海也者，心之边际；万物也者，心之形色。往古来今，惟有此心浩浩渊渊，不可得而测而穷也。此心自朝至暮，能闻能见，能孝能弟，无间昼夜，不须计度，自然明觉，与天同流。一入声臭，即是意念，是己私也。人之日用起居食息，谁非天者？即此是真知真识，又从而知识之，是二知识也。人身之痛痒视听，无不觉者，此觉之外，更有觉乎？愚不肖者，未尝离此为体，奚谓不知？不自知其用处是性，故曰‘蠢动’。是以动处是觉，觉处亦昏昧也。”此即现成良知之言，以不犯做手为妙诀者也。心斋常谓先生曰：“何谓至善？”曰：“至善即性善。”曰：“性即道乎？”曰：“然。”曰：“道与身孰尊？身与道何异？”曰：“一也。”曰：“今子之身能尊乎？否欤？”先生避席请问曰：“何哉，夫子之所谓尊身也？”心斋曰：“身与道原是一件，至尊者此道，至尊者此身。尊身不尊道，不谓之尊身，尊道不尊身，不谓之尊道。道尊身尊，才是至善。故曰‘天下有道，以道狥身；天下无道，以身狥道。’若以道狥人，妾妇之道也。己不能尊信，又岂能使彼尊信哉！”先生拜而谢曰：“某甚惭於夫子之教。”即以受降一事论之，先生职主督饷，受降非其分内，冒昧一往，即不敢以喜功议先生，其於尊身之道，则有间矣。