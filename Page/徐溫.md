**徐溫**（），[字](../Page/表字.md "wikilink")**敦美**，海州胊山（今[江蘇](../Page/江蘇.md "wikilink")[東海](../Page/東海縣.md "wikilink")）人，唐末軍事人物。[南唐建立者](../Page/南唐.md "wikilink")[徐知誥](../Page/徐知誥.md "wikilink")（[李昪](../Page/李昪.md "wikilink")）養父，廟號義祖。

## 生平

徐溫出身盐贩，為淮南[節度使](../Page/節度使.md "wikilink")、[吳王](../Page/南吳.md "wikilink")[楊行密帳下右衙指揮使](../Page/楊行密.md "wikilink")。[唐哀帝](../Page/唐哀帝.md "wikilink")[天祐二年](../Page/天祐_\(唐朝\).md "wikilink")（905年），楊行密去世，長子[楊渥繼立](../Page/楊渥.md "wikilink")，调动自己在宣州的亲兵将领朱思勍等人入卫，將領們不安。因此徐溫與左衙指揮使[張顥於天祐四年](../Page/張顥.md "wikilink")（907年）發動[政變](../Page/政變.md "wikilink")，攻灭朱思勍，共掌軍政，楊氏大權旁落。

次年（908年）与张颢殺楊渥，立楊行密次子[楊隆演](../Page/楊隆演.md "wikilink")。张颢意欲排挤徐温出外，但淮南谋士[严可求与徐温交好](../Page/严可求.md "wikilink")，说服节度副使[李承嗣一同干预](../Page/李承嗣.md "wikilink")，遂未果。不久，徐溫又殺張顥，将杀杨渥之事单独归咎之，遂獨攬大權。徐溫個性多疑，只信任謀士[駱知祥](../Page/駱知祥.md "wikilink")、[嚴可求而已](../Page/嚴可求.md "wikilink")，掌權後逐步翦除楊氏舊將勢力。

911年，[天祐八年](../Page/天祐_\(唐朝\).md "wikilink")，为镇海军节度使、同中书门下平章事。派兵攻打吴越国。当年秋，吴越国反击，进攻常州，吴、吴越战于常州，吴越败北。

南吳天祐十二年（915年）徐溫受封為管內水陸馬步諸軍都指揮使、兩浙都招討使、守[侍中](../Page/侍中.md "wikilink")、齊國公，鎮潤州（今江蘇[鎮江](../Page/鎮江.md "wikilink")）。

天佑十六年（919年）[杨隆演登吳國王位](../Page/杨隆演.md "wikilink")，封徐溫為大[丞相](../Page/丞相.md "wikilink")、都督中外諸軍事、諸道都統、鎮海、寧國節度使、守[太尉兼](../Page/太尉.md "wikilink")[中書令](../Page/中書令.md "wikilink")、**東海郡王**。其子[徐知训因骄傲荒淫為](../Page/徐知训.md "wikilink")[朱瑾所害後](../Page/朱瑾.md "wikilink")，因其他兒子都年幼且能力不足，徐温只得讓養子[徐知诰代徐知训管理政事](../Page/徐知诰.md "wikilink")，還把朱瑾的尸体沉入雷塘，并杀死可能有意联络朱瑾的泰宁军节度使[米志诚及其诸子](../Page/米志诚.md "wikilink")。徐温对徐知训的胡作非为尚不知情，并且想[肃清朝中](../Page/肃清.md "wikilink")[大臣](../Page/大臣.md "wikilink")。经过徐知诰等人的说明之后，方才解怒。处罚了负责教育知训的人。随后回到[金陵](../Page/金陵.md "wikilink")，将一切政务交由知诰处置。[順義七年](../Page/顺义_\(南吴\).md "wikilink")（927年）徐溫去世，被追封為**齊王**，[諡](../Page/諡.md "wikilink")**忠武王**。虽然严可求等谋士一直力劝，徐温一直不愿以亲子取代徐知诰，徐温的妾[陳夫人也认为](../Page/陳夫人_\(徐溫\).md "wikilink")，既然在贫贱时以徐知诰为[養子](../Page/養子.md "wikilink")，就不应该在富贵时将其抛弃。徐温直至临死前才有意让亲生子[徐知询取代徐知诰](../Page/徐知询.md "wikilink")，写了表文让徐知询上表杨溥，徐知诰闻讯也有意请辞求任[镇南节度使](../Page/镇南节度使.md "wikilink")，但徐知询尚未将表文送达，徐温已死，徐知询折返奔丧，徐知誥也因而没有辞职，并得以繼位。

## 家庭

徐温原配[白夫人](../Page/白夫人_\(徐溫\).md "wikilink")，继妻[李氏](../Page/明德李皇后_\(南唐\).md "wikilink")，还有妾陈夫人等。共有六子：[徐知训](../Page/徐知训.md "wikilink")、[徐知询](../Page/徐知询.md "wikilink")、[徐知诲](../Page/徐知诲.md "wikilink")、[徐知谏](../Page/徐知谏.md "wikilink")、[徐知证](../Page/徐知证.md "wikilink")、[徐知谔](../Page/徐知谔.md "wikilink")，可能还有早死的长子\[1\]。另有一女出家，二女分别嫁[李建勋和](../Page/李建勋.md "wikilink")[王崇文](../Page/王崇文_\(南唐\).md "wikilink")，后都被封为[广德长公主](../Page/广德长公主.md "wikilink")。

徐知誥建[南唐後](../Page/南唐.md "wikilink")，追諡徐溫為[武皇帝](../Page/武皇帝.md "wikilink")，[廟號](../Page/廟號.md "wikilink")[太祖](../Page/太祖.md "wikilink")，陵墓号为**定陵**。徐知誥恢復原姓，並改名李昪後，再改徐溫廟號為[義祖](../Page/義祖.md "wikilink")。徐溫第二任夫人[李夫人尊為明德皇后](../Page/明德皇后_\(徐溫\).md "wikilink")。\[2\]

徐温的儿子徐知证、徐知谔在[宋朝時顯靈](../Page/宋朝.md "wikilink")，展示[神蹟](../Page/神蹟.md "wikilink")，成为[福建信仰的](../Page/福建.md "wikilink")[二徐真人](../Page/二徐真人.md "wikilink")，徐溫第一任夫人白夫人也得到了[道士的祭祀](../Page/道士.md "wikilink")。徐温因而也被祭拜，[宋理宗封为](../Page/宋理宗.md "wikilink")**忠武真人**，其妻[白夫人为仁寿仙妃](../Page/白夫人_\(徐溫\).md "wikilink")。

## 注释

[X](../Category/五代十国人物.md "wikilink")
[Category:南吴人物](../Category/南吴人物.md "wikilink")
[X](../Category/南唐追尊皇帝.md "wikilink")
[X](../Category/中国人物神.md "wikilink")
[X](../Category/道教神祇.md "wikilink")
[W](../Category/徐姓.md "wikilink")
[X徐](../Category/谥忠武.md "wikilink")

1.  《资治通鉴》载徐知训被宋齐丘称为“三郎”，《十国春秋》载徐温称徐知诰为诸子的“二哥”。
2.  一說徐知誥先建[徐齊](../Page/徐齊.md "wikilink")，以徐溫廟號為太祖，待改國號為大唐（南唐）後，始改為義祖。