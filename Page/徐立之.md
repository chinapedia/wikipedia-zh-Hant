[LapCheeTsui.jpg](https://zh.wikipedia.org/wiki/File:LapCheeTsui.jpg "fig:LapCheeTsui.jpg")

**徐立之**，<small>[OC](../Page/加拿大勳章.md "wikilink")</small>（，），加拿大華裔分子遺傳學家。[香港中文大學理學士及碩士](../Page/香港中文大學.md "wikilink")，美國[匹茲堡大學](../Page/匹茲堡大學.md "wikilink")[哲學博士](../Page/哲學博士.md "wikilink")（生物學），於2002年9月至2014年3月間擔任[香港大學校長](../Page/香港大學.md "wikilink")。現任[港科院院長](../Page/港科院.md "wikilink")、[香港科技園董事](../Page/香港科技園.md "wikilink")、中大[新亞書院校董](../Page/新亞書院.md "wikilink")、[珠海學院校董](../Page/珠海學院.md "wikilink")、[浙江大學求是高等研究院院長](../Page/浙江大學.md "wikilink")，同時為上市公司[培力控股的獨立非執行董事](../Page/培力控股.md "wikilink")。\[1\]\[2\]

## 生平

徐立之祖籍[杭州](../Page/杭州.md "wikilink")，出生於[上海](../Page/上海.md "wikilink")，在[香港長大](../Page/香港.md "wikilink")，小學就讀過於位處[佐敦谷八座](../Page/佐敦谷.md "wikilink")（但已被清拆）[庇護十二學校](../Page/庇護十二學校.md "wikilink")\[3\]\[4\]、[香港真光中學](../Page/香港真光中學.md "wikilink")（小學部）、昔日位於鑽石山的恩光小學以及一間已結束營辦的北角小學\[5\]。他中學就讀於當時稱為「巴富街官立中學」的[何文田官立中學](../Page/何文田官立中學.md "wikilink")，在中五會考時取得2A4C的良好成績（A的是英文科和生物科）。到了預科，即大學入學試時只有生物一科取得A級成績\[6\]。徐立之之後入讀[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院生物系](../Page/新亞書院.md "wikilink")，1972年僅獲理學士三級榮譽學位\[7\]，1974年於[香港中文大學完成碩士後](../Page/香港中文大學.md "wikilink")，負笈[美國](../Page/美國.md "wikilink")[匹茲堡大學](../Page/匹茲堡大學.md "wikilink")，1979年取得博士學位。

其後，徐立之於美國[田納西州Oak](../Page/田納西州.md "wikilink")
Ridge國家實驗室及[加拿大](../Page/加拿大.md "wikilink")遺傳系進行博士後研究，及後於[多倫多大學出任教席](../Page/多倫多大學.md "wikilink")，並任多倫多病童醫院研究所遺傳系主任及首席遺傳學家，同時為多倫多大學教授及H.E.
Sellers[囊狀纖維症講座教授](../Page/囊狀纖維症.md "wikilink")。於2002年9月起接替[戴義安出任](../Page/戴義安.md "wikilink")[香港大學校長](../Page/香港大學.md "wikilink")。

徐立之同時是[中央研究院院士](../Page/中央研究院.md "wikilink")、[英国皇家学会會員](../Page/英国皇家学会.md "wikilink")、[加拿大皇家學會會員](../Page/加拿大皇家學會.md "wikilink")、加拿大醫學研究局資深研究員、[美國侯活曉士醫學研究所國際學人](../Page/美國侯活曉士醫學研究所.md "wikilink")、[美國國家科學院外籍院士](../Page/美國國家科學院.md "wikilink")、[國際創新基金會院士](../Page/國際創新基金會.md "wikilink")，並於2000年至2002年曾擔任[人類基因組織](../Page/国际人类基因组组织.md "wikilink")（HUGO）的會長一職。他曾獲加拿大勳章、[安大略省勳章](../Page/安大略省.md "wikilink")、加拿大國會Killam獎，以及[香港中文大學等多所大學頒授榮譽博士學位](../Page/香港中文大學.md "wikilink")。

就任香港大學校長期間，徐立之除了處理一般校內事務外，還致力發展港大成為基因遺傳方面的研究中心。他也十分重視與大學各方成員的溝通，多次出訪與海外校友會面，並為香港大學籌募經費而到處奔波。

2003年，徐立之帶領[香港大學醫學院團隊最先發現](../Page/香港大學.md "wikilink")[SARS](../Page/嚴重急性呼吸系統綜合症.md "wikilink")（[沙士](../Page/沙士事件.md "wikilink")）[冠狀病毒和傳播方法](../Page/冠状病毒.md "wikilink")，並且研製出[血清快速測試法](../Page/血清.md "wikilink")。其後更成功發現超過100種化合物，可阻止SARS冠狀病毒入侵人體細胞及在細胞內複製，達到治療效果。\[8\]

2015年1月9日，徐立之受聘為[浙江大學求是高等研究院院長](../Page/浙江大學.md "wikilink")。同時，浙江省政府特聘他擔任參事以促進[浙江省的創新創業](../Page/浙江省.md "wikilink")。\[9\]

2015年12月，徐立之出任[港科院創院院長](../Page/港科院.md "wikilink")。\[10\]

2017年10月，[港府成立檢討研究政策及資助專責小組](../Page/香港政府.md "wikilink")，專責檢討香港的專上教育，徐立之出任主席。\[11\]

## 獲獎紀錄

  - 加拿大國會

  -
  - [加拿大勋章](../Page/加拿大勋章.md "wikilink")

  - [法國榮譽軍團勳章](../Page/法國榮譽軍團勳章.md "wikilink")

  -
  - [大紫荊勳章](../Page/大紫荊勳章.md "wikilink")

  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")

## 榮銜

  - [加拿大皇家學會會員](../Page/加拿大皇家學會.md "wikilink")

  - [皇家學會會員](../Page/皇家學會.md "wikilink")

  - [中央研究院院士](../Page/中央研究院.md "wikilink")

  - [美國國家科學院外籍院士](../Page/美國國家科學院.md "wikilink")

  - [中國科學院外籍院士](../Page/中國科學院.md "wikilink")

  - [倫敦皇家內科醫學院名譽院士](../Page/倫敦皇家內科醫學院.md "wikilink")

  - （2012年3月28日，七名獲獎者中唯一華人，亦是歷來第二名獲獎華人）

  - 世界創新基金會名譽會員

## 港大818事件

## 參考文獻

{{-}}

[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:加拿大皇家學會院士](../Category/加拿大皇家學會院士.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:法國榮譽軍團勳章持有人](../Category/法國榮譽軍團勳章持有人.md "wikilink")
[Category:分子遗传学家](../Category/分子遗传学家.md "wikilink")
[Category:加拿大遺傳學家](../Category/加拿大遺傳學家.md "wikilink")
[Category:香港科學家](../Category/香港科學家.md "wikilink")
[Category:香港大學校長](../Category/香港大學校長.md "wikilink")
[Category:多倫多大學教師](../Category/多倫多大學教師.md "wikilink")
[Category:匹茲堡大學校友](../Category/匹茲堡大學校友.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:歸化加拿大公民](../Category/歸化加拿大公民.md "wikilink")
[Category:香港上海人](../Category/香港上海人.md "wikilink")
[L](../Category/徐姓.md "wikilink")
[Category:庇護十二學校校友](../Category/庇護十二學校校友.md "wikilink")
[Category:何文田官立中學校友](../Category/何文田官立中學校友.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:中央研究院生命科學組院士](../Category/中央研究院生命科學組院士.md "wikilink")
[Category:复旦大学名誉博士](../Category/复旦大学名誉博士.md "wikilink")
[Category:浙江大学学者](../Category/浙江大学学者.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:港科院院士](../Category/港科院院士.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")

1.  [培力控股：董事名單及其角色及職能](http://www.hkexnews.hk/listedco/listconews/SEHK/2017/1115/LTN201711151014_C.pdf)
2.  [培力控股：2016年度報告](http://www.hkexnews.hk/listedco/listconews/SEHK/2017/0424/LTN201704241094_C.pdf)
3.
4.
5.
6.  [香港中文大學〈徐立之；
    我一世好運！〉](http://www.com.cuhk.edu.hk/ubeat_past/021253/tsui.htm)
7.
8.
9.
10.
11.