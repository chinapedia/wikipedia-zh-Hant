**徐致靖**（），字**子静**，號**僅叟**\[1\]，清末高官，[维新派人士](../Page/维新派.md "wikilink")。[江苏](../Page/江苏.md "wikilink")[宜兴人](../Page/宜兴.md "wikilink")，寄籍宛平。

## 生平

徐致靖為道光二十七年（1847年）進士[徐家-{杰}-之子](../Page/徐家杰_\(進士\).md "wikilink")。[光绪二年](../Page/光绪.md "wikilink")（1876年）中进士，选[庶吉士](../Page/庶吉士.md "wikilink")，[散馆授](../Page/散馆.md "wikilink")[编修](../Page/编修.md "wikilink")。光緒二十四年（戊戌年，1898年），官拜礼部右侍郎、翰林侍读学士。戊戌四月二十日（1898年6月8日），上书《请明定国是疏》（[康有为代拟](../Page/康有为.md "wikilink")）请求[光绪帝正式改变旧法](../Page/光绪帝.md "wikilink")，实施新政。上书后第三天（6月11日），光绪帝颁布《定国是诏》。[戊戌变法正式启动](../Page/戊戌变法.md "wikilink")。变法开始后，上《密保人才摺》，向光绪帝保荐康有为、[谭嗣同](../Page/谭嗣同.md "wikilink")、[张元济](../Page/张元济.md "wikilink")、[黄遵宪](../Page/黄遵宪.md "wikilink")、[梁启超等人](../Page/梁启超.md "wikilink")。在变法中多次上书，主张废八股，开书局，裁冗官，上摺保举[袁世凯等](../Page/袁世凯.md "wikilink")。变法失败后，在[上斜街寓所被捕](../Page/上斜街.md "wikilink")。徐家认为他必死无疑，备好棺材。后经[李鸿章](../Page/李鸿章.md "wikilink")（與徐父為同年故交）疏通，[荣禄力保](../Page/荣禄.md "wikilink")，被判绞监候（死缓）。庚子事变，[慈禧太后西逃](../Page/慈禧太后.md "wikilink")。八国联军入京，[刑部大牢无人职守](../Page/刑部大牢.md "wikilink")，请徐致靖回家。慈禧回京后，下诏赦免。后归隐杭州姚园寺巷直至病逝。他被称为“六君子”之外的七君子，參與變法最早最深，逃生後曾勸[康有爲不要參加復辟](../Page/康有爲.md "wikilink")\[2\]。

著有《上虞县志》、《奏议》、《仅叟诗文》若干卷，《论语解》（未完稿）。

## 参考文献

  - 《[清史稿](../Page/清史稿.md "wikilink")》列传二百五十一

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝禮部侍郎](../Category/清朝禮部侍郎.md "wikilink")
[Category:宜兴人](../Category/宜兴人.md "wikilink")
[Z](../Category/徐姓.md "wikilink")
[Category:戊戌变法](../Category/戊戌变法.md "wikilink")

1.

2.