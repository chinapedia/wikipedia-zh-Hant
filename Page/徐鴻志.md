**徐鴻志**（），為第九任及第十任[台灣](../Page/台灣.md "wikilink")[桃園縣縣長](../Page/桃園縣縣長.md "wikilink")，接替[許信良](../Page/許信良.md "wikilink")。徐鴻志也是僅有的兩位（另一名為第一、二屆縣長[徐崇德](../Page/徐崇德.md "wikilink")）能夠做完八年任期的[桃園縣縣長之一](../Page/桃園縣縣長.md "wikilink")。

早年曾擔任文昌國中、八德國中教師，首任桃園市長[鄭文燦為其學生](../Page/鄭文燦.md "wikilink")。\[1\]

縣長任內將[桃園神社完整保留而不改建](../Page/桃園神社.md "wikilink")，成為全台唯一留存至今的神社。

## 參見

  - [桃園市](../Page/桃園市.md "wikilink")

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

{{-}}   |- |colspan="3"
style="text-align:center;"|**[Emblem_of_Taoyuan_City.svg](https://zh.wikipedia.org/wiki/File:Emblem_of_Taoyuan_City.svg "fig:Emblem_of_Taoyuan_City.svg")[桃園縣政府](../Page/桃園市.md "wikilink")**
|-

[Category:臺灣自來水公司董事長](../Category/臺灣自來水公司董事長.md "wikilink")
[Category:臺灣省政府委員](../Category/臺灣省政府委員.md "wikilink")
[Category:桃園縣縣長](../Category/桃園縣縣長.md "wikilink")
[Category:第9屆桃園縣議員](../Category/第9屆桃園縣議員.md "wikilink")
[Category:第8屆桃園縣議員](../Category/第8屆桃園縣議員.md "wikilink")
[Category:第7屆桃園縣議員](../Category/第7屆桃園縣議員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:東吳大學校友](../Category/東吳大學校友.md "wikilink")
[Category:國立桃園高級中學校友](../Category/國立桃園高級中學校友.md "wikilink")
[Category:桃園市人](../Category/桃園市人.md "wikilink")
[Hong鴻](../Category/徐姓.md "wikilink")

1.  [老縣長徐鴻志辭世
    吳敦義捻香慰問家屬](http://www.chinatimes.com/realtimenews/20180129003141-260407)