《**徒然草**》（），[吉田兼好法師著](../Page/吉田兼好.md "wikilink")，日本中世文學隨筆體的代表作之一，跟[清少納言著作的](../Page/清少納言.md "wikilink")《[枕草子](../Page/枕草子.md "wikilink")》和[鴨長明著作的](../Page/鴨長明.md "wikilink")《[方丈記](../Page/方丈記.md "wikilink")》同被譽為日本三大[隨筆之一](../Page/隨筆.md "wikilink")。

一般認為，《徒然草》於1330年至1332年之間寫成。《徒然草》由一篇序段以及另外二百四十三段組成，文体为[和漢混淆文与以](../Page/和漢混淆文.md "wikilink")[假名文字为中心的](../Page/日语假名.md "wikilink")[和文相混合](../Page/和文.md "wikilink")，主題環繞無常、死亡、自然美等等。

中文有[王以铸](../Page/王以铸.md "wikilink")、李均洋的译本。

## 參考文獻

  - [加藤周一](../Page/加藤周一.md "wikilink")『日本文学史序説（上）』 筑摩書房
  - [清水義範](../Page/清水義範.md "wikilink")『身もフタもない日本文学史』
    PHP研究所〈PHP新書〉、2009年7月、ISBN 978-4-569-70983-3
  - 上野友愛 佐々木康之 内田洸（サントリー美術館）編集 『徒然草 美術で楽しむ古典文学』 サントリー美術館、2014年6月

## 外部連接

  - 「[徒然草](http://www2s.biglobe.ne.jp/~Taiju/turez_1.htm)」、H.
    Shinozaki『日本古典文学テキスト』（[山崎麓校訂](../Page/山崎麓.md "wikilink")、校註日本文學大系3『徒然草』国民図書株式会社、1924年の複製）
  - 「[徒然草](http://melisande.cs.kyoto-wu.ac.jp/eguchi/pdd/turedure.html)」、（[西尾実](../Page/西尾実.md "wikilink")・[安良岡康作校訂](../Page/安良岡康作.md "wikilink")『徒然草』岩波文庫を元にしたパブリック・ドメイン・データ、[満開製作所](../Page/満開製作所.md "wikilink")『電脳倶楽部』からの転載）
  - 吾妻利秋訳「[現代語訳
    徒然草](http://www.tsurezuregusa.com/)」（[西尾実](../Page/西尾実.md "wikilink")・[安良岡康作校訂](../Page/安良岡康作.md "wikilink")『徒然草』岩波文庫、および[今泉忠義訳注](../Page/今泉忠義.md "wikilink")『改訂　徒然草』角川ソフィア文庫を底本とした対訳）
  - \[　池辺義象による註付き現代語訳\]　近代デジタルライブラリー　

[Category:佛教文学](../Category/佛教文学.md "wikilink")
[Category:隨筆](../Category/隨筆.md "wikilink")
[Category:14世紀書籍](../Category/14世紀書籍.md "wikilink")
[Category:鎌倉時代文學](../Category/鎌倉時代文學.md "wikilink")