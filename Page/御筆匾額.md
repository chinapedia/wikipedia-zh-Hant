[避暑山庄匾额.JPG](https://zh.wikipedia.org/wiki/File:避暑山庄匾额.JPG "fig:避暑山庄匾额.JPG")御笔[避暑山庄龙匾](../Page/避暑山庄.md "wikilink")\]\]
**御筆匾額**，是指[東亞君主](../Page/東亞.md "wikilink")（[中國皇帝](../Page/中國皇帝.md "wikilink")、[日本天皇](../Page/日本天皇.md "wikilink")、[朝鮮國王](../Page/朝鮮國王.md "wikilink")、[越南國王等](../Page/越南.md "wikilink")）所賜，由君主親筆題字之[匾額](../Page/匾額.md "wikilink")。賜給[寺院的御筆匾額又稱](../Page/寺院.md "wikilink")[敕額](../Page/敕額.md "wikilink")。中國皇帝的御筆匾額除了賜給於國內的[建築](../Page/建築.md "wikilink")、臣民外，還有賜給[朝貢國](../Page/朝貢國.md "wikilink")、[附屬國的](../Page/附屬國.md "wikilink")。

## 形式特徵

### 清朝

清朝皇帝御筆匾額的[落款中](../Page/落款.md "wikilink")，沒有[上款及](../Page/上款.md "wikilink")[下款](../Page/下款.md "wikilink")，所以看不到年代歲次及題字贈匾人名等常見訊息；皇帝御筆匾額僅有[中款題字](../Page/中款題字.md "wikilink")，通常為四個大字。形式簡潔。

要辨視皇帝御筆匾額，全靠匾額正中央上方的[御印](../Page/御印.md "wikilink")。御印刻有六個字，前兩個字是該皇帝的[年號](../Page/年號.md "wikilink")，後四個字為**御筆之寶**，例如，光緒皇帝所賜的匾即蓋有**光緒御筆之寶**之御印。

## 現存的御筆匾額

### 中國皇帝御筆匾額

#### 中國大陸

##### 北京

  - 紫禁城[乾清宮高懸](../Page/乾清宮.md "wikilink")[順治皇帝御筆匾額](../Page/順治皇帝.md "wikilink")「正大光明」，
  - 紫禁城[養心殿高懸](../Page/養心殿.md "wikilink")[雍正皇帝御筆匾額](../Page/雍正皇帝.md "wikilink")「中正仁和」，
  - 紫禁城[永寿宫殿内高悬](../Page/永寿宫.md "wikilink")[乾隆皇帝御笔匾额](../Page/乾隆皇帝.md "wikilink")「令儀淑德」，
  - [光绪皇帝御笔](../Page/光绪皇帝.md "wikilink")[颐和园匾额](../Page/颐和园.md "wikilink")。

##### 河北

  - [康熙皇帝御笔](../Page/康熙皇帝.md "wikilink")[避暑山庄匾额](../Page/避暑山庄.md "wikilink")

##### 山西

[Emperor_Yongle_caligraphy_plague_"Magnificent_and_Magical_Work"_at_the_Fogong_Temple.JPG](https://zh.wikipedia.org/wiki/File:Emperor_Yongle_caligraphy_plague_"Magnificent_and_Magical_Work"_at_the_Fogong_Temple.JPG "fig:Emperor_Yongle_caligraphy_plague_\"Magnificent_and_Magical_Work\"_at_the_Fogong_Temple.JPG")御笔匾额峻极神工（應縣佛宮寺釋迦塔）\]\]
[山西应县木塔明万历匾额.JPG](https://zh.wikipedia.org/wiki/File:山西应县木塔明万历匾额.JPG "fig:山西应县木塔明万历匾额.JPG")皇帝御笔“天下奇观”匾额（應縣佛宮寺釋迦塔）\]\]

山西應縣[佛宮寺釋迦塔](../Page/佛宮寺釋迦塔.md "wikilink")

  - [明成祖御筆匾額](../Page/明成祖.md "wikilink")「峻極神工」
  - [明武宗御筆匾額](../Page/明武宗.md "wikilink")「天下奇觀」

##### 福建

  - [雍正皇帝御賜](../Page/雍正皇帝.md "wikilink")[湄洲媽祖祖廟](../Page/湄洲媽祖祖廟.md "wikilink")「神昭海表」匾額

##### 四川

[康熙御题报国寺额匾.JPG](https://zh.wikipedia.org/wiki/File:康熙御题报国寺额匾.JPG "fig:康熙御题报国寺额匾.JPG")
四川[峨眉山](../Page/峨眉山.md "wikilink")[报国寺](../Page/报国寺.md "wikilink")[康熙御笔](../Page/康熙.md "wikilink")

##### 甘肃

武威文庙：40多幅\[1\]

##### [青海](../Page/青海.md "wikilink")

[塔爾寺](../Page/塔爾寺.md "wikilink")：乾隆皇帝曾賜匾額「梵教法幢」。

##### 西藏

  - [布达拉宫](../Page/布达拉宫.md "wikilink")：[清朝](../Page/清朝.md "wikilink")[乾隆皇帝御笔匾额](../Page/乾隆皇帝.md "wikilink")“涌莲初地”、[清朝](../Page/清朝.md "wikilink")[同治皇帝御笔匾额](../Page/同治皇帝.md "wikilink")“振锡绥疆”、“福田妙果”。
  - [扎什伦布寺](../Page/扎什伦布寺.md "wikilink")：[清朝同治皇帝御笔匾额](../Page/清朝.md "wikilink")“三摩正觉”。
  - [门珠林寺](../Page/门珠林寺.md "wikilink")：清朝[道光皇帝御笔匾额](../Page/道光皇帝.md "wikilink")“大千普佑”

#### 香港

  - [上環文武廟](../Page/上環文武廟.md "wikilink")：因1877年華北水患，[東華三院籌得五十萬兩賑災](../Page/東華三院.md "wikilink")，光緒皇帝賜“神威普佑”匾額。
  - [東華醫院禮堂](../Page/東華醫院禮堂.md "wikilink")：因1885年6月（清朝光緒十年）珠江三角洲洪水泛濫，[東華三院籌得超過十萬元善款賑災](../Page/東華三院.md "wikilink")，遂由兩廣總督張之洞奏議光緒皇帝御賜“萬物咸利”牌匾表揚，惟牌匾乃光緒親點翰林代筆，並非光緒親題。\[2\]

#### 臺灣

  - [台北市](../Page/台北市.md "wikilink")
      - [艋舺祖師廟](../Page/艋舺祖師廟.md "wikilink")：光緒御筆之寶「功資拯濟」，因[中法戰爭建功所賜](../Page/中法戰爭.md "wikilink")。
      - [松山慈祐宮](../Page/松山慈祐宮.md "wikilink")：乾隆御筆之寶「利濟參天」
      - [艋舺龍山寺](../Page/艋舺龍山寺.md "wikilink")：光緒御筆之寶「慈輝遠蔭」，因龍山寺於中法戰爭中協助清兵協助擊退法軍所賜。
      - [北投關渡宮](../Page/北投關渡宮.md "wikilink")：光緒御筆之寶「翌天昭佑」
  - [新北市](../Page/新北市.md "wikilink")
      - [淡水區](../Page/淡水區.md "wikilink")
          - [淡水福佑宮](../Page/淡水福佑宮.md "wikilink")：光緒御筆之寶「翌天昭佑」，為光緒十二年巡撫[劉銘傳因](../Page/劉銘傳.md "wikilink")[中法戰爭之捷奏請賜匾](../Page/中法戰爭.md "wikilink")。
          - [淡水龍山寺](../Page/淡水龍山寺.md "wikilink")：光緒御筆之寶「慈航普渡」，為光緒十二年巡撫[劉銘傳因](../Page/劉銘傳.md "wikilink")[中法戰爭之捷奏請賜匾](../Page/中法戰爭.md "wikilink")。
          - [淡水祖師廟](../Page/淡水祖師廟.md "wikilink")：光緒御筆之寶「功資拯濟」，為艋舺祖師廟同款匾之複製品。
  - [新竹市](../Page/新竹市.md "wikilink")
      - [新竹都城隍廟](../Page/新竹都城隍廟.md "wikilink")：光緒御筆之寶--金匾「金門保障」。
      - [新竹都城隍廟](../Page/新竹都城隍廟.md "wikilink")：溥儀御筆之寶--石匾「正直」「聰明」。。
      - [新竹竹蓮寺](../Page/新竹竹蓮寺.md "wikilink")：光緒親筆御題的金匾「大海慈雲」，
        黑字金底雙龍搶珠，匾側有一對金龍護匾。
      - 新竹[明志書院](../Page/明志書院.md "wikilink")：光緒親筆御題的金匾「澤普瀛儒」。
  - [新竹縣](../Page/新竹縣.md "wikilink")
      - [新埔鎮](../Page/新埔鎮.md "wikilink")
          - [褒忠亭義民廟](../Page/褒忠亭義民廟.md "wikilink")：乾隆御筆之寶「褒忠」
  - [彰化縣](../Page/彰化縣.md "wikilink")
      - [鹿港鎮](../Page/鹿港鎮.md "wikilink")
          - [日茂行](../Page/日茂行.md "wikilink")：嘉慶御筆之寶「大觀」
  - [雲林縣](../Page/雲林縣.md "wikilink")
      - [北港鎮](../Page/北港鎮_\(臺灣\).md "wikilink")
          - [北港朝天宮](../Page/北港朝天宮.md "wikilink")：光緒御筆之寶「慈雲灑潤」
          - [北港義民廟](../Page/北港義民廟.md "wikilink")：乾隆御筆之寶「旌義」
  - [嘉義市](../Page/嘉義市.md "wikilink")
      - [東區](../Page/東區.md "wikilink")
          - [嘉邑城隍廟](../Page/嘉邑城隍廟.md "wikilink") : 光緒御筆之寶「臺洋顯祐」
  - [台南市](../Page/台南市.md "wikilink")
      - [中西區](../Page/中西區_\(台灣\).md "wikilink")
          - [台南孔廟](../Page/台南孔廟.md "wikilink")：歷任清朝皇帝統治臺灣期間，除[清穆宗未以](../Page/清穆宗.md "wikilink")[祺祥年號贈匾外](../Page/祺祥.md "wikilink")，登基時例皆贈匾，目前共八方御製匾額。共有康熙御筆之寶「萬世師表」、雍正御筆之寶「生民未有」、乾隆御筆之寶「與天地參」、嘉慶御筆之寶「聖集大成」、道光御筆之寶「聖協時中」、咸豐御筆之寶「德齊幬載」、同治御筆之寶「聖神天縱」及光緒御筆之寶「斯文在茲」等匾。[民國時期](../Page/臺灣戰後時期.md "wikilink")，歷任中華民國總統例皆贈匾，目前共六方總統贈匾。共有[蔣中正](../Page/蔣中正.md "wikilink")「有教無類」、[嚴家淦](../Page/嚴家淦.md "wikilink")「萬世師表」、[蔣經國](../Page/蔣經國.md "wikilink")「道貫古今」、[李登輝](../Page/李登輝.md "wikilink")「德配天地」、[陳水扁](../Page/陳水扁.md "wikilink")「中和位育」、[馬英九](../Page/馬英九.md "wikilink")「聖德化育」及[蔡英文](../Page/蔡英文.md "wikilink")「德侔道昌」等匾。其中馬英九「聖德化育」贈匾兩次，第一次因為尺寸過小、無金色鑲邊、匾文採陰刻而非陽刻，與歷任總統沿襲之規格不符，而引起地方上微詞，三年後重製新匾，符合規格，再贈匾一次。\[3\]
          - [台南](../Page/台南.md "wikilink")[大天后宮](../Page/大天后宮.md "wikilink")：如康熙御筆之寶「輝煌海澨」嘉慶御筆之寶「海國安瀾」、咸豐御筆之寶「德侔厚載」
          - [台南](../Page/台南.md "wikilink")[祀典武廟](../Page/祀典武廟.md "wikilink")：咸豐御筆之寶「萬世人極」
      - [佳里區](../Page/佳里區.md "wikilink")
          - [佳里金唐殿](../Page/佳里金唐殿.md "wikilink")：乾隆御筆之寶「旌義」
  - [台東縣](../Page/台東縣.md "wikilink")
      - [台東市](../Page/台東市.md "wikilink")
          - [台東天后宮](../Page/台東天后宮.md "wikilink")：光緒御筆之寶「靈昭誠佑」
  - 共同御匾
      - 雍正御筆之寶「神昭海表」：[北港朝天宮](../Page/北港朝天宮.md "wikilink")、[新港奉天宮](../Page/新港奉天宮.md "wikilink")、[台南](../Page/台南.md "wikilink")[大天后宮](../Page/大天后宮.md "wikilink")、[鹿港天后宮](../Page/鹿港天后宮.md "wikilink")
      - 乾隆御筆之寶「佑濟昭靈」：[台北天后宮](../Page/台北天后宮.md "wikilink")、[大甲鎮瀾宮](../Page/大甲鎮瀾宮.md "wikilink")、[鹿港天后宮](../Page/鹿港天后宮.md "wikilink")、[台南](../Page/台南.md "wikilink")[大天后宮](../Page/大天后宮.md "wikilink")、[鹿港](../Page/鹿港.md "wikilink")[新祖宮](../Page/新祖宮.md "wikilink")
      - 光緒御筆之寶「與天同功」：[台北天后宮](../Page/台北天后宮.md "wikilink")、[大甲鎮瀾宮](../Page/大甲鎮瀾宮.md "wikilink")、[鹿港天后宮](../Page/鹿港天后宮.md "wikilink")、[北港朝天宮](../Page/北港朝天宮.md "wikilink")、[台南](../Page/台南.md "wikilink")[大天后宮](../Page/大天后宮.md "wikilink")、[內埔天后宮](../Page/內埔天后宮.md "wikilink")、[中港慈裕宮](../Page/中港慈裕宮.md "wikilink")、[松山慈祐宮](../Page/松山慈祐宮.md "wikilink")、[澎湖天后宮](../Page/澎湖天后宮.md "wikilink")

#### 蒙古

  - [庆宁寺](../Page/慶寧寺_\(蒙古國\).md "wikilink")：清朝[乾隆皇帝御笔匾额](../Page/乾隆皇帝.md "wikilink")“福佑恒沙”。

#### 琉球

  - [那霸](../Page/那霸.md "wikilink")[首里城](../Page/首里城.md "wikilink")：首里城正殿內懸掛有數個清朝皇帝賜給琉球國中山王的御筆匾額。原匾皆在[沖繩島戰役中燒失](../Page/沖繩島戰役.md "wikilink")，現掛的都是複製品。

| 匾額   | 賜予者                              | 被賜予者                             | 賜予時間   |
| ---- | -------------------------------- | -------------------------------- | ------ |
| 中山世土 | [康熙帝](../Page/康熙帝.md "wikilink") | [尚貞王](../Page/尚貞王.md "wikilink") | 康熙二十二年 |
| 輯瑞球陽 | [雍正帝](../Page/雍正帝.md "wikilink") | [尚敬王](../Page/尚敬王.md "wikilink") | 雍正元年   |
| 永祚瀛壖 | [乾隆帝](../Page/乾隆帝.md "wikilink") | 尚敬王                              | 乾隆二年   |
| 海邦濟美 | 乾隆帝                              | [尚穆王](../Page/尚穆王.md "wikilink") | 乾隆四十九年 |
| 海表恭藩 | [嘉慶帝](../Page/嘉慶帝.md "wikilink") | [尚溫王](../Page/尚溫王.md "wikilink") | 嘉慶五年   |
| 屏翰東南 | [道光帝](../Page/道光帝.md "wikilink") | [尚灝王](../Page/尚灝王.md "wikilink") | 道光二年   |
| 弼服海隅 | 道光帝                              | [尚育王](../Page/尚育王.md "wikilink") | 道光十七年  |
| 同文式化 | [咸豐帝](../Page/咸豐帝.md "wikilink") | [尚泰王](../Page/尚泰王.md "wikilink") | 咸豐二年   |
| 瀛嶠屏藩 | [同治帝](../Page/同治帝.md "wikilink") | 尚泰王                              | 同治四年   |
|      |                                  |                                  |        |

### 日本天皇御筆匾額

  - 福岡縣

      - 福岡市博多[筥崎八幡宮](../Page/筥崎八幡宮.md "wikilink")：[龜山天皇為](../Page/龜山天皇.md "wikilink")[上皇時御筆親題](../Page/上皇.md "wikilink")「敵國降伏」的匾額，是[元寇入侵日本時所書](../Page/元寇.md "wikilink")。1594年由[九州武將](../Page/九州.md "wikilink")[小早川隆景摹刻成匾額](../Page/小早川隆景.md "wikilink")。現懸於宮內的是十七世紀經重修後的匾子。

  -
### 朝鮮國王御筆匾額

#### 韓國

  - 全羅南道
      - 順天市曹溪山[仙岩寺](../Page/仙岩寺.md "wikilink")：朝鮮純祖御筆親題的“大福田”匾額，掛於圓通殿內

#### 朝鮮

### 越南国王御筆匾額

### 琉球国王御筆匾額

### 滿洲國皇帝御筆匾額

#### 臺灣

  - [新竹都城隍廟](../Page/新竹都城隍廟.md "wikilink")：溥儀御筆之寶(台灣唯一)--
    「正直聰明」，為昭和10年10月4日
    [滿洲國駐日大使](../Page/滿洲國.md "wikilink")[謝介石](../Page/謝介石.md "wikilink")\[4\]\[5\]氏(新竹市人)勳一等旭日大綬章-之奏請賜匾。

## 圖像集

<File:Inside> the Forbidden
City.jpg|乾清宮[順治御筆](../Page/順治.md "wikilink")「正大光明」匾額
<File:避暑山庄匾额.JPG>|[康熙御笔](../Page/康熙.md "wikilink")[避暑山庄龙匾](../Page/避暑山庄.md "wikilink")
<File:雍正御笔中正仁和匾.JPG>|[养心殿](../Page/养心殿.md "wikilink")[雍正御笔](../Page/雍正.md "wikilink")「中正仁和」匾
<File:SV101392.JPG>|[光绪皇帝御笔](../Page/光绪.md "wikilink")[颐和园匾额](../Page/颐和园.md "wikilink")
<File:Imperial> Tablet which was sent by the Chinese Emperor
Chenghua.JPG|[明憲宗御賜](../Page/明憲宗.md "wikilink")[琉球國的御筆匾額](../Page/琉球國.md "wikilink")
<File:Naha> Shuri Castle25s3200.jpg|康熙御筆賜琉球國中山王匾額「中山世土」
[File:HakozakinomiyaFukutekimon.JPG|日本](File:HakozakinomiyaFukutekimon.JPG%7C日本)[龜山天皇御筆匾額](../Page/龜山天皇.md "wikilink")「敵國降伏」

## 參考文獻

[Category:東亞書法](../Category/東亞書法.md "wikilink")
[Category:東亞古代政府獎勵](../Category/東亞古代政府獎勵.md "wikilink")
[Category:東亞文物](../Category/東亞文物.md "wikilink")

1.
2.  來源參考：[光緒皇帝御賜「萬物咸利」牌匾
    移至香港歷史博物館](http://www.tungwah.org.hk/upload/CH/hongkong/event.php?target=3)
3.  來源參考：[總統贈台南孔廟御匾，陰刻錯掛2年](http://www.tvbs.com.tw/news/news_list.asp?no=chen198720110131124859)
4.  來源參考：[「台灣人在滿洲國」揭80年前秘辛](http://kongtaigi.pts.org.tw/2013/03/80.html)
5.  來源參考：[謝介石與新竹城隍廟](http://tw.myblog.yahoo.com/jw!VC00iUmLHwKalCCy98M4/article?mid=940)