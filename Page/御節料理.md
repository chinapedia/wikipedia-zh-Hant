[Oseti.jpg](https://zh.wikipedia.org/wiki/File:Oseti.jpg "fig:Oseti.jpg")

[Japanese_Osechi.jpg](https://zh.wikipedia.org/wiki/File:Japanese_Osechi.jpg "fig:Japanese_Osechi.jpg")

**御節料理
(日式年菜)**是[日本在](../Page/日本.md "wikilink")[節日時所做的特殊](../Page/節日.md "wikilink")[料理](../Page/日本料理.md "wikilink")，特別是在[過年時的主要](../Page/過年.md "wikilink")[料理](../Page/日本料理.md "wikilink")，所以又稱**正月料理**，通常也只說「」即可。

在日本有**[人日](../Page/人日.md "wikilink")**（）、**[上巳節](../Page/上巳節.md "wikilink")**（）、**[端午節](../Page/端午節.md "wikilink")**（）、**[七夕](../Page/七夕.md "wikilink")**（）、**[重陽節](../Page/重陽節.md "wikilink")**（）等五大節日，在這些日子都有推出御節料理的習慣，意指[五穀豐收](../Page/五穀.md "wikilink")，全家平安健康的祈福。\[1\]

## 概要

**御節**本來是指一些季節（[節氣](../Page/節氣.md "wikilink")）轉換時的主要節日。這些節日會有些慶祝活動與慶祝的料理，[日本人將這些料理當作是個幸運的](../Page/日本人.md "wikilink")[緣起](../Page/緣起.md "wikilink")（、Engi），所以這些為了慶典而做的食膳就叫做「**御節料理**」。

[日本的](../Page/日本.md "wikilink")[過年](../Page/過年.md "wikilink")（[明治維新前為](../Page/明治維新.md "wikilink")[天保曆](../Page/天保曆.md "wikilink")[正月](../Page/正月.md "wikilink")[初一](../Page/初一.md "wikilink")，明治維新後改為[陽曆](../Page/格里曆.md "wikilink")1月1日），是自古以來認為要呼喚已回到山裡的田神（守護稻田使其豐收之神）所要慶祝的重大[節慶](../Page/節日.md "wikilink")。在現代還保有一般的[風俗習慣之固定慶典只剩下過年](../Page/風俗.md "wikilink")。所以，在現代御節料理已大多是指會在[除夕都先做好](../Page/除夕.md "wikilink")，為了[跨年後的](../Page/跨年.md "wikilink")**正月料理**而已。

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/日本料理.md" title="wikilink">日本料理</a></li>
<li><a href="../Page/格里曆.md" title="wikilink">格里曆</a></li>
<li><a href="../Page/過年.md" title="wikilink">過年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/人日.md" title="wikilink">人日</a></li>
<li><a href="../Page/上巳節.md" title="wikilink">上巳節</a></li>
<li><a href="../Page/端午節.md" title="wikilink">端午節</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/七夕.md" title="wikilink">七夕</a></li>
<li><a href="../Page/重陽節.md" title="wikilink">重陽節</a></li>
<li><a href="../Page/除夕.md" title="wikilink">除夕</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [おせち料理大事典](https://web.archive.org/web/20070112100440/http://www.kibun.co.jp/enter/osechi/shogatu/)

[日本節日食品](../Category/日本節日食品.md "wikilink")
[Category:日本賀年食品](../Category/日本賀年食品.md "wikilink")

1.  [おせちのいわれ](http://www.kibun.co.jp/enter/osechi/shogatu/mini/index.html)
    （關於御節的說明）