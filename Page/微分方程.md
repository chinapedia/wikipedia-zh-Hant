[Airflow-Obstructed-Duct.png](https://zh.wikipedia.org/wiki/File:Airflow-Obstructed-Duct.png "fig:Airflow-Obstructed-Duct.png")\]\]
[Elmer-pump-heatequation.png](https://zh.wikipedia.org/wiki/File:Elmer-pump-heatequation.png "fig:Elmer-pump-heatequation.png")得到的泵浦外殼熱分佈圖，假設外界是較低溫度的溫度分佈，熱由泵浦內部傳出，由外界冷卻。\]\]

**微分方程**（，**DE**）是一種[數學](../Page/數學.md "wikilink")[方程](../Page/方程.md "wikilink")，用來描述某一類[函数與其](../Page/函数.md "wikilink")[导数之间的关系](../Page/导数.md "wikilink")。微分方程的解是一個符合方程的[函數](../Page/函數.md "wikilink")。而在初等数学的代数方程裡，其解是常数值。

微分方程的应用十分广泛，可以解决许多与导数有关的问题\[1\]。[物理中许多涉及变力的](../Page/物理.md "wikilink")[运动学](../Page/运动学.md "wikilink")、[动力学问题](../Page/动力学.md "wikilink")，如空气的阻力為速度函數的落体运动等问题，很多可以用微分方程求解。此外，微分方程在[化学](../Page/化学.md "wikilink")、[工程学](../Page/工程学.md "wikilink")、[经济学和人口统计等领域都有应用](../Page/经济学.md "wikilink")。

数学领域对微分方程的研究着重在几个不同的面向，但大多数都是关心微分方程的解。只有少数简单的微分方程可以求得[解析解](../Page/解析解.md "wikilink")。不过即使没有找到其解析解，仍然可以确认其解的部份性质。在无法求得解析解时，可以利用[数值分析的方式](../Page/数值分析.md "wikilink")，利用电脑来找到其数值解。
[动力系统理论强调对于微分方程系统的量化分析](../Page/动力系统.md "wikilink")，而许多数值方法可以计算微分方程的数值解，且有一定的准确度。

## 分類

微分方程可分為以下幾類，而隨著微分方程種類的不同，其相關研究的方式也會隨之不同。

### 常微分方程及偏微分方程

  - [常微分方程](../Page/常微分方程.md "wikilink")（ODE）是指一微分方程的未知數是單一自變數的函數<ref name="episte">

</ref>。最簡單的常微分方程，未知數是一個實數或是複數的函數，但未知數也可能是一個向量函數或是矩陣函數，後者可對應一個由常微分方程組成的系統。微分方程的表达通式是：

\[f\left(x, \frac{d^n y}{dx^n},\frac{d^{(n-1)} y}{dx^{(n-1)}},\cdots, \frac{dy}{dx}, y\right)=0\]

  -
    常微分方程常依其階數分類，階數是指自變數導數的最高階數\[2\]，最常見的二種為一階微分方程及二階微分方程。例如以下的贝塞尔方程：

\[x^2 \frac{d^2 y}{dx^2} + x \frac{dy}{dx} + (x^2 - \alpha^2)y = 0\]

  -
    （其中y為[應變數](../Page/應變數.md "wikilink")）為二階微分方程，其解為[贝塞尔函数](../Page/贝塞尔函数.md "wikilink")。

<!-- end list -->

  - [偏微分方程](../Page/偏微分方程.md "wikilink")（PDE）是指一微分方程的未知數是多個自變數的函數\[3\]，且方程式中有未知數對自變數的[偏微分](../Page/偏微分.md "wikilink")。偏微分方程的階數定義類似常微分方程，但更細分為[橢圓型](../Page/橢圓算子.md "wikilink")、[雙曲線型及](../Page/雙曲線偏微分方程.md "wikilink")[拋物線型的偏微分方程](../Page/抛物偏微分方程.md "wikilink")，尤其在二階偏微分方程中上述的分類更是重要。有些偏微分方程在整個自變數的值域中無法歸類在上述任何一種型式中，這種偏微分方程則稱為混合型。像以下的方程就是偏微分方程：

<!-- end list -->

  -

      -
        \(\frac{\partial u}{\partial t} + t\frac{\partial u}{\partial x} = 0.\)

### 線性及非線性

常微分方程及偏微分方程都可以分為線性及非線性二類。

若微分方程中沒有出現自变數及微分項的平方或其他乘積項，也沒有出現应变數及其微分項的乘積，此微分方程為[線性微分方程](../Page/線性微分方程.md "wikilink")，否則即為**非線性微分方程**。

**齊次線性微分方程**是線性微分方程中更細的分類，微分方程的解乘上一係數或是與另一個解相加後的結果仍為微分方程的解。

若線性微分方程的係數均為常數，則為**常係數線性微分方程**。常係數線性微分方程可以利用[拉氏轉換轉換為代數方程](../Page/拉氏轉換.md "wikilink")\[4\]，因此簡化求解的過程。

針對非線性的微分方程，只有相當少數的方法可以求得微分方程的解析解，而且這些方法需要微分方程有特別的[對稱性](../Page/對稱性.md "wikilink")。長時間時非線性微分方程可能會出現非常複雜的特性，也可能會有[混沌現象](../Page/混沌理论.md "wikilink")。有關非線性微分方程的一些基本問題，例如解的存在性、唯一性及初始值非線性微分方程的[適定性問題](../Page/適定性問題.md "wikilink")，以及邊界值非線性微分方程都是相當難的問題，甚至針對特定非線性微分方程的上述基本問題都被視為是數學理論的一大突破。例如2000年提出的7個[千禧年大獎難題中](../Page/千禧年大獎難題.md "wikilink")，其中一個是[納維-斯托克斯存在性與光滑性](../Page/納維-斯托克斯存在性與光滑性.md "wikilink")，都是探討[納維－斯托克斯方程式其解的數學性質](../Page/納維－斯托克斯方程式.md "wikilink")\[5\]，截至2018年8月此問題仍尚未被證明。

線性微分方程常常用來近似非線性微分方程，不過只在特定的條件下才能近似。例如單擺的運動方程為非線性的微分方程，但在小角度時可以近似為線性的微分方程。

### 舉例

以下是常微分方程的一些例子，其中\(u\)為未知的函數，自變數為\(x\)，\(c\)及\(\omega\)均為常數。

  - 非齊次一階常係數線性微分方程：

<!-- end list -->

  -

      -
        \(\frac{du}{dx} = cu+x^2.\)

<!-- end list -->

  - 齊次二階線性微分方程：

<!-- end list -->

  -

      -
        \(\frac{d^2u}{dx^2} - x\frac{du}{dx} + u = 0.\)

<!-- end list -->

  - 描述[諧振子的齊次二階常係數線性微分方程](../Page/諧振子.md "wikilink")：

<!-- end list -->

  -

      -
        \(\frac{d^2u}{dx^2} + \omega^2u = 0.\)

<!-- end list -->

  - 非齊次一階非線性微分方程：

<!-- end list -->

  -

      -
        \(\frac{du}{dx} = u^2 + 1.\)

<!-- end list -->

  - 描述長度為\(L\)的[單擺的二階非線性微分方程](../Page/單擺.md "wikilink")：

<!-- end list -->

  -

      -
        \(L\frac{d^2u}{dx^2} + g\sin u = 0.\)

以下是偏微分方程的一些例子，其中\(u\)為未知的函數，自變數為\(x\)及\(t\)或者是\(x\)及\(y\)。

  - 齊次一階線性偏微分方程：

<!-- end list -->

  -

      -
        \(\frac{\partial u}{\partial t} + t\frac{\partial u}{\partial x} = 0.\)

<!-- end list -->

  - [拉普拉斯方程](../Page/拉普拉斯方程.md "wikilink")，是橢圓型的齊次二階常係數線性偏微分方程：

<!-- end list -->

  -

      -
        \(\frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} = 0.\)

<!-- end list -->

  - [KdV方程](../Page/KdV方程.md "wikilink")，是三階的非線性偏微分方程：

<!-- end list -->

  -

      -
        \(\frac{\partial u}{\partial t} = 6u\frac{\partial u}{\partial x} - \frac{\partial^3 u}{\partial x^3}.\)

## 性質

### 普遍性的數學描述

許多[物理或是](../Page/物理.md "wikilink")[化學的基本定律都可以寫成微分方程的形式](../Page/化學.md "wikilink")。在[生物學及](../Page/生物學.md "wikilink")[經濟學中](../Page/經濟學.md "wikilink")，微分方程用來作為複雜系統的[數學模型](../Page/數學模型.md "wikilink")。微分方程的數學理論最早是和方程對應的科學領域一起出現，而微分方程的解就可以用在該領域中。不過有時二個截然不同的科學領域會形成相同的微分方程，此時微分方程對應的數學理論可以看到不同現象後面一致的原則。

例如考慮光和聲音在空氣中的傳播，以及池塘水面上的波動，這些都可以用同一個二階的偏微分方程來描述，此方程即為[波動方程](../Page/波動方程.md "wikilink")，因此可以將光和聲音視為一種波，和水面上的水波有些類似之處。[約瑟夫·傅立葉所發展的熱傳導理論](../Page/約瑟夫·傅立葉.md "wikilink")，其統御方程是另一個二階偏微分方程－[熱傳導方程式](../Page/熱傳導方程式.md "wikilink")，[扩散作用看似和熱傳導不同](../Page/扩散作用.md "wikilink")，但也適用同一個統御方程，而經濟學中的布萊克-休斯方程也和熱傳導方程有關。

### 微分方程的解

微分方程的解通常是一个[函数表达式](../Page/函数.md "wikilink")\(y=f(x)\,\)（含一个或多个待定常数，由初始条件确定）。例如：

\[\frac{dy}{dx}=\sin x\]，

的解是

\[y=-\cos x+C\]，

其中\(C\)是待定常数；

例如，如果知道

\[y=f(\pi)=2\]，

则可推出

\[C=1\]，

而可知 \(y=-\cos x+1\)，

### 簡易微分方程的求解方法

#### 一階線性常微分方程

對於一階線性常微分方程，常用的方法是常數變易法：

對於方程：\(y'+p(x)y+q(x)=0\)

可知其通解：\(y=C(x)e^{-\int p(x)\, dx}\)

然後將這個通解代回到原式中，即可求出\(C(x)\)的值

#### 二階常係數齊次常微分方程

對於二階常係數齊次常微分方程，常用方法是求出其特征方程的解

對於方程：\(y''+py'+qy=0\)

其特征方程：\(r^2+pr+q=0\)

根據其特征方程，判斷根的分佈情況，然後得到方程的通解： \(y=c_1 y_1+c_2 y_2\)

一般的通解形式為

（在\(r_1=r_2\)的情況下）\[y=(C_1+C_2 x) e^{r x}\]

（在\(r_1\neq r_2\)的情況下）\[y=C_1 e^{r_1 x}+C_2 e^{r_2 x}\]

（在共軛複數根的情況下）：\(y=e^{\alpha x} (C_1 \cos (\beta x) + C_2 \sin (\beta x))\)

### 約束條件

微分方程的約束條件是指其解需符合的條件，依常微分方程及偏微分方程的不同，有不同的約束條件。

[常微分方程常見的約束條件是函數在特定點的值](../Page/常微分方程.md "wikilink")，若是高階的微分方程，會加上其各階導數的值，有這類約束條件的常微分方程稱為[初值問題](../Page/初值問題.md "wikilink")。

若是二階的常微分方程，也可能會指定函數在二個特定點的值，此時的問題即為[邊界值問題](../Page/邊界值問題.md "wikilink")。若[邊界條件指定二點數值](../Page/邊界條件.md "wikilink")，稱為[狄利克雷邊界條件](../Page/狄利克雷邊界條件.md "wikilink")（第一類邊值條件），此外也有指定二個特定點上導數的邊界條件，稱為[諾伊曼邊界條件](../Page/諾伊曼邊界條件.md "wikilink")（第二類邊值條件）等。

[偏微分方程常見的問題以](../Page/偏微分方程.md "wikilink")[邊界值問題為主](../Page/邊界值問題.md "wikilink")，不過邊界條件則是指定一特定[超曲面的值或導數需符定特定條件](../Page/超曲面.md "wikilink")。

### 解的存在性及唯一性

存在性是指給定一微分方程及約束條件，判斷其解是否存在。唯一性是指在上述條件下，是否只存在一個解。

針對常微分方程的[初值問題](../Page/初值問題.md "wikilink")，[皮亚诺存在性定理可判別解的存在性](../Page/皮亚诺存在性定理.md "wikilink")，[柯西-利普希茨定理則可以判別解的存在性及唯一性](../Page/柯西-利普希茨定理.md "wikilink")。

針對偏微分方程，可以判別解的存在性及唯一性。
[皮亚诺存在性定理可以判斷常微分方程](../Page/皮亚诺存在性定理.md "wikilink")[初值問題的解是否存在](../Page/初值問題.md "wikilink")。

## 歷史

微分方程的起源約在十七世紀末，为了解决物理及天文学问题而產生，大約和微積分的發展同時。[惠更斯在](../Page/惠更斯.md "wikilink")1693年的《教师学报》中提到常微分方程，[雅各布·白努利在](../Page/雅各布·白努利.md "wikilink")1691年建立[悬链线的微分方程](../Page/悬链线.md "wikilink")，並求得其函數。微分方程在十八世紀中期成為一個獨立的學科\[6\]，而微分方程也帶動許多當時的科學發展，例如海王星的發現就和微分方程的分析有關\[7\]。

偏微分方程是由[傅立葉開始的](../Page/傅立葉.md "wikilink")，他在1822年發表《熱的解析理論》，提出[熱傳導方程的偏微分方程](../Page/熱傳導方程.md "wikilink")，並且利用[分離變數法求得級數解](../Page/分離變數法.md "wikilink")，並且開始有關[傅立葉級數的研究](../Page/傅立葉級數.md "wikilink")。另外在十九世紀有關
[拉普拉斯方程的研究也是偏微分方程的重要发展](../Page/拉普拉斯方程.md "wikilink")。[拉普拉斯和](../Page/拉普拉斯.md "wikilink")[泊松都有許多的貢獻](../Page/西莫恩·德尼·泊松.md "wikilink")，後來[喬治·格林提出了相關](../Page/喬治·格林.md "wikilink")[格林函數及](../Page/格林函數.md "wikilink")[格林公式等概念](../Page/格林公式.md "wikilink")，並帶動[斯托克斯](../Page/斯托克斯.md "wikilink")、[麦克斯韦及後來](../Page/麦克斯韦.md "wikilink")[電磁學相關的研究](../Page/電磁學.md "wikilink")。而[流體力學的](../Page/流體力學.md "wikilink")[纳维-斯托克斯方程及彈性介質的](../Page/纳维-斯托克斯方程.md "wikilink")[柯西方程也是在十九世紀提出的偏微分方程](../Page/柯西方程.md "wikilink")。\[8\]。後來許多的理論都是以偏微分方程的形式出現，[量子力學的基礎方程式](../Page/量子力學.md "wikilink")[薛丁格方程也是偏微分方程](../Page/薛丁格方程.md "wikilink")，[廣義相對論中的](../Page/廣義相對論.md "wikilink")[愛因斯坦重力場方程式也有類似偏微分的](../Page/愛因斯坦重力場方程式.md "wikilink")[協變導數](../Page/協變導數.md "wikilink")。

## 相關概念

  - [時滯微分方程](../Page/時滯微分方程.md "wikilink")（DDE）是一個單一自變數的方程，此變數一般稱為時間，未知數在某一時間的導數和特定函數在之前時間的值有關。

<!-- end list -->

  - [隨機微分方程](../Page/隨機微分方程.md "wikilink")（SDE）是一個未知數為[隨機過程](../Page/隨機過程.md "wikilink")，且方程中有包括已知隨機過程（例如[维纳过程](../Page/维纳过程.md "wikilink")）的方程，不過雖名為微分方程，其中沒有微分項。

<!-- end list -->

  - （DAE）是包括自變數微分項的方程，但是為自變數微分項的[隱函數](../Page/隱函數.md "wikilink")。

## 和差分方程的關係

微分方程的理論和[差分方程的理論有密切的關係](../Page/差分方程.md "wikilink")，後者的座標只允許離散值，許多計算微分方程數值解的方法或是對於微分方程性質的研究都需要將微分方程的解近似為對應差分方程的解。

## 著名的微分方程

<div style="-moz-column-count:2; column-count:2;">

### 物理及工程

  - [動力學中的](../Page/動力學.md "wikilink")[牛頓第二運動定律](../Page/牛頓第二運動定律.md "wikilink")

  - 經典力學中的[歐拉－拉格朗日方程](../Page/歐拉－拉格朗日方程.md "wikilink")

  - 經典力學中的[哈密頓力學](../Page/哈密頓力學.md "wikilink")

  - [熱力學中的](../Page/熱力學.md "wikilink")[牛頓冷卻定律](../Page/冷卻定律.md "wikilink")

  - [波动方程](../Page/波动方程.md "wikilink")

  - [電磁學中的](../Page/電磁學.md "wikilink")[麦克斯韦方程组](../Page/麦克斯韦方程组.md "wikilink")

  - 熱力學中的[熱傳導方程式](../Page/熱傳導方程式.md "wikilink")

  - 定義[调和函数的](../Page/调和函数.md "wikilink")[拉普拉斯方程](../Page/拉普拉斯方程.md "wikilink")

  - [泊松方程](../Page/泊松方程.md "wikilink")

  - [廣義相對論中的](../Page/廣義相對論.md "wikilink")[爱因斯坦场方程](../Page/爱因斯坦场方程.md "wikilink")

  - [量子力學中的](../Page/量子力學.md "wikilink")[薛丁格方程式](../Page/薛丁格方程式.md "wikilink")

  - [測地線](../Page/測地線.md "wikilink")

  - [流體力學中的](../Page/流體力學.md "wikilink")[納維－斯托克斯方程式](../Page/納維－斯托克斯方程式.md "wikilink")

  - [隨機過程中的](../Page/隨機過程.md "wikilink")[擴散方程](../Page/擴散方程.md "wikilink")

  - 流體力學中的[對流－擴散方程](../Page/對流－擴散方程.md "wikilink")

  - [複變分析中的](../Page/複變分析.md "wikilink")[柯西－黎曼方程](../Page/柯西－黎曼方程.md "wikilink")

  - [分子動力學中的](../Page/分子動力學.md "wikilink")[泊松－玻爾茲曼方程](../Page/泊松－玻爾茲曼方程.md "wikilink")

  -
  - [通用微分方程](../Page/通用微分方程.md "wikilink")

  - [勞侖次吸子](../Page/勞侖次吸子.md "wikilink")，其解包括了渾沌現象

</div>

### 生物學

  - [威尔霍斯特方程](../Page/威尔霍斯特方程.md "wikilink")–生物族群增長模型

  - [個體成長模型](../Page/個體成長模型.md "wikilink")–生物個體增長模型

  - [洛特卡－沃爾泰拉方程](../Page/洛特卡－沃爾泰拉方程.md "wikilink")–掠食者和獵物的動態模型

  - –應用在[生物數學中](../Page/生物數學.md "wikilink")

  - –神經的[动作电位](../Page/动作电位.md "wikilink")

### 經濟學

  - [布萊克-休斯方程](../Page/布萊克-休斯模型.md "wikilink")

  - [索洛模型](../Page/索洛模型.md "wikilink")

  - [马尔萨斯模型](../Page/马尔萨斯模型.md "wikilink")

  -
## 参见

  - [线性微分方程](../Page/线性微分方程.md "wikilink")
  - [拉普拉斯变换](../Page/拉普拉斯变换.md "wikilink")
  - [常微分方程](../Page/常微分方程.md "wikilink")
  - [偏微分方程](../Page/偏微分方程.md "wikilink")
  - [初值問題](../Page/初值問題.md "wikilink")
  - [边值问题](../Page/边值问题.md "wikilink")

## 參考資料

### 參考文獻

  - D. Zwillinger, *Handbook of Differential Equations (3rd edition)*,
    Academic Press, Boston, 1997.
  - A. D. Polyanin and V. F. Zaitsev, *Handbook of Exact Solutions for
    Ordinary Differential Equations (2nd edition)*, Chapman & Hall/CRC
    Press, Boca Raton, 2003. ISBN 978-1-58488-297-8.
  - W. Johnson, [*A Treatise on Ordinary and Partial Differential
    Equations*](http://www.hti.umich.edu/cgi/b/bib/bibperm?q1=abv5010.0001.001),
    John Wiley and Sons, 1913, in [University of Michigan Historical
    Math Collection](http://hti.umich.edu/u/umhistmath/)
  - E. L. Ince, *Ordinary Differential Equations*, Dover Publications,
    1956
  - E. A. Coddington and N. Levinson, *Theory of Ordinary Differential
    Equations*, McGraw-Hill, 1955
  - P. Blanchard, R. L. Devaney, G. R. Hall, *Differential Equations*,
    Thompson, 2006
  - P. Abbott and H. Neill, *Teach Yourself Calculus*, 2003 pages
    266-277
  - R. I. Porter, *Further Elementary Analysis*, 1978, chapter XIX
    Differential Equations

[\*](../Category/微分方程.md "wikilink")

1.

2.
3.
4.
5.  [Official statement of the
    problem](http://www.claymath.org/millennium/Navier-Stokes_Equations/navierstokes.pdf)
    , Clay Mathematics Institute.

6.

7.

8.