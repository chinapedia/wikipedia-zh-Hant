**微山县**是位于[山东省南部的一个](../Page/山东省.md "wikilink")[县](../Page/县.md "wikilink")，人口68万。频临北方最大的淡水湖[微山湖](../Page/微山湖.md "wikilink")，总面积为1780平方千米，其中微山湖面积1266平方千米，是中国著名的六大淡水湖泊之一，她以秀丽的湖光山色，富饶的自然资源被誉为“鲁南明珠”“齐鲁灵秀”。

## 简介

微山县位于山东省南部，置县于[秦代](../Page/秦代.md "wikilink")，县治设于夏镇，始称[广戚县](../Page/广戚县.md "wikilink")；至[南北朝时并入](../Page/南北朝.md "wikilink")[留县](../Page/留县.md "wikilink")，此后不复县治。建国后，1953年设立微山县，因地处[微山湖故名](../Page/微山湖.md "wikilink")。辖
15 处乡镇（街道），68万人，总面积 1780 平方千米，其中微山湖面积 1266 平方千米，是中国北方最大的淡水湖。

微山县历史悠久，文化底蕴厚重，[伏羲文化](../Page/伏羲.md "wikilink")、秦汉文化、运河文化、[梁祝文化](../Page/梁祝.md "wikilink")、微山湖文化源远流长。微山岛上存有[商代仁人](../Page/商代.md "wikilink")[微子的墓园](../Page/微子.md "wikilink")，微山岛、微山湖、微山县因此而得名。抗日战争时期，以微山湖为根据地的
“微湖大队 ”、“运河支队 ”、“[铁道游击队](../Page/铁道游击队.md "wikilink")”等革命武装
，创造了许多可歌可泣的英雄业绩，一曲《弹起我心爱的土琵琶》唱响大江南北。

## 行政区划

微山县辖2个街道：夏镇，昭阳； 8个镇：韩庄镇 傅村镇 欢城镇 留庄镇 两城镇 鲁桥镇 马坡镇 南阳镇； 5个乡：微山岛乡 张楼乡 西平乡
赵庙乡 高楼乡。 共4个居委会、523个村委会，553个自然村。

## 微山湖

境内微山湖是中国北方最大的淡水湖，由南四湖（微山湖、昭阳湖、独山湖和南阳湖）组成，统称微山湖，是中国著名的六大淡水湖泊之一。她以秀丽的湖光山色,
富饶的自然资源被誉为“鲁南明珠”“[齐鲁灵秀](../Page/齐鲁.md "wikilink")”。

[南四湖位于鲁西南](../Page/南四湖.md "wikilink")，其西南部为苏鲁边界。南北全长120公里，宽4.5公里至24.5公里，周长306公里、总面积为1266平方公里。可控蓄水量为17.3亿立方米。最大库容量47.31亿立方米。平均水深1.5米，汛期最深为3米。流域面积31700平方公里。京杭大运河纵贯全湖南北。

微山湖区物产富饶，素有“日出斗金”之称。湖区水资源门类比较齐全。鱼类现有78种，以[鲤鱼为主](../Page/鲤鱼.md "wikilink")（其中微山湖的鲤鱼四个鼻孔），经济鱼类有[鲫鱼](../Page/鲫鱼.md "wikilink")、黄（鱼桑）鱼、[乌鳢](../Page/乌鳢.md "wikilink")、红鳍（鱼白）、长春鳊和鲤鱼6种，底栖动物包括软体动物、节肢动物、环节动物、昆虫等63种（科），其资源总量为98876吨，浮游植物共116种，其中优势种14种，浮游动物248种，优势种共32种，水生维管束植物74种，全湖现存为304万吨，渔业生物饵料相当丰富。

## 资源及特产

微山矿产资源丰厚。[煤炭已探明储量](../Page/煤炭.md "wikilink")127亿吨,是中国重点煤炭基地之一，且埋藏浅、煤层厚，大多为优质气煤、肥煤。[稀土位于郗山](../Page/稀土.md "wikilink")，已探明储量1275万吨，具有含磷、铁等杂质少、品位高、冶炼工艺简单等优点，是目前国内发现的唯一典型氟碳铈澜矿资源。此外，还有大量的石灰岩、煤矸石、黄沙等资源。

微山水产资源得天独厚。微山湖水质肥沃，物产丰富，属富营养型湖泊，资源量居全国同类大型湖泊之首，素有“日出斗金”之盛誉。目前，有鱼虾、鳖、蟹等共78种；[芦苇](../Page/芦苇.md "wikilink")、菰江草、[莲藕](../Page/莲藕.md "wikilink")、[菱米](../Page/菱角.md "wikilink")、[芡实等水生经济植物](../Page/芡实.md "wikilink")74种；野鸭、水鸡、天鹅等乌类87种，是山东省鸟类自然保护区;浮游动植物364种，尤以“四鼻孔鲤鱼”、“中华鳖”、“中华绒螫蟹”、“中华圆田螺”、“中华新米虾”、“中华小长臂虾”、“微山麻鸭”最负盛名，名贯中外。其中，“四鼻孔鲤鱼”“中华鳖”在清朝被[乾隆皇帝定为晋京贡品](../Page/乾隆.md "wikilink")。

## 文化

梁祝文化、运河文化、微湖文化等，古迹有微山岛三贤墓、梁祝墓、郗山郗鉴墓、伏羲庙、仲子庙、古木兰寺、泰山庙等。

## 外部链接

  - [济宁市微山县政府网站](http://www.weishan.gov.cn/)

[微山县](../Category/微山县.md "wikilink")
[县](../Category/济宁区县市.md "wikilink")
[济宁](../Category/山东省县份.md "wikilink")