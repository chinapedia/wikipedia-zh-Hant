**微形草科**（学名：）是[卫矛目的一个科](../Page/卫矛目.md "wikilink")，只有一[属](../Page/属.md "wikilink")（[微形草属](../Page/微形草属.md "wikilink")）一[种](../Page/种.md "wikilink")（[微形草](../Page/微形草.md "wikilink")，*Lepuropetalon
spathulatum*），主要分布在[美国南部](../Page/美国.md "wikilink")，[墨西哥和](../Page/墨西哥.md "wikilink")[智利部分地区](../Page/智利.md "wikilink")。

微形草是小型[草本](../Page/草本.md "wikilink")[植物](../Page/植物.md "wikilink")，单[叶对生](../Page/叶.md "wikilink")，肉质，一年生，有[褐色条纹](../Page/褐色.md "wikilink")，无托叶；[花单独生长在花茎顶端](../Page/花.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")5。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[梅花草属中](../Page/梅花草属.md "wikilink")，放在[虎耳草科内](../Page/虎耳草科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，没有放入任何[目中](../Page/目.md "wikilink")，直接在[I类真蔷薇分支之下](../Page/I类真蔷薇分支.md "wikilink")，但可以选择性地和[梅花草科合并](../Page/梅花草科.md "wikilink")，2003年经过修订的[APG
II
分类法将其列在](../Page/APG_II_分类法.md "wikilink")[卫矛目下](../Page/卫矛目.md "wikilink")，仍然认为可以选择性地和[梅花草科合并](../Page/梅花草科.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[微形草科](http://delta-intkey.com/angio/www/lepurope.htm)
  - [APG II
    网站中的微形草科](http://www.mobot.org/MOBOT/Research/APWeb/orders/celastralesweb.html#Parnassiaceae)
  - [NCBI中的微形草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=233877)

[Category:微形草科](../Category/微形草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")