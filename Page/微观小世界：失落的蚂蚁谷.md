《微观小世界：失落的蚂蚁谷》（法文：***Minuscule : La Vallée des fourmis
perdues***）是法国电视动画《微观小世界》的电影版。本片获得[第40届凯萨奖最佳长篇动画奖](../Page/第40届凯萨奖.md "wikilink")。

2019年1月30日，續作《[昆蟲總動員2：來自遠方的後援](../Page/昆蟲總動員2：來自遠方的後援.md "wikilink")》在法國先行上映\[1\]。

## 译名

该系列的电视版在中国译为《微观小世界》，与法语相近。在中国，皮克斯作品往往被冠以“总动员”的译名。引进方为了商业化的考量而给该片冠以“总动员”之名。

## 参考资料

## 外部链接

  -
  -
  -
[Category:2013年电影](../Category/2013年电影.md "wikilink")
[Category:法国电影作品](../Category/法国电影作品.md "wikilink")
[Category:比利时电影作品](../Category/比利时电影作品.md "wikilink")
[Category:2010年代动画电影](../Category/2010年代动画电影.md "wikilink")

1.