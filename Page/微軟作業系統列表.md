以下列出微軟所有的作業系統。

## 早於視窗

  - [Xenix](../Page/Xenix.md "wikilink")（發行日期：20世紀70年代）

<!-- end list -->

  - [MS-DOS](../Page/MS-DOS.md "wikilink")（****）
      - MS-DOS 1.x
          - [MS-DOS
            1.14](../Page/MS-DOS_1.14.md "wikilink")（發行日期：1981年7月）
          - [MS-DOS
            1.25](../Page/MS-DOS_1.25.md "wikilink")（發行日期：1982年5月）
      - MS-DOS 2.x
          - [MS-DOS 2.0](../Page/MS-DOS_2.0.md "wikilink")（發行日期：1983年3月）
          - [MS-DOS
            2.11](../Page/MS-DOS_2.11.md "wikilink")（發行日期：1984年3月）
          - [MS-DOS
            2.25](../Page/MS-DOS_2.25.md "wikilink")（發行日期：1985年10月）
      - MS-DOS 3.x
          - [MS-DOS 3.0](../Page/MS-DOS_3.0.md "wikilink")（發行日期：1984年8月）
          - [MS-DOS
            3.1](../Page/MS-DOS_3.1.md "wikilink")（發行日期：1984年11月）
          - [MS-DOS 3.2](../Page/MS-DOS_3.2.md "wikilink")（發行日期：1986年1月）
          - [MS-DOS 3.3](../Page/MS-DOS_3.3.md "wikilink")（發行日期：1987年8月）
      - MS-DOS 4.x
          - [MS-DOS 4.0](../Page/MS-DOS_4.0.md "wikilink")（發行日期：1988年6月）
          - [MS-DOS
            4.01](../Page/MS-DOS_4.01.md "wikilink")（發行日期：1988年12月）
      - MS-DOS 5.x
          - [MS-DOS 5.0](../Page/MS-DOS_5.0.md "wikilink")（發行日期：1991年6月）
      - MS-DOS 6.x
          - [MS-DOS 6.0](../Page/MS-DOS_6.0.md "wikilink")（發行日期：1993年3月）
          - [MS-DOS
            6.2](../Page/MS-DOS_6.2.md "wikilink")（發行日期：1993年11月）
          - [MS-DOS
            6.21](../Page/MS-DOS_6.21.md "wikilink")（發行日期：1994年2月）
          - [MS-DOS
            6.22](../Page/MS-DOS_6.22.md "wikilink")（發行日期：1994年6月）
      - MS-DOS 7.x
          - [MS-DOS 7.0](../Page/MS-DOS_7.0.md "wikilink")（發行日期：1995年8月）
          - [MS-DOS 7.1](../Page/MS-DOS_7.1.md "wikilink")（發行日期：1996年8月）
      - MS-DOS 8.x
          - [MS-DOS 8.0](../Page/MS-DOS_8.0.md "wikilink")（發行日期：2000年9月）
      - [PC
        DOS](../Page/PC_DOS.md "wikilink")（專為支援[IBM-PC而設計的MS](../Page/IBM-PC.md "wikilink")-DOS系統）
          - [PC DOS 1.0](../Page/PC_DOS_1.0.md "wikilink")（發行日期：1981年8月）
          - [PC DOS 1.1](../Page/PC_DOS_1.1.md "wikilink")（發行日期：1982年5月）
          - [PC DOS
            2.1](../Page/PC_DOS_2.1.md "wikilink")（發行日期：1983年10月）
          - [PC DOS 3.3](../Page/PC_DOS_3.3.md "wikilink")（發行日期：1987年4月）
          - [PC DOS 4.0](../Page/PC_DOS_4.0.md "wikilink")（發行日期：1988年7月）
          - [PC DOS 6.3](../Page/PC_DOS_6.3.md "wikilink")（發行日期：1994年4月）
          - [PC DOS 7.0](../Page/PC_DOS_7.0.md "wikilink")（發行日期：1995年4月）
          - [PC DOS 2000](../Page/PC_DOS_2000.md "wikilink")（發行日期：2000年）

<!-- end list -->

  - [OS/2](../Page/OS/2.md "wikilink")（与[IBM共同開發](../Page/IBM.md "wikilink")）
      - [OS/2 1.0](../Page/OS/2_1.0.md "wikilink")
      - [OS/2 1.1](../Page/OS/2_1.1.md "wikilink")
      - [OS/2 1.2](../Page/OS/2_1.2.md "wikilink")
      - [OS/2 1.3](../Page/OS/2_1.3.md "wikilink")
      - [OS/2 2.0](../Page/OS/2_2.0.md "wikilink")

## 視窗系列（[Windows](../Page/Windows.md "wikilink")）

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    1.0](../Page/Windows_1.0.md "wikilink")（代號：“”）
      - [Windows
        1.01](../Page/Windows_1.01.md "wikilink")（發行日期：1985年11月12日）
      - [Windows 1.02](../Page/Windows_1.02.md "wikilink")（發行日期：1986年5月）
      - [Windows 1.03](../Page/Windows_1.03.md "wikilink")（發行日期：1986年8月）
      - [Windows 1.04](../Page/Windows_1.04.md "wikilink")（發行日期：1987年4月）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") Windows 2.x（俗稱Windows
    2.0系列）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
        2.0](../Page/Windows_2.0.md "wikilink")（發行日期：1987年11月23日）
      - [Microsoft](../Page/Microsoft.md "wikilink") Windows 2.1x
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
            2.1](../Page/Windows_2.1.md "wikilink")（發行日期：1988年5月27日）
              - [Windows/286 2.1](../Page/Windows/286_2.1.md "wikilink")
              - [Windows/386
                2.1](../Page/Windows/386_2.1.md "wikilink")（即Windows/286的進階版）
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
            2.11](../Page/Windows_2.11.md "wikilink")（發行日期：1989年3月）
              - [Windows/286
                2.11](../Page/Windows/286_2.11.md "wikilink")
              - [Windows/386
                2.11](../Page/Windows/386_2.11.md "wikilink")（即Windows/286的進階版）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    3.x](../Page/Windows_3.x.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
        3.0](../Page/Windows_3.0.md "wikilink")（發行日期：1990年5月22日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
        3.1x](../Page/Windows_3.1x.md "wikilink")
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
            3.1](../Page/Windows_3.1.md "wikilink")（代號：“”，發行日期：1992年3月18日）
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
            3.11](../Page/Windows_3.11.md "wikilink")（即Windows
            3.1的升級版，發行日期：1993年）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
        3.2](../Page/Windows_3.2.md "wikilink")（即Windows的簡體中文版，發行日期：1994年）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows for
        Workgroups](../Page/Windows_for_Workgroups.md "wikilink")
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows for
            Workgroups
            3.1](../Page/Windows_for_Workgroups_3.1.md "wikilink")（代號：“”，發行日期：1992年10月）
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows for
            Workgroups
            3.11](../Page/Windows_for_Workgroups_3.11.md "wikilink")（代號：“”，發行日期：1993年12月）

### 視窗9x系列（[Windows 9x](../Page/Windows_9x.md "wikilink")）

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    95](../Page/Windows_95.md "wikilink")（內部版本碼：“4.00.950”，代號：“”，發行日期：1995年8月24日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 95
        SP1](../Page/Windows_95_SP1.md "wikilink")（包含第一補丁“”，內部版本碼：“4.00.950a”，發行日期：1995年12月31日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 95 OEM
        Service Release
        1](../Page/Windows_95_OEM_Service_Release_1.md "wikilink")（服務授權第一版，內部版本碼：“4.00.950A”，發行日期：1996年）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 95 OEM
        Service Release
        2](../Page/Windows_95_OEM_Service_Release_2.md "wikilink")（服務授權第二版，內部版本碼：“4.00.950B（4.00.1111）”，發行日期：1996年）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 95 OEM
        Service Release
        2.1](../Page/Windows_95_OEM_Service_Release_2.1.md "wikilink")（服務授權2.1版，內部版本碼：“4.00.950B（4.03.1212）”，發行日期：1996年）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 95 OEM
        Service Release
        2.5](../Page/Windows_95_OEM_Service_Release_2.5.md "wikilink")（服務授權2.5版，內部版本碼：“4.00.950C（4.03.1214）”，發行日期：1997年）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    98](../Page/Windows_98.md "wikilink")（內部版本碼：“4.10.1998”，已升級之版本碼：“4.10.1998A”，代號：“”，發行日期：1998年6月25日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 98
        SE](../Page/Windows_98_SE.md "wikilink")（即“Win98”的第二版，內部版本碼：“4.10.2222A”，已升級之版本碼：“4.10.2222B”，發行日期：1999年5月5日）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    ME](../Page/Windows_ME.md "wikilink")（即“Windows”的千禧年特別版，內部版本碼：“4.90.3000”，代號：“”，發行日期：2000年9月14日）

### 視窗NT系列（[Windows NT](../Page/Windows_NT.md "wikilink")）

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows NT
    3.1](../Page/Windows_NT_3.1.md "wikilink")（僅命名爲“”，發行日期：1993年7月27日）
      - [Windows NT 3.1
        Workstation](../Page/Windows_NT_3.1_Workstation.md "wikilink")（工作站版）
      - [Windows NT 3.1 Advanced
        Server](../Page/Windows_NT_3.1_Advanced_Server.md "wikilink")（進階伺服器版）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows NT
    3.5](../Page/Windows_NT_3.5.md "wikilink")（代號：“”，發行日期：1994年9月21日）
      - [Windows NT 3.5
        Workstation](../Page/Windows_NT_3.5_Workstation.md "wikilink")（工作站版）
      - [Windows NT 3.5
        Server](../Page/Windows_NT_3.5_Server.md "wikilink")（伺服器版）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows NT
    3.51](../Page/Windows_NT_3.51.md "wikilink")（發行日期：1995年5月30日）
      - [Windows NT 3.51
        Workstation](../Page/Windows_NT_3.51_Workstation.md "wikilink")（工作站版）
      - [Windows NT 3.51
        Server](../Page/Windows_NT_3.51_Server.md "wikilink")（伺服器版）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows NT
    4.0](../Page/Windows_NT_4.0.md "wikilink")（代號：“”，發行日期：1996年7月29日）
      - [Windows NT 4.0
        Workstation](../Page/Windows_NT_4.0_Workstation.md "wikilink")（工作站版）
      - [Windows NT 4.0
        Server](../Page/Windows_NT_4.0_Server.md "wikilink")（伺服器版）
      - [Windows NT 4.0 Server Enterprise
        Edition](../Page/Windows_NT_4.0_Server_Enterprise_Edition.md "wikilink")（伺服器商用版）
      - [Windows NT 4.0 Terminal
        Server](../Page/Windows_NT_4.0_Terminal_Server.md "wikilink")（終端伺服器版）
      - [Windows NT 4.0
        Embedded](../Page/Windows_NT_4.0_Embedded.md "wikilink")（嵌入版）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    Neptune](../Page/Windows_Neptune.md "wikilink")（僅開發，無上市）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    2000](../Page/Windows_2000.md "wikilink")（即“Windows
    NT”的“5.0”版本，發行日期：2000年2月17日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 2000
        Professional](../Page/Windows_2000_Professional.md "wikilink")（專業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 2000
        Server](../Page/Windows_2000_Server.md "wikilink")（伺服器版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 2000
        Advanced
        Server](../Page/Windows_2000_Advanced_Server.md "wikilink")（進階伺服器版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 2000
        Datacenter
        Server](../Page/Windows_2000_Datacenter_Server.md "wikilink")（資料中心伺服器版）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    XP](../Page/Windows_XP.md "wikilink")（即“Windows
    NT”的“5.1”版本，代號：“”，發行日期：2001年10月25日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Home
        Edition](../Page/Windows_XP_Home_Edition.md "wikilink")（家用版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP
        Professional](../Page/Windows_XP_Professional.md "wikilink")（專業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP 64-bit
        Edition](../Page/Windows_XP_64-bit_Edition.md "wikilink")（64位元版）
          - [Windows XP x64 Edition for Itanium systems, Version
            2002](../Page/Windows_XP_x64_Edition_for_Itanium_systems,_Version_2002.md "wikilink")
          - [Windows XP x64 Edition, Version
            2003](../Page/Windows_XP_x64_Edition,_Version_2003.md "wikilink")（發行日期：2003年3月28日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Media
        Center
        Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink")（媒體中心版）
          - [Windows XP Media Center
            Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink")（2002版或原版，代號：“”，發行日期：2002年6月）
          - [Windows XP Media Center Edition
            2003](../Page/Windows_XP_Media_Center_Edition_2003.md "wikilink")（2003版，發行日期：2002年9月）
          - [Windows XP Media Center Edition
            2004](../Page/Windows_XP_Media_Center_Edition_2004.md "wikilink")（2004版，代號：“”，發行日期：2003年11月）
          - [Windows XP Media Center Edition
            2005](../Page/Windows_XP_Media_Center_Edition_2005.md "wikilink")（2005版，代號：“”，發行日期：2004年10月12日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Tablet
        PC
        Edition](../Page/Windows_XP_Tablet_PC_Edition.md "wikilink")（平板電腦版，代號：“”）
          - [Windows XP Tablet PC
            Edition](../Page/Windows_XP_Tablet_PC_Edition.md "wikilink")（原版，發行日期：2002年11月）
          - [Windows XP Tablet PC Edition
            2005](../Page/Windows_XP_Tablet_PC_Edition_2005.md "wikilink")（2005版，發行日期：2004年8月）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Starter
        Edition](../Page/Windows_XP_Starter_Edition.md "wikilink")（低价入門版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP
        Embedded](../Page/Windows_XP_Embedded.md "wikilink")（嵌入版，發行日期：2001年11月28日）
          - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
            Embedded Standard
            2009](../Page/Windows_Embedded_Standard_2009.md "wikilink")（嵌入式2009版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Edition
        N](../Page/Windows_XP_Edition_N.md "wikilink")（N
        =“（不含媒體播放器）”，發行日期：未知）
          - [Windows XP Home Edition
            N](../Page/Windows_XP_Home_Edition_N.md "wikilink")（家用版N）
          - [Windows XP Professional
            N](../Page/Windows_XP_Professional_N.md "wikilink")（專業版N）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Edition
        K](../Page/Windows_XP_Edition_K.md "wikilink")（K版）
          - [Windows XP Home Edition
            K](../Page/Windows_XP_Home_Edition_K.md "wikilink")（家用版K）
          - [Windows XP Professional
            K](../Page/Windows_XP_Professional_K.md "wikilink")（專業版K）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP Edition
        KN](../Page/Windows_XP_Edition_KN.md "wikilink")（KN版）
          - [Windows XP Home Edition
            KN](../Page/Windows_XP_Home_Edition_KN.md "wikilink")（家用版KN）
          - [Windows XP Professional
            KN](../Page/Windows_XP_Professional_KN.md "wikilink")（專業版KN）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Fundamentals
    for Legacy
    PCs](../Page/Windows_Fundamentals_for_Legacy_PCs.md "wikilink")（即“Windows
    NT 5.1+”版本，代號：“”/“”，發行日期：2006年8月7日）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
    2003](../Page/Windows_Server_2003.md "wikilink")（即“Windows NT”的“5.1
    Server”版本版本号为5.2.3790，代號：“”，發行日期：2003年4月23日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
        2003 Web
        Edition](../Page/Windows_Server_2003_Web_Edition.md "wikilink")（Web版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
        2003 Standard
        Edition](../Page/Windows_Server_2003_Standard_Edition.md "wikilink")（標準版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
        2003 Enterprise
        Edition](../Page/Windows_Server_2003_Enterprise_Edition.md "wikilink")（企業版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
        2003 Datacenter
        Edition](../Page/Windows_Server_2003_Datacenter_Edition.md "wikilink")（資料中心版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
        2003 Small Business
        Server](../Page/Windows_Server_2003_Small_Business_Server.md "wikilink")（小型商用伺服器版，發行日期：未知）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Compute
        Cluster Server
        2003](../Page/Windows_Compute_Cluster_Server_2003.md "wikilink")（叢集伺服器版，發行日期：2006年6月）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Storage
        Server
        2003](../Page/Windows_Storage_Server_2003.md "wikilink")（記憶體伺服器版，發行日期：未知）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows XP
    (x64)](../Page/Windows_XP_\(x64\).md "wikilink")（爲“Windows
    NT”的“5.2”版本，發行日期：2005年4月25日）
      - [Windows XP Professional x64
        Edition](../Page/Windows_XP_Professional_x64_Edition.md "wikilink")（64bit專業版）

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Home
    Server](../Page/Windows_Home_Server.md "wikilink")（同爲“Windows
    NT”的“5.2”版本，預期發行時間：2007年後期）

#### Windows Vista 系列

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    Vista](../Page/Windows_Vista.md "wikilink")（即“Windows
    NT”的“6.0”版本，代號：“”，發行日期：2007年1月30日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Starter](../Page/Windows_Vista_Starter.md "wikilink")（簡易版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Basic](../Page/Windows_Vista_Home_Basic.md "wikilink")（家用基本版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Premium](../Page/Windows_Vista_Home_Premium.md "wikilink")（家用進階版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Business](../Page/Windows_Vista_Business.md "wikilink")（商業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Enterprise](../Page/Windows_Vista_Enterprise.md "wikilink")（企業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Ultimate](../Page/Windows_Vista_Ultimate.md "wikilink")（旗舰版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Basic
        N](../Page/Windows_Vista_Home_Basic_N.md "wikilink")（家用基本版N）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Premium
        K](../Page/Windows_Vista_Home_Premium_K.md "wikilink")（家用進階版K）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Business
        N](../Page/Windows_Vista_Business_N.md "wikilink")（商業版N）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Enterprise
        K](../Page/Windows_Vista_Enterprise_K.md "wikilink")（企業版K）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Basic
        K](../Page/Windows_Vista_Home_Basic_K.md "wikilink")（家用基本版K）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Ultimate
        K](../Page/Windows_Vista_Ultimate_K.md "wikilink")（旗舰版K）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista Home
        Basic
        KN](../Page/Windows_Vista_Home_Basic_KN.md "wikilink")（家用基本版KN）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Vista
        Business
        KN](../Page/Windows_Vista_Business_KN.md "wikilink")（商業版KN）

<!-- end list -->

  - Microsoft [Windows Server
    2008](../Page/Windows_Server_2008.md "wikilink")
  - Microsoft [Windows Server 2008
    R2](../Page/Windows_Server_2008_R2.md "wikilink")

#### Windows 7系列

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    7](../Page/Windows_7.md "wikilink")（“Windows
    NT”版本號：6.1，代號：“”（原代號爲“”），发行日期：2009年10月23日）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7
        Starter](../Page/Windows_7_Starter.md "wikilink")（簡易版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7 Home
        Basic](../Page/Windows_7_Home_Basic.md "wikilink")（家用基本版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7 Home
        Premium](../Page/Windows_7_Home_Premium.md "wikilink")（家用進階版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7
        Professional](../Page/Windows_7_Professional.md "wikilink")（專業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7
        Enterprise](../Page/Windows_7_Enterprise.md "wikilink")（企業版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7
        Ultimate](../Page/Windows_7_Ultimate.md "wikilink")（旗舰版）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7 Edition
        N](../Page/Windows_7_Edition_N.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 7 Edition
        E](../Page/Windows_7_Edition_E.md "wikilink")

#### Windows 8全系列

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    8](../Page/Windows_8.md "wikilink")（Windows
    NT版本6.2，發行日期：2012年10月26日）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
    2012](../Page/Windows_Server_2012.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    8.1](../Page/Windows_8.1.md "wikilink")（Windows
    NT版本6.3，發行日期：2013年10月17日）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server 2012
    R2](../Page/Windows_Server_2012_R2.md "wikilink")

#### Windows 10全系列

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    10](../Page/Windows_10.md "wikilink")（Windows NT
    10.0，代號Thershold，發行日期：2015年7月29日）\[1\]
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Server
    2016](../Page/Windows_Server_2016.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows 10
    Mobile](../Page/Windows_10_Mobile.md "wikilink") （Windows NT
    10.0，代號Thershold，發行日期：2015年12月）

### 其他

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    Cairo](../Page/Windows_Cairo.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    PE](../Page/Windows_PE.md "wikilink")（預先安裝環境，有作業系統功能，但官方定位為工具軟體。）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        1.0](../Page/Windows_PE_1.0.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        1.1](../Page/Windows_PE_1.1.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        1.2](../Page/Windows_PE_1.2.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        1.5](../Page/Windows_PE_1.5.md "wikilink")（2004）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        1.6](../Page/Windows_PE_1.6.md "wikilink")（2005）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows PE
        2.0](../Page/Windows_PE_2.0.md "wikilink")（2006）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    Holographic](../Page/Windows_Holographic.md "wikilink") （計算平台）

### 微軟嵌入式作業系統

  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows
    CE](../Page/Windows_CE.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    1.0](../Page/Windows_CE_1.0.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    1.01](../Page/Windows_CE_1.01.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    2.0](../Page/Windows_CE_2.0.md "wikilink") - [Handheld PC
    2.0](../Page/Handheld_PC_2.0.md "wikilink"), [Intel
    Core](../Page/Intel_Core.md "wikilink"), [Embedded Toolkit
    2.0](../Page/Embedded_Toolkit_2.0.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    2.01](../Page/Windows_CE_2.01.md "wikilink") - [Palm PC
    1.0](../Page/Palm_PC_1.0.md "wikilink"), [Auto PC
    1.0](../Page/Auto_PC_1.0.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    2.10](../Page/Windows_CE_2.10.md "wikilink") - [Intel
    Core](../Page/Intel_Core.md "wikilink"), [Embedded Toolkit
    2.1](../Page/Embedded_Toolkit_2.1.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    2.11](../Page/Windows_CE_2.11.md "wikilink") - [Palm-Size PC
    1.1](../Page/Palm-Size_PC_1.1.md "wikilink"), [Palm-Size PC
    1.2](../Page/Palm-Size_PC_1.2.md "wikilink"), [Handheld PC
    Professional](../Page/Handheld_PC_Professional.md "wikilink"),
    [Intel Core](../Page/Intel_Core.md "wikilink"), [Platform
    Builder](../Page/Platform_Builder.md "wikilink"),
    [Webphone](../Page/Webphone.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    2.12](../Page/Windows_CE_2.12.md "wikilink") - [Intel
    Core](../Page/Intel_Core.md "wikilink"), [Platform
    Builder](../Page/Platform_Builder.md "wikilink"), [Auto PC
    2.0](../Page/Auto_PC_2.0.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    3.0](../Page/Windows_CE_3.0.md "wikilink") - [Pocket PC
    2000](../Page/Pocket_PC_2000.md "wikilink"), [Handheld PC
    2000](../Page/Handheld_PC_2000.md "wikilink"), [Intel
    Core](../Page/Intel_Core.md "wikilink"), [Platform
    Builder](../Page/Platform_Builder.md "wikilink"), [Core Add-on
    Pack](../Page/Core_Add-on_Pack.md "wikilink"), [Pocket PC
    2002](../Page/Pocket_PC_2002.md "wikilink"), [Chinese Pocket PC
    2002](../Page/Chinese_Pocket_PC_2002.md "wikilink"), [CE for
    Automotive
    3.5](../Page/CE_for_Automotive_3.5.md "wikilink")，[Smartphone
    2002](../Page/Smartphone_2002.md "wikilink")

<!-- end list -->

  - [Microsoft](../Page/Microsoft.md "wikilink") [Pocket PC
    2002](../Page/Pocket_PC_2002.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    4.0](../Page/Windows_CE_4.0.md "wikilink") - [Net
    Core](../Page/Net_Core.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    4.1](../Page/Windows_CE_4.1.md "wikilink") - [Net
    Core](../Page/Net_Core.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    4.2](../Page/Windows_CE_4.2.md "wikilink") - [Net
    Core](../Page/Net_Core.md "wikilink"), [Windows Automotive
    Net](../Page/Windows_Automotive_Net.md "wikilink"), [Windows Mobile
    2003](../Page/Windows_Mobile_2003.md "wikilink"), [Windows Mobile
    2003 Second
    Edition](../Page/Windows_Mobile_2003_Second_Edition.md "wikilink")（第二版本）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
    2003](../Page/Windows_Mobile_2003.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 for
        PocketPC](../Page/Windows_Mobile_2003_for_PocketPC.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 for
        SmartPhone](../Page/Windows_Mobile_2003_for_SmartPhone.md "wikilink")
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 Second Edition for
        PocketPC](../Page/Windows_Mobile_2003_Second_Edition_for_PocketPC.md "wikilink")（第二版本）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 Second Edition for
        SmartPhone](../Page/Windows_Mobile_2003_Second_Edition_for_SmartPhone.md "wikilink")（第二版本）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 Second Edition for PocketPC
        Phone](../Page/Windows_Mobile_2003_Second_Edition_for_PocketPC_Phone.md "wikilink")（第二版本）
      - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile
        2003 Second Edition for PocketPC
        Phone](../Page/Windows_Mobile_2003_Second_Edition_for_PocketPC_Phone.md "wikilink")（第二版本）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    5.0](../Page/Windows_CE_5.0.md "wikilink") -[Intel
    Core](../Page/Intel_Core.md "wikilink"), [Windows Mobile
    5.0](../Page/Windows_Mobile_5.0.md "wikilink"), [Windows Automotive
    5.0](../Page/Windows_Automotive_5.0.md "wikilink"), [Networked Media
    Device (NMD) Feature
    Pack](../Page/Networked_Media_Device_\(NMD\)_Feature_Pack.md "wikilink"),
    [Windows Mobile 5.0 Second
    Edition](../Page/Windows_Mobile_5.0_Second_Edition.md "wikilink")（第二版本）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 5.0 for
    PocketPC](../Page/Windows_Mobile_5.0_for_PocketPC.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 5.0 for
    SmartPhone](../Page/Windows_Mobile_5.0_for_SmartPhone.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 6
    Classic](../Page/Windows_Mobile_6_Classic.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 6
    Standard](../Page/Windows_Mobile_6_Standard.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 6
    Professional](../Page/Windows_Mobile_6_Professional.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows CE
    6.0](../Page/Windows_CE_6.0.md "wikilink") - [Intel
    Core](../Page/Intel_Core.md "wikilink"), [Windows Mobile Vista
    6.0](../Page/Windows_Mobile_Vista_6.0.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Mobile 6.5
    Professional](../Page/Windows_Mobile_6.5_Professional.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Phone
    7](../Page/Windows_Phone_7.md "wikilink")（[Windows CE
    7核心](../Page/Windows_CE_7.0.md "wikilink")）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Phone
    8](../Page/Windows_Phone_8.md "wikilink")（[Windows
    NT](../Page/Windows_NT.md "wikilink")[内核](../Page/内核.md "wikilink")）
  - [Microsoft](../Page/Microsoft.md "wikilink") [Windows Embedded
    Compact 7](../Page/Windows_Embedded_Compact_7.md "wikilink")

### ARM 版本

  - [Windows RT](../Page/Windows_RT.md "wikilink")

## 参考文献

## 參見

  - [蘋果公司作業系統列表](../Page/蘋果公司作業系統列表.md "wikilink")
  - [操作系统](../Page/操作系统.md "wikilink")（[操作系统列表](../Page/操作系统列表.md "wikilink")）
  - [微软](../Page/微软.md "wikilink")

{{-}}

[\*](../Category/微软操作系统.md "wikilink")
[MS](../Category/操作系统列表.md "wikilink")

1.