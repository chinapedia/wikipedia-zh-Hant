**微風廣場**（[英語](../Page/英語.md "wikilink")：Breeze
Center）是[臺灣一個連鎖零售品牌](../Page/台灣.md "wikilink")，包括[購物中心](../Page/購物中心.md "wikilink")、[百貨公司](../Page/百貨公司.md "wikilink")、[商場](../Page/商場.md "wikilink")、[美食廣場等業種](../Page/美食廣場.md "wikilink")，由塑膠用品製造商[三僑國際與本土飲料大廠](../Page/三僑國際.md "wikilink")[黑松合資成立](../Page/黑松公司.md "wikilink")。在經營上，為了突顯[都會流行風格](../Page/都會.md "wikilink")，微風廣場自從開幕以來即固定經常邀請[模特兒或時尚](../Page/模特兒.md "wikilink")[藝人作為代言人](../Page/藝人.md "wikilink")，並將代言人形象廣泛運用於購物型錄及活動宣傳海報等。[名模](../Page/名模.md "wikilink")[林志玲為微風廣場的第一任代言人](../Page/林志玲.md "wikilink")，之後包括[周汶錡](../Page/周汶錡.md "wikilink")、[香月明美和](../Page/香月明美.md "wikilink")[關穎等皆扮演過此一角色](../Page/關穎.md "wikilink")；[孫芸芸也曾經擔任共同代言人](../Page/孫芸芸.md "wikilink")。開幕初期亦曾以[貝蒂娃娃作為風格象徵](../Page/貝蒂娃娃.md "wikilink")，並發行聯名[信用卡](../Page/信用卡.md "wikilink")。除了位於[臺北市](../Page/台北市.md "wikilink")[復興南路與](../Page/復興南路.md "wikilink")[市民大道交叉口的本館外](../Page/市民大道.md "wikilink")，在臺北市區尚有9處分館及1件開發案。

## 現有分店

<table>
<thead>
<tr class="header">
<th><p>分店名稱</p></th>
<th><p>圖片</p></th>
<th><p>地址</p></th>
<th><p>介紹</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>微風廣場（Breeze Center）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Breeze_Plaza_20130908.jpg" title="fig:Breeze_Plaza_20130908.jpg">Breeze_Plaza_20130908.jpg</a></p></td>
<td><p>105<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/松山區_(臺北市).md" title="wikilink">松山區復興南路一段</a>39號（位於<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/忠孝復興站.md" title="wikilink">忠孝復興站</a>5號出口）</p></td>
<td><p>原址曾為專門生產汽水的黑松臺北中崙工廠（1937年落成），廠區土地於1994年對外公開標售。1995年6月，黑松與土地得標者三僑國際共同合資成立「三僑實業」（後更名為「微風廣場實業」），配合政府推動之工商綜合區之土地政策，自1998年9月開始在原本的工廠用地上動工興建。商場建築的造型由三門建築師事務所與澳洲The Buchan Group負責設計，樓地板總面積為2萬3000坪，其中包含1萬2700坪的營業空間。由於微風廣場與<a href="../Page/太平洋崇光百貨.md" title="wikilink">太平洋SOGO百貨忠孝館之間的步行距離僅約</a>500多公尺，因此微風廣場招商期間即曾傳出與太平洋SOGO展開設櫃廠商與經營人才的爭奪戰[1]。2001年，受到<a href="../Page/颱風納莉_(2001年).md" title="wikilink">納莉颱風所造成的風災影響</a>，微風廣場的開幕時程也向後順延。2001年10月22日開始試賣、10月26日正式開幕[2]，成為臺北市首家開張營運的大型購物中心[3]。</p>
<p>館內分為A、B兩區。A區為地上九層、地下三層的高樓建築，設有<a href="../Page/國賓影城.md" title="wikilink">國賓影城</a>（位於8F～9F）以及<a href="../Page/路易·威登.md" title="wikilink">LV</a>、<a href="../Page/GUCCI.md" title="wikilink">GUCCI</a>、<a href="../Page/Dior.md" title="wikilink">Dior等多家服飾</a>、精品<a href="../Page/品牌.md" title="wikilink">品牌的大</a>、小專櫃；B2亦設有兼賣進口食品，並設置熟食區與<a href="../Page/超級市場.md" title="wikilink">超級市場</a>，後者包括來自法國傳統麵包的<a href="../Page/Maison_Kayser.md" title="wikilink">Maison Kayser與自有品牌的微風超市</a>。2F有agnès b.旗艦店、<a href="../Page/Bose音響.md" title="wikilink">Bose音響</a>、寬庭美學生活館、法國百年寢具FRETTE；5F整層皆為<a href="../Page/紀伊國屋書店.md" title="wikilink">紀伊國屋書店微風店的範圍</a>；以生活雜貨為主的<a href="../Page/台隆手創館.md" title="wikilink">台隆手創館與</a><a href="../Page/無印良品.md" title="wikilink">無印良品也分別在</a>6F與3F設有分店，由<a href="../Page/統一企業.md" title="wikilink">統一企業出資引進的日本生活雜貨及餐廳</a><a href="../Page/Afternoon_Tea.md" title="wikilink">Afternoon Tea也進駐</a>3F；<a href="../Page/UNIQLO.md" title="wikilink">UNIQLO則位於</a>4F。</p>
<p>B區為地上兩層、地下四層的「L」型建築，設有<a href="../Page/PRADA.md" title="wikilink">PRADA</a>、<a href="../Page/周仰傑.md" title="wikilink">Jimmy Choo</a>、<a href="../Page/Burberry.md" title="wikilink">Burberry等國際品牌的專櫃</a>；地下樓層皆作為汽、機車<a href="../Page/停車場.md" title="wikilink">停車場使用</a>；建築西南側的地面噴泉廣場，有時亦作為活動舉辦場地。二樓設有介紹黑-{松}-企業發展史，並以仿古方式重現1950年代老街景物的「黑-{松}-世界」展覽館；與丈夫<a href="../Page/廖鎮漢.md" title="wikilink">廖鎮漢同為微風廣場核心人物的</a><a href="../Page/孫芸芸.md" title="wikilink">孫芸芸</a>。</p></td>
</tr>
<tr class="even">
<td><p>微風臺北車站（Breeze Taipei Station）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Breeze_Taipei_Station_outside_banner_2012-06-16.jpg" title="fig:Breeze_Taipei_Station_outside_banner_2012-06-16.jpg">Breeze_Taipei_Station_outside_banner_2012-06-16.jpg</a></p></td>
<td><p>100<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/中正區_(臺北市).md" title="wikilink">中正區北平西路</a>3號2F至B1（位於<a href="../Page/台北車站.md" title="wikilink">臺北車站</a>）</p></td>
<td><p>原址為曾在<a href="../Page/台北車站.md" title="wikilink">臺北車站二樓營業</a>，因經營不善而歇業的<a href="../Page/金華百貨.md" title="wikilink">金華百貨</a>。2006年12月25日，<a href="../Page/三僑實業.md" title="wikilink">三僑實業與</a><a href="../Page/台灣鐵路管理局.md" title="wikilink">臺鐵簽訂臺北車站二樓</a>、一樓和地下一樓商場的經營合約（期限12年），將閒置空間改造成以<a href="../Page/美食廣場.md" title="wikilink">美食廣場為主要定位的車站型商場</a>。2007年10月26日，規劃為美食街和餐廳的二樓開幕[4]；2011年11月11日，號稱「全國最大食尚伴手禮中心」、以<a href="../Page/伴手禮.md" title="wikilink">伴手禮和外帶專區為主的地下一樓與地上一樓開幕</a>[5]，商場營業面積約3,500坪。2016年營業額約為25億元。</p></td>
</tr>
<tr class="odd">
<td><p>微風A1機場捷運站（Breeze A1 Airport MRT Station）</p></td>
<td></td>
<td><p>100<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/中正區_(臺北市).md" title="wikilink">中正區鄭州路</a>8號1F、B1（位於<a href="../Page/桃園捷運.md" title="wikilink">桃園捷運</a><a href="../Page/臺北車站_(桃園捷運).md" title="wikilink">臺北車站</a>）</p></td>
<td><p>機場捷運A1站500坪商場經營權由微風得標，業種包含伴手禮與台灣文創禮品[6]。</p></td>
</tr>
<tr class="even">
<td><p>微風臺大醫院（Breeze NTU Hospital）</p></td>
<td></td>
<td><p>100<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/中山區_(臺北市).md" title="wikilink">中山區中山南路</a>7號B1（鄰近<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/台大醫院站.md" title="wikilink">臺大醫院站</a>2號出口）</p></td>
<td><p>原為<a href="../Page/誠品書店.md" title="wikilink">誠品臺大醫院店</a>，後由微風集團標得經營權，2014年4月開業，同年6月改裝開幕。其定位為<a href="../Page/美食廣場.md" title="wikilink">美食廣場</a>，亦是微風跨足醫院型商場的首店[7]。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/微風南京.md" title="wikilink">微風南京</a>（Breeze NAN JING）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Breeze_Nanjing_20160430.jpg" title="fig:Breeze_Nanjing_20160430.jpg">Breeze_Nanjing_20160430.jpg</a></p></td>
<td><p>105<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/松山區_(臺北市).md" title="wikilink">松山區南京東路三段</a>337號4F～B2（目前位於<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/台北小巨蛋站.md" title="wikilink">臺北小巨蛋站以西</a>、<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/南京復興站.md" title="wikilink">南京復興站以東</a>，自<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/南京復興站.md" title="wikilink">南京復興站</a>6號出口往敦化北路方向行走約10分鐘內即可到達。）</p></td>
<td><p>原為<a href="../Page/momo百貨.md" title="wikilink">momo百貨南京店</a>。2013年8月16日，微風集團與富邦集團簽約取得商場經營權，於2013年9月1日正式開幕[8]。由於經營績效並不如微風其他場館，故於2015年5月18日起，除1F~2F無印良品及B1FUNIQLO繼續營業外，全館進行封館改裝。2016年1月7日重新開幕，營業樓層由原本B2~2F向上延伸至4F，並因應辦公商圈特性，重新開幕後餐飲占比從25%提高至40%，年營業額目標為由改裝前的約8億元成長至20億元。 微風南京屬於中型<a href="../Page/購物中心.md" title="wikilink">購物中心之規模</a>（營業面積為5,700坪），營業樓層為地上四層至地下二層，主力客層為25歲至45歲的女性族群。全館計有115個櫃位，其中有23個獨家櫃位，並首創於1F設置甜點櫃位[9]。</p></td>
</tr>
<tr class="even">
<td><p>微風信義（Breeze XIN YI）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Breeze_Xin_Yi_at_Cathay_Landmark_20150912_1.jpg" title="fig:Breeze_Xin_Yi_at_Cathay_Landmark_20150912_1.jpg">Breeze_Xin_Yi_at_Cathay_Landmark_20150912_1.jpg</a></p></td>
<td><p>110<a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/信義區_(臺北市).md" title="wikilink">信義區</a><a href="../Page/忠孝東路.md" title="wikilink">忠孝東路五段</a>68號4F至B1、45F至47F（<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/市政府站_(臺北市).md" title="wikilink">市政府站</a>3號出口連通道連通商場B1）</p></td>
<td><p>位於<a href="../Page/國泰置地廣場.md" title="wikilink">國泰置地廣場的地上四層至地下一層以及地上四十七層至地上四十五層</a>，樓地板面積約12,700坪。2011年，微風集團以一年五億元租金向國泰人壽簽訂A3館租約[10]，投資金額約為16億元；2015年11月5日開業，首年營業額目標為53億元。 微風信義之規模屬於中型<a href="../Page/購物中心.md" title="wikilink">購物中心</a>（營業面積約9,000坪[11]），全館設有108個專櫃，全臺獨家櫃位佔35％，客層定位與微風廣場同為國際精品與流行時尚，特色服務為2F女廁提供廁所咖啡，讓女性能利用梳妝的時間悠閒品嘗咖啡[12]。</p></td>
</tr>
<tr class="odd">
<td><p>微風松高（Breeze SONG GAO）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Humble_House_Taipei_and_Breeze_Song_Gao_20160510.jpg" title="fig:Humble_House_Taipei_and_Breeze_Song_Gao_20160510.jpg">Humble_House_Taipei_and_Breeze_Song_Gao_20160510.jpg</a></p></td>
<td><p>110<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/信義區_(臺北市).md" title="wikilink">信義區松高路</a>16號4F至B2（鄰近<a href="../Page/台北捷運.md" title="wikilink">臺北捷運</a><a href="../Page/市政府站_(臺北市).md" title="wikilink">市政府站</a>3號出口）</p></td>
<td><p>原址為<a href="../Page/momo百貨.md" title="wikilink">momo百貨計劃開設的信義店</a>，與<a href="../Page/寒舍艾麗酒店.md" title="wikilink">寒舍艾麗酒店同棟</a>。2013年8月，富邦集團因momo百貨信義店招商不順，而將商場經營權轉售給微風集團，由微風承接富邦人壽A10館商場之租約[13]；2014年10月24日開業，首年營業額目標為30億元。 微風松高之規模屬於小型購物中心（營業面積約3,800坪），營業樓層為地上四層至地下兩層，其主力客層定為16歲到30歲之年輕族群。全館計有126個櫃位，並引進臺灣首間<a href="../Page/H&amp;M.md" title="wikilink">H&amp;M進駐</a>[14]。</p></td>
</tr>
<tr class="even">
<td><p>微風南山（Breeze NAN SHAN）</p></td>
<td></td>
<td><p>110<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/信義區_(臺北市).md" title="wikilink">信義區松廉路</a>3號7F至B2 、46F至48F</p></td>
<td><p>位於<a href="../Page/臺北南山廣場.md" title="wikilink">臺北南山廣場</a>（原<a href="../Page/台北世界貿易中心.md" title="wikilink">世貿二館</a>）內，商場部分由微風置地股份有限公司承租20年[15]，2019年1月10日開業，首年營業額目標為70億元。 微風南山其規模屬於大型購物中心，樓地板面積約16,200坪，營業樓層為地上七層至地下二層以及地上四十八層至地上四十六層；全館約有200個櫃位，其中獨家店櫃130間，佔比近七成，而全館餐飲佔比高達45％，並有多家米其林星級餐廳進駐[16]。特色主力店有佔地2,222坪的微風超市，以及JR東日本旗下購物中心<a href="../Page/atre.md" title="wikilink">atre</a>（海外首間分店）[17]。</p></td>
</tr>
<tr class="odd">
<td><p>微風三總商店街（Breeze TSG HOSPITAL）</p></td>
<td></td>
<td><p>114<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/內湖區.md" title="wikilink">內湖區成功路二段</a>325號B1（<a href="../Page/三軍總醫院.md" title="wikilink">三軍總醫院內湖總院地下美食街</a>）</p></td>
<td><p>原為大成三總醫院美食街，後由微風集團標得經營權，定位為美食廣場，2015年12月改裝開幕[18]。</p></td>
</tr>
<tr class="even">
<td><p>微風中央研究院（Breeze Academia Sinica）</p></td>
<td></td>
<td><p>115<a href="../Page/台北市.md" title="wikilink">臺北市</a><a href="../Page/南港區.md" title="wikilink">南港區研究院路二段</a>128號（<a href="../Page/中央研究院.md" title="wikilink">中央研究院學術活動中心</a>）</p></td>
<td><p>定位為美食廣場，分為學術活動中心及蔡元培紀念館兩處，2018年3月1日位於蔡元培紀念館的Trine&amp;Zen Cafe崔妮傑恩咖啡廳開業，2018年10月26日位於學術活動中心的美食廣場開業，有11個餐飲品牌進駐[19]。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 未來計畫

| 分店名稱                  | 圖片 | 地址                                                                  | 介紹                                                                                                                      |
| --------------------- | -- | ------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| 微風南港（Breeze NAN GANG） |    | 115[臺北市](../Page/台北市.md "wikilink")[南港區](../Page/南港區.md "wikilink") | [南港輪胎舊廠的](../Page/南港輪胎.md "wikilink")「世界明珠」開發案，商場部分由微風旗下的長僑投資開發股份有限公司承租20年，樓地板面積約為2,500坪，將引進微風直營品牌、超市、牛排館、鐵板燒等業種\[20\]。 |
|                       |    |                                                                     |                                                                                                                         |

## 過往分店

| 分店名稱                    | 圖片                                                                                                                                                                                                  | 地址                                                                                                                                                                    | 介紹                                                                                                                                                                                                                                             |
| ----------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 微風忠孝（Breeze Zhong Xiao） | [Breeze_Zhongxiao_in_Huaxin_Building_20131120.jpg](https://zh.wikipedia.org/wiki/File:Breeze_Zhongxiao_in_Huaxin_Building_20131120.jpg "fig:Breeze_Zhongxiao_in_Huaxin_Building_20131120.jpg") | 106[臺北市](../Page/台北市.md "wikilink")[大安區忠孝東路四段](../Page/大安區_\(臺北市\).md "wikilink")219號1F（位於[臺北捷運](../Page/台北捷運.md "wikilink")[忠孝敦化站](../Page/忠孝敦化站.md "wikilink")2號出口） | 位於華新大樓地上一層至地上二層，營業面積約1,000坪，原為[ATT流行廣場忠孝店](../Page/ATT.md "wikilink")。2005年6月10日開幕，初期原有專櫃皆保留，以年輕客層的進口服飾為主\[21\]。2015年3月封館改裝，並於同年7月9日重新開幕，全館皆由韓國E-land集團旗下品牌「SPAO」、「MIXXO」及「Cafe Lugo」進駐 \[22\]。因東區商圈租金過高、人流減少等因素，已於2018年6月30日結束營業\[23\]\[24\]。 |
|                         |                                                                                                                                                                                                     |                                                                                                                                                                       |                                                                                                                                                                                                                                                |

## 樓層簡介

  - 微風廣場

<!-- end list -->

  - 8-9F 微風國賓影城 Ambassador Theatres
  - 7F 時尚品味館
  - 6F 台隆手創館 HANDS
  - 5F 紀伊國屋書店
  - 4F UNIQLO
  - 3F MUJI / Afternoon Tea
  - 2F 都會時尚館 Life Style
  - 1F 國際精品館 Luxury Ave.
  - GF 夢幻仙履館 Cinderella City
  - B1 微風美饌 Breeze Gourmet
  - B2 微風超市 Breeze Super

<!-- end list -->

  - 微風台北車站

<!-- end list -->

  - 2F 食尚中心
  - 1F 伴手禮中心
  - B1 台灣名產中心 / 趣味小物

<!-- end list -->

  - 微風信義

<!-- end list -->

  - 45-47F 天際薈 Sky Dining & Bar
  - 4F 名廚坊 Master Chef’s Kitchen
  - 3F 名品迴廊 In-Style Gallery
  - 2F 精品 & 鞋履沙龍 Boutique & Shoe Salon
  - 1F 精品大道 Luxury Ave.
  - B1 風尚廣場 Living Square
  - B3 活動會場 & 停車場 Breeze Gallery & Parking Area

<!-- end list -->

  - 微風松高

<!-- end list -->

  - 4F 天際花園 SKYLINE PARK
  - 3F 潮流玩家 HIPSTER
  - 2F 流行女孩 POP GIRL
  - 1F 超級巨星 SUPERSTARS
  - B1 居家美妝 BEAUTY & LIVING
  - B2 食尚主廚 CHEF

<!-- end list -->

  - 微風三總商店街

<!-- end list -->

  - B1 - Mr.果 、 Yamazaki山崎麵包-三總店 、 三商巧福 、 乳油木之家 、 喜樂絲 、 好好吃大餛飩 、 安永鮮食 、
    小南門點心世界 、 屈臣氏 、 快樂味 、 新加玻琉元堂 、 日出茶太 、 明德素食園 、 真食補滴雞精 、 維康醫療用品 、
    翰林茶棧 、 輕活圈子 、 采豐中華料理 、 金石堂書店 、 阿鑫麵線

<!-- end list -->

  - 微風台大醫院

<!-- end list -->

  - B1 美食廣場 - C.Y.L 馨亞麗 、 STARBUCKS COFFEE 、 Yamazaki山崎麵包 巡洋記、八番赤野 、 品御方
    、 天仁喫茶趣TOGO 、 好好吃大餛飩 、 好實選果 、 安永鮮食 、 小南門點心世界 、 巴掌包 、 德風健康館 、 摩斯漢堡 、
    明德素食園 、 朱記餡餅粥店 、 欣豐自助餐 、 洪十一擔仔麵 、 湯布院本川製麵所 、 烽記沙茶牛肉炒麵 、 皇品珍 、
    真食補滴雞精 、 老董牛肉麵 、 高麗亭

## 營業時間

  - 微風廣場

<!-- end list -->

  - 週日 \~ 週三 11:00 am \~ 09:30 pm
  - 週四 \~ 週六 11:00 am \~ 10:00 pm

<!-- end list -->

  - 微風松高

<!-- end list -->

  - 週日 \~ 週三 11:00 am \~ 09:30 pm
  - 週四 \~ 週六 11:00 am \~ 10:00 pm

<!-- end list -->

  - 微風台北車站

<!-- end list -->

  - 週一 \~ 週日 10:00 am \~ 10:00 pm

<!-- end list -->

  - 微風信義

<!-- end list -->

  - 週日 \~ 週三 11:00 am \~ 09:30 pm
  - 週四 \~ 週六 11:00 am \~ 10:00 pm

<!-- end list -->

  - 微風三總商店街

<!-- end list -->

  - 週一 \~ 週日 06:30 am \~ 10:30 pm

<!-- end list -->

  - 微風台大醫院

<!-- end list -->

  - 週一 \~ 週日 6:30 am \~ 9:30 pm

<!-- end list -->

  - 微風中央研究院

<!-- end list -->

  - 週一 \~ 週日 8:00 am \~ 7:00 pm

## 代言人

  - 女性

<!-- end list -->

  - [林志玲](../Page/林志玲.md "wikilink")
  - [周汶錡](../Page/周汶錡.md "wikilink")
  - [香月明美](../Page/香月明美.md "wikilink")
  - [關穎](../Page/關穎.md "wikilink")
  - [孫芸芸](../Page/孫芸芸.md "wikilink")
  - [昆凌](../Page/昆凌.md "wikilink")
  - [郭源元](../Page/郭源元.md "wikilink")
  - [陳艾熙](../Page/陳艾熙.md "wikilink")
  - [陳若穎](../Page/陳若穎.md "wikilink")
  - [書那娜](../Page/書那娜.md "wikilink")
  - [林繪里香](../Page/林繪里香.md "wikilink")
  - [宋蘋恩](../Page/宋蘋恩.md "wikilink")

<!-- end list -->

  - 男性

<!-- end list -->

  - [溫耀任](../Page/溫耀任.md "wikilink")

<!-- end list -->

  - 小孩

<!-- end list -->

  - [池東澤](../Page/池東澤.md "wikilink")

## 參考資料與注釋

## 相關條目

  - [太平洋崇光百貨](../Page/太平洋崇光百貨.md "wikilink")
  - [遠東百貨](../Page/遠東百貨.md "wikilink")
  - [京華城](../Page/京華城.md "wikilink")
  - [臺北101](../Page/台北101.md "wikilink")
  - [新光三越](../Page/新光三越.md "wikilink")
  - [美麗華百樂園](../Page/美麗華百樂園.md "wikilink")
  - 轉投資公司

<!-- end list -->

1.  [微風事業股份有限公司](../Page/微風事業股份有限公司.md "wikilink")
2.  [微風美饌股份有限公司](../Page/微風美饌股份有限公司.md "wikilink")
3.  [微風數位時代股份有限公司](../Page/微風數位時代股份有限公司.md "wikilink")
4.  [微風場站開發股份有限公司](../Page/微風場站開發股份有限公司.md "wikilink")
5.  [聖約國際開發股份有限公司](../Page/聖約國際開發股份有限公司.md "wikilink")
6.  [僑漢股份有限公司](../Page/僑漢股份有限公司.md "wikilink")
7.  [虎將餐飲股份有限公司](../Page/虎將餐飲股份有限公司.md "wikilink")

## 外部連結

  - [微風廣場Breeze](http://www.breezecenter.com/)

  - [微風中央研究院](https://web.archive.org/web/20180318054917/https://www.breezecenter.com/branch/list?branch_id=011)

  -
  -
  - [微風A1機場捷運站Breeze A1 Airport MRT Station - 首頁|
    Facebook](https://zh-tw.facebook.com/BreezeA1Airport/)

  -
  -
  -
  -
  - [台灣公司資料](http://company.g0v.ronny.tw/id/89477962)

  - [微風南山餐廳美食介紹與攻略](https://weismile.tw/breeze-arte/)

[W](../Category/總部位於臺北市松山區的工商業機構.md "wikilink")
[W](../Category/台北市商場.md "wikilink")
[Category:2001年台灣建立](../Category/2001年台灣建立.md "wikilink")
[Category:2001年開業商場](../Category/2001年開業商場.md "wikilink")

1.  過去曾為太平洋SOGO總經理的岡一郎，2000年退休後隨即跳槽至微風廣場擔任總經理；太平洋SOGO曾因此向[法院聲請](../Page/法院.md "wikilink")[假處分](../Page/假處分.md "wikilink")，試圖禁止其任職。此外，太平洋SOGO忠孝館與進駐廠商簽訂的合約中也有「半徑兩公-{里}-內不得設櫃」的規定（外界稱之為「微風條款」），微風廣場亦曾因此向[公平交易委員會提出檢舉](../Page/公平交易委員會.md "wikilink")。

2.  [微風廣場開幕
    冠蓋雲集](http://www.libertytimes.com.tw/2001/new/oct/27/today-e2.htm)


3.  [萬坪百貨賣場 捲起千堆雪 揮別蜜月人潮
    今春見勝負](http://www.libertytimes.com.tw/2002/new/feb/3/today-e5.htm)


4.  [微風台北車站開幕
    全台最大食尚中心](http://www.epochtimes.com/b5/7/10/26/n1880548.htm)

5.  [微風台北車站 開賣伴手禮 重新開幕進駐88家店
    旅客讚方便](http://www.appledaily.com.tw/appledaily/article/headline/20111112/33810879)

6.  [機場捷運A1商場
    微風奪標](http://www.chinatimes.com/newspapers/20170223000052-260202)

7.  [微風進駐台大醫院美食街　人潮擠爆](http://m.appledaily.com.tw/realtimenews/article/new/20140604/410256)

8.  [momo百貨走入歷史　微風南京今日起接手營業](http://www.nownews.com/2013/09/01/327-2981055.htm)

9.  [微風南京變身
    靠「女力」拚年收20億](http://a.udn.com/focus/2016/01/07/16699/index.html)

10. [微風進駐信義區 打造台北中環](http://www.sinyi.com.tw/knowledge/newsCt.php/1671)

11.
12. [【更新】喝咖啡不只聊是非　微風信義創「廁所咖啡」](http://www.appledaily.com.tw/realtimenews/article/new/20151103/724656/)

13. [富邦集團momo經營不善
    微風接棒](http://www.chinatimes.com/newspapers/20130817000383-260114)

14. [台微風松高開幕
    首日業績估1千5百萬](http://www.epochtimes.com/b5/14/10/24/n4280067.htm)

15. 彭禎伶，[挑戰新光三越在信義區霸主地位 南山廣場商場
    微風承租](http://www.chinatimes.com/newspapers/20160609000869-260202)，工商時報，2016-06-09

16. [微風南山創造五個第一](https://tw.news.yahoo.com/amphtml/微風南山-創造五個第-232325108.html)

17. [微風攜手三井、ATRE 共同開發微風南山](https://udn.com/news/story/7241/2630965)

18. [微風美食街再下一城
    三總改裝12月營運](http://www.appledaily.com.tw/appledaily/article/finance/20150803/36700860/)

19. 林海，[插旗中央研究院　微風開出新據點](https://tw.appledaily.com/new/realtime/20181024/1453687/)，蘋果日報，2018-10-24

20. 曾仁凱，[南港「世界明珠」超級大案
    引進微風廣場進駐](https://money.udn.com/money/story/5710/3417012)，經濟日報，2018-10-11

21. [忠孝東路ATT換招牌　微風二館10日亮相](http://www.nownews.com/2005/06/09/1138-1801495.htm)

22. 張妤瑄，[直擊／SPAO、MIXXO開幕人潮滿　290元包1分鐘架上空](http://www.ettoday.net/news/20150712/533778.htm#ixzz4Bde2yTzO)，ETtoday新聞雲，2015-07-12

23. 張妤瑄，[SJ加持也沒用！快時尚韓牌SPAO「撤出東區」，全台剩1間](https://www.ettoday.net/news/20180615/1191654.htm)
    ，ETtoday新聞雲，2018-06-15

24. 邱怡萱，[台北東區沒落！2大韓系服飾也掰了...揭密驚人租金](https://www.chinatimes.com/realtimenews/20180620000003-260410)
    ，中時電子報 ，2018-06-20