**德化县**（）是[中国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[泉州市下辖的一个](../Page/泉州市.md "wikilink")[县](../Page/县.md "wikilink")。中国当代[瓷器的著名产地](../Page/瓷器.md "wikilink")，1996年被国务院发展研究中心命名为“中国陶瓷之乡”，2003年又被称评为“中国民间（陶瓷）艺术之乡”，获“中国瓷都·德化”之称。有些媒体将德化县与[景德镇](../Page/景德镇.md "wikilink")、[醴陵并称为中国古代三大瓷都](../Page/醴陵.md "wikilink")\[1\]。面积2220平方千米，人口30万。县人民政府驻[浔中镇](../Page/浔中镇.md "wikilink")。

## 历史沿革

唐[光启二年](../Page/光启.md "wikilink")（886年），[王潮克](../Page/王潮.md "wikilink")[泉州](../Page/泉州.md "wikilink")，其弟[王审知創立](../Page/王审知.md "wikilink")[閩國](../Page/閩國.md "wikilink")。[閩國龙启元年](../Page/閩國.md "wikilink")（933年），以[永泰县归德场置德化县](../Page/永泰县.md "wikilink")。

[宋代属](../Page/宋代.md "wikilink")[泉州](../Page/泉州.md "wikilink")，[元代属](../Page/元代.md "wikilink")[福建行省泉州路](../Page/福建行省.md "wikilink")。明朝属福建省泉州府。

[清](../Page/清.md "wikilink")[雍正時改属](../Page/雍正.md "wikilink")[永春直隶州](../Page/永春直隶州.md "wikilink")。1950年10月劃歸泉州。

## 行政区划

辖10个镇、8个乡：

  - 镇：[浔中镇](../Page/浔中镇.md "wikilink")、[龙浔镇](../Page/龙浔镇.md "wikilink")、[三班镇](../Page/三班镇.md "wikilink")、[龙门滩镇](../Page/龙门滩镇.md "wikilink")、[雷峰镇](../Page/雷峰镇.md "wikilink")、[南埕镇](../Page/南埕镇.md "wikilink")、[水口镇](../Page/水口镇_\(德化县\).md "wikilink")、[赤水镇](../Page/赤水镇_\(德化县\).md "wikilink")、[葛坑镇](../Page/葛坑镇.md "wikilink")、[上涌镇](../Page/上涌镇.md "wikilink")。
  - 乡：[盖德乡](../Page/盖德乡.md "wikilink")、[杨梅乡](../Page/杨梅乡_\(德化县\).md "wikilink")、[汤头乡](../Page/汤头乡.md "wikilink")、[桂阳乡](../Page/桂阳乡.md "wikilink")、[国宝乡](../Page/国宝乡.md "wikilink")、[美湖乡](../Page/美湖乡.md "wikilink")、[大铭乡](../Page/大铭乡.md "wikilink")、[春美乡](../Page/春美乡.md "wikilink")。

## 地理

以中低山地为主，最高峰[戴云山](../Page/戴云山.md "wikilink")，海拔1856米，位于县中心。

## 交通

  - 203省道
  - 206省道
  - [厦沙高速公路德化段](../Page/厦沙高速公路.md "wikilink")

## 经济

主要以[陶瓷出口为主](../Page/陶瓷器.md "wikilink")，经济产业较为单一，德化丧失了很多经济亮点，近年来随着政府将绿色生态作为一个卖点，旅游是一个重要的突破口。

## 教育

  - [泉州工艺美术职业学院](../Page/泉州工艺美术职业学院.md "wikilink")
  - 德化一中

## 风景名胜

  - [戴云山](../Page/戴云山.md "wikilink")：现为[戴云山自然保护区](../Page/戴云山自然保护区.md "wikilink")。
  - [石牛山](../Page/石牛山_\(德化縣\).md "wikilink")
  - [岱仙瀑布](../Page/岱仙瀑布.md "wikilink")
  - [九仙山](../Page/九仙山（德化）.md "wikilink")：在赤水、上涌、大铭三乡交界处，海拔1658米，面积约35平方公里。景区在冬天因时常出现雾凇景观而出名。

## 历代名人

  - [何朝宗](../Page/何朝宗_\(德化\).md "wikilink")，德化县浔中乡隆泰村后所人。明代驰名中外的瓷雕艺术大师。何朝宗的祖籍在江西省临川县甘二都，后移居南城县十六都四脚桥。其先祖何昆源，号德举，明洪武二年（1369）任江西建昌府卫军，七年调泉州右卫所任右营旗宫，十七年奉命拨军到德化县隆泰社厚苏村（即今后所）屯垦，并定居开基该地。其子世样、孙尚志皆习文，先后出任莆田县儒学训导和古田县儒学教谕。
  - [陈仁海](../Page/陈仁海.md "wikilink")

[Taocijie_20151129.JPG](https://zh.wikipedia.org/wiki/File:Taocijie_20151129.JPG "fig:Taocijie_20151129.JPG")
[Guojicaotang_20151129.JPG](https://zh.wikipedia.org/wiki/File:Guojicaotang_20151129.JPG "fig:Guojicaotang_20151129.JPG")

## 特产

[德化白瓷和](../Page/德化白瓷.md "wikilink")[德化黑鸡](../Page/德化黑鸡.md "wikilink")（亦戴云黑鸡）为[中国地理标志产品](../Page/中国地理标志产品.md "wikilink")\[2\]。而德化瓷烧製技艺则被列入[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

## 注释

<div class="references-small">

<references />

</div>

[德化县](../Category/德化县.md "wikilink")
[县](../Category/泉州区县市.md "wikilink")
[泉州](../Category/福建省县份.md "wikilink")

1.
2.  [《地理标志产品保护规定》实施以来批准的地理标志名录（2005年7月至2009年6月）](http://kjs.aqsiq.gov.cn/dlbzcpbhwz/bhml/200906/t20090622_118997.htm)