**德古拉**（Dracula，或譯為**卓九勒**），原型來自外號為「采佩什」（Ţepeş，意为刺棒）的[中世纪人物](../Page/中世纪.md "wikilink")：[瓦拉几亚大公](../Page/瓦拉几亚统治者列表.md "wikilink")[弗拉德三世](../Page/弗拉德三世.md "wikilink")。

弗拉德三世在1456年至1462年間統治現在的[羅馬尼亞地區](../Page/羅馬尼亞.md "wikilink")。
[Vlad.dracula.jpg](https://zh.wikipedia.org/wiki/File:Vlad.dracula.jpg "fig:Vlad.dracula.jpg")
當時的敵軍[鄂圖曼土耳其人曾在德古拉城堡前看見兩萬人被插在長矛上任由其腐爛](../Page/鄂圖曼土耳其.md "wikilink")，而被這令人作噁的景象嚇得拔腿就跑。儘管多數人將德古拉視為虛構的嗜血怪物，但羅馬尼亞人視他為民族英雄，而德古拉王子的敵人很樂見於歷史將他變成民間故事中邪惡的**[吸血鬼](../Page/吸血鬼.md "wikilink")**。

## 小说

[愛爾蘭作家](../Page/愛爾蘭.md "wikilink")[布莱姆·斯托克於](../Page/布莱姆·斯托克.md "wikilink")1897年寫了一本名為《[德古拉](../Page/德古拉_\(小說\).md "wikilink")》的小說，小說中的德古拉伯爵是個嗜血、專挑年輕美女下手的吸血鬼。小說後來被改編成電影，而這座出現過在無數吸血殭屍電影中，小說中描述、位於羅馬利亞的[布蘭城堡](../Page/布蘭城堡.md "wikilink")（即德古拉古堡）一夜成名。

[美國作家](../Page/美國.md "wikilink")[伊麗莎白·柯斯托娃於](../Page/伊麗莎白·柯斯托娃.md "wikilink")2005年發表的作品《[歷史學家](../Page/歷史學家_\(小說\).md "wikilink")》亦在歐美掀起新一輪的吸血鬼熱潮。此書並會改編成電影，電影版權由[新力影業奪得](../Page/新力影業.md "wikilink")。

## 電影

[布莱姆·斯托克撰寫的](../Page/布莱姆·斯托克.md "wikilink")《[德古拉](../Page/德古拉_\(小說\).md "wikilink")》小說造成轟動，小說內容與德古拉伯爵成為許多人創作的靈感來源。許多以「德古拉」為名的電影作品不斷地被拍攝出來。「德古拉伯爵」也成為一種經典代表，並出現在各種以吸血鬼為題材的電影當中。

隨著時代的改變，德古拉在各種電影作品中的形象也有所不同。在早期的電影當中，吸血鬼德古拉的形象是扭曲而醜陋，擁有足以讓人望而生畏的恐怖外貌，例如1922年的《[不死殭屍—恐慄交響曲](../Page/不死殭屍—恐慄交響曲.md "wikilink")》，與1979年翻拍的《[诺斯费拉图：夜晚的幽灵](../Page/诺斯费拉图：夜晚的幽灵.md "wikilink")》。

1930年代之後的電影，德古拉又有另一番不同的形象。這時期的德古拉伯爵，是具有貴族氣息的[紳士](../Page/紳士.md "wikilink")，通常梳著整齊的髮型、穿著深色的正式[西服](../Page/西服.md "wikilink")、披著黑色的寬大[披風](../Page/披風.md "wikilink")。這樣的外型亦成為一種經典的形象；以演出該類型德古拉而聞名的演員有，在[環球影業的](../Page/環球影業.md "wikilink")1931年版《[德古拉](../Page/德古拉_\(電影\).md "wikilink")》及其後續系列中）與[克里斯多福·李](../Page/克里斯多福·李.md "wikilink")（在咸馬電影的1958年版《[德古拉](../Page/德古拉_\(1958年電影\).md "wikilink")》及其後續系列中）。

1958年版的《德古拉》非常成功，該片的製片商亦為該片拍攝了一系列的[續集](../Page/續集.md "wikilink")，將吸血鬼電影推向另一個巔峰。

1992年的電影《-{zh-hans:[吸血殭屍：驚情四百年](../Page/吸血殭屍：驚情四百年.md "wikilink");
zh-hant:[吸血鬼：真愛不死](../Page/吸血鬼：真愛不死.md "wikilink");}-》也是以布莱姆·斯托克的小說為藍本而拍攝。在電影當中飾演德古拉伯爵的演員[蓋瑞·歐德曼](../Page/蓋瑞·歐德曼.md "wikilink")，在外型上雖與傳統的形象略有不同，不再梳著拘謹的髮型、穿著深色衣服與誇張的披風，但其出色的演出與紳士形象亦讓這部劇情取自舊題材的電影成為經典。

2000年以後，在各種吸血鬼電影當中出現的德古拉形象開始有了更大的轉變。例如《[神鬼大反撲](../Page/神鬼大反撲.md "wikilink")》中，由[傑哈德·巴特勒飾演的德古拉](../Page/傑哈德·巴特勒.md "wikilink")，就具備了性感與健美的形象，與傳統樣貌全然不同。另外，在電影《[刀鋒戰士3](../Page/刀鋒戰士3.md "wikilink")》（Blade:
Trinity）中登場的德古拉，就似大多數[動作片當中的主角](../Page/動作片.md "wikilink")，是身材壯碩、力大無窮的戰士形象。

## 德古拉堡

德古拉堡位於[羅馬尼亞的](../Page/羅馬尼亞.md "wikilink")[布拉索夫](../Page/布拉索夫.md "wikilink")，是當地一個著名旅遊點，古堡估計價值2500萬美元，原屬[瑪麗皇后所有](../Page/玛丽_\(爱丁堡\).md "wikilink")，1938年傳給女兒伊萊亞娜公主，1948年被當時的政府沒收，但羅馬尼亞政府於2006年初通過法例，將[共產主義政權時代被沒收的產業歸還業主](../Page/共產主義.md "wikilink")，並成立「產業基金」，向有損失的業主賠償。同年5月，當地政府文化部決定將古堡歸還原來的主人。這座14世紀的拜恩古堡，現時周邊開設不少售賣德古拉羊毛外衣和吸血殭屍酒的攤檔。

## 參見

  - [弗拉德三世](../Page/弗拉德三世.md "wikilink")
  - [吸血鬼](../Page/吸血鬼.md "wikilink")

## 相关作品

  - 《[歷史學家](../Page/歷史學家_\(小說\).md "wikilink")》（小說）

  - 《[不死殭屍—恐慄交響曲](../Page/不死殭屍—恐慄交響曲.md "wikilink")》

  - 《[诺斯费拉图：夜晚的幽灵](../Page/诺斯费拉图：夜晚的幽灵.md "wikilink")》

  - 《[怪物小王子](../Page/怪物小王子.md "wikilink")》

  - 《[Hellsing](../Page/Hellsing.md "wikilink")》

  - 《[吸血鬼德古拉的真实](../Page/吸血鬼德古拉的真实.md "wikilink")》（著者：
    [阿莱桑德拉](../Page/阿莱桑德拉.md "wikilink")．[毕塞亚](../Page/毕塞亚.md "wikilink")，国际书号：ISBN9789861773841，原书名：Dracula:
    Viaggio in Transilvania ）

  - [达丘拉
    (电影)](../Page/达丘拉_\(电影\).md "wikilink")（（Dracula）（1931），主演：[贝拉·路高西](../Page/贝拉·路高西.md "wikilink")（Bela
    Logosi） ）

  -
  - [德古拉 (漫威漫畫)](../Page/德古拉_\(漫威漫畫\).md "wikilink")

  - [怪鸭历险记](../Page/怪鸭历险记.md "wikilink")

  - [德古拉](../Page/德古拉_\(1931年電影\).md "wikilink")：1931年美國電影*Dracula*，

  - [德古拉](../Page/德古拉_\(1958年電影\).md "wikilink")：1958年英國電影*Dracula*，[克里斯多福·李主演](../Page/克里斯多福·李.md "wikilink")

  - \-{zh-hans:[吸血殭屍：驚情四百年](../Page/吸血殭屍：驚情四百年.md "wikilink");
    zh-hant:[吸血鬼：真愛不死](../Page/吸血鬼：真愛不死.md "wikilink");}-：1992年美國電影；[蓋瑞．歐德曼主演](../Page/蓋瑞．歐德曼.md "wikilink")

  - [德古拉2000](../Page/德古拉2000.md "wikilink")：2000年美國電影*Dracula 2000*

  - [德古拉3D](../Page/德古拉3D_\(2012年電影\).md "wikilink")：2012年西班牙電影*Draculla
    3D*，[托馬斯·克萊舒曼主演](../Page/托馬斯·克萊舒曼.md "wikilink")

  - 德库拉2 阿森松

  - 德库拉3复兴

  - 德库拉的恶梦

<!-- end list -->

  -
    [鬼太郎](../Page/鬼太郎.md "wikilink")

<!-- end list -->

  - [惡魔城](../Page/惡魔城.md "wikilink")（電子遊戲系列）

<!-- end list -->

  -
    [变身忍者岚](../Page/变身忍者岚.md "wikilink")

<!-- end list -->

  - [尖叫旅社](../Page/尖叫旅社.md "wikilink")
  - [尖叫旅社2](../Page/尖叫旅社2.md "wikilink")
  - [Fate/Apocrypha](../Page/Fate/Apocrypha.md "wikilink")
  - [Fate/EXTRA](../Page/Fate/EXTRA.md "wikilink")
  - [Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink")
  - [凡赫辛](../Page/凡赫辛_\(電影\).md "wikilink")：2004年美國電影*Van
    Helsing*，[理查·羅森堡主演](../Page/理查·羅森堡.md "wikilink")
  - [德古拉：永咒傳奇](../Page/德古拉：永咒傳奇.md "wikilink")：2014年美國電影*Dracula
    Untold*，[路克·伊凡斯主演](../Page/路克·伊凡斯.md "wikilink")
  - [王牌對決](../Page/王牌對決.md "wikilink")（LostSaga）的可使用稀有英雄
  - [手里剑战队忍忍者](../Page/手里剑战队忍忍者.md "wikilink")

## 参考文献

## 外部連結

  -
  - [Bram Stoker
    Online](http://www.bramstoker.org/novels/05dracula.html) *Dracula*.

[Category:文学角色](../Category/文学角色.md "wikilink")
[Category:虚构吸血鬼](../Category/虚构吸血鬼.md "wikilink")
[德古拉](../Category/德古拉.md "wikilink")
[Category:虛構鍊金術師](../Category/虛構鍊金術師.md "wikilink")
[Category:虛構天氣能力者](../Category/虛構天氣能力者.md "wikilink")