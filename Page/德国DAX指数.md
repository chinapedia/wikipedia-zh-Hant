|                                                                     |
| :-----------------------------------------------------------------: |
|                             **德國DAX指數**                             |
|                                ****                                 |
| [DAX.png](https://zh.wikipedia.org/wiki/File:DAX.png "fig:DAX.png") |
|                                 概要                                  |
|                              **發行日期**                               |
|           **[識別代碼](../Page/:en:ISO_10383.md "wikilink")**           |
|            **[FX識別代碼](../Page/金融資訊交換協定.md "wikilink")**             |
|                                交易所資訊                                |
|                              **交易所全銜**                              |
|        **[交易所BIC識別代碼](../Page/:en:ISO_9362.md "wikilink")**         |

**德国DAX指数**
（，最早来源于）是[德国重要的](../Page/德国.md "wikilink")[股票指数](../Page/股票指数.md "wikilink")。由德意志交易所集团（Deutsche
Börse
Group）推出的一个[蓝筹股指数](../Page/蓝筹股.md "wikilink")。该指数中包含有30家主要的德国公司。DAX指数在欧洲與[巴黎CAC40指數合稱為歐洲兩大主要股市](../Page/巴黎CAC40指數.md "wikilink")，也是世界证券市场中的重要指数之一。

该指数通过[Xetra交易系统进行交易](../Page/Xetra交易系统.md "wikilink")，因此其交易方式不同于传统的公开交易方式，而是采用电子交易的方式，便于进行全球交易。

DAX指数于1987年推出，以取代当时的Börsen-Zeitung指数和[法兰克福汇报指数](../Page/法兰克福汇报指数.md "wikilink")（Frankfurter
Allgemeinen Zeitung
Aktienindex）。1988年7月1日起开始正式交易，基准点为1000点。指数以“整体回报法”进行计算，即在考虑公司股价的同时，考虑预期的股息回报。但因为该指数只包含德国最赚钱的公司，因此有人批评无法准确反应德国经济。
[Boerse_Frankfurt_inside.jpg](https://zh.wikipedia.org/wiki/File:Boerse_Frankfurt_inside.jpg "fig:Boerse_Frankfurt_inside.jpg")证券交易所内显示当日DAX指数走势的显示牌\]\]

## 其它DAX指数:

1994年建立了称作的指数，它将作为记录市场中100支价值成长性、流动性最佳股票的指数。1996年引入MDAX，它由30支DAX成分股和70支MDAX成分股组合而成。通过将MDAX缩编至50支成分股，将原有70支MDAX成份股与30支DAX成份股组合成为[HDAX](http://de.wikipedia.org/wiki/HDAX)（取代DAX
100）。[CDAX](http://de.wikipedia.org/wiki/CDAX)是由所有在法兰克福交易所交易的股票的集合而成。[SDAX](http://de.wikipedia.org/wiki/SDAX)是集合50支小股票动向的指数。

## DAX指数的组成公司

[Dax-Unternehmen_Maerz_2009_english.png](https://zh.wikipedia.org/wiki/File:Dax-Unternehmen_Maerz_2009_english.png "fig:Dax-Unternehmen_Maerz_2009_english.png")
以下是DAX指数的组成公司(截止2010年7月15日)：\[1\]

  - [阿迪达斯](../Page/阿迪达斯.md "wikilink")（Adidas AG，体育用品）
  - [安联](../Page/安联.md "wikilink")（Allianz SE，保险）
  - [巴斯夫](../Page/巴斯夫.md "wikilink")（BASF SE，化工）
  - [拜耳](../Page/拜耳.md "wikilink")（Bayer AG，医药）
  - [拜尔斯道夫](../Page/拜尔斯道夫.md "wikilink")（Beiersdorf AG，日化）
  - [宝马](../Page/宝马.md "wikilink")（Bayerische Motoren Werke AG，汽车制造）
  - [德国商业银行](../Page/德国商业银行.md "wikilink")（Commerzbank AG，金融）
  - [戴姆勒股份有限公司](../Page/戴姆勒股份有限公司.md "wikilink")（Daimler AG，汽车制造）
  - [德意志银行](../Page/德意志银行.md "wikilink")（Deutsche Bank AG，金融）
  - [德国证券交易所](../Page/德国证券交易所.md "wikilink")（Deutsche Börse AG，金融）
  - [德国邮政](../Page/德国邮政.md "wikilink")（Deutsche Post AG，物流）
  - [德国电信](../Page/德国电信.md "wikilink")（Deutsche Telekom AG，电信）
  - [E.ON](../Page/E.ON.md "wikilink")（E.ON AG能源）
  - [费森尤斯医疗](../Page/费森尤斯医疗.md "wikilink")（Fresenius Medical Care AG &
    Co. KGaA，肾透析产品和服务,Fresenius SE的子公司）
  - [费森尤斯集团](../Page/费森尤斯集团.md "wikilink")（Fresenius SE，健康，医院）
  - [汉高](../Page/汉高.md "wikilink")（Henkel，化工）
  - [英飞凌](../Page/英飞凌.md "wikilink")（Infineon Technologies AG，半导体制造）
  - [K+S](../Page/K+S.md "wikilink")（K+S Aktiengesellschaft采矿业）
  - [林德](../Page/林德集团.md "wikilink")（Linde AG，化工）
  - [汉莎航空](../Page/汉莎航空.md "wikilink")（Lufthansa AG，航空运输）
  - [MAN](../Page/奥格斯堡-纽伦堡机械制造厂.md "wikilink")（Man SE,汽车及机械制造）
  - [默克合资股份公司](../Page/默克合资股份公司.md "wikilink")（Merck KGaA，化工制药）
  - [麦德龙](../Page/麦德龙.md "wikilink")（Metro AG，商品零售）
  - [慕尼黑再保险](../Page/慕尼黑再保险.md "wikilink")（ Münchener
    Rückversicherungs-Ges. AG，保险）
  - [RWE](../Page/RWE.md "wikilink")（RWE AG，能源）
  - [海德堡水泥](../Page/海德堡水泥.md "wikilink")（HeidelbergCement AG，水泥、骨料）
  - [SAP公司](../Page/SAP公司.md "wikilink")（SAP AG，企业管理软件与解决方案供应商）
  - [西门子](../Page/西门子.md "wikilink")（Siemens AG，电器制造）
  - [蒂森克虏伯](../Page/蒂森克虏伯.md "wikilink")（ThyssenKrupp AG，钢铁、建筑及制造业）
  - [大众汽车](../Page/大众汽车.md "wikilink")（Volkswagen AG，汽车制造）

## DAX指数和道琼斯工业指数走势对比

以上是自1959年至2004年3月7日DAX指数与道琼斯工业指数（Dow Jones Industrial
Average）的走势对比。1988年以前的走势为DAX指数的前身——Börsen-Zeitung指数的走势。1988年DAX指数正式开盘交易时，基准指数为1959年基准指数内的1.163倍，而2004年3月7日的收盘指数为1959年基准指数的8.136倍。

## 參考資料

<div class="references-small">

<references />

</div>

## 参见

  - [证券指数](../Page/证券指数.md "wikilink")

## 外部链接

  - [DAX指数官方网站](https://web.archive.org/web/20070929111611/http://deutsche-boerse.com/dbag/dispatch/en/isg/gdb_navigation/private_investors/20_Equities/20_Indices/10_DAX?module=InOverview_Index&wp=DE0008469008&wplist=DE0008469008&foldertype=_Index)

[Category:欧洲股市指数](../Category/欧洲股市指数.md "wikilink")
[Category:德国经济](../Category/德国经济.md "wikilink")

1.  来自德国电视一台证券网页[1](http://ard.gedif.de/ard/kurse_listen.htm?sektion=dax)