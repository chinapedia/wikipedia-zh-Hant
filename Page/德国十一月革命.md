**德國十一月革命**（），又称“**德国1918年－1919年革命**”，是[德国在](../Page/德国.md "wikilink")[第一次世界大战](../Page/第一次世界大战.md "wikilink")1918年与1919年发生的一连串事件，致使[德意志帝國被推翻以及](../Page/德意志帝國.md "wikilink")[魏玛共和国的建立](../Page/魏玛共和国.md "wikilink")。与[俄罗斯的](../Page/俄罗斯.md "wikilink")[二月革命类似](../Page/俄國二月革命.md "wikilink")，德国革命并非由一个[政党领导](../Page/政党.md "wikilink")。同时，类似[苏维埃的工人团体亦希望夺权](../Page/苏维埃.md "wikilink")，然而此类事件使得[左派分子更分裂](../Page/左派.md "wikilink")，如[社会民主党领导的共和政府曾利用](../Page/德國社會民主黨.md "wikilink")[右翼的](../Page/右翼.md "wikilink")[自由军团来镇压同为](../Page/自由军团.md "wikilink")[左翼的](../Page/左翼.md "wikilink")[斯巴達克同盟](../Page/斯巴達克同盟.md "wikilink")。

正如俄罗斯的[大革命一般](../Page/1917年俄国革命.md "wikilink")，[第一次世界大战导致的灾难引發了德国革命](../Page/第一次世界大战.md "wikilink")。德军统帅[埃里希·魯登道夫想领导军队在](../Page/埃里希·魯登道夫.md "wikilink")1918年向盟军投降，結果引起政治危机。威廉二世退位後，虽然[巴登親王任命社会民主党代表领导临时政府](../Page/馬克斯·馮·巴登.md "wikilink")，但仍未能阻止叛乱继续。

1918年10月29日至11月3日，[基尔港首先发生起义](../Page/基尔.md "wikilink")：四万名海员及船只认为德国在战争中大势已去，出战只是自取灭亡，於是抗拒海军的出兵命令。11月8日时，工人与士兵的议会已占领了德国西部的大部分，为“议会共和”作准备。11月9日，威廉二世被迫退位，德意志帝国灭亡。纵然如此，仍有不少上層或中产的分子支持帝制。11月11日，德国宣布[无条件投降](../Page/无条件投降.md "wikilink")，第一次世界大战结束。

社会民主党跃升成为领导德国的政党，与激进的[德國独立社會民主黨共同执政](../Page/德國独立社會民主黨.md "wikilink")。不过，独立社民黨认为社民黨想保持[资本主义在德国的现状](../Page/资本主义.md "wikilink")，便在1918年12月脱离联合政府。其后，1919年1月，[斯巴達克同盟发动第二股革命浪潮](../Page/斯巴達克同盟.md "wikilink")，让它横扫德国。社民党主席[弗里德里希·艾伯特聘用](../Page/弗里德里希·艾伯特.md "wikilink")[自由军团镇压起义](../Page/自由军团.md "wikilink")。1月15日，斯巴達克團两位领导人[卡爾·李卜克內西与](../Page/卡爾·李卜克內西.md "wikilink")[羅莎·盧森堡惨遭杀害](../Page/羅莎·盧森堡.md "wikilink")。4月，[巴伐利亚苏维埃共和国成立](../Page/巴伐利亚苏维埃共和国.md "wikilink")，形成革命高潮。5月，巴伐利亚苏维埃共和国遭社民党政府血腥镇压。

德国革命促成了[魏玛共和国的成立](../Page/魏玛共和国.md "wikilink")，同时又促使民族主义的[纳粹党崛起](../Page/纳粹党.md "wikilink")。

## 參考資料

<small>

  - [Max von Baden](../Page/Max_von_Baden.md "wikilink"): *Erinnerungen
    und Dokumente*, Berlin u. Leipzig 1927
  - [Eduard Bernstein](../Page/Eduard_Bernstein.md "wikilink"): *Die
    deutsche Revolution von 1918/19. Geschichte der Entstehung und
    ersten Arbeitsperiode der deutschen Republik. Herausgegeben und
    eingeleitet von Heinrich August Winkler und annotiert von Teresa
    Löwe.* Bonn 1998, ISBN 3801202720
  - [Pierre Broué](../Page/Pierre_Broué.md "wikilink"): *Die Deutsche
    Revolution 1918-1923*, in: *Aufstand der Vernunft Nr. 3.* Hrsg.: Der
    Funke e.V., Eigenverlag, Wien 2005
  - [Alfred Döblin](../Page/Alfred_Döblin.md "wikilink"): *November
    1918. Eine deutsche Revolution*, Roman in vier Bänden, München 1978,
    ISBN 3423013893
  - [Bernt Engelmann](../Page/Bernt_Engelmann.md "wikilink"): *Wir
    Untertanen* und *Einig gegen Recht und Freiheit - Ein Deutsches
    Anti-Geschichtsbuch.* Frankfurt 1982 und 1981, ISBN 359621680x, ISBN
    3596218381
  - [Sebastian Haffner](../Page/Sebastian_Haffner.md "wikilink"): *Die
    deutsche Revolution 1918/1919.* München 1979 (u. a. ISBN
    349961622X); auch veröffentlicht unter dem Titel *Der Verrat*,
    Berlin 2002, ISBN 393027800
  - Institut für Marxismus-Leninismus beim ZK der SED (Hg.):
    *Illustrierte Geschichte der deutschen Novemberrevolution
    1918/1919*. Berlin: Dietz Verlag, 1978 (o. ISBN, Großformat, mit
    umfangreichem Bildmaterial)
  - [Wilhelm Keil](../Page/Wilhelm_Keil.md "wikilink"): *Erlebnisse
    eines Sozialdemokraten.* Zweiter Band, Stuttgart 1948
  - [Harry Graf Kessler](../Page/Harry_Graf_Kessler.md "wikilink"):
    *Tagebücher 1918 bis 1937.* Frankfurt am Main 1982
  - Ulrich Kluge: *Soldatenräte und Revolution. Studien zur
    Militärpolitik in Deutschland 1918/19.* Göttingen 1975, ISBN
    3525359659
  - derselbe: *Die deutsche Revolution 1918/1919.* Frankfurt am Main
    1985, ISBN 3518112627
  - Eberhard Kolb: *Die Weimarer Republik.* München 2002, ISBN
    3486497960
  - Ottokar Luban: *Die ratlose Rosa. Die KPD-Führung im Berliner
    Januaraufstand 1919. Legende und Wirklichkeit.* Hamburg 2001, ISBN
    387975960X
  - Erich Matthias (Hrsg.): *Die Regierung der Volksbeauftragten
    1918/19.* 2 Bände, Düsseldorf 1969 (Quellenedition)
  - Wolfgang Michalka u. Gottfried Niedhart (Hg.): *Deutsche Geschichte
    1918-1933. Dokumente zur Innen- und Außenpolitik*, Frankfurt am Main
    1992 ISBN 3-596-11250-8
  - [Hans Mommsen](../Page/Hans_Mommsen.md "wikilink"): *Die verspielte
    Freiheit. Der Weg der Republik von Weimar in den Untergang 1918 bis
    1933.* Berlin 1989, ISBN 3548331416
  - [Carl von Ossietzky](../Page/Carl_von_Ossietzky.md "wikilink"): *Ein
    Lesebuch für unsere Zeit.* Aufbau-Verlag Berlin-Weimar 1989
  - Detlef J.K. Peukert: *Die Weimarer Republik. Krisenjahre der
    klassischen Moderne.* Frankfurt am Main 1987, ISBN 3518112821
  - Gerhard A. Ritter/Susanne Miller (Hg.): *Die deutsche Revolution
    1918-1919. Dokumente.* 2. erheblich erweiterte und überarbeitete
    Auflage, Frankfurt am Main 1983, ISBN 3596243009
  - [Arthur Rosenberg](../Page/Arthur_Rosenberg.md "wikilink"):
    *Geschichte der Weimarer Republik.* Frankfurt am Main 1961
    (Erstausgabe: Karlsbad 1935), ISBN 3434000038 \[zeitgenössische
    Deutung\]
  - [Hagen Schulze](../Page/Hagen_Schulze.md "wikilink"): *Weimar.
    Deutschland 1917-1933*, Berlin 1982
  - [Kurt Sontheimer](../Page/Kurt_Sontheimer.md "wikilink"):
    *Antidemokratisches Denken in der Weimarer Republik. Die politischen
    Ideen des deutschen Nationalismus zwischen 1918 und 1933*, München
    1962
  - [Kurt Tucholsky](../Page/Kurt_Tucholsky.md "wikilink"): *Gesammelte
    Werke in 10 Bänden*, hg. von Mary Gerold-Tucholsky und Fritz J.
    Raddatz, Reinbek 1975, ISBN 3-499-29012-x
  - Volker Ullrich: *Die nervöse Großmacht. Aufstieg und Untergang des
    deutschen Kaisserreichs 1871-1918*, FRankfurt am Main 1997 ISBN
    3-10-086001-2
  - Richard Wiegand: *"Wer hat uns verraten ..." - Die Sozialdemokratie
    in der Novemberrevolution.* Neuauflage: Ahriman-Verlag, Freiburg
    i.Br 2001, ISBN 389484812X
  - [Heinrich August
    Winkler](../Page/Heinrich_August_Winkler.md "wikilink"): *Weimar
    1918-1933.* München 1993
  - derselbe: *Deutschland vor Hitler.* In: *Der historische Ort des
    Nationalsozialismus*, Fischer TB 4445

</small>

### 地域性的資料

<small>

  - Peter Berger: *Brunonia mit rotem Halstuch. Novemberrevolution in
    Braunschweig 1918/19*, Hannover 1979
  - Peter Brandt/Reinhard Rürup: *Volksbewegung und demokratische
    Neuordnung in Baden 1918/19. Zur Vorgeschichte und Geschichte der
    Revolution*, Sigmaringen 1991
  - Günter Cordes: *Das Revolutionsjahr 1918/19 in Württemberg und die
    Ereignisse in Ludwigsburg.* (Ludwigsburger Geschichtsblätter, Heft
    32), Ludwigsburg 1980
  - Holger Frerichs: *Von der Monarchie zur Republik - Der politische
    Umbruch in Varel, der Friesischen Wehde und in Jade / Schweiburg
    1918/19.* Varel 2001, ISBN 3934606083
  - Gustav Füllner: *Das Ende der Spartakisten-Herrschaft in
    Braunschweig. Einsatz der Regierungstruppen unter General Maerker
    vor 50 Jahren.* In: Braunschweigisches Jahrbuch Nr. 50, Braunschweig
    1969
  - Wolfgang Günther: *Die Revolution von 1918/19 in Oldenburg.*
    Oldenburg 1979
  - Eberhard Kolb und Klaus Schönhoven: *Regionale und Lokale
    Räteorganisationen in Württemberg 1918/19.* Düsseldorf 1976, ISBN
    3770050843
  - Klaus Schönhoven: *Die württembergischen Soldatenräte in der
    Revolution von 1918/19.* (Zeitschrift für Württembergische
    Landesgeschichte, Jg. 33, 1974), Stuttgart 1976

</small>

## 外部链接

  - [关于德国革命的三篇文章](https://www.webcitation.org/query?id=1256578127112373&url=www.geocities.com/Athens/Acropolis/8195/ger_int.htm)
    由[委员会共产主义者编写](../Page/委员会共产主义.md "wikilink")
  - [关于德国革命的资料](http://www.marxists.org/subject/germany-1918-23/)
    在marxists.org

[br:Reveulzi spartakour](../Page/br:Reveulzi_spartakour.md "wikilink")

[Category:第一次世界大战](../Category/第一次世界大战.md "wikilink")
[Category:德國歷史](../Category/德國歷史.md "wikilink")
[Category:革命](../Category/革命.md "wikilink")
[Category:1918年](../Category/1918年.md "wikilink")