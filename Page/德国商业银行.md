**德國商業银行**（）成立于1870年，[德國原三大](../Page/德國.md "wikilink")[银行之一](../Page/银行.md "wikilink")，2009年底資產總额约為8441.03億[歐元](../Page/歐元.md "wikilink")\[1\]。總部[商業銀行大廈現時設在](../Page/商業銀行大廈.md "wikilink")[法蘭克福](../Page/法蘭克福.md "wikilink")，为德国股票指数[DAX的成分公司之一](../Page/DAX.md "wikilink")。就资产规模来看，该银行是全德国第二大银行，仅次于[德意志银行](../Page/德意志银行.md "wikilink")。专业地产银行Eurohypo是该行的下属公司。由于最近洗钱事件和对客户傲慢的态度，目前德国商业银行深陷财务危机，从2016年开始，计划裁员10000人。

## 历史

德国商业银行1870年由一批商人成立于[汉堡市](../Page/漢堡.md "wikilink")，1905年和柏林人银行合并后迁到[柏林](../Page/柏林.md "wikilink")，[二战之后一度迁居](../Page/二战.md "wikilink")[杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")，1958年迁移至法兰克福。2008年8月，[雷曼兄弟破产](../Page/雷曼兄弟.md "wikilink")——金融危机全面爆发前一个月，德国商業银行宣佈以98亿欧元收購欧洲顶尖投资银行之一[德累斯頓銀行全部股份](../Page/德累斯頓銀行.md "wikilink")，使得新德商受到并购和[金融危机的双重影响](../Page/金融危机.md "wikilink")。两家银行合并之后，新德国商业银行在客户数量和国内网点数量上均超过了德意志银行。合并后，德累斯顿银行的[投资银行部经过缩减后](../Page/投资银行.md "wikilink")，与德国商业银行的相关部门整合，合并组成新的投资银行部门Corporates
&
Markets，并在世界各大[金融中心如](../Page/金融中心.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")，[纽约](../Page/纽约.md "wikilink")，[香港](../Page/香港.md "wikilink")，[东京](../Page/东京.md "wikilink")，[新加坡等设有办公室](../Page/新加坡.md "wikilink")。

德国商业银行自1993年设立[上海分行](../Page/上海.md "wikilink")，并在2000年就成为第一批获得全面人民币业务牌照的[外资银行](../Page/外资银行.md "wikilink")。现上海分行办公地点设在[陆家嘴](../Page/陆家嘴.md "wikilink")[环球金融中心](../Page/环球金融中心.md "wikilink")，并在大陆设有[北京和](../Page/北京.md "wikilink")[天津分行](../Page/天津.md "wikilink")。另外在香港和新加坡保留了庞大的投资银行队伍。

## 另见

  - [德国商业银行大厦](../Page/德国商业银行大厦.md "wikilink")
  - [德国商业银行竞技场](../Page/德国商业银行竞技场.md "wikilink")

## 参考资料

## 外部链接

  -
  -
[Category:德国银行](../Category/德国银行.md "wikilink")
[Category:德国品牌](../Category/德国品牌.md "wikilink")
[Category:1870年成立的银行](../Category/1870年成立的银行.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")

1.  [经审计的财务报表](https://www.commerzbank.de/en/hauptnavigation/aktionaere/konzern_/unternehmensberichtserstattung.html)