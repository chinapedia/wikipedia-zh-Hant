**德国商業銀行競技場**（，球場原名**瓦爾德球場**，，意即“森林”球場）是位於[德國](../Page/德國.md "wikilink")[法蘭克福的](../Page/法蘭克福.md "wikilink")52,000座運動場，主要供足球及美式足球比賽用，為安德烈法蘭克福-{zh-hans:足球俱乐部;
zh-hant:足球會;}-及[-{zh-hans:法兰克福银河美式足球俱乐部;
zh-hant:法蘭克福銀河美式足球會;}-](../Page/:en:Frankfurt_Galaxy.md "wikilink")（Frankfurt
Galaxy）的主場球場。於1925年揭幕，在[二次大戰期間亦安排作政治集會用途](../Page/第二次世界大戰.md "wikilink")。

在2005年7月更名商業銀行競技場），但在[2006年世界盃期間將稱為](../Page/2006年世界盃足球賽.md "wikilink")“法蘭克福世界杯球場”（）。為舉行世界盃幾乎將整個球場重新建造及現代化，安排上演四場分組初賽及一場半準決賽。[2005年的](../Page/2005年聯合會盃.md "wikilink")[洲際國家盃亦安排三場分組初賽及由](../Page/国际足联联合会杯足球赛.md "wikilink")[巴西對](../Page/巴西國家足球隊.md "wikilink")[阿根廷的決賽在這裡上演](../Page/阿根廷國家足球隊.md "wikilink")。

2008年9月12日原定晚上由[法蘭克福主场对](../Page/法兰克福足球俱乐部.md "wikilink")[-{zh-hans:卡尔斯鲁厄;zh-hk:卡爾斯魯厄}-的](../Page/卡尔斯鲁厄体育俱乐部.md "wikilink")[德甲第](../Page/德甲.md "wikilink")4轮比赛，因之前三天[麥當娜在此舉行](../Page/麥當娜.md "wikilink")[演唱會](../Page/演唱會.md "wikilink")，造成草皮严重受损，球场在當天才铺上新草皮，[德国足球职业联盟](../Page/德国足球职业联盟.md "wikilink")（DFL）总经理希罗尼穆斯与本场比赛主裁判金赫费尔在中午视察过草皮情况后，作出了暂时取消比赛的决定<small>\[1\]</small>。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [BBC世界盃球場：法蘭克福](http://news.bbc.co.uk/sport1/hi/football/world_cup_2006/venues/4459002.stm)

[Category:德國足球場](../Category/德國足球場.md "wikilink")
[Category:美式足球場](../Category/美式足球場.md "wikilink")
[Category:1974年世界盃足球場](../Category/1974年世界盃足球場.md "wikilink")
[Category:2006年世界盃足球場](../Category/2006年世界盃足球場.md "wikilink")

1.  [麦当娜个唱破坏草皮 周五德甲比赛改期](http://www.dfo.cn/dfo/show.asp?id=8309)