根据1887年国际统计会议的定义，“大型城市”为人口超过100,000的城市，“中型城市”为人口在20,000至100,000之间的城市，“小型城市”为人口在20,000以下的城市。以下所列为人口超过100,000的**德国大型城市**和人口在20,000至100,000之间的**德国中型城市**（截至2006年底）(括弧內為2010年人口數)：

[Berlin_Mitte_by_night.JPG](https://zh.wikipedia.org/wiki/File:Berlin_Mitte_by_night.JPG "fig:Berlin_Mitte_by_night.JPG")\]\]
[HP_L4225.JPG](https://zh.wikipedia.org/wiki/File:HP_L4225.JPG "fig:HP_L4225.JPG")\]\]
[Munich_skyline.jpg](https://zh.wikipedia.org/wiki/File:Munich_skyline.jpg "fig:Munich_skyline.jpg")\]\]
[Kdom.jpg](https://zh.wikipedia.org/wiki/File:Kdom.jpg "fig:Kdom.jpg")\]\]
[Bankenviertel_Frankfurt.jpg](https://zh.wikipedia.org/wiki/File:Bankenviertel_Frankfurt.jpg "fig:Bankenviertel_Frankfurt.jpg")\]\]
[Blick_vom_Max-Kade-Haus_Stuttgart_2007_(Alter_Fritz)_45.JPG](https://zh.wikipedia.org/wiki/File:Blick_vom_Max-Kade-Haus_Stuttgart_2007_\(Alter_Fritz\)_45.JPG "fig:Blick_vom_Max-Kade-Haus_Stuttgart_2007_(Alter_Fritz)_45.JPG")\]\]
[Medienhafen_Duesseldorf_Nacht.jpg](https://zh.wikipedia.org/wiki/File:Medienhafen_Duesseldorf_Nacht.jpg "fig:Medienhafen_Duesseldorf_Nacht.jpg")\]\]
[Dortmund_Panorama.jpg](https://zh.wikipedia.org/wiki/File:Dortmund_Panorama.jpg "fig:Dortmund_Panorama.jpg")\]\]
[Essen-Südviertel_Luft.jpg](https://zh.wikipedia.org/wiki/File:Essen-Südviertel_Luft.jpg "fig:Essen-Südviertel_Luft.jpg")\]\]
[Weserhb2.jpg](https://zh.wikipedia.org/wiki/File:Weserhb2.jpg "fig:Weserhb2.jpg")\]\]
[Dresden_elbe_nacht_zunehmender_mond.JPG](https://zh.wikipedia.org/wiki/File:Dresden_elbe_nacht_zunehmender_mond.JPG "fig:Dresden_elbe_nacht_zunehmender_mond.JPG")\]\]
[AUGUSTUSPLATZ-014.jpg](https://zh.wikipedia.org/wiki/File:AUGUSTUSPLATZ-014.jpg "fig:AUGUSTUSPLATZ-014.jpg")\]\]
[Hannover_Skyline.jpg](https://zh.wikipedia.org/wiki/File:Hannover_Skyline.jpg "fig:Hannover_Skyline.jpg")\]\]
[Nürnberg_panorama.jpg](https://zh.wikipedia.org/wiki/File:Nürnberg_panorama.jpg "fig:Nürnberg_panorama.jpg")\]\]
[2917_Duisburg.JPG](https://zh.wikipedia.org/wiki/File:2917_Duisburg.JPG "fig:2917_Duisburg.JPG")\]\]

## 1,000,000人以上

1.  [柏林](../Page/柏林.md "wikilink")（Berlin）（**3,460,725**）
2.  [汉堡](../Page/汉堡市.md "wikilink")（Hamburg）（**1,786,448**）
3.  [慕尼黑](../Page/慕尼黑.md "wikilink")（München）（**1,353,186**）
4.  [科隆](../Page/科隆.md "wikilink")（Köln）（**1,007,119**）

## 500,000人-1,000,000人

5.  [美因河畔法兰克福](../Page/美因河畔法兰克福.md "wikilink")（Frankfurt am
    Main）（**679,664**）
6.  [斯图加特](../Page/斯图加特.md "wikilink")（Stuttgart）（**606,588**）
7.  [杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")（Düsseldorf）（**588,735**）
8.  [多特蒙德](../Page/多特蒙德.md "wikilink")（Dortmund）（**580,444**）
9.  [埃森](../Page/埃森.md "wikilink")（Essen）（**574,635**）
10. [不来梅](../Page/不来梅.md "wikilink")（Bremen）（**547,340**）
11. [德累斯顿](../Page/德累斯顿.md "wikilink")（Dresden）（**523,058**）
12. [莱比锡](../Page/莱比锡.md "wikilink")（Leipzig）（**522,883**）
13. [汉诺威](../Page/汉诺威.md "wikilink")（Hannover）（**522,686**）
14. [纽伦堡](../Page/纽伦堡.md "wikilink")（Nürnberg）（**505,664**）

## 200,000人-500,000人

15. [杜伊斯堡](../Page/杜伊斯堡.md "wikilink")（Duisburg） （**489,599**）
16. [波鸿](../Page/波鸿.md "wikilink")（Bochum） （**374,737**）
17. [伍珀塔尔](../Page/伍珀塔尔.md "wikilink")（Wuppertal） （**349,721**）
18. [波恩](../Page/波恩.md "wikilink")（Bonn） （**324,899**）
19. [比勒费尔德](../Page/比勒费尔德.md "wikilink")（Bielefeld） （**323,270**）
20. [曼海姆](../Page/曼海姆.md "wikilink")（Mannheim）（**313,174**）
21. [卡尔斯鲁厄](../Page/卡尔斯鲁厄.md "wikilink")（Karlsruhe） （**294,761**）
22. [明斯特](../Page/明斯特.md "wikilink")（Münster） （**279,803**）
23. [威斯巴登](../Page/威斯巴登.md "wikilink")（Wiesbaden） （**275,976**）
24. [奥格斯堡](../Page/奥格斯堡.md "wikilink")（Augsburg） （**264,708**）
25. [亚琛](../Page/亚琛.md "wikilink")（Aachen） （**258,664**）
26. [门兴格拉德巴赫](../Page/门兴格拉德巴赫.md "wikilink")（Mönchengladbach）
    （**257,993**）
27. [盖尔森基兴](../Page/盖尔森基兴.md "wikilink")（Gelsenkirchen） （**257,981**）
28. [不伦瑞克](../Page/不伦瑞克.md "wikilink")（Braunschweig） （**248,867**）
29. [开姆尼茨](../Page/开姆尼茨.md "wikilink")（Chemnitz） （**243,248**）
30. [基尔](../Page/基尔.md "wikilink")（Kiel） （**239,526**）
31. [克雷费尔德](../Page/克雷费尔德.md "wikilink")（Krefeld） （**235,076**）
32. [哈雷](../Page/哈雷_\(德国\).md "wikilink")（Halle） （**232,963**）
33. [马格德堡](../Page/马格德堡.md "wikilink")（Magdeburg） （**231,549**）
34. [弗赖堡](../Page/弗赖堡.md "wikilink")（Freiburg）（**224,191**）
35. [奥伯豪森](../Page/奥伯豪森.md "wikilink")（Oberhausen） （**212,945**）
36. [吕贝克](../Page/吕贝克.md "wikilink")（Lübeck） （**210,232**）
37. [埃尔福特](../Page/埃尔福特.md "wikilink")（Erfurt） （**204,994**）
38. [罗斯托克](../Page/罗斯托克.md "wikilink")（Rostock） （**202,735**）

## 100,000人-200,000人

39. [美因茨](../Page/美因茨.md "wikilink")（Mainz）
40. [哈根](../Page/哈根.md "wikilink")（Hagen）
41. [卡塞尔](../Page/卡塞尔.md "wikilink")（Kassel）
42. [哈姆](../Page/哈姆.md "wikilink")（Hamm）
43. [萨尔布吕肯](../Page/萨尔布吕肯.md "wikilink")（Saarbrücken）
44. [黑尔讷](../Page/黑尔讷.md "wikilink")（Herne）
45. [米尔海姆](../Page/米尔海姆.md "wikilink")（Mülheim）
46. [路德维希港](../Page/路德维希港.md "wikilink")（Ludwigshafen）
47. [奥斯纳布吕克](../Page/奥斯纳布吕克.md "wikilink")（Osnabrück）
48. [索林根](../Page/索林根.md "wikilink")（Solingen）
49. [勒沃库森](../Page/勒沃库森.md "wikilink")（Leverkusen）
50. [奥尔登堡](../Page/奥尔登堡.md "wikilink")（Oldenburg）
51. [诺伊斯](../Page/诺伊斯.md "wikilink")（Neuss）
52. [波茨坦](../Page/波茨坦.md "wikilink")（Potsdam）
53. [海德堡](../Page/海德堡.md "wikilink")（Heidelberg）
54. [帕德博恩](../Page/帕德博恩.md "wikilink")（Paderborn）
55. [达姆施塔特](../Page/达姆施塔特.md "wikilink")（Darmstadt）
56. [维尔茨堡](../Page/维尔茨堡.md "wikilink")（Würzburg）
57. [雷根斯堡](../Page/雷根斯堡.md "wikilink")（Regensburg）
58. [因戈尔施塔特](../Page/因戈尔施塔特.md "wikilink")（Ingolstadt）
59. [格丁根](../Page/格丁根.md "wikilink")（Göttingen）
60. [雷克林豪森](../Page/雷克林豪森.md "wikilink")（Recklinghausen）
61. [海尔布隆](../Page/海尔布隆.md "wikilink")（Heilbronn）
62. [乌尔姆](../Page/乌尔姆.md "wikilink")（Ulm）
63. [沃尔夫斯堡](../Page/沃尔夫斯堡.md "wikilink")（Wolfsburg）
64. [普福尔茨海姆](../Page/普福尔茨海姆.md "wikilink")（Pforzheim）
65. [博特罗普](../Page/博特罗普.md "wikilink")（Bottrop）
66. [美因河畔奥芬巴赫](../Page/美因河畔奥芬巴赫.md "wikilink")（Offenbach am Main）
67. [不来梅哈芬](../Page/不来梅哈芬.md "wikilink")（Bremerhaven）
68. [雷姆沙伊德](../Page/雷姆沙伊德.md "wikilink")（Remscheid）
69. [菲尔特](../Page/菲尔特.md "wikilink")（Fürth）
70. [罗伊特林根](../Page/罗伊特林根.md "wikilink")（Reutlingen）
71. [默尔斯](../Page/默尔斯.md "wikilink")（Moers）
72. [萨尔茨吉特](../Page/萨尔茨吉特.md "wikilink")（Salzgitter）
73. [科布伦茨](../Page/科布伦茨.md "wikilink")（Koblenz）
74. [锡根](../Page/锡根.md "wikilink")（Siegen）
75. [贝尔吉施格拉德巴赫](../Page/贝尔吉施格拉德巴赫.md "wikilink")（Bergisch Gladbach）
76. [科特布斯](../Page/科特布斯.md "wikilink")（Cottbus）
77. [埃尔朗根](../Page/埃尔朗根.md "wikilink")（Erlangen）
78. [特里尔](../Page/特里尔.md "wikilink")（Trier）
79. [希尔德斯海姆](../Page/希尔德斯海姆.md "wikilink")（Hildesheim）
80. [格拉](../Page/格拉.md "wikilink")（Gera）
81. [耶拿](../Page/耶拿.md "wikilink")（Jena）
82. [维滕](../Page/维滕.md "wikilink")（Witten）

## 20,000人-100,000人

83. [凯撒斯劳滕](../Page/凯撒斯劳滕.md "wikilink")（Kaiserslautern）
84. [茨维考](../Page/茨维考.md "wikilink")（Zwickau）
85. [伊瑟隆](../Page/伊瑟隆.md "wikilink")（Iserlohn）
86. [居特斯洛](../Page/居特斯洛.md "wikilink")（Gütersloh）
87. [什未林](../Page/什未林.md "wikilink")（Schwerin）
88. [迪伦](../Page/迪伦.md "wikilink")（Düren）
89. [拉廷根](../Page/拉廷根.md "wikilink")（Ratingen）
90. [内卡河畔埃斯林根](../Page/埃斯林根.md "wikilink")（Esslingen am Neckar）
91. [马尔](../Page/马尔.md "wikilink")（Marl）
92. [吕嫩](../Page/吕嫩.md "wikilink")（Lünen）
93. [哈瑙](../Page/哈瑙.md "wikilink")（Hanau）
94. [路德维希堡](../Page/路德维希堡.md "wikilink")（Ludwigsburg）
95. [费尔贝特](../Page/费尔贝特.md "wikilink")（Velbert）
96. [弗伦斯堡](../Page/弗伦斯堡.md "wikilink")（Flensburg）
97. [蒂宾根](../Page/蒂宾根.md "wikilink")（Tübingen）
98. [明登](../Page/明登.md "wikilink")（Minden）
99. [威廉港](../Page/威廉港.md "wikilink")（Wilhelmshaven）
100. [沃尔姆斯](../Page/沃尔姆斯.md "wikilink")（Worms）
101. [菲林根-施文宁根](../Page/菲林根-施文宁根.md "wikilink")（Villingen-Schwenningen）
102. [康斯坦茨](../Page/康斯坦茨.md "wikilink")（Konstanz）
103. [马尔堡](../Page/马尔堡_\(德国\).md "wikilink")（Marburg）
104. [多斯滕](../Page/多斯滕.md "wikilink")（Dorsten）
105. [吕登沙伊德](../Page/吕登沙伊德.md "wikilink")（Lüdenscheid）
106. [新明斯特](../Page/新明斯特.md "wikilink")（Neumünster）
107. [德绍](../Page/德绍.md "wikilink")（Dessau）
108. [卡斯特罗普-劳克塞尔](../Page/卡斯特罗普-劳克塞尔.md "wikilink")（Castrop-Rauxel）
109. [赖讷](../Page/赖讷.md "wikilink")（Rheine）
110. [格拉德贝克](../Page/格拉德贝克.md "wikilink")（Gladbeck）
111. [菲尔森](../Page/菲尔森.md "wikilink")（Viersen）
112. [阿恩斯贝格](../Page/阿恩斯贝格.md "wikilink")（Arnsberg）
113. [德尔门霍斯特](../Page/德尔门霍斯特.md "wikilink")（Delmenhorst）
114. [特罗斯多夫](../Page/特罗斯多夫.md "wikilink")（Troisdorf）
115. [吉森](../Page/吉森.md "wikilink")（Gießen）
116. [代特莫尔德](../Page/代特莫尔德.md "wikilink")（Detmold）
117. [博霍尔特](../Page/博霍尔特.md "wikilink")（Bocholt）
118. [拜罗伊特](../Page/拜罗伊特.md "wikilink")（Bayreuth）
119. [哈弗尔河畔勃兰登堡](../Page/哈弗尔河畔勃兰登堡.md "wikilink")（Brandenburg an der
     Havel）
120. [吕讷堡](../Page/吕讷堡.md "wikilink")（Lüneburg）
121. [诺德施泰特](../Page/诺德施泰特.md "wikilink")（Norderstedt）
122. [策勒](../Page/策勒_\(德国\).md "wikilink")（Celle）
123. [丁斯拉肯](../Page/丁斯拉肯.md "wikilink")（Dinslaken）
124. [班贝格](../Page/班贝格.md "wikilink")（Bamberg）
125. \-{[阿沙芬堡](../Page/阿沙芬堡.md "wikilink")}-（Aschaffenburg）
126. [普劳恩](../Page/普劳恩.md "wikilink")（Plauen）
127. [翁纳县](../Page/翁纳县.md "wikilink")（Unna）
128. [新勃兰登堡](../Page/新勃兰登堡.md "wikilink")（Neubrandenburg）
129. [利普施塔特](../Page/利普施塔特.md "wikilink")（Lippstadt）
130. [阿伦](../Page/阿伦.md "wikilink")（Aalen）
131. [诺伊维德](../Page/诺伊维德.md "wikilink")（Neuwied）
132. [黑尔福德](../Page/黑尔福德.md "wikilink")（Herford）
133. [魏玛](../Page/魏玛.md "wikilink")（Weimar）
134. [克彭](../Page/克彭.md "wikilink")（Kerpen）
135. [格雷文布罗伊希](../Page/格雷文布罗伊希.md "wikilink")（Grevenbroich）
136. [黑尔滕](../Page/黑尔滕.md "wikilink")（Herten）
137. [富尔达](../Page/富尔达.md "wikilink")（Fulda）
138. [多尔马根](../Page/多尔马根.md "wikilink")（Dormagen）
139. [加布森](../Page/加布森.md "wikilink")（Garbsen）
140. [贝格海姆](../Page/贝格海姆.md "wikilink")（Bergheim）
141. [奥德河畔法兰克福](../Page/奥德河畔法兰克福.md "wikilink")（Frankfurt (Oder)）
142. [兰茨胡特](../Page/兰茨胡特.md "wikilink")（Landshut）
143. [肯普滕](../Page/肯普滕.md "wikilink")（Kempten (Allgäu)）
144. [韦塞尔](../Page/韦塞尔.md "wikilink")（Wesel）
145. [施韦比施格明德](../Page/施韦比施格明德.md "wikilink")（Schwäbisch Gmünd）
146. [辛德林根](../Page/辛德林根.md "wikilink")（Sindelfingen）
147. [罗森海姆](../Page/罗森海姆.md "wikilink")（Rosenheim）
148. [吕塞尔斯海姆](../Page/吕塞尔斯海姆.md "wikilink")（Rüsselsheim）
149. [朗根费尔德](../Page/朗根费尔德.md "wikilink")（Langenfeld (Rheinland)）
150. [奥芬堡](../Page/奥芬堡.md "wikilink")（Offenburg）
151. [施托尔贝格](../Page/施托尔贝格.md "wikilink")（Stolberg (Rheinland)）
152. [哈梅尔恩](../Page/哈梅尔恩.md "wikilink")（Hameln）
153. [施特拉尔松](../Page/施特拉尔松.md "wikilink")（Stralsund）
154. [腓特烈港](../Page/腓特烈港.md "wikilink")（Friedrichshafen）
155. [格平根](../Page/格平根.md "wikilink")（Göppingen）
156. [门登](../Page/门登.md "wikilink")（Menden (Sauerland)）
157. [格尔利茨](../Page/格尔利茨.md "wikilink")（Görlitz）
158. [哈廷根](../Page/哈廷根.md "wikilink")（Hattingen）
159. [希尔登](../Page/希尔登.md "wikilink")（Hilden）
160. [圣奥古斯丁](../Page/圣奥古斯丁_\(德国\).md "wikilink")（Sankt Augustin）
161. [埃施韦勒](../Page/埃施韦勒.md "wikilink")（Eschweiler）
162. [许尔特](../Page/许尔特.md "wikilink")（Hürth）
163. [奥伊斯基兴](../Page/奥伊斯基兴.md "wikilink")（Euskirchen）
164. [巴登-巴登](../Page/巴登-巴登.md "wikilink")（Baden-Baden）
165. [阿伦](../Page/阿伦_\(北莱茵-威斯特法伦\).md "wikilink")（Ahlen）
166. [巴特萨尔茨乌夫伦](../Page/巴特萨尔茨乌夫伦.md "wikilink")（Bad Salzuflen）
167. [梅尔布施](../Page/梅尔布施.md "wikilink")（Meerbusch）
168. [沃尔芬比特尔](../Page/沃尔芬比特尔.md "wikilink")（Wolfenbüttel）
169. [施韦因富特](../Page/施韦因富特.md "wikilink")（Schweinfurt）
170. [普尔海姆](../Page/普尔海姆.md "wikilink")（Pulheim）
171. [葡萄酒之路上的诺伊施塔特](../Page/葡萄酒之路上的诺伊施塔特.md "wikilink")（Neustadt an der
     Weinstraße）
172. [格赖夫斯瓦尔德](../Page/格赖夫斯瓦尔德.md "wikilink")（Greifswald）
173. [诺德霍恩](../Page/诺德霍恩.md "wikilink")（Nordhorn）
174. [魏布林根](../Page/魏布林根.md "wikilink")（Waiblingen）
175. [古默斯巴赫](../Page/古默斯巴赫.md "wikilink")（Gummersbach）
176. [韦茨拉尔](../Page/韦茨拉尔.md "wikilink")（Wetzlar）
177. [贝格卡门](../Page/贝格卡门.md "wikilink")（Bergkamen）
178. [维利希](../Page/维利希.md "wikilink")（Willich）
179. [巴特霍姆堡](../Page/巴德洪布尔格福尔德尔赫黑.md "wikilink")（Bad Homburg vor der
     Höhe）
180. [库克斯港](../Page/库克斯港.md "wikilink")（Cuxhaven）
181. [埃姆登](../Page/埃姆登.md "wikilink")（Emden）
182. [新乌尔姆](../Page/新乌尔姆.md "wikilink")（Neu-Ulm）
183. [林根](../Page/林根.md "wikilink")（Lingen (Ems)）
184. [朗根哈根](../Page/朗根哈根.md "wikilink")（Langenhagen）
185. [伊本比伦](../Page/伊本比伦.md "wikilink")（Ibbenbüren）
186. [埃尔夫茨塔特](../Page/埃尔夫茨塔特.md "wikilink")（Erftstadt）
187. [施派尔](../Page/施派尔.md "wikilink")（Speyer）
188. [帕绍](../Page/帕绍.md "wikilink")（Passau）
189. [弗莱贝格](../Page/弗莱贝格.md "wikilink") (Freiberg (Sachsen))

## 参考文献

<div class="references-small">

  - [德国联邦统计局 - 城镇目录](http://www.destatis.de/gv/suche_gv2000.htm)
  - [德国城市议会](https://web.archive.org/web/20071005125411/http://www.staedtetag.de/10/staedte/nach_einwohner/index.html)

</div>

[分類:城市人口列表](../Page/分類:城市人口列表.md "wikilink")

[az:Almaniya şəhərlərinin
siyahısı](../Page/az:Almaniya_şəhərlərinin_siyahısı.md "wikilink")
[bg:Списък на градовете в
Германия](../Page/bg:Списък_на_градовете_в_Германия.md "wikilink")
[cs:Seznam měst v
Německu](../Page/cs:Seznam_měst_v_Německu.md "wikilink")
[el:Κατάλογος πόλεων της
Γερμανίας](../Page/el:Κατάλογος_πόλεων_της_Γερμανίας.md "wikilink")
[eu:Alemaniako hiri nagusien
zerrenda](../Page/eu:Alemaniako_hiri_nagusien_zerrenda.md "wikilink")
[ka:გერმანიის დიდი ქალაქების
სია](../Page/ka:გერმანიის_დიდი_ქალაქების_სია.md "wikilink")
[ko:독일의 인구순 도시 목록](../Page/ko:독일의_인구순_도시_목록.md "wikilink") [sk:Zoznam
miest v Nemecku](../Page/sk:Zoznam_miest_v_Nemecku.md "wikilink")
[sw:Orodha ya miji ya
Ujerumani](../Page/sw:Orodha_ya_miji_ya_Ujerumani.md "wikilink")
[vi:Danh sách những thành phố lớn của
Đức](../Page/vi:Danh_sách_những_thành_phố_lớn_của_Đức.md "wikilink")

[德國城市](../Category/德國城市.md "wikilink")
[Category:德國相關列表](../Category/德國相關列表.md "wikilink")
[De](../Category/歐洲城市列表.md "wikilink")