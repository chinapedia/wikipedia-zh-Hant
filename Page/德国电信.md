[Bonn_DTAG2.jpg](https://zh.wikipedia.org/wiki/File:Bonn_DTAG2.jpg "fig:Bonn_DTAG2.jpg")

**德国电信股份公司**（，），總部坐落于[德國](../Page/德國.md "wikilink")[波恩](../Page/波恩.md "wikilink")，是欧洲大型[电信公司](../Page/电信公司.md "wikilink")，通过通信网络（[ISDN](../Page/ISDN.md "wikilink"),
[DSL](../Page/DSL.md "wikilink"),
[卫星等](../Page/卫星.md "wikilink")）提供信息和通讯服务，例如[电话](../Page/电话.md "wikilink")（固定电话和移动电话）和上网服务。同時也是全德国最重要的[无线电发射运营商](../Page/无线电.md "wikilink")。原民主德国所有重要的无线发射台现归德国电信所有。在老的联邦州，私人广播电台（[欧洲一台例外](../Page/欧洲一台.md "wikilink")）、[德意志广播电台](../Page/德意志广播电台.md "wikilink")、[德国广播电台](../Page/德国广播电台.md "wikilink")，以及[德国电视二台的节目都是通过德国电信的发射台进行广播](../Page/德国电视二台.md "wikilink")。在某些地方甚至[美国军队电视网的节目也是通过德国电信的发射台发射](../Page/美国军队电视网.md "wikilink")。德国电信在全球拥有257,600名雇员\[1\]

德国电信前身是由原[民主德国](../Page/民主德国.md "wikilink")（東德）的德国邮政和[联邦德国](../Page/联邦德国.md "wikilink")（西德）的联邦邮政合并成国有[德国联邦邮政的](../Page/德国邮政.md "wikilink")[电信部门](../Page/电信.md "wikilink")，該部門分離並私有化後成為[股份公司](../Page/股份公司.md "wikilink")（AG）。德意志联邦共和国直接持有31%的股份，另外通过[德国重建信贷银行](../Page/德国重建信贷银行.md "wikilink")（）间接持有12%的股份。剩余的57%的股份（至2003年6月）为公开发行股票。

2006年11月13日，德国电信公司任命[勒内·奥伯曼为](../Page/勒内·奥伯曼.md "wikilink")[首席执行官](../Page/首席执行官.md "wikilink")。\[2\]

## 股票发行

在1995年1月1日公司成立时，该公司股票是不上市交易的。直到1996年11月18日才进入交易所。德国电信的股票被认为是一枝“大众股”，他意味着安全长期可靠的盈利。他带起了德国不常见的股市投资风潮。他由最初[招股书中所认定的每股](../Page/招股书.md "wikilink")14，57上升到2000年3月6日的104，90每股，之后开始慢慢回落。

这起股市波动还引起了集团内部人事变动。最后在2002年11月15日[Kai-Uwe
Ricke成为了德国电信的新主席](../Page/Kai-Uwe_Ricke.md "wikilink")。

## 集团组成

[Deutsche_Telekom_world_locations.svg](https://zh.wikipedia.org/wiki/File:Deutsche_Telekom_world_locations.svg "fig:Deutsche_Telekom_world_locations.svg")
[Deutsche_Telekom_booth.JPG](https://zh.wikipedia.org/wiki/File:Deutsche_Telekom_booth.JPG "fig:Deutsche_Telekom_booth.JPG")
**至2004的业务划分**
德国电信到2004年底划分出4个主要的业务范围，每一部分都有自己独立的[董事会和经营自主权](../Page/董事会.md "wikilink")：

  - **[T-Com](../Page/T-Com.md "wikilink")**，固定电话业务。除了通过[数字电话網](../Page/数字电话.md "wikilink")（商标名:
    T-Net）和T-ISDN（ISDN）提供基本通话服务之外还有还有数字服务T-DSL（DSL）和DTAG-IPnet（基于光纤的高性能骨干互联网，参看[IP电话](../Page/IP电话.md "wikilink")）。

<!-- end list -->

  - **[T-Mobile](../Page/T-Mobile.md "wikilink")**移动电话业务。通过[GSM网络](../Page/GSM.md "wikilink"),
    [GPRS网络和](../Page/GPRS.md "wikilink")[UMTS网络提供移动电话语音和数据服务](../Page/UMTS.md "wikilink")。

<!-- end list -->

  - **[T-Online](../Page/T-Online.md "wikilink")**互联网业务。T-Online作为[ISP通过](../Page/ISP.md "wikilink")[调制解调器](../Page/调制解调器.md "wikilink"),
    [ISDN和](../Page/ISDN.md "wikilink")[DSL提供](../Page/DSL.md "wikilink")[互联网接入](../Page/互联网.md "wikilink")。

<!-- end list -->

  - **[T-Systems](../Page/T-Systems.md "wikilink")**，提供系统集成服务。T-Systems主要面向大用户和集团企业用户和大型项目实施。另外，他也有对应领域的研究和开发部门。

### 2005起

德国电信“四大支柱”在2005年初定出**3个战略领域**。这个"战越性新格局"到现在还是不很明朗。

## 著名设施

德国电信作为德国联邦邮政和东德邮政的法定继承法人，现在拥有很多著名的建筑设施:

**电信塔/电视塔**

下面的建筑是钢混或者钢架塔，部分还有开放的观光平台。

  - [柏林电视塔](../Page/柏林电视塔.md "wikilink")
  - （电信塔Berlin-Schäferberg）
  - （不来梅电视塔）
  - （Geyer）发射塔
  - （Telemax），汉诺威
  - （VW塔），汉诺威
  - （Heinrich-Hertz-Turm），汉堡
  - （Florianturm），多特蒙德
  - （莱茵塔），杜塞尔多夫
  - 电视塔（Bungsberg）, Eutin
  - （基尔电视塔），基尔
  - （Colonius），科隆
  - （Brocken）发射塔
  - （Kulpenberg电视塔）
  - （德累斯顿电视塔）
  - （Schwerin-Zippendorf电视塔）
  - （欧罗巴电视塔），美茵河畔法兰克福
  - （Nürnberg电视塔）
  - （曼海姆电视塔）
  - （Heubach）电视塔
  - （明斯特电视塔）
  - （斯图加特电视塔）
  - （奥林匹亚塔），慕尼黑
  - （Jakobsberg）电视塔，（Porta Westfalica）
  - （Friedrich-Clemens-Gerke-Turm）, Cuxhaven
  - （北黑森塔）, Schömberg
  - Richtfunkturm,（Torfhaus）
  - （太阳堡）发射塔-（Bleßberg）

**Abgespannte Sendemasten für UKW, Richtfunk und TV**

  - 定向发射台柏林-（Frohnau）
  - 定向发射台（Gartow）
  - 调频和电视发射天线（Heidelstein）
  - 调频和电视发射天线（Torfhaus）
  - 调频和电视发射天线（Wesel）
  - 调频和电视发射天线（Treplin）
  - 调频和电视发射天线（Casekow）

**长波和中波发射台**

  - 发射台（Donebach） (长波，节目：DLF）
  - 发射台（Zehlendorf，长波，节目：DLR；中波，节目：Stimme Rußlands）
  - 发射台（Aholming，长波，节目：DLF）
  - 发射台（Nordkirchen，中波，节目：DLF）
  - 发射台（Thurnau，中波，节目：DLF）
  - 发射台（Ravensburg，中波，节目：DLF）
  - 发射台（Cremlingen，中波，节目：DLF）
  - 发射台（Neumünster，中波，节目：DLF）
  - 发射台（Mainflingen，长波和中波）
  - 发射台（Burg，长波和中波）
  - 发射台（Wilsdruff，中波，节目：北德广播电台）
  - 发射台（Wiederau，中波，节目：北德广播电台）
  - 发射台（Wachenbrunn，中波，节目：北德广播电台和俄罗斯之声）
  - 发射台（Wöbbelin，中波，在建）
  - 发射台（Hirschlanden，中波，节目：AFN）
  - 发射台（Reichenbach，中波，节目：北德广播电台）

**短波发射台**

  - 发射台（Wertachtal）
  - 发射台（Jülich）
  - 发射台（Nauen）

**Antennenmessplätze**

  - Antennenmessplatz（Brück）

**Erdfunkstellen**

  - （Erdfunkstelle Raisting）
  - （Erdfunkstelle Usingen）

## 参考文献

## 外部链接

  - [德国电信网站](http://www.telekom.com/)
  - [T-Com网站](http://www.t-com.de/)
  - [T-Mobile网站](http://www.t-mobile.de/)
  - [T-Online网站](http://www.t-online.de/)
  - [T-Systems网站](http://www.t-systems.de/)
  - [Das Informationsportal der
    Telekom](https://web.archive.org/web/20050413081307/http://www.t-info.de/)
  - [Chart](http://aktien.onvista.de/charts_historical.html?ID_OSI=181029&MONTHS=120&TYPE=HISTORICAL&PERIOD=5&DISPLAY=1&SCALE=1&GRID=1&VOL=1&SUPP_INFO=0&ID_EXCHANGE=#chart)
    mit dem Kursverlauf der T-Aktie
  - [Chronik des
    Niedergangs](http://www.manager-magazin.de/geld/artikel/0,2828,149279,00.html)
    – Artikel im Manager-Magazin

## 参见

  - [T-Online](../Page/T-Online.md "wikilink")
  - [T-Mobile](../Page/T-Mobile.md "wikilink")

{{-}}

[德国电信](../Category/德国电信.md "wikilink")
[Category:德國電訊公司](../Category/德國電訊公司.md "wikilink")
[Category:波恩公司](../Category/波恩公司.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")

1.  [德国电信2009年报](http://www.download-telekom.de/dt/StaticPage/82/04/80/deutsche_telekom_annual_report_2009_820480.pdf)

2.  [德国电信：勒内·奥伯曼为公司新的首席执行官](http://news.xinhuanet.com/tech/2006-11/15/content_5331136.htm)