**德国电视一台**（，直译为：第一）是[德国](../Page/德国.md "wikilink")[公共电视频道](../Page/公共电视.md "wikilink")，是[德国广播电视联合会各成员电视台的共同体](../Page/德国广播电视联合会.md "wikilink")。

## 历史

[Tagesschau_kulissen.JPG](https://zh.wikipedia.org/wiki/File:Tagesschau_kulissen.JPG "fig:Tagesschau_kulissen.JPG")
[Erfurt_Maus_und_Elefant1.jpg](https://zh.wikipedia.org/wiki/File:Erfurt_Maus_und_Elefant1.jpg "fig:Erfurt_Maus_und_Elefant1.jpg")
该电视台的节目最早是在1952年在[汉堡录制](../Page/汉堡.md "wikilink")，然后由当时的德国西北广播共同制作并放送。从1954年11月1日起，电视台称“德国电视台”（），其节目通过[德国公共广播联盟向全](../Page/德国公共广播联盟.md "wikilink")[联邦德国发送](../Page/联邦德国.md "wikilink")。直至1963年[德国电视二台出现前](../Page/德国电视二台.md "wikilink")，德国电视台为西德唯一的电视台。该台自1952年起播出的《[今日新闻](../Page/今日新闻_\(德国\).md "wikilink")》（）是德国最老且收视率最高的新闻节目。

1984年，该台被重命名为“第一德国电视”（Erstes Deutsches
Fernsehen），自1997年起有了现在的名字“第一台”（Das
Erste）。

## 德国广播联盟

德国广播联盟简称ARD，这个联盟的九个地区性的成员电视台共同组成了德国电视一台。这九个电视台在共同的节目时间表下向德国电视一台提供其节目。

### 节目时间分配

德国电视一台节目时间分配比例为：（2006年2月）

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>广播机构</p></th>
<th style="text-align: center;"><p>节目占有比例</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/拜仁广播.md" title="wikilink">拜仁广播</a> (BR)</p></td>
<td style="text-align: center;"><p>15.25</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/黑森广播.md" title="wikilink">黑森广播</a> (HR)</p></td>
<td style="text-align: center;"><p>7.35</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>(MDR)</p></td>
<td style="text-align: center;"><p>11.45</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/北德广播.md" title="wikilink">北德广播</a> (NDR)</p></td>
<td style="text-align: center;"><p>17.45</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/不莱梅广播.md" title="wikilink">不莱梅广播</a> (RB)</p></td>
<td style="text-align: center;"><p>1</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/柏林－勃兰登堡广播.md" title="wikilink">柏林－勃兰登堡广播</a> (RBB)</p></td>
<td style="text-align: center;"><p>7</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/萨尔兰广播.md" title="wikilink">萨尔兰广播</a> (SR)</p></td>
<td style="text-align: center;"><p>1.3</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/德国西南广播.md" title="wikilink">德国西南广播</a> (SWR)</p></td>
<td style="text-align: center;"><p>17.95</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/德国西北广播.md" title="wikilink">德国西北广播</a> (WDR)</p></td>
<td style="text-align: center;"><p>21.25</p></td>
</tr>
</tbody>
</table>

## 參見

  - [德國電視二台](../Page/德國電視二台.md "wikilink")
  - [德國之聲](../Page/德國之聲.md "wikilink")

## 参考资料

## 外部链接

  - [德国电视一台主页](http://www.daserste.de)

  - [德国广播联盟主页](http://www.ard.de)

[Category:德国电视台](../Category/德国电视台.md "wikilink")
[Category:德語電視頻道](../Category/德語電視頻道.md "wikilink")
[Category:慕尼黑公司](../Category/慕尼黑公司.md "wikilink")