**德国社会民主党**（），简称**德国社民党**（），[德国主要](../Page/德国.md "wikilink")[政党之一](../Page/政党.md "wikilink")，它是[德國歷史最悠久的](../Page/德國歷史.md "wikilink")[社会民主主义](../Page/社会民主主义.md "wikilink")[中間偏左政黨](../Page/中間偏左.md "wikilink")，成立于1863年。社民党於1966年至1982年，1998年至2009年和2013年至今参与聯合政府。政治光譜上，它較[基民盟/基社盟和](../Page/基民盟/基社盟.md "wikilink")[德国自由民主党為左](../Page/德国自由民主党.md "wikilink")，但較[聯盟90/綠党和](../Page/聯盟90/綠党.md "wikilink")[德国左派党為右](../Page/德国左派党.md "wikilink")。

## 基本政见

社民党来源于工人运动，有着明确的[社会主义性质](../Page/社会主义.md "wikilink")；社民党和[工会关系密切](../Page/工会.md "wikilink")，在意识形态上像19世纪[欧洲大多数](../Page/欧洲.md "wikilink")[社会主义和](../Page/社会主义.md "wikilink")[社会民主主义政党那样向革命性的](../Page/社会民主主义.md "wikilink")[马克思主义看齐](../Page/马克思主义.md "wikilink")。[愛德華·伯恩斯坦以他的](../Page/愛德華·伯恩斯坦.md "wikilink")[修正主义理论反对当时在社民党中仍占大多数的具有革命倾向的阵营](../Page/修正主义_\(马克思主义\).md "wikilink")。最晚在[第一次世界大战以后修正主义理论就在党内得到逐步但非正式的贯彻](../Page/第一次世界大战.md "wikilink")。这个理论主要包含：通过民主选举以民主的合法的手段来接管政府，并且以改革的方式来实现社会的社会主义改造的目标。

1989年，社民党告别了从1959年一直延用的[哥德斯堡綱領](../Page/哥德斯堡綱領.md "wikilink")，取而代之为[柏林宣言](../Page/柏林宣言.md "wikilink")。如今，社民党为了能更好的强调党派观点，清晰党派政见（特别是和基民盟），于2007年新发布的党派宣言。

2005年至2009年，社民党是德国大联合政府（社民党、[基民盟/基社盟](../Page/基民盟/基社盟.md "wikilink")）的重要成员，三个政党根据联合组阁协议，社民党取得8个内阁席位，主導內閣人事變動。前总理施罗德的办公厅主任[弗兰克-瓦尔特·施泰因迈尔将出任外交部长](../Page/弗兰克-瓦尔特·施泰因迈尔.md "wikilink")，财政部长一职则属于前州长施泰因布吕克。

社民党把[社会正义作为它的一个主要政见](../Page/社会正义.md "wikilink")。经济应该发展，利益应该公平分配，因此公民才能更好的享受社会福利。同时社民党认为一个强大而重视社会福利的国家才能保护弱势群体的权利，一个充分满足下一代需要的财政政策也是非常必要。为了能够实现这个目标，不同的人群能够获利，社民党提出了新改革议题，如新的富人税政策等。

在社会政治上，社民党致力于民法，开放式社会和公民参政上，它们是自由、正义和团结的基石。

国际政策上，社民党采取了比较积极的[欧洲一体化和对外政策](../Page/欧洲一体化.md "wikilink")。

2013年，63％的社民党党员都在60岁以上，8％为35岁以下的青年，三分之二的党员为男性，大多有工會背景；将近一半的党员是工人或者公司雇员，12％为家庭妇女（或居家男性），11％为公务员。

## 历史

1863年5月23日，社会主义活动家[斐迪南·拉萨尔创立了](../Page/斐迪南·拉萨尔.md "wikilink")“[全德意志工人联合会](../Page/全德意志工人联合会.md "wikilink")”（即拉萨尔派）。1869年，[奥古斯特·倍倍尔和](../Page/奥古斯特·倍倍尔.md "wikilink")[威廉·李卜克内西](../Page/威廉·李卜克内西.md "wikilink")（爱森纳赫派）创立“[德意志社会民主工党](../Page/德意志社会民主工党.md "wikilink")”。1875年5月22日—27日，兩黨在[哥达召开大会](../Page/哥达.md "wikilink")，在[卡爾·馬克思的調和下](../Page/卡爾·馬克思.md "wikilink")，合併整合成為德国社会主义工人党，并通过，马克思因此创作了。
因为社民党赞成革命反对[君主制](../Page/君主制.md "wikilink")，[俾斯麦在](../Page/俾斯麦.md "wikilink")1878年开始实施的“[反社会民主党人法](../Page/反社会民主党人法.md "wikilink")”，宣布该党为非法，进行镇压。
此举导致社民党党派结构的高速发展，并提高了工作效率。在此期间，被认为是国家力量代言人的陆军元帅[阿尔弗雷德·冯·瓦德西多次要求对社会民主人士进行暴力镇压](../Page/阿尔弗雷德·冯·瓦德西.md "wikilink")。但是，庞大的工人阶级在最紧要关头觉醒，使社民党很快成为了德国最大的党派。
1890年，德国社会主义工人党改名为“德国社会民主党”，恢复活动。1892年，该党正式合法化，并在[爱尔福特通过了](../Page/爱尔福特.md "wikilink")[爱尔福特纲领](../Page/爱尔福特纲领.md "wikilink")。由[卡尔·考茨基和](../Page/卡尔·考茨基.md "wikilink")[爱德华·伯恩斯坦撰写的党纲拒绝了改良主义](../Page/爱德华·伯恩斯坦.md "wikilink")，以马克思主义作为主要的思想指南针。1890年社民党已经占据了27.2%的席位，1912年社民党提高到了34.8%的席位。1913年倍倍尔死后，[弗里德里希·艾伯特和](../Page/弗里德里希·艾伯特.md "wikilink")[胡戈·哈阿兹共同领导社民党](../Page/胡戈·哈阿兹.md "wikilink")。

19世纪末20世纪初，爱德华·伯恩斯坦发表了他的[修正主义理论](../Page/修正主义.md "wikilink")，以反对社民党内部占主流的革命马克思主义理论。伯恩施坦认为在德国可以通过一次民主的，合法的选举，进行一次政府更替，从而实现德国的社会主义变革。修正主义理论一直在社民党实际政治策略中得到部分运用，但不被公开承认。直到[第一次世界大战之后](../Page/第一次世界大战.md "wikilink")，修正主义才逐步成为社民党的官方意识形态。

### 1914－1919: 一战、十一月革命、分裂

当[第一次世界大战爆发时](../Page/第一次世界大战.md "wikilink")，社民党支持的发行。只有[卡尔·李卜克内西](../Page/卡尔·李卜克内西.md "wikilink")（[威廉·李卜克内西之子](../Page/威廉·李卜克内西.md "wikilink")）和[奥托·吕勒两位社民党议员反对](../Page/奥托·吕勒.md "wikilink")1915年的战争公债。1916年在一次反战游行之后，卡尔·李卜克内西被捕，并判处监禁。但出于不明原因，他在战争结束前被释放。在战争中，很多议员不赞同社民党越来越低贱的姿态，他们在社民党之外另外组建了一个独立于社民党的党派——[德国独立社会民主党](../Page/德国独立社会民主党.md "wikilink")（USPD）。

1916年[卡尔·李卜克内西出狱后](../Page/卡尔·李卜克内西.md "wikilink")，他和[罗莎·卢森堡一同组织左派的](../Page/罗莎·卢森堡.md "wikilink")[斯巴达克同盟](../Page/斯巴达克同盟.md "wikilink")，又称“国际派”。左翼社会民主党人从社会民主党内分离出来，组建了新政党：独立社会民主党。形式上，斯巴达克同盟是独立社会民主党内的一派。加入独立社民党的社民党人不但有左派领袖[罗莎·卢森堡](../Page/罗莎·卢森堡.md "wikilink")，还有马克思主义的教皇、《[新时代](../Page/新时代_\(德国刊物\).md "wikilink")》的发行人[卡尔·考茨基](../Page/卡尔·考茨基.md "wikilink")，以及修正主义之父、理论家[爱德华·伯恩斯坦](../Page/爱德华·伯恩斯坦.md "wikilink")。

从1915年起一个叫列施—库诺—亨尼施集团（Lensch-Cunow-Hänisch-Gruppe）的组织（它和俄裔德国社会主义右翼政治家很亲近），取代考茨基和伯恩斯坦，成为主要的修正主义派别。他们期待[德国能在](../Page/德国.md "wikilink")[一战中获胜](../Page/一战.md "wikilink")，能够在欧洲实行[社会主义制度](../Page/社会主义制度.md "wikilink")，解放在俄罗斯[沙皇统治下的人民](../Page/沙皇.md "wikilink")。，人种学家，社民党党校讲师）在1917年取代了考茨基，成为《新时代》的发行人，也成为了后来的《格尔利茨宣言》
和《海德堡宣言》的撰写人之一。从1917年开始，当大多社会民主党人开始觉得，战争将以失败告终，该组织影响力也随之衰退。

1918年，德国的败局已定，在维尔赫尔姆斯哈芬和基尔的水兵开始起义，也就是历史上的[德国革命](../Page/德国革命.md "wikilink")，又称[十一月革命](../Page/十一月革命.md "wikilink")。革命使[德国皇帝](../Page/德国皇帝.md "wikilink")[威廉二世退位](../Page/威廉二世_\(德國\).md "wikilink")，最后逃到了[荷兰](../Page/荷兰.md "wikilink")。[弗里德里希·艾伯特领导](../Page/弗里德里希·艾伯特.md "wikilink")“多数派社会民主党”
（Mehrheitssozialdemokratische Partei
Deutschlands）为政府更替做好了准备，同时[马克斯·冯·巴登亲王也将政权移交给了社会民主党主席](../Page/马克斯·冯·巴登.md "wikilink")[弗里德里希·艾伯特](../Page/弗里德里希·艾伯特.md "wikilink")。

斯巴达克联盟和部分独立社民党人支持仿照年前在[俄国爆发的](../Page/俄国.md "wikilink")[十月革命](../Page/十月革命.md "wikilink")，在德国建立一个[苏维埃政权](../Page/巴伐利亚苏维埃共和国.md "wikilink")。但是革命士兵和工人并没有把俄国的十月革命做为榜样，他们大多都在为结束战争和推翻军政奔走。在此目标下多数派社民党和独立社民党再度合并。这个在艾伯特和哈阿兹领导下的，由多数派社民党和独立社民党平等组成的革命政府只是一个过渡政府，它只是作为不久后举行全民大选最终产生国民议会做准备。

1918年末，多数派社会民主党人和部分独立社民党人在联合政府中为军事镇压起义的水兵争论不休。多数社民党认为没有授权的行动是违背工人运动民主原则的，他们尝试建立和多数派社会民主党的自由团体。1919年1月，斯巴达克同盟发动第二股革命浪潮，让它横扫德国。艾伯特聘用自由军团镇压起义。

由[古斯塔夫·诺斯克征召的右派民族主义](../Page/古斯塔夫·诺斯克.md "wikilink")[自由军团在多数社会民主党的支持下](../Page/自由军团.md "wikilink")，对和苏维埃政权进行了血腥镇压。由此，古斯塔夫·诺斯克也成了[魏玛共和国的第一任国防部长](../Page/魏玛共和国.md "wikilink")，同时他也被冠以了“血手”的称号。他应该为众多的，知名的或不知名的，自由军团对革命者的谋杀负责。罗莎·卢森堡和卡尔·李卜克内西在1919年1月15日也未幸免于难。

艾伯特和诺斯克在十一月革命，以及对其镇压的角色，被当时的议会内外的左派政党的攻击，被认为是「革命的叛徒」。斯巴达克联盟和其他左派革命者联盟在1919年1月1日建立了[德国共产党](../Page/德国共产党.md "wikilink")。这也是革命社会民主党人和改良社会民主党人分道扬镳的标志。

### 1919－1933: 魏玛共和国

从1919－1925年，社民党的[弗里德里希·艾伯特一直主导年轻的](../Page/弗里德里希·艾伯特.md "wikilink")[魏玛共和国政府](../Page/魏玛共和国.md "wikilink")。1922年，左派开始崛起，社民党和残余的独立社民党再一次合并，从那以后，社民党和他们分担了政府部门的主导权，最后在1928－1930年组成了以[赫爾曼·穆勒为首的米勒二世内阁大联合政府](../Page/赫爾曼·穆勒.md "wikilink")。[奧托·布勞恩在](../Page/奧托·布勞恩.md "wikilink")[普鲁士从](../Page/普鲁士.md "wikilink")1920－1932年也一直担任普鲁士总理。

[第一次世界大战以后](../Page/第一次世界大战.md "wikilink")，社民党和由大部分脫離者组建的[德国共产党成了新的激烈竞争对手](../Page/德国共产党.md "wikilink")，相当重要的原因是因为[德国革命的余热](../Page/德国革命.md "wikilink")。由于一些工人阶级选民投向了[德国共产党](../Page/德国共产党.md "wikilink")，社民党一直试图作为反对党来保持在帝国内部政治的影响以减少选票的流失。社民党的社会基础在魏玛共和国期间以工会组织的专业工人队伍为主。

虽然[国家社会主义德国工人党](../Page/国家社会主义德国工人党.md "wikilink")（即[纳粹党](../Page/纳粹党.md "wikilink")）开始逐渐崛起，社民党还是保持了它的选民群体。纳粹党依靠年轻选民甚至非选民赢得选举，但是社民党并没有太多的反对。由于社民党结构上的症结，它不愿意和指责社民党是社会法西斯主义的[德国共产党合作](../Page/德国共产党.md "wikilink")，同时其他被边缘化的民主党也不是对抗纳粹的好伙伴。

社民党议会议员采取的妥协政策导致了在布吕宁政府中一部分年轻党员和社民党左派越来越多的责备。1931年一部分社民党左派再度组成了德国社会主义工人党（Sozialistische
Arbeiterpartei Deutschlands）。

[柏林的普鲁士政府领导人](../Page/柏林.md "wikilink")，社会主义者奧托·布勞恩，1932年7月20日在军事政变中被驱逐，在一场被称为的运动，社民党最后的堡垒也被攻破。

1933年，社民党在其他所有党派对启动《[授权法](../Page/德國1933年授權法.md "wikilink")》\[1\]投了赞成票的的情况下，毅然投出了反对票，唯一一个在投票中反对启动《授权法》的政党，捍卫了它作为一个强调民主的党派引以为豪。在社民党的所有党产被没收以后，相当一部分的党派领袖移居它国。5月17日一些社民党议会主要成员在生命威胁下通过了[希特勒的外交解释](../Page/希特勒.md "wikilink")。6月21日，社民党收到了希特勒的禁令，7月14日，社民党被禁止。

### 二戰後至今

[二战以后社民党重建](../Page/第二次世界大战.md "wikilink")。在[联邦德国](../Page/联邦德国.md "wikilink")，社民党起初处于在野位置，但是从1966年－1982年领导了联邦政府，[维利·勃兰特和](../Page/维利·勃兰特.md "wikilink")[赫尔穆特·施密特先後出任](../Page/赫尔穆特·施密特.md "wikilink")[西德总理](../Page/西德总理.md "wikilink")。在1959年的《[哥德斯堡纲领](../Page/哥德斯堡纲领.md "wikilink")》中社民党摒弃了阶级政党和[马克思主义原理的概念](../Page/马克思主义.md "wikilink")\[2\]，继续推行社会福利计划，在1989年德国社会民主党的基本纲领（1998年补充）的我们的历史渊源中称：“欧洲民主社会主义思想渊源渊源自基督教、人道主义哲学基督教、人道主义哲学、启蒙主义、马克思的历史和社会学说以及工人运动的经验。”\[3\]\[4\]又在2007年社民党纲领中的我们的核心价值观和核心理念称：“1959年的哥德斯堡纲领是植根于犹太教和基督教，人文主义和启蒙、社会的马克思主义分析和工人运动的经验”\[5\]。

虽然起初社民党反对[西德於](../Page/西德.md "wikilink")1955年加入[北约](../Page/北大西洋公约组织.md "wikilink")，不过现在十分强烈的支持德国加强与[歐洲联盟和北約的关系](../Page/歐洲联盟.md "wikilink")。

在苏军占领区也就是后来的[東德](../Page/東德.md "wikilink")，社民党被迫与[德国共产党合并成](../Page/德国共产党.md "wikilink")[德国统一社会党](../Page/德国统一社会党.md "wikilink")。1989年[东欧巨变以后](../Page/东欧.md "wikilink")，东德的社民党重新回到独立政党状态，[两德统一后与西德社民党合并](../Page/两德统一.md "wikilink")。

1990年德国统一后，1998年社民党在[施罗德带领下](../Page/施罗德.md "wikilink")，顺利在大选中胜利夺取政权，并在2002年连任，但其在2005年因提前解散大选而下台。随后，社民党加入了以基民盟为首的大联合政府，后于2009年因大选中惨败退出政府，成为在野党。

在[格哈特·施罗德领导下](../Page/格哈特·施罗德.md "wikilink")，社民党宣称建立一个更好的平台减少失业。1998年德国联邦选举社民党以40.9%的支持率成为第一大党。社民党获胜至关重要的力量在于大城市和拥有传统产业的联邦州。通过与[绿党组成的](../Page/德国绿党.md "wikilink")[联合政府](../Page/联合政府.md "wikilink")，社民党在1982年选举失利之后十六年重新执政。

1995年11月[奥斯卡·拉方丹经过选举成为社民党主席](../Page/奥斯卡·拉方丹.md "wikilink")，1998年他出任财政部长，1999年3月因與總理施罗德理念不合，辞去党内和政府职务。施罗德继拉方丹之后成为党主席。

2002年德国联邦选举，社民党得到了全国选票的38.5%，與绿党繼續组成新一届联合政府。其後联合政府反對[伊拉克戰爭](../Page/伊拉克戰爭.md "wikilink")。

2004年欧洲选举对社民党是一场灾难，在全国范围的选举中取得了最坏的成绩，[二战以后的最低点](../Page/二战.md "wikilink")21.5%。这一年早期，社民党领导权从总理施罗德移交给了弗朗茨·明特菲林，这被广泛认为是要解决党内对联邦政府推行经济改革的反对。

社民党的成员近年开始减少。1976年社民党拥有一百万成员，到1998年只有77万5千人，2003年9月降到了66万3千人，2005年11月只剩下59万1千人。不少支持者轉而支持比社民黨更為左傾的綠黨和左派黨。

2005年4月，党主席明特菲林公开反对在德国[自由市场经济中的过于牟利](../Page/自由市场经济.md "wikilink")，并且要求联邦政府在推进经济正义方面更多干涉。这引起了一场主导全国新闻几个礼拜的辩论，成为了几乎所有主要期刊头版文章的主题，也得到了主要电视新闻几乎每天报道。明特菲林的建议已遭到一些雇员组织和经济学家批评，但是仍然得到公众支持（某些民意测验有75%支持）。

2005年1月，一些社民党左派成员离开该党成立了劳动和社会公平党（WASG）反对社民党的右倾和[新自由主义](../Page/新自由主义.md "wikilink")，該黨2007年併入[德國左派黨](../Page/德國左派黨.md "wikilink")。5月，前社民党主席[奥斯卡·拉方丹加入左派党](../Page/奥斯卡·拉方丹.md "wikilink")。

2005年，一向有“社民党家乡”之称的[北威-{zh-hans:州;zh-hant:邦}-举行大选](../Page/北威州.md "wikilink")，社民党失去了长达39年的执政党地位。从而导致施罗德政府提前于9月18日举行聯邦國會大选。大選結果，社民党與主要对手基民盟打成平手，但由於與綠黨議席不過半，最終與基民盟組建[大聯合政府](../Page/大聯合政府.md "wikilink")。

2006年4月10日，社民党主席马蒂亚斯·普拉策克因听力严重下降辭職。\[6\]臨時黨大會選出前副主席[庫特·貝克繼任](../Page/庫特·貝克.md "wikilink")。

[2009年德国聯邦議院选举](../Page/2009年德国聯邦議院选举.md "wikilink")，社民黨的得票率為23.0%。

[2013年德国聯邦議院选举](../Page/2013年德国聯邦議院选举.md "wikilink")，社民黨的得票率為25.7%，與基民盟組建[大聯合政府](../Page/大聯合政府.md "wikilink")。

[2017年德国聯邦議院选举](../Page/2017年德国聯邦議院选举.md "wikilink")，社民黨的得票率為20.5%，為68年來最低得票率。

## 選舉席次

### 國會席次

<table>
<thead>
<tr class="header">
<th><p>選舉年份</p></th>
<th><p>選區投票</p></th>
<th><p>黨派選票</p></th>
<th><p>總票數（至1912年）/<br />
黨派選票（截至1919年）</p></th>
<th><p>總佔據席位</p></th>
<th><p>+/–</p></th>
<th><p>政府陣營</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1877年德國聯邦選舉.md" title="wikilink">1877年</a></p></td>
<td><p>493,447</p></td>
<td></td>
<td><p>9.1 (#4)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1878年德國聯邦選舉.md" title="wikilink">1878年</a></p></td>
<td><p>437,158</p></td>
<td></td>
<td><p>7.6 (#5)</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1881年德國聯邦選舉.md" title="wikilink">1881年</a></p></td>
<td><p>311,961</p></td>
<td></td>
<td><p>6.1 (#7)</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1884年德國聯邦選舉.md" title="wikilink">1884年</a></p></td>
<td><p>549,990</p></td>
<td></td>
<td><p>9.7 (#5)</p></td>
<td></td>
<td><p>11</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1887年德國聯邦選舉.md" title="wikilink">1887年</a></p></td>
<td><p>763,102</p></td>
<td></td>
<td><p>10.1 (#5)</p></td>
<td></td>
<td><p>13</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1890年德國聯邦選舉.md" title="wikilink">1890年</a></p></td>
<td><p>1,427,323</p></td>
<td></td>
<td><p>19.7 (#1)</p></td>
<td></td>
<td><p>24</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1893年德國聯邦選舉.md" title="wikilink">1893年</a></p></td>
<td><p>1,786,738</p></td>
<td></td>
<td><p>23.3 (#1)</p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1898年德國聯邦選舉.md" title="wikilink">1898年</a></p></td>
<td><p>2,107,076</p></td>
<td></td>
<td><p>27.2 (#1)</p></td>
<td></td>
<td><p>12</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1903年德國聯邦選舉.md" title="wikilink">1903年</a></p></td>
<td><p>3,010,771</p></td>
<td></td>
<td><p>31.7 (#1)</p></td>
<td></td>
<td><p>25</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1907年德國聯邦選舉.md" title="wikilink">1907</a></p></td>
<td><p>3,259,029</p></td>
<td></td>
<td><p>28.9 (#1)</p></td>
<td></td>
<td><p>38</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1912年德國聯邦選舉.md" title="wikilink">1912年</a></p></td>
<td><p>4,250,399</p></td>
<td></td>
<td><p>34.8 (#1)</p></td>
<td></td>
<td><p>67</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1919年德國聯邦選舉.md" title="wikilink">1919年</a></p></td>
<td></td>
<td><p>11,509,048</p></td>
<td><p>37.9 (#1)</p></td>
<td></td>
<td><p>55</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1920年德國聯邦選舉.md" title="wikilink">1920年</a></p></td>
<td></td>
<td><p>6,179,991</p></td>
<td><p>21.9 (#1)</p></td>
<td></td>
<td><p>63</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1924年5月德國聯邦選舉.md" title="wikilink">1924年5月</a></p></td>
<td></td>
<td><p>6,008,905</p></td>
<td><p>20.5 (#1)</p></td>
<td></td>
<td><p>2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1924年12月德國聯邦選舉.md" title="wikilink">1924年12月</a></p></td>
<td></td>
<td><p>7,881,041</p></td>
<td><p>26.0 (#1)</p></td>
<td></td>
<td><p>31</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1928年德國聯邦選舉.md" title="wikilink">1928年</a></p></td>
<td></td>
<td><p>9,152,979</p></td>
<td><p>29.8 (#1)</p></td>
<td></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1930年德國聯邦選舉.md" title="wikilink">1930年</a></p></td>
<td></td>
<td><p>8,575,244</p></td>
<td><p>24.5 (#1)</p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1932年7月德国国会选举.md" title="wikilink">1932年7月</a></p></td>
<td></td>
<td><p>7,959,712</p></td>
<td><p>21.6 (#2)</p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1932年11月德国国会选举.md" title="wikilink">1932年11月</a></p></td>
<td></td>
<td><p>7,247,901</p></td>
<td><p>20.4 (#2)</p></td>
<td></td>
<td><p>12</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1933年3月德国国会选举.md" title="wikilink">1933年3月</a></p></td>
<td></td>
<td><p>7,181,629</p></td>
<td><p>18.3 (#2)</p></td>
<td></td>
<td><p>1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1949年西德國聯邦選舉.md" title="wikilink">1949年</a></p></td>
<td></td>
<td><p>6,934,975</p></td>
<td><p>29.2 (#2)</p></td>
<td></td>
<td><p>11</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1953年西德聯邦選舉.md" title="wikilink">1953年</a></p></td>
<td><p>8,131,257</p></td>
<td><p>7,944,943</p></td>
<td><p>28.8 (#2)</p></td>
<td></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1957年西德聯邦選舉.md" title="wikilink">1957年</a></p></td>
<td><p>11,975,400</p></td>
<td><p>11,875,339</p></td>
<td><p>31.8 (#2)</p></td>
<td></td>
<td><p>19</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1961年西德聯邦選舉.md" title="wikilink">1961年</a></p></td>
<td><p>11,672,057</p></td>
<td><p>11,427,355</p></td>
<td><p>36.2 (#2)</p></td>
<td></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1965年西德聯邦選舉.md" title="wikilink">1965年</a></p></td>
<td><p>12,998,474</p></td>
<td><p>12,813,186</p></td>
<td><p>39.3 (#2)</p></td>
<td></td>
<td><p>14</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1969年西德聯邦選舉1969.md" title="wikilink">1969年</a></p></td>
<td><p>14,402,374</p></td>
<td><p>14,065,716</p></td>
<td><p>42.7 (#2)</p></td>
<td></td>
<td><p>20</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年西德聯邦選舉.md" title="wikilink">1972年</a></p></td>
<td><p>18,228,239</p></td>
<td><p>17,175,169</p></td>
<td><p>45.8 (#1)</p></td>
<td></td>
<td><p>5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年西德聯邦選舉.md" title="wikilink">1976年</a></p></td>
<td><p>16,471,321</p></td>
<td><p>16,099,019</p></td>
<td><p>42.6 (#2)</p></td>
<td></td>
<td><p>18</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年西德聯邦選舉.md" title="wikilink">1980年</a></p></td>
<td><p>16,808,861</p></td>
<td><p>16,260,677</p></td>
<td><p>42.9 (#2)</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1983年西德聯邦選舉.md" title="wikilink">1983年</a></p></td>
<td><p>15,686,033</p></td>
<td><p>14,865,807</p></td>
<td><p>38.2 (#2)</p></td>
<td></td>
<td><p>26</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1987年西德聯邦選舉.md" title="wikilink">1987年</a></p></td>
<td><p>14,787,953</p></td>
<td><p>14,025,763</p></td>
<td><p>37.0 (#2)</p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990年德國聯邦選舉.md" title="wikilink">1990年</a></p></td>
<td><p>16,279,980</p></td>
<td><p>15,545,366</p></td>
<td><p>33.5 (#2)</p></td>
<td></td>
<td><p>46</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年德國聯邦選舉.md" title="wikilink">1994年</a></p></td>
<td><p>17,966,813</p></td>
<td><p>17,140,354</p></td>
<td><p>36.4 (#2)</p></td>
<td></td>
<td><p>13</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年德國聯邦選舉.md" title="wikilink">1998年</a></p></td>
<td><p>21,535,893</p></td>
<td><p>20,181,269</p></td>
<td><p>40.9 (#1)</p></td>
<td></td>
<td><p>43</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年德國聯邦選舉.md" title="wikilink">2002年</a></p></td>
<td><p>20,059,967</p></td>
<td><p>18,484,560</p></td>
<td><p>38.5 (#1)[7]</p></td>
<td></td>
<td><p>47</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005年德国联邦议院选举.md" title="wikilink">2005年</a></p></td>
<td><p>18,129,100</p></td>
<td><p>16,194,665</p></td>
<td><p>34.2 (#2)</p></td>
<td></td>
<td><p>29</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009年德國聯邦議院選舉.md" title="wikilink">2009年</a></p></td>
<td><p>12,077,437</p></td>
<td><p>9,988,843</p></td>
<td><p>23.0 (#2)</p></td>
<td></td>
<td><p>76</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2013年德国联邦议院选举.md" title="wikilink">2013年</a></p></td>
<td><p>12,835,933</p></td>
<td><p>11,247,283</p></td>
<td><p>25.7 (#2)</p></td>
<td></td>
<td><p>42</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年德国联邦议院选举.md" title="wikilink">2017年</a></p></td>
<td><p>11,426,613</p></td>
<td><p>9,538,367</p></td>
<td><p>20.5 (#2)</p></td>
<td></td>
<td><p>40</p></td>
<td></td>
</tr>
</tbody>
</table>

### 歐洲議會席次

<table>
<thead>
<tr class="header">
<th><p>Election year</p></th>
<th><p># of<br />
overall votes</p></th>
<th><p>% of<br />
overall vote</p></th>
<th><p># of<br />
overall seats won</p></th>
<th><p>+/–</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1979_(West_Germany).md" title="wikilink">1979</a></p></td>
<td><p>11,370,045</p></td>
<td><p>40.8 (#1)</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_1984_(West_Germany).md" title="wikilink">1984</a></p></td>
<td><p>9,296,417</p></td>
<td><p>37.4 (#2)</p></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1989_(West_Germany).md" title="wikilink">1989</a></p></td>
<td><p>10,525,728</p></td>
<td><p>37.3 (#1)</p></td>
<td></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_1994_(Germany).md" title="wikilink">1994</a></p></td>
<td><p>11,389,697</p></td>
<td><p>32.2 (#1)</p></td>
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1999_(Germany).md" title="wikilink">1999</a></p></td>
<td><p>8,307,085</p></td>
<td><p>30.7 (#2)</p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_2004_(Germany).md" title="wikilink">2004</a></p></td>
<td><p>5,547,971</p></td>
<td><p>21.5 (#2)</p></td>
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_2009_(Germany).md" title="wikilink">2009</a></p></td>
<td><p>5,472,566</p></td>
<td><p>20.8 (#2)</p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_2014_(Germany).md" title="wikilink">2014</a></p></td>
<td><p>7,999,955</p></td>
<td><p>27.2 (#2)</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 邦議會席次

## 社會民主黨領袖

### 第一次世界大战前的社民党领袖

  - [奥古斯特·倍倍尔](../Page/奥古斯特·倍倍尔.md "wikilink")
  - [威廉·李卜克内西](../Page/威廉·李卜克内西.md "wikilink")
  - [卡尔·考茨基](../Page/卡尔·考茨基.md "wikilink")
  - [爱德华·伯恩斯坦](../Page/爱德华·伯恩斯坦.md "wikilink")
  - [罗莎·卢森堡](../Page/罗莎·卢森堡.md "wikilink")

### 两次大战间的社民党领袖

  - [弗里德里希·艾伯特](../Page/弗里德里希·艾伯特.md "wikilink")
  - [菲利普·谢德曼](../Page/菲利普·谢德曼.md "wikilink")
  - [古斯塔夫·鲍尔](../Page/古斯塔夫·鲍尔.md "wikilink")
  - [赫尔曼·穆勒](../Page/赫尔曼·穆勒.md "wikilink")

### 社民党主席

  - [奥古斯特·倍倍尔和](../Page/奥古斯特·倍倍尔.md "wikilink")[保罗·辛格尔](../Page/保罗·辛格尔.md "wikilink")
    1892-1911
  - [奥古斯特·倍倍尔和](../Page/奥古斯特·倍倍尔.md "wikilink")[胡戈·哈塞](../Page/胡戈·哈塞.md "wikilink")
    1911-1913
  - [弗里德里希·艾伯特和](../Page/弗里德里希·艾伯特.md "wikilink")[胡戈·哈塞](../Page/胡戈·哈塞.md "wikilink")
    1913-1916
  - [弗里德里希·艾伯特](../Page/弗里德里希·艾伯特.md "wikilink") 1916-1917
  - [弗里德里希·艾伯特和](../Page/弗里德里希·艾伯特.md "wikilink")[菲利普·谢德曼](../Page/菲利普·谢德曼.md "wikilink")
    1917-1919
  - [奥托·维尔斯和赫尔曼](../Page/奥托·维尔斯.md "wikilink")·穆勒 1919-1922
  - [亚瑟·克里斯平](../Page/亚瑟·克里斯平.md "wikilink")，[奥托·维尔斯和赫尔曼](../Page/奥托·维尔斯.md "wikilink")·穆勒
    1922-1928
  - [亚瑟·克里斯平和奥托](../Page/亚瑟·克里斯平.md "wikilink")·维尔斯 1928-1931
  - [亚瑟·克里斯平](../Page/亚瑟·克里斯平.md "wikilink")，奥托·维尔斯和[汉斯·沃格尔](../Page/汉斯·沃格尔.md "wikilink")
    1931-1933
  - 奥托·维尔斯和[汉斯·沃格尔流亡](../Page/汉斯·沃格尔.md "wikilink") 1933-1939
  - [汉斯·沃格尔](../Page/汉斯·沃格尔.md "wikilink")（Hans Vogel） 流亡 1939-1945
  - [库尔特·舒马赫](../Page/库尔特·舒马赫.md "wikilink")（Kurt Schumacher） 1946-1952
  - [埃里希·奥伦豪尔](../Page/埃里希·奥伦豪尔.md "wikilink")（Erich Ollenhauer）
    1952-1963
  - [维利·勃兰特](../Page/维利·勃兰特.md "wikilink") 1964-1987
  - [汉斯-约申·沃格尔](../Page/汉斯-约申·沃格尔.md "wikilink") 1987-1991
  - [布容·英格霍姆](../Page/布容·英格霍姆.md "wikilink")（Björn Engholm） 1991-1993
  - [约翰内斯·劳](../Page/约翰内斯·劳.md "wikilink") （代理） 1993
  - [鲁道夫·沙尔平](../Page/鲁道夫·沙尔平.md "wikilink") 1993-1995
  - [奥斯卡·拉方丹](../Page/奥斯卡·拉方丹.md "wikilink") 1995-1999
  - [格哈特·施罗德](../Page/格哈特·施罗德.md "wikilink") 1999-2004
  - [弗朗茨·明特费林](../Page/弗朗茨·明特费林.md "wikilink") 2004.3.21-2005.11.15
  - [马蒂亚斯·普拉策克](../Page/马蒂亚斯·普拉策克.md "wikilink") 2005.11.15-2006.4.10
  - [庫特·貝克](../Page/庫特·貝克.md "wikilink") 2006.4.10-2008.9.7
  - [弗朗茨·明特费林](../Page/弗朗茨·明特费林.md "wikilink") 2008.10.18-2009.11.13
  - [西格玛尔·加布里尔](../Page/西格玛尔·加布里尔.md "wikilink") 2009.11.13-2017.3.19
  - [马丁·舒尔茨](../Page/马丁·舒尔茨.md "wikilink") 2017.3.19-至今

### 社民党政府

  - 弗里德里希·艾伯特 1918
  - [菲利普·谢德曼](../Page/菲利普·谢德曼.md "wikilink") 1919
  - [古斯塔夫·鲍尔](../Page/古斯塔夫·鲍尔.md "wikilink") 1919－1920
  - 赫尔曼·穆勒 1920, 1928-1930
  - 维利·勃兰特 1969－1974
  - [赫尔莫特·施密特](../Page/赫尔莫特·施密特.md "wikilink") 1974－1982
  - [格哈特·施罗德](../Page/格哈特·施罗德.md "wikilink") 1998－2005

### 德国总统

  - [弗里德里希·艾伯特](../Page/弗里德里希·艾伯特.md "wikilink") 1919-1925
  - [古斯塔夫·海涅曼](../Page/古斯塔夫·海涅曼.md "wikilink") 1969-1974
  - [约翰内斯·劳](../Page/约翰内斯·劳.md "wikilink") 1999-2004
  - [弗兰克-瓦尔特·施泰因迈尔](../Page/弗兰克-瓦尔特·施泰因迈尔.md "wikilink") 2017-至今

## 注释

## 相关条目

  - [德国政治](../Page/德国政治.md "wikilink")
  - [德国政党](../Page/德国政党.md "wikilink")
  - [德国联邦议院](../Page/德国联邦议院.md "wikilink")
  - [爱德华·伯恩施坦](../Page/爱德华·伯恩施坦.md "wikilink")
  - [魏玛共和国](../Page/魏玛共和国.md "wikilink")

## 参考资料

## 外部链接

  - [社民党主页](http://www.spd.de/)

[Category:德國政黨](../Category/德國政黨.md "wikilink")
[德國社會民主黨](../Category/德國社會民主黨.md "wikilink")
[Category:社會民主主義政黨](../Category/社會民主主義政黨.md "wikilink")
[Category:歐洲社會黨成員](../Category/歐洲社會黨成員.md "wikilink")
[Category:1863年建立的政黨](../Category/1863年建立的政黨.md "wikilink")
[Category:1863年德國建立](../Category/1863年德國建立.md "wikilink")

1.  魏玛共和国宪法规定的《授权法》，授权法规定总理可以不通过议会自行制订规章以代替法律，可是授权法需要国会三分之二的多数议员通过才能生效，在魏玛共和国的历史上，只在1923年经济危机时启用一次。
2.
3.  [哥德斯堡纲领以及1989年社会民主党基本纲领](http://zhidao.baidu.com/share/aa93fde238f4223a57866ed3ab6646e3.html)
4.  [德国社民党1989年纲领](http://www.spd.de/linkableblob/1812/data/berliner_programm.pdf)
    :"In der sozialdemokratischen Partei Deutschlands arbeiten Menschen
    verschiedener Grundüberzeugungen und Glaubenshaltungen zusammen.
    Ihre Übereinstimmung beruht auf gemeinsamen Grundwerten und gleichen
    politischen Zielen. Der Demokratische Sozialismusin Europa hat seine
    geistigenWurzeln im Christentum und in der humanistischen
    Philosophie, in der Aufklärung, in Marxscher Geschichts- und
    Gesellschaftslehre und in den Erfahrungen der Arbeiterbewegung. Die
    Ideen der Frauenbefreiung sind bereitsim 19. Jahrhundert von der
    Arbeiterbewegung aufgenommen und weiterentwickelt worden.Wir haben
    mehr als 100 Jahre gebraucht, diese Ideen wirksam werden zu
    lassen.Wir begrüßen und achten persönliche Grundüberzeugungen und
    Glaubenshaltungen. Sie können niemals Parteibeschlüssen unterworfen
    sein."
5.  [德国社民党2007年纲领](http://www.spd.de/linkableblob/1778/data/hamburger_programm.pdf)
    13页：“Die Sozialdemokratie war von Anbeginn die Demokratiepartei. Sie
    hat die politische Kultur unseres Landes entscheidend geprägt. In
    ihr arbeiten Frauen und Männer unterschiedlicher Herkunft,
    verschiedener religiöser und weltanschaulicher Überzeugungen
    zusammen. Sie verstehen sich seit dem Godesberger Programm von 1959
    als linke Volkspartei, die ihre Wurzeln in Judentum und Christentum,
    Humanismus und Aufklärung, marxistischer Gesellschaftsanalyse und
    den Erfahrungen der Arbeiterbewegung hat. Die linke Volkspartei
    verdankt wichtige Impulse der Frauenbewegung und den neuen sozialen
    Bewegungen.”
6.  [德国社民党主席普拉策克因健康原因辞职](http://news.xinhuanet.com/newscenter/2006-04/10/content_4408278.htm)
7.  <http://edition.cnn.com/2002/WORLD/europe/09/23/germany.0700/>