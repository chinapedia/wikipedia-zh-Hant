[Wesselburenkraut_19.06.2012_18-35-26.jpg](https://zh.wikipedia.org/wiki/File:Wesselburenkraut_19.06.2012_18-35-26.jpg "fig:Wesselburenkraut_19.06.2012_18-35-26.jpg")
[Zuurkool_op_bord.jpg](https://zh.wikipedia.org/wiki/File:Zuurkool_op_bord.jpg "fig:Zuurkool_op_bord.jpg")\]\]
**德国酸菜**（[德文](../Page/德文.md "wikilink")：**Sauerkraut**；；）是[德国的传统食品](../Page/德国.md "wikilink")，用[圆白菜或](../Page/圆白菜.md "wikilink")[大头菜醃製](../Page/大头菜.md "wikilink")。现常见于德国和[美国的](../Page/美国.md "wikilink")[威斯康辛州](../Page/威斯康辛州.md "wikilink")。常用於搭配肉類產品如[香腸或](../Page/香腸.md "wikilink")[德国猪脚](../Page/德国猪脚.md "wikilink")\[1\]，也常被用来制作[鲁宾三明治](../Page/鲁宾三明治.md "wikilink")。\[2\]

## 历史

发酵食品在各个文化中都有很长的历史，如今最有名的发酵白菜的例子是德国酸菜和韩国泡菜。一世紀時古羅馬時代就有食用此發酵類蔬菜的紀錄，罗马作家卡托在他的著作《农业论》中就提到用盐来腌白菜和萝卜，加以保存。現在不知為何傳言德国酸菜是在卡托著作的一千年、成吉思汗入侵中国后，才由鞑靼人把它带在自己的马鞍从中国引入到欧洲的。此論點明顯與歐洲原本存在記錄的醃菜歷史矛盾。它於16世紀到18世紀之間在东欧和日耳曼菜肴里扎根，但也传到了其他国家，包括法国（在法国则叫「choucroute」，意即「酸菜」）。\[3\]

## 保健功效

[醫師表示德國酸菜之成分可防治](../Page/醫師.md "wikilink")[癌症](../Page/癌症.md "wikilink")，但每日勿攝取超過百[克](../Page/克.md "wikilink")，否则反而可能致癌。\[4\]

## 參見

  - [酸菜](../Page/酸菜.md "wikilink")
  - [韓國泡菜](../Page/韓國泡菜.md "wikilink")
  - [鲁宾三明治](../Page/鲁宾三明治.md "wikilink")
  - [德国猪脚](../Page/德国猪脚.md "wikilink")

## 參考

## 連結

  - [RichCat 山羊居: Kraut
    德國酸菜](http://richcat-richcat.blogspot.tw/2014/06/kraut.html)

[Category:德国食品](../Category/德国食品.md "wikilink")
[Category:醃製蔬菜](../Category/醃製蔬菜.md "wikilink")
[Category:發酵食品](../Category/發酵食品.md "wikilink")

1.
2.
3.
4.