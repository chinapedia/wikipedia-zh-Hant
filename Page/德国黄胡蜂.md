**德國黃胡蜂**（*Vespula
germanica*）是在大部份[北半球地區都能見到的](../Page/北半球.md "wikilink")[黃蜂](../Page/黃蜂.md "wikilink")，原住於[歐洲](../Page/歐洲.md "wikilink")、[北非及](../Page/北非.md "wikilink")[亞洲的](../Page/亞洲.md "wikilink")[溫帶](../Page/溫帶.md "wikilink")。牠們被引入其他地方，如[北美洲](../Page/北美洲.md "wikilink")、[澳洲及](../Page/澳洲.md "wikilink")[新西蘭](../Page/新西蘭.md "wikilink")。德國黃胡蜂屬於[胡蜂科](../Page/胡蜂科.md "wikilink")，並有時被誤指是[胡蜂屬的一種](../Page/胡蜂屬.md "wikilink")。

## 特徵

[Vespula_germanica01.jpg](https://zh.wikipedia.org/wiki/File:Vespula_germanica01.jpg "fig:Vespula_germanica01.jpg")
德國黃胡蜂約有13毫米長，呈典型的黃黑色。牠們的外形很像[普通黃胡蜂](../Page/普通黃胡蜂.md "wikilink")，但從[頭部觀看](../Page/頭部.md "wikilink")，可見牠們的臉上有三個細小的黑點。德國黃胡蜂在[腹部亦有黑點](../Page/腹部.md "wikilink")，但普通黃胡蜂有類似的斑紋並與黑環連接，形成另一種圖樣。

## 蜂巢

德國黃胡蜂的蜂巢是以咀嚼後的[植物纖維](../Page/植物.md "wikilink")，加上[唾液組成](../Page/唾液.md "wikilink")。蜂巢一般會築在接近地面，而非灌木及樹上。它的巢房是開放式的，並有巢柄連接蜂巢及底部。德國黃胡蜂會在巢柄周圍分泌一種可以抗拒[螞蟻的](../Page/螞蟻.md "wikilink")[化學物質](../Page/化學物質.md "wikilink")，以避免牠們的侵襲。

德國黃胡蜂的蜂后會獨自開始建立蜂巢，在第一次產卵前會完成20-30個巢房。蜂后會視乎氣候的情況，一般在[春天](../Page/春天.md "wikilink")，留在一根葉柄上，並開始建立一個蜂房。接著在此蜂房上另外再建立6個蜂房，形成典型的[六角形](../Page/六角形.md "wikilink")。

當幼蟲長成[工蜂後](../Page/工蜂.md "wikilink")，牠們會肩負尋找食物、照顧幼蟲／卵及蜂巢修理的工作。完成的蜂巢可能闊20-30厘米，當中有達3000隻德國黃胡蜂。

每一個德國黃胡蜂的蜂群中有一隻蜂后及多隻工蜂。蜂群一般只會存在一年，除了蜂后外，其他的工蜂都會在[冬天前](../Page/冬天.md "wikilink")[死亡](../Page/死亡.md "wikilink")。但是在較溫和的地方，如[新西蘭](../Page/新西蘭.md "wikilink")，到了冬天仍會有約10%的德國黃胡蜂生存。新的蜂后及雄蜂會在[夏天末出生](../Page/夏天.md "wikilink")，[交配過後](../Page/交配.md "wikilink")，蜂后會在石隙或其他有蓋地方過冬。

德國黃胡蜂會捕捉其他[昆蟲及](../Page/昆蟲.md "wikilink")[毛蟲來餵養其幼蟲](../Page/毛蟲.md "wikilink")，所以牠們一般而言是益蟲。成蟲吃[花蜜及甜的](../Page/花蜜.md "wikilink")[果實](../Page/果實.md "wikilink")，且會吃[人類的食物及渣滓](../Page/人類.md "wikilink")，尤其是[肉類](../Page/肉類.md "wikilink")。

[蜂鷹會侵襲及挖開蜂巢](../Page/蜂鷹.md "wikilink")，吃當中的幼蟲。[蜂蚜蠅屬的](../Page/蜂蚜蠅屬.md "wikilink")*Volucella
pellucens*及其親屬會產卵在蜂巢內，讓其幼蟲出生時吃德國黃胡蜂的幼蟲。

## 害蟲

德國黃胡蜂與[普通黃胡蜂及](../Page/普通黃胡蜂.md "wikilink")[馬蜂屬的兩個](../Page/馬蜂屬.md "wikilink")[物種在](../Page/物種.md "wikilink")[新西蘭都被認為是害蟲](../Page/新西蘭.md "wikilink")。德國黃胡蜂可能是於19世紀末引進新西蘭，於約1940年前大量減少。\[1\]牠們在[櫸木林中很普遍](../Page/櫸木.md "wikilink")，因為牠們吃樹皮上昆蟲所分泌的[蜜露](../Page/蜜露.md "wikilink")。這使[森林的蜜露大量減少](../Page/森林.md "wikilink")，令原住的[鳥類失去食糧](../Page/鳥類.md "wikilink")，影響當中的[生態](../Page/生態.md "wikilink")。

德國黃胡蜂的蜂巢在室內亦十分之大，甚至整個閣樓都是其蜂巢。這可能與新西蘭較溫暖的冬季有關，與一般歐洲的環境有很大的分別。被蛰后皮肤会起红包但没有毒。

## 參考

## 外部連結

  - [Ohio State University Yellowjacket fact
    sheet](http://ohioline.osu.edu/hyg-fact/2000/2075.html)
  - [Harvard University fact sheet on
    yellowjackets](https://web.archive.org/web/20080517055930/http://www.uos.harvard.edu/ehs/pes_yellowjackets.shtml)
  - [Differences between Yellowjackets and
    Hornets](https://web.archive.org/web/20060914090728/http://www.wvu.edu/~agexten/ipm/insects/hpm7002.pdf)
  - [Stunning photographs of Yellowjackets (and other insects) in
    flight](http://www.insektenflug.de)

[Category:黑胡蜂屬](../Category/黑胡蜂屬.md "wikilink")

1.  [Pest Animal
    Control](http://www.ebop.govt.nz/land/media/pdf/Fact_Sheet_PA03.pdf)
     Bay of Plenty environment report. Retrieved 7 January 2007