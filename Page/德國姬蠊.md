**德國-{}-姬蠊**（學名：），亦作**德國-{}-小蠊**，俗稱**德國蟑螂**，是一個[蟑螂的](../Page/蟑螂.md "wikilink")[物种](../Page/物种.md "wikilink")，身長多為1.0到1.6厘米\[1\]\[2\]\[3\]，比[美洲家蠊小](../Page/美洲家蠊.md "wikilink")。顏色從淺棕色到接近黑色的深棕色，而且在其[前胸有兩條由頭部至翅膀末端的直紋](../Page/前胸_\(昆蟲\).md "wikilink")。飛行距離並不遠，只有在被搔擾時才會飛\[4\]。

這種蟑螂是最常見的家居蟑螂之一，屬於本目物種少數的[有害生物](../Page/有害生物.md "wikilink")，在全球不少有人居住的地方均有機會找到
\[5\]，但在寒冷的地區則較少。人工养殖只需要2个月即可繁殖一个世代，成虫寿命约为200天\[6\]。本物種與密切相關，對於不經意的觀察者來說，兩者看起來幾乎相同，可能彼此誤認。然而，亞洲蟑螂會被光吸引，而且可以像飛蛾一樣飛翔，但德国小蠊卻不能。

## 生物學及害處

德国小蠊很容易在建築物裡紮根，但在電器如微波爐,咖啡機,[餐馆](../Page/餐馆.md "wikilink")、[食品加工业設施](../Page/食品加工业.md "wikilink")、[酒店及](../Page/酒店.md "wikilink")等機構設施裡尤為常見。牠們會在裡面建造巢穴，具群居性,且繁殖速度快，每個卵囊可孵出多隻若蟲，這些若蟲由出生至交配期的所需時間短暫，因此人們難以將之徹底驅除。不過在寒冷氣候環境，牠們只會在人類聚居處出現，因為嚴寒的環境令牠們難以存活，在室外牠們很快就會死去。儘管如此，德国小蠊目前已知會出現的最北處位於[加拿大](../Page/加拿大.md "wikilink")[努納武特地區的](../Page/努納武特地區.md "wikilink")[阿勒特](../Page/阿勒特.md "wikilink")\[7\]；而最南的地區在[阿根廷](../Page/阿根廷.md "wikilink")[巴塔哥尼亞的南部](../Page/巴塔哥尼亞.md "wikilink")\[8\]。

本物種最初被認為是歐洲的原生種，後來認為很可能是從[非洲東北部的](../Page/非洲.md "wikilink")[埃塞俄比亚首度出現](../Page/埃塞俄比亚.md "wikilink")，再擴散至其他地區\[9\]\[10\]，不過最新的證據顯示德国小蠊其實是起源於東南亞\[11\]\[12\]。但不管這物種起源於甚麼地方，德国小蠊對寒冷氣候的敏感，反映出這個物種應該起源於較溫暖的氣候環境，然後在古代時由於透過遷進人類的居所成為家居有害生物，繼而隨同人類遷移和運輸而擴散至其他地區。該物種現在，在除南極洲之外的所有大陸上以及在許多主要島嶼以家居有害生物的形式存活。因此，牠們在不同地區文化中被賦予各種名稱。例如，雖然牠們在英語國家被廣泛稱為「德國蟑螂」，但在德國卻被稱為「俄羅斯蟑螂」\[13\]。

作為一種[夜行性物種](../Page/夜行性.md "wikilink")，德国小蠊偶爾會在白天出現，特別是當其數量過於擁擠，又或受到干擾。然而，這些時刻通常都在傍晚出現，特別是當有人突然在一間昏暗的廢棄房間時亮燈，例如當牠們剛巧在清理廚房的廚餘時最為常見\[14\]。當被刺激或受到驚嚇時，該物種會散發出令人不快的氣味。

## 食性

德國蟑螂為一種[雜食性](../Page/雜食動物.md "wikilink")[食腐昆蟲](../Page/食腐動物.md "wikilink")，尤其喜歡吃[肉類](../Page/肉類.md "wikilink")、[澱粉](../Page/澱粉.md "wikilink")、[糖類食物及各種油脂](../Page/糖.md "wikilink")。當食物短缺時，牠們會連肥皂、膠水、牙膏，甚或連[潤滑油也會吃](../Page/潤滑油.md "wikilink")。當真正饑荒時，還會同類相食，咬吃其他同類的翅膀和腿\[15\]。
行動方面，這種蟑螂多數會於夜間活動，但在日間如果受到干擾或聯群結隊的情況下也會出現。

## 繁殖

另外雌性的德国小蠊在產卵期間不會即時把卵囊排出，而是留在體內孵化一段時間，使人們不會發現牠們的卵囊，而將之清除。此外牠們的身型也比其他品種的小，可躲在狹窄的縫隙中，從而避過人們的攻擊。除人們家中飼養的寵物外，這種蟑螂在人類居住地方，其自然天敵非常少。

The German cockroach reproduces faster than any other residential
cockroach,\[16\] growing from egg to reproductive adult in roughly 50 –
60 days.\[17\] 人工养殖只需要2个月即可繁殖一个世代，成虫寿命约为200天\[18\]。 Once fertilized, a
female German cockroach develops an  in her abdomen. The abdomen swells
as her eggs develop, until the translucent tip of the ootheca begins to
protrude from the end of her abdomen, and by that time the eggs inside
are fully sized, about 1/4 inch long with 16 segments. The ootheca, at
first translucent, soon turns white and then within a few hours it turns
pink, progressively darkening until, some 48 hours later, it attains the
dark red-brown of the shell of a chestnut. The ootheca has a keel-like
ridge along the line where the young emerge, and curls slightly towards
that edge as it completes its maturation. A small percentage of the
nymphs may hatch while the ootheca is still attached to the female, but
the majority emerge some 24 hours after it has detached from the
female's body. The newly hatched 3-mm-long black nymphs then progress
through six or seven s before becoming sexually mature, but  is such a
hazardous process that nearly half the nymphs die of natural causes
before reaching adulthood. Molted skins and dead nymphs are soon eaten
by living nymphs present at the time of molting.\[19\]

## 蟲害控制

The German cockroach is very successful at establishing an
[生态位](../Page/生态位.md "wikilink") in buildings, and is resilient in
the face of many [病虫害防治](../Page/病虫害防治.md "wikilink") measures. Reasons
include:

  - lack of natural [捕食s](../Page/捕食.md "wikilink") in a human habitat
  - prolific reproduction
  - short reproductive cycle
  - the ability to hide in very small refuges
  - sexual maturity attained within several weeks, and
  - adaptation and resistance to some [农药](../Page/农药.md "wikilink")

German cockroaches are [趨性](../Page/趨性.md "wikilink"), meaning they
prefer confined spaces, and they are small compared to other pest
species, so they can hide within small cracks and crevices that are easy
to overlook, thereby evading humans and their eradication efforts.
Conversely, the seasoned pest controller is alert for cracks and
crevices where it is likely to be profitable to place baits or spray
surfaces.

To be effective, control measures must be comprehensive, sustained, and
systematic; survival of just a few eggs is quite enough to regenerate a
nearly exterminated pest population within a few generations, and
recolonisation from surrounding populations often is very rapid,
too.\[20\]

Another problem in controlling German cockroaches is the nature of their
population behaviour. Though they are not social and practise no
organised maternal care, females carry oothecae of 18-50 eggs (average
about 32) during incubation until just before hatching, instead of
dropping them as most other species of cockroaches do. This protects the
eggs from certain classes of predation. Then, after hatching, nymphs
largely survive by consuming excretions and moults from adults, thereby
establishing their own internal microbial populations and avoiding
contact with most insecticidal surface treatments and baits. One
effective control is insect growth regulators (hydroprene, methoprene,
etc.), which act by preventing molting, thus prevent maturation of the
various instars. Caulking baseboards and around pipes may prevent the
travel of adults from one apartment to another within a building.

[Blatella_germanica_cdc.jpg](https://zh.wikipedia.org/wiki/File:Blatella_germanica_cdc.jpg "fig:Blatella_germanica_cdc.jpg")
As an [適應](../Page/適應.md "wikilink") consequence of pest control by
poisoned sugar baits, a strain of German cockroaches has emerged that
reacts to glucose as distastefully bitter. They refuse to eat sweetened
baits, which presents an obstacle to their control, given that baits are
an economical and effective means of control. It also is a dramatic
illustration of adaptive selection; in the absence of poisoned sweet
baits, attraction to sugars strongly promotes growth, energy, and
reproduction; cockroaches that are not attracted to sugars take longer
to grow and reproduce, whereas in the presence of poisoned sugared
baits, sugar avoidance promotes reproduction.\[21\]

## 三種蟑螂的比較

<table>
<thead>
<tr class="header">
<th><p>蟑螂種類</p></th>
<th><p><strong>德國小蠊</strong></p></th>
<th><p><a href="../Page/东方蠊.md" title="wikilink">东方蠊</a></p></th>
<th><p><a href="../Page/美洲蟑螂.md" title="wikilink">美洲蟑螂</a> |-on|<strong>Size</strong></p></th>
<th><p>[22]</p></th>
<th><p>[23]</p></th>
<th><p>[24]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>喜好的溫度</p></td>
<td><p>[25]</p></td>
<td><p>[26]</p></td>
<td><p>[27]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>幼蟲成長期</p></td>
<td><p>54–215天<br />
()[28]</p></td>
<td><p>164–542天<br />
(at )[29]</p></td>
<td><p>150–360天<br />
(at )[30]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>壽命</p></td>
<td><p>約200天[31]</p></td>
<td><p>35–190天[32]</p></td>
<td><p>90–706天[33]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>飛行能力</p></td>
<td><p>極少[34]</p></td>
<td><p>無[35]</p></td>
<td><p>無[36]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 基因組

The genome of the German cockroach was published in February 2018 in
**.\[37\] The relatively large genome (2.0 Gb) harbours a very high
number of proteins, of which most notably one group of , called the , is
particularly numerous. These chemoreceptors possibly allow the German
cockroach to detect a broad range of chemical cues from toxins, food,
pathogens, and pheromones.\[38\]

## 參看

  - [蟑螂](../Page/蟑螂.md "wikilink")
  - [东方蠊](../Page/东方蠊.md "wikilink")

<!-- end list -->

  - ：一種在德國小蠊出現的。

## 註釋

## 參考文獻

18\. John Allen First Response Pest Control
[1](https://www.lewisvillepestcontrol.net/single-post/2017/10/21/German-Roach-Blatella-Germanica)

## 外部連結

  - [German
    cockroach](http://entomology.ifas.ufl.edu/creatures/urban/roaches/german.htm)
    on the [UF](../Page/佛罗里达大学.md "wikilink") /  Featured Creatures Web
    site

  - [German cockroach fact
    sheet](http://www.ento.psu.edu/extension/factsheets/german_cockroach.htm)
    from the [Penn State](../Page/宾夕法尼亚州立大学.md "wikilink") Extension

  -
[Category:蟑螂](../Category/蟑螂.md "wikilink")
[Category:住家中的害蟲](../Category/住家中的害蟲.md "wikilink")
[Category:都市動物](../Category/都市動物.md "wikilink")
[germanica](../Category/姬蠊屬.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.  Faúndez, E. I. & M. A. Carvajal. 2011. Blattella germanica
    (Linnaeus, 1767) (Insecta: Blattaria) en la Región de Magallanes.
    Boletín de Biodiversidad de Chile, 5: 50-55.

9.

10.

11.
12.

13.

14.

15.

16.

17. {<http://museumpests.net/wp-content/uploads/2014/03/German-Cockroach.pdf>
    Museumpests.net Accessed July 15, 2015}

18.
19.
20.
21.  ([summary at BBC
    News](https://www.bbc.co.uk/news/science-environment-22611143))

22.
23.

24.
25.

26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.

38.