**德川十六神將**（）是為了彰顯仕於[徳川家康](../Page/徳川家康.md "wikilink")，並有創立[江戶幕府最大功績的](../Page/江戶幕府.md "wikilink")16位武將，而出現的稱呼。在[江戶時代](../Page/江戶時代.md "wikilink")，[德川家康及十六神将的畫像一直存放在](../Page/德川家康.md "wikilink")[東照宮](../Page/東照宮.md "wikilink")，作為[信仰而受人敬仰](../Page/信仰.md "wikilink")。

## 概要

模仿[佛教繪畫的集體圖像](../Page/佛教.md "wikilink")，以供養和彰顯[戰國大名家為目的](../Page/戰國大名.md "wikilink")，於是製作出集體肖像畫。選擇「16」這個數字的理由不明，不過以「16」為名數的，有[賢劫十六尊](../Page/賢劫十六尊.md "wikilink")、[十六善神](../Page/十六善神.md "wikilink")、[十六羅漢](../Page/十六羅漢.md "wikilink")、[十六社等被宗教](../Page/十六社.md "wikilink")（特別是佛教）使用。而[酒井忠次](../Page/酒井忠次.md "wikilink")、[本多忠勝](../Page/本多忠勝.md "wikilink")、[榊原康政](../Page/榊原康政.md "wikilink")、[井伊直政](../Page/井伊直政.md "wikilink")4人就被特稱為「[德川四天王](../Page/德川四天王.md "wikilink")」，守護持有「東照大權現」神號的家康，因此應該是佛教的「[四天王](../Page/四天王.md "wikilink")」加上「[十二神將](../Page/十二神將.md "wikilink")」而合計成為「十六神將」。再加上12個功臣，合計有28人的「[德川二十八神將](../Page/德川二十八神將.md "wikilink")」，在[日光東照宮內受配祀](../Page/日光東照宮.md "wikilink")。人選的準則不明，不過大部分都是[三河時代開始出仕的家臣](../Page/三河.md "wikilink")，在領土擴張時期，與家康一同在戰場上戰鬥的武功派武將。被認為是在吏僚派治世的家臣抬頭時，為了向後世流傳創業時期的困苦和活躍而被選出。

## 成員

  - 德川四天王

<!-- end list -->

  - [酒井忠次](../Page/酒井忠次.md "wikilink")（1527年—1596年）
  - [本多忠勝](../Page/本多忠勝.md "wikilink")（1548年—1610年）
  - [榊原康政](../Page/榊原康政.md "wikilink")（1548年—1606年）
  - [井伊直政](../Page/井伊直政.md "wikilink")（1561年—1602年）

<!-- end list -->

  - 其他

<!-- end list -->

  - [米津常春](../Page/米津常春.md "wikilink")（1524年—1612年）
  - [高木清秀](../Page/高木清秀.md "wikilink")（1526年—1610年)
  - [內藤正成](../Page/內藤正成.md "wikilink")（1528年—1602年）
  - [大久保忠世](../Page/大久保忠世.md "wikilink")（1532年—1594年）
  - [大久保忠佐](../Page/大久保忠佐.md "wikilink")（1537年—1613年）
  - [蜂屋貞次](../Page/蜂屋貞次.md "wikilink")（1539年—1564年）或[植村家存](../Page/植村家存.md "wikilink")（家政）(1541年—1577年）
  - [鳥居元忠](../Page/鳥居元忠.md "wikilink")（1539年—1600年）
  - [鳥居忠廣](../Page/鳥居忠廣.md "wikilink")（？—1573年）
  - [渡邊守綱](../Page/渡邊守綱.md "wikilink")（1542年—1620年）
  - [平岩親吉](../Page/平岩親吉.md "wikilink")（1542年—1611年）
  - [服部正成](../Page/服部正成.md "wikilink")（1542年—1596年）
  - [松平康忠](../Page/松平康忠.md "wikilink")（1545年—1618年）或
    [松平家忠](../Page/松平家忠.md "wikilink")（1555年—1600年）

## 德川二十將

  - [大須賀康高](../Page/大須賀康高.md "wikilink")（1527年—1589年）
  - [板倉勝重](../Page/板倉勝重.md "wikilink")（1545年—1624年）
  - [戶田忠次](../Page/戶田忠次.md "wikilink")（1531年—1597年）
  - [水野忠重](../Page/水野忠重.md "wikilink")（1541年—1600年）
  - [渥美勝吉](../Page/渥美勝吉.md "wikilink")（1557年—？）
  - [安藤直次](../Page/安藤直次.md "wikilink")（1555年—1635年）
  - [酒井重忠](../Page/酒井重忠.md "wikilink")（1549年—1617年）
  - [松平定勝](../Page/松平定勝.md "wikilink")（1560年—1624年）
  - [本多俊正](../Page/本多俊正.md "wikilink")

## 相關條目

  - [德川氏](../Page/德川氏.md "wikilink")

[Category:德川家康](../Category/德川家康.md "wikilink")
[Category:三河國出身人物](../Category/三河國出身人物.md "wikilink")
[Category:名數16](../Category/名數16.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:日本人物并称](../Category/日本人物并称.md "wikilink")