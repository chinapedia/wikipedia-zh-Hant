**德川喜代姬**（，即生於[文政元年](../Page/文政.md "wikilink")，卒於[弘化元年](../Page/弘化.md "wikilink")），[日本](../Page/日本.md "wikilink")[江戶時代後期第](../Page/江戶時代.md "wikilink")5代[姬路藩藩主](../Page/姬路藩.md "wikilink")[酒井忠學正室](../Page/酒井忠學.md "wikilink")。幼名都子，院號謙光院。她是[德川幕府第](../Page/德川幕府.md "wikilink")11代將軍[德川家齊第](../Page/德川家齊.md "wikilink")25女，母親是家齊側室────土屋忠兵衛平知光養女於八重之方（[皆春院](../Page/皆春院.md "wikilink")）。\[1\]

1822年（文政5年）與酒井忠學訂立婚約，1831年（[天保](../Page/天保.md "wikilink")3年）3月輿入[姬路城](../Page/姬路城.md "wikilink")，這場政治婚姻成功使酒井家家格上昇。\[2\]婚後她以孝順公婆而獲得好名聲\[3\]。喜代姬和忠學的女兒喜曾姫於1834年（天保5年）3月1日誕生，長大後嫁予忠學的養子、後來的第6代姬路藩藩主[酒井忠寶為正室](../Page/酒井忠寶.md "wikilink")。

現在，姬路著名[和菓子](../Page/和菓子.md "wikilink")「玉椿」就是當時為忠學與喜代姬的婚禮而製作的\[4\]。

## 參考資料

<div class="references-small">

<references />

</div>

[Category:江戶時代女性](../Category/江戶時代女性.md "wikilink")
[Category:德川將軍家](../Category/德川將軍家.md "wikilink")
[Category:酒井氏](../Category/酒井氏.md "wikilink")
[Category:姬路藩](../Category/姬路藩.md "wikilink")

1.  [徳川将軍データ：11代 徳川家斉](http://wolfpac.press.ne.jp/tokugawa11.html)
    ，2009年1月18日驗證。
2.  [歴史の風景－播磨伝説異聞－寸翁が馳せた夢](http://www.aurora.dti.ne.jp/~atorasu/p05/essey312.html)，2009年1月18日驗證。
3.  [織豊～江戸時代の姫君 喜代姫](http://s-mizoe.hp.infoseek.co.jp/m286.html#mark25)
    ，2009年1月18日驗證。
4.  [株式会社伊勢屋本店](http://nttbj.itp.ne.jp/0792885155/index.html?Media_cate=populer&svc=1303)，2009年1月18日驗證。