[Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_01.jpg](https://zh.wikipedia.org/wiki/File:Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_01.jpg "fig:Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_01.jpg")
**德意志之角**，又称**德意志角**或**德国之角**（[德语](../Page/德语.md "wikilink")：），是位于[德国](../Page/德国.md "wikilink")[科布伦茨市](../Page/科布伦茨.md "wikilink")[摩泽尔河和](../Page/摩泽尔河.md "wikilink")[莱茵河交汇处的一块陆地](../Page/莱茵河.md "wikilink")，建有一座宏伟的[德意志帝国首任皇帝](../Page/德意志帝国.md "wikilink")[威廉一世铜制骑马](../Page/威廉一世_\(德国\).md "wikilink")[雕塑](../Page/雕塑.md "wikilink")。

## 地理

[Karte_koblenz_in_deutschland.PNG](https://zh.wikipedia.org/wiki/File:Karte_koblenz_in_deutschland.PNG "fig:Karte_koblenz_in_deutschland.PNG")
德意志之角位于[德国城市](../Page/德国.md "wikilink")[科布伦茨](../Page/科布伦茨.md "wikilink")，[摩泽尔河和](../Page/摩泽尔河.md "wikilink")[莱茵河的交汇处](../Page/莱茵河.md "wikilink")。德意志之角上最著名的建筑是[威廉一世的骑马雕塑](../Page/威廉一世_\(德国\).md "wikilink")，雕塑总高37米，其中骑士像高14米。

## 历史

### 来源

[Deutscheseck1875.png](https://zh.wikipedia.org/wiki/File:Deutscheseck1875.png "fig:Deutscheseck1875.png")
1216年，[特里尔的](../Page/特里尔.md "wikilink")[大主教和](../Page/大主教.md "wikilink")[选帝侯特奥德里希](../Page/选帝侯.md "wikilink")·冯·维德（Theoderich
von
Wied，约1170年—1242年3月28日）调遣[条顿骑士团来](../Page/条顿骑士团.md "wikilink")[科布伦茨](../Page/科布伦茨.md "wikilink")，并出于保卫当地[医院的考虑](../Page/医院.md "wikilink")，将当地的[教堂和医院赠予他们](../Page/教堂.md "wikilink")。条顿骑士团在[摩泽尔河和](../Page/摩泽尔河.md "wikilink")[莱茵河河口的领地建立了](../Page/莱茵河.md "wikilink")“德意志庄园”（德语：Deutschherrenhaus），作为条顿骑士团在科布伦茨的驻地，河口领地由此得名“德意志之角”。

### 威廉一世纪念雕塑

[Deutsches_Eck_LOC.jpeg](https://zh.wikipedia.org/wiki/File:Deutsches_Eck_LOC.jpeg "fig:Deutsches_Eck_LOC.jpeg")
1888年[德意志帝国的首任皇帝](../Page/德意志帝国.md "wikilink")[威廉一世去世以后](../Page/威廉一世_\(德国\).md "wikilink")，德意志帝国上下萌发出了为他建造纪念雕塑的想法，以感谢这位德意志皇帝在1864年、1866年和1871年的三场战争中为[德国统一所作的战斗](../Page/德国统一.md "wikilink")。德国各地纷纷申请在自己的城市建造他们皇帝的雕塑，科布伦茨也是其中之一，年轻的皇帝[威廉二世在](../Page/威廉二世_\(德国\).md "wikilink")1891年最终决定选择位于[莱茵兰的科布伦茨](../Page/莱茵兰.md "wikilink")，塑像的具体位置选择在[摩泽尔河和](../Page/摩泽尔河.md "wikilink")[莱茵河的交汇处](../Page/莱茵河.md "wikilink")。建造塑像所需的一百万[金马克由全国性的募捐筹集而得](../Page/金马克.md "wikilink")，1897年8月31日，威廉二世亲自为威廉一世雕塑揭幕，雕塑的设计者是德国建筑师布鲁诺·施米茨（Bruno
Schmitz，1858年11月21日—1916年4月27日），雕塑总高37米，其中骑马像高14米，雕塑中的威廉一世皇帝身着将军制服和迎风飘扬的大衣，身旁的天才一只手牵着骏马，另一只手拿着垫褥和皇帝的[皇冠](../Page/皇冠.md "wikilink")。雕塑前方雕刻有德意志帝国鹰鹫，它抓着蛇并喝叱着敌人，以此表现威廉一世的威严。雕塑底座上方刻着科布伦茨诗人马克斯·冯·申肯多夫（Max
von
Schenkendorf，1783年12月11日—1817年12月11日）“给祖国的春天问候”（德语：**）一诗中的最后两句：“只要团结和忠诚，帝国将永存不灭”（德语原文：**）。这座雕塑与同时代建造的其他威廉皇帝雕塑，构成了“威廉时代精神”，受到极大的好评。但是德意志帝国灭亡以后，批评之声接踵而至，[民主主义者认为它是对昏庸](../Page/民主主义.md "wikilink")[君主制度的](../Page/君主制度.md "wikilink")[个人崇拜](../Page/个人崇拜.md "wikilink")，[和平主义者将其看作是威廉](../Page/和平主义.md "wikilink")[军国主义和强权抱负的化身](../Page/军国主义.md "wikilink")\[1\]。

### 1945年后的纪念雕塑

[第二次世界大战即将结束前的](../Page/第二次世界大战.md "wikilink")1945年3月16日，威廉一世骑马雕塑被[美国军队的大炮严重损坏](../Page/美国.md "wikilink")。据史料记载，是[艾森豪威尔下令进行摧毁的](../Page/艾森豪威尔.md "wikilink")，因为他担心德国士兵以纪念雕塑为掩护进行反击\[2\]。在美国军队的炮火之下，散落的雕塑从底座上摔下掉进莱茵河，铜制雕塑最终被完全拆除并融化，仅存威廉皇帝的头像现存于科布伦茨的莱茵河中游博物馆中。

第二次世界大战后，[法国占领军曾计划彻底拆除雕塑的底座](../Page/法国.md "wikilink")，并用一座代表和平和人民友好交流的雕塑代替，但是由于资金不足，这一计划并没有付之实现。

1953年5月18日，由[德国联邦总统](../Page/德国联邦总统.md "wikilink")[特奥多尔·豪斯建议将残存的雕塑底座作为](../Page/特奥多尔·豪斯.md "wikilink")“德国统一的警示碑”。为了表达“德国统一”这个象征意义，雕塑底座上刻上了德国各州的州徽，包括战后德国被割让给[波兰而失去的](../Page/波兰.md "wikilink")[西里西亚和](../Page/西里西亚.md "wikilink")[东普鲁士](../Page/东普鲁士.md "wikilink")，从法国占领下回归的[萨尔州徽也在](../Page/萨尔.md "wikilink")4年后加入其中。损毁的骑马雕塑被替换上一面[德国国旗](../Page/德国国旗.md "wikilink")。1990年10月3日德国统一日当天，人们为州徽刻上了新的[联邦州的名字](../Page/德国联邦州.md "wikilink")。

[柏林墙倒塌后](../Page/柏林墙.md "wikilink")，雕塑旁竖立起三块柏林墙存留的水泥砖，上书“纪念在德国分裂中遇难的人们（1953年6月17日—1989年11月9日）”。成为“德国统一警示”的又一纪念。

### 两德统一，雕塑重建

1990年[两德统一后](../Page/两德统一.md "wikilink")，雕塑不再具有作为警示碑的作用，对于是否要重建这座威廉一世骑马雕塑，引起了激烈的争论，支持重建者认为骑马雕塑作为科布伦茨城市的象征将给旅游业带来正面效应，而反对重建者则批评当今仍对皇帝的敬拜不合时宜，而且威廉一世对[1848年三月革命的血腥镇压更使得他得到](../Page/1848年革命.md "wikilink")“霰弹亲王”的恶名。最后雕塑所在的[莱茵兰-普法尔茨州将是否重建的决定权交给科布伦茨市](../Page/莱茵兰-普法尔茨.md "wikilink")，而科布伦茨接受了当地出版商泰瑟恩夫妇表示的捐资，重建雕塑。新建的雕塑不再使用铜制，而是改用不锈的青铜。

1993年9月2日，新雕塑落成。9月2日这一天在德意志帝国时期是“[色当日](../Page/色当日.md "wikilink")”，以纪念1870年9月2日[普鲁士军队在](../Page/普鲁士.md "wikilink")[普法战争中于法国城市](../Page/普法战争.md "wikilink")[色当附近的决胜一战](../Page/色当.md "wikilink")，迫使法国皇帝[拿破仑三世投降被俘](../Page/拿破仑三世.md "wikilink")。

## 旅游和文化

[Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_02.jpg](https://zh.wikipedia.org/wiki/File:Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_02.jpg "fig:Koblenz_im_Buga-Jahr_2011_-_Deutsches_Eck_02.jpg")
德意志之角从它建造威廉一世骑马雕塑的那天起，吸引了大量的游客。2002年起它作为莱茵河中上游河谷的一部分，入选[联合国教科文组织的](../Page/联合国教科文组织.md "wikilink")[世界遗产名录](../Page/世界遗产.md "wikilink")。

威廉一世骑马雕塑所在的陆地，适合于各种大型露天活动，经常举办音乐会、露天节日、莱茵河上的烟花表演，自2005年起也是每年莱茵河中游[马拉松赛的终点](../Page/马拉松.md "wikilink")。德国[2006年世界杯足球赛期间](../Page/2006年世界杯足球赛.md "wikilink")，在德意志之角上竖立起一面大型投影墙，转播所有比赛。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [德意志之角](http://www.deutsche-schutzgebiete.de/deutsches_eck.htm) -
    德国自然保护区

[Category:德國地理](../Category/德國地理.md "wikilink")
[Category:雕塑作品](../Category/雕塑作品.md "wikilink")

1.  Kurt Tucholsky: *Denkmal am Deutschen Eck*.
2.  Heinz-Günther Borck (Hrsg) u.a.: *Vor 60 Jahren, Krieg und Frieden
    an Rhein und Mosel 1944-1945*, Koblenz 2005 (Veröffentlichungen der
    Landesarchivverwaltung Rheinland-Pfalz, Bd. 105), S. 40 f. und 104
    ISBN 3-931014-67-3