**德懷特·D·艾森豪號航空母艦**（**USS Dwight D. Eisenhower
CVN-69**），或簡稱為**艾森豪號航空母艦**，是[美國](../Page/美國.md "wikilink")[尼米茲級核動力航空母艦的二號艦](../Page/尼米茲級核動力航空母艦.md "wikilink")。艦名承襲自參加過[第二次世界大戰的美國第](../Page/第二次世界大戰.md "wikilink")34任[總統](../Page/美國總統.md "wikilink")[德懷特·艾森豪](../Page/德懷特·艾森豪.md "wikilink")，因此也與艾森豪總統一樣，經常被暱稱為「**艾克**」（Ike）。

## 歷史

1970年6月29日，位於[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[紐波特紐斯](../Page/紐波特紐斯.md "wikilink")（Newport
News, VI）的[紐波特紐斯造船廠](../Page/紐波特紐斯造船廠.md "wikilink")（Newport News
Shipbuilding）獲得一紙合同，正式開始艾森豪號的建造工程。該艦原本在起造時使用的是代表核動力攻擊航空母艦的「CVAN-69」之艦隻代號，但在1975年6月30日時，順應[美國海軍的艦種簡化政策將攻擊航空母艦併入航空母艦的類別](../Page/美國海軍.md "wikilink")，而改編號為CVN-69。1970年8月15日，艾森豪號的[龍骨正式安放](../Page/龍骨.md "wikilink")（[船體編號為](../Page/船體編號.md "wikilink")599），當時斥資6億7千9百萬[美元的預算以](../Page/美元.md "wikilink")2007年的標準來看，約合45億美元的水準。

艾森豪號於1975年10月11日正式受洗[下水](../Page/下水儀式.md "wikilink")，正式取代美國海軍在[二戰時代就已建造的老航艦](../Page/二戰.md "wikilink")[富蘭克林·羅斯福號](../Page/富蘭克林·羅斯福號航空母艦.md "wikilink")（USS
Franklin D. Roosevelt,
CV-42），當時主持下水典禮的贊助人[瑪咪·艾森豪](../Page/瑪咪·艾森豪.md "wikilink")（Mamie
Doud-Eisenhower）是艾森豪的遺孀。經過為期約兩年的測試後，該艦於1977年10月18日正式就役，首任艦長為[威廉·藍西](../Page/威廉·藍西.md "wikilink")（William
E.
Ramsey）。截至2007年為止，艾森豪號共歷經了13任的艦長之領導。艾森豪號就役首先就被指派至[美國海軍大西洋艦隊](../Page/美國海軍大西洋艦隊.md "wikilink")，並在1979年1月時首次部署至[地中海地區](../Page/地中海.md "wikilink")。

1990年8月，[伊拉克發動奇襲佔領了](../Page/伊拉克.md "wikilink")[科威特](../Page/科威特.md "wikilink")，艾森豪號是第一艘趕至[紅海馳援的航空母艦](../Page/紅海.md "wikilink")，也是歷史上第二艘曾經通過[蘇伊士運河的核動力航空母艦](../Page/蘇伊士運河.md "wikilink")。在伊拉克侵襲[沙烏地阿拉伯的期間](../Page/沙烏地阿拉伯.md "wikilink")，艾森豪號艦上的航空武力負責支援[聯合國對伊拉克的](../Page/聯合國.md "wikilink")[禁運](../Page/禁運.md "wikilink")，除此之外，它也參與了1991年時的[沙漠風暴行動](../Page/沙漠風暴行動.md "wikilink")（Operation
Desert Storm），這是成軍以來大部分的時間都只負責和平巡曳的艾森豪號，首次的實戰任務。

## 航艦戰鬥群

艾森豪號是美國海軍[第8航艦戰鬥群](../Page/航艦戰鬥群.md "wikilink")（CSG-8）的組成武力之一也是該戰鬥群的[旗艦](../Page/旗艦.md "wikilink")，負責搭載[第7艦載機聯隊](../Page/艦載機聯隊.md "wikilink")（CVW-7），也是[第28驅逐戰隊](../Page/驅逐戰隊.md "wikilink")（Destroyer
Squadron 28）指揮官的駐艦。整個第8航艦戰鬥群的成員包括了：

  - 第28驅逐戰隊（DESRON-28）
      - [碉堡山號飛彈巡洋艦](../Page/CG-52.md "wikilink")（USS Bunker Hill CG-52）
      - [雷米吉號飛彈驅逐艦](../Page/DDG-61.md "wikilink")（USS Ramage DDG-61）
      - [安齊奧號飛彈巡洋艦](../Page/CG-68.md "wikilink")（USS Anzio CG-68）
      - [梅森號飛彈驅逐艦](../Page/DDG-87.md "wikilink")（USS Mason DDG-87）
      - [紐波特紐斯號核子攻擊潛艇](../Page/SSN-750.md "wikilink")（USS Newport News
        SSN-750）
  - 第7艦載機聯隊（CVW-7）
      - 第103「海盜旗」[戰鬥攻擊中隊](../Page/戰鬥攻擊中隊.md "wikilink")（VFA-103 "Jolly
        Rogers"）
      - 第143「吐狗」戰鬥攻擊中隊（VFA-143 "Puking' Dogs"）
      - 第131「野貓」戰鬥攻擊中隊（VFA-131 "Wildcats"）
      - 第83「暴跳者」戰鬥攻擊中隊（VFA-83 "Rampagers"）
      - 第140「愛國者」[電子攻擊中隊](../Page/電子攻擊中隊.md "wikilink")（VAQ-140
        "Patriots"）
      - 第125「虎尾」[空中早期預警中隊](../Page/空中早期預警中隊.md "wikilink")（VAW-125
        "Tigertails"）
      - 第5「夜杓」[直昇機反潛中隊](../Page/直昇機反潛中隊.md "wikilink")（HS-5
        "Nightdippers"）
      - 第40「生皮鞭」[艦隊後勤支援中隊](../Page/艦隊後勤支援中隊.md "wikilink")（VRC-40
        "Rawhides"）

## 相關條目

  - [尼米茲級航空母艦](../Page/尼米茲級航空母艦.md "wikilink")
  - [美國海軍航空母艦列表](../Page/美國海軍航空母艦列表.md "wikilink")

## 外部連結

  - [美國海軍官方網站](http://www.navy.mil)
  - [艾森豪號官方網站](http://www.navy.mil/homepages/cvn69/)

## 参考资料

[分类:1975年下水](../Page/分类:1975年下水.md "wikilink")

[Category:核動力航空母艦](../Category/核動力航空母艦.md "wikilink")
[Category:尼米茲級核動力航空母艦](../Category/尼米茲級核動力航空母艦.md "wikilink")
[Category:1977年竣工的船只](../Category/1977年竣工的船只.md "wikilink")