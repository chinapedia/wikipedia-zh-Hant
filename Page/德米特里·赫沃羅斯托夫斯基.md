**德米特里·亞歷山德羅維奇·赫沃羅斯托夫斯基**（，），[俄羅斯籍](../Page/俄羅斯.md "wikilink")[男中音](../Page/男中音.md "wikilink")[歌劇唱家](../Page/歌劇.md "wikilink")。

赫沃羅斯托夫斯基出生於俄國[西伯利亞](../Page/西伯利亞.md "wikilink")[克拉斯諾亞爾斯克](../Page/克拉斯諾亞爾斯克.md "wikilink")，早年在當地的藝術學校學習歌唱，並在當地的歌劇院首次登台，飾演《[弄臣](../Page/弄臣_\(歌劇\).md "wikilink")》中蒙特羅內伯爵一角。1987年，霍式贏得俄國[格林卡音樂大賽](../Page/格林卡.md "wikilink")（the
Russian Glinka
Competition），次年贏得[圖盧茲歌唱大賽](../Page/圖盧茲.md "wikilink")（Toulouse
Singing
Competition）。1989年更打敗有地利優勢的[威爾士男中音](../Page/威爾士.md "wikilink")[布莱恩·特菲尔](../Page/布莱恩·特菲尔.md "wikilink")，奪得[加的夫](../Page/加的夫.md "wikilink")[BBC全球歌手大賽冠軍](../Page/BBC.md "wikilink")，一舉成名，同年便在[倫敦](../Page/倫敦.md "wikilink")，次年在[紐約各辦了首次個人音樂會](../Page/紐約.md "wikilink")。

赫沃羅斯托夫斯基在西方世界的首次登台，是1989年在[法國](../Page/法國.md "wikilink")[尼斯上演](../Page/尼斯.md "wikilink")[柴可夫斯基的](../Page/柴可夫斯基.md "wikilink")《[黑桃皇后](../Page/黑桃皇后.md "wikilink")》。在[義大利的首演是在](../Page/義大利.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")[鳳凰劇院上演同樣是柴可夫斯基筆下的](../Page/鳳凰劇院.md "wikilink")[尤金·奧涅金](../Page/尤金·奧涅金.md "wikilink")（），獲得空前成功，自此聲名遠播。1993年，赫沃羅斯托夫斯基終於在[美洲大陸首次出演歌劇](../Page/美洲大陸.md "wikilink")，參演當時[芝加哥歌劇院舞台上的](../Page/芝加哥歌劇院.md "wikilink")《[茶花女](../Page/茶花女.md "wikilink")》。

1995年，赫沃羅斯托夫斯基終於在[紐約](../Page/紐約.md "wikilink")[大都會歌劇院首次擔綱演出](../Page/大都會歌劇院.md "wikilink")，自此成為各大歌劇院的委託不斷，包括[倫敦皇家歌劇院](../Page/倫敦皇家歌劇院.md "wikilink")、[柏林國立歌劇院](../Page/柏林國立歌劇院.md "wikilink")、[斯卡拉大劇院和](../Page/斯卡拉大劇院.md "wikilink")[維也納國立歌劇院](../Page/維也納國立歌劇院.md "wikilink")。赫沃羅斯托夫斯基被認為是柴可夫斯基筆下的[叶甫盖尼·奥涅金的最佳詮飾者](../Page/叶甫盖尼·奥涅金_\(歌剧\).md "wikilink")。《[紐約時報](../Page/紐約時報.md "wikilink")》曾稱讚到霍氏就是為此角色而生的。

2017年，因腦腫瘤逝世於倫敦。

## 備註及參考資料來源

1.  Tommasini, A. (2007, February 12)

[Star Power, Charisma and Ardor in
'Onegin'](http://www.nytimes.com/2007/02/12/arts/music/12oneg.html?ex=1172466000&en=95f404056a79b74f&ei=5070).
Anthony Opera Review, *New York Times*.

<div class="references-small">

  - Warrack, J. & West, E. (1996). *The Concise Oxford Dictionary of
    Opera - 3rd Edition*. New York, NY: Oxford University Press. Page
    240. ISBN 0-19-280028-0.
  - Kennedy M.(2004). *The Concise Oxford Dictionary of Music - 4th
    Edition*. New York, NY: Oxford University Press. Page 354. ISBN
    0-19-860884-5.

</div>

## 外部連結

  - [歌迷網](http://www.hvorostovsky.com/)
  - [Dmitri
    Hvorostovsky](https://web.archive.org/web/20011122094506/http://www.geocities.com/onegin_new/)
  - [即將表演列表](https://web.archive.org/web/20070127000409/http://www.operabase.com/listart.cgi?name=hvorostovsky)

[Category:克拉斯诺亚尔斯克边疆区人](../Category/克拉斯诺亚尔斯克边疆区人.md "wikilink")
[Category:俄罗斯男歌手](../Category/俄罗斯男歌手.md "wikilink")
[Category:男中音](../Category/男中音.md "wikilink")