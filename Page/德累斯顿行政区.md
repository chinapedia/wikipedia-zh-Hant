| 行政区的位置                                                                                                                                                                                                                |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Lage_des_Regierungsbezirkes_Dresden_in_Deutschland.GIF](https://zh.wikipedia.org/wiki/File:Lage_des_Regierungsbezirkes_Dresden_in_Deutschland.GIF "fig:Lage_des_Regierungsbezirkes_Dresden_in_Deutschland.GIF") |
| 基本数据                                                                                                                                                                                                                  |
| 所属联邦州:                                                                                                                                                                                                                |
| 首府:                                                                                                                                                                                                                   |
| 面积:                                                                                                                                                                                                                   |
| 人口:                                                                                                                                                                                                                   |
| 人口密度:                                                                                                                                                                                                                 |
| 下设行政区划:                                                                                                                                                                                                               |
| 政府网站:                                                                                                                                                                                                                 |
| 行政区地图                                                                                                                                                                                                                 |
| [Sachsen_rbdresden.png](https://zh.wikipedia.org/wiki/File:Sachsen_rbdresden.png "fig:Sachsen_rbdresden.png")                                                                                                        |

**德累斯顿行政区**（Regierungsbezirk
Dresden）是[德国](../Page/德国.md "wikilink")[萨克森州的](../Page/萨克森州.md "wikilink")3个[行政区之一](../Page/行政区_\(德国\).md "wikilink")，位于该州的东部，首府[德累斯顿](../Page/德累斯顿.md "wikilink")。

## 行政区划

德累斯顿行政区下设8个县：

1.  [鲍岑县](../Page/鲍岑县.md "wikilink")（Landkreis），首府[鲍岑](../Page/鲍岑.md "wikilink")（Bautzen）
2.  [卡门茨县](../Page/卡门茨县.md "wikilink")（Landkreis
    Kamenz），首府[卡门茨](../Page/卡门茨.md "wikilink")（Kamenz）
3.  [勒鲍-齐陶县](../Page/勒鲍-齐陶县.md "wikilink")（Landkreis
    Löbau-Zittau），首府[齐陶](../Page/齐陶.md "wikilink")（Zittau）
4.  [迈森县](../Page/迈森县.md "wikilink")（Landkreis
    Meißen），首府[迈森](../Page/迈森.md "wikilink")（Meißen）
5.  [下西里西亚上劳西茨县](../Page/下西里西亚上劳西茨县.md "wikilink")（Niederschlesische
    Oberlausitzkreis），首府[尼斯基](../Page/尼斯基.md "wikilink")（Niesky）
6.  [里萨-格罗森海因县](../Page/里萨-格罗森海因县.md "wikilink")（Landkreis
    Riesa-Großenhain），首府[格罗森海因](../Page/格罗森海因.md "wikilink")（Großenhain）
7.  [萨克森施魏茨县](../Page/萨克森施魏茨县.md "wikilink")（Landkreis Sächsische
    Schweiz），首府[皮尔纳](../Page/皮尔纳.md "wikilink")（Pirna）
8.  [魏瑟里茨县](../Page/魏瑟里茨县.md "wikilink")（Weißeritzkreis），首府[迪波尔迪斯瓦尔德](../Page/迪波尔迪斯瓦尔德.md "wikilink")（Dippoldiswalde）

德累斯顿行政区下设3个直辖市：

1.  [德累斯顿市](../Page/德累斯顿.md "wikilink")（Dresden）
2.  [格尔利茨市](../Page/格尔利茨.md "wikilink")（Görlitz）
3.  [霍耶斯韦达市](../Page/霍耶斯韦达.md "wikilink")（Hoyerswerda）

[萨克森州](../Category/德国行政区.md "wikilink")
[行政区](../Category/萨克森州行政区划.md "wikilink")