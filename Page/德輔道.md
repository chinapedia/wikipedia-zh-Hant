[Des_Voeux_Road_Central_at_night.jpg](https://zh.wikipedia.org/wiki/File:Des_Voeux_Road_Central_at_night.jpg "fig:Des_Voeux_Road_Central_at_night.jpg")
[Desvoeuxchater1912.jpg](https://zh.wikipedia.org/wiki/File:Desvoeuxchater1912.jpg "fig:Desvoeuxchater1912.jpg")及[遮打道交界](../Page/遮打道.md "wikilink")，圖中可見舊[郵政總局](../Page/香港郵政總局.md "wikilink")，現址為[環球大廈](../Page/環球大廈.md "wikilink")\]\]
[Desvoeux1925.jpg](https://zh.wikipedia.org/wiki/File:Desvoeux1925.jpg "fig:Desvoeux1925.jpg")
[Desvoeuxroadcentral1955.jpg](https://zh.wikipedia.org/wiki/File:Desvoeuxroadcentral1955.jpg "fig:Desvoeuxroadcentral1955.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:德輔道中_Des_Voeux_Road_Central,_1960s.jpg "fig:缩略图")
[Des_Voeux_Road_West_Shek_Tong_Tsui_section_201211.JPG](https://zh.wikipedia.org/wiki/File:Des_Voeux_Road_West_Shek_Tong_Tsui_section_201211.JPG "fig:Des_Voeux_Road_West_Shek_Tong_Tsui_section_201211.JPG")），[西港中心方向](../Page/西港中心.md "wikilink")\]\]
[Des_Voeux_Road_Central_night_view_201607.jpg](https://zh.wikipedia.org/wiki/File:Des_Voeux_Road_Central_night_view_201607.jpg "fig:Des_Voeux_Road_Central_night_view_201607.jpg")

**德輔道中**（）及**德輔道西**（）是[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[中西區的主要](../Page/中西區_\(香港\).md "wikilink")[道路](../Page/道路.md "wikilink")，德輔道中與德輔道西並不連接，兩條道路的分隔處在[上環](../Page/上環.md "wikilink")，[電車路線沿德輔道中及德輔道西而行](../Page/香港電車.md "wikilink")。

在[第二次世界大戰之前的文件中](../Page/第二次世界大战.md "wikilink")，德輔的[英文名稱中的兩](../Page/英语.md "wikilink")[字母](../Page/英文字母.md "wikilink")「」多寫作「」。不過現時官方的寫法是。

## 歷史

德輔道西本來稱為**寶靈海旁西**，是第4任[香港總督](../Page/香港總督.md "wikilink")[寶靈所倡議的](../Page/寶寧.md "wikilink")[寶靈填海計劃下的產物](../Page/寶靈填海計劃.md "wikilink")。寶靈於1854年開始寶靈填海計劃。然而因為部分英國商人的反對，工程進展緩慢，最終只能在上環和[西環填海](../Page/西環.md "wikilink")，完成了寶靈海旁西。

第10任香港總督[德輔上任後](../Page/德輔.md "wikilink")，決定繼續進行填海計劃，是為[海旁填海計劃](../Page/海旁填海計劃.md "wikilink")。在1890年至1904年，中環填海得出57英畝土地，德輔道中為新建成的街道之一。1904年，寶靈海旁西和寶靈海旁中正式貫通。為了紀念德輔，寶靈海旁西更名德輔道西（「德輔」本身並非[英文姓氏](../Page/英文姓氏.md "wikilink")，而是[法文姓氏](../Page/法文姓氏.md "wikilink")）。[香港日佔時期](../Page/香港日佔時期.md "wikilink")，德輔道曾更名為**昭和通**。

## 德輔道中

德輔道中東面連接[金鐘道及](../Page/金鐘道.md "wikilink")[皇后大道中](../Page/皇后大道中.md "wikilink")，西面則至[西港城](../Page/西港城.md "wikilink")（上環街市）止，與安泰街相交。[上環街市電車總站亦設在此處](../Page/上環街市電車總站.md "wikilink")。西行車輛可經[摩利臣街至](../Page/摩利臣街.md "wikilink")[干諾道西](../Page/干諾道西.md "wikilink")，東行車輛則可由干諾道西經[急庇利街進入德輔道中](../Page/急庇利街.md "wikilink")。在干諾道西設有一段分隔道路，供車輛及電車前往德輔道西。在中環[皇后像廣場至](../Page/皇后像廣場.md "wikilink")[畢打街一段東行線及皇后像廣場至](../Page/畢打街.md "wikilink")[租庇利街一段西行線均屬](../Page/租庇利街.md "wikilink")[巴士及電車專線](../Page/巴士.md "wikilink")。德輔道中地下為港鐵[港島綫管道](../Page/港島綫.md "wikilink")。

22家香港註冊持牌銀行當中，有多達12家都將總部設於德輔道中，包括[匯豐銀行](../Page/匯豐銀行.md "wikilink")（門牌地址位於[皇后大道中](../Page/皇后大道中.md "wikilink")1號，但與德輔道中相鄰）、[渣打銀行](../Page/渣打銀行（香港）.md "wikilink")（4至4A號）、[東亞銀行](../Page/東亞銀行.md "wikilink")（10號）、[創興銀行](../Page/創興銀行.md "wikilink")（24號）、[富邦銀行](../Page/富邦銀行（香港）.md "wikilink")（38號）、[招商永隆銀行](../Page/招商永隆銀行.md "wikilink")（45號）、[集友銀行](../Page/集友銀行.md "wikilink")（78號）、[恒生銀行](../Page/恒生銀行.md "wikilink")（83號）、[大眾銀行](../Page/大眾銀行（香港）.md "wikilink")（120號）、[大生銀行](../Page/大生銀行.md "wikilink")（130-132號）、[南洋商業銀行](../Page/南洋商業銀行.md "wikilink")（151號）及[中信銀行](../Page/中信銀行（國際）.md "wikilink")（232號）。

<File:Hong> Kong Legislative Council Building.jpg|
[香港終審法院大樓](../Page/終審法院大樓.md "wikilink") <File:HK>
World Wide House.jpg|[環球大廈](../Page/環球大廈.md "wikilink")
<File:HK_ChinaInsuranceGroupBuilding.JPG>|[中保集團大廈](../Page/中保集團大廈.md "wikilink")
<File:Des> Voeux Road Central near Wing
On.jpg|德輔道中近[永安百貨上環總店](../Page/永安百貨.md "wikilink")
<File:HK> Sheung Wan OTB Des Voeux Road C 257.jpg
|[海外信託大廈](../Page/海外信託銀行.md "wikilink")
<File:Western> Market Overview
201008.jpg|[西港城](../Page/西港城.md "wikilink") <File:Des> Voeux
Road Central at Morrison Street (Hong
Kong).jpg|德輔道中近[摩利臣街的](../Page/摩利臣街.md "wikilink")[西港城電車總站](../Page/西港城電車總站.md "wikilink")
<File:HK> Des Voeux Road
Central.jpg|德輔道中近[上環](../Page/上環.md "wikilink")[林士街](../Page/林士街.md "wikilink")
<File:榮德押> Hong Kong Island.jpg|德輔道中近榮德大押

## 德輔道西

[Des_Voeux_Road_West.JPG](https://zh.wikipedia.org/wiki/File:Des_Voeux_Road_West.JPG "fig:Des_Voeux_Road_West.JPG")
德輔道西東面連接[干諾道西](../Page/干諾道西.md "wikilink")，前稱為[三角碼頭一帶](../Page/三角碼頭.md "wikilink")，經過海味店雲集的上環和[西營盤](../Page/西營盤.md "wikilink")、[西區警署](../Page/西區警署.md "wikilink")，至[屈地街一帶止](../Page/屈地街.md "wikilink")，西面連接的[堅彌地城海旁](../Page/堅彌地城海旁.md "wikilink")。介乎[皇后街與](../Page/皇后街.md "wikilink")[正街之間的一段德輔道西](../Page/正街.md "wikilink")，是[海味街的一部份](../Page/海味街.md "wikilink")。

<File:HK> Sai Ying Pun Island Pacific Hotel noon b.JPG
|[港島太平洋酒店](../Page/港島太平洋酒店.md "wikilink") <File:HK> Sai
Ying Pun Chiu Kwong Street Cross-border Bus a.jpg |
[朝光街](../Page/朝光街.md "wikilink")[港中跨境](../Page/跨境巴士.md "wikilink")[巴士站](../Page/巴士站.md "wikilink")
<File:Chong> Yip Centre Entrance
2016.jpg|[創業商場](../Page/創業商場.md "wikilink") <File:Hong>
Kong Plaza 1.jpg| [香港商業中心](../Page/香港商業中心.md "wikilink") <File:HK> STT
Des Voeux Road West 418 Pacific Plaza.JPG
|德輔道西[太平洋廣場](../Page/太平洋廣場.md "wikilink")
<File:Des> Voeux Road West 1.jpg|德輔道西近[水街](../Page/水街.md "wikilink")
<File:HK> Sai Ying Pun Des Voeux Road West Seafood Shops
2a.jpg|德輔道西海味街一景 <File:HK> Sai Ying Pun Tramway Gov of
the PRChina n Centre
Street.JPG|德輔道西[電車路一景](../Page/香港電車.md "wikilink")，背景的[藍色大廈為](../Page/藍色.md "wikilink")[香港中聯辦大樓](../Page/中央人民政府駐香港特別行政區聯絡辦公室.md "wikilink")
<File:HK> Sai Ying Pun Des Voeux Road West Sunday Bike sport
riders.JPG|德輔道西近西端
[File:Des_Voeux_Road_West_Sunset.jpg|在](File:Des_Voeux_Road_West_Sunset.jpg%7C在)[電車上觀看德輔道西](../Page/香港電車.md "wikilink")（近[屈地街](../Page/屈地街.md "wikilink")）日落的景象

## 民間構思行人及電車專用區方案

[Very_DVRC_green_lawn_20160925.jpg](https://zh.wikipedia.org/wiki/File:Very_DVRC_green_lawn_20160925.jpg "fig:Very_DVRC_green_lawn_20160925.jpg")實驗\]\]
早於2000年，因一次竹棚意外，香港規劃師學會向政府提出將德輔道中改劃為行人及電車專用區。不過到2003年，政府架構變為三司十一局後，方案未有再處理。

2014年4月，規劃師學會聯同城大及思匯政策研究所等建議，待[中環灣仔繞道落成後](../Page/中環灣仔繞道.md "wikilink")，將德輔道中改為行人專用區及只准電車行駛，加上全面綠化，將令整個中環大變身，提供高質素公共空間。\[1\]

到2015年9月，商界、學會及關注團體組成「德輔道中聯盟」，大力推動行人及電車專用區，焦點是將上環[西港城至中環](../Page/西港城.md "wikilink")[畢打街的一段德輔道中](../Page/畢打街.md "wikilink")，限制各類汽車駛入，以改善中環空氣質素。總部設於上環的[南豐集團董事總經理蔡宏興亦身支持](../Page/南豐集團.md "wikilink")，指專用區有利商業發展。聯盟有意籌款1000萬元為計劃做顧問研究，又擬向政府申請封路一天試行。

聯盟邀請了香港大學建築學院及美國哥倫比亞大學建築、規劃及歷史保護研究院，合作設計出多個發展構思，內容會分別於今年12月的「2015深港城市/建築雙城雙年展」及中區作巡迴展出。兩間大學的初步構思，將德輔道中分為6個行人小區，舉辦街頭嘉年華、劇場甚至變身做森林等。\[2\]

2016年9月25日，3個民間團體於早上10時至下午4時在德輔道中[文華里至](../Page/文華里.md "wikilink")[摩利臣街一段長](../Page/摩利臣街.md "wikilink")200多米的道路設立臨時行人專區，活動名為「非常（　）德」。超過40個團體在鋪上人造草皮的專區內舉行13個不同形式的主題活動，包括手工藝作坊、親子閱讀、以物易物、音樂/舞蹈表演、單車遊、遊戲攤位及踢足球等。團體希望讓公眾人士能體驗城市空間內，可創造不同可能。\[3\]

<File:Very> DVRC mock up seats 20160925.jpg|仿草製閒座區 <File:Very> DVRC
music performance 20160925.jpg|音樂表演 <File:Very> DVRC workshop
20160925.jpg|手工藝作坊 <File:Very> DVRC activities 20160925.jpg|單車競速賽

## 沿路著名地點

[Hong_Kong_Plaza_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Plaza_\(Hong_Kong\).jpg "fig:Hong_Kong_Plaza_(Hong_Kong).jpg")
[Pacific_Plaza_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Pacific_Plaza_\(Hong_Kong\).jpg "fig:Pacific_Plaza_(Hong_Kong).jpg")

  - [維壹](../Page/維壹.md "wikilink")
  - [香港商業中心](../Page/香港商業中心.md "wikilink")

均益大廈

  - [太平洋廣場](../Page/太平洋廣場.md "wikilink")
  - [創業中心](../Page/創業中心.md "wikilink")
  - [瑧璈](../Page/瑧璈.md "wikilink")
  - [華明中心](../Page/華明中心.md "wikilink")
  - [維港峰](../Page/維港峰.md "wikilink")
  - [華大盛品酒店](../Page/華大盛品酒店.md "wikilink")
  - [香港中聯辦](../Page/香港中聯辦.md "wikilink")
  - [西區警署](../Page/西區警署.md "wikilink")
  - [德輔道西207號](../Page/德輔道西207號.md "wikilink")
  - [港島太平洋酒店](../Page/港島太平洋酒店.md "wikilink")
  - [西營盤站](../Page/西營盤站.md "wikilink")
  - [普頓臺](../Page/普頓臺.md "wikilink")
  - [蓮香居](../Page/蓮香居.md "wikilink")
  - 宜必思香港中上環 (Hotel ibis Hong Kong Central and Sheung Wan)
  - [西港城](../Page/西港城.md "wikilink")
  - [上環站](../Page/上環站.md "wikilink")
  - [永安中心](../Page/永安中心.md "wikilink")
  - [無限極廣場](../Page/無限極廣場.md "wikilink")
  - [金龍中心](../Page/金龍中心.md "wikilink")
  - [南豐大廈](../Page/南豐大廈.md "wikilink")
  - [中保集團大廈](../Page/中保集團大廈.md "wikilink")
  - [中環中心](../Page/中環中心.md "wikilink")
  - [恒生銀行總行大廈](../Page/恒生銀行總行大廈.md "wikilink")
  - [中環至半山自動扶梯系統](../Page/中環至半山自動扶梯系統.md "wikilink")
  - [中環街市](../Page/中環街市.md "wikilink")
  - [永安集團大廈](../Page/永安集團大廈.md "wikilink")
  - [萬宜大廈](../Page/萬宜大廈.md "wikilink")
  - [環球大廈](../Page/環球大廈.md "wikilink")
  - [會德豐大廈](../Page/會德豐大廈.md "wikilink")
  - [置地廣場](../Page/置地廣場.md "wikilink")
  - [東亞銀行總行](../Page/東亞銀行.md "wikilink")
  - [歷山大廈](../Page/歷山大廈.md "wikilink")
  - [太子大廈](../Page/太子大廈.md "wikilink")
  - [皇后大道中九號](../Page/皇后大道中九號.md "wikilink")
  - [渣打銀行大廈](../Page/渣打銀行大廈.md "wikilink")
  - [香港匯豐銀行總行大廈](../Page/香港匯豐銀行總行大廈.md "wikilink")
  - [中環站](../Page/中環站.md "wikilink")
  - [皇后像廣場](../Page/皇后像廣場.md "wikilink")
  - [終審法院大樓](../Page/終審法院大樓.md "wikilink")
  - [遮打花園](../Page/遮打花園.md "wikilink")
  - [中國銀行大廈](../Page/中國銀行大廈.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[西營盤站](../Page/西營盤站.md "wikilink")、[上環站](../Page/上環站.md "wikilink")
  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[中環站](../Page/中環站.md "wikilink")

<!-- end list -->

  - [香港電車](../Page/香港電車.md "wikilink")

## 相關街道

  - [-{干}-諾道](../Page/干諾道.md "wikilink")
  - [皇后大道](../Page/皇后大道.md "wikilink")
  - [遮打道](../Page/遮打道.md "wikilink")
  - [金鐘道](../Page/金鐘道.md "wikilink")
  - [林士街](../Page/林士街.md "wikilink")
  - [急庇利街](../Page/急庇利街.md "wikilink")
  - [文華里](../Page/文華里.md "wikilink")
  - [屈地街](../Page/屈地街.md "wikilink")
  - [畢打街](../Page/畢打街.md "wikilink")
  - [高陞街](../Page/高陞街.md "wikilink")
  - [文咸街](../Page/文咸街.md "wikilink")
  - [東邊街](../Page/東邊街.md "wikilink")
  - [永樂街](../Page/永樂街.md "wikilink")

## 註釋

## 參考資料

  - 《[解密百年香港](../Page/解密百年香港.md "wikilink")——港督這份工》[第一節](https://web.archive.org/web/20070929131914/http://app.hkatv.com/webtv/control.php?program=100012)，2007年7月24日於[亞洲電視播映](../Page/亞洲電視.md "wikilink")

[Category:中西區街道 (香港)](../Category/中西區街道_\(香港\).md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  [學會倡德輔道中變行人區
    《蘋果日報》 2014年4月28日](http://hk.apple.nextmedia.com/news/art/20140428/18703110)
2.  [德輔道中變行人專區 團體籌千萬力撐
    《信報》 2015年9月22日](http://forum.hkej.com/node/125949)
3.  [【只行電車】德輔道中9.25變身行人專區　路上鋪仿草睇戲踢波
    《香港01》 2016年9月14日](http://www.hk01.com/%E7%A4%BE%E5%8D%80/43159/-%E5%8F%AA%E8%A1%8C%E9%9B%BB%E8%BB%8A-%E5%BE%B7%E8%BC%94%E9%81%93%E4%B8%AD9-25%E8%AE%8A%E8%BA%AB%E8%A1%8C%E4%BA%BA%E5%B0%88%E5%8D%80-%E8%B7%AF%E4%B8%8A%E9%8B%AA%E4%BB%BF%E8%8D%89%E7%9D%87%E6%88%B2%E8%B8%A2%E6%B3%A2)