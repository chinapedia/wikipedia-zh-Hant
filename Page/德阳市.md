**德阳市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[四川省下辖的](../Page/四川省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于四川省中部。市境西南邻[成都市](../Page/成都市.md "wikilink")，东南达[资阳市](../Page/资阳市.md "wikilink")、[遂宁市](../Page/遂宁市.md "wikilink")，东北临[绵阳市](../Page/绵阳市.md "wikilink")，西北界[阿坝州](../Page/阿坝州.md "wikilink")。地处[四川盆地西北部](../Page/四川盆地.md "wikilink")，西北部为[龙门山区](../Page/龙门山.md "wikilink")，中南部为[成都平原](../Page/成都平原.md "wikilink")，东部为川中丘陵区。[绵远河自西北往东南流经城区](../Page/绵远河.md "wikilink")，于南部与[石亭江](../Page/石亭江.md "wikilink")、[鸭子河汇合](../Page/鸭子河.md "wikilink")，后段称[沱江](../Page/沱江.md "wikilink")，东南部有[凯江](../Page/凯江.md "wikilink")、[郪江等](../Page/郪江.md "wikilink")[涪江支流](../Page/涪江.md "wikilink")。全市面积5,911平方公里，人口351.32万。德阳是全省重要的工业城市，以重型机械与磷矿工业为核心产业。著名的[三星堆遗址于](../Page/三星堆遗址.md "wikilink")[广汉市出土](../Page/广汉市.md "wikilink")，为[古蜀国的重要遗迹](../Page/古蜀国.md "wikilink")，现为[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

## 历史

[先秦前为](../Page/先秦.md "wikilink")[古蜀国地](../Page/古蜀国.md "wikilink")。[秦惠文王九年](../Page/秦惠文王.md "wikilink")（前316年），[秦灭蜀国建](../Page/秦.md "wikilink")[蜀郡](../Page/蜀郡.md "wikilink")，今境属之。[西汉](../Page/西汉.md "wikilink")[高帝六年](../Page/汉高帝.md "wikilink")（前201年）分[巴郡](../Page/巴郡.md "wikilink")、蜀郡置[广汉郡](../Page/广汉郡.md "wikilink")，治雒县乘乡（治今[旌阳区](../Page/旌阳区.md "wikilink")[孝泉镇](../Page/孝泉镇.md "wikilink")），属[益州](../Page/益州.md "wikilink")；又置[雒县](../Page/雒县.md "wikilink")（治今[广汉市](../Page/广汉市.md "wikilink")[北外镇](../Page/北外镇.md "wikilink")）、[绵竹县](../Page/绵竹县.md "wikilink")（治今旌阳区北）、[什方县](../Page/什方县.md "wikilink")（治今[什邡市](../Page/什邡市.md "wikilink")），皆属广汉郡。[东汉雒县为广汉郡治](../Page/东汉.md "wikilink")，兼为益州治。[三国](../Page/三国.md "wikilink")[蜀汉分绵竹县置](../Page/蜀汉.md "wikilink")[阳泉县](../Page/阳泉县.md "wikilink")（治今旌阳区孝泉镇），又置[五城县](../Page/五城县.md "wikilink")（治今[中江县](../Page/中江县.md "wikilink")），皆属广汉郡。[延熙年间分广汉郡置](../Page/延熙.md "wikilink")[东广汉郡](../Page/东广汉郡.md "wikilink")，五城县改属之。

[东晋置](../Page/东晋.md "wikilink")[万安县](../Page/万安县.md "wikilink")（治今[罗江区](../Page/罗江区.md "wikilink")）。[隆安二年](../Page/隆安.md "wikilink")（398年）置[晋熙县](../Page/晋熙县.md "wikilink")（治今[绵竹市](../Page/绵竹市.md "wikilink")），又于县置[晋熙郡](../Page/晋熙郡.md "wikilink")。[南朝宋改五城县为](../Page/南朝宋.md "wikilink")[伍城县](../Page/伍城县.md "wikilink")。[南朝梁万安县更名](../Page/南朝梁.md "wikilink")[潺亭县](../Page/潺亭县.md "wikilink")。[西魏潺亭县复名万安县](../Page/西魏.md "wikilink")，并于县置[万安郡](../Page/万安郡.md "wikilink")。[北周置](../Page/北周.md "wikilink")[玄武郡](../Page/玄武郡.md "wikilink")，治伍城县；省晋熙县、绵竹县。[闵帝时什方县更名](../Page/北周闵帝.md "wikilink")[方亭县](../Page/方亭县.md "wikilink")，[武帝时省方亭县入雒县](../Page/北周武帝.md "wikilink")。

[隋](../Page/隋朝.md "wikilink")[开皇元年](../Page/开皇.md "wikilink")（581年）废晋熙郡，复置晋熙县；废广汉郡，雒县属益州；废玄武郡，改伍城县为[玄武县](../Page/玄武县.md "wikilink")，属益州。开皇十八年（598年）晋熙县改名[孝水县](../Page/孝水县.md "wikilink")；雒县改名[绵竹县](../Page/绵竹县.md "wikilink")，徙治今广汉市。[仁寿初置](../Page/仁寿.md "wikilink")[凯州](../Page/凯州.md "wikilink")，玄武县为州治。[大业二年](../Page/大业.md "wikilink")（607年）废凯州；绵竹县复名雒县；孝水县改名[绵竹县](../Page/绵竹县.md "wikilink")，属蜀郡。

[唐](../Page/唐朝.md "wikilink")[武德三年](../Page/武德.md "wikilink")（620年）析雒县置**[德阳县](../Page/德阳县.md "wikilink")**（治今旌阳区）；复置[什邡县](../Page/什邡县.md "wikilink")，属[益州](../Page/益州.md "wikilink")。[调露元年](../Page/调露.md "wikilink")（679年）析[郪县](../Page/郪县.md "wikilink")、[飞鸟县置](../Page/飞鸟县.md "wikilink")[铜山县](../Page/铜山县.md "wikilink")（治今中江县[广福镇](../Page/广福镇.md "wikilink")），与玄武县同属[梓州](../Page/梓州.md "wikilink")。[垂拱二年](../Page/垂拱.md "wikilink")（686年）于雒县置[汉州](../Page/汉州.md "wikilink")，并划德阳、什邡、绵竹县来属。[天宝元年](../Page/天宝.md "wikilink")（742年）改万安县为[罗江县](../Page/罗江县.md "wikilink")，属[绵州](../Page/绵州.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[大中祥符五年](../Page/大中祥符.md "wikilink")（1012年）改玄武县为[中江县](../Page/中江县.md "wikilink")。

[蒙古](../Page/蒙古.md "wikilink")[中统元年](../Page/中统.md "wikilink")（1260年）省雒县入汉州。[元](../Page/元朝.md "wikilink")[至元八年](../Page/至元.md "wikilink")（1271年）升德阳县置[德州](../Page/德州.md "wikilink")。至元十三年（1276年）德州复改德阳县。至元二十年（1283年）省铜山县入中江县，属[潼川府](../Page/潼川府.md "wikilink")。元末[明玉珍](../Page/明玉珍.md "wikilink")[大夏政权复置雒县](../Page/大夏.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武四年](../Page/洪武.md "wikilink")（1371年）复省雒县入汉州。洪武十年（1377年）省什邡县入绵竹县。洪武十三年（1380年）复置什邡县。[清](../Page/清朝.md "wikilink")[顺治十六年](../Page/顺治.md "wikilink")（1659年）并罗江县入德阳县。[雍正五年](../Page/雍正.md "wikilink")（1727年）复置罗江县。[乾隆三十五年](../Page/乾隆.md "wikilink")（1770年）省罗江县入绵州，并移绵州治至。[嘉庆六年](../Page/嘉庆.md "wikilink")（1801年）绵州还旧治，复置罗江县。

[民国二年](../Page/民国.md "wikilink")（1913年）裁府州留县，改汉州为[广汉县](../Page/广汉县.md "wikilink")，与德阳、绵竹、罗江、什邡等县划属[川西道](../Page/川西道.md "wikilink")，次年改[西川道](../Page/西川道.md "wikilink")；中江县划属[川北道](../Page/川北道.md "wikilink")，次年改[嘉陵道](../Page/嘉陵道.md "wikilink")。民国十九年（1930年）废道制。民国二十四年（1935年）德阳、广汉、绵竹、罗江、什邡五县属四川省第十三行政督察区，中江县属第十二行政督察区。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，德阳、广汉、绵竹、罗江、什邡五县属[川西行署区](../Page/川西行署区.md "wikilink")[绵阳专区](../Page/绵阳专区.md "wikilink")，中江县属[川北行署区](../Page/川北行署区.md "wikilink")[遂宁专区](../Page/遂宁专区.md "wikilink")。1952年各专区由四川省直接领导，广汉、什邡二县改属[温江专区](../Page/温江专区.md "wikilink")。1958年中江县划归绵阳专区。1959年罗江县并入德阳县。1960年什邡县并入广汉县。1962年复置什邡县。1968年各专区改称地区。1983年3月3日，广汉、什邡二县划属[成都市](../Page/成都市.md "wikilink")；8月18日，析德阳县城区街道、[汉旺镇和旌阳](../Page/汉旺镇.md "wikilink")、城区、八角井三个公社，设立地级[德阳市](../Page/德阳市.md "wikilink")，将[绵阳地区的德阳](../Page/绵阳地区.md "wikilink")、中江、绵竹三县和成都市的广汉、什邡两县划归德阳市管辖；9月12日，撤销德阳县并入德阳市，并设立[市中区](../Page/市中区_\(德阳市\).md "wikilink")。1988年2月24日，撤销广汉县，设立县级[广汉市](../Page/广汉市.md "wikilink")。1995年10月27日，撤销什邡县，设立县级[什邡市](../Page/什邡市.md "wikilink")。1996年8月3日，撤销德阳市市中区，改设[旌阳区和](../Page/旌阳区.md "wikilink")[罗江县](../Page/罗江县.md "wikilink")；同年10月8日，撤销绵竹县，设立县级[绵竹市](../Page/绵竹市.md "wikilink")。2017年8月，国务院批复同意撤销罗江县，设立罗江区。

## 地理

属于[长江上游](../Page/长江.md "wikilink")[沱江流域](../Page/沱江.md "wikilink")。东北接[绵阳市](../Page/绵阳市.md "wikilink")，东南与[遂宁](../Page/遂宁.md "wikilink")、[资阳两市交界](../Page/资阳.md "wikilink")，西南连[成都市](../Page/成都市.md "wikilink")，西与[阿坝州接壤](../Page/阿坝州.md "wikilink")。距省会成都50余公里，是成都旅游门户圈的重要组成部分。

德阳地形西高东低，西北部为[山地](../Page/山地.md "wikilink")，中部为[平原](../Page/平原.md "wikilink")，东南部为[丘陵](../Page/丘陵.md "wikilink")，属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，四季分明，雨量充沛，年降雨量1000毫米左右，平均气温15.7－16.7℃。

## 政治

### 现任领导

<table>
<caption>德阳市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党德阳市委员会.md" title="wikilink">中国共产党<br />
德阳市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/德阳市人民代表大会.md" title="wikilink">德阳市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/德阳市人民政府.md" title="wikilink">德阳市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议德阳市委员会.md" title="wikilink">中国人民政治协商会议<br />
德阳市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/赵世勇.md" title="wikilink">赵世勇</a>[1]</p></td>
<td><p><a href="../Page/陈彬_(1962年).md" title="wikilink">陈彬</a>[2]</p></td>
<td><p><a href="../Page/何礼_(1965年).md" title="wikilink">何礼</a>[3]</p></td>
<td><p><a href="../Page/张万平.md" title="wikilink">张万平</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p>四川省<a href="../Page/营山县.md" title="wikilink">营山县</a></p></td>
<td><p>四川省<a href="../Page/南充市.md" title="wikilink">南充市</a></p></td>
<td><p>四川省<a href="../Page/大竹县.md" title="wikilink">大竹县</a></p></td>
<td><p>四川省<a href="../Page/金川县.md" title="wikilink">金川县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年5月</p></td>
<td><p>2017年1月</p></td>
<td><p>2018年7月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市辖2个[市辖区](../Page/市辖区.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管3个[县级市](../Page/县级市.md "wikilink")，是国家和四川省重点规划的百万人口大城市之一。

  - 市辖区：[旌阳区](../Page/旌阳区.md "wikilink")、[罗江区](../Page/罗江区.md "wikilink")
  - 县级市：[广汉市](../Page/广汉市.md "wikilink")、[什邡市](../Page/什邡市.md "wikilink")、[绵竹市](../Page/绵竹市.md "wikilink")
  - 县：[中江县](../Page/中江县.md "wikilink")

此外，[德阳经济技术开发区是德阳市设立的](../Page/德阳经济技术开发区.md "wikilink")[国家级经济技术开发区](../Page/国家级经济技术开发区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>德阳市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>510600</p></td>
</tr>
<tr class="odd">
<td><p>510603</p></td>
</tr>
<tr class="even">
<td><p>510604</p></td>
</tr>
<tr class="odd">
<td><p>510623</p></td>
</tr>
<tr class="even">
<td><p>510681</p></td>
</tr>
<tr class="odd">
<td><p>510682</p></td>
</tr>
<tr class="even">
<td><p>510683</p></td>
</tr>
<tr class="odd">
<td><p>注：旌阳区数字包含德阳经济技术开发区所辖旌东、八角井2街道。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>德阳市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>德阳市</p></td>
<td><p>3615758</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>旌阳区</p></td>
<td><p>735070</p></td>
<td><p>20.33</p></td>
</tr>
<tr class="even">
<td><p>中江县</p></td>
<td><p>1186762</p></td>
<td><p>32.82</p></td>
</tr>
<tr class="odd">
<td><p>罗江区</p></td>
<td><p>212185</p></td>
<td><p>5.87</p></td>
</tr>
<tr class="even">
<td><p>广汉市</p></td>
<td><p>591115</p></td>
<td><p>16.35</p></td>
</tr>
<tr class="odd">
<td><p>什邡市</p></td>
<td><p>412758</p></td>
<td><p>11.42</p></td>
</tr>
<tr class="even">
<td><p>绵竹市</p></td>
<td><p>477868</p></td>
<td><p>13.22</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")3615759人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共减少172297人，减少4.55%，年平均减少0.46%。其中，男性人口为1827143人，占50.53%；女性人口为1788616人，占49.47%。总人口性别比（以女性为100）为102.15。0－14岁人口为470307人，占13.01%；15－64岁人口为2723981人，占75.33%；65岁及以上人口为421471人，占11.66%。[汉族人口为](../Page/汉族.md "wikilink")3606577人，占99.81%；各[少数民族人口为](../Page/少数民族.md "wikilink")9182人，占0.25%。

## 交通

### 铁道

[缩略图](https://zh.wikipedia.org/wiki/File:Deyangzhan.JPG "fig:缩略图")\]\]

#### 既有线

  - [宝成铁路](../Page/宝成铁路.md "wikilink")
      - [德阳站](../Page/德阳站.md "wikilink")
  - [成兰铁路建设中](../Page/成兰铁路.md "wikilink")
      - [绵竹南站](../Page/绵竹南站.md "wikilink")-[什邡西站](../Page/什邡西站.md "wikilink")-[三星堆站](../Page/三星堆站.md "wikilink")
  - 无客运服务:[达成铁路](../Page/达成铁路.md "wikilink")（本市境内不设客运站）、[广岳铁路](../Page/广岳铁路.md "wikilink")、[德天铁路](../Page/德天铁路.md "wikilink")（均为专用铁路）

#### 高速线

  - [西成客运专线](../Page/西成客运专线.md "wikilink")
      - [罗江东站](../Page/罗江东站.md "wikilink")-[德阳站](../Page/德阳站.md "wikilink")-[广汉北站](../Page/广汉北站.md "wikilink")

### 长途汽车客运站

  - 旌阳区：[德阳汽车北站](../Page/德阳汽车北站.md "wikilink")、[德阳汽车南站](../Page/德阳汽车南站.md "wikilink")

### 道路

#### 高速公路

[成绵高速公路](../Page/成绵高速公路.md "wikilink")（属于G42京昆高速）、[成德南高速公路](../Page/成德南高速公路.md "wikilink")、[成什绵高速公路](../Page/成什绵高速公路.md "wikilink")、[成都第二绕城高速公路](../Page/成都第二绕城高速公路.md "wikilink")

##### 建设中或计划中

[成都第三绕城高速公路](../Page/成都经济区环线高速公路.md "wikilink")、[遂德阿高速公路](../Page/遂德阿高速公路.md "wikilink")

#### 国道

[国道108线](../Page/108国道.md "wikilink")

### 机场

德阳本市无民用机场。南距[成都双流国际机场](../Page/成都双流国际机场.md "wikilink")50公里，北距[绵阳南郊机场](../Page/绵阳南郊机场.md "wikilink")40公里。

## 经济

德阳是中国重大装备制造业基地、国家首批新型工业化产业示范基地和四川省第二大工业城市。拥有[中国二重](../Page/中国二重.md "wikilink")、东方电机、东方汽轮机、宏华石油等一批国内一流、世界知名的重装制造企业。德阳重大装备制造业集群在中国乃至世界都具有巨大的影响力，全国60%以上的核电产品、40%的水电机组、30%以上的火电机组和汽轮机、50%的大型轧钢设备和大型电站铸锻件、20%的大型船用铸锻件都是由德阳制造装备，发电设备产量连续多年居世界第一，石油钻机出口居全国第一；食品工业享誉中外，拥有中国名酒剑南春、长城雪茄、冰川时代矿泉水等一批优质品牌，建成了亚洲最大的雪茄烟生产基地；化学工业以磷化工、氯碱化工、钛化工、天然气化工为主，是全国重要的磷化工基地和化肥生产基地；德阳着力培育新能源装备制造战略性新兴产业，大力发展以核电、风力发电、太阳能、潮汐发电、生物能、燃料电池等为重点的新能源装备制造业，被联合国列为清洁技术与新能源装备制造业国际示范城市；新材料、医药等新兴产业快速发展，是国家新材料产业化基地和中药现代化生产基地。德阳经济技术开发区为国家级经济技术开发区。雄厚的工业基础，奠定了德阳在中国西部重要工业城市的地位。

  - 机械制造业有：三大重装制造企业[中国第二重型机械集团公司](../Page/中国第二重型机械集团公司.md "wikilink")（控制上市公司二重重装（SHA：601268））、[东方电机厂和](../Page/东方电机厂.md "wikilink")[东方汽轮机厂](../Page/东方汽轮机厂.md "wikilink")；四川宏华石油设备有限公司、四川蓝星机械有限公司、科新机电（SHE：300092）、广汉广达机械设备有限公司、德阳迪泰机械有限公司等。
  - 石油、钻机企业:[四川宏华石油设备有限公司](../Page/四川宏华石油设备有限公司.md "wikilink")（HK：0196）
  - 化工工业重要企业有：[金路集团](../Page/金路集团.md "wikilink")（SHE：000510）、[宏达股份](../Page/宏达股份.md "wikilink")（SHA：600331）、龙蟒集团、川恒股份等。
  - 食品工业重要企业有：[剑南春](../Page/剑南春.md "wikilink")、华润蓝剑啤酒、什邡卷烟厂。
  - 建材及其它工业重要企业有：四川森普管材股份有限公司、特变电工（德阳）电缆股份有限公司、绵竹市盘龙化建有限责任公司等。

德阳经济发展较快，年产值500万元以上工业企业接近1000家、资产总额达1022亿，2009年，全市实现地区生产总值780.00亿元，同比增长14.4%，固定资产投资746.86亿元，同比增长241.2%，实现社会品零售总额245.8亿元，同比增长22.6%，财政总收入129.64亿元，同比增长32.04%，地方财政收入64.99亿元，其中一般预算收入31.88亿元，城镇居民人均可支配收入达到16349元，[农村居民人均纯收入达到](../Page/农村居民人均纯收入.md "wikilink")5625元，四川省200强乡镇中，德阳占51个。

德阳经济的特征是县域经济比较发达，其下辖的旌阳区、绵竹市、什邡市、广汉市以前均为四川省经济综合实力“十强县”，因受[特大地震影响](../Page/汶川大地震.md "wikilink")，2008年度绵竹和什邡的经济总量负增长30%以上，连同广汉市均未能进入当年四川十强县。德阳一区五县在四川省2008年县级经济中的排位分别是旌阳（8）、广汉（13）、什邡（19）、绵竹（34）、中江（56）、罗江（95）。随着灾后重建的进行，德阳经济加速复苏，绵竹，什邡，广汉有望在2011年全部重返四川十强县之列。前身为广汉市工业开发小区的德阳高新技术产业园区2015年升级为国家高新技术产业开发区，在2014年高新技术产业占44.5%。

## 教育

有各类学校663所，在校学生52.81万人，专任教师26647人。其中：普通高校7所([中国民用航空飞行学院等](../Page/中国民用航空飞行学院.md "wikilink"))，在校普通本（专）科学生5.1万人，增30.10%；普通中学211所，在校生18.79万人；小学405所，在校生24.08万人。学龄儿童入学率99.15%。

## 物产

  - 中国名酒剑南春产自德阳的绵竹市。
  - 德阳的主要特产有天府花生、德阳酱油、广汉缠丝兔、黄许松花皮蛋、什邡板鸭、罗江豆鸡、中江挂面、孝泉果汁牛肉等，绵竹年画是中国四大[年画之一](../Page/年画.md "wikilink")。

## 名胜古迹

  - 庞统白马寺
  - 罗江万佛寺
  - [蓥华山](../Page/蓥华山.md "wikilink")
  - [三星堆遗址](../Page/三星堆遗址.md "wikilink")
  - 德阳孔庙
  - 德阳艺术墙
  - 李冰陵墓
  - [庞统祠墓](../Page/庞统祠墓.md "wikilink")
  - 黄继光纪念馆

## 名人

  - [秦宓](../Page/秦宓.md "wikilink")，[东汉末期](../Page/东汉.md "wikilink")[蜀汉以辩材出众的](../Page/蜀汉.md "wikilink")[大司农](../Page/大司农.md "wikilink")
  - 安安，著名孝子，“安安送米”
  - [马祖道一](../Page/马祖道一.md "wikilink")，[唐朝高僧](../Page/唐朝.md "wikilink")，[禅宗八祖](../Page/禅宗.md "wikilink")
  - [苏舜钦](../Page/苏舜钦.md "wikilink")，[北宋诗人](../Page/北宋.md "wikilink")、诗文革新先驱
  - [张浚](../Page/张浚.md "wikilink")，[南宋宰相](../Page/南宋.md "wikilink")、抗金名将
  - [张栻](../Page/张栻.md "wikilink")，南宋学者，[湖湘学派代表人物](../Page/湖湘学派.md "wikilink")
  - 张师古，[农学家](../Page/农学.md "wikilink")
  - [李调元](../Page/李调元.md "wikilink")，[清朝文学家](../Page/清朝.md "wikilink")
  - [杨锐](../Page/杨锐.md "wikilink")，[戊戌变法六君子之一](../Page/戊戌变法.md "wikilink")
  - [能海法师](../Page/能海法师.md "wikilink")，著名高僧，[中国佛教协会副会长](../Page/中国佛教协会.md "wikilink")
  - [戴季陶](../Page/戴季陶.md "wikilink")，[国民党元老](../Page/国民党.md "wikilink")，[民国政治家](../Page/民国.md "wikilink")
  - [陈豹隐](../Page/陈豹隐.md "wikilink")，第一个翻译《[资本论](../Page/资本论.md "wikilink")》
  - [覃子豪](../Page/覃子豪.md "wikilink")，近代诗人，[台湾诗坛三老之一](../Page/台湾.md "wikilink")
  - [黄继光](../Page/黄继光.md "wikilink")，[抗美援朝战斗英雄](../Page/抗美援朝.md "wikilink")
  - 刘沧龙，企业家，全国工商联副主席
  - “中江表妹”，方言艺人
  - 黎阳，著名新生代作家
  - [谢娜](../Page/谢娜.md "wikilink")，著名主持艺人
  - [张含韵](../Page/张含韵.md "wikilink")，著名少女艺人
  - [陈欧](../Page/陈欧.md "wikilink")，聚美优品CEO

## 友好城市

  - [东广岛市](../Page/东广岛市.md "wikilink") 1993年10月14日

  - [弗拉基米尔州](../Page/弗拉基米尔州.md "wikilink") 1994年6月1日

  - [曼西](../Page/曼西_\(印第安纳州\).md "wikilink") 1994年9月15日

  - [西根-维特根施泰因县](../Page/西根-维特根施泰因县.md "wikilink") 1996年8月6日

  - [拉古迪亚市](../Page/拉古迪亚市.md "wikilink") 1997年6月4日

  - [拉赫蒂](../Page/拉赫蒂.md "wikilink") 2000年9月7日

  - [淄博市](../Page/淄博市.md "wikilink")

  - [株洲市](../Page/株洲市.md "wikilink") 2010年7月14日

  - [顺义区](../Page/顺义区.md "wikilink") 2013年8月1日

  - [江陵市](../Page/江陵市.md "wikilink") 2013年11月28日

  - [虹口区](../Page/虹口区.md "wikilink") 1986年9月

## 參考資料

## 外部链接

  - [德阳市人民政府](https://web.archive.org/web/20161209162212/http://www.deyang.gov.cn/)

[Category:德阳](../Category/德阳.md "wikilink")
[Category:四川地级市](../Category/四川地级市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.