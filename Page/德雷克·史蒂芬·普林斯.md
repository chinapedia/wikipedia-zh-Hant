**德雷克·史蒂芬·普林斯**是一位[美國專業](../Page/美國.md "wikilink")[配音員](../Page/配音員.md "wikilink")，曾為《[數碼寶貝](../Page/數碼寶貝.md "wikilink")》系列多個不同角色配音。

## 主要出演作品

### 電視動畫

  - [BLEACH](../Page/BLEACH.md "wikilink")（[石田雨龍](../Page/石田雨龍.md "wikilink")）
  - [星際牛仔](../Page/星際牛仔.md "wikilink")（Lin）
  - [數碼寶貝大冒險](../Page/数码宝贝大冒险.md "wikilink")（DemiDevimon、Piedmon）
  - [數碼寶貝大冒險02](../Page/数码宝贝大冒险02.md "wikilink")（Ken
    Ichijouji、DemiVeemon/Veemon/ExVeemon）
  - [數碼寶貝馴獸師](../Page/數碼寶貝馴獸師.md "wikilink")（Impmon/Beelzemon）
  - [數碼寶貝最前線](../Page/數碼寶貝最前線.md "wikilink")（Grumblemon/Gigasmon、Dynasmon）
  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink")（夕城圭介）
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")（Birgit Pirjo）
  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink")（浦島景太郎、蘭巴·魯）
  - [魔力女管家](../Page/魔力女管家.md "wikilink")（美里優）
  - [火影忍者](../Page/火影忍者.md "wikilink")（[油女志乃](../Page/油女志乃.md "wikilink")）
  - [天堂之吻](../Page/天堂之吻.md "wikilink")（嵐）
  - [浪客劍心](../Page/浪客劍心.md "wikilink")（沢下条張、癋見）
  - [混沌武士](../Page/混沌武士.md "wikilink")（渋井友之進、伝鬼坊）
  - [超能奇兵](../Page/超能奇兵.md "wikilink")（立浪ジョージ、マサキ）
  - [爆裂戰士戰藍寶](../Page/爆裂戰士戰藍寶.md "wikilink")（ニッパー国王）

### OVA

  - [Love Hina Again](../Page/純情房東俏房客.md "wikilink")（浦島景太郎）

### 劇場版

  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")（Birgit Pirjo）
  - [數碼寶貝大冒險02
    帝阿波羅獸的逆襲](../Page/数码宝贝大冒险02.md "wikilink")（Veemon、インペリアルドラモン）
  - [數碼寶貝馴獸師 冒險者們的戰鬥](../Page/數碼寶貝馴獸師.md "wikilink")（上原武人）

### 遊戲

  - [.hack](../Page/.hack.md "wikilink")
  - [ＮＡＲＵＴＯ－ナルト- 最強忍者大結集](../Page/火影忍者.md "wikilink") （油女志乃）
  - [女神異聞錄3](../Page/女神異聞錄3.md "wikilink")（タカヤ）
  - [魂之搖籃 侵蝕世界者](../Page/魂之搖籃_侵蝕世界者.md "wikilink")（Vitali）

## 外部連結

  - [互聯網電影數據庫資料](http://www.imdb.com/name/nm1821497/)

[P](../Category/美国男配音演员.md "wikilink")
[P](../Category/加州人.md "wikilink")