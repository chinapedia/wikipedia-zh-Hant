**徽劇**是一種[中國地方](../Page/中國.md "wikilink")[戲曲](../Page/戲曲.md "wikilink")，起源於[明代](../Page/明代.md "wikilink")[嘉靖年間](../Page/嘉靖.md "wikilink")，迄今已有三百多年歷史。它形成於[安徽](../Page/安徽.md "wikilink")[徽州](../Page/徽州.md "wikilink")、[池州](../Page/池州.md "wikilink")、[太平](../Page/太平府_\(安徽省\).md "wikilink")（今[歙縣](../Page/歙縣.md "wikilink")、[貴池](../Page/貴池.md "wikilink")、[當塗](../Page/當塗.md "wikilink")）一帶的“徽池雅調”，在[安徽省境內](../Page/安徽省.md "wikilink")（尤其是[皖南](../Page/皖南.md "wikilink")，包括今属[江西省的](../Page/江西省.md "wikilink")[婺源縣](../Page/婺源縣.md "wikilink")）流行起來。特色是把傳入安徽的亂彈聲腔與地方聲腔及民間音樂結合起來。它為[京劇](../Page/京劇.md "wikilink")、[粵劇及南方的許多地方戲曲劇種提供一個重要的發展基礎](../Page/粵劇.md "wikilink")，其影響幾乎遍及全國。

## 歷史沿革

[明代中葉](../Page/明代.md "wikilink")，[皖南的徽州](../Page/皖南.md "wikilink")、池州是中國東南商貿文化的中心，當時著名的戲曲聲腔譬如[餘姚腔](../Page/餘姚腔.md "wikilink")、[弋陽腔已在這一帶流行](../Page/弋陽腔.md "wikilink")。[嘉靖年間](../Page/嘉靖.md "wikilink")，[江西弋陽腔流傳到安徽](../Page/江西.md "wikilink")[青陽](../Page/青陽.md "wikilink")、[貴池一帶](../Page/貴池.md "wikilink")，與當地民眾喜愛的民間曲調相結合，創造了新腔，形成了具有當地地方特色的「青陽腔」。[萬曆年間](../Page/萬曆.md "wikilink")，這一帶產生了「徽州腔」、「青陽腔」(亦稱池州腔)、「太平腔」、「四平腔」等多種聲腔。這些聲腔興起後，便風靡各地。後來，青陽腔又受到民間山歌小調和[昆曲的影響](../Page/昆曲.md "wikilink")，慢慢形成今天的徽劇。

明末清初，[西秦腔等亂彈聲腔流入安徽](../Page/西秦腔.md "wikilink")，受當地諸腔影響，逐漸衍變，形成了徽調主要唱腔之一的撥子。

撥子高亢激越、吹腔柔和，常在同一個劇目中配合使用，通稱“吹撥”。由於它產生和流行在[樅陽](../Page/樅陽.md "wikilink")、[石牌](../Page/石牌鎮_\(懷寧縣\).md "wikilink")（今懷寧）、[安慶一帶](../Page/安慶.md "wikilink")，曾被稱為「樅陽腔」、「石牌調」或「安慶梆子」。

吹腔(崑弋腔)為[曲牌體受到滾調的影響](../Page/曲牌體.md "wikilink")，逐漸形成七字句或十字句一套板式的唱腔。

撥子又稱「二凡」，為五聲音階，有時亦出現「變宮(si)」，但無「變徽(fa)」。唱時以棗木梆擊節。後來受青陽腔等聲腔的影響，發展了導板、十八板(回龍)、原板、流水、疊板、散板等各種板式，才演變成撥子。

二凡與撥子均擅長表現激越之情，在老徽戲劇本中，二凡和撥子可以通用。另外，撥子和吹腔可以結合使用，往往在一本戲中，唱腔採用吹腔與撥子兩種腔調，抒情時唱吹腔，激昂處唱撥子;文戲部分唱吹腔，武戲部分唱撥子。這種互取其長的配合運用，漸漸發展、融合、衍變，於是產生了二簧腔。

二簧腔（二黄腔）的形成是由幾種聲腔融合而成。初時，吹腔用崑笛伴奏，因其四平腔、崑腔風味較濃，稱之為「四崑腔」、「崑平腔」。這種腔調後受撥子影響，並改用[嗩吶伴奏](../Page/嗩吶.md "wikilink")，形成了“嗩吶二簧”。當時的曲調結構和板式變化都還比較簡單，吸收了撥子之後並加以演化成比較完整的二簧。二簧改用[胡琴伴奏](../Page/胡琴.md "wikilink")，唱腔更加流暢柔和。後又變出了反二簧。另外，還衍生了二簧平、老二簧、二簧、反二簧。

[清朝](../Page/清朝.md "wikilink")[乾隆年間](../Page/乾隆.md "wikilink")，二簧腔就已盛行，於[皖南](../Page/皖南.md "wikilink")、[鄂東](../Page/鄂東.md "wikilink")、[贛東北相鄰地區](../Page/贛東.md "wikilink")，二簧、西皮慢慢合流，並奠定了徽劇的基礎。徽劇名藝人[高朗亭](../Page/高朗亭.md "wikilink")、[郝天壽等人](../Page/郝天壽.md "wikilink")，把徽劇帶到[揚州](../Page/揚州.md "wikilink")。當時揚州是[花部的集中地](../Page/花部.md "wikilink")，但很快被徽劇壓倒。[乾隆五十五年](../Page/乾隆.md "wikilink")(1790年)，高朗亭又把徽劇帶到了[北京](../Page/北京.md "wikilink")。後來進京的還有四慶徽、五慶徽、四喜、春台、和春、三和等徽班。其中，以三慶、四喜、春台、和春四班最為有名，人稱“四大徽班”。\[1\]徽劇在北京後便興盛起來，直到[嘉慶](../Page/嘉慶.md "wikilink")、[道光年間](../Page/道光.md "wikilink")，發展成以唱[西皮](../Page/西皮.md "wikilink")、[二簧為主的](../Page/二簧.md "wikilink")[京劇](../Page/京劇.md "wikilink")。自此，徽班的影響遍及全國南北各省。在[川劇](../Page/川劇.md "wikilink")、[湘劇](../Page/湘劇.md "wikilink")、[贛劇](../Page/贛劇.md "wikilink")、[閩劇](../Page/閩劇.md "wikilink")、[粵劇](../Page/粵劇.md "wikilink")、[滇劇](../Page/滇劇.md "wikilink")、[黔劇](../Page/黔劇.md "wikilink")、[婺劇](../Page/婺劇.md "wikilink")、[淮劇等劇種中](../Page/淮劇.md "wikilink")，都可以找到徽戲的影子。

清代末葉，京劇興起後，藝人紛紛改學京劇，徽劇卻日益衰落。到了20世紀40年代，已瀕於消亡。中華人民共和國成立後，組織了[安徽省徽劇團](../Page/安徽省徽劇團.md "wikilink")，徽劇老藝人培養年輕演員，挖掘及整理劇目。60年代，徽劇在北京重生，《水淹七軍》、《淤泥河》等劇都獲得好評。二十世紀八十年代，徽劇在婺源仍然很受歡迎。每逢新年過節，各地搶著請劇團去演出。

## 藝術特色

徽劇的劇目很多，根據記載有一千四百零四個，曲牌800多首，其中文場曲牌也有210多首，但因年代較久，多為手抄本，不少已經失傳。\[2\]重新發掘後，徽劇著名劇目有《[七擒孟獲](../Page/七擒孟獲.md "wikilink")》、《[八陣圖](../Page/八陣圖.md "wikilink")》、《[昭君出塞](../Page/昭君出塞.md "wikilink")》、《貴妃醉酒》、《[千里駒](../Page/千里駒.md "wikilink")》、《雙合印》、《巧姻緣》、《[龍虎鬥](../Page/龍虎鬥.md "wikilink")》、《反昭關》、《[宇宙鋒](../Page/宇宙鋒.md "wikilink")》、《[花田錯](../Page/花田錯.md "wikilink")》、《春秋配》、《水淹七軍》、《[齊王點兵](../Page/齊王點兵.md "wikilink")》、《[戰長沙](../Page/戰長沙.md "wikilink")》、《古城會》、《[百花贈劍](../Page/百花贈劍.md "wikilink")》、《義虎報》、《齊王點馬》、《借靴》、《龍鳳扇》、《三擋》、《醉打三門》等。

根據唱腔，徽劇可分[徽崑](../Page/徽崑.md "wikilink")、[吹腔](../Page/吹腔.md "wikilink")、[撥子](../Page/撥子.md "wikilink")、[二簧](../Page/二簧.md "wikilink")、[西皮](../Page/西皮.md "wikilink")、[花腔小調](../Page/花腔小調.md "wikilink")。徽崑的唱腔曲牌較[蘇崑粗獷強烈](../Page/蘇崑.md "wikilink")，以演武戲為主。吹腔以笛和小嗩吶為主要，分曲牌、板式變化加曲牌體、板式變化體。撥子以棗木梆擊出節奏為主。二簧、西皮大部分以徽胡為主。花腔小調大部分是民間俗曲俚歌，生活氣息較濃。

早期徽劇的腳色分為：[末](../Page/末.md "wikilink")、[生](../Page/生.md "wikilink")、[小生](../Page/小生.md "wikilink")、[外](../Page/外.md "wikilink")、[旦](../Page/旦.md "wikilink")、[貼](../Page/貼.md "wikilink")、[夫](../Page/夫.md "wikilink")、[淨](../Page/淨.md "wikilink")、-{[丑](../Page/丑.md "wikilink")}-九行。在表演上，動作粗獷豪壯，擅長武戲，如獨腳單踢、叉腿單踢、刀門、翻台子、跳圈、竄火、飛叉、紅拳等。

徽劇傑出代表是[程長庚](../Page/程長庚.md "wikilink")。他把徽音、京音、楚音兼收並蓄，自成一家，承先啟後。因此，他成為[京劇的開山祖師](../Page/京劇.md "wikilink")。

## 注釋

<references />

## 外部链接

  - [安徽省人民政府:徽劇](https://web.archive.org/web/20051215175951/http://www.ah.gov.cn/zjah/mainmenu.asp?kind=lswh&name=yyqp&thrdname=hj)
  - [京劇的前身-徽劇](http://www.chinanews.com.cn/zxys/dfxq-hj.html)
  - [徽劇簡介](http://www.dsxm.com/ahdfx-0001-hj.htm)

[Category:戏曲剧种](../Category/戏曲剧种.md "wikilink")
[艺术\*戏剧](../Category/徽州文化.md "wikilink")
[Category:安徽文化](../Category/安徽文化.md "wikilink")
[Category:中国非物质文化遗产](../Category/中国非物质文化遗产.md "wikilink")

1.  《夢華瑣簿》，[道光二十二年](../Page/道光.md "wikilink")
2.  [黃山攬勝:徽劇與徽班](http://www.hsyts.com/big5/general/200622715816.html)