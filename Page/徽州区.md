**徽州区**是[中國](../Page/中國.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[黄山市下辖的一个区](../Page/黄山市.md "wikilink")。1987年由原[歙县西部地区析置](../Page/歙县.md "wikilink")。

## 历史

1987年11月成立黄山市，同时设立[市辖区徽州区](../Page/市辖区.md "wikilink")。\[1\]

## 行政区划

徽州区下辖1个街道、4个镇、3个乡：

  - 街道：[徽州街道](../Page/徽州街道.md "wikilink")
  - 镇：[岩寺镇](../Page/岩寺镇.md "wikilink")、[西溪南镇](../Page/西溪南镇.md "wikilink")、[潜口镇](../Page/潜口镇.md "wikilink")、[呈坎镇](../Page/呈坎镇.md "wikilink")
  - 乡：[富溪乡](../Page/富溪乡.md "wikilink")、[杨村乡](../Page/杨村乡.md "wikilink")、[洽舍乡](../Page/洽舍乡.md "wikilink")。

## 地理

地势北高南低。北部为黄山余脉，以[山地为主](../Page/山地.md "wikilink")，建有[丰乐水库](../Page/丰乐水库.md "wikilink")。南部以[丘陵](../Page/丘陵.md "wikilink")[平原为主](../Page/平原.md "wikilink")。

## 交通

  - [杭黄客运专线](../Page/杭黄客运专线.md "wikilink")
  - [皖赣铁路](../Page/皖赣铁路.md "wikilink")
  - [京台高速公路](../Page/京台高速公路.md "wikilink")
  - [205国道](../Page/205国道.md "wikilink")、慈张公路

## 人口

第五次人口普查全区总人口91122人。

## 经济

2006年生产总值10.91亿元，人均生产总值达1.1万元。三次产业比例为17.6:47.1:35.3。\[2\]

2007年生产总值141698万元，人均生产总值达14240元，三次产业比例为14.7:55.7:29.6。\[3\]

2008年生产总值164689万元，人均生产总值达16497元。三次产业比例为14.1:56.0:29.9。\[4\]

## 教育

2008年徽州区有高级中学、职业中学各1所，初级中学6所，小学57所。小学、初中学龄儿童入学率分别为99.72%、98.04%，15周岁人口初等教育完成率99.94%。\[5\]

## 风景名胜

[Yansi_5428.jpg](https://zh.wikipedia.org/wiki/File:Yansi_5428.jpg "fig:Yansi_5428.jpg")
徽州区有4处[国家重点文物保护单位](../Page/国家重点文物保护单位.md "wikilink")：

  - [潜口民宅](../Page/潜口民宅.md "wikilink")，位于[潜口镇](../Page/潜口镇.md "wikilink")
  - [罗东舒祠](../Page/罗东舒祠.md "wikilink")，位于[呈坎镇](../Page/呈坎镇.md "wikilink")[呈坎村](../Page/呈坎村.md "wikilink")
  - [呈坎村古建筑群](../Page/呈坎村古建筑群.md "wikilink")，位于[呈坎镇](../Page/呈坎镇.md "wikilink")[呈坎村](../Page/呈坎村.md "wikilink")
  - [老屋阁及绿绕亭](../Page/老屋阁及绿绕亭.md "wikilink")，位于[西溪南镇西溪南村](../Page/西溪南镇.md "wikilink")

5处[安徽省文物保护单位](../Page/安徽省文物保护单位.md "wikilink")：

  - [南方八省红军游击队集中地旧址](../Page/南方八省红军游击队集中地旧址.md "wikilink")（凤山台），位于[岩寺镇](../Page/岩寺镇.md "wikilink")
  - [岩寺文峰塔](../Page/岩寺文峰塔.md "wikilink")，位于[岩寺镇](../Page/岩寺镇.md "wikilink")
  - [檀干园](../Page/檀干园.md "wikilink")，位于潜口镇[唐模村](../Page/唐模村.md "wikilink")
  - [洪坑牌坊群及洪氏家庙](../Page/洪坑牌坊群及洪氏家庙.md "wikilink")
  - [金紫祠](../Page/金紫祠.md "wikilink")

## 参考文献

## 参见

  - [歙县](../Page/歙县.md "wikilink")

[黄山](../Page/category:安徽市辖区.md "wikilink")

[徽州区](../Category/徽州区.md "wikilink") [区](../Category/黄山区县.md "wikilink")

1.
2.
3.
4.
5.