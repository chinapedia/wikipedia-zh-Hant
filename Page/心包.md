  -
    *有關中醫的心包，見[心包絡](../Page/心包絡.md "wikilink")*

**心包**，又名**心膜**，是一個圆锥形雙層纤维浆膜囊，包裹[心臟和出入心臟大血管根部](../Page/心臟.md "wikilink")。心包的兩層分別为：

  - 内层的[浆膜心包](../Page/浆膜心包.md "wikilink")（*Pericardium serosum*）和
  - 外层的[纤维心包](../Page/纤维心包.md "wikilink")（*Pericardium fibrosum*）。

## 語源

心包的學名來自[希臘語的](../Page/希臘語.md "wikilink")（環繞、周圍）與（心臟）兩字合成的/perikardion/。

## 解剖

心包是一個堅靭的雙層膜囊，包裹整顆心臟。兩層心包之間的潜在腔隙被称为[心包腔](../Page/心包腔.md "wikilink")\[1\]，有被叫作的浆液在其中，以保護心臟免受外來撞擊影響，並有润滑作用。

在內層的浆膜心包分为壁层（*parietal pericardium*）和臟层（*visceral
pericardium*）两层。壁層已跟纖維心包融合成一體，不能分開；臟层又称[心外膜](../Page/心外膜.md "wikilink")（epicardium），是心臟肌肉以外最貼近的保護層\[2\]。臟壁两层在出入心根部互相移行。两层之间。在心包腔内，臟壁两层折返处的间隙被称为[心包窦](../Page/心包窦.md "wikilink")，主要有[心包横窦](../Page/心包横窦.md "wikilink")、[心包斜窦和](../Page/心包斜窦.md "wikilink")[心包前下窦三者](../Page/心包前下窦.md "wikilink")。\[3\]

纤维心包则是由致密的纤维结缔组织组成。它在出入心大血管的根部与后者的外膜相连接\[4\]。

## 圖庫

<File:Thorax>` section 5.jpg|Pericardium`
`Image:Gray806.png|The phrenic nerve and its relations with the vagus nerve.`
`Image:Gray846.png|Thoracic portion of the sympathetic trunk.`
`Image:Gray1088.png|Liver with the septum transversum. Human embryo 3 mm. long.`
`Image:Gray1178.png|The thymus of a full-term fetus, exposed in situ.`
<File:Thoracic>` cavity of foetus 2.JPG|Fetal pericardium`
[`File:Slide8eeee.JPG|Pericardium`](File:Slide8eeee.JPG%7CPericardium)` `

[File:Slide5wwww.JPG|Pericardium](File:Slide5wwww.JPG%7CPericardium)
[File:Slide2111.JPG|Pericardium](File:Slide2111.JPG%7CPericardium)
[File:Slide10444.JPG|Pericardium](File:Slide10444.JPG%7CPericardium)

## 參考資料

## 參看

  - [心包炎](../Page/心包炎.md "wikilink")

## 外部連結

  - \- "Mediastinum: Pericardium (pericardial sac)"

  - ()

[Category:心臟解剖學](../Category/心臟解剖學.md "wikilink")

1.

2.

3.
4.