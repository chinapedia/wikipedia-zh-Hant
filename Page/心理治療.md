**心理治療**（）是由經過受過心理治療專業訓練並通過考核的人員，主要是[心理師以及接受心理治療訓練的](../Page/心理師.md "wikilink")[精神科醫師](../Page/精神科醫師.md "wikilink")。建立一種獨特的人際關係來協助當事人（或稱案主、個案）處理心理問題、減輕主觀痛苦經驗、醫治[精神疾病及促進心理健康](../Page/精神疾病.md "wikilink")、個人成長。心理治療一般是基於心理治療理論及相關實證研究（主要是[諮商心理學](../Page/諮商心理學.md "wikilink")、[臨床心理學和](../Page/臨床心理學.md "wikilink")[精神病學](../Page/精神病學.md "wikilink")）而建立的治療系統，以建立關係、對話、溝通、深度自我探索、行為改變等的技巧來達到治療目標，例如改善受助者的心理健康或減輕[精神疾病](../Page/精神疾病.md "wikilink")[徵狀等](../Page/徵狀.md "wikilink")。

## 主要派別

心理治療有多個不同學派，而且不斷發展中。不同學派之間對學派之劃分也有歧異，即使有經驗的治療師也無法熟稔所有學派的治療方式。美國著名的精神科經典教材Synopsis
of Psychiatry和英國牛津出版的精神科教科書也沒有一致的分類法。在此引用英國系統的分類方式\[1\]

  - [精神分析](../Page/精神分析.md "wikilink")：由[弗洛依德創立](../Page/弗洛依德.md "wikilink")，主要目標為揭示[潛意識的](../Page/潛意識.md "wikilink")[焦慮](../Page/焦慮.md "wikilink")，主要技巧包括[自由聯想](../Page/自由聯想.md "wikilink")（free
    association）、[轉移和](../Page/轉移.md "wikilink")[反轉移](../Page/反轉移.md "wikilink")（transference
    and counter-transference）、[夢境解析](../Page/夢境解析.md "wikilink")（dream
    analysis）等。

  - [荣格的](../Page/荣格.md "wikilink")[分析心理學](../Page/分析心理學.md "wikilink")：由[荣格創立](../Page/荣格.md "wikilink")，主要技巧如[沙遊](../Page/沙遊.md "wikilink")（sandplay），[藝術治療大多以](../Page/藝術治療.md "wikilink")[荣格的理論為基礎](../Page/荣格.md "wikilink")。

  - [阿德勒精神分析學派](../Page/阿尔弗雷德·阿德勒.md "wikilink")：由[阿德勒所創立](../Page/阿德勒.md "wikilink")，主要目標為減少[自卑](../Page/自卑.md "wikilink")（inferiority）
    ，主要技巧包括：分析[家庭系统排列](../Page/家庭系统排列.md "wikilink")（family
    constellation），[生活風格等](../Page/生活風格.md "wikilink")。

  - [動力取向心理治療](../Page/動力取向心理治療.md "wikilink")：儘管精神分析學者不認為精神分析是精神動力學派其中一支，但大多數治療師仍同意精神分析是動力取向的始祖。

  - [行為治療](../Page/行為治療.md "wikilink")，如[系統減敏法和](../Page/系統減敏法.md "wikilink")[厭惡療法等等](../Page/厭惡療法.md "wikilink")

  - [認知行為治療](../Page/認知行為治療.md "wikilink")，包括[認知治療](../Page/認知治療.md "wikilink")（CT）和[理性情緒行為治療](../Page/理性情緒行為治療.md "wikilink")（REBT）以及Behavioral
    Modification

  - [案主中心治療](../Page/個人中心治療.md "wikilink")，由[羅哲斯從](../Page/羅哲斯.md "wikilink")[人本主義中發展出來的心理治療](../Page/人本主義.md "wikilink")

  - ：以神經心理學及認知科學為基礎之跨診斷的心理治療取向，主要透過提升個體大腦各神經心理功能部件(如選擇性注意力,記憶力,情緒調控等)，增進個案之適應能力

  - [團體心理治療與](../Page/團體心理治療.md "wikilink")[性治療](../Page/性治療.md "wikilink")

  - [完形治療](../Page/完形治療.md "wikilink")

  - [Chuang’s Music
    psychotherapy](../Page/Chuang’s_Music_psychotherapy.md "wikilink")

是結合中西的一種音樂心理療法,
引用中醫的攻、洩療法,針對心理療法的應用,從陰陽兩極之間平衡的基礎,運用音樂的共振原理導入神經引導,讓身心平衡,是進入醫學輔助在身心科有效的應用。

  - 會心治療
  - [心理劇](../Page/心理劇.md "wikilink")
  - 語言治療
  - 原始療法
  - [存在主義治療](../Page/存在主義治療.md "wikilink")：亞隆（Irvin
    Yalom）提出，與其說是提供具體治療技巧，不如說是提供治療者一個觀點。
  - 意象对话
  - [敘事治療](../Page/敘事治療.md "wikilink")
  - [問題焦點解決治療](../Page/問題焦點解決治療.md "wikilink")
  - [現實療法](../Page/現實療法.md "wikilink")
  - 合作取向療法

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:精神病學](../Category/精神病學.md "wikilink")
[Category:精神病](../Category/精神病.md "wikilink")
[Category:臨床心理學](../Category/臨床心理學.md "wikilink")
[Category:諮商心理學](../Category/諮商心理學.md "wikilink")
[Category:心理與行為疾病](../Category/心理與行為疾病.md "wikilink")
[Category:心理學](../Category/心理學.md "wikilink")
[Category:心理健康](../Category/心理健康.md "wikilink")

1.  Anthony Bateman等著 陳登義譯 心理治療入門（Introduction to Psychotherapy）