**心理语言学**是通过[心理学来研究](../Page/心理学.md "wikilink")[语言学的学科](../Page/语言学.md "wikilink")。他从心理过程和语言结构的对应关系来研究人类[语言机制的构造和功能](../Page/语言.md "wikilink")。从心理学的角度，它属于[认知心理学](../Page/认知心理学.md "wikilink")；从语言学的角度，它属于[实验语言学](../Page/实验语言学.md "wikilink")。

## 历史

心理语言学（*Psycholinguistics*）一词是1936年[美国心理学家](../Page/美国.md "wikilink")[坎特在其著作](../Page/坎特.md "wikilink")《语法的客观心理学》中开始使用的。但是作为心理语言学成为一个学科的标志则是1953年在美国[印第安纳大学举办的](../Page/印第安纳大学.md "wikilink")“心理语言学”讨论会，以及1954年[奥斯古德和](../Page/奥斯古德.md "wikilink")[西比奥克在此基础上合编的](../Page/西比奥克.md "wikilink")《心理语言学——理论和研究问题概述》。

心理语言学在1950年代时，其理论基础主要是[行为主义心理学](../Page/行为主义心理学.md "wikilink")。此时认为语言的学习就是不断的刺激、强化，儿童对语言的学习就是不断地对环境刺激做出反应，反复强化，形成语言习惯，从而形成语言行为。在[语言教学中](../Page/语言教学.md "wikilink")，注重于强调[语法结构](../Page/语法.md "wikilink")，忽视[语义变化](../Page/语义.md "wikilink")；强调[句型练习](../Page/句型.md "wikilink")。此时的心理语言学对语言行为进行了狭义的编码和译码研究，并且对语言与意义、[人格](../Page/人格.md "wikilink")、[文化等问题进行了研究](../Page/文化.md "wikilink")，并且对[失语症和](../Page/失语症.md "wikilink")[精神分裂症的语言病理进行了临床研究](../Page/精神分裂症.md "wikilink")。

1957年，[乔姆斯基发表了](../Page/乔姆斯基.md "wikilink")《[句法结构](../Page/句法结构.md "wikilink")》一书，提出[生成语法理论](../Page/生成语法.md "wikilink")，从而引起了心理语言学对人类利用语言时的心理过程的激烈讨论，研究的问题主要是：

  - 语言运用的模式
  - 语言结构和语义的储存与记忆
  - 人脑对各种句子的处理

1959年，美国语言学家[乔姆斯基撰文指出](../Page/乔姆斯基.md "wikilink")，虽然通过实验能够有效地支持对动物的行为进行刺激、反应和强化的解释，但是人类语言本身非常复杂，很难确定应该刺激什么和使什么发生反应，因此行为主义心理学关于语言的解释含糊不清，无法说明问题。此后心理语言学开始检验乔姆斯基的理论是否适用于心理学和[实证语言学中心理世纪的研究和试验](../Page/实证语言学.md "wikilink")。多数心理语言学家的结论是，语言结构和使用它的人的心理实际结构之间存在着某种对应关系，但不见得是一一对应的。

## 研究範圍

**心理语言学**研究人类如何使用和学习语言，并且如何通过语言来表达思想，进行思维。它的研究基礎主要是來自[心理學](../Page/心理學.md "wikilink")、[認知科學](../Page/認知科學.md "wikilink")、[語言學的理論和研究方法](../Page/語言學.md "wikilink")。

[語言學方面](../Page/語言學.md "wikilink"):

  - [語音學及](../Page/語音學.md "wikilink")[音韻學關注語言發音的問題](../Page/音位學.md "wikilink").
    在**心理語言學**的範疇裏，則研究[腦部如何理解及處理語言的聲音](../Page/腦部.md "wikilink")。

<!-- end list -->

  - [構詞學是研究字的結構](../Page/構詞學.md "wikilink")，特別是有相同字根的字
    (例如：dog和dogs)，以及產生這些字的規則。

<!-- end list -->

  - [語法學是研究語言的規則](../Page/語法學.md "wikilink")，以及[文法](../Page/文法.md "wikilink")。[喬姆斯基提出](../Page/喬姆斯基.md "wikilink")[轉換-生成語法的理論](../Page/轉換-生成語法.md "wikilink")，令**心理語言學**的研究視[語法學為語言中非常重要的研究對象](../Page/語法學.md "wikilink")。

<!-- end list -->

  - [語義學則處理字詞和句子的含意](../Page/語義學.md "wikilink")。

<!-- end list -->

  - [語用學就關注語言環境對語意理解的影響](../Page/語用學.md "wikilink")。

[心理學方面](../Page/心理學.md "wikilink")：

**心理語言學**主要集中研究認字和閱讀的過程，去了解人類思維的運作。而[認知心理學中](../Page/認知心理學.md "wikilink")[運作記憶](../Page/運作記憶.md "wikilink")
(*Working Memory*) 模型則成為**心理語言學**研究人類在處理語言 (聽、講、讀、寫四方面) 的研究基礎方向。

而[發展心理語言學則集中研究幼兒及小童如何學習語言](../Page/發展心理語言學.md "wikilink")，通常使用實驗方法或[量性研究去進行研究](../Page/量性研究.md "wikilink")。（相對於[讓·皮亞傑以](../Page/讓·皮亞傑.md "wikilink")[自然觀察的方法去研究兒童認知發展](../Page/自然觀察.md "wikilink")）

## 參見

  - [Animal language](../Page/Animal_language.md "wikilink")
  - [教育心理学](../Page/教育心理学.md "wikilink")
  - [人腦](../Page/人腦.md "wikilink")
  - [语言习得](../Page/语言习得.md "wikilink")
  - [Language processing](../Page/Language_processing.md "wikilink")
  - [薩丕爾-沃夫假說](../Page/薩丕爾-沃夫假說.md "wikilink")
  - [神经语言学](../Page/神经语言学.md "wikilink")
  - [先天論](../Page/先天論.md "wikilink")
  - [第二語言習得](../Page/第二語言習得.md "wikilink")
  - [Speech perception](../Page/Speech_perception.md "wikilink")
  - [TRACE model](../Page/Trace_\(psycholinguistics\).md "wikilink")

## 延伸閱讀

A short list of books that deal with psycholinguistics, written in
language accessible to the non-expert, includes:

  - [Belyanin V.P](../Page/Belyanin_V.P.md "wikilink") (Белянин, Валерий
    Павлович). *[Foundations of Psycholinguistic Diagnostics (Models of
    the
    World)](https://web.archive.org/web/20051111090813/http://www.textology.ru/belyanin/bel_ann1.html)*.
    Moscow, 2000 (in Russian)
  - Chomsky, Noam. (2000) *New Horizons in the Study of Language and
    Mind*. Cambridge: Cambridge University Press.
  - [Harley, Trevor](../Page/Trevor_Harley.md "wikilink"). (2008) *[The
    Psychology of Language: From data to theory (3rd.
    ed.)](https://web.archive.org/web/20080914072626/http://www.psypress.com/harley/)*
    Hove: Psychology Press.
  - [Harley, Trevor](../Page/Trevor_Harley.md "wikilink"). (2009)
    *Talking the talk: Language, psychology and science*. Hove:
    Psychology Press.
  - Lakoff, George. (1987) *Women, fire, and dangerous things: what
    categories reveal about the mind*. Chicago: University of Chicago
    Press.
  - Menn, Lise. (2016). *Psycholinguistics: Introduction and
    Applications*, 2nd ed. San Diego: Plural Publishing, Inc.
  - Piattelli-Palmarini, Massimo. (ed.) (1980) *Language and learning:
    the debate between Jean Piaget and Noam Chomsky*. Cambridge, Mass.:
    Harvard University Press.
  - [Pinker, Steven](../Page/Steven_Pinker.md "wikilink"). (1994) *The
    Language Instinct*. New York: William Morrow.
  - Rayner, K. and Pollatsek, A. (1989) *The Psychology of Reading*. New
    York:Prentice Hall.
  - Steinberg, Danny D., Hiroshi Nagata, and David P. Aline, ed. (2001)
    *[Psycholinguistics: Language, Mind and
    World](http://www.ling.ed.ac.uk/linguist/issues/13/13-55.html)*, 2nd
    ed. Longman
  - Aitchison, Jean. (1998). *The Articulate Mammal: An Introduction to
    Psycholinguistics*. Routledge.
  - Scovel, Thomas. (1998). *Psycholinguistics*. Oxford University
    Press.

## 參見

  - [語言學](../Page/語言學.md "wikilink")
  - [認知心理學](../Page/認知心理學.md "wikilink")
  - [轉換-生成語法](../Page/轉換-生成語法.md "wikilink")
  - [語言習得](../Page/語言習得.md "wikilink")
  - [關鍵期假說](../Page/關鍵期假說.md "wikilink") （）

## 外部連結

  -
[Category:语言学分支](../Category/语言学分支.md "wikilink")
[Category:心理学分支](../Category/心理学分支.md "wikilink")
[心理語言學](../Category/心理語言學.md "wikilink")