[Coronary_artery_bypass_surgery_Image_657C-PH.jpg](https://zh.wikipedia.org/wiki/File:Coronary_artery_bypass_surgery_Image_657C-PH.jpg "fig:Coronary_artery_bypass_surgery_Image_657C-PH.jpg"))中使用體外心肺機（在右上角，附四個轉盤即為推動血液的[泵](../Page/泵.md "wikilink")）\]\]
[Heart_transplant.jpg](https://zh.wikipedia.org/wiki/File:Heart_transplant.jpg "fig:Heart_transplant.jpg")和[大血管示意圖](../Page/大血管.md "wikilink")\]\]
[Пересаженное_сердце_в_грудной_клетке_реципиента.JPG](https://zh.wikipedia.org/wiki/File:Пересаженное_сердце_в_грудной_клетке_реципиента.JPG "fig:Пересаженное_сердце_в_грудной_клетке_реципиента.JPG")中的位置\]\]
**心脏外科**，指外科医师在[心脏](../Page/心脏.md "wikilink")、[主动脉或](../Page/主动脉.md "wikilink")[血管上进行手术](../Page/血管.md "wikilink")。通常包括[心臟移植](../Page/心臟移植.md "wikilink")(換心)、[先天性心脏病外科](../Page/先天性心脏病.md "wikilink")、[瓣膜病外科](../Page/心瓣.md "wikilink")、[冠心病外科](../Page/冠狀動脈性心臟病.md "wikilink")、[大血管外科](../Page/大血管轉位.md "wikilink")。以治療[缺血性心臟病](../Page/缺血性心臟病.md "wikilink")（可利用[冠狀動脈繞道手術](../Page/冠狀動脈搭橋手術.md "wikilink")）、矯正先天性心臟病，以及治療[心內膜炎等疾病所致的](../Page/心內膜炎.md "wikilink")[心瓣膜病變](../Page/心瓣.md "wikilink")。

## 歷史

因为手术需要[没有血的手术视野和靜止的心臟](../Page/无血术野.md "wikilink")，因此心脏手术十分困难，一度被认为是禁区。

已知最早的心脏外科手术是[西班牙醫師Francisco](../Page/西班牙.md "wikilink")
Romero於1801年為心包膜積水的患者施行[心包膜開窗術](../Page/心包膜開窗術.md "wikilink")[1](http://ats.ctsnetjournals.org/cgi/content/abstract/64/3/870)。1896年9月7日，[德國](../Page/德國.md "wikilink")[法蘭克福的](../Page/法蘭克福.md "wikilink")
Ludwig Rehn
醫師為一個患者修補刺穿的右心室，術後完全康復，成了第一例成功的心臟心術。那时，心脏手术的风险极高，奥地利著名医生西奥多·比尔罗特（Theodor
Billroth，1829-1894）曾说过：“在心脏上做手术，是对外科艺术的亵渎。任何一个试图进行心脏手术的人，都将落得身败名裂的下场。”

20世纪，一系列心脏外科技术发展起来。先后出现了[BT分流](../Page/BT分流.md "wikilink")、[降低体温暂停血液循环](../Page/降低体温暂停血液循环.md "wikilink")、[体外心肺循环等技术](../Page/体外心肺循环.md "wikilink")。

### BT分流

BT分流技术是在肺动脉与主动脉之间建立管道，以缓解[肺动脉狭窄造成的人体缺氧症状](../Page/法洛四联症.md "wikilink")。该技术由[海伦·陶西格](../Page/海伦·陶西格.md "wikilink")（1898-1986）和[阿尔弗雷德·布莱洛克](../Page/阿尔弗雷德·布莱洛克.md "wikilink")（1899
–1964）首创，因而得名BT分流。

### 降低体温暂停血液循环

加拿大医师威尔弗雷德·戈登·比奇洛（Wilfred Gordon Bigelow，1913-2005）和美国医师约翰·刘易斯（F. John
Lewis，1916-1993）是该领域的先驱。比奇洛最先在动物身上做降低体温暂停血液循环实验，获得成功。1952年9月2日由[C·沃尔顿·李拉海和F](../Page/C·沃尔顿·李拉海.md "wikilink").
John
Lewis兩位醫師於[明尼苏达进行了第一例成功的低溫法開心手術](../Page/明尼苏达.md "wikilink")，矯正先天性的心臟缺陷。但这种技术中心脏停跳时间一般在十分钟左右，因此不能进行复杂的心脏手术。

### 体外心肺循环

[Blausen_0468_Heart-Lung_Machine.png](https://zh.wikipedia.org/wiki/File:Blausen_0468_Heart-Lung_Machine.png "fig:Blausen_0468_Heart-Lung_Machine.png")
[Coronary_artery_bypass_surgery_Image_657B-PH.jpg](https://zh.wikipedia.org/wiki/File:Coronary_artery_bypass_surgery_Image_657B-PH.jpg "fig:Coronary_artery_bypass_surgery_Image_657B-PH.jpg")

[體外心肺循環指将患者的血液在体外的機器進行氧氣交換](../Page/體外心肺循環.md "wikilink")，再输送回体内的技术。在此期间心臟停止跳動，进行手術。经二十多年的努力，1953年，[賓州](../Page/賓州.md "wikilink")[傑弗遜醫學院的醫師约翰](../Page/傑弗遜醫學院.md "wikilink")·希舍姆·吉本（John
Heysham
Gibbon，1903-1973）在一个18岁的病人完成了第一例成功的体外心肺循环手术。但当时低温法更受关注，而且接下去的三次手术都失败，使该技术的应用陷入低潮。

1954-1955年，明尼苏达大学的外科医生[沃尔顿·李拉海](../Page/沃尔顿·李拉海.md "wikilink")（1919——1999）利用患者的父母當作
'體外心肺機'，完成了45例手术，近一半获得成功。

在吉本第一例手术5年后，明尼苏达州[梅約診所的醫師约翰](../Page/梅約診所.md "wikilink")·韦伯斯特·柯克林（John
Webster Kirklin，1917-2004)使用 '長臂猿式'
充氧泵，完成一系列成功的手術，很快地世界各地的醫師也跟進使用。后来，该技术也结合了低温法，局部降溫以減少心肌耗氧。\[1\]

## 参考资料

## 外部連結

  - [(英文)美國心臟學會](http://www.americanheart.org)

[Category:外科學](../Category/外科學.md "wikilink")

1.