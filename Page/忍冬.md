**忍冬**（[学名](../Page/学名.md "wikilink")：），是[忍冬科的一種植物](../Page/忍冬科.md "wikilink")，花称为**金银花**、也叫**忍冬藤**、**鴛鴦草**、**雙花**、**毛金銀花**、**二寶花**、**雙寶花**。\[1\]

“金银花”一名出自《[本草纲目](../Page/本草纲目.md "wikilink")》，由于忍冬花初开为白色，后转为黄色，因此得名金银花，并且花有甜美的香草香味。[果实为黑色球形的](../Page/果实.md "wikilink")[浆果](../Page/浆果.md "wikilink")，直径是，含有几粒[种子](../Page/种子.md "wikilink")\[2\]。

## 形态

[Honeysuckle-1.jpg](https://zh.wikipedia.org/wiki/File:Honeysuckle-1.jpg "fig:Honeysuckle-1.jpg")
[Lonicera_japonica,_Fruit.JPG](https://zh.wikipedia.org/wiki/File:Lonicera_japonica,_Fruit.JPG "fig:Lonicera_japonica,_Fruit.JPG")
多年生半常绿缠绕[灌木](../Page/灌木.md "wikilink")。小枝细长，中空，藤为褐色至赤褐色。卵形叶子对生，枝叶均密生柔毛和腺毛。夏季开花，苞片叶状，唇形花有淡香，外面有柔毛和腺毛，[雄蕊和花柱均伸出花冠](../Page/雄蕊.md "wikilink")，花成对生于叶腋，花色初为白色，渐变为黄色，黄白相映，故称金银花。球形[浆果](../Page/浆果.md "wikilink")，熟时黑色。

## 分布

廣產於[东亚](../Page/东亚.md "wikilink")。[中国大部分地区均产](../Page/中国.md "wikilink")，主要产於[黑龙江](../Page/黑龙江.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")、[宁夏](../Page/宁夏.md "wikilink")、[青海](../Page/青海.md "wikilink")、[新疆](../Page/新疆.md "wikilink")、[河南](../Page/河南.md "wikilink")、[海南](../Page/海南.md "wikilink")、[西藏等地](../Page/西藏.md "wikilink")，[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")、[日本亦有分布](../Page/日本.md "wikilink")。生长在50－1500米的地区。多野生于较湿润的地带，如溪河两岸、湿润山坡灌丛、疏林中。

在南美、北美、夏威夷和澳大利亞等地是入侵種。

## 药用

[中医认为金银花](../Page/中医.md "wikilink")（即忍冬的花蕾）性寒，味甘，略有酸味，具有[清热解毒](../Page/清热.md "wikilink")、凉血、[通经活络](../Page/经络.md "wikilink")、收敛的作用。[广东民间常用之泡茶喝](../Page/广东.md "wikilink")，以清热解毒，是製作[五花茶的其中一種花](../Page/五花茶.md "wikilink")。中医称，金银花能宣散风热，还善清解血毒，用于各种热性病，如身热、发疹、发斑、热毒疮痈、咽喉肿痛等症状。

## 别名

忍冬、金银藤（[江西](../Page/江西.md "wikilink")[铅山](../Page/铅山.md "wikilink")、[云南](../Page/云南.md "wikilink")[楚雄](../Page/楚雄.md "wikilink")），银藤（[浙江](../Page/浙江.md "wikilink")[临海](../Page/临海.md "wikilink")、[江苏](../Page/江苏.md "wikilink")），二色花藤（[上海](../Page/上海.md "wikilink")），二宝藤、右转藤（[四川](../Page/四川.md "wikilink")），子风藤（[浙江](../Page/浙江.md "wikilink")[丽水](../Page/丽水.md "wikilink")），密桷藤（江西铅山），鸳鸯藤（[福建](../Page/福建.md "wikilink")），二花（河南封丘），老翁须（常用中草药图谱）等。\[3\]

## 註釋

## 参考文献

  - 《天未然金银花茶》

## 外部链接

  -
  - [忍冬
    Rendong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01078)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [忍冬藤
    Rendongteng](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00144)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [忍冬藤 Ren Dong
    Teng](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00501)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [金銀花
    Jinyinhua](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00305)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [金銀花 Jin Yin
    Hua](http://libproject.hkbu.edu.hk/was40/outline?page=1&channelid=44273&searchword=%E9%87%91%E9%8A%80%E8%8A%B1&sortfield=+name_chi_sort)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[藤](../Category/藥用植物.md "wikilink")
[Category:忍冬属](../Category/忍冬属.md "wikilink")
[Category:中草藥](../Category/中草藥.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")
[Category:爬藤植物](../Category/爬藤植物.md "wikilink")
[Category:耐旱植物](../Category/耐旱植物.md "wikilink")

1.
2.
3.