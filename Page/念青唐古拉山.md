[Nyainqentanglha.JPG](https://zh.wikipedia.org/wiki/File:Nyainqentanglha.JPG "fig:Nyainqentanglha.JPG")所拍攝的念青唐古拉山主峰\]\]
**念青唐古拉山脉**（，意为）位于[西藏自治区中东部](../Page/西藏.md "wikilink")。近东西走向。西侧[冈底斯山脉](../Page/冈底斯山脉.md "wikilink"),东侧[横断山脉](../Page/横断山脉.md "wikilink")。全长1400公里，平均宽80公里。平均海拔5000～6000米。

## 地理

[Nyainqentanglha_from_Qingzang_Railway.jpg](https://zh.wikipedia.org/wiki/File:Nyainqentanglha_from_Qingzang_Railway.jpg "fig:Nyainqentanglha_from_Qingzang_Railway.jpg")所見的念青唐古拉山脉\]\]
念青唐古拉山脉是[雅鲁藏布江与](../Page/雅鲁藏布江.md "wikilink")[怒江分水岭](../Page/怒江.md "wikilink")。也是高原上寒冷气候带与温暖（凉）气候带的分界线。主峰[念青唐古拉峰海拔](../Page/念青唐古拉峰.md "wikilink")7162米。

## 地质

山脉形成于燕山运动晚期，为一系列向东逆冲的褶皱山带，沿山带南侧均有深大断裂通过。

## 冰川

念青唐古拉山脉有7080条冰川，总面积达10,701平方公里，在中国的几大山脉中冰川面积排名第二。山脉西段降水少，冰川属于亚大陆型冰川，而山脉东段受印度洋西南季风影响降水多，雪线海拔低，是中国最大的海洋型冰川地区，冰川分布集中，冰川数占了整个山脉冰川总数的2/3，冰川面积占了整个山脉冰川总面积的5/6，也是地球上中低纬地区冰川作用最强的中心之一。\[1\]

其中有27条冰川长度超过10公里。如易贡八玉沟的[卡钦冰川长达](../Page/卡钦冰川.md "wikilink")33公里，冰川末端海拔仅2530米，为西藏最大冰川，也是中国最大的海洋性冰川。

## 气候

山脉东段受印度洋西南季风影响，降水多；山脉西段位于半干旱气候地区，降水少。　

## 宗教信仰

[苯教信仰的山神主要有](../Page/苯教.md "wikilink")4位：雅拉香波（）山神、库拉卡日（）山神、诺吉康桑山神、念青唐古拉山神，分别居住于西藏的东南西北四方。四大山神加上其他五位山神是“世界形成之九神”。\[2\]

## 參考文獻

[N](../Category/西藏地理.md "wikilink") [N](../Category/中国山脉.md "wikilink")

1.
2.