[How_to_shoot_down_airplane.jpg](https://zh.wikipedia.org/wiki/File:How_to_shoot_down_airplane.jpg "fig:How_to_shoot_down_airplane.jpg")

《**怎樣打飛機**》是[中國人民解放軍](../Page/中國人民解放軍.md "wikilink")[廣州軍區司令部編印的一本作戰手冊](../Page/廣州軍區.md "wikilink")，封面標示出版日期為1965年2月15日，內容主要介紹使用[輕武器擊落敵軍](../Page/輕武器.md "wikilink")[戰機的方法要訣](../Page/戰機.md "wikilink")。

2006年初，該小冊子封面被-{zh-cn:[中国大陆](../Page/中国大陆.md "wikilink");zh-hk:[中國大陸](../Page/中國大陸.md "wikilink");zh-tw:[中國](../Page/中國.md "wikilink")}--{zh-cn:网民;zh-tw:網友;}-張貼在[討論區上](../Page/討論區.md "wikilink")；由於「打飛機」一詞在近年代的[粵語中是男性](../Page/粵語.md "wikilink")[自慰之意](../Page/自慰.md "wikilink")，圖片引起熱烈討論與該手冊內容之臆測。

## 「打飛機」之由來

[中华人民共和国成立初期](../Page/中华人民共和国.md "wikilink")，經歷[抗日戰爭和](../Page/中国抗日战争.md "wikilink")[国共内战之後](../Page/国共内战.md "wikilink")，經濟崩潰，軍隊無力裝備重型武器，實力遠遜於西方，就算是個人裝備也很簡陋，所以[解放軍希望以](../Page/中国人民解放军.md "wikilink")[士兵的技術補充不足](../Page/军人.md "wikilink")。當時士兵的[文化水平並不高](../Page/文化.md "wikilink")，於是解放軍在《[軍訓手冊](../Page/軍訓手冊.md "wikilink")》裡面附設圖解，簡略地教導士兵如何以[步枪擊落](../Page/步枪.md "wikilink")[飞机](../Page/飞机.md "wikilink")，使得士兵可掌握箇中竅門。

那時，以輕武器擊落戰機的行為，稱作「打飛機」，在昔日的解放軍中普遍使用。1943年，[八路军战士](../Page/八路军.md "wikilink")[宋岭春曾用](../Page/宋岭春.md "wikilink")[三八式步枪击落一架日军飞机](../Page/三八式步枪.md "wikilink")\[1\]。[毛泽东在](../Page/毛泽东.md "wikilink")1952年7月接見參加志願軍歸國代表團時，得悉[王海大隊](../Page/王海.md "wikilink")[空軍飛行員](../Page/空軍.md "wikilink")[趙寶桐參與](../Page/趙寶桐.md "wikilink")[朝鮮戰爭時曾擊落數架](../Page/朝鮮戰爭.md "wikilink")[美國](../Page/美國.md "wikilink")[戰機時](../Page/戰機.md "wikilink")，說：「打飛機，我不如趙寶桐。」

[-{zh-hans:朝鲜战争;zh-hant:韓戰;zh-cn:朝鲜战争;zh-tw:韓戰;zh-hk:韓戰;zh-sg:韩战;zh-mo:韓戰;}-爆發時](../Page/朝鲜战争.md "wikilink")，[解放军以](../Page/中国人民解放军.md "wikilink")[中国人民志愿军的名義參加戰事](../Page/中国人民志愿军.md "wikilink")。1951年3月初一天的[清晨](../Page/清晨.md "wikilink")，志願軍戰士[關崇貴成功以](../Page/關崇貴.md "wikilink")[輕機槍射出](../Page/輕機槍.md "wikilink")14發子彈擊落一架[美軍](../Page/美軍.md "wikilink")[P-51螺旋槳戰鬥機](../Page/P-51戰鬥機.md "wikilink")。關崇貴這次創舉，使他成為解放軍史上首名成功「打飛機」先例。他後來被授予「一級戰鬥英雄」稱號，並記了特等功。，但確實有助激勵士氣；自此，「打飛機」這種集技巧與運氣於一身的技術發揚光大。

## 書中內容與重議論

1961年[越戰爆發](../Page/越南战争.md "wikilink")，1965年戰爭形式惡化。[中华人民共和国国务院出於](../Page/中华人民共和国国务院.md "wikilink")[意識形態和](../Page/意識形態.md "wikilink")[地缘政治關係](../Page/地缘政治学.md "wikilink")，決定軍事援助[北越](../Page/北越.md "wikilink")，提供大量防空武器，利用地面的高射炮對付美國戰機的空襲。儘管如此，解放軍的武器依然比不上美軍先進的武器，面對美國戰機的襲擊威脅，解放軍欲以運用「打飛機」技術還擊。

以上背景下，中國人民解放軍廣州軍區司令部編印了一本作戰手冊《怎樣打飛機》，內容完全詳解使用輕武器擊落戰機的要訣。薄薄數頁除文字解釋，還附上彩色插畫與[換算表](../Page/換算表.md "wikilink")，指導士兵計算射擊距離和角度。

以下節錄自《怎樣打飛機》：

隨著科技進步，飛機速度遠比以前快得多，機體裝甲防護不斷強化。而[步槍](../Page/步槍.md "wikilink")、[機槍則朝向小口徑](../Page/機槍.md "wikilink")、輕量化發展，其[彈頭動能相應減少](../Page/彈頭.md "wikilink")。此消彼長下，輕武器對空中目標射擊的作用也變得意義不大，擊中飛機的機會近乎零，「打飛機」亦變成空談。但[美伊戰爭中](../Page/美伊戰爭.md "wikilink")，[伊拉克南部](../Page/伊拉克.md "wikilink")[農民曾於](../Page/農民.md "wikilink")2003年3月24日以[AK-47擊落美國](../Page/AK-47.md "wikilink")[AH-64](../Page/AH-64.md "wikilink")「阿帕奇」[直升機](../Page/直升機.md "wikilink")\[2\]，並俘虜兩名美軍\[3\]，令全[世界嘖嘖稱奇](../Page/世界.md "wikilink")。。

## 衍生

2006年初，有中國大陸的-{zh-cn:网民;zh-tw:網友;}-上傳《怎樣打飛機》封面並引起熱潮。因[俚語](../Page/俚語.md "wikilink")「打飛機」一詞與[手淫相關](../Page/自慰.md "wikilink")\[4\]，很多人以此在网络上戏谑。至2006年9月2日，有網民在[貓撲網上傳書中其他內容](../Page/貓撲網.md "wikilink")\[5\]，再度引起廣泛轉載與議論。

## 参考文献

### 引用

### 来源

  - [中国史上第一个用机枪打下飞机的士兵](http://www.cchere.com/article/462300)─《鼎盛军事论坛》2005年7月27日

  - [访抗美援朝战争一级战斗英雄赵宝桐](http://www.people.com.cn/GB/paper83/2026/324449.html)─《時代潮》2000年第11期

  -
[Category:广州军区](../Category/广州军区.md "wikilink")
[Category:1965年中国书籍](../Category/1965年中国书籍.md "wikilink")
[Category:1960年代中国军事](../Category/1960年代中国军事.md "wikilink")
[Category:中国军事书籍](../Category/中国军事书籍.md "wikilink")
[武](../Category/恶搞文化.md "wikilink")

1.  [枪起机落：用步枪击落日寇飞机的八路军宋岭春](http://tech.sina.com.cn/d/2007-07-30/08041644449.shtml)，[CCTV](../Page/CCTV.md "wikilink")「科技博览」节目，2007年7月30日。
2.
3.
4.
5.