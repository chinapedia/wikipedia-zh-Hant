[Schrodingers_cat.svg](https://zh.wikipedia.org/wiki/File:Schrodingers_cat.svg "fig:Schrodingers_cat.svg")
(1935)，是一只或死或活的猫，而这只猫到底是死是活则决定于较早的一次随机事件。这个由常见物品构成的思想实验相对通俗地具体化了[哥本哈根詮釋中的问题](../Page/哥本哈根詮釋.md "wikilink")。\]\]

**思想實驗**是指：使用想像力去進行的[實驗](../Page/實驗.md "wikilink")，所做的都是在現實中無法做到（或現實未做到）的實驗。例如愛因斯坦有關相對運動的著名思想實驗，又例如在[愛因斯坦和](../Page/愛因斯坦.md "wikilink")[英費爾德合著的科普讀物](../Page/利奧波德·英費爾德.md "wikilink")《[物理学的进化](../Page/物理学的进化.md "wikilink")》中，就有一個實驗要求讀者想像一個平滑，無[摩擦力的地面及球體進行實驗](../Page/摩擦力.md "wikilink")，但這在現實（或暫時）是做不到的。思想實驗需求的是想像力，而不是感官。

## 著名思想實驗

### 物理學

在[物理學裏](../Page/物理學.md "wikilink")，思想實驗是很有用的理論工具：

  - [牛顿大炮](../Page/牛顿大炮.md "wikilink")
  - [雙縫實驗](../Page/雙縫實驗.md "wikilink")
  - [EPR弔詭](../Page/EPR弔詭.md "wikilink")
  - [拉普拉斯妖](../Page/拉普拉斯妖.md "wikilink")
  - [马克士威妖](../Page/马克士威妖.md "wikilink")
  - [移動中的磁鐵與導體問題](../Page/移動中的磁鐵與導體問題.md "wikilink")
  - [薛丁格貓](../Page/薛丁格貓.md "wikilink")
  - [維格納的友人](../Page/維格納的友人.md "wikilink")
  - [孿生子弔詭](../Page/孿生子弔詭.md "wikilink")
  - [惠勒延遲選擇實驗](../Page/惠勒延遲選擇實驗.md "wikilink")
  - 伽利略用来推翻亚里士多德“越重的物体下落越快”的双球实验。

亚里士多德称越重的物体下落越快，那么我们来考虑这样一个实验：一重一轻两个球，用绳子连接起来，一起下落。一方面，轻的球下落速度慢，重的球下落速度快，在绳子的牵引下整体速度将会介于两者分别下落的速度之间。另一方面，将两个球作为一个整体考察，其质量比任意一个单球都要重，因此下落速度比任意一个球单独下落速度都要快。于是得出矛盾。

有传说称伽利略在比萨斜塔上亲自做过这个实验，实际上这仅仅是一个思想实验，并没有付诸行动。\[1\]

### 哲學

关于哲学的思想试验包括伦理学和认知论的辩驳。

  - [有轨电车难题](../Page/有轨电车难题.md "wikilink")
  - [洞穴奇案](../Page/洞穴奇案.md "wikilink")
  - [忒修斯之船](../Page/忒修斯之船.md "wikilink")
  - [卡涅阿德斯船板](../Page/卡涅阿德斯船板.md "wikilink")
  - [定时炸弹 (实验)](../Page/定时炸弹_\(实验\).md "wikilink")
  - [葛梯尔问题](../Page/葛梯尔问题.md "wikilink")
  - [桶中之腦](../Page/桶中之腦.md "wikilink")
  - [莊周夢蝶](../Page/莊周夢蝶.md "wikilink")
  - [莫利紐茲問題](../Page/莫利紐茲問題.md "wikilink")
  - [無知之幕](../Page/無知之幕.md "wikilink")
  - [變成蝙蝠會怎樣？](../Page/變成蝙蝠會怎樣？.md "wikilink")
  - [孿生地球](../Page/孿生地球.md "wikilink")
  - [帕斯卡的賭注](../Page/帕斯卡的賭注.md "wikilink")
  - [知識論證](../Page/知識論證.md "wikilink")

### 數學

### 生物

### 人工智能

  - [中文房間](../Page/中文房間.md "wikilink")
  - [图灵测试](../Page/图灵测试.md "wikilink")
  - [哲學殭屍](../Page/哲學殭屍.md "wikilink")

一个由图灵提出的关于判断机器是否能够思考的著名试验。
如果一个人（代号C）使用测试对象皆理解的语言去询问两个他不能看见的对象任意一串问题。对象为：一个是正常思维的人（代号B）、一个是机器（代号A）。如果经过若干询问以后，C不能得出实质的区别来分辨A与B的不同，则此机器A通过[图灵试验](../Page/图灵试验.md "wikilink")。

### 經濟

  - [囚徒困境](../Page/囚徒困境.md "wikilink")

### 其它

  - [戴森球](../Page/戴森球.md "wikilink")

## 相關

  - [奶油貓悖論](../Page/奶油貓悖論.md "wikilink")

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
{{-}}

[思想實驗](../Category/思想實驗.md "wikilink")
[T](../Category/物理学实验.md "wikilink")

1.    An exception is [Drake (1978, pp. 19–21,
    414–416)](../Page/#Reference-Drake-1978.md "wikilink"), who argues
    that the experiment did take place, more or less as Viviani
    described it.