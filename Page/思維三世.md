[教宗](../Page/教宗.md "wikilink")**思維三世**（，？—1062年或1063年），本名**Giovanni dei
Crescenzi**，於1045年1月20日至1045年3月9日岀任教宗。

## 譯名列表

  - 西爾維斯特三世：來自[大英簡明百科知識庫](http://wordpedia.britannica.com/concise/content.aspx?id=15618&hash=#EBCHash:15618#)與[大英百科全書線上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00055381.html)。
  - 西尔韦斯特三世、西尔维斯特三世：來自《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版。
  - 思維：來自[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)。
  - 席維斯三世
  - 希尔维斯特三世、[希爾维斯特三世](../Page/希爾维斯特三世.md "wikilink")

[S](../Category/教宗.md "wikilink") [S](../Category/羅馬人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")