[教宗](../Page/教宗.md "wikilink")**思齊四世**（，），原名**Pietro Martino
Boccapecora**，於1009年7月31日至1012年5月12日岀任教宗。

## 譯名列表

  - 塞爾吉烏斯四世：來自[大英百科全書線上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00057171.html)。
  - 瑟吉厄斯四世、塞尔吉乌斯四世：來自《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版。
  - 思齊四世：來自[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)。

[S](../Category/教宗.md "wikilink") [S](../Category/羅馬人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")