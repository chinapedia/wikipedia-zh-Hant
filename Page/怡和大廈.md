[HK_Jardine_House_Lift_Lobby.jpg](https://zh.wikipedia.org/wiki/File:HK_Jardine_House_Lift_Lobby.jpg "fig:HK_Jardine_House_Lift_Lobby.jpg")
[Connaught_Garden_201506.jpg](https://zh.wikipedia.org/wiki/File:Connaught_Garden_201506.jpg "fig:Connaught_Garden_201506.jpg")

**怡和大廈**（[英文](../Page/英文.md "wikilink")：**Jardine
House**）亦稱**康樂大廈**（因旁邊為康樂廣場而得名；也是1988年前的專稱），位於[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[康樂廣場](../Page/康樂廣場.md "wikilink")1號，樓高52層，由中環大業主[置地公司興建](../Page/置地公司.md "wikilink")，於1973年落成，是香港首幢[摩天大樓](../Page/香港摩天大樓.md "wikilink")，在1970年代是香港及[亞洲最高的](../Page/亞洲.md "wikilink")[建築物](../Page/建築物.md "wikilink")，由建築師[木下一設計](../Page/木下一.md "wikilink")。

## 歷史

### 舊渣甸大廈

舊渣甸大廈與建於1948年，樓高16層，位處畢打街20號（現時為[會德豐大廈](../Page/會德豐大廈.md "wikilink")）。

### 新怡和大廈

1970年6月1日，[香港政府把](../Page/香港政府.md "wikilink")[中環填海區的](../Page/香港填海工程#歷史.md "wikilink")「地王」拍賣，土地面積53,000平方呎，底價5300万港元，吸引18個財團競投，置地出價2亿5800萬港元投得，是當時香港最高每呎地價的紀錄。大廈由[金門建築興建](../Page/金門建築.md "wikilink")，由動工至[平頂只用了](../Page/平頂.md "wikilink")16個月，建築速度亦是當時的一項紀錄。怡和大廈於1973年落成，成為當時香港最高的建築物，至1980年由[合和中心取代](../Page/合和中心.md "wikilink")。

1988年，[怡和集團總部搬遷至本建築內](../Page/怡和集團.md "wikilink")（康樂大廈），故此康樂大廈亦改名為怡和大廈。

<File:Tcitp> d219 old premises of jardine matheson and co.jpg|第一代渣甸大廈
<File:Ice> House Street 1930s.jpg|第二代渣甸大廈

## 特色

怡和大廈是本港第一幢[摩天大樓](../Page/摩天大樓.md "wikilink")，以獨特的[圓窗設計見稱](../Page/圓形.md "wikilink")，成為香港的[地標](../Page/地標.md "wikilink")，迄今未變。圓形的「月洞門」窗戶，秉承了[中国傳統的建築特色](../Page/中国.md "wikilink")，臨窗遠眺，[維多利亞港和](../Page/維多利亞港.md "wikilink")[太平山景色一覽無遺](../Page/太平山.md "wikilink")。

怡和大廈位處黃金地段，不少本地和國際的大機構均是怡和大廈的長期租戶，如：[巴斯夫](../Page/巴斯夫.md "wikilink")、[勞力士](../Page/勞力士.md "wikilink")、[KPF建築師事務所香港分公司](../Page/KPF建築師事務所.md "wikilink")、[合生創展等](../Page/合生創展.md "wikilink")。

大廈高層為[辦公室](../Page/辦公室.md "wikilink")，低層設有[商場及](../Page/商場.md "wikilink")[美心集團旗下食肆](../Page/美心食品有限公司.md "wikilink")，鄰接[交易廣場](../Page/交易廣場.md "wikilink")、[香港郵政總局及](../Page/香港郵政總局.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[香港站](../Page/香港站.md "wikilink")，並以[行人天橋連接](../Page/行人天橋.md "wikilink")。

怡和大廈原先的外牆以[混凝土及白色](../Page/混凝土.md "wikilink")[紙皮石為主](../Page/紙皮石.md "wikilink")，到1984年[外牆更換為鋁製](../Page/外牆.md "wikilink")，看起來更有[現代感](../Page/現代化.md "wikilink")，也沒有紙皮石飛脫傷人的風險。\[1\]1988年再翻新室內公共空間，原本灰啡色[雲石牆身被更換為](../Page/雲石.md "wikilink")[意大利綠色](../Page/意大利.md "wikilink")[大理石](../Page/大理石.md "wikilink")，假天花換上德國製白色金屬板，也加裝高級商廈常見的[旋轉門系統](../Page/旋轉門.md "wikilink")，客運升降機被更換為[Otis
Elevonic
401](../Page/奧的斯電梯公司.md "wikilink")。2003年及2008年曾兩度度翻新升降機，其中2008年更將升降機營辦商從[奧的斯轉換至](../Page/奧的斯電梯公司.md "wikilink")[怡和迅達](../Page/迅達集團.md "wikilink")，並加裝[Miconic
10升降機分配系統](../Page/Miconic_10.md "wikilink")

## 樓層分佈

  - 4樓：中倫律師事務所
  - 5樓：何耀棣律師事務所
  - 6樓：香港商歷峯亞太有限公司（Richemont Asia Pacific Ltd）
  - 8樓：[KPF建築事務所](../Page/KPF建築事務所.md "wikilink")（香港分公司）
  - 9樓：Jaeger-LeCoultre North Asia（919室）
  - 11樓：ashurst
  - 14樓：勞力士（香港）有限公司
  - 15樓：Optima Capital（1501室）、Wilson Sonsini Goodrich &
    Rosati（1509室）、aimHigher Consultancy
    Limited（1513室-1516室）、[阿根廷駐港總領事館](../Page/阿根廷.md "wikilink")（Consulado
    General en HONG KONG）（1517室-1519室）
  - 16樓：富鑽集團富鑽有限公司（1608室）
  - 17樓：瑞典北歐斯安銀行（SEB)（1701室-1709室及1720室）、Synergy Health（Hong
    Kong）Ltd.（1710室）、方正香港金控、方正證券（香港）（1710-1719室）
  - 20樓：Neuberger Berman、碧雲資本管理有限公司（2007室-2009室）
  - 21樓：普信集團（T. Rowe Price）
  - 22樓：韓國投資證券亞洲有限公司（2220室）
  - 24樓：[偉祿美林證券有限公司](../Page/偉祿美林證券有限公司.md "wikilink")（2402室)、[偉祿集團控股有限公司](../Page/偉祿集團控股有限公司.md "wikilink")（2403室-2410室）

<!-- end list -->

  - 25樓至26樓：胡關李羅律師行
  - 27樓至31樓：[高偉紳律師事務所](../Page/高偉紳律師事務所.md "wikilink")（Clifford Chance）
  - 32樓：泰國匯商銀行（3209室）
  - 33樓：Mannheimer Swartling Advokatbyrå AB、永靈通（3318室）、JuTeng
    International Holdings Ltd.(3311-3312)
  - 34樓：3W Partners（3407室-3409室）
  - 35樓：Hamon Investment Group（3510室-3515室）；上達資本（3501室）
  - 36樓：景林資産管理香港有限公司（Greenwoods Asset Management Ltd.）
  - 38樓：Norton Rose Fulbright Hong Kong
  - 39樓：王珮玲律師事務所（3908A室）、TCW Group（3912室）
  - 40樓：Chiu & Partners、Mirabaud Group（4005室）
  - 41樓：博時基金（4109室）、Princeville Global
  - 42樓至46樓：巴斯夫（BASF）
  - 47樓：司力達律師樓（Slaughter and May）

## 圖片

<File:Jardine> House view1 2015.jpg|大廈基座 <File:Connaught> Garden Henry
Moore Sculpture 2006.JPG|康樂花園1977年增設大型[雕塑](../Page/雕塑.md "wikilink")「對環」
<File:Jardine> House Night view 201703.jpg|晚上的怡和大廈

## 參看

  - [香港摩天大樓](../Page/香港摩天大樓.md "wikilink")
  - [幻彩詠香江](../Page/幻彩詠香江.md "wikilink")
  - [中區行人天橋系統](../Page/中區行人天橋系統.md "wikilink")
  - [置地公司](../Page/置地公司.md "wikilink")

## 參考資料

## 外部連結

  - [怡和大廈
    (英語)](https://web.archive.org/web/20070714061526/http://www.hkland.com/commercial_property/hongkong_properties/jardinehouse/index.html)
  - [摩天漢世界的介紹](http://www.skyscrapers.cn/city/asia/cn/hk/hk_skyscrapers_Jardine_House.htm)
  - 《[西洋建築及其他（1842年以後）](https://web.archive.org/web/20050507170726/http://intro2arch.arch.hku.hk/hist/heritage/c5.htm)》，[香港大學](../Page/香港大學.md "wikilink")

{{-}}

[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink") [Category:中西區寫字樓
(香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港置地物業](../Category/香港置地物業.md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")
[Category:怡和](../Category/怡和.md "wikilink")
[Category:幻彩詠香江](../Category/幻彩詠香江.md "wikilink")
[Category:1973年完工建築物](../Category/1973年完工建築物.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")

1.  [康樂大廈：曾經的亞洲第一高樓](http://news.hkheadline.com/figure/index.asp?id=54)
    頭條日報 圖說往昔