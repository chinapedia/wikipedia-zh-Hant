**急水溪**位於[台灣南部](../Page/台灣.md "wikilink")，屬於[中央管河川](../Page/中央管河川.md "wikilink")，幹流長度65公里，流域面積379平方公里。幹流流經[台南市](../Page/臺南市.md "wikilink")：[白河區](../Page/白河區.md "wikilink")、[新營區](../Page/新營區.md "wikilink")、[鹽水區](../Page/鹽水區.md "wikilink")、[學甲區](../Page/學甲區.md "wikilink")、[柳營區](../Page/柳營區.md "wikilink")、[東山區](../Page/東山區_\(臺南市\).md "wikilink")、[北門區](../Page/北門區.md "wikilink")、[後壁區](../Page/後壁區.md "wikilink")、[六甲區](../Page/六甲區.md "wikilink")、[下營區等行政區](../Page/下營區.md "wikilink")，主要支流有六重溪、龜重溪等。

急水溪上游稱為[白水溪](../Page/白水溪.md "wikilink")，發源於[嘉義縣](../Page/嘉義縣.md "wikilink")[中埔鄉與](../Page/中埔鄉.md "wikilink")[大埔鄉交界處的](../Page/大埔鄉.md "wikilink")[凍子頂](../Page/凍子頂山.md "wikilink")\[1\]\[2\]\[3\]。流經台南市白河區[關子嶺北側](../Page/關子嶺.md "wikilink")，進入[白河水庫淹沒區](../Page/白河水庫.md "wikilink")。再流至白河區南側與[六重溪匯集後](../Page/六重溪.md "wikilink")，始稱急水溪。其後又匯集另一大支流[龜重溪](../Page/龜重溪.md "wikilink")，最終於北門區注入[台灣海峽](../Page/台灣海峽.md "wikilink")。若把嘉義算中台灣、台南算南台灣的話，急水溪-關子嶺一線恰好形成中南部的地理分界。

## 急水溪水系主要河川

以下由下游至上游列出水系主要河川，幹流河道以粗體字表示：

  - **急水溪**：[台南市](../Page/臺南市.md "wikilink")、[嘉義縣](../Page/嘉義縣.md "wikilink")
    - 65公里
      - [龜重溪](../Page/龜重溪.md "wikilink")：台南市[柳營區](../Page/柳營區.md "wikilink")、[東山區](../Page/東山區_\(臺南市\).md "wikilink")、[白河區](../Page/白河區.md "wikilink")
        - 35公里
          - [鹿寮溪](../Page/鹿寮溪_\(臺南市\).md "wikilink")：台南市東山區
          - [茄苳溪](../Page/茄苳溪_\(臺南市\).md "wikilink")：台南市東山區
          - [北寮溪](../Page/北寮溪.md "wikilink")：台南市東山區、白河區
      - [六重溪](../Page/六重溪.md "wikilink")：台南市東山區、白河區 - 35公里
          - [石雅溪](../Page/石雅溪.md "wikilink")：台南市白河區
      - **[白水溪](../Page/白水溪.md "wikilink")**：台南市白河區、嘉義縣[中埔鄉](../Page/中埔鄉.md "wikilink")、[大埔鄉](../Page/大埔鄉.md "wikilink")
        - 20公里
          - [柚子頭溪](../Page/柚子頭溪.md "wikilink")：台南市白河區
          - **[三重溪](../Page/三重溪.md "wikilink")**：台南市白河區、嘉義縣中埔鄉、大埔鄉

## 主要橋樑

以下由河口至源頭列出主流上之主要橋樑：

### 急水溪河段

[急水溪橋.jpg](https://zh.wikipedia.org/wiki/File:急水溪橋.jpg "fig:急水溪橋.jpg")

  - [王爺港橋](../Page/王爺港橋.md "wikilink")（[快速公路](../Page/台灣省道快速公路.md "wikilink")[台61線](../Page/台61線.md "wikilink")）
  - [五王大橋](../Page/五王大橋.md "wikilink")（[省道](../Page/省道.md "wikilink")[台17線](../Page/台17線.md "wikilink")）
  - [筏子頭橋](../Page/筏子頭橋.md "wikilink")（鄉道[南1線](../Page/南1線.md "wikilink")）
  - [二港橋](../Page/二港橋.md "wikilink")（鄉道[南7線](../Page/南7線.md "wikilink")）
  - [宅港橋](../Page/宅港橋.md "wikilink")（省道[台19線](../Page/台19線.md "wikilink")）
  - 無名橋
  - [坔頭港大橋](../Page/坔頭港大橋.md "wikilink")（鄉道[南67線](../Page/南67線.md "wikilink")）
  - 無名橋
  - [國道1號急水溪橋](../Page/國道1號急水溪橋.md "wikilink")（[國道1號](../Page/國道1號.md "wikilink")）
  - 無名橋
  - [急水溪橋](../Page/急水溪橋.md "wikilink")（省道[台19甲線](../Page/台19甲線.md "wikilink")）
  - 急水溪過水路橋（鄉道[南70線](../Page/南70線.md "wikilink")）
  - 無名橋（鄉道[南75線](../Page/南75線.md "wikilink")）
  - [雙營橋](../Page/雙營橋.md "wikilink")（柳營外環道）
  - 無名橋
  - 台糖鐵路橋（台糖鐵路）
  - [急水溪橋](../Page/急水溪橋.md "wikilink")（鄉道[南69線](../Page/南69線.md "wikilink")、[南95線共線](../Page/南95線.md "wikilink")）
  - [急水溪橋](../Page/急水溪橋.md "wikilink")（[台灣鐵路](../Page/台灣鐵路.md "wikilink")[縱貫線](../Page/縱貫線.md "wikilink")）
  - [急水溪橋](../Page/急水溪橋.md "wikilink")（省道[台1線](../Page/台1線.md "wikilink")）
  - [急水溪橋](../Page/急水溪橋.md "wikilink")（[台灣高鐵](../Page/台灣高鐵.md "wikilink")）
  - [新東橋](../Page/新東橋.md "wikilink")（鄉道[南100線](../Page/南100線.md "wikilink")）
  - 無名橋
  - 無名橋（鄉道[南81線](../Page/南81線.md "wikilink")）
  - 台糖鐵路橋（台糖鐵路）
  - [青葉橋](../Page/青葉橋.md "wikilink")（[縣道165號](../Page/縣道165號.md "wikilink")）

### 白水溪河段

  - [春暉橋](../Page/春暉橋.md "wikilink")（鄉道[南101線](../Page/南101線.md "wikilink")）
  - [白河橋](../Page/白河橋.md "wikilink")（[縣道172號](../Page/縣道172號.md "wikilink")）
  - [國道3號白水溪橋](../Page/國道3號白水溪橋.md "wikilink")（[國道3號](../Page/國道3號.md "wikilink")）
  - [甘宅二號橋](../Page/甘宅二號橋.md "wikilink")（鄉道[南93線](../Page/南93線.md "wikilink")）
  - [白河水庫壩底道路](../Page/白河水庫.md "wikilink")
  - 白河水庫壩頂道路
  - [白水溪橋](../Page/白水溪橋.md "wikilink")（鄉道[南98線](../Page/南98線.md "wikilink")）
  - [善忍橋](../Page/善忍橋.md "wikilink")

### 三重溪河段

  - 無名橋（[縣道172號](../Page/縣道172號.md "wikilink")）

## 拜溪文化

其拜溪文化有「中庄仔拜溪」（今[台南市](../Page/台南市.md "wikilink")[下營區甲中里](../Page/下營區.md "wikilink")）和「五間厝拜溪」（今[台南市](../Page/台南市.md "wikilink")[新營區五興里](../Page/新營區.md "wikilink")）兩處，都是當地的重大民間祭典之一。起因在清治時期急水溪不斷氾濫沖刷改道，水患危及河堤南北兩岸村莊(中庄仔和五間厝)，庄民開始祭拜河神，以求平安。「中庄仔拜溪」於農曆8月16日祭拜河神，而「五間厝拜溪」是農曆8月15日祭拜，儀式特徵是這兩天庄民會抬著祭品到河道祭拜，無任何特定儀式，只有擺祭品與祭拜行為。持續到午後4點，酒過三巡後，焚燒紙錢，拜溪圓滿達成流，傳至今不斷。至今轉化成飲水思源的拜溪模式，感念河道改道，免於水患之苦。

## 相關條目

  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")

## 參考文獻

## 參考資料

  - [讓我們看河去(重要河川)--
    急水溪-經濟部水利署](https://web.archive.org/web/20080102020905/http://www.wra.gov.tw/ct.asp?xItem=20080&CtNode=4361)

[急水溪水系](../Category/急水溪水系.md "wikilink")
[Category:台南市河川](../Category/台南市河川.md "wikilink")
[Category:嘉義縣河川](../Category/嘉義縣河川.md "wikilink")

1.  [《臺南市東山區南勢社區農村再生計畫》，臺南市東山區南勢社區發展協會，2013年6月8日，第10頁](http://www.tainan.gov.tw/tn/agron/warehouse/G00000/26.pdf)
2.
3.  [《環境地質災害查詢系統》，經濟部中央地質調查所官方網站](http://envgeo.moeacgs.gov.tw/moeapaper/rock/94191se.htm)