**恐嚇**是以加害他人權益或公共利益等事項威脅他人，使他人心理感到畏怖恐慌，在許多國家是一项[刑事](../Page/刑事.md "wikilink")[犯罪](../Page/犯罪.md "wikilink")，無論有無向對方動粗，無論是否行使[暴力行動](../Page/暴力.md "wikilink")，即使只是[語言上威脅受害者](../Page/語言.md "wikilink")(對方)，有死亡威脅或傷害當事人或其[家族](../Page/家族.md "wikilink")、[公司](../Page/公司.md "wikilink")、[財產權等](../Page/財產權.md "wikilink")。包括死亡威脅、詐彈威脅、以自殺做威脅等。若意圖以此方式來獲取他人財物或利益而實行者，稱為「恐嚇取財」。

影射也可能是恐嚇，使得對方感覺到受威脅。犯此罪行者可能自稱[黑社會或者跟警察司法人員或者跟某政治人物關係良好](../Page/黑社會.md "wikilink")，向[商場](../Page/商場.md "wikilink")、生意人、[學生](../Page/學生.md "wikilink")、[小販](../Page/小販.md "wikilink")、[大牌檔等索取](../Page/大牌檔.md "wikilink")[金錢或利益](../Page/金錢.md "wikilink")。或者以侵犯個人隱私的手段向政治人物或演藝人員勒索取得金錢財物。有些人也有可能以此為打壓某些人或團體的手段。

恐嚇言語不受任何[言論自由的保障](../Page/言論自由.md "wikilink")。

## 相關

  - [欺凌](../Page/欺凌.md "wikilink")
  - [保護費](../Page/保護費.md "wikilink")
  - [郵包炸彈](../Page/郵包炸彈.md "wikilink")
  - [抢劫](../Page/抢劫.md "wikilink")
  - [保護費](../Page/保護費.md "wikilink")
  - [三合會](../Page/三合會.md "wikilink")
  - [澎恰恰光碟案](../Page/澎恰恰.md "wikilink")
  - [訴諸武力](../Page/訴諸武力.md "wikilink")

## 参考文獻

  -
## 外部参考

  - [Just Fight On\! Centre Against Workplace Bullying
    UK](http://www.jfo.org.uk)
  - [Coping.org - Eliminating
    Intimidation](https://web.archive.org/web/20070408030215/http://www.coping.org/control/intimid.htm)
  - [mobbing.ca](http://mobbing.ca/) (Mobbing resources in Canada)
  - [Together - Initimidation and
    Harassment](https://web.archive.org/web/20070103150900/http://www.together.gov.uk/category.asp?c=283)

[Category:犯罪](../Category/犯罪.md "wikilink")