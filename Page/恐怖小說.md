[Edgar_Allan_Poe_2.jpg](https://zh.wikipedia.org/wiki/File:Edgar_Allan_Poe_2.jpg "fig:Edgar_Allan_Poe_2.jpg")\]\]
**恐怖小說**是[小說類型的一種](../Page/小說類型.md "wikilink")，屬於[大眾文學](../Page/大眾文學.md "wikilink")。小說內容以恐怖為主、意圖帶給讀者恐怖感（達到使其恐懼的目的）而書寫的小說。恐怖小說在日本，又稱作[怪奇小說](../Page/怪奇.md "wikilink")。評論家將恐怖小說大致分為「[哥德小說](../Page/哥德小說.md "wikilink")」及「現代恐怖」兩種。恐怖小說與恐怖電影之間，在怪誕題材的處理上，有著強烈的[文本互涉性](../Page/文本互涉.md "wikilink")。

## 歷史

### 18世紀

18世紀[哥特式恐怖小說發源於](../Page/哥特式.md "wikilink")[霍勒斯·沃波爾](../Page/霍勒斯·沃波爾.md "wikilink")《[奧特蘭托堡](../Page/奧特蘭托堡.md "wikilink")》（1764年）。這標誌著現代小說第一次納入[超自然元素](../Page/超自然.md "wikilink")，而非純粹[寫實主義](../Page/寫實主義.md "wikilink")。事實上，[意大利中世紀小說再版後](../Page/意大利.md "wikilink")，許多人認為這些作品不合時宜，品味不佳，但是之後受到讀者歡迎。

許多哥特式恐怖小說是由女作家完成，以女性讀者為目標，故事典型場景包含足智多謀的女主人與陰沉[城堡](../Page/城堡.md "wikilink")。

### 19世紀

哥特小說在19世紀發展為恐怖文學，並持續影響至[電影](../Page/電影.md "wikilink")。[瑪麗·雪萊](../Page/瑪麗·雪萊.md "wikilink")《[科學怪人](../Page/科學怪人.md "wikilink")》（1818年）、[愛倫坡的作品](../Page/愛倫坡.md "wikilink")、[喬瑟夫·雪利登·拉·芬努的作品](../Page/喬瑟夫·雪利登·拉·芬努.md "wikilink")、[羅伯特·路易斯·史蒂文森](../Page/羅伯特·路易斯·史蒂文森.md "wikilink")《[化身博士](../Page/化身博士.md "wikilink")》（1886年）、[奧斯卡·王爾德](../Page/奧斯卡·王爾德.md "wikilink")《[道林·格雷的画像](../Page/道林·格雷的画像.md "wikilink")》（1890年）。

### 20世紀

恐怖作家[霍华德·菲利普斯·洛夫克拉夫特以](../Page/霍华德·菲利普斯·洛夫克拉夫特.md "wikilink")[克蘇魯神話開創宇宙恐怖的風格](../Page/克蘇魯神話.md "wikilink")。Montague
Rhodes James在那個時代重新定義鬼故事精神。

早期電影受到恐怖文學的許多啟發，早期[恐怖電影以傳統的恐怖小說為基礎](../Page/恐怖電影.md "wikilink")。

[李察·麥森的](../Page/李察·麥森.md "wikilink")1954年小說《[我是傳奇](../Page/我是傳奇_\(小說\).md "wikilink")》也影響著末日小說、[殭屍小說](../Page/殭屍.md "wikilink")、[喬治·安德魯·羅梅羅電影](../Page/喬治·安德魯·羅梅羅.md "wikilink")。

## 恐怖小說作家

### 西方

  - [爱伦·坡](../Page/爱伦·坡.md "wikilink")（Edgar Allan Poe）
  - [霍華·菲力普·洛夫克萊夫特](../Page/霍華·菲力普·洛夫克萊夫特.md "wikilink")（H.P. Lovecraft）
  - [丁·昆士](../Page/丁·昆士.md "wikilink")（Dean Koontz）
  - [斯蒂芬·金](../Page/斯蒂芬·金.md "wikilink")（Stephen Edwin King）
  - [克里夫·巴克](../Page/克里夫·巴克.md "wikilink")（Clive Barker）

### 華人

  - [倪匡](../Page/倪匡.md "wikilink")（倪匡鬼话系列）
  - [蔡骏](../Page/蔡骏.md "wikilink")（[荒村公寓](../Page/荒村公寓.md "wikilink")、蝴蝶公墓、病毒）
  - [王雨辰](../Page/王雨辰.md "wikilink")（异闻录）
  - [天下霸唱](../Page/天下霸唱.md "wikilink")（[鬼吹灯](../Page/鬼吹灯.md "wikilink")、贼猫、谜踪之国）
  - Tinadannis（冤鬼路系列）
  - [既晴](../Page/既晴.md "wikilink")（恐怖推理系列）
  - [張草](../Page/張草.md "wikilink")（極短篇）
  - [畢名](../Page/畢名.md "wikilink")（驚慄劇場系列、靈異出版社系列、恐怖短篇系列）
  - [D51](../Page/D51.md "wikilink")（妝鬼師系列、討鬼債系列、靈異檢察官辦案實錄系列）
  - [柚臻](../Page/柚臻.md "wikilink")（病態系列、妖的忍法帖系列）
  - [馬尼](../Page/馬尼_\(作家\).md "wikilink") （鬼陰緣）
  - [路邊攤](../Page/路邊攤_\(小說家\).md "wikilink") （詭誌系列、異數系列）

### 日本

  - [山田悠介](../Page/山田悠介.md "wikilink")
  - [朝松健](../Page/朝松健.md "wikilink")
  - [綾辻行人](../Page/綾辻行人.md "wikilink")
  - [岩井志麻子](../Page/岩井志麻子.md "wikilink")
  - [小野不由美](../Page/小野不由美.md "wikilink")
  - [乙一](../Page/乙一.md "wikilink")
  - [貴志祐介](../Page/貴志祐介.md "wikilink")
  - [菊地秀行](../Page/菊地秀行.md "wikilink")
  - [倉阪鬼一郎](../Page/倉阪鬼一郎.md "wikilink")
  - [小林泰三](../Page/小林泰三.md "wikilink")
  - [鈴木光司](../Page/鈴木光司.md "wikilink")
  - [瀨名秀明](../Page/瀨名秀明.md "wikilink")
  - [竹河聖](../Page/竹河聖.md "wikilink")
  - [恒川光太郎](../Page/恒川光太郎.md "wikilink")
  - [津原泰水](../Page/津原泰水.md "wikilink")
  - [早瀨乱](../Page/早瀨乱.md "wikilink")
  - [平山夢明](../Page/平山夢明.md "wikilink")
  - [道尾秀介](../Page/道尾秀介.md "wikilink")
  - [三津田信三](../Page/三津田信三.md "wikilink")
  - [宮部美幸](../Page/宮部美幸.md "wikilink")
  - [夢枕獏](../Page/夢枕獏.md "wikilink")
  - [吉村達也](../Page/吉村達也.md "wikilink")
  - [秋元康](../Page/秋元康.md "wikilink")
  - [赤川次郎](../Page/赤川次郎.md "wikilink")
  - [芥川龙之介](../Page/芥川龙之介.md "wikilink")

## 參看

  - [獵奇](../Page/獵奇.md "wikilink")
  - [怪奇](../Page/怪奇.md "wikilink")
  - [異端文學](../Page/異端文學.md "wikilink")
  - [恐怖電影](../Page/恐怖電影.md "wikilink")
  - [哥特小说](../Page/哥特小说.md "wikilink")

## 外部連結

[K](../Category/小說類型.md "wikilink") [\*](../Category/恐怖小說.md "wikilink")