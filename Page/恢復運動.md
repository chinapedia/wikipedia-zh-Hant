**恢復運動**（**Restoration
Movement**），又譯**復原運動**，是一個18、19世紀，在[美國](../Page/美國.md "wikilink")[第二次大覺醒期間的](../Page/大覺醒運動.md "wikilink")[基督教改革運動](../Page/宗教改革.md "wikilink")。強調活出[新約](../Page/新約.md "wikilink")[聖經的原則](../Page/聖經.md "wikilink")，恢復聖經裏的[教會樣式](../Page/教會.md "wikilink")，促進在聖經真理的基礎上的合一，廢除一切晚近[神學名稱及教會組織等](../Page/神學.md "wikilink")。與[基督新教相比](../Page/基督新教.md "wikilink")，他們更加像[基要派](../Page/基要派.md "wikilink")。主要領導人物包括[亞歷山大·坎伯](../Page/亞歷山大·坎伯.md "wikilink")（Alexander
Campbell）和[伯通·史東](../Page/伯通·史東.md "wikilink")（Burton Stone）等。

## 關鍵原則

  - [基督教不應被分割](../Page/基督教.md "wikilink")：[基督只建立了一個教會](../Page/基督.md "wikilink")。
  - 基督徒合一的基礎是聖經真理，不是人的意見。
  - 基督徒應該在早期教會的樣式上找到共同點，不是教會人為的傳統。
  - 教會的名稱應用聖經裡的名稱，不是人名（反對「[衛斯理宗](../Page/衛斯理宗.md "wikilink")」或「[路德宗](../Page/路德宗.md "wikilink")」等的名稱）。

## 歷史過程

## 宗派分支

三個[北美](../Page/北美.md "wikilink")[宗派可追溯到恢復運動](../Page/宗派.md "wikilink")：

  - [基督會](../Page/基督會.md "wikilink")（Disciples of Christ； The Christian
    Church）─ 在北美約有85萬名成員。
  - 基督的教會（The Churches of Christ）─ 在北美約有150萬名成員。
  - 獨立基督教教會（Independent Christian Churches）─
    全球的大約有200萬名成員；截至2001年，在美國約有15萬名成員。

## 參考文献

  - North, James B. (1994). *Union in Truth: An Interpretive History of
    the Restoration Movement.* Cincinnati, Ohio: The Standard Publishing
    Company. ISBN 0-7847-0197-0.
  - Flavil R. Yeakley, ed., *The Discipling Dilemma: A Study of the
    Discipling Movement Among Churches of Christ* (Nashville: Gospel
    Advocate Co., 1988).
  - C. Leonard Allen and Richard T. Hughes, *Discovering Our Roots: The
    Ancestry of Churches of Christ* (Abilene, Texas: ACU Press, 1988)
  - Martin Edward Wooten, "The Boston Movement as a 'Revitalization
    Movement'" (D.Min. thesis, Harding Graduate School of Religion,
    1990)
  - Jerry Jones, *What Does the Boston Movement Teach?* vols. 1-3
    (Bridgeton, Missouri: Jerry Jones, 12880 Bittick, 1991-93)
  - United States Department of Commerce and Labor, Bureau of the
    Census, Religious Bodies, 1906 (United States Printing Office,
    1910), 236
  - West, Earl Irvin (2002). The Search for the Ancient Order Vol. 1.
    Gospel Light Publishing Company. ISBN 0-89225-154-9
  - Douglas A. Foster (Editor), Paul M. Blowers (Editor), Anthony L.
    Dunnavant (Editor), D. Newell Williams (Editor). *The Encyclopedia
    of the Stone-Campbell Movement*. Wm. B. Eerdmans Publishing Company,
    Grand Rapids, Michigan. ISBN 0-8028-3898-7
  - Douglas A. Foster, Jack Reese, Jeff W. Childers, *The Crux of the
    Matter: Crisis, Tradition, and the Future of Churches of Christ*.
    ACU Press. ISBN 0-89112-035-1
  - Murch, James DeForest. *Christians Only*. Cincinnati: Standard
    Publishing, 1962.
  - Jennings, Walter Wilson. *Origin and Early History of the Disciples
    of Christ* Cincinnati: Standard Publishing, 1919.
  - Morrill, Milo True. *History of the Christian Denomination in
    America*. Dayton: The Christian Publishing Association, 1912.

[恢復主義](../Category/恢復主義.md "wikilink")
[Category:基督教運動及主義](../Category/基督教運動及主義.md "wikilink")
[Category:美国基督教史](../Category/美国基督教史.md "wikilink")
[Category:大觉醒运动](../Category/大觉醒运动.md "wikilink")
[Category:基督教基要主义](../Category/基督教基要主义.md "wikilink")