[CM-Ngaoundere.png](https://zh.wikipedia.org/wiki/File:CM-Ngaoundere.png "fig:CM-Ngaoundere.png")

**恩冈代雷**（**Ngaoundéré**或者**N'Gaoundéré**）
位于喀麦隆中北部，阿达马瓦高原城市，是[喀麦隆](../Page/喀麦隆.md "wikilink")[阿达马瓦省的省会](../Page/阿达马瓦省.md "wikilink")。城市海拔高度为1212米，人口约
189,800(2001年)。城市有南下通往[雅温得的](../Page/雅温得.md "wikilink")[铁路](../Page/铁路.md "wikilink")，从海港大都会[杜阿拉](../Page/杜阿拉.md "wikilink")—恩冈代雷的铁路线是喀麦隆的铁路主干线，公路北通[马鲁阿](../Page/马鲁阿.md "wikilink")，邻国[尼日利亚和](../Page/尼日利亚.md "wikilink")[乍得](../Page/乍得.md "wikilink")。城市也拥有一所机场。

## 气候

## 经济

恩冈代雷建于19世纪中叶，是喀麦隆中北部工商业重镇和交通中心。有大型[屠宰场](../Page/屠宰场.md "wikilink")、[肉类加工厂和](../Page/肉类.md "wikilink")[奶酪](../Page/奶酪.md "wikilink")、皮毛、[轧棉](../Page/棉花.md "wikilink")、水电等工业。[花生和](../Page/花生.md "wikilink")[畜产品贸易甚盛](../Page/家畜.md "wikilink")。乍得部分进出口货物也经恩冈代雷转运。附近[马塔普有大型铝土矿](../Page/马塔普.md "wikilink")，恩冈代雷也拥有[铝土矿](../Page/铝土矿.md "wikilink")。[Downtown_Ngaoundere.jpg](https://zh.wikipedia.org/wiki/File:Downtown_Ngaoundere.jpg "fig:Downtown_Ngaoundere.jpg")

## 景点

城市的景点有：

  - [拉米多宫殿](../Page/拉米多宫殿.md "wikilink")(Lamido Palace)
  - [拉米多大清真寺](../Page/拉米多大清真寺.md "wikilink")(Lamido Grand Mosque)。

## 参考资料

## 外部链接

  - [中国行政区划·世界地名索引](https://web.archive.org/web/20061230183230/http://www.xzqh.org/suoyin/sjdm/102.htm)
    - 中国行政区划网 - 2007年1月29日造访

<!-- end list -->

  - [FallingRain
    Map](http://www.fallingrain.com/world/CM/10/Ngaoundere.html)

[Category:喀麦隆城市](../Category/喀麦隆城市.md "wikilink")