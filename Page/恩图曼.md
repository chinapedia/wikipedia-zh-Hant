**恩图曼**（），又译**乌姆杜尔曼**，[苏丹](../Page/苏丹.md "wikilink")[喀土穆省的一座城市](../Page/喀土穆省.md "wikilink")，位于[尼罗河西岸](../Page/尼罗河.md "wikilink")，与[喀土穆和](../Page/喀土穆.md "wikilink")[北喀土穆隔河相望](../Page/北喀土穆.md "wikilink")，三地有桥梁相连。

恩图曼的人口约为300万人（2007年），是苏丹人口最多的城市，又常与被视为首都的三个镇之一（另外两个为喀土穆和北喀土穆），\[1\]
三镇共同组成苏丹的文化和工业中心，其中恩图曼的商业比较发达。

## 人口

| 年份                | 人口\[2\]   |
| ----------------- | --------- |
| 1909年 (人口普查)\[3\] | 42.779    |
| 1941年             | 116.196   |
| 1956年             | 113.600   |
| 1973年             | 299.399   |
| 1983年             | 526.284   |
| 1993年             | 1.271.403 |
| 2007年(估計)         | 3.127.802 |
| 2008年             | 2.395.159 |
| 2010年             | 2.577.780 |

## 注释

[Category:苏丹城市](../Category/苏丹城市.md "wikilink")
[Category:喀土穆](../Category/喀土穆.md "wikilink")
[Category:喀土穆省](../Category/喀土穆省.md "wikilink")

1.
2.  [<http://bevoelkerungsstatistik.de>](http://bevoelkerungsstatistik.de/wg.php?x=&men=gcis&lng=de&dat=32&geo=-188&srt=npan&col=aohdq&pt=c&va=x.&srt=pnan)

3.  [Encyclopædia Britannica von 1911: Band 20,
    Seite 101](http://encyclopedia.jrank.org/NUM_ORC/OMDURMAN.html)