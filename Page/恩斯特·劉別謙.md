**恩斯特·劉別謙**（**Ernst
Lubitsch**，）是一位[德國](../Page/德國.md "wikilink")[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")，被廣泛的認為是德國電影史上影響最大的導演之一，對於[喜劇電影的影響甚大](../Page/喜劇.md "wikilink")。劉別謙獨特的電影風格則被稱為「劉別謙式觸動」（Lubitsch
touch）。

## 生平

## 作品

### 有聲電影

  - That Lady in Ermine ------- (1948)
  - [克朗内勃朗](../Page/克朗内勃朗.md "wikilink")/[打工落難結良緣](../Page/打工落難結良緣.md "wikilink")
    Cluny Brown ------- (1946)
  - A Royal Scandal ------- (1945)
  - 《[天長地久](../Page/天堂可待.md "wikilink")》 Heaven Can Wait ------- (1943)
      - 提名[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")
  - [生死攸关](../Page/生死攸关.md "wikilink")/To Be or Not to Be ------- (1942)
  - [夫妇之道](../Page/夫妇之道.md "wikilink") That Uncertain Feeling -------
    (1941)
  - [街角的商店The](../Page/街角的商店.md "wikilink") Shop Around the Corner
    ------- (1940)
  - 《[俄宮艷史](../Page/俄宮艷史.md "wikilink")》Ninotchka ------- (1939)
  - [蓝胡子的第八任妻子](../Page/蓝胡子的第八任妻子.md "wikilink") Bluebeard's Eighth Wife
    ------- (1938)
  - [天使](../Page/天使_\(1937年電影\).md "wikilink") Angel ------- (1937)
  - [风流寡妇](../Page/风流寡妇.md "wikilink") The Merry Widow ------- (1934)
  - [爱情无计](../Page/爱情无计.md "wikilink") Design for Living ------- (1933)
  - If I Had a Million ------- (1932)
  - [红楼艳史](../Page/红楼艳史.md "wikilink")/One Hour with You ------- (1932)
  - Une heure près de toi ------- (1932)
  - Broken Lullaby ------- (1932)
  - 《[天堂問題](../Page/天堂問題.md "wikilink")》Trouble in Paradise -------
    (1932)
  - [微笑的上尉The](../Page/微笑的上尉.md "wikilink") Smiling Lieutenant -------
    (1931)
  - The Vagabond King ------- (1930)
  - [蒙特卡罗](../Page/蒙特卡罗.md "wikilink") Monte Carlo ------- (1930)
  - Paramount on Parade ------- (1930)
  - Galas de la Paramount ------- (1930)
  - [璇宫艳史](../Page/璇宫艳史.md "wikilink") The Love Parade ------- (1929)
      - 提名[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")
  - Eternal Love ------- (1929)
  - [爱国者](../Page/爱国者.md "wikilink") The Patriot ------- (1928)
      - 提名[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")

### 無聲電影

  - [学生王子](../Page/学生王子.md "wikilink") The Student Prince in Old
    Heidelberg ------- (1927)
  - [笙歌满巴黎](../Page/笙歌满巴黎.md "wikilink") So This Is Paris ------- (1926)
  - The Honeymoon Express ------- (1926)
  - [少奶奶的扇子](../Page/少奶奶的扇子.md "wikilink") Lady Windermere's Fan -------
    (1925)
  - Kiss Me Again ------- (1925)
  - Three Women ------- (1924)
  - [宫廷禁恋](../Page/宫廷禁恋.md "wikilink") Forbidden Paradise ------- (1924)
  - [结婚集团](../Page/结婚集团.md "wikilink") The Marriage Circle -------
    (1924)
  - Die Flamme ------- (1923)
  - [露茜塔](../Page/露茜塔.md "wikilink") Rosita ------- (1923)
  - Das Weib des Pharao ------- (1922)
  - Die Bergkatze ------- (1921)
  - [安娜·博林](../Page/安娜·博林.md "wikilink") Anna Boleyn ------- (1920)
  - Kohlhiesels Töchter ------- (1920)
  - [苏姆伦王妃](../Page/苏姆伦王妃.md "wikilink") Sumurun ------- (1920)
  - Romeo und Julia im Schnee ------- (1920)
  - [牡蛎公主](../Page/牡蛎公主.md "wikilink") Die Austernprinzessin -------
    (1919)
  - Rausch ------- (1919)
  - Meine Frau, die Filmschauspielerin ------- (1919)
  - Die Puppe ------- (1919)
  - Meyer aus Berlin ------- (1919)
  - Das Schwabenmädel ------- (1919)
  - Madame DuBarry ------- (1919)
  - Das Mädel vom Ballet ------- (1918)
  - Augen der Mumie Ma, Die ------- (1918)
  - Carmen ------- (1918)
  - [我不想做男人](../Page/我不想做男人.md "wikilink") Ich möchte kein Mann sein
    ------- (1918)
  - Fuhrmann Henschel ------- (1918)
  - Prinz Sami ------- (1918)
  - Der Rodelkavalier ------- (1918)
  - Der Fall Rosentopf ------- (1918)
  - Wenn vier dasselbe tun ------- (1917)
  - Ossis Tagebuch ------- (1917)
  - Das fidele Gefängnis ------- (1917)
  - Der Blusenkönig ------- (1917)
  - Wo ist mein Schatz? ------- (1916)
  - Der gemischte Frauenchor ------- (1916)
  - Der G.m.b.H. Tenor ------- (1916)
  - Seine neue Nase ------- (1916)
  - Der erste Patient ------- (1916)
  - Schuhpalast Pinkus ------- (1916)
  - Das schönste Geschenk ------- (1916)
  - Der Kraftmeier ------- (1915)
  - Blindekuh ------- (1915)
  - Aufs Eis geführt ------- (1915)
  - Zucker und Zimmt ------- (1915)
  - Der letzte Anzug ------- (1915)
  - Fräulein Seifenschaum ------- (1914)

## 外部連結

  -
[Category:德國導演](../Category/德國導演.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:猶太導演](../Category/猶太導演.md "wikilink")
[Category:歸化美國公民的德國人](../Category/歸化美國公民的德國人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:德國猶太人](../Category/德國猶太人.md "wikilink")
[Category:柏林人](../Category/柏林人.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:葬于加利福尼亚州格伦代尔森林草坪纪念公园](../Category/葬于加利福尼亚州格伦代尔森林草坪纪念公园.md "wikilink")