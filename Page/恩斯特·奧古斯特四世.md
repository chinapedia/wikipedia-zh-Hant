**恩斯特·奧古斯特四世**（**Ernst August
IV**，），全名恩斯特·奧古斯特·格奥尔格·威廉·克里斯蒂安·路德维希·弗朗茨·约瑟夫·尼库劳斯·奥斯卡（Ernst
August Georg Wilhelm Christian Ludwig Franz Joseph Nikolaus
Oskar）。[汉诺威王位继承人](../Page/汉诺威统治者列表.md "wikilink")。

## 生平

恩斯特·奧古斯特是[不伦瑞克公爵](../Page/不伦瑞克统治者列表.md "wikilink")、汉诺威王位继承人[恩斯特·奧古斯特与](../Page/恩斯特·奧古斯特三世_\(漢諾威\).md "wikilink")[德意志皇帝兼](../Page/德国君主列表.md "wikilink")[普鲁士国王](../Page/普鲁士国王列表.md "wikilink")[威廉二世的唯一的女儿](../Page/威廉二世_\(德国\).md "wikilink")[维多利亚·路易丝公主的长子](../Page/维多利亚·路易丝公主_\(普鲁士\).md "wikilink")。1914年出生便成为不伦瑞克公爵世子。

1918年恩斯特·奧古斯特四岁时德国[君主制覆亡](../Page/君主制.md "wikilink")，他失去了继承王位的可能随家人流亡[奥地利](../Page/奥地利.md "wikilink")。后来在撒冷的[寄宿学校学习](../Page/寄宿学校.md "wikilink")，在[哥廷根学习](../Page/哥廷根.md "wikilink")[法学](../Page/法学.md "wikilink")。1936年，恩斯特·奧古斯特成为法学[博士](../Page/博士.md "wikilink")。

[二战中](../Page/二战.md "wikilink")，恩斯特·奧古斯特加入了对[俄战争并成为](../Page/俄罗斯.md "wikilink")[埃里希·赫普納](../Page/埃里希·赫普納.md "wikilink")[上将](../Page/上将.md "wikilink")（Generaloberst
Erich
Hoepner）旗下的一名[中尉](../Page/中尉.md "wikilink")。他于1943年在[哈爾科夫受了重伤](../Page/哈爾科夫.md "wikilink")。1944年的[7月20日密谋案失败之后](../Page/7月20日密谋案.md "wikilink")，恩斯特·奧古斯特被[盖世太保拘留在](../Page/盖世太保.md "wikilink")[柏林阿尔布雷希特亲王路上的](../Page/柏林.md "wikilink")[司令部里](../Page/司令部.md "wikilink")。

二战末，恩斯特·奧古斯特与全家从哈尔茨的布兰肯堡城堡迁至[汉诺威附近的玛丽安堡城堡](../Page/汉诺威.md "wikilink")。1951年他迎娶了石勒苏益格-荷尔斯泰因-宗德堡-格吕克斯堡公主奥特鲁德（1925年—1980年），有三子三女：

  - 长女**玛丽**·维多利亚·路易丝·赫尔塔·弗里德里克（*Marie Viktoria Luise Hertha
    Friederike*），1952年生，1982年嫁给霍赫贝格伯爵家族成员米哈埃尔。
  - 长子[**恩斯特·奧古斯特**·阿尔贝特·奥托·鲁普雷希特·奥斯卡·贝托尔德·弗里德里希-斐迪南·克里斯蒂安-路德维希](../Page/恩斯特·奧古斯特五世_\(漢諾威\).md "wikilink")（*Ernst
    August Albert Otto Rupprecht Oskar Berthold Friedrich-Ferdinand
    Christian-Ludwig*），1954年2月26日生，汉诺威亲王、王位继承人、王室家族首领。
  - 次子**路德维希·鲁道夫**·格奥尔格·威廉·菲利普·弗里德里希·沃尔拉德·马克西米利安（*Ludwig Rudolph Georg
    Wilhelm Philipp Friedrich Wolrad Maximilian*，1955年—1988年），有一子。
  - 次女**奥尔嘉**·索菲·夏洛特·安娜（*Olga Sophie Charlotte Anna*），1958年生。
  - 三女**亚历山德拉**·伊莲妮·玛尔吉塔·伊莉莎白·巴蒂尔蒂丝（*Alexandra Irene Margitha Elisabeth
    Bathildis*），1981年嫁给[莱宁根侯爵](../Page/莱宁根统治者列表.md "wikilink")[安德烈阿斯](../Page/安德烈阿斯_\(莱宁根\).md "wikilink")，有二子一女。
  - 三子**亨利**·尤利乌斯·克里斯蒂安·奥托·弗里德里希·弗朗茨·安东·京特尔（*Heinrich Julius Christian
    Otto Friedrich Franz Anton Günter*），1961年生，有一子一女，另有一名私生女。

1953年，恩斯特·奧古斯特的父亲在玛丽安堡城堡去世。恩斯特·奧古斯特成为汉诺威王室[韦尔夫家族首领](../Page/韦尔夫家族.md "wikilink")，王位继承人，称**汉诺威亲王、不伦瑞克和吕内堡的公爵**。在亲王夫人奥特鲁德的管理下，玛丽安堡城堡转变成了一座[博物馆](../Page/博物馆.md "wikilink")。

1954年，恩斯特·奧古斯特与[普鲁士的](../Page/普鲁士.md "wikilink")[路易·斐迪南亲王一起参加了当年的](../Page/路易·斐迪南_\(普鲁士\).md "wikilink")[联邦德国总统竞选](../Page/联邦德国.md "wikilink")。二人都以失败告终，各只得一票。[特奥多尔·豪斯继任](../Page/特奥多尔·豪斯.md "wikilink")[德国联邦总统](../Page/德国联邦总统.md "wikilink")。

1980年，恩斯特·奧古斯特的妻子奥特鲁德去世，他于次年娶[索尔姆斯-劳巴赫伯爵](../Page/索尔姆斯统治者列表.md "wikilink")[格奥尔格的次女莫妮卡](../Page/格奥尔格_\(索尔姆斯-劳巴赫\).md "wikilink")（1929年生）为第二任妻子，二人没有子嗣。恩斯特·奧古斯特于1987年在汉诺威去世，终年73岁。

## 頭銜

  - 1914年-1918年：不伦瑞克王儲殿下、大不列顛及愛爾蘭親王 （*HRH* The Hereditary Prince of
    Brunswich, Prince of Great Britain and Ireland）
  - 1918年-1953年：汉诺威王储殿下 （*HRH* The Hereditary Prince of Hanover）
  - 1953年-1987年：汉诺威親王殿下 （*HRH* The Prince of Hanover）

## 祖先

<center>

</center>

[Category:汉诺威国王](../Category/汉诺威国王.md "wikilink")
[Category:韦尔夫王朝](../Category/韦尔夫王朝.md "wikilink")
[Category:德國政治人物](../Category/德國政治人物.md "wikilink")
[Category:德国贵族](../Category/德国贵族.md "wikilink")
[Category:未继位的储君](../Category/未继位的储君.md "wikilink")