[Ernst_Zermelo.jpeg](https://zh.wikipedia.org/wiki/File:Ernst_Zermelo.jpeg "fig:Ernst_Zermelo.jpeg")
**恩斯特·策梅洛**（[德语](../Page/德语.md "wikilink")：****，），生于[柏林](../Page/柏林.md "wikilink")，是[德国](../Page/德国.md "wikilink")[数学家](../Page/数学家.md "wikilink")，其工作主要為[数学基础](../Page/数学基础.md "wikilink")，因而对[哲学有重要影响](../Page/哲学.md "wikilink")。

## 生平

1889年，他毕业于柏林*Luisenstädtisches
Gymnasium*。他然后在[柏林大学](../Page/柏林大学.md "wikilink")、[哈雷-維滕貝格大學和](../Page/哈雷-維滕貝格大學.md "wikilink")[弗莱堡大學研究数学](../Page/弗莱堡大學.md "wikilink")、[物理和](../Page/物理.md "wikilink")[哲学](../Page/哲学.md "wikilink")。他于1894年在[柏林大学完成博士学位](../Page/柏林大学.md "wikilink")，其博士论文是关于[变分法的](../Page/变分法.md "wikilink")（*Untersuchungen
zur
Variationsrechnung*）。策梅洛留在柏林大学，他被聘为[普朗克的助手](../Page/普朗克.md "wikilink")，在其指导下，他开始研究[流体力学](../Page/流体力学.md "wikilink")。在1897年，策梅洛去了[哥廷根](../Page/哥廷根.md "wikilink")，那裡當时是整个世界的数学研究的中心，在那里他于1899年完成了[教员资格论文](../Page/教员资格论文.md "wikilink")。

策海洛于1953年5月21日在[弗赖堡逝世](../Page/弗赖堡.md "wikilink")。

## 參見

  - [策梅洛-弗倫克爾公理系統](../Page/策梅洛-弗倫克爾公理系統.md "wikilink")（ZFC）
  - [策梅洛定理 (博弈論)](../Page/策梅洛定理_\(博弈論\).md "wikilink")
  - [选择公理](../Page/选择公理.md "wikilink")
  - [巴拿赫-塔斯基定理](../Page/巴拿赫-塔斯基定理.md "wikilink")（Banach–Tarski 悖论）

## 文献

英语译本主要文献：

  - [Jean van Heijenoort](../Page/Jean_van_Heijenoort.md "wikilink"),
    1967. *From Frege to Godel: A Source Book in Mathematical Logic,
    1879-1931*. Harvard Univ. Press.
      - 1904\. "Proof that every set can be well-ordered," 139-41.
      - 1908\. "A new proof of the possibility of well-ordering,"
        183-98.
      - 1908\. "Investigations in the foundations of set theory I,"
        199-215.
  - 1930\. "On boundary numbers and domains of sets: new investigations
    in the foundations of set theory" in Ewald, William B., ed., 1996.
    *From Kant to Hilbert: A Source Book in the Foundations of
    Mathematics*, 2 vols. Oxford Uni. Press: 1219-33.

次要文献:

  - [Ivor
    Grattan-Guinness](../Page/Ivor_Grattan-Guinness.md "wikilink"),
    2000. *The Search for Mathematical Roots 1870-1940*. Princeton Uni.
    Press.

## 外部链接

  -
  -
  - [策梅洛的科学传记（1871－1953）。](https://web.archive.org/web/20050324004833/http://hrz.upb.de/~apeck1/zermelo/id9.htm)

[Category:20世紀哲學家](../Category/20世紀哲學家.md "wikilink")
[Category:德国哲学家](../Category/德国哲学家.md "wikilink")
[Category:德国数学家](../Category/德国数学家.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:弗賴堡大學教師](../Category/弗賴堡大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:弗萊堡大學校友](../Category/弗萊堡大學校友.md "wikilink")
[Category:哈雷-維滕貝格大學校友](../Category/哈雷-維滕貝格大學校友.md "wikilink")
[Category:柏林人](../Category/柏林人.md "wikilink")
[Category:集合論者](../Category/集合論者.md "wikilink")
[Category:数理逻辑学家](../Category/数理逻辑学家.md "wikilink")