[EngelsLaw.svg](https://zh.wikipedia.org/wiki/File:EngelsLaw.svg "fig:EngelsLaw.svg")

**恩格尔定律**是19世纪[德国统计学家](../Page/德国.md "wikilink")[恩格尔根据统计资料](../Page/恩格尔.md "wikilink")，对[消费结构的变化得出一个](../Page/消费结构.md "wikilink")[规律](../Page/规律.md "wikilink")：

一个[家庭收入越少](../Page/家庭.md "wikilink")，[家庭收入中](../Page/家庭收入.md "wikilink")（或总支出中）用来购买[食物的支出所佔的比例就越大](../Page/食物.md "wikilink")，随着家庭收入的增加，家庭收入中（或总支出中）用来购买食物的支出比例则会下降。推而广之，一个[国家越穷](../Page/国家.md "wikilink")，每个国民的平均收入中（或平均支出中）用于购买食物的支出所占比例就越大，随着国家的富裕，这个比例呈下降趋势。恩格尔定律的[公式](../Page/公式.md "wikilink")：

  -
    **食物支出对总支出的比率**(R1)＝食物支出变动百分比÷总支出变动百分比

或

  -
    **食物支出对收入的比率**(R2)＝食物支出变动百分比÷收入变动百分比

R2又称为**食物支出的收入弹性**。　

恩格尔定律是根据经验数据提出的，它是在假定其他一切变量都是常数的前提下才适用的，因此在考察食物支出在收入中所占比例的变动问题时，还应当考虑[城市化程度](../Page/城市化.md "wikilink")、[食品加工](../Page/食品加工.md "wikilink")、[饮食业和食物本身结构变化等因素都会影响家庭的食物支出增加](../Page/饮食业.md "wikilink")。只有达到相当高的平均食物消费水平时，收入的进一步增加才不对食物支出发生重要的影响。\[1\]

## 参考文献

  - 《經濟學-理論與實務》 4th
    [張清溪](../Page/張清溪.md "wikilink")、[許嘉棟](../Page/許嘉棟.md "wikilink")、[劉鶯釧](../Page/劉鶯釧.md "wikilink")、[吳聰敏合著](../Page/吳聰敏.md "wikilink")
    ISBN 957-97490-3-5

## 連結

  - [恩格尔定律与恩格尔系数 -
    国家统计局](http://www.stats.gov.cn/tjzs/tjcd/200206/t20020605_25327.html)
  - [恩格尔系数——达到小康水平的标尺-《每天学点经济学》](http://book.weibo.com/book/play/182417-143884.html)
  - [恩格尔系数_中国经济网](http://intl.ce.cn/zhuanti/data/egl/)

## 延伸閱讀

  - 姜国刚：[恩格尔系数失灵的若干影响因素分析](http://202.119.108.161:93/modules/showContent.aspx?title=&Word=&DocGUID=0e622ad8639a482a916f641dd7db43af)

[Category:发展经济学](../Category/发展经济学.md "wikilink")

1.