**恩里克·佩雷拉·羅薩**（**Henrique Pereira
Rosa**，），[非洲國家](../Page/非洲.md "wikilink")[幾內亞比索政府官員](../Page/幾內亞比索.md "wikilink")。他為[幾內亞比索前](../Page/幾內亞比索.md "wikilink")[總統](../Page/總統.md "wikilink")，此任期從2003年一直擔任至2005年。

2013年5月15日，因罹患肺癌于[葡萄牙](../Page/葡萄牙.md "wikilink")[波尔图逝世](../Page/波尔图.md "wikilink")。\[1\]

## 参考资料

[Category:几内亚比绍总统](../Category/几内亚比绍总统.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")

1.