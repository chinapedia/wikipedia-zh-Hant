**恭碩良**（，），又名**李尊傑**、**李祖倫**，[澳門土生葡人](../Page/澳門土生葡人.md "wikilink")，是[香港](../Page/香港.md "wikilink")、[澳門創作歌手](../Page/澳門.md "wikilink")、鼓手及唱片監製。目前以創作歌手的形象，在澳門、香港及[中國大陸發展](../Page/中國大陸.md "wikilink")。\[1\]

## 簡歷

恭碩良的外祖母為中墨混血兒，母為澳門土生葡人，父為菲律賓人，因此他是[澳門土生葡人](../Page/澳門土生葡人.md "wikilink")。他生於[香港](../Page/香港.md "wikilink")，但年幼時在[澳门生活及就讀小學](../Page/澳门.md "wikilink")，曾於澳門[粵華中學就讀](../Page/粵華中學.md "wikilink")，畢業後赴美國[紐約城市大學主修音樂](../Page/紐約城市大學.md "wikilink")。\[2\]

1999年回到香港後，推出首張專輯《Here & Now》。1999年6月推出首張《Here &
Now》[EP](../Page/迷你專輯.md "wikilink")，於2000年獲得[商業電台](../Page/商業電台.md "wikilink")《[叱吒樂壇流行榜新力軍](../Page/叱吒樂壇流行榜.md "wikilink")
– 金獎》。2001年7月推出第二張專輯《Nowhere Man》、2008年自資限量推出《Ginja Rock
EP》。近年多以音樂人身份與電影及唱片製作人合作，曾經是[黃貫中樂隊](../Page/黃貫中.md "wikilink")「汗」的其中一位成員，亦為多位香港歌手的演唱會擔任鼓手一職，曾參與香港巨星[張學友在](../Page/張學友.md "wikilink")2007年於世界各地舉辦105場的「[學友光年世界巡迴演唱會](../Page/學友光年世界巡迴演唱會.md "wikilink")」，更加在演唱會中被張學友形容為他心目中最棒最型的鼓手。應香港著名歌手[陳奕迅邀請](../Page/陳奕迅.md "wikilink")，2010年3月20日至4月6日於[香港體育館舉行的](../Page/香港體育館.md "wikilink")「DUO陳奕迅2010演唱會」中擔任鼓手，並隨團開始[陳奕迅的](../Page/陳奕迅.md "wikilink")「DUO
World
Tour」。\[3\]巡迴演出之演唱會包括[王菲](../Page/王菲.md "wikilink")、[林憶蓮及早期](../Page/林憶蓮.md "wikilink")[莫文蔚](../Page/莫文蔚.md "wikilink")、[方大同](../Page/方大同.md "wikilink")、[謝安琪等](../Page/謝安琪.md "wikilink")，恭碩良已經成為香港最具代表性的鼓手，沒有之一。

2010年9月推出睽違已久的第四張專輯《JUN K》，並於同月舉辦「Jun On The Moon Live
2010演唱會」，同場嘉賓有張學友、林憶蓮、香港本地hip
hop組合[廿四味以及](../Page/廿四味_\(組合\).md "wikilink")[尹子維](../Page/尹子維.md "wikilink")
。近年恭碩良回歸大銀幕，《[同門](../Page/同門.md "wikilink")》之演出更獲得一致好評。他亦曾參與多個電影配樂，早期包括《順流逆流》、《妖野迴廊》、《四大天王》；近年有《[打擂台](../Page/打擂台.md "wikilink")》、《[東風破](../Page/東風破.md "wikilink")》等，《東風破》電影內歌曲《Here
To
Stay》更獲[第三十屆](../Page/2011年香港電影金像獎.md "wikilink")[香港電影金像獎最佳原創電影歌曲獎](../Page/香港電影金像獎.md "wikilink")。\[4\]2012年5月6日，恭碩良發行了第五張專輯《Playback
is a bitch》。

2017年，他的女友、香港著名女歌手[林憶蓮以竞演歌手身份参加](../Page/林憶蓮.md "wikilink")[湖南卫视音乐竞技节目](../Page/湖南卫视.md "wikilink")《[歌手2017](../Page/歌手2017.md "wikilink")》，他在这一节目中但任她的乐队鼓手。\[5\]

2018年6月7至8日，和前[藍戰士低音結他手](../Page/藍戰士.md "wikilink")[單立文](../Page/單立文.md "wikilink")（Pal、豹哥）、前[Beyond結他手](../Page/Beyond.md "wikilink")[黃貫中](../Page/黃貫中.md "wikilink")（Paul）、[太極樂隊結他手](../Page/太極樂隊.md "wikilink")[鄧建明](../Page/鄧建明.md "wikilink")（Joey）及[夏韶聲](../Page/夏韶聲.md "wikilink")（Danny
Summer）組成「Hong Kong
Band」在[香港體育館舉行兩場演唱會](../Page/香港體育館.md "wikilink")。\[6\]

## 个人生活

2010年，已和[李宗盛离婚的香港知名歌手](../Page/李宗盛.md "wikilink")[林憶蓮与恭硕良开始秘密相恋](../Page/林憶蓮.md "wikilink")。2012年，两人才向外界公开恋情，而且还公开了同居关系。\[7\]

## 音樂

### 個人專輯

  - 《Here & Now》1999年6月發行

<!-- end list -->

1.  Here & Now
2.  你著幾號鞋
3.  愛空間
4.  Here & Now（Version 9.9）

<!-- end list -->

  - 《Nowhere Man》2001年7月發行

<!-- end list -->

1.  喺你嗰度
2.  喺Eugene嗰度 featuring [包以正](../Page/包以正.md "wikilink")
3.  一米七四
4.  In my dreams
5.  Germs olo
6.  Sailing
7.  無地自容
8.  好去處（總有些用處）party mix
9.  全日遊街dum da d dum
10. Missing you
11. 六日七夜
12. 老幼咸宜

<!-- end list -->

  - 《Ginja Rock EP》2005年發行

<!-- end list -->

1.  Rize & Fall
2.  Fly
3.  Dead Zone

<!-- end list -->

  - 《JUN K》2010年9月發行

<!-- end list -->

1.  Here I Am
2.  Do I…
3.  月球人
4.  Perfecto
5.  如果
6.  Miracles
7.  十字步
8.  Life
9.  如果（Remix feat. [MC
    Jin](../Page/歐陽靖_\(香港\).md "wikilink")、[莊冬昕](../Page/莊冬昕.md "wikilink")）
10. It's Over Now
11. Man On The Moon

<!-- end list -->

  - 《Playback Is A Bitch》2012年6月發行

<!-- end list -->

1.  Damn
2.  DOB
3.  Do It All Over Again
4.  Help is On The Way（English）
5.  Whatcha Gonna Do
6.  Durrty Gurl
7.  Help is On The Way（Cantonese）
8.  Caught In A Web

<!-- end list -->

  - 《Past Future Present Tense》2015年3月發行

<!-- end list -->

1.  Hit And Run
2.  Sun Shines Down
3.  Strange Faces
4.  Give Me A Reason
5.  Here And Now
6.  愛空間
7.  Believe (English)
8.  你著幾號鞋
9.  我在機場等船
10. Believe (Canto)
11. Stay Awhile

### 派台歌曲成績

| **四台上榜歌曲最高位置**                                                               |
| ---------------------------------------------------------------------------- |
| 唱片                                                                           |
| **1999年**                                                                    |
| [Here & Now](../Page/Here_&_Now.md "wikilink")                               |
| Here & Now                                                                   |
| Here & Now                                                                   |
| [Pop 巨星聯盟2](../Page/Pop_巨星聯盟2.md "wikilink")                                 |
| **2001年**                                                                    |
| [Nowhere Man](../Page/Nowhere_Man_\(專輯\).md "wikilink")                      |
| Nowhere Man                                                                  |
| Nowhere Man                                                                  |
| **2008年**                                                                    |
| [Jun K](../Page/Jun_K.md "wikilink")                                         |
| **2009年**                                                                    |
| Jun K                                                                        |
| **2010年**                                                                    |
| Jun K                                                                        |
| Jun K                                                                        |
| **2012年**                                                                    |
| [林憶蓮 MMXI 演唱會](../Page/林憶蓮_MMXI_演唱會.md "wikilink")                           |
|                                                                              |
| [Playback Is A Bitch](../Page/Playback_Is_A_Bitch.md "wikilink")             |
| **2013年**                                                                    |
|                                                                              |
| **2014年**                                                                    |
| [Past Future Present Tense](../Page/Past_Future_Present_Tense.md "wikilink") |
| **2015年**                                                                    |
| Past Future Present Tense                                                    |
| Past Future Present Tense                                                    |
| [Roundabout](../Page/Roundabout.md "wikilink")                               |
| **2016年**                                                                    |
|                                                                              |
|                                                                              |
| **2017年**                                                                    |
|                                                                              |
| **2019年**                                                                    |
|                                                                              |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **2**       |

（\*）代表歌曲仍在榜上

### 幕後製作

  - 1999年

<!-- end list -->

  - [張柏芝](../Page/張柏芝.md "wikilink")、[高雪嵐](../Page/高雪嵐.md "wikilink") -
    男人這東西（編）
  - [陳慧琳](../Page/陳慧琳.md "wikilink")、[陳曉東](../Page/陳曉東_\(藝人\).md "wikilink")、張柏芝、[鄭中基](../Page/鄭中基.md "wikilink")、恭碩良、[蘇永康](../Page/蘇永康.md "wikilink")、[森　美](../Page/森美.md "wikilink")、[小　儀](../Page/小儀.md "wikilink")
    - 熱熱地飛（曲、編、監）
  - [鄭秀文](../Page/鄭秀文.md "wikilink") - Arigatou（編）

<!-- end list -->

  - 2000年

<!-- end list -->

  - [梅艷芳](../Page/梅艷芳.md "wikilink") - 陰差陽錯（編）
  - [陳奕迅](../Page/陳奕迅.md "wikilink") - 送院途中（編）
  - 陳奕迅 - 和平飯店（監）
  - [許志安](../Page/許志安.md "wikilink") - 直覺（編）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [梁漢文](../Page/梁漢文.md "wikilink") - 超級英雄（編、監）
  - 梁漢文 - 快歌（編、監）
  - 梁漢文 - 慢歌（編、監）
  - [許志安](../Page/許志安.md "wikilink") - 遊花園（編）
  - 許志安 - 情話太毒（編）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [朱　茵](../Page/朱茵.md "wikilink") - 一分一吋（編）
  - [謝霆鋒](../Page/謝霆鋒.md "wikilink") - 吸（曲、編、監）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [朱　茵](../Page/朱茵.md "wikilink") - 原地跳（編）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [官恩娜](../Page/官恩娜.md "wikilink") - 上岸（曲、編）
  - [黃　馨](../Page/黃馨_\(歌手\).md "wikilink") - 世界小姐（曲、編）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [官恩娜](../Page/官恩娜.md "wikilink") - 魔戒三部曲之四（曲、編）
  - 官恩娜 - 魔戒三部曲之四（Reprise）（曲、編）
  - [林海峰](../Page/林海峰_\(香港\).md "wikilink") - 老婆仔女狗仔隊（曲、編、監）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [何超儀](../Page/何超儀.md "wikilink") - 標準太太（編）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [何超儀](../Page/何超儀.md "wikilink") - 搖滾太太（編）
  - [官恩娜](../Page/官恩娜.md "wikilink") - 24小時表演（曲、編、監）
  - 官恩娜 - 假如我是一張中國畫（曲、編、監）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [胡　琳](../Page/胡琳.md "wikilink") - 野地戀人（曲）
  - 胡　琳、恭碩良 - 十字路（曲、編、監）
  - [梁詠琪](../Page/梁詠琪.md "wikilink") - 一個地方（編）
  - [謝霆鋒](../Page/謝霆鋒.md "wikilink") - 近視（曲、編）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [容祖兒](../Page/容祖兒.md "wikilink") - 蜉蝣（監）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [尹子維](../Page/尹子維.md "wikilink") - Over You（曲、編、監）
  - [林憶蓮](../Page/林憶蓮.md "wikilink") - 也許（曲）
  - 林憶蓮 - 蓋亞（曲）
  - 林憶蓮 - 假釋（曲）
  - 林憶蓮 - 兩心花（曲）
  - [胡　琳](../Page/胡琳.md "wikilink") - 尋愛旅程（曲、編）
  - [馮允謙](../Page/馮允謙.md "wikilink") - 今天開始（曲、編、監）
  - 馮允謙 - 夜半敲門（曲、編、監）
  - 馮允謙 - 想想想（編、監）
  - 馮允謙 - 笑笑（曲、編、監）
  - 馮允謙 - 那是純粹的（曲、編、監）
  - 馮允謙 - 我的九十年代（曲、編、監）
  - 馮允謙 - Spin Cycle（編、監）
  - 馮允謙 - 想寫一首歌（曲、編、監）
  - 馮允謙 - All I Need is Time（曲、編、監）
  - 馮允謙、[鍾舒漫](../Page/鍾舒漫.md "wikilink") - Feel Like Dancing（曲、編、監）
  - [蔡卓妍](../Page/蔡卓妍.md "wikilink") - 明明（編）
  - 蔡卓妍 - 鬆弛熊（編）
  - 蔡卓妍 - 但願明天一醒世界末日（編）
  - 蔡卓妍 - 種我（編）
  - 蔡卓妍 - 火車瓹山窿（編）
  - [薛凱琪](../Page/薛凱琪.md "wikilink") - 倒刺（曲）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [林憶蓮](../Page/林憶蓮.md "wikilink") - 赤裸的秘密（編、監）
  - [陳柏宇](../Page/陳柏宇.md "wikilink") - 晴空（曲、編、監）
  - 陳柏宇 - 身邊人（曲、監）
  - 陳柏宇 - 夭心夭肺（曲、編、監）
  - 陳柏宇 - 亳無餘地（曲、編、監）
  - 陳柏宇 - 倒流（編、監）
  - 陳柏宇 - I Wanna Fly（曲、編、監）
  - 陳柏宇 - 矛盾大對決（曲、編、監）
  - 陳柏宇 - 籌碼（監）
  - 陳柏宇 - Let It Go（曲、編、監）
  - [馮允謙](../Page/馮允謙.md "wikilink") - 從頭靠近你（編、監）
  - 馮允謙 - 值得愛（曲、編、監）
  - 馮允謙 - What Do You Want From Me（編、監）
  - 馮允謙 - 懂得愛（曲、編、監）
  - 馮允謙 - 世界劇團（編、監）
  - 馮允謙 - 撕開票尾（編、監）
  - 馮允謙 - Worth the Mess（編、監）
  - 馮允謙 - 如今我仍然是我嗎（曲、編、監）
  - 馮允謙 - Find My Baby（曲、編、監）
  - 馮允謙 - 無奈（曲、編、監）
  - [鍾舒漫](../Page/鍾舒漫.md "wikilink") - 吞聲忍氣（編、監）
  - 鍾舒漫 - 衝流（編、監）
  - 鍾舒漫 - 海綿（編、監）
  - 鍾舒漫 - Stop Me Baby（曲、編、監）
  - 鍾舒漫 - You（曲、編、監）
  - [蘇永康](../Page/蘇永康.md "wikilink") - 勁抽（曲、編、監）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [林憶蓮](../Page/林憶蓮.md "wikilink") - 破曉（編、監）
  - 林憶蓮 - 前塵（編、監）
  - 林憶蓮 - 喜樂（編、監）
  - 林憶蓮 - 哭（編、監）
  - 林憶蓮 - 沉淪（編、監）
  - 林憶蓮 - 點唱機（編、監）
  - 林憶蓮 - 願（編、監）
  - 林憶蓮、恭碩良 - 下雨天（編、監）
  - [柳妍熙](../Page/柳妍熙.md "wikilink") - 愛愛愛愛什麼（曲、編、監）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [吳雨霏](../Page/吳雨霏.md "wikilink") - 豔羨（監）
  - 吳雨霏 - 一分鐘稀客一分鐘色誘（曲、編、監）
  - 吳雨霏 - 一千個假想結局（曲、編、監）
  - 吳雨霏 - 我敢愛（曲、編、監）
  - [許志安](../Page/許志安.md "wikilink") - Hello，你好（曲、編、監）
  - [曾詠欣](../Page/曾詠欣.md "wikilink") - 永遠的愛麗斯（監）
  - 曾詠欣 - 灰色的小紅帽（曲、編、監）
  - 曾詠欣 - 華麗的灰姑娘（編、監）
  - 曾詠欣 - 賣火柴的女孩（曲、編、監）
  - [盧凱彤](../Page/盧凱彤.md "wikilink")、[周柏豪](../Page/周柏豪.md "wikilink")、[林奕匡](../Page/林奕匡.md "wikilink")、恭碩良
    - Now I Know（曲）
  - [謝霆鋒](../Page/謝霆鋒.md "wikilink")、[林憶蓮](../Page/林憶蓮.md "wikilink") -
    愛的味道（曲、編、監）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [林憶蓮](../Page/林憶蓮.md "wikilink") - 李香蘭（監）
  - 林憶蓮 - 殘夢（監）
  - 林憶蓮 - 鐵塔凌雲（監）
  - 林憶蓮 - 陪著你走（編、監）
  - 林憶蓮 - 分分鐘需要你（編、監）
  - 林憶蓮 - 笛子姑娘（編、監）
  - 林憶蓮 - 天各一方（監）
  - 林憶蓮 - Within You'll Remain（編、監）
  - 林憶蓮 - 童年時（編、監）
  - 林憶蓮 - 愛的根源（編、監）

<!-- end list -->

  - 2018年

<!-- end list -->

  - 陳奕迅 - 瘋狂的朋友（曲）

<!-- end list -->

  - 林憶蓮 - 沙文（曲）

## 電影

|                                         |                                             |                                                                                                                                                                                                                                                                                                               |                                                                                                                                                                                                                                        |                                  |
| --------------------------------------- | ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| **年 份**                                 | **片 名**                                     | **角 色**                                                                                                                                                                                                                                                                                                       | **合 作**                                                                                                                                                                                                                                | **導演**                           |
| 2001年                                   | [順流逆流](../Page/順流逆流.md "wikilink")          | Miguel                                                                                                                                                                                                                                                                                                        | [伍　佰](../Page/伍佰.md "wikilink")、[謝霆鋒](../Page/謝霆鋒.md "wikilink")、[盧巧音](../Page/盧巧音.md "wikilink")、[黃秋生](../Page/黃秋生.md "wikilink")、[高　捷](../Page/高捷.md "wikilink")                                                                     | [徐克](../Page/徐克.md "wikilink")   |
| 2006年                                   | [四大天王](../Page/四大天王_\(香港電影\).md "wikilink") | 恭碩良                                                                                                                                                                                                                                                                                                           | [吳彥祖](../Page/吳彥祖.md "wikilink")、[尹子維](../Page/尹子維.md "wikilink")、[陳子聰](../Page/陳子聰.md "wikilink")、[連　凱](../Page/連凱.md "wikilink")                                                                                                     | [吳彥祖](../Page/吳彥祖.md "wikilink") |
| 2009年                                   | [同門](../Page/同門.md "wikilink")              | 沙紙                                                                                                                                                                                                                                                                                                            | [余文樂](../Page/余文樂.md "wikilink")、[蔡少芬](../Page/蔡少芬.md "wikilink")、[杜汶澤](../Page/杜汶澤.md "wikilink")、[官恩娜](../Page/官恩娜.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[黃貫中](../Page/黃貫中.md "wikilink")、[梁俊一](../Page/梁俊一.md "wikilink") | [邱禮濤](../Page/邱禮濤.md "wikilink") |
| 2011年                                   | [報應](../Page/報應_\(電影\).md "wikilink")       | 綁匪乙                                                                                                                                                                                                                                                                                                           | [黃秋生](../Page/黃秋生.md "wikilink")、[任賢齊](../Page/任賢齊.md "wikilink")、[文詠珊](../Page/文詠珊.md "wikilink")、[張可頤](../Page/張可頤.md "wikilink")、[盧巧音](../Page/盧巧音.md "wikilink")、                                                                  | [羅永昌](../Page/羅永昌.md "wikilink") |
| [喜愛夜蒲](../Page/喜愛夜蒲.md "wikilink")      | Leslie                                      | [連詩雅](../Page/連詩雅.md "wikilink")、[沈志明](../Page/沈志明.md "wikilink")、[陳柏宇](../Page/陳柏宇.md "wikilink")、[陳　靜](../Page/陳靜_\(香港\).md "wikilink")、[單立文](../Page/單立文.md "wikilink")、[楊愛瑾](../Page/楊愛瑾.md "wikilink")、[何佩瑜](../Page/何佩瑜.md "wikilink")、[鄭　融](../Page/鄭融.md "wikilink")、[黃伊汶](../Page/黃伊汶.md "wikilink") | [錢國偉](../Page/錢國偉.md "wikilink")                                                                                                                                                                                                       |                                  |
| 2015年                                   | [衝上雲霄](../Page/衝上雲霄_\(電影\).md "wikilink")   | TM經理人                                                                                                                                                                                                                                                                                                         | [吳鎮宇](../Page/吳鎮宇.md "wikilink")、[張智霖](../Page/張智霖.md "wikilink")、[古天樂](../Page/古天樂.md "wikilink")、[鄭秀文](../Page/鄭秀文.md "wikilink")、[佘詩曼](../Page/佘詩曼.md "wikilink")                                                                   |                                  |
| [殺破狼2](../Page/殺破狼2.md "wikilink")      | 洪文彪                                         | [任達華](../Page/任達華.md "wikilink")、[古天樂](../Page/古天樂.md "wikilink")                                                                                                                                                                                                                                             | [鄭保瑞](../Page/鄭保瑞.md "wikilink")                                                                                                                                                                                                       |                                  |
| [紀念日](../Page/紀念日_\(電影\).md "wikilink") | 張先生                                         | [方力申](../Page/方力申.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[鍾浠文](../Page/鍾浠文.md "wikilink")                                                                                                                                                                                                            | [葉念琛](../Page/葉念琛.md "wikilink")                                                                                                                                                                                                       |                                  |

## 參考資料

## 外部链接

  - [恭碩良blog](http://www.alivenotdead.com/junkung)
  - [Jun Kung's Official Facebook
    Page](http://www.facebook.com/group.php?gid=377689563943&v=photos&so=15#!/group.php?gid=377689563943&v=wall)
  - [Jun Kung's Weibo](http://weibo.com/junkung)
  - [1](http://hk.news.yahoo.com/%E5%85%A9%E8%AD%B7%E5%A3%AB%E9%A0%82%E8%AD%89%E6%81%AD%E7%A2%A9%E8%89%AF%E8%A5%B2%E9%86%AB%E7%94%9F-223000589.html)

[Category:澳門男歌手](../Category/澳門男歌手.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:土生葡人](../Category/土生葡人.md "wikilink")
[R](../Category/香港吉他手.md "wikilink")
[Category:香港鼓手](../Category/香港鼓手.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.