**息烽县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[贵州省](../Page/贵州省.md "wikilink")[贵阳市北郊的一个](../Page/贵阳市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。[东经](../Page/东经.md "wikilink")106°27'-106°53'，[北纬](../Page/北纬.md "wikilink")26°57'-27°19'。与之接壤的县市有：东边[开阳县](../Page/开阳县.md "wikilink")，南边[修文县](../Page/修文县.md "wikilink")，西北边[遵义县](../Page/遵义县.md "wikilink")、[金沙县](../Page/金沙县.md "wikilink")。面积1037平方公里。

二戰時，息烽不是日佔區域，[国民政府军事委员会息烽](../Page/国民政府军事委员会.md "wikilink")[行辕在此設立軍官訓練學校及](../Page/行辕.md "wikilink")[集中營](../Page/息烽集中營.md "wikilink")（關押共產黨員與親中共人士），國民黨黨部也設在此地。号称“书生杀手”的[国民党](../Page/国民党.md "wikilink")[军统军官](../Page/军统.md "wikilink")[周养浩曾经担任过该行辕主任](../Page/周养浩.md "wikilink")。

息烽對外的主要公路是[210國道](../Page/210國道.md "wikilink")。

## 行政区划

下辖9个[镇](../Page/镇.md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")、1个社区服务中心\[1\]\[2\]：

。

## 外部链接

  - [新华网-息烽县网页](http://www.gz.xinhuanet.com/zfpd/gys/xf/index.htm)

## 参考文献

[息烽县](../Category/息烽县.md "wikilink")
[县](../Category/贵阳区县市.md "wikilink")
[贵阳](../Category/贵州县份.md "wikilink")

1.
2.  [贵阳市人民政府关于撤销四十九个街道办事处设立九十个社区服务中心的通知（筑府发〔2012〕58号）](http://www.gygov.gov.cn/art/2012/8/14/art_18583_411039.html)