[William_Hamilton_Prospero_and_Ariel.jpg](https://zh.wikipedia.org/wiki/File:William_Hamilton_Prospero_and_Ariel.jpg "fig:William_Hamilton_Prospero_and_Ariel.jpg")》是著名的悲喜劇作品\]\]
**悲喜剧**是一種[悲剧和](../Page/悲剧.md "wikilink")[喜剧交融并延伸的文學體裁](../Page/喜剧.md "wikilink")，兼有[悲剧和](../Page/悲剧.md "wikilink")[喜剧成分](../Page/喜剧.md "wikilink")，通常具有喜剧的圆满结局。[英國文學裡](../Page/英國文學.md "wikilink")，[威廉·莎士比亞晚期偏好寫悲喜劇的劇本](../Page/威廉·莎士比亞.md "wikilink")（又稱『傳奇劇』），在此之後直到19世紀這段期間是悲喜劇最盛行的時候。[亞里士多德的](../Page/亞里士多德.md "wikilink")《[詩學](../Page/詩學_\(亞里士多德\).md "wikilink")》中討論到關於悲劇和喜劇重複的問題\[1\]。

## 戲劇作品中的悲喜劇

### 古典時期

[古典時期並未有對於悲喜劇完整的正式定義](../Page/古典時期.md "wikilink")。古希臘哲學家[亞里士多德在他的作品](../Page/亞里士多德.md "wikilink")[詩學中探討了有著雙重結局的悲劇](../Page/詩學.md "wikilink")，似乎表明他已有與悲喜劇在[文藝復興時期的含義相似的構想](../Page/文藝復興.md "wikilink")。從這個角度來看，包括[歐里庇得斯的](../Page/歐里庇得斯.md "wikilink")《[阿爾刻提斯](../Page/阿爾刻提斯.md "wikilink")》在內的許多古希臘和古羅馬的戲劇都可稱為悲喜劇，儘管除情節外並無其他明確的悲喜劇特質。悲喜劇一詞源自[普勞圖斯的戲劇作品](../Page/普勞圖斯.md "wikilink")[安菲特律翁](../Page/安菲特律翁.md "wikilink")，他在序幕中半開玩笑地創造了該詞。劇中角色墨丘利認為在喜劇中同時囊括國王和神明以及他們的侍從是無禮的，并聲稱這劇最好是部“悲喜劇”。\[2\]\[3\]}}

## 參考文獻

<small>

  - Foster, Verna A. *The Name and Nature of Tragicomedy.* London,
    Ashgate, 2004.

</small>

[Category:戲劇類型](../Category/戲劇類型.md "wikilink")
[Category:悲劇](../Category/悲劇.md "wikilink")
[Category:喜劇](../Category/喜劇.md "wikilink")

1.  亞里士多德《詩學》第13章1453a。
2.
3.