**悲情牌**\[1\]在[選舉中指的是](../Page/選舉.md "wikilink")[政黨或](../Page/政黨.md "wikilink")[候選人](../Page/候選人.md "wikilink")，借喻受[政治迫害](../Page/政治迫害.md "wikilink")、[疾病](../Page/疾病.md "wikilink")、被[暴力](../Page/暴力.md "wikilink")[脅迫等](../Page/恐嚇.md "wikilink")[歷史或](../Page/歷史.md "wikilink")[新聞事件作為訴求](../Page/新聞.md "wikilink")，爭取[游離票](../Page/游離票.md "wikilink")[選民投下](../Page/選民.md "wikilink")[同情的](../Page/同情.md "wikilink")[選票](../Page/選票.md "wikilink")。

悲情牌可以創造出由[谷底絕處反彈的](../Page/谷底.md "wikilink")[奇蹟](../Page/奇蹟.md "wikilink")，利用[選民情緒化的反映讓選票極大化](../Page/選民.md "wikilink")。

## 臺灣著名事件

### 成功

  - [盧修一](../Page/盧修一.md "wikilink")：1997年抱病（罹[癌](../Page/癌症.md "wikilink")）下跪求票。在媒體多次播送畫面後，一般認為促使民主進步黨籍[蘇貞昌當選臺北縣長](../Page/蘇貞昌.md "wikilink")。
  - [陳水扁](../Page/陳水扁.md "wikilink")、[呂秀蓮](../Page/呂秀蓮.md "wikilink")：於[2004年中華民國總統選舉前一天](../Page/2004年中華民國總統選舉.md "wikilink")（3月19日）下午，陳、呂二人於[台南縣](../Page/台南縣.md "wikilink")[台南市宣傳之際遭刺客槍傷](../Page/台南市.md "wikilink")，結果翌日大選以三萬票之差險勝國民黨候選人[連戰](../Page/連戰.md "wikilink")、[宋楚瑜](../Page/宋楚瑜.md "wikilink")。
  - [陳建銘](../Page/陳建銘.md "wikilink")（[臺灣團結聯盟](../Page/臺灣團結聯盟.md "wikilink")）：2006年臺北市議員選前，陳建銘同妻子召開記者會抱頭痛哭，獲得最高票當選；2010年同樣再與妻子聲淚俱下，連任成功。
  - [侯彩鳳](../Page/侯彩鳳.md "wikilink")：2008年立委選前當眾**落髮明志**，支持者哭成一團，並高喊把畫面播送出去（旁邊尚有國民黨總統候選人[馬英九](../Page/馬英九.md "wikilink")）。造勢活動主持人稱：「你看看，侯彩鳳不惜她的頭髮，頭髮是女人的生命，女人的生命，你看看，她用這種無言的抗議，鄉親給她加油！給她加油！」最後侯彩鳳順利蟬聯。（中國國民黨籍）
  - [連勝文](../Page/連勝文.md "wikilink")：[2010年臺灣五都選舉前一天晚上](../Page/2010年中華民國直轄市市長暨市議員選舉.md "wikilink")（11月26日），時任國民黨中央委員、正為候選人助選的連勝文於台北縣永和市突然被黑幫刺客[槍擊重傷](../Page/1126槍擊事件.md "wikilink")，[連戰](../Page/連戰.md "wikilink")、[盧秀燕](../Page/盧秀燕.md "wikilink")、[郭素春](../Page/郭素春.md "wikilink")、[吳育昇等](../Page/吳育昇.md "wikilink")[中國國民黨人士在未有初步調查結果前](../Page/中國國民黨.md "wikilink")，旋即於造勢場合聲稱「用選票制裁暴力」並喊出「不要讓勝文的血白流」，此槍擊案對該次選舉結果造成重大影響，國民黨最終驚險守住新北市及臺中市。

### 失敗

  - [連勝文](../Page/連勝文.md "wikilink")：2014年11月19日，郝柏村抨擊台北市長候選人柯文哲是台灣「皇民的後裔」，郝柏村認為柯文哲爺爺同李登輝一樣都是皇民，是不是做官並不重要，但當時的皇民在台灣是特權階級。郝此言論引起輿論撻伐。
  - [陳朝容](../Page/陳朝容.md "wikilink")：2008年選前之夜由前妻[游月霞和現任妻子](../Page/游月霞.md "wikilink")[鄭秀珠等人激情下跪](../Page/鄭秀珠.md "wikilink")、淚灑會場催票，最後仍不敵國民黨候選人，競選連任失敗。（[親民黨籍](../Page/親民黨.md "wikilink")）
  - [宋楚瑜](../Page/宋楚瑜.md "wikilink")：2015年8月6日，宋楚瑜宣布再度參選中華民國總統，並公布此次宣傳影片，宋於片中著白色襯衫，不斷遭泥巴慘丟、抹臉，但卻全程表情鎮定，並稱「將以泥巴當作成長的養份，雖然成長不一定都是美好順利，但沒有這些美好焠鍊，怎麼雕刻出動人的靈魂」。（親民黨籍）
  - [王進士](../Page/王進士.md "wikilink")：2016年1月11日，屏東市立委王進士當著媒體鏡頭，自潑墨汁黑咖啡，潑下去整個人幾乎看不到五官，稱是要表明「無懼對手的抹黑」，參選連任奮鬥到底。惟選舉結果王進士仍輸給對手。（中國國民黨籍）

## 相關

  - [電影](../Page/電影.md "wikilink")《[悲情城市](../Page/悲情城市.md "wikilink")》
  - [配票](../Page/配票.md "wikilink")
  - [鐘擺效應](../Page/鐘擺效應.md "wikilink")
  - [西瓜效應](../Page/西瓜效應.md "wikilink")
  - [棄保效應](../Page/棄保效應.md "wikilink")
  - [競選告急牌](../Page/競選告急牌.md "wikilink")

## 注釋

<references />

[Category:選舉](../Category/選舉.md "wikilink")

1.  [台灣媒體對於](../Page/台灣媒體.md "wikilink")[選舉習慣借用](../Page/選舉.md "wikilink")[賭桌術語進行](../Page/賭桌.md "wikilink")[討論及](../Page/討論.md "wikilink")[比喻](../Page/比喻.md "wikilink")。「牌」一詞就頗有此味道，也稱為[選舉策略](../Page/選舉策略.md "wikilink")、[政治手段](../Page/政治.md "wikilink")。