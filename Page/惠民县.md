**惠民县**在[中国](../Page/中国.md "wikilink")[山东省北部](../Page/山东省.md "wikilink")、[黄河北岸](../Page/黄河.md "wikilink")，[徒骇河流贯](../Page/徒骇河.md "wikilink")，是[滨州市下辖的一个县](../Page/滨州市.md "wikilink")。

## 历史

[春秋战国时期](../Page/春秋战国.md "wikilink")，惠民县隶属于[齐国](../Page/齐国.md "wikilink")；[汉置](../Page/汉朝.md "wikilink")[厌次县](../Page/厌次县.md "wikilink")，[明朝入](../Page/明朝.md "wikilink")[武定州](../Page/武定州_\(明朝\).md "wikilink")，[清改惠民县](../Page/清.md "wikilink")。

## 地理

### 气候

惠民县气候四季分明，雨热同季，季风气候明显。 1月平均气温−3.3℃，7月平均气温26.5℃，年平均气温12.6℃。

## 行政区划

惠民县辖3个街道办事处，11个镇： 街道办事处为孙武街道办事处、武定府街道办事处、何坊街道办事处
镇为皂户李镇、姜楼镇、石庙镇、李庄镇、清河镇、胡集镇、魏集镇、淄角镇、麻店镇、桑落墅镇、大年陈镇、辛店镇

## 经济

惠民县是国家优质棉、商品粮基地县，农产有[小麦](../Page/小麦.md "wikilink")，[玉米](../Page/玉米.md "wikilink")，[大豆](../Page/大豆.md "wikilink")、[谷子](../Page/谷子.md "wikilink")、[高梁](../Page/高梁.md "wikilink")，[薯类](../Page/薯.md "wikilink")，[棉花](../Page/棉花.md "wikilink")、[花生等](../Page/花生.md "wikilink")。

工业有轻纺、化工、机械等产业。

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[魏氏庄园](../Page/魏氏庄园.md "wikilink")
  - [山东省文物保护单位](../Page/山东省文物保护单位.md "wikilink")：[大商遗址](../Page/大商遗址.md "wikilink")、[大盖遗址](../Page/大盖遗址.md "wikilink")、[路家遗址](../Page/路家遗址.md "wikilink")、[郝家遗址](../Page/郝家遗址.md "wikilink")
  - [孙子故园](../Page/孙子故园.md "wikilink")
  - [孙子兵法城](../Page/孙子兵法城.md "wikilink")（每年10月份[国际孙子文化旅游节在这里举办](../Page/国际孙子文化旅游节.md "wikilink"))。
  - [魁星阁](../Page/魁星阁.md "wikilink")
  - [古城河水利风景区](../Page/古城河水利风景区.md "wikilink")（[国家水利风景区](../Page/国家水利风景区.md "wikilink")）

## 名人

[:Category: 惠民人](../Category/_惠民人.md "wikilink")

著名军事家[孙武](../Page/孙武.md "wikilink")，[西汉重臣](../Page/西汉.md "wikilink")[东方朔等](../Page/东方朔.md "wikilink")。

## 参考文献

[惠民县](../Category/惠民县.md "wikilink") [县](../Category/滨州区县.md "wikilink")
[滨州](../Category/山东省县份.md "wikilink")