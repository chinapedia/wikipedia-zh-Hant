**惡作劇**，又稱**整蠱**、**整人**，是一种藉由[欺騙](../Page/欺騙.md "wikilink")、作弄或[冒犯他人以得到樂趣的行為](../Page/冒犯.md "wikilink")。

惡作劇最基本的形式即是故意使他人陷入窘境，並在旁觀賞他人[尷尬](../Page/尷尬.md "wikilink")、[吃驚](../Page/驚慌.md "wikilink")、惶恐等等尋常難以得見的情緒表現，藉此得到樂趣。這種惡作劇純粹是以滿足個人樂趣為目的，其他觀看者或被惡作劇的人不一定也會覺得有趣，而且往往適得其反地令人憎惡。這種惡作劇有時已經達到了[欺凌或](../Page/欺凌.md "wikilink")[犯罪的程度](../Page/犯罪.md "wikilink")，可能會面臨始料未及的嚴重後果。

惡作劇也可用文學或藝術的形式來表現，例如隨處[塗鴉](../Page/塗鴉.md "wikilink")、[諷刺漫畫](../Page/諷刺漫畫.md "wikilink")、[惡搞創作等等](../Page/惡搞.md "wikilink")。和前類惡作劇相比，這類惡作劇因為不直接與被惡作劇者接觸，所以一般較能為人所接受或原諒，但有時也可能引致意想不到的反效果，例如隨處塗鴉可能會面臨損害[賠償的問題](../Page/賠償.md "wikilink")，諷刺漫畫也有像[日德蘭郵報穆罕默德漫畫事件一樣演變成為國際問題](../Page/日德蘭郵報穆罕默德漫畫事件.md "wikilink")。

雖然惡作劇有時確實能達到[幽默](../Page/幽默.md "wikilink")、[滑稽的效果](../Page/滑稽.md "wikilink")，但也有時不但不讓人覺得有趣，反而還招人厭惡，因此社會對惡作劇的存在價值評價兩極。不過也有些高明的惡作劇是寓教於樂的，例如[澳洲廣播公司電視台的](../Page/澳洲廣播公司.md "wikilink")《[追趕者](../Page/追趕者.md "wikilink")》諷刺電視劇即為利用惡作劇來突顯某些事物的[荒谬性](../Page/荒谬.md "wikilink")，达到[讽刺或者](../Page/讽刺.md "wikilink")[教育的目的](../Page/教育.md "wikilink")。

## 例子

2012年[澳洲電台](../Page/澳洲電台.md "wikilink")兩名[唱片騎師惡作劇打扮成](../Page/唱片騎師.md "wikilink")[英女皇的聲線](../Page/英女皇.md "wikilink")，致電，查問[劍橋公爵夫人病情](../Page/凱瑟琳_\(劍橋公爵夫人\).md "wikilink")，令醫院女護士疑羞憤自殺身亡\[1\]。

[YouTube上有多个分享恶作剧视频的频道](../Page/YouTube.md "wikilink")，如[PrankvsPrank](../Page/PrankvsPrank.md "wikilink")、[ViralBrothers](../Page/ViralBrothers.md "wikilink")

## 參考文獻

## 參見

  - [幽默](../Page/幽默.md "wikilink")
  - [惡搞](../Page/惡搞.md "wikilink")
  - [騙局](../Page/騙局.md "wikilink")
  - [愚人節](../Page/愚人節.md "wikilink")
  - [亞特蘭大之夜](../Page/亞特蘭大之夜.md "wikilink")

[恶作剧](../Category/恶作剧.md "wikilink")

1.  [BBC中文網新聞](http://www.bbc.co.uk/zhongwen/trad/uk/2012/12/121207_uk_hoax_death.shtml)