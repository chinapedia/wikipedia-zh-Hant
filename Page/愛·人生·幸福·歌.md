**愛·人生·幸福·歌
(4つのL)**是[平原綾香的第四張原創專輯](../Page/平原綾香.md "wikilink")，2006年3月22日由[DREAMUSIC在日本發行](../Page/DREAMUSIC.md "wikilink")。[台灣方面是在](../Page/台灣.md "wikilink")2006年11月14日由[環球音樂代理台壓版發行](../Page/環球音樂.md "wikilink")

## 說明

專輯的原標題「4つのL」是代表"Love"（[愛](../Page/愛.md "wikilink")）、「Life」、（[命](../Page/命.md "wikilink")）、"Luck"（[運](../Page/運氣.md "wikilink")）、"Live"（生）。專輯內則收錄依這四個主題所編寫程的間奏曲。

## 收錄曲

1.  **誓言**
      -
        作詞・作曲:[小林建樹](../Page/小林建樹.md "wikilink") / 編曲:龜田誠治
        NHK轉播[杜林冬運會時的主題曲](../Page/2006年冬季奧林匹克運動會.md "wikilink")
2.  **Theme of LIVE-interlude-**
      -
        作詞:平原綾香 / 作・編曲:[新田雄一](../Page/新田雄一.md "wikilink")
3.  **Circle Game**
      -
        作詞:[松井五郎](../Page/松井五郎.md "wikilink") / 作・編曲:[2 SOUL for 2 SOUL
        Music,Inc.](../Page/2_SOUL_for_2_SOUL_Music,Inc..md "wikilink")
4.  **Reset**
      -
        作詞:[TAK\&BABY](../Page/TAK&BABY.md "wikilink") / 作曲:JUN /
        編曲:[竹下欣伸](../Page/竹下欣伸.md "wikilink")
        [PS2專用](../Page/PS2.md "wikilink")[遊戲](../Page/遊戲.md "wikilink")「[大神](../Page/大神_\(游戏\).md "wikilink")」的主題曲
5.  **I will be with you**
      -
        作詞・作曲・編曲:[村山晉一郎](../Page/村山晉一郎.md "wikilink")
6.  **Theme of LIFE-interlude-**
      -
        作・編曲:新田雄一
7.  **WILL**
      -
        作詞・作曲・編曲:新田雄一
8.  **起跑線**
      -
        作詞:平原綾香 / 作・編曲:[澤田完](../Page/澤田完.md "wikilink")
        CBC「拯救孩子！守護未來」活動主題曲
9.  **Theme of LUCK-interlude-**
      -
        作曲:平原綾香 / 編曲:平原綾香・新田雄一
10. **緣起的風**
      -
        作詞:[路川ひまり](../Page/路川ひまり.md "wikilink") /
        作曲:[ID](../Page/ID.md "wikilink") /
        編曲:[YANAGIMAN](../Page/YANAGIMAN.md "wikilink")
        NHK動畫「[彩雲國物語](../Page/彩雲國物語.md "wikilink")」片頭曲
11. **白羊座**
      -
        作詞:平原綾香 / 作・編曲:[宮川彬良](../Page/宮川彬良.md "wikilink")
12. **給未來的我們**
      -
        作詞:平原綾香 / 作・編曲:2 SOUL for 2 SOUL Music,Inc.
13. **Theme of LOVE-interlude-**
      -
        作詞・作曲・編曲:平原綾香
14. **Eternally**
      -
        作詞:平原綾香・松井五郎 / 作曲:平原綾香 / 編曲:[島健](../Page/島健.md "wikilink")
        電影「[四日間の奇蹟](../Page/四日間の奇蹟.md "wikilink")」主題曲
15. **Music**
      -
        作詞:平原綾香 / 作曲:新田雄一 / 編曲:龜田誠治
16. **誓言 (Album Version)**
      -
        作詞・作曲:小林建樹 / 編曲:龜田誠治
17. **明日 (亞洲版特別收錄)**
      -
        作詞:松井五郎 / 作曲:A.Gagnon / 編曲:小林信吾

[Category:平原綾香音樂專輯](../Category/平原綾香音樂專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:Dreamusic音樂專輯](../Category/Dreamusic音樂專輯.md "wikilink")