**株式會社愛媛電視**（Ehime Broadcasting Co.,
Ltd.），簡稱**愛媛電視台**，是[日本的一家以](../Page/日本.md "wikilink")[愛媛縣為放送地域的](../Page/愛媛縣.md "wikilink")[電視台](../Page/電視台.md "wikilink")，為[富士電視台](../Page/富士電視台.md "wikilink")[聯播網](../Page/聯播網.md "wikilink")（[FNN](../Page/FNN.md "wikilink")·[FNS](../Page/FNS.md "wikilink")）的成員。開播之初名稱為「愛媛放送株式會社」，但其愛稱「愛媛電視台」反被縣民認為較親切，于2004年10月1日正式改名為「愛媛電視台」。簡稱[EBC](../Page/EBC.md "wikilink")，[呼號為JOEI](../Page/呼號.md "wikilink")-DTV。開播于1969年12月10日。
[EBC_Matsuyama_HQ_20090808-001.jpg](https://zh.wikipedia.org/wiki/File:EBC_Matsuyama_HQ_20090808-001.jpg "fig:EBC_Matsuyama_HQ_20090808-001.jpg")

## 節目

### 新聞

  - 午間新聞：FNN Speak ()星期一-星期五11:30-12:00

<!-- end list -->

  -
    节目的途中，广播爱媛县的新闻和天气预报。

<!-- end list -->

  - 晚間新聞：EBC超級新聞FNN（） 星期一-星期五16:53-18:55
      - 这个节目是「2・5小时」的一部分。
      - 节目的途中，广播爱媛县的新闻和天气预报。
  - FNN週末超級新聞EBC（） 星期六及星期日17:30-18:00
      - 节目的途中，广播爱媛县的新闻和天气预报。

### 生活信息节目(爱媛县)

  - 新鲜！全部纯粹的爱媛()星期一-星期五16:25-16:52
      - 这个节目是「2・5小时」的一部分。

## 外部連結

  - [愛媛電視台](http://www.ebc.co.jp/)(日語)

[Category:日本電視台](../Category/日本電視台.md "wikilink")
[E](../Category/富士新聞網.md "wikilink")