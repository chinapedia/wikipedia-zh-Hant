[Edward_Steichen.jpg](https://zh.wikipedia.org/wiki/File:Edward_Steichen.jpg "fig:Edward_Steichen.jpg")
**愛德華·史泰欽**（Edward Steichen；1879年3月27日——1973年3月25日） 全名為Edward Jean
Steichen，也常簡寫為Edward J.
Steichen。他是一位知名的美國攝影師。同時也是畫家，和美術畫廊主人，還有博物館主任，出生在[盧森堡](../Page/盧森堡.md "wikilink")。1881年隨家族移民到美國，在1900年成為歸化的美國公民。

史泰欽青年時原本想要成為一位頂尖的畫家，16歲時受到了畫意攝影主義影響，開始對攝影產生興趣，在21歲轉站紐約要到歐洲習畫時，結識了當時的攝影大師[史帝格力茲](../Page/史帝格力茲.md "wikilink")，並受到青睞賣給他三張攝影作品，歐洲回來後，史泰親轉行成為了一位優秀的現代攝影師。並在1905年與史帝格力茲成立了[291藝廊](../Page/291藝廊.md "wikilink")。

[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，他成為了[美國遠征部隊](../Page/美國遠征部隊.md "wikilink")（[:en:American
Expeditionary
Forces](../Page/:en:American_Expeditionary_Forces.md "wikilink")）的一員，風格趨向於寫實攝影主義，後期則轉向了時裝攝影的領域。拍攝過相當多的影視巨星和政治人物，作品屢屢在當時的[生活雜誌封面登出](../Page/生活雜誌.md "wikilink")。

在[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")，他是美國海軍攝影學院的主任。戰爭結束後到1962年間，史泰欽都在紐約市的[現代藝術博物館擔任攝影部的主任](../Page/現代藝術博物館.md "wikilink")。1973年，史泰欽在康州的West
Redding過世。

## 攝影展

1955年，史泰欽主持的大型攝影團體展《[四海一家](../Page/四海一家.md "wikilink")》（[:en:The Family
of
Man](../Page/:en:The_Family_of_Man.md "wikilink")：暫譯）獲得了不少好評，展覽共有來自不同攝影師所拍攝的500多張作品，以來自不同國家的人民為主題，描述了他們的生活、愛和死亡。史泰欽的姐夫[卡爾·山伯格](../Page/卡爾·山伯格.md "wikilink")（[:en:Carl
Sandburg](../Page/:en:Carl_Sandburg.md "wikilink")）替此展出版有攝影集（ISBN
0810961695）並依史泰欽的遺願捐給了[盧森堡大公國](../Page/卢森堡.md "wikilink")，這500張作品目前永久的被盧森堡[克萊沃鎮](../Page/克萊沃鎮.md "wikilink")（[:en:Clervaux](../Page/:en:Clervaux.md "wikilink")）保存。

## 池畔月色

2006年2月，早期帶有畫意攝影主意的史泰欽作品《池畔月色》（The
Pond-Moonlight；1904年，非第一張沖洗版本。[圖片](http://www.artnet.com/magazineus/news/artmarketwatch/artmarketwatch2-3-06_detail.asp?picnum=7)）以美金290萬元售出，是美國攝影史上，攝影作品拍賣的最高價之一。此作品在紐約州[威徹斯特郡](../Page/威徹斯特郡.md "wikilink")（[Westchester](../Page/:en:Westchester_County,_New_York.md "wikilink")）的[馬馬羅內克](../Page/馬馬羅內克.md "wikilink")（[Mamaroneck](../Page/:en:Mamaroneck_\(town\),_New_York.md "wikilink")）拍攝，畫面中有一片幽靜的林子，月光在林子上灑下，並造成了池畔的些微反光。此作之所以能以史上高價售出，主要原因就在於其為畫意攝影主義的經典之作；另一難得之處是僅沖洗了三份，其中兩份已被博物館收藏。這幅作品在攝影史上也有技術的意義，主要是史泰欽採用了法國魯米爾兄弟所發明的[天然彩色相片技術](../Page/天然彩色相片技術.md "wikilink")〈[:en:Autochrome](../Page/:en:Autochrome.md "wikilink")〉，而拍出帶有豐富層次感和意境的早期彩色相片之一。

## 外部連結

  - [Edward J. Steichen
    Online](http://www.artcyclopedia.com/artists/steichen_edward_j.html)
  - [Edward Steichen
    Photographs](https://web.archive.org/web/20081007003802/http://worldart.sjsu.edu/4DACTION/HANDLECGI/CTN1?theKW=Steichen&RefineSearch=NewSelection)
  - [CNN Article regarding the sale of "The
    Pond-Moonlight"](http://www.cnn.com/2006/US/02/14/photo.auction.ap/)

[S](../Category/美国摄影师.md "wikilink")
[S](../Category/1879年出生.md "wikilink")
[S](../Category/1973年逝世.md "wikilink")