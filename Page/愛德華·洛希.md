**愛德華·艾伯特·洛希**
（，），[法國數學家與天文學家](../Page/法國.md "wikilink")，他最傑出的表現是在[天體力學的領域](../Page/天體力學.md "wikilink")，他的名字被冠在[洛希球](../Page/洛希球.md "wikilink")、[洛希極限和](../Page/洛希極限.md "wikilink")[洛希瓣等觀念上](../Page/洛希瓣.md "wikilink")。

他誕生於[蒙彼利埃](../Page/蒙彼利埃.md "wikilink")，並就讀於[蒙彼利埃大學](../Page/蒙彼利埃大學.md "wikilink")，稍後他並成為該校的教授，於1849年開始擔任Faculté
des科學講座。洛希利用數學研究[拉普拉斯的](../Page/皮埃爾-西蒙·拉普拉斯.md "wikilink")[星雲假說並將得到的結果發表在他任職的的蒙特利埃研究會的學報上](../Page/太陽星雲.md "wikilink")，直到1877年。其中最重要的是[彗星](../Page/彗星.md "wikilink")（1860年）和[星雲假說](../Page/太陽星雲.md "wikilink")（1873年）本身。洛希的研究解釋了強大引力場中小顆粒群集的效應。

他在歷史上最著名的理論或許是關於[土星的](../Page/土星.md "wikilink")[行星環如何形成的理論](../Page/行星環.md "wikilink")，當一顆巨大的[衛星過度接近土星時會被](../Page/天然衛星.md "wikilink")[重力拉扯而分離](../Page/萬有引力.md "wikilink")。他描述了一種計算聚集在一起的物體在何種距離內就會被[潮汐力扯碎掉](../Page/潮汐力.md "wikilink")，這個距離就是所知的[洛希極限](../Page/洛希極限.md "wikilink")。

他另一個著名的著名的工作是在[軌道力學上的發展](../Page/軌道力學.md "wikilink")。[洛希瓣描述一個小物體環繞著另兩個物體時會被后者捕獲的限制](../Page/洛希瓣.md "wikilink")，而[洛希球類似於](../Page/洛希球.md "wikilink")[重力場球對](../Page/重力場.md "wikilink")[天體的影響](../Page/天體.md "wikilink")，在受到另一個大質量天體的攝動時如何影響它環繞的[軌道](../Page/軌道.md "wikilink")。

[Category:法國天文學家](../Category/法國天文學家.md "wikilink")
[Category:潮汐力](../Category/潮汐力.md "wikilink")