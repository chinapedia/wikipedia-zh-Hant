《**愛情的證明**》（[天城體](../Page/天城體.md "wikilink")：कुछ कुछ होता है，
[拉丁字母](../Page/拉丁字母.md "wikilink"): *Kuch Kuch Hota
Hai*，意：“有事，有事会发生”）
是1998年的[寶萊塢電影](../Page/寶萊塢.md "wikilink")，由導演[卡蘭·喬哈](../Page/卡蘭·喬哈.md "wikilink")（Karan
Johar）執導，明星陣容鼎盛，當年此片獲得極大成功，打破當時的票房記錄，並順理成章地獲得《[印度電影觀眾獎──最佳影片](../Page/印度電影觀眾獎.md "wikilink")》、以及男女主角等大獎。

## 故事梗概

男主角Shahrukh和Rani，Kajol都是大學同學，Rani和Kajol都喜歡shahrukh，Rani美麗大方，Kajol活潑好動，梳著短髮，男孩子的性格。
Shahrukh愛上Rani，並與之結婚。妻子在分婉的時候難產，但是為了保住孩子，選擇犧牲自己，臨死前給女兒留下很多信，讓她每年生日讀一封，並給她取了Kajol的名字。
7年後，男主角從心中明白了kajol的心意，此時的kajol也變得美麗大方，留起了長髪，很有女人味，他們再次相遇了，愛火重燃，可是kajol已經和另一個男人訂婚。

## 卡士

  - [沙鲁可·汗](../Page/沙鲁可·汗.md "wikilink") （*[Shah Rukh
    Khan](../Page/Shah_Rukh_Khan.md "wikilink")*）
  - [卡卓兒](../Page/卡卓兒.md "wikilink")
    （*[Kajol](../Page/Kajol.md "wikilink")*）
  - [拉妮·玛克赫吉](../Page/拉妮·玛克赫吉.md "wikilink") （*[Rani
    Mukerji](../Page/Rani_Mukerji.md "wikilink")*）
  - [萨尔曼·罕](../Page/萨尔曼·罕.md "wikilink") （*[Salman
    Khan](../Page/Salman_Khan.md "wikilink")*）
  - [法莉達·賈勒爾](../Page/法莉達·賈勒爾.md "wikilink") （*Farida Jalal*）

## 參考

  - [寶萊塢票房記錄](../Page/寶萊塢票房記錄.md "wikilink")

## 外部連結

  -
  - [電影的公式網址](http://www.yashrajfilms.com/Movies/MovieIndividual.aspx?MovieID=41974cae-0d7e-4d01-84ec-c53ab5920d78)

[K](../Category/1998年電影.md "wikilink")
[K](../Category/印度電影作品.md "wikilink")