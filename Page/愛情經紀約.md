《**愛情經紀約**》（[英語](../Page/英語.md "wikilink")：Engagement For
Love）是2006年[三立華人電視劇週日十點檔系列的第十三部作品](../Page/三立電視戲劇節目列表#華人電視劇.md "wikilink")。全劇共15集。由[三立電視和](../Page/三立電視.md "wikilink")[台视合作推出](../Page/台湾电视公司.md "wikilink")。接檔《[微笑Pasta](../Page/微笑Pasta.md "wikilink")》。

## 劇情

劇中講述一家經紀公司的老闆刑天（[杜德偉飾](../Page/杜德偉.md "wikilink")）被外界視為吸血魔王，而初出茅蘆一心要當導演的小女孩宣夢跳（[南拳媽媽的Lara梁心頤飾](../Page/南拳媽媽.md "wikilink")）更因為好友朱可欣（[卓文萱飾](../Page/卓文萱.md "wikilink")）的關係仇視刑天，但她卻亂打亂撞，闖進了刑天的世界，更把刑天當成「刑天的司機」，進而更戀上了他。另外，宣夢跳的弟弟在網上以她的名義玩「網絡同居」，以拍電影作理由，騙了[香港](../Page/香港.md "wikilink")[油麻地](../Page/油麻地.md "wikilink")[廟街流氓張家寶](../Page/庙街_\(香港\).md "wikilink")（[許紹洋飾](../Page/許紹洋.md "wikilink")）十萬港幣，最終更被高利貸上台討債，他結果來台找宣夢跳算帳，除了意外地戀上宣夢跳之外，更因為外形酷似刑天旗下卻失蹤的藝人Young（許紹洋分飾），刑天看中了他要躲債，更要他假扮Young，從而展開一段愛與約定的故事。劇情最終發展為，張家寶所扮的Young，最終被飾破，卻在刑天的巧妙佈局下，張家寶卻成為新一代的巨星，最後張家寶與宣夢跳在一起後浪跡天涯，而真正的Young回歸經紀公司，卻被要求假扮為張家寶。

## 角色介紹

### 宣家

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>關係/暱稱/簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>宣大綱</p></td>
<td><p><a href="../Page/卜學亮.md" title="wikilink">卜學亮</a></p></td>
<td><p><strong>宣爸</strong><br />
55歲，宣夢跳、宣夢魚之父，一身小人物性格，自卑又自大，好大喜功，樂觀但無毅力，做事三分鐘熱度，能言善道，但時常吹噓彭風。唯一優點是對生命絕對樂觀，屢敗屢戰，堅持夢想，勇往直前。口頭禪是：「隨時都有可能出現奇蹟」</p></td>
</tr>
<tr class="odd">
<td><p>宣　媽</p></td>
<td><p><a href="../Page/呂曼茵.md" title="wikilink">呂曼茵</a></p></td>
<td><p><strong>宣媽</strong><br />
50歲，宣夢跳、宣夢魚之母，宣家家庭家長。 跳跳的媽媽，個性熱情爽朗，雞婆又衝動，嗓門特大，常常在門口就可聽到宣媽罵人的嗓音。雙手萬能，擁有一手媲美傅培梅的好廚藝，凡事斤斤計算</p></td>
</tr>
<tr class="even">
<td><p>宣夢跳</p></td>
<td><p><a href="../Page/Lara.md" title="wikilink">梁心頤</a></p></td>
<td><p><strong>跳跳</strong><br />
宣大綱之女、宣夢魚之姐<br />
女，20歲，目前就讀大學生影劇系四年級。 熱情，幹勁，充滿目標與理想，清楚的知道自己就是要當導演。只是對於導演夢以外的事，因為不太在乎，因此顯得神經大條，甚至有些狀況外。 口頭禪「在16:9的世界裡，每一秒都有千萬種可能，包括奇蹟」</p></td>
</tr>
<tr class="odd">
<td><p>宣夢魚</p></td>
<td><p><a href="../Page/謝健忠.md" title="wikilink">安東尼</a></p></td>
<td><p>16歲，宣大綱之子、宣夢跳之弟，青春期的男孩，生性樂天，叛逆，衝動，愛吹牛誇大卻又創意十足</p></td>
</tr>
</tbody>
</table>

### 張家

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>關係/暱稱/簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>張家寶</p></td>
<td><p><a href="../Page/許紹洋.md" title="wikilink">許紹洋</a></p></td>
<td><p><strong>小混混/假Young/家寶</strong><br />
25歲，喜歡跳跳。看似漫不經心，毫不在乎，一痞天下無難事，實則為人四海，不畏強權，重情重義，『有苦自己吞，有難自己扛』，永遠開朗而陽光的笑容裡，正是他體貼易感又善良的心。</p></td>
</tr>
<tr class="odd">
<td><p>鳳　姨</p></td>
<td><p><a href="../Page/王琄.md" title="wikilink">王琄</a></p></td>
<td><p>張家寶養母</p></td>
</tr>
<tr class="even">
<td><p>張　爸</p></td>
<td></td>
<td><p>張家寶親生父親</p></td>
</tr>
<tr class="odd">
<td><p>張　媽</p></td>
<td></td>
<td><p>張家寶親生母親</p></td>
</tr>
</tbody>
</table>

### 朱家

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>關係/暱稱/簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>朱正峰</p></td>
<td><p><a href="../Page/陶傳正.md" title="wikilink">陶傳正</a></p></td>
<td><p><strong>朱老爺</strong><br />
朱可欣之父</p></td>
</tr>
<tr class="odd">
<td><p>朱可欣</p></td>
<td><p><a href="../Page/卓文萱.md" title="wikilink">卓文萱</a></p></td>
<td><p><strong>可欣小姐</strong><br />
20歲，跳跳的同學。也是刑天的大恩人——朱正峰唯一的獨生女。 可欣出生的時候，父親朱正峰已由有志難伸的檢察官，成功轉型為人人敬重的企業家。所以可欣從小錦衣玉食，被父母捧在手心上，也養成了任性又倔強的脾氣。<br />
童年由<a href="../Page/傅佩慈.md" title="wikilink">傅佩慈饰演</a></p></td>
</tr>
<tr class="even">
<td><p>刑　天</p></td>
<td><p><a href="../Page/杜德偉.md" title="wikilink">杜德偉</a></p></td>
<td><p><strong>吸血魔王/奇蹟大叔</strong><br />
38歲，朱正峰的養子，隻手打造業界無可匹敵的「銓經紀」娛樂王國，是圈內動見觀瞻，舉足輕重的幕後推手。</p></td>
</tr>
<tr class="odd">
<td><p>Elson</p></td>
<td><p><a href="../Page/狄志杰.md" title="wikilink">狄志杰</a></p></td>
<td><p>朱正峰第二老婆的兒子</p></td>
</tr>
<tr class="even">
<td><p>Lyia</p></td>
<td><p><a href="../Page/林美貞.md" title="wikilink">林美貞</a></p></td>
<td><p>朱正峰的第二個老婆</p></td>
</tr>
</tbody>
</table>

### 銓經紀

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>關係/暱稱/簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>太　司</p></td>
<td><p><a href="../Page/金沛晟.md" title="wikilink">金沛晟</a></p></td>
<td><p>30歲，經紀人。冷、酷、狠、深沉的個性裏卻蘊含著無限的爆發力，平時沉默寡言一天說不到三句話，可是一旦惹到他，絕對吃不完兜著走</p></td>
</tr>
<tr class="odd">
<td><p>芳　華</p></td>
<td><p><a href="../Page/鄧九雲.md" title="wikilink">鄧九雲</a></p></td>
<td><p>29歲，經紀人。積極能幹，八面玲瓏，美麗大方，對人溫柔體貼，善解人意，因此公司內藝員有委屈都會像她抱怨，做事認真負責，忠心耿耿</p></td>
</tr>
<tr class="even">
<td><p>喬安娜</p></td>
<td><p><a href="../Page/林孟瑾.md" title="wikilink">林孟瑾</a></p></td>
<td><p><strong>Joanna</strong><br />
30歲，喜歡刑天。螢光幕上的形象，知性美麗，八面玲瓏，溫柔可人，EQ高，對記者影迷永遠友善隨和，因此有著「甜心寶貝」的封號。私下的喬安娜，聰明，積極，事事要求完美，有時會有著歇斯底里的神經質，常常讓周遭的人大感吃不消。</p></td>
</tr>
<tr class="odd">
<td><p>Lawrence</p></td>
<td><p><a href="../Page/唐家豪.md" title="wikilink">唐豐</a></p></td>
<td><p>25歲。銓經紀屈居Young之下的男藝人</p></td>
</tr>
<tr class="even">
<td><p>Young</p></td>
<td><p><a href="../Page/許紹洋.md" title="wikilink">許紹洋</a></p></td>
<td><p>27歲。 充滿明星魅力，精湛的演技，憂鬱無極的眼神，再再都征服了影迷，成為聲勢如日中天的一代天王巨星</p></td>
</tr>
</tbody>
</table>

### 其他角色

<table>
<tbody>
<tr class="odd">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td><p><strong>關係/暱稱/簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>高金發</p></td>
<td><p><a href="../Page/高捷_(演員).md" title="wikilink">高捷</a></p></td>
<td><p><strong>高老闆</strong><br />
45歲。台式老大，心狠手辣，殺人不眨眼，江湖中令人聞風破膽的「虎堂」黑道大哥。常掛在嘴上的名言為：「這不是錢的問題，是面子問題…」</p></td>
</tr>
<tr class="odd">
<td><p>力　哥</p></td>
<td><p><a href="../Page/張朝閔.md" title="wikilink">張朝閔</a></p></td>
<td><p><strong>阿力</strong><br />
25歲。高老闆身旁小弟</p></td>
</tr>
<tr class="even">
<td><p>美　惠</p></td>
<td><p><a href="../Page/夏曈.md" title="wikilink">夏曈</a></p></td>
<td><p>喬安娜的助理</p></td>
</tr>
<tr class="odd">
<td><p>小　貞</p></td>
<td></td>
<td><p>Young的影迷</p></td>
</tr>
<tr class="even">
<td><p>吳婉怡</p></td>
<td></td>
<td><p>娛樂記者</p></td>
</tr>
</tbody>
</table>

## 收視率（台視）

| 播映日期       | 集數    | 收視率  |
| ---------- | ----- | ---- |
| 2006/11/12 | 01    | 1.87 |
| 2006/11/19 | 02    | 1.54 |
| 2006/11/26 | 03    | 1.71 |
| 2006/12/03 | 04    | 1.30 |
| 2006/12/10 | 05    | 1.25 |
| 2006/12/17 | 06    | 1.09 |
| 2006/12/24 | 07    | 0.87 |
| 2007/01/07 | 08    | 0.56 |
| 2007/01/14 | 09    | 0.92 |
| 2007/01/21 | 10    | 1.03 |
| 2007/01/28 | 11    | 1.13 |
| 2007/02/04 | 12    | 1.14 |
| 2007/02/11 | 13    | 0.86 |
| 2007/02/25 | 14    | 0.80 |
| 2007/03/04 | 15    | 1.26 |
| 收視平均       | |1.16 |      |

## 其他

此劇在第一集開首是許紹洋飾的張家寶在香港被討債的劇情，故此出現數分鐘的純粵語對白，在台灣偶像劇中十分罕見。而此劇三個主要男演員，都可歸類為香港藝人
，同時也是[最佳娛樂經紀公司旗下藝人](../Page/最佳娛樂經紀公司.md "wikilink")。主角[杜德偉是香港歌手](../Page/杜德偉.md "wikilink")，而另一男主角[許紹洋雖在台灣出道](../Page/許紹洋.md "wikilink")，但卻是在香港出生和生活多年，[馬來西亞籍的配角金沛晟之前則是香港](../Page/馬來西亞.md "wikilink")[亞洲電視的基本演員](../Page/亞洲電視.md "wikilink")，也是三立電視偶像劇少見的配搭。三個以粵語作為主要語言的演員，再加上以英語為母語的女主角的梁心頤，整套電視劇的演員在對白上平添一份趣味。

## 音樂

  - 主題曲：＜迷途＞演唱：[杜德偉](../Page/杜德偉.md "wikilink")
  - 片尾曲：＜未完待續＞演唱：[杜德偉](../Page/杜德偉.md "wikilink") 及
    [Lara](../Page/Lara.md "wikilink")
  - 插　曲：＜愛正要起飛＞、＜無人值守＞、＜跳舞 跳舞＞演唱：[許紹洋](../Page/許紹洋.md "wikilink")
  - 插　曲：＜想戀愛＞演唱：[許紹洋](../Page/許紹洋.md "wikilink") 及
    [金沛晟](../Page/金沛晟.md "wikilink")

## 作品的變遷

## 外部連結

  - [《愛情經紀約》台視官方網站](http://www.ttv.com.tw/drama/2006/forlove/)

[Ai愛情經紀約](../Category/台灣偶像劇.md "wikilink")
[Ai愛情經紀約](../Category/2006年台灣電視劇集.md "wikilink")
[Ai愛情經紀約](../Category/台視電視劇.md "wikilink")
[Ai愛情經紀約](../Category/三立都會台戲劇節目.md "wikilink")
[Ai愛情經紀約](../Category/無綫電視外購劇集.md "wikilink")
[Category:演藝界題材電視劇](../Category/演藝界題材電視劇.md "wikilink")