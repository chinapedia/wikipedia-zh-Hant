是一部於2008年4月17日上映的[香港愛情](../Page/香港.md "wikilink")[電影](../Page/香港電影.md "wikilink")，由導演[崔允信執導](../Page/崔允信.md "wikilink")。該片曾經參展[第三十二屆香港國際電影節](../Page/第三十二屆香港國際電影節.md "wikilink")\[1\]，並且安排在「香港電影面面觀2007-2008」單元中放映。本劇由香港2007年4個樂壇新人王[陳柏宇](../Page/陳柏宇.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[周柏豪和](../Page/周柏豪.md "wikilink")[鍾舒漫主演](../Page/鍾舒漫.md "wikilink")。

《愛情萬歲》劇情以[香港](../Page/香港.md "wikilink")[中環為背景](../Page/中環.md "wikilink")，用三段分支描述三種不同的[愛情風貌](../Page/愛情.md "wikilink")\[2\]。影評家[翁子光認為](../Page/翁子光.md "wikilink")，該片企圖探索[香港人在都市愛情的疑問與徬徨](../Page/香港人.md "wikilink")，卻只提問而無解答\[3\]。

## 主要演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></strong></p></td>
<td><p><strong>阿生</strong></p></td>
<td><p>戀戀SoHo職員<br />
阿晴男友<br />
阿星、楊溢德好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/洪卓立.md" title="wikilink">洪卓立</a></strong></p></td>
<td><p><strong>阿星</strong></p></td>
<td><p>外賣仔<br />
暗戀阿如，後男友<br />
阿生好友</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></strong></p></td>
<td><p><strong>楊溢德</strong></p></td>
<td><p><strong>Joe</strong><br />
見習醫生<br />
Kelly男友<br />
阿生好友<br />
Maggie前男友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鍾舒漫.md" title="wikilink">鍾舒漫</a></strong></p></td>
<td><p><strong>黃日晴</strong></p></td>
<td><p>戀戀SoHo職員<br />
阿生女友<br />
許少雄前女友<br />
阿如好友</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鄭希怡.md" title="wikilink">鄭希怡</a></strong></p></td>
<td><p><strong>阿如</strong></p></td>
<td><p>畫廊老闆<br />
方志豪女友，後分手<br />
阿星暗戀對象，後女友<br />
黃日晴好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐素琪.md" title="wikilink">唐素琪</a></strong></p></td>
<td><p><strong>Kelly</strong></p></td>
<td><p>Maggie表妹<br />
Joe女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧健泓.md" title="wikilink">鄧健泓</a></p></td>
<td><p>方志豪</p></td>
<td><p>股票經紀<br />
阿如男友，後分手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張繼聰.md" title="wikilink">張繼聰</a></p></td>
<td><p>許少雄</p></td>
<td><p>Martin<br />
黃日晴前男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/方皓玟.md" title="wikilink">方皓玟</a></p></td>
<td><p>Maggie</p></td>
<td><p>Kelly表姐<br />
楊溢德前女友</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歌曲

  - 〈傻小子〉 - [周柏豪](../Page/周柏豪.md "wikilink")=主題曲
  - 〈萬中無一〉 - [陳柏宇](../Page/陳柏宇.md "wikilink")=插曲
  - 〈愛．無膽〉 - [洪卓立](../Page/洪卓立.md "wikilink")=插曲

## 參考文獻與注釋

## 外部連結

  - 《[愛情萬歲](https://web.archive.org/web/20080407012816/http://hk.movies.yahoo.com/movie.html?id=mcl_loveiselsewhere)》在香港[雅虎的電影頁面](../Page/雅虎.md "wikilink")

  -
  -
  -
  -
  - [第三十二屆香港國際電影節](http://www.hkiff.org.hk/chi/programme/show_detail.php?fi_id=671&se_id=11)

[8](../Category/2000年代香港電影作品.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:香港愛情片](../Category/香港愛情片.md "wikilink")
[Category:香港島背景電影](../Category/香港島背景電影.md "wikilink")

1.  [第三十二屆香港國際電影節](http://www.hkiff.org.hk/chi/programme/show_detail.php?fi_id=671&se_id=11)
2.  [香港愛情面面觀《愛情萬歲》](http://movie.pchome.com.tw/review.php?no=1305587135&sid=amyyym&type=1)
3.  [《愛情萬歲》愛情土壤貧瘠的蘇豪](http://hk.myblog.yahoo.com/yungtszkwong/article?mid=574)