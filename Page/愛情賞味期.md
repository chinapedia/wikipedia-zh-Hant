《**-{zh-tw:愛情賞味期; zh-hk:5x2;
zh-cn:爱情赏味期;}-**》（）是一套[2004年上映的](../Page/2004年電影.md "wikilink")[法國劇情片](../Page/法國電影.md "wikilink")。導演為法國名導[法蘭索瓦·歐容](../Page/法蘭索瓦·歐容.md "wikilink")。

## 故事大綱

主要是藉由[倒敘的手法來描寫一對男女由當初的邂逅](../Page/倒敘.md "wikilink")、認識、熱戀進而[結婚](../Page/結婚.md "wikilink")[生子步入](../Page/生子.md "wikilink")[家庭而最終關係破裂](../Page/家庭.md "wikilink")，曲終人散。

## 主要演員

  - [史蒂芬．福瑞斯](../Page/史蒂芬．福瑞斯.md "wikilink")
  - [瓦萊麗雅·布魯尼-特德斯奇](../Page/瓦萊麗雅·布魯尼-特德斯奇.md "wikilink")

## 外部連結

  - [電影在導演官網上的頁面](http://www.francois-ozon.com/en/filmo-5x2-five-times-two)

  -
  - [愛情賞味期 5x2 -- @movies【開眼電影】 @movies
    <http://www.atmovies.com.tw>](http://app.atmovies.com.tw/movie/movie.cfm?action=filmdata&film_id=fffr50354356)

[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:法語電影](../Category/法語電影.md "wikilink")
[Category:法国电影作品](../Category/法国电影作品.md "wikilink")
[Category:非直線敘事電影](../Category/非直線敘事電影.md "wikilink")
[Category:2000年代浪漫劇情片](../Category/2000年代浪漫劇情片.md "wikilink")
[Category:法兰索瓦·奥桑电影](../Category/法兰索瓦·奥桑电影.md "wikilink")