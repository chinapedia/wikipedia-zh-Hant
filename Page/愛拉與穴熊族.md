《**愛拉與穴熊族**》（[英語](../Page/英語.md "wikilink")：***The Clan of the Cave
Bear***）是[美國作家](../Page/美國.md "wikilink")[珍·奧爾](../Page/珍·奧爾.md "wikilink")（）
於1980年出版的[歷史小說](../Page/歷史小說.md "wikilink")，《》[叢書的第一部](../Page/叢書.md "wikilink")。

在[舊石器時代晚期](../Page/舊石器時代.md "wikilink")，[尼安德塔人與](../Page/尼安德塔人.md "wikilink")[克羅瑪儂人](../Page/克羅瑪儂人.md "wikilink")（最初的現代智人）同時生存於[歐洲](../Page/歐洲.md "wikilink")，作者由此假想兩種人種彼此接觸的過程。故事敘述一個五歲的克羅瑪儂人女孩愛拉，因為地震災害失去親人，而被一個穴熊族部落（尼安德塔人）收養。愛拉身為「異族」而備受歧視，但她仍然在困境中努力成長。

本書對於石器時代原始部落的採集狩獵生活、以及宗教巫術儀式，皆有獨到而細膩的描寫，唯作者是基於寫作當時的人類學知識來設想，未必與晚近的考古新發現相符。

## 外部連結

[誠品書局石器時代傳奇愛拉與穴熊族](http://www.eslitebooks.com/exhibition/070625_cavebear/answer.shtml)

[Category:美國歷史小說](../Category/美國歷史小說.md "wikilink")
[Category:1980年美國小說](../Category/1980年美國小說.md "wikilink")