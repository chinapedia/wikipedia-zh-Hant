**愛斯基摩星雲**（Eskimo Nebula，**NGC
2392**），亦稱**小丑臉星雲**\[1\]，是位於[雙子座的一個](../Page/雙子座.md "wikilink")[行星狀星雲](../Page/行星狀星雲.md "wikilink")，距離地球約2,900光年遠，由英国天文學家[威廉·赫歇尔在](../Page/威廉·赫歇尔.md "wikilink")1787年發現的；使用小型[望遠鏡就可以看見](../Page/望遠鏡.md "wikilink")。

由於地面上觀察受大氣影響，影像不甚清晰之下，它像是被帶頭罩雪衣的敞篷圍攏的正面頭象，也就是因為影像不太清晰，愛斯基摩頭像更甚「相似」（略帶想像空間）。2000年[哈伯太空望遠鏡對這個星雲作高解像照相觀測](../Page/哈伯太空望遠鏡.md "wikilink")，呈現人們尚未能充分了解的複雜氣體結構。

星雲前身是一顆類似[太陽質量的恆星](../Page/太陽.md "wikilink")，約在10,000年前，氣體才在它的外圍開始籠罩。其內部的細絲是由中心恆星吹出的[星風中的微粒组成的](../Page/星風.md "wikilink")，外殼中有異常的長達一光年的橙色絲線。

## 参考文献

<references/>

## 參見

  - [行星狀星雲列表](../Page/行星狀星雲列表.md "wikilink")
  - [NGC天體列表](../Page/NGC天體列表.md "wikilink")

## 外部連結

  - 哈勃望遠鏡2000年1月的[拍攝資料](http://hubblesite.org/newscenter/newsdesk/archive/releases/2000/07/image/a)
  - 美國國家光學天文台20吋望遠鏡[拍攝之愛斯基摩星雲](http://www.noao.edu/outreach/aop/observers/n2392.html)

[Category:双子座NGC天体](../Category/双子座NGC天体.md "wikilink")
[Category:行星狀星雲](../Category/行星狀星雲.md "wikilink")
[Category:雙子座](../Category/雙子座.md "wikilink")
[039](../Category/科德韦尔天体.md "wikilink")
[Category:1787年發現的天體](../Category/1787年發現的天體.md "wikilink")

1.  <http://www.windows.ucar.edu/the_universe/images/n2392_image.html>