[Tung_To_Court_201407.jpg](https://zh.wikipedia.org/wiki/File:Tung_To_Court_201407.jpg "fig:Tung_To_Court_201407.jpg")
[Oi_Tung_Estate_Shopping_Arcade_Void_201407.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Shopping_Arcade_Void_201407.jpg "fig:Oi_Tung_Estate_Shopping_Arcade_Void_201407.jpg")
[Oi_Tung_Estate_Plaza_201407.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Plaza_201407.jpg "fig:Oi_Tung_Estate_Plaza_201407.jpg")
[Oi_Tung_Estate_Table_Tennis_Play_Area.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Table_Tennis_Play_Area.jpg "fig:Oi_Tung_Estate_Table_Tennis_Play_Area.jpg")
[Oi_Tung_Estate_Slides,_Pattern_Bingo_and_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Slides,_Pattern_Bingo_and_Covered_Walkway.jpg "fig:Oi_Tung_Estate_Slides,_Pattern_Bingo_and_Covered_Walkway.jpg")
[Oi_Tung_Estate_Spring_Riders_and_Fitness_Area.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Spring_Riders_and_Fitness_Area.jpg "fig:Oi_Tung_Estate_Spring_Riders_and_Fitness_Area.jpg")
[Oi_Tung_Estate_Chess_Area.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Chess_Area.jpg "fig:Oi_Tung_Estate_Chess_Area.jpg")
[Oi_Tung_Estate_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Oi_Tung_Estate_Pebble_Walking_Trail.jpg "fig:Oi_Tung_Estate_Pebble_Walking_Trail.jpg")
**愛東邨**（英語：**Oi Tung
Estate**）是[香港的](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，項目編號為HK11NR，位於[香港島](../Page/香港島.md "wikilink")[筲箕灣](../Page/筲箕灣.md "wikilink")[愛秩序灣填海區範圍內](../Page/愛秩序灣.md "wikilink")，於2000年開始分3個階段入伙，其中愛澤樓、愛旭樓及愛平樓和「愛東商場」由興業建築師有限公司設計，愛寶樓（東濤苑E座）由周古梁建築工程師有限公司設計，愛逸樓由房屋署總建築師（2）設計，現由卓安顧問有限公司負責屋邨管理。

## 屋邨資料

### 樓宇

<table>
<thead>
<tr class="header">
<th><p>樓宇名稱 （座號）</p></th>
<th><p>樓宇類型</p></th>
<th><p>落成年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>愛澤樓 （第1座）</p></td>
<td><p><a href="../Page/和諧一型.md" title="wikilink">和諧一型</a></p></td>
<td><p>2000</p></td>
</tr>
<tr class="even">
<td><p>愛平樓 （第2座）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>愛旭樓 （第3座）</p></td>
<td><p>和諧一型連新和諧附翼第三型及第四型</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>愛善樓 （第9座）</p></td>
<td><p>小型單位大廈（<a href="../Page/和諧式大廈.md" title="wikilink">和諧式設計</a>）</p></td>
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td><p>愛寶樓 （第5座）<br />
（原屬<a href="../Page/東濤苑.md" title="wikilink">東濤苑</a>）</p></td>
<td><p>2005</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>愛逸樓（第10座）</p></td>
<td><p><a href="../Page/非標準設計大廈.md" title="wikilink">非標準設計大廈</a><br />
（十字型）</p></td>
<td><p>2008</p></td>
</tr>
</tbody>
</table>

（註：因發展計劃更改關係，部分座號被略去。 座號以房屋署Estate Property 的紀錄為依歸。）

## 設施

### 商場、街市與社區服務

  - [愛東商場](../Page/愛東商場.md "wikilink")（愛賢街18號）
  - [愛秩序灣街市](../Page/愛秩序灣街市.md "wikilink")
  - [愛秩序灣綜合服務大樓](../Page/愛秩序灣綜合服務大樓.md "wikilink")
  - [愛秩序灣社區會堂](../Page/愛秩序灣社區會堂.md "wikilink")（愛寶樓地下）

### 教育及福利

### 幼稚園

  - [香港東區婦女福利會幼兒園](http://www.ednursery.edu.hk/home.php)（1956年創辦）（位於愛平樓地下）
  - [筲箕灣循道衞理幼稚園](http://www.skwmk.edu.hk)（2001年創辦）（位於愛旭樓地下）

### 家長資源中心

  - [協康會賽馬會家長資源中心](https://www.heephong.org/center/detail/jockey-club-parents-resource-centre)（位於愛善樓地下G1室）

### 小學

  - [中華基督教會基灣小學（愛蝶灣）](../Page/中華基督教會基灣小學（愛蝶灣）.md "wikilink")（1970年創辦）

### 中學

  - [聖馬可中學](../Page/聖馬可中學.md "wikilink")（1949年創辦）

Oi Tung Shopping Centre (sky blue version).JPG|愛東商場外貌 Oi Tung Estate Car
Park.jpg|停車場

## 鄰近建築

  - [居屋屋苑](../Page/居者有其屋計劃.md "wikilink")：[愛蝶灣](../Page/愛蝶灣.md "wikilink")、[東旭苑](../Page/東旭苑.md "wikilink")、[東濤苑](../Page/東濤苑.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{港島綫色彩}}">█</font><a href="../Page/港島綫.md" title="wikilink">港島綫</a>：<a href="../Page/筲箕灣站.md" title="wikilink">筲箕灣站</a></li>
</ul>
<dl>
<dt><a href="../Page/愛禮街.md" title="wikilink">愛禮街</a></dt>

</dl>
<dl>
<dt><a href="../Page/愛秩序灣道.md" title="wikilink">愛秩序灣道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>筲箕灣至<a href="../Page/旺角.md" title="wikilink">旺角線</a> (晚上服務)[1]</li>
<li>筲箕灣至<a href="../Page/西環.md" title="wikilink">西環線</a>[2]</li>
<li>筲箕灣至<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣線</a> (晚上服務)[3]</li>
<li><a href="../Page/柴灣.md" title="wikilink">柴灣至</a><a href="../Page/觀塘.md" title="wikilink">觀塘線</a>[4]</li>
<li>柴灣至<a href="../Page/九龍灣.md" title="wikilink">九龍灣線</a> (上午及下午繁時服務)[5]</li>
</ul>
<dl>
<dt><a href="../Page/愛賢街.md" title="wikilink">愛賢街</a></dt>

</dl>
<dl>
<dt><a href="../Page/愛勤道.md" title="wikilink">愛勤道</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 著名住客

已故[藝人](../Page/藝人.md "wikilink")[藍潔瑛](../Page/藍潔瑛.md "wikilink")，曾居於本邨愛澤樓一單位，後遷往[赤柱](../Page/赤柱.md "wikilink")[馬坑邨](../Page/馬坑邨.md "wikilink")。

## 資料來源

## 外部連結

  - [香港房屋委員會：愛東邨簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2850)

[en:Public housing estates in Shau Kei
Wan](../Page/en:Public_housing_estates_in_Shau_Kei_Wan.md "wikilink")

[Category:筲箕灣](../Category/筲箕灣.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  [筲箕灣站　—　旺角砵蘭街](http://www.16seats.net/chi/rmb/r_kh02.html)
2.  [西環　—　筲箕灣／柴灣](http://www.16seats.net/chi/rmb/r_h12.html)
3.  [筲箕灣站　—　土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kh06.html)
4.  [觀塘宜安街　—　柴灣站](http://www.16seats.net/chi/rmb/r_kh08.html)
5.  [九龍灣　—　柴灣站](http://www.16seats.net/chi/rmb/r_kh07.html)