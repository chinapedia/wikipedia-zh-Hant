[Diplomatic_missions_of_Ireland.png](https://zh.wikipedia.org/wiki/File:Diplomatic_missions_of_Ireland.png "fig:Diplomatic_missions_of_Ireland.png")

[愛爾蘭擁有](../Page/愛爾蘭共和國.md "wikilink")161個外國政府的外交關係。愛爾蘭政府共有74個特派團在世界各地，包括55個[大使館](../Page/大使館.md "wikilink")，八個和8個[總領事館和其他](../Page/總領事館.md "wikilink")[辦事處](../Page/辦事處.md "wikilink")。該國還任命了24個名譽[總領事](../Page/總領事.md "wikilink")，名譽領事62個。
IDA，愛爾蘭的投資和貿易促進機構，也有自己的據點。

下面這個名單是源自愛爾蘭外交部網站，該網站於2009年1月的最後更新。

## 歐洲

[Erottajankatu_7_Helsinki.JPG](https://zh.wikipedia.org/wiki/File:Erottajankatu_7_Helsinki.JPG "fig:Erottajankatu_7_Helsinki.JPG")
[Moscow,_Grokholsky_5,_embassy_of_Ireland.JPG](https://zh.wikipedia.org/wiki/File:Moscow,_Grokholsky_5,_embassy_of_Ireland.JPG "fig:Moscow,_Grokholsky_5,_embassy_of_Ireland.JPG")
[Mala_Strana_Trziste_13_2844.JPG](https://zh.wikipedia.org/wiki/File:Mala_Strana_Trziste_13_2844.JPG "fig:Mala_Strana_Trziste_13_2844.JPG")
[Irish_Embassy_Riga_Latvia.jpg](https://zh.wikipedia.org/wiki/File:Irish_Embassy_Riga_Latvia.jpg "fig:Irish_Embassy_Riga_Latvia.jpg")
[IrelandEmbassyRome.jpg](https://zh.wikipedia.org/wiki/File:IrelandEmbassyRome.jpg "fig:IrelandEmbassyRome.jpg")
[Embassy_of_Ireland,_Sofia.jpg](https://zh.wikipedia.org/wiki/File:Embassy_of_Ireland,_Sofia.jpg "fig:Embassy_of_Ireland,_Sofia.jpg")
[Vilnius_tentement_house.jpg](https://zh.wikipedia.org/wiki/File:Vilnius_tentement_house.jpg "fig:Vilnius_tentement_house.jpg")
[Embassy_of_Ireland_in_Washington_DC.jpg](https://zh.wikipedia.org/wiki/File:Embassy_of_Ireland_in_Washington_DC.jpg "fig:Embassy_of_Ireland_in_Washington_DC.jpg")

  -   - [維也納](../Page/維也納.md "wikilink")（大使館）

  -   - [布魯塞爾](../Page/布魯塞爾.md "wikilink")（大使館）

  -   - [索非亞](../Page/索非亞.md "wikilink")（大使館）

  -   - [尼古西亞](../Page/尼古西亞.md "wikilink")（大使館）

  -   - [布拉格](../Page/布拉格.md "wikilink")（大使館）

  -   - [哥本哈根](../Page/哥本哈根.md "wikilink")（大使館）

  -   - [塔林](../Page/塔林.md "wikilink")（大使館）

  -   - [赫爾辛基](../Page/赫爾辛基.md "wikilink")（大使館）

  -   - [巴黎](../Page/巴黎.md "wikilink")（大使館）

  -   - [柏林](../Page/柏林.md "wikilink")（大使館）

  -   - [雅典](../Page/雅典.md "wikilink")（大使館）

  -   - [梵蒂岡城](../Page/梵蒂岡城.md "wikilink")（大使館）\[1\]

  -   - [布達佩斯](../Page/布達佩斯.md "wikilink")（大使館）

  -   - [羅馬](../Page/羅馬.md "wikilink")（大使館）

  -   - [里加](../Page/里加.md "wikilink")（大使館）

  -   - [維爾紐斯](../Page/維爾紐斯.md "wikilink")（大使館）

  -   - [盧森堡](../Page/盧森堡市.md "wikilink")（大使館）

  -   - [瓦萊塔](../Page/瓦萊塔.md "wikilink")（大使館）

  -   - [海牙](../Page/海牙.md "wikilink")（大使館）

  -   - [奧斯陸](../Page/奧斯陸.md "wikilink")（大使館）

  -   - [華沙](../Page/華沙.md "wikilink")（大使館）

  -   - [里斯本](../Page/里斯本.md "wikilink")（大使館）

  -   - [布加勒斯特](../Page/布加勒斯特.md "wikilink")（大使館）

  -   - [莫斯科](../Page/莫斯科.md "wikilink")（大使館）

  -   - [布拉迪斯拉發](../Page/布拉迪斯拉發.md "wikilink")（大使館）

  -   - [盧布爾雅那](../Page/盧布爾雅那.md "wikilink")（大使館）

  -   - [馬德里](../Page/馬德里.md "wikilink")（大使館）

  -   - [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")（大使館）

  -   - [伯恩](../Page/伯恩.md "wikilink")（大使館）

  -   - [倫敦](../Page/倫敦.md "wikilink")（大使館）
      - [愛丁堡](../Page/愛丁堡.md "wikilink") （總領事館）

## 美洲

  -   - [布宜諾斯艾利斯](../Page/布宜諾斯艾利斯.md "wikilink")（大使館）

  -   - [巴西利亞](../Page/巴西利亞.md "wikilink")（大使館）

  -   - [渥太華](../Page/渥太華.md "wikilink") （大使館）

  -   - [墨西哥城](../Page/墨西哥城.md "wikilink") （大使館）

  -   - [華盛頓特區](../Page/華盛頓特區.md "wikilink") （大使館）
      - [亞特蘭大](../Page/亞特蘭大.md "wikilink")（總領事館）
      - [波士頓](../Page/波士頓.md "wikilink")（總領事館）
      - [芝加哥](../Page/芝加哥.md "wikilink")（總領事館）
      - [紐約](../Page/紐約.md "wikilink")（總領事館）
      - [舊金山](../Page/舊金山.md "wikilink")（總領事館）

## 亞洲

[Irish_Embassy_in_Beijing.JPG](https://zh.wikipedia.org/wiki/File:Irish_Embassy_in_Beijing.JPG "fig:Irish_Embassy_in_Beijing.JPG")

  -   - [北京](../Page/北京.md "wikilink")（大使館）
      - [上海](../Page/上海.md "wikilink")（總領事館）
      - [香港](../Page/香港.md "wikilink")（總領事館）

  -   - [新德里](../Page/新德里.md "wikilink")（大使館）

  -   - [德黑蘭](../Page/德黑蘭.md "wikilink")（大使館）

  -   - [特拉維夫](../Page/特拉維夫.md "wikilink")（大使館）

  -   - [東京](../Page/東京.md "wikilink")（大使館）

  -   - [首爾](../Page/首爾.md "wikilink")（大使館）

  -   - [吉隆坡](../Page/吉隆坡.md "wikilink")（大使館）

  -   - [拉姆安拉](../Page/拉姆安拉.md "wikilink")（代表處）

  -   - [利雅得](../Page/利雅得.md "wikilink")（大使館）

  -   - [新加坡](../Page/新加坡.md "wikilink")（大使館）

  -   - [帝力](../Page/帝力.md "wikilink")（代表處）（關閉）\[2\]

  -   - [安卡拉](../Page/安卡拉.md "wikilink")（大使館）

  -   - [阿布達比](../Page/阿布達比.md "wikilink")（大使館）

  -   - [河內](../Page/河內.md "wikilink")（大使館）

### 代表處

  -   - [台北](../Page/台北市.md "wikilink")（愛爾蘭投資貿易促進會）

## 非洲

  -   - [開羅](../Page/開羅.md "wikilink")（大使館）

  -   - [阿迪斯阿貝巴](../Page/阿迪斯阿貝巴.md "wikilink")（大使館）

  -   - [馬塞盧](../Page/馬塞盧.md "wikilink")（大使館）

  -   - [里郎威](../Page/里郎威.md "wikilink")（大使館）

  -   - [馬普托](../Page/馬普托.md "wikilink")（大使館）

  -   - [阿布加](../Page/阿布加.md "wikilink")（大使館）

  -   - [普勒托利亞](../Page/普勒托利亞.md "wikilink")（大使館）
      - [開普敦](../Page/開普敦.md "wikilink")（總領事館）

  -   - [三蘭港](../Page/三蘭港.md "wikilink")（大使館）

  -   - [坎帕拉](../Page/坎帕拉.md "wikilink")（大使館）

  -   - [盧薩卡](../Page/盧薩卡.md "wikilink")（大使館）

## 大洋洲

[Irish_Embassy_in_Canberra.jpg](https://zh.wikipedia.org/wiki/File:Irish_Embassy_in_Canberra.jpg "fig:Irish_Embassy_in_Canberra.jpg")

  - [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg")
    [澳大利亞](../Page/澳大利亞.md "wikilink")
      - [坎培拉](../Page/坎培拉.md "wikilink") （大使館）
      - [雪梨](../Page/雪梨_\(城市\).md "wikilink") （總領事館）

## 國際組織

  -   - [布魯塞爾](../Page/布魯塞爾.md "wikilink")（歐盟常駐聯合國代表團）
      - [日內瓦](../Page/日內瓦.md "wikilink")（聯合國代表團和其他國際組織）
      - [紐約](../Page/紐約.md "wikilink")（聯合國代表團）
      - [巴黎](../Page/巴黎.md "wikilink")（教科文組織）
      - [羅馬](../Page/羅馬.md "wikilink")（糧食及農業組織）
      - [斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")（歐洲理事會常駐聯合國代表團）

## 其他條目

  - [愛爾蘭外交](../Page/愛爾蘭外交.md "wikilink")

## 參考文獻

## 外部連結

  - [愛爾蘭駐中國大使館](https://web.archive.org/web/20070304171119/http://embassyofireland.cn/Ireland-Chinese/index.htm)
  - [愛爾蘭駐日本大使館](https://web.archive.org/web/20070228124304/http://www.irishembassy.jp/index_jap.html)
  - [愛爾蘭常駐聯合國代表團](https://web.archive.org/web/20070205041201/http://www.un.int/ireland/)
  - [Department of Foreign
    Affairs](http://www.dfa.ie/home/index.aspx?id=369)

[愛](../Category/各国驻外机构列表.md "wikilink")
[Category:爱尔兰驻外机构](../Category/爱尔兰驻外机构.md "wikilink")

1.

2.