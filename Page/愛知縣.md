[Mikawa_Bay_Aichi_Japan_SRTM.jpg](https://zh.wikipedia.org/wiki/File:Mikawa_Bay_Aichi_Japan_SRTM.jpg "fig:Mikawa_Bay_Aichi_Japan_SRTM.jpg")
**愛知縣**（）是位於[日本](../Page/日本.md "wikilink")[中部地方的](../Page/中部地方.md "wikilink")[縣](../Page/日本行政區劃.md "wikilink")，[首府與第一大城為](../Page/都道府縣廳所在地.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")。

## 歷史

[090408_aichi_kenchou.jpg](https://zh.wikipedia.org/wiki/File:090408_aichi_kenchou.jpg "fig:090408_aichi_kenchou.jpg")
愛知縣在歷史上，是**[尾張國](../Page/尾張國.md "wikilink")**、**[三河國](../Page/三河國.md "wikilink")**這兩個[令制國的轄區](../Page/令制國.md "wikilink")。1871年，[明治新政府實行](../Page/明治新政府.md "wikilink")[廢藩置縣](../Page/廢藩置縣.md "wikilink")，除[知多郡外的尾張成為](../Page/知多郡.md "wikilink")****，而三河則與知多合併成****。名古屋縣於1872年4月易名為**愛知縣**，之後在同年11月27日與額田縣合併成為今天的愛知縣。

縣名是由曾管轄名古屋[市中心的](../Page/市中心.md "wikilink")[愛知郡而來](../Page/愛知郡.md "wikilink")。爱知县和名古屋市获得[2026年亚运会举办权](../Page/2026年亚运会.md "wikilink")。

## 地理

愛知縣位處[日本列島中央](../Page/日本列島.md "wikilink")，向南可經[三河灣及](../Page/三河灣.md "wikilink")[伊勢灣通往](../Page/伊勢灣.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")。東面與[靜岡縣接鄰](../Page/靜岡縣.md "wikilink")；東北面為[長野縣](../Page/長野縣.md "wikilink")；北面為[岐阜縣](../Page/岐阜縣.md "wikilink")；西面則為[三重縣](../Page/三重縣.md "wikilink")。縣的東西端相距106[公里](../Page/公里.md "wikilink")，南北端相距94公里；全縣總面積為5153.81[平方公里](../Page/平方公里.md "wikilink")，約佔全日本面積1.36％。最高點為海拔1415公尺的[茶臼山](../Page/茶臼山.md "wikilink")。

## 行政區劃

轄下共38[市](../Page/市.md "wikilink")7[郡](../Page/郡.md "wikilink")14[町
(行政區劃)](../Page/町_\(行政區劃\).md "wikilink")2[村](../Page/村.md "wikilink")。

### 尾張地方

  - [名古屋市](../Page/名古屋市.md "wikilink")：[千種區](../Page/千種區.md "wikilink")、[東區](../Page/東區_\(名古屋市\).md "wikilink")、[北區](../Page/北區_\(名古屋市\).md "wikilink")、[西區](../Page/西區_\(名古屋市\).md "wikilink")、[中村區](../Page/中村區.md "wikilink")、[中區](../Page/中區_\(名古屋市\).md "wikilink")、[昭和區](../Page/昭和區.md "wikilink")、[瑞穗區](../Page/瑞穗區.md "wikilink")、[熱田區](../Page/熱田區.md "wikilink")、[中川區](../Page/中川區.md "wikilink")、[港區](../Page/港區_\(名古屋市\).md "wikilink")、[南區](../Page/南區_\(名古屋市\).md "wikilink")、[守山區](../Page/守山區.md "wikilink")、[綠區](../Page/綠區_\(名古屋市\).md "wikilink")、[名東區](../Page/名東區.md "wikilink")、[天白區](../Page/天白區.md "wikilink")
  - [愛西市](../Page/愛西市.md "wikilink")、[海部市](../Page/海部市.md "wikilink")、[一宮市](../Page/一宮市.md "wikilink")、[稻澤市](../Page/稻澤市.md "wikilink")、[犬山市](../Page/犬山市.md "wikilink")、[岩倉市](../Page/岩倉市.md "wikilink")、[大府市](../Page/大府市.md "wikilink")、[尾張旭市](../Page/尾張旭市.md "wikilink")、[春日井市](../Page/春日井市.md "wikilink")、[清須市](../Page/清須市.md "wikilink")、[江南市](../Page/江南市.md "wikilink")、[小牧市](../Page/小牧市.md "wikilink")、[瀨戶市](../Page/瀨戶市.md "wikilink")、[知多市](../Page/知多市.md "wikilink")、[津島市](../Page/津島市.md "wikilink")、[東海市](../Page/東海市_\(日本\).md "wikilink")、[常滑市](../Page/常滑市.md "wikilink")、[豐明市](../Page/豐明市.md "wikilink")、[日進市](../Page/日進市.md "wikilink")、[半田市](../Page/半田市.md "wikilink")、[北名古屋市](../Page/北名古屋市.md "wikilink")、[彌富市](../Page/彌富市.md "wikilink")、[長久手市](../Page/長久手市.md "wikilink")
  - [愛知郡](../Page/愛知郡_\(愛知縣\).md "wikilink")：[東鄉町](../Page/東鄉町.md "wikilink")
  - [海部郡](../Page/海部郡_\(愛知縣\).md "wikilink")：[大治町](../Page/大治町.md "wikilink")、[蟹江町](../Page/蟹江町.md "wikilink")、[飛島村](../Page/飛島村.md "wikilink")
  - [西春日井郡](../Page/西春日井郡.md "wikilink")：[豐山町](../Page/豐山町.md "wikilink")
  - [丹羽郡](../Page/丹羽郡.md "wikilink")：[大口町](../Page/大口町.md "wikilink")、[扶桑町](../Page/扶桑町.md "wikilink")
  - [知多郡](../Page/知多郡.md "wikilink")：[阿久比町](../Page/阿久比町.md "wikilink")、[武豐町](../Page/武豐町.md "wikilink")、[東浦町](../Page/東浦町.md "wikilink")、[南知多町](../Page/南知多町.md "wikilink")、[美濱町](../Page/美濱町_\(愛知縣\).md "wikilink")

### 西三河地方

  - [安城市](../Page/安城市_\(日本\).md "wikilink")、[岡崎市](../Page/岡崎市.md "wikilink")、[刈谷市](../Page/刈谷市.md "wikilink")、[高濱市](../Page/高濱市.md "wikilink")、[知立市](../Page/知立市.md "wikilink")、[豐田市](../Page/豐田市.md "wikilink")、[西尾市](../Page/西尾市.md "wikilink")、[碧南市](../Page/碧南市.md "wikilink")、[三好市](../Page/三好市_\(愛知縣\).md "wikilink")
  - [額田郡](../Page/額田郡.md "wikilink")：[幸田町](../Page/幸田町.md "wikilink")

### 東三河地方

  - [豐橋市](../Page/豐橋市.md "wikilink")、[豐川市](../Page/豐川市.md "wikilink")、[蒲郡市](../Page/蒲郡市.md "wikilink")、[田原市](../Page/田原市.md "wikilink")、[新城市](../Page/新城市.md "wikilink")
  - [北設樂郡](../Page/北設樂郡.md "wikilink")：[設樂町](../Page/設樂町.md "wikilink")、[東榮町](../Page/東榮町.md "wikilink")、[豐根村](../Page/豐根村.md "wikilink")

## 經濟

愛知縣是日本的工業大縣，是中京工業地帯的中心。愛知縣的工業產值在2013年達約42兆18億日元，連續37年排名日本第一，是第二位的神奈川縣的2倍以上。愛知縣有以[豐田汽車為首的眾多](../Page/豐田汽車.md "wikilink")[汽車企業](../Page/汽車.md "wikilink")。豐田汽車在1937年設立。在[高度經濟成長期之後](../Page/高度經濟成長.md "wikilink")，愛知縣的[汽車產業發展成為高度發達的產業](../Page/汽車產業.md "wikilink")。

### 產業

縣內總生產（2004年）有34兆6503億日圓，佔日本國內生產總值的7.0%。

  - [汽車工業](../Page/汽車工業.md "wikilink")
  - [飛機生產](../Page/飛機生產.md "wikilink")

### 經濟界

[Expo_2005_Flaggs_and_Corporate_Pavillion_Zone.jpg](https://zh.wikipedia.org/wiki/File:Expo_2005_Flaggs_and_Corporate_Pavillion_Zone.jpg "fig:Expo_2005_Flaggs_and_Corporate_Pavillion_Zone.jpg")\]\]

#### 過去的「五攝家」

**[五攝家](../Page/五攝家_\(愛知縣\).md "wikilink")**是以愛知縣名古屋市為根據地的中部圈財界的一群名門企業，包括有旧[伊藤財閥系企業的](../Page/伊藤財閥.md "wikilink")[東海銀行及](../Page/東海銀行.md "wikilink")[松坂屋](../Page/松坂屋.md "wikilink")、基建企業的[名古屋鐵道](../Page/名古屋鐵道.md "wikilink")、[中部電力及](../Page/中部電力.md "wikilink")[東邦瓦斯五家商社所構成](../Page/東邦瓦斯.md "wikilink")。曾經獨佔[中部經濟聯合會](../Page/中部經濟聯合會.md "wikilink")（簡稱「中經聯」）、[名古屋商工會議所](../Page/名古屋商工會議所.md "wikilink")（名商）等經濟團體的代表，[名古屋觀光酒店及](../Page/名古屋觀光酒店.md "wikilink")[名古屋波士頓美術館等主導負責的企業](../Page/名古屋波士頓美術館.md "wikilink")。

## 人口

愛知縣的人口在所有[都道府縣中排行第四](../Page/都道府縣.md "wikilink")，計有7,046,000人（2009年5月1日）。在縣內[市町村中](../Page/市町村.md "wikilink")，名古屋市人口最多，有2,090,549人；人口最少為[富山村](../Page/富山村_\(愛知縣\).md "wikilink")，只有512人。

## 交通

過去愛知的交通主要依靠火車連接。但自從[中部国际机场開放以後](../Page/中部国际机场.md "wikilink")，對外交通就大大改善。乘搭火車從[名铁名古屋站出發](../Page/名铁名古屋站.md "wikilink")，只要30分鐘就可到達中部国际机场。

## 文化

  - [2005年日本國際博覽會](../Page/2005年日本國際博覽會.md "wikilink")（EXPO
    2005）於2005年3月25日到9月25日在[瀨戶市和](../Page/瀨戶市.md "wikilink")[長久手町舉行](../Page/長久手町.md "wikilink")。

## 旅遊

  - [名古屋城](../Page/名古屋城.md "wikilink")
  - [犬山城](../Page/犬山城.md "wikilink")
  - [岡崎城](../Page/岡崎城.md "wikilink")
  - [豐田博物館](../Page/豐田博物館.md "wikilink")
  - [博物館明治村](../Page/博物館明治村.md "wikilink")
  - [熱田神宮](../Page/熱田神宮.md "wikilink")
  - [德川美術館](../Page/德川美術館.md "wikilink")
  - [日本樂高樂園](../Page/日本樂高樂園.md "wikilink")

[Ajisainosato1.jpg](https://zh.wikipedia.org/wiki/File:Ajisainosato1.jpg "fig:Ajisainosato1.jpg")

<File:2016> Japan Nagoya 81
(33038233234).jpg|[綠洲21](../Page/:jp:オアシス21.md "wikilink")（[名古屋市](../Page/名古屋市.md "wikilink")[中區](../Page/中區_\(名古屋市\).md "wikilink")）
<File:(with> Sakura) Nagoya Castle Keep
Tower.JPG|[名古屋城](../Page/名古屋城.md "wikilink")（名古屋市中區）
[File:Jorakuden1.jpg|本丸御殿](File:Jorakuden1.jpg%7C本丸御殿) (名古屋城)
[File:Osukannon.jpg|大須観音（名古屋市中區](File:Osukannon.jpg%7C大須観音（名古屋市中區)）
[File:Tokugawabijutsukan1.JPG|德川美術館（名古屋市](File:Tokugawabijutsukan1.JPG%7C德川美術館（名古屋市)[東區](../Page/東區_\(名古屋市\).md "wikilink")）
[File:Toyotasangyoukinen8.JPG|豐田產業技術紀念館（名古屋市](File:Toyotasangyoukinen8.JPG%7C豐田產業技術紀念館（名古屋市)[西區](../Page/西區_\(名古屋市\).md "wikilink")）
<File:Noritake> garden2.jpg|Noritake的森林（名古屋市西區） <File:Legoland>
japan.jpg|[日本樂高樂園](../Page/日本樂高樂園.md "wikilink")（名古屋市[港區](../Page/港區_\(名古屋市\).md "wikilink")）
<File:Port> of Nagoya Public Aquarium1.jpg|名古屋港水族館（名古屋市港區） <File:JR>
MLX01-1 001.jpg|[磁浮·鐵道館](../Page/磁浮·鐵道館.md "wikilink")（名古屋市港區）
<File:Atsuta-jinguu>
keidai.JPG|[熱田神宮](../Page/熱田神宮.md "wikilink")（名古屋市[熱田區](../Page/熱田區.md "wikilink")）
[File:Inuyamamatsuri1.jpg|犬山市](File:Inuyamamatsuri1.jpg%7C犬山市)
[File:Komakiyama.JPG|小牧市](File:Komakiyama.JPG%7C小牧市) <File:Kamagaki> no
Komichi06.jpg|瀬戸市
[File:Kiyosu-jo\&ote-bashi.jpg|清須市](File:Kiyosu-jo&ote-bashi.jpg%7C清須市)
[File:Tsushimatennosai1.JPG|津島市](File:Tsushimatennosai1.JPG%7C津島市)
[File:Handaunga.JPG|半田市](File:Handaunga.JPG%7C半田市)
[File:Yakimonosanpomichi1.JPG|常滑市](File:Yakimonosanpomichi1.JPG%7C常滑市)
<File:Mikawa> Isshiki Lantern Festival.jpg|西尾市
[File:Atsumihantou.JPG|田原市](File:Atsumihantou.JPG%7C田原市)
[File:Taharananohana.jpg|油菜花(田原市)](File:Taharananohana.jpg%7C油菜花\(田原市\))
[File:Nagashinofestival.JPG|新城市](File:Nagashinofestival.JPG%7C新城市)
<File:Inuyama> Castle and Kiso
River.JPG|[犬山城](../Page/犬山城.md "wikilink") <File:Aichi>
museum of flight2.jpg|愛知航空博物館 <File:FLIGHT> OF DREAMS4.jpg|FLIGHT OF
DREAMS <File:TOYOTA> AUTOMOBILE MUSEUM.JPG|豐田博物館
[File:Meijimurabunkazai15.JPG|博物館明治村](File:Meijimurabunkazai15.JPG%7C博物館明治村)
<File:Little> world1.JPG|野外民族博物館小小世界
<File:Okazakijo2.JPG>|[岡崎城](../Page/岡崎城.md "wikilink")
[File:Ragunatenbos1.JPG|拉格娜蒲郡](File:Ragunatenbos1.JPG%7C拉格娜蒲郡)
[File:Himakajima8.JPG|日間賀島](File:Himakajima8.JPG%7C日間賀島)
[File:Shinojima1.jpg|筱岛](File:Shinojima1.jpg%7C筱岛)
[File:Sakushima5.jpg|佐久島](File:Sakushima5.jpg%7C佐久島)
[File:Kankonouen.JPG|观光农场花广场](File:Kankonouen.JPG%7C观光农场花广场)
[File:Horaijisan1.jpg|鳳来寺山](File:Horaijisan1.jpg%7C鳳来寺山)
<File:Mt.Tyausu> Moss phlox.jpg|茶臼山
[File:Houraisantoushouguu1.JPG|鳳来寺山東照宮](File:Houraisantoushouguu1.JPG%7C鳳来寺山東照宮)
<File:Toyokawainari1.JPG>|[豐川稻荷](../Page/豐川稻荷.md "wikilink")
<File:Morikoro> Park.JPG|爱・地球博纪念公园
[File:Matsudairago1.jpg|松平郷](File:Matsudairago1.jpg%7C松平郷)
[File:Kisogawazutsumi.JPG|木曾川堤](File:Kisogawazutsumi.JPG%7C木曾川堤)
[File:138towerpark.JPG|雙子塔138](File:138towerpark.JPG%7C雙子塔138)
<File:Sakurabuchi> park2.JPG|櫻淵公園 <File:Kourankei> was illuminated
香嵐渓のライトアップ - panoramio.jpg|香嵐渓 <File:Noma>
lighthouse.jpg|野間埼燈塔 <File:Irago> view hotel.jpg|伊良湖岬

## 主要节日活动

  - 尾张津岛天王节（UNESCO的[非物质文化遗产](../Page/非物质文化遗产.md "wikilink")）
  - 犬山节（UNESCO的非物质文化遗产）
  - 亀崎潮干节（UNESCO的非物质文化遗产）
  - 知立节（UNESCO的非物质文化遗产）
  - 須成节（UNESCO的非物质文化遗产）
  - 名古屋节
  - 筒井町出来町天王节
  - 大田节
  - 三谷节
  - 鯛魚节
  - 乙川节
  - 万灯节
  - 一色大灯笼节
  - 鳥羽的火祭
  - 尾张津岛秋季节

[File:Tsushimatennosai1.JPG|尾张津岛天王节](File:Tsushimatennosai1.JPG%7C尾张津岛天王节)
<File:Inuyama> Festival.jpg|犬山节 <File:Kamezakishiohi>
Festival2.jpg|亀崎潮干节
[File:Tiryuumatsuri7.JPG|知立节](File:Tiryuumatsuri7.JPG%7C知立节)
[File:Sunarimatsuri1.JPG|須成节](File:Sunarimatsuri1.JPG%7C須成节)
[File:Nagoyamatsuri7.JPG|名古屋节](File:Nagoyamatsuri7.JPG%7C名古屋节)
[File:Tokugawaendashizoroe1.JPG|筒井町出来町天王节](File:Tokugawaendashizoroe1.JPG%7C筒井町出来町天王节)
[File:Ootamatsuri.JPG|大田节](File:Ootamatsuri.JPG%7C大田节)
[File:Miyamatsuri.JPG|三谷节](File:Miyamatsuri.JPG%7C三谷节)
[File:Taimatsuri1.jpg|鯛魚节](File:Taimatsuri1.jpg%7C鯛魚节)
[File:Okkawamatsuri1.jpg|乙川节](File:Okkawamatsuri1.jpg%7C乙川节)
[File:Kariyamandomatsuri.jpg|万灯节](File:Kariyamandomatsuri.jpg%7C万灯节)
<File:Mikawa> Isshiki Lantern Festival.jpg|一色大灯笼节 <File:Toba> Fire
Festival3.jpg|鳥羽的火祭 <File:Owari> Tsushima autumn festival1.jpg|尾张津岛秋季节

## 特產

| 水果・蔬菜                                    |
| ---------------------------------------- |
| [甜瓜](../Page/甜瓜.md "wikilink")           |
| [葡萄](../Page/葡萄.md "wikilink")           |
| [抹茶](../Page/抹茶.md "wikilink")           |
| 魚介類・海產物                                  |
| [鰻](../Page/日本鰻鱺.md "wikilink")          |
| [花蛤](../Page/花蛤.md "wikilink")           |
| 肉類・乳製品                                   |
| [名古屋交趾雞](../Page/名古屋交趾雞.md "wikilink")   |
| [和牛](../Page/和牛.md "wikilink")           |
| 鄉土料理                                     |
| 味噌煮込みうどん                                 |
| [棊子麺](../Page/棊子麺.md "wikilink")         |
| 櫃まぶし                                     |
| [台灣拉麵](../Page/台灣拉麵.md "wikilink")       |
| 味增豬排                                     |
| [外郎](../Page/外郎.md "wikilink")           |
| 工藝品・民藝品                                  |
| [常滑燒](../Page/常滑燒.md "wikilink")         |
| [瀬戶染付燒](../Page/瀬戶染付燒.md "wikilink")     |
| [赤津燒](../Page/赤津燒.md "wikilink")         |
| [尾張七寶](../Page/尾張七寶.md "wikilink")       |
| 有松・鳴海絞                                   |
| [名古屋友禪](../Page/名古屋友禪.md "wikilink")     |
| [名古屋黒紋付染](../Page/名古屋黒紋付染.md "wikilink") |
| [豊橋筆](../Page/豊橋筆.md "wikilink")         |
| [名古屋佛壇](../Page/名古屋佛壇.md "wikilink")     |
| [三河佛壇](../Page/三河佛壇.md "wikilink")       |
| [岡崎石工品](../Page/岡崎石工品.md "wikilink")     |
| [名古屋桐箪笥](../Page/名古屋桐箪笥.md "wikilink")   |
| 參考資料：\[1\]                               |

## 出身名人

[鈴木一朗](../Page/鈴木一朗.md "wikilink")

[海部俊樹](../Page/海部俊樹.md "wikilink")

[加藤高明](../Page/加藤高明.md "wikilink")

[松井珠理奈](../Page/松井珠理奈.md "wikilink")

[木藤亞也](../Page/木藤亞也.md "wikilink")

[諸星大](../Page/諸星大.md "wikilink")

平手友梨奈

[櫻井孝宏](../Page/櫻井孝宏.md "wikilink")

## 媒體

### 電視台

|               |                                                                                   |
| ------------- | --------------------------------------------------------------------------------- |
| **頻道（遙控器號碼）** | **電視台**                                                                           |
| 1             | [東海電視台](../Page/東海電視台.md "wikilink")（[FNN系列](../Page/富士新聞網.md "wikilink")）        |
| 2             | [NHK E](../Page/NHK教育頻道.md "wikilink")                                            |
| 3             | [NHK G](../Page/NHK綜合頻道.md "wikilink")（[名古屋放送局](../Page/NHK名古屋放送局.md "wikilink")） |
| 4             | [中京電視台](../Page/中京電視台.md "wikilink")（[NNN系列](../Page/日視新聞網.md "wikilink")）        |
| 5             | [中部日本放送](../Page/中部日本放送.md "wikilink")（[JNN系列](../Page/日本新聞網.md "wikilink")）      |
| 6             | [名古屋電視台](../Page/名古屋電視台.md "wikilink")                                            |
| 7             | [三重電視台](../Page/三重電視台.md "wikilink")（獨立UHF，愛知縣東部平原、西部可接收到）                        |
| 10            | [愛知電視台](../Page/愛知電視台.md "wikilink")                                              |

## 註解

## 外部連結

  - [愛知縣官方網站](http://www.pref.aichi.jp/global/ch/index.html)
  - [愛知旅遊官方網站](https://www.aichi-now.jp/tw/)
  - [織田信長](https://web.archive.org/web/20131002175417/http://aichikanko.jp/App_Themes/IMG/SYSIMG/BOOK/nobunaga_zh-tw/#page=1)
  - [德川家康](https://web.archive.org/web/20131002175415/http://aichikanko.jp/App_Themes/IMG/SYSIMG/BOOK/ieyasu_zh-tw/#page=1)

[Category:日本都道府縣](../Category/日本都道府縣.md "wikilink")
[\*](../Category/愛知縣.md "wikilink")
[Category:亚洲运动会主办城市](../Category/亚洲运动会主办城市.md "wikilink")

1.