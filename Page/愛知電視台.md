**愛知電視台**（），簡稱「TVA」、「」，是以[愛知縣為放送地域的一家](../Page/愛知縣.md "wikilink")[日本](../Page/日本.md "wikilink")[電視台](../Page/電視台.md "wikilink")，成立於1982年12月1日，開播於1983年9月1日。該電視台是[東京電視台](../Page/東京電視台.md "wikilink")[聯播網](../Page/聯播網.md "wikilink")（[TXN](../Page/TXN.md "wikilink")）旗下的電視台。

## 外部連結

  - [愛知電視台](http://tv-aichi.co.jp/)

[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:愛知縣公司](../Category/愛知縣公司.md "wikilink")
[A](../Category/東京電視網.md "wikilink")