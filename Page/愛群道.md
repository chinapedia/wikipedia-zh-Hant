[HK_WC_Oi_Kwan_Road_SKH_Tang_Siu_Kin_School.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Oi_Kwan_Road_SKH_Tang_Siu_Kin_School.jpg "fig:HK_WC_Oi_Kwan_Road_SKH_Tang_Siu_Kin_School.jpg")
**愛群道**（）是[香港的一條街道](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[灣仔東部](../Page/灣仔.md "wikilink")，屬一條環形的街道。街道東接[崇德街往](../Page/崇德街.md "wikilink")[摩理臣山道](../Page/摩理臣山道.md "wikilink")，北接[德仁街往](../Page/德仁街.md "wikilink")[灣仔道](../Page/灣仔道.md "wikilink")。

## 特色

愛群道是[學校](../Page/學校.md "wikilink")、[醫院](../Page/醫院.md "wikilink")、[政府部門](../Page/政府.md "wikilink")、公共和商業[建築物為主](../Page/建築物.md "wikilink")，午飯人潮不少。

## 主要建築物

### 宗教

  - [愛群道浸信會](../Page/愛群道浸信會.md "wikilink")
  - [愛群清真寺](../Page/愛群清真寺.md "wikilink")

### 體育建築

  - [伊利沙伯體育館](../Page/伊利沙伯體育館.md "wikilink")
  - [摩理臣山游泳池](../Page/摩理臣山游泳池.md "wikilink")

### 學校

  - [愛群道浸信會呂郭碧鳳幼稚園](../Page/愛群道浸信會呂郭碧鳳幼稚園.md "wikilink")
  - [灣仔學校](../Page/灣仔學校.md "wikilink")
  - [聖公會鄧肇堅中學](../Page/聖公會鄧肇堅中學.md "wikilink")
  - [鄧肇堅維多利亞官立中學](../Page/鄧肇堅維多利亞官立中學.md "wikilink")
  - [香港專業教育學院摩理臣山分校](../Page/香港專業教育學院摩理臣山分校.md "wikilink")
  - [戴麟趾夫人訓練中心](../Page/戴麟趾夫人訓練中心.md "wikilink")

### 醫療設施

  - [麥理浩牙科](../Page/麥理浩牙科.md "wikilink")
  - [鄧肇堅醫院](../Page/鄧肇堅醫院.md "wikilink")

### 社會福利機構

  - [浸會愛群社會服務處](../Page/浸會愛群社會服務處.md "wikilink")(位於[鄧肇堅社會服務中心](../Page/鄧肇堅社會服務中心.md "wikilink"))

商業 愛群商業大廈（又名守護商業大廈） 住宅 愛群閣

## 鄰近街道

  - [灣仔道](../Page/灣仔道.md "wikilink")
  - [皇后大道東](../Page/皇后大道.md "wikilink")
  - [摩理臣山道](../Page/摩理臣山道.md "wikilink")
  - [崇德街](../Page/崇德街.md "wikilink")
  - [德仁街](../Page/德仁街.md "wikilink")
  - [日善街](../Page/日善街.md "wikilink")
  - [永祥街](../Page/永祥街.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[銅鑼灣站A出口](../Page/銅鑼灣站.md "wikilink")

## 外部連線

  - [灣仔愛群道地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=836507&cy=815281&zm=3&mx=836507&my=815281&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[en:Oi Kwan Road](../Page/en:Oi_Kwan_Road.md "wikilink")

[Category:灣仔街道](../Category/灣仔街道.md "wikilink")