《淚光閃閃》，又譯《[愛與淚相隨](../Page/愛與淚相隨.md "wikilink")》，是為[東京廣播公司](../Page/TBS電視.md "wikilink")（TBS）開台50週年紀念計劃的[日本電影](../Page/日本電影.md "wikilink")，劇情是根據[夏川里美演唱的](../Page/夏川里美.md "wikilink")[流行曲](../Page/流行曲.md "wikilink")《[淚光閃閃](../Page/淚光閃閃.md "wikilink")》歌詞改編，由《[現在，很想見你](../Page/現在，很想見你.md "wikilink")》[導演](../Page/導演.md "wikilink")[土井裕泰執導](../Page/土井裕泰.md "wikilink")。此片創下300萬觀影人次，票房突破30億[日圓](../Page/日圓.md "wikilink")，成為2006年[日本年度十大賣座](../Page/日本.md "wikilink")[電影](../Page/電影.md "wikilink")。

## 劇情概要

「不論發生任何事，都會守護小薰。」

住在[沖繩縣](../Page/沖繩縣.md "wikilink")[那霸市的](../Page/那霸市.md "wikilink")**新垣洋太郎**（[妻夫木聰飾](../Page/妻夫木聰.md "wikilink")）為了實現夢想──在市內開一家[酒館而不斷努力工作](../Page/居酒屋.md "wikilink")。同時，自少時便住在離島的**新垣薰**（洋太郎之妹，[長澤雅美飾](../Page/長澤雅美.md "wikilink")）因考上了高中而搬回了那霸居住。

洋太郎和小薰本無血緣關係，只是二人年幼時，因為洋太郎的母親改嫁給小薰的父親，二人才以兄妹相稱。之後，小薰的父親弃家而去，洋太郎的母親因病去世，洋太郎按照母親遺命，把薰送到住在島上的外婆撫養，而自己一直打工賺錢，照顧妹妹。

小薰初来那霸，十分高兴能与哥哥重逢，但她很快发现，洋太郎已经有了女朋友，准备报考醫學院，继承父亲医院的稻嶺惠子。洋太郎为了让小薰读上大学，努力工作。他用存下的钱盘下了一家酒馆，却在开业的前夜勒令交出。原来洋太郎受到騙子欺騙，酒馆已经被原主转卖他人。洋太郎因此身負巨債，在島上開設酒館的夢想破滅。于此同时，惠子的父亲提出帮洋太郎还清债务，但要求他和惠子分手，因为两人已不是同一个世界的人了。洋太郎也有此感觉，但他拒绝了惠子父亲的帮助，做体力工来还债，也和惠子分手。洋太郎听学校老师认为小薰的成绩必能考上大学，十分高兴。小薰却不忍哥哥辛苦，瞒着洋太郎打工。小薰和洋太郎慢慢发觉自己深爱对方，但礙於有名無實的「兄妹」枷鎖，只能各自压抑自己的感情。无法面对的小薰終於在考上[琉球大學後搬走](../Page/琉球大學.md "wikilink")。

在小薰的20歲成人禮前，[沖繩群島遭受颱風突襲](../Page/沖繩群島.md "wikilink")。夜里，小薰家的窗子被狂风下的树枝砸破。小薰最彷徨无助之时，洋太郎突然赶到，帮小薰堵住窗口，而洋太郎却病重跌倒。小薰致电惠子，将洋太郎送到医院，发现洋太郎因长年超负荷工作，患上了心肌炎。最终洋太郎病情恶化，终告不治。

葬礼之後，小薰收到洋太郎之前寄来的成人礼用的和服。小薰读着洋太郎附上的祝贺信，止不住泪如雨下……

## 得獎記錄

  - 第30回[日本電影金像獎](../Page/日本電影金像獎.md "wikilink")：
      - 優秀男主演賞：[妻夫木聰](../Page/妻夫木聰.md "wikilink")（提名）
      - 優秀女主演賞：[長澤雅美](../Page/長澤雅美.md "wikilink")（提名）

## 主要演員

  - 新垣洋太郎：[妻夫木聰](../Page/妻夫木聰.md "wikilink")（**主演**）（幼年：[廣田亮平](../Page/廣田亮平.md "wikilink")）（香港配音：[曹啟謙](../Page/曹啟謙.md "wikilink")、幼年：[謝潔貞](../Page/謝潔貞.md "wikilink")）
  - 新垣薰：[長澤雅美](../Page/長澤雅美.md "wikilink")（**主演**）（幼年：[佐佐木麻緒](../Page/佐佐木麻緒.md "wikilink")、[春名風花](../Page/春名風花.md "wikilink")）（香港配音：[黃紫嫻](../Page/黃紫嫻.md "wikilink")）
  - 稻嶺惠子：[麻生久美子](../Page/麻生久美子.md "wikilink")
  - 島袋勇一：[塚本高史](../Page/塚本高史.md "wikilink")（香港配音：[梁偉德](../Page/梁偉德.md "wikilink")）
  - 金城昭嘉（小薰的父親）：[中村達也](../Page/中村達也.md "wikilink")（香港配音：[蕭徽勇](../Page/蕭徽勇.md "wikilink")）
  - 新垣美戶\[1\]（外婆）：[平良登美](../Page/平良登美.md "wikilink")
  - 小綠：[森下愛子](../Page/森下愛子.md "wikilink")
  - 醫生：[大森南朋](../Page/大森南朋.md "wikilink")
  - 龜岡：[船越英一郎](../Page/船越英一郎.md "wikilink")（友情客串）
  - 稲嶺義郎（惠子的父親）：[橋爪功](../Page/橋爪功.md "wikilink")
  - 新垣光江（洋太郎的母親）：[小泉今日子](../Page/小泉今日子.md "wikilink")（香港配音：[黃玉娟](../Page/黃玉娟.md "wikilink")）
  - 小薰高中的班導：[與座嘉秋](../Page/與座嘉秋.md "wikilink")

## 製作人員

  - 製作人：[八木康夫](../Page/八木康夫.md "wikilink")
  - 導演：[土井裕泰](../Page/土井裕泰.md "wikilink")
  - 劇本：[吉田紀子](../Page/吉田紀子.md "wikilink")
  - 副導演：[豬腰弘之](../Page/豬腰弘之.md "wikilink")
  - 監製：[濱名一哉](../Page/濱名一哉.md "wikilink")
    [那須田淳](../Page/那須田淳.md "wikilink")
    [進藤淳一](../Page/進藤淳一.md "wikilink")

<!-- end list -->

  - 音樂：[千住明](../Page/千住明.md "wikilink")
  - 攝影：[濱田毅](../Page/濱田毅.md "wikilink")
  - 美術：[小川富美夫](../Page/小川富美夫.md "wikilink")
  - 燈光：[松岡泰彦](../Page/松岡泰彦.md "wikilink")
  - 錄音：[武進](../Page/武進.md "wikilink")
  - 編輯：[穗垣順之助](../Page/穗垣順之助.md "wikilink")
  - 記錄：[鈴木一美](../Page/鈴木一美.md "wikilink")
  - 製作擔當：[森太郎](../Page/森太郎.md "wikilink")
  - 主題歌：「淚光閃閃」（Victor娛樂集團）
      - 作詞：[森山良子](../Page/森山良子.md "wikilink")
      - 作曲：[BEGIN](../Page/BEGIN.md "wikilink")
      - 主唱：[夏川里美](../Page/夏川里美.md "wikilink")
  - 插曲：「三線之花」（Teichiku娛樂集團）
      - 主唱：[BEGIN](../Page/BEGIN.md "wikilink")
  - 共同製作：
  - 發行：[東寶株式會社](../Page/東寶株式會社.md "wikilink")
  - 製作：[「涙光閃閃」製作委員會](../Page/製作委員會方式.md "wikilink")（[TBS](../Page/東京廣播公司.md "wikilink")、[Amuse株式會社](../Page/Amuse.md "wikilink")、[東寶](../Page/東寶株式會社.md "wikilink")、[Horipro](../Page/Horipro.md "wikilink")、[TBS
    R\&C](../Page/TBS廣播與信息公司.md "wikilink")、[MBS](../Page/每日放送.md "wikilink")）

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [中文官網](http://tearsforyou.cmcmovie.com/)
  - [DVD版官方網頁](http://www.amuse-s-e.co.jp/nadasou/)

[Category:长泽雅美](../Category/长泽雅美.md "wikilink")
[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:沖繩縣背景作品](../Category/沖繩縣背景作品.md "wikilink")
[Category:TBS製作的電影](../Category/TBS製作的電影.md "wikilink")
[Category:每日放送製作的電影](../Category/每日放送製作的電影.md "wikilink")

1.  原文為「新垣ミト」，「ミト」為琉球語，是琉球女性常見童名，漢字作「美戶」