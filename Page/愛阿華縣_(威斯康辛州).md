**愛阿華縣**（**Iowa County,
Wisconsin**）是[美國](../Page/美國.md "wikilink")[威斯康辛州西南部的一個縣](../Page/威斯康辛州.md "wikilink")，北以[威斯康辛河為界](../Page/威斯康辛河.md "wikilink")。面積1,989平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口22,780。縣治[道奇維爾市](../Page/道奇維爾市_\(威斯康辛州\).md "wikilink")（Dodgeville）。

成立於1829年10月9日。縣名來自[艾奧瓦族](../Page/艾奧瓦族.md "wikilink")。\[1\]

## 參考資料

[I](../Category/威斯康辛州行政區劃.md "wikilink")
[I](../Category/威斯康辛州行政区划.md "wikilink")

1.  [威斯康辛州歷史學會](http://www.wisconsinhistory.org/dictionary/index.asp?action=view&term_id=3392&term_type_id=2&term_type_text=Places&letter=I)