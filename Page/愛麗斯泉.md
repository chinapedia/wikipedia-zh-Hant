[Alice_Springs_Australia.jpg](https://zh.wikipedia.org/wiki/File:Alice_Springs_Australia.jpg "fig:Alice_Springs_Australia.jpg")
**愛麗斯泉**（[英語](../Page/英語.md "wikilink")：**Alice Springs**，**The
Springs**，**The
Alice**，[阿蘭恩特族土著語](../Page/阿蘭恩特族.md "wikilink")：**Mparntwe**），又譯**-{zh-hant:艾麗斯泉、艾利斯斯普林斯;
zh-cn:爱丽斯泉、艾丽斯泉;
zh-tw:艾麗斯泉;}-**，舊稱**斯圖爾特**（**Stuart**），是[澳洲中部的一個城市](../Page/澳洲.md "wikilink")，距離最近海岸1,200公里，是[北領地三大主要城鎮之一](../Page/北領地.md "wikilink")
（另外兩個為首府[達爾文港和](../Page/達爾文港.md "wikilink")[凱瑟琳](../Page/凱瑟琳_\(北領地\).md "wikilink")）、第二大城市，距離達爾文港1,499公里，距離[南澳首府](../Page/南澳大利亞州.md "wikilink")[阿德萊德](../Page/阿德萊德.md "wikilink")1,532公里。2005年估計人口為26,486人\[1\]。

在區外以澳洲最著名的自然景觀和[澳洲土著文化重心](../Page/澳洲土著.md "wikilink")[烏魯汝以及](../Page/烏魯汝.md "wikilink")[叔特的小說](../Page/叔特.md "wikilink")《[像艾麗斯一樣的城市](../Page/像艾麗斯一樣的城市.md "wikilink")》聞名
（雖然兩者都位於城外）。

## 歷史

原名「斯圖爾特」，以紀念首位跨越澳洲南北的探險家[約翰·麥克杜爾·斯圖爾特](../Page/約翰·麥克杜爾·斯圖爾特.md "wikilink")，最初本地為橫越[澳洲荒野的](../Page/澳洲荒野.md "wikilink")[駱駝商隊之中途站](../Page/駱駝商隊.md "wikilink")。後來在一處名為愛麗斯（[澳洲郵政部長](../Page/澳洲郵政部.md "wikilink")[查爾斯·托德的妻子](../Page/查爾斯·托德.md "wikilink")）的永久水[泉附近建立了](../Page/泉.md "wikilink")[電報站](../Page/電報.md "wikilink")。1929年[汗號列車首度開抵本地](../Page/汗號列車.md "wikilink")，1933年改稱現名。1960年代美國和澳洲於本地西南之19公里處聯合建立了[松樹谷衛星監測站](../Page/松樹谷.md "wikilink")，為當地增加了約700人口。

## 人口

### 原住民

根據[2001年人口普查](../Page/2001年澳大利亞人口普查.md "wikilink")，當地有17%居民為原住民，而北領地的比例為29%\[2\]。作為澳洲中部的中心城市，周邊的原住民
（他們通常居住在市郊、特別租賃地甚至遠郊的原住民社區）定期會到那裡使用城市的服務。當地的傳統居民是中阿蘭恩特族。其他可以聽到的原住民語言有十多種之多\[3\]。

### 美國人

由於松樹谷的關係（當地有包括美國人和澳洲人的700工作人員），愛麗斯泉約2,000人持有美國國籍。美國人仍然慶祝[萬聖節前夕](../Page/萬聖節前夕.md "wikilink")、[感恩節](../Page/感恩節.md "wikilink")、[美國國慶等傳統節日](../Page/美國國慶.md "wikilink")，部分澳洲人也參與節慶活動。在[澳洲足球](../Page/澳洲足球.md "wikilink")、[橄欖球](../Page/橄欖球.md "wikilink")、[木球等傳統運動流行的同時](../Page/木球.md "wikilink")，[美式足球](../Page/美式足球.md "wikilink")、[棒球](../Page/棒球.md "wikilink")、[籃球等美式運動也有不少人參加](../Page/籃球.md "wikilink")。

### 流動人口

  - 旅客
  - 松樹谷的居民
  - 附近地區的原住民
  - 澳洲其他地區以及外國的短期勞工 （俗稱blow-ins）

## 氣候

愛麗斯泉每年任意一日的平均氣溫最高或最低為 20 °C。夏季氣溫通常達 40 °C （最高可達 48 °C），冬季最低溫度常達 -7 °C
（極端低溫為 -10 °C）。常年少雨或無雨，但每年的雨量變化很大\[4\]。

## 經濟

愛麗斯泉以旅遊業 （每年有500,000旅客經當地前往烏魯汝）\[5\]、政府對原住民的資助和松樹谷所帶來的收入為經濟支柱
（每年達1,200萬美元）\[6\]。

## 旅遊

以往愛麗斯泉是前往烏魯汝的必經之地，也是橫跨和縱貫澳洲大陸時的中途站。但在1984年[尤拉拉渡假村落成並建立空中交通連繫](../Page/尤拉拉.md "wikilink")，以及長途航機的出現後，許多旅客不再停留愛麗斯泉。

當地的旅遊設施包括渡假村、賭場、[亞拉倫藝文中心](../Page/亞拉倫藝文中心.md "wikilink") （Araluen Centre
for Arts and
Entertainment）、夜總會、會所、酒吧、餐廳和咖啡廳。景點和活動包括野生動物中心、老電報站、皇家飛行醫務所、空中學校、乘坐[熱氣球或駱駝](../Page/熱氣球.md "wikilink")、[爬蟲類動物中心](../Page/爬蟲類.md "wikilink")、[平克植物園](../Page/平克植物園.md "wikilink")
（Olive Pink Botanic Garden）和電影院。

住宿方向酒店、汽車公園、青年旅舍，各形各色。另外有舉辦到市外景點（包括[赫曼斯堡](../Page/赫曼斯堡_\(北領地\).md "wikilink")、[奧爾加山](../Page/奧爾加山.md "wikilink")、烏魯汝、華萊士羅克霍爾、[國王谷](../Page/國王谷_\(澳大利亞\).md "wikilink")、[錢伯斯柱等](../Page/錢伯斯柱.md "wikilink")）行程的旅客中心\[7\]特色活動有[芬克沙漠電單車賽和托德河賽艇](../Page/芬克沙漠電單車賽.md "wikilink")\[8\]。

附近原住民創作的工藝品和[神話故事可以在當地買到](../Page/澳洲神話.md "wikilink")\[9\]。

## 交通

1929年汗號列車自阿德萊得開通至本市。1980年代曾有開往[-{zh-hans:悉尼;zh-hant:悉尼;zh-cn:悉尼;zh-hk:悉尼;zh-tw:雪梨;zh-mo:悉尼;}-](../Page/悉尼.md "wikilink")、名為「愛麗斯號」的客車，但只營運了四年就結束；[愛麗斯泉車站現為](../Page/愛麗斯泉車站.md "wikilink")[汗號列車的中途站](../Page/汗號列車.md "wikilink")。這裡也是[斯圖爾特公路的中途站](../Page/斯圖爾特公路.md "wikilink")。市南14公里有[愛麗斯泉機場](../Page/愛麗斯泉機場.md "wikilink")，可通往各州首府和烏魯汝。

[Alice_Springs.jpg](https://zh.wikipedia.org/wiki/File:Alice_Springs.jpg "fig:Alice_Springs.jpg")

## 犯罪

本市設有一個高度設防的[拘留所和一所中高度設防](../Page/拘留所.md "wikilink")[監獄](../Page/監獄.md "wikilink")。在1986/86年度，在囚人士中有70%為原住民，主要涉及[酒後互相攻擊](../Page/酒.md "wikilink")。有一段時間當局曾嘗試容許以原住民的[習慣法](../Page/習慣法.md "wikilink")
（如[復仇](../Page/復仇.md "wikilink")）作為抗辯理由，但成效不大。在附近的烏魯汝和[巴羅克里克曾發生過](../Page/巴羅克里克.md "wikilink")[阿匝黎雅·張伯倫失蹤案和](../Page/阿匝黎雅·張伯倫.md "wikilink")[彼得·法爾孔尼奧失蹤案](../Page/彼得·法爾孔尼奧.md "wikilink")。

## 體育

比較流行的體育運動有澳式足球，另外還有木球。[澳式足球中部聯盟當中有數隊](../Page/澳式足球中部聯盟.md "wikilink")
（特別在原住民之間）有很高的參與率。該市的體育館 （Traeger
Park）可以舉行[澳洲足球聯賽和國際級木球賽](../Page/澳洲足球聯賽.md "wikilink")，有一萬個座席。

2004年，一場[阿德雷德港足球俱樂部](../Page/阿德雷德港足球俱樂部.md "wikilink") （Port Adelaide
Football Club）和[科靈伍德足球俱樂部](../Page/科靈伍德足球俱樂部.md "wikilink") （Collingwood
Football Club）的季前挑戰賽吸引了超過售票額的觀眾。

當地每年有兩項特色的體育賽事：一為[托德河賽艇](../Page/托德河賽艇.md "wikilink") （Henley-on-Todd
Regatta，又稱 Todd River Race）：以無底船在乾涸的河床上航行。這是世界上唯一在乾河上行進的賽艇。另外每年在賽馬場上
（Blatherskite
Park）舉辦的「駱駝杯」。顧名思義，全天的賽事參賽的是[駱駝而不是](../Page/駱駝.md "wikilink")[馬](../Page/馬.md "wikilink")。

## 媒體

本市有四個官方[澳洲廣播公司提供無線電廣播頻道](../Page/澳洲廣播公司.md "wikilink")：一個本地台 （**783 ABC
Alice Springs**）並轉播三個全國性頻道：[Radio
National](../Page/Radio_National.md "wikilink")、[ABC Classical
FM和](../Page/ABC_Classical_FM.md "wikilink")[Triple
J](../Page/Triple_J.md "wikilink")。本市另有兩家商業電台 （8HA 900 kHz 和 SUN FM 96.9
MHz）以及一家由原住民經營的[社區電台](../Page/社區電台.md "wikilink") （8KIN 100.5 MHz）。

本市有四條電視頻道，分別由澳洲廣播公司、[SBS](../Page/Special_Broadcasting_Service.md "wikilink")、由原住民經營的[足印電視台和](../Page/足印電視台.md "wikilink")[第七頻道](../Page/第七頻道.md "wikilink")。

## 流行文化

  - [叔特的小說](../Page/叔特.md "wikilink")《[像艾麗斯一樣的城市](../Page/像艾麗斯一樣的城市.md "wikilink")》
    （後來改編成電影和電視短劇）的故事雖然發生只發生在愛麗斯泉附近，但仍然以該市命名。
  - 在《[星艦前傳](../Page/星艦前傳.md "wikilink")》中，太空人接受生存訓練的地點就在愛麗斯泉。
  - 電影《[沙漠妖姬](../Page/沙漠妖姬.md "wikilink")》中，兩位[異裝皇后和一位](../Page/異裝皇后.md "wikilink")[變性者乘坐一輛名為Priscilla的大巴士穿越](../Page/變性.md "wikilink")[澳洲荒野前往愛麗斯泉](../Page/澳洲荒野.md "wikilink")。

## 参考文献

## 参见

  - [在世界中心呼喚愛](../Page/在世界中心呼喚愛.md "wikilink")
  - [愛麗斯泉熱氣球相撞事故](../Page/愛麗斯泉熱氣球相撞事故.md "wikilink")

## 外部連結

  - [市政府網站](http://www.alicesprings.nt.gov.au/)
  - [愛麗斯泉與周邊地區旅遊網站](https://web.archive.org/web/20140823102857/http://www.travelnt.com/en/explore/alice-springs/)
  - [市議會網站](http://www.alicesprings.nt.gov.au/)
  - [原住民法律資源](http://beta.austlii.edu.au/au/other/IndigLRes/rciadic/national/vol5/11.html)
  - [當地市集](https://web.archive.org/web/20070311064924/http://www.feelinfoodie.com/archives/2006/07/alice_springs_markets.html)
  - [中澳洲旅遊業商會](https://web.archive.org/web/20110725135938/http://www.centralaustraliantourism.com/)
  - [電影與電視](http://www.alicespringsfilmtv.com.au/_sum.asp)

[A](../Category/北領地城市.md "wikilink")

1.  [澳洲政府統計局](http://www.abs.gov.au/ausstats/abs@.nsf/Latestproducts/3218.0Main%20Features82004-05?opendocument&tabname=Summary&prodno=3218.0&issue=2004-05&num=&view=)
     Retrieved on 2006年9月25日
2.  [About Alice
    Springs](http://www.alicesprings.nt.gov.au/about_alice/about_sum.asp)

3.
4.  [Alice Springs'
    Climate](http://www.alicesprings.nt.gov.au/about_alice/climate.asp)
5.  [Alice Springs
    Lifestyle](http://www.alicesprings.nt.gov.au/about_alice/lifestyle.asp)

6.  [The American
    Connection](http://www.alicesprings.nt.gov.au/about_alice/american.asp)

7.  [Alice Springs
    Lifestyle](http://www.alicesprings.nt.gov.au/about_alice/lifestyle.asp)

8.  [Major Alice Springs
    Events](http://www.alicesprings.nt.gov.au/tourism/events.asp)
9.  [Visiting the Alice
    Springs](http://www.alicesprings.nt.gov.au/tourism/tourism.asp)