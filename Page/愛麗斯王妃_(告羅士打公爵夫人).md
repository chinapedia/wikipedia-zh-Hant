[Australia_stamp_Gloucesters_1945.jpg](https://zh.wikipedia.org/wiki/File:Australia_stamp_Gloucesters_1945.jpg "fig:Australia_stamp_Gloucesters_1945.jpg")
**告羅士打公爵夫人愛麗斯王妃**（，），全名**艾丽斯·克丽斯特贝尔·蒙塔古-道格拉斯-斯科特**（），英国国王[乔治五世的第三子](../Page/乔治五世_\(英国\).md "wikilink")、[乔治六世的弟弟](../Page/乔治六世.md "wikilink")[格洛斯特公爵](../Page/格洛斯特公爵.md "wikilink")[亨利王子的王妃](../Page/亨利王子_\(告羅士打公爵\).md "wikilink")，现女王[伊丽莎白二世的婶母](../Page/伊丽莎白二世.md "wikilink")。

## 家庭

1935年，艾丽斯嫁给了格洛斯特公爵[亨利王子](../Page/亨利王子_\(告羅士打公爵\).md "wikilink")，共育有两子。

1.  長子：[威廉王子](../Page/威廉王子_\(格洛斯特\).md "wikilink")(1941年12月18日-1972年8月28日)参加飞行比赛而死于空难，未婚，终年30岁。
2.  次子：[理查王子，第二代格罗切斯特公爵](../Page/理察親王_\(告羅士打公爵\).md "wikilink")
    (1944年8月26日-)1972年娶丹麦平民Birgitte Eva van Deurs (1946年6月20日-)。

<!-- end list -->

  -   - 孫：[阿爾斯特伯爵**亞歷山大·溫莎**](../Page/:en:Alexander_Windsor,_Earl_of_Ulster.md "wikilink")
          - 曾孫：[卡洛登勳爵**克桑·溫莎**](../Page/:en:Xan_Windsor,_Lord_Culloden.md "wikilink")
          - 曾孫女：[科西瑪·溫莎](../Page/:en:Lady_Cosima_Windsor.md "wikilink")

[Category:溫莎王朝貴族](../Category/溫莎王朝貴族.md "wikilink")
[Category:英国王妃](../Category/英国王妃.md "wikilink")
[Category:英国公爵夫人](../Category/英国公爵夫人.md "wikilink")
[Category:GBE勳銜](../Category/GBE勳銜.md "wikilink")
[Category:英國人瑞](../Category/英國人瑞.md "wikilink")