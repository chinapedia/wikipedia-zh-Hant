是一部1980年由[馬丁·史柯西斯執導](../Page/馬丁·史柯西斯.md "wikilink")，根据[意大利裔前中量级](../Page/意大利.md "wikilink")[拳-{}-王](../Page/拳王.md "wikilink")於1970年的回憶錄《**》的真实经历改编的美國[傳記](../Page/傳記片.md "wikilink")[黑白運動電影](../Page/黑白.md "wikilink")。由[羅拔·迪尼路主演](../Page/羅拔·迪尼路.md "wikilink")，他以本片獲得了[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")。本片也是[奧斯卡最佳剪接獎得主](../Page/奧斯卡最佳剪接獎.md "wikilink")。

## 劇情

意大利裔前拳王杰克·拉莫塔出生于[纽约](../Page/纽约.md "wikilink")[布朗克斯区](../Page/布朗克斯.md "wikilink")。他身体结实、出手敏捷，在拳坛被称作「愤怒的公牛」。由于不愿向[黑手党低头](../Page/黑手党.md "wikilink")，他始终无法获得拳王挑战的资格。

经历了数次挫折之后，他终于同意黑手党的安排，故意输掉一场比赛，以换取挑战拳王的资格。在取得拳王称号后，拉莫塔开始怀疑自己的妻子和弟弟，认定他们背叛了自己，于是对他们大打出手。矛盾的激化以及他看透拳击场上的名利追逐，最终使其退出了拳击界。经历了众多悲欢的拉莫塔最后在一家[夜总会里担任司仪](../Page/夜总会.md "wikilink")，并准备以此度过余生。

## 角色

  - [羅拔·迪尼路](../Page/羅拔·迪尼路.md "wikilink") 飾 \[1\]

  - 飾 \[2\]

  - [喬·佩西](../Page/喬·佩西.md "wikilink") 飾 \[3\]

  - 飾 Tommy Como\[4\]

  - 飾 Lenora LaMotta（喬伊的妻子）\[5\]

  - 飾 Salvy Batts\[6\]

  - Mario Gallo 飾 Mario\[7\]

  - Johnny Barnes 飾 [舒格·雷·羅賓遜](../Page/舒格·雷·羅賓遜.md "wikilink")

## 荣誉

  - [第53屆](../Page/第53屆奧斯卡金像獎.md "wikilink")[奧斯卡](../Page/奧斯卡金像獎.md "wikilink")

<!-- end list -->

  - 获奖- [最佳男主角](../Page/奧斯卡最佳男主角獎.md "wikilink")
  - 获奖- [最佳剪接](../Page/奧斯卡最佳剪接獎.md "wikilink")
  - 提名-最佳影片
  - 提名-最佳导演
  - 提名-女配角
  - 提名-男配角
  - 提名-最佳摄影
  - 提名-最佳音效

<!-- end list -->

  - [美国电影学会](../Page/美国电影学会.md "wikilink")

<!-- end list -->

  - [AFI百年百大电影](../Page/AFI百年百大电影.md "wikilink")：\#24
  - [AFI百年百大惊悚片](../Page/AFI百年百大惊悚片.md "wikilink")：\#51
  - [AFI十大類型十大佳片](../Page/AFI十大類型十大佳片.md "wikilink")：运动片\#1

## 參考資料

## 外部链接

  -
  - [Raging
    Bull](http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=54882)
    at the [American Film Institute Catalog of Motion
    Pictures](../Page/American_Film_Institute_Catalog_of_Motion_Pictures.md "wikilink")

  -
  - [*Raging Bull*](http://www.filmsite.org/ragi.html) at
    [FilmSite.org](../Page/Filmsite.org.md "wikilink")

  -
  -
  -
  -
[Category:马丁·斯科塞斯电影](../Category/马丁·斯科塞斯电影.md "wikilink")
[Category:1980年電影](../Category/1980年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:美国传记片](../Category/美国传记片.md "wikilink")
[Category:1980年代剧情片](../Category/1980年代剧情片.md "wikilink")
[Category:表現主義電影](../Category/表現主義電影.md "wikilink")
[Category:真人真事改編電影](../Category/真人真事改編電影.md "wikilink")
[Category:拳擊電影](../Category/拳擊電影.md "wikilink")
[Category:俄亥俄州背景电影](../Category/俄亥俄州背景电影.md "wikilink")
[Category:迈阿密背景电影](../Category/迈阿密背景电影.md "wikilink")
[Category:密歇根州背景电影](../Category/密歇根州背景电影.md "wikilink")
[Category:纽约市背景电影](../Category/纽约市背景电影.md "wikilink")
[Category:1940年代背景电影](../Category/1940年代背景电影.md "wikilink")
[Category:1950年代背景电影](../Category/1950年代背景电影.md "wikilink")
[Category:1960年代背景电影](../Category/1960年代背景电影.md "wikilink")
[Category:洛杉矶取景电影](../Category/洛杉矶取景电影.md "wikilink")
[Category:紐約市取景電影](../Category/紐約市取景電影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:奥斯卡最佳剪辑获奖电影](../Category/奥斯卡最佳剪辑获奖电影.md "wikilink")
[Category:金球獎最佳戲劇類男主角獲獎電影](../Category/金球獎最佳戲劇類男主角獲獎電影.md "wikilink")

1.  Evans, Mike *The Making of Raging Bull* 2006 p.177.

2.
3.
4.
5.
6.
7.