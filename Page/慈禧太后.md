**孝欽顯皇后**（\[1\]；），[葉赫那拉氏](../Page/那拉氏.md "wikilink")\[2\]\[3\]，[满洲镶蓝旗](../Page/满洲镶蓝旗.md "wikilink")。是[清朝末年的一位](../Page/清朝.md "wikilink")[皇太后](../Page/皇太后.md "wikilink")，为[同治](../Page/同治.md "wikilink")、[光緒年間](../Page/光緒.md "wikilink")（1861年至1908年，合計47年）實際上的主政者。她是[咸丰皇帝妃](../Page/咸丰皇帝.md "wikilink")，也是[同治皇帝的生母](../Page/同治皇帝.md "wikilink")。[同治皇帝即位後](../Page/同治皇帝.md "wikilink")，尊封為**聖母皇太后**；光緒帝即位後，稱為皇爸爸（一說满人稱姑母為爸爸，但慈禧為光緒姨母而非姑母；另有一說為滿族常將家中地位顯赫的長輩女子稱呼男性化），人人以[老佛爺尊稱之](../Page/老佛爺.md "wikilink")，與[慈安太后在](../Page/慈安太后.md "wikilink")[養心殿裡一起](../Page/養心殿.md "wikilink")[垂簾聽政長達二十年](../Page/垂簾聽政.md "wikilink")，直到[光緒七年](../Page/光緒.md "wikilink")（1881年）[慈安太后過世](../Page/慈安太后.md "wikilink")，慈禧太后才獨攬大權。

慈禧太后是[同治](../Page/同治.md "wikilink")，[光緒年间](../Page/光緒.md "wikilink")[清朝实际上的最高統治者](../Page/清朝.md "wikilink")，包括前面与[慈安太后的](../Page/慈安太后.md "wikilink")[兩宮聽政](../Page/兩宮聽政.md "wikilink")，統治[清王朝长达四十七年](../Page/清王朝.md "wikilink")，在[清朝僅次於](../Page/清代.md "wikilink")[康熙帝與](../Page/康熙帝.md "wikilink")[乾隆帝](../Page/乾隆帝.md "wikilink")。掌权时間不仅超越[唐朝](../Page/唐朝.md "wikilink")[武则天](../Page/武则天.md "wikilink")\[4\]、[漢朝](../Page/漢朝.md "wikilink")[吕后](../Page/吕后.md "wikilink")\[5\]且超越大多数帝王；统治期间发动[政变兩次](../Page/政变.md "wikilink")，立皇储两次，推動變革三次。死后[谥号为](../Page/谥号.md "wikilink")「**孝欽慈禧端佑康頤昭豫-{zh-hans:庄;zh-hant:莊;}-誠壽恭欽獻崇熙配天興聖顯皇后**」，長度為大清皇后之最，亦超過大清開國的[孝莊文皇后及](../Page/孝莊文皇后.md "wikilink")[孝德](../Page/孝德顯皇后.md "wikilink")、[孝貞](../Page/慈安太后.md "wikilink")（東太后慈安）二位咸豐帝的[正妻](../Page/正妻.md "wikilink")。在慈禧太后死後僅三年，[清朝覆滅](../Page/宣統退位.md "wikilink")，[中華民國成立](../Page/中華民國.md "wikilink")。

## 生平

[道光十五年十月十日](../Page/道光.md "wikilink")（1835年11月29日）出生，归化城副都统惠显之女镶黄旗满洲富察氏所出。根据《宫中档差务杂录》的記載，孝钦显皇后给其本家祖先祭祀时所写的文辞“孝次女”来看，孝钦显皇后應是惠徵的次女。葉赫那拉家族後人根正所稱，慈禧太后的乳名叫**杏兒姑**，「姑」是滿人對未成年女子的通常稱呼，而「杏兒」則是因為當時家中庭院種有幾顆白杏樹。

### 咸豐時期

咸豐二年（1852年）二月十一日，時年十七歲的葉赫那拉氏在外八旗選秀中被指定**蘭貴人**，同年五月初九日由本家送入圓明園，居儲秀宮麗景軒。\[6\]。葉赫那拉氏未知何時改號為**懿貴人**\[7\]。

咸豐三年六月初三日（1853年7月8日），蘭貴人之父惠徵在[鎮江府病逝](../Page/镇江府.md "wikilink")。同年七月初六日报单内开，咸豐帝命沈振麟画皇上穿盔甲乘马式御容大挂轴一张，以及主位喜容稿九张
，即兰贵人那拉氏等全體內庭主位俱有一幅畫像。咸豐四年二月二十六日（1854年），時年十九歲的蘭貴人詔封**懿嬪**\[8\]。根據《鴻稱通用》的記載，懿字的滿文意思為「端莊」和「文雅」，充分體現了文宗視角內孝欽顯皇后的性格。册文如下：

咸丰五年十月十三日，候补员外郎春年和懋勤殿太监张得喜交御笔字条一张：「慎重和平」。咸豐帝命人用一寸蓝绫边贴储秀宫殿内，不进工匠，而張贴地方要问懿嫔娘娘。咸豐六年三月（1856年），生皇子[載淳](../Page/同治帝.md "wikilink")（即[同治帝](../Page/同治帝.md "wikilink")），晉封為**懿妃**。册文如下：

咸豐七年正月（1857年），時年二十二歲的懿妃晉封**懿貴妃**。册文如下：

咸丰六年十二月二十七日，咸丰帝命人交「御笔福、禄、寿、喜」各一张。翌日，进内贴储秀宫大爷殿内，張貼的地方要问贵妃娘娘。[咸豐帝體弱多病](../Page/咸丰帝.md "wikilink")，兼之當時的大清北有[英法聯軍入侵](../Page/英法聯軍.md "wikilink")[北京](../Page/北京.md "wikilink")、南有[太平天國反清農民運動](../Page/太平天國.md "wikilink")，正值內憂外患之際，讓他心力憔悴。懿貴妃工於書法，於是咸豐帝時常口授並讓其代筆批閱奏章，並且允許懿貴妃發表自己的意見，因而大臣們多對葉赫那拉氏不滿。

咸豐十年八月（1860年），[英法联军在](../Page/第二次鸦片战争.md "wikilink")[第二次鸦片战争中攻破](../Page/第二次鸦片战争.md "wikilink")[大沽口](../Page/大沽口.md "wikilink")，占领[天津](../Page/天津.md "wikilink")。随后在八里桥击溃了清军的精锐，[京师危在旦夕](../Page/北京.md "wikilink")。9月22日，咸丰帝率包含慈禧在內的一干宫眷逃往[热河](../Page/热河厅.md "wikilink")[避暑山庄避难](../Page/避暑山庄.md "wikilink")，留恭親王[奕訢在京师與聯軍議和](../Page/奕訢.md "wikilink")。英法聯軍在北京大肆搶劫後，10月18日將包括[圓明園在內的皇家](../Page/圓明園.md "wikilink")“三山五園”焚燒，大火燒了三天三夜。

[咸豐十一年七月十七日](../Page/咸豐.md "wikilink")（1861年8月22日），[咸丰帝在](../Page/咸丰帝.md "wikilink")[承德](../Page/承德.md "wikilink")[避暑山庄煙波致爽殿內](../Page/避暑山庄.md "wikilink")[驾崩](../Page/驾崩.md "wikilink")，享年30歲。

[咸豐皇帝临终前曾經賜給](../Page/咸豐皇帝.md "wikilink")[慈安太后和慈禧太后兩人各一枚印章](../Page/慈安太后.md "wikilink")。賜給[慈安太后的稱為](../Page/慈安太后.md "wikilink")“御賞”，慈禧太后的則稱為“同道堂”。也就是所有的旨意先由[顧命八大臣擬定後](../Page/顧命八大臣.md "wikilink")，再交由[兩宮太后審查後蓋上](../Page/兩宮太后.md "wikilink")[咸豐皇帝所賜的御印後](../Page/咸豐皇帝.md "wikilink")，即可正式生效。咸丰帝令其年仅五岁的獨子[载淳继承皇位](../Page/同治帝.md "wikilink")，并任命怡親王[載垣](../Page/載垣.md "wikilink")、鄭親王[端華](../Page/端華.md "wikilink")、戶部尚書[肅順](../Page/肅順.md "wikilink")、額駙[景壽](../Page/景壽.md "wikilink")、兵部尚書[穆蔭](../Page/穆蔭.md "wikilink")、吏部左侍郎[匡源](../Page/匡源.md "wikilink")、禮部右侍郎[杜翰](../Page/杜翰.md "wikilink")、太僕寺少卿[焦祐瀛八人为](../Page/焦祐瀛.md "wikilink")「贊襄政務王、大臣」，辅佐嗣君，人称“[顾命八大臣](../Page/顾命八大臣.md "wikilink")”。

[同治帝在避暑山庄居丧期间](../Page/同治帝.md "wikilink")，奉[嫡母](../Page/嫡母.md "wikilink")[皇后](../Page/皇后.md "wikilink")[钮祜禄氏為](../Page/慈安太后.md "wikilink")**母后皇太后**，住[烟波致爽殿的东暖阁](../Page/烟波致爽殿.md "wikilink")；奉生母那拉氏為**皇太妃**。次日，尊**母后皇太后**為**慈安太后**，皇太妃那拉氏加封為**聖母皇太后**，尊號**慈禧**，住在西暖阁；因此，慈安和慈禧分别被称为**东宮太后**和**西宮太后**。

#### 祺祥之變

咸豐帝死後，皇子載淳即位，9月3日發命，明年改元**[祺祥](../Page/祺祥.md "wikilink")**\[9\]，葉赫那拉氏與皇后[鈕祜祿氏並尊為皇太后](../Page/慈安太后.md "wikilink")。

[顾命八大臣与慈禧产生了严重的矛盾](../Page/顾命八大臣.md "wikilink")。而当时恭亲王[奕訢已与西方列国达成议和](../Page/奕訢.md "wikilink")，于9月5日赴[热河奔丧](../Page/热河.md "wikilink")。奕訢与慈禧秘密取得联系，决定策划一次[政变](../Page/政变.md "wikilink")。在慈禧的鼓动下，这次政变得到了[慈安太后的同意](../Page/慈安太后.md "wikilink")。同年9月14日，山東道監察御史[董元醇奏请两宫皇太后](../Page/董元醇.md "wikilink")[垂簾聽政](../Page/垂簾聽政.md "wikilink")，慈禧与[慈安便召八大臣入议](../Page/慈安.md "wikilink")，八大臣以“本朝未有皇太后垂簾”为由拒绝。在奕訢的帮助下，慈禧取得了侍郎[勝保](../Page/勝保.md "wikilink")、大學士[賈楨等多人的支持](../Page/賈楨.md "wikilink")。10月26日，咸丰帝的灵柩运回京师时，慈禧命八大臣护送灵柩殿后，自己与慈安、嗣君载淳則先达京师。随后，慈禧便先发制人，利用帝-{后}-和[咸豐帝的梓宮回京的機會發動](../Page/咸丰帝.md "wikilink")[辛酉政變](../Page/辛酉政变.md "wikilink")，設計逮捕了八大臣，判處怡親王[載垣](../Page/載垣.md "wikilink")、鄭親王[端華自裁](../Page/端华.md "wikilink")、[肅順斬立決](../Page/肃顺.md "wikilink")，其他人革職。[奕訢被封為議政王](../Page/奕訢.md "wikilink")。

自此，八大臣势力被铲除。由八大臣拟定的年号“[祺祥](../Page/祺祥.md "wikilink")”也被廢除，11月7日下詔，祺祥年號廢除，翌年改元為[同治](../Page/同治.md "wikilink")，以誌「同歸於治」、「君臣同治」、「同於[順治](../Page/顺治帝.md "wikilink")」。

年號祺祥為[同治甲子](../Page/同治.md "wikilink")，載淳在北京[紫禁城](../Page/紫禁城.md "wikilink")[太和殿登基](../Page/太和殿.md "wikilink")，頒詔天下，以第二年為同治元年，故稱同治帝。十一月乙酉朔，[嫡母](../Page/嫡母.md "wikilink")[慈安太后](../Page/慈安太后.md "wikilink")、生母慈禧太后在[養心殿正式](../Page/养心殿.md "wikilink")[垂簾聽政](../Page/垂帘听政.md "wikilink")。登基時，同治帝年僅五歲，故其後一直由[慈安太后](../Page/慈安太后.md "wikilink")、慈禧太后垂簾聽政，史稱[兩宮聽政](../Page/兩宮聽政.md "wikilink")。

### 同治時期

#### 垂簾聽政

執政初期，在議政王[奕訢的輔佐下](../Page/奕訢.md "wikilink")，整飭吏治，重用漢臣，依靠[曾國藩](../Page/曾国藩.md "wikilink")、[左宗棠](../Page/左宗棠.md "wikilink")、[李鴻章等漢族地主武裝](../Page/李鴻章.md "wikilink")；又在列強支持下，先後鎮壓了[太平天國](../Page/太平天国.md "wikilink")、捻軍、苗民、回民起義，緩解清王朝的統治危機，使清王朝得到暫時穩定。出於維護封建專制統治，她又重用[洋務派](../Page/洋务派.md "wikilink")，以“自強”和“求富”的方針，發展一些軍用，民用工業，訓練海軍和陸軍以加強政權實力。客觀上對清國的近代化起到了一定積極作用。這一時期，國內起義被平定，兩次鴉片戰爭暫時滿足列強的貪欲，外交上沒有吃大虧，洋務運動後清王朝的軍事實力有所提高，工商業有了初步發展，史稱為“
[同治中興](../Page/同光中兴.md "wikilink") ”。

[同治十一年](../Page/同治.md "wikilink")（1872年），[載淳已](../Page/同治帝.md "wikilink")17歲，慈禧不得已為他選-{后}-，次年兩宮太后捲簾歸政。但同治帝親政後仍難擺脫慈禧的干預。慈禧為享樂授意同治帝修繕圓明園以供其居住，同治帝也想趁此機會讓太后離宮居住以擺脫慈禧干預朝政，然而當時財政緊缺，[圓明園又殘毀嚴重](../Page/圆明园.md "wikilink")，修復耗資甚鉅，同治帝堅持開工，引起奕訢等王公大臣多人反對，同治帝竟要將他們全部革職，慈禧出面制止了同治帝這一決定。

[同治十三年](../Page/同治.md "wikilink")（1875年），同治帝病逝，年僅18歲。同治無後，慈禧太后就立[咸豐帝之弟](../Page/咸丰帝.md "wikilink")[奕譞之子](../Page/奕譞.md "wikilink")[載湉入嗣大宗](../Page/光绪帝.md "wikilink")，繼承[大清皇位](../Page/大清.md "wikilink")，改年號為[**光緒**](../Page/光绪.md "wikilink")，兩宮太后再次垂簾聽政。

### 光緒時期

#### 光緒初年政局與中法戰爭

光绪六年（1880年），慈禧太后重病。

光绪八年（1882年），慈禧太后痊癒。同年，慈安太后逝世，慈禧太后独自[垂帘听政](../Page/垂帘听政.md "wikilink")。

光緒九年（1883年），[中法戰爭爆發](../Page/中法戰爭.md "wikilink")，雙方在軍事上互有勝負，但朝廷卻主張“乘勝即收”。

光緒十年（1884年），清軍在北圻作戰不利，慈禧太后遂罷免[恭親王奕訢為首的全班軍機大臣](../Page/奕訢.md "wikilink")。以禮親王[世鐸入值領班軍機](../Page/世鐸.md "wikilink")，慶郡王[奕劻領](../Page/奕劻.md "wikilink")[總理各國事務衙門](../Page/總理各國事務衙門.md "wikilink")，惟慈禧獨斷乾綱，此為甲申易樞朝局之變。

[光緒十一年](../Page/光緒.md "wikilink")（1885年），與[法國簽定](../Page/法國.md "wikilink")《[中法新約](../Page/中法新約.md "wikilink")》，中國默認法國對越南的保護權。此條約亦加速中國西南邊陲淪爲法國的勢力範圍。

[光緒十四年](../Page/光緒.md "wikilink")（1888年），[光緒皇帝大婚](../Page/光緒皇帝.md "wikilink")，慈禧太后主張冊立其侄女，就是後來的[隆裕太后為](../Page/隆裕太后.md "wikilink")[皇后](../Page/皇后.md "wikilink")。婚後形成[光緒皇帝親政](../Page/光緒皇帝.md "wikilink")，慈禧太后[訓政的朝政格局](../Page/訓政.md "wikilink")。訓政結束後，朝廷一切用人行政，仍惟慈禧之命是從，一體裁決：“上（光緒帝）事太后謹，朝廷大政，必請命乃行。”

[光緒二十年](../Page/光緒.md "wikilink")（1894年），[醇親王以慈禧六十壽辰擬](../Page/载沣.md "wikilink")“在[頤和園受賀](../Page/颐和园.md "wikilink")，仿康熙、乾隆年間成例，自大內至園，路所經，設彩棚經壇，舉行慶典”。納海防捐以繕修頤和園、佈置點景，廣收貢獻。

#### 中日甲午戰爭

[光緒二十年](../Page/光緒.md "wikilink")（1894年），[**甲午海戰**爆發](../Page/甲午海战.md "wikilink")。[光緒皇帝主戰](../Page/光緒皇帝.md "wikilink")，慈禧復議“不准有示弱語”。但當部分大臣奏請停止頤和園工程，停辦點景，開内帑疏财赡军时，慈禧大怒，曰“今日令吾不歡者，吾亦將令彼終生不歡”。战争伊始，清軍便在朝鮮戰場上接連失利，而北洋水師在黃海之戰中又遭受嚴重挫折。礙於六旬壽典，慈禧期冀列强干涉，以盡快結束戰爭。授意李鴻章避戰求和，以各種藉口，打擊以光緒為首的主戰派。鑒於列强干涉失敗，形勢日益緊張以及朝野上下的重重壓力，慈禧不得不改變原來的計劃，壽典規模有所收斂。在金州、大連相繼陷落，旅順萬分危急的情況下，紫禁城寧壽宮内慈禧的壽典落幕。

光緒二十一年二月（1895年），威海衛日艦及砲台夾攻劉公島，[北洋水師全軍覆沒](../Page/北洋水師.md "wikilink")，日軍挺進山海關，京師告急。不得已，慈禧下定決心不惜代價向日本求和。三月，慈禧派[李鴻章為全權大臣](../Page/李鴻章.md "wikilink")，赴日議和。四月簽訂中國歷史上空前屈辱的《[**馬關條約**](../Page/马关条约.md "wikilink")》，中國放棄對[朝鮮國的](../Page/朝鮮國.md "wikilink")[宗主地位](../Page/宗主.md "wikilink")，賠款二億兩[白銀](../Page/白銀.md "wikilink")，割讓[遼東半島](../Page/遼東半島.md "wikilink")（在[俄國](../Page/俄國.md "wikilink")，[德國](../Page/德國.md "wikilink")，[法國等西方列強干涉下](../Page/法國.md "wikilink")，後以白銀3000萬兩贖回）、台灣、澎湖列島，開放4個通商口岸，允許日本在通商口岸開礦設廠。

#### 戊戌變法與慈禧訓政

二十四年六月（1898年），光绪帝令[翁同龢起草](../Page/翁同龢.md "wikilink")《[明定國是詔](../Page/明定國是詔.md "wikilink")》，面陳慈禧，得到應允。乃于翌日颁布，[戊戌变法正式展開](../Page/戊戌变法.md "wikilink")。但變法過於操切，觸動了滿洲貴族和眾多官僚的利益，反對者十之八九。慈禧支持維新强國，默許皇帝對變法反對者的制裁。但慈禧亦擔心他人藉變法生事端對自己不利，又處處干涉，大權在握。當時傳言光緒帝授意維新派命[袁世凱派兵殺](../Page/袁世凯.md "wikilink")[榮祿](../Page/荣禄.md "wikilink")，圍頤和園迫使慈禧歸政。對大權旁落深感恐懼的慈禧軟禁皇帝，對外宣稱光绪帝得病，不能视朝，“不得已临朝称制”，重新训政，此為[戊戌政變](../Page/戊戌政變.md "wikilink")。自此，爲期一百余日的維新變法終止，維新期間的大部分條綱被废止，維新派重要人物[譚嗣同](../Page/譚嗣同.md "wikilink")、[楊銳](../Page/楊銳_\(清朝\).md "wikilink")、[林旭](../Page/林旭.md "wikilink")、[劉光第](../Page/劉光第.md "wikilink")、[楊深秀](../Page/楊深秀.md "wikilink")、[康有溥等](../Page/康有溥.md "wikilink")[維新六君子被](../Page/維新六君子.md "wikilink")[斩首](../Page/斩首.md "wikilink")，康有为、梁启超流亡海外。[The_Imperial_Portrait_of_the_Ci-Xi_Imperial_Dowager_Empress.PNG](https://zh.wikipedia.org/wiki/File:The_Imperial_Portrait_of_the_Ci-Xi_Imperial_Dowager_Empress.PNG "fig:The_Imperial_Portrait_of_the_Ci-Xi_Imperial_Dowager_Empress.PNG")

[上海師範大學教授](../Page/上海師範大學.md "wikilink")[雷家聖指出](../Page/雷家聖.md "wikilink")：日本前首相[伊藤博文在戊戌變法期間到中國訪問](../Page/伊藤博文.md "wikilink")。當時英國傳教士[李提摩太向變法派領袖](../Page/李提摩太.md "wikilink")[康有為建議](../Page/康有為.md "wikilink")，要求清朝方面聘請伊藤為顧問，甚至付以事權\[10\]\[11\]，變法派官員在伊藤抵華後，紛紛上書請求重用伊藤，引起保守派官員的警惕。保守派官員[楊崇伊甚至就事件密奏](../Page/楊崇伊.md "wikilink")[慈禧太后](../Page/慈禧太后.md "wikilink")：「風聞東洋故相伊藤博文，將專政柄。伊藤果用，則祖宗所傳之天下，不啻拱手讓人。」\[12\]楊崇伊的激烈言論，促使慈禧太后在9月19日由頤和園回到紫禁城，意欲瞭解光緒皇帝對伊藤有何看法。

伊藤與李提摩太又向康有為提議「中美英日合邦」，於是在康有為的授意下，變法派官員[楊深秀於](../Page/楊深秀.md "wikilink")9月20日上書光緒：「臣尤伏願我皇上早定大計，固結英、美、日本三國，勿嫌『合邦』之名之不美。」\[13\]另一變法派官員[宋伯魯也於](../Page/宋伯魯.md "wikilink")9月21日上書言道：雷家圣认为，这是欲將中國軍事、財稅、外交等國家大權，交於外人之手，所以慈禧太后驚覺事態嚴重，當機立斷發動政變，重新訓政，結束了戊戌變法\[14\]。雷家圣亦認為「合邦」为外国的阴谋，康有为在戊戌变法前即曾与日本人联系，要与日本人联合召开「两国合邦大会议」\[15\]。戊戌变法开始后，李提摩太又向康有为建议中、美、英、日四国「合邦」，藉以对抗俄国，他指出，这在当时是完全不切实际的。但身为高级知识分子的李提摩太却向康有为提出这种建议，动机令人怀疑。康有为更向光绪建议要向李提摩太与伊藤博文「商酌办法」，则控制权将完全掌握在外人手中。因此李提摩太「合邦」的计划，可以说是一个外交的骗局，利用康有为等人对国际常识不足的弱点，诱骗康有为等人与光绪将交出军事、财政、外交等权力给外国人，任由外国操控宰割\[16\]。

戊戌政變之後，許多官吏紛紛上書[彈劾康有為](../Page/彈劾.md "wikilink")、梁啟超等人，如兵部掌印給事中[高燮曾於八月十一日上奏言](../Page/高燮曾.md "wikilink")：「從前朝鮮被倭人戕妃逼王，其明證也。」[福建道](../Page/福建.md "wikilink")[監察御史](../Page/監察御史.md "wikilink")[黃桂鋆上奏](../Page/黃桂鋆.md "wikilink")：「大約康有為等，內則巧奪政權，外則私通敵國，其主持變法之說，皆欺人語也。」民間學者[王先謙也批評康有為](../Page/王先謙.md "wikilink")「借兵外臣，倚重鄰敵，以危宗社，又兼崔胤、張邦昌而有之，誠亂臣賊子之尤也。」當時的官僚與士大夫，已經將康有為等人的陰謀與朝鮮的[乙未事變作比較](../Page/乙未事變.md "wikilink")，並發現了其中的相似之處\[17\]。

#### 義和團之亂與八國聯軍

受到[戊戌政變的影響](../Page/戊戌政變.md "wikilink")，慈禧太后在此后一段时间内开始排斥維新派，並因此利用端王[載漪](../Page/載漪.md "wikilink")、[剛毅等守舊親貴](../Page/剛毅.md "wikilink")，又打算為[光绪帝立皇嗣](../Page/光绪帝.md "wikilink")\[18\]\[19\]。

[光緒二十五年十二月二十四日](../Page/光绪.md "wikilink")（1899年），[溥儁受詔入](../Page/溥儁.md "wikilink")[宮](../Page/紫禁城.md "wikilink")，封為大阿哥，有「立儲」之意，並以[崇綺為師傅](../Page/崇綺.md "wikilink")，命在[弘德殿讀書](../Page/弘德殿.md "wikilink")，是為[己亥立儲](../Page/己亥立儲.md "wikilink")。列國公使認為此舉有廢立之意，或導致中國局勢不穩，從而影響自己的利益。出於此慮，拒絕入賀，廢立之事遂偃旗息鼓。\[20\]。[Hubert_Vos's_painting_of_the_Dowager_Empress_Cixi_(Tzu_Hsi).jpg](https://zh.wikipedia.org/wiki/File:Hubert_Vos's_painting_of_the_Dowager_Empress_Cixi_\(Tzu_Hsi\).jpg "fig:Hubert_Vos's_painting_of_the_Dowager_Empress_Cixi_(Tzu_Hsi).jpg")繪\]\]

[載漪為求其子早日登基](../Page/载漪.md "wikilink")，乃利用皇太后對“洋人”的嫉忌之心，極力離間帝-{后}-。順此，朝中形勢乃逐漸演變為非理性仇視“洋人”的守舊親貴，結合保守的清流派，對抗主張[務實外交的朝臣之局](../Page/務實外交.md "wikilink")。在端王等當權親貴的縱容，甚至暗助之下，以[扶乩](../Page/扶乩.md "wikilink")[迷信加上](../Page/迷信.md "wikilink")[民族主義起家的](../Page/民族主義.md "wikilink")“[義和拳](../Page/義和拳.md "wikilink")”乃得以大舉進入[直隸](../Page/直隸.md "wikilink")、進迫北京，形成一股“逼宮”的形勢。

慈禧太后雖未必相信拳民“神功護體、[刀槍不入](../Page/刀槍不入.md "wikilink")”之說，但看到“民氣可用”；且号称上百万的义和团民已经在北京附近大量聚集，慈禧太后担心镇压义和团会促使其矛头转而指向清王朝，亦未嘗嚴令鎮壓拳民，終於釀成拳民大规模进入[京畿](../Page/京畿.md "wikilink")，并且殘殺“教民”、攻擊外人、甚至殺死[德國公使](../Page/德國.md "wikilink")、[日本](../Page/大日本帝國.md "wikilink")[外交官等人員的事故](../Page/外交官.md "wikilink")，引起[八國聯軍干涉之禍](../Page/八國聯軍.md "wikilink")。慈禧太后又誤信各國欲迫其退位的假情報，負氣处死主張透過外交途徑解決危機的[大臣](../Page/庚子被禍五大臣.md "wikilink")，並随即對多國宣戰，至此大勢乃全無轉圜餘地\[21\]。

光緒二十六年七月二十日（1900年8月14日凌晨），八國聯軍攻入北京；15日凌晨，攻紫禁城[東華門](../Page/東華門.md "wikilink")，慈禧帶著光緒帝等宫眷自[德胜门逃出京师](../Page/德胜门_\(北京\).md "wikilink")，经过[宣化](../Page/宣化府_\(清朝\).md "wikilink")、[大同](../Page/大同市.md "wikilink")、[太原](../Page/太原.md "wikilink")，于九月到达[西安](../Page/西安.md "wikilink")。令奕劻、李鴻章為全權大臣，與列強進行談判，把戰爭的責任推到[義和團身上](../Page/義和團.md "wikilink")，下令對義和團“痛加剿除”。

[光緒二十六年十二月二十六日](../Page/光緒.md "wikilink")（1901年2月14日），批准《議和大綱》，並發布上諭，表示要「量中華之物力，結與國之歡心。」同年9月7日清廷與11個帝國主義國家簽訂了《[辛丑條約](../Page/辛丑條約.md "wikilink")》，規定按照當時中國人口的數量賠款4.5億兩白銀，39年內賠款9.8億兩白銀，懲辦主戰官員，拆除大沽到北京沿線所有砲台等。是[中國歷史上賠款最多的](../Page/中國歷史.md "wikilink")[條約](../Page/條約.md "wikilink")。

[光緒二十八年十一月二十九日](../Page/光緒.md "wikilink")（1902年1月8日），歷時三月回到北京。慈禧和光绪帝都下詔罪己。端郡王[載漪失势](../Page/载漪.md "wikilink")，[溥儁也被废除大阿哥头衔](../Page/溥儁.md "wikilink")，以公爵头衔迁出宫。

#### 庚子新政

1901年（[光緒二十七年](../Page/光緒.md "wikilink")）《[辛丑条约](../Page/辛丑条约.md "wikilink")》签订之后，慈禧太后為了挽回人心下詔實行新政，是为[庚子新政](../Page/庚子新政.md "wikilink")。这次改革比戊戌变法更广更深，实行千年之久的[科举制度也被废除](../Page/科举制度.md "wikilink")。在[张之洞](../Page/张之洞.md "wikilink")、[刘坤一的建议下](../Page/刘坤一.md "wikilink")，慈禧决定效仿日本，实行君主立宪制，下令預備立憲，又派五大臣前往西方列國考察。

1904年（[光緒三十年](../Page/光緒.md "wikilink")）爆發[日俄戰爭](../Page/日俄战争.md "wikilink")，戰場正是在中國東北，以[慈禧太后為首的清政府宣布中立](../Page/慈禧太后.md "wikilink")，戰爭的結果是日本戰勝了沙俄。國內人們普遍意識到[君主立憲優於](../Page/君主立憲.md "wikilink")[君主專制](../Page/君主專制.md "wikilink")，要求清政府進行憲政改革；與此同時，國內革命運動也愈發高漲。為了維持[政權](../Page/政權.md "wikilink")，慈禧作出要立憲的姿態。

1905年（[光緒三十一年](../Page/光緒.md "wikilink")）派五大臣出洋考察，1906年（[光緒三十二年](../Page/光緒.md "wikilink")）又下詔預備立憲，1908（[光緒三十四年](../Page/光緒.md "wikilink")）年頒布《[欽定憲法大綱](../Page/钦定宪法大纲.md "wikilink")》，內容仿照德國和日本的憲法，維護皇帝“君上大權”。

1908年由於慈禧通過照片外交，美國總統[西奧多·羅斯福簽署法案](../Page/西奥多·罗斯福.md "wikilink")，退還庚子賠款一千多萬美元，主要用於支持中國官派留美學生；之後，英國、法國、[比利時](../Page/比利時.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[荷蘭等國相繼](../Page/荷蘭.md "wikilink")；七國退還中子賠國之庚款“溢款”總數，約在海關銀三億兩左右，對興辦教育事業頗有效果，應當肯定。

[光緒三十四年十月二十一日](../Page/光緒.md "wikilink")（1908年11月14日），[光绪皇帝在](../Page/光绪皇帝.md "wikilink")[北京](../Page/北京.md "wikilink")[中南海](../Page/中南海.md "wikilink")[瀛臺涵元殿內駕崩](../Page/瀛臺.md "wikilink")（今考証被[砒霜毒死](../Page/砒霜.md "wikilink")），享年三十七歲，大行皇帝無嗣，經慈禧太后下詔，命醇親王[載灃為監國](../Page/载沣.md "wikilink")[攝政王](../Page/攝政王.md "wikilink")，其長子[溥儀繼承](../Page/溥仪.md "wikilink")[大清王朝皇位](../Page/大清王朝.md "wikilink")，年號**[宣統](../Page/宣統.md "wikilink")**，慈禧被尊為[太皇太后](../Page/太皇太后.md "wikilink")。

[光緒三十四年十月二十二日](../Page/光緒.md "wikilink")（1908年11月15日），慈禧太后崩逝於[北京](../Page/北京.md "wikilink")[中南海](../Page/中南海.md "wikilink")[儀鸞殿的後殿福昌殿內](../Page/儀鸞殿.md "wikilink")，享壽七十三歲，結束了長達四十七年的統治。慈禧太后臨終之前遺言交待
“此後，女人不可預聞國政。此與本朝家法相違，必須嚴加限制，不得令[太監擅權](../Page/太監.md "wikilink")。[明末之事](../Page/明末.md "wikilink")，可為殷鑑！”
[Cixi_tomb_memorial_tower_2011_11.jpg](https://zh.wikipedia.org/wiki/File:Cixi_tomb_memorial_tower_2011_11.jpg "fig:Cixi_tomb_memorial_tower_2011_11.jpg")
[宣統元年十月四日](../Page/宣統.md "wikilink")（1909年11月16日），將[慈禧太后的靈柩從](../Page/慈禧太后.md "wikilink")[北京](../Page/北京.md "wikilink")[紫禁城遷到](../Page/紫禁城.md "wikilink")[河北省](../Page/河北省.md "wikilink")[遵化市](../Page/遵化市.md "wikilink")[清東陵內的](../Page/清東陵.md "wikilink")[菩陀峪定東陵安葬](../Page/菩陀峪定東陵.md "wikilink")，並將[慈禧太后的](../Page/慈禧太后.md "wikilink")[牌位請入](../Page/牌位.md "wikilink")[北京太廟供奉](../Page/北京太廟.md "wikilink")。定徽號
“**慈禧端佑康頤昭豫莊誠壽恭欽獻崇熙太皇太后**”，諡號
“**孝欽慈禧端佑康頤昭豫莊誠壽恭欽獻崇熙配天興聖顯皇后**”，簡稱“**孝欽顯皇后**”，諡號共22字，諡號長度超過清朝開國皇后[孝慈](../Page/孝慈高皇后_\(清朝\).md "wikilink")、本朝[孝德](../Page/孝德顯皇后.md "wikilink")、[孝貞兩位正宮皇后](../Page/慈安太后.md "wikilink")，為清代及中國歷代皇后之最。

### 身後之事

慈禧的陵寝菩陀峪定东陵，营建工程歷時十三年，直到她死前才告結束。耗银227万两，金碧辉煌、极尽奢华。

建築材料的貴重、工藝的精湛、裝飾的奢華等方面均居於清朝皇后陵寢的首位。即使是與清朝皇陵相比，某些皇陵也要比她遜色很多。她的隨葬品之奢華也令人瞠目結舌，嘆為觀止。慈禧的隨葬品分為兩部分：生前置放於墓中金井裡的珍寶與下葬時的隨葬珍品。

1928年6月，軍閥[孙殿英藉演習之名](../Page/孙殿英.md "wikilink")，率其部下对慈禧的菩陀峪定东陵和[乾隆帝的](../Page/乾隆帝.md "wikilink")[裕陵进行大规模盗掘](../Page/清裕陵.md "wikilink")。盗墓者将定东陵内的珍宝洗劫一空，甚至连慈禧口中所含的一粒大如雞蛋的[夜明珠都被挖走](../Page/夜明珠.md "wikilink")，此案即是轰动全国的“清东陵盗宝案”。在清皇室的呼吁下，民国政府派员调查此事。孙殿英对外宣称是报祖上[孙承宗之仇](../Page/孙承宗.md "wikilink")，并将其中部分盗取的宝物贿赂[宋美龄](../Page/宋美龄.md "wikilink")、[孔祥熙等人](../Page/孔祥熙.md "wikilink")，案件查办最终不了了之。寓居[天津的](../Page/天津.md "wikilink")[溥仪只得派人将挖出的遗骨重新敛葬](../Page/溥仪.md "wikilink")。\[22\]

後來[溥儀在其回憶錄裡提及](../Page/溥仪.md "wikilink")：祖母慈禧太后夜明珠被盜事，並改餽贈給某位民初權貴夫人（指宋美龄），讓他耿耿於懷。

## 身世來源

一般認為慈禧為滿洲鑲藍旗人，玉牒明確記載是“葉赫那拉氏惠徵之女”。

但也有學者也提出了不同的觀點，認為慈禧太后很有可能是漢族人。

1989年6月，[長治市郊區](../Page/长治市.md "wikilink")（原屬長治縣）下秦村77歲的村民趙發旺帶著他和上秦村宋雙花、宋六則、宋德文、宋德武等人的聯名信，找到長治市地方志辦公室。趙發旺說，慈禧是上秦村人。他是慈禧太后的五輩外甥，宋雙花、宋六則等人是慈禧的五輩侄孫。他們要求政府幫助澄情。從此，劉奇踏上了慈禧童年的研究之路。佐證材料的不斷豐富，愈加增強了劉奇的信心，有關著述也頗見報端。2012年4月，在文化部中國藝術研究院主持召開的“共和國社會主義文學藝術五十年研討會”上，劉奇撰寫的《揭開慈禧童年之謎》，獲得一等獎。這篇7000餘字的論文，集中闡述了慈禧的身世問題。

據劉奇考證，1835年，慈禧出生在山西[長治縣西坡村一個貧窮的漢族農民家庭](../Page/長治縣.md "wikilink")，取名“王小慊”。由於家窮、娘死，4歲時，被賣給本縣上秦村宋四元為女，改名“宋齡娥”。12歲時，又被賣給潞安府知府惠徵為婢，惠徵夫人一次無意見到慈禧的雙腳心各長一個貴痣，認為她是大福之人，就收她為養女，改姓葉赫那拉，歸為滿族。咸豐二年（
1852年），以葉赫那拉惠徵之女的身份，應選入宮，平步青雲，直至皇太后。

歷史上關於慈禧是[山西省](../Page/山西省.md "wikilink")[長治縣的漢人是有很多依據](../Page/长治县.md "wikilink")：

1.  第一，滿族女人是不[裹腳的](../Page/缠足.md "wikilink")，但慈禧是個小腳女人。滿族正黃旗後代佟女士說她小時候，常聽家裡的老人說，慈禧的腳是纏過又放開的那種，不是滿族人的「天足」。當年曾被慈禧召見過的前清京官曹春圃說，當時他跪在地上迎接，「老佛爺由太監李蓮英和一位宮女攙扶著由屏風後面走出來，穿元寶鞋，走路一搗一搗的，是一雙『半落子』腳」（纏過又放掉的小腳）。
2.  第二，慈禧不認識滿文，批改奏摺基本都是用漢文。慈禧集大權於一身，要負責批閱奏摺。慈禧的御前女官[裕德齡在](../Page/裕德齡.md "wikilink")《[御香縹緲錄](../Page/御香縹緲錄.md "wikilink")》里說，「老佛爺對於滿文实在是認識得很少，少到差不多可以說完全不認識。」也許如果慈禧真是滿族人，怎麼會不認識滿族的文字？而且滿人[檀林著](../Page/檀林.md "wikilink")《圓明園秘聞》說，慈禧「壓根就不是一個旗人，是個窮漢人家的小媳婦，被賣給有錢有勢的旗人老爺，趕上這次選秀，就被人頂了槓」。
3.  第三，慈禧與其惠徵夫人不親近。燕北老人著《[清代十三朝宮闈秘史](../Page/清代十三朝宮闈秘史.md "wikilink")》記載，慈禧極不尊敬母親惠徵夫人。該書寫道：“慈禧及為太后，每見其母，輒曰：『母向言女為賠奩物，今竟何如？』母甚怒，而不敢言。故事，太后母人官必行大禮，多不敢受者，否亦必側身避之。慈禧獨端坐受焉，母恨之。母喜淡素惡花。每入宮，慈禧輒為簪於頭，頭上花嘗滿。母大恚，後遂不入”。
4.  第四，慈禧對待母家的態度。她被封為太后後，雖權傾天下，卻獨獨不富母家。她的侄子和兩個弟弟窮到不能為生。
5.  第五，上秦村宋家有祖傳的慈禧當年寄給的單身照片，以及慈禧家書殘信，殘信說的都是皇家之事與同治帝年幼近況，字跡對比與慈禧本人一致。
6.  第六，上秦村宋家有祖傳的慈禧當年寄給的光緒年間清廷特製皮夾式清朝帝后宗祀譜（簡稱“皮夾子”）。
7.  第七，徐錫山所撰《慈禧出身漢人的幾點補充理由》一文說，當年其伯祖父徐岫青老先生聽「前清京官曹春圃」說，「老佛爺說話不是京腔，帶山西老『侉子』口音」。
8.  第八，慈禧愛吃[長治人常吃](../Page/长治.md "wikilink")、愛吃的食品（如窩頭、團子、小米粥、玉米糝粥、蘿蔔、沁州黃小米、壺關醋、襄垣黑醬等），還專門請長治人作御廚。
9.  第九，慈禧愛看流傳不廣，僅是長治本地人才能聽懂的長治地方戲劇[上黨梆子等](../Page/上党梆子.md "wikilink")。
10. 第十，關心長治百姓和地方建設，對他們特別關照。她用的奶媽是長治七里坡村韓印則的二老奶奶’她的御廚是長治小常村的陳四孩；她安排長治史家莊村原殿鰲擔任御前侍衛；後原殿鰲觸犯刑律，本當處斬，但太后念他是同鄉，不僅赦免了他，還讓他到江西做官。對長治地方官和山西商人，慈禧也特別關照，光緒二十六年八國聯軍打進北京，慈禧和光緒帝逃至大同，留住三天，兵荒馬亂中，慈禧仍不忘召見潞安知府許涵度，並「擢冀寧道」。
11. 第十一，西坡村王家記有慈禧出生及賣入宋家、葉赫那拉家後入宮的《家譜》與上秦村宋家的家書殘信不謀而合。
12. 第十二，給慈禧畫像的美國畫家卡爾女士在其《慈禧寫照記》中寫道：“外間傳述，為慈禧家世極為卑賤，初僅為他家使女，厥後始遷入大內，登寶位焉。”
13. 第十三，從光緒二十八年[內務府](../Page/內務府.md "wikilink")《[差務雜錄](../Page/差務雜錄.md "wikilink")》檔得知，慈禧祭典她父母的下款程式中，稱其父惠徵為'先考惠二太爺'，稱其母為'先妣惠二老太太'。”由此可見，惠徵夫婦並非慈禧的生身父母。這些現象表明，慈禧小時候得不到惠徵夫婦善待的原因在於慈禧不是惠徵夫婦的親生女兒。

## 傳說軼事

  - 傳闻清末重臣[榮祿少年時代](../Page/榮祿.md "wikilink")，與選秀入宮前之慈禧為情侶，故當慈禧成為皇太后並掌權之後，對於[榮祿大力提拔](../Page/荣禄.md "wikilink")，寵信有加。此說，見於前清宮中女官[裕德齡原以英文出版](../Page/裕德齡.md "wikilink")、亦發行有中文版之小說體作品《愛戀紫禁城：慈禧私秘感情生活》，亦為[台灣](../Page/台灣.md "wikilink")[中視電視連續劇](../Page/中視.md "wikilink")《[戲說慈禧](../Page/戲說慈禧.md "wikilink")》所採用。然而此说并无任何实际依据、与当时的社会常理相悖并且不符合逻辑。实际上荣禄为人非常能干而且主张改革，所以为慈禧太后重用。
  - 《[清史稿](../Page/清史稿.md "wikilink")》記載，慈禧去世時仍然在批摺子，可見她的勤政。[-{涂}-良軍推薦的古裝劇](../Page/涂良軍.md "wikilink")《[戲說慈禧](../Page/戲說慈禧.md "wikilink")》亦如實拍攝。
  - [大英帝国外交官](../Page/大英帝国.md "wikilink")[埃蒙德·特拉内·巴恪思爵士曾出使清朝](../Page/埃德蒙·巴恪思爵士，第二代從男爵.md "wikilink")，據其著作《[太后与我](../Page/太后与我.md "wikilink")》所載，慈禧在满五十岁之前，嗜好[房事](../Page/房事.md "wikilink")，数度祕密地招幸外交官，一夜能行房五次。\[23\]然而該著作被指内容极为离奇荒诞，对于該书的指责，主要集中于两点，一是「不实」，一是「[色情](../Page/色情.md "wikilink")」，其文学价值大于史料价值。\[24\]
  - 有關光绪帝之死因传闻与慈禧有关联，其遗骨及衣物经现代法医技术鉴定后，確認死于急性[砷](../Page/砷.md "wikilink")（[砒霜](../Page/砒霜.md "wikilink")）中毒，凶手极有可能为慈禧。\[25\]主因是慈禧病重時，曾猶豫對光緒帝要如何處置，遂以自己不久人世的消息透露給光緒帝知道，惟其近侍回報，帝曾微露喜色，故慈禧決意自己病終前，帝須先於自己命終，以免皇帝有再度親政、否定慈禧生前之佈局的可能。
  - 民间传说称，叶赫那拉氏先祖[布扬古被努尔哈赤杀害前诅咒到](../Page/布扬古.md "wikilink")：“吾子孙虽存一女子，亦必覆满洲”，巧合的是清朝最后两位太后——慈禧太后和[隆裕太后均为叶赫那拉氏](../Page/隆裕太后.md "wikilink")，而且一个被认为治国无能导致国力日衰，另一个在位时清朝灭亡。然此说法最早在清[光绪年间才出现](../Page/光绪.md "wikilink")，且从未出现在之前的史料当中，可能为后人穿凿附会之说。

## 家族

<center>

</center>

### 兄弟姊妹

  - 姊 夭折
  - 妹
    [醇親王嫡福晋](../Page/葉赫那拉氏_\(奕譞嫡福晋\).md "wikilink")（醇親王[奕譞嫡妃](../Page/奕譞.md "wikilink")）
      - 第一子 [載瀚](../Page/載瀚.md "wikilink")
      - 第二子
        [載湉](../Page/載湉.md "wikilink")（[光緒帝](../Page/光緒帝.md "wikilink")）
      - 第三子
      - 第四子 [載洸](../Page/載洸.md "wikilink")
  - 妹
    （[慶親王](../Page/慶親王.md "wikilink")[奕劻二弟](../Page/奕劻.md "wikilink")[奕勛嫡妻](../Page/奕勛.md "wikilink")）
  - 大弟 [照祥](../Page/照祥.md "wikilink")
      - 第一子 [德善](../Page/德善.md "wikilink")
  - 二弟 [桂祥](../Page/桂祥.md "wikilink")
      - 第一女 [靜榮](../Page/靜榮.md "wikilink")（載澤福晉）
      - 第二女 [靜芬](../Page/隆裕太后.md "wikilink")（孝定景皇后）
      - 第三女 [靜芳](../Page/靜芳.md "wikilink")
      - 第一子 [德恒](../Page/德恒.md "wikilink")，字「健亭」
          - 第一女 [淑敏](../Page/淑敏.md "wikilink")
          - 第二女 [淑琴](../Page/淑琴.md "wikilink")
          - 第一子 [恩賢](../Page/恩賢.md "wikilink")
      - 第二子 [德祺](../Page/德祺.md "wikilink")，字「壽芝」
          - 第一女 [希賢](../Page/希賢.md "wikilink")
          - 第二女 [希嬿](../Page/希嬿.md "wikilink")
          - 第一子 [恩印](../Page/恩印.md "wikilink")
          - 第二子 [恩顯](../Page/恩顯.md "wikilink")
          - 第三子 [恩民](../Page/恩民.md "wikilink")
          - 第四子 [恩植](../Page/恩植.md "wikilink")
  - 三弟 [福祥](../Page/福祥.md "wikilink")
      - 第一子 [德奎](../Page/德奎.md "wikilink")，字「文伯」
          - 第一子 [恩華](../Page/恩華.md "wikilink")
          - 第二女 [恩秀](../Page/恩秀.md "wikilink")
          - 第一子 [恩銓](../Page/恩銓.md "wikilink")
          - 第二子 [恩輝](../Page/恩輝.md "wikilink")
          - 第三子 [恩耀](../Page/恩耀.md "wikilink")
          - 第四子 [恩光](../Page/恩光.md "wikilink")

## 字画

<center>

<File:Painting> by Dowager Empress Cixi 02.jpg|*粉色牡丹* <File:Painting> by
Empress Dowager Cixi.PNG|*牡丹* <File:Painting> by the Empress Dowager
Cixi 06.JPG|*鸟与水果* <File:Painting> by the Empress Dowager Cixi
04.JPG|*黄牡丹* <File:Calligraphy> of Empress Dowager Cixi at Summer
Palace.JPG|慈禧手书

</center>

## 影視作品

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>作品</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/唐若菁.md" title="wikilink">唐若菁</a></p></td>
<td><p>《<a href="../Page/清宫秘史.md" title="wikilink">清宫秘史</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李湄.md" title="wikilink">李湄</a></p></td>
<td><p>《<a href="../Page/西太后與珍妃.md" title="wikilink">西太后與珍妃</a>》（1964年電影）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁舜燕.md" title="wikilink">梁舜燕</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盧燕.md" title="wikilink">盧燕</a></p></td>
<td><p>《<a href="../Page/末代皇帝_(電影).md" title="wikilink">末代皇帝</a>》、《<a href="../Page/倾国倾城.md" title="wikilink">倾国倾城</a>》、《<a href="../Page/瀛台泣血.md" title="wikilink">瀛台泣血</a>》、《德齡與慈禧》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李麗華.md" title="wikilink">李麗華</a></p></td>
<td><p>《八國聯軍》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吴韻.md" title="wikilink">吴韻</a></p></td>
<td><p>《夢图的清宫》、《夢图的清宫II》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張冰玉.md" title="wikilink">張冰玉</a></p></td>
<td><p>《<a href="../Page/清宮殘夢_(台視電視劇).md" title="wikilink">清宮殘夢</a>》（1970年電視劇）、《<a href="../Page/末代兒女情.md" title="wikilink">末代兒女情</a>》（1990年電視劇）、《<a href="../Page/末代皇孫.md" title="wikilink">末代皇孫</a>》（1991年電視劇）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳莎莉.md" title="wikilink">陳莎莉</a></p></td>
<td><p>《庚子风云》《欢喜游龙》《雙印傳奇》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡茵夢.md" title="wikilink">胡茵夢</a></p></td>
<td><p>《慈禧外传》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯琴高娃.md" title="wikilink">斯琴高娃</a></p></td>
<td><p>《沧海英雄》《慈禧秘传》 《嫁到宫里的男人》《钱王》《日落紫禁城》《台湾首任巡抚刘铭传》《太后吉祥》《无盐女》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a></p></td>
<td><p>《<a href="../Page/慈禧秘密生活.md" title="wikilink">慈禧秘密生活</a>》（1995年電影）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李明啟.md" title="wikilink">李明啟</a></p></td>
<td><p>《厨子当官》《蓝色妖姬》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/呂中.md" title="wikilink">呂中</a></p></td>
<td><p>《<a href="../Page/走向共和.md" title="wikilink">走向共和</a>》、《<a href="../Page/德齡公主.md" title="wikilink">德齡公主</a>》、《茶馆》《定军山》《槍炮侯》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/田中裕子.md" title="wikilink">田中裕子</a></p></td>
<td><p>《<a href="../Page/蒼穹之昴_(電視劇).md" title="wikilink">蒼穹之昴</a>》（2010年電視劇）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭少芸.md" title="wikilink">郭少芸</a></p></td>
<td><p>《<a href="../Page/状王宋世杰.md" title="wikilink">状王宋世杰</a>》、《<a href="../Page/狀王宋世傑（貳）.md" title="wikilink">狀王宋世傑（貳）</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉曉慶.md" title="wikilink">劉曉慶</a></p></td>
<td><p>《<a href="../Page/火烧圆明园.md" title="wikilink">火烧圆明园</a>》、《<a href="../Page/垂簾聽政_(電影).md" title="wikilink">垂簾聽政</a>》（1983年電影）、《<a href="../Page/西太后_(电影).md" title="wikilink">西太后</a>》（又名「<a href="../Page/一代妖后.md" title="wikilink">一代妖后</a>」，1988年電影）、《大太监李莲英》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁立.md" title="wikilink">袁立</a></p></td>
<td><p>《一生为奴》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉雪華.md" title="wikilink">劉雪華</a></p></td>
<td><p>《少女慈禧》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/叢珊.md" title="wikilink">叢珊</a></p></td>
<td><p>《<a href="../Page/戲說慈禧.md" title="wikilink">戲說慈禧</a>》（1993年電視劇）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳潔儀.md" title="wikilink">陳潔儀</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柳影虹.md" title="wikilink">柳影虹</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓馬利.md" title="wikilink">韓馬利</a></p></td>
<td><p>《太平天國》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/米雪.md" title="wikilink">米雪</a></p></td>
<td><p>《<a href="../Page/滿清十三皇朝.md" title="wikilink">滿清十三皇朝之血染紫禁城</a>》、《<a href="../Page/大太監.md" title="wikilink">大太監</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮寶寶.md" title="wikilink">馮寶寶</a></p></td>
<td><p>《冒牌皇帝》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐陽珮珊.md" title="wikilink">歐陽珮珊</a></p></td>
<td><p>《滿清十三皇朝之危城争霸》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/程可為.md" title="wikilink">程可為</a></p></td>
<td><p>《匯通天下》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳志森.md" title="wikilink">吳志森</a></p></td>
<td><p>《頭條新聞》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/汪明荃.md" title="wikilink">汪明荃</a></p></td>
<td><p>無數的舞台劇</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陶虹.md" title="wikilink">陶虹</a></p></td>
<td><p>《一帘幽梦》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/田中裕子.md" title="wikilink">田中裕子</a></p></td>
<td><p>《苍穹之昴》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/方舒.md" title="wikilink">方舒</a></p></td>
<td><p>《两宫皇太后》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洪欣.md" title="wikilink">洪欣</a></p></td>
<td><p>《最后的格格》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马羚.md" title="wikilink">马羚</a></p></td>
<td><p>《天下第一丑》《豪门金枝》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吕丽萍.md" title="wikilink">吕丽萍</a></p></td>
<td><p>《十三格格》《大栅栏》《一八九四·甲午大海战》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/谢芳.md" title="wikilink">谢芳</a></p></td>
<td><p>《壮士出征》《总督张之洞》《御花子》《马江海战》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁小冰.md" title="wikilink">梁小冰</a></p></td>
<td><p>《清宫气数录》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盖丽丽.md" title="wikilink">盖丽丽</a></p></td>
<td><p>《太平天国》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱媛媛.md" title="wikilink">朱媛媛</a></p></td>
<td><p>《十三格格新传》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宋佳_(1962年出生).md" title="wikilink">宋佳</a></p></td>
<td><p>《胡雪岩》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱紫汶.md" title="wikilink">朱紫汶</a></p></td>
<td><p>《红墙绿瓦之残阳》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾丽娅.md" title="wikilink">艾丽娅</a></p></td>
<td><p>《大龙脉》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邓婕.md" title="wikilink">邓婕</a></p></td>
<td><p>《慈禧西行》《龙非龙，凤非凤》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韩再芬.md" title="wikilink">韩再芬</a></p></td>
<td><p>《船政风云》《大龙邮票》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凯丽.md" title="wikilink">凯丽</a></p></td>
<td><p>《皇亲国戚》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/程可為.md" title="wikilink">程可為</a><br />
<a href="../Page/康華.md" title="wikilink">康華</a>(年輕)</p></td>
<td><p>《<a href="../Page/滙通天下_(電視劇).md" title="wikilink">汇通天下</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李滨_(女演员).md" title="wikilink">李滨</a></p></td>
<td><p>《西洋镜》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李丽凤.md" title="wikilink">李丽凤</a></p></td>
<td><p>《<a href="../Page/吾土吾民.md" title="wikilink">吾土吾民</a>》（1981年電視劇）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刘慧玲.md" title="wikilink">刘慧玲</a></p></td>
<td><p>《皇帝保重》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柳格格.md" title="wikilink">柳格格</a></p></td>
<td><p>《粉墨王侯》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/潘虹.md" title="wikilink">潘虹</a></p></td>
<td><p>《<a href="../Page/臺灣1895.md" title="wikilink">臺灣1895</a>》（2007年電視劇）、《绣娘兰馨》、《<a href="../Page/楊乃武與小白菜.md" title="wikilink">楊乃武與小白菜</a>》（2006年電視劇）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/庞敏.md" title="wikilink">庞敏</a></p></td>
<td><p>《银鼠》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宋春丽.md" title="wikilink">宋春丽</a></p></td>
<td><p>《甲午陆战》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宋晓英.md" title="wikilink">宋晓英</a></p></td>
<td><p>《神医喜来乐》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王莱.md" title="wikilink">王莱</a></p></td>
<td><p>《<a href="../Page/賽金花.md" title="wikilink">賽金花</a>》（1992年電視劇）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王玉梅.md" title="wikilink">王玉梅</a></p></td>
<td><p>《谭嗣同》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奚美娟.md" title="wikilink">奚美娟</a></p></td>
<td><p>《大清药王》《孙中山》《<a href="../Page/那年花開月正圓.md" title="wikilink">那年花開月正圓</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁玫.md" title="wikilink">袁玫</a></p></td>
<td><p>《北洋水师》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵奎娥.md" title="wikilink">赵奎娥</a></p></td>
<td><p>《大宅门》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郑振瑶.md" title="wikilink">郑振瑶</a></p></td>
<td><p>《大阿哥溥峻》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱琳_(演员).md" title="wikilink">朱琳</a></p></td>
<td><p>《末代皇帝》《夜盗珍妃墓》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马丽.md" title="wikilink">马丽</a></p></td>
<td><p>《岭南药侠》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刘广宁.md" title="wikilink">刘广宁</a></p></td>
<td><p>《十月围城》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/简沛恩.md" title="wikilink">简沛恩</a></p></td>
<td><p>《<a href="../Page/女人花_(2012年电视剧).md" title="wikilink">女人花</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇丹丹.md" title="wikilink">蘇丹丹</a></p></td>
<td><p>《<a href="../Page/亂世豪門.md" title="wikilink">亂世豪門</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘佳_(1960年).md" title="wikilink">刘佳</a></p></td>
<td><p>《<a href="../Page/独有英雄.md" title="wikilink">独有英雄</a>》</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《<a href="../Page/公公出宮.md" title="wikilink">公公出宮</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅蘭_(香港).md" title="wikilink">羅蘭</a></p></td>
<td><p>《<a href="../Page/末代御醫.md" title="wikilink">末代御醫</a>》</p></td>
</tr>
</tbody>
</table>

## 参考文献

### 引用

### 来源

  - 書籍

<!-- end list -->

  - 《[清史稿](../Page/清史稿.md "wikilink")·卷二百十四·孝欽顯皇后》
  - 《御香缥缈录》，裕德齡，美國：紐約陶德曼圖書公司，1933
  - 《西太后》，俞炳坤，紫禁城出版社
  - 《晚清七十年》系列，唐德剛，遠流出版社
  - 《原来慈禧》，张研，重庆出版集团 重庆出版社
  - 《我在慈禧身邊的日子：宮女谈往錄》，金易、沈義羚 著，台北：長樂文化／智庫股份有限公司，2001年 ISBN 957-0484-42-X
  - 《力挽狂瀾－戊戌政變新探》，雷家聖 著，台北：萬卷樓，2004年12月 ISBN 957-739-507-4
  - 《失落的真相－晚清戊戌政變史事新探》，雷家聖 著，台北：五南圖書公司，2016年9月 ISBN 9789571188119
  - 《我所知道的慈禧太后》，葉赫那拉·根正、郝曉輝 著，臺中市：好讀，2005年
  - 《慈禧》，張戎，倫敦，麥田出版社，2015年
  - 《慈禧長治說見聞實錄》，張培禮
  - 《慈禧童年》、《慈禧童年續編》、《慈禧童年考》慈禧童年研究會
  - 《慈禧身世》，劉奇，中國社會出版社出版，2008年

<!-- end list -->

  - 論文

<!-- end list -->

  - 劉奇《揭開慈禧童年之謎》

## 外部連結

  - [唐德剛](../Page/唐德剛.md "wikilink")：[〈慈禧太后和她的頤和園〉](http://www.aisixiang.com/data/61667.html)（1994）
  - [汪榮祖](../Page/汪榮祖.md "wikilink")：〈[記憶與歷史：葉赫那拉氏個案論述](http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_65.pdf)〉
  - [梁元生](../Page/梁元生.md "wikilink")：〈[為慈禧立傳的兩枝曲筆](http://www.cuhk.edu.hk/ics/21c/media/articles/c016-199203006.pdf)〉
  - [〈張戎為慈禧翻案〉](http://www.voacantonese.com/content/jung-chang-20140122/1835172.html)

{{-}}      |-    |-

[Category:清文宗皇后](../Category/清文宗皇后.md "wikilink")
[Category:清朝追封皇后](../Category/清朝追封皇后.md "wikilink")
[Category:清朝皇太后](../Category/清朝皇太后.md "wikilink")
[Category:清朝太皇太后](../Category/清朝太皇太后.md "wikilink")
[Category:甲午戰爭人物](../Category/甲午戰爭人物.md "wikilink")
[Category:中法戰爭人物](../Category/中法戰爭人物.md "wikilink")
[Category:洋務運動領袖](../Category/洋務運動領袖.md "wikilink")
[Category:满洲镶黄旗人](../Category/满洲镶黄旗人.md "wikilink")
[Category:叶赫那拉氏](../Category/叶赫那拉氏.md "wikilink")
[Category:中国女性摄政者](../Category/中国女性摄政者.md "wikilink")
[Category:墓穴遭搗毀者](../Category/墓穴遭搗毀者.md "wikilink")

1.  《漢滿詞典》，劉厚生、李樂營等主編，民族出版社，2005年1月出版，640頁。（ISBN 7-105-06386-6）

2.  《清史稿·卷二百十四·孝钦显皇后》：“[孝欽显皇后叶赫那拉氏，安徽徽宁池太广道惠征女。咸丰元年，后被选入宫，号蘭贵人。四年，封懿嫔。六年三月庚辰，穆宗生，进懿妃……（光绪三十四年十月）甲戌，太后崩，年七十四，葬定陵隆福寺。](http://www.angelibrary.com/oldies/qsg/214.htm)”

3.  清代《[玉牒](../Page/玉牒.md "wikilink")》記載，慈禧太后是「葉赫那拉氏，安徽徽寧池太廣道惠徵女」。

4.  武則天自660年起臨朝聽政，但[唐高宗仍擁有一定的政治權力](../Page/唐高宗.md "wikilink")，二人地位平等，直至唐高宗683年去世，这时期相当于慈禧太后掌权时间前期与慈安太后的[兩宮聽政](../Page/兩宮聽政.md "wikilink")；武則天經廢立[唐中宗](../Page/唐中宗.md "wikilink")、[唐睿宗二帝](../Page/唐睿宗.md "wikilink")，直接掌權及稱帝，到705年退位時，共在位二十二年，連同唐高宗時臨朝聽政，共在位四十五年。

5.  自前195年[漢高祖去世後臨朝聽政](../Page/漢高祖.md "wikilink")，操縱廢立皇帝，至前180年去世，共在位十五年。

6.  中國第一歷史檔案館藏《宮中雜件》：五月初九日，蘭貴人下新進宮女子四名，麗貴人下新進宮女子四名，五月十一日春貴人下新進宮女子四名，婉常在下新進宮女子四名

7.  《清實錄·文宗實錄·卷之一百五十二》（咸豐四年十一月）○庚寅。上詣......○命協辦大學士賈楨為正使、禮部左侍郎肅順為副使、持節齎冊。晉封懿貴人那拉氏為懿嬪。冊文曰。朕維椒庭翊化.....

8.  中國第一歷史檔案館藏《宮中雜件》：咸豐四年二月二十六日敬事房太監王瑞傳旨：蘭貴人封為懿嬪，為此特記，伊貴人下因病退出女子一名。咸豐四年十二月二十四日敬事房太監張信傳旨：麗貴人封為麗嬪，蘭貴人封為懿嬪，婉貴人封為婉嬪，玫常在封為玫貴人

9.  《[宋史](../Page/宋史.md "wikilink")·樂志》：「不涸不童，誕降祺祥」，

10. 雷家聖《失落的真相：晚清戊戌政變史事新探》，台北:五南出版社，2016年。

11. [Timothy Richard ,*Forty-five years in China*, Chapter
    12.](http://archive.org/details/fourtyfiveyears00richuoft)

12. 楊崇伊〈掌廣西道監察御史楊崇伊摺〉，《戊戌變法檔案史料》，北京中華書局，1959，p.461.

13. 楊深秀〈山東道監察御史楊深秀摺〉，《戊戌變法檔案史料》，北京中華書局，1959，p.15.

14.
15.
16.
17.
18. 次日朝罢，荣相请独对，问太后曰：传闻将有废立事，信乎？太后曰：无有也。事果可行乎？荣曰：太后行之，谁敢谓其不可行者。（《崇陵传信录》）

19. 荣曰：上春秋已盛，无皇子，不如择宗室近支子建为大阿哥为上嗣，兼祧穆宗，育之宫中，徐承大统，则此举为有名矣。太后沉吟久之曰：汝言是也。（《崇陵传信录》）

20. 荣曰：太后行之谁敢谓其不可者？顾上罪不明，外国公使将起而干涉，此不可不惧也。（《崇陵传信录》）

21.

22.

23.

24.

25. 主要见于《[清室外记](../Page/清室外记.md "wikilink")》、《[清稗类抄](../Page/清稗类抄.md "wikilink")》和《[崇陵传信录](../Page/崇陵传信录.md "wikilink")》。