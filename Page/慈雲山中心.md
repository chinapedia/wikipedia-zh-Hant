[Tsz_Wan_Shan_Shopping_Centre_6F.jpg](https://zh.wikipedia.org/wiki/File:Tsz_Wan_Shan_Shopping_Centre_6F.jpg "fig:Tsz_Wan_Shan_Shopping_Centre_6F.jpg")
[Tsz_Wan_Shan_Shopping_Centre_central_atrium.JPG](https://zh.wikipedia.org/wiki/File:Tsz_Wan_Shan_Shopping_Centre_central_atrium.JPG "fig:Tsz_Wan_Shan_Shopping_Centre_central_atrium.JPG")
[Tsz_Wan_Shan_Shopping_Centre_Level_5.jpg](https://zh.wikipedia.org/wiki/File:Tsz_Wan_Shan_Shopping_Centre_Level_5.jpg "fig:Tsz_Wan_Shan_Shopping_Centre_Level_5.jpg")
[FRESHIN.jpg](https://zh.wikipedia.org/wiki/File:FRESHIN.jpg "fig:FRESHIN.jpg")
[Tsz_Wan_Shan_Shopping_Centre_2.jpg](https://zh.wikipedia.org/wiki/File:Tsz_Wan_Shan_Shopping_Centre_2.jpg "fig:Tsz_Wan_Shan_Shopping_Centre_2.jpg")
[Tsz_Wan_Shan_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Tsz_Wan_Shan_Shopping_Centre.jpg "fig:Tsz_Wan_Shan_Shopping_Centre.jpg")
[HK_TszWanShanShoppingCentre.JPG](https://zh.wikipedia.org/wiki/File:HK_TszWanShanShoppingCentre.JPG "fig:HK_TszWanShanShoppingCentre.JPG")

**慈雲山中心**（**Tsz Wan Shan Shopping
Centre**）是[香港一個中小型的購物中心](../Page/香港.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[黃大仙](../Page/黃大仙.md "wikilink")[慈雲山](../Page/慈雲山.md "wikilink")[毓華街](../Page/毓華街.md "wikilink")23號，為慈雲山最大型的購物中心，於1997年落成，由房屋署總建築師（4）設計，現為[領展旗下](../Page/領展房地產投資信託基金.md "wikilink")。

## 簡介

慈雲山中心前身是[慈安邨第](../Page/慈安邨.md "wikilink")57座（安欣樓）、第58座（安基樓）、第59座（安宜樓）及第60座（安家樓）原址，在「整體重建計劃」中，該邨的舊型徙廈在1992年清拆重建，1997年重建成慈雲山中心。
慈雲山中心樓高七層，建築樓面面積約36,700平方米，商用面積約為21,000平方米。慈雲山中心位於[慈雲山區中央地段](../Page/慈雲山.md "wikilink")，是區內居民的生活匯聚點，區內屋苑林立，除了服務區內居民，慈雲山中心亦為[黃大仙](../Page/黃大仙.md "wikilink")、[鑽石山等附近一帶逾十萬居民提供購物消閒設施](../Page/鑽石山.md "wikilink")。慈雲山中心有兩個分別位於毓華街及惠華街的主要出入口，位於惠華街的出入口連接有蓋巴士總站、小巴站、的士站，於毓華街方向設有兩層停車場。慈雲山中心設計橫跨多個不同高度的平台，居民可利用貫通[慈雲山各屋邨和屋苑的有蓋行人通道及天橋網絡到慈雲山中心及作上落山之用](../Page/慈雲山.md "wikilink")。

慈雲山中心更曾為房委會旗下表現最好的商場之一。房委會出售資產後，是[領展](../Page/領展.md "wikilink")（前稱領匯）幾個重點發展商場的其中一個\[1\]。[領展接手後進行改革](../Page/領展.md "wikilink")，以引入大型[連鎖店來提升競爭力](../Page/連鎖店.md "wikilink")，增加租金收益，據統計連鎖店比例就高達74%。2007年中，慈雲山中心的鳳德樓海鮮酒家不獲續租而結業\[2\]\[3\]，引起領展與商戶的糾紛\[4\]。民間組織[領匯監察就批評](../Page/領匯監察.md "wikilink")[領展藉詞重新發展](../Page/領展.md "wikilink")、改善商場，企圖令小商戶被迫搬遷或結束營業，例子見[領展拒絕承諾目前的商舖可在商場翻新工程後繼續經營](../Page/領展.md "wikilink")。惟由於鳳德樓主要股東同時收購慈雲山中心七樓的酒樓，在壟斷下令食物質素越來越差，因此當區居民普遍歡迎領展（前稱領匯）不與該酒樓續約\[5\]。

商場改善工程於2005年起展開，提升工程把2004年[吉之島百貨遷出後空置的五樓及六樓的樓面轉變為](../Page/吉之島.md "wikilink")「購物街」購物環境，為顧客帶來更多購物選擇。最近引入的新商戶，包括著名的書店及多間特色食肆，以及將七樓的商場管理處及[慈民邨管理處遷出後騰空的面積改建成的商舖及匯診中心](../Page/慈民邨.md "wikilink")，預料能進一步提升商場較高樓層的顧客流量。這些工程也有助帶動其他現有租戶改善舖面設計、服務質素與產品種類。

另外，往日顧客要在巴士總站前去慈雲山中心較高樓層，只可從惠華街商場三、四樓入口進入，再在商場內接駁電梯。現時新建的電梯由[慈雲山（中）巴士總站直達商場五樓](../Page/慈雲山（中）巴士總站.md "wikilink")，有如「飲管」，把顧客「引進」商場，電梯每日的使用量高達過萬人次，有效增加人流\[6\]。

2017年，商場五樓惠康超級市場結業，一樓百佳超級市場搬到五樓惠康超市原址，而四樓裕滿人家及鄰近的店舖結業。一樓和四樓兩間大舖被拆成數間面積較小的店舖，並於2018年1月起陸續開業，其中四樓包括曾在鄰近位置開業的[肯德基](../Page/肯德基.md "wikilink")，一樓亦包括曾在商場外（但屬商場範圍）的[七十一便利店](../Page/七十一便利店.md "wikilink")。

## 顧客服務

  - 免費上網（Wi-Fi）服務
  - 失物認領
  - 簡單救傷設施
  - 小型公具借用
  - 影印服務
  - 地圖借用
  - 雨傘借用
  - 簡便縫補針線借用
  - 手提電話電池充電服務
  - 汽車充電線借用
  - 本地傳真服務

## 商戶

慈雲山中心設有大大少少不同的店鋪以滿足居民日常生活, 主要較著名的商店包括:

食肆：百匯軒、翠河餐廳、[薩莉亞](../Page/薩莉亞.md "wikilink")、[椰林閣餐廳](../Page/椰林閣餐廳.md "wikilink")、米線陣、[味千拉麵](../Page/味千拉麵.md "wikilink")(已結業)、手作之店、[大家樂](../Page/大家樂.md "wikilink")、[肯德基](../Page/肯德基.md "wikilink")、101台式滷肉飯專門店、[定食8](../Page/爭鮮.md "wikilink")、裕滿人家<sup>註1</sup>（已結業）、[大快活](../Page/大快活.md "wikilink")、[吉野家](../Page/吉野家.md "wikilink")、[太興燒味](../Page/太興燒味.md "wikilink")、[麥當勞餐廳](../Page/麥當勞餐廳.md "wikilink")、[爭鮮外帶壽司](../Page/爭鮮.md "wikilink")、PHD（[必勝客旗下](../Page/必勝客.md "wikilink")）、[聖安娜餅屋](../Page/聖安娜餅屋.md "wikilink")、[元氣壽司](../Page/元氣壽司.md "wikilink")、[譚仔三哥雲南米線](../Page/譚仔雲南米線.md "wikilink")、[美心西餅](../Page/美心西餅.md "wikilink")、[八方雲集](../Page/八方雲集.md "wikilink")、桃園制作粥麵餐廳等

超級市場及便利店：[百佳超級市場](../Page/百佳超級市場.md "wikilink")、[大昌食品專門店](../Page/大昌食品市場.md "wikilink")（已結業）、優品360、盞記、[7-11便利店](../Page/7-11便利店.md "wikilink")、Vango便利店、[759阿信屋](../Page/759阿信屋.md "wikilink")、零食皇、HKTV
mall、[惠康超級市場](../Page/惠康超級市場.md "wikilink")<sup>註1</sup>（已結業）

電器產品：[豐澤電器](../Page/豐澤電器.md "wikilink")、[DSC德爾斯](../Page/DSC德爾斯.md "wikilink")（全線已結業）

生活產品：[屈臣氏](../Page/屈臣氏.md "wikilink")、[萬寧](../Page/萬寧.md "wikilink")、[位元堂](../Page/位元堂.md "wikilink")、[華潤堂](../Page/華潤堂.md "wikilink")、[Living
PLAZA by
AEON](../Page/永旺.md "wikilink")、[名創優品](../Page/名創優品.md "wikilink")、[彩豐行](../Page/彩豐行.md "wikilink")

服裝：[馬拉松](../Page/馬拉松.md "wikilink")、[Baleno](../Page/Baleno.md "wikilink")、DR.
KONG、M3
SPORTS、GITTI、[眼鏡88](../Page/眼鏡88.md "wikilink")<sup>註1</sup>（已結業）

消閒：[美國冒險樂園](../Page/美國冒險樂園.md "wikilink")、[三聯書店](../Page/三聯書店.md "wikilink")（已結業）

<small>註1：因商場翻新工程而結業</small>

Tsz Wan Shan Shopping Centre Ground Level.jpg|地下 Tsz Wan Shan Shopping
Centre Level 1.jpg|1樓 Tsz Wan Shan Shopping Centre Level 2.jpg|2樓 Tsz
Wan Shan Shopping Centre Level 3.jpg|3樓 Tsz Wan Shan Shopping Centre
Level 4.jpg|4樓 Tsz Wan Shan Shopping Centre south atrium.JPG|5樓（2015年）
Tsz Wan Shan Shopping Centre Level 6.jpg|6樓 Tsz Wan Shan Shopping Centre
Level 7.jpg|7樓

## 街市

2018年4月1日，領展將慈雲山中心一樓街市（好運街市）由好眼光轉予建華（街市）管理有限公司後而進行翻新，令居民相當不便外，也擔心物價會壟斷。\[7\]居民和地區人士收集超過3000個居民的簽名和投票，要求政府在該區多建一個公營街市。惟政府及相關部門未有回應。\[8\]

翻新後街市於6月23日重開，改名為「慈雲山市場FRESHIN」，大部分為建華的新商舖，只有少量舊商舖留底。而二樓街市在8月31日清場，將改建為商舖。地區組織慈雲山街市居民關注組、監察公營街市發展聯盟、慈雲山建設力量、黃大仙區區議員黃逸旭及民主黨發起為街市舉辦悼念活動，為其劃上完滿句號。\[9\]

FRESHIN Entrance.jpg|街市入口 FRESHIN (2).jpg|海產、冰鮮雞及牛肉檔 FRESHIN
(3).jpg|菜及[燒味檔](../Page/燒味.md "wikilink")、蔘茸[海味店](../Page/海味.md "wikilink")
FRESHIN (4).jpg|燒烤配料及水果檔

<table>
<thead>
<tr class="header">
<th><p>改裝商舖前的慈雲山街市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Tsz Wan Shan Market (1).jpg|水果、香燭祭品及菜檔 Tsz Wan Shan Market (2).jpg|豆腐芽菜檔 Tsz Wan Shan Market (3).jpg|魚及蛋檔 Tsz Wan Shan Market (4).jpg|藥房及活雞檔 Tsz Wan Shan Market (5).jpg|<a href="../Page/燒味.md" title="wikilink">燒味及凍肉檔</a></p>
<p>Tsz Wan Shan Market (6).jpg|飾物及鮮肉檔 Tsz Wan Shan Market (7).jpg|服裝店</p></td>
</tr>
</tbody>
</table>

## 交通網絡

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 參考資料及附注

<div class="references-small">

<references />

</div>

## 其他連結

  - [慈雲山中心地圖位置](http://www.centamap.com/gc/centamaplocation.aspx?x=838705&y=823176&sx=838705.676&sy=823176.72&z=3&lg=b5)
  - [慈雲山中心簡介](https://web.archive.org/web/20161024223417/http://www.linkreit.com/TC/properties/Pages/Property-Locator-Details.aspx?pid=85)

[Category:黃大仙區商場](../Category/黃大仙區商場.md "wikilink")
[Category:慈雲山](../Category/慈雲山.md "wikilink")
[Category:領展商場及停車場](../Category/領展商場及停車場.md "wikilink")
[Category:1997年完工建築物](../Category/1997年完工建築物.md "wikilink")

1.
2.
3.
4.
5.  領匯問卷調查
6.
7.
8.
9.