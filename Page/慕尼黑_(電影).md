《**慕尼黑**》（）是一部-{zh:於;zh-hans:于;zh-hant:於;}-[2005年上映的](../Page/2005年電影.md "wikilink")[美國](../Page/美國.md "wikilink")[傳記電影](../Page/傳記電影.md "wikilink")，由[史蒂芬·史匹柏執導兼製片人](../Page/史蒂芬·史匹柏.md "wikilink")，[艾瑞克·巴納](../Page/艾瑞克·巴納.md "wikilink")、[丹尼爾·克雷格](../Page/丹尼爾·克雷格.md "wikilink")、[希朗·漢德](../Page/希朗·漢德.md "wikilink")、[马修·卡索维茨及](../Page/马修·卡索维茨.md "wikilink")等主演。

該片改編自所-{著}-的書籍《》（原文：），其主要內容講述1972年在[慕尼黑舉行](../Page/慕尼黑.md "wikilink")[奧運會時發生的](../Page/1972年夏季奥林匹克运动会.md "wikilink")[慕尼黑慘案](../Page/慕尼黑慘案.md "wikilink")，即[黑色九月屠殺](../Page/黑色九月.md "wikilink")[以色列運動員事件](../Page/以色列.md "wikilink")，及其後發展出的報復行動[天誅行動](../Page/天誅行動.md "wikilink")。電影中亦敘述了兩個真實發生的事件和行動—[青春之泉行動及](../Page/青春之泉行動.md "wikilink")[利勒哈默爾暗殺事件](../Page/利勒哈默爾暗殺事件.md "wikilink")。

該片獲得[奧斯卡金像獎](../Page/第78屆奧斯卡金像獎.md "wikilink")「[最佳影片](../Page/奧斯卡最佳影片獎.md "wikilink")」、「[最佳導演](../Page/奧斯卡最佳導演獎.md "wikilink")」等五項大獎提名。

## 劇情

[1972慕尼黑奧運慘劇後](../Page/慕尼黑惨案.md "wikilink")，以色列情報員對暗殺以色列運動員的巴勒斯坦恐怖份子展開追蹤報復。一名年輕愛國的以色列情報局幹員艾夫納（[艾瑞克·巴納](../Page/艾瑞克·巴納.md "wikilink")
飾），仍然處於慕尼黑事件發生後的震驚及悲慟之時，被一位名為埃弗蘭（[傑佛瑞·羅許](../Page/傑佛瑞·羅許.md "wikilink")
飾）的以色列情報局官員找上，要求他進行一項以色列史無前例的任務。

他要艾夫納離開懷孕的妻子，放棄他原來的身份，進行一項機密行動，獵殺十一名被以色列情報局指控為密謀策劃慕尼黑屠殺事件的幕後主使。雖然艾夫納年紀輕輕、缺乏經驗，但是卻被指派為共有四名組員的暗殺小組組長，每一位組員都有不同的性格和專長：出生於南非的史帝夫（[丹尼爾·克雷格](../Page/丹尼爾·克雷格.md "wikilink")
飾）負責開車接應；猶太裔德國人漢斯（
飾）負責偽造文件；比利時玩具工人羅伯（[马修·卡索维茨](../Page/马修·卡索维茨.md "wikilink")
飾）負責製造炸彈；沉穩冷靜的卡爾（[希朗·漢德](../Page/希朗·漢德.md "wikilink")
飾）則負責清理善後。艾夫納率領組員在日內瓦、羅馬、巴黎、賽普勒斯、倫敦和貝魯特活動，先找出他們鎖定的目標，然後執行計劃周詳的暗殺行動，一個一個將他們殺死。

他們遊走在國際法邊緣，並遠離家鄉及家人，對方是唯一的同伴，但是他們在一一執行暗殺行動之後不禁自問：「我們殺的到底是什麼人？這一切具有正當性嗎？以暴制暴能夠抑止恐怖行動嗎？」替祖國同胞報仇雪恨、伸張正義的決心，以及對這些暗殺行動日益強烈的疑慮，讓他們的內心受到痛苦煎熬，而且最後發現在獵殺目標的同時，他們也成為被獵殺的對象。

## 演員

<table>
<thead>
<tr class="header">
<th><p>演員</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/艾瑞克·巴納.md" title="wikilink">艾瑞克·巴納</a>（Eric Bana）</p></td>
<td><p>艾夫納（Avner）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹尼爾·克雷格.md" title="wikilink">丹尼爾·克雷格</a>（Daniel Craig）</p></td>
<td><p>史帝夫（Steve）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/希朗·漢德.md" title="wikilink">希朗·漢德</a>（Ciarán Hinds）</p></td>
<td><p>卡爾（Carl）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马修·卡索维茨.md" title="wikilink">马修·卡索维茨</a>（Mathieu Kassovitz）</p></td>
<td><p>羅伯（Robert）</p></td>
</tr>
<tr class="odd">
<td><p>（Hanns Zischler）</p></td>
<td><p>漢斯（Hans）</p></td>
</tr>
<tr class="even">
<td><p>（Ayelet Zurer）</p></td>
<td><p>戴芬娜（Daphna），艾夫納的妻子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傑佛瑞·羅許.md" title="wikilink">傑佛瑞·羅許</a>（Geoffrey Rush）</p></td>
<td><p>埃弗蘭（Ephraim）</p></td>
</tr>
<tr class="even">
<td><p>（Gila Almagor）</p></td>
<td><p>艾夫納的母親</p></td>
</tr>
<tr class="odd">
<td><p>（Michael Lonsdale）</p></td>
<td><p>帕帕（Papa）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬修·亞瑪希.md" title="wikilink">馬修·亞瑪希</a> （Mathieu Amalric）</p></td>
<td><p>路易斯（Louis）</p></td>
</tr>
<tr class="odd">
<td><p>（Moritz Bleibtreu）</p></td>
<td><p>安德烈亞斯（Andreas）</p></td>
</tr>
<tr class="even">
<td><p>（Valeria Bruni Tedeschi）</p></td>
<td><p>西爾維（Sylvie）</p></td>
</tr>
<tr class="odd">
<td><p>（Meret Becker）</p></td>
<td><p>伊馮娜（Yvonne）</p></td>
</tr>
<tr class="even">
<td><p>（Marie Josée Croze）</p></td>
<td><p>尚奈特（Jeanette），荷蘭女殺手</p></td>
</tr>
<tr class="odd">
<td><p>（Yvan Attal）</p></td>
<td><p>湯尼（Tony），安德烈亞斯的朋友</p></td>
</tr>
</tbody>
</table>

## 評價

[爛番茄新鮮度](../Page/爛番茄.md "wikilink")78%，基於202條評論，平均分為7.5/10\[1\]，而在[Metacritic上得到](../Page/Metacritic.md "wikilink")74分\[2\]，收穫普遍好評。

## 參考資料

## 外部連結

  -
  - {{@movies|fmen00408306|慕尼黑}}

  -
  -
  -
  -
  -
  -
  -
  -
## 參見

  - [上帝的復仇行動](../Page/上帝的復仇行動.md "wikilink")
  - [青春之泉行動](../Page/青春之泉行動.md "wikilink")
  - [利勒哈默爾暗殺事件](../Page/利勒哈默爾暗殺事件.md "wikilink")

[Category:2005年电影](../Category/2005年电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國政治劇情片](../Category/美國政治劇情片.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:美國間諜片](../Category/美國間諜片.md "wikilink")
[Category:德國真人真事改編電影](../Category/德國真人真事改編電影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")
[Category:恐怖活動電影](../Category/恐怖活動電影.md "wikilink")
[Category:報復題材電影](../Category/報復題材電影.md "wikilink")
[Category:奧運背景電影](../Category/奧運背景電影.md "wikilink")
[Category:猶太電影](../Category/猶太電影.md "wikilink")
[Category:夢工廠電影](../Category/夢工廠電影.md "wikilink")
[Category:巴黎背景電影](../Category/巴黎背景電影.md "wikilink")
[Category:1970年代背景電影](../Category/1970年代背景電影.md "wikilink")
[Category:德國背景電影](../Category/德國背景電影.md "wikilink")
[Category:荷蘭背景電影](../Category/荷蘭背景電影.md "wikilink")
[Category:匈牙利取景電影](../Category/匈牙利取景電影.md "wikilink")
[Category:約翰·威廉斯配樂電影](../Category/約翰·威廉斯配樂電影.md "wikilink")
[Category:安培林娛樂電影](../Category/安培林娛樂電影.md "wikilink")

1.
2.