[Goguryeo-monarchs(1-6).PNG](https://zh.wikipedia.org/wiki/File:Goguryeo-monarchs\(1-6\).PNG "fig:Goguryeo-monarchs(1-6).PNG")
**慕本王**（，），姓**高**（）名**解憂**（），又名**愛婁**（）、**莫來**（），是[高句麗的第](../Page/高句麗.md "wikilink")5代君主，是前兩任君主[大武神王](../Page/大武神王.md "wikilink")[高無恤的嫡子](../Page/高無恤.md "wikilink")、前任君主[閔中王的侄兒](../Page/閔中王.md "wikilink")\[1\]。他於公元32年[十二月](../Page/十二月.md "wikilink")（？）被冊立為高句麗的太子，44年[十一月](../Page/十一月.md "wikilink")（？）[大武神王駕崩時](../Page/大武神王.md "wikilink")，由於年紀尚幼，所以並未有繼位，其叔叔閔中王繼任高句麗君主。至48年，閔中王駕崩，解憂繼任王位。

## 治世

49年2月，派遣將軍襲撃[東漢的](../Page/東漢.md "wikilink")[北平](../Page/北平郡.md "wikilink")、[漁陽](../Page/漁陽郡.md "wikilink")、[上谷](../Page/上谷郡.md "wikilink")、[太原等四個郡](../Page/太原郡.md "wikilink")。[遼東](../Page/遼東郡.md "wikilink")[太守](../Page/太守.md "wikilink")[蔡彤以恩義及信義向慕本王對質](../Page/蔡彤.md "wikilink")，並透過[和親使兩國的關係得以回復](../Page/和親.md "wikilink")。根據《[後漢書](../Page/後漢書.md "wikilink")·蔡彤傳》的記載，為共同對付[匈奴這個敵人](../Page/匈奴.md "wikilink")，後漢對[鮮卑及高句麗採取懷柔政策](../Page/鮮卑.md "wikilink")，以賞賜拉攏二部向後漢朝貢。

根據朝鮮史書《[三國史記](../Page/三國史記.md "wikilink")》記載：慕本王為人兇殘，不單沒有容人之量，對批評他的人加以殺害。

## 諡号及埋葬地

53年11月，被为侍从[杜鲁刺杀死亡](../Page/杜鲁.md "wikilink")。由於他被葬在[慕本原](../Page/慕本原.md "wikilink")，所以亦以「慕本」來作[諡號](../Page/諡號.md "wikilink")。

## 參考

  - [金富軾](../Page/金富軾.md "wikilink")：《[三國史記](../Page/三國史記.md "wikilink")》

## 參看

  - [百濟](../Page/百濟.md "wikilink")
      - [多婁王](../Page/多婁王.md "wikilink") (，29年-77年)
  - [新羅](../Page/新羅.md "wikilink")
      - [儒理尼師今](../Page/儒理尼師今.md "wikilink") (，24年-57年)

## 備註

<references />

## 外部連結

  -
[Category:高句麗君主](../Category/高句麗君主.md "wikilink")
[J](../Category/高姓.md "wikilink")

1.  《[三國遺事](../Page/三國遺事.md "wikilink")》的記載有異：指慕本王是閔中王的兄長。