**慧超**（），也作**惠超**，[梵語法名](../Page/梵語.md "wikilink")**Prajñā-vikrama**（即慧超），朝鮮半島[三國時](../Page/朝鮮三國時代.md "wikilink")[新罗僧人](../Page/新罗.md "wikilink")，幼年入華，故亦爲[唐朝僧人](../Page/唐朝.md "wikilink")。

## 生平

719年，慧超十六歲，在[唐國](../Page/唐.md "wikilink")[广州被佛教](../Page/广州.md "wikilink")[密宗大师](../Page/密宗.md "wikilink")[金刚智收为出家弟子](../Page/金刚智.md "wikilink")。723年，慧超前往[印度诸国巡礼](../Page/印度.md "wikilink")，先到[东印度](../Page/东印度.md "wikilink")，再往[中印度](../Page/中印度.md "wikilink")、[南印度](../Page/南印度.md "wikilink")、[西印度](../Page/西印度.md "wikilink")、[北印度](../Page/北印度.md "wikilink")，然后前往[波斯](../Page/波斯.md "wikilink")、[大食](../Page/大食.md "wikilink")、大[拂临](../Page/拂临.md "wikilink")、[突厥](../Page/突厥.md "wikilink")，经[葱岭](../Page/葱岭.md "wikilink")、[疏勒](../Page/疏勒.md "wikilink")、[龟兹](../Page/龟兹.md "wikilink")、[于阗](../Page/于阗.md "wikilink")，在727年抵达[安西](../Page/安西.md "wikilink")，再经[焉耆回到](../Page/焉耆.md "wikilink")[长安](../Page/长安.md "wikilink")，期間見聞著有《[往五天竺国传](../Page/往五天竺国传.md "wikilink")》，後世散佚，20世紀初殘卷發現於敦煌。慧超回到长安後繼續在[大薦福寺金刚智座下学习](../Page/大薦福寺.md "wikilink")，金剛智入滅後，又跟隨[不空法師](../Page/不空.md "wikilink")（705年—774年）學習，并开始翻译《[大乘瑜伽金刚性海曼殊利室利千臂千钵大教王经](../Page/大乘瑜伽金刚性海曼殊利室利千臂千钵大教王经.md "wikilink")》，780年在[五台山将此经译完](../Page/五台山.md "wikilink")。一說783年圓寂，現代研究認為他於787年去世。

《[唐高僧傳](../Page/唐高僧傳.md "wikilink")》中有慧超傳。

## 参见

  - [朝鲜佛教](../Page/朝鲜佛教.md "wikilink")
  - [元曉](../Page/元曉.md "wikilink")
  - [異次頓](../Page/異次頓.md "wikilink")

## 參考資料

  - 冉雲華：〈[惠超「往五天竺國傳」中天竺國新箋考](http://nhdh.nhu.edu.tw/pdf/dunhung/02/2-5.pdf)〉。
  - 【唐】慧超 原著 .張毅 箋釋. 【唐】杜環 原著.張一純 箋注.《往五天竺國傳箋釋經行記箋注》
  - [慧超—韓国文化弘報部](https://web.archive.org/web/20040911195743/http://www.mct.go.kr/korea/info/culture/culture_view.jsp?menu=761&viewFlag=readUser&oid=%4029849%7C1%7C2)
  - [往五天竺国传残卷全文](https://web.archive.org/web/20070927043140/http://www.suttaworld.org/gbk/sutra/lon/other51/2089.htm)

[H](../Category/唐朝人.md "wikilink")
[Category:統一新羅人物](../Category/統一新羅人物.md "wikilink")
[H](../Category/朝鮮半島佛教出家眾.md "wikilink")
[Category:中外交通史人名](../Category/中外交通史人名.md "wikilink")
[Category:唐密代表人物](../Category/唐密代表人物.md "wikilink")
[Category:朝鮮半島佛教](../Category/朝鮮半島佛教.md "wikilink")
[Category:唐朝僧人](../Category/唐朝僧人.md "wikilink")