**慶南足球俱樂部**（[韓文](../Page/韓文.md "wikilink"):경남
FC）是[韓國一間](../Page/韓國.md "wikilink")[足球俱樂部](../Page/足球.md "wikilink")。球隊基地在[慶尚南道的首府](../Page/慶尚南道.md "wikilink")[昌原市](../Page/昌原市.md "wikilink")。

## 歷史

2006年，球隊成立並加入2006年的[K聯賽](../Page/K聯賽.md "wikilink")，成為第14隊[K聯賽球隊](../Page/K聯賽.md "wikilink")。

慶南FC在2007年的表現是轟動整個[韓國球壇的](../Page/韓國.md "wikilink")，因為球隊得到了聯賽第四名。雖然最後在季後賽被[浦項制鐵擊敗](../Page/浦項制鐵.md "wikilink")，但球隊的表現是值得嘉許的。

2018年球季，FC慶南以第二名完成[K聯賽1](../Page/K聯賽1.md "wikilink")，確定取得下屆亞冠盃入場券。

## 球衣贊助

  - 2006-2009 : [Hummel](../Page/:en:Hummel_International.md "wikilink")
  - 2010-現在 : [Kelme](../Page/:en:Kelme_\(company\).md "wikilink")

## Main Sponsor

  - 2006-現在: [STX](../Page/:en:STX_Corporation.md "wikilink")

## 队徽

<File:Gyeongnam> FC.gif|2006–2009 <File:Gyeongnam> FC.svg|2010–现在

## 榮譽

### 聯賽

  - **[K聯賽2](../Page/K聯賽2.md "wikilink")**

<!-- end list -->

  -

      -
        **冠軍 (1)**: 2017

### 盃賽

  - **[韓國足總盃](../Page/韓國足總盃.md "wikilink")**

<!-- end list -->

  -

      -
        亞軍 (2): 2008、2012

## 外部連結

  - [官方網頁](http://www.gyeongnamfc.com)

  - [Gyeongnam FC at
    ROKfootball.com](http://www.rokfootball.com/gyeongnam_fc)

[Category:韓國足球俱樂部](../Category/韓國足球俱樂部.md "wikilink")
[Category:2006年建立的足球俱樂部](../Category/2006年建立的足球俱樂部.md "wikilink")