**慶州金氏**是[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[金姓人口最繁盛的一支](../Page/金姓_\(韓國\).md "wikilink")，發祥地慶州（韓國[慶尚北道](../Page/慶尚北道.md "wikilink")[慶州市](../Page/慶州市.md "wikilink")）為新羅首都金城所在地。根據[大韓民國在](../Page/大韓民國.md "wikilink")2000年的人口統計，全國有170萬人口屬於慶州金氏，佔全國姓金的人約10%，全國人口的2%。

慶州金氏的始祖來自[新羅王朝](../Page/新羅.md "wikilink")。相傳金氏的始祖[金閼智是公元](../Page/金閼智.md "wikilink")1世紀時新羅的君主[脫解尼師今](../Page/脫解尼師今.md "wikilink")(昔脫解)所收養的孤兒。到了金閼智的第七代後人[金味鄒](../Page/金味鄒.md "wikilink")，他於262年接替[沾解尼師今成為新羅的君主](../Page/沾解尼師今.md "wikilink")。之後再經歷昔氏的三代([儒礼尼師今](../Page/儒礼尼師今.md "wikilink")、[基临尼師今](../Page/基临尼師今.md "wikilink")、[訖解尼師今](../Page/訖解尼師今.md "wikilink"))之後，新羅的王位又再回到金氏，並開始了新羅王朝的光輝時代，不單結束了三國分治的局面，還在朝鮮半島上立足近一千年，並留下大量豐盛的文化遺產。

## 世系

  - *參考[姓氏類別大観 金氏](http://www.myj7000.jp-biz.net/clan/fclan_index.htm)*
  - 右下角腳註代表第X代新羅王。例如：金味鄒<sub>13</sub>，即13代新羅王。
  - 用<span style="background: pink">粉紅色</span>標出者皆為[女性](../Page/女性.md "wikilink")。

## 知名人物

1.  [金閼智](../Page/金閼智.md "wikilink")
2.  [金味鄒](../Page/金味鄒.md "wikilink")
3.  奈勿尼師今 [金樓寒](../Page/金樓寒.md "wikilink") (356年—402年)
4.  實聖尼師今 [金堤](../Page/金堤.md "wikilink") (402年—417年)
5.  納祗麻立干 (417年—458年)
6.  慈悲麻立干 (458年—479年)
7.  炤知麻立干 (479年—500年)
8.  智證麻立干 [金智大路](../Page/金智大路.md "wikilink") (500年—514年)
9.  法興王 [金原宗](../Page/法兴王.md "wikilink") (514年—540年)
10. 真興王 [金三麥宗](../Page/真兴王.md "wikilink") (540年—576年)
11. 真智王 [金舍輪](../Page/金舍輪.md "wikilink") (576年—579年)
12. 真平王 [金白淨](../Page/金白淨.md "wikilink") (579年—632年)
13. 真安王 [金國飯](../Page/金國飯.md "wikilink") (真平王封)
14. 真正王 [金伯飯](../Page/金伯飯.md "wikilink") (真平王封)
15. 善德王 [金德曼](../Page/金德曼.md "wikilink") (632年—647年)
16. 真德王 [金勝曼](../Page/金勝曼.md "wikilink") (647年—654年)
17. 文興王 [金龍春](../Page/金龍春.md "wikilink") (武烈王追崇)
18. 新罗太宗 武烈王 [金春秋](../Page/金春秋.md "wikilink") (654年—661年)

## 注釋

## 參考資料

## 外部連結

  - [慶州金氏宗親會](https://web.archive.org/web/20060527210244/http://kyongkim.or.kr/)
  - [姓氏類別大観——金氏](http://www.myj7000.jp-biz.net/clan/fclan_index.htm)

[\*](../Category/金姓本贯.md "wikilink")