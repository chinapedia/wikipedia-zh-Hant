[各種顆粒大小，微米(µm)。【縱軸】 biological contaminants 生物污染物 types of dust 灰塵的類型
particulate contaminants 顆粒污染物 gas molecules 氣體分子。【橫軸】pollen 花粉 mold
spores 黴菌孢子 house dust mite allergens 屋塵蟎過敏原 bacteria 細菌 cat allergens
貓過敏原 virus 病毒 heavy dust 沉重的灰塵 setting dust 沉澱灰塵 suspended
atmospheric dust 懸浮大氣塵 cement dust 水泥粉塵 fly ash 煤灰 oil smoke 油煙 smog 煙霧
tobacco smoke 香煙煙霧 soot 煤煙 gaseous contaminants
氣態污染物。](https://zh.wikipedia.org/wiki/File:Airborne-particulate-size-chart.jpg "fig:各種顆粒大小，微米(µm)。【縱軸】 biological contaminants 生物污染物 types of dust 灰塵的類型 particulate contaminants 顆粒污染物 gas molecules 氣體分子。【橫軸】pollen 花粉 mold spores 黴菌孢子 house dust mite allergens 屋塵蟎過敏原 bacteria 細菌 cat allergens 貓過敏原 virus 病毒 heavy dust 沉重的灰塵 setting dust 沉澱灰塵 suspended atmospheric dust 懸浮大氣塵 cement dust 水泥粉塵 fly ash 煤灰 oil smoke 油煙 smog 煙霧 tobacco smoke 香煙煙霧 soot 煤煙 gaseous contaminants 氣態污染物。")
[這個動畫顯示從2006年8月17日至2007年4月10日，主要對流層懸浮微粒光學厚度的射出與運送。\[1\]\[2\] (*click for
more detail*)
\* 綠色：黑色和有機碳
\* 紅/橙：灰塵
\* 白：硫酸鹽
\*
藍：海鹽](https://zh.wikipedia.org/wiki/File:Atmospheric_Aerosol_Eddies_and_Flows_-_NASA_GSFC_S.ogv "fig:這個動畫顯示從2006年8月17日至2007年4月10日，主要對流層懸浮微粒光學厚度的射出與運送。 (click for more detail) * 綠色：黑色和有機碳 * 紅/橙：灰塵 * 白：硫酸鹽 * 藍：海鹽")
[懸浮微粒的分佈影片圖，根據美國航天局的特拉衛星中等分辨率成像光譜儀的數據。
\* 綠色區域顯示了較大的顆粒為主的懸浮微粒。
\* 紅色區域由小顆粒懸浮微粒為主。
\* 黃色區域顯示大，小混合的懸浮微粒。
\*
灰色顯示了傳感器並沒有收集數據。](https://zh.wikipedia.org/wiki/File:MODAL2_M_AER_RA.ogv "fig:懸浮微粒的分佈影片圖，根據美國航天局的特拉衛星中等分辨率成像光譜儀的數據。 * 綠色區域顯示了較大的顆粒為主的懸浮微粒。 * 紅色區域由小顆粒懸浮微粒為主。 * 黃色區域顯示大，小混合的懸浮微粒。 * 灰色顯示了傳感器並沒有收集數據。")
**懸浮顆粒**或稱**颗粒物**（particulate matter (PM)）、**大气颗粒物**（atmospheric
particulate
matter）、**颗粒**（particulates），泛指悬浮在[空气中的固体颗粒或液滴](../Page/空气.md "wikilink")，顆粒微小甚至肉眼難以辨識但仍有尺度的差異。在[环境科学中](../Page/环境科学.md "wikilink")，人類活動造成的過量顆粒散布與懸浮為[空气污染的主要指標之一](../Page/空气污染.md "wikilink")，但可能造成生物體不適或影響生態及能量圈循環範圍涵蓋尺度廣泛，從水霧、塵埃、花粉、皮屑、過敏源、[霾](../Page/霾.md "wikilink")；人為排放廢氣、灑布農藥、肥料、以及廢棄物如畜牧的糞便遇風揚塵等，一直到前驅物在大氣環境中經過一連串極其複雜的化學變化與[光化反應後形成硫酸鹽](../Page/光化學煙霧.md "wikilink")、硝酸鹽及銨鹽\[3\]。

其中，[空气动力学直径](../Page/空气动力学直径.md "wikilink")（以下简称直径）小于或等于10[微米](../Page/微米.md "wikilink")
(µm)的颗粒物称为**颗粒物**（**PM<sub>10</sub>**）；直径小于或等于2.5微米的颗粒物称为**细颗粒物**（**PM<sub>2.5</sub>**），例如室內的二手菸霧。颗粒物能够在大气中停留很长时间，并可随呼吸进入体内，积聚在[气管或](../Page/气管.md "wikilink")[肺中](../Page/肺.md "wikilink")，影响身体健康。\[4\]
PM2.5細小顆粒 ，比病毒大，比細菌小。 PM2.5細小顆粒，容易帶有毒物質進入人體。

## 来源及成分

[Diesel-smoke.jpg](https://zh.wikipedia.org/wiki/File:Diesel-smoke.jpg "fig:Diesel-smoke.jpg")
颗粒物的成分很复杂，主要取决于其来源。主要的来源是从地表扬起的[尘土](../Page/灰塵.md "wikilink")，含有[氧化物](../Page/氧化物.md "wikilink")[矿物和其他成分](../Page/矿物.md "wikilink")。[海盐是颗粒物的第](../Page/海盐.md "wikilink")2大来源，其组成与[海水的成分类似](../Page/海水.md "wikilink")。一部分颗粒物是自然过程产生的，源自[火山爆发](../Page/火山爆发.md "wikilink")、[沙尘暴](../Page/沙尘暴.md "wikilink")、[森林火灾](../Page/森林火灾.md "wikilink")、浪花等。

PM<sub>2.5</sub>还可以由[硫和](../Page/硫.md "wikilink")[氮的](../Page/氮.md "wikilink")[氧化物转化而成](../Page/氧化物.md "wikilink")。而这些[气体](../Page/气体.md "wikilink")[污染物往往是人类对](../Page/污染物.md "wikilink")[化石燃料](../Page/化石燃料.md "wikilink")（[煤](../Page/煤.md "wikilink")、[石油等](../Page/石油.md "wikilink")）和垃圾的燃烧造成的。在開發中国家，煤炭燃烧是家庭取暖和能源供应的主要方式。沒有先進廢氣處理裝置的[柴油](../Page/柴油.md "wikilink")[汽車也是颗粒物的來源](../Page/汽車.md "wikilink")。

在室內，[塵蟎](../Page/塵蟎.md "wikilink")、[二手菸是颗粒物最主要的來源](../Page/二手菸.md "wikilink")。颗粒物的來源是不完全燃燒、因此只要是靠燃燒的菸草產品，都會產生具有嚴重危害的颗粒物，使用品質較佳的香菸也只是吸菸者的自我安慰（甚至可能因為臭味較低，而造成更大的危害）；同理也適用於[金紙燃燒](../Page/紙錢.md "wikilink")、焚[香及燃燒](../Page/香.md "wikilink")[蚊香](../Page/蚊香.md "wikilink")。對於人體有害。

## 参考文献

## 外部链接

  - [空氣品質監測網](http://taqm.epa.gov.tw/taqm/tw/Default.aspx) －
    [行政院環境保護署](../Page/行政院環境保護署.md "wikilink")

## 參見

  - [煙霧質](../Page/煙霧質.md "wikilink")
  - [二手菸](../Page/二手菸.md "wikilink")
  - [空气质量指数](../Page/空气质量指数.md "wikilink")、[空氣污染指數](../Page/空氣污染指數.md "wikilink")
  - [穹頂之下 (紀錄片)](../Page/穹頂之下_\(紀錄片\).md "wikilink")
  - [脫空罩 找藍天行動計劃](../Page/脫空罩_找藍天行動計劃.md "wikilink")
  - [臺灣空氣污染](../Page/臺灣空氣污染.md "wikilink")

{{-}}

[ru:Взвешенные частицы](../Page/ru:Взвешенные_частицы.md "wikilink")

[Category:空氣污染](../Category/空氣污染.md "wikilink")

1.  <http://gmao.gsfc.nasa.gov/research/aerosol/modeling/nr1_movie/>
2.  <http://gmao.gsfc.nasa.gov/research/aerosol/>
3.  [空氣品質改善維護資訊網](http://air.epa.gov.tw/Public/suspended_particles.aspx)，2016年11月06日。
4.  [中日韩将共办PM2.5会议](http://www.apdnews.com/news/53447.html)
    ，亚太日报，2013年12月11日。