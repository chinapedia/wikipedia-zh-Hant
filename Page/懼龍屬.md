**懼龍屬**（[學名](../Page/學名.md "wikilink")：**）又名**惡霸龍**，是[暴龍科下的一](../Page/暴龍科.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[上白堊紀的](../Page/上白堊紀.md "wikilink")[北美洲西部](../Page/北美洲.md "wikilink")，距今7700萬-7400萬年前。目前有兩個有效[物種](../Page/物種.md "wikilink")：**強健懼龍**（*D.
torosus*）和**霍氏懼龍**（*D.
horneri*），[化石是在](../Page/化石.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[艾伯塔省中被發現](../Page/艾伯塔省.md "wikilink")，而在艾伯塔省、[美國](../Page/美國.md "wikilink")[蒙大拿州及](../Page/蒙大拿州.md "wikilink")[新墨西哥州發現的其他化石](../Page/新墨西哥州.md "wikilink")，還沒有被正式研究、命名。若包括這些未命名的物種，懼龍是暴龍科中具有最多種的屬。

懼龍與年代較晚期的[暴龍是近親](../Page/暴龍.md "wikilink")，並且擁有很多[解剖學上的相同特徵](../Page/解剖學.md "wikilink")。就像其他已知的暴龍科，懼龍是重達數噸的雙足獵食動物，有著很多尖銳的大型[牙齒](../Page/牙齒.md "wikilink")。牠有著小型的前肢，但與其他暴龍科的屬相比則較長。

懼龍位於[食物鏈的頂端](../Page/食物鏈.md "wikilink")，是一種[頂級掠食動物](../Page/頂級掠食動物.md "wikilink")，可能以大型恐龍為食，例如[角龍科的](../Page/角龍科.md "wikilink")[尖角龍](../Page/尖角龍.md "wikilink")、以及[鴨嘴龍科的](../Page/鴨嘴龍科.md "wikilink")[亞冠龍](../Page/亞冠龍.md "wikilink")。在一些地區，懼龍與同屬暴龍科的[蛇髮女怪龍同時存在](../Page/蛇髮女怪龍.md "wikilink")，不過牠們之間卻有著不同的[生態位](../Page/生態位.md "wikilink")。雖然在暴龍科內懼龍的化石算是稀少，但都足以提供資料作[生物學](../Page/生物學.md "wikilink")、群體活動、食性及壽命等研究。

## 描述

[Daspletosaurusscale.png](https://zh.wikipedia.org/wiki/File:Daspletosaurusscale.png "fig:Daspletosaurusscale.png")的大小比較。\]\]
[Daspletosaurus_torosus_steveoc.jpg](https://zh.wikipedia.org/wiki/File:Daspletosaurus_torosus_steveoc.jpg "fig:Daspletosaurus_torosus_steveoc.jpg")
雖然對於現今的獵食動物而言，懼龍體型是非常的大，但並非體型最大的[暴龍科](../Page/暴龍科.md "wikilink")。成年的懼龍的身長可達8-9公尺長\[1\]。平均體重估計值約為2.5噸\[2\]\[3\]\[4\]，誤差在為1.8噸\[5\]至3.8噸之間\[6\]。

懼龍有巨大的[頭顱骨](../Page/頭顱骨.md "wikilink")，約有1公尺長\[7\]。頭顱骨的構造特別堅固，例如口鼻部上的鼻骨互相癒合，以增加強度，而頭骨的大型洞孔則可減低重量。成年的懼龍約有60多顆[牙齒](../Page/牙齒.md "wikilink")，每隻牙齒都非常長。牙齒的[橫切面呈](../Page/橫切面.md "wikilink")[橢圓形而非短刃形](../Page/橢圓形.md "wikilink")。[前上頜骨的牙齒卻是呈D型的](../Page/前上頜骨.md "wikilink")，這種[異齒型在](../Page/異齒型.md "wikilink")[暴龍科中是非常普遍的](../Page/暴龍科.md "wikilink")。懼龍頭顱骨的獨特處在於，[上頜骨的表面凹凸不平](../Page/上頜骨.md "wikilink")，以及[眼睛周圍的](../Page/眼睛.md "wikilink")[淚骨](../Page/淚骨.md "wikilink")、[眶後骨及](../Page/眶後骨.md "wikilink")[顴骨有明顯的隆起](../Page/顴骨.md "wikilink")。[眼窩呈長橢圓形](../Page/眼窩.md "wikilink")，介於[蛇髮女怪龍的](../Page/蛇髮女怪龍.md "wikilink")[圓形及](../Page/圓形.md "wikilink")[暴龍的鑰孔形之間](../Page/暴龍.md "wikilink")\[8\]\[9\]\[10\]。

懼龍與其他的暴龍科都有著相似的體型，都是由呈S型的[頸部支撐著沉重的頭部](../Page/頸部.md "wikilink")。牠的前肢非常的短小，只有二指，不過在暴龍科中，懼龍的前肢/身體比例已是最長的了。懼龍具有兩個巨大後肢，上有四趾，第一趾為[反爪](../Page/反爪.md "wikilink")，無法接觸地面。懼龍具有長及重的尾巴，可平衡頭部與胸部，使[重心位在](../Page/重心.md "wikilink")[臀部上](../Page/臀部.md "wikilink")\[11\]\[12\]。

## 分類系統


<u>

<center>

卡爾等人版本，2005年\[13\]

</center>

</u>  <small>**\*<u>備註</u>：卡爾等人認為特暴龍是暴龍的一個種**</small>
<u>

<center>

居爾等人版本，2003年\[14\]

</center>

</u>
在[暴龍科中](../Page/暴龍科.md "wikilink")，懼龍與[特暴龍](../Page/特暴龍.md "wikilink")、[暴龍及](../Page/暴龍.md "wikilink")[分支龍同屬於](../Page/分支龍.md "wikilink")[暴龍亞科](../Page/暴龍亞科.md "wikilink")。暴龍亞科的物種都是親緣關係較接近暴龍，而較不類似[艾伯塔龍](../Page/艾伯塔龍.md "wikilink")，特徵都是較粗壯的體型、比例上較大的[頭顱骨](../Page/頭顱骨.md "wikilink")、以及較長的[股骨](../Page/股骨.md "wikilink")\[15\]\[16\]。

懼龍通常被認為是暴龍的近親，或是往暴龍演化的直接祖先\[17\]。[格里高利·保羅](../Page/格里高利·保羅.md "wikilink")（Gregory
S.
Paul）曾將[強健懼龍編入暴龍屬中](../Page/強健懼龍.md "wikilink")\[18\]，但這個分類卻一般不被接受\[19\]\[20\]。很多學者認為特暴龍及暴龍是姊妹[分類單元](../Page/分類單元.md "wikilink")，或甚至是同屬，而懼龍是這個演化支的基礎物種\[21\]\[22\]。[菲力·柯爾](../Page/菲力·柯爾.md "wikilink")（Phil
Currie）等人提出一個不同的理論，[分支龍與特暴龍這兩個](../Page/分支龍.md "wikilink")[亞洲屬構成一個演化支](../Page/亞洲.md "wikilink")，而懼龍較接近這個演化支，而離[北美洲的暴龍較遠](../Page/北美洲.md "wikilink")\[23\]。要待所有的懼龍[物種被研究](../Page/物種.md "wikilink")、描述，才可以得到較清晰的懼龍分類關係。

## 發現及命名

[強健懼龍](../Page/強健懼龍.md "wikilink")（*Daspletosaurus
torosus*）的[模式標本](../Page/模式標本.md "wikilink")（編號CMN
8506）是一些部份骨骼，包括了[頭顱骨](../Page/頭顱骨.md "wikilink")、[頸椎](../Page/頸椎.md "wikilink")、[背椎](../Page/背椎.md "wikilink")、[薦椎](../Page/薦椎.md "wikilink")、前11節[尾椎](../Page/尾椎.md "wikilink")、肩膀、一個前肢、[骨盆及一根](../Page/骨盆.md "wikilink")[股骨](../Page/股骨.md "wikilink")。這些化石是於1921年由[查爾斯·斯騰伯格](../Page/查爾斯·斯騰伯格.md "wikilink")（Charles
Mortram
Sternberg）所發現，他最初認為這些化石屬於[蛇髮女怪龍的一個新](../Page/蛇髮女怪龍.md "wikilink")[種](../Page/種.md "wikilink")。但是，這些標本要直到1970年才由[戴爾·羅素](../Page/戴爾·羅素.md "wikilink")（Dale
Russell）完全的描述，並建立新的懼龍屬。懼龍的[學名是由](../Page/學名.md "wikilink")[古希臘文的](../Page/古希臘文.md "wikilink")「*δασπλητo-*」（意即「懼怕」）及「*σαυρος*」（意即「[蜥蜴](../Page/蜥蜴.md "wikilink")」）而來\[24\]。種名的「*torosus*」在[拉丁文意指](../Page/拉丁文.md "wikilink")「強壯」或「結實」\[25\]。除了模式標本，在2001年發現一個相當完整的懼龍骨骼標本（編號RTMP
2001.36.1）。這兩個標本都是來自[加拿大的](../Page/加拿大.md "wikilink")[艾伯塔省](../Page/艾伯塔省.md "wikilink")，發現於[朱迪斯河群中的](../Page/朱迪斯河群.md "wikilink")[奧爾德曼組](../Page/奧爾德曼組.md "wikilink")。另一個年代較晚的[馬蹄峽谷組發現的標本](../Page/馬蹄峽谷組.md "wikilink")，被重新歸類於[肉食艾伯塔龍](../Page/肉食艾伯塔龍.md "wikilink")\[26\]。奧爾德曼組是在[上白堊紀的](../Page/上白堊紀.md "wikilink")[坎帕階中期形成的](../Page/坎帕階.md "wikilink")，距今約有7700萬-7600萬年前\[27\]。

### 未命名物種

多年來，有兩或三個其他種被編入懼龍屬中，但至今仍未有正式的描述或命名。雖然牠們未必是同一物種，但都暫被編入「*D.
sp.*」中，但這不意味牠們屬於同一個種\[28\]\[29\]。

#### 恐龍公園物種

[FMNH_Daspletosaurus.jpg](https://zh.wikipedia.org/wiki/File:FMNH_Daspletosaurus.jpg "fig:FMNH_Daspletosaurus.jpg")[菲爾德自然歷史博物館](../Page/菲爾德自然歷史博物館.md "wikilink")\]\]
[Daspletosaurus_horneri.jpg](https://zh.wikipedia.org/wiki/File:Daspletosaurus_horneri.jpg "fig:Daspletosaurus_horneri.jpg")\]\]
在建立[強健懼龍的](../Page/強健懼龍.md "wikilink")[正模標本時](../Page/正模標本.md "wikilink")，[羅素將由](../Page/戴爾·羅素.md "wikilink")[巴納姆·布郎](../Page/巴納姆·布郎.md "wikilink")（Barnum
Brown）在1913年所挖掘的標本，建立為強健懼龍的[副模標本](../Page/副模標本.md "wikilink")（編號AMNH
5438）。這個標本包含了一個[骨盆](../Page/骨盆.md "wikilink")、部份後肢、及一些相連的[脊椎](../Page/脊椎.md "wikilink")，是發現於[艾伯塔省的](../Page/艾伯塔省.md "wikilink")[奧爾德曼組的較上層](../Page/奧爾德曼組.md "wikilink")\[30\]。這個較上層後被更名為[恐龍公園組](../Page/恐龍公園組.md "wikilink")，屬於[坎帕階晚期](../Page/坎帕階.md "wikilink")，距今7600萬-7400萬年前\[31\]。於1914年，布郎從同一地層發現接近完整的骨骼及[頭顱骨](../Page/頭顱骨.md "wikilink")（編號FMNH
PR308），並於40年後由[美國自然歷史博物館售予](../Page/美國自然歷史博物館.md "wikilink")[芝加哥的](../Page/芝加哥.md "wikilink")[菲爾德自然歷史博物館](../Page/菲爾德自然歷史博物館.md "wikilink")。這顱骨目前在芝加哥菲爾德博物館展出中，並多年以來都被歸類為是「*Albertosaurus
libratus*」。但在發現幾個頭顱骨特徵（包括大部份的[牙齒](../Page/牙齒.md "wikilink")）被石膏隱藏多年後，這個標本才被重置在懼龍屬中\[32\]。在恐龍公園組一共發現了八具標本，大部份都是在[恐龍省立公園的範圍內](../Page/恐龍省立公園.md "wikilink")。菲力·柯爾發現恐龍公園標本的頭顱骨上有一些不同的特徵，認為這代表一個新的懼龍[物種](../Page/物種.md "wikilink")。這個新物種的圖畫經已被出版，但仍有待正式的命名及描述\[33\]。

#### 新墨西哥州物種

在1990年，[新墨西哥州的](../Page/新墨西哥州.md "wikilink")[嘉德蘭組Hunter](../Page/嘉德蘭組.md "wikilink")
Wash段發現了一個新的[暴龍科標本](../Page/暴龍科.md "wikilink")（編號OMNH
10131），包括[頭顱骨碎片](../Page/頭顱骨.md "wikilink")、[肋骨及部份後肢](../Page/肋骨.md "wikilink")，被編入[後彎齒龍](../Page/後彎齒龍.md "wikilink")，後彎齒龍的狀態是個[疑名](../Page/疑名.md "wikilink")\[34\]。後來很多學者都將這個標本，連同一些其他新墨西哥州發現的化石，重新歸類於懼龍屬中的未命名[種](../Page/種.md "wikilink")\[35\]\[36\]\[37\]。在2010年的一個研究，認為這個新墨西哥物種，其實是早期的暴龍科，並建立為新屬，[虐龍](../Page/虐龍.md "wikilink")\[38\]。目前對嘉德蘭組的年代有所爭議，一些研究指出其年代屬於[坎帕階晚期](../Page/坎帕階.md "wikilink")\[39\]，而其他研究則認為是[麥斯特里希特階早期](../Page/麥斯特里希特階.md "wikilink")\[40\]。

#### 雙麥迪遜物種

於1992年，[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）及他的同僚發表一份有關[美國](../Page/美國.md "wikilink")[蒙大拿州](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組較上層的新發現](../Page/雙麥迪遜組.md "wikilink")[暴龍科](../Page/暴龍科.md "wikilink")[恐龍的非常初步報告](../Page/恐龍.md "wikilink")，認為這些新化石是懼龍及後期[暴龍的過渡](../Page/暴龍.md "wikilink")[物種](../Page/物種.md "wikilink")，其年代為坎潘階\[41\]。在2001年，雙麥迪遜組的上層發現另一個部份骨骼，在其下腹內保存了一頭[鴨嘴龍科的幼年個體化石](../Page/鴨嘴龍科.md "wikilink")。這個標本被歸類於懼龍的一種，但卻沒有明定是哪一個種\[42\]。在雙麥迪遜的[骨床中](../Page/骨床.md "wikilink")，曾發現最少三個以上的懼龍化石\[43\]。這些標本都沒有被詳細描述，但[柯爾認為所有的雙麥迪遜化石都是懼龍屬中的第三個未被命名種](../Page/菲力·柯爾.md "wikilink")\[44\]。

## 古生物學

雖然懼龍的[化石並不像其他](../Page/化石.md "wikilink")[暴龍科般常見](../Page/暴龍科.md "wikilink")，但都足以讓[古生物學家來研究其](../Page/古生物學家.md "wikilink")[生物學的某些範疇](../Page/生物學.md "wikilink")。過去曾在懼龍的一顆牙齒，發現隆突分叉的現象\[45\]\[46\]。

### 與蛇髮女怪龍的競爭

[Daspletosaurus_torosus_skull_FMNH.jpg](https://zh.wikipedia.org/wiki/File:Daspletosaurus_torosus_skull_FMNH.jpg "fig:Daspletosaurus_torosus_skull_FMNH.jpg")[菲爾德博物館標明為](../Page/菲爾德博物館.md "wikilink")[艾伯塔龍](../Page/艾伯塔龍.md "wikilink")（編號FMNH
PR308），1999年被發現屬於懼龍\]\]
在[坎帕階晚期的](../Page/坎帕階.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，懼龍與[蛇髮女怪龍生存於相同時代的相同地區](../Page/蛇髮女怪龍.md "wikilink")。這是少數兩個[暴龍科的](../Page/暴龍科.md "wikilink")[屬共存的例子](../Page/屬.md "wikilink")。在現今的獵食動物中，體型相似的獵食動物因為[解剖學](../Page/解剖學.md "wikilink")、行為或[地理的原因](../Page/地理.md "wikilink")，會區分成不同的[生態位](../Page/生態位.md "wikilink")，以限制競爭\[47\]。有幾個研究嘗試解釋懼龍及蛇髮女怪龍的生態位差異。

[戴爾·羅素假設較輕盈](../Page/戴爾·羅素.md "wikilink")、常見的蛇髮女怪龍可能獵食當時繁盛的[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")，而較強壯、稀少的懼龍則專門獵食數量較小、防禦較好的[角龍科](../Page/角龍科.md "wikilink")\[48\]。但是，在[雙麥迪遜組發現的編號OTM](../Page/雙麥迪遜組.md "wikilink")
200懼龍標本，在其胃部保存了鴨嘴龍科幼年體的已[消化化石](../Page/消化.md "wikilink")\[49\]。[暴龍亞科](../Page/暴龍亞科.md "wikilink")（例如懼龍）有著較高及較寬的口鼻部，比有較低口鼻部的[艾伯塔龍亞科](../Page/艾伯塔龍亞科.md "wikilink")（例如蛇髮女怪龍）更為強壯，不過[牙齒的強度則差異不大](../Page/牙齒.md "wikilink")。這顯示兩者在攝食的技巧或食性都有所不同\[50\]。在一個[屍骨層](../Page/屍骨層.md "wikilink")，則發現三個懼龍、至少五個[鴨嘴龍類化石](../Page/鴨嘴龍類.md "wikilink")\[51\]。

其他學者認為牠們之間的競爭受到地理區分的限制。不像其他的[恐龍類](../Page/恐龍.md "wikilink")，牠們的區分似乎與離海岸的距離沒有關聯，而也與海拔高或低無關\[52\]。但是，雖然生活區域有部份的重疊，蛇髮女怪龍似乎在北方較為普遍，而懼龍則傾向於南方。其他的恐龍亦有類似的地理分佈。在坎潘階，[鴨嘴龍亞科與](../Page/鴨嘴龍亞科.md "wikilink")[開角龍亞科繁盛於](../Page/開角龍亞科.md "wikilink")[雙麥迪遜組與北美洲西南部](../Page/雙麥迪遜組.md "wikilink")。[湯瑪斯·荷茲](../Page/湯瑪斯·荷茲.md "wikilink")（Thomas
Holtz）指出這個地理分佈型式，顯示暴龍亞科、開角龍亞科及鴨嘴龍亞科都有著同樣的生態位傾向。他認為於[麥斯特里希特階末期](../Page/麥斯特里希特階.md "wikilink")，暴龍亞科（如[暴龍](../Page/暴龍.md "wikilink")）、鴨嘴龍亞科及開角龍亞科（如[三角龍](../Page/三角龍.md "wikilink")）廣泛分佈在北美洲西部，而艾伯塔龍亞科及[尖角龍亞科已](../Page/尖角龍亞科.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")，[賴氏龍亞科則已很稀少](../Page/賴氏龍亞科.md "wikilink")\[53\]。

### 群體活動

[Daspletosaurus_torosus_models.jpg](https://zh.wikipedia.org/wiki/File:Daspletosaurus_torosus_models.jpg "fig:Daspletosaurus_torosus_models.jpg")中一對強健懼龍模型\]\]
在[恐龍公園地層發現的一個年輕懼龍標本](../Page/恐龍公園地層.md "wikilink")（編號TMP
94.143.1），面部上有被其他[暴龍科咬傷的痕跡](../Page/暴龍科.md "wikilink")。這個咬痕已經痊癒，顯示牠在這次事件後存活過來。在同一地層發現的一個成年懼龍標本（編號TMP
85.62.1）也有相似咬痕，可見這類攻擊並不限於年輕的[動物](../Page/動物.md "wikilink")。雖然這些攻擊可能是來自其他[物種](../Page/物種.md "wikilink")，但獵食動物常發生物種內的打鬥、攻擊行為。目前已在其他的暴龍科（如[蛇髮女怪龍及](../Page/蛇髮女怪龍.md "wikilink")[暴龍](../Page/暴龍.md "wikilink")）以及其他的[獸腳亞目](../Page/獸腳亞目.md "wikilink")（如[中華盜龍及](../Page/中華盜龍.md "wikilink")[蜥鳥盜龍](../Page/蜥鳥盜龍.md "wikilink")）發現這種面部的攻擊痕跡。Darren
Tanke與菲力·柯爾提出假說，指出這些攻擊是因為競爭疆域、食物資源、或支配群族而出現的[種內競爭](../Page/種內競爭.md "wikilink")\[54\]。

懼龍群體生活的證據是發現於[蒙大拿州](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")[骨床的化石](../Page/骨床.md "wikilink")。骨床內有三頭懼龍的遺骸，包括一頭大的成年個體、一頭小的幼龍、及另一頭中型大小的懼龍。同一地點還發現最少五頭[鴨嘴龍科化石](../Page/鴨嘴龍科.md "wikilink")。[地質學證據顯示](../Page/地質學.md "wikilink")，這些遺骸並非被河流沖積在一起，而是在短暫時間遭到集體掩葬。鴨嘴龍科的遺骸是分散的，並且有很多暴龍科[牙齒的咬痕](../Page/牙齒.md "wikilink")，可見懼龍在死前曾吃過這些鴨嘴龍科動物。這些動物的[死亡原因並不清楚](../Page/死亡.md "wikilink")。[菲力·柯爾猜測懼龍是群體狩獵的](../Page/菲力·柯爾.md "wikilink")，但無法確定\[55\]。其他科學家則對懼龍及其他大型獸腳亞目的群體活動證據抱有懷疑\[56\]。Brian
Roach與Daniel
Brinkman認為懼龍的群體生活方式很類似現今的[科莫多龍](../Page/科莫多龍.md "wikilink")，當科莫多龍在競爭屍體食物時，有時會出現[噬食同類的情況](../Page/噬食同類.md "wikilink")\[57\]。

### 成長及壽命

[Tyrantgraph.png](https://zh.wikipedia.org/wiki/File:Tyrantgraph.png "fig:Tyrantgraph.png")(紅)、[蛇髮女怪龍](../Page/蛇髮女怪龍.md "wikilink")(藍)、懼龍(綠)、[暴龍](../Page/暴龍.md "wikilink")(黑)。\]\]
[古生物學家](../Page/古生物學家.md "wikilink")[格里高利·艾利克森](../Page/格里高利·艾利克森.md "wikilink")（Gregory
Erickson
）及其同僚研究了[暴龍科的生長及壽命](../Page/暴龍科.md "wikilink")。透過骨頭[組織學的分析](../Page/組織學.md "wikilink")，可以確定標本[死亡時的年齡](../Page/死亡.md "wikilink")。將不同個體的年齡與體型繪製為圖表，就可得到生長曲線。艾利克森指出暴龍科經歷長時間的幼龍狀態，會在4年內急速成長。這個急速成長的階段會在達到[性成熟時完結](../Page/性成熟.md "wikilink")，成年後的生長率會減慢。艾利克森只研究了從[雙麥迪遜組發現的懼龍標本](../Page/雙麥迪遜組.md "wikilink")，但這些標本都有相同的生長模式。與[艾伯塔龍相比](../Page/艾伯塔龍.md "wikilink")，因懼龍的體重較高，在急速成長期有著較快的生長率。懼龍的最大生長率為每年180公斤（假設成年的懼龍體重是1800公斤）。其他學者指出懼龍應為更重，不過這只會影響懼龍生長率的量，而非整體模式\[58\]。

艾利克森等人將每一個年齡層的標本作成表格，整理出[艾伯塔龍群族的生長模式](../Page/艾伯塔龍.md "wikilink")。艾利克森指出艾伯塔龍的幼年個體[化石紀錄較為稀少](../Page/化石紀錄.md "wikilink")，處在急速成長期的接近成年個體及成年個體則更為常見。這可能是因為化石化過程與挖掘化石的偏差。艾利克森假設這些差異是來自幼龍在某一體型的低死亡率，就像現今的某些大型[哺乳類](../Page/哺乳類.md "wikilink")（例如[象](../Page/象.md "wikilink")）一樣。因為暴龍科在兩歲時體型就超越了所有同期的獵食動物，所以在缺乏被獵食的情況下，出現低死亡率。古生物學家並沒有足夠的懼龍化石來進行類似的分析，但艾利克森則認為懼龍也有類似的生長趨勢\[59\]。

## 古生態學

[Two_Medicine.jpg](https://zh.wikipedia.org/wiki/File:Two_Medicine.jpg "fig:Two_Medicine.jpg")[雙麥迪遜組的一處露頭](../Page/雙麥迪遜組.md "wikilink")\]\]
[Daspletosaurus_torDB.jpg](https://zh.wikipedia.org/wiki/File:Daspletosaurus_torDB.jpg "fig:Daspletosaurus_torDB.jpg")的想像圖\]\]
目前已發現的懼龍化石都發現於[上白堊紀](../Page/上白堊紀.md "wikilink")[坎帕階中至晚期的地層](../Page/坎帕階.md "wikilink")，距今約7700萬-7400萬年前。在白堊紀中期，[北美洲被](../Page/北美洲.md "wikilink")[西部內陸海道一分為二](../Page/西部內陸海道.md "wikilink")，[蒙大拿州及](../Page/蒙大拿州.md "wikilink")[艾伯塔省大部份都在海平面之下](../Page/艾伯塔省.md "wikilink")。在懼龍存活的這段時期，西部[拉拉米造山運動使得](../Page/拉拉米造山運動.md "wikilink")[洛磯山脈隆起](../Page/洛磯山脈.md "wikilink")，使海道向東方及南方後退。大型河流從西側的山脈流至東側的海道，當中的沉積物形成廣大的海岸平原，成為[朱迪斯河群的](../Page/朱迪斯河群.md "wikilink")[雙麥迪遜組與其他的地層](../Page/雙麥迪遜組.md "wikilink")。約7300萬年前，海道再次向西方與北方海侵，逐漸淹沒恐龍公園組。這個新海域名為熊掌海（Bearpaw
Sea），形成海相的[熊掌頁岩組](../Page/熊掌頁岩組.md "wikilink")（Bearpaw Shale
Formation）地層，涵蓋[美國西部及](../Page/美國.md "wikilink")[加拿大西部](../Page/加拿大.md "wikilink")\[60\]\[61\]\[62\]。

懼龍生活在西部內陸海道西岸的廣大[氾濫平原](../Page/氾濫平原.md "wikilink")。大型的河流流經土地，有時會出現氾濫，形成新的沉積層。當水源充足時，可以維持大量的[植物及](../Page/植物.md "wikilink")[動物生活](../Page/動物.md "wikilink")，但這個區域會發生週期性的乾旱，造成大量生物的死亡，形成朱迪斯河群與雙麥迪遜組的眾多[骨床](../Page/骨床.md "wikilink")\[63\]。在現今的[東非也有相似的環境](../Page/東非.md "wikilink")\[64\]。平原西方的週期性[火山噴發](../Page/火山.md "wikilink")，產生的火山灰會影響整個地區，這成大量的死亡，而同時豐富了土壤，有助於日後的植物生長。科學家也藉由這些火山灰得出準確的[放射性測年](../Page/放射性測年.md "wikilink")。海平面的改變，在不同時期對朱迪斯河群的不同地區，造成許多環境變動，包括離岸及近岸的海洋生態、海岸濕地、三角洲及礁湖、內陸的氾濫平原等\[65\]。與朱迪斯河群的其他地層相比，雙麥迪遜組是在更內陸的高海拔地區\[66\]。

在雙麥迪遜組及朱迪斯河群有良好的[脊椎動物](../Page/脊椎動物.md "wikilink")[化石紀錄](../Page/化石紀錄.md "wikilink")，這是因為豐富的動物群、週期性的自然災害及大量的沉積岩所造成的。這些地區有多樣性的淡水及入海口[魚類化石](../Page/魚類.md "wikilink")，包括[鯊魚](../Page/鯊魚.md "wikilink")、[鰩目](../Page/鰩目.md "wikilink")、[鱘魚](../Page/鱘魚.md "wikilink")、[雀鱔目及其他魚類](../Page/雀鱔目.md "wikilink")。朱迪斯河群也保存很多水生[爬行動物及](../Page/爬行動物.md "wikilink")[兩棲動物化石](../Page/兩棲動物.md "wikilink")，包括有[青蛙](../Page/青蛙.md "wikilink")、[鲵](../Page/鲵.md "wikilink")、[龜](../Page/龜.md "wikilink")、[鱷龍及](../Page/鱷龍.md "wikilink")[鱷魚](../Page/鱷魚.md "wikilink")。陸地上的爬行動物，則有[叉舌蜥科](../Page/叉舌蜥科.md "wikilink")、[石龍子科](../Page/石龍子科.md "wikilink")、[巨蜥科及](../Page/巨蜥科.md "wikilink")[蛇蜥科](../Page/蛇蜥科.md "wikilink")。天空的飛行動物則包含：[神龍翼龍科及](../Page/神龍翼龍科.md "wikilink")[新鳥亞綱](../Page/新鳥亞綱.md "wikilink")（如[虛椎鳥等](../Page/虛椎鳥.md "wikilink")），而[反鳥亞綱的](../Page/反鳥亞綱.md "wikilink")*Apatornis*生存於地面。[哺乳動物則包含](../Page/哺乳動物.md "wikilink")：[多瘤齒目](../Page/多瘤齒目.md "wikilink")、[有袋目及](../Page/有袋目.md "wikilink")[真獸類](../Page/真獸類.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")。除此上述物種之外，還有懼龍及其他恐龍\[67\]。

在[奧爾德曼組](../Page/奧爾德曼組.md "wikilink")，[強健懼龍可能獵食](../Page/強健懼龍.md "wikilink")[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")（如[短冠龙及](../Page/短冠龙.md "wikilink")[亞冠龍](../Page/亞冠龍.md "wikilink")）、小型的[鳥腳下目](../Page/鳥腳下目.md "wikilink")（如[奔山龍](../Page/奔山龍.md "wikilink")）、[角龍科](../Page/角龍科.md "wikilink")（如[尖角龙](../Page/尖角龙.md "wikilink")）、[腫頭龍下目](../Page/腫頭龍下目.md "wikilink")、[似鳥龍下目](../Page/似鳥龍下目.md "wikilink")、[鐮刀龍超科](../Page/鐮刀龍超科.md "wikilink")、可能還有[甲龍下目](../Page/甲龍下目.md "wikilink")。其他獵食恐龍包括：[傷齒龍科](../Page/傷齒龍科.md "wikilink")、[竊蛋龍下目](../Page/竊蛋龍下目.md "wikilink")、[馳龍科的](../Page/馳龍科.md "wikilink")[蜥鳥盜龍](../Page/蜥鳥盜龍.md "wikilink")、可能還有[艾伯塔龍亞科](../Page/艾伯塔龍亞科.md "wikilink")。恐龍公園組及雙麥迪遜組有著類似奧爾德曼組的動物群組成，而恐龍公園組則多種大型獵食恐龍，不會互相競爭食物來源\[68\]。[蛇髮女怪龍與懼龍同時生活於恐龍公園組與雙麥迪遜組上層](../Page/蛇髮女怪龍.md "wikilink")\[69\]。幼年暴龍科的生態位，介乎成年[暴龍科及小型的](../Page/暴龍科.md "wikilink")[獸腳亞目之間](../Page/獸腳亞目.md "wikilink")，牠們之間的[體重有數倍的差距](../Page/體重.md "wikilink")\[70\]\[71\]\[72\]\[73\]。恐龍公園組發現的一個[蜥鳥盜龍](../Page/蜥鳥盜龍.md "wikilink")[齒骨](../Page/齒骨.md "wikilink")，表面帶有年輕暴龍類的齒痕，可能是由當地的懼龍所留下\[74\]。

## 大眾文化

懼龍曾出現在[探索頻道的節目](../Page/探索頻道.md "wikilink")《[恐龍星球](../Page/恐龍星球.md "wikilink")》（*Dinosaur
Planet*）第三集，一群懼龍獵食一隻受傷的幼年[慈母龍](../Page/慈母龍.md "wikilink")。

## 參考資料

## 外部連結

  - [Discussion and specimen
    list](https://web.archive.org/web/20070125140546/http://staff.washington.edu/eoraptor/Tyrannosauroidea.html#Daspletosaurustorosus)
    at The Theropod Database.

  - [Skull
    image](https://web.archive.org/web/20070109160125/http://www.paleograveyard.com/daspletosaurus.html)
    of the Dinosaur Park Formation *Daspletosaurus* at The Graveyard.

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:暴龍亞科](../Category/暴龍亞科.md "wikilink")

1.

2.
3.

4.

5.

6.

7.
8.

9.

10. {{ref name=holtz2004\>

11.
12.
13.
14.
15.
16.

17.

18.
19.
20.
21.
22.

23.
24.

25.
26.
27.

28.
29.
30.
31.
32.
33.
34.

35.
36.
37.

38.

39.

40.

41.
42.  [DOI](../Page/Digital_object_identifier.md "wikilink"):
    [10.1666/0022-3360(2001)075\<0401:GCFACT\>2.0.CO;2](http://www.bioone.org/perlserv/?request=get-abstract&doi=10.1666%2F0022-3360\(2001\)075%3C0401%3AGCFACT%3E2.0.CO%3B2)

43.

44.
45. <http://www.geology.cz/bulletin/fulltext/bullgeosci200803351.pdf>

46.

47.

48.
49.
50.

51.
52.
53.
54.  \[not printed until 2000\]

55.
56. \[published abstract only\]

57.

58.
59.

60.

61.

62.

63.

64.

65.
66.
67.
68.
69.
70.
71.
72.
73.

74.