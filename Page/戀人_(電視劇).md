《**戀人**》（）是[韓國](../Page/大韓民國.md "wikilink")[SBS於](../Page/SBS_\(韓國\).md "wikilink")2006年11月8日起播出的[水木迷你連續劇](../Page/SBS水木連續劇.md "wikilink")，此劇是導演[申宇哲和編劇](../Page/申宇哲.md "wikilink")[金銀淑](../Page/金銀淑.md "wikilink")《[戀人系列](../Page/SBS戀人系列.md "wikilink")》中，繼2004年《[巴黎戀人](../Page/巴黎戀人.md "wikilink")》及2005年《[布拉格戀人](../Page/布拉格戀人.md "wikilink")》之後的第三部曲。

## 劇情介紹

高中都沒有畢業的黑道頭目河康在（[李瑞鎮飾](../Page/李瑞鎮.md "wikilink")），小時候就被[黑幫老大收養](../Page/黑幫.md "wikilink")，長大後順理成章繼承養父的組織，及組織有關係的建築公司。有一次，康在被襲受傷，被尹美珠（[金諪恩飾](../Page/金諪恩.md "wikilink")）救了。美珠是一位窮牧師的女兒，才貌兼具的整形外科醫生。相處之下，康在對她動了心，可是美珠非常討厭黑幫，而且康在已經有一個女朋友朴郁珍（[金奎梨飾](../Page/金奎梨.md "wikilink")）。另一方面，康在的養父本身有個親生兒子姜世延（[鄭燦飾](../Page/鄭燦.md "wikilink")），世延因為被康在奪去建築公司的繼承權而對他十分憎恨，在與美珠的相親中，世延也對美珠動心了……

## 演出陣容

### 主要人物

<table>
<thead>
<tr class="header">
<th><p>style="background: #CCCCFF; color:#black" ; align="center"; width="55"|<strong>演員</strong></p></th>
<th><p>style="background: #CCCCFF; color:#black" ; align="center"; width="55"|<strong>角色</strong></p></th>
<th><p>style="background: #CCCCFF; color:#black" ; align="center"|<strong>介紹</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p><a href="../Page/李瑞鎮.md" title="wikilink">李瑞鎮</a></p></td>
<td><p>河康在</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/金諪恩.md" title="wikilink">金諪恩</a></p></td>
<td><p>尹美珠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/鄭燦.md" title="wikilink">鄭燦</a></p></td>
<td><p>姜世延</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/金奎梨.md" title="wikilink">金奎梨</a></p></td>
<td><p>朴郁珍</p></td>
<td></td>
</tr>
</tbody>
</table>

### 其它人物

<table>
<thead>
<tr class="header">
<th><p>style="background: #CCCCFF; color:#black" ; align="center"; width="100"|<strong>演員</strong></p></th>
<th><p>style="background: #CCCCFF; color:#black" ; align="center"; width="100"|<strong>角色</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p><a href="../Page/李基英.md" title="wikilink">李基英</a></p></td>
<td><p>尚澤</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/金雷夏.md" title="wikilink">金雷夏</a></p></td>
<td><p>昌培</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/金南佶.md" title="wikilink">金南佶</a></p></td>
<td><p>泰山</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/張航善.md" title="wikilink">張航善</a></p></td>
<td><p>白宗代</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/李世昌_(演員).md" title="wikilink">李世昌</a></p></td>
<td><p>李振修</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/朴仁煥.md" title="wikilink">朴仁煥</a></p></td>
<td><p>尹牧師</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/崔荷娜.md" title="wikilink">崔荷娜</a></p></td>
<td><p>洪純貞</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/柳政鉉.md" title="wikilink">柳政鉉</a></p></td>
<td><p>薛元澤</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/崔日和.md" title="wikilink">崔日和</a></p></td>
<td><p>姜會長</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/楊琴實.md" title="wikilink">楊琴實</a></p></td>
<td><p>鄭楊琴</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/延美珠.md" title="wikilink">延美珠</a></p></td>
<td><p>崔允</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/金永哲.md" title="wikilink">金永哲</a></p></td>
<td><p>東勳</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/姜仁錫.md" title="wikilink">姜仁錫</a></p></td>
<td><p>熙東</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/金基邦.md" title="wikilink">金基邦</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/姜珠馨.md" title="wikilink">姜珠馨</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/申素律.md" title="wikilink">申素律</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/石修.md" title="wikilink">石修</a></p></td>
<td><p>（客串）</p></td>
</tr>
</tbody>
</table>

## 得獎列表

|                                  |                                          |     |                                  |
| -------------------------------- | ---------------------------------------- | --- | -------------------------------- |
| 年度                               | 頒獎典禮                                     | 獎項  | 獲獎者                              |
| 2006年                            | [SBS演技大賞](../Page/SBS演技大賞.md "wikilink") | PD賞 | [金諪恩](../Page/金諪恩.md "wikilink") |
| 十大明星賞                            | [李瑞鎮](../Page/李瑞鎮.md "wikilink")         |     |                                  |
| [金諪恩](../Page/金諪恩.md "wikilink") |                                          |     |                                  |
| 人氣賞                              | [李瑞鎮](../Page/李瑞鎮.md "wikilink")         |     |                                  |
|                                  |                                          |     |                                  |

## 外部連結

  - [韓國SBS官方網站](http://tv.sbs.co.kr/lovers2006/)
  - [臺灣GTV官方網站](https://web.archive.org/web/20081209050803/http://www.gtv.com.tw/Program/B051420081117U/)

[Category:2006年韓國電視劇集](../Category/2006年韓國電視劇集.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:八大電視外購韓劇](../Category/八大電視外購韓劇.md "wikilink")
[Category:SBS戀人系列](../Category/SBS戀人系列.md "wikilink")
[Category:緯來電視外購韓劇](../Category/緯來電視外購韓劇.md "wikilink")