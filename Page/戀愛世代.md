《**戀愛世紀**》（，Love
Generation）是一部[日本](../Page/日本.md "wikilink")[電視劇](../Page/日本電視劇.md "wikilink")，1997年10月13日至12月22日於[富士電視台播出](../Page/富士電視台.md "wikilink")，共11集，由[木村拓哉及](../Page/木村拓哉.md "wikilink")[松隆子主演](../Page/松隆子.md "wikilink")。平均收視為30.8％。2001年3月於[台灣](../Page/台灣.md "wikilink")[緯來日本台首播](../Page/緯來日本台.md "wikilink")，之後陸續於[JET日本台與](../Page/JET日本台.md "wikilink")[TVBS歡樂台及](../Page/TVBS歡樂台.md "wikilink")[八大戲劇台重播](../Page/八大戲劇台.md "wikilink")。2001年6月2日於[香港](../Page/香港.md "wikilink")[無綫電視以粵語配音首播](../Page/無綫電視.md "wikilink")。

## 劇情簡介

片桐哲平在廣告代理公司的企劃部任職。在他人生中「最倒楣的一天」遇上一個被男友趕下車的女子，便邀她上賓館，沒想到反被她擺了一道。第二天他自認倒楣去上班，卻因為太過自我中心，被上司調到營業課去，赫然發現前一晚的那個女子——上杉理子，原來是他的同事。

哲平在適應新環境和新工作的同時，跟理子成為了一對鬥氣冤家。但是哲平唸書時的女友水原早苗卻又在他面前出現，而且更偏偏成了哲平的哥哥莊一郎的現任女友。哲平對早苗還有愛意，理子也明白這點，卻仍然情不自禁的愛上了他…\[1\]

## 主要角色

*以下所列配音部分，為香港無綫電視粵語配音版*

<table style="width:90%;">
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>粵語配音</strong></p></td>
<td><p><strong>介紹</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/木村拓哉.md" title="wikilink">木村拓哉</a></p></td>
<td><center>
<p>片桐哲平</p></td>
<td><center>
<p><a href="../Page/蘇強文.md" title="wikilink">蘇強文</a></p></td>
<td><p>本來從事廣告設計，性格我行我素、玩世不恭，不顧旁人想法，遭到降職，到營業部當業務員。起初十分不甘心，不願迎合辦公室的工作文化。經歷了事業及感情上的磨練，處事開始成熟起來。和高中同學水原早苗曾經交往過。雖然哲平生性風流，卻對她念念不忘。不擅面對愛情。一開始很討厭上杉理子，卻被她的不認輸性格所影響</p></td>
<td><center>
<p>24歲</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/松隆子.md" title="wikilink">松隆子</a></p></td>
<td><center>
<p>上杉理子</p></td>
<td><center>
<p><a href="../Page/陸惠玲.md" title="wikilink">陸惠玲</a></p></td>
<td><p>OL。專長是魔術及高爾夫球，廚藝極度糟糕。外表上喜歡胡鬧，專門破壞別人的好事，其實對感情相當認真。一旦有喜歡的人，會不顧一切取悅對方，卻不自覺的會傷害到自己。她喜歡哲平，但對他的愛沒有信心，深知哲平對水原早苗未能忘情</p></td>
<td><center>
<p>22歲</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/內野聖陽.md" title="wikilink">內野聖陽</a></p></td>
<td><center>
<p>片桐莊一郎</p></td>
<td><center>
<p><a href="../Page/招世亮.md" title="wikilink">招世亮</a></p></td>
<td><p>哲平的兄長。任職檢察官，無論人品和職業都深受期待及仰慕的菁英。早年曾經為了前途，拋棄了受到家族醜聞困擾的女友白石奈美。性格穩重，一直信任哲平和早苗。但白石的出現，使他努力建立的優越感和自信逐漸崩潰，開始懷疑早苗與哲平之間存有私情</p></td>
<td><center>
<p>28歲</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/純名里沙.md" title="wikilink">純名里沙</a></p></td>
<td><center>
<p>水原早苗</p></td>
<td><center>
<p><a href="../Page/沈小蘭.md" title="wikilink">沈小蘭</a></p></td>
<td><p>從事傳譯工作，是哲平的初戀情人。對哲平暱稱為「小哲」。後來和哲平的兄長莊一郎交往，已到達談婚論嫁的階段。因為哲平的不果斷，加上不斷節外生枝，使早苗與莊一郎的感情出現裂痕。早苗亦感到自己愛的可能不是莊一郎，而是哲平</p></td>
<td><center>
<p>25歲</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/藤原紀香.md" title="wikilink">藤原紀香</a></p></td>
<td><center>
<p>高木惠理香</p></td>
<td><center>
<p><a href="../Page/黃麗芳.md" title="wikilink">黃麗芳</a></p></td>
<td><p>理子的好朋友，職業是空中小姐。外表艷麗豪放，內心渴望能夠找到好歸宿。對哲平有好感，但她知道理子喜歡哲平後，甘願放手，鼓勵理子勇敢去愛</p></td>
<td><center>
<p>24歲</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/平田滿.md" title="wikilink">平田滿</a></p></td>
<td><center>
<p>黑崎武士</p></td>
<td><center>
<p><a href="../Page/陳欣_(配音員).md" title="wikilink">陳欣</a></p></td>
<td><p>營業部長。哲平的調動處的上司。綽號是「鬼軍曹」，經常給哲平許多關於人生的建議及開示</p></td>
<td><center>
<p>44歲</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/森口瑤子.md" title="wikilink">森口瑤子</a></p></td>
<td><p>|</p>
<center>
<p>白石奈美</p></td>
<td><center>
<p><a href="../Page/曾秀清.md" title="wikilink">曾秀清</a></p></td>
<td><p>片桐莊一郎的舊情人。生母曾經因出手打傷丈夫而被捕，年輕的莊一郎為了自清而拋棄了她。婚後的她並不快樂，重遇莊一郎的白石看見莊一郎擺出一副事不關己的態度，心有不甘。雖然她有意稍微破壞莊一郎的名譽，心裡仍然愛著莊一郎，但是她早知彼此緣份已盡，決定離開並重過新生活</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/川端龍太.md" title="wikilink">川端龍太</a></p></td>
<td><center>
<p>吉本民夫</p></td>
<td><center>
<p><a href="../Page/陳卓智.md" title="wikilink">陳卓智</a></p></td>
<td><p>哲平的朋友，對理子有好感</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/井筒森介.md" title="wikilink">井筒森介</a></p></td>
<td><center>
<p>佐佐木大作</p></td>
<td></td>
<td></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/井川比佐志.md" title="wikilink">井川比佐志</a></p></td>
<td><center>
<p>上杉謙一</p></td>
<td><center>
<p><a href="../Page/黃子敬.md" title="wikilink">黃子敬</a></p></td>
<td><p>理子的父親（第7集客串）</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/吹石一惠.md" title="wikilink">吹石一惠</a></p></td>
<td><center>
<p>上杉綠</p></td>
<td></td>
<td><p>理子的妹妹（第9~10集客串）</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/椎名桔平.md" title="wikilink">椎名桔平</a></p></td>
<td><center>
<p>小笠原是清</p></td>
<td><center>
<p><a href="../Page/潘文柏.md" title="wikilink">潘文柏</a></p></td>
<td><p>小笠原興產董事（第4集客串）</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/佐藤珠緒.md" title="wikilink">佐藤珠緒</a></p></td>
<td><center>
<p>理子前男友的現任女友</p></td>
<td><center></td>
<td><p>（第4集客串）</p></td>
<td><center>
<p>－</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/藤木直人.md" title="wikilink">藤木直人</a></p></td>
<td><center>
<p>片桐哲平的同學</p></td>
<td></td>
<td><p>（第7集客串）</p></td>
<td><center>
<p>－</p></td>
</tr>
</tbody>
</table>

  - 其他演員：堀真樹、長嶺尚子、丸山浩之、淺見貴子、三枝慎、王建軍、野村信次、大原香織、品川徹、生瀬勝久（第7集客串）、高橋克實（第10集客串、完結篇及特別篇客串）
  - 完結篇及特別篇客串演員：五十嵐泉、奈月寬子、鈴木美惠、黑谷友香

## 象徵物品和景點

### 水晶蘋果

哲平所住的公寓有很多特色的物品，例如[泰國的雕像](../Page/泰國.md "wikilink")、有三十年歷史不再生產的舊款冰箱……還有一個水晶蘋果。水晶蘋果代表著男女之間的愛情，就像是讓亞當與夏娃結緣的禁果一樣。水晶是易碎品，而且透過水晶蘋果顯示出來的景像是倒轉的。在最終回的結尾，水晶蘋果顯示的景像卻不再倒轉了，象徵哲平和理子的戀情終於開花結果。這個蘋果在劇集播出後成為了熱門的訂情信物，也有在原聲大碟的封套上出現。

### True love never runs smooth廣告看板

這個廣告看板在片中重覆亮相，出現的地方包括公園以至哲平的冰箱。[模特兒手中拿著水晶蘋果](../Page/模特兒.md "wikilink")，上面寫著True
love never runs
smooth（真愛的路途永遠顛簸），正好反映哲平和理子兩人從初次見面到糾纏在三角關係之間的矛盾和掙扎，這句話可以視作本劇的非官方副標題。

### 彩虹橋／台場

[RainbowBridge_over_TokyoBay.jpg](https://zh.wikipedia.org/wiki/File:RainbowBridge_over_TokyoBay.jpg "fig:RainbowBridge_over_TokyoBay.jpg")，執台場進出樞紐的[彩虹大橋](../Page/彩虹大橋.md "wikilink")。\]\]
在第一話中，哲平和理子就是沿著[彩虹橋的行人通道](../Page/彩虹橋.md "wikilink")，從芝浦的哲平家走到[台場的沙灘](../Page/台場.md "wikilink")，兩人就在那裡找理子遺失的戒指，一直到天亮。第十話理子回[長野家鄉前](../Page/長野.md "wikilink")，兩人也再到[台場一行](../Page/台場.md "wikilink")。彩虹橋也因這些場面成為了日本愛情劇的重要地標。

## 製作團隊

  - 編劇：[淺野妙子](../Page/淺野妙子.md "wikilink")、[尾崎將也](../Page/尾崎將也.md "wikilink")
  - 音樂：[CAGNET](../Page/CAGNET.md "wikilink")
  - 選曲、音響效果：大貫悦男
  - 攝影技術 ：川田正幸
  - 技術製作：石井勝浩
  - 技術指導：礒崎守隆
  - 美術製作：[梅田正則](../Page/梅田正則.md "wikilink")
  - 設計：[山本修身](../Page/山本修身.md "wikilink")
  - 執行製作：山崎康生
  - 製作主任：北田宏和
  - 製作人：[小岩井宏悦](../Page/小岩井宏悦.md "wikilink")
  - 副製作人：伊藤達哉、[關卓也](../Page/關卓也.md "wikilink")
  - 導演：[永山耕三](../Page/永山耕三.md "wikilink")、二宮浩行、木村達昭
  - 副導演：佐佐木詳太、[小林和宏](../Page/小林和宏.md "wikilink")

## 收視率

<table>
<tbody>
<tr class="odd">
<td><p><strong>集數</strong></p></td>
<td><p><strong>播放日期</strong></p></td>
<td><p><strong>日文副標題</strong></p></td>
<td><p><strong>中文副標題</strong></p></td>
<td><p><strong>編劇</strong></p></td>
<td><p><strong>導演</strong></p></td>
<td><p><strong>收視率</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第1集</p></td>
<td><p>1997年10月13日</p></td>
<td></td>
<td><p>新的自己，遇上命中注定的戀愛</p></td>
<td><p>淺野妙子</p></td>
<td></td>
<td><p>31.3%</p></td>
<td><p>加長版，播出時間21:03－22:09</p></td>
</tr>
<tr class="even">
<td><p>第2集</p></td>
<td><p>1997年10月20日</p></td>
<td></td>
<td><p>戀愛降臨的一瞬間</p></td>
<td><p>30.1%</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3集</p></td>
<td><p>1997年10月27日</p></td>
<td></td>
<td><p>淚和雨沾濕的禮物</p></td>
<td><p>木村達昭</p></td>
<td><p>29.0%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4集</p></td>
<td><p>1997年11月3日</p></td>
<td></td>
<td><p>沒有愛的一夜</p></td>
<td><p>30.8%</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5集</p></td>
<td><p>1997年11月10日</p></td>
<td></td>
<td><p>接吻，接吻，接吻</p></td>
<td><p>尾崎將也</p></td>
<td></td>
<td><p><span style="color: blue;">28.6%</span></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6集</p></td>
<td><p>1997年11月17日</p></td>
<td></td>
<td><p>愛的混浴露天風呂</p></td>
<td><p>淺野妙子</p></td>
<td><p>30.8%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7集</p></td>
<td><p>1997年11月24日</p></td>
<td></td>
<td><p>幸福來臨以後的事</p></td>
<td></td>
<td><p>30.3%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8集</p></td>
<td><p>1997年12月1日</p></td>
<td></td>
<td><p>|突然刺破愛的碎片</p></td>
<td></td>
<td><p>29.4%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9集</p></td>
<td><p>1997年12月8日</p></td>
<td></td>
<td><p>分別</p></td>
<td></td>
<td><p><font color=red>32.5%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10集</p></td>
<td><p>1997年12月15日</p></td>
<td></td>
<td><p>東京的最後約會</p></td>
<td></td>
<td><p>32.3%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大結局</p></td>
<td><p>1997年12月22日</p></td>
<td></td>
<td><p>為愛而生</p></td>
<td></td>
<td><p>32.1%</p></td>
<td><p>加長版，播出時間21:03－22:24</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>特別篇</p></td>
<td><p>1998年4月6日</p></td>
<td></td>
<td><p>新的自己，遇上命中注定的戀愛</p></td>
<td><p>淺野妙子、尾崎將也</p></td>
<td><p>永山耕三、二宮浩行、木村達昭</p></td>
<td><p>－</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平均收視率30.6%（收視率由<a href="../Page/Video_Research.md" title="wikilink">Video Research於</a><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/關東地區.md" title="wikilink">關東地區調查</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 得獎紀錄

  - [第15回日劇學院賞](../Page/第15回日劇學院賞.md "wikilink")
    最佳男主角：[木村拓哉](../Page/木村拓哉.md "wikilink")
  - [第15回日劇學院賞](../Page/第15回日劇學院賞.md "wikilink")
    最佳導演：[永山耕三](../Page/永山耕三.md "wikilink")、[木村達昭](../Page/木村達昭.md "wikilink")、[二宮浩行](../Page/二宮浩行.md "wikilink")

## 參考資料

## 外部連結

  - [緯來日本台官方網站](http://japan.videoland.com.tw/channel/20150803/default.asp)

  -
## 作品的變遷

[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:1997年日本電視劇集](../Category/1997年日本電視劇集.md "wikilink")
[Category:八大電視外購日劇](../Category/八大電視外購日劇.md "wikilink")
[Category:日本愛情劇](../Category/日本愛情劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:淺野妙子劇本作品](../Category/淺野妙子劇本作品.md "wikilink")
[Category:Now寬頻電視外購劇集](../Category/Now寬頻電視外購劇集.md "wikilink")
[Category:木村拓哉](../Category/木村拓哉.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:日刊體育日劇大賞作品獎](../Category/日刊體育日劇大賞作品獎.md "wikilink")

1.  [緯來日本台－戀愛世代](http://japan.videoland.com.tw/channel/20150803/introduction.html)