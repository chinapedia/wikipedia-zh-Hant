[Wikipe-tan_visual_novel_(Ren'Py).png](https://zh.wikipedia.org/wiki/File:Wikipe-tan_visual_novel_\(Ren'Py\).png "fig:Wikipe-tan_visual_novel_(Ren'Py).png")\]\]
**戀愛冒險遊戲**（）是[電子遊戲類型其中一種](../Page/電子遊戲.md "wikilink")，在[冒險遊戲當中](../Page/冒險遊戲.md "wikilink")，特別著重於[戀愛要素](../Page/戀愛.md "wikilink")（與異性間的互動）的遊戲。其中有許多作品和[戀愛模擬遊戲不容易加以區別](../Page/戀愛模擬遊戲.md "wikilink")，通常兩者會合稱為[戀愛遊戲](../Page/戀愛遊戲.md "wikilink")。在部分國家會將戀愛冒險遊戲都會歸類為[視覺小說類型](../Page/視覺小說.md "wikilink")。

戀愛冒險遊戲可以再細分多種類型，例如影音劇本、動作格鬥、策略模擬、角色養成、第一人稱冒險等等，早期發售的遊戲多半是[限制級](../Page/成人電子遊戲.md "wikilink")[男性向的](../Page/男性向.md "wikilink")[美少女遊戲](../Page/美少女遊戲.md "wikilink")，不過近年來[女性向的遊戲作品也不斷增加中](../Page/女性向.md "wikilink")。遊戲平台種類也不斷的增加，除[PC以外](../Page/PC.md "wikilink")，尚有[PS2](../Page/PS2.md "wikilink")、[XBOX](../Page/XBOX.md "wikilink")、[PSP等平台](../Page/PSP.md "wikilink")。

## 定義

戀愛冒險遊戲的玩法是透過閱讀劇本文字，在特定劇情會出現數個選項讓玩家選擇。不同的選項會影響後面劇情發展或是影響特定角色好感度，最後系統會依據玩家的選擇而進入不同的故事結局。

## 與[視覺小說的區别](../Page/視覺小說.md "wikilink")

戀愛冒險遊戲的劇本文字出現在畫面下方，而視覺小說的劇本文字出現在整個畫面或是半個畫面。劇本文字的換行頻率戀愛冒險遊戲是隨著不同角色的台詞或旁白來做替換，視覺小說則是同時出現各角色的台詞和旁白，因此戀愛冒險遊戲比視覺小說的替換劇本文字的次數更為頻繁。在遊戲畫面的演出效果方面，戀愛冒險遊戲會比視覺小說好一些。這些差別產生的原因是戀愛冒險遊戲比較重視人物和背景的演出，為了讓玩家能清楚觀看演出效果，所以文字需要儘可能短少，在可提供最底限的情報即可。視覺小說是從小說轉化成遊戲而來，所以重視閱讀文字部分。

## 関連項目

  - [日本成人遊戲](../Page/日本成人遊戲.md "wikilink")
  - [美少女遊戲](../Page/美少女遊戲.md "wikilink")
  - [戀愛遊戲](../Page/戀愛遊戲.md "wikilink")
      - [戀愛模擬遊戲](../Page/戀愛模擬遊戲.md "wikilink")
  - [冒險遊戲](../Page/冒險遊戲.md "wikilink")
      - [音聲小說](../Page/音聲小說.md "wikilink")（）
      - [視覺小說](../Page/視覺小說.md "wikilink")

[ja:恋愛アドベンチャーゲーム](../Page/ja:恋愛アドベンチャーゲーム.md "wikilink")

[Category:电子游戏类型](../Category/电子游戏类型.md "wikilink")
[\*](../Category/戀愛冒險遊戲.md "wikilink")