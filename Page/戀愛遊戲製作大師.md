《**戀愛遊戲製作大師**》（又稱**戀愛模擬共和國**）是由日本[Enterbrain公司開發的一款](../Page/Enterbrain.md "wikilink")[冒險遊戲製作工具](../Page/冒險遊戲.md "wikilink")，最大的特色就是使用[GUI圖形介面大幅降低遊戲製作的](../Page/GUI.md "wikilink")[程式技術](../Page/程式.md "wikilink")，同時內建大量圖片素材，讓使用者可以簡簡單單按幾個按鈕就可以製作出自己心目中的AVG遊戲。由於其上手簡易的特色，成為華文地區[同人遊戲製作的主流工具](../Page/同人遊戲.md "wikilink")。

《**戀愛遊戲製作大師**》這個系列一共出了兩款，在台灣由不同的公司代理，因此翻譯名稱略有不同。

## 戀愛遊戲製作大師1

### 系列沿革

1999年由[ASCII公司首次發行](../Page/ASCII.md "wikilink")，2000年由[Enterbrain重新發行](../Page/Enterbrain.md "wikilink")，[台灣由](../Page/台灣.md "wikilink")[大宇資訊代理](../Page/大宇資訊.md "wikilink")，目前已經完全絕版，網路上簡稱為**戀一**，知名的戀一遊戲則有Elieloveyou所製作的《灰姑娘》。

### 動作環境

  -
    OS：Microsoft(R) Windows(R) 95/98/Me
    CPU：Pentium(R)166mb以上（Pentium(R)200 MB推薦）
    DirectX：DirectX5 以上
    顯示：800×600像素以上
    音效：サウンドブラスター互換の音源とPCMを搭載したもの
    記憶體：32MB以上（64MB以上推薦）
    硬碟容量：200MB以上
    CD-ROM：2倍速以上

## 戀愛遊戲製作大師2

簡稱**戀二**，2001年發售日文版，2004年由[光譜資訊代理](../Page/光譜資訊.md "wikilink")，光譜資訊並且主辦、協辦多次遊戲製作比賽，對於台港地區自製遊戲風氣有很大的推廣功用。相對於該公司不斷推出新的[RPG製作大師系列](../Page/RPG製作大師.md "wikilink")，戀愛遊戲製作大師系列則是僅此兩款。戀二本身是2001年發售，到現在已過了相當一段時間，隨著[作業系統](../Page/作業系統.md "wikilink")、[DirectX等不斷更新](../Page/DirectX.md "wikilink")，戀二在系統穩定度上遠遠不如其他同等級的工具，但由於上手簡易等優點，[台港中地區依然相當多人使用戀二製作遊戲](../Page/台港中.md "wikilink")。

### 動作環境

  -
    OS：Windows98/Me/2000
    ※要求[DirectX](../Page/DirectX.md "wikilink")7.0以上（軟體內附[DirectX](../Page/DirectX.md "wikilink")8.0a安裝程式）
    CPU：PentiumⅢ450MHz以上
    硬碟：200MB（600MB以上推薦）
    記憶體：32MB以上（64MB以上推薦）
    顯示：800×600像素以上
    音效：サウンドブラスター互換音源、かつＰＣＭを搭載したもの

## 作品列表

  - 2004年
      - [白王子與黑騎士](../Page/白王子與黑騎士.md "wikilink")
      - [生存倒數SEVEN DAYS](../Page/生存倒數SEVEN_DAYS.md "wikilink")
      - [我的高校日記](../Page/我的高校日記.md "wikilink")
      - [溝通communication](../Page/溝通communication.md "wikilink")
      - [金錢戰爭](../Page/金錢戰爭.md "wikilink")
      - [神祇－戀愛混戰之卷](../Page/神祇－戀愛混戰之卷.md "wikilink")
      - [謎樣之夏](../Page/謎樣之夏.md "wikilink")
      - [我的ID的故事](../Page/我的ID的故事.md "wikilink")
      - [clips @ Angle Street
        角落街](../Page/clips_@_Angle_Street_角落街.md "wikilink")
      - [2004聖夜祭-追愛急先鋒](../Page/2004聖夜祭-追愛急先鋒.md "wikilink")
  - 2005年
      - [青色之戀](../Page/青色之戀.md "wikilink")
      - [Mela en'coiamin](../Page/Mela_en'coiamin.md "wikilink")
      - [少年行之情人的秘密](../Page/少年行之情人的秘密.md "wikilink")
      - [新選學院異聞錄](../Page/新選學院異聞錄.md "wikilink")
      - [青梅竹馬 (遊戲)](../Page/青梅竹馬_\(遊戲\).md "wikilink")
      - [Mayu & Mayu](../Page/Mayu_&_Mayu.md "wikilink")
      - [聖域風雲—蚩尤的野望](../Page/聖域風雲—蚩尤的野望.md "wikilink")
      - [七憶情緣](../Page/七憶情緣.md "wikilink")
      - [奪糖爭心](../Page/奪糖爭心.md "wikilink")
      - [怪談 (遊戲)](../Page/怪談_\(遊戲\).md "wikilink")
      - [金錢戰爭Kuso野球拳](../Page/金錢戰爭Kuso野球拳.md "wikilink")
      - [美少男夢工廠](../Page/美少男夢工廠.md "wikilink")
      - [Only you](../Page/Only_you.md "wikilink")
  - 2006年
      - [顛覆童話](../Page/顛覆童話.md "wikilink")
      - [超．戀愛完全引力](../Page/超．戀愛完全引力.md "wikilink")
      - [落入魔法國](../Page/落入魔法國.md "wikilink")
  - 2007年
      - [Dawn～破曉之刻～](../Page/Dawn～破曉之刻～.md "wikilink")<small>（※同人活動發售作品）</small>
  - 2010年
      - [薰衣草之戀](../Page/薰衣草之戀.md "wikilink")

## 外部連結

  - [戀一　日文官網](http://www.enterbrain.co.jp/digifami/products/renaiv/index.html)
  - [戀二　日文官網](http://www.enterbrain.co.jp/digifami/products/renai2/index.html)
  - [株式会社エンターブレイン](http://www.enterbrain.co.jp/)
  - [T-Time光譜資訊](http://www.ttime.com.tw/)

[Category:软件](../Category/软件.md "wikilink")
[Category:電子遊戲研發](../Category/電子遊戲研發.md "wikilink")
[Category:戀愛遊戲](../Category/戀愛遊戲.md "wikilink")