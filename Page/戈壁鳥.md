**戈壁鳥**（[學名](../Page/學名.md "wikilink")*Gobipteryx*）是[反鳥亞綱下](../Page/反鳥亞綱.md "wikilink")[上白堊紀的一](../Page/上白堊紀.md "wikilink")[屬](../Page/屬.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。牠的[化石在](../Page/化石.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")[戈壁沙漠的](../Page/戈壁沙漠.md "wikilink")[巴魯恩戈約特組發現](../Page/巴魯恩戈約特組.md "wikilink")，首次於1976年根據兩個[頭顱骨碎片](../Page/頭顱骨.md "wikilink")（編號ZPAL-MgR-I/12及ZPAL-MgR-I/32）而描述。[模式種是](../Page/模式種.md "wikilink")*G.
minuta*。\[1\]

原先認為是屬於[侏儒鳥的](../Page/侏儒鳥.md "wikilink")[頭顱骨碎片及一些顱後骨](../Page/頭顱骨.md "wikilink")（編號PIN-4492，被命名為*Nanantius
valifanovi*）亦被編入此屬中。這是從研究首個完好的標本（編號IGM-100/011）：一個在[烏哈托喀](../Page/烏哈托喀.md "wikilink")[德加多克塔組保存得非常好的部份頭顱骨而得知的](../Page/德加多克塔組.md "wikilink")。\[2\]被分類為[蛋屬的](../Page/蛋屬.md "wikilink")*oogenus
Laevisoolithus*亦被重新編入戈壁鳥，因為從中發現了鳥類[胚胎的骨頭](../Page/胚胎.md "wikilink")。

戈壁鳥有時被認為已經[滅絕](../Page/滅絕.md "wikilink")，且分類在已滅絕的戈壁鳥目及戈壁鳥科中，但有些[科學家則反對這個分類](../Page/科學家.md "wikilink")，認為需要更多牠的物種及其親屬的資料才可以決定。\[3\]

## 參考

[Category:真反鳥類](../Category/真反鳥類.md "wikilink")
[Category:白堊紀鳥類](../Category/白堊紀鳥類.md "wikilink")

1.
2.
3.