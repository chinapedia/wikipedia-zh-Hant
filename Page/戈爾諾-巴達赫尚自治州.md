[Gorno_badakhshan_map.png](https://zh.wikipedia.org/wiki/File:Gorno_badakhshan_map.png "fig:Gorno_badakhshan_map.png")

**戈爾諾-巴達赫尚自治州**（； / Gorno-Badakhshanskaya
(a.o.)），又译作**山地-巴達赫尚自治州**，是[塔吉克斯坦的一個多山地的省](../Page/塔吉克斯坦.md "wikilink")。面積大約佔塔吉克斯坦的40%。它的名稱源于[俄語](../Page/俄語.md "wikilink")，意思是「多山地的[巴達赫尚](../Page/巴达克山.md "wikilink")」。[中華民國政府曾宣稱該地區大部分應該屬於](../Page/中華民國政府.md "wikilink")[中華民國版圖](../Page/中華民國疆域.md "wikilink")，屬[新疆省的一部分](../Page/新疆省.md "wikilink")（参见[原中華民國法理疆域行政區劃](../Page/原中華民國法理疆域行政區劃.md "wikilink")）。

## 歷史

[thumb](../Page/image:_Zunghar_Khanate_at_1750.jpg.md "wikilink")（粉紅色）南部與[巴達赫尚](../Page/巴達赫尚.md "wikilink")（綠色）的邊界處。\]\]1895年以前，今天的戈爾諾-巴達赫尚自治州加上[阿富汗東北部](../Page/阿富汗.md "wikilink")[巴達赫尚省所屬區域由若干個小型半自治政權構成](../Page/巴達赫尚省.md "wikilink")，它們包括[達爾瓦斯](../Page/達爾瓦斯.md "wikilink")、Shughnun-Rushan、[瓦罕等](../Page/瓦罕.md "wikilink")。當時這塊區域同時被[清朝](../Page/清朝.md "wikilink")、[沙皇俄國和](../Page/沙皇俄國.md "wikilink")[阿富汗酋長國聲稱為己方領土](../Page/阿富汗酋長國.md "wikilink")。清政府聲稱擁有整個[帕米爾地區](../Page/帕米爾.md "wikilink")，但其軍隊僅僅控制了[塔什庫爾干城東面的關口](../Page/塔什庫爾干.md "wikilink")。

*1890年以前，此地居民臣服於[清朝](../Page/清朝.md "wikilink")，此地劃歸[新疆省管轄](../Page/新疆省_\(清朝\).md "wikilink")*\[1\]。19世紀後期[英俄在亞洲爭奪勢力範圍](../Page/大博弈.md "wikilink")，1890年沙俄進駐並控制了包括此地在内的[帕米爾高原北部地區](../Page/帕米爾高原.md "wikilink")，不久與清簽訂了[中俄续勘喀什噶尔界约](../Page/中俄续勘喀什噶尔界约.md "wikilink")，與英國簽訂《關於帕米爾地區勢力範圍的協議》。從滿清到民國，中國政府始終堅持對此地擁有主權，不承認沙俄和[蘇聯的佔領合法](../Page/蘇聯.md "wikilink")，但是卻無力進駐此地實行有效統治。
1990年代，[中華人民共和國政府和](../Page/中華人民共和國政府.md "wikilink")[塔吉克斯坦政府簽署邊界條約](../Page/塔吉克斯坦政府.md "wikilink")，正式承認此地屬於塔吉克斯坦所有，作為交換，塔方割讓了1100平方公里的土地，2011年邊界碑立下。\[2\]

蘇聯時代州被划成**戈爾諾—巴達赫尚自治區**（），自治區建于1925年。1929年[塔吉克蘇維埃社會主義共和國成立后](../Page/塔吉克蘇維埃社會主義共和國.md "wikilink")，該區成為共和國的一部分。1950年代，戈爾諾—巴達赫尚自治區的大量土著居民[帕米爾人被迫遷往塔吉克西南部](../Page/帕米爾人.md "wikilink")。1991年塔吉克斯坦獨立后，該區改名為“戈爾諾－巴達赫尚自治州”，當1992年塔吉克斯坦爆發[内戰時](../Page/塔吉克內戰.md "wikilink")，自治州政府宣佈獨立。在内戰期間，許多帕米爾人被塔吉克人等其他族群殺害，該州也同时成为[塔国伊斯兰政治反對派的大本營](../Page/塔吉克斯坦伊斯蘭復興黨.md "wikilink")。後來自治州政府放棄了獨立要求。

## 行政區劃

  - [萬奇縣](../Page/萬奇縣.md "wikilink")（）
  - [達爾瓦玆縣](../Page/達爾瓦玆縣.md "wikilink")（）
  - [伊斯卡希姆縣](../Page/伊斯卡希姆縣.md "wikilink")（）
  - [穆爾加布縣](../Page/穆爾加布縣.md "wikilink")（）
  - [羅什特卡拉縣](../Page/羅什特卡拉縣.md "wikilink")（）
  - [魯珊縣](../Page/魯珊縣.md "wikilink")（）
  - [賽格南縣](../Page/賽格南縣.md "wikilink")（）

## 地理

[帕米爾高原的最高處](../Page/帕米爾高原.md "wikilink")，以及[蘇聯](../Page/蘇聯.md "wikilink")[中亞細亞五座海拔](../Page/中亞細亞.md "wikilink")7,000公尺以上的山峰其中三座都位於本州：[獨立峰](../Page/列宁峰.md "wikilink")（前稱列寧峰，海拔
7,134公尺，在[吉爾吉斯邊界上](../Page/吉爾吉斯.md "wikilink")），[伊斯梅爾·薩馬尼峰](../Page/伊斯梅尔·索莫尼峰.md "wikilink")（前稱共產主義峰，海拔
7,495公尺）和[卡季尼夫斯基峰](../Page/卡季尼夫斯基峰.md "wikilink")（海拔 7,105公尺）。

## 人口

主要人口為[帕米尔人](../Page/帕米尔.md "wikilink")，少數為[吉尔吉斯人和](../Page/吉尔吉斯人.md "wikilink")[塔吉克人](../Page/塔吉克人.md "wikilink")。最大城市為[霍羅格](../Page/霍羅格.md "wikilink")（）人口
22,000；第二大城市為[末合](../Page/末合_\(塔吉克\).md "wikilink")（），人口約
4,000。戈州是多種[帕米爾語的故鄉](../Page/帕米爾語.md "wikilink")，包括[舒格南語](../Page/舒格南語.md "wikilink")、[瓦罕語](../Page/瓦罕語.md "wikilink")、[色勒库尔語等等](../Page/色勒库尔語.md "wikilink")。除此之外，俄語和[塔吉克語都在當地通用](../Page/塔吉克語.md "wikilink")，而其中末合地區有不少操[吉爾吉斯語的居民](../Page/吉爾吉斯語.md "wikilink")。當地主要宗教為[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")[伊斯玛仪派](../Page/伊斯玛仪派.md "wikilink")，對[阿迦汗的尊崇十分流行](../Page/阿迦汗.md "wikilink")。

## 交通

對外聯繫有四條路線，分別是霍羅格—[奧什](../Page/奧什.md "wikilink")、霍羅格—[杜尚別](../Page/杜尚別.md "wikilink")（前兩者路況較佳，屬[帕米爾公路一部份](../Page/帕米爾公路.md "wikilink")）、霍羅格—[塔什庫爾干](../Page/塔什庫爾干.md "wikilink")（中經[庫里瑪口岸](../Page/庫里瑪口岸.md "wikilink")）和霍羅格—[瓦罕走廊](../Page/瓦罕走廊.md "wikilink")—[阿富汗邊界](../Page/阿富汗.md "wikilink")。

## 注释

## 参考文献

## 外部連結

## 参见

  - [帕米尔高原](../Page/帕米尔高原.md "wikilink")

{{-}}

[Category:塔吉克行政區劃](../Category/塔吉克行政區劃.md "wikilink")
[Category:中華民國爭議地區](../Category/中華民國爭議地區.md "wikilink")
[Category:1925年建立的行政區劃](../Category/1925年建立的行政區劃.md "wikilink")

1.  [中国近代史课程教案](http://www.hebtu.edu.cn/col90/col157/col159/article.htm1?id=2036)

2.  [中吉边界的现代“丝绸之路”](http://news.sohu.com/20050616/n225972452.shtml)