**成功時代廣場**（）是一座由一間大型[購物中心和兩座](../Page/購物中心.md "wikilink")[酒店大樓所組成的一座大型建築](../Page/酒店.md "wikilink")，位於[吉隆坡](../Page/吉隆坡.md "wikilink")[武吉免登商圈內](../Page/武吉免登.md "wikilink")，由[成功集團所興建和經營](../Page/成功集團.md "wikilink")。是[馬來西亞第二大](../Page/馬來西亞.md "wikilink")，[亞洲排名第五及世界排名第六的](../Page/亞洲.md "wikilink")[購物中心](../Page/購物中心.md "wikilink")，每月進出這棟建築的人次多達300萬人以上。

## 背景

[Berjaya_Times_Square_atrium.jpg](https://zh.wikipedia.org/wiki/File:Berjaya_Times_Square_atrium.jpg "fig:Berjaya_Times_Square_atrium.jpg")
成功時代廣場是全世界第八大單體建築面積最大的建物，總面積達700,000平方米
(7,500,000平方呎)，建築底部是擁有超過1,000座單位的購物中心，其中包括65間[食肆](../Page/食肆.md "wikilink")、9間[戲院](../Page/戲院.md "wikilink")、1間3D影院、[保齡球場](../Page/保齡球場.md "wikilink")、[卡拉OK中心](../Page/卡拉OK.md "wikilink")、會展中心、亞洲最大的室內遊樂場，是一座結合購物、[休閒](../Page/休閒.md "wikilink")、餐飲、娛樂等項目的多功能大型[購物中心](../Page/商場.md "wikilink")，上方的兩座45樓高的建築則是擁有超過1,200間客房的成功時代廣場酒店（Berjaya
Times Square Hotel）。

建築前身的所在地為[馬來亞](../Page/馬來亞.md "wikilink")20世紀初的錫礦大亨——[張郁才的故居](../Page/張郁才.md "wikilink")，該建築在二戰期間被日軍佔領，其後院曾作為[防空洞](../Page/防空洞.md "wikilink")。在[張郁才過世後](../Page/張郁才.md "wikilink")，其後裔將此地賣給成功集團。耗資10億[令吉的成功時代廣場原本預計在](../Page/令吉.md "wikilink")1998年開幕，但由於受1997年[亞洲金融風暴的影響](../Page/亞洲金融風暴.md "wikilink")，一度停工四年，而拖至2003年9月開幕。

開幕期間，[馬來西亞的景氣才剛剛復甦](../Page/馬來西亞.md "wikilink")，加上購物中心的面積過於廣大，樓層數過多（共有13樓），當時整個購物中心內只有150間店家開幕。至今購物中心的高樓層（6樓以上）依舊人潮稀少，部分單位被[中東人租下作為旅行社和服務阿拉伯人士的機構](../Page/中東.md "wikilink")，其他大部分則被自行吸收，作為該集團旗下公司（如[星巴克](../Page/星巴克.md "wikilink")、[7-Eleven](../Page/7-Eleven.md "wikilink")、[溫蒂漢堡](../Page/溫蒂漢堡.md "wikilink")、[Krispy
Kreme](../Page/Krispy_Kreme.md "wikilink")、等業務）的辦公室和服務學習中心，以及的校園。購物中心的部分低樓層原預計承租給高端精品品牌，隨著招商失敗以及許多單位在[亞洲金融風暴期間販售出去作為籌集建造工程的資金](../Page/亞洲金融風暴.md "wikilink")，使得早期購物中心內部的店家沒有完善規劃，每個樓層都沒有主題性，加上成功集團讓旗下品牌大量進駐，減低購物中心的空置率，使得店家的重複率頗高。

在2005年後，景氣開始回溫，加上[吉隆坡單軌](../Page/吉隆坡單軌.md "wikilink")（KL
Monorail）的開通等因素，為購物中心帶來大量的人潮，成為[吉隆坡市區內最熱鬧的購物中心](../Page/吉隆坡.md "wikilink")。購物中心的店家主要以平價服飾、電影院、卡啦ok等娛樂設備為主，不少的服飾商品的價格都從10[令吉起跳](../Page/令吉.md "wikilink")，其顧客群為以中學生為主的年青族群、遊客為主。

## 商場內部

### IT CENTRE

原為來自[英國的Debenhams百貨](../Page/英國.md "wikilink")，後改為美羅百貨(Metrojaya)進駐,
但購物中心內的消費者多以低收入的年輕族群為主，[百貨公司內的中高價商品無法吸引年輕族群消費](../Page/百貨公司.md "wikilink")，因此繼而倒閉，改由販賣3C產品的IT
Centre 所取代，但無法跟臨近的[劉蝶廣場](../Page/劉蝶廣場.md "wikilink")（Plaza Low
Yat）所競爭而慘淡經營，目前樓下的單位則改由[Uniqlo進駐](../Page/Uniqlo.md "wikilink")。

### Borders

2005年四月，來自[美國的大型連鎖書店](../Page/美國.md "wikilink")——[博德斯集團](../Page/博德斯集團.md "wikilink")（Borders）在廣場內的2至3樓開設了當時該集團在全球面積最大的書店，也是[馬來西亞的最大書店](../Page/馬來西亞.md "wikilink")，主要是以英文書籍為主，當時書店下層設有[星巴克咖啡館及上層有本地的](../Page/星巴克.md "wikilink")[大將書行進駐](../Page/大將書行.md "wikilink")，專售賣來自台、中及本地的中文書籍。隨著2010年的[經濟大衰退導致該集團面臨](../Page/經濟大衰退.md "wikilink")[破產的情況](../Page/破產.md "wikilink")，其在[馬來西亞的業務被成功集團收購](../Page/馬來西亞.md "wikilink")，且書店的面積被大幅縮小，並遷至廣場內的地下一樓，該稱為Borders
Express，原本書店的位置改為1st Avenue，專承租給經驗販售各式配飾的店家。

### GSC影院(GSC Cinema)

內有9間戲院，是GSC電影院在武吉免等(Bukit Bintang)的第一間影院，在購物中心的十樓也開設了GSC
MAXX，該院廳的熒幕長達23公尺，是馬來西亞最大的電影螢幕。

### Cosmo's World室內遊樂場

[Berjaya_Times_Square_Indoor_Theme_Park.JPG](https://zh.wikipedia.org/wiki/File:Berjaya_Times_Square_Indoor_Theme_Park.JPG "fig:Berjaya_Times_Square_Indoor_Theme_Park.JPG")
是亞洲最大的室內遊樂場，內有亞洲最長的[雲霄飛車軌道](../Page/雲霄飛車.md "wikilink")、旋轉木馬、跳樓機等。遊樂院內分為兩個部份即為Fantasy
Island及Galaxy Station,後者需要年齡超過13歲及身高達到一定的標準方可進入。

### Tiny Taipei

購物中心3樓電影院前的區域被規劃為“Tiny
Taipei（小台北）”，並仿造[台北夜市的景色](../Page/台北.md "wikilink")，以販賣服飾及日式和台式的食物為主。

## 啦啦文化

[馬來西亞](../Page/馬來西亞.md "wikilink")[華裔族群習慣將穿著奇裝異服](../Page/華裔.md "wikilink")，自以為很新潮年輕男女稱為**啦啦仔**和**啦啦妹**，他們說穿著的服裝則稱為“啦啦裝”(用[粵語發音](../Page/粵語.md "wikilink"))。原先**啦啦族群**多聚集在[金河廣場](../Page/金河廣場.md "wikilink")，隨著成功時代廣場的店家越來越多，且售賣的商品多以潮流物品為主并价格便宜，亦有許多針對青少年的娛樂場所，[金河廣場也逐漸沒落](../Page/金河廣場.md "wikilink")，因此**啦啦族群**也開始大量流入成功時代廣場內。

## 交通

[吉隆坡单轨列车](../Page/吉隆坡单轨列车.md "wikilink")（KL
Monorail）的[燕美站位於商場旁](../Page/燕美站.md "wikilink")，有行人天橋可連接對面的成功廣場（Berjaya
Plaza），在商場的另一側也蓋有冷氣天橋，讓行人跨越車流如織的燕美路（Jalan
Imbi）至對面的[燕美廣場](../Page/燕美廣場.md "wikilink")（Imbi
Plaza）、[金河廣場](../Page/金河廣場.md "wikilink")（Sungei Wang
Plaza）和[劉蝶廣場](../Page/劉蝶廣場.md "wikilink")（Low Yat Plaza）等。

## 參考資料

  - [1](http://www.timessquarekl.com/)
  - [2](http://www.mytravel.com.my/map/goldentriangle-map/time-square)

## 外部連結

  - [官方網站](http://www.timessquarekl.com/)

[Category:双塔楼](../Category/双塔楼.md "wikilink")
[Category:2003年完工建築物](../Category/2003年完工建築物.md "wikilink")
[Category:馬來西亞旅遊](../Category/馬來西亞旅遊.md "wikilink")
[Category:吉隆坡購物中心](../Category/吉隆坡購物中心.md "wikilink")
[Category:武吉免登](../Category/武吉免登.md "wikilink")
[Category:吉隆坡建築物](../Category/吉隆坡建築物.md "wikilink")
[Category:馬來西亞摩天大樓](../Category/馬來西亞摩天大樓.md "wikilink")
[Category:2003年馬來西亞建立](../Category/2003年馬來西亞建立.md "wikilink")