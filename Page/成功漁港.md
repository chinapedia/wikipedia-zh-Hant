[2010_07_13430_6402_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG](https://zh.wikipedia.org/wiki/File:2010_07_13430_6402_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG "fig:2010_07_13430_6402_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG")
[2010_07_13640_6388_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG](https://zh.wikipedia.org/wiki/File:2010_07_13640_6388_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG "fig:2010_07_13640_6388_Chenggong_Chenggong_Fishing_Harbor_Taiwan.JPG")
**成功漁港**，又名**新港漁港**，位於[台東縣](../Page/台東縣.md "wikilink")[成功鎮](../Page/成功鎮.md "wikilink")，且與[富岡漁港為](../Page/富岡漁港.md "wikilink")[臺灣東海岸](../Page/臺灣.md "wikilink")[近海漁業的重要基地](../Page/近海漁業.md "wikilink")，屬第二級漁港，以海產及[柴魚聞名](../Page/柴魚.md "wikilink")。

成功漁港其相對於[成廣澳港而言](../Page/成廣澳港.md "wikilink")，是較晚開發的港口，所以，當地人又將之稱為**新港**，由於有天然的海灣地形，因此開發的歷史可遠溯自[日治時期](../Page/日治時期.md "wikilink")，後來在1929年至1932年期間曾將原有漁港予以大規模整建，挖深與浚疏航道，最近的大型疏濬及建設則是在1979年，當時台灣省政府曾再次加以擴建，變成今日的模樣。

成功漁港是[新港區漁會所屬的漁港中規模最大的漁港](../Page/新港區漁會.md "wikilink")，除提供漁船停泊外，並可供附近地區漁船拍賣漁獲，主要漁獲包含有[鬼頭刀](../Page/鬼頭刀.md "wikilink")、黃鰭[鮪](../Page/鮪.md "wikilink")、雨傘[旗魚](../Page/旗魚.md "wikilink")、正[鰹等](../Page/鰹.md "wikilink")，魚市場每日下午三時左右拍賣漁獲，港區內並有漁貨直銷市場可供一般遊客採買生鮮漁獲，亦設有熟食區可就地將所購得的新鮮漁獲交予店家料理。

除了發展漁業之外，此港口還兼具觀光功能。交通部觀光局東部海岸管理處在這裡設有旅客服務中心與浮動碼頭設施，為一條件頗佳之休閒漁港，港內並有豪華舒適遊艇，可載客出海賞鯨、賞景。

## 歷史

1908年[總督府通信局長](../Page/總督府.md "wikilink")[鹿子木小五郎考察東部地區後](../Page/鹿子木小五郎.md "wikilink")，便提出建港於[麻荖漏的看法](../Page/麻荖漏.md "wikilink")。1929年新港漁港動工，1932年完工。當中歷經了關東大地震帶來的經濟恐慌，台灣總督府配合財政緊縮而新港建港工程因此停工，復工後預算又遭刪減。1941年漁港開闢內港1954年新港漁港升格為**省內港**。1988年起展開的擴建工程於1996年完工，目前是新港漁港建港以來最大規模，也是東臺灣最大漁港。\[1\]

## 爭議

1927年，總督府交通局工事部打狗出張所主張開發新港，估計至少有五萬人於此發展，新港築港消息發佈後，花蓮方面認為會因此衰退，而台東方面認為會因此完沒，原計畫花費六、七百萬圓築為商港，而因群眾抗議而作罷。\[2\]

## 參考資料

## 外部連結

  - [旅行台灣‧說自己的故事
    成功漁港](http://www.clicktaiwan.com.tw/taiwan/2016/index.jsp)

[category:台灣漁港](../Page/category:台灣漁港.md "wikilink")

[Category:台東縣交通](../Category/台東縣交通.md "wikilink")
[Category:臺東縣漁港](../Category/臺東縣漁港.md "wikilink")
[Category:成功鎮](../Category/成功鎮.md "wikilink")
[Category:台灣日治時期產業建築](../Category/台灣日治時期產業建築.md "wikilink")
[Category:1932年建立](../Category/1932年建立.md "wikilink")

1.
2.