**成县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省东南部](../Page/甘肃省.md "wikilink")、[西汉水北岸](../Page/西汉水.md "wikilink")，是[陇南市下属的一个](../Page/陇南市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。成县是甘肃省的主要产粮县之一，有“陇右粮仓”之称。

## 历史

成县先秦时是[西戎聚居地](../Page/西戎.md "wikilink")。战国时，[白马氐在此居住](../Page/白马氐.md "wikilink")。秦朝属[陇西郡](../Page/陇西郡.md "wikilink")。西汉时属[武都郡](../Page/武都郡.md "wikilink")[下辨道管辖](../Page/下辨道.md "wikilink")。东汉时移武都郡治到下辨。[三国](../Page/三国.md "wikilink")[刘备夺取](../Page/刘备.md "wikilink")[汉中后](../Page/汉中.md "wikilink")，[曹操徙武都郡居民至](../Page/曹操.md "wikilink")[扶风郡](../Page/扶风郡.md "wikilink")，蜀汉遂占领此地。[后魏太平真君七年](../Page/后魏.md "wikilink")，改武都郡为[仇池镇](../Page/仇池镇.md "wikilink")。太和十二年，析仇池镇设置[渠州](../Page/渠州.md "wikilink")。正始初年，改为[南秦州](../Page/南秦州.md "wikilink")。[西魏改称](../Page/西魏.md "wikilink")[成州](../Page/成州.md "wikilink")。隋朝初年废郡存州，仍称成州。隋大业初年，改成州为[汉阳郡](../Page/汉阳郡.md "wikilink")。唐朝仍称成州。天宝初年改名为[同谷郡](../Page/同谷郡.md "wikilink")。乾元初复改称成州。[安史之乱后被](../Page/安史之乱.md "wikilink")[吐蕃占领](../Page/吐蕃.md "wikilink")。咸通中年被唐朝收复，仍称成州，治[同谷县](../Page/同谷县.md "wikilink")。五代[后梁初](../Page/后梁.md "wikilink")，为避[朱诚讳改称](../Page/朱诚.md "wikilink")[汶州](../Page/汶州.md "wikilink")。贞明初年被[后蜀占领](../Page/后蜀.md "wikilink")。宋灭后蜀之后又改名为成州。南宋时成州是抗金前线，[四川](../Page/四川.md "wikilink")[经略安抚使驻地](../Page/经略安抚使.md "wikilink")，宝庆初年升为[同庆府](../Page/同庆府.md "wikilink")。元灭宋，改同庆府为成州。明初，改成州为成县，属[巩昌府直辖](../Page/巩昌府.md "wikilink")。\[1\]清朝仍称成县，改属[阶州直隶州](../Page/阶州直隶州.md "wikilink")。

## 自然环境

成县介于[秦岭和](../Page/秦岭.md "wikilink")[巴山之间](../Page/巴山.md "wikilink")，属于丘陵地形。有[嘉陵江支流](../Page/嘉陵江.md "wikilink")[青泥河穿城而过](../Page/青泥河.md "wikilink")。

## 行政区划

下辖14个[镇](../Page/镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 经济

成县是国务院批准的对外开放县，拥有亚洲第二大铅锌矿带：西成铅锌矿带。\[2\]
工业主要有[采矿业](../Page/采矿业.md "wikilink")、[水泥](../Page/水泥.md "wikilink")、[酿酒等](../Page/酿酒.md "wikilink")。

## 交通

### 公路

、、、。

### 航空

[陇南成州机场](../Page/陇南成州机场.md "wikilink")

## 名胜古迹

境内有国家级重点保护文物汉隶《[西狭颂](../Page/西峡颂摩崖石刻.md "wikilink")》摩崖石刻；诗圣杜甫流寓同谷纪念地[杜少陵祠](../Page/杜少陵祠.md "wikilink")；南宋抗金名将[吴挺陵园](../Page/吴挺陵园.md "wikilink")；以及国家级森林公园[鸡峰山](../Page/鸡峰山国家森林公园.md "wikilink")、唐韵遗风[裴公莲沼等人文景观](../Page/裴公湖.md "wikilink")，

## 参考资料

[成县](../Category/成县.md "wikilink") [县](../Category/陇南区县.md "wikilink")
[陇南](../Category/甘肃省县份.md "wikilink")

1.  《[读史方舆纪要](../Page/读史方舆纪要.md "wikilink")/陕西八》：古西戎地。战国时，白马氐居此。秦属陇西郡。汉为武都郡下辨道地。后汉为武都郡治。晋因之。后魏太平真君七年，改置仇池镇。太和十二年，兼置渠州。正始初，改为南秦州。西魏曰成州。隋初，郡废，州存。大业初，改州为汉阳郡。唐亦曰成州。天宝初曰同谷郡。乾元初复故。皆治上禄县。大历后，没于吐蕃。咸通中，复置成州，治同谷县。五代梁初，改曰汶州，李茂贞为朱温父讳也。贞明初，地入于蜀。后唐复曰成州。宋因之。宝庆初，以尝为潜邸升为同庆府。元仍曰成州，以州治同谷县省入。明初，改州为县。
2.  [成县县情概况](http://www.gscx.gov.cn/lygk.html)