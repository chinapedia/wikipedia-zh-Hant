[Old_Sungkyunkwan_map_from_1785.jpg](https://zh.wikipedia.org/wiki/File:Old_Sungkyunkwan_map_from_1785.jpg "fig:Old_Sungkyunkwan_map_from_1785.jpg")
[Old_Sungkyunkwan.JPG](https://zh.wikipedia.org/wiki/File:Old_Sungkyunkwan.JPG "fig:Old_Sungkyunkwan.JPG")
**成均館**（）是[朝鮮半島歷代王朝最高學府](../Page/朝鮮半島.md "wikilink")，地位同等於[古代中國的](../Page/中國歷史.md "wikilink")[國子監](../Page/國子監.md "wikilink")、[太學](../Page/太學.md "wikilink")。[高麗王朝的成均館位於現今](../Page/高麗王朝.md "wikilink")[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")[開城](../Page/開城.md "wikilink")，而[朝鮮王朝的成均館位於現今](../Page/朝鮮王朝.md "wikilink")[大韓民國](../Page/大韓民國.md "wikilink")[首爾](../Page/首爾.md "wikilink")。現成均館為三星集團旗下企業大學。

2013年6月23日，[聯合國教科文組織在柬埔寨舉行的第](../Page/聯合國教科文組織.md "wikilink")37屆世界遺產大會，公佈把開城歷史古蹟（包括開城區、滿月台、開城南大門、成均館、嵩陽書院、王建皇陵和恭愍皇陵）列入世界文化遺產。

## 建築

  - 佔地約1萬平方公呎，共有18棟建築物；
  - [明倫堂](../Page/明倫堂.md "wikilink")：位於中央，用作講堂；
  - [東齋及](../Page/東齋.md "wikilink")[西齋](../Page/西齋.md "wikilink")：位於明倫堂的兩旁，它們是集體[宿舍](../Page/宿舍.md "wikilink")；
  - 已生長了1000多年的[櫸樹及](../Page/櫸.md "wikilink")[銀杏樹](../Page/銀杏.md "wikilink")

## 成立歷史

成均一詞源於《[周禮](../Page/周禮.md "wikilink")·春官·大司樂》：「大司樂掌成均之法，以治建國之學政，而合國之子弟焉。」。原為周代的貴族大學之一。[鄭玄注](../Page/鄭玄.md "wikilink")：「[董仲舒曰](../Page/董仲舒.md "wikilink")：五帝名大學曰成均。」。成均泛稱官設的最高學府。

在古代[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[高麗時代](../Page/高麗.md "wikilink")（公元918年至1392年），為了培養人才為國家的未來發展，在公元992年建立了最高教育機關[國子監](../Page/國子監.md "wikilink")，後來改組成為成均館。隨著高麗王朝被朝鮮王朝取代、定都漢陽（今首爾特別市），朝鮮太宗[李芳遠將新成均館建立於都內](../Page/李芳遠.md "wikilink")，也就是現今的首爾[成均館大學](../Page/成均館大學.md "wikilink")。

## 當時的職責

[高麗政府委任飽讀中國的](../Page/高麗.md "wikilink")[儒家思想學者來經營](../Page/儒家思想.md "wikilink")，對學生傳授：[儒家思想](../Page/儒家思想.md "wikilink")、[高麗](../Page/高麗.md "wikilink")[法律](../Page/法律.md "wikilink")、[數學及](../Page/數學.md "wikilink")[書法等科目](../Page/書法.md "wikilink")。

## 成就

  - 製造了[國際聞名的高麗](../Page/國際.md "wikilink")[青瓷](../Page/青瓷.md "wikilink")；
  - 製造了顏色雪白、質地堅韌的優質[紙張](../Page/紙張.md "wikilink")；
  - 除[中國外](../Page/中國.md "wikilink")，當時世界上唯一使用活字印刷技術；
  - 研究出在[朝鮮半島栽種](../Page/朝鮮半島.md "wikilink")[棉花的方法](../Page/棉花.md "wikilink")。

## 現況

在-{zh-hant:南北韓;zh-hans:朝韩}-雙方政府的保護歷史文物政策下，各建築物都保存良好，現今分別為北韓[高麗成均館輕工業綜合大學與南韓](../Page/高麗成均館輕工業綜合大學.md "wikilink")[成均館大學](../Page/成均館大學.md "wikilink")。南韓[成均館大學以](../Page/成均館大學.md "wikilink")[三星集團為贊助財團](../Page/三星集團.md "wikilink")，並為韓國前五大學之一。

## 參见

  - [成均館大學](../Page/成均館大學.md "wikilink")
  - [高麗成均館輕工業綜合大學](../Page/高麗成均館輕工業綜合大學.md "wikilink")

[Category:高麗建築](../Category/高麗建築.md "wikilink")
[Category:朝鮮民主主義人民共和國旅遊景點](../Category/朝鮮民主主義人民共和國旅遊景點.md "wikilink")
[Category:朝鲜民主主义人民共和国世界遗产](../Category/朝鲜民主主义人民共和国世界遗产.md "wikilink")
[Category:黃海北道建築物](../Category/黃海北道建築物.md "wikilink")
[Category:朝鮮王朝建築](../Category/朝鮮王朝建築.md "wikilink")
[Category:首爾旅遊景點](../Category/首爾旅遊景點.md "wikilink")
[Category:韓國世界遺產](../Category/韓國世界遺產.md "wikilink")
[Category:首爾建築物](../Category/首爾建築物.md "wikilink")
[Category:朝鮮古代學校](../Category/朝鮮古代學校.md "wikilink")
[Category:國學 (學府)](../Category/國學_\(學府\).md "wikilink")
[Category:開城特級市建築物](../Category/開城特級市建築物.md "wikilink")