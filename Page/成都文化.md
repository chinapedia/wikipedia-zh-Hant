## 民俗

*相关条目列表*

  - **[川剧](../Page/川剧.md "wikilink")**：[变脸](../Page/变脸.md "wikilink")-[吐火](../Page/吐火.md "wikilink")-[滚灯](../Page/滚灯.md "wikilink")

<!-- end list -->

  - **节日**：[成都花会](../Page/成都花会.md "wikilink")-[成都庙会](../Page/成都庙会.md "wikilink")-[游喜神方](../Page/游喜神方.md "wikilink")

<!-- end list -->

  - **文艺**：[谐剧](../Page/谐剧.md "wikilink")-[散打](../Page/散打.md "wikilink")-[金钱板](../Page/金钱板.md "wikilink")-[竹琴](../Page/竹琴.md "wikilink")-[清音](../Page/清音.md "wikilink")-[精木偶](../Page/精木偶.md "wikilink")-[扬琴](../Page/扬琴.md "wikilink")

<!-- end list -->

  - **游戏**：[扯响簧](../Page/扯响簧.md "wikilink")-[掺牛牛](../Page/掺牛牛.md "wikilink")-[滚铁环](../Page/滚铁环.md "wikilink")-[逮猫儿](../Page/逮猫儿.md "wikilink")-[倒糖饼](../Page/倒糖饼.md "wikilink")-[川牌](../Page/川牌.md "wikilink")-

<!-- end list -->

  - **其它**：[茶馆文化](../Page/茶馆文化.md "wikilink")-[麻将](../Page/麻将.md "wikilink")-[滑杆](../Page/滑杆.md "wikilink")-[鸡公车](../Page/鸡公车.md "wikilink")

<!-- end list -->

  - **特产**：[十色笺](../Page/十色笺.md "wikilink")-[薛涛笺](../Page/薛涛笺.md "wikilink")-[蜀笺](../Page/蜀笺.md "wikilink")-
    [竹丝瓷胎](../Page/竹丝瓷胎.md "wikilink")-[银丝工艺](../Page/银丝工艺.md "wikilink")

<File:Spit> Fire.JPG|吐火 <File:Roll> Lamp.JPG|滚灯

## 宗教

  - 佛教：[文殊院](../Page/文殊院.md "wikilink") [大慈寺](../Page/大慈寺.md "wikilink")
    [昭觉寺](../Page/昭觉寺.md "wikilink") [石经寺](../Page/石经寺.md "wikilink")
  - 道教：[青羊宫](../Page/青羊宫.md "wikilink") [上清宫](../Page/上清宫.md "wikilink")
  - 伊斯兰教：[皇城清真寺](../Page/皇城清真寺.md "wikilink")
    [古楼街清真寺](../Page/古楼街清真寺.md "wikilink")
    [土桥清真寺](../Page/土桥清真寺.md "wikilink")
    [牟尼镇清真寺](../Page/牟尼镇清真寺.md "wikilink")
  - 天主教： [后子门天主教堂](../Page/后子门天主教堂.md "wikilink")
    [平安桥主教座堂](../Page/平安桥主教座堂.md "wikilink")

## 饮食

*相关条目列表*

  - **[川菜](../Page/川菜.md "wikilink")**：[回锅肉](../Page/回锅肉.md "wikilink")-[宫爆鸡丁](../Page/宫爆鸡丁.md "wikilink")-[麻婆豆腐](../Page/麻婆豆腐.md "wikilink")-[开水白菜](../Page/开水白菜.md "wikilink")-[鱼香肉丝](../Page/鱼香肉丝.md "wikilink")

<!-- end list -->

  - **[成都小吃](../Page/成都小吃.md "wikilink")**：[夫妻肺片](../Page/夫妻肺片.md "wikilink")-[钟水饺](../Page/钟水饺.md "wikilink")-[龙抄手](../Page/龙抄手.md "wikilink")-[赖汤圆](../Page/赖汤圆.md "wikilink")-[韩包子](../Page/韩包子.md "wikilink")-[糖油果子](../Page/糖油果子.md "wikilink")-[三大炮](../Page/三大炮.md "wikilink")-[三和泥](../Page/三和泥.md "wikilink")-[担担面](../Page/担担面.md "wikilink")

<!-- end list -->

  - **特产**：[四川泡菜](../Page/四川泡菜.md "wikilink")-[青山绿水](../Page/青山绿水.md "wikilink")（茶）-[蟹目香珠](../Page/蟹目香珠.md "wikilink")（茶）

## 知名人物

<table>
<tbody>
<tr class="odd">
<td><p><strong>出生于成都的人物</strong></p>
<ul>
<li><strong>古代人物</strong>
<ul>
<li><a href="../Page/司马相如.md" title="wikilink">司马相如</a></li>
<li><a href="../Page/花蕊夫人.md" title="wikilink">花蕊夫人</a></li>
<li><a href="../Page/卓文君.md" title="wikilink">卓文君</a></li>
<li><a href="../Page/薛涛.md" title="wikilink">薛涛</a></li>
<li><a href="../Page/杨雄.md" title="wikilink">杨雄</a></li>
<li><a href="../Page/鄧通.md" title="wikilink">鄧通</a></li>
<li><a href="../Page/王褒.md" title="wikilink">王褒</a></li>
<li><a href="../Page/杨升庵.md" title="wikilink">杨升庵</a></li>
<li><a href="../Page/严君平.md" title="wikilink">严君平</a></li>
<li><a href="../Page/何武.md" title="wikilink">何武</a></li>
<li><a href="../Page/罗裒.md" title="wikilink">罗裒</a></li>
<li><a href="../Page/蒲元.md" title="wikilink">蒲元</a></li>
<li><a href="../Page/杜弢.md" title="wikilink">杜弢</a></li>
<li><a href="../Page/常璩.md" title="wikilink">常璩</a></li>
<li><a href="../Page/袁天罡.md" title="wikilink">袁天罡</a></li>
<li><a href="../Page/欧阳炯.md" title="wikilink">欧阳炯</a></li>
<li><a href="../Page/房纵真.md" title="wikilink">房纵真</a></li>
<li><a href="../Page/李昇.md" title="wikilink">李昇</a></li>
<li><a href="../Page/黄筌.md" title="wikilink">黄筌</a></li>
<li><a href="../Page/黄居采.md" title="wikilink">黄居采</a></li>
<li><a href="../Page/阮知海.md" title="wikilink">阮知海</a></li>
<li><a href="../Page/彭晓.md" title="wikilink">彭晓</a></li>
<li><a href="../Page/王小波.md" title="wikilink">王小波</a></li>
<li><a href="../Page/李顺.md" title="wikilink">李顺</a></li>
<li><a href="../Page/范镇.md" title="wikilink">范镇</a></li>
<li><a href="../Page/范冲.md" title="wikilink">范冲</a></li>
<li><a href="../Page/王眭.md" title="wikilink">王眭</a></li>
<li><a href="../Page/张唐英.md" title="wikilink">张唐英</a></li>
<li><a href="../Page/张商英.md" title="wikilink">张商英</a></li>
<li><a href="../Page/范祖禹.md" title="wikilink">范祖禹</a></li>
<li><a href="../Page/宇文虚中.md" title="wikilink">宇文虚中</a></li>
<li><a href="../Page/黄休复.md" title="wikilink">黄休复</a></li>
<li><a href="../Page/费著.md" title="wikilink">费著</a></li>
<li><a href="../Page/杨廷和.md" title="wikilink">杨廷和</a></li>
<li><a href="../Page/杨慎.md" title="wikilink">杨慎</a></li>
<li><a href="../Page/日章.md" title="wikilink">日章</a></li>
<li><a href="../Page/何卿.md" title="wikilink">何卿</a></li>
<li><a href="../Page/费密.md" title="wikilink">费密</a></li>
<li><a href="../Page/岳钟琪.md" title="wikilink">岳钟琪</a></li>
<li><a href="../Page/魏长生.md" title="wikilink">魏长生</a></li>
<li><a href="../Page/杨遇春.md" title="wikilink">杨遇春</a></li>
<li><a href="../Page/谢朝恩.md" title="wikilink">谢朝恩</a></li>
<li><a href="../Page/肖楷成.md" title="wikilink">肖楷成</a></li>
<li><a href="../Page/刘咸荣.md" title="wikilink">刘咸荣</a></li>
<li><a href="../Page/吴虞.md" title="wikilink">吴虞</a></li>
</ul></li>
<li><strong>近现代人物</strong>
<ul>
<li><a href="../Page/巴金.md" title="wikilink">巴金</a></li>
<li><a href="../Page/琼瑶.md" title="wikilink">琼瑶</a></li>
<li><a href="../Page/李劼人.md" title="wikilink">李劼人</a></li>
<li><a href="../Page/流沙河_(文人).md" title="wikilink">流沙河</a></li>
<li><a href="../Page/艾芜.md" title="wikilink">艾芜</a></li>
<li><a href="../Page/李鹏.md" title="wikilink">李鹏</a></li>
<li><a href="../Page/邓捷.md" title="wikilink">邓捷</a>：演员</li>
<li><a href="../Page/刘永好.md" title="wikilink">刘永好</a></li>
<li><a href="../Page/郭峰.md" title="wikilink">郭峰</a>：歌手</li>
<li><a href="../Page/刘心武.md" title="wikilink">刘心武</a>：作家</li>
<li><a href="../Page/沈伐.md" title="wikilink">沈伐</a></li>
<li><a href="../Page/李伯清.md" title="wikilink">李伯清</a></li>
<li><a href="../Page/李宇春.md" title="wikilink">李宇春</a></li>
<li><a href="../Page/张靓颖.md" title="wikilink">张靓颖</a></li>
<li><a href="../Page/郭蓉华.md" title="wikilink">郭蓉华</a>：画家</li>
<li><a href="../Page/杨莲.md" title="wikilink">杨莲</a>：围棋手</li>
<li><a href="../Page/余东风.md" title="wikilink">余东风</a></li>
<li><a href="../Page/刀郎_(歌手).md" title="wikilink">刀郎</a></li>
<li><a href="../Page/刘仪伟.md" title="wikilink">刘仪伟</a></li>
<li><a href="../Page/郑洁.md" title="wikilink">郑洁</a></li>
<li><a href="../Page/陈彼得.md" title="wikilink">陈彼得</a></li>
<li><a href="../Page/郑时龄.md" title="wikilink">郑时龄</a> ：院士，建筑学家</li>
<li><a href="../Page/赵尔宓.md" title="wikilink">赵尔宓</a> ：院士，动物学家</li>
<li><a href="../Page/廖昌永.md" title="wikilink">廖昌永</a></li>
<li><a href="../Page/刘适兰.md" title="wikilink">刘适兰</a></li>
<li><a href="../Page/孔祥明.md" title="wikilink">孔祥明</a></li>
<li><a href="../Page/张蓉芳.md" title="wikilink">张蓉芳</a></li>
<li><a href="../Page/胡坤.md" title="wikilink">胡坤</a></li>
<li><a href="../Page/王辉耀.md" title="wikilink">王辉耀</a></li>
<li><a href="../Page/韩素音.md" title="wikilink">韩素音</a></li>
<li><a href="../Page/王光祈.md" title="wikilink">王光祈</a></li>
<li><a href="../Page/田家英.md" title="wikilink">田家英</a></li>
<li><a href="../Page/刘正成.md" title="wikilink">刘正成</a></li>
<li><a href="../Page/聂荣政.md" title="wikilink">聂荣政</a></li>
<li><a href="../Page/刘伯承.md" title="wikilink">刘伯承</a></li>
<li><a href="../Page/陈洪范.md" title="wikilink">陈洪范</a></li>
<li><a href="../Page/刘文辉.md" title="wikilink">刘文辉</a></li>
<li><a href="../Page/刘湘.md" title="wikilink">刘湘</a></li>
<li><a href="../Page/彭家珍.md" title="wikilink">彭家珍</a></li>
<li><a href="../Page/余杰.md" title="wikilink">余杰</a></li>
</ul></li>
</ul></td>
<td><p><strong>与成都有渊源的人物</strong></p>
<p>:*<a href="../Page/杜甫.md" title="wikilink">杜甫</a></p>
<p>:*<a href="../Page/诸葛亮.md" title="wikilink">诸葛亮</a></p>
<p>:*<a href="../Page/刘备.md" title="wikilink">刘备</a></p>
<p>:*<a href="../Page/王建_(前蜀).md" title="wikilink">王建</a></p>
<p>:*<a href="../Page/孟知祥.md" title="wikilink">孟知祥</a></p>
<p>:*<a href="../Page/苏东坡.md" title="wikilink">苏东坡</a></p>
<p>:*<a href="../Page/石达开.md" title="wikilink">石达开</a></p>
<p>:*<a href="../Page/邓小平.md" title="wikilink">邓小平</a></p>
<p>:*<a href="../Page/陈毅.md" title="wikilink">陈毅</a></p>
<p>:*<a href="../Page/王小丫.md" title="wikilink">王小丫</a></p>
<p>:*<a href="../Page/刘文彩.md" title="wikilink">刘文彩</a></p>
<p>:*<a href="../Page/李雪梅.md" title="wikilink">李雪梅</a></p>
<p>:*<a href="../Page/高敏.md" title="wikilink">高敏</a></p>
<p>:*<a href="../Page/张翔.md" title="wikilink">张翔</a></p>
<p>:*<a href="../Page/郭沫若.md" title="wikilink">郭沫若</a></p>
<p>:*<a href="../Page/李白.md" title="wikilink">李白</a></p>
<p>:*<a href="../Page/张大千.md" title="wikilink">张大千</a></p>
<p>:*<a href="../Page/凌子风.md" title="wikilink">凌子风</a></p>
<p>:*<a href="../Page/林青霞.md" title="wikilink">林青霞</a></p>
<p>:*<a href="../Page/海灯法师.md" title="wikilink">海灯法师</a></p></td>
</tr>
</tbody>
</table>

## 以成都为背景的文学作品

  - 《[家](../Page/家_\(小说\).md "wikilink")》——[巴金](../Page/巴金.md "wikilink")
  - 《[春](../Page/春_\(小说\).md "wikilink")》——巴金
  - 《[秋](../Page/秋_\(小说\).md "wikilink")》——巴金
  - 《[死水微澜](../Page/死水微澜.md "wikilink")》——[李劼人](../Page/李劼人.md "wikilink")
  - 《[四川好人](../Page/四川好人.md "wikilink")》——[布莱希特](../Page/布莱希特.md "wikilink")（德）
  - 《[成都粉子](../Page/成都粉子.md "wikilink")》——[深爱金莲](../Page/深爱金莲.md "wikilink")
  - 《[成都，今夜请将我遗忘](../Page/成都，今夜请将我遗忘.md "wikilink")》——[慕容雪村](../Page/慕容雪村.md "wikilink")
  - 《[成都爱情只有八个月](../Page/成都爱情只有八个月.md "wikilink")》——[江树](../Page/江树.md "wikilink")

## 体育

  - **[中国足球超级联赛](../Page/中国足球超级联赛.md "wikilink")**：[四川冠城](../Page/四川冠城.md "wikilink")
  - **[中国足球甲级联赛](../Page/中国足球甲级联赛.md "wikilink")**：[成都五牛](../Page/成都五牛.md "wikilink")

## 之最拾零

### 世界之最

  - 世界第一只人工繁殖的大熊猫
  - 世界第一张纸币：[交子](../Page/交子.md "wikilink")
  - 世界现存历史最悠久的水利工程：[都江堰](../Page/都江堰.md "wikilink")
  - 世界最早开采天然气的地点：[邛崃](../Page/邛崃.md "wikilink")[火井镇](../Page/火井镇.md "wikilink")
  - 世界发现的最古老古代酿酒作坊：[水井坊](../Page/水井坊.md "wikilink")

### 中国之最

  - 中国第一家官办学堂：[石室中学](../Page/石室中学.md "wikilink")
  - 中国第一个商业广告
  - 中国第一张股票：蜀都股份
  - 中国第一个民间股份制商业银行：汇通城市合作银行
  - 中国第一家产权交易市场
  - 1949年后中国第一所私立学校：光亚小学

[\*](../Category/成都文化.md "wikilink")