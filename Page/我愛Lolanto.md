導演[王晶執導](../Page/王晶_\(導演\).md "wikilink")，並與[陳百祥自編自演的愛情喜劇](../Page/陳百祥.md "wikilink")。

## 劇情大綱

汽修廠老闆Lolanto偷用惡人郁德仁的名車泡妞，被識破後給毆傷送去醫院。在醫院巧遇富商的[弱智女兒小蚊](../Page/弱智.md "wikilink")。後來小蚊回家不久就離家出走，遇到Lolanto跟倫狄龍，就好心收留他住一起。可是小蚊之後進醫院做腦部手術，把Lolanto給忘記了。

## 劇中人物

  - [陳百祥](../Page/陳百祥.md "wikilink") （Lolanto）
  - [王晶](../Page/王晶_\(導演\).md "wikilink") （倫狄龍）
  - [夏文汐](../Page/夏文汐.md "wikilink") （小蚊）
  - [董驃](../Page/董驃.md "wikilink") （小蚊父廖鏢）
  - [艾蒂](../Page/艾蒂.md "wikilink") （小蚊後母）
  - [陳惠敏](../Page/陳惠敏.md "wikilink") （郁德仁）
  - [金燕玲](../Page/金燕玲.md "wikilink") (郁德仁太太)
  - [冼灝英](../Page/冼灝英.md "wikilink") （朱錦春）
  - [余慕蓮](../Page/余慕蓮.md "wikilink") (校長)
  - [杜少明](../Page/杜少明.md "wikilink") (嫖客)

## 電影歌曲

  - 主題曲〈今天再不可〉 主唱：[陳慧嫻](../Page/陳慧嫻.md "wikilink")

## 參考文獻

## 外部連結

  - {{@movies|fihk30087447}}

  -
  -
  -
  -
[Category:王晶電影](../Category/王晶電影.md "wikilink")
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[Category:香港浪漫喜劇片](../Category/香港浪漫喜劇片.md "wikilink")
[4](../Category/1980年代香港電影作品.md "wikilink")