《**我爱上流**》（），[2005年首映的](../Page/2005年电影.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/電影.md "wikilink")，由[迪恩·派瑞薩特執導](../Page/迪恩·派瑞薩特.md "wikilink")，[賈德·阿帕圖及](../Page/賈德·阿帕圖.md "wikilink")[尼古拉斯·斯圖勒編劇](../Page/尼古拉斯·斯圖勒.md "wikilink")，[占·基利與](../Page/占·基利.md "wikilink")[蒂雅·李歐妮主演](../Page/蒂雅·李歐妮.md "wikilink")。

## 情節

狄克、珍與他們的兒子──比利，一家三口子的生活過得非常美滿，有自己的屋、車、一隻可愛的狗與及即將完成的漂亮花園。可惜，他的老闆──傑克知道自己的集團將[倒閉而拋售手上的](../Page/倒閉.md "wikilink")[股票](../Page/股票.md "wikilink")，還讓狄克升任副總裁及上電視財經節目接受訪，其實是讓他當[替死鬼](../Page/替死鬼.md "wikilink")。狄克知道[升遷的事後](../Page/升遷.md "wikilink")，更讓妻子辭去[旅行社的工作](../Page/旅行社.md "wikilink")，專心在家照顧兒子。

可憐的狄克在電視接受訪問時才知道集團快將倒閉的事，最後集團倒閉，數千員工與狄克一樣變成[失業大軍](../Page/失業.md "wikilink")。幾個月後，狄克兩夫婦仍找不到工作，而他們的積蓄也快花光。他們為此變賣家當，但這樣也撐不到很久。終於，狄克收到[銀行寄來的警告信](../Page/銀行.md "wikilink")，若果他們再不交屋子的分期付款，他們便面臨被銀行收回物業，於是，兩夫婦唯有兵行險著……

## 演員表

### 主要角色

  - **[占·基利](../Page/占·基利.md "wikilink")**（[Jim
    Carrey](../Page/:en:Jim_Carrey.md "wikilink")） 飾演 **狄克·哈普**
  - **[蒂雅·李歐妮](../Page/蒂雅·李歐妮.md "wikilink")**（[Téa
    Leoni](../Page/:en:Téa_Leoni.md "wikilink")） 飾演 **珍·哈普**

### 其他角色

  - **[亞歷·鮑德溫](../Page/亞歷·鮑德溫.md "wikilink")**（[Alec
    Baldwin](../Page/:en:Alec_Baldwin.md "wikilink")） 飾演 **傑克·麥克卡里斯特**
  - **[里查·詹金斯](../Page/里查·詹金斯.md "wikilink")**（[Richard
    Jenkins](../Page/:en:Richard_Jenkins.md "wikilink")） 飾演 **法蘭克·巴斯康**
  - **[亞倫·米高·佐臣](../Page/亞倫·米高·佐臣.md "wikilink")**（[Aaron Michael
    Drozin](../Page/:en:Aaron_Michael_Drozin.md "wikilink")） 飾演
    **比利·哈普**

## 外部链接

  - [IMDb](http://imdb.com/title/tt0369441/)

[Category:2005年电影](../Category/2005年电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:西班牙語電影](../Category/西班牙語電影.md "wikilink")
[Category:美國喜劇犯罪片](../Category/美國喜劇犯罪片.md "wikilink")
[Category:哥倫比亞影業電影](../Category/哥倫比亞影業電影.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:賈德·阿帕托編劇作品](../Category/賈德·阿帕托編劇作品.md "wikilink")