《**我王莫臥兒**》（[烏爾都語](../Page/烏爾都語.md "wikilink"): مغلِ اعظم,
[印地語](../Page/印地語.md "wikilink"): मुग़ल-ए
आज़म）是1960年的[印度](../Page/印度電影.md "wikilink")[黑白電影](../Page/黑白電影.md "wikilink")，由著名演員[迪利普·庫馬主演莫臥兒王](../Page/迪利普·庫馬.md "wikilink")，[瑪杜芭拉演阿娜卡麗](../Page/瑪杜芭拉.md "wikilink")（Anarkali）。

本片佈景極其豪華奢侈，花九年時間才完成拍攝。推出當年即在印度獲得空前成功，打破歷來票房記錄。

2006年， 該片被《[印度時報](../Page/印度時報.md "wikilink")》評為「歷來頭二十五齣不可不看的影片」。

## 故事梗概

## 影片歌曲

  - Pyar Kiya to Darna Kya, "I have loved, so what is there to fear?"
    This song was one of three sequences shot on Eastman Kodak color
    film, while the rest of the movie was in black and white. The
    singing is, of course, playback singing by Lata Mangeshkar and
    lip-synched by Madhubala.

## 票房記錄

  - 本片創下印度歷來票房記錄並一直保持十五年，直到1975年，才被由[拉米什·西闢](../Page/拉米什·西闢.md "wikilink")（[Ramesh
    Sippy](../Page/Ramesh_Sippy.md "wikilink")）導演的《[Sholay](../Page/Sholay.md "wikilink")》打破。

## 演員陣容

  - [Dilip Kumar](../Page/Dilip_Kumar.md "wikilink")
  - [Madhubala](../Page/Madhubala.md "wikilink")
  - [Prithviraj Kapoor](../Page/Prithviraj_Kapoor.md "wikilink")
  - [Ajit](../Page/Ajit.md "wikilink")
  - [Durga Khote](../Page/Durga_Khote.md "wikilink")
  - [Nigar Sultana](../Page/Nigar_Sultana.md "wikilink")

## 外部連結

  -
  - [不朽的電影：《我王莫臥兒》](http://www.rediff.com/movies/2004/nov/12mughal.htm)

[M](../Category/1960年電影.md "wikilink")
[M](../Category/印度電影作品.md "wikilink")