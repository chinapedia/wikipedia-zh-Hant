[Vomkriege.jpg](https://zh.wikipedia.org/wiki/File:Vomkriege.jpg "fig:Vomkriege.jpg")

**战争论**（，）是[普鲁士](../Page/普鲁士.md "wikilink")[军事家](../Page/军事家.md "wikilink")[卡尔·冯·克劳塞维茨的一部軍事理論作品](../Page/卡尔·冯·克劳塞维茨.md "wikilink")。

克劳塞维茨是德军参谋部的最初奠基人[格哈德·冯·沙恩霍斯特将军最得力的助手之一](../Page/格哈德·冯·沙恩霍斯特.md "wikilink")，（另外一个是[奥古斯特·冯·格奈森瑙](../Page/奥古斯特·冯·格奈森瑙.md "wikilink")），参加过[反法同盟的战争](../Page/反法同盟.md "wikilink")，《战争论》是他总结自己所经历过的历次重大战役后所著的军事理论著作。

在书中克劳塞维茨提出了著名的“战争是政治的继续”观点，以及进攻是最好的防御等西方军事理论的基本思想。因此《战争论》被奉为西方军事理论的经典之作，克劳塞维茨本人也成为了西方军事理论的鼻祖。

## 全書架構

全书共8篇，100餘章，基本观点可以总结如下：

1.  战争是政治的继续，战争的母体是政治。
2.  战争的目的即是消灭敌人。因为政治斗争的目的便是消灭敌人，战争是这一目的最直接的表现。
3.  战略要素包括精神，物质，[数学](../Page/数学.md "wikilink")，[地理](../Page/地理.md "wikilink")，[统计五方面](../Page/统计.md "wikilink")。
4.  数量上的优势是战略，战术上最普遍的致胜因素，集中优势兵力是战略上最简单有效的准则。
5.  战争中攻防相互转换，进攻是最好的防御。
6.  战争理论生长在以往战史中，研究战史将得到最为有效的指导原则。

战争论是克劳塞维茨的未完成作品，在其生前仅整理好了其中六篇，第七、八篇的草稿由其遗孀整理，全书在他身故後由他的遺孀于1832年出版。

## 篇章

  - 第一篇論戰爭的性質
  - 第二篇論戰爭理論
  - 第三篇戰略概論
  - 第四篇戰鬥
  - 第五篇軍隊
  - 第六篇防禦
  - 第七篇進攻（草稿）
  - 第八篇戰爭計劃（草稿）

## 其他

  - 另外一名軍學大師[若米尼在其](../Page/安東莞·亨利·約米尼.md "wikilink")[战争艺术概论開頭時](../Page/战争艺术概论.md "wikilink")，也談及克劳塞维茨與戰爭論，他認為克劳塞维茨是博學的，但他的戰爭理論較為理想性。

## 外部链接

  - [克劳塞维茨网主页](http://www.clausewitz.com/index.htm)
  - [1832年德语原版《战争论》](http://www.clausewitz.com/readings/VomKriege1832/TOC.htm)
  - [1873年英语译本《战争论》](http://www.clausewitz.com/readings/OnWar1873/TOC.htm)

[Category:1832年書籍](../Category/1832年書籍.md "wikilink")
[Category:軍事書籍](../Category/軍事書籍.md "wikilink")
[Category:未完成書籍](../Category/未完成書籍.md "wikilink")
[Category:德語文學](../Category/德語文學.md "wikilink")
[Category:德国军事史](../Category/德国军事史.md "wikilink")