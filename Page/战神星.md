****（Bellona）是第28颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1854年3月1日发现。的[直径为](../Page/直径.md "wikilink")120.9千米，[质量为](../Page/质量.md "wikilink")1.9×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1691.362天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1854年发现的小行星](../Category/1854年发现的小行星.md "wikilink")