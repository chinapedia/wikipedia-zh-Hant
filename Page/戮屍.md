**戮屍**，泛指所有對[屍体進行的懲罰性破壞](../Page/屍体.md "wikilink")。

## 中國

戮屍於古代中國曾列入[刑法](../Page/刑法.md "wikilink")。通常的做法是砍去死屍的[頭](../Page/頭.md "wikilink")。

### 刑典中的戮屍

一般而言，犯罪的人如在事件揭發之前已死，其罪行多不會被追究，俗語謂「已死勿論」
(見於《[水滸傳](../Page/水滸傳.md "wikilink")》\[1\]、《[東周列國志](../Page/東周列國志.md "wikilink")》\[2\])。然而，如果罪行較為嚴重
(如謀反)
，即使在事件揭發時犯人已去世，當局亦可根據當時的法例，在犯人的屍體上施刑，是為戮屍。另外，對於被判[死刑但在行刑前已身故的犯人](../Page/死刑.md "wikilink")，亦可根據其罪行嚴重程度決定是否戮屍。

《[清史稿](../Page/清史稿.md "wikilink")》卷一百一十八〈刑法志〉中，指戮屍為*「所以待惡逆及强盜應[梟諸犯之監故者](../Page/枭首.md "wikilink")。」*，即所有應判梟首但在待刑時去世者均處戮屍之刑。

[明朝自](../Page/明朝.md "wikilink")[萬曆十六年](../Page/萬曆.md "wikilink") (1588年)
起，定有戮屍的條例，主要針對謀殺父母或祖父母者。[清朝沿用有關刑法](../Page/清朝.md "wikilink")，並 (明文)
擴大至於強盜身上亦有效。

戮屍在1905年的刑法改革中被廢除。

### 執行紀錄

中國歷代均有人由於在死後被定罪而被戮屍。

  - 根據《[史記](../Page/史記.md "wikilink")》卷六〈秦始皇本紀〉，[秦始皇之弟](../Page/秦始皇.md "wikilink")[成嶠反叛失敗後死於](../Page/成蟜.md "wikilink")[屯留](../Page/屯留.md "wikilink")，其從屬軍吏*「皆斬死」*。另外*「卒屯留、蒲鶮反，戮其屍」*，已死的軍吏受戮屍之刑。
  - 《[遼史](../Page/遼史.md "wikilink")》卷一百二〈奸臣傳上〉所記載的其中三人 (耶律乙辛、張孝杰、蕭十三)
    死後均因生前罪行揭發而被戮屍。
  - 明朝太监魏忠贤死后，被下令将尸体凌迟枭首。\[3\]
  - [清朝](../Page/清朝.md "wikilink")[文字獄期間亦有進行戮屍](../Page/文字獄.md "wikilink")。[吕留良即於](../Page/吕留良.md "wikilink")[雍正年間一次文字獄中與其長子](../Page/雍正.md "wikilink")[吕葆中](../Page/吕葆中.md "wikilink")、弟子[嚴鴻逵同遭戮屍](../Page/嚴鴻逵.md "wikilink")。\[4\]

## 西方

在古代，信奉[基督教國家的人曾認為](../Page/基督教.md "wikilink")：死後屍體要保持完整並朝東方埋葬，才能在[最後的審判中起來面見](../Page/最後的審判.md "wikilink")[神](../Page/神.md "wikilink")。

[英國在](../Page/英國.md "wikilink")[亨利八世統治時期的一項](../Page/亨利八世.md "wikilink")[法令曾指示只有](../Page/法令.md "wikilink")[謀殺犯的屍體可被用作](../Page/謀殺.md "wikilink")[解剖](../Page/解剖.md "wikilink")，而這被看作是對已被[處死的犯人的附加懲罰](../Page/死刑.md "wikilink")。

### 執行紀錄

  - [教宗](../Page/教宗.md "wikilink")[福爾摩塞](../Page/教宗福慕.md "wikilink")
    (896年去世)
    的屍體被後來的教宗[司提反七世掘了出來並帶往](../Page/司提反七世.md "wikilink")「審訊」。屍體的三隻手指在定罪後被斬去，而屍體最終被扔到[台伯河中](../Page/台伯河.md "wikilink")。
  - [約翰·威克里夫在死後被定為](../Page/威克里夫.md "wikilink")[異端](../Page/異端.md "wikilink")，屍體被掘出和燒毀。
  - [弗拉德三世死後被](../Page/弗拉德三世.md "wikilink")[奧斯曼軍隊](../Page/奧斯曼帝國.md "wikilink")[斬首示眾](../Page/斬首.md "wikilink")。
  - [克倫威爾死後](../Page/奥利弗·克伦威尔.md "wikilink")，繼任的英王[查理二世把其屍體掘出並進行](../Page/查理二世.md "wikilink")[車裂](../Page/挂拉分.md "wikilink")。

## 參見

  - [鞭尸](../Page/鞭尸.md "wikilink")

## 參考資料

<references/>

[Category:中国古代刑罚](../Category/中国古代刑罚.md "wikilink")
[Category:死刑](../Category/死刑.md "wikilink")
[Category:喪葬](../Category/喪葬.md "wikilink")

1.  [水滸傳第二十六回](../Page/水滸傳.md "wikilink")：*「姦夫淫婦雖該重罪，已死勿論」*
2.  [東周列國志第十九回](../Page/東周列國志.md "wikilink")：*「[祭足已死勿論](../Page/祭仲.md "wikilink")」*
3.  《明史》卷三百五：（天启七年）十一月，遂安置忠贤于凤阳，寻命逮治。忠贤行至阜城，闻之，与李朝钦偕缢死。诏磔其尸。悬首河间。
4.  [清史稿本紀九](../Page/清史稿.md "wikilink")：*「乙丑，治呂留良罪，與呂葆中、嚴鴻逵俱戮屍」*