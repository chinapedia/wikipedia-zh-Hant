**《战国策》**是[中国古代的](../Page/中国.md "wikilink")[史学](../Page/史学.md "wikilink")[名著](../Page/名著.md "wikilink")。由[漢朝](../Page/漢朝.md "wikilink")[劉向編訂](../Page/劉向.md "wikilink")，但其原作者不詳。劉向因此書所记的多是[東周後期时諸國混戰](../Page/東周.md "wikilink")，纵横家为其君王擬定的政治主张和外交策略，因此定名为《战国策》，而該時期亦因此被史家稱為[戰國時代](../Page/戰國_\(中國\).md "wikilink")。

《战国策》為[國別體](../Page/國別體.md "wikilink")，全书按[东周國](../Page/东周國.md "wikilink")、[西周國](../Page/西周國.md "wikilink")、[秦国](../Page/秦国.md "wikilink")、[齐国](../Page/田齐.md "wikilink")、[楚国](../Page/楚国.md "wikilink")、[赵国](../Page/赵国.md "wikilink")、[魏国](../Page/魏国_\(战国\).md "wikilink")、[韓国](../Page/韩国_\(诸侯\).md "wikilink")、[燕国](../Page/燕国.md "wikilink")、[宋国](../Page/宋国.md "wikilink")、[衞國](../Page/衞國.md "wikilink")、[中山国依次分国编写](../Page/中山国.md "wikilink")，共三十三[卷](../Page/卷.md "wikilink")，约十二[万](../Page/万.md "wikilink")[字](../Page/字.md "wikilink")。主要[记述了](../Page/记述.md "wikilink")[战国时期的說客們言行](../Page/战国.md "wikilink")，更包括了不少[纵横家的](../Page/纵横家.md "wikilink")[政治策略與](../Page/政治.md "wikilink")[辯論技巧](../Page/辯論.md "wikilink")，也可說是[合纵與](../Page/合纵.md "wikilink")[連橫的实战演习手冊](../Page/連橫_\(外交策略\).md "wikilink")。本書展示了战国时代的[历史特点和](../Page/历史.md "wikilink")[社会风貌](../Page/社会.md "wikilink")，是研究战国历史的重要[典籍](../Page/典籍.md "wikilink")。在[四庫全書之中為史部](../Page/四庫全書.md "wikilink")。

## 編者

《战国策》只知編者是漢代的[劉向](../Page/劉向.md "wikilink")，原作者直到现在也没有确定，大致上認為是非一時一地一人的作品。學者[羅根澤認為](../Page/羅根澤.md "wikilink")，可能是[蒯徹所作](../Page/蒯徹.md "wikilink")，羅根澤說：“《战国策》始作于蒯通（蒯徹）；增补并重编者为刘向；[司马贞所见是否即刘向重编本不可知](../Page/司马贞.md "wikilink")，今本则有残阙矣。”\[1\]

本書隨著戰亂，也散逸不全，到了[宋朝](../Page/宋朝.md "wikilink")，[曾鞏又蒐羅闕文](../Page/曾鞏.md "wikilink")，加以重修。

## 题解

《战国策》的文章原有《国策》、《国事》、《短长》、《事语》、《长书》、《修书》等名称。[西汉末年](../Page/西汉.md "wikilink")，[刘向校录群书时在皇家藏书中发现了六种记录纵横家的写本](../Page/刘向.md "wikilink")，但是内容混乱，文字残缺。因此，战国策显然不是一时一人所作。刘向依照[國別體分類](../Page/國別體.md "wikilink")，並加以編修。\[2\]刘向只是战国策的校订者和编订者。而後[高誘為之作註解](../Page/高誘.md "wikilink")。

[北宋时](../Page/北宋.md "wikilink")，《战国策》散佚颇多，经[曾巩](../Page/曾巩.md "wikilink")“訪之士大夫家，始盡得其書”，並加以校补，是为今本《战国策》三十三篇。\[3\]，[姚宏](../Page/姚宏.md "wikilink")、[鲍彪](../Page/鲍彪.md "wikilink")、[吴师道皆有注本](../Page/吴师道.md "wikilink")。

1973年，在[长沙](../Page/长沙.md "wikilink")[马王堆三号汉墓出土了一批](../Page/馬王堆.md "wikilink")[帛书](../Page/馬王堆帛書.md "wikilink")，其中一部类似于今本《战国策》，整理后定名为《[戰國縱橫家書](../Page/:s:戰國縱橫家書.md "wikilink")》。该书共27篇，其中11篇内容和文字与今本《战国策》和《[史记](../Page/史记.md "wikilink")》大致上相同。

## 体例

今本共33卷。东周策1卷，西周策1卷，秦策5卷，齐策6卷，楚策4卷，赵策4卷，魏策4卷，韩策3卷，燕策3卷、宋卫策1卷，中山策1卷，共497篇。

所记载的历史，上起前455年[晉陽之戰](../Page/晉陽之戰.md "wikilink")（稍提到前490年[智伯灭范氏](../Page/智伯.md "wikilink")），下至前221年[高渐离以](../Page/高渐离.md "wikilink")[筑击](../Page/筑.md "wikilink")[秦始皇](../Page/秦始皇.md "wikilink")。

## 名篇

  - [苏秦以连横说秦](../Page/wikisource:zh:蘇秦以連橫說秦.md "wikilink")（[成语](../Page/成语.md "wikilink")**[前倨后恭](../Page/前倨后恭.md "wikilink")**的出处）
  - [馮諼客孟嘗君](../Page/wikisource:zh:馮諼客孟嘗君.md "wikilink")（成語**[狡兔三窟](../Page/狡兔三窟.md "wikilink")**、**[高枕無憂](../Page/高枕無憂.md "wikilink")**的來源）
  - [趙且伐燕](../Page/趙且伐燕.md "wikilink")（成語**[鷸蚌相爭](../Page/鷸蚌相爭.md "wikilink")**、**[漁翁得利](../Page/漁翁得利.md "wikilink")**的來源
    )
  - [邹忌修八尺有余](../Page/邹忌修八尺有余.md "wikilink")（又称[邹忌讽齐王纳谏](../Page/邹忌讽齐王纳谏.md "wikilink"))
  - [秦王使人谓安陵君曰](../Page/秦王使人谓安陵君曰.md "wikilink")（又称[唐雎不辱使命](../Page/唐雎不辱使命.md "wikilink"))
  - [燕太子丹质于秦](../Page/燕太子丹质于秦.md "wikilink")（又称[荆轲刺秦王](../Page/荆轲刺秦王.md "wikilink"))

## 文學風格

《戰國策》善於鋪敘，長於說事，選取曲折生動的故事情節，緊湊生動，富戲劇性，喜誇張渲染，善用排偶，捭闔譎誑，辯麗恣肆，又善用比喻與寓言，諷刺幽默，說服力強。

《戰國策》亦善於描寫人物，抒發情感，部分篇章深沉蘊藉，委婉動人。

## 评价

《战国策》一书反映了[战国时代的社会风貌](../Page/战国.md "wikilink")，当时士人的精神风采，不仅是一部历史著作，也是一部非常好的历史散文。它作为一部反映战国历史的历史资料，比较客观地记录了当时的一些重大历史事件，是战国历史的生动写照。它详细地记录了当时纵横家的言论和事迹，展示了这些人的精神风貌和思想才干，另外也记录了一些义勇志士的人生风采。《战国策》的[文学成就也非常突出](../Page/文学.md "wikilink")，在[中国文学史上](../Page/中国文学史.md "wikilink")，它标志着中国古代[散文发展的一个新时期](../Page/散文.md "wikilink")，文学性非常突出，尤其在人物形象地刻画，语言文字的运用，寓言故事等方面具有非常鲜明的艺术特色。清初学者[陸隴其稱](../Page/陸隴其.md "wikilink")《战国策》“其文章之奇足以娱人耳目，而其机变之巧足以坏人之心术”\[4\]。

《战国策》的思想内容比较复杂，主体上体现了[纵横家的思想倾向](../Page/纵横家.md "wikilink")，同时也反映出了[战国时期思想活跃](../Page/战国.md "wikilink")，文化多元的历史特点。《战国策》的政治观比较进步，最突出的是体现了重视人才的政治思想。《战国策》一书是游说辞总集，幾乎所有纵横家谋士的言行都在此书。有三大特点：一智谋细，二虚实间，三文辞妙。

《战国策》一书对[司马迁的](../Page/司马迁.md "wikilink")《[史记](../Page/史记.md "wikilink")》的[纪传体的形成](../Page/纪传体.md "wikilink")，具有很大影响。《史記》有九十-{卷}-的史料直接取自於《戰國策》的史料。但《戰國策》許多記載並不可靠，司馬遷在《蘇秦傳》後曰“世言[蘇秦多異](../Page/蘇秦.md "wikilink")，異時事有類之者皆附之蘇秦”。《战国策》历来为研究者称赞其文学价值，但是对它的思想却是众说纷纭。这是由于该书与后世的[儒家思想不符](../Page/儒家思想.md "wikilink")，过于追逐名利。而且过于夸大纵横家的历史作用，降低了史学价值。

## 注釋

## 参考文獻

  - 《战国策》评介，钱国旗，史著英华（中国典籍精华丛书），魏良弢主编，中国青年出版社，2000，第1册，p157-239. ISBN
    750063764
  - 熊宪光，《战国策研究与选译》，重庆出版社，1988年. ISBN 9787536600201
  - 孟庆祥，《战国策译注》，黑龙江人民出版社，1986年 统一书号：10093·701
  - 王守谦，《战国策全译》，贵州人民出版社，1992年. ISBN 9787221041326
  - 朱友华，《战国策选译》，上海古籍出版社，1987年
  - 赵丕杰，《战国策选译》，人民文学出版社，1994年
  - 蓝开祥，《战国策名篇赏析》，北京十月文艺出版社，1991年
  - 何建章，《战国策注释》，中华书局，1990年. ISBN 9787101006223
  - 何晋，《战国策研究》，北京大学出版社，2001年. 书号 7-301-05101-8
  - 楊子彥，《戰國策正宗》，北京華夏出版社，2008年. ISBN 978-7-5080-4206-0

## 外部链接

[Category:史部雜史類](../Category/史部雜史類.md "wikilink")
[Category:国别体](../Category/国别体.md "wikilink")

1.  [羅根澤](../Page/羅根澤.md "wikilink")《战国策作于蒯通考》
2.  《战国策·叙录》中说：“所校中《战国策书》，中书余卷，错乱相糅莒。又有国别者八篇，少不足。臣向因国别者略以时次之，分别不以序者以相补，除复重，得三十三篇。……中书本号，或曰《国策》，或曰《国事》，或曰《短长》，或曰《事语》，或曰《长书》，或曰《
    书》。臣向以为战国时游士，辅所用之国，为之策谋，宜为《战国策》。”
3.  曾巩：《战国策目录序》
4.  陸隴其：《戰國策去毒·自記》