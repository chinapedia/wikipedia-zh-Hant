是一个[科幻](../Page/科幻.md "wikilink")[第一人称射击](../Page/第一人称射击游戏.md "wikilink")[遊戲](../Page/電子遊戲.md "wikilink")，由[Valve开发](../Page/維爾福.md "wikilink")，[维旺迪环球发行](../Page/维旺迪环球.md "wikilink")。游戏的-{引擎}-基于[雷神之锤的](../Page/雷神之锤.md "wikilink")[游戏引擎](../Page/游戏引擎.md "wikilink")\[1\]
的大幅改进版本。游戏最先发布在[微软Windows平台上](../Page/Microsoft_Windows.md "wikilink")，后来也发布了[PS2版本](../Page/PlayStation_2.md "wikilink")。据说[Dreamcast版本和](../Page/Dreamcast.md "wikilink")[Macintosh版本已经开发完毕](../Page/Macintosh.md "wikilink")，但是没有正式发布。《半条命》的一些遊戲模組也十分流行，例如[反恐精英](../Page/反恐精英.md "wikilink")（Counter-Strike，通常简称CS）。

## 半条命系列

  - [半条命](../Page/半条命_\(游戏\).md "wikilink")（1998年11月9日）
      - [半条命：Uplink](../Page/半条命#.E9.81.8A.E6.88.B2.E9.96.8B.E7.99.BC.md "wikilink")（1999年2月12日）（半条命的展示版）
      - [半条命：针锋相对](../Page/半条命：针锋相对.md "wikilink")（1999年11月1日）
      - [半条命：蓝色偏移](../Page/半条命：蓝色偏移.md "wikilink")（2001年6月1日）
      - [半条命：衰变](../Page/半条命：衰变.md "wikilink")（2001年11月14日）（只在PS2平台上發布）

<!-- end list -->

  - [戰慄時空2](../Page/戰慄時空2.md "wikilink")（2004年11月16日）
      - [戰慄時空2：消失的海岸線](../Page/戰慄時空2：消失的海岸線.md "wikilink")（2005年10月27日）
  - [戰慄時空2首部曲：浩劫重生](../Page/戰慄時空2首部曲：浩劫重生.md "wikilink")（2006年6月1日）
  - [戰慄時空2：二部曲](../Page/戰慄時空2：二部曲.md "wikilink")（2007年10月10日）
  - [戰慄時空2：三部曲](../Page/戰慄時空2：三部曲.md "wikilink")（尚未發佈）

## 相关作品

### 传送门系列

  - [传送门](../Page/传送门.md "wikilink")（2007年10月10日）
  - [传送门2](../Page/传送门2.md "wikilink")（2011年4月19日）

## 名稱

《半条命》的英文原文“”的原意是物質的[半衰期](../Page/半衰期.md "wikilink")，即[放射性物質衰變到本身一半的時間](../Page/放射性.md "wikilink")。小写的希臘字母[λ被作為遊戲的標誌](../Page/λ.md "wikilink")，代表[半衰期方程中的](../Page/半衰期#方程.md "wikilink")[衰减常数](../Page/衰减常数.md "wikilink")。\[2\]

《半条命》和它第一代的資料片都以科學詞彙命名：《[戰慄時空：對立力量](../Page/戰慄時空：對立力量.md "wikilink")》（）名字來自於[牛頓運動定律](../Page/牛頓運動定律.md "wikilink")；《[戰慄時空：藍色偏移](../Page/戰慄時空：藍色偏移.md "wikilink")》（）則以[電磁波](../Page/電磁波.md "wikilink")[蓝移為名](../Page/蓝移.md "wikilink")；而《[戰慄時空：衰变](../Page/戰慄時空：衰变.md "wikilink")》（）的名称来源于[放射性衰变](../Page/放射性#衰变.md "wikilink")。

「**半条-{}-命**」是普遍通行於中國大陸的稱法，此译名如今也为代理商所接受。在[台灣和](../Page/台灣.md "wikilink")[香港稱此遊戲為](../Page/香港.md "wikilink")「**戰慄-{}-時空**」
。著名的遊戲模組[Counter-Strike在大陸及港台則分别被稱為](../Page/反恐精英.md "wikilink")「反恐-{}-精英」和「絕對-{}-武力」。

## 參見

[Half-life](../Category/戰慄時空系列.md "wikilink")
[Half-life](../Category/第一人称射击游戏.md "wikilink")
[Half-life](../Category/Windows游戏.md "wikilink")
[Half-life](../Category/PlayStation_2游戏.md "wikilink")

1.  [雷神之錘引擎](../Page/雷神之錘引擎.md "wikilink")
2.