《**戰略迷魂**》（）是一部[2004年的](../Page/2004年電影.md "wikilink")[美國](../Page/美國電影.md "wikilink")[驚悚電影](../Page/驚悚電影.md "wikilink")，由[尊尼芬·迪米執導](../Page/尊尼芬·迪米.md "wikilink")，[丹泽尔·华盛顿](../Page/丹泽尔·华盛顿.md "wikilink")、[梅麗·史翠普及](../Page/梅麗·史翠普.md "wikilink")[李佛·薛伯主演](../Page/李佛·薛伯.md "wikilink")，劇情改編自1959年（Richard
Condon）的一本[同名小說](../Page/滿洲候選人.md "wikilink")。

## 劇情

[波灣戰爭時馬可少校率領的小隊在](../Page/波灣戰爭.md "wikilink")[科威特沙漠中遭](../Page/科威特.md "wikilink")[伊拉克軍隊伏擊](../Page/伊拉克.md "wikilink")，槍戰中馬可遭擊昏，幸虧小隊成員雷蒙蕭[中士單人殲滅了大半敵軍](../Page/中士.md "wikilink")，帶領剩下的弟兄殺出重圍；雷蒙蕭英勇的行為使他獲頒國會[榮譽勳章](../Page/榮譽勳章.md "wikilink")，馬可則繼續軍人生涯但是卻承受著多年失眠與詭怪的惡夢。

多年後本來就有政治世家背景的雷蒙蕭以戰爭英雄功勳活躍於政壇無往不利，並且成為熱門[美國副總統候選人](../Page/美國副總統.md "wikilink")；馬可某一天和昔日同袍的聚會得知他不是唯一受惡夢困擾的人；深入探訪之後，他赫然發現一切回憶都是人為操弄的驚天騙局，和官方最高機密的催眠計畫有關。

## 演員

  - [丹泽尔·华盛顿](../Page/丹泽尔·华盛顿.md "wikilink") ... Major Bennett Ezekiel
    Marco
  - [梅麗·史翠普](../Page/梅麗·史翠普.md "wikilink") ... Senator Eleanor Prentiss
    Shaw (D-[VA](../Page/VA.md "wikilink"))
  - [李佛·薛伯](../Page/李佛·薛伯.md "wikilink") ... Congressman Raymond
    Prentiss Shaw (D-[NY](../Page/NY.md "wikilink"))
  - [Kimberly Elise](../Page/Kimberly_Elise.md "wikilink") ... FBI Ag.
    Eugenie Rose
  - [強·沃特](../Page/強·沃特.md "wikilink") ... Senator Thomas Jordan
    (D-[CT](../Page/CT.md "wikilink"))
  - [薇菈·法蜜嘉](../Page/韋娜·法米加.md "wikilink") ... Jocelyne Jordan
  - [Jeffrey Wright](../Page/Jeffrey_Wright_\(actor\).md "wikilink") ...
    Al Melvin
  - [Simon McBurney](../Page/Simon_McBurney.md "wikilink") ... Dr.
    Atticus Noyle
  - [布鲁诺·冈茨](../Page/布鲁诺·冈茨.md "wikilink") ... Delp
  - [David Keeley](../Page/David_Keeley.md "wikilink") ... Agent Evan
    Anderson
  - [Ted Levine](../Page/Ted_Levine.md "wikilink") ... Colonel Howard
  - [Miguel Ferrer](../Page/Miguel_Ferrer.md "wikilink") ... Colonel
    Garret
  - [Dean Stockwell](../Page/Dean_Stockwell.md "wikilink") ... Mark
    Whiting
  - [Jude Ciccolella](../Page/Jude_Ciccolella.md "wikilink") ... David
    Donovan
  - [Tom Stechschulte](../Page/Tom_Stechschulte.md "wikilink") ...
    Governor Robert Arthur
  - [Pablo Schreiber](../Page/Pablo_Schreiber.md "wikilink") ... PFC
    Eddie Ingram
  - [Anthony Mackie](../Page/Anthony_Mackie.md "wikilink") ... PFC
    Robert Baker III
  - [Robyn Hitchcock](../Page/Robyn_Hitchcock.md "wikilink") ... Laurent
    Tokar
  - [Obba Babatunde](../Page/Obba_Babatunde.md "wikilink") ... Senator
    Wells
  - [Zeljko Ivanek](../Page/Zeljko_Ivanek.md "wikilink") ... Vaughn Utly

## 1962年版本

## 書籍

| 出版日期  | 書名                         | 作者             | ISBN               |
| ----- | -------------------------- | -------------- | ------------------ |
| 2004年 | 《The Manchurian Candidate》 | Richard Condon | ISBN 9780965931540 |

## 外部連結

  -
  -
  -
  -
  -
  -
  - {{@movies|fmen40368008}}

  -
  -
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:英语電影](../Category/英语電影.md "wikilink")
[Category:纽约市背景電影](../Category/纽约市背景電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:重拍電影](../Category/重拍電影.md "wikilink")
[Category:驚悚小說改編電影](../Category/驚悚小說改編電影.md "wikilink")
[Category:亂倫題材電影](../Category/亂倫題材電影.md "wikilink")