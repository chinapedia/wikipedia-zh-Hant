**戲神**為戲團中所奉祀之[神祇](../Page/神祇.md "wikilink")，[中國傳統](../Page/中國.md "wikilink")[戲曲戲團的信仰核心](../Page/戲曲.md "wikilink")，維繫舊時戲團規矩、運作，傳承戲團特有的倫理、精神，為多數藝人的心靈寄託。戲神奉祀，與戲團與[行會組織發展緊密結合](../Page/行會.md "wikilink")，崇奉行會共同始祖，藉以建立組織共同規範。各類戲曲依其劇種、師承、地區，奉祀戲神各有不同。以[田都元帥](../Page/田都元帥.md "wikilink")、[西秦王爺為其代表](../Page/西秦王爺.md "wikilink")。

在廣東，[粵劇各戲班本來各有其戲神](../Page/粵劇.md "wikilink")，有[田竇二師等](../Page/田竇二師.md "wikilink")，而後統一奉祀[華光大帝](../Page/華光大帝.md "wikilink")，將西秦王爺與田都元帥視為華光天王的輔佐神。這是因為一個廣東的傳說，戲班演戲時不避[禁忌](../Page/禁忌.md "wikilink")，冒犯上天，上天欲加以[天譴](../Page/天譴.md "wikilink")，命[火神](../Page/火神.md "wikilink")[華光大帝放火燒掉所有的戲臺](../Page/華光大帝.md "wikilink")，華光不忍心，於是違背[天命](../Page/天命.md "wikilink")，還託夢教導各個戲班如何悔過，並向上天祈禱，赦免演戲時不敬的罪過，終於保全了所有戲班，於是眾人皆改奉華光，以原本的戲神作為其佐神。

## 奉祀

戲班中平日奉祀於班主家中，出班時則奉祀於[後臺](../Page/後臺.md "wikilink")，藝人上下場均須，向戲神禮敬。亦有行會建廟奉為[主神](../Page/主神.md "wikilink")，或陪祀於地方[公廟中者](../Page/公廟.md "wikilink")。

## 信仰源流

  - 傳統戲曲所供奉祖師爺，原為「翼宿星君」，因在中國[星宿信仰中](../Page/星宿信仰.md "wikilink")，[翼宿主掌音樂](../Page/翼宿.md "wikilink")、戲劇。

<!-- end list -->

  - [唐明皇創設](../Page/唐明皇.md "wikilink")[梨園](../Page/梨園.md "wikilink")，故後世戲曲界多奉為戲神。

<!-- end list -->

  - [後唐莊宗完成了父親遺志](../Page/後唐莊宗.md "wikilink")，打敗了[劉仁恭](../Page/劉仁恭.md "wikilink")、[朱溫等](../Page/朱溫.md "wikilink")，而後耽溺戲曲，曾自扮優伶上場，沈迷戲曲，終至身死，戲曲界極為感念，故祭拜之。

<!-- end list -->

  - 北方崇祀[喜神](../Page/喜神.md "wikilink")，是年幼的演員之神、道具之神，「喜神神偶」本身也是一種道具，該神偶只能扮演[公子](../Page/公子.md "wikilink")[王孫](../Page/王孫.md "wikilink")，如須扮演[乞食](../Page/乞食.md "wikilink")、[孤兒](../Page/孤兒.md "wikilink")，下台後須設牲酒酬神。一說[喜神即](../Page/喜神.md "wikilink")[後唐莊宗](../Page/後唐莊宗.md "wikilink")，因為後唐莊宗曾扮演兒童。另一說，喜神即「翼宿星君」化身。

## 各劇種戲神

  - 以[田都元帥為主神](../Page/田都元帥.md "wikilink")：[高甲戲](../Page/高甲戲.md "wikilink")、[四平戲](../Page/四平戲.md "wikilink")、[歌仔戲](../Page/歌仔戲.md "wikilink")、[亂彈戲](../Page/亂彈戲.md "wikilink")、[北管](../Page/北管.md "wikilink")[西皮派](../Page/西皮派.md "wikilink")、[南管戲](../Page/梨園戲.md "wikilink")、[福州戲](../Page/閩劇.md "wikilink")、[採茶戲](../Page/採茶戲.md "wikilink")、[皮影戲](../Page/皮影戲.md "wikilink")、[掌中戲](../Page/掌中戲.md "wikilink")、[傀儡戲](../Page/傀儡戲.md "wikilink")、[潮州戲](../Page/潮州戲.md "wikilink")
  - 以[西秦王爺為主神](../Page/西秦王爺.md "wikilink")：北管[福祿派](../Page/福祿派.md "wikilink")、[亂彈](../Page/亂彈.md "wikilink")、[西秦戲](../Page/西秦戲.md "wikilink")、布袋戲、傀儡戲
  - 以[孟府郎君為主神](../Page/孟昶.md "wikilink")：南管
  - 以[華光大帝為主神](../Page/華光大帝.md "wikilink")：[粵劇](../Page/粵劇.md "wikilink")
  - 以[老郎神為主神](../Page/老郎神.md "wikilink")：[京戲](../Page/京戲.md "wikilink")
  - 以[鄭元和為主神](../Page/鄭元和.md "wikilink")：[車鼓](../Page/車鼓弄.md "wikilink")[小戲](../Page/小戲.md "wikilink")

[Category:戏曲神](../Category/戏曲神.md "wikilink")
[Category:戏曲](../Category/戏曲.md "wikilink")