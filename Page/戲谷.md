**和信超媒體股份有限公司戲-{谷}-分公司**（**FunTown**），簡稱**戲-{谷}-**，是[台灣一家](../Page/台灣.md "wikilink")[網路遊戲](../Page/網路遊戲.md "wikilink")[公司](../Page/公司.md "wikilink")，於1998年1月1日\[1\]成立，前稱**宏碁戲谷**（**Acer
Funtown**/**AcerGameZONE**）。原為[宏碁集團旗下的](../Page/宏碁集團.md "wikilink")[遊戲軟體服務提供者](../Page/遊戲軟體.md "wikilink")[第三波資訊營運](../Page/第三波資訊.md "wikilink")。其營運權於2006年1月1日轉讓予[和信超媒體](../Page/和信超媒體.md "wikilink")（[GigaMedia
Ltd.](../Page/:en:GigaMedia.md "wikilink")）繼續經營，全名「和信超媒體股份有限公司戲谷分公司」，並於2008年成立[戰谷品牌](../Page/戰谷.md "wikilink")\[2\]推出角色扮演類型線上遊戲。至今在台灣擁有600萬名會員。

戲谷擅長開發[娛樂及](../Page/娛樂.md "wikilink")[麻將耍樂相關遊戲](../Page/麻將.md "wikilink")，成功開發[網路廣告桌布](../Page/網路廣告.md "wikilink")、[網路](../Page/網路.md "wikilink")[橋牌轉播機](../Page/橋牌.md "wikilink")、全球第一個[WAP多人遊戲](../Page/WAP.md "wikilink")、首座網路遊樂園、台灣第一個免費網路[賽車遊戲及台灣第一個](../Page/賽車遊戲.md "wikilink")3D網路撞球等創舉。並於[香港及](../Page/香港.md "wikilink")[中國大陸設有分公司](../Page/中國大陸.md "wikilink")。

中國大陸戲谷娛樂館已經停止營運。

## 旗下遊戲

### 台灣

#### 運營中

  - [戲谷娛樂館](../Page/戲谷娛樂館.md "wikilink")
      - 戲谷麻將館（戲谷麻將、自摸館、暗棋館、Web版新戲谷麻將、iPhone麻將、iPad麻將）
      - 戲谷撲克館（大老二、Web大老二、怪物十三支、梭哈、真接龍、德州撲克、Vegas德州撲克、撿紅)
      - 戲谷金銀島（水果盤、決戰百家樂、至尊21點、3D拉霸、夢幻滿貫、骰寶、金多蝦、跑馬風雲、拉霸、奇幻接龍、大海賊、賓果、四聖獸、大海賊II、柏青哥、麻將九宮格、7PK、孔雀、百樂町(剪刀石頭布、紅狗、牌九、紅利撲克50手、樂透城堡)、常夏娘、Vegas骰寶、野蠻海盜、嗨爆水果盤、全民大預測）
      - 戲谷OSuKe遊戲町（魔法方塊、逗陣坦克、野蠻碰碰王、歡樂連連）
      - 戲谷小鎮

#### 戰谷營運

  - [戰地之王](../Page/戰地之王.md "wikilink") - 結束代理權、代理權轉交給Garena。

#### 已結束營運

  - [瘋狂坦克](../Page/瘋狂坦克.md "wikilink")
  - 新衝天跑(TALES RUNNER)

.新衝天跑合併至跑Online(HK)

2015/4月合約到期停止營運

  - [幸福Online](../Page/幸福Online.md "wikilink")

<!-- end list -->

  -
    戲谷於2004年代理幸福Online，在使用者規約中預定代理至少三年。代理之初，曾遭董事長大力的反對，但高層主管群認為遊戲的賣相好，終於在日本代表團即將搭上飛機之前簽下合約。一年後（即2005年），戲谷便火速結束此遊戲在兩岸三地的營運，並聲言此後便不代理任何RPG(角色扮演)類型的網路遊戲\[3\]。

<!-- end list -->

  - [寵物王Online](../Page/寵物王.md "wikilink")

<!-- end list -->

  -
    為戲谷前任的營運者──第三波所自行開發的線上遊戲，曾創下了萬人上線的紀錄，但自從第三波成功於2004年代理幸福Online後，第三波旋即結束開發及營運寵物王。即使網絡上有很多人希望戲谷重新營運寵物王，戲谷亦無意重新營運寵物王遊戲的業務。

<!-- end list -->

  - [龍族](../Page/龍族.md "wikilink")

<!-- end list -->

  -
    世界上推出不少於10個地區版本[龍族](../Page/龍族.md "wikilink")；戲谷代理中、港、台版本[龍族](../Page/龍族.md "wikilink")，自從戲谷陸續正式結束[龍族營運](../Page/龍族.md "wikilink")。網絡上就出現各式各樣龍族私服，證明[龍族魅力歷久不衰](../Page/龍族.md "wikilink")。

<!-- end list -->

  - [四國戰棋](../Page/四國戰棋.md "wikilink")
  - [PP 熊](../Page/PP_熊.md "wikilink")
  - [太閤立志傳系列](../Page/太閤立志傳系列.md "wikilink")
  - [撞球館](../Page/撞球館.md "wikilink")
  - [勇者泡泡龍2 Online](../Page/勇者泡泡龍2_Online.md "wikilink")
  - [10點半](../Page/10點半.md "wikilink")
  - [賽馬機](../Page/賽馬機.md "wikilink")
  - [小瑪莉](../Page/小瑪莉.md "wikilink")
  - [戰鎚Online](../Page/戰鎚Online.md "wikilink")
  - [光明戰記](../Page/光明戰記.md "wikilink")
  - [戰世紀](../Page/戰世紀.md "wikilink")
  - [纳纳炸机宝](../Page/拿拿林Online.md "wikilink")

<!-- end list -->

  -
    本游戏香港與台湾基本同步作內部測試，台湾官方也曾经使用了「**纳纳**」，随后决定重新选择名字，提供了几个选项让网友选择，最后使用了「**纳纳炸机宝**」一名，但是之后一直没有二测的消息，现在官网也默默下线。

<!-- end list -->

  - [戰地之王](../Page/戰地之王.md "wikilink")

<!-- end list -->

  -
    原廠不同意續約，代理權被轉移至[Garena競舞娛樂](../Page/Garena.md "wikilink")。

### 香港

#### 運營中

  - [戲谷娛樂館](../Page/戲谷娛樂館.md "wikilink")
      - 戲谷麻將館（港式十三張、台式十六張）
      - 戲谷幸運館（拉霸館(LuckyBAR)、跑馬風雲、水果盤）
      - 戲谷小鎮
  - [跑Online](../Page/跑Online.md "wikilink")

#### 將來運營

#### 已結束營運

  - [寵物王Online](../Page/寵物王.md "wikilink")
  - [龍族](../Page/龍族.md "wikilink")
  - [PushBear](../Page/PushBear.md "wikilink")
  - [幸福Online](../Page/幸福Online.md "wikilink")
  - [戰錘](../Page/戰錘.md "wikilink")
  - [光明戰記](../Page/光明戰記.md "wikilink")
  - [拿拿林Online](../Page/拿拿林Online.md "wikilink")
  - [創界](../Page/創界.md "wikilink")
  - [A.V.A 戰地之王](../Page/戰地之王.md "wikilink")
  - 戲谷娛樂館
      - 戲谷撲克館（鋤大Dee、13張、鬥地主）
      - 戲谷遊樂館（連族、方塊、坦克特攻隊、動物對對碰）
      - 戲谷盲棋館
      - [桌球館](../Page/桌球館.md "wikilink")
      - 戲谷幸運館（幸運賽豬、至尊21點、奇幻接龍、麻將九宮格、骰寶、決戰百家樂）

### 中國大陸

以下是中國戲谷於2007年8月23日之前曾經運營之遊戲。

''' 已結束營運 '''

  - [寵物王Online](../Page/寵物王Online.md "wikilink")
  - [龍族](../Page/龍族.md "wikilink")
  - [幸福花園](../Page/幸福花園.md "wikilink")
  - [戲谷娛樂館](../Page/戲谷娛樂館.md "wikilink")
      - 上海鬥地主
      - 鬥地主
      - 十三張麻將
      - 桌球
      - 戲谷遊樂場（歡樂3+1、閃電連連看、動物總動員）

## 其他產品

  - 戲谷時數卡（台灣）
  - 戲谷點數通（香港）

## 永久合作伙伴

  - [樂古資訊娛樂網有限公司](../Page/樂古資訊娛樂網.md "wikilink")

## 參考文獻

## 外部連結

  - [台灣戲谷](http://www.funtown.com.tw/)
  - [香港戲谷](http://www.funtown.com.hk/)
  - [台灣戰谷](http://www.wartown.com.tw/)

[Category:和信集團](../Category/和信集團.md "wikilink")
[Category:網路遊戲運營商](../Category/網路遊戲運營商.md "wikilink")
[Category:臺灣電子遊戲公司](../Category/臺灣電子遊戲公司.md "wikilink")
[Category:總部位於臺北市內湖區的工商業機構](../Category/總部位於臺北市內湖區的工商業機構.md "wikilink")
[Category:台灣博彩公司](../Category/台灣博彩公司.md "wikilink")

1.  [宏碁戲谷突破千萬人次
    為中文線上遊戲市場交出亮麗成績單\!](http://www.funtown.com.tw/news/19990103.html)－戲谷新聞稿，1999年1月3日。
2.  <http://www.wartown.com.tw> -
    和信超媒體旗下只有戲谷分公司，而戰谷官方網頁底部的聲明網頁所有權是屬於戲谷分公司，足以證明戰谷只是一個品牌，並不是分公司。
3.  <http://gnn.gamer.com.tw/7/19437.html> -
    時任戲谷總經理周謀式當時果決表示(對於萬人線上遊戲)「敬謝不敏」，以後絕不再碰任何
    MMOG。