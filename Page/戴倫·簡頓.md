**戴倫·愛德華·簡頓**（，）是一名[英格蘭前足球員](../Page/英格蘭.md "wikilink")，曾效力[諾域治](../Page/諾域治足球會.md "wikilink")、[修咸頓](../Page/修咸頓足球會.md "wikilink")、[李斯特城和](../Page/李斯特城足球會.md "wikilink")[列斯聯等球會](../Page/列斯聯足球會.md "wikilink")，現時擔任私人健身教練\[1\]。

## 球員生涯

戴倫·簡頓的球員生涯在[諾域治開始](../Page/諾域治足球會.md "wikilink")。2003年，戴倫·簡頓轉投[修咸頓](../Page/修咸頓足球會.md "wikilink")，在2006年1月7日，對[米爾頓凱恩斯的足總杯賽事中攻入其效力的第一球](../Page/米爾頓凱恩斯足球俱樂部.md "wikilink")。2006年5月，戴倫·簡頓遭[修咸頓解雇](../Page/修咸頓足球會.md "wikilink")，其後在同年的6月27日加盟[李斯特城](../Page/李斯特城.md "wikilink")。

雖然戴倫·簡頓在對[西布朗的賽事中](../Page/西布朗.md "wikilink")，攻入加盟以來的首球，其後卻受傷缺陣多個月。但戴倫·簡頓在傷癒後很快便重返一隊，不久，在對[史篤城的賽事中](../Page/史篤城.md "wikilink")，攻入效力[李斯特城的最後一球](../Page/李斯特城.md "wikilink")。

2007年6月19日，戴倫·簡頓被當時[李斯特城的領隊](../Page/李斯特城.md "wikilink")掛牌求售\[2\]，2008年1月10日，戴倫簡頓被外借至[列斯聯](../Page/列斯聯.md "wikilink")<small>\[3\]</small>。，不久後，在同年1月31日轉投列斯聯<small>\[4\]</small>。2008年5月28日，戴倫簡頓被列斯聯放棄<small>\[5\]</small>。10月3日以自由身加盟[-{zh-hans:切尔滕汉姆;zh-hk:車頓咸}-](../Page/切尔滕汉姆足球俱乐部.md "wikilink")，簽署3個月短期合約<small>\[6\]</small>。

## 參考文獻

## 外部連結

  -
  - [Career information at
    ex-canaries.co.uk](http://www.ex-canaries.co.uk/players/kenton.htm)

[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:諾域治球員](../Category/諾域治球員.md "wikilink")
[Category:修咸頓球員](../Category/修咸頓球員.md "wikilink")
[Category:李斯特城球員](../Category/李斯特城球員.md "wikilink")
[Category:車頓咸球員](../Category/車頓咸球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")

1.
2.  [Kenton On The
    List](http://www.lcfc.premiumtv.co.uk/page/LatestNews/0,,10274~1055007,00.html)

3.  [Kents on the
    move](http://www.lcfc.premiumtv.co.uk/page/LatestNews/0,,10274~1211256,00.html)

4.  [Leeds land Michalik and Foxes
    duo](http://news.bbc.co.uk/sport1/hi/football/teams/l/leeds_united/7220461.stm)
5.  [Pair offered new deals by
    Leeds](http://news.bbc.co.uk/sport1/hi/football/teams/l/leeds_united/7423536.stm)
6.  [Allen adds pair to Robins
    squad](http://news.bbc.co.uk/sport2/hi/football/teams/c/cheltenham_town/7649715.stm)