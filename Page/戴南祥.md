**戴南祥**（），[臺灣](../Page/臺灣.md "wikilink")[帶動唱創始人](../Page/帶動唱.md "wikilink")，部分人稱其“戴老師”，之後[外遇並且](../Page/外遇.md "wikilink")[離婚](../Page/離婚.md "wikilink")。
現在居住於[臺中市](../Page/臺中市.md "wikilink")，[私立天主教輔仁大學畢業](../Page/私立天主教輔仁大學.md "wikilink")。

戴南祥原是[臺北市立新興國民中學](../Page/臺北市立新興國民中學.md "wikilink")[童軍教師](../Page/童軍.md "wikilink")，其收集學生的創意並加以研發整理，並藉由於[臺視民歌節目](../Page/臺視.md "wikilink")《[大學城](../Page/大學城.md "wikilink")》的表演，讓帶動唱成為臺灣普遍的集會表演方式。帶動唱甚至從學生集會場所擴張至政治活動，至今仍偶於臺灣群眾場合偶然可見，其中包含改良過的童歌表演、政治活動、民歌表演場合。[中美斷交後](../Page/中美斷交.md "wikilink")，應聘擔任美國夏令營輔導員。2006年9月，曾參與[百萬人民倒扁運動](../Page/百萬人民倒扁運動.md "wikilink")。2014年5月4日，曾參與[新黨](../Page/新黨.md "wikilink")「新[五四運動](../Page/五四運動.md "wikilink")」開場表演。

## 評價

帶動唱在全盛時期，幾乎是臺灣校園的全民運動。2001年10月16日，[淡江大學歐洲研究所副教授兼](../Page/淡江大學.md "wikilink")[台灣教授協會副秘書長](../Page/台灣教授協會.md "wikilink")[林立](../Page/林立.md "wikilink")\[1\]說，[韓國總統](../Page/韓國總統.md "wikilink")[全斗煥任內為了平息韓國大學生的抗議運動](../Page/全斗煥.md "wikilink")，派人來學習臺灣校園帶動唱；但韓國大學生悍然拒絕學習，這些學生說：「我們不要玩這個，這東西我們在[幼稚園早就玩過了](../Page/幼稚園.md "wikilink")！」\[2\]

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [YouTube戴戴動唱頻道](http://www.youtube.com/user/watchtowerdai)

[D戴](../Category/臺灣藝術家.md "wikilink") [N南](../Category/戴姓.md "wikilink")
[D戴](../Category/台灣男歌手.md "wikilink")
[D戴](../Category/輔仁大學校友.md "wikilink")

1.
2.