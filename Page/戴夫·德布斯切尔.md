**戴维·阿尔伯特·德布斯切尔**（，），美国[NBA职业篮球运动员和教练](../Page/NBA.md "wikilink")，[奈史密斯籃球名人堂成员](../Page/奈史密斯籃球名人堂.md "wikilink")。他在[1962年NBA选秀中以](../Page/1962年NBA选秀.md "wikilink")[底特律大学球员的身份被](../Page/底特律大学.md "wikilink")[底特律活塞队选中](../Page/底特律活塞队.md "wikilink")，在随后的NBA生涯中帮助[纽约尼克斯夺得过两届](../Page/纽约尼克斯.md "wikilink")[总冠军](../Page/NBA总冠军.md "wikilink")，他也是联盟历史上最年轻的主教练。

## 生平

德布斯切尔出生在[密歇根州的](../Page/密歇根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")，完成了12年的职业篮球生涯(1962–1974)并入选篮球名人堂，他职业生涯取得场均16.1分和11个篮板，八次入选NBA全明星阵容；在场上，他以顽强的防守著称，曾六次入选最佳防守第一阵容，在他24岁那年，底特律活塞给了他一个球员兼教练的职位，从而成为联盟史上最年轻的教练员。德布斯切尔还出版了一本名为*The
Open Man*的书，书中阐述了[纽约尼克斯](../Page/纽约尼克斯.md "wikilink")1969-1970夺冠赛季的路程。

他的22号球衣直到很多年以后才在尼克斯退役，耽搁球衣退役是因为德布斯切尔接受了[ABA联盟的执行官这项直接对抗](../Page/ABA.md "wikilink")[纽约尼克斯乃至整个NBA联盟的工作](../Page/纽约尼克斯.md "wikilink")。

德布斯切尔在也1962-63期间也曾是[美国职棒大联盟](../Page/美国职棒大联盟.md "wikilink")[芝加哥白袜队的](../Page/芝加哥白袜队.md "wikilink")[投手](../Page/投手.md "wikilink")。他是仅有的12位分别在美国职棒以及NBA篮球联盟效力过的运动员之一(其他球员包括有[马克·亨德里克森](../Page/马克·亨德里克森.md "wikilink")、[丹尼·安吉](../Page/丹尼·安吉.md "wikilink")、[杰纳·康利](../Page/杰纳·康利.md "wikilink")、[罗恩·里德](../Page/罗恩·里德.md "wikilink")、[迪克·格鲁特](../Page/迪克·格鲁特.md "wikilink")、[史蒂夫·汉密尔顿](../Page/史蒂夫·汉密尔顿.md "wikilink")、[科顿·纳什](../Page/科顿·纳什.md "wikilink")、[弗兰克·包霍兹](../Page/弗兰克·包霍兹.md "wikilink")、[迪克·瑞克茨和](../Page/迪克·瑞克茨.md "wikilink")[查克·康纳斯](../Page/查克·康纳斯.md "wikilink"))。

在逝世前一年，他还出现在了Lee
Muffler电视广告片中。2003年，62岁的德布斯切尔因[心肌梗塞病逝于](../Page/心肌梗塞.md "wikilink")[纽约](../Page/纽约.md "wikilink")。

## 轶事

在**。德布斯切尔出现在电子游戏中。

## 参考资料

## 外部链接

  - [Dave DeBusschere career
    summary](http://www.nba.com/history/players/debusschere_summary.html)
    @ NBA.com

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:美国篮球教练](../Category/美国篮球教练.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")
[Category:纽约尼克斯队球员](../Category/纽约尼克斯队球员.md "wikilink")
[Category:芝加哥白袜队球员](../Category/芝加哥白袜队球员.md "wikilink")
[Category:底特律活塞队教练](../Category/底特律活塞队教练.md "wikilink")
[Category:籃球名人堂成員](../Category/籃球名人堂成員.md "wikilink")
[Category:NBA退休背號球员](../Category/NBA退休背號球员.md "wikilink")