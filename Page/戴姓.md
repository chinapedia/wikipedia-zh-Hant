**戴姓**是[漢姓之一](../Page/漢姓.md "wikilink")，在[百家姓中排名](../Page/百家姓.md "wikilink")116位，按人口計算排名第96位（2007年數據）。

## 來源

[漢族戴姓主要來源有三](../Page/漢族.md "wikilink")：[子姓](../Page/子姓.md "wikilink")[宋國](../Page/宋國.md "wikilink")[宋戴公後裔以](../Page/宋戴公.md "wikilink")[謚為姓](../Page/謚.md "wikilink")、子姓（或[姬姓](../Page/姬姓.md "wikilink")？）戴國君民以國為姓、[殷朝殷氏宗室](../Page/殷朝.md "wikilink")（本源上也應該是子姓）改姓戴。

1、出自宋國子姓，為[商湯的](../Page/商湯.md "wikilink")[後裔](../Page/後裔.md "wikilink")，以謚為氏。據《[元和姓纂](../Page/元和姓纂.md "wikilink")》及《[古今姓氏書辯證](../Page/古今姓氏書辯證.md "wikilink")》所載，周初，[周公旦在平定](../Page/周公旦.md "wikilink")「[管蔡之亂](../Page/管蔡之亂.md "wikilink")」後，封商朝末代[君主](../Page/君主.md "wikilink")[紂之庶兄](../Page/紂.md "wikilink")[微子啟](../Page/微子啟.md "wikilink")（子姓）於商的舊都（今[河南](../Page/河南.md "wikilink")[商丘南](../Page/商丘.md "wikilink")），建立宋國。宋國第11位君主（前799年-前766年），史佚其名，死後被謚為戴公。戴公傳子[宋武公司空](../Page/宋武公.md "wikilink")（前765年-前748年）其子孫遂以謚號「戴」為氏，是為河南戴氏。

2、以國為姓，出自春秋時期的一個諸侯國[戴國](../Page/戴國.md "wikilink")，據《通志·氏族略》及《[左傳](../Page/左傳.md "wikilink")》所載，[春秋時的戴國](../Page/春秋.md "wikilink")，為子姓或姬姓諸侯國，其國位於現今河南[民權縣東](../Page/民權縣.md "wikilink")，一說在河南[蘭考縣](../Page/蘭考縣.md "wikilink")。隱公十年（前713年）亡於[鄭國](../Page/鄭國.md "wikilink")，一說亡於宋國。國君因身為殷商後裔而封國，戴國國土位置在今天的河南省兰考县民权县一带。戴國被鄭國滅掉後，其國君以及全國子民遂以国为姓，全体姓戴，世代相传。

3、出自[殷朝的子姓](../Page/殷朝.md "wikilink")，據《[鼠璞](../Page/鼠璞.md "wikilink")》所載，[武王](../Page/武王.md "wikilink")[牧野之戰滅亡](../Page/牧野之戰.md "wikilink")[殷商後](../Page/殷商.md "wikilink")，有不少殷[宗室遺族以國為氏](../Page/宗室.md "wikilink")，稱[殷氏](../Page/殷氏.md "wikilink")，殷氏其後就有改姓戴的。這一支戴氏也是出自河南。

## 歷史名人

  - [戴崇](../Page/戴崇.md "wikilink")
  - [戴德](../Page/戴德.md "wikilink")：漢代禮學者，編修《大戴禮記》
  - [戴聖](../Page/戴聖.md "wikilink")：漢代禮學家，編修《小戴禮記》
  - [戴淵](../Page/戴淵.md "wikilink")：東晉名士
  - [戴逵](../Page/戴逵.md "wikilink")：東晉學者、畫家、雕塑家。反對佛教的因果報應說，著有《釋疑論》
  - 唐代父子（实为叔侄）宰相[戴冑](../Page/戴冑.md "wikilink")，[戴至德](../Page/戴至德.md "wikilink")
  - [戴叔倫](../Page/戴叔倫.md "wikilink")：唐代詩人，以田園詩著稱.
  - [戴復古](../Page/戴復古.md "wikilink")：南宋詩人，著有《石屏詩集》、《石屏詞》
  - [戴進](../Page/戴進.md "wikilink")：明代畫家
  - [戴潮春](../Page/戴潮春.md "wikilink")：台灣清同治年間配合太平天國起事民變領袖
  - [戴震](../Page/戴震.md "wikilink")：清代語言學家、思想家
  - [戴名世](../Page/戴名世.md "wikilink")：安徽桐城人，清代史學家。曾任翰林院編修。刊行有《南山集》
  - [戴祉庭](../Page/戴祉庭.md "wikilink")
  - [戴思恭](../Page/戴思恭.md "wikilink")：名醫
  - [戴衡](../Page/戴衡.md "wikilink")：詩人，「遼東三老」之一
  - [戴隆邦](../Page/戴隆邦.md "wikilink")：清代武術家，戴氏心意拳始創人
  - [戴二閭](../Page/戴二閭.md "wikilink")：清代武術家、鏢師，戴隆邦之子
  - [戴錶元](../Page/戴錶元.md "wikilink")：元朝詩人

## 近現代名人

  - [戴望舒](../Page/戴望舒.md "wikilink")：著名抒情詩人
  - [戴愛蓮](../Page/戴愛蓮.md "wikilink")：舞蹈家
  - [戴季陶](../Page/戴季陶.md "wikilink")
  - [戴安瀾](../Page/戴安瀾.md "wikilink")：著名將軍
  - [戴熙](../Page/戴熙.md "wikilink")
  - [戴鴻慈](../Page/戴鴻慈.md "wikilink")：清朝大臣
  - [戴笠](../Page/戴笠.md "wikilink")：中國陸軍上將，軍事統計局局長
  - [戴戟](../Page/戴戟.md "wikilink")
  - [戴平萬](../Page/戴平萬.md "wikilink")
  - [戴芳瀾](../Page/戴芳瀾.md "wikilink")
  - [戴-{于}-程](../Page/戴于程.md "wikilink")
  - [戴勝益](../Page/戴勝益.md "wikilink")：台灣企業家、[王品集團創辦人暨董事長](../Page/王品集團.md "wikilink")
  - [戴佩妮](../Page/戴佩妮.md "wikilink")：馬來西亞歌手
  - [戴愛玲](../Page/戴愛玲.md "wikilink")：台灣歌手
  - [戴玉強](../Page/戴玉強.md "wikilink")
  - [戴復東](../Page/戴復東.md "wikilink")
  - [戴相龍](../Page/戴相龍.md "wikilink")：中國政治家
  - [戴秉國](../Page/戴秉國.md "wikilink")：中國政治家、外交家
  - [戴嬌倩](../Page/戴嬌倩.md "wikilink")
  - [戴嬈](../Page/戴嬈.md "wikilink")
  - [戴志偉](../Page/戴志偉.md "wikilink")
  - [戴夢夢](../Page/戴夢夢.md "wikilink")
  - [戴耀明](../Page/戴耀明.md "wikilink")
  - [戴綺霞](../Page/戴綺霞.md "wikilink")：原名**戴志蘭**，京劇演員，工旦行，在新加坡出生
  - [戴思聰](../Page/戴思聰.md "wikilink")
  - [戴婉瑩](../Page/戴婉瑩.md "wikilink")
  - [戴東原](../Page/戴東原_\(醫界\).md "wikilink")：前臺大醫院院長，現任糖尿病關懷基金會董事長
  - [戴韫](../Page/戴韫.md "wikilink")
  - [戴立忍](../Page/戴立忍.md "wikilink")：台灣演員、導演。
  - [戴立綱](../Page/戴立綱.md "wikilink")：台灣氣象主播。
  - [戴資穎](../Page/戴資穎.md "wikilink")：台灣女子[羽毛球運動員](../Page/羽毛球.md "wikilink")。
  - [戴祖儀](../Page/戴祖儀.md "wikilink")：香港新晉女歌手

## 参考文献

## 参见

  - [中國姓氏](../Page/中國姓氏.md "wikilink")
      - [百家姓](../Page/百家姓.md "wikilink")

{{-}}

[D戴](../Category/漢字姓氏.md "wikilink") [戴姓](../Category/戴姓.md "wikilink")