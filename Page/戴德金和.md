**戴德金和**（Dedekind
sum）是數學家[戴德金在跟](../Page/戴德金.md "wikilink")[戴德金η函數有關的工作中提出的](../Page/戴德金η函數.md "wikilink")。

定義這個[函數](../Page/函數.md "wikilink")，首先要定義\(((x))\)：若\(x\)是[整數](../Page/整數.md "wikilink")，\(((x))=0\)，否則為\(x-[x]-0.5\)，其中\([x]\)是最大而又不大於\(x\)的整數。

對於非零整數\(h,k\)，戴德金和\(s(h,k)\)定義為
\(s(h,k) = \sum_{\mu = 0}^{k-1} ((\frac{\mu}{k})) ((\frac{h \mu}{k}))\)

若\(h,k\)[互質且均大於](../Page/互質.md "wikilink")0，有\(s(h,k) = \frac{1}{4k} \sum_{\mu=1}^{k-1} \cot\left(\frac{\pi h \mu}{k}\right ) \cot\left(\frac{\pi \mu}{k}\right)\)

## 公式

  - 有[公因數時](../Page/公因數.md "wikilink")：\(s(ch,ck) = s(h,k)\)
  - Petersson-Knopp恆等式：\(\sum_{d|n} \sum_{m=0}^{d-1} s\left(\frac{n}{d} h + mk, kd\right) = \sigma(n) s(h,k)\)，\(\sigma(n)\)為[因數函數](../Page/因數函數.md "wikilink")，是\(n\)的正因數之和。其中一個較易證明的特例為當\(p\)為[質數](../Page/質數.md "wikilink")，\((p+1) s(h,k) = s(ph,k) + \sum^{p-1}_{m = 0} s(h+mk,pk)\)
  - 周期性：\(s(nk+h,k) = s(h,k)\)
  - 若\(pq \equiv 1 \pmod{k}\)，\(s(p,k) = s(q,k)\)。
  - \(s(1,k) = \frac{(k-1)(k-2)}{12k}\)
  - 若\(k\)為[奇數](../Page/奇數.md "wikilink")，\(s(2,k) = \frac{(k-1)(k-5)}{24k}\)
  - 對於\(k \equiv 1 \pmod{h}\)，\(12hk s(h,k)=(k-1)(k-(h^2+1))\)
  - 對於\(k \equiv 2 \pmod{h}\)，\(12hk s(h,k)=(k-2)(k-(h^2+1)/2)\)
  - 對於\(k \equiv -1 \pmod{h}\)，\(12hk s(h,k)=k^2+(h^2-6h+2)k+(h^2+1)\)
  - 互反和：

<!-- end list -->

  -

      -
        \(s(h,k)+s(k,h) = - \frac{1}{4} + \frac{1}{12} \left(\frac{h}{k}+\frac{1}{hk}+\frac{k}{h}\right)\)

## 參考

  - <https://web.archive.org/web/20070929120859/http://gifted.hkedcity.net/Gifted/Download/notes/0607math2phase/advanced/06-11-4-11-18_dedekind%20sums.pdf>
  - <http://mathworld.wolfram.com/DedekindSum.html>
  - <http://arxiv.org/abs/math/0112077>

[Category:數論](../Category/數論.md "wikilink")