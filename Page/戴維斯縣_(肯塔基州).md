**戴維斯縣**（**Daviess County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[西北部的一個縣](../Page/西部煤礦區.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[印地安納州相望](../Page/印地安納州.md "wikilink")。面積1,234平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口91,545人。縣治[歐文斯伯勒](../Page/歐文斯伯勒.md "wikilink")
(Owensboro)。

成立於1815年1月14日。縣名紀念在[蒂帕卡努之役中陣亡的](../Page/蒂帕卡努之役.md "wikilink")[約瑟·H·戴維斯](../Page/約瑟·H·戴維斯.md "wikilink")
（Joseph Hamilton Daveiss，縣名為手民之誤）\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[D](../Category/肯塔基州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.