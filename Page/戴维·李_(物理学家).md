**戴维·莫里斯·李**（,
)，[纽约州](../Page/纽约州.md "wikilink")[拉伊](../Page/拉伊_\(紐約州\).md "wikilink")），[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，1996年，因為發現了在[氦-3裏的](../Page/氦-3.md "wikilink")[超流動性](../Page/超流體.md "wikilink")，與[道格拉斯·奧謝羅夫](../Page/道格拉斯·奧謝羅夫.md "wikilink")、[羅伯特·理查森共同榮获](../Page/羅伯特·理查森.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站戴维·李自传](https://web.archive.org/web/20100124022856/http://nobelprize.org/nobel_prizes/physics/laureates/1996/lee-autobio.html)

[L](../Category/美国物理学家.md "wikilink")
[L](../Category/诺贝尔物理学奖获得者.md "wikilink")
[L](../Category/美國諾貝爾獎獲得者.md "wikilink")
[L](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[L](../Category/康乃狄克大學校友.md "wikilink")
[L](../Category/耶魯大學校友.md "wikilink")
[L](../Category/哈佛大學校友.md "wikilink")
[L](../Category/德州農工大學教師.md "wikilink")
[L](../Category/紐約州人.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")