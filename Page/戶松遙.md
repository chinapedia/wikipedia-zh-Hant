**戶松遙**（）是[日本的女性](../Page/日本.md "wikilink")[声優](../Page/声優.md "wikilink")、[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。[Music
Ray'n所屬](../Page/Music_Ray'n.md "wikilink")。[愛知縣](../Page/愛知縣.md "wikilink")[一宮市出身](../Page/一宮市.md "wikilink")。血型為B型。身高165cm。\[1\]\[2\]

代表作有 《[出包王女](../Page/出包王女.md "wikilink")》
(菈菈)、《[未聞花名](../Page/未聞花名.md "wikilink")》（安城鳴子)、《[刀劍神域](../Page/刀劍神域.md "wikilink")》（亞絲娜）、《[Happiness
Charge
光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")》（冰川伊緒奈／命運天使）、《[妖怪手錶](../Page/妖怪手錶.md "wikilink")》（天野景太）《[DARLING
in the FRANXX](../Page/DARLING_in_the_FRANXX.md "wikilink")》(02) 等\[3\]。

## 概要

### 經歷

  - 於2005年至2006年裡所舉辦的「Music Ray'n 超級聲優選拔」活動中金榜題名，並從此開始了聲優之路。
  - 2006年1月份舉辦的第六屆的東寶シンデレラ（東寶灰姑娘選拔\[暫\]）中，從3萬7443人裡脫穎而出，是能進入最終選拔中15人的其中一人。
  - 2007年以演出「[神曲奏界](../Page/神曲奏界.md "wikilink")」裡的女主角「克緹卡兒蒂·阿巴·拉格蘭潔絲」後獲得各界注目，參加動畫演出的機會也於此增加。
  - 2008年高中畢業前因仍就讀[愛知縣](../Page/愛知縣.md "wikilink")[一宮市當地的學校](../Page/一宮市.md "wikilink")，所以每週末必須來回穿梭於東京及名古屋來經營她的聲優事業\[4\]。錄音的前一天都會住在東京都內之事已經習以為常\[5\]。
  - 2008年高中畢業後隻身前往東京開始一人生活，一邊就讀大學的同時一邊經營聲優事業\[6\]。
  - 2008年9月3日於連續劇「夢回綠園
    ～青春男子寮日誌～」裡擔任「新田美惠子」的角色，同時以個人名義發表出道單曲也是該劇片尾曲的『naissance』藉此宣佈將進軍歌壇（隸屬於Music
    Ray'n公司）。
  - 2009年2月15日在「Music Ray'n 女孩們的春天巧克力祭典」活動中與同樣屬於Music
    Ray'n的壽美菜子、高垣彩陽、豐崎愛生三人組成聲優團體「Sphere」進行音樂活動（隸屬於Glory
    Heaven）。
  - 2009年獲得[聲優獎之新人女優賞的獎項](../Page/聲優獎.md "wikilink")。
  - 2009年10月9日發售第一本寫真集「HARUKAs」。
  - 2009年12月6日為了紀念Sphere第三張單曲的發售於東京的乃木坂Sony Studio裡舉行了活動與Pl@net
    Sphere的公開錄音中宣布於2010年2月份發售個人第二寫真集與首張專輯。
  - 2019年1月11日在個人部落格宣布結婚，對象為一般人。\[7\]

### 特色

  - 有著可以表現出青澀少女或成年女性的特質，是個豐富多彩的嗓音的同時，所演出的角色以開朗的少女及驕蠻系的女生比較多，不過有時也會擔任輕浮的角色或是反派角色。
  - 由於自己本身的年齡與具有張力的嗓音，基本上比較偏向於演出開朗活潑的少女，但是有時也會演出像是成熟淑女或充滿神秘感的角色，演出範圍極為寬廣。此外自己在演出以下類型角色時也獲得一致的公認：[神](../Page/神.md "wikilink")（[神薙](../Page/神薙.md "wikilink")）、[精靈](../Page/精靈.md "wikilink")（[神曲奏界](../Page/神曲奏界.md "wikilink")）、[外星人](../Page/外星人.md "wikilink")（[出包王女](../Page/出包王女.md "wikilink")）、[幽靈](../Page/幽靈.md "wikilink")（[機巧魔神](../Page/機巧魔神.md "wikilink")），因此演出神秘角色的機會也不少。

### 人物

  - 想當上聲優的契機是在國中三年級時，對演戲頗具興趣，還曾想過說要替[吉卜力工作室的動畫作品配音](../Page/吉卜力工作室.md "wikilink")。但是對於這條人生道路，心情一直含糊不定，最後接受了朋友的建議前去參加試鏡。
  - 興趣是圖畫創作，但據說也僅止於在腳本上面的塗鴉。
  - 自稱鮫マニア（Shark
    Mania），但並非是喜歡[鯊魚](../Page/鯊魚.md "wikilink")，而是為了避免來自鯊魚的危機而對牠們的習性比較了解。
  - 因為有著「要不要來學點樂器」這樣的念頭，於2009年1月買了[電貝斯](../Page/電貝斯.md "wikilink")，現在擁有兩把。
  - 憧憬著日本女演員也兼任聲優的[戶田惠子](../Page/戶田惠子.md "wikilink")。
  - 由於多部作品都演姊妹，所以和[花澤香菜感情很好](../Page/花澤香菜.md "wikilink")，但有趣的是，不論是入行時間還是實際年紀花澤都比戶松長和大，但動畫中卻總是戶松演姊姊。

### 歌手

  - 除了以「[Sphere](../Page/sphere_\(聲優團體\).md "wikilink")」的名義進行團體音樂活動之外，個人名義的演出與唱片也陸續地在進行。像是自己本身就在參與演出的「神薙」中擔任片頭曲與片尾曲的演唱，而片頭曲的『motto☆派手にね\!』在發售之時還達成過當週排行榜第10名，獲得了更大的注目。
  - 會依據音樂曲風的不同決定用什麼樣的嗓音唱歌，對她本人來說這有一種接近演戲時的感覺。

### 趣事

  - 在參加試鏡表現自我魅力與能力時，一字不漏地背出「[神隱少女](../Page/神隱少女.md "wikilink")」的台詞，重點還是一人分飾五角。\[8\]

## 演出作品

※**粗體**字為擔任該作品之主要角色

### 電視動畫

  - 2007年

<!-- end list -->

  - [校園烏托邦 學美向前衝！](../Page/校園烏托邦_學美向前衝！.md "wikilink")（スイーツ生徒、學生）
  - [星界死者之書](../Page/星界死者之書.md "wikilink")（まゆら）
  - [神曲奏界](../Page/神曲奏界.md "wikilink")（**克緹卡兒蒂·阿巴·拉格蘭潔絲**）
  - [天翔少女](../Page/天翔少女.md "wikilink")（蒔原彌生）
  - [地球防衛少年](../Page/地球防衛少年.md "wikilink")（フタバ／矢村双葉）
  - [萌單](../Page/萌單.md "wikilink")（**黑威澄**）
  - [悲慘世界 少女珂賽特](../Page/悲慘世界_少女珂賽特.md "wikilink")（奧黛麗）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [神薙](../Page/神薙.md "wikilink")（**薙**）
  - [機動戰士高達00](../Page/機動戰士高達00.md "wikilink")（**蜜蕾娜·瓦斯提**）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（**姫宮千子 / 亂崎千花**）
  - [鋼鐵新娘](../Page/鋼鐵新娘.md "wikilink")（**艾姆艾姆**）
  - [絕對可愛CHILDREN](../Page/絕對可愛CHILDREN.md "wikilink")（**三宮紫穗**）
  - [鐵腕女警DECODE](../Page/鐵腕女警DECODE.md "wikilink")（神官的太空船）
  - [出包王女](../Page/出包王女.md "wikilink")（**菈菈·薩塔林·戴比路克**）
  - [交響情人夢巴黎篇](../Page/交響情人夢.md "wikilink")（ごろ太）
  - [魍魎之匣](../Page/魍魎之匣.md "wikilink")（柚木加菜子）
  - [夜櫻四重奏](../Page/夜櫻四重奏.md "wikilink")（**岸桃華**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [明日的與一](../Page/明日的與一.md "wikilink")（**斑鳩菖蒲**）
  - [機巧魔神](../Page/機巧魔神.md "wikilink")（**水無神操緒**）
  - [機巧魔神2](../Page/機巧魔神.md "wikilink")（**水無神操緒**）
  - [CANAAN](../Page/CANAAN.md "wikilink")（云云）
  - [四葉遊戲](../Page/四葉遊戲.md "wikilink")（**月島青葉**）
  - [GA 藝術科美術設計班](../Page/GA_藝術科美術設計班.md "wikilink")（**山口如月**）
  - [地獄少女 三鼎](../Page/地獄少女_三鼎.md "wikilink")（柏木秀美）
  - [神曲奏界POLYPHONICA
    crimsonS](../Page/神曲奏界.md "wikilink")（**克緹卡兒蒂·阿巴·拉格蘭潔絲**）
  - [浪漫追星社](../Page/浪漫追星社.md "wikilink")（**蒔田姫**）
  - [戰鬥司書 The book of
    Bantorra](../Page/戰鬥司書系列.md "wikilink")（**洛蘿緹＝瑪爾伽**）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（灣內絹保）
  - [貓願三角戀](../Page/貓願三角戀.md "wikilink")（**桐嶋朱莉**、**桐嶋琴音**）
  - [BASQUASH\!](../Page/BASQUASH!.md "wikilink")（**魯修**）
  - [WHITE ALBUM](../Page/WHITE_ALBUM.md "wikilink")（觀月麻奈）
  - [瑪莉亞的凝望 第4季](../Page/瑪莉亞的凝望.md "wikilink")（高知日出實）
  - [簡單易懂的現代魔法](../Page/簡單易懂的現代魔法.md "wikilink")（**一之瀨·弓子·克莉絲汀娜**）
  - [碧陽學園學生會議事錄](../Page/碧陽學園學生會議事錄.md "wikilink")（宮代奏）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [閃電十一人](../Page/閃電十一人.md "wikilink")（**久遠冬花**、鈴目）
  - [刀語](../Page/刀語.md "wikilink")（**否定姫**）
  - [空·之·音](../Page/空·之·音.md "wikilink")（瑪利亞）
  - [無頭騎士異聞錄
    DuRaRaRa\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（神近莉緒）
  - [管家後宮學園](../Page/管家後宮學園.md "wikilink")（哈蒂耶）
  - [屍鬼](../Page/屍鬼.md "wikilink")（**清水惠**）
  - [最後大魔王](../Page/最後大魔王.md "wikilink")（照屋榮子）
  - [玩伴貓耳娘](../Page/玩伴貓耳娘.md "wikilink")（**金武城真奈美**）
  - [超元氣3姊妹](../Page/超元氣3姊妹.md "wikilink")（**丸井一葉**）
  - [出包王女第二季](../Page/出包王女.md "wikilink")（**菈菈·薩塔琳·戴比路克**）
  - [STAR DRIVER
    閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")（**氣多的巫女**）
  - [半妖少女綺麗譚](../Page/半妖少女綺麗譚.md "wikilink")（百綠）
  - [女僕咖啡廳](../Page/女僕咖啡廳_\(漫畫\).md "wikilink")（西老師）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [花開物語](../Page/花開物語.md "wikilink")（**和倉結名**）
  - [超元氣3姊妹 增量中](../Page/超元氣3姊妹.md "wikilink")（**丸井一葉**）
  - [我們仍未知道那天所看見的花的名字。](../Page/我們仍未知道那天所看見的花的名字。.md "wikilink")（**安城鳴子**）
  - [「C」](../Page/C_\(動畫\).md "wikilink")（**真朱**）
  - [惡魔奶爸](../Page/惡魔奶爸.md "wikilink")（花澤由加、安潔莉卡）
  - [閃電十一人GO](../Page/閃電十一人GO.md "wikilink")（**西園信助**、桃山我聞）
  - [沉默的森田同學](../Page/沉默的森田同學.md "wikilink")（**村越美樹**）
  - [沉默的森田同學。2](../Page/沉默的森田同學。2.md "wikilink")（**村越美樹**）
  - [眾神中的貓神](../Page/眾神中的貓神.md "wikilink")（**繭**）
  - [WORKING'\!\!](../Page/迷糊餐厅.md "wikilink")（真柴美月）
  - [學園救援團](../Page/學園救援團.md "wikilink")（加納亞里莎）
  - [偶像大師](../Page/偶像大師_\(動畫\).md "wikilink")（日高愛）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [在盛夏等待](../Page/在盛夏等待.md "wikilink")（**貴月一花**）
  - [猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")（**格里埃爾·塞雷尼提**）
  - [戀研！](../Page/戀研！.md "wikilink")（**若宫楓**）
  - [夏色奇蹟](../Page/夏色奇蹟.md "wikilink")（**花木優香**）
  - [加速世界](../Page/加速世界.md "wikilink")（若宮惠）
  - [刀劍神域](../Page/刀劍神域.md "wikilink")（**亞絲娜／結城明日奈**）
  - [貧乏神來了](../Page/窮神_\(漫畫\).md "wikilink")（**龍膽嵐丸**）
  - [閃電十一人GO 時空之石](../Page/閃電十一人GO.md "wikilink")（**西園信助**）
  - [心連·情結](../Page/心連·情結.md "wikilink")（西野菜菜）
  - [出包王女DARKNESS](../Page/出包王女.md "wikilink")（**菈菈·薩塔琳·戴比路克**）
  - [鄰座的怪同學](../Page/鄰座的怪同學.md "wikilink")（**水谷雫**）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（**摩兒迦娜**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [魔王勇者](../Page/魔王勇者.md "wikilink")（女僕姐〈農奴姐〉）
  - [THE UNLIMITED 兵部京介](../Page/楚楚可憐超能少女組.md "wikilink")（三宮紫穗）
  - [百花繚亂 SAMURAI
    BRIDE](../Page/百花繚亂_SAMURAI_GIRLS.md "wikilink")（荒木又右衛門）
  - [星光少女 Rainbow
    Live](../Page/星光少女_Rainbow_Live.md "wikilink")（**蓮城寺貝露**）
  - [革命機Valvrave](../Page/革命機Valvrave.md "wikilink")（**流木野咲**）
  - [科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")（灣内絹保）
  - [閃電十一人GO 銀河](../Page/閃電十一人GO.md "wikilink")（西園信助、瞬木瞬）
  - [只有神知道的世界 女神篇](../Page/只有神知道的世界.md "wikilink")（瑠寧）
  - [黏黏糊糊\!\!角質君](../Page/黏黏糊糊!!角質君.md "wikilink")（ニュナ、うさぎカクセン）
  - [核爆末世錄](../Page/核爆末世錄.md "wikilink")（**成瀨荊**）
  - [當不成勇者的我，只好認真找工作了。](../Page/當不成勇者的我，只好認真找工作了。.md "wikilink")（戶松遙）
  - [魔奇少年 The Kingdom of Magic](../Page/魔奇少年.md "wikilink")（**摩兒迦娜**）
  - [我要成為世界最強偶像](../Page/我要成為世界最強偶像.md "wikilink")（風間璃緒）
  - [夜櫻四重奏 -花之歌-](../Page/夜櫻四重奏.md "wikilink")（**岸桃華**）
  - [武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")（**真野茉莉**）
  - [刀劍神域 Extra Edition](../Page/刀劍神域.md "wikilink")（**亞絲娜／結城明日奈**）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [妖怪手錶](../Page/妖怪手錶.md "wikilink")（**天野景太**、石森勇香）
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（獄卒）
  - [櫻Trick](../Page/櫻Trick.md "wikilink")（**高山春香**）
  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")（卡莉娜）
  - [Happiness Charge
    光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")（**冰川伊绪奈／命運天使**、記者）
  - [魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")（壬生紗耶香）
  - [刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")（**亞絲娜／結城明日奈**）
  - [火星異種](../Page/火星異種.md "wikilink")「阿聶克斯一號篇」（**源百合子**）
  - [魔彈之王與戰姬](../Page/魔彈之王與戰姬.md "wikilink")（**艾蕾歐諾拉·維爾塔利亞**）
  - [-{zh-cn:临时女友;zh-tw:女友伴身邊}-](../Page/臨時女友.md "wikilink")（笹原野野花）
  - [漫畫家與助手們](../Page/漫畫家與助手們.md "wikilink")(**音砂見張**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [絕對雙刃](../Page/絕對雙刃.md "wikilink")（永倉伊萬里）
  - [東京喰種√A](../Page/東京喰種.md "wikilink")（安久奈白）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    承](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（神近莉緒）
  - [果然我的青春戀愛喜劇搞錯了。續](../Page/果然我的青春戀愛喜劇搞錯了。.md "wikilink")（折本佳織）
  - [在地下城尋求邂逅是否搞錯了什麼](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink")（埃伊娜·祖爾）
  - [御神樂學園組曲](../Page/御神樂學園組曲.md "wikilink")（喵琳）
  - [Punch Line](../Page/Punch_Line.md "wikilink")（**秩父拉布拉**）
  - [銀魂°](../Page/銀魂_\(動畫\).md "wikilink")（**坂田銀子**）
  - [GATE 奇幻自衛隊 在彼方的土地，如斯的戰鬥](../Page/奇幻自衛隊.md "wikilink")（**平娜·戈·蘭達**）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    轉](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（神近莉緒）
  - [迷糊餐廳 第三期](../Page/迷糊餐廳.md "wikilink")（真柴美月）
  - [出包王女DARKNESS 2nd](../Page/出包王女.md "wikilink")（**菈菈·薩塔琳·戴比路克**）
  - [不管怎樣都想成為干支](../Page/不管怎樣都想成為干支.md "wikilink")（雞）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [GATE 奇幻自衛隊](../Page/GATE_奇幻自衛隊.md "wikilink") 在彼方的土地，如斯的戰鬥
    第2期（**平娜·戈·蘭達**）
  - [灰與幻想的格林姆迦爾](../Page/灰與幻想的格林姆迦爾.md "wikilink")（慕茲蜜）
  - [火星異種 復仇](../Page/火星異種.md "wikilink")（**源百合子**）
  - [機動戰士GUNDAM UC
    RE:0096](../Page/機動戰士GUNDAM_UC.md "wikilink")（**米寇特·帕奇**）
  - [ReLIFE](../Page/ReLIFE.md "wikilink")（**狩生玲奈**）
  - [天真與閃電](../Page/天真與閃電.md "wikilink")（小鹿忍）
  - [時間旅行少女～真理、和花與8名科學家～](../Page/時間旅行少女～真理、和花與8名科學家～.md "wikilink")（**早瀨晶**）
  - [齊木楠雄的災難](../Page/齊木楠雄的災難.md "wikilink")（齊木楠子）
  - [WWW.WORKING\!\!](../Page/WORKING!!_\(網路漫畫\).md "wikilink")（**宮越華**）
  - [神裝少女小纏](../Page/神裝少女小纏.md "wikilink")（**克拉露絲·托尼托露絲**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（矢萩惠美 ）
  - [幼女戰記](../Page/幼女戰記.md "wikilink")（瑪麗）
  - [人渣的本願](../Page/人渣的本願.md "wikilink")（**繪鳩早苗**）
  - [在地下城尋求邂逅是否搞錯了什麼 外傳
    劍姬神聖譚](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink")（埃伊娜·祖爾）
  - [徒然喜歡你](../Page/徒然喜歡你.md "wikilink")（古屋螢）
  - [側車搭檔](../Page/側車搭檔.md "wikilink")（永井美咲）
  - Wake Up, Girls\! 新章（卡莉娜）
  - [無論何時我們的戀情都是10厘米。](../Page/告白實行委員會_～戀愛系列～.md "wikilink")（**榎本夏樹**）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [紫羅蘭永恆花園](../Page/紫羅蘭永恆花園.md "wikilink")（愛麗絲·卡娜莉）
  - [DARLING in the
    FRANXX](../Page/DARLING_in_the_FRANXX.md "wikilink")（**02**）
  - 齊木楠雄的災難 第2期（齊木楠子/栗子）
  - [龍王的工作！](../Page/龍王的工作！.md "wikilink")（祭神雷）
  - [閃電十一人 戰神的天秤](../Page/閃電十一人_戰神的天秤.md "wikilink")（*'服部半太*、宮野茜）
  - [女神異聞錄5 the Animation](../Page/女神異聞錄5.md "wikilink")（**奧村春**\[9\]）
  - [天狼 Sirius the
    Jaeger](../Page/天狼_Sirius_the_Jaeger.md "wikilink")（尤里〈幼少期〉）
  - 刀劍神域 Alicization（**亞絲娜／結城明日奈**）
  - [哥布林殺手](../Page/哥布林殺手.md "wikilink")（劍聖）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [笨拙之極的上野](../Page/笨拙之極的上野.md "wikilink")（北長同學）
  - [八十龜醬觀察日記](../Page/八十龜醬觀察日記.md "wikilink")（**八十龜最中**\[10\]）
  - [女高中生的虛度日常](../Page/女高中生的虛度日常.md "wikilink")（**菊池茜**\[11\]）

<!-- end list -->

  - 時期未定

<!-- end list -->

  - [喜歡本大爺的竟然就妳一個？](../Page/喜歡本大爺的竟然就妳一個？.md "wikilink")（**三色苑堇子**\[12\]）

### OVA

  - 2007年

<!-- end list -->

  - [草莓棉花糖 Original Video Animation](../Page/草莓棉花糖.md "wikilink")（女學生）
  - [Tales of Symphonia THE ANIMATION
    VOL.3](../Page/Tales_of_Symphonia_THE_ANIMATION_VOL.3.md "wikilink")（女子）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [世外魔島～黑月之王與雙月公主～](../Page/世外魔島～黑月之王與雙月公主～.md "wikilink")（**卡蕾特·費依·索瓦裘**）
  - ピューと吹く\!ジャガー リターン・オブ・約1年ぶり（サヤカ）

<!-- end list -->

  - 2009年

<!-- end list -->

  - きグルみっくV3 -ベストエピソードコレクション-（**江戸前あづき**）
  - [出包王女 OVA](../Page/出包王女.md "wikilink")（**菈菈·撒塔琳·戴比路克**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [飛輪少年 黑色羽毛與沉睡森林](../Page/飛輪少年.md "wikilink")（**野山野林檎**）
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")（米寇特·巴奇）
  - 劍與魔法與学園。2（**ジェラート**）
  - [GA 藝術科美術設計班](../Page/GA_藝術科美術設計班.md "wikilink") OVA（**山口如月**）
  - [楚楚可憐超能少女組～愛多憎生！被奪走的未來？～](../Page/楚楚可憐超能少女組.md "wikilink")（**三宮紫穗**）
  - [夜櫻四重奏～星之海～](../Page/夜櫻四重奏.md "wikilink")（岸桃華）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink") OVA（灣内絹保）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [Carnival Phantasm](../Page/Carnival_Phantasm.md "wikilink")（セブン）
  - [沉默的森田同學](../Page/沉默的森田同學.md "wikilink")（村越美樹）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [眾神中的貓神](../Page/眾神中的貓神.md "wikilink")（**繭**）
  - [夏色奇蹟 第15次暑假](../Page/夏色奇蹟.md "wikilink")（**花木優香**）
  - [出包王女DARKNESS](../Page/出包王女.md "wikilink")（**菈菈·撒塔琳·戴比路克**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [鄰座的怪同學 鄰座的極道同學](../Page/鄰座的怪同學.md "wikilink")（**雫**） ※漫畫12卷附贈DVD特裝版
  - [夜櫻四重奏～對月哭泣～](../Page/夜櫻四重奏.md "wikilink")（岸桃華）
  - 出包王女DARKNESS（**菈菈·撒塔琳·戴比路克**）※漫畫8、9卷附贈DVD特裝版

<!-- end list -->

  - 2014年

<!-- end list -->

  - 出包王女DARKNESS（**菈菈·撒塔琳·戴比路克**）※漫畫12卷附贈DVD特裝版

<!-- end list -->

  - 2016年

<!-- end list -->

  - 出包王女DARKNESS（**菈菈·撒塔琳·戴比路克**）※漫畫16卷附贈DVD特裝版

<!-- end list -->

  - 2018年

<!-- end list -->

  - [ReLIFE"完結篇"](../Page/ReLIFE.md "wikilink")（**狩生玲奈**）

### 動畫電影

  - [劇場版 機動戰士GUNDAM00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士GUNDAM00_-A_wakening_of_the_Trailblazer-.md "wikilink")（**蜜蕾娜·瓦斯提**）
  - [閃電十一人GO 究極之絆 獅鷲獸](../Page/閃電十一人GO_究極之絆_獅鷲獸.md "wikilink")（**西園信助**）
  - [劇場版 花開物語 HOME SWEET HOME](../Page/花開物語.md "wikilink")（和倉結名）
  - [劇場版 星光少女 全星選拔
    星光☆十強](../Page/星光少女_All_Star_Selection.md "wikilink")（**蓮城寺貝兒**）
  - [猛烈宇宙海賊 ABYSS OF HYPERSPACE
    -超空間的深淵-](../Page/迷你裙宇宙海賊.md "wikilink")（**格里埃爾·塞雷尼提**）
  - [我們仍未知道那天所看見的花名。](../Page/我們仍未知道那天所看見的花名。.md "wikilink")（**安城鳴子**）
  - [Wake Up, Girls\! 七位偶像](../Page/Wake_Up,_Girls!.md "wikilink")（卡莉娜）
  - [光之美少女 All Stars
    春之嘉年華♪](../Page/光之美少女_All_Stars_春之嘉年華♪.md "wikilink")（**冰川伊緒奈／命運天使**）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [從好久以前就喜歡你。～告白實行委員會～](../Page/從好久以前就喜歡你。～告白實行委員會～.md "wikilink")（**榎本夏樹**）
  - [喜歡上你的那瞬間。～告白實行委員會～](../Page/告白實行委員會_～戀愛系列～.md "wikilink")（榎本夏樹）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [刀劍神域劇場版
    -序列爭戰-](../Page/刀劍神域劇場版_-序列爭戰-.md "wikilink")（**亞絲娜／結城明日奈**）
  - [魔法少女奈叶Reflection](../Page/魔法少女奈叶Reflection.md "wikilink")（**阿米蒂埃·弗洛利安**）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [魔法少女奈叶Detonation](../Page/魔法少女奈叶Detonation.md "wikilink")（**阿米蒂埃·弗洛利安**）

### 網路動畫

  - 2008年

<!-- end list -->

  - [企鵝美眉♥心](../Page/企鵝美眉.md "wikilink")（海豚）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [絲襪視界](../Page/絲襪視界.md "wikilink")（**藍川蓮**）

### 特攝

  - [獸電戰隊強龍者](../Page/獸電戰隊強龍者.md "wikilink")（喜之戰騎嘉黎蕾配音與偽裝型態客串演出）

### 電視演出

  - 電視連續劇

<!-- end list -->

  - [RHプラス](../Page/RHプラス.md "wikilink")（岬亞美）
  - [夢回綠園 〜青春男子寮日誌〜](../Page/夢回綠園.md "wikilink")（新田美惠子）
  - ドクロゲキ2 サイドストーリー 「川口典子」
  - モンキーパーマ3 （晴海奈美）
  - 声ガール\!（戸松遥）

<!-- end list -->

  - 綜藝節目

<!-- end list -->

  - [NHK-BS2](../Page/NHK-BS2.md "wikilink")《[The☆Net
    Star\!](../Page/The☆Net_Star!.md "wikilink")》（第12回來賓身份出場）
  - [東京電視台](../Page/東京電視台.md "wikilink")《[AniTV Presents Anisong
    PLUS+](../Page/Anisong_PLUS.md "wikilink")》（來賓身份出場）
  - [埼玉電視台等台](../Page/埼玉電視台.md "wikilink")《[Run Run
    LAN\!](../Page/Run_Run_LAN!.md "wikilink")》（來賓身份出場）
  - [日本電視台](../Page/日本電視台.md "wikilink")《[Sphere
    Club](../Page/Sphere_Club.md "wikilink")》（1\~22回）

<!-- end list -->

  - 紀錄片(發展過程)

<!-- end list -->

  - うたたね♪（[TOKYO MX](../Page/TOKYO_MX.md "wikilink")：2009年9月12日、特別來賓）

### 廣播連續劇

  - （）

      - [日本放送節目](../Page/日本放送.md "wikilink")[有樂町動畫小鎮內的廣播連續劇](../Page/有樂町動畫小鎮.md "wikilink")

  - [VOMIC](../Page/VOMIC.md "wikilink")

      - [偽戀](../Page/偽戀.md "wikilink")（**桐崎千棘**）

### 廣播節目

  - [Mini
    Town](../Page/Mini_Town.md "wikilink")（日本放送『有樂町動畫小鎮』裡的小單元：2007年2月4日至2007年2月18日）
  - [萌英單語 Listening
    RADIO](../Page/萌英單語_Listening_RADIO.md "wikilink")（2007年7月6日開始至今，在日本[BEAT☆Net
    Radio\!及](../Page/BEAT☆Net_Radio!.md "wikilink")[Lantis web
    radio兩網站中可收聽到與](../Page/Lantis_web_radio.md "wikilink")[森永理科一同演出的線上廣播](../Page/森永理科.md "wikilink")）
  - [かんなぎRadio](../Page/かんなぎRadio.md "wikilink")（位於該作品官方網站，始於2008年6月11日）
  - [絕對可憐放送局](../Page/絕對可憐放送局.md "wikilink")

### 廣播劇CD

  - [愛を歌うより俺に溺れろ\!](../Page/愛を歌うより俺に溺れろ!.md "wikilink")
  - [海之彼方](../Page/海之彼方.md "wikilink")（**南雲火凜**）
  - ELEMENT GIRLS 元素周期 萌えて覚える化学の基本（[氟](../Page/氟.md "wikilink")）
  - [神薙](../Page/神薙.md "wikilink") 廣播劇CD（月刊Comic REX
    2008年12月號特別付錄）（**薙**）
  - 神薙 廣播劇CD『高1の夏は一度しかない+ちょっと』（**薙**）
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink") CD廣播劇・スペシャル3
    アナザーストーリー COOPERATION-2312（蜜蕾娜·瓦斯提）
  - [出包王女](../Page/出包王女.md "wikilink")（**菈菈·薩塔琳·戴比路克**）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（**亂崎千花**）
  - [創聖的亞庫艾里翁](../Page/創聖的亞庫艾里翁.md "wikilink")\~太陽之翼\~（Alicia公主）
  - [絕對可愛CHILDREN 廣播劇CD
    ESP.1st](../Page/絕對可愛CHILDREN.md "wikilink")（**三宮紫穗**）
  - [神聖福音](../Page/神聖福音.md "wikilink")（木下熨斗目）
  - [魔法禁書目錄](../Page/魔法禁書目錄.md "wikilink")
  - [刀語](../Page/刀語.md "wikilink")（**否定姫**）
  - [星歌戀曲](../Page/星歌戀曲.md "wikilink")（**椎名紗久也**）
  - [刀劍神域](../Page/刀劍神域.md "wikilink")（**亞絲娜／結城明日奈**）
  - 閃電十一人GO 廣播劇CD「永遠之絆\!\!」（**西園信助**）
  - [光在地球之時……](../Page/光在地球之時…….md "wikilink")（**式部帆夏**）
  - [魔法少女奈叶 The MOVIE 2nd A's 廣播劇CD
    Side-Y](../Page/魔法少女奈叶A's.md "wikilink")（莉莉·修特罗杰克）
  - [魔法少女奈叶INNOCENT Sound
    Stage](../Page/魔法少女奈葉_INNOCENT.md "wikilink")（**阿米蒂埃·弗洛利安**）
  - [天使1/2方程式](../Page/天使1/2方程式.md "wikilink")（杉本雛）

### 有聲小說

  - 女子大生会計士の事件簿（藤原萌實）

### 音樂CD

  - [神曲奏界](../Page/神曲奏界.md "wikilink") （角色歌曲集
    Meta-morphose）（**克緹卡兒蒂·阿巴·拉格蘭潔絲**）
  - [世界で一番ヤバイ恋](../Page/世界で一番ヤバイ恋.md "wikilink")（[狂亂家族日記片尾曲](../Page/狂亂家族日記.md "wikilink")）（乱崎千花）
  - [出包王女](../Page/出包王女.md "wikilink")（喜劇CD Vol.1）（**菈菈·薩塔琳·戴比路克**）
  - 魔法少女マジカルたん\!（萌英單語片尾曲）（黒威澄）
  - 萌英單語 電視原聲帶&迷你角色專輯（Magical Melody）（黒威澄）

**合唱歌曲**

  - 超元氣3姊妹1 片頭曲 「みっつ数えて大集合\!」（歌：三葉（高垣彩陽）、雙葉（明坂聰美）、一葉（戶松遙））
  - 超元氣3姊妹2 片頭曲 「わが名は小学生」（歌：三葉（高垣彩陽）、雙葉（明坂聰美）、一葉（戶松遙））

**角色歌曲**

  - 楚楚可憐少女組 角色CD さわってViolet（CV.戸松遥）
  - 明日的與一\! 角色CD Girl grows up\!～TSUN-DERE Ver.～（CV.戸松遥）
  - 浪漫追星社 角色CD Dive to the sky（CV.戸松遥）
  - 明日的與一\! 角色CD あたしはぜんぜん気にしてないっ（CV.戸松遥）
  - 猫神やおよろず 角色CD 極楽浄土へいらっしゃい（CV.戸松遥）
  - TV動畫 『超元氣3姊妹』 角色歌曲Vol.3 丸井一葉（CV.戸松遥）

### 遊戲

  - ARCRISE FANTASIA（セシル・ガルシア）
  - 鋼鐵新娘DS（**艾姆艾姆**）
  - サモンナイトグランテーゼ 滅びの剣と約束の騎士（ココ）
  - 楚楚可憐少女組DS（**三宮紫穗**）
  - 電撃学園RPG Cross of Venus（**水無神操緒**）
  - 出包王女系列（**菈菈·薩塔琳·戴比路克**）
      - 出包王女 令人期待\!\!森林學校篇（NDS）
      - 出包王女 臉紅心跳\!\!海濱學校篇（PSP）
  - [牧場物語 歡迎來到風之市集](../Page/牧場物語_歡迎來到風之市集.md "wikilink")（**エンジュ**、シギュン）
  - 萌單DS（黑威澄）(預約特典的DVD與演出貓君的檜山修之一同演出)
  - [偶像大師 深情之星](../Page/偶像大師_深情之星.md "wikilink")（**日高愛**）
  - [偶像大師 灰姑娘女孩](../Page/偶像大師_灰姑娘女孩.md "wikilink")（日高愛）
  - [武裝神姬BATTLE](../Page/武裝神姬.md "wikilink") RONDO（天使コマンド型 MMS ウェルクストラ）
  - KERORO RPG 騎士與武者與傳說的海賊（ミミクリ）
  - 鶺鴒女神 〜來自未來的禮物〜（来瀨、赤ちゃん）
  - [戰場女武神2](../Page/戰場女武神2.md "wikilink") 加立亞王立士官學校（エイリアス）
  - 東京鬼祓師 鴉乃杜學園奇譚（武藤いちる）
  - 闘真伝（ウィノ・マクガバン）
  - NUGA-CEL\!（**ピヨ**）
  - Battle Spirits 輝石の覇者（**蓮華**）
  - PHANTASY STAR Protable 2（チェルシー）
  - Marriage Royale Prism Story（美久）
  - [符文工廠3 -新牧場物語-](../Page/符文工廠3_-新牧場物語-.md "wikilink")（**イオン**）
  - [閃電十一人3 挑戰世界\!\!](../Page/閃電十一人3_挑戰世界!!.md "wikilink")（**久遠冬花**）
  - [紅魔城傳說 2
    幻妖之鎮魂歌](../Page/紅魔城傳說_2_幻妖之鎮魂歌.md "wikilink")（アリス・マーガトロイド、小野塚小町）
  - 闘真伝（ウィノ・マクガバン）
  - 魔法少女奈葉 A's 攜帶版：命運齒輪（**阿米蒂埃·弗洛利安**、莉莉·修特罗杰克）
  - 屍體派對-Book of Shadows（山本美月）
  - [刀剑神域 －无限瞬间－](../Page/刀剑神域_－无限瞬间－.md "wikilink")（**亞絲娜**）
  - [刀剑神域 －虚空断章－](../Page/刀剑神域_－虚空断章－.md "wikilink")（**亞絲娜**）
  - [刀剑神域 －Lost Song－](../Page/刀剑神域_－失落之歌－.md "wikilink")（**亞絲娜**）
  - [刀剑神域 －虛空幻界－](../Page/刀剑神域_－虛空幻界－.md "wikilink")（**亞絲娜**）
  - [零～濡鴉之巫女～](../Page/零～濡鴉之巫女～.md "wikilink")（**百百瀨春河**）
  - [狩龍戰紀](../Page/狩龍戰紀.md "wikilink")（煉金術師瑪蓮達、系統語音8）
  - [數碼寶貝物語 網路偵探](../Page/數碼寶貝物語_網路偵探.md "wikilink")（四之宫莉娜）
  - [J-Stars Victory
    VS](../Page/J-Stars_Victory_VS.md "wikilink")（菈菈·萨塔琳·戴比路克）
  - [女神異聞錄5](../Page/女神異聞錄5.md "wikilink")（**奧村春**）
  - [御靈錄otogi](../Page/御靈錄otogi.md "wikilink")（**靈**）
  - [Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink")（**源賴光**）
  - [少女前線](../Page/少女前線.md "wikilink")（**M4A1、李－恩菲爾德、WA2000**）
  - [陰陽師RPG](../Page/陰陽師RPG.md "wikilink")（鴆）
  - [閃耀幻想曲](../Page/閃耀幻想曲.md "wikilink")（高山春香、山口如月）

### 人偶劇

**2016年**

  - [Thunderbolt Fantasy
    東離劍遊紀](../Page/Thunderbolt_Fantasy_東離劍遊紀.md "wikilink")（獵魅）

### 寫真集

  - 『HARUKAs』（2009年10月9日）
  - 『High Touch\!』（2010年2月25日）
  - 『Rainbow Vacation』（2011年10月20日）
  - 『ハルカモード 2009-2012』（2012年12月25日）
  - 『ハルカモードSS』（2018年2月1日）
  - 『ハルカモードAW』（2018年2月1日）

### 其他

  - 電視動畫『[GA 藝術科美術設計班](../Page/GA_藝術科美術設計班.md "wikilink")』DVD CM（真人演出）

  - 電影『[我最最最討厭的學長](../Page/我最最最討厭的學長.md "wikilink")』（客串演出）

  - （CloudCore VPS萌化角色）\[13\]

## 唱片

### 單曲

以聲優團體發行的作品，詳見[sphere項目](../Page/sphere_\(聲優團體\)#唱片.md "wikilink")。

|       | 發售日         | 名稱                                                                            | 規格品號              | 最高位       | 備註                |
| ----- | ----------- | ----------------------------------------------------------------------------- | ----------------- | --------- | ----------------- |
| 初回限定盤 | 通常盤         | 動畫盤                                                                           |                   |           |                   |
| 1st   | 2008年9月3日   | **[naissance](../Page/naissance.md "wikilink")**                              | SMCL-148/SMCL-149 | SMCL-150  |                   |
| 2nd   | 2008年10月29日 | **[motto☆派手にね\!](../Page/motto☆派手にね!.md "wikilink")**                         | SMCL-155/SMCL-156 | SMCL-157  | SMCL-158（神薙盤）     |
| 3rd   | 2008年11月26日 | **[産巣日の時](../Page/產巢日之時.md "wikilink")**                                      | SMCL-159/SMCL-160 | SMCL-161  | SMCL-162（神薙盤）     |
| 4th   | 2009年5月13日  | **[こいのうた](../Page/戀之歌.md "wikilink")**                                        | SMCL-173/SMCL-174 | SMCL-175  |                   |
| 5th   | 2010年1月27日  | **[Girls, Be Ambitious.](../Page/Girls,_Be_Ambitious..md "wikilink")**        | SMCL-181/SMCL-182 | SMCL-183  |                   |
| 6th   | 2010年8月4日   | **[渚のSHOOTING STAR](../Page/渚之SHOOTING_STAR.md "wikilink")**                  | SMCL-195/SMCL-196 | SMCL-197  |                   |
| 7th   | 2010年11月3日  | **[Baby Baby Love](../Page/Baby_Baby_Love.md "wikilink")**                    | SMCL-216/SMCL-217 | SMCL-218  |                   |
| 8th   | 2011年7月13日  | **[Oh My God♥](../Page/Oh_My_God♥.md "wikilink")**                            | SMCL-241/SMCL-242 | SMCL-243  |                   |
| 9th   | 2012年7月25日  | **[ユメセカイ](../Page/夢世界.md "wikilink")**                                        | SMCL-273/SMCL-274 | SMCL-275  | SMCL-276（SAO盤）    |
| 10th  | 2012年10月17日 | **[Q\&A リサイタル\!](../Page/Q&A_リサイタル!.md "wikilink")**                          | SMCL-281/SMCL-282 | SMCL-283  |                   |
| 11th  | 2013年7月10日  | **[PACHI PACHI PARTY](http://www.tomatsuharuka.com/disco/archive/?SMCL-300)** | SMCL-300/SMCL-301 | SMCL-302  |                   |
| 12th  | 2014年1月15日  | **[ヒカリギフト](http://www.tomatsuharuka.com/disco/archive/?SMCL-317)**            | SMCL-317/SMCL-318 | SMCL-319  |                   |
| 13th  | 2014年7月30日  | '''\[\[Fantastic_Soda                                                        | |Fantastic Soda   | \]\]'''   | SMCL-337/SMCL-338 |
| 14th  | 2014年12月3日  | **[courage](../Page/courage.md "wikilink")**                                  | SMCL-367/SMCL-368 | SMCL-369  |                   |
| 15th  | 2015年9月30日  | **[STEP A GO\!GO\!](../Page/STEP_A_GO!GO!.md "wikilink")**                    | SMCL-403/SMCL-404 | SMCL-405  |                   |
| 16th  | 2016年2月17日  | **[シンデレラ☆シンフォニー](http://www.tomatsuharuka.com/disco/archive/?SMCL-415)**      | SMCL-415/SMCL-416 | SMCL-417  |                   |
| 17th  | 2016年10月26日 | **[モノクロ/Two of us](http://www.tomatsuharuka.com/disco/archive/?SMCL-449)**    | SMCL-449/SMCL-450 | SMCL-451  | SMCL-452(ゲーム盤)    |
| 18th  | 2017年10月11日 | **[有頂天トラベラー](../Page/有頂天Traveller.md "wikilink")**                            | SMCL-505/SMCL-506 | SMCL-507  | SMCL-508【期間生産限定盤】 |
| 19th  | 2018年9月5日   | **[TRY & JOY](../Page/TRY_&_JOY.md "wikilink")**                              | SMCL-557/SMCL-558 | SMCL-5559 |                   |

### 專輯

|       | 發售日        | 名稱                                                                        | 規格品號              | [Oricon](../Page/:ja:オリコンチャート.md "wikilink") 最高位 |
| ----- | ---------- | ------------------------------------------------------------------------- | ----------------- | ------------------------------------------------ |
| 初回限定盤 | 通常盤        |                                                                           |                   |                                                  |
| 1st   | 2010年2月24日 | **[Rainbow Road](../Page/Rainbow_Road.md "wikilink")**                    | SMCL-189/SMCL-190 | SMCL-191                                         |
| 2nd   | 2013年1月16日 | **[Sunny Side Story](../Page/Sunny_Side_Story.md "wikilink")**            | SMCL-288/SMCL-289 | SMCL-290                                         |
| 3rd   | 2015年3月18日 | **[Harukarisk\*Land](../Page/Harukarisk*Land.md "wikilink")**             | SMCL-375/SMCL-376 | SMCL-377                                         |
| 4th   | 2018年5月2日  | **[COLORFUL GIFT](http://www.tomatsuharuka.com/disco/archive/?SMCL-536)** | SMCL-536/SMCL-537 | SMCL-538                                         |

### 最佳專輯

|            | 發售日                                                                                        | 名稱                                                                                        | 規格品號              | [Oricon](../Page/:ja:オリコンチャート.md "wikilink") 最高位 |
| ---------- | ------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------- | ----------------- | ------------------------------------------------ |
| 初回限定盤      | 通常盤                                                                                        |                                                                                           |                   |                                                  |
| 1th        | 2016年6月15日                                                                                 | **[戸松遥 BEST SELECTION -sunshine-](http://www.tomatsuharuka.com/disco/archive/?SMCL-430)** | SMCL-430/SMCL-431 | SMCL-432                                         |
| 2016年6月15日 | **[戸松遥 BEST SELECTION -starlight-](http://www.tomatsuharuka.com/disco/archive/?SMCL-433)** | SMCL-433/SMCL-434                                                                         | SMCL-435          | 13位                                              |

### LIVE影像作品

|             | 發售日         | 名稱                                                                                                        | 規格品號      |
| ----------- | ----------- | --------------------------------------------------------------------------------------------------------- | --------- |
| **Blu-ray** | **DVD**     |                                                                                                           |           |
| 1st         | 2012年2月22日  | **[戸松遥 first live tour 2011「オレンジ☆ロード」](http://www.tomatsuharuka.com/disco/archive/?SMXL-2)**              | SMXL-2    |
| 2nd         | 2013年10月30日 | **[戸松遥 second live tour「Sunny Side Stage\!」](http://www.tomatsuharuka.com/disco/archive/?SMXL-5)**        | SMXL-5    |
| 3rd         | 2016年2月17日  | '''\[<http://www.tomatsuharuka.com/disco/archive/?SMXL-9> 戸松遥 3rd Live Tour 2015「Welcome\!Harukarisk＊Land | \!」\] ''' |
| 4rd         | 2017年8月9日   | **[戸松遥 BEST LIVE TOUR 2016〜SunQ&ホシセカイ〜](http://www.tomatsuharuka.com/disco/archive/?SMXL-13)**            | SMXL-12   |

### 精選音樂剪輯

|     | 發售日        | 名稱                                                                                          | 規格品號    |
| --- | ---------- | ------------------------------------------------------------------------------------------- | ------- |
| 1st | 2016年8月10日 | **[HARUKA TOMATSU Music Clips step1](http://www.tomatsuharuka.com/disco/archive/?SMXL-10)** | SMXL-10 |

## 註腳

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [官方网站](http://www.tomatsuharuka.com/)
  - [经纪公司下属网页](http://musicrayn.com/tomatsu/tomatsu_top.html)
      - [](http://www.musicrayn.com/tomatsu/tomatsu_myroom.html)
  - [官方Blog ](http://ameblo.jp/tomatsuharuka-blog/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:愛知縣出身人物](../Category/愛知縣出身人物.md "wikilink")
[Category:聲優獎新人女優獎得主](../Category/聲優獎新人女優獎得主.md "wikilink")
[Category:聲優獎助演女優獎得主](../Category/聲優獎助演女優獎得主.md "wikilink")
[Category:Sphere](../Category/Sphere.md "wikilink")
[Category:戶松遙](../Category/戶松遙.md "wikilink") [Category:Music
Ray'n所屬藝人](../Category/Music_Ray'n所屬藝人.md "wikilink")
[Category:日本索尼音樂娛樂旗下藝人](../Category/日本索尼音樂娛樂旗下藝人.md "wikilink")
[Category:日本女性流行音樂歌手](../Category/日本女性流行音樂歌手.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")

1.
2.
3.
4.  出自《神曲奏界》官方網頁的「」專欄。
5.  TBS動畫節（TBS Anime Festa）'2007上發表
6.  發表於本人的部落格
7.  <https://ameblo.jp/tomatsuharuka-blog/entry-12432210350.html>
8.  資料來自《FRIDAY》2011年5月20日號
9.
10.
11.
12.
13. [2012四月馬鹿 - CloudCore VPS](http://www.cloudcore.jp/vps/kumonocoa2012/)