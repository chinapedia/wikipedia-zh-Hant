[房兆楹.jpg](https://zh.wikipedia.org/wiki/File:房兆楹.jpg "fig:房兆楹.jpg")
**房兆楹**（），出生於天津，山东泰安人。[歷史學家](../Page/歷史學家.md "wikilink")，[明](../Page/明朝.md "wikilink")、[清史權威学者](../Page/清朝.md "wikilink")，與燕京校友、同事[杜聯喆女士結婚](../Page/杜聯喆.md "wikilink")，夫婦是國際知名的中國史專家和文獻學家，尤精於明清史及近代人物研究。

房兆楹、杜聯喆夫婦在中國、美國、澳大利亞等地的大學和圖書館工作，參與撰寫《清代名人傳略》、《中華民國人物傳記辭典》、《明代名人錄》。這三部著作是明清以降中國著名人物傳記資料的大彙編，均用英文編寫，在國際漢學界有很大影響。一九七六年*Dictionary
of Ming Biography,
1368-1644*（中译為《明代名人錄》）出版時，哥倫比亞大學特以極隆重之儀式贈夫婦二位荣誉文學博士學位。

房兆楹1928年畢業於[燕京大學數學系](../Page/燕京大學.md "wikilink")，獲理學士學位。1930年從武昌文華圖書館學專科學校畢業，任燕京大學圖書館助理館長。後官費留美。\[1\]

[二战期间](../Page/二战.md "wikilink")，房兆楹教導美国军人学习汉语，並编写汉语口语教材，後列入美国“陆军部教育手册”（War
Department Education
Manual）。1948年撰写《清初满洲家庭里的分家子和未分家子》一文。後参加[华盛顿大学](../Page/华盛顿大学.md "wikilink")、哥伦比亚大学共同主持的“中国历史计划”（Chinese
History Project）。1965年参加美国哥伦比亚大学“明代传记历史计划”（Ming Biographical History
Project）。晚年把大量图书捐赠给臺灣[中央研究院近代史研究所图书馆和](../Page/中央研究院.md "wikilink")[澳大利亚国家图书馆](../Page/澳大利亚国家图书馆.md "wikilink")。

史學家[史景遷](../Page/史景遷.md "wikilink")（Jonathan D.
Spence）與其夫人[金安平](../Page/金安平.md "wikilink")、[芮玛丽](../Page/芮玛丽.md "wikilink")（Mary
Wright）、[倪德卫](../Page/倪德卫.md "wikilink")（David S. Nivison）都是他的弟子。

## 著作

  - 《三十三種清代傳記綜合引得》（1932年與[杜聯喆合編](../Page/杜聯喆.md "wikilink")）
  - *Eminent Chinese of the Ch'ing Period
    (1644–1912)*\[《清代名人传略》\]（1944年）
  - 《中华民国人物传记辞典》
  - *Dictionary of Ming Biography, 1368-1644*《明朝人名录》（1976年）

## 評價

  - [杨联陞](../Page/杨联陞.md "wikilink")：“論明清史料史事,今日當推房兆楹、杜連喆夫婦。”\[2\]
  - [何炳棣說房兆楹一向專捧](../Page/何炳棣.md "wikilink")“洋人”學者，對華裔學者評按照例非常刻薄。華人中他只捧兩個人，一位是前輩[胡適](../Page/胡適.md "wikilink")，一位是同輩的[楊聯陞](../Page/楊聯陞.md "wikilink")。又按照满洲入关前的旧惯俗，财产及其他都由仍与父母同居的儿子（未分家子）承继，[雍正业已封为雍亲王](../Page/雍正帝.md "wikilink")、自建雍邸，已是分家子，而[康熙钟爱的](../Page/康熙帝.md "wikilink")[第十四子是未分家子](../Page/允禵.md "wikilink")，《清代名人传略》因此結論雍正奪嫡。何炳棣則批評房只知其一，不知其二，對於雍正奪嫡的結論是錯誤的，因康熙在1675年立第二子[胤扔为皇太子時](../Page/允礽.md "wikilink")，就已经放弃满洲旧俗。\[3\]

## 参考文献

  - 齐鲁文化大辞典
  - 20世纪中华人物名字号辞典

## 外部链接

  - [史學家房兆楹夫婦獲哥大榮譽博士學位。拍攝時間：1976/2/28](https://archive.is/20130713033245/http://nrch.cca.gov.tw/ccahome/photo/photo_meta.jsp?xml_id=0000794144&collectionname=%E8%8F%AF%E8%97%9D%E8%80%81%E7%85%A7%E7%89%87)
  - [房兆楹先生和他的学术研究](https://web.archive.org/web/20070102061517/http://xiangyata.net/data/articles/b03/641.html)

[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[Category:美国汉学家](../Category/美国汉学家.md "wikilink")
[Category:燕京大學校友](../Category/燕京大學校友.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:泰安人](../Category/泰安人.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Z兆](../Category/房姓.md "wikilink")

1.
2.  1984年6月7日写给[周一良教授的信](../Page/周一良.md "wikilink")。收入周一良《毕竟是书生》〈纪念[楊聯陞教授](../Page/楊聯陞.md "wikilink")〉
3.