**扇蟹總科**（学名），是[短尾次目](../Page/短尾次目.md "wikilink")([螃蟹](../Page/螃蟹.md "wikilink"))的其中一個總科。扇蟹總科過往曾經有過11個科，分為130個屬及超過一千個不同的品種\[1\]。
根據2009年的分類，現時除了扇蟹科、[Panopeidae及](../Page/Panopeidae.md "wikilink")[Pseudorhombilidae以外](../Page/Pseudorhombilidae.md "wikilink")，其他各科均已獨立離開\[2\]\[3\]，成為了\[4\]：

  - [瓢蟹總科](../Page/瓢蟹總科.md "wikilink") Carpilioidea
  - [酉婦蟹總科](../Page/酉婦蟹總科.md "wikilink") Eriphioidea
  - [長腳蟹總科](../Page/長腳蟹總科.md "wikilink") Goneplacoidea
  - [六足蟹總科](../Page/六足蟹總科.md "wikilink") Hexapodoidea
  - [毛刺蟹總科](../Page/毛刺蟹總科.md "wikilink") Pilumnoidea
  - [梯形蟹總科](../Page/梯形蟹總科.md "wikilink") Trapezioidea

雖然如此，但只有三個科的扇蟹總科依然是物種最豐富的總科\[5\]。

## 參考

## 外部連結

  -
[扇蟹總科](../Category/扇蟹總科.md "wikilink")

1.

2.

3.

4.
5.