**手印**（），又叫**印相**、**印**、**印契**、**密印**、**契印**，為[印度教及](../Page/印度教.md "wikilink")[佛教術語](../Page/佛教.md "wikilink")，係以兩手擺成特定的姿勢，用來象徵特定的教義或理念，或在參與宗教儀式時發生某種形而上的作用。

## 概述

佛教一般可列出36種主要手印，12種單手手印和24種雙手手印。佛教[金剛乘中](../Page/金剛乘.md "wikilink")，一般可以看到12種主要手印。日本佛教列出八大手印：[法輪印](../Page/法輪.md "wikilink")、施法印、[金剛印](../Page/金剛.md "wikilink")、金剛界印、[金剛薩埵所結手印](../Page/金剛薩埵.md "wikilink")、[無量光佛所結手印](../Page/無量光佛.md "wikilink")。其中，釋迦牟尼佛的“釋迦五印”爲佛教三大流派共通的手印。

[印度教](../Page/印度教.md "wikilink")[密宗和](../Page/印度教密宗.md "wikilink")[瑜伽派共列出](../Page/瑜伽派.md "wikilink")108种不同的手印，其中常用的有54种。

## 釋迦五印

[釋迦牟尼佛的五種手印為佛教各派共同認可](../Page/釋迦牟尼佛.md "wikilink")，其意義也大致相同，只是在各個宗派之間會有一些細微的區別。

### 禪定印

[COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013975.jpg](https://zh.wikipedia.org/wiki/File:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013975.jpg "fig:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013975.jpg")

禪定印（）是佛陀入於禪定時所結的手印，或可稱為定印、法界定印。如果只有左手，也可稱為定印、等持印。持此印者有[阿彌陀佛](../Page/阿彌陀佛.md "wikilink")（無量壽佛）、[药师佛](../Page/药师佛.md "wikilink")、[阿逸多](../Page/阿逸多.md "wikilink")[尊者等](../Page/尊者.md "wikilink")。

  - 姿势
    雙手平伸，手心向上，通常右手在上，两拇指指端相拄，交互疊放於兩腿間。

  - 示例

<File:Kamakura> Budda Daibutsu front
1885.jpg|[日本](../Page/日本.md "wikilink")[高德院的镰仓大佛](../Page/高德院.md "wikilink")
<File:BuddhaSothorn.jpg>|[泰国的](../Page/泰国.md "wikilink")[索通佛](../Page/索通佛.md "wikilink")

### 說法印

[COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60019836.jpg](https://zh.wikipedia.org/wiki/File:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60019836.jpg "fig:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60019836.jpg")

說法印（），又名「轉法輪印」，持此印者有[釋迦文佛](../Page/釋迦文佛.md "wikilink")、[彌勒尊佛等](../Page/彌勒尊佛.md "wikilink")。

  - 示例

<File:Maitreya> Statue.jpg|藏传[弥勒菩萨塑像](../Page/弥勒菩萨.md "wikilink")
<File:Buddha> in Sarnath Museum (Dhammajak
Mutra).jpg|[印度](../Page/印度.md "wikilink")[鹿野苑](../Page/鹿野苑.md "wikilink")[释迦牟尼佛像](../Page/释迦牟尼.md "wikilink")

### 觸地印

[COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013973.jpg](https://zh.wikipedia.org/wiki/File:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013973.jpg "fig:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_TMnr_60013973.jpg")

**觸地印**（，或）又稱**[證悟印](../Page/證悟.md "wikilink")**或**降魔印**。[釋迦牟尼佛在證悟時](../Page/釋迦牟尼佛.md "wikilink")，受魔王波旬干擾，魔王波旬提問：「誰可為你做證你已經開悟？」
釋迦牟尼佛即以右手觸地並說：「大地可以為我作證！」 隨即堅牢地神出現為釋迦牟尼佛證明他已經成就佛道，魔王才退去。

持此印者有[釋迦牟尼](../Page/釋迦牟尼.md "wikilink")、[阿閦如來等](../Page/阿閦如來.md "wikilink")。

  - 姿势
    右手自然下垂，手心向內，置於右膝蓋與小腿腹之間，中指觸地。

  - 示例

<File:2013> Phra Buddha Chinnarat
01.jpg|泰国的[成功佛](../Page/成功佛.md "wikilink")
<File:Akshobhya.jpg>|[阿閦佛唐卡](../Page/阿閦佛.md "wikilink")

### 施無畏印

[COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_voorstellende_Dhyani_Boeddha_Amogasiddha_TMnr_10026907.jpg](https://zh.wikipedia.org/wiki/File:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_voorstellende_Dhyani_Boeddha_Amogasiddha_TMnr_10026907.jpg "fig:COLLECTIE_TROPENMUSEUM_Boeddhabeeld_van_de_Borobudur_voorstellende_Dhyani_Boeddha_Amogasiddha_TMnr_10026907.jpg")
 意为“全然无惧的人”或是“令人安心，平静者”。 据说能使众生心安，无所畏怖，所以称为施无畏。 屈手上举于胸前，手指自然舒展，手掌向外。
这一手印表示了佛为救济众生的大慈心愿。

  - 示例

<File:LinShanDaFo.jpg>|[灵山大佛](../Page/灵山大佛.md "wikilink") <File:Bangkok>
Vesak - 2017-05-07
(026).jpg|[老挝国宝](../Page/老挝.md "wikilink")[勃拉邦佛](../Page/勃拉邦佛.md "wikilink")
<File:Big> Buddha
Lantau.JPG|[香港](../Page/香港.md "wikilink")[天坛大佛](../Page/天坛大佛.md "wikilink")
<File:普陀山南海观音立像.jpg>|[普陀山南海观音](../Page/普陀山.md "wikilink")

### 与願印

[Wat_Tham_Khao_Rup_Chang_-_107_80_sa_po_he_(detail)_(14685598713).jpg](https://zh.wikipedia.org/wiki/File:Wat_Tham_Khao_Rup_Chang_-_107_80_sa_po_he_\(detail\)_\(14685598713\).jpg "fig:Wat_Tham_Khao_Rup_Chang_-_107_80_sa_po_he_(detail)_(14685598713).jpg")

**与願印**（）又稱**施予印**、**施願印**或**佈施印**。持此印者有[藥師佛](../Page/藥師佛.md "wikilink")、[觀音](../Page/觀音.md "wikilink")、[綠度母](../Page/綠度母.md "wikilink")、[白度母等](../Page/白度母.md "wikilink")。

  - 含义
    表示以普度眾生的慈悲心施與，給予眾生所需，滿足眾生所願的意思。

  - 姿勢
    右手自然平伸下垂，手心向上。

  - 示例

<File:Khasarpana>
Lokesvara.jpg|來自[那爛陀寺的](../Page/那爛陀寺.md "wikilink")[波羅王朝公元](../Page/波羅王朝.md "wikilink")9世紀的觀音菩薩像，現藏於印度[新德里國家博物館](../Page/新德里國家博物館.md "wikilink")。
<File:Amitabha.png>|[阿弥陀佛](../Page/阿弥陀佛.md "wikilink")

## 其他手印

### 皈依印

皈依印（）又稱施依印、護法印或三寶印。持此印者有菩薩、觀音、度母等。

  - 姿势
    左手拇指的指尖輕觸小指以外任一指的指尖，其它三指自然抬起。

  - 含义
    翘起的三指代表皈依佛、法、僧[三寶](../Page/三宝_\(佛教\).md "wikilink")。

  - 示例

[File:大势至.png|左手结皈依印的](File:大势至.png%7C左手结皈依印的)[大势至菩萨像](../Page/大势至菩萨.md "wikilink")
<File:九天禅院>
文殊菩萨.jpg|[汕头市](../Page/汕头市.md "wikilink")[天坛花园九天禅院的](../Page/天坛花园.md "wikilink")[文殊菩萨像](../Page/文殊菩萨.md "wikilink")
<File:塘埔观音像>
(cropped).jpg|[揭阳市](../Page/揭阳市.md "wikilink")[塘埔观音像](../Page/塘埔观音像.md "wikilink")

### 殊勝三界印

殊勝三界印（）

  - 示例

<File:Vajradhara7.jpg>|[金刚总持唐卡](../Page/金刚总持.md "wikilink")

### 智拳印

以兩手分別作金剛拳（以四指握拇指於掌中，稱為金剛拳），再以右拳握左手食指於胸前，據說此印相表示消滅無明煩惱，能得佛智慧。

### 期剋印

[Wat_Tham_Khao_Rup_Chang_-_092_67_sa_po_he_(detail)_(14479201039).jpg](https://zh.wikipedia.org/wiki/File:Wat_Tham_Khao_Rup_Chang_-_092_67_sa_po_he_\(detail\)_\(14479201039\).jpg "fig:Wat_Tham_Khao_Rup_Chang_-_092_67_sa_po_he_(detail)_(14479201039).jpg")
**期剋印**（） 佛顶尊胜佛母身面白色，三面：中白、右黄、左蓝，八臂：右上手持十字杵、当胸，余右手：无量光佛、箭、布施印，左上
手**期克印**
、当胸持索，余左手：无畏印、弓、盈满甘露之宝瓶，黑发、半髻半旒（liu音流，帝王礼帽上前后悬垂的玉串）、五佛宝冠、身披羂衣，跏趺坐光蕴中。

### 合十

[Wat_Tham_Khao_Rup_Chang_-_041_28_du_lu_du_lu_fa_she_ye_di_(detail)_(14666386655).jpg](https://zh.wikipedia.org/wiki/File:Wat_Tham_Khao_Rup_Chang_-_041_28_du_lu_du_lu_fa_she_ye_di_\(detail\)_\(14666386655\).jpg "fig:Wat_Tham_Khao_Rup_Chang_-_041_28_du_lu_du_lu_fa_she_ye_di_(detail)_(14666386655).jpg")

合十（Anjali）可算是日常生活中最常見的手印，出家人打招呼時常做此印，姿勢是雙手當胸，十指自然舒展相合，手心中央形成空穴，如捧物狀。除了表示禮敬、虔誠外，也隱含著證悟空性、相印、無二無別。

持此印者有[千手觀音](../Page/千手觀音.md "wikilink")、[四臂觀音](../Page/四臂觀音.md "wikilink")、[龍王等](../Page/龍王.md "wikilink")。

  - 示例

[File:塔顶千手观音.jpg|汕头市](File:塔顶千手观音.jpg%7C汕头市)[天坛花园九天玄女宝塔塔顶的](../Page/天坛花园.md "wikilink")[千手观音像](../Page/千手千眼观世音菩萨.md "wikilink")
<File:Mya> Nan Mwe
Statue.jpg|[缅甸](../Page/缅甸.md "wikilink")[仰光](../Page/仰光.md "wikilink")的[许愿女神像](../Page/嘛南維.md "wikilink")
<File:Apsornsiha> at Phra Meru of HRH Princess Galyani
Vadhana.JPG|[甘拉亚妮·瓦塔娜皇家火化台的Apsornsiha](../Page/甘拉亚妮·瓦塔娜.md "wikilink")（อัปสรสีหะ，泰国神话中半人半狮的生物）塑像

### 事業手印

在金剛乘中，以[事業手印](../Page/事業手印.md "wikilink")（儀式化的男女[性行為](../Page/性行為.md "wikilink")）進行[雙身法修行](../Page/雙身法.md "wikilink")。透過觀想，而不進行實際性行為的修行方法，稱為[智慧手印](../Page/智慧手印.md "wikilink")。

## 參考資料

  - [略述佛教中的手印](https://web.archive.org/web/20101217072829/http://hkbuddhist.org/magazine/592/592_11.html)。《香港佛教》。
  - [手印書籤](https://web.archive.org/web/20101223041944/http://garuda.tw/book/dm/shucian/sh-1.html)。台北：卡廬。
  - 全佛編輯部編（2000）。[佛教的手印](https://web.archive.org/web/20101218133704/http://www.buddhall.com/BookList/Book.aspx?BookID=123)。台北：全佛。ISBN
    957-8254-84-9。
  - 全佛編輯部編（2000）。[密教的修法手印上：十八道法．金剛界法](https://web.archive.org/web/20101218133722/http://www.buddhall.com/BookList/Book.aspx?BookID=124)。台北：全佛。ISBN
    957-8254-85-7。
  - 全佛編輯部編（2000）。[密教的修法手印下：胎藏界法．護摩法](https://web.archive.org/web/20101218133741/http://www.buddhall.com/BookList/Book.aspx?BookID=125)。台北：全佛。ISBN
    957-8254-86-5。
  - 弘學編著（2003）。佛教諸尊手印。成都：巴蜀書社。ISBN 7-80659-515-5。

[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:印度教](../Category/印度教.md "wikilink")
[Category:手勢](../Category/手勢.md "wikilink")