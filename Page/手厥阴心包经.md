[Acupuncture_chart_300px.jpg](https://zh.wikipedia.org/wiki/File:Acupuncture_chart_300px.jpg "fig:Acupuncture_chart_300px.jpg")

**手厥陰心包經**（）是一條[經脈](../Page/經脈.md "wikilink")，[十二正经之一](../Page/十二正经.md "wikilink")，与[手少陽三焦經相表里](../Page/手少陽三焦經.md "wikilink")。本經起於[天池](../Page/天池穴.md "wikilink")，止於[中衝](../Page/中衝穴.md "wikilink")，左右各9個[腧穴](../Page/腧穴.md "wikilink")。

## 经脉循行

起于胸中，出属[心包络](../Page/心包络.md "wikilink")，向下通过横隔，从胸至腹，依次联络上、中、下[三焦](../Page/三焦.md "wikilink")。

胸部支脉：沿着胸中，出于胁部，至腋下3寸处（[天池](../Page/天池穴.md "wikilink")），上行抵腋窝中，沿上臂内侧正中，行于[手太阴和](../Page/手太阴肺经.md "wikilink")[手少阴之间](../Page/手少阴心经.md "wikilink")，进入肘窝中，向下行于前臂掌长肌腱与桡侧腕屈肌腱之间，进入掌中，沿着中指到指端（[中冲](../Page/中冲.md "wikilink")）。

掌中支脉：从[劳宫分出](../Page/劳宫.md "wikilink")，沿着无名指[尺側到指端](../Page/尺骨.md "wikilink")，与[手少阳三焦经相接](../Page/手少阳三焦经.md "wikilink")。\[1\]\[2\]

## 主治概要

本经主要治疗心、胸、胃、神志病证。如心痛、心悸、胃痛、呕吐、胸痛、癫狂、昏迷及经脉循行部位的病变。

## 腧穴

1.  [天池](../Page/天池穴.md "wikilink")
2.  [天泉](../Page/天泉穴.md "wikilink")
3.  [曲澤](../Page/曲澤穴.md "wikilink")，[合穴](../Page/合穴.md "wikilink")
4.  [郄門](../Page/郄門穴.md "wikilink")，[郄穴](../Page/郄穴.md "wikilink")
5.  [間使](../Page/間使穴.md "wikilink")，[經穴](../Page/經穴.md "wikilink")
6.  [內關](../Page/內關穴.md "wikilink")，[絡穴](../Page/絡穴.md "wikilink")，[八脈交會穴](../Page/八脈交會穴.md "wikilink")，通[阴维脉](../Page/阴维脉.md "wikilink")
7.  [大陵](../Page/大陵穴.md "wikilink")，[輸穴](../Page/輸穴.md "wikilink")，[原穴](../Page/原穴.md "wikilink")
8.  [勞宮](../Page/勞宮穴.md "wikilink")，[滎穴](../Page/滎穴.md "wikilink")
9.  [中衝](../Page/中衝穴.md "wikilink")，[井穴](../Page/井穴.md "wikilink")

## 参考文献

[Category:十二正經](../Category/十二正經.md "wikilink")

1.
2.  《[靈樞](../Page/靈樞.md "wikilink")·經脈》：心主手厥陰心包絡之脈，起於胸中，出屬心包絡，下膈，厲絡三焦。其支者，循胸出胁，下腋三寸，上抵腋下，循臑內，行太陰、少陰之間，入肘中，下臂，行兩筋之間，入掌中，循中指，出其端。其支者，別掌中，循小指次指出其端。