**手太陽小腸經**（）是一條[經脈](../Page/經脈.md "wikilink")，[十二正经之一](../Page/十二正经.md "wikilink")，与[手少陰心經相表里](../Page/手少陰心經.md "wikilink")。本經起於[少澤](../Page/少澤穴.md "wikilink")，止於[聽宮](../Page/聽宮穴.md "wikilink")，左右各19個[腧穴](../Page/腧穴.md "wikilink")，其中8穴分佈於上肢背面的尺側，
11穴在[肩](../Page/肩.md "wikilink")、[頸](../Page/頸.md "wikilink")、[面部](../Page/面部.md "wikilink")。

## 经脉循行

從小指外側末端開始（[少澤](../Page/少澤.md "wikilink")），沿手掌尺側（[前谷](../Page/前谷.md "wikilink")、[後溪](../Page/後溪.md "wikilink")），上向腕部（[腕骨](../Page/腕骨.md "wikilink")、[陽谷](../Page/陽谷.md "wikilink")），出[尺骨小頭部](../Page/尺骨.md "wikilink")（[養老](../Page/養老_\(日本年號\).md "wikilink")），直上沿尺骨下邊（[支正](../Page/支正.md "wikilink")），出於肘內側當[肱骨內上髁和尺骨鷹嘴之間](../Page/肱骨.md "wikilink")（[小海](../Page/小海.md "wikilink")），向上沿上臂外後側，出肩關節部（[肩貞](../Page/肩貞.md "wikilink")、[臑俞](../Page/臑俞.md "wikilink")），繞[肩胛](../Page/肩胛.md "wikilink")（[天宗](../Page/天宗.md "wikilink")、[秉風](../Page/秉風.md "wikilink")、[曲垣](../Page/曲垣.md "wikilink")），交會肩上（[肩外俞](../Page/肩外俞.md "wikilink")、[肩中俞](../Page/肩中俞.md "wikilink");
會[附分](../Page/附分.md "wikilink")、[大杼](../Page/大杼.md "wikilink")、[大椎](../Page/大椎.md "wikilink")），進入[缺盆](../Page/缺盆.md "wikilink")（[鎖骨上窩](../Page/鎖骨.md "wikilink")），絡於心，沿食管，通過膈肌，到[胃](../Page/胃.md "wikilink")（會[上脘](../Page/上脘.md "wikilink")、[中脘](../Page/中脘.md "wikilink")），屬於[小腸](../Page/小腸.md "wikilink")。

缺盆部支脉：從鎖骨上行沿[頸旁](../Page/頸.md "wikilink")（[天窗](../Page/天窗.md "wikilink")、[天容](../Page/天容.md "wikilink")），上向[面頰](../Page/面頰.md "wikilink")（[顴髎](../Page/顴髎.md "wikilink")），到外眼角（會[瞳子髎](../Page/瞳子髎.md "wikilink")），彎向後（會[禾髎](../Page/禾髎.md "wikilink")），進入耳中（[聽宮](../Page/聽宮.md "wikilink")）。

颊部支脉：從面頰部分出，上向[顴骨](../Page/顴骨.md "wikilink")，靠[鼻旁到內眼角](../Page/鼻.md "wikilink")（會[睛明](../Page/睛明.md "wikilink")），接[足太陽膀胱經](../Page/足太陽膀胱經.md "wikilink")。
此外，小腸與[足陽明胃經的下](../Page/足陽明胃經.md "wikilink")[巨虛脈氣相通](../Page/巨虛.md "wikilink")。\[1\]\[2\]

## 主治概要

主治[腹部](../Page/腹部.md "wikilink")[小腸與](../Page/小腸.md "wikilink")[胸](../Page/胸.md "wikilink")、心、[咽喉病症](../Page/咽喉.md "wikilink")，某些[熱性病症](../Page/熱性.md "wikilink")，[神經方面病症和](../Page/神經.md "wikilink")[頭](../Page/頭.md "wikilink")、[面](../Page/面.md "wikilink")、[頸](../Page/頸.md "wikilink")、[眼](../Page/眼.md "wikilink")、[耳病症以及本經脈所經過部位之病症](../Page/耳.md "wikilink")。患者主要症狀在頭部兩側及耳部，有[癲癇](../Page/癲癇.md "wikilink")、[痙攣](../Page/痙攣.md "wikilink")、[喉間痛](../Page/喉間痛.md "wikilink")、[下頰腫](../Page/下頰腫.md "wikilink")、[肩臑痛](../Page/肩臑痛.md "wikilink")、[耳聾](../Page/耳聾.md "wikilink")、[目黃等](../Page/目黃.md "wikilink")。

## 腧穴

1.  [少澤](../Page/少澤穴.md "wikilink")，[井穴](../Page/井穴.md "wikilink")
2.  [前谷](../Page/前谷穴.md "wikilink")，[滎穴](../Page/滎穴.md "wikilink")
3.  [後谿](../Page/後谿穴.md "wikilink")，[輸穴](../Page/輸穴.md "wikilink")，[八脈交會穴](../Page/八脈交會穴.md "wikilink")，通於[督脈](../Page/督脈.md "wikilink")
4.  [腕骨](../Page/腕骨穴.md "wikilink")，[原穴](../Page/原穴.md "wikilink")
5.  [陽谷](../Page/陽谷穴.md "wikilink")，[經穴](../Page/經穴.md "wikilink")
6.  [養老](../Page/養老穴.md "wikilink")，[郄穴](../Page/郄穴.md "wikilink")
7.  [支正](../Page/支正穴.md "wikilink")，[絡穴](../Page/絡穴.md "wikilink")
8.  [小海](../Page/小海穴.md "wikilink")，[合穴](../Page/合穴.md "wikilink")
9.  [肩貞](../Page/肩貞穴.md "wikilink")
10. [臑俞](../Page/臑俞穴.md "wikilink")
11. [天宗](../Page/天宗穴.md "wikilink")
12. [秉風](../Page/秉風穴.md "wikilink")
13. [曲垣](../Page/曲垣穴.md "wikilink")
14. [肩外俞](../Page/肩外俞穴.md "wikilink")
15. [肩中俞](../Page/肩中俞穴.md "wikilink")
16. [天窗](../Page/天窗穴.md "wikilink")
17. [天容](../Page/天容穴.md "wikilink")
18. [顴髎](../Page/顴髎穴.md "wikilink")
19. [聽宮](../Page/聽宮穴.md "wikilink")

## 参考文献

[Category:十二正經](../Category/十二正經.md "wikilink")

1.
2.  《[靈樞](../Page/靈樞.md "wikilink")·經脈》：小腸手太陽之脈，起於小指之端，循手外側上腕，出踝中，直上循臂骨下廉，出肘內側兩骨之間，上循臑外後廉出肩解，繞肩胛，交肩上，入缺盆，絡心，循咽下膈，抵胃，屬小腸。其支者：從缺盆循頸，上頰，至目銳眦，卻入耳中。其支者：別頰上　抵鼻，至目內眦（斜絡於顴）。