[High_power_torch.jpg](https://zh.wikipedia.org/wiki/File:High_power_torch.jpg "fig:High_power_torch.jpg")

**手電筒**或稱**手燈筒**、**手電燈筒**、**電筒**或**電棒**（[美式英文](../Page/美式英文.md "wikilink")：Flashlight；[英式英文](../Page/英式英文.md "wikilink")：Torch），是一種手持式電子照明工具。

## 總論

一個典型的手電筒有一個經由[電池供電的](../Page/電池.md "wikilink")[燈泡和聚焦反射鏡](../Page/燈泡.md "wikilink")，並有供手持用的手把式外殼。

雖然是相當簡單的設計，它一直遲至19世紀末期才被發明，因為它必須結合電池與電燈泡的發明。在早期因為電池的蓄電力不足，因此在英文中它被稱為"flashlight"，意即短暫的燈。

## 發光二極體

[SurefireU2JPG.jpg](https://zh.wikipedia.org/wiki/File:SurefireU2JPG.jpg "fig:SurefireU2JPG.jpg")的Surefire
U2手电\]\]

最近愈來愈多的手電筒改用[發光二極體取代傳統的電燈泡](../Page/發光二極體.md "wikilink")。發光二極體已發明了數十年，在1999年，位在[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[聖荷西Lumileds公司](../Page/聖荷西.md "wikilink")\[[http://www.luxeon.com/\]發明了白色高能量的鹵素發光二極體。2001年時，Arc](http://www.luxeon.com/%5D發明了白色高能量的鹵素發光二極體。2001年時，Arc)
Flashlight公司將這種鹵素燈應用到手電筒上。

發光二極體可以用較少的電量但散發出更強的光源，因此它比傳統燈泡更省電。這類的手電筒有較長的電池壽命，有的可達數百個小時。

一個關於發光二極體的錯誤觀念是認為它們不會發熱。雖然低能量的發光二極體的確散發出很低的熱量，但高能量的發光二極體仍可放出高熱。

## 其他的手電筒設計型式

[LED-headlamp.jpg](https://zh.wikipedia.org/wiki/File:LED-headlamp.jpg "fig:LED-headlamp.jpg")
[Beretta90TWO.JPG](https://zh.wikipedia.org/wiki/File:Beretta90TWO.JPG "fig:Beretta90TWO.JPG")[半自動手槍的](../Page/半自動手槍.md "wikilink")[SureFire槍燈](../Page/:en:SureFire.md "wikilink")\]\]
[Lithium-ion_cell_(18650).jpg](https://zh.wikipedia.org/wiki/File:Lithium-ion_cell_\(18650\).jpg "fig:Lithium-ion_cell_(18650).jpg")及CR123A[鋰電池](../Page/鋰電池.md "wikilink")\]\]
**頭燈**是一種將燈戴在頭頂上的設計，戴在[安全帽上的高亮度頭燈已被礦工使用了幾十年之久](../Page/安全帽.md "wikilink")。

**槍燈**指裝置在槍枝上，以高亮度照射使敵人難以直視反擊，甚至使目標暫時炫目以利將之擊倒的軍警用手電筒。

槍用電筒經常會換裝尾線開關，以便在雙手保持[射擊姿勢的清況下就近按壓操控](../Page/射擊.md "wikilink")，或者直接與[護木](../Page/護木.md "wikilink")、前方[輔助握把整合在一起](../Page/輔助握把.md "wikilink")。許多款式還可以加裝專用的IR（[紅外線](../Page/紅外線.md "wikilink")）濾光罩，使發出的照明無法以肉眼發覺，僅有佩帶[夜視鏡的人員才能加以利用](../Page/夜視鏡.md "wikilink")。

槍用電筒主要使用[CR123A](../Page/CR123A.md "wikilink")
3[伏特](../Page/伏特.md "wikilink")[鋰電池](../Page/鋰電池.md "wikilink")，與可充電式的3伏特ICR123A，3.7伏特的[16340](../Page/16340.md "wikilink").[18650](../Page/18650.md "wikilink")（兩倍CR123長度）款電池。
[1](https://web.archive.org/web/20061210051607/http://www.handgunsmag.com/tactics_training/lights_072804/)
[2](https://web.archive.org/web/20061117043040/http://www.policeone.com/writers/columnists/JohnMeyer/articles/98209/).
參見 [Streamlight](../Page/Streamlight.md "wikilink")
和[Surefire](../Page/Surefire.md "wikilink").

某些軍警用手電筒還會將燈頭前緣做成王冠狀造型，在人員遭遇[格鬥時可發揮類似](../Page/格鬥.md "wikilink")[手指虎的打擊效果](../Page/手指虎.md "wikilink")，也可作為緊急時的[玻璃擊破器使用](../Page/玻璃擊破器.md "wikilink")。

大部份的手電筒被設計成圓筒型，然而早期的手電筒常被設計成各種樣式，有的長得像早年的燈籠，有的像蠟燭，預計在將來會有更多有趣的型式出現。

高品質的手電筒使用特別[電池](../Page/電池.md "wikilink")，它們可以調整亮度，有防水功能，而且可調整焦距，但通常也比較昂貴。

## 其它電力來源

[Knijpkat_996637.JPG](https://zh.wikipedia.org/wiki/File:Knijpkat_996637.JPG "fig:Knijpkat_996637.JPG")\]\]
因為電池是不小的花費，因此發展出了可自行提供電力的手電筒，有的利用[太陽能電池](../Page/太陽能.md "wikilink")。有的則利用手動[發電機](../Page/發電機.md "wikilink")，如**[動力燈](../Page/動力燈.md "wikilink")**。

## 圖片

[File:Radio-Light.jpg|收音機燈](File:Radio-Light.jpg%7C收音機燈)
[File:Multi-Light.jpg|多重灯泡的手電筒](File:Multi-Light.jpg%7C多重灯泡的手電筒)
[File:Taschenlampe1.jpg|其他各種型式的手電筒](File:Taschenlampe1.jpg%7C其他各種型式的手電筒)
<File:Chung> Nam
Flashlight.JPG|1960年代中南電筒廠生產的手電筒（[香港歷史博物館展品](../Page/香港歷史博物館.md "wikilink")）

## 參見

  - [筆燈](../Page/筆燈.md "wikilink")
  - [螢光棒](../Page/螢光棒.md "wikilink")
  - [動力燈](../Page/動力燈.md "wikilink")

[Category:消費電子產品](../Category/消費電子產品.md "wikilink")
[Category:灯](../Category/灯.md "wikilink")