**打生樁**，或稱**活人奠基**，是[東亞民間在建築前的習俗](../Page/東亞.md "wikilink")，屬[人祭的一種](../Page/人祭.md "wikilink")，[日本稱之為](../Page/日本.md "wikilink")**人柱**。是指在建築工程動工前，把若干人（通常是[兒童](../Page/兒童.md "wikilink")）活埋生葬在工地內，其目的是祈祷工程順利。

## 歷史

相傳這個方法是由[魯班首度提出的](../Page/魯班.md "wikilink")，當人們在一處地方動土時，便會破壞該處[風水](../Page/風水.md "wikilink")，且會觸怒該處的冤魂，以致在建造期間時常發生意外，因此便出現了「打生樁」，把活人生葬在工地上用作鎮邪，以減少出現的意外。現時考古發現最早的打生樁見於[河南](../Page/河南.md "wikilink")[鄭州](../Page/鄭州.md "wikilink")[東趙](../Page/東趙.md "wikilink")[二里頭文化的古城遺址](../Page/二里頭文化.md "wikilink")，當中有三座堆疊起來的古城，中間一層的地基發現用作奠基的[嬰兒遺骸](../Page/嬰兒.md "wikilink")\[1\]。可見打生樁習俗遠早於魯班所處的[春秋時代](../Page/春秋時代.md "wikilink")。

在古時，相傳在建橋前，先要活捉一對童男童女，把男童活埋在橋頭的橋墩內，而女童則生葬在橋尾的橋墩中，當橋建成後，他們就會成為了該橋的守護神。

[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[高麗時代](../Page/高麗時代.md "wikilink")[忠惠王在位期間](../Page/忠惠王.md "wikilink")，首都[開城曾有傳聞指忠惠王欲取民間小兒數十名埋在新宮殿的地基之下](../Page/開城.md "wikilink")，一時間開城人心惶惶，抱兒逃竄，治安混亂。

香港資深傳媒人[韋基舜表示](../Page/韋基舜.md "wikilink")，[廣州](../Page/廣州.md "wikilink")[海珠橋當年動工時亦出現打生樁的傳說](../Page/海珠橋.md "wikilink")。

隨著時代的進步，建築界也改用活雞，或以雞血灑在建築地盤四角取代來進行儀式。

## 疑似個案

[香港一些](../Page/香港.md "wikilink")[二戰前的建築](../Page/二戰.md "wikilink")，也流傳著「打生樁」的傳說，在1930到1940年代，家長會以「打生樁」一詞來恫嚇不聽話的兒童。2006年初於[香港](../Page/香港.md "wikilink")[何文田公主道一個水務署水管工程地盤](../Page/何文田.md "wikilink")，發現的大量兒童骸骨，有傳就是昔日的「生樁」。\[2\]

## 類似的風俗

類似的習俗還有**塞豆窿**。塞豆窿是一種非常殘忍的儀式，傳說古時在洪水為患的地方，防洪的堤壩經常氾濫，便會把一些小孩放進堤壩內的排水口（豆窿）內，他們相信以這個方法便能退洪。現今[粵語](../Page/粵語.md "wikilink")「塞豆窿」一詞借指小朋友，正是出於此典故。

在燒製[青銅器或](../Page/青銅器.md "wikilink")[瓷器亦有類似的傳說](../Page/瓷器.md "wikilink")，很多地方將犧牲者稱作「投爐神」、「爐神姑」等\[3\]。[韓國](../Page/韓國.md "wikilink")[慶州的媽媽鐘據說以此法鑄成](../Page/慶州.md "wikilink")。其科學根據在於人骨中的鈣質。

[緬甸古代興建大型建築物時](../Page/緬甸.md "wikilink")，會以生人[活埋在](../Page/活埋.md "wikilink")[地基之下](../Page/地基.md "wikilink")，作為守護神，這種風俗緬人稱為「苗賽」（Myosade）。\[4\]

## 参考文献

### 引用

### 来源

  -
  - [殘忍建築習俗打生樁鎮邪](http://the-sun.on.cc/channels/adult/20060629/20060628225915_0000.html)，《[太陽報](../Page/太陽報.md "wikilink")》，2006年6月29日

  - 《打生樁豬頭代人頭》，《太陽報》，2007年3月2日

## 參見

  - [上樑式](../Page/上樑式.md "wikilink")
  - [干將莫邪](../Page/干將莫邪.md "wikilink")
  - [消波塊](../Page/消波塊.md "wikilink")
  - [人柱力](../Page/尾獸#關於祭品之力.md "wikilink")，[日本動漫](../Page/日本動漫.md "wikilink")《[火影忍者](../Page/火影忍者.md "wikilink")》中的概念。
  - [人祭](../Page/人祭.md "wikilink")
  - 無綫劇集《[收規華](../Page/收規華.md "wikilink")》中，主角救回被拐作生樁的小孩。

[D打](../Category/人祭.md "wikilink")
[Category:東亞民間信仰](../Category/東亞民間信仰.md "wikilink")
[Category:東亞傳統建築](../Category/東亞傳統建築.md "wikilink")
[Category:都市傳說](../Category/都市傳說.md "wikilink")
[Category:建築儀式](../Category/建築儀式.md "wikilink")

1.  [古人夯筑城墙
    要用婴儿奠基？](http://newpaper.dahe.cn/hnsb/html/2015-01/04/content_1205614.htm?div=-1)
2.
3.  [炉神姑传说的形成探源](http://zibo.sdnews.com.cn/zbwh/201204/t20120420_671518.html)
4.  \*《緬甸史》，戈·埃·哈威著，姚梓良譯，132頁