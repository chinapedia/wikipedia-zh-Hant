**托勒密十一世·亚历山大二世 Πτολεμαίος ΙΑ' Αλέξανδρος
Β**'（？—前80年）[埃及](../Page/埃及.md "wikilink")[托勒密王朝国王](../Page/托勒密王朝.md "wikilink")（前80年在位）。他是最后一个完全**合法**地继承王位的托勒密王朝成员。

所谓**合法**，是指父母双方均出自王族，这是埃及自[法老时代就形成的惯例](../Page/法老.md "wikilink")。早在[古王国时期](../Page/埃及古王国.md "wikilink")，法老们为了保持王位继承人血统上的“纯洁”，经常与自己的姐妹结婚。由[亞歷山大帝國入主埃及的](../Page/亞歷山大帝國.md "wikilink")[托勒密王朝承袭了这一习惯](../Page/托勒密王朝.md "wikilink")。

托勒密十一世的在位时间极短，这部分与他对妻子的暴行有关。他本来是[罗马扶植的](../Page/古罗马.md "wikilink")“敌对国王”[托勒密十世·亚历山大一世之子](../Page/托勒密十世.md "wikilink")，罗马人计划继续扶持他当埃及国王。他为了继承王位而与先王[托勒密九世的遗孀](../Page/托勒密九世.md "wikilink")[貝勒尼基三世结婚](../Page/貝勒尼基三世.md "wikilink")。然而他在登上王位后不久竟将妻子杀害。托勒密十一世的倒行逆施引起人民的愤怒，不久他在[亚历山大城市民的起义中被杀](../Page/亚历山大城.md "wikilink")。由于他没有与贝勒尼基三世生下子女，托勒密九世的一个[私生子](../Page/私生子.md "wikilink")[托勒密十二世继承了王位](../Page/托勒密十二世.md "wikilink")。

## 参考

  - Peter Green, *Alexander to Actium*， University of California Press,
    1990, pp. 553-554 ISBN 0-520-05611-6

[Category:托勒密王朝法老](../Category/托勒密王朝法老.md "wikilink")
[Category:被處決者](../Category/被處決者.md "wikilink")