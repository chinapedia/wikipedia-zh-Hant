**白扥渾布**（；），[博爾濟吉特氏](../Page/博爾濟吉特氏.md "wikilink")，[字](../Page/表字.md "wikilink")**安敦**，又字**子元**、**愛山**\[1\]，室名**瑞留堂**、**瑞榴堂**\[2\]。[蒙古正藍旗人](../Page/蒙古正藍旗.md "wikilink")。[中國](../Page/中國.md "wikilink")[清朝政治人物](../Page/清朝.md "wikilink")、詩人。

## 經歷

[嘉慶二十三年](../Page/嘉慶.md "wikilink")（1818年）戊寅舉人。嘉慶二十四年（1819年）己卯恩科第三甲第八十三名[同進士出身](../Page/同進士出身.md "wikilink")。分發[湖南以](../Page/湖南.md "wikilink")[知縣任用](../Page/知縣.md "wikilink")，署[安化縣知縣](../Page/安化縣.md "wikilink")。後歷署[東安縣](../Page/東安縣.md "wikilink")、[湘潭縣知縣](../Page/湘潭縣.md "wikilink")，補授龍山縣知縣。[道光三年](../Page/道光.md "wikilink")（1823年）起任東安縣知縣。

道光八年（1828年），調任[福建](../Page/福建.md "wikilink")，歷署[福州府](../Page/福州府.md "wikilink")[知府](../Page/知府.md "wikilink")、[漳州府知府](../Page/漳州府.md "wikilink")、[興化府知府](../Page/興化府.md "wikilink")。十三年（1833年）兼署[台灣府知府](../Page/台灣府知府.md "wikilink")，擢[廣西](../Page/廣西.md "wikilink")[右江道](../Page/右江道.md "wikilink")。十四年（1834年）改福建[督糧道](../Page/督糧道.md "wikilink")。

道光十六年（1836年），擢[直隸](../Page/直隸.md "wikilink")[按察使](../Page/按察使.md "wikilink")。十七年（1837年），改[布政使](../Page/布政使.md "wikilink")。十九年（1839年）八月七日擢[山東巡撫](../Page/山東巡撫.md "wikilink")。

道光二十二年（1842年）五月二十日病假，十二月二十一日\[3\]以病免。二十三年（1843年）病卒。

## 著作

  - 《瑞榴軒詩》四卷

## 家庭及關連

子：金鎧、金鏞、金鑑。 孫：寶杰、寶樑、[寶棻](../Page/寶棻.md "wikilink")。

## 注釋

## 參考資料

  - 繆荃孫 編《續碑傳集》二冊卷二十三,284
  - 劉寧顏 編《重修台灣省通志》（台北市，台灣省文獻委員會，1994年版）
  - 《清國史館傳包》2533-1號,2801-1號

[Category:清朝安化縣知縣](../Category/清朝安化縣知縣.md "wikilink")
[Category:清朝湖南東安縣知縣](../Category/清朝湖南東安縣知縣.md "wikilink")
[Category:清朝湘潭縣知縣](../Category/清朝湘潭縣知縣.md "wikilink")
[Category:清朝龍山縣知縣](../Category/清朝龍山縣知縣.md "wikilink")
[Category:清朝福州府知府](../Category/清朝福州府知府.md "wikilink")
[Category:臺灣府知府](../Category/臺灣府知府.md "wikilink")
[Category:清朝直隸按察使](../Category/清朝直隸按察使.md "wikilink")
[Category:清朝直隸布政使](../Category/清朝直隸布政使.md "wikilink")
[Category:清朝山東巡撫](../Category/清朝山東巡撫.md "wikilink")
[Category:清朝詩人](../Category/清朝詩人.md "wikilink")
[Category:蒙古正藍旗人](../Category/蒙古正藍旗人.md "wikilink")
[Category:博爾濟吉特氏](../Category/博爾濟吉特氏.md "wikilink")

1.  《清史館傳稿》6102號
2.  杨廷福杨同甫 編《清人室名別稱字號索引》下冊905
3.  1843年1月21日