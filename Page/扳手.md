[Gedore_No._7_combination_wrenches_6–19_mm.jpg](https://zh.wikipedia.org/wiki/File:Gedore_No._7_combination_wrenches_6–19_mm.jpg "fig:Gedore_No._7_combination_wrenches_6–19_mm.jpg")

**扳手**是一種[工具](../Page/工具.md "wikilink")，用以轉動[螺栓](../Page/螺絲.md "wikilink")、[螺帽及其他難以用手轉動的物件](../Page/螺絲.md "wikilink")。也被稱為“扳頭”\[1\]。在[香港](../Page/香港.md "wikilink")，板手也被稱為**士巴拿**，該詞為[英國英語中](../Page/英國英語.md "wikilink")「spanner」一詞的音譯；扳手在[美國英語則稱為](../Page/美國英語.md "wikilink")「wrench」。更高質量的板手通常材質為[鉻](../Page/鉻.md "wikilink")-[釩合金工具鋼](../Page/釩.md "wikilink")，通常是[鑄造](../Page/鑄造.md "wikilink")，耐腐蝕，易於清洗。\[2\]

諸如[鉗子或](../Page/鉗子.md "wikilink")[鉗等鉸鏈工具通常不會被認為是扳手](../Page/鉗.md "wikilink")。

## 歷史

使用扳手或需要扳手的設備的相關應用，歷史學家早在十五世紀就已經注意到了。\[3\]

## 原理

扳手是以[杠杆原理為作動機制的工具](../Page/杠杆.md "wikilink")。

## 常見扳手

|                                                                                                                                                                                                                              | 名稱                                       | 說明                                                                                                                                                            | 備註   |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- |
| [Clé_plate.jpg](https://zh.wikipedia.org/wiki/File:Clé_plate.jpg "fig:Clé_plate.jpg")                                                                                                                                       | [開口扳手](../Page/開口扳手.md "wikilink")       | 此類扳手的末端有U形開口，方便握緊螺栓或螺帽的兩個邊；此類扳手通常是雙頭，每頭的開口有不同大小。                                                                                                              | 固定   |
| [Kluc_ockovy_vysunuty.jpg](https://zh.wikipedia.org/wiki/File:Kluc_ockovy_vysunuty.jpg "fig:Kluc_ockovy_vysunuty.jpg")                                                                                                     | [梅花扳手](../Page/梅花扳手.md "wikilink")       | 此類扳手的末端有封閉型開口，可以握緊螺栓或螺帽的各邊。封閉型開口通常是六角形或十二角形，可以轉動六角形的螺栓或螺帽；其中十二角形開口能以十二個角度套住螺栓或螺帽，於空間窄小的地方尤其有用。其他種類的閉口扳手包括八角形開口、梅花形開口等。                                        | 固定   |
| [Kluc_ockoplochy.jpg](https://zh.wikipedia.org/wiki/File:Kluc_ockoplochy.jpg "fig:Kluc_ockoplochy.jpg")                                                                                                                     | [兩用扳手](../Page/兩用扳手.md "wikilink")       | 此類扳手為雙頭扳手，一頭是開口，另一頭是閉口；兩頭的尺寸大小相同。                                                                                                                             | 固定   |
| [AdjustableWrenchWhiteBackground.jpg](https://zh.wikipedia.org/wiki/File:AdjustableWrenchWhiteBackground.jpg "fig:AdjustableWrenchWhiteBackground.jpg")                                                                      | [活動扳手](../Page/活動扳手.md "wikilink")       | 一種可自由調節開口大小的開口扳手，按調節鈕的設計和特性，再細分為月牙型扳手、水管扳手等。                                                                                                                  | 活動   |
| [Socket_wrench_and_sockets.JPG](https://zh.wikipedia.org/wiki/File:Socket_wrench_and_sockets.JPG "fig:Socket_wrench_and_sockets.JPG")                                                                                     | [套筒扳手](../Page/套筒扳手.md "wikilink")       | 此類扳手末端為中空的套筒，可以套住螺栓或螺帽的一端。套筒有時附有把手，和萬向接頭及插座配合其他工具使用。优点是夹座内置[棘輪這種单向](../Page/棘輪.md "wikilink")[机构](../Page/机构_\(工程学\).md "wikilink")，可直接来回摆动而不必放开，故有“喀哩喀哩”的绰号。 | 插座   |
| [Allen_keys_Scott_Ehardt.jpg](https://zh.wikipedia.org/wiki/File:Allen_keys_Scott_Ehardt.jpg "fig:Allen_keys_Scott_Ehardt.jpg")                                                                                           | |[内六角扳手](../Page/内六角扳手.md "wikilink")    | 常見為L型粗鋼線，鋼線的切面為正六角形，有各種不同大小。這類扳手適合轉動六角形凹槽的螺絲與螺栓。                                                                                                              |      |
|                                                                                                                                                                                                                              | [内三角扳手](../Page/内三角扳手.md "wikilink")     | 一种小型的扳手，弯头，切面為正三角形，外有圆形护套，常用于火车门的开关。实际上是一种通用[钥匙](../Page/钥匙.md "wikilink")。                                                                                   |      |
|                                                                                                                                                                                                                              | [外三角扳手](../Page/外三角扳手.md "wikilink")     | 切面为正三角形，与上述内三角扳手作用相似。                                                                                                                                         |      |
| [Torx_01_KMJ.jpg](https://zh.wikipedia.org/wiki/File:Torx_01_KMJ.jpg "fig:Torx_01_KMJ.jpg")                                                                                                                                | [星形扳手](../Page/星形扳手.md "wikilink")       | 另一種用以轉動凹槽螺絲的扳手，切面成星形，常見於自動組合器械。                                                                                                                               |      |
|                                                                                                                                                                                                                              | [帶狀或鍊狀扳手](../Page/帶狀或鍊狀扳手.md "wikilink") | 由一條金屬帶或皮帶或鐵鍊，繫著一柄把手組成，用以握緊並轉動光滑的圓柱形物件。                                                                                                                        |      |
| [Trimo_pattern_Aluminum_Pipe_Wrenches.jpg](https://zh.wikipedia.org/wiki/File:Trimo_pattern_Aluminum_Pipe_Wrenches.jpg "fig:Trimo_pattern_Aluminum_Pipe_Wrenches.jpg")                                                   | [管子扳手](../Page/管子扳手.md "wikilink")       | 又名管鉗，其活動顎兩邊皆有鋸齒狀之硬齒，能將光滑之管壁鉗住。                                                                                                                                |      |
| [Impact_wrench_01.jpg](https://zh.wikipedia.org/wiki/File:Impact_wrench_01.jpg "fig:Impact_wrench_01.jpg")                                                                                                                 | [氣動扳手](../Page/氣動扳手.md "wikilink")       | 由壓縮空氣為動力，可提供較手動扳手大的扭力。                                                                                                                                        |      |
| [Die_wrench_handle_holding_a_die,_plus_a_second_die..jpg](https://zh.wikipedia.org/wiki/File:Die_wrench_handle_holding_a_die,_plus_a_second_die..jpg "fig:Die_wrench_handle_holding_a_die,_plus_a_second_die..jpg") | [螺絲扳手](../Page/絲攻和絲板.md "wikilink")      | 中間可挾持刀具，以两側長柄轉動操作，可进行切削螺紋及各類孔加工作業                                                                                                                             | 亦稱絞槓 |
|                                                                                                                                                                                                                              | [力矩扳手](../Page/力矩扳手.md "wikilink")       | 可以以特定力矩紧固螺母的扳手                                                                                                                                                |      |

## 尺寸名稱

[5-8_vs_5-8_003.jpg](https://zh.wikipedia.org/wiki/File:5-8_vs_5-8_003.jpg "fig:5-8_vs_5-8_003.jpg")

尺寸通常由尺寸指定，例如跨平面距離（內六角形尺寸）。在十九世紀和二十世紀初期，根據“螺紋”的名義尺寸來定義扳手的公稱尺寸是很常見的。

## 参见

  - [杠杆](../Page/杠杆.md "wikilink")
  - [螺丝刀](../Page/螺丝刀.md "wikilink")
  - [钥匙](../Page/钥匙.md "wikilink")
  - [螺丝](../Page/螺丝.md "wikilink")、[螺栓](../Page/螺栓.md "wikilink")、[螺母](../Page/螺母.md "wikilink")
  - [內六角板手](../Page/內六角板手.md "wikilink")
  - [扳手尺寸](../Page/扳手尺寸.md "wikilink")

## 參考來源

### 引用

### 書目

[Category:工具](../Category/工具.md "wikilink")
[Category:螺絲](../Category/螺絲.md "wikilink")

1.
2.
3.  Henry C. Mercer, Ancient Carpenters' Tools: Illustrated and
    Explained, Together with the Implements of the Lumberman, Joiner and
    Cabinet-Maker, 1928, reprint Courier Corporation - 2013, pages
    271-272