[Bruce_McCandless_II_during_EVA_in_1984.jpg](https://zh.wikipedia.org/wiki/File:Bruce_McCandless_II_during_EVA_in_1984.jpg "fig:Bruce_McCandless_II_during_EVA_in_1984.jpg")。\]\]

**技術**可以指人類對[機器](../Page/機器.md "wikilink")、[硬體或人造器皿的運用](../Page/硬體.md "wikilink")，但它也可以包含更廣的架構，如[系統](../Page/系統.md "wikilink")、組織[方法學和技巧](../Page/方法學.md "wikilink")。它是[知識進化的主體](../Page/知識.md "wikilink")，由[社會形塑或形塑](../Page/社會.md "wikilink")[社會](../Page/社會.md "wikilink")。如[電腦等新技術的增生使人們相信技術是社會進化的決定性](../Page/電腦.md "wikilink")[力量](../Page/力量.md "wikilink")，換句話說，它是驅動改變的自發性動力。最好放棄[化約主義的觀點](../Page/化約主義.md "wikilink")，而將技術視為包含了[社會](../Page/社會.md "wikilink")、[政治](../Page/政治.md "wikilink")、[歷史及](../Page/歷史.md "wikilink")[經濟因素等一起作用而造成改變之多面向](../Page/經濟.md "wikilink")[社會](../Page/社會.md "wikilink")[網絡的一組成元素不論有形或無形](../Page/網絡.md "wikilink")。

最初，人類會把[石塊等](../Page/石.md "wikilink")[自然界的材料](../Page/自然界.md "wikilink")，製作成一些簡單的工具，這已是技術的起源。而[史前](../Page/史前.md "wikilink")[人類發現](../Page/人類.md "wikilink")[生火的方法](../Page/早期人類對火的使用.md "wikilink")，也增添了[食物的](../Page/食物.md "wikilink")[來源和](../Page/來源.md "wikilink")[種類](../Page/種類.md "wikilink")；[輪子的](../Page/輪子.md "wikilink")[發明則令人類的](../Page/發明.md "wikilink")[運輸變得更為方便](../Page/運輸.md "wikilink")。這些都是[古時技術的例子](../Page/古時技術.md "wikilink")。現今的[發明](../Page/發明.md "wikilink")，如[印刷機](../Page/印刷機.md "wikilink")、[電報](../Page/電報.md "wikilink")、[電話](../Page/電話.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、[手機](../Page/手機.md "wikilink")、[網路和](../Page/網路.md "wikilink")[網際網路](../Page/網際網路.md "wikilink")，為人類提供了新的[通信途徑](../Page/通信.md "wikilink")。不過，技術並不總是用在改善生活的用途上；無論是[原始的](../Page/原始.md "wikilink")[棍棒還是大殺傷力的](../Page/棍棒.md "wikilink")[核武器](../Page/核武器.md "wikilink")，都是為追求破壞性能而發明的。

技術對社會的影響不容忽視，就連現今全球的[經濟都離不開技術發展的成果](../Page/經濟.md "wikilink")。而許多[技術生產](../Page/技術生產.md "wikilink")、[加工的過程中](../Page/加工.md "wikilink")，可能會產生一些無用途的[副產品](../Page/副產品.md "wikilink")，成為[污染排放的來源](../Page/污染.md "wikilink")，並耗用了大量的自然資源，引致不同的[環境問題](../Page/環境問題.md "wikilink")。新技術的[發展](../Page/發展.md "wikilink")，亦會帶來一些新的[倫理問題](../Page/倫理.md "wikilink")，或是改變大眾的習慣。其中的例子包括，原來用作描述機器運作的[效率一詞](../Page/能量轉換效率.md "wikilink")，近來也被廣泛用在表示人的工作能力上。

對於技術的發展，[哲學上亦有不同的論調](../Page/哲學.md "wikilink")。其中[新卢德主义和](../Page/新卢德主义.md "wikilink")大致上都反對現代技術在社會的應用，認為技術並未真正改善[人類的生活之餘](../Page/人類.md "wikilink")，還破壞了環境，疏遠人與人之間的關係。與之相反，[超人文主义和](../Page/超人文主义.md "wikilink")的[意識形態則認為技術有助人類進步](../Page/意識.md "wikilink")，以及可以突破人類遇到的限制。

## 科技的本質

觀其本質，技術的存在取決於人們的需要，並滿足其需要。早期人類創造及使用技術是為了解決其基本需求。而現在的技術則是為了滿足人們更廣泛的需求和慾望，並需要一巨大的[社會結構來支撐它](../Page/社會結構.md "wikilink")。

在今日，此一現象的一重要例子為[電話](../Page/電話.md "wikilink")。當電話在發展的過程中，社會變得有想要更可攜設備的慾望。最後，此一慾望產生了對新產品的需求，導致了[手機的發明](../Page/手機.md "wikilink")。現在，幾乎每個人都可以隨時通話，不論其身在何處。此一發明改變了人們之間的關係：有些人現今被負更多說明義務及更被依賴，且更少理由不保持連繫。技術的複雜性創造了一個技術與社會間的相互影響。

## 科學、技術與工程

[Zoom_lunette_ardente.jpg](https://zh.wikipedia.org/wiki/File:Zoom_lunette_ardente.jpg "fig:Zoom_lunette_ardente.jpg")进行由放大的太阳光线产生的燃烧实验。\]\]

**[科學](../Page/科學.md "wikilink")**、**技術**與**[工程](../Page/工程.md "wikilink")**的分別通常並不明確。一般來講，廣義的“科學”可指*[基礎科學](../Page/基礎科學.md "wikilink")*、*[應用科學](../Page/應用科學.md "wikilink")*等。前者較注重對自然的觀察、理論和純研究，而後者則放較多的焦點在實際經驗上；技術則介於兩者之間。

狹義的“科學”只指[基礎科學](../Page/基礎科學.md "wikilink")（[數學及](../Page/數學.md "wikilink")[自然科學](../Page/自然科學.md "wikilink")），是研究和解釋[自然現象的學科](../Page/自然現象.md "wikilink")，着重尋找事物間的關係。它通常利用一套預先建立好的方法來進行[理論研究](../Page/理論研究.md "wikilink")，即[科學方法](../Page/科學方法.md "wikilink")\[1\]。另一方面，工程學則着重於解決問題，即而製作工具，並為自然現象找出實際應用，時常（但非一定）用到科學的研究成果或方法。技術並不只是科學發展的產物，因為技術發展亦講求[效用](../Page/效用.md "wikilink")、[易用性和](../Page/易用性.md "wikilink")[安全](../Page/安全.md "wikilink")。除此之外，為解決問題，技術發展還會應用[數學](../Page/數學.md "wikilink")、[語言學](../Page/語言學.md "wikilink")、[歷史等多個領域的知識](../Page/歷史.md "wikilink")，務求取得實質的結果。

雖然人類的技術歷程其實遠在基礎科學和工程發展之先已經開始（見[技術史一節](../Page/技术#技術（科技）史.md "wikilink")），但現今技術大多都需要上述兩者的基礎。例如，科學家可能會研究[電子在](../Page/電子.md "wikilink")[導體內的流動](../Page/導體.md "wikilink")。此後，工程師可能利用這些新知識，制造出新工具或設備，如[半導體](../Page/半導體.md "wikilink")、[電腦及其他類型的先進技術](../Page/電腦.md "wikilink")。這種情況下，上述兩人都可視為對技術發展作出貢獻。事實上，科學、工程和技術三個領域的研究時常被認為是密不可分的。\[2\]

二十世紀後期的科學家、歷史學家和政治家，對於科學和技術之間的準確關係，有着不同的看法。[二次大戰時的美國](../Page/第二次世界大战.md "wikilink")，普遍認為技術就是單純的「應用科學」，而支持基礎科學的研究，只是為了及時收穫技術的成果。[万尼瓦尔·布什在論文](../Page/万尼瓦尔·布什.md "wikilink")〈科學,無盡的邊疆〉(*Science—The
Endless
Frontier*)中論到戰後的[科技政策](../Page/科技.md "wikilink")，說：「新的產品、新的產業、更多的就業職位，這些都需要持續地探索自然定律來維持……我們只能透過科學來從事這種探索。」但到了1960年代後期，這種觀點受到了各方面的攻擊，又倡議撥款支持一些特定的科學研究計劃。這個議題至今仍然備受爭議，但一般已不再認為技術就是科學研究的成果。\[3\]\[4\]

## 技術（科技）史

### 旧石器时代（公元前250万年 - 公元前10,000年）

[Biface_de_St_Acheul_MHNT.jpg](https://zh.wikipedia.org/wiki/File:Biface_de_St_Acheul_MHNT.jpg "fig:Biface_de_St_Acheul_MHNT.jpg")手斧（Acheulean
handaxes）\]\]
技術的歷史和人類一樣久遠。一些主要的工具類型幾乎在每一次舊人類的考古發現中都會被找到，甚至是[巧人的時代](../Page/巧人.md "wikilink")。不過，其他動物也被發覺可以學習使用及精製工具，所以將人類視為唯一會使用及製作工具的動物是不正確的。技術的歷史依順著由簡單的工具及能源（多為人力）至複雜的[高科技工具及能源的過程發展](../Page/高科技.md "wikilink")。人類約在五萬年前開始有[行為現代性](../Page/行為現代性.md "wikilink")，使用精製的工具，許多考古學家相信和完整的[語言出現有關](../Page/語言.md "wikilink")\[5\]。

#### 石器工具

人類的祖先早在[智人出現的二十萬年前](../Page/智人.md "wikilink")，就已開始使用工具\[6\]。最早的[石器製造方式是在](../Page/石器.md "wikilink")文化中，約在二百三十萬年前\[7\]，最早使用工具的直接證據出現在[衣索匹亞的](../Page/衣索匹亞.md "wikilink")[東非大裂谷](../Page/東非大裂谷.md "wikilink")，時間約在二百五十萬年前\[8\]。這個使用石器工具的時期稱為[舊石器時代](../Page/舊石器時代.md "wikilink")，所有的人類歷史都是從約一萬二千年前發展的[農業開始](../Page/農業.md "wikilink")。

#### 火

約五十萬至一百萬年前，人類開始使用[火而然後掌握它](../Page/火.md "wikilink")，這是人類歷史上技術演進的重要轉捩點\[9\]，提供了一具有許多深遠用途的簡單能源。目前還不知道第一次使用火的時間，根據[斯泰克方丹、斯瓦特科蘭斯、科羅姆德拉伊和維羅恩斯的化石遺址發現的燒焦動物骨頭](../Page/斯泰克方丹、斯瓦特科蘭斯、科羅姆德拉伊和維羅恩斯的化石遺址.md "wikilink")，推測人類早在西元前一百萬年前就已將火馴化\[10\]。學者認為[直立人在約西元前五十萬至四十萬年前已可以控制火](../Page/直立人.md "wikilink")\[11\]\[12\]。第一次使用火來加熱可能是在食物的準備上。其使得植物及動物等食物來源大大地增加，因為它大大地降低了食物的腐敗速度。

火再來擴展到了天然資料的加工上，且允許利用需用火加工的天然資料。（現今找到最古老的拋射武器為用火燒固的木製長矛，約在二十五萬年前）[木材和](../Page/木材.md "wikilink")[木炭是第一種被使用來能源的物質](../Page/木炭.md "wikilink")。木材、[黏土和石頭](../Page/黏土.md "wikilink")（如[石灰岩](../Page/石灰岩.md "wikilink")）是最早用火來塑形和加工的物質，用來製造如[武器](../Page/武器.md "wikilink")、[陶器](../Page/陶器.md "wikilink")、[磚和](../Page/磚.md "wikilink")[水泥等加工品](../Page/水泥.md "wikilink")。

#### 衣服及住處

在舊石器時代出現的其他技術有[衣服及住處](../Page/衣服.md "wikilink")，目前還不知道這二種技術具體出現的時間，但這些是人類發展的關鍵。在舊石器時代中，住處變的越來越複雜而精緻，早在西元前三十八萬年前，人類就已建造出臨時的木屋\[13\]\[14\]。衣服從獵殺動物後取皮毛製成，讓人類可以適應較冷的地區，人類也在西元前二十萬年開始了，由非洲移居到其他洲，例如[歐亞大陸](../Page/歐亞大陸.md "wikilink")\[15\]。

### 新石器时代至古典古代（公元前10,000年 - 公元300年）

人类技术的提升发轫于所谓的[新石器时代](../Page/新石器时代.md "wikilink")。研磨[石斧的发明是一个重大的进步](../Page/石器.md "wikilink")，因为它允许大规模砍伐森林来创建农场。

#### 金属工具

持續地改善，創造出來[火爐和](../Page/火爐.md "wikilink")[風箱](../Page/風箱.md "wikilink")，及[精煉與](../Page/精煉.md "wikilink")[鍛造自然金屬的能力](../Page/鍛造.md "wikilink")。[金](../Page/金.md "wikilink")、[銅](../Page/銅.md "wikilink")、[銀和](../Page/銀.md "wikilink")[鉛為最早可被精煉出的幾種金屬](../Page/鉛.md "wikilink")。勝過石、骨及木製工具之銅製工具的優點很快地展現在早期人類身上，而自然銅的使用則大約在[新石器時代初期](../Page/新石器時代.md "wikilink")（約西元前八千年）。在自然界裡並不存在很大量的自然銅，但銅礦則是很普遍的，且有些可以用木材或木炭很容易地製造出來。

約在西元前4000年，金屬的製作導致[青銅和](../Page/青銅.md "wikilink")[黃銅等](../Page/黃銅.md "wikilink")[合金的發明](../Page/合金.md "wikilink")，因此稱為[青銅時代](../Page/青銅時代.md "wikilink")。[鋼等鐵合金的第一次使用則在西元前](../Page/鋼.md "wikilink")1400年，[鐵器時代開始](../Page/鐵器時代.md "wikilink")。

#### 能源和交通运输

[Wheel_Iran.jpg](https://zh.wikipedia.org/wiki/File:Wheel_Iran.jpg "fig:Wheel_Iran.jpg")約在西元前4000年被發明出來。\]\]
其間，人類學會到控制其他類型的能量。所知最早風力的使用為帆船。一裝有帆的書於西元前三千兩百年的埃及壁畫中被發現。從史前時代開始，埃及人大約就在使用「尼羅河的力量」一年一次的洪河來灌溉他們的土地，並漸漸學會經由計畫性地建造灌溉渠道和水池來管理田地。類似地，美索不達米亞的早期人民，蘇美人會來了使用底格里斯河和幼發拉底河來做為相同的用途。但更多風力和水力，甚至人力的使用則需要再另一次的發明。

### 中世纪和现代历史（公元300年 - 至今）

工具包含有[簡單機械](../Page/簡單機械.md "wikilink")，如[槓桿](../Page/槓桿.md "wikilink")、[螺絲和](../Page/螺絲.md "wikilink")[滑輪](../Page/滑輪.md "wikilink")，和複雜的機械，如[時鐘](../Page/時鐘.md "wikilink")、[發動機](../Page/發動機.md "wikilink")、[發電機](../Page/發電機.md "wikilink")、[電動機](../Page/電動機.md "wikilink")、[收音機](../Page/收音機.md "wikilink")、[電報](../Page/電報.md "wikilink")、[電話](../Page/電話.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、[手機和](../Page/手機.md "wikilink")[太空站等](../Page/太空站.md "wikilink")。

[紙及](../Page/紙.md "wikilink")[活字印刷術的發明是技術的重大進展](../Page/活字印刷術.md "wikilink")。一般認為[蔡倫是紙的發明者](../Page/蔡倫.md "wikilink")，其所發明的紙為現今所認為的紙，相對於由[紙莎草的莖織成的](../Page/紙莎草.md "wikilink")[莎草紙](../Page/莎草紙.md "wikilink")。他於西元105年描述了現在的造紙方法。大部份早期的原料都是很少見且昂貴的。紙在數個世紀之間都還是個奢侈品，直到十九世紀蒸氣造紙機的發明，它可以由紙漿中取出纖維來造紙。一般利用如雲杉木等的針葉樹材。

活字印刷術為一能將同一文字印在多份紙上的[印刷裝備](../Page/印刷.md "wikilink")。允許單一文字被排列成文字的活字排版於西元1041年至1048年之間由[中國的](../Page/中國.md "wikilink")[畢昇所發明](../Page/畢昇.md "wikilink")。將活字印刷使用於大量生產的印刷業的第一人為德國金匠，最後成為印刷業者的[約翰內斯·古騰堡](../Page/約翰內斯·古騰堡.md "wikilink")，他於西元1440年引入活字印刷，並使其普及起來。

[Late_model_Ford_Model_T.jpg](https://zh.wikipedia.org/wiki/File:Late_model_Ford_Model_T.jpg "fig:Late_model_Ford_Model_T.jpg")彻底改变了个人交通。\]\]
[工業革命為於](../Page/工業革命.md "wikilink")18世紀晚期及19世紀初期的一在技術、社會、經濟及文化上的重大改變。它起源於英國，並擴散至全世界。於此期間，基於手工的經濟被[機械的產業及](../Page/機械.md "wikilink")[製造所替代](../Page/製造業.md "wikilink")。它開始於紡織產業的機械化和製[鐵技術的發展](../Page/鐵.md "wikilink")，以及因[運河引進](../Page/運河.md "wikilink")、[道路改善及](../Page/道路.md "wikilink")[鐵路運輸等而產生的貿易擴張](../Page/鐵路運輸.md "wikilink")。[蒸汽機](../Page/蒸汽機.md "wikilink")（燃料主要為[煤](../Page/煤.md "wikilink")）的引進及動力機械（主要在[紡織製造上](../Page/紡織製造.md "wikilink")）更鞏固了生產量戲劇性的增加。
19世紀前兩個年代的全金屬[機床發展促進了其他產業更多的生產機械](../Page/機床.md "wikilink")。

[IC_Nanotecnology_2400X.JPG](https://zh.wikipedia.org/wiki/File:IC_Nanotecnology_2400X.JPG "fig:IC_Nanotecnology_2400X.JPG")\]\]

當工具的複雜度增加時，支撐其所需的知識種類也增加了。複雜的當代機械需要一整套有關知識的技術手冊，且還在不斷地增加及改良，其設計者、建立者、維修者和使用者常需要數年一般和特定的訓練來熟練它。甚至，工具會太過複雜，以致需要如：[工程学](../Page/工程学.md "wikilink")、[醫學和](../Page/醫學.md "wikilink")[電腦科學等基礎知識的工具](../Page/電腦科學.md "wikilink")、程序及練習來支撐它們。複雜的[製造及](../Page/製造.md "wikilink")[土木](../Page/土木.md "wikilink")[建築技術及組織被需要以用來建立及維持它們](../Page/建築.md "wikilink")。整個產業都被用來支撐與發展下一代更複雜的工具。

## 專利權

當被用於商業行為時，技術可以確保公司和其他公司的競爭力。但達到、發明或使用技術的投資成本，稱為[知識產權](../Page/知識產權.md "wikilink")，是非常高的。因此，許多社會（如[美國](../Page/美國.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")）都會對此一此一投資給予政府的保護，經由給予[專利](../Page/專利.md "wikilink")，一排他性的權利。此一保護能使公司投資在技術上的成本能夠回收，因此可以鼓勵創新。但亦有另一觀點指出，專利的過度保護，反而會使得創新遭到阻礙。

## 其他物種的技術

[鳥是另一種時常會以許多材料建立精緻的窩和簡單工具的動物](../Page/鳥.md "wikilink")。通常，它們並不被認為在實作一技術技藝，主要是因為此行為大多來自於本能。但還是有一些偶發性文化轉移的證據，尤其在非人類靈長類之間。已有許多證據證明人類以外的動物亦有些簡單的技術。例如：[紅毛猩猩會用尖銳的樹枝捕魚](../Page/紅毛猩猩.md "wikilink")，[大猩猩會用樹枝測水深](../Page/大猩猩.md "wikilink")、當作手杖來使用等等。

## 参考文献

## 参见

  - [20世纪最伟大的工程学成就](../Page/20世纪最伟大的工程学成就.md "wikilink")
  - [知識經濟](../Page/知識經濟.md "wikilink")
  - [工具規律](../Page/工具規律.md "wikilink")
  - [劉易斯·芒福德](../Page/劉易斯·芒福德.md "wikilink")
  - [科技樹](../Page/科技樹.md "wikilink")
  - [理工](../Page/理工.md "wikilink")

<!-- end list -->

  - 技術的概念及理論

<!-- end list -->

  - [适用技术](../Page/适用技术.md "wikilink")
  - [人类增强](../Page/人类增强.md "wikilink")
  - [范式](../Page/范式.md "wikilink")
  - [預防原則](../Page/預防原則.md "wikilink")
  - [专家统治](../Page/专家统治.md "wikilink")
  - [技術奇異點](../Page/技術奇異點.md "wikilink")
  - [技術就緒指數](../Page/技術就緒指數.md "wikilink")
  - [超人類主義](../Page/超人類主義.md "wikilink")

<!-- end list -->

  - 技術的經濟

<!-- end list -->

  - [後稀缺](../Page/後稀缺.md "wikilink")
  - [专家统治](../Page/专家统治.md "wikilink")
  - [技術接受模型](../Page/技術接受模型.md "wikilink")

<!-- end list -->

  - 技術期刊

<!-- end list -->

  - [Engadget](../Page/Engadget.md "wikilink")
  - [TechCrunch](../Page/TechCrunch.md "wikilink")
  - [The Verge](../Page/The_Verge.md "wikilink")
  - [連線](../Page/連線.md "wikilink")

{{-}}

[技術](../Category/技術.md "wikilink")

1.
2.
3.  .
4.  .
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.