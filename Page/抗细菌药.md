[Staphylococcus_aureus_(AB_Test).jpg](https://zh.wikipedia.org/wiki/File:Staphylococcus_aureus_\(AB_Test\).jpg "fig:Staphylococcus_aureus_(AB_Test).jpg")的培養皿\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Antibiotics_action.png "fig:缩略图")
**抗细菌药**（）也称为“**抗细菌剂**”，是一类用于抑制细菌生长或杀死细菌的药物。\[1\]在不引起歧义的情况下，抗细菌药也可简称为“**抗菌药**”，包括**抗生素**（）
由微生物（包括细菌、真菌、放线菌属）所产生的具有抑制其它类微生物生长、生存的一类[次级代谢产物](../Page/次级代谢产物.md "wikilink")，以及用化学方法合成或半合成的类似化合物。在定義上是一較廣的概念，包括抗细菌抗生素、抗真菌抗生素以及對付其他微小病原之抗生素；但臨床實務中，抗生素常常是指抗細菌抗生素。抗生素的副作用之一是使肠道正常菌群失调。\[2\]

## 用途

  - 抗细菌药最主要用於醫療方面，可以用來殺死細菌。
  - 對抗在人或動物體內的致幹菌等病原體，可治療大多數[細菌](../Page/細菌.md "wikilink")、[立克次體](../Page/立克次體.md "wikilink")、[支原體](../Page/支原體.md "wikilink")、[衣原體](../Page/衣原體.md "wikilink")、[螺旋體等微生物感染導致的](../Page/螺旋體.md "wikilink")[疾病](../Page/疾病.md "wikilink")。
  - 對於[病毒](../Page/病毒.md "wikilink")、[朊毒體等結構簡單的病原體所引起的](../Page/朊毒體.md "wikilink")[疾病沒有效用](../Page/疾病.md "wikilink")。
  - 除了抗細菌性的[感染外](../Page/感染.md "wikilink")，某些抗细菌药還具有抗[腫瘤活性](../Page/腫瘤.md "wikilink")，用於腫瘤的[化學治療](../Page/化學治療.md "wikilink")。
  - 有些抗细菌药還具有[免疫抑制作用](../Page/免疫.md "wikilink")。
  - 抗细菌药除用於[醫療](../Page/醫療.md "wikilink")，還應用於生物科學研究、[農業](../Page/農業.md "wikilink")、[畜牧業和](../Page/畜牧業.md "wikilink")[食品工業等方面](../Page/食品工業.md "wikilink")。
  - 在畜牧業和農業中非治療用途的抗细菌药，稱為[抗生素生長促進劑](../Page/抗生素生長促進劑.md "wikilink")。

## 製造方法

[缩略图](https://zh.wikipedia.org/wiki/File:Pyrazinamide_ball-and-stick.png "fig:缩略图")的[吡嗪醯胺化學式](../Page/吡嗪醯胺.md "wikilink")\]\]
抗細菌藥的主要製造方法為[发酵](../Page/发酵.md "wikilink")，也可以通過[化學合成和](../Page/化學合成.md "wikilink")[半合成的方法製造出來](../Page/半合成.md "wikilink")。
先採用適當的菌種，經輻射處理，選擇最具潛力的變異菌株，將其從試管轉移到燒瓶再到發酵槽，在溫度與通風控制的環境中培育，然後從培養基中分離、純化、析離成抗生素的結晶。經研判化學結構、藥理實驗與臨床試驗後，才告完成。

## 歷史

人类合成的第一种抗菌药是[磺胺](../Page/磺胺.md "wikilink")，1932～1933年间德国病理与细菌学家[格哈德·多馬克发现其具有体内抗菌活性](../Page/格哈德·多馬克.md "wikilink")，他因此获得1939年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

人类发现的第一种抗生素——[青黴素](../Page/青黴素.md "wikilink")（盤尼西林），是英国微生物学家[亞歷山大·弗莱明于](../Page/亞歷山大·弗莱明.md "wikilink")1928年偶然發現的，但当时并没有提纯出有效成分和分析化学结构。他從被黴菌汙染的[葡萄球菌培養皿中](../Page/葡萄球菌.md "wikilink")，觀察到黴菌附近的細菌都無法生長，推測黴菌中可能有殺菌的物質，1929年，弗萊明將這個發現發表在《英國實驗病理學期刊》，但沒有得到重視。直到1939年，牛津大學的佛洛里（Howard
Florey）和柴恩（Ernst
Chain）想開發能醫療細菌感染的藥物，才在聯絡弗萊明取得菌株後，成功提纯出青黴素。1939年至1940年酪毛黴素、盤尼西林及放線黴素先後被分離出，開創了抗生素的新紀元。弗萊明、佛洛里與柴恩因此於1945年共同獲得[诺贝尔生理学和医学奖](../Page/诺贝尔生理学和医学奖.md "wikilink")\[3\]\[4\]。
[Right](https://zh.wikipedia.org/wiki/File:MRSA7820.jpg "fig:Right")

## 分类

作为抗菌剂使用的抗生素有以下几个主要类别：

  - [β-内酰胺类抗生素](../Page/β-内酰胺类抗生素.md "wikilink")
      - [青黴素](../Page/青黴素.md "wikilink")
      - [头孢菌素](../Page/头孢菌素.md "wikilink")
      - 非典型的β-内酰胺类抗生素
  - [氨基糖苷类抗生素](../Page/氨基糖苷类抗生素.md "wikilink")
  - [大环内酯类抗生素](../Page/大环内酯类抗生素.md "wikilink")
  - [四环素类抗生素](../Page/四环素类抗生素.md "wikilink")
  - [氯霉素](../Page/氯霉素.md "wikilink")（全合成抗生素）

另外，既不是微生物分泌物又不是其类似物的人工全合成抗菌剂有：

  - [喹诺酮](../Page/喹诺酮.md "wikilink")
  - [磺胺类药物](../Page/磺胺类药物.md "wikilink")

在不严格的情况下，有时候也把这两类抗菌剂并称为“抗生素”。

<center>

<table>
<caption><strong>抗生素</strong>[5]</caption>
<thead>
<tr class="header">
<th><p>中文名称</p></th>
<th><p>英文名称</p></th>
<th><p>廠牌名稱</p></th>
<th><p>应用</p></th>
<th><p>副作用</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/氨基糖苷类抗生素.md" title="wikilink">氨基糖苷类抗生素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿米卡星.md" title="wikilink">阿米卡星</a></p></td>
<td><p>amikacin</p></td>
<td><p>Amikin</p></td>
<td><p>革兰氏阴性菌感染，如<a href="../Page/大肠杆菌.md" title="wikilink">大肠杆菌</a>、<a href="../Page/克雷伯氏菌.md" title="wikilink">克雷伯氏菌</a>、<a href="../Page/绿脓杆菌.md" title="wikilink">绿脓杆菌</a></p></td>
<td><ul>
<li>听力损害</li>
<li>眩晕</li>
<li>肾毒性</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庆大霉素.md" title="wikilink">庆大霉素</a></p></td>
<td><p>Gentamycin</p></td>
<td><p>Garamycin</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡那霉素.md" title="wikilink">卡那霉素</a></p></td>
<td><p>Kanamycin</p></td>
<td><p>Kantrex</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新霉素.md" title="wikilink">新霉素</a></p></td>
<td><p>Neomycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奈替米星.md" title="wikilink">奈替米星</a></p></td>
<td><p>Netilmicin</p></td>
<td><p>Netromycin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/链霉素.md" title="wikilink">链霉素</a></p></td>
<td><p>Streptomycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/妥布霉素.md" title="wikilink">妥布霉素</a></p></td>
<td><p>Tobramycin</p></td>
<td><p>Nebcin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴龙霉素.md" title="wikilink">巴龙霉素</a></p></td>
<td><p>Paromomycin</p></td>
<td><p>Humatin</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安莎霉素类.md" title="wikilink">安莎霉素类</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格尔德霉素.md" title="wikilink">格尔德霉素</a></p></td>
<td><p>Geldanamycin</p></td>
<td></td>
<td><p>用於抗腫瘤，處於實驗階段</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/除莠霉素.md" title="wikilink">除莠霉素</a></p></td>
<td><p>Herbimycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/碳头孢烯.md" title="wikilink">碳头孢烯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯碳头孢.md" title="wikilink">氯碳头孢</a></p></td>
<td><p>Loracarbef</p></td>
<td><p>Lorabid</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/碳青霉烯.md" title="wikilink">碳青霉烯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厄他培南.md" title="wikilink">厄他培南</a></p></td>
<td><p>Ertapenem</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亚胺培南.md" title="wikilink">亚胺培南</a></p></td>
<td><p>Imipenem/Cilastatin</p></td>
<td><p>Primaxin</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美罗培南.md" title="wikilink">美罗培南</a></p></td>
<td><p>Meropenem</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第一代頭孢菌素.md" title="wikilink">第一代頭孢菌素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢羟氨苄.md" title="wikilink">头孢羟氨苄</a></p></td>
<td><p>Cefadroxil</p></td>
<td><p>Duricef</p></td>
<td></td>
<td><ul>
<li><a href="../Page/消化道.md" title="wikilink">消化道不適及腹瀉</a></li>
<li><a href="../Page/噁心.md" title="wikilink">噁心</a>（若與<a href="../Page/酒精.md" title="wikilink">酒精同服</a>）</li>
<li><a href="../Page/過敏.md" title="wikilink">過敏反應</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢唑啉.md" title="wikilink">头孢唑啉</a></p></td>
<td><p>Cefazolin</p></td>
<td><p>Ancef</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/頭孢氨苄.md" title="wikilink">頭孢氨苄</a></p></td>
<td><p>Cefalexin</p></td>
<td><p>Keflex</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢硫脒.md" title="wikilink">头孢硫脒</a></p></td>
<td><p>Cefathiamidine</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢拉定.md" title="wikilink">头孢拉定</a></p></td>
<td><p>Cefradine</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢替唑钠.md" title="wikilink">头孢替唑钠</a></p></td>
<td><p>Ceftezole Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢羟氨苄.md" title="wikilink">头孢羟氨苄</a></p></td>
<td><p>Cefadroxil</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第二代頭孢菌素.md" title="wikilink">第二代頭孢菌素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢克洛.md" title="wikilink">头孢克洛</a></p></td>
<td><p>Cefaclor</p></td>
<td><p>Ceclor</p></td>
<td></td>
<td><ul>
<li>消化道不適及腹瀉</li>
<li>噁心（若與酒精同服）</li>
<li>過敏反應</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/頭孢孟多.md" title="wikilink">頭孢孟多</a></p></td>
<td><p>Cefamandole</p></td>
<td><p>Mandole</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/頭孢西丁.md" title="wikilink">頭孢西丁</a></p></td>
<td><p>Cefoxitin</p></td>
<td><p>Mefoxin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢丙烯.md" title="wikilink">头孢丙烯</a></p></td>
<td><p>Cefprozil</p></td>
<td><p>Cefzil</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/頭孢呋辛酯.md" title="wikilink">頭孢呋辛酯</a></p></td>
<td><p>Cefuroxime Axetil</p></td>
<td><p>Ceftin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢美唑.md" title="wikilink">头孢美唑</a></p></td>
<td><p>Cefmetazole</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢替安.md" title="wikilink">头孢替安</a></p></td>
<td><p>Cefotiam</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢克洛.md" title="wikilink">头孢克洛</a></p></td>
<td><p>Cefaclor</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢尼西.md" title="wikilink">头孢尼西</a></p></td>
<td><p>Cedonicid</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢孟多酯.md" title="wikilink">头孢孟多酯</a></p></td>
<td><p>Cefamandole Nafate</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第三代頭孢菌素.md" title="wikilink">第三代頭孢菌素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢克肟.md" title="wikilink">头孢克肟</a></p></td>
<td><p><a href="../Page/Cefixime.md" title="wikilink">Cefixime</a></p></td>
<td></td>
<td></td>
<td><ul>
<li>消化道不適及腹瀉</li>
<li>噁心（若與酒精同服）</li>
<li>過敏反應</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢地尼.md" title="wikilink">头孢地尼</a></p></td>
<td><p>Cefdinir</p></td>
<td><p>Omnicef</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢妥仑.md" title="wikilink">头孢妥仑</a></p></td>
<td><p>Cefditoren</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢哌酮.md" title="wikilink">头孢哌酮</a></p></td>
<td><p>Cefoperazone</p></td>
<td><p>Cefobid</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢唑肟.md" title="wikilink">头孢唑肟</a></p></td>
<td><p>Cefotaxime</p></td>
<td><p>Claforan</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢泊肟.md" title="wikilink">头孢泊肟</a></p></td>
<td><p>Cefpodoxime</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢他啶.md" title="wikilink">头孢他啶</a></p></td>
<td><p>Ceftazidime</p></td>
<td><p>Fortum</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢布烯.md" title="wikilink">头孢布烯</a></p></td>
<td><p>Ceftibuten</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢唑肟.md" title="wikilink">头孢唑肟</a></p></td>
<td><p>Ceftizoxime</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢曲松.md" title="wikilink">头孢曲松</a></p></td>
<td><p>Ceftriaxone</p></td>
<td><p>Rocephin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢噻肟钠.md" title="wikilink">头孢噻肟钠</a></p></td>
<td><p>Cefotaxime</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢地秦.md" title="wikilink">头孢地秦</a></p></td>
<td><p>Cefodizime</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢甲肟.md" title="wikilink">头孢甲肟</a></p></td>
<td><p>Cefmenoxime</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢匹胺钠.md" title="wikilink">头孢匹胺钠</a></p></td>
<td><p>Cefpiramide Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢布宗.md" title="wikilink">头孢布宗</a></p></td>
<td><p>Cefbuperazone Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢磺啶.md" title="wikilink">头孢磺啶</a></p></td>
<td><p>Cefsulodine Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢唑南钠.md" title="wikilink">头孢唑南钠</a></p></td>
<td><p>Cefuzonam Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢替坦.md" title="wikilink">头孢替坦</a></p></td>
<td><p>Cefotetan</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拉氧头孢.md" title="wikilink">拉氧头孢</a></p></td>
<td><p>Latamoxef</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氟氧头孢钠.md" title="wikilink">氟氧头孢钠</a></p></td>
<td><p>Flomoxef Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢米诺钠.md" title="wikilink">头孢米诺钠</a></p></td>
<td><p>Cefminox Sodium</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢咪唑.md" title="wikilink">头孢咪唑</a></p></td>
<td><p>Cefpimizole</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢他美酯.md" title="wikilink">头孢他美酯</a></p></td>
<td><p>Cefetamet Pivoxil</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢泊肟酯.md" title="wikilink">头孢泊肟酯</a></p></td>
<td><p>Cefpodoxime Proxetil</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢托仑酯.md" title="wikilink">头孢托仑酯</a></p></td>
<td><p>Cefditoren Pivoxil</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢特仑匹酯.md" title="wikilink">头孢特仑匹酯</a></p></td>
<td><p>Cefteram Pivoxil</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第四代頭孢菌素.md" title="wikilink">第四代頭孢菌素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢吡肟.md" title="wikilink">头孢吡肟</a></p></td>
<td><p>Cefepime</p></td>
<td><p>Maxipime</p></td>
<td></td>
<td><ul>
<li>消化道不適及腹瀉</li>
<li>噁心（若與酒精同服）</li>
<li><a href="../Page/過敏反應.md" title="wikilink">過敏反應</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢匹羅.md" title="wikilink">头孢匹羅</a></p></td>
<td><p>Cefpirome</p></td>
<td><p>Cefrom</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/头孢克定.md" title="wikilink">头孢克定</a></p></td>
<td><p>Cefclidin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/头孢噻利.md" title="wikilink">头孢噻利</a></p></td>
<td><p>Cefoselis</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/糖肽类抗生素.md" title="wikilink">糖肽类抗生素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/替考拉宁.md" title="wikilink">替考拉宁</a></p></td>
<td><p>Teicoplanin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/万古霉素.md" title="wikilink">万古霉素</a></p></td>
<td><p>Vancomycin</p></td>
<td><p>Vancocin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大环内酯类抗生素.md" title="wikilink">大环内酯类抗生素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿奇霉素.md" title="wikilink">阿奇霉素</a></p></td>
<td><p>Azithromycin</p></td>
<td><p>Zithromax、Sumamed、Zitrocin</p></td>
<td><p>治疗链球菌感染、<a href="../Page/梅毒.md" title="wikilink">梅毒</a>、呼吸道感染、支原体感染、<a href="../Page/莱姆病.md" title="wikilink">莱姆病</a></p></td>
<td><ul>
<li>噁心、嘔吐及腹瀉（大劑量使用時尤其常見）</li>
<li><a href="../Page/黄疸.md" title="wikilink">黄疸</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克拉霉素.md" title="wikilink">克拉霉素</a></p></td>
<td><p>Clarithromycin</p></td>
<td><p>Biaxin</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地红霉素.md" title="wikilink">地红霉素</a></p></td>
<td><p>Dirithromycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/红霉素.md" title="wikilink">红霉素</a></p></td>
<td><p>Erythromycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/罗红霉素.md" title="wikilink">罗红霉素</a></p></td>
<td><p>Roxithromycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/醋竹桃霉素.md" title="wikilink">醋竹桃霉素</a></p></td>
<td><p>Troleandomycin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/单内酰环类.md" title="wikilink">单内酰环类</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安曲南.md" title="wikilink">安曲南</a></p></td>
<td><p>Aztreonam</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青霉素.md" title="wikilink">青霉素類</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿莫西林.md" title="wikilink">阿莫西林</a></p></td>
<td><p>Amoxicillin</p></td>
<td><p>Novamox</p></td>
<td><p>广谱抗菌、<a href="../Page/链球菌.md" title="wikilink">链球菌感染</a>、<a href="../Page/梅毒.md" title="wikilink">梅毒</a>、莱姆病</p></td>
<td><ul>
<li>胃肠道不适及腹泻</li>
<li>严重过敏</li>
<li>脑和肾脏毒性（很少发生）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氨苄西林.md" title="wikilink">安比西林</a></p></td>
<td><p>Ampicillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿洛西林.md" title="wikilink">阿洛西林</a></p></td>
<td><p>Azlocillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羧苄西林.md" title="wikilink">羧苄西林</a></p></td>
<td><p>Carbenicillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄰氯西林.md" title="wikilink">鄰氯西林</a></p></td>
<td><p>Cloxacillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙氯西林.md" title="wikilink">雙氯西林</a></p></td>
<td><p>Dicloxacillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氟氯西林.md" title="wikilink">氟氯西林</a></p></td>
<td><p>Flucloxacillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美洛西林.md" title="wikilink">美洛西林</a></p></td>
<td><p>Mezlocillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乙氧萘西林.md" title="wikilink">乙氧萘西林</a></p></td>
<td><p>Nafcillinum</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青霉素.md" title="wikilink">青霉素</a>（盘尼西林）</p></td>
<td><p>Penicillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哌拉西林.md" title="wikilink">哌拉西林</a></p></td>
<td><p>Piperacillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/替卡西林.md" title="wikilink">替卡西林</a></p></td>
<td><p>Ticarcillin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多肽類抗生素.md" title="wikilink">多肽類抗生素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杆菌肽.md" title="wikilink">杆菌肽</a></p></td>
<td><p>Bacitracin</p></td>
<td></td>
<td><p>眼、耳和膀胱感染（直接用于眼或肺部吸入，很少用于注射）</p></td>
<td><ul>
<li>肾脏和神经损害（<a href="../Page/注射.md" title="wikilink">注射用</a>）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/粘杆菌素.md" title="wikilink">粘杆菌素</a></p></td>
<td><p>Colistin</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多黏菌素B.md" title="wikilink">多黏菌素B</a></p></td>
<td><p>polymyxinB</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/四環素類抗生素.md" title="wikilink">四環素類抗生素</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地美环素.md" title="wikilink">地美环素</a></p></td>
<td><p>Demeclocycline</p></td>
<td></td>
<td><p><a href="../Page/梅毒.md" title="wikilink">梅毒</a>、<a href="../Page/支原体.md" title="wikilink">支原体感染</a>、<a href="../Page/衣原体.md" title="wikilink">衣原体感染</a>、<a href="../Page/膝立克次氏体.md" title="wikilink">膝立克次氏体感染</a>、莱姆病</p></td>
<td><ul>
<li>胃肠道不适</li>
<li>畏光</li>
<li>四环素牙</li>
<li>妊娠期母婴潜在毒性</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/强力霉素.md" title="wikilink">强力霉素</a></p></td>
<td><p>doxycycline</p></td>
<td><p>Vibramycin</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/米诺环素.md" title="wikilink">米诺环素</a></p></td>
<td><p>Minocycline</p></td>
<td><p>Minocin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/土霉素.md" title="wikilink">土霉素</a></p></td>
<td><p>Oxytetracycline</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四環素.md" title="wikilink">四環素</a></p></td>
<td><p>Tetracycline</p></td>
<td><p>Sumycin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>其他</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胂凡纳明.md" title="wikilink">胂凡纳明</a></p></td>
<td><p>Arsphenamine</p></td>
<td><p>Salvarsan</p></td>
<td><p><a href="../Page/螺旋体感染.md" title="wikilink">螺旋体感染</a>（已淘汰）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯霉素.md" title="wikilink">氯霉素</a></p></td>
<td><p>Chloromycetin</p></td>
<td></td>
<td><p>各种敏感菌（已基本不用）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯潔霉素.md" title="wikilink">氯潔霉素</a>（克林霉素）</p></td>
<td><p>Clinmycin</p></td>
<td><p>Cleocin</p></td>
<td><p>革兰氏阳性菌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乙胺丁醇.md" title="wikilink">乙胺丁醇</a></p></td>
<td><p>Ethambutol</p></td>
<td></td>
<td><p>结核杆菌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磷霉素.md" title="wikilink">磷霉素</a></p></td>
<td><p>Fosfomycin</p></td>
<td></td>
<td><p>革兰阴性菌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夫西地酸.md" title="wikilink">夫西地酸</a></p></td>
<td><p>Fusidic acid</p></td>
<td></td>
<td><p>革兰氏阳性球菌、葡萄球菌（皮肤感染）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/呋喃唑酮.md" title="wikilink">呋喃唑酮</a></p></td>
<td><p>Furazolidone</p></td>
<td></td>
<td><p>革兰氏阴性菌和阳性菌（兽用）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/异烟肼.md" title="wikilink">异烟肼</a></p></td>
<td><p>Isoniazid</p></td>
<td></td>
<td><p>结核杆菌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷奈佐利.md" title="wikilink">雷奈佐利</a></p></td>
<td><p>Linezolid</p></td>
<td><p>Zyvox</p></td>
<td><p>革兰氏阳性菌的严重感染</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甲硝唑.md" title="wikilink">甲硝唑</a></p></td>
<td><p>Metronidazole</p></td>
<td><p>Flagyl</p></td>
<td><p>各种厌氧菌感染</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫匹罗星.md" title="wikilink">莫匹罗星</a></p></td>
<td><p>Mupirocin</p></td>
<td></td>
<td><p>革兰氏阳性球菌（外用皮肤感染）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/呋喃妥因.md" title="wikilink">呋喃妥因</a></p></td>
<td><p>Nitrofurantoin</p></td>
<td><p>Macrodantin、Macrobid</p></td>
<td><p>革兰阳性菌及阴性菌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/平板霉素.md" title="wikilink">平板霉素</a></p></td>
<td><p>Platensimycin</p></td>
<td></td>
<td><p>革兰氏阳性菌（实验中）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吡嗪酰胺.md" title="wikilink">吡嗪酰胺</a></p></td>
<td><p>Pyrazinamide</p></td>
<td></td>
<td><p>结核杆菌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奎奴普丁.md" title="wikilink">奎奴普丁</a>/<a href="../Page/达福普汀.md" title="wikilink">达福普汀</a></p></td>
<td><p>Quinupristin/Dalfopristin</p></td>
<td><p>Syncercid</p></td>
<td><p>革兰阳性菌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/利福平.md" title="wikilink">利福平</a></p></td>
<td><p>Rifampin</p></td>
<td></td>
<td><p>结核分枝杆菌和部分非结核分枝杆菌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大观霉素.md" title="wikilink">大观霉素</a></p></td>
<td><p>Spectinomycin</p></td>
<td></td>
<td><p>淋病奈瑟菌所致尿道、宫颈和直肠感染</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/替利霉素.md" title="wikilink">替利霉素</a></p></td>
<td><p>Telithromycin</p></td>
<td><p><a href="../Page/Ketek.md" title="wikilink">Ketek</a></p></td>
<td><p><a href="../Page/肺炎.md" title="wikilink">肺炎</a></p>
<ul>
<li>视力损害</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

<center>

| [喹诺酮](../Page/喹诺酮.md "wikilink")         |
| ---------------------------------------- |
| [环丙沙星](../Page/环丙沙星.md "wikilink")       |
| [伊诺沙星](../Page/伊诺沙星.md "wikilink")       |
| [加替沙星](../Page/加替沙星.md "wikilink")       |
| [左氧氟沙星](../Page/左氧氟沙星.md "wikilink")     |
| [洛美沙星](../Page/洛美沙星.md "wikilink")       |
| [莫西沙星](../Page/莫西沙星.md "wikilink")       |
| [诺氟沙星](../Page/诺氟沙星.md "wikilink")       |
| [氧氟沙星](../Page/氧氟沙星.md "wikilink")       |
| [曲氟沙星](../Page/曲氟沙星.md "wikilink")       |
| [磺胺類](../Page/磺胺.md "wikilink")          |
| [磺胺米隆](../Page/磺胺米隆.md "wikilink")       |
| [百浪多息](../Page/百浪多息.md "wikilink")       |
| [磺胺醋酰](../Page/磺胺醋酰.md "wikilink")       |
| [磺胺甲二唑](../Page/磺胺甲二唑.md "wikilink")     |
| [磺胺二甲异恶唑](../Page/磺胺二甲异恶唑.md "wikilink") |
| [柳氮磺吡啶](../Page/柳氮磺吡啶.md "wikilink")     |
| [磺胺异恶唑](../Page/磺胺异恶唑.md "wikilink")     |
| [甲氧苄定](../Page/甲氧苄定.md "wikilink")       |
| [复方增效磺胺](../Page/复方增效磺胺.md "wikilink")   |
|                                          |

**人工合成抗菌藥**\[6\]

</center>

## 作用机理

抗生素等抗菌剂有抑菌或杀菌作用，但抗生素對微生物的作用具選擇性，依其作用，可分為廣效或是專一。主要是针对“细菌有而人（或其它高等动植物）没有”的机制进行杀伤，有4大类作用机理：

1.  阻碍[细菌](../Page/细菌.md "wikilink")[细胞壁的合成](../Page/细胞壁.md "wikilink")，导致细菌在低[渗透压环境下溶胀破裂死亡](../Page/渗透压.md "wikilink")，以这种方式作用的抗生素主要是[β-内酰胺类抗生素](../Page/β-内酰胺类抗生素.md "wikilink")。[哺乳动物的细胞没有细胞壁](../Page/哺乳动物.md "wikilink")，不受这类药物的影响。
2.  与细菌[细胞膜相互作用](../Page/细胞膜.md "wikilink")，增强细菌细胞膜的[通透性](../Page/通透性.md "wikilink")、打开膜上的[离子通道](../Page/离子通道.md "wikilink")，让细菌内部的有用物质漏出菌体或电解质平衡失调而死。以这种方式作用的抗生素有[多粘菌素和](../Page/多粘菌素.md "wikilink")[短杆菌肽等](../Page/短杆菌肽.md "wikilink")。
3.  与细菌[核糖体或其反应](../Page/核糖体.md "wikilink")[底物](../Page/底物.md "wikilink")（如[tRNA](../Page/tRNA.md "wikilink")、[mRNA](../Page/mRNA.md "wikilink")）相互所用，抑制[蛋白质的](../Page/蛋白质.md "wikilink")[合成](../Page/不对称合成.md "wikilink")——这意味着细胞存活所必需的[结构蛋白和](../Page/结构蛋白.md "wikilink")[酶不能被合成](../Page/酶.md "wikilink")。以这种方式作用的抗生素包括[四环素类抗生素](../Page/四环素类抗生素.md "wikilink")、[大环内酯类抗生素](../Page/大环内酯类抗生素.md "wikilink")、[氨基糖苷类抗生素](../Page/氨基糖苷类抗生素.md "wikilink")、[氯霉素等](../Page/氯霉素.md "wikilink")。
4.  阻碍细菌[脫氧核醣核酸的](../Page/脫氧核醣核酸.md "wikilink")[复制和](../Page/DNA复制.md "wikilink")[转录](../Page/转录.md "wikilink")，阻碍DNA复制将导致细菌细胞分裂繁殖受阻，阻碍DNA转录成[mRNA则导致后续的mRNA](../Page/mRNA.md "wikilink")[翻译合成蛋白的过程受阻](../Page/翻译_\(遗传学\).md "wikilink")。以这种方式作用的主要是人工合成的抗菌剂[喹诺酮类](../Page/喹诺酮.md "wikilink")（如[氧氟沙星](../Page/氧氟沙星.md "wikilink")）。

與細胞壁或細胞膜作用的兩類抗生素，是以破壞菌體完整性的方式殺死細菌，故可稱為殺菌劑（Bactericidal
agent）；另外兩類抗生素則是靠抑制細菌大分子合成的方式，阻斷其繁殖，故又可稱之為抑菌劑（Bacteriostatic
agent）\[7\]。

## 抗藥性

## 参见

  - [分类药物列表](../Page/分类药物列表.md "wikilink")
  - [合成药物列表](../Page/合成药物列表.md "wikilink")
  - [药物列表](../Page/药物列表.md "wikilink")
  - [抗生素滥用](../Page/抗生素滥用.md "wikilink")
  - [抗菌藥之時間表](../Page/抗菌藥之時間表.md "wikilink")
  - [噬菌體](../Page/噬菌體.md "wikilink")

## 参考文献

## 外部链接

  - [抗生物分子](http://www.ecosci.jp/ab/ab_jmol.html)
  - [正確使用抗生素](http://www.kmuh.org.tw/www/kmcj/data/9108/10.htm)

{{-}}

[Category:抗生素](../Category/抗生素.md "wikilink")
[Category:希腊语外来词](../Category/希腊语外来词.md "wikilink")

1.
2.
3.
4.
5.  Robert Berkow (ed.) *[The Merck Manual of Medical Information - Home
    Edition](../Page/Merck_Manual.md "wikilink")*. Pocket (September
    1999), ISBN 0-671-02727-1.
6.  Robert Berkow (ed.) *[The Merck Manual of Medical Information - Home
    Edition](../Page/Merck_Manual.md "wikilink")*. Pocket (September
    1999), ISBN 0-671-02727-1.
7.