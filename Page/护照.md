[Passport_design_world_map.png](https://zh.wikipedia.org/wiki/File:Passport_design_world_map.png "fig:Passport_design_world_map.png")

**護照**或**護證**是一個[國家或](../Page/國家.md "wikilink")[地區的](../Page/地區.md "wikilink")[政府发放給本國](../Page/政府.md "wikilink")[公民或](../Page/公民.md "wikilink")[國民的一種](../Page/國民.md "wikilink")[旅行](../Page/旅行.md "wikilink")[证件](../Page/证件.md "wikilink")，用於證明持有人的[身分與](../Page/身分標識方式.md "wikilink")[國籍](../Page/國籍.md "wikilink")，以便其出入本國及在外國旅行，同時亦用於請求有關外國當局給予持照人通行便利及保護。

護照關係到在國外所受合法保護的權利與進入本籍國的權利。护照上通常有持有者的[照片](../Page/照片.md "wikilink")、[签名](../Page/签名.md "wikilink")、[出生日期](../Page/出生日期.md "wikilink")、[国籍和其它个人身分的证明](../Page/国籍.md "wikilink")。许多国家正在開發將[生物识别技术用於護照](../Page/生物识别.md "wikilink")，以便能够更精确地确认护照的使用者是其合法持有人。

如今国际间旅行通常要求出示护照，但也有例外的情况。护照实际上仅仅是一种国际认可的用来对旅行者身份鉴定的手段。而这样的鉴定要求在很多情况下或针对某些旅行者可以免除。例如现在[美国公民凭](../Page/美国.md "wikilink")[駕駛執照即可进入](../Page/駕駛執照.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")，而[欧盟国民在欧盟内部旅行也不需要护照](../Page/欧盟.md "wikilink")。

同样的，护照在一个国家内部也可以被当成身份证件而使用。

[國際民航組織已经就护照的标准格式和特点制定了Doc](../Page/國際民航組織.md "wikilink") 9303标准。

最近有许多关于在护照中采用[生物识别技术以改进身份识别安全的讨论](../Page/生物识别.md "wikilink")，美国在[911事件后已经针对](../Page/911.md "wikilink")27个免签证国家要求启用含有此种技术的护照，它是通过内嵌非接触式[芯片来儲存生物信息](../Page/芯片.md "wikilink")。

## 历史

[QingPassport.jpg](https://zh.wikipedia.org/wiki/File:QingPassport.jpg "fig:QingPassport.jpg")護照\]\]
全世界最早出現的護照似乎是在在[公元前](../Page/公元前.md "wikilink")450年，根據《[聖經](../Page/聖經.md "wikilink")》《[尼希米記](../Page/尼希米記.md "wikilink")》二章7節記載：「我又對王說，王若以為美，請賜我詔書，通知大河以西的諸省長准我經過，直到我抵達[猶大](../Page/猶大王國.md "wikilink")」\[1\]。[波斯王發給](../Page/波斯.md "wikilink")[巴勒斯坦的一封詔書](../Page/巴勒斯坦.md "wikilink")，要求沿途官員保障波斯官員的旅行安全。

以[中國历史上為例](../Page/中國.md "wikilink")，不同历史时期有不同的称呼，例如称为[符](../Page/符節.md "wikilink")、传、[过所](../Page/过所.md "wikilink")、公验、[度牒](../Page/度牒.md "wikilink")、路证、[通关文牒](../Page/通关文牒.md "wikilink")、安全证书、通行证、暂住证等。到了[清朝](../Page/清朝.md "wikilink")，根据通行证件的证明身份、提请边境关防检查机关予以“保护”和“关照”、给予通行的便利和必要的协助之功能，将“PASSPORT”一词，于1845年首次译为“护照”，并沿用至今。

在[二战后](../Page/第二次世界大战.md "wikilink")，[國際聯盟](../Page/國際聯盟.md "wikilink")、[联合国和](../Page/联合国.md "wikilink")[国际民航组织](../Page/国际民航组织.md "wikilink")，颁布了在护照的设计和特征的指导标准，这些指导很大方面上形成了现代的护照。近幾年則有MRP（Machine
Readable Passport,
[可機讀護照](../Page/可機讀護照.md "wikilink")）統一規定持照人資料頁在封面內側頁，並有可利用掃描方式讓電腦判讀的資料，以節省移民暨出入國管理人員查驗證照時的時間。

## 類型

  - **普通護照**：发放的对象是普通[公民](../Page/公民.md "wikilink")。
  - **外交護照**：发放的对象是[外交官和外交代表](../Page/外交官.md "wikilink")、或者是根據某些国家法规而发放给该国的公务员。持有外交護照並不自動享有[外交豁免权](../Page/外交豁免权.md "wikilink")。持有外交護照，同时在一些國家會给予不同於非外交護照的[簽證禮遇](../Page/簽證.md "wikilink")。另外，大多數國家的元首到別國進行訪問時，若不持用護照，則會由該國外交部部長簽發「元首通行狀」供元首及其陪同出訪的配偶使用，此通行狀會記載訪問國家的國名及出訪事由，不需事先申請簽證，可獲禮遇免除通關及證照查驗程序，僅會由邀訪國另行蓋印以茲證明，因此在每次出訪前必需簽發一份新的元首通行狀，出訪完成後交由外交部歸檔列為[政府檔案](../Page/政府檔案.md "wikilink")。\[2\]\[3\]。
  - **公務護照**：发放的对象是政府派出執行公務的雇員（或者技术和行政人员），因为他們不具有外交官資格或[維也納公約认可的外交身分](../Page/維也納公約.md "wikilink")。持有公務護照，在一些國家會有不同於普通護照的簽證禮遇。
  - **特殊護照**：发放的对象是高级政府官员和他们的随从，作为对其免除签证要求的官方许可。
  - **集體護照**：在某些情況，如学校团体旅行，同團的所有兒童在旅行期间共用一本集體護照。
  - **[永久居留證](../Page/永久居留證.md "wikilink")**：某些國家發給没有公民身份的居民的身份證明及入境/居留許可。
  - **國內護照**：某些國家為管控本國人口遷徙而發行使用。例如[沙特阿拉伯的iqama](../Page/沙特阿拉伯.md "wikilink")，[中國大陸的](../Page/中國大陸.md "wikilink")[户口登记制度](../Page/中华人民共和国户籍制度.md "wikilink")，还有實行[計劃經濟的](../Page/計劃經濟.md "wikilink")[蘇聯](../Page/蘇聯.md "wikilink")，是一種為監控人口流動情況而發行的內部護照。
  - **加急護照**或**临时護照**：发放给那些原先护照遗失或失窃但急需出国旅行的人。
  - **商务護照**：发放给那些频繁旅行的人，这种护照的页数较普通护照的多。

[UN-laissez-passer.jpg](https://zh.wikipedia.org/wiki/File:UN-laissez-passer.jpg "fig:UN-laissez-passer.jpg")

  - **[聯合國通行證](../Page/聯合國护照.md "wikilink")**（Laissez-passer）：[联合国和各专门机构根据](../Page/联合国.md "wikilink")《联合国特权和豁免公约》和《专门机构特权和豁免公约》向本机构的职员颁发一种具有护照效力的身份证件。有红皮通行证和蓝皮通行证两种，其效力分别相当于外交护照和公务护照。「Laissez-passer」是[法文](../Page/法文.md "wikilink")「請予通行」之意（註：聯合國部分機構設立在使用法文的[巴黎和](../Page/巴黎.md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")，以及法國之[常任理事國地位](../Page/常任理事國.md "wikilink")，法文亦為聯合國官方語言之一。）。
  - **家庭護照**：是一种只发放给家庭（家长和儿童）而不是个人的护照，美国政府已经不再发放此种护照。
  - **[难民旅行证](../Page/难民旅行证.md "wikilink")**：某些国家的政府根据1951年7月28日联合国《关于难民地位的公约》第28条及1967年1月31日《难民地位议定书》的规定颁发给合法在其领土内居留的难民的旅行证，供持证人到发证国领土以外旅行。

## 護照排行表

<table>
<caption>2019亨利護照指數 (<a href="../Page/Henley_Passport_Index.md" title="wikilink">Henley Passport Index</a>)</caption>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>免簽国家(個)</p></th>
<th><p>地区排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>190</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>189</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>188</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>187</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>186</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>185</p></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>184</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>183</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>182</p></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>181</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>180</p></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>179</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>169</p></td>
<td><p>19</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>149</p></td>
<td><p>29</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>74</p></td>
<td><p>69</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>30</p></td>
<td><p>104[4]</p></td>
</tr>
</tbody>
</table>

## 国际民航组织标准

[国际民航组织](../Page/国际民航组织.md "wikilink")(ICAO)提供了护照规格的标准化建议，如今被大部分国家政府所采用。护照的大小需要符合
ISO/IEC 7810 标准，即 125x88 毫米，B7 规格。

## 特別情況

君主制国家的君主一般无护照，如[英女王](../Page/英女王.md "wikilink")[伊莉莎白二世不需要護照和簽證就可以自由進出各個國家](../Page/伊莉莎白二世.md "wikilink")，因為英國護照是以英國女王陛下的名義簽發的，因此女王本人不需要護照。

### 國際旅行不需護照的情況

护照通常在国际旅游中都必须要用到，通常在跨国与国的边境时需要使用。但是有些时候，某些国家的公民出境到另外一个国家时，由于国与国订立了某些契约（如[欧盟](../Page/欧盟.md "wikilink")），可能就无需出示护照，但可能会以其他身份证明文件来证明身份，如驾驶执照，个人身份证，护照上的签证。

### 國內旅行需要護照的情況

某些國家，例如實行或曾實行[計劃經濟的國家](../Page/計劃經濟.md "wikilink")，為了監控人口流動或管控人民遷徙，要求人民在國內旅行或遷徙時，申請並持有國內護照。例如蘇聯。

由於[英國的](../Page/英國.md "wikilink")[殖民地與國籍法複雜](../Page/殖民地.md "wikilink")，在[大不列顛與](../Page/大不列顛.md "wikilink")[屬地之間](../Page/屬地.md "wikilink")，或英國各屬地之間旅行，也常需要護照。

1963年[沙巴和](../Page/沙巴.md "wikilink")[砂拉越与](../Page/砂拉越.md "wikilink")[新加坡和](../Page/新加坡.md "wikilink")[马来亚共同参组](../Page/马来亚.md "wikilink")[馬來西亞聯邦時](../Page/馬來西亞聯邦.md "wikilink")，依據各方事前同意的決議，[沙巴和](../Page/沙巴.md "wikilink")[砂拉越兩州可保有原先獨立的邊境管制](../Page/砂拉越.md "wikilink")。[西馬來西亞人民進入沙巴或砂拉越](../Page/西馬來西亞.md "wikilink")，以及兩州之間相互旅行，均需檢查護照，但沙巴、砂拉越人民進入[西馬來西亞時則不需檢查護照](../Page/西馬來西亞.md "wikilink")。

###  中國大陸

中國大陆居民前往[香港或](../Page/香港特别行政区.md "wikilink")[澳門](../Page/澳门特别行政区.md "wikilink")，需要[往來港澳通行证](../Page/往來港澳通行证.md "wikilink")（俗稱雙程證）或者[前往港澳通行证](../Page/前往港澳通行证.md "wikilink")（俗稱單程證，前往港澳地區定居使用）；定居外国的中国大陆居民而无往来港澳通行证者，需要在中国驻外领事机构申请进入许可；中国大陆居民经港澳过境至其他国家者，可用[中華人民共和國護照](../Page/中华人民共和国护照.md "wikilink")，在港澳逗留时间各不超过7天。

依據[中華人民共和國憲法](../Page/中華人民共和國憲法.md "wikilink")，中华人民共和国主張[台灣地区](../Page/台灣地区.md "wikilink")（包含[台澎金馬](../Page/台澎金馬.md "wikilink")）是中華人民共和國一部分。中華人民共和國[大陆地区公民前往臺灣地区](../Page/中国大陆.md "wikilink")，亦須持用替代護照的「[大陸居民往來台灣通行證](../Page/大陸居民往來台灣通行證.md "wikilink")」在中華人民共和國（中國大陸）出境，並持由[中华民国内政部移民署签发的](../Page/中华民国.md "wikilink")「[中華民國台灣地區入出境許可證](../Page/中華民國台灣地區入出境許可證.md "wikilink")」入境。

###  香港及澳門

根據[香港特別行政區基本法及](../Page/香港特別行政區基本法.md "wikilink")[澳門特別行政區基本法](../Page/澳門特別行政區基本法.md "wikilink")，有簽發護照、進行入境管控、與外國訂定單獨簽證協定等權力。[中國內地](../Page/中國內地.md "wikilink")、[香港特別行政區](../Page/香港特別行政區.md "wikilink")、[澳門特別行政區之間有各自的邊境控制](../Page/澳門特別行政區.md "wikilink")。

香港居民進入澳門，需於兩地邊防出示香港居民身份證（有'\*','\*\*\*','R'的[香港居民身份證](../Page/香港居民身份證.md "wikilink")）或[回港證](../Page/回港證.md "wikilink")。

澳門居民進入香港，則需於兩地邊防出示澳門居民往來香港特別行政區旅遊證（非永久性居民及其他人士）或澳門永久性居民身份證。

港澳居民中的中国公民前往中國大陸，則不得持港澳特区護照，除需於港澳邊防出示上述港澳地區的證件外，還需要於中國邊防出示「[港澳居民來往內地通行證](../Page/港澳居民來往內地通行證.md "wikilink")（俗稱回鄉證）」（舊為「[港澳同胞回鄉證](../Page/港澳同胞回鄉證.md "wikilink")」）或「中華人民共和國旅行證」或「[中華人民共和國出入境通行證](../Page/中華人民共和國出入境通行證.md "wikilink")」。

###  中華民國

[中華民國自](../Page/中華民國.md "wikilink")[退守台湾後的管轄領土僅治及于](../Page/国共内战.md "wikilink")[台澎金馬地區](../Page/台澎金馬地區.md "wikilink")（包含[臺灣省及](../Page/臺灣省.md "wikilink")[福建省部分地區](../Page/福建省_\(中華民國\).md "wikilink")，即包含[澎湖](../Page/澎湖.md "wikilink")、[金門和](../Page/金門.md "wikilink")[馬祖](../Page/馬祖.md "wikilink")）、[東沙群島和](../Page/東沙群島.md "wikilink")[南沙群島及其它島嶼](../Page/南沙群島.md "wikilink")，而[中華民國憲法及其](../Page/中華民國憲法.md "wikilink")[附屬條款上的](../Page/中華民國憲法增修條文.md "wikilink")[領土仍包含](../Page/中華民國領土.md "wikilink")[大陆地区](../Page/中國大陸.md "wikilink")、台湾地区以及[蒙古国](../Page/蒙古国.md "wikilink")。

而[中華民國](../Page/中華民國.md "wikilink")、[中華人民共和國與](../Page/中華人民共和國.md "wikilink")[蒙古國三方之間存在各自獨立的](../Page/蒙古國.md "wikilink")[法制与边境管制](../Page/法制.md "wikilink")。值得一提的是，中華民國與中華人民共和國均不認可對方護照為有效國際旅行證件，不在對方護照上注記或蓋章。

中華民國國民前往中國大陸，須持用「[台灣居民來往大陸通行證](../Page/台灣居民來往大陸通行證.md "wikilink")」或「[中華人民共和國旅行證](../Page/中華人民共和國旅行證.md "wikilink")」於中華人民共和國（中國大陸）口岸入境。

## 参见

  - [签证](../Page/签证.md "wikilink")
  - [护照列表](../Page/护照列表.md "wikilink")
  - [生物特徵護照](../Page/生物特徵護照.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 延伸阅读

  -
[护照](../Category/护照.md "wikilink")

1.  <http://www.recoveryversion.com.tw/Style0A/026/read_List.php?f_BookNo=16&f_ChapterNo=2>
2.
3.
4.  <https://www.henleypassportindex.com/global-ranking>