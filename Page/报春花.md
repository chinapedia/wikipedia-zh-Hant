[Hong_Kong_Flower_Show_2011_(Primula).JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_Flower_Show_2011_\(Primula\).JPG "fig:Hong_Kong_Flower_Show_2011_(Primula).JPG")

**报春花**（[學名](../Page/學名.md "wikilink")：**''Primula
malacoides**''）又名**樱花草**，是[报春花科](../Page/报春花科.md "wikilink")[报春花属的植物](../Page/报春花属.md "wikilink")。多生长于荒野、田边，原产中国的[云南](../Page/云南.md "wikilink")、[贵州](../Page/贵州.md "wikilink")，各地栽培，颇具观赏性。

報春花適宜盆栽，某些品種可在較溫暖的地區栽種於岩石園或花壇，少數較大型的品種，還可作砌花用\[1\]。

## 形态

一年生[草本](../Page/草本.md "wikilink")。[叶基生](../Page/叶.md "wikilink")，长[卵形](../Page/卵.md "wikilink")，顶端圆钝，基部楔形或心形，边缘有不整齐具细锯齿状的缺裂；秋季开花，[花为](../Page/花.md "wikilink")[碟状](../Page/碟.md "wikilink")，淡[紫色或](../Page/紫色.md "wikilink")[红色](../Page/红色.md "wikilink")；[伞状](../Page/伞.md "wikilink")[花序](../Page/花序.md "wikilink")2-4轮，球形[蒴果](../Page/蒴果.md "wikilink")。

## 資料來源

  -
## 扩展阅读

  -
[lbe:Ччиккул тIутIи](../Page/lbe:Ччиккул_тIутIи.md "wikilink")

[Category:草本植物](../Category/草本植物.md "wikilink")
[malacoides](../Category/報春花屬.md "wikilink")

1.