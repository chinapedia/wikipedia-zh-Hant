**押上美猫**，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")，[長崎縣](../Page/長崎縣.md "wikilink")[五島市出身](../Page/五島市.md "wikilink")。1989年以短篇「」出道。代表作是《[皇龍騎士團](../Page/皇龍騎士團.md "wikilink")》。作品大多於日本[新書館](../Page/新書館.md "wikilink")[月刊Wings連載](../Page/月刊Wings.md "wikilink")。

## 作品

  - 《皇龍騎士團》系列
    1.  [皇龍騎士團](../Page/皇龍騎士團.md "wikilink")、全26冊+外傳1冊（大然、青文、中文版未有外傳）、原名《》
    2.  祝福的黒與破滅的白、1冊待續（連載中）、原名《》\[1\]
  - [月華佳人](../Page/月華佳人.md "wikilink")、5冊待續、原名《》
  - [ASUKA2](../Page/ASUKA2.md "wikilink")、1冊、原名《》
  - [尋找傳說中的國王](../Page/尋找傳說中的國王.md "wikilink")、全1冊、原名《》
  - [爸爸是壞蛋](../Page/爸爸是壞蛋.md "wikilink")、全1冊、原名《》
  - [草莓宮殿](../Page/草莓宮殿.md "wikilink")、1冊、原名《Strawberry Palace》
  - [陣取合戰](../Page/陣取合戰.md "wikilink")、3冊待續（東立）

## 參考資料

## 外部連結

  - [](http://min.pussycat.jp/)

  - [](http://talent.yahoo.co.jp/pf/detail/pp230011)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:長崎縣出身人物](../Category/長崎縣出身人物.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")

1.