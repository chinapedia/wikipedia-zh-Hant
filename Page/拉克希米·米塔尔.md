**拉克希米·米塔尔**（；，）是一位以[倫敦為基地的](../Page/倫敦.md "wikilink")[印度](../Page/印度.md "wikilink")[亿万富翁及](../Page/亿万富翁.md "wikilink")[工業家](../Page/工業家.md "wikilink")，在印度[拉賈斯坦邦](../Page/拉賈斯坦邦.md "wikilink")[楚盧縣的](../Page/楚盧縣.md "wikilink")[Sadulpur村出生](../Page/Sadulpur.md "wikilink")，現時於倫敦[肯辛頓定居](../Page/肯辛頓.md "wikilink")。根據《[福布斯](../Page/福布斯.md "wikilink")》雜誌的報導，他現時的財富總和高達320億美元，在世界富豪榜排行第五，是全英國最富有的人、及全世界最富有的印度人\[1\]\[2\]，《[英國金融時報](../Page/英國金融時報.md "wikilink")》奉米塔爾為2006年度的風雲人物。2007年5月，他成為了《[時代雜誌](../Page/時代雜誌.md "wikilink")》(Time
Magazine)的一百位最具影響力的人物。

## 早年生活

米塔爾的童年與他祖父一家人一起生活，住在由祖父興建的房屋裡。當時他們一家生活貧困，每晚也只是睡在[繩床上](../Page/繩床.md "wikilink")。後來，他的父親Mohan幸運地成為了一家鋼鐵廠的股東，使一家人的幸福得到大大的改善。
1976年，米塔爾家族成立自己的家庭造鋼事業，拉克希米·米塔尔亦在此時加入，並透過收購一家位於[印尼的鋼鐵生產線](../Page/印尼.md "wikilink")，為家族生意創立其國際部門。不久，他與家鄉一位放債人的女兒Usha結婚。1994年，由於與父親及兄弟意見不合，他透過一直掌控在其中的國際業務成立一家獨立的公司[米塔尔钢铁公司](../Page/米塔尔钢铁公司.md "wikilink")（Mittal
Steel Co.）。

## 收购阿赛洛

[安賽樂米塔爾鋼鐵公司](../Page/安賽樂米塔爾.md "wikilink")（Arcelor
Mittal），是现今全球规模最大的钢铁制造集团。在2006年[恶意收购了欧洲最大的钢铁集团阿赛洛后](../Page/恶意收购.md "wikilink")，其规模超越佔第二位的“[新日本製鐵](../Page/新日本製鐵.md "wikilink")”（因为具有先进技术，被认为是下一个恶意收购的对象）、第三位的“韩国浦项制铁”及第四位的总和还多。

钢铁业为国家的支柱产业。以日本为例，钢铁业支撑着日本的整个汽车工业。米塔尔恶意收购阿赛洛的事实让很多界内人士大为震惊。日本相关单位也引起了很大重视，意在防止阿赛洛的不幸再一次发生。

中日韩三国曾呼吁组成亚洲钢铁联盟，以迎接米塔尔挑战。\[3\]

2007年，阿赛洛·米塔尔收了[中國東方集團](../Page/中國東方集團.md "wikilink")73%的股權。

## 慈善事業

在目睹了[印度只贏得一塊獎牌](../Page/印度.md "wikilink")（[銅牌](../Page/銅牌.md "wikilink")）在[2000年夏季奧林匹克運動會之上](../Page/2000年夏季奧林匹克運動會.md "wikilink")，以及一面獎牌（[銀牌](../Page/銀牌.md "wikilink")）在[2004年夏季奧林匹克運動會之後](../Page/2004年夏季奧林匹克運動會.md "wikilink")，米塔爾決定設立4億[盧比](../Page/盧比.md "wikilink")（9百萬美元）的「米塔爾冠軍信託基金」（[Mittal
Champions
Trust](../Page/:en:Mittal_Champions_Trust.md "wikilink")）去支持贊助10位具有世界級潛力的印度運動員。\[4\]在2008年，米塔爾頒給[阿比納夫·賓德拉](../Page/阿比納夫·賓德拉.md "wikilink")1,500萬盧比（333,000美金），獎勵他在射擊項目中獲得印度**第一面**[奧運個人項目金牌](../Page/奧林匹克運動會.md "wikilink")（上一面印度的金牌是1980年奧運會男子曲棍球團體金牌）。

## 米塔尔家庭生活

米塔爾目前居住在[倫敦](../Page/倫敦.md "wikilink")[Kensington Palace
Gardens上第](../Page/:en:Kensington_Palace_Gardens.md "wikilink")18-19號，這棟宅第是2004年他用5千7百萬[英鎊](../Page/英鎊.md "wikilink")（28.5億[新台幣](../Page/新台幣.md "wikilink")）從[一級方程式賽車老闆](../Page/一級方程式賽車.md "wikilink")[Bernie
Ecclestone手中買下的](../Page/:en:Bernie_Ecclestone.md "wikilink")，在當時破了天價成為全球最貴的豪宅。\[5\]米塔爾在[倫敦肯辛頓區的住所使用的大理石裝潢](../Page/倫敦.md "wikilink")，是來自於與搭建[印度](../Page/印度.md "wikilink")[泰姬瑪哈陵](../Page/泰姬瑪哈陵.md "wikilink")（Taj
Mahal）相同的[採石場](../Page/採石場.md "wikilink")。如此奢侈的財富展現也被稱為「泰姬米塔爾陵」（Taj
Mittal）。\[6\]這座房子有12個房間、一個室內游泳池、土耳其澡堂、以及20個停車位。\[7\]

米塔爾花了1億1千7百萬英鎊（58.5億新台幣）買下肯辛頓公園Palace Greens上6號的宅第給他的兒子[Aditya
Mittal](../Page/:en:Aditya_Mittal.md "wikilink")，這棟宅第之前是金融家[Noam
Gottesman所擁有](../Page/:en:Noam_Gottesman.md "wikilink")，Aditya
Mittal的妻子[Megha
Mittal是德國時尚奢侈品牌](../Page/:en:Megha_Mittal.md "wikilink")[Escada的主席與董事](../Page/:en:Escada.md "wikilink")。阿蒂亚·米塔尔（Aditya
Mittal）是Arcelor Mittal的首席财富官（CFO）。

米塔爾在2008年花了7千萬英鎊（35億新台幣）買下肯辛頓公園Palace Greens上9A號的宅第給他的女兒[Vanisha Mittal
Bhatia](../Page/:en:Vanisha_Mittal.md "wikilink")，這棟宅第之前是[菲律賓大使館](../Page/菲律賓.md "wikilink")，Vanisha
Mittal Bhatia的丈夫[Amit
Bhatia是一位商人與](../Page/:en:Amit_Bhatia.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")。兩人结婚时，花费约价值460多万人民币包下整个凡尔赛宫。

米塔爾擁有三棟頂級房產總值5億英鎊（250億新台幣）在俗稱「億萬富豪之路」（Billionaire's Row）的Kensington
Palace Gardens之上。\[8\]

米塔爾也擁有一棟叫做Summer Palace的宅第，位於[The Bishops
Avenue](../Page/:en:The_Bishops_Avenue.md "wikilink")
46B號，這條路被稱為「百萬富豪之路」（Millionares
Row）而且據報導售價約為4千萬英鎊。

## 參考

<references />

## 外部連結

  -
[Category:印度企業家](../Category/印度企業家.md "wikilink")
[Category:印度億萬富豪](../Category/印度億萬富豪.md "wikilink")
[Category:在英國的印度人](../Category/在英國的印度人.md "wikilink")
[Category:加爾各答大學校友](../Category/加爾各答大學校友.md "wikilink")
[Category:印度印度教徒](../Category/印度印度教徒.md "wikilink")
[Category:拉賈斯坦邦人](../Category/拉賈斯坦邦人.md "wikilink")
[Category:印度素食主義者](../Category/印度素食主義者.md "wikilink")

1.  <http://www.forbes.com/lists/2007/10/07billionaires_Lakshmi-Mittal_R0YG.html>
2.  [Foreigners dominate UK rich
    list](http://news.bbc.co.uk/2/hi/business/6571269.stm) BBC
    News，2007年4月19日
3.  [迎接米塔尔挑战
    中日韩钢铁巨头可能联手](http://business.sohu.com/20061204/n246784088.shtml)
4.  [DNA - Sport - Mittal’s Olympic dream is worth Rs 40 crore - Daily
    News & Analysis](http://dnaindia.com/report.asp?NewsID=9054)
5.
6.  [Takeover Week: Billionaires Row (Rob) - Google
    Sightseeing](http://googlesightseeing.com/2007/07/13/takeover-week-billionaires-row-rob/)
7.
8.  [The Mittal Monopoly: Britains richest man buys
    property](http://www.dailymail.co.uk/news/article-1028725/The-Mittal-Monopoly-Britains-richest-man-buys-property-Billionaires-Row.html)