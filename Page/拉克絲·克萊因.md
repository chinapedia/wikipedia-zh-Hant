**拉克絲·克萊因**（[拉丁字母](../Page/拉丁字母.md "wikilink")：**Lacus
Clyne**）是在[动画片](../Page/动画片.md "wikilink")[机动战士GUNDAM
SEED的女主角](../Page/机动战士GUNDAM_SEED.md "wikilink")，她也在续作[机动战士GUNDAM
SEED
DESTINY中作为主要人物出现](../Page/机动战士GUNDAM_SEED_DESTINY.md "wikilink")。为她担当配音的是声优[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")，田中理惠也因此角色而入圍第1回聲優獎。\[1\]

## 名字

拉克絲·克萊因的名字（Lacus）在[拉丁语中的意思是](../Page/拉丁语.md "wikilink")[湖](../Page/湖.md "wikilink")。在DESTINY中假扮拉克絲的[蜜雅（MEER）呼应着拉克絲這個名字](../Page/米娅·坎贝尔.md "wikilink")，因为[蜜雅（MEER）在](../Page/米娅·坎贝尔.md "wikilink")[荷兰语中也是](../Page/荷兰语.md "wikilink")“湖”的意思，[蜜雅（MEER）同时也是德语中的](../Page/米娅·坎贝尔.md "wikilink")“海”，或者“洋”的意思。

## 剧情

### 第一季

拉克絲·克萊因被稱為[P.L.A.N.T的歌姬和](../Page/P.L.A.N.T.md "wikilink")“粉色的公主”、“白色的皇后”。札夫特議會原議長[西蓋爾·克萊因之女](../Page/希捷尔·克萊因.md "wikilink")，此外還是在P.L.A.N.T有名的偶像歌手。SEED前期是[阿斯蘭·薩拉的未婚妻](../Page/阿斯蘭·薩拉.md "wikilink")，在身為尤尼烏斯7號追悼團代表視察時因意外坐上逃生艇漂流在宇宙中被[煌·大和所救](../Page/煌·大和.md "wikilink")，在[Z.A.F.T殲滅先遣隊後攻擊大天使號時被當成人質迫使](../Page/Z.A.F.T..md "wikilink")[Z.A.F.T軍撤退](../Page/Z.A.F.T..md "wikilink")，在得知[煌和](../Page/煌·大和.md "wikilink")[阿斯蘭明明是朋友但卻得互相戰鬥時為他們感到難過](../Page/阿斯蘭·薩拉.md "wikilink")，後來被[煌私自交還給](../Page/煌·大和.md "wikilink")[阿斯蘭後平安回到](../Page/阿斯蘭·薩拉.md "wikilink")[P.L.A.N.T](../Page/P.L.A.N.T.md "wikilink")，在[阿斯蘭休假到她家玩時向](../Page/阿斯蘭·薩拉.md "wikilink")[阿斯蘭提及](../Page/阿斯蘭·薩拉.md "wikilink")[煌還保留當初和](../Page/煌·大和.md "wikilink")[阿斯蘭分離時送給他的機械鳥以及當時在大天使號上和](../Page/阿斯蘭·薩拉.md "wikilink")[煌談及](../Page/煌·大和.md "wikilink")[阿斯蘭時的情況](../Page/阿斯蘭·薩拉.md "wikilink")，最後還說出很喜歡[煌這個人](../Page/煌·大和.md "wikilink")。

SEED後期幫助因為和[阿斯蘭戰鬥而受傷的](../Page/阿斯蘭·薩拉.md "wikilink")[煌養傷](../Page/煌·大和.md "wikilink")，之後因為[煌所抱持的理念與自己相同而帶領](../Page/煌·大和.md "wikilink")[煌·大和前往駕駛](../Page/煌·大和.md "wikilink")[Z.A.F.T研發的新機體](../Page/Z.A.F.T..md "wikilink")[自由鋼彈](../Page/ZGMF-X10A_Freedom.md "wikilink")，因為將[自由鋼彈交給](../Page/ZGMF-X10A_Freedom.md "wikilink")[煌時被監視器拍到所以拉克絲被視為叛國賊](../Page/煌·大和.md "wikilink")，與[阿斯蘭·薩拉的婚約也因此取消](../Page/阿斯蘭·薩拉.md "wikilink")，全家人也被阿斯蘭的父親派屈克[·薩拉通緝](../Page/阿斯蘭·薩拉.md "wikilink")，之後利用哈囉引導[阿斯蘭暗中和自己見面](../Page/阿斯蘭·薩拉.md "wikilink")，她告訴[阿斯蘭她把](../Page/阿斯蘭·薩拉.md "wikilink")[自由鋼彈交給](../Page/ZGMF-X10A_Freedom.md "wikilink")[煌還有](../Page/煌·大和.md "wikilink")[煌還活著讓](../Page/煌·大和.md "wikilink")[阿斯蘭對自己的信念產生質疑](../Page/阿斯蘭·薩拉.md "wikilink")，最後告知[煌人在地球希望他能和](../Page/煌·大和.md "wikilink")[煌以朋友的身份好好談談後離開並繼續逃亡](../Page/煌·大和.md "wikilink")，在逃亡時仍透過廣播向大家傳達自己的意志，後來她的父親[西蓋爾·克萊因在逃亡途中被殺害](../Page/希捷尔·克莱因.md "wikilink")，之後救出為了詢問父親對戰爭看法而被捕的[阿斯蘭](../Page/阿斯蘭·薩拉.md "wikilink")。救出[阿斯蘭後拉克絲和安德列](../Page/阿斯蘭·薩拉.md "wikilink")·渥特菲德等克萊因派的成員搭乘[永恆號逃離](../Page/永恆號.md "wikilink")[P.L.A.N.T](../Page/P.L.A.N.T.md "wikilink")，在被雅金‧杜威防衛隊攻擊時被[煌駕駛的](../Page/煌·大和.md "wikilink")[自由鋼彈所救並和大天使號還有草薙號相會](../Page/ZGMF-X10A_Freedom.md "wikilink")，後來因為父親的死而衝向[煌的懷裡哭泣](../Page/煌·大和.md "wikilink")，之後以[永恆號艦長的身份和](../Page/永恆號.md "wikilink")[煌·大和等人組成三艦同盟投身於戰場進行武裝和平運動](../Page/煌·大和.md "wikilink")。

在[煌因為知道自己的身世感到難過和痛苦時認同了](../Page/煌·大和.md "wikilink")[煌的存在](../Page/煌·大和.md "wikilink")
。
並希望[煌能留在自己身邊而讓](../Page/煌·大和.md "wikilink")[煌重新振作](../Page/煌·大和.md "wikilink")，最後開戰前將自己戴的戒指交給[煌並希望](../Page/煌·大和.md "wikilink")[煌能平安回來](../Page/煌·大和.md "wikilink")，最終戰時指揮[永恆號參與戰鬥](../Page/永恆號.md "wikilink")，並和草薙號一起去摧毀創世紀，之後被拉烏·魯·克魯澤駕駛的天帝鋼彈攻擊時被憤怒的[煌所救](../Page/煌·大和.md "wikilink")，最後戰爭在創世紀爆炸的情況下結束。

### 第二季

拉克絲在戰後和[煌·大和隱居在歐普近海的島嶼幫忙馬爾奇歐導師照顧戰爭孤兒](../Page/煌·大和.md "wikilink")，在尤尼烏斯7號墜落時與[煌及](../Page/煌·大和.md "wikilink")[煌的養母雁田大和還有馬爾奇歐導師和照顧的孩子們一起在防空洞避難並唱歌安撫害怕的孩子們](../Page/煌·大和.md "wikilink")，之後因為尤尼烏斯7號墜毀導致居住地全毀，拉克絲和[煌以及馬爾奇歐](../Page/煌·大和.md "wikilink")、雁田大和等人與孩子們一起搬到在阿斯哈名下的別墅生活的瑪琉等人那裡居住，在歐普決定跟大西洋聯邦簽約而考慮要搬去P.L.A.N.T生活的晚上遭到調整者的特殊部隊襲擊，在特殊部隊的MS亞修的攻擊下，拉克絲對於要讓[煌再次駕駛](../Page/煌·大和.md "wikilink")[自由鋼彈戰鬥這件事感到猶豫不決](../Page/ZGMF-X10A_Freedom.md "wikilink")，之後在[煌為了保護大家的覺悟下把打開](../Page/煌·大和.md "wikilink")[自由鋼彈機庫的鑰匙交給](../Page/ZGMF-X10A_Freedom.md "wikilink")[煌](../Page/煌·大和.md "wikilink")，最後暗殺計畫因被[自由鋼彈擊敗而導致失敗](../Page/ZGMF-X10A_Freedom.md "wikilink")，之後為了避免上次戰爭的悲劇再度重演和[煌·大和等人再次回到了戰場](../Page/煌·大和.md "wikilink")。

為了調查杜蘭朵議長的計畫和[煌等人分開行動](../Page/煌·大和.md "wikilink")，為了前往宇宙與安德列·渥特菲德搶奪穿梭機，在穿梭機被追擊的MS攻擊時被[自由鋼彈即使解救並阻止](../Page/ZGMF-X10A_Freedom.md "wikilink")[煌跟他們前往宇宙](../Page/煌·大和.md "wikilink")，在分離前向[煌保證會回到他的身邊](../Page/煌·大和.md "wikilink")，最後在[煌的目送下離開了地球](../Page/煌·大和.md "wikilink")，在殖民地孟德爾衛星中發現杜蘭朵實施的命運計畫的筆記本。後來因為被札夫特軍發現而被追擊，在危急時被駕駛嫣紅攻擊鋼彈的[煌所救](../Page/煌·大和.md "wikilink")，為了擋下攻擊永恆號的光束嫣紅攻擊鋼彈失去了所有武器，之後拉克絲將新的MS
[ZGMF-X20A
攻擊自由鋼彈給予前來救援的](../Page/ZGMF-X20A_Strike_Freedom.md "wikilink")[煌](../Page/煌·大和.md "wikilink")，最後成功擊退追擊的札夫特軍，在札夫特攻打歐普時駕駛ZGMF-X19A
無限正義鋼彈和[煌從宇宙降下並把機體交給](../Page/煌·大和.md "wikilink")[阿斯蘭](../Page/阿斯蘭·薩拉.md "wikilink")，當歐普保衛戰結束後[卡佳里透過全球轉播向全世界傳達歐普的立場和質問杜蘭朵前陣子的侵略行為時被假冒拉克絲的蜜雅妨礙](../Page/卡佳里·尤拉·阿斯哈.md "wikilink")，為了幫助[卡佳里](../Page/卡佳里·尤拉·阿斯哈.md "wikilink")，拉克絲在[煌的護送下到歐普行政府表露身分](../Page/煌·大和.md "wikilink")，揭穿蜜雅是假冒的，同時也希望大家思考杜蘭朵議長的目的以及真面目，讓世界上的人們開始質疑杜蘭朵。

在哥白尼與[煌](../Page/煌·大和.md "wikilink")、[阿斯蘭還有美鈴購物時收到蜜雅](../Page/阿斯蘭·薩拉.md "wikilink")·坎貝爾的紅色哈囉帶來的一張便條，內容是「拉克絲小姐，請你救救我！我將會被殺！」雖然[阿斯蘭和美鈴相信這是一個陷阱](../Page/阿斯蘭·薩拉.md "wikilink")，但拉克絲無論如何都決定要去，她認為蜜雅請求自己的幫助。最後在[煌的同意和幫忙說服](../Page/煌·大和.md "wikilink")[阿斯蘭和美鈴的情況下決定一起陪同拉克絲去見蜜雅](../Page/阿斯蘭·薩拉.md "wikilink")。蜜雅在空的圓形露天劇場會見拉克絲等人，莎拉帶領的暗殺部隊圍繞著等待伏擊。蜜雅威脅著要開槍射傷拉克絲，但被阿斯蘭阻止了，射飛了蜜雅手上的槍。蜜雅堅持自己就是拉克絲·克萊因，拉克絲嘗試使蜜雅冷靜下來，告訴她即使現在樣子和聲音很像自己，但蜜雅和自己仍然是兩個不同的人，任何人也無法代替對方，自己的夢想是屬於自己的。拉克絲不知情地進入了莎拉的視線，但煌的機械鳥提醒他們注意莎拉，之後槍聲接連不斷，當[阿斯蘭幹掉大部分暗殺部隊人員的時候](../Page/阿斯蘭·薩拉.md "wikilink")，莎拉投擲了一個手榴彈出來，但被[煌和美鈴將之射回給莎拉](../Page/煌·大和.md "wikilink")，莎拉重傷倒下。當所有槍響結束後，穆·拉·福拉卡駕駛曉鋼彈到達了，當拉克絲登上曉鋼彈的手上時，[煌邀請蜜雅跟他們一起逃跑](../Page/煌·大和.md "wikilink")，蜜雅也接受了。不過，瀕死的莎拉開槍射向拉克絲，唯一發現這件事的蜜雅設法推開拉克絲，但被子彈擊中了。[阿斯蘭馬上開槍射殺莎拉](../Page/阿斯蘭·薩拉.md "wikilink")。最後被憧憬的拉克絲，和寄予感情的阿斯蘭，二人一邊看護，安靜地一邊斷氣。感到極度痛苦的蜜雅，給了拉克絲一張自己接受整形手術前的照片，並向拉克絲道歉，懇求拉克絲不要忘記自己的歌曲和生命，熱淚湧上蜜雅的眼眶，她就這樣死在拉克絲的手臂上。之後把她的屍體帶到大天使號上，在那裡為她舉行了一個簡短而嚴肅的葬禮。並從蜜雅的隨身物品看到她的日記，看著裡面的內容拉克絲在蜜雅的遺體前留下了眼淚。

在杜蘭朵獲得『安魂曲』武器系統並發表命運計畫後，拉克絲以[永恆號為旗艦](../Page/永恆號.md "wikilink")，與命運計畫的反對者們為了破壞『安魂曲』阻止杜蘭朵攻擊歐普而與杜蘭朵所率領的札夫特軍交戰。最後與[煌·大和駕駛的](../Page/煌·大和.md "wikilink")[攻擊自由鋼彈一起擊沉軍事移動要塞彌賽亞後獲得了勝利](../Page/ZGMF-X20A_Strike_Freedom.md "wikilink")。

得知己方擊毀『安魂曲』，彌賽亞要塞全毀，拉克絲在確認[煌與](../Page/煌·大和.md "wikilink")[阿斯蘭從崩毀的彌賽亞中平安生還後](../Page/阿斯蘭·薩拉.md "wikilink")，從[永恆號廣播](../Page/永恆號.md "wikilink")，對札夫特軍提出雙方立即停火，參戰各方船艦紛紛發出撤退信號彈，召回所屬
MA, MS. 全線逐漸停戰，第二次 PLANT 與地球之戰至此告一段落。

最後結局，接受[P.L.A.N.T最高評議會的邀請與陪同的](../Page/P.L.A.N.T.md "wikilink")[煌·大和一起回到](../Page/煌·大和.md "wikilink")[P.L.A.N.T](../Page/P.L.A.N.T.md "wikilink")，在OVA
final plus
以及重製版第50集『被選擇的未來』中受伊薩克等人護衛的拉克絲與從電梯中出來穿著札夫特白衣的[煌·大和在眾目睽睽下互相擁抱](../Page/煌·大和.md "wikilink")，最後的畫面是和伊薩克等札夫特軍官一起進入[P.L.A.N.T評議會內](../Page/P.L.A.N.T.md "wikilink")。

### 完結解釋

劇中並沒有明確顯示出拉克絲當時的身份，但是在第50集中曾出現議長這個詞，因此有拉克絲就任議長的解釋，但是另一方面，導演福田在Twitter上一直否認有拉克絲當上議長這種結果。

但也有設定提過戰後拉克絲是在國防委員體系的地方工作，跟議長無關。\[2\]

### 地下武裝組織

由於前議長[西蓋爾·克萊因遭到暗殺](../Page/希捷尔·克莱因.md "wikilink")，境內的克萊因派系為求自保，搭乘[永恆號逃離](../Page/永恆號.md "wikilink")[P.L.A.N.T](../Page/P.L.A.N.T.md "wikilink")，並立其女拉克絲為領導。

第一次[雅金·杜威攻防戰結束](../Page/雅金·杜威攻防戰.md "wikilink")，在新議長[杜蘭朵·吉伯特的鐵腕整肅下](../Page/杜蘭朵·吉伯特.md "wikilink")，[P.L.A.N.T以非常有效率的速度進行戰後重建](../Page/P.L.A.N.T.md "wikilink")，但逐漸開始有人們對新議長的權力擴張擔憂；另一方面，[地球聯合境內也有越來越多人開始對政治感到不滿](../Page/地球聯合.md "wikilink")。而後，這些人被克萊因一派所吸收，開始祕密竊取各國情資與科技、以小行星帶作為隱藏，建構出一個新的地下武裝集團[終端機](../Page/終端機_\(GUNDAM\).md "wikilink")。

### 暗殺未遂事件

以拉克絲·克萊因為首的私立武裝組織，在[尤尼烏斯7號墜落事件後](../Page/尤尼烏斯7號.md "wikilink")，[暗殺部隊曾企圖暗殺她](../Page/暗殺.md "wikilink")。

暗殺部隊為[札夫特軍激進派](../Page/ZAFT.md "wikilink")（薩拉派）調整者所為，在暗殺過程中使用[札夫特軍大戰後](../Page/ZAFT.md "wikilink")，所配有最新銳機體[亞修進行暗殺](../Page/UMF/SSO-3.md "wikilink")，但並沒有成功，全部被[自由鋼彈擊敗最後](../Page/自由鋼彈.md "wikilink")[自爆而死](../Page/自爆.md "wikilink")。

在第一次[雅金·杜威攻防戰後](../Page/雅金·杜威攻防戰.md "wikilink")，由於溫和派掌權，[P.L.A.N.T和](../Page/P.L.A.N.T.md "wikilink")[地球聯合表面上和平](../Page/地球聯合.md "wikilink")，對於自然人懷有強烈憎恨，並主張清除所有的自然人的激進派人士所組成的暗殺部隊，以[尤普·馮·阿拉法斯為首](../Page/尤普·馮·阿拉法斯.md "wikilink")，暗殺拉克絲未遂。

## 演唱歌曲

拉克絲在動畫中演唱的歌曲。

  - SEED

<!-- end list -->

  - 《寂靜的夜裏（）》
  - 《水之証（）》

<!-- end list -->

  - DESTINY

<!-- end list -->

  - 《希望的領域（）》

## 參考資料

<references />

  - 《[GUNDAM戰記超百科](../Page/GUNDAM戰記超百科.md "wikilink")》
    第99期－【SEED】：拉克絲·克萊因 .雨禾國際.2013/8/6.EAN
    4710446414434 99

## 外部連結

  - [「Ｑ．ひな祭りは過ぎちゃったけど…ガンダムでお姫様キャラといえば？」はラクスが1位！【2008年3月10日～2008年3月16日】](https://web.archive.org/web/20080719090713/http://www.gundam.info/content/172)
  - [Lacus Clyne](http://myanimelist.net/character/116/Clyne_Lacus)
  - [高达中文机体资料库](https://web.archive.org/web/20150518074701/http://985.so/j7Ah)

[Category:C.E.人物](../Category/C.E.人物.md "wikilink")
[Category:虛構歌手](../Category/虛構歌手.md "wikilink")
[Category:源自基因工程的虛構角色](../Category/源自基因工程的虛構角色.md "wikilink")

1.
2.  週刊 ガンダム パーフェクトファイル