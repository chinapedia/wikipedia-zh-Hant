**拉夫连季·帕夫洛维奇·贝利亚**（；；，），[格鲁吉亚人](../Page/格鲁吉亚.md "wikilink")，[苏联共产党高级领导人](../Page/苏联共产党.md "wikilink")，长期担任[內務人民委員部](../Page/內務人民委員部.md "wikilink")（秘密警察）首脑。是[斯大林](../Page/斯大林.md "wikilink")[大清洗计划的主要执行者之一](../Page/大清洗.md "wikilink")，[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")1945年7月9日成為[苏联元帅](../Page/苏联元帅.md "wikilink")。[雅尔塔会议中斯大林曾向](../Page/雅尔塔会议.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")[小罗斯福介绍说](../Page/小罗斯福.md "wikilink")：“他是我们的[希姆莱](../Page/海因里希·希姆莱.md "wikilink")。”\[1\]在斯大林逝世之前，他是苏联的极重要权力人物，也是前者發動紅色恐怖的犯人與幹部之一，斯大林逝世之后馬上在权利斗争中败给[赫鲁晓夫和](../Page/赫鲁晓夫.md "wikilink")[马林科夫](../Page/马林科夫.md "wikilink")，后被撤职并秘密处决。與斯大林同為蘇聯領導層裡罕見的格魯吉亞裔人。

## 生平

### 早年生活

贝利亚出生于[格鲁吉亚](../Page/格鲁吉亚.md "wikilink")[阿布哈兹首府](../Page/阿布哈兹.md "wikilink")[苏呼米附近的一座小镇的](../Page/苏呼米.md "wikilink")[明格列尔人家庭](../Page/明格列尔人.md "wikilink")，父母均是农民。从一所技校毕业之后，他于1917年3月在[巴库加入了](../Page/巴库.md "wikilink")[布尔什维克](../Page/布尔什维克.md "wikilink")，当时他正在学习[工程学](../Page/工程学.md "wikilink")。（但是据称根据現今[亞塞拜然首都](../Page/亞塞拜然.md "wikilink")[巴库之](../Page/巴库.md "wikilink")[苏联共产党的纪录](../Page/苏联共产党.md "wikilink")，贝利亚实际上入党的时间是1919年，另有传言说贝利亚曾经先加入过[红军](../Page/红军.md "wikilink")，但是在1919年作了逃兵，不过这些说法均没有实际的证据支撑）他曾经当过足球运动员。\[2\]

### 地方工作

[Lavrenti_Beria_Stalins_family.jpg](https://zh.wikipedia.org/wiki/File:Lavrenti_Beria_Stalins_family.jpg "fig:Lavrenti_Beria_Stalins_family.jpg")\]\]
1920年或1921年，贝利亚加入了[契卡](../Page/契卡.md "wikilink")（苏俄的政治警察组织，[克格勃的前身](../Page/克格勃.md "wikilink")）。在格鲁吉亚于红军侵占下成立社会主义国家后，他于1922年成了OGPU-国家政治指导部格鲁吉亚分部的副首脑，1924年格鲁吉亚民族起义中，他由于镇压得力（处决了一万人）而得到了红旗勋章与当上泛高加索OGPU秘密政治部的首脑。1926年他当上了[格鲁吉亚OGPU部长并且认识了斯大林](../Page/格鲁吉亚.md "wikilink")。在与斯大林合作的这段时间内他摧毁了[伊朗和](../Page/伊朗.md "wikilink")[土耳其在高加索建立的谍报网并开始负责斯大林的安全保卫](../Page/土耳其.md "wikilink")。

1931年至1932年，他先后当上格鲁吉亚和泛高加索共产党书记，1934年又成了中央委员会委员。

### 进入中央

1938年8月，贝利亚当了[内务人民委员部的副委员长](../Page/内务人民委员部.md "wikilink")，在[叶若夫指挥下开始了大清洗](../Page/叶若夫.md "wikilink")。当大清洗对国家的各个方面造成的危害越发明显时，叶若夫落马，贝利亚开始负责（斯大林考虑过让卡冈诺维奇接任，但还是选定了秘密警察专业出身的贝利亚），尽管贝利亚的名字总是和大清洗联系在一起，但是他正式主管内务人民委员部时大清洗已经开始缓和，有十万人从劳改营裡被释放。此时内务部已经是一支拥有几个师和数十万保卫工作者的庞大军队。但由于叶若夫“错误”地枪决了大批优秀的驻外特工，对外情报网遭受重大破坏，但后来贝利亚重建了它。1939年3月他当上了苏共政治局候补委员。1941年他当上了[国家安全总委员](../Page/国家安全总委员.md "wikilink")，这是苏联警察系统内的最高阶级，相当于军队元帅。

### 二战时期

1940年在第三次召开后，3月5日贝利亚提交报告说现在关在乌克兰的波兰犯人都是苏联的敌人，然后经斯大林批准，执行[卡廷大屠杀](../Page/卡廷大屠杀.md "wikilink")。

贝利亚在[卫国战争所做出的贡献被后来的官方故意遗忘](../Page/卫国战争.md "wikilink")。他曾是战时国家最高权利机关国防委员会成员，其任务从情报、外交到组织修造防御工事、工厂后撤和武器生产等，他也组织了游击队在德军后方活动。[莫斯科保卫战时期](../Page/莫斯科保卫战.md "wikilink")，他寸步不离斯大林。战后随着整个苏联警察系统全面采用军阶，
1944年，当刚把德军赶出苏联国土，贝利亚就着手开始以通敌的罪名迫害少数民族的行动。主要对象是[卡拉恰伊人](../Page/卡拉恰伊人.md "wikilink")、[卡尔梅克人](../Page/卡尔梅克人.md "wikilink")、[车臣人](../Page/车臣人.md "wikilink")、[印古什人](../Page/印古什人.md "wikilink")、[克里米亚鞑靼人](../Page/克里米亚鞑靼人.md "wikilink")、[伏尔加德意志人](../Page/伏尔加德意志人.md "wikilink")，他们都被流放到[中亚](../Page/中亚.md "wikilink")。
1945年7月9日正式成为[苏联元帅](../Page/苏联元帅.md "wikilink")，地位仅次于斯大林。但是不知为什么斯大林从来没有象对其他元帅那样，公开表扬他对战争的贡献或是授予官方荣誉（最常见的是胜利勋章）。

### 战后时期

[二战后](../Page/二战.md "wikilink")，抢夺德国专家和研发[核武器等首要任务](../Page/核武器.md "wikilink")，均由贝利亚负责。事实上他负责组织犯人开采的第一座铀矿早在1939年就在[科雷马河附近建立了](../Page/科雷马河.md "wikilink")。在那里犯人等于没有任何辐射防护措施，二战末期起他更建立了更大规模的“核子古拉格”，大量的犯人被集中起来开采铀矿或是建设相关设施。\[3\]在苏联爆炸第一颗原子弹后，贝利亚的人望一时达到顶峰。\[4\]
斯大林、贝利亚和米高扬预见了[中國共產黨将得到](../Page/中國共產黨.md "wikilink")[国共内战的胜利](../Page/国共内战.md "wikilink")，并设法运动提供[满洲作为](../Page/满洲.md "wikilink")[中国人民解放军的避风港](../Page/中国人民解放军.md "wikilink")，还安排大量提供主要是缴自[关东军的武器给中共](../Page/关东军.md "wikilink")。\[5\]

1952年10月的苏共十九大上，斯大林批评了贝利亚、[伏罗希洛夫](../Page/伏罗希洛夫.md "wikilink")、[莫洛托夫](../Page/莫洛托夫.md "wikilink")、[米高揚和](../Page/阿那斯塔斯·米高揚.md "wikilink")[卡冈诺维奇](../Page/卡冈诺维奇.md "wikilink")。

1953年3月5日，斯大林去世，有人认为斯大林是被贝利亚所毒死，\[6\]据赫鲁晓夫的说法，在斯大林刚失去知觉时，贝利亚立刻大骂斯大林，等斯大林一醒来马上跪下吻他的手，之后见又昏过去了就吐痰在地。\[7\]事实上当斯大林一断气，贝利亚第一个扑上去亲吻其尸体“动作之快有如在抢夺死掉国王的戒指”。\[8\]

### 中央争权

3月5日，[苏联国家安全部和苏联内务部合并成一个部](../Page/苏联国家安全部.md "wikilink")——[苏联内务部](../Page/苏联内务部.md "wikilink")，贝利亚出任部长。
3月13日，发布了《关于成立侦查小组重新审理一些特大案件的命令》、《关于成立苏联公民被强制迁出格鲁吉亚案件审理委员会的命令》。
3月17日，提交了《关于移交苏联内务部一些单位的报告》。
3月18日，提交了《苏联部长会议关于移交内务部一些单位的决议草案》。
3月21日，提交了《关于重新审议1953年一些基建项目的报告》，发布了《关于重新审理控告苏联空军和航空工业部前领导人案件的命令》。
为卷入[医生案件](../Page/医生案件.md "wikilink")、[明格列尔人事件](../Page/明格列尔人事件.md "wikilink")、[列宁格勒案件](../Page/列宁格勒案件.md "wikilink")、[犹太人反法西斯委员会案件的几个和自己关系密切的同僚予以平反](../Page/犹太人反法西斯委员会案件.md "wikilink")，并留在[內務人民委員部担任要职](../Page/內務人民委員部.md "wikilink")，同时让刚担任中央书记的[谢苗·杰尼索维奇·伊格纳季耶夫失势下台](../Page/谢苗·杰尼索维奇·伊格纳季耶夫.md "wikilink")，之后又借平反对格鲁吉亚负责安全部门的官员进行更换。

3月25日，提交了《苏联部长会议关于修改1953年建设计划的决议草案》。并开始市场化尝试和机构改革，使[內務人民委員部的作用日益凸显](../Page/內務人民委員部.md "wikilink")。\[9\]

3月26日，提交了《关于实行大赦给苏共中央主席团的报告》。 3月27日，提交了《苏共中央主席团关于大赦的决议草案》。
3月28日，提交了《关于移交劳动改造营的报告》、《关于将劳改营和劳改队由苏联内务部移交苏联司法部的决议草案》。
贝利亚违反大赦令的规定，下令将几个劳改集中营关闭，数百万犯人（规定一般拘留期不得超过5年，儿童、怀孕妇女和老人、绝症患者立即释放，但是政治犯被排除在外）被释放，这些人流窜到莫斯科和其它城市，社会治安迅速恶化激起很大民愤，贝利亚以维护公共秩序为托辞，在莫斯科保留了自己的武装力量；

4月4日，发布了《关于禁止对被捕人员采取任何强制和体罚措施的命令》、《关于重新审理公民被强制迁出格鲁吉亚案件的命令》。\[10\]
5月20日，递交了《关于立陶宛共和国国家安全部在同民族主义分子地下活动作斗争中存在缺点的意见书》。
6月15日，提交了《关于限制苏联内务部长下属特别委员会权力的报告》、《苏共中央主席团关于苏联内务部长下属特别委员会的决议草案》、《苏联内务部长下属特别委员会条例草案》。
内务部一国家安全部机构，其分支机构和特工人员广布于全国上下所有的组织和部门之中。此外，保护克里姆林宫以及所有苏维埃政府大员和中央主席团成员的卫队也均属贝利亚统领。内务部负有边防警戒的职责，因而还拥有不少于
10 个师的直属部队。

改变对[南斯拉夫政策](../Page/南斯拉夫.md "wikilink")，对内乱中的[东德不采取武力干涉](../Page/东德.md "wikilink")，[东德](../Page/东德.md "wikilink")[六一七事件中](../Page/六一七事件.md "wikilink")，莫洛托夫等一众苏联元老认为贝利亚有交出东德以换取停止冷战、及将外交政策“去布尔什维克化”、主张对美屈服的倾向，促成了拿下他的决心。

### 被捕受审

1953年6月26日上午，苏共中央召开部长会议主席团和中央委员会主席团联席会议，朱可夫按照约定带领将军和士兵在外等候，会议室内当赫鲁晓夫将他召来斥其是“混进党内的、投机钻营的野心家”时，他惊讶地说“怎么了尼基塔·谢尔盖耶维奇？你怎么抓起我裤子里的虱子来了？”之后布尔加宁、莫洛托夫等要人纷纷痛斥他，当他向马林科夫求救时，这位好友别过头去。众人中只有米高扬说“（贝利亚）只要接受批评，并能坚决改正，不是不可挽救的”。之后马林科夫按下电铃，朱可夫带着一群军人将他抓走，据传说朱可夫说“以苏联人民的名义，你被捕了，杂种同志。”之后从他公文包里搜出一张纸条，上面用红铅笔写着“警报\!警报\!警报\!”，这是他在意识到危险后立即写下的（可能想交给他的克里姆林宫卫队）。\[11\]在事先布置人手时，朱可夫特别调用了他自己和布尔加宁的黑玻璃轿车两辆将人手运进克里姆林宫去。\[12\]《[真理报](../Page/真理报.md "wikilink")》发表《党、政府、苏联人民牢不可破的团结》社论。

7月2日[马林科夫在全会上的](../Page/马林科夫.md "wikilink")《<关于贝利亚反党叛国的罪行>的报告》指出：“……贝利亚灵活巧妙地利用自己在内务部的地位……以便把内务部置于党和政府之上……企图把党中央和政府置于内务部的监督之下，简直令人不能容忍……”。

12月17日，[塔斯社宣布](../Page/塔斯社.md "wikilink")，蘇聯最高蘇維埃主席團於1953年6月26日決定撤銷拉·巴·貝利亞蘇聯部長會議第一副主席職務，並將其案件送交法院審理。\[13\]12月24日，莫斯科蘇聯最高法院正式宣判貝利亞叛國罪。\[14\]贝利亚被秘密枪决，罪名是：“背叛祖国，为外国资本家卖力，纠集仇视苏维埃国家的叛徒阴谋团伙，妄图夺取国家政权，推翻工农苏维埃制度，复辟资本主义和重建资本家的国家。”

贝利亚在[克格勃总部](../Page/克格勃.md "wikilink")[盧比揚卡大樓被](../Page/盧比揚卡大樓.md "wikilink")[帕·费·巴季茨基秘密](../Page/帕·费·巴季茨基.md "wikilink")[處決](../Page/處決.md "wikilink")，其母亲、岳母、妻子、妹妹和儿子等20人被[流放](../Page/流放.md "wikilink")。

### 身后事件

1998年，在贝利亚位于莫斯科“花园环路”的旧居整修玫瑰花园水管时发现了五具年轻女子的遗骸。\[15\]

[蘇聯解體後貝利亞的家人曾要求為貝利亞平反](../Page/蘇聯解體.md "wikilink")，但在2000年5月時被[俄罗斯联邦最高法院駁回](../Page/俄罗斯联邦最高法院.md "wikilink")\[16\]。

## 影响与评价

苏联有人评价其像魔鬼一样善变，关于贝利亚道德败坏的故事数不胜数，在他被捕后，他曾經[强奸數十位女性](../Page/强奸.md "wikilink")，甚至是未成年女子的恶行暴露了，而後的蘇聯甚至以「淫棍」形容他。\[17\]2003年俄国相关档案的开放表明斯大林早就在收集多达“数十名”受害者的材料。早有史家说他如此恶名远扬决不是没有来由的。\[18\]在贝刚被捕时马林科夫告诉赫鲁晓夫，他一个侍卫长的继女被其强奸过。\[19\]也有许多女人在未被强迫下与他睡觉（至少表面如此）。有一次他带了个女演员进入政治局区域，答应释放她关在内务人民委员会监狱里的父亲和祖父，随即奸污了她，并说：“想叫就叫吧，没关系的。”但是此女的家人已经被处决了，她随后也被捕，投入监狱。\[20\]贝利亚在他的办公室旁设立了专门用于拷打未成年犯人的刑讯室。\[21\]2003年每日电讯报道在他的住所（现土耳其使馆）地下发现了装在袋里的人腿骨。\[22\]

[蘇聯中央政治局對貝利亞時常設法](../Page/蘇聯共產黨中央政治局.md "wikilink")[強姦](../Page/強姦.md "wikilink")、[誘姦女性的事件十分清楚](../Page/誘姦.md "wikilink")。由於貝利亞戰時的重要性，斯大林對此採取了寬容的態度，但是當斯大林知道他的女兒[斯韋特蘭娜單獨與貝利亞在他家後](../Page/斯韋特蘭娜·約瑟福芙娜·阿利盧耶娃.md "wikilink")。他說，“我不信任貝利亞”，並打電話命她馬上離開。當貝利亞在[伏羅希洛夫元帥的夏季鄉間別墅看上他的妻子後](../Page/克里門特·葉弗列莫維奇·伏羅希洛夫.md "wikilink")，他一路緊跟著他們的車回到[克里姆林宮](../Page/克里姆林宮.md "wikilink")，把伏羅希洛夫的妻子嚇壞了。\[23\]

贝利亚曾有一句名言：“当我们[布尔什维克想要做成一件事](../Page/布尔什维克.md "wikilink")，我们不顾其他的一切。”斯大林在很长时间内都对其颇为器重，贝利亚也一再高升。党内的干部害怕贝利亚，如同像敬畏斯大林一样，害怕[大清洗的厄运落在自己头上](../Page/大清洗.md "wikilink")。斯大林死后，贝利亚迅速在党内被打倒，斯大林去世不到一年，贝利亚便被枪决，并在此后，贝利亚的种种恶行被揭露，[蘇聯共產黨开动宣传机器迅速将其塑造成人民的叛徒](../Page/蘇聯共產黨.md "wikilink")。

另外[赫魯晓夫的蘇聯與](../Page/赫魯晓夫.md "wikilink")[狄托的南斯拉夫破冰交好時](../Page/狄托.md "wikilink")，也指責其為破壞兩國关系的元兇，赫魯晓夫對狄托說，[蘇、南關係的破裂完全是貝利亞挑唆的](../Page/蘇南衝突.md "wikilink")，現在這個惡棍已被[正法了](../Page/正法.md "wikilink")，蘇聯已改正錯誤，兩黨不應再爭吵，還是重新和好吧。\[24\]而据魏特林的说法，贝利亚不仅提出要“允许轻工业领域的私人资本存在”“改变集体农场制度”，还提议和西方及铁托修好关系。\[25\]而有一种说法，在贝利亚倒台后于他的保险箱发现给铁托的信的副本，提议进行一次秘密会晤，还强调“除了兰科维奇和铁托同志外不要让人知道。”\[26\]

## 轶闻

在[赫鲁晓夫的秘密报告中重述了一则关于贝利亚](../Page/赫鲁晓夫.md "wikilink")、说明[红色恐怖的著名笑话](../Page/红色恐怖.md "wikilink")：斯大林的[烟斗丢了](../Page/烟斗.md "wikilink")，贝利亚在第二天就抓到了十個竊賊，他们全都招供了。而斯大林则在自己的沙发下找到了那个烟-{斗}-。

斯大林有次说自己年轻时乘[雪橇走了](../Page/雪橇.md "wikilink")12[俄里](../Page/俄里.md "wikilink")（約12.8[公里](../Page/公里.md "wikilink")）发现12只鸟，他打死一只后，回去拿子弹，又把其他的也打死並且带回家。贝利亚私下说：“一个高加索人沒有甚麼机会乘雪橇，他怎么可能走那么远的路（合计48俄里，約51.2公里）？他在撒谎。”\[27\]

## 参见

  - [贝利亚帮](../Page/贝利亚帮.md "wikilink")
  - [希姆莱](../Page/希姆莱.md "wikilink")
  - [康生](../Page/康生.md "wikilink")

## 参考文献

### 引用

### 网页

  - [藍英年：誰殺了一萬五千波蘭軍官？](https://web.archive.org/web/20140522125601/https://tw.knowledge.yahoo.com/question/question?qid=1205080815449)

## 參考資料

  -
  - [Берия Л. П.](http://www.memo.ru/history/NKVD/kto/biogr/gb42.htm) //


  -
  - *Гусаров А. Ю.* Маршал Берия. Штрихи к биографии. — М.:
    Центрполиграф, 2015. — ISBN 978-5-227-06055-6.

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [Benjamin B. Fischer](../Page/Benjamin_B._Fischer.md "wikilink"),
    ["The Katyn Controversy - Stalin's Killing
    Field"](https://web.archive.org/web/20070509174522/https://www.cia.gov/csi/studies/winter99-00/art6.html)
  - [Photocopy of Beria's letter to Stalin to execute Katyn officers
    prisoners](https://web.archive.org/web/20090327115220/https://www.cia.gov/library/center-for-the-study-of-intelligence/csi-publications/csi-studies/studies/winter99-00/pg62.gif)
  - [Official site of the Memorial to
    Katyn](https://web.archive.org/web/20071112110507/http://admin.smolensk.ru/history/katyn/)
  - [Interview with Sergo
    Beria](http://www.cnn.com/SPECIALS/cold.war/episodes/01/interviews/beria/)
  - [An outline of the Russian Supreme Court decision of 29
    May 2000](http://www.globalsecurity.org/intell/library/news/2000/05/000529-beria1.htm)
  - [Annotated bibliography for Lavrenty Beria from the Alsos Digital
    Library for Nuclear
    Issues](http://alsos.wlu.edu/qsearch.aspx?browse=people/Beria,+Lavrenti)
  - Central Intelligence Agency, Office of Current Intelligence. [*The
    Reversal of the Doctors' Plot and Its Immediate
    Aftermath*](https://web.archive.org/web/20070711041744/http://www.foia.cia.gov/CPE/CAESAR/caesar-04.pdf),
    17 July 1953.
  - Central Intelligence Agency, Office of Current Intelligence. [*Purge
    of
    L.P. Beria*](https://web.archive.org/web/20070711041734/http://www.foia.cia.gov/CPE/CAESAR/caesar-10.pdf),
    17 April 1954.
  - Central Intelligence Agency, Office of Current Intelligence.
    [*Summarization of Reports Preceding Beria
    Purge*](https://web.archive.org/web/20070711041734/http://www.foia.cia.gov/CPE/CAESAR/caesar-10.pdf),
    17 August 1954.

[category:明格列爾人](../Page/category:明格列爾人.md "wikilink")

[Берия, Лаврентий Павлович](../Category/蘇聯元帥.md "wikilink") [Берия,
Лаврентий Павлович](../Category/被處決的蘇聯人.md "wikilink")
[Category:老布尔什维克](../Category/老布尔什维克.md "wikilink")
[Category:大清洗加害者](../Category/大清洗加害者.md "wikilink")
[Category:内务人民委员部](../Category/内务人民委员部.md "wikilink")
[Category:格魯吉亞共產黨第一書記](../Category/格魯吉亞共產黨第一書記.md "wikilink")
[Category:蘇聯共產黨中央政治局委員](../Category/蘇聯共產黨中央政治局委員.md "wikilink")
[Category:苏联的政治迫害](../Category/苏联的政治迫害.md "wikilink")
[Category:苏联强奸犯](../Category/苏联强奸犯.md "wikilink")
[Category:蘇聯核子武器計畫](../Category/蘇聯核子武器計畫.md "wikilink")

1.  Montefiore, Simon Sebag (2005). Stalin: Court of the Red Tsar.
    Random House. p. 483.
2.  <http://sports.163.com/12/0322/16/7T7BMOGB000502OI.html>
3.  Historical Dictionary of Russian and Soviet Intelligence P109
4.  Барсенков А. С., Вдовин А. И., «История России. 1917—2007» — М.:
    Аспект Пресс, 2008 — стр. 385
5.  Chang and Halliday, 2005
6.  [史達林遇害真相——死於貝利亞投毒](http://big5.ce.cn/xwzx/gjss/gdxw/200712/20/t20071220_13981864.shtml)

7.  Sebag-Montefiore, 571
8.  Sebag-Montefiore, 649
9.  [贝利亚的真面目](http://www.news365.com.cn/wxpd/wz/rwcq/200901/t20090119_2170694.htm)
10. Барсенков А. С., Вдовин А. И., «История России. 1917—2007» — М.:
    Аспект Пресс, 2008 — стр. 438
11. Andrew, Christopher; Oleg Gordievsky (1990). "11". KGB: The Inside
    Story (1st edition ed.). New York, NY, USA: HarperCollins
    Publishers. pp. 423–424. ISBN 978-0-06-016605-2. .
12. K. S. Moskalenko. The arrest of Beria. Newspaper Московские новости.
    No. 23, year 1990.
13.
14.
15. [Mass grave may hold Beria's sex
    victims](https://www.independent.co.uk/news/world/mass-grave-may-hold-berias-sex-victims-1453126.html)
16. [An outline of the Russian Supreme Court decision of 29
    May 2000](http://www.globalsecurity.org/intell/library/news/2000/05/000529-beria1.htm)
17. Donald Rayfield. Stalin and His Hangmen: The Tyrant and Those Who
    Killed for Him. Random House, 2005. ISBN 978-0-375-75771-6; pp.
    466–467
18. Knight, 97.
19. 赫鲁晓夫回忆录
20. Sebag-Montefiore, 507
21. Knight, Amy, 'Beria: Stalin's First Lieutenant', Princeton
    University Press, 1993
22. <http://www.telegraph.co.uk/news/worldnews/europe/russia/1450145/Stalins-depraved-executioner-still-has-grip-on-Moscow.html>
23.
24. [A Tift with Tito? Beria's to Blame, The Miami
    News, 1955-05-29](http://news.google.com/newspapers?nid=2206&dat=19550529&id=erwyAAAAIBAJ&sjid=5uoFAAAAIBAJ&pg=4133,4441340)
25. Thaddeus Wittlin, Commissar: The Life and Death of Lavrenty
    Pavlovich Beria (New York: Macmillan, 1972), p. 354.
26. Vladislav Martinovich Zubok, Konstantin Pleshakov Inside the
    Kremlin's cold war: from Stalin to Khrushchev - 第 159 页
27. 赫鲁晓夫回忆录