**拉妮·穆科吉**（，，，）是[印度女演員](../Page/印度.md "wikilink")，她1978年3月21日出生于[寶萊塢](../Page/寶萊塢.md "wikilink")[導演的家庭](../Page/導演.md "wikilink")。
拉妮贏得過很多寶萊塢的電影獎項。

在2007年的[國際婦女節](../Page/國際婦女節.md "wikilink")，印度的《[電影觀眾](../Page/電影觀眾雜誌.md "wikilink")》雜誌將她評為歷來第三位最受歡迎的女演員，前二名分別為[瑪都麗·迪西特和](../Page/瑪都麗·迪西特.md "wikilink")[納吉斯所獲得](../Page/納吉斯_\(印度演員\).md "wikilink")。

## 電影事業

拉妮1996年初登影壇，處女作為：《*[Raja Ki Aayegi
Baraat](../Page/Raja_Ki_Aayegi_Baraat.md "wikilink")*》。1998年她因為在[卡蘭·喬哈導演的影片](../Page/卡蘭·喬哈.md "wikilink")《[愛情的證明](../Page/愛情的證明.md "wikilink")》（Kuch
Kuch Hota
Hai）中表演出色而獲得《[印度電影觀眾──最佳女配角獎](../Page/Filmfare_Award.md "wikilink")》。

以後她多次擔當影片的女主角或配角，成績卓然。1998年顯然是她迄今最為成功的一年，憑藉在影片《*[Hum
Tum](../Page/Hum_Tum.md "wikilink")*》和《*[Yuva](../Page/Yuva.md "wikilink")*》中的出色表演，她破天荒獲得了同年的[最佳女主角獎和](../Page/Filmfare_Best_Actress_Award.md "wikilink")[女配角獎](../Page/Filmfare_Best_Supporting_Actress_Award.md "wikilink")。

2005年，她在影片《*[黑暗](../Page/黑暗_\(印度電影\).md "wikilink")*（*[Black](../Page/Black_\(Indian_Film\).md "wikilink")*）》中扮演一位雙目失明的倔強女子，又為廣大觀眾留下極深刻的印象，她第二度獲得印度影后殊榮。也奠定了她在[寶萊塢影壇上的](../Page/寶萊塢.md "wikilink")「大姐級」地位。\[1\]

## 巡迴演出

1999年，拉妮參加：《*寶萊塢五傑世界大巡演*》──五傑是：她自己、[Aamir
Khan](../Page/Aamir_Khan.md "wikilink")、[Aishwarya
Rai](../Page/Aishwarya_Rai.md "wikilink")、[Akshaye
Khanna和](../Page/Akshaye_Khanna.md "wikilink")[Twinkle
Khanna](../Page/Twinkle_Khanna.md "wikilink")\[2\]。世界觀眾得以了解她們的藝術。

五年之後，《*2004年誘惑*》世界大巡演，拉妮又赫然在列，這一次她同[Shahrukh
Khan](../Page/Shahrukh_Khan.md "wikilink")、[Saif Ali
Khan](../Page/Saif_Ali_Khan.md "wikilink")、[Preity
Zinta](../Page/Preity_Zinta.md "wikilink")、[Arjun
Rampal](../Page/Arjun_Rampal.md "wikilink")、[Priyanka
Chopra等人一道在世界十九個城市登臺演出](../Page/Priyanka_Chopra.md "wikilink")\[3\]。

## 個人生活

拉妮出生於[印度西孟加拉邦一個導演家庭](../Page/西孟加拉邦.md "wikilink")，她的父親[蘭姆·穆科吉](../Page/蘭姆·穆科吉.md "wikilink")（Ram
Mukherjee）是瑪拉亞製片公司（Filmalaya Studios）的創始人，現已退休。\[4\]
她的母親克麗詩娜（Krishna）是一個歌手。\[5\]她的哥哥[拉賈·穆科吉](../Page/拉賈·穆科吉.md "wikilink")（Raja
Mukherjee）是一個製片人、導演。近來拉妮有時幫助哥哥創辦電視劇錄制場。\[6\]\[7\]她還有其他親戚如[卡約兒活躍在寶萊塢的銀幕上](../Page/卡約兒.md "wikilink")。\[8\]

拉妮很小就開始接受舞蹈訓練，\[9\]她初中就讀[曼尼克吉－庫柏中學](../Page/曼尼克吉－庫柏中學.md "wikilink")（Maneckji
Cooper High
School），後來到了孟買入讀[米迪拜中學](../Page/米迪拜中學.md "wikilink")（Mithibai
College）並在那裡開始了她的演藝事業。

喜歡發佈閑言的娛樂雜誌總把拉妮同一些走紅的男星扯到一起，\[10\]
又有一些小報道說她經常玩弄某些男星的感情，但是拉妮對此類報道一概否認。\[11\]

現在拉妮多數時間居住在[孟買](../Page/孟買.md "wikilink")，她曾在2005年為父母在家鄉[西孟加拉邦買下一棟房屋](../Page/西孟加拉邦.md "wikilink")，並花兩年時間裝修它。\[12\]2007年她自己也在同一地買下另一棟房屋。\[13\]

2014年4月21日，拉妮和印度知名導演在意大利舉行私人婚禮。2015年12月9日，她在孟買誕下女兒，取名Adira。\[14\]

## 圍繞拉妮的爭議

2005年，拉妮在一次會見媒體面談中提到她最仰慕的人是[阿道夫·希特勒](../Page/阿道夫·希特勒.md "wikilink")。可想而知引起輿論大嘩，事後她矢口否認曾提到希特勒的名字。\[15\]\[16\]

2006年，在一次拍攝中有一些影迷受到保安人員的毆打，傳媒職責拉妮沒有及時制止保安，她事後對此公開道歉。\[17\] \[18\]

## 榮譽

2006年2月，拉妮連續第二年被《[電影觀眾雜誌](../Page/電影觀眾雜誌.md "wikilink")》
評為寶萊塢「十大最有影響力」的人物，當年她是榜上唯一的女明星。\[19\]\[20\]
2007年她第三度上榜，名次晉為第五。\[21\]

近年許多[印](../Page/印度.md "wikilink")、[歐電影雜誌評選她為](../Page/歐洲.md "wikilink")「亞洲最性感的女性」\[22\]、「寶萊塢最美麗的女演員」\[23\]、「寶萊塢最佳服飾的女演員」\[24\]、「千百張臉的女性（Women
of Many Faces）」\[25\]。

2005年，[巴基斯坦總統](../Page/巴基斯坦.md "wikilink")[穆沙拉夫將軍訪問](../Page/穆沙拉夫.md "wikilink")[印度](../Page/印度.md "wikilink")，因為她是總統最喜愛的[寶萊塢影星](../Page/寶萊塢.md "wikilink")，所以被邀請出席[曼莫漢·辛格總統所主持的歡迎晚宴](../Page/曼莫漢·辛格.md "wikilink")\[26\]\[27\]。

## 出演電影

<div style="font-size: 95%">

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>電影名</p></th>
<th><p>角色名</p></th>
<th><p>註解</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990s</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><em><a href="../Page/Raja_Ki_Aayegi_Baraat.md" title="wikilink">Raja Ki Aayegi Baraat</a></em></p></td>
<td><p>Mala</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><em><a href="../Page/Ghulam.md" title="wikilink">Ghulam</a></em></p></td>
<td><p>Alisha</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Kuch_Kuch_Hota_Hai.md" title="wikilink">Kuch Kuch Hota Hai</a></em></p></td>
<td><p>Tina Malhotra</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Mehndi_(film).md" title="wikilink">Mehndi</a></em></p></td>
<td><p>Pooja</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><em><a href="../Page/Mann_(film).md" title="wikilink">Mann</a></em></p></td>
<td><p> </p></td>
<td><p>特別出演</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Hello_Brother.md" title="wikilink">Hello Brother</a></em></p></td>
<td><p>Rani</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000s</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><em><a href="../Page/Badal.md" title="wikilink">Badal</a></em></p></td>
<td><p>Rani</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Hey_Ram.md" title="wikilink">Hey Ram</a></em></p></td>
<td><p>Aparna Ram</p></td>
<td><p>代表印度角逐《<a href="../Page/奧斯卡最佳外語片獎.md" title="wikilink">奧斯卡最佳外語片獎</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Hadh_Kar_Di_Aapne.md" title="wikilink">Hadh Kar Di Aapne</a></em></p></td>
<td><p>Anjali Khanna</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Bichhoo.md" title="wikilink">Bichhoo</a></em></p></td>
<td><p>Kiran Bali</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Har_Dil_Jo_Pyar_Karega.md" title="wikilink">Har Dil Jo Pyar Karega</a></em></p></td>
<td><p>Pooja Oberoi</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Kahin_Pyaar_Na_Ho_Jaaye.md" title="wikilink">Kahin Pyaar Na Ho Jaaye</a></em></p></td>
<td><p>Priya Sharma</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><em><a href="../Page/Chori_Chori_Chupke_Chupke.md" title="wikilink">Chori Chori Chupke Chupke</a></em></p></td>
<td><p>Priya Malhotra</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Bas_Itna_Sa_Khwaab_Hai.md" title="wikilink">Bas Itna Sa Khwaab Hai</a></em></p></td>
<td><p>Pooja Shrivastav</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Nayak:_The_Real_Hero.md" title="wikilink">Nayak: The Real Hero</a></em></p></td>
<td><p>Manjari</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Kabhi_Khushi_Kabhie_Gham.md" title="wikilink">Kabhi Khushi Kabhie Gham</a></em></p></td>
<td><p>Naina Kapoor</p></td>
<td><p>Cameo</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><em><a href="../Page/Pyaar_Diwana_Hota_Hai.md" title="wikilink">Pyaar Diwana Hota Hai</a></em></p></td>
<td><p>Payal Khuranna</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Mujhse_Dosti_Karoge!.md" title="wikilink">Mujhse Dosti Karoge!</a></em></p></td>
<td><p>Pooja Sahani</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Saathiya.md" title="wikilink">Saathiya</a></em></p></td>
<td><p>Dr. Suhani Sharma/Saigol</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Performance_Award.md" title="wikilink">印度電影觀眾──最佳表演獎</a>》。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Chalo_Ishq_Ladaaye.md" title="wikilink">Chalo Ishq Ladaaye</a></em></p></td>
<td><p>Sapna</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><em><a href="../Page/Chalte_Chalte.md" title="wikilink">Chalte Chalte</a></em></p></td>
<td><p>Priya Chopra</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女主角獎</a>》。</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Chori_Chori_(2003_film).md" title="wikilink">Chori Chori</a></em></p></td>
<td><p>Khushi Malhotra</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Calcutta_Mail.md" title="wikilink">Calcutta Mail</a></em></p></td>
<td><p>Reema/Bulbul</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Kal_Ho_Naa_Ho.md" title="wikilink">Kal Ho Naa Ho</a></em></p></td>
<td><p> </p></td>
<td><p>特別出演歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/LOC_Kargil.md" title="wikilink">LOC Kargil</a></em></p></td>
<td><p>Hema</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><em><a href="../Page/Yuva.md" title="wikilink">Yuva</a></em></p></td>
<td><p>Sashi Biswas</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Hum_Tum.md" title="wikilink">Hum Tum</a></em></p></td>
<td><p>Rhea Sharma</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Veer-Zaara.md" title="wikilink">Veer-Zaara</a></em></p></td>
<td><p>Saamiya Siddiqui</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><em><a href="../Page/Black_(film).md" title="wikilink">Black</a></em></p></td>
<td><p>Michelle McNally</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女主角獎</a>》。</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Bunty_Aur_Babli.md" title="wikilink">Bunty Aur Babli</a></em></p></td>
<td><p>Vimmi Saluja (Babli)</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女主角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Paheli.md" title="wikilink">Paheli</a></em></p></td>
<td><p>Lachchi Bhanwarlal</p></td>
<td><p>代表印度角逐《<a href="../Page/奧斯卡最佳外語片獎.md" title="wikilink">奧斯卡最佳外語片獎</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/The_Rising_(Indian_movie).md" title="wikilink">The Rising</a></em></p></td>
<td><p>Heera</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><em><a href="../Page/勿言再會(印度電影).md" title="wikilink">勿言再會</a></em><br />
（<em><a href="../Page/Kabhi_Alvida_Naa_Kehna.md" title="wikilink">Kabhi Alvida Naa Kehna</a></em>）</p></td>
<td><p>Maya Talwar</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女主角獎</a>》。</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Baabul.md" title="wikilink">Baabul</a></em></p></td>
<td><p>Malvika "Milli" Talwar/Kapoor</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><em><a href="../Page/飛車載卿情.md" title="wikilink">飛車載卿情</a></em><br />
（<em><a href="../Page/Ta_Ra_Rum_Pum.md" title="wikilink">Ta Ra Rum Pum</a></em>）</p></td>
<td><p>Radhika Shekar Rai Banerjee (a.k.a. Shona)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Laaga_Chunari_Mein_Daag.md" title="wikilink">Laaga Chunari Mein Daag</a></em></p></td>
<td><p>Vaibhavari Sahay (Badki)/Natasha</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Saawariya.md" title="wikilink">Saawariya</a></em></p></td>
<td><p>Gulab</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/珊蒂別傳.md" title="wikilink">珊蒂別傳</a></em></p></td>
<td><p>拉妮</p></td>
<td><p>與全體寶萊塢影星同舞於樂曲──《<em><a href="../Page/Deewangi_Deewangi.md" title="wikilink">Deewangi Deewangi</a></em>》，而且她是第一個出場的影星。</p></td>
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p><em>Rab Ne Bana Di Jodi</em></p></td>
<td></td>
<td><p>Special appearance in song "Phir Milenge Chalte Chalte"</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><em>Luck by Chance</em></p></td>
<td></td>
<td><p>Cameo</p></td>
</tr>
<tr class="odd">
<td><p><em>Dil Bole Hadippa!</em></p></td>
<td><p>Veera Kaur/Veer Pratap Singh</p></td>
<td><p>Special appearance in song "Phir Milenge Chalte Chalte"</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><em>No One Killed Jessica</em></p></td>
<td><p>Meera Gaity</p></td>
<td><p>榮獲：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><em>Aiyyaa</em></p></td>
<td><p>Meenakshi Deshpande</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em>Talaash: The Answer Lies Within</em></p></td>
<td><p>Roshni Shekhawat</p></td>
<td><p>被提名：《<a href="../Page/Filmfare_Best_Supporting_Actress_Award.md" title="wikilink">印度電影觀眾──最佳女配角獎</a>》。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><em>Bombay Talkies</em></p></td>
<td><p>Gayatri</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><em>Mardaani</em></p></td>
<td><p>Shivani Shivaji Roy</p></td>
<td></td>
</tr>
</tbody>
</table>

</div>

## 注釋

## 外部連結

  -
[Category:1978年出生](../Category/1978年出生.md "wikilink")
[Category:印度电影女演员](../Category/印度电影女演员.md "wikilink")
[Category:孟加拉人](../Category/孟加拉人.md "wikilink")
[Category:加尔各答人](../Category/加尔各答人.md "wikilink")
[Category:孟買人](../Category/孟買人.md "wikilink")
[Category:印度印度教徒](../Category/印度印度教徒.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.
14. [1](http://m.ibtimes.co.in/rani-mukherji-delivers-baby-girl-adira-rishi-kapoor-others-congratulate-her-658677).
    "Rani Mukerji delivers baby girl Adira; Rishi Kapoor, Karan Johar,
    Parineeti and others congratulate her". International Business
    Times. 9 December, 2015.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.