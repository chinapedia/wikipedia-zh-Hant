**-{zh-hans:拉克尔·韦尔奇;zh-hant:拉寇兒·薇芝;zh-hk:麗歌慧珠;}-**（又译作**-{zh-hans:拉奎尔·韦尔奇、拉寇儿·薇芝;zh-hk:拉奎爾·韋爾奇、拉寇兒·薇芝;zh-tw:拉克爾·韋爾奇、拉奎爾·韋爾奇;}-**）（，）是一位[美國](../Page/美國.md "wikilink")[電影與](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")[演員](../Page/演員.md "wikilink")，曾經獲得[金球獎](../Page/金球獎.md "wikilink")。

## 生平

## 作品

  - 1966年：《[洪荒浩劫](../Page/洪荒浩劫.md "wikilink")》
  - 1975年：《[豪情三劍客](../Page/豪情三劍客.md "wikilink")》

## 軼聞

  - 在《[阿甘正傳](../Page/阿甘正傳.md "wikilink")》小說當中出現

<!-- end list -->

  - 為《[肖申克的救赎](../Page/肖申克的救赎.md "wikilink")》電影中的海報女郎

## 外部連結

  -
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:金球獎最佳音樂及喜劇類電影女主角獲獎演員](../Category/金球獎最佳音樂及喜劇類電影女主角獲獎演員.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:玻利維亞裔美國人](../Category/玻利維亞裔美國人.md "wikilink")
[Category:美国女歌手](../Category/美国女歌手.md "wikilink")
[Category:美國基督徒](../Category/美國基督徒.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")