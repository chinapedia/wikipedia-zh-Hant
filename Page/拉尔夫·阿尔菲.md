**拉尔夫·艾舍尔·阿尔菲**（，），美国犹太裔物理学家、天文学家。

## 生平

阿尔菲在少年时期就表现出了极高的天分。阿尔菲在[乔治·华盛顿大学获得学士学位](../Page/乔治·华盛顿大学.md "wikilink")，并在那里结识了旅美的俄裔物理学家[乔治·伽莫夫](../Page/乔治·伽莫夫.md "wikilink")。阿尔菲1948年与伽莫夫、[汉斯·贝特一起发表了大爆炸元素合成的理论](../Page/汉斯·贝特.md "wikilink")，即[αβγ理论](../Page/αβγ理论.md "wikilink")。汉斯·贝特实际并没有在其中做出工作，只是在伽莫夫的建议下署了名，这样三人的名字凑成了前三个希腊字母的发音。1948年阿尔菲还与伽莫夫一道预言了[宇宙背景辐射的存在](../Page/宇宙背景辐射.md "wikilink")，然而当时这项预言并没有引起人们关注，直到1964年[彭齐亚斯和](../Page/彭齐亚斯.md "wikilink")[威尔逊偶然发现了微波背景辐射](../Page/威尔逊.md "wikilink")，证实了他们的预言。1993年，阿尔菲与他当年的合作者[罗伯特·赫尔曼一起获得了](../Page/罗伯特·赫尔曼.md "wikilink")[亨利·德雷伯獎章](../Page/亨利·德雷伯獎章.md "wikilink")。

## 主要著作

  - R.A.Alpher,H.Bethe,G.Gamow,*The Origin of Chemical
    Elements*,*Phys.Rev.***73**,803-804(1948)
  - Ralph A.Alpher,Robert C.Herman,*Remarks on the Evolution of the
    Expanding Universe*,*Phys.Rev.***75**,1089-1095(1949)

## 参阅

  - [大爆炸理论](../Page/大爆炸理论.md "wikilink")
  - [αβγ理论](../Page/αβγ理论.md "wikilink")
  - [微波背景辐射](../Page/微波背景辐射.md "wikilink")
  - [乔治·伽莫夫](../Page/乔治·伽莫夫.md "wikilink")
  - [罗伯特·赫尔曼](../Page/罗伯特·赫尔曼.md "wikilink")

## 外部連結

  - [light-science.com](https://archive.is/20130103185715/http://www.light-science.com/alpher.html)

[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:美国天文学家](../Category/美国天文学家.md "wikilink")
[Category:20世紀天文學家](../Category/20世紀天文學家.md "wikilink")
[Category:喬治·華盛頓大學校友](../Category/喬治·華盛頓大學校友.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:華盛頓哥倫比亞特區人](../Category/華盛頓哥倫比亞特區人.md "wikilink")
[Category:亨利·德雷伯奖章获得者](../Category/亨利·德雷伯奖章获得者.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")