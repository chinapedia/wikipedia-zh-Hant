[Tour_Hassan-Rabat.jpg](https://zh.wikipedia.org/wiki/File:Tour_Hassan-Rabat.jpg "fig:Tour_Hassan-Rabat.jpg")
**拉巴特**（[阿拉伯語](../Page/阿拉伯語.md "wikilink")： ／
），[摩洛哥的首都](../Page/摩洛哥王國.md "wikilink")，是全国政治、文化和交通的中心。建成區位於摩洛哥西北部[-{布}-雷格雷格河河口](../Page/布賴格賴格河.md "wikilink")，是一處濱臨[大西洋的城市](../Page/大西洋.md "wikilink")。

拉巴特都會區有180萬人口，擁有全國重要的紡織業及建築業，自1912年起就一直是摩洛哥的首都。

## 歷史

拉巴特最早的建城歷史於1146年，當時[穆瓦希德王朝的哈里發](../Page/穆瓦希德王朝.md "wikilink")[阿卜杜勒·慕敏為了渡海進攻伊比利亞半島](../Page/阿卜杜勒·慕敏.md "wikilink")，而在沿海古城萨累的废墟建立的一座城堡。

## 气候

拉巴特是典型的[地中海气候](../Page/地中海气候.md "wikilink")。处在大西洋的岸边使拉巴特的气候温和适中，冬天凉爽而夏天温暖。夜晚一般凉爽（在冬天可能比较寒冷）。相比夜晚，白天的气温一般会上升9-10C°（15-18 F°）。在12月和1月里，冬天的最高温度一般只会有17.5 °C（或63.5 °F）（见下方的气温表）。

## 友好城市

**<big>非洲</big>**

  - [突尼斯省](../Page/突尼斯省.md "wikilink")[突尼斯市](../Page/突尼斯.md "wikilink")


**<big>歐洲</big>**

  - [馬德里自治區](../Page/馬德里自治區.md "wikilink")[马德里市](../Page/马德里.md "wikilink")

  - [安達魯西亞自治區](../Page/安達魯西亞自治區.md "wikilink")[塞維亞省](../Page/塞維亞省.md "wikilink")[塞維亞市](../Page/塞維亞.md "wikilink")

  - [加那利自治區](../Page/加那利群島.md "wikilink")[拉斯帕爾馬斯省](../Page/拉斯帕爾馬斯省.md "wikilink")[拉斯帕爾馬斯市](../Page/拉斯帕爾馬斯.md "wikilink")

  - [里斯本大區](../Page/里斯本大區.md "wikilink")[里斯本市](../Page/里斯本.md "wikilink")

  - [斯德哥爾摩省](../Page/斯德哥爾摩省.md "wikilink")[斯德哥爾摩市](../Page/斯德哥爾摩.md "wikilink")

  - [伊斯坦堡省](../Page/伊斯坦堡省.md "wikilink")[伊斯坦堡市](../Page/伊斯坦堡.md "wikilink")

  - [布爾薩省](../Page/布爾薩省.md "wikilink")[布爾薩市](../Page/布爾薩.md "wikilink")


**<big>亞洲</big>**

  - [伯利恆省](../Page/伯利恆省.md "wikilink")[伯利恆市](../Page/伯利恆.md "wikilink")

  - [納布盧斯省](../Page/納布盧斯省.md "wikilink")[納布盧斯市](../Page/納布盧斯.md "wikilink")

  - [廣東省](../Page/廣東省.md "wikilink")[廣州市](../Page/廣州市.md "wikilink")

  - [山東省](../Page/山東省.md "wikilink")[濟南市](../Page/济南.md "wikilink")


**<big>大洋洲</big>**

  - [夏威夷州](../Page/夏威夷州.md "wikilink")[檀香山市縣](../Page/檀香山市縣.md "wikilink")[檀香山市](../Page/檀香山.md "wikilink")

## 參考文獻

## 参见

  - [拉巴特皇家武装力量足球俱乐部](../Page/拉巴特皇家武装力量足球俱乐部.md "wikilink")

## 外部連結

  - 济南和非洲拉巴特、利伯维尔两市建友好合作关系[1](http://news.e23.cn/content/2013-06-29/2013062900025.html)

[Category:非洲首都](../Category/非洲首都.md "wikilink")
[Category:大西洋沿海城市](../Category/大西洋沿海城市.md "wikilink")
[Category:摩洛哥城市](../Category/摩洛哥城市.md "wikilink")
[Category:拉巴特](../Category/拉巴特.md "wikilink")