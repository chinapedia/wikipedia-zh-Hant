**拉斯海玛**（），是[阿聯酋七個成員國之一](../Page/阿聯酋.md "wikilink")，位於北部靠近[阿曼邊境](../Page/阿曼.md "wikilink")。面積1683平方公里，人口260,000。

1972年2月11日加入阿聯酋。
[缩略图](https://zh.wikipedia.org/wiki/File:Rak_lagune.JPG "fig:缩略图")
{{-}}

## 参考文献

## 外部連結

  - [Ras al-Khaimah English Information Site](http://www.rakinfo.ae)
  - [Ras al-Khaimah e-Government portal](http://www.rak.ae)

{{-}}

[Category:阿拉伯聯合大公國行政區劃](../Category/阿拉伯聯合大公國行政區劃.md "wikilink")
[Category:城邦](../Category/城邦.md "wikilink")