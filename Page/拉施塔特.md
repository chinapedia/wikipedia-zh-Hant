**拉施塔特**（德语：**Rastatt**，发音：\[ˈʁaʃtat\]）是[德国](../Page/德国.md "wikilink")[巴登-符腾堡州城市](../Page/巴登-符腾堡州.md "wikilink")，位于该州的西部，[卡尔斯鲁厄西南约](../Page/卡尔斯鲁厄.md "wikilink")22千米，[巴登-巴登以北约](../Page/巴登-巴登.md "wikilink")12千米。它隶属于[卡尔斯鲁厄行政区](../Page/卡尔斯鲁厄行政区.md "wikilink")[拉施塔特县](../Page/拉施塔特县.md "wikilink")（Landkreis
Rastatt），是该县的县政府所在地和第一大城市。

## 地理

位處[穆爾格河的兩岸](../Page/穆爾格河_\(黑森林北部\).md "wikilink")，河流貫穿拉施塔特，離[卡爾斯魯厄約](../Page/卡爾斯魯厄.md "wikilink")30公里，與[法國](../Page/法國.md "wikilink")[斯特拉斯堡相隔](../Page/斯特拉斯堡.md "wikilink")65公里。

## 歷史

拉施塔特早於古代已有人聚居，但直至十七世紀，拉施塔特尚未具影響力。後來到了1689年，[路德维希·威廉以](../Page/路德维希·威廉_\(巴登-巴登\).md "wikilink")[凡爾賽為藍本](../Page/凡爾賽.md "wikilink")，重建拉施塔特，成為歷代[巴登-巴登藩侯的居處](../Page/巴登统治者列表#巴登-巴登藩侯.md "wikilink")，直至1771年。其間，《[拉什塔特和約](../Page/拉什塔特和約.md "wikilink")》正是於拉施塔特簽訂。

## 景點

  - 拉施塔特宮殿：是多位巴登-巴登藩侯的居所。

## 經濟

拉施塔特是[梅賽德斯-賓士的生產基地之一](../Page/梅賽德斯-賓士.md "wikilink")。該廠於1992年投產，主力生產[A系列和](../Page/梅赛德斯-奔驰A级.md "wikilink")[B系列汽車](../Page/梅赛德斯-奔驰B级.md "wikilink")。

## 友好城市

  - [意大利](../Page/意大利.md "wikilink")[法诺](../Page/法诺.md "wikilink")

  - [巴西](../Page/巴西.md "wikilink")[瓜拉普阿瓦](../Page/瓜拉普阿瓦.md "wikilink")

  - [英国](../Page/英国.md "wikilink")[沃金](../Page/沃金.md "wikilink")

  - [美国](../Page/美国.md "wikilink")[康涅狄格州新不列颠](../Page/新不列颠_\(康涅狄格州\).md "wikilink")

## 圖片

<File:Schloss> Rastatt- Rückansicht.jpg|拉施塔特宮殿 <File:Murgbrücke> Rastatt
Rheintalbahn.JPG|穆格爾河
[File:Rastatter-Bernhardusbrunnen.jpg|市內一處噴泉](File:Rastatter-Bernhardusbrunnen.jpg%7C市內一處噴泉)
[File:Mercedes-Benz-Werk-Rastatt.jpg|平治車廠鳥瞰圖](File:Mercedes-Benz-Werk-Rastatt.jpg%7C平治車廠鳥瞰圖)

[R](../Category/巴登-符腾堡州市镇.md "wikilink")