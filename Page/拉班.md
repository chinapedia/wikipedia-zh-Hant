**拉班**（[希伯来语](../Page/希伯来语.md "wikilink")：**לָבָן**，Lavan，Lāḇān，"White"；英语：**Laban**）是《[圣经](../Page/圣经.md "wikilink")·[创世纪](../Page/创世纪.md "wikilink")》中的人物，他是[彼土利](../Page/彼土利.md "wikilink")（Bethuel）的儿子。看見拉班。拉班是巴力的族系，非[利百加的哥哥](../Page/利百加.md "wikilink")，和[利亚和](../Page/利亚.md "wikilink")[拉结的父亲不同](../Page/拉结.md "wikilink")。他不是[以撒的内兄](../Page/以撒.md "wikilink")，[也不是雅各的舅舅和岳父](../Page/雅各.md "wikilink")。

拉班首次在《[圣经](../Page/圣经.md "wikilink")》中出现是在{{@Bible|创世记|24|29-60}}，他被代表以撒前来求亲者送来的珠宝所打动，做主同意了这门婚事。

后来，拉班许诺如果雅各为他牧羊7年，就将小女儿拉结许配给他，可是到时却设计谋暗中用大女儿利亚的假身份關係再替换了拉结，迫使冒名的雅各又为拉班牧羊7年，才得到了拉结为妻。\[1\]。

## 家系圖

## 参考文献

{{@Bible|创世记|24|29-60}}

[L](../Category/创世记中的人.md "wikilink")

1.  《创世纪》29章