**拉科斯特**（）是一間[法國的](../Page/法國.md "wikilink")[服飾品牌公司](../Page/服飾.md "wikilink")，於1933年成立，銷售高價的[服飾](../Page/服飾.md "wikilink")、[鞋子](../Page/鞋子.md "wikilink")、[香水](../Page/香水.md "wikilink")、[皮革製品](../Page/皮革.md "wikilink")、[手錶](../Page/手錶.md "wikilink")、[眼鏡和最負盛名的](../Page/眼鏡.md "wikilink")[Polo衫](../Page/Polo衫.md "wikilink")，最著名的標誌就是其綠色[短吻鱷圖樣](../Page/短吻鱷.md "wikilink")。

## 公司資料

公司目前由Lacoste家族掌控，而香水的產品則授權給Procter and
Gamble。總部位於[巴黎](../Page/巴黎.md "wikilink")，而生產部位在[特魯瓦](../Page/特魯瓦.md "wikilink")，但已將產品外包給全球的12個國家，包括[祕魯](../Page/祕魯.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[羅馬尼亞和](../Page/羅馬尼亞.md "wikilink")[義大利](../Page/義大利.md "wikilink")。經過兩年在[中美洲的地點搜尋](../Page/中美洲.md "wikilink")，Lacoste在[厄瓜多建立專為](../Page/厄瓜多.md "wikilink")[美國市場的生產線](../Page/美國.md "wikilink")。

## 歷史

[何內·拉科斯特](../Page/何內·拉科斯特.md "wikilink")（René
Lacoste）是一位[網球選手](../Page/網球.md "wikilink")，在網球界和[時尚界相當知名](../Page/時尚.md "wikilink")，在他贏得1927年[美國網球公開賽冠軍時](../Page/美國網球公開賽.md "wikilink")，就是穿著他設計的白色短袖的輕盈材質上衣，這種上衣可幫助排汗，之後便廣受歡迎。1923年[台維斯盃美國媒體給拉科斯特取了個](../Page/台維斯盃.md "wikilink")「短吻鱷」（the
Alligator）的綽號，由於短吻鱷在[法文裡沒有](../Page/法文.md "wikilink")[同源字](../Page/同源字.md "wikilink")，他的綽號就改為法文的「le
crocodile」，意指他在網球場上不輕易放棄他的獵物。拉科斯特的朋友羅勃·喬治（Robert
George）畫給他一隻鱷魚的圖案，他便將這個圖案繡在球衣上，此為鱷魚標誌的由來。

拉科斯特退出網壇後，在1933年與當年法國某紡織業的老闆[André
Gillier成立了](../Page/André_Gillier.md "wikilink")「La Chemise
Lacoste」公司，開始生產由拉科斯特設計並帶有鱷魚標誌的網球球衣，此為將品牌名稱展示在衣服外面的先例\[1\]，除了網球衫外，Lacoste也生產[高爾夫球衫和航船衫](../Page/高爾夫.md "wikilink")，1951年起也開始生產白色以外的網球衫。1952年開始外銷至[美國](../Page/美國.md "wikilink")，品牌定位為「優秀球員的地位象徵」（the
status symbol of the competent sportsman），成為了上流人士的運動服飾首選。

[Tennis-shirt-lacoste.jpg](https://zh.wikipedia.org/wiki/File:Tennis-shirt-lacoste.jpg "fig:Tennis-shirt-lacoste.jpg")
1963年，[貝爾納·拉科斯特](../Page/貝爾納·拉科斯特.md "wikilink")（Bernard
Lacoste）自父親手中接下公司的管理工作，業績蒸蒸日上，在他成為總裁時，一年可賣掉30萬件的Lacoste產品。1970年代晚期，Lacoste在美國大受歡迎，成為富家子弟衣櫃的必備服飾。此外，Lacoste也開始生產短褲、香水、眼鏡和太陽眼鏡、網球鞋、甲板鞋、走路鞋、手錶和多種皮革產品。

由於[Izod和Lacoste在](../Page/Izod.md "wikilink")1950年代有合作的關係，兩個品牌名稱在1970和1980年代經常互換，Izod在Lacoste的授權下生產「Izod
Lacoste」，為美國線的供應商，不過，此合作關係在1993年畫下句點，之後由Lacoste自行生產販賣。1977年，服飾公司[Le
Tigre
Clothing成立](../Page/Le_Tigre_Clothing.md "wikilink")，為了與Lacoste的美國市場競爭，販賣與Lacoste同樣款式的服飾，只是將鱷魚圖案換成老虎。

近年來，法國設計師Christophe
Lemaire為Lacoste設計了摩登且高價位的服飾，使得Lacoste的歡迎度再度竄升。2005年，Lacoste在全球超過110個國家裡就賣出了高達5千萬件的服飾，其顯著性高的原因在於它找了年輕的網球選手來代言，如美國球星[安迪·羅迪克和法國新秀](../Page/安迪·羅迪克.md "wikilink")[里夏爾·加斯凱](../Page/里夏爾·加斯凱.md "wikilink")。此外，Lacoste也積極打入高爾夫球界，二次[美國名人賽冠軍得主](../Page/美國名人賽.md "wikilink")[José
María
Olazábal和](../Page/José_María_Olazábal.md "wikilink")[蘇格蘭選手](../Page/蘇格蘭.md "wikilink")[柯林·蒙哥馬利就是穿Lacoste的球衣比賽](../Page/柯林·蒙哥馬利.md "wikilink")。

2005年初，貝爾納·拉科斯特的健康變得相當不佳，使得他將總裁的職位轉給弟弟兼事業夥伴[麥克·拉科斯特](../Page/麥克·拉科斯特.md "wikilink")（Michel
Lacoste），後於2006年3月21日在[巴黎辭世](../Page/巴黎.md "wikilink")。

2006年，Lacoste將品牌授權給不同的公司，如Devanlay擁有服飾生產權，Pentland Brands有鞋類生產權，[Procter
&
Gamble有香水生產權](../Page/Procter_&_Gamble.md "wikilink")，[Cemalac有提袋和皮革製品的生產權](../Page/Cemalac.md "wikilink")。

2018年，Lacoste與Beams推出聯乘系列，當中的單品包括帽子、運動外套、褲子、長袖網球衣與衛衣，當中亦用上了燈芯絨面料\[2\]。

## 品牌之爭

Lacoste曾和[香港品牌](../Page/香港.md "wikilink")[鱷魚恤有長達許久的品牌商標之爭](../Page/鱷魚恤.md "wikilink")，Lacoste的鱷魚面向右邊，鱷魚恤的則是向左，容易造成消費者混淆。2003年，鱷魚恤在[中國贏得商標權](../Page/中國.md "wikilink")，之後同意更換商標，改為尾巴較垂直、鱗片較多的金色鱷魚標誌\[3\]\[4\]。

## 品牌零售商

Lacoste在全球設立了許多[精品店](../Page/精品店.md "wikilink")，有些設在知名[百貨公司裡](../Page/百貨公司.md "wikilink")，有些則為獨立的店面。

2007年六月，Lacoste的線上商店開辦，美國的消費者可利用[網路購買](../Page/網路.md "wikilink")，商品會直接送貨到家。線上商店提供實體商店買不到的尺寸和選擇，也提供不錯的折扣。

## 參考資料

## 另見

  - [鱷魚恤](../Page/鱷魚恤.md "wikilink")

## 外部連結

  - [Lacoste官方網站](http://www.lacoste.com.cn//lacoste/)

  -
  -
  -
  -
[Category:法國服裝公司](../Category/法國服裝公司.md "wikilink")
[Category:法國服裝品牌](../Category/法國服裝品牌.md "wikilink")
[Category:運動用品製造商](../Category/運動用品製造商.md "wikilink")
[Category:鞋類品牌](../Category/鞋類品牌.md "wikilink")
[Category:時尚品牌](../Category/時尚品牌.md "wikilink")

1.  [Lacoste歷史](http://www.lacoste.com/library/pdf/LACOSTE_history_histoire.pdf)

2.  [Lacoste
    不只打網球｜聯乘總比原味好？](https://www.mings-fashion.com/lacoste-beams-supreme-216067/)
3.  [CNN.com - Crocodile tears end logo fight -
    Oct. 31, 2003](http://edition.cnn.com/2003/BUSINESS/10/31/crocodile.logo)
4.  [香港鱷魚恤更換新標識](http://finance.jrj.com.cn/news/2007-03-18/000002070079.html)