**拉赫蒂足球俱乐部**（**FC
Lahti**）是[芬兰足球俱乐部](../Page/芬兰.md "wikilink")，位于芬兰小城[拉赫蒂](../Page/拉赫蒂.md "wikilink")。俱乐部成立于1996年，球队从1997年开始角逐职业联赛，1998赛季结束后球队得以晋级芬兰顶级足球联赛。球队现在属于[芬兰足球超级联赛球队](../Page/芬兰足球超级联赛.md "wikilink")。球队主体育场为拉赫蒂体育场（Lahden
Stadion）。

## 著名球员

  - [托马斯·哈帕拉](../Page/托马斯·哈帕拉.md "wikilink")
  - [马尔科·克里斯塔](../Page/马尔科·克里斯塔.md "wikilink")
  - [尼亚吉·库齐](../Page/尼亚吉·库齐.md "wikilink")
  - [佩卡·拉格布罗姆](../Page/佩卡·拉格布罗姆.md "wikilink")
  - [亚里·利特马宁](../Page/亚里·利特马宁.md "wikilink")
  - [佩特里·帕萨宁](../Page/佩特里·帕萨宁.md "wikilink")
  - [米卡·瓦依莱宁](../Page/米卡·瓦依莱宁.md "wikilink")
  - [米切尔·斯拉乌塔](../Page/米切尔·斯拉乌塔.md "wikilink")
  - [因德雷克·柴林斯基](../Page/因德雷克·柴林斯基.md "wikilink")

## 外部链接

  - [Official website](http://www.fclahti.fi/)

  - [LahtiFutis.net](http://lahtifutis.net/)

[Category:芬兰足球俱乐部](../Category/芬兰足球俱乐部.md "wikilink")