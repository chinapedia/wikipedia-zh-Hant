**拉金德拉·普拉萨德**（ **डा॰ राजेन्द्र प्रसाद**，**Rajendra
Prasad**，）是[印度共和國第一任總統](../Page/印度共和國.md "wikilink")（1950年－1962年）。

普拉萨德出生於中等[地主家庭](../Page/地主.md "wikilink")，加爾各答法律學院畢業，曾在高等法院當律師。1916年創辦《比哈爾法律週報》（Bihar
Law
Weekly）。1920年參加[不合作運動](../Page/不合作運動.md "wikilink")。他用英文在《探照燈》（Searchlight）上撰文，積極宣揚[民族主義](../Page/民族主義.md "wikilink")。

[Category:印度總統](../Category/印度總統.md "wikilink")
[Category:印度國大黨黨員](../Category/印度國大黨黨員.md "wikilink")
[Category:印度獨立運動人物](../Category/印度獨立運動人物.md "wikilink")
[Category:加爾各答總統大學校友](../Category/加爾各答總統大學校友.md "wikilink")
[Category:印度素食主義者](../Category/印度素食主義者.md "wikilink")
[Category:印度印度教徒](../Category/印度印度教徒.md "wikilink")
[Category:比哈爾邦人](../Category/比哈爾邦人.md "wikilink")