**拉魯县**（**LaRue County,
Kentucky**）是位於[美國](../Page/美國.md "wikilink")[肯塔基州中部的一個縣](../Page/肯塔基州.md "wikilink")。面積683平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,373人。縣治[霍金維爾](../Page/霍金維爾.md "wikilink")。该县是美国总统[林肯的出生地](../Page/林肯.md "wikilink")。

成立於1843年3月4日。縣名紀念拓荒者、地主約翰·P·拉魯 （John P. LaRue）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[L](../Category/肯塔基州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.