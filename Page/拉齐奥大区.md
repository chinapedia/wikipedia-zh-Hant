**拉齐奥**（，）是[意大利的一个大区](../Page/意大利.md "wikilink")，其政府所在地为[罗马](../Page/罗马.md "wikilink")。又譯為**拉提雍**或**拉丁姆**。

## 历史

  -
    *参见：[拉丁姆](../Page/拉丁姆.md "wikilink")*

拉齐奥的名称来自于，即“拉丁人之地”，是古罗马发源的地方。[东哥特人征服意大利后](../Page/东哥特人.md "wikilink")，拉齐奥成为[东哥特王国的一部分](../Page/东哥特王国.md "wikilink")。554年成为[拜占庭帝国控制下的](../Page/拜占庭帝国.md "wikilink")[罗马大公国的一部分](../Page/罗马大公国.md "wikilink")，728年[教宗获得这一地区](../Page/教宗.md "wikilink")。从[英诺森三世开始的历代教宗逐渐加强了教会在拉齐奥的权力](../Page/英诺森三世.md "wikilink")，直到16世纪中期拉齐奥并入[教皇国](../Page/教皇国.md "wikilink")。1870年[攻占罗马后](../Page/攻占罗马.md "wikilink")，[意大利王国控制了拉齐奥](../Page/意大利王国.md "wikilink")。

1927年，原属于[翁布里亚的列蒂省加入了拉齐奥](../Page/翁布里亚.md "wikilink")。

## 行政区划

<table>
<thead>
<tr class="header">
<th><p>省份</p></th>
<th><p>面积（平方千米）</p></th>
<th><p>人口</p></th>
<th><p>人口密度<br />
（每平方千米）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/弗罗西诺内省.md" title="wikilink">弗罗西诺内省</a></p></td>
<td><p>3,244</p></td>
<td><p>496,545</p></td>
<td><p>153.1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉蒂纳省.md" title="wikilink">拉蒂纳省</a></p></td>
<td><p>2,251</p></td>
<td><p>543,844</p></td>
<td><p>241.4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列蒂省.md" title="wikilink">列蒂省</a></p></td>
<td><p>2,749</p></td>
<td><p>158,545</p></td>
<td><p>57.7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅馬首都廣域市.md" title="wikilink">羅馬首都廣域市</a></p></td>
<td><p>5,352</p></td>
<td><p>4,097,085</p></td>
<td><p>765.5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/维泰博省.md" title="wikilink">维泰博省</a></p></td>
<td><p>3,612</p></td>
<td><p>314,690</p></td>
<td><p>87.1</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [拉吉歐大區](http://www.regione.lazio.it/)

[\*](../Category/拉齐奥大区.md "wikilink")