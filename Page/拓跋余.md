**拓跋-{余}-**（），鮮卑名**可博真**，[北魏](../Page/北魏.md "wikilink")[太武帝](../Page/太武帝.md "wikilink")[拓跋燾之子](../Page/拓跋燾.md "wikilink")，生母[闾左昭仪](../Page/闾左昭仪.md "wikilink")。北魏皇帝，為[中常侍](../Page/中常侍.md "wikilink")[宗愛所立](../Page/宗愛.md "wikilink")，但同年又被其殺害，所在位232天。

## 生平

拓跋余生年不详。[太平真君三年](../Page/太平真君.md "wikilink")（442年），拓跋余獲封為**吳王**。[正平元年](../Page/正平_\(北魏太武帝\).md "wikilink")（451年）九月，太武帝南征，以拓跋余留守[平城](../Page/平城.md "wikilink")。十二月改封**南安王**。

正平二年二月五日（452年3月11日），中常侍宗愛弒[太武帝](../Page/太武帝.md "wikilink")，[尚书左仆射](../Page/尚书左仆射.md "wikilink")[兰延](../Page/兰延.md "wikilink")、[侍中吴兴公](../Page/侍中.md "wikilink")[和疋](../Page/和疋.md "wikilink")、侍中太原公[薛提等秘不发丧](../Page/薛提.md "wikilink")。兰延、和疋认为太武帝嫡孙[拓跋濬冲幼](../Page/拓跋濬.md "wikilink")，欲立太武帝在世长子，于是召东平王[拓跋翰](../Page/拓跋翰_\(东平王\).md "wikilink")，置于秘室。薛提则坚持立拓跋濬，兰延等犹豫未决。宗爱得知。宗爱曾得罪拓跋晃，素来厌恶拓跋翰，却和拓跋-{余}-关系好，于是秘密迎拓跋-{余}-从中宫便门入宫，矫[皇后令征召兰延等](../Page/太武皇后.md "wikilink")，斩于殿堂，再杀死拓跋翰，立拓跋-{余}-为帝，为太武帝发丧，改元[承平](../Page/承平_\(北魏\).md "wikilink")（或作[永平](../Page/永平_\(北魏\).md "wikilink")），[大赦](../Page/大赦.md "wikilink")。

拓跋-{余}-即位後，因自己不是作为先帝长子继位，即厚待群下以取悅眾人，以宗爱为大司马、大将军、太师、都督中外诸军事，领中秘书，封冯翊王，以兄长原太子[拓跋晃辅臣](../Page/拓跋晃.md "wikilink")[尚书令](../Page/尚书令.md "wikilink")[古弼为](../Page/古弼.md "wikilink")[司徒](../Page/司徒.md "wikilink")，兼[太尉](../Page/太尉.md "wikilink")[张黎为太尉](../Page/張黎_\(北魏\).md "wikilink")，羽林中郎、幢将乌程子建威将军[吕罗汉典宿卫](../Page/吕罗汉.md "wikilink")。但他也徹夜暢飲，夜夜笙歌，很快即令國庫空虛，又多次出獵，即使邊境有事，亦不加体恤，百姓皆憤怒，而他不作改變。南朝宋文帝[刘义隆遣将](../Page/刘义隆.md "wikilink")[檀和之侵犯](../Page/檀和之.md "wikilink")[济州](../Page/济州_\(北魏\).md "wikilink")，拓跋余令侍中、尚书左仆射、征南将军[韩茂讨之](../Page/韩茂.md "wikilink")，檀和之遁走。

另外，宗愛自拓跋-{余}-登位後掌權日久，朝野內外皆忌憚他，而拓跋-{余}-則懷疑宗愛另有所圖，密謀削奪宗愛權力，宗愛於是于十月一日（10月29日）使小黄门贾周等乘夜趁拓跋-{余}-祭祀东庙而杀之（《宋书》作与宗爱同被兄[拓跋谭所杀](../Page/拓跋谭.md "wikilink")，疑误），隐秘其事，只有羽林中郎[刘尼知道](../Page/刘尼.md "wikilink")。

拓跋余被杀后，百官不知道立谁为新君，刘尼、吕罗汉等迎立拓跋濬为[北魏文成帝](../Page/北魏文成帝.md "wikilink")，诛杀宗爱。文成帝以王禮安葬拓跋-{余}-，諡為**隱王**。

先前太平真君七年（439年）八月，月犯荧惑；八月至十一月，又犯轩辕。是岁正月，太白经天。九月火犯太微。这些星象被认为是从拓跋晃去世到拓跋余被杀一系列事的预兆。

## 參考資料

  - 《魏書·卷十八·拓跋余傳》
  - 《北史·卷十六·拓跋余傳》
  - 《宋書·卷九十五·索虜傳》

## 形象

<table style="width:14%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>劇名</p></td>
<td><p>演員</p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/錦繡未央.md" title="wikilink">錦繡未央</a>》(2016年電視劇)</p></td>
<td><p><a href="../Page/吴建豪.md" title="wikilink">吴建豪</a></p></td>
</tr>
</tbody>
</table>

[Category:北魏皇帝](../Category/北魏皇帝.md "wikilink")
[6](../Category/魏太武帝皇子.md "wikilink")
[Category:中國被弒帝王](../Category/中國被弒帝王.md "wikilink")