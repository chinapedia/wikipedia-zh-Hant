**招商局港口控股有限公司**（**China Merchants Port Holdings Company
Limited**，）是[招商局集團屬下](../Page/招商局集團.md "wikilink")[旗艦公司](../Page/旗艦公司.md "wikilink")，創辦於1992年，同年以[紅籌公司名義於](../Page/紅籌股.md "wikilink")[香港交易所上市的綜合企業公司](../Page/香港交易所.md "wikilink")，是首家在香港上市的紅籌公司。在2004年9月亦成為香港33隻[恒生指數成份股](../Page/恒生指數.md "wikilink")（[藍籌股](../Page/藍籌股.md "wikilink")）公司之一。公司舊名**招商局國際**。

招商局港口主要業務是[港口業務](../Page/港口.md "wikilink")、[集裝箱生產及相關業務](../Page/集裝箱.md "wikilink")，收費[公路業務](../Page/公路.md "wikilink")、[房地產業務及](../Page/房地產.md "wikilink")[油輪業務](../Page/油輪.md "wikilink")。公司在[香港註冊](../Page/香港.md "wikilink")，主席為[付剛鋒](../Page/付剛鋒.md "wikilink")，但是已把油漆製造和公路業務出售。

2018年6月20日招商局港口與深圳赤灣進行資產重組，根據協議[深圳赤灣港航將於](../Page/深圳赤灣港航.md "wikilink")246.5億元人民幣，向招商局投資發展收購招商局港口12.69億股，相當於38.72%股權，為了支付上述收購，深圳赤灣將發行11.48億股A股給招商局集團，相當於64.05%股權。完成重組之後，招商局集團將間接持有深圳赤灣87.8%股權，並將維持為招商局港口的最終控股股東。

## 公司業務

### 港口業務

  - 香港
      - [現代貨箱碼頭有限公司](../Page/現代貨箱碼頭有限公司.md "wikilink")：是[香港](../Page/香港.md "wikilink")[葵涌貨櫃碼頭](../Page/葵涌貨櫃碼頭.md "wikilink")4個集裝箱碼頭營運商之一，現時擁有並經營葵涌碼頭的1號、2號、5號及9號（南）碼頭。
        (間接持有27.01%股份)\[1\]
      - 招商局貨櫃服務有限公司：位於[香港](../Page/香港.md "wikilink")[青衣島](../Page/青衣島.md "wikilink")，提供24小時多元化的貨櫃處理服務，為香港最主要的[中流作業企業之一](../Page/中流作業.md "wikilink")。
      - 港瑞物流有限公司
  - [蛇口](../Page/蛇口.md "wikilink")
      - [蛇口集裝箱碼頭](../Page/蛇口集裝箱碼頭.md "wikilink")(MEGA SCT
        Ltd)：是[深圳市最早的一家專業化集裝箱碼頭](../Page/深圳市.md "wikilink")。由招商局國際及香港[現代貨箱碼頭公司共同擁有](../Page/現代貨箱碼頭.md "wikilink")(招商局國際佔80%權益)。
      - [赤灣集裝箱碼頭有限公司已出售](../Page/赤灣集裝箱碼頭有限公司.md "wikilink")
      - [深圳赤灣港航股份有限公司已出售](../Page/深圳赤灣港航.md "wikilink")
      - 招商港務（深圳）有限公司
      - [深圳海星港口發展有限公司](../Page/深圳海星港口發展有限公司.md "wikilink") (67%權益)
      - [深圳媽灣項目](../Page/深圳媽灣項目.md "wikilink") (70%權益)已出售
      - 深圳市招商局海運物流有限公司
      - 深圳前海灣物流園區
      - [南山开发集团](../Page/南山开发集团.md "wikilink")

<!-- end list -->

  - [中山](../Page/中山.md "wikilink")
      - [中山港](../Page/中山港.md "wikilink")51%股權
  - 2017年9月11日旗下附屬赤灣港航以總代價4.85億元人民幣(約5.8億港元)，向中航投資收購中山港集團51%股權。

<!-- end list -->

  - [漳州](../Page/漳州.md "wikilink")
      - 漳州招商局碼頭有限公司：是[廈門灣主要的集裝箱及散雜貨碼頭](../Page/廈門灣.md "wikilink")。
  - 青島
      - [青島前灣聯合集裝箱碼頭公司](../Page/青島前灣聯合集裝箱碼頭公司.md "wikilink") (50%)
  - [寧波](../Page/寧波.md "wikilink")
      - 寧波大榭招商國際碼頭有限公司
  - [上海](../Page/上海.md "wikilink")
      - 上海國際港務（集團）股份有限公司
  - [天津](../Page/天津.md "wikilink")
      - 天津五洲國際集裝箱碼頭有限公司
  - [多哥](../Page/多哥.md "wikilink")
  - 2012年8月30日斥資1.5億歐羅（約14.6億港元）收購Thesar

Maritime
Limited(TML)50%股權，據多哥政府於去年底授出的特許協議，TML擁有開發及營運洛美集裝箱碼頭項目35年特許權，另可再延續10年

  - [吉布提港](../Page/吉布提港.md "wikilink")
  - 2012年12月30日招商局國際收購位於吉布提港口之合營Port de Djibouti S.A
    23.5%股權，現金代價1.85億美元。

<!-- end list -->

  - [法國](../Page/法國.md "wikilink")
  - 2013年1月25日招商局國際斥資4億歐元（約41.7億港元）收購持有法國、摩洛哥、馬耳他、美國、象牙海岸、比利時、中國及韓國各地的集裝箱港口不同比例權益的法國企業Terminal
    Link49%股權

Terminal Link現時由法國大型航運集團CMA CGM（法國達飛海運集團）全資持有

  - [巴西](../Page/巴西.md "wikilink")
  - [巴拉那瓜港](../Page/巴拉那瓜港.md "wikilink")
  - 2017年9月5日招商局港口斥72.28億港元收購巴西TCP的90％股權，後者主要在巴拉那瓜港口營運巴西第二大集裝箱碼頭

<!-- end list -->

  - [斯里蘭卡](../Page/斯里蘭卡.md "wikilink")
  - [漢班托塔港](../Page/漢班托塔港.md "wikilink")
  - 2017年7月25日斯里蘭卡將和招商局港口簽署一單11.6億美元的協議，其中招商局港口斥资9.74億美元收購漢班托塔港國際港口集團85%股份，餘下1.46億美元將存入斯里蘭卡政府銀行帳戶，用於港口及運相關業務，特許經營協議為期99年

<!-- end list -->

  - [澳洲](../Page/澳洲.md "wikilink")
  - [紐卡索港](../Page/紐卡索港.md "wikilink")50%
  - 2018年2月6日招商局港口以6.075億澳元，相等于约38.09億港元向主要股東及[招商局集團的聯繫人](../Page/招商局集團.md "wikilink")）及Gold
    Newcastle Property Pty Holding Limited （'Gold
    Newcastle'，CMU的全資附屬公司）收購澳洲東岸最大的港口Port of
    Newcastle[紐卡索港](../Page/紐卡索港.md "wikilink")50%权益

### 海运物流信息化

  - 招商局国际信息技术有限公司（简称：招商国际信息；英文缩写：CMHIT；原名：港航网络；）成立于2001年，注册资金已达五千万。由招商局国际有限公司和深圳赤湾港航股份有限公司共同投资组建，是招商局国际有限公司旗下唯一一家专门研发港口系统，从事海运物流信息化服务和提供海运物流信息化解决方案的高科技企业。发展至今，主要服务与中国深圳西部港区。自主研发的码头操作管理系统，已达到国际先进水平。

### 集裝箱製造

  - 中國國際海運集裝箱（集團）股份有限公司：是全球規模最大、品種最齊全的集裝箱製造集團，目前國際市場份額超過50%，招商局國際是其單一最大股東。

### 收費公路業務

  - 招商局亞太有限公司：新加坡上市公司，擁有位於[中國](../Page/中華人民共和國.md "wikilink")[廣西自治區的](../Page/廣西自治區.md "wikilink")[桂柳高速公路](../Page/桂柳高速公路.md "wikilink")40％股權、[貴州省](../Page/貴州省.md "wikilink")[貴黃公路](../Page/貴黃公路.md "wikilink")60％股權、[廣東省](../Page/廣東省.md "wikilink")[羅梅公路](../Page/羅梅公路.md "wikilink")33.4%股權、[浙江省](../Page/浙江省.md "wikilink")[甯鎮駱公路](../Page/甯鎮駱公路.md "wikilink")60%股權（2006年3月31日以1.675億港元出售）及[余姚公路](../Page/余姚公路.md "wikilink")60％股權。於2007年5月23日以總對價29.5億港元出售悉數權益予招商局集團間接全資附屬公司──招商局香港有限公司。

香港西區隧道有限公司：香港行車隧道公司，於1994至96年動工興建，為通往香港島之第三者行車隧道。

## 參考資料

## 外部連結

  - [cmport.com.hk](http://www.cmport.com.hk/)
  - [CMA CGM (Hong Kong) Limited - 香港中文網站主頁](http://www.cmport.com.hk/)

[分類:前恒生指數成份股](../Page/分類:前恒生指數成份股.md "wikilink")

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:香港物流公司](../Category/香港物流公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:招商局](../Category/招商局.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:香港船公司](../Category/香港船公司.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")

1.  <http://www.cmhi.com.hk/Upload/9/images/2008ann_rpt_c.pdf>