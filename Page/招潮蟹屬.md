**招潮蟹屬**（學名：**），又名**招潮屬**、**提琴手蟹**\[1\]，属中包含95个[種](../Page/種_\(生物\).md "wikilink")。其最大的特徵是[雄蟹擁有一大一小相差懸殊的一對](../Page/雄.md "wikilink")[螯](../Page/螯.md "wikilink")。

## 分類

招潮屬原為[沙蟹科](../Page/沙蟹科.md "wikilink")[招潮亞科之下的一個屬](../Page/招潮亞科.md "wikilink")，2005年招潮亞科獲提升至科級，屬[沙蟹總科](../Page/沙蟹總科.md "wikilink")。

## 得名

### 中文

招潮蟹會做出舞動大螯的動作，像是在招喚[潮水似的](../Page/潮水.md "wikilink")，所以得名；這動作也像在拉小提琴，所以又叫作「提琴手蟹」。\[2\]

## 外觀

[Fiddler_crab_anatomy-en.svg](https://zh.wikipedia.org/wiki/File:Fiddler_crab_anatomy-en.svg "fig:Fiddler_crab_anatomy-en.svg")
招潮蟹有一對火柴棒般突出的眼睛。雄蟹擁有一隻大[螯和一支小螯](../Page/螯.md "wikilink")，大螯其中一隻跟蟹身其餘部份差不多大，而且顏色鮮艷，有特別的圖案；重達該蟹總體重的一半，長度可為該蟹甲殼直徑的三倍以上。\[3\]雌蟹外觀上除了兩隻螯都是很小的之外，基本上和雄蟹一樣。

## 分佈

  - 臺灣：文獻記載，目前臺灣有11種招潮蟹，分別為[粗腿招潮](../Page/粗腿招潮.md "wikilink")、[糾結招潮](../Page/糾結招潮.md "wikilink")、[乳白招潮](../Page/乳白招潮.md "wikilink")、[弧邊招潮](../Page/弧邊招潮.md "wikilink")、[四角招潮](../Page/四角招潮.md "wikilink")、[三角招潮](../Page/三角招潮蟹.md "wikilink")、[北方招潮](../Page/北方招潮.md "wikilink")、[屠氏招潮](../Page/屠氏招潮蟹.md "wikilink")、[窄招潮](../Page/窄招潮.md "wikilink")、[台灣招潮](../Page/台灣招潮.md "wikilink")、[賈瑟琳招潮及近日在](../Page/賈瑟琳招潮.md "wikilink")[澎湖再度被發現](../Page/澎湖.md "wikilink")(原被視為無效物種)的[麗彩招潮蟹](../Page/麗彩招潮蟹.md "wikilink")，此舉已讓台灣招潮蟹增至12種\[4\]。
  - [大西洋砂招潮](../Page/大西洋砂招潮.md "wikilink")（*[Uca
    pugilator](../Page/Uca_pugilator.md "wikilink")*）是美國東岸常見的物種。

## 圖集

<center>

[File:Shiomaneki070224.jpg|弧邊招潮蟹](File:Shiomaneki070224.jpg%7C弧邊招潮蟹)*Uca
arcuata* <File:fiddler> crab burrows.jpg|招潮蟹在紅樹林根部的洞穴

</center>

## 注釋與參考資料

## 外部連結

  -
[招潮属](../Category/招潮属.md "wikilink")

1.  [1](http://tamsui.yam.org.tw/tsec/tsr-1-5.htm)[英文為](../Page/英文.md "wikilink")：fiddler
    crab

2.
3.

4.