**拜倫龍屬**（[學名](../Page/學名.md "wikilink")：*Byronosaurus*）是[傷齒龍科](../Page/傷齒龍科.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[上白堊紀](../Page/上白堊紀.md "wikilink")。牠的屬名是紀念拜倫先生（Byron
Jaffe），以感謝他及其家人對[蒙古科學院](../Page/蒙古科學院.md "wikilink")、[美國自然歷史博物館的](../Page/美國自然歷史博物館.md "wikilink")[古生物學挖掘團隊的支持與援助](../Page/古生物學.md "wikilink")。拜倫龍的第一個標本於1993年在[蒙古](../Page/蒙古.md "wikilink")[戈壁沙漠的](../Page/戈壁沙漠.md "wikilink")[烏哈托喀被發現](../Page/烏哈托喀.md "wikilink")，第二個則於1996年在大約8公里以外的地方被發現。

## 古生物學

拜倫龍是小型、敏捷的[恐龍](../Page/恐龍.md "wikilink")，約只有1.5米長及50厘米高。牠的體重約有4公斤\[1\]。不像其他的[傷齒龍科](../Page/傷齒龍科.md "wikilink")，拜倫龍的[牙齒是沒有鋸齒的](../Page/牙齒.md "wikilink")，呈針狀，適合捕捉小型的[鳥類](../Page/鳥類.md "wikilink")、[蜥蜴及](../Page/蜥蜴.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")。這些牙齒與[始祖鳥最為相似](../Page/始祖鳥.md "wikilink")。

## 化石

[Juvenile_Deinonychosaur.jpg](https://zh.wikipedia.org/wiki/File:Juvenile_Deinonychosaur.jpg "fig:Juvenile_Deinonychosaur.jpg")
目前發現了兩個拜倫龍的成年[化石](../Page/化石.md "wikilink")，都是[頭顱骨](../Page/頭顱骨.md "wikilink")。其中一個長23公分的標本，保存狀況比其他已發現的[傷齒龍科頭顱骨更為良好](../Page/傷齒龍科.md "wikilink")。這個頭顱骨在鼻端有空間，可以讓[空氣先從](../Page/空氣.md "wikilink")[鼻孔進入再經過口部](../Page/鼻孔.md "wikilink")，這是與現今[鳥類相似的特徵](../Page/鳥類.md "wikilink")。

在1994年，[馬克·諾瑞爾](../Page/馬克·諾瑞爾.md "wikilink")（Mark
Norell）等人曾經研究過兩個幼體標本（編號IGM 100/972、IGM
100/974）。這兩個標本發現於[蒙古的](../Page/蒙古.md "wikilink")[德加多克塔組](../Page/德加多克塔組.md "wikilink")（Djadochta
Formation），鄰近火焰崖，位於一個[偷蛋龍類的蛋巢內](../Page/偷蛋龍類.md "wikilink")。由於其中一顆蛋內有偷蛋龍類胚胎，幾乎可以確定這個蛋巢是屬於偷蛋龍類。當時，諾瑞爾等人將這兩個部分顱骨歸類於[馳龍科](../Page/馳龍科.md "wikilink")。在2009年，諾瑞爾等人經過重新研究後，將這兩個部分顱骨改歸類於拜倫龍\[2\]\[3\]。這兩個顱骨屬於幼年個體，鄰近者蛋殼碎片。科學家無法解釋為何幼年拜倫龍會出現在偷蛋龍類的蛋巢。目前的相關解釋有：幼年拜倫龍成為偷蛋龍類的獵物、牠們以剛出生的的偷蛋龍類為食、拜倫龍有依賴、與其他動物共生的習性\[4\]。

## 參考

  - Makovicky, P.J., Norell, M.A., Clark, J.M., and Rowe, T.E. (2003).
    [Osteology and relationships of *Byronosaurus jaffei* (Theropoda:
    Troodontidae).](https://web.archive.org/web/20070316124611/http://fm1.fieldmuseum.org/aa/Files/pmakovicky/Makovicky_et_al_2003.pdf)
    *American Museum Novitates* **3402**:1-32.
  - [Dinosauria.com](https://web.archive.org/web/20060208092122/http://www.dinosauria.com/dml/names/dinob.htm)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:傷齒龍科](../Category/傷齒龍科.md "wikilink")

1.  Montague, R. (2006) "[Estimates of body size and geological time of
    origin for 612 dinosaur genera (Saurischia,
    Ornithischia)](http://apt.allenpress.com/perlserv/?request=get-abstract&doi=10.1043%2F0098-4590%282006%29069%5B0243%3AEOBSAG%5D2.0.CO%3B2)".
    *Florida Scientist* 69(4):243–257
2.  Bever, G.S. and Norell, M.A. (2009). "The perinate skull of
    *Byronosaurus* (Troodontidae) with observations on the cranial
    ontogeny of paravian theropods." *American Museum Novitates*,
    **3657**: 51 pp.
3.
4.