**拜州**（[索馬里語](../Page/索馬里語.md "wikilink")：）是[索馬里南部的一個州](../Page/索馬里.md "wikilink")，位於[朱巴河和](../Page/朱巴河.md "wikilink")[謝貝利河之間](../Page/謝貝利河.md "wikilink")。面積35,156平方公里。首府[拜达博](../Page/拜达博.md "wikilink")。

[Baay](../Category/索馬里行政區劃.md "wikilink")
[\*](../Category/拜州.md "wikilink")