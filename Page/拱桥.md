[Bridge_2_(4277567395).jpg](https://zh.wikipedia.org/wiki/File:Bridge_2_\(4277567395\).jpg "fig:Bridge_2_(4277567395).jpg")\]\]
[Ponte_Vecchio_at_dusk_1.JPG](https://zh.wikipedia.org/wiki/File:Ponte_Vecchio_at_dusk_1.JPG "fig:Ponte_Vecchio_at_dusk_1.JPG")
**拱橋**是指以[拱作为主要承重结构的](../Page/拱.md "wikilink")[桥梁](../Page/桥.md "wikilink")。

最早出现的拱橋是石拱桥，藉著類似梯形石頭的小單位，將橋本身的重量和加諸其上的載重，水平傳遞到兩端的橋墩。各個小單位互相推擠時，同時也增加了橋體本身的強度。近现代的拱桥则更多的使用混凝土或钢材建造。

現存最早的石拱橋是約公元前1300年前的[邁錫尼文明的](../Page/邁錫尼文明.md "wikilink")[Arkadiko
bridge](../Page/:en:Arkadiko_bridge.md "wikilink")\[1\]
\[2\]；最早並還在使用的石拱桥則是前850年的[卡雷凡橋](../Page/卡雷凡橋.md "wikilink")\[3\]。

有些[高速公路或是較長的橋可能由好幾段拱橋連接而成](../Page/高速公路.md "wikilink")，雖然現在已經有其他更符合經濟成本的結構可供選擇。歷史上著名的拱桥包括羅馬的[输水道系統](../Page/高架渠.md "wikilink")、中国建于[隋朝的](../Page/隋朝.md "wikilink")[趙州橋](../Page/趙州橋.md "wikilink")。

## 參考文獻

## 參見

  - [橋](../Page/橋.md "wikilink")

## 外部連結

  - [同济大学 －
    拱桥介绍](https://web.archive.org/web/20080226184428/http://bridge.tongji.edu.cn/chinabridge/107.htm)
  - [NOVA Online - Super Bridge - Arch
    Bridges](http://www.pbs.org/wgbh/nova/bridge/meetarch.html)
  - [Matsuo Bridge Co. - Arch
    Bridges](https://web.archive.org/web/20050302094119/http://www.matsuo-bridge.co.jp/english/bridges/basics/arch.shtm)

[拱橋](../Category/拱橋.md "wikilink")

1.  [Hellenic Ministry of Culture: Mycenaean bridge at
    Kazarma](http://odysseus.culture.gr/h/2/eh251.jsp?obj_id=1710)
2.  Nakassis, Athanassios (2000): "The Bridges of Ancient Eleutherna",
    *The Annual of the British School at Athens*, Vol. 95, pp. 353–365
3.  [10 Record-Breaking
    Bridges](http://travel.yahoo.com/ideas/10-record-breaking-bridges-080000812.html)