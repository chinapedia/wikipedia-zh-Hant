**拿破崙四世**，即**拿破仑·欧仁·路易·让·约瑟夫·波拿巴**（；），是[法兰西第二帝国](../Page/法兰西第二帝国.md "wikilink")[拿破仑三世与其妻](../Page/拿破仑三世.md "wikilink")的独生子，是[帝国的皇太子](../Page/法国王太子.md "wikilink")（），也被称为“**拿破仑四世**”。

## 生平

在[普法战争爆发初期](../Page/普法战争.md "wikilink")，欧仁陪伴他的父亲到前线，在[萨尔布吕肯与普军正面交锋](../Page/萨尔布吕肯.md "wikilink")。不过，随着法军屡见败绩，欧仁逃离[法国](../Page/法国.md "wikilink")，与母亲一起到达[英格兰](../Page/英格兰.md "wikilink")[肯特郡的](../Page/肯特郡.md "wikilink")

(今[大伦敦东南部](../Page/大伦敦.md "wikilink"))。他父亲在1873年于该地去世后，[波拿巴主义者宣称欧仁为](../Page/波拿巴主义.md "wikilink")“拿破仑四世”。在流亡英国期间，英国皇室曾考虑他与[维多利亚女王的最幼女](../Page/维多利亚女王.md "wikilink")[比阿特丽丝公主的婚事](../Page/贝翠丝公主_\(英国\).md "wikilink")。

欧仁后来就任[英军的军官](../Page/英国陆军.md "wikilink")，并自愿到[祖鲁兰出征](../Page/祖鲁兰.md "wikilink")。在侦察期间，他被[祖鲁人突袭并刺死](../Page/祖鲁人.md "wikilink")。证据显示他生前曾以[左轮手枪反抗](../Page/左轮手枪.md "wikilink")，直到弹药耗尽。他的死讯令法國震惊，他與祖國的軍事聯繫上溝通不好，因為他是[波拿巴家族恢复皇朝的最后希望](../Page/波拿巴家族.md "wikilink")。祖鲁人后来宣称如果当时他有表現出他是法國皇室，就不会杀了他。之后一度有传言沸沸扬扬，暗指英国女王维多利亚故意安排了欧仁的这场意外，理由是与祖鲁武士遭遇时，陆军少尉凯里和他的部下距离欧仁仅有50码远，但却一枪未发撤回营地。

欧仁严重腐烂的尸体最后被运返英国，在下葬。后来，尸体被转移到其母亲下令在[汉普郡建造的陵墓](../Page/汉普郡.md "wikilink")，与父亲同葬。在欧仁去世之前，欧仁指定由堂弟[拿破仑·维克托·波拿巴继任为波拿巴家族之主](../Page/拿破仑·维克托·波拿巴.md "wikilink")，却不是维克托之父亲、欧仁之叔父[拿破仑·约瑟夫·夏尔·保罗·波拿巴](../Page/拿破仑·约瑟夫·夏尔·保罗·波拿巴.md "wikilink")。

[小行星卫星](../Page/小行星卫星.md "wikilink")[小王子在](../Page/小王子_\(小行星卫星\).md "wikilink")1998年被发现。它的名字最初为小说《[小王子](../Page/小王子_\(小说\).md "wikilink")》而设，后来也用来纪念欧仁·波拿巴，因为它围绕的正是根据欧仁的母亲命名的[小行星45](../Page/小行星45.md "wikilink")
(45欧仁妮)。

## 称号

  - *His Imperial Highness The Prince Imperial* (1856-70年) 皇太子陛下 : 帝國王子
  - *His Imperial Highness* Louis Napoléon, Prince Imperial of France
    (1870-73年) 皇太子陛下 : 法蘭西帝國王子路易拿破崙
  - *His Imperial Highness* Prince Imperial Napoléon, Head of the
    Imperial House of France (1873-79年) 皇太子陛下 : 法蘭西帝國皇室的王子路易拿破崙

## 参考文献

  - Morris, Donald R. *The Washing of the Spears*. [Simon and
    Schuster](../Page/Simon_and_Schuster.md "wikilink"), 1965, pp
    511-545.

## 其他阅读材料

  - Ellen Barlee, *Life of Napoleon, Prince Imperial of France*,
    (London, 1889)
  - M. d'Hérrison, *Le prince impérial*, (Paris, 1890)
  - André Martinet, *Le prince impérial*, (Paris, 1895)
  - R. Minon, *Les derniers jours du prince impérial sur le continent*,
    (Paris, 1900)
  - Ernest Barthez, *Empress Eugenie and her Circle*, (New York, 1913)

## 參閱

  - [法國王位繼承人](../Page/法國王位繼承人.md "wikilink")

## 外部链接

  - [The South African Military History Society The Prince
    Imperial](http://www.rapidttp.com/milhist/vol032aw.html)


{| align="center" cellpadding="2" border="2" |- | width="30%"
align="center" | 前任
**[拿破仑三世](../Page/拿破仑三世.md "wikilink")**
<small>(法国人民的皇帝) | width="40%" align="center" |
**[拿破仑家族之主](../Page/拿破仑家族.md "wikilink")** |
width="30%" align="center" | 继任
**[拿破崙五世](../Page/拿破崙五世.md "wikilink")**
<small>(拿破仑家族之主) |}

[Category:法国储君](../Category/法国储君.md "wikilink")
[Category:法国军事人物](../Category/法国军事人物.md "wikilink")
[Category:英国军官](../Category/英国军官.md "wikilink")
[Category:法国战争身亡者](../Category/法国战争身亡者.md "wikilink")
[E](../Category/波拿巴家族.md "wikilink")
[Category:从未继位的推定继承人](../Category/从未继位的推定继承人.md "wikilink")