**納蘇郡**（**Nassau County,
Florida**）是[美國](../Page/美國.md "wikilink")[佛羅里達州最東北的一個縣](../Page/佛羅里達州.md "wikilink")，東傍[大西洋](../Page/大西洋.md "wikilink")，北鄰[喬治亞州](../Page/喬治亞州.md "wikilink")。面積1,880平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口57,663。縣治[費南迪納比奇](../Page/費南迪納比奇.md "wikilink")。

成立於1824年12月29日。縣名是紀念昔日德意志的[拿騷公國](../Page/拿騷_\(公國\).md "wikilink")。納蘇郡的经济非常多样化，中西部是以树农场为主的农业。其他经济活动包括房地产、建筑业、法律服务、医护、旅游业。当地还有一稀有动物的动物园。

## 参考资料

<http://en.wikipedia.org/wiki/Nassau_County,_Florida>

[N](../Category/佛羅里達州行政區劃.md "wikilink")