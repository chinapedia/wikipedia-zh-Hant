**持續性性興奮症候群**（，[縮寫](../Page/縮寫.md "wikilink")：、**陰蒂異常勃起**），患者會在沒有任何外來[性刺激或者存有](../Page/性刺激.md "wikilink")[性需要的情況下](../Page/性需要.md "wikilink")，持續而不由自主地感到[生殖器官處於](../Page/生殖器官.md "wikilink")[性興奮狀態](../Page/性興奮.md "wikilink")，甚至連續達到多次[性高潮](../Page/性高潮.md "wikilink")。然而，症狀不一定因為感受了高潮而消失，興奮的感覺會於短時間後恢復。部份患者的感覺有可能持續多日至數星期而不消退，一般日常生活的事務（如乘車）都有可能誘發症狀，嚴重影響到患者的日常生活。

一直以來，與持續性性興奮症候群相關的[資料及](../Page/資料.md "wikilink")[醫學](../Page/醫學.md "wikilink")[研究不多](../Page/研究.md "wikilink")，其原因有待證實。可以肯定的是，持續性性興奮症候群與[性傾向及](../Page/性傾向.md "wikilink")[縱慾無關係](../Page/縱慾.md "wikilink")。然而，由於患者的情況特殊，不少患者因為羞於啟齒，默默忍受徵狀所帶來的不便多年，卻無法改善自身的病況；故此持續性性興奮症候群對患者的心理影響有可能比較生理影響更為甚要。

## 成因

  - 連接生殖器官的[盤骨血管異常](../Page/盤骨.md "wikilink")
  - 腦部神經異常\[1\]
  - 生殖器官周圍的神經受壓\[2\]
  - [血液循環失調](../Page/血液循環.md "wikilink")\[3\]
  - 曾經服用屬於[選擇性血清素再吸收抑制劑](../Page/選擇性血清素再吸收抑制劑.md "wikilink")（SSRIs）的抗[憂鬱症藥物](../Page/憂鬱症.md "wikilink")
  - 生理改變（如手術後）\[4\]

## 徵狀

  - 生殖器官充血腫脹

## 治療

### 藥物

未有解藥之前，持續性性興奮症候群一直未有明確的治療方法，沿用的治療法有麻醉性藥物等。2013年11月17日，英國《每日郵報》指出土耳其有醫師成功利用肉毒桿菌製成解藥，可有效減輕持續性性興奮症候群患者的病情。\[5\]

### 心理

患有持續性性興奮症候群后，多為[自卑](../Page/自卑.md "wikilink")、自我形象低的人，因此要戒癮都得先尋求精神科醫師做心理評估，然後配合醫師的心理治療，培養自己多元的興趣來提升自信，以減低心理上因持續性性興奮症候群所控制之病情。\[6\]

## 参考文献

## 外部連結

  - [雅虎上的PSAS支援小組](http://groups.yahoo.com/group/psas_group/)
  - [Boston
    Globe關於PSAS的專輯](http://www.boston.com/yourlife/health/women/articles/2003/11/11/sexual_syndrome_that_takes_joy_out_of_life/)
  - [24Drs.ETtoday.com](http://24drs.ettoday.com/international/detail.asp?no=3161&page=4)
  - [Dr. Lin網頁關於PSAS的治療及個案](http://www.actionlove.com/extra/psas.htm)
  - [Persistent Sexual Arousal Syndrome (Website: English and
    Dutch)](http://www.psas.nl/)

[Category:婦科疾病](../Category/婦科疾病.md "wikilink")
[女](../Category/性兴奋.md "wikilink")
[女](../Category/性高潮.md "wikilink")
[女](../Category/症候群.md "wikilink")
[女](../Category/性功能障碍.md "wikilink")

1.
2.
3.
4.

5.

6.