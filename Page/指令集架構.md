**指令集架構**（，縮寫為ISA），又稱**指令集**或**指令集体系**，是[计算机体系结构中與](../Page/计算机体系结构.md "wikilink")[程序設計有關的部分](../Page/程序設計.md "wikilink")，包含了[基本数据类型](../Page/資料型別.md "wikilink")，指令集，[寄存器](../Page/寄存器.md "wikilink")，[寻址模式](../Page/寻址模式.md "wikilink")，[存储体系](../Page/存储体系.md "wikilink")，[中斷](../Page/中斷.md "wikilink")，[異常處理以及外部](../Page/異常處理.md "wikilink")[I/O](../Page/I/O.md "wikilink")。指令集架構包含一系列的[opcode即操作码](../Page/opcode.md "wikilink")（[機器語言](../Page/機器語言.md "wikilink")），以及由特定處理器执行的基本命令。

指令集体系与[微架构](../Page/微架构.md "wikilink")（一套用于执行指令集的微处理器设计方法）不同。使用不同微架構的電腦可以共享一种指令集。例如，[Intel的](../Page/英特爾.md "wikilink")[Pentium和](../Page/奔騰.md "wikilink")[AMD的](../Page/超微半導體.md "wikilink")[AMD
Athlon](../Page/AMD_Athlon.md "wikilink")，兩者几乎採用相同版本的[x86指令集体系](../Page/x86.md "wikilink")，但是兩者在内部设计上有本质的区别。

一些虛擬機器支持基于[Smalltalk](../Page/Smalltalk.md "wikilink")，[Java虛擬機](../Page/Java虛擬機.md "wikilink")，微軟的[公共語言运行时虛擬機所生成的](../Page/公共語言运行时.md "wikilink")[字节码](../Page/字节码.md "wikilink")，他們的指令集体系將bytecode（字节码）从作为一般手段的代码路径翻譯成本地的機器語言，并通过解译执行并不常用的代码路径，[全美達以相同的方式开发了基于x](../Page/全美達.md "wikilink")86指令体系的[VLIW處理器](../Page/VLIW.md "wikilink")。

## 指令集的分类

[复杂指令集计算机包含许多应用程序中很少使用的特定指令](../Page/复杂指令集计算机.md "wikilink")，由此产生的缺陷是指令长度不固定。[精简指令集计算机通过只执行在程序中经常使用的指令来简化处理器的结构](../Page/精简指令集计算机.md "wikilink")，而特殊操作则以子程序的方式实现，它们的特殊使用通过处理器额外的执行时间来弥补。理论上的重要类型还包括与[单指令集计算机](../Page/單一指令集.md "wikilink")，但都未用作商业处理器。另外一种衍生类型是[超长指令字](../Page/超长指令字.md "wikilink")，处理器接受许多经过编码的指令并通过检索提取出一个指令字并执行。

## 機器語言

機器語言是由*声明*和*指令*所組成的。在处理结构上，一個特定指令指明了以下几个部分：

  - 用于算术运算，寻址或者控制功能的特定[寄存器](../Page/寄存器.md "wikilink")；
  - 特定存储空间的地址或偏移量；
  - 用于解译操作数的特定[寻址模式](../Page/寻址模式.md "wikilink")；

複雜的操作可以藉由將簡單的指令合併而達成，可以（在[冯·诺依曼体系中](../Page/冯·诺依曼体系.md "wikilink")）連續的執行，也可以藉[控制流來執行指令](../Page/控制流.md "wikilink")。

### 指令类型

有效的指令操作須包含：

  - 数据處理與存储操作
      - 將[暫存器的值](../Page/暫存器.md "wikilink")（在中央處理器作为高速缓存的存储空间）設為固定值；
      - 將数据从存储空间中传送至寄存器，反之亦然。用于将数据取出并执行计算，或者将计算结果予以保存；
      - 從硬體设备读取或寫入数据。
  - [算术逻辑单元](../Page/算术逻辑单元.md "wikilink")
      - 對兩個儲存於暫存器的數字進行**add**，**subtract**，**multiply**，**divide**,將結果放到一個暫存器內，一個或是更多的[状态码可能被設置在](../Page/旗標.md "wikilink")中；
      - 执行[位操作](../Page/位操作.md "wikilink")，藉對兩組數字（為兩串的數字，都由零與一構成，分別儲存於兩個暫存器內）執行**[邏輯與](../Page/邏輯與.md "wikilink")**和**[邏輯或](../Page/邏輯或.md "wikilink")**，或者对寄存器的每一位執行**[邏輯非](../Page/邏輯非.md "wikilink")**操作；
      - **比較**兩个寄存器中的数据（例如是大于或者相等）；
  - [控制流](../Page/控制流.md "wikilink")
      - **[分支](../Page/分支_\(計算機科學\).md "wikilink")**，跳跃至程序某地址并执行相应指令；
      - **[条件分支](../Page/条件表达式.md "wikilink")**，假設某一條件成立，就跳到程序的另一個位置；
      - **[間接分支](../Page/間接分支.md "wikilink")**，在跳到另一個位置之前，將現在所執行的指令的下一個指令的位置儲存起來，作為[子程式執行完返回的地址](../Page/子程式.md "wikilink")；

### 複雜指令

一些電腦在他們的指令集架構內包含複雜指令。複雜指令包含：

  - 將許多暫存器存成堆疊的形式。
  - 移動記憶體內大筆的資料。
  - 複雜或是浮點數運算（[正弦](../Page/正弦.md "wikilink")，[餘弦](../Page/餘弦.md "wikilink")，[平方根等等](../Page/平方根.md "wikilink")）
  - 執行[test-and-set指令](../Page/test-and-set.md "wikilink")。
  - 執行數字存在記憶體而非暫存器的運算

有一種複雜指令[單指令流多資料流](../Page/單指令流多資料流.md "wikilink")，英文全名是Single-Instruction
Stream Multiple-Data
Stream。或是[向量指令](../Page/並行向量處理機.md "wikilink")，這是一種可以在同一時間對多筆資料進行相同運算的操作。SIMD有能力在短時間內將大筆的向量和矩陣計算完成。SIMD指令使[平行計算變得簡單](../Page/並行計算.md "wikilink")，各種SIMD指令集被開發出來，例如[MMX](../Page/MMX.md "wikilink")，[3DNow\!以及](../Page/3DNow!.md "wikilink")[AltiVec](../Page/AltiVec.md "wikilink")。

### 指令的組成

[Mips32_addi.svg](https://zh.wikipedia.org/wiki/File:Mips32_addi.svg "fig:Mips32_addi.svg")

在傳統的架構上，一條指令包含[op
code](../Page/op_code.md "wikilink")，表示運算的方式，以及零個或是更多的[運算元](../Page/運算元.md "wikilink")，有些像是運算元的數字可能指的是[暫存器的編號](../Page/暫存器.md "wikilink")，還有記憶體位置，或是文字資料。

在[超長指令字](../Page/超長指令字.md "wikilink")（VLIW）的結構中，包含了許多[微指令](../Page/微指令.md "wikilink")，藉此將複雜的指令分解為簡單的指令。

### 指令的長度

指令長度的範圍可以說是相當廣泛，從[微控制器的](../Page/微控制器.md "wikilink")4
bit，到[VLIW系統的數百bit](../Page/超長指令字.md "wikilink")。在[個人電腦](../Page/個人電腦.md "wikilink")，[大型電腦](../Page/大型電腦.md "wikilink")，[超級電腦內的處理器](../Page/超級電腦.md "wikilink")，其內部的指令長度介於8到64
bits（在x86處理器結構內，最長的指令長達15 bytes，等於120
bits）。在一個指令集架構內，不同的指令可能會有不同長度。在一些結構，特別是大部分的[精簡指令集](../Page/精簡指令集.md "wikilink")（RISC），指令是固定的長度，長度對應到結構內一個[字的大小](../Page/字.md "wikilink")。在其他結構，長度則是[byte的整數倍或是一個](../Page/byte.md "wikilink")[halfword](../Page/字.md "wikilink")。

### 設計

對微處理器而有兩種指令集。第一種是[複雜指令集](../Page/複雜指令集.md "wikilink")（Complex
Instruction Set
Computer），擁有許多不同的指令。在1970年代，許多機構，像是IBM，發現有許多指令是不需要的。結果就產生了[精簡指令集](../Page/精簡指令集.md "wikilink")（Reduced
Instruction Set
Computer），它所包含的指令就比較少。精簡的指令集可以提供比較高的速度，使處理器的尺寸縮小，以及較少的電力損耗。然而，比較複雜的指令集較容易使工作更完善，記憶體及[缓存的效率較高](../Page/缓存.md "wikilink")，以及較為簡單的程式碼。

一些指令集保留了一個或多個的opcode，以執行[系統調用或](../Page/系統調用.md "wikilink")[軟體中斷](../Page/中斷.md "wikilink")。

## 指令集的實作

在設計處理器內的[微架構時](../Page/微架構.md "wikilink")，工程師使用藉電路連接的區塊來架構，區塊用來表示加法器，乘法器，計數器，暫存器，算術邏輯單元等等，[暫存器傳遞語言通常被用來描述被解碼的指令](../Page/暫存器傳遞語言.md "wikilink")，指令是藉由微架構來執行指令。
有兩種基本的方法來建構[控制單元](../Page/控制單元.md "wikilink")，藉控制單元，以微架構作為通路來執行指令：

1.  早期的電腦和採用精簡指令集的電腦藉將電路接線（像是微架構剩下的部分）。
2.  其他的裝置使用[微程序來達成](../Page/微程序.md "wikilink")—像是電晶體ROM或PLA（即使RAM已使用很久）。

電腦[微处理器的](../Page/微处理器.md "wikilink")**指令集架構**（Instruction Set
Architecture）常见的有三种：

  - **[复杂指令集运算](../Page/複雜指令集.md "wikilink")**（Complex Instruction Set
    Computing，CISC）

<!-- end list -->

  -
    目前[x86架构微处理器如](../Page/x86.md "wikilink")[Intel的](../Page/Intel.md "wikilink")[Pentium](../Page/Pentium.md "wikilink")/[Celeron](../Page/Celeron.md "wikilink")/[Xeon与](../Page/Xeon.md "wikilink")[AMD的](../Page/AMD.md "wikilink")[Athlon](../Page/Athlon.md "wikilink")/[Duron](../Page/Duron.md "wikilink")/[Sempron](../Page/Sempron.md "wikilink")；以及其64位扩展系统的[x86-64架构的Intel](../Page/x86-64.md "wikilink")
    64的[Intel
    Core](../Page/Intel_Core.md "wikilink")/[Core2](../Page/Core2.md "wikilink")/Pentium/Xeon与AMD64的[Phenom
    II](../Page/Phenom_II.md "wikilink")/[Phenom](../Page/Phenom.md "wikilink")/[Athlon
    64](../Page/Athlon_64.md "wikilink")/[Opteron](../Page/Opteron.md "wikilink")/[Ryzen](../Page/Ryzen.md "wikilink")/[EPYC都属于复杂指令集](../Page/EPYC.md "wikilink")。主要针对的操作系统是[微软的](../Page/微软.md "wikilink")[Windows和](../Page/Windows.md "wikilink")[苹果公司的](../Page/苹果公司.md "wikilink")[OS
    X](../Page/OS_X.md "wikilink")。另外[Linux](../Page/Linux.md "wikilink")，一些UNIX等，都可以运行在x86（复杂指令集）架构的微处理器。

<!-- end list -->

  - **[精简指令集运算](../Page/精简指令集.md "wikilink")**（Reduced Instruction Set
    Computing，RISC）

<!-- end list -->

  -
    这种指令集运算包括惠普的PA-RISC，[国际商业机器的](../Page/国际商业机器.md "wikilink")[PowerPC](../Page/PowerPC.md "wikilink")，[康柏](../Page/康柏.md "wikilink")（後被[惠普收购](../Page/惠普.md "wikilink")）的Alpha，[美普思科技公司的MIPS](../Page/美普思科技.md "wikilink")，SUN公司的SPARC，ARM公司的ARM、Cortex等。目前有UNIX、Linux、MacOS以及包括iOS、Android、WindowsPhone、WindowsRT等在内的大多数移动操作系统运行在精简指令集的处理器上。

<!-- end list -->

  - **[顯式並行指令運算](../Page/顯式並行指令運算.md "wikilink")**（Explicitly Parallel
    Instruction Computing，EPIC）

<!-- end list -->

  -
    顯式並行指令運算乃先进的全新指令集运算，只有Intel的[IA-64架构的纯](../Page/IA-64.md "wikilink")64位微处理器的[Itanium](../Page/Itanium.md "wikilink")/[Itanium
    2](../Page/Itanium_2.md "wikilink")。EPIC指令集运算的IA-64架构主要针对的操作系统是微软64位安腾版的[Windows
    XP以及](../Page/Windows_XP.md "wikilink")64位安腾版的[Windows Server
    2003](../Page/Windows_Server_2003.md "wikilink")。另外一些64位的Linux，一些64位的UNIX也可以运行IA-64（顯式並行指令運算）架构。

<!-- end list -->

  - **[超長指令字指令集运算](../Page/VLIW.md "wikilink")**（VLIW）

<!-- end list -->

  -
    通過將多條指令放入一個指令字，有效的提高了CPU各個計算功能部件的利用效率，提高了程序的性能

## 參見

  - [微架構](../Page/微架構.md "wikilink")
  - [计算机系统结构](../Page/计算机系统结构.md "wikilink")

## 延伸閱讀

  -
## 外部連結

  - [Programming Textfiles: Bowen's Instruction Summary
    Cards](http://www.textfiles.com/programming/CARDS/)
  - [Mark Smotherman's Historical Computer Designs
    Page](http://www.cs.clemson.edu/~mark/hist.html)

[Category:微處理器](../Category/微處理器.md "wikilink")
[Category:指令集架構](../Category/指令集架構.md "wikilink")