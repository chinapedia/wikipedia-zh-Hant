  -
    历史数据见[历史上的城市规模](../Page/历史上的城市规模.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>大都会区</p></th>
<th><p>國家</p></th>
<th><p>人口</p></th>
<th><p>面積 (km<sup>2</sup>)</p></th>
<th><p>人口密度 (人/km<sup>2</sup>)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/東京都會區.md" title="wikilink">東京</a></p></td>
<td></td>
<td><p>35,676,000</p></td>
<td><p>13,556</p></td>
<td><p>2,632</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/首爾.md" title="wikilink">首爾</a></p></td>
<td></td>
<td><p>24,472,063</p></td>
<td><p>11,743</p></td>
<td><p>2,084</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/上海.md" title="wikilink">上海</a></p></td>
<td></td>
<td><p>23,019,100</p></td>
<td><p>6,340</p></td>
<td><p>3,631</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/墨西哥城.md" title="wikilink">墨西哥城</a>[1]</p></td>
<td></td>
<td><p>20,450,000</p></td>
<td><p>7,346</p></td>
<td><p>2,784</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/廣佛同城化.md" title="wikilink">广州-佛山</a>[2]</p></td>
<td></td>
<td><p>19,894,800</p></td>
<td><p>11,283</p></td>
<td><p>1,763</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/紐約市.md" title="wikilink">紐約市</a>[3]</p></td>
<td></td>
<td><p>19,750,000</p></td>
<td><p>17,884</p></td>
<td><p>1,104</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td></td>
<td><p>19,612,000</p></td>
<td><p>16,410</p></td>
<td><p>1,195</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/孟買.md" title="wikilink">孟買</a></p></td>
<td></td>
<td><p>19,200,000</p></td>
<td><p>2,350</p></td>
<td><p>8,170</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/雅加达.md" title="wikilink">雅加达</a></p></td>
<td></td>
<td><p>18,900,000</p></td>
<td><p>5,100</p></td>
<td><p>3,706</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/聖保羅.md" title="wikilink">聖保羅</a></p></td>
<td></td>
<td><p>18,850,000</p></td>
<td><p>8,479</p></td>
<td><p>2,223</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/德里.md" title="wikilink">德里</a></p></td>
<td></td>
<td><p>18,600,000</p></td>
<td><p>3,182</p></td>
<td><p>5,845</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/深圳.md" title="wikilink">深圳</a>-<a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p>17,457,600</p></td>
<td><p>3,057</p></td>
<td><p>5,711</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/京阪神.md" title="wikilink">京阪神</a></p></td>
<td></td>
<td><p>17,375,000</p></td>
<td><p>6,930</p></td>
<td><p>2,507</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/馬尼拉.md" title="wikilink">馬尼拉</a></p></td>
<td></td>
<td><p>16,300,000</p></td>
<td><p>2,521</p></td>
<td><p>6,466</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
<td></td>
<td><p>15,250,000</p></td>
<td><p>10,780</p></td>
<td><p>1,415</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/加爾各答.md" title="wikilink">加爾各答</a></p></td>
<td></td>
<td><p>15,100,000</p></td>
<td><p>1,785</p></td>
<td><p>8,459</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/莫斯科.md" title="wikilink">莫斯科</a></p></td>
<td></td>
<td><p>15,000,000</p></td>
<td><p>14,925</p></td>
<td><p>1,005</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/开罗.md" title="wikilink">开罗</a></p></td>
<td></td>
<td><p>14,450,000</p></td>
<td><p>1,600</p></td>
<td><p>9,031</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/布宜諾斯艾利斯.md" title="wikilink">布宜諾斯艾利斯</a></p></td>
<td></td>
<td><p>13,170,000</p></td>
<td><p>10,888</p></td>
<td><p>1,210</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
<td><p>12,875,000</p></td>
<td><p>11,391</p></td>
<td><p>1,130</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/卡拉奇.md" title="wikilink">卡拉奇</a></p></td>
<td></td>
<td><p>11,800,000</p></td>
<td><p>1,100</p></td>
<td><p>10,727</p></td>
</tr>
</tbody>
</table>

## 按照国家的分布

<table>
<tbody>
<tr class="odd">
<td><p>国家</p></td>
<td><p>列入前100名大都会区的数目</p></td>
<td><p>总人口</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>15</p></td>
<td><p>96,209,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>13</p></td>
<td><p>90,509,526</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>9</p></td>
<td><p>80,100,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8</p></td>
<td><p>52,337,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>49,164,844</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>26,975,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>26,324,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>19,062,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>17,897,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>17,341,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>16,544,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>14,905,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>14,898,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>12,550,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>11,624,807</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>11,491,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>11,317,587</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>11,193,721</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>10,886,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>10,686,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>10,294,752</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>9,229,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>8,982,171</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>8,952,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>8,187,398</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2</p></td>
<td><p>7,957,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2</p></td>
<td><p>7,605,306</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>7,314,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>6,593,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>6,239,745</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>6,049,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>5,904,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>5,683,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>4,518,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>4,326,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>4,300,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>4,193,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>4,107,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>3,894,573</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>3,577,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>3,351,000</p></td>
</tr>
</tbody>
</table>

__NOTOC__

## 参考注释区

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

## 参见

  - [按人口排列的世界城市列表](../Page/按人口排列的世界城市列表.md "wikilink")

## 外部链接

### 其他列表

  - [PopulationData.net: all 1M+ inhabitants Metropolitan
    Areas](http://www.populationdata.net/palmaresvilles.php)
  - [Demographia.com: 50 Largest World Metropolitan
    Areas](http://www.demographia.com/db-world-metro2000.htm)
  - [Citypopulation.de: The Principal Agglomerations of the
    World](http://citypopulation.de/World.html)
  - [World Gazetteer: Metropolitan
    Areas](https://web.archive.org/web/20070930220511/http://www.world-gazetteer.com/wg.php?x=1131897520&men=gcis&lng=en&gln=xx&des=gamelan&dat=32&srt=npan&col=ohdq&pt=a&va=&srt=pnan)

### 延伸阅读

  - [Largest Cities Through
    History](http://geography.about.com/library/weekly/aa011201a.htm)
  - [Canada Metropolitan
    Areas](http://www.demographia.com/db-cancma.htm) Census 2001.
  - [United States Metropolitan
    Areas](http://www.demographia.com/db-metmic2004.pdf) County Based
    Metropolitan Area Estimates Based on Census Bureau Data 2004.

[ar:قائمة أكبر بلديات العالم كثافة
سكانية](../Page/ar:قائمة_أكبر_بلديات_العالم_كثافة_سكانية.md "wikilink")
[de:Liste der größten Städte der
Welt](../Page/de:Liste_der_größten_Städte_der_Welt.md "wikilink")
[eo:Plej grandaj urboj laŭ
enloĝantaro](../Page/eo:Plej_grandaj_urboj_laŭ_enloĝantaro.md "wikilink")
[id:Daftar wilayah metropolitan berdasarkan
populasi](../Page/id:Daftar_wilayah_metropolitan_berdasarkan_populasi.md "wikilink")
[pt:Lista de regiões metropolitanas por
população](../Page/pt:Lista_de_regiões_metropolitanas_por_população.md "wikilink")

[Category:城市列表](../Category/城市列表.md "wikilink")
[Category:大都会区](../Category/大都会区.md "wikilink")
[Category:人口列表](../Category/人口列表.md "wikilink")

1.  Consists of two separate [zona
    metropolitanas](../Page/Metropolitan_areas_of_Mexico.md "wikilink"):
    the Zona Metropolitana del Valle de Mexico (ZMVM) and the Zona
    Metropolitana de Toluca (ZMT)
2.  There are restrictions on the link of metro transit and construction
    between Guangzhou and Foshan and the two areas are sometimes listed
    separately in other lists.
3.  Consists of two separate [metropolitan statistical
    areas](../Page/metropolitan_statistical_area.md "wikilink") (MSA):
    the New York-Northern New Jersey Long Island MSA and the
    Bridgeport-Stamford-Norwalk MSA.