**[挪威国旗](../Page/挪威.md "wikilink")**由靛藍色[十字架加上白色邊和周围的红色组成](../Page/十字架.md "wikilink")。在白色十字中加入藍色十字，於是誕生了三种颜色的國旗。其中红白蓝三色象徵[自由與](../Page/自由.md "wikilink")[獨立](../Page/獨立.md "wikilink")。

## 设计

<table>
<thead>
<tr class="header">
<th><p>绘制方法</p></th>
<th><p>标准</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Norway_with_proportions.svg" title="fig:Flag_of_Norway_with_proportions.svg">Flag_of_Norway_with_proportions.svg</a><br />
</p>
<center>
<p><small>民用旗</small></p>
</center>
<p><br />
<a href="https://zh.wikipedia.org/wiki/File:Flag_of_Norway,_state_with_proportions.svg" title="fig:Flag_of_Norway,_state_with_proportions.svg">Flag_of_Norway,_state_with_proportions.svg</a><br />
</p>
<center>
<p><small>政府旗與軍旗</small></p>
</center></td>
<td><ul>
<li>挪威民用国旗的长宽比为22:16，各颜色的水平分布比例为 6:1:2:1:12，纵向分布比例为 6:1:2:1:6。</li>
<li>挪威政府旗的长宽比为27:16，各颜色的水平分布比例为 6:1:2:1:6:11，纵向分布比例为 6:1:2:1:6。</li>
</ul>
<ul>
<li>色彩以<a href="../Page/Pantone.md" title="wikilink">彩通表示</a>，红色为PMS 032 U，蓝色为PMS 281 U。</li>
<li>以RGB表示的近似值为红色为#EF2B2D，蓝色为#002664。</li>
</ul></td>
</tr>
</tbody>
</table>

## 历史的旗幟

Flag of Denmark.svg|1814年以前 Flag of Norway (1814–1821).svg|1814年-1821年
Norge-Unionsflagg-1844.svg|1844年-1905年

## 外部連結

  - [挪威國旗格式與國旗變遷](https://web.archive.org/web/20060323002636/http://www.vexilla-mundi.com/norway.htm)

[旗](../Category/挪威国家标志.md "wikilink") [N](../Category/国旗.md "wikilink")
[Category:北欧十字旗](../Category/北欧十字旗.md "wikilink")