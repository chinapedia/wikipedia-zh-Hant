**振興醫療財團法人振興醫院**，位於[臺北市](../Page/臺北市.md "wikilink")[北投區](../Page/北投區.md "wikilink")，一般直稱**振興醫院**或基於地理位置稱作**北投振興醫院**或**石牌振興醫院**。創立於西元1967年，原為專門收容[小兒麻痺的復健醫院](../Page/小兒麻痺.md "wikilink")，現今則是以[心臟移植及](../Page/心臟移植.md "wikilink")[心臟外科聞名的私立綜合醫院](../Page/心臟外科.md "wikilink")。

## 歷史

1964年，[第一夫人](../Page/第一夫人.md "wikilink")[蔣宋美齡派專機迎接在](../Page/蔣宋美齡.md "wikilink")[韓國訪問的世界復健基金會主席](../Page/韓國.md "wikilink")[郝華德洛斯克等](../Page/郝華德洛斯克.md "wikilink")5位專家來[臺北市訪問](../Page/臺北市.md "wikilink")，並在近郊[石牌里研商](../Page/石牌里_\(臺北市\).md "wikilink")、籌備設院計畫，因而成立振興育幼院，並於1967年改成立振興復健醫學中心開始收容因[小兒麻痺而成為](../Page/小兒麻痺.md "wikilink")[殘障的兒童](../Page/殘障.md "wikilink")。提供醫療救援與[復健](../Page/復健.md "wikilink")。2009年，更名為振興醫療財團法人振興醫院。

1967年至1993年期間為小兒麻痺及其他殘障兒童專治醫院，免費提供外科矯治、職能治療、聽語治療、心理治療、復健護理及社工服務，同時也提供職前訓練以及義肢支架裝配，每年服務病患平均達千人次。

1994年延聘[三軍總醫院](../Page/三軍總醫院.md "wikilink")[魏崢醫師及其醫療團隊](../Page/魏崢.md "wikilink")，開始開心及換心手術，並轉型成現代化[綜合醫院](../Page/綜合醫院.md "wikilink")。

### 歷任院長

#### 復健中心時期

  - [張先林](../Page/張先林.md "wikilink")
  - [彭達謀](../Page/彭達謀.md "wikilink")
  - [鄧述微](../Page/鄧述微.md "wikilink")
  - [趙尚良](../Page/趙尚良.md "wikilink")

#### 綜合醫院時期

  - [魏崢](../Page/魏崢.md "wikilink")
  - [韓韶華](../Page/韓韶華.md "wikilink")
  - [張心湜](../Page/張心湜.md "wikilink")
  - [韓韶華](../Page/韓韶華.md "wikilink")
  - [劉榮宏](../Page/劉榮宏.md "wikilink")
  - [李壽東](../Page/李壽東.md "wikilink")
  - [魏崢](../Page/魏崢.md "wikilink")

## 服務

振興醫院因具備[軍醫色彩](../Page/軍醫.md "wikilink")，在轉型成綜合醫院普及醫療服務前，多數當地人並不清楚該醫院有對外服務，並保留貴族醫院形象。直到[全民健保實施後](../Page/全民健保.md "wikilink")，就醫人數才大為增加，同時也廣聘各科別醫師應診，並成為[區域](../Page/區域醫院.md "wikilink")[教學醫院](../Page/教學醫院.md "wikilink")。

### 急重症醫學部

[重症科](../Page/重症科.md "wikilink")、[急診科](../Page/急診科.md "wikilink")、[災難醫學科](../Page/災難醫學科.md "wikilink")

### 復健醫學部

[運動復健科](../Page/運動復健科.md "wikilink")、[神經復健科](../Page/神經復健科.md "wikilink")

### [精神醫學部](../Page/精神醫學.md "wikilink")

[身心治療科](../Page/身心治療科.md "wikilink")、[一般精神科](../Page/一般精神科.md "wikilink")、[老年精神科](../Page/老年精神科.md "wikilink")、[兒童青少年暨婦女身心醫學科](../Page/兒童青少年暨婦女身心醫學科.md "wikilink")、[社會工作室](../Page/社會工作室.md "wikilink")、[臨床心理室](../Page/臨床心理室.md "wikilink")、[職能治療室](../Page/職能治療室.md "wikilink")

### 家庭暨社區醫學部

[家庭醫學科](../Page/家庭醫學科.md "wikilink")、[社區醫學科](../Page/社區醫學科.md "wikilink")

### 健康管理中心

[一般健檢科](../Page/一般健檢科.md "wikilink")、[健康管理科](../Page/健康管理科.md "wikilink")、[健康管理影像醫學科](../Page/健康管理影像醫學科.md "wikilink")

### 兒童醫學部

[新生兒科](../Page/新生兒科.md "wikilink")、[一般兒科](../Page/一般兒科.md "wikilink")、[小兒重症科](../Page/小兒重症科.md "wikilink")

### 內科部

[胃腸科](../Page/胃腸科.md "wikilink")、[腎臟科](../Page/腎臟科.md "wikilink")、[感染科](../Page/感染科.md "wikilink")、[胸腔內科](../Page/胸腔內科.md "wikilink")、[神經內科](../Page/神經內科.md "wikilink")、[一般內科](../Page/一般內科.md "wikilink")、[新陳代謝科](../Page/新陳代謝科.md "wikilink")、[血液腫瘤科](../Page/血液腫瘤科.md "wikilink")、[呼吸治療科](../Page/呼吸治療科.md "wikilink")、[過敏免疫風濕科](../Page/過敏免疫風濕科.md "wikilink")

### 心臟醫學中心

[心臟血管內科](../Page/心臟血管內科.md "wikilink")、[心臟血管外科](../Page/心臟血管外科.md "wikilink")、[心臟加護中心科](../Page/心臟加護中心科.md "wikilink")、[心臟功能重建室](../Page/心臟功能重建室.md "wikilink")、[小兒心臟科](../Page/小兒心臟科.md "wikilink")、[心臟護理科](../Page/心臟護理科.md "wikilink")

### 外科部

[胸腔外科](../Page/胸腔外科.md "wikilink")、[神經外科](../Page/神經外科.md "wikilink")、[整形外科](../Page/整形外科.md "wikilink")、[小兒外科](../Page/小兒外科.md "wikilink")、[一般外科](../Page/一般外科.md "wikilink")、[腫瘤外科](../Page/腫瘤外科.md "wikilink")、[創傷外科](../Page/創傷外科.md "wikilink")、[大腸直腸外科](../Page/大腸直腸外科.md "wikilink")

### 婦產部

[婦科](../Page/婦科.md "wikilink")、[產科](../Page/產科.md "wikilink")、[一般婦產科](../Page/一般婦產科.md "wikilink")、[婦女泌尿科](../Page/婦女泌尿科.md "wikilink")、[生殖內分泌科](../Page/生殖內分泌科.md "wikilink")

### 骨科部

[骨病科](../Page/骨病科.md "wikilink")、[一般骨科](../Page/一般骨科.md "wikilink")、[運動醫學科](../Page/運動醫學科.md "wikilink")、[關節重建科](../Page/關節重建科.md "wikilink")

### 泌尿部

[一般泌尿科](../Page/一般泌尿科.md "wikilink")、[泌尿腫瘤科](../Page/泌尿腫瘤科.md "wikilink")、[微創手術科](../Page/微創手術科.md "wikilink")

### 眼科部

[青光眼科](../Page/青光眼科.md "wikilink")、[視網膜科](../Page/視網膜科.md "wikilink")、[一般眼科](../Page/一般眼科.md "wikilink")

### 耳鼻喉部

[鼻科](../Page/鼻科.md "wikilink")、[耳科](../Page/耳科.md "wikilink")、[聽覺科](../Page/聽覺科.md "wikilink")、[喉及頭頸外科](../Page/喉及頭頸外科.md "wikilink")

### 腫瘤醫學部

[放射腫瘤科](../Page/放射腫瘤科.md "wikilink")、[藥物治療科](../Page/藥物治療科.md "wikilink")、[安寧照護科](../Page/安寧照護科.md "wikilink")

### 核子醫學部

[正子影像中心](../Page/正子影像中心.md "wikilink")、[核子醫學科](../Page/核子醫學科.md "wikilink")

### 影像醫學部

[泌尿放射科](../Page/泌尿放射科.md "wikilink")、[介入治療科](../Page/介入治療科.md "wikilink")、[一般放射科](../Page/一般放射科.md "wikilink")、[乳房與超音波科](../Page/乳房與超音波科.md "wikilink")

### 藥學部

[門診藥學科](../Page/門診藥學科.md "wikilink")、[臨床藥學科](../Page/臨床藥學科.md "wikilink")、[藥事管理科](../Page/藥事管理科.md "wikilink")

### 獨立醫療科

[牙科](../Page/牙科.md "wikilink")、[皮膚科](../Page/皮膚科.md "wikilink")、[營養治療科](../Page/營養治療科.md "wikilink")、[老年醫學科](../Page/老年醫學科.md "wikilink")、[中醫科](../Page/中醫科.md "wikilink")、[加護中心科](../Page/加護中心科.md "wikilink")、[感染管理室](../Page/感染管理室.md "wikilink")

### 護理部

### 麻醉部

[疼痛科](../Page/疼痛科.md "wikilink")、[心臟麻醉科](../Page/心臟麻醉科.md "wikilink")、[一般麻醉科](../Page/一般麻醉科.md "wikilink")

### 病理檢驗部

[臨床病理科](../Page/臨床病理科.md "wikilink")、[解剖病理科](../Page/解剖病理科.md "wikilink")

### 醫品中心

### 教學研究部

### 醫療業務部

[病歷課](../Page/病歷課.md "wikilink")、[櫃台作業課](../Page/櫃台作業課.md "wikilink")、[健康保險課](../Page/健康保險課.md "wikilink")、[社會服務課](../Page/社會服務課.md "wikilink")

### 護理之家

### 管理階層

董事會、稽核室、法務室、秘書室、院長、醫療副院長、行政副院長、教研副院長

### 行政單位

人事室(含人力資源課、人事行政課)、會計室(含帳務課、績效課)、採購室(含採購管理課、採購作業課)、出納室、資材室(含財產課、供應課)、營運中心、總務室(含庶務課、文書課、房務課)、工務室(含醫工課、養護課)、資訊室(含系統應用課、系統工程課)、勞工安全衛生室

### 任務編組

人力資源委員會、人事評議委員會、人體試驗委員會、手術室品質及管理委員會、加護病房管理委員會、母嬰親善醫院推動委員會、生物安全會、全院教育訓練委員會、危機管理委員會、急診醫療品質審查委員會、病理組織委員會、病歷管理暨品質審查委員會、院史委員會、健保遠委員會、健康促進醫院推動與認證委員會、宿舍管理委員會、專科護理師培育暨執業委員會、採購委員會、創傷外科委員會、勞工安全衛生委員會、勞工退休準備金監督委員會、結核病委員會、感染管制委員會、資訊管理委員會、達文西機器手臂委員會、圖書委員會、實驗動物照護及使用委員會、福利委員會、管制藥品管理委員會、緩和暨預立醫療照護委員會、衛材管理委員會、學術倫理委員會、輸血委員會、輻射防護管理委員會、營養委員會、癌症委員會、績效管理委員會、醫事評議委員會暨生育事故關懷小組、醫學研究委員會、醫學教育委員會、醫學倫理委員會、醫療品質暨病人安全委員會、藥事委員會

## 國際合作

  - 1967年
    獲[美國醫藥援華會](../Page/美國醫藥援華會.md "wikilink")（ABMAC）、[世界復健基金會等單位贊助](../Page/世界復健基金會.md "wikilink")，派員赴[菲律賓接受義肢支架製作訓練](../Page/菲律賓.md "wikilink")。
  - 1975年
    獲[美國國際開發總署](../Page/美國國際開發總署.md "wikilink")（AID）透過美國醫藥援華會捐建少年部醫院。
  - 1977年 獲美國國際開發總署透過美國醫藥援華會捐建「兒童部通少年部走廊」及「義肢支架工廠」。
  - 1983年
    與[美國](../Page/美國.md "wikilink")[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[亞力山大市保健服務專家組織合作物理治療員教育交流計畫](../Page/亞力山大市.md "wikilink")。
  - 1983年
    與[韓國](../Page/韓國.md "wikilink")[賀爾德兒童福祉會結盟姊妹院](../Page/賀爾德兒童福祉會.md "wikilink")。
  - 1998年
    與[義大利](../Page/義大利.md "wikilink")[帕維亞大學建教合作](../Page/帕維亞大學.md "wikilink")。
  - 2010年 與越南軍方排名第一的103總醫院合作，並完成越南第一例成功的心臟移植。

## 參見

  - [臺灣醫院列表](../Page/臺灣醫院列表.md "wikilink")

## 外部連結

  - [振興醫療財團法人振興醫院](http://www.chgh.org.tw)

[Category:臺北市醫院](../Category/臺北市醫院.md "wikilink")
[Category:台灣區域醫院](../Category/台灣區域醫院.md "wikilink")
[Category:北投區](../Category/北投區.md "wikilink")
[Category:石牌](../Category/石牌.md "wikilink")
[Category:1967年建立](../Category/1967年建立.md "wikilink")