《**捭阖策**》又名《**鬼谷子**》，相傳是[戰國時代](../Page/戰國.md "wikilink")[鬼谷子所著](../Page/鬼谷子.md "wikilink")，最早見於[明朝](../Page/明朝.md "wikilink")[嘉靖乙已鈔本](../Page/嘉靖.md "wikilink")，內容主要論述外交遊說的技巧。在[四庫全書中為子部雜家類](../Page/四庫全書.md "wikilink")。

## 章節

  - 捭闔第一
  - 反應第二
  - 內揵第三
  - 抵巇第四
  - 飛箝第五
  - 忤合第六
  - 揣篇第七
  - 摩篇第八
  - 權篇第九
  - 謀篇第十
  - 決篇第十一
  - 符言第十二
  - 轉丸（一本作轉丸第十三）
  - 胠亂（一本作胠亂第十四）
  - 本經陰符七術
      - 盛神法五龍
      - 養志法靈龜
      - 實意法螣蛇
      - 分威法伏熊
      - 散勢法鷙鳥
      - 轉圓法猛獸
      - 損兌法靈蓍
  - 持樞
  - 中經

## 考證

鬼谷子三卷，鬼谷先生撰，皇甫謐注。\[1\]\[2\]

## 書目

《鬼谷子集校集注》，許富宏撰，北京：中華書局，2008.12（2011.10重印）。

## 註釋

## 參考文獻

  - 蕭登福《鬼谷子研究》。 2001 文津出版社
  - 陈宇《鬼谷子兵法破解》。 ISBN 7-5065-4584-5
  - Broschat, Michael Robert. "'Guiguzi': A Textual Study and
    Translation". University of Washington PhD Thesis, 1985
  - Chung Se Kimm, "Kuei-Kuh-Tse: Der Philosoph vom Teufelstal". 1927
  - Robert van Gulik: 'Kuei-ku-tzu, The Philosopher of the Ghost Vale",
    "China", XIII, no 2 (May 1939)
  - «Гуй Гу-цзы». В кн: Искусство управления. Сост., пер., вступ. ст. и
    коммент. В.В. Малявина. М.: «Издательство Астрель»: «Издательство
    АСТ», 2003. С.244-318.
  - 大橋武夫著『鬼谷子：国際謀略の原典を読む』（[徳間書店](../Page/徳間書店.md "wikilink")、1982年） ISBN
    4192425963
  - 酒井洋著『鬼谷子の人間学：孫子が超えられなかった男：より巧みに生きる縦横学的発想のすすめ』（太陽企画出版、1993年） ISBN
    488466213X

## 延伸阅读

  -
## 外部連結

  -
[Category:国别体](../Category/国别体.md "wikilink")
[Category:先秦典籍](../Category/先秦典籍.md "wikilink")

1.  [橫秋閣本長孫無忌鬼谷子序](../Page/橫秋閣.md "wikilink")
2.  [晁公武](../Page/晁公武.md "wikilink")[郡齋讀書志](../Page/郡齋讀書志.md "wikilink")