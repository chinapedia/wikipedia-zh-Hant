**掃叭石柱**又叫**掃只石柱**、**舞鶴石柱**，是[台灣一處](../Page/台灣.md "wikilink")[考古遺址](../Page/考古.md "wikilink")，位於[花蓮縣](../Page/花蓮縣.md "wikilink")[瑞穗鄉舞鶴村](../Page/瑞穗鄉.md "wikilink")，緊鄰[花東公路](../Page/花東公路.md "wikilink")（[台九線](../Page/台九線.md "wikilink")）旁。

掃叭石柱有兩根，兩根石柱各長575公分、339公分，各重10.6噸及12.3噸，原本一根橫躺，另一根則是豎立，現今掃叭石柱呈現兩根是豎立狀況，導致原貌遭受人為破壞與保存不力狀況下，難以提供考古，形成一道難解謎題。

在[臺灣日治時期就開始被稱作](../Page/臺灣日治時期.md "wikilink")「掃叭」，[鳥居龍藏對這處遺址的年代歸類為](../Page/鳥居龍藏.md "wikilink")[巨石文化](../Page/巨石文化.md "wikilink")，是目前東部地區所存最大遺石。現今臺灣的考古學界則是認為掃叭遺址是屬於[新石器時代](../Page/新石器時代.md "wikilink")，距今約有三千多年，最主要的文化相是[卑南文化和](../Page/卑南文化.md "wikilink")[麒麟文化](../Page/麒麟文化.md "wikilink")，其中掃叭遺址即屬卑南文化的一支，有一個南北長約六百公尺，東西寬約四百公尺的遺址。

## 參考資料

  -
  -
[category:台灣史前時期](../Page/category:台灣史前時期.md "wikilink")

[S](../Category/台灣考古遺址.md "wikilink")
[Category:花東縱谷國家風景區](../Category/花東縱谷國家風景區.md "wikilink")
[Category:花蓮縣文化資產](../Category/花蓮縣文化資產.md "wikilink")
[Category:瑞穗鄉](../Category/瑞穗鄉.md "wikilink")