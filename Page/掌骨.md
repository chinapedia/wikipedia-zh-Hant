**掌骨**是[手部](../Page/手.md "wikilink")[骨骼中間的部份](../Page/骨骼.md "wikilink")，連接在近端[手指及](../Page/手指.md "wikilink")[腕骨之間](../Page/腕骨.md "wikilink")，而[腕骨再與](../Page/腕骨.md "wikilink")[前臂連結](../Page/前臂.md "wikilink")。

## 掌骨的具體名稱

掌骨由五個圓柱狀的[骨頭組成](../Page/骨頭.md "wikilink")，由[橈骨到](../Page/橈骨.md "wikilink")[尺骨側依序命名](../Page/尺骨.md "wikilink")（*I-V*）。

  - [第一掌骨](../Page/第一掌骨.md "wikilink")（1st metacarpal bone）
  - [第二掌骨](../Page/第二掌骨.md "wikilink")（2nd metacarpal bone）
  - [第三掌骨](../Page/第三掌骨.md "wikilink")（3rd metacarpal bone）
  - [第四掌骨](../Page/第四掌骨.md "wikilink")（4th metacarpal bone）
  - [第五掌骨](../Page/第五掌骨.md "wikilink")（5th metacarpal bone）

## 掌骨的一般特性

每個由一塊主體及兩個極端組成。

## 其他影像

<File:Scheme>` human hand bones-en.svg |人體手部骨骼。`
<File:Metacarpal>` fractures.jpg|掌骨的多重骨折（aka broken hand）。 `
[`File:Gray219.png|左手骨頭，掌面`](File:Gray219.png%7C左手骨頭，掌面)`。`

[`File:Gray334.png|手腕韌帶，前面觀`](File:Gray334.png%7C手腕韌帶，前面觀)`。`
[`File:Gray337.png|掌指骨關節及手指關節，掌面`](File:Gray337.png%7C掌指骨關節及手指關節，掌面)`。`
[`File:Gray338.png|掌指骨關節及手指關節，尺骨側觀`](File:Gray338.png%7C掌指骨關節及手指關節，尺骨側觀)`。`

## 参见

  - [人体骨骼列表](../Page/人体骨骼列表.md "wikilink")

## 外部連結

  - [解剖學(Anatomy)-骨頭-掌骨與腕骨](http://smallcollation.blogspot.com/2013/01/anatomy.html)

[Category:骨骼](../Category/骨骼.md "wikilink")
[Category:手](../Category/手.md "wikilink")