**掛川市**（）是[東海地方中部](../Page/東海地方.md "wikilink")、[靜岡縣西部地區的城市](../Page/靜岡縣.md "wikilink")。為日本最大的綠茶產地。

## 交通

### 鐵道

  - 中央車站：[掛川站](../Page/掛川站.md "wikilink")
  - [JR東海](../Page/東海旅客鐵道.md "wikilink")
      - [東海道本線](../Page/東海道本線.md "wikilink")：－掛川站－
      - [東海道新幹線](../Page/東海道新幹線.md "wikilink")：－掛川站－
  - 天龍濱名湖鐵道：掛川站－掛川市役所前站－西掛川站－櫻木站－憩之廣場站－細谷站－原谷站－原田站－

## 地理

位于[牧之原台地西部](../Page/牧之原台地.md "wikilink")。南北長約30公里、東西寬約16公里。

  - [山](../Page/山.md "wikilink")：[粟岳](../Page/粟岳_\(静岡縣\).md "wikilink")、[小笠山](../Page/小笠山.md "wikilink")、[八高山](../Page/八高山.md "wikilink")、[大尾山](../Page/大尾山.md "wikilink")
  - [川](../Page/川.md "wikilink")：[逆川](../Page/逆川_\(太田川水系\).md "wikilink")、[原野谷川](../Page/原野谷川.md "wikilink")、[菊川](../Page/菊川.md "wikilink")、[倉真川](../Page/倉真川.md "wikilink")、[垂木川](../Page/垂木川.md "wikilink")
  - [海岸](../Page/海岸.md "wikilink")：[遠州灘](../Page/遠州灘.md "wikilink")（[海龜産卵地](../Page/海龜.md "wikilink")）

## 外部連結

  - [靜岡縣掛川市](http://www.city.kakegawa.shizuoka.jp/)
  - [掛川觀光協會](https://web.archive.org/web/20070826155330/http://www.wbs.ne.jp/bt/kakegawa/)