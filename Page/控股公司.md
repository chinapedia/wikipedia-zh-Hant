**控股公司**（），或稱**握股公司**、**控制公司**（）、**母公司**（），為已擁有其他[公司多數](../Page/公司.md "wikilink")[控制股權的方式](../Page/控制股權.md "wikilink")，掌握其管理及營運的公司。

## 定義

一般而言，控股公司可描述為任何擁有某家公司大部份股份的公司，本身亦可能是一般[企業](../Page/企業.md "wikilink")，但嚴格來說它指的是其存在只為持有股票的公司。通常，這名詞用來強調一家公司不自己生產[商品或](../Page/商品.md "wikilink")[服務](../Page/服務.md "wikilink")，而以純粹持股營運為目的。控股公司可以使擁有者的[風險降低](../Page/風險.md "wikilink")，並取得其他公司的所有權及控制權。

另外，即使未達多數控制股權，仍可足以影響、掌握該公司的經營決策。有時候，公司為了使自己成為名實相符的純粹控股公司，會在公司名稱加上「控股」（）字樣。\[1\]

## 例子

[波克夏·哈薩威公司是一家世上最大](../Page/波克夏·哈薩威公司.md "wikilink")[公開交易的控股公司](../Page/上市公司.md "wikilink")，其擁有眾多的[保險公司](../Page/保險.md "wikilink")、[製造商](../Page/製造.md "wikilink")、[零售商和其他類型的公司](../Page/零售.md "wikilink")。另一著名的控股公司為[聯合航空母公司](../Page/聯合航空.md "wikilink")，同為公開交易的控股公司，其成立係因應2010年聯合航空合併[美國大陸航空後所帶來的股權分配](../Page/美國大陸航空.md "wikilink")。

[福特汽車於](../Page/福特汽車.md "wikilink")1990年代，以33.4%持股控制[馬自達汽車](../Page/馬自達汽車.md "wikilink")，福特即是當時馬自達的控股公司。兩者的控股關係至2015年，福特完全售出擁有的馬自達股份後結束。

[Google為強化旗下事業經營](../Page/Google.md "wikilink")，於2015年8月10日成立控股公司[Alphabet](../Page/Alphabet.md "wikilink")，Google則成為Alphabet旗下最大子公司、並專注於經營網路本業。

[東京電力於](../Page/東京電力.md "wikilink")2016年4月1日起，因應日本的[電力自由化政策而轉型與更名為控股公司](../Page/電力自由化.md "wikilink")「東京電力控股」，將其售電、發電及輸配電業務拆至三家子公司。

## 另見

  - [銀行控股公司](../Page/銀行控股公司.md "wikilink")／[金融控股公司](../Page/金融控股公司.md "wikilink")
  - [企業集團](../Page/企業集團.md "wikilink")
  - [空殼公司](../Page/空殼公司.md "wikilink")
  - [專利持有公司](../Page/專利持有公司.md "wikilink")（Patent holding company）
  - [創業投資公司](../Page/創業投資.md "wikilink")（創投公司）
  - [子公司](../Page/子公司.md "wikilink")
  - [分公司](../Page/分公司.md "wikilink")
  - [大股東](../Page/大股東.md "wikilink")

## 註釋

## 外部連結

  -
  -
  -
{{-}}

[Category:企业类型](../Category/企业类型.md "wikilink")
[Category:公司法](../Category/公司法.md "wikilink")
[Category:公司管理及营运](../Category/公司管理及营运.md "wikilink")
[控股公司](../Category/控股公司.md "wikilink")

1.  其他還有「投資公司」（Investment company）等名義。