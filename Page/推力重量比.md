**推力重量比**（简称**推重比**、***TWR*** (Thrust-to-weight
ratio)）是一个[無量纲的参数](../Page/無量纲.md "wikilink")，用来描述利用排氣產生的推力以及所負擔的重量之間的比例。這種描述多使用於[火箭或](../Page/火箭.md "wikilink")[喷气发动机作為推力來源的飛行器上](../Page/喷气发动机.md "wikilink")，譬如[飛彈或者是](../Page/飛彈.md "wikilink")[飛機](../Page/飛機.md "wikilink")。

如果是計算飛行器的推重比，比如飞机，是用海平面的最大静推力除以[最大起飞重量得出](../Page/最大起飞重量.md "wikilink")。如果是計算發動機的推重比，重量是以發動機本身的重量與所產生的推力作比較。

由於噴氣發動機產生的推力會隨高度而改變，飛行器的重量計算的標準也沒有一定，因此推重比的數字在使用不同的重量或者是推力下會產生不小的差異。譬如說，當使用飛機最大起飛重量或者是只有50%燃料的重量分別計算推重比時，計算出來的數字差別很大。

## 例子

[俄罗斯](../Page/俄罗斯.md "wikilink")[RD-180的海平面推力为](../Page/RD-180.md "wikilink")3,820 kN，净重5,307 kg，地球表面重力加速度取9.80665 m/s²，则海平面推重比为:
(1 kN = 1000 N = 1000 kg⋅m/s²)

\(twr=\frac{T}{W}=\frac{3,820\ \mathrm{kN}}{(5,307\ \mathrm{kg})(9.807\ \mathrm{m/s^2})}=0.07340\ \frac{\mathrm{kN}}{\mathrm{N}}=73.40\ \frac{\mathrm{N}}{\mathrm{N}}=73.40\)

### 航空

| 飛行器                                            | 推重比        |
| ---------------------------------------------- | ---------- |
| [B-2 Spirit](../Page/B-2_Spirit.md "wikilink") | 0.205\[1\] |
| [協和式客機](../Page/協和式客機.md "wikilink")           | 0.373      |
| [Su-30](../Page/Su-30.md "wikilink")           | 1.00\[2\]  |
| [F-15 鷹](../Page/F-15.md "wikilink")           | 1.04\[3\]  |
| [F-16 戰隼](../Page/F-16.md "wikilink")          | 1.096      |
| [獵鷹式戰鬥機](../Page/AV-8.md "wikilink")           | 1.1        |
| [陣風戰鬥機](../Page/陣風戰鬥機.md "wikilink")           | 1.13       |
| [米格29](../Page/米格29.md "wikilink")             | 1.13       |
| [颱風戰鬥機](../Page/颱風戰鬥機.md "wikilink")           | 1.15\[4\]  |
| [F-22猛禽](../Page/F-22.md "wikilink")           | 1.26\[5\]  |

## 发动机

| 名称                                                                                       | 推重比        |
| ---------------------------------------------------------------------------------------- | ---------- |
| [协和号的](../Page/协和号.md "wikilink")[奥林匹斯 593](../Page/奥林匹斯593发动机.md "wikilink")            | 4.0\[6\]   |
| [J-58](../Page/J-58.md "wikilink") （[SR-71](../Page/SR-71.md "wikilink") 黑鸟的发动机）         | 5.2\[7\]   |
| [F119-PW-100](../Page/普惠_F119.md "wikilink")([F22战斗机的发动机](../Page/F22.md "wikilink"))    | 11.2       |
| [航天飞机主发动机](../Page/航天飞机主发动机.md "wikilink")                                               | 73.12\[8\] |
| [RD-180](../Page/RD-180.md "wikilink")\[9\]                                              | 73.4       |
| [RD-170](../Page/RD-170.md "wikilink")                                                   | 82.5       |
| [洛克达因F-1发动机](../Page/F-1火箭发动机.md "wikilink")（[土星五号火箭第一级发动机](../Page/土星五号.md "wikilink")） | 94.1\[10\] |
| [NK-33](../Page/NK-33.md "wikilink")\[11\]                                               | 136.7      |
| [默林1D](../Page/默林火箭發動機#默林1D.md "wikilink")\[12\]                                         | 180.1      |

## 相关条目

  - [功重比](../Page/功重比.md "wikilink")

## 參考資料

## 參考文獻

  - John P. Fielding. *Introduction to Aircraft Design*, Cambridge
    University Press, ISBN 978-0-521-65722-8
  - Daniel P. Raymer (1989). *Aircraft Design: A Conceptual Approach*,
    American Institute of Aeronautics and Astronautics, Inc.,
    Washington, DC. ISBN 0-930403-51-7
  - George P. Sutton & Oscar Biblarz. *Rocket Propulsion Elements*,
    Wiley, ISBN 978-0-471-32642-7

## 外部連結

  - [NASA webpage with overview and explanatory diagram of aircraft
    thrust to weight
    ratio](http://www.grc.nasa.gov/WWW/K-12/airplane/fwrat.html)

[Category:发动机](../Category/发动机.md "wikilink")
[Category:比率](../Category/比率.md "wikilink")

1.  [B-2 Spirit](../Page/B-2_Spirit.md "wikilink")
2.
3.  [F-15
    鷹](http://inventors.about.com/library/inventors/blF_15_Eagle.htm)
4.  "Eurofighter Typhoon". *All the World's Aircraft*. Jane's
    Information Group, 2013.
5.  <http://www.aviationsmilitaires.net/display/aircraft/87/f_a-22>
6.  [奥林匹斯 593](http://www.faa.gov/about/office_org/headquarters_offices/AEP/supersonic_noise/media/1-Panel3-Brines_Smith-AADC.pdf)

7.  [黑鸟](http://www.marchfield.org/sr71a.htm)
8.  [SSME](http://www.astronautix.com/engines/ssme.htm)
9.
10. [Encyclopedia Astronautica:
    F-1](http://www.astronautix.com/engines/f1.htm)
11. [Astronautix NK-33
    entry](http://www.astronautix.com/engines/nk33.htm)
12.