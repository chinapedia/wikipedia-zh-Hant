**措美县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[山南市下属的一个](../Page/山南市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。[藏语中意思是](../Page/藏语.md "wikilink")“湖的下游”，因其位于[哲古湖下游](../Page/哲古湖.md "wikilink")。与之接壤的县市有：东边[隆孜县](../Page/隆孜县.md "wikilink")，南边[洛扎县](../Page/洛扎县.md "wikilink")，西边[浪卡子县](../Page/浪卡子县.md "wikilink")，北边[琼结县](../Page/琼结县.md "wikilink")、[乃东县](../Page/乃东县.md "wikilink")。面积4549平方公里，2003年人口1万人。[邮政编码](../Page/邮政编码.md "wikilink")
856900，县政府驻措美镇当许。

## 行政区划

下辖：\[1\] 。

## 参考资料

[措美县](../Category/措美县.md "wikilink")
[Category:山南地区县份](../Category/山南地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.