**提督九門步軍巡捕五營統領**，簡稱**九門提督**或者**步軍統領**，是[清朝的驻](../Page/清朝.md "wikilink")[燕京](../Page/燕京.md "wikilink")[武官](../Page/武官.md "wikilink")，主要负责[京师守备和治安](../Page/京师.md "wikilink")，类似今天的[北京卫戍区](../Page/北京卫戍区.md "wikilink")[司令员兼](../Page/司令员.md "wikilink")[北京市公安局局长](../Page/北京市公安局.md "wikilink")，但职权更大。九門提督统合[北京内城九座城门](../Page/北京城池.md "wikilink")（[正阳门](../Page/正阳门.md "wikilink")、[崇文门](../Page/崇文门.md "wikilink")、[宣武门](../Page/宣武门.md "wikilink")、[安定门](../Page/安定门_\(北京\).md "wikilink")、[德胜门](../Page/德胜门.md "wikilink")、[东直门](../Page/东直门.md "wikilink")、[西直门](../Page/西直门.md "wikilink")、[朝阳门](../Page/朝阳门.md "wikilink")、[阜成门](../Page/阜成门.md "wikilink")）内外的守卫和[门禁](../Page/门禁.md "wikilink")，还负责巡夜、救火、编查[保甲](../Page/保甲.md "wikilink")、禁令、缉捕、断狱等，实际为清朝[皇室](../Page/皇室.md "wikilink")[禁军的统领](../Page/禁军.md "wikilink")，[品秩初为](../Page/品秩.md "wikilink")[正二品](../Page/清朝官職表.md "wikilink")，後於嘉慶年間升為[从一品](../Page/清朝官職表.md "wikilink")。

## 设立

  - 清[顺治元年](../Page/顺治.md "wikilink")（1644年），[清兵入关](../Page/清兵入关.md "wikilink")，定鼎[燕京](../Page/燕京.md "wikilink")，设立了军警合一的军事机构——“[步军统领衙门](../Page/步军统领衙门.md "wikilink")”，以繫[京師防務與治安](../Page/京師.md "wikilink")。主官为“**步军统领**”，辖制[满](../Page/八旗.md "wikilink")、[蒙八旗](../Page/蒙古八旗.md "wikilink")[步军营及九门官兵](../Page/步军营.md "wikilink")，同时节制南北[巡捕二营](../Page/巡捕.md "wikilink")，即[绿营马步兵](../Page/绿营.md "wikilink")。步军营主要防守内城，按八旗方位防守。巡捕营主要防守[外城](../Page/北京外城.md "wikilink")（也称南城）及[郊区的重要地方](../Page/郊区.md "wikilink")。
  - 顺治十六年（1659年），清廷增设巡捕中营，仍由步军统领衙门主官节制，是为“**提督九门步军巡捕三营统领**”\[1\]。

## 沿革

  - 九门提督一职由于关乎[京师防务](../Page/京师.md "wikilink")，所以自设立以后主要由[旗人大臣担任](../Page/旗人.md "wikilink")。
  - [乾隆四十六年](../Page/乾隆.md "wikilink")（1781年），清廷将巡捕三营（南、北、中）增为五营，皆为步军统领衙门节制，至此该[衙门主官称谓正式变为](../Page/衙门.md "wikilink")“**提督九门步军巡捕五营统领**”。
  - 1900年[八国联军攻入北京](../Page/八国联军.md "wikilink")，设立安民公所，维持治安。八国联军进入北京前的最后一任步军统领是[慈禧太后亲信](../Page/慈禧太后.md "wikilink")[荣禄](../Page/荣禄.md "wikilink")。1901年清廷仿效联军方式，设“善后协巡营”，后来改名“巡警总厅”，替代原步军统领衙门的巡捕营。1902年[肃亲王](../Page/肃亲王.md "wikilink")[善耆任步军统领](../Page/善耆.md "wikilink")，支持军警分离的改革。1905年清廷正式创设巡警部，并开办现代警察学校，培养警察人才。自此，军警编制分离，现代警察制度初步创立。
  - 1912年[宣統退位後](../Page/宣統退位.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[北洋政府保留了这个官职](../Page/北洋政府.md "wikilink")，只是将其更名为“**京师步军统领衙门**”，辖左右翼总兵；1924年11月最终裁撤。

## 参考文献

## 外部链接

  - [北京老城门](https://web.archive.org/web/20071217053849/http://www.bj.xinhuanet.com/bjpd_sdwm/2006-08/22/content_3137481.htm)
  - [俞玉储：步军统领衙门及其现存档案状况](https://web.archive.org/web/20070928021026/http://www.bjpopss.gov.cn/bjpssweb/show.aspx?id=22339&cid=52)
  - [朱詠：试论清代北京地区的司法建制及其近代的变迁](http://www.bjww.gov.cn/2006/4-10/173559.shtml)

## 参见

  - [顺天府](../Page/顺天府.md "wikilink")
      - [明清北京城](../Page/明清北京城.md "wikilink")
          - [北京城门](../Page/北京城门.md "wikilink")
  - [中国警察制度](../Page/中国警察制度.md "wikilink")

{{-}}

[Category:清朝武官](../Category/清朝武官.md "wikilink")
[步軍營步軍統領](../Category/步軍營步軍統領.md "wikilink")
[Category:老北京](../Category/老北京.md "wikilink")
[Category:1924年廢除](../Category/1924年廢除.md "wikilink")

1.  [中国历史纪事
    崇祯十七年](http://www.gg-art.com/history/hcontent.php?dynastydetailid=2602)