**提贝斯提高原**，又称**提贝斯提山脉**或**提贝斯提山地**，是[撒哈拉沙漠中部](../Page/撒哈拉沙漠.md "wikilink")、[乍得北部由一系列](../Page/乍得.md "wikilink")[火山组成的](../Page/火山.md "wikilink")[熔岩高地](../Page/熔岩.md "wikilink")，平均海拔超过2000米，其最高峰[库西山是一座](../Page/库西山.md "wikilink")[死火山](../Page/死火山.md "wikilink")，海拔3415米。

提贝斯提高原年降水量略多于周边[沙漠地区](../Page/沙漠.md "wikilink")，有[温泉和](../Page/温泉.md "wikilink")[季节性水流](../Page/季节性水流.md "wikilink")。[图布族世居于此](../Page/图布族.md "wikilink")，早在2500年前便与[柏柏尔人开始通商](../Page/柏柏尔人.md "wikilink")。提贝斯提高原北部[奥祖地带是重要的](../Page/奥祖地带.md "wikilink")[铀矿产区](../Page/铀.md "wikilink")，曾为乍得和[利比亚争议地区](../Page/利比亚.md "wikilink")，高原中尚有[钨矿和](../Page/钨.md "wikilink")[锡矿](../Page/锡.md "wikilink")。提贝斯提高原中有神秘[岩画](../Page/岩画.md "wikilink")，约创作于公元前5000年至前3000年间，是[撒哈拉沙漠岩画群的重要组成部分](../Page/撒哈拉沙漠岩画群.md "wikilink")。

## 外部链接

  - [Information on climbing, with
    map](http://www.peakware.com/areas.html?a=385)
  - [Information about the mountains, with
    images](https://web.archive.org/web/20050904140316/http://volcano.und.nodak.edu/vwdocs/volc_images/img_tibesti.html)
  - [WWF report on the Tibesti
    region](http://www.worldwildlife.org/wildworld/profiles/terrestrial/pa/pa1331_full.html)
  - [Photo
    gallery](http://www.trekearth.com/gallery/Africa/Chad/North/Borkou-Ennedi-Tibesti/)
  - [Bird life in the Tibesti
    Mountains](http://www.birdlife.org/datazone/sites/?action=SitHTMDetails.asp&sid=6889&m=0)
  - [Photo
    gallery](https://web.archive.org/web/20081204143931/http://tchadmonpays.free.fr/cartes.php3)
  - [Travel page with photos (in
    German)](http://www.tlc-exped.net/R18Ber.html)

[T](../Category/乍得地理.md "wikilink") [T](../Category/高原.md "wikilink")