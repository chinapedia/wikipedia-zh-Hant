[thumb](../Page/file:Iranian_handicraft.jpg.md "wikilink")
[Tea_set.jpg](https://zh.wikipedia.org/wiki/File:Tea_set.jpg "fig:Tea_set.jpg")\]\]
[Lindenstraße_alt.JPG](https://zh.wikipedia.org/wiki/File:Lindenstraße_alt.JPG "fig:Lindenstraße_alt.JPG")
[Enamelled_plate_for_commemorating_coronation_of_Queen_Elizabeth_II_(HKMH).jpg](https://zh.wikipedia.org/wiki/File:Enamelled_plate_for_commemorating_coronation_of_Queen_Elizabeth_II_\(HKMH\).jpg "fig:Enamelled_plate_for_commemorating_coronation_of_Queen_Elizabeth_II_(HKMH).jpg")1952年登基而製造的搪瓷托盤（[香港歷史博物館展品](../Page/香港歷史博物館.md "wikilink")）\]\]
**搪瓷**（），又称**瑯**，指將[玻璃或](../Page/玻璃.md "wikilink")[陶瓷質](../Page/陶瓷.md "wikilink")[粉末熔結在基質](../Page/粉末.md "wikilink")（如[金屬](../Page/金屬.md "wikilink")、玻璃或陶瓷）表面形成的外殼，多為[彩色具有](../Page/彩色.md "wikilink")[藝術美感的花樣](../Page/藝術.md "wikilink")，用於保護和裝飾。

用[石英](../Page/石英.md "wikilink")、[长石等为主要原料](../Page/长石.md "wikilink")，并加入[纯碱](../Page/纯碱.md "wikilink")、[硼砂等为溶剂](../Page/硼砂.md "wikilink")，[氧化钛](../Page/氧化钛.md "wikilink")、[氧化锑](../Page/氧化锑.md "wikilink")、[氟化物等为乳浊剂](../Page/氟化物.md "wikilink")，金属氧化物为着色剂，经过粉碎、混合、熔融后，倾入水中急冷成珐琅浆，涂敷于金属制品表面，经过干燥、烧成，即得制品。

搪瓷[鍋](../Page/鍋.md "wikilink")、[盛盤](../Page/盛盤.md "wikilink")、[浴缸](../Page/浴缸.md "wikilink")、[冰箱等是其例子](../Page/冰箱.md "wikilink")。

## 中國琺瑯器

琺瑯是音譯，古代又稱「佛」、「佛郎」、「拂郎」，都是古代對[法蘭克](../Page/法蘭克.md "wikilink")（Frank）的音譯。其中金屬胎琺瑯工藝，常見有[掐絲琺瑯以及](../Page/掐絲琺琅.md "wikilink")[畫琺瑯兩大類](../Page/琺瑯彩瓷.md "wikilink")，此外偶爾可見[鏨胎琺瑯和](../Page/鏨胎琺琅.md "wikilink")[透明琺瑯](../Page/透明琺琅.md "wikilink")（廣琺瑯）。明清文人又稱「大食窯」、「鬼國窯」、「鬼國嵌」、「佛郎嵌」，都是琺瑯的不同做法。

### 掐絲琺瑯

多數專家根據[明代初年的紀錄](../Page/明代.md "wikilink")\[1\]，認為**大食窯**即是掐絲琺瑯，將其傳入中國的時間定為公元13至14世紀。由於[元代](../Page/元代.md "wikilink")[蒙古大軍四處掠奪工匠](../Page/蒙古.md "wikilink")，很多[西亞](../Page/西亞.md "wikilink")、[中亞工匠就此隨軍被安置在](../Page/中亞.md "wikilink")[雲南](../Page/雲南.md "wikilink")，中外技術交流使得滇工馳名天下，在元代及明初一直貢奉內廷\[2\]\[3\]。在對[北京故宮館藏清宮舊藏掐絲琺瑯器物的排查中](../Page/北京故宮.md "wikilink")，被劃歸元代及明初的器物，其釉色濃豔明快，特別是白色釉料呈半透明水晶狀。這一特徵在[宣德以後消失](../Page/宣德.md "wikilink")。可以推斷元末明初內府仍然有[西亞釉料留存或有外國工匠指導](../Page/西亞.md "wikilink")。

### 畫琺瑯

畫琺瑯，又稱洋瓷，元明皆無，[康熙時期傳入](../Page/康熙.md "wikilink")，清三代尤為盛行。因為琺瑯釉料在燒製時容易流淌走樣，對於釉料配方以及火候控制均有嚴格要求，其技術難度較掐絲琺瑯高，是琺瑯工藝中最難的一種。

## 相關條目

  - [矽](../Page/矽.md "wikilink")

## 參考資料

[Category:珐琅工艺](../Category/珐琅工艺.md "wikilink")

1.
2.  [天順三年](../Page/天顺_\(明朝\).md "wikilink")（1459年）王佐《[新增格古要論](../Page/新增格古要論.md "wikilink")》：今雲南人在京多作酒盞，俗呼曰鬼國嵌。
3.