[炫彩津门1Tianjin_Eye.jpg](https://zh.wikipedia.org/wiki/File:炫彩津门1Tianjin_Eye.jpg "fig:炫彩津门1Tianjin_Eye.jpg")\]\]
[Ocean_City_Ferris_Wheel.jpg](https://zh.wikipedia.org/wiki/File:Ocean_City_Ferris_Wheel.jpg "fig:Ocean_City_Ferris_Wheel.jpg")
[StarOfNanchang20070718.jpg](https://zh.wikipedia.org/wiki/File:StarOfNanchang20070718.jpg "fig:StarOfNanchang20070718.jpg")
[BA_London_Eye.jpg](https://zh.wikipedia.org/wiki/File:BA_London_Eye.jpg "fig:BA_London_Eye.jpg")\]\]
[美麗華百樂園摩天輪.jpg](https://zh.wikipedia.org/wiki/File:美麗華百樂園摩天輪.jpg "fig:美麗華百樂園摩天輪.jpg")摩天輪\]\]
[Dream_mall_ferris_wheel_tw.JPG](https://zh.wikipedia.org/wiki/File:Dream_mall_ferris_wheel_tw.JPG "fig:Dream_mall_ferris_wheel_tw.JPG")高雄之眼摩天輪\]\]
[Cape_Wheel.jpg](https://zh.wikipedia.org/wiki/File:Cape_Wheel.jpg "fig:Cape_Wheel.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:大阪摩天轮.jpeg "fig:缩略图")
**摩天輪**，又称**观览车**，是一種大型轉輪狀的機械建築設施，上面掛在輪邊緣的是供乘客乘搭的座艙。乘客坐在摩天輪慢慢的往上轉，可以從高俯瞰四周景色。通常與[雲霄飛車](../Page/雲霄飛車.md "wikilink")、[旋轉木馬出現於](../Page/旋轉木馬.md "wikilink")[遊樂園](../Page/遊樂園.md "wikilink")、[主題公園與](../Page/主題公園.md "wikilink")[園遊會](../Page/園遊會.md "wikilink")，也有單獨存在作為觀景台使用。

## 概述

摩天輪可分為

最早的摩天輪由[美國人](../Page/美國人.md "wikilink")[小喬治·華盛頓·蓋爾·費里斯在](../Page/小喬治·華盛頓·蓋爾·費里斯.md "wikilink")1893年為[芝加哥哥倫布紀念博覽會設計](../Page/芝加哥哥倫布紀念博覽會.md "wikilink")，目的是與[巴黎鐵塔媲美](../Page/巴黎鐵塔.md "wikilink")\[1\]。此摩天輪高度約80.4米（264英尺），重2200噸，可乘坐2160人。正由於費里斯的成功，日後人們皆以「費里斯巨輪」（Ferris
Wheel）來稱呼這種設施。

[维也纳摩天轮是現存唯一建於](../Page/维也纳摩天轮.md "wikilink")19世纪的摩天轮，至今仍在营运。维也纳摩天轮于1897年为[弗朗茨·约瑟夫一世的金禧庆典而兴建在](../Page/弗朗茨·约瑟夫一世.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")[维也纳](../Page/维也纳.md "wikilink")[利奥波德城普拉特游乐场](../Page/利奥波德城.md "wikilink")，总高64.75米\[2\]。1920年，[巴黎摩天轮拆除后](../Page/巴黎摩天轮.md "wikilink")，维也纳摩天轮就成为世界最高的摩天轮。1944年在二战期间烧毁，次年重建後\[3\]，仍為世界最高的摩天轮，直到1985年被[日本](../Page/日本.md "wikilink")[筑波市超过](../Page/筑波市.md "wikilink")85米高。

建於[倫敦](../Page/倫敦.md "wikilink")[泰晤士河南畔](../Page/泰晤士河.md "wikilink")[蘭貝斯區的](../Page/蘭貝斯區.md "wikilink")[倫敦眼](../Page/倫敦眼.md "wikilink")，高135公尺，1999年開幕時為世界最高的摩天輪，。位於[南昌市](../Page/南昌市.md "wikilink")，高160公尺的重力式摩天輪的[南昌之星於](../Page/南昌之星.md "wikilink")[2006年3月開業](../Page/2006年3月.md "wikilink")。[新加坡摩天觀景輪為觀景摩天輪](../Page/新加坡摩天觀景輪.md "wikilink")，總高165公尺（輪徑150公尺），於[2008年3月開業的後成為世界最高的摩天輪](../Page/2008年3月.md "wikilink")。南昌之星為最高重力式摩天輪。

世界上有多個正在籌備中的大型摩天輪建設案，包括預計在2009年第四季度啟用的[北京朝天輪](../Page/北京朝天輪.md "wikilink")，這座預計總高208公尺、輪徑198公尺的摩天輪，採用的是與倫敦眼一模一樣的觀景式摩天輪結構。

## 摩天輪排名

參見[摩天輪列表](../Page/摩天輪列表.md "wikilink")

### 正在營運中的摩天輪

  - ，[拉斯維加斯](../Page/拉斯維加斯.md "wikilink")（High Roller，總高167.6公尺)

  - ，[新加坡摩天觀景輪](../Page/新加坡摩天觀景輪.md "wikilink")（Singapore Flyer，165公尺）

  - [南昌市](../Page/南昌市.md "wikilink")，[灨江市民公園的](../Page/灨江市民公園.md "wikilink")[南昌之星](../Page/南昌之星.md "wikilink")（160公尺）

  - [倫敦](../Page/伦敦.md "wikilink")，[倫敦眼](../Page/倫敦眼.md "wikilink")（135公尺）

  - [路氹城](../Page/路氹城.md "wikilink")，建在[新濠影匯兩座酒店大樓之間的](../Page/新濠影匯.md "wikilink")[影滙之星](../Page/新濠影匯#「影滙之星」.md "wikilink")（130公尺）

  - [武威市](../Page/武威市.md "wikilink")，[甘肃（武威）天马文化产业园暨——摩天轮主题公园的](../Page/甘肃（武威）天马文化产业园暨——摩天轮主题公园.md "wikilink")[天马之眼](../Page/天马之眼.md "wikilink")（128公尺）

  - [大阪市](../Page/大阪市.md "wikilink")，大阪之輪（）（123公尺）

  - [宿迁市](../Page/宿迁市.md "wikilink")，[骆马湖的](../Page/骆马湖.md "wikilink")[宿迁欢乐岛摩天轮](../Page/宿迁欢乐岛摩天轮.md "wikilink")（122.5公尺）

  - [天津市](../Page/天津市.md "wikilink")，建在海河[永樂橋上的](../Page/永樂橋.md "wikilink")[天津之眼](../Page/天津之眼.md "wikilink")（總高120公尺，輪徑110公尺）

  - <s>[福岡市](../Page/福岡市.md "wikilink")，[天空之夢福岡](../Page/天空之夢福岡.md "wikilink")</s>（總高120公尺，輪徑102公尺）：已於2009年停止營業並拆除，拆除後遷至[台灣](../Page/台灣.md "wikilink")[台中市的](../Page/台中市.md "wikilink")[麗寶樂園內重新營運](../Page/麗寶樂園.md "wikilink")。

  - [台中市](../Page/台中市.md "wikilink")[后里區](../Page/后里區.md "wikilink")，[麗寶樂園天空之夢](../Page/麗寶樂園.md "wikilink")
    （總高120公尺，輪徑102公尺）

  - [苏州市](../Page/苏州市.md "wikilink")，[苏州摩天轮主题公园中的摩天轮](../Page/苏州摩天轮主题公园.md "wikilink")（120米）

  - [長沙市](../Page/长沙市.md "wikilink")，[贺龙体育文化中心摩天轮](../Page/贺龙体育文化中心#摩天轮.md "wikilink")（總高120公尺，輪徑99公尺）

  - [鄭州市](../Page/郑州市.md "wikilink")，[鄭州世紀歡樂園摩天輪](../Page/鄭州世紀歡樂園.md "wikilink")（總高120公尺，輪徑88公尺）

  - [千葉縣](../Page/千葉縣.md "wikilink")，[葛西臨海公園](../Page/葛西臨海公園.md "wikilink")[鑽石與花大摩天輪](../Page/鑽石與花大摩天輪.md "wikilink")（117公尺）

  - [无锡](../Page/无锡市.md "wikilink")，[太湖](../Page/太湖.md "wikilink")[之星](../Page/太湖之星.md "wikilink")（115米）

  - [東京都](../Page/東京都.md "wikilink")[台場](../Page/御台場.md "wikilink")，[調色板城大摩天輪](../Page/調色板城大摩天輪.md "wikilink")（，115公尺）

  - [高雄市](../Page/高雄市.md "wikilink")[大樹區](../Page/大樹區.md "wikilink")，[義大遊樂世界摩天輪](../Page/義大遊樂世界.md "wikilink")（總高115公尺
    輪徑80公尺）

  - [大阪市](../Page/大阪市.md "wikilink")，[天保山大觀覽車](../Page/天保山大觀覽車.md "wikilink")（112.5公尺，輪徑100公尺）

  - [橫濱市](../Page/橫濱市.md "wikilink")[橫濱港未來21](../Page/橫濱港未來21.md "wikilink")，[橫濱宇宙世界的](../Page/橫濱宇宙世界.md "wikilink")[宇宙時鐘21大摩天輪](../Page/宇宙時鐘21大摩天輪.md "wikilink")（，總高112.5公尺，輪徑100公尺）

  - [黑龍江省](../Page/黑龍江省.md "wikilink")[哈爾濱遊樂園中的摩天輪](../Page/哈爾濱遊樂園.md "wikilink")（110公尺）

  - [上海](../Page/上海市.md "wikilink")[錦江樂園中的摩天輪](../Page/錦江樂園.md "wikilink")（108公尺）

  - [高雄市](../Page/高雄市.md "wikilink")[前鎮區](../Page/前鎮區.md "wikilink")，[夢時代購物中心高雄之眼又名](../Page/統一夢時代購物中心.md "wikilink")「OPEN\!家族摩天輪」（總高102.5公尺，輪徑50公尺）

  - [台北市](../Page/台北市.md "wikilink")[中山區](../Page/中山區_\(臺北市\).md "wikilink")，[美麗華百樂園摩天輪](../Page/美麗華百樂園#摩天輪.md "wikilink")（總高100公尺
    輪徑70公尺）

  - [雲林縣](../Page/雲林縣.md "wikilink")[古坑鄉](../Page/古坑鄉.md "wikilink")，[劍湖山世界摩天輪](../Page/劍湖山世界#摩天輪.md "wikilink")（輪徑88公尺）

  - [開普敦](../Page/開普敦.md "wikilink")，開普輪（Cape Wheel，60公尺）

  - [中環](../Page/中環.md "wikilink")，[香港摩天輪](../Page/香港摩天輪.md "wikilink")（60公尺）

  - [花蓮縣](../Page/花蓮縣.md "wikilink")[壽豐鄉](../Page/壽豐鄉.md "wikilink")，[花蓮遠雄海洋公園摩天輪](../Page/遠雄海洋公園.md "wikilink")（輪徑31公尺）

  - [广东省](../Page/广东省.md "wikilink")[茂名市](../Page/茂名市.md "wikilink")，茂名市小东江摩天輪（68米）

### 規劃與興建中的摩天輪

  - [杜拜](../Page/杜拜.md "wikilink")，[杜拜眼](../Page/杜拜眼.md "wikilink")(Dubai
    Eye)(總高210公尺)\[4\]

  - [紐約](../Page/紐約市.md "wikilink")[史泰登島摩天輪](../Page/史泰登島.md "wikilink")(190.5公尺)

  - [莫斯科](../Page/莫斯科.md "wikilink")，[麻雀山](../Page/麻雀山.md "wikilink")（170公尺）

  - [拉斯維加斯](../Page/拉斯維加斯.md "wikilink")（150公尺）

  - [常州市](../Page/常州市.md "wikilink")，[时来运转摩天轮](../Page/时来运转摩天轮.md "wikilink")（高84公尺，轮径89公尺)

  - [高雄市](../Page/高雄市.md "wikilink"),
    [愛情摩天輪](../Page/愛情摩天輪.md "wikilink") (87公尺)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [新加坡摩天觀景輪](http://www.singaporeflyer.com.sg/)

  - [倫敦眼](http://www.londoneye.com/)

  - <http://m.discoverhongkong.com/tc/see-do/highlight-attractions/harbour-view/hong-kong-observation-wheel.jsp>

[Category:遊樂場機動遊戲](../Category/遊樂場機動遊戲.md "wikilink")
[Category:建築物](../Category/建築物.md "wikilink")
[摩天輪](../Category/摩天輪.md "wikilink")
[Category:1893年面世](../Category/1893年面世.md "wikilink")

1.
2.  [Wiener Riesenrad - technical
    data](http://www.wienerriesenrad.com/technik.php?technik&lang=en)
3.  [Wiener Riesenrad -
    History](http://www.wienerriesenrad.com/geschichte.php?geschichte&lang=en)

4.