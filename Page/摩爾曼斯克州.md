**摩爾曼斯克州**（），位於[俄羅斯最西北部](../Page/俄羅斯.md "wikilink")，是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[西北部联邦管区](../Page/西北部联邦管区.md "wikilink")。面積144,900平方公里，人口892,534（2002年）。首府為[摩爾曼斯克](../Page/摩爾曼斯克.md "wikilink")。该州与芬兰、挪威接壤

## 標準時區

[Map_of_Russia_-_Moscow_time_zone.svg](https://zh.wikipedia.org/wiki/File:Map_of_Russia_-_Moscow_time_zone.svg "fig:Map_of_Russia_-_Moscow_time_zone.svg")
摩爾曼斯克州使用[莫斯科時間](../Page/莫斯科時間.md "wikilink")。時差為[UTC+4小時](../Page/UTC+4.md "wikilink")、無[夏令時](../Page/夏令時.md "wikilink")。（2011年3月之前時區為[UTC+3](../Page/UTC+3.md "wikilink")、夏令時為[UTC+4](../Page/UTC+4.md "wikilink")）

## 地理

[Murmansk_region.png](https://zh.wikipedia.org/wiki/File:Murmansk_region.png "fig:Murmansk_region.png")
該州位於[拉普蘭地區東部](../Page/拉普蘭.md "wikilink")，佔有[科拉半島全境](../Page/科拉半島.md "wikilink")。西與[挪威及](../Page/挪威.md "wikilink")[芬蘭接境](../Page/芬蘭.md "wikilink")。在俄羅斯聯邦內則南與[卡累利阿共和国相鄰](../Page/卡累利阿共和国.md "wikilink")。東北為[白海及](../Page/白海.md "wikilink")[巴倫支海](../Page/巴倫支海.md "wikilink")。

境內[河流及](../Page/河流.md "wikilink")[湖泊眾多](../Page/湖泊.md "wikilink")，主要河流為[波諾伊河及](../Page/波諾伊河.md "wikilink")[瓦爾祖加河等](../Page/瓦爾祖加河.md "wikilink")。最大的湖泊為[伊曼德拉湖](../Page/伊曼德拉湖.md "wikilink")。

摩爾曼斯克州境內大部份土地均為[凍原地帶](../Page/凍原.md "wikilink")，或者為森林凍原地帶，南部部份可見[泰加林](../Page/泰加林.md "wikilink")。

## 人民

主要人口為[俄羅斯人](../Page/俄羅斯人.md "wikilink")，并有少數[薩米人居住](../Page/薩米人.md "wikilink")。人口主要集中于摩爾曼斯克至[聖彼得堡的鐵路沿線](../Page/聖彼得堡.md "wikilink")。

## 產業

[漁業繁榮](../Page/漁業.md "wikilink")。出產[磷灰石](../Page/磷灰石.md "wikilink")、[鐵礦石](../Page/鐵礦石.md "wikilink")、[銅](../Page/銅.md "wikilink")、[鎳等](../Page/鎳.md "wikilink")。巴倫支海的大陸架上擁有[石油](../Page/石油.md "wikilink")。

## 城市

除了首府摩爾曼斯克之外，還有礦業城市[阿帕季特](../Page/阿帕季特.md "wikilink")、[基洛夫斯克](../Page/基洛夫斯克_\(摩爾曼斯克州\).md "wikilink")、[蒙切戈爾斯克](../Page/蒙切戈爾斯克.md "wikilink")、軍港城市[北莫爾斯克](../Page/北莫爾斯克.md "wikilink")、古老的港城[坎達拉克沙及](../Page/坎達拉克沙.md "wikilink")[科拉等](../Page/科拉.md "wikilink")。

## 注释

## 参考资料

[\*](../Category/摩爾曼斯克州.md "wikilink")
[Category:西北部聯邦管區](../Category/西北部聯邦管區.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")