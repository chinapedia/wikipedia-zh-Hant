*摩頓·甘斯特·柏達臣*'（，）在[挪威東北部](../Page/挪威.md "wikilink")[北極圈內的](../Page/北極圈.md "wikilink")[瓦德瑟出生](../Page/:en:Vadsø.md "wikilink")。他是一位多才多藝的[挪威足球運動員](../Page/挪威.md "wikilink")，現時效力[英超球會](../Page/英超.md "wikilink")[布力般流浪](../Page/布力般流浪.md "wikilink")，擔當左翼或中場位置，而他亦能勝任右翼或影子前鋒。他跟著名歌手[琳恩·玛莲有親戚關係](../Page/琳恩·玛莲.md "wikilink")。值得一提的是摩頓·柏達臣曾在挪威出版[唱片](../Page/唱片.md "wikilink")。

## 球會生涯

摩頓·柏達臣的父親是一名足球教練，當他只有15歲的時候，雖然是天生一名右腳球員，但其父親開始訓練他使用左腳，因為當時正缺乏左腳的球員，這使他更易立足職業足球界。

在1997年，他加入了地區球隊諾里德（Norild
IL），其後再轉到[挪威超級聯賽球隊](../Page/挪威足球超级联赛.md "wikilink")[特林素](../Page/特林素.md "wikilink")，在成名挪威球壇嶄露頭角。在2004年的賽季中，他便為特林素射入了7個入球，但他只為球隊上陣了若半個球季。2004年8月，摩頓柏達臣以150萬[英鎊加盟](../Page/英鎊.md "wikilink")[英超球會](../Page/英格蘭超級聯賽.md "wikilink")[布力般流浪](../Page/布力般流浪.md "wikilink")，轉會費將按其表現可提升到250萬英鎊。

在這宗轉會交易完成之前，曾有報導指其他英超球會如[曼聯](../Page/曼聯.md "wikilink")、[阿士東維拉](../Page/阿士東維拉.md "wikilink")、[紐卡素及](../Page/紐卡素.md "wikilink")[熱刺等有意收購摩頓](../Page/熱刺.md "wikilink")·柏達臣。最終失去剛於2003年7月轉會[車路士的](../Page/車路士.md "wikilink")[杜夫的布力般流浪獲得摩頓](../Page/达米恩·达夫.md "wikilink")·柏達臣首肯加盟，填補左路的空缺。2004年8月28日，他在對曼聯的比賽中首次上陣，布力般流浪大部分時間領前，直到補時才被曼聯逼和1-1。

2004年9月，[马克·休斯取代離任的](../Page/马克·休斯.md "wikilink")[桑拿士擔任布力般流浪的領隊](../Page/格雷姆·索内斯.md "wikilink")，摩頓·柏達臣的表現未獲新領隊的歡心，有一段頗長的時間待在後備席等待機會。踏入2005年，摩頓·柏達臣在重任正選的首場對[卡迪夫城即取得入球](../Page/卡迪夫城足球俱乐部.md "wikilink")，接下來的3場比場再射入3球，在球季結束時以8球成為球隊的次席射手。

2005/06年賽季，摩頓·柏達臣成為球隊主力，於9月作客[老特拉福德球场對曼聯時更梅開二度](../Page/老特拉福德球场.md "wikilink")，協助球隊以2-1取勝成為當年的代表作。

在2006年7月，摩頓·柏達臣獲英超足總批准將球衣背上的姓氏從「柏達臣」改為跟隨母親姓氏的「甘斯特」（Gamst）。然而，在2008/09賽季，他的球衣背後的姓氏又回復為「柏達臣」。

2010年5月19日與布力般流浪續約至2014年<small>\[1\]</small>。

## 國家隊

自2004年起，摩頓·柏達臣首次入選[挪威國家隊](../Page/挪威國家足球隊.md "wikilink")，現時他已為[挪威國家足球隊上陣超過](../Page/挪威國家足球隊.md "wikilink")50次，並射入超過10個入球。

其中在2006年9月，摩頓·柏達臣代表挪威對[匈牙利時](../Page/匈牙利國家足球隊.md "wikilink")，在底線窄位射入一球，類似其兒時偶像[雲巴士頓在](../Page/马尔科·范巴斯滕.md "wikilink")1988年[欧洲足球锦标赛決賽射破](../Page/欧洲足球锦标赛.md "wikilink")[蘇聯鋼門](../Page/蘇聯國家足球隊.md "wikilink")[达萨耶夫](../Page/里纳特·达萨耶夫.md "wikilink")（Rinat
Dasaev）的經典入球，當地主要的報紙《Aftenposten》\[2\]、《Dagbladet》\[3\]及《Verdens
Gang》戲稱他為「雲甘斯特」（**）。

### 國際賽進球

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>場地</p></th>
<th><p>對賽球隊</p></th>
<th><p>比數</p></th>
<th><p>賽果</p></th>
<th><p>競賽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2004年2月18日</p></td>
<td><p><a href="../Page/貝爾法斯特.md" title="wikilink">貝爾法斯特</a>，<a href="../Page/溫莎球場.md" title="wikilink">溫莎球場</a></p></td>
<td></td>
<td><p>4–1</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2004年10月13日</p></td>
<td><p><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a>，<a href="../Page/烏利華球場.md" title="wikilink">烏利華球場</a></p></td>
<td></td>
<td><p>3–0</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/2006年世界盃外圍賽.md" title="wikilink">2006年世界盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2004年11月16日</p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a>，<a href="../Page/卡雲農舍球場.md" title="wikilink">卡雲農舍球場</a></p></td>
<td></td>
<td><p>2–2</p></td>
<td><p>和</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2005年9月3日</p></td>
<td><p><a href="../Page/采列.md" title="wikilink">采列</a>，<a href="../Page/汽油競技場.md" title="wikilink">汽油競技場</a></p></td>
<td></td>
<td><p>3–2</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/2006年世界盃外圍賽.md" title="wikilink">2006年世界盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2006年8月16日</p></td>
<td><p><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a>，<a href="../Page/烏利華球場.md" title="wikilink">烏利華球場</a></p></td>
<td></td>
<td><p>1–1</p></td>
<td><p>和</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2006年9月2日</p></td>
<td><p><a href="../Page/布達佩斯.md" title="wikilink">布達佩斯</a>，<a href="../Page/苏斯萨费伦茨球场.md" title="wikilink">苏斯萨费伦茨球场</a></p></td>
<td></td>
<td><p>4–1</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/2008年欧洲足球锦标赛外围赛.md" title="wikilink">2008年歐國盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2007年10月21日</p></td>
<td><p><a href="../Page/塔加利.md" title="wikilink">塔加利</a>，<a href="../Page/塔加利球场.md" title="wikilink">塔加利球场</a></p></td>
<td></td>
<td><p>4–1</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/2008年欧洲足球锦标赛外围赛.md" title="wikilink">2008年歐國盃外圍賽</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2009年3月28日</p></td>
<td><p><a href="../Page/勒斯滕堡.md" title="wikilink">勒斯滕堡</a>，<a href="../Page/皇家班巴夫肯球場.md" title="wikilink">皇家班巴夫肯球場</a></p></td>
<td></td>
<td><p>1–2</p></td>
<td><p>負</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2009年4月1日</p></td>
<td><p><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a>，<a href="../Page/烏利華球場.md" title="wikilink">烏利華球場</a></p></td>
<td></td>
<td><p>3–2</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2009年8月12日</p></td>
<td><p><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a>，<a href="../Page/烏利華球場.md" title="wikilink">烏利華球場</a></p></td>
<td></td>
<td><p>4–0</p></td>
<td><p>勝</p></td>
<td><p><a href="../Page/2010年世界盃外圍賽.md" title="wikilink">2010年世界盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歌唱事業

摩頓柏達臣、[F.山度士](../Page/:en:Freddy_dos_Santos.md "wikilink") (Freddy dos
Santos)、[卡維斯域克](../Page/:en:Raymond_Kvisvik.md "wikilink") (Raymond
Kvisvik)、[斯衛寧](../Page/:no:Øyvind_Svenning.md "wikilink") (Øyvind
Svenning) 和[克里斯](../Page/:en:Kristofer_Hæstad.md "wikilink") (Kristofer
Hæstad) 。他們都是挪威的著名足球運動員之一。他們在2006年7月3日，在挪威成立樂隊。他們的第一首歌曲是 "This is for
real"。他們自稱這是足球運動員樂隊 "[The Players
(boyband)](../Page/:en:The_Players_\(boyband\).md "wikilink")"。他們的發行這首音樂後，同時音樂錄影帶亦在該日在電台上播放。他們認為這首歌詞會成為2006年的[Summer
hit排行榜的第一位](../Page/:en:Summer_hit.md "wikilink")。另外，他們的所有收益將會用作慈善用途及捐款到由[魯恩·布萊特塞和藝人](../Page/:en:Rune_Bratseth.md "wikilink")[基捷·森美所創辦的](../Page/:en:Ketil_Siem.md "wikilink")[對抗球場暴力慈善機構](../Page/:en:Soccer_Against_Crime.md "wikilink")。

## 榮譽

### 挪威

  - [挪威足總金錶](../Page/:en:Norwegian_Football_Association_Gold_Watch.md "wikilink")（）：獲選代表[挪威](../Page/挪威國家足球隊.md "wikilink")25次以上的紀念品

## 參考資料

## 外部連結

  - [非官方球迷網站](https://web.archive.org/web/20060909124712/http://www.mortengamstpedersen.tk/)
  - [足球資料庫：摩頓柏達臣統計](https://web.archive.org/web/20061117001844/http://www.soccerbase.com/players_details.sd?playerid=21488)
  - [Uefa.com
    雜誌訪問](http://www.uefa.com/magazine/news/Kind=512/newsId=365103.html)
  - [衛報：摩頓柏達臣可協助曼聯重上高峰](http://football.guardian.co.uk/Observer_Match_Report/0,,1577769,00.html)
  - [衛報：比挪威國王更出名的足球員](http://football.guardian.co.uk/News_Story/0,,1692124,00.html)
  - ["The Players"出版新歌"This is for
    real"](https://web.archive.org/web/20070928163518/http://www.universalmusic.no/les_mer.asp?newsid=3477)
  - [Footballdatabase.com：摩頓柏達臣](http://www.footballdatabase.com/index.php?page=player&Id=6324)

[Category:挪威足球運動員](../Category/挪威足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:布力般流浪球員](../Category/布力般流浪球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:特林素球員](../Category/特林素球員.md "wikilink")
[Category:挪超球員](../Category/挪超球員.md "wikilink")

1.
2.  [Aftenposten, 2006.9.3,
    pg.34（挪威文）](http://www.vg.no/pub/vgart.hbs?artid=128559)
3.  [Dagbladet（挪威文）](http://www.dagbladet.no/sport/2006/09/02/475623.html)