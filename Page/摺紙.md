[Orizuru.jpg](https://zh.wikipedia.org/wiki/File:Orizuru.jpg "fig:Orizuru.jpg")\]\]
[HK_paper_toy_handgun_摺紙手槍_5-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_paper_toy_handgun_摺紙手槍_5-2013.JPG "fig:HK_paper_toy_handgun_摺紙手槍_5-2013.JPG")\]\]
[Oragami_star.jpg](https://zh.wikipedia.org/wiki/File:Oragami_star.jpg "fig:Oragami_star.jpg")的例子\]\]

**摺紙**是摺或疊[紙張的](../Page/紙張.md "wikilink")[藝術](../Page/藝術.md "wikilink")，把紙張摺出各種特定的形狀和花樣，可能是一張紙的作品，也可能是二張以上紙張作品。

摺紙只需要透過摺疊的技巧就可以創造出複雜精細的設計。摺紙設計，一般而言由[正方形的紙張摺成](../Page/正方形.md "wikilink")，有些摺疊者也會用非正方形的紙張。有一說指，[日本的傳統摺紙早見於](../Page/日本.md "wikilink")[江戶時代](../Page/江戶時代.md "wikilink")（1603年─1867年），但當時這門傳統手藝並不嚴謹，製作時間中甚至會運用到[剪紙](../Page/剪紙.md "wikilink")。另外，[長方形](../Page/長方形.md "wikilink")、[圓形](../Page/圓形.md "wikilink")、[三角形以至其他](../Page/三角形.md "wikilink")[形狀的](../Page/形狀.md "wikilink")[紙張都能夠用來製作摺紙作品](../Page/紙張.md "wikilink")。

另外一種[摺紙則是指必須使用張完整的](../Page/摺紙_\(Origami\).md "wikilink")[正方形紙](../Page/正方形.md "wikilink")，並且不能包含切割、剪裁、撕開或用黏膠黏貼。

## 歷史

現代普遍認為摺紙在[日本得到更加廣泛的發展](../Page/日本.md "wikilink")，在近代研究下被發現應源自[中國](../Page/中國.md "wikilink")\[1\]。中國歷史中，摺紙主要是兒童用作消遣時間的一門傳統藝術。後來經[日本摺紙創作家](../Page/日本摺紙.md "wikilink")[吉澤章加以改良](../Page/吉澤章.md "wikilink")，使之創新。他提出了[濕摺法](../Page/濕摺法.md "wikilink")，並與[美國人](../Page/美國.md "wikilink")發明了[吉澤章-蘭德列特圖解摺紙記號系統](../Page/吉澤章-蘭德列特系統.md "wikilink")。

自1960年代起，出現許多不同種類的摺紙技巧和相關研究活動，又被稱為「現代摺紙」。從[組合式摺紙和傳統作品為基礎](../Page/組合式摺紙.md "wikilink")，衍生出各式各樣的摺紙類型和設計方式。

## 紙張與其他材料

[Smallcrane.jpg](https://zh.wikipedia.org/wiki/File:Smallcrane.jpg "fig:Smallcrane.jpg")
雖然幾乎所有薄片狀材料都可以摺疊，但材料的選用會直接影響摺疊的效果，以至模型的最終外型。

標準[影印紙](../Page/影印.md "wikilink")（70–90
g/m²）適用於簡單的摺疊，例如[鶴和水彈](../Page/紙鶴.md "wikilink")。較重的紙（100
g/m²或以上）適用於[濕摺](../Page/濕摺法.md "wikilink")。濕摺法可替模型塑造較立體並能夠持久的造形，因為濕水部分乾後會變得堅硬。

特別的摺紙用[紙](../Page/紙.md "wikilink")（[英語稱作](../Page/英語.md "wikilink")「kami」，即[日語](../Page/日語.md "wikilink")「」）通常以預先包製的方形紙的形式售賣，各種尺寸由2.5至25厘米或更大不等。常見的摺紙用紙是一面有色，一面白色的；不過，雙面顏色或有圖案的色紙亦見於市面，這些最適合雙色作品。至於較輕或較薄的摺紙用紙，則可適用更廣的作品範圍。

有襯鋁箔紙，顧名思義，是一張與薄紙張膠合的金屬薄片。

某些摺紙愛好者也會自製所謂「合成紙」，也就是將鋁箔與薄棉紙膠合起來使用。

## 摺紙形式與技法

[Ap_20080528082915442.jpg](https://zh.wikipedia.org/wiki/File:Ap_20080528082915442.jpg "fig:Ap_20080528082915442.jpg")
最近幾年，摺紙基本技巧的運用除了傳統的「傘形摺法」以外，「蛇腹‧段摺法」也被普遍運用。「蛇腹‧段摺法」最成功的表現者應屬日本之北條高史，其創作之摺紙作品原創性極高，可惜後來者之作品複雜度雖高，但創意顯然不如北條氏，且形式逐漸僵化。

利用經緯排列的正方形格眼(格數多為2和3的倍數)及45度的轉摺，使許多摺紙模型（造型）的實現可能性大幅度提高。

摺紙的學習與研究涵蓋[數學的不同範疇](../Page/數學.md "wikilink")。譬如，[平面可摺性的問題](../Page/平面可摺性.md "wikilink")（一件起縐的形態能否摺成二維模型）是重要的數學研究主題。

值得注意的是，紙張面上的所有點展現零[高斯曲率](../Page/高斯曲率.md "wikilink")，而僅僅沿零曲率的線條自然地摺疊。但是，沿曲率不起摺痕的紙張面，可透過濕紙張或手指實現，並不展現這限制。

[硬式摺紙的問題](../Page/硬式摺紙.md "wikilink")（「若用薄金屬板取代紙張，而摺線中有節點，我們能否仍舊摺出模型？」）有重大的實用價值。舉例，[三浦公亮提出的硬式摺疊](../Page/三浦公亮.md "wikilink")──[三浦摺疊](../Page/三浦摺疊.md "wikilink")（）已應用於為太空人造衛星部署大型太陽電池板的陣列。

## 工藝摺紙

工藝摺紙，又稱「摺紙設計」。

## 摺紙書籍的作者

  - [吉澤章](../Page/吉澤章.md "wikilink") -
    現代摺紙的創始者之一，致力於製作現代[摺紙圖解的作品](../Page/摺紙圖解.md "wikilink")

  - [神谷哲史](../Page/神谷哲史.md "wikilink") - 世界級的日本摺紙大師之一

  - \- 摺紙書作家，著作之一為《摺紙設計祕密》（Origami Design Secrets）

  - [Peter Engel](../Page/Peter_Engel.md "wikilink") - 影響深遠的摺紙藝術家、理論家

  - \- 以立體盒子和組合式幾何摺紙著名

  - \- 英國的大眾化摺紙作家

## 參考文獻

## 參見

  - [組合式摺紙](../Page/組合式摺紙.md "wikilink")（）

  - [摺紙數學](../Page/摺紙數學.md "wikilink")（）

  - [吉澤章-蘭德列特系統](../Page/吉澤章-蘭德列特系統.md "wikilink")，一种[折纸步骤图表示系统](../Page/折纸步骤图.md "wikilink")

  - [中國摺紙](../Page/中國摺紙.md "wikilink")（）

  - [日本摺紙](../Page/日本摺紙.md "wikilink")（）

  - [紙飛機](../Page/紙飛機.md "wikilink")

  - （）

  - [摺紙戰士](../Page/摺紙戰士.md "wikilink")（[漫畫](../Page/漫畫.md "wikilink")、[動畫](../Page/動畫.md "wikilink")）

  - [摺紙圖](../Page/摺紙圖.md "wikilink")

## 外部連結

  - [The math and magic of
    origami](http://www.ted.com/talks/robert_lang_folds_way_new_origami.htm)
  - [纸飞机](https://www.youtube.com/watch?v=AayKyuR6uys&feature=youtu.be/)

[摺紙](../Category/摺紙.md "wikilink")
[Category:藝術技巧](../Category/藝術技巧.md "wikilink")

1.  Lang, Robert James. \[1988\] (1988). The Complete Book of Origami:
    Step-by Step Instructions in Over 1000 Diagrams/48 Original Models.
    Courier Dover Publications. ISBN 0486258378