**撒克遜薩州花粉**（[學名](../Page/學名.md "wikilink")：*Saxonipollis
saxonicus*）為[已滅絕的一種](../Page/滅絕.md "wikilink")[植物](../Page/植物.md "wikilink")。僅能由化石化的[花粉來瞭解它](../Page/花粉.md "wikilink")，發現於[東德的](../Page/東德.md "wikilink")[始新世沈積物](../Page/始新世.md "wikilink")。\[1\]

本種是可能是[貉藻屬的先驅](../Page/貉藻屬.md "wikilink")，或是與它的先驅有近緣關係。\[2\]

## 注釋

[Category:茅膏菜科](../Category/茅膏菜科.md "wikilink")
[Category:食虫植物](../Category/食虫植物.md "wikilink")
[Category:史前植物](../Category/史前植物.md "wikilink")
[Category:始新世生物](../Category/始新世生物.md "wikilink")

1.  Krutzsch, W. 1970. Zur Kenntnis fossiler disperses Tetradenpollen.
    *Abh. Zentr. Geol. Inst. (Berlin), Palaeontol. Abh., B.* **3**(3):
    399-433.
2.  Degreef, J.D. 1997.  *[Carnivorous Plant
    Newsletter](../Page/Carnivorous_Plant_Newsletter.md "wikilink")*
    **26**(3): 93–97.