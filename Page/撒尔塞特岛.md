[Bombaycitydistricts.png](https://zh.wikipedia.org/wiki/File:Bombaycitydistricts.png "fig:Bombaycitydistricts.png")大都会区和[塔纳市](../Page/塔纳.md "wikilink")\]\]

**撒尔塞特岛**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：Salsete,
[马拉地语](../Page/马拉地语.md "wikilink")：Sashti（साष्टी））是[印度](../Page/印度.md "wikilink")[马哈拉施特拉邦西海岸的一个岛屿](../Page/马哈拉施特拉邦.md "wikilink")。[孟买大都会区和](../Page/孟买.md "wikilink")[塔纳市都位于岛上](../Page/塔纳.md "wikilink")。

## 位置

今天的撒尔塞特岛原来是几个小岛，在19世纪和20世纪初被连成一个岛屿。它的界限北面到Vasai溪，东北到Ulhas河，东面到塔纳[溪和](../Page/溪.md "wikilink")[孟买湾](../Page/孟买湾.md "wikilink")，南面和西面濒临[阿拉伯海](../Page/阿拉伯海.md "wikilink")。孟买市位于该岛南端的半岛上，市区和郊区加在一起，覆盖了该岛的大部分地方。该岛北部拥有[桑贾伊·甘地国家公园](../Page/桑贾伊·甘地国家公园.md "wikilink")。塔那市位于该岛的东北角，濒临塔纳溪。地理坐标大约为北纬20°N，东经72°。在行政上，该岛大部分属于孟买市辖区。在行政上被分为2个[区](../Page/Districts_of_Maharashtra.md "wikilink")，孟买市区和孟买郊区。该岛北部归塔纳区，塔纳区通过Vasai溪和塔纳河延伸到大陆。

## 历史

“撒尔塞特”这个名字的意思是"六十六个村庄"。今天的撒尔塞特岛在以前是几个各自独立的岛屿。目前岛屿的北部和中部的大部分是历史上的撒尔塞特岛，而岛屿南部，包括孟买市区在内，在以前是从撒尔塞特岛向南延伸[七个小岛](../Page/孟买七岛.md "wikilink")（Mahim,
Bombay, Mazagaon, Parel, Colaba, Little
Colaba和Sion）。[Trombay岛位于撒尔塞特岛的东南](../Page/Trombay.md "wikilink")。

## 地理

*参见：* [孟买地理](../Page/孟买地理.md "wikilink")

该岛部分地方丘陵起伏，尽管许多小山被削平用来填海，连接原有的七岛，扩大城市面积。该岛的最高点在北部的[桑贾伊·甘地国家公园内](../Page/桑贾伊·甘地国家公园.md "wikilink")，海拔450米。这个[国家公园是世界最大的一个位于市区界限以内的国家公园](../Page/国家公园.md "wikilink")。

### 地质

### 其它自然形态

#### 湖泊

这里有3个主要湖泊[Powai湖](../Page/Powai湖.md "wikilink")、[Tulsi湖和](../Page/Tulsi湖.md "wikilink")[Vihar湖后面两个湖泊供应城市部分水源](../Page/Vihar湖.md "wikilink")。在[Thane区还有大量的小池塘和湖泊](../Page/Thane.md "wikilink")。在Powai湖岸边是著名的[印度理工学院孟买分校](../Page/印度理工学院.md "wikilink")。

#### 河流

有3条小河发源于国家公园内：Mithi
(Mahim)河、Oshiwara河和Dahisar河注入阿拉伯海。Mithi河发源于Powai湖。Vasai和Thane溪是[Ulhas河的](../Page/Ulhas河.md "wikilink")[河口](../Page/河口.md "wikilink")[支流](../Page/支流.md "wikilink")

#### 小溪

许多盐溪或[咸水溪从海岸向内陆延伸](../Page/咸水.md "wikilink")。Mahim溪从西部，Sion溪（Sion溪已不存在）从东部将市区与郊区分开。在西海岸更靠北的地方，Oshiwara河注入Malad溪，Dahisar河注入Gorai溪。东部海滨也有许多小溪。

#### 湿地

#### 海滩

孟买的西海岸有许多海滩，最著名的是焦柏蒂海滩

[Category:孟买地理](../Category/孟买地理.md "wikilink")
[Category:印度岛屿](../Category/印度岛屿.md "wikilink")