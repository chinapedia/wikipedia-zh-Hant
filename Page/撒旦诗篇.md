《**撒旦诗篇**》（，另譯名為**-{zh-cn:魔鬼诗篇;zh-tw:撒旦詩篇;}-**），[萨尔曼·鲁西迪的第四部小说](../Page/萨尔曼·鲁西迪.md "wikilink")，出版於1988年9月26日，其灵感来源于[穆罕默德的生活](../Page/穆罕默德.md "wikilink")。

在[英国](../Page/英国.md "wikilink")，《撒旦诗篇》得到评论界的广泛接受，它曾是1988年[布克奖的候选作品](../Page/布克奖.md "wikilink")，但最终输给了[彼得·凯利的](../Page/彼得·凯利.md "wikilink")《[奥斯卡与露辛达](../Page/奥斯卡与露辛达.md "wikilink")》。

## 全球追殺令

在[伊斯兰国家](../Page/伊斯兰国家.md "wikilink")，《撒旦诗篇》引发了大论战，因为许多[穆斯林指责该书](../Page/穆斯林.md "wikilink")[亵渎伊斯兰教先知穆罕默德](../Page/亵渎.md "wikilink")。1989年2月16日，鲁西迪遭[伊朗精神领袖](../Page/伊朗精神领袖.md "wikilink")[何梅尼下达追杀令](../Page/赛义德·鲁霍拉·霍梅尼.md "wikilink")。在这次伊斯兰教判决之后，鲁西迪被置於英国警方保护之下，英伊兩國甚至一度[斷交](../Page/斷交.md "wikilink")。尽管伊朗於1998年发表了一份妥协的声明，鲁西迪也表示不再继续躲躲藏藏的生活，但至今[伊朗宣布的死刑判决并未废除](../Page/伊朗.md "wikilink")。

尽管鲁西迪一直未受到任何身体侵犯，但《撒旦诗篇》的各國翻译者与出版者中已有多人遇襲受傷甚至身亡。1991年7月3日，[義大利文版翻譯Ettore](../Page/義大利文.md "wikilink")
Capriolo在[米蘭遇刺重傷](../Page/米蘭.md "wikilink")；1991年7月11日，[日文版翻譯](../Page/日文.md "wikilink")[五十嵐一](../Page/五十嵐一.md "wikilink")[遇刺身亡](../Page/《撒旦诗篇》译者被杀事件.md "wikilink")，他的屍體在[筑波大學辦公室被發現](../Page/筑波大學.md "wikilink")；\[1\]1993年7月2日，[土耳其文版翻譯](../Page/土耳其文.md "wikilink")[阿吉兹·涅辛在土耳其](../Page/阿吉兹·涅辛.md "wikilink")[錫瓦斯下榻的旅館遭到暴民縱火](../Page/錫瓦斯.md "wikilink")，37人死亡；\[2\]1993年10月，[挪威版出版商William](../Page/挪威.md "wikilink")
Nygaard在[奧斯陸遭槍擊三次](../Page/奧斯陸.md "wikilink")，但倖存。悬赏追杀作者的奖金被一再提高，现在已经高达3百万美元。激进团体还坚称，追杀令依然有效，至今从未改口。

## 注釋

[Category:宗教题材小说](../Category/宗教题材小说.md "wikilink")
[Category:英国小说](../Category/英国小说.md "wikilink")
[Category:英文小说](../Category/英文小说.md "wikilink")
[Category:1988年小说](../Category/1988年小说.md "wikilink")
[Category:伊斯蘭教審查制度](../Category/伊斯蘭教審查制度.md "wikilink")
[Category:伊斯兰教相关争议](../Category/伊斯兰教相关争议.md "wikilink")

1.
2.  [Freedom of Expression after the “Cartoon
    Wars”](http://www.freedomhouse.org/uploads/fop/FOP2006cartoonessay.pdf)
    By Arch Puddington, Freedom House, 2006