**撒马尔罕**（
Самарқанд；；）是[中亞地區的歷史名城](../Page/中亚.md "wikilink")，也是伊斯蘭學術中心，現在是[乌兹别克斯坦的舊都兼第二大城市](../Page/乌兹别克斯坦.md "wikilink")、[撒马尔罕州的](../Page/撒马尔罕州.md "wikilink")[首府](../Page/首府.md "wikilink")，首任總統[伊斯拉木·卡里莫夫生於此城](../Page/伊斯拉木·卡里莫夫.md "wikilink")。“撒马尔罕”一词在粟特语中意为“石城”或“石要塞”、“石堡垒”；另根据[耶律楚材说](../Page/耶律楚材.md "wikilink")：“寻思干者西人云肥也，以地土肥饶故名之。\[1\]”

十四世紀時為[帖木兒帝國國都](../Page/帖木兒帝國.md "wikilink")，這裡也是[帖木兒陵墓的遺址Gur](../Page/帖木兒.md "wikilink")-e
Amir所在地。Bibi-Khanym清真寺也是著名地標之一，Registan則是市區古老的中心。

在2001年時，[聯合國教育、科學及文化組織將擁有](../Page/聯合國教育、科學及文化組織.md "wikilink")2750年之久歷史的撒馬爾罕編入[世界遗产之列](../Page/世界遗产.md "wikilink")，稱為“文化交匯之地”（“Crossroads
of Cultures”，列入[亚洲和大洋洲世界遗产](../Page/亚洲和大洋洲世界遗产.md "wikilink")）。

## 歷史

撒马尔罕建立于公元前3世纪。由于地处[絲綢之路上中国和中东的交界處](../Page/絲綢之路.md "wikilink")，使當地成為兩地貨物的交流地，並促使當地經濟繁盛。

撒馬爾罕城市中的[粟特人在絲綢之路中做出了卓越的貢獻](../Page/粟特.md "wikilink")。[粟特人也是伊斯蘭化之前撒馬爾罕主要民族](../Page/粟特.md "wikilink")。

[亞歷山大大帝的傳記中記載了馬蘭坎逹](../Page/亚历山大大帝.md "wikilink")（撒馬爾罕的希臘語名字）居民激烈的抵抗。最終他們還是屈服於亞歷山大大帝。\[2\]

中國古代便對撒馬爾罕有較多的了解。《[魏书](../Page/魏书.md "wikilink")》称为悉万斤，《[隋书](../Page/隋书.md "wikilink")·西域记》称为康国，唐[慧超](../Page/慧超.md "wikilink")《[往五天竺国传](../Page/往五天竺国传.md "wikilink")》作康国，屬[昭武九姓之一](../Page/昭武九姓.md "wikilink")\[3\]，唐[杜环](../Page/杜环.md "wikilink")《[经行记](../Page/经行记.md "wikilink")〉作[康国](../Page/康国.md "wikilink")、萨末建\[4\]，《[新唐书](../Page/新唐书.md "wikilink")》称为康国、萨秣建，玄奘《[大唐西域记](../Page/大唐西域记.md "wikilink")》称为飒秣建国，元[耶律楚材](../Page/耶律楚材.md "wikilink")《[西游录](../Page/西游录.md "wikilink")》作寻思干\[5\]，《[长春真人西游记](../Page/长春真人西游记.md "wikilink")》作邪米思干，《[元史](../Page/元史.md "wikilink")》作薛迷思加，明[陈诚](../Page/陳誠_\(明朝\).md "wikilink")《[西域番国志](../Page/西域番国志.md "wikilink")》\[6\]、《[明史](../Page/明史.md "wikilink")》中稱「撒馬兒罕，即漢罽賓之地，隋曰漕國，唐復名罽賓，皆通中國。」、明[严从简](../Page/严从简.md "wikilink")《[殊域周咨录](../Page/殊域周咨录.md "wikilink")》均作撒马儿罕\[7\]。

《[回回館譯語](../Page/回回館譯語.md "wikilink")》中称**撒馬兒罕**。\[8\]\[9\]

撒马尔罕曾經是[花剌子模的首都](../Page/花剌子模.md "wikilink")。當年[成吉思汗攻打撒马尔罕之時](../Page/成吉思汗.md "wikilink")，包圍八天後破城。所以在撒马尔罕陷落之後，成吉思汗下令屠城。和伊斯蘭教長有關係的五萬人可留在城內。其他不出城的格殺勿論。下令把十萬[康里人處死](../Page/康里.md "wikilink")，其他人用二十萬[第納爾贖金免死](../Page/第納爾.md "wikilink")。这里人口多是說波斯語的[塔吉克人](../Page/塔吉克人.md "wikilink")（因「[颯秣鞬](../Page/颯秣鞬.md "wikilink")」本即[突厥語辞彙](../Page/突厥語.md "wikilink")），唐時[突厥化](../Page/突厥化.md "wikilink")，[西突厥汗國曾統治這座城市](../Page/西突厥.md "wikilink")，但仍讓撒馬爾罕當地王繼續為王。[玄奘西行時曾到訪撒馬爾罕](../Page/玄奘.md "wikilink")。\[10\]16世纪至苏联时期[烏兹别克化](../Page/烏兹别克化.md "wikilink")。

## 氣候

## 友好城市

  - [阿富汗](../Page/阿富汗.md "wikilink")[巴尔赫](../Page/巴尔赫.md "wikilink")

  - [-{zh-cn:土库曼斯坦; zh-hk:土庫曼斯坦;
    zh-tw:土庫曼}-](../Page/土库曼斯坦.md "wikilink")[梅尔夫](../Page/梅尔夫.md "wikilink")

  - [乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")[布哈拉](../Page/布哈拉.md "wikilink")

  - [伊朗](../Page/伊朗.md "wikilink")[內沙布爾](../Page/內沙布爾.md "wikilink")

  - [秘鲁](../Page/秘鲁.md "wikilink")[库斯科](../Page/库斯科.md "wikilink")

  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")[拉合尔](../Page/拉合尔.md "wikilink")

  - [乌克兰](../Page/乌克兰.md "wikilink")[利沃夫](../Page/利沃夫.md "wikilink")

  - [土耳其](../Page/土耳其.md "wikilink")[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")

  - [土耳其](../Page/土耳其.md "wikilink")[伊兹密尔](../Page/伊兹密尔.md "wikilink")

## 交通

  - [撒馬爾罕國際機場](../Page/撒馬爾罕國際機場.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:乌兹别克斯坦城市](../Category/乌兹别克斯坦城市.md "wikilink")
[Category:烏茲別克世界遺產](../Category/烏茲別克世界遺產.md "wikilink")
[Category:西域](../Category/西域.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")
[Category:絲綢之路聚居地](../Category/絲綢之路聚居地.md "wikilink")

1.  （元）耶律楚材《[西游录](../Page/西游录.md "wikilink")》
2.
3.  慧超《往五天竺国传》第三十四节
4.  杜环《经行记》第二节
5.  耶律楚材《西游录》上
6.  陈诚《西域番国志·撒马儿罕》
7.  严从简《殊域周咨录》十五卷《撒马儿罕》
8.  <https://archive.org/stream/02076762.cn#page/n6/mode/2up>
9.  <http://www.huizucn.org/article-16-2.html>
10.