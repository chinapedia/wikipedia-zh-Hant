**擬態革魨**（[学名](../Page/学名.md "wikilink")：），又稱**長尾革單棘魨**，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[魨形目](../Page/魨形目.md "wikilink")[鱗魨亞目](../Page/鱗魨亞目.md "wikilink")[單棘魨科的其中一種](../Page/單棘魨科.md "wikilink")。分布于印度洋非洲东岸到太平洋夏威夷群岛、南至澳大利亚、北至日本以及[西沙群岛](../Page/西沙群岛.md "wikilink")、南沙群岛等海域等，属于暖水性鱼类。其常生活于在热带海区的海藻间觅食。\[1\]

Image:Aluterus scriptus 600.jpg Image:A_scriptus.jpg Image:Scribbled
filefish2.jpg Image:Aluterus_scriptus_02_Pengo.jpg

## 深度

水深2至80公尺。

## 特徵

本魚體型較其他單棘魨瘦長，呈橢圓形，頭高約略等魚體高，成魚體色以暗灰色為底，身上散佈著許多黑色小斑點集許多不規則的藍紋；尾鰭長且末端呈圓弧形，背鰭兩個，第二背鰭棘退化埋於皮下，背鰭鰭條45至47枚、臀鰭鰭條46至49枚。幼魚體色則以金黃色為底，身上有許多橘色塊斑，其上並有許多小黑斑，尾鰭上亦有些小黑斑。身體粗糙，被小鱗片，體長可達100公分。

## 生態

本魚棲息在礁湖或向海礁坡上，幼魚長可見在開放海域有漂浮物的地方發現，偶而也看到成魚；多單獨行動。幼魚有時候會靜止的倒立在海底，屬雜食性。

## 經濟利用

體內有劇毒只作為觀賞魚，不可食用

## 参考文献

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[Category:有毒魚類](../Category/有毒魚類.md "wikilink")
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[scriptus](../Category/革魨屬.md "wikilink")
[Category:1765年描述的動物](../Category/1765年描述的動物.md "wikilink")

1.