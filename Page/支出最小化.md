**支出最小化问题**，在[经济学](../Page/经济学.md "wikilink")，特别是[微观经济学中是](../Page/微观经济学.md "wikilink")[效用最大化问题的](../Page/效用最大化问题.md "wikilink")[对偶问题](../Page/对偶问题.md "wikilink")，直观地说就是：“需要多少钱我才能让我觉得高兴？”
给定[效用函数](../Page/效用函数.md "wikilink")，[价格](../Page/价格.md "wikilink")，和效用目标，这个问题可以分为两部分。

  - 消费者需要多少钱？这个由[支出函数来回答](../Page/支出函数.md "wikilink")。
  - 消费者会买些什么在最小化支出的情况下满足其的效用目标？这个由[希克斯需求对应来回答](../Page/希克斯需求对应.md "wikilink")。

## 支出函数

假设消费者有一个定义在\(L\)种商品\(\mathbf{x}\in\mathbb{R}^L_+\)上的效用函数\(u\)。则该消费者的支出函数给出在给定价格为\(\mathbf{p}\)的情况下购买一组商品\(x\)，使得得到的效用\(u(x)\geq u^*\)时所需钱的数额。
形式上，支出函数可以这样定义。

\[e(\mathbf{p}, u^*) = \min_{\mathbf{x} \in \geq(u^*)} \mathbf{p} \cdot \mathbf{x}\]

这里

\[\geq(u^*):= \{\mathbf{x} \in \mathbb{R}^L_+ | u(\mathbf{x}) \geq u^*\}\]，

表示所得效用至少等于\(u^*\)的组合的集合。

## 希克斯需求对应

**希克斯需求对应**
\(\mathbf{h}(\mathbf{p}, u^*)\)定义为一个得到所需效用的最便宜组合。它也可以用支出函数来定义[马歇尔需求对应](../Page/马歇尔需求对应.md "wikilink")，

\[\mathbf{h}(\mathbf{p}, u^*) = \mathbf{x}(\mathbf{p}, e(\mathbf{p}, u^*))\]。

如果马歇尔需求对应\(\mathbf{x}(\mathbf{p}, w)\)是一个总是给出唯一解的函数，则与之相对应的希克斯需求对应\(\mathbf{h}(\mathbf{p},u^*)\)
也可以被称为是**希克斯需求函数**。

## 相关条目

  - [效用最大化问题](../Page/效用最大化问题.md "wikilink")

## 参考文献

  - Mas-Colell, A., M. Whinston, and J. Green, 1995, *Microeconomic
    Theory*. Oxford: Oxford University Press. ISBN 0195073401

[Category:微观经济学](../Category/微观经济学.md "wikilink")
[Category:最优化](../Category/最优化.md "wikilink")