[Antique_cash_register.png](https://zh.wikipedia.org/wiki/File:Antique_cash_register.png "fig:Antique_cash_register.png")

**收銀機**是一部[儀器](../Page/儀器.md "wikilink")，由人手操作，設置於[零售商店](../Page/零售.md "wikilink")，由[收銀員打入每位顧客所購買的貨品牌價](../Page/收銀員.md "wikilink")，收銀機會计算出其總消費金額。收銀機是[日記賬式记录當天的所有贸易](../Page/日記賬.md "wikilink")。在每天，商店休息前，店員會點算收銀機內的實際[現金額](../Page/現金.md "wikilink")，再與機內的日記賬（俗稱蛇仔）總額比對，此為[內部審計之一](../Page/內部審計.md "wikilink")。通常收银机也打印收据给顾客，為保證每單交易在日記賬必有記錄。有不少店主提示顧客，購物必須要索取機印收據，若收銀員收銀後不打印收據，顧客可以向店主投訴。

## 歷史

。James是[美国](../Page/美国.md "wikilink")[俄亥俄州一间酒栈的老板](../Page/俄亥俄州.md "wikilink")，为了監督不诚实的员工，保障店主的[收入](../Page/收入.md "wikilink")，因此发明收銀機。

## 現況

在[超級市場等商店](../Page/超級市場.md "wikilink")，收銀機是一部[POS的電腦](../Page/POS.md "wikilink")[終端機](../Page/終端機.md "wikilink")，它連線至總公司的電腦中心，作[實時](../Page/實時.md "wikilink")[ERP及資訊流管理](../Page/ERP.md "wikilink")。
[Nuvola_check-out.svg](https://zh.wikipedia.org/wiki/File:Nuvola_check-out.svg "fig:Nuvola_check-out.svg")

## 相關

  - [物流管理](../Page/物流管理.md "wikilink")
  - [審計學](../Page/審計學.md "wikilink")
  - [會計](../Page/會計.md "wikilink")
  - [條碼資料輸入](../Page/條碼.md "wikilink")
  - [透寫紙](../Page/透寫紙.md "wikilink")
  - [感光紙](../Page/感光紙.md "wikilink")
  - [打印機](../Page/打印機.md "wikilink")
  - [八達通](../Page/八達通.md "wikilink")
  - [八達通日日賞](../Page/八達通日日賞.md "wikilink")

## 外部連結

  - [Cash register
    terms](http://www.cashregistergroup.com/acatalog/Glossary.html)
  - [Cash register forum](http://www.tillservices.co.uk/discus)
  - [Cash register instruction
    manuals](https://web.archive.org/web/20170523222430/http://cash-register.info/)
  - [Restaurant POS 餐馆电脑](https://www.i1024.com/pos/)

[Category:现金](../Category/现金.md "wikilink")
[Category:商業](../Category/商業.md "wikilink")
[Category:工具](../Category/工具.md "wikilink")