**收音机**,是一种小型的[无线电接收机](../Page/无线电接收机.md "wikilink")。主要用于接受[无线电](../Page/无线电.md "wikilink")[電台的](../Page/電台.md "wikilink")[广播节目](../Page/广播.md "wikilink")，收听无线电发射台，通常是广播电台发送的[娛樂及](../Page/娛樂.md "wikilink")[資訊节目](../Page/資訊.md "wikilink")。它有異於[軍事用途的無線電接收器](../Page/軍事.md "wikilink")，後者不是廣播。

## 分类

### 按照大小

  - [台式收音机](../Page/台式收音机.md "wikilink")。这一类型常见于1970-1980年代的[中國](../Page/中國.md "wikilink")[家庭](../Page/家庭.md "wikilink")。在中國一些家庭还能看到。这类收音机一般是电子管或者晶体管收音机。比较费电。
  - [便携式收音机](../Page/便携式收音机.md "wikilink")
  - [口袋型收音机](../Page/口袋型收音机.md "wikilink")
  - [袖珍式收音机](../Page/袖珍式收音机.md "wikilink")。这种类型的收音机体积小巧，便于携带，但是技术指标和科技含量一点也不比普通收音机差。\[1\]
  - [微型收音机](../Page/微型收音机.md "wikilink")。内置于磁带播放机CD播放机内的收音机，有自己的独立电路部分和调谐装置，甚至供电部分也独立。通过特定的开关打开。
  - [内置收音机](../Page/内置收音机.md "wikilink")。作为电子设备例如[手机](../Page/手机.md "wikilink")\[2\]，[MP3播放器](../Page/MP3.md "wikilink")\[3\]，CD播放机\[4\]，卫星接收器，[机顶盒等的功能组成部分](../Page/机顶盒.md "wikilink")。全数字化操作。所有部件由一块集成芯片完成。

### 按元器件分類

  - [真空管收音機](../Page/真空管收音機.md "wikilink")
  - [礦石收音機](../Page/礦石收音機.md "wikilink")
  - [半導體收音機](../Page/半導體收音機.md "wikilink")
  - [積體電路收音機](../Page/積體電路收音機.md "wikilink")

### 按解调方式和波长

  - [调幅收音机](../Page/幅度调制.md "wikilink") (AM)
      - [长波收音机](../Page/长波.md "wikilink") (LW)
      - [中波收音机](../Page/中波.md "wikilink") (MW)
      - [短波收音机](../Page/短波廣播.md "wikilink") (SW)

<!-- end list -->

  -
    短波收音機的頻率大於3兆赫，接送波長範圍為10到80公尺\[5\]

<!-- end list -->

  - [调频收音机](../Page/频率调制.md "wikilink") (FM)\[6\]

## 知名品牌

  - [德生](../Page/德生.md "wikilink")

  - [山進](../Page/山進.md "wikilink")

  - [根德](../Page/根德.md "wikilink")

  - [索尼](../Page/索尼.md "wikilink")

  - [东芝](../Page/东芝.md "wikilink")

  - [松下](../Page/松下.md "wikilink")

  - [日立制作所](../Page/日立制作所.md "wikilink")

  - [伊顿](../Page/伊顿_\(消费电子公司\).md "wikilink")

## 图片

<File:Sangean_WR-11.jpg>|[山進WR](../Page/山進.md "wikilink")-11 收音機
<File:Tecsun> DR-910.jpeg|[德生DR](../Page/德生.md "wikilink")-910收音机
[File:Radio.jpg|Letron多波段收音機](File:Radio.jpg%7CLetron多波段收音機)

## 參考文獻

## 参看

  - [无线电通讯](../Page/无线电通讯.md "wikilink")
  - [发射机](../Page/发射机.md "wikilink")
  - [矿石收音机](../Page/矿石收音机.md "wikilink")
  - [行山](../Page/行山.md "wikilink")[對講機](../Page/對講機.md "wikilink")
  - [網路電台](../Page/網路電台.md "wikilink")

## 外部連結

  -
[de:Rundfunkempfänger](../Page/de:Rundfunkempfänger.md "wikilink")
[en:Receiver (radio)](../Page/en:Receiver_\(radio\).md "wikilink")
[fr:Récepteur radio](../Page/fr:Récepteur_radio.md "wikilink")
[es:Receptor de radio](../Page/es:Receptor_de_radio.md "wikilink")
[ja:受信機](../Page/ja:受信機.md "wikilink")

[R](../Category/消費電子產品.md "wikilink") [R](../Category/無線電.md "wikilink")
[R](../Category/通讯设备.md "wikilink") [\*](../Category/收音机.md "wikilink")

1.  [什么是袖珍型收音机？](http://zhidao.baidu.com/question/293922332.html)_百度知道
2.  [调频收音机](http://zhidao.baidu.com/question/544973433.html)_百度知道
3.  [带收音机功能的MP3有哪些？](http://zhidao.baidu.com/question/336283441.html)_百度知道
4.  [一般台式CD机带收音机吗？](http://zhidao.baidu.com/question/341171806.html)_百度知道
5.  [藝術與建築索引典—短波收音機](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300266363)
     於2010年8月6日查閱
6.  [fm调频收音机简介](http://zhidao.baidu.com/question/177703414.html)_百度知道