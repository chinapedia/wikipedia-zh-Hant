是由[松野泰己擔任總監](../Page/松野泰己.md "wikilink")，於2000年為[Square開發的第二款](../Page/史克威爾.md "wikilink")[角色扮演遊戲](../Page/電子角色扮演遊戲.md "wikilink")。遊戲獲得[日本遊戲雜誌](../Page/日本.md "wikilink")《[Fami通](../Page/Fami通.md "wikilink")》評為滿分40/40的頂級評價。故事以異教組織Müllenkamp襲擊公爵宅邸為起點，講述事敗後引致身為國家維持治安騎士的主角Ashley，及教會的聖印騎士團到廢墟──魔法都市Lea
Monde追捕其異教領袖Sydney的經過。

此遊戲的媒體評價甚高，但由於遊戲系統過於複雜，不利推廣銷售。PS版累計售出約87萬份\[1\]。

## 参考文献

[Category:2000年电子游戏](../Category/2000年电子游戏.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")
[Category:奇幻电子游戏](../Category/奇幻电子游戏.md "wikilink")
[Category:史克威尔游戏](../Category/史克威尔游戏.md "wikilink")
[Category:PlayStation
(游戏机)游戏](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")
[Category:PlayStation
Network游戏](../Category/PlayStation_Network游戏.md "wikilink")
[Category:僅有單人模式的電子遊戲](../Category/僅有單人模式的電子遊戲.md "wikilink")

1.  <http://www.vgchartz.com/game/2587/vagrant-story/>