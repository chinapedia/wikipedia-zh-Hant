《**放牛吃草**》（），是[迪士尼於](../Page/迪士尼.md "wikilink")2004年推出的第44部[经典动画长片](../Page/经典动画长片.md "wikilink")。本片在中国大陆暂不发行DVD。在台湾已经由博伟发行英文发音DVD。香港於2004年8月12日以[粵語版本上映此電影](../Page/粵語.md "wikilink")。

本片是以[美国西部拓荒时期为背景](../Page/美国西部.md "wikilink")。的片名其实来源于一首家喻户晓的歌曲，在美国早期的拓荒过程中，它表现出浓厚的田园情怀和浪漫的西部精神为人心动，这首被广为传唱的歌曲也最终被美国[堪萨斯州定为自己的州歌](../Page/堪萨斯州.md "wikilink")。

## 劇情

故事是發生在一個寡婦珍珠經營的農場，她由於無法支付貸款，因此面臨要被驅逐的命運，農場上的三隻母牛瑪姬、卡洛威夫人和葛瑞絲為了挽救農場的命運，於是決定想要逮捕一位通緝犯「阿拉米達·施林」，並以獲得優渥的懸賞獎金來為主人珍珠償還貸款，一匹之前曾當過獎金獵人座騎名叫巴克的馬兒也給予她們幫助，因為他想藉著抓到通緝犯來證明自己的能力，然而他們卻不知道，那位惡名昭彰的通緝犯會使用催眠術，讓他們陷入困境...。

## 配音員

<table>
<thead>
<tr class="header">
<th><p>英語配音</p></th>
<th><p>角色（中文）</p></th>
<th><p>角色（原文）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/蘿珊妮·巴爾.md" title="wikilink">蘿珊妮·巴爾</a>（<a href="../Page/:en:Roseanne_Barr.md" title="wikilink">Roseanne Barr</a>）</p></td>
<td><p>瑪姬</p></td>
<td><p>Maggie</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/茱蒂·丹契.md" title="wikilink">茱蒂·丹契</a></p></td>
<td><p>卡洛威夫人</p></td>
<td><p>Mrs. Calloway</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/珍妮佛·提莉.md" title="wikilink">珍妮佛·提莉</a></p></td>
<td><p>葛瑞絲</p></td>
<td><p>Grace</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小庫珀·古丁.md" title="wikilink">小庫珀·古丁</a>（<a href="../Page/:en:Cuba_Gooding,_Jr..md" title="wikilink">Cuba Gooding, Jr.</a>）</p></td>
<td><p>巴克</p></td>
<td><p>Buck</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藍迪·奎德.md" title="wikilink">藍迪·奎德</a>（<a href="../Page/蘭迪·奎德.md" title="wikilink">蘭迪·奎德</a>）</p></td>
<td><p>阿拉米達·施林</p></td>
<td><p>Alameda Slim</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/查爾斯·丹尼斯.md" title="wikilink">查爾斯·丹尼斯</a>（<a href="../Page/:en:Charles_Dennis.md" title="wikilink">Charles Dennis</a>）</p></td>
<td><p>李哥</p></td>
<td><p>Rico</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/查爾斯·海德.md" title="wikilink">查爾斯·海德</a>（<a href="../Page/:en:Charles_Haid.md" title="wikilink">Charles Haid</a>）</p></td>
<td><p>幸運傑克</p></td>
<td><p>Lucky Jack</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡洛兒·庫克.md" title="wikilink">卡洛兒·庫克</a>（<a href="../Page/:en:Carole_Cook.md" title="wikilink">Carole Cook</a>）</p></td>
<td><p>珍珠·蓋斯納</p></td>
<td><p>Pearl Gesner</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喬·佛拉赫蒂.md" title="wikilink">喬·佛拉赫蒂</a>（<a href="../Page/:en:Joe_Flaherty.md" title="wikilink">Joe Flaherty</a>）</p></td>
<td><p>山羊老賈</p></td>
<td><p>Jeb the Goat</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史蒂夫·布西密.md" title="wikilink">史蒂夫·布西密</a></p></td>
<td><p>衛斯理</p></td>
<td><p>Wesley</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/理查·李爾.md" title="wikilink">理查·李爾</a>（<a href="../Page/:en:Richard_Riehle.md" title="wikilink">Richard Riehle</a>）</p></td>
<td><p>山姆·布朗警長</p></td>
<td><p>Sheriff Sam Brown</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘭斯·李高爾.md" title="wikilink">蘭斯·李高爾</a>（<a href="../Page/:en:Lance_LeGault.md" title="wikilink">Lance LeGault</a>）</p></td>
<td><p>水牛老么</p></td>
<td><p>Junior the Buffalo</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/G·W·貝里.md" title="wikilink">G·W·貝里</a>（<a href="../Page/:en:G.W._Bailey.md" title="wikilink">G.W. Bailey</a>）</p></td>
<td><p>魯斯蒂</p></td>
<td><p>Rusty</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/派屈克·華柏頓.md" title="wikilink">派屈克·華柏頓</a>（<a href="../Page/派屈克·華博頓.md" title="wikilink">派屈克·華博頓</a>）</p></td>
<td><p>派屈克</p></td>
<td><p>Patrick</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛絲黛兒·哈里斯.md" title="wikilink">愛絲黛兒·哈里斯</a>（<a href="../Page/:en:Estelle_Harris.md" title="wikilink">Estelle Harris</a>）</p></td>
<td><p>雞奧黛麗</p></td>
<td><p>Audrey the Chicken</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/查理·戴爾.md" title="wikilink">查理·戴爾</a>（<a href="../Page/:en:Charlie_Dell.md" title="wikilink">Charlie Dell</a>）</p></td>
<td><p>奧利</p></td>
<td><p>Ollie</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬歇爾·艾弗隆.md" title="wikilink">馬歇爾·艾弗隆</a>（<a href="../Page/:en:Marshall_Efron.md" title="wikilink">Marshall Efron</a>）</p></td>
<td><p>賴瑞</p></td>
<td><p>Larry</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山姆·J·李文.md" title="wikilink">山姆·J·李文</a>（<a href="../Page/:en:Sam_J._Levine.md" title="wikilink">Sam J. Levine</a>）</p></td>
<td><p>威利兄弟</p></td>
<td><p>Willie Brothers</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鮑比·布洛克.md" title="wikilink">鮑比·布洛克</a>（<a href="../Page/:en:Bobby_Block.md" title="wikilink">Bobby Block</a>）<br />
<a href="../Page/基頓·薩維奇.md" title="wikilink">基頓·薩維奇</a>（<a href="../Page/:en:Keaton_Savage.md" title="wikilink">Keaton Savage</a>）<br />
<a href="../Page/羅斯·賽門特里斯.md" title="wikilink">羅斯·賽門特里斯</a>（<a href="../Page/:en:Ross_Simanteris.md" title="wikilink">Ross Simanteris</a>）</p></td>
<td><p>小豬們</p></td>
<td><p>The Piggy</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬克·華爾頓.md" title="wikilink">馬克·華爾頓</a>（<a href="../Page/:en:Mark_Walton.md" title="wikilink">Mark Walton</a>）</p></td>
<td><p>貝瑞與鮑伯</p></td>
<td><p>Barry &amp; Bob</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安·理查茲.md" title="wikilink">安·理查茲</a>（<a href="../Page/:en:Ann_Richards.md" title="wikilink">Ann Richards</a>）</p></td>
<td><p>安妮</p></td>
<td><p>Annie</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹尼斯·威佛.md" title="wikilink">丹尼斯·威佛</a>（<a href="../Page/:en:Dennis_Weaver.md" title="wikilink">Dennis Weaver</a>）</p></td>
<td><p>阿布納</p></td>
<td><p>Abner</p></td>
</tr>
<tr class="odd">
<td><p>不詳</p></td>
<td><p>摩斯</p></td>
<td><p>Morse</p></td>
</tr>
</tbody>
</table>

## 外部链接

  -
  -
  -
  -
  -
  -
[H](../Category/2004年電影.md "wikilink")
[H](../Category/美國动画電影.md "wikilink")
[H](../Category/迪士尼經典動畫長片電影.md "wikilink")
[H](../Category/牛主角故事.md "wikilink")
[Category:擬人化角色電影](../Category/擬人化角色電影.md "wikilink")