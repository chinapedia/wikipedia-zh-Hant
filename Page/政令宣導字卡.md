**政令宣導字卡**是早期[台灣](../Page/台灣.md "wikilink")[三家電視台](../Page/老三台.md "wikilink")，因宣導[政府的一些政令](../Page/政府.md "wikilink")，或者宣佈一些公告事項等所需透過[電視媒體所傳達的訊息字卡之一](../Page/電視.md "wikilink")。但此類宣導方法沒有現在[影片宣導的效果來得好](../Page/影片.md "wikilink")。

## 播放方式

[TaiwanGovernment.JPG](https://zh.wikipedia.org/wiki/File:TaiwanGovernment.JPG "fig:TaiwanGovernment.JPG")格式政令靜態字卡範例，本範例為防颱宣導（此圖並非由台視擷取，僅為模擬）。\]\]
[CTVTaiwanGovernment.JPG](https://zh.wikipedia.org/wiki/File:CTVTaiwanGovernment.JPG "fig:CTVTaiwanGovernment.JPG")格式政令靜態字卡範例，文字敘述同圖1.（此圖並非由[中視擷取](../Page/中視.md "wikilink")，僅為模擬）。\]\]
[TaiwanIDVTax.JPG](https://zh.wikipedia.org/wiki/File:TaiwanIDVTax.JPG "fig:TaiwanIDVTax.JPG")格式政令靜態字卡範例，本範例為[個人綜合所得稅之開徵](../Page/個人綜合所得稅.md "wikilink")（此圖並非由[台視擷取](../Page/台視.md "wikilink")，僅為模擬）。\]\]
[TTV_Program_Table.JPG](https://zh.wikipedia.org/wiki/File:TTV_Program_Table.JPG "fig:TTV_Program_Table.JPG")隔日節目預告一覽表（此圖並非由[台視擷取](../Page/台視.md "wikilink")，僅為模擬）。\]\]
[TaiwanMilitarylife1.png](https://zh.wikipedia.org/wiki/File:TaiwanMilitarylife1.png "fig:TaiwanMilitarylife1.png")格式政令靜態字卡，此範例為八十年後備軍人演習（此圖並非由台視擷取，僅為模擬）。\]\]
[台视播放民国国歌前的字卡.png](https://zh.wikipedia.org/wiki/File:台视播放民国国歌前的字卡.png "fig:台视播放民国国歌前的字卡.png")在播放[中华民国国歌之前播放的静态字卡](../Page/中华民国国歌.md "wikilink")（2009年7月6日画面）\]\]
每次「政令宣導字卡」的出現，是一個節目與一個節目的廣告時間播出，當每一張「政令宣導字卡」出現時，旁白會同時將該張字卡讀出來，一邊讓觀眾閱讀、一邊讓觀眾聽取，以加強觀眾對該政令與消息的瞭解。如一個政令不足以一張紙卡顯示時，幕後人員可能會以兩張以上的紙卡方式顯現。

旁白朗讀所有政令，均採用「[國語](../Page/現代標準漢語.md "wikilink")」，無其他地方語言之朗讀。故對一些不很精通國語的年長者而言，很可能不具有較好的效果。

## 格式

就這類字卡，將其分類於下列三種電視台的格式。

### 台視

  - [台視的靜態紙卡](../Page/台視.md "wikilink")，均以藍底白字為主，大多都是宣導政令、稅徵，以及[颱風動向等消息](../Page/颱風.md "wikilink")。
  - 男性旁白僅有[陳振中](../Page/陳振中.md "wikilink")；女性旁白自開播起分別有[傅筱燕](../Page/傅筱燕.md "wikilink")、[陳正美](../Page/陳正美.md "wikilink")、[黃麗珍](../Page/黃麗珍.md "wikilink")、[張蘋](../Page/張蘋.md "wikilink")、[蘇斐麗](../Page/蘇斐麗.md "wikilink")、[林懷生](../Page/林懷生.md "wikilink")、[劉淑芳](../Page/劉淑芳.md "wikilink")、[曲渝青](../Page/曲渝青.md "wikilink")、[鄢蘭](../Page/鄢蘭.md "wikilink")、[夏琍琍](../Page/夏琍琍.md "wikilink")、[劉定華等人](../Page/劉定華.md "wikilink")。
  - 不僅是政令宣導，[台視有時候也不在政令宣導時採用此類靜態紙卡](../Page/台視.md "wikilink")：收播前公佈隔日節目一覽表，也是採用此類藍底白字的靜態紙卡。
  - [台視節目跳槽至其他無線電視台播出時](../Page/台視.md "wikilink")，也是採用此類藍底白字的靜態紙卡，但是[文字的排列方向改為由左至右橫列](../Page/文字.md "wikilink")；交亂雜點的黑白畫面（Noise）之前的「再見」兩字紙卡，卻不是藍底白字的樣式。

### 中視

  - [中視的靜態紙卡](../Page/中視.md "wikilink")，大多以紅底白字為主，旁邊會加一些花邊作裝飾，同樣也是宣導[政令](../Page/政令.md "wikilink")、[稅徵等較多](../Page/稅徵.md "wikilink")。
  - [旁白只有女性](../Page/旁白.md "wikilink")（與[中視各節目名稱字卡的](../Page/中視.md "wikilink")[旁白為同一人](../Page/旁白.md "wikilink")），無男性[旁白](../Page/旁白.md "wikilink")，而女性[旁白的](../Page/旁白.md "wikilink")[名字有待確認](../Page/名字.md "wikilink")。

### 華視

  - [華視的靜態紙卡](../Page/華視.md "wikilink")，大多以[藍紫底白字為主](../Page/雪青色.md "wikilink")。[旁白也只有女性](../Page/旁白.md "wikilink")，但姓名未知。

## 消逝

現在由於影片宣導的效果比起靜態紙卡的效果更具有活力，而影音科技也比以往進步許多，故無論現在哪一家電視台，此類靜態紙卡之使用可說是少之又少。

1990年代至2000年代，[中視部份重播之八點檔連續劇](../Page/中視.md "wikilink")，也是採用靜態紙卡介紹節目名稱，但是那些靜態紙卡都是重新設計過的，並不沿用首播時用過的靜態紙卡；同時期的中視[電視動畫也是採用靜態紙卡介紹節目名稱](../Page/電視動畫.md "wikilink")，但直接以動畫截圖為底圖。2010年代，中視停用靜態紙卡，成為最後一家停用靜態紙卡的電視台。

## 其他

  - 除了政令宣導外，在早期，電視台公告（徵才啟事、設備故障、節目異動等）、新聞快報、與[廣告亦有利用字卡宣傳的型態](../Page/廣告.md "wikilink")。\[1\]
  - 早期三台在部分的節目開始前，會利用字卡顯示提供廠商等贊助商資訊。普通的節目大約一張，特別節目（尤其是國慶日時）字卡可能有八張以上（如祝賀某某[總統就任](../Page/總統.md "wikilink")）。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [台視政令宣導字卡三張](http://www.youtube.com/watch?v=227BcSVBl6g)（在台視1981年[中秋節](../Page/中秋節.md "wikilink")[特別節目](../Page/特別節目.md "wikilink")《君在前哨：[鄧麗君勞軍](../Page/鄧麗君.md "wikilink")[專輯](../Page/專輯.md "wikilink")》預告片前播映）
  - [民國76年（1987年）7月15日中視新聞快報](http://www.youtube.com/watch?v=5X1g8SpkFNY&feature=related)
  - [中視開播，包含檢驗圖(該檢驗圖是華視的檢驗圖)、中華民國國歌、中視台呼，與兩張政令宣導字卡，隨即進入《中國早安》節目](https://www.youtube.com/watch?v=dfr-a0dFtX8)

[Category:電視技術](../Category/電視技術.md "wikilink")
[Category:台灣電視](../Category/台灣電視.md "wikilink")

1.  例如：中視[1987年7月14日新聞快報字卡](http://www.youtube.com/watch?v=5X1g8SpkFNY)只有一張字卡；[〈田邊製藥廣告集錦〉](https://www.youtube.com/watch?v=hOLdD2WV7_Y)這段影片中，「[田邊綜合](../Page/田邊製藥.md "wikilink")-{[維他命](../Page/維他命.md "wikilink")}-」的廣告，就是用三張字卡組成的一則廣告。