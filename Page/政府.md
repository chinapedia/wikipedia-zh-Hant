**政府**是一个[政治体系](../Page/政治.md "wikilink")，於某个區域訂立、執行[法律和](../Page/法律.md "wikilink")[管理的一套](../Page/管理.md "wikilink")[机构](../Page/机构.md "wikilink")。广义的政府包括[立法机关](../Page/立法机关.md "wikilink")、[行政机关](../Page/行政机关.md "wikilink")、[司法机关](../Page/司法机关.md "wikilink")、[军事机关](../Page/军事.md "wikilink")，狭义的政府仅指行政机关；在[内阁制国家](../Page/内阁制.md "wikilink")，“政府”一词也用來指代表国家最高行政机构的核心，即“[内阁](../Page/内阁.md "wikilink")”。

政府也是一种[权力分配的格局](../Page/权力.md "wikilink")。不仅是[司法权](../Page/司法权.md "wikilink")、[立法权和](../Page/立法权.md "wikilink")[行政权之间的关系](../Page/行政权.md "wikilink")，也是中央政府与地方政府之间的关系，甚至也包括了政府各部门单位之间以及[公务员之间的权力分配](../Page/公务员.md "wikilink")。

政府是组成国家整体的一个幫派，政府隶属于国家。因此执政政府的倒台或[政权的更替并不意味着国家的灭亡](../Page/政权.md "wikilink")，而如果国家灭亡了，政府也就不可能存在。公民对于国家的[合法性存在认同性高于对政府的认同性](../Page/合法性.md "wikilink")。

政府也是一种[组织](../Page/组织.md "wikilink")，区别于一般的社会组织，政府权力的取得必须具有合法性，同时具有强制性和权威性。权力只为权力来源负责，政府也不例外。如果政府的权力来源恰好是国民或公民，政府就对国民公民负责，为公共利益服务。

政府依照法律行使执法权，如果超出法律赋予的权限范围，就是“滥用职权”；如果没有完全行使执法权，就是“不作为”。两者都是政府的错误。

## 概述

[GuGongRichyLi.jpg](https://zh.wikipedia.org/wiki/File:GuGongRichyLi.jpg "fig:GuGongRichyLi.jpg")為500多年間中國政府所在\]\]
[Nagoya_City_Hall_2011-10-28.jpg](https://zh.wikipedia.org/wiki/File:Nagoya_City_Hall_2011-10-28.jpg "fig:Nagoya_City_Hall_2011-10-28.jpg")市政府\]\]
[Palacio_presidencial_de_Haiti.jpg](https://zh.wikipedia.org/wiki/File:Palacio_presidencial_de_Haiti.jpg "fig:Palacio_presidencial_de_Haiti.jpg")總統府\]\]
[国家都有自己的政府](../Page/国家.md "wikilink")。政府是[税收的主体](../Page/税收.md "wikilink")，可以实现[福利的合理利用](../Page/公民福利.md "wikilink")。政府的服務對象依其國家體制而不同。在[民主國家中](../Page/民主.md "wikilink")，政府服務的對象是[人民](../Page/人民.md "wikilink")，政府是由人民委任，代為管理國家的機構；在[專制國家中](../Page/專制.md "wikilink")，政府服務的對象是[统治阶级](../Page/统治阶级.md "wikilink")，協助统治阶级維持统治。

在[分權制度中](../Page/分權.md "wikilink")，狭义的政府只是[行政權的代表](../Page/行政.md "wikilink")，受[立法機關訂立的法律約束](../Page/立法.md "wikilink")，且需執行[司法機關的判決](../Page/司法.md "wikilink")。也有些政府受到权利机关授权进行[行政行为](../Page/行政行为.md "wikilink")，其权力机关通常为通过选举或名义上的选举产生，本身拥有立法权、司法解释权（或立法解释权）、对政府的审核信任权等，对部分重大影响或干系的事务，进行权利干涉，如干涉司法部门判决、干涉行政部门施政等，本身利用选举使之可以凌驾于其他[常务机构而体现民意和实现民主](../Page/常务机构.md "wikilink")。

在专制国家中，政府使用不同的方法维持统治，包括[警察和](../Page/警察.md "wikilink")[军队](../Page/军队.md "wikilink")（参看[警察国家](../Page/警察国家.md "wikilink")），与其他国家签订协议，赢取国家内部的支持。典型的赢取支持的方法包括提供[司法体系](../Page/司法体系.md "wikilink")、[管理机构和](../Page/管理机构.md "wikilink")[社会福利](../Page/社会福利.md "wikilink")，声明国家受到[神的支持](../Page/神.md "wikilink")，为大多数群体提供利益，在国内的重要部门举行[选举](../Page/选举.md "wikilink")，通过[法律和](../Page/法律.md "wikilink")[宪法限制国家权力以及使用](../Page/宪法.md "wikilink")[民族主义](../Page/民族主义.md "wikilink")。反对政府控制的群体包括[自由意志主义者和](../Page/自由意志主义.md "wikilink")[无政府主义者](../Page/无政府主义者.md "wikilink")。

## 政府組成

现代政府的构成原理主要是[人民主权原则和](../Page/人民主权.md "wikilink")[代议制原则](../Page/代议制.md "wikilink")。政府体系由[国家元首](../Page/国家元首.md "wikilink")、[行政机构](../Page/行政机构.md "wikilink")、[立法机构和](../Page/立法机构.md "wikilink")[司法机构组成](../Page/司法机构.md "wikilink")。它们之间的力量对比决定了一个政府的组织类型。主要有：

  - [三权分立结构](../Page/三权分立.md "wikilink")：国家的立法权、司法权和行政权分别由[议会](../Page/议会.md "wikilink")、[法院和](../Page/法院.md "wikilink")[总统分别行使](../Page/总统.md "wikilink")，相互制衡。[美国是一个典型的三权分立国家](../Page/美国.md "wikilink")。
  - [五权分立结构](../Page/五权分立.md "wikilink")：國家除有原先的立法权、司法权和行政权，另增加考試權及監察權，五權個別行使職權，相互制衡。首創五權分立之五權憲法為[中華民國國父](../Page/中華民國國父.md "wikilink")[孫中山先生](../Page/孫中山.md "wikilink")。[中華民國是典型的五權分立的國家](../Page/中華民國.md "wikilink")。
  - [议会制结构](../Page/议会制.md "wikilink")：行使立法权的议会是最高权力机构，内阁和法院都由议会产生，并对其负责。起源于[英国](../Page/英国.md "wikilink")，为大多数[英联邦国家采用](../Page/英联邦.md "wikilink")，[瑞典](../Page/瑞典.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[德国和](../Page/德国.md "wikilink")[意大利也采用这种结构](../Page/意大利.md "wikilink")。
  - 元首集权结构：[国家元首的权力凌驾于其他政府机构之上](../Page/国家元首.md "wikilink")，而立法、司法和行政机构则在国家元首之下保持平衡。例如[法国的](../Page/法国.md "wikilink")[半总统制](../Page/半总统制.md "wikilink")、[中華民國的](../Page/中華民國.md "wikilink")[五院制和](../Page/五權分立.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")。

为了便于治理，[中央政府由不同的](../Page/中央政府.md "wikilink")[政府机关组成](../Page/政府机关.md "wikilink")。这包括了政府部门之间的各级机关以及依据地理区域划分的行政区域的各级政府。[中央政府与](../Page/中央政府.md "wikilink")[地方政府之间的权力关系可以分为](../Page/地方政府.md "wikilink")\[1\]：

  - [单一制](../Page/单一制.md "wikilink")：中央政府掌握所有的权力，地方政府的权力只是中央政府为了管理需要而授予的，中央政府具有最终决定权。
  - [联邦制](../Page/联邦制.md "wikilink")：中央政府的权力来自于地方政府的让予，地方政府的权力是固有的。[联邦政府无权干涉联邦成员政府独立的职权范围](../Page/联邦政府.md "wikilink")。

一个[发达国家的政府通常包括诸如办公室](../Page/发达国家.md "wikilink")、部（省）、局、处、科（课）等的机构，其中的负责人被称为“部长”或“秘书”。部长或大臣在理论上是[国家元首的顾问](../Page/国家元首.md "wikilink")，但是在实际中，在特定领域会有一些实权。在很多现代[民主国家](../Page/民主.md "wikilink")，虽然国家元首有很大的权力指定政府，但是由选举产生的[立法机构也有权解散政府](../Page/立法机构.md "wikilink")。

## 政體分類

[古希臘](../Page/古希臘.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[柏拉圖提出了](../Page/柏拉圖.md "wikilink")，分為\[2\]：

  - [贵族政治](../Page/贵族政治.md "wikilink")：統治權由某個階級中的少數人共有，以公利為出發點來統治。
  - [荣誉至上政治](../Page/荣誉至上政治.md "wikilink")（Timocracy）：由荣誉为原则所进行的统治。
  - [寡頭政治](../Page/寡頭政治.md "wikilink")：統治權由某個階級中的少數人共有，統治階級自身利益為出發。
  - [民主政治](../Page/民主.md "wikilink")
  - [暴君政治](../Page/僭主.md "wikilink")：不通過世襲、傳統或是合法民主選舉程序，憑藉個人的聲望與影響力，獲得權力所進行的統治。

以下還有其他的分類

### 以執政者身份的分類

#### 貴族精英（Aristarchic）類

  - [贵族政治](../Page/贵族政治.md "wikilink")

  -
  -
  - [精英政治](../Page/精英政治.md "wikilink")

  -
  - [专家统治](../Page/专家统治.md "wikilink")

#### 獨裁類

  - [獨裁](../Page/獨裁.md "wikilink")
  - [专制主义](../Page/专制主义.md "wikilink")
  - [獨裁政體](../Page/獨裁政體.md "wikilink")
  - [法西斯](../Page/法西斯.md "wikilink")

#### 君主類

  - [君主專制](../Page/君主專制.md "wikilink")
  - [君主立宪制](../Page/君主立宪制.md "wikilink")
  - [二头政治](../Page/二头政治.md "wikilink")
  - [選舉君主制](../Page/選舉君主制.md "wikilink")
  - [酋長國](../Page/酋長國.md "wikilink")
  - [聯邦君主制](../Page/聯邦君主制.md "wikilink")
  - [君主制](../Page/君主制.md "wikilink")

#### 退化（Pejorative）類

  - [銀行專政](../Page/銀行專政.md "wikilink")
  - [公司王国](../Page/公司王国.md "wikilink")
  - [裙帶關係](../Page/裙帶關係.md "wikilink")
  - [竊盜統治](../Page/竊盜統治.md "wikilink")
  - [惡人政治](../Page/惡人政治.md "wikilink")（Kakistocracy）
  - [眾愚政治](../Page/眾愚政治.md "wikilink")

### 以執政者選舉方式的分類

#### 威權類

  - [威权主义](../Page/威权主义.md "wikilink")
  - [極權主義](../Page/極權主義.md "wikilink")

#### 民主類

  -
  - [民主](../Page/民主.md "wikilink")

  - [直接民主制](../Page/直接民主制.md "wikilink")

  - [自由民主制](../Page/自由民主制.md "wikilink")

  - [代議民主制](../Page/代議民主制.md "wikilink")

  - [社會民主主義](../Page/社會民主主義.md "wikilink")

  - [极权民主](../Page/极权民主.md "wikilink")

#### 寡頭政治類

  -
  - [判官统治](../Page/判官统治.md "wikilink")

  -
  - [寡头政治](../Page/寡头政治.md "wikilink")

  - [金權政治](../Page/金權政治.md "wikilink")

  - [軍政府](../Page/軍政府.md "wikilink")

  - [神權政治](../Page/神權政治.md "wikilink")

#### 其他

  - [無政府狀態](../Page/無政府狀態.md "wikilink")
  - [半民主狀態](../Page/半民主狀態.md "wikilink")
  - [香蕉共和國](../Page/香蕉共和國.md "wikilink")
  - [毛澤東思想](../Page/毛澤東思想.md "wikilink")

### 以權力分配方式的分類

#### 共和類

  - [共和制](../Page/共和制.md "wikilink")

  - [共和立憲制](../Page/共和立憲制.md "wikilink")

  -
  - [议会共和制](../Page/议会共和制.md "wikilink")

  - [聯邦共和制](../Page/聯邦共和制.md "wikilink")

  - [伊斯兰共和国](../Page/伊斯兰共和国.md "wikilink")

  - [社會主義國家](../Page/社會主義國家.md "wikilink")

#### 聯邦類

  - [联邦主义](../Page/联邦主义.md "wikilink")
  - [联邦君主制](../Page/联邦君主制.md "wikilink")
  - [聯邦共和制](../Page/聯邦共和制.md "wikilink")

#### 其他權力分配方式

  -
  - [无政府主义](../Page/无政府主义.md "wikilink")

  -
  - [官僚制](../Page/官僚制.md "wikilink")

  - [酋邦](../Page/酋邦.md "wikilink")

  -
  - [議會制](../Page/議會制.md "wikilink")

  - [总统制](../Page/总统制.md "wikilink")

  - [法治](../Page/法治.md "wikilink")

## 政府职能

[RIGHT](https://zh.wikipedia.org/wiki/File:Departure_Herald-Detail.jpg "fig:RIGHT")
[Rajavartijoita_passintarkastuksessa.jpg](https://zh.wikipedia.org/wiki/File:Rajavartijoita_passintarkastuksessa.jpg "fig:Rajavartijoita_passintarkastuksessa.jpg")
[NYPD-Motorcycles.jpg](https://zh.wikipedia.org/wiki/File:NYPD-Motorcycles.jpg "fig:NYPD-Motorcycles.jpg")
[Central_Bank_of_China_(0176).JPG](https://zh.wikipedia.org/wiki/File:Central_Bank_of_China_\(0176\).JPG "fig:Central_Bank_of_China_(0176).JPG")
政府作为社会中最大的公共组织，不仅负有价值导向、安全保卫、维持秩序的责任，也肩负了政治统治、经济管理、社会协调的社会寄托。政府具有公共属性，政府的职能是为公众服务，并以公众的利益为依归。同时政府也具有一定的阶级性，它往往为某个特定的阶级服务，成为该阶级管理其他阶级的工具。政府具有价值导向责任，如[亚里士多德所说的追求](../Page/亚里士多德.md "wikilink")“最高而最广的善业。”\[3\]政府职能也涵盖了社会公众生活的全方位，并且随着社会的改变不断适应与发展。政府也同时为公众福利提供方便，创造条件，甚至通过直接提供[公共物品提高公众的生活水平](../Page/公共物品.md "wikilink")。从程序的角度，政府的职能包括了决策、计划、组织、指挥、协调、控制等。决策是政府的核心职能，决策水平的高低反映了政府的能力和水平。

从任务范围角度，政府的职能可以分为政治职能、[经济职能和社会职能](../Page/经济.md "wikilink")。其中政治职能是政府最主要的职能，这主要体现在维护现存的政治秩序上。政府的首要任务是维护社会的正常运转，其次就是指定有利于国家与社会的政策，推动社会的进步。为了保证政府职能顺利发挥作用，[军队](../Page/军队.md "wikilink")、[警察](../Page/警察.md "wikilink")、[监狱等暴力机关对外保卫国家主权](../Page/监狱.md "wikilink")，对内维护政府统治，打击犯罪。政府也需要建立和维护从中央到地方政府的结构系统，并保障工作人员的权益。

在经济方面政府通过财政和货币政策调节经济发展，保证宏观经济的稳定增长。经济低迷时期，通过减免[税收](../Page/税.md "wikilink")、调节[税率](../Page/税率.md "wikilink")、加大支出等手段，促进[投资和](../Page/投资.md "wikilink")[生产](../Page/生产.md "wikilink")；经济过热时，减少政府支出，减少[货币供应量](../Page/货币.md "wikilink")、提高[利率](../Page/利率.md "wikilink")，抑制需求，降低物价。同时政府通过税收和政府支出等手段，对社会财富进行再分配，以保证社会[公平与稳定](../Page/公平.md "wikilink")。为了控制[垄断和外部不经济行为](../Page/垄断.md "wikilink")，政府通过提高资源配置手段來保证企业间的公平竞争。政府还需要提供充分的政策和资金的支持，积极创造条件，促进经济结构的优化和转型，鼓励新兴产业的成长。政府还提供[国防安全](../Page/国防.md "wikilink")、[公共工程](../Page/公共工程.md "wikilink")、[社会福利等公共产品为社会公众服务](../Page/社会福利.md "wikilink")。

政府作为社会公共组织，负有维护社会治安和秩序的职责，减少各种[社会问题](../Page/社会问题.md "wikilink")。牵涉到社会公共利益的事务，例如交通、卫生、教育、文化、环境保护等，也往往是政府的责任。

## 政府与市场

[市场为了保持其平等与开放的特性](../Page/市场.md "wikilink")，促进竞争，对政府的干扰有本能的排斥；另一方面，市场为了维持秩序又需要政府调停。尤其是[市场失灵时](../Page/市场失灵.md "wikilink")，更需要政府的介入。政府对市场的干预包括开展公益服务，发展公共事业，解决[外部性问题](../Page/外部性.md "wikilink")（例如环境污染）；控制[垄断的出现](../Page/垄断.md "wikilink")，维护市场的稳定和机会公平；解决[信息不对称问题等](../Page/信息不对称.md "wikilink")。

政府由于在政策和工作上的低效率，对市场的干预也可能导致[政府失败的发生](../Page/政府失败.md "wikilink")。现实中政府的干预或多或少难免存在失误，关键是如何将政府失败控制在最小限度之内。

政府干预市场的模型按照最小政府到最大政府可以分成[无政府社会](../Page/无政府社会.md "wikilink")、[最低限度国家](../Page/最低限度国家.md "wikilink")（[守夜人国家](../Page/守夜人国家.md "wikilink")）、[混合政治经济国家](../Page/混合政治经济国家.md "wikilink")、[福利国家](../Page/福利国家.md "wikilink")、[全能主义国家](../Page/全能主义国家.md "wikilink")。

无政府社会理论上没有政府干预，完全依靠市场规则，但是实际中的无政府状态常常表现为混乱无序，根本无法建立有效的市场规则。在守夜人国家模式下，政府除了国家安全和维持秩序外，基本不干涉经济。混合政治经济国家则主张在自由市场经济的基础上实行国家适度干预，现代国家大多属于这种类型。福利国家为公民提供详尽的福利政策，政府对经济有很强的控制。全能主义国家则不主张市场经济，政府不仅占有大量经济资源，而且控制其配置，经济完全依附于政治。

## 地圖

[Forms_of_government.svg](https://zh.wikipedia.org/wiki/File:Forms_of_government.svg "fig:Forms_of_government.svg")

<hr>

\]\]
[Electoral_democracies.svg](https://zh.wikipedia.org/wiki/File:Electoral_democracies.svg "fig:Electoral_democracies.svg")2014年世界自由度報告內容，列為[代議民主制的國家](../Page/代議民主制.md "wikilink")\[4\]，世界自由度報告考慮的是民主是否有實際實施，不止是官方聲稱而已\]\]

## 參見

  - [影子政府](../Page/影子政府.md "wikilink")
  - [无政府主义](../Page/无政府主义.md "wikilink")
  - [夕陽政府](../Page/夕陽政府.md "wikilink")
  - [公家](../Page/公家.md "wikilink")
  - [合法性](../Page/合法性.md "wikilink")
  - [邦联制](../Page/邦联制.md "wikilink")
  - [政府干预](../Page/政府干预.md "wikilink")
  - [新公共管理运动](../Page/新公共管理运动.md "wikilink")
  - [中央政府](../Page/中央政府.md "wikilink")
  - [公民教育](../Page/公民教育.md "wikilink")
  - [比較政治學](../Page/比較政治學.md "wikilink")
  - [自然权利](../Page/自然权利.md "wikilink")
  - [各國政體列表](../Page/各國政體列表.md "wikilink")
  - [政治經濟學](../Page/政治經濟學.md "wikilink")
  - [政治](../Page/政治.md "wikilink")
  - [國 (政治)](../Page/國_\(政治\).md "wikilink")
  - [投票制度](../Page/投票制度.md "wikilink")
  - [世界政府](../Page/世界政府.md "wikilink")
  - [行政主導](../Page/行政主導.md "wikilink")
  - [行政一體](../Page/行政一體.md "wikilink")

## 参考文献

<div class="references-small">

<references>

\[5\]

</references>

</div>

## 外部链接

  - 趙鼎新：[〈費納與政府史研究〉](http://www.aisixiang.com/data/44755.html)（2011年）
  - [政府与法治](https://web.archive.org/web/20070610025457/http://elsa.berkeley.edu/users/yqian/government%20and%20law.html)
  - [The Phrontistery Word List: Types of Government and
    Leadership](http://phrontistery.info/govern.html)
  - [What Are the Different Types of
    Governments?](http://www.lifeslittlemysteries.com/1096-what-are-the-different-types-of-governments.html)
  - [Parlamentarisches
    Regierungssystem](https://www.bpb.de/nachschlagen/lexika/politiklexikon/17990/parlamentarisches-regierungssystem)
  - [Types of Governments from Historical Atlas of the 20th
    Century](http://users.erols.com/mwhite28/20c-govt.htm)
  - [Other classifications examples from Historical Atlas of the 20th
    Century](http://users.erols.com/mwhite28/othergov.htm)
  - [World Affairs: Types of
    Government](http://stutzfamily.com/mrstutz/WorldAffairs/typesofgovt.html)
  - [Regime Types](http://www.polisci.ccsu.edu/brown/regime_types.htm)
  - [Typen und Systeme von
    Demokratie](https://demokratie.geschichte-schweiz.ch/typen-systeme.html)
  - [CBBC Newsround: types of
    government](http://news.bbc.co.uk/cbbcnews/hi/find_out/guides/world/united_nations/types_of_government/)
  - [Bill Moyers: Plutocracy
    Rising](http://billmoyers.com/episode/full-show-plutocracy-rising/)
  - [Phobiocracy by Chris
    Claypoole](http://www.ncc-1776.org/tle2003/libe229-20030629-03.html)

{{-}}

[Category:政治组织类型](../Category/政治组织类型.md "wikilink")
[政府](../Category/政府.md "wikilink")
[Category:国家](../Category/国家.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.
2.
3.
4.  [Freedom in The World, 2014 scores table
    (PDF)](http://freedomhouse.org/sites/default/files/FIW%202014%20Scores%20-%20Countries%20and%20Territories.pdf)
5.