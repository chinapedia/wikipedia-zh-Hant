**政教分離**（），指的是**國家政府權力**（凱撒即世俗王權）與**宗教機構權力**（基督即教會）的分割；也可以是說，是指政府不支持或壓迫任何一個宗教機構，任何一個宗教機構也不可以代理政府的職權。

## 定義

政教分離是現代[政治學的一項原則](../Page/政治學.md "wikilink")，其提倡源於[歐洲的](../Page/歐洲.md "wikilink")[宗教改革運動](../Page/宗教改革運動.md "wikilink")。[馬丁·路德](../Page/馬丁·路德.md "wikilink")、[加爾文等人領導的宗教改革首先反省的是基督教的真理](../Page/加爾文.md "wikilink")，並因而帶來對政府（即世俗王權）與教會權力各自邊界的反思，於是，也就有了所謂的政教分離主張，惟實際上，這些從[天主教會中獨立出來的](../Page/天主教會.md "wikilink")[基督新教只是不受](../Page/基督新教.md "wikilink")[教廷控制的](../Page/教廷.md "wikilink")[政教合一國家](../Page/政教合一.md "wikilink")，教會配合日益[專制的](../Page/專制.md "wikilink")[君主權力起了一定的統治作用](../Page/君主制.md "wikilink")，路德和加爾文領導的新教宗派在那些國家取代了[天主教成為了該國的國教](../Page/天主教.md "wikilink")，部分[英國聖公會](../Page/英國聖公會.md "wikilink")、[路德宗和](../Page/路德宗.md "wikilink")[加爾文宗等](../Page/加爾文宗.md "wikilink")。此外，[東正教也繼續是](../Page/東正教.md "wikilink")[俄羅斯帝國的國教](../Page/俄羅斯帝國.md "wikilink")，[伊斯蘭教是](../Page/伊斯蘭教.md "wikilink")[鄂圖曼帝國的國教](../Page/鄂圖曼帝國.md "wikilink")。

從概念上，「政教合一」和「政教分離」中的「政」是指[政府](../Page/政府.md "wikilink")；「教」是指[教會](../Page/教會.md "wikilink")。政府是直接負責管治社會的國家機構。教會是指統管某一宗教信仰信徒的宗教組織。政府不等於「政治」，同樣宗教組織也不等於「宗教」。「政治」廣義是指所有有關社會管理的事宜；狹義是指政府在制訂管治社會的政策的事宜。教會不等於宗教，也不等於宗教領袖和持宗教信仰的人。有些宗教是沒有教會的，也有持宗教信仰人士不屬教會或不代表教會。\[1\]

[美國是第一個在憲法中明確規定](../Page/美國.md "wikilink")[政教分離的](../Page/政教分離.md "wikilink")[世俗](../Page/世俗.md "wikilink")[共和制國家](../Page/共和制.md "wikilink")，在1791年起實施的《[美国宪法第一修正案](../Page/美国宪法第一修正案.md "wikilink")》中明文規定：「國會不得制定關於設立國教或禁止[宗教自由之法律](../Page/宗教自由.md "wikilink")。」\[2\]在1789年[法國大革命後](../Page/法國大革命.md "wikilink")，[法國通過的](../Page/法國.md "wikilink")[法國人權和公民權宣言亦包括](../Page/法國人權和公民權宣言.md "wikilink")[政教分離](../Page/政教分離.md "wikilink")，此後[革命共和政府一度禁止天主教的活動](../Page/法蘭西第一共和國.md "wikilink")，並改行[共和曆](../Page/共和曆.md "wikilink")，後[拿破崙一世恢復天主教的國教地位](../Page/拿破崙一世.md "wikilink")，直至1905年法國才完成實行政教分離，把宗教從公共領域中移出。

現代社會的趨勢是向[世俗主義方向發展](../Page/世俗主義.md "wikilink")，指政府不會承認亦不會設立[國教](../Page/國教.md "wikilink")，教會在政府施政時也不會享有優惠及特權。宗教與政治勢力不互相干涉，政治決定不受宗教勢力所影響。政教分離對保障[宗教自由有一定程度的幫助](../Page/宗教自由.md "wikilink")，確保不同的宗教可以有[信仰自由空間](../Page/信仰自由.md "wikilink")，同時不受政府干涉。但在世界上不少國家，仍然有宗教背景濃厚的政黨，不論是[佛教](../Page/佛教.md "wikilink")、[基督教](../Page/基督教.md "wikilink")、[伊斯蘭教等](../Page/伊斯蘭教.md "wikilink")。宗教組織仍對政府有影響力，不少政治人物也與宗教團體有一定程度上的關係。宗教團體仍有力影響政府，政府亦會對宗教組織提供稅務寬免（例如教會所收取的奉獻金額不用上繳政府），亦會用公帑去資助宗教辦學團體，宗教組織亦繼續提供醫療和教育等[社會福利的服務](../Page/社會福利.md "wikilink")，部分亦得到政府的資助。最先實行政教分離的美國，宗教組織對政治的影響力比其他[歐美](../Page/歐美.md "wikilink")[已發展國家有過之而無不及](../Page/已發展國家.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[韓國等在立國時已實行政教分離的國家](../Page/韓國.md "wikilink")，亦較[歐洲有更大的宗教影響力](../Page/歐洲.md "wikilink")。

## 參見

  - [政教合一](../Page/政教合一.md "wikilink")
  - [世俗主義](../Page/世俗主義.md "wikilink")

## 參考資料

[Category:宗教信仰自由](../Category/宗教信仰自由.md "wikilink")
[Category:宗教學](../Category/宗教學.md "wikilink")
[Category:政體](../Category/政體.md "wikilink")
[Category:政治學](../Category/政治學.md "wikilink")

1.  [宗教干政、政教合一、政教分離的概念謬誤](http://www.inmediahk.net/node/101397)
    2006-03-13
2.  [藝術與建築索引典—政教分離](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055491)
     於2011年4月11日查閱