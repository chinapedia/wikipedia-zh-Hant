**政治人物**（），是指以[政治為](../Page/政治.md "wikilink")[職業](../Page/職業.md "wikilink")，或積極投入[政治活動或公共事務的人](../Page/政治活動.md "wikilink")，無論其動機是私人或[黨派](../Page/政黨.md "wikilink")[利益](../Page/利益.md "wikilink")，還是社會或[國家利益](../Page/國家利益.md "wikilink")。其動機被視為私人或政黨利益者常被批評者稱為「**政客**」，為一種輕蔑和貶低的用詞。在現代[民主](../Page/民主.md "wikilink")[政體中](../Page/政體.md "wikilink")，職業政治人物常通過[組織或加入](../Page/組織.md "wikilink")[政黨](../Page/政黨.md "wikilink")、參與[選舉](../Page/選舉.md "wikilink")，以爭取在[政府中擔任職務](../Page/政府.md "wikilink")\[1\]；這些人在[英文中稱為](../Page/英文.md "wikilink")「politician」，指的是一種[社會角色](../Page/社會.md "wikilink")。[廣義的](../Page/廣義.md "wikilink")「政治人物」還包括其他各種政體的[政府首腦](../Page/政府首腦.md "wikilink")、政黨或[軍事](../Page/武裝力量.md "wikilink")[領袖](../Page/領袖.md "wikilink")，或其他[政權中的主要人物](../Page/政權.md "wikilink")。一般來說，曾從事主要公職的人都會被視為政治人物。

## 政治家或政客

[美國作家克拉克](../Page/美國.md "wikilink")（James Freeman
Clarke）認為:「政客是為了下一次的選舉，政治家卻是為了下一代。」\[2\]換句話說，「[政治家](../Page/政治家.md "wikilink")」的理想[典型](../Page/典型.md "wikilink")，是為了後代子孫的[尊嚴](../Page/尊嚴.md "wikilink")、自主與[幸福著想](../Page/幸福.md "wikilink")，而致力於公共事務；而「政客」的典型則是想盡辦法、用盡手段，只為了保有一己的[權力](../Page/權力.md "wikilink")、地位或[利益](../Page/利益.md "wikilink")。\[3\]從[倫理學的觀點](../Page/倫理學.md "wikilink")，所谓「政治家」，就是以[政治为业而遵守基本](../Page/政治.md "wikilink")[职业道德的人](../Page/职业道德.md "wikilink")；而「政客」則是以政治为主副业，卻不嚴守职业道德的人。\[4\]

[中文裡有](../Page/中文.md "wikilink")[褒意的](../Page/褒意.md "wikilink")「[政治家](../Page/政治家.md "wikilink")」及含有[貶意的](../Page/貶意.md "wikilink")「政客」二詞，分別用以指涉受到尊敬或遭人鄙視的政治人物；[英文中的](../Page/英文.md "wikilink")「statesman」一詞意義近似為「政治家」，帶有尊敬的含義；而「politician」一般譯為「政治人物」或「政客」，前者是中性詞彙，只表示其社會角色身份，不帶有褒貶含義；而後者則帶有一種輕蔑、貶低之含義。能直接對應「政客」的單詞沒有。\[5\]要注意的是，[日文中也有漢字](../Page/日文.md "wikilink")「政治家」，但屬於偏向中性的「政治人物」或英文的「politician」，無褒貶之意，通常指當過[國會議員的](../Page/日本國會.md "wikilink")「政治人物」，與中文不同。若要表示中文「政客」這樣的意思時，日文中通常是「悪徳政治家」或「政治屋」等。若真要指相當於中文的「政治家」時，日文會說是「偉大的政治家」。

## 注腳

<references/>

## 相關條目

  - [政治家](../Page/政治家.md "wikilink")
  - [政治](../Page/政治.md "wikilink")

[政治人物](../Category/政治人物.md "wikilink")

1.  安東尼·唐斯（Anthony Downs）著，《民主的經濟理論》（*An Economic Theory of
    Democracy*），1957年（ISBN 9780060417505）。
2.  [Clarke, James
    Freeman](http://quotationsbook.com/quote/30904/#axzz1M6QG4jqm),
    Quotations Book.
3.  [羅榮光撰，『要做政客還是政治家？』（2010年）](http://www.taiwanus.net/news/news/2010/201009010639511428.htm)
4.  [百志撰，『中國的政治家與政客』（2003年）。](http://epochtimes.com/gb/3/12/22/n434819.htm)

5.  [陳錫蕃撰，『政客與政治家』（2004年）。](http://old.npf.org.tw/PUBLICATION/NS/093/NS-C-093-020.htm)