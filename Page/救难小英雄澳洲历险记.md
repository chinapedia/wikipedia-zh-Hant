《**救難小英雄澳洲歷險記**》（，港譯：神勇敢死隊、中譯：地下拯救者），是[迪士尼於](../Page/迪士尼.md "wikilink")1990年推出的第29部[经典动画长片](../Page/经典动画长片.md "wikilink")，也是第23部经典动画长片[救难小英雄的续集](../Page/救難小英雄_\(迪士尼\).md "wikilink")。

这部电影的票房很差，一个原因是与[小鬼当家的竞争](../Page/小鬼当家.md "wikilink")。由此以后，迪士尼发行续集电影大多采取[录影带首映的形式](../Page/录影带首映.md "wikilink")。

本片中文版VCD已经由[中录德加拉在中国大陆发行](../Page/中录德加拉.md "wikilink")（大陆[影音产品译作](../Page/影音产品.md "wikilink")“地下拯救者”），DVD由[博伟在台湾发行](../Page/博伟.md "wikilink")。

## 配音员

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英语</p></td>
<td><p>国语</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鮑勃·紐哈特.md" title="wikilink">鮑勃·紐哈特</a></p></td>
<td><p><a href="../Page/姜先誠.md" title="wikilink">姜先誠</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/李映淑.md" title="wikilink">李映淑</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/喬治·坎貝爾·斯科特.md" title="wikilink">喬治·坎貝爾·斯科特</a></p></td>
<td><p><a href="../Page/佟紹宗.md" title="wikilink">佟紹宗</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弗兰克·维尔克.md" title="wikilink">法拉克·華爾克</a></p></td>
<td><p>使用原音</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/孫德成.md" title="wikilink">孫德成</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彼得·佛斯.md" title="wikilink">彼得·佛斯</a></p></td>
<td><p><a href="../Page/徐健春.md" title="wikilink">徐健春</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/姜先誠.md" title="wikilink">姜先誠</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/胡立成.md" title="wikilink">胡立成</a></p></td>
</tr>
<tr class="even">
<td><p>卡拉·梅爾</p></td>
<td><p><a href="../Page/李映淑.md" title="wikilink">李映淑</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/孫德成.md" title="wikilink">孫德成</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李直平.md" title="wikilink">李直平</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 首映日期

  - ** [美國](../Page/美國.md "wikilink")**：1990年11月16日
  - ** [巴西](../Page/巴西.md "wikilink")**：1990年12月14日
  - ** [澳洲](../Page/澳洲.md "wikilink")**：1991年6月13日
  - ** [阿根廷](../Page/阿根廷.md "wikilink")**：1991年7月4日
  - ** [英國](../Page/英國.md "wikilink")**：1991年10月11日
  - ** [法國](../Page/法國.md "wikilink")**：1991年11月27日
  - ** [芬蘭](../Page/芬蘭.md "wikilink")**：1991年11月29日
  - ** [瑞典](../Page/瑞典.md "wikilink")**：1991年11月29日
  - ** [西班牙](../Page/西班牙.md "wikilink")**：1991年12月3日
  - ** [德國](../Page/德國.md "wikilink")**：1991年12月5日
  - ** [荷蘭](../Page/荷蘭.md "wikilink")**：1991年12月6日
  - ** [意大利](../Page/意大利.md "wikilink")**：1990年12月8日（首映）
      - 2001年6月1日（重映）
  - ****：1991年7月26日
  - ** [日本](../Page/日本.md "wikilink")**：1996年4月19日

## 參考文獻

## 外部連結

  -
  -
  -
  -
[Category:1990年電影](../Category/1990年電影.md "wikilink")
[Category:奇幻冒險電影](../Category/奇幻冒險電影.md "wikilink")
[Category:迪士尼動畫](../Category/迪士尼動畫.md "wikilink")