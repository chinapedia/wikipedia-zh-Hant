**教堂门**（[马拉地语](../Page/马拉地语.md "wikilink")：चर्चगेट，英语：Churchgate）是[孟买南区的一个区域](../Page/孟买南区.md "wikilink")，以著名的教堂门车站著称。教堂门车站是[孟买郊区铁路](../Page/孟买郊区铁路.md "wikilink")[西部铁路的](../Page/西部铁路.md "wikilink")[终点站](../Page/终点站.md "wikilink")，孟买最南面的一个车站，也是[西部铁路的总部](../Page/西部铁路.md "wikilink")。

## 车站

[Old_Churchgate.jpg](https://zh.wikipedia.org/wiki/File:Old_Churchgate.jpg "fig:Old_Churchgate.jpg")
教堂门车站得名于教堂和门。教堂是指18世纪位于同一条路上、大约500米远处的圣多马教堂。大门是指精确地址位于[Flora
喷泉处的古代大门](../Page/Flora_喷泉.md "wikilink")，在16世纪作为孟买的南部边界。
这座门后来在城市扩展时被拆除。

今天，这座车站是该市最繁忙的车站之一。住在郊区的数百万市民在该站下车前往他们位于孟买南区商业区的工作地点。

这座车站是[西部铁路的终点站](../Page/西部铁路.md "wikilink")。首班列车于上午4:00出发，末班车于凌晨1:00结束。

## 区域

教堂门基本上是一个商业区。许多事务所与银行都位于距离教堂门咫尺之遥的地方。该市的最主要商业区[Nariman
Point距此仅有](../Page/Nariman_Point.md "wikilink")1千米。教堂门的西部是[海滨大道区域](../Page/海滨大道.md "wikilink")。

## 教育机构

教堂门附近有大量教育机构。

1.  [孟买大学](../Page/孟买大学.md "wikilink")
2.  [Jamnalal Bajaj
    管理研究协会](../Page/Jamnalal_Bajaj_管理研究协会.md "wikilink")：印度最好的商业学校之一
3.  [Jai Hind
    学院](../Page/Jai_Hind_学院.md "wikilink")：成立于1948年，[印度分治后由迁移来此的](../Page/印度分治.md "wikilink")[巴基斯坦](../Page/巴基斯坦.md "wikilink")[信德人长老创办](../Page/信德人.md "wikilink")。该学院最初只有一个房间，允许从上午7时到10时使用
4.  [K.C. 学院](../Page/K.C._学院.md "wikilink"): 成立于1954年
5.  [H R 学院](../Page/H_R_学院.md "wikilink"):
6.  [Sydenham
    学院](../Page/Sydenham_学院.md "wikilink")：亚洲最古老的[商学院](../Page/商学院.md "wikilink")，1913年由当时[孟买市长](../Page/孟买.md "wikilink")[Lord
    Sydenham创办](../Page/Lord_Sydenham.md "wikilink")。
7.  [政府法学院](../Page/政府法学院.md "wikilink")
8.  [孟加拉女子中学](../Page/孟加拉女子中学.md "wikilink")
9.  [SNDT 女子大学](../Page/SNDT_女子大学.md "wikilink")
10. [Nirmala Niketan](../Page/Nirmala_Niketan.md "wikilink")
11. [Indo-German 培训中心
    (IGTC)](../Page/Indo-German_培训中心_\(IGTC\).md "wikilink"):

## 体育设施

1.  [Wankhede Cricket Stadium](../Page/Wankhede_Stadium.md "wikilink")
2.  Mahindra Hockey Stadium
3.  [Brabourne Stadium](../Page/Brabourne_Stadium.md "wikilink")
4.  Oval Maidan

[Category:孟买地理](../Category/孟买地理.md "wikilink")