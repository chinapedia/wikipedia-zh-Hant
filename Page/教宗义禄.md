教宗[聖](../Page/聖人.md "wikilink")**義祿**（，，原名**Eleutherius**，於171年或177年—185年或193年出任教宗），出生於[希臘北部](../Page/希臘.md "wikilink")[伊庇鲁斯地區的](../Page/伊庇鲁斯.md "wikilink")。

在《[歷任教宗名錄](../Page/歷任教宗名錄.md "wikilink")》一書中聲稱教宗義祿曾與想改信[基督教的](../Page/基督教.md "wikilink")[不列顛](../Page/不列顛.md "wikilink")[國王](../Page/國王.md "wikilink")[路魯斯](../Page/路魯斯.md "wikilink")
*Lucius*通信，然而，沒有其他證據支持這說法。

他可能是[殉教者](../Page/殉教者.md "wikilink")，他的聖日是5月26日。

## 譯名列表

  - 義祿：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作义禄。

[E](../Category/教宗.md "wikilink") [E](../Category/基督教聖人.md "wikilink")
[E](../Category/希臘出生的教宗.md "wikilink")