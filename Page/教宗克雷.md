教宗[聖](../Page/聖人.md "wikilink")**克雷**（，）是第3任[教宗](../Page/教宗.md "wikilink")，於76年9月23日-88年4月26日岀任教宗。

教宗克雷在[羅馬帝國](../Page/羅馬帝國.md "wikilink")[羅馬出生](../Page/羅馬.md "wikilink")，在88年於出生地逝世。

教宗克雷的名字是[希臘語](../Page/希臘語.md "wikilink")，「Cletus」解作「召喚」；「Anacletus」解作「召回來」。

## 譯名列表

  - 克雷：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作克雷。
  - 格肋多：[天主教广西教会](http://www.gxjn.com.cn/ShowNews.asp?ID=518&CataID=51)，[天主教辽宁教区](http://www.lncatholic.org/Article.asp?action=article_shengren&aid=1557&search=search_anname&name=)作格肋多。

## 注釋

## 外部連結

  - [關於聖克雷的文檔](http://www.documentacatholicaomnia.eu/01_01_0076-0088-_Anacletus_I,_Sanctus.html)

[C](../Category/義大利出生的教宗.md "wikilink")
[Category:罗马人](../Category/罗马人.md "wikilink")
[Category:基督教圣人](../Category/基督教圣人.md "wikilink")