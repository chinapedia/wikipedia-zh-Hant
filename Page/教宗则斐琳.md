[教宗](../Page/教宗.md "wikilink")[聖](../Page/聖人.md "wikilink")**則斐琳**（，，原名**Zephyrinus**）於199年—217年12月20日出任教宗，出生於[羅馬](../Page/羅馬.md "wikilink")。

教宗則斐琳的主要顧問[教宗嘉禮一世接任成為教宗](../Page/教宗嘉禮一世.md "wikilink")。

他的聖日是8月26日。

## 譯名列表

  - 則斐琳：[梵蒂岡廣播電台](https://web.archive.org/web/20040906201343/http://www.radiovaticana.org/cinesegb/santuari/34catacombe.html)作則斐琳。
  - 才斐：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作才斐。

[Z](../Category/教宗.md "wikilink")
[Z](../Category/義大利出生的教宗.md "wikilink")
[Z](../Category/羅馬人.md "wikilink")
[Z](../Category/基督教聖人.md "wikilink")