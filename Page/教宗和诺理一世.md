教宗**何諾一世**（，）是[天主教](../Page/天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")，於625年10月27日到638年10月12日在位。

据说天主教里的[耶稣的升架节是他引入的](../Page/耶稣的升架节.md "wikilink")，此外他修建、更新和美化了多个[罗马市内的教堂](../Page/罗马市.md "wikilink")。他在传教工作中也很有成绩，比如在[英格兰](../Page/英格兰.md "wikilink")。此外他还有效地整顿了[西西里岛上的教士的纪律](../Page/西西里岛.md "wikilink")。

一些人認爲，教宗何諾一世支持[拜占庭皇帝](../Page/拜占庭.md "wikilink")[赫拉克留的神学论点](../Page/赫拉克留.md "wikilink")，企图将[基督一志論与天主教的神学论点融合到一起](../Page/基督一志論.md "wikilink")。因此680年召开的[第三次君士坦丁堡公會議革除了他的教籍](../Page/第三次君士坦丁堡公會議.md "wikilink")，宣布他为支持異端者。后来多位教宗，包括[教宗利奧二世肯定了这个措施](../Page/教宗良二世.md "wikilink")。在1870年的[梵蒂冈第一届大公会议上这个事件被用来作为反对](../Page/第一次梵蒂岡大公會議.md "wikilink")[教宗不能错误性的实例](../Page/教宗無謬誤.md "wikilink")。但就實情看，他從未教導異端，亦未墮入異端，只是在教會需要他履行鞏固信仰的職責時，沒有意識到異端的威脅，並主張與一志論者妥協。

## 譯名列表

  - 和诺理一世：[梵蒂冈广播电台](https://web.archive.org/web/20040330193828/http://www.radiovaticana.org/cinesegb/santuari/39catacombe.html)作和诺理。
  - 何諾一世、何诺一世：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作何諾。

<!-- end list -->

  - 洪諾留一世：[大英簡明百科](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=honorius&mode=1)、[大英綫上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00028770.html)作洪諾留。
  - 洪诺留一世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作洪诺留。
  - 霍諾里烏斯一世：[中国大百科智慧藏　外國人名譯名對照表](https://archive.is/20050112031137/http://140.128.103.1/web/Content.asp?History=C2&Query=9)作霍諾里烏斯。
  - 霍诺里乌斯一世：[1](http://prologue.orthodox.cn/terms/name.htm)

[Honorius 1](../Category/教宗.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")
[Category:坎帕尼亚大区人](../Category/坎帕尼亚大区人.md "wikilink")