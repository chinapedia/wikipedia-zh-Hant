教宗聖**恩仁一世**（，），於654年8月10日至657年6月2日為[教宗](../Page/教宗.md "wikilink")。

## 譯名列表

  - 安日纳一世：[梵蒂冈广播电台](https://web.archive.org/web/20070713215619/http://www.radiovaticana.org/cinesebig5/notiziari/notiz2004/notiz04-03/not03-08.html)作安日纳。
  - 恩仁一世：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作恩仁。
  - 尤金一世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=eugenius&mode=3)、[國立編譯舘](http://www.nict.gov.tw/tc/dic/search.php?p=2&ps=20&w1=eugene&w2=&w3=&c1=&c2=c2&l=&u=&pr=&r=&o=1&pri=undefined)作尤金。
  - 犹金一世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作犹金。

[E](../Category/教宗.md "wikilink") [E](../Category/657年逝世.md "wikilink")
[E](../Category/基督教聖人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")
[Category:羅馬人](../Category/羅馬人.md "wikilink")