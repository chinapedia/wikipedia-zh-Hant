教宗**德吾二世**（，），原名不詳，於672年至676年為[教宗](../Page/教宗.md "wikilink")。

## 譯名列表

  - 德吾：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作德吾。
  - [阿德奧達圖斯](../Page/阿德奧達圖斯.md "wikilink")、[阿德奧达图斯](../Page/阿德奧达图斯.md "wikilink")：[大英綫上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00028770.html)作阿德奧達圖斯。
  - [阿德奥达托](../Page/阿德奥达托.md "wikilink")：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作阿德奥达托。

[A](../Category/教宗.md "wikilink") [A](../Category/羅馬人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")