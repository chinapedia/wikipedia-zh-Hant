[教宗](../Page/教宗.md "wikilink")**本篤十四世**\[1\]（，），原名**Prospero Lorenzo
Lambertini**，1740年8月17日－1758年5月3日在位，[意大利人](../Page/意大利.md "wikilink")。

## 譯名列表

  - 本篤十四世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)、[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)、[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=benedict&mode=3)作本篤。
  - 本尼狄克十四世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作本尼狄克。

## 注释

[B](../Category/教宗.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  教宗[本笃十世现在被看作一位](../Page/本笃十世_\(对立教宗\).md "wikilink")[对立教宗](../Page/对立教宗.md "wikilink")，但当时天主教会并没有官方认定此事，因此严格说来，教宗本笃十一世至本笃十六世，实际是第10到第15位用此名号的正统教宗。