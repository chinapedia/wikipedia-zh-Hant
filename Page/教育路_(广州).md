**教育路**是[中國](../Page/中國.md "wikilink")[廣州市](../Page/廣州市.md "wikilink")[越秀區的一條南北走向的](../Page/越秀區.md "wikilink")[道路](../Page/道路.md "wikilink")，位於[吉祥路以南](../Page/吉祥路.md "wikilink")，[惠福東路以北](../Page/惠福東路.md "wikilink")。全長522米，寬15米。

## 歷史

[教育路何氏书院旧址.jpg](https://zh.wikipedia.org/wiki/File:教育路何氏书院旧址.jpg "fig:教育路何氏书院旧址.jpg")\]\]
自[明朝以来](../Page/明朝.md "wikilink")，教育路一帶一直是[廣東最高級文教官署的所在地](../Page/廣東.md "wikilink")，有深厚的文化積澱。[明代提學道署](../Page/明代.md "wikilink")、[清代學政署](../Page/清代.md "wikilink")、[民國時代的教育委員會及現今](../Page/民國.md "wikilink")[廣州市教育局](../Page/廣州市教育局.md "wikilink")，均在教育路[藥洲一帶](../Page/藥洲.md "wikilink")。

1932年，市政府擴建觀蓮街，並穿學署地入[書坊街建成教育路](../Page/書坊街.md "wikilink")。由於[民國時學署為](../Page/民國.md "wikilink")[廣州教育會](../Page/廣州.md "wikilink")，故名**教育路**。[文化大革命時期](../Page/文化大革命.md "wikilink")，曾被命名為“**教育南路**”，1982年復名教育路。

## 特色

教育路接近[北京路步行街](../Page/北京路步行街.md "wikilink")，全路段有較多專營[西服](../Page/西服.md "wikilink")、[皮鞋](../Page/皮鞋.md "wikilink")、[領帶商舖](../Page/領帶.md "wikilink")。

每年春節的[西湖路花市](../Page/西湖路花市.md "wikilink")，是由[西湖路及教育路全程組成](../Page/西湖路_\(广州\).md "wikilink")，亦是廣州市最出名，人流最旺的[迎春花市](../Page/广州花市.md "wikilink")。

## 附近设施

  - [藥洲遺址](../Page/藥洲遺址.md "wikilink")
  - [南方戲院](../Page/南方剧院.md "wikilink")
  - [广州市教育局](../Page/广州市教育局.md "wikilink")

## 與之交匯道路

道路顺序由北往南排列

  - [中山五路](../Page/中山五路.md "wikilink")
  - [西湖路](../Page/西湖路_\(广州\).md "wikilink")
  - [惠福東路](../Page/惠福東路.md "wikilink")

## 交通

地鐵

  - [廣州地鐵一](../Page/廣州地鐵.md "wikilink")、二號綫[公園前站](../Page/公園前站.md "wikilink")

## 參見

  - [西湖路](../Page/西湖路_\(广州\).md "wikilink")
  - [北京路步行街](../Page/北京路步行街.md "wikilink")

[J教](../Category/广州街道.md "wikilink") [J教](../Category/越秀区.md "wikilink")