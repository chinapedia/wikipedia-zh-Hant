**敦達古魯翼龍**（*Tendaguripterus*）屬於[翼龍目](../Page/翼龍目.md "wikilink")[翼手龍亞目](../Page/翼手龍亞目.md "wikilink")[準噶爾翼龍超科](../Page/準噶爾翼龍超科.md "wikilink")，生活於[侏儸紀晚期的](../Page/侏儸紀.md "wikilink")[啟莫里階到](../Page/啟莫里階.md "wikilink")[提通階](../Page/提通階.md "wikilink")。化石發現於[坦尚尼亞的](../Page/坦尚尼亞.md "wikilink")[姆特瓦拉區](../Page/姆特瓦拉區.md "wikilink")。

在1909年到1913年之間，一個[德國挖掘團隊前往](../Page/德國.md "wikilink")[德屬東非挖掘化石](../Page/德屬東非.md "wikilink")，Hans
Reck發現一些翼龍類化石\[1\]。在1999年，[大衛·安文](../Page/大衛·安文.md "wikilink")（David
Unwin）、Wolf-Dieter
Heinrich將這些翼龍類化石命名為新屬，[模式種是](../Page/模式種.md "wikilink")**雷氏敦達古魯翼龍**（*T.
recki*）。屬名意為「敦達古魯的翼」；種名則是以發現化石的Hans Reck為名。

[正模標本](../Page/正模標本.md "wikilink")（編號MB.R.1290）包括一部分帶有牙齒的下頜（下頜骨頭的聯合部位）。牙齒位於頜部關節的後方。總體來說，敦達古魯翼龍是種小型的翼龍，頭骨估計長度為20公分，翼展約為1公尺。敦達古魯翼龍的化石也是在[敦達古魯第一次發現的翼龍類頭骨化石](../Page/敦達古魯.md "wikilink")\[2\]。敦達古魯翼龍被認為屬於準噶爾翼龍超科的未定位屬\[3\]\[4\]。有研究根據齒槽的形狀，認為敦達古魯翼龍以[螃蟹或其他](../Page/螃蟹.md "wikilink")[貝類為食](../Page/貝類.md "wikilink")\[5\]。

在2007年，[亞歷山大·克爾納](../Page/亞歷山大·克爾納.md "wikilink")（Alexander
Kellner）提出敦達古魯翼龍的外表類似[德國翼龍](../Page/德國翼龍.md "wikilink")、[準噶爾翼龍](../Page/準噶爾翼龍.md "wikilink")，但無法確定敦達古魯翼龍是否屬於[翼手龍亞目或使基礎型](../Page/翼手龍亞目.md "wikilink")[翼龍類](../Page/翼龍類.md "wikilink")。克爾納建立[敦達古魯翼龍科](../Page/敦達古魯翼龍科.md "wikilink")（Tendaguripteridae）以包含此屬，但沒有建立[演化支定義](../Page/演化支.md "wikilink")\[6\]。

## 參考資料

## 外部連結

  - [*Tendaguripterus*](https://web.archive.org/web/20070930153959/http://www.pterosaur.co.uk/species/UJP/german/Tendaguripterus.htm)
    in The Pterosaur Database
  - [*Tendaguripterus*](https://web.archive.org/web/20070923154540/http://archosauria.org/pterosauria/taxonomy/genera/tendaguripterus.html)
    in The Pterosauria

[Category:準噶爾翼龍超科](../Category/準噶爾翼龍超科.md "wikilink")
[Category:侏羅紀翼龍類](../Category/侏羅紀翼龍類.md "wikilink")

1.  Reck, H. (1931), "Die deutschostafrikanischen Flugsaurier",
    *Centralblatt für Mineralogie und Paläontologie* B 7: 321-336
2.
3.
4.
5.
6.  Kellner A.W.A., Mello A.M.S. & Ford T. (2007). "A survey of
    pterosaurs from Africa with the description of a new specimen from
    Morocco". in: Carvalho I.S. et al. (eds.). *Paleontologia: Cenários
    da Vida*, Vol. 1. Interciência, p. 257-267