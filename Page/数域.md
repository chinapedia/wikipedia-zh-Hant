**数域**是[近世](../Page/抽象代数.md "wikilink")[代数学中常见的概念](../Page/代数学.md "wikilink")，指对[加](../Page/加法.md "wikilink")[减](../Page/减法.md "wikilink")[乘](../Page/乘法.md "wikilink")[除](../Page/除法.md "wikilink")[四则运算封闭的](../Page/四则运算.md "wikilink")[代数系统](../Page/代数系统.md "wikilink")。通常定义的数域是指[复数域](../Page/复数.md "wikilink")\(\mathbb{C}\)的[子域](../Page/域.md "wikilink")。“数域”一词有时也被用作[代数数域的简称](../Page/代数数域.md "wikilink")，但两者的定义有细微的差别。

## 定义

设\(\mathcal{P}\)是复数域\(\mathbb{C}\)的子集。若\(\mathcal{P}\)中包含0与1，并且\(\mathcal{P}\)中任两个数的和、差、乘积以及商（约定[除数不为](../Page/带余除法.md "wikilink")0）都仍在\(\mathcal{P}\)中，就称\(\mathcal{P}\)为一个数域。用[域论的话语来说](../Page/域论.md "wikilink")，复数域的[子域是为数域](../Page/域.md "wikilink")。

任何数域都包括[有理数域](../Page/有理数.md "wikilink")\(\mathbb{Q}\)，但并不一定是\(\mathbb{Q}\)的[有限扩张](../Page/有限扩张.md "wikilink")，因此数域不一定是代数数域。例如实数域\(\mathbb{R}\)和复数域\(\mathbb{C}\)都不是代数数域。反之，每个代数数域都同构于某个数域。

## 例子

除了常见的实数域\(\mathbb{R}\)和复数域\(\mathbb{C}\)以外，通过在有理数域中添加特定的无理数进行扩张得到的扩域也是数域。例如所有形同：

\[a + b\sqrt{2}, \; \; a, b \in \mathbb{Q}\]
的数的集合，就是一个数域。可以验证，任何两个这样的数，它们的和、差、乘积以及商（约定[除数不为](../Page/带余除法.md "wikilink")0）都能写成\(a+b\sqrt{2}\)的形式，故仍然在集合之中。这个集合记作\(\mathbb{Q}(\sqrt{2})\)，是有理数域\(\mathbb{Q}\)的[二次扩域](../Page/二次域.md "wikilink")。

### 可构造数

[可构造数也叫规矩数](../Page/规矩数.md "wikilink")，指的是从给定的单位长度开始，能够通过有限次标准的[尺规作图步骤做出的长度数值](../Page/尺规作图.md "wikilink")。所有可构造数的集合记为\(\mathcal{C}\)，是一个数域。因为给定了两个已经做出的[线段后](../Page/线段.md "wikilink")，可以通过符合尺规作图规定的手段，在有限步内作出长度为两者长度之和、差、乘积以及商的线段。\(\mathcal{C}\)是\(\mathbb{Q}\)的扩域，次数为无限大，是实数域\(\mathbb{R}\)的子域。

### 代数数

[代数数指能够成为某个有理系数](../Page/代数数.md "wikilink")[多项式的根的数](../Page/多项式.md "wikilink")。所有代数数的集合记作\(\mathcal{A}\)，是一个数域。\(\mathcal{A}\)也常被称为代数数域，但与定义为“\(\mathbb{Q}\)的有限扩张”的代数数域是不同的概念。不过，每个\(\mathbb{Q}\)的有限扩张生成的域都可看作是\[1\]\(\mathbb{Q}\)中加入某个代数数扩成的，所以都是\(\mathcal{A}\)的子域。可构造数构成的数域\(\mathcal{C}\)也是\(\mathcal{A}\)的子域。由于[虚数单位](../Page/虚数单位.md "wikilink")也是代数数，所以\(\mathcal{A}\)不是\(\mathbb{R}\)的子域。另一方面，[自然对数的底](../Page/自然对数的底.md "wikilink")以及[圆周率](../Page/圆周率.md "wikilink")都不是代数数，所以\(\mathbb{R}\)也不是\(\mathcal{A}\)的子域\[2\]。

## 注释

## 参考来源

[Category:代数结构](../Category/代数结构.md "wikilink")
[Category:抽象代数](../Category/抽象代数.md "wikilink")

1.  在[同构意义上](../Page/同构.md "wikilink")。
2.  事实上\(\mathcal{A}\)的元素个数是[可数的](../Page/可数集.md "wikilink")，所以元素个数不可数的\(\mathbb{R}\)不可能是\(\mathcal{A}\)的子域。