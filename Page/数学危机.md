**數學危機**在歷史上發生過三次，每一次均對數學的發展有重大影響。在[第一次數學危機中](../Page/第一次數學危機.md "wikilink")，因為發現腰長為1的等腰直角三角形的斜邊長度無法寫成[有理數](../Page/有理數.md "wikilink")，從而引申出日後的[無理數概念](../Page/無理數.md "wikilink")。[第二次數學危機得解決](../Page/第二次數學危機.md "wikilink")[微積分引入](../Page/微積分.md "wikilink")[無窮小量而產生的問題](../Page/無窮小量.md "wikilink")。[第三次數學危機則是因](../Page/第三次數學危機.md "wikilink")[羅素悖論而起](../Page/羅素悖論.md "wikilink")，它點出[樸素集合論中的缺失](../Page/樸素集合論.md "wikilink")。

Ernst Snapper所著The Three Crises in Mathematics: Logicism, Intuitionism,
and Formalism, *Mathematics Magazine*, Vol. 52 (1979), pp.
207-216這篇得獎論文(美國數學學協會MAA 1980年Carl B. Allendoerfer
Award)所列說的三次數學危機則跟上面所講的這三個不完全相同。

## 參閱

  - [第一次數學危機](../Page/第一次數學危機.md "wikilink")
  - [第二次數學危機](../Page/无穷小量.md "wikilink")
  - [第三次數學危機](../Page/第三次數學危機.md "wikilink")
  - [数学基础](../Page/数学基础.md "wikilink")

## 外部連結

  -
  -
  -
[Category:數學哲學](../Category/數學哲學.md "wikilink")
[Category:數學史](../Category/數學史.md "wikilink")