**数学物理**是[数学和](../Page/数学.md "wikilink")[物理学的交叉领域](../Page/物理学.md "wikilink")，指应用特定的数学方法来研究物理学的某些部分。对应的数学方法也叫**数学物理方法**。

数学和物理学的发展历史上一直密不可分。许多数学理论是在物理问题的基础上发展起来的；很多数学方法和工具通常也只在物理学中找到实际应用\[1\]。

## 主要内容

  - [微分方程的解算](../Page/微分方程.md "wikilink")：很多物理问题，比如在[经典力学和](../Page/经典力学.md "wikilink")[量子力学中求解](../Page/量子力学.md "wikilink")[运动方程](../Page/运动方程.md "wikilink")，都可以被归结为求解一定[边界条件下的微分方程](../Page/边界条件.md "wikilink")。因此求解微分方程成为数学物理的最重要组成部分。相关的数学工具包括：
      - [常微分方程的求解](../Page/常微分方程.md "wikilink")
      - [偏微分方程求解](../Page/偏微分方程.md "wikilink")
      - [特殊函数](../Page/特殊函数.md "wikilink")
      - [积分变换](../Page/积分变换.md "wikilink")
      - [复变函数论](../Page/复变函数论.md "wikilink")
  - [场的研究](../Page/场_\(物理\).md "wikilink")（[场论](../Page/场论_\(物理学\).md "wikilink")）：场是现代物理的主要研究对象。[电动力学研究](../Page/电动力学.md "wikilink")[电磁场](../Page/电磁场.md "wikilink")；[广义相对论研究](../Page/广义相对论.md "wikilink")[引力场](../Page/引力场.md "wikilink")；[规范场论研究](../Page/规范场论.md "wikilink")[规范场](../Page/规范场.md "wikilink")。对不同的场要应用不同的数学工具，包括：
      - [矢量分析](../Page/矢量分析.md "wikilink")
      - [张量分析](../Page/张量分析.md "wikilink")
      - [微分几何](../Page/微分几何.md "wikilink")
  - [对称性的研究](../Page/对称性.md "wikilink")：对称性是物理中的重要概念。它是[守恒律的基础](../Page/守恒律.md "wikilink")，在[晶体学和](../Page/晶体学.md "wikilink")[量子场论中都有重要应用](../Page/量子场论.md "wikilink")。对称性由[对称群或相關的代數結構描述](../Page/对称群.md "wikilink")，研究它的数学工具是：
      - [群论](../Page/群论.md "wikilink")
      - [表示論](../Page/表示論.md "wikilink")
  - [作用量](../Page/作用量.md "wikilink")（action）理论：作用量理论被广泛应用于物理学的各个领域，例如[分析力学和](../Page/分析力学.md "wikilink")[路径积分](../Page/路径积分.md "wikilink")。相关的数学工具包括：
      - [变分法](../Page/变分法.md "wikilink")
      - [泛函分析](../Page/泛函分析.md "wikilink")

## 參見

  - [希爾伯特第六問題](../Page/希爾伯特第六問題.md "wikilink")
  - [理論物理學](../Page/理論物理學.md "wikilink")

## 參考

## 文獻

  -
  -
  -
  - (pbk.)

  - (softcover)

  -
  - (This is a reprint of the second (1980) edition of this title.)

  - (This is a reprint of the 1956 second edition.)

  - (This is a reprint of the original (1953) edition of this title.)

  -
  -
  - (This tome was reprinted in 1985.)

  -
  -
  -
[\*](../Category/数学物理.md "wikilink") [S](../Category/應用數學.md "wikilink")
[Category:数学科学职业](../Category/数学科学职业.md "wikilink")

1.  Definition from the *Journal of Mathematical Physics*.