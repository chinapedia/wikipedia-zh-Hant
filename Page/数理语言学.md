**数理语言学**（Mathematical
linguistics）又稱**數學語言學**，是运用[数学的理论和方法来研究和说明](../Page/数学.md "wikilink")[语言的一门](../Page/语言.md "wikilink")[语言学分支学科](../Page/语言学.md "wikilink")。

## 分支学科

### 按照数学方法分类

数理语言学按照运用的数学方法可以分为：

  - [组合语言学](../Page/组合语言学.md "wikilink")（又称为[非数值语言学或](../Page/非数值语言学.md "wikilink")[质量语言学](../Page/质量语言学.md "wikilink")）：主要运用数学中的[集论](../Page/集论.md "wikilink")、[数理逻辑](../Page/数理逻辑.md "wikilink")、[算法等研究语言](../Page/算法.md "wikilink")
  - [定量语言学或](../Page/定量语言学.md "wikilink")[数值语言学或](../Page/数值语言学.md "wikilink")[数量语言学](../Page/数量语言学.md "wikilink")：主要运用[统计学](../Page/统计学.md "wikilink")、[概率论](../Page/概率论.md "wikilink")、[信息论](../Page/信息论.md "wikilink")、[数理分析等研究语言](../Page/数理分析.md "wikilink")

### 按照研究领域分类

数理语言学按照研究领域的不同可以分为：[统计语言学](../Page/统计语言学.md "wikilink")、[代数语言学](../Page/代数语言学.md "wikilink")、[模糊语言学](../Page/模糊语言学.md "wikilink")。

#### 统计语言学

统计语言学主要对语言进行统计研究，以及对语言行为的概率模式研究。包括对语言结构和语言单位，语言变化和语言差异等方面的统计研究。

统计语言学对[字母](../Page/字母.md "wikilink")、[字](../Page/字.md "wikilink")、词的频率统计对于语言信息的计算机化处理非常有帮助，此外还可以用于辞书编纂方面对字词的检索设计。此外，统计语言学还常用于对[作家语言的研究](../Page/作家.md "wikilink")，通过对其作品中字词的统计，可以比较准确的测定常用的修辞手法、语言风格等。

#### 代数语言学

代数语言学又称做[形式语言学](../Page/形式语言学.md "wikilink")，主要研究如何对语言的形式结构进行严格的数学描述，并据此创立形式化的普遍语法。认为语言拥有一种递归机制和生成功能。也就是说有限的语言单位和规则可以生成无限的句子，并用数学的方法将之公式化，创建普遍语法的[数学模型](../Page/数学模型.md "wikilink")。

代数语言学对[计算语言学](../Page/计算语言学.md "wikilink")，[机器翻译](../Page/机器翻译.md "wikilink")、[语言信息处理学](../Page/语言信息处理学.md "wikilink")、[计算机科学都有很大的贡献](../Page/计算机科学.md "wikilink")。但是代数语言学对语言结构、语言系统、语言本质的研究是不擅长的。

#### 模糊语言学

[模糊语言学是用](../Page/模糊语言学.md "wikilink")[模糊数学的方法来研究语言的](../Page/模糊数学.md "wikilink")。

## 參考文獻

  - András Kornai (2001): *[Mathematical
    Linguistics](http://www.helsinki.fi/esslli/courses/readers/K54.pdf)*.
  - Dominic Welsh (1988): *Codes and Cryptography*, Clarendon Press,
    Oxford, ISBN 0-19-853287-3.

[es:Lingüística
matemática](../Page/es:Lingüística_matemática.md "wikilink")

[S](../Category/语言学.md "wikilink") [S](../Category/数学.md "wikilink")
[Category:计算语言学](../Category/计算语言学.md "wikilink")