以下為（程式語言撰寫開發）[整合開發環境的列表](../Page/集成开发环境.md "wikilink")：

## 自由/開放原碼的整合開發環境

### 自由的多（作業）平台整合開發環境

[Gps-screenshot.png](https://zh.wikipedia.org/wiki/File:Gps-screenshot.png "fig:Gps-screenshot.png")正在開發撰寫[Ada](../Page/Ada.md "wikilink")/[CORBA的程式碼](../Page/CORBA.md "wikilink")。\]\]

  - [MonoDevelop是一套自由開放原碼的整合開發環境](../Page/MonoDevelop.md "wikilink")，支援以下的[作業系統](../Page/作業系統.md "wikilink")：[GNU/Linux](../Page/GNU/Linux.md "wikilink")、[MS
    Windows](../Page/MS_Windows.md "wikilink")、[Mac OS
    X](../Page/Mac_OS_X.md "wikilink")，主要用來開發[Mono與](../Page/Mono.md "wikilink")[.NET
    Framework軟體](../Page/.NET_Framework.md "wikilink")。目前支援的[程式語言包括](../Page/程式語言.md "wikilink")：[Python](../Page/Python.md "wikilink")、[Vala](../Page/Vala.md "wikilink")、[C\#](../Page/C♯.md "wikilink")、[Java](../Page/Java.md "wikilink")、[BOO](../Page/BOO.md "wikilink")、[Nemerle](../Page/Nemerle.md "wikilink")、[Visual
    Basic
    .NET](../Page/Visual_Basic_.NET.md "wikilink")、[CIL](../Page/CIL.md "wikilink")、[C與](../Page/C.md "wikilink")[C++](../Page/C++.md "wikilink")。

<!-- end list -->

  - [GNAT Programming
    Studio支援以下的](../Page/GNAT_Programming_Studio.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")：[GNU/Linux](../Page/GNU/Linux.md "wikilink")、[MS
    Windows及](../Page/MS_Windows.md "wikilink")[Solaris for
    SPARC](../Page/Solaris.md "wikilink")，而可使用的[程式語言包括](../Page/程式語言.md "wikilink")：[Ada](../Page/Ada.md "wikilink")、[C](../Page/C語言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Fortran
    90](../Page/Fortran#Fortran_90.md "wikilink")、[Pascal](../Page/Pascal.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python及](../Page/Python.md "wikilink")[Tcl](../Page/Tcl.md "wikilink")。

<!-- end list -->

  - [VIM text
    editor](../Page/Vim.md "wikilink")（文字編輯器）支援14種作業系統與39種不同的[編譯器](../Page/編譯器.md "wikilink")，且VIM能延伸支援任何的編譯器與程式語言。

<!-- end list -->

  - [GNAVI視覺化軟體](../Page/GNAVI.md "wikilink")[開發環境可成為Delphi及Visual](../Page/集成开发环境.md "wikilink")
    Basic的替用，在Windows版的GNAVI中對這些程式語言所提供的相對特點，如今也移植到[Mac OS
    X](../Page/Mac_OS_X.md "wikilink")、[Linux及其他](../Page/Linux.md "wikilink")[類UNIX的](../Page/類UNIX.md "wikilink")[作業系統中](../Page/作業系統.md "wikilink")。

<!-- end list -->

  - [Emacs及](../Page/Emacs.md "wikilink")[XEmacs是大家熟悉的編輯器](../Page/XEmacs.md "wikilink")（Editor），它衍生自[GNU專案](../Page/GNU.md "wikilink")，它的主要特點在於延伸性。

<!-- end list -->

  - [Code::Blocks是一套自由開放原碼的整合開發環境](../Page/Code::Blocks.md "wikilink")，支援[Windows](../Page/Windows.md "wikilink")、[Linux作業平台](../Page/Linux.md "wikilink")，此外也支援[GCC](../Page/GCC.md "wikilink")（[MinGW](../Page/MinGW.md "wikilink")/Linux
    GCC）、[Visual C++](../Page/Visual_C++.md "wikilink") 、[Digital Mars
    Compiler](../Page/Digital_Mars_Compiler.md "wikilink")、[Borland
    C++](../Page/Borland_C++.md "wikilink") 5.5、[Open
    Watcom等](../Page/Open_Watcom.md "wikilink")。

<!-- end list -->

  - [Lazarus是以](../Page/Lazarus.md "wikilink")[Free
    Pascal為基礎的](../Page/Free_Pascal.md "wikilink")[Borland
    Delphi代用品](../Page/Delphi.md "wikilink")，用來建立「[即看即瞭](../Page/即看即瞭.md "wikilink")，[look-and-feel](../Page/:en:look-and-feel.md "wikilink")」的原生性二進位碼、應用程式。

<!-- end list -->

  - [VisualWorks](http://www.cincomsmalltalk.com/) The non commercial
    full version of the mother of all IDEs for
    [Smalltalk](../Page/Smalltalk.md "wikilink") with origins in the
    early 70's. Still under heavy development

### 自由的Windows整合開發環境

[Dev-c-plus-plus.png](https://zh.wikipedia.org/wiki/File:Dev-c-plus-plus.png "fig:Dev-c-plus-plus.png")：一套可在[Windows平台上使用的自由整合開發環境](../Page/Windows.md "wikilink")。\]\]

  - [Dev-C++](../Page/Dev-C++.md "wikilink")：一套開放原碼、用於[Windows平台上的整合開發環境](../Page/Windows.md "wikilink")，是運用[MinGW技術移植而成](../Page/MinGW.md "wikilink")。

<!-- end list -->

  - [SharpDevelop是一套自由](../Page/SharpDevelop.md "wikilink")、開放原碼的[.NET整合開發環境](../Page/.NET.md "wikilink")，且用於[Windows平台上](../Page/Windows.md "wikilink")，支援[C\#](../Page/C_Sharp.md "wikilink")、VB.NET及更多的[.NET程式語言](../Page/.NET.md "wikilink")。

<!-- end list -->

  - [Maguma Open
    Studio](../Page/Maguma_Open_Studio.md "wikilink")，一套在[Windows平台上使用的自由](../Page/Windows.md "wikilink")、開放原碼整合開發環境。

<!-- end list -->

  - [FBSL](../Page/FBSL.md "wikilink")（[Freestyle Basic
    Language](http://www.fbsl.net/phpbb2/index.php)）是一個文字模式的整合開發環境，用於[Windows平台上](../Page/Windows.md "wikilink")，用來開發撰寫FBSL程式語言的程式。

<!-- end list -->

  - [BLIde](../Page/BLIde.md "wikilink")（[BlitzMax
    IDE](http://www.blide.org)）是一套功效強悍的整合開發環境，專門用來開發撰寫BLIde程式語言的應用程式及遊戲，用於[Windows平台](../Page/Windows.md "wikilink")。

### 自由的Linux/Unix整合開發環境

[kdevelop-screenshot.png](https://zh.wikipedia.org/wiki/File:kdevelop-screenshot.png "fig:kdevelop-screenshot.png")是[Linux](../Page/Linux.md "wikilink")/Unix上的一套自由的整合開發環境\]\]

  - [KDevelop](../Page/KDevelop.md "wikilink")，一套正在發展、展露中的整合開發環境，是以[GNU開發工具](../Page/GNU.md "wikilink")（如：[gcc](../Page/gcc.md "wikilink")、make、及[GDB](../Page/GNU_除錯器.md "wikilink")）為基礎所開發成，它還包含了一套圖像化的前端建立器。

<!-- end list -->

  - [Anjuta對](../Page/Anjuta.md "wikilink")[GNOME開發者而言格外好用](../Page/GNOME.md "wikilink")，它十分穩定同時也十分適合重度開發之用。

<!-- end list -->

  - [Gambas採行](../Page/Gambas.md "wikilink")[GPL授權](../Page/GPL.md "wikilink")，以[BASIC程式語言直譯器及物件延伸程式等為基礎](../Page/BASIC.md "wikilink")，整合開發環境，它被設計成一個類似Visual
    Basic的程式語言，並在Linux下執行。

<!-- end list -->

  - [OpenLDev是一個用來與Linux上的開發工具](../Page/OpenLDev.md "wikilink")（如：gcc、autotools及make）搭配使用的圖像形前端。多數的整合開發環境在使用上可說是既笨動又令人困惑，而OpenDev則反此道而行，主張提供一個同時適用於重度開發的專業程式師與起步的入門學習者的易用型開發介面。

<!-- end list -->

  - [kinterdev](http://kinterdev.sourceforge.net/)
  - [Quanta Plus之前稱為Quanta](../Page/Quanta_Plus.md "wikilink")，針對網頁開發之用。
  - [eric是一套以](../Page/eric_Python_IDE.md "wikilink")[Qt為基礎的整合開發環境](../Page/Qt.md "wikilink")，主要是針對[Python程式語言的開發者所設計](../Page/Python.md "wikilink")，雖然它也支援[Java](../Page/Java.md "wikilink")、[C](../Page/C語言.md "wikilink")、[HTML以及其他程式語言](../Page/HTML.md "wikilink")。
  - [Motor](https://web.archive.org/web/20060715191812/http://konst.org.ua/en/motor)是一套文字模式的整合開發環境，可以在Linux作業環境中開發撰寫C/C++程式語言的程式。
  - [Geany](http://geany.uvena.de/)是一套運用[GTK2工具套件的文字編輯器](../Page/GTK+.md "wikilink")，它具備一個整合開發環境的基礎功效特點。

### 自由的Java整合開發環境

  - [Eclipse是針對Java所設計](../Page/Eclipse.md "wikilink")，不過也可以透過外掛程式（如：[DevRocket](../Page/DevRocket.md "wikilink")）而輕易地延伸支援其他程式語言的開發，或網頁程式的開發。

<!-- end list -->

  - [JGrasp](http://www.jgrasp.org/)是[歐本大學](../Page/奧本大學.md "wikilink")（[Auburn
    University](../Page/:en:Auburn_University.md "wikilink")，簡稱：AU）為感謝美國國家科學基金會（[National
    Science
    Foundation](../Page/:en:National_Science_Foundation.md "wikilink")，簡稱：NSF）的研究資助所創寫的一套開放原碼整合開發環境（針對Java程式語言）。除支持Java外，也能夠支援[Ada](../Page/Ada.md "wikilink")、[VHDL](../Page/VHDL.md "wikilink")、[C](../Page/C程式語言.md "wikilink")、[C++](../Page/C++.md "wikilink")、以及不再於主控系統上以GNU工具方式提供的Objective
    C。

<!-- end list -->

  - [jEdit是一位名為](../Page/jEdit.md "wikilink")[Slava
    Pestov的程式師所創寫及維護的文字編輯器](../Page/Slava_Pestov.md "wikilink")，這套編輯器具有彈性且支援公有的外掛介面，因此可以很容易地成為不錯的整合開發環境（針對各種不同的程式語言）。

<!-- end list -->

  - [NetBeans
    IDE是第一套模組化](../Page/NetBeans.md "wikilink")、開放原碼、支援多種程式語言的平台，同時也是支援Java程式語言開發的整合開發環境，事實上它自身就是100%用Java程式語言開發而成。

<!-- end list -->

  - [TruStudio
    Foundation](http://www.xored.com/trustudio)是一個開放原碼專案，同時也是一個支援描述語言（scripting
    language）的開發工具的基礎，它建立在Eclipse平台之上。TruStudio承襲了Eclipse的多樣性開發支援，包括了編輯、除錯所需的開發工具，且具有最先進的支援特點，包括支援PHP、Python、以及其他開放原碼的技術等。

### 自由的Macintosh（麥金塔）整合開發環境

Linux/UNIX的開發工具都可以在[麥金塔](../Page/麥金塔.md "wikilink")（[Macintosh](../Page/Macintosh.md "wikilink")）電腦上執行，此外也可以執行[X11伺服程式](../Page/X11.md "wikilink")，且[蘋果電腦](../Page/蘋果電腦.md "wikilink")（Apple）已經免費附贈[Xcode](../Page/Xcode.md "wikilink")。另外[fink軟體開發專案也能設定Macintosh電腦](../Page/fink.md "wikilink")，透過自由軟體的技術，使其能做到這些。

## 專屬性的整合開發環境

### 專屬性的多（作業）平台整合開發環境

  - [SyncRO Soft公司的](../Page/SyncRO_Soft.md "wikilink")[Oxygen XML
    Editor是一套跨平台的商業整合開發環境](../Page/Oxygen_XML_Editor.md "wikilink")，用來開發以[XML為基礎的應用程式](../Page/XML.md "wikilink")。

<!-- end list -->

  - [Omnis
    Studio是一套跨平台的開發環境](../Page/Omnis_Studio.md "wikilink")，用來開發企業或網站所用的應用程式，且開發成的應用程式能在Windows、Linux、Solaris、及Mac
    OS X上執行。

<!-- end list -->

  - [REALbasic是一套易用的跨平台開發環境](../Page/REALbasic.md "wikilink")，它能開發出在[Mac
    OS 9/X](../Page/Apple_Macintosh.md "wikilink")、[Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink")、[Linux上原生執行的程式](../Page/Linux.md "wikilink")。

<!-- end list -->

  - [Maguma
    Workbench是一套具完整功效特點](../Page/Maguma_Workbench.md "wikilink")、模組化的整合開發環境，且是針對PHP、Python開發需求而設計，能提供完整的功能特色組工PHP、Python運用，此外它也能夠進行客製化，使其在Windows及Linux上執行。

<!-- end list -->

  - [Green Hills
    Software公司的](../Page/Green_Hills_Software.md "wikilink")[MULTI是一套支援多平台的C](../Page/MULTI.md "wikilink")/C++整合開發環境，它能夠在Windows、Linux、及Solaris上安裝使用。
    它是針對嵌入式工程師而設計，並且能與Green Hill
    Software公司的最佳化編譯器緊密搭配，以即予硬體的除錯探針緊密搭配。幾乎可以用在所有現行的處理器上，包括[ARM](../Page/ARM.md "wikilink")、黑鰭（[Blackfin](../Page/Blackfin.md "wikilink")）、冷火（[ColdFire](../Page/ColdFire.md "wikilink")）、[MIPS](../Page/MIPS.md "wikilink")、[PowerPC](../Page/PowerPC.md "wikilink")、[x86](../Page/x86.md "wikilink")、以及[XScale等](../Page/XScale.md "wikilink")。此外也具有CVS瀏覽器、差異觀察器、原始碼自動完成（打字打到一半，自動推測與顯示可能的接續輸入）、圖像式的類別階層產生器、與Eclipse的搭配-{zh-hans:集成器;zh-hant:整合器}-、-{zh-hans:bug跟踪器;zh-hant:臭蟲追蹤器}-、-{zh-hans:条件断点;zh-hant:條件中斷點}、可描述的中斷點、以及更多開發撰寫上的支援特點。

<!-- end list -->

  - [Omnicore公司的](../Page/Omnicore.md "wikilink")[X-develop是一套支援多平台](../Page/X-develop.md "wikilink")（以Java為基礎）的.NET/Java開發環境，它具有一些特別的功能，立即可以在程式開發撰寫時就立即進行邊寫邊進行錯誤查核，例如錯字、錯誤語法等都能立即提醒告知，或可進行程式的再分拆解構，與單元測試-{zh-hans:集成;zh-hant:整合}-、以及及時回復的Java-{zh-hans:调试器;zh-hant:除錯器}-。

<!-- end list -->

  - [ActiveState公司的](../Page/ActiveState.md "wikilink")[Komodo是一套多平台的整合開發環境](../Page/ActiveState_Komodo.md "wikilink")，針對與支援多種描述語言，如：Ruby、Python、Perl、Tcl、以及PHP等。它也包含了一套已先行整合的除錯器，同時也是個「原始碼智慧資料庫」、版本系統整合、以及能將所有各類型的原始碼進行一致性格式化轉換的公用程式。預估此將使其成為極具現代性的整合開發環境。

<!-- end list -->

  - [Wingware公司的](../Page/Wingware.md "wikilink")[Wing
    IDE是一套支援Python撰寫的多平台整合開發環境](../Page/Wing_IDE.md "wikilink")，它具有專業級的原始碼編輯功能、圖像式的除錯器、原始碼瀏覽器、以及其他原始碼的智慧功能，這些都是針對Python程式撰寫所特有的支援設計。

<!-- end list -->

  - [Wind River
    Systems公司的](../Page/Wind_River_Systems.md "wikilink")[Wind
    River
    Workbench是專門用來開發撰寫裝置上執行程式的C](../Page/Wind_River_Workbench.md "wikilink")/C++語言整合開發環境，它是以[Eclipse為基礎所加搭形成的整合開發環境](../Page/Eclipse.md "wikilink")，它也能與協力業者的外掛程式緊密整合，且自有的內部外掛程式也能輕易地進行客製化，它能用於Windows、Linux、Solaris、以及其他主要的目標架構中。

<!-- end list -->

  - [VisualWorks](http://www.cincomsmalltalk.com/)是所有整合開發環境之祖的商業化版本，在早是從1970年代早期開始，並支援[Smalltalk程式語言](../Page/Smalltalk.md "wikilink")，目前仍持續深厚地發展著。

### 專屬性的Windows整合開發環境

  - [Visual Studio](../Page/Microsoft_Visual_Studio.md "wikilink")
    ，[Windows上最普及的整合開發環境為](../Page/Microsoft_Windows.md "wikilink")[Microsoft公司的](../Page/Microsoft.md "wikilink")[Visual
    Studio](../Page/Microsoft_Visual_Studio.md "wikilink")，它支援多種程式語言，如[C\#](../Page/C♯.md "wikilink")、C++、及[Visual
    Basic](../Page/Visual_Basic.md "wikilink")，之前2002年版、2003年版的Visual
    Studio還在名稱末尾處加註「.NET」以表示支援.NET的新程式語言：C\#、[Visual Basic
    .NET以及](../Page/Visual_Basic_.NET.md "wikilink")[Managed
    C++](../Page/C++托管扩展.md "wikilink")，而2005年版的Visual
    Studio則去掉「.NET」的字詞。

<!-- end list -->

  - [Visual Studio](../Page/Microsoft_Visual_Studio.md "wikilink") Team
    System，就一般而言，整合開發環境多是以程式撰寫的個員為主的設計，然而2005年版的Visual
    Studio首次衍生出一套更適合程式開發團隊所用的Visual Studio Team
    System，開始有往應用程式生命週期管理（[Application Lifecycle
    Management](../Page/:en:Application_Lifecycle_Management.md "wikilink")，ALM）的路線發展的意味，Visual
    Studio Team System也一樣支援.NET程式語言。

<!-- end list -->

  - [Visual Studio
    Express](../Page/Microsoft_Visual_Studio_Express.md "wikilink")，由於開放原碼的軟體開發工具在使用普及性與知名度上都逐漸高漲，迫使Microsoft公司必須有所因應，因此自2005年版的Visual
    Studio開始另行提供一套免費的的個員開發工具，不過從「Express」一字即可體會是原有標準版的速簡化設計，功能與特點上都有所限縮，好與其他商業銷售版本保持[價值區隔](../Page/價值區隔.md "wikilink")，不過Express版依舊是封閉原始碼。

<!-- end list -->

  - [DMDScript是](../Page/DMDScript.md "wikilink")[Digital
    Mars公司的IDE是針對C](../Page/Digital_Mars.md "wikilink")++以及[D語言而設計的整合開發環境](../Page/D語言.md "wikilink")。\[1\]

<!-- end list -->

  - [Delphi是針對物件導向型](../Page/Delphi.md "wikilink")[Pascal程式語言](../Page/Pascal.md "wikilink")（[Object
    Pascal](../Page/Object_Pascal.md "wikilink")），由[Borland公司所設計的整合開發環境](../Page/Borland.md "wikilink")，可說是[Turbo
    Pascal的後續接替](../Page/Turbo_Pascal.md "wikilink")，Turbo
    Pascal是Borland公司過去極成功、極受歡迎的程式開發工具。

<!-- end list -->

  - [Turbo C](../Page/Borland_Turbo_C.md "wikilink")、[Turbo
    C++](../Page/Turbo_C++.md "wikilink")：Turbo
    C是由Borland公司開發一套C語言的整合開發環境與編譯器軟體，後來被被Turbo
    C++所取代，具有一個互動的IDE等特點。
  - [C++
    Builder是由](../Page/C++_Builder.md "wikilink")[Borland公司針對C](../Page/Borland.md "wikilink")++程式語言而設計的整合開發環境。

<!-- end list -->

  - [MinGW Developer
    Studio是一套自由授權但卻封閉原始程式碼的整合開發環境](../Page/MinGW_Developer_Studio.md "wikilink")，它運用[MinGW將](../Page/MinGW.md "wikilink")[GNU的開發工具軟體](../Page/GNU.md "wikilink")（過去多在[Linux](../Page/Linux.md "wikilink")、[FreeBSD上](../Page/FreeBSD.md "wikilink")）轉移到Windows平台上來使用，且在Windows上使用的效果與在Linux、FreeBSD上一樣好。

<!-- end list -->

  - [Stylus Studio](../Page/Stylus_Studio.md "wikilink")
    [XML](../Page/XML.md "wikilink")
    IDE是一套商業版的整合開發環境，用來開發以[XML為基礎的應用程式](../Page/XML.md "wikilink")。

<!-- end list -->

  - [Zeus是一套通用性](../Page/Zeus_for_Windows.md "wikilink")、程式語言中立性的整合開發環境，適合用來支援如C/C++、[Java](../Page/Java.md "wikilink")、[PHP](../Page/PHP.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python及](../Page/Python.md "wikilink")[Ruby等程式語言](../Page/Ruby.md "wikilink")。

<!-- end list -->

  - [Peltarion公司的](../Page/Peltarion.md "wikilink")[Synapse是以](../Page/Peltarion_Synapse.md "wikilink")[.NET為基礎的整合開發環境](../Page/.NET.md "wikilink")，特別適合用來開發自適型系統（[adaptive
    systems](../Page/:en:Adaptive_system.md "wikilink")）及[類神經網路](../Page/人工神经网络.md "wikilink")（[neural
    networks](../Page/:en:Artificial_Neural_Network.md "wikilink")）。

### 專屬性的Linux/Unix整合開發環境

  - 在Linux作業平台上，寶蘭（[Borland](../Page/Borland.md "wikilink")）公司推出[Kylix](../Page/Kylix.md "wikilink")，支援Object
    Pascal、C++及C等程式語言的撰寫開發。
  - [Code
    Forge](https://web.archive.org/web/20060709195005/http://www.codeforge.com/products/?id=4)

### 專屬性的Java整合開發環境

  - [JBuilder是](../Page/JBuilder.md "wikilink")[寶蘭公司](../Page/Borland.md "wikilink")（Borland）所最為人熟知的商業性販售的Java程式語言整合開發環境，不過Borland公司也有提供自由、免費的版本，但功能已經過精縮化簡，更先進完整的版本仍要付費才能取得使用授權。

<!-- end list -->

  - [JetBrains公司的](../Page/JetBrains公司.md "wikilink")[IntelliJ
    IDEA是一套商業化銷售的](../Page/IntelliJ_IDEA.md "wikilink")[Java開發環境](../Page/Java.md "wikilink")，主要訴求在於提升開發撰寫上的產能，且開發環境在設計時就已經考慮針對「開發撰寫的分拆、解構」與「由測試端推動的程式設計（意思是：日後程式完成時當如何進行測試驗證等，事先在規劃設計與撰寫階段就將其考慮進去）」進行支援。JetBrains雖是商業性販售，但若是用於開發撰寫開放原碼相關的程式時則可自由使用而不需花費。

<!-- end list -->

  - [昇陽電腦](../Page/昇陽電腦.md "wikilink")（Sun Microsystems）的[Sun ONE
    Studio其本身即是完全用](../Page/Sun_ONE_Studio.md "wikilink")[Java程式語言所開發撰寫成](../Page/Java.md "wikilink")，是以[開放原碼的](../Page/開放原碼.md "wikilink")[NetBeans開發工具平台為基礎所構築成](../Page/NetBeans.md "wikilink")，也因為完全以Java開發撰寫成，所以技術上可以轉移到多種作業平台上執行及管理，此套開發整合開發環境另有自由、免費的社群版（Community
    Edition）可用。

<!-- end list -->

  - [IBM公司的](../Page/IBM.md "wikilink")[Rational Application
    Developer是以](../Page/Rational_Application_Developer.md "wikilink")[Eclipse為基礎所構築成](../Page/Eclipse.md "wikilink")，並對Java及J2EE提供大量、廣泛的支援，此外也能支援其他的程式語言，如[COBOL](../Page/COBOL.md "wikilink")、[PL/I](../Page/PL/I.md "wikilink")。

<!-- end list -->

  - [Xinox軟體公司的](../Page/Xinox軟體公司.md "wikilink")[JCreator是一套用於Java的整合開發環境](../Page/JCreator.md "wikilink")，功效近似於[微軟公司的](../Page/Microsoft.md "wikilink")[Visual
    C++](../Page/Visual_C++.md "wikilink")。

<!-- end list -->

  - [TruStudio
    Professional](http://www.xored.com/trustudio)是一套用於PHP及Python的整合開發環境，是以TruStudio
    Foundation為基礎所構築，特點是具有所見即所得的HTML編輯器（WYSIWYG HTML
    Editor）、原始碼分析/驗證以及原始碼導引等。

<!-- end list -->

  - [Sun Java Studio
    Creator是](../Page/Sun_Java_Studio_Creator.md "wikilink")[昇陽電腦針對](../Page/昇陽電腦.md "wikilink")[Java程式語言的入門初學者所另行設計的整合開發工具](../Page/Java.md "wikilink")，特別容易上手與使用。

<!-- end list -->

  - [JDeveloper](../Page/JDeveloper.md "wikilink")：過去由[甲骨文公司](../Page/Oracle.md "wikilink")（Oracle）向寶蘭公司公司購買[JBuilder的技術後](../Page/JBuilder.md "wikilink")，再添入自有的需求而成的Java程式語言開發工具（IDE），之後陸續的新版本有JDeveloper
    9i、JDeveloper
    10g等，通常不單獨販售，而是隨Oracle的相關軟體一同附贈。JDeveloper支援Java、XML、Web
    Services、PL/SQL、PHP以及其他需求的開發。

<!-- end list -->

  - [WebLogic
    Workshop](../Page/WebLogic_Workshop.md "wikilink")：最早是[賽門鐵克公司](../Page/赛门铁克.md "wikilink")（Symantec）所推出的[Visual
    Café for
    Java](../Page/Visual_Cafe.md "wikilink")，之後賽門鐵克公司將程式開發工具部門分立成一家獨立公司，叫[WebGain公司](../Page/WebGain公司.md "wikilink")，更之後這家公司由[比爾亞系統公司](../Page/BEA.md "wikilink")（BEA
    Systems）所收購，然後運用該團隊的技術及經驗，再加上一批來自微軟公司（[Microsoft](../Page/Microsoft.md "wikilink")）的程式開發工具團隊（過去開發過[Visual
    Basic](../Page/Visual_Basic.md "wikilink")，VB以直覺簡易開發聞名），這群人共同打造了新的Java程式語言的IDE：WebLogic
    Workshop。不過WebLogic Workshop也多半不單獨販售，而是隨BEA的相關軟體一同附贈。

<!-- end list -->

  - [WebSphere
    Studio](../Page/WebSphere_Studio.md "wikilink")：多半不單獨販售，而是隨IBM的相關軟體一同附贈。

### 專屬性的Macintosh（麥金塔）整合開發環境

  - [Apple也為](../Page/蘋果電腦.md "wikilink")[Mac OS
    X作業系統推出廣泛包容性整合開發環境](../Page/Mac_OS_X.md "wikilink")：[Xcode](../Page/Xcode.md "wikilink")，並用其取代較簡易、工具整合性較低的[ProjectBuilder](../Page/ProjectBuilder.md "wikilink")。

### 專屬性的AmigaOS整合開發環境

  - [Cubic
    IDE是一套針對AmigaOS](../Page/Cubic_IDE.md "wikilink")3、MorphOS而設計的整合開發環境。

## 外部連結

  - [PHP程式語言開發撰寫的整合開發環境列表](http://www.php-editors.com/)
  - [Python程式語言開發撰寫的整合開發環境列表](http://wiki.python.org/moin/IntegratedDevelopmentEnvironments)

[\*](../Category/集成开发环境.md "wikilink")

1.  <http://www.digitalmars.com/> DMDScript Scripting Language