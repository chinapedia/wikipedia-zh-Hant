[數位相機製造商品牌列表](../Page/數位相機.md "wikilink")，以能夠提供一般消費需求等級之影像成果的品牌為主。可以照數位相片的手機、Webcam、攝像機等等的製造商不一定會列入。

  - [明基](../Page/明基.md "wikilink")
  - [佳能](../Page/佳能.md "wikilink")
      - **[PowerShot](../Page/Canon_PowerShot.md "wikilink")**
      - **[EOS Digital](../Page/佳能影像产品列表.md "wikilink")**
          -
            （**EOS**加上**數字**加上**D**）
      - **[Digital IXUS](../Page/Digital_IXUS.md "wikilink")**
          -
            （**IXY DIGITAL**日本，至於北美則合併到PowerShot之下）
  - [愛普生](../Page/愛普生.md "wikilink")
      -
        **PhotoPC**
  - [富士軟片](../Page/富士軟片.md "wikilink")
      - **[FinePix](../Page/FinePix.md "wikilink")**
    <!-- end list -->
      -
        已不使用：**MX**-系列、**DS**-系列。
  - [HP](../Page/惠普公司.md "wikilink")
      -
        **Photosmart**
  - [柯達](../Page/伊士曼柯達公司.md "wikilink")
      -
        **DC**-系列，**LS**-系列，**DX**-系列……
        搭載的**EasyShare**功能曾被大幅宣傳。
  - [莱卡](../Page/莱卡.md "wikilink")
      -
        **Digilux**、**-Lux**-系列。
  - [玛米亚](../Page/玛米亚.md "wikilink")
  - [尼康](../Page/尼康.md "wikilink")
      - **[Coolpix](../Page/Coolpix.md "wikilink")**
      - **D**-系列（**D**加上**數字**）。
  - [奧林巴斯](../Page/奧林巴斯.md "wikilink")
      - **[Camedia](../Page/Camedia.md "wikilink")**
      - **[μ](../Page/Olympus_μ.md "wikilink")**
          -
            （**Mju:**／**Stylus**北美，有時候會加上Digital以便與類似型號的傳統相機區分）
      - [***E***系統](../Page/奧林巴斯_E.md "wikilink")
          -
            （**Evolt**北美，為[Four
            Thirds規格](../Page/Four_Thirds.md "wikilink")）
      - **[i:robe](../Page/i:robe.md "wikilink")**
          -
            （**IR**-系列）
    <!-- end list -->
      -
        其他：**FE**-系列、**SP**-系列……
  - [Panasonic](../Page/松下電器.md "wikilink")
      - **[Lumix](../Page/Lumix.md "wikilink")**
  - [賓得士](../Page/賓得士.md "wikilink")
      - **[Optio](../Page/Optio.md "wikilink")**
      - **[\*ist D](../Page/*ist_D.md "wikilink")**、**K**-系列
    <!-- end list -->
      -
        已不使用：**EI**-系列。
  - [宝丽来](../Page/宝丽来.md "wikilink")
  - [理光](../Page/理光.md "wikilink")
      - **[Caplio](../Page/Caplio.md "wikilink")**
    <!-- end list -->
      -
        其他：**RDC**-系列。

\*:其他：**VPC**-系列（**DSC**日本）、**IDshot**。

  - [三星](../Page/三星電子.md "wikilink")
      -
        **NV**-系列。
  - [適馬](../Page/適馬.md "wikilink")
  - [新力](../Page/新力.md "wikilink")
      - **[Cyber-Shot](../Page/Cyber-Shot.md "wikilink")**
      - **[α](../Page/Sony_α.md "wikilink")**
    <!-- end list -->
      -
        已不使用：**[Mavica](../Page/Mavica.md "wikilink")**

## 退出市場的著名製造商與品牌

  - Agfa，**ePhoto**。
  - Contax
  - [JVC](../Page/JVC.md "wikilink")，**GC**-系列。
  - [柯尼卡美能達](../Page/柯尼卡美能達.md "wikilink")，**Dynax D**歐洲（**Maxxum
    D**美洲／**α Digital**日本）2006年退出。
      - [柯尼卡](../Page/柯尼卡.md "wikilink")，**Digital
        Revio**。2003年與[美能達合併](../Page/美能達.md "wikilink")。
      - [美能達](../Page/美能達.md "wikilink")，**[DiMAGE](../Page/DiMAGE.md "wikilink")**。
  - [京瓷](../Page/京瓷.md "wikilink")，**FineCam**。2006年退出。
  - 普立爾
  - [东芝](../Page/东芝.md "wikilink")，**PDR**-系列、**Sora**、**Allegretto**。
  - [宏碁](../Page/宏碁.md "wikilink")
  - [卡西歐](../Page/卡西歐.md "wikilink")
      - **[Exilim](../Page/Exilim.md "wikilink")**
    <!-- end list -->
      -
        其他：**QV**-系列、**GV**-系列

[\*](../Category/数码照相机.md "wikilink")
[Category:生活相關列表](../Category/生活相關列表.md "wikilink")
[Category:品牌列表](../Category/品牌列表.md "wikilink")
[Category:相機品牌](../Category/相機品牌.md "wikilink")