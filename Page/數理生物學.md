**數理生物學**（），又稱**數學生物學**（）或**生物数学**（）是一个[跨学科的领域](../Page/跨学科.md "wikilink")，其主要目标是利用[数学的技巧和工具为自然界](../Page/数学.md "wikilink")，特别是[生物学中的过程](../Page/生物学.md "wikilink")[建模并进行分析](../Page/建模.md "wikilink")。生物数学在生物学的理论和实践中都有广泛的应用。

## 重要性

很久以前，数学即被应用于生物学的研究中。然而直到最近，这一领域才引起人们足够的重视，其原因包括：

  - 由于[基因学的发展](../Page/基因学.md "wikilink")，生物学家采集到的大量数据必须通过解析方法加以处理。

<!-- end list -->

  - 数学理论，特别是[混沌理论的发展](../Page/混沌.md "wikilink")，使人们对[复杂性系统的认识更加深刻](../Page/复杂性系统.md "wikilink")，从而提供了研究生物学中[非线性动力过程的工具和方法](../Page/非线性.md "wikilink")。

<!-- end list -->

  - [计算机科学的发展使](../Page/计算机.md "wikilink")[大规模计算和](../Page/大规模计算.md "wikilink")[模拟成为可能](../Page/模拟.md "wikilink")。

<!-- end list -->

  - 基于人类与动物研究中的复杂性，人们对*[In
    silico](../Page/In_silico.md "wikilink")*的兴趣与日俱增。

## 研究领域

下面是一些生物数学界的热门研究领域。这些项目所研究对象的共同特点是极其复杂并具有非线性的动力特征。一种观点认为，此类多种因素交互的问题只能通过数学或计算机模拟的方式来理解。由于此类研究涉及多个学科，時常是由数学家、物理学家、生物学家、医生、动物学家和化学家等共同完成的。

### 演化生物學及生態學

傳統上，演化生物學和生態學都大量使用數學理論。數學模型在演化及生態學有許多不用的功能，包括用統計學分析資料、預測生物現象、以及檢驗假說的正確性。

在[微演化中](../Page/微演化.md "wikilink")，最主要的數學應用是[族群遺傳學](../Page/族群遺傳學.md "wikilink")，計算的是有限數量的基因頻率如何受[天擇](../Page/天擇.md "wikilink")、[性擇](../Page/性擇.md "wikilink")、[突變](../Page/突變.md "wikilink")、[漂變](../Page/漂變.md "wikilink")、[遷徒等演化力量影響](../Page/遷徒.md "wikilink")；可以由觀察到的基因頻率回推演化力量，或是由演化力量預測未來的基因頻率。當有大量基因座，而且個基因座對性狀的影響都很小時，可以用[計量遺傳學](../Page/計量遺傳學.md "wikilink")（Quantitative
genetics）描述性狀的分佈如何演化；通常假設性狀是常態分佈，計算基平均值和方差，[羅納德·費雪為](../Page/羅納德·費雪.md "wikilink")[統計學打下的基礎即是由此建立](../Page/統計學.md "wikilink")。由[John
Maynard
Smith引进的](../Page/:en:John_Maynard_Smith.md "wikilink")[进化博弈理论是另一個重要的數理應用](../Page/进化博弈理论.md "wikilink")。

在[巨演化中](../Page/巨演化.md "wikilink")，[系統分類學大量使用數學](../Page/系統分類學.md "wikilink")。該領域比較生物間性狀的異同（包括基因組成）後，用[最大簡約法或](../Page/最大簡約法.md "wikilink")[最大似然估計等數學理論來重建演化歷史](../Page/最大似然估計.md "wikilink")。

在生態學中，族群動態學（Population
dynamics）描述生物族群大小的變化。[馬爾薩斯的](../Page/馬爾薩斯.md "wikilink")《[人口論](../Page/人口論.md "wikilink")》提出指數成長的人口模型，可以說是最早的族群動態學理論。[Lottak-Volterra方程解釋天敵和獵物的族群波動關係](../Page/洛特卡－沃尔泰拉方程.md "wikilink")，也早在19世纪就被广泛地研究。与人口动力学密切相关的另一领域是[数学流行病学](../Page/流行病学.md "wikilink")，其主要研究内容为传染病在易感人群中的传播。目前已经有多个病毒传播模型在公共健康政策的决策中产生了重要影响。[群集生態學以及](../Page/群集生態學.md "wikilink")[生物地理學也大量使用數學](../Page/生物地理學.md "wikilink")，包括[羅伯特·麥克阿瑟和](../Page/羅伯特·麥克阿瑟.md "wikilink")[艾德華·威爾森提出的的島嶼模型](../Page/艾德華·威爾森.md "wikilink")，以及[生態學中性理論](../Page/生態學中性理論.md "wikilink")，計算環境因子影響如何影響物種遷徒、滅絕、以及種化的頻率，從而解釋一個地區的物種多樣性。

### 细胞模型和分子生物学

由于[分子生物学的发展](../Page/分子生物学.md "wikilink")，近年来该领域的研究硕果累累。

  - [神经元模型和](../Page/神经元.md "wikilink")[致癌物](../Page/致癌物.md "wikilink")[1](http://www.maths.gla.ac.uk/research/groups/biology/kal.htm)
  - 生物组织培养动力学
    [2](https://web.archive.org/web/20090202005852/http://www.maths.gla.ac.uk/~rwo/research_areas.htm)
  - 酶化学和酶动力学。[3](http://www.informatics.indiana.edu/schnell/research/enzymology.asp)
  - [癌症模型与模拟](../Page/癌症.md "wikilink")
    [4](http://calvino.polito.it/~mcrtn/)
  - 交互细胞动力学
    [5](http://www.ma.hw.ac.uk/~jas/researchinterests/index.html)
  - 疤痕组织形成模型
    [6](http://www.ma.hw.ac.uk/~jas/researchinterests/scartissueformation.html)
  - 细胞内部动力学模型
    [7](https://web.archive.org/web/20060323192719/http://www.sbi.uni-rostock.de/dokumente/p_gilles_paper.pdf)

### 生理系统模型

  - 动脉疾病模型 [8](http://www.maths.gla.ac.uk/~nah/research_interests.html)
  - 多尺度心脏模型
    [9](https://web.archive.org/web/20090113215021/http://www.integrativebiology.ox.ac.uk/heartmodel.html)

## 数学方法

一般来说，在生物数学中，一个生物学的模型往往被抽象转化成为一个方程或方程组。在不严格的意义下，往往将“模型”和“方程组”视为同一含义。该方程或方程组的解，可以描述一个生物系统随时间的演进或在[平衡点附近的性态](../Page/平衡点.md "wikilink")。

生物数学中有多种类型的方程和性态，它们一般与模型或方程是独立的。在建模的过程中，往往进行一些假设，从而使得问题更容易用抽象语言描述。

下面是一些常用的数学工具和假设：

### 确定过程（动力系统）

动力系统用来描述一个从给定的初态到某个终态的[映射](../Page/映射.md "wikilink")。由给定的初态出发，随着时间的变化，一个动力系统始终产生相同的轨线，并且不同的轨线彼此不相交。

  - [常微分方程](../Page/常微分方程.md "wikilink")（连续时间域，连续相空间，没有空间域的微分）。可以参考[数值常微分方程](../Page/数值常微分方程.md "wikilink")。
  - [偏微分方程](../Page/偏微分方程.md "wikilink")（连续时间域，连续相空间，有空间域的微分）。可以参考[数值偏微分方程](../Page/数值偏微分方程.md "wikilink")。
  - [差分方程](../Page/差分方程.md "wikilink")（离散时间域，连续相空间）。

### 不确定过程（随机动力系统）

随即动力系统用来描述一个从给定的初态到某个终态随机的[映射](../Page/映射.md "wikilink")，将相空间视为一个[随机变量及相应的](../Page/随机变量.md "wikilink")[随机分布](../Page/随机分布.md "wikilink")。

  - 非马尔可夫过程。
  - 跳跃。
  - 连续马尔可夫过程。

### 空间域模型

这方面的经典工作可以参考[艾伦·图灵](../Page/艾伦·图灵.md "wikilink")1952年发表於《[器官学](../Page/器官学.md "wikilink")》（**）的文章〈器官学的化学基础〉。

  - 创伤康复实验中的波传递。[10](http://www.maths.ox.ac.uk/~maini/public/gallery/twwha.htm)
  - 细胞群形态
    [11](http://www.math.ubc.ca/people/faculty/keshet/research.html)
  - 器官的机械化学理论
    [12](http://www.maths.ox.ac.uk/~maini/public/gallery/mctom.htm)
  - 生物模式的形成
    [13](http://www.maths.ox.ac.uk/~maini/public/gallery/bpf.htm)
  - 由样本生成的空间分布
    [14](http://links.jstor.org/sici?sici=0030-1299%28199008%2958%3A3%3C257%3ASDOTMU%3E2.0.CO%3B2-S&size=LARGE&origin=JSTOR-enlargePage)

## 参考书目

  - S.H. Strogatz, *Nonlinear dynamics and Chaos: Applications to
    Physics, Biology, Chemistry, and Engineering.* Perseus., 2001, ISBN
    0-7382-0453-6
  - N.G. van Kampen, *Stochastic Processes in Physics and Chemistry*,
    North Holland., 3rd ed. 2001, ISBN 0-444-89349-0
  - P.G. Drazin, *Nonlinear systems*. C.U.P., 1992. ISBN 0-521-40668-4
  - L. Edelstein-Keshet, *Mathematical Models in Biology*. SIAM, 2004.
    ISBN 0-07-554950-6
  - G. Forgacs and S. A. Newman, *Biological Physics of the Developing
    Embryo*. C.U.P., 2005. ISBN 0-521-78337-2
  - A. Goldbeter, *Biochemical oscillations and cellular rhythms*.
    C.U.P., 1996. ISBN 0-521-59946-6
  - F. Hoppensteadt, *Mathematical theories of populations:
    demographics, genetics and epidemics*. SIAM, Philadelphia, 1975
    (reprinted 1993). ISBN 0-89871-017-0
  - D.W. Jordan and P. Smith, *Nonlinear ordinary differential
    equations*, 2nd ed. O.U.P., 1987. ISBN 0-19-856562-3
  - J.D. Murray, *Mathematical Biology*. Springer-Verlag, 3rd ed. in 2
    vols.: *Mathematical Biology: I. An Introduction*, 2002 ISBN
    0-387-95223-3; *Mathematical Biology: II. Spatial Models and
    Biomedical Applications*, 2003 ISBN 0-387-95228-4.
  - E. Renshaw, *Modelling biological populations in space and time*.
    C.U.P., 1991. ISBN 0-521-44855-7
  - S.I. Rubinow, *Introduction to mathematical biology*. John Wiley,
    1975. ISBN 0-471-74446-8
  - L.A. Segel, *Modeling dynamic phenomena in molecular and cellular
    biology*. C.U.P., 1984. ISBN 0-521-27477-X
  - L. Preziosi, *Cancer Modelling and Simulation*. Chapman Hall/CRC
    Press, 2003. ISBN 1-58488-361-8

## 扩展阅读

  - F. Hoppensteadt, *[Getting Started in Mathematical
    Biology](http://www.ams.org/notices/199509/hoppensteadt.pdf)*.
    Notices of American Mathematical Society, Sept. 1995.
  - M. C. Reed, *[Why Is Mathematical Biology So
    Hard?](http://www.resnet.wm.edu/~jxshix/math490/reed.pdf)* Notices
    of American Mathematical Society, March, 2004.
  - R. M. May, *[Uses and Abuses of Mathematics in
    Biology](http://www.resnet.wm.edu/~jxshix/math490/may.pdf)*.
    Science, February 6, 2004.
  - J. D. Murray, *[How the leopard gets its
    spots?](http://www.resnet.wm.edu/~jxshix/math490/murray.doc)*
    Scientific American, 258(3): 80-87, 1988.
  - S. Schnell, R. Grima, P. K. Maini, *[Multiscale Modeling in
    Biology](http://eprints.maths.ox.ac.uk/567/01/224.pdf)*, American
    Scientist, Vol 95, pages 134-142, March-April 2007.

## 外部链接

  - [The Collection of Biostatistics Research
    Archive](http://www.biostatsresearch.com/repository/)
  - [Statistical Applications in Genetics and Molecular
    Biology](https://web.archive.org/web/20060420031028/http://www.bepress.com/sagmb/)
  - [The International Journal of
    Biostatistics](https://web.archive.org/web/20060323060012/http://www.bepress.com/ijb/)
  - [Society for Mathematical Biology](http://www.smb.org/)
  - [European Society for Mathematical and Theoretical
    Biology](http://www.esmtb.org/)
  - [Biomathematics Research Centre at University of
    Canterbury](http://www.math.canterbury.ac.nz/bio/)
  - [Centre for Mathematical Biology at Oxford
    University](http://www.maths.ox.ac.uk/cmb/)
  - [Mathematical Biology at the National Institute for Medical
    Research](https://web.archive.org/web/20050308114707/http://mathbio.nimr.mrc.ac.uk/)
  - [Institute for Medical BioMathematics](http://www.imbm.org/)
  - [*Mathematical Biology Systems of Differential
    Equations*](http://eqworld.ipmnet.ru/en/solutions/syspde/spde-toc2.pdf)
    from EqWorld: The World of Mathematical Equations
  - [Systems Biology Workbench - a set of tools for modelling
    biochemical networks](http://sbw.kgi.edu)

{{-}}

[生物數學](../Category/生物數學.md "wikilink")