[Wang_Wei_001.jpg](https://zh.wikipedia.org/wiki/File:Wang_Wei_001.jpg "fig:Wang_Wei_001.jpg")《伏生受经图》\]\]
[Ni_Zan_Water_and_Bamboo_Dwelling.jpg](https://zh.wikipedia.org/wiki/File:Ni_Zan_Water_and_Bamboo_Dwelling.jpg "fig:Ni_Zan_Water_and_Bamboo_Dwelling.jpg")《水竹居图》\]\]
**文人画**亦称“**士人画**”。[中国传统绘画及東亞其他地區](../Page/中国画.md "wikilink")[唐畫的重要流派](../Page/唐畫.md "wikilink")，泛指中国[古代社会中](../Page/古代社会.md "wikilink")[文人](../Page/文人.md "wikilink")、[士大夫的绘画](../Page/士大夫.md "wikilink")，以别于[宫廷绘画](../Page/宫廷.md "wikilink")（[院畫](../Page/院畫.md "wikilink")）和[民间绘画](../Page/民间.md "wikilink")。

文人畫的歷史，大多數的研究多起於[宋代](../Page/宋代.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[蘇軾所提倡的](../Page/蘇軾.md "wikilink")「墨戲」、“士人画”，被認為是文人畫的濫觴。到了[北宋末期](../Page/北宋.md "wikilink")，[米芾](../Page/米芾.md "wikilink")、[米友仁父子的橫點](../Page/米友仁.md "wikilink")[山水畫](../Page/山水畫.md "wikilink")（後世稱之為米點皴、米家山）被視作文人畫山水最早的典型。\*[南宋的文人畫發展上看不出明顯的傳承](../Page/南宋.md "wikilink")，反而今日可以看到[金代畫家](../Page/金代.md "wikilink")[武元直的](../Page/武元直.md "wikilink")《赤壁圖》、[王庭筠的](../Page/王庭筠.md "wikilink")《幽竹枯槎圖》、[李山的](../Page/李山.md "wikilink")《風雪山松圖》等，繼承北宋文人的繪畫風格，發展出有別於南宋[院畫的體系](../Page/院畫.md "wikilink")。[明代](../Page/明朝.md "wikilink")[董其昌提出](../Page/董其昌.md "wikilink")“文人画”一词，推[唐代](../Page/唐朝.md "wikilink")[王维为始祖](../Page/王维.md "wikilink")。文人画家讲究全面的文化修养，提倡“读万卷书，行万里路”，必须[诗](../Page/诗.md "wikilink")、[书](../Page/书法.md "wikilink")、画、[印相得益彰](../Page/印.md "wikilink")，人品、才情、学问、思想缺一不可。在题材上多为[山水](../Page/山水.md "wikilink")、[花鸟以及](../Page/花鸟.md "wikilink")[梅](../Page/梅.md "wikilink")[兰](../Page/兰.md "wikilink")[竹](../Page/竹.md "wikilink")[菊一类](../Page/菊.md "wikilink")，他们强调[气韵和](../Page/气韵.md "wikilink")[笔情墨趣](../Page/笔情墨趣.md "wikilink")，多为抒发“性灵”之作，标举“士气”、“逸品”，注重意境的缔造。

## 參見

  - [王维](../Page/王维.md "wikilink")
  - [文同](../Page/文同.md "wikilink")
  - [苏轼](../Page/苏轼.md "wikilink")
  - [清初四僧](../Page/清初四僧.md "wikilink")
  - [吴门画派](../Page/吴门画派.md "wikilink")

## 延伸閱讀

  - 何沛雄：〈[文人画与文人画传统——对20世纪中国画史研究中一个概念的界定](http://www.nssd.org/articles/article_read.aspx?id=1003360740)〉。
  - 蔡星仪：〈[白阳之“仙”与青藤之“禅”——兼论文人画之主流与潜变](http://www.nssd.org/articles/Article_Read.aspx?id=26294363)〉。
  - 森正夫：〈[明末清初中國的“文人畫＂到江戸時代日本的“南畫＂─關於它的時間間隔─](http://www.his.ncku.edu.tw/chinese/uploadeds/331.pdf)〉。

[Category:中国传统绘画](../Category/中国传统绘画.md "wikilink")
[Category:中国美术史](../Category/中国美术史.md "wikilink")
[Category:東亞傳統繪畫](../Category/東亞傳統繪畫.md "wikilink")