**文化史**是研究一个时期或一个地区的精神文化生活的科学。文化史不直接讨论政治或国家的历史，在文化史中，一个特定的年代或日期较于政治史不十分重要。在文化史中，重要的因素是[语言](../Page/语言.md "wikilink")、[文學](../Page/文學.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[宗教](../Page/宗教.md "wikilink")、[制度和](../Page/制度.md "wikilink")[科技等](../Page/科技.md "wikilink")。

文化史的开始可以追溯到18世纪，当时[启蒙运动](../Page/启蒙运动.md "wikilink")（其代表人[伏尔泰](../Page/伏尔泰.md "wikilink")）相信人类文化的发展和不断的进步，布克哈特也是代表人物之一。

## 新文化史

1970年代-1980年代開始，西方的文化史開始有了新的發展，這個新的發展又被稱作新文化史。

其發展主要來自幾個方面的影響刺激：

  - [文化人類學](../Page/文化人類學.md "wikilink")，尤其[詮釋人類學學者](../Page/詮釋人類學.md "wikilink")，[克里福得·葛茲](../Page/克里福得·葛茲.md "wikilink")（Clifford
    Geertz）對文化[深層敘述](../Page/深層敘述.md "wikilink")（thick
    description）的概念的影響。
  - [米歇爾·傅柯的文化史研究](../Page/米歇爾·傅柯.md "wikilink")。尤其是對於[權力概念的討論](../Page/權力.md "wikilink")。
  - [文學批評理論](../Page/文學批評.md "wikilink")，主要是敘事跟符號理論，後來[後現代主義理論逐漸成為強勢的主流](../Page/後現代主義.md "wikilink")。

其主要特色，是對於傳統的文化史敘述方式有所改變，更重視探討隱藏於各種文化事物表面之下的深層意義。而對於歷史中的敘述資料，也更重視其背後隱藏的文化內含、價值而不僅看其本身所傳達的內容。也因此，在傳統文化史被最不受關注的政治史，也可以用新的詮釋方式，來研究政治行為中隱含的表演、背後的文化價值等。

新文化史相關的重要概念包括：[權力](../Page/權力.md "wikilink")、[意識型態](../Page/意識型態.md "wikilink")、[階級](../Page/階級.md "wikilink")、[認同](../Page/認同.md "wikilink")、[族群](../Page/族群.md "wikilink")、[表演](../Page/表演.md "wikilink")、[歷史記憶](../Page/歷史記憶.md "wikilink")、傳統的發明等。

## 时间

  - [古代文化史](../Page/古代文化史.md "wikilink")
  - [近代文化史](../Page/近代文化史.md "wikilink")
  - [现代文化史](../Page/现代文化史.md "wikilink")

## 地域

  - [西方文化史](../Page/西方文化史.md "wikilink")
      - [欧洲文化史](../Page/欧洲文化史.md "wikilink")
      - [美国文化史](../Page/美国文化史.md "wikilink")
  - [东方文化史](../Page/东方文化史.md "wikilink")
      - [中国文化史](../Page/中国文化史.md "wikilink")
      - [印度文化史](../Page/印度文化史.md "wikilink")
      - [日本文化史](../Page/日本文化史.md "wikilink")
      - [台灣文化史](../Page/台灣文化史.md "wikilink")

## 学科

  - [文学史](../Page/文学史.md "wikilink")
  - [艺术史](../Page/艺术史.md "wikilink")
  - [宗教史](../Page/宗教史.md "wikilink")
  - [生活史](../Page/生活史.md "wikilink")
  - [科学史](../Page/科学史.md "wikilink")
  - [哲学史](../Page/哲学史.md "wikilink")
  - [圖書史](../Page/圖書史.md "wikilink")

[category:各種主題的歷史](../Page/category:各種主題的歷史.md "wikilink")

[\*](../Category/文化史.md "wikilink")