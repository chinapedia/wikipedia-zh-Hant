**文化地理学**（），是研究人类[文化的空间组合](../Page/文化.md "wikilink")，人类活动所创造的文化在起源、传播、宗教、经济、政府方面与环境的关系的学科。文化地理学的研究，旨在探讨各地区人类[社会的文化定型活动](../Page/社会.md "wikilink")，人们对景观的开发利用和影响，人类文化在改变生态环境过程中所起的作用，以及该地区区域特性的文化继承性，也就是研究人类文化活动的空间变化。它是[人文地理学的重要分支](../Page/人文地理学.md "wikilink")，也是[文化学的一个组成部分](../Page/文化学.md "wikilink")\[1\]。

## 领域

[Mall_culture_jakarta06.jpg](https://zh.wikipedia.org/wiki/File:Mall_culture_jakarta06.jpg "fig:Mall_culture_jakarta06.jpg")的全球化和商城文化\]\]
文化地理学的研究领域非常广阔。其研究领域包括有：

  - [全球化已经被理论化为文化趋同的解释](../Page/全球化.md "wikilink")\[2\]

  - [西方化或其他类似过程如](../Page/西方化.md "wikilink")[现代化](../Page/现代化.md "wikilink")、[美国化](../Page/美国化.md "wikilink")、[伊斯兰化和其他特点](../Page/伊斯兰化.md "wikilink")\[3\]

  - 通过[文化帝国主义传播的](../Page/文化帝国主义.md "wikilink")[文化霸权或](../Page/文化霸权.md "wikilink")[文化同化](../Page/文化同化.md "wikilink")

  - 文化地域差异，即生活方式包括思想、差异研究的态度、语言、行为、制度和权力结构和整个地域文化的实践范围\[4\]

  - \[5\]\[6\] 和

  - 其他主题包括有、[殖民主义](../Page/殖民主义.md "wikilink")、、[国际主义](../Page/国际主义.md "wikilink")、[移民](../Page/移民.md "wikilink")、[移居](../Page/移居.md "wikilink")、[生态旅游](../Page/生态旅游.md "wikilink")。

## 历史

文化地理学的历史可以追溯到古代地理学家如[托勒密或](../Page/托勒密.md "wikilink")[斯特拉波](../Page/斯特拉波.md "wikilink")；然而它作为一门学术学科，是在20世纪早期引入，以取代此前统治地理学的[环境决定论](../Page/环境决定论.md "wikilink")，后者主张人类和社会均由其所在的[自然环境所控制](../Page/自然环境.md "wikilink")\[7\]。文化地理学并非研究环境差距所引起的预决性，它对更有兴趣\[8\]。这一趋势由[加州大学伯克利分校](../Page/加州大学伯克利分校.md "wikilink")所领导，该领域也长期由美国所主导。

苏尔将景观定义为地理学的研究单元。他认为文化和社会均与其创造的景观相辅相成\[9\]，使得与人类创造的人文景观相映\[10\]。他的研究具有高度的[定性研究和描述性](../Page/定性研究.md "wikilink")，并超过1930年代[理查德·哈特向的](../Page/理查德·哈特向.md "wikilink")[区域地理学](../Page/区域地理学.md "wikilink")，后者跟随获得长足发展。文化地理学曾是边缘学科，尽管大量地理学作家比如大量促使人文景观的发展。

在1970年代，对于实证主义地理学的批判促使地理学家寻求超过定量研究的发展，包括文化地理学的一些领域从而得以重新评估。然而，在许多地理分支学科中，文化地理学继续扮演着重要的角色。

## 变革

1980年代以来，一类“新文化地理学”（）开始出现，它基于不同的理论传统，包括[马克思主义](../Page/马克思主义.md "wikilink")[政治经济模型](../Page/政治经济.md "wikilink")、[女性主义理论](../Page/女性主义理论.md "wikilink")、、[后结构主义和](../Page/后结构主义.md "wikilink")[精神分析](../Page/精神分析.md "wikilink")。

特别是它吸取了欧洲的[米歇尔·福柯和](../Page/米歇尔·福柯.md "wikilink")理论，和更多样化的，有一种协调一致的努力试图解构文化背后的各种权力关系。比如与身份建构所体现的某一特定地区的利益。

一类研究领域包括：

  -
  - [儿童地理学](../Page/儿童地理学.md "wikilink")

  - 部分[旅游地理](../Page/旅游地理.md "wikilink")

  - [行为地理学](../Page/行为地理学.md "wikilink")

  -
  - 部分[政治地理学](../Page/政治地理学.md "wikilink")

  - [音乐地理学](../Page/音乐地理学.md "wikilink")

一些新文化地理学的学者开始反思自我批判，认为此前身份和空间的观点是静态的。它遵循了[后结构主义者对于福柯的批评](../Page/后结构主义.md "wikilink")，比如和[吉尔·德勒兹](../Page/吉尔·德勒兹.md "wikilink")。在这个领域中，和[人口流动学研究成为主流](../Page/人口流動.md "wikilink")。其他学者也有尝试将这些批评融入到新文化地理学中。

## 相关条目

  - [文化区域](../Page/文化区域.md "wikilink")
  - [环境决定论](../Page/环境决定论.md "wikilink")
  - [或然性 (地理学)](../Page/或然性_\(地理学\).md "wikilink")

## 参考

## 延伸阅读

  - Carter, George F. *Man and the Land. A Cultural Geography*. New
    York: Holt, Rinehart & Winston, 1964.
  - Tuan, Yi-Fu. 2004. "CENTENNIAL FORUM: Cultural Geography: Glances
    Backward and Forward". Annals of the Association of American
    Geographers. 94 (4): 729-733.

[文化地理学](../Category/文化地理学.md "wikilink")

1.

2.

3.

4.  Jones, Richard C.; 2006; Cultural Diversity in a “Bi-Cultural” City:
    Factors in the Location of Ancestry Groups in San Antonio; Journal
    of Cultural Geography

5.  Sinha, Amita; 2006; Cultural Landscape of Pavagadh: The Abode of
    Mother Goddess Kalika; Journal of Cultural Geography

6.  Kuhlken, Robert; 2002; Intensive Agricultural Landscapes of Oceania;
    Journal of Cultural Geography

7.  Peet, Richard; 1998; Modern Geographical Thought; Blackwell

8.
9.  Sauer, Carl; 1925; The Morphology of Landscape

10.