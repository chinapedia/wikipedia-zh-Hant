**文学社**，[清末](../Page/清末.md "wikilink")[武汉](../Page/武汉.md "wikilink")[新军中的](../Page/新军.md "wikilink")[革命团体](../Page/革命.md "wikilink")，与[同盟会无任何渊源](../Page/同盟会.md "wikilink")。\[1\]

## 沿革

清[光緒己酉年](../Page/光緒.md "wikilink")[正月初九](../Page/正月初九.md "wikilink")[玉皇誕](../Page/玉皇誕.md "wikilink")（1909年1月30日，星期六），[张廷辅](../Page/张廷辅.md "wikilink")、[刘复基](../Page/刘复基.md "wikilink")、[蒋翊武](../Page/蒋翊武.md "wikilink")、[李擎甫](../Page/李擎甫.md "wikilink")、[沈廷桢](../Page/沈廷桢.md "wikilink")、[张筱溪](../Page/张筱溪.md "wikilink")、[唐子洪](../Page/唐子洪.md "wikilink")、[商旭旦](../Page/商旭旦.md "wikilink")、[谢鸣岐](../Page/谢鸣岐.md "wikilink")、[萧良才](../Page/萧良才.md "wikilink")、[曹珩和](../Page/曹珩.md "wikilink")[黄季修等](../Page/黄季修.md "wikilink")12人于[武昌奧略樓](../Page/武昌.md "wikilink")（[黄鹤楼被焚毀的替代建築](../Page/黄鹤楼.md "wikilink")）發起革命組織**羣治学社**，又稱**羣志學社**，宗旨是“**兴汉排满，推翻专制，驱逐满奴，夺回汉室江山**”
；1910年8月，因懷疑被[公家發覺](../Page/公家.md "wikilink")，改组为**振武学社**。

1911年1月，湖北革命党人以**振武学社**之名已经暴露，改名**文学社**，宗旨自稱「以聯契約志研究文學」；蒋翊武、刘复基为正副社长；社址设在武昌[小朝街](../Page/小朝街.md "wikilink")85号。

文学社为发动[武昌起義的两个核心团体之一](../Page/武昌起義.md "wikilink")，另一个是[共进会](../Page/共进会.md "wikilink")。文学社有严密的组织，入社会员需经严格考察，以防不测；到武昌起义前，文学社已在新军镇（师）、协（旅）、标（团）、营、队（连）、哨（排）、棚（班）各层单位发展，共有400多人。文学社的成员多为士兵，社费在每月饷银中捐输；共进会的會費並不在士兵中月捐。\[2\]

## 參見

  - [共進會](../Page/共進會.md "wikilink")

## 参考资料

[文学社](../Category/文学社.md "wikilink")
[Category:清末革命组织](../Category/清末革命组织.md "wikilink")
[Category:武汉政治组织](../Category/武汉政治组织.md "wikilink")
[Category:1909年建立的組織](../Category/1909年建立的組織.md "wikilink")

1.
2.