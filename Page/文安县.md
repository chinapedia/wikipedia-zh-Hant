**文安县**在中国[河北省中部](../Page/河北省.md "wikilink")、[大清河上游](../Page/大清河.md "wikilink")，是[廊坊市下辖的一个县](../Page/廊坊市.md "wikilink")。

## 气候

河流洼淀众多，水量充沛，年降水量583.4毫米，年均温12℃。

## 行政区划

下辖12个[镇](../Page/行政建制镇.md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")：

。

## 名人

  - [王儀](../Page/王儀_\(嘉靖進士\).md "wikilink")、[王緘](../Page/王緘.md "wikilink")：明代父子進士。
  - [戴玉强](../Page/戴玉强.md "wikilink")

## 官方网站

  - [中国文安政府网](http://www.wenan.lf.gov.cn/)
  - [廊坊市文安县商务之窗](https://web.archive.org/web/20090725222321/http://wenan.mofcom.gov.cn/)

[文安县](../Page/category:文安县.md "wikilink")
[县](../Page/category:廊坊区县市.md "wikilink")

[廊坊](../Category/河北省县份.md "wikilink")