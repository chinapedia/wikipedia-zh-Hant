**文恩澄**（，）原名**文婉澄**，香港女導演，前唱作歌手。

## 生平

[唱作人](../Page/唱作人.md "wikilink")，[導演](../Page/導演.md "wikilink")，[編劇](../Page/編劇.md "wikilink")，[剪接](../Page/剪接.md "wikilink")
八級鋼琴，五級樂理，三級小提琴。6歲開始學習鋼琴，就讀[迦密主恩中學](../Page/迦密主恩中學.md "wikilink")，中學時移民美國，大學主修[電腦科學](../Page/電腦科學.md "wikilink")。回港發展音樂事業後，曾任兼職模特兒，拍過不多亦不少的廣告，亦曾任鋼琴教師及參與電台配樂/電視台音樂節目伴奏等多項音樂有關的工作，算是半幕前半幕後出身。賣點是琴藝及唱作，聲底厚。經常與網上紅人ming仔拍擋製作及演出MV。
[TVB更形容她為](../Page/TVB.md "wikilink")「新晉美女唱作歌手」，以美麗清純的玉女形象教人為之傾倒\[1\]。

2004年開始在網上論壇上載自己的作品，亦曾經在網上以網名「橙橙」為名，她的原創音樂也曾被人流傳過好一陣子，特別是寫了 "水孩子"
及為已故的著名網上歌手阿霖寫出歌曲 "灰姑娘正傳"
後更廣為網上原創聽眾認識，自從自己開設了個人網站後，便停止在網上論壇上載作品，簽公司後網頁也關閉了。正式做歌手前，有豐富的音樂幕後經驗，電台配樂製作/演唱會及電視節目琴手和音/作曲等等，入行前最近期的流行曲作品有許志安的《孫果與了因》。

2007年被公司發掘，正式成為 [EC
Music](../Page/EC_Music.md "wikilink")-[國際娛樂旗下歌手](../Page/國際娛樂.md "wikilink")，與[陳曉東屬同門歌手](../Page/陳曉東.md "wikilink")，第一首派台歌是與陳曉東合唱的《愛情第二》，也是出自她的手筆。

2008年10月初已與 [EC
Music](../Page/EC_Music.md "wikilink")-[國際娛樂解約](../Page/國際娛樂.md "wikilink")。

2010年 接管狗狗咖啡店（DogSide cafe）2011 年轉讓咖啡店

2011年
與唱片公司框格音樂事務簽約,成為旗下歌手。現亦定期上[新城的愛情Phone](../Page/新城電台.md "wikilink")-in搞笑節目\<\<[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")\>\>做嘉賓主持。

2012年 加入導演及影片剪接製作行列，於YOUTUBE上載自家製的
〔宅女BLOG〕開始，之後為自己的歌曲《幸褔大曬》及《水孩子》執導，其執導之兩套微電影《預見》及《溝女？唔駛用劍嘅》於《香港心》短片比賽中得獎，同時入圍澳門影展。

2013年 - 2016 年
主力於幕後影片製作，工作範圍有電視廣告、微電影、宣傳片、節目、MV等等。同時也有不時參與幕前演出，擔任演員，及在新城電台擔任了兩年「吳彤人敢唱」及
「BUSKINGDOM」節目主持

2018年 - 已全職擔任影片導演工作多年，接拍不少電視廣告、MV及宣傳片。於2018年完成首播處女電影作品
《極品閨蜜》，入圍多個國際影展並奪得多個提名及獎項，當中包括《最佳新導演》《最佳外語片》《最受歡迎電影》等等

## 入行前曾參與演出的廣告

### 2005年

  - 3月：清涼蒸餾水（廣告）
  - 6月：護舒寶（國內硬照廣告）
  - 7月：[KFC](../Page/KFC.md "wikilink")（電視廣告）、[加德士](../Page/加德士.md "wikilink")（平面廣告）、衣之纯服装（國內電視廣告）
  - 8月：Teenix（電視廣告）
  - 10月：脆得樂朱古力（電視廣告）
  - 11月：[勞工處](../Page/勞工處.md "wikilink")（電視廣告）、[滙豐銀行](../Page/滙豐銀行.md "wikilink")（電視廣告）、3手提電話（電視廣告，Part
    1）
  - 12月：[海洋公園](../Page/香港海洋公園.md "wikilink")（電視廣告）、撒隆巴斯（電視廣告）、3手提電話（電視廣告，Part
    2）

### 2006年

  - 3月：[泰國建築材料廣告](../Page/泰國.md "wikilink")（泰國廣告）
  - 5月：[保誠保險](../Page/保誠保險.md "wikilink")（電視廣告）
  - 8月：[諾基亞手提電話](../Page/諾基亞.md "wikilink")（平面廣告）、[麥當勞](../Page/麥當勞.md "wikilink")（電視廣告）
  - 12月：禁煙廣告、清熱酷（電視廣告）

## 入行前模特兒經驗

### 2005年

  - 3月：E時E地（[有線電視節目](../Page/有線電視.md "wikilink")）
  - 4月：Magic East Video（Video model）
  - 6月：熱車雜誌（雜誌訪問）、助助身茶（平面廣告）
  - 9月：[東Touch](../Page/東Touch.md "wikilink")（Fashion supplyment）
  - 10月：[左麟右李](../Page/左麟右李_\(電影\).md "wikilink")、[孫燕姿簽名會及](../Page/孙燕姿.md "wikilink")[萬聖節活動](../Page/萬聖節.md "wikilink")

### 2006年

  - 3月：[生日快樂](../Page/生日快樂_\(電影\).md "wikilink")

## 曾參與的演出

### 2004年

  - 10月：[銅鑼灣](../Page/銅鑼灣.md "wikilink")（勞工處攤位）
  - 12月：[屯門](../Page/屯門.md "wikilink")[屋邨街坊Show](../Page/屋邨.md "wikilink")、[星光大道星光新人Show](../Page/香港星光大道.md "wikilink")、“我本事強”記者招待會及開幕禮、
    [德福廣場](../Page/德福廣場.md "wikilink")（“想創遊樂唱”商場Show）

### 2005年

  - 1月：[鑽石山](../Page/鑽石山.md "wikilink")[龍蟠苑](../Page/龍蟠苑.md "wikilink")（街坊Show）、[德福廣場](../Page/德福廣場.md "wikilink")（[海嘯籌款](../Page/2004年印度洋大地震.md "wikilink")）
  - 4月：[海港城鋼琴演奏表演](../Page/海港城.md "wikilink")
  - 7月：[尖沙咀](../Page/尖沙咀.md "wikilink")[信和集團樓盤展銷會](../Page/信和集團.md "wikilink")（鋼琴演奏）
  - 8月：[愉景新城](../Page/愉景新城.md "wikilink")（[Delifrance活動司儀](../Page/Delifrance.md "wikilink")）、[觀塘](../Page/觀塘.md "wikilink")[apm鋼琴演奏表演](../Page/apm.md "wikilink")
  - 9月：[屯門](../Page/屯門.md "wikilink")[恆順園](../Page/恆順園.md "wikilink")（[中秋節晚會活動唱歌表演](../Page/中秋節.md "wikilink")）
  - 12月：[屯門屋邨聖誕街坊Show](../Page/屯門.md "wikilink")

### 2006年

  - [無綫電視](../Page/無綫電視.md "wikilink")[勁歌金曲鋼琴手及和音](../Page/勁歌金曲.md "wikilink")
  - 1月：[荃灣大會堂露天劇場青協唱作人表演](../Page/荃灣大會堂.md "wikilink")
  - 5月：[理工大學青少年十八成人禮](../Page/香港理工大學.md "wikilink")、[新世紀廣場Live](../Page/新世紀廣場.md "wikilink")
    show
  - 6月：[九龍灣](../Page/九龍灣.md "wikilink")[國際展貿中心](../Page/國際展貿中心.md "wikilink")[關心妍音樂會](../Page/關心妍.md "wikilink")（和音）
  - 7月：[迦密主恩中學](../Page/迦密主恩中學.md "wikilink")（試後活動表演）
  - 8月：[上水區歌唱比賽](../Page/上水.md "wikilink")
  - 9月：[朗豪坊鋼琴演奏表演](../Page/朗豪坊.md "wikilink")

### 2007年

  - 4月：[迦密主恩中學](../Page/迦密主恩中學.md "wikilink")（擔任音樂比賽評判）
  - 11月：[新城唱好陳曉東Love](../Page/新城電台.md "wikilink") Moments音樂會表演嘉賓／琴手
  - 12月：VEGA mini concert、[無綫音樂台陳曉東live](../Page/無綫電視.md "wikilink")
    house

### 2008年

  - 4月：[迦密主恩中學](../Page/迦密主恩中學.md "wikilink")
  - 2008、2009年yeah show演唱嘉賓

### 2009年

  - 1月23日：[將軍澳官立中學](../Page/將軍澳官立中學.md "wikilink")[Singing Contest
    五校聯校合歌唱比賽](../Page/Singing_Contest_五校聯校合歌唱比賽.md "wikilink")（擔任音樂比賽評判）
  - 4月：《我們的華星時代》舞台劇（主演：文恩澄、周國賢、劉浩龍、王若琪）
  - Ice Watch 廣告
  - 化粧品牌 Shiru 廣告
  - 環保促進會 gogreen 環保大使
  - 盞記廣告（配樂）
  - 《Season得寵時尚》專欄寫作

### 2010年

  - 《Season得寵時尚》專欄寫作
  - 創世電視節目主持
  - 各大小公司 function MC（日清集團、環保促進會、少年警訊等等）
  - 香港電台節目《文化長河》第一及二輯主持

### 2011年

  - 《秒速18米》舞台劇

### 2012年

  - 4月：[迦密主恩中學](../Page/迦密主恩中學.md "wikilink")（擔任音樂比賽評判）
  - 《風之后》舞台劇
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》

### 2013年

  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[女人多自在5](../Page/女人多自在5.md "wikilink")》
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》

### 2014年

  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[性本善](../Page/性本善.md "wikilink")》
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》

### 2015年

  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[卓越教室](../Page/卓越教室.md "wikilink")》
  - [微電影主要演員](../Page/微電影.md "wikilink")《[代代有愛](../Page/代代有愛.md "wikilink")》系列
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》

### 2016年

  - [網絡電影主要演員](../Page/網絡電影.md "wikilink")《[蒲出去3](../Page/蒲出去3.md "wikilink")》

## 作曲作品

### 2005年

• 新城電台足球節目配樂制作 • 新城電台廣播劇《上海101》配樂製作 • 勞工處《我本事強》主題曲

### 2006年

  - 許志安《孫果與了因》

### 2007年

  - 文恩澄《同床異枕》

### 2008年

  - 陳曉東、文恩澄《愛情第二》
  - 文恩澄《新約》、《離別曲》、《誤交好友》
  - 文恩澄《同床異枕（劇場版）》（編曲）
  - Unidex《1914》
  - Unidex《會計會計》
  - 陳曉東 《唇彩》
  - 日清春雨粉絲湯系列廣告歌（廣告版作詞）

### 2009年

  - 環保促進會主題曲《為藍天奮鬥》

• 盞記電視廣告《月亮明白了》系列配樂製作

### 2010年

  - 《Yeah Show褔音專輯》作曲、主唱

` 《愛在潮流外》（唱）、《心仍在跳》（曲）`

• 舞台劇《難兄難弟》CD製作監製

### 2011年

  - 文恩澄 -《怪獸愛侶》
  - 文恩澄 - 《幸福大曬》

### 2012年

  - 文恩澄、陳國峰 - 《地獄式戀愛》
  - 麥貝夷 - 《特別鳴謝》

### 2013年

  - [馮凱淇](../Page/馮凱淇.md "wikilink") - 《看得開的女人》
  - 第25屆澳門煙花比賽滙演全球廣告配樂
  - 牌照事務處持牌賓館廣告配樂
  - TVB劇集配樂
  - TVB歌曲《忘我》（歌名暫定）

### 2014年

  - TVB劇集《單戀雙城》主題曲 [林夏薇](../Page/林夏薇.md "wikilink") - 《很想討厭你》

## 音樂

### 2008年

  - 《[Rannes個人專輯](../Page/Rannes.md "wikilink")》（推出日期：9月9日）
  - 國內電影《回家的路》片尾曲主唱
  - Yeah show音樂專輯《愛在潮流外》（唱）、《心仍在跳》（曲）

## 曾參與MV

### 2008年

  - 文恩澄 - 同床異枕
  - 文恩澄 - 娛人娛己（TVB版）
  - 文恩澄 - 娛人娛己
  - [吳彤](../Page/吳彤.md "wikilink") - 天空一片藍
  - [陳曉東](../Page/陳曉東.md "wikilink") - 易來易碎
  - [陳曉東](../Page/陳曉東.md "wikilink")、文恩澄 - 愛情第二

### 2010年

  - [泳兒](../Page/泳兒.md "wikilink")《Forever Friends》MV 客串
  - 台灣樂隊 Over Dose MV 女主角

### 2011年

  - 文恩澄 - 《怪獸愛侶》
  - 文恩澄 - 《幸福大曬》
  - 新城電台《新香蕉JUNIOR》主題曲
  - 商業電台節目《AFTERNOON D》歌曲《吹吹風吹吹水》

### 2012年

  - 文恩澄、陳國峰 - 《地獄式戀愛》
  - 文恩澄 《水孩子》

## 主持

### 2005年

[愉景新城](../Page/愉景新城.md "wikilink")（[Delifrance活動司儀](../Page/Delifrance.md "wikilink")）

### 2008年

  - RoadShow路訊通鈴聲榜主持
  - 網上電台節目《澄天Pet Pet》嘉賓主持
  - 網上電台節目《動漫薦場》嘉賓主持
  - 網上電台節目《港澄港樂》嘉賓主持
  - 無線電視收費台節目《Boom部落格》嘉賓主持

### 2009年

  - [香港電台](../Page/香港電台.md "wikilink")[文化長河](../Page/文化長河.md "wikilink")-[非物質文化遺產系列主持](../Page/非物質文化遺產系列.md "wikilink")
  - [Namco Wonder
    Park夾公仔達人2009主持](../Page/Namco_Wonder_Park夾公仔達人2009.md "wikilink")

### 2010年

  - 創世電視節目主持
  - 各大小公司 function MC（日清集團、環保促進會、少年警訊等等）

### 2011年

  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[文化長河－鐵道行](../Page/文化長河－鐵道行.md "wikilink")》
  - [新城電台](../Page/新城電台.md "wikilink")《[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")》嘉賓主持

### 2012年

  - [新城電台](../Page/新城電台.md "wikilink")《[吳彤人敢唱](../Page/吳彤人敢唱.md "wikilink")》主持
  - [新城電台](../Page/新城電台.md "wikilink")《[Buskingdom](../Page/Buskingdom.md "wikilink")》主持
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》主持

### 2015年

  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[卓越教室](../Page/卓越教室.md "wikilink")》
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《[教育電視](../Page/教育電視.md "wikilink")》主持

## 代言

### 2008年

  - [日清春雨粉絲湯系列代言人](../Page/日清食品.md "wikilink")

## 影片幕後製作

  - 《幸褔大曬》MV
  - 《水孩子》MV
  - 微電影 - 《預見》
  - 微電影 - 《溝女？唔駛用劍嘅》
  - 微電影 - LogON 《天使的禮物》
  - 微電影 － 《三婚愛﹣第一步戀曲》 婚之戀人主題微電影
  - 微電影 － 《三婚愛﹣第二步戀曲》 婚之戀人主題微電影
  - 微電影 －SONY Z 宣傳
  - 廣告客戶 － PIONEER、牌照事務處、房屋署、康文署
  - 宣傳短片客戶－滙豐銀行、DR REBORN、TESLA、UNWIRE、EZEECUBE、MARRIOTT等等
  - 節目－ESDLIFE（真人SHOW）、UNWIRE

<!-- end list -->

  - 還有大量電視廣告、微電影、MV 及商業影片不能盡錄

## 参考资料

## 外部連結

  - [文恩澄Facebook專頁](http://www.facebook.com/rannesmanfanspage)
  - [文恩澄官方論壇](http://www.rannesman.com)
  - [文恩澄微博](http://www.weibo.com/u/1718849393)
  - [文恩澄blog](http://hk.myblog.yahoo.com/rannes_man)

[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:創作歌手](../Category/創作歌手.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:文姓](../Category/文姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港飲食界人士](../Category/香港飲食界人士.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:迦密主恩中學校友](../Category/迦密主恩中學校友.md "wikilink")
[Category:香港泛民主派人士](../Category/香港泛民主派人士.md "wikilink")

1.