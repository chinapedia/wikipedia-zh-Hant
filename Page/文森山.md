[Vinson-Massif.jpg](https://zh.wikipedia.org/wiki/File:Vinson-Massif.jpg "fig:Vinson-Massif.jpg")
从太空拍摄的文森山\]\]

**文森山**（），是[南极洲的最高峰](../Page/南极洲.md "wikilink")，以致力於推動南極探險的美國國會議員[卡爾·文森為此峰命名](../Page/卡爾·文森.md "wikilink")。於2004年以[GPS技術最新測得海拔](../Page/GPS.md "wikilink")4,892公尺（16,050英尺），山頂距[南极點约](../Page/南极.md "wikilink")1,200公里，山體长21公里，宽13公里。

## 外部链接

  - [Vinson Massif on
    TierraWiki.org](http://www.tierrawiki.org/wiki/index.php?title=Vinson_Massif)
  - [Mountain of Ice](http://www.pbs.org/wgbh/nova/vinson/)

[Category:南极洲山峰](../Category/南极洲山峰.md "wikilink")
[Category:南极洲地理之最](../Category/南极洲地理之最.md "wikilink")
[Category:七大洲最高峰](../Category/七大洲最高峰.md "wikilink")