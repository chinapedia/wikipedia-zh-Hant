**文武王**（）；姓**金**名**法敏**，是[新罗第三十代君主](../Page/新罗.md "wikilink")（公元661年至681年在位），在位期間達成了[朝鮮半島的三國統一](../Page/朝鮮半島.md "wikilink")。

[武烈王長子](../Page/武烈王.md "wikilink")，生母[文明王后為名將](../Page/文明王后.md "wikilink")[金庾信之妹](../Page/金庾信.md "wikilink")，正室[慈儀王后](../Page/慈儀王后.md "wikilink")，波珍飡[善品之女](../Page/善品.md "wikilink")。650年曾遠赴[唐朝](../Page/唐朝.md "wikilink")，獲得「大府卿」之官位。654年成為新羅[兵部令](../Page/兵部令.md "wikilink")。655年立爲太子，武烈王七年（660年）領兵船一百艘与[唐朝大将](../Page/唐朝.md "wikilink")[苏定方一起围攻](../Page/苏定方.md "wikilink")[百濟都城](../Page/百濟.md "wikilink")，攻灭百濟。661年六月武烈王薨逝，法敏即位。此后金法敏联合唐军多次攻取百濟諸城，攻破百濟殘衆。六年（666年）文武王欲滅[高句麗](../Page/高句麗.md "wikilink")，向唐請兵，唐派[李勣伐高句麗](../Page/李勣.md "wikilink")，668年九月唐兵联合新罗攻克[平壤城](../Page/平壤.md "wikilink")，高句麗王高臧投降，高句麗亡。

灭亡高句丽后，唐朝在朝鲜半岛设置[安东都护府作为统治机构](../Page/安东都护府.md "wikilink")。但遭到了新罗的反击，并最终导致[新罗与唐朝的战争](../Page/罗唐战争.md "wikilink")。在与唐朝的战争中，文武王联合并扶植了[朝鲜半岛上的原](../Page/朝鲜半岛.md "wikilink")[高句丽和](../Page/高句丽.md "wikilink")[百济的反唐势力](../Page/百济.md "wikilink")，包括[剑牟岑扶植](../Page/剑牟岑.md "wikilink")[安舜建立的高句丽复兴政权](../Page/安舜.md "wikilink")。为对付文武王，[唐高宗扶植文武王的弟弟](../Page/唐高宗.md "wikilink")[金仁问为](../Page/金仁问.md "wikilink")[新罗君主](../Page/新罗.md "wikilink")，并派[刘仁轨领兵攻打文武王](../Page/刘仁轨.md "wikilink")。676年，由于新罗的攻势，唐朝在与[吐蕃战争的失败和](../Page/吐蕃.md "wikilink")[高句丽与](../Page/高句丽.md "wikilink")[百济遗民从未间断的反唐运动](../Page/百济.md "wikilink")，唐朝退出大同江以南土地。

文武王统治[新罗](../Page/新罗.md "wikilink")20余载，直至681年病故。文武王在病故前将王位传让给了其子[神文王](../Page/神文王.md "wikilink")。按照文武王的遗愿，神文王将文武王的遗体在[东海火化](../Page/朝鲜东海.md "wikilink")，以使文武王的灵魂变成海上蛟龙保护[新罗免受外敌的入侵](../Page/新罗.md "wikilink")。

## 新罗文武王陵碑

在[韩国](../Page/韩国.md "wikilink")[庆州](../Page/庆州.md "wikilink")，全称《大唐乐浪郡王[开府仪同三司](../Page/开府仪同三司.md "wikilink")[上柱国新罗文武王陵之碑](../Page/上柱国.md "wikilink")》，永淳元年（682年）世次壬午七月壬辰朔廿五日景辰建碑，碑文载新罗金氏王族是“秺侯（[金日磾](../Page/金日磾.md "wikilink")）祭天之胤”，十五代祖星汉王\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]\[7\]。

## 脚注

<references/>

## 相关条目

  - [罗唐战争](../Page/罗唐战争.md "wikilink")

  - （[韓國海軍](../Page/韓國海軍.md "wikilink")）

{{-}}  |-style="text-align: center; background: \#FFE4E1;"
|align="center" colspan="3"|**文武王**

[Category:統一新羅君主](../Category/統一新羅君主.md "wikilink")
[Category:681年逝世](../Category/681年逝世.md "wikilink")
[Category:唐朝太府卿](../Category/唐朝太府卿.md "wikilink")
[Category:唐朝郡王](../Category/唐朝郡王.md "wikilink")
[Category:庆州金氏](../Category/庆州金氏.md "wikilink")

1.
2.
3.
4.  [경주 사천왕사（寺） 사천왕상（四天王像） 왜 4개가
    아니라 3개일까](https://web.archive.org/web/20141230090440/http://news.chosun.com/site/data/html_dir/2009/02/26/2009022601873.html)
    조선일보 2009.02.27자
5.  [2부작 <문무왕릉비의 비밀> - 제1편: 신라 김씨왕족은 흉노（匈奴）의
    후손인가?](http://www.kbs.co.kr/1tv/sisa/tracehistory/vod/review/1556800_28170.html),
    《KBS 역사추적》, 2008년 11월 22일 방송.
6.  [2부작 <문무왕비문의 비밀> - 제2편: 왜 흉노(匈奴)의 후예라고
    밝혔나?](http://www.kbs.co.kr/1tv/sisa/tracehistory/vod/review/1557643_28170.html),
    《KBS 역사추적》, 2008년 11월 29일 방송.
7.  [（채널돋보기） 신라 김씨 왕족은 흉노의
    후손일까](http://www.imaeil.com/sub_news/sub_news_view.php?news_id=53308&yy=2008)
    매일신문 2008.11.21