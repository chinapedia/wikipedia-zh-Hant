[CCHK.jpg](https://zh.wikipedia.org/wiki/File:CCHK.jpg "fig:CCHK.jpg")
**文理書院（香港）**（），校名COGNITIO
一詞是拉丁文，是有不斷追尋知識的意思，而中文校名「文理」一詞的意思為提倡通識教育，文科，理科同樣並重，故名「文理」。文理書院（香港）是[香港一間全日制政府津貼的男女](../Page/香港.md "wikilink")[文法中學](../Page/文法中學.md "wikilink")，位於[港島](../Page/港島.md "wikilink")[東區柴灣萃文道四號](../Page/東區_\(香港\).md "wikilink")。文理書院（香港）為[文理書院的於](../Page/文理書院.md "wikilink")1962年所創立的第一所學校，而第二所分校是於1970年開辦，位於[九龍](../Page/九龍.md "wikilink")[黃大仙區](../Page/黃大仙區.md "wikilink")[新蒲崗景福街](../Page/新蒲崗.md "wikilink")96號的[文理書院
(九龍)](../Page/文理書院_\(九龍\).md "wikilink")。中學所屬組別：2B

## 歷史及建築特色

文理書院創校早期是一所私立學校，辦學團體是[文理英文書院（1979）有限公司](../Page/文理英文書院（1979）有限公司.md "wikilink")，在1962年由一群香港大學畢業生創立，最初成立的校址位於銅鑼灣金殿大廈2樓，即今日港鐵[天后站上蓋](../Page/天后站.md "wikilink")[栢景臺對面](../Page/栢景臺.md "wikilink")，鄰近[維多利亞公園](../Page/維多利亞公園.md "wikilink")，其後於1967年遷往灣仔萬茂徑21號，後來學校繼續擴展，並於1970年開辦九龍分校名為[文理書院（九龍）](../Page/文理書院（九龍）.md "wikilink")。

書院於1974年自建校舍，遷往[柴灣](../Page/柴灣.md "wikilink")[萃文道](../Page/萃文道.md "wikilink")4號現址，因為資金不足因此校舍要分期興建，1974年只能完成第一期的部份課室、辦公室、實驗室及特別室工程，而港九文理在1982年轉作政府津貼中學後，便向政府申請興建柴灣校舍未完成的部份，但政府以財政為理由只批出資助興建課室及特別室，後來於1987年完成了第二期5層高之增建課室工程，但一所標準學校基本要有的禮堂要經過多年的折騰到1996年11月才能獲批正式動土。而第三期則為4層高連地庫一層的船型禮堂（方樹泉堂）禮堂，座落於原有的籃球場之上。因此，學校需要在禮堂上蓋興建一個標準籃球場。建築物的船型外型是喻意年青人的成長和發展，正如通過茫茫大海，絕對需要一艘堅固的船隻支持以渡過無法預測的風浪及人生中的高低起跌。新禮堂的設計正象徵著那艘帶領年青人渡過充滿挑戰的前路的船隻。而前方的燈塔形建築物，實為消防泵房及水缸，喻意文理老師為學生的人生旅程作出導航指引，正如燈塔給予船隻方向般，保護船隻有安全的旅程。禮堂內部刻有著名書法家[余寄撫先生所贈的對聯](../Page/余寄撫.md "wikilink")「弘揚五育，愛延孺子；勵學專志，回饋社群。」上聯是勉勵老師的，而下聯則是勉勵學生和校友。此建築是由本港著名建築師[關永康校友設計](../Page/關永康.md "wikilink")，在1998年落成啟用，學校亦從此結束了長期借用其他地方的禮堂舉行活動的日子。在2001年，該校更完成最後一期第四期7層高的張永賢教學樓工程，該期工程由林陳簡建築師事務所負責設計。

## 辦學宗旨

五育並重發展潛能，培育熱愛祖國，關心社會，有理想識見，勇於服務社群的青年。通過校本管理，全校參與，與校友、家長、社會機構緊密合作發展全人教育。

## 校訓

文理書院以「博學、審問、慎思、明辨、篤行\[1\]」為校訓。原文出自儒家經書《禮. 中
庸》第二十章:「博學之，審問之，慎思之，明辨之，篤行之。」國父[孫中山先生曾於](../Page/孫中山.md "wikilink")1924年11月11日在[廣東大學](../Page/廣東大學.md "wikilink")(今廣東省廣州市[中山大學](../Page/中山大學.md "wikilink"))舉行成立典禮時親筆提寫「博學、審問、慎思、明辨、篤行」，後來成為廣東省廣州市的[中山大學](../Page/中山大學.md "wikilink")\[2\]，臺灣的[國立中山大學](../Page/國立中山大學.md "wikilink")\[3\]的校訓。

## 信念和使命

  - 五育並重，發展潛能，培育熱愛祖國、關心社會、有理想識見、勇於服務社群的青年。
  - 通過校本管理、全校參與、與校友、家長、社會機構緊密合作，發展全人教育。

## 簡介

文理書院每年都均會舉行一次水運會以及一次陸運會(分初賽以及決賽)，每隔五年亦會與文理書院(九龍）舉行聯校陸運會.

## 學校設施

[資訊科技教學設施包括](../Page/資訊科技.md "wikilink")[電腦室](../Page/電腦室.md "wikilink")、[電腦](../Page/電腦.md "wikilink")200餘台、[手提電腦](../Page/手提電腦.md "wikilink")60部、平板電腦約35部；另有[語言室](../Page/語言室.md "wikilink")、[校園電視台](../Page/校園電視台.md "wikilink")、[輔導室](../Page/輔導室.md "wikilink")、[禮堂](../Page/禮堂.md "wikilink")、[圖書館](../Page/圖書館.md "wikilink")、[飯堂等](../Page/飯堂.md "wikilink")；全部課室及特別室均安裝[冷氣](../Page/冷氣.md "wikilink")、[投影機及](../Page/投影機.md "wikilink")[電腦](../Page/電腦.md "wikilink")。
校舍內的設施如下：

<table style="width:7%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>28個<a href="../Page/課室.md" title="wikilink">課室</a></li>
<li>1個<a href="../Page/生物.md" title="wikilink">生物實驗室</a></li>
<li>1個<a href="../Page/物理.md" title="wikilink">物理實驗室</a></li>
<li>1個<a href="../Page/化學.md" title="wikilink">化學實驗室</a></li>
<li>1個<a href="../Page/科學實驗.md" title="wikilink">科學實驗室</a></li>
<li>1個語言室</li>
<li>1個電腦輔助學習室</li>
<li>1個<a href="../Page/英語活動.md" title="wikilink">英語活動中心</a></li>
<li>1個<a href="../Page/音樂.md" title="wikilink">音樂室</a></li>
<li>1個<a href="../Page/社工.md" title="wikilink">社工室</a></li>
<li>1個校園電視台</li>
</ul></td>
<td><ul>
<li><a href="../Page/美術.md" title="wikilink">美術室</a></li>
<li><a href="../Page/食物.md" title="wikilink">食物部</a></li>
<li><a href="../Page/禮堂.md" title="wikilink">禮堂</a></li>
<li>訓導及輔導室</li>
<li>教職員、家長及校友聯誼室</li>
<li>會議室</li>
<li><a href="../Page/校務處.md" title="wikilink">校務處</a></li>
<li><a href="../Page/教員室.md" title="wikilink">教員室</a></li>
<li>籃球場</li>
<li>攀石牆</li>
<li>有蓋操場</li>
<li><a href="../Page/圖書館.md" title="wikilink">圖書館</a></li>
<li>學生活動中心</li>
<li>新翼大樓</li>
</ul></td>
</tr>
</tbody>
</table>

## 香港傑出學生選舉

直至2018年(第33屆)，在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 著名校友

  - [陳英麟](../Page/陳英麟.md "wikilink"): 香港立法局議員 (1983年 - 1991年)；香港區議會議員
    (1982年 - 1991年)
  - [余承章](../Page/余承章.md "wikilink")：香港特別行政區資深大律師
  - [余志穩](../Page/余志穩.md "wikilink")：前[社會福利署署長](../Page/社會福利署.md "wikilink")（1970年中七）
  - [呂愛蓮](../Page/呂愛蓮.md "wikilink")：香港大學李嘉誠醫學院病理學系講座教授及陸佑基金教授席-病理學
  - [關永康](../Page/關永康.md "wikilink")：本港著名建築師，關黃建築師事務所創辦人，主要作品包括文理書院（香港）船型禮堂、中環聖約翰大廈、淺水灣[影灣園](../Page/影灣園.md "wikilink")、北角港運城、灣仔循道衛理總堂及九龍站[漾日居](../Page/漾日居.md "wikilink")
  - [鄧特希](../Page/鄧特希.md "wikilink")：前香港無綫電視劇集監製（1979年中五）
  - [王貽興](../Page/王貽興.md "wikilink")：香港新生代作家、作詞人及主持人
  - [徐子見](../Page/徐子見.md "wikilink")：[東區區議會](../Page/東區區議會.md "wikilink")（漁灣選區）區議員
  - [譚玉瑛](../Page/譚玉瑛.md "wikilink")：香港著名節目主持（1979年中五）
  - [鄭淇德](../Page/鄭淇德.md "wikilink")：醫療專業人士協會前副主席、榮譽秘書長
  - [陳淑媛](../Page/陳淑媛_\(乒乓球運動員\).md "wikilink")：前香港乒乓球運動員

## 注釋

## 參考

## 外部連結

  - [文理書院](http://www.cognitiohk.edu.hk/)
  - [LifeinHK:文理書院](http://www.lifein.hk/Education/SecondarySchDet.aspx?id=581&name=文理書院)

[category:柴灣](../Page/category:柴灣.md "wikilink")

[C](../Category/香港東區中學.md "wikilink")
[Category:1962年創建的教育機構](../Category/1962年創建的教育機構.md "wikilink")

1.
2.  [1](http://gjs.sysu.edu.cn/zsdxxs/szsyzsdx/9833.htm)
3.