[Wen_Xiang.jpg](https://zh.wikipedia.org/wiki/File:Wen_Xiang.jpg "fig:Wen_Xiang.jpg")

**文祥**（；），[瓜爾佳氏](../Page/瓜爾佳氏.md "wikilink")，字**博川**，號**子山**，[盛京](../Page/盛京.md "wikilink")[正紅旗人](../Page/正紅旗.md "wikilink")，清代晚期的政治人物，[自強運動的重要領導人之一](../Page/自強運動.md "wikilink")。

## 生平

清[道光二十五年](../Page/道光.md "wikilink")（1845年）中[進士](../Page/進士.md "wikilink")，入[工部當官](../Page/工部.md "wikilink")，[太平天國軍北伐時](../Page/太平天國.md "wikilink")，許多北京一帶的工匠逃走，他被調至在北京設立臨時的巡防處作事。咸豐四年（1854年）昇作工部[員外郎](../Page/員外郎.md "wikilink")，咸豐五年（1855年）昇[郎中](../Page/郎中.md "wikilink")，咸豐八年（1858年）昇為[內閣學士](../Page/內閣學士.md "wikilink")，同年以軍機大臣行走，次年正式昇為[軍機大臣](../Page/軍機大臣.md "wikilink")，[英法聯軍之役](../Page/英法聯軍之役.md "wikilink")，[咸豐帝逃往熱河](../Page/咸豐帝.md "wikilink")，文祥協助[桂良及](../Page/桂良.md "wikilink")[奕訢與各國進行交涉](../Page/奕訢.md "wikilink")，其後參與了[總理衙門的成立提案](../Page/總理衙門.md "wikilink")。咸豐十一年（1861年）的[辛酉政變中](../Page/辛酉政變.md "wikilink")，支持[東太后與](../Page/東太后.md "wikilink")[西太后](../Page/西太后.md "wikilink")，待[肅順等](../Page/肅順.md "wikilink")[顧命八大臣敗後](../Page/顧命八大臣.md "wikilink")，上奏提議[東太后與](../Page/東太后.md "wikilink")[西太后](../Page/西太后.md "wikilink")[兩宮垂簾](../Page/兩宮垂簾.md "wikilink")，其後受到兩位太后重用，死前一直擔任軍機大臣和總理衙門大臣之職，並曾兼任[都察院左都御史](../Page/都察院.md "wikilink")、工部尚書、吏部尚書、協辦大學士、大學士等職位。光緒二年（1876年）去世，諡**文忠**。

## 傳記

他的自傳《文忠公自訂年譜》收錄於《文文忠公事略》。\[1\]

## 評價

他被認為是勤勉正直的一位政治人物，為官廉潔，私下生活也很樸素，在晚年十多年中對於[自強運動的推行](../Page/自強運動.md "wikilink")，以及與西方的外交事務上扮演重要的角色。他在死前不久，光緒元年（1875年）上《密陈大计疏》指出[國會的重要](../Page/國會.md "wikilink")：“说者谓各国性近犬羊，未知政治，然其国中偶有动作，必由其[国主付](../Page/君主.md "wikilink")[上议院议之](../Page/上议院.md "wikilink")，所谓谋及[卿士也](../Page/卿士.md "wikilink")；付[下议院议之](../Page/下议院.md "wikilink")，所谓谋及[庶人也](../Page/庶人.md "wikilink")。议之可行则行，否则止，事事必合乎民情而后决然行之。”认为这样的制度，清帝国“势有难行，而义可采取”。这是中国最高领导层第一次议论吸取[民主制度的精神](../Page/民主制度.md "wikilink")，改进本国的施政。\[2\]

## 參考文獻

  - 《[清史稿](../Page/清史稿.md "wikilink")》[列傳一百七十三](../Page/:wikisource:zh:清史稿/卷386.md "wikilink")·文祥傳

## 外部連結

  - [中国宪政：曲折而凄惨的开篇](http://glxy.blogspot.com/2007/03/blog-post_11.html)
  - [(清)(瓜爾佳)文祥](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:0:mctauac:TM%3D%A4%E5%A4s)
    - [中央研究院](../Page/中央研究院.md "wikilink") - 歷史語言研究所 - 明清檔案工作室

[Category:諡文忠](../Category/諡文忠.md "wikilink")
[Category:洋務運動領袖](../Category/洋務運動領袖.md "wikilink")
[Category:軍機大臣](../Category/軍機大臣.md "wikilink")
[Category:總理各國事務衙門大臣](../Category/總理各國事務衙門大臣.md "wikilink")
[Category:前鋒營前鋒統領](../Category/前鋒營前鋒統領.md "wikilink")
[Category:清朝詹事府詹事](../Category/清朝詹事府詹事.md "wikilink")
[Category:清朝工部尚書](../Category/清朝工部尚書.md "wikilink")
[Category:清朝武英殿大學士](../Category/清朝武英殿大學士.md "wikilink")
[Category:步軍營步軍統領](../Category/步軍營步軍統領.md "wikilink")
[Category:清朝體仁閣大學士](../Category/清朝體仁閣大學士.md "wikilink")
[Category:文淵閣領閣事](../Category/文淵閣領閣事.md "wikilink")
[Category:內閣學士](../Category/內閣學士.md "wikilink")
[Category:清朝太僕寺少卿](../Category/清朝太僕寺少卿.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝協辦大學士](../Category/清朝協辦大學士.md "wikilink")
[Category:清朝三公](../Category/清朝三公.md "wikilink")
[Category:清朝禮部侍郎](../Category/清朝禮部侍郎.md "wikilink")
[Category:清朝戶部侍郎](../Category/清朝戶部侍郎.md "wikilink")
[Category:清朝吏部尚書](../Category/清朝吏部尚書.md "wikilink")
[W文](../Category/瓜爾佳氏.md "wikilink")
[Category:清朝左都御史](../Category/清朝左都御史.md "wikilink")
[Category:滿洲正紅旗人](../Category/滿洲正紅旗人.md "wikilink")
[Category:賜紫禁城騎馬](../Category/賜紫禁城騎馬.md "wikilink")
[Category:鑲黃旗滿洲都統](../Category/鑲黃旗滿洲都統.md "wikilink")
[Category:鑲白旗滿洲都統](../Category/鑲白旗滿洲都統.md "wikilink")

1.
2.