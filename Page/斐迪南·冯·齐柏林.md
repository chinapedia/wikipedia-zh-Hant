**斐迪南·冯·齐柏林**伯爵（，）。[德国](../Page/德国.md "wikilink")[贵族](../Page/贵族.md "wikilink")、[工程师和](../Page/工程师.md "wikilink")[飞行员](../Page/飞行员.md "wikilink")。出生于巴登大公国的[康斯坦茨](../Page/康斯坦茨.md "wikilink")（现属于德国[巴登-符腾堡州](../Page/巴登-符腾堡州.md "wikilink")）

他是人类航空史的重要人物之——他发明了[齐柏林飞艇](../Page/齐柏林飞艇.md "wikilink")。同时他还创建了齐柏林飞艇公司（Zeppelin
airship company）。

## 早年经历

斐迪南·冯·齐柏林于1838年7月8日出生在德国博登湖畔康斯坦茨的一个贵族世家。他的父亲[弗里德里希·冯·齐柏林](../Page/弗里德里希·冯·齐柏林.md "wikilink")（Friedrich
von
Zeppelin）是[巴登大公国的](../Page/巴登大公国.md "wikilink")[内廷总监](../Page/内廷.md "wikilink")。与其他贵族家庭的孩子一样，他从小接受私人教师的教育。小时候，他就对机械和新奇的技术表现出了极大的兴趣。1853年，他前往位于[斯图加特的综合科技学校上学](../Page/斯图加特.md "wikilink")。1855年，他前往[路德维希堡的军校上学](../Page/路德维希堡.md "wikilink")，随后入伍，开始了他的军人生涯。

## 参见

  - [飞艇](../Page/飞艇.md "wikilink")

[Z](../Category/德国企业家.md "wikilink")
[Z](../Category/德国发明家.md "wikilink")
[Z](../Category/德國飛行員.md "wikilink")
[Z](../Category/巴登-符騰堡人.md "wikilink")
[Z](../Category/德國貴族.md "wikilink")