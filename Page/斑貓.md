**斑貓**（[学名](../Page/学名.md "wikilink")：**）或稱**野貓**或**山貓**，是一種小型[貓科動物](../Page/貓科.md "wikilink")，原生於歐洲地區、[亞洲西部](../Page/亞洲.md "wikilink")，以及[非洲](../Page/非洲.md "wikilink")。斑貓會獵捕小型哺乳類、鳥類，或是其他體型相仿的動物。斑貓可分為多個分布於不同地域的[亞種](../Page/亞種.md "wikilink")，其中包含了[家貓](../Page/家貓.md "wikilink")（*Felis
silvestris catus*），而世界各地又有許多回歸野外的家貓。

一項調查顯示，現存家貓是源自1萬年前生活於[近東地區的的一部份斑貓](../Page/近東.md "wikilink")\[1\]。此外，與斑貓最接近的物種是[沙漠貓](../Page/沙漠貓.md "wikilink")（*Felis
margarita*）。

## 亞種

根據2007年的DNA分析，斑貓之中（除了家貓以外）共有5個[亞種](../Page/亞種.md "wikilink")：\[2\]

  - [歐洲野貓](../Page/歐洲野貓.md "wikilink") *Felis silvestris silvestris*
    [歐洲及](../Page/歐洲.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")。
  - [非洲野貓](../Page/非洲野貓.md "wikilink") *Felis silvestris lybica*
    [北非地帶](../Page/北非.md "wikilink")、[中東](../Page/中東.md "wikilink")、[西亞至](../Page/西亞.md "wikilink")[鹹海之間](../Page/鹹海.md "wikilink")。
  - [南非野猫](../Page/南非野猫.md "wikilink") *Felis silvestris cafra*
    [南非洲](../Page/南非洲.md "wikilink")。
  - [亞洲野貓](../Page/亞洲野貓.md "wikilink") *Felis silvestris ornata*
    [巴基斯坦](../Page/巴基斯坦.md "wikilink")、[印度東北](../Page/印度.md "wikilink")、[中國北部與](../Page/中國.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")。
  - [中國野貓](../Page/中國野貓.md "wikilink") *Felis silvestris bieti*
    [中國](../Page/中國.md "wikilink")，目前還是一個獨立的物種，學名*Felis bieti*。

此外較早的文獻中還有以下的分類方式：

  - 非洲
      - *Felis silvestris cafra* 南非
      - *Felis silvestris foxi* 西非
      - *Felis silvestris griselda* 中非
      - *Felis silvestris lybica* 北非
      - *Felis silvestris ocreata* 中東非
      - *Felis silvestris mellandi* 中西非

<!-- end list -->

  - 亞洲
      - *Felis silvestris caudata* 裏海地區
      - *Felis silvestris ornata* 印度與伊朗
      - *Felis silvestris bieti* 中國

<!-- end list -->

  - 歐洲
      - *Felis silvestris cretensis* 克里特，已滅絕，但仍有零星的目擊紀錄
      - *Felis silvestris caucasica* 高加索山與土耳其
      - *Felis silvestris grampia* 蘇格蘭西部與北部
      - *Felis silvestris jordansi*
        [巴利阿里群島](../Page/巴利阿里群島.md "wikilink")
      - *Felis silvestris reyi* 科西嘉島，可能已滅絕
      - *Felis silvestris silvestris* 歐洲

<!-- end list -->

  - 地區未知
      - *Felis silvestris chutuchta*
      - *Felis silvestris gordoni*
      - *Felis silvestris haussa*
      - *Felis silvestris iraki*
      - *Felis silvestris nesterovi*
      - *Felis silvestris rubida*
      - *Felis silvestris tristrami*
      - *Felis silvestris ugandae*
      - *Felis silvestris vellerosa*

## 參考文獻

## 外部連結

  - [*Felis
    silvestris*:](https://web.archive.org/web/20070930181250/http://www.lioncrusher.com/animal.asp?animal=72)
    three distinct populations
  - [*IUCN/SSC Cat Specialist Group - Cat Species
    Information*](https://web.archive.org/web/20080222164316/http://lynx.uio.no/lynx/catsgportal/cat-website/20_cat-website/home/index_en.htm)
  - [Digimorph.org:](http://digimorph.org/specimens/Felis_sylvestris_lybica/female/)
    3D computed tomographic (CT) animations of male and female African
    wild cat skulls

[Category:貓屬](../Category/貓屬.md "wikilink")

1.

2.