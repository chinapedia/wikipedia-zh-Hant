《**斑馬在線**》（[英文](../Page/英文.md "wikilink")：**Zebra
Online**）是[香港](../Page/香港.md "wikilink")[亞洲電視的](../Page/亞洲電視.md "wikilink")[清談節目](../Page/清談節目.md "wikilink")，2007年10月8日起逢星期一至五晚上22:30於亞洲電視[本港台播出](../Page/本港台.md "wikilink")，於2008年2月11日至3月23日期間停播。主要暢談[金庸筆下的武俠世界](../Page/金庸.md "wikilink")，借古論今。

《斑馬在線》由[陶傑和](../Page/陶傑.md "wikilink")[劉天賜主持](../Page/劉天賜.md "wikilink")，通过[金庸小说讨论](../Page/金庸小说.md "wikilink")[社会](../Page/社会.md "wikilink")[哲学](../Page/哲学.md "wikilink")，邀請名人擔任節目的嘉賓，例如[林燕妮](../Page/林燕妮.md "wikilink")、[倪匡等](../Page/倪匡.md "wikilink")。此外，節目亦尋訪各大院校的青年[學生](../Page/學生.md "wikilink")，參與節目，以新一代的見解角度，探索[金庸筆下的](../Page/金庸.md "wikilink")[武俠奧秘](../Page/武俠.md "wikilink")。

在2007年11月13日播出以「[漢奸](../Page/漢奸.md "wikilink")」為主題的一集，[中國大陸部分地區被插播其他節目](../Page/中國大陸.md "wikilink")。

在2007年12月24日及12月25日播出的兩集，邀請嘉賓[李純恩](../Page/李純恩.md "wikilink")、[張寶華及](../Page/張寶華.md "wikilink")[張家瑩到](../Page/張家瑩.md "wikilink")[中環](../Page/中環.md "wikilink")[鏞記酒家與主持品嚐](../Page/鏞記酒家.md "wikilink")「射雕英雄宴」。

《斑馬在線》原本是在2008年3月3日復播，但因配合播放《[台海和平與戰爭](../Page/台海和平與戰爭.md "wikilink")》。所以延遲在2008年3月24日復播，主題則轉為討論世界各地的文化風情。

## 外部連結

  - [亞洲電視節目《斑馬在線》官方網站](https://web.archive.org/web/20071029210627/http://www.hkatv.com/v3/infoprogram/07/zebraonline/)

[Category:亞洲電視節目](../Category/亞洲電視節目.md "wikilink")
[Category:亞洲電視清談節目](../Category/亞洲電視清談節目.md "wikilink")
[Category:2007年亞洲電視節目](../Category/2007年亞洲電視節目.md "wikilink")
[Category:2008年亞洲電視節目](../Category/2008年亞洲電視節目.md "wikilink")