**斑鳍鲉**（[学名](../Page/学名.md "wikilink")：）为[鲉科](../Page/鲉科.md "wikilink")[鲉属的](../Page/鲉属.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")，俗名石降。分布于[朝鲜](../Page/朝鲜.md "wikilink")、[日本以及沿海等](../Page/日本.md "wikilink")，属于近海底层小型鱼类。其常生活于由潮间带至深水的沙底海区或岩礁附近。该物种的模式产地在長崎。

**斑鰭鮋**又稱**絡鰓石狗公**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目](../Page/鮋形目.md "wikilink")[鮋科的一](../Page/鮋科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[日本](../Page/日本.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣北部等海域](../Page/台灣.md "wikilink")。

## 深度

水深80\~200公尺。

## 特徵

頭部有發達的皮膜，眶前骨下部最後一棘向後下方走，眶前骨下緣之最後一棘以上無棘。身體有明顯之斑紋，雄魚背鰭第四與第五棘之間有一明顯之黑斑，各鰭密佈不規則深金黃色斑點，全身有一些細小黑褐色斑點。鰭尖皆有毒，須小心刺傷。體長可達30公分。

## 生態

本種主要棲息於沿岸之沙泥質海底、珊瑚礁，為稀有魚種，以[甲殼類](../Page/甲殼類.md "wikilink")、[魚類為主食](../Page/魚類.md "wikilink")。

## 經濟利用

食用魚，一般煮味增湯食用。

## 參考資料

[台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[neglecta](../Category/鲉属.md "wikilink")
[Category:食用鱼](../Category/食用鱼.md "wikilink")