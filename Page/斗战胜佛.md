**鬥戰勝佛**\[1\]，[三十五佛之一](../Page/佛陀名号列表.md "wikilink")\[2\]。鬪戰勝佛一名真實出現於多本佛經，如《[佛說佛名經](../Page/佛說佛名經.md "wikilink")》、《》、《[大方廣佛華嚴經](../Page/大方廣佛華嚴經.md "wikilink")》的〈入不思議解脫境界普賢行願品〉\[3\]等。

《[觀虛空藏菩薩經](../Page/觀虛空藏菩薩經.md "wikilink")》記載[虛空藏菩薩的如意珠中就能映現三十五佛](../Page/虚空藏菩萨.md "wikilink")，鬪戰勝佛則為三十五佛之一。\[4\]

## 民俗文化

神怪小说《[西遊記](../Page/西游记.md "wikilink")》中主要角色[孫悟空於結局裡成佛的佛號](../Page/孙悟空.md "wikilink")，就取材自该尊佛。

## 参考註释

[Category:佛](../Category/佛.md "wikilink")

1.  古文中“-{鬥}-”字也作「-{鬪}-」，简体通作“斗”
2.
3.
4.