**斯基克达**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****位于[阿尔及利亚东北部](../Page/阿尔及利亚.md "wikilink")[地中海沿岸](../Page/地中海.md "wikilink")，是[斯基克达省的首府](../Page/斯基克达省.md "wikilink")，人口152,355（1998年）。

斯基克达在1962年[阿尔及利亚独立战争結束之前称为](../Page/阿尔及利亚独立战争.md "wikilink")**菲利普维尔**（Philippeville）。

[S](../Category/阿尔及利亚城市.md "wikilink")