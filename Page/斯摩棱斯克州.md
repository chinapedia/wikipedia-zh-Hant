**斯摩棱斯克州**（）是[俄罗斯的一个](../Page/俄罗斯.md "wikilink")[联邦州](../Page/俄罗斯联邦主体.md "wikilink")，屬[中央聯邦管區的管轄範圍](../Page/中央聯邦管區.md "wikilink")。面積49,786平方公里，人口1,049,574（2006年全俄人口普查），首府[斯摩棱斯克](../Page/斯摩棱斯克.md "wikilink")，距離首都[莫斯科](../Page/莫斯科.md "wikilink")405公里。该州与[白俄罗斯接壤](../Page/白俄罗斯.md "wikilink")。

## 注释

## 参考资料

[Category:中央聯邦管區](../Category/中央聯邦管區.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")