**Steyr
AUG**是1977年[奧地利公司](../Page/奧地利.md "wikilink")推出的軍用自動步槍，是史上首次正式装備採用[犢牛式設計的軍用步槍](../Page/犢牛式.md "wikilink")。「AUG」是德文「Armee-Universal-Gewehr」的縮寫，意即「陸軍通用步槍」\[1\]。[1978年](../Page/1978年.md "wikilink")，[奧地利聯邦軍引入並採用](../Page/奧地利聯邦軍.md "wikilink")，1978年，[奧地利聯邦軍重新命名為StG](../Page/奧地利聯邦軍.md "wikilink")
77，後來被[世界各地的多個](../Page/世界.md "wikilink")[軍事](../Page/軍事.md "wikilink")[機構採用](../Page/機構.md "wikilink")。

## 設計

犢牛式的設計使得全長在不影響彈道表現下縮短了25%（與其他有同樣槍管長度的步槍相比），多數的機型配有1.5倍光學瞄準鏡。

AUG的使用半透明[彈匣](../Page/彈匣.md "wikilink")，射手可以快速的檢視子彈存量，其控制系統亦可左右對換，大約9磅釋放壓力的扳機同時控制射擊模式的選擇，第一段為半自動射擊模式，而扳機繼續扣則進入第二段的[全自動射擊模式](../Page/自動火器.md "wikilink")。

AUG是1970至1980年代中少數擁有模組化設計的步槍，它的槍管可快速拆卸，並可與槍族中的長管、短管、重管互換使用。

## 採用

由於優良的設計、品質及美觀外型，AUG一直深受軍用及民用使用者的喜愛。

除了[奧地利](../Page/奧地利.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭和](../Page/紐西蘭.md "wikilink")[沙特阿拉伯等國的軍隊外](../Page/沙特阿拉伯.md "wikilink")，[比利時](../Page/比利時.md "wikilink")[ESI](../Page/ESI.md "wikilink")[特警隊](../Page/特警隊.md "wikilink")、[愛沙尼亞特別行動隊](../Page/愛沙尼亞.md "wikilink")、[法國](../Page/法國.md "wikilink")[國家憲兵干預隊](../Page/國家憲兵干預隊.md "wikilink")、[德國的](../Page/德國.md "wikilink")[特別行動突擊隊](../Page/特別行動突擊隊.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[烏克蘭](../Page/烏克蘭.md "wikilink")[特种部隊](../Page/特种部隊.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")[行動應變及機動組](../Page/行動應變及機動組.md "wikilink")、[英國](../Page/英國.md "wikilink")[特種空勤團及](../Page/特種空勤團.md "wikilink")[美國](../Page/美國.md "wikilink")[海關署等也有列裝AUG](../Page/海關署.md "wikilink")。

在[2011年敘利內戰當中](../Page/2011年敘利亞反政府示威.md "wikilink")，[自由敘利亞軍的戰士也取得了一些AUG並在實戰中投入使用](../Page/自由敘利亞軍.md "wikilink")。\[2\]有趣的是，這批步槍的序編號均被刪去，因此它們的來源仍然成謎。\[3\]

## 生產

除了有斯泰爾公司的原裝版本外，AUG也有在外國生產。

[馬來西亞的SME工廠在](../Page/馬來西亞.md "wikilink")1991年獲斯泰爾公司授權生產AUG，並在2004年跟斯泰爾公司共同生產。[澳洲為其授權生產的AUG重新命名為F](../Page/澳洲.md "wikilink")88，[美國的Microtech](../Page/美國.md "wikilink")
Small Arms及Tactical Products Design也有生產民用型。

[中華民國](../Page/中華民國.md "wikilink")[聯勤二○五兵工廠曾少量引進](../Page/聯勤.md "wikilink")，並參考AUG仿製成[T68步槍](../Page/T68步槍.md "wikilink")，原先預定換裝空降旅、裝甲旅，但最後沒有列裝，僅少量配發高空排進行測試，後來全數汰除。

## 优缺点

### 優點

  - AUG突擊步槍集無槍托，塑料槍身、影片監視系統、模塊化四大優點於一身：易攜帶、耐腐蝕、使用壽命長、配備高倍瞄準鏡、模塊化的部件設計方便拆卸。
  - AUG是一種把以往多種已知的設計意念聰明地組合起來，結合成一個可靠美觀的整體：
  - 無托結構——斯泰爾AUG不是第一把軍用無托結構的步槍，事實上英國的[XL-64突擊步槍和蘇聯的](../Page/XL-64突擊步槍.md "wikilink")[TKB-022突擊步槍要早於AUG約前](../Page/TKB-022突擊步槍.md "wikilink")25-30年。法國的無托步槍——[FAMAS自動步槍也在同一年代公佈](../Page/FAMAS.md "wikilink")。
  - 塑料製的武器主體——早在1962年，蘇聯的一種實驗無托步槍[TKB-022突擊步槍就已經採用了塑料主體](../Page/TKB-022突擊步槍.md "wikilink")，而[FAMAS突擊步槍也同樣具有這個特徵](../Page/FAMAS突擊步槍.md "wikilink")。
  - 採用望遠瞄準鏡為標準瞄具——1940年代後期英國的EM-2無托步槍和加拿大試驗的FN
    FAL在1950年代初設計的原型，都配備有一個低倍率的望遠瞄準鏡作為基本瞄準具。
  - 模塊化設計——最先由相同機匣和動作部件為基礎所組成的各種不同的武器（自動步槍、輕機槍、卡賓槍）的模塊化系統，最先是由法國的Rossignol和蘇俄的Fedorov分別在1920年代研製的。
  - 不過有一點可以肯定的是以優良的人體工程學、高精度和良好的可靠性而聞名的AUG比起上述的那些前輩們要成功得多。

### 缺点

  - 所有的AUG的瞄准镜、把手太小，近身搏击后容易折断。
  - 同时AUG步枪的结构比较复杂，活塞与前握把挨得很近，易灼伤在前的手，扳机力偏大，光学瞄具视场小，必须达到镜轴眼轴重合的要求，并且要用手控制发射方式，这使射手难以获得迅速准确的射击效果。
  - 该枪前小后大，前“瘦”后“肥”，背带环的位置也不够合理，使该枪背挂、携行以及战斗使用难以得心应手，恶劣条件下的可靠性也较差劣。
  - 早期型的AUG沒有射擊控制旋鈕，因此單發時扣下板機時必須按一半板機（如果全扣的話有可能會打出2～4發子彈）。
  - 彈殼排出口貼近面部。需更改小零件才能左右手互換射擊，於巷戰需隨時更改射擊角度時存在劣勢。另外，在黑夜中作戰時不利於更改小零件。

## 使用國家和地區

  -
  -   - [阿根廷武裝部隊](../Page/阿根廷武裝部隊.md "wikilink")（A1）

  -   - [奧地利聯邦軍](../Page/奥地利軍事.md "wikilink")（A1、A2、A3）
      - [奧地利聯邦內務部](../Page/奧地利聯邦內務部.md "wikilink")[眼鏡蛇作戰司令部](../Page/眼鏡蛇作戰司令部.md "wikilink")（A1）

  - ：本土生產並命名為「F88 Austeyr」。

      - [澳洲國防軍](../Page/澳大利亞國防軍.md "wikilink")

  -
  -   - （A1、A1 9mm）

  -
  -
  -   -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -   - 福克蘭群島防衛部隊（A1）

  -   - [國家憲兵干預隊](../Page/國家憲兵干預隊.md "wikilink")（A1）

  -
  -   - [德國聯邦警察](../Page/德國聯邦警察.md "wikilink")[第九國境守備隊](../Page/德國聯邦警察第九國境守備隊.md "wikilink")

      - [特別行動突擊隊](../Page/特別行動突擊隊.md "wikilink")

  -   - 特種部隊單位
      - 特警單位

  -   - [愛爾蘭國防軍](../Page/愛爾蘭國防軍.md "wikilink")（A1、A2、A3）

  -
  -
  -   - 特警單位（A1）

  -
  -
  -
  -   - [盧森堡陸軍](../Page/盧森堡軍事.md "wikilink")（A1）

  - ：本土生產

      - [馬來西亞武裝部隊](../Page/馬來西亞軍事.md "wikilink")

  -
  -
  -
  -   - [紐西蘭國防軍](../Page/紐西蘭國防軍.md "wikilink")（採用由澳洲生產的型號，即將被LMT公司生產的[AR-15樣式步槍所取代](../Page/AR-15自動步槍.md "wikilink")）

  -
  -
  -
  -
  -   - [波蘭武裝部隊](../Page/波蘭軍事.md "wikilink")[行動應變及機動組](../Page/行動應變及機動組.md "wikilink")

  -
  -
  -   - [警政署](../Page/警政署.md "wikilink")[維安特勤隊](../Page/維安特勤隊.md "wikilink")（另外設計製造出本土改型T68步槍，少量配發於高空排等特種單位試用）

  -
  -   - （A1）

  -
  -
  -
  -
  -   - 烏克蘭內務部特種部隊單位（AUG A1 HBAR）

  -
  -   - [英國陸軍](../Page/英國陸軍.md "wikilink")[特種空勤團](../Page/特種空勤團.md "wikilink")
      - 部份武裝警察單位

  -   - [美國移民局](../Page/美國移民局.md "wikilink")（A1）
      - [聯邦調查局](../Page/聯邦調查局.md "wikilink")
      - 部份地方警察局的[特種武器和戰術部隊](../Page/特種武器和戰術部隊.md "wikilink")

  -
  -
  -
  - [Flag_of_Syria_2011,_observed.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Syria_2011,_observed.svg "fig:Flag_of_Syria_2011,_observed.svg")
    [敘利亞自由軍](../Page/敘利亞自由軍.md "wikilink")：[敘利亞內戰期間使用](../Page/敘利亞內戰.md "wikilink")。

## 衍生型

[AUG_A1_508mm_04.jpg](https://zh.wikipedia.org/wiki/File:AUG_A1_508mm_04.jpg "fig:AUG_A1_508mm_04.jpg")
[AUG_A2_407mm_klein_03.jpg](https://zh.wikipedia.org/wiki/File:AUG_A2_407mm_klein_03.jpg "fig:AUG_A2_407mm_klein_03.jpg")
[Steyr_AUG_A3.jpg](https://zh.wikipedia.org/wiki/File:Steyr_AUG_A3.jpg "fig:Steyr_AUG_A3.jpg")
[AUG_9mm_03.jpg](https://zh.wikipedia.org/wiki/File:AUG_9mm_03.jpg "fig:AUG_9mm_03.jpg")

### 突擊步槍衍生型

  - **Steyr AUG
    A1**：[1982年推出](../Page/1982年.md "wikilink")，AUG的改進版本，有[橄欖色和](../Page/橄欖色.md "wikilink")[黑色版本](../Page/黑色.md "wikilink")，20吋槍管。\[4\]
  - **Steyr AUG
    A2**：[1997年](../Page/1997年.md "wikilink")[12月推出](../Page/12月.md "wikilink")，與AUG
    A1相似，具有重新設計的槍機拉柄，可拆卸式[瞄準鏡](../Page/瞄準鏡.md "wikilink")，配備[皮卡汀尼導軌](../Page/皮卡汀尼導軌.md "wikilink")，16[吋](../Page/吋.md "wikilink")[槍管](../Page/槍管.md "wikilink")。
  - **Steyr AUG A3**：與AUG
    A2相似，[皮卡汀尼導軌重新設計在](../Page/皮卡汀尼導軌.md "wikilink")[機匣頂部](../Page/機匣.md "wikilink")。
  - **Steer AUG A3SF（AUG A2 Commando、StG 77
    A2）**：[2007年底](../Page/2007年.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[特種部隊](../Page/特種部隊.md "wikilink")採用，與AUG
    A2相似，配備[皮卡汀尼導軌和提供](../Page/皮卡汀尼導軌.md "wikilink")1.5倍和3倍放大倍率的可拆卸式集成[瞄準鏡](../Page/瞄準鏡.md "wikilink")。
  - **Steyr AUG M203**：AUG
    A1，裝有[M203榴彈發射器](../Page/M203榴彈發射器.md "wikilink")。
  - **Steyr AUG AG-C**：AUG A1，裝有[HK
    AG-C/EGLM附加型榴彈發射器](../Page/HK_AG-C/EGLM附加型榴彈發射器.md "wikilink")。

### 衝鋒鎗衍生型

  - **AUG Para（AUG SMG、AUG
    9mm）**：[1988年開始生產](../Page/1988年.md "wikilink")，採用[9×19mm魯格彈的](../Page/9×19mm魯格彈.md "wikilink")[衝鋒鎗版本](../Page/衝鋒鎗.md "wikilink")，使用[斯泰爾MPi
    69衝鋒槍的彈匣](../Page/斯泰爾MPi_69衝鋒槍.md "wikilink")，運作方式是[反沖作用](../Page/反沖作用.md "wikilink")，並非[步槍的](../Page/步槍.md "wikilink")[氣動式](../Page/氣動式.md "wikilink")。與AUG
    A1不同的套件是槍管、槍機、彈匣、彈匣適配器，更換上述套件可將任何版本的AUG改裝為AUG Para。

### 輕機槍和狙擊槍衍生型

  - **Steyr AUG
    LMG**：[轻机枪型](../Page/轻机枪.md "wikilink")，配用42发弹匣，采用开膛待击，配4倍光學瞄準具。
  - **Steyr AUG HBAR**：重槍管[自动步枪型](../Page/自动步枪.md "wikilink")。
  - **Steyr AUG HBAR–T**：重槍管精確射擊型。

### 半自動衍生型

  - **Steyr AUG P**：AUG A1，短[槍管版本](../Page/槍管.md "wikilink")。
  - **Steyr AUG P Special Receiver**：AUG
    P，[皮卡汀尼導軌版本](../Page/皮卡汀尼導軌.md "wikilink")。
  - **Steyr AUG Z**：半自動民用型。
  - **Steyr USR**：美國市場專用，半自動民用型。

### 澳大利亞軍用型

F88及其型號包括：

  - **F88**：標準型，與AUG A1相同。
  - **F88C**：卡賓槍版本，407毫米槍管，與AUG A2相同。
  - **F88S**：配有戰術導軌版本，與AUG A3相同。
  - **F88 GLA**：裝有[M203PI](../Page/M203榴彈發射器.md "wikilink")，與AUG M203相同。
  - **F88T**：[.22 LR訓練步槍版本](../Page/.22_LR.md "wikilink")。
  - **F88A4**：改進試驗型，裝有[M203PI及戰術配件](../Page/M203榴彈發射器.md "wikilink")\[5\]。
  - **EF88**:F88 GLA的改进实验型，装有皮卡汀尼导轨用以安装战术附件。

### 仿製型

  - **MSAR STG-556**：於2007年SHOT SHOW（美國著名槍展）亮相的版本，MSAR STG-556以AUG
    A1重新設計而成，加入了[復進助推器及](../Page/復進助推器.md "wikilink")[M16突擊步槍的槍栓固定卡榫](../Page/M16突擊步槍.md "wikilink")，分為半自動運動步槍型及軍用自動步槍型，由美國生產\[6\]。
  - **TPD USA AXR**：同樣於2007年SHOT SHOW中亮相，由生產，以AUG
    A2修改而成的半自動版本，改用[STANAG彈匣](../Page/STANAG彈匣.md "wikilink")，主要供民用及執法部門使用\[7\]。

## 流行文化

### [電影](../Page/電影.md "wikilink")

  - 1996年—《[国产凌凌漆](../Page/国产凌凌漆.md "wikilink")》（From Beijing With
    Love）：型号为F88
    Austeyr，在剧中被命名为“UFO来福枪”，被李香琴（[袁詠儀飾演](../Page/袁詠儀.md "wikilink")）所使用。
  - 2012年—《[-{zh-hk:轟天猛將2; zh-tw:浴血任務2;
    zh-cn:敢死队2;}-](../Page/浴血任務2.md "wikilink")》（The
    Expendables 2）：型號為AUG
    A3，被吉恩·維蘭（[尚-克勞德·范·達美飾演](../Page/尚-克勞德·范·達美.md "wikilink")）所使用。
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》（The
    Expendables 3）：型號為AUG
    A1卡賓槍型，黑色槍身，被「死亡醫生」（[衛斯里·史奈普飾演](../Page/衛斯里·史奈普.md "wikilink")）所使用。
  - 2018年—《[红海行动](../Page/红海行动.md "wikilink")》（Operation Red Sea）：型号为AUG
    A3，被中国海军“蛟龙突击队”卫生兵陆琛（郭家豪饰）在潜入小镇任务中所使用。

### [電視劇](../Page/電視劇.md "wikilink")

  - 2015年—《[-{zh-hans:反击; zh-hk:絕地反擊;
    zh-tw:勇者逆襲;}-第四季](../Page/勇者逆襲_\(第四季\).md "wikilink")》：型號為AUG
    A1卡賓槍型和AUG
    A2，於[泰國行動期間被日本黑幫所使用](../Page/泰國.md "wikilink")，前者亦曾被德米安·斯科特（[蘇利文·斯坦布萊頓飾演](../Page/蘇利文·斯坦布萊頓.md "wikilink")）所繳獲。
  - 《[-{zh-hans:行尸走肉; zh-hk:行屍;
    zh-tw:陰屍路;}-](../Page/陰屍路.md "wikilink")》系列

### 電子遊戲

  - 1999年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-](../Page/絕對武力.md "wikilink")》（Counter-Strike）：型號為AUG
    A1，使用灰色槍身，命名為「[Bullpup](../Page/犊牛式.md "wikilink")」，為反恐部隊的專用武器。换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。
  - 2002年—《[-{zh-hans:秘密潛入2：隱秘行動;
    zh-hant:核武浩劫2;}-](../Page/秘密潜入2.md "wikilink")》（IGI2
    COVERT STRIKE）：型號為AUG A1，為在中国境内使用的武器之一。
  - 2003年—《[-{zh-hans:反恐精英：零点行动;
    zh-hant:絕對武力：一觸即發;}-](../Page/絕對武力：一觸即發.md "wikilink")》（Counter-Strike:
    Condition Zero）：同絕對武力。换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。
  - 2004年—《[-{zh-hans:反恐精英：起源;
    zh-hant:絕對武力：次世代;}-](../Page/絕對武力：次世代.md "wikilink")》（Counter-Strike:
    Source）：型號為AUG
    A1，使用軍綠色槍身，命名為「Bullpup」，為反恐部隊的專用武器，换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。奇怪地發射[7.62×51mm
    NATO口徑子彈](../Page/7.62×51mm_NATO.md "wikilink")。
  - 2007年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-Online](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為AUG A1与EF88：
      - AUG
        A1为隨遊戲登場武器，使用灰色槍身，奇怪地在造型上使用高倍率瞄準鏡但實際倍率只有1.5倍，命名為「Bullpup」，為反恐小組的專用武器。换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。
      - EF88命名为「强袭AUG」，加装[FN
        GL-1型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")、[ACOG
        TA31瞄准镜](../Page/先进战斗光学瞄准镜.md "wikilink")（不可使用）并在瞄准镜旁边加装一个可调式镜座以安装Trijicon
        RMR红点瞄准镜（不可使用），榴弹发射器奇怪的使用FN40GL的扳机并且在建模上穿过了枪械的握把护圈，奇怪的榴弹发射器扳机被当作榴弹发射器的拉机柄。为双方的可用武器。
  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型号为AUG A1以及AUG
    A3：
      - AUG A1命名为“Steyr AUG
        A1”，武器模組採用鏡像佈局（右手持枪时位於右边），30发弹匣，换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。无军衔限制，使用14000GP即可购买并永久使用。拥有改型“Steyr
        AUG A1-A”以及“新AUG A1”：
          - Steyr AUG A1-A使用沙色迷彩枪身以及35发弹匣，以CF点作时限贩卖；
          - 新AUG A1使用金色数码迷彩枪身以及35发弹匣，且右键放大功能修改为正确使用瞄准镜瞄准，为活动武器。
      - AUG A3命名为“斯泰尔 AUG
        A3”，加装[辅助握把以及](../Page/辅助握把.md "wikilink")[全息瞄准镜](../Page/全息瞄准镜.md "wikilink")，30发弹匣，以CF点作时限贩卖。
  - 2008年—《[AVA Online](../Page/戰地之王.md "wikilink")》（Alliance of Valiant
    Arms）：遊戲中型號為：AUGA1和AUGA3（步槍兵使用）以及AUG A3
    SF（偵查兵使用），以上三者皆在商城以歐元作永久販賣，AUGA3和AUGA3
    SF為可改裝型，AUGA3 SF在遊戲中命名為「AUGA2」。
  - 2008年—《[-{zh-hans:战地：叛逆连队;
    zh-hant:戰地風雲：惡名昭彰;}-](../Page/战地：叛逆连队.md "wikilink")》（Battlefield:
    Bad Company）：型號為AUG A3近戰型。
  - 2009年—《[-{zh-hans:使命召唤：现代战争2; zh-hk:使命召喚：現代戰爭2;
    zh-hant:決勝時刻：現代戰爭2;}-](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare 2）：型號為AUG HBAR-T，命名為“AUG
    HBAR”，被歸類為[輕機槍](../Page/輕機槍.md "wikilink")，使用軍綠色槍身和42發彈匣，在故事模式及特種作戰模式中裝有原廠提供的光學瞄準鏡，而在配備[機械瞄具時會奇怪的欠缺後](../Page/機械瞄具.md "wikilink")[照準器](../Page/照準器.md "wikilink")，故事模式中被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量所使用](../Page/極端民族主義.md "wikilink")。在聯機模式於等級32解鎖，並可使用[前握把](../Page/輔助握把.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測儀](../Page/心跳探測儀.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、及延長彈匣（增至63發）。
  - 2010年—《[-{zh-hans:使命召唤：黑色行动; zh-hk:使命召喚：黑色行動;
    zh-hant:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black Ops）：單人模式中的型號為AUG
    A1，使用軍綠色槍身，奇怪地會在1960年代出現並被[美國中央情報局](../Page/美國中央情報局.md "wikilink")[特別活動分部所使用](../Page/特別活動分部.md "wikilink")。聯機模式時的型號為AUG
    A2，同樣使用軍綠色槍身和不合理的出現在1960年代，配備從機匣頂部的導軌安裝的機械瞄具（奇怪地後照準器被前後倒置的安裝），於等級26解鎖，並可使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[Masterkey](../Page/Masterkey霰彈槍.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、延長彈匣（增至45發）、[消音器](../Page/抑制器.md "wikilink")、[火焰噴射器](../Page/火焰噴射器.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（實際上為A1型的原廠瞄準鏡）、[紅外線探測式瞄具](../Page/紅外線.md "wikilink")、[反射式瞄準鏡及](../Page/反射式瞄準鏡.md "wikilink")[雙彈匣](../Page/彈匣並聯.md "wikilink")。此槍在殭屍模式中亦有出現，並有一款稱為「AUG-5OM3」的改型。
  - 2010年—《[-{zh-hans:战地：叛逆连队2;
    zh-hant:戰地風雲：惡名昭彰2;}-](../Page/战地：叛逆连队2.md "wikilink")》（Battlefield:
    Bad Company 2）：型號為AUG A3近戰型。
  - 2011年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-3](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：型號為AUG
    A3近戰型，使用軍綠色槍身，命名為“AUG”，奇怪的能夠手動切換射擊模式。為資料片"短兵相接"新增武器，經過任務：牧羊人（Shepard）10次小隊復活，30次突击步枪擊殺后方可解鎖獲得并擁有18種套件隨使用解鎖。
  - 2012年—《[-{zh-hans:反恐精英：全球攻势;
    zh-hant:絕對武力：全球攻勢;}-](../Page/絕對武力：全球攻勢.md "wikilink")》（Counter-Strike:
    Global Offensive）：型號為AUG
    A3，命名為「AUG」，使用軍綠色槍身，為反恐部隊的專用武器。奇怪的枪上安装的[ACOG瞄准镜变为了](../Page/先進戰鬥光學瞄準鏡.md "wikilink")[反射式瞄准镜](../Page/反射式瞄准镜.md "wikilink")。
  - 2012年—《[-{zh-hans:荣誉勋章：战士;
    zh-hant:榮譽勳章：鐵血悍將;}-](../Page/榮譽勳章：鐵血悍將.md "wikilink")》（Medal
    of Honor: Warfighter）：只在多人遊戲中登場。型號為AUG
    A3，使用泥色槍身，被誤稱為“F88”，為前鋒兵的可用武器。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型号为AUG A3，AUG
    HBAR和AUG A3 9MM。
      - AUG A3被命名为“AUG
        A3”，使用16英寸枪管，30发[弹匣装填](../Page/弹匣.md "wikilink")。为步枪手专用武器，同时亦在生存模式中加装激光瞄准器后被黑木帝国特种兵所使用，K点购买，并可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、[突击刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")）。
      - AUG HBAR被命名为“AUG A3 HBAR”，使用AUG
        A3的上导轨机匣，使用42发弹匣但载弹量却奇怪的为50发。为步枪手专用武器，拥有雪地迷彩涂装版本，所有版本均可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")），预设装备双脚架。
          - 通常版本中国大陆服务器为专家解锁且有所削弱，威力更低且后座力更大；欧美与俄罗斯服务器服务器为K点武器，未被削弱。雪地迷彩版本只能通过生存模式“冷锋行动”系列地图通关奖励箱获得。
      - AUG A3 9MM被命名为“AUG A3 9MM
        XS”，30发弹匣。为工程兵专用武器，高级解锁，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")）。拥有雪地迷彩改装版本与流光迷彩改装版本，强化威力、精准度和载弹。
          - 雪地迷彩版本可以通过生存模式“冷锋行动”系列地图通关奖励箱以及抽奖获得，流光迷彩版本为STEAM专属DLC特供武器，只能通过购买STEAM专属DLC获得。
  - 2013年—《[-{zh-hans:生化危机 启示; zh-hant:惡靈古堡
    啟示;}-](../Page/惡靈古堡_啟示.md "wikilink")》（Resident
    Evil: Revelations）：型號為AUG A2，命名為「AUG」，在單人/RAID模式中登場。
  - 2013年—《[-{zh-hans:收获日2;
    zh-hant:劫薪日2;}-](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為AUG A2，可透過零件A3 Tactical改造為AUG A3，命名為「UAR」。
  - 2013年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-4](../Page/戰地風雲4.md "wikilink")》（Battlefield
    4）：型號為AUG A3，使用軍綠色槍身，30+1發彈匣，奇怪的能夠手動切換射擊模式。單機模式中能夠被主角丹尼爾·雷克（Daniel
    Recker）所使用。聯機模式中為突擊兵的可解鎖武器。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six: Siege）：型號為AUG A2、AUG A3 9mm XS以及F90 MBR；
      - AUG A2为隨遊戲登場武器，命名为“AUG
        A2”，使用軍綠色槍身，被[德國聯邦警察第九國境守備隊和](../Page/德國聯邦警察第九國境守備隊.md "wikilink")“白面具”恐怖份子所使用。
      - AUG A3 9mm XS于第三年追加下载内容“风城行动”（Operation Wind Bastion）中登场，命名为“AUG
        A3”，被[摩洛哥皇家宪兵干预队的特勤干员Kaid用作他的其中一款主武器](../Page/摩洛哥皇家武装力量.md "wikilink")。
      - F90 MBR于第四年追加下载内容“燃烧地平线行动”（Operation Burnt
        Horizon）中登场，命名为“F90”，被[澳大利亚特种空勤团的特勤干员Gridlock用作她的其中一款主武器](../Page/澳大利亚特种空勤团.md "wikilink")。
  - 2015年—《[-{zh-cn:战地：硬仗;
    zh-tw:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：型號為AUG A3和AUG A3 9mm XS。
      - AUG A3命名為「AUG
        A3」，使用黑色槍身，為資料片「叛逃」新增的武器，歸類為突擊步槍，30+1發彈匣，奇怪的能夠手動切換射擊模式，被警匪兩方操作者（Operator）所使用，價格為$48,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、延長彈匣（增至40+1發）、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。
      - AUG A3 9mm XS命名為「AUG
        Para」，使用黑色槍身，為資料片「大逃亡」新增的資料片獨有武器，歸類為衝鋒槍，32+1發彈匣，奇怪的能夠手動切換射擊模式，而且還具備三發點放模式。被匪方機械師（Mechanic）所使用（警察解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$50,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。
  - 2016年—《[少女前線](../Page/少女前線.md "wikilink")》（Girls'
    Frontline）：2018年活動限定獲得。
  - 2017年—《[荒野行動](../Page/荒野行動.md "wikilink")》（Knives
    Out）：作為空投武器出現，使用小口徑子彈，配備2倍光學瞄準鏡 。
  - 2017年—《[絕地求生](../Page/絕地求生.md "wikilink")》（PlayersUnknown's
    BattleGrounds）：作為只能夠在空投補給獲得的武器及全自動步槍出現，裝彈量為30（擴容後為40），可配備：所有突擊步槍槍口及彈匣、所有握把、紅點瞄準鏡、全息瞄準鏡、6倍及以下高倍瞄準鏡。
  - 2018年—《[穿越火线高清竞技大区](../Page/穿越火线高清竞技大区.md "wikilink")》（Crossfire
    HD）：型号为AUG A2，但奇怪的命名为“AUG
    A1”，沙色枪身，使用30发弹匣装填，奇怪地射击时拉机柄会随枪机一同复进，换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。

### 動畫

  - 2003年—《[神槍少女](../Page/神槍少女.md "wikilink")》（GUNSLINGER GIRL）：AUG
    A1為安潔麗卡所使用武器。
  - 2009年—《[幻灵镇魂曲](../Page/幻灵镇魂曲.md "wikilink")》（Phantom～Requiem for the
    Phantom～）：型號為AUG A1，被吾妻玲二/Zwei和卡爾·迪本斯/Drei所使用。
  - 2011年—《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》：AUG
    A1為久宇舞彌的專屬武器。
  - 2012年—《[枪械少女](../Page/枪械少女.md "wikilink")》：以拟人化少女出场，有两种模式，应该是短管型与标准型（508毫米枪管），但是片中注解称标准型为“轻型支援武器”。
  - 2012年—《[軍火女王](../Page/軍火女王.md "wikilink")》（Jormungand）：登場型號為AUG A1。
  - 2014年—《[刀劍神域II](../Page/刀劍神域.md "wikilink")》（Sword Art Online
    II）：於第四集在GGO的射擊靶場內出現使用AUG A1的玩家。

## 參見

  - [95式自動步槍](../Page/95式自動步槍.md "wikilink")
  - [LAPA FA-03突擊步槍](../Page/LAPA_FA-03突擊步槍.md "wikilink")
  - [EM-2突擊步槍](../Page/EM-2突擊步槍.md "wikilink")
  - [L64突擊步槍](../Page/L64突擊步槍.md "wikilink")
  - [EMARK-1突擊步槍](../Page/EMARK-1突擊步槍.md "wikilink")
  - [瓦爾梅特M82突擊步槍](../Page/瓦爾梅特M82突擊步槍.md "wikilink")
  - [T-68突擊步槍](../Page/T-68突擊步槍.md "wikilink")
  - [犢牛式槍械列表](../Page/犢牛式槍械列表.md "wikilink")
  - [各國軍隊制式步槍列表](../Page/各國軍隊制式步槍列表.md "wikilink")

## 参考文献

## 外部參考

  - —[steyrarms-AUG](http://www.steyrarms.com/index.php?id=37)

  - —[steyraug.net](http://www.steyraug.net)

  - —[Steyr-AUG.com](http://www.steyr-aug.com)

  - —[5.56mm F88
    Austeyr](https://web.archive.org/web/20070618165304/http://www.adi-limited.com/2-01-010-010-026.html)

  - —[奥地利軍用型Steyr-AUG圖像](http://www.bmlv.gv.at/download_archiv/photos/infanterie/galerie_uebersicht.php)

  - —[愛爾蘭陸軍的AUG圖像](https://web.archive.org/web/20070704094507/http://www.62infantry.com/Photos.shtml)

  - —[Remtek.com-Steyr
    HBar-T](http://remtek.com/arms/steyr/aug/aug24/special/hbart.htm)

  - —[愛爾蘭陸軍中的AUG](https://web.archive.org/web/20151017162913/http://www.62infantry.com/Weapons_Equipment/Steyr_AUG.shtml)

  - —[Modern Firearms—Steyr
    AUG](http://world.guns.ru/assault/as20-e.htm)

  - —[D Boy Gun
    World（槍炮世界）—斯泰爾AUG](http://firearmsworld.net/austria/steyr/aug/aug.htm)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:精確射手步槍](../Category/精確射手步槍.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:衝鋒槍](../Category/衝鋒槍.md "wikilink")
[Category:輕機槍](../Category/輕機槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:奧地利槍械](../Category/奧地利槍械.md "wikilink")
[Category:犢牛式槍械](../Category/犢牛式槍械.md "wikilink")
[Category:1978年面世的產品](../Category/1978年面世的產品.md "wikilink")

1.  [1](http://www.doppeladler.com/oebh/infanterie/stg77.htm)
2.
3.
4.  [The Arms Site - Steyr AUG
    A1](http://remtek.com/arms/steyr/aug/auga1/a1.htm)
5.  <http://www.bfevo.com/wiki/tiki-index.php?page=F88A4>
6.  [MSAR - Microtech Small Arms Research
    Inc](http://www.msarinc.com/home.html)
7.  [TPD-USA - Tactical Products Design
    Inc](http://www.tpdusa.com/products.php?cat=5)