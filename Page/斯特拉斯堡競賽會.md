[Stade_de_la_Meinau_fassade2.JPG](https://zh.wikipedia.org/wiki/File:Stade_de_la_Meinau_fassade2.JPG "fig:Stade_de_la_Meinau_fassade2.JPG")
**史特拉斯堡競賽會**（**Racing Club de
Strasbourg**）是位於[法國東部](../Page/法國.md "wikilink")[萊茵河左岸的](../Page/萊茵河.md "wikilink")[斯特拉斯堡的足球會](../Page/斯特拉斯堡.md "wikilink")，現時在[法國甲組足球聯賽比賽](../Page/法國甲組足球聯賽.md "wikilink")。

## 球會歷史

斯特拉斯堡於1906年成立，最初只是業餘球會，在低組別聯賽比賽比賽。球會經過多次改名，最終採用「Racing Club de
Strasbourg」這名字。1931年球會首次升上甲組，連續年排在聯賽榜前列位置，最差也不過是第六名。

二次大戰後球會成績開始倒退，1951–52年球季更降班收場，此後斯特拉斯堡經歷數十年努力，終於在1976–77年球季升回上甲組，並於1978–79年贏得[法甲聯賽冠軍](../Page/法甲.md "wikilink")，並締造了聯賽主場不敗、最少失球等一系列紀錄。

此後斯特拉斯堡無以為繼，在1986年再次降回乙組；1992年再升回甲組，但聯賽一直排在中下游。2005–06年球季斯特拉斯堡在法甲聯賽榜排 19
位，結果要降回乙組。

2016-17球季斯特拉斯堡以67積分取得[法國乙級足球聯賽冠軍](../Page/法國足球乙級聯賽.md "wikilink")，同時獲得升級[法甲的資格](../Page/法国足球甲级联赛.md "wikilink")。

## 主場球場

梅纳乌球场（Stade de la Meinau）通常簡稱為「*La
Meinau*」，在1914年於[普法戰爭後仍為](../Page/普法戰爭.md "wikilink")[德國領土的](../Page/德國.md "wikilink")[斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")，當時仍稱為「*FC
Neudorf*」的球隊已開始租用球場土地，於1921年才建成球場，最初可容 30,000 名觀眾。最近一次於2001年重修後，容量為
29,320 人。

[1938年世界盃舉行一場由](../Page/1938年世界盃足球賽.md "wikilink")[巴西對](../Page/巴西國家足球隊.md "wikilink")[波蘭的初賽](../Page/波蘭國家足球隊.md "wikilink")，巴西於[加時賽才以](../Page/加時賽.md "wikilink")
6–5
淘汰對手；而[1984年歐國盃則有包括](../Page/1984年欧洲足球锦标赛.md "wikilink")[丹麥對](../Page/丹麥國家足球隊.md "wikilink")[比利時及](../Page/比利時國家足球隊.md "wikilink")[西德對](../Page/西德國家足球隊.md "wikilink")[葡萄牙兩場分組初賽](../Page/葡萄牙國家足球隊.md "wikilink")；而[1988年歐洲盃賽冠軍盃決賽](../Page/1987–88年歐洲盃賽冠軍盃.md "wikilink")[梅赫倫以](../Page/梅赫倫足球俱樂部.md "wikilink")
1–0 擊敗[阿積士捧盃亦在此舉行](../Page/阿積士.md "wikilink")。

## 球會榮譽

  - **[法國甲組足球聯賽](../Page/法國甲組足球聯賽.md "wikilink") 冠軍**：1979年
  - **[法國乙組足球聯賽](../Page/法國足球乙級聯賽.md "wikilink") 冠軍**：1977年, 1988年,
    2017年
  - **[法國盃](../Page/法國盃.md "wikilink") 冠軍**：1951年, 1966年, 2001年
  - **[聯賽盃](../Page/法國聯賽盃.md "wikilink") 冠軍**：1997年, 2005年,2019年
  - **[-{zh-hans:欧洲足联国际托托杯;
    zh-hant:歐洲足協圖圖盃;}-](../Page/歐洲足協圖圖盃.md "wikilink")
    冠軍**：1995年

## 著名球星

  - [拿保夫](../Page/弗兰克·勒伯夫.md "wikilink")
  - [溫格](../Page/溫格.md "wikilink")
  - [杜明尼治](../Page/杜明尼治.md "wikilink")
  - [佐卡夫](../Page/佐卡夫.md "wikilink")
  - [芝拉華特](../Page/何塞·路易斯·奇拉维特.md "wikilink")

## 外部連結

  - [斯特拉斯堡官方網站（法文）](http://www.rcstrasbourg.fr/)

[S](../Category/法國足球俱樂部.md "wikilink")
[Category:1900年建立的足球俱樂部](../Category/1900年建立的足球俱樂部.md "wikilink")