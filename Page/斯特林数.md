在組合數學，**Stirling數**可指兩類數，都是由18世紀數學家James Stirling提出的。

## 第一類

[Stirling_first_four_two.png](https://zh.wikipedia.org/wiki/File:Stirling_first_four_two.png "fig:Stirling_first_four_two.png")

第一類Stirling數是有正負的，其絕對值是\(n\)個元素的項目分作\(k\)個環[排列的方法數目](../Page/排列.md "wikilink")。常用的表示方法有\(s(n,k) , \left[\begin{matrix} n \\ k \end{matrix}\right]\)。

換個較生活化的說法，就是有\(n\)個人分成\(k\)組，每組內再按特定順序圍圈的分組方法的數目。例如\(s(4,2)=11\)：

1.  {A,B},{C,D}
2.  {A,C},{B,D}
3.  {A,D},{B,C}
4.  {A},{B,C,D}
5.  {A},{B,D,C}
6.  {B},{A,C,D}
7.  {B},{A,D,C}
8.  {C},{A,B,D}
9.  {C},{A,D,B}
10. {D},{A,B,C}
11. {D},{A,C,B}

這可以用[有向圖來表示](../Page/有向圖.md "wikilink")。

  - 給定\(s(n,0)=0,s(1,1)=1\)，有[遞歸關係](../Page/遞歸.md "wikilink")\(s(n,k)=s(n-1,k-1) + (n-1) s(n-1,k)\)

递推关系的说明：考虑第n个物品，n可以单独构成一个非空循环排列，这样前n-1种物品构成k-1个非空循环排列，有\(s(n-1,k-1)\)种方法；也可以前n-1种物品构成k个非空循环排列，而第n个物品插入第i个物品的左边，这有\((n-1)*s(n-1,k)\)种方法。

  - \(| s(n,1) | =(n-1)!\)
  - \(s(n,k) = (-1)^{n+k} | s(n,k) |\)
  - \(s(n,n-1) = - C(n,2)\)
  - \(s(n,2) = (-1)^n (n-1)!\; H_{n-1}\)
  - \(s(n,3) = \frac{1}{2} (-1)^{n-1} (n-1)! [ (H_{n-1})^2 - H_{n-1}^{(2)} ]\)

\(H_n^{(m)}\)是[調和數的推廣](../Page/調和數.md "wikilink")，\(s(n,k)\)是遞降[階乘多項式的係數](../Page/階乘.md "wikilink")：

\(x^{\underline{n}}= x(x-1)(x-2)\ldots(x-n+1) = \sum_{k=1}^n s(n,k)x^k\)

## 第二類

第二類Stirling數是\(n\)個元素的集定義k個[等價類的方法數目](../Page/等價類.md "wikilink")。常用的表示方法有\(S(n,k) , S_n^{(k)} ,  \left\{\begin{matrix} n \\ k \end{matrix}\right\}\)。

換個較生活化的說法，就是有\(n\)個人分成\(k\)組的分組方法的數目。例如有甲、乙、丙、丁四人，若所有人分成1組，只能所有人在同一組，因此\(S(4,1)=1\)；若所有人分成4組，只能每人獨立一組，因此\(S(4,4)=1\)；若分成2組，可以是甲乙一組、丙丁一組，或甲丙一組、乙丁一組，或甲丁一組、乙丙一組，或其中三人同一組另一人獨立一組，即是：

1.  {A,B},{C,D}
2.  {A,C},{B,D}
3.  {A,D},{B,C}
4.  {A},{B,C,D}
5.  {B},{A,C,D}
6.  {C},{A,B,D}
7.  {D},{A,B,C}

因此\(S(4,2)=7\)。

  - 給定\(S(n,n)=S(n,1)=1\)，有遞歸關係\(S(n,k) = S(n-1,k-1) + k S(n-1,k)\)

递推关系的说明：考虑第n个物品，n可以单独构成一个非空集合，此时前n-1个物品构成k-1个非空的不可辨别的集合，有\(S(n-1,k-1)\)种方法；也可以前n-1种物品构成k个非空的不可辨别的集合，第n个物品放入任意一个中，这样有\(k*S(n-1,k)\)种方法。

  - \(S(n,n-1)=C(n,2)=n(n-1)/2\)
  - \(S(n,2)=2^{n-1} - 1\)
  - \(S(n,k) =\frac{1}{k!}\sum_{j=1}^{k}(-1)^{k-j} C(k,j) j^n\)
  - \(B_n=\sum_{k=1}^n S(n,k)\)

\(C(k,j)\)是二項式係數，\(B_n\)是[貝爾數](../Page/貝爾數.md "wikilink")。

## 兩者關係

\(\sum_{n=0}^{\max\{j,k \}} S(n,j) s(k,n) = \sum_{n=0}^{\max\{j,k \}} s(n,j) S(k,n) = \delta_{jk}\)

\(\delta_{jk}\)是[克羅內克爾δ](../Page/克羅內克爾δ.md "wikilink")。

[Category:置换](../Category/置换.md "wikilink")
[S](../Category/整数数列.md "wikilink")
[Category:阶乘与二项式主题](../Category/阶乘与二项式主题.md "wikilink")