**斯琴高娃**（；），[瑞士](../Page/瑞士.md "wikilink")[女演员](../Page/女演员.md "wikilink")，[汉族](../Page/汉族.md "wikilink")（父亲[汉族](../Page/汉族.md "wikilink")、母亲蒙古族），原籍中国[内蒙古](../Page/内蒙古.md "wikilink")[昭乌达盟](../Page/昭乌达盟.md "wikilink")[宁城](../Page/宁城.md "wikilink")，出生于中国[广东省](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")，在[蒙古语中](../Page/蒙古语.md "wikilink")「斯琴（）」的意思是「聪明」，「高娃（）」的意思是「美丽」。1986年，斯琴高娃隨第三任丈夫华裔音乐家[陈亮声加入瑞士国籍](../Page/陈亮声.md "wikilink")。

## 从艺经历

斯琴高娃的父母都是军人。她1950年在广东出生，1954年她4岁时父亲去世，母亲带着包括她的三个孩子回到内蒙古。斯琴高娃于1965年进入[内蒙古自治区歌舞团](../Page/内蒙古自治区歌舞团.md "wikilink")，任舞蹈演员、报幕员。1968年，斯琴高娃与内蒙古电影制片厂的一位大她9岁的年轻导演[孙天相相恋结婚](../Page/孙天相.md "wikilink")，于6年后的1976年春节后离婚，當時4岁的儿子孙铁判给了丈夫，5岁的女儿孙丹跟着斯琴高娃\[1\]。斯琴高娃其後再与影片剧务[达斡尔族](../Page/达斡尔族.md "wikilink")[敖醒晨结婚](../Page/敖醒晨.md "wikilink")，婚后第二年，已经怀有四个月的身孕而被[八一电影制片厂选中主演电影](../Page/中国人民解放军八一电影制片厂.md "wikilink")《[归心似箭](../Page/归心似箭.md "wikilink")》，斯琴高娃对这剧本极其喜爱，为此下了一个决定：[捨孩子而取事业](../Page/堕胎.md "wikilink")。1979年憑《归心似箭》中出色表演而获得获当年[文化部颁发的优秀青年创作奖](../Page/中华人民共和国文化部.md "wikilink")。

1982年，斯琴高娃因为成功塑造了《[骆驼祥子](../Page/骆驼祥子_\(电影\).md "wikilink")》（[凌子风导演](../Page/凌子风.md "wikilink")）中的虎妞一角而一举成名，1983年获第三届中国电影[金鸡奖最佳女主角奖和第六届中国电影](../Page/金鸡奖.md "wikilink")[百花奖最佳女演员奖](../Page/百花奖.md "wikilink")。在拍摄《骆驼祥子》的时候，斯琴高娃的第二次婚姻也走到了尽头。1985年憑《[似水流年](../Page/似水流年.md "wikilink")》獲得第四屆[香港電影金像獎最佳女主角](../Page/香港電影金像獎.md "wikilink")，是[香港電影金像獎史上第一位奪得該獎的大陸女演員](../Page/香港電影金像獎.md "wikilink")。1986年12月，她和现任丈夫瑞士[华裔音乐指挥家](../Page/华裔.md "wikilink")[陳亮聲结婚](../Page/陳亮聲.md "wikilink")，并一同移居瑞士\[2\]。1989年，她受[关锦鹏邀请](../Page/关锦鹏.md "wikilink")，同[張曼玉和](../Page/張曼玉.md "wikilink")[张艾嘉出演电影](../Page/张艾嘉.md "wikilink")《[人在纽约](../Page/人在纽约.md "wikilink")》，大获成功。

此后，斯琴高娃逐渐回到中国国内演艺界，陆续出演了一系列电影，如《[香魂女](../Page/香魂女.md "wikilink")》、《[太后吉祥](../Page/太后吉祥.md "wikilink")》等。1992年主演的《[香魂女](../Page/香魂女.md "wikilink")》还获得了[柏林电影节](../Page/柏林电影节.md "wikilink")[金熊奖](../Page/金熊奖.md "wikilink")。2001年，她相继在[电视剧](../Page/电视剧.md "wikilink")《[大宅门](../Page/大宅门.md "wikilink")》和《[康熙王朝](../Page/康熙王朝.md "wikilink")》中饰演主要角色白文氏和[孝莊](../Page/孝莊文皇后.md "wikilink")，其演出获得了广泛好评。

2008年，斯琴高娃憑《[姨媽的後現代生活](../Page/姨妈的后现代生活_\(电影\).md "wikilink")》再次獲得[香港電影金像獎最佳女主角](../Page/香港電影金像獎.md "wikilink")。成為第一位兩奪[香港電影金像獎最佳女主角的大陆演員](../Page/香港電影金像獎.md "wikilink")。

## 影视作品

### 电影

|                                      |                                                     |        |
| ------------------------------------ | --------------------------------------------------- | ------ |
| **上映年份**                             | **电影名**                                             | **角色** |
| 1979年                                | 《[归心似箭](../Page/归心似箭.md "wikilink")》                |        |
| 1980年                                | 《残雪》                                                |        |
| 1981年                                | 《阿丽玛》                                               |        |
| 1981年                                | 《许茂和他的女儿们》                                          |        |
| 1982年                                | 《[骆驼祥子](../Page/骆驼祥子.md "wikilink")》                |        |
| 1982年                                | 《大泽龙蛇》                                              |        |
| 1982年                                | 《风雨下钟山》                                             |        |
| 1983年                                | 《再生之地》                                              |        |
| 1984年                                | 《高山下的花环》                                            |        |
| 1984年                                | 《[似水流年](../Page/似水流年_\(1984年電影\).md "wikilink")》    |        |
| 1984年                                | 《森吉德玛》                                              |        |
| 1984年                                | 《驼峰上的爱》                                             |        |
| 1986年                                | 《[成吉思汗](../Page/成吉思汗_\(电影\).md "wikilink")》         |        |
| 1986年                                | 《[月牙儿](../Page/月牙儿.md "wikilink")》                  |        |
| 1988年                                | 《电影人》                                               |        |
| 1989年                                | 《三个女人的故事》                                           |        |
| 1990年                                | 《贩母案考》                                              |        |
| 1990年                                | 《嫁到宫里的男人》                                           |        |
| 1990年                                | 《[异域](../Page/異域_\(電影\).md "wikilink")》             |        |
| 1992年                                | 《香魂女》                                               |        |
| 1993年                                | 《少林英雄》                                              |        |
| 1993年                                | 《老人与狗》                                              |        |
| 1994年                                | 《[刀剑笑](../Page/刀剑笑.md "wikilink")》                  |        |
| 1994年                                | 《[阳光灿烂的日子](../Page/阳光灿烂的日子.md "wikilink")》          |        |
| 1995年                                | 《天国逆子》                                              |        |
| 1995年                                | 《太后吉祥》                                              |        |
| 1998年                                | 《日落紫禁城》                                             |        |
| 2002年                                | 《世界上最疼我的那个人去了》                                      |        |
| 2003年                                | 《不扣纽扣的情人》                                           |        |
| 2004年                                | 《重金防线》                                              |        |
| 2006年                                | 《[姨媽的後現代生活](../Page/姨妈的后现代生活_\(电影\).md "wikilink")》 |        |
| 2013年                                | 《[富春山居图](../Page/天機：富春山居圖.md "wikilink")》           |        |
| 2014年                                | 《[北京愛情故事](../Page/北京愛情故事_\(电影\).md "wikilink")》     |        |
| 2015年                                | 《[搏击迷城](../Page/搏击迷城.md "wikilink")》                |        |
| 《[擦枪走火](../Page/擦枪走火.md "wikilink")》 |                                                     |        |
| 2018年                                | 《[一墙之隔](../Page/一墙之隔.md "wikilink")》（2010年拍摄）       |        |
| 《[荒城纪](../Page/荒城纪.md "wikilink")》   |                                                     |        |
| 待上映                                  | 《第一大案》                                              |        |
| 待上映                                  | 《大破天门阵》                                             |        |
|                                      |                                                     |        |

### 电视剧

|          |                                                     |                                      |
| -------- | --------------------------------------------------- | ------------------------------------ |
| **首播年份** | **剧名**                                              | **角色**                               |
| 1993年    | 《[大太监与小木匠](../Page/大太监与小木匠.md "wikilink")》          | [李康妃](../Page/李康妃.md "wikilink")     |
| 2000年    | 《[開心就好](../Page/開心就好.md "wikilink")》                |                                      |
| 2001年    | 《[康熙帝國](../Page/康熙王朝.md "wikilink")》                | 孝莊太皇太后                               |
| 2001年    | 《[大宅门](../Page/大宅门.md "wikilink")》                  | 白文氏                                  |
| 2002年    | 《[絕對權力](../Page/絕對權力.md "wikilink")》                |                                      |
| 2002年    | 《[肥猫寻亲记](../Page/肥猫寻亲记.md "wikilink")》              |                                      |
| 2003年    | 《[孝莊秘史](../Page/孝莊秘史.md "wikilink")》                | 大福晋                                  |
| 2004年    | 《[康定情歌](../Page/康定情歌.md "wikilink")》                |                                      |
| 2004年    | 《[沧海百年](../Page/沧海百年.md "wikilink")》                |                                      |
| 2005年    | 《[青花](../Page/青花.md "wikilink")》                    |                                      |
| 2005年    | 《[北京，我的爱](../Page/北京，我的爱.md "wikilink")》            |                                      |
| 2005年    | 《[野火春风斗古城](../Page/野火春风斗古城.md "wikilink")》          |                                      |
| 2005年    | 《[凤临阁](../Page/凤临阁.md "wikilink")》                  |                                      |
| 2006年    | 《[无字碑歌](../Page/无字碑歌.md "wikilink")》                | [武則天](../Page/武則天.md "wikilink")     |
| 2008年    | 《[老柿子树](../Page/老柿子树.md "wikilink")》（CCTV8首播）       |                                      |
| 2009年    | 《[百年虚云](../Page/百年虚云.md "wikilink")》                | 慈禧太后                                 |
| 2010年    | 《[娘](../Page/娘_\(电视剧\).md "wikilink")》              | 母親                                   |
| 2011年    | 《师傅》                                                | 铁母                                   |
| 2011年    | 《[武则天秘史](../Page/武则天秘史.md "wikilink")》              | [武則天](../Page/武則天.md "wikilink")(晚年) |
| 2012年    | 《[穆桂英掛帥](../Page/穆桂英掛帥_\(2012年電視劇\).md "wikilink")》 | [佘太君](../Page/佘太君.md "wikilink")     |
| 2012年    | 《房战》                                                |                                      |
| 2012年    | 《[母亲，母亲](../Page/母亲，母亲.md "wikilink")》              | 侯志宏的母亲                               |
| 2013年    | 《[隋唐演義](../Page/隋唐演義_\(2013年電視劇\).md "wikilink")》   | [程咬金的母親](../Page/程咬金.md "wikilink")  |
| 2014年    | 《兄弟兄弟》                                              | 孟五德堂大娘                               |
| 2014年    | 《当妈不容易》                                             |                                      |
| 2014年    | 《乱世烟雨》                                              |                                      |
| 2014年    | 《上线下线》                                              |                                      |
| 2015年    | 《二婶》                                                | 婆婆                                   |
| 2015年    | 《左手劈刀》                                              | 马凤岳                                  |
| 2016     | 《[姐妹姐妹](../Page/姐妹姐妹.md "wikilink")》                | 宋海靖                                  |
| 2016     | 《守婚》(原名:幸福相依)                                       | 陈金花                                  |
| 2016     | 《39度青春》                                             | 客串                                   |
| 2017     | 《[龙珠传奇](../Page/龙珠传奇.md "wikilink")》                | 太皇太后                                 |
| 2017     | 《[平凡岁月](../Page/平凡岁月.md "wikilink")》                | 姑奶奶                                  |
| 2017     | 《[苦乐村官](../Page/苦乐村官.md "wikilink")》                | 万奶奶                                  |
| 2017     | 《啊，父老乡亲》                                            | 斯校长(客串)                              |
| 2018     | 《[星火云雾街](../Page/星火云雾街.md "wikilink")》              | 卢母                                   |
| 2018     | 《音乐会》                                               | 老年金英子                                |
| 2018     | 《英雄烈》                                               | 梁母                                   |
| 2019     | 《[往事云烟](../Page/往事云烟.md "wikilink")》(2015年拍攝)       | 魏母                                   |
| 2019     | 《[东宫](../Page/东宫_\(电视剧\).md "wikilink")》            | 太皇太后                                 |
| 2019     | 《小幸福》                                               |                                      |
| 待播       | 《[烽火长城](../Page/烽火长城.md "wikilink")》(2010年拍攝)       |                                      |
| 待播       | 《爱没那么简单》(2012年拍攝)                                   |                                      |
| 待播       | 《[海上丝路](../Page/海上丝路.md "wikilink")》                | [武則天](../Page/武則天.md "wikilink")     |
| 待播       | 《玉玺传奇》                                              |                                      |
| 待播       | 《[黑河风云](../Page/黑河风云.md "wikilink")》                |                                      |
| 待播       | 《[十里洋场拾年花](../Page/十里洋场拾年花.md "wikilink")》          |                                      |
| 待播       | 《宣武门》                                               |                                      |
| 待播       | 《立秋》                                                |                                      |
| 待播       | 《[父親的草原母親的河](../Page/父親的草原母親的河.md "wikilink")》      |                                      |
| 待播       | 《[你永远在我身边](../Page/你永远在我身边.md "wikilink")》          |                                      |
|          |                                                     |                                      |

## 获奖与提名

  - 获奖

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1979年</p></td>
<td><p>文化部</p></td>
<td><p>优秀青年创作奖</p></td>
<td><p>《归心似箭》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p>第3届中国电影金鸡奖</p></td>
<td><p>最佳女主角</p></td>
<td><p>《骆驼祥子》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p>第6届中国电影百花奖</p></td>
<td><p>最佳女演员</p></td>
<td><p>《骆驼祥子》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/第4届香港电影金像奖.md" title="wikilink">第4届香港电影金像奖</a></p></td>
<td><p>最佳女演员</p></td>
<td><p>《<a href="../Page/似水流年_(1984年電影).md" title="wikilink">似水流年</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p>芝加哥国际电影节</p></td>
<td><p>最佳女演员</p></td>
<td><p>《香魂女》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>第2届香港电影评论学会奖</p></td>
<td><p>最佳女演员</p></td>
<td><p>《天国逆子》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p>第17届全国电视剧飞天奖</p></td>
<td><p>特别荣誉表演奖</p></td>
<td><p>《党员二愣妈》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>第14届大学生电影节</p></td>
<td><p>最佳女演员</p></td>
<td><p>《姨妈的后现代生活》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>第12届上海影评人奖</p></td>
<td><p>最佳女演员</p></td>
<td><p>《姨妈的后现代生活》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>第12届香港电影评论协会奖</p></td>
<td><p>最佳女演员</p></td>
<td><p>《姨妈的后现代生活》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/第27届香港电影金像奖.md" title="wikilink">第27届香港电影金像奖</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/姨妈的后现代生活_(电影).md" title="wikilink">姨妈的后现代生活</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/2012年华鼎奖.md" title="wikilink">华鼎奖</a></p></td>
<td><p>全国观众最喜爱的十大影视明星</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/2012年国剧盛典获奖名单.md" title="wikilink">国剧盛典</a></p></td>
<td><p>终身成就奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/第13届华鼎奖.md" title="wikilink">第13届华鼎奖</a></p></td>
<td><p>中国电视剧最受媒体欢迎演员奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 提名

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/第27届台湾金马奖.md" title="wikilink">第27届台湾金马奖</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《<a href="../Page/贩暮案考.md" title="wikilink">贩暮案考</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/第43届台湾金马奖.md" title="wikilink">第43届台湾金马奖</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/姨妈的后现代生活_(电影).md" title="wikilink">姨妈的后现代生活</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [新浪网－斯琴高娃](http://ent.sina.com.cn/s/m/f/siqgw1.html)

  -
  -
  -
  -
{{-}}

[Category:归化瑞士公民的中华人民共和国人](../Category/归化瑞士公民的中华人民共和国人.md "wikilink")
[Category:内蒙古人](../Category/内蒙古人.md "wikilink")
[Category:宁城县人](../Category/宁城县人.md "wikilink")
[Category:广东人](../Category/广东人.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")
[Category:内蒙古演员](../Category/内蒙古演员.md "wikilink")
[Category:广东演员](../Category/广东演员.md "wikilink")
[Category:中国蒙古族演员](../Category/中国蒙古族演员.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:瑞士演員](../Category/瑞士演員.md "wikilink")
[Category:1905-2004年中国电影百位优秀演员](../Category/1905-2004年中国电影百位优秀演员.md "wikilink")
[Category:金鸡奖最佳女主角得主](../Category/金鸡奖最佳女主角得主.md "wikilink")
[Category:大众电影百花奖最佳女主角得主](../Category/大众电影百花奖最佳女主角得主.md "wikilink")
[Category:香港電影金像獎最佳女主角得主](../Category/香港電影金像獎最佳女主角得主.md "wikilink")
[Category:北京大學生電影節最佳女演員得主](../Category/北京大學生電影節最佳女演員得主.md "wikilink")
[Category:香港電影評論學會大獎最佳女演員得主](../Category/香港電影評論學會大獎最佳女演員得主.md "wikilink")

1.  [图文：斯琴高娃与第一任前夫](http://ctdsb.cnhubei.com/html/ctdsbfk/20071112/ctdsbfk162741.html)，楚天都市报，2007年11月12日
2.  [回顾三段婚姻
    斯琴高娃：演"虎妞"导致家庭破裂](http://www.chinanews.com.cn/yl/yrfc/news/2008/02-15/1164099.shtml)，中国新闻网，2008年02月15日