**斯皮克斯金剛鸚鵡**又名**小藍金剛鸚鵡**
（**）是[鹦鹉科中唯一被編入藍金剛鸚鵡屬](../Page/鹦鹉科.md "wikilink")（**）的品種。1819年，[德國自然歷史學家](../Page/德國.md "wikilink")[斯皮克斯受雇於日耳曼國王前住巴西收集標樣](../Page/w:en:Johann_Baptist_von_Spix.md "wikilink")，他於[巴西北部現稱](../Page/巴西.md "wikilink")[巴伊亞州一帶首先發現這種鸚鵡](../Page/巴伊亞州.md "wikilink")，並射殺了一隻以製成標本，斯皮克斯鸚鵡的名稱也就因此而來。

斯皮克斯金剛鸚鵡有長尾巴及翅膀，身體大部份呈灰藍色，背部和尾巴為藍色，眼睛附近的皮膚和臉頰呈暗灰色，鳥喙為黑色，虹膜為黃色。

斯皮克斯金剛鸚鵡現時被[IUCN紅色名錄列入極危級別](../Page/IUCN紅色名錄.md "wikilink")\<ref
=IUCNRedList\>國際雀鳥聯盟（）（2004）[Cyanopsitta
spixii](http://www.redlist.org/search/details.php?species=5992)
。2006年[IUCN紅色名錄的受威脅物種](../Page/世界自然保護聯盟紅色名錄.md "wikilink")，資料說明了此物種被編入極危級別的原因</ref>，且被列於[華盛頓公約附錄一及二](../Page/華盛頓公約.md "wikilink")，與《[瀕危野生動植物種國際貿易公約](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")》的附錄I，禁止進入交易。

## 生存威脅

此品種雖然被紅色名錄列入極危級別，但可能已經野外絕種，原因是已知的最後一隻野外雄性2000年死亡，但鑑於仍有部份可能會發現該品種的生境未被深入調查，故此將此品種列入極危級別\[1\]，然而，人工飼養的仍有超過60隻\[2\]\[3\]。當中大部份都是在人工飼養環境下繁殖，而當中只有9隻（2隻來自[西班牙](../Page/西班牙.md "wikilink")，7隻來自巴西動物園）被動物園用作繁殖野放計劃。2004年，成功繁殖出2隻鶵鳥\[4\]
，但由於雀鳥之間都是近親關係，人工繁殖可能會導致[近交衰退](../Page/近交衰退.md "wikilink")，物種的生存能力每況愈下。

造成斯皮克斯金剛鸚鵡幾乎絕種的原因，主要是[雀鳥貿易的捕捉活動](../Page/鳥類貿易.md "wikilink")，而人類居地亦侵佔了其生境，亦有人捕獵其為食用及交易。而人類所引進的[非洲化蜜蜂](../Page/非洲化蜜蜂.md "wikilink")（**）殺死正在孵卵的鸚鵡，估計蜜蜂佔據了40%的巢居地。

这种鸟最近一次在野外被看到是在[巴伊亚州的](../Page/巴伊亞州.md "wikilink")[库拉萨](../Page/库拉萨.md "wikilink")，一只斯皮克斯金刚鹦鹉被视频摄影机捕捉到，在巴伊亚州的树木之间飞翔。这是它15年来第一次出现。\[5\]

## 艺术改编

在2011年上映的动画电影《[里约大冒险](../Page/里约大冒险.md "wikilink")》（*Rio*，香港译《奇鹦嘉年华》）中，主角珠儿（*Jewel*）和阿藍（*Blu*）就是一对雌雄斯皮克斯金剛鸚鵡。影片讲述了在斯皮克斯金剛鸚鵡濒临灭绝的关头，阿藍要远去[巴西](../Page/巴西.md "wikilink")[里约热内卢和繁育中心的雌性斯皮克斯金剛鸚鵡珠儿一起繁育后代](../Page/里约热内卢.md "wikilink")，并以此引发了一系列神奇经历的冒险故事。

## 參考資料

  -
<!-- end list -->

  - Juniper, Tony (2003) *Spix's Macaw : The Race to Save the World's
    Rarest Bird* ISBN 074347550X

  -
## 外部連結

  - [City Parrots](http://www.cityparrots.org)
  - [含2張於Loro
    Parque拍攝的斯皮克斯金剛鸚鵡的相片](https://web.archive.org/web/20060305000920/http://www.loroparque-fundacion.org/Downloads/Parrot%20Photos/Cyanopsitta%20spixii.jpg)
  - [SOE鵡林鸚雄 -
    斯皮克金剛](https://web.archive.org/web/20070313041834/http://www.soe-parrot.com/macaw2/m6.htm)
  - [斯皮克斯金剛鸚鵡的悲歌](http://www.toaparrotzone.com/endengered%20spix's%20macaw.htm)
  - [絕跡15年，珍稀小藍金剛鸚鵡再現巴西](http://www.bbc.com/zhongwen/trad/world/2016/06/160625_brazil_spix_macaw)

[Category:極危物種](../Category/極危物種.md "wikilink")
[Category:鹦鹉科](../Category/鹦鹉科.md "wikilink")
[Category:巴西鳥類](../Category/巴西鳥類.md "wikilink")

1.  [國際雀鳥聯盟物種說明](http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=1546&m=0)

2.
3.  [密西根州大學](http://www.umich.edu/~esupdate/marchapril2000/schischakin.htm)
    斯皮克斯金剛鸚鵡

4.
5.