**斯科舍板块**是一个大洋[板块](../Page/板块.md "wikilink")，北与[南美洲板块相接](../Page/南美洲板块.md "wikilink")，东与[南桑威奇微板块相接](../Page/南桑威奇板块.md "wikilink")，南和西则与[南极洲板块接壤](../Page/南极洲板块.md "wikilink")。

斯科舍板块的北部和南部边界是[转换边界](../Page/转换边界.md "wikilink")，其东界与南桑威奇板块之间是[离散边界](../Page/离散边界.md "wikilink")。由于南美洲板块向西的俯冲作用，造成了南桑威奇[岛弧和](../Page/岛弧.md "wikilink")[弧后盆地](../Page/弧后盆地.md "wikilink")，因而南桑威奇板块从斯科舍板块中分离出来，并以弧后盆地中的扩张带与斯科舍板块为界。斯科舍板块西部与南极洲板块的边界较复杂，至今仍难于判断是什么类型。

据推断，南美洲板块的西向运动使分别与其北端和南端相邻的[加勒比板块和斯科舍板块均受到挤压](../Page/加勒比板块.md "wikilink")。这两个板块因而具有相似的外形，其东界都是南美洲板块的[消减带](../Page/消减带.md "wikilink")。[1](https://web.archive.org/web/20080908074248/http://www.geophysics.rice.edu/department/research/alan1/SECAR/Plate_Tectonic_Evolution_.htm)

## 参见

  -
## 参考來源

## 外部链接

  - [South Sandwich
    microplate](https://web.archive.org/web/20080226194735/http://volcano.und.nodak.edu/vwdocs/volc_images/south_america/south_sandwich_islands.html)
  - [Motion of Scotia plate](http://www.liv.ac.uk/~tine/owork.html)

[Category:板块](../Category/板块.md "wikilink")
[Category:大西洋](../Category/大西洋.md "wikilink")