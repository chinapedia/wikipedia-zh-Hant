[Old_provinces_scania.png](https://zh.wikipedia.org/wiki/File:Old_provinces_scania.png "fig:Old_provinces_scania.png")和[克里斯蒂安斯塔德省](../Page/克里斯蒂安斯塔德省.md "wikilink")\]\]
**斯科讷省**（瑞典语：****）是[瑞典最南部的一个省](../Page/瑞典.md "wikilink")。斯科讷被認為是北歐民族的發源地，在1658年之前，[该地区归属](../Page/斯科訥.md "wikilink")[丹麦](../Page/丹麦.md "wikilink")。1997年，[马尔默胡斯省和](../Page/馬爾默胡斯省.md "wikilink")[克里斯蒂安斯塔德省合併成现在的斯科讷省](../Page/克里斯蒂安斯塔德省.md "wikilink")。

斯科讷省的北面是[哈兰省](../Page/哈兰省.md "wikilink")、[克鲁努贝里省和](../Page/克鲁努贝里省.md "wikilink")[布莱金厄省](../Page/布莱金厄省.md "wikilink")，东南面是[波罗的海](../Page/波罗的海.md "wikilink")，西面是[厄勒海峡](../Page/厄勒海峡.md "wikilink")。它是[厄勒地区的一部分](../Page/厄勒地区.md "wikilink")。从北到南大约130公里，占瑞典总国土的3%不到，但是人口有120万，占瑞典总人口的13%。

斯科讷省的火车铁轨非常多。

## 主要城市

[Skåne_County.png](https://zh.wikipedia.org/wiki/File:Skåne_County.png "fig:Skåne_County.png")

  - [恩厄尔霍尔姆](../Page/恩厄尔霍尔姆.md "wikilink") - Ängelholm
  - [赫尔辛堡](../Page/赫尔辛堡.md "wikilink") - Helsingborg
  - [赫比](../Page/赫比市.md "wikilink") - Hörby
  - [赫尔](../Page/赫尔市.md "wikilink") - Höör
  - [克利潘](../Page/克利潘市.md "wikilink") - Klippan
  - [克里斯蒂安斯塔德](../Page/克里斯蒂安斯塔德.md "wikilink") - Kristianstad
  - [谢夫灵厄](../Page/谢夫灵厄市.md "wikilink") - Kävlinge
  - [兰斯克鲁纳](../Page/兰斯克鲁纳.md "wikilink") - Landskrona
  - [盧馬](../Page/卢马市.md "wikilink") - Lomma
  - [隆德](../Page/隆德.md "wikilink")- Lund
  - [马尔默](../Page/马尔默.md "wikilink") - Malmö
  - [-{于}-斯塔德](../Page/于斯塔德.md "wikilink") - Ystad

## 外部链接

  - [斯科讷省官方网站](http://www.m.lst.se/)
  - [斯科讷省议会](http://www.skane.se/)

[Category:瑞典省份](../Category/瑞典省份.md "wikilink")