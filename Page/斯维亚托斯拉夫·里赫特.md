**斯维亚托斯拉夫·特奥菲洛维奇·里赫特**（，，又译**里希特**或**李希特**，），[乌克兰](../Page/乌克兰.md "wikilink")[德裔](../Page/德裔.md "wikilink")[钢琴家](../Page/钢琴家.md "wikilink")。里赫特被公认为是[20世纪最伟大的钢琴家之一](../Page/20世纪.md "wikilink")，他以极广的曲目范围，举重若轻的技术以及富有诗意的分句闻名\[1\]\[2\]。

## 早年

里赫特生于[乌克兰](../Page/乌克兰.md "wikilink")[日托米尔](../Page/日托米尔.md "wikilink")，在[敖德萨长大](../Page/敖德萨.md "wikilink")，父亲是德裔[管风琴家](../Page/管风琴.md "wikilink")，毕业于[维也纳音乐学院](../Page/维也纳音乐学院.md "wikilink")。不同于一般的钢琴家，虽然父亲教了他一些音乐的入门知识，但里赫特基本上是自学成才。里赫特少时就有了出众的视奏能力，经常参与当地[歌剧和](../Page/歌剧.md "wikilink")[芭蕾团体的活动](../Page/芭蕾.md "wikilink")。在[法国](../Page/法国.md "wikilink")音乐节上，他发现自己迷上了歌剧、[人声和](../Page/人声.md "wikilink")[室内乐](../Page/室内乐.md "wikilink")，並持續了一輩子。他的第一份工作是在[国立敖德萨戏剧和芭蕾舞学院剧场为歌剧排练](../Page/国立敖德萨戏剧和芭蕾舞学院剧场.md "wikilink")[伴奏](../Page/伴奏.md "wikilink")。

1934年在敖德萨的一个技师俱乐部，里赫特开了他的第一场独奏[音乐会](../Page/音乐会.md "wikilink")，三年后进入[莫斯科音乐学院开始正式学琴](../Page/莫斯科音乐学院.md "wikilink")，与[吉列尔斯一道师从钢琴教育家](../Page/埃米尔·格里戈里耶维奇·吉列尔斯.md "wikilink")[涅高兹](../Page/海因里希·古斯塔沃维奇·涅高兹.md "wikilink")，涅高兹说里赫特是他“盼了一辈子才盼来的天才学生”。

还是一名学生的里赫特在1940年4月首演了[普罗科菲耶夫的](../Page/普罗科菲耶夫.md "wikilink")，自此他的名字便与普罗科菲耶夫紧密联系了起来\[3\]。另一件在音乐学院时期的著名事迹是他不上政治必修课，结果第一学期便被开除了两次。里赫特一直是[苏联的政治局外人](../Page/苏联.md "wikilink")，也从来没有加入过[共产党](../Page/蘇聯共產黨.md "wikilink")。

## 伴侶

1945年，里赫特認識了[女高音](../Page/女高音.md "wikilink")，并作为伴奏与她合作演出了[里姆斯基-科萨科夫和普罗科菲耶夫的一些歌曲](../Page/里姆斯基-科萨科夫.md "wikilink")。两人于1946年结婚，但未有子嗣\[4\]。

有传言说里赫特是[同性恋者](../Page/同性恋者.md "wikilink")，而多利阿克的妻子身份为里赫特在公众面前掩盖性倾向，因為在苏维埃政权时期，同性恋是违法的\[5\]\[6\]。里赫特极少在访谈中涉及个人私生活，直到生命的最后一年在为其拍摄的传记影片《Richter:
The Enigma》中才稍有谈及。

## 巡回演出

[Святослав_Рихтер.jpg](https://zh.wikipedia.org/wiki/File:Святослав_Рихтер.jpg "fig:Святослав_Рихтер.jpg")
1949年里赫特获颁[斯大林奖](../Page/斯大林奖.md "wikilink")，之后在蘇聯、[东欧](../Page/东欧.md "wikilink")、[中国举行了多场巡回音乐会](../Page/中国.md "wikilink")。里赫特50年代的录音令其引起西方注意，而他直到1960年才被允许到[美国进行巡回演出](../Page/美国.md "wikilink")，轰动一时，在[卡内基音乐厅的音乐会场场爆满](../Page/卡内基音乐厅.md "wikilink")。尽管如此，里赫特并不十分喜欢巡回演出，他习惯于不事先安排音乐会的日程。晚年时，他的音乐会通常在不知名的小音乐厅进行，有时还会关掉灯，只用一盏小灯照着钢琴。

1995年3月在德国[吕贝克举办了最后一场音乐会之后](../Page/吕贝克.md "wikilink")，里赫特逐渐由于听力障碍抑郁，于1997年因心肌梗塞在[莫斯科逝世](../Page/莫斯科.md "wikilink")，去世之前仍在为将要进行的音乐会作准备。

## 演奏

里希特擅长演奏[巴赫](../Page/约翰·塞巴斯蒂安·巴赫.md "wikilink")、[貝多芬](../Page/貝多芬.md "wikilink")、[舒伯特](../Page/舒伯特.md "wikilink")、[海顿](../Page/海顿.md "wikilink")、[萧邦](../Page/萧邦.md "wikilink")、[舒曼](../Page/舒曼.md "wikilink")、[李斯特](../Page/李斯特·费伦茨.md "wikilink")、[拉赫玛尼诺夫](../Page/拉赫玛尼诺夫.md "wikilink")、[普罗科菲耶夫](../Page/普罗科菲耶夫.md "wikilink")、[肖斯塔科维奇等作曲家的曲目](../Page/肖斯塔科维奇.md "wikilink")。

里赫特演奏的范围包括了钢琴文献中绝大部分的主要作品，但也有遗漏，例如：[巴赫](../Page/巴赫.md "wikilink")[哥德堡变奏曲](../Page/哥德堡变奏曲.md "wikilink")、[贝多芬的](../Page/贝多芬.md "wikilink")、[第四钢琴协奏曲和](../Page/第4号钢琴协奏曲_\(贝多芬\).md "wikilink")[第五钢琴协奏曲](../Page/第5号钢琴协奏曲_\(贝多芬\).md "wikilink")、[舒伯特](../Page/舒伯特.md "wikilink")（D.959）。普罗科菲耶夫的和都由里赫特完成首演，其中后者是普罗科菲耶夫专门题献给钢琴家本人。

里赫特演奏的特別之處在於，能夠賦予作品全新的風貌，同时非常具有個人風格。在独奏之外，里赫特也是一位很好的[室內樂演出者](../Page/室內樂.md "wikilink")，与小提琴家[奥伊斯特拉赫](../Page/奥伊斯特拉赫.md "wikilink")、作曲家[布里顿](../Page/本杰明·布里顿.md "wikilink")、大提琴家[罗斯特洛波维奇](../Page/罗斯特洛波维奇.md "wikilink")、小提琴家[柯岗](../Page/列奥尼德·柯岗.md "wikilink")、中提琴家、大提琴家等，都有相當動人以及珍貴的演奏記錄。

## 录音

[richter_WT.jpg](https://zh.wikipedia.org/wiki/File:richter_WT.jpg "fig:richter_WT.jpg")
里赫特著名的唱片录音包括了舒伯特、贝多芬、巴赫（里赫特自称用一个月的时间背谱学会了巴赫的[平均律第二册](../Page/平均律键盘曲集.md "wikilink")）、肖邦、李斯特、普罗科菲耶夫、拉赫玛尼诺夫、[斯克里亚宾等人的作品](../Page/斯克里亚宾.md "wikilink")。

里希特的手很大，可以掌握12度音。他喜欢现场演奏，因此有着数不清的现场演奏录音，不論是正式或非正式，每年仍有大量的錄音出现，可說是生前死後都引領风骚的一代大家。尽管留下了这么多唱片，里赫特本人其實很讨厌录音。[格伦·古尔德称他为这个时代最强的音乐交流者](../Page/格伦·古尔德.md "wikilink")，而只有在音乐会上，里赫特的音乐天才方能充分发挥出来。

## 参考资料

## 延伸阅读

  -
## 外部链接

  -
  - [里赫特唱片列表（包括未发行和私人录音）](http://www.doremi.com/sr.html)

[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:列寧獎獲得者](../Category/列寧獎獲得者.md "wikilink")
[Category:斯大林獎獲得者](../Category/斯大林獎獲得者.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:十月革命勳章獲得者](../Category/十月革命勳章獲得者.md "wikilink")
[Category:俄羅斯聯邦國家獎獲得者](../Category/俄羅斯聯邦國家獎獲得者.md "wikilink")
[R](../Category/葛萊美獎獲得者.md "wikilink")
[Category:蘇聯人民藝術家](../Category/蘇聯人民藝術家.md "wikilink")
[Category:蘇聯鋼琴家](../Category/蘇聯鋼琴家.md "wikilink")
[Category:俄羅斯鋼琴家](../Category/俄羅斯鋼琴家.md "wikilink")
[Category:烏克蘭鋼琴家](../Category/烏克蘭鋼琴家.md "wikilink")
[R](../Category/LGBT音樂家.md "wikilink")
[Category:莫斯科音樂學院校友](../Category/莫斯科音樂學院校友.md "wikilink")
[Category:德國裔蘇聯人](../Category/德國裔蘇聯人.md "wikilink")
[Category:德國裔俄羅斯人](../Category/德國裔俄羅斯人.md "wikilink")
[Category:德國裔烏克蘭人](../Category/德國裔烏克蘭人.md "wikilink")
[Category:俄羅斯裔烏克蘭人](../Category/俄羅斯裔烏克蘭人.md "wikilink")
[Category:蘇聯LGBT人物](../Category/蘇聯LGBT人物.md "wikilink")
[Category:安葬於新聖女公墓者](../Category/安葬於新聖女公墓者.md "wikilink")
[Category:日托米爾人](../Category/日托米爾人.md "wikilink")

1.
2.
3.
4.
5.
6.