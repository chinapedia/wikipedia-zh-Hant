**斯蒂法诺·菲奥雷**（**Stefano
Fiore**，），是一名[意大利足球員](../Page/意大利.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，現時效力[意乙球會](../Page/意乙.md "wikilink")[文多瓦](../Page/曼托瓦足球俱樂部.md "wikilink")。

## 生平

### 球會

費奧利出身於意大利小球會哥辛沙，兩季後轉投帕爾馬，但只上陣8場，之後轉至帕多瓦、基爾禾等小球會效力。1999年費奧利加盟[意甲](../Page/意甲.md "wikilink")[烏甸尼斯](../Page/烏甸尼斯.md "wikilink")，首季上陣33場聯賽取得9個入球，翌季34賽聯賽入9球，兩季合共攻入18球，是現時為止費奧利在足球生涯中得到最多入球的兩季。

2000-01年球季，費奧利轉投意甲首都球會拉素，在這支[羅馬球會三季間分別得到](../Page/羅馬.md "wikilink")3球、5球以及8球入球。這時拉素出現財政困難，除大量球員被欠薪外，還要不斷出售球員套現，結果2004年費奧利被售往[西甲球會華倫西亞](../Page/西甲.md "wikilink")。2005-06年球季，費奧利被外借至[費倫天拿](../Page/費倫天拿.md "wikilink")，只效力了一季；一年後他再被外借到[拖連奴](../Page/拖連奴.md "wikilink")，也是效力一季。

由於年事已高，離開拖連奴後他轉到意乙，加盟[文多瓦](../Page/曼托瓦足球俱樂部.md "wikilink")。

### 國家隊

費奧利首次為意大利上陣是於2000年2月對[瑞典的友賽](../Page/瑞典國家足球隊.md "wikilink")，在[2000年歐洲足球錦標賽的分組賽中](../Page/2000年歐洲足球錦標賽.md "wikilink")，費奧利對[比利時時射入](../Page/比利時國家足球隊.md "wikilink")1球，助國家隊以2比0贏得3分。

[2002年世界杯前](../Page/2002年世界杯足球賽.md "wikilink")，費奧利因於球會的表現欠佳，而未能入選國家隊，及後在2004年，費奧利的表現回勇，再次成為[歐洲國家盃的球員之一](../Page/歐洲國家盃.md "wikilink")，但這次後備上陣兩場，未有入球。直到現在，費奧利共為國家隊上陣38場，攻入兩球。

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:帕爾馬球員](../Category/帕爾馬球員.md "wikilink")
[Category:帕多瓦球員](../Category/帕多瓦球員.md "wikilink")
[Category:基爾禾球員](../Category/基爾禾球員.md "wikilink")
[Category:烏甸尼斯球員](../Category/烏甸尼斯球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:拖連奴球員](../Category/拖連奴球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")