**斯蒂芬·科尔·克莱尼**（，）美國[數學家](../Page/數學家.md "wikilink")、[逻辑學家](../Page/逻辑學家.md "wikilink")，主要从事對[可計算函數的研究](../Page/可計算函數.md "wikilink")，而他的遞歸理論研究有助於奠定[理論電腦科學的基礎](../Page/理論電腦科學.md "wikilink")。他為數學[直覺主義的基礎做出了重要貢獻](../Page/直覺主義.md "wikilink")，[克莱尼層次結構](../Page/算数阶层.md "wikilink")、[克莱尼代数](../Page/克莱尼代数.md "wikilink")、[克莱尼星号](../Page/Kleene星号.md "wikilink")（克莱尼閉包）、[克莱尼遞歸定理和](../Page/克莱尼遞歸定理.md "wikilink")[克莱尼不動點定理數學概念以他的名字命名](../Page/克莱尼不動點定理.md "wikilink")。他也是[正規表示法的發明者](../Page/正規表示法.md "wikilink")。

## 生平

斯蒂芬·科尔·克莱尼出生於美國[康涅狄克州的](../Page/康涅狄克州.md "wikilink")[哈特福德](../Page/哈特福德.md "wikilink")。他的父親古斯塔夫·克莱尼（Gustav
Adolph Kleene）是一位[經濟學教授](../Page/經濟學.md "wikilink")，母親艾麗絲·科爾（Alice Lena
Cole）是一位[詩人](../Page/詩人.md "wikilink")。

克莱尼於1930年在[阿默斯特學院獲得學士學位](../Page/阿默斯特學院.md "wikilink")，於1934年在[普林斯頓大學獲得數學博士學位](../Page/普林斯頓大學.md "wikilink")。他的導師[阿隆佐·邱奇是著名的邏輯學家](../Page/阿隆佐·邱奇.md "wikilink")。丘奇在1936年使用[lambda演算來證明了判定問題是沒有答案的](../Page/lambda演算.md "wikilink")；克莱尼的博士論文題目為“形式邏輯中的正整數理論”。

在20世紀30年代，他在丘奇的lambda演算上做了重要的工作。
1935年，他加入了[威斯康辛大學麥迪遜分校數學系](../Page/威斯康辛大學麥迪遜分校.md "wikilink")，在那裡度過了他幾乎所有的職業生涯。在擔任導師兩年後，他於1937年被任命為助理教授。1939年至1940年間，他是[普林斯頓高等研究所的訪問學者](../Page/普林斯頓高等研究所.md "wikilink")。他為遞歸理論奠定了基礎，這個研究領域將是他一生的研究興趣。1941年，他回到了阿默斯特學院並在那任職了一年的數學副教授。

克莱尼在[二戰期間是美國海軍少校](../Page/二戰.md "wikilink")，他是紐約[美國海軍](../Page/美國海軍.md "wikilink")[預備役軍艦學校的導航教官](../Page/預備役.md "wikilink")，之後是[華盛頓海軍研究實驗室的專案主任](../Page/美國海軍研究實驗室.md "wikilink")。克莱尼1946年回到[威斯康辛州](../Page/威斯康辛州.md "wikilink")，1948年成為正教授，1964年成為“Cyrus
C.
MacDuffee”數學教授。他于1962年到1963年担任數學與計算機科學系，1969年到1974年担任文學院與科學院院長。儘管當時的[越南戰爭引起了學生們的不安](../Page/越南戰爭.md "wikilink")，但他還是接受了後者的任命。他於1979年從威斯康辛大學退休。1999年，威斯康辛大學的數學圖書館以他的榮譽而更名。\[1\]

克莱尼在威斯康辛州的教學產生了3篇关于[數學邏輯的文章](../Page/數學邏輯.md "wikilink")，一篇发表于1952年，一篇发布于1967年，还有一篇和Vesley合作的发布于1965年。前2個經常被引用，仍然在印刷中。克莱尼1952年的论文給[哥德爾不完備定理提供了另外的證明](../Page/哥德爾不完備定理.md "wikilink")，這些定理增強了其規範地位，使其更容易教導和理解。克莱尼和Vesley合写于1965年的论文是美國人對直覺主義邏輯和數學的經典介紹。克莱尼曾于1956年至1988年擔任符號邏輯協會（Association
for Symbolic Logic）主席，1961年擔任國際科學史與科學哲學聯盟（International Union of History
and Philosophy of
Science）主席\[2\]。1990年，他被授予[國家科學獎章](../Page/國家科學獎章.md "wikilink")。

## 作品

  -
  - \[3\]

  -
## 个人生活

克莱尼和妻子Nancy
Elliott有4個孩子。他畢生熱愛[緬因州的家庭農場](../Page/緬因州.md "wikilink")。他是一名熱心的[登山者](../Page/登山.md "wikilink")，對自然和環境有濃厚的興趣，並積極參與許多[环境保護事業](../Page/环境保護.md "wikilink")。

## 参考资料

[Category:20世纪逻辑学家](../Category/20世纪逻辑学家.md "wikilink")
[Category:美國邏輯學家](../Category/美國邏輯學家.md "wikilink")
[Category:数理逻辑学家](../Category/数理逻辑学家.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:阿默斯特學院校友](../Category/阿默斯特學院校友.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")

1.
2.  ；也叫做“国际科学历史与哲学联盟”（"International Union of the History and the
    Philosophy of
    Science"），是[国际科学理事会的成员之一](../Page/国际科学理事会.md "wikilink")
    (曾名[国际科学联合会理事会](../Page/国际科学联合会理事会.md "wikilink"))。
3.