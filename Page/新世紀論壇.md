**新世紀論壇**（），簡稱**新論壇**，香港[建制派政黨](../Page/建制派.md "wikilink")，於香港慶祝澳門回歸中國活動告一段落之後成立，成員多數是[專業人士](../Page/專業人士.md "wikilink")、[工](../Page/工.md "wikilink")[商](../Page/商.md "wikilink")[金融及服務業界](../Page/金融.md "wikilink")、[中小型企業和](../Page/中小企.md "wikilink")[學者等](../Page/教授.md "wikilink")。新世紀論壇不時會發表一些[社會問題的](../Page/社會問題.md "wikilink")[研究報告](../Page/研究報告.md "wikilink")，或者回應[香港政府的](../Page/香港政府.md "wikilink")[公共政策](../Page/公共政策.md "wikilink")，所以也可以稱為香港的[智庫](../Page/智庫.md "wikilink")。

雖然該組織曾經參選[香港立法會地區直選](../Page/香港立法會.md "wikilink")（尤其是2000年的[藍鴻震和](../Page/藍鴻震.md "wikilink")2004年的[呂孝端這兩位前政府高官](../Page/呂孝端.md "wikilink")，之後2008年亦有[龐愛蘭](../Page/龐愛蘭.md "wikilink")），但是一直未有能夠取得席位。但2012年除了派出[龐愛蘭外](../Page/龐愛蘭.md "wikilink")，又於功能界別派出[馬逢國](../Page/馬逢國.md "wikilink")，最後只有馬當選。

## 議員

### 立法會議員

<div style="overflow:auto; width:100%">

| 席位                                       | 選區／界別                                                                                        | 議員                                                                                           | 1998-2000                                                                                    | 2000-2004 | 2004-2008 | 2008-2012 | 2012-2016                                                                                    | 2016-2020                                                                                    | 備註 |
| ---------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | --------- | --------- | --------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -- |
| [地方選區](../Page/地方選區.md "wikilink")       | [新界西](../Page/新界西選區.md "wikilink")                                                           | |[何君堯](../Page/何君堯.md "wikilink")                                                            |                                                                                              |           |           |           |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |    |
| [功能界別](../Page/功能界別.md "wikilink")       | [體育、演藝、文化及出版界](../Page/體育、演藝、文化及出版界功能界別.md "wikilink")                                       | [馬逢國](../Page/馬逢國.md "wikilink")                                                             |                                                                                              |           |           |           | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |    |
| [選舉委員會](../Page/選舉委員會功能界別.md "wikilink") | [吳清輝](../Page/吳清輝.md "wikilink")                                                             | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |           |           |           |                                                                                              |                                                                                              |    |
| [馬逢國](../Page/馬逢國.md "wikilink")         | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |           |           |           |                                                                                              |                                                                                              |    |
| **所得議席**                                 | 2                                                                                            | 1                                                                                            | 0                                                                                            | 0         | 1         | 2         |                                                                                              |                                                                                              |    |

</div>

### 區議員

2016年至2019年，新世紀論壇在4個區議會共有7個民選議席，議員包括：

| 區議會                                 | 選區號碼 | 選區                               | 議員                               | 備註                                                    |
| ----------------------------------- | ---- | -------------------------------- | -------------------------------- | ----------------------------------------------------- |
| [中西區](../Page/中西區區議會.md "wikilink") | A05  | 大學                               | [陳捷貴](../Page/陳捷貴.md "wikilink") |                                                       |
| A10                                 | 石塘咀  | [陳財喜](../Page/陳財喜.md "wikilink") |                                  |                                                       |
| [觀塘區](../Page/觀塘區議會.md "wikilink")  | J25  | 麗港城                              | [鄧詠駿](../Page/鄧詠駿.md "wikilink") |                                                       |
| J29                                 | 月華   | [徐海山](../Page/徐海山.md "wikilink") |                                  |                                                       |
| J31                                 | 康樂   | [馬軼超](../Page/馬軼超.md "wikilink") |                                  |                                                       |
| [屯門區](../Page/屯門區議會.md "wikilink")  | L19  | 樂翠                               | [何君堯](../Page/何君堯.md "wikilink") | 同時為[新界關注大聯盟成員](../Page/新界關注大聯盟.md "wikilink")，兼任立法會議員 |
| [沙田區](../Page/沙田區議會.md "wikilink")  | R22  | 火炭                               | [龐愛蘭](../Page/龐愛蘭.md "wikilink") |                                                       |

## 選舉

### 立法會選舉

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>民選得票</p></th>
<th><p>民選得票比例</p></th>
<th><p>地方選區議席</p></th>
<th><p>功能界別議席</p></th>
<th><p>總議席</p></th>
<th><p>增減</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1998年香港立法會選舉.md" title="wikilink">1998</a></p></td>
<td><p>0</p></td>
<td><p>0.00%</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td></td>
<td><p>2 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年香港立法會選舉.md" title="wikilink">2000</a></p></td>
<td><p>21,103 </p></td>
<td><p>1.60% </p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年香港立法會選舉.md" title="wikilink">2004</a></p></td>
<td><p>4,511 </p></td>
<td><p>0.25% </p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年香港立法會選舉.md" title="wikilink">2012</a></p></td>
<td><p>0</p></td>
<td><p>0.00%</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年香港立法會選舉.md" title="wikilink">2016</a></p></td>
<td><p>35657 </p></td>
<td><p>1.64% </p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
</tbody>
</table>

### 區議會選舉

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>民選得票</p></th>
<th><p>民選得票比例</p></th>
<th><p>民選議席</p></th>
<th><p>委任議席</p></th>
<th><p>當然議席</p></th>
<th><p>總議席</p></th>
<th><p>增減</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/2003年香港區議會選舉.md" title="wikilink">2003</a></p></td>
<td><p>833</p></td>
<td><p>0.08%</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年香港區議會選舉.md" title="wikilink">2007</a></p></td>
<td><p>8,821 </p></td>
<td><p>0.77% </p></td>
<td><p>7</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>7 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年香港區議會選舉.md" title="wikilink">2011</a></p></td>
<td><p>9,915 </p></td>
<td><p>0.83% </p></td>
<td><p>8</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年香港區議會選舉.md" title="wikilink">2015</a></p></td>
<td><p>15,482 </p></td>
<td><p>1.07% </p></td>
<td><p>6</p></td>
<td></td>
<td><p>0</p></td>
<td></td>
<td><p>2 </p></td>
</tr>
</tbody>
</table>

## 代表人物

  - [吳清輝](../Page/吳清輝.md "wikilink")
  - [馬逢國](../Page/馬逢國.md "wikilink")
  - [藍鴻震](../Page/藍鴻震.md "wikilink")

## 外部連結

  - [新世紀論壇 官方網址](http://www.ncforum.org.hk/)

[Category:香港政黨](../Category/香港政黨.md "wikilink")
[Category:香港智库](../Category/香港智库.md "wikilink")
[Category:香港建制派組織](../Category/香港建制派組織.md "wikilink")