**新中國未來記**，[清朝](../Page/清朝.md "wikilink")[光緒二十八年](../Page/光緒.md "wikilink")（1902年）[梁啟超發表的政治小說](../Page/梁啟超.md "wikilink")。

《新中國未來記》開文“话表孔子降生后2513年，即西历2062年”，中国维新成功，諸友邦均遣使前來慶賀，這部小說預言其後六十年後「[新中國](../Page/新中國.md "wikilink")」的壯盛繁榮，僅撰成5回，其中一位主人公名叫[黃克強](../Page/黃克強.md "wikilink")([黃興](../Page/黃興.md "wikilink"))，主張君主立憲；另一位[李去病](../Page/李去病.md "wikilink")([李國杰](../Page/李國杰.md "wikilink"))主張法蘭西式革命，雙方展開激烈辯論。

## 受外國小說影響

《新中國未來記》在形式上很明顯的受到英國小說《[百年一覺](../Page/百年一覺.md "wikilink")》和日本小說《[雪中梅](../Page/雪中梅.md "wikilink")》的影響，1902年梁啟超發表《新中國未來記》時說：“余欲著此書，五年於玆矣”，“未來記”是日本明治時期政治小說常見的形式，如[廣末鐵腸](../Page/廣末鐵腸.md "wikilink")《[二十三年未來記](../Page/二十三年未來記.md "wikilink")》，新中國未來記刊出時，即標明為政治小說。

### 日本[明治小說的影響](../Page/明治小說.md "wikilink")

#### [柴四郎](../Page/柴四郎.md "wikilink")《[佳人奇遇](../Page/佳人奇遇.md "wikilink")》

#### [矢野龍溪](../Page/矢野龍溪.md "wikilink")《[經國美談](../Page/經國美談.md "wikilink")》

#### [末廣鐵腸](../Page/末廣鐵腸.md "wikilink")《[二三十年未來記](../Page/二三十年未來記.md "wikilink")》《[雪中梅](../Page/雪中梅.md "wikilink")》《[花間鶯](../Page/花間鶯.md "wikilink")》

#### [福地櫻癡](../Page/福地櫻癡.md "wikilink")《[或許草紙](../Page/或許草紙.md "wikilink")》

### 英國小說《[百年一覺](../Page/百年一覺.md "wikilink")》

## 主要角色

### [黃興](../Page/黃興.md "wikilink")

### [李國杰](../Page/李國杰.md "wikilink")

### [康有為](../Page/康有為.md "wikilink")

### [譚嗣同](../Page/譚嗣同.md "wikilink")

### [朱九江](../Page/朱九江.md "wikilink")

## 故事結構

## 影響

### [陸士諤](../Page/陸士諤.md "wikilink")《[新中國](../Page/新中國.md "wikilink")》

### [碧荷館主人](../Page/碧荷館主人.md "wikilink")《[新紀元](../Page/新紀元.md "wikilink")》

### [無名氏](../Page/無名氏.md "wikilink")《[未來世界](../Page/未來世界.md "wikilink")》

## 評價

  - [夏志清認為作者從第四回開始就](../Page/夏志清.md "wikilink")「靈感枯竭……放棄了原先的演說格式，開始用敘述手法」。
  - 《新中國未來記》可以說是以中國近代為主要素材，以日本人的創意為第一加添的元素，因此其預言除了對[北洋政府時期產生影響外](../Page/北洋政府.md "wikilink")，也影響了日本的「[大東亞共榮圈](../Page/大東亞共榮圈.md "wikilink")」，對於中日整合的問題提出相當大規格的未來構想，也為[中共中央總書記](../Page/中共中央總書記.md "wikilink")[習近平的](../Page/習近平.md "wikilink")「[一帶一路](../Page/一帶一路.md "wikilink")」鋪路，對[亞洲地區的整合提供了一種最早的藍圖](../Page/亞洲地區.md "wikilink")。

## 參考資料

  - 寇振鋒「《新中國未來記》中的日本明治政治小說因素」
  - 王德威：〈[小说作为“革命”-重读梁启超《新中国未来记》](http://www.nssd.org/articles/article_read.aspx?id=662552955)〉。

[Category:清朝小說](../Category/清朝小說.md "wikilink")
[Category:梁启超作品](../Category/梁启超作品.md "wikilink")
[Category:1902年長篇小說](../Category/1902年長篇小說.md "wikilink")