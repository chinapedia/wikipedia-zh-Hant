《**新五代史**》，[北宋](../Page/北宋.md "wikilink")[歐陽脩撰](../Page/歐陽脩.md "wikilink")，是[唐代以後唯一私修](../Page/唐朝.md "wikilink")[正史](../Page/正史.md "wikilink")。

[尹洙与欧阳修打算合撰](../Page/尹洙.md "wikilink")《新五代史》\[1\]，但因史觀不同而作罷，尹洙后来独撰两卷的《五代春秋》\[2\]。[宋仁宗](../Page/宋仁宗.md "wikilink")[皇祐五年](../Page/皇祐.md "wikilink")（1053年）《新五代史》成书\[3\]，原名《五代史记》。因為私撰，故藏於家。脩卒，家人上呈於朝廷。

[乾隆时](../Page/乾隆.md "wikilink")，因[薛居正](../Page/薛居正.md "wikilink")《[舊五代史](../Page/舊五代史.md "wikilink")》列為正史，歐史改稱《新五代史》。共七十四卷，[本紀十二卷](../Page/本紀.md "wikilink")、[列傳四十五卷](../Page/列傳.md "wikilink")、[考三卷](../Page/志.md "wikilink")、[世家及](../Page/世家.md "wikilink")[年譜十一卷](../Page/年譜.md "wikilink")、四夷附錄三卷。記載自[後梁](../Page/後梁.md "wikilink")[開平元年](../Page/開平_\(朱全忠\).md "wikilink")（907年）至[後周](../Page/後周.md "wikilink")[顯德七年](../Page/顯德.md "wikilink")（960年）歷史。

《新五代史》撰寫時，增加了《[舊五代史](../Page/舊五代史.md "wikilink")》所未能見到的史料，如《[五代會要](../Page/五代會要.md "wikilink")》、《[五代史補](../Page/五代史補.md "wikilink")》、《[五代史闕文](../Page/五代史闕文.md "wikilink")》、《[唐餘錄](../Page/唐餘錄.md "wikilink")》、《[九國志](../Page/九國志.md "wikilink")》等，因此《新五代史》在《舊五代史》的基礎上更加詳實。如王景仁、郭崇韜、安重誨、李茂貞、孔謙、王彦章、段凝、趙在禮、范延光、盧文紀、馬胤孫、姚顗、崔税、吕琦、楊渥等傳內容都有補強。但《新五代史》對舊“志”部分大加繁削，則不足為訓，故史料價值比《舊五代史》要略遜一籌。

## 體例

《新五代史》其中的列傳實為“類傳”，採用類比方式，最有特色，有：《家人傳》、《臣傳》、《死節傳》、《死事傳》、《一行傳》、《唐六臣傳》、《義兒傳》、《伶官傳》、《雜傳》等，然其《死節傳》僅湊得三人，《死事傳》十一人，遠少於《[冊府元龜](../Page/冊府元龜.md "wikilink")》所載人數；又如《唐六臣傳》皆唐末助[朱溫篡唐者](../Page/朱溫.md "wikilink")，雖名為唐臣，諷刺意味深遠。

《新五代史》改“志”為“考”，因為歐陽脩認為五代的典章制度一无可取，故将“志”删除，仅有《司天考》、《职方考》二考，相当於《旧五代史》的《天文志》和《郡县志》。

《新五代史》新增史料最多的是《十国世家》。十國稱為〈世家〉，並有《十國世家年譜》，載有吴、南唐、前蜀、后蜀、南汉、楚、吴越、闽、南平、东汉等十国。《四夷附录》相当于《旧五代史》的《外国列传》。

[金章宗](../Page/金章宗.md "wikilink")[泰和七年](../Page/泰和_\(金\).md "wikilink")（1207年），明令立《新五代史》於[學官](../Page/學官.md "wikilink")，从此大行于世，于是“薛史”渐湮不传。歐陽脩的學生[徐無黨曾為](../Page/徐無黨.md "wikilink")《新五代史》作注，多阐述微言大意。但是《新五代史》的史料價值比《舊五代史》要少一些，這主要是歐陽脩在刪繁就簡時，將不少具體資料也一併刪去。另外歐陽脩寫《新五代史》時，踵[孔子的](../Page/孔子.md "wikilink")《[春秋](../Page/春秋_\(史書\).md "wikilink")》筆法，為尊者諱，寓褒貶，重議論。並強調君臣倫理思想，史論常用“**嗚呼**”二字發端，發表感慨議論。

## 評價

[顧炎武](../Page/顧炎武.md "wikilink")、[錢大昕等學者對](../Page/錢大昕.md "wikilink")《新五代史》都有批評，認為歐陽脩迂腐，既著史書，卻不重視史實的探討，好發議論，愛說空話，讀來令人生厭。[章学诚讥称](../Page/章学诚.md "wikilink")：“只是一部吊祭哀挽之集，如何可称史才？”

宋人亦對歐陽脩的史學有所批評。《[宋稗类钞](../Page/宋稗类钞.md "wikilink")》中引[劉敞](../Page/劉敞.md "wikilink")“好个欧九，极有文章，可惜不甚读书”之语\[4\]，《[苕溪渔隐丛话](../Page/苕溪渔隐丛话.md "wikilink")》亦引[王安石的](../Page/王安石.md "wikilink")“欧九不学”的话。一日，[刘攽問歐的弟子](../Page/刘攽.md "wikilink")[焦千之说](../Page/焦千之.md "wikilink")《新五代史》有為[韓通立傳嗎](../Page/韓通_\(後周\).md "wikilink")？焦說沒有，刘攽大笑說：“如此，亦是第二等文字耳。”\[5\]

但[王鳴盛高度評價](../Page/王鳴盛.md "wikilink")《新五代史·職方考》：「五代土地，梁爲最小，晉、漢差大，周又大，而唐爲最大……觀歐《職方考》自明，此考雖簡略，然提綱挈領，洗眉刷目，此則歐公筆力非薛《史》所能及。」\[6\]。[趙翼亦稱](../Page/趙翼.md "wikilink")“卷跌雖不及薛史之半，而訂正之功倍之”\[7\]。

[章太炎評](../Page/章太炎.md "wikilink")《新五代史》：“歐陽脩作《五代史記》，自負上法《春秋》，於唐本紀大書契丹立晉，為通人所笑。此學《春秋》而誤也。《春秋》書法，本不可學，‘衛人立晉’-{云}-者，晉為衛宣之名，今契丹所立之晉，國名而非人名。東家之顰，不亦醜乎？”是故《新五代史》以文學價值見長，並不能取代以史學價值為主的《舊五代史》。

當時學者[吳縝撰](../Page/吴缜.md "wikilink")《[五代史纂誤](../Page/五代史纂誤.md "wikilink")》三卷，是糾舉《新五代史》謬誤的專著。[清代](../Page/清朝.md "wikilink")[吳蘭庭撰](../Page/吳蘭庭.md "wikilink")《五代史記纂誤補》六卷，乃吳縝《纂誤》之續作。[彭元瑞](../Page/彭元瑞.md "wikilink")、[劉鳳誥有](../Page/劉鳳誥.md "wikilink")《五代史記注》。

## 内容

### 本紀

1.  [梁本紀第一](../Page/:wikisource:zh:新五代史/卷01.md "wikilink")-
    [太祖上](../Page/朱全忠.md "wikilink")
2.  梁本紀第二 - 太祖下
3.  梁本紀第三 - [末帝](../Page/朱友貞.md "wikilink")
4.  [唐本紀第四](../Page/:wikisource:zh:新五代史/卷04.md "wikilink") -
    [莊宗上](../Page/李存勗.md "wikilink")
5.  唐本紀第五 - 莊宗下
6.  唐本紀第六 - [明宗](../Page/李嗣源.md "wikilink")
7.  唐本紀第七 -
    [閔帝](../Page/李从厚.md "wikilink")・[废帝](../Page/李从珂.md "wikilink")
8.  [晋本紀第八](../Page/:wikisource:zh:新五代史/卷08.md "wikilink") -
    [高祖](../Page/石敬瑭.md "wikilink")
9.  晋本紀第九 - [出帝](../Page/石重貴.md "wikilink")
10. [漢本紀第十](../Page/:wikisource:zh:新五代史/卷10.md "wikilink") -
    [高祖](../Page/劉知遠.md "wikilink")・[隐帝](../Page/劉承祐.md "wikilink")
11. [周本紀第十一](../Page/:wikisource:zh:新五代史/卷11.md "wikilink") -
    [太祖](../Page/郭威.md "wikilink")
12. 周本紀第十二 -
    [世宗](../Page/柴荣.md "wikilink")・[恭帝](../Page/柴宗訓.md "wikilink")

### 传

1.  [梁家人传第一](../Page/:wikisource:zh:新五代史/卷13.md "wikilink") -
    [王皇后](../Page/王皇后_\(後梁\).md "wikilink")・[張皇后](../Page/張皇后_\(後梁\).md "wikilink")・[朱全昱](../Page/朱全昱.md "wikilink")・[朱友諒](../Page/朱友諒.md "wikilink")・[朱友能](../Page/朱友能.md "wikilink")・[朱友誨](../Page/朱友誨.md "wikilink")・[朱存](../Page/朱存.md "wikilink")・[朱友寧](../Page/朱友寧.md "wikilink")・[朱友倫](../Page/朱友倫.md "wikilink")・[朱友裕](../Page/朱友裕.md "wikilink")・[朱友文](../Page/朱友文.md "wikilink")・[朱友孜](../Page/朱友孜.md "wikilink")
2.  [唐太祖家人传第二](../Page/:wikisource:zh:新五代史/卷14.md "wikilink") -
    [劉皇后](../Page/劉玉娘.md "wikilink")・[李克让](../Page/李克让.md "wikilink")・[李克修](../Page/李克修.md "wikilink")・[李克恭](../Page/李克恭.md "wikilink")・[李克寧](../Page/李克宁_\(五代\).md "wikilink")・[李继岌](../Page/李继岌.md "wikilink")・[李继潼](../Page/李继潼.md "wikilink")・[李继嵩](../Page/李继嵩.md "wikilink")・[李继蟾](../Page/李继蟾.md "wikilink")・[李继嶢](../Page/李继嶢.md "wikilink")
3.  唐明宗家人传第三 -
    [曹皇后](../Page/曹皇后_\(後唐\).md "wikilink")・[魏皇后](../Page/魏皇后_\(後唐\).md "wikilink")・[李从璟](../Page/李从璟.md "wikilink")・[李从荣](../Page/李从荣.md "wikilink")・[李从璨](../Page/李从璨.md "wikilink")・[李从璋](../Page/李从璋.md "wikilink")・[李从温](../Page/李从温.md "wikilink")・[李从敏](../Page/李从敏.md "wikilink")
4.  唐废帝家人传第四 -
    [劉皇后](../Page/劉皇后_\(後唐废帝\).md "wikilink")・[李重吉](../Page/李重吉.md "wikilink")・[李重美](../Page/李重美.md "wikilink")
5.  [晋家人传第五](../Page/:wikisource:zh:新五代史/卷17.md "wikilink") -
    [石敬威](../Page/石敬威.md "wikilink")・[石敬贇](../Page/石敬贇.md "wikilink")・[石敬暉](../Page/石敬暉.md "wikilink")・[石重英](../Page/石重英.md "wikilink")・[石重信](../Page/石重信.md "wikilink")・[石重乂](../Page/石重乂.md "wikilink")・[石重睿](../Page/石重睿.md "wikilink")・[石重杲](../Page/石重杲.md "wikilink")・[石延煦](../Page/石延煦.md "wikilink")・[石延宝](../Page/石延宝.md "wikilink")
6.  [漢家人传第六](../Page/:wikisource:zh:新五代史/卷18.md "wikilink") -
    [劉崇](../Page/劉崇.md "wikilink")・[劉信](../Page/劉信.md "wikilink")・[劉承訓](../Page/劉承訓.md "wikilink")・[劉承祐](../Page/劉承祐.md "wikilink")・[劉承勋](../Page/劉承勋.md "wikilink")
7.  周太祖家人传第七 - [柴皇后](../Page/柴皇后_\(後周\).md "wikilink")
8.  [周世宗家人传第八](../Page/:wikisource:zh:新五代史/卷20.md "wikilink") -
    [柴守礼](../Page/柴守礼.md "wikilink")・[劉皇后](../Page/劉皇后_\(後周\).md "wikilink")・[符皇后](../Page/符皇后_\(後周\).md "wikilink")・[柴誼](../Page/柴誼.md "wikilink")・[柴誠](../Page/柴誠.md "wikilink")・[柴熙讓](../Page/柴熙讓.md "wikilink")・[柴熙謹](../Page/柴熙謹.md "wikilink")・[柴熙誨](../Page/柴熙誨.md "wikilink")
9.  [梁臣传第九](../Page/:wikisource:zh:新五代史/卷21.md "wikilink") -
    [敬翔](../Page/敬翔.md "wikilink")・[朱珍](../Page/朱珍.md "wikilink")・[龐師古](../Page/龐師古.md "wikilink")・[葛从周](../Page/葛从周.md "wikilink")・[霍存](../Page/霍存.md "wikilink")・[張存敬](../Page/張存敬.md "wikilink")・[符道昭](../Page/符道昭.md "wikilink")・[劉捍](../Page/劉捍.md "wikilink")・[寇彦卿](../Page/寇彦卿.md "wikilink")
10. 梁臣传第十 -
    [康怀英](../Page/康怀英.md "wikilink")・[劉鄩](../Page/劉鄩.md "wikilink")・[牛存節](../Page/牛存節.md "wikilink")・[張归霸](../Page/張归霸.md "wikilink")・[王重師](../Page/王重師.md "wikilink")・[徐怀玉](../Page/徐怀玉.md "wikilink")
11. 梁臣传第十一 -
    [楊師厚](../Page/楊師厚.md "wikilink")・[王景仁](../Page/王景仁.md "wikilink")・[賀瓌](../Page/賀瓌.md "wikilink")・[王檀](../Page/王檀.md "wikilink")・[馬嗣勋](../Page/馬嗣勋.md "wikilink")・[王虔裕](../Page/王虔裕.md "wikilink")・[謝彦章](../Page/謝彦章.md "wikilink")
12. [唐臣传第十二](../Page/:wikisource:zh:新五代史/卷24.md "wikilink") -
    [郭崇韜](../Page/郭崇韜.md "wikilink")・[安重誨](../Page/安重誨.md "wikilink")
13. 唐臣传第十三 -
    [周德威](../Page/周德威.md "wikilink")・[符存審](../Page/符存審.md "wikilink")・[史建瑭](../Page/史建瑭.md "wikilink")・[王建及](../Page/王建及.md "wikilink")・[元行欽](../Page/元行欽.md "wikilink")・[安金全](../Page/安金全.md "wikilink")・[袁建丰](../Page/袁建丰.md "wikilink")・[西方鄴](../Page/西方鄴.md "wikilink")
14. 唐臣传第十四 -
    [符習](../Page/符習.md "wikilink")・[烏震](../Page/烏震.md "wikilink")・[孔謙](../Page/孔謙_\(五代\).md "wikilink")・[張延朗](../Page/張延朗.md "wikilink")・[李嚴](../Page/李嚴.md "wikilink")・[李仁矩](../Page/李仁矩.md "wikilink")・[毛璋](../Page/毛璋.md "wikilink")
15. 唐臣传第十五 -
    [朱弘昭](../Page/朱弘昭.md "wikilink")・[馮贇](../Page/馮贇.md "wikilink")・[劉延朗](../Page/劉延朗.md "wikilink")・[康思立](../Page/康思立.md "wikilink")・[康義誠](../Page/康義誠.md "wikilink")・[药彦稠](../Page/药彦稠.md "wikilink")
16. 唐臣传第十六 -
    [豆盧革](../Page/豆盧革.md "wikilink")・[盧程](../Page/盧程.md "wikilink")・[任圜](../Page/任圜.md "wikilink")・[趙鳳](../Page/趙鳳.md "wikilink")・[李襲吉](../Page/李襲吉.md "wikilink")・[張憲](../Page/張憲.md "wikilink")・[蕭希甫](../Page/蕭希甫.md "wikilink")・[劉賛](../Page/劉賛.md "wikilink")・[何瓚](../Page/何瓚.md "wikilink")
17. [晋臣传第十七](../Page/:wikisource:zh:新五代史/卷29.md "wikilink") -
    [桑維翰](../Page/桑維翰.md "wikilink")・[景延广](../Page/景延广.md "wikilink")・[吴巒](../Page/吴巒.md "wikilink")
18. 漢臣传第十八 -
    [蘇逢吉](../Page/蘇逢吉.md "wikilink")・[史弘肇](../Page/史弘肇.md "wikilink")・[楊邠](../Page/楊邠.md "wikilink")・[王章](../Page/王章_\(五代\).md "wikilink")・[劉銖](../Page/劉銖.md "wikilink")・[李業](../Page/李業.md "wikilink")・[聶文進](../Page/聶文進.md "wikilink")・[後贊](../Page/後贊.md "wikilink")・[郭允明](../Page/郭允明.md "wikilink")
19. [周臣传第十九](../Page/:wikisource:zh:新五代史/卷31.md "wikilink") -
    [王朴](../Page/王朴.md "wikilink")・[鄭仁誨](../Page/鄭仁誨.md "wikilink")・[扈載](../Page/扈載.md "wikilink")
20. 死節传第二十 -
    [王彦章](../Page/王彦章.md "wikilink")・[裴約](../Page/裴約.md "wikilink")・[劉仁贍](../Page/劉仁贍.md "wikilink")
21. 死事传第二十一 -
    [張源德](../Page/張源德.md "wikilink")・[夏魯奇](../Page/夏魯奇.md "wikilink")・[姚洪](../Page/姚洪.md "wikilink")・[王思同](../Page/王思同.md "wikilink")・[張敬達](../Page/張敬達.md "wikilink")・[翟進宗](../Page/翟進宗.md "wikilink")・[張万迪](../Page/張万迪.md "wikilink")・[沈斌](../Page/沈斌.md "wikilink")・[王清](../Page/王清.md "wikilink")・[史彦超](../Page/史彦超.md "wikilink")・[孫晟](../Page/孫晟.md "wikilink")
22. 一行传第二十二 -
    [鄭遨](../Page/鄭遨.md "wikilink")・[張薦明](../Page/張薦明.md "wikilink")・[石昂](../Page/石昂.md "wikilink")・[程福贇](../Page/程福贇.md "wikilink")・[李自倫](../Page/李自倫.md "wikilink")
23. [唐六臣传第二十三](../Page/:wikisource:zh:新五代史/卷35.md "wikilink") -
    [張文蔚](../Page/張文蔚.md "wikilink")・[楊涉](../Page/楊涉.md "wikilink")・[張策](../Page/张策_\(五代\).md "wikilink")・[趙光逢](../Page/趙光逢.md "wikilink")・[薛貽矩](../Page/薛貽矩.md "wikilink")・[蘇循](../Page/蘇循.md "wikilink")
24. 義兒传第二十四 -
    [李嗣昭](../Page/李嗣昭.md "wikilink")・[李嗣本](../Page/李嗣本.md "wikilink")・[李嗣恩](../Page/李嗣恩.md "wikilink")・[李存信](../Page/李存信_\(武将\).md "wikilink")・[李存孝](../Page/李存孝_\(唐朝\).md "wikilink")・[李存進](../Page/李存進.md "wikilink")・[李存璋](../Page/李存璋.md "wikilink")・[李存賢](../Page/李存賢.md "wikilink")
25. 伶官传第二十五 -
    [周匝](../Page/周匝.md "wikilink")・[敬新磨](../Page/敬新磨.md "wikilink")・[景進](../Page/景進.md "wikilink")・[史彦瓊](../Page/史彦瓊.md "wikilink")・[郭从謙](../Page/郭从謙.md "wikilink")
26. 宦者传第二十六 -
    [張承業](../Page/张承业_\(五代\).md "wikilink")・[張居翰](../Page/張居翰.md "wikilink")
27. [杂传第二十七](../Page/:wikisource:zh:新五代史/卷39.md "wikilink") -
    [王鎔](../Page/王鎔.md "wikilink")・[羅紹威](../Page/羅紹威.md "wikilink")・[王处直](../Page/王处直.md "wikilink")・[劉守光](../Page/劉守光.md "wikilink")
28. 杂传第二十八 -
    [李茂貞](../Page/李茂貞.md "wikilink")・[韓建](../Page/韓建.md "wikilink")・[李仁福](../Page/李仁福.md "wikilink")・[韓遜](../Page/韓遜.md "wikilink")・[楊崇本](../Page/楊崇本.md "wikilink")・[高万興](../Page/高万興.md "wikilink")・[温韜](../Page/温韜.md "wikilink")
29. 杂传第二十九 -
    [盧光稠](../Page/盧光稠.md "wikilink")・[譚全播](../Page/譚全播.md "wikilink")・[雷满](../Page/雷满.md "wikilink")・[鍾传](../Page/鍾传.md "wikilink")・[趙匡凝](../Page/趙匡凝.md "wikilink")
30. [杂传第三十](../Page/:wikisource:zh:新五代史/卷42.md "wikilink") -
    [朱宣](../Page/朱瑄.md "wikilink")・[王師範](../Page/王師範.md "wikilink")・[李罕之](../Page/李罕之.md "wikilink")・[孟方立](../Page/孟方立.md "wikilink")・[王珂](../Page/王珂_\(唐朝\).md "wikilink")・[趙犨](../Page/趙犨.md "wikilink")・[馮行襲](../Page/馮行襲.md "wikilink")
31. 杂传第三十一 -
    [氏叔琮](../Page/氏叔琮.md "wikilink")・[李彦威](../Page/李彦威.md "wikilink")・[李振](../Page/李振.md "wikilink")・[裴迪](../Page/裴迪.md "wikilink")・[韋震](../Page/韋震.md "wikilink")・[孔循](../Page/孔循.md "wikilink")・[孫德昭](../Page/孫德昭.md "wikilink")・[王敬蕘](../Page/王敬蕘.md "wikilink")・[蒋殷](../Page/蒋殷.md "wikilink")
32. 杂传第三十二 -
    [劉知俊](../Page/劉知俊.md "wikilink")・[丁會](../Page/丁會.md "wikilink")・[賀德倫](../Page/賀德倫.md "wikilink")・[閻宝](../Page/閻宝.md "wikilink")・[康延孝](../Page/康延孝.md "wikilink")
33. 杂传第三十三 -
    [張全義](../Page/張全義.md "wikilink")・[朱友謙](../Page/朱友謙.md "wikilink")・[袁象先](../Page/袁象先.md "wikilink")・[朱漢賓](../Page/朱漢賓.md "wikilink")・[段凝](../Page/段凝.md "wikilink")・[刘玘](../Page/刘玘.md "wikilink")・[周知裕](../Page/周知裕.md "wikilink")・[陸思鐸](../Page/陸思鐸.md "wikilink")
34. 杂传第三十四 -
    [趙在礼](../Page/趙在礼.md "wikilink")・[霍彦威](../Page/霍彦威.md "wikilink")・[房知温](../Page/房知温.md "wikilink")・[王晏球](../Page/王晏球.md "wikilink")・[安重霸](../Page/安重霸.md "wikilink")・[王建立](../Page/王建立.md "wikilink")・[康福](../Page/康福.md "wikilink")・[郭延魯](../Page/郭延魯.md "wikilink")
35. [杂传第三十五](../Page/:wikisource:zh:新五代史/卷47.md "wikilink") -
    [華温琪](../Page/華温琪.md "wikilink")・[萇从簡](../Page/萇从簡.md "wikilink")・[張筠](../Page/張筠.md "wikilink")・[楊彦詢](../Page/楊彦詢.md "wikilink")・[李周](../Page/李周.md "wikilink")・[劉处让](../Page/劉处让.md "wikilink")・[李承約](../Page/李承約.md "wikilink")・[張希崇](../Page/張希崇.md "wikilink")・[相里金](../Page/相里金.md "wikilink")・[張廷蘊](../Page/張廷蘊.md "wikilink")・[馬全節](../Page/馬全節.md "wikilink")・[皇甫遇](../Page/皇甫遇.md "wikilink")・[安彦威](../Page/安彦威.md "wikilink")・[李瓊](../Page/李瓊.md "wikilink")・[劉景岩](../Page/劉景岩.md "wikilink")
36. 杂传第三十六 -
    [盧文進](../Page/盧文進.md "wikilink")・[李金全](../Page/李金全.md "wikilink")・[楊思权](../Page/楊思权.md "wikilink")・[尹暉](../Page/尹暉.md "wikilink")・[王弘贄](../Page/王弘贄.md "wikilink")・[劉審交](../Page/劉審交.md "wikilink")・[王周](../Page/王周.md "wikilink")・[高行周](../Page/高行周.md "wikilink")・[白再荣](../Page/白再荣.md "wikilink")・[安叔千](../Page/安叔千.md "wikilink")
37. 雜传第三十七 -
    [翟光鄴](../Page/翟光鄴.md "wikilink")・[馮暉](../Page/馮暉.md "wikilink")・[皇甫暉](../Page/皇甫暉.md "wikilink")・[唐景思](../Page/唐景思.md "wikilink")・[王進](../Page/王進_\(五代\).md "wikilink")・[常思](../Page/常思.md "wikilink")・[孫方諫](../Page/孫方諫.md "wikilink")
38. 雜传第三十八 -
    [王峻](../Page/王峻_\(后周\).md "wikilink")・[王殷](../Page/王殷.md "wikilink")・[劉詞](../Page/劉詞.md "wikilink")・[王環](../Page/王環.md "wikilink")
    ・[折从阮](../Page/折从阮.md "wikilink")
39. [雜传第三十九](../Page/:wikisource:zh:新五代史/卷51.md "wikilink") -
    [朱守殷](../Page/朱守殷.md "wikilink")
    ・[董璋](../Page/董璋.md "wikilink")・[范延光](../Page/范延光.md "wikilink")
    ・[娄继英](../Page/娄继英.md "wikilink")・[安重荣](../Page/安重荣.md "wikilink")・[安从进](../Page/安从进.md "wikilink")・[楊光遠](../Page/楊光遠.md "wikilink")
40. 雜传第四十 -
    [杜重威](../Page/杜重威.md "wikilink")・[李守貞](../Page/李守貞.md "wikilink")・[张彦泽](../Page/张彦泽.md "wikilink")
41. 雜传第四十一 -
    [王景崇](../Page/王景崇_\(五代\).md "wikilink")・[趙思綰](../Page/趙思綰.md "wikilink")・[慕容彦超](../Page/慕容彦超.md "wikilink")
42. 雜传第四十二 -
    [馮道](../Page/馮道.md "wikilink")・[李琪](../Page/李琪_\(五代\).md "wikilink")・[鄭玨](../Page/鄭玨.md "wikilink")・[李愚](../Page/李愚.md "wikilink")・[盧導](../Page/盧導.md "wikilink")・[司空頲](../Page/司空頲.md "wikilink")
43. 雜传第四十三 -
    [劉昫](../Page/劉昫.md "wikilink")・[卢文纪](../Page/卢文纪.md "wikilink")・[馬胤孫](../Page/馬胤孫.md "wikilink")・[姚顗](../Page/姚顗.md "wikilink")・[劉岳](../Page/劉岳.md "wikilink")・[馬縞](../Page/馬縞.md "wikilink")・[崔居俭](../Page/崔居俭.md "wikilink")・[崔梲](../Page/崔梲.md "wikilink")・[李懌](../Page/李懌.md "wikilink")
44. 雜传第四十四 -
    [和凝](../Page/和凝.md "wikilink")・[趙瑩](../Page/趙瑩.md "wikilink")・[馮玉](../Page/馮玉.md "wikilink")・[盧質](../Page/盧質.md "wikilink")・[呂琦](../Page/呂琦.md "wikilink")・[薛融](../Page/薛融.md "wikilink")・[何泽](../Page/何泽.md "wikilink")・[王权](../Page/王权_\(五代\).md "wikilink")・[史圭](../Page/史圭.md "wikilink")・[龍敏](../Page/龍敏.md "wikilink")
45. [雜传第四十五](../Page/:wikisource:zh:新五代史/卷57.md "wikilink") -
    [李崧](../Page/李崧.md "wikilink")・[李鏻](../Page/李鏻.md "wikilink")・[賈緯](../Page/賈緯.md "wikilink")・[段希堯](../Page/段希堯.md "wikilink")・[張允](../Page/張允_\(五代\).md "wikilink")・[王松](../Page/王松.md "wikilink")・[裴皞](../Page/裴皞.md "wikilink")・[王仁裕](../Page/王仁裕.md "wikilink")・[裴羽](../Page/裴羽.md "wikilink")・[王延](../Page/王延.md "wikilink")・[馬重績](../Page/馬重績.md "wikilink")・[趙延義](../Page/趙延義.md "wikilink")

### 考

1.  [新五代史考第一](../Page/:wikisource:zh:新五代史/卷58.md "wikilink") -
2.  司天考第二 -
3.  [職方考第三](../Page/:wikisource:zh:新五代史/卷60.md "wikilink") -

### 世家

1.  吴世家第一 - [吳](../Page/吳.md "wikilink")
2.  [南唐世家第二](../Page/:wikisource:zh:新五代史/卷62.md "wikilink") -
    [南唐](../Page/南唐.md "wikilink")
3.  前蜀世家第三 - [前蜀](../Page/前蜀.md "wikilink")
4.  後蜀世家第四 - [後蜀](../Page/後蜀.md "wikilink")
5.  南漢世家第五 - [南漢](../Page/南漢.md "wikilink")
6.  楚世家第六 - [楚](../Page/楚.md "wikilink")
7.  [吴越世家考第七](../Page/:wikisource:zh:新五代史/卷67.md "wikilink") -
    [吳越](../Page/吳越.md "wikilink")
8.  閩世家第八 - [閩](../Page/閩.md "wikilink")
9.  南平世家第九 - [荊南](../Page/荊南.md "wikilink")
10. 東漢世家第十 - [北漢](../Page/北漢.md "wikilink")
11. [十国世家年譜第十一](../Page/:wikisource:zh:新五代史/卷71.md "wikilink") -

### 附録

1.  [卷七十二 四夷附录第一](../Page/:wikisource:zh:新五代史/卷72.md "wikilink")
    [契丹](../Page/辽朝.md "wikilink")
2.  卷七十三 四夷附录第二 契丹
3.  [卷七十四 四夷附录第三](../Page/:wikisource:zh:新五代史/卷74.md "wikilink")
    [奚](../Page/奚.md "wikilink") [吐谷浑](../Page/吐谷浑.md "wikilink")
    [达靼](../Page/鞑靼.md "wikilink") [党项](../Page/党项.md "wikilink")
    [突厥](../Page/突厥.md "wikilink") [吐蕃](../Page/吐蕃.md "wikilink")
    [回鹘](../Page/甘州回鹘.md "wikilink") [于阗](../Page/于阗.md "wikilink")
    [高丽](../Page/王氏高丽.md "wikilink") [渤海](../Page/渤海国.md "wikilink")
    [新罗](../Page/新罗.md "wikilink")
    [黑水靺鞨](../Page/黑水靺鞨.md "wikilink")
    [南诏蛮](../Page/南诏.md "wikilink")
    [牂牁蛮](../Page/牂牁.md "wikilink")
    [昆明](../Page/昆明.md "wikilink")
    [占城](../Page/占城.md "wikilink")

<!-- end list -->

  - 卷七十五 五代史记序

## 參見

  - [五代史人物列表](../Page/五代史人物列表.md "wikilink")

## 参考文献

## 外部链接

  - [五代史記](http://www.wdl.org/zh/item/11380/)
  - [《新五代史》全文(简体)](http://www.guoxue.com/shibu/24shi/Newwudai/xwdml.htm)
  - [《新五代史》全文(繁体)](http://www.sidneyluo.net/a/a19/a19.htm)
  - 东英寿：〈[从虚词的使用看欧阳修《五代史记》的文体特色](http://www.nssd.org/articles/article_read.aspx?id=28415651)〉。

[Category:史部正史類](../Category/史部正史類.md "wikilink")
[Category:纪传体](../Category/纪传体.md "wikilink")
[五](../Category/二十四史.md "wikilink")
[Category:11世紀書籍](../Category/11世紀書籍.md "wikilink")
[Category:五代十国史书](../Category/五代十国史书.md "wikilink")
[Category:北宋典籍](../Category/北宋典籍.md "wikilink")
[Category:1050年代書籍](../Category/1050年代書籍.md "wikilink")

1.  《渑水燕谈录》卷六说：“天圣中，欧阳文忠公与尹师鲁议分撰，后师鲁别为《五代春秋》，止四千余言，简有史法，而文忠卒重修《五代》。”
2.  《河南先生文集》卷二六、二七
3.  歐陽修編撰《新五代史》的時間沒有明確記載。從他給尹洙、梅堯臣等人的信件看，在景祐三年（1036年）之前，已著手編寫，到皇祐五年（1053年）完成。
4.  《諫書稀庵筆記》：“濰有李星南行九，長者見之，呼曰李九；少者見之，亦戲呼李九。李九怒甚。予為之解紛曰：宋名臣歐陽文忠公，爵位功業，暄赫一時。當其幼年，群戲之曰：『好個歐九，可惜不讀書。』歐曰：『歐九書已讀熟矣，莫笑我歐九也。』文忠且自稱歐九，人呼李九，又何傷？”
5.  [周密](../Page/周密.md "wikilink")：《齐东野语》
6.  《[十七史商榷](../Page/十七史商榷.md "wikilink")》卷九十六〈五代土地，梁最小唐最大〉條，頁1101。
7.  [趙翼](../Page/趙翼.md "wikilink")：《廿二史劄記》卷二一《歐史不專據薛史舊本條》