**[新加坡](../Page/新加坡.md "wikilink")**的**[内阁](../Page/内阁.md "wikilink")**是负责新加坡所有[政府政策和事务的国家机关](../Page/新加坡政府.md "wikilink")，对[国会负责](../Page/新加坡国会.md "wikilink")，包括总理和各部部长所轄的[总理公署](../Page/新加坡总理公署.md "wikilink")、[社會及家庭發展部](../Page/新加坡社會及家庭發展部.md "wikilink")
、、[國防部](../Page/新加坡國防部.md "wikilink")、[教育部](../Page/新加坡教育部.md "wikilink")、[環境及水源部](../Page/新加坡環境及水源部.md "wikilink")、[財政部](../Page/財政部_\(新加坡\).md "wikilink")、[外交部](../Page/新加坡外交部.md "wikilink")、[衛生部](../Page/新加坡衛生部.md "wikilink")、[內政部](../Page/內政部_\(新加坡\).md "wikilink")、[通訊及新聞部](../Page/新加坡通訊及新聞部.md "wikilink")、[律政部](../Page/新加坡律政部.md "wikilink")、[人力部](../Page/新加坡人力部.md "wikilink")、[國家發展部](../Page/新加坡國家發展部.md "wikilink")、[貿易和工業部和](../Page/新加坡貿易和工業部.md "wikilink")[交通部](../Page/新加坡交通部.md "wikilink")。

## 现任内阁成员

<table>
<caption>新加坡內閣成員名單（截至2018年5月1日）[1]</caption>
<thead>
<tr class="header">
<th><p>職務</p></th>
<th><p>姓名</p></th>
<th><p>肖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/新加坡总理.md" title="wikilink">总理</a></p></td>
<td><p><a href="../Page/李显龙.md" title="wikilink">李显龙</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lee_Hsien-Loong_-_World_Economic_Forum_Annual_Meeting_2012_cropped.jpg" title="fig:Lee_Hsien-Loong_-_World_Economic_Forum_Annual_Meeting_2012_cropped.jpg">Lee_Hsien-Loong_-_World_Economic_Forum_Annual_Meeting_2012_cropped.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>副总理<br />
</p></td>
<td><p><a href="../Page/张志贤.md" title="wikilink">张志贤</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TeoCheeHean-Singapore-20060603.jpg" title="fig:TeoCheeHean-Singapore-20060603.jpg">TeoCheeHean-Singapore-20060603.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>副总理<br />
</p></td>
<td><p><a href="../Page/尚达曼.md" title="wikilink">尚达曼</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tharman_Shanmugaratnam_at_the_official_opening_of_Yuan_Ching_Secondary_School&#39;s_new_building,_Singapore_-_20100716_(cropped).jpg" title="fig:Tharman_Shanmugaratnam_at_the_official_opening_of_Yuan_Ching_Secondary_School&#39;s_new_building,_Singapore_-_20100716_(cropped).jpg">Tharman_Shanmugaratnam_at_the_official_opening_of_Yuan_Ching_Secondary_School's_new_building,_Singapore_-_20100716_(cropped).jpg</a></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/新加坡交通部.md" title="wikilink">交通部長</a></p></td>
<td><p><a href="../Page/许文远.md" title="wikilink">许文远</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Minister_Khaw_Boon_Wan.JPG" title="fig:Minister_Khaw_Boon_Wan.JPG">Minister_Khaw_Boon_Wan.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡总理公署.md" title="wikilink">总理公署部长</a></p></td>
<td><p><a href="../Page/黄志明_(新加坡).md" title="wikilink">黄志明</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Maj_Gen_Ng_Chee_Meng_at_the_Shangri-La_Dialogue,_Singapore_-_20140530.jpg" title="fig:Maj_Gen_Ng_Chee_Meng_at_the_Shangri-La_Dialogue,_Singapore_-_20140530.jpg">Maj_Gen_Ng_Chee_Meng_at_the_Shangri-La_Dialogue,_Singapore_-_20140530.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡总理公署.md" title="wikilink">总理公署部长</a><br />
<a href="../Page/新加坡律政部.md" title="wikilink">律政部第二部长</a><br />
<a href="../Page/財政部_(新加坡).md" title="wikilink">财政部第二部长</a><br />
<a href="../Page/新加坡教育部.md" title="wikilink">教育部第二部长</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Indranee_Rajah_in_Lecture_Theatre_B.2.17,_Block_B,_SIM_University,_Singapore_-_20150905-02_(cropped).jpg" title="fig:Indranee_Rajah_in_Lecture_Theatre_B.2.17,_Block_B,_SIM_University,_Singapore_-_20150905-02_(cropped).jpg">Indranee_Rajah_in_Lecture_Theatre_B.2.17,_Block_B,_SIM_University,_Singapore_-_20150905-02_(cropped).jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/通讯及新闻部.md" title="wikilink">通讯及新闻部长</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:S_Iswaran_at_the_International_Summit_on_the_Teaching_Profession,_New_York_City,_New_York_-_20110316_(cropped).jpg" title="fig:S_Iswaran_at_the_International_Summit_on_the_Teaching_Profession,_New_York_City,_New_York_-_20110316_(cropped).jpg">S_Iswaran_at_the_International_Summit_on_the_Teaching_Profession,_New_York_City,_New_York_-_20110316_(cropped).jpg</a></p></td>
</tr>
<tr class="even">
<td><p>文化、社区及青年部长</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:GraceFu-2010SummerYouthOlympics-20090426-02.jpg" title="fig:GraceFu-2010SummerYouthOlympics-20090426-02.jpg">GraceFu-2010SummerYouthOlympics-20090426-02.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡国防部.md" title="wikilink">国防部长</a></p></td>
<td><p><a href="../Page/黄永宏.md" title="wikilink">黄永宏</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ng_Eng_Hen_at_the_Pentagon_-_20120404.jpg" title="fig:Ng_Eng_Hen_at_the_Pentagon_-_20120404.jpg">Ng_Eng_Hen_at_the_Pentagon_-_20120404.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>教育部长</p></td>
<td><p><a href="../Page/王乙康.md" title="wikilink">王乙康</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ong_Ye_Kung_at_the_PAP_Party_Convention_-_20151206.jpg" title="fig:Ong_Ye_Kung_at_the_PAP_Party_Convention_-_20151206.jpg">Ong_Ye_Kung_at_the_PAP_Party_Convention_-_20151206.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/环境及水源部.md" title="wikilink">环境及水源部长</a><br />
<a href="../Page/回教事务主管部长.md" title="wikilink">回教事务主管部长</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Masagos_Zulkifli_at_The_Pentagon,_USA_-_20061017.jpg" title="fig:Masagos_Zulkifli_at_The_Pentagon,_USA_-_20061017.jpg">Masagos_Zulkifli_at_The_Pentagon,_USA_-_20061017.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>财政部长</p></td>
<td><p><a href="../Page/王瑞杰.md" title="wikilink">王瑞杰</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Heng_Swee_Keat_at_Nan_Hua_High_School,_Singapore_-_20120707.jpg" title="fig:Heng_Swee_Keat_at_Nan_Hua_High_School,_Singapore_-_20120707.jpg">Heng_Swee_Keat_at_Nan_Hua_High_School,_Singapore_-_20120707.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>外交部長</p></td>
<td><p><a href="../Page/維文_(新加坡).md" title="wikilink">維文</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vivian_Balakrishnan_-_2010_(cropped).jpg" title="fig:Vivian_Balakrishnan_-_2010_(cropped).jpg">Vivian_Balakrishnan_-_2010_(cropped).jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡卫生部.md" title="wikilink">卫生部长</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gan_Kim_Yong_at_a_PCF_graduation_ceremony_-_20081113_(cropped).jpg" title="fig:Gan_Kim_Yong_at_a_PCF_graduation_ceremony_-_20081113_(cropped).jpg">Gan_Kim_Yong_at_a_PCF_graduation_ceremony_-_20081113_(cropped).jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>內政部长<br />
律政部长</p></td>
<td><p><a href="../Page/尚穆根.md" title="wikilink">尚穆根</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:K_Shanmugam_(cropped).jpg" title="fig:K_Shanmugam_(cropped).jpg">K_Shanmugam_(cropped).jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡人力部.md" title="wikilink">人力部长</a><br />
內政部第二部长</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Josephine_Teo_September_2016_(29908295102).jpg" title="fig:Josephine_Teo_September_2016_(29908295102).jpg">Josephine_Teo_September_2016_(29908295102).jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>国家发展部长<br />
財政部第二部長</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lawrence_Wong_at_a_Singapore_International_Energy_Week_conference_-_20101102.jpg" title="fig:Lawrence_Wong_at_a_Singapore_International_Energy_Week_conference_-_20101102.jpg">Lawrence_Wong_at_a_Singapore_International_Energy_Week_conference_-_20101102.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/社会及家庭发展部.md" title="wikilink">社会及家庭发展部长</a><br />
<a href="../Page/新加坡国家发展部.md" title="wikilink">国家发展部第二部长</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Desmond_Lee_at_Bukit_Brown_Cemetery,_Singapore_-_20171118.jpg" title="fig:Desmond_Lee_at_Bukit_Brown_Cemetery,_Singapore_-_20171118.jpg">Desmond_Lee_at_Bukit_Brown_Cemetery,_Singapore_-_20171118.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡贸易及工业部.md" title="wikilink">贸工部长</a></p></td>
<td><p><a href="../Page/陈振声_(政治家).md" title="wikilink">陈振声</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chan_Chun_Sing_at_the_We_Welcome_Families_Awards_Ceremony_-_20130425_(cropped).jpg" title="fig:Chan_Chun_Sing_at_the_We_Welcome_Families_Awards_Ceremony_-_20130425_(cropped).jpg">Chan_Chun_Sing_at_the_We_Welcome_Families_Awards_Ceremony_-_20130425_(cropped).jpg</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [內閣](../Page/內閣.md "wikilink")
  - [新加坡政府](../Page/新加坡政府.md "wikilink")
  - [新加坡國會](../Page/新加坡國會.md "wikilink")

## 參考資料

## 延伸閱讀

### 書目

  - .

  - .

  - .

  - .

  - .

### 新聞報導

  - .

## 外部連結

  - [新加坡內閣官方網站](https://web.archive.org/web/20150430155311/http://www.pmo.gov.sg/cabinet)
  - [新加坡政府官方網站](http://www.gov.sg/)

[Category:新加坡政府](../Category/新加坡政府.md "wikilink")

1.