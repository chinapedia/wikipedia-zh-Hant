**新加坡盃**（**Singapore
Cup**）是[新加坡一項最主要的](../Page/新加坡.md "wikilink")[足球盃賽](../Page/足球.md "wikilink")，聯事成立於
1998年\[1\]，賽事冠軍可獲得[亞洲足協盃參賽資格](../Page/亞洲足協盃.md "wikilink")，但只限[新加坡國內成立的球會](../Page/新加坡.md "wikilink")。另外，本賽事最大特色是會邀請其他國外聯賽球隊參賽，主要是[東南亞地區的球隊](../Page/東南亞.md "wikilink")，例如[泰國](../Page/泰國足球超級聯賽.md "wikilink")、[馬來西亞](../Page/馬來西亞超級足球聯賽.md "wikilink")、[菲律賓](../Page/菲律賓足球聯盟國家男子公開錦標賽.md "wikilink")、[柬埔寨](../Page/柬埔寨足球聯賽.md "wikilink")、[老撾等球會都曾獲邀參賽](../Page/老撾超級足球聯賽.md "wikilink")。

其實於早一年前，[新加坡足球協會已經舉辦了名為](../Page/新加坡足球協會.md "wikilink")「**新加坡聯賽盃**」的比賽，一年後由現時「新加坡盃」承辦。同時間於
1996–1998年間，亦有舉辦過「**新加坡足總盃**」，但及後這盃賽改由業餘隊參賽。

再往前看的歷史中，早於1892年當時的[新加坡足球協會已經推行](../Page/新加坡足球協會.md "wikilink")「**新加坡業餘挑戰盃**」（1892–1951年）、「**新加坡挑戰盃**」（1952–1968年）及「**新加坡主席盃**」（1974–1995年），以上都是當其時新加坡國內的主要足球賽事。

## 歷屆決賽

|  |                                                                                           |
| :-: | ----------------------------------------------------------------------------------------- |
|  | 賽事進入[加時賽](../Page/加時賽.md "wikilink")                                                      |
|  | 賽事於[加時賽後仍然賽和](../Page/加時賽.md "wikilink")，需要以[互射十二碼決勝](../Page/互射十二碼.md "wikilink")（適用於決賽） |
|  | 賽事於常規 90 分鐘內賽和，直接以[互射十二碼決勝](../Page/互射十二碼.md "wikilink")（適用於季軍戰）                          |
|  | 被邀請參賽（球隊非於[新加坡職業足球聯賽中角逐](../Page/新加坡職業足球聯賽.md "wikilink")）                                |

圖示

<table>
<caption>歷屆新加坡盃決賽[2]</caption>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>決賽</p></th>
<th></th>
<th><p>季軍戰</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>冠軍</p></td>
<td><p>比數</p></td>
<td><p>亞軍</p></td>
<td><p>季軍</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p>'''<a href="../Page/丹戎巴葛聯足球俱樂部.md" title="wikilink">丹戎巴葛</a></p></td>
<td><p><strong>2–0</strong></p></td>
<td><p><a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p>'''<a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></p></td>
<td><p><strong>3–1</strong></p></td>
<td><p><a href="../Page/裕廊足球俱樂部.md" title="wikilink">裕廊</a></p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p>'''<a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>'''<a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
<td><p><strong>8–0</strong></p></td>
<td><p><a href="../Page/芽籠國際足球會.md" title="wikilink">芽籠聯</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>'''<a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/裕廊足球俱樂部.md" title="wikilink">裕廊</a></p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p>'''<a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
<td><p><strong>2–1</strong></p></td>
<td><p><a href="../Page/芽籠國際足球會.md" title="wikilink">芽籠聯</a></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>'''<a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
<td><p><strong>4–1</strong></p></td>
<td><p><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p>'''<a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
<td><p><strong>3–2</strong></p></td>
<td><p><a href="../Page/兀蘭威靈頓足球會.md" title="wikilink">威靈頓</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>'''<a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
<td><p><strong>3–2</strong></p></td>
<td><p><a href="../Page/春武里足球俱樂部.md" title="wikilink">春武里</a></p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>'''<a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></p></td>
<td><p><strong>4–3</strong></p></td>
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>'''<a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></p></td>
<td><p><strong>2–1</strong></p></td>
<td><p><a href="../Page/兀蘭威靈頓足球會.md" title="wikilink">威靈頓</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><strong><a href="../Page/芽籠國際足球會.md" title="wikilink">芽籠聯</a></strong></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/曼谷玻璃足球俱樂部.md" title="wikilink">曼谷玻璃</a></p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><strong> <a href="../Page/曼谷玻璃足球俱樂部.md" title="wikilink">曼谷玻璃</a></strong></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><strong><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></strong></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><strong><a href="../Page/勇士足球會.md" title="wikilink">新加坡武裝部隊</a></strong></p></td>
<td><p><strong>2–1</strong></p></td>
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><strong><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></strong></p></td>
<td><p><strong>4–1</strong></p></td>
<td><p><a href="../Page/丹戎巴葛聯足球俱樂部.md" title="wikilink">丹戎巴葛</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><strong><a href="../Page/馬里士他卡沙足球會.md" title="wikilink">馬里士他卡沙</a></strong></p></td>
<td><p><strong>3–1</strong></p></td>
<td><p><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><strong> <a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></strong></p></td>
<td><p><strong>2–1</strong></p></td>
<td><p><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><strong> <a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></strong></p></td>
<td><p><strong>2–0</strong></p></td>
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><strong> <a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></strong></p></td>
<td><p><strong>2–2</strong></p></td>
<td><p><a href="../Page/環球宿霧足球會.md" title="wikilink">環球宿霧</a></p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p><strong> <a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></strong></p></td>
<td><p><strong>4–1</strong></p></td>
<td><p><a href="../Page/文萊DPMM足球會.md" title="wikilink">文萊DPMM</a></p></td>
</tr>
</tbody>
</table>

## 球會成績統計

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>冠軍</p></th>
<th><p>亞軍</p></th>
<th><p>冠軍年份</p></th>
<th><p>亞軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/內政聯足球俱樂部.md" title="wikilink">內政聯</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2000, 2001, 2003, 2005, 2011, 2013</p></td>
<td><p>2004, 2014, 2015</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勇士足球會.md" title="wikilink">勇士</a></p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>1999, 2007, 2008, 2012</p></td>
<td><p>1998, 2000</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡新潟天鵝足球會.md" title="wikilink">新潟天鵝乙隊</a></p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>2015, 2016, 2017, 2018</p></td>
<td><p>2011</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>2002, 2004, 2006</p></td>
<td><p>2007, 2010, 2012, 2016</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芽籠國際足球會.md" title="wikilink">芽籠國際</a></p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>2009</p></td>
<td><p>2001, 2003</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼谷玻璃足球俱樂部.md" title="wikilink">曼谷玻璃</a></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>2010</p></td>
<td><p>2009</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丹戎巴葛聯足球俱樂部.md" title="wikilink">丹戎巴葛</a></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1998</p></td>
<td><p>2013</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬里士他卡沙足球會.md" title="wikilink">馬里士他卡沙</a></p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2014</p></td>
<td><p>－</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/裕廊足球俱樂部.md" title="wikilink">裕廊</a></p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>－</p></td>
<td><p>1999, 2002</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/兀蘭威靈頓足球會.md" title="wikilink">威靈頓</a></p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>－</p></td>
<td><p>2005, 2008</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/春武里足球俱樂部.md" title="wikilink">春武里</a></p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>－</p></td>
<td><p>2006</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/環球宿霧足球會.md" title="wikilink">環球宿霧</a></p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>－</p></td>
<td><p>2017</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文萊DPMM足球會.md" title="wikilink">文萊DPMM</a></p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>－</p></td>
<td><p>2018</p></td>
</tr>
</tbody>
</table>

## 以往各盃賽歷史

### 新加坡聯賽盃

| 球季   | 冠軍                                     | 比數  | 亞軍                                   | 備註 |
| ---- | -------------------------------------- | --- | ------------------------------------ | -- |
| 1997 | [新加坡武裝部隊](../Page/勇士足球會.md "wikilink") | 1–0 | [芽龍聯](../Page/芽籠國際足球會.md "wikilink") |    |

歷屆新加坡聯賽盃決賽\[3\]

### 新加坡足總盃

| 球季   | 冠軍                                       | 比數                                        | 亞軍                                      | 備註 |
| ---- | ---------------------------------------- | ----------------------------------------- | --------------------------------------- | -- |
| 1996 | [芽龍聯](../Page/芽籠國際足球會.md "wikilink")     | 1–1（4–2 [p](../Page/互射十二碼.md "wikilink")） | [新加坡武裝部隊](../Page/勇士足球會.md "wikilink")  |    |
| 1997 | [新加坡武裝部隊](../Page/勇士足球會.md "wikilink")   | 4–2                                       | [威靈頓](../Page/兀蘭威靈頓足球會.md "wikilink")   |    |
| 1998 | [丹戎巴葛](../Page/丹戎巴葛聯足球俱樂部.md "wikilink") | 1–0                                       | [三巴旺流浪](../Page/三巴旺流浪足球會.md "wikilink") |    |

歷屆新加坡足總盃決賽\[4\]

### 新加坡主席盃

| Year  | 冠军                                                |
| ----- | ------------------------------------------------- |
| 1995年 | '''[芽笼国际](../Page/芽笼联.md "wikilink")              |
| 1994年 | '''[中峇鲁选区体育俱乐部](../Page/丹戎巴葛联.md "wikilink")      |
| 1993年 | *无赛事*                                             |
| 1992年 | '''[马里士他联](../Page/马里士他卡沙.md "wikilink")          |
| 1991年 | '''[芽笼国际](../Page/芽笼联.md "wikilink")              |
| 1990年 | '''[芽笼国际](../Page/芽笼联.md "wikilink")              |
| 1989年 | '''[裕廊镇](../Page/裕廊FC.md "wikilink")              |
| 1988年 | '''[中峇鲁选区体育俱乐部](../Page/丹戎巴葛联.md "wikilink")      |
| 1987年 | '''[中峇鲁选区体育俱乐部](../Page/丹戎巴葛联.md "wikilink")      |
| 1986年 | '''[新加坡武装部队体育协会](../Page/新加坡武装部队FC.md "wikilink") |
| 1985年 | '''[中峇鲁选区体育俱乐部](../Page/丹戎巴葛联.md "wikilink")      |
| 1984年 | '''[新加坡武装部队体育协会](../Page/新加坡武装部队FC.md "wikilink") |
| 1983年 | '''[花拉公园联](../Page/花拉公园联.md "wikilink")           |
| 1982年 | '''[中峇鲁选区体育俱乐部](../Page/丹戎巴葛联.md "wikilink")      |
| 1981年 | '''[花拉公园联](../Page/花拉公园联.md "wikilink")           |
| 1980年 | '''[警察运动协会](../Page/内政联.md "wikilink")            |
| 1979年 | '''[大巴窑联](../Page/大巴窑联.md "wikilink")             |
| 1978年 | '''[芽笼国际](../Page/芽笼联.md "wikilink")              |
| 1977年 | '''[大巴窑联](../Page/大巴窑联.md "wikilink")             |
| 1976年 | '''[芽笼国际](../Page/芽笼联.md "wikilink")              |
| 1975年 | '''[新加坡武装部队体育协会](../Page/新加坡武装部队FC.md "wikilink") |

### 主席盃以前的新加坡盃

  - 详见[总统杯之前的历届新加坡杯赛冠军](../Page/总统杯之前的历届新加坡杯赛冠军.md "wikilink")

## 參考

## 外部連結

  - [官方網站](https://web.archive.org/web/20160310162220/http://www.sleague.com/competitions/singapore-cup/overview)

[Category:新加坡足球](../Category/新加坡足球.md "wikilink")

1.
2.
3.
4.