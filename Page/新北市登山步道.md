**新北市登山步道**，又名**新北市綠色走廊登山步道**，為之前的[臺北縣政府建設局規劃推動的](../Page/臺北縣政府建設局.md "wikilink")[臺北縣山區步道建設計畫](../Page/新北市.md "wikilink")，係針對既有登山步道，以不破壞生態環境之方式整修改善。該步道系統共有40條路線，縣政府並發行「台北縣登山步道導覽手冊」，可作為民眾登上低海拔郊區山系的入門書籍。

  - 北海岸風景線登山步道
      - [魚路古道](../Page/魚路古道.md "wikilink") (金包里大路)
      - [富士坪、鹿堀坪古道](../Page/富士坪、鹿堀坪古道.md "wikilink")
      - [獅頭山公園步道](../Page/獅頭山公園步道.md "wikilink")
      - [青山瀑布、尖山湖步道](../Page/青山瀑布、尖山湖步道.md "wikilink")
  - 東北角海岸風景線登山步道
      - [金字碑古道](../Page/金字碑古道.md "wikilink")
      - [貂山古道](../Page/貂山古道.md "wikilink")
      - [嶺頭觀日步道](../Page/嶺頭觀日步道.md "wikilink")
      - [草嶺古道](../Page/草嶺古道.md "wikilink")
      - [桃源谷、灣坑頭山登山步道](../Page/桃源谷、灣坑頭山登山步道.md "wikilink")
      - [南子杏山登山步道](../Page/南子杏山登山步道.md "wikilink")
      - [鼻頭角、龍洞灣岬步道](../Page/鼻頭角、龍洞灣岬步道.md "wikilink")
  - 平溪、瑞芳、汐止風景線登山步道
      - [平溪三尖登山步道](../Page/平溪三尖登山步道.md "wikilink")
      - [慈母峰、孝子山登山步道](../Page/慈母峰、孝子山登山步道.md "wikilink")
      - [五分山登山步道](../Page/五分山登山步道.md "wikilink")
      - [九份登山步道網](../Page/九份登山步道網.md "wikilink")
      - [金瓜石登山步道網](../Page/金瓜石登山步道網.md "wikilink")
      - [汐止大尖山登山步道](../Page/大尖山_\(汐止區\).md "wikilink")
      - [新山夢湖登山步道](../Page/新山夢湖登山步道.md "wikilink")
  - 新店、烏來風景線登山步道
      - [直潭山登山步道](../Page/直潭山登山步道.md "wikilink")
      - [銀河洞越嶺登山步道](../Page/銀河洞越嶺登山步道.md "wikilink")
      - [內洞林道登山步道](../Page/內洞林道登山步道.md "wikilink")
      - [加九寮人行步道](../Page/加九寮人行步道.md "wikilink")
  - 坪林、石碇、深坑風景線登山步道
      - [皇帝殿山登山步道](../Page/皇帝殿山登山步道.md "wikilink")
      - [獅公髻尾山登山步道](../Page/獅公髻尾山登山步道.md "wikilink")
      - [筆架連峰、二格山登山步道](../Page/筆架連峰、二格山登山步道.md "wikilink")
      - [炮子崙越嶺登山步道](../Page/炮子崙越嶺登山步道.md "wikilink")
      - [胡桶古道](../Page/胡桶古道.md "wikilink")
      - [九芎根自然景觀登山步道](../Page/九芎根自然景觀登山步道.md "wikilink")
  - 土城、三峽、鶯歌、樹林風景線登山步道
      - [天上山登山步道](../Page/天上山登山步道.md "wikilink")
      - [鳶山登山步道](../Page/鳶山登山步道.md "wikilink")
      - [五寮尖登山步道](../Page/五寮尖登山步道.md "wikilink")
      - [白雞三山登山步道](../Page/白雞三山登山步道.md "wikilink")
      - [紫微聖母環山步道](../Page/紫微聖母環山步道.md "wikilink")
      - [滿月圓山登山步道](../Page/滿月圓山登山步道.md "wikilink")
      - [大棟山、大同山、青龍嶺步道](../Page/大棟山、大同山、青龍嶺步道.md "wikilink")
      - [鶯歌石、牛灶坑山登山步道](../Page/鶯歌石、牛灶坑山登山步道.md "wikilink")
  - 五股、八里、林口風景線登山步道
      - [觀音山、占山登山步道](../Page/觀音山、占山登山步道.md "wikilink")
      - [牛港稜山登山步道](../Page/牛港稜山登山步道.md "wikilink")
  - 中和、泰山風景線登山步道
      - [圓通寺越嶺烘爐地登山步道](../Page/圓通寺越嶺烘爐地登山步道.md "wikilink")
      - [山腳頂山、瓊子湖登山步道](../Page/山腳頂山、瓊子湖登山步道.md "wikilink")

## 相關條目

  - [國家步道系統](../Page/國家步道系統.md "wikilink")

## 參考資料

[Category:新北市地理](../Category/新北市地理.md "wikilink")
[Category:自然步道](../Category/自然步道.md "wikilink")
[新北市登山步道](../Category/新北市登山步道.md "wikilink")