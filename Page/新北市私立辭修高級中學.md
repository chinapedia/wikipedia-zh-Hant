**辭修學校財團法人新北市私立辭修高級中學**，簡稱為**辭修高中**，是[臺灣新北三峽的一所](../Page/臺灣.md "wikilink")[完全中學](../Page/完全中學.md "wikilink")。
[Tsz-shiou_Senior_high_school_Junior_Department.jpg](https://zh.wikipedia.org/wiki/File:Tsz-shiou_Senior_high_school_Junior_Department.jpg "fig:Tsz-shiou_Senior_high_school_Junior_Department.jpg")
[Tsz-_shiou_senior_high_school_Senior_Department.jpg](https://zh.wikipedia.org/wiki/File:Tsz-_shiou_senior_high_school_Senior_Department.jpg "fig:Tsz-_shiou_senior_high_school_Senior_Department.jpg")

## 創校歷史

由[陳履安先生為紀念父親](../Page/陳履安.md "wikilink")[陳誠](../Page/陳誠.md "wikilink")（前中華民國副總統與前臺灣省主席）所創的學校，因陳誠字辭修，故稱為辭修高中。

## 地理環境

辭修高中地處橫溪畔，出校門後左右幾公里內呈現淳樸的山林鄉村風味，對面為[佛光山](../Page/佛光山.md "wikilink")[金光明寺隔壁為成福國小](../Page/金光明寺.md "wikilink")，平常上課時間憑假單出入大門。

## 歷任首長

### 歷任董事長

  - [陳譚祥](../Page/譚祥.md "wikilink") 1971年8月
  - [陳曹倩](../Page/陳曹倩.md "wikilink") 1981年6月
  - [陳履慶](../Page/陳履慶.md "wikilink") 1993年8月
  - [陳履安](../Page/陳履安.md "wikilink") 2011年10月

### 歷任校長

  - [孫衍](../Page/孫衍.md "wikilink") 1971年8月－1971年10月
  - [陳履安](../Page/陳履安.md "wikilink") 1971年10月－1972年1月
  - [楊光華](../Page/楊光華.md "wikilink") 1972年1月－1984年7月
  - [薛德愍](../Page/薛德愍.md "wikilink") 1984年7月－1995年7月
  - [邱億明](../Page/邱億明.md "wikilink") 1995年8月－2000年7月
  - [戴曉影](../Page/戴曉影.md "wikilink") 2000年7月－2016年7月
  - [李玉美](../Page/李玉美.md "wikilink") 2016年8月－2018年8月
  - [邱一峰](../Page/邱一峰.md "wikilink") 2018年8月－2019年1月（代理校長）
  - [李坤興](../Page/李坤興.md "wikilink") 2019年1月－

## 教學

### 高中部班級人數

97學年度
\*一年級 7班 共326人

  - 二年級 7班 共298人
  - 三年級 6班 共239人

96學年度
\*一年級 8班 共309人

  - 二年級 6班 共308人
  - 三年級 7班 共356人

### 國中部班級人數

97學年度
\*一年級 8班 共406人

  - 二年級 8班 共363人
  - 三年級 8班 共308人

96學年度
\*一年級 9班 共374人

  - 二年級 8班 共340人
  - 三年級 8班 共328人

### 分類組方式

升二年級時分類組，社會組從第一班開始排，自然組從八班(最後一個班)往回排。現在則是隨機排，同時也增設六年一貫班，為自然組與社會組同班分組上課

## 校風

### 七大禁忌

  - 「吸煙」
  - 「打架」
  - 「偷竊」
  - 「作弊」
  - 「欺騙師長」
  - 「不假外出」

＊【男女交往】

### 以往規定

  - 男女分班。
  - 高中部學生一律住校，住宿生採軍事化管理，例如棉被規定折成豆腐形狀，必須要有菱有角，否則會被處罰。
  - 男女交談（必須在老師辦公室或有老師在現場），走廊、樓梯與公共電話均不得交談，須將男、女生區分開來。

### 現有規定

  - 居住鄰近地區（三峽、鶯歌、土城、樹林、板橋、新店、新莊、桃園、中和）學生搭校車通勤上學，其餘住校。
  - 所有學生均須參加晚自習。

## 校友

| [姓名](../Page/姓名.md "wikilink")   | [性別](../Page/性別.md "wikilink") | 畢業年月    | 屆別 | 現職                                                                                         |
| -------------------------------- | ------------------------------ | ------- | -- | ------------------------------------------------------------------------------------------ |
| [陳弱水](../Page/陳弱水.md "wikilink") | 男                              | 1974年6月 | 1  | [國立臺灣大學歷史系特聘教授](../Page/國立臺灣大學.md "wikilink")、[中央研究院史語所合聘研究員](../Page/中央研究院.md "wikilink") |
| [葉永南](../Page/葉永南.md "wikilink") | 男                              | 1974年6月 | 1  | 中央研究院研究員                                                                                   |
| [黃富源](../Page/黃富源.md "wikilink") | 男                              | 1974年6月 | 1  | [中央警察大學教務長](../Page/中央警察大學.md "wikilink")                                                  |
| [吳均龐](../Page/吳均龐.md "wikilink") | 男                              | 1974年7月 | 1  | [德意志銀行臺灣區總經理](../Page/德意志銀行.md "wikilink")                                                 |
| [陳光興](../Page/陳光興.md "wikilink") | 男                              | 1975年6月 | 1  | [清華大學外語系教授](../Page/國立清華大學.md "wikilink")                                                  |
| [韓良露](../Page/韓良露.md "wikilink") | 女                              | 1976年6月 | 3  | [作家](../Page/作家.md "wikilink")                                                             |
| [周行一](../Page/周行一.md "wikilink") | 男                              | 1976年6月 | 3  | [國立政治大學校長](../Page/國立政治大學.md "wikilink")                                                   |
| [劉楷](../Page/劉楷.md "wikilink")   | 男                              | 1976年6月 | 3  | [浩宇律師事務所負責人](../Page/浩宇律師事務所.md "wikilink")                                                |
| [黃介正](../Page/黃介正.md "wikilink") | 男                              | 1978年6月 | 5  | [淡江大學大陸研究所所長](../Page/淡江大學.md "wikilink")                                                  |
| [楊偉文](../Page/楊偉文.md "wikilink") | 男                              | 1984年6月 | 11 | [開南大學法律](../Page/開南大學.md "wikilink")-{系}-系主任                                               |
| [須文蔚](../Page/須文蔚.md "wikilink") | 男                              | 1984年6月 | 12 | [東華大學華文文學系副教授](../Page/國立東華大學.md "wikilink")                                               |
| [高道鵬](../Page/高道鵬.md "wikilink") | 男                              | 1985年6月 | 12 | [新文豐出版公司總經理](../Page/新文豐出版公司.md "wikilink")                                                |
| [胡衍南](../Page/胡衍南.md "wikilink") | 男                              | 1985年6月 | 13 | [國立臺灣師範大學國文學系副教授](../Page/國立臺灣師範大學國文學系.md "wikilink")                                      |
| [桑國忠](../Page/桑國忠.md "wikilink") | 男                              | 1985年6月 | 13 | [臺北海洋技術學院國貿](../Page/臺北海洋技術學院.md "wikilink")-{系}-系主任                                       |
| [徐國能](../Page/徐國能.md "wikilink") | 男                              | 1989年6月 | 18 | [國立臺灣師範大學國文學系副教授](../Page/國立臺灣師範大學國文學系.md "wikilink")                                      |

傑出校友（依
[辭修校友會資料](http://202.39.31.173/tsshs/default.asp?todowhat=goodman)）

### 其他校友

在職教師中，國文科教師李鎮如、國文科教師魏淑雯、國文科教師廖本詔、輔導主任陳姿樺、國文科教師黃子凌皆是畢業後回到母校任教。
藝人如[賈永婕](../Page/賈永婕.md "wikilink")、[卜學亮](../Page/卜學亮.md "wikilink")（轉學）、[馬念先](../Page/馬念先.md "wikilink")（轉學）、[宋少卿](../Page/宋少卿.md "wikilink")（轉學）[連晨翔](../Page/連晨翔.md "wikilink")

## 新聞事件

校方方面仍然在髮禁方面只有稍稍放寬但不完全開放，在2005年，曾有高二學生因不滿髮禁的緣故，發起全校罷課的活動，但實質上只有在中午集結學生於校園禮堂前，向學校表達其不滿之情緒，便草草結束，辭修高中也成為現今[北台灣高級中學中少數還存在著髮禁的學校](../Page/北台灣.md "wikilink")。

## 參考文獻

<div class='references-small'>

<references/>

</div>

## 外部連結

  - [辭修高級中學](http://www.tsshs.ntpc.edu.tw)
  - [教育部中部辦公室：辭修高中](http://www.tpde.edu.tw/client/school/forward.jsp?url=detail.jsp&id=011329--01)
  - [《誠經擁有。誠園寫真館》（辭修活動照片）](http://www.tsshs.ntpc.edu.tw/epaper/evey/index.htm)

[私](../Category/新北市私立高級中學.md "wikilink")
[Category:1971年創建的教育機構](../Category/1971年創建的教育機構.md "wikilink")
[Category:新北市完全中學](../Category/新北市完全中學.md "wikilink")
[北](../Category/台灣冠以人名的教育機構.md "wikilink")