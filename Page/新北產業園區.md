**新北產業園區**，前稱**五股工業區**，為[中華民國](../Page/中華民國.md "wikilink")[經濟部工業局所創辦的國家級](../Page/經濟部工業局.md "wikilink")[工業區](../Page/工業區.md "wikilink")，位於今[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[五股區與](../Page/五股區.md "wikilink")[新莊區](../Page/新莊區.md "wikilink")。

## 歷史

為積極發展工業、促進地方繁榮與發展，當時台北縣政府（今[新北市政府](../Page/新北市政府.md "wikilink")）得到[行政院核准](../Page/行政院.md "wikilink")，在1984年1月依據《[獎勵投資條例](../Page/獎勵投資條例.md "wikilink")》的規定，開發五股工業區。一方面容納[二重疏洪道的合法](../Page/二重疏洪道.md "wikilink")[工廠拆遷戶使用](../Page/工廠.md "wikilink")，一方面提供興辦工業人作為建廠用地，以紓解工業用地殷切之需求。此外，當時的台北縣政府在此工業區內設立了[工商展覽中心](../Page/新北市工商展覽中心.md "wikilink")，提供了一個國際級展覽場地給當地的廠商。2011年4月由[新北市市長](../Page/新北市市長.md "wikilink")[朱立倫出面協調](../Page/朱立倫.md "wikilink")，於100年6月1日起正式更名為新北產業園區\[1\]。

## 地理位置

新北產業園區位於[新北市](../Page/新北市.md "wikilink")[五股區及](../Page/五股區.md "wikilink")[新莊區交界](../Page/新莊區.md "wikilink")，東臨[二重疏洪道](../Page/二重疏洪道.md "wikilink")，北臨[中山高速公路](../Page/中山高速公路.md "wikilink")，南鄰[新莊區市區](../Page/新莊區.md "wikilink")，西界為[中港大排](../Page/中港大排.md "wikilink")，在中華民國經濟部轄工業區中最為接近[台北市](../Page/台北市.md "wikilink")，區位條件非常優越。

## 公共設施

  - 道路、路燈：全區闢設14條道路總長13,850公尺，路燈354盞。
  - 排水系統：主要排水溝全長3,800公尺，雨水管線 23,800公尺。
  - 自來水系統：每日最大供水量9,000噸，供水量充裕。
  - 電話系統：區內供應電話40,000門，目前使用24,000門。
  - 電力：區內有變電所，電力供應穩定，最高使用達180,000千瓦。
  - 綠帶：區內綠帶3.309公頃，於各主要、次要道路栽種

## 概況

新北產業園區的業種廣泛，舉凡[食品](../Page/食品.md "wikilink")、[紡織](../Page/紡織.md "wikilink")、[家具](../Page/家具.md "wikilink")、[印刷](../Page/印刷.md "wikilink")、[塑化](../Page/塑膠化學.md "wikilink")、[電子](../Page/電子.md "wikilink")、[機械等等業種一應俱全](../Page/機械.md "wikilink")，用地幾乎完全開發完畢，新進廠商一地難求。然而，最近十年，因為產業轉型與外移緣故，閒置土地高達45.92公頃，相當於2座大安森林公園，5座大巨蛋，浪費超過60億元。\[2\]

## 教育

[勞動部勞動力發展署在區內設立](../Page/勞動部勞動力發展署.md "wikilink")[北區職業訓練中心](../Page/北區職業訓練中心.md "wikilink")，是一所公立職業訓練機構。

## 相關條目

[桃園捷運機場線](../Page/桃園捷運機場線.md "wikilink")、[台北捷運環狀線](../Page/台北捷運環狀線.md "wikilink")：[新北產業園區站](../Page/新北產業園區站.md "wikilink")

## 相關網站

  - [中華民國經濟部工業局](http://www.moeaidb.gov.tw/)
  - [新北產業園區](http://www.moeaidb.gov.tw/iphw/wuku/home/Main.jsp)
  - [新北市新莊北側知識產業園區](https://web.archive.org/web/20160915141917/http://xznorth.land.ntpc.net.tw/1-1.asp)

## 参考

<div class="references-small" style="height: 220px; overflow: auto; padding: 3px">

</div>

[W](../Category/新北市經濟.md "wikilink")
[W](../Category/台灣工業區.md "wikilink")
[Category:五股區](../Category/五股區.md "wikilink")
[Category:新莊區](../Category/新莊區.md "wikilink")

1.
2.  產業園區土地閒置 立院：十年浪費超過60億 聯合報 2017-03-23