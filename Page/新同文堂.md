[New_Tongwen.png](https://zh.wikipedia.org/wiki/File:New_Tongwen.png "fig:New_Tongwen.png")
[Tongwen_extension.png](https://zh.wikipedia.org/wiki/File:Tongwen_extension.png "fig:Tongwen_extension.png")

**新同文堂**为[Mozilla
Firefox一個Firefox瀏覽器](../Page/Mozilla_Firefox.md "wikilink")-{zh-hans:附加组件;
zh-hant:附加元件;}-，用以方便[中文](../Page/中文.md "wikilink")-{zh-hans:用户;
zh-hant:使用者;}-把[網頁內容進行](../Page/網頁.md "wikilink")[繁體](../Page/繁體.md "wikilink")、[簡體中文互換](../Page/簡體.md "wikilink")。根據Mozilla
Taiwan討論區的記載，同文堂本是一位[中國大陸網友哈少開發的](../Page/中國大陸.md "wikilink")，但至0.2.0版本時開發工作暫停了，當時的版本只支援Mozilla
Firefox
0.9版本\[1\]。及後，由[台灣網友Softcup接手開發新版本的同文堂](../Page/台灣.md "wikilink")，定名為「新同文堂」。該-{zh-hans:附加组件;
zh-hant:附加元件;}-使用[GPL第](../Page/GPL.md "wikilink")2版發布。

安裝了新同文堂之後，Firefox瀏覽器的工具列以及快捷選單都會加入了新同文堂的功能，方便-{zh-hans:用户;
zh-hant:使用者;}-使用。新同文堂除了能讓-{zh-hans:用户;
zh-hant:使用者;}-在網頁上進行[繁簡中文互換和文字縮放之外](../Page/繁簡中文互換.md "wikilink")，更可以把選定的網頁內容轉換至繁體或簡體中文再直接輸出至剪貼簿上。

## 参看

  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")

## 参考文献

## 外部链接

  - [同文堂](http://tongwen.mozdev.org/)（中国大陆開發版本）
  - [新同文堂](https://addons.mozilla.org/en-US/firefox/addon/new_tongwentang/)
  - [有關新同文堂的討論區](http://forum.moztw.org/viewtopic.php?p=19073)
  - [Softcup的新同文堂
    github](https://github.com/softcup/New-Tongwentang-for-Firefox)

[Category:中文漢字處理‎](../Category/中文漢字處理‎.md "wikilink") [Category:Firefox
附加组件](../Category/Firefox_附加组件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")

1.  新同文堂相關討論 <http://forum.moztw.org/viewtopic.php?p=19073>