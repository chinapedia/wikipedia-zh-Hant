[Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg](https://zh.wikipedia.org/wiki/File:Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg "fig:Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg")
[Big_Costilla_Peak.jpg](https://zh.wikipedia.org/wiki/File:Big_Costilla_Peak.jpg "fig:Big_Costilla_Peak.jpg")
[Central_Ave_and_5th_St,_Albuquerque_NM.jpg](https://zh.wikipedia.org/wiki/File:Central_Ave_and_5th_St,_Albuquerque_NM.jpg "fig:Central_Ave_and_5th_St,_Albuquerque_NM.jpg")
[Holloman_AFB_F-22.jpg](https://zh.wikipedia.org/wiki/File:Holloman_AFB_F-22.jpg "fig:Holloman_AFB_F-22.jpg")

**新墨西哥州**（[納瓦霍語](../Page/納瓦霍語.md "wikilink")：；；）是[美國西南方的一州](../Page/美國.md "wikilink")，它曾是[墨西哥的一省](../Page/墨西哥.md "wikilink")。該州有許多西班牙裔的居民，亦有不少的美國原住民。因此具有相當獨特的文化。它也是美國唯一官方州名有兩個語言的州份。該州郵政簡寫為NM，它的首府是[聖塔菲](../Page/圣菲_\(新墨西哥州\).md "wikilink")。

该州早在公元前即有人类活动，州内一些地区发现了史前人类活动遗址。公元三世纪后半定居活动出现，出现了建筑遗址。公元16至17世纪后半叶，虽然欧洲探险者已经开始出现，但当地依然保留了传统文化和独特的社会组织。

18世纪随着西班牙殖民势力拓展，新墨西哥州所在的地区逐步被纳入新西班牙总督区的统治。

1821年墨西哥摆脱西班牙的殖民统治，民族主义情绪高涨，加上美国购买的路易斯安那领地西部与墨西哥的边界不清晰，墨西哥政府遂移民北部各地区，其中包括新墨西哥。

1845年爆发的美墨战争中，美国军队进入这一地区，但因没有稳定的后勤通道无法长期占领据守。1848年新墨西哥领地(包括今天亚利桑那东部，新墨西哥，科罗拉多南部和犹他东南部)被割让给美国，美国政府随后建立新墨西哥领地。

1853年为修建横贯大陆铁路南线而发起的加兹登购地为新墨西哥增加了埃尔帕索以西的地区，包括西南角向南突出的方形区域

内战期间，新墨西哥忠于北方的联邦政府，但同时也有很多居民支持奴隶制的邦联。邦联军队曾试图占领新墨西哥和亚利桑那来缓解西部战线的威胁，但没能成功。与此同时，有不少黑奴逃亡于此。战后美国政府拆分了新墨西哥领地，在其西部建立了亚利桑那领地。东北部少量地区划入科罗拉多，西北角部分地区划入内华达州，包括今天的拉斯维加斯。

1912年，新墨西哥与亚利桑那分别建州，开始时州府定在阿尔伯克基，但后来迁址圣菲。

新墨西哥州人均收入在美国各州排名靠后，人均收入，家庭中位收入和贫困率都是倒数。

## 地理

## 行政區劃

## 经济

生產石油和天然氣，及貴金屬。

## 人口

新墨西哥州是美国第六人口稀少的州，人口密度为每平方公里/6人。
2003年人口普查，新墨西哥州有1,874,614人。自1990年起的成長率約為23.7%。種族的組成大略如下：

  - 40.5%是盎格魯白人（無西班牙血統）
  - 46.3% [拉美裔人](../Page/拉美裔人.md "wikilink")
  - 9.4%是[印地安原住民](../Page/印第安人.md "wikilink")
  - 1.9%是非裔美國人
  - 1.4%是亞裔美國人
  - 3.6%混合的種族

根據2000年人口大普查，在新墨西哥州的居民祖先中，[墨西哥人](../Page/墨西哥.md "wikilink")（18.1%）最多，其次是[德國人](../Page/德國.md "wikilink")（9.9%）、[印地安原住民](../Page/印第安人.md "wikilink")（9.5%）、[西班牙人](../Page/西班牙.md "wikilink")（9.3%）和[英國人](../Page/英國.md "wikilink")（7.6%）。5岁以上在家中说英语的居民占总人口的64%，说西班牙语的居民占28%。

新墨西哥州在5歲以下的居民則有7.2%、18歲以下有28%、高齡者如超過65歲以上則有11.7%。女性占全新墨西哥州人口比為50.8%。

### 宗教

[宗教上](../Page/宗教.md "wikilink")，新墨西哥州的人口信仰比例如下。\[1\]

  - 38% [基督新教](../Page/基督新教.md "wikilink")
  - 34% [羅馬天主教](../Page/羅馬天主教.md "wikilink")
  - 4% 其他宗教
  - 2% [摩門教](../Page/摩門教.md "wikilink")

### 重要城市

  - 阿尔伯克基
  - 圣达菲

## 教育

### 州立大學

  - [新墨西哥大學](../Page/新墨西哥大學.md "wikilink")
  - [新墨西哥州立大學](../Page/新墨西哥州立大學.md "wikilink")
  - [東新墨西哥大學](../Page/東新墨西哥大學.md "wikilink")（）
  - [新墨西哥高地大學](../Page/新墨西哥高地大學.md "wikilink")（New Mexico Highland
    University）
  - [西新墨西哥大學](../Page/西新墨西哥大學.md "wikilink")（）
  - [新墨西哥礦業及科技學院](../Page/新墨西哥礦業及科技學院.md "wikilink")

### 私立大學

  - [聖菲學院](../Page/聖菲學院.md "wikilink")（）
  - [西南學院](../Page/西南學院.md "wikilink")（）
  - [聖約翰學院](../Page/聖約翰學院.md "wikilink")
  - [聖璜學院](../Page/聖璜學院.md "wikilink")（）

## 重要職業運動

### 篮球

  - [NCAA](../Page/NCAA.md "wikilink")
      - University of New Mexico

### 美式足球

  - [NCAA](../Page/NCAA.md "wikilink")
      - University of New Mexico

### 棒球

  - [小聯盟](../Page/小聯盟.md "wikilink")
      - [愛伯克奇同位素](../Page/愛伯克奇同位素.md "wikilink")（Albuquerque
        Isotopes），3A級大平洋岸聯盟，母隊：[科羅拉多洛磯隊](../Page/科羅拉多洛磯隊.md "wikilink")
        (從2015-現在)

## 参考资料

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/新墨西哥州.md "wikilink")

1.  [Religious composition of adults in New
    Mexico](http://www.pewforum.org/religious-landscape-study/state/new-mexico/),
    Pew Research Center