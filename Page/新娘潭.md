[Hong_Kong_Bride's_Pool_Bridge.JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_Bride's_Pool_Bridge.JPG "fig:Hong_Kong_Bride's_Pool_Bridge.JPG")範圍內\]\]
[Bride's_Pool_Nature_trails_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Bride's_Pool_Nature_trails_Entrance.jpg "fig:Bride's_Pool_Nature_trails_Entrance.jpg")
[Bride_Pool_1.JPG](https://zh.wikipedia.org/wiki/File:Bride_Pool_1.JPG "fig:Bride_Pool_1.JPG")
**新娘潭**（英文：**Bride's
Pool**）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[大埔區之](../Page/大埔區.md "wikilink")[船灣郊野公園](../Page/船灣郊野公園.md "wikilink")。\[1\]
景點包括「新娘潭瀑布」及「新娘潭」，[瀑布的高度達](../Page/瀑布.md "wikilink")15米，水潭的面積廣闊而且水深，夏季有觀光客喜歡在水潭裡游泳，因為水源未受污染，而且水清見底，水溫清涼消暑。鄰近設置有自然教育徑、多處大型燒烤場、野餐檯及[洗手間](../Page/洗手間.md "wikilink")。

## 名稱由來

據說，以前有四位轎夫抬著一位[新娘](../Page/新娘.md "wikilink")，從[烏蛟騰嫁到](../Page/烏蛟騰.md "wikilink")[鹿頸](../Page/鹿頸.md "wikilink")，中途路過此處，由於地上石岩濕滑，其中一位轎夫滑倒，引致新娘連同花轎一併掉落瀑-{布}-下的水潭溺斃。此水潭因而得名“新娘潭”。新娘潭時有鬧鬼事件，有說在水中會看到死去新娘的腳，而新娘潭旁邊的[照鏡潭亦因傳說死去新娘在該潭上照鏡而得來的](../Page/照鏡潭.md "wikilink").

## 鄰近觀光熱點

  - [照鏡潭](../Page/照鏡潭.md "wikilink")：獲譽為香港「最壯觀的瀑布及水潭」。
  - [龍珠潭](../Page/龍珠潭.md "wikilink")
  - [船灣淡水湖](../Page/船灣淡水湖.md "wikilink")

## 公共交通

  - [巴士](../Page/公共汽車.md "wikilink")
      - 在[港鐡](../Page/港鐵.md "wikilink")[大埔墟站搭乘](../Page/大埔墟站.md "wikilink")[九龍巴士營運的](../Page/九龍巴士.md "wikilink")75K路線到[大美督](../Page/大美督.md "wikilink")，再步行約40分鐘。
      - 在[港鐡](../Page/港鐵.md "wikilink")[大埔墟站乘搭](../Page/大埔墟站.md "wikilink")[九龍巴士營運的](../Page/九龍巴士.md "wikilink")275R路線直達（只在星期日及公眾假期行駛）。
  - [小巴](../Page/香港小型巴士.md "wikilink")
      - 在[港鐡](../Page/港鐵.md "wikilink")[大埔墟站乘搭綠色專線小巴](../Page/大埔墟站.md "wikilink")20R路線。
      - 在[港鐡](../Page/港鐵.md "wikilink")[大埔墟站乘搭綠色專線小巴](../Page/大埔墟站.md "wikilink")20C路線到[大美督](../Page/大美督.md "wikilink")，再步行約40分鐘。

## 參考

<references />



[Category:香港自然風景](../Category/香港自然風景.md "wikilink")
[Category:大埔區](../Category/大埔區.md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")

1.  [1](http://www.afcd.gov.hk/tc_chi/country/cou_vis/cou_vis_cou/cou_vis_cou_pc/cou_vis_cou_pc.html)，漁農自然護理署。