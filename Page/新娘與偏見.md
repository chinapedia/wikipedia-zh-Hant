《**新娘與偏見**》（）是[2004年的](../Page/2004年電影.md "wikilink")[印度](../Page/印度電影.md "wikilink")[喜劇](../Page/喜劇片.md "wikilink")[歌舞片](../Page/歌舞片.md "wikilink")，由[古林德·乍得哈](../Page/古林德·乍得哈.md "wikilink")（）導演。可以說她是一部愛情歌舞片，亦是1813年[簡·奧斯汀的小說](../Page/簡·奧斯汀.md "wikilink")《[傲慢與偏見](../Page/傲慢與偏見.md "wikilink")》的歌舞雜燴版。

## 故事梗概

年輕、美麗且有學識的拉妮塔·巴什（Lalita
Bakshi──由[愛絲維婭·莉飾演](../Page/愛絲維婭·莉.md "wikilink")）
在朋友的婚禮上遇到美國加州酒店業主威廉·達西（Will
Darcy──由[马丁·亨德森飾演](../Page/马丁·亨德森.md "wikilink")）時，出入所料，兩人卻一見生恨。她認為他是一個自以為是的有錢瘋人，他認為她是一個沒見過世面的鄉村小妞，後來又經過好多機會，誤會始終未能消除，但是兩人經過反復比較，終於確認彼此是最合適不過的終身侶伴。。。

## 影片拍攝地

影片得到英國電影協會的大力資助，因此多數內、外景均在英國拍攝。

  - [白金漢郡](../Page/白金漢郡.md "wikilink")[Halton
    House](../Page/Halton_House.md "wikilink") - 安姆利沙的婚禮場面
  - [白金漢郡](../Page/白金漢郡.md "wikilink")[Stoke
    Park](../Page/Stoke_Park.md "wikilink") - 威廉·達西的比華麗山莊酒店外景
  - [倫敦](../Page/倫敦.md "wikilink")[萨默塞特府](../Page/萨默塞特府.md "wikilink") -
    泉水邊的舞會場面
  - [白金漢郡](../Page/白金漢郡.md "wikilink")[Turville](../Page/Turville.md "wikilink")
    - 伊利莎白的婚禮夢鄉場面
  - [伊陵片場](../Page/伊陵片場.md "wikilink") - 內景佈置場面
  - [白金漢郡](../Page/白金漢郡.md "wikilink")[Cobstone
    Windmill](../Page/Cobstone_Windmill.md "wikilink") - 風車場面
  - [倫敦](../Page/倫敦.md "wikilink")[小威尼斯](../Page/小威尼斯（倫敦）.md "wikilink")
  - [倫敦眼](../Page/倫敦眼.md "wikilink")
  - [倫敦](../Page/倫敦.md "wikilink")[國立電影大禮堂](../Page/倫敦國立電影大禮堂.md "wikilink")
  - [倫敦](../Page/倫敦.md "wikilink")[Southall](../Page/Southall.md "wikilink")

以下場面取景於印度：

  - [安姆利沙的](../Page/安姆利沙.md "wikilink")[錫克金廟](../Page/錫克金廟.md "wikilink")
  - [果阿的海灘](../Page/果阿.md "wikilink")

以下場面取景於美國：

  - [大峽谷](../Page/大峽谷.md "wikilink")
  - [洛杉磯](../Page/洛杉磯.md "wikilink")[迪士尼音樂廳](../Page/迪士尼音樂廳.md "wikilink")
  - [加州的](../Page/加州.md "wikilink")[桑塔莫尼卡海灘](../Page/圣莫尼卡.md "wikilink")

## 人物表

| 演員姓名                                                             | 片中角色名                            | 相對《傲慢與偏見》中的人物                                                                                       |
| ---------------------------------------------------------------- | -------------------------------- | --------------------------------------------------------------------------------------------------- |
| [Aishwarya Rai](../Page/Aishwarya_Rai.md "wikilink")             | 拉妮塔·巴克什（Lalita Bakshi）           | [伊利莎白·貝內特](../Page/伊丽莎白·班内特.md "wikilink")（Elizabeth Bennet）                                        |
| [Martin Henderson](../Page/Martin_Henderson.md "wikilink")       | 威廉·達西（William Darcy）             | [菲茲威廉·達西](../Page/達西先生.md "wikilink")（Fitzwilliam Darcy）                                            |
| [Daniel Gillies](../Page/Daniel_Gillies.md "wikilink")           | 約翰·威克漢姆（Johnny Wickham）          | 威克漢姆先生（[Mr. Wickham](../Page/Mr._Wickham.md "wikilink")）                                            |
| [Naveen Andrews](../Page/Naveen_Andrews.md "wikilink")           | 波拉基先生（Mr. Balraj）                | 賓利先生（[Mr. Bingley](../Page/Mr._Bingley.md "wikilink")）                                              |
| [阿努帕姆·卡爾](../Page/阿努帕姆·卡爾.md "wikilink")                         | 巴克什先生（Mr. Bakshi）                | 貝內特先生（[Mr. Bennet](../Page/Mr._Bennet_\(Pride_and_Prejudice\).md "wikilink")）                       |
| [Nadira Babbar](../Page/Nadira_Babbar.md "wikilink")             | 巴克什夫人（Mrs. Bakshi）               | 貝內特夫人（[Mrs. Bennet](../Page/Mrs._Bennet.md "wikilink")）                                             |
| [Namrata Shirodkar](../Page/Namrata_Shirodkar.md "wikilink")     | 佳亞·巴克什（Jaya Bakshi）              | 簡·貝內特（[Jane Bennet](../Page/Jane_Bennet.md "wikilink")）                                             |
| [Indira Varma](../Page/Indira_Varma.md "wikilink")               | 吉蘭·波拉基（Kiran Balraj）             | 卡羅琳·賓利（[Caroline Bingley](../Page/Caroline_Bingley.md "wikilink")）                                  |
| [Sonali Kulkarni](../Page/Sonali_Kulkarni.md "wikilink")         | 陳德拉·蘭芭（Chandra Lamba）            | 夏洛逖·盧卡斯（[Charlotte Lucas](../Page/Charlotte_Lucas_\(Pride_and_Prejudice_character\).md "wikilink")） |
| [Nitin Ganatra](../Page/Nitin_Ganatra.md "wikilink")             | 柯立先生（Mr. Kholi）                  | 柯林斯先生（[Mr. Collins](../Page/Mr._Collins.md "wikilink")）                                             |
| [Meghna Kothari](../Page/Meghna_Kothari.md "wikilink")           | 瑪亞·巴克什（Maya Bakshi）              | 瑪莉·貝內特（[Mary Bennet](../Page/Mary_Bennet.md "wikilink")）                                            |
| [Peeya Rai Chowdhary](../Page/Peeya_Rai_Chowdhary.md "wikilink") | 拉吉·巴克什（Lakhi Bakshi）             | 莉迪雅·貝內特（[Lydia Bennet](../Page/Lydia_Bennet.md "wikilink")）                                         |
| [Alexis Bledel](../Page/Alexis_Bledel.md "wikilink")             | 佐金娜·達西（Georgina "Georgie" Darcy） | 佐吉安娜·達西（[Georgiana Darcy](../Page/Georgiana_Darcy.md "wikilink")）                                   |
| [Marsha Mason](../Page/Marsha_Mason.md "wikilink")               | 凱瑟琳·達西（Catherine Darcy）          | 凱瑟琳·德波（[Lady Catherine de Bourgh](../Page/Lady_Catherine_de_Bourgh.md "wikilink")）                  |
| [Harvey Virdi](../Page/Harvey_Virdi.md "wikilink")               | 蘭芭夫人（Mrs. Lamba）                 | 盧卡斯夫人（[Lady Lucas](../Page/Lady_Lucas.md "wikilink")）                                               |
| [Georgina Chapman](../Page/Georgina_Chapman.md "wikilink")       | 安妮（Anne）                         | 安妮·德波（[Anne de Bourgh](../Page/Anne_de_Bourgh.md "wikilink")）                                       |
| 無人                                                               | 無此角色                             | 基隄·貝內特（[Kitty Bennet](../Page/Kitty_Bennet.md "wikilink")）                                          |

## 評價

「當[寶萊塢與](../Page/寶萊塢.md "wikilink")[好萊塢相遇](../Page/好萊塢.md "wikilink")......這實在是一個絕妙的匹配。」

## 雜聞

  - 影片對白由[英語](../Page/英語.md "wikilink")、[印度語](../Page/印度語.md "wikilink")、[烏爾都語原版錄制](../Page/烏爾都語.md "wikilink")，由于片中還有一段[西班牙語的小旁白](../Page/西班牙語.md "wikilink")，所以採用語言一共有四種。
  - 印度語原版的標題為《吧浬吧浬 -- 從安姆利沙到洛杉磯》（Balle, Balle -- Amritsar to L.A.）

## 外部連結

  - [《新娘與偏見》的公式網址](https://web.archive.org/web/20071118173008/http://www.miramax.com/bride/)

  -
  -
  - [Upperstall.com的述評](http://www.upperstall.com/bandp.html)

[Category:印度電影作品](../Category/印度電影作品.md "wikilink")
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:浪漫喜劇電影](../Category/浪漫喜劇電影.md "wikilink")
[Category:珍·奧斯汀](../Category/珍·奧斯汀.md "wikilink")