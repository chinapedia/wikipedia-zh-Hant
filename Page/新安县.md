**新安县**在[中国](../Page/中華人民共和國.md "wikilink")[河南省西北部](../Page/河南省.md "wikilink")、[黄河南岸](../Page/黄河.md "wikilink")，是[洛阳市下辖的一个](../Page/洛阳市.md "wikilink")[县](../Page/县.md "wikilink")；位于[黄河南岸](../Page/黄河.md "wikilink")，处河南、[山西二省交界处](../Page/山西省.md "wikilink")，坐标为北纬34°36′-35°05′，东经111°53′-112°19′之间；境南与[宜阳县接壤](../Page/宜阳县.md "wikilink")，西与[渑池县](../Page/渑池县.md "wikilink")、[义马市毗邻](../Page/义马市.md "wikilink")，北与[济源市](../Page/济源市.md "wikilink")、山西省[垣曲县交界](../Page/垣曲县.md "wikilink")，东与[洛阳市区](../Page/洛阳市.md "wikilink")、[孟津县相连](../Page/孟津县.md "wikilink")。

新安县为极具实力的[县（市）之一](../Page/县级行政区.md "wikilink")，2009年GDP总量为2,204,567万元（[折合322,730万美元](../Page/人民币汇率.md "wikilink")），[位居河南各县（市）第十六位](../Page/河南省各县市国内生产总值列表.md "wikilink")；人均GDP为44,665元（合6,539美元），[居第五位](../Page/河南省各县市国内生产总值列表.md "wikilink")。

## 行政区划

新安县辖10个镇、1个乡、2个产业区：城关镇、磁涧镇、石寺镇、五头镇、铁门镇、石井镇、仓头镇、北冶镇、曹村乡、正村镇、南李村镇、洛新产业集聚区（原属磁涧镇）、万基产业集聚区（原属铁门镇）。县人民政府驻城关镇。

## 歷史

根據歷史書及地方誌記載，新安縣[秦代建縣](../Page/秦代.md "wikilink")，[隋代曾改名谷州](../Page/隋代.md "wikilink")，因為境內有谷山。民國25年（1936年）恢復新安縣。

## 交通

  - [陇海铁路](../Page/陇海铁路.md "wikilink")。
  - [310国道](../Page/310国道.md "wikilink")、[连霍高速公路](../Page/连霍高速公路.md "wikilink")。

## 资源

新安矿藏资源的主要矿种有[煤](../Page/煤.md "wikilink")（储量10亿吨）、[铁](../Page/铁.md "wikilink")、[硫铁](../Page/硫铁.md "wikilink")（储量为2.15亿吨）、[铝矿](../Page/铝矿.md "wikilink")（铝矾土储量为1.5亿吨以上，铝含量为37.77%-71.78%）、[耐火粘土](../Page/耐火粘土.md "wikilink")（储量为2.3亿吨）、[石英岩](../Page/石英岩.md "wikilink")（储量为2.8亿吨）、[白云岩](../Page/白云岩.md "wikilink")、[硅沙](../Page/硅沙.md "wikilink")（储量7亿吨）、[石灰岩等](../Page/石灰岩.md "wikilink")20余种。

## 旅游

  - 汉[函谷关](../Page/函谷关.md "wikilink")
  - [千唐志斋](../Page/千唐志斋.md "wikilink")
  - [青要山](../Page/青要山.md "wikilink")
  - [紫荆山](../Page/紫荆山_\(河南\).md "wikilink")
  - [黄河](../Page/黄河.md "wikilink")[八里胡洞](../Page/八里胡洞.md "wikilink")
  - [洛阳龙潭大峡谷](../Page/洛阳龙潭大峡谷.md "wikilink")
  - [小浪底水库](../Page/小浪底水库.md "wikilink")[青要山旅游区](../Page/青要山旅游区.md "wikilink")：黄河截流后形成“千岛湖”景观。
  - 天坛（玉皇阁）
  - 通仙观
  - 西沃石窟
  - 盐东原始社会部落遗址
  - 西沃盐仓城遗址

## 来源参考

## 外部链接

  - [新安县人民政府](http://www.xinan.gov.cn/)

[新安县](../Page/category:新安县.md "wikilink")
[县](../Page/category:洛阳区县市.md "wikilink")
[洛阳](../Page/category:河南省县份.md "wikilink")