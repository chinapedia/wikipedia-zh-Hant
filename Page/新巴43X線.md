**新巴43X線**是香港一條已取消的巴士路線，來往[華貴邨及](../Page/華貴邨.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")（[會展](../Page/香港會議展覽中心.md "wikilink")）。

## 歷史

  - 1993年8月17日：本線投入服務，來往[中環（交易廣場）至華貴](../Page/中環（交易廣場）巴士總站.md "wikilink")。初期只於平日早上至黃昏提供服務，以普通巴士行走，假日不設服務，並繞經[摩理臣山道及](../Page/摩理臣山道.md "wikilink")[禮頓道](../Page/禮頓道.md "wikilink")，當時路線牌以<span style="border: 0px solid #000000; background: yellow; padding: 0px 2px; color: red">黃底紅字</span>顯示。
  - 1997年4月13日：與543線合併，並設假日服務及派出空調巴士行走，往華貴方向改由[堅拿道天橋直入](../Page/堅拿道天橋.md "wikilink")[香港仔隧道](../Page/香港仔隧道.md "wikilink")，往中環方向改經天樂里、[軒尼詩道及](../Page/軒尼詩道.md "wikilink")[杜老誌道](../Page/杜老誌道.md "wikilink")。
  - 1997年12月22日：配合[田灣邨重建落成](../Page/田灣邨.md "wikilink")，本線繞經該處。
  - 1998年9月1日：[中巴](../Page/中華巴士.md "wikilink")[專營權結束](../Page/專營權.md "wikilink")，本線由[新巴接辦至今](../Page/新巴.md "wikilink")。
  - 2000年4月2日：本線與43線重組，本線縮短至金鐘（東），同時減收全程車費。
  - 2003年：[香港爆發](../Page/香港.md "wikilink")[非典型肺炎](../Page/非典型肺炎.md "wikilink")，本線被削減班次，直至疫情受控才逐漸恢復。
  - 2004年12月19日：配合南區路線重組，改經[薄扶林道](../Page/薄扶林道.md "wikilink")、[士美非路及](../Page/士美非路.md "wikilink")[堅尼地城](../Page/堅尼地城.md "wikilink")，不再途經田灣、[香港仔](../Page/香港仔.md "wikilink")、[黃竹坑](../Page/黃竹坑.md "wikilink")、[香港仔隧道及](../Page/香港仔隧道.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")，而舊有的服務範圍由[70M延長至華貴取代](../Page/城巴70M線.md "wikilink")\[1\]。
  - 2008年6月8日：配合新巴路線加價，本線全程車費調整至$5.8。
  - 2010年4月17日：配合[新巴46X線縮減至平日早上服務](../Page/新巴46X線.md "wikilink")，平日09:00後及假日全日延長至[灣仔](../Page/灣仔.md "wikilink")（[會展](../Page/香港會議展覽中心.md "wikilink")），並改為循環運作，來回程繞經[田灣邨而不再停靠金鐘](../Page/田灣邨.md "wikilink")，平日09:00前時段則行車路線維持不變。由華貴開出的尾班車亦同時延長至23:20\[2\]。此外，本線的路線牌亦改為<span style="border: 0px solid #000000; background: purple; padding: 0px 2px; color: white">紫底白字</span>顯示。
  - 2011年10月17日：配合[添馬艦發展工程完成](../Page/添馬艦發展工程.md "wikilink")，本線改經[添美道](../Page/添美道.md "wikilink")、[龍匯道](../Page/龍匯道.md "wikilink")、[分域碼頭街](../Page/分域碼頭街.md "wikilink")、[港灣道](../Page/港灣道.md "wikilink")、[菲林明道](../Page/菲林明道.md "wikilink")、[會議道並增設龍匯道近](../Page/會議道.md "wikilink")[中信大廈及港灣道近](../Page/中信大廈.md "wikilink")[香港會議展覽中心分站](../Page/香港會議展覽中心.md "wikilink")，平日上午繁忙時間往返金鐘的服務則維持不變\[3\]。
  - 2012年2月15日：本線往灣仔方向增設[夏慤道近](../Page/夏慤道.md "wikilink")[新政府總部分站](../Page/政府總部_\(添馬艦\).md "wikilink")\[4\]。
  - 2015年5月17日：配合[港鐵](../Page/港鐵.md "wikilink")[西港島綫通車而作出的巴士路線重組計劃](../Page/港島綫西延.md "wikilink")，本線停止服務\[5\]。

## 服務時間及班次

| [華貴邨開](../Page/華貴邨.md "wikilink") | [灣仔](../Page/灣仔.md "wikilink")（[會展](../Page/香港會議展覽中心.md "wikilink")）／[金鐘站開](../Page/金鐘站.md "wikilink") |
| --------------------------------- | ------------------------------------------------------------------------------------------------------ |
| **日期**                            | **服務時間**                                                                                               |
| 星期一至六                             | 06:30-08:50                                                                                            |
| 09:10-19:50                       | 20                                                                                                     |
| 19:50-23:20                       | 30                                                                                                     |
|                                   | （約）20:35-00:05                                                                                         |
| 星期日及公眾假期                          | 06:50-19:50                                                                                            |
| 19:50-23:20                       | 30                                                                                                     |

## 收費

全程：$5.8

  - [瑪麗醫院往灣仔](../Page/瑪麗醫院.md "wikilink")（會展）／金鐘站：$5.1
  - [爹核士街往灣仔](../Page/爹核士街.md "wikilink")（會展）／金鐘站：$3.6
  - [港澳碼頭往華貴邨](../Page/港澳碼頭_\(香港\).md "wikilink")／金鐘站：$5.8
  - 瑪麗醫院往華貴邨：$4.2
  - [華富道往華貴邨](../Page/華富道.md "wikilink")：$3.2

<!-- end list -->

  - 往灣仔方向，由華貴邨至中山紀念公園登車的乘客，若需繼續前往皇后像廣場或之後往華貴邨方向的巴士站，需於香港會議展覽中心站重新繳付車資。

### 八達通轉乘優惠

乘客登上本線後指定時間內以同一張八達通卡在瑪麗醫院轉乘以下路線，或從以下路線登車後指定時間內以同一張八達通卡在瑪麗醫院轉乘本線，次程可獲車資折扣優惠：

  - 43X←→7、71、46X、90B，轉乘時限為90分鐘

<!-- end list -->

  - **43X往金鐘／灣仔** → 46X往灣仔，次程可獲$4.4折扣優惠
  - **43X往金鐘／灣仔** → 90B往金鐘，次程車資全免
  - 7往石排灣或71往黃竹坑 → **43X往華貴邨**，次程可獲$3.9折扣優惠
  - 46X往田灣邨或90B往海怡半島 → **43X往華貴邨**，次程車資全免

<!-- end list -->

  - 43X←→A10

<!-- end list -->

  - **43X往金鐘／灣仔** → A10往機場，回贈首程車資，轉乘時限為90分鐘
  - A10往鴨脷洲邨 → **43X往華貴邨**，次程車資全免，轉乘時限為120分鐘

## 行車路線

  - 常規班次

**經**：[田灣海旁道](../Page/田灣海旁道.md "wikilink")、[石排灣道](../Page/石排灣道.md "wikilink")、[田灣街](../Page/田灣街.md "wikilink")、[田灣山道](../Page/田灣山道.md "wikilink")、[香港仔海旁道](../Page/香港仔海旁道.md "wikilink")、石排灣道、[薄扶林道](../Page/薄扶林道.md "wikilink")、[士美菲路](../Page/士美菲路.md "wikilink")、[科士街](../Page/科士街.md "wikilink")、[加多近街](../Page/加多近街.md "wikilink")、[吉席街](../Page/吉席街.md "wikilink")、[堅彌地城海旁](../Page/堅彌地城海旁.md "wikilink")、[德輔道西](../Page/德輔道西.md "wikilink")、[水街](../Page/水街.md "wikilink")、[干諾道西](../Page/干諾道西.md "wikilink")、[干諾道中](../Page/干諾道中.md "wikilink")、[民吉街](../Page/民吉街.md "wikilink")、[中環（林士街）巴士總站](../Page/中環（林士街）巴士總站.md "wikilink")、民吉街、干諾道中、[夏愨道](../Page/夏愨道.md "wikilink")、[添美道](../Page/添美道.md "wikilink")、[龍匯道](../Page/龍匯道.md "wikilink")、[分域碼頭街](../Page/分域碼頭街.md "wikilink")、[港灣道](../Page/港灣道.md "wikilink")、[菲林明道](../Page/菲林明道.md "wikilink")、[會議道](../Page/會議道.md "wikilink")、分域碼頭街、天橋、夏愨道、干諾道中、[林士街天橋](../Page/香港4號幹線.md "wikilink")、干諾道西、[嘉安街](../Page/嘉安街.md "wikilink")、德輔道西、堅彌地城海旁、[山市街](../Page/山市街.md "wikilink")、[石山街](../Page/石山街.md "wikilink")、士美菲路、薄扶林道、石排灣道、田灣街、田灣山道及田灣海旁道。

  - 上午特別班次

**華貴邨開**經：田灣海旁道、香港仔海傍道、石排灣道、薄扶林道、士美菲路、科士街、加多近街、吉席街、堅彌地城海旁、德輔道西、水街、干諾道西、干諾道中、民吉街、中環（林士街）巴士總站、民吉街、干諾道中、夏愨道、[紅棉路支路](../Page/紅棉路.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[添馬街及](../Page/添馬街.md "wikilink")[德立街](../Page/德立街.md "wikilink")。

**金鐘站開**經：[樂禮街](../Page/樂禮街.md "wikilink")、德立街、添馬街、夏愨道、紅棉路支路、[琳寶徑](../Page/琳寶徑.md "wikilink")、[遮打道](../Page/遮打道.md "wikilink")、[昃臣道](../Page/昃臣道.md "wikilink")、干諾道中、[林士街天橋](../Page/香港4號幹線.md "wikilink")、干諾道西、嘉安街、德輔道西、堅彌地城海旁、山市街、石山街、士美菲路、薄扶林道、石排灣道、田灣山道及田灣海旁道。

### 沿線車站

[NWFB43XRtMap.png](https://zh.wikipedia.org/wiki/File:NWFB43XRtMap.png "fig:NWFB43XRtMap.png")

  - 常規班次

| [華貴邨開](../Page/華貴邨.md "wikilink") |
| --------------------------------- |
| **序號**                            |
| 1                                 |
| 2                                 |
| 3                                 |
| 4                                 |
| 5                                 |
| 6                                 |
| 7                                 |
| 8                                 |
| 9                                 |
| 10                                |
| 11                                |
| 12                                |
| 13                                |
| 14                                |
| 15                                |
| 16                                |
| 17                                |
| 18                                |
| 19                                |
| 20                                |
| 21                                |
| 22                                |
| 23                                |
| 24                                |
| 25                                |
| 26                                |
| 27                                |
| 28                                |
| 29                                |
| 30                                |
| 31                                |
| 32                                |
| 33                                |
| 34                                |
| 35                                |
| 36                                |
| 37                                |
| 38                                |
| 39                                |
| 40                                |
| 41                                |
| 42                                |
| 43                                |
| 44                                |
| 45                                |
| 46                                |
| 47                                |
| 48                                |
| 49                                |
| 50                                |

  - 上午特別班次

| [華貴邨開](../Page/華貴邨.md "wikilink") | [金鐘站開](../Page/金鐘站.md "wikilink")                                |
| --------------------------------- | ---------------------------------------------------------------- |
| **序號**                            | **車站名稱**                                                         |
| 1                                 | [華貴邨](../Page/華貴邨.md "wikilink")                                 |
| 2                                 | [牛奶公司冰廠及冷房](../Page/牛奶公司.md "wikilink")                          |
| 3                                 | [華富道](../Page/華富道.md "wikilink")                                 |
| 4                                 | [余振強紀念第二中學](../Page/余振強紀念第二中學.md "wikilink")                     |
| 5                                 | [薄扶林村](../Page/薄扶林村.md "wikilink")                               |
| 6                                 | [薄扶林水塘道](../Page/薄扶林水塘道.md "wikilink")                           |
| 7                                 | [嘉林閣](../Page/嘉林閣.md "wikilink")                                 |
| 8                                 | [心光學校](../Page/心光盲人院暨學校.md "wikilink")                           |
| 9                                 | [瑪麗醫院](../Page/瑪麗醫院.md "wikilink")                               |
| 10                                | 香港華人基督教聯會薄扶林道墳場                                                  |
| 11                                | 科士街                                                              |
| 12                                | [爹核士街](../Page/爹核士街.md "wikilink")                               |
| 13                                | [卑路乍灣公園](../Page/卑路乍灣公園.md "wikilink")                           |
| 14                                | [西祥街](../Page/西祥街.md "wikilink")                                 |
| 15                                | [歌連臣街](../Page/歌連臣街.md "wikilink")                               |
| 16                                | \-{[皇后大道西](../Page/皇后大道西.md "wikilink")}-                        |
| 17                                | 山道                                                               |
| 18                                | [嘉安街](../Page/嘉安街.md "wikilink")                                 |
| 19                                | [中山紀念公園](../Page/中山紀念公園.md "wikilink")                           |
| 20                                | [港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")                        |
| 21                                | [中環](../Page/中環.md "wikilink")（[林士街](../Page/林士街.md "wikilink")） |
| 22                                | [怡和大廈](../Page/怡和大廈.md "wikilink")                               |
| 23                                | 金鐘站                                                              |

## 營運狀況

由於服務範圍及時間有限，加上班次並不頻密，除繁忙時間外客量偏低。此外，本線南區至堅尼地城一段，亦與專線小巴[58及](../Page/香港島專線小巴58線.md "wikilink")[59線競爭](../Page/香港島專線小巴59線.md "wikilink")，是此路線客量偏低的其中一個主因；而且本線途經薄扶林道，沿途沿線各站分站眾多。

隨著本線於平日早上後及假日全日延長至灣仔（北），由堅尼地城往灣仔（北）只需$3.6，比[新巴18線及](../Page/新巴18線.md "wikilink")[新巴18P線的全程收費](../Page/新巴18P線.md "wikilink")$5.2還要便宜，而由灣仔港灣道至堅尼地城海旁一段更與新巴18P線完全相同，預期將吸引部份往來堅尼地城至灣仔會展一帶的乘客。

## 參考資料及注釋

  - 《二十世紀港島區巴士路線發展史》，容偉釗編著，BSI出版
  - 《香港島巴士路線與社區發展》，ISBN：9789888310067，出版商：中華書局(香港)有限公司，出版日期：2014年11月

## 外部連結

  - 新巴

<!-- end list -->

  - [新巴43X線](https://web.archive.org/web/20160304091619/http://mobileapp.nwstbus.com.hk/nw/?l=0&f=1&r=43X&v=D)
  - [新巴43X線（特別班次）](https://web.archive.org/web/20160304090406/http://mobileapp.nwstbus.com.hk/nw/?l=0&f=1&r=43X&v=P)
  - [新巴43X線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-NWFB-43X-43X-D.pdf)
  - [新巴43X線詳細時間表](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/TS-NWFB-43X-43X-D.pdf)

<!-- end list -->

  - 其他

<!-- end list -->

  - [681巴士總站－新巴43X線](http://www.681busterminal.com/hk-43x.html)

[043X](../Category/已取消新世界第一巴士路線.md "wikilink")
[043X](../Category/前中華巴士路線.md "wikilink")

1.  [新巴及城巴七條南區路線　本星期日實施新安排](http://www.nwstbus.com.hk/tc/uploadedPressRelease/1531_2004121501_chi.pdf)，2004年12月15日，[城巴](../Page/城巴.md "wikilink")／[新巴](../Page/新巴.md "wikilink")
2.  [新巴43X及46X號線實施新安排](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/14042010-CB.pdf)，2010年4月14日，[城巴](../Page/城巴.md "wikilink")／[新巴](../Page/新巴.md "wikilink")
3.  [新巴18、43X及720A號線實施新服務安排](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/14102011-CB_1.pdf)，2011年10月14日，[城巴](../Page/城巴.md "wikilink")／[新巴](../Page/新巴.md "wikilink")
4.  [新巴18、43X、720A及914X號線將繞經夏慤道新政府總部](http://www.nwstbus.com.hk/tc/uploadedPressRelease/5091_14022012-chi.pdf)，2012年2月14日，[城巴](../Page/城巴.md "wikilink")／[新巴](../Page/新巴.md "wikilink")
5.  [城巴新巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/Poster/WIE0510CTB.pdf)