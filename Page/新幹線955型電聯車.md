[300X_955-6_Maibara_19990714.jpg](https://zh.wikipedia.org/wiki/File:300X_955-6_Maibara_19990714.jpg "fig:300X_955-6_Maibara_19990714.jpg")

**新幹線955型電聯車**，又稱**300X**，是[東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")（JR東海）為設計[300系後繼車種在](../Page/新幹線300系電力動車組.md "wikilink")[1995年以次世代](../Page/1995年.md "wikilink")[新幹線技術製作的一款](../Page/新幹線.md "wikilink")[實驗列車](../Page/实验.md "wikilink")。

## 簡介

1995年，JR東海製造了一列六輛編組的高速實驗車輛，並定名為300X（正式名稱為「新幹線955型電聯車」）； 該編組所有車輛均為動力車。

1996年7月26日凌晨，該編組在[東海道新幹線](../Page/東海道新幹線.md "wikilink")[京都至](../Page/京都站.md "wikilink")[米原間創下最高速度時速](../Page/米原站.md "wikilink")443公里的紀錄。

該編組曾多次在東海道新幹線和[山陽新幹線進行列車試驗](../Page/山陽新幹線.md "wikilink")，並為後續的[700系以及N](../Page/新幹線700系電聯車.md "wikilink")700系車輛提供許多關鍵技術的實驗機會。

在該編組於2002年1月終止使用後，JR東海於同年2月1日正式宣布其除役廢車。\[1\]

## 保存車輛

  - 995-1：和[WIN350](../Page/新幹線500系900番台電力動車組.md "wikilink")（新幹線500系900番台實驗車）一起擺放在位於[滋賀縣](../Page/滋賀縣.md "wikilink")[米原市的財團法人](../Page/米原市.md "wikilink")[鐵道綜合技術研究所風洞測試中心](../Page/鐵道綜合技術研究所.md "wikilink")
  - 955-6：展示於JR東海[磁悬浮・铁道馆](../Page/磁浮·鐵道館.md "wikilink")

## 參考

[955](../Category/新幹線車輛.md "wikilink")
[Category:東海旅客鐵道車輛](../Category/東海旅客鐵道車輛.md "wikilink")
[Category:日本電力動車組](../Category/日本電力動車組.md "wikilink")

1.  [300X 新幹線試験車両,三菱重工技報 Vol.32
    No.4(1995)](https://web.archive.org/web/20160304112915/http://www.mhi.co.jp/technology/review/pdf/324/324293.pdf)