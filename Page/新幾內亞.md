**新畿內亞**（；[巴布亞皮欽語](../Page/巴布亞皮欽語.md "wikilink")：Niugini；）位於[澳大利亞北面](../Page/澳大利亞.md "wikilink")，是[世界上第二大島嶼](../Page/島嶼面積列表.md "wikilink")。有時，本島亦被稱為「**巴布亞**」，但有時巴布亞亦單指本島的一部份。此外，[印尼官方过去稱呼本島為](../Page/印尼.md "wikilink")「**伊里安岛**」（
），至2002年开始才称为**巴布亚岛**（）。

19世紀受[歐洲國家殖民侵略](../Page/歐洲.md "wikilink")，[國界大致上沿用昔日](../Page/國界.md "wikilink")[德](../Page/德意志帝國.md "wikilink")、[荷佔領地一分為二](../Page/荷蘭.md "wikilink")：位於[東經](../Page/東經.md "wikilink")141度以東的，屬於[巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink")，由1975年開始脫離澳大利亞成為獨立的國家；位於[東經](../Page/東經.md "wikilink")141度以西的，屬於[印尼的](../Page/印尼.md "wikilink")[巴布亞省和](../Page/巴布亞省.md "wikilink")[西巴布亞省](../Page/西巴布亞省.md "wikilink")。這部份一直都有分離活動，要求從印尼獨立。

## 歷史

本島從4萬五千年前已開始有人類居住，島上約有一千多個不同種族，都屬於[美拉尼西亞人種](../Page/美拉尼西亞.md "wikilink")，都講[巴布亞語系的語言](../Page/巴布亞語系.md "wikilink")。

本島的歐洲殖民歷史始於1828年，當時[荷蘭人佔領了本島的西半部](../Page/荷蘭.md "wikilink")，並相繼於1895年設立貿易站，及於1910年建立省城Hollandia（即今日的[查亞普拉](../Page/查亞普拉.md "wikilink")）。

1883年，[法國佔領了本島的東南部](../Page/法國.md "wikilink")，改名為[新愛爾蘭](../Page/新幾內亞.md "wikilink")，但很快又被[昆士蘭自治殖民領佔領](../Page/昆士蘭州.md "wikilink")。[英國在](../Page/英國.md "wikilink")1884年反對昆士蘭佔據新愛爾蘭，並把當地變成由英國直接管轄。餘下本島的東北部亦於同年被[德國佔領](../Page/德意志帝國.md "wikilink")，並宣稱為其[保護地](../Page/保護地.md "wikilink")。

1906年，英國把新愛爾蘭的管轄權交給[澳大利亞](../Page/澳大利亞.md "wikilink")。[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，澳大利亞強行奪取德國在新幾內亞的屬地，並於1920年得到[國際聯盟的承認](../Page/國際聯盟.md "wikilink")。

1942年，日本軍隊南下至本島，同時入侵[荷屬新幾內亞及東部澳大利亞的領土](../Page/荷屬新幾內亞.md "wikilink")，使本島東部和北部的高地成為了[第二次世界大戰中西南](../Page/第二次世界大戰.md "wikilink")[太平洋的主戰場](../Page/太平洋.md "wikilink")。盟軍和日軍各自動員所屬佔領區內的原住民協助軍事行動，這種動員有時會造成相同的族群在同時間為交戰雙方所徵召。這種割裂和戰爭所帶來的破壞，帶給了原住民嚴重的傷害。\[1\]

第二次世界大戰結束後，新幾內亞西部於1959年舉行選舉，成立巴布亞議會，並籌備於1961年4月5日獨立。當時議會已決定了新成立的國家的國號為[新巴布亞](../Page/西新几内亚.md "wikilink")、訂立了新的國徽、新國歌，以及以[晨星為圖案的新國旗](../Page/晨星.md "wikilink")。新國旗於1961年12月1日升起，並與[荷蘭國旗並排](../Page/荷蘭國旗.md "wikilink")。1961年12月18日，[印尼入侵西巴布亞](../Page/印度尼西亞歷史.md "wikilink")，結束了它短暫的獨立。

1975年，澳大利亞正式給予新幾內亞東部全面獨立的地位，成立了[巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink")。

## 地理

新几內亚在七千年前仍与澳大利亞相連，後由於海平面上升，使兩者被海水分隔，形成了今日的[托列斯海峽](../Page/托列斯海峽.md "wikilink")。

雪山山脉和[马勒山脉横贯全岛](../Page/马勒山脉.md "wikilink")，海拔4000米以上；最高峰[查亚峰高达](../Page/查亚峰.md "wikilink")5030米，南部的[里古-弗莱平原为最大平原](../Page/里古-弗莱平原.md "wikilink")，沿海多沼泽和红树林。

地处赤道南侧，东南沿海属[热带草原气候](../Page/热带草原气候.md "wikilink")，海拔1000米以上属[山地气候](../Page/山地气候.md "wikilink")，其余地区属[热带雨林气候](../Page/热带雨林气候.md "wikilink")；北半部年降水量3000毫米以上，南部1000－2000多毫米，1－4月常受热带飓风袭击。

## 生態

在大約786,000[平方千米的](../Page/平方千米.md "wikilink")[熱帶土地當中](../Page/熱帶.md "wikilink")，新幾内亞有巨大的生態價值：共有11,000種[植物](../Page/植物.md "wikilink")；將近600種獨特的[鳥類](../Page/鳥類.md "wikilink")，包括[極樂鳥](../Page/極樂鳥.md "wikilink")；超過400種[兩棲類動物](../Page/兩棲類.md "wikilink")；455種[蝴蝶](../Page/蝴蝶.md "wikilink")；[哺乳動物種類包括](../Page/哺乳動物.md "wikilink")[樹袋鼠](../Page/樹袋鼠.md "wikilink")、[長吻針鼴](../Page/長吻針鼴.md "wikilink")、高山鼠、[斑袋貂與](../Page/斑袋貂.md "wikilink")[袋貂等](../Page/袋貂.md "wikilink")[有袋動物](../Page/有袋動物.md "wikilink")，和其他各式各樣的[哺乳動物](../Page/哺乳動物.md "wikilink")。在島上大多數物種的起源，至少是與直到相當最近一次地質時期與其相連的[澳大利亚大陆相同](../Page/澳洲.md "wikilink")。

## 参考文献

## 外部連結

  - [*The Intoxicating Birds of New Guinea* by John
    Tidwell](https://web.archive.org/web/20040603091709/http://natzoo.si.edu/Publications/ZooGoer/2001/2/intoxnewguineabirds.cfm)

## 参见

  - [存在多个国家的岛屿列表](../Page/存在多个国家的岛屿列表.md "wikilink")
  - [自由巴布亚运动](../Page/自由巴布亚运动.md "wikilink")

{{-}}

[N](../Category/太平洋岛屿.md "wikilink")
[N](../Category/大洋洲岛屿.md "wikilink")
[新幾內亞](../Category/新幾內亞.md "wikilink")
[Category:跨國島嶼](../Category/跨國島嶼.md "wikilink")

1.  Brij V. Lal,Kate Fortune, *The Pacific Islands: An Encyclopedia*,
    [University of Hawaii Press](../Page/夏威夷大學出版社.md "wikilink"), 2000.
    p243\~248