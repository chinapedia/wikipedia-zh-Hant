**新-{庄}-町**（）本屬奈良縣轄下的一個町。2004年10月1日與同郡的[當麻町合併至](../Page/當麻町_\(奈良縣\).md "wikilink")[葛城市](../Page/葛城市.md "wikilink")。合併後人口約35000人。

## 歷史

新-{庄}-町於古代[大和國](../Page/大和國.md "wikilink")[忍海郡屬地](../Page/忍海郡.md "wikilink")、現剩下葛木縣神社（葛木）、葛木座位火雷神社（笛吹）、角刺神社（忍海）等的[延喜式神名帳](../Page/延喜式神名帳.md "wikilink")。再有相傳是神秘的女王[飯豐青皇女的忍海角刺皇宮](../Page/飯豐青皇女.md "wikilink")，北花內有被認為是飯豐天皇皇陵的前方後日元墳。

## 施設

  - 社會教育中心（寺口）

## 行政

  - 町長:吉川義彦

## 姊妹都市・友好都市

### 國內

  - [新-{庄}-市](../Page/新庄市.md "wikilink")（[山形縣](../Page/山形縣.md "wikilink")）
  - [新-{庄}-村](../Page/新庄村.md "wikilink")（[岡山縣](../Page/岡山縣.md "wikilink")[真庭郡](../Page/真庭郡.md "wikilink")）

## 外部連結

  - [新-{庄}-町・當麻町合併協議会會議](https://web.archive.org/web/20070629222339/http://www.town.shinjo.nara.jp/gappei/)

[Category:北葛城郡](../Category/北葛城郡.md "wikilink")
[Category:葛城市](../Category/葛城市.md "wikilink")