《**新戀愛白書**》（原名：**BOYS
BE…**）是原作，[玉越博幸绘画的一部](../Page/玉越博幸.md "wikilink")[少年漫画作品](../Page/少年漫画.md "wikilink")，在台灣翻譯成**新戀愛白書**。另被改编成動畫[BOYS
BE...](../Page/BOYS_BE....md "wikilink") 與日本的舞台劇。

## 介紹

《新戀愛白書I》 第一部為1-32集冊，在這眾多的小故事中，只有3個失戀的故事，其它都是成功的青春告白。

《新戀愛白書II》 第二部為1-20集冊，在這裡還是沿用傳統的小故事，不過在這其中多了許許多多的白癡舉動的少年回憶，在其中的BOYS
BE...中也首度出現了接續性的故事情結，名為BOYS　BE...JUSTICE雖然只有短短的六集卻非常的豐富好玩。

《新戀愛白書III》
第三部為1-6集就結束了，這次已經是個連貫性的長篇故事，其中最大的改變為女主角變為了固執、不愛理人擁有不同於一般[少年漫畫的嬌羞小女生的](../Page/少年漫畫.md "wikilink")[不良少女](../Page/不良少女.md "wikilink")，是一個時代的轉變吧！板橋雅宏如此說，在這本看得出來整體的畫風已經逐漸的成熟，筆法也較為細膩了。

然後於2009年4月24日時日本講談社[MouRA發佈了最新的連載消息](../Page/MouRA.md "wikilink")，同樣由[板橋雅宏著作與](../Page/板橋雅宏.md "wikilink")[玉越博幸畫](../Page/玉越博幸.md "wikilink")，同時也會加入很多知名著作與畫家一起聯合創作，並取名為BOYS
BE...2009 1学期。

## 出場人物

## 音樂CD

## 舞台劇

## 電視動畫

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:2000年日本電視動畫](../Category/2000年日本電視動畫.md "wikilink")
[Category:WOWOW動畫](../Category/WOWOW動畫.md "wikilink")
[Category:少年漫畫](../Category/少年漫畫.md "wikilink")
[Category:週刊少年Magazine連載作品](../Category/週刊少年Magazine連載作品.md "wikilink")
[Category:Magazine Special](../Category/Magazine_Special.md "wikilink")
[Category:青年漫畫](../Category/青年漫畫.md "wikilink")
[Category:Evening](../Category/Evening.md "wikilink")
[Category:高中背景漫畫](../Category/高中背景漫畫.md "wikilink")
[Category:高中背景動畫](../Category/高中背景動畫.md "wikilink")
[Category:戀愛漫畫](../Category/戀愛漫畫.md "wikilink")
[Category:戀愛動畫](../Category/戀愛動畫.md "wikilink") [Category:HAL
FILM MAKER](../Category/HAL_FILM_MAKER.md "wikilink")