[Making_a_Nova.jpg](https://zh.wikipedia.org/wiki/File:Making_a_Nova.jpg "fig:Making_a_Nova.jpg")

**新星**是[激变变星的一类](../Page/激变变星.md "wikilink")，是由[吸積在](../Page/吸積.md "wikilink")[白矮星表面的](../Page/白矮星.md "wikilink")[氫造成劇烈的核子爆炸的現象](../Page/氫.md "wikilink")。这类星通常原本都很暗，难以发现，爆发时突然增亮，被认为是新产生的[恒星](../Page/恒星.md "wikilink")，因此而得名。新星按光度下降速度分为[快新星](../Page/快新星.md "wikilink")（NA）、[中速新星](../Page/中速新星.md "wikilink")（NAB）、[慢新星](../Page/慢新星.md "wikilink")（NB）和[甚慢新星](../Page/甚慢新星.md "wikilink")（NC），爆发时亮度会增加几万、几十万甚至几百万倍，持续几星期或几年。但不能和[Ia超新星或其它恆星的爆炸混淆](../Page/Ia超新星.md "wikilink")，包括[加州理工學院在](../Page/加州理工學院.md "wikilink")2007年5月首度發現的[發光紅新星](../Page/發光紅新星.md "wikilink")。

目前在[银河系中已发现超过](../Page/银河系.md "wikilink")200颗新星。

## 發展

如果[白矮星有一顆距離夠近的伴星](../Page/白矮星.md "wikilink")，使它能在伴星的[洛希半徑內](../Page/洛希極限.md "wikilink")，因此能穩定的從伴星的外層大氣吸积氣體於表面。這顆伴星可以是一顆[主序星](../Page/主序星.md "wikilink")，或是已經膨脹成[紅巨星的老年恆星](../Page/紅巨星.md "wikilink")。被捕獲的氣體主要是[氫和](../Page/氫.md "wikilink")[氦](../Page/氦.md "wikilink")，兩種都是[宇宙間最](../Page/宇宙.md "wikilink")[平常與最主要的成份](../Page/重子物質.md "wikilink")。吸積在白矮星表面的氣體因為重力被壓得更緊密，壓力使得溫度變得非常的高並且傳導至內部。白矮星包含的[簡併物質不會因為受熱而膨脹](../Page/簡併物質.md "wikilink")，而受到壓縮的氫氣不斷在表面增長。氫融合的速率受到溫度和壓力的影響，這意味著只要繼續壓縮，表面的溫度和壓力就會繼續增加，當溫度達到2,000萬[K時](../Page/熱力學溫標.md "wikilink")，[核融合反應就會發生](../Page/核融合.md "wikilink")；在這種溫度下的氫主要經由[碳氮氧循環燃燒](../Page/碳氮氧循環.md "wikilink")。對多數的雙星系統，氫燃燒的熱量是不穩定的，並且會很快的將大量的氫轉換成其他[元素](../Page/化學元素.md "wikilink")，而造成[熱失控反應](../Page/熱失控.md "wikilink")\[1\]（只有在範圍很窄的吸積率下，氫融合可以在表面穩定的進行）。這個過程會释放出大量的能量，使白矮星發生極端明亮的爆發，並將表面剩餘的氣體吹散。光度的上升是快還是慢，與新星的類型有關，而在到達高峰之後，光度的下降是很穩定的\[2\]。從最大光度下降2至3個星等所花費的時間，可以用來對新星進行分類。[快新星在短於](../Page/快新星.md "wikilink")25天的時間內光度會下降2等，[慢新星則會超過](../Page/慢新星.md "wikilink")80天才降低2星等\[3\]。

但無論變化有多劇烈，新星所拋出的質量大約只有[太陽質量的萬分之一](../Page/太陽質量.md "wikilink")，相較於白矮星的質量是非常小的。此外，也只有5%吸積的質量參與核融合成為爆發的動力\[4\]。但是，這已有足夠的能量讓噴出物的速度高達每秒數千[公里](../Page/公里.md "wikilink")
－ 快新星的速度比慢新星快，並同時讓光度從太陽的數倍增加至50,000至100,000倍\[5\]\[6\]。

只要伴星能繼續的供應氫在白矮星的表面吸積，一顆白矮星就能反覆的爆發成為新星，例如[蛇夫座
RS](../Page/蛇夫座_RS.md "wikilink")，就是一顆已經知道有過6次爆發記錄的新星（分別在1893、1933、1958、1967、1985和2006年）。最後，白矮星或是將燃料用盡，或是塌縮成為[中子星](../Page/中子星.md "wikilink")，或是爆炸成為[Ia超新星](../Page/Ia超新星.md "wikilink")。

有時，新星會有足夠的亮度，並且以肉眼就能清楚的看見，在最近的例子就是1975年明亮的[天鵝座新星](../Page/天鵝座V1500.md "wikilink")。這顆新星於1975年8月29日出現在天鵝座的[天津四北方約](../Page/天津四.md "wikilink")5度之處，[視星等達到](../Page/視星等.md "wikilink")2.0等（與天津四的光度相似）。最靠近現在的是[天蝎座V1280](../Page/天蝎座V1280.md "wikilink")，在2007年2月17日亮度達到3.7等。

## 出現的機率，和天文物理上的意義

天文學家以[銀河系每年粗略估計有](../Page/銀河系.md "wikilink")20至60顆新星出現的經驗，估計出現率為每年40顆\[7\]。每年被發現的新星數量低於此一數值被歸咎於距離的遙遠和[觀測的偏差](../Page/選擇性的偏差.md "wikilink")\[8\]。比較之下，每年在[仙女座大星系發現的新星數量更低](../Page/仙女座大星系.md "wikilink")，只有銀河系的1/2到1/3\[9\]。

觀察新星噴發出[星雲的](../Page/星雲.md "wikilink")[光譜](../Page/光譜學.md "wikilink")，已經發現其中含有豐富的氦、[碳](../Page/碳.md "wikilink")、[氮](../Page/氮.md "wikilink")、[氧](../Page/氧.md "wikilink")、[氖和](../Page/氖.md "wikilink")[鎂等元素](../Page/鎂.md "wikilink")\[10\]。新星對[星際物質的貢獻並不大](../Page/星際物質.md "wikilink")，在[銀河系內只相當於超新星的](../Page/銀河系.md "wikilink")1/50，[紅巨星和](../Page/紅巨星.md "wikilink")[超巨星的](../Page/超巨星.md "wikilink")1/200\[11\]。

再發新星，像是[蛇夫座
RS](../Page/蛇夫座_RS.md "wikilink")（再發的周期大約是數十年）是罕見的。儘管理論上認為多數的新星
－ 即使不是全部 －
都會再發，然而時間的尺度可能要長達1,000年到100,000年\[12\]。新星再現的時間間隔依靠白矮星質量吸積的速率、表面重力的強度；質量較大的白矮星吸積足夠下次爆炸的燃料所需要的時間短於質量較低的\[13\]。結果是，質量大的白矮星再發的間隔較短\[14\]。

## 歷史的意義

1572年，丹麥天文學家[第谷·布拉赫在](../Page/第谷·布拉赫.md "wikilink")[仙后座觀察到超新星](../Page/仙后座.md "wikilink")[SN
1572](../Page/SN_1572.md "wikilink")，並且在他的著作《de nova
stella》（[拉丁文](../Page/拉丁文.md "wikilink")，意思為“關於新星”）中描述時，給了“新星”這個名稱。在書中，他以近處的物體應該會相對於恆星產生位置的改變，來論述說新星的距離非常遙遠。雖然這是一顆超新星，而不是一顆傳統的新星，但直到1930年代人們才意識到這兩個概念的不同\[15\]。

## 新星做為距離的指標

新星有些特性可以做為距離的[標準燭光](../Page/標準燭光.md "wikilink")，像是[絕對星等的分布是](../Page/絕對星等.md "wikilink")[雙峰的](../Page/雙峰分布.md "wikilink")，一個主峰值在-7.5等，另一個次要的在-8.8等；大致上在峰值之後的15天，會有相似的絕對星等（-5.5）。以新星建立的距離估計，和以[造父變星對鄰近的](../Page/造父變星.md "wikilink")[星系和](../Page/星系.md "wikilink")[星系團估計的距離比較](../Page/星系團.md "wikilink")，它們是比較準確的\[16\]。

## 新星是鋰元素工廠

2015年2月19日，日本[國立天文台研究團隊從觀察](../Page/國立天文台.md "wikilink")[2013年海豚座新星發現](../Page/2013年海豚座新星.md "wikilink")，新星爆炸製成大量[鋰元素](../Page/鋰.md "wikilink")，這意味著經典新星爆炸可能是宇宙製造鋰元素的主要機制\[17\]。

## 1890年以後的明亮新星

| 年度    | 新星                                               | 最大亮度                |
| ----- | ------------------------------------------------ | ------------------- |
| 1891年 | [御夫座T](../Page/御夫座T.md "wikilink")               | 3.8等                |
| 1898年 | [人馬座V1059](../Page/人馬座V1059.md "wikilink")       | 4.5等                |
| 1899年 | [天鷹座V606](../Page/天鷹座V606.md "wikilink")         | 5.5等                |
| 1901年 | [英仙座GK](../Page/英仙座GK.md "wikilink")             | 0.2等                |
| 1903年 | [双子座DM](../Page/双子座DM.md "wikilink")             | 6等                  |
| 1905年 | [天鹰座V604](../Page/天鹰座V604.md "wikilink")         | 7.3等                |
| 1910年 | [蝎虎座DI](../Page/蝎虎座DI.md "wikilink")             | 4.6等                |
| 1912年 | [1912雙子座新星](../Page/1912雙子座新星.md "wikilink")     | 3.5 等               |
| 1918年 | [天鷹座V603](../Page/天鷹座V603.md "wikilink")         | −1.8等               |
| 1919年 | [天琴座HR](../Page/天琴座HR.md "wikilink")             | 7.4等                |
| 1919年 | [蛇夫座V849](../Page/蛇夫座V849.md "wikilink")         | 7.4等                |
| 1920年 | [天鹅座V476](../Page/天鹅座V476.md "wikilink")         | 2.0等                |
| 1925年 | [繪架座RR](../Page/繪架座RR.md "wikilink")             | 1.2等                |
| 1934年 | [武仙座DQ](../Page/武仙座DQ.md "wikilink")             | 1.4等                |
| 1936年 | [蝎虎座CP](../Page/蝎虎座CP.md "wikilink")             | 2.1等                |
| 1939年 | [麒麟座BT](../Page/麒麟座BT.md "wikilink")             | 4.5等                |
| 1942年 | [船尾座CP](../Page/船尾座CP.md "wikilink")             | 0.3等                |
| 1943年 | [天鷹座V500](../Page/天鷹座V500.md "wikilink")         | 6.1等                |
| 1950年 | [蝎虎座DK](../Page/蝎虎座DK.md "wikilink")             | 5.0等                |
| 1960年 | [武仙座V446](../Page/武仙座V446.md "wikilink")         | 2.8等                |
| 1963年 | [武仙座V533](../Page/武仙座V533.md "wikilink")         | 3等                  |
| 1970年 | [巨蛇座FH](../Page/巨蛇座FH.md "wikilink")             | 4等                  |
| 1975年 | [天鵝座V1500](../Page/天鵝座V1500.md "wikilink")       | 2.0等                |
| 1975年 | [盾牌座V373](../Page/盾牌座V373.md "wikilink")         | 6等                  |
| 1976年 | [狐狸座NQ](../Page/狐狸座NQ.md "wikilink")             | 6等                  |
| 1978年 | [天鵝座V1668](../Page/天鵝座V1668.md "wikilink")       | 6等                  |
| 1984年 | [狐狸座QU](../Page/狐狸座QU.md "wikilink")             | 5.2等                |
| 1986年 | [半人馬座V842](../Page/半人馬座V842.md "wikilink")       | 4.6等                |
| 1991年 | [武仙座V838](../Page/武仙座V838.md "wikilink")         | 5.0等                |
| 1992年 | [天鵝座V1974](../Page/天鵝座V1974.md "wikilink")       | 4.2等                |
| 1999年 | [天鷹座V1494](../Page/天鷹座V1494.md "wikilink")       | 5.03等               |
| 1999年 | [船帆座V382](../Page/船帆座V382.md "wikilink")         | 2.6等                |
| 2006年 | [蛇夫座 RS](../Page/蛇夫座_RS.md "wikilink")           | 4.5等                |
| 2007年 | [天蝎座V1280](../Page/天蝎座V1280.md "wikilink")       | 3.9等 \[18\]\[19\]   |
| 2009年 | [波江座KT](../Page/波江座KT.md "wikilink")             | 5.5等                |
| 2013年 | [2013年海豚座新星](../Page/2013年海豚座新星.md "wikilink")   | 4.3等                |
| 2013年 | [2013年半人马座新星](../Page/2013年半人马座新星.md "wikilink") | 3.6等<ref>{{cite web |
| 2015年 | [2015年人马座新星2](../Page/2015年人马座新星2.md "wikilink") | 4.3等<ref>{{cite web |

<small>註：請隨時加入亮度超過6.0等以上的新星。[](https://web.archive.org/web/20050912175943/http://www.tsm.toyama.toyama.jp/curators/aroom/var/nova/1600.htm)</small>

## 註

  - 在[摩根—肯納光譜分類下](../Page/恆星光譜#摩根-肯那光譜分類法.md "wikilink")，新星的光譜屬於 **Q**
    型。

## 相關條目

  - [激變變星](../Page/激變變星.md "wikilink")
  - [蟹狀星雲](../Page/蟹狀星雲.md "wikilink")
  - [矮新星](../Page/矮新星.md "wikilink")
  - [极超新星](../Page/极超新星.md "wikilink")
  - [新星遺跡](../Page/新星遺跡.md "wikilink")
  - [超新星](../Page/超新星.md "wikilink")

### 再發新星

  - [蛇夫座 RS](../Page/蛇夫座_RS.md "wikilink")
  - [北冕座T](../Page/北冕座T.md "wikilink")
  - [羅盤座 T](../Page/羅盤座_T.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [General Catalog of Variable
    Stars](http://www.sai.msu.su/groups/cluster/gcvs/gcvs/), [Sternberg
    Astronomical
    Institute](../Page/Sternberg_Astronomical_Institute.md "wikilink"),
    [Moscow](../Page/Moscow.md "wikilink")
  - [NASA Observatorium: Classical
    Nova](https://web.archive.org/web/20070510100249/http://observe.arc.nasa.gov/nasa/space/stellardeath/stellardeath_4a.html)

[Category:恒星](../Category/恒星.md "wikilink")
[Category:激變變星](../Category/激變變星.md "wikilink")
[Category:新星](../Category/新星.md "wikilink")
[Category:恆星現象](../Category/恆星現象.md "wikilink")

1.  Prialnik, Dina. "Novae", pp. 1846-56, in Paul Murdin, ed.
    *Encyclopedia of Astronomy and Astrophysics.* London: Institute of
    Physics Publishing Ltd and Nature Publishing Group, 2001. ISBN
    978-1-56159-268-5

2.  [AAVSO](../Page/AAVSO.md "wikilink") Variable Star Of The Month:
    [May 2001: Novae](http://www.aavso.org/vstar/vsots/0501.shtml)

3.

4.
5.
6.  Zeilik, Michael. *Conceptual Astronomy*. New York: John Wiley &
    Sons, Inc., 1993. ISBN 978-0-471-50996-7

7.
8.  Muirden, James. "Searching for Novae", pp. 259-79. In James Muirden,
    ed., *Sky Watcher's Handbook*. New York: W.H. Freeman and Company
    Ltd., 1993. ISBN 978-0-7167-4502-0

9.  W. Liller, B. Mayer, July 1987, "The rate of nova production in the
    Galaxy", *Publications Astronomical Society of the Pacific*, vol.
    99, pp. 606-609.

10.
11.
12. Seeds, Michael A. *Horizons: Exploring the Universe*, 5th ed.
    Belmont: Wadsworth Publishing Company, 1998, ISBN 978-0-534-52434-0,
    p.194.

13.
14.
15.
16. Alloin, D., and W. Gieren, eds. *Stellar Candles for the
    Extragalactic Distance Scale.* Robert Gilmozzi and Massimo Della
    Valle, "Novae as Distance Indicators", pp. 229-241. Berlin:
    Springer, 2003. ISBN 978-3-540-20128-1.

17. <http://www.nao.ac.jp/en/news/science/2015/20150218-subaru.html>

18.

19.