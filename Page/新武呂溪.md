**新武呂溪**是[台灣河川](../Page/台灣.md "wikilink")，又稱為**新武路溪**，位於[臺東縣北部](../Page/臺東縣.md "wikilink")，為[卑南溪主流的上游](../Page/卑南溪.md "wikilink")。其流域包含[海端鄉的中部及北部](../Page/海端鄉.md "wikilink")、[池上鄉西北端及](../Page/池上鄉.md "wikilink")[關山鎮東北端](../Page/關山鎮_\(台灣\).md "wikilink")。該溪是以位在上游的[大崙溪為主流](../Page/大崙溪.md "wikilink")，發源於[中央山脈標高](../Page/中央山脈.md "wikilink")3,293公尺的[卑南主山東側](../Page/卑南主山.md "wikilink")，朝東北東方向流經新溪頭、拔六頭，於[南橫公路](../Page/南橫公路.md "wikilink")[新武橋與](../Page/新武橋.md "wikilink")[霧鹿溪匯集](../Page/霧鹿溪.md "wikilink")，轉向東南流經[初來](../Page/初來.md "wikilink")、[新興](../Page/新興村_\(池上鄉\).md "wikilink")，至池上鄉[萬安村附近才改稱](../Page/萬安村_\(池上鄉\).md "wikilink")「卑南溪」\[1\]。

[Sinwulyu_River.jpg](https://zh.wikipedia.org/wiki/File:Sinwulyu_River.jpg "fig:Sinwulyu_River.jpg")

## 水系主要河川

  - **新武呂溪**
      - [霧鹿溪](../Page/霧鹿溪.md "wikilink")
          - [利稻溪](../Page/利稻溪.md "wikilink")
          - [馬里蘭溪](../Page/馬里蘭溪.md "wikilink")
              - [哈里博松溪](../Page/哈里博松溪.md "wikilink")
              - [馬斯博爾溪](../Page/馬斯博爾溪.md "wikilink")
      - [大崙溪](../Page/大崙溪.md "wikilink")

## 主要橋樑

[Chulai_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Chulai_Bridge.jpg "fig:Chulai_Bridge.jpg")[初來橋](../Page/初來橋.md "wikilink")（2010.10.11攝）\]\]
以下由[河口至](../Page/河口.md "wikilink")[源頭列出](../Page/源頭.md "wikilink")[主流上之主要](../Page/主流.md "wikilink")[橋樑](../Page/橋樑.md "wikilink")：

#### 新武呂溪河段

  - [臺鐵](../Page/臺鐵.md "wikilink")[新武呂溪橋](../Page/新武呂溪橋.md "wikilink")（[臺鐵](../Page/臺鐵.md "wikilink")[臺東線](../Page/臺東線.md "wikilink")）
  - [初來橋](../Page/初來橋.md "wikilink")（省道[台20甲線](../Page/台20甲線.md "wikilink")）

#### 大崙溪河段

  - [新武橋](../Page/新武橋.md "wikilink")（省道[台20線](../Page/台20線.md "wikilink")）

#### 馬里蘭溪河段

  - [霧鹿橋](../Page/霧鹿橋.md "wikilink")（省道[台20線](../Page/台20線.md "wikilink")）

## 相關條目

  - [卑南溪](../Page/卑南溪.md "wikilink")
  - [南橫公路](../Page/南橫公路.md "wikilink")
  - [新武路峽](../Page/新武路峽.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")

## \-{注}-釋

## 參考文獻

[Category:台東縣河川](../Category/台東縣河川.md "wikilink")
[Category:卑南溪水系](../Category/卑南溪水系.md "wikilink")

1.