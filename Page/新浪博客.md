**新浪博客**（官方又称其为：**新浪BLOG**，英文：**Sina
Blog**）为[新浪网旗下的](../Page/新浪网.md "wikilink")[博客网站](../Page/博客.md "wikilink")，于2005年正式上线。因为新浪博客的“明星政策”，因此此网站成为了大陆比较流行的博客网站之一。

## 关于

新浪博客于2005年正式上线，其有的明星政策即招聘多名明星组成特殊的博客团体以吸引访问量。

## 博客门事件

[Bo_ke_men.jpg](https://zh.wikipedia.org/wiki/File:Bo_ke_men.jpg "fig:Bo_ke_men.jpg")
由于2008年5月新浪博客系统升级，出现了一点“小故障”：系统根据评论的[IP地址](../Page/IP地址.md "wikilink")，如果与博主的IP地址相同，原本是匿名的评论会显示出博主，这样一来，原本博主匿名发表的评论就不再匿名。\[1\]众多网友因此发现，很多明星曾在自己的博客中，匿名以各种身份发表评论支持自己。事后，也有网友称这是新浪博客的炒作，并附上分析。\[2\]

### 举例

歌星[金莎在写完一篇博文后](../Page/金莎.md "wikilink")，立刻以不同的身份匿名写了多条留言猛夸自己：\[3\]

  - “私底下你好像个小男生好可爱”
  - “好喜欢你！！我们全班都爱你！”
  - “金莎写的文章好有feel，美女还充满才气”

### 回应

事后，新浪博客修复了这一故障，有些明星删除相关评论，有些明星写文道歉或解释。\[4\]

  - 金莎在博客上道歉，\[5\]称这样的行为“很寒很猥琐”。\[6\]

<!-- end list -->

  - 演员[周杰在博客中写文解释](../Page/周杰.md "wikilink")，称他的博客前期由工作人员管理，且匿名留言是工作人员所为，自己不在乎博客人气。\[7\]

### 涉及明星

既有[张彤](../Page/张彤_\(演员\).md "wikilink")、[黄明](../Page/黄明.md "wikilink")、[李菲儿等不出名或](../Page/李菲儿.md "wikilink")“半红不紫”的演员，也有[金巧巧](../Page/金巧巧.md "wikilink")、金莎、[马天宇](../Page/马天宇.md "wikilink")、周杰等小有名气的。

## 參考來源

<div class="references-2column">

<references/>

</div>

## 链接

  - [新浪博客](http://blog.sina.com.cn)
  - [新浪网站](http://www.sina.com)
  - [新浪网](../Page/新浪网.md "wikilink")
  - [Mysinablog](../Page/Mysinablog.md "wikilink")

[Category:網誌](../Category/網誌.md "wikilink")
[Category:新浪](../Category/新浪.md "wikilink")
[Category:2005年建立的网站](../Category/2005年建立的网站.md "wikilink")

1.
2.
3.
4.
5.
6.
7.