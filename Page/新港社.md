[Gospel_of_St._Matthew_in_Formosan.jpg](https://zh.wikipedia.org/wiki/File:Gospel_of_St._Matthew_in_Formosan.jpg "fig:Gospel_of_St._Matthew_in_Formosan.jpg")，1650年左右\]\]

**新港社**（），西拉雅語稱為
**Tagloeloe**\[1\]，為17世紀[台灣原住民](../Page/台灣原住民.md "wikilink")[西拉雅族的四大](../Page/西拉雅族.md "wikilink")[社](../Page/社.md "wikilink")（新港社、蕭壟社、目加溜灣社、蔴荳社）之一，位於今[台南市](../Page/台南市.md "wikilink")[新市區一帶](../Page/新市區_\(台灣\).md "wikilink")，該區的「社內里」即因屬新港社內之主要聚落所在而名。其為台灣最早接受[西方文明的區域](../Page/西方文明.md "wikilink")，在1636年，當時[殖民統治台灣南部的](../Page/臺灣荷蘭統治時期.md "wikilink")[荷蘭人在新港社興建教堂](../Page/荷蘭人.md "wikilink")[佈教](../Page/宣教.md "wikilink")，並以[羅馬字](../Page/羅馬字.md "wikilink")[拼寫其使用的語言](../Page/羅馬化.md "wikilink")[新港語](../Page/新港語.md "wikilink")，稱之為「[新港文書](../Page/新港文書.md "wikilink")」。1636年5月26日，第一所學校在新港社建立；迄今新市耆老仍有荷蘭人設「文書館」之說。

## 參考文獻

<references />

## 相關條目

  - [理加](../Page/理加.md "wikilink")：新港社長老，荷蘭時代前往[日本](../Page/日本.md "wikilink")，晉見幕府將軍[德川家光](../Page/德川家光.md "wikilink")。
  - [南科新港堂](../Page/南科新港堂.md "wikilink")：據說所在地即荷蘭時代設在新港社的文書館，而該寺廟的樓下即是新港社地方文化館。
  - [新港社大道](../Page/新港社大道.md "wikilink")：以新港社命名的道路，位於[南科臺南園區](../Page/南部科學工業園區#台南園區.md "wikilink")。

## 外部連結

  - [臺灣歷史辭典：新港社](http://nrch.cca.gov.tw/ccahome/website/site20/contents/013/cca220003-li-wpkbhisdict003247-0955-u.xml)
  - [殖民背影下的宣教-十七世紀荷蘭改革宗教會的宣教師與西拉雅族：建立本土化教會](https://web.archive.org/web/20091214210425/http://ianthro.tw/p/101)
    (荷蘭人建立學校資料來源)
  - [國立台南生活美學館：尋找失落的新港社](http://www.tncsec.gov.tw/c_cultures/index2.php?c99=%E5%8E%9F%E4%BD%8F%E6%B0%91%E9%A2%A8%E6%83%85&c03=59#3)

[Category:西拉雅族](../Category/西拉雅族.md "wikilink")
[Category:台南市歷史](../Category/台南市歷史.md "wikilink")
[Category:台灣原住民歷史](../Category/台灣原住民歷史.md "wikilink")
[Category:新市區 (臺灣)](../Category/新市區_\(臺灣\).md "wikilink")
[Category:西拉雅族部落](../Category/西拉雅族部落.md "wikilink")
[Category:源自閩南語的台灣地名](../Category/源自閩南語的台灣地名.md "wikilink")

1.