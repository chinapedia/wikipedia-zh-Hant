**新源县（[維吾爾語](../Page/維吾爾語.md "wikilink")： كۈنەس ناھىيىسى / Künes
Nahiyisi）**是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[伊犁哈萨克自治州所辖的一个](../Page/伊犁哈萨克自治州.md "wikilink")[县](../Page/县.md "wikilink")。

## 历史沿革

[民国二十八年](../Page/民国.md "wikilink")（1939年），伊犁行政公署长姚雄经呈报新疆省政府批准，将巩乃斯（Künes）游牧区从巩留县划出，设立恰克满设治局；民国三十一年（1943年）更名为新源设治局，意即「新开拓之原野」\[1\]；1945年改巩乃斯（Künes）县，1946年10月改新源县。

## 行政区划

下辖8个[镇](../Page/行政建制镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 参考资料

## 外部链接

  - [新源县政府网站](http://www.xinyuan.gov.cn/)

[新源县](../Category/新源县.md "wikilink")
[县](../Category/伊犁州直管县市.md "wikilink")
[伊犁](../Category/新疆县份.md "wikilink")

1.