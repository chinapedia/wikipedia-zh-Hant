或稱是一個位於[日本](../Page/日本.md "wikilink")[新潟市的多功能體育場](../Page/新潟市.md "wikilink")。體育場可以容納42,300人。現時，體育場作為[日本職業足球聯賽球隊](../Page/日本職業足球聯賽.md "wikilink")[新潟天鵝的主場](../Page/新潟天鵝.md "wikilink")。

## 欖球

**新潟球場**亦可以進行欖球比賽。體育場不時舉行[聯合式橄欖球比賽](../Page/聯合式橄欖球.md "wikilink")。

## 2002年世界盃

**新潟球場**於[2002年世界盃足球賽舉行了三場賽事](../Page/2002年世界盃足球賽.md "wikilink")。

小組賽

  - 2002年6月1日 [愛爾蘭](../Page/愛爾蘭國家足球隊.md "wikilink") 1 - 1
    [喀麥隆](../Page/喀麥隆國家足球隊.md "wikilink")（本場是日本賽區的第一場比賽）
  - 2002年6月3日 [克羅地亞](../Page/克羅地亞國家足球隊.md "wikilink") 0 - 1
    [墨西哥](../Page/墨西哥國家足球隊.md "wikilink")

十六強

  - 2002年6月15日 [丹麥](../Page/丹麥國家足球隊.md "wikilink") 0 - 3
    [英格蘭](../Page/英格蘭國家足球隊.md "wikilink")

## 參考資料

## 外部連結

  - [World
    Stadiums](http://www.worldstadiums.com/stadium_pictures/asia/japan/chubu/niigata_stadium.shtml)

  - [官方網頁](http://www.denka-bigswan.com/)

[Category:中央區 (新潟市)](../Category/中央區_\(新潟市\).md "wikilink")
[Category:日本足球場](../Category/日本足球場.md "wikilink")
[Category:日本田徑場](../Category/日本田徑場.md "wikilink")
[Category:橄欖球場](../Category/橄欖球場.md "wikilink")
[Category:2002年世界盃足球場
(日本)](../Category/2002年世界盃足球場_\(日本\).md "wikilink")
[Category:國際田聯一級田徑場](../Category/國際田聯一級田徑場.md "wikilink")
[Category:2001年完工體育場館](../Category/2001年完工體育場館.md "wikilink")