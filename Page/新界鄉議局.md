**新界鄉議局**（官方稱為**鄉議局**，英文：**Heung Yee
Kuk**）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")700條[原居民鄉村的諮詢及協商的](../Page/新界原居民.md "wikilink")[法定組織](../Page/香港法定機構.md "wikilink")，成立於1926年。現時分為三約及二十七鄉鄉事委員會。

該局的職能，受到《[香港法例](../Page/香港法例.md "wikilink")》第1097章《[鄉議局條例](../Page/鄉議局條例.md "wikilink")》\[1\]及[香港基本法第](../Page/香港基本法.md "wikilink")40條\[2\]所確立。

## 主要職責

  - 促進新界居民之間的合作及了解；
  - 作為香港政府與新界居民的溝通橋樑；
  - 就新界的社會及經濟發展向香港政府提供意見；
  - 鼓勵遵守新界居民的傳統風俗和習慣；
  - 執行[香港行政長官邀請的職能](../Page/香港行政長官.md "wikilink")。

## 歷史

鄉議局的前身為「新界農工商業研究總會」，由[荃灣](../Page/荃灣.md "wikilink")[鄉紳](../Page/鄉紳.md "wikilink")[楊國瑞](../Page/楊國瑞.md "wikilink")、[粉嶺鄉紳](../Page/粉嶺.md "wikilink")[李仲莊及](../Page/李仲莊.md "wikilink")[元朗鄉紳](../Page/元朗.md "wikilink")[鄧偉棠於](../Page/鄧偉棠.md "wikilink")1923年發起組織，對政府擬推行之建屋補價政策，提出反對，最終迫使政府終止有關政策。

1926年，當時[香港總督](../Page/香港總督.md "wikilink")[金文泰為改善](../Page/金文泰.md "wikilink")[政府與原居民的關係](../Page/香港政府.md "wikilink")，飭令「新界農工商業研究總會」改組為鄉議局，並賦予更多權力，成立初期連一般[民事案件也會交由鄉議局辦理](../Page/民事.md "wikilink")。1959年，[香港法例第](../Page/香港法例.md "wikilink")1097章《鄉議局條例》實施，鄉議局也成為[香港法定機構之一](../Page/香港法定機構.md "wikilink")，並與當時的[新界民政署保持密切聯繫](../Page/新界民政署.md "wikilink")。

雖然隨後[區議會及](../Page/香港區議會.md "wikilink")[區域市政局先後成立](../Page/香港區域市政局.md "wikilink")，作為地區性事務的諮詢及管理議會機構，但鄉議局至今仍然有一定的地位，並分別於香港區議會及[香港立法會擁有當然議席及](../Page/香港立法會.md "wikilink")[功能界別議席](../Page/功能界別.md "wikilink")，參見[香港選舉及](../Page/香港選舉.md "wikilink")[鄉議局功能界別條目](../Page/鄉議局功能界別.md "wikilink")。

## 局徽

[HK_HeungYeeKukBuilding_FrontFacade.JPG](https://zh.wikipedia.org/wiki/File:HK_HeungYeeKukBuilding_FrontFacade.JPG "fig:HK_HeungYeeKukBuilding_FrontFacade.JPG")
新界鄉議局是在第十六屆第十八次常務會議從多個草圖經過商討後，決定採用現時的局徽設計，但因配色意見不一，延至第十七屆第二次常務會議才通過設計，一直沿用至今。

局徽最外的圓圈表示「新界居民之團結象徵，新界鄉議局是新界民意的高機構」，圓圈內上方有弧形中文[隸書局名](../Page/隸書.md "wikilink")「新界鄉議局」，下方亦有弧形英文局名「HEUNG
YEE KUK
N.T.」，上方有三個互相重疊成倒「品」字形的小圓圈，右上方小圓圈代表[大埔區](../Page/鄉議局大埔區.md "wikilink")，圈內是該區地形，貫穿其中的是[九廣鐵路](../Page/九廣鐵路.md "wikilink")；左上方的小圓圈代表[元朗區](../Page/鄉議局元朗區.md "wikilink")，圈內是該區地形，[青山公路穿越全區](../Page/青山公路.md "wikilink")\[3\]；下方小圓圈代表[南約區](../Page/鄉議局南約區.md "wikilink")，圈內是該區地形，除[島嶼外](../Page/香港島嶼.md "wikilink")，是一片[海洋](../Page/海洋.md "wikilink")，三圈相連「代表新界之三大區之緊密團結，互相依存之象徵」；三個小圓圈之下是繫以紅色綢帶的金色對稱[稻穗](../Page/稻.md "wikilink")，代表新界社會是以[農業經濟為主體](../Page/香港農業.md "wikilink")。

## 局址

[HeungYeeKuk.jpg](https://zh.wikipedia.org/wiki/File:HeungYeeKuk.jpg "fig:HeungYeeKuk.jpg")
[HK_HeungYeeKukNewTerritoriesBuilding.JPG](https://zh.wikipedia.org/wiki/File:HK_HeungYeeKukNewTerritoriesBuilding.JPG "fig:HK_HeungYeeKukNewTerritoriesBuilding.JPG")

新界鄉議局前身農工商會於1926年5月3日以9,000港元向商人俞宏波購入[大埔墟祟德街地皮](../Page/大埔墟.md "wikilink")，面積1,980平方呎，並於1926年建成第一代局址，樓高兩層，部份用作祟德高初小學。

直到1970年代，新界鄉議局不斷擴大後地方不敷應用，遂於[九龍](../Page/九龍.md "wikilink")[油麻地北海街增設辦公室以處理日常行政事務](../Page/油麻地.md "wikilink")，但不少特別委員會的會議需要到酒樓開會，於第廿一屆成立「籌建大會堂委員會」（其後更名「籌建大廈委員會」），並於1974年7月10日的會員大會通過在[界限街以北](../Page/界限街.md "wikilink")、[沙田隧道以南興建新局址的決議](../Page/獅子山隧道.md "wikilink")。1975年5月委員會選定[九龍塘](../Page/九龍塘.md "wikilink")[金巴倫道面積](../Page/金巴倫道.md "wikilink")12,975[平方呎地皮作為新局址](../Page/平方呎.md "wikilink")，為大埔舊局址的六倍，地價連建築費合共200萬港元，由新界鄉議局全資購入，並獲[新界民政署資助](../Page/新界民政署.md "wikilink")5萬元。建築工程分兩期進行，首期用了6個月將地皮上的原有建築物改裝為辦公及會議大樓，並加建門樓；第二期工程於次年完成，在辦公及會議大樓後方新建一座可容300人的大禮堂。

經運作達30年後金巴倫道局址亦逐漸不敷應用，2004年新界鄉議局提議於沙田石門市地段547段（沙田[城門河畔](../Page/城門河.md "wikilink")[小瀝源安睦街地段](../Page/小瀝源.md "wikilink")）興建新局址，[行政長官會同行政會議於](../Page/行政長官會同行政會議.md "wikilink")2005年11月1日通過私人協約方式（Private
Treaty
Grant），以象徵式1,000港元地價撥出估值約3億元面積約38,167[平方呎的原商貿用地皮](../Page/平方呎.md "wikilink")，用地為期50年，每年收取應課[差餉租值的](../Page/差餉.md "wikilink")3%作為[地租](../Page/地租.md "wikilink")。鄉議局成立籌建新局址專責小組，新局址採用南中國鄉僑大宅作為設計模式，以「維護傳統，走向未來」為主題，樓高三層，樓面面積超過10萬[平方呎](../Page/平方呎.md "wikilink")，內部將有粵劇館、多功能禮堂、展覽館、圖書館、活動室和新界文物館等設施，弘揚華夏文化藝術，維繫新界氏族文化精神。於2006年11月16日舉行動土典禮，2009年1月16日舉行新廈興工及奠基儀式，2010年8月5日舉行上樑典禮，2011年4月2日[新界鄉議局大樓舉行入](../Page/新界鄉議局大樓.md "wikilink")-{伙}-儀式。

新界鄉議局大樓整項建築工程費原本預計約8,300萬港元，但因材料價格上升及提升設計質素後，有關工程超支至1億多元，加上一場金融海嘯，一度令新界鄉議局資金緊絀，曾考慮出售金巴倫道局址，籌措資金興建新大樓。新界鄉議局於2005年2月開始進行進行本地籌款工作，動員1,500名村代表連續四年，每季每人捐出1,000元村代表金資助新大樓的建造費，又四-{出}-舉辦籌款活動，城中富豪、鄉親、團體等大力支持，另得[香港賽馬會透過其慈善信託基金撥款近](../Page/香港賽馬會.md "wikilink")4,200萬港元資助，才使財政壓力大減。新界鄉議局並成立「新局廈籌建委員會海外籌款委員會」，於2005年6月組織訪歐團，向散居[英國](../Page/英國.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[德國各地的](../Page/德國.md "wikilink")[新界原居民籌集資金](../Page/新界原居民.md "wikilink")，翌年又邀請「[嗚芝聲劇團](../Page/嗚芝聲劇團.md "wikilink")」在[倫敦](../Page/倫敦.md "wikilink")、[曼徹斯特和](../Page/曼徹斯特.md "wikilink")[格拉斯哥三地巡迴演出](../Page/格拉斯哥.md "wikilink")，並再派出代表團訪歐。經過多方努力，才籌募足夠資金完成新大樓的建築工程。

## 組成

鄉議局由屬下的27個地區[鄉事委員會所組成](../Page/鄉事委員會.md "wikilink")。每個鄉事委員會屬下有多條鄉村，而鄉委會主席由村代表選舉產生。27個鄉委會由分別屬於三約：[大埔約](../Page/大埔區_\(鄉議局\).md "wikilink")、[元朗約及](../Page/元朗區_\(鄉議局\).md "wikilink")[南約](../Page/南約.md "wikilink")。

現時鄉議局主席是劉業強（屯門區），兩位副主席分別是[張學明](../Page/張學明.md "wikilink")（大埔區）[林偉強](../Page/林偉強.md "wikilink")（南約區）及，三人於2015年改選均在沒有競爭對手下自動當選。

<table style="width:90%;">
<caption>style="font-size: larger;"| <strong>二十七鄉鄉事委員會</strong><br />
（2019年4月至2023年3月）</caption>
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th><p>鄉議局<br />
分區</p></th>
<th><p>行政區別</p></th>
<th><p>鄉 別</p></th>
<th><p>主 席</p></th>
<th><p>首副主席</p></th>
<th><p>副主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鄉議局元朗區.md" title="wikilink">元朗約</a></p></td>
<td><p><strong><a href="../Page/屯門區.md" title="wikilink">屯門區</a></strong></p></td>
<td><p><a href="../Page/屯門.md" title="wikilink">屯門</a></p></td>
<td><p><a href="../Page/劉業強.md" title="wikilink">劉業強</a>[4]</p></td>
<td><p><a href="../Page/陶錫源.md" title="wikilink">陶錫源</a></p></td>
<td><p><a href="../Page/曾展雄.md" title="wikilink">曾展雄</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/元朗區.md" title="wikilink">元朗區</a></strong></p></td>
<td><p><a href="../Page/廈村.md" title="wikilink">廈村</a></p></td>
<td><p><a href="../Page/鄧勵東.md" title="wikilink">鄧勵東</a></p></td>
<td><p><a href="../Page/陳植良.md" title="wikilink">陳植良</a></p></td>
<td><p><a href="../Page/鄧廉光.md" title="wikilink">鄧廉光</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/屏山_(香港).md" title="wikilink">屏山</a></p></td>
<td><p><a href="../Page/鄧志強.md" title="wikilink">鄧志強</a></p></td>
<td><p><a href="../Page/林權.md" title="wikilink">林權</a></p></td>
<td><p><a href="../Page/鄧同發.md" title="wikilink">鄧同發</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十八鄉.md" title="wikilink">十八鄉</a></p></td>
<td><p><a href="../Page/程振明.md" title="wikilink">程振明</a></p></td>
<td><p><a href="../Page/林照權.md" title="wikilink">林照權</a></p></td>
<td><p><a href="../Page/梁智峯.md" title="wikilink">梁智峯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八鄉.md" title="wikilink">八鄉</a></p></td>
<td><p><a href="../Page/鄧瑞民.md" title="wikilink">鄧瑞民</a></p></td>
<td><p><a href="../Page/郭永昌.md" title="wikilink">郭永昌</a></p></td>
<td><p><a href="../Page/黎永添.md" title="wikilink">黎永添</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/錦田.md" title="wikilink">錦田</a></p></td>
<td><p><a href="../Page/鄧賀年.md" title="wikilink">鄧賀年</a></p></td>
<td><p><a href="../Page/鄧乾德.md" title="wikilink">鄧乾德</a></p></td>
<td><p><a href="../Page/鄧水倫.md" title="wikilink">鄧水倫</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新田_(香港).md" title="wikilink">新田</a></p></td>
<td><p><a href="../Page/文美桂.md" title="wikilink">文美桂</a></p></td>
<td><p><a href="../Page/黎志超.md" title="wikilink">黎志超</a></p></td>
<td><p><a href="../Page/文貴旗.md" title="wikilink">文貴旗</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄉議局大埔區.md" title="wikilink">大埔約</a></p></td>
<td><p><strong><a href="../Page/沙田區.md" title="wikilink">沙田區</a></strong></p></td>
<td><p><a href="../Page/沙田鄉事委員會.md" title="wikilink">沙田</a></p></td>
<td><p><a href="../Page/莫錦貴.md" title="wikilink">莫錦貴</a></p></td>
<td><p><a href="../Page/李志麒.md" title="wikilink">李志麒</a></p></td>
<td><p><a href="../Page/張子賢.md" title="wikilink">張子賢</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/大埔區.md" title="wikilink">大埔區</a></strong></p></td>
<td><p><a href="../Page/西貢北.md" title="wikilink">西貢北約</a></p></td>
<td><p><a href="../Page/李耀斌.md" title="wikilink">李耀斌</a></p></td>
<td><p><a href="../Page/林啤.md" title="wikilink">林啤</a></p></td>
<td><p><a href="../Page/何志超.md" title="wikilink">何志超</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大埔_(香港).md" title="wikilink">大埔</a></p></td>
<td><p><a href="../Page/林奕權.md" title="wikilink">林奕權</a></p></td>
<td><p><a href="../Page/陳笑權.md" title="wikilink">陳笑權</a></p></td>
<td><p><a href="../Page/張國棟_(香港).md" title="wikilink">張國棟</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/北區_(香港).md" title="wikilink">北區</a></strong></p></td>
<td><p><a href="../Page/粉嶺.md" title="wikilink">粉嶺</a></p></td>
<td><p><a href="../Page/李國鳳.md" title="wikilink">李國鳳</a></p></td>
<td><p><a href="../Page/劉永安.md" title="wikilink">劉永安</a></p></td>
<td><p><a href="../Page/李廣明.md" title="wikilink">李廣明</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上水.md" title="wikilink">上水</a></p></td>
<td><p><a href="../Page/侯志強.md" title="wikilink">侯志強</a></p></td>
<td><p><a href="../Page/侯福達.md" title="wikilink">侯福達</a></p></td>
<td><p><a href="../Page/廖駿駒.md" title="wikilink">廖駿駒</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/打鼓嶺.md" title="wikilink">打鼓嶺</a></p></td>
<td><p><a href="../Page/陳月明.md" title="wikilink">陳月明</a></p></td>
<td><p><a href="../Page/林金貴.md" title="wikilink">林金貴</a></p></td>
<td><p><a href="../Page/陳富鵬.md" title="wikilink">陳富鵬</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙頭角.md" title="wikilink">沙頭角</a></p></td>
<td><p><a href="../Page/李冠洪.md" title="wikilink">李冠洪</a></p></td>
<td><p><a href="../Page/李炳華.md" title="wikilink">李炳華</a></p></td>
<td><p><a href="../Page/曾玉安.md" title="wikilink">曾玉安</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄉議局南約區.md" title="wikilink">南約</a></p></td>
<td><p><strong><a href="../Page/西貢區.md" title="wikilink">西貢區</a></strong></p></td>
<td><p><a href="../Page/西貢.md" title="wikilink">西貢</a></p></td>
<td><p><a href="../Page/王水生.md" title="wikilink">王水生</a></p></td>
<td><p><a href="../Page/張土勝.md" title="wikilink">張土勝</a></p></td>
<td><p><a href="../Page/劉球_(香港).md" title="wikilink">劉球</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/坑口鄉事委員會.md" title="wikilink">坑口</a></p></td>
<td><p><a href="../Page/劉啟康.md" title="wikilink">劉啟康</a></p></td>
<td><p><a href="../Page/成向文.md" title="wikilink">成向文</a></p></td>
<td><p><a href="../Page/劉運明.md" title="wikilink">劉運明</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/葵青區.md" title="wikilink">葵青區</a></strong></p></td>
<td><p><a href="../Page/青衣島.md" title="wikilink">青衣</a></p></td>
<td><p><a href="../Page/陳志榮_(香港).md" title="wikilink">陳志榮</a></p></td>
<td><p><a href="../Page/陳碧文.md" title="wikilink">陳碧文</a></p></td>
<td><p><a href="../Page/鄧育財.md" title="wikilink">鄧育財</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/荃灣區.md" title="wikilink">荃灣區</a></strong></p></td>
<td><p><a href="../Page/荃灣.md" title="wikilink">荃灣</a></p></td>
<td><p><a href="../Page/邱錦平.md" title="wikilink">邱錦平</a></p></td>
<td><p><a href="../Page/傅振光.md" title="wikilink">傅振光</a></p></td>
<td><p><a href="../Page/孫華安.md" title="wikilink">孫華安</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬灣.md" title="wikilink">馬灣</a></p></td>
<td><p><a href="../Page/陳崇業.md" title="wikilink">陳崇業</a></p></td>
<td><p><a href="../Page/吳炳坤.md" title="wikilink">吳炳坤</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/離島區.md" title="wikilink">離島區</a></strong></p></td>
<td><p><a href="../Page/長洲_(香港).md" title="wikilink">長洲</a></p></td>
<td><p><a href="../Page/翁志明.md" title="wikilink">翁志明</a></p></td>
<td><p><a href="../Page/孔憲禮.md" title="wikilink">孔憲禮</a></p></td>
<td><p><a href="../Page/陳超傑.md" title="wikilink">陳超傑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/坪洲.md" title="wikilink">坪洲</a></p></td>
<td><p><a href="../Page/黃漢權.md" title="wikilink">黃漢權</a></p></td>
<td><p><a href="../Page/李文安_(香港).md" title="wikilink">李文安</a></p></td>
<td><p><a href="../Page/曾紀洲.md" title="wikilink">曾紀洲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東涌.md" title="wikilink">東涌</a></p></td>
<td><p><a href="../Page/黃秋萍.md" title="wikilink">黃秋萍</a></p></td>
<td><p><a href="../Page/莫廣源.md" title="wikilink">莫廣源</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大澳.md" title="wikilink">大澳</a></p></td>
<td><p><a href="../Page/何紹基_(香港).md" title="wikilink">何紹基</a></p></td>
<td><p><a href="../Page/蘇光_(香港).md" title="wikilink">蘇光</a></p></td>
<td><p><a href="../Page/黃容根.md" title="wikilink">黃容根</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梅窩.md" title="wikilink">梅窩</a></p></td>
<td><p><a href="../Page/黃文漢.md" title="wikilink">黃文漢</a></p></td>
<td><p><a href="../Page/鄒長福.md" title="wikilink">鄒長福</a></p></td>
<td><p><a href="../Page/李國強_(梅窩).md" title="wikilink">李國強</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大嶼山.md" title="wikilink">大嶼南</a></p></td>
<td><p><a href="../Page/何進輝.md" title="wikilink">何進輝</a></p></td>
<td><p><a href="../Page/張蓮壽.md" title="wikilink">張蓮壽</a></p></td>
<td><p><a href="../Page/何德輝.md" title="wikilink">何德輝</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南丫島.md" title="wikilink">南丫南</a></p></td>
<td><p><a href="../Page/周玉堂.md" title="wikilink">周玉堂</a></p></td>
<td><p><a href="../Page/胡國光.md" title="wikilink">胡國光</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南丫島.md" title="wikilink">南丫北</a></p></td>
<td><p><a href="../Page/陳連偉.md" title="wikilink">陳連偉</a></p></td>
<td><p><a href="../Page/陳錦貴.md" title="wikilink">陳錦貴</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<small>資料來源：[2019至2023年度鄉事委員會執行委員會名單](https://www.had.gov.hk/rre/chi/other/2019_27rcs.html)</small>

## 歷任首長

[Lau_Wong-fat_at_Victoria_Park_20100619.jpg](https://zh.wikipedia.org/wiki/File:Lau_Wong-fat_at_Victoria_Park_20100619.jpg "fig:Lau_Wong-fat_at_Victoria_Park_20100619.jpg")在1980年至2015年擔任主席\]\]
鄉議局的首長是鄉議局主席（第9屆及以前稱「局長」），由選舉產生。

| 屆數         | 任期                               | 名稱                                      | 來自                                         | 分區             | 備註                                          |
| ---------- | -------------------------------- | --------------------------------------- | ------------------------------------------ | -------------- | ------------------------------------------- |
| 第一屆        | 1926-1928年                       | [李仲莊](../Page/李仲莊.md "wikilink")        | [粉嶺](../Page/粉嶺.md "wikilink")\[5\]        | 大埔區            | 首任局長                                        |
| 第二屆        | 1928-1930年                       | [彭樂三](../Page/彭樂三.md "wikilink")        | [粉嶺](../Page/粉嶺.md "wikilink")             | 大埔區            |                                             |
| 第三屆        | 1930-1932年                       |                                         |                                            |                |                                             |
| 第四屆        | 1932-1934年                       | [鄧煒堂](../Page/鄧煒堂.md "wikilink")        | [錦田](../Page/錦田.md "wikilink")             | 元朗區            |                                             |
| 第五屆        | 1934-1936年                       | [彭樂三](../Page/彭樂三.md "wikilink")        | [粉嶺](../Page/粉嶺.md "wikilink")             | 大埔區            |                                             |
| 第六屆        | 1936-1938年                       | [鄧勳臣](../Page/鄧勳臣.md "wikilink")        | [大埔](../Page/大埔_\(香港\).md "wikilink")      | 大埔區            |                                             |
| 第七屆        | 1938-1940年                       |                                         |                                            |                |                                             |
| 第八屆        | 1940-1941年                       | [李渭鎏](../Page/李渭鎏.md "wikilink")        | [八鄉](../Page/八鄉.md "wikilink")             | 元朗區            |                                             |
| 第二次世界大戰    |                                  |                                         |                                            |                |                                             |
| 第九屆        | 1947年                            | [彭樂三](../Page/彭樂三.md "wikilink")        | [粉嶺](../Page/粉嶺.md "wikilink")             | 大埔區            | 復局後首任局長，同年病逝                                |
| 第十屆        | 1947-1952年                       | [陳永安](../Page/陳永安_\(香港\).md "wikilink") | [荃灣](../Page/荃灣.md "wikilink")             | 南約區            | 以副局長身份接任至1952年                              |
| 第十一屆       | 1952-1954年                       | [黃炳英](../Page/黃炳英.md "wikilink")        | [上水](../Page/上水.md "wikilink")             | 大埔區            |                                             |
| 第十二屆       | 1954-1955年                       | 1955年12月病逝                              |                                            |                |                                             |
| 1955-1957年 | [何傳耀](../Page/何傳耀.md "wikilink") | [荃灣](../Page/荃灣.md "wikilink")          | 南約區                                        | 以副主席身份暫代至1957年 |                                             |
| 第十三屆       | 1957-1959年                       | [鄧乾新](../Page/鄧乾新.md "wikilink")        | [錦田](../Page/錦田.md "wikilink")             | 元朗區            | 鄉議局分裂，身份不獲政府承認，於1959年辭職                     |
| 第十四屆       | 1960-1962年                       | [何傳耀](../Page/何傳耀.md "wikilink")        | [荃灣](../Page/荃灣.md "wikilink")             | 南約區            |                                             |
| 第十五屆       | 1962-1964年                       | [陳日新](../Page/陳日新_\(香港\).md "wikilink") | [屯門](../Page/屯門.md "wikilink")             | 元朗區            |                                             |
| 第十六屆       | 1964-1966年                       | [張人龍](../Page/張人龍.md "wikilink")        | [上水](../Page/上水.md "wikilink")             | 大埔區            |                                             |
| 第十七屆       | 1966-1968年                       | [彭富華](../Page/彭富華.md "wikilink")        | [粉嶺](../Page/粉嶺.md "wikilink")             | 大埔區            |                                             |
| 第十八屆       | 1968-1970年                       | [陳日新](../Page/陳日新_\(香港\).md "wikilink") | [屯門](../Page/屯門.md "wikilink")             | 元朗區            |                                             |
| 第十九屆       | 1970-1972年                       |                                         |                                            |                |                                             |
| 第二十屆       | 1972-1974年                       |                                         |                                            |                |                                             |
| 第二十一屆      | 1974-1976年                       |                                         |                                            |                |                                             |
| 第二十二屆      | 1976-1978年                       |                                         |                                            |                |                                             |
| 第二十三屆      | 1978-1980年                       | [黃源章](../Page/黃源章.md "wikilink")        | [大埔](../Page/大埔_\(香港\).md "wikilink")\[6\] | 大埔區            |                                             |
| 第二十四屆      | 1980-1982年                       | [劉皇發](../Page/劉皇發.md "wikilink")        | [屯門](../Page/屯門.md "wikilink")             | 元朗區            |                                             |
| 第二十五屆      | 1982-1985年                       | 由本屆起任期改為3年\[7\]                         |                                            |                |                                             |
| 第二十六屆      | 1985-1988年                       |                                         |                                            |                |                                             |
| 第二十七屆      | 1988-1991年                       |                                         |                                            |                |                                             |
| 第二十八屆      | 1991-1995年                       | 由本屆起任期改為4年\[8\]                         |                                            |                |                                             |
| 第二十九屆      | 1995-1999年                       |                                         |                                            |                |                                             |
| 第三十屆       | 1999-2003年                       |                                         |                                            |                |                                             |
| 第三十一屆      | 2003-2007年                       |                                         |                                            |                |                                             |
| 第三十二屆      | 2007-2011年                       |                                         |                                            |                |                                             |
| 第三十三屆      | 2011-2015年                       |                                         |                                            |                |                                             |
| 第三十四屆      | 2015-2019年                       | [劉業強](../Page/劉業強.md "wikilink")        | [屯門](../Page/屯門.md "wikilink")             | 元朗區            | 為24-33屆主席[劉皇發長子](../Page/劉皇發.md "wikilink") |
|            |                                  |                                         |                                            |                |                                             |

## 教育

1961年鄉議局之執行委員會會議通過設立鄉議局籌建三區中學委員會。經與政府商討後，以鄉議局選舉區為單位，於[元朗](../Page/元朗區_\(鄉議局\).md "wikilink")、[大埔](../Page/大埔區_\(鄉議局\).md "wikilink")、[南約三區各建一所中學](../Page/南約區.md "wikilink")。當時每所中學之建築費，預算約為港幣200萬元。建校委員會承擔每所百分之三十建築費。1968年[新界鄉議局元朗區中學校舍開幕](../Page/新界鄉議局元朗區中學.md "wikilink")；1982年9月[新界鄉議局南約區中學落成啟用](../Page/新界鄉議局南約區中學.md "wikilink")；1984年5月[港督](../Page/港督.md "wikilink")[尤德爵士為](../Page/尤德.md "wikilink")[新界鄉議局大埔區中學主持開幕禮](../Page/新界鄉議局大埔區中學.md "wikilink")。

[File:HK_NTHYK_YuenLongDistrictSecondarySchool.JPG|新界鄉議局元朗區中學](File:HK_NTHYK_YuenLongDistrictSecondarySchool.JPG%7C新界鄉議局元朗區中學)
[File:HK_NTHYK_SouthernDistrictSecondarySchool.JPG|新界鄉議局南約區中學](File:HK_NTHYK_SouthernDistrictSecondarySchool.JPG%7C新界鄉議局南約區中學)
<File:N.T.H.Y.K>. Tai Po District Secondary School.jpg|新界鄉議局大埔區中學

## 参考文献

《新界鄉議局史：由租借地到一國兩制》，[薛鳳旋](../Page/薛鳳旋.md "wikilink")、[鄺智文](../Page/鄺智文.md "wikilink")
著，三聯書店，ISBN 978-962-04-3065-7

## 參考資料

## 外部連結

  - [鄉議局](http://www.hyknt.org/)
  - [鄉議局在新界都市化所扮演的角色](https://web.archive.org/web/20070915105437/http://hkila.org.hk/news7.htm)

[鄉議局](../Category/鄉議局.md "wikilink")
[Category:香港辦學團體](../Category/香港辦學團體.md "wikilink")

1.  [鄉議局條例（PDF格式）](http://www.heungyeekuk.org/003a.pdf)

2.  [香港基本法第40條](http://www.heungyeekuk.org/004.htm)

3.  按《新界鄉議局史》第106頁稱為[屯門公路](../Page/屯門公路.md "wikilink")，但於設計局徽時的1960年代尚未建成，相信為手民之誤。

4.  劉皇發於2016年2月22日起辭任，劉業強於2016年5月30日自動當選為屯門鄉委會主席

5.  為[沙頭角](../Page/沙頭角.md "wikilink")[南涌李屋人](../Page/南涌.md "wikilink")，其後定居[粉嶺](../Page/粉嶺.md "wikilink")[軍地](../Page/軍地.md "wikilink")

6.  按《新界鄉議局史》第112頁稱「來自地區：不詳」，黃源章曾任大埔鄉事委員會主席，故補回。

7.

8.