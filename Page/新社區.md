**新社區**（[臺灣客家語](../Page/臺灣客家語.md "wikilink")[大埔腔](../Page/大埔腔.md "wikilink")：sin+
sha+
ki+），是[中華民國](../Page/中華民國.md "wikilink")[臺中市的市轄區](../Page/臺中市.md "wikilink")，位於臺中市中部偏東，[臺中盆地之外圍台地平頂](../Page/臺中盆地.md "wikilink")。氣候屬[亞熱帶氣候](../Page/亞熱帶.md "wikilink")，由於群山環繞，多地形雨。特產有[椪柑](../Page/椪柑.md "wikilink")、[葡萄](../Page/葡萄.md "wikilink")、[楊桃](../Page/楊桃.md "wikilink")、高接[梨](../Page/梨.md "wikilink")、[枇杷](../Page/枇杷.md "wikilink")、[香菇](../Page/香菇.md "wikilink")、[盆景等種植](../Page/盆景.md "wikilink")。

## 歷史

本區在漢人未入墾之前，為[平埔族原住民聚居之地](../Page/平埔族.md "wikilink")，屬[噶哈巫族](../Page/噶哈巫族.md "wikilink")「**樸仔籬**」社群居住地。據記載，[清](../Page/清.md "wikilink")[道光十三年](../Page/道光.md "wikilink")（1833年），有[粵人杜行修者](../Page/粵.md "wikilink")，率領五十壯男經大墩、三櫃城、大坑（皆[臺中市境內](../Page/臺中市.md "wikilink")），到達本區，為漢人到達新社之始，由於當時遷移以粵人居多，目前新社區為臺中市第二大客家聚落，僅次[東勢區](../Page/東勢區.md "wikilink")。而原住民於[嘉慶年間](../Page/嘉慶.md "wikilink")，內遷到新社轄區各地後，不久即有漢人進入與他們做買賣，並稱該地原住民聚落為「**新番社**」（簡稱「新社」），此為新社地名的起源。

清[道光末年](../Page/道光.md "wikilink")，彰化地區[戴萬生等人響應](../Page/戴萬生.md "wikilink")「[太平天國](../Page/太平天國.md "wikilink")」起義之[民變](../Page/戴潮春事件.md "wikilink")，在臺發動革命，全臺陷入一片混亂之中，沿海居民則紛紛入山避難，大舉遷入本鄉墾殖，與原住民劃地而居，新社因而逐漸發展。

民變平息之後，[彰化縣府派兵到東勢角設治](../Page/彰化縣.md "wikilink")，新社為[栜東上堡](../Page/栜東上堡.md "wikilink")**新社區**，為新社治理之始。[日本治臺後](../Page/台灣日治時期.md "wikilink")，新社改為[臺中廳東勢角支署新社區](../Page/臺中廳.md "wikilink")；[大正](../Page/大正.md "wikilink")9年（1920年）臺灣地方制度改變，改為[臺中州](../Page/臺中州.md "wikilink")[東勢郡](../Page/東勢郡.md "wikilink")**[新社-{庄}-](../Page/新社庄.md "wikilink")**。

1945年[中華民國政府接收日本](../Page/中華民國政府.md "wikilink")[臺灣總督府轄區之初](../Page/臺灣總督府.md "wikilink")，[臺中縣東勢區](../Page/臺中縣.md "wikilink")「**新社鄉**」；2010年12月25日[臺中縣市合併後](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，改為臺中市「**新社區**」。（並非指新的社區）

## 地理

新社區位於[臺中市中部偏東](../Page/臺中市.md "wikilink")，輪廓略呈扭曲之西北－東南走向長條形。東北部隔[大甲溪與](../Page/大甲溪.md "wikilink")[東勢區相望](../Page/東勢區.md "wikilink")，東與[和平區為鄰](../Page/和平區_\(台灣\).md "wikilink")，南與[臺灣省](../Page/臺灣省.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[國姓鄉為界](../Page/國姓鄉.md "wikilink")，西南與[太平區](../Page/太平區_\(臺灣\).md "wikilink")、[北屯區連接](../Page/北屯區.md "wikilink")，西為[豐原區](../Page/豐原區.md "wikilink")，北為[石岡區](../Page/石岡區.md "wikilink")。全區之中心點位於東經120.4度，北緯24.2度處，面積68.89平方公里。在全市區當中，面積排名第五位\[1\]。

新社區境內佔多數地形是屬於河階台地面，為古代[大甲溪受到](../Page/大甲溪.md "wikilink")[蓬萊運動影響](../Page/蓬萊運動.md "wikilink")，造成河道數次更動，因此形成數階的台地面，共有十三座河階，是為「[新社河階群](../Page/新社河階群.md "wikilink")」。大南、水底寮、下水底寮、七分、十分及永居湖等河階位於本區轄內，水井子、南眉及仙塘坪等河階則與鄰區相交界，其餘的河階已不在本區轄內。

## 行政

[新社區公所.JPG](https://zh.wikipedia.org/wiki/File:新社區公所.JPG "fig:新社區公所.JPG")
新社區公所隸屬於[臺中市政府](../Page/臺中市政府.md "wikilink")，官派[區長一人綜理區務](../Page/區長.md "wikilink")，區長下設主任秘書一人。下設秘書室、民政課、社會課、農建課、人文課、人事室、會計室及政風室等八課室。

### 歷任首長

末任鄉長：李光銘 首任區長: 徐照山 第二任區長:劉孟富 第三任區長:林淑惠

### 行政區劃

  - [大南里](../Page/大南里.md "wikilink")、[東興里](../Page/東興里_\(臺中市新社區\).md "wikilink")、[慶西里](../Page/慶西里.md "wikilink")、[月湖-{里}-](../Page/月湖里.md "wikilink")、[協成里](../Page/協成里.md "wikilink")、[中正里](../Page/中正里_\(臺中市新社區\).md "wikilink")、[崑山-{里}-](../Page/崑山里.md "wikilink")
  - [中和里](../Page/中和里_\(臺中市新社區\).md "wikilink")、[復盛里](../Page/復盛里.md "wikilink")、[中興里](../Page/中興里_\(臺中市新社區\).md "wikilink")、[新社-{里}-](../Page/新社里.md "wikilink")、[永源里](../Page/永源里.md "wikilink")、[福興里](../Page/福興里_\(臺中市新社區\).md "wikilink")

## 人口

## 教育

## 交通

### 公路

  - [TW_PHW21.svg](https://zh.wikipedia.org/wiki/File:TW_PHW21.svg "fig:TW_PHW21.svg")[台21線](../Page/台21線.md "wikilink")
  - [TW_CHW129.svg](https://zh.wikipedia.org/wiki/File:TW_CHW129.svg "fig:TW_CHW129.svg")[市道129號](../Page/市道129號.md "wikilink")：石岡區
    - 新社區 - 北屯區
  - [新社景點地圖](https://web.archive.org/web/20071022074624/http://www.shinshe.org.tw/main/index.php?map)

### 台中市公車

<table>
<thead>
<tr class="header">
<th><p>路線號碼</p></th>
<th><p>營運區間</p></th>
<th><p>經營業者</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車21路.md" title="wikilink"><font color="red">21延2</font></a></strong></p></td>
<td><p><a href="../Page/臺中刑務所演武場.md" title="wikilink">臺中刑務所演武場</a>－三貴城－中興嶺</p></td>
<td><p><a href="../Page/仁友客運.md" title="wikilink">仁友客運</a></p></td>
<td><p>部分班次延駛至中興嶺</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車91路.md" title="wikilink"><font color="red">91</font></a></strong></p></td>
<td><p>舊庄－中興嶺停車場</p></td>
<td><p><a href="../Page/豐原客運.md" title="wikilink">豐原客運</a></p></td>
<td><p>所有班次繞駛至豐原東站<br />
部分班次延駛至<a href="../Page/臺中航空站.md" title="wikilink">臺中航空站</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車263路.md" title="wikilink"><font color="red">263</font></a></strong></p></td>
<td><p>東勢－崑山</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車264路.md" title="wikilink"><font color="red">264</font></a></strong></p></td>
<td><p>東勢－東興－新五村</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車265路.md" title="wikilink"><font color="red">265</font></a></strong></p></td>
<td><p>東勢－大南－新五村</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車266路.md" title="wikilink"><font color="red">266</font></a></strong></p></td>
<td><p>東勢－谷關</p></td>
<td><p>豐原客運</p></td>
<td><p>經龍安、豐埔產業道路</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車266路.md" title="wikilink"><font color="red">266繞</font></a></strong></p></td>
<td><p>東勢－松鶴部落－谷關</p></td>
<td><p>豐原客運</p></td>
<td><p>經龍安、豐埔產業道路、松鶴部落</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車270路.md" title="wikilink"><font color="red">270</font></a></strong></p></td>
<td><p>豐富公園－大南－東勢</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車271路.md" title="wikilink"><font color="red">271</font></a></strong></p></td>
<td><p>豐富公園－東興－東勢</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車272路.md" title="wikilink"><font color="red">272</font></a></strong></p></td>
<td><p>東勢－茄苳寮</p></td>
<td><p>豐原客運</p></td>
<td><p>東勢端17:25班次繞駛水井</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車273路.md" title="wikilink"><font color="red">273</font></a></strong></p></td>
<td><p>新五村－茄苳寮</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車275路.md" title="wikilink"><font color="red">275</font></a></strong></p></td>
<td><p>東勢-中和</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車275路.md" title="wikilink"><font color="purple">275區</font></a></strong></p></td>
<td><p>郵局-中和</p></td>
<td><p>豐原客運</p></td>
<td><p>例假日及寒暑假停駛</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車276路.md" title="wikilink"><font color="red">276</font></a></strong></p></td>
<td><p>豐富公園－興中山莊－東勢</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車277路.md" title="wikilink"><font color="red">277</font></a></strong></p></td>
<td><p>豐富公園－復盛里－東勢</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車278路.md" title="wikilink"><font color="red">278</font></a></strong></p></td>
<td><p>東勢－水井</p></td>
<td><p>豐原客運</p></td>
<td><p>經新社、大南<br />
例假日及寒暑假停駛</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車279路.md" title="wikilink"><font color="red">279</font></a></strong></p></td>
<td><p>新社－水井</p></td>
<td><p>豐原客運</p></td>
<td><p>經大南<br />
例假日及寒暑假停駛</p></td>
</tr>
</tbody>
</table>

## 旅遊

[殉職山崗先生之碑.JPG](https://zh.wikipedia.org/wiki/File:殉職山崗先生之碑.JPG "fig:殉職山崗先生之碑.JPG")
[新社菇類種植.JPG](https://zh.wikipedia.org/wiki/File:新社菇類種植.JPG "fig:新社菇類種植.JPG")

  - [新社花季](../Page/新社花海.md "wikilink")：是[種苗改良繁殖場苗圃](../Page/種苗改良繁殖場.md "wikilink")，原意是施以綠肥之用，卻無心插柳成了新社景點。
  - 殉職[山岡榮之碑](../Page/山岡榮.md "wikilink")
  - 新社魅力商圈（新社香菇街）
  - 千年古樟神木
  - [白冷圳](../Page/白冷圳.md "wikilink")
  - [種苗改良繁殖場](../Page/種苗改良繁殖場.md "wikilink")
  - 七分窯
  - 枇杷產業文化館
  - 新社-{[九庄媽](../Page/九庄媽.md "wikilink")}-遶境
  - [新社聖普宮](../Page/新社聖普宮.md "wikilink")：位於協中街159號，大坑5-1步道登山口旁。
  - 新社莊園

## 參考資料

## 外部連結

  - [新社區公所](https://web.archive.org/web/20110323050104/http://www.xinshe.taichung.gov.tw/index.asp)
  - [微笑台灣：常要溜回去的地方
    新社的美麗，要深入走進去](https://smiletaiwan.cw.com.tw/article/1057)

[山線](../Category/臺中市行政區劃.md "wikilink")
[新社區](../Category/新社區.md "wikilink")
[中](../Category/臺灣客家文化重點發展區.md "wikilink")

1.  [《地理位置》，臺中市新社區公所](http://www.shinshou.gov.tw/1-2.php)