[Hsinchu_Science_and_Industrial_Park_Administration_20101017.jpg](https://zh.wikipedia.org/wiki/File:Hsinchu_Science_and_Industrial_Park_Administration_20101017.jpg "fig:Hsinchu_Science_and_Industrial_Park_Administration_20101017.jpg")

**新竹科學工業園區**（簡稱**新竹科學園區**、**竹科**）是[臺灣的第一座](../Page/臺灣.md "wikilink")[科學園區](../Page/科學園區.md "wikilink")，涵蓋範圍橫跨[新竹市](../Page/新竹市.md "wikilink")[東區與](../Page/東區_\(新竹市\).md "wikilink")[新竹縣](../Page/新竹縣.md "wikilink")[寶山鄉](../Page/寶山鄉_\(台灣\).md "wikilink")，園區內廠商以經營[電子代工服務為主](../Page/電子代工服務.md "wikilink")，全球[高科技代工產業的主要科技重鎮之一](../Page/高科技.md "wikilink")。

成立至今有400家以上高科技代工業、服務業廠商進駐，主要產業包括有[半導體業](../Page/半導體.md "wikilink")、[電腦業](../Page/電腦.md "wikilink")、[通訊業](../Page/通訊.md "wikilink")、[光電業](../Page/光電.md "wikilink")、精密機械業與[生物技術業](../Page/生物技術.md "wikilink")，是全球半導體製造業最密集的地方之一，目前已開發新竹園區約632公頃與竹南園區約141公頃，約有12萬人在園區工作。經過多年開發，新竹科學園區逐漸成為北台灣的科技產業中心，並且按國家發展計畫擴大基地，目前擴充計畫包括桃園龍潭園區、苗栗銅鑼園區、新竹生物醫學園區以及宜蘭園區。亦由於其成功經驗，中華民國政府陸續在台灣其他地區設立[中科及](../Page/中科.md "wikilink")[南科](../Page/南科.md "wikilink")。但這些產業伴隨著高污染的副產品，因此半導體製造業皆為環保及健康問題，為歐美國家所不能接受，\[1\]\[2\]\[3\]\[4\]因此法規及執法寬鬆的台灣成為最適合的設廠地點\[5\]，也是世界半導體代工製造的重鎮，且以竹科為首，創造了不少經濟及工作機會，如[台積電等知名公司皆在此設置據點](../Page/台積電.md "wikilink")。

## 歷史

[Light_decor_SBIP_Hsinchu_Taiwan.JPG](https://zh.wikipedia.org/wiki/File:Light_decor_SBIP_Hsinchu_Taiwan.JPG "fig:Light_decor_SBIP_Hsinchu_Taiwan.JPG")
新竹科學園區透過[工研院技術引進](../Page/工研院.md "wikilink")、人才培育、衍生公司、育成中心、技術服務與技術移轉等不同方式協助業者，對台灣產業發展歷程具有舉足輕重的地位。

  - 1977年，[工研院建立台灣首座](../Page/工研院.md "wikilink")4吋晶圓的積體電路示範工廠

<!-- end list -->

  - 1979年1月10日，[新竹市市區東郊](../Page/新竹市.md "wikilink")[金山面擴建動土成立以電子代工為核心的專業工業區](../Page/金山面.md "wikilink")——新竹科學工業園區；於1980年12月15日完工\[6\]，並同時成立「[行政院國家科學委員會科學工業園區管理局](../Page/行政院國家科學委員會科學工業園區管理局.md "wikilink")」，擴建後首任局長為[何宜慈](../Page/何宜慈.md "wikilink")。

1986年，啟用[污水處理廠](../Page/污水處理廠.md "wikilink")\[7\]以解決產生的廢水污染，另外環保局也持續在園區周圍檢測空氣。

1990年位於桃園市龍潭區的[龍潭園區](../Page/新竹科學園區龍潭園區.md "wikilink")，1999年7月，位於[新竹市南邊](../Page/新竹市.md "wikilink")[竹南鎮的](../Page/竹南鎮.md "wikilink")[竹南園區動工](../Page/新竹科學園區竹南園區.md "wikilink")。2003年，於[新竹市北邊成立](../Page/新竹市.md "wikilink")[新竹生物醫學園區](../Page/新竹科學園區竹北園區.md "wikilink")。2005年5月16日，開始規劃設置位於宜蘭市的[宜蘭園區](../Page/新竹科學園區宜蘭園區.md "wikilink")。2007年2月8日，位於苗栗縣[銅鑼鄉的](../Page/銅鑼鄉_\(台灣\).md "wikilink")[銅鑼園區動工](../Page/新竹科學園區銅鑼園區.md "wikilink")。

## 基地

[Hsinchu_Science-Park.jpg](https://zh.wikipedia.org/wiki/File:Hsinchu_Science-Park.jpg "fig:Hsinchu_Science-Park.jpg")
新竹科学工业园区共有六個園區，總開發面積1,342公頃。

### 新竹科學園區

主要園區，距離[新竹市市區約](../Page/新竹市.md "wikilink")5分鐘車程，公路交通以[中山高速公路為主](../Page/中山高速公路.md "wikilink")，距台北市70公里，到[桃園國際機場車程約](../Page/臺灣桃園國際機場.md "wikilink")65分鐘，至[新竹港需](../Page/新竹港.md "wikilink")20分鐘車程、往北至[基隆港](../Page/基隆港.md "wikilink")、往南到[台中港分別約需](../Page/臺中港.md "wikilink")2小時車程，另亦可經由北部第二高速公路自台北經新竹系統交流道轉[中山高速公路北上直接進入](../Page/中山高速公路.md "wikilink")[科學園區](../Page/科學園區.md "wikilink")，共650公頃。

### 竹南科學園區

竹南園區位於[苗栗縣](../Page/苗栗縣.md "wikilink")[竹南鎮頂埔里](../Page/竹南鎮.md "wikilink")，民國八十六年七月奉行政院核定為科學園區用地，於民國八十八年七月開始動工，土地面積159公頃以支援新竹科學園區發展，並已陸續引進生物科技、通訊、光電等高科技產業。竹科四期竹南基地在41家廠商（投資額1,366億元、員工近10,839人）進駐之後飽和，進行四期擴編計劃，總開發面積165.72公頃，先從竹南鎮大埔里公義路以東136公頃著手辦理都市計畫變更。\[8\]

### 龍潭科學園區

龍潭科學園區現有「台積電」、「美國蘋果電腦」、「友達光電」、「明基材料」、「輔祥實業」、「晶元光電」、「合晶科技」、「同欣電子」、「艾克爾國際科技」、「台灣日鑛金屬」及「聯亞科技」營運中。未來本園區將建構為光電及太陽能上、中、下游產業創新聚落，帶動地方產業轉型，增進就業機會，繁榮地方經濟並吸引高科技人才至龍潭科學園區服務。

### 銅鑼科學園區

[Tongluo_Science_Park.jpg](https://zh.wikipedia.org/wiki/File:Tongluo_Science_Park.jpg "fig:Tongluo_Science_Park.jpg")
國科會副主委周景揚說明，2010年初在電池廠賸玖科技公司起租後，銅鑼園區的土地出租率已達百分之百。

2017年4月5日自由時報 -
新竹科學園區管理局銅鑼科學園區，於九十九年起開始有企業進駐，但園區基地景象仍顯冷清，加上園區開發工程持續，更讓地方不解，憂心園區土地閒置。

### 宜蘭科學園區

宜蘭科學園區位於宜蘭市中山路以西、縣政特區以北、[宜蘭運動公園以南](../Page/宜蘭運動公園.md "wikilink")，園區土地在[日本時代曾經是](../Page/台灣日治時期.md "wikilink")[宜蘭機場](../Page/宜蘭機場.md "wikilink")，目前現況有十三家廠商\[9\]。

目前有[宇正精密科技](../Page/宇正精密科技.md "wikilink")、[佩兒國際股份有限公司宜蘭分公司](../Page/佩兒國際.md "wikilink")、[海納微加工等十三家公司](../Page/海納微加工.md "wikilink")

### 新竹生物醫學園區

[新竹生物醫學園區.jpg](https://zh.wikipedia.org/wiki/File:新竹生物醫學園區.jpg "fig:新竹生物醫學園區.jpg")
[新竹生物醫學園區位於](../Page/新竹生物醫學園區.md "wikilink")[新竹竹北市](../Page/新竹.md "wikilink")，目前有39間核准入區的廠商。

## 進駐廠商

[Hsinchu_Science_Park,_30th_anniversary_and_ROC_100th_anniversary_decoration_20110223.jpg](https://zh.wikipedia.org/wiki/File:Hsinchu_Science_Park,_30th_anniversary_and_ROC_100th_anniversary_decoration_20110223.jpg "fig:Hsinchu_Science_Park,_30th_anniversary_and_ROC_100th_anniversary_decoration_20110223.jpg")
[TSMC_Fab5.JPG](https://zh.wikipedia.org/wiki/File:TSMC_Fab5.JPG "fig:TSMC_Fab5.JPG")的晶圓五廠\]\]
[友達光電_(竹科力行路).jpg](https://zh.wikipedia.org/wiki/File:友達光電_\(竹科力行路\).jpg "fig:友達光電_(竹科力行路).jpg")的廠房\]\]

<table>
<tbody>
<tr class="odd">
<td><p><span style="font-size:12pt;"> 半導體業</p></td>
<td><p><a href="../Page/台積電.md" title="wikilink">台積電</a><br />
<strong>TSMC</strong></p></td>
<td><p><a href="../Page/聯華電子.md" title="wikilink">聯華電子</a><br />
<strong>UMC</strong></p></td>
<td><p><a href="../Page/力晶科技.md" title="wikilink">力晶科技</a><br />
<strong>Powerchip</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/旺宏電子.md" title="wikilink">旺宏電子</a><br />
<strong>MXIC</strong></p></td>
<td><p><a href="../Page/新唐科技.md" title="wikilink">新唐科技</a><br />
<strong>Nuvoton</strong></p></td>
<td><p><a href="../Page/矽統科技.md" title="wikilink">矽統科技</a><br />
<strong>SIS</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聯發科.md" title="wikilink">聯發科</a><br />
<strong>MediaTek</strong></p></td>
<td><p><a href="../Page/瑞昱半導體.md" title="wikilink">瑞昱半導體</a><br />
<strong>Realtek</strong></p></td>
<td><p><a href="../Page/頎邦科技.md" title="wikilink">頎邦科技</a><br />
<strong>Chipbond</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凌陽創新.md" title="wikilink">凌陽創新</a><br />
<strong>Sunplus</strong></p></td>
<td><p><a href="../Page/聯陽半導體.md" title="wikilink">聯陽半導體</a><br />
ITE Tech.</p></td>
<td><p><a href="../Page/合勤科技.md" title="wikilink">合勤科技</a><br />
ZyXEL</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/漢磊科技.md" title="wikilink">漢磊科技</a><br />
EPISIL</p></td>
<td><p><a href="../Page/台灣茂矽電子.md" title="wikilink">台灣茂矽電子</a><br />
MOSEL</p></td>
<td><p><a href="../Page/矽成積體電路.md" title="wikilink">矽成積體電路</a><br />
ISSI</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界先進積體電路.md" title="wikilink">世界先進積體電路</a><br />
VIS</p></td>
<td><p><a href="../Page/茂德科技.md" title="wikilink">茂德科技</a><br />
ProMOS</p></td>
<td><p><a href="../Page/聯詠科技.md" title="wikilink">聯詠科技</a><br />
NOVATEK</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南茂科技.md" title="wikilink">南茂科技</a><br />
ChipMOS</p></td>
<td><p><a href="../Page/創意電子.md" title="wikilink">創意電子</a><br />
UNICHIP</p></td>
<td><p><a href="../Page/威盛電子.md" title="wikilink">威盛電子</a><br />
VIA</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/智原科技.md" title="wikilink">智原科技</a><br />
faraday</p></td>
<td><p><a href="../Page/鈺創科技.md" title="wikilink">鈺創科技</a><br />
Etron</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="font-size:12pt;"> 光電業</p></td>
<td><p><a href="../Page/友達光電.md" title="wikilink">友達光電</a><br />
<strong>AUO</strong></p></td>
<td><p><a href="../Page/中強光電.md" title="wikilink">中強光電</a><br />
<strong>Coretronic</strong></p></td>
<td><p><a href="../Page/鼎元光電.md" title="wikilink">鼎元光電</a><br />
<strong>tyntek</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虹光精密.md" title="wikilink">虹光精密</a><br />
<strong>Avision</strong></p></td>
<td><p><a href="../Page/晶元光電.md" title="wikilink">晶元光電</a><br />
<strong>EPISTAR</strong></p></td>
<td><p><a href="../Page/新日光能源科技.md" title="wikilink">新日光能源科技</a><br />
<strong>NeoSolarPower</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台積固態照明.md" title="wikilink">台積固態照明</a><br />
<strong>TSMC Solid State Lighting</strong></p></td>
<td><p><a href="../Page/新世紀光電.md" title="wikilink">新世紀光電</a><br />
<strong>GENESIS photonics</strong></p></td>
<td><p><a href="../Page/隆達電子.md" title="wikilink">隆達電子</a><br />
<strong>Lextar</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="font-size:12pt;"> 電腦資訊及軟體產業</p></td>
<td><p><a href="../Page/全景軟體.md" title="wikilink">全景軟體</a><br />
<strong>ChangingTec</strong></p></td>
<td><p><a href="../Page/緯創資通.md" title="wikilink">緯創資通</a><br />
<strong>wistron</strong></p></td>
<td><p><a href="../Page/佳世達.md" title="wikilink">佳世達</a><br />
<strong>Qisda</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東訊.md" title="wikilink">東訊</a><br />
<strong>Tecom</strong></p></td>
<td><p><a href="../Page/核心智識.md" title="wikilink">核心智識</a><br />
<strong>Coretech</strong></p></td>
<td><p><a href="../Page/敏盛科技.md" title="wikilink">敏盛科技</a><br />
<strong>Teams</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

## 園區管理局

依«科技部新竹科學工業園區管理局組織法»設

  - 局長
  - 副局長
  - 主任秘書

## 相關條目

  - [矽谷毒物聯盟](../Page/矽谷毒物聯盟.md "wikilink")
  - [台灣科學園區列表](../Page/台灣科學園區列表.md "wikilink")
  - [內湖科技園區](../Page/內湖科技園區.md "wikilink")
  - [南港軟體園區](../Page/南港軟體園區.md "wikilink")
  - [新竹工業區](../Page/新竹工業區.md "wikilink")
  - [中部科學工業園區](../Page/中部科學工業園區.md "wikilink")
  - [南部科學工業園區](../Page/南部科學工業園區.md "wikilink")
  - [科技部](../Page/科技部.md "wikilink")
  - [國立科學工業園區實驗高級中學](../Page/國立科學工業園區實驗高級中學.md "wikilink")：設立以提供園區人員子女教育特殊機會的幼稚園至高中學校。
  - [科技部新竹科學園區公共自行車租賃系統](../Page/科技部新竹科學園區公共自行車租賃系統.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [新竹科學園區](https://www.sipa.gov.tw)

[H](../Category/桃園市經濟.md "wikilink")
[H](../Category/新竹市經濟.md "wikilink")
[H](../Category/新竹縣經濟.md "wikilink")
[H](../Category/苗栗縣經濟.md "wikilink")
[H](../Category/宜蘭縣經濟.md "wikilink")
[H](../Category/東區_\(新竹市\).md "wikilink")
[H](../Category/寶山鄉_\(台灣\).md "wikilink")
[D](../Category/中華民國科技部.md "wikilink")
[H](../Category/1980年台灣建立.md "wikilink")
[H](../Category/1980年完工建築物.md "wikilink")
[H](../Category/台灣科技園區.md "wikilink")

1.
2.
3.
4.
5.
6.  [金山面社區營造工作之我思](http://www.cpmah.org.tw/2001/c90046/www/community/booklet18.htm)

7.
8.  [苗栗縣政府中文版 苗栗縣政府](http://idipc.miaoli.gov.tw/energy_3_6.html)
9.  [園區廠商](http://www.sipa.gov.tw/home.jsp?serno=201001210043&mserno=201001210037&menudata=ChineseMenu&contlink=ap/introduction_6_5.jsp&level3=Y&serno3=201002010043)