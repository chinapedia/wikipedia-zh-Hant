**新竹縣**（；）是[中華民國](../Page/中華民國.md "wikilink")[臺灣省的一個](../Page/臺灣省.md "wikilink")[縣](../Page/縣_\(中華民國\).md "wikilink")，位於[臺灣本島](../Page/臺灣.md "wikilink")[西北部](../Page/北臺灣.md "wikilink")，北臨[桃園市](../Page/桃園市.md "wikilink")，南接[苗栗縣](../Page/苗栗縣.md "wikilink")，東南以[雪山山脈與](../Page/雪山山脈.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[臺中市相連](../Page/臺中市.md "wikilink")，西部面向[台灣海峽](../Page/台灣海峽.md "wikilink")，西接與[新竹市交界](../Page/新竹市.md "wikilink")。全縣總面積約1,427平方公里，除[鳳山溪](../Page/鳳山溪.md "wikilink")、[頭前溪中下游沖積平原外](../Page/頭前溪.md "wikilink")，其餘大多為丘陵、台地及山地\[1\]。

早期新竹縣郊區多務農，1970年代[工業技術研究院創設於](../Page/工業技術研究院.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")，1980年代[新竹科學工業園區設立於](../Page/新竹科學工業園區.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")[東區及新竹縣](../Page/東區_\(新竹市\).md "wikilink")[寶山鄉](../Page/寶山鄉.md "wikilink")，1990年代位於[湖口鄉的](../Page/湖口鄉.md "wikilink")[新竹工業區也逐漸從](../Page/新竹工業區.md "wikilink")[傳統產業聚落轉型為新興高科技產業聚落](../Page/傳統產業.md "wikilink")，使得新竹縣成為北台灣的高科技產業重鎮，而人口也在近幾年急速增加。

本縣方言於絕大部分地區使用[海陸客家話](../Page/海陸客家話.md "wikilink")，竹北市及新豐鄉沿海地區部分使用[泉州音](../Page/泉州音.md "wikilink")[閩南話較多](../Page/閩南話.md "wikilink")，關西鎮及峨眉鄉以使用[四縣腔](../Page/四縣腔.md "wikilink")[客家話為主](../Page/客家話.md "wikilink")。

## 歷史

新竹地區古稱「[竹塹](../Page/竹塹.md "wikilink")」，遠在[漢人入墾之前](../Page/漢族.md "wikilink")，即為[原住民](../Page/台灣原住民.md "wikilink")[道卡斯](../Page/道卡斯族.md "wikilink")[平埔族](../Page/平埔族.md "wikilink")、[賽夏族](../Page/賽夏族.md "wikilink")、[泰雅族世代聚居之地](../Page/泰雅族.md "wikilink")\[2\]。

17世紀[荷蘭殖民台灣時](../Page/荷蘭.md "wikilink")，曾對居住竹塹地區海岸平原的平埔原住民進行戶口調查。在1654年的調查中，分布於今香山、鹽水港一帶的道卡斯族[竹塹社計有](../Page/竹塹社.md "wikilink")149戶、523人\[3\]。1661年（[明永曆](../Page/明朝.md "wikilink")16年），[明鄭大軍進攻臺灣](../Page/鄭成功攻台之役.md "wikilink")，鄭成功部將[楊祖率左先鋒陣軍隊於新港仔](../Page/楊祖.md "wikilink")（今[苗栗縣](../Page/苗栗縣.md "wikilink")[後龍鎮](../Page/後龍鎮_\(台灣\).md "wikilink")）、及竹塹一帶屯墾。幾個月後遭原住民攻擊，楊祖負傷退回[赤崁](../Page/赤崁.md "wikilink")。1683年（[清](../Page/清.md "wikilink")[康熙](../Page/康熙.md "wikilink")22年）台灣併入清朝版圖
，竹塹劃歸[諸羅縣管轄](../Page/諸羅縣.md "wikilink")，漢人再度墾殖竹塹地區。1691年福建泉州[王世傑獲准帶領親族](../Page/王世傑_\(金門\).md "wikilink")180餘人和福建銀同同鄉以暗仔街為中心往市區發展，同時向竹塹社族人取得土地墾植，廣興水利開發良田，形成[閩南族群移民聚落](http://gisapsrv0.cpami.gov.tw/cpis/cprpts/hsinchu_city/depart/tab-img/culture/image/3-2-1.gif)
。

1723年（雍正元年）[大甲溪以北析置](../Page/大甲溪.md "wikilink")[淡水廳](../Page/淡水廳.md "wikilink")，廳治即設於[竹塹](../Page/竹塹.md "wikilink")（今[新竹市](../Page/新竹市.md "wikilink")），成為清朝時期北台灣之重鎮。拓墾規模較大者如1725年粵籍徐立鵬帶領族人進入今[竹北市](../Page/竹北市.md "wikilink")、[竹東鎮](../Page/竹東鎮.md "wikilink")、[新豐鄉等地墾殖](../Page/新豐鄉_\(台灣\).md "wikilink")；嘉慶年間，粵籍吳氏會昌公派下族人亦從桃園南崁、中壢、楊梅等地播散至竹北二堡（今[關西鎮](../Page/關西鎮_\(臺灣\).md "wikilink")）展開拓墾，形成[客家移民聚落](../Page/客家.md "wikilink")。

至19世紀後期，西部沿海平原多為閩南籍移民聚居地，中部河谷平原及丘陵地多屬客籍移民與平埔族聚居，而賽夏族、泰雅族則退居於東部山區\[4\]。

1875年（[光緒元年](../Page/光緒.md "wikilink")）淡水廳拆分為[新竹縣](../Page/新竹縣_\(清朝\).md "wikilink")、[淡水縣與](../Page/淡水縣.md "wikilink")[基隆廳](../Page/基隆廳.md "wikilink")，竹塹即於此改稱「新竹」並首度設縣，行政中心設於今新竹市，其範圍包括今[桃園市](../Page/桃園市.md "wikilink")[社子溪以南地區](../Page/社子溪.md "wikilink")、新竹縣市、[苗栗縣及](../Page/苗栗縣.md "wikilink")[臺中市大甲溪以北地區](../Page/臺中市.md "wikilink")。1887年新竹、苗栗分治，新竹縣轄區縮小為今[桃園市](../Page/桃園市.md "wikilink")[社子溪以南地區](../Page/社子溪.md "wikilink")、新竹縣市、[苗栗縣](../Page/苗栗縣.md "wikilink")[中港溪以北地區](../Page/中港溪.md "wikilink")，行政中心設於今新竹市。

1895年（明治28年）[台灣割讓給](../Page/台灣割讓.md "wikilink")[日本後](../Page/日本.md "wikilink")，[初期行政區劃變更頻繁](../Page/臺灣日治時期行政區劃.md "wikilink")。1901年（[明治](../Page/明治.md "wikilink")34年）11月分全台為20廳後始趨於穩定，當時新竹地區設為[新竹廳](../Page/新竹廳.md "wikilink")，下轄五支廳，範圍約為今之新竹縣不含關西鎮，並含中港溪以北的苗栗地區。1909年（明治42年）10月拆[苗栗廳廢廳](../Page/苗栗廳.md "wikilink")，[大安溪以北併入新竹廳](../Page/大安溪.md "wikilink")，以南併入臺中廳；此時新竹廳轄區擴大為今新竹縣市及苗栗縣（不含關西鎮），行政區中心設於新竹區（今[新竹火車站一帶](../Page/新竹火車站.md "wikilink")）。1920年（[大正](../Page/大正.md "wikilink")9年）9月，廢廳置州，合併新竹廳全境、[桃園廳](../Page/桃園廳.md "wikilink")（不含三角湧支廳）為[新竹州](../Page/新竹州.md "wikilink")，下置8郡，州治設於新竹郡[新竹街](../Page/新竹街.md "wikilink")（後升格為[新竹市](../Page/新竹市_\(州轄市\).md "wikilink")），[中正路是城市軸線](../Page/中正路_\(新竹市\).md "wikilink")。

1945年（[民國](../Page/民國.md "wikilink")34年）[二戰結束後](../Page/二戰.md "wikilink")，[中華民國國民政府接收台灣](../Page/中華民國國民政府.md "wikilink")，將新竹州改為新竹縣，轄區不變；原轄郡改為[縣轄區](../Page/縣轄區.md "wikilink")，郡轄街-{庄}-改為區轄鎮跟鄉。同年10月30日劃出[新竹市為省轄市](../Page/新竹市_\(第一次省轄市\).md "wikilink")，原設於新竹市的縣政府，終戰後遷出新竹市，經有關官員及各級民意代表多次研討後，改設於桃園區[桃園鎮](../Page/桃園區.md "wikilink")。1946年（民國35年）下轄[竹東區](../Page/竹東區.md "wikilink")[竹東鎮與](../Page/竹東鎮.md "wikilink")[寶山鄉劃入](../Page/寶山鄉_\(台灣\).md "wikilink")[新竹市](../Page/新竹市_\(第一次省轄市\).md "wikilink")，改制為[竹東區和](../Page/竹東區_\(新竹市\).md "wikilink")[寶山區](../Page/寶山區_\(新竹市\).md "wikilink")2區。1949年3月，大溪區角板鄉（今桃園市復興區）與竹東區尖石、五峰二鄉以及大湖區大安鄉（今苗栗縣泰安鄉）劃出為新峰區管轄。

1950年（民國39年）[臺灣省進行調整行政區劃](../Page/臺灣省.md "wikilink")，新竹縣改劃分為新竹、[桃園](../Page/桃園市.md "wikilink")、[苗栗](../Page/苗栗縣.md "wikilink")3縣，並廢除縣轄區；新竹縣、市合併，新竹市降為新竹縣[縣轄市](../Page/縣轄市.md "wikilink")，並將[竹東](../Page/竹東區.md "wikilink")、[寶山](../Page/寶山鄉_\(台灣\).md "wikilink")、[香山等三區自新竹市析出](../Page/香山區.md "wikilink")，改制為新竹縣[竹東鎮](../Page/竹東鎮.md "wikilink")、[寶山鄉](../Page/寶山鄉_\(台灣\).md "wikilink")、[香山鄉](../Page/香山鄉_\(新竹縣\).md "wikilink")，縣治設於[新竹市](../Page/新竹市_\(縣轄市\).md "wikilink")。

1982年（民國71年）7月[新竹市與新竹縣之香山鄉合併](../Page/新竹市_\(縣轄市\).md "wikilink")，升格為省轄市，香山鄉再一次成為[新竹市香山區](../Page/新竹市.md "wikilink")。設於過去亦曾為[新竹市政府辦公處的](../Page/新竹市.md "wikilink")[原新竹縣政府辦公處](../Page/新竹州廳.md "wikilink")，[新竹縣政府改遷至](../Page/新竹縣政府.md "wikilink")[竹北鄉](../Page/竹北鄉_\(新竹縣\).md "wikilink")，故[竹北鄉成為新竹縣治](../Page/竹北鄉_\(新竹縣\).md "wikilink")，並於1988年（民國77年）10月31日改制為縣轄市[竹北市](../Page/竹北市.md "wikilink")。

## 地理

### 地形

新竹縣的地形以山地、丘陵多而平原少。全縣以東南部與[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[台中市交界一帶之](../Page/臺中市.md "wikilink")[雪山山脈地勢最高](../Page/雪山山脈.md "wikilink")，[海拔多在](../Page/海拔.md "wikilink")3,000尺上下。由此向西北逐次降低，至[竹北市附近已降至](../Page/竹北市.md "wikilink")20至30公尺。平原分布於西部沿海及河谷地帶，中間有廣大的丘陵與台地。

[新竹平原位於本縣西部](../Page/新竹平原.md "wikilink")，為[鳳山溪與](../Page/鳳山溪.md "wikilink")[頭前溪間之沖積平原](../Page/頭前溪.md "wikilink")，範圍包含新竹縣[竹北市幾乎全部](../Page/竹北市.md "wikilink")，[新埔鎮](../Page/新埔鎮.md "wikilink")、[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[芎林鄉](../Page/芎林鄉.md "wikilink")、[竹東鎮](../Page/竹東鎮.md "wikilink")、[橫山鄉的一部分](../Page/橫山鄉_\(臺灣\).md "wikilink")，以及[新竹市的北半部](../Page/新竹市.md "wikilink")。全區地勢平緩，水源充足，為全縣主要的農業生產基地，也是聚落與商業中心所在地。

[湖口台地位於本縣北部](../Page/湖口台地.md "wikilink")，為一略呈東西走向的狹長帶狀台地，範圍包含[新豐鄉](../Page/新豐鄉_\(台灣\).md "wikilink")、[湖口鄉全部](../Page/湖口鄉.md "wikilink")，以及[新埔鎮](../Page/新埔鎮.md "wikilink")、[關西鎮部分地區](../Page/關西鎮_\(台灣\).md "wikilink")。農業以旱作或[茶葉](../Page/茶葉.md "wikilink")、水果等經濟作物為主；工商業則以位於[湖口鄉的](../Page/湖口鄉.md "wikilink")[新竹工業區為重鎮](../Page/新竹工業區.md "wikilink")，除了傳統製造業外，近來隨著[新竹科學工業園區用地漸趨不足](../Page/新竹科學工業園區.md "wikilink")，吸引許多高科技廠商來此設廠。

本縣中部及南部有[飛鳳山丘陵](../Page/飛鳳山丘陵.md "wikilink")、[竹東丘陵](../Page/竹東丘陵.md "wikilink")、[竹南丘陵等廣大丘陵地帶](../Page/竹南丘陵.md "wikilink")，範圍涵蓋新埔鎮、關西鎮、芎林鄉、橫山鄉、竹東鎮、[北埔鄉](../Page/北埔鄉.md "wikilink")、[寶山鄉](../Page/寶山鄉_\(台灣\).md "wikilink")、[峨眉鄉等行政區](../Page/峨眉鄉.md "wikilink")。旱作產物或茶葉、水果等為主要的農業產品。

本縣山地主要分佈於東南部[尖石鄉](../Page/尖石鄉.md "wikilink")、[五峰鄉全境和關西鎮](../Page/五峰鄉_\(台灣\).md "wikilink")、橫山鄉、竹東鎮、峨眉鄉的一部份。其中五峰鄉西南端[爺巴堪溪流域被劃入](../Page/爺巴堪溪.md "wikilink")[雪霸國家公園](../Page/雪霸國家公園.md "wikilink")，蘊含豐富的自然景觀資源\[5\]。

### 水文

新竹縣擁有[頭前溪](../Page/頭前溪.md "wikilink")、[鳳山溪兩條](../Page/鳳山溪.md "wikilink")[中央管河川和](../Page/中央管河川.md "wikilink")[縣市管河川](../Page/縣市管河川.md "wikilink")[新豐溪](../Page/新豐溪.md "wikilink")。另外本縣東南部為北台灣第一大河[淡水河的源流區](../Page/淡水河.md "wikilink")。

頭前溪為新竹地區最主要的河川，主流河長63.03公里，流域面積565.94平方公里，分佈於[新竹市北部及新竹縣中部的](../Page/新竹市.md "wikilink")[五峰鄉](../Page/五峰鄉_\(台灣\).md "wikilink")、[橫山鄉](../Page/橫山鄉_\(臺灣\).md "wikilink")、[尖石鄉](../Page/尖石鄉.md "wikilink")、[芎林鄉](../Page/芎林鄉.md "wikilink")、[竹北市等鄉鎮市](../Page/竹北市.md "wikilink")。主流上源為[霞喀羅溪](../Page/霞喀羅溪.md "wikilink")，發源於[雪山山脈標高](../Page/雪山山脈.md "wikilink")2,512公尺之[檜山西北側](../Page/檜山_\(新竹苗栗\).md "wikilink")\[6\]，中流稱為[上坪溪](../Page/上坪溪.md "wikilink")，流至下公館附近與另一支流[油羅溪會合後](../Page/油羅溪.md "wikilink")，始稱頭前溪，於[南寮注入](../Page/南寮.md "wikilink")[台灣海峽](../Page/台灣海峽.md "wikilink")。頭前溪的落差大，平均坡降比約1:190，在夏季常造成山洪，如新竹五峰鄉、尖石鄉在颱風季節，都被列為[土石流高度警介區](../Page/土石流.md "wikilink")。頭前溪流經知名的[新竹科學工業園區](../Page/新竹科學工業園區.md "wikilink")，由於工業區用水量大，但頭前溪屬短促型的急流，枯水期時流域內用水常遇緊迫，因此建有並有[上坪攔河堰](../Page/上坪攔河堰.md "wikilink")、[隆恩圳攔河堰](../Page/隆恩堰.md "wikilink")、燥樹排攔河堰等水利工程，以及[寶山水庫及](../Page/寶山水庫.md "wikilink")[寶二水庫](../Page/寶二水庫.md "wikilink")。

鳳山溪是本縣僅次於[頭前溪的重要](../Page/頭前溪.md "wikilink")，河長約45.45公里，流域面積約250.10平方公里，涵蓋新竹縣及[桃園市](../Page/桃園市.md "wikilink")，其中本縣包含[尖石鄉](../Page/尖石鄉.md "wikilink")、[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[新埔鎮](../Page/新埔鎮.md "wikilink")、[橫山鄉](../Page/橫山鄉_\(臺灣\).md "wikilink")、[湖口鄉](../Page/湖口鄉.md "wikilink")、[竹北市等行政區](../Page/竹北市.md "wikilink")。鳳山溪發源自新竹縣[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[尖石鄉與桃園市](../Page/尖石鄉.md "wikilink")[復興區交界處的](../Page/復興區_\(桃園市\).md "wikilink")[鳥嘴山](../Page/外鳥嘴山.md "wikilink")（外鳥嘴山）西側，於崁子腳附近與南邊之頭前溪匯合注入[台灣海峽](../Page/台灣海峽.md "wikilink")。

[馬里闊丸溪又名玉峰溪](../Page/馬里闊丸溪.md "wikilink")，為[淡水河的源流](../Page/淡水河.md "wikilink")，其流域涵蓋本縣[尖石鄉大部分及](../Page/尖石鄉.md "wikilink")[桃園市](../Page/桃園市.md "wikilink")[復興區的一小部分](../Page/復興區_\(桃園市\).md "wikilink")。其主要源流有二，其中[塔克金溪](../Page/塔克金溪.md "wikilink")（又名泰崗溪）為主流，發源於[品田山北側](../Page/品田山.md "wikilink")，先向東北後轉西北流，至[秀巒與另一源流](../Page/秀巒.md "wikilink")[薩克亞金溪](../Page/薩克亞金溪.md "wikilink")（又名白石溪）會合，始稱馬里闊丸溪。馬里闊丸溪續流入桃園市境後，成為[大漢溪及淡水河的直接源流](../Page/大漢溪.md "wikilink")。

[新豐溪位於新豐鄉](../Page/新豐溪.md "wikilink")，河長33.98公里，流域面積94.75平方公里，入海口處分佈有10多公頃的[紅樹林](../Page/紅樹林.md "wikilink")，為一知名紅樹林生態遊憩區。

### 氣候

[Avg_Temperature_of_Hsinchu.png](https://zh.wikipedia.org/wiki/File:Avg_Temperature_of_Hsinchu.png "fig:Avg_Temperature_of_Hsinchu.png")
本縣属于[副热带季风气候](../Page/副热带季风气候.md "wikilink")。全年平均溫度為攝氏25度，最冷月為12月和1月、最熱月則是8月，年平均雨量為1,949.1公釐\[7\]。

於1、2、3月時由於強烈大陸冷氣團的來襲所形成的數波[寒流影響](../Page/寒流.md "wikilink")，因而潛在著[寒害的風險](../Page/寒害.md "wikilink")，常有數天的低溫比全台平地月均溫最低的新北市[淡水區還要低](../Page/淡水區.md "wikilink")1至3度不等。

## 區劃人口

[Hsinchu_labelled_map2.png](https://zh.wikipedia.org/wiki/File:Hsinchu_labelled_map2.png "fig:Hsinchu_labelled_map2.png")
民國三十四年（1945年）臺灣脫離日本統治，[新竹州改制為新竹縣](../Page/新竹州.md "wikilink")，[新竹市改制為省轄市](../Page/新竹市_\(1945年-1950年\).md "wikilink")，縣市分治，縣治遷至[桃園鎮](../Page/桃園區.md "wikilink")，共轄有8[縣轄區](../Page/縣轄區_\(中華民國\).md "wikilink")44個鄉鎮。民國三十五年（1946年），將竹東鎮與寶山鄉劃入新竹市，民國三十九年（1950年）調整行政區域，將[桃園區](../Page/桃園郡.md "wikilink")、[中壢區](../Page/中壢郡.md "wikilink")、[大溪區等](../Page/大溪郡.md "wikilink")4鎮9鄉合併分出[桃園縣](../Page/桃園市.md "wikilink")，[苗栗區](../Page/苗栗區.md "wikilink")、[竹南區](../Page/竹南區.md "wikilink")、[大湖區等](../Page/大湖區.md "wikilink")5鎮13鄉合併分出[苗栗縣](../Page/苗栗縣.md "wikilink")，新竹縣由原省轄新竹市7區改為4區1鎮2鄉，與[新竹區的](../Page/新竹區.md "wikilink")2鎮3鄉、[竹東區的](../Page/竹東區.md "wikilink")6鄉等4區14個鄉鎮組成。民國四十年（1951年）將東、西、南、北四區撤銷，合併組成縣轄[新竹市](../Page/新竹市_\(縣轄市\).md "wikilink")，縣治設於原新竹市；民國七十一年（1982年）七月一日[新竹市與](../Page/新竹市.md "wikilink")[香山鄉合併升格為](../Page/香山區.md "wikilink")[省轄市與該縣再度分治](../Page/省轄市.md "wikilink")，並同時將縣治遷建竹北鄉，轄3鎮、10鄉。民國七十七年（1988年）竹北鄉升格為縣轄市，目前全縣共轄有1[市](../Page/縣轄市.md "wikilink")（竹北市）、3[鎮](../Page/鎮_\(中華民國\).md "wikilink")（竹東鎮、新埔鎮、關西鎮）、9[鄉](../Page/鄉_\(中華民國\).md "wikilink")（湖口鄉、新豐鄉、峨眉鄉、寶山鄉、北埔鄉、芎林鄉、橫山鄉，尖石鄉、五峰鄉為[山地鄉](../Page/山地鄉.md "wikilink")），共有13個鄉鎮市。

  - 以下資料為2019年3月\[8\]，該縣人口約55.6萬人，居民以[海陸腔](../Page/海陸腔.md "wikilink")[客家人為主](../Page/客家人.md "wikilink")，[閩南人](../Page/閩南人.md "wikilink")、[外省人及](../Page/外省人.md "wikilink")[原住民為少數](../Page/臺灣原住民.md "wikilink")，近30年來由於[新竹科學園區的發展](../Page/新竹科學園區.md "wikilink")，也吸引許多外來人口移居\[9\]。

<table>
<thead>
<tr class="header">
<th><p>鄉鎮市名</p></th>
<th><p>面積<br />
<small>（km²）</small></p></th>
<th><p>下轄村<br />
-{里}-數</p></th>
<th><p>下轄鄰數</p></th>
<th><p>人口數</p></th>
<th><p>人口<br />
消長</p></th>
<th><p>人口密度<br />
<small>（人/km²）</small></p></th>
<th><p>郵遞<br />
區號</p></th>
<th><p>傳統地域</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/竹北市.md" title="wikilink">竹北市</a></p></td>
<td><p>46.8341</p></td>
<td><p>31</p></td>
<td><p>721</p></td>
<td><p>188,818</p></td>
<td><p><font color=blue>+530</font></p></td>
<td><p>4,032</p></td>
<td><p>302</p></td>
<td><p>竹北</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關西鎮_(台灣).md" title="wikilink">關西鎮</a></p></td>
<td><p>125.5193</p></td>
<td><p>21</p></td>
<td><p>297</p></td>
<td><p>28,332</p></td>
<td><p><font color=red>-49</font></p></td>
<td><p>226</p></td>
<td><p>306</p></td>
<td><p>竹北</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新埔鎮.md" title="wikilink">新埔鎮</a></p></td>
<td><p>72.1911</p></td>
<td><p>19</p></td>
<td><p>291</p></td>
<td><p>33,030</p></td>
<td><p><font color=red>-7</font></p></td>
<td><p>458</p></td>
<td><p>305</p></td>
<td><p>竹北</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/竹東鎮.md" title="wikilink">竹東鎮</a></p></td>
<td><p>53.5133</p></td>
<td><p>25</p></td>
<td><p>528</p></td>
<td><p>96,374</p></td>
<td><p><font color=red>-30</font></p></td>
<td><p>1,801</p></td>
<td><p>310</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湖口鄉.md" title="wikilink">湖口鄉</a></p></td>
<td><p>58.4303</p></td>
<td><p>20</p></td>
<td><p>462</p></td>
<td><p>77,626</p></td>
<td><p><font color=red>-45</font></p></td>
<td><p>1,329</p></td>
<td><p>303</p></td>
<td><p>竹北</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/橫山鄉_(台灣).md" title="wikilink">橫山鄉</a></p></td>
<td><p>66.3502</p></td>
<td><p>11</p></td>
<td><p>152</p></td>
<td><p>12,922</p></td>
<td><p><font color=blue>+18</font></p></td>
<td><p>195</p></td>
<td><p>312</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新豐鄉_(台灣).md" title="wikilink">新豐鄉</a></p></td>
<td><p>46.3496</p></td>
<td><p>17</p></td>
<td><p>272</p></td>
<td><p>57,135</p></td>
<td><p><font color=blue>+48</font></p></td>
<td><p>1,233</p></td>
<td><p>304</p></td>
<td><p>竹北</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/芎林鄉.md" title="wikilink">芎林鄉</a></p></td>
<td><p>40.7858</p></td>
<td><p>12</p></td>
<td><p>142</p></td>
<td><p>19,976</p></td>
<td><p><font color=red>-16</font></p></td>
<td><p>490</p></td>
<td><p>307</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶山鄉_(台灣).md" title="wikilink">寶山鄉</a></p></td>
<td><p>64.7871</p></td>
<td><p>10</p></td>
<td><p>120</p></td>
<td><p>14,576</p></td>
<td><p><font color=red>-45</font></p></td>
<td><p>225</p></td>
<td><p>308</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北埔鄉.md" title="wikilink">北埔鄉</a></p></td>
<td><p>50.6676</p></td>
<td><p>9</p></td>
<td><p>97</p></td>
<td><p>9,285</p></td>
<td><p><font color=red>-14</font></p></td>
<td><p>183</p></td>
<td><p>314</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/峨眉鄉.md" title="wikilink">峨眉鄉</a></p></td>
<td><p>46.8010</p></td>
<td><p>6</p></td>
<td><p>86</p></td>
<td><p>5,552</p></td>
<td><p><font color=red>-15</font></p></td>
<td><p>119</p></td>
<td><p>315</p></td>
<td><p>竹東</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尖石鄉.md" title="wikilink">尖石鄉</a></p></td>
<td><p>527.5795</p></td>
<td><p>7</p></td>
<td><p>86</p></td>
<td><p>9,677</p></td>
<td><p><font color=blue>+14</font></p></td>
<td><p>18</p></td>
<td><p>313</p></td>
<td><p>山地鄉</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/五峰鄉_(台灣).md" title="wikilink">五峰鄉</a></p></td>
<td><p>227.7280</p></td>
<td><p>4</p></td>
<td><p>58</p></td>
<td><p>4,717</p></td>
<td><p><font color=red>-10</font></p></td>
<td><p>21</p></td>
<td><p>311</p></td>
<td><p>山地鄉</p></td>
</tr>
<tr class="even">
<td><p><strong>新竹縣</strong></p></td>
<td><p>1427.5369</p></td>
<td><p>192</p></td>
<td><p>3,312</p></td>
<td><p>558,020</p></td>
<td><p><font color=blue><strong>+379</strong></font></p></td>
<td><p>391</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 人口消長計算方式為**本月人口**減去**上月人口**，負值以<font color=red>紅字</font>表示，正值以<font color=blue>藍字</font>表示，沒有增長以<font color=green>綠字</font>表示
  - 各區人口密度以4捨5入至小數點前1位計之
  - 新竹縣、新竹市總人口數已破100萬人

## 政治

新竹縣在新竹市被降格為縣轄市期間，[客家人稍多於](../Page/客家人.md "wikilink")[閩南人](../Page/閩南人.md "wikilink")，但差距不大，形成縣長由客家人擔任，議會議長由閩南人掌握的傳統。在閩南人佔多數的[新竹市恢復為省轄市後](../Page/新竹市.md "wikilink")，新竹縣的客家人比率高達85%左右，[宗親勢力抬頭](../Page/宗親.md "wikilink")，[派系反而退居其次](../Page/派系.md "wikilink")，地方上[政黨力量與個人魅力更形薄弱](../Page/政黨.md "wikilink")。\[10\]

### 縣政

[新竹縣政府是新竹縣的最高](../Page/新竹縣政府.md "wikilink")[行政機關](../Page/行政機關.md "wikilink")，在中華民國政府架構中屬於縣政府位階。[新竹縣議會是新竹縣的最高](../Page/新竹縣議會.md "wikilink")[民意機關](../Page/民意機關.md "wikilink")，代表新竹縣全體縣民立法和監察縣政府之施政。[臺灣新竹地方法院是新竹縣的](../Page/臺灣新竹地方法院.md "wikilink")[司法機關](../Page/司法機關.md "wikilink")，屬於普通法院，[臺灣新竹地方法院檢察署是新竹縣的](../Page/臺灣新竹地方法院檢察署.md "wikilink")[檢察機關](../Page/檢察機關.md "wikilink")，為新竹地區之民、刑訴訟和審理機關。

### 歷任縣長

現任新竹縣長為第18屆縣長，由籍的[楊文科擔任](../Page/楊文科.md "wikilink")。

## 經濟產業

[ITRI_(0413).JPG](https://zh.wikipedia.org/wiki/File:ITRI_\(0413\).JPG "fig:ITRI_(0413).JPG")
新竹縣傳統農物產品有[甜柿](../Page/柿.md "wikilink")、[水蜜桃](../Page/水蜜桃.md "wikilink")、[海梨柑](../Page/柑.md "wikilink")、[桶柑](../Page/柑.md "wikilink")、[東方美人茶](../Page/東方美人茶.md "wikilink")、[高接梨](../Page/高接梨.md "wikilink")、[柿餅](../Page/柿餅.md "wikilink")。

[工業技術研究院總院區位於新竹縣竹東鎮](../Page/工業技術研究院.md "wikilink")，成立於1937年（日本海軍第一艦隊科研所）[新竹市東區](../Page/新竹市.md "wikilink")，是台灣最大的產業技術研發機構，也是台灣[半導體產業的開路先鋒](../Page/半導體.md "wikilink")。透過技術引進、人才培育、衍生公司、育成中心、技術服務與技術移轉等不同方式協助業者，對台灣產業發展歷程具有舉足輕重的地位\[11\]。

[新竹科學工業園區主園區座落](../Page/新竹科學工業園區.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")[東區](../Page/東區_\(新竹市\).md "wikilink")，總面積約632公頃，從業人員超過10萬人，是台灣第一個[科學園區](../Page/科學園區.md "wikilink")，進駐廠商分屬[半導體](../Page/半導體.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、[通訊](../Page/通訊.md "wikilink")、[光電](../Page/光電.md "wikilink")、[精密機械](../Page/精密機械.md "wikilink")、[生物技術等產業](../Page/生物技術.md "wikilink")。新竹科學園區是全球半導體製造業最密集的地方之一。

[新竹工業區](../Page/新竹工業區.md "wikilink")，全稱新竹工業園區，舊稱湖口工業區，位於本縣[湖口鄉](../Page/湖口鄉.md "wikilink")，總面積517公頃，屬於綜合性工業區為台灣新竹縣大型工業區，隸屬經濟部工業局管理，；於1977年開發完成第一期，西臨明新科技大學，南鄰竹北台元科技園區。工業區內目前計有614家工廠，從業員工數約64,000人，年產值約新台幣5,908億元。新進駐廠商大都屬高科技產業，如：電子、機械設備與金屬製品……等，其中又以電子零組件製造業佔多數，已逐漸從傳統行業中轉向[高科技](../Page/高科技.md "wikilink")[工業邁進](../Page/工業.md "wikilink")。

## 藝文

### 文學

在文學史及其相關論述方面，博碩士論文有薛建蓉（清代台灣本土[士紳角色扮演及在地意識研究](../Page/士紳.md "wikilink") -
以新竹市[竹塹文人](../Page/竹塹.md "wikilink")[鄭用錫與](../Page/鄭用錫.md "wikilink")[林占梅為探討對象](../Page/林占梅.md "wikilink")）、許楓萱〈清代[明志書院研究](../Page/明志書院.md "wikilink")〉新竹市、武麗芳（日據時期[竹塹地區詩社研究](../Page/竹塹.md "wikilink")）新竹市、黃美娥（清代台灣[竹塹地區傳統文學研究](../Page/竹塹.md "wikilink")）新竹市等；期刊論文有黃美娥（北台文學之冠
- 清代[竹塹地區的文人及其文學活動](../Page/竹塹.md "wikilink")）閩南族群活動等。

該縣古典詩文的編採成果，目前已有新竹縣政府編輯《[大新吟社詩集](../Page/大新吟社.md "wikilink")》。在[賽夏族的神話及習俗的研究成果](../Page/賽夏族.md "wikilink")，目前已有朱鳳生《[賽夏族神話及習俗](../Page/賽夏族.md "wikilink")》。

縣文化局舉辦重要、長期的文學活動有「[吳濁流文藝獎](../Page/吳濁流文藝獎.md "wikilink")」徵文活動並出版得獎作品集、持續發行《新竹文獻》、縣史館設置[吳濁流與](../Page/吳濁流.md "wikilink")[龍瑛宗作家文物展示區](../Page/龍瑛宗.md "wikilink")。

縣籍作家方面，日治時期重要的兩位作家[吳濁流](../Page/吳濁流.md "wikilink")、[龍瑛宗](../Page/龍瑛宗.md "wikilink")，都是在新竹縣出生、成長。戰後，新竹縣出生或是歸入該縣的作家、文學評論者不少，比較著名的有[杜潘芳格](../Page/杜潘芳格.md "wikilink")（新埔）、[林柏燕](../Page/林柏燕.md "wikilink")（新埔）、[劉還月](../Page/劉還月.md "wikilink")（新埔）、[范文芳](../Page/范文芳\(學者\).md "wikilink")（竹東）、[彭鏡禧](../Page/彭鏡禧.md "wikilink")（竹東）、[六月](../Page/六月.md "wikilink")（本名：[劉菊英](../Page/劉菊英.md "wikilink")，新埔）、[徐仁修](../Page/徐仁修.md "wikilink")（芎林）、[彭瑞金](../Page/彭瑞金.md "wikilink")（北埔）、[范陽松](../Page/范陽松.md "wikilink")（湖口）、[張堂錡](../Page/張堂錡.md "wikilink")（湖口）、[馮菊枝等人](../Page/馮菊枝.md "wikilink")\[12\]確定出生在新竹縣市的作家、文學評論者有：[田新彬](../Page/田新彬.md "wikilink")、[江上](../Page/江上.md "wikilink")（本名：江文雙）、[法藍西斯](../Page/法藍西斯.md "wikilink")（本名：謝岱玲）、[袁瓊瓊](../Page/袁瓊瓊.md "wikilink")、[張守禮](../Page/張守禮.md "wikilink")、[張曼娟](../Page/張曼娟.md "wikilink")、[野聲](../Page/野聲.md "wikilink")（本名：曾繼雄）、[黃娟](../Page/黃娟.md "wikilink")（本名：黃瑞娟）、[楊維晨](../Page/楊維晨.md "wikilink")、[詩薇](../Page/詩薇.md "wikilink")（本名：羅秀珍）、[趙妍如](../Page/趙妍如.md "wikilink")、[趙莒玲](../Page/趙莒玲.md "wikilink")、[劉賢妹](../Page/劉賢妹.md "wikilink")、[鄭明娳](../Page/鄭明娳.md "wikilink")、[鄭明痕](../Page/鄭明痕.md "wikilink")、[澍文](../Page/澍文.md "wikilink")（本名：唐美惠）
\[13\]。

推廣及贊助文學活動的基金會，目前有「新竹縣文化基金會」\[14\]；文學社團有「新竹縣客家文化發展協會」\[15\]。

## 文化資產

### 有形文化資產

至2012年2月4日為止，新竹縣文化局（包括改制前的新竹縣立文化中心）已經調查研究並且出版成果的有形文化資產包括國定古蹟有[金廣福公館](../Page/金廣福公館.md "wikilink")1項，縣定古蹟有[竹北蓮華寺](../Page/竹北蓮華寺.md "wikilink")、[大山背樂善堂](../Page/大山背樂善堂.md "wikilink")、[新埔張氏家廟等](../Page/新埔張氏家廟.md "wikilink")23項、歷史建築有[大湖口公學校](../Page/大湖口公學校.md "wikilink")、[竹東頭重並禁、示禁二碑](../Page/竹東頭重並禁、示禁二碑.md "wikilink")、[北埔忠恕堂等](../Page/北埔忠恕堂.md "wikilink")23項。

  - 文化景觀
      - 新竹縣[竹東圳文化地景](../Page/竹東圳.md "wikilink")
      - 新竹縣[北埔事件文化地景](../Page/北埔事件.md "wikilink")
      - 芎林鄉[紙寮窩文化](../Page/紙寮窩.md "wikilink")
      - [頭前溪中上游開墾史的相關史料](../Page/頭前溪.md "wikilink")

### 無形文化資產

至2012年2月4日為止，新竹縣文化局（包括改制前的新竹縣立文化中心），已經調查研究並且出版成果的無形文化資產有：

  - 傳統藝術
      - 客家傳統[山歌](../Page/山歌.md "wikilink")
      - 客家三腳[採茶戲](../Page/採茶戲.md "wikilink")
      - 客家採茶大戲

<!-- end list -->

  - 風俗及有關文物
      - [賽夏族神話故事及習俗](../Page/賽夏族.md "wikilink")
      - 台灣客家俗諺、傳說、詞彙
      - [義民祭文化](../Page/義民祭.md "wikilink")
      - 竹東客家聚落歷史人文資產
      - 竹東[伯公信仰及民俗節慶](../Page/伯公.md "wikilink")
      - 新竹地區[客家人的](../Page/客家人.md "wikilink")[媽祖信仰](../Page/媽祖.md "wikilink")
      - 褒忠亭[義民爺信仰](../Page/義民爺.md "wikilink")

<!-- end list -->

  - 古物
      - 湖口文人遺世書畫作品
      - [大新吟社詩集](../Page/大新吟社.md "wikilink")
      - [陶社詩集](../Page/陶社.md "wikilink")
      - 老大份客詩小集
      - [渡臺悲歌](../Page/渡臺悲歌.md "wikilink")

## 教育

大專院校：[中國科技大學新竹校區](../Page/中國科技大學_\(台灣\).md "wikilink")、[明新科技大學](../Page/明新科技大學.md "wikilink")、[大華科技大學](../Page/大華科技大學.md "wikilink")、[中華科技大學新竹校區](../Page/中華科技大學.md "wikilink")
社會教育:竹北社區大學、竹東社區大學
高中職:[國立竹東高中](../Page/國立竹東高級中學.md "wikilink")、[國立竹北高中](../Page/國立竹北高級中學.md "wikilink")、[國立關西高中](../Page/國立關西高級中學.md "wikilink")、[縣立湖口高中](../Page/新竹縣立湖口高級中學.md "wikilink")、[縣立六家高中](../Page/新竹縣立六家高級中學.md "wikilink")、[內思高工](../Page/新竹縣私立內思高級工業職業學校.md "wikilink")、[忠信高中](../Page/忠信高中.md "wikilink")、[東泰高中](../Page/東泰高中.md "wikilink")、[義民高中](../Page/義民高中.md "wikilink")、[仰德高中](../Page/仰德高中.md "wikilink")

其他事項:
新竹縣在語文閱讀方面也極為推動及重視，曾舉辦過中華民國92年[全國語文競賽重大賽事](../Page/全國語文競賽.md "wikilink")。

## 交通

### 高鐵、台鐵

[台灣高鐵穿越本縣](../Page/台灣高速鐵路.md "wikilink")，[高鐵新竹站設置於](../Page/高鐵新竹站.md "wikilink")[竹北市](../Page/竹北市.md "wikilink")。
[Taiwan_HighSpeedRail_HsinChu_Station_3.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_HighSpeedRail_HsinChu_Station_3.JPG "fig:Taiwan_HighSpeedRail_HsinChu_Station_3.JPG")\]\]
[台鐵](../Page/台鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")、[六家線](../Page/六家線.md "wikilink")、[內灣線都有在本縣設站](../Page/內灣線.md "wikilink")。自2007年3月1日起因興建六家線，內灣線的[新竹](../Page/新竹車站.md "wikilink")
-
[竹東間列車停駛](../Page/竹東車站.md "wikilink")4年8個月，內灣線因為六家線完工已於2011年11月11日恢復通車。

### 公路

#### 高速公路

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/湖口交流道.md" title="wikilink">湖口交流道</a>（新豐、湖口、新竹工業區）</p></li>
<li><p><a href="../Page/竹北交流道.md" title="wikilink">竹北交流道</a>（竹北、芎林、高鐵新竹站）</p></li>
<li><p><a href="../Page/新竹交流道.md" title="wikilink">新竹交流道</a> （出入口B，新竹科學園區）</p></li>
<li><p><a href="../Page/新竹系統交流道.md" title="wikilink">新竹系統交流道</a>（與交會）</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/關西交流道.md" title="wikilink">關西交流道</a>（關西、新埔）</p></li>
<li><p><a href="../Page/竹林交流道.md" title="wikilink">竹林交流道</a>（竹東、芎林、橫山、高鐵新竹站，可間接連接）</p></li>
<li><p><a href="../Page/寶山交流道.md" title="wikilink">寶山交流道</a>（寶山、新竹科學園區）</p></li>
<li><p><a href="../Page/新竹系統交流道.md" title="wikilink">新竹系統交流道</a>（與交會）</p></li>
</ul></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h4 id="快速公路">快速公路</h4>
<ul>
<li><p>：福興溪橋－鳳山溪橋（60k＋151－71k＋900）</p></li>
<li></li>
</ul>
<h4 id="省道">省道</h4>
<ul>
<li><p>：縱貫公路（60K－90K）</p></li>
<li><p>：內山公路（－91K）</p></li>
<li><p>：西濱公路（深坑橋－竹港大橋）</p></li>
<li><p>：高鐵聯外公路桃園段</p></li>
</ul></td>
<td><h4 id="縣道">縣道</h4>
<ul>
<li><p>：觀音－新屋－楊梅－新埔－芎林</p></li>
<li><p>：埔和－湖口－犁頭山下－六家－新竹市<a href="../Page/新竹科學園區.md" title="wikilink">新竹科學園區</a>－<a href="../Page/茄苳交流道.md" title="wikilink">茄苳交流道</a>－新竹市香山區內湖</p></li>
<li><p>：新竹市南寮－竹北－新埔－關西－羅浮</p></li>
<li><p>：下斗崙－芎林－橫山－尖石</p></li>
<li><p>：新竹市南寮－新竹市新竹科學園區－二重埔－竹東－五峰－清泉</p></li>
<li><p>：下山橋－芎林－竹東</p></li>
</ul>
<h4 id="鄉道">鄉道</h4></td>
</tr>
</tbody>
</table>

### 公車客運

國道客運有[國光客運](../Page/國光客運.md "wikilink")、[統聯客運](../Page/統聯客運.md "wikilink")、[台聯客運](../Page/台聯客運.md "wikilink")。

公路客運有[新竹客運](../Page/新竹客運.md "wikilink")、[中壢客運](../Page/中壢客運.md "wikilink")、[桃園客運](../Page/桃園客運.md "wikilink")、[苗栗客運](../Page/苗栗客運.md "wikilink")、金牌客運、科技之星。

## 旅遊

[二寮神木.jpg](https://zh.wikipedia.org/wiki/File:二寮神木.jpg "fig:二寮神木.jpg")
[LeofooVillage_MainEntrance_Back.jpg](https://zh.wikipedia.org/wiki/File:LeofooVillage_MainEntrance_Back.jpg "fig:LeofooVillage_MainEntrance_Back.jpg")主題遊樂園\]\]

### 新竹縣八景

1888年（[清](../Page/清.md "wikilink")[光緒十四年](../Page/光緒.md "wikilink")），廩生陳朝龍受[知縣](../Page/知縣.md "wikilink")[方祖蔭](../Page/方祖蔭.md "wikilink")（今新竹市）委編《新竹采訪冊》，列出「新竹八景」為「指峰凌霄」（[五指山](../Page/五指山_\(新竹縣\).md "wikilink")）、「隙溪吐墨」（[客雅溪](../Page/客雅溪.md "wikilink")）、「香山觀海」（[香山](../Page/香山區.md "wikilink")）、「合水信潮」（[雙溪](../Page/寶山鄉_\(臺灣\).md "wikilink")）、「鳳崎晚霞」（鳳山崎）、「北郭煙雨」（[北郭園](../Page/北郭園.md "wikilink")）、「靈泉試茗」（[冷水坑](../Page/冷水坑溪.md "wikilink")）及「潛園探梅」（[潛園](../Page/潛園.md "wikilink")）\[16\]。唯當時新竹縣、市尚未分治，上述八景中，有五景已不在今日新竹縣境。

縣、市分治後，新竹縣文獻委員會曾重擬「新竹縣八景」，包括、「獅山佛洞」（[獅頭山](../Page/獅頭山風景區.md "wikilink")）、「五指凌霄」（[五指山](../Page/五指山.md "wikilink")）、「清泉試浴」（五峰鄉清泉）、「飛鳯探梅」（芎林[飛鳳山](../Page/飛鳳山_\(新竹縣\).md "wikilink")）、「內灣垂釣」（[內灣溪流](../Page/內灣.md "wikilink")）、「馬武連峰」（關西[馬武督山峰](../Page/馬武督.md "wikilink")）及「鳳崎晚霞」（新豐鳳山崎）\[17\]，均為傳統文人雅士賞景最佳去處。

### 國家級景點

[雪霸國家公園是台灣第五座](../Page/雪霸國家公園.md "wikilink")[國家公園](../Page/國家公園.md "wikilink")，範圍跨越新竹縣、[苗栗縣](../Page/苗栗縣.md "wikilink")、[臺中市](../Page/臺中市.md "wikilink")，總面積達76,850公頃。其中新竹縣境內有兩部分，一部分位於[五峰鄉西南端的](../Page/五峰鄉_\(台灣\).md "wikilink")[爺巴堪溪流域](../Page/爺巴堪溪.md "wikilink")，擁有樂山林道、柳杉樹海、檜山神木等景點，地緣上屬於國家公園三大遊憩區之一的[觀霧遊憩區](../Page/觀霧.md "wikilink")。另一部分位於[尖石鄉南端的](../Page/尖石鄉.md "wikilink")[淡水河源流區](../Page/淡水河.md "wikilink")，有[大霸尖山](../Page/大霸尖山.md "wikilink")、[品田山](../Page/品田山.md "wikilink")、[池有山等多座三千公尺以上高峰](../Page/池有山.md "wikilink")，為台灣知名的登山聖地之一。

[馬告檜木國家公園為](../Page/馬告檜木國家公園.md "wikilink")2002年公告的一座新的國家公園，位於[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[桃園市與新竹縣交界處](../Page/桃園市.md "wikilink")，因當地原住民反對，並未成立管理處，亦未推動實施\[18\]。

[獅頭山風景區為](../Page/獅頭山風景區.md "wikilink")[參山國家風景區的一部分](../Page/參山國家風景區.md "wikilink")，範圍包括新竹縣的[峨眉鄉](../Page/峨眉鄉.md "wikilink")、[北埔鄉](../Page/北埔鄉.md "wikilink")、[竹東鎮](../Page/竹東鎮.md "wikilink")，以及[苗栗縣](../Page/苗栗縣.md "wikilink")[南庄鄉](../Page/南庄鄉_\(台灣\).md "wikilink")、[三灣鄉](../Page/三灣鄉_\(台灣\).md "wikilink")，主要景點有[二寮神木](../Page/二寮神木.md "wikilink")、蓬萊溪自然生態園區、峨嵋湖、向天湖等。

[觀霧國家森林遊樂區位於新竹縣](../Page/觀霧國家森林遊樂區.md "wikilink")[五峰鄉與](../Page/五峰鄉_\(台灣\).md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[泰安鄉交界處](../Page/泰安鄉.md "wikilink")，海拔高約2,000公尺，由[林務局新竹林區管理處經營管理](../Page/林務局.md "wikilink")。區內有瀑布、雲海日出、雪霸連峰、巨大檜木群等特殊景觀。春夏時節百花齊放，秋冬時節楓紅層層，而高山雪景更是遠近馳名。

### 其他景點

[清泉風景特定區位於](../Page/清泉風景特定區.md "wikilink")[五峰鄉桃山村清泉聚落](../Page/五峰鄉_\(台灣\).md "wikilink")，約居五峰聚落通往[觀霧的中點](../Page/觀霧國家森林遊樂區.md "wikilink")，為新竹八景之一。該地原以[清泉溫泉而聞名](../Page/清泉溫泉.md "wikilink")，日治時期舊名「井上溫泉」，曾以「溫泉試浴」名列新竹八景之一。[奉系少帥](../Page/奉系.md "wikilink")[張學良曾被軟禁於此地達十餘年](../Page/張學良.md "wikilink")，其故居原在[上坪溪清泉大橋北側](../Page/上坪溪.md "wikilink")，1963年遭[葛樂禮颱風沖毀](../Page/葛樂禮颱風.md "wikilink")，受限於土石流敏感區無法原地興建，故於二號吊橋西南側參照故居相片重建\[19\]。

新竹縣的主題樂園數量頗多，包括位於關西鎮的[六福村](../Page/六福村主題遊樂園.md "wikilink")、位於新豐鄉的[小叮噹科學主題樂園](../Page/小叮噹科學主題樂園.md "wikilink")、位於橫山鄉的[萬瑞森林樂園](../Page/萬瑞森林樂園.md "wikilink")、位於北埔鄉的[綠世界生態農場](../Page/綠世界生態農場.md "wikilink")、位於竹東鎮的[神秘國度森林度假村](../Page/神秘國度森林度假村.md "wikilink")，以及位於新埔鎮的[新竹楓育樂中心](../Page/新竹楓育樂中心.md "wikilink")\[20\]。

新竹縣尖石鄉有[尖石溫泉](../Page/尖石溫泉.md "wikilink")，此外數量甚多的登山步道亦是運動健身的好去處\[21\]。

### 博物館

新竹縣博物館有：

  - [竹北市](../Page/竹北市.md "wikilink")：[新竹縣文化局美術館](../Page/新竹縣文化局美術館.md "wikilink")
  - [新埔鎮](../Page/新埔鎮.md "wikilink")：[德興古代文物城](../Page/德興古代文物城.md "wikilink")
  - [關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")：[關西鎮立東安博物館](../Page/關西鎮立東安博物館.md "wikilink")
  - [芎林鄉](../Page/芎林鄉.md "wikilink")：[蛋之藝博物館](../Page/蛋之藝博物館.md "wikilink")、[瑞龍博物館](../Page/瑞龍博物館.md "wikilink")
  - [橫山鄉](../Page/橫山鄉_\(台灣\).md "wikilink")：幻多奇另類博物館、[內灣漫畫館](https://www.facebook.com/Neiwancomichouse)
  - [北埔鄉](../Page/北埔鄉.md "wikilink")：[北埔地方文化館](../Page/北埔地方文化館.md "wikilink")
  - [新豐鄉](../Page/新豐鄉_\(台灣\).md "wikilink")：[明新科技大學藝文中心](../Page/明新科技大學藝文中心.md "wikilink")
  - [寶山鄉](../Page/寶山鄉_\(台灣\).md "wikilink")：[竹科寶山藝術村](../Page/竹科寶山藝術村.md "wikilink")

## 縣歌

新竹縣縣歌歌詞為彭崇仁先生所作，由吳丁連先生譜曲。
新竹縣前縣長[范振宗](../Page/范振宗.md "wikilink")(1989～1997年擔任新竹縣長)於任內1995年先公開徵求縣歌歌詞，再聘專家譜曲。除發文至縣內各公立機關學校，也在報紙刊登消息。經由評選，歌詞由當時雙溪國民小學主任彭崇仁先生獲得首獎，並頒發獎金5萬元﹔歌詞確定後，禮聘當時交通大學音樂研究所教授吳丁連先生譜曲，縣歌於焉誕生。歌詞內容為「新竹風，鄉土情，族群情誼最芬芳。忠義勤儉，先民薪傳，敬老恤弱致和祥。大霸雄，鳳溪秀，宜農宜商宜觀光。振興文教，科技櫥窗，共創新竹新希望。」

## 名人

  - [田馥甄](../Page/田馥甄.md "wikilink")：台灣知名女[歌手](../Page/歌手.md "wikilink")，台灣女子團體[S.H.E的成員](../Page/S.H.E.md "wikilink")，出生於新竹縣新豐鄉。
  - [陳喬恩](../Page/陳喬恩.md "wikilink")：台灣知名女[演員](../Page/演員.md "wikilink")
  - [王心凌](../Page/王心凌.md "wikilink")：台灣知名女[歌手](../Page/歌手.md "wikilink")、女[藝人](../Page/藝人.md "wikilink")
  - [羅美玲](../Page/羅美玲.md "wikilink")：台灣知名女[歌手](../Page/歌手.md "wikilink")
  - [溫嵐](../Page/溫嵐.md "wikilink")：台灣知名女[歌手](../Page/歌手.md "wikilink")
  - [曾之喬](../Page/曾之喬.md "wikilink")：台灣知名女[演員](../Page/演員.md "wikilink")、[歌手](../Page/歌手.md "wikilink")
  - [陳盈潔](../Page/陳盈潔.md "wikilink")：台灣資深女[歌手](../Page/歌手.md "wikilink")
  - [劉瑞琪](../Page/劉瑞琪.md "wikilink")：台灣知名女[演員](../Page/演員.md "wikilink")
  - [朱俐靜](../Page/朱俐靜.md "wikilink")：台灣知名女[歌手](../Page/歌手.md "wikilink")
  - [小小彬](../Page/小小彬.md "wikilink") : 台灣[童星](../Page/童星.md "wikilink")
  - [迷你彬](../Page/迷你彬.md "wikilink") : 台灣[童星](../Page/童星.md "wikilink")
  - [游詩璟](../Page/游詩璟.md "wikilink")：臺灣知名本土劇演員，出生於新竹縣竹東鎮。

## 相關條目

  - [桃竹苗](../Page/桃竹苗.md "wikilink")
  - [新竹縣政府](../Page/新竹縣政府.md "wikilink")
  - [新竹縣議會](../Page/新竹縣議會.md "wikilink")
  - [台灣行政區劃](../Page/台灣行政區劃.md "wikilink")
  - [臺灣客家文化重點發展區](../Page/臺灣客家文化重點發展區.md "wikilink")

## 參考文獻

## 外部連結

{{ Commons|新竹縣}}

  - [新竹縣政府](http://www.hchg.gov.tw/)
  - [新竹縣議會](http://www.hcc.gov.tw/)

[新竹縣](../Category/新竹縣.md "wikilink")

1.

2.  [《新竹縣歷史人文：竹塹風貌》，發現新竹縣網站](http://w3.hsinchu.gov.tw/hchg/hcintro/discover/taiwan/taiwan.htm)

3.  王郭章，《圖說竹塹》，2004年

4.
5.  [《環境地景特質分析：新竹縣-地理》，發現新竹縣網站](http://w3.hsinchu.gov.tw/hchg/hcintro/discover/taiwan/taiwan.htm)

6.  《台灣走透透地圖王》，戶外生活圖書，2007年3月，第33頁

7.
8.

9.  [新竹縣戶政便民服務網-人口統計](http://w3.hsinchu.gov.tw/HouseWeb/main/Webpage/PeoCount/List.aspx)

10. 王建民，《台灣地方派系與權力結構》，九州出版社，2003年

11. [《工研院簡介》，工業技術研究院官方網站](http://www.itri.org.tw/chi/about/index.asp?RootNodeId=080&NodeId=0800)

12. [黃恆秋等人](../Page/黃恆秋.md "wikilink")/著，《台灣客家族群史：學藝篇》的「附錄：當代客籍文學家及其作品」，南投市：[台灣文獻館](../Page/台灣文獻館.md "wikilink")，2004年初版。

13. 台灣文學館，「2007台灣作家作品名錄」搜尋系統。

14. 台灣文學館出版《2007 台灣文學年鑑》的「文學活動基金會」名錄。

15. 台灣文學館出版《2007 台灣文學年鑑》的「文學團體」名錄。

16. 陳朝龍，《新竹采訪冊》

17. [陳立台，《新竹市青草湖文化景觀之探討》，元培科學技術大學通識教育中心，第9頁](http://edoc.ypu.edu.tw:8080/%E9%80%9A%E8%AD%98%E4%B8%AD%E5%BF%83/%E7%A0%94%E8%A8%8E%E6%9C%83/%E4%BA%BA%E6%96%87%E7%A4%BE%E6%9C%83%E7%B5%84/Microsoft%20Word%20-%20AA-154-%E6%96%B0%E7%AB%B9%E5%B8%82%E9%9D%92%E8%8D%89%E6%B9%96%E6%96%87%E5%8C%96%E6%99%AF%E8%A7%80%E4%B9%8B%E6%8E%A2%E8%A8%8E.pdf)


18.

19. [《景點介紹》，將軍文學美人湯官方網站](http://general-oldhome.emmm.tw/?ptype=marj)

20. [《主題旅遊：主題樂園》，新竹縣旅遊網，新竹縣政府觀光旅遊處](http://www.hsinchu.gov.tw/travel/modules/descovering/amusement/detail.asp?Planid=209&pageno=1)


21. [新竹縣登山步道](http://lohas.hcc.edu.tw/facilities.php?fID=15)