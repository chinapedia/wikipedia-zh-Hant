[新竹郡役所.JPG](https://zh.wikipedia.org/wiki/File:新竹郡役所.JPG "fig:新竹郡役所.JPG")
**新竹郡**為[台灣日治時期的](../Page/台灣日治時期.md "wikilink")46郡二級行政區之一。隸屬於一級行政區[新竹州](../Page/新竹州.md "wikilink")。

## 簡介

新竹郡役所設於[新竹市](../Page/新竹市.md "wikilink")，所址為今[國立新竹高級工業職業學校](../Page/國立新竹高級工業職業學校.md "wikilink")（行政大樓）。管轄[新竹街](../Page/新竹街.md "wikilink")、[新埔街](../Page/新埔街.md "wikilink")、[關西街](../Page/關西街.md "wikilink")、[香山-{庄}-](../Page/香山庄.md "wikilink")、[舊港-{庄}-](../Page/舊港庄.md "wikilink")、[六家-{庄}-](../Page/六家庄.md "wikilink")、[紅毛-{庄}-](../Page/紅毛庄.md "wikilink")、[湖口-{庄}-](../Page/湖口庄.md "wikilink")，以及不設街-{庄}-之[蕃地](../Page/蕃地_\(新竹郡\).md "wikilink")（今[關西鎮錦山](../Page/關西鎮_\(台灣\).md "wikilink")-{里}-馬武督部落）。即今新竹市、[竹北市](../Page/竹北市.md "wikilink")、[新埔鎮](../Page/新埔鎮.md "wikilink")、[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[新豐鄉](../Page/新豐鄉_\(台灣\).md "wikilink")、[湖口鄉](../Page/湖口鄉.md "wikilink")。

其中新竹街於1930年升格新竹市，獨立直轄[新竹州](../Page/新竹州.md "wikilink")，設市役所（今新竹市美術館暨開拓館）。郡役所雖已不轄新竹市，但仍維持原址辦公。而舊港-{庄}-、六家-{庄}-1941年合併成立[竹北-{庄}-](../Page/竹北庄.md "wikilink")。香山-{庄}-1941年廢-{庄}-，併入新竹市。

## 統計

<table>
<caption>1942年本籍國籍別常住戶口[1]</caption>
<thead>
<tr class="header">
<th style="text-align: center;"><p>街-{庄}-名</p></th>
<th style="text-align: center;"><p>面積<br />
（km²）</p></th>
<th style="text-align: center;"><p>人口 （人）</p></th>
<th style="text-align: center;"><p>人口密度<br />
（人/km²）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>本籍</p></td>
<td style="text-align: center;"><p>國籍</p></td>
<td style="text-align: center;"><p>計</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>臺灣</p></td>
<td style="text-align: center;"><p>內地</p></td>
<td style="text-align: center;"><p>朝鮮</p></td>
<td style="text-align: center;"><p>中華民國</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>竹北-{庄}-</p></td>
<td style="text-align: center;"><p>49.4584</p></td>
<td style="text-align: center;"><p>22,407</p></td>
<td style="text-align: center;"><p>97</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>紅毛-{庄}-</p></td>
<td style="text-align: center;"><p>44.3486</p></td>
<td style="text-align: center;"><p>12,720</p></td>
<td style="text-align: center;"><p>51</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>湖口-{庄}-</p></td>
<td style="text-align: center;"><p>58.4303</p></td>
<td style="text-align: center;"><p>17,237</p></td>
<td style="text-align: center;"><p>203</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>新埔-{庄}-</p></td>
<td style="text-align: center;"><p>72.1911</p></td>
<td style="text-align: center;"><p>25,394</p></td>
<td style="text-align: center;"><p>127</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>關西-{庄}-</p></td>
<td style="text-align: center;"><p>105.5629</p></td>
<td style="text-align: center;"><p>25,461</p></td>
<td style="text-align: center;"><p>124</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>蕃地</p></td>
<td style="text-align: center;"><p>19.9564</p></td>
<td style="text-align: center;"><p>1,843</p></td>
<td style="text-align: center;"><p>14</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>新竹郡</p></td>
<td style="text-align: center;"><p>414.7248</p></td>
<td style="text-align: center;"><p>105,062</p></td>
<td style="text-align: center;"><p>616</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 後續發展

1945年3月[重慶](../Page/重慶.md "wikilink")[國民政府通過之](../Page/國民政府.md "wikilink")[台灣接管計劃綱要地方政制的](../Page/台灣接管計劃綱要地方政制.md "wikilink")[台灣](../Page/台灣.md "wikilink")[行政區劃中](../Page/行政區劃.md "wikilink")，，將新竹郡改制為**滄海縣**，為該地方政制的30個[縣之一](../Page/縣.md "wikilink")。因為據傳丘逢甲旗下之[台灣民主國兵士曾在](../Page/台灣民主國.md "wikilink")[乙未戰爭於關西](../Page/乙未戰爭.md "wikilink")、湖口等地與日軍激戰，因此國民政府特將此縣以抗日名將丘逢甲之別號「滄海」為名，以紀念他在對台灣的貢獻。

1945年10月，因[台灣省行政長官公署認為該政制不符實際](../Page/台灣省行政長官公署.md "wikilink")，因此「台灣接管計劃地方政制」並未實施，僅將**州廳－郡市－街-{庄}-**各級行政區改稱**縣市－區－鄉鎮**，新竹郡改稱**新竹區**。1950年新竹區廢止，所轄之鄉鎮直接隸屬[新竹縣管轄](../Page/新竹縣.md "wikilink")。

## 參考資料

## 參考文獻與網站

  - 施亞軒，《台灣的行政區變遷》，2003年，台北，遠足文化出版社。
  - [日治時期公共建築系列之十六_新竹郡役所新廳舍](http://mypaper.pchome.com.tw/o1ympic/post/1322510203)

[Category:新竹州](../Category/新竹州.md "wikilink")
[新竹郡](../Category/新竹郡.md "wikilink")

1.