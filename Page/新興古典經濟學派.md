**第二代新古典主义经济学**（）也被译为**-{zh-cn:新兴古典经济学派; zh-sg:新兴古典经济学派; zh-hk:新興古典經濟學派;
zh-mo:新興古典經濟學派;
zh-tw:第二代新古典主義經濟學;}-**、**第二代新古典派經濟學**，也被称为**新興古典總體經濟學派**（），是宏觀經濟學派之一。它于1970年代形成，是[第一代新古典主义经济学](../Page/第一代新古典主义经济学.md "wikilink")（）的後繼者。它強調[總體經濟學必須立足於](../Page/總體經濟學.md "wikilink")[個體經濟學的基礎](../Page/個體經濟學.md "wikilink")，主張[理性預期](../Page/理性預期.md "wikilink")，主要的理論競爭對手為[新興凱恩斯學派](../Page/新興凱恩斯學派.md "wikilink")。

## 歷史

在[經濟大蕭條後](../Page/經濟大蕭條.md "wikilink")，因[羅斯福新政的成功](../Page/羅斯福新政.md "wikilink")，[新凱恩斯學派成為經濟學界的主流](../Page/新凱恩斯學派.md "wikilink")。1970年代，[停滯性通貨膨脹的出現](../Page/停滯性通貨膨脹.md "wikilink")，使[第一代新凯恩斯主义经济学遭受質疑](../Page/第一代新凯恩斯主义经济学.md "wikilink")。[米爾頓·佛利民建立的](../Page/米爾頓·佛利民.md "wikilink")[貨幣學派](../Page/貨幣學派.md "wikilink")，以及[小罗伯特·卢卡斯建立的](../Page/小罗伯特·卢卡斯.md "wikilink")[理性預期假說](../Page/理性預期.md "wikilink")，讓古典經濟學派重回經濟學舞台，新興古典經濟學派得以产生\[1\]。支持新興古典經濟學派的大學，被稱為[淡水學派](../Page/淡水學派.md "wikilink")，它与支持凱恩斯主義的[鹽水學派成為](../Page/鹽水學派.md "wikilink")[主流經濟學的兩大骨幹](../Page/主流經濟學.md "wikilink")。

第二代新古典主义经济学的理论框架由[动态分析](../Page/动态分析.md "wikilink")、[理性预期假说和](../Page/理性预期假说.md "wikilink")[自然失业率假说组成](../Page/自然失业率.md "wikilink")，因此最初被稱為理性预期学派（）。该学派主张市场经济能自动解决失业、不景气等问题，而政府主导的稳定政策没有任何效果。在[失业和](../Page/失业.md "wikilink")[通货膨胀的两难问题不仅在长期](../Page/通货膨胀.md "wikilink")，短期也不存在这一点上，与[货币主义不同](../Page/货币主义.md "wikilink")。

## 主要代表人物

  - [小罗伯特·卢卡斯](../Page/小罗伯特·卢卡斯.md "wikilink")（Robert E. Lucas Jr.）
  - [羅伯特·巴羅](../Page/羅伯特·巴羅.md "wikilink")（Robert J. Barro）
  - [托瑪斯·薩金特](../Page/托瑪斯·薩金特.md "wikilink")（Thomas, J. Sargent）
  - [杨小凯](../Page/杨小凯.md "wikilink")（xiaokai,yang）

## 備註

目前在台灣與中國大陆，“Neoclassical economics”与“New classical economics”的翻译方式并不相同。

  - 在[中国大陆](../Page/中国大陆.md "wikilink")，翻译方式如下
      - 对应于早期的，称-{zh-cn:“第一代新古典主义经济学”; zh-sg:“第一代新古典主义经济学”;
        zh-hk:“第一代新古典主義經濟學”; zh-tw: 「第一代新古典主義經濟學」;
        }-（Neoclassical economics）。
      - 对应于始于1970年代的，称-{zh-cn:“第二代新古典主义经济学”; zh-sg:“第二代新古典主义经济学”;
        zh-hk:“第二代新古典主義經濟學”; zh-tw: 「第二代新古典主義經濟學」;}-（New Classical
        economics）。

<!-- end list -->

  - 至於台灣，則有各自對應的翻譯名稱。
      - 對應於早期的，稱-{zh-cn:“新古典经济学派”; zh-sg:“新古典经济学派”; zh-hk:“新古典經濟學派”;
        zh-tw:「新古典經濟學派」;}-（Neoclassical economics）。
      - 對應於始於20世紀70年代的，稱-{zh-cn:“新兴古典经济学派”; zh-sg:“新兴古典经济学派”;
        zh-hk:“新興古典經濟學派”; zh-tw:「新興古典經濟學派」;}-（（New Classical
        economics）。

## 参见

  - [第一代新古典主义经济学](../Page/第一代新古典主义经济学.md "wikilink")（）
  - [第一代新凯恩斯主义经济学](../Page/第一代新凯恩斯主义经济学.md "wikilink")（）
  - [第二代新凯恩斯主义经济学](../Page/第二代新凯恩斯主义经济学.md "wikilink")（）

## 注释

## 外部链接

  - [New Classical
    Macroeconomics](http://www.econlib.org/library/Enc/NewClassicalMacroeconomics.html)

[X](../Category/宏观经济学学派.md "wikilink")
[X](../Category/芝加哥經濟學派.md "wikilink")
[X](../Category/新興古典經濟學派.md "wikilink")

1.  [金融危机：为何不能完全预测和防范](http://finance.ifeng.com/roll/20090817/1094248.shtml)