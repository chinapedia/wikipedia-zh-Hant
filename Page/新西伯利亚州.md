**新西伯利亚州**（），位於[西西伯利亞平原東南部](../Page/西西伯利亞平原.md "wikilink")，是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")。面積178,200平方公里，人口2,692,251（2002年）。首府[新西伯利亚](../Page/新西伯利亚.md "wikilink")。该州与[哈萨克斯坦接壤](../Page/哈萨克斯坦.md "wikilink")。

## 历史

中世纪时，该地区由[西伯利亞韃靼人和](../Page/西伯利亞韃靼人.md "wikilink")[铁列乌特人居住](../Page/铁列乌特人.md "wikilink")。

## 参注

[\*](../Category/新西伯利亞州.md "wikilink")
[Category:西伯利亚联邦管区](../Category/西伯利亚联邦管区.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")