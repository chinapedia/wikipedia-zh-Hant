**新西兰工党**是现今新西兰两大主要政党之一，是政治立場[中间偏左的政党](../Page/中间偏左.md "wikilink")。在2011年大选中，工党赢得121个议会议席中的34个，现任党魁（第17任）是[傑辛達·阿德恩](../Page/傑辛達·阿德恩.md "wikilink")。該黨的綱領自稱其建黨理論是[民主社會主義](../Page/民主社會主義.md "wikilink")，\[1\]但觀察者一般將其實際行動形容為[社会民主主义和趨向實用主義](../Page/社会民主主义.md "wikilink")。與其他民主社會主義及社會民主主義政黨類似，新西蘭工黨參加的政黨國際組織是[進步聯盟](../Page/進步聯盟.md "wikilink")。

## 历史

工党创建于[1916年](../Page/1916年.md "wikilink")。20世紀初，新西蘭的左翼政治勢力組織主要為激進的社會黨和溫和的獨立政治勞工聯盟。兩個組織經歷歷次合并和分裂後在1916年合并為現在的工黨。1919年，工党在成立后第一場选举中赢得8个议席（当时的执政党有47个议席），之後议席不断增加。1935年，在[迈克尔·萨瓦奇带领下赢得大选](../Page/迈克尔·萨瓦奇.md "wikilink")，组成[新西兰历史上第一个工党政府](../Page/新西兰历史.md "wikilink")。工党的胜利导致当时两个主要党派，[自由党和](../Page/新西兰自由党.md "wikilink")[改革党合并](../Page/新西兰改革党.md "wikilink")，组成现今新西兰政界两大政党之一的[国家党](../Page/新西兰国家党.md "wikilink")。

## 历任党魁

1.  [阿尔弗雷德·欣德马什](../Page/阿尔弗雷德·欣德马什.md "wikilink")（1916年－1918年）
2.  [哈里·霍兰德](../Page/哈里·霍兰德.md "wikilink")（1919年－1933年）
3.  [迈克尔·约瑟夫·萨瓦奇](../Page/迈克尔·约瑟夫·萨瓦奇.md "wikilink")（1933年－1940年；1935年－1940年任[总理](../Page/新西兰总理.md "wikilink")）
4.  [彼得·弗雷泽](../Page/彼得·弗雷泽.md "wikilink")（1940年－1950年；1940年－1949年任总理）
5.  [華爾特·納許](../Page/華爾特·納許.md "wikilink")（1951年－1963年；1957年－1960年任总理）
6.  [阿诺德·诺德梅尔](../Page/阿诺德·诺德梅尔.md "wikilink")（1963年－1965年）
7.  [诺曼·柯克](../Page/诺曼·柯克.md "wikilink")（1965年－1974年；1972年－1974年任总理）
8.  [比尔·罗林](../Page/比尔·罗林.md "wikilink")（1974年－1983年；1974年－1975年任总理）
9.  [戴维·朗伊](../Page/戴维·朗伊.md "wikilink")（1983年－1989年；1984年－1989年任总理）
10. [杰弗里·帕尔默](../Page/杰弗里·帕尔默.md "wikilink")（1989年－1990年；1989年－1990年任总理）
11. [迈克尔·穆尔](../Page/迈克尔·穆尔.md "wikilink")（1990年－1993年；1990年任总理）
12. [海伦·克拉克](../Page/海伦·克拉克.md "wikilink")（1993年－2008年；1999年－2008年任总理）
13. [菲爾·戈夫](../Page/菲爾·戈夫.md "wikilink")（2008年－2011年）
14. [大衛·舒利亞](../Page/大衛·舒利亞.md "wikilink")（2011年－2013年）
15. [戴维·坎利夫](../Page/戴维·坎利夫.md "wikilink")（2013年－2014年）
16. [安德魯·利特爾](../Page/安德魯·利特爾.md "wikilink")（2014年－2017年）
17. [傑辛達·阿德恩](../Page/傑辛達·阿德恩.md "wikilink")（2017年至今，現為紐西蘭總理）

## 参考文献

## 外部链接

  - [官方网站](http://www.labour.org.nz/)
  - [Red Alert – the Labour Party
    Blog](https://web.archive.org/web/20110821011112/http://blog.labour.org.nz/)
  - [Official web site for City Vision – the Auckland Local Government
    Group that includes Labour
    candidates](http://www.cityvision.org.nz/)

{{-}}

[Category:紐西蘭政黨](../Category/紐西蘭政黨.md "wikilink")
[新西兰工党](../Category/新西兰工党.md "wikilink")
[Category:社會民主主義政黨](../Category/社會民主主義政黨.md "wikilink")
[Category:工黨](../Category/工黨.md "wikilink")
[Category:1916年建立的政黨](../Category/1916年建立的政黨.md "wikilink")
[Category:社會黨國際](../Category/社會黨國際.md "wikilink")

1.