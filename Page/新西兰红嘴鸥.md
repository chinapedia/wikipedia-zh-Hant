**新西蘭紅嘴鷗**（[學名](../Page/學名.md "wikilink")：*Larus
scopulinus*）是[新西蘭的](../Page/新西蘭.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，分佈全國及其外圍[島嶼](../Page/島嶼.md "wikilink")，包括[查塔姆群島及](../Page/查塔姆群島.md "wikilink")。

新西蘭紅嘴鷗是較為細小的[海鷗](../Page/海鷗.md "wikilink")，整個[鳥喙都是](../Page/鳥喙.md "wikilink")[紅色的](../Page/紅色.md "wikilink")，紅色的[腳](../Page/腳.md "wikilink")，[淡灰色的](../Page/淡灰色.md "wikilink")[翼](../Page/翅膀.md "wikilink")，及[黑色的](../Page/黑色.md "wikilink")[翼尖](../Page/翼尖.md "wikilink")。牠們似乎不是[雙性異形](../Page/雙性異形.md "wikilink")。牠們是新西蘭最細小普遍的海鷗。牠們曾被認為是[澳洲銀鷗的](../Page/澳洲銀鷗.md "wikilink")[亞種](../Page/亞種.md "wikilink")，而兩種在[外表上亦很相似](../Page/外表.md "wikilink")。但是亦有研究指牠們是[近親](../Page/近親.md "wikilink")\[1\]。

新西蘭紅嘴鷗有典型海鷗的行為。牠們吃[腐肉](../Page/腐肉.md "wikilink")，亦有[搶食行為](../Page/搶食.md "wikilink")。大部份新西蘭紅嘴鷗是生活在新西蘭的[城市中](../Page/城市.md "wikilink")，在[垃圾堆中尋找](../Page/垃圾.md "wikilink")[食物](../Page/食物.md "wikilink")。牠們以大的[族群居住](../Page/族群.md "wikilink")，[配偶所生的子女會在巢中居住幾季](../Page/配偶.md "wikilink")，而且亦有一定的偶外交配\[2\]。求偶餵食行為是牠們交配的重要一環\[3\]。

## 參考資料

## 外部連結

  - [Picture](http://home.planet.nl/~mkramer/pic/Redbilled_Gull.jpg)

[Category:鷗屬](../Category/鷗屬.md "wikilink")

1.
2.
3.