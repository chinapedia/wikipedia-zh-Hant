**新西蘭鴝鶲**（*Petroica australis
longipes*）是[新西蘭](../Page/新西蘭.md "wikilink")[鴝鶲科的一種](../Page/鴝鶲科.md "wikilink")，原住在[北島](../Page/北島_\(紐西蘭\).md "wikilink")。牠們曾一度被認為是[南島及](../Page/南島.md "wikilink")[斯圖爾特島](../Page/斯圖爾特島.md "wikilink")[鴝鶲的](../Page/鴝鶲.md "wikilink")[亞種](../Page/亞種.md "wikilink")，但牠們的[線粒體DNA序列顯示於](../Page/線粒體DNA.md "wikilink")[更新世就與鴝鶲分開](../Page/更新世.md "wikilink")[演化](../Page/演化.md "wikilink")，故是屬於兩個不同的[物種](../Page/物種.md "wikilink")。\[1\]新西蘭鴝鶲主要分佈在北島的中部，小部份在[島嶼灣](../Page/島嶼灣.md "wikilink")、[小巴里爾島及](../Page/小巴里爾島.md "wikilink")[卡皮蒂島上](../Page/卡皮蒂島.md "wikilink")。\[2\]在[惠靈頓的](../Page/惠靈頓.md "wikilink")[瑪提尤/薩姆斯島及](../Page/瑪提尤/薩姆斯島.md "wikilink")[卡洛里動植物保護區現正重新引入這種](../Page/卡洛里動植物保護區.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。\[3\]\[4\]牠們生活在介乎海平面至樹線的天然森林中，尤其是[羅漢松屬及](../Page/羅漢松屬.md "wikilink")[假山毛櫸屬的森林](../Page/假山毛櫸屬.md "wikilink")。

## 描述

新西蘭鴝鶲長18厘米，較為大型，但卻比[鴝鶲細小些](../Page/鴝鶲.md "wikilink")。\[5\][羽毛呈深灰黑色](../Page/羽毛.md "wikilink")，[腹部及](../Page/腹部.md "wikilink")[胸部顏色較淺](../Page/胸部.md "wikilink")，上身有淡色的斑汶。牠們是[兩性異形的](../Page/兩性異形.md "wikilink")，雄鳥羽毛較深色，雌鳥體型較大。\[6\]

## 行為

### 食性

新西蘭鴝鶲像[鴝鶲般](../Page/鴝鶲.md "wikilink")，是有區域性的，喜歡在地面上覓食。牠們會在木上等待獵物的出現，並將獵物帶到[樹葉堆中](../Page/樹葉.md "wikilink")、灌木或樹幹中。牠們主要獵食[無脊椎動物](../Page/無脊椎動物.md "wikilink")，包括[蟬](../Page/蟬.md "wikilink")、[蚯蚓](../Page/蚯蚓.md "wikilink")、[蝸牛及](../Page/蝸牛.md "wikilink")[蜘蛛](../Page/蜘蛛.md "wikilink")。牠們亦會吃[果實](../Page/果實.md "wikilink")。\[7\]新西蘭鴝鶲會將多餘的食物儲起，而雄鳥比雌鳥儲得更多食物。\[8\]雄鳥及雌鳥都會偷走牠們配偶的食物，若配偶在時則較少會儲起食物。

## 參考

[Category:鸲鹟属](../Category/鸲鹟属.md "wikilink")

1.

2.

3.

4.

5.
6.

7.
8.