**新都区**是[中国](../Page/中华人民共和国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[成都市的一个](../Page/成都市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于成都市北部，距离成都市中心[天府广场约](../Page/天府广场.md "wikilink")20公里，是成都的八大[卫星城之一](../Page/卫星城.md "wikilink")、城北副中心，城区已与成都五城区、[青白江区](../Page/青白江区.md "wikilink")、[广汉市连成一片](../Page/广汉市.md "wikilink")，是成都重要的商贸集聚区及高端装备制造业基地\[1\]。2017年，被纳入成都中心城区\[2\]。

## 历史沿革

新都为[古蜀国都邑](../Page/古蜀国.md "wikilink")，于公元前七世纪左右由蜀王[开明氏称帝后所建](../Page/开明_\(蜀国\).md "wikilink")，为有别于[杜宇氏的旧都郫邑](../Page/杜宇氏.md "wikilink")，得名“新都”。\[3\]

公元前221年，[秦置新都县](../Page/秦朝.md "wikilink")，隶[蜀郡](../Page/蜀郡.md "wikilink")，后隶[广汉郡](../Page/廣漢郡.md "wikilink")（子同郡）。[西晋](../Page/西晋.md "wikilink")[泰始二年](../Page/泰始_\(西晋\).md "wikilink")（266年），设[新都郡](../Page/新都郡_\(西晋\).md "wikilink")，属[梁州](../Page/梁州_\(曹魏\).md "wikilink")，领新都等四县。[宋](../Page/刘宋.md "wikilink")、[齐时属益州广汉郡](../Page/南齐.md "wikilink")，[梁属始康郡](../Page/梁_\(南朝\).md "wikilink")。[西魏](../Page/西魏.md "wikilink")[废帝二年](../Page/元钦.md "wikilink")（533年）改隶蜀郡，[隋](../Page/隋朝.md "wikilink")[开皇二年](../Page/开皇.md "wikilink")（598年）改为兴乐县，随即并入[成都县](../Page/成都縣.md "wikilink")。[唐](../Page/唐朝.md "wikilink")[武德二年](../Page/武德.md "wikilink")（619年）复置、复名为新都县，隶[剑南道](../Page/剑南道.md "wikilink")[益州](../Page/益州.md "wikilink")（后更名为[蜀郡](../Page/蜀郡.md "wikilink")、[成都府](../Page/成都府.md "wikilink")）。[宋属](../Page/宋朝.md "wikilink")[四川路](../Page/四川路.md "wikilink")[成都府路成都府](../Page/成都府路.md "wikilink")，元属[四川等处行中书省](../Page/四川等处行中书省.md "wikilink")[成都路](../Page/成都路.md "wikilink")，明属[四川布政使司成都路](../Page/四川等處承宣布政使司.md "wikilink")，清属[四川省](../Page/四川省_\(清\).md "wikilink")[成绵龙茂道成都府](../Page/成绵龙茂道.md "wikilink")，[民国初年属](../Page/中華民國_\(大陸時期\).md "wikilink")[四川省](../Page/四川省_\(中華民國\).md "wikilink")[西川道](../Page/西川道.md "wikilink")。\[4\]\[5\]

1950年，新都县隶属[川西行署](../Page/川西行政区.md "wikilink")[温江专区](../Page/温江专区.md "wikilink")。1953年，改隶四川省温江专区。1960年2月，撤销新都县并入[新繁县](../Page/新繁县.md "wikilink")。1962年10月，恢复新都县。1965年7月，撤销新繁县并入新都县。1983年，温江地区撤销，改隶[成都市](../Page/成都市.md "wikilink")。2001年11月，国务院批复同意撤销新都县，设立成都市新都区，以原新都县的行政区域为新都区的行政区域，区人民政府驻[新都镇](../Page/新都镇.md "wikilink")（今新都街道），2002年1月1日正式挂牌成立。\[6\]\[7\]\[8\]\[9\]

## 行政区划

新都区下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、10个[镇](../Page/行政建制镇.md "wikilink")\[10\]：

  - 街道：[新都街道](../Page/新都街道_\(成都市\).md "wikilink")、[大丰街道](../Page/大丰街道.md "wikilink")、[三河街道](../Page/三河街道.md "wikilink")
  - 镇：[新繁镇](../Page/新繁镇.md "wikilink")、[石板滩镇](../Page/石板滩镇_\(成都市\).md "wikilink")、[新民镇](../Page/新民镇_\(成都市\).md "wikilink")、[泰兴镇](../Page/泰兴镇.md "wikilink")、[清流镇](../Page/清流镇_\(成都市\).md "wikilink")、[马家镇](../Page/马家镇_\(成都市\).md "wikilink")、[龙桥镇](../Page/龙桥镇_\(成都市\).md "wikilink")、[木兰镇](../Page/木兰镇_\(成都市\).md "wikilink")、[军屯镇](../Page/军屯镇_\(成都市\).md "wikilink")、[斑竹园镇](../Page/斑竹园镇_\(成都市\).md "wikilink")

## 现任领导

<table>
<caption>成都市新都区五大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党成都市新都区委员会.md" title="wikilink">中国共产党<br />
成都市新都区委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/成都市新都区人民代表大会.md" title="wikilink">成都市新都区人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/成都市新都区人民政府.md" title="wikilink">成都市新都区人民政府</a><br />
<br />
区长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议成都市新都区委员会.md" title="wikilink">中国人民政治协商会议<br />
成都市新都区委员会</a><br />
主席</p></th>
<th><p><br />
<a href="../Page/成都市新都区监察委员会.md" title="wikilink">成都市新都区监察委员会</a><br />
<br />
主任</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/许兴国.md" title="wikilink">许兴国</a>[11]</p></td>
<td><p><a href="../Page/戴军_(官员).md" title="wikilink">戴军</a>[12]</p></td>
<td><p><a href="../Page/李云_(官员).md" title="wikilink">李云</a>[13]</p></td>
<td><p><a href="../Page/方正行.md" title="wikilink">方正行</a>[14]</p></td>
<td><p><a href="../Page/钟思勇.md" title="wikilink">钟思勇</a>[15]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/宜宾市.md" title="wikilink">宜宾市</a></p></td>
<td><p>四川省<a href="../Page/眉山市.md" title="wikilink">眉山市</a></p></td>
<td><p>四川省自贡市</p></td>
<td><p>四川省<a href="../Page/邛崃市.md" title="wikilink">邛崃市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年7月</p></td>
<td><p>2016年2月</p></td>
<td><p>2016年12月</p></td>
<td><p>2016年2月</p></td>
<td><p>2018年2月</p></td>
</tr>
</tbody>
</table>

## 自然地理

新都区地处[四川盆地](../Page/四川盆地.md "wikilink")[川西平原腹地](../Page/成都平原.md "wikilink")、[都江堰自流灌区](../Page/都江堰.md "wikilink")，土地肥沃，气候温和，物产丰富，历史上被誉为“蜀中宝玉”、“天府明珠”，是四川著名的粮库之一，是全国著名的商品粮油生产基地，素有“天府粮仓”之称。区内北有[清白江](../Page/清白江.md "wikilink")，南有毗河，中有锦水河分干渠，水源充足。\[16\]

新都区属于中亚热带湿润季风气候区，气候温和，雨量充足，温湿同季，水热同步，四季分明，年均气温16.1℃，年均降水量为828.3毫米，年均日照时数为1214.9小时。\[17\]

## 经济

新都区位优越，是[成德绵高新技术产业带重要节点城市](../Page/成德绵高新技术产业带.md "wikilink")，位于成都城市发展北中轴线，是“蓉欧快铁”和“南丝绸之路”的起点城市\[18\]。

2017年新都区[地区生产总值达到](../Page/地区生产总值.md "wikilink")722.67亿元，同比增长8.9%，其中[第一产业](../Page/第一产业.md "wikilink")、[第二产业](../Page/第二产业.md "wikilink")、[第三产业占比为](../Page/第三产业.md "wikilink")3.9：59.5：36.6\[19\]。实现社会消费品零售总额176.01亿元，同比增长14.6%\[20\]。

## 著名景点

  - [升庵桂湖](../Page/升庵桂湖.md "wikilink")，川西园林代表作之一，入选[第四批全国重点文物保护单位](../Page/第四批全国重点文物保护单位.md "wikilink")\[21\]
  - [宝光寺](../Page/宝光寺.md "wikilink")，[全国重点寺庙](../Page/汉族地区佛教全国重点寺院.md "wikilink")，入选[第五批全国重点文物保护单位](../Page/第五批全国重点文物保护单位.md "wikilink")

## 歷史名人

  - [楊廷和](../Page/楊廷和.md "wikilink")（1458年-1529年），字介夫，号石斋，[明朝中葉大臣](../Page/明朝.md "wikilink")、政治改革家，为[武宗](../Page/明武宗.md "wikilink")、[世宗两朝内阁首辅](../Page/明世宗.md "wikilink")，后被革职。谥文忠。\[22\]\[23\]\[24\]
  - [杨廷仪](../Page/楊廷儀.md "wikilink")，字正夫，杨廷和弟，官至[礼部尚书](../Page/礼部尚书.md "wikilink")。\[25\]
  - [楊慎](../Page/楊慎.md "wikilink")，字用修，號昇庵，楊廷和長子，[正德六年辛未科](../Page/正德_\(明朝\).md "wikilink")[狀元](../Page/狀元.md "wikilink")（也是明朝四川唯一一名状元）。官至[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")，以[議大禮忤世宗意](../Page/議大禮.md "wikilink")，謫戍[雲南](../Page/雲南.md "wikilink")，歿於是。在哲学、史学、天文、音乐等多领域都有较高的造诣，与[解缙](../Page/解縉.md "wikilink")、[徐渭合称](../Page/徐渭.md "wikilink")“明朝三才子”。谥文宪。\[26\]\[27\]\[28\]

## 特产

  - [新繁棕编](../Page/新繁棕编.md "wikilink")，入选第三批[国家级非物质文化遗产名录](../Page/中国国家级非物质文化遗产列表.md "wikilink")\[29\]
  - 新都柚，曾多次获得全国柚类“金杯奖”、农业博览会国家银奖\[30\]
  - 五香金钩豆腐干，曾于1915年荣获在旧金山[万国博览会上的金质奖章和奖状](../Page/万国博览会.md "wikilink")\[31\]
  - 杏仁甜豆瓣
  - 清流板鸭
  - 新繁泡菜\[32\]
  - 宝光禅茶\[33\]

## 外部連結

  - [成都市新都区人民政府门户网站](http://www.xindu.gov.cn/)

## 注释

## 参考文献

[新都区](../Category/新都区.md "wikilink")
[区](../Category/成都区县市.md "wikilink")
[蓉](../Category/四川市辖区.md "wikilink")

1.

2.

3.

4.
5.  陈习删等. 新都县志. 1928（中华民国17年）.

6.

7.
8.

9.

10.

11.

12.

13.

14.
15.

16.

17.
18.

19.

20.

21. [中华人民共和国国务院](../Page/中华人民共和国国务院.md "wikilink").
    國務院關於公布第四批全國重點文物保護單位的通知（國發〔1996〕47號）.
    1996-11-20

22.
23.

24. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷190）

25.
26.
27.

28. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷192）

29.

30.

31.

32.

33.