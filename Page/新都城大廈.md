[HK_North_Point_King_s_Road_Metropole_Building.JPG](https://zh.wikipedia.org/wiki/File:HK_North_Point_King_s_Road_Metropole_Building.JPG "fig:HK_North_Point_King_s_Road_Metropole_Building.JPG")

[HK_北角_North_Point_新都城大廈_Metropole_Building_name_sign_416-438_King's_Road_Jan-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_北角_North_Point_新都城大廈_Metropole_Building_name_sign_416-438_King's_Road_Jan-2013.JPG "fig:HK_北角_North_Point_新都城大廈_Metropole_Building_name_sign_416-438_King's_Road_Jan-2013.JPG")
**新都城大廈**（**Metropole
Building**）是[香港一個舊式私人屋苑](../Page/香港.md "wikilink")，位置前身是開業於1918年的[名園遊樂場的前半部份](../Page/名園.md "wikilink")，共分A、B、C、D四座，由著名香港建築師[司徒惠設計](../Page/司徒惠.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[北角](../Page/北角.md "wikilink")[英皇道](../Page/英皇道.md "wikilink")416至438號，鄰近[糖水道交界](../Page/糖水道.md "wikilink")，距離[港鐵](../Page/港鐵.md "wikilink")[北角站](../Page/北角站.md "wikilink")、[東區走廊出口](../Page/東區走廊.md "wikilink")、[香港電車北角總站](../Page/香港電車.md "wikilink")、[北角碼頭及其](../Page/北角碼頭.md "wikilink")[巴士總站都非常接近](../Page/巴士總站.md "wikilink")。

## 簡介

新都城大廈第一期（A、B座）於1967年入-{伙}-，第二期（C、D座）於1972年10月入-{伙}-，整個屋苑共分為4座，每座樓高25層，A，B
及 C 座公眾走廊相連相通，類似公共屋邨設計
。整個屋苑共有1,037個單位。地面一至三樓設有[新都城百貨公司](../Page/新都城百貨.md "wikilink")、[富臨酒家](../Page/富臨酒家.md "wikilink")、薩莉亞意式餐廳(
[Saizeriya Italian
Restaurant](../Page/Saizeriya_Italian_Restaurant.md "wikilink"))、[大家樂](../Page/大家樂.md "wikilink")，面積達16萬平方呎，商場地庫則有[惠康超級市場](../Page/惠康超級市場.md "wikilink")、[吉之島十元店](../Page/吉之島.md "wikilink")、[Play
House @ North Point](../Page/Play_House_@_North_Point.md "wikilink")
、[豐澤電器](../Page/豐澤電器.md "wikilink")、[Bossini](../Page/Bossini.md "wikilink")、[G2000](../Page/G2000.md "wikilink")、[鴻福堂](../Page/鴻福堂.md "wikilink")、[五十嵐日式拉面等商號](../Page/五十嵐日式拉面.md "wikilink")，新都城大廈是北角區歷史較長、規模較大和廣為人知的購物地點。

2010年末，新都城大廈展開屋苑有史以來最大規模的翻新工程，更換電梯，翻新大堂、走廊、喉管等設施，工程於2011年上旬完工。

## 六七暴動

[六七暴動期間](../Page/六七暴動.md "wikilink")，新都城大廈、[僑冠大廈和](../Page/僑冠大廈.md "wikilink")[明園大廈](../Page/明園大廈.md "wikilink")，被[港英警察當局視作](../Page/港英政府.md "wikilink")[左派暴亂的主要據點](../Page/左派.md "wikilink")，因其業主「大元置業公司」由[鬥委會委員](../Page/鬥委會.md "wikilink")[王寬誠所擁有](../Page/王寬誠.md "wikilink")。警方曾聯同英軍，於1967年8月4日空降該三幢大廈圍搜左派人士\[1\]。

## 外部參考

<references />

[Category:東區私人屋苑](../Category/東區私人屋苑.md "wikilink")
[Category:北角](../Category/北角.md "wikilink")
[Category:六七暴動](../Category/六七暴動.md "wikilink")

1.  [工商日報社論 1967年8月5日](https://1967riot.wordpress.com/2012/08/05/ksyp-19670805/)