**新金屬**（Nu Metal）属于[重金属音乐中的一个分支](../Page/重金属音乐.md "wikilink")。最早起源於90年代。

## 新金屬之發展與特色

1990年代，一些新派的重金屬樂隊把重金屬樂加入了不同的元素和演繹方式，如加入PUNK或HIP
HOP的節奏、[DJ混音](../Page/DJ.md "wikilink")、MIDI、說唱等，形成了新金屬(Nu-Metal)。

由於新金屬的編曲與彈奏技巧較傳統重金屬簡單許多，所以幾乎沒有吉他獨奏部份，整體而言新金屬更趨向大眾化與流行化。

## 攻占主流

1998年，[崆樂團](../Page/崆樂團.md "wikilink")（Korn）的第三張專輯《跟著領隊》（Follow the
Leader）在全球賣出900萬張，率領新金屬打進主流市場。隔年，許多樂團獲得電台的播放機會，[MTV也開始強力放送](../Page/MTV.md "wikilink")。該年的暢銷樂團有[盲音樂團](../Page/盲音樂團.md "wikilink")（Deftones）、[礦坑樂團](../Page/礦坑樂團.md "wikilink")（Coal
Chamber）、[林普巴茲提特](../Page/林普巴茲提特.md "wikilink")（Limp
Bizkit）與[史丹樂團](../Page/史丹樂團.md "wikilink")（Staind）。

許多第一波新金屬浪潮的樂團都來自[洛杉磯地區](../Page/洛杉磯.md "wikilink")，他們在相同場地表演並且互相認識。這些樂團包括了[靜止X樂團](../Page/靜止X樂團.md "wikilink")（Static-X）、[礦坑樂團和](../Page/礦坑樂團.md "wikilink")[脊椎小腿](../Page/脊椎小腿.md "wikilink")（Spineshank）。來自[洛杉磯以外的樂團有](../Page/洛杉磯.md "wikilink")[狄蒙的](../Page/狄蒙.md "wikilink")[滑結樂團](../Page/滑結樂團.md "wikilink")（Slipknot）、[亞特蘭大的](../Page/亞特蘭大.md "wikilink")[血海七塵](../Page/血海七塵.md "wikilink")（Sevendust）、[傑克遜維爾的](../Page/傑克遜維爾_\(佛羅里達州\).md "wikilink")[林普巴茲提特](../Page/林普巴茲提特.md "wikilink")、[芝加哥的](../Page/芝加哥.md "wikilink")[騷動樂團](../Page/騷動樂團.md "wikilink")（Disturbed）、[鳳凰城的](../Page/鳳凰城.md "wikilink")[飛靈樂團](../Page/飛靈樂團.md "wikilink")（Soulfly）和[勞倫斯的](../Page/勞倫斯_\(麻薩諸塞州\).md "wikilink")[老天發威](../Page/老天發威.md "wikilink")（Godsmack）。

[Family Values
Tour](../Page/:en:Family_Values_Tour.md "wikilink")、[Lollapalooza和](../Page/:en:Lollapalooza.md "wikilink")[Ozzfest也促成了新金屬的崛起](../Page/:en:Ozzfest.md "wikilink")。[胡士托音樂節30周年也有新金屬樂團參與](../Page/胡士托音樂節.md "wikilink")。
FUC 世紀交會之際，更多的樂團破繭而出，像是[蟑螂老爹](../Page/蟑螂老爹.md "wikilink")（Papa
Roach）的主流首張專輯《橫行霸道》（Infest）成為[白金唱片](../Page/白金唱片.md "wikilink")。[花錢找死](../Page/P.O.D.md "wikilink")（P.O.D）和[騷動樂團也成功打進主流市場](../Page/騷動樂團.md "wikilink")。2001年，隨著各家唱片公司陸續簽下樂團，新金屬達到高峰。雖然不斷有新樂團興起，但促成新金屬曲風的元老級樂團們的作品都獲得優異成績，像是[史丹樂團的](../Page/史丹樂團.md "wikilink")《打破循環》（Break
the
Cycle）、[花錢找死的](../Page/P.O.D.md "wikilink")《衛星》（Satellite）、[滑結樂團的](../Page/滑結樂團.md "wikilink")《愛荷華》（Iowa），[聯合公園](../Page/聯合公園.md "wikilink")（Linkin
Park）的《[混合理論](../Page/混合理論.md "wikilink")》（Hybrid Theory）是該年最暢銷專輯。

2002年，新金屬逐漸退燒。[崆樂團睽違已久的第五張專輯](../Page/崆樂團.md "wikilink")《不要惹我》（Untouchables）和[蟑螂老爹第三張專輯](../Page/蟑螂老爹.md "wikilink")《愛.恨.悲劇》（Lovehatetragedy）都未達過過去的銷售水準。新金屬[樂團逐漸淡出搖滾廣播電台](../Page/樂團.md "wikilink")，[MTV也將焦點轉移至](../Page/MTV.md "wikilink")[流行龐克與](../Page/流行龐克.md "wikilink")[情緒核樂團](../Page/情緒核.md "wikilink")。之後，部分樂團將曲風改變為[硬搖滾或](../Page/硬搖滾.md "wikilink")[重金屬](../Page/重金屬音樂.md "wikilink")。

## 音樂特色

新金屬主唱通常採取具有侵略性的唱腔，包括有清腔、喉音嘶吼、[重金屬與](../Page/重金屬音樂.md "wikilink")[硬蕊龐克的各式吼腔](../Page/硬蕊龐克.md "wikilink")，有時也如[放克金屬使用饒舌](../Page/放克金屬.md "wikilink")。

[崆樂團的](../Page/崆樂團.md "wikilink")[Jonathan
Davis](../Page/:en:Jonathan_Davis.md "wikilink")、[聯合公園的](../Page/聯合公園.md "wikilink")[麥克·信田與](../Page/麥克·信田.md "wikilink")[查斯特·班寧頓](../Page/查斯特·班寧頓.md "wikilink")、[雪菲爾樂團](../Page/雪菲爾樂團.md "wikilink")（Chevelle）的[Pete
Loeffler](../Page/:en:Pete_Loeffler.md "wikilink")、[滑結樂團的](../Page/滑結樂團.md "wikilink")[Corey
Taylor](../Page/:en:Corey_Taylor.md "wikilink")、[主力樂團](../Page/主力樂團.md "wikilink")（Taproot）的
Stephen Richards、[騷動樂團](../Page/騷動樂團.md "wikilink")（Disturbed）的[David
Draiman和](../Page/:en:David_Draiman.md "wikilink")[林普巴茲提特的](../Page/林普巴茲提特.md "wikilink")[Fred
Durst都表示受到](../Page/:en:Fred_Durst.md "wikilink")[工具樂團](../Page/工具樂團.md "wikilink")（Tool）主唱
[Maynard James Keenan](../Page/:en:Maynard_James_Keenan.md "wikilink")
的影響，Fred
Durst也說[工具樂團是影響他最大](../Page/工具樂團.md "wikilink")、也是他最喜愛的樂團。\[1\]
[不再信仰](../Page/不再信仰.md "wikilink")（Faith No More）的[Mike
Patton](../Page/:en:Mike_Patton.md "wikilink") 也是影響新金屬的重要人物。

新金屬的[貝斯常常讓人聯想起](../Page/電貝斯.md "wikilink")[嘻哈或](../Page/嘻哈.md "wikilink")[放克](../Page/放克.md "wikilink")。在有些歌曲中，貝斯手會使用[slap的技巧](../Page/:en:slapping.md "wikilink")，為歌曲增添放克節奏。許多新金屬貝斯手會使用五弦貝斯取代常見的四弦貝斯。

許多新金屬樂團會使用[DJ來增加音樂元素](../Page/DJ.md "wikilink")（像是音樂取樣、刮盤、電子背景）。代表人物有[滑結樂團的](../Page/滑結樂團.md "wikilink")[Sid
Wilson](../Page/:en:Sid_Wilson.md "wikilink")、[聯合公園的](../Page/聯合公園.md "wikilink")[Mr.
Hahn和](../Page/:en:Joe_Hahn.md "wikilink")[林普巴茲提特的](../Page/林普巴茲提特.md "wikilink")[DJ
Lethal](../Page/:en:DJ_Lethal.md "wikilink")。

## 比較有代表性的新金屬樂團

  - [林普巴茲提特](../Page/林普巴茲提特.md "wikilink")
  - [花錢找死](../Page/P.O.D.md "wikilink")
  - [墮落體制](../Page/墮落體制.md "wikilink")
  - [滑結樂團](../Page/滑結樂團.md "wikilink")
  - [崆樂團](../Page/崆樂團.md "wikilink")
  - [洛斯帕飛](../Page/洛斯帕飛樂團.md "wikilink")
  - [伊凡塞斯](../Page/伊凡塞斯.md "wikilink")
  - [討伐體制樂團](../Page/討伐體制樂團.md "wikilink")
  - 騷動樂團
  - 礦坑樂團
  - 超鈾元素
  - 聯合公園

## 註釋

[Category:重金屬音樂](../Category/重金屬音樂.md "wikilink")

1.  [Biography](http://freddurst.com/bio)  on Fred Durst's website