[Yacht_The_World_Is_Not_Enough,_James_Bond_007,_2000.jpg](https://zh.wikipedia.org/wiki/File:Yacht_The_World_Is_Not_Enough,_James_Bond_007,_2000.jpg "fig:Yacht_The_World_Is_Not_Enough,_James_Bond_007,_2000.jpg")

**《黑日危机》**（）是一部於[1999年上映的](../Page/1999年电影.md "wikilink")[英国电影](../Page/英国电影.md "wikilink")，是[詹姆斯·邦德系列电影的第](../Page/詹姆斯·邦德系列电影.md "wikilink")19部，也是第3部由男演员[皮尔斯·布鲁斯南扮演英国](../Page/皮尔斯·布鲁斯南.md "wikilink")[秘密情报局](../Page/秘密情报局.md "wikilink")[间谍](../Page/间谍.md "wikilink")[詹姆斯·邦德的电影](../Page/詹姆斯·邦德.md "wikilink")。本片由迈克尔·艾普特执导，故事和剧本由尼尔·珀维斯、罗伯特·韦德和布鲁斯·菲尔斯坦创作\[1\]，迈克尔·G·威尔森和芭芭拉·布洛柯里担任制片人，片名来自《[007之女王密使](../Page/007之女王密使.md "wikilink")》原著小说中的一句台词。

电影的剧情主要讲述亿万富翁罗伯特·金爵士被[恐怖分子雷纳德](../Page/恐怖分子.md "wikilink")[暗杀](../Page/暗杀.md "wikilink")，邦德随即被派前往保护金的女儿艾莉卡，后者之前曾被雷纳德绑架向金勒索。但邦德之后发现艾莉卡并不像上司M所想像的那么简单，而且恐怖分子的目标是要在[伊斯坦堡水域引发](../Page/伊斯坦堡.md "wikilink")[堆芯熔毁](../Page/堆芯熔毁.md "wikilink")。

电影的拍摄地点包括[西班牙](../Page/西班牙.md "wikilink")、[法国](../Page/法国.md "wikilink")、[阿塞拜疆](../Page/阿塞拜疆.md "wikilink")、[土耳其和](../Page/土耳其.md "wikilink")[英国](../Page/英国.md "wikilink")，内景镜头大多在拍摄。虽然得到的专业评价褒贬不一，但商业上获得了成功，全球票房达3.61億[美元](../Page/美元.md "wikilink")\[2\]。《黑日危机》也是拍摄的詹姆斯·邦德系列电影中第一部由[米高梅公司正式发行的](../Page/米高梅公司.md "wikilink")，之前的都是由[联美公司发行](../Page/联美公司.md "wikilink")。

## 剧情

[秘密情報局间谍詹姆斯](../Page/秘密情報局.md "wikilink")·邦德与一位[瑞士](../Page/瑞士.md "wikilink")[银行家会面](../Page/银行家.md "wikilink")，目的是帮上司M的一位朋友，英国[石油](../Page/石油.md "wikilink")[大亨罗伯特](../Page/大亨.md "wikilink")·金爵士找回一笔钱。邦德告诉对方这笔钱是用来买回一份从秘密情报局盗走的报告，行事的间谍已遭灭口，所以邦德想知道是谁杀了他。银行家试图杀死邦德，但邦德制服了他，可银行家还没来得及说出刺客的名字就被伪装成他女助手的杀手所杀。不过邦德带着钱成功脱身。

罗伯特爵士来到秘密情报局取回自己的钱，却被藏在钱中的诱杀装置所杀。邦德又看到杀死银行家的女助手身在[泰晤士河的一艘快艇上](../Page/泰晤士河.md "wikilink")，双方展开追逐，杀手到达[千禧巨蛋后试图乘热气球逃跑](../Page/千禧巨蛋.md "wikilink")，但被邦德阻止，邦德愿意提供保护来换取情报，但她拒绝并引爆气球自杀。

邦德从案发现场找到余下的钱并追踪到一个叫雷诺德的恐怖分子，此人以前是[克格勃间谍](../Page/克格勃.md "wikilink")，秘密情报局曾试图取其性命，虽然没有成功，但还是在他大脑中留下一颗[子弹](../Page/子弹.md "wikilink")，这个子弹逐渐摧毁了他的感官，让他感觉不到疼痛。M派邦德保护金的女儿艾莉卡，雷诺德之前曾绑架她索要赎金，秘密情报局相信他有可能故技重演。艾莉卡正在[阿塞拜疆督导石油运输管道的建设](../Page/阿塞拜疆.md "wikilink")，邦德到达后两人在山上参观管道的拟建路线，但遭到一伙配备有[滑翔伞和](../Page/滑翔伞.md "wikilink")[雪上摩托車的全副武装攻击小队的袭击](../Page/雪上摩托車.md "wikilink")。

之后邦德通过线人得知艾莉卡的保安主管大卫多夫其实和雷诺德是一伙的。杀死此人后，邦德根据线索追查到位于[哈萨克斯坦的一个](../Page/哈萨克斯坦.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[洲際彈道飛彈基地](../Page/洲際彈道飛彈.md "wikilink")。邦德假扮成一位俄国核科学家，认识了美国核物理学家克丽斯玛斯·琼斯并一起进入基地的发射井。他们发现雷诺德从一个核弹中取出[全球定位卡和武器级的](../Page/全球定位系统.md "wikilink")[钚](../Page/钚.md "wikilink")。但由于琼斯发出的声音被雷诺德发现，邦德没能杀死对方。雷诺德带着钚逃脱并启动了炸弹，不过邦德和琼斯也带着核弹的全球定位卡成功脱身。

回到阿塞拜疆后，邦德告诉M自己觉得艾莉卡可能并不像看起来那么单纯，但M没有相信他的判断。全球定位卡的警报表明从哈萨克斯坦被偷的核弹正在通过检修管道朝石油码头逼近，邦德和琼斯进入管道试图拆除核弹，后者发现上面大部分的钚都不见了。两人在核弹爆炸前成功跳出了管道，一大段管道被炸毁，邦德和琼斯两人被推定已经死亡。艾莉卡于是绑架了M，表明自己设计杀死了父亲，以此报复用她做诱饵试图抓捕雷诺德的做法。

邦德来到[里海地区一家](../Page/里海.md "wikilink")[鱼子酱工厂找到前俄罗斯黑手党老大祖科夫斯基](../Page/鱼子酱.md "wikilink")，一行人又遭到艾莉卡派来的[直升机袭击](../Page/直升机.md "wikilink")。之后从祖科夫斯基口中邦德得知艾莉卡需要使用一艘由祖科夫斯基外甥掌管的[潜艇](../Page/潜艇.md "wikilink")。邦德和祖科夫斯基推测雷诺德和艾莉卡计划在这艘目前正在[伊斯坦堡的潜艇上引爆核弹](../Page/伊斯坦堡.md "wikilink")，不但伊斯坦堡会被摧毁，俄罗斯在[博斯普鲁斯海峡的石油管道也会严重受损](../Page/博斯普鲁斯海峡.md "wikilink")，这样艾莉卡没有通过伊斯坦堡的管道和石油就将大幅升值。邦德刚通过定位卡发现核弹位置，祖科夫斯基手下的金牙保镖布利昂就炸毁了指挥中心，祖科夫斯基炸晕了过去，邦德和琼斯被艾莉卡的爪牙抓获。琼斯被带到已经被雷诺德爪牙抢占的潜艇，邦德则被带到一个塔上，受到艾莉卡的折磨。祖科夫斯基和手下赶来并攻进塔内，但祖科夫斯基被艾莉卡用枪击中，临死前他用自己手杖裡的枪帮助邦德脱身，邦德于是杀死了艾莉卡并救出M。

邦德潜入潜艇救出琼斯。最后的大战中潜艇下沉并撞在博斯普鲁斯海峡底部破裂，邦德成功杀死了雷诺德并和琼斯一起逃出潜艇，反应堆也在水下安全地引爆了。

## 演员

  - [皮尔斯·布鲁斯南饰詹姆斯](../Page/皮尔斯·布鲁斯南.md "wikilink")·邦德

  - [丹妮丝·理查兹饰克丽斯玛斯](../Page/丹妮丝·理查兹.md "wikilink")·琼斯（），一位协助邦德的[核物理学家](../Page/原子核物理学.md "wikilink")\[3\]。理查兹表示自己喜欢这个角色，因为她“有头脑”、“稳重”，而且“和以前数十年的[邦德女郎相比](../Page/邦德女郎.md "wikilink")，这个角色有深度”\[4\]

  - [羅勃·卡萊爾饰雷诺德](../Page/羅勃·卡萊爾.md "wikilink")（）：电影中的两个大反派之一，曾是克格勃间谍但之后变成了[恐怖主义者](../Page/恐怖主义.md "wikilink")，由于脑子里有一颗子弹而感觉不到疼痛。

  - [苏菲·玛索饰艾莉卡](../Page/苏菲·玛索.md "wikilink")·金（）：电影中的两个大反派之一，石油大亨罗伯特·金爵士的女儿，被雷诺德绑架后患上[斯特哥尔摩综合症并爱上了对方](../Page/斯特哥尔摩综合症.md "wikilink")，试图利用核污染让自己垄断石油管道运输市场。

  - [茱蒂·丹契饰M](../Page/茱蒂·丹契.md "wikilink")，秘密情报局主管，詹姆斯·邦德的顶头上司。

  - [戴斯蒙·李維林饰](../Page/戴斯蒙·李維林.md "wikilink")[Q](../Page/Q先生.md "wikilink")，秘密情报局的军需官，给邦德提供多用途车辆和各种先进、精密武器及装备。

  - [约翰·克里斯饰Q的助手](../Page/约翰·克里斯.md "wikilink")，之后也成为新的Q。

  - 饰M的秘书曼妮潘妮小姐（）。

  - [乌尔里奇·汤姆森饰大卫多夫](../Page/乌尔里奇·汤姆森.md "wikilink")（），艾莉卡·金在阿塞拜疆的保安主管。

  - [罗彼·考特拉尼饰瓦伦汀](../Page/羅比·科特瑞恩.md "wikilink")·祖科夫斯基（），前[俄羅斯黑手黨老大和](../Page/俄羅斯黑手黨.md "wikilink")[巴库赌场老板](../Page/巴库.md "wikilink")。他协助邦德来营救自己被雷诺德控制的外甥。

  - [科林·萨蒙饰查尔斯](../Page/科林·萨蒙.md "wikilink")·罗宾逊（，秘密情报局参谋长。

  - 饰莫莉·沃恩弗拉什医生（），秘密情报局医生。

  - 饰加伯尔（），艾莉卡·金的保镖。

  - 饰布利昂（），瓦伦汀·祖科夫斯基的金牙保镖。

  - 饰雷诺德手下一名经验丰富的杀手，片头杀死银行家的人，演员表上显示的名字是“雪茄女”（）。

  - 饰罗伯特·金爵士，艾莉卡的父亲，也是M的朋友，英国石油大亨。

## 制作

和[彼得·杰克逊都曾有过](../Page/彼得·杰克逊.md "wikilink")[导演本片的机会](../Page/导演.md "wikilink")。芭芭拉·布洛柯里很喜欢后者执导的《》，不过不喜欢之后的《[恐怖幽灵](../Page/恐怖幽灵.md "wikilink")》，所以也就没有表现出和杰克逊合作的意图。杰克逊一直都是邦德的粉丝，他指出制片公司往往倾向于选择不是很有名的导演，所以自己完成《[魔戒電影三部曲](../Page/魔戒電影三部曲.md "wikilink")》后也不大可能会有机会再执导一部邦德电影了\[5\]。

[ChathamHDSubmarine0083.JPG](https://zh.wikipedia.org/wiki/File:ChathamHDSubmarine0083.JPG "fig:ChathamHDSubmarine0083.JPG")级潜艇\]\]

《黑日危机》在正式片名和字幕出现前的镜头有14分钟长，截止2012年上映的《007：大破天幕杀机》，本片仍然保持着最长的纪录。在电影发行的旗舰版[DVD中有一段拍摄纪录片](../Page/DVD.md "wikilink")，导演迈克尔·艾普特表示这部镜头起初并不是这样安排的，但由于和以前的邦德电影对比来看，字幕前的内容显得不够精彩，所以字幕的部分推迟到了现在的位置，造就了这样一段最长的邦德电影字幕前桥段纪录。《[每日电讯报](../Page/每日电讯报.md "wikilink")》曾报道称英国政府出于安全上的顾虑而没有同意让部分镜头在真正的秘密情报局总部前拍摄。不过一位[外交事务部发言人否认了这一说法](../Page/外交及聯邦事務部.md "wikilink")，并表示对这篇文章感到不满\[6\]。

电影起初计划于2000年发行，有传言称片名为《邦德2000》（）。另外还有传言称片名为《死神不等人》（）、《火与冰》（）、《压力点》（）等\[7\]。英文片名“”是英语对[拉丁文短句](../Page/拉丁文.md "wikilink")“Orbis
non-sufficit”的译文，后者在现实生活中是的格言。在电影《007之女王密使》及其原著小说中，这也被描述为邦德家族的格言。这句话又源自于[亚历山大大帝的墓志铭](../Page/亚历山大大帝.md "wikilink")\[8\]。

编剧尼尔·珀维斯和罗伯特·韦德完成《》后获聘编写本片的剧本\[9\]。达娜·史蒂文斯（）对剧本作了一些修改，但她的名字没有列入编剧名单中，之后再由布鲁斯·菲尔斯坦接手\[10\]。

### 拍摄

[PierceBMW99.jpg](https://zh.wikipedia.org/wiki/File:PierceBMW99.jpg "fig:PierceBMW99.jpg")Z8\]\]

电影标题字幕前的段落起始于西班牙的[毕尔巴鄂](../Page/毕尔巴鄂.md "wikilink")，并在[毕尔巴鄂古根海姆美术馆取景](../Page/毕尔巴鄂古根海姆美术馆.md "wikilink")。然后转战[伦敦](../Page/伦敦.md "wikilink")，展示了秘密情报局总部大楼和泰晤士河上的[千禧巨蛋](../Page/千禧巨蛋.md "wikilink")\[11\]。片头字幕结束后，剧组用[苏格兰的](../Page/苏格兰.md "wikilink")[艾琳多南堡作为秘密情报局总部进行拍摄](../Page/艾琳多南堡.md "wikilink")。其它取景地点还包括阿塞拜疆的巴库、土耳其的伊斯坦堡等\[12\]。电影的大部分内景镜头是在松林制片厂拍摄的。运河中追逐的镜头也是在英国拍摄\[13\]。片中秘密情报局的临时运作中心是在[爱莲·朵娜岛上取景拍摄的](../Page/爱莲·朵娜.md "wikilink")，而滑雪追逐的桥段则是在[法国的](../Page/法国.md "wikilink")[霞慕尼摄制](../Page/霞慕尼.md "wikilink")\[14\]。后者的镜头拍摄还因为[雪崩而延迟](../Page/雪崩.md "wikilink")，剧组还协助了救援工作\[15\]。

电影中位于哈萨克斯坦的核设施实际上是在西班牙的[纳瓦拉拍摄的](../Page/纳瓦拉.md "wikilink")，而炼油厂控制中心则是位于[威尔特郡](../Page/威尔特郡.md "wikilink")[斯温顿的一幢](../Page/斯温顿.md "wikilink")[摩托罗拉办公楼](../Page/摩托罗拉.md "wikilink")\[16\]。电影中的输油管道外景在[威尔士的](../Page/威尔士.md "wikilink")[史諾多尼亞拍摄](../Page/史諾多尼亞.md "wikilink")，水下潜艇的镜头是在[巴哈马摄制的](../Page/巴哈马.md "wikilink")\[17\]。

片中皮尔斯·布鲁斯南开的[宝马Z](../Page/宝马.md "wikilink")8是电影公司与宝马达成的三部电影推广协议的最后一部分，之前已经在《[新鐵金剛之金眼睛](../Page/新鐵金剛之金眼睛.md "wikilink")》中使用了Z3，并在《[新鐵金剛之明日帝國](../Page/新鐵金剛之明日帝國.md "wikilink")》中使用了750iL。由于电影发行时Z8还没有上市，所以片中的多个模型都是專門为影片的拍摄而制作的\[18\]。

### 音乐

[大衛·阿諾德为](../Page/大衛·阿諾德.md "wikilink")《黑日危机》谱写了配乐，这也是他第二次为邦德电影谱曲\[19\]。阿诺德打破了以重复开场主题来结束电影的传统，而是以一首歌来作片尾曲。起初他选择了《只能怪我自己》（）作片尾曲，但导演艾普特没有接受，但是改用混音变奏的詹姆斯·邦德主题音乐代替\[20\]。《只能怪我自己》由阿诺德和唐·布莱克（）创作，斯科特·沃克（）演唱，收录在电影原声带唱片中，是其中的第19首，也是最后一首曲目，其旋律是艾莉卡·金的主题音乐。唱片中还有另外三段曲目：《赌场》（）、《艾莉卡的主题》（）和《我从不失手》（）里面也可以听到这个主题\[21\]。

  - 曲目列表\[22\]：

<!-- end list -->

1.  《》 3:57
2.  《》 1:27
3.  《》 5:19
4.  《》 1:33
5.  《》 1:31
6.  《》 1:42
7.  《》 2:57
8.  《》 3:42
9.  《》 2:06
10. 《》 3:00
11. 《》 6:27
12. 《》 4:15
13. 《》 2:45
14. 《》 6:01
15. 《》 2:22
16. 《》 3:32
17. 《》 10:20
18. 《》 1:28
19. 《》 3:37

电影的同名主题曲《》由大卫·阿诺德和唐·布莱克创作，[垃圾合唱團表演](../Page/垃圾合唱團.md "wikilink")，这是布莱克参与创作的第5首邦德主题歌，他之前还曾为《[007之霹雳弹](../Page/007之霹雳弹.md "wikilink")》\[23\]、《[007之金刚钻](../Page/007之金刚钻.md "wikilink")》\[24\]、《[007之金枪人](../Page/007之金枪人.md "wikilink")》\[25\]和《[007之明日帝国](../Page/007之明日帝国.md "wikilink")》\[26\]创作过主题音乐。垃圾合唱团也表演了电影中一个追逐戏段的配乐曲目《》。2006年末，[IGN网站将](../Page/IGN.md "wikilink")《》在所有邦德电影主题曲中排在第9名\[27\]。2012年，网站把这首歌评为所有邦德电影歌曲的第2位，仅次于《[007之金手指](../Page/007之金手指.md "wikilink")》\[28\]。这首歌还登上了两个“1999年最佳”名单，在[89X的](../Page/89X.md "wikilink")“1999年最优秀的89首歌曲”中名列第87位\[29\]，还在Q101的“1999年最优秀的101首歌曲”中名列第100位\[30\]。

## 发行和反响

《黑日危机》于1999年11月19日在美国[首映](../Page/首演.md "wikilink")，1999年11月26日在英国上映\[31\]。当时米高梅和MTV公司签订了一份市场推广合作计划，主要针对的是美国青少年，并认为这一目标观众群会将邦德视为“老一辈的秘密间谍”。结果MTV在电影发行后马上播放了超过100[小时与邦德有关的节目](../Page/小时.md "wikilink")，其中大部分都以丹妮丝·理查兹为卖点\[32\]。

影片上映首周就以3551万9007美元的收入登上北美电影票房排列榜冠军宝座，最终在美国进账1亿2694万3684美元，全球票房3亿6183万2400美元\[33\]，创下了詹姆斯·邦德电影票房的新纪录，直到《[新鐵金剛之不日殺機](../Page/新鐵金剛之不日殺機.md "wikilink")》上映后才被超越\[34\]。电影还参加[第72届奥斯卡金像奖](../Page/第72届奥斯卡金像奖.md "wikilink")[视觉效果奖的第一轮角逐](../Page/奥斯卡最佳视觉效果奖.md "wikilink")，但没能获得提名\[35\]。《黑日危机》获得了[土星獎最佳动作](../Page/土星獎.md "wikilink")/历险/惊悚电影奖提名，皮尔斯·布鲁斯南赢得了帝国奖和大片娱乐奖的最佳男演员奖，大卫·阿诺德获得了一项BMI电影音乐奖。不过，丹妮丝·理查兹被授予了1999年[金酸莓奖最差女配角奖](../Page/金酸莓奖.md "wikilink")，让本片成为第一部获得这一奖项“肯定”的邦德系列电影。理查兹和布鲁斯南还被提名“最差银幕情侣”\[36\]。

《黑日危机》于2000年5月16日发行了[DVD](../Page/DVD.md "wikilink")，其中包含《007的秘密》（）花絮内容，电影拍摄的纪录片，两条评论音轨——一条由导演解说，另一条由艺术指导、助理导演和作曲家解说，还有同名游戏的预告片和垃圾合唱团的音乐视频\[37\]。2006年发行的旗舰版DVD中包括一部名为《邦德鸡尾酒》（）的2000年纪录片，皮尔斯·布鲁斯南在[香港的一场记者招待会](../Page/香港.md "wikilink")、删除镜头以及向[戴斯蒙·李維林致敬的段落](../Page/戴斯蒙·李維林.md "wikilink")\[38\]。

电影获得的评价褒贬不一。根据[烂蕃茄上收集的](../Page/烂蕃茄.md "wikilink")120篇评论文章，其中61篇给出了“新鲜”的正面评价，“新鲜度”为51%，平均得分5.7（最高10分）\[39\]。而在[Metacritic上收集的](../Page/Metacritic.md "wikilink")33篇评论中则有18篇给出好评，1篇差评，14篇褒贬不一，平均得分59（满分100）\[40\]。《[芝加哥太陽報](../Page/芝加哥太陽報.md "wikilink")》影评人[罗杰·埃伯特给出了三星半的评价](../Page/罗杰·埃伯特.md "wikilink")（最高四星），认为影片精彩纷呈，刺激而优雅，并且不断给人惊喜\[41\]。而对电影的批评则主要针对于剧情的处理和动作场面过多上\[42\]，《[娱乐周刊](../Page/娱乐周刊.md "wikilink")》认为这是史上最差的邦德电影，认为其剧情是“如此令人费解，甚至皮尔斯·布鲁斯南也要承认感到迷惑”\[43\]。[MSN的诺曼](../Page/MSN.md "wikilink")·威尔纳（）认为这不是最差，而是第3差，另外两部更差的分别是《[鐵金剛勇戰大狂魔](../Page/鐵金剛勇戰大狂魔.md "wikilink")》和《[鐵金剛勇戰殺人狂魔](../Page/鐵金剛勇戰殺人狂魔.md "wikilink")》\[44\]。而[IGN网站则认为这是史上第](../Page/IGN.md "wikilink")5烂的邦德电影\[45\]。

评论家批评让理查兹来扮演一个核科学家实在缺乏可信度\[46\]\[47\]。她的服装——背心和短裤也招来了类似的反应\[48\]。2008年，《[娱乐周刊](../Page/娱乐周刊.md "wikilink")》将丹妮丝·理查兹评为史上最糟糕的[邦德女郎之一](../Page/邦德女郎.md "wikilink")\[49\]。

## 改编

邦德小说家根据电影的剧本创作了同名改编小说，部分细节有所改动，比如邦德的武器仍然是[瓦爾特PP手槍而不是更新款的P](../Page/瓦爾特PP手槍.md "wikilink")99等\[50\]。

2000年，[美国艺电根据本片创作了基于](../Page/美国艺电.md "wikilink")[任天堂64和](../Page/任天堂64.md "wikilink")[PlayStation平台的同名](../Page/PlayStation.md "wikilink")[第一人称射击游戏](../Page/第一人称射击游戏.md "wikilink")。不过之后针对[个人电脑和](../Page/个人电脑.md "wikilink")[PlayStation
2版本的发布计划予以取消](../Page/PlayStation_2.md "wikilink")，这些版本本有望使用[Id Tech
3](../Page/Id_Tech_3引擎.md "wikilink")[游戏引擎](../Page/游戏引擎.md "wikilink")\[51\]\[52\]。

## 参见

  - [詹姆斯·邦德系列电影](../Page/詹姆斯·邦德系列电影.md "wikilink")
  - [斯特哥尔摩综合症](../Page/斯特哥尔摩综合症.md "wikilink")

## 参考文献

## 外部链接

  - [米高梅上《黑日危机》的官方主页](http://www.mgm.com/view/Movie/231/The-World-Is-Not-Enough/)

  - [“秘密情报局”粉丝站点，上面有许多电影细节信息](https://web.archive.org/web/20090109052943/http://www.mi6.co.uk//sections/movies/twine.php3)

  - [DVD评论](https://web.archive.org/web/20120205095737/http://www.thedigitalbits.com/reviews/worldisnotenough.html)

  - [《黑日危机》拍摄地点的地图和方向](http://www.movietourguide.com/World_Is_Not_Enough,_The/filming_locations)

  -
  -
  -
  -
  -
  - {{@movies|fWatm0884130}}

  -
  -
[Category:1999年電影](../Category/1999年電影.md "wikilink")
[Category:詹姆士·龐德電影](../Category/詹姆士·龐德電影.md "wikilink")
[Category:1990年代動作片](../Category/1990年代動作片.md "wikilink")
[Category:蘇格蘭背景電影](../Category/蘇格蘭背景電影.md "wikilink")
[Category:土耳其取景电影](../Category/土耳其取景电影.md "wikilink")
[Category:米高梅電影](../Category/米高梅電影.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:西班牙背景電影](../Category/西班牙背景電影.md "wikilink")
[Category:伊斯坦布尔背景电影](../Category/伊斯坦布尔背景电影.md "wikilink")
[Category:倫敦背景電影](../Category/倫敦背景電影.md "wikilink")
[Category:1999年小說](../Category/1999年小說.md "wikilink")
[Category:英國電影作品](../Category/英國電影作品.md "wikilink")

1.

2.
3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.
15.

16.

17.

18.
19.

20.
21.

22.
23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51. Black Ops had previously adapted *Tomorrow Never Dies* for the
    PlayStation and would go on to develop *Nightfire* in 2002.

52.