**新党日本**（****）是[日本一个](../Page/日本.md "wikilink")[政党](../Page/政党.md "wikilink")，由反对“[邮政民营化改革相关法案](../Page/日本郵政民營化.md "wikilink")”的[日本自由民主党议员](../Page/日本自由民主党.md "wikilink")[小林兴起等](../Page/小林兴起.md "wikilink")4人和[长野县知事](../Page/长野县.md "wikilink")[田中康夫于](../Page/田中康夫.md "wikilink")2005年8月21日宣布成立，代表（黨魁）一職由田中康夫担任。但新党日本於成立不久後舉行的[2005年日本大选中選情惨败](../Page/2005年日本大选.md "wikilink")，仅1人当选[眾議員](../Page/眾議員.md "wikilink")，而不得不与[国民新党在](../Page/国民新党.md "wikilink")[眾議院共同组成联合会派集体活动](../Page/日本眾議院.md "wikilink")。於[2012年眾議院選舉落敗](../Page/第46屆日本眾議院議員總選舉.md "wikilink")，喪失唯一議席及政黨資格，現以政治團體名義繼續活動。

## 外部連結

  - [官方網站](http://www.nippon-dream.com/)
  - 「[チーム・ニッポン](http://www.team-nippon.com/index.html)」（平山誠が「編集長」を務める新党日本の政策立案団体だった。かつては新党日本公式サイトからもリンクされていたが、2010年現在はリンクされていない。）


[Category:日本政黨](../Category/日本政黨.md "wikilink")
[Category:2005年建立的政黨](../Category/2005年建立的政黨.md "wikilink")
[Category:東京都組織](../Category/東京都組織.md "wikilink")