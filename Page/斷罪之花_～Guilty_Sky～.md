《**斷罪之花 ～Guilty
Sky～**》是[日本女](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[BeForU成員](../Page/BeForU.md "wikilink")[小坂梨由的第](../Page/小坂梨由.md "wikilink")3枚[單曲](../Page/單曲.md "wikilink")。

## 概要

  - 2007年5月30日[avex
    mode以](../Page/avex_mode.md "wikilink")「CD」和「CD+DVD」2種形式發售。在DVD版「斷罪之花
    〜Guilty
    Sky〜」中收錄了音樂片斷。這是繼「[true...](../Page/true....md "wikilink")」、「[大和撫子魂](../Page/大和撫子魂.md "wikilink")」之後小坂梨由的第3首單曲。
  - 這首曲子是[日本電視台聯播網](../Page/日本電視台.md "wikilink")[動畫](../Page/動畫.md "wikilink")《[大劍](../Page/大劍.md "wikilink")》的片尾曲。作為歌詞的作者，小坂坦言受到登場人物中泰莉莎故事的影響。還有小坂是該作品的FANS之一。

## 收錄内容

### CD

1.  斷罪之花 ～Guilty Sky～
      -
        作詞：小坂梨由 / 作曲：[LOVE+HATE](../Page/LOVE+HATE.md "wikilink") /
        編曲：鳴瀬シュウヘイ・LOVE+HATE・[中川幸太郎](../Page/中川幸太郎.md "wikilink")
        [日本電視台動畫](../Page/日本電視台.md "wikilink")「[大劍](../Page/大劍.md "wikilink")」片尾曲
2.  ignore
      -
        作詞：小坂梨由 / 作曲・編曲：Ryo
3.  DANZAI NO HANA ～Guilty Sky～
4.  斷罪之花 ～Guilty Sky～ instrumental
5.  ignore instrumental

### DVD

1.  斷罪之花 ～Guilty Sky～（Music Clip）

[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:BeForU](../Category/BeForU.md "wikilink")
[Category:日本電視台動畫主題曲](../Category/日本電視台動畫主題曲.md "wikilink")