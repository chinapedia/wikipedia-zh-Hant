[缩略图](https://zh.wikipedia.org/wiki/File:Kai_Yuan_Tong_Bao_-_Dr._Luke_Roberts.jpg "fig:缩略图")
**方孔钱**，又称「**孔方钱**」、「**圆形方孔钱**」、“**方孔圆钱**”，是[古中国](../Page/古中国.md "wikilink")[钱币中最常见的一种](../Page/古钱币.md "wikilink")，一般以[銅鑄造](../Page/銅.md "wikilink")，故俗称**銅錢**。方孔钱由[圜钱演变而来](../Page/圜钱.md "wikilink")，自[秦朝统一货币](../Page/秦朝.md "wikilink")，全国铸行[半两钱之后](../Page/半两钱.md "wikilink")，除[王莽改制的短暂时间](../Page/王莽改制.md "wikilink")，方孔钱一直是中国[流通货币的最主要](../Page/流通货币.md "wikilink")[货币形制](../Page/货币.md "wikilink")，直至清朝末年该地位才被[银元所取代](../Page/银元.md "wikilink")，但中国最晚的方孔钱是[民国时期部分地区铸行的](../Page/民国时期.md "wikilink")[民国通宝](../Page/民国通宝.md "wikilink")。

中国方孔钱的形制影响了古代周边地区的造币，[日本](../Page/日本.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[越南](../Page/越南.md "wikilink")、[琉球等国家和地区均曾铸行方孔钱](../Page/琉球.md "wikilink")。

## 别称

[西晉](../Page/西晉.md "wikilink")[魯褒著](../Page/魯褒.md "wikilink")《[錢神論](../Page/錢神論.md "wikilink")》以諷時弊，擬錢為長兄，字曰「孔方」，因此方孔錢也被戏称为「孔方兄」。

## 相关术语

  - 面
  - 背
  - 穿
      - 穿左
      - 穿右

## 钱文

### 读法

圆形方孔钱上的钱文印制顺序不尽相同，主要有以下几种：

  - 直读：又称“顺读”或“对读”，面文按上下右左顺序排列，是一种最常见的钱文读法，始见于[王莽所铸造的](../Page/王莽.md "wikilink")[六泉](../Page/六泉.md "wikilink")。[通宝钱多为此类读法](../Page/通宝.md "wikilink")。
  - 旋读：又称“环读”或“回读”，按上右下左顺序读，一般[元宝钱多为旋读](../Page/元宝.md "wikilink")。
  - 横读/顺读：指两字钱，如“[半两](../Page/半两.md "wikilink")”、“[五铢](../Page/五铢.md "wikilink")”，由右向左读。
  - 环读：文字朝向切线方向，顺时针依次读出，如“[清宁二年](../Page/清宁二年.md "wikilink")”。

另有部分钱币读法特殊，如[天聪汗钱钱文为以左上下右次序排列](../Page/天聪汗钱.md "wikilink")。

### 字体

## 相關著作

[顾烜钱志](../Page/顾烜钱志.md "wikilink")，成書于南朝梁，撰者顾烜，乃孙吴丞相[顾雍之后](../Page/顾雍.md "wikilink")，是最早的錢幣學專著，《隋书·经籍志》記載其有钱图一卷，[钱谱一卷](../Page/钱谱.md "wikilink")，現已亡軼。

續錢譜，唐代[封演撰](../Page/封演.md "wikilink")，今已亡軼。

历代钱谱，宋代[李孝美撰](../Page/李孝美.md "wikilink")，合十卷，今已亡軼。

泉志，书成于南宋宋高宗十九年，撰者[洪遵](../Page/洪遵.md "wikilink")，合十五卷。

錢譜，宋代董逌撰，見載於明代[陶宗儀](../Page/陶宗儀.md "wikilink")[说郛第一百二十卷](../Page/说郛.md "wikilink")，今已亡軼。

錢錄，清乾隆十五年[梁诗正奉敕纂辑](../Page/梁诗正.md "wikilink")，合十六卷，收錄于《[西清古鑒](../Page/西清古鑒.md "wikilink")》附錄。

## 外部链接

## 参考

<references />

[es:Cash chino](../Page/es:Cash_chino.md "wikilink")

[Category:東亞古代貨幣](../Category/東亞古代貨幣.md "wikilink")
[Category:硬币](../Category/硬币.md "wikilink")