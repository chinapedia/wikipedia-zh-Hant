**方正科技集團股份有限公司**（），1986年由[北京大學投資創辦](../Page/北京大學.md "wikilink")，[母公司是](../Page/母公司.md "wikilink")[方正集團](../Page/方正集團.md "wikilink")。公司從事[資訊科技業務](../Page/資訊科技.md "wikilink")，包括[電腦](../Page/電腦.md "wikilink")、電腦配件、[軟件](../Page/軟件.md "wikilink")、電子[儀器](../Page/儀器.md "wikilink")、[機械設備](../Page/機械.md "wikilink")、[收銀機等的](../Page/收銀機.md "wikilink")[研發](../Page/研發.md "wikilink")、[生產](../Page/生產.md "wikilink")、[銷售和售後服務](../Page/銷售.md "wikilink")。\[1\]\[2\]方正科技在1998年於[上海證券交易所借壳上市](../Page/上海證券交易所.md "wikilink")。

現主要業務為科技解決方案規劃，多承接政府相關標案，例如智能城市管理、智能公安、智能水網...等。

## 連結

  - [方正科技集團股份有限公司](http://www.foundertech.com/)

## 參考

[Category:方正集团](../Category/方正集团.md "wikilink")
[Category:上海公司](../Category/上海公司.md "wikilink")
[Category:北京大學校辦產業](../Category/北京大學校辦產業.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:信息技术公司](../Category/信息技术公司.md "wikilink")
[Category:中國軟件公司](../Category/中國軟件公司.md "wikilink")
[Category:中国信息技术公司](../Category/中国信息技术公司.md "wikilink")

1.  [方正科技集团股份有限公司](http://www.foundertech.com/tabid/81/Default.aspx)
2.  [方正科技](http://money.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/600601.phtml)