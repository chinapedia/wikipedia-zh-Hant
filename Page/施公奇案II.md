《**施公奇案II**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司根據](../Page/電視廣播有限公司.md "wikilink")《[施公案](../Page/施公案.md "wikilink")》改編而拍攝製作的清裝[劇集](../Page/劇集.md "wikilink")，全劇共21集，由[林志華擔任監製](../Page/林志華.md "wikilink")，以及由[歐陽震華及](../Page/歐陽震華.md "wikilink")[宣萱領銜主演](../Page/宣萱.md "wikilink")，並由[李思捷](../Page/李思捷.md "wikilink")、[唐寧和](../Page/唐寧_\(香港\).md "wikilink")[陳山聰聯合主演](../Page/陳山聰.md "wikilink")，此劇為《施公奇案系列》第二輯，亦是[唐寧於無綫電視的告別作](../Page/唐寧_\(香港\).md "wikilink")。

## 演員表

### 施家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李楓.md" title="wikilink">李　楓</a></strong></p></td>
<td><p><strong>趙月娥</strong></p></td>
<td><p>施世綸之母</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong><a href="../Page/施世綸.md" title="wikilink">施世綸</a></strong></p></td>
<td><p>趙月娥之子<br />
龐葛愛、賈秀玉、錢麗舒、米香蓉、吳君柔、丁美人之夫<br />
施雅、施若、施文翔、施大力之父<br />
鲁公公、穆奇德·燕嫻之天敌<br />
楊官之好友<br />
於第16集与苏英俊至京城途中遇到船难<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/呂珊.md" title="wikilink">呂　珊</a></strong></p></td>
<td><p><strong>龐葛愛</strong></p></td>
<td><p><strong>大奶奶</strong><br />
施世綸之妻<br />
施琅、趙月娥之大媳<br />
賈秀玉、錢麗舒、米香蓉、吳君柔、丁美人之大姐<br />
於第20集遭魯公公收押，后獲司馬追凤所救<br />
於第21集因食用仙果而成功懷孕</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/姚瑩瑩.md" title="wikilink">姚瑩瑩</a></strong></p></td>
<td><p><strong>賈秀玉</strong></p></td>
<td><p><strong>二奶奶</strong><br />
施世綸之二姨太<br />
施琅、趙月娥之二媳<br />
龐葛愛之二妹<br />
米香蓉、吳君柔、丁美人之二姐<br />
於第20集遭魯公公收押，后獲司馬追凤所救<br />
於第21集因食用仙果而成功懷孕</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p><strong>五奶奶</strong><br />
施世綸之五姨太<br />
施琅、趙月娥之五媳<br />
龐葛愛、賈秀玉、錢麗舒、米香蓉之五妹<br />
丁美人之五姐兼好友<br />
吳軍瑜之妹<br />
吳守信之姪女<br />
吳喜琳之姑姐<br />
施大力之母（於第21集在井中生產）<br />
蘇英俊、丁美人之好友<br />
穆奇德·燕嫻之天敌<br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p><strong>六奶奶</strong><br />
施世綸之有名無實六姨太<br />
施琅、趙月娥之六媳<br />
龐葛愛、賈秀玉、錢麗舒、米香蓉之六妹<br />
吳君柔之六妹兼好友<br />
司馬追風之契女<br />
與蘇英俊相戀<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴耀明.md" title="wikilink">戴耀明</a></p></td>
<td><p><strong>雷　鳴</strong></p></td>
<td><p>施世綸之管家兼師爺<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
</tr>
</tbody>
</table>

### 吳家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅浩楷.md" title="wikilink">羅浩楷</a></p></td>
<td><p>吳守信</p></td>
<td><p>「龍門居」客棧老闆<br />
吳軍瑜、吳君柔之叔<br />
吳喜琳之叔公<br />
於第20集遭魯公公收押（獲司馬追風所救）<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td><p>信叔</p></td>
<td><p>56</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>吳軍瑜</p></td>
<td><p>吳守信之侄<br />
吳君柔之兄<br />
吳喜琳之父<br />
於第20集遭魯公公收押（獲司馬追風所救）</p></td>
<td></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p>吳守信之姪女<br />
吳軍喻之妹<br />
吳喜琳之姑姐<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td><p><strong>君柔<br />
君柔姐<br />
五夫人</strong></p></td>
<td><p>25</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸詠姿.md" title="wikilink">陸詠姿</a></p></td>
<td><p><strong>吳喜琳</strong></p></td>
<td><p>吳守信之侄孫女<br />
吳軍喻之女<br />
吳君柔之姪女<br />
於第20集遭魯公公收押（獲司馬追風所救）</p></td>
<td><p>琳琳</p></td>
<td><p>9</p></td>
</tr>
</tbody>
</table>

### 丁家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅樂林.md" title="wikilink">羅樂林</a></p></td>
<td><p><strong>丁仁川</strong></p></td>
<td><p>退役將軍<br />
丁美人之父<br />
丁叮之大伯父<br />
施世綸之岳父<br />
司馬追風之好友<br />
因其女成親當日遭退婚而吐血<br />
於第13集被戚耀天錯手殺害</p></td>
<td><p><strong>丁將軍</strong></p></td>
<td><p>58</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p>丁仁川之女<br />
丁叮之堂家姐<br />
與蘇英俊為指腹成婚之妻子（成親當日遭退婚）<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong><br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>美人<br />
六夫人</strong></p></td>
<td><p>23</p></td>
</tr>
<tr class="even">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龔茜彤.md" title="wikilink">樂　瞳</a></p></td>
<td><p>丁　叮</p></td>
<td><p>丁仁川之侄女<br />
丁美人之堂妹<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td></td>
<td><p>20</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莊狄生.md" title="wikilink">莊狄生</a></p></td>
<td><p>-</p></td>
<td><p>丁仁川手下</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/裘卓能.md" title="wikilink">裘卓能</a></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### [仙游縣衙門](../Page/仙游.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong><a href="../Page/施世綸.md" title="wikilink">施世綸</a></strong></p></td>
<td><p><a href="../Page/仙游縣.md" title="wikilink">仙游縣</a><a href="../Page/知縣.md" title="wikilink">知縣大人</a>（於第2集開始暫代知縣）<br />
蘇英俊、司馬追風、陵小鳳、雷鳴、四大、名步、張虎之上司<br />
於第16集與蘇英俊至京城途中遇到船難（獲牛大力找人所救）<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>世綸<br />
施大人</strong></p></td>
<td><p>37</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陳山聰.md" title="wikilink">陳山聰</a></strong></p></td>
<td><p><strong>蘇英俊</strong></p></td>
<td><p>「仙游縣」高級<a href="../Page/捕快.md" title="wikilink">捕快</a>（於第4集遭革職，於第7集復職）<br />
施世綸之下屬<br />
於第16集與施世綸至京城途中遇到船難（獲牛大力找人所救）<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>英俊哥<br />
八貝子</strong></p></td>
<td><p>26</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p>「仙游縣」衙門之二爺<br />
蘇英俊之救命恩人<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>美人<br />
六夫人</strong></p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/劉家輝.md" title="wikilink">劉家輝</a></strong></p></td>
<td><p><strong>司馬追風</strong></p></td>
<td><p>仙游縣衙門捕頭<br />
施世綸之下屬<br />
丁仁川之好友<br />
丁美人之契爺（于第13集上契）</p></td>
<td><p><strong>司馬捕頭</strong></p></td>
<td><p>53</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴耀明.md" title="wikilink">戴耀明</a></p></td>
<td><p>雷　鳴</p></td>
<td><p>「仙游縣」衙門之師爺<br />
施世綸之下屬兼管家<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅天池.md" title="wikilink">羅天池</a></p></td>
<td><p>四　大</p></td>
<td><p>仙游縣衙門捕快<br />
施世綸、蘇英俊之下屬</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥嘉倫.md" title="wikilink">麥嘉倫</a></p></td>
<td><p>名　步</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湯俊明.md" title="wikilink">湯俊明</a></p></td>
<td><p>張　虎</p></td>
<td><p>阿虎</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江富強.md" title="wikilink">江富強</a></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寧進.md" title="wikilink">寧　進</a></p></td>
<td><p>-</p></td>
<td><p>仙游縣衙差</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 龍門居客棧

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅浩楷.md" title="wikilink">羅浩楷</a></p></td>
<td><p>吳守信</p></td>
<td><p>「龍門居」客棧老闆<br />
鐵劍蘭、諸葛良、金毛鼠之生意夥伴<br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong></p></td>
<td></td>
<td><p>56</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p>「龍門居」客棧掌門<br />
未婚前專做騙人、問米之事（於第3集遭施世綸拆穿）<br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong><br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>五夫人</strong></p></td>
<td><p>25</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳文靜.md" title="wikilink">陳文靜</a></p></td>
<td><p>鐵劍蘭</p></td>
<td><p>「龍門居」客棧員工<br />
與諸葛良、金毛鼠、吳守信為生意夥伴<br />
曾被西门永欺騙感情及金錢<br />
諸葛良之妻（第21集）</p></td>
<td><p>剑兰</p></td>
<td><p>21</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林子善.md" title="wikilink">林子善</a></p></td>
<td><p>諸葛良</p></td>
<td><p>「龍門居」客棧員工<br />
與鐵劍蘭、金毛鼠、吳守信為生意夥伴<br />
鐵劍蘭之夫（第21集）</p></td>
<td></td>
<td><p>22</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韋家雄.md" title="wikilink">韋家雄</a></p></td>
<td><p>金毛鼠</p></td>
<td><p>「龍門居」客棧員工<br />
與鐵劍蘭、諸葛良、吳守信為生意夥伴</p></td>
<td></td>
<td><p>29</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐寧.md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p>「龍門居」客棧住客<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong><br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
<td><p><strong>美人<br />
六夫人</strong></p></td>
<td><p>23</p></td>
</tr>
<tr class="even">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龔茜彤.md" title="wikilink">樂　瞳</a></p></td>
<td><p>丁　叮</p></td>
<td><p>「龍門居」客棧住客<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong></p></td>
<td></td>
<td><p>20</p></td>
</tr>
</tbody>
</table>

### 燕嫻苑

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/劉玉翠.md" title="wikilink">劉玉翠</a></strong></p></td>
<td><p><strong>穆奇德·燕嫻</strong></p></td>
<td><p>薩嘛喇·阿齊之妻<br />
蘇英俊之表姐<br />
吳牡丹之情敵<br />
魯公公之上司<br />
「德寶號」爆炸沉船命案之主謀<br />
指示魯公公以薩嘛喇·阿齊之名義引吳牡丹上「德寶號」<br />
將沉船爆炸栽贓嫁禍于吳守信、吳君柔、鐵劍蘭、金毛鼠、諸葛良等五人<br />
殺害吴牡丹、阿寶<br />
直接害死錢麗舒、米香蓉、施雅、施若、施翔文<br />
於第20集與鲁公公串謀绑架吳君柔後，將其殺害<br />
於第21集刺殺蘇英俊未遂，被揭發罪行後自殺身亡</p></td>
<td><p><strong>燕嫻<a href="../Page/郡主.md" title="wikilink">郡主</a></strong></p></td>
<td><p>33</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李成昌.md" title="wikilink">李成昌</a></strong></p></td>
<td><p><strong>薩嘛喇·阿齊</strong></p></td>
<td><p>原名：張齊（漢人）<br />
薩嘛喇家之過繼子弟<br />
穆奇德·燕嫻之夫<br />
吳牡丹<a href="../Page/青梅竹馬.md" title="wikilink">青梅竹馬之情人</a>（為攀附權貴而將其拋棄）<br />
於第20集替穆奇德·燕娴顶罪而自杀身亡</p></td>
<td><p><strong>郡馬爺</strong></p></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳山聰.md" title="wikilink">陳山聰</a></strong></p></td>
<td><p><strong>蘇英俊</strong></p></td>
<td><p>原名：蘇完瓜爾·錦春<br />
八王爺之子<br />
穆奇德·燕嫻之表弟<br />
丁美人指腹成婚之丈夫（成親當日退婚）<br />
倩倩之未婚夫<br />
与丁美人相恋<br />
於第19集遭魯公公毒害昏迷（獲丁美人所救）<br />
于第21集险被穆奇德·燕嫻刺杀<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
<td><p><strong>英俊哥<br />
八貝子<br />
锦春</strong></p></td>
<td><p>26</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳自瑤.md" title="wikilink">陳自瑤</a></p></td>
<td><p>倩　倩</p></td>
<td><p>赦大人之女<br />
喜歡蘇英俊<br />
蘇英俊之未婚妻<br />
丁美人之情敵<br />
於第19集因發現蘇英俊深愛丁美人而自願撮合兩人</p></td>
<td><p>倩倩格格</p></td>
<td><p>24</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>魯公公</p></td>
<td><p><a href="../Page/太監.md" title="wikilink">太監</a><br />
穆奇德·燕嫻、薩嘛喇·阿齊之下屬<br />
杀害胡公公、牛大力<br />
于第19集毒害苏英俊<br />
於第20集被穆奇德·燕嫻殺害</p></td>
<td></td>
<td><p>57</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾鈺精.md" title="wikilink">鍾鈺精</a></p></td>
<td><p>丹　兒</p></td>
<td><p>穆奇德·燕嫻之近身侍女</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥皓兒.md" title="wikilink">麥皓兒</a></p></td>
<td><p>美　兒</p></td>
<td><p>倩倩之近身侍女</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 與案件有關人物

#### 殺人凶魚（第1集至第2集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄺佐輝.md" title="wikilink">鄺佐輝</a></p></td>
<td><p>楊　官</p></td>
<td><p>「仙游縣」縣官大人<br />
施世綸好友<br />
因家鄉蘭州旱災而與陳大均合某偷取五千兩官銀<br />
於第1集因分贓不均而利用羊皮囊潛入水中換氣，再以劍魚嘴殺害陳大均<br />
於第2集遭施世綸揭發送辦</p></td>
<td><p>楊大人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅浩楷.md" title="wikilink">羅浩楷</a></p></td>
<td><p>吳守信</p></td>
<td><p>於第2因遭楊官將官銀放至家中而被冤枉偷官銀<br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p>到陳家做法事（實為找回官銀領獎賞）<br />
因與陳大均起口角而遭誤為兇手<br />
於第2集因遭楊官將官銀放至家中而被冤枉偷官銀<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td></td>
<td><p>25</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魏惠文.md" title="wikilink">魏惠文</a></p></td>
<td><p>陳大均</p></td>
<td><p>與楊官合某偷取官銀<br />
於第1集因分贓不均而遭楊官以劍魚嘴所殺身亡</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>朱捕頭</p></td>
<td><p>「仙游縣」捕頭<br />
楊官之下屬<br />
於第1集受楊官命令到「龍門居」客棧殺害吳君柔、吳守信以滅口（事敗遭蘇英俊逮捕）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾慧雲.md" title="wikilink">曾慧雲</a></p></td>
<td><p>陳夫人</p></td>
<td><p>陳大鈞之妻</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁珈詠.md" title="wikilink">梁珈詠</a></p></td>
<td><p>-</p></td>
<td><p>陳夫人近身侍婢</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵卓堯.md" title="wikilink">邵卓堯</a></p></td>
<td><p>漁民</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 肚兜失竊（第4集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td><p>花大娘</p></td>
<td><p>「萬花樓」老鴇</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡考藍.md" title="wikilink">蔡考藍</a></p></td>
<td><p>鶯　鶯</p></td>
<td><p>「萬花樓」<a href="../Page/妓女.md" title="wikilink">妓女</a><br />
遭唐仁偷去<a href="../Page/肚兜.md" title="wikilink">肚兜</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>燕　燕</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾煌.md" title="wikilink">鍾　煌</a></p></td>
<td><p>嬌　嬌</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日伽.md" title="wikilink">日　伽</a></p></td>
<td><p>艷　艷</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾健明.md" title="wikilink">曾健明</a></p></td>
<td><p>唐　仁</p></td>
<td><p>剃頭師傅<br />
偷肚兜賊<br />
因其妻生前專縫製肚兜而藉偷其妻所做之肚兜以思念亡妻<br />
於第4集遭施世綸處罰免費為街坊剃頭一個月</p></td>
<td><p>仁伯</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關菁.md" title="wikilink">關　菁</a></p></td>
<td><p>明燈大師</p></td>
<td><p>深受皇帝寵信之高僧<br />
於第4集遭蘇英俊誤為偷肚兜之賊<br />
不滿遭誤會借皇帝之聲名向施世綸施壓革職蘇英俊</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 名畫失竊（第5集至第6集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王維德.md" title="wikilink">王維德</a></p></td>
<td><p>馮之愷</p></td>
<td><p>倩影之夫<br />
高心妍之婚外情对象<br />
因「月下飛仙」成為名畫家<br />
於第6集為掩護倩影而撞牆自殺身亡</p></td>
<td><p>馮畫師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳琪_(香港藝員).md" title="wikilink">陳　琪</a></p></td>
<td><p>倩　影</p></td>
<td><p>「月下飛仙」之畫中人<br />
馮之愷之妻<br />
於十八年前跳崖自殺未遂，導致雙腳癱瘓及毀容<br />
於第5集假扮女鬼搶去「月下飛仙」名畫<br />
於第6集在衙門自殺身亡</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳宛蔚.md" title="wikilink">陳宛蔚</a></p></td>
<td><p>高心妍</p></td>
<td><p>冯之愷之婚外情对象<br />
倩影之情敌</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 五年凶案（第7集至第10集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>洪煥堂</p></td>
<td><p>劇團「三元班」班主<br />
唐小蓮之夫<br />
玲瓏之父<br />
曾與歐陽漢走私<a href="../Page/和闐玉.md" title="wikilink">和闐玉</a>（欲退出但遭歐陽漢威脅）<br />
於第9集因以為其女玲瓏殺害歐陽漢而頂替認罪</p></td>
<td><p>洪班主</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張達倫.md" title="wikilink">張達倫</a></p></td>
<td><p>閻伯生</p></td>
<td><p>劇團「三元班」文武生<br />
小四之師兄<br />
玲瓏之男友（五年前分手）<br />
因移情別戀愛上王慎盈而拋棄玲瓏<br />
范小樓之師兄兼情敵<br />
於五年前因遭范小樓挑撥指吳軍瑜賣假玉而與其起爭執<br />
於五年前因因玩弄玲瓏感情而遭范小樓殺害，並嫁禍于吳軍瑜</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄彦欣.md" title="wikilink">黄彦欣</a></p></td>
<td><p>王慎盈</p></td>
<td><p>妓女<br />
閻伯生之移情別戀之情婦</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/簡慕華.md" title="wikilink">簡慕華</a></p></td>
<td><p>玲　瓏</p></td>
<td><p>原名：唐小紅<br />
劇團「三元班」花旦<br />
洪煥堂、唐小蓮之女<br />
閻伯生之女友（五年前分手）<br />
欧阳汉之情妇，於第8集分手<br />
范小樓之暗戀對象<br />
缪美如、何佩珊之情敌<br />
於第9集因以為其父洪煥堂殺害歐陽漢而頂替認罪</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李啟傑.md" title="wikilink">李啟傑</a></p></td>
<td><p>范小樓</p></td>
<td><p>劇團「三元班」丑生<br />
專長為：縮骨功<br />
暗戀玲瓏<br />
於五年前利用縮骨功將閻伯生殺害後，栽贓于吳軍瑜<br />
於第8集利用縮骨功殺害歐陽漢後，栽贓于賈秀玉<br />
於第10集刺傷施世綸臀部<br />
於第10集遭施世綸逮捕</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林遠迎.md" title="wikilink">林遠迎</a></p></td>
<td><p>歐陽漢</p></td>
<td><p>何佩珊之夫<br />
玲瓏之戲迷兼情夫（于第8集分手）<br />
范小樓之情敵<br />
與洪煥堂走私和闐玉（威脅洪煥堂不准退出）<br />
生前居住於「龍門居」客棧<br />
於第8集遭范小樓所殺，並嫁禍給賈秀玉</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張國洪.md" title="wikilink">張國洪</a></p></td>
<td><p>小　四</p></td>
<td><p>劇團「三元班」現任文武生<br />
閰伯生之師弟</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚政浩.md" title="wikilink">姚政浩</a></p></td>
<td><p>俊　军</p></td>
<td><p>劇團「三元班」小工</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄碧仁.md" title="wikilink">黄碧仁</a></p></td>
<td><p>唐小蓮</p></td>
<td><p>洪煥堂之亡妻<br />
玲瓏之母<br />
已去世<br />
（劇中以牌位形式出現）</p></td>
<td><p>小莲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>吳軍瑜</p></td>
<td><p>於五年前因遭懷疑殺害閻伯生而被施世綸判刑入獄<br />
於第10集遭洗刷嫌疑<br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong></p></td>
<td></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/姚瑩瑩.md" title="wikilink">姚瑩瑩</a></strong></p></td>
<td><p><strong>賈秀玉</strong></p></td>
<td><p>於第8集離家出走到「龍門居」投棧<br />
於第8集誤以為遭歐陽漢非禮<br />
於第8集因遭范小樓所陷害而被懷疑為殺害歐陽漢（于第10集洗刷嫌疑）<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>阿玉<br />
二夫人</strong></p></td>
<td><p>30</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吴君柔</strong></p></td>
<td><p>假冒洪煥堂之情婦為引出范小樓<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#吳家.md" title="wikilink">吳家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td><p><strong>君柔<br />
君柔姐<br />
五夫人<br />
施五夫人</strong></p></td>
<td><p>25</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong><a href="../Page/施世綸.md" title="wikilink">施世綸</a></strong></p></td>
<td><p>於第10集遭范小樓刺傷臀部<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
<td><p><strong>世綸<br />
施大人</strong></p></td>
<td><p>37</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p>於第9集案件重演時因報仇而險掐死蘇英俊<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong><br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
<td><p><strong>美人<br />
六夫人</strong></p></td>
<td><p>23</p></td>
</tr>
<tr class="even">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳山聰.md" title="wikilink">陳山聰</a></strong></p></td>
<td><p><strong>蘇英俊</strong></p></td>
<td><p>於第9集案件重演時被因丁美人報仇而險被掐死<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong><br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>英俊哥<br />
八貝子</strong></p></td>
<td><p>26</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇麗明.md" title="wikilink">蘇麗明</a></p></td>
<td><p>何佩珊</p></td>
<td><p>歐陽漢之妻<br />
玲瓏之情敵<br />
於第9集為歐陽漢認屍</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/許明志.md" title="wikilink">許明志</a></p></td>
<td><p>黃小二</p></td>
<td><p>前「莆田客棧」小二<br />
已回鄉下黃土鎮<br />
目賭閰伯生與吳軍瑜爭執之證人（第8集））</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥子樂.md" title="wikilink">麥子樂</a></p></td>
<td><p>李伙記</p></td>
<td><p>「莆田客棧」伙記<br />
遭范小樓所撞，導致右手嚴重燙傷（第8集）</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 惡有惡報（第11集至第13集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王青.md" title="wikilink">王　青</a></p></td>
<td><p>盧樹根</p></td>
<td><p>「盧家鎮」鎮長<br />
黃嫦之夫<br />
盧有財、盧達之父<br />
因其子盧有財姦殺盧小嬌而將所有罪證推向戚銘賢<br />
涉嫌放火燒死戚家九口人命（一名下人）<br />
於第11集遭戚耀天在廟裡以銅片引雷電電死</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雪妮.md" title="wikilink">雪　妮</a></p></td>
<td><p>黃　嫦</p></td>
<td><p>盧樹根之妻<br />
盧有財、盧達之母<br />
戚耀天之救命恩人<br />
因目賭戚家大火而患上失心瘋</p></td>
<td><p>根婶</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卓躒.md" title="wikilink">卓　躒</a></p></td>
<td><p>戚耀天</p></td>
<td><p>「盧家鎮」廟祝<br />
年幼時獲黃嫦所救而得以存活<br />
假冒盧家昌過繼盧家鎮之居民<br />
戚銘賢之侄<br />
於第11集因在廟裡以銅片引雷電而電死盧樹根<br />
於第12集集因以牌匾壓斷盧有財之頭部而導致其身亡<br />
於第12集以腐肉作餌而導致盧達被烏鴉啄死<br />
於第13集欲自殺，與丁仁川拉扯之間誤將其刺死</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李子奇.md" title="wikilink">李子奇</a></p></td>
<td><p>盧有財</p></td>
<td><p>盧樹根、黃嫦之長子<br />
盧達之兄<br />
因與其弟盧達姦殺盧小嬌而嫁禍給戚銘賢<br />
涉嫌放火燒死戚家九口人命（一名下人）<br />
於第12集與蘇英俊起爭執<br />
於第12集遭戚耀天以牌匾壓斷頭部身亡</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅君左.md" title="wikilink">羅君左</a></p></td>
<td><p>盧　達</p></td>
<td><p>盧樹根、黃嫦之次子<br />
盧有財之弟<br />
與其兄盧有財姦殺盧小嬌後、嫁禍給戚銘賢<br />
涉嫌放火燒死戚家九口人命（一名下人）<br />
於第12集與蘇英俊起爭執<br />
於第12集遭戚耀天以腐肉作餌導致被烏鴉啄死</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅樂林.md" title="wikilink">羅樂林</a></p></td>
<td><p>丁仁川</p></td>
<td><p>於第13集為阻止戚耀天自殺而遭其錯手殺害<br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong></p></td>
<td><p>丁將軍</p></td>
<td><p>58</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳山聰.md" title="wikilink">陳山聰</a></strong></p></td>
<td><p><strong>蘇英俊</strong></p></td>
<td><p>為追回丁美人而來到「盧家鎮」埋下〈和合符〉<br />
被指殺死盧達（於第12集證實在山中小屋並非真兇）<br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong><br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>英俊哥<br />
八貝子</strong></p></td>
<td><p>26</p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p>盧小嬌</p></td>
<td><p>被盧有財、盧達姦殺<br />
已去世<br />
（劇中以口述形式）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p>盧家昌</p></td>
<td><p>戚耀天之朋友<br />
因病人去世而遭戚耀天假冒</p></td>
<td><p>阿昌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊明.md" title="wikilink">楊　明</a></p></td>
<td><p>戚銘賢</p></td>
<td><p>戚耀天之二叔<br />
遭盧有財、盧達嫁禍姦殺盧小嬌<br />
遭盧樹根放火燒死</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 姦殺之謎（第13集至第15集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭家生.md" title="wikilink">鄭家生</a></p></td>
<td><p>葛　飛</p></td>
<td><p>強姦重犯<br />
林狗之獄友<br />
於第13集利用蘇英俊及林狗以助他出獄<br />
收買沈怡以騙取蘇英俊信任<br />
收買并指使林狗模仿其犯案手法<br />
杀害田小冬、沈怡、林狗、马佩霏<br />
於第15集被捉拿</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬蹄露_(藝人).md" title="wikilink">馬蹄露</a></p></td>
<td><p>田小冬</p></td>
<td><p>四年前姦殺案唯一的倖存者<br />
於四年前遭葛飛劃傷右臉頰<br />
於第13集遭林狗姦殺未遂而劃傷左臉頰<br />
於第14集遭葛飛所殺</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱慧敏.md" title="wikilink">朱慧敏</a></p></td>
<td><p>马佩霏</p></td>
<td><p>于五年前被葛飞杀害</p></td>
<td><p>霏霏</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵秀仪.md" title="wikilink">赵秀仪</a></p></td>
<td><p>沈　怡</p></td>
<td><p>林狗之母<br />
被葛飛收買假扮其母，以騙取蘇英俊信任<br />
於第14集遭葛飛毒殺身亡</p></td>
<td><p>葛太夫人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄俊伸.md" title="wikilink">黄俊伸</a></p></td>
<td><p>林　狗</p></td>
<td><p>沈怡之子<br />
葛飛之獄友<br />
於第13集遭葛飛收買指并使姦殺田小冬未遂<br />
於第14集遭葛飛毒殺身亡</p></td>
<td><p>狗仔</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵国东.md" title="wikilink">赵国东</a></p></td>
<td><p>陈　水</p></td>
<td><p>猎人<br />
葛飞之师弟<br />
於第15集被懐疑幫助葛飛躲藏</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 爆炸事件（第1集至第21集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/劉玉翠.md" title="wikilink">劉玉翠</a></strong></p></td>
<td><p><strong>穆奇德·燕嫻</strong></p></td>
<td><p>「德寶號」爆炸沉船命案之主謀<br />
吳牡丹之情敵<br />
指示魯公公以薩嘛喇·阿齊之名義引吳牡丹至「德寶號」<br />
將沉船爆炸栽贓嫁禍給吳守信、吳君柔、鐵劍蘭、金毛鼠、諸葛良等五人<br />
杀害鲁公公、吴牡丹、阿寶、錢麗舒、米香蓉、施雅、施若、施翔文，20-30幾名的船客<br />
於第21集刺殺蘇英俊未遂，被揭發罪行後自殺身亡<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong><br />
参见<strong><a href="../Page/#吳君柔遭挾持事件（第20集至第21集）.md" title="wikilink">吳君柔遭挾持事件（第20集至第21集）</a></strong></p></td>
<td><p><strong>燕嫻<a href="../Page/郡主.md" title="wikilink">郡主</a></strong></p></td>
<td><p>33</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李成昌.md" title="wikilink">李成昌</a></strong></p></td>
<td><p><strong>薩嘛喇·阿齊</strong></p></td>
<td><p>原名：张齐<br />
穆奇德·燕嫻之夫<br />
吴牡丹之<a href="../Page/青梅竹马.md" title="wikilink">青梅竹马</a>，后为情夫<br />
于第20集替穆奇德·燕嫻顶罪而自杀身亡<br />
参见<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>郡馬爺</strong></p></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>魯公公</p></td>
<td><p>受穆奇德·燕嫻指使，命令牛大力把炸藥錦盒交予吳牡丹<br />
於第18集放火燒毀吳牡丹之屋時，險燒死施世綸、雷鳴（獲司馬追風所救）<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td></td>
<td><p>57</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李思捷.md" title="wikilink">李思捷</a></strong></p></td>
<td><p><strong>牛大力</strong></p></td>
<td><p>被魯公公指示送炸藥錦盒于吳牡丹<br />
參見<strong><a href="../Page/#牛大力身世事件（第14集至第19集）.md" title="wikilink">牛大力身世事件（第14集至第19集）</a></strong></p></td>
<td><p><strong>枕仙<br />
小牛子<br />
半個仙</strong></p></td>
<td><p>30</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳丹丹.md" title="wikilink">陳丹丹</a></p></td>
<td><p>吳牡丹</p></td>
<td><p>前「花好悦圆」姑娘<br />
薩嘛喇·阿齊之<a href="../Page/青梅竹馬.md" title="wikilink">青梅竹馬情人</a>（遭拋棄而淪落至青樓），后为情妇<br />
穆奇德·燕嫻之情敵<br />
陸姑娘之同村好友<br />
因帶著穆奇德·燕嫻的炸藥錦盒上船而炸沉「德寶號」身亡</p></td>
<td></td>
<td><p>28</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/文頌嫻.md" title="wikilink">文頌嫻</a></strong></p></td>
<td><p><strong>錢麗舒</strong></p></td>
<td><p>因乘坐「德寶號」船隻，遭穆奇德·燕嫻用炸藥炸死<br />
參見上集<strong><a href="../Page/施公奇案_(香港).md" title="wikilink">施公奇案</a></strong></p></td>
<td><p>丽舒</p></td>
<td><p>28</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/郭可盈.md" title="wikilink">郭可盈</a></strong></p></td>
<td><p><strong>米香蓉</strong></p></td>
<td><p>香蓉</p></td>
<td><p>26</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>施　雅</p></td>
<td><p>施世綸之女<br />
因乘坐「德寶號」船隻，遭穆奇德·燕嫻用炸藥炸死<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p>洛洛</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>施　若</p></td>
<td><p>晴晴</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>施翔文</p></td>
<td><p>施世綸之子<br />
因乘坐「德寶號」船隻，遭穆奇德·燕嫻用炸藥炸死<br />
參見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p>维维</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎彼得.md" title="wikilink">黎彼得</a></p></td>
<td><p>大口德</p></td>
<td><p>打更佬<br />
阿寶之兄<br />
其弟阿寶於「德寶號」沉船爆炸死亡<br />
將偷運之洋酒放置「德寶號」</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/吕珊.md" title="wikilink">吕　珊</a></strong></p></td>
<td><p><strong>庞葛爱</strong></p></td>
<td><p>于第20集遭鲁公公收押（获司马追风所救）<br />
参见<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td><p><strong>阿爱<br />
大奶奶</strong></p></td>
<td><p>35</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/姚莹莹.md" title="wikilink">姚莹莹</a></strong></p></td>
<td><p><strong>贾秀玉</strong></p></td>
<td><p><strong>阿玉<br />
二夫人</strong></p></td>
<td><p>30</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李枫.md" title="wikilink">李　枫</a></p></td>
<td><p>赵月娥</p></td>
<td><p>施太夫人</p></td>
<td><p>62</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>吴军瑜</p></td>
<td><p>于第20集遭鲁公公收押（获司马追风所救）<br />
参见<strong><a href="../Page/#吴家.md" title="wikilink">吴家</a></strong></p></td>
<td></td>
<td><p>34</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陆咏姿.md" title="wikilink">陆咏姿</a></p></td>
<td><p>吴喜琳</p></td>
<td><p>琳琳</p></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p>遭穆奇德·燕嫻栽贓嫁禍而誤以為炸沉「德寶號」<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
参见<strong><a href="../Page/#吴家.md" title="wikilink">吴家</a></strong><br />
参見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong></p></td>
<td></td>
<td><p>25</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/罗浩楷.md" title="wikilink">罗浩楷</a></p></td>
<td><p>吳守信</p></td>
<td><p>遭穆奇德·燕嫻栽贓嫁禍而誤以為炸沉「德寶號」<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
参见<strong><a href="../Page/#吴家.md" title="wikilink">吴家</a></strong></p></td>
<td></td>
<td><p>56</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳文静.md" title="wikilink">陳文静</a></p></td>
<td><p>鐵劍蘭</p></td>
<td><p>遭穆奇德·燕嫻栽贓嫁禍而誤以為炸沉「德寶號」<br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong></p></td>
<td></td>
<td><p>21</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林子善.md" title="wikilink">林子善</a></p></td>
<td><p>諸葛良</p></td>
<td></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韋家雄.md" title="wikilink">韋家雄</a></p></td>
<td><p>金毛鼠</p></td>
<td></td>
<td><p>29</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陈山聪.md" title="wikilink">陈山聪</a></strong></p></td>
<td><p><strong>苏英俊</strong></p></td>
<td><p>于第19集欲揭發穆奇德·燕嫻之罪行、遭鲁公公毒害（获丁美人所救）<br />
参见<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td></td>
<td><p>26</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伍慧珊.md" title="wikilink">伍慧珊</a></p></td>
<td><p>-</p></td>
<td><p>遭魯公公收買，自認「花好悅圓」之妓女（以擾亂施世綸調查牛大力之身世）（第13集）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳宛蔚.md" title="wikilink">陳宛蔚</a></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃鳳瓊.md" title="wikilink">黃鳳瓊</a></p></td>
<td><p>鳳　媽</p></td>
<td><p>京城「花好悅圓」妓院老闆娘</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 枕仙身世（第7集至第19集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李思捷.md" title="wikilink">李思捷</a></strong></p></td>
<td><p><strong>牛大力</strong></p></td>
<td><p>朱迎娣之子<br />
因家貧而<a href="../Page/自宮.md" title="wikilink">自宮</a>，欲入宮當太監<br />
生前經魯公公介紹在妓院「花好悅園」當雜工<br />
施世綸之枕仙兼好兄弟（提供線索幫其破案）<br />
生前因發現魯公公炸毀「德寶號」而慘遭魯公公用枕頭（即現施世綸之仙枕）殺害滅口<br />
於第19集想起生前記憶而感到愧對施世綸後避而不見<br />
於第19集因與丁美人交換而從仙枕出來指證魯公公<br />
於第21集犧牲自己拯救吳君柔，而升格為「果仙」</p></td>
<td><p><strong>枕仙<br />
小牛子<br />
半個仙</strong></p></td>
<td><p>30</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>魯公公</p></td>
<td><p>受穆奇德·燕嫻指使、命令牛大力把炸藥錦盒交給吳牡丹<br />
因炸毀「德寶號」而遭牛大力發現<br />
杀害牛大力、朱迎涕<br />
於第14集利用胡公公以擾亂施世綸查牛大力之身世（事后將其殺害）<br />
於第19集以牛大力之〈寶貝〉將其威脅<br />
于第21集被穆奇德·燕嫻杀害<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td></td>
<td><p>57</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陈丹丹.md" title="wikilink">陈丹丹</a></p></td>
<td><p>吴牡丹</p></td>
<td><p>前「花好悦圆」前姑娘<br />
萨嘛喇·阿齐之情妇<br />
陆姑娘之同村好友<br />
「德宝号」命案死者之一<br />
参见<strong><a href="../Page/#德寶號命案兼爆炸沉船事件（第16集至第20集）.md" title="wikilink">德寶號命案兼爆炸沉船事件（第16集至第20集）</a></strong></p></td>
<td></td>
<td><p>28</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王偉樑.md" title="wikilink">王偉樑</a></p></td>
<td><p>胡公公</p></td>
<td><p>胡夫人之夫<br />
被魯公公利用隱瞞殺死牛大力之真相<br />
於第14集遭魯公公所殺</p></td>
<td><p>小胡子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余慕莲.md" title="wikilink">余慕莲</a></p></td>
<td><p>朱迎涕</p></td>
<td><p>曾化名：趙冬妮<br />
牛大力之母<br />
被鲁公公殺害</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳志健.md" title="wikilink">陳志健</a></p></td>
<td><p>小黑子</p></td>
<td><p>受魯公公指使殺害施世綸、吳君柔（以阻止他們追查牛大力之身世）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘冠霖.md" title="wikilink">潘冠霖</a></p></td>
<td><p>胡夫人</p></td>
<td><p>前「花好悅圓」妓女<br />
胡公公之妻</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳香倫.md" title="wikilink">吳香倫</a></p></td>
<td><p>牛 婶</p></td>
<td><p>牛家村村民<br />
带领施世倫和吳君柔去牛大力的家</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳念君.md" title="wikilink">陳念君</a></p></td>
<td><p>陸姑娘</p></td>
<td><p>吳牡丹之同村好友<br />
陪吳牡丹至「莆田縣」客棧投棧<br />
於第18集告知施世綸吳牡丹有情人一事</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江漢.md" title="wikilink">江　漢</a></p></td>
<td><p>敬親王</p></td>
<td><p>於第20集到「仙游縣」調查吳君柔炸沉「寶德號」一案<br />
施　琅之好友<br />
參見上集<strong><a href="../Page/施公奇案_(香港).md" title="wikilink">施公奇案</a></strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 挾持事件（第20集至第21集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係</strong></p></td>
<td><p><strong>暱稱</strong></p></td>
<td><p><strong>年齡</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李成昌.md" title="wikilink">李成昌</a></strong></p></td>
<td><p><strong>薩嘛喇·阿齊</strong></p></td>
<td><p>於第20集替穆奇德·燕嫻頂罪而自殺身亡<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>郡馬爺</strong></p></td>
<td><p>34</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/劉玉翠.md" title="wikilink">劉玉翠</a></strong></p></td>
<td><p><strong>穆奇德·燕嫻</strong></p></td>
<td><p>杀害鲁公公<br />
於第20集與鲁公公串謀绑架吳君柔<br />
於第21集刺殺蘇英俊未遂，被揭發罪行後自殺身亡<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td><p><strong>燕嫻<a href="../Page/郡主.md" title="wikilink">郡主</a></strong></p></td>
<td><p>33</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>魯公公</p></td>
<td><p>于第20集与穆奇德·燕嫻串谋绑架吴君柔<br />
於第21集被穆奇德·燕嫻殺害<br />
參見<strong><a href="../Page/#燕嫻苑.md" title="wikilink">燕嫻苑</a></strong></p></td>
<td></td>
<td><p>57</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></strong></p></td>
<td><p><strong>吳君柔</strong></p></td>
<td><p>於第20集遭穆奇德·燕嫻挾持至「天神村」，並推落至井中（獲施世綸所救）<br />
於第21集困在井中後，因食用仙果而順利誔下施大力<br />
参見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#龍門居客棧.md" title="wikilink">龍門居客棧</a></strong><br />
参见<strong><a href="../Page/#吴家.md" title="wikilink">吴家</a></strong></p></td>
<td><p><strong>五夫人</strong></p></td>
<td><p>25</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>丁美人</strong></p></td>
<td><p>於第19集為救施世綸、吳君柔而自戕進入仙枕求牛大力幫助<br />
於第21集為救吳君柔而犧牲自己而成為「枕仙」<br />
参見<strong><a href="../Page/#施家.md" title="wikilink">施家</a></strong><br />
參見<strong><a href="../Page/#丁家.md" title="wikilink">丁家</a></strong><br />
參見<strong><a href="../Page/#仙游縣衙門.md" title="wikilink">仙游縣衙門</a></strong></p></td>
<td><p><strong>美人<br />
六夫人</strong></p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td><p><strong>陸小鳳</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>陸小蝶</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李思捷.md" title="wikilink">李思捷</a></strong></p></td>
<td><p><strong>牛大力</strong></p></td>
<td><p>於第21集為救吳君柔而犧牲自己成為「果仙」<br />
参见<strong><a href="../Page/#牛大力身世事件（第14集至第19集）.md" title="wikilink">牛大力身世事件（第14集至第19集）</a></strong></p></td>
<td><p><strong>枕仙<br />
小牛子<br />
半個仙</strong></p></td>
<td><p>30</p></td>
</tr>
</tbody>
</table>

## 記事

  - 2008年7月28日：此劇於[將軍澳](../Page/將軍澳.md "wikilink")[電視廣播城舉行試造型記者會](../Page/電視廣播城.md "wikilink")。
  - 2008年8月13日：下午3時，此劇於[將軍澳](../Page/將軍澳.md "wikilink")[電視廣播城](../Page/電視廣播城.md "wikilink")11廠舉行開鏡拜神。
  - 2008年9月12日：上午11時，此劇於[將軍澳](../Page/將軍澳.md "wikilink")[電視廣播城古裝街探班](../Page/電視廣播城.md "wikilink")。
  - 2010年7月11日：此劇舉行《施公奇案II》之「枕邊人齊相聚」宣傳活動。
  - 2010年7月19日：此劇於19:15在尖沙咀彌敦道90至94號頂好海鮮酒家舉行《施公奇案II》祝捷會。
  - 2010年7月25日：此劇於12:30在將軍澳新都城中心二期商場L1天幕廣場 舉行《施公奇案II》之「估‧舞‧施公」宣傳活動。
  - 2010年8月2日：此劇於[將軍澳](../Page/將軍澳.md "wikilink")[電視廣播城舉行](../Page/電視廣播城.md "wikilink")《施公奇案II最終解謎》錄影。
  - 2010年8月7日：此劇於20:30於翡翠台及高清翡翠台播出《施公奇案II最終解謎》特備節目，21:00-23:00播映兩小時大結局。

## 備註

## 收視

以下為本劇於[香港](../Page/香港.md "wikilink")[無綫電視](../Page/電視廣播有限公司.md "wikilink")[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台及广州地区之收視紀錄](../Page/高清翡翠台.md "wikilink")：

|        |                      |                  |          |          |         |        |        |        |
| ------ | -------------------- | ---------------- | -------- | -------- | ------- | ------ | ------ | ------ |
| **週次** | **集數**               | **日期**           | **平均收視** | **最高收視** | **百分比** | 广州省网收视 | 广州市网收视 | 广州收视合计 |
| 1      | 01-05                | 2010年7月12日-7月16日 | 30點      | 36點      | 93%     | 3.63   | 6.08   | 9.71   |
| 2      | 06-09                | 2010年7月19日-7月22日 | 32點      | 37點      | 87%     | 3.89   | 5.73   | 9.62   |
| 3      | 10-14                | 2010年7月26日-7月30日 | 31點      | 39點      |         | 4.49   | 4.99   | 9.48   |
| 4      | 15-19                | 2010年8月2日-8月6日   | 31點      | 39點      |         | 3.73   | 5.84   | 9.57   |
| 特輯     | 2010年8月7日20:30-21:00 | 25點              |          |          |         |        |        |        |
| 20-21  | 2010年8月7日21:00-23:00 | 34點              | 37點      | 96%      |         |        |        |        |

  - 括號内爲最高一分鐘收視。
  - 全劇平均收視31.2點。
  - 首日收視33點，最高36點。

## 外部連結

  - [《施公奇案II》 GOTV
    第1集重溫](https://web.archive.org/web/20140222160133/http://gotv.tvb.com/programme/102325/148457/)

## 電視節目的變遷

|align="center" colspan="4"|[天天天晴](../Page/天天天晴.md "wikilink")
6月14日- |- |align="center" colspan="1"|**上一套：**
[蒲松齡](../Page/蒲松齡_\(電視劇\).md "wikilink")
\-7月9日 |align="center"
colspan="2"|**翡翠台/高清翡翠台第二綫劇集([2010](../Page/翡翠台電視劇集列表_\(2010年\)#第二線劇集.md "wikilink"))**
**施公奇案II**
7月12日-8月7日 |align="center" colspan="1"|**下一套：**
[摘星之旅](../Page/摘星之旅.md "wikilink")
8月9日- |- |align="center"
colspan="2"|[情越雙白線](../Page/情越雙白線.md "wikilink")
\-7月23日 |align="center" colspan="2"|[女人最痛](../Page/女人最痛.md "wikilink")
7月26日-

[Category:2010年無綫電視劇集](../Category/2010年無綫電視劇集.md "wikilink")
[Category:無綫電視劇集系列](../Category/無綫電視劇集系列.md "wikilink")
[Category:2010年AOD電視劇集](../Category/2010年AOD電視劇集.md "wikilink")
[Category:無綫電視康熙時期背景劇集](../Category/無綫電視康熙時期背景劇集.md "wikilink")
[Category:施公案題材電視劇](../Category/施公案題材電視劇.md "wikilink")
[Category:電視劇續集](../Category/電視劇續集.md "wikilink")