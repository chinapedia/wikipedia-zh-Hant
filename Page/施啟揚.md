**施啟揚**，[臺灣](../Page/臺灣.md "wikilink")[臺中縣人](../Page/臺中縣.md "wikilink")，[中國國民黨籍](../Page/中國國民黨.md "wikilink")，曾任[司法院院長](../Page/司法院院長.md "wikilink")，著名法學家。配偶為[李鍾桂](../Page/李鍾桂.md "wikilink")。

## 經歷

1958年臺灣大學法律系畢業後，一方面擔任助教，一方面攻讀碩士學位，1962年取得法律學碩士學位後，出國至[德國攻讀博士學位](../Page/德國.md "wikilink")，1967年取得[海德堡大學法學博士學位](../Page/海德堡大學.md "wikilink")。回國後，先是任職臺大法律系副教授，[政治大學](../Page/政治大學.md "wikilink")[國際關係研究中心助理研究員](../Page/政大國關中心.md "wikilink")、研究員等職務，1971年起擔任[臺大法律系兼任教授至](../Page/國立臺灣大學法律學院.md "wikilink")1984年。

施啟揚在回國後即開始參與[中國國民黨的黨務工作](../Page/中國國民黨.md "wikilink")，並在1976年步入政壇。先後擔任過教育局常務、政務次長以及法務部常務次長，1984年出任法務部部長，1988年任行政院副院長，期間經歷[俞國華](../Page/俞國華.md "wikilink")、[李煥及](../Page/李煥.md "wikilink")[郝柏村等人出任](../Page/郝柏村.md "wikilink")[行政院院長](../Page/行政院院長.md "wikilink")。1993年[連戰出任行政院院長後](../Page/連戰.md "wikilink")，轉任[國家安全會議秘書長](../Page/國家安全會議.md "wikilink")，隔年司法院院長[林洋港辭職](../Page/林洋港.md "wikilink")，施啟揚出任遞補院長的位置。1999年將院長一職交棒給[翁岳生後](../Page/翁岳生.md "wikilink")，卸下官職退休。

## 參與319真調會

在[2004年中華民國總統大選投票日前一天](../Page/2004年中華民國總統大選.md "wikilink")3月19日，發生了[319槍擊案](../Page/319槍擊案.md "wikilink")，事後對選舉與政壇都產生巨大的政治效應。[泛藍陣營更宣稱整個事件是](../Page/泛藍.md "wikilink")[綠營為選舉獲勝而製造的](../Page/綠營.md "wikilink")「[陰謀論](../Page/陰謀論.md "wikilink")」，事後相關調查跟質疑排山倒海而來。

2004年8月24日立法院的泛藍立委三讀通過[三一九槍擊事件真相調查特別委員會條例](../Page/s:三一九槍擊事件真相調查特別委員會條例.md "wikilink")，成立[三一九槍擊事件真相調查特別委員會](../Page/三一九槍擊事件真相調查特別委員會.md "wikilink")，施啟揚在[泛藍陣營的邀請下出任該真調會的召集人](../Page/泛藍.md "wikilink")。然而執政的民進黨則發動抵抗權，總統依法公佈該條例，卻違反憲政慣例加以批註，並拒派代表參與，引發了嚴重的憲政危機。當時真調會資金短絀，施啟揚還放話就是靠自己寫的書也要來湊集資金。12月15日司法院大法官作成585號釋憲，確認真調會條例「部分違憲」，剝奪了以泛藍成員為主的真調會部分權力，最後在2005年1月17日公佈自行的調查報告，並斷言這是一場「操作選舉」後，真調會解散。

## 之後的調查工作

2006年3月，在槍擊案滿兩週年前夕，施啟揚在新出版的「三一九槍擊事件調查經過與感想」一書中認為，由於執政當局刻意封鎖有關消息，違法抵抗真調會的合法調查，案件雖發生近兩年，在全民極度關注、沸沸揚揚情況下，全案真相如何，外界仍「嘸宰羊」（閩南語「不知道」之意）。並還表示真調會不是在找陳水扁總統麻煩，而是在澄清黑白，不分藍綠，三一九槍擊案可以說是千古奇案、世紀之謎，特別是[案發現場的真相到今天仍撲朔迷離](../Page/案發現場.md "wikilink")；他並質疑，難道「相信三一九槍擊案是六十三歲的獨行俠[陳義雄](../Page/陳義雄.md "wikilink")，對[陳水扁總統政治謀殺行為](../Page/陳水扁.md "wikilink")」，但這種說法根據民調顯示當時只有百分之十九國民相信。還指出，[三一九槍擊案發現場在那里](../Page/三一九槍擊案.md "wikilink")？情況如何？迄今仍真相未明，因此在書中提出「0319專案現場勘察重建暨証物鑑定報告」、[副總統](../Page/副總統.md "wikilink")[呂秀蓮](../Page/呂秀蓮.md "wikilink")「319中彈紀實」、真調會報告，供外界參考。施啟揚在書中認為，並不知道什麼人是參與三一九槍擊案的核心人物，但只能大膽假設，認為「『與聞機要』或『參與謀議』者，只有陳００、邱００兩人」。由于真調會自始遭執政當局排斥、封鎖及抵抗，無法行使公權力，只有片段書面資料，及傳聞證據，作為推論依據，至於「呂、游、謝、蘇等四人，事前應該都在狀況外」。[1](http://news.sina.com/ausdaily/101-102-101-103/2006-03-15/0532727449.html)

## 參見

  - [中華民國司法院](../Page/中華民國司法院.md "wikilink")
  - [319槍擊事件](../Page/319槍擊事件.md "wikilink")

|- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-           |- |colspan="3"
style="text-align:center;"|**[Seal_of_the_Judicial_Yuan.svg](https://zh.wikipedia.org/wiki/File:Seal_of_the_Judicial_Yuan.svg "fig:Seal_of_the_Judicial_Yuan.svg")[司法院](../Page/司法院.md "wikilink")**
|-

[S施](../Category/台湾法学家.md "wikilink")
[S施](../Category/司法院院長.md "wikilink")
[S](../Category/中華民國行政院大陸委員會主任委員.md "wikilink")
[S](../Category/中華民國法務部部長.md "wikilink")
[S施](../Category/中國國民黨黨員.md "wikilink")
[S施](../Category/海德堡大學校友.md "wikilink")
[S施](../Category/國立臺灣大學法律學院校友.md "wikilink")
[S](../Category/臺灣省立臺中第一中學校友.md "wikilink")
[S](../Category/台中市人.md "wikilink")
[Q啟](../Category/施姓.md "wikilink")