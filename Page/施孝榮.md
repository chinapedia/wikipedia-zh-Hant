**施孝榮**，[臺灣](../Page/臺灣.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[排灣族人](../Page/排灣族.md "wikilink")，1980年代初、中期臺灣[校園民歌歌手](../Page/校園民歌.md "wikilink")。

## 生平

從小在[基督教家庭長大](../Page/基督教.md "wikilink")，直到大學四年級時才在[國立臺灣師範大學對面的](../Page/國立臺灣師範大學.md "wikilink")[台北市召會第三聚會所](../Page/台北市召會.md "wikilink")[受浸](../Page/受浸.md "wikilink")。\[1\]

從小熱愛[國術](../Page/國術.md "wikilink")，特別專研[南少林拳的](../Page/南少林拳.md "wikilink")[羅漢拳](../Page/羅漢拳.md "wikilink")，並於民國67年參加由[中華民國國術協會主辦的](../Page/中華民國國術協會.md "wikilink")「世界杯國術比賽」奪得冠軍，因此保送[國立臺灣師範大學體育系](../Page/國立臺灣師範大學.md "wikilink")。

大學二年級時，參加[金韻獎獲得優勝](../Page/金韻獎.md "wikilink")，隨後便推出首支單曲《歸人沙城》（收錄於《金韻獎5》合輯中），並有個人專輯問世。退伍後則在小學任教。

近年皆以創作[福音詩歌為主](../Page/聖詩.md "wikilink")，偶爾會出席民歌演唱會。

## 作品

  - 主持

<!-- end list -->

  - [中國電視公司](../Page/中國電視公司.md "wikilink")《[黃金假期
    海陸大進擊](../Page/黃金假期_海陸大進擊.md "wikilink")》（先與[紀芳玲主持](../Page/紀芳玲.md "wikilink")，後與[馬維欣主持](../Page/馬維欣.md "wikilink")）
  - 中國電視公司《[黃金假期 歡樂總動員](../Page/黃金假期_歡樂總動員.md "wikilink")》（與馬維欣主持）
  - [衛視中文台](../Page/衛視中文台.md "wikilink")《[美夢成真](../Page/美夢成真.md "wikilink")》（與[蔡閨主持](../Page/蔡閨.md "wikilink")）
  - [中央廣播電臺](../Page/中央廣播電臺.md "wikilink")《笑容時間》

<!-- end list -->

  - 電視劇

<!-- end list -->

  - [中華電視公司](../Page/中華電視公司.md "wikilink")《[大兵日記](../Page/大兵日記.md "wikilink")》飾
    賴熊
  - [中華電視公司](../Page/中華電視公司.md "wikilink")《[兄弟有緣](../Page/兄弟有緣.md "wikilink")》飾
    阿德
  - [中華電視公司](../Page/中華電視公司.md "wikilink")《[阿足](../Page/阿足.md "wikilink")》飾
    阿伊達
  - [台灣電視公司](../Page/台灣電視公司.md "wikilink")《[台灣廖添丁](../Page/台灣廖添丁.md "wikilink")》飾
    鈴木
  - [民間全民電視公司](../Page/民間全民電視公司.md "wikilink")《民視劇場—聽海》飾 比的

<!-- end list -->

  - 電影

<!-- end list -->

  - 《[報告班長2](../Page/報告班長2.md "wikilink")》（1988年）飾 吳茂男
  - 《[過河小卒](../Page/過河小卒.md "wikilink")》（1989年）飾 施全
  - 《[超級班長](../Page/超級班長.md "wikilink")》（1996年）

## 參考資料

## 外部連結

  - [中央廣播電臺主持人施孝榮](http://radio.rti.org.tw/program/host/?recordId=303)

[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:台灣校園民歌歌手](../Category/台灣校園民歌歌手.md "wikilink")
[Category:台灣原住民歌手](../Category/台灣原住民歌手.md "wikilink")
[Category:華語歌手](../Category/華語歌手.md "wikilink")
[Category:台灣喜劇演員](../Category/台灣喜劇演員.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")
[Category:台灣綜藝節目主持人](../Category/台灣綜藝節目主持人.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")
[Category:排灣族人](../Category/排灣族人.md "wikilink")
[Category:霧臺人](../Category/霧臺人.md "wikilink")
[Xiao孝](../Category/施姓.md "wikilink")
[Category:台灣創作歌手](../Category/台灣創作歌手.md "wikilink")
[Category:台灣作詞家](../Category/台灣作詞家.md "wikilink")
[Category:台灣作曲家](../Category/台灣作曲家.md "wikilink")

1.  [生命見證分享(施孝榮弟兄)](https://lambfollower.wordpress.com/testimony/%E7%94%9F%E5%91%BD%E8%A6%8B%E8%AD%89%E5%88%86%E4%BA%AB%E6%96%BD%E5%AD%9D%E6%A6%AE%E5%BC%9F%E5%85%84/)，一個基督徒的屬靈旅程