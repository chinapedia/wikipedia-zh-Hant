[Chandragupt_maurya_Birla_mandir_6_dec_2009_(31).JPG](https://zh.wikipedia.org/wiki/File:Chandragupt_maurya_Birla_mandir_6_dec_2009_\(31\).JPG "fig:Chandragupt_maurya_Birla_mandir_6_dec_2009_(31).JPG")

**旃陀羅笈多·孔雀**（，，[古希腊文](../Page/古希腊文.md "wikilink")：，），又称**月护王**、**旃陀羅崛多**，是[印度](../Page/印度.md "wikilink")[孔雀王朝开国君主](../Page/孔雀王朝.md "wikilink")，首次將[印度次大陸大部分地區統一於一個政權之下](../Page/印度次大陸.md "wikilink")。他于前320年至前298年间在位。\[1\]其孫[阿育王促成佛教從印度向外傳播](../Page/阿育王.md "wikilink")。\[2\]

## 生平

他的早期生平仍是一个谜，据说他的姓“孔雀”（Maurya）说明他来自一个饲养[孔雀的家族](../Page/孔雀.md "wikilink")，这意味着他出生于较低下的[种姓](../Page/种姓.md "wikilink")，但是也有传说称他是[难陀王朝王子的后代和一个女仆Mura的私生子](../Page/难陀王朝.md "wikilink")，Maurya是Mura的音讹。

他在少年时被[摩揭陀国王从国中放逐](../Page/摩揭陀.md "wikilink")。在途中他遇到了著名的[婆罗门谋臣](../Page/婆罗门.md "wikilink")[考底利耶](../Page/考底利耶.md "wikilink")，这是一位绝对的[现实主义者](../Page/现实主义.md "wikilink")（后世称他为印度的[马基亚维利](../Page/马基亚维利.md "wikilink")）。另一个故事则是说考底利耶因为一件小事与难陀国王争吵，而被放逐，在途中他发现了一位有着王者气息的少年在鬥羊。不管怎么说，在前321年，旃陀羅笈多在考底利耶的协助下攻克了摩揭陀的都城[华氏城](../Page/华氏城.md "wikilink")，自立为摩揭陀国王。

之后，他组织起了一支大军，据称有3万骑兵、9千[战象和](../Page/大象.md "wikilink")60万步兵。他攻打[马其顿王国](../Page/马其顿王国.md "wikilink")[亚历山大大帝在](../Page/亚历山大大帝.md "wikilink")[印度河流域建立的军事要塞](../Page/印度河.md "wikilink")，夺取了[旁遮普](../Page/旁遮普.md "wikilink")，使摩揭陀的领土从[孟加拉湾延伸到](../Page/孟加拉湾.md "wikilink")[阿拉伯海](../Page/阿拉伯海.md "wikilink")，之后，他又南下攻打中印度诸国，将领土扩张到[德干高原](../Page/德干高原.md "wikilink")，统一北印度和中印度部分地区。

前305年，继承了亚历山大的[亚洲领地的](../Page/亚洲.md "wikilink")[塞琉古一世率军入侵旁遮普](../Page/塞琉古一世.md "wikilink")，迫使旃陀羅笈多回师与其决战。关于这场战争印度人和希腊人有着不同的说法。在印度人的版本中，旃陀羅笈多彻底打败了希腊军队，夺取了[兴都库什山脉以南所有土地](../Page/兴都库什山脉.md "wikilink")，迫使希腊人求和。而在希腊人的版本中恰恰相反，塞琉古一直打到了华氏城，印度人再也无力支撑。不管怎么说，在前302年，双方达成了和约，塞琉古承认旃陀羅笈多对旁遮普的统治，并将一个女儿嫁给旃陀羅笈多，以换取印度人送给他五百头大象和象手，支持他对[埃及的扩张](../Page/埃及.md "wikilink")。和约的签订确立了孔雀帝国的大国身份，埃及[托勒密王朝和](../Page/托勒密王朝.md "wikilink")[塞琉西帝国都向华氏城派出了常驻使节](../Page/塞琉西帝国.md "wikilink")。

傳說在西元前298年，旃陀羅笈多皈依了[耆那教](../Page/耆那教.md "wikilink")，成为圣者Bhadrabahu的亲传弟子。Bhadrabahu預言了十二年的大饑荒，而他努力也無法挽救饑荒帶來的悲慘情形。他将王位传给了儿子[频头娑罗](../Page/频头娑罗.md "wikilink")，自己前往森林中苦行，最终绝食而死于耆那教聖地Shravanabelagola。\[3\]


## 注釋

[Category:印度君主](../Category/印度君主.md "wikilink")
[Category:孔雀王朝](../Category/孔雀王朝.md "wikilink")

1.  [Chandragupta Maurya, EMPEROR OF
    INDIA](https://www.britannica.com/biography/Chandragupta),
    Encyclopædia Britannica

2.

3.