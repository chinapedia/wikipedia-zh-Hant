**旅行團**是一種[旅遊的模式](../Page/旅遊.md "wikilink")，先付款、後[消費](../Page/消費.md "wikilink")。旅遊團的價錢包括了[交通](../Page/交通.md "wikilink")、[三餐](../Page/三餐.md "wikilink")、[旅館住房](../Page/旅館.md "wikilink")、[觀光景點的入場費等](../Page/觀光景點.md "wikilink")，而[導遊](../Page/導遊.md "wikilink")、[車長及](../Page/車長.md "wikilink")[領隊的](../Page/領隊.md "wikilink")[建議小費則另計](../Page/建議小費.md "wikilink")。當中的交通包括[飛機票及](../Page/飛機票.md "wikilink")[遊覽車](../Page/遊覽車.md "wikilink")。旅行團是[旅行社的](../Page/旅行社.md "wikilink")[商業活動主要](../Page/商業.md "wikilink")[收入來源](../Page/收入.md "wikilink")。

相對於旅行團，即是[自由行](https://www.taxicar-trip.com/)、[自助遊](https://www.taxicar-trip.com/)，自費到外地，找當地人當導遊，自費包車、[[租車](https://www.taxicar-trip.com/)](../Page/汽車租賃.md "wikilink")，自費找旅館房間過夜，自己設計觀光及行程等。

第一次有組織的旅行團可以追溯到1841年7月5日到托馬斯·庫克。

## [旅行團](https://www.taxicar-trip.com/)的分類

  - 本地：在旅客本土旅遊，参加本地遊的旅行團。
  - 外遊：出國外旅遊
      - 長線：適合長假期，例如七天[黃金週](../Page/黃金週.md "wikilink")、[聖誕節](../Page/聖誕節.md "wikilink")、[農曆新年等](../Page/農曆新年.md "wikilink")。
      - 短線：適合短假期，例如[復活節](../Page/復活節.md "wikilink")、[端午節等](../Page/端午節.md "wikilink")。

## 相關

[Travelers_from_Shilin_District,_Taipei_Gathered_at_No.11_Pier_20130504.jpg](https://zh.wikipedia.org/wiki/File:Travelers_from_Shilin_District,_Taipei_Gathered_at_No.11_Pier_20130504.jpg "fig:Travelers_from_Shilin_District,_Taipei_Gathered_at_No.11_Pier_20130504.jpg")

  - [消費者](../Page/消費者.md "wikilink")
  - [服務業](../Page/服務業.md "wikilink")
  - [茶會講座](../Page/茶會.md "wikilink")
  - 早機去、晚機返
  - 旅遊團之後逗留多天（Stay behind）
  - 自由活動
  - [中途團](../Page/中途團.md "wikilink")、跳機
  - 鴨仔團、水魚團
  - [零團費旅行團](../Page/零團費旅行團.md "wikilink")
  - [香港導遊總工會](../Page/香港導遊總工會.md "wikilink")
  - [定點購物](../Page/定點購物.md "wikilink")

## 外部連結

  - [中國旅行社業協會](http://www.cats.org.cn/)
  - [永安旅遊](https://www.wingontravel.com/)
  - [LuxeTravel 品味遊](https://www.luxetravel.com.hk/)

[Category:旅遊](../Category/旅遊.md "wikilink")
[Category:服務業](../Category/服務業.md "wikilink")