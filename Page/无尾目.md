**無尾目**（[学名](../Page/学名.md "wikilink")：）是[两生纲的一個目](../Page/两生纲.md "wikilink")，其下生物即**蛙**或**蟾**。該目的生物成体基本无尾，[卵一般产于水中](../Page/卵.md "wikilink")，孵化成[蝌蚪](../Page/蝌蚪.md "wikilink")，用[鰓呼吸](../Page/鰓.md "wikilink")，经过[变态](../Page/变态_\(生物\).md "wikilink")，成体主要用[肺呼吸](../Page/肺.md "wikilink")，但多数[皮肤也有部分呼吸功能](../Page/皮肤.md "wikilink")。无尾目是[生物从水中走上陆地的第一步](../Page/生物.md "wikilink")，比其他[两生纲生物要先进](../Page/两生纲.md "wikilink")，虽然多数已经可以离开[水生活](../Page/水.md "wikilink")，但繁殖仍然离不开水，卵需要在水中经过[变态才能成长](../Page/变态.md "wikilink")。因此不如[爬行纲动物先进](../Page/爬行纲.md "wikilink")，爬行纲动物已经可以完全离开水生活。

## 分类

**蛙類**，俗称**水鸡**、**田鸡**、**石鸡**，台語俗稱**四腳仔**、**四腳魚仔**或**水雞**\[1\]，大約有4800種。
主要包括两类动物：[青蛙和](../Page/青蛙.md "wikilink")[蟾蜍](../Page/蟾蜍.md "wikilink")。这两类动物没有太严格的区别，有的一[科中同时包括两种](../Page/科.md "wikilink")。一般来说，蟾蜍多在陆地生活，因此皮肤多粗糙；蛙体形较苗条，多善于游泳。两种体形相似，颈部不明显，无肋骨。前肢的尺骨与桡骨愈合，后肢的胫骨与腓骨愈合，因此爪不能灵活转动，但四肢肌肉发达。蛙類和蟾類很難絕對地區分開，有的科如盤舌蟾科就即包括蛙類也有蟾類。

### 青蛙

**青蛙**又稱**田雞**，是因為味道鮮美之故，口感近似雞肉。人類很早就知道食用青蛙，《汉书》说：“鄠杜之间水多蛙，鱼人得不饥。”[张华](../Page/张华.md "wikilink")《[博物志](../Page/博物志.md "wikilink")》说：“东南之人……食水产者，龟蚌螺蛤，以为珍味，不觉其腥。”唐末[尉迟枢](../Page/尉迟枢.md "wikilink")《南楚新闻》言：“百越人好食虾蟆，凡有筵会，斯为上味。”當時吃青蛙還是南方人的習俗，[韓愈在](../Page/韓愈.md "wikilink")《答柳柳州食虾蟆》诗说[柳宗元](../Page/柳宗元.md "wikilink")“居然当鼎味，岂不辱钓罩”，責怪柳柳州不該把青蛙當美味。[苏东坡在](../Page/苏东坡.md "wikilink")《闻子由瘦》诗中写道：“旧闻蜜唧尝呕吐，稍近虾蟆缘习俗。”

### 蟾蜍

**蟾蜍**，俗称“**癞蛤蟆**”，大部分蟾蜍耳后有毒腺，分泌毒性分泌物，可以制作[中药](../Page/中药.md "wikilink")。

蟾蜍科的动物大约有250种，分布在除了[澳大利亚和](../Page/澳大利亚.md "wikilink")[马达加斯加岛等海岛以外的全世界各地](../Page/马达加斯加岛.md "wikilink")，目前澳大利亚也引入了蟾蜍。虽然大部分蟾类生活在陆地上，栖身地洞内，但也有必须生活在水中或树上的蟾类。[墨西哥的](../Page/墨西哥.md "wikilink")[异舌蟾以](../Page/异舌蟾.md "wikilink")[蚂蚁为食](../Page/蚂蚁.md "wikilink")。

### 分类列表

无尾目大约有5,070种，分为三个亚目：

  - [始蛙亚目](../Page/始蛙亚目.md "wikilink")（Archaeobatrachia） - 共4科6属28种。

Tailed frog.gif|[尾蟾科](../Page/尾蟾科.md "wikilink") （Ascaphidae） Bombina
variegata1.jpg|[铃蟾科](../Page/铃蟾科.md "wikilink") （Bombinatoridae）
AlytesObstet.jpg|[盘舌蟾科](../Page/盘舌蟾科.md "wikilink") （Discoglossidae）
Hochstetters Frog on Moss.jpg|[滑蹠蟾科](../Page/滑蹠蟾科.md "wikilink")
（Leiopelmatidae）

  - [中蛙亚目](../Page/中蛙亚目.md "wikilink") （Mesobatrachia） - 共6科20属168种。

Leptob hasselt M 080208-4534 clobk.jpg|[角蟾科](../Page/角蟾科.md "wikilink")
（Megophryidae） PelobatesFuscus.jpg|[锄足蟾科](../Page/锄足蟾科.md "wikilink")
（Pelobatidae） Pelodytes punctatus
side.jpg|[合附蟾科](../Page/合附蟾科.md "wikilink")
（Pelodytidae） Xenopus laevis
1.jpg|[负子蟾科](../Page/负子蟾科.md "wikilink") （Pipidae）
Rhinophrynus dorsalis.jpg|[异舌蟾科](../Page/异舌蟾科.md "wikilink")
（Rhinophrynidae） Spea hammondii
1.jpg|[北美锄足蟾科](../Page/北美锄足蟾科.md "wikilink")
（Scaphiopodidae）

  - [新蛙亚目](../Page/新蛙亚目.md "wikilink") （Neobatrachia） - 以下幾科。
  - 真正的[蛙](../Page/蛙.md "wikilink")（包括[亞洲角蛙科](../Page/亞洲角蛙科.md "wikilink")
    Ceratobatrachidae，[叉舌蛙科](../Page/叉舌蛙科.md "wikilink")
    Dicroglossidae，[小岩蛙科](../Page/小岩蛙科.md "wikilink")
    Micrixalidae，[昏蛙科](../Page/昏蛙科.md "wikilink")
    Nyctibatrachidae，[石蛙科](../Page/石蛙科.md "wikilink")
    Petropedetidae，[蟾蛙科](../Page/蟾蛙科.md "wikilink")
    Phrynobatrachidae，[皱蛙科](../Page/皱蛙科.md "wikilink")
    Ptychadenidae，[箱头蛙科](../Page/箱头蛙科.md "wikilink")
    Pyxicephalidae，[赤蛙科](../Page/赤蛙科.md "wikilink") Ranidae）
  - [真齒蛙科](../Page/真齒蛙科.md "wikilink") Amphignathodontidae–
    有袋蛙（有時被列為[扩角蛙科](../Page/扩角蛙科.md "wikilink")
    Hemiphractidae）
  - [隱林蛙科](../Page/隱林蛙科.md "wikilink") Aromobatidae– 臭鼬蛙（有時列入叢蛙科）
  - [節蛙科](../Page/節蛙科.md "wikilink") Arthroleptidae
  - [短頭蟾科](../Page/短頭蟾科.md "wikilink") Brachycephalidae
  - [蟾蜍科](../Page/蟾蜍科.md "wikilink") Bufonidae– 真正的蟾蜍
  - [智利蟾科](../Page/智利蟾科.md "wikilink") Calyptocephalellidae（有時列入蟾蜍科）
  - [瞻星蛙科](../Page/瞻星蛙科.md "wikilink") Centrolenidae –
    [玻璃蛙](../Page/玻璃蛙.md "wikilink")（包括[疣蛙科](../Page/疣蛙科.md "wikilink")
    Allophrynidae）
  - [Craugastoridae](../Page/Craugastoridae.md "wikilink")
    <small>Hedges, Duellmann & Heinicke, 2008</small>（以前列為短頭蟾科）
  - [角花蟾科](../Page/角花蟾科.md "wikilink") Ceratophryidae–
    又稱為[角蛙科](../Page/角蛙科.md "wikilink")，[角蛙](../Page/角蛙.md "wikilink")
  - [Conrauidae](../Page/Conrauidae.md "wikilink")
    <small>Nieden,1908</small>
  - [叢蛙科](../Page/叢蛙科.md "wikilink") Dendrobatidae– 箭毒蛙
  - [卵齒蟾科](../Page/卵齒蟾科.md "wikilink") Eleutherodactylidae<small>Lutz,
    1954</small>（以前列為细趾蟾科）
  - [沼蟾科](../Page/沼蟾科.md "wikilink") Heleophrynidae– 鬼蛙
  - [肩蛙科](../Page/肩蛙科.md "wikilink") Hemisotidae– 鯊鏟蛙
  - [雨蛙科](../Page/雨蛙科.md "wikilink") Hylidae–
    真正的[樹蛙](../Page/樹蛙.md "wikilink")，與[幽蛙科](../Page/幽蛙科.md "wikilink")
    Cryptobatrachidae、扩角蛙科 Hemiphractidae有親屬關係
  - [若雨蛙科](../Page/若雨蛙科.md "wikilink") Hylodidae
  - [非洲樹蛙科](../Page/非洲樹蛙科.md "wikilink") Hyperoliidae–
    莎草蛙，“[灌木蛙](../Page/灌木蛙.md "wikilink")”
  - [滑背蟾科](../Page/滑背蟾科.md "wikilink") Leiuperidae （有時列為細趾蟾科）
  - [細趾蟾科](../Page/細趾蟾科.md "wikilink") Leptodactylidae–
    南方蛙，熱帶蛙（包括[胯腺蟾科](../Page/胯腺蟾科.md "wikilink")
    Cycloramphidae）
  - [曼蛙科](../Page/曼蛙科.md "wikilink") Mantellidae– 馬爾加什蛙
  - [姬蛙科](../Page/姬蛙科.md "wikilink") Microhylidae– 狹口蛙（包括短頭蛙）
  - [龜蟾科](../Page/龜蟾科.md "wikilink")
    Myobatrachidae（包括[汀蟾科](../Page/汀蟾科.md "wikilink")，Rheobatrachidae）
  - [多指節蟾科](../Page/多指節蟾科.md "wikilink") Pseudidae
  - [跳蛙科](../Page/跳蛙科.md "wikilink") Ranixalidae
    （有時被列入[赤蛙科](../Page/赤蛙科.md "wikilink")
    Ranidae）
  - [樹蛙科](../Page/樹蛙科.md "wikilink") Rhacophoridae– 灌木蛙
  - [達爾蛙科](../Page/達爾蛙科.md "wikilink") Rhinodermatidae – （有時列為胯腺蟾科）
  - [塞舌蛙科](../Page/塞舌蛙科.md "wikilink") Sooglossidae–
    [塞舌爾蛙和豬鼻蛙](../Page/塞舌爾.md "wikilink")（包括[印度鼻蛙](../Page/印度鼻蛙.md "wikilink")
    Nasikabatrachidae）
  - [Strabomantidae](../Page/Strabomantidae.md "wikilink")
    <small>Hedges, Duellmann & Heinicke, 2008</small> （某些以前被認為屬於短頭蟾科）
  - [池蟾科](../Page/池蟾科.md "wikilink") Telmatobiidae
    <small>[Fitzinger](../Page/Leopold_Fitzinger.md "wikilink"),
    1843</small>

## 变态

<center>

<File:Frogspawn> closeup.jpg|孵化的卵 <File:Tadpoles> 10 days.jpg|10天的蝌蚪
[File:Frog-Zhe.jpg|幼蛙](File:Frog-Zhe.jpg%7C幼蛙)
[File:Green-leopard-frog-in-swamp.jpg|成體](File:Green-leopard-frog-in-swamp.jpg%7C成體)

</center>

## 身体

蛙類最小的只有30毫米，只相當一個人的大拇指長，大的有300毫米（一尺多長），瞳孔都是橫向的，皮膚光滑，舌尖分兩叉，舌跟在口的前部，倒著長回口中，能突然翻出捕捉蟲子。有三個眼瞼，其中一個是透明的，在水中保護眼睛用，另外兩個上下眼瞼是普通的。頭兩側有兩個聲囊，可以產生共鳴，放大叫聲。體形小的品種叫聲[頻率較高](../Page/頻率.md "wikilink")。

### 器官

  - 心臟：有二個心房和一個心室，外有圍心膜
  - 脾臟：紅色呈豆形，能製造白血球
  - 肺臟：呈紅色，左右各一葉
  - 皮膚：裸露，内含丰富的粘液腺，可以幫助氣體交換，有些种类在不同部位集中形成毒腺、腺褶、疣粒(呼吸皮膚要潮濕)
  - 腎臟：扁平暗紅色
  - 腎上線：淺黃色線狀
  - 輸尿管：於腎臟下方，與膀胱相通
  - 膀胱：呈元寶形，透明無色
  - 脂肪體：黃色脂狀
  - 睪丸：雄蛙才有，一對，呈橢圓形，白色式淡紅色
  - 卵巢：雌蛙才有，形狀不規則，內有黑白各半的卵
  - 輸卵管：雌蛙才有，白色
  - 子宮：雌蛙才有

## 习性

蛙類的繁殖方式基本和蟾蜍類相似，也是以昆蟲為食，但大型蛙類可以捕食小[魚甚至小](../Page/魚.md "wikilink")[鼠](../Page/鼠.md "wikilink")。基本在夜間捕食。絕大部分生活在[水中](../Page/水.md "wikilink")，也有生活在[雨林潮濕環境的](../Page/雨林.md "wikilink")[樹上的](../Page/樹.md "wikilink")。[卵產於水中](../Page/卵.md "wikilink")，也有的樹蛙僅僅利用樹洞中或[植物葉根部積累殘餘的水窪就能使卵經過蝌蚪階段](../Page/植物.md "wikilink")。2003年在[印度西部新發現一種](../Page/印度.md "wikilink")「[紫蛙](../Page/紫蛙.md "wikilink")（*Nasikabatrachus
sahyadrensis*）」，常年生活在地底的洞中，只有[季風帶來雨水時才出洞生育](../Page/季風.md "wikilink")。

## 防衛機制

[R._imitator_Chazuta.jpg](https://zh.wikipedia.org/wiki/File:R._imitator_Chazuta.jpg "fig:R._imitator_Chazuta.jpg")（*Ranitomeya
imitator*）身上披上鮮艷的顏色，假裝成其有毒的同類\]\]
[Dendrobates_pumilio.jpg](https://zh.wikipedia.org/wiki/File:Dendrobates_pumilio.jpg "fig:Dendrobates_pumilio.jpg")鮮艷的警戒態表明牠的皮膚上有不同的毒素\]\]
第一眼看青蛙時可能會認為牠們缺乏防禦能力。其細小的身體、緩慢的動作、薄而缺乏如尖刺、利爪及牙齒的外型往往令人疏於防範。事實上牠們有豐富的防衛機制去保護牠們。首先不少物種都有良好的[偽裝能力](../Page/偽裝.md "wikilink")，其與附近環境十分接近的膚色令一動不動的牠們易於隱藏於環境之中。遇到危險時，其驚人的彈跳力令牠們迅速跳到水中，以避開獵食者的追捕\[2\]。

很多青蛙的表面都有輕度毒性的[蟾毒素](../Page/蟾毒素.md "wikilink")（bufotoxin），使牠們並不受獵食者的歡迎。而大部分蟾蜍則有較大的制毒腺體，稱作[腮腺](../Page/腮腺.md "wikilink")（parotoid
gland），主要位於當中的兩側眼後的位置，或身體的其他部分。這些腺體能分泌不同的毒素或黏液，使牠們的皮膚變得滑溜而且並不可口。如果能產生即時的不快感覺，捕獵者多會終止牠們的捕獵行動而令牠們得以逃脫；如果效果需時甚久才有效果，也可減少捕獵者於下一次再度捕獵的機會\[3\]。而帶有劇毒的青蛙則多會披上鮮艷的顏色，以標示牠們並不適合作為食物，這種適應的的策略稱作[警戒作用](../Page/警戒作用.md "wikilink")（Aposematism）。牠們身上的顏色多為鮮艷的紅色、橙色或黃色配以黑色。一些物種的警告色長在腹部上，如[铃蟾属的物種](../Page/铃蟾属.md "wikilink")。因此牠們在遇到攻擊時反而會將腹部朝上，並分泌毒液以趕退敵人。並有一些物種本身沒有毒性，如[紅背異箭毒蛙](../Page/紅背異箭毒蛙.md "wikilink")，就會[模擬在其地域中有毒的物種的膚色以嚇退獵食者](../Page/貝氏擬態.md "wikilink")\[4\]。

[Bufo_bufo-defensive_reaction1.JPG](https://zh.wikipedia.org/wiki/File:Bufo_bufo-defensive_reaction1.JPG "fig:Bufo_bufo-defensive_reaction1.JPG")\]\]
一些蛙類物種，例如[箭毒蛙等的毒性特別強](../Page/箭毒蛙.md "wikilink")。南美洲的土著很久以前已懂得抽取這些箭毒蛙身上的毒液去製造[飛鏢](../Page/飛鏢.md "wikilink")（Dart
(missile)）以作打獵之用\[5\]，雖然只有少數物種適合此目的。此外，一些物種，[紅背異箭毒蛙](../Page/紅背異箭毒蛙.md "wikilink")、[卵齿蟾属](../Page/卵齿蟾属.md "wikilink")（*Eleutherodactylus*）的一個物種（*Eleutherodactylus
gaigei*）及[细趾蟾属](../Page/细趾蟾属.md "wikilink")（*Leptodactylus*）的一個物種（*Lithodytes
lineatus*）雖然本身沒有製造毒液，但牠們會透過[模擬當區有毒的近親的膚色而達致保護自己的目的](../Page/貝氏擬態.md "wikilink")\[6\]\[7\]。一些物種雖透過進食如[螞蟻或](../Page/螞蟻.md "wikilink")[節肢類動物等以吸收牠們的毒素](../Page/節肢類動物.md "wikilink")\[8\]，但亦有一些物種，如[澳洲的](../Page/澳洲.md "wikilink")[科羅澳擬蟾則能夠自己合成這些有毒的](../Page/科羅澳擬蟾.md "wikilink")[生物鹼](../Page/生物鹼.md "wikilink")\[9\]。這些化合物，包括[蛙毒素](../Page/蛙毒素.md "wikilink")（batrachotoxin）及[蟾毒素](../Page/蟾毒素.md "wikilink")（bufotoxin）等，能令受體不適、[產生幻覺](../Page/致幻剂.md "wikilink")、[癲癇性痙攣](../Page/癲癇性痙攣.md "wikilink")（Epileptic
seizure）、[神經中毒及](../Page/神經毒素.md "wikilink")[血管收縮](../Page/血管收縮.md "wikilink")。不少以蛙類為食的捕獵者已能適應高濃度的毒素，但其他物種，如人類則完全沒有抵抗力\[10\]。
一些蛙類則依靠虛張聲勢及小詭計去保護自己。像大蟾蜍獨有的站立姿態就是嘗試去嚇怕敵人。牠們會吸入空氣使身體脹大，並提高後半身及令頭部向下以製造一種雄偉的形態\[11\]。美國牛蛙受到威脅時會將頭部向前傾但合上眼睛。此姿態目的是要將帶毒的腮腺放置在最清晰的位置上，與此同時其他在背部上的腺體則開始滲出有毒的分泌物，並將身體最脆弱的部分加以覆蓋\[12\]。其他策略包括發出刺耳的聲音，並利用突如其來的巨響嚇走捕獵者。灰樹蛙能夠發出如爆破般的聲響，這聲響有時就能夠嚇退其天敵[北短尾鼩鼱](../Page/北短尾鼩鼱.md "wikilink")（*Blarina
brevicauda*）\[13\]。蟾蜍類一般不甚受歡迎，但偶爾也會受到飢餓的蛇類如[东部带蛇](../Page/东部带蛇.md "wikilink")（*Thamnophis
sirtalis*）的垂涎。年幼的[美國蟾蜍如遇到對其感興趣的蛇時](../Page/美國蟾蜍.md "wikilink")，就會蹲下身體並且一動不動。這種不動聲息的自保方法往往令蛇失去了牠們的蹤影而成功自保。但如牠們被蛇的頭部感應到時，就會毫不猶豫地放棄蹲下的姿態而迅速跳走\[14\]。

有的蛙類皮膚分泌毒液以防天敵，生活在[亞馬遜河流域雨林中的一種樹蛙分泌物被當地](../Page/亞馬遜河.md "wikilink")[印第安人用來製作箭毒](../Page/印第安人.md "wikilink")，見血封喉。

## 分布

蛙類分佈在除了[加勒比海島嶼和](../Page/加勒比海.md "wikilink")[太平洋島嶼以外的全世界](../Page/太平洋.md "wikilink")。但目前在全世界迅速地減少，主要原因有[環境](../Page/環境.md "wikilink")[污染](../Page/污染.md "wikilink")、氣候變化、外來物種侵入、人類擴張而造成的棲息環境縮小等。

## 文化

無尾目動物自古以來就在人類文化中佔有一定的地位，許多傳說和它們有關：

  - 「蛙災」—[聖經](../Page/舊約.md "wikilink")[出埃及記](../Page/出埃及記.md "wikilink")，[耶和華予以摩西為埃及降下的第二個災難](../Page/耶和華.md "wikilink")（《出埃及記
    8：2》）。
  - [中國古代認為月亮是三條腿的白玉蟾蜍](../Page/中國.md "wikilink")，因為月亮表面和蟾蜍皮膚一樣凹凸不平；
  - [中國神話認為三條腿的](../Page/中國神話.md "wikilink")[金蟾蜍能夠吐出](../Page/金蟾蜍.md "wikilink")[金錢](../Page/金錢.md "wikilink")，象徵著財富，是廣陽真人[劉海的坐騎](../Page/劉海.md "wikilink")；
  - [辛弃疾](../Page/辛弃疾.md "wikilink")《西江月·夜行黄沙道中》：“听取蛙声一片。”
  - 蛙市是指群蛙齐鸣，有如[闹市](../Page/闹市.md "wikilink")。[葛长庚](../Page/葛长庚.md "wikilink")《夏夜宿水馆》诗：“蛙市无声万籟沉。”[方岳](../Page/方岳.md "wikilink")《农谣》之五：“池塘水满蛙成市，门巷春深燕作家。”
  - 「癞蛤蟆想吃天鵝肉」是中國的俗語；该俗语表示异想天开。
  - 「坐井觀天」是中國的成語，主角是青蛙；
  - 「井底之蛙」是中國的成語，比喻見識淺薄的人。
  - 青蛙王子是西方的童话故事
  - [拉·封丹寓言說青蛙想和牛比賽誰大](../Page/拉·封丹.md "wikilink")，最終將自己吹爆了；
  - [伊索寓言說青蛙想飛上天](../Page/伊索.md "wikilink")，咬着一根棍讓大雁帶上天，一高興張嘴叫，卻掉了下来。
  - 「青蛙」（Frog）是大部分[英语国家的人们对](../Page/英语.md "wikilink")[法国人的](../Page/法国人.md "wikilink")[蔑称](../Page/蔑称.md "wikilink")(因法國人爰吃[田雞腿](../Page/田雞腿.md "wikilink"))。
  - [膜蛤文化是中国的网络次文化](../Page/膜蛤文化.md "wikilink")，对长相酷似蛤蟆的[江泽民进行崇拜](../Page/江泽民.md "wikilink")。

## 參考文獻

## 外部链接

  - [全球两栖动物评估](http://www.globalamphibians.org/)
  - [无尾目数据库](http://www.livingunderworld.org/)
  - [蛙类解剖](http://www-itg.lbl.gov/ITG.hm.pg.docs/Whole.Frog/Whole.Frog.html)
  - [科学家对无尾目动物消失的担忧](http://raysweb.net/specialplaces/pages/frogsdecline.html)
  - [关于紫蛙发现报导和图片](http://news.bbc.co.uk/1/hi/sci/tech/3200214.stm)

[无尾目](../Category/无尾目.md "wikilink")
[Category:兩棲類](../Category/兩棲類.md "wikilink")
[Category:益蟲](../Category/益蟲.md "wikilink")

1.  《金匮要略校注》言：“水鸡即田鸡，蛙也。水鸡声是形容喉间痰鸣声连连不绝，犹如水鸡之声。”

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.
13.
14.