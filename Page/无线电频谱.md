**无线电波**是[频率介于](../Page/频率.md "wikilink")3[赫兹和约](../Page/赫兹.md "wikilink")300[吉赫之间的](../Page/吉赫.md "wikilink")[电磁波](../Page/电磁波.md "wikilink")，也作**射頻電波**，或簡稱**射頻**、**射電**。

**[无线电](../Page/无线电.md "wikilink")**技术將声音[訊號或其他](../Page/訊號.md "wikilink")[信号經過轉換](../Page/信号.md "wikilink")，利用無線電波傳播。

**射頻**技術也用在[核磁共振及](../Page/核磁共振.md "wikilink")[磁振造影上](../Page/磁振造影.md "wikilink")；[射頻線圈是其研究課題之一](../Page/射頻線圈.md "wikilink")。

顾名思义，无线电频谱是无线电波或者电磁波的频率。 无线电波定义在频率在3000GHz以下，不用人工导波而在空间传播的电磁波。
一般而言，无线电频谱是指9kHz-3000GHz频率范围内无线电频率的总称。

无线电波的来源也可能是自然的，比如[射电天文学就是研究来自](../Page/射电天文学.md "wikilink")[外空的无线电波的一门科学](../Page/外空.md "wikilink")。

## 频谱和波段划分

| [国际电信联盟波段号码](../Page/国际电信联盟.md "wikilink") | 频段名称                             | 缩写  | 频率范围                                            | 波段  | 波长范围                 | 用法                                                                                                           |
| ------------------------------------------ | -------------------------------- | --- | ----------------------------------------------- | --- | -------------------- | ------------------------------------------------------------------------------------------------------------ |
|                                            |                                  |     | ≤ 3 [赫兹](../Page/赫兹.md "wikilink")（≤3Hz）        |     | ≥ 100,000 千米         |                                                                                                              |
| 1                                          | [极低频](../Page/极低频.md "wikilink") | ELF | 3-30 赫兹（3Hz–30Hz）                               | 极长波 | 100,000千米 – 10,000千米 | 潛艇通訊或直接转换成声音                                                                                                 |
| 2                                          | [超低频](../Page/超低频.md "wikilink") | SLF | 30–300 赫茲（30Hz–300Hz）                           | 超长波 | 10,000千米 – 1,000千米   | 直接转换成声音或交流输电系统（50-60赫兹）                                                                                      |
| 3                                          | [特低频](../Page/特低频.md "wikilink") | ULF | 300–3000 赫茲（300Hz–3KHz）                         | 特长波 | 1,000千米 – 100千米      | 礦場通訊或直接转换成声音                                                                                                 |
| 4                                          | [甚低频](../Page/甚低频.md "wikilink") | VLF | 3–30 [千赫](../Page/千赫.md "wikilink")（3KHz–30KHz） | 甚长波 | 100千米 – 10千米         | 直接转换成声音、[超声](../Page/超声.md "wikilink")、[地球物理學研究](../Page/地球物理學.md "wikilink")                                |
| 5                                          | [低頻](../Page/低頻.md "wikilink")   | LF  | 30–300 千赫（30KHz–300KHz）                         | 长波  | 10千米 – 1千米           | 國際廣播、全向信標                                                                                                    |
| 6                                          | [中频](../Page/中频.md "wikilink")   | MF  | 300–3000 千赫（300KHz–3MHz）                        | 中波  | 1千米 – 100米           | [調幅](../Page/調幅.md "wikilink")（AM）广播、全向信標、海事及航空通訊                                                            |
| 7                                          | [高頻](../Page/高頻.md "wikilink")   | HF  | 3–30 [兆赫](../Page/兆赫.md "wikilink")（3MHz–30MHz） | 短波  | 100米 – 10米           | 短波、民用電臺                                                                                                      |
| 8                                          | [甚高频](../Page/甚高频.md "wikilink") | VHF | 30–300 兆赫（30MHz–300MHz）                         | 米波  | 10米 – 1米             | [調頻](../Page/調頻.md "wikilink")（FM）廣播、電視廣播、航空通訊                                                               |
| 9                                          | [特高频](../Page/特高频.md "wikilink") | UHF | 300–3000 兆赫（300MHz–3GHz）                        | 分米波 | 1米 – 100毫米           | 電視廣播、無線電話通訊、[無線網絡](../Page/無線網絡.md "wikilink")、[微波爐](../Page/微波爐.md "wikilink")                              |
| 10                                         | [超高频](../Page/超高频.md "wikilink") | SHF | 3–30 [吉赫](../Page/吉赫.md "wikilink")（3GHz–30GHz） | 厘米波 | 100毫米 – 10毫米         | 無線網絡、[雷達](../Page/雷達.md "wikilink")、人造衛星接收                                                                   |
| 11                                         | [极高频](../Page/极高频.md "wikilink") | EHF | 30–300 吉赫（30GHz–300GHz）                         | 毫米波 | 10毫米 – 1毫米           | [射電天文學](../Page/射電天文學.md "wikilink")、[遙感](../Page/遙感.md "wikilink")、[人體掃瞄安檢儀](../Page/人體掃瞄安檢儀.md "wikilink") |
|                                            |                                  |     | \> 300 吉赫（\>300GHz）                             |     | \< 1毫米               |                                                                                                              |

  - 註

<!-- end list -->

  - 極低頻、超低頻、特低頻、甚低頻四個頻帶（頻率約3–30,000赫茲）和[音頻](../Page/音頻.md "wikilink")（audio
    frequency, AF）頻率（約20–20,000赫茲）重疊，雖然聲頻是用在透過空氣傳播的音波上。

## 使用无线电波的家电

  - [电视机](../Page/电视机.md "wikilink")
  - [收音机](../Page/收音机.md "wikilink")
  - [对讲机](../Page/对讲机.md "wikilink")

## 相关条目

  - [无线电](../Page/无线电.md "wikilink")
  - [无线电接收机](../Page/无线电接收机.md "wikilink")

[Category:無線電](../Category/無線電.md "wikilink")
[\*](../Category/頻譜.md "wikilink")