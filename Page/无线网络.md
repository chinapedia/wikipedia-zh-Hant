[Stock_news_bw.png](https://zh.wikipedia.org/wiki/File:Stock_news_bw.png "fig:Stock_news_bw.png")
**無線網路**（）指的是任何型式的[無線電](../Page/無線電.md "wikilink")[電腦網路](../Page/電腦網路.md "wikilink")，普遍和電信網路結合在一起，不需電纜即可在節點之間相互連結\[1\]。無線電信網路一般被應用在使用[電磁波的搖控資訊傳輸系統](../Page/電磁波.md "wikilink")，像是[無線電波作為](../Page/無線電波.md "wikilink")[載波和實體層的網路](../Page/載波.md "wikilink")\[2\]。如：

  - [LTE-A](../Page/長期演進技術升級版.md "wikilink")
  - [LTE](../Page/LTE.md "wikilink")
  - [WCDMA](../Page/WCDMA.md "wikilink")
  - [CDMA2000](../Page/CDMA2000.md "wikilink")
  - [EDGE](../Page/EDGE.md "wikilink")
  - [GSM](../Page/GSM.md "wikilink")
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")
  - [WiMax](../Page/WiMax.md "wikilink")
  - [ZigBee](../Page/ZigBee.md "wikilink")

無線網路的發展方向之一就是“萬有無線網路技術”，也就是將各種不同的無線網路統一在單一的設備下。[Intel正在開發的一個](../Page/Intel.md "wikilink")[晶片採用](../Page/晶片.md "wikilink")[軟體無線電技術](../Page/軟體無線電.md "wikilink")，可以在同一個晶片上處理[WiFi](../Page/WiFi.md "wikilink")、[WiMAX和](../Page/WiMAX.md "wikilink")[DVB-H](../Page/DVB-H.md "wikilink")[數位電視等不同無線技術](../Page/數位電視.md "wikilink")\[3\]。

## 類型

### 無線個人網

[Transmitting_tower_top_us.jpg](https://zh.wikipedia.org/wiki/File:Transmitting_tower_top_us.jpg "fig:Transmitting_tower_top_us.jpg")
[NBN_Co_Wireless_Network_Termination_Device_located_in_the_NBN_Co_Discovery_Truck.jpg](https://zh.wikipedia.org/wiki/File:NBN_Co_Wireless_Network_Termination_Device_located_in_the_NBN_Co_Discovery_Truck.jpg "fig:NBN_Co_Wireless_Network_Termination_Device_located_in_the_NBN_Co_Discovery_Truck.jpg")
[Wavelan_Half_ISA_915.jpg](https://zh.wikipedia.org/wiki/File:Wavelan_Half_ISA_915.jpg "fig:Wavelan_Half_ISA_915.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:USB-wireless-adapter.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Wireless_rede.gif "fig:缩略图")
[無線個人網](../Page/無線個人網.md "wikilink")（WPAN）是在小範圍內相互連接數個裝置所形成的無線網路，通常是個人可及的範圍內。例如[藍牙連接耳機及膝上電腦](../Page/藍牙.md "wikilink")，[ZigBee也提供了無線個人網的應用平台](../Page/ZigBee.md "wikilink")\[4\]。

### 無線區域網

[無線區域網路](../Page/無線區域網路.md "wikilink")（WLAN）類似其他無線裝置，利用無線電而非電纜在同一個網路上傳送資料、甚至無線上網，是[IEEE
802.11系列標準](../Page/IEEE_802.11.md "wikilink")。

  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")
  - [Fixed Wireless Data](../Page/Fixed_Wireless_Data.md "wikilink")

### 無線城域網

無線城域網是連接數個無線區域網的無線網路型式。

  - [WiMAX](../Page/WiMAX.md "wikilink")

### 行動裝置網路

  - [全球移动通信系统](../Page/全球移动通信系统.md "wikilink")（[GSM](../Page/GSM.md "wikilink")）：GSM網絡分成三個主要系統：轉接系統、基地系統、操作和支持系統。[行動電話連接到基地系統](../Page/行動電話.md "wikilink")，然後連接到操作和支持系統；然後連接到轉接系統後，[電話就會被轉到要到的地方](../Page/電話.md "wikilink")。GSM是大多數手機最常見的使用標準\[5\]。
  - [個人通信服務](../Page/個人通信服務.md "wikilink")（[PCS](../Page/PCS.md "wikilink")）：PCS是[北美地區的一種可藉由](../Page/北美地區.md "wikilink")[行動電話使用的無線電頻帶](../Page/行動電話.md "wikilink")。[斯普林特](../Page/斯普林特.md "wikilink")（Sprint）正好是第一家創立PCS的服務。
  - [D-AMPS](../Page/D-AMPS.md "wikilink")：即數位高階行動電話服務，由[AMPS升級的版本](../Page/AMPS.md "wikilink")，但是因為技術的進步。新的網絡如[GSM](../Page/GSM.md "wikilink")、[3G等正取代較舊的](../Page/3G.md "wikilink")[AMPS系統](../Page/AMPS.md "wikilink")。

## 應用

無線網路對世界的重大變革最早可追溯至[第二次世界大戰時期](../Page/第二次世界大戰.md "wikilink")。由於無線網路的應用，訊息的傳輸能夠輕易、有效且確實地越洋及越過敵軍戰線。從此，無線網路技術持續發展，地位也越來越重要。[行動電話就是無線網路系統的一部分](../Page/行動電話.md "wikilink")，人們每天使用行動電話與他人通話。經由利用人造衛星及其他訊號，無線網路系統使越洋訊息的傳送化為可能。在[災難應對上](../Page/災難應對.md "wikilink")，警局使用無線網路迅速地傳播重要訊息；不論是在小型辦公大樓內或橫越整個地球，個人及公司都利用無線網路快速地發送或分享資料\[6\]。

無線網路的其他重要應用之一，就是在基礎電信建設貧乏或缺乏資源的國家和地區提供一個便宜及快速的管道連接上[網際網路](../Page/網際網路.md "wikilink")，像是大部分的[發展中國家](../Page/發展中國家.md "wikilink")。

而在使用無線網路時，相容性的問題也同時浮現。不同的製造廠商所生產的元件可能無法在同一個平台使用，或是需要額外的作業程序來使修正這些問題。基本上無線網路比起經由[乙太網電纜直接連線要來的慢](../Page/乙太網.md "wikilink")。

無線網路比較容易受到攻擊，因為任何人都可以嘗試去入侵無線網路的訊號。許多網路提供[有線等效加密](../Page/有線等效加密.md "wikilink")（[WEP](../Page/WEP.md "wikilink")）防護系統，但它其實也相當容易受到攻擊。雖然WEP能夠擋掉一些入侵者，但許多公司基於安全性考量，仍堅持使用有線網路直到問題改善為止。另一種無線網路防護系統為[WPA](../Page/WPA.md "wikilink")（Wi-Fi
Protected Access）。WPA提供了比WEP更安全的無線網路環境，而這道防火牆可以幫助易受入侵的無線網路修補漏洞。

## 健康疑慮

最近已有許多議題及研究連接無線通訊與疾病之間的關連性。但目前沒有研究能確實證明無線通訊與疾病的關連及影響。\[7\]\[8\]

## 參見

  - [無線區域網路](../Page/無線區域網路.md "wikilink")
  - [無線接取器](../Page/無線接取器.md "wikilink")
  - [實體層](../Page/實體層.md "wikilink")
  - [全球互通微波存取](../Page/全球互通微波存取.md "wikilink")

## 参考文献

## 外部連結

  -
[de:Kabellose
Übertragungsverfahren](../Page/de:Kabellose_Übertragungsverfahren.md "wikilink")

[Category:无线网络](../Category/无线网络.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.