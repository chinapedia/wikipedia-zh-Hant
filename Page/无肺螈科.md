**无肺螈科**是[蠑螈中最大](../Page/蠑螈.md "wikilink")、多樣性最豐富的一科，約有400種\[1\]。分布於[溫帶](../Page/溫帶.md "wikilink")、[亞熱帶和](../Page/亞熱帶.md "wikilink")[熱帶地區](../Page/熱帶.md "wikilink")，是唯一分布於熱帶的蠑螈。除了5種*Speleomantes*屬分布於[義大利和一種](../Page/義大利.md "wikilink")*Karsenia*屬分布於[韓國以外](../Page/韓國.md "wikilink")，所有種類的无肺螈都分布於[西半球](../Page/西半球.md "wikilink")。

肋骨間溝明顯。無[肺部構造](../Page/肺.md "wikilink")，完全使用皮膚進行氣體交換。身體纖細，四肢短小。通常有背部條紋。

## 分類

  - 脊口螈亚科 Desmognathinae <small>Gray, 1850</small>
      - [脊口螈属](../Page/脊口螈属.md "wikilink") *Desmognathus* <small>Baird,
        1850</small>
      - [深口螈属](../Page/深口螈属.md "wikilink") *Phaeognathus*
        <small>Highton, 1961</small>
  - 无肺螈亚科 Plethodontinae <small>Gray, 1850</small>
      - [攀螈属](../Page/攀螈属.md "wikilink") *Aneides* <small>Baird,
        1851</small>
      - [蜥尾螈属](../Page/蜥尾螈属.md "wikilink") *Batrachoseps*
        <small>[Bonaparte](../Page/夏尔·吕西安·波拿巴.md "wikilink"),
        1839</small>
      - [游舌螈属](../Page/游舌螈属.md "wikilink") *Bolitoglossa*
        <small>Duméril, Bibron and Duméril, 1854</small>
      - [缓螈属](../Page/缓螈属.md "wikilink") *Bradytriton* <small>Wake and
        Elias, 1983</small>
      - [膜螈属](../Page/膜螈属.md "wikilink") *Chiropterotriton*
        <small>Taylor, 1944</small>
      - *Cryptotriton* <small>García-París and Wake, 2000</small>
      - [丛螈属](../Page/丛螈属.md "wikilink") *Dendrotriton* <small>Wake and
        Elias, 1983</small>
      - [剑螈属](../Page/剑螈属.md "wikilink") *Ensatina* <small>Gray,
        1850</small>
      - [河溪螈属](../Page/河溪螈属.md "wikilink") *Eurycea* <small>Rafinesque,
        1822</small>
      - [泉螈属](../Page/泉螈属.md "wikilink") *Gyrinophilus* <small>Cope,
        1869</small>
      - [瞎螈属](../Page/瞎螈属.md "wikilink") *Haideotriton* <small>Carr,
        1939</small>
      - [半趾螈属](../Page/半趾螈属.md "wikilink") *Hemidactylium*
        <small>Tschudi, 1838</small>
      - [穴螈属](../Page/穴螈属.md "wikilink") *Hydromantes* <small>Gistel,
        1848</small>
      - *Ixalotriton* <small>Wake and Johnson, 1989</small>
      - *Karsenia* <small>Min, Yang, Bonett, Vieites, Brandon and Wake,
        2005</small>
      - [条螈属](../Page/条螈属.md "wikilink") *Lineatriton* <small>Tanner,
        1950</small>
      - [南螈属](../Page/南螈属.md "wikilink") *Nototriton* <small>Wake and
        Elias, 1983</small>
      - [夜色螈属](../Page/夜色螈属.md "wikilink") *Nyctanolis* <small>Elias and
        Wake, 1983</small>
      - [板足螈属](../Page/板足螈属.md "wikilink") *Oedipina* <small>Keferstein,
        1868</small>
      - [儒螈属](../Page/儒螈属.md "wikilink") *Parvimolge* <small>Taylor,
        1944</small>
      - [无肺螈属](../Page/无肺螈属.md "wikilink") *Plethodon* <small>Tschudi,
        1838</small>
      - [涓螈属](../Page/涓螈属.md "wikilink") *Pseudoeurycea* <small>Taylor,
        1944</small>
      - [土螈属](../Page/土螈属.md "wikilink") *Pseudotriton* <small>Tschudi,
        1838</small>
      - *Speleomantes* <small>Dubois, 1984</small>
      - [合颌螈属](../Page/合颌螈属.md "wikilink") *Stereochilus* <small>Cope,
        1869</small>
      - [索里螈属](../Page/索里螈属.md "wikilink") *Thorius* <small>Cope,
        1869</small>

## 參考

  -
[Category:蠑螈亞目](../Category/蠑螈亞目.md "wikilink")
[Category:無肺螈科](../Category/無肺螈科.md "wikilink")

1.  Min, M.S., S. Y. Yang, R. M. Bonett, D. R. Vieites, R. A. Brandon &
    D. B. Wake. (2005). Discovery of the first Asian plethodontid
    salamander. *Nature* (435), 87-90 ([5
    May](../Page/5_May.md "wikilink") 2005)