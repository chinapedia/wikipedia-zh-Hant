在[量綱分析中](../Page/量綱分析.md "wikilink")，**無量綱量**，或称**-{zh-hans:无因次量;
zh-hant:無量綱量}-**、**无维量**、**无维度量**、**无维数量**、**无次元量**等，指的是沒有[量綱的](../Page/量綱.md "wikilink")[量](../Page/量_\(物理\).md "wikilink")。它是個單純的數字，量綱為[1](../Page/1.md "wikilink")\[1\]。無量綱量在[數學](../Page/數學.md "wikilink")、[物理學](../Page/物理學.md "wikilink")、[工程學](../Page/工程學.md "wikilink")、[經濟學以及日常生活中](../Page/經濟學.md "wikilink")（如數數）被廣泛使用。一些廣為人知的無量綱量包括[圓周率](../Page/圓周率.md "wikilink")（[π](../Page/π.md "wikilink")）、[歐拉常數](../Page/e_\(数学常数\).md "wikilink")（[e](../Page/e.md "wikilink")）和[黃金分割率](../Page/黃金分割率.md "wikilink")（[φ](../Page/φ.md "wikilink")）等。與之相對的是有量綱量，擁有諸如長度、面積、時間等單位。

無量綱量常寫作兩個有量綱量之[積或](../Page/積.md "wikilink")[比](../Page/比例.md "wikilink")，但其最終的綱量互相消除後會得出無量綱量。比如，應變是量度[形變的量](../Page/形變.md "wikilink")，定義為長度差與原先長度之比。但由於兩者的量綱均為*L*（長度），因此相除後得出的量是沒有量綱的。

## 屬性

  - 雖然無量綱量本身沒有量綱，但是它也有時被加以無量綱的[單位](../Page/計量單位.md "wikilink")。在分子和分母使用同樣的單位（kg/kg或mol/mol），有時可以幫助表達所測量的數值（如[質量百分濃度或](../Page/質量百分濃度.md "wikilink")[摩爾分數等](../Page/摩爾分數.md "wikilink")）。某些量還可以表示為不同的單位之比，但這兩個單位的量綱相同（如[光年除以](../Page/光年.md "wikilink")[米](../Page/米.md "wikilink")）。這種做法可以用於計算圖表中的[斜率](../Page/斜率.md "wikilink")，或者進行單位轉換。這樣的寫法並不意味著存在量綱，而只不過是符號表達上的慣例。其他常用的無量綱量有：%=0.01，[百分率](../Page/百分率.md "wikilink")；‰=0.001，[千分率](../Page/千分率.md "wikilink")；ppm=10<sup>−6</sup>，[百萬分率](../Page/百萬分率.md "wikilink")；ppb（=10<sup>−9</sup>，[十億分率](../Page/十億分率.md "wikilink")；ppt=10<sup>−12</sup>，[兆分率](../Page/兆分率.md "wikilink")（萬億分率）以及角度單位（[度](../Page/角度.md "wikilink")、[弧度](../Page/弧度.md "wikilink")、[梯度](../Page/梯度.md "wikilink")）等等。

<!-- end list -->

  - 兩個具有相同量綱之比是沒有量綱的，而且無論用甚麼單位計算，該量還是不變的。例如，如果物體**A**對物體**B**施大小為*F*的作用力，那**B**也會向**A**施大小為*f*的力。兩個力的比率*F*/*f*永遠等於1（見[牛頓第三定律](../Page/牛頓第三定律.md "wikilink")），而不取決於測量*F*和*f*所用的單位。這是因為物理中一個重要的假設：物理定律是獨立於人們選用的單位制的。如果以上的*F*/*f*不經常等於1，而在我們從[國際單位制轉用](../Page/國際單位制.md "wikilink")[厘米-克-秒制時改變了的話](../Page/厘米-克-秒制.md "wikilink")，這就意味著牛頓第三定律的真偽要看我們選取哪一種單位制，而這就與假設矛盾了。這一假設是[白金漢π定理的基礎](../Page/白金漢π定理.md "wikilink")，其表述為：所有物理定律均能以數個無量綱量的數學組合（加、減、乘、除等等）寫成[恆等式](../Page/恆等式.md "wikilink")。如果無量綱量組合後的值在替換所用單位制後改變了的話，那麼白金漢π定理就不成立了。

## 白金漢π定理

[白金漢π定理的另一項推論為](../Page/白金漢π定理.md "wikilink")，如果*n*個[變數之間有某種](../Page/變數.md "wikilink")[函數關係](../Page/函數.md "wikilink")，而這些變數中有*k*個獨立的量綱，則可以產生*p*
= *n* − *k*個獨立的無量綱量。

### 例子

某[磁力攪拌器的](../Page/磁力攪拌器.md "wikilink")[電功率是被攪拌液體的](../Page/電功率.md "wikilink")[密度和](../Page/密度.md "wikilink")[黏度](../Page/黏度.md "wikilink")、攪拌器的[直徑及攪拌速度的函數](../Page/直徑.md "wikilink")。因此這裡共有*n*
= 5個變量

這*n* = 5個變量共由以下*k* = 3個量綱組成：

  - 長度：*L* (m)
  - 時間：*T* (s)
  - 質量：*M* (kg)

根據該定理，通過組合這*n* = 5個變量，可以得出*p* = *n* − *k* = 5 − 3 =
2個獨立的無量綱量。此例中的這兩個無量綱量分別為：

  - [雷諾數](../Page/雷諾數.md "wikilink")（描述流體流動的無量綱量）
  - [功率數](../Page/功率數.md "wikilink")（描述攪拌器，同時包含流體密度的無量綱量）

## 例子

  - 在10個蘋果中，有1個是壞了的。總蘋果數中壞蘋果的比例為1個蘋果/10個蘋果= 0.1 = 10%，這是個無量綱量。
  - [角](../Page/角.md "wikilink")：角度的定義為，以圓心為頂點劃出的弧的長度除以某另一長度。這個比率由長度除以長度所得，因此是個無量綱量。當所用的（無量綱）單位為[弧度時](../Page/弧度.md "wikilink")，那個「另一長度」就是圓的[半徑](../Page/半徑.md "wikilink")。當單位為[角度時](../Page/角度.md "wikilink")，「另一長度」就是圓[周長的](../Page/周長.md "wikilink")360分之1。
  - [圓周率是個無量綱量](../Page/圓周率.md "wikilink")，定義為圓周長與直徑之比。該數值無論在用甚麼單位量度這些[長度時](../Page/長度.md "wikilink")（[厘米](../Page/厘米.md "wikilink")、[英里](../Page/英里.md "wikilink")、[光年等等](../Page/光年.md "wikilink")）都會是相同的，只要周長和直徑以同樣的單位量度。

## 無量綱量列表

下表中所有的量均為無量綱量：

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>標準符號</p></th>
<th><p>定義</p></th>
<th><p>應用範疇</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿贝数.md" title="wikilink">阿贝数</a></p></td>
<td><p><em>V</em></p></td>
<td><p><span class="math inline">$V = \frac{ n_d - 1 }{ n_F - n_C }$</span></p></td>
<td><p><a href="../Page/光學.md" title="wikilink">光學</a>（<a href="../Page/光的色散.md" title="wikilink">光的色散</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/活度系數.md" title="wikilink">活度系數</a></p></td>
<td><p><em>γ</em></p></td>
<td><p><span class="math inline">$\gamma= \frac {{a}}{{x}}$</span></p></td>
<td><p><a href="../Page/化學.md" title="wikilink">化學</a>（活躍分子或原子佔總數之比）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/反照率.md" title="wikilink">反照率</a></p></td>
<td><p><span class="math inline"><em>α</em></span></p></td>
<td><p><span class="math inline">${\alpha}= (1-D) \bar \alpha(\theta_i) + D \bar{ \bar \alpha}$</span></p></td>
<td><p><a href="../Page/氣候學.md" title="wikilink">氣候學</a>、<a href="../Page/天文學.md" title="wikilink">天文學</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勞侖茲因子.md" title="wikilink">勞侖茲因子</a></p></td>
<td><p><span class="math inline"><em>γ</em></span></p></td>
<td><p><span class="math inline">$\gamma = \frac{1}{\sqrt{1-\frac{v^2}{c^2} } } = \frac{1}{\sqrt{1- \beta^2} }$</span></p></td>
<td><p><a href="../Page/相對論.md" title="wikilink">相對論</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿基米德數.md" title="wikilink">阿基米德數</a></p></td>
<td><p><em>Ar</em></p></td>
<td><p><span class="math inline">$Ar = \frac{g L^3 \rho_\ell (\rho - \rho_\ell)}{\mu^2}$</span></p></td>
<td><p><a href="../Page/密度.md" title="wikilink">密度差造成的</a><a href="../Page/流體.md" title="wikilink">流體運動</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿倫尼烏斯數.md" title="wikilink">阿倫尼烏斯數</a></p></td>
<td><p><span class="math inline"><em>α</em></span></p></td>
<td></td>
<td><p><a href="../Page/活化能.md" title="wikilink">活化能與</a><a href="../Page/熱能.md" title="wikilink">熱能之比</a>[2]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/相對原子質量.md" title="wikilink">相對原子質量</a></p></td>
<td><p><em>M</em></p></td>
<td></td>
<td><p><a href="../Page/化學.md" title="wikilink">化學</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伯格诺德数.md" title="wikilink">伯格诺德数</a></p></td>
<td><p><em>Ba</em></p></td>
<td><p><span class="math inline">$Ba = \frac{\rho d^2 \lambda^{1/2} \gamma}{\mu}$</span></p></td>
<td><p>固體塊的流動（如米粒或沙子）[3]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Bejan數.md" title="wikilink">Bejan數</a><br />
<small>（熱力學）</small></p></td>
<td><p><em>Be</em></p></td>
<td><p><span class="math inline">$Be = \frac {\dot S'_{gen, \Delta T}} {\dot S'_{gen, \Delta T}+ \dot S'_{gen, \Delta p}}$</span></p></td>
<td><p>熱傳導不可逆性與由於熱傳導和流體阻力的總不可逆性之比[4]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Bejan數.md" title="wikilink">Bejan數</a><br />
<small>（流體力學）</small></p></td>
<td><p><em>Be</em></p></td>
<td><p><span class="math inline">$Be = \frac{\Delta P . L^2} {\mu \alpha}$</span></p></td>
<td><p>沿著通道的壓力差[5]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賓漢數.md" title="wikilink">賓漢數</a></p></td>
<td><p><em>Bm</em></p></td>
<td><p><span class="math inline">$Bm = \frac{ \tau_yL }{ \mu V }$</span></p></td>
<td><p>屈服應力與黏滯應力之比[6]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/毕奥数.md" title="wikilink">毕奥数</a></p></td>
<td><p><em>Bi</em></p></td>
<td><p><span class="math inline">$Bi = \frac{h L_C}{\ k_b}$</span></p></td>
<td><p>固體的表面傳導率與體積傳導率之比</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Bl</em>或<em>B</em></p></td>
<td><p><span class="math inline">$B = \frac{V \rho}{\mu ( 1-\epsilon) D}$</span></p></td>
<td><p>流體穿過多孔介質時慣性相對黏滯力的重要性</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/博登斯坦数.md" title="wikilink">博登斯坦数</a></p></td>
<td><p><em>Bo</em></p></td>
<td><p><span class="math inline"><em>B</em><em>o</em> = <em>R</em><em>e</em> ⋅ <em>S</em><em>c</em> = <em>v</em><em>L</em>/𝒟</span></p></td>
<td><p><a href="../Page/停留時間.md" title="wikilink">停留時間的分佈</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邦德數.md" title="wikilink">邦德數</a></p></td>
<td><p><em>Bo</em></p></td>
<td><p><span class="math inline">$Bo = \frac{\rho a L^2}{\gamma}$</span></p></td>
<td><p>由<a href="../Page/浮力.md" title="wikilink">浮力推動的</a><a href="../Page/毛細作用.md" title="wikilink">毛細作用</a>[7]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布林克曼數.md" title="wikilink">布林克曼數</a></p></td>
<td><p><em>Br</em></p></td>
<td><p><span class="math inline">$Br = \frac {\mu U^2}{\kappa(T_w-T_0)}$</span></p></td>
<td><p>從容器壁到黏性流體的熱傳導</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Brownell-Katz數.md" title="wikilink">Brownell-Katz數</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/毛細管數.md" title="wikilink">毛細管數和</a><a href="../Page/邦德數.md" title="wikilink">邦德數的組合</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/毛細管數.md" title="wikilink">毛細管數</a></p></td>
<td><p><em>Ca</em></p></td>
<td></td>
<td><p>受<a href="../Page/表面張力.md" title="wikilink">表面張力影響的流體流動</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><span class="math inline"> <em>Q</em></span></p></td>
<td><p><span class="math inline">${Q}\ =\ \frac{{B_0}^2 d^2}{\mu_0 \rho \nu \lambda}$</span></p></td>
<td><p>磁<a href="../Page/對流.md" title="wikilink">對流</a>，用以表達<a href="../Page/洛伦兹力.md" title="wikilink">洛伦兹力與</a><a href="../Page/黏度.md" title="wikilink">黏度之比</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靜摩擦係數.md" title="wikilink">靜摩擦係數</a></p></td>
<td><p><span class="math inline"><em>μ</em><sub><em>s</em></sub></span></p></td>
<td></td>
<td><p>物體間的靜摩擦</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/動摩擦係數.md" title="wikilink">動摩擦係數</a></p></td>
<td><p><span class="math inline"><em>μ</em><sub><em>k</em></sub></span></p></td>
<td></td>
<td><p>物體互相滑動時的摩擦</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柯尔伯恩j因数.md" title="wikilink">柯尔伯恩j因数</a></p></td>
<td></td>
<td></td>
<td><p>熱傳導的無量綱係數</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庫朗數.md" title="wikilink">庫朗數</a></p></td>
<td><p><span class="math inline"><em>ν</em></span></p></td>
<td></td>
<td><p><a href="../Page/雙曲型偏微分方程.md" title="wikilink">雙曲型偏微分方程之解</a>[8]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/达姆科勒数.md" title="wikilink">达姆科勒数</a></p></td>
<td><p><em>Da</em></p></td>
<td><p><span class="math inline"><em>D</em><em>a</em> = <em>k</em><em>τ</em></span></p></td>
<td><p>反應時間與共振時間之比</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阻尼比.md" title="wikilink">阻尼比</a></p></td>
<td><p><span class="math inline"><em>ζ</em></span></p></td>
<td><p><span class="math inline">$\zeta = \frac{c}{2 \sqrt{km}}$</span></p></td>
<td><p>系統中<a href="../Page/阻尼.md" title="wikilink">阻尼的程度</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/達西阻力係數.md" title="wikilink">達西阻力係數</a></p></td>
<td><p><span class="math inline"><em>C</em><sub><em>f</em></sub></span>或<span class="math inline"><em>f</em></span></p></td>
<td></td>
<td><p>流體流動</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狄恩数.md" title="wikilink">狄恩数</a></p></td>
<td><p><em>D</em></p></td>
<td></td>
<td><p>彎曲管道中的流體<a href="../Page/渦.md" title="wikilink">渦</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/底波拉数.md" title="wikilink">底波拉数</a></p></td>
<td><p><em>De</em></p></td>
<td></td>
<td><p><a href="../Page/粘彈性.md" title="wikilink">粘彈性流體的</a><a href="../Page/流動學.md" title="wikilink">流動學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/分貝.md" title="wikilink">分貝</a></p></td>
<td><p><em>dB</em></p></td>
<td></td>
<td><p>兩個強度之比，通常用於聲音</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阻力系數.md" title="wikilink">阻力系數</a></p></td>
<td><p><span class="math inline"><em>C</em><sub><em>d</em></sub></span></p></td>
<td></td>
<td><p>流動阻力</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Dukhin數.md" title="wikilink">Dukhin數</a></p></td>
<td><p><em>Du</em></p></td>
<td></td>
<td><p>異質系統中表面<a href="../Page/電導率.md" title="wikilink">電導率與體積電導率之比</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐拉常數.md" title="wikilink">歐拉常數</a></p></td>
<td><p><em>e</em></p></td>
<td></td>
<td><p><a href="../Page/數學.md" title="wikilink">數學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/埃克特数.md" title="wikilink">埃克特数</a></p></td>
<td><p><em>Ec</em></p></td>
<td></td>
<td><p>熱對流傳導</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃克曼数.md" title="wikilink">埃克曼数</a></p></td>
<td><p><em>Ek</em></p></td>
<td></td>
<td><p><a href="../Page/地球物理學.md" title="wikilink">地球物理學</a>（黏質阻力）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弹性_(经济学).md" title="wikilink">彈性</a></p></td>
<td><p><em>E</em></p></td>
<td></td>
<td><p><a href="../Page/經濟學.md" title="wikilink">經濟學</a>，常用於量度<a href="../Page/供給和需求.md" title="wikilink">供給和需求如何受價格變化的影響</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厄特沃什数.md" title="wikilink">厄特沃什数</a></p></td>
<td><p><em>Eo</em></p></td>
<td></td>
<td><p>判斷汽泡或液滴形狀</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/埃里克森数.md" title="wikilink">埃里克森数</a></p></td>
<td><p><em>Er</em></p></td>
<td></td>
<td><p>液晶流動特性</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐拉數_(物理學).md" title="wikilink">歐拉數 (物理學)</a></p></td>
<td><p><em>Eu</em></p></td>
<td></td>
<td><p><a href="../Page/流體動力學.md" title="wikilink">流體動力學</a>（壓力與慣性力之比）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/過量溫度係數.md" title="wikilink">過量溫度係數</a></p></td>
<td><p><em>Θ<sub>r</sub></em></p></td>
<td><p><span class="math inline">$\Theta_r = \frac{T-T_e}{U_e^2/(2c_p)}$</span></p></td>
<td><p>熱力學與流體動力學[9]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>f</em></p></td>
<td></td>
<td><p>管道中的流體流動[10]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/费根鲍姆常数.md" title="wikilink">费根鲍姆常数</a></p></td>
<td><p><span class="math inline"><em>α</em>, <em>δ</em></span></p></td>
<td></td>
<td><p><a href="../Page/混沌理論.md" title="wikilink">混沌理論</a>（週期倍增）[11]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/精細結構常數.md" title="wikilink">精細結構常數</a></p></td>
<td><p><span class="math inline"><em>α</em></span></p></td>
<td><p><span class="math inline">$\alpha = \frac{e^2}{2\varepsilon_0 hc}$</span></p></td>
<td><p><a href="../Page/量子電動力學.md" title="wikilink">量子電動力學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/焦比.md" title="wikilink">焦比</a></p></td>
<td><p><span class="math inline"><em>f</em></span></p></td>
<td></td>
<td><p><a href="../Page/光學.md" title="wikilink">光學</a>、<a href="../Page/攝影.md" title="wikilink">攝影</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Foppl-von_Karman數.md" title="wikilink">Foppl-von Karman數</a></p></td>
<td></td>
<td></td>
<td><p>薄壳失稳</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傅里叶数.md" title="wikilink">傅里叶数</a></p></td>
<td><p><em>Fo</em></p></td>
<td></td>
<td><p><a href="../Page/熱傳導.md" title="wikilink">熱傳導</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/菲涅耳数.md" title="wikilink">菲涅耳数</a></p></td>
<td><p><em>F</em></p></td>
<td></td>
<td><p>狹縫<a href="../Page/衍射.md" title="wikilink">衍射</a>[12]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福禄数.md" title="wikilink">福禄数</a></p></td>
<td><p><em>Fr</em></p></td>
<td><p><span class="math inline">$Fr = \frac{V}{\sqrt{g\ell}}$</span></p></td>
<td><p><a href="../Page/波.md" title="wikilink">波和表面行為</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/增益.md" title="wikilink">增益</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/電子學.md" title="wikilink">電子學</a>（信號輸出與信號輸入之比）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/速比.md" title="wikilink">速比</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/單車.md" title="wikilink">單車傳動</a>[13]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伽利莱数.md" title="wikilink">伽利莱数</a></p></td>
<td><p><em>Ga</em></p></td>
<td></td>
<td><p>引力造成的黏質流動</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃金分割比.md" title="wikilink">黃金分割比</a></p></td>
<td><p><span class="math inline"><em>φ</em></span></p></td>
<td></td>
<td><p><a href="../Page/數學.md" title="wikilink">數學</a>、<a href="../Page/美學.md" title="wikilink">美學</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格雷茨数.md" title="wikilink">格雷茨数</a></p></td>
<td><p><em>Gz</em></p></td>
<td></td>
<td><p>熱流</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格拉斯霍夫数.md" title="wikilink">格拉斯霍夫数</a></p></td>
<td><p><em>Gr</em></p></td>
<td></td>
<td><p>自由<a href="../Page/對流.md" title="wikilink">對流</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重力耦合常數.md" title="wikilink">重力耦合常數</a></p></td>
<td><p><span class="math inline"><em>α</em><sub><em>G</em></sub></span></p></td>
<td><p><span class="math inline">$\alpha_G=\frac{Gm_e^2}{\hbar c}$</span></p></td>
<td><p><a href="../Page/重力.md" title="wikilink">重力</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八田數.md" title="wikilink">八田數</a></p></td>
<td><p><em>Ha</em></p></td>
<td></td>
<td><p>化學反應造成的吸附增強</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈根數.md" title="wikilink">哈根數</a></p></td>
<td><p><em>Hg</em></p></td>
<td></td>
<td><p>強制對流</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水力梯度.md" title="wikilink">水力梯度</a></p></td>
<td><p><em>i</em></p></td>
<td></td>
<td><p><a href="../Page/地下水.md" title="wikilink">地下水流動</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雅各布数.md" title="wikilink">雅各布数</a></p></td>
<td><p><em>Ja</em></p></td>
<td><p><span class="math inline">$Ja = \frac{c_p (T_s - T_{sat}) }{h_{fg} }$</span></p></td>
<td><p>液汽相变時所吸收的顯能與潛能之比[14]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Karlovitz數.md" title="wikilink">Karlovitz數</a></p></td>
<td></td>
<td></td>
<td><p>湍流燃烧</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Kc數.md" title="wikilink">Kc數</a></p></td>
<td><p><span class="math inline"><em>K</em><sub><em>C</em></sub></span></p></td>
<td></td>
<td><p>震盪流場中物體的<a href="../Page/阻力.md" title="wikilink">阻力與</a><a href="../Page/慣性.md" title="wikilink">慣性之比</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克努森数.md" title="wikilink">克努森数</a></p></td>
<td><p><em>Kn</em></p></td>
<td></td>
<td><p>分子<a href="../Page/平均自由程.md" title="wikilink">平均自由程長度與某代表性長度之比</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尿素清除指數.md" title="wikilink">尿素清除指數</a></p></td>
<td><p><em>Kt/V</em></p></td>
<td></td>
<td><p><a href="../Page/醫學.md" title="wikilink">醫學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Kutateladze數.md" title="wikilink">Kutateladze數</a></p></td>
<td><p><em>K</em></p></td>
<td></td>
<td><p>兩相逆流</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉普拉斯数.md" title="wikilink">拉普拉斯数</a></p></td>
<td><p><em>La</em></p></td>
<td></td>
<td><p><a href="../Page/混溶.md" title="wikilink">混溶流體中的自由對流</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/路易斯数.md" title="wikilink">路易斯数</a></p></td>
<td><p><em>Le</em></p></td>
<td></td>
<td><p>質量擴散率與熱擴散率之比</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/升力係數.md" title="wikilink">升力係數</a></p></td>
<td><p><span class="math inline"><em>C</em><sub><em>L</em></sub></span></p></td>
<td></td>
<td><p>在某<a href="../Page/攻角.md" title="wikilink">攻角下</a><a href="../Page/翼型.md" title="wikilink">翼型的</a><a href="../Page/升力.md" title="wikilink">升力</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Lockhart-Martinelli參數.md" title="wikilink">Lockhart-Martinelli參數</a></p></td>
<td><p><span class="math inline"><em>χ</em></span></p></td>
<td></td>
<td><p>濕氣的流動 [15]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乐甫数.md" title="wikilink">乐甫数</a></p></td>
<td></td>
<td></td>
<td><p>地球的硬性</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伦德奎斯特数.md" title="wikilink">伦德奎斯特数</a></p></td>
<td><p><span class="math inline"><em>S</em></span></p></td>
<td></td>
<td><p>ratio of a resistive time to an <a href="../Page/Alfvén_wave.md" title="wikilink">Alfvén wave</a> crossing time in a plasma</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马赫数.md" title="wikilink">马赫数</a></p></td>
<td><p><em>M</em></p></td>
<td><p><span class="math inline">$\ M = \frac {{V}}{{a}}$</span></p></td>
<td><p><a href="../Page/氣體動力學.md" title="wikilink">氣體動力學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/磁雷诺数.md" title="wikilink">磁雷诺数</a></p></td>
<td><p><span class="math inline"><em>R</em><sub><em>m</em></sub></span></p></td>
<td></td>
<td><p><a href="../Page/磁流体力学.md" title="wikilink">磁流体力学</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼宁糙率系数.md" title="wikilink">曼宁糙率系数</a></p></td>
<td><p><em>n</em></p></td>
<td></td>
<td><p>開放管道流體流動（由引力推動）[16]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马兰戈尼数.md" title="wikilink">马兰戈尼数</a></p></td>
<td><p><em>Mg</em></p></td>
<td></td>
<td><p>由熱表面張力偏差引起的<a href="../Page/马兰戈尼效应.md" title="wikilink">马兰戈尼流</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫顿数.md" title="wikilink">莫顿数</a></p></td>
<td><p><em>Mo</em></p></td>
<td></td>
<td><p>判斷汽泡或液滴形狀</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彭巴數.md" title="wikilink">彭巴數</a></p></td>
<td><p><em><span class="math inline"><em>K</em><sub><em>M</em></sub></span></em></p></td>
<td></td>
<td><p>溶液冷凍時的熱傳導與擴散[17]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/努塞尔特数.md" title="wikilink">努塞尔特数</a></p></td>
<td><p><em>Nu</em></p></td>
<td><p><span class="math inline">$Nu =\frac{hd}{k}$</span></p></td>
<td><p>強制對流下的<a href="../Page/熱傳導.md" title="wikilink">熱傳導</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥内佐格数.md" title="wikilink">奥内佐格数</a></p></td>
<td><p><em>Oh</em></p></td>
<td></td>
<td><p>液體霧化，<a href="../Page/马兰戈尼流.md" title="wikilink">马兰戈尼流</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佩克莱特数.md" title="wikilink">佩克莱特数</a></p></td>
<td><p><em>Pe</em></p></td>
<td><p><span class="math inline">$Pe = \frac{du\rho c_p}{k} = (Re)(Pr)$</span></p></td>
<td><p><a href="../Page/平流.md" title="wikilink">平流</a>-<a href="../Page/擴散.md" title="wikilink">擴散問題</a>，總動量傳遞和分子熱傳遞之間的關係</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/剥离数.md" title="wikilink">剥离数</a></p></td>
<td></td>
<td></td>
<td><p>微觀結構與<a href="../Page/底物.md" title="wikilink">底物的黏附作用</a>[18]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/導流係數.md" title="wikilink">導流係數</a></p></td>
<td><p><em>K</em></p></td>
<td></td>
<td><p>在帶電離子束中空間電荷的強度</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/圓周率.md" title="wikilink">圓周率</a></p></td>
<td><p><span class="math inline"><em>π</em></span></p></td>
<td></td>
<td><p><a href="../Page/數學.md" title="wikilink">數學</a>（圓周長與直徑之比）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泊松比.md" title="wikilink">泊松比</a></p></td>
<td><p><span class="math inline"><em>ν</em></span></p></td>
<td></td>
<td><p><a href="../Page/彈性_(物理學).md" title="wikilink">彈性</a>（橫向與縱向負荷）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多孔性.md" title="wikilink">多孔性</a></p></td>
<td><p><span class="math inline"><em>ϕ</em></span></p></td>
<td></td>
<td><p><a href="../Page/地質學.md" title="wikilink">地質學</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/功率因數.md" title="wikilink">功率因數</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/電子學.md" title="wikilink">電子學</a>（有功功率与视在功率之比）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/功率數.md" title="wikilink">功率數</a></p></td>
<td><p><span class="math inline"><em>N</em><sub><em>p</em></sub></span></p></td>
<td></td>
<td><p>攪拌器的功率消耗</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/普兰特数.md" title="wikilink">普兰特数</a></p></td>
<td><p>Pr</p></td>
<td><p><span class="math inline">$Pr = \frac{\nu}{\alpha} = \frac{c_p \mu}{k}$</span></p></td>
<td><p>黏性擴散率與熱擴散率之比</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/壓力係數.md" title="wikilink">壓力係數</a></p></td>
<td><p><span class="math inline"><em>C</em><sub><em>P</em></sub></span></p></td>
<td></td>
<td><p>翼型上某個點的壓力</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/品質因子.md" title="wikilink">品質因子</a></p></td>
<td><p><span class="math inline"><em>Q</em></span></p></td>
<td></td>
<td><p>描述<a href="../Page/振子.md" title="wikilink">振子的</a><a href="../Page/阻尼.md" title="wikilink">阻尼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弧度.md" title="wikilink">弧度</a></p></td>
<td><p><span class="math inline"><em>θ</em><sub><em>r</em><em>a</em><em>d</em></sub></span></p></td>
<td><p><span class="math inline">$\theta_{rad} =\frac{s}{r}$</span></p></td>
<td><p>量度平面角，<span class="math inline">$1 \text{ rad} = \frac {180^\circ} {\pi}$</span></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瑞利数.md" title="wikilink">瑞利数</a></p></td>
<td><p><em>Ra</em></p></td>
<td></td>
<td><p>自由對流中的浮力和黏滯力</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/折射率.md" title="wikilink">折射率</a></p></td>
<td><p><em>n</em></p></td>
<td></td>
<td><p>電磁學、光學</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷诺数.md" title="wikilink">雷诺数</a></p></td>
<td><p><em>Re</em></p></td>
<td><p><span class="math inline">$Re = \frac{vL\rho}{\mu}$</span></p></td>
<td><p>流體的慣性力與黏滯力之比[19]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/比重.md" title="wikilink">比重</a></p></td>
<td><p><em>RD</em></p></td>
<td></td>
<td><p><a href="../Page/比重計.md" title="wikilink">比重計</a>，物質間的比較</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/理查逊数.md" title="wikilink">理查逊数</a></p></td>
<td><p><em>Ri</em></p></td>
<td></td>
<td><p>浮力對流動穩定性的影響[20]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛氏硬度.md" title="wikilink">洛氏硬度</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/硬度.md" title="wikilink">硬度</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/滚动阻力系数.md" title="wikilink">滚动阻力系数</a></p></td>
<td><p><em>C<sub>rr</sub></em></p></td>
<td><p><span class="math inline">$C_{rr} = \frac{N_f}{F}$</span></p></td>
<td><p><a href="../Page/車輛動力學.md" title="wikilink">車輛動力學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/罗斯贝数.md" title="wikilink">罗斯贝数</a></p></td>
<td><p><span class="math inline"><em>R</em><sub><em>o</em></sub></span></p></td>
<td></td>
<td><p><a href="../Page/地球物理學.md" title="wikilink">地球物理學中的慣性力</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劳斯数.md" title="wikilink">劳斯数</a></p></td>
<td><p><em>Z</em>或<em>P</em></p></td>
<td></td>
<td><p><a href="../Page/沈積物流移.md" title="wikilink">沈積物流移</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/施密特数.md" title="wikilink">施密特数</a></p></td>
<td><p><em>Sc</em></p></td>
<td></td>
<td><p>流體動力學（質量轉移與<a href="../Page/擴散.md" title="wikilink">擴散</a>）[21]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/形狀因數.md" title="wikilink">形狀因數</a></p></td>
<td><p><em>H</em></p></td>
<td></td>
<td><p><a href="../Page/边界层流動.md" title="wikilink">边界层流動中排移厚度與動量厚度之比</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舍伍德数.md" title="wikilink">舍伍德数</a></p></td>
<td><p><em>Sh</em></p></td>
<td></td>
<td><p>強制對流中的質量轉移</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/希尔兹參數.md" title="wikilink">希尔兹參數</a></p></td>
<td><p><em>τ</em><sub>∗</sub>或<em>θ</em></p></td>
<td></td>
<td><p>流體運動造成的沈積物流移的臨界</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/索默菲德数.md" title="wikilink">索默菲德数</a></p></td>
<td></td>
<td></td>
<td><p>邊層<a href="../Page/潤滑.md" title="wikilink">潤滑</a>[22]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯坦顿数.md" title="wikilink">斯坦顿数</a></p></td>
<td><p><em>St</em></p></td>
<td></td>
<td><p>強制<a href="../Page/對流.md" title="wikilink">對流中的熱傳遞</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯蒂芬数.md" title="wikilink">斯蒂芬数</a></p></td>
<td><p><em>Ste</em></p></td>
<td><p><span class="math inline">$Ste = \frac{C_p\Delta T}{L}$</span></p></td>
<td><p>相變時的熱傳遞</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯托克斯数.md" title="wikilink">斯托克斯数</a></p></td>
<td><p><em>Stk</em>或<em><span class="math inline"><em>S</em><sub><em>k</em></sub></span></em></p></td>
<td><p><span class="math inline">$Stk = \frac{\tau\,U_o}{d_c}$</span></p></td>
<td><p>流體流中的粒子動力學</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/应变_(物理学).md" title="wikilink">應變</a></p></td>
<td><p><span class="math inline"><em>ϵ</em></span></p></td>
<td><p><span class="math inline">$\epsilon = \cfrac{\partial{F}}{\partial{X}} - 1$</span></p></td>
<td><p><a href="../Page/材料科学.md" title="wikilink">材料科学</a>、<a href="../Page/彈性_(物理學).md" title="wikilink">彈性</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯特劳哈尔数.md" title="wikilink">斯特劳哈尔数</a></p></td>
<td><p><em>St</em>或<em>Sr</em></p></td>
<td><p><span class="math inline">$St = {f L\over V}$</span></p></td>
<td><p>持續並脈動的流體流動[23]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/泰勒数.md" title="wikilink">泰勒数</a></p></td>
<td><p><em>Ta</em></p></td>
<td><p><span class="math inline">$Ta = \frac{4\Omega^2 R^4}{\nu^2}$</span></p></td>
<td><p>旋轉的流體流動</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ursell數.md" title="wikilink">Ursell數</a></p></td>
<td><p><em>U</em></p></td>
<td><p><span class="math inline">$U = \frac{H\, \lambda^2}{h^3}$</span></p></td>
<td><p>在淺流體層上<a href="../Page/海浪.md" title="wikilink">表面引力波的非線性度</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Vadasz數.md" title="wikilink">Vadasz數</a></p></td>
<td><p><em>Va</em></p></td>
<td><p><span class="math inline">$Va = \frac{\phi Pr}{Da}$</span></p></td>
<td><p>在多孔介質中流體流動時，該數影響多孔性<span class="math inline"><em>ϕ</em></span>、普兰特数以及達西阻力係數</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/范特霍夫因子.md" title="wikilink">范特霍夫因子</a></p></td>
<td><p><em>i</em></p></td>
<td><p><span class="math inline"><em>i</em> = 1 + <em>α</em>(<em>n</em> − 1)</span></p></td>
<td><p>化學<a href="../Page/定量分析.md" title="wikilink">定量分析</a>（<a href="../Page/凝固點降低.md" title="wikilink"><em>K</em><sub>f</sub>及</a><a href="../Page/沸點升高.md" title="wikilink"><em>K</em><sub>b</sub></a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Wallis參數.md" title="wikilink">Wallis參數</a></p></td>
<td><p><em>J</em><sup>*</sup></p></td>
<td><p><span class="math inline">$\alpha = R \left( \frac{\omega \rho}{\mu} \right)^\frac{1}{2}$</span></p></td>
<td><p>多相流體流動時的<a href="../Page/表現速.md" title="wikilink">表現速</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韦伯数.md" title="wikilink">韦伯数</a></p></td>
<td><p><em>We</em></p></td>
<td><p><span class="math inline">$We = \frac{\rho v^2 l}{\sigma}$</span></p></td>
<td><p>表面極為彎曲的多相流體流動</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魏森贝格数.md" title="wikilink">魏森贝格数</a></p></td>
<td><p><em>Wi</em></p></td>
<td><p><span class="math inline"><em>W</em><em>i</em> = <em>γ̇</em><em>λ</em></span></p></td>
<td><p><a href="../Page/粘彈性.md" title="wikilink">粘彈性流體流動</a>[24]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沃默斯利数.md" title="wikilink">沃默斯利数</a></p></td>
<td><p><span class="math inline"><em>α</em></span></p></td>
<td><p><span class="math inline">$\alpha = R \left( \frac{\omega \rho}{\mu} \right)^\frac{1}{2}$</span></p></td>
<td><p>持續並脈動的流體流動[25]</p></td>
</tr>
</tbody>
</table>

## 無量綱的物理常數

一些基本物理常數，如真空中的[光速](../Page/光速.md "wikilink")、[萬有引力常數](../Page/萬有引力常數.md "wikilink")、[普朗克常數和](../Page/普朗克常數.md "wikilink")[波兹曼常数等等](../Page/波兹曼常数.md "wikilink")，在適當挑選[時間](../Page/時間.md "wikilink")、[長度](../Page/長度.md "wikilink")、[質量](../Page/質量.md "wikilink")、[電荷及](../Page/電荷.md "wikilink")[溫度等單位後](../Page/溫度.md "wikilink")，可以歸一（數值為1）。這種[單位制被稱為](../Page/單位制.md "wikilink")[自然單位制](../Page/自然單位制.md "wikilink")。不過不可能在每一個單位制中都把所有的[物理常數歸一](../Page/物理常數.md "wikilink")，剩餘的量必須以實驗判定。這些剩餘的量包括：

  - a：[精細結構常數](../Page/精細結構常數.md "wikilink")，[電磁交互作用的](../Page/電磁交互作用.md "wikilink")[耦合常数](../Page/耦合常数.md "wikilink")，α
    ≈ 1/137；
  - μ或β：[質子與](../Page/質子.md "wikilink")[電子的](../Page/電子.md "wikilink")[不變質量之比](../Page/不變質量.md "wikilink")，可更廣義地指所有[基本粒子相對電子的不變質量之比](../Page/基本粒子.md "wikilink")，μ
    ≈ 1836；
  - α<sub>s</sub>：[強相互作用的耦合常數](../Page/強相互作用.md "wikilink")；
  - α<sub>G</sub>：[重力的耦合常數](../Page/重力.md "wikilink")，α<sub>G</sub> ≈
    1.75×10<sup>−45</sup>。

## 參見

  - [量綱分析](../Page/量綱分析.md "wikilink")
  - [標準化](../Page/標準化_\(統計學\).md "wikilink")
  - [白金漢π定理](../Page/白金漢π定理.md "wikilink")

## 参考文献

## 外部連結

  - [John Baez](../Page/John_Baez.md "wikilink"), "[How Many Fundamental
    Constants Are There?](http://math.ucr.edu/home/baez/constants.html)"
  - Huba, J. D., 2007, *[NRL Plasma Formulary: Dimensionless Numbers of
    Fluid Mechanics.](http://www.ipp.mpg.de/~dpc/nrl/)* [Naval Research
    Laboratory](../Page/United_States_Naval_Research_Laboratory.md "wikilink").
    p. [23](http://www.ipp.mpg.de/~dpc/nrl/23.html),
    [24](http://www.ipp.mpg.de/~dpc/nrl/24.html),
    [25](http://www.ipp.mpg.de/~dpc/nrl/25.html)
  - Sheppard, Mike, 2007, "[Systematic Search for Expressions of
    Dimensionless Constants using the NIST database of Physical
    Constants.](https://web.archive.org/web/20120928062000/http://www.mit.edu/~mi22295/constants/constants.html)"

[Category:物理常數](../Category/物理常數.md "wikilink")
[無量綱](../Category/無量綱.md "wikilink")

1.

2.
3.  [Bagnold
    number](http://www2.umt.edu/Geology/faculty/hendrix/g432/g432_L6.htm)


4.

5.

6.
7.  [Bond
    number](http://ising.phys.cwru.edu/plt/PapersInPdf/181BridgeCollapse.pdf)


8.  [Courant–Friedrich–Levy
    number](http://www.cnrm.meteo.fr/aladin/newsletters/news22/J_Vivoda/Texte.html)


9.

10. [Fanning friction
    factor](http://www.engineering.uiowa.edu/~cee081/Exams/Final/Final.htm)

11. [Feigenbaum
    constants](http://www.drchaos.net/drchaos/Book/node44.html)

12. [Fresnel
    number](http://www.ilt.fraunhofer.de/default.php?web=1&id=100050&lan=eng&dat=2)


13. [Gain Ratio - Sheldon Brown](http://sheldonbrown.com/gain.html)

14.

15. [Lockhart–Martinelli
    parameter](http://www.flowprogramme.co.uk/publications/guidancenotes/GN40.pdf)


16.

17.  [1](http://wuphys.wustl.edu/~katz/mpemba.pdf) Mpemba number

18. [Peel number](http://web.imech.ac.cn/efile/2000.htm)

19.

20. [Richardson
    number](http://apollo.lsc.vsc.edu/classes/met455/notes/section4/2.html)


21. [Schmidt number](http://www.ent.ohiou.edu/~hbwang/fluidynamics.htm)

22. [Sommerfeld number](http://epubl.luth.se/avslutade/0348-8373/41/)

23. [Strouhal
    number](http://www.seas.upenn.edu/courses/belab/LabProjects/2001/be310s01m2.doc)


24. [Weissenberg number](http://physics.ucsd.edu/~des/Shear1999.pdf)

25. [Womersley
    number](http://www.seas.upenn.edu/courses/belab/LabProjects/2001/be310s01m2.doc)