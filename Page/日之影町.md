**日之影町**（）是位于[日本](../Page/日本.md "wikilink")[宮崎縣北部的一個](../Page/宮崎縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[西臼杵郡](../Page/西臼杵郡.md "wikilink")。轄區全域位於[九州山地](../Page/九州山地.md "wikilink")，包括有[傾山](../Page/傾山.md "wikilink")、[五葉山](../Page/五葉山.md "wikilink")、[釣鐘山](../Page/釣鐘山.md "wikilink")、[真弓岳等山峰](../Page/真弓岳.md "wikilink")。

## 歷史

在[平安時代至](../Page/平安時代.md "wikilink")[安土桃山時代屬於](../Page/安土桃山時代.md "wikilink")[三田井氏的領地](../Page/三田井氏.md "wikilink")，豐臣秀吉征服九州之後，屬於[延岡藩](../Page/延岡藩.md "wikilink")，在[廢藩置縣時](../Page/廢藩置縣.md "wikilink")，屬於[延岡縣](../Page/延岡縣.md "wikilink")，後又先後被改編於[美美津縣及宮崎縣](../Page/美美津縣.md "wikilink")。\[1\]

### 年表

  - 1879年：設置七折村、岩井川村、分城村各戶長役場。\[2\]
  - 1889年5月1日：實施[町村制](../Page/町村制.md "wikilink")，設置七折村戶長役場改設七折村役場，岩井川村和分城村合併為岩井川村。
  - 1951年1月1日：七折村和岩井川村[合併為](../Page/市町村合併.md "wikilink")**日之影町**（日文原名：）。
  - 1956年9月30日：岩戶村的部分地區被併入，同時日文名稱改為（發音及意義仍然相同）。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年5月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>上野村</p></td>
<td><p>1969年4月1日<br />
併入高千穗町</p></td>
<td><p>高千穗町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>高千穗村</p></td>
<td><p>1920年4月1日<br />
改制為高千穗町</p></td>
<td><p>1956年9月30日<br />
合併為高千穗町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>田原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>岩戶村</p></td>
<td><p>1956年9月30日<br />
併入高千穗町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1956年9月30日<br />
併入日之影町</p></td>
<td><p>日之影町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>七折村</p></td>
<td><p>1951年1月1日<br />
合併為日の影町<br />
（）</p></td>
<td><p>1956年9月30日<br />
改名為日之影町<br />
（）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>岩井川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [高千穗鐵道](../Page/高千穗鐵道.md "wikilink")（因颱風造成鐵路損毀並無法修復，已於2008年12月28日停止營運。）
      - [高千穗線](../Page/高千穗線.md "wikilink")：[槇峰車站](../Page/槇峰車站.md "wikilink")
        - [日向八戶車站](../Page/日向八戶車站.md "wikilink") -
        [吾味車站](../Page/吾味車站.md "wikilink") -
        [日之影溫泉車站](../Page/日之影溫泉車站.md "wikilink") -
        [影待車站](../Page/影待車站.md "wikilink") -
        [深角車站](../Page/深角車站.md "wikilink")

|                                                                                                                                                                      |                                                                                                                                                          |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Makimine_Station_in_Takachiho_Line.jpg](https://zh.wikipedia.org/wiki/File:Makimine_Station_in_Takachiho_Line.jpg "fig:Makimine_Station_in_Takachiho_Line.jpg") | [Gomi_Station_in_Takachiho_Line.jpg](https://zh.wikipedia.org/wiki/File:Gomi_Station_in_Takachiho_Line.jpg "fig:Gomi_Station_in_Takachiho_Line.jpg") |

### 道路

  - 高速道路

<!-- end list -->

  - [九州中央自動車道](../Page/九州中央自動車道.md "wikilink")：[日之影交流道](../Page/日之影交流道.md "wikilink")

## 觀光資源

  - [八戶觀音瀑布](../Page/八戶觀音瀑布.md "wikilink")
  - 石垣之村：日本梯田百選之一
  - 英國館：國登錄有形文化財

## 本地出身之名人

  - [赤星多美子](../Page/赤星多美子.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")

## 參考資料

## 外部連結

  - [日之影町商工會](http://www.miya-shoko.or.jp/hinokage/)

  - [日之影町觀光協會](http://www.hinokage-kanko.jp/)

<!-- end list -->

1.
2.