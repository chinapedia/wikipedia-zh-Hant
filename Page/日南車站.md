**日南車站**位於[台灣](../Page/台灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[大甲區](../Page/大甲區.md "wikilink")，[大安溪以北](../Page/大安溪.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[海岸線的](../Page/海岸線_\(臺鐵\).md "wikilink")[鐵路車站是](../Page/鐵路車站.md "wikilink")[火車進入](../Page/火車.md "wikilink")[台中市的第一個](../Page/台中市.md "wikilink")[車站](../Page/車站.md "wikilink")。

## 簡介

日南車站的命名源自於以往此地[平埔族社名的漢譯](../Page/平埔族.md "wikilink")。1922年10月30日落成啟用\[1\]，日南車站是海線沿線尚存的五座木造車站之一，其與[談文](../Page/談文車站.md "wikilink")、[大山](../Page/大山車站.md "wikilink")、[新埔等站採用的是幾乎一樣的建築式樣](../Page/新埔車站_\(苗栗縣\).md "wikilink")（洋小屋平家切妻造的[懸山頂建築](../Page/懸山頂.md "wikilink")），這些車站的共同特色包括位於側面的牛眼窗、破風造主結構，西式木架屋簷，以及雨淋板外牆等等。與大部分台灣小型車站常用之跨線天橋方式不同，日南車站的站房與[月台之間是以](../Page/月台.md "wikilink")[人行地下道相連](../Page/人行地下道.md "wikilink")。

雖然實際利用日南車站搭車的旅客數量稀少，但由於已被列為台中市市定古蹟車站建築保存狀態非常良好，因此日南車站反而成為遊客經常走訪的熱門地點之一，或甚至成為[婚紗照業者取景的素材](../Page/婚紗照.md "wikilink")。另日南車站仍有發售多站已停售的[名片式車票](../Page/埃德蒙森式鐵路車票.md "wikilink")，吸引許多車票收集者到訪。

## 車站構造

  - [島式月台兩座](../Page/島式月台.md "wikilink")，與站房之間是以人行地下道相連。
  - 木造站房，為臺中市市定古蹟。

<File:TRA> Rihnan Station 20070714.jpg|日南車站周邊景觀整理前的樣貌
[File:縱貫鐵路（海線）日南車站.JPG|日南車站站體後方](File:縱貫鐵路（海線）日南車站.JPG%7C日南車站站體後方)
[File:LA09602001006縱貫鐵路（海線）日南車站01.JPG|日南車站站體後方](File:LA09602001006縱貫鐵路（海線）日南車站01.JPG%7C日南車站站體後方)
<File:TRA> Rihnan Station ViewFromPlatform.jpg|自第一月台上瞭望日南車站站體後方
<File:LA09602001006縱貫鐵路（海線）日南車站02.JPG> <File:Tickets> from
Rinan.JPG|日南車站出售的名片式車票 <File:TRA> Rihnan Station
Interior.jpg|日南車站的站屋內部

## 利用狀況

  - 目前為乙種[簡易站](../Page/臺灣鐵路管理局車站等級#簡易站.md "wikilink")，由[大甲站管理](../Page/大甲車站.md "wikilink")，設有售票窗口並有站員一人派駐。
  - 僅有[區間車停靠](../Page/臺鐵區間車.md "wikilink")。
  - 是台鐵少數仍有販售名片式車票的車站。
  - 車站內設有「鐵路之旅 - 小站巡禮紀念章」之戳章，可供旅客蓋戳留念。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通](../Page/一卡通_\(台灣\).md "wikilink")、[icash
    2.0及](../Page/icash.md "wikilink")[HappyCash付費](../Page/HappyCash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 137       |
| 1999 | 157       |
| 2000 | 145       |
| 2001 | 184       |
| 2002 | 173       |
| 2003 | 175       |
| 2004 | 163       |
| 2005 | 163       |
| 2006 | 150       |
| 2007 | 158       |
| 2008 | 181       |
| 2009 | 200       |
| 2010 | 240       |
| 2011 | 295       |
| 2012 | 313       |
| 2013 | 333       |
| 2014 | 308       |
| 2015 | 297       |
| 2016 | 290       |
| 2017 | 274       |

歷年旅客人次紀錄

## 名片式車票

  - 日南車站於2012年仍然常態性販售販售名片式車票，購買後可持名片式車票直接或加價搭乘列車。
  - 日南車站於2012年時仍保有數量不少的老車票，包含1970年代的普通列車車票，1980年代普通快車費率合併初期、仍保有剪斷線設計等的舊式車票都還未售罄。*——其中至基隆的普通列車車票背面，還可見到曾在1960年代廣泛使用在台灣各類印刷物品的反共標語「保密防諜，人人*有*責」，是在台灣非常少見(已完售)、*還保有眾多舊式車票的車站。

## 車站週邊

  - [大安溪](../Page/大安溪.md "wikilink")
  - 日南公園
  - 北堤東路
  - 通天路
  - [黎明路](../Page/黎明路_\(台中市\).md "wikilink")
  - [臺中市立日南國民中學](../Page/臺中市立日南國民中學.md "wikilink")
  - [臨江路](../Page/臨江路_\(大甲區\).md "wikilink")
  - 中山路二段
  - 北堤西路
  - 長安路
  - [經國路](../Page/經國路_\(大甲區\).md "wikilink")
  - 臺中市大甲區日南國民小學
  - 臺中市政府警察局大甲分局日南派出所

## 歷史

  - 1922年10月30日：「日南驛」開業。\[2\]
  - 1994年3月15日：由[三等站降為](../Page/三等站.md "wikilink")[甲種簡易站](../Page/甲種簡易站.md "wikilink")，並指定由[大甲站管理](../Page/大甲車站.md "wikilink")。\[3\]
  - 2015年6月30日：啟用多卡通刷卡機。
  - 2017年9月16日：由[甲種簡易站調整為乙種](../Page/甲種簡易站.md "wikilink")[簡易站](../Page/簡易站.md "wikilink")。

## 公車資訊

### [台中市市區公車](../Page/台中市市區公車.md "wikilink")

  - [台中客運](../Page/台中客運.md "wikilink")：
      - **[<font color="red">93</font>](../Page/台中市公車93路.md "wikilink")**
        大甲區銅安厝－高鐵臺中站
      - **[<font color="red">657</font>](../Page/台中市公車657路.md "wikilink")**
        大甲高工－建興
      - **[<font color="red">659</font>](../Page/台中市公車659路.md "wikilink")**
        幼獅工業區－[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")－台中榮總

<!-- end list -->

  - [東南客運](../Page/東南客運.md "wikilink")：
      - **[<font color="red">97</font>](../Page/台中市公車97路.md "wikilink")**
        國立苑裡高中－大甲幼獅工業區－臺中航空站

<!-- end list -->

  - [建明客運](../Page/建明客運.md "wikilink")：
      - **[<font color="red">662</font>](../Page/台中市公車662路.md "wikilink")**
        大甲高中－大甲火車站－國立苑裡高中

<!-- end list -->

  - [苗栗客運](../Page/苗栗客運.md "wikilink")：
      - **[<font color="red">181</font>](../Page/台中市公車181路.md "wikilink")**
        大甲區公所－大甲火車站－苑裡

### 公路客運

  - [巨業客運](../Page/巨業客運.md "wikilink")：
      - **<font color="red">6354</font>** 大甲－通霄

<!-- end list -->

  - [苗栗客運](../Page/苗栗客運.md "wikilink")：
      - **<font color="red">5808</font>** 高鐵苗栗站－通霄－苑裡－大甲
      - **<font color="red">5814</font>** 苗栗－山腳－大甲

## 鄰近車站

  - **廢止營運模式**

<!-- end list -->

  - **現行營運模式**

## 參考資料

  - 《台灣鐵路車站圖誌》，[蘇昭旭](../Page/蘇昭旭.md "wikilink")，人人出版（2002年），ISBN
    978-986-7916-09-9

## 外部連結

  - [日南車站（臺灣鐵路管理局）](http://www.railway.gov.tw/Taichung-Transportation/cp.aspx?sn=11799)
  - [文化部文化資產局 -
    縱貫鐵路（海線）日南車站](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/20021126000002)

[Category:台中市鐵路車站](../Category/台中市鐵路車站.md "wikilink")
[中](../Category/台灣鐵道旅遊景點.md "wikilink")
[Category:台中市古蹟](../Category/台中市古蹟.md "wikilink")
[Category:大甲區](../Category/大甲區.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")
[Category:海岸線車站](../Category/海岸線車站.md "wikilink")
[Category:台灣日治時期交通建築](../Category/台灣日治時期交通建築.md "wikilink")
[中](../Category/臺灣鐵路文化資產.md "wikilink")

1.

2.
3.