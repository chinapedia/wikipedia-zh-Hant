[孔子](../Page/孔子.md "wikilink"){{.w}}[岳飛](../Page/岳飛.md "wikilink") |
type = [文武廟](../Page/文武廟.md "wikilink") | struct = 北朝式 | open_year =
1938年 | cere_date = 9月28日（[孔子誕辰日](../Page/孔子誕辰日.md "wikilink")） |
cultural_collection = |}}
**日月潭文武廟**，是位於[臺灣](../Page/臺灣.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[日月潭北邊山腰處的一座](../Page/日月潭.md "wikilink")[文武廟](../Page/文武廟.md "wikilink")。

1932年，[奧萬大引水由](../Page/奧萬大.md "wikilink")[日月潭建](../Page/日月潭.md "wikilink")[水壩儲水後](../Page/水壩.md "wikilink")，由附近的潭畔水社村水社巷的龍鳳宮與北吉巷的益化堂兩廟合併而成，於1934年選得現址，1938年完工。1969年，日月潭文武廟重建，建築設計師由澎湖出身的[謝自南擔綱](../Page/謝自南.md "wikilink")，得到[蔣介石等各級首長關心](../Page/蔣介石.md "wikilink")，由[臺灣省主席](../Page/臺灣省主席.md "wikilink")[黃-{杰}-核定以](../Page/黃杰.md "wikilink")「北朝式」風格建築，1975年完工。

日月潭文武廟主祀[關聖帝君](../Page/關聖帝君.md "wikilink")，加奉祀[孔子和](../Page/孔子.md "wikilink")[岳飛](../Page/岳飛.md "wikilink")，所有[神像皆用](../Page/神像.md "wikilink")[銅鑄塑像](../Page/銅.md "wikilink")。廟分三進，第一進是供奉孔子，第二進供奉武聖[關公及](../Page/關公.md "wikilink")[神農大帝](../Page/神農大帝.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、[三官大帝和](../Page/三官大帝.md "wikilink")[玉皇大帝](../Page/玉皇大帝.md "wikilink")；第三進則可以俯瞰日月潭全景。

## 照片集

<File:Wunwu> temple sun moon lake temple guard.jpg|文武廟石獅 <File:Wunwu>
temple sun moon lake front doors.jpg|文武廟廟門 <File:Wen> Wu Temple
04.jpg|文武廟石雕龍柱 <File:Wen> Wu Temple 06.jpg|文武廟天井 <File:Wen> Wu
Temple 07.jpg|文武廟關公及岳飛像 <File:Wen> Wu Temple 05.jpg|文武廟石雕 <File:Wen> Wu
Temple 09.jpg|文武廟石獅 <File:Wen> Wu Temple 02.jpg <File:Wen> Wu Temple
03.jpg|文武廟香爐 <File:Wen> Wu Temple 01.jpg|文武廟大牌樓 <File:Wen> Wu Temple
08.jpg <File:Wen> Wu Temple - Chinese lion.jpg|文武廟石獅 <File:Wen> Wu
Temple 12.jpg <File:Wen> Wu Temple 11.jpg <File:Wen> Wu Temple 10.jpg
<File:Wen> Wu Temple 13.jpg|文武廟龍雕

## 外部連結

  - [日月潭文武廟](http://www.wenwu.org.tw/)

[Y](../Category/南投縣廟宇.md "wikilink")
[Category:日月潭國家風景區](../Category/日月潭國家風景區.md "wikilink")
[Category:台灣關帝廟](../Category/台灣關帝廟.md "wikilink") [Category:魚池鄉
(臺灣)](../Category/魚池鄉_\(臺灣\).md "wikilink")