**日本100名城**是[財團法人](../Page/財團法人.md "wikilink")[日本城郭協会於](../Page/日本城郭協会.md "wikilink")2007年為紀念成立40周年的一項工作。2005年（平成17年）展開日本国内[名城的選拔](../Page/名城.md "wikilink")，並交由[歷史學家與](../Page/歷史學.md "wikilink")[建築學家於](../Page/建築學.md "wikilink")2006年2月13日發表（並明定4月6日為「城之日」）。

以下列出選定的百大名城。

## 北海道（1號～3號）

| No | 城名                                       | 所在地                                      | 印章位置                             | 備註                          |
| -- | ---------------------------------------- | ---------------------------------------- | -------------------------------- | --------------------------- |
| 1  | [根室半島砦跡群](../Page/根室半島砦跡群.md "wikilink") | [北海道](../Page/北海道.md "wikilink")         | [根室市](../Page/根室市.md "wikilink") | 根室市歷史與自然資料館、根室市觀光諮詢中心（根室站前） |
| 2  | [五稜郭](../Page/五稜郭.md "wikilink")         | [函館市](../Page/函館市.md "wikilink")         | 箱館奉行所附屬建物「板庫（休憩所）」、五稜郭塔售票櫃台      | 國之特別史跡                      |
| 3  | [松前城](../Page/松前城.md "wikilink")         | [松前町](../Page/松前町_\(北海道\).md "wikilink") | 松前城資料館施設内                        | 國之史跡                        |

## 東北（4號～13號）

| No | 城名                                   | 所在地                                  | 印章位置                                            | 備註                                                             |
| -- | ------------------------------------ | ------------------------------------ | ----------------------------------------------- | -------------------------------------------------------------- |
| 4  | [弘前城](../Page/弘前城.md "wikilink")     | [青森縣](../Page/青森縣.md "wikilink")     | [弘前市](../Page/弘前市.md "wikilink")                | 天守（弘前城史料館）内（11月24日～3月31日弘前公園綠的相談所内）                            |
| 5  | [根城](../Page/根城.md "wikilink")       | [八戶市](../Page/八戶市.md "wikilink")     | 史跡根城廣場料金所、八戶市博物館受付、史跡根城志工解說處窗口                  | 國之史跡                                                           |
| 6  | [盛岡城](../Page/盛岡城.md "wikilink")     | [岩手縣](../Page/岩手縣.md "wikilink")     | [盛岡市](../Page/盛岡市.md "wikilink")                | Plaza Odette 2樓觀光文化情報廣場、盛岡歴史文化館                                |
| 7  | [多賀城](../Page/多賀城.md "wikilink")     | [宮城縣](../Page/宮城縣.md "wikilink")     | [多賀城市](../Page/多賀城市.md "wikilink")              | 多賀城市埋蔵文化財調査中心展示室                                               |
| 8  | [仙台城](../Page/仙台城.md "wikilink")     | [仙台市](../Page/仙台市.md "wikilink")     | 仙台城見聞館展示處                                       | 國之史跡                                                           |
| 9  | [久保田城](../Page/久保田城.md "wikilink")   | [秋田縣](../Page/秋田縣.md "wikilink")     | [秋田市](../Page/秋田市.md "wikilink")                | [秋田市立佐竹史料館](../Page/秋田市立佐竹史料館.md "wikilink")、久保田城御隅櫓           |
| 10 | [山形城](../Page/山形城.md "wikilink")     | [山形縣](../Page/山形縣.md "wikilink")     | [山形市](../Page/山形市.md "wikilink")                | [最上義光歴史館](../Page/最上義光歴史館.md "wikilink")、山形市鄉土館受付窗口、二之丸東大手門櫓内部 |
| 11 | [二本松城](../Page/二本松城.md "wikilink")   | [福島縣](../Page/福島縣.md "wikilink")     | [二本松市](../Page/二本松市.md "wikilink")              | 二本松市歴史資料館受付窗口、JR[二本松站内観光案内所](../Page/二本松站.md "wikilink")       |
| 12 | [會津若松城](../Page/會津若松城.md "wikilink") | [會津若松市](../Page/會津若松市.md "wikilink") | 天守閣内賣店                                          | 國之史跡                                                           |
| 13 | [白河小峰城](../Page/白河小峰城.md "wikilink") | [白河市](../Page/白河市.md "wikilink")     | 白河集古苑、JR[白河站内觀光案内所](../Page/白河車站.md "wikilink") | 市指定史跡                                                          |

## 關東（14號～23號）

| No | 城名                                   | 所在地                                | 印章位置                               | 備註                                                                   |
| -- | ------------------------------------ | ---------------------------------- | ---------------------------------- | -------------------------------------------------------------------- |
| 14 | [水戶城](../Page/水戶城.md "wikilink")     | [茨城縣](../Page/茨城縣.md "wikilink")   | [水戶市](../Page/水戶市.md "wikilink")   | [弘道館料金所窗口](../Page/弘道館.md "wikilink")                                |
| 15 | [足利氏館](../Page/足利氏館.md "wikilink")   | [栃木縣](../Page/栃木縣.md "wikilink")   | [足利市](../Page/足利市.md "wikilink")   | [鑁阿寺本堂](../Page/鑁阿寺.md "wikilink")（[國寶](../Page/日本國寶.md "wikilink")） |
| 16 | [箕輪城](../Page/箕輪城.md "wikilink")     | [群馬縣](../Page/群馬縣.md "wikilink")   | [高崎市](../Page/高崎市.md "wikilink")   | [高崎市役所箕鄉支所受付窗口](../Page/高崎市役所.md "wikilink")（箕鄉公民館）                  |
| 17 | [新田金山城](../Page/新田金山城.md "wikilink") | [太田市](../Page/太田市.md "wikilink")   | 中島紀念公園（史跡金山城跡・南曲輪）休憩施設内            | 國之史跡                                                                 |
| 18 | [鉢形城](../Page/鉢形城.md "wikilink")     | [埼玉縣](../Page/埼玉縣.md "wikilink")   | [寄居町](../Page/寄居町.md "wikilink")   | 鉢形城歴史館受付                                                             |
| 19 | [川越城](../Page/川越城.md "wikilink")     | [川越市](../Page/川越市.md "wikilink")   | 川越城本丸御殿受付窗口                        | 縣指定史跡                                                                |
| 20 | [佐倉城](../Page/佐倉城.md "wikilink")     | [千葉縣](../Page/千葉縣.md "wikilink")   | [佐倉市](../Page/佐倉市.md "wikilink")   | 佐倉城址公園管理中心                                                           |
| 21 | [江戶城](../Page/江戶城.md "wikilink")     | [東京都](../Page/東京都.md "wikilink")   | [千代田區](../Page/千代田區.md "wikilink") | 楠公休憩場、和田倉休憩場、北之丸休憩場                                                  |
| 22 | [八王子城](../Page/八王子城.md "wikilink")   | [八王子市](../Page/八王子市.md "wikilink") | 八王子城跡管理棟前                          | 國之史跡                                                                 |
| 23 | [小田原城](../Page/小田原城.md "wikilink")   | [神奈川縣](../Page/神奈川縣.md "wikilink") | [小田原市](../Page/小田原市.md "wikilink") | 天守閣1樓                                                                |

## 甲信越（24號～32號）

<table>
<thead>
<tr class="header">
<th><p>No</p></th>
<th><p>城名</p></th>
<th><p>所在地</p></th>
<th><p>印章位置</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>24</p></td>
<td><p><a href="../Page/武田氏館.md" title="wikilink">武田氏館</a><br />
（<a href="../Page/武田神社.md" title="wikilink">武田神社</a>）</p></td>
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><a href="../Page/甲府市.md" title="wikilink">甲府市</a></p></td>
<td><p><a href="../Page/武田神社.md" title="wikilink">武田神社寶物殿</a></p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p><a href="../Page/甲府城.md" title="wikilink">甲府城</a></p></td>
<td><p>舞鶴城公園管理事務所</p></td>
<td><p>縣指定史跡</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p><a href="../Page/松代城.md" title="wikilink">松代城</a></p></td>
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><a href="../Page/長野市.md" title="wikilink">長野市</a></p></td>
<td><p>真田邸</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p><a href="../Page/上田城.md" title="wikilink">上田城</a></p></td>
<td><p><a href="../Page/上田市.md" title="wikilink">上田市</a></p></td>
<td><p><a href="../Page/上田市立博物館.md" title="wikilink">上田市立博物館</a>、上田市觀光會館</p></td>
<td><p>國之史跡</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p><a href="../Page/小諸城.md" title="wikilink">小諸城</a></p></td>
<td><p><a href="../Page/小諸市.md" title="wikilink">小諸市</a></p></td>
<td><p>懷古園事務所</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p><a href="../Page/松本城.md" title="wikilink">松本城</a></p></td>
<td><p><a href="../Page/松本市.md" title="wikilink">松本市</a></p></td>
<td><p>松本城管理事務所</p></td>
<td><p><a href="../Page/日本國寶.md" title="wikilink">國寶</a>・國之史跡・現存天守</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p><a href="../Page/高遠城.md" title="wikilink">高遠城</a></p></td>
<td><p><a href="../Page/伊那市.md" title="wikilink">伊那市</a></p></td>
<td><p>伊那市立高遠町歴史博物館</p></td>
<td><p>國之史跡</p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p><a href="../Page/新發田城.md" title="wikilink">新發田城</a></p></td>
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><a href="../Page/新發田市.md" title="wikilink">新發田市</a></p></td>
<td><p>新發田城辰巳櫓内（12月～3月在市立圖書館）</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p><a href="../Page/春日山城.md" title="wikilink">春日山城</a></p></td>
<td><p><a href="../Page/上越市.md" title="wikilink">上越市</a></p></td>
<td><p>春日山城跡故事館（12月～2月在市埋藏文化財中心）</p></td>
<td><p>國之史跡・日本五大山岳城</p></td>
</tr>
</tbody>
</table>

## 北陸（33號～37號）

| No | 城名                                 | 所在地                              | 印章位置                             | 備註                                             |
| -- | ---------------------------------- | -------------------------------- | -------------------------------- | ---------------------------------------------- |
| 33 | [高岡城](../Page/高岡城.md "wikilink")   | [富山縣](../Page/富山縣.md "wikilink") | [高岡市](../Page/高岡市.md "wikilink") | [高岡市立博物館](../Page/高岡市立博物館.md "wikilink")（鍛冶丸跡） |
| 34 | [七尾城](../Page/七尾城.md "wikilink")   | [石川縣](../Page/石川縣.md "wikilink") | [七尾市](../Page/七尾市.md "wikilink") | 七尾城史資料館（12月11日～3月10日在七尾市教育委員會文化財課（七尾市役所内））     |
| 35 | [金澤城](../Page/金澤城.md "wikilink")   | [金澤市](../Page/金澤市.md "wikilink") | 二之丸案内所、石川門入口案内所                  | 國之史跡                                           |
| 36 | [丸岡城](../Page/丸岡城.md "wikilink")   | [福井縣](../Page/福井縣.md "wikilink") | [坂井市](../Page/坂井市.md "wikilink") | 霞城公園管理事務所                                      |
| 37 | [一乘谷城](../Page/一乘谷城.md "wikilink") | [福井市](../Page/福井市.md "wikilink") | 復原町並入口（南・北）、朝倉氏遺跡資料館             | 國之特別史跡及特別名勝                                    |

## 東海（38號～48號）

| No | 城名                                   | 所在地                                | 印章位置                                                                                                      | 備註                                      |
| -- | ------------------------------------ | ---------------------------------- | --------------------------------------------------------------------------------------------------------- | --------------------------------------- |
| 38 | [岩村城](../Page/岩村城.md "wikilink")     | [岐阜縣](../Page/岐阜縣.md "wikilink")   | [恵那市](../Page/恵那市.md "wikilink")                                                                          | 岩村歴史資料館受付窗口                             |
| 39 | [岐阜城](../Page/岐阜城.md "wikilink")     | [岐阜市](../Page/岐阜市.md "wikilink")   | 岐阜城資料館                                                                                                    | 市指定史跡                                   |
| 40 | [山中城](../Page/山中城.md "wikilink")     | [靜岡縣](../Page/靜岡縣.md "wikilink")   | [三島市](../Page/三島市.md "wikilink")                                                                          | 山中城跡賣店内                                 |
| 41 | [駿府城](../Page/駿府城.md "wikilink")     | [靜岡市](../Page/靜岡市.md "wikilink")   | 東御門券賣所                                                                                                    |                                         |
| 42 | [掛川城](../Page/掛川城.md "wikilink")     | [掛川市](../Page/掛川市.md "wikilink")   | 掛川城御殿                                                                                                     |                                         |
| 43 | [犬山城](../Page/犬山城.md "wikilink")     | [愛知縣](../Page/愛知縣.md "wikilink")   | [犬山市](../Page/犬山市.md "wikilink")                                                                          | 城郭內                                     |
| 44 | [名古屋城](../Page/名古屋城.md "wikilink")   | [名古屋市](../Page/名古屋市.md "wikilink") | 正門改札所、東門改札所                                                                                               | 國之特別史跡・[三名城](../Page/三名城.md "wikilink") |
| 45 | [岡崎城](../Page/岡崎城.md "wikilink")     | [岡崎市](../Page/岡崎市.md "wikilink")   | 天守1樓                                                                                                      | 市指定史跡                                   |
| 46 | [長篠城](../Page/長篠城.md "wikilink")     | [新城市](../Page/新城市.md "wikilink")   | 新城市長篠城址史跡保存館窗口                                                                                            | 國之史跡                                    |
| 47 | [伊賀上野城](../Page/伊賀上野城.md "wikilink") | [三重縣](../Page/三重縣.md "wikilink")   | [伊賀市](../Page/伊賀市.md "wikilink")                                                                          | 大天守閣1樓                                  |
| 48 | [松坂城](../Page/松坂城.md "wikilink")     | [松阪市](../Page/松阪市.md "wikilink")   | [松阪市立歴史民俗資料館](../Page/松阪市立歴史民俗資料館.md "wikilink")、[本居宣長紀念館](../Page/本居宣長紀念館.md "wikilink")（兩館休館日時在松阪市觀光中心） | 國之史跡                                    |

## 近畿（49號～62號）

| No | 城名                                 | 所在地                                  | 印章位置                               | 備註                                                                                                                       |
| -- | ---------------------------------- | ------------------------------------ | ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| 49 | [小谷城](../Page/小谷城.md "wikilink")   | [滋賀縣](../Page/滋賀縣.md "wikilink")     | [長濱市](../Page/長濱市.md "wikilink")   | 小谷城戰國歴史資料館　周二休館日在長濱市役所湖北支所                                                                                               |
| 50 | [彥根城](../Page/彥根城.md "wikilink")   | [彥根市](../Page/彥根市.md "wikilink")     | 彥根城表門券賣所前                          | [國寶](../Page/日本國寶.md "wikilink")・現存天守・國之特別史跡及名勝                                                                          |
| 51 | [安土城](../Page/安土城.md "wikilink")   | [近江八幡市](../Page/近江八幡市.md "wikilink") | 安土城天主信長之館、安土城郭資料館、安土城跡受付           | 國之特別史跡                                                                                                                   |
| 52 | [觀音寺城](../Page/觀音寺城.md "wikilink") | 石寺樂市會館（12月中旬～3月上旬中止）、觀音正寺、桑實寺、摠見寺    | 國之史跡・日本五大山城                        |                                                                                                                          |
| 53 | [二条城](../Page/二条城.md "wikilink")   | [京都府](../Page/京都府.md "wikilink")     | [京都市](../Page/京都市.md "wikilink")   | 大休憩所                                                                                                                     |
| 54 | [大阪城](../Page/大阪城.md "wikilink")   | [大阪府](../Page/大阪府.md "wikilink")     | [大阪市](../Page/大阪市.md "wikilink")   | 天守閣1樓資訊處                                                                                                                 |
| 55 | [千早城](../Page/千早城.md "wikilink")   | [千早赤阪村](../Page/千早赤阪村.md "wikilink") | 金剛山麓「まつまさ」、千早城跡茶屋（不定期）             | 國之史跡                                                                                                                     |
| 56 | [竹田城](../Page/竹田城.md "wikilink")   | [兵庫縣](../Page/兵庫縣.md "wikilink")     | [朝來市](../Page/朝來市.md "wikilink")   | JR[竹田站](../Page/竹田車站_\(兵庫縣\).md "wikilink")、和田山觀光案内所「山城之鄉」                                                               |
| 57 | [篠山城](../Page/篠山城.md "wikilink")   | [篠山市](../Page/篠山市.md "wikilink")     | 大書院館内                              | 國之史跡                                                                                                                     |
| 58 | [明石城](../Page/明石城.md "wikilink")   | [明石市](../Page/明石市.md "wikilink")     | 明石公園服務中心受付窗口                       | 國之史跡                                                                                                                     |
| 59 | [姬路城](../Page/姬路城.md "wikilink")   | [姬路市](../Page/姬路市.md "wikilink")     | 姬路城管理處                             | 世界遺産・国宝8棟・国の特別史跡 現存天守・[三大平山城](../Page/日本三大列表#.E5.9F.8E.md "wikilink")・[三大連立式平山城](../Page/日本三大列表#.E5.9F.8E.md "wikilink") |
| 60 | [赤穗城](../Page/赤穗城.md "wikilink")   | [赤穗市](../Page/赤穗市.md "wikilink")     | 本丸櫓門下                              | 國之史跡及名勝                                                                                                                  |
| 61 | [高取城](../Page/高取城.md "wikilink")   | [奈良縣](../Page/奈良縣.md "wikilink")     | [高取町](../Page/高取町.md "wikilink")   | 高取町觀光案内所「夢創館」                                                                                                            |
| 62 | [和歌山城](../Page/和歌山城.md "wikilink") | [和歌山縣](../Page/和歌山縣.md "wikilink")   | [和歌山市](../Page/和歌山市.md "wikilink") | 和歌山城天守閣楠門受付                                                                                                              |

## 中國（63號～75號）

| No | 城名                                         | 所在地                                  | 印章位置                             | 備註                                                       |
| -- | ------------------------------------------ | ------------------------------------ | -------------------------------- | -------------------------------------------------------- |
| 63 | [鳥取城](../Page/鳥取城.md "wikilink")           | [鳥取縣](../Page/鳥取縣.md "wikilink")     | [鳥取市](../Page/鳥取市.md "wikilink") | 鳥取城跡内「[仁風閣](../Page/仁風閣.md "wikilink")」内                 |
| 64 | [松江城](../Page/松江城.md "wikilink")           | [島根縣](../Page/島根縣.md "wikilink")     | [松江市](../Page/松江市.md "wikilink") | 松江城天守内受付窗口                                               |
| 65 | [月山富田城](../Page/月山富田城.md "wikilink")       | [安來市](../Page/安來市.md "wikilink")     | 安來市立歴史資料館                        | 國之史跡・[日本五大山岳城](../Page/日本五大山岳城.md "wikilink")            |
| 66 | [津和野城](../Page/津和野城.md "wikilink")         | [津和野町](../Page/津和野町.md "wikilink")   | リフト茶屋                            | 國之史跡                                                     |
| 67 | [津山城](../Page/津山城.md "wikilink")           | [岡山縣](../Page/岡山縣.md "wikilink")     | [津山市](../Page/津山市.md "wikilink") | 津山城備中櫓受付                                                 |
| 68 | [備中松山城](../Page/松山城_\(備中國\).md "wikilink") | [高梁市](../Page/高梁市.md "wikilink")     | 備中松山城券売所                         | 國之史跡・現存天守・[三大山城](../Page/日本三大列表#.E5.9F.8E.md "wikilink") |
| 69 | [鬼之城](../Page/鬼之城.md "wikilink")           | [總社市](../Page/總社市.md "wikilink")     | 鬼城山遊客中心                          | 國之史跡                                                     |
| 70 | [岡山城](../Page/岡山城.md "wikilink")           | [岡山市](../Page/岡山市.md "wikilink")     | 岡山城天守閣入口                         | 國之史跡及特別名勝                                                |
| 71 | [福山城](../Page/福山城_\(備後國\).md "wikilink")   | [廣島縣](../Page/廣島縣.md "wikilink")     | [福山市](../Page/福山市.md "wikilink") | 福山城天守閣内(售票口)                                             |
| 72 | [郡山城](../Page/吉田郡山城.md "wikilink")         | [安藝高田市](../Page/安藝高田市.md "wikilink") | 安藝高田市吉田歴史民俗資料館                   | 國之史跡                                                     |
| 73 | [廣島城](../Page/廣島城.md "wikilink")           | [廣島市](../Page/廣島市.md "wikilink")     | 1樓博物館商店                          | 國之史跡                                                     |
| 74 | [岩國城](../Page/岩國城.md "wikilink")           | [山口縣](../Page/山口縣.md "wikilink")     | [岩國市](../Page/岩國市.md "wikilink") | 岩國城受付窗口(天守內)                                             |
| 75 | [萩城](../Page/萩城.md "wikilink")             | [萩市](../Page/萩市.md "wikilink")       | 本丸入口料金所                          | 國之史跡                                                     |

## 四國（76號～84號）

| No | 城名                                 | 所在地                                | 印章位置                             | 備註                                                 |
| -- | ---------------------------------- | ---------------------------------- | -------------------------------- | -------------------------------------------------- |
| 76 | [德島城](../Page/德島城.md "wikilink")   | [德島縣](../Page/德島縣.md "wikilink")   | [德島市](../Page/德島市.md "wikilink") | [徳島市立徳島城博物館受付窗口](../Page/徳島市立徳島城博物館.md "wikilink") |
| 77 | [高松城](../Page/高松城.md "wikilink")   | [香川縣](../Page/香川縣.md "wikilink")   | [高松市](../Page/高松市.md "wikilink") | 高松城東・西入口                                           |
| 78 | [丸龜城](../Page/丸龜城.md "wikilink")   | [丸龜市](../Page/丸龜市.md "wikilink")   | 丸龜城天守（天守閉館期間在丸龜市立資料館）            | 國之史跡・現存天守                                          |
| 79 | [今治城](../Page/今治城.md "wikilink")   | [愛媛縣](../Page/愛媛縣.md "wikilink")   | [今治市](../Page/今治市.md "wikilink") | 今治城管理事務所（天守閣内）                                     |
| 80 | [湯築城](../Page/湯築城.md "wikilink")   | [松山市](../Page/松山市.md "wikilink")   | 湯築城資料館                           | 國之史跡                                               |
| 81 | [松山城](../Page/松山城.md "wikilink")   | 松山城天守入口                            | 國之史跡・三大平山城・現存天守・三大連立式平山城         |                                                    |
| 82 | [大洲城](../Page/大洲城.md "wikilink")   | [大洲市](../Page/大洲市.md "wikilink")   | 台所櫓入口（大洲城内入口）                    | 縣指定史跡                                              |
| 83 | [宇和島城](../Page/宇和島城.md "wikilink") | [宇和島市](../Page/宇和島市.md "wikilink") | 宇和島城天守                           | 國之史跡・現存天守                                          |
| 84 | [高知城](../Page/高知城.md "wikilink")   | [高知縣](../Page/高知縣.md "wikilink")   | [高知市](../Page/高知市.md "wikilink") | 本丸御殿入口                                             |

## 九州（85號～97號）

| No | 城名                                       | 所在地                                | 印章位置                                          | 備註                                                               |
| -- | ---------------------------------------- | ---------------------------------- | --------------------------------------------- | ---------------------------------------------------------------- |
| 85 | [福岡城](../Page/福岡城.md "wikilink")         | [福岡縣](../Page/福岡縣.md "wikilink")   | [福岡市](../Page/福岡市.md "wikilink")              | 福岡城昔日探訪館                                                         |
| 86 | [大野城](../Page/大野城_\(筑前國\).md "wikilink") | [大野城市](../Page/大野城市.md "wikilink") | 福岡縣立四王寺縣民之森管理事務所、太宰府市文化ふれあい館、大宰府展示館、大野城市綜合体育館 | 國之特別史跡                                                           |
| 87 | [名護屋城](../Page/名護屋城.md "wikilink")       | [佐賀縣](../Page/佐賀縣.md "wikilink")   | [唐津市](../Page/唐津市_\(日本\).md "wikilink")       | [佐賀縣立名護屋城博物館](../Page/佐賀縣立名護屋城博物館.md "wikilink")                 |
| 88 | [吉野里遺址](../Page/吉野里遺址.md "wikilink")     | [吉野里町](../Page/吉野里町.md "wikilink") | 吉野里歴史公園東口                                     | 國之特別史跡                                                           |
| 89 | [佐賀城](../Page/佐賀城.md "wikilink")         | [佐賀市](../Page/佐賀市.md "wikilink")   | 佐賀城本丸歴史館受付                                    | 縣指定史跡                                                            |
| 90 | [平戶城](../Page/平戶城.md "wikilink")         | [長崎縣](../Page/長崎縣.md "wikilink")   | [平戶市](../Page/平戶市.md "wikilink")              | 平戶城天守閣                                                           |
| 91 | [島原城](../Page/島原城.md "wikilink")         | [島原市](../Page/島原市.md "wikilink")   | 島原城受付窗口                                       | 市指定史跡                                                            |
| 92 | [熊本城](../Page/熊本城.md "wikilink")         | [熊本縣](../Page/熊本縣.md "wikilink")   | [熊本市](../Page/熊本市.md "wikilink")              | 頰當御門、櫨方門、須戶口門、不開門（2016年熊本地震後，改在櫻之馬場遊客服務處，及熊本城二之丸停車場內之「熊本城二之丸憩所」） |
| 93 | [人吉城](../Page/人吉城.md "wikilink")         | [人吉市](../Page/人吉市.md "wikilink")   | 人吉城歷史館                                        | 國之史跡                                                             |
| 94 | [大分府内城](../Page/府內城.md "wikilink")       | [大分縣](../Page/大分縣.md "wikilink")   | [大分市](../Page/大分市.md "wikilink")              | 大分城址公園内、文化會館北西側事務所2樓出入口                                          |
| 95 | [岡城](../Page/岡城_\(豐後國\).md "wikilink")   | [竹田市](../Page/竹田市.md "wikilink")   | 觀覧料徴收所（総役所跡）                                  | 國之特別史跡                                                           |
| 96 | [飫肥城](../Page/飫肥城.md "wikilink")         | [宮崎縣](../Page/宮崎縣.md "wikilink")   | [日南市](../Page/日南市.md "wikilink")              | 飫肥城歴史資料館                                                         |
| 97 | [鹿兒島城](../Page/鹿兒島城.md "wikilink")       | [鹿兒島縣](../Page/鹿兒島縣.md "wikilink") | [鹿兒島市](../Page/鹿兒島市.md "wikilink")            | 鹿兒島縣歴史資料中心黎明館内綜合案内所                                              |

## 沖繩（98號～100號）

| No  | 城名                                 | 所在地                              | 印章位置                               | 備註         |
| --- | ---------------------------------- | -------------------------------- | ---------------------------------- | ---------- |
| 98  | [今歸仁城](../Page/今歸仁城.md "wikilink") | [沖繩縣](../Page/沖繩縣.md "wikilink") | [今歸仁村](../Page/今歸仁村.md "wikilink") | 今歸仁村御城交流中心 |
| 99  | [中城城](../Page/中城城.md "wikilink")   | [中城村](../Page/中城村.md "wikilink") | 管理事務所窗口                            | 世界遺產・國之史跡  |
| 100 | [首里城](../Page/首里城.md "wikilink")   | [那霸市](../Page/那霸市.md "wikilink") | 首里杜館、系圖座・用物座、北殿                    | 世界遺產・國之史跡  |

## 相關項目

  - [城 (日本)](../Page/城_\(日本\).md "wikilink")
  - [日本城堡列表](../Page/日本城堡列表.md "wikilink")
  - [續日本100名城](../Page/續日本100名城.md "wikilink")

[\*](../Category/日本100名城.md "wikilink")
[Category:2006年日本](../Category/2006年日本.md "wikilink")
[Category:日本名数100](../Category/日本名数100.md "wikilink")
[Category:百大](../Category/百大.md "wikilink")