**日本Falcom股份有限公司**（；）是主要開發及販售[電腦](../Page/個人電腦.md "wikilink")[遊戲軟體的日本公司](../Page/遊戲軟體.md "wikilink")，其代表作品有《[Dragon
Slayer系列](../Page/Dragon_Slayer系列.md "wikilink")》、《[伊蘇系列](../Page/伊蘇系列.md "wikilink")》和《[英雄傳說系列](../Page/英雄傳說系列.md "wikilink")》等。

## 概要

[Toshihiro_Kondo.jpg](https://zh.wikipedia.org/wiki/File:Toshihiro_Kondo.jpg "fig:Toshihiro_Kondo.jpg")

1981年3月由[加藤正幸創立](../Page/加藤正幸.md "wikilink")，最早為[美國](../Page/美國.md "wikilink")[蘋果電腦在日本的代理經銷商](../Page/蘋果電腦.md "wikilink")。Falcom的名稱來至於《[星際大戰](../Page/星際大戰.md "wikilink")》中的[千年隼號](../Page/千年隼號.md "wikilink")（Millennium
Falcon）與[電腦](../Page/電腦.md "wikilink")（Computer）的「COM」結合而成。

2001年遊戲部門從公司內單獨分割出來，成為了現在的「日本Falcom股份有限公司」。2003年12月2日在[東京證券交易所上市](../Page/東京證券交易所.md "wikilink")，而原公司則更名為「股份有限公司Falcom」，經營方向為[資訊科技和投資事業](../Page/資訊科技.md "wikilink")。現址位於[東京都](../Page/東京都.md "wikilink")[立川市](../Page/立川市.md "wikilink")[曙町](../Page/曙町.md "wikilink")，現任代表社長為[近藤季洋](../Page/近藤季洋.md "wikilink")。

專注於[電腦遊戲的研發](../Page/電腦遊戲.md "wikilink")，也經常把旗下開發之遊戲系列授權給其他公司，製作非[PC平台的遊戲](../Page/個人電腦.md "wikilink")。同時也以高水準的[遊戲配樂著稱](../Page/遊戲音樂.md "wikilink")，成立了公司專屬樂團《[Falcom
Sound Team
jdk](../Page/Falcom_Sound_Team_jdk.md "wikilink")》為旗下遊戲製作各種主題曲與配樂。

80年代是Falcom的精華時期，但90年代初期一度陷入沒有新作的窘境。度過了90年代中後半到2000年代初期的復刻遊戲期，目前正處於持續開發新作的復甦狀態，對外授權遊戲的數量也大幅增加。2002年開始其公司的遊戲內容已逐漸轉向3D化。2006年因全球[PC平台的銷售環境逐漸惡化](../Page/PC.md "wikilink")，將作品轉向[PSP平台](../Page/PSP.md "wikilink")。2011年開始開發[PSV平台的作品](../Page/PlayStation_Vita.md "wikilink")。2012年开始在[PS3平台上开发作品](../Page/PlayStation_3.md "wikilink")。2016年开始在[PS4平台上开发作品](../Page/PlayStation_4.md "wikilink")。

## 主要作品

  - [Dragon
    Slayer](../Page/Dragon_Slayer.md "wikilink")（屠龍劍）系列1984年－1995年（而後部分成独立系列）

      - [XANADU](../Page/XANADU.md "wikilink")（雷諾尼都紀事、迷城-{zh-cn:的国度;
        zh-tw:國度;}-或桃源鄉）系列 1985年－2015年
          - [Xanadu -Dragon Slayer
            II-](../Page/Xanadu_-Dragon_Slayer_II-.md "wikilink") 1985年
          - [Xanadu ScenarioII The Resurrection of
            Dragon](../Page/Xanadu_ScenarioII_The_Resurrection_of_Dragon.md "wikilink")
            1986年
          - [Faxanadu](../Page/Faxanadu.md "wikilink") 1987年
          - [Xanadu Next](../Page/Xanadu_Next.md "wikilink") 2005年
          - [東京迷城](../Page/东京Xanadu.md "wikilink") 2015年
      - Romancia 1986年
      - Dragon Slayer IV Drasle Family 1987年
      - [SORCERIAN](../Page/SORCERIAN.md "wikilink")（-{zh-hans:七星魔法使;
        zh-hant:魔域傳說;}-）系列 1987年－2005年
      - [LORD MONARCH系列](../Page/LORD_MONARCH.md "wikilink") 1991年－2004年
      - [風之傳說XANADU系列](../Page/風之傳說XANADU.md "wikilink") 1994年－1995年

  - [伊苏系列](../Page/伊苏系列.md "wikilink") 1987年－

  - [英雄傳說系列](../Page/英雄傳說系列.md "wikilink") 1989年－

      - [伊赛鲁哈撒篇](../Page/伊赛鲁哈撒篇.md "wikilink") 1989年－1992年（隸屬於Dragon
        Slayer系列，為Dragon Slayer VI）
          - [Dragon Slayer 英雄傳說](../Page/英雄傳說_\(遊戲\).md "wikilink")
            1989年
          - [Dragon Slayer 英雄傳說II](../Page/英雄傳說II.md "wikilink") 1992年
      - [卡卡布三部曲](../Page/卡卡布三部曲.md "wikilink") 1994年－1999年
          - [英雄传说III 白发魔女](../Page/英雄传说III_白发魔女.md "wikilink") 1994年
          - [英雄傳說IV -{zh-cn:朱红的泪;
            zh-tw:朱紅血;}-](../Page/英雄傳說IV_朱紅的淚.md "wikilink")
            1996年
          - [英雄传说V 海之槛歌](../Page/英雄传说V_海之槛歌.md "wikilink") 1999年
      - [軌跡系列](../Page/軌跡系列.md "wikilink") 2004年－
          - [英雄傳說VI 空之軌跡](../Page/英雄傳說VI_空之軌跡.md "wikilink") （含FC、SC、the
            3rd三部作品）2004年－2007年
          - [英雄傳說VII](../Page/英雄傳說VII.md "wikilink")\[1\]\[2\]
              - [英雄傳說 零之軌跡](../Page/英雄傳說_零之軌跡.md "wikilink") 2010年
              - [英雄傳說 碧之軌跡](../Page/英雄傳說_碧之軌跡.md "wikilink") 2011年
          - [英雄傳說 閃之軌跡](../Page/英雄傳說_閃之軌跡.md "wikilink") 2013年
          - [英雄傳說 閃之軌跡2](../Page/英雄傳說_閃之軌跡2.md "wikilink") 2014年
          - [英雄传说 晓之轨迹](../Page/英雄传说_晓之轨迹.md "wikilink") 2016年
          - [英雄傳說 閃之軌跡3](../Page/英雄傳說_閃之軌跡3.md "wikilink") 2017年
          - 英雄传说 闪之轨迹4 2018年秋

  - 1990年

      - DINOSAUR ～Resurrection～ 2002年

  - [Brandish](../Page/Brandish.md "wikilink")（撼天神塔系列）1991年－2009年

  - [Popful Mail](../Page/Popful_Mail.md "wikilink")（美少女劍士梅兒）1991年

  - [魔唤精灵系列](../Page/魔唤精灵.md "wikilink") 1997年－2008年

  - [双星物语系列](../Page/双星物语.md "wikilink") 2001年－2008年

      - \-{zh-hans:[双星物语](../Page/双星物语.md "wikilink");
        zh-hant:[奇幻仙境](../Page/双星物语.md "wikilink");}- 2001年
      - [双星物语2](../Page/双星物语2.md "wikilink") 2008年

  - \-{zh-hans:[咕噜小天使](../Page/咕噜小天使.md "wikilink");
    zh-hant:[可樂米物語](../Page/咕噜小天使.md "wikilink");
    zh-hk:[咕噜小天使](../Page/咕噜小天使.md "wikilink");}- 2004年

  - [RINNE](../Page/RINNE.md "wikilink") 2005年（由SOFTBANK BB贩售）

  - [Crossover作品](../Page/跨界作品.md "wikilink")

      - [伊苏vs空之轨迹 Alternative
        Saga](../Page/伊苏vs空之轨迹_Alternative_Saga.md "wikilink")
        2010年

  - [那由多的轨迹](../Page/那由多的轨迹.md "wikilink") 2012年

## 參考文獻

## 外部链接

  - [Falcom Homepage](http://www.falcom.co.jp/)

[Falcom](../Category/日本電子遊戲公司.md "wikilink")
[日本Falcom](../Category/日本Falcom.md "wikilink")
[Category:1981年開業電子遊戲公司](../Category/1981年開業電子遊戲公司.md "wikilink")
[Category:立川市](../Category/立川市.md "wikilink")

1.  若依過去作品慣例應為「英雄傳說VIII（第八代）」，但Falcom在《空之軌跡SC》之後即取消了作品標題中的代號，僅以回答玩家問題的方式回應《零之軌跡》與《碧之軌跡》為英雄傳說VII（第七代）。
2.