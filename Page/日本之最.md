**日本之最**是按类别分列的日本之最列表。

## 自然

### 地理

  - 日本最高的山
    [富士山](../Page/富士山.md "wikilink")（[静岡縣](../Page/静岡縣.md "wikilink")、[山梨縣](../Page/山梨縣.md "wikilink")）
    3,776m
  - 日本最低的山
    [天保山](../Page/天保山.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[港区](../Page/港区_\(大阪市\).md "wikilink")）
    4.53m（人工山）
      - 日本最低的天然山
        [辯天山](../Page/辯天山_\(德島市\).md "wikilink")（[德島縣](../Page/德島縣.md "wikilink")[德島市](../Page/德島市.md "wikilink")）
        6.1m
      - 日本最低的火山
        [笠山](../Page/笠山.md "wikilink")（[山口縣](../Page/山口縣.md "wikilink")[萩市](../Page/萩市.md "wikilink")）　112m（也是世界最低）
  - 日本最低的地方 [八戶礦山](../Page/八戶礦山.md "wikilink")（青森縣） -135m（人工）
  - 日本最大的湖 [琵琶湖](../Page/琵琶湖.md "wikilink")（滋賀縣） 670.33km²
  - 日本最深的湖 [田澤湖](../Page/田澤湖.md "wikilink")（秋田縣） 423.4m
  - 日本透明度最高的湖 [摩周湖](../Page/摩周湖.md "wikilink")（北海道）
    19.0m（2004年）（1931年時曾達41.6m，為當時的世界第一）
  - 日本最高的湖沼 [御嶽山二值池](../Page/御嶽山.md "wikilink")（岐阜縣・長野縣） 2,905m
  - 日本最大的島 [本州](../Page/本州.md "wikilink")227,945.15km²
      - 本土（北海道、本州、四国、九州四島）以外為[琉球本島](../Page/琉球本島.md "wikilink")
        1,206.49km²
      - 日本最大的離島（非縣廳所在地島嶼）
        [佐渡島](../Page/佐渡島.md "wikilink")854.88km²（也是[日本海中最大的島嶼](../Page/日本海.md "wikilink")）
  - 日本最小的島 [沖之鳥島](../Page/沖之鳥島.md "wikilink")（地理學上只算是一個島礁，沒有被聯合國承認是島嶼）
  - 日本最大的無人島 [渡島大島](../Page/渡島大島.md "wikilink")（北海道） 9.73km²
  - 日本最小的有人島
    [蕨小島](../Page/蕨小島.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")[五島市](../Page/五島市.md "wikilink")）
    0.03km²
  - 日本最大的有人離島
    [佐渡島](../Page/佐渡島.md "wikilink")（[新潟縣](../Page/新潟縣.md "wikilink")）854.88km²
  - 日本最長的河流 [信濃川](../Page/信濃川.md "wikilink")（新潟縣・群馬縣・長野縣） 367km
  - 日本最短的河流 [鹽川](../Page/鹽川.md "wikilink")（沖繩縣）300m
  - 日本[流域面積最大的河流](../Page/流域面積.md "wikilink")
    [利根川](../Page/利根川.md "wikilink")（群馬縣、長野縣、栃木縣、茨城縣、埼玉縣、千葉縣、東京都）
    約16,840km²
  - 日本最寬的河（両岸堤防間的距離）[荒川](../Page/荒川_\(關東\).md "wikilink")
    [埼玉縣御成橋附近](../Page/埼玉縣.md "wikilink")2.5km
  - 日本最大的平原 [關東平原](../Page/關東平原.md "wikilink")17,000平方公里
  - 日本最深的海灣 [駿河灣](../Page/駿河灣.md "wikilink") 2,500m
  - 日本最大的沙灘 [鳥取砂丘](../Page/鳥取砂丘.md "wikilink")（鳥取縣） 545ha
  - 日本（世界）最窄的海峡 [土淵海峡](../Page/土淵海峡.md "wikilink")（香川縣） 最窄處9.93米
  - 日本最細長的[半島](../Page/半島.md "wikilink")
    [佐田岬半島](../Page/佐田岬半島.md "wikilink")（[愛媛縣](../Page/愛媛縣.md "wikilink")[伊方町](../Page/伊方町.md "wikilink")）
  - 日本最長的[洞窟](../Page/洞窟.md "wikilink")
    [安家洞](../Page/安家洞.md "wikilink")（[岩手縣](../Page/岩手縣.md "wikilink")[岩泉町](../Page/岩泉町.md "wikilink")）
    12,736.2m
  - 日本最長的水中洞窟
    [稻積水中鍾乳洞](../Page/稻積水中鍾乳洞.md "wikilink")（[大分縣](../Page/大分縣.md "wikilink")[豐後大野市](../Page/豐後大野市.md "wikilink")）
    1,000m以上
  - 日本最長的[海底鍾乳洞](../Page/海底鍾乳洞.md "wikilink")（[沖繩縣](../Page/沖繩縣.md "wikilink")[恩納村](../Page/恩納村.md "wikilink")）
    640m
  - 日本最深的洞窟
    [白蓮洞](../Page/白蓮洞.md "wikilink")（[新潟縣](../Page/新潟縣.md "wikilink")[糸魚川市](../Page/糸魚川市.md "wikilink")）
    -450m
  - 日本最深的水中洞窟 [龍泉洞](../Page/龍泉洞.md "wikilink")（岩手縣） -120m以上
  - 日本最東端
    [南鳥島](../Page/南鳥島.md "wikilink")（東京都[小笠原村](../Page/小笠原村.md "wikilink")）（普通民眾可以進入的最東端是[納沙布岬](../Page/納沙布岬.md "wikilink")（[北海道](../Page/北海道.md "wikilink")））
  - 日本最西端
    [与那国島](../Page/与那国島.md "wikilink")（沖繩縣[与那国町](../Page/与那国町.md "wikilink")）
  - 日本最南端
    [沖之鳥島](../Page/沖之鳥島.md "wikilink")（東京都[小笠原村](../Page/小笠原村.md "wikilink")）（有人島為[波照間島](../Page/波照間島.md "wikilink")（沖縄縣[竹富町](../Page/竹富町.md "wikilink")））
  - 日本最北端
    [辯天島](../Page/辯天島_\(稚内市\).md "wikilink")（北海道[稚内市](../Page/稚内市.md "wikilink")）
  - 日本離海最遠的地点
    （[長野縣](../Page/長野縣.md "wikilink")[佐久市與](../Page/佐久市.md "wikilink")[群馬縣](../Page/群馬縣.md "wikilink")[甘樂郡](../Page/甘樂郡.md "wikilink")[南牧村交界處](../Page/南牧村_\(群馬縣\).md "wikilink")）

### 氣象

  - 日本國内的[最高温](../Page/最高温.md "wikilink")（[氣象官署海底鍾乳洞](../Page/氣象官署.md "wikilink")[地域氣象觀測系統](../Page/地域氣象觀測系統.md "wikilink")）
    [岐阜縣](../Page/岐阜縣.md "wikilink")[多治見市](../Page/多治見市.md "wikilink")、[埼玉縣](../Page/埼玉縣.md "wikilink")[熊谷市](../Page/熊谷市.md "wikilink")
    40.9℃（均於2007年8月16日）
      - 氣象廳委託觀測所得記録
        [德島縣](../Page/德島縣.md "wikilink")[鳴門市撫養](../Page/鳴門市.md "wikilink")
        42.5℃（1923年8月6日）
      - 参考記録
        [東京都](../Page/東京都.md "wikilink")[足立区江北](../Page/足立区.md "wikilink")
        42.7℃（2004年7月20日）\[東京都環境科学研究所による\]
  - 日本国内的[最低氣温](../Page/最低氣温.md "wikilink")（氣象官署地域氣象觀測系統）
    [北海道](../Page/北海道.md "wikilink")[旭川市](../Page/旭川市.md "wikilink")
    -41.0℃（1902年1月25日）
      - 氣象庁委託観測所的記録 北海道[美深町](../Page/美深町.md "wikilink")
        -41.5℃（1931年1月27日）
      - 参考記録 北海道[歌登町上幌別](../Page/歌登町.md "wikilink")
        -44.0℃（1931年1月27日）\[北海道森林気象観測所による\]
  - 日本最高的[降水量](../Page/降水量.md "wikilink")\[1\]
      - 10分鐘降水量
        [高知縣](../Page/高知縣.md "wikilink")[土佐清水市](../Page/土佐清水市.md "wikilink")
        49.0mm （1946年9月13日）
      - 1小時降水量 153mm
        （[千葉縣](../Page/千葉縣.md "wikilink")[香取市](../Page/香取市.md "wikilink")
        1999年10月27日、[長崎縣](../Page/長崎縣.md "wikilink")[長崎市長浦岳](../Page/長崎市.md "wikilink")
        1982年7月23日）
          - 1小時降水量（除地域氣象觀測系統外） 長崎縣[長与町](../Page/長与町.md "wikilink") 187mm
            （1982年7月23日、町役場観測）
      - 1日降水量
        [奈良縣](../Page/奈良縣.md "wikilink")[上北山村日出岳](../Page/上北山村.md "wikilink")
        844mm （1982年8月1日）
          - 1日降水量（除地域氣象觀測系統外）
            [德島縣](../Page/德島縣.md "wikilink")[那賀町海川](../Page/那賀町.md "wikilink")
            1,317mm （2004年8月1日、[四國電力觀測](../Page/四國電力.md "wikilink")）
          - 24小時降水量
            [高知縣](../Page/高知縣.md "wikilink")[香美市繁藤](../Page/香美市.md "wikilink")
            979mm （1998年9月25日）
  - 日本最深的積雪
      - 氣象觀測所得記録
        [富山縣大山町](../Page/富山縣.md "wikilink")（現[富山市](../Page/富山市.md "wikilink")）真川
        750cm（1945年2月26日）
      - [国鐵的記錄](../Page/日本國有鐵道.md "wikilink")
        [森宮野原站](../Page/森宮野原站.md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")[榮村](../Page/榮村.md "wikilink")）
        785cm（1945年2月14日）
      - 在山岳測量的記録 [伊吹山山頂](../Page/伊吹山.md "wikilink") 1,182cm（1927年2月14日）
      - 縣廳所在地的記録
        [福井縣](../Page/福井縣.md "wikilink")[福井市](../Page/福井市.md "wikilink")
        213cm(1963年1月31日)
  - 日本最大風速（平地） [室戶岬](../Page/室戶岬.md "wikilink") 69.8m/s （1965年9月10日）
      - 日本最大風速（山岳） [富士山山頂](../Page/富士山.md "wikilink") 72.5m/s
        （1942年4月5日）
  - 日本最大瞬間風速（平地） 宮古島 85.3m/s
    （1966年9月5日。[第2宮古島台風](../Page/第2宮古島台風.md "wikilink")）
      - 日本最大瞬間風速（山岳） 富士山山頂 91.0m/s （1966年9月25日）
  - 日本最低氣壓(含日本近海) 昭和54年台風20号 870hPa
    （[沖之鳥島南南東海上](../Page/沖之鳥島.md "wikilink")、1979年10月12日）
      - 日本最低氣壓(陸上) [沖永良部台風](../Page/沖永良部台風.md "wikilink") 907.3hPa
        （[沖永良部島](../Page/沖永良部島.md "wikilink")、1977年9月9日）
      - 在日本登錄時氣壓最低的台風 [室戶台風](../Page/室戶台風.md "wikilink") 911.6hPa
        （室戶岬、1934年9月21日）
  - 日本最大的地震 [東北地方太平洋近海地震](../Page/東北地方太平洋近海地震.md "wikilink")
    [Mw](../Page/Mw.md "wikilink")9.0 （2011年3月11日）
      - 日本最大的内陸直下型地震 [濃尾地震](../Page/濃尾地震.md "wikilink")
        [Mj](../Page/Mj.md "wikilink")8.0 （1891年10月28日）
          - 研究途上ではあるが、1586年1月18日の[天正大地震を日本最大の直下型地震とする説もある](../Page/天正大地震.md "wikilink")。M7.9～8.1
  - 日本最多人死亡的地震 [關東地震](../Page/關東地震.md "wikilink") 死者105,385　（1923年9月1日）
      - 日本因海嘯導致最多人死亡的地震 [明治三陸地震](../Page/明治三陸地震.md "wikilink")
        死者約21,900（1896年6月15日）
  - 日本最高的海嘯 明治三陸地震 38.2m
    （[岩手縣](../Page/岩手縣.md "wikilink")[大船渡市綾里](../Page/大船渡市.md "wikilink")，1896年6月15日，不含島嶼）
      - 另外2011年3月11日發生的[東北地方太平洋近海地震所引發的海嘯在](../Page/東北地方太平洋近海地震.md "wikilink")[岩手縣](../Page/岩手縣.md "wikilink")[宮古市竟高達](../Page/宮古市.md "wikilink")40.5m
  - 日本死者最多的火山災害
    [雲仙岳噴發](../Page/雲仙岳.md "wikilink")・山体崩壊・海嘯（[島原大変肥後迷惑](../Page/島原大変肥後迷惑.md "wikilink")）
    死者約15,000 （1792年4月1日）

## 地方自治体

### 面積

  - 日本最大的都道府縣 [北海道](../Page/北海道.md "wikilink") 83,453.57km²
      - 日本最大的縣 [岩手縣](../Page/岩手縣.md "wikilink") 15,279km²（在所有都道府縣中位居次席）
  - 日本最小的都道府縣 [香川縣](../Page/香川縣.md "wikilink")
    1,862km²　1994年（[平成](../Page/平成.md "wikilink")6年）[大阪府因](../Page/大阪府.md "wikilink")[關西國際機場填海完成后變為第二小](../Page/關西國際機場.md "wikilink")
  - 日本最大的市町村 [高山市](../Page/高山市.md "wikilink")（岐阜縣）
    2,179.67km²（比香川縣、大阪府還大，相當于不含島嶼部分的東京都）
      - 日本最大的[政令指定都市](../Page/政令指定都市.md "wikilink")
        [濱松市](../Page/濱松市.md "wikilink")（静岡縣） 1,511.17km²
      - 日本最大的政令指定都市行政區 静岡市[葵区](../Page/葵区.md "wikilink")（静岡縣）
        1,073.32km²
      - 日本最大的町 [足寄町](../Page/足寄町.md "wikilink")（北海道） 1,408.09km²
      - 日本最大的村 [十津川村](../Page/十津川村.md "wikilink")（奈良縣） 672.35km²
  - 日本最小的市町村 [舟橋村](../Page/舟橋村.md "wikilink")（富山縣） 3.47km²
    （2006年（平成18年）1月10日前為[鵜殿村](../Page/鵜殿村.md "wikilink")（現[紀宝町](../Page/紀宝町.md "wikilink")））
      - 日本最小的政令指定都市 [川崎市](../Page/川崎市.md "wikilink")（神奈川縣） 144.35km²
      - 日本最小的政令指定都市行政區 大阪市[浪速区](../Page/浪速区.md "wikilink")（大阪府） 4.37km²
      - 日本最小的市 [蕨市](../Page/蕨市.md "wikilink")（埼玉縣） 5.10km²
      - 日本最小的町 [春日町](../Page/春日町.md "wikilink")（愛知縣） 4.01km²

### 人口

以下數字若非特定說明，均為2007年人口普查的結果。

  - 日本人口最多的都道府縣 [東京都](../Page/東京都.md "wikilink") 12,576,601人
  - 日本人口最少的都道府縣 [鳥取縣](../Page/鳥取縣.md "wikilink") 607,012人
  - 日本人口最多的市町村 [横濱市](../Page/横濱市.md "wikilink")（神奈川縣） 3,579,628人
      - 日本人口最多的非政令指定都市 [相模原市](../Page/相模原市.md "wikilink")（神奈川縣） 701,630人
        ※相模原市及平成17年国勢調査以后編入該市的各町人口總和）
          - 2007年（平成19年）3月31日前為濱松市（靜岡縣）（804,032人）
      - 日本人口最多的町 [三好町](../Page/三好町.md "wikilink")（愛知縣） 56,252人
      - 日本人口最多的村 [瀧澤村](../Page/瀧澤村.md "wikilink")（岩手縣） 53,560人
      - 日本人口最多的[特別区](../Page/特別区.md "wikilink")（[東京都](../Page/東京都.md "wikilink")）
        [世田谷区](../Page/世田谷区.md "wikilink") 841,165人
      - 日本人口最多的政令指定都市行政區 橫濱市[港北区](../Page/港北区.md "wikilink")（神奈川縣）
        311,722人
  - 日本人口最少的市町村 [青島村](../Page/青島村_\(日本\).md "wikilink")（東京都） 214人
      - 2005年（平成17年）11月27日前衛[富山村](../Page/富山村.md "wikilink")（208人），后編入[豐根村](../Page/豐根村.md "wikilink")。
      - 不含離島的話是[大川村](../Page/大川村.md "wikilink")（高知縣） 538人
      - 日本人口最少的市 [歌志內市](../Page/歌志內市.md "wikilink")（北海道） 5,221人
      - 日本人口最少的政令指定都市 [静岡市](../Page/静岡市.md "wikilink")（靜岡縣） 700,886人
      - 日本人口最少的町 [早川町](../Page/早川町.md "wikilink")（山梨縣） 1,534人
      - 日本人口最少的縣廳所在地 [山口市](../Page/山口市.md "wikilink")（山口縣）191,704人
      - 日本人口最少的政令指定都市行政区　濱松市[天龍區](../Page/天龍區.md "wikilink")（靜岡縣）37,524人（是日本政令指定都市的行政區中唯一一個被定為過疏地域的區）
  - 日本人口密度最高的市町村 [蕨市](../Page/蕨市.md "wikilink")（埼玉縣）
      - 日本人口密度最高的町 [府中町](../Page/府中町.md "wikilink")（廣島縣）
          - 2006年（平成18年）3月20日前衛[師勝町](../Page/師勝町.md "wikilink")（愛知縣），市町村合併后為[北名古屋市的一部分](../Page/北名古屋市.md "wikilink")。
      - 日本人口密度最高的村 [北中城村](../Page/北中城村.md "wikilink")（沖繩縣）
          - 2006年（平成18年）1月10日前衛[鵜殿村](../Page/鵜殿村.md "wikilink")（三重縣），市町村合併后為[紀寶町的一部分](../Page/紀寶町.md "wikilink")。
  - 日本人口密度最低的市町村 [檜枝岐村](../Page/檜枝岐村.md "wikilink")（福島縣）
      - 日本人口密度最低的市 [夕張市](../Page/夕張市.md "wikilink")（北海道）
      - 日本人口密度最低的町 [幌加内町](../Page/幌加内町.md "wikilink")（北海道）
  - 日本昼間人口最多的市町村 [大阪市](../Page/大阪市.md "wikilink")（大阪府）3,581,675人
  - 日本昼間人口比率最高的市町村 [飛島村](../Page/飛島村.md "wikilink")（愛知縣） 313.8%
      - 日本昼間人口比率最高的市 [成田市](../Page/成田市.md "wikilink")（千葉縣） 141.2%
      - 日本昼間人口比率最高的町 [久御山町](../Page/久御山町.md "wikilink")（京都府） 173.6%
      - 日本昼間人口比率最高的東京都特別区 [千代田区](../Page/千代田区.md "wikilink") 2,047.3%
  - 日本昼間人口密最高的市町村 [大阪市](../Page/大阪市.md "wikilink")（大阪府）
  - 日本人口最多的[過疎地域](../Page/過疎地域.md "wikilink")
    [上越市](../Page/上越市.md "wikilink")（新潟縣） 208,082人

### 自治體数

  - 日本市町村数最多的都道府縣 [北海道](../Page/北海道.md "wikilink") 180市町村 ※日本實際統治地區
      - 日本市数最多的都道府縣 [埼玉縣](../Page/埼玉縣.md "wikilink") 40市
      - 日本郡数最多的都道府縣 [北海道](../Page/北海道.md "wikilink") 64郡 ※日本實際統治地區
      - 日本町数最多的都道府縣 [北海道](../Page/北海道.md "wikilink") 130町
      - 日本町村数最多的都道府縣 [北海道](../Page/北海道.md "wikilink") 145町村 ※日本實際統治地區
      - 日本村数最多的都道府縣 [長野縣](../Page/長野縣.md "wikilink") 37村
      - 日本区数最多的都市 [大阪市](../Page/大阪市.md "wikilink")（大阪府） 24区
      - 日本人口10万人以上的市最多的都道府縣 [大阪府](../Page/大阪府.md "wikilink") 22市
  - 日本市町村数最少的都道府縣 [富山縣](../Page/富山縣.md "wikilink") 15市町村
      - 日本市数最少的都道府縣 [鳥取縣](../Page/鳥取縣.md "wikilink") 4市
      - 日本郡数最少的都道府縣 [東京都](../Page/東京都.md "wikilink") 1郡
      - 日本町村数最少的都道府縣 [大分縣](../Page/大分縣.md "wikilink") 4町村
      - 日本町数最少的都道府縣 [大分縣](../Page/大分縣.md "wikilink") 3町
      - 日本村数最少的都道府縣 [栃木縣等](../Page/栃木縣.md "wikilink")13縣，均為0村
      - 日本人口10万人以上的市最少的都道府縣 [高知縣](../Page/高知縣.md "wikilink")
        [和歌山縣](../Page/和歌山縣.md "wikilink") 1市
  - 日本過疎地域最少的都道府縣
    [神奈川縣](../Page/神奈川縣.md "wikilink")・[大阪府](../Page/大阪府.md "wikilink")
    都是0%

### 其他

  - 日本人口與[社長比最高的市](../Page/社長.md "wikilink")
    [三条市](../Page/三条市.md "wikilink")（新潟縣）
  - 日本面積魚[工場比最高得市](../Page/工場.md "wikilink")
    [東大阪市](../Page/東大阪市.md "wikilink")（大阪府）
  - 日本標高最高的市町村 [川上村](../Page/川上村_\(長野縣\).md "wikilink")（長野縣） 村役場標高1,185m
  - 日本標高最高的市 [茅野市](../Page/茅野市.md "wikilink")（長野縣） （市庁舎標高801m）
  - 日本高低差最大的市 [富士宮市](../Page/富士宮市.md "wikilink")（靜岡縣）
    最高3,776m、最低35m、高低差3,741m
  - 日本海岸線最長的都道府縣 [長崎縣](../Page/長崎縣.md "wikilink")
  - 日本島數最多的都道府縣 [長崎縣](../Page/長崎縣.md "wikilink") 971島
    （[海上保安廳水路部調查](../Page/海上保安廳.md "wikilink")）
  - 日本島數最多的市町村 [對馬市](../Page/對馬市.md "wikilink")（長崎縣） 160島
  - 日本市議會議員名額最多的市 [横濱市](../Page/横濱市.md "wikilink")（神奈川縣） 92人
  - 日本市町村議會議員名額最少的市町村 [北大東村](../Page/北大東村.md "wikilink")（沖繩縣） 5人
  - 日本[水田面積最廣的市](../Page/水田.md "wikilink")
    [新潟市](../Page/新潟市.md "wikilink")（新潟縣）

## 文学・書籍・平面媒體

  - 銷售量最多的記録
      - 一般書籍
        《[窗口邊的小荳荳](../Page/窗口邊的小荳荳.md "wikilink")》（[黒柳徹子](../Page/黒柳徹子.md "wikilink")）
      - 中型國語辞典 『[廣辞苑](../Page/廣辞苑.md "wikilink")』
      - 小型國語辞典 『[新明解國語辞典](../Page/新明解國語辞典.md "wikilink")』
      - 新書
        『[バカの壁](../Page/バカの壁.md "wikilink")』（[養老孟司](../Page/養老孟司.md "wikilink")）
      - 小説（單行本）
        『[在世界的中心呼喚愛](../Page/在世界的中心呼喚愛.md "wikilink")』（[片山恭一](../Page/片山恭一.md "wikilink")）
      - 小説（新潮文庫）
        『[人間失格](../Page/人間失格.md "wikilink")』（[太宰治](../Page/太宰治.md "wikilink")）、『[心](../Page/心_\(小説\).md "wikilink")』（[夏目漱石](../Page/夏目漱石.md "wikilink")）
  - 日本卷數最多的系列小說 『[グイン・サーガ](../Page/グイン・サーガ.md "wikilink")』栗本薫
    正传120卷、外传21卷(正传、外传皆未完結)
  - 日本排队购买队列最长的书籍
    『[西田幾多郎全集](../Page/西田幾多郎.md "wikilink")』（1947年、在发售前3天就有人到[岩波書店门口熬夜排队去了](../Page/岩波書店.md "wikilink")）
  - 日本初版印数最多的书
    『[哈利·波特与凤凰社](../Page/哈利·波特与凤凰社.md "wikilink")』（290万本、漫画书除外）
  - 日本编修耗时最长的书 『[大日本史](../Page/大日本史.md "wikilink")』（大约花了250年才完成）
  - 连载回数总计最多的连载小说
    『[人間革命](../Page/人間革命.md "wikilink")』（[池田大作](../Page/池田大作.md "wikilink")、含续作『新·人間革命』）至2006年11月18日現在有4,955回（连载还在继续）
  - 日本卷、号数总计最多的杂志 『週刊东洋经济』（[东洋经济新报社](../Page/东洋经济新报社.md "wikilink")）
  - 日本發行量最多的報紙（也是世界第一） [讀賣新聞](../Page/讀賣新聞.md "wikilink")
  - 日本發行量最多的免费報紙（也是世界第一） 『[ぱど](../Page/ぱど.md "wikilink")』
  - 日本發行量最多的機關報（誌） 『JAFMATE』（[日本汽車聯盟](../Page/日本汽車聯盟.md "wikilink")）

### 漫画

  - 通算發行本数
      - [少年漫画](../Page/少年漫画.md "wikilink") 『[ONE
        PIECE](../Page/ONE_PIECE.md "wikilink")』（[尾田榮一郎](../Page/尾田榮一郎.md "wikilink")）2亿8,000万部
      - 劇画 『ゴルゴ13』（さいとう・たかを）2亿部
      - 搞笑漫画
        『[烏龍派出所](../Page/烏龍派出所.md "wikilink")』（[秋本治](../Page/秋本治.md "wikilink")）1亿5,527万部
      - S科幻漫画
        『[哆啦A夢](../Page/哆啦A夢.md "wikilink")』（[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")）1亿部
      - 報紙連載漫画 『蠑螺小姐』（[長谷川町子](../Page/長谷川町子.md "wikilink")）8,600万部
      - [少女漫画](../Page/少女漫画.md "wikilink")
        『[流星花園](../Page/流星花園.md "wikilink")』（[神尾葉子](../Page/神尾葉子.md "wikilink")）5,800万部
  - 初版發行本数
      - 少年漫画 『ONE PIECE』64卷・65卷・66卷（尾田榮一郎） 400万部
      - 少女漫画
        『[櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")』7卷（[櫻桃子](../Page/櫻桃子.md "wikilink")）
        238.8万部
  - 通算連載回数 『まっぴら君』（加藤芳郎）13,615回
      - 单独一人持续绘画连载最久的是『ほのぼの君』（佃公彦）2007年3月8日完结。若包含途中变更了标题的『ちびっこ紳士』则有15,451回
  - 最長連載
      - 总计 『仙人部落』（[小島功](../Page/小島功.md "wikilink")）1956年～2014年 共2861話
      - 未停載 『ゴルゴ13』（さいとう・たかを）1968年10月～連載中
      - 未停載・週刊誌 『烏龍派出所』（秋本治）1976年～2016年9月17日共1960話
  - 日本卷数最多的漫画 『烏龍派出所』（秋本治）200卷
      - 日本卷數最多的劇画 『ゴルゴ13』（さいとう・たかを）182卷（連載中）
      - 日本卷数最多的少女漫画 『あさりちゃん』（室山まゆみ）92卷（連載中）

## 電視・廣電媒體

  - 日本最[長壽節目](../Page/長壽節目.md "wikilink") -
    「[假面騎士系列](../Page/假面騎士系列.md "wikilink")」（1971年4月3日到今日，連續46年播出）
      - 含新聞節目 -
        [NNN今日消息](../Page/NNN今日消息.md "wikilink")（1954年到2006年，連續52年）
  - 最高[收視率](../Page/收視率.md "wikilink")（關東地区） - 「ボクシング世界フライ級選手権
    [白井義男](../Page/白井義男.md "wikilink")×[パスカル・ペレス](../Page/パスカル・ペレス.md "wikilink")」（[日本電視台](../Page/日本電視台.md "wikilink")）（[電通調查](../Page/電通.md "wikilink")、96.1%）\[2\]
      - [Video Research成立後](../Page/Video_Research.md "wikilink") -
        「[第14回NHK紅白歌合戰](../Page/第14回NHK紅白歌合戰.md "wikilink")」(81.4%)
      - [AC尼爾森調査](../Page/AC尼爾森.md "wikilink") - 「第14回NHK紅白歌合戰」(89.8%)
      - 多台同時播出節目（Video Research） -
        [淺間山莊事件](../Page/淺間山莊事件.md "wikilink")（1972年2月28日18時26分。NHK・民放合計89.7%）
      - 6時～24時的平均總家庭收視率（Video Research） -
        1972年2月28日（[淺間山莊事件](../Page/淺間山莊事件.md "wikilink")）、1989年2月24日（[昭和天皇的](../Page/昭和天皇.md "wikilink")[葬禮](../Page/葬禮.md "wikilink")）（NHK・民放合計62.8%）
  - 連續直播時間テレビ局 - [QVC](../Page/QVC.md "wikilink")（89小時。2004年5月19日
    7:00～2004年5月22日 24:00）
  - 頻道總數最多的電視台 - [SKY
    PerfecTV\!](../Page/SKY_PerfecTV!.md "wikilink")（运行中，频道数多于280）
      - 只有音频 -
        [サウンドプラネット](../Page/サウンドプラネット.md "wikilink")（运行中，频道数多于500）
  - 可觀看觀眾最多的電視台（[無線電視](../Page/無線電視.md "wikilink")） -
    [NHK](../Page/日本放送協會.md "wikilink")（全日本）
      - 含[衛星放送](../Page/衛星放送.md "wikilink") -
        [放送大學學園](../Page/放送大學學園.md "wikilink")
      - CS電視台 - [SKY PerfecTV\!](../Page/SKY_PerfecTV!.md "wikilink")
      - 民營的專門頻道 -
        [ショップチャンネル](../Page/ショップチャンネル.md "wikilink")（1,300万戶以上）
          - （新聞・經濟） - [日經CNBC](../Page/日經CNBC.md "wikilink")（500万戶以上）
          - （音樂） -
            [スペースシャワーTV](../Page/スペースシャワーTV.md "wikilink")（600万戶以上）
          - (動畫) - [キッズステーション](../Page/キッズステーション.md "wikilink")（600万戶以上）
      - 網路電視 - [BIGLOBEストリーム](../Page/BIGLOBEストリーム.md "wikilink")
  - 有線電視 - [USEN440](../Page/USEN440.md "wikilink")
  - 放送對象地域最小的[縣域放送局](../Page/縣域放送.md "wikilink")（民放） -
    [西日本放送廣播](../Page/西日本放送.md "wikilink")・[FM香川](../Page/FM香川.md "wikilink")（只限縣域民放電視台的話是[大阪電視台](../Page/大阪電視台.md "wikilink")）

## 土地・建造物

  - 日本最高的建造物
    [東京天空樹](../Page/東京天空樹.md "wikilink")（[東京都](../Page/東京都.md "wikilink")[墨田區](../Page/墨田區.md "wikilink")）高634m
  - 日本最高的大廈
    [阿倍野HARUKAS](../Page/阿倍野HARUKAS.md "wikilink")（[大阪市](../Page/大阪市.md "wikilink")）
    高300m
  - 日本最大的巨蛋[福岡巨蛋建築面積](../Page/福岡巨蛋.md "wikilink")：69,130 m²最高高度：83.96
    m（地上7階）
  - 日本最高的分契式公寓 The
    Kitahama（北濱鐵塔）（[大阪市](../Page/大阪市.md "wikilink")）地上209m
    54階建 戸数465

## 脚注

<references/>

## 外部連結

  - [日本一ネット](http://www.nippon-1.net/)
  - [都道府縣プロフィール](http://uub.jp/prf/)
  - [『日本の米を支える空知産米』 概要 整備
    効果](http://www.sorachi.pref.hokkaido.lg.jp/NR/rdonlyres/60BA0BE6-C597-4A9A-9206-280F909FEBBC/0/sorachikomepr.pdf)

[\*](../Category/各國之最.md "wikilink")
[Category:日本相關列表](../Category/日本相關列表.md "wikilink")

1.  [気象庁歴代全国ランキング](http://www.data.jma.go.jp/obd/stats/etrn/view/rankall.php?prec_no=&prec_ch=&block_no=&block_ch=&year=&month=&day=&elm=rankall&view=)
2.  『放送五十年史 資料編』 p.590