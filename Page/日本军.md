**日本軍**（），簡稱**日軍**，廣義上是指[日本的國家](../Page/日本.md "wikilink")[軍隊](../Page/軍隊.md "wikilink")，但通常指[大日本帝國時期](../Page/大日本帝國.md "wikilink")（[明治維新至](../Page/明治維新.md "wikilink")[第二次世界大戰結束前](../Page/第二次世界大戰.md "wikilink")）的日本軍隊，包含[日本陸軍與](../Page/大日本帝國陸軍.md "wikilink")[日本海軍](../Page/大日本帝國海軍.md "wikilink")。其又稱“**皇军**”（），即[天皇统领下的军队](../Page/天皇.md "wikilink")；此外，另有**帝國陸海軍**、**大日本軍**、**官軍**等稱呼。為了與戰後的[自衛隊區別](../Page/自衛隊.md "wikilink")，有時亦以**舊日本軍**、**舊軍**稱呼。[最高统帅为天皇](../Page/最高统帅.md "wikilink")，[軍政事務分由](../Page/军事管治.md "wikilink")[陸軍部與](../Page/陸軍部.md "wikilink")[海軍部掌理](../Page/海軍部.md "wikilink")（均為[日本內閣機關](../Page/日本內閣.md "wikilink")），軍事指揮則分由陆军[参谋本部](../Page/参谋本部_\(大日本帝国\).md "wikilink")、海军[军令部負責](../Page/軍令部_\(日本海軍\).md "wikilink")。其無獨立的[空軍](../Page/空軍.md "wikilink")，陸軍與海軍擁有各自的空中武力部隊。

## 发展历史

日本軍起始於1868年[鳥羽伏見之戰後組建的](../Page/鳥羽伏見之戰.md "wikilink")「御親兵」（[近衛師團前身](../Page/近衛師團.md "wikilink")），直至1871年[明治新政府實施](../Page/明治維新.md "wikilink")後，成為日本的[武裝力量](../Page/武裝力量.md "wikilink")。在建軍初期，由於許多[薩摩藩出身者組成了](../Page/薩摩藩.md "wikilink")[帝國海軍的骨幹](../Page/日本帝國海軍.md "wikilink")，而陸軍的中樞將校群則有許多[長州藩的人士](../Page/長州藩.md "wikilink")，因此有「陸軍長州，海軍薩摩」（）的並稱。

1894年[甲午战争后](../Page/甲午战争.md "wikilink")，日本將《[马关条约](../Page/马关条约.md "wikilink")》中的赔款投放於擴充军队。

1945年8月15日，身為[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[軸心國日本向同盟國](../Page/軸心國.md "wikilink")[投降](../Page/日本投降.md "wikilink")，日本軍解散。

## 組織結構

日本的兩大[軍種](../Page/軍種.md "wikilink")——[陸軍和](../Page/大日本帝國陸軍.md "wikilink")[海軍是各自為政的](../Page/大日本帝國海軍.md "wikilink")，彼此擁有自己的軍事組織與空中武力。兩者之間亦長期存在[軍種對立的情形](../Page/軍種對立.md "wikilink")\[1\]。大體來說，日本的軍事組織可分為軍隊（如陸軍的軍、師團，海軍的鎮守府、艦隊等）、[官衙](../Page/政府機關.md "wikilink")（如[陸軍省與](../Page/陸軍省.md "wikilink")[海軍省](../Page/海軍省.md "wikilink")）、[學校](../Page/軍校.md "wikilink")、[特務機關等四大類型](../Page/特務機關.md "wikilink")。在政治關係中，陸軍的影響力較大，特別是在[東条英機成為](../Page/東条英機.md "wikilink")[首相後](../Page/日本首相.md "wikilink")。

  - [天皇](../Page/天皇_\(日本\).md "wikilink")（[最高指揮官兼](../Page/總司令.md "wikilink")[大元帥](../Page/大元帥.md "wikilink")）
      - [元帥府](../Page/元帥府.md "wikilink")（由[元帥組成](../Page/日本元帅.md "wikilink")，是天皇對軍事問題的最高顧問機關）
      - [軍事參議院](../Page/軍事參議院.md "wikilink")（由元帥與高等[軍官組成的](../Page/軍官.md "wikilink")[合議制機關](../Page/合議制.md "wikilink")，為天皇提供軍事上的諮詢）
      - [大本營](../Page/大本营_\(大日本帝国\).md "wikilink")（戰爭與[事變發生時設置的](../Page/事變.md "wikilink")[統帥機關](../Page/統帥權.md "wikilink")）
      - [陸軍](../Page/大日本帝國陸軍.md "wikilink")
          - 官衙：[陸軍省](../Page/陸軍省.md "wikilink")、[參謀本部](../Page/参谋本部_\(大日本帝国\).md "wikilink")、[教育總監部](../Page/教育總監部.md "wikilink")、[陸軍航空總監部](../Page/陸軍航空總監部.md "wikilink")、[防衛總司令部](../Page/防衛總司令部.md "wikilink")
            等
          - 軍隊：**[總軍](../Page/總軍.md "wikilink")**、**[方面軍](../Page/方面軍.md "wikilink")**、**[軍](../Page/军_\(军队\).md "wikilink")**、[師團](../Page/師團.md "wikilink")、[集團](../Page/集團.md "wikilink")、[旅團](../Page/旅.md "wikilink")、[團](../Page/團_\(軍隊\).md "wikilink")、[聯隊等](../Page/联队_\(日本\).md "wikilink")
          - 學校：[陸軍士官學校](../Page/陆军士官学校_\(日本\).md "wikilink")、[陸軍大學校等](../Page/陸軍大學校.md "wikilink")
      - [海軍](../Page/大日本帝國海軍.md "wikilink")
          - 官衙：[海軍省](../Page/海軍省.md "wikilink")、[軍令部](../Page/軍令部_\(日本海軍\).md "wikilink")、[海軍艦政本部](../Page/艦政本部.md "wikilink")、[海軍航空本部](../Page/海軍航空本部.md "wikilink")、[海軍教育本部](../Page/海軍教育本部.md "wikilink")、[水路部等](../Page/水路部_\(日本海軍\).md "wikilink")
          - 軍隊：[聯合艦隊](../Page/聯合艦隊.md "wikilink")、[艦隊](../Page/艦隊.md "wikilink")、[鎮守府](../Page/镇守府_\(日本海军\).md "wikilink")、[海軍航空隊](../Page/大日本帝國海軍航空隊.md "wikilink")、[海軍陸戰隊](../Page/大日本帝國海軍陸戰隊.md "wikilink")
          - 學校：[海軍大學校](../Page/海軍大學校.md "wikilink")、[海軍兵學校](../Page/海軍兵學校.md "wikilink")、[海軍機關學校](../Page/海軍機關學校.md "wikilink")、[海軍經理學校等](../Page/海軍經理學校.md "wikilink")
      - 陸海軍共通之特務機關
          - [侍從武官府](../Page/侍從武官.md "wikilink")
          - [東宮武官](../Page/東宮武官.md "wikilink")
          - [皇族附武官](../Page/皇族附武官.md "wikilink")
          - [駐在武官](../Page/武官.md "wikilink")

※備註：粗體者為戰時特殊編制。

## 参考文献

## 参见

  - [日本军事](../Page/日本军事.md "wikilink")
  - [自衛隊](../Page/自衛隊.md "wikilink")

[日军](../Category/日军.md "wikilink")
[Category:第二次世界大战](../Category/第二次世界大战.md "wikilink")
[Category:1871年建立](../Category/1871年建立.md "wikilink")
[Category:1945年廢除](../Category/1945年廢除.md "wikilink")

1.  [陸海軍對立，以致不能統制_檢證戰爭責任_讀書_和訊網](http://data.book.hexun.com.tw/chapter-642-4-8.shtml)