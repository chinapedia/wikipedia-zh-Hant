**全國地方公共團體編號**是[日本](../Page/日本.md "wikilink")[地方公共團體](../Page/地方公共團體.md "wikilink")（[都道府縣](../Page/都道府縣.md "wikilink")・[市町村](../Page/市町村.md "wikilink")・[特別區](../Page/東京23區.md "wikilink")・[行政區](../Page/行政區.md "wikilink")・[一部事務組合等](../Page/一部事務組合.md "wikilink")）的5-6位數編碼。又名**JIS地名編號**、**地方自治體編號**、**都道府縣編號**、**市町村編號**。

1968年，日本[自治省](../Page/自治省.md "wikilink")（現[總務省](../Page/總務省.md "wikilink")）為簡化事務處理而導入。1970年4月1日成為行政管理廳（後為[總務廳](../Page/總務廳.md "wikilink")，現總務省）用於統計處理用時的編號，以後用於[國勢調査等各種統計](../Page/人口普查.md "wikilink")。另外，於同日指定為[日本工業規格](../Page/日本工業規格.md "wikilink")(JIS)。

編號是6位數字，頭2位是JIS X 0401制定的都道府縣編號，加上後3位共5位為JIS X
0402的市區町村編號及一部事務組合等編號，最後1位是檢查數字（[校驗碼](../Page/校驗碼.md "wikilink")）。有些時候只使用5位編號而不記錄檢查數字。

## 編號方式

### 都道府縣編號

各都道府縣由北至南，分別為01至47號。這個編號亦用於ISO地域編號[ISO
3166-2](../Page/ISO_3166.md "wikilink")([ISO
3166-2:JP](../Page/ISO_3166-2:JP.md "wikilink"))。

01:[北海道](../Page/北海道.md "wikilink") 02:[青森縣](../Page/青森縣.md "wikilink")
03:[岩手縣](../Page/岩手縣.md "wikilink") 04:[宮城縣](../Page/宮城縣.md "wikilink")
05:[秋田縣](../Page/秋田縣.md "wikilink") 06:[山形縣](../Page/山形縣.md "wikilink")
07:[福島縣](../Page/福島縣.md "wikilink")
08:[茨城縣](../Page/茨城縣.md "wikilink") 09:[栃木縣](../Page/栃木縣.md "wikilink")
10:[群馬縣](../Page/群馬縣.md "wikilink") 11:[埼玉縣](../Page/埼玉縣.md "wikilink")
12:[千葉縣](../Page/千葉縣.md "wikilink") 13:[東京都](../Page/東京都.md "wikilink")
14:[神奈川縣](../Page/神奈川縣.md "wikilink")
15:[新潟縣](../Page/新潟縣.md "wikilink") 16:[富山縣](../Page/富山縣.md "wikilink")
17:[石川縣](../Page/石川縣.md "wikilink") 18:[福井縣](../Page/福井縣.md "wikilink")
19:[山梨縣](../Page/山梨縣.md "wikilink") 20:[長野縣](../Page/長野縣.md "wikilink")
21:[岐阜縣](../Page/岐阜縣.md "wikilink") 22:[静岡縣](../Page/静岡縣.md "wikilink")
23:[愛知縣](../Page/愛知縣.md "wikilink")
24:[三重縣](../Page/三重縣.md "wikilink") 25:[滋賀縣](../Page/滋賀縣.md "wikilink")
26:[京都府](../Page/京都府.md "wikilink") 27:[大阪府](../Page/大阪府.md "wikilink")
28:[兵庫縣](../Page/兵庫縣.md "wikilink") 29:[奈良縣](../Page/奈良縣.md "wikilink")
30:[和歌山縣](../Page/和歌山縣.md "wikilink")
31:[鳥取縣](../Page/鳥取縣.md "wikilink") 32:[島根縣](../Page/島根縣.md "wikilink")
33:[岡山縣](../Page/岡山縣.md "wikilink") 34:[廣島縣](../Page/廣島縣.md "wikilink")
35:[山口縣](../Page/山口縣.md "wikilink") 36:[德島縣](../Page/德島縣.md "wikilink")
37:[香川縣](../Page/香川縣.md "wikilink") 38:[愛媛縣](../Page/愛媛縣.md "wikilink")
39:[高知縣](../Page/高知縣.md "wikilink")
40:[福岡縣](../Page/福岡縣.md "wikilink") 41:[佐賀縣](../Page/佐賀縣.md "wikilink")
42:[長崎縣](../Page/長崎縣.md "wikilink") 43:[熊本縣](../Page/熊本縣.md "wikilink")
44:[大分縣](../Page/大分縣.md "wikilink") 45:[宮崎縣](../Page/宮崎縣.md "wikilink")
46:[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")
47:[沖繩縣](../Page/沖繩縣.md "wikilink")

### 市區町村及部份事務組合等編號

都道府縣内各市區町村、廣域聯合的一部事務組合、地方公共團體，加上都道府縣自體的都道府縣編號組成5位數字 。

  - [都道府縣](../Page/都道府縣.md "wikilink")
      - 使用"000"。
          - 例 : 青森縣 02000

<!-- end list -->

  - [政令指定都市](../Page/政令指定都市.md "wikilink")
      - 政令指定都市，原則上依都道府縣内的政令指定施行順序使用"100"、"130"、"160"
        等30間隔的數字。政令指定都市內的各區為"101"、"131"、"161"
        等順序編號。
      - 至于[堺市](../Page/堺市.md "wikilink")，由於[大阪市已經有](../Page/大阪市.md "wikilink")24個分區，為今後分區數增加等考量，例外地使用140編號。
          - 例 : [横濱市](../Page/横濱市.md "wikilink")
            14100、[鶴見區](../Page/鶴見區_\(横濱市\).md "wikilink")
            14101、[神奈川區](../Page/神奈川區.md "wikilink") 14102……
            　　　[堺市](../Page/堺市.md "wikilink")
            27140、[堺區](../Page/堺區.md "wikilink")
            27141、[中區](../Page/中區_\(堺市\).md "wikilink") 27142……

<!-- end list -->

  - 東京都的[特別區](../Page/東京23區.md "wikilink")
      - 與政令指定都市相同，「特別區」編號由"100"開始。各區由"101"起順序編號。
          - 例 : 特別區 13100、[千代田區](../Page/千代田區.md "wikilink")
            13101、[中央區](../Page/中央區_\(東京\).md "wikilink")
            13102……

<!-- end list -->

  - [市](../Page/市.md "wikilink")
      - "201"起順序為各市編號。
          - 例 : [佐賀市](../Page/佐賀市.md "wikilink")
            41201、[唐津市](../Page/唐津市_\(日本\).md "wikilink")
            41202、[鳥栖市](../Page/鳥栖市.md "wikilink") 41203……

<!-- end list -->

  - [町](../Page/町.md "wikilink")[村](../Page/村.md "wikilink")
      - 使用"301"以後數字。町村以郡為集團，各郡間分別為"301"、"321" 、"341" ……等間隔20的數字順序編號。
          - 例 :
            [和歌山縣](../Page/和歌山縣.md "wikilink")[海草郡](../Page/海草郡.md "wikilink")[下津町](../Page/下津町.md "wikilink")
            30301、[野上町](../Page/野上町.md "wikilink")
            30302、[美里町](../Page/美里町_\(和歌山縣\).md "wikilink")
            30303
            　　　[那賀郡](../Page/那賀郡_\(和歌山縣\).md "wikilink")[打田町](../Page/打田町.md "wikilink")
            30321、[粉河町](../Page/粉河町.md "wikilink")
            30322、[那賀町](../Page/那賀町_\(和歌山縣\).md "wikilink")
            30323……
      - 沖繩縣島尻郡的町村數超過20個，例外地分配有341～369編號、宮古郡變為371～379。
      - 北海道的町村以[支廳為集團](../Page/支廳.md "wikilink")，採用"301"、"331"、"361"
        ……等間隔30的數字順序編號。
        東京都的離島亦使用支廳集團式編號，但續以20為間隔。

<!-- end list -->

  - [一部事務組合等](../Page/一部事務組合.md "wikilink")
      - 使用"801"以後數字。

## 外部連結

  - [地方公共團體編號住所一覧](https://web.archive.org/web/20071028074159/http://www.lasdec.nippon-net.ne.jp/com/addr/jyu_top.htm)（LASDEC
    財團法人 地方自治情報中心）

[X0401](../Category/JIS.md "wikilink")
[\*](../Category/日本行政區劃.md "wikilink")
[\*](../Category/日本政府.md "wikilink")