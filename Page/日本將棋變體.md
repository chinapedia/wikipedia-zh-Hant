此條目所描述的是日本將棋除了本將棋之外，從昔而今衍生而成的[變體列舉](../Page/象棋變體.md "wikilink")。

## 傳統日本將棋

### 古將棋

從[平安時代到](../Page/平安時代.md "wikilink")[江戶時代間衍生出來的日本將棋變體都被統稱為](../Page/江戶時代.md "wikilink")「古將棋」。

| 將棋稱呼                                   | 出現時代                               | 棋盤大小        | 各方棋數  | 棋子種類(包含升級) | 備註說明                                         |
| -------------------------------------- | ---------------------------------- | ----------- | ----- | ---------- | -------------------------------------------- |
| [平安將棋](../Page/平安將棋.md "wikilink")     | 平安時代                               | 8×8或8×9、9×9 | 16或18 | 6          | 最早的將棋，與[泰國象棋類似](../Page/泰國象棋.md "wikilink")。 |
| [平安大將棋](../Page/平安大將棋.md "wikilink")   | 平安時代                               | 13×13       | 34    | 14         | 自平安將棋擴充。                                     |
| [小將棋](../Page/小將棋.md "wikilink")       | [鎌倉時代](../Page/鎌倉時代.md "wikilink") | 9×9         | 21    | 11         | 自平安將棋改良，本將棋的前身。                              |
| [大將棋](../Page/大將棋.md "wikilink")       | 鎌倉時代                               | 15×15       | 65    | 36         | 自平安大將棋擴充。                                    |
| [中將棋](../Page/中將棋.md "wikilink")       | [室町時代](../Page/室町時代.md "wikilink") | 12×12       | 46    | 28         | 自大將棋改良，是本將棋出現前，最流行的變體。                       |
| [大大將棋](../Page/大大將棋.md "wikilink")     | 室町時代                               | 17×17       | 96    | 68         | 自大將棋擴充。                                      |
| [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink") | 室町時代                               | 19×19       | 96    | 63         | 從大大將棋擴充。                                     |
| [泰將棋](../Page/泰將棋.md "wikilink")       | 室町時代                               | 25×25       | 177   | 101        | 自摩訶大大將棋擴充，曾被認為是最大規模的將棋。                      |
| [天竺大將棋](../Page/天竺大將棋.md "wikilink")   | 室町時代                               | 16×16       | 78    | 43         | 從中將棋改良，擁有獨特的棋類。                              |
| [和將棋](../Page/和將棋.md "wikilink")       | 江戶時代                               | 11×11       | 27    | 22         | 棋類有許多動物。                                     |
| [大局將棋](../Page/大局將棋.md "wikilink")     | 江戶時代                               | 36×36       | 402   | 300        | 收入之前所有將棋的棋類，最大規模的將棋。                         |
| [廣將棋](../Page/廣將棋.md "wikilink")       | 江戶時代                               | 19×19       | 90    | 53         | 使用[圍棋盤](../Page/圍棋.md "wikilink")，有近代火器的棋類。  |
| [禽將棋](../Page/禽將棋.md "wikilink")       | 江戶時代                               | 7×7         | 16    | 6          | 以鳥類為棋類。                                      |

### 介紹古將棋的書籍

  - [象戲圖式](../Page/象戲圖式.md "wikilink")
  - [諸象戲圖式](../Page/諸象戲圖式.md "wikilink")
  - [象棋六種之圖式](../Page/象棋六種之圖式.md "wikilink")

## 本將棋變體

  - 原本形式：[本將棋](../Page/本將棋.md "wikilink")（9×9）

<!-- end list -->

  - [將棋殘局](../Page/詰將棋.md "wikilink")
  - [王手將棋](../Page/王手將棋.md "wikilink") -
    首先做到[王手](../Page/王手.md "wikilink")(將)的一方得到勝利
  - [安南將棋](../Page/安南將棋.md "wikilink") - 每一枚棋子的走法都成為正後鄰格己方棋子的步法
  - [戰霧將棋](../Page/戰霧將棋.md "wikilink") -
    在對局開始之前，玩家可以自由佈局。當雙方佈局完成之後，玩家才可以知道對方的佈局，對局隨即開始
  - [覆面將棋](../Page/覆面將棋.md "wikilink") -
    玩家不知道對手哪一隻棋子才是真正的王，玉將在對方的陣營可能只會是普通的棋子
  - [衝立將棋](../Page/衝立將棋.md "wikilink") - 玩家不知道對手的行動，他必須猜測對手棋子的位置
  - [獅王將棋](../Page/獅王將棋.md "wikilink") -
    [王將於此可以在一回合之內行動兩次](../Page/玉將.md "wikilink")(與[獅子的步法相同](../Page/獅子_\(日本將棋\).md "wikilink"))
  - [八方桂將棋](../Page/八方桂.md "wikilink") -
    [桂馬的移動方式於此與](../Page/桂馬.md "wikilink")[國際象棋的馬相同](../Page/國際象棋.md "wikilink")
  - [定身將棋](../Page/定身將棋.md "wikilink") - 隨機封鎖一縱行的棋子的行動
  - [不死銀將棋](../Page/不死銀將棋.md "wikilink")
  - [資本還原將棋](../Page/資本還原將棋.md "wikilink")
  - [三手将棋](../Page/三手将棋.md "wikilink")
  - [圓筒將棋](../Page/圓筒將棋.md "wikilink")

## 現代變體

  - [動物將棋](../Page/動物將棋.md "wikilink")（3×4）
  - [5五將棋](../Page/5五將棋.md "wikilink") (5×5)
  - [京都將棋](../Page/京都將棋.md "wikilink") (5×5)
  - [槍將棋](../Page/槍將棋.md "wikilink") (7×9)
  - [鯨將棋](../Page/鯨將棋.md "wikilink") (6×6)
  - [則憲將棋](../Page/則憲將棋.md "wikilink") (6×6)
  - [五分摩訶將棋](../Page/五分摩訶將棋.md "wikilink") (4×5)
  - [大砲將棋](../Page/大砲將棋.md "wikilink") (9×9)
  - [四人將棋](../Page/四人將棋.md "wikilink") (9×9) -
    [島根縣](../Page/島根縣.md "wikilink")[平田市長的概念](../Page/平田市.md "wikilink")。
  - [京將棋](../Page/京將棋.md "wikilink") (10×10)
  - [御妃將棋](../Page/御妃將棋.md "wikilink") (10×10)
  - [鬼將棋](../Page/鬼將棋.md "wikilink") (11×11)
  - [六角將棋](../Page/六角將棋.md "wikilink")
  - [三角將棋](../Page/三角將棋.md "wikilink")
  - [磚形將棋](../Page/磚形將棋.md "wikilink")
  - [軍人將棋](../Page/軍人將棋.md "wikilink")

## 遊戲方法上的變體

  - [記憶將棋](../Page/記憶將棋.md "wikilink")
  - [郵寄將棋](../Page/郵寄將棋.md "wikilink")
  - [人間將棋](../Page/人間將棋.md "wikilink") -
    [山形縣](../Page/山形縣.md "wikilink")[天童市的](../Page/天童市.md "wikilink")[祭典](../Page/祭典.md "wikilink")
  - [盲人將棋](../Page/盲人將棋.md "wikilink") - 為視障者而設的將棋
  - [跳將棋](../Page/跳將棋.md "wikilink")
  - [夾將棋](../Page/夾將棋.md "wikilink")

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")

## 外部連結

  - [將棋變體即時對弈網](http://shogitter.com/rule/#0)
  - [日本古將棋軟體](http://www.netspace.net.au/~trout/)
  - [各種將棋變體軟體](http://www.shogi.net/variants/variants.html)
  - [日本古將棋介紹](http://www.chushogi-renmei.com/koramu/index2.htm)
  - [日本古將棋介紹](http://history.chess.free.fr/shogivar.htm)
  - [古將棋館](http://home.att.ne.jp/omega/s-edge/shougi-main.html)

[\*](../Category/日本將棋變體.md "wikilink")