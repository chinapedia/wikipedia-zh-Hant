[Original_Ramen.jpg](https://zh.wikipedia.org/wiki/File:Original_Ramen.jpg "fig:Original_Ramen.jpg")

日式**拉麵**（）是[日本料理所使用的](../Page/日本料理.md "wikilink")[麵條與](../Page/麵條.md "wikilink")[麵食種類之一](../Page/麵食.md "wikilink")，多以切製而非拉製而成。因屬於中文[外來語](../Page/外來語.md "wikilink")，故常用[片假名來拼音](../Page/片假名.md "wikilink")“”，[日文汉字写法有](../Page/日文汉字.md "wikilink")“拉麺”、“老麺”、“柳麺”三种，在日本其他常见名称包括“****”、“****”或“****”。

## 历史

日本最早关于中国面条的记载是[明朝遗臣](../Page/明朝.md "wikilink")[朱舜水流亡到日本后](../Page/朱舜水.md "wikilink")，用面条來款待日本[江戶時代的](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")——[水户藩藩主](../Page/水户藩.md "wikilink")[德川光圀](../Page/德川光圀.md "wikilink")，但並未普及。

“”的名称的来源有多种说法。被最多人认同的说法是来自[汉语的](../Page/汉语.md "wikilink")“[拉面](../Page/拉面.md "wikilink")”，所以汉字写做「拉面」。另外的说法包括：老面、卤面、柳面
。

在[明治时代早期](../Page/明治.md "wikilink")，拉面是[横滨中华街常见的食品](../Page/横滨中华街.md "wikilink")。1900年代，来自[上海和](../Page/上海.md "wikilink")[广东的中国人在日本卖](../Page/广东.md "wikilink")[切面](../Page/切面.md "wikilink")，配以简单的汤底和配料。在[昭和年间](../Page/昭和.md "wikilink")，拉面在日本開始流行。那时拉面被叫做“”或“”（以中国都市命名乃强调其为中国产，而非特指南京）。

[第二次世界大战之后](../Page/第二次世界大战.md "wikilink")，来自[美国的廉价面粉和从中国战场回来的士兵使得中国风味的面条大行其道](../Page/美国.md "wikilink")。同时日本政府官方禁止使用一切关于“[支那](../Page/支那_\(日语\).md "wikilink")”的词汇，故民間習慣稱呼改為“”。“”是日本使用[荞麦製作的](../Page/荞麦.md "wikilink")[荞麦面条](../Page/荞麦面条.md "wikilink")。

1910年出生于[大日本帝國](../Page/大日本帝國.md "wikilink")[台灣](../Page/台灣.md "wikilink")[嘉義廳的](../Page/嘉義廳.md "wikilink")[安藤百福](../Page/安藤百福.md "wikilink")，在1958年发明的[-{zh-hans:方便面;zh-hant:泡麵;}-](../Page/方便面.md "wikilink")（即食麵）叫“拉面”，创立[日清食品公司](../Page/日清.md "wikilink")。拉面成为流行的方便食品。由此“拉面”这个词的使用也超过了其他的名称的使用。1980年代，日本拉面成了日本饮食文化的代表之一，日本各地都有人研发出别具地方风味的拉面。

近年来，由于对[-{zh-hans:疯牛病;zh-hant:狂牛病;}-的恐惧](../Page/疯牛病.md "wikilink")，日本人对和拉面的顾客層相同之另一快餐食品「[-{zh-hans:牛肉盖浇饭;zh-hk:日式牛肉飯;zh-tw:牛丼;}-](../Page/牛丼.md "wikilink")」的消费明显下降，拉面成了上班族的第一午餐选择。

而全球第一間取得米芝蓮一星的拉麵店「蔦」亦創新地以松露醬配搭拉面，把別具特色的拉面帶給世界\[1\]。

## 拉麵的製作

[Fresh_ramen_noodle_001.jpg](https://zh.wikipedia.org/wiki/File:Fresh_ramen_noodle_001.jpg "fig:Fresh_ramen_noodle_001.jpg")

早期最普遍的拉麵是加上[日式叉燒](../Page/日式叉燒.md "wikilink")、[筍子的](../Page/筍子.md "wikilink")[醬油口味](../Page/醬油.md "wikilink")，但現在拉麵的口味也越來越多樣化。

將使用手工或機械製作的麵條煮好，加上利用[豬骨](../Page/骨頭.md "wikilink")（日文：豚骨）或雞肉、蔬菜、小魚乾等熬煮的湯頭，大多都會再搭配[日式叉燒](../Page/日式叉燒.md "wikilink")、[筍子](../Page/筍子.md "wikilink")、[蔥花等配料](../Page/蔥.md "wikilink")。

大部分的麵條都是使用[麵粉](../Page/麵粉.md "wikilink")（小麥粉、強力粉）、[水](../Page/水.md "wikilink")、盐，以及「[鹼水](../Page/鹼水.md "wikilink")」（，又被音譯為「甘素」）為原料，顏色大多是黃色。鹼水是指[碳酸钾和](../Page/碳酸钾.md "wikilink")[碳酸钠的混合物](../Page/碳酸钠.md "wikilink")（有時也會加入[磷酸](../Page/磷酸.md "wikilink")）。這是由於曾有人使用[內蒙古的湖水來製作麵條](../Page/內蒙古.md "wikilink")，結果發現麵條變得更加好吃，因此研究了湖水的成份之後，發展出了這樣的配方。鹼水是屬於[碱性](../Page/碱.md "wikilink")，會讓麵粉中的[穀蛋白黏膠質產生性質變化](../Page/穀蛋白黏膠質.md "wikilink")，讓麵條具有光澤感和增加彈性，也會讓麵粉中的[黃酮類變成黃色](../Page/黃酮類.md "wikilink")，讓麵條具有獨特的顏色。

日本[戰後有一段時期](../Page/第二次世界大戰.md "wikilink")，出現了許多品質惡劣的「鹼水」，並且對健康可能會有不良的影響。現在[日本農林規格](../Page/日本農林規格.md "wikilink")（JAS）制定了成份的規定，鹼水已經不再有安全性的問題了。此外，也有因為不喜歡鹼水那種獨特的味道，而改用[雞蛋取代的做法](../Page/雞蛋.md "wikilink")。水含量（加水率）較高的麵條較柔軟，耐浸泡；水含量低的面条比较有劲，能够更多地吸收汤的味道，但容易泡烂。一般麵條中水的比例大約為20%到40%。

面条在各地粗细有所不同，但通常一家店只使用一种面。拉面店可以自己制作面条，也可以专门的面厂订做。订做时可以指定原料比例、制作方法、晾晒时间等等，以保持拉面店自己的特点。

## 湯底

[Japanese_Salt_flavor_Sapporo_Ramen.JPG](https://zh.wikipedia.org/wiki/File:Japanese_Salt_flavor_Sapporo_Ramen.JPG "fig:Japanese_Salt_flavor_Sapporo_Ramen.JPG")
[Misora-men2222222.jpg](https://zh.wikipedia.org/wiki/File:Misora-men2222222.jpg "fig:Misora-men2222222.jpg")
拉麵的[湯底大多有基本的調味材料](../Page/湯底.md "wikilink")，再添加不同的額外材料，成為各式各樣的湯頭。此外使用各地區不同的食材，也產生當地獨特的口味，使拉麵成為深入日本各地的普遍食物。

汤底的常见原料包括：[鸡肉](../Page/鸡肉.md "wikilink")、[猪骨](../Page/猪骨.md "wikilink")、[牛骨](../Page/牛.md "wikilink")、柴鱼乾（鰹節）、[青花魚乾](../Page/青花魚.md "wikilink")、[小鱼乾](../Page/小鱼乾.md "wikilink")、[海带](../Page/海带.md "wikilink")、炒[黄豆](../Page/黄豆.md "wikilink")、[香菇](../Page/香菇.md "wikilink")、[洋葱](../Page/洋葱.md "wikilink")、[葱等等](../Page/葱.md "wikilink")。拉面汤通常需要连续炖煮数小时甚至数天。有些拉面店使用或混用成桶买进的商业拉面汤，这种做法方便且可以降低成本，但专门的拉面饕客可以吃出其中的区别。

湯底的口味一般來說可分為[醬油味](../Page/醬油.md "wikilink")、[豚骨](../Page/豚骨.md "wikilink")（豬骨）味、[鹽味](../Page/鹽.md "wikilink")、[味噌味](../Page/味噌.md "wikilink")。

此外，也有像[擔擔面一般使用](../Page/擔擔面.md "wikilink")[唐辛子](../Page/唐辛子.md "wikilink")（[辣椒](../Page/辣椒.md "wikilink")）的辣味和[芝麻口味的湯底](../Page/芝麻.md "wikilink")、類似[生馬麵](../Page/生馬麵.md "wikilink")（日本一種地方麵食）的[醋味湯底](../Page/醋.md "wikilink")，以及歐式風味的[番茄湯底](../Page/番茄.md "wikilink")，甚至也有[咖哩的口味](../Page/咖哩.md "wikilink")。

亦有不同味道的演變，例如魚介味，主要用作混搭雞、豚湯底，配以昆布能更有效帶出鮮味\[2\]。

### 醬油味

[本州最主流的风味](../Page/本州.md "wikilink")，使用日本酱油、[鸡肉和](../Page/鸡.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。可隨個人喜好加入[辣油和](../Page/辣油.md "wikilink")[胡椒](../Page/胡椒.md "wikilink")。

### 豚骨味

最早發源自[九州](../Page/九州_\(日本\).md "wikilink")，使用猪骨长期熬炖出白色的浓汤。也可加酱油成为「豚骨酱油味」。

### 鹽味

麵汤清澈，源于[大正时期的](../Page/大正.md "wikilink")[北海道](../Page/北海道.md "wikilink")[函馆](../Page/函馆.md "wikilink")，因此又被稱為「[函館拉麵](../Page/函館拉麵.md "wikilink")」。和其他风味相比，更能突出湯底材料本身的味道。

### 味噌味

使用雞肉或豬肉熬製湯底，再以日本传统的[味噌酱调味](../Page/味噌.md "wikilink")。

## 其他元素

除了面条和湯底，拉面的其他重要元素包括面码、酱料、香油。

通常是把湯底放在碗中，加以特制的酱料，再把煮好的面放进汤中端给食用者。面上通常會放上的配料包括：[叉烧肉](../Page/叉烧.md "wikilink")、[海苔](../Page/海苔.md "wikilink")、[豆芽](../Page/豆芽.md "wikilink")、[白菜](../Page/白菜.md "wikilink")、[鸡蛋](../Page/鸡蛋.md "wikilink")、[蒜末](../Page/蒜.md "wikilink")、[笋](../Page/笋.md "wikilink")、[鱼板](../Page/鱼板.md "wikilink")、[玉米粒](../Page/玉米.md "wikilink")、[雪菜](../Page/雪菜.md "wikilink")、[土豆](../Page/土豆.md "wikilink")、燉肉、[酸梅等等](../Page/酸梅.md "wikilink")。最后可以再加一些香油或者[香辛料](../Page/香辛料.md "wikilink")。

## 日本各地方口味的拉麵

日本的“三大拉面”為：[北海道](../Page/北海道.md "wikilink")[札幌拉面](../Page/札幌拉面.md "wikilink")、[福冈](../Page/福冈县.md "wikilink")[博多拉面和](../Page/博多拉面.md "wikilink")[福岛](../Page/福岛县.md "wikilink")[喜多方拉面](../Page/喜多方拉面.md "wikilink")。此外，东京拉面、和歌山拉面、熊本拉面、长崎拉面等也都很有名。

### [北海道](../Page/北海道.md "wikilink")

  - [釧路拉麵](../Page/釧路拉麵.md "wikilink")（[釧路市](../Page/釧路市.md "wikilink")）：湯底主要使用[柴魚](../Page/柴魚.md "wikilink")，再加上[昆布](../Page/昆布.md "wikilink")（即[海带](../Page/海带.md "wikilink")）、小魚乾等魚類、豬骨、雞肉等，是屬於醬油風味的拉麵。味道十分清新，不會產生後勁。麵條原料中水的比例較高，是捲曲的極細麵條。

<!-- end list -->

  - 北見拉麵（[北見市](../Page/北見市.md "wikilink")）：主要使用[洋蔥熬煮的醬油湯底](../Page/洋蔥.md "wikilink")。

[Asahikawara-men.jpg](https://zh.wikipedia.org/wiki/File:Asahikawara-men.jpg "fig:Asahikawara-men.jpg")

  - [旭川拉麵](../Page/旭川拉麵.md "wikilink")（[旭川市](../Page/旭川市.md "wikilink")）：

<!-- end list -->

  -
    使用魚類和豬骨、雞肉等熬製的醬油風味。因為發源自寒冷的地區，因此為了避免熱度快速流失，添加了[豬油](../Page/豬油.md "wikilink")。口味較為清淡。
    麵條為水含量較低的捲曲細麵，很容易將湯汁附著於麵上。
    雖然以醬油口味為主流進行宣傳，但也有很受歡迎的味噌口味，因此旭川拉麵等于醬油拉麵的這個認知是錯誤的。和[札幌的味噌口味不同](../Page/札幌.md "wikilink")，旭川的[味噌湯底是強調味噌獨特的甜味](../Page/味噌湯.md "wikilink")。
    在附近的上川町也有所謂的「上川拉麵」，但基本上和旭川拉麵類似。

<!-- end list -->

  - [札幌拉麵](../Page/札幌拉麵.md "wikilink")（[札幌市](../Page/札幌市.md "wikilink")）：

<!-- end list -->

  -
    最早起源的是鹽味拉麵，但後來卻是味噌口味成為代表。是日本各地風味拉麵中最早獲得全國性知名度的口味。
    為了增加味噌湯底的濃郁口感，加入了大量的豬油和[蒜](../Page/蒜.md "wikilink")。湯底中還加入了炒過的蔬菜，將這些蔬菜與味噌一起融化之後還可以澆在白飯上，成為口味獨特的「味噌丼飯」。
    麵條的含水量很高。除了味噌口味之外，也有許多店家提供醬油口味和鹽味的拉麵，但比起其他地方較鹹和較辣。

[Siora-men.jpg](https://zh.wikipedia.org/wiki/File:Siora-men.jpg "fig:Siora-men.jpg")

  - [函館拉麵](../Page/函館拉麵.md "wikilink")（[函館市](../Page/函館市.md "wikilink")）：

<!-- end list -->

  -
    以鹽味的湯底為主流，是在日本其他地區都沒有類似的當地風味。
    用小火熬煮出淡淡的[豬骨湯底](../Page/豬骨湯.md "wikilink")，再加上鹽來調味，口味相當清淡。
    麵條是在拉麵中少見的「直條麵」。也有加上[乾酪粉的吃法](../Page/乾酪.md "wikilink")。

### 東北

  - 津輕拉麵（[弘前市](../Page/弘前市.md "wikilink")）

<!-- end list -->

  - 仙台拉麵（[仙台市](../Page/仙台市.md "wikilink")）

<!-- end list -->

  - [冷拉麵](../Page/冷拉麵.md "wikilink")（[山形市](../Page/山形市.md "wikilink")）

<!-- end list -->

  - 酒田拉麵（[酒田市](../Page/酒田市.md "wikilink")）

<!-- end list -->

  - 米澤拉麵（[米澤市](../Page/米澤市.md "wikilink")）

<!-- end list -->

  - [白河拉麵](../Page/白河麵.md "wikilink")（[白河市](../Page/白河市.md "wikilink")）

<!-- end list -->

  - [喜多方拉麵](../Page/喜多方麵.md "wikilink")（[喜多方市](../Page/喜多方市.md "wikilink")）\<\!--
    滋味豊かな地下水がラーメンスープに適していたためである。

スープは豚骨と煮干しのブレンドした醤油味で、かなり淡泊。 麵は太めの平打ち縮れ麵で、加水率が高くコシが強い。
ラーメン屋というスタイルで店舗を構えていないのが特徴で、「○○食堂」という風に大衆食堂の店構えをしているものが多い。--\>

### 關東

  - 東京拉麵（[東京都區部](../Page/東京都區部.md "wikilink")）

<!-- end list -->

  - 生馬麵（[橫濱市](../Page/橫濱市.md "wikilink")）

<!-- end list -->

  - 家系拉麵（橫濱市）：濃厚的豬骨醬油湯底，也加入了雞油。\<\!-- 太いストレート麵。

具はチャーシュー、葱、ホウレン草のほか、大判の焼き海苔が2～3枚載る。
俗に家系と呼ばれるラーメンが現在では横浜のご当地ラーメンとして知られるが、これは1990年代半ば頃の○○家と付く店が始まりで、比較的新しいものである。--\>

  - 油拉麵（[武藏野市](../Page/武藏野市.md "wikilink")）

<!-- end list -->

  - 豚骨醬油拉麵（東京都區部）

<!-- end list -->

  - 八王子拉麵（[八王子市](../Page/八王子市.md "wikilink")）

<!-- end list -->

  - 竹岡拉麵（[富津市](../Page/富津市.md "wikilink")）

<!-- end list -->

  - 佐野拉麵（[佐野市](../Page/佐野市.md "wikilink")）

<!-- end list -->

  - 藤岡拉麵（[藤岡市](../Page/藤岡市.md "wikilink")）

<!-- end list -->

  - 體力拉麵（）（[水户市](../Page/水户市.md "wikilink")）

<!-- end list -->

  -
    醬油
    風味拉麵。

<!-- end list -->

  - 體力拉麵（）（[埼玉縣](../Page/埼玉縣.md "wikilink")）：加入[豆瓣醬的醬油風味拉麵](../Page/豆瓣醬.md "wikilink")。

### 北信越

  - 燕三條系拉麵（[燕市](../Page/燕市.md "wikilink")、[三條市](../Page/三條市.md "wikilink")）：

<!-- end list -->

  -
    在湯底中加入豬背脊脂肪始祖。主要發源於[燕市](../Page/燕市.md "wikilink")，流傳到鄰近的[三條市](../Page/三條市.md "wikilink")。
    麵條極粗。以小魚干為主，在加上柴魚、青花魚等魚類熬煮的湯底。最後再加上大量的豬背脊脂肪和洋蔥切片。

<!-- end list -->

  - 新潟清淡系拉麵（[新潟市](../Page/新潟市.md "wikilink")）

<!-- end list -->

  - 新潟濃厚味噌拉麵（新潟市）

<!-- end list -->

  - 長岡系拉麵（[長岡市](../Page/長岡市.md "wikilink")）

<!-- end list -->

  - 富山黑拉麵（[富山市](../Page/富山市.md "wikilink")）\<\!-- 富山県内で多い系統のラーメン。

醤油をベースにしており、墨汁のように黒いのが語源。 麵は少し固めの太麵。 薬味として黒胡椒が振り掛けられている。--\>

### 東海

  - [高山拉麵](../Page/高山拉麵.md "wikilink")（[高山市](../Page/高山市.md "wikilink")）
  - [台灣拉麵](../Page/台灣拉麵.md "wikilink")（[名古屋市](../Page/名古屋市.md "wikilink")）
  - [越共拉麵](../Page/越共拉麵.md "wikilink")（[一宮市](../Page/一宮市.md "wikilink")）

### 近畿（京都附近）

[YakisobaM0829.jpg](https://zh.wikipedia.org/wiki/File:YakisobaM0829.jpg "fig:YakisobaM0829.jpg")

  - [京都拉麵](../Page/京都拉麵.md "wikilink")（[京都市](../Page/京都市.md "wikilink")）
  - [神户拉麵](../Page/神户拉麵.md "wikilink")（[神户市](../Page/神户市.md "wikilink")）
  - 天理拉麵（[天理市](../Page/天理市.md "wikilink")）
  - 和歌山拉麵（[和歌山市](../Page/和歌山市.md "wikilink")）
  - 播州拉麵（[西脇市](../Page/西脇市.md "wikilink")）

### 中四國

  - [岡山拉麵](../Page/岡山拉麵.md "wikilink")（[岡山市](../Page/岡山市.md "wikilink")）
  - 笠岡拉麵（[笠岡市](../Page/笠岡市.md "wikilink")）
  - 福山拉麵（[福山市](../Page/福山市.md "wikilink")）
  - 尾道拉麵（[尾道市](../Page/尾道市.md "wikilink")）
  - [廣島拉麵](../Page/廣島拉麵.md "wikilink")（[廣島市](../Page/廣島市.md "wikilink")）
  - [德島拉麵](../Page/德島拉麵.md "wikilink")（[徳島市](../Page/徳島市.md "wikilink")）
  - 鍋燒拉麵（[須崎市](../Page/須崎市.md "wikilink")）

### [九州](../Page/九州.md "wikilink")

[Hakata_ramen.JPG](https://zh.wikipedia.org/wiki/File:Hakata_ramen.JPG "fig:Hakata_ramen.JPG")

  - [博多拉麵](../Page/博多拉麵.md "wikilink")（[福岡市](../Page/福岡市.md "wikilink")）

<!-- end list -->

  - 長濱拉麵（福岡市）

<!-- end list -->

  - 久留米拉麵（[久留米市](../Page/久留米市.md "wikilink")）：豚骨（豬骨）拉麵的發源地。使用極細的直條麵，再加上比博多拉麵更多的豬油，湯底大都相當濃郁。佐料有生紅薑、白芝麻、蒜等。

<!-- end list -->

  - [熊本拉麵](../Page/熊本拉麵.md "wikilink")（[熊本市](../Page/熊本市.md "wikilink")）

<!-- end list -->

  - [宮崎拉麵](../Page/宮崎拉麵.md "wikilink")（[宮崎市](../Page/宮崎市.md "wikilink")）

<!-- end list -->

  - [鹿兒島拉麵](../Page/鹿兒島拉麵.md "wikilink")（[鹿兒島市](../Page/鹿兒島市.md "wikilink")）\<\!--
    トンコツ+鶏ガラの半濁スープに大量の野菜を使うため、独特の甘味がある。

市外に行くと豚の頭骨のみでスープを取る店もある。(灰汁抜きの為に卵の殻や、甘味用に玉葱が入ることもある)
店舗によっては焼豚(煮豚)を一緒に入れてる場合もあり。(煮た後にごま油等入れた鍋で焼を入れるところもある)
鹹水を使わない白っぽい中太ストレート麺。 目立った具に焦がしネギや木耳がある。 白い急須に入ったお茶、大根の漬物が一緒に出されることが多い。
漬物やにんにく(おろし＆乾燥にんにくの醤油漬け)･紅しょうが使い放題の店もあるので○--\>

## 食用

[High_school_students_eating_Ramen.JPG](https://zh.wikipedia.org/wiki/File:High_school_students_eating_Ramen.JPG "fig:High_school_students_eating_Ramen.JPG")
[Ramen_with_half_sized_fried_rice（ラーメン半チャーハンセット）.JPG](https://zh.wikipedia.org/wiki/File:Ramen_with_half_sized_fried_rice（ラーメン半チャーハンセット）.JPG "fig:Ramen_with_half_sized_fried_rice（ラーメン半チャーハンセット）.JPG")
在日本，经营拉面的有流动拉面摊、专门的“拉面屋”（通常门面较小，可以坐十几个人）、和一些普通饭馆。

食用拉面需趁热吃，在日本吃面时发出大声的唏声也不会有人认为吃相不雅。

在日本，一碗拉面通常是卖几百[日元](../Page/日元.md "wikilink")。拉面店通常兼营[炒飯和](../Page/炒飯.md "wikilink")[锅贴](../Page/锅贴.md "wikilink")，二、三百日元一份几只锅贴。一碗拉面不够的顾客可以花一、二百日元加大面条的量（汤和面码不变），或要一份锅贴。

拉面的经营者通常会提供一些免费的[朝鲜泡菜](../Page/朝鲜泡菜.md "wikilink")、[胡椒等香辛料](../Page/胡椒.md "wikilink")、[蒜酥等](../Page/蒜酥.md "wikilink")，供食客调味。

## 即食麵

在美国2006年，十几[美分就可以买到一包最便宜的](../Page/美分.md "wikilink")[即食麵](../Page/即食麵.md "wikilink")。常见的品牌有[东洋水产出品的Maruchan](../Page/东洋水产.md "wikilink")，[札幌一番](../Page/札幌一番.md "wikilink")（Sapporo
Ichiban）和日清（Nissin）。

在[墨西哥等国家](../Page/墨西哥.md "wikilink")，一些迎合当地口味的[即食麵大受当地人的欢迎](../Page/即食麵.md "wikilink")。

有不少西方人认为即食麵是日本人在20世纪对人类做出的最重要的发明之一。

## 其他

  - 在欧美一些[亚洲人集中的地区](../Page/亚洲.md "wikilink")，正宗的[日本拉面店近年来像雨后春笋一样大量开张](../Page/日本拉面店.md "wikilink")，大有超过传统的[寿司店的趋势](../Page/寿司.md "wikilink")。很多日本餐馆也兼售拉面。

[Shinyokohamaraumenshops1.jpg](https://zh.wikipedia.org/wiki/File:Shinyokohamaraumenshops1.jpg "fig:Shinyokohamaraumenshops1.jpg")

  - 1994年在日本[新横浜落成的](../Page/新横浜.md "wikilink")“新横浜拉面博物馆”，以重现1958年（发明方便拉面的那一年）的[东京而闻名](../Page/东京.md "wikilink")。

<!-- end list -->

  - 香港亦有不少拉麵店及日式食店有售拉麵，但大部分店舖所售的拉麵都是由來自香港的南天製麵麵廠出產。該麵廠由日本人創辦及主理，已有十三年歷史，每日生產超過一萬份拉麵\[3\]。

## 流行文化

  - 1999年：日本[漫画](../Page/漫画.md "wikilink")《[拉面王](../Page/拉面王.md "wikilink")》，是讲述一个上班族藤本浩平如何开创有自己风味的拉面的故事。
  - 2009年：[好萊塢拍攝的電影](../Page/好萊塢.md "wikilink")《[拉麵女孩](../Page/拉麵女孩.md "wikilink")》，就是描述一位美國女孩料理拉麵的酸甜喜樂故事。
  - 2018年：《[愛吃拉麵的小泉同學](../Page/愛吃拉麵的小泉同學.md "wikilink")》，描述把吃拉麵當人生樂趣的小泉同學的故事，先後動畫化與真人化。

[Category:拉麵](../Category/拉麵.md "wikilink")
[Category:日本麵條食品](../Category/日本麵條食品.md "wikilink")

1.  [當松露遇上拉麵](https://bkb.mpweekly.com/cu0003/20170523-33741)
2.  [關於拉麵，有說「豬骨退場，魚介上位」是真的嗎？](https://bkb.mpweekly.com/cu0003/20180317-68931)
3.  [拉麵麵條「香港製造」 愈新鮮愈好？](https://bkb.mpweekly.com/cu0003/20180319-69074)