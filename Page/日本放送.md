**株式会社日本放送**（，英語譯名：）是一家[日本](../Page/日本.md "wikilink")[广播电台](../Page/广播电台.md "wikilink")，為廣播聯播網「[NRN](../Page/NRN.md "wikilink")」的兩個[核心局之一](../Page/核心局.md "wikilink")，識別呼號為**JOLF**、**JOLF-FM**，隸屬[富士產經集團旗下](../Page/富士產經集團.md "wikilink")。

## 概要

[JOLF_1961.JPG](https://zh.wikipedia.org/wiki/File:JOLF_1961.JPG "fig:JOLF_1961.JPG")
[Broadcasting_Antenna_of_Nippon_Broadcasting_System.jpg](https://zh.wikipedia.org/wiki/File:Broadcasting_Antenna_of_Nippon_Broadcasting_System.jpg "fig:Broadcasting_Antenna_of_Nippon_Broadcasting_System.jpg")發射台\]\]

日本放送於1953年12月23日取得籌設許可，1954年4月23日成立，1954年7月15日開播，播出頻率為[AM](../Page/調幅廣播.md "wikilink")1242。1957年11月，日本放送與[株式會社文化放送共同成立](../Page/株式會社文化放送.md "wikilink")[富士電視台](../Page/富士電視台.md "wikilink")。2005年9月1日，日本放送成為富士電視台全資[子公司](../Page/子公司.md "wikilink")。2005年9月12日，日本放送[工會成立](../Page/工會.md "wikilink")。

日本放送於2006年4月1日更名為「日本放送控股株式會社」（），同時分割成立「株式會社日本放送」接手電台之經營。

2015年12月7日下午1時（[JST](../Page/日本標準時間.md "wikilink")），[FM](../Page/調頻廣播.md "wikilink")（暱稱Happy
FM 93）開播，頻率為FM93.0。

日本放送也是日本国内极少数在全天节目开始时播放国歌《[君之代](../Page/君之代.md "wikilink")》的民营广播电视机构之一，目前仅在设备检修结束后播放。

## 外部連結

  - [日本放送企業網站](http://www.jolf.co.jp/)
  - [日本放送日間節目網站](http://www.1242.com/)
  - [日本放送夜間節目系列「All Night Nippon」網站](http://www.allnightnippon.com/)

[category:日本广播电台](../Page/category:日本广播电台.md "wikilink")

[Category:富士產經集團](../Category/富士產經集團.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:1954年開播的電台](../Category/1954年開播的電台.md "wikilink")