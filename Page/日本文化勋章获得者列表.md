**日本文化勋章获得者列表**，收集了获得过[日本](../Page/日本.md "wikilink")[文化勋章的人物名称](../Page/文化勋章.md "wikilink")。截止到2012年11月3日，總共有362人獲得。

## 1937年 - 1944年

  - 1937年 : [長岡半太郎](../Page/長岡半太郎.md "wikilink") -
    [本多光太郎](../Page/本多光太郎.md "wikilink") -
    [木村榮](../Page/木村榮_\(天文學家\).md "wikilink") -
    [佐佐木信綱](../Page/佐佐木信綱.md "wikilink") -
    [幸田露伴](../Page/幸田露伴.md "wikilink") -
    [岡田三郎助](../Page/岡田三郎助.md "wikilink") -
    [竹內栖鳳](../Page/竹內栖鳳.md "wikilink") -
    [藤島武二](../Page/藤島武二.md "wikilink") -
    [橫山大觀](../Page/橫山大觀.md "wikilink")
  - 1940年 : [高木貞治](../Page/高木貞治.md "wikilink") -
    [西田幾多郎](../Page/西田幾多郎.md "wikilink") -
    [川合玉堂](../Page/川合玉堂.md "wikilink") -
    [佐佐木隆興](../Page/佐佐木隆興.md "wikilink")
  - 1943年 : [伊東忠太](../Page/伊東忠太.md "wikilink") -
    [鈴木梅太郎](../Page/鈴木梅太郎.md "wikilink") -
    [朝比奈泰彦](../Page/朝比奈泰彦.md "wikilink") -
    [湯川秀樹](../Page/湯川秀樹.md "wikilink") -
    [德富蘇峰](../Page/德富蘇峰.md "wikilink") -
    [三宅雪嶺](../Page/三宅雪嶺.md "wikilink") -
    [和田英作](../Page/和田英作.md "wikilink")
  - 1944年 : [田中館愛橘](../Page/田中館愛橘.md "wikilink") -
    [岡部金治郎](../Page/岡部金治郎.md "wikilink") -
    [志賀潔](../Page/志賀潔.md "wikilink") -
    [稻田龍吉](../Page/稻田龍吉.md "wikilink") -
    [狩野直喜](../Page/狩野直喜.md "wikilink") -
    [高楠順次郎](../Page/高楠順次郎.md "wikilink")

## 1946年 - 1949年

  - 1946年 : [中田薫](../Page/中田薫.md "wikilink") -
    [宮部金吾](../Page/宮部金吾.md "wikilink") -
    [俵国一](../Page/俵国一.md "wikilink") -
    [仁科芳雄](../Page/仁科芳雄.md "wikilink") -
    [梅若万三郎](../Page/梅若万三郎.md "wikilink") -
    [岩波茂雄](../Page/岩波茂雄.md "wikilink")
  - 1948年 : [木原均](../Page/木原均.md "wikilink") -
    [長谷川如是閑](../Page/長谷川如是閑.md "wikilink") -
    [朝倉文夫](../Page/朝倉文夫.md "wikilink") -
    [上村松園](../Page/上村松園.md "wikilink") -
    [安田靫彦](../Page/安田靫彦.md "wikilink")
  - 1949年 : [尾上菊五郎](../Page/尾上菊五郎_\(6代目\).md "wikilink") -
    [津田左右吉](../Page/津田左右吉.md "wikilink") -
    [鈴木大拙](../Page/鈴木大拙.md "wikilink") -
    [三浦謹之助](../Page/三浦謹之助.md "wikilink") -
    [岡田武松](../Page/岡田武松.md "wikilink") -
    [真島利行](../Page/真島利行.md "wikilink") -
    [谷崎潤一郎](../Page/谷崎潤一郎.md "wikilink") -
    [志賀直哉](../Page/志賀直哉.md "wikilink")

## 1950年 - 1959年

  - 1950年 : [牧野英一](../Page/牧野英一.md "wikilink") -
    [田邊元](../Page/田邊元.md "wikilink") -
    [藤井健次郎](../Page/藤井健次郎.md "wikilink") -
    [三島德七](../Page/三島德七.md "wikilink") -
    [小林古徑](../Page/小林古徑.md "wikilink") -
    [土井晩翠](../Page/土井晩翠.md "wikilink") -
    [正宗白鳥](../Page/正宗白鳥.md "wikilink")
  - 1951年 : [柳田国男](../Page/柳田国男.md "wikilink") -
    [光田健輔](../Page/光田健輔.md "wikilink") -
    [西川正治](../Page/西川正治.md "wikilink") -
    [菊池正士](../Page/菊池正士.md "wikilink") -
    [齋藤茂吉](../Page/齋藤茂吉.md "wikilink") -
    [武者小路實篤](../Page/武者小路實篤.md "wikilink") -
    [中村吉右衛門](../Page/中村吉右衛門_\(初代\).md "wikilink")
  - 1952年 : [梅原龍三郎](../Page/梅原龍三郎.md "wikilink") -
    [熊谷岱藏](../Page/熊谷岱藏.md "wikilink") -
    [佐佐木惣一](../Page/佐佐木惣一.md "wikilink") -
    [辻善之助](../Page/辻善之助.md "wikilink") -
    [朝永振一郎](../Page/朝永振一郎.md "wikilink") -
    [永井荷風](../Page/永井荷風.md "wikilink") -
    [安井曾太郎](../Page/安井曾太郎.md "wikilink")
  - 1953年 : [板谷波山](../Page/板谷波山.md "wikilink") -
    [宇井伯寿](../Page/宇井伯寿.md "wikilink") -
    [香取秀真](../Page/香取秀真.md "wikilink") -
    [喜多六平太](../Page/喜多六平太.md "wikilink") -
    [羽田亨](../Page/羽田亨.md "wikilink") -
    [矢部長克](../Page/矢部長克.md "wikilink")
  - 1954年 : [勝沼精藏](../Page/勝沼精藏.md "wikilink") -
    [鏑木清方](../Page/鏑木清方.md "wikilink") -
    [金田一京助](../Page/金田一京助.md "wikilink") -
    [高浜虚子](../Page/高浜虚子.md "wikilink") -
    [萩原雄祐](../Page/萩原雄祐.md "wikilink")
  - 1955年 : [大谷竹次郎](../Page/大谷竹次郎.md "wikilink") -
    [稀音家淨觀](../Page/稀音家淨觀.md "wikilink") -
    [平沼亮三](../Page/平沼亮三.md "wikilink") -
    [二木謙三](../Page/二木謙三.md "wikilink") -
    [前田青邨](../Page/前田青邨.md "wikilink") -
    [增本量](../Page/增本量.md "wikilink") -
    [和辻哲郎](../Page/和辻哲郎.md "wikilink")
  - 1956年 : [安藤廣太郎](../Page/安藤廣太郎.md "wikilink") -
    [坂本繁二郎](../Page/坂本繁二郎.md "wikilink") -
    [新村出](../Page/新村出.md "wikilink") -
    [古畑種基](../Page/古畑種基.md "wikilink") -
    [村上武次郎](../Page/村上武次郎.md "wikilink") -
    [八木秀次](../Page/八木秀次.md "wikilink") -
    [山田耕筰](../Page/山田耕筰.md "wikilink")
  - 1957年 : [牧野富太郎](../Page/牧野富太郎.md "wikilink") -
    [緒方知三郎](../Page/緒方知三郎.md "wikilink") -
    [久保田万太郎](../Page/久保田万太郎.md "wikilink") -
    [小平邦彦](../Page/小平邦彦.md "wikilink") -
    [西山翠嶂](../Page/西山翠嶂.md "wikilink") -
    [山田孝雄](../Page/山田孝雄.md "wikilink") -
    [吉住小三郎](../Page/吉住小三郎.md "wikilink")
  - 1958年 : [北村西望](../Page/北村西望.md "wikilink") -
    [近藤平三郎](../Page/近藤平三郎.md "wikilink") -
    [野副鐵男](../Page/野副鐵男.md "wikilink") -
    [-{松}-林桂月](../Page/松林桂月.md "wikilink")
  - 1959年 : [川端龍子](../Page/川端龍子.md "wikilink") -
    [小泉信三](../Page/小泉信三.md "wikilink") -
    [丹羽保次郎](../Page/丹羽保次郎.md "wikilink") -
    [里見弴](../Page/里見弴.md "wikilink") -
    [吉田富三](../Page/吉田富三.md "wikilink")

## 1960年 - 1969年

  - 1960年 : [岡潔](../Page/岡潔.md "wikilink") -
    [佐藤春夫](../Page/佐藤春夫.md "wikilink") -
    [田中耕太郎](../Page/田中耕太郎.md "wikilink") -
    [吉川英治](../Page/吉川英治.md "wikilink")
  - 1961年 : [川端康成](../Page/川端康成.md "wikilink") -
    [鈴木虎雄](../Page/鈴木虎雄.md "wikilink") -
    [富本憲吉](../Page/富本憲吉.md "wikilink") -
    [堂本印象](../Page/堂本印象.md "wikilink") -
    [福田平八郎](../Page/福田平八郎.md "wikilink") -
    [水島三一郎](../Page/水島三一郎.md "wikilink")
  - 1962年 : [梅澤濱夫](../Page/梅澤濱夫.md "wikilink") -
    [奥村土牛](../Page/奥村土牛.md "wikilink") -
    [桑田義備](../Page/桑田義備.md "wikilink") -
    [中村岳陵](../Page/中村岳陵.md "wikilink") -
    [平櫛田中](../Page/平櫛田中.md "wikilink")
  - 1963年 : [久野寧](../Page/久野寧.md "wikilink") -
    [古賀逸策](../Page/古賀逸策.md "wikilink")
  - 1964年 : [茅誠司](../Page/茅誠司.md "wikilink") -
    [大佛次郎](../Page/大佛次郎.md "wikilink") -
    [藪田貞治郎](../Page/藪田貞治郎.md "wikilink") -
    [吉田五十八](../Page/吉田五十八.md "wikilink") -
    [我妻榮](../Page/我妻榮.md "wikilink")
  - 1965年 : [赤堀四郎](../Page/赤堀四郎.md "wikilink") -
    [小絲源太郎](../Page/小絲源太郎.md "wikilink") -
    [諸橋轍次](../Page/諸橋轍次.md "wikilink") -
    [山口蓬春](../Page/山口蓬春.md "wikilink") -
    [山本有三](../Page/山本有三.md "wikilink")
  - 1966年 : [井伏鱒二](../Page/井伏鱒二.md "wikilink") -
    [德岡神泉](../Page/德岡神泉.md "wikilink") -
    [仁田勇](../Page/仁田勇.md "wikilink")
  - 1967年 : [小林秀雄](../Page/小林秀雄.md "wikilink") -
    [坂口謹一郎](../Page/坂口謹一郎.md "wikilink") -
    [林武](../Page/林武.md "wikilink") -
    [村野藤吾](../Page/村野藤吾.md "wikilink") -
    [山縣昌夫](../Page/山縣昌夫.md "wikilink")
  - 1968年 : [堅山南風](../Page/堅山南風.md "wikilink") -
    [黑川利雄](../Page/黑川利雄.md "wikilink") -
    [鈴木雅次](../Page/鈴木雅次.md "wikilink") -
    [濱田庄司](../Page/濱田庄司.md "wikilink")
  - 1969年 : [獅子文六](../Page/獅子文六.md "wikilink") -
    [落合英二](../Page/落合英二_\(藥學\).md "wikilink") -
    [正田建次郎](../Page/正田建次郎.md "wikilink") -
    [東山魁夷](../Page/東山魁夷.md "wikilink")

## 1970年 - 1979年

  - 1970年 : [冲中重雄](../Page/冲中重雄.md "wikilink") -
    [棟方-{志}-功](../Page/棟方志功.md "wikilink")
  - 1971年 : [赤木正雄](../Page/赤木正雄.md "wikilink") -
    [荒川豐藏](../Page/荒川豐藏.md "wikilink") -
    [野上弥生子](../Page/野上弥生子.md "wikilink") -
    [安井琢磨](../Page/安井琢磨.md "wikilink")
  - 1972年 : [内田祥三](../Page/内田祥三.md "wikilink") -
    [小野清一郎](../Page/小野清一郎.md "wikilink") -
    [岡鹿之助](../Page/岡鹿之助.md "wikilink") -
    [早石修](../Page/早石修.md "wikilink")
  - 1973年 : [石原謙](../Page/石原謙.md "wikilink") -
    [勝木保次](../Page/勝木保次.md "wikilink") -
    [久保亮五](../Page/久保亮五.md "wikilink") -
    [瀨藤象二](../Page/瀨藤象二.md "wikilink") -
    [谷口吉郎](../Page/谷口吉郎.md "wikilink")
  - 1974年 : [石坂公成](../Page/石坂公成.md "wikilink") -
    [江崎玲於奈](../Page/江崎玲於奈.md "wikilink") -
    [杉山寧](../Page/杉山寧.md "wikilink") -
    [永田武](../Page/永田武.md "wikilink") -
    [橋本明治](../Page/橋本明治.md "wikilink")
  - 1975年 : [江橋節郎](../Page/江橋節郎.md "wikilink") -
    [小山敬三](../Page/小山敬三.md "wikilink") -
    [田崎廣助](../Page/田崎廣助.md "wikilink") -
    [中川一政](../Page/中川一政.md "wikilink") -
    [廣中平祐](../Page/廣中平祐.md "wikilink")
  - 1976年 : [井上靖](../Page/井上靖.md "wikilink") -
    [小野竹喬](../Page/小野竹喬.md "wikilink") -
    [木村資生](../Page/木村資生.md "wikilink") -
    [松田權六](../Page/松田權六.md "wikilink") -
    [森嶋通夫](../Page/森嶋通夫.md "wikilink")
  - 1977年 : [櫻田一郎](../Page/櫻田一郎.md "wikilink") -
    [田宮博](../Page/田宮博.md "wikilink") -
    [中村元](../Page/中村元.md "wikilink") -
    [丹羽文雄](../Page/丹羽文雄.md "wikilink") -
    [山本丘人](../Page/山本丘人.md "wikilink")
  - 1978年 : [尾崎一雄](../Page/尾崎一雄.md "wikilink") -
    [楠部弥弌](../Page/楠部弥弌.md "wikilink") -
    [杉村隆](../Page/杉村隆.md "wikilink") -
    [田中美知太郎](../Page/田中美知太郎.md "wikilink") -
    [南部陽一郎](../Page/南部陽一郎.md "wikilink")
  - 1979年 : [今西錦司](../Page/今西錦司.md "wikilink") -
    [中村歌右衛門](../Page/中村歌右衛門.md "wikilink") -
    [澤田政廣](../Page/澤田政廣.md "wikilink") -
    [高橋誠一郎](../Page/高橋誠一郎.md "wikilink") -
    [堀口大学](../Page/堀口大学.md "wikilink")

## 1980年 - 1989年

  - 1980年 : [小倉遊龜](../Page/小倉遊龜.md "wikilink") -
    [小谷正雄](../Page/小谷正雄.md "wikilink") -
    [丹下健三](../Page/丹下健三.md "wikilink") -
    [東畑精一](../Page/東畑精一.md "wikilink") -
    [中村勘三郎](../Page/中村勘三郎.md "wikilink")
  - 1981年 : [高柳健次郎](../Page/高柳健次郎.md "wikilink") -
    [永井龍男](../Page/永井龍男.md "wikilink") -
    [松本白鸚](../Page/松本白鸚.md "wikilink") -
    [山口華楊](../Page/山口華楊.md "wikilink") -
    [横田喜三郎](../Page/横田喜三郎.md "wikilink") -
    [福井謙一](../Page/福井謙一.md "wikilink")
  - 1982年 : [坂本太郎](../Page/坂本太郎.md "wikilink") -
    [高山辰雄](../Page/高山辰雄.md "wikilink") -
    [津田恭介](../Page/津田恭介.md "wikilink") -
    [藤間勘十郎](../Page/藤間勘十郎.md "wikilink") -
    [吉識雅夫](../Page/吉識雅夫.md "wikilink")
  - 1983年 : [山本健吉](../Page/山本健吉.md "wikilink") -
    [牛島憲之](../Page/牛島憲之.md "wikilink") -
    [小磯良平](../Page/小磯良平.md "wikilink") -
    [服部四郎](../Page/服部四郎.md "wikilink") -
    [武藤清](../Page/武藤清.md "wikilink")
  - 1984年 : [上村松篁](../Page/上村松篁.md "wikilink") -
    [奥田元宋](../Page/奥田元宋.md "wikilink") -
    [貝塚茂樹](../Page/貝塚茂樹.md "wikilink") -
    [高橋信次](../Page/高橋信次.md "wikilink") -
    [利根川進](../Page/利根川進.md "wikilink")
  - 1985年 : [圓地文子](../Page/圓地文子.md "wikilink") -
    [黑澤明](../Page/黑澤明.md "wikilink") -
    [相良守峯](../Page/相良守峯.md "wikilink") -
    [西川寧](../Page/西川寧.md "wikilink") -
    [和達清夫](../Page/和達清夫.md "wikilink")
  - 1986年 : [荻須高德](../Page/荻須高德.md "wikilink") -
    [岡義武](../Page/岡義武.md "wikilink") -
    [土屋文明](../Page/土屋文明.md "wikilink") -
    [名取礼二](../Page/名取礼二.md "wikilink") -
    [林忠四郎](../Page/林忠四郎.md "wikilink")
  - 1987年 : [池田遙邨](../Page/池田遙邨.md "wikilink") -
    [岡田善雄](../Page/岡田善雄.md "wikilink") -
    [草野心平](../Page/草野心平.md "wikilink") -
    [桑原武夫](../Page/桑原武夫.md "wikilink") -
    [尾上松綠](../Page/尾上松綠.md "wikilink")
  - 1988年 : [今井功](../Page/今井功.md "wikilink") -
    [圓鍔勝三](../Page/圓鍔勝三.md "wikilink") -
    [河盛好蔵](../Page/河盛好蔵.md "wikilink") -
    [末永雅雄](../Page/末永雅雄.md "wikilink") -
    [西塚泰美](../Page/西塚泰美.md "wikilink")
  - 1989年 : [片岡球子](../Page/片岡球子.md "wikilink") -
    [鈴木竹雄](../Page/鈴木竹雄.md "wikilink") -
    [富永直樹](../Page/富永直樹.md "wikilink") -
    [西澤潤一](../Page/西澤潤一.md "wikilink") -
    [吉井淳二](../Page/吉井淳二.md "wikilink")

## 1990年 - 1999年

  - 1990年 : [石井良助](../Page/石井良助.md "wikilink") -
    [市古貞次](../Page/市古貞次.md "wikilink") -
    [井上八千代](../Page/井上八千代.md "wikilink") -
    [金子鷗亭](../Page/金子鷗亭.md "wikilink") -
    [長倉三郎](../Page/長倉三郎.md "wikilink")
  - 1991年 : [豬瀨博](../Page/豬瀨博.md "wikilink") -
    [江上波夫](../Page/江上波夫.md "wikilink") -
    [蓮田修吾郎](../Page/蓮田修吾郎.md "wikilink") -
    [福澤一郎](../Page/福澤一郎.md "wikilink") -
    [森繁久彌](../Page/森繁久彌.md "wikilink")
  - 1992年 : [青山杉雨](../Page/青山杉雨.md "wikilink") -
    [井深大](../Page/井深大.md "wikilink") -
    [大塚久雄](../Page/大塚久雄.md "wikilink") -
    [佐藤太清](../Page/佐藤太清.md "wikilink") -
    [森野米三](../Page/森野米三.md "wikilink")
  - 1993年 : [大隅健一郎](../Page/大隅健一郎.md "wikilink") -
    [小田稔](../Page/小田稔.md "wikilink") -
    [帖佐美行](../Page/帖佐美行.md "wikilink") -
    [司馬遼太郎](../Page/司馬遼太郎.md "wikilink") -
    [森田茂](../Page/森田茂.md "wikilink")
  - 1994年 : [朝比奈隆](../Page/朝比奈隆.md "wikilink") -
    [岩橋英遠](../Page/岩橋英遠.md "wikilink") -
    [梅棹忠夫](../Page/梅棹忠夫.md "wikilink") -
    [島秀雄](../Page/島秀雄.md "wikilink") -
    [滿田久輝](../Page/滿田久輝.md "wikilink")
  - 1995年 : [遠藤周作](../Page/遠藤周作.md "wikilink") -
    [佐治賢使](../Page/佐治賢使.md "wikilink") -
    [團藤重光](../Page/團藤重光.md "wikilink") -
    [花房秀三郎](../Page/花房秀三郎.md "wikilink") -
    [增田四郎](../Page/增田四郎.md "wikilink")
  - 1996年 : [淺藏五十吉](../Page/淺藏五十吉.md "wikilink") -
    [伊藤清永](../Page/伊藤清永.md "wikilink") -
    [伊藤正男](../Page/伊藤正男.md "wikilink") -
    [竹内理三](../Page/竹内理三.md "wikilink") -
    [森英惠](../Page/森英惠.md "wikilink")
  - 1997年 : [千宗室](../Page/千宗室.md "wikilink") -
    [宇澤弘文](../Page/宇澤弘文.md "wikilink") -
    [小柴昌俊](../Page/小柴昌俊.md "wikilink") -
    [高橋節郎](../Page/高橋節郎.md "wikilink") -
    [向山光昭](../Page/向山光昭.md "wikilink")
  - 1998年 : [芦原義信](../Page/芦原義信.md "wikilink") -
    [岸本忠三](../Page/岸本忠三.md "wikilink") -
    [平山郁夫](../Page/平山郁夫.md "wikilink") -
    [村上三島](../Page/村上三島.md "wikilink") -
    [山本達郎](../Page/山本達郎.md "wikilink")
  - 1999年 : [阿川弘之](../Page/阿川弘之.md "wikilink") -
    [秋野不矩](../Page/秋野不矩.md "wikilink") -
    [伊藤正己](../Page/伊藤正己.md "wikilink") -
    [梅原猛](../Page/梅原猛.md "wikilink") -
    [田村三郎](../Page/田村三郎.md "wikilink")

## 2000年 - 2009年

  - 2000年 : [石川忠雄](../Page/石川忠雄.md "wikilink") -
    [大久保婦久子](../Page/大久保婦久子.md "wikilink") -
    [白川英樹](../Page/白川英樹.md "wikilink") -
    [杉岡華邨](../Page/杉岡華邨.md "wikilink") -
    [野依良治](../Page/野依良治.md "wikilink") -
    [山田五十鈴](../Page/山田五十鈴.md "wikilink")
  - 2001年 : [井口洋夫](../Page/井口洋夫.md "wikilink") -
    [豐島久真男](../Page/豐島久真男.md "wikilink") -
    [中根千枝](../Page/中根千枝.md "wikilink") -
    [守屋多多志](../Page/守屋多多志.md "wikilink") -
    [淀井敏夫](../Page/淀井敏夫.md "wikilink")
  - 2002年 : [小宮隆太郎](../Page/小宮隆太郎.md "wikilink") -
    [近藤次郎](../Page/近藤次郎.md "wikilink") -
    [新藤兼人](../Page/新藤兼人.md "wikilink") -
    [杉本苑子](../Page/杉本苑子.md "wikilink") -
    [田中耕一](../Page/田中耕一.md "wikilink") -
    [藤田喬平](../Page/藤田喬平.md "wikilink")
  - 2003年 : [大岡信](../Page/大岡信.md "wikilink") -
    [緒方貞子](../Page/緒方貞子.md "wikilink") -
    [加山又造](../Page/加山又造.md "wikilink") -
    [西島和彥](../Page/西島和彥.md "wikilink") -
    [森亘](../Page/森亘.md "wikilink")
  - 2004年 : [白川静](../Page/白川静.md "wikilink") - [中村雀右衛門
    (4代目)](../Page/中村雀右衛門_\(4代目\).md "wikilink") -
    [戶塚洋二](../Page/戶塚洋二.md "wikilink") -
    [福王寺法林](../Page/福王寺法林.md "wikilink") -
    [小林斗盦](../Page/小林斗盦.md "wikilink")
  - 2005年 : [青木龍山](../Page/青木龍山.md "wikilink") -
    [齋藤真](../Page/齋藤真.md "wikilink") -
    [澤田敏男](../Page/澤田敏男.md "wikilink") -
    [日野原重明](../Page/日野原重明.md "wikilink") -
    [森光子](../Page/森光子.md "wikilink")
  - 2006年 : [荒田吉明](../Page/荒田吉明.md "wikilink") -
    [大山忠作](../Page/大山忠作.md "wikilink") -
    [篠原三代平](../Page/篠原三代平.md "wikilink") -
    [瀨戶內寂聽](../Page/瀨戶內寂聽.md "wikilink") -
    [吉田秀和](../Page/吉田秀和.md "wikilink")
  - 2007年 : [岡田節人](../Page/岡田節人.md "wikilink") -
    [茂山千作](../Page/茂山千作.md "wikilink") -
    [中西香爾](../Page/中西香爾.md "wikilink") -
    [中村晋也](../Page/中村晋也.md "wikilink") -
    [三個月章](../Page/三個月章.md "wikilink")
  - 2008年 : [小林誠](../Page/小林誠_\(物理學家\).md "wikilink") -
    [益川敏英](../Page/益川敏英.md "wikilink") -
    [下村脩](../Page/下村脩.md "wikilink") -
    [伊藤清](../Page/伊藤清.md "wikilink") -
    [小澤征爾](../Page/小澤征爾.md "wikilink") -
    [田邊聖子](../Page/田邊聖子.md "wikilink") -
    [唐納德·基恩](../Page/唐納德·基恩.md "wikilink") -
    [古橋廣之進](../Page/古橋廣之進.md "wikilink")
  - 2009年 : [飯島澄男](../Page/飯島澄男.md "wikilink") -
    [桂米朝](../Page/桂米朝.md "wikilink") -
    [坂田藤十郎](../Page/坂田藤十郎.md "wikilink") -
    [速水融](../Page/速水融.md "wikilink") -
    [日沼賴夫](../Page/日沼賴夫.md "wikilink")

## 2010年 - 2019年

  - 2010年 ：
    [有馬朗人](../Page/有馬朗人.md "wikilink")、[安藤忠雄](../Page/安藤忠雄.md "wikilink")、[鈴木章](../Page/鈴木章.md "wikilink")、[蜷川幸雄](../Page/蜷川幸雄.md "wikilink")、[根岸英一](../Page/根岸英一.md "wikilink")、[三宅一生](../Page/三宅一生.md "wikilink")、[脇田晴子](../Page/脇田晴子.md "wikilink")
  - 2011年 ：
    [赤崎勇](../Page/赤崎勇.md "wikilink")、[大樋年朗](../Page/大樋年朗.md "wikilink")、[丸谷才一](../Page/丸谷才一.md "wikilink")、[三谷太一郎](../Page/三谷太一郎.md "wikilink")、[柳田充弘](../Page/柳田充弘.md "wikilink")
  - 2012年 ：
    [小田滋](../Page/小田滋.md "wikilink")、[高階秀爾](../Page/高階秀爾.md "wikilink")、[松尾敏男](../Page/松尾敏男.md "wikilink")、[山田康之](../Page/山田康之.md "wikilink")、[山田洋次](../Page/山田洋次.md "wikilink")、[山中伸弥](../Page/山中伸弥.md "wikilink")
  - 2013年 ：
    [岩崎俊一](../Page/岩崎俊一.md "wikilink")、[高倉健](../Page/高倉健.md "wikilink")、[高木聖鶴](../Page/高木聖鶴.md "wikilink")、[中西進](../Page/中西進.md "wikilink")、[本庶佑](../Page/本庶佑.md "wikilink")
  - 2014年 ：
    [天野浩](../Page/天野浩.md "wikilink")、[國武豐喜](../Page/國武豐喜.md "wikilink")、[河野多惠子](../Page/河野多惠子.md "wikilink")、[竹本住大夫](../Page/竹本住大夫.md "wikilink")、[中村修二](../Page/中村修二.md "wikilink")、[根岸隆](../Page/根岸隆.md "wikilink")、[野見山曉治](../Page/野見山曉治.md "wikilink")
  - 2015年 ：
    [大村智](../Page/大村智.md "wikilink")、[梶田隆章](../Page/梶田隆章.md "wikilink")、[塩野宏](../Page/塩野宏.md "wikilink")、[志村福美](../Page/志村福美.md "wikilink")、[末松安晴](../Page/末松安晴.md "wikilink")、[仲代達矢](../Page/仲代達矢.md "wikilink")、[中西重忠](../Page/中西重忠.md "wikilink")
  - 2016年 ：
    [大隅良典](../Page/大隅良典.md "wikilink")、[草間彌生](../Page/草間彌生.md "wikilink")、[平岩弓枝](../Page/平岩弓枝.md "wikilink")、[船村徹](../Page/船村徹.md "wikilink")、[中野三敏](../Page/中野三敏.md "wikilink")、[太田朋子](../Page/太田朋子.md "wikilink")
  - 2017年 ：
    [奥谷博](../Page/奥谷博.md "wikilink")、[芝祐靖](../Page/芝祐靖.md "wikilink")、[斯波義信](../Page/斯波義信.md "wikilink")、[藤嶋昭](../Page/藤嶋昭.md "wikilink")、[松原謙一](../Page/松原謙一.md "wikilink")
  - 2018年 ：
    [一柳慧](../Page/一柳慧.md "wikilink")、[今井政之](../Page/今井政之.md "wikilink")、[金子宏](../Page/金子宏.md "wikilink")、[長尾真](../Page/長尾真.md "wikilink")、[山崎正和](../Page/山崎正和.md "wikilink")

[category:日本人列表](../Page/category:日本人列表.md "wikilink")

[+](../Category/文化勳章獲得者.md "wikilink")