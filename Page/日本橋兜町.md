**日本橋兜町**（）是[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[中央區的一個](../Page/中央區_\(東京\).md "wikilink")[地方町名](../Page/町.md "wikilink")。[太平洋戰爭前是東京市日本橋區的一個地區](../Page/太平洋戰爭.md "wikilink")。

## 概要

[經濟](../Page/經濟.md "wikilink")、企業性質強烈、為[東京證券交易所的所在地](../Page/東京證券交易所.md "wikilink")，亦有眾多[證券公司設立於該地](../Page/證券公司.md "wikilink")，因此兜町成為[日本](../Page/日本.md "wikilink")[證券市場的代名詞](../Page/證券市場.md "wikilink")，有如[華爾街代表](../Page/華爾街.md "wikilink")[紐約證券交易所](../Page/紐約證券交易所.md "wikilink")。與[倫敦](../Page/倫敦.md "wikilink")[金融城](../Page/倫敦市.md "wikilink")、[紐約](../Page/紐約.md "wikilink")[華爾街](../Page/華爾街.md "wikilink")、[香港](../Page/香港.md "wikilink")[中環為世界重要的金融中心](../Page/中環.md "wikilink")。\[1\]

## 地理

位於日本橋地域南部。

  - [河川](../Page/川.md "wikilink")、[橋](../Page/橋.md "wikilink")

<!-- end list -->

  - [日本橋川](../Page/日本橋川.md "wikilink")
      - 鎧橋

## 歷史

兜町附近原為沼地。[江戶時代](../Page/江戶時代.md "wikilink")，[江戶城築城時填埋造陸](../Page/江戶城.md "wikilink")，周邊陸續興建大名屋敷。當時此地稱為「兜橋」、「兜の渡し」。

[明治時代](../Page/明治.md "wikilink")，作為[明治維新的恩賞](../Page/明治維新.md "wikilink")，[三井家接受了兜町周邊的土地](../Page/三井家.md "wikilink")，「兜町」之名因而得來。1871年，以[澀澤榮一為中心的](../Page/澀澤榮一.md "wikilink")本店成立。1878年，東京證券交易所的前身東京株式取引所設立，此地急速發展成商業區。1923年[關東大震災](../Page/關東大震災.md "wikilink")，部份地方發生火災燒毀，震災後轉變為現代的樣貌。

### 地名由來

來自於此地的兜山（兜神社）。傳聞[源義家前往](../Page/源義家.md "wikilink")[奧州時在此地遇上風暴](../Page/奧州.md "wikilink")，因而將鎧甲丟入河中祈求龍神庇佑，之後歸返途中將兜埋在此地（兜山）並建立神社祭祀（兜神社）\[2\]。

### 町名變遷

| 實施後   | 實施年月日     | 實施前（各町全域）                  |
| ----- | --------- | -------------------------- |
| 日本橋兜町 | 1982年1月1日 | 日本橋兜町一丁目、日本橋兜町二丁目、日本橋兜町三丁目 |

## 地域

  - [公園](../Page/公園.md "wikilink")

<!-- end list -->

  - 楓川新場橋公園
  - 坂本町公園

<!-- end list -->

  - [教育](../Page/教育.md "wikilink")

<!-- end list -->

  - －阪本幼稚園位於同處。

  - 中央區立阪本幼稚園－現在休園中

<!-- end list -->

  - [機關](../Page/機關.md "wikilink")

<!-- end list -->

  -
  - [日本橋消防署](../Page/東京消防廳第一消防方面本部#日本橋消防署.md "wikilink")

<!-- end list -->

  - [企業](../Page/企業.md "wikilink")

<!-- end list -->

  - [東京證券交易所](../Page/東京證券交易所.md "wikilink")（東証ARROWS）

  - 兜町本社－登記上的本店在[千代田區](../Page/千代田區.md "wikilink")[丸之內的丸之內本店](../Page/丸之內.md "wikilink")。

## 觀光

  - 景點、古蹟

<!-- end list -->

  - 銀行發祥地－設有紀念碑。

## 交通

  - [鐵路](../Page/鐵路.md "wikilink")

<!-- end list -->

  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")[茅場町站](../Page/茅場町站.md "wikilink")（<span style="color:#95989F">**○**</span>[日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")、<span style="color:#0080FF">**○**</span>[東西線](../Page/東京地下鐵東西線.md "wikilink")）-
    設有出入口。（所在地：[日本橋茅場町](../Page/日本橋茅場町.md "wikilink")）

<!-- end list -->

  - [巴士](../Page/巴士.md "wikilink")

<!-- end list -->

  - [都營巴士](../Page/都營巴士.md "wikilink")[錦11](../Page/都營巴士臨海支所#錦11系統.md "wikilink")
    茅場町（[錦糸町站前方向](../Page/錦糸町站.md "wikilink")）－[築地站前方向設於日本橋茅場町](../Page/築地站.md "wikilink")。

<!-- end list -->

  - [道路](../Page/道路.md "wikilink")

<!-- end list -->

  - （）

<!-- end list -->

  - [首都高速道路](../Page/首都高速道路.md "wikilink")、出入口

<!-- end list -->

  - [首都高速都心環狀線](../Page/首都高速都心環狀線.md "wikilink")
  - [首都高速6號向島線](../Page/首都高速6號向島線.md "wikilink")

## 備註

## 相關條目

  - －[大阪的金融](../Page/大阪市.md "wikilink")、證券中心

[Category:東京都區部地區](../Category/東京都區部地區.md "wikilink")
[Category:東京都中央區町名](../Category/東京都中央區町名.md "wikilink")
[Category:日本橋 (東京都中央區)](../Category/日本橋_\(東京都中央區\).md "wikilink")
[Category:日本股市](../Category/日本股市.md "wikilink")

1.
2.  [日本橋茅場町・日本橋兜町地区　中央区ホームページ](http://www.city.chuo.lg.jp/syokai/tyomeiyurai/kayabacho/index.html)