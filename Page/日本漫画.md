[Japan_Bookstore.jpg](https://zh.wikipedia.org/wiki/File:Japan_Bookstore.jpg "fig:Japan_Bookstore.jpg")漫畫出租店\]\]
**日本漫画**（）指[日本於](../Page/日本.md "wikilink")18世紀以後從12世紀戯画（）延伸發展出的視覺藝術\[1\]\[2\]。當代日本[漫畫包括故事连环画和四幅一组的组画](../Page/漫畫.md "wikilink")，它是画面组合作品的总称，也是指刊载这类作品的[杂志和](../Page/漫畫雜誌.md "wikilink")[单行本](../Page/单行本.md "wikilink")。快速且高度發展的日本漫畫已經成為世界漫畫當中具有獨特風格以及龐大影響力的流派。
[Logo_serie_manga.png](https://zh.wikipedia.org/wiki/File:Logo_serie_manga.png "fig:Logo_serie_manga.png")

## 概要

日本的漫畫讀者包括了所有的年齡層\[3\]，因此日本漫畫的題材非常廣泛。1950年代以後，漫畫逐漸成為日本出版業的主要部分\[4\]。2006年漫畫市值更達到4810亿[日圓](../Page/日圓.md "wikilink")\[5\]。除了一小部分漫畫是彩色的\[6\]之外，大多數日本漫畫採用黑白印刷\[7\]。在日本，漫畫一般在[漫畫雜誌上連載](../Page/漫畫雜誌.md "wikilink")，毎期漫畫雜誌可包含多個漫畫系列，但毎個系列只刊登一個章節留待下期繼續\[8\]\[9\]。如果某個漫畫系列已連載一段時期並且受到讀者歡迎，那個系列通常會以[單行本的形式出版](../Page/單行本.md "wikilink")\[10\]\[11\]。在單行本中可收集多個漫畫章節（與漫畫雜誌不同，一本單行本仅限於一個漫畫系列）。日本擁有大量的漫畫讀者。為了滿足讀者的需求，日本設有專門的漫畫咖啡廳（），在那里讀者可以一邊喝咖啡一邊看漫畫，許多人還在漫畫咖啡廳過夜。日本漫畫向海外輸出始於1970年代末。近年來，日本漫畫業向全球擴張；在世界范围内，日本漫畫也越來越普及\[12\]\[13\]。許多外國出版商在取得授權後翻譯和發行外文版日本漫畫。

## 发展历史

[Wikipe-tan_sailor_fuku.png](https://zh.wikipedia.org/wiki/File:Wikipe-tan_sailor_fuku.png "fig:Wikipe-tan_sailor_fuku.png")體現了日本漫畫風格\]\]
[日本可算是](../Page/日本.md "wikilink")[漫画王国](../Page/漫画.md "wikilink")。日本[漫画业从](../Page/漫画.md "wikilink")12世纪就开始发展。[平安時代的](../Page/平安時代.md "wikilink")《[鳥獸戲畫](../Page/鳥獸戲畫.md "wikilink")》（）被認為是日本最古老的漫畫作品\[14\]\[15\]。

1862年，漫畫雜誌《Japan
punch》（）在[橫濱外國人居留地發行](../Page/橫濱.md "wikilink")。1877年代表[明治時代的漫畫雜誌](../Page/明治時代.md "wikilink")《團團珍聞》創刊。日本漫畫家[北澤樂天於](../Page/北澤樂天.md "wikilink")1905年創立《》，對日本諷刺漫畫的發展貢献很大。樂天從1928年開始在《時事漫畫》連載的《》是日本最初的以少女作為主人公的連載漫畫，是[少女漫畫的先驅作品](../Page/少女漫畫.md "wikilink")。1915年，日本漫畫家[岡本一平創立漫畫家團體東京漫畫會](../Page/岡本一平.md "wikilink")（也就是以後的[日本漫畫會](../Page/日本漫畫會.md "wikilink")）。[第二次世界大戰時期由於日本参戰](../Page/第二次世界大戰.md "wikilink")，加上情報局的法規和用紙不足等緣故，日本的漫畫產業陷於衰退狀態。

[第二次世界大战后](../Page/第二次世界大战.md "wikilink")，日本漫画界再次恢复了生气。1954年，月刊少女漫畫雜誌《[Nakayoshi](../Page/Nakayoshi.md "wikilink")》創刊。1959年，最初的週刊漫畫雜誌《[週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")》和《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》創刊。1968年《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》創刊。戰後初期影響了現代日本漫畫歷史的代表作品是[手塚治虫的](../Page/手塚治虫.md "wikilink")《[鐵臂阿童木](../Page/鐵臂阿童木.md "wikilink")》。1950年代以後越來越多日本漫畫家受到手塚治虫作品的啟發。1960年代，[石森章太郎](../Page/石森章太郎.md "wikilink")、[赤塚不二夫和](../Page/赤塚不二夫.md "wikilink")[藤子不二雄的作品大受歡迎](../Page/藤子不二雄.md "wikilink")。随着电视突飞猛进的发展，颇受欢迎的漫画开始被搬上银幕，日本开始了[动画时代](../Page/动画.md "wikilink")。1990年代後，漫畫的類型進一步擴大，漫畫雜誌數量迅速增長，網絡漫畫等文化也應運而生。

有觀点認為日本漫畫發展始於戰後[美軍佔領時期](../Page/同盟國軍事佔領日本.md "wikilink")，這種觀点強調日本漫畫受[美國文化的影響很大](../Page/美國.md "wikilink")；其中包括該時期[美軍帶到日本的](../Page/美軍.md "wikilink")[美國漫畫及](../Page/美國漫畫.md "wikilink")[電視](../Page/電視.md "wikilink")、[電影和](../Page/電影.md "wikilink")[卡通片](../Page/卡通片.md "wikilink")（特别是[迪士尼](../Page/迪士尼.md "wikilink")）\[16\]\[17\]。但是一些作家強調日本文化的延續和[美學傳統是日本漫畫歷史的關鍵](../Page/美學.md "wikilink")，他們包括Frederik
L. Schodt\[18\]\[19\]、Kinko Ito\[20\]和Adam L. Kern\[21\]\[22\]。

## 出版和表現形式

[Manga_reading_direction.svg](https://zh.wikipedia.org/wiki/File:Manga_reading_direction.svg "fig:Manga_reading_direction.svg")
日本漫畫基本由[格](../Page/格子.md "wikilink")、登場人物、[背景](../Page/背景.md "wikilink")、[文字氣球](../Page/文字氣球.md "wikilink")、[音喩](../Page/音喩.md "wikilink")、[漫符](../Page/漫符.md "wikilink")、[台詞和其他技法構成](../Page/台詞.md "wikilink")。日本漫畫的閱讀順序（格的排列）一般是從上往下，從右至左（同一行中）；在同一格中閱讀文字氣球中的文字也是從上往下，從右至左（見右圖）。這和[傳統日語文字的排列順序相一致](../Page/東亞文字排列方向.md "wikilink")。雖然多數出版商在出版日本漫畫外文版時保持了這種風格，仍然有一些外文出版商把漫畫的閱讀方向改為從左至右，這樣的作法可能會違背原作者的意圖，有時也會造成完整的圖片（如覆蓋兩頁的格）斷開的情况。

### 技法

  - 登場人物的言談和思考内容以文字形式記錄在文字氣球中。
  - [擬聲詞以手寫字體形式表現](../Page/擬聲詞.md "wikilink")。
  - 漫符用以體現人物的心理和動作（比如在人物的額頭、後腦邊畫上汗水滴表示角色的惊訝、憤慨、無奈或困惑；讓人物的額頭、拳頭浮現青筋代表憤怒等）。

## 分类

### 按讀者對象分

1.  幼年漫画（小学生向）
2.  少年漫画（小学生 - 高校生）
3.  少女漫画（小学生 - 高校生、一些大人的女性向）
4.  青年漫画（高校生以上）
5.  女性漫画（大人的女性向）
6.  成人漫画（18歲以上的对象，含性描写、暴力或過度血腥）

詳見下表[<sup>注2</sup>](../Page/#注解.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>colspan = "7"| 日本漫畫分類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>類別</p></td>
</tr>
<tr class="even">
<td><p>少年</p></td>
</tr>
<tr class="odd">
<td><p>少年</p></td>
</tr>
<tr class="even">
<td><p>TOP（大然）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶島少年.md" title="wikilink">寶島少年</a>（東立）</p></td>
</tr>
<tr class="even">
<td><p>少年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新少年週刊.md" title="wikilink">新少年週刊</a>（東立）</p></td>
</tr>
<tr class="even">
<td><p>青年</p></td>
</tr>
<tr class="odd">
<td><p>少女</p></td>
</tr>
<tr class="even">
<td><p>少女</p></td>
</tr>
<tr class="odd">
<td><p>少女</p></td>
</tr>
<tr class="even">
<td><p>女性</p></td>
</tr>
</tbody>
</table>

[少年漫画](../Page/少年漫画.md "wikilink")[<sup>注3</sup>](../Page/#注解.md "wikilink")：特徵：努力、友情、勝利、熱血，近年的少年漫畫更有「[主角不死論](../Page/主角不死論.md "wikilink")」、「[主角威能](../Page/主角威能.md "wikilink")」的風氣。

[少女漫畫](../Page/少女漫畫.md "wikilink")：面向6歲到18歲的女孩子。這類漫畫沒有明確的界限，不以故事類型、繪畫風格或是情節而分。僅僅當出版商想要將某個漫畫面向年輕女性發行時，就稱之為少女漫畫。

[青年漫畫](../Page/青年漫畫.md "wikilink")：更多的性和暴力畫面。這類漫畫一般較少複雜怪異的情節。一般是以考試、體育或學校生活為主。描寫了大學生、工薪族、失業者等等。帶社會或公司情節的更受歡迎。也有少量的科幻、神秘、幻想的成人漫畫。

[淑女漫畫](../Page/女性漫画.md "wikilink")：面向20歲以上女性的漫畫，特別是那些家庭主婦和白領女性。她們喜歡肥皂劇那樣的浪漫小說。

### 按題材分

[Bookshelves_with_manga_(2).jpg](https://zh.wikipedia.org/wiki/File:Bookshelves_with_manga_\(2\).jpg "fig:Bookshelves_with_manga_(2).jpg")
[MangaStoreJapan.jpg](https://zh.wikipedia.org/wiki/File:MangaStoreJapan.jpg "fig:MangaStoreJapan.jpg")

  - [諷刺漫畫](../Page/諷刺漫畫.md "wikilink") - 以諷刺現實社會為目的的漫畫。
  - [學習漫畫](../Page/學習漫畫.md "wikilink") - 以傳授知識為目的的漫畫。
  - [幽默漫畫](../Page/幽默漫畫.md "wikilink") - 以搞笑為目的的漫畫。
  - [戀愛漫畫](../Page/戀愛漫畫.md "wikilink") -
    以[戀愛為題材的漫畫](../Page/戀愛.md "wikilink")。
  - [體育漫畫](../Page/體育漫畫.md "wikilink") -
    以[體育運動為題材的漫畫](../Page/體育運動.md "wikilink")。
  - [格鬥漫畫](../Page/格鬥漫畫.md "wikilink") - 以格鬥技為題材的漫畫，面向少年和青年。
  - [校園漫畫](../Page/校園漫畫.md "wikilink") -
    以[學校為舞台和](../Page/學校.md "wikilink")[學生生活為中心的漫畫](../Page/學生.md "wikilink")。
  - [黑道漫畫](../Page/黑道漫畫.md "wikilink") -
    以[日本黑道為題材的漫畫](../Page/日本黑道.md "wikilink")。
  - [政治漫畫](../Page/政治漫畫.md "wikilink") -
    以政界（[政治家](../Page/政治家.md "wikilink")）為題材的漫畫。
  - [經濟漫畫](../Page/經濟漫畫.md "wikilink") -
    以[經濟為題材的漫畫](../Page/經濟.md "wikilink")。
  - [医療漫畫](../Page/医療漫畫.md "wikilink") -
    以[医療為題材的漫畫](../Page/医療.md "wikilink")。
  - [料理漫畫](../Page/料理漫畫.md "wikilink") -
    以[烹飪](../Page/烹飪.md "wikilink")、[廚藝為題材的漫畫](../Page/廚藝.md "wikilink")。
  - [恐怖漫畫](../Page/恐怖漫畫.md "wikilink") - 以恐怖為題材的漫畫。
  - [科幻漫畫](../Page/科幻漫畫.md "wikilink") -
    以[科學幻想為題材的漫畫](../Page/科學幻想.md "wikilink")。
  - [奇幻漫畫](../Page/奇幻漫畫.md "wikilink") - 以異世界為舞台的漫畫。
  - [推理漫畫](../Page/推理漫畫.md "wikilink") -
    以[解謎為題材的漫畫](../Page/解謎.md "wikilink")。
  - [音樂漫畫](../Page/音樂漫畫.md "wikilink") -
    以[音樂](../Page/音樂.md "wikilink")・[樂器為題材的漫畫](../Page/樂器.md "wikilink")。
  - [冒險漫畫](../Page/冒險漫畫.md "wikilink") -
    以[冒險為題材的漫畫](../Page/冒險.md "wikilink")。
  - [歷史漫畫](../Page/歷史漫畫.md "wikilink") -
    以[歷史為題材的漫畫](../Page/歷史.md "wikilink")。
  - [動物漫畫](../Page/動物漫畫.md "wikilink") -
    以[動物為題材的漫畫](../Page/動物.md "wikilink")。
  - [戰爭漫畫](../Page/戰爭漫畫.md "wikilink") -
    以[戰爭為題材的漫畫](../Page/戰爭.md "wikilink")。

### 按表現形式分

  - [單格漫畫](../Page/單格漫畫.md "wikilink")
  - [四格漫畫](../Page/四格漫畫.md "wikilink")
  - [單頁漫畫](../Page/單頁漫畫.md "wikilink")

## 繪畫工具

### 傳統

  - 基本繪畫工具

<!-- end list -->

  - [原稿用紙](../Page/原稿用紙.md "wikilink")
  - [鉛筆](../Page/鉛筆.md "wikilink")・[橡皮擦](../Page/橡皮擦.md "wikilink")
  - [沾水筆](../Page/沾水筆.md "wikilink")
  - [製圖筆](../Page/製圖筆.md "wikilink")
  - [墨水](../Page/墨水.md "wikilink")・[墨汁](../Page/墨汁.md "wikilink")
  - [塗改液](../Page/塗改液.md "wikilink")
  - [毛筆](../Page/毛筆.md "wikilink")・日本製毛筆（）
  - [網點紙](../Page/網點紙.md "wikilink")
  - [美工刀](../Page/美工刀.md "wikilink")
  - 尺・[雲形尺](../Page/雲形尺.md "wikilink")・[模板](../Page/模板.md "wikilink")

<!-- end list -->

  - 彩色原稿繪畫工具

<!-- end list -->

  - [Copic](../Page/Copic.md "wikilink")
  - 水彩畫工具
  - 彩色墨水
  - [噴槍](../Page/噴槍.md "wikilink")
  - [粉彩](../Page/粉彩.md "wikilink")・[蠟筆](../Page/蠟筆.md "wikilink")

### 數位繪圖

  - 板繪

硬體：[個人電腦](../Page/個人電腦.md "wikilink")+[绘图板或者單一的智機](../Page/数码绘图板.md "wikilink")、平板。

  - 鼠繪

硬體：[個人電腦](../Page/個人電腦.md "wikilink")+[滑鼠](../Page/滑鼠.md "wikilink")

  - 軟體：[Adobe
    Photoshop](../Page/Adobe_Photoshop.md "wikilink")、[Painter](../Page/Painter.md "wikilink")、[ComicStudio](../Page/ComicStudio.md "wikilink")、[SAI等](../Page/SAI.md "wikilink")。

## 日本漫画家

日本龐大的漫畫產業使日本湧現出眾多優秀的[漫畫家](../Page/漫畫家.md "wikilink")。由於日本漫畫家的作品通常在[漫畫雜誌上定期連載](../Page/漫畫雜誌.md "wikilink")，他們的工作是比較辛苦的。許多日本漫畫家在創作時有[助手輔助](../Page/助手.md "wikilink")。一般來說，故事構成，主要人物的繪製由漫畫家自己完成；群衆和背景描寫，原稿[塗黑](../Page/塗黑_\(漫畫\).md "wikilink")，黏貼[網點紙等細節上的操作則由助手擔任](../Page/網點紙.md "wikilink")[<sup>注4</sup>](../Page/#注解.md "wikilink")。

## 同人和Cosplay

[同人并非制作中的一环](../Page/同人.md "wikilink")，而是指其狂热爱好者们的组织。日本有许多有名的漫画家都是画同人出身的。同人的活动很多，有写同人小说的，有画同人漫画的。现重点介绍一下Cosplay和同人志。

[Cosplay這個字來自日文的コスプレ](../Page/Cosplay.md "wikilink")，而這四個字分別來自コスチューム(Costume)與プレー(Play)，意即一種穿著舞台用衣裝的聚會[<sup>注5</sup>](../Page/#注解.md "wikilink")，在这里指扮演动漫或游戏里的人物，是广大动漫FANS和游戏FANS的梦想和最爱。中文译为角色扮演。近年Cosplay活动也涵盖视觉系乐团与电影。在日本，COSPLAY
SHOW(真人模仿秀或是角色扮演)极为风行。基本上大小游戏、动漫展会都可以看到形形色色的Cosplay扮演者。

在日本，同人志也非常的繁荣昌盛。所谓的同人志也就是业余漫画家所组成的团体。他们自费出版自己的漫画杂志，单行本，周边产品，目的可以是为自娱自乐，也可以作为磨练画技，积累经验，提升名气，从而获得漫画出版社的赏识青睐，成为职业的漫画家。在日本各地同人志团体非常的多，入团人数连年增长，每年举办的各项活动如画展，Cosplay比赛等动辄就有几千人参加，他们出版的漫画也有固定销量。

## 日式漫画

[Mahuri.svg](https://zh.wikipedia.org/wiki/File:Mahuri.svg "fig:Mahuri.svg")
日式漫画是[日本](../Page/日本.md "wikilink")[漫画的另一种类型](../Page/漫画.md "wikilink")。

战后时期，漫画自西方传入[日本](../Page/日本.md "wikilink")，并发展出独特的风格，逐渐在日本形成了拥有大量读者群的庞大产业，其影响也延伸到其他国家。日式漫畫並不局限於[日本漫畫](../Page/日本漫畫.md "wikilink")，在[華人地區](../Page/華人地區.md "wikilink")、[南韓](../Page/南韓.md "wikilink")、[東南亞和](../Page/東南亞.md "wikilink")[歐美等地區也有許多和前者風格相近的作品](../Page/歐美.md "wikilink")。

“漫画”在[日语中的意思相当于](../Page/日语.md "wikilink")[连环画](../Page/连环画.md "wikilink")，是用多幅画面连续叙述一个故事或事件发展过程的绘画形式。日本人将漫画这一特殊的[文化现象视作一门](../Page/文化.md "wikilink")[艺术](../Page/艺术.md "wikilink")，它的读者不局限于任何年齡，但大體上還是針對[青少年](../Page/青少年.md "wikilink")。

大多数日式漫画通常连载于[漫画杂志上](../Page/漫画杂志.md "wikilink")。当一部漫画连载了一段时间之后，将以单行本的形式重新制作出版。此类单行本大多印刷在较高级的纸张上，有利于那些想收集的人。

日式漫画的风格十分独特，强调对于线的处理，其故事情节以及分格的设置与其他漫画有较多区别。

### 特色与风格

日式漫畫的人物[身體和](../Page/身體.md "wikilink")[四肢比較接近真人](../Page/四肢.md "wikilink")，[頭和](../Page/頭.md "wikilink")[眼睛比較大](../Page/眼睛.md "wikilink")，[鼻子和](../Page/鼻子.md "wikilink")[嘴巴比真人小](../Page/嘴巴.md "wikilink")。日式漫畫向來不刻意描繪[人種特徵](../Page/人種.md "wikilink")，有時連[性別也難以分辨](../Page/性別.md "wikilink")。[年齡大多設定在](../Page/年齡.md "wikilink")[青少年階段](../Page/青少年.md "wikilink")，因此[臉的輪廓和皺紋相對較少](../Page/臉.md "wikilink")。另外，日式漫畫裡的人物通常具有異於常人的[人格和](../Page/人格.md "wikilink")[情緒表情](../Page/情緒.md "wikilink")，請參見[動畫物理學](../Page/動畫物理學.md "wikilink")。

這些設定在[ACG作品已成为一种公认设定](../Page/ACG.md "wikilink")，其起始于[手塚治虫的](../Page/手塚治虫.md "wikilink")[原子小金鋼等作品](../Page/原子小金鋼.md "wikilink")。這種概念到1980年代至1990年代正式成行，2000年代開始走向[國際化](../Page/國際化.md "wikilink")。

## 注释

1.  *其实日本漫画没有特别明确的分类，这里说的仅仅是其中一种。*
2.  '' 资料来源：www.snsn.com.cn上《日本漫画分类简言》一文。''
3.  '' 资料来源：南方网动漫频道《日本漫画分类》一文。 ''
4.  *另見[日本漫畫家條目](../Page/日本漫畫家.md "wikilink")。*
5.  '' 引用www.southcn.com上《台湾Cosplay发展史》一文。''

## 參考來源

## 相關條目

  - [漫畫](../Page/漫畫.md "wikilink")
  - [漫畫咖啡店](../Page/漫畫咖啡店.md "wikilink")
  - [日本動畫](../Page/日本動畫.md "wikilink")
  - [漫畫改編電視劇](../Page/漫畫改編電視劇.md "wikilink")

## 外部链接

[\*](../Category/日本漫畫.md "wikilink")
[Category:漫畫類型](../Category/漫畫類型.md "wikilink")

1.

2.

3.  Gravett, Paul. 2004. *Manga: Sixty Years of Japanese Comics.* NY:
    Harper Design. ISBN 978-1-85669-391-2. p. 8.

4.  Schodt, Frederik L. 1996. *Dreamland Japan: Writings on Modern
    Manga.* Berkeley, CA: Stone Bridge Press. ISBN 978-1880656235.

5.  "Japanese Manga Market Drops Below 500 Billion Yen." (2007-03-10)
    <http://comipress.com/news/2007/03/10/1622> Accessed 2007-09-14.

6.  Kishi, Torajiro. 1998. *Colorful*. Tokyo: Shueisha. ISBN
    4-08-782556-6.

7.  Katzenstein, Peter. J. & Takashi Shiraishi 1997. '' Network Power:
    Japan in Asia.'' Ithaca, NY: Cornell University Press. ISBN
    978-0801483738.

8.
9.  Schodt, Frederik L. 1986. *Manga\! Manga\! The World of Japanese
    Comics.* Tokyo: Kodansha. ISBN 978-0-87011-752-7.

10.
11.
12.

13. Patten, Fred. 2004. *Watching Anime, Reading Manga: 25 Years of
    Essays and Reviews.* Berkeley, CA: Stone Bridge Press. ISBN
    978-1-880656-92-1.

14.

15.

16.
17. Kinsella, Sharon 2000. *Adult Manga: Culture and Power in
    Contemporary Japanese Society.* Honolulu: University of Hawai'i
    Press. ISBN 978-0-8248-2318-4.

18.
19.
20. Ito, Kinko. 2004. "Growing up Japanese reading manga."
    *International Journal of Comic Art*, 6:392-401.

21. Kern, Adam. 2006. *Manga from the Floating World: Comicbook Culture
    and the Kibyoshi of Edo Japan.* Cambridge: Harvard University Press.
    ISBN 978-0-674-02266-9.

22. Kern, Adam. 2007. "Symposium: Kibyoshi: The World's First
    Comicbook?" *International Journal of Comic Art*, 9:1-486.