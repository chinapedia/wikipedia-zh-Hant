**日本電視劇**（簡稱**日劇**），指[日本的](../Page/日本.md "wikilink")[電視台制作播出的](../Page/電視台.md "wikilink")[電視連續劇](../Page/電視連續劇.md "wikilink")。日剧[故事题材內容多樣](../Page/故事.md "wikilink")，不少著重於描述特定[職業的職場生態](../Page/職業.md "wikilink")，或以一般大眾日常生活場景為主軸，或以[漫畫改編故事](../Page/漫畫.md "wikilink")，甚至大胆揭露社會黑暗面。

## 概要

市場上有時稱之為「日劇長片」，用以區別日本[電影和綜藝節目](../Page/電影.md "wikilink")。由於日本製作電視劇是邊拍邊播，每天同一時段有不同的電視劇播出，而且每週播出一集，以一季（三個月）為一個週期，故大部分的日劇的集數比較少，在11或12集左右，不過還是有一些集數比較長的日劇，如[NHK的](../Page/日本放送協會.md "wikilink")[大河劇和](../Page/大河劇.md "wikilink")[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")，預算成本比較高，製作上也較為細緻與嚴謹。還有較特殊的劇種為[特攝](../Page/特攝.md "wikilink")，但大多數的情況歸為[卡通](../Page/卡通.md "wikilink")。

[日剧是](../Page/日剧.md "wikilink")1950年代随着[黑白电视机的普及发展而兴起的](../Page/黑白电视机.md "wikilink")。第一部日劇為NHK電視台試驗播放的《[晚飯前](../Page/晚飯前.md "wikilink")》。這次試驗播放取得了很好的迴響。NHK已經計劃好製作第二套電視劇進行試驗播放。但是不久後，[太平洋戰爭爆發](../Page/太平洋戰爭.md "wikilink")，計劃被迫擱置。直到戰後的1950年代，日本才正式開始播放電視劇。直到1969年1月，大河劇《[天與地](../Page/天與地_\(大河劇\).md "wikilink")》才為彩色播出的節目。

依序創立日期為[NHK](../Page/NHK.md "wikilink")、[TBS電視台](../Page/TBS電視台.md "wikilink")、[日本電視台](../Page/日本電視台.md "wikilink")、[富士電視台](../Page/富士電視台.md "wikilink")、[朝日電視台](../Page/朝日電視台.md "wikilink")、[東京電視台](../Page/東京電視台.md "wikilink")。

自從1991年《[東京愛情故事](../Page/東京愛情故事.md "wikilink")》、1994年《[一個屋簷下](../Page/一個屋簷下.md "wikilink")》等劇陸續在日本以及[香港](../Page/香港.md "wikilink")、[台灣等鄰近地區播出後](../Page/台灣.md "wikilink")，不但在日本國內引發迴響，在其他得以收看日劇的地區也開始產生顯著的影響力。隨著[日本流行文化的逐漸盛行](../Page/日本流行文化.md "wikilink")，之後數年播出的一些著名[劇集](../Page/劇集.md "wikilink")，亦曾在各地獲得相當高的[收視率](../Page/收視率.md "wikilink")，並成為[哈日族的主要話題](../Page/哈日族.md "wikilink")，與[日語學習者的主要參考對象](../Page/日語.md "wikilink")，許多情節也被其他[電影或電視劇仿傚](../Page/電影.md "wikilink")。

不過2001年之後，由於[韓國電視劇隨著](../Page/韓國電視劇.md "wikilink")[韓國流行文化向海外擴張而興起](../Page/韓國.md "wikilink")，劇集採購成本亦較為便宜，因此許多[中国大陆](../Page/中国大陆.md "wikilink")、[香港和](../Page/香港.md "wikilink")[臺灣的](../Page/臺灣.md "wikilink")[電視台轉而採購](../Page/電視台.md "wikilink")[韓劇](../Page/韓劇.md "wikilink")，並在黃金時段播出，使得原本長期居於收視較前地位的日劇備受威脅。儘管如此，仍有不少日劇依舊受到相當程度的注目，如2003年的《[白色巨塔](../Page/白色巨塔_\(2003年\).md "wikilink")》、2009年的《[仁醫](../Page/仁醫.md "wikilink")》、2013年的《[半澤直樹](../Page/半澤直樹.md "wikilink")》等。

在[臺灣](../Page/臺灣.md "wikilink")，定期播出日劇的電視台有[緯來日本台](../Page/緯來日本台.md "wikilink")、[國興衛視與](../Page/國興衛視.md "wikilink")[Z頻道](../Page/Z頻道.md "wikilink")；在[香港](../Page/香港.md "wikilink")，各電視台旗下各頻道都會不定期播放日劇，包括[無綫電視](../Page/電視廣播有限公司.md "wikilink")、[有線電視和](../Page/香港有線電視.md "wikilink")[Now寬頻電視](../Page/Now寬頻電視.md "wikilink")；在[马来西亚](../Page/马来西亚.md "wikilink")，播出日劇的電視台主要為
[8TV
八度空间](../Page/八度空间_\(电视台\).md "wikilink")、[Astro](../Page/Astro.md "wikilink")；而在[中国大陆](../Page/中国大陆.md "wikilink")，由于[政策原因](../Page/中华人民共和国电视审查.md "wikilink")，日剧同[美剧等境外影视作品一样已很少在官办的电视台上播出](../Page/美国电视剧.md "wikilink")，更多的是在[网络上以](../Page/互联网.md "wikilink")[正版或](../Page/正版.md "wikilink")[盗版的方式流传](../Page/盗版.md "wikilink")。

## 製作體系

日本電視劇與[韓國電視劇](../Page/韓國電視劇.md "wikilink")、[臺灣電視劇的製作體系相同](../Page/臺灣電視劇.md "wikilink")，也是邊拍邊播，但在正式公開播出前會先拍好2、3集存檔，故在劇情上也較為穩定，同時也甚少因為[觀眾的意見而隨意更動劇本](../Page/觀眾.md "wikilink")，與[演員經常因為趕拍戲劇而常發生意外事故或有健康風險的臺](../Page/演員.md "wikilink")、韓相比，日本演員這方面的風險就少了很多，但也因為這種製作模式，日本電視劇的收視率波動率也較臺、韓電視劇要來的大。

## 拍攝場景

### 日本取景

絕大多數位於[東京](../Page/東京.md "wikilink")、[大阪等大城市](../Page/大阪.md "wikilink")。以下為少數特有的多數場景在少有行政區：

  - [北海道](../Page/北海道.md "wikilink")：
  - [青森縣](../Page/青森縣.md "wikilink")：
  - [鹿兒島縣](../Page/鹿兒島縣.md "wikilink")：
  - [沖繩縣](../Page/沖繩縣.md "wikilink")：

### 海外取景

  - 亞洲

<!-- end list -->

  - [臺灣](../Page/臺灣.md "wikilink")：[華麗一族](../Page/華麗一族.md "wikilink")（2007版；[臺北市](../Page/臺北市.md "wikilink")）、[台灣歌姬·鄧麗君](../Page/台灣歌姬·鄧麗君.md "wikilink")（特別版；[臺北市](../Page/臺北市.md "wikilink")）、[四重奏](../Page/四重奏_\(日本電視劇\).md "wikilink")（臺北市[萬華區](../Page/萬華區.md "wikilink")）、[新娘暖簾](../Page/新娘暖簾.md "wikilink")（[台北市](../Page/台北市.md "wikilink")）、[GTO
    TAIWAN](../Page/GTO_TAIWAN.md "wikilink")（特別版；[臺北市](../Page/臺北市.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")）、[惡貨](../Page/惡貨.md "wikilink")（臺北市）、[死之臓器](../Page/死之臓器.md "wikilink")（臺北市萬華區[西門町](../Page/西門町.md "wikilink")）、[孤獨的美食家
    Season5](../Page/孤獨的美食家.md "wikilink")（[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[臺北市](../Page/臺北市.md "wikilink")）、[乞愛者](../Page/乞愛者.md "wikilink")（[嘉義市](../Page/嘉義市.md "wikilink")、[阿里山鄉](../Page/阿里山鄉.md "wikilink")）
  - [中国大陆](../Page/中国大陆.md "wikilink")：[流转的王妃·最后的皇弟](../Page/流转的王妃·最后的皇弟.md "wikilink")（特別版；[故宫](../Page/故宫.md "wikilink")）、[手機刑警錢形舞](../Page/手機刑警錢形舞.md "wikilink")（[天安門](../Page/天安門.md "wikilink")）、[華麗一族](../Page/華麗一族.md "wikilink")（2007版；[上海市](../Page/上海市.md "wikilink")）、[蒼穹之昴](../Page/蒼穹之昴_\(電視劇\).md "wikilink")（[橫店影視城](../Page/橫店影視城.md "wikilink")）、[OL生存戰](../Page/OL日本.md "wikilink")、[坂上之雲](../Page/坂上之雲.md "wikilink")（[內蒙古](../Page/內蒙古.md "wikilink")、上海影視樂園、北京飛騰影視基地）、[月之戀人](../Page/月之戀人.md "wikilink")（[上海市](../Page/上海市.md "wikilink")）、[紅十字女護士](../Page/紅十字女護士.md "wikilink")（[遼寧省](../Page/遼寧省.md "wikilink")、[吉林省](../Page/吉林省.md "wikilink")、[黑龙江省](../Page/黑龙江省.md "wikilink")）、[LEADERS](../Page/LEADERS.md "wikilink")（特別版；[上海市](../Page/上海市.md "wikilink")）
  - [香港](../Page/香港.md "wikilink")：[Friends](../Page/命運之戀.md "wikilink")（特別版）、[大胃王神探－香港猴腮雷篇](../Page/大胃王神探.md "wikilink")（特別版）、[台灣歌姬·鄧麗君](../Page/台灣歌姬·鄧麗君.md "wikilink")（特別版）、[金田一少年之事件簿
    香港九龍財寶殺人事件](../Page/金田一少年之事件簿_香港九龍財寶殺人事件.md "wikilink")（特別版）、[相棒Eleven](../Page/相棒.md "wikilink")、[死亡筆記本
    NEW
    GENERATION](../Page/死亡筆記本_\(電影\).md "wikilink")（網路劇）、[愛在香港](../Page/愛在香港.md "wikilink")、[致命之吻](../Page/致命之吻.md "wikilink")
  - [澳門](../Page/澳門.md "wikilink")：
  - [蒙古國](../Page/蒙古國.md "wikilink")：[熟女真命苦日本电视剧](../Page/熟女真命苦.md "wikilink")
  - [韓國](../Page/韓國.md "wikilink")：[Friends](../Page/命運之戀.md "wikilink")（特別版）、[愛美大作戰－去韓國找男人篇](../Page/愛美大作戰.md "wikilink")（特別版）、[Fighting
    Girl](../Page/Fighting_Girl.md "wikilink")、[東京灣景](../Page/東京灣景.md "wikilink")（[首爾](../Page/首爾.md "wikilink")）、[輪舞曲](../Page/輪舞曲_\(電視劇\).md "wikilink")（[首爾](../Page/首爾.md "wikilink")、[安眠島](../Page/安眠島.md "wikilink")）、[情定大飯店](../Page/情定大飯店_\(日本電視劇\).md "wikilink")（[首爾](../Page/首爾.md "wikilink")）、[淺草福丸旅館2](../Page/淺草福丸旅館.md "wikilink")
    （[濟州島](../Page/濟州島.md "wikilink")）、[最遙遠的銀河](../Page/最遙遠的銀河.md "wikilink")（特別版；[首爾](../Page/首爾.md "wikilink")）、[精靈守護者](../Page/精靈守護者.md "wikilink")、[對不起，我愛你](../Page/對不起，我愛你_\(日本電視劇\).md "wikilink")
    （[首爾](../Page/首爾.md "wikilink")）
  - [馬來西亞](../Page/馬來西亞.md "wikilink")：[金田一少年之事件簿
    獄門塾殺人事件](../Page/金田一少年之事件簿_獄門塾殺人事件.md "wikilink")（特別版）
  - [新加坡](../Page/新加坡.md "wikilink")：[第二處女](../Page/第二處女.md "wikilink")
  - [泰國](../Page/泰國.md "wikilink")：[HOME
    DRAMA！](../Page/HOME_DRAMA！.md "wikilink")、[台灣歌姬·鄧麗君](../Page/台灣歌姬·鄧麗君.md "wikilink")（特別版）、[謊言戰爭](../Page/謊言戰爭.md "wikilink")（[曼谷](../Page/曼谷.md "wikilink")）
  - [緬甸](../Page/緬甸.md "wikilink")：[HOME
    DRAMA！](../Page/HOME_DRAMA！.md "wikilink")

<!-- end list -->

  - 歐洲

<!-- end list -->

  - [俄羅斯](../Page/俄羅斯.md "wikilink")：[坂上之雲](../Page/坂上之雲.md "wikilink")（[聖彼得堡](../Page/聖彼得堡.md "wikilink")）
  - [拉脫維亞](../Page/拉脫維亞.md "wikilink")：[坂上之雲](../Page/坂上之雲.md "wikilink")
  - [英國](../Page/英國.md "wikilink")：[悠長假期](../Page/悠長假期.md "wikilink")（[倫敦](../Page/倫敦.md "wikilink")）、[不愉快的基因](../Page/不愉快的基因.md "wikilink")（[倫敦](../Page/倫敦.md "wikilink")）、[坂上之雲](../Page/坂上之雲.md "wikilink")（[倫敦](../Page/倫敦.md "wikilink")、[朴次茅斯](../Page/朴次茅斯.md "wikilink")）、[阿政](../Page/阿政.md "wikilink")（[格拉斯哥](../Page/格拉斯哥.md "wikilink")）
  - [法國](../Page/法國.md "wikilink")：[網路情人](../Page/網路情人.md "wikilink")、[美女或野獸](../Page/美女或野獸.md "wikilink")、[別開玩笑！](../Page/別開玩笑！.md "wikilink")、[交響情人夢·巴黎篇](../Page/交響情人夢_\(電視劇\).md "wikilink")（特別版；[巴黎](../Page/巴黎.md "wikilink")）、[三角效應](../Page/三角效應.md "wikilink")（[巴黎](../Page/巴黎.md "wikilink")）、[坂上之雲](../Page/坂上之雲.md "wikilink")、[玻璃之家](../Page/玻璃之家.md "wikilink")（[布列塔尼](../Page/布列塔尼.md "wikilink")）、[小希](../Page/小希.md "wikilink")（[巴黎](../Page/巴黎.md "wikilink")）、[天皇的御廚](../Page/天皇的御廚.md "wikilink")（[巴黎](../Page/巴黎.md "wikilink")）、
    [山田孝之的坎城影展](../Page/山田孝之的坎城影展.md "wikilink")（[坎城](../Page/坎城.md "wikilink")）、[西村京太郎懸疑劇場
    十津川警部系列
    伊豆海岸殺人根源](../Page/西村京太郎懸疑劇場_十津川警部系列.md "wikilink")（[巴黎](../Page/巴黎.md "wikilink")）
  - [德國](../Page/德国.md "wikilink")：[Second
    Love](../Page/Second_Love.md "wikilink")（[易北河](../Page/易北河.md "wikilink")）、[愛吃拉麵的小泉同學2016年末SP](../Page/愛吃拉麵的小泉同學.md "wikilink")（[法蘭克福](../Page/法蘭克福.md "wikilink")）
  - [波蘭](../Page/波蘭.md "wikilink")：[白色巨塔](../Page/白色巨塔_\(2003年電視劇\).md "wikilink")（2003版）
  - [義大利](../Page/義大利.md "wikilink")：[料理新人王](../Page/料理新鮮人.md "wikilink")（[羅馬](../Page/羅馬.md "wikilink")）、[愛情革命](../Page/愛情革命.md "wikilink")
  - [西班牙](../Page/西班牙.md "wikilink")：[派遣女王](../Page/派遣女王.md "wikilink")
  - [馬爾他](../Page/馬爾他.md "wikilink")：[坂上之雲](../Page/坂上之雲.md "wikilink")

<!-- end list -->

  - 美洲

<!-- end list -->

  - [美國](../Page/美國.md "wikilink")：[百年物語](../Page/百年物語.md "wikilink")、[櫻花](../Page/櫻花_\(晨間劇\).md "wikilink")（[夏威夷](../Page/夏威夷.md "wikilink")）、[離婚女律師](../Page/離婚女律師.md "wikilink")（特別版；[紐約](../Page/紐約.md "wikilink")）、[大和拜金女](../Page/大和拜金女.md "wikilink")（[紐約](../Page/紐約.md "wikilink")）、[Attention
    Please特別篇](../Page/Attention_Please.md "wikilink")（特別版；[夏威夷](../Page/夏威夷.md "wikilink")）、[考試之神](../Page/考試之神.md "wikilink")（[波士頓](../Page/波士頓.md "wikilink")）、[求婚大作戰](../Page/求婚大作戰.md "wikilink")（特別版；[夏威夷](../Page/夏威夷.md "wikilink")）、[被迫歸來的33分偵探](../Page/33分偵探.md "wikilink")（[芝加哥](../Page/芝加哥.md "wikilink")）、[坂上之雲](../Page/坂上之雲.md "wikilink")（[紐約](../Page/紐約.md "wikilink")、[尼亚加拉瀑布](../Page/尼亚加拉瀑布.md "wikilink")、[聖地亞哥灣](../Page/聖地亞哥灣.md "wikilink")）、[外交官
    黑田康作](../Page/外交官_黑田康作.md "wikilink")（[洛杉磯](../Page/洛杉磯.md "wikilink")）、[都市傳說之女](../Page/都市傳說之女.md "wikilink")（[紐約](../Page/紐約.md "wikilink")）、[ATARU特別篇〜從紐約來的挑戰書\!\!〜](../Page/ATARU.md "wikilink")（[紐約](../Page/紐約.md "wikilink")）、[RICH
    MAN, POOR WOMAN in
    紐約](../Page/RICH_MAN,_POOR_WOMAN.md "wikilink")（特別版；[紐約](../Page/紐約.md "wikilink")）、[空中刑事〜紐約殺人事件〜](../Page/空中刑事〜紐約殺人事件〜.md "wikilink")（特別版；[紐約](../Page/紐約.md "wikilink")）
  - [加拿大](../Page/加拿大.md "wikilink")：[花子與安妮](../Page/花子與安妮.md "wikilink")、[愛在聖誕節](../Page/愛在聖誕節.md "wikilink")（[黃刀鎮](../Page/黃刀鎮.md "wikilink")）

<!-- end list -->

  - 大洋洲

<!-- end list -->

  - [澳大利亞](../Page/澳大利亞.md "wikilink")：[在世界的中心呼喊愛情](../Page/在世界的中心呼喊愛情.md "wikilink")、[西遊記](../Page/西遊記_\(2006年電視劇\).md "wikilink")（[新南威爾士州](../Page/新南威爾士州.md "wikilink")）、[Attention
    Please之飛向澳洲雪梨](../Page/Attention_Please.md "wikilink")（特別版；[雪梨](../Page/雪梨.md "wikilink")）
  - [紐西蘭](../Page/紐西蘭.md "wikilink")：[愛在聖誕節](../Page/愛在聖誕節.md "wikilink")、[魔法戰隊魔法連者](../Page/魔法戰隊魔法連者.md "wikilink")（[奧克蘭市](../Page/奧克蘭市.md "wikilink")）

## 特殊現象

  - **集數短**

<!-- end list -->

  -
    與東亞其他地方 ( 如香港、臺灣、中國大陸和韓國 )
    相比，日本電視劇除了[NHK的晨間連續劇與](../Page/NHK.md "wikilink")[大河劇](../Page/大河劇.md "wikilink")、東海電視台（富士系）的午間劇及朝日電視的電視劇《相棒》外，1980年以後的日本電視劇幾乎清一色集數都不長，大部分日本電視劇集數都只有9～12集左右，偶爾甚至會出現只有5～8集或少數14～20集就完結的情況。
    另外特殊的電視劇特別篇，長度跟電視電影的長度差不多，一般僅有一集，有些跟比連續劇推出還來早的特別篇也有關，例如《[時尚女王](../Page/時尚女王.md "wikilink")》、《[殺人草莓夜](../Page/殺人草莓夜.md "wikilink")》、《[ST
    警視廳科學特搜班](../Page/ST_警視廳科學特搜班.md "wikilink")》、《最後的晚餐〜刑事・遠野一行與七個嫌疑人〜》，依應節日的特別篇，例如《NHK正月時代劇》、《東京電視台新春大型時代劇》。有些原先為集數多的連續劇，現在以幾乎每年推出特別篇的方式，例如《[世界奇妙物語](../Page/世界奇妙物語.md "wikilink")》、《[毛骨悚然撞鬼經驗](../Page/毛骨悚然撞鬼經驗.md "wikilink")》、《[冷暖人間](../Page/冷暖人間.md "wikilink")》、《必殺仕事人》。有些有兩集以上的，例如《[我家的歷史](../Page/我家的歷史.md "wikilink")》、《[東方快車謀殺案](../Page/東方快車謀殺案_\(日本電視劇\).md "wikilink")》、《[永遠的0](../Page/永遠的0.md "wikilink")》。

<!-- end list -->

  - **社會風氣**

<!-- end list -->

  -
    一些起源於日本的社會風氣經常被拍攝成為電視劇，包括[辣妹](../Page/辣妹.md "wikilink")、[御宅族](../Page/御宅族.md "wikilink")、[草食男](../Page/草食男.md "wikilink")、[肉食女](../Page/肉食女.md "wikilink")、[怪獸家長等等](../Page/怪獸家長.md "wikilink")。

<!-- end list -->

  - **拍攝地**

<!-- end list -->

  -
    除了晨間連續劇和大河劇以外，日本電視劇的主要拍攝地場景還是在[東京或](../Page/東京.md "wikilink")[橫濱等首都圈地區](../Page/橫濱.md "wikilink")；偶爾才是日本的其他各縣市，有些日劇偶爾也有前往國外拍攝的橋段。

<!-- end list -->

  - **傑尼斯偶像肖像權**

<!-- end list -->

  -
    一般電視劇通常在官方網站或是文宣上都會將演員的頭像放在所飾演的角色介紹旁邊，但日本的傑尼斯事務所對旗下藝人的肖像權向來把關較為嚴格，因而過去日本電視劇只要介紹演員角色時，只要是傑尼斯旗下藝人的頭像通常是不放出來的或是直接拿原作圖像來代替的現象。但近年傑尼斯對肖像應用於電視劇角色介紹已有所放寬，傑尼斯旗下藝人的頭像可以用於角色介紹。以[空中急診英雄系列為例](../Page/空中急診英雄.md "wikilink")，於2008及2010年播放之第一及第二季，官方網站人物關系圖以剪影代替飾演主角藍澤耕作之[山下智久頭像](../Page/山下智久.md "wikilink")\[1\]\[2\]；但於2017年播放之第三季中，官網已可使用山下智久之頭像於人物關系圖\[3\]。

<!-- end list -->

  - **不隨意更改劇本或增加集數**

<!-- end list -->

  -
    與常因觀眾意見而隨意更改劇本或增加集數的台韓電視劇相較，日本電視劇很少做這樣的變動，除了不得已才做修改（例如遇到電視尺度、遭投訴有嚴重問題，可能在後面集數稍作剪輯），但如果收視不振，仍會將電視劇腰斬。

<!-- end list -->

  - **外國影星的演出**

<!-- end list -->

  -
    有部分的日劇會有外國影星的加入演出，包括[翁倩玉](../Page/翁倩玉.md "wikilink")、[林志玲](../Page/林志玲.md "wikilink")、[王菲](../Page/王菲.md "wikilink")、[柳時元](../Page/柳時元.md "wikilink")、[崔智友](../Page/崔智友.md "wikilink")、[徐若瑄](../Page/徐若瑄.md "wikilink")、[元斌](../Page/元斌.md "wikilink")、[金泰希等等](../Page/金泰希.md "wikilink")。

<!-- end list -->

  - **與電影近期推出**

<!-- end list -->

  -
    有部分的日劇與電影近期推出，甚至會跟近期上映的電影有關，包括《[詐欺遊戲](../Page/詐欺遊戲_\(電視劇\).md "wikilink")》番外篇、《[MW
    第0章 〜惡魔的遊戲〜](../Page/MW_\(漫畫\).md "wikilink")》、《[鬼壓床了沒 特別版
    〜完美的門房](../Page/鬼壓床了沒.md "wikilink")》、《[黑百合住宅區～序章～](../Page/黑百合住宅區.md "wikilink")》、《[大奧
    ～誕生【有功·家光篇】](../Page/大奧_～誕生【有功·家光篇】.md "wikilink")》、《[近距離戀愛～Season
    Zero～](../Page/近距離戀愛.md "wikilink")》、《[進擊的巨人](../Page/進擊的巨人.md "wikilink")》、《[信長協奏曲](../Page/信長協奏曲.md "wikilink")》。

<!-- end list -->

  - **近年原創作品較少，改編小說或漫畫之作品較多；改編書籍之電視劇與被改編的原作之初版日期時間縮短**

## 播出時段

依目前情況，如果可能會隨時間變更。以下是粗略的介紹：

<table>
<thead>
<tr class="header">
<th><p>播出時段</p></th>
<th><p>星期一</p></th>
<th><p>星期二</p></th>
<th><p>星期三</p></th>
<th><p>星期四</p></th>
<th><p>星期五</p></th>
<th><p>星期六</p></th>
<th><p>星期日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>月曜日</p></td>
<td><p>火曜日</p></td>
<td><p>水曜日</p></td>
<td><p>木曜日</p></td>
<td><p>金曜日</p></td>
<td><p>土曜日</p></td>
<td><p>日曜日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>早上</p></td>
<td><p>晨間小說連續劇</p></td>
<td><p>日7</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中午</p></td>
<td><p>午間劇</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>晚上</p></td>
<td><p>月9</p></td>
<td><p>火9<br />
火10</p></td>
<td><p>水9<br />
水10</p></td>
<td><p>木8<br />
木9<br />
木10</p></td>
<td><p>金8<br />
金10</p></td>
<td><p>土9<br />
土10</p></td>
<td><p>大河歷史劇<br />
（日8）<br />
日9<br />
日10B</p></td>
</tr>
<tr class="odd">
<td><p>深夜</p></td>
<td><p>月24</p></td>
<td><p>火25</p></td>
<td><p>水25</p></td>
<td><p>木23</p></td>
<td><p>金23<br />
電視劇24<br />
（金24）<br />
金25</p></td>
<td><p>土23<br />
土25</p></td>
<td><p>日24</p></td>
</tr>
</tbody>
</table>

  - 為8:00\~12:00（[日本時間](../Page/日本時間.md "wikilink")）的時段，NHK目前以最受好評的[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")，題材多為家庭劇，另外也有設定為近代背景，放在早晨時段，每集約15分鐘，大多為150集左右。日7為朝日電視台的特攝劇為[超級戰隊系列和](../Page/超級戰隊系列.md "wikilink")[假面騎士系列](../Page/假面騎士系列.md "wikilink")。
  - 中午時段，12:00\~14:00的時段，以[東海電視台及](../Page/東海電視台.md "wikilink")[朝日電視台午間劇](../Page/朝日電視台.md "wikilink")，題材多為家庭劇，同樣地，另外也有設定為近代背景，每集約30分鐘，大多為60集左右。
  - 晚上時段，20:00\~24:00的時段，因為比其他時段觀看的人數來的多，故又稱作黃金時段，每集約50分鐘，大多為13集內。但[NHK大河劇以一劇唯一整年撥出](../Page/NHK大河劇.md "wikilink")，每集約50分鐘，為50集左右。
  - 深夜時段，通常24:00（即隔天0:00）之後的劇集都歸類於此，每集約30分鐘、15分鐘，甚至有5分鐘，同樣地，大多為13集內。這段時間的節目在日本國外播出的分級相對就比較不適合闔家觀賞。
  - 同一時段的電視劇各電視台不一定會在同一時間點播出，例如：[朝日電視台週四晚間八點推理連續劇](../Page/朝日電視台週四晚間八點推理連續劇.md "wikilink")（木8，20:00）、[朝日電視台週四晚間九點連續劇](../Page/朝日電視台週四晚間九點連續劇.md "wikilink")（木9，21:00）、[富士電視台週四連續劇](../Page/富士電視台週四連續劇.md "wikilink")（木10，22:00）、週四時代劇（木10，22:00），相對不會搶收視率。
  - 與其他東亞國家劇集不同的是，收視率的不同時段皆有一定的門檻，也視依前幾檔劇集收視率的幅降多寡，午間劇時段4%以上，晚上時段10%以上，深夜劇2%以上，可以決定由收視率來是否廢除此時段，例如：[讀賣電視台週四連續劇收視不濟而廢除](../Page/讀賣電視台週四連續劇.md "wikilink")，另外，四大電視網（或電視台）不常安排同一時段、同一時間作收視競爭，因此收視率若不是因大眾的喜好，大部分不受他台影響，但是可能會受到他台的非戲劇節目（例如：棒球比賽）影響。

## 電視劇列表

{{-}}

## 另見

  - [日本電視節目列表](../Page/日本電視節目列表.md "wikilink")
  - [日本电视剧列表](../Page/日本电视剧列表.md "wikilink")
  - [日本廣播劇](../Page/日本廣播劇.md "wikilink")

## 參考資料

[Japan](../Category/各国电视剧.md "wikilink")
[日本電視劇](../Category/日本電視劇.md "wikilink")
[Category:動態列表](../Category/動態列表.md "wikilink")

1.  <http://www.fujitv.co.jp/codeblue1st/chart/index.html>
2.  <http://www.fujitv.co.jp/codeblue2nd/chart/>
3.  <http://www.fujitv.co.jp/codeblue/chart/index.html>