**日本索尼音樂娛樂**（，簡稱索尼音樂（**''**''）或**SME**、**SMEJ**）是[索尼專責日本國內](../Page/索尼.md "wikilink")[音樂事業部門的經營統合及](../Page/音樂.md "wikilink")[控股公司](../Page/控股公司.md "wikilink")。1968年創立，之後曾經歷多次企業[合併與分割](../Page/合併.md "wikilink")。現在的公司法人成立於2003年4月1日，為索尼全資[子公司](../Page/子公司.md "wikilink")，總部位於[東京](../Page/東京.md "wikilink")[麴町](../Page/麴町.md "wikilink")。以其為中心組成的日本索尼音樂集團（Sony
Music
Group）獨立於全球的[索尼音樂娛樂體系之外](../Page/索尼音樂娛樂.md "wikilink")，後者由索尼在美國的事業體主導經營，兩者僅在品牌與公司名稱上共用「Sony
Music」名稱。

## 歷史

1968年3月1日，索尼與美國[哥倫比亞廣播公司](../Page/哥倫比亞廣播公司.md "wikilink")（CBS）合資成立[CBS索尼](../Page/CBS/Sony.md "wikilink")（CBS/Sony；成立時名為「CBS索尼唱片」）。此後索尼在日本國內又陸續成立[日本史詩唱片](../Page/史诗唱片日本.md "wikilink")、CBS索尼唱片等音樂關聯企業。1988年1月，在索尼[董事長](../Page/董事長.md "wikilink")[盛田昭夫的主導下](../Page/盛田昭夫.md "wikilink")，索尼跨海併購CBS旗下的[電影與音樂公司](../Page/哥倫比亞電影公司.md "wikilink")，並在同年將旗下日本國內的音樂關聯企業全部併入CBS/Sony。索尼在美國所併購的CBS音樂部門（CBS唱片公司），於1991年更名為[索尼音樂娛樂](../Page/索尼音樂娛樂.md "wikilink")（Sony
Music），負責經營索尼在日本以外的全球音樂事業。

1991年4月，CBS/Sony改用現名（索尼音樂娛樂，下稱SME），並於同年11月在[東證二部](../Page/東京證券交易所.md "wikilink")（中小企業板）上市。2000年，SME與索尼交換股權，成為後者的全資子公司，並自東證二部下市。2001年，SME將旗下音樂品牌與音樂製作部門分拆為獨立公司，改以[控股公司形式營運](../Page/控股公司.md "wikilink")。2003年4月1日，SME進行企業重組，公司更名為「株式會社SMEJ」，並同時成立索尼音樂娛樂（SME新公司法人）、索尼文化娛樂（簡稱SCU）兩家子公司；株式會社SMEJ則於同年7月1日併入索尼母公司，使上述兩家公司成為索尼的「親公司」，擁有完整的控制權。2006年12月，索尼文化娛樂併入索尼音樂娛樂。

[SME_Nogizaka_Building.jpg](https://zh.wikipedia.org/wiki/File:SME_Nogizaka_Building.jpg "fig:SME_Nogizaka_Building.jpg")
由於股權分開的緣故，索尼音樂的全球體系在2000年後的一系列企業整併，與日本索尼音樂集團沒有直接關係。索尼音樂的全球體系與[博德曼音樂](../Page/贝塔斯曼音乐集团.md "wikilink")（BMG）在2004年11月合併為[索尼博德曼時](../Page/索尼博德曼.md "wikilink")，BMG的日本子公司「BMG
Fun House」直屬於全球的索尼博德曼體系，隔年更名為「日本博德曼音樂」（BMG
JAPAN）。2008年8月5日，索尼宣布收購[博德曼所持有的索尼博德曼](../Page/博德曼.md "wikilink")50%股權，並於10月1日正式生效，索尼博德曼成為美國索尼的全資子公司，並更名回「索尼音樂娛樂」；日本博德曼音樂撥歸日本索尼音樂集團擁有及管理\[1\]，之後因公司部門分割重組而在2009年10月解散。

2017年10月，日本索尼音乐宣布成立游戏发行公司“[Unties](../Page/Unties.md "wikilink")”，并以此品牌在[PlayStation
4](../Page/PlayStation_4.md "wikilink")、[PlayStation
VR](../Page/PlayStation_VR.md "wikilink")，甚至[任天堂Switch和PC上发行游戏](../Page/任天堂Switch.md "wikilink")，发行的作品以独立游戏为主\[2\]。

## 現轄子公司

備註：下列有標註“※”的公司為舊索尼文化娛樂股份公司旗下子公司。

### 品牌事業群

  - [索尼音樂品牌](../Page/#索尼音樂品牌.md "wikilink")

  - （由前索尼音樂之家（）改制而成。主要負責老作品的再版及郵購，有時也以“”作為廠牌。其產品使用MH為編號，意為。）

#### 索尼音樂品牌

2014年4月1日，索尼音樂娛樂旗下8家音樂廠牌整合並重組為索尼音樂品牌公司，以取代原來的索尼音樂唱片公司（）。

##### 旗下廠牌

  - [Sony Music Records](../Page/Sony_Music_Records.md "wikilink")
    前身為1968年3月由索尼與CBS合併而成的“CBS/Sony公司”。2001年10月被索尼音樂娛樂所成立的同名製作公司合併。

<!-- end list -->

  - [SME Records](../Page/SME_Records.md "wikilink")
    1998年成立，業務涵蓋J-Pop的偶像派及實力派歌手。原為索尼音樂唱片旗下子廠牌，2003年4月成為獨立廠牌。

<!-- end list -->

  - [SACRA MUSIC](../Page/SACRA_MUSIC.md "wikilink")
    2017年成立。主要針對動畫主題曲，以及那些不僅活躍在日本同時也活躍在海外藝人的唱片。

<!-- end list -->

  -
    2009年成立。由原[博德曼日本在日本國內業務改組而成](../Page/BMG_JAPAN.md "wikilink")，主要承擔J-Pop中的個性派藝人作品的發行。

<!-- end list -->

  - [史詩唱片日本](../Page/史詩唱片日本.md "wikilink")（EPIC Records Japan）
    1978年成立。前身為史詩索尼唱片。主要承擔J-Pop中的實力派藝人作品的發行。

<!-- end list -->

  - 史詩JYP（Epic JYP）
    主要承擔[JYP娛樂旗下藝人作品的發行](../Page/JYP娛樂.md "wikilink")。

<!-- end list -->

  -
    1992年成立。前身為Ki/oon Sony，主要承擔獨立音樂及搖滾音樂藝人的作品發行。

<!-- end list -->

  - [Sony Music Associated
    Records](../Page/Sony_Music_Associated_Records.md "wikilink")
    1998年成立，業務涵蓋[J-Pop的偶像派及實力派歌手](../Page/J-Pop.md "wikilink")。同時也涉及跨界藝人的作品。

<!-- end list -->

  - 日本索尼音樂國際（Sony Music Japan International）
    2001年成立。主要負責在日本發行海外音樂。

<!-- end list -->

  - （Sony Music Marketing Inc.）
    2001年成立。負責娛樂軟件的發行與銷售。

### 視聽事業群

  - [Aniplex股份公司](../Page/Aniplex.md "wikilink")※
      - [A-1 Pictures](../Page/A-1_Pictures.md "wikilink")※
      - Quatro A※
      - Aniplex of America※

<!-- end list -->

  - ※

### 網媒事業群

  - （負責日本專業音樂頻道“”的運營事務）※

  -
## 參考資料

## 相關條目

  - [索尼音樂娛樂](../Page/索尼音樂娛樂.md "wikilink")（全球的索尼音樂事業體系）

## 外部連結

  -
  - [日本索尼音樂企業網站](https://www.sme.co.jp)

[日本索尼音樂娛樂](../Category/日本索尼音樂娛樂.md "wikilink")
[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")
[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:日本藝人經紀公司](../Category/日本藝人經紀公司.md "wikilink")
[S](../Category/日本控股公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:国际唱片业协会成员](../Category/国际唱片业协会成员.md "wikilink")
[Category:1968年建立](../Category/1968年建立.md "wikilink")

1.  <http://www.sony.co.jp/SonyInfo/IR/news/qfhh7c00000jbz2m-att/081002_SMEJ.pdf>
2.