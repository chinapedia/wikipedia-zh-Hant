[Nikkei-Building-01.jpg](https://zh.wikipedia.org/wiki/File:Nikkei-Building-01.jpg "fig:Nikkei-Building-01.jpg")
[Nikkei-Osaka-head-office-01.jpg](https://zh.wikipedia.org/wiki/File:Nikkei-Osaka-head-office-01.jpg "fig:Nikkei-Osaka-head-office-01.jpg")
《**日本經濟新聞**》（），簡稱《**日經**》或《**日經新聞**》，是[日本具有相當影響力的全國性的大](../Page/日本.md "wikilink")[報紙之一](../Page/報紙.md "wikilink")；創刊于1876年12月2日。而出版該報的[日本經濟新聞社](../Page/日本經濟新聞社.md "wikilink")，則負責算出[東京證券交易所的](../Page/東京證券交易所.md "wikilink")[日經平均股票價格](../Page/日經平均股票價格.md "wikilink")（Nikkei225），是全球主要的股市指標之一。該社亦同時是一間綜合媒體集團，經營[東京電視台](../Page/東京電視台.md "wikilink")、[日經CNBC以及](../Page/日經CNBC.md "wikilink")[日經廣播電台](../Page/日經廣播電台.md "wikilink")。

## 沿革

  - 1876年12月2日：[三井物產發行的週刊](../Page/三井物產.md "wikilink")《中外物價新報》創刊。
  - 1885年7月：改為日刊（星期日、節日翌日停刊）。
  - 1889年1月：更名為《[中外商業新報](../Page/中外商業新報.md "wikilink")》。
  - 1920年1月：紐約設置特派員事務所。
  - 1924年10月：[夕刊創刊](../Page/晚报.md "wikilink")。
  - 1942年11月1日：根據[日本政府的報紙合併令](../Page/日本.md "wikilink")，東日本的經濟報紙合併，更名為《日本產業經濟》。
  - 1946年3月1日：更名為《日本經濟新聞》。
  - 1966年12月：創刊90周年。
  - 1996年12月：創刊120周年。
  - 2015年7月：收購英國[金融時報](../Page/金融時報.md "wikilink")。\[1\]

## 论调

《日本經濟新聞》以財經新聞爲主，也是日本公認在[政治](../Page/政治.md "wikilink")、[經濟方面相對權威](../Page/經濟.md "wikilink")，政治[派系色彩較淡的報紙](../Page/派系.md "wikilink")，被认为属于[中间派](../Page/中间派.md "wikilink")。该报反对日本首相参拜[靖国神社](../Page/靖国神社.md "wikilink")\[2\]。相較日本其他媒體，《日本經濟新聞》更關注[中國快速增長的經濟狀況](../Page/中國.md "wikilink")。日本經濟新聞社在中國設有4處支局，分別是：[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[重慶](../Page/重慶.md "wikilink")、[廣州](../Page/廣州.md "wikilink")。

日本[京都大学有研究指出该报论调倾向于](../Page/京都大学.md "wikilink")[新自由主义](../Page/新自由主义.md "wikilink")\[3\]，经常直接提出建议。对于政府干预企业运营较为反对，曾发报道质疑安倍经济学中政府要求企业加薪的行为以及量化宽松是否有效\[4\]。

《日本經濟新聞》較少刊登中國的[民主化和追求](../Page/民主化.md "wikilink")[自由的文章](../Page/自由.md "wikilink")。[法國國際廣播電台也稱](../Page/法國國際廣播電台.md "wikilink")《日本經濟新聞》「注重經濟而代表日本經濟界[親中立場](../Page/親中.md "wikilink")」\[5\]。

## 本社・支社

  - 本社
    [東京本社](../Page/東京.md "wikilink")：[東京都](../Page/東京都.md "wikilink")[千代田區大手町一丁目](../Page/千代田區.md "wikilink")3番7号
    [大阪本社](../Page/大阪.md "wikilink")：[大阪市](../Page/大阪市.md "wikilink")[中央區大手前一丁目](../Page/中央區_\(大阪市\).md "wikilink")1番1号
  - 支社
    [名古屋支社](../Page/名古屋.md "wikilink")：[名古屋市](../Page/名古屋市.md "wikilink")[中區榮四丁目](../Page/中區_\(名古屋市\).md "wikilink")16番33号
    [西部支社](../Page/西日本.md "wikilink")：[福岡市](../Page/福岡市.md "wikilink")[博多區博多駅東二丁目](../Page/博多區.md "wikilink")16番1号
    [札幌支社](../Page/札幌.md "wikilink")：[札幌市](../Page/札幌市.md "wikilink")[中央區北一条西六丁目](../Page/中央區_\(札幌市\).md "wikilink")1番2号
    [神戶支社](../Page/神戶.md "wikilink")：[神戶市](../Page/神戶市.md "wikilink")[中央區下山手通七丁目](../Page/中央區_\(神戶市\).md "wikilink")1番24号
    [京都支社](../Page/京都.md "wikilink")：[京都市](../Page/京都市.md "wikilink")[中京區烏丸通竹屋町角](../Page/中京區.md "wikilink")

### 對象地域

  - [東京本社](../Page/東京.md "wikilink")：[關東](../Page/關東.md "wikilink")、[东北](../Page/东北地方.md "wikilink")、[甲信越](../Page/甲信越.md "wikilink")、[靜岡縣](../Page/靜岡縣.md "wikilink")
  - [大阪本社](../Page/大阪.md "wikilink")：[近畿](../Page/近畿.md "wikilink")（含[三重縣的](../Page/三重縣.md "wikilink")[伊賀與](../Page/伊賀.md "wikilink")[熊野](../Page/熊野.md "wikilink")）、[北陸三縣](../Page/北陸地方.md "wikilink")、[中國](../Page/中國地方.md "wikilink")（不含山口縣一部份）、[四國](../Page/四國.md "wikilink")
  - [名古屋支社](../Page/名古屋.md "wikilink")：[愛知縣](../Page/愛知縣.md "wikilink")、[岐阜縣](../Page/岐阜縣.md "wikilink")、[三重縣](../Page/三重縣.md "wikilink")（不含[伊賀與](../Page/伊賀.md "wikilink")[熊野](../Page/熊野.md "wikilink")）
  - [西部支社](../Page/西部.md "wikilink")：[九州](../Page/九州.md "wikilink")、[沖繩](../Page/沖繩.md "wikilink")、[山口縣一部份](../Page/山口縣.md "wikilink")
  - [札幌支社](../Page/札幌.md "wikilink")：[北海道](../Page/北海道.md "wikilink")

## 重大事件

### 激進人士投瓶事件

2006年7月21日，[日本民族主義激進派人士](../Page/日本民族主義.md "wikilink")[平岡元秀向日本經濟新聞社東京本社](../Page/平岡元秀.md "wikilink")[大樓投擲](../Page/大樓.md "wikilink")[燃燒瓶](../Page/燃燒瓶.md "wikilink")，原因是《日本經濟新聞》刊登了有關已故[昭和天皇不滿](../Page/昭和天皇.md "wikilink")[靖國神社合祭](../Page/靖國神社.md "wikilink")[二戰](../Page/二戰.md "wikilink")[甲級戰犯的](../Page/甲級戰犯.md "wikilink")「****」。2007年4月18日，東京[警視廳公安部以](../Page/警視廳.md "wikilink")「違反《燃燒瓶處罰法》」的罪名逮捕平岡。日本[共同社報道時援引平岡的話說](../Page/共同社.md "wikilink")，平岡想警告日本經濟新聞社「不許拿[天皇來操縱靖國神社話題](../Page/天皇.md "wikilink")」\[6\]。<small>（）</small>

該事件也導致日本在[無國界記者組織公布的](../Page/無國界記者.md "wikilink")「2006年[全球新聞自由指數](../Page/全球新聞自由指數.md "wikilink")」中，。

## 參考資料

## 外部連結

  - [日本經濟新聞](http://www.nikkei.co.jp)
  - [日经中文网--日本经济新闻中文版](http://cn.nikkei.com/)
  - [金融時報中文網](http://www.ftchinese.com/)

[\*](../Category/日本經濟新聞.md "wikilink")
[Category:日本經濟](../Category/日本經濟.md "wikilink")
[Category:日本報紙](../Category/日本報紙.md "wikilink")
[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:菊池宽奖获奖者](../Category/菊池宽奖获奖者.md "wikilink")
[Category:財經報紙](../Category/財經報紙.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:1876年建立的出版物](../Category/1876年建立的出版物.md "wikilink")
[Category:1876年日本建立](../Category/1876年日本建立.md "wikilink")

1.

2.  [日经中文网-2013/12/27-社评：参拜靖国神社对日本无益](http://cn.nikkei.com/columnviewpoint/criticism/7509-20131227.html)

3.
4.

5.

6.