**日本航空**（，英語譯名：），簡稱**日航**（）、或（），是[日本的](../Page/日本.md "wikilink")[國家航空公司](../Page/國家航空公司.md "wikilink")，\[1\]同時為[寰宇一家成員之一](../Page/寰宇一家.md "wikilink")\[2\]。總部設於[東京](../Page/東京.md "wikilink")[品川區](../Page/品川區.md "wikilink")\[3\]\[4\]，原為日本規模最大的[航空公司](../Page/航空公司.md "wikilink")，但在2010年1月申請[破產保護後被](../Page/破產保護.md "wikilink")[全日空超越](../Page/全日本空輸.md "wikilink")，目前仍與全日空並列為日本兩大航空公司。2018年7月17日，獲[Skytrax評定為五星級航空公司](../Page/Skytrax.md "wikilink")。

日本航空於1951年8月创立、並於同年10月開始經營國內定期航線；至1953年10月1日，改制為（[日本政府特別以專法成立的](../Page/日本政府.md "wikilink")[國有企業](../Page/國有企業.md "wikilink")）\[5\]。1954年，開辦了第一條往[美國的跨](../Page/美國.md "wikilink")[太平洋國際航線](../Page/太平洋.md "wikilink")。經過30年的擴展，在1987年實現完全[民營化](../Page/民營化.md "wikilink")。2002年，與當時日本第三大航空公司[日本佳速航空合併](../Page/日本佳速航空.md "wikilink")。其安全措施亦為[國際航空運輸協會所認可](../Page/國際航空運輸協會.md "wikilink")\[6\]。2008年，日本航空每年乘客量為5000萬人次\[7\]。

日本航空現使用[波音767-300ER](../Page/波音767-300ER.md "wikilink")、[波音777-200ER](../Page/波音777-200ER.md "wikilink")、[波音777-300ER](../Page/波音777-300ER.md "wikilink")、[波音787等作中長程國際飛行](../Page/波音787.md "wikilink")；短程及內陸則使用[波音737](../Page/波音737.md "wikilink")、767及787。集團內另有1間廉價航空公司：[捷星日本航空](../Page/捷星日本航空.md "wikilink")（Jetstar
Japan）及4間營運國內線的航空公司：[J-Air](../Page/J-Air.md "wikilink")、[日本空中通勤](../Page/日本空中通勤.md "wikilink")（JAC）、[日本越洋航空](../Page/日本越洋航空.md "wikilink")（JTA）及[琉球空中通勤](../Page/琉球空中通勤.md "wikilink")（RAC），提供接駁服務及往次要航點的短途航班\[8\]。

## 歷史

[JAL_Aircraft_Mokusei-go.JPG](https://zh.wikipedia.org/wiki/File:JAL_Aircraft_Mokusei-go.JPG "fig:JAL_Aircraft_Mokusei-go.JPG")
[DC-6_Japan_Air_Lines_San_Francisco_(4762839480).jpg](https://zh.wikipedia.org/wiki/File:DC-6_Japan_Air_Lines_San_Francisco_\(4762839480\).jpg "fig:DC-6_Japan_Air_Lines_San_Francisco_(4762839480).jpg")
[DC-8_Japan_Airlines.JPG](https://zh.wikipedia.org/wiki/File:DC-8_Japan_Airlines.JPG "fig:DC-8_Japan_Airlines.JPG")—[道格拉斯DC-8](../Page/道格拉斯DC-8.md "wikilink")\]\]
[Boeing_747-146,_Japan_Air_Lines_-_JAL_AN0660216.jpg](https://zh.wikipedia.org/wiki/File:Boeing_747-146,_Japan_Air_Lines_-_JAL_AN0660216.jpg "fig:Boeing_747-146,_Japan_Air_Lines_-_JAL_AN0660216.jpg")（第一代塗裝）\]\]
[Japan_Airlines_McDonnell_Douglas_MD-11_Monty.jpg](https://zh.wikipedia.org/wiki/File:Japan_Airlines_McDonnell_Douglas_MD-11_Monty.jpg "fig:Japan_Airlines_McDonnell_Douglas_MD-11_Monty.jpg")（第二代塗裝）\]\]
[Jal.747.newcolours.arp.750pix.jpg](https://zh.wikipedia.org/wiki/File:Jal.747.newcolours.arp.750pix.jpg "fig:Jal.747.newcolours.arp.750pix.jpg")

[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[日本戰敗後](../Page/日本戰敗.md "wikilink")，日本政府意識到有需要開設一間可靠的航空公司來協助日本重新增長，因此，於1951年8月成立。同年10月25日，日航使用3架[西北航空的](../Page/西北航空.md "wikilink")飛機及西北航空職員開始服務[東京至數個國內城市的航線](../Page/東京.md "wikilink")\[9\]。

1953年8月1日，[日本國會通過](../Page/日本國會.md "wikilink")《日本航空株式會社法》，成立新的國有航空公司**日本航空**（），並承繼之前的所有資產。日航的第一架飛機是從[菲律賓航空租賃的](../Page/菲律賓航空.md "wikilink")[道格拉斯DC-3](../Page/道格拉斯DC-3.md "wikilink")，名為「」。1950年代，日航共使用、[道格拉斯DC-3](../Page/道格拉斯DC-3.md "wikilink")、[道格拉斯DC-4](../Page/道格拉斯DC-4.md "wikilink")、[道格拉斯DC-6及](../Page/道格拉斯DC-6.md "wikilink")[道格拉斯DC-7](../Page/道格拉斯DC-7.md "wikilink")。

1954年2月2日，日航開辦第一條國際航線，來往[東京及](../Page/東京.md "wikilink")[三藩市](../Page/三藩市.md "wikilink")。\[10\]
使用[道格拉斯DC-6B飛行](../Page/道格拉斯DC-6.md "wikilink")，中途停[威克島及](../Page/威克島.md "wikilink")[檀香山](../Page/檀香山.md "wikilink")，單程機票價格為[USD](../Page/USD.md "wikilink")650，每兩週一班，航班號碼為001。翌年[香港和](../Page/香港.md "wikilink")[台北同時成為了日航首個](../Page/台北.md "wikilink")[亞洲地區航點](../Page/亞洲.md "wikilink")。1960年接收了首架[噴射機](../Page/噴射機.md "wikilink")[道格拉斯DC-8](../Page/道格拉斯DC-8.md "wikilink")\[11\]。不久，日航就開辦了多條國際航線\[12\]。

1972年，在「」下，日本政府頒布了所謂的「航空法」，日航獲准以[國家航空公司的名義開辦國際航線](../Page/國家航空公司.md "wikilink")，並被委派開辦國內支線與[全日本空輸競爭](../Page/全日本空輸.md "wikilink")。在這年代，日航航線不斷增加，因而購買了[波音747](../Page/波音747.md "wikilink")、[波音727及](../Page/波音727.md "wikilink")[道格拉斯DC-10以應付需求](../Page/道格拉斯DC-10.md "wikilink")\[13\]。日航第一架波音747於1970年4月22日交付，同年7月1日投放檀香山航線。1970年代，日航開始增加推銷活動，如大量製造[飛機模型及其他促銷品](../Page/比例模型#飛機模型.md "wikilink")。日航亦購買了新的[波音767並將舊DC](../Page/波音767.md "wikilink")-8及波音727退役。

在1965年之前，日航超過一半的收入都是來自往美國的航線，因此日航向美國加強游說給予往[東岸的跨](../Page/美國東岸.md "wikilink")[大西洋航線的](../Page/大西洋.md "wikilink")[航權](../Page/航權.md "wikilink")\[14\]。1978年開辦經[安克雷奇往](../Page/安克雷奇.md "wikilink")[巴西](../Page/巴西.md "wikilink")[聖保羅及](../Page/聖保羅_\(巴西\).md "wikilink")[里約熱內盧的航班](../Page/里約熱內盧.md "wikilink")，在1980年代中途站改為[洛杉磯](../Page/洛杉磯.md "wikilink")\[15\]，之後再改為[紐約](../Page/紐約.md "wikilink")[甘迺迪國際機場](../Page/甘迺迪國際機場.md "wikilink")\[16\]。

1970年代末，日本開始考慮取消管制規定，隨後在1985年，日本政府宣佈取消「」。1987年，日航被完全私有化，日本另外兩間航空公司：[全日空及](../Page/全日空.md "wikilink")[日本佳速航空獲准自由開辦任何航線與日航競爭](../Page/日本佳速航空.md "wikilink")\[17\]。由於競爭增加，日航的公司架構亦有所轉變；日航重整為三個分部：國際客運、國內客運及貨運（包括郵件）服務\[18\]。

1990年代初，[波斯灣戰爭開始前](../Page/波斯灣戰爭.md "wikilink")，日航加派航班協助日本侨民撤離[伊朗](../Page/伊朗.md "wikilink")。1992年，日航開辦了「[日航包機](../Page/日線航空.md "wikilink")」，1997年，[華特迪士尼公司宣佈日航為](../Page/華特迪士尼公司.md "wikilink")[東京迪士尼樂園的指定航空公司](../Page/東京迪士尼樂園.md "wikilink")。同年，[日本航空快運成立](../Page/日本航空快運.md "wikilink")，主要使用[波音737客機](../Page/波音737.md "wikilink")。1990年代，美國及英國經濟衰退，使日航遭遇經濟困難。\[19\]
自1986年，日航每年一直有盈利，直到1992年開始虧損。日航開始實行節流措施，包括設立[廉價航空公司日本航空快運及將度假遊客轉往](../Page/廉價航空公司.md "wikilink")[日線航空](../Page/日線航空.md "wikilink")（前身「日航包機」）。這些措施使日航於1999年轉虧為盈\[20\]。1990年代，日航訂購更多[波音777客機翻新機隊](../Page/波音777.md "wikilink")。日航是波音777「Working
Together」開發計劃八位成員之一，為設計提供建議\[21\]。

2001年，日航及[日本佳速航空同意合併](../Page/日本佳速航空.md "wikilink")。2002年成立了新的[控股公司](../Page/控股公司.md "wikilink")「**日本航空系統**」（，），組成了日航集團新的核心。飛機塗裝亦被更改以配合新日航集團的設計。以乘客量來計，日航當時為全球第六大\[22\]；以收入來計，日航當時為全球第三大。

2004年4月1日，日航改名為「日本國際航空」，日本佳速航空改名為「日本國內航空」\[23\]。日本佳速航空的航班號碼改為日航航班號碼，塗裝亦逐漸轉換。2004年6月26日，母公司「日本航空系統」更名為「**日本航空株式會社**」（）。合併後「日本國內航空」負責日航的國內航線網絡，「日本國際航空」則兼負國際航線及國內支線。2006年，「日本國際航空」及「日本國內航空」合併為單一品牌「日本航空」\[24\]。

2005年10月25日，日航申請加入[寰宇一家](../Page/寰宇一家.md "wikilink")[航空聯盟](../Page/航空聯盟.md "wikilink")，並於2007年4月1日正式加入。現時，日航廣泛地與其他寰宇一家成員實行[代碼共享](../Page/代碼共享.md "wikilink")，包括[美國航空](../Page/美國航空.md "wikilink")、[英國航空](../Page/英國航空.md "wikilink")、[國泰航空](../Page/國泰航空.md "wikilink")、[澳洲航空等](../Page/澳洲航空.md "wikilink")。

2008年4月1日，日航將其原有子公司[日本亞細亞航空併入主線日本航空](../Page/日本亞細亞航空.md "wikilink")。在1975年至2008年間，由於[台灣的](../Page/台灣.md "wikilink")[政治形勢](../Page/台日關係.md "wikilink")，日本亞細亞航空專門飛行日本至台灣的航線\[25\]。

2009年11月13日，日航2009年第二季的盈利報告和記錄達1,312億日元[赤字](../Page/赤字.md "wikilink")。傳出日航將會申請破產的消息，其聯盟[寰宇一家宣佈一系列拯救行動](../Page/寰宇一家.md "wikilink")，同時[天合聯盟的](../Page/天合聯盟.md "wikilink")[達美航空亦提出拯救日航的消息](../Page/達美航空.md "wikilink")，令[美國航空將提供日航的資金提升到](../Page/美國航空.md "wikilink")14億美元。2010年1月19日，日航向[東京地方裁判所申請](../Page/東京地方裁判所.md "wikilink")[破產保護](../Page/破產保護.md "wikilink")。2月9日，日航宣佈將加強與美國航空的合作關係，並將共同申請美日航線的反壟斷豁免權\[26\]\[27\]。日本政府持有的企業再生支援機構（ETIC）於2010年向日航注資3500億日圓。

在进行经营重组後，日航自2011年4月1日起\[28\]重新啟用“红鹤”标志。新標誌將對舊標誌的“JAL”三個字母的字体進行改動，同時，机身侧面將用黑色粗体字寫上日航英文名。第一架使用新標誌的飛機被命名為「红鹤1号」（），機型为[波音767-346ER](../Page/波音767.md "wikilink")（編號JA654J），於2月28日下午首航执飞羽田—北海道钏路航线。日航旗下的飛機標誌將逐步更換，全部更換估計將需要8年的時間\[29\]。

2011年3月1日，日航的波音747客機正式退役。自1970年啟用的波音747客機，最高峰期間（1994－2001年）曾有80架飛機在運營，後由於公司經營情況惡化、飛機油耗過大等問題而被迫退役。日航最後一班使用波音747執飛的國際航班是2月28日從成田機場飛往夏威夷[檀香山的](../Page/檀香山.md "wikilink")**日本航空76號班機**\[30\]。

2011年3月28日，東京地方裁判所结束对日本航空長達14個月的破产保护程序\[31\]。同年7月，日航宣佈與[澳洲航空合作](../Page/澳洲航空.md "wikilink")，組建[捷星日本航空](../Page/捷星日本航空.md "wikilink")。

持有日本航空達96.5%股權的企業再生支援機構（ETIC），以每股3790日圓，出售1.75億股股份，籌集6630億日圓（約663億港元），以退出在日航的投資。日航擬於2012年9月10日定價，並於19日在東京證券交易所上市。

2017年，日本航空意外遭到以租借[波音777-300ER为理由的电信诈骗而出现大面积亏损](../Page/波音777.md "wikilink")，这起电信诈骗导致日航自2009年后第二次最终财报出现赤字。

2018年，日本航空正式獲[Skytrax评为五星級航空公司](../Page/Skytrax.md "wikilink")，成為日本第二家五星級航空公司。

## 公司組織

除日本航空的普通運作外，日航另擁有6間國內航空公司為日航提供接駁服務及往次要航點的短途航班：

  - [日本航空快運](../Page/日本航空快運.md "wikilink")（JEX）：次要城市之間的[廉價航空服務](../Page/廉價航空公司.md "wikilink")
  - [J-Air](../Page/J-Air.md "wikilink")：來往[東京](../Page/東京.md "wikilink")、[大阪](../Page/大阪.md "wikilink")、[名古屋的國內](../Page/名古屋.md "wikilink")[噴射機服務](../Page/噴射機.md "wikilink")
  - [日本空中通勤](../Page/日本空中通勤.md "wikilink")（JAC）：[西日本的螺旋槳飛機服務](../Page/西日本.md "wikilink")
  - [日本越洋航空](../Page/日本越洋航空.md "wikilink")（JTA）：[琉球群島的噴射機服務](../Page/琉球群島.md "wikilink")
  - [琉球空中通勤](../Page/琉球空中通勤.md "wikilink")（RAC）：[琉球群島的的螺旋槳飛機服務](../Page/琉球群島.md "wikilink")

於1962年成立，是日航一間提供膳食服務的子公司，業務包括「De sky」生產小食、供應日航「Blue
Sky」餐廳及「JAL-DFS」商店、飛機燃料組件、機內服務及機上免稅銷售服務。2004年1月，JALUX與JAS
Trading合併以統一日航集團的物資供應。

曾是日本航空貨運服務的品牌，為[WOW航空聯盟成員之一](../Page/WOW航空聯盟.md "wikilink")。2006年在國內運載了338,443貨物及85,519tkm郵件；國際運載了4,541,293tkm貨物及161,690tkm郵件\[32\]。日航貨運已於2010年10月終止服務，所有貨機亦已離開日航機隊，貨物改由客機負責運載。

<File:Old> JAL logo
2.svg|日本航空第二代「紅鶴」（鶴丸）標誌（1959至1989年），2011年起再度使用，但標誌字體有分別
[File:Japan_Airlines_Logo_(1989_-_2002).svg|日本航空第三代標誌（1989至2002年](File:Japan_Airlines_Logo_\(1989_-_2002\).svg%7C日本航空第三代標誌（1989至2002年)），與「紅鶴」標誌並用
<File:JAL> logo.svg|日本航空第四代標誌「The Arc of the Sun」（2002年-2011年）
<File:Jal.cargo.b747-400.ja8909.arp.jpg>|的[波音747](../Page/波音747.md "wikilink")-400BCF
<File:JAL> B777-200 JA8984 ITM
20080920-001.jpg|日航宣揚環保的「」（JA8984與JA734J現改為新版Sky
Eco塗裝\[33\]） <File:Boeing> 767-300ER (Japan Airlines)
01.jpg|日本航空在[女滿別機場的](../Page/女滿別機場.md "wikilink")[波音767](../Page/波音767.md "wikilink")-300
啟用“红鹤”标志 <File:JAL> Express 737-336 KIX
JA8993.jpg|[日本航空快運在](../Page/日本航空快運.md "wikilink")[關西國際機場的](../Page/關西國際機場.md "wikilink")[波音737](../Page/波音737.md "wikilink")-400
<File:JAC-SAAB340B-JA8900-01.jpg>|[日本空中通勤](../Page/日本空中通勤.md "wikilink")
<File:Japan> Transocean 737-429 OHA
JA8933.jpg|[日本越洋航空](../Page/日本越洋航空.md "wikilink")

### 歷任經營者

[Noboru_Takeshita_full.jpg](https://zh.wikipedia.org/wiki/File:Noboru_Takeshita_full.jpg "fig:Noboru_Takeshita_full.jpg")(JA8542)执行日本首相[竹下登访美专机任务](../Page/竹下登.md "wikilink")\]\]

| 日本航空成立以来歷代經營陣 |
| ------------- |
| 就任年月          |
| 1951年8月       |
| 1953年10月      |
| 1961年1月       |
| 1963年5月       |
| 1969年5月       |
| 1971年5月       |
| 1973年5月       |
| 1977年6月       |
| 1979年6月       |
| 1981年6月       |
| 1983年6月       |
| 1985年12月      |
| 1986年6月       |
| 1988年6月       |
| 1990年6月       |
| 1991年6月       |
| 1995年6月       |
| 1998年6月       |
| 2004年4月       |
| 2006年6月       |
| 2010年2月       |
| 2012年2月       |
|               |

#### 歷任會長（董事長）

  - 藤山愛一郎（1951年8月－1953年10月）
  - 原邦造（1953年10月－1961年1月）
  - 植村甲午郎（1963年5月－1969年5月）
  - 伍堂輝雄（1969年5月－1971年5月）
  - 松尾靜麿（1971年5月－1973年5月）
  - 小林中（1973年5月－1977年6月）
  - 植村甲午郎（1977年6月－1979年6月）
  - 堀田莊三（1979年6月－1981年6月）
  - 花村仁八郎（1983年6月－1986年6月）
  - 伊藤淳二（1986年6月－1988年6月）
  - 渡辺文夫（1988年6月－1991年6月）
  - 山地進（1991年6月－1998年6月）
  - 兼子勳（2004年4月－2006年6月）

#### 歷任社長（總裁）

  - 柳田誠二郎（1951年8月－1961年1月）
  - 松尾静麿（1961年1月－1971年5月）
  - 朝田靜夫（1971年1月－1981年6月）
  - 高木養根（1981年6月－1985年12月）
  - 山地進（1985年12月－1990年6月）
  - 利光松男（1990年6月－1995年6月）
  - 近藤晃（1995年6月－1998年6月）
  - 兼子勲（1998年6月－2004年4月）
  - 新町敏行（2004年4月－2006年6月）
  - 西松遙（2006年6月－2010年2月）

## 航點

[JL-JapanAirlines-World-Dest.svg](https://zh.wikipedia.org/wiki/File:JL-JapanAirlines-World-Dest.svg "fig:JL-JapanAirlines-World-Dest.svg")
[Kansai_International_Airport.jpg](https://zh.wikipedia.org/wiki/File:Kansai_International_Airport.jpg "fig:Kansai_International_Airport.jpg")的登機櫃檯\]\]

日本航空現時航點覆蓋[亞洲](../Page/亞洲.md "wikilink")、[美洲](../Page/美洲.md "wikilink")、[歐洲及](../Page/歐洲.md "wikilink")[大洋洲](../Page/大洋洲.md "wikilink")。日航四大[樞紐分別是](../Page/樞紐機場.md "wikilink")[東京國際機場](../Page/東京國際機場.md "wikilink")、[大阪國際機場](../Page/大阪國際機場.md "wikilink")(國內線)，[成田國際機場](../Page/成田國際機場.md "wikilink")、[關西國際機場](../Page/關西國際機場.md "wikilink")(國際線)。包括日航6間子公司的廣大國內網絡及代碼共享航班，在日本國內共有39個航點\[34\]。

近年，由於與[寰宇一家其他航空公司實行](../Page/寰宇一家.md "wikilink")[-{A](../Page/代碼共享.md "wikilink")，日航減少了往各大[洲次要機場的航班](../Page/洲.md "wikilink")。1980年代，日航在[中東有很廣大的航點網絡](../Page/中東.md "wikilink")，如[開羅](../Page/開羅國際機場.md "wikilink")、[巴林](../Page/巴林國際機場.md "wikilink")、[科威特](../Page/科威特國際機場.md "wikilink")、[阿布達比等](../Page/阿布達比國際機場.md "wikilink")，但現時服務已中止。日航曾是5間有航班往[拉丁美洲的亞洲航空公司之一](../Page/拉丁美洲.md "wikilink")<span style="font-size:smaller;">（另外四間為[中國國際航空](../Page/中國國際航空.md "wikilink")、[阿聯酋航空](../Page/阿聯酋航空.md "wikilink")、[馬來西亞航空及](../Page/馬來西亞航空.md "wikilink")[大韓航空](../Page/大韓航空.md "wikilink")）</span>，包括經[紐約往來](../Page/肯尼迪國際機場.md "wikilink")[聖保羅的航線](../Page/聖保羅/瓜魯柳斯-安德烈·弗朗哥·蒙托羅州長國際機場.md "wikilink")，但已於2010年9月30日停止服務。

在[波音787交付之後](../Page/波音787.md "wikilink")，日航已於2012年4月22日開航[東京-成田到](../Page/成田國際機場.md "wikilink")[波士頓的航線](../Page/洛根國際機場.md "wikilink")。

## 機隊

[Japan_Air_Lines_Boeing_747-246B_(JA8162_581_22991)_(8276881426).jpg](https://zh.wikipedia.org/wiki/File:Japan_Air_Lines_Boeing_747-246B_\(JA8162_581_22991\)_\(8276881426\).jpg "fig:Japan_Air_Lines_Boeing_747-246B_(JA8162_581_22991)_(8276881426).jpg")”（Executive
Express）\]\]
[JA732J@LHR_27AUG13_(11351866496).jpg](https://zh.wikipedia.org/wiki/File:JA732J@LHR_27AUG13_\(11351866496\).jpg "fig:JA732J@LHR_27AUG13_(11351866496).jpg")彩繪客機\]\]
[JA708J@HKG_(20190125160547).jpg](https://zh.wikipedia.org/wiki/File:JA708J@HKG_\(20190125160547\).jpg "fig:JA708J@HKG_(20190125160547).jpg")彩繪客機\]\]
[JAL_JA772J_Boeing_777-246_Happiness_Express.jpg](https://zh.wikipedia.org/wiki/File:JAL_JA772J_Boeing_777-246_Happiness_Express.jpg "fig:JAL_JA772J_Boeing_777-246_Happiness_Express.jpg")開業30周年的“JAL
Happiness Express”彩繪\]\]
[JAL_B767-300ER(JA614J)_(6405615375).jpg](https://zh.wikipedia.org/wiki/File:JAL_B767-300ER\(JA614J\)_\(6405615375\).jpg "fig:JAL_B767-300ER(JA614J)_(6405615375).jpg")
[Boeing_777-346ER_'JA731J'_Japan_Airlines_(14070000628).jpg](https://zh.wikipedia.org/wiki/File:Boeing_777-346ER_'JA731J'_Japan_Airlines_\(14070000628\).jpg "fig:Boeing_777-346ER_'JA731J'_Japan_Airlines_(14070000628).jpg")
[JA828J@HKG_(20190411111753).jpg](https://zh.wikipedia.org/wiki/File:JA828J@HKG_\(20190411111753\).jpg "fig:JA828J@HKG_(20190411111753).jpg")起飛\]\]
[JA870J@HKG_(20180918121606).jpg](https://zh.wikipedia.org/wiki/File:JA870J@HKG_\(20180918121606\).jpg "fig:JA870J@HKG_(20180918121606).jpg")
[JAL_domestic_777_economy.jpg](https://zh.wikipedia.org/wiki/File:JAL_domestic_777_economy.jpg "fig:JAL_domestic_777_economy.jpg")

截至2018年12月，日航機隊的平均機齡10.3年，擁有下列飛機\[35\]\[36\]\[37\]：

<center>

<table>
<caption><strong>現役機隊</strong></caption>
<thead>
<tr class="header">
<th><p><font style="color:white;">機型</p></th>
<th><p><font style="color:white;">數量[38]</p></th>
<th><p><font style="color:white;">已訂購</p></th>
<th><p><font style="color:white;">選購權</p></th>
<th><p><font style="color:white;">載客量[39][40][41]</p></th>
<th><p><font style="color:white;">備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><abbr title="頭等艙"><font style="color:white;">F</abbr></p></td>
<td><p><abbr title="商務艙"><font style="color:white;">C</abbr></p></td>
<td><p><abbr title="豪華經濟艙"><font style="color:white;">W</abbr></p></td>
<td><p><abbr title="經濟艙"><font style="color:white;">Y</abbr></p></td>
<td><p><font style="color:white;">總數</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A350.md" title="wikilink">空中巴士A350-941XWB</a></p></td>
<td><p>—</p></td>
<td><p>31</p></td>
<td><p>12</p></td>
<td><p>12</p></td>
<td><p>94</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A350.md" title="wikilink">空中巴士A350-1041XWB</a></p></td>
<td><p>—</p></td>
<td><p>13</p></td>
<td><p>即將公佈</p></td>
<td><p>預計2019年6月起交付，將取代<a href="../Page/波音777.md" title="wikilink">B777-300ER</a>，為國際線專用。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-846</a></p></td>
<td><p>50</p></td>
<td><p>—</p></td>
<td><p>10</p></td>
<td><p>—</p></td>
<td><p>12<br />
20</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-346</a></p></td>
<td><p>6</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>42</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-346ER</a></p></td>
<td><p>29</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>5<br />
－<br />
－<br />
－<br />
－</p></td>
<td><p>42<br />
42<br />
30<br />
30<br />
24</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-246</a></p></td>
<td><p>12</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>14</p></td>
<td><p>82</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-246ER</a></p></td>
<td><p>11</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>56<br />
42</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-346</a></p></td>
<td><p>4</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>78</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-346ER</a></p></td>
<td><p>13</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>49</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音787.md" title="wikilink">波音787-8</a></p></td>
<td><p>25</p></td>
<td><p>4</p></td>
<td><p>20</p></td>
<td><p>—</p></td>
<td><p>30<br />
38<br />
30</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音787.md" title="wikilink">波音787-9</a></p></td>
<td><p>14</p></td>
<td><p>6</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>44<br />
52<br />
28</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三菱支線噴射機.md" title="wikilink">三菱支線噴射機MRJ90</a></p></td>
<td><p>—</p></td>
<td><p>32</p></td>
<td><p>—</p></td>
<td><p>即將公佈</p></td>
<td><p>預計2021年交付。</p></td>
</tr>
<tr class="even">
<td><p>總數</p></td>
<td><p>164</p></td>
<td><p>86</p></td>
<td><p>42</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

### 國際線機艙等級

2007年12月，日本航空於商務旅客較多的長途航線，引入特選經濟艙。現時這些航線合共提供頭等，商務艙，特選經濟艙以及經濟艙四種等別。而大部分中短距離航線，以及夏威夷與關島等長距離渡假航線，則提供商務艙以及經濟艙兩種等別。同時亦制定各機艙的屬色，頭等為紅色，商務艙為深藍色，經濟艙則為綠色，這些顏色均會應用於機場櫃檯，機票，行李標籤等地方。

#### 座椅規格

| 座椅類別      | JAL Suite | JAL Skysleeper SOLO | Shell flat seat neo | Shell flat seat | Skyrecliner | Skyluxe Seat | Millennium Version | Sky Shell Seat |
| --------- | --------- | ------------------- | ------------------- | --------------- | ----------- | ------------ | ------------------ | -------------- |
| 座距        | 211cm     | 200cm               | 153cm               | 157cm           | 129.5cm     | 119-127cm    | 112-127cm          | 97cm           |
| 後仰角度      | 180°      | 180°                | 171°（假平躺）           | 170°（假平躺）       | 132°        | 152°         | 139°               | \-             |
| 頭枕        | ○         | ○                   | ○                   | ○               | ○           | ○            | ○                  | ○              |
| 腳踏        | ○         | ○                   | ○                   | ○               | ○           | ○            | ○                  | ○              |
| 腰墊        | ○         | ○                   | ○                   | ○               | ○           | ○            | ○                  | ×              |
| 座椅殼       | ○         | ○                   | ○                   | ○               | ×           | ×            | ×                  | ○              |
| 電動輔助      | ○         | ○                   | ○                   | ○               | ×           | ×            | ×                  | ×              |
| 高檯        | ○         | ○                   | ○                   | ○               | ×           | ×            | ×                  | ×              |
| 酒吧台       | ○         | ○                   | ○                   | ○               | ○           | ×            | ×                  | ×              |
| 電腦電源      | ○         | ○                   | ○                   | ○               | ○           | ×            | △（只有738）           | ○              |
| 娛樂系統      | ○         | ○                   | ○                   | ○               | ○           | ○            | ○                  | ○              |
| 扶手下儲存空間   | ○         | ○                   | ○                   | ○               | ×           | ×            | ×                  | ×              |
| LAN線      | ×         | ×                   | ×                   | ×               | ×           | ×            | ×                  | ×              |
| USB・RCA插頭 | ×         | ×                   | △（只有787-8）          | ×               | ○           | ×            | ×                  | ×              |
| 閱讀燈       | ○         | ○                   | ○                   | ○               | ○           | ○            | ○                  | ○              |
| 配備機種      | 777-300ER | \-                  | 787-8               | 777             | 767-300ER   | 767          | 777/767/738        | 777            |

頭等・商務艙・特選經濟艙座椅規格

  - 機種簡稱：787…787-8、787-9、777…777-300 (ER) /-200 (ER)、767…767-300
    (ER)、738…[737-800](../Page/ボーイング737_ネクストジェネレーション.md "wikilink")

#### 經濟艙

經濟艙為所有艙等中除了提供最便宜的票價外，還提供「Economy
Saver」，「JAL悟空」等稱呼的常規折扣票價，而經旅行社購買亦可提供團體折扣。

所有波音777，波音767-300ER與波音737-800的座椅均配置個人娛樂系統，稱為MAGIC，乘客能夠隨時揀選喜愛的節目播放。2008年8月，例如波音777-300ER的長途航線使用的飛機引入新座位，個人螢幕擴大至9寸，而新引入的波音767-300ER，波音777-300ER（倫敦線）與波音787-8的個人螢幕則為10.6寸，新座位同時引入吊床式頭枕。座距雖然不變，但由於坐墊較薄，減低舒適性，除座椅本身向後傾外，坐墊亦會向前滑動，結果座椅成功擴大腳部空間。

<center>

[File:JAL_Economy_class_(B777-200ER).JPG|經濟客位(B777-200ER)](File:JAL_Economy_class_\(B777-200ER\).JPG%7C經濟客位\(B777-200ER\))
[File:Japan_Airlines_777-200ER_Economy_cabin.jpg|配備MAGIC娛樂系統的經濟客位(B777-200ER)](File:Japan_Airlines_777-200ER_Economy_cabin.jpg%7C配備MAGIC娛樂系統的經濟客位\(B777-200ER\))

</center>

### 已退役機隊

  - [空中巴士A300-600R](../Page/空中客车A300.md "wikilink")

  -
  - [波音727–100](../Page/波音727.md "wikilink")

  - [波音737–400](../Page/波音737.md "wikilink")

  - [波音747–100、SR/SUD、200B、300、400](../Page/波音747.md "wikilink")

  - [波音767–200](../Page/波音767.md "wikilink")

  - [波音767-300F](../Page/波音767.md "wikilink")

  - [康維爾880](../Page/康維爾880.md "wikilink")

  - [道格拉斯DC-3](../Page/道格拉斯DC-3.md "wikilink")

  - [道格拉斯DC-4](../Page/道格拉斯DC-4.md "wikilink")

  - [道格拉斯DC-6](../Page/道格拉斯DC-6.md "wikilink")

  - [道格拉斯DC-7](../Page/道格拉斯DC-7.md "wikilink")

  - [道格拉斯DC-8-30, 50, 60](../Page/道格拉斯DC-8.md "wikilink")

  - [达索猎鹰20型](../Page/达索猎鹰20型.md "wikilink")

  - [马丁2-0-2](../Page/马丁2-0-2.md "wikilink")

  - [道格拉斯DC-10-40](../Page/道格拉斯DC-10.md "wikilink")

  - [麦道MD-11](../Page/麦道MD-11.md "wikilink")

  - [麦道MD-87](../Page/麥道MD-80.md "wikilink")

  - [麦道MD-90](../Page/麥道MD-80.md "wikilink")

  - [YS-11](../Page/YS-11.md "wikilink")

  - [图-114](../Page/图-114.md "wikilink")

## 代碼共享

除[寰宇一家其他成員外](../Page/寰宇一家.md "wikilink")，日本航空亦與以下航空公司實行[代碼共享](../Page/代碼共享.md "wikilink")：

  - [法國航空](../Page/法國航空.md "wikilink")（[天合聯盟](../Page/天合聯盟.md "wikilink")）
  - 印度尼西亞鷹航空（天合聯盟）
  - [中國東方航空](../Page/中國東方航空.md "wikilink")（天合聯盟）
  - [捷星航空](../Page/捷星航空.md "wikilink")

<!-- end list -->

  - [中國南方航空](../Page/中國南方航空.md "wikilink")
  - [阿聯酋航空](../Page/阿聯酋航空.md "wikilink")
  - [大韓航空](../Page/大韓航空.md "wikilink")（天合聯盟）
  - [大溪地航空](../Page/大溪地航空.md "wikilink")

<!-- end list -->

  - 夏威夷航空
  - [中華航空](../Page/中華航空.md "wikilink")（天合聯盟）

## 工會

日本航空的工會，由地勤，維修，機師，空中服務員等各職位組成，資方派系有一個，自主派系有五個，合共六個工會。

資方派系的工會為JAL工會，採取勞資協調路線，不主張工業行動，由地勤與空中服務員組成，截至2015年3月31日，該工會有7177名會員，為現時日本航空最大工會。

自主派系有五個，分別為：

  - 日本航空工會（日本航空工會與日本航空日本工會合併，日本航空工會前身為日航工會，自1951年起為日航第一個工會）
      - 成員：地勤
      - 成員人數：472人（截至2015年3月31日）
      - 註：當時日本航空工會的小倉寛太郎，為[不沉的太陽故事中主角恩地元的原型](../Page/不沉的太陽.md "wikilink")。

<!-- end list -->

  - 日本航空機長工會（機長在日本航空體制內被歸類為管理職級）
      - 成員：機組人員
      - 成員人數：458人（截至2015年3月31日）

<!-- end list -->

  - 日本航空前任飛行工程師工會（2009年7月日本航空的波音747-300退役後，日航內已沒有需要飛行工程師的飛機）
      - 成員：地勤（前機組人員）
      - 成員人數：20人（截至2015年3月31日）

<!-- end list -->

  - 日本航空服務員工會
      - 成員：地勤，機組人員
      - 成員人數：1270人（截至2015年3月31日）

<!-- end list -->

  - 日本航空空中服務員工會
      - 成員：空中服務員
      - 成員人數：367人（截至2015年3月31日）

他們各自有自己的立場和主張，但進行勞資談判時，多數會聯合與資方對話，日本航空將這五個工會合稱為「五工會」。

## 事件

### 劫機

  - 1970年3月31日，日本航空351號班機一架[波音727](../Page/波音727.md "wikilink")-100（註冊編號：[JA8315](../Page/淀號劫機事件.md "wikilink")）從[東京往](../Page/東京.md "wikilink")[福岡](../Page/福岡.md "wikilink")，被九名[日本赤軍成員劫機](../Page/日本赤軍.md "wikilink")，四日後全機乘客及機組人員分別於[福岡機場及](../Page/福岡機場.md "wikilink")[金浦機場被安全釋放](../Page/金浦機場.md "wikilink")，劫匪最後成功流亡[北韓](../Page/北韓.md "wikilink")[平壤](../Page/平壤.md "wikilink")\[42\]。
  - 1973年7月23日，[日本航空404號班機](../Page/日本航空404號班機劫機事件.md "wikilink")，一架[波音747-246B](../Page/波音747.md "wikilink")（註冊編號：JA8109）從阿姆斯特丹經安克雷奇飛往東京，被[日本赤軍和](../Page/日本赤軍.md "wikilink")[解放巴勒斯坦人民阵线成員劫持至班加西](../Page/解放巴勒斯坦人民阵线.md "wikilink")，機上人員被釋放後，飛機被炸燬。\[43\]
  - 1977年9月28日，[日本航空472號班機](../Page/日本航空472號班機_\(1977年\).md "wikilink")，一架[道格拉斯DC-8從](../Page/道格拉斯DC-8.md "wikilink")[巴黎經](../Page/巴黎.md "wikilink")[孟買往東京](../Page/孟買.md "wikilink")，在中停地[孟買起飛後被](../Page/孟買.md "wikilink")[日本赤軍劫持至](../Page/日本赤軍.md "wikilink")[孟加拉首都](../Page/孟加拉.md "wikilink")[達卡](../Page/達卡.md "wikilink")。

### 故障

  - 1982年9月17日，[日本航空792號班機](../Page/日本航空792號班機事故.md "wikilink")，一架[道格拉斯DC-8](../Page/道格拉斯DC-8.md "wikilink")（註冊編號：JA8048）從[上海](../Page/上海.md "wikilink")[虹橋往](../Page/虹橋國際機場.md "wikilink")[東京](../Page/東京.md "wikilink")，在虹橋國際機場起飛後9分鐘出現了多個機件發出的警告，機員決定折返虹橋國際機場緊急降落，航機高速降落在36跑道並衝出跑道直到排水渠附近才停下\[44\]。
  - 1991年10月2日，日本航空10號班機（東京-芝加哥）為一架波音747-246B（註冊編號：JA8161），在東京一帶發生進氣管爆裂，機身裂開一個長1米，寬0.7米的洞，飛機放油後返航成田機場，於1992年3月修復。\[45\]
  - 2013年1月7日，日本航空的波音787-8降落后[辅助动力系统](../Page/辅助动力系统.md "wikilink")（APU）电瓶组件故障，客舱内冒出烟雾。
  - 2013年1月8日，同样是日本航空的波音787-8在地面发生燃油泄漏事故。
  - 2013年1月13日，日本航空的波音787-8在[成田國際機場检修时发生漏油](../Page/成田國際機場.md "wikilink")。
  - 2014年1月14日：日本航空的一架787客機因鋰電池滲出液體而停飛。\[46\]
  - 2014年3月8日：日本航空002號班機一架波音787-8在飛行中途儀發出警報，指右引擎可能漏油，轉降[夏威夷](../Page/夏威夷.md "wikilink")[檀香山國際機場](../Page/檀香山國際機場.md "wikilink")。
  - 2017年9月5日，由东京羽田飞往纽约肯尼迪的日本航空6号班机（波音777-300ER，注册编号：JA743J）在起飞后不久疑似遭遇鸟击，返航羽田机场。\[47\]

### 空難

  - 1952年，日本航空一架租自[西北航空的](../Page/西北航空.md "wikilink")（註冊編號：[N93043](../Page/三原山空難.md "wikilink")）從[東京往](../Page/東京.md "wikilink")[福岡](../Page/福岡.md "wikilink")，在[三原山墜毀](../Page/三原山.md "wikilink")，機上37人全部罹難\[48\]。
  - 1968年11月22日，[日本航空2號班機](../Page/日本航空2号班机事故.md "wikilink")，一架[道格拉斯DC-8](../Page/道格拉斯DC-8.md "wikilink")（註冊編號：JA8032）從[東京往](../Page/東京.md "wikilink")[三藩市](../Page/三藩市.md "wikilink")，在大[霧及其他因素影響下](../Page/霧.md "wikilink")，航機錯誤降落在距離跑道[三藩市灣水面上](../Page/三藩市灣.md "wikilink")，無人受傷\[49\]。
  - 1972年6月14日，[日本航空471號班機](../Page/日本航空471號班機空難.md "wikilink")：一架道格拉斯DC-8（註冊編號：JA8012）從[曼谷往](../Page/曼谷.md "wikilink")[德里](../Page/德里.md "wikilink")，在[亞穆納河邊墜毀](../Page/亞穆納河.md "wikilink")，機上87人中之82人罹難，包括10名機組人員及72名乘客\[50\]。
  - 1972年11月28日，[日本航空446號班機](../Page/日本航空446號班機空難.md "wikilink")，一架道格拉斯DC-8（註冊編號：JA8040）從[哥本哈根經](../Page/哥本哈根.md "wikilink")[莫斯科往東京](../Page/莫斯科.md "wikilink")，在[謝列梅捷沃國際機場起飛時失速墜毀](../Page/謝列梅捷沃國際機場.md "wikilink")，機上76人中62人遇難。\[51\]。
  - 1975年12月16日，[日本航空422號班機为一架](../Page/日本航空422號班機事故.md "wikilink")[波音747](../Page/波音747.md "wikilink")-246B（注册编号：JA8122）从巴黎经伦敦、安克雷奇飞往东京，在[泰德·史蒂文斯安克雷奇國際機場因滑行道結冰而滑出滑行道並撞上路堤](../Page/泰德·史蒂文斯安克雷奇國際機場.md "wikilink")，造成機身損壞，2人重傷，9人輕傷。
  - 1977年1月13日，[日本航空1054號班機為一架道格拉斯DC](../Page/日本航空1054號班機空難.md "wikilink")-8貨機（註冊編號：JA8054）從[安克雷奇往](../Page/安克雷奇.md "wikilink")[東京](../Page/東京.md "wikilink")，在[安克雷奇起飛不久後墜毀](../Page/安克雷奇.md "wikilink")，機上5人及56頭[牛全部罹難](../Page/牛.md "wikilink")，事後發現機長體內酒精含量超標\[52\]。
  - 1977年9月27日，[日本航空715號班機為一架道格拉斯DC](../Page/日本航空715号班机空难.md "wikilink")-8（註冊編號：JA8051）從[香港往](../Page/香港.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")，準備降落吉隆坡[梳邦國際機場時撞山墜毀](../Page/梳邦國際機場.md "wikilink")，機上79人中之34人罹難，包括8名機員及26名乘客\[53\]。
  - 1978年6月2日，[日本航空115号班机為一架波音](../Page/日本航空115号班机事故.md "wikilink")747-146SR（註冊编号：JA8119），执行从羽田飞往伊丹的JAL115航班，在大阪国际机场着陆时角度过大导致机体尾部触地（這起事故被認為是導致[日本航空123號班機空難的原因](../Page/日本航空123號班機空難.md "wikilink")）\[54\]。
  - 1982年2月9日，[日本航空350號班機](../Page/日本航空350號班機空難.md "wikilink")，一架道格拉斯DC-8（註冊編號：JA8061）由福岡飛往東京，機長蓄意將飛機撞毀，24人遇難。

[747_jal2.png](https://zh.wikipedia.org/wiki/File:747_jal2.png "fig:747_jal2.png")意想圖\]\]

  - 1985年8月12日，[日本航空123號班機](../Page/日本航空123號班機.md "wikilink")，一架波音747-100SR（註冊編號：JA8119）從[東京往](../Page/東京.md "wikilink")[大阪](../Page/大阪.md "wikilink")，在[羽田機場起飛不久後因对后端壁破损的不当修理导致垂直尾翼脱落以及液压油泄露](../Page/羽田機場.md "wikilink")，導致全機無法有效控制，航機於折返東京途中在[群馬縣高天原山墜毀](../Page/群馬縣.md "wikilink")，機上524人中之520人罹難，是航空史上單一客機傷亡最慘重的事故\[55\]。
  - 1991年7月21日，日本航空009號班機為一架載有278人從[芝加哥飛往東京的波音](../Page/芝加哥.md "wikilink")747-246B（註冊編號：JA8162），在奧黑爾機場西北約600公里的31000英呎高度（FL310）遭遇[湍流](../Page/湍流.md "wikilink")，導致1名[空中乘務員受傷](../Page/空中乘務員.md "wikilink")。\[56\]
  - 1997年6月8日，[日本航空706號班機為一架](../Page/日本航空706號班機事故.md "wikilink")[麥道MD-11](../Page/麥道MD-11.md "wikilink")（註冊編號：JA8580），由[香港飛往](../Page/香港.md "wikilink")[名古屋](../Page/名古屋.md "wikilink")，在[志摩半島上空因機師失誤而急速下降](../Page/志摩半島.md "wikilink")，造成15人受傷，其中一人於事發20個月後傷重不治。\[57\]

### 違規

  - 1982年8月19日，从羽田机场飞往千岁机场的波音747-146SR（註冊编号：JA8119）。降落时由于视野不良导致飞行员判断错误，使機體从跑道右侧滑出並讓四號引擎触地，因而重飛。在视野不良的情况下，机长仍然允许副机长操作着陆，这是违反当时日航规定的。
  - 2001年1月31日，日本航空907號班機一架[波音747](../Page/波音747.md "wikilink")-400D（註冊編號：JA8904）從[東京往](../Page/東京.md "wikilink")[沖繩及日本航空](../Page/沖繩.md "wikilink")958號班機一架[道格拉斯DC-10](../Page/道格拉斯DC-10.md "wikilink")（註冊編號：JA8546）從[釜山往東京](../Page/釜山.md "wikilink")，在[駿河灣發生空中接近事件](../Page/駿河灣.md "wikilink")，兩機在最後一刻作出迴避動作，避免了相撞，907號班機上100人受傷，958號班機上無人受傷。[JAL2001incident.png](https://zh.wikipedia.org/wiki/File:JAL2001incident.png "fig:JAL2001incident.png")意想圖\]\]

<!-- end list -->

  - 2018年11月1日，日本航空公司一名副机长在起飞前因酒精浓度超标，遭英国警方扣留，\[58\]无法登机。是一星期内日本航空公司发生的第二起机长因酒精浓度超标而影响航班，日航高管全體出面道歉，1977年日本航空1054號班機空難事後也發現機長酒精超標。\[59\]
  - 2018年11月13日，日本航空公司一架B787-9飞机在中国[上海浦东国际机场侵入](../Page/上海浦东国际机场.md "wikilink")34L跑道，导致原本使用该跑道的[达美航空DL](../Page/达美航空.md "wikilink")582航班被迫中断起飞\[60\]。

## 注释

## 參考資料

## 外部連結

  - [日本航空](https://www.jal.co.jp)
  - [日航全球](https://www.jal.com)

[日本航空_(公司)](../Category/日本航空_\(公司\).md "wikilink")
[Category:1950年成立的航空公司](../Category/1950年成立的航空公司.md "wikilink")
[Category:日本航空公司](../Category/日本航空公司.md "wikilink")
[Category:寰宇一家](../Category/寰宇一家.md "wikilink")

1.  ["Japan Air Lines Flying High on National
    Loyalty"，*[紐約時報](../Page/紐約時報.md "wikilink")*](http://select.nytimes.com/gst/abstract.html?res=F60D1EFD39551A7493CBA81789D85F478785F9)

2.  [Asia's largest airline joins oneworld
    alliance](http://news.cheapflights.com/airlines/2007/04/asias_largest_a.html)


3.
4.
5.  [Japan Airlines Company, Ltd - Company
    History](http://www.fundinguniverse.com/company-histories/Japan-Airlines-Company-Ltd-Company-History.html)

6.  [IATA website - IATA Operational Safety
    Audit](http://www.iata.org/ps/certification/iosa/operators/Japan+Airlines.htm)


7.  [JAL Traffic stats for 2009](http://www.jal.com/en/traffic/)

8.  [Oneworld Member Airlines \> Japan Airlines (JAL)
    ](http://www.oneworld.com/ow/member-airlines/jal)

9.  [Japan Airlines website -
    History](http://www.japanair.com/e/aboutjal/history.php)

10.
11.
12.
13.
14. [Bitterness in the
    Air](http://www.time.com/time/magazine/article/0,9171,842077,00.html)，*[時代雜誌](../Page/時代雜誌.md "wikilink")*

15. [Bem Vindo: JAL to inaugurate New York-Sao
    Pauloflights](http://www.allbusiness.com/transportation/air-transportation-airports/6707758-1.html)

16.
17.
18.
19.
20.
21. Birtles, Philip (1998), Motorbooks International, *Boeing 777,
    Jetliner for a New Century*. ISBN 0-7603-0581-1, p. 13-16

22. [Japan Airlines and Japan Air System Take Merger
    Move](http://query.nytimes.com/gst/fullpage.html?res=9505EED71338F930A25752C1A9679C8B63)

23. Moody's affirms JAL Int'l Ba3 rating, ups JAL Domestic rating.
    *Japan Transportation Scan.*2004-04-05

24. [JAL to merge domestic and international operations next
    year](http://atwonline.com/news/story.html?storyID=2611)

25. [Planned Integration of Japan Asia Airways with JAL
    International](http://www.etravelblackboardasia.com/article.asp?id=49605&nav=2)

26.

27.

28.

29.

30.

31.

32. [JAL Cargo
    statistics](http://www.jal.com/ja/ir/finance/factbook/2006_03.xls)

33. [Photos: Boeing 777-346/ER Aircraft Pictures |
    Airliners.net](http://www.airliners.net/photo/Japan-Airlines--/Boeing-777-346-ER/2230831/L/)

34.
35. [All About the JAL Group -
    Flight](http://www.jal.com/en/corporate/gaiyo/flight.html)

36. <http://www.jal.co.jp/en/aircraft/>

37.

38.

39.

40.

41.

42. [ASN Aircraft accident Boeing 727-89 JA8315 Itazuke, Seoul,
    Pyongyang](http://aviation-safety.net/database/record.php?id=19700331-0)

43.

44. [ASN Aircraft accident McDonnell Douglas DC-8-61 JA8048
    Shanghai](http://aviation-safety.net/database/record.php?id=19820917-0)

45. [ASN Aircraft accident Boeing 747-246B JA8161
    Tokyo](http://aviation-safety.net/database/record.php?id=19911002-0)

46. [日航一架787客機電池冒煙及滲漏要停飛](http://www.881903.com/Page/ZH-TW/newsdetail.aspx?ItemId=690412&csid=261_367)，商業電台（香港）即時國際，2014-01-15

47.

48. [ASN Aircraft accident Martin 2-0-2 N93043 Mihara
    Volcano](http://aviation-safety.net/database/record.php?id=19520409-0)

49. [ASN Aircraft accident McDonnell Douglas DC-8-62 JA8032 San
    Francisco Bay,
    CA](http://aviation-safety.net/database/record.php?id=19681122-0)

50. [ASN Aircraft accident Douglas DC-8-53 JA8012 Delhi-Palam Airport
    (DEL)](http://aviation-safety.net/database/record.php?id=19720614-1)

51. [ASN Aircraft accident McDonnell Douglas DC-8-62 JA8040
    Moskva-Sheremetyevo Airport
    (SVO)](http://aviation-safety.net/database/record.php?id=19721128-1)

52. [ASN Aircraft accident McDonnell Douglas DC-8-62AF JA8054 Anchorage
    International Airport, AK
    (ANC)](http://aviation-safety.net/database/record.php?id=19770113-0)

53. [ASN Aircraft accident McDonnell Douglas DC-8-62H JA8051 Kuala
    Lumpur Subang International Airport
    (KUL)](http://aviation-safety.net/database/record.php?id=19770927-0)

54. 因為此事故的飛機與該空難的是同一架

55. [ASN Aircraft accident Boeing 747SR-46 JA8119
    Ueno](http://aviation-safety.net/database/record.php?id=19850812-1)

56.

57.

58. [酒后开飞机](http://news.ifeng.com/a/20181102/60141193_0.shtml)

59. [遭英警方扣留
    日本航空公开致歉](https://news.online.sh.cn/news/gb/content/2018-11/03/content_9097143.htm)

60. [民航华东管理局介入调查“达美航空中断起飞”事件](http://www.bjnews.com.cn/news/2018/11/13/520978.html)