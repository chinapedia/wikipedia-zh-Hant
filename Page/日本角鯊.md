**日本角鯊**（[學名](../Page/學名.md "wikilink")：*Squalus
japonicus*），又名**日本棘鮫**，是[軟骨魚綱](../Page/軟骨魚綱.md "wikilink")[角鯊目](../Page/角鯊目.md "wikilink")[角鯊科的一](../Page/角鯊科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分佈於西[太平洋區](../Page/太平洋.md "wikilink")，包括[日本東南部](../Page/日本.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[越南等海域](../Page/越南.md "wikilink")。

## 深度

水深150至300公尺。

## 特徵

本魚體細而延長，頭部平扁而較長。眼長橢圓形，無瞬膜。具盾鱗，魚體灰褐色，腹面淡白色。背鰭兩個，各具一硬棘。鰭後緣具白邊，尾細長，尾基上方具凹窪。體長可達91公分。

## 生態

本魚棲息在[大陸棚及大陸坡](../Page/大陸棚.md "wikilink")，屬肉食性，以[魚類和](../Page/魚類.md "wikilink")[甲殼類為主](../Page/甲殼類.md "wikilink")，[卵胎生](../Page/卵胎生.md "wikilink")。

## 經濟利用

食用魚，可做成沙魚煙。

## 参考文献

  -
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[japonicus](../Category/角鯊屬.md "wikilink")