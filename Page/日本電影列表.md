本列表為[日本出品的](../Page/日本.md "wikilink")[電影列表](../Page/日本電影.md "wikilink")。

## 0-9

  - [2077日本鎖國](../Page/2077日本鎖國.md "wikilink")
  - [20世紀少年](../Page/20世紀少年.md "wikilink")
  - [69 sixty nine](../Page/69_sixty_nine.md "wikilink")

## A

  - [空氣人形](../Page/空氣人形.md "wikilink")（2009）
  - [要聽神明的話](../Page/要聽神明的話_\(電影\).md "wikilink")（2014）

<!-- end list -->

  - [爱的捆绑](../Page/爱的捆绑.md "wikilink")
  - [爱情全垒打](../Page/爱情全垒打.md "wikilink")
  - [愛上July24大道](../Page/愛上July24大道.md "wikilink")
  - [愛我別走](../Page/愛我別走.md "wikilink")
  - [愛與淚相隨](../Page/愛與淚相隨.md "wikilink")（又译**泪光闪闪**）
  - [案山子](../Page/案山子.md "wikilink")
  - [Apartment1303](../Page/Apartment1303.md "wikilink")

## B

  - [八墓村 (1977年電影)](../Page/八墓村_\(1977年電影\).md "wikilink")

  - [八墓村 (1996年電影)](../Page/八墓村_\(1996年電影\).md "wikilink")

  -
  - [不死蝶](../Page/不死蝶.md "wikilink")

  - [伯拉图式的性爱](../Page/伯拉图式的性爱.md "wikilink")

  - [爆粗Band友](../Page/爆粗Band友.md "wikilink")

  - [BALLAD
    無名的戀曲_(蠟筆小新風起雲湧\!壯烈戰國大合戰真人版](../Page/BALLAD_無名的戀曲_\(蠟筆小新風起雲湧!壯烈戰國大合戰真人版.md "wikilink")

  - [BANDAGE](../Page/BANDAGE.md "wikilink")

## C

  - [告白](../Page/告白_\(電影\).md "wikilink")（2010）

<!-- end list -->

  - [车站](../Page/车站.md "wikilink")
  - [赤桥下的暖流](../Page/赤桥下的暖流.md "wikilink")
  - [春琴抄](../Page/春琴抄.md "wikilink")
  - [春之雪](../Page/春之雪.md "wikilink")
  - [丑闻](../Page/丑闻.md "wikilink")
  - [此后朋友们](../Page/此后朋友们.md "wikilink")
  - [寵物小精靈-超夢夢反擊戰](../Page/寵物小精靈-超夢夢反擊戰.md "wikilink")(超夢的逆襲)
  - [寵物小精靈-利基亞爆誕](../Page/寵物小精靈-利基亞爆誕.md "wikilink")
  - [寵物小精靈-結晶塔之帝王](../Page/寵物小精靈-結晶塔之帝王.md "wikilink")
  - [草裙娃娃呼啦啦](../Page/草裙娃娃呼啦啦.md "wikilink")
  - [穿越時空的少女](../Page/穿越時空的少女.md "wikilink")
  - [瑪莉與三隻幼犬](../Page/柴犬奇迹物语.md "wikilink")（又譯**瑪莉與三隻幼犬**）
  - [初雪之戀](../Page/初雪之戀.md "wikilink")

## D

  - [大海的见证](../Page/大海的见证.md "wikilink")

  -
  - [大逃杀](../Page/大逃杀.md "wikilink")

  - [大逃杀2](../Page/大逃杀2.md "wikilink")

  - [大約三十個謊言](../Page/大約三十個謊言.md "wikilink")

  - [导盲犬小Q](../Page/导盲犬小Q.md "wikilink")（又译**再見了，可魯**或**再见了，小Q**）

  - [等待，只為與你相遇](../Page/等待，只為與你相遇.md "wikilink")

  - [钓鱼迷日记](../Page/钓鱼迷日记.md "wikilink")

  - [东京流浪汉](../Page/东京流浪汉.md "wikilink")

  - [東京大歌廳](../Page/東京大歌廳.md "wikilink")

  - [東京密友](../Page/東京密友.md "wikilink")

  - [東京傾情男女](../Page/東京傾情男女.md "wikilink")

  - [东京日和](../Page/东京日和.md "wikilink")

  - [東京鐵塔:我的母親父親](../Page/東京鐵塔:我的母親父親.md "wikilink")

  - [东京物语](../Page/东京物语.md "wikilink")

  - [東尼瀧谷](../Page/東尼瀧谷.md "wikilink")

  - [地海戰記](../Page/地海戰記.md "wikilink")（又译**地海傳說**）

  - [電車男](../Page/電車男.md "wikilink")

  - [多啦A夢-大雄的太陽王傳說](../Page/多啦A夢-大雄的太陽王傳說.md "wikilink")

  - [多啦A夢-大雄的宇宙飄流記](../Page/多啦A夢-大雄的宇宙飄流記.md "wikilink")

  - [多啦A夢-大雄與翼之勇者](../Page/多啦A夢-大雄與翼之勇者.md "wikilink")

  - [多啦A夢-大雄與機械人王國](../Page/多啦A夢-大雄與機械人王國.md "wikilink")

  - [多啦A夢-大雄的貓狗時空傳](../Page/多啦A夢-大雄的貓狗時空傳.md "wikilink")

  - [多啦A夢-大雄的新魔界大冒險](../Page/多啦A夢-大雄的新魔界大冒險.md "wikilink")

  - [多羅羅](../Page/多羅羅.md "wikilink")（又译**怪俠多羅羅**）

  - [新·第六感生死戀](../Page/Ghost_再一次擁抱你.md "wikilink")（[第六感生死戀](../Page/第六感生死戀.md "wikilink")
    亞洲版）

  - [鬪茶](../Page/鬪茶_\(電影\).md "wikilink")

  - [東京地震8.0](../Page/東京地震8.0.md "wikilink")

## E

  - [二十四只眼睛](../Page/二十四只眼睛.md "wikilink")

## F

  - [蜂蜜與四葉草](../Page/蜂蜜與四葉草.md "wikilink")（又译**蜂蜜與幸運草**）
  - [風之谷](../Page/風之谷.md "wikilink")
  - [佛 (电影)](../Page/佛_\(电影\).md "wikilink")
  - [福音戰士新劇場版：序](../Page/福音戰士新劇場版：序.md "wikilink")
  - [福音戰士新劇場版：破](../Page/福音戰士新劇場版：破.md "wikilink")

## G

  - [逆轉裁判](../Page/逆轉裁判_\(電影\).md "wikilink")（2012）

<!-- end list -->

  - [Go\!大暴走](../Page/Go!大暴走.md "wikilink")
  - [感染列島](../Page/感染列島.md "wikilink")
  - [关于莉莉周的一切](../Page/关于莉莉周的一切.md "wikilink")
  - [鋼之練金術師-森巴拉的征服者](../Page/鋼之練金術師-森巴拉的征服者.md "wikilink")
  - [怪俠多羅羅](../Page/多羅羅.md "wikilink")（又译**多羅羅**）
  - [鬼水凶靈](../Page/鬼水凶靈.md "wikilink")
  - [鬼來電](../Page/鬼來電.md "wikilink")

## H

  - [哈尔移动城堡](../Page/哈尔移动城堡.md "wikilink")（又译**霍爾的移動城堡**）
  - [海猿之終極海難](../Page/海猿.md "wikilink")
  - [黑幫有個荷里活](../Page/魔幻時刻.md "wikilink")（又译**魔幻時刻**）
  - [黑猫亭事件](../Page/黑猫亭事件.md "wikilink")
  - [HERO (2007年電影)](../Page/HERO_\(2007年電影\).md "wikilink")
  - [和豬豬一起上課的日子](../Page/和豬豬一起上課的日子.md "wikilink")
  - [忽然7鴨](../Page/忽然7鴨.md "wikilink")
  - [花火](../Page/花火_\(日本电影\).md "wikilink")
  - [花樣男子Final](../Page/花樣男子Final.md "wikilink")
  - [花樣奇緣](../Page/令人討厭的松子的一生.md "wikilink")（又译**令人討厭的松子的一生**、**松子被嫌弃的一生**）
  - [花样跳水少年](../Page/花样跳水少年.md "wikilink")（又译**跳水少年**）
  - [花與爱丽丝](../Page/花與爱丽丝.md "wikilink")
  - [花之武者](../Page/花之武者.md "wikilink")
  - [坏孩子的天空](../Page/坏孩子的天空.md "wikilink")
  - [幻之光](../Page/幻之光.md "wikilink")
  - [黄昏的清兵卫](../Page/黄昏的清兵卫.md "wikilink")
  - [黃泉路](../Page/黃泉路.md "wikilink")（又译**黃泉復活**）

## I

  - [謝謝你，在世界的角落找到我](../Page/謝謝你，在世界的角落找到我_\(電影\).md "wikilink")（2016）

## J

  - [Jose與虎魚們](../Page/Jose與虎魚們.md "wikilink")
  - [機動戰士Z剛彈](../Page/機動戰士Z剛彈.md "wikilink")-星之繼承者
  - [機動戰士Z剛彈](../Page/機動戰士Z剛彈.md "wikilink")2戀人們
  - [機動戰士Z剛彈](../Page/機動戰士Z剛彈.md "wikilink")3星之躍動是愛
  - [甲賀忍法帖](../Page/甲賀忍法帖.md "wikilink")
  - [假面騎士](../Page/幪面超人.md "wikilink")（又译**幪面超人**）
  - [假面騎士The First](../Page/幪面超人The_First.md "wikilink")（又译**幪面超人The
    First**）
  - [姐妹坡Shimaizaka](../Page/姐妹坡.md "wikilink")
  - [解夏](../Page/解夏_\(電影\).md "wikilink")
  - [藉著雨點說愛你](../Page/藉著雨點說愛你.md "wikilink")（又译**現在，很想見你**）
  - [藉著寫真說愛你](../Page/藉著寫真說愛你.md "wikilink")
  - [儘管如此我沒做過](../Page/儘管如此我沒做過.md "wikilink")
  - [居酒屋兆治](../Page/居酒屋兆治.md "wikilink")
  - [菊次郎之夏](../Page/菊次郎之夏.md "wikilink")
  - [绝唱](../Page/绝唱.md "wikilink")

## K

  - [Keroro軍曹大電影](../Page/Keroro軍曹大電影.md "wikilink")
  - [Keroro軍曹大電影2:決戰水著兵團](../Page/Keroro軍曹大電影2:決戰水著兵團.md "wikilink")
  - [Keroro軍曹大電影3之天空大作戰](../Page/Keroro軍曹大電影3之天空大作戰.md "wikilink")

## L

  - [喇叭書院](../Page/搖擺女孩.md "wikilink")（又译**搖擺女孩**、**Swing Girls**）
  - [藍色青春](../Page/藍色青春.md "wikilink")
  - [泪光闪闪](../Page/愛與淚相隨.md "wikilink")（又译**愛與淚相隨**）
  - [冷静与热情之间](../Page/冷静与热情之间.md "wikilink")
  - [令人討厭的松子的一生](../Page/令人討厭的松子的一生.md "wikilink")（又译**花樣奇緣**、**松子被嫌弃的一生**）
  - [亂](../Page/亂.md "wikilink")
  - [罗生门](../Page/羅生門_\(電影\).md "wikilink")
  - [L之終章:最後的23天](../Page/L之終章:最後的23天.md "wikilink")
  - [龍貓](../Page/龍貓.md "wikilink")
  - [Liar Game The Final Stage
    Trailer](../Page/Liar_Game_The_Final_Stage_Trailer.md "wikilink")

## M

  - [瑪琲時光](../Page/瑪琲時光.md "wikilink")
  - [瑪莉與三隻幼犬](../Page/柴犬奇迹物语.md "wikilink")（又译**柴犬奇迹物语**）
  - [鳗鱼](../Page/鳗鱼.md "wikilink")
  - [貓之報恩](../Page/貓之報恩.md "wikilink")
  - [猛鬼勿語](../Page/猛鬼勿語.md "wikilink")
  - [幪面超人](../Page/幪面超人.md "wikilink")（又译**假面騎士**）
  - [幪面超人The First](../Page/幪面超人The_First.md "wikilink")（又译**假面騎士The
    First**）
  - [魔幻時刻](../Page/魔幻時刻.md "wikilink")（又译**黑幫有個荷里活**）
  - [梦](../Page/夢\(黑澤明電影\).md "wikilink")
  - [夢幻街少女](../Page/夢幻街少女.md "wikilink")
  - [梦旅人](../Page/梦旅人.md "wikilink")
  - [秘密](../Page/秘密_\(小说\)#電影.md "wikilink")
  - [秒速5厘米](../Page/秒速5厘米.md "wikilink")
  - [明日的記憶](../Page/明日的記憶.md "wikilink")
  - [名偵探柯南:水平線上的陰謀](../Page/名偵探柯南:水平線上的陰謀.md "wikilink")
  - [名偵探柯南:紺碧之棺](../Page/名偵探柯南:紺碧之棺.md "wikilink")
  - [命](../Page/命.md "wikilink")

## N

  - [無人知曉的夏日清晨](../Page/無人知曉的夏日清晨.md "wikilink")（2004）

<!-- end list -->

  - [NaNa2](../Page/NaNa2.md "wikilink")
  - [那个夏季，最宁静的海](../Page/那个夏季，最宁静的海.md "wikilink")
  - [這麼遠，那麼近](../Page/這麼遠，那麼近.md "wikilink")
  - [戀愛寫真](../Page/戀愛寫真.md "wikilink")
  - [戀愛復活](../Page/戀愛復活.md "wikilink")
  - [戀愛初稿](../Page/戀愛初稿.md "wikilink")
  - [蠟筆小新劇場版:燒肉反擊戰](../Page/蠟筆小新劇場版:燒肉反擊戰.md "wikilink")
  - [蠟筆小新劇場版:呼風喚雨\!我的新娘](../Page/蠟筆小新劇場版:呼風喚雨!我的新娘.md "wikilink")

## O

  - [On Your Mark](../Page/On_Your_Mark.md "wikilink")

## P

  - [PACO與魔法繪本](../Page/PACO與魔法繪本.md "wikilink")
  - [平成狸合战](../Page/平成狸合战.md "wikilink")
  - [憑神](../Page/憑神.md "wikilink")

## Q

  - [七武士](../Page/七武士.md "wikilink")
  - [奇幻世紀](../Page/世界奇妙物語.md "wikilink")（又译**世界奇妙物語**）
  - [千年女優](../Page/千年女優.md "wikilink")
  - [千与千寻](../Page/千与千寻.md "wikilink")
  - [青春電幻物語](../Page/青春電幻物語.md "wikilink")
  - [情书](../Page/情书.md "wikilink")
  - [秋刀魚之味](../Page/秋刀魚之味.md "wikilink")

## R

  - [人性的证明](../Page/人性的证明.md "wikilink")
  - [壬生义士传](../Page/壬生义士传.md "wikilink")
  - [日本沉没](../Page/日本沉没.md "wikilink")
  - [日出前向青春告別](../Page/日出前向青春告別.md "wikilink")
  - [惹鬼回路](../Page/惹鬼回路.md "wikilink")
  - [惹鬼狂叫](../Page/惹鬼狂叫.md "wikilink")

## S

  - [橫山家之味](../Page/橫山家之味.md "wikilink")（2008）
  - [貞子3D](../Page/貞子3D.md "wikilink")（2012）
  - [小偷家族](../Page/小偷家族.md "wikilink")（2018）

<!-- end list -->

  - [SABU](../Page/SABU_\(電影\).md "wikilink")
  - [三億日圓極盜初戀](../Page/三億日圓極盜初戀.md "wikilink")
  - [三個衝浪少年](../Page/三個衝浪少年.md "wikilink")
  - [色凶怪談](../Page/色凶怪談.md "wikilink")
  - [殺手阿一](../Page/殺手阿一.md "wikilink")
  - [砂之器](../Page/砂之器.md "wikilink")
  - [上班族金太郎](../Page/上班族金太郎.md "wikilink")
  - [少年馴象師](../Page/少年馴象師.md "wikilink")
  - [生死恋](../Page/生死恋.md "wikilink")
  - [世界奇妙物語](../Page/世界奇妙物語.md "wikilink")（又译**奇幻世紀**）
  - [世上的另一個我Nana](../Page/世上的另一個我Nana.md "wikilink")
  - [死者田園祭](../Page/死者田園祭.md "wikilink")
  - [死神的精確度](../Page/死神的精確度.md "wikilink")
  - [數碼暴龍](../Page/數碼暴龍.md "wikilink")
  - [數碼暴龍02大電影](../Page/數碼暴龍02大電影.md "wikilink")
  - [數碼暴龍03大電影](../Page/數碼暴龍03大電影.md "wikilink")
  - [數碼暴龍大電影之暴走特急](../Page/數碼暴龍大電影之暴走特急.md "wikilink")
  - [五個撲水的少年](../Page/五個撲水的少年.md "wikilink")（又译**水男孩**）
  - [四月物语](../Page/四月物语.md "wikilink")
  - [四日之奇蹟](../Page/四日之奇蹟.md "wikilink")
  - [松子被嫌弃的一生](../Page/令人討厭的松子的一生.md "wikilink")（又译**花樣奇緣**、**令人討厭的松子的一生**）
  - [歲月的童話](../Page/歲月的童話.md "wikilink")
  - [送行者：禮儀師的樂章](../Page/送行者：禮儀師的樂章.md "wikilink")
  - [死亡筆記本](../Page/死亡筆記本.md "wikilink")
  - [死亡筆記本:最後的名字](../Page/死亡筆記本:最後的名字.md "wikilink")
  - [死亡筆記:決戰新世界](../Page/死亡筆記:決戰新世界.md "wikilink")

## T

  - [太陽之歌](../Page/太陽之歌.md "wikilink")
  - [Takeshis'](../Page/Takeshis'.md "wikilink")
  - [談談情，跳跳舞](../Page/談談情，跳跳舞.md "wikilink")
  - [天國之戀火](../Page/天國之戀火.md "wikilink")
  - [天空之城](../Page/天空之城.md "wikilink")
  - [天使](../Page/天使.md "wikilink")
  - [天使的伤痕](../Page/天使的伤痕.md "wikilink")
  - [天與地](../Page/天與地_\(日本電影\).md "wikilink")
  - [跳水少年](../Page/花样跳水少年.md "wikilink")（又译**花样跳水少年**）
  - [铁道员](../Page/铁道员.md "wikilink")
  - [鐵道凶靈](../Page/鐵道凶靈.md "wikilink")
  - [聽說你愛我](../Page/聽說你愛我.md "wikilink")
  - [Touch](../Page/Touch.md "wikilink")
  - [頭文字D](../Page/頭文字D.md "wikilink")
  - [甜蜜皮鞭](../Page/甜蜜皮鞭.md "wikilink")

## W

  - [玩偶](../Page/玩偶.md "wikilink")
  - [望乡](../Page/望乡_\(1974年电影\).md "wikilink")
  - [我愛奇諾奧](../Page/我愛奇諾奧.md "wikilink")
  - [我的機械人女友](../Page/我的機械人女友.md "wikilink")
  - [我和尋回犬的10個約定](../Page/我和尋回犬的10個約定.md "wikilink")（又译**與狗狗的10個約定**）
  - [我們來跳舞](../Page/我們來跳舞.md "wikilink")
  - [我們這一家](../Page/我們這一家.md "wikilink")
  - [烏龍廚神](../Page/烏龍廚神.md "wikilink")（又译**烏冬廚神**）
  - [无名地带](../Page/无名地带.md "wikilink")
  - [五个相扑的少年](../Page/五个相扑的少年.md "wikilink")
  - [五个撲水的少年](../Page/五个撲水的少年.md "wikilink")（又译**水男孩**）
  - [武士的一分](../Page/武士的一分.md "wikilink")
  - [午夜凶鈴](../Page/午夜凶鈴.md "wikilink")
  - [武者回歸](../Page/武者回歸.md "wikilink")

## X

  - [下妻物語](../Page/下妻物語.md "wikilink")
  - [下弦之月呼喚愛](../Page/下弦之月呼喚愛.md "wikilink")
  - [現在，很想見你](../Page/藉著雨點說愛你.md "wikilink")（又译**藉著雨點說愛你**）
  - [現在只想愛你](../Page/現在只想愛你.md "wikilink")
  - [星級大改造](../Page/星級大改造.md "wikilink")
  - [幸福的黄手绢](../Page/幸福的黄手绢.md "wikilink")
  - [凶氣之櫻](../Page/凶氣之櫻.md "wikilink")
  - [学校](../Page/学校_\(电影\).md "wikilink")
  - [學校怪談](../Page/學校怪談_\(電影\).md "wikilink")

## Y

  - [那些年，我們一起追的女孩](../Page/那些年，我們一起追的女孩_\(日本\).md "wikilink")（2018）

<!-- end list -->

  - [炎之舞](../Page/炎之舞.md "wikilink")
  - [燕尾蝶](../Page/燕尾蝶_\(電影\).md "wikilink")
  - [搖擺女孩](../Page/搖擺女孩.md "wikilink")（又译**喇叭書院**、**Swing Girls**）
  - [伊豆的舞女](../Page/伊豆的舞女.md "wikilink")
  - [一公升眼淚](../Page/一公升眼淚.md "wikilink")
  - [仪式](../Page/仪式.md "wikilink")
  - [銀髮阿基德](../Page/銀髮阿基德.md "wikilink")
  - [银河铁道之夜](../Page/银河铁道之夜.md "wikilink")
  - [寅次郎的故事](../Page/寅次郎的故事.md "wikilink")
  - [隱劍鬼爪](../Page/隱劍鬼爪.md "wikilink")
  - [萤火虫之墓](../Page/萤火虫之墓.md "wikilink")
  - [影武者](../Page/影武者.md "wikilink")
  - [拥抱](../Page/拥抱.md "wikilink")
  - [幽灵公主](../Page/幽灵公主.md "wikilink")
  - [與狗狗的10個約定](../Page/我和尋回犬的10個約定.md "wikilink")（又译**我和尋回犬的10個約定**）
  - [遇人不熟](../Page/遇人不熟.md "wikilink")
  - [远山的呼唤](../Page/远山的呼唤.md "wikilink")
  - [永遠的0](../Page/永遠的0#電影.md "wikilink")

## Z

  - [再見二丁目](../Page/再見二丁目.md "wikilink")
  - [再見了，可魯](../Page/导盲犬小Q.md "wikilink")（又译**再见了，小Q**、**导盲犬小Q**）
  - [在世界中心呼喚愛](../Page/在世界中心呼喚愛.md "wikilink")
  - [追捕](../Page/追捕_\(电影\).md "wikilink")
  - [姿三四郎](../Page/姿三四郎_\(電影\).md "wikilink")
  - [子狐物語](../Page/子狐物語.md "wikilink")
  - [奏鳴曲](../Page/奏鳴曲_\(電影\).md "wikilink")
  - [座头市](../Page/座头市.md "wikilink")
  - [只有你聽到 CALLING YOU](../Page/只有你聽到_CALLING_YOU.md "wikilink")
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")

## 参见

  - [中国电影列表](../Page/中国电影列表.md "wikilink")
  - [香港电影列表](../Page/香港电影列表.md "wikilink")
  - [台湾电影列表](../Page/台湾电影列表.md "wikilink")
  - [韓國電影列表](../Page/韓國電影列表.md "wikilink")
  - [法国电影列表](../Page/法国电影列表.md "wikilink")
  - [德国电影列表](../Page/德国电影列表.md "wikilink")
  - [英国电影列表](../Page/英国电影列表.md "wikilink")
  - [美国电影列表](../Page/美国电影列表.md "wikilink")

[日本電影作品](../Category/日本電影作品.md "wikilink")
[電影](../Category/日本相關列表.md "wikilink")
[日本](../Category/各國電影列表.md "wikilink")