[Jasrac_head_office_shibuya.JPG](https://zh.wikipedia.org/wiki/File:Jasrac_head_office_shibuya.JPG "fig:Jasrac_head_office_shibuya.JPG")[上原的JASRAC總部](../Page/上原_\(澀谷區\).md "wikilink")\]\]

**一般社團法人日本音樂著作權協會**（），簡稱**JASRAC**（來自其英文譯名「」），是依[日本](../Page/日本.md "wikilink")《》設立，並於日本國內經營[音樂](../Page/音樂.md "wikilink")[著作權集中管理事業的](../Page/著作權.md "wikilink")[社團法人機構](../Page/社團法人.md "wikilink")。

JASRAC會從持有[音樂](../Page/音樂.md "wikilink")（樂曲、歌詞）著作權的[作詞者](../Page/作詞家.md "wikilink")、[作曲者](../Page/作曲家.md "wikilink")、[音樂出版業者中受取錄音權及演奏權等著作權的](../Page/音樂出版社.md "wikilink")[信託](../Page/信託.md "wikilink")，對於音樂使用者進行使用許可（授權）、使用費的徵收與給權利者的分配、[著作權侵犯的監視](../Page/盜版.md "wikilink")，並對侵犯著作權者追求法律責任等為其主要業務。總部位於[東京都](../Page/東京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")[上原](../Page/上原_\(澀谷區\).md "wikilink")，並在日本全國的主要城市設置了22個分支單位。JASRAC是現存日本國內著作權管理事業者中最古老的，其前身為1939年（昭和14年）成立的大日本音樂著作權協會（）。

JASRAC因壟斷著作權收益（2005年為99.3%）而備受批評。由於絶大部份的唱片公司會在合約上要求作曲家把著作權讓給唱片公司以交換作曲費，所以很多時候作曲家並不擁有他們自己作品的著作權。就曾有創作者從來沒有因自己的作品，從JASRAC得到任何著作權收益分帳，卻在使用自己的作品時，被JASRAC要求繳交著作權費用，否則採取法律行動。其他批評還有財政透明度不足、高層收入過高、濫收著作權費用等。

## 參見

  - [JASRAC獎](../Page/JASRAC獎.md "wikilink")

## 外部連結

  - [日本音樂著作權協會官方網站](http://www.jasrac.or.jp/)

[Category:日本音樂](../Category/日本音樂.md "wikilink")
[Category:日本協會](../Category/日本協會.md "wikilink")
[Category:音樂產業協會](../Category/音樂產業協會.md "wikilink")
[Category:知識產權業收費機構](../Category/知識產權業收費機構.md "wikilink")