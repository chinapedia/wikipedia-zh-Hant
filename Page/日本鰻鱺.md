**日本鰻鱺**（[学名](../Page/学名.md "wikilink")：，俗名**白鳝**、**青鳝**、**鳗鱼**、**白鰻**）為[鰻鱺属的一](../Page/鰻鱺属.md "wikilink")[種](../Page/種.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")，由于过度捕捞，日本鳗鲡数量极速减少，2014年时被[国际自然保护联盟评估为濒危物种](../Page/國際自然保護聯盟.md "wikilink")。\[1\]\[2\]

## 分布

本魚分布於[马来半岛](../Page/马来半岛.md "wikilink")、[朝鲜半島](../Page/朝鲜半島.md "wikilink")、[日本](../Page/日本.md "wikilink")、台灣（蘭嶼、綠島）\[3\]、中国大陸沿海（四川境内的长江干流、[金沙江](../Page/金沙江.md "wikilink")、[岷江](../Page/岷江.md "wikilink")、[涪江](../Page/涪江.md "wikilink")、[嘉陵江](../Page/嘉陵江.md "wikilink")、[沱江](../Page/沱江.md "wikilink")、[渠江等水系](../Page/渠江.md "wikilink")）以及[菲律賓群岛等地的淡水溪流中](../Page/菲律賓.md "wikilink")。该物种的模式产地在日本[长崎](../Page/长崎.md "wikilink")。\[4\]

## 特徵

本魚體延長呈圓柱形，頭部呈錐型，但肛門後的尾部則稍側扁。鱗片細小，背鰭及臀鰭均與尾鰭相連。胸鰭位於鰓蓋後方，略呈圓形。魚體背部深灰綠或[黑色](../Page/黑色.md "wikilink")，腹部[白色](../Page/白色.md "wikilink")，無任何斑紋，體表無任何花紋。體長成年體長可達40至90[厘米](../Page/厘米.md "wikilink")，目前體型最大的日本鰻鱺長達150公分。

## 生態

屬於降海產卵的洄游性魚類，在河流中生活6至8年後，成熟的個體於秋、冬季順河游入海中，在大洋深處產卵。卵孵化後隨海流漂送，經[柳葉魚期的變態行為](../Page/柳葉魚.md "wikilink")，而達到河口成為透明的「鰻線」，再順著漲潮溯河而上成長。肉食性，以小魚及底棲生物為食物。

## 經濟利用

可以燒烤、煮[湯](../Page/湯.md "wikilink")、[紅燒](../Page/紅燒.md "wikilink")。常被加工製作成[蒲燒鰻](../Page/蒲燒.md "wikilink")。以養殖为主，野生種已很少見。

## 参考文献

[japonica](../Category/鰻鱺屬.md "wikilink")
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:日本魚類](../Category/日本魚類.md "wikilink")
[Category:台灣魚類](../Category/台灣魚類.md "wikilink")

1.

2.

3.  [日本鰻鱺](http://fishdb.sinica.edu.tw/chi/species.php?id=380711)，sinica，2016-12-11查閱

4.