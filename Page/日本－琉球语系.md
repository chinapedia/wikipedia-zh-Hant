**日本语系**，或者称作**日本－琉球语系**，是學者提出的一個[語系](../Page/语言系属分类.md "wikilink")，包含[日語和](../Page/日語.md "wikilink")[琉球語两大分支](../Page/琉球語.md "wikilink")。

日本語系這個概念由[美國的日本學者Leon](../Page/美國.md "wikilink")
Serafim提出。\[1\]日本語系的所有語言的[祖語都是原始日本語](../Page/祖語.md "wikilink")（Proto-Japonic）。\[2\][服部四郎認為](../Page/服部四郎.md "wikilink")，日本語和琉球語分化的時間為[大和時代](../Page/大和時代.md "wikilink")。\[3\]

日本語系這一概念受到國際語言學者的普遍接受，但在日本學者中，有些人将它们认为都是日语的[方言](../Page/方言.md "wikilink")，称之为**本土方言**和**冲绳方言**，但大部分语言学家认为日语和琉球语是两个属于同语系但有很大区别的语言，各自有各自的方言。

## 概述

日本语系与其它语言和语系的关系到目前为止并不确定。关于日本语系学者们提出过许多理论，但目前并没有一个是被普遍接受的，而且其中有部分理论甚至存在相当大的争议。

持[阿尔泰超语系假说的人认为日語與](../Page/阿尔泰超语系.md "wikilink")[阿爾泰語系也有關](../Page/阿爾泰語系.md "wikilink")，如与[朝鲜语](../Page/朝鲜语.md "wikilink")、[蒙古语的](../Page/蒙古语.md "wikilink")[语法](../Page/语法.md "wikilink")、[语音有相似性](../Page/语音.md "wikilink")，但至今基本上沒有任何實質證據。[南岛语与日语也有类似点](../Page/南岛语.md "wikilink")：[元音](../Page/元音.md "wikilink")（母音）有5个，即あ（a）、い（i）、う（u）、え（e）、お（o），不使用[双重元音](../Page/元音.md "wikilink")；[单词以元音结束](../Page/单词.md "wikilink")；[浊音不同于单词之首等](../Page/浊音.md "wikilink")。这表明，使用“南岛语”的[东南亚的一些民族](../Page/东南亚.md "wikilink")，有可能对[日本民族的构成曾发生影响](../Page/日本民族.md "wikilink")。但因为没有同源[词汇](../Page/词汇.md "wikilink")，一般并不认为日本语系属于南岛语系的分支，而只是可能受到了一些影响，如同[汉藏语系对东南亚南岛语系的影响](../Page/汉藏语系.md "wikilink")（声调，语法等）。而烏越憲三郎等人認為認為[日本人的主要成分應與](../Page/日本人.md "wikilink")[傣族或](../Page/傣族.md "wikilink")[哈尼族有同源關係](../Page/哈尼族.md "wikilink")，主要證據是體質上的諸多共同點，例如：青綠色胎斑、[血型A型占多數等](../Page/血型.md "wikilink")，而語言上的證據則需等待古[傣語等語言重建後方能進行進一步比對](../Page/傣語.md "wikilink")。

根据[比较语言学的成果](../Page/比较语言学.md "wikilink")，日语、琉球语和[高句丽语](../Page/高句丽语.md "wikilink")、[百济语等](../Page/百济语.md "wikilink")[扶余语可能同源](../Page/扶余语.md "wikilink")。[分子人类学新近研究成果也推定日本O](../Page/分子人类学.md "wikilink")2b父系基因可能来自[朝鲜半岛一带](../Page/朝鲜半岛.md "wikilink")

## 分类

### [日語](../Page/日語.md "wikilink")

  - [日語](../Page/日語.md "wikilink")（）
      - 八丈方言
      - 東日本方言
          - 北海道方言
          - 東北方言
              - 北奥羽方言
              - 南奥羽方言
          - 關東方言
              - 東關東方言
              - 西關東方言
              - 東京方言
          - 東海東山方言
              - 長野、山梨、靜岡方言
              - 越後方言
              - 岐阜、愛知方言
      - 西日本方言
          - 北陸方言
          - [近畿方言](../Page/近畿方言.md "wikilink")
          - 四國方言
          - 中國方言
              - 東山陽方言
              - 西中國方言
              - 東山陰方言
          - 雲伯方言
      - 九州方言
          - 豐日方言
          - 肥筑方言
          - 薩隅方言
      - 琉球日語
          - [冲绳日语](../Page/冲绳日语.md "wikilink")

### 琉球語

  - [琉球語](../Page/琉球語.md "wikilink")（[ryuk](http://multitree.linguistlist.org/codes/ryuk);
    [JPB](http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=JPB)）
      - 奄美－沖繩方言（北琉球方言）（[amok](http://multitree.linguistlist.org/codes/amok);
        [JPBA](http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=JPBA)）
          - 北部（[naok](http://multitree.linguistlist.org/codes/naok);
            [JPBAA](http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=JPBA)）
              - [北奄美方言](../Page/北奄美方言.md "wikilink")（）
              - [南奄美方言](../Page/南奄美方言.md "wikilink")（）
              - [喜界島方言](../Page/喜界島方言.md "wikilink")（）
              - [德之島方言](../Page/德之島方言.md "wikilink")（）
          - 南部（[saok](http://multitree.linguistlist.org/codes/saok);
            [JPBAB](http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=JPBA)）
              - [沖永良部島方言](../Page/沖永良部島方言.md "wikilink")（）
              - [与論島方言](../Page/与論島方言.md "wikilink")（）
              - [國頭方言](../Page/國頭方言.md "wikilink")（）
              - [首里－那霸中央方言](../Page/首里－那霸中央方言.md "wikilink")（）
      - 先島方言（南琉球方言）（[saki](http://multitree.linguistlist.org/codes/saki);
        [JPBB](http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=JPBB)）
          - [宮古方言](../Page/宮古方言.md "wikilink")（）
              - 宮古次方言
              - 伊良部次方言
          - [八重山方言](../Page/八重山方言.md "wikilink")（）
              - 石垣島次方言
              - 西表島次方言
              - 竹富島次方言
          - [与那国方言](../Page/与那国方言.md "wikilink")（）

## 人造語言

《[星界的纹章](../Page/星界的纹章.md "wikilink")》中所用的[人工语言](../Page/人工语言.md "wikilink")[亞維語亦被其作者歸入本語系裡](../Page/亞維語_\(虛構語言\).md "wikilink")。

## 参见

  - [粘着语](../Page/粘着语.md "wikilink")
  - [高句丽语](../Page/高句丽语.md "wikilink")
  - [扶餘語系](../Page/扶餘語系.md "wikilink")

## 參考文獻

## 外部链接

  - [Ethnologue上的有关日本语系的报告](http://www.ethnologue.com/family/17-1710)

{{-}}

[Category:語系](../Category/語系.md "wikilink")

1.  Shimabukuro, Moriyo. (2007). *The Accentual History of the Japanese
    and Ryukyuan Languages: a Reconstruction,* p. 1.
2.  Miyake, Marc Hideo. (2008).
3.  Heinrich, Patrick. ["What leaves a mark should no longer stain:
    Progressive erasure and reversing language shift activities in the
    Ryukyu
    Islands,"](http://www.sicri-network.org/ISIC1/j.%20ISIC1P%20Heinrich.pdf)
    First International Small Island Cultures Conference at [Kagoshima
    University](../Page/Kagoshima_University.md "wikilink"), Centre for
    the Pacific Islands, February 7–10, 2005; citing [Shiro
    Hattori](../Page/Shiro_Hattori.md "wikilink"). (1954) *Gengo
    nendaigaku sunawachi goi tokeigaku no hoho ni tsuite* ("Concerning
    the Method of Glottochronology and Lexicostatistics"), *Gengo
    kenkyu* (*Journal of the Linguistic Society of Japan*), Vols. 26/27.