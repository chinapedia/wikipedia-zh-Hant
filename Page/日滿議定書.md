[Signature_of_Japan-Manchukuo_Protocol.JPG](https://zh.wikipedia.org/wiki/File:Signature_of_Japan-Manchukuo_Protocol.JPG "fig:Signature_of_Japan-Manchukuo_Protocol.JPG")

《**日滿議定書**》是在[滿洲國](../Page/滿洲國.md "wikilink")[大同元年](../Page/大同_\(滿洲\).md "wikilink")（1932年，[昭和七年](../Page/昭和.md "wikilink")）9月15日由[大日本帝國與](../Page/大日本帝國.md "wikilink")[滿洲國簽訂的](../Page/滿洲國.md "wikilink")[議定書](../Page/條約.md "wikilink")。

日本代表是陸軍大將[武藤信義](../Page/武藤信義.md "wikilink")（[關東軍司令官](../Page/關東軍.md "wikilink")），[滿洲國代表則是國務總理](../Page/滿洲國.md "wikilink")[鄭孝胥](../Page/鄭孝胥.md "wikilink")。雙方於[新京簽字於上](../Page/新京.md "wikilink")，彼此享以[香檳酒](../Page/香檳酒.md "wikilink")，完成手續。\[1\]

## 議定書內容

  - 日本**承認滿洲國**
  - 滿洲國領土由日本和滿洲共同防衛

## 議定書全文

### 原文

### 漢文翻譯

## 簽署之後的結果

使得[滿洲徹底的變相淪為了日本](../Page/滿洲.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")，[滿洲國成為了對日本](../Page/滿洲國.md "wikilink")[關東軍軍部的](../Page/關東軍.md "wikilink")[傀儡政權](../Page/傀儡政權.md "wikilink")。

## 註釋

## 相关条目

  - [日蘇邊界衝突](../Page/日蘇邊界衝突.md "wikilink")

[Category:滿洲國外交](../Category/滿洲國外交.md "wikilink")
[Category:日蘇邊界衝突](../Category/日蘇邊界衝突.md "wikilink")
[Category:1932年日本外交](../Category/1932年日本外交.md "wikilink")
[Category:1932年中国政治](../Category/1932年中国政治.md "wikilink")
[Category:1932年9月](../Category/1932年9月.md "wikilink")

1.  "The protocol specified a mutual defence arrangement between Japan
    and Manchukuo and an unconditional stationing of Japanese troops,
    thereby justifying Japan's occupation of Manchuria and Japan's
    expanding aggression." in [*Continent, coast, ocean: dynamics of
    regionalism in Eastern Asia* by Universiti Kebangsaan Malaysia.
    Institut Alam dan Tamadun Melayu,Institute of Southeast Asian
    Studies
    p.20](https://books.google.com/books?id=Q4pX1uKkWf8C&pg=PA20)