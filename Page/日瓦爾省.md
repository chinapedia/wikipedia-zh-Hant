**日瓦爾省**（****）是[俄羅斯帝國的一個省](../Page/俄羅斯帝國.md "wikilink")。成立於1719年6月3日。1721年[瑞典正式讓予俄國](../Page/瑞典.md "wikilink")。1796年改名為**愛斯特蘭省**（****）。1917年其範圍擴大到[北利沃尼亞](../Page/利沃尼亞省.md "wikilink")，與今日[愛沙尼亞的疆域相當](../Page/愛沙尼亞.md "wikilink")。

[Category:愛沙尼亞歷史](../Category/愛沙尼亞歷史.md "wikilink")
[Р](../Category/俄罗斯帝国直辖省.md "wikilink")
[Category:1917年废除的行政区划](../Category/1917年废除的行政区划.md "wikilink")