|                                                                                                                  |
| :--------------------------------------------------------------------------------------------------------------: |
|                                                    **日经平均指数**                                                    |
|                                                       ****                                                       |
| [Nikkei_225(1970-).svg](https://zh.wikipedia.org/wiki/File:Nikkei_225\(1970-\).svg "fig:Nikkei_225(1970-).svg") |
|                                                        概要                                                        |
|                                                     **發行日期**                                                     |
|                                 **[識別代碼](../Page/:en:ISO_10383.md "wikilink")**                                  |
|                                **[BIC識別代碼](../Page/:en:ISO_9362.md "wikilink")**                                 |
|                                   **[FX識別代碼](../Page/金融資訊交換協定.md "wikilink")**                                   |
|                                                                                                                  |

**日经平均指数**（****）又名**日經225**，是由[日本經濟新聞社推出的](../Page/日本經濟新聞社.md "wikilink")[東京證券交易所的](../Page/東京證券交易所.md "wikilink")225品種的股價指數，1971年第一次发表。

日经指数的最高点是1989年12月29日，一度升至38957.44點，当日日经指数收盘于38915.87點。

东京证券交易所（JPX）的另外一个主要指数是[東証股價指數](../Page/東証股價指數.md "wikilink")（Topix）。

## 225間公司列表

  - 按股份編號順序排列

<div style="float:left; width:48%;">

### 食品（11）

  - [日清製粉集團](../Page/日清製粉集團.md "wikilink")（）
  - [明治控股](../Page/明治控股.md "wikilink")（）
  - [日本火腿](../Page/日本火腿.md "wikilink")（）
  - [札幌啤酒](../Page/札幌啤酒.md "wikilink")（）
  - [朝日啤酒](../Page/朝日啤酒.md "wikilink")（）
  - [麒麟控股](../Page/麒麟控股.md "wikilink")（）
  - [寶酒造](../Page/寶酒造.md "wikilink")（）
  - [龜甲萬](../Page/龜甲萬.md "wikilink")（）
  - [味之素](../Page/味之素.md "wikilink")（）
  - [日冷](../Page/日冷.md "wikilink")（）（）
  - [日本煙草產業](../Page/日本煙草產業.md "wikilink")（）

### 纖維（5）

  - [東洋紡織](../Page/東洋紡織.md "wikilink")（）
  - [尤尼吉可](../Page/尤尼吉可.md "wikilink")（）（）
  - [日清紡織控股](../Page/日清紡織控股.md "wikilink")（）
  - [帝人](../Page/帝人.md "wikilink")（）
  - [東麗株式會社](../Page/東麗株式會社.md "wikilink")（）

### 製紙（4）

  - [王子控股](../Page/王子製紙.md "wikilink")（）
  - [三菱製紙](../Page/三菱製紙.md "wikilink")（）
  - [北越紀州製紙](../Page/北越紀州製紙.md "wikilink")（）
  - [日本製紙集團](../Page/日本製紙集團.md "wikilink")（）

### 化學（17）

  - [可樂麗](../Page/可樂麗.md "wikilink")（）（）
  - [旭化成](../Page/旭化成.md "wikilink")（）
  - [昭和電工](../Page/昭和電工.md "wikilink")（）
  - [住友化學](../Page/住友化學.md "wikilink")（）
  - [日產化學工業](../Page/日產化學工業.md "wikilink")（）
  - [日本蘇打](../Page/日本蘇打.md "wikilink")（）
  - [東蘇](../Page/東蘇.md "wikilink")（）
  - [德山](../Page/德山.md "wikilink")（）
  - [電氣化學工業](../Page/電氣化學工業.md "wikilink")（）
  - [信越化學工業](../Page/信越化學工業.md "wikilink")（）
  - [三井化學](../Page/三井化學.md "wikilink")（）
  - [三菱化學控股](../Page/三菱化學控股.md "wikilink")（）
  - [宇部興產](../Page/宇部興產.md "wikilink")（）
  - [日本化藥](../Page/日本化藥.md "wikilink")（）
  - [花王](../Page/花王.md "wikilink")（）
  - [富士軟片控股](../Page/富士軟片控股.md "wikilink")（）
  - [資生堂](../Page/資生堂.md "wikilink")（）

### 醫藥品（8）

  - [協和發酵工業](../Page/協和發酵工業.md "wikilink")（）
  - [武田藥品工業](../Page/武田藥品工業.md "wikilink")（）
  - [安斯泰來製藥](../Page/安斯泰來製藥.md "wikilink")（）（）
  - [大日本住友製藥](../Page/大日本住友製藥.md "wikilink")（）
  - [鹽野義製藥](../Page/鹽野義製藥.md "wikilink")（）
  - [中外製藥](../Page/中外製藥.md "wikilink")（）
  - [日本衛材](../Page/日本衛材.md "wikilink")（）（）
  - [第一三共](../Page/第一三共.md "wikilink")（）

### 石油（2）

  - [昭和殼牌石油](../Page/昭和殼牌石油.md "wikilink")（）
  - [JX控股](../Page/JX控股.md "wikilink")（）

### 橡膠製品（2）

  - [橫濱橡膠](../Page/橫濱橡膠.md "wikilink")（）
  - [普利司通](../Page/普利司通.md "wikilink")（）

### 窯業（8）

  - [日東紡織](../Page/日東紡織.md "wikilink")（）
  - [旭硝子](../Page/旭硝子.md "wikilink")（）
  - [日本板硝子](../Page/日本板硝子股份.md "wikilink")（）
  - [住友大阪水泥](../Page/住友大阪水泥.md "wikilink")（）
  - [太平洋水泥](../Page/太平洋水泥.md "wikilink")（）
  - [東海炭素](../Page/東海炭素.md "wikilink")（）
  - [TOTO](../Page/TOTO.md "wikilink")（）
  - [日本碍子](../Page/日本碍子.md "wikilink")（）

### 鋼鐵製品（5）

  - [新日鐵住金](../Page/新日鐵住金.md "wikilink")（）
  - [神戶製鋼所](../Page/神戶製鋼所.md "wikilink")（）
  - [日新製鋼控股](../Page/日新製鋼控股.md "wikilink")（）
  - [JFE控股](../Page/JFE控股.md "wikilink")（）
  - [太平洋金屬](../Page/太平洋金屬.md "wikilink")（）

### 非鐵金屬製品（12）

  - [SUMCO](../Page/SUMCO.md "wikilink")（）
  - [古河金屬機械](../Page/古河金屬機械.md "wikilink")（）
  - [三井金屬礦業](../Page/三井金屬礦業.md "wikilink")（）
  - [東邦亞鉛](../Page/東邦亞鉛.md "wikilink")（）
  - [三菱綜合材料](../Page/三菱綜合材料.md "wikilink")（）
  - [住友金屬礦山](../Page/住友金屬礦山.md "wikilink")（）
  - [DOWA控股](../Page/DOWA控股.md "wikilink")（）
  - [日本輕金屬](../Page/日本輕金屬.md "wikilink")（）
  - [古河電器工業](../Page/古河電器工業.md "wikilink")（）
  - [住友電器工業](../Page/住友電器工業.md "wikilink")（）
  - [藤倉](../Page/藤倉.md "wikilink")（）
  - [東洋製罐](../Page/東洋製罐.md "wikilink")（）

### 機械（15）

  - [日本製鋼所](../Page/日本製鋼所.md "wikilink")（）
  - [大隈](../Page/大隈.md "wikilink")（）
  - [小松製作所](../Page/小松製作所.md "wikilink")（）
  - [住友重機械工業](../Page/住友重機械工業.md "wikilink")（）
  - [日立建機](../Page/日立建機.md "wikilink")（）
  - [荏原製作所](../Page/荏原製作所.md "wikilink")（）
  - [千代田化工建設](../Page/千代田化工建設.md "wikilink")（）
  - [大金工業](../Page/大金工業.md "wikilink")（）
  - [日本精工](../Page/日本精工.md "wikilink")（）
  - [NTN](../Page/NTN.md "wikilink")（）
  - [JTEKT](../Page/JTEKT.md "wikilink")（）
  - [久保田](../Page/久保田_\(企業\).md "wikilink")（）
  - [日立造船](../Page/日立造船.md "wikilink")（）
  - [三菱重工業](../Page/三菱重工業.md "wikilink")（）
  - [IHI](../Page/IHI.md "wikilink")（）

### 電氣機器（29）

  - [Advantest](../Page/Advantest.md "wikilink")（）
  - [GS Yuasa控股](../Page/GS_Yuasa控股.md "wikilink")（）
  - [佳能](../Page/佳能.md "wikilink")（）
  - [Minebea](../Page/Minebea.md "wikilink")（）
  - [卡西歐計算機](../Page/卡西歐.md "wikilink")（）
  - [日立製作所](../Page/日立製作所.md "wikilink")（）
  - [東芝](../Page/東芝.md "wikilink")（）
  - [三菱電機](../Page/三菱電機.md "wikilink")（）
  - [富士電機控股](../Page/富士電機控股.md "wikilink")（）
  - [明電舍](../Page/明電舍.md "wikilink")（）
  - [日本電氣](../Page/日本電氣.md "wikilink")（）
  - [富士通](../Page/富士通.md "wikilink")（）
  - [沖電器工業](../Page/沖電器工業.md "wikilink")（）
  - [松下電器](../Page/松下電器.md "wikilink")（）
  - [東京電子](../Page/東京電子.md "wikilink")（）
  - [夏普](../Page/夏普.md "wikilink")（）
  - [索尼](../Page/索尼.md "wikilink")（）
  - [TDK](../Page/TDK.md "wikilink")（）
  - [三洋電機](../Page/三洋電機.md "wikilink")（）
  - [三美電機](../Page/三美電機.md "wikilink")（）
  - [阿爾卑斯電氣](../Page/阿爾卑斯電氣.md "wikilink")（）
  - [先鋒](../Page/先鋒公司.md "wikilink")（）
  - [Clarion](../Page/Clarion.md "wikilink")（）
  - [橫河電機](../Page/橫河電機.md "wikilink")（）
  - [電裝](../Page/電裝.md "wikilink")（）
  - [松下電工](../Page/松下電工.md "wikilink")（）
  - [太陽誘電](../Page/太陽誘電.md "wikilink")（）
  - [京瓷](../Page/京瓷.md "wikilink")（）
  - [FANUC](../Page/FANUC.md "wikilink")（）

### 造船（2）

  - [川崎重工業](../Page/川崎重工業.md "wikilink")（）
  - [三井造船](../Page/三井造船.md "wikilink")（）

</div>

<div style="float:right; width:48%;">

### 汽車（9）

  - [三菱自動車工業](../Page/三菱汽車.md "wikilink")（）
  - [日產自動車](../Page/日產汽車.md "wikilink")（）
  - [日野自動車](../Page/日野汽车.md "wikilink")（）
  - [五十鈴自動車](../Page/五十鈴.md "wikilink")（）
  - [豐田自動車](../Page/丰田.md "wikilink")（）
  - [馬自達](../Page/马自达.md "wikilink")（）
  - [本田技研工業](../Page/本田技研工業.md "wikilink")（）
  - [鈴木自動車](../Page/鈴木株式會社.md "wikilink")（）
  - [富士重工業](../Page/富士重工業.md "wikilink")（）

### 精密工業（6）

  - [尼康](../Page/尼康.md "wikilink")（）
  - [奧林巴斯](../Page/奧林巴斯.md "wikilink")（）
  - [TERUMO](../Page/TERUMO.md "wikilink")（）
  - [柯尼卡美能達](../Page/柯尼卡美能達.md "wikilink")（）
  - [理光](../Page/理光.md "wikilink")（）
  - [星辰控股](../Page/星辰控股.md "wikilink")（）

### 其他製造（3）

  - [凸版印刷](../Page/凸版印刷_\(公司\).md "wikilink")（）
  - [大日本印刷](../Page/大日本印刷.md "wikilink")（）
  - [山葉](../Page/山葉株式會社.md "wikilink")（）

### 水產（2）

  - [日本水產](../Page/日本水產.md "wikilink")（）
  - [Maruha Nichiro控股](../Page/Maruha_Nichiro控股.md "wikilink")（）

### 礦業（1）

  - [國際石油開發帝石](../Page/國際石油開發帝石.md "wikilink")（）

### 建設（8）

  - [COMSYS控股](../Page/COMSYS控股.md "wikilink")（）
  - [大成建設](../Page/大成建設.md "wikilink")（）
  - [大林組](../Page/大林組.md "wikilink")（）
  - [清水建設](../Page/清水建設.md "wikilink")（）
  - [鹿島建設](../Page/鹿島建設.md "wikilink")（）
  - [大和房屋工業](../Page/大和房屋工業.md "wikilink")（）
  - [日揮](../Page/日揮.md "wikilink")（）
  - [積水房屋](../Page/積水房屋.md "wikilink")（）

### 貿易（7）

  - [伊藤忠商事](../Page/伊藤忠商事.md "wikilink")（）
  - [豐田通商](../Page/豐田通商.md "wikilink")（）
  - [丸紅](../Page/丸紅.md "wikilink")（）
  - [三井物產](../Page/三井物產.md "wikilink")（）
  - [住友商事](../Page/住友商事.md "wikilink")（）
  - [三菱商事](../Page/三菱商事.md "wikilink")（）
  - [双日](../Page/双日.md "wikilink")（）

### 零售（8）

  - [J.FRONT RETAILING](../Page/J.FRONT_RETAILING.md "wikilink")（）
  - [三越伊勢丹控股](../Page/三越伊勢丹控股.md "wikilink")（）
  - [7\&I控股](../Page/7&I控股.md "wikilink")（）
  - [高島屋](../Page/高島屋.md "wikilink")（）
  - [丸井](../Page/丸井.md "wikilink")（）
  - [永旺](../Page/永旺.md "wikilink")（）
  - [生活創庫](../Page/生活創庫.md "wikilink")（）
  - [迅銷](../Page/迅銷.md "wikilink")（）

### 銀行（11）

  - [新生銀行](../Page/新生銀行.md "wikilink")（）
  - [青空銀行](../Page/青空銀行.md "wikilink")（）
  - [三菱UFJ金融集團](../Page/三菱UFJ金融集團.md "wikilink")（）
  - [理索納控股](../Page/理索納控股.md "wikilink")（）
  - [三井住友信託控股](../Page/三井住友信託控股.md "wikilink")（）
  - [三井住友金融集團](../Page/三井住友金融集團.md "wikilink")（）
  - [千葉銀行](../Page/千葉銀行.md "wikilink")（）
  - [橫濱銀行](../Page/橫濱銀行.md "wikilink")（）
  - [福岡金融集團股份](../Page/福岡金融集團股份.md "wikilink")（）
  - [靜岡銀行](../Page/靜岡銀行.md "wikilink")（）
  - [瑞穗金融集團](../Page/瑞穗金融集團.md "wikilink")（）

### 證券（3）

  - [大和證券集團](../Page/大和證券集團.md "wikilink")（）
  - [野村控股](../Page/野村控股.md "wikilink")（）
  - [松井證券](../Page/松井證券.md "wikilink")（）

### 保險（6）

  - [NKSJ控股](../Page/NKSJ控股.md "wikilink")（）
  - [MS\&AD保險集團控股](../Page/MS&AD保險集團控股.md "wikilink")（）
  - [索尼金融控股](../Page/索尼金融控股.md "wikilink")（）
  - [第一生命保險](../Page/第一生命保險.md "wikilink")（）
  - [東京海上控股](../Page/東京海上控股.md "wikilink")（）
  - [T\&D控股](../Page/T&D控股.md "wikilink")（）

### 其他金融服務（1）

  - [Credit Saison](../Page/Credit_Saison.md "wikilink")（）

### 不動產（5）

  - [三井不動產](../Page/三井不動產.md "wikilink")（）
  - [三菱地所](../Page/三菱地所.md "wikilink")（）
  - [平和不動產](../Page/平和不動產.md "wikilink")（）
  - [東京建物](../Page/東京建物.md "wikilink")（）
  - [東急不動產](../Page/東急不動產.md "wikilink")（）
  - [住友不動產](../Page/住友不動產.md "wikilink")（）

### 鐵路、巴士（8）

  - [東武鐵道](../Page/東武鐵道.md "wikilink")（）
  - [東京急行電鐵](../Page/東京急行電鐵.md "wikilink")（）
  - [小田急電鐵](../Page/小田急電鐵.md "wikilink")（）
  - [京王電鐵](../Page/京王電鐵.md "wikilink")（）
  - [京成電鐵](../Page/京成電鐵.md "wikilink")（）
  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（）
  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（）
  - [東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")（）

### 陸運（2）

  - [日本通運](../Page/日本通運.md "wikilink")（）
  - [大和控股](../Page/大和控股.md "wikilink")（）

### 海運（3）

  - [日本郵船](../Page/日本郵船.md "wikilink")（）
  - [商船三井](../Page/商船三井.md "wikilink")（）
  - [川崎汽船](../Page/川崎汽船.md "wikilink")（）

### 空運（1）

  - [全日本空輸](../Page/全日本空輸.md "wikilink")（）

### 倉儲（1）

  - [三菱倉庫](../Page/三菱倉庫.md "wikilink")（）

### 通訊（5）

  - [SKY Perfect JSAT控股](../Page/SKY_Perfect_JSAT控股.md "wikilink")（）
  - [日本電信電話](../Page/日本電信電話.md "wikilink")（）
  - [KDDI](../Page/KDDI.md "wikilink")（）
  - [NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink")（）
  - [NTT Data](../Page/NTT_Data.md "wikilink")（）
  - [軟體銀行](../Page/軟體銀行.md "wikilink")（）

### 電力（3）

  - [東京電力](../Page/東京電力.md "wikilink")（）
  - [中部電力](../Page/中部電力.md "wikilink")（）
  - [關西電力](../Page/關西電力.md "wikilink")（）

### 瓦斯（2）

  - [東京瓦斯](../Page/東京瓦斯.md "wikilink")（）
  - [大阪瓦斯](../Page/大阪瓦斯.md "wikilink")（）

### 服務業（7）

  - [電通](../Page/電通.md "wikilink")（）
  - [雅虎日本](../Page/雅虎日本.md "wikilink")（）
  - [趨勢科技](../Page/趨勢科技.md "wikilink")（）
  - [東寶](../Page/東寶.md "wikilink")（）
  - [東京巨蛋](../Page/東京巨蛋.md "wikilink")（）
  - [西科姆](../Page/西科姆.md "wikilink")（SECOM，）
  - [科樂美](../Page/科樂美.md "wikilink")（）

</div>

## 指數歷史

單位計算為[日圓](../Page/日圓.md "wikilink")，最高與最低係以交易中出現的極值為準

<table>
<thead>
<tr class="header">
<th><p>時間</p></th>
<th><p>年初開盤價</p></th>
<th></th>
<th><p>最高值<br />
日期</p></th>
<th></th>
<th><p>最低值<br />
日期</p></th>
<th><p>年終收盤價</p></th>
<th><p>年間升跌</p></th>
<th><p>年間升跌幅度</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1949年（昭和24年）</p></td>
<td><p>176.21</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>109.91</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1950年（昭和25年）</p></td>
<td><p>108.56</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>101.91</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1951年（昭和26年）</p></td>
<td><p>102.10</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>166.06</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1952年（昭和27年）</p></td>
<td><p>167.80</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>362.64</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1953年（昭和28年）</p></td>
<td><p>364.89</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>377.95</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1954年（昭和29年）</p></td>
<td><p>362.88</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>356.09</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1955年（昭和30年）</p></td>
<td><p>361.10</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>425.69</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956年（昭和31年）</p></td>
<td><p>428.59</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>549.14</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1957年（昭和32年）</p></td>
<td><p>549.45</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>474.55</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1958年（昭和33年）</p></td>
<td><p>475.20</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>666.54</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1959年（昭和34年）</p></td>
<td><p>671.28</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>874.88</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1960年（昭和35年）</p></td>
<td><p>869.34</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,356.71</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1961年（昭和36年）</p></td>
<td><p>1,366.74</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,432.60</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1962年（昭和37年）</p></td>
<td><p>1,425.30</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,420.43</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1963年（昭和38年）</p></td>
<td><p>1,418.25</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,225.10</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1964年（昭和39年）</p></td>
<td><p>1,204.40</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,216.55</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1965年（昭和40年）</p></td>
<td><p>1,227.11</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,417.83</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1966年（昭和41年）</p></td>
<td><p>1,430.13</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,452.10</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1967年（昭和42年）</p></td>
<td><p>1,441.35</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,283.47</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1968年（昭和43年）</p></td>
<td><p>1,266.27</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,714.89</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1969年（昭和44年）</p></td>
<td><p>1,733.64</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>2,358.96</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1970年（昭和45年）</p></td>
<td><p>2,402.85</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>1,987.14</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1971年（昭和46年）</p></td>
<td><p>2,001.34</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>2,713.74</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972年（昭和47年）</p></td>
<td><p>2,712.31</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>5,207.94</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1973年（昭和48年）</p></td>
<td><p>5,232.86</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>4,306.80</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1974年（昭和49年）</p></td>
<td><p>4,259.20</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>3,817.22</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1975年（昭和50年）</p></td>
<td><p>3,777.40</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>4,358.60</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1976年（昭和51年）</p></td>
<td><p>4,403.06</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>4,990.85</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1977年（昭和52年）</p></td>
<td><p>4,998.85</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>4,865.60</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1978年（昭和53年）</p></td>
<td><p>4,867.91</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>6,001.85</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1979年（昭和54年）</p></td>
<td><p>6,041.57</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>6,569.47</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980年（昭和55年）</p></td>
<td><p>6,560.16</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>7,116.38</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1981年（昭和56年）</p></td>
<td><p>7,150.95</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>7,681.84</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1982年（昭和57年）</p></td>
<td><p>7,718.84</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>8,016.67</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983年（昭和58年）</p></td>
<td><p>8,021.40</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>9,893.82</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年（昭和59年）</p></td>
<td><p>9,927.11</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>11,542.60</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985年（昭和60年）</p></td>
<td><p>11,558.06</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>13,113.32</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986年（昭和61年）</p></td>
<td><p>13,136.87</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>18,701.30</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年（昭和62年）</p></td>
<td><p>18,820.55</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>21,564.00</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年（昭和63年）</p></td>
<td><p>21,217.04</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>30,159.00</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989年（平成元年）</p></td>
<td><p>30,243.66</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>38,915.87</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1990年（平成2年）</p></td>
<td><p>38,712.88</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>23,848.71</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991年（平成3年）</p></td>
<td><p>24,069.18</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>22,983.77</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年（平成4年）</p></td>
<td><p>23,030.66</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>16,924.95</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年（平成5年）</p></td>
<td><p>16,980.23</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>17,417.24</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994年（平成6年）</p></td>
<td><p>17,421.64</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>19,723.06</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995年（平成7年）</p></td>
<td><p>19,724.76</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>19,868.15</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年（平成8年）</p></td>
<td><p>19,945.68</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>19,361.35</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年（平成9年）</p></td>
<td><p>19,364.24</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>15,258.74</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年（平成10年）</p></td>
<td><p>15,268.93</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>13,842.17</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年（平成11年）</p></td>
<td><p>13,779.05</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>18,934.34</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年（平成12年）</p></td>
<td><p>18,937.45</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>13,785.69</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年（平成13年）</p></td>
<td><p>13,898.09</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>10,542.62</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年（平成14年）</p></td>
<td><p>10,631.00</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>8,578.95</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年（平成15年）</p></td>
<td><p>8,669.89</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
<td><p>-</p></td>
<td><p>10,676.64</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年（平成16年）</p></td>
<td><p>10,787.83</p></td>
<td></td>
<td><p>04/26</p></td>
<td></td>
<td><p>02/10</p></td>
<td><p>11,488.76</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年（平成17年）</p></td>
<td><p>11,458.27</p></td>
<td></td>
<td><p>12/29</p></td>
<td></td>
<td><p>04/21</p></td>
<td><p>16,111.43</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年（平成18年）</p></td>
<td><p>16,294.65</p></td>
<td></td>
<td><p>04/07</p></td>
<td></td>
<td><p>06/14</p></td>
<td><p>17,225.83</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年（平成19年）</p></td>
<td><p>17,322.50</p></td>
<td></td>
<td><p>02/26</p></td>
<td></td>
<td><p>11/22</p></td>
<td><p>15,307.78</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年（平成20年）</p></td>
<td><p>15,155.73</p></td>
<td></td>
<td><p>01/04</p></td>
<td></td>
<td><p>10/29</p></td>
<td><p>8,859.56</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年（平成21年）</p></td>
<td><p>8,991.21</p></td>
<td></td>
<td><p>08/31</p></td>
<td></td>
<td><p>03/10</p></td>
<td><p>10,546.44</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年（平成22年）</p></td>
<td><p>10,654.79</p></td>
<td></td>
<td><p>04/05</p></td>
<td></td>
<td><p>07/02</p></td>
<td><p>10,228.92</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年（平成23年）</p></td>
<td><p>10,398.10</p></td>
<td></td>
<td><p>02/21</p></td>
<td></td>
<td><p>11/25</p></td>
<td><p>8,455.35</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年（平成24年）</p></td>
<td><p>8,549.54</p></td>
<td></td>
<td><p>12/28</p></td>
<td></td>
<td><p>06/24</p></td>
<td><p>10,395.18</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年（平成25年）</p></td>
<td><p>10,604.50</p></td>
<td></td>
<td><p>12/30</p></td>
<td></td>
<td><p>01/09</p></td>
<td><p>16,291.31</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年（平成26年）</p></td>
<td><p>16,147.54</p></td>
<td></td>
<td><p>12/08</p></td>
<td></td>
<td><p>04/11</p></td>
<td><p>17,450.77</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年（平成27年）</p></td>
<td><p>17,325.68</p></td>
<td></td>
<td><p>06/24</p></td>
<td></td>
<td><p>01/16</p></td>
<td><p>19,033.71</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年（平成28年）</p></td>
<td><p>18,818.58</p></td>
<td></td>
<td><p>12/21</p></td>
<td></td>
<td><p>06/24</p></td>
<td><p>19,114.37</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年（平成29年）</p></td>
<td><p>19,298.68</p></td>
<td></td>
<td><p>11/09</p></td>
<td></td>
<td><p>|04/17</p></td>
<td><p>22,764.94</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018年（平成30年）</p></td>
<td><p>23,073.73</p></td>
<td></td>
<td></td>
<td></td>
<td><p>|</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [雅虎奇摩理財
    日本日經225指數](https://web.archive.org/web/20140215093819/http://tw.money.yahoo.com/intlindex_quote?s=%5EN225)

  - [雅虎香港財經 日本日經225指數](http://hk.finance.yahoo.com/q?s=%5EN225)

  - [Stock-ai投資級經濟指標使用指南
    日本日經225指數](https://stock-ai.com/dly-2-%5EN225.php)

  - [雅虎 日本日經225指數](http://finance.yahoo.com/q?s=%5EN225&d=t)

[Category:日本經濟](../Category/日本經濟.md "wikilink")
[Category:日本股市指数](../Category/日本股市指数.md "wikilink")
[Category:日本經濟新聞](../Category/日本經濟新聞.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:香港交易所上市牛熊證](../Category/香港交易所上市牛熊證.md "wikilink")
[\*](../Category/日經平均指數.md "wikilink")
[日](../Category/日本公司列表.md "wikilink")