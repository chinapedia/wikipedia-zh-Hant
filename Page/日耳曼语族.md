**日耳曼語族**是[印歐語系的一支](../Page/印歐語系.md "wikilink")，是居住在北部[歐洲](../Page/歐洲.md "wikilink")[日耳曼民族的](../Page/日耳曼人.md "wikilink")[語族](../Page/語族.md "wikilink")。這一族[語言有鮮明的特徵](../Page/語言.md "wikilink")，最著名的有關於[輔音演變的](../Page/輔音.md "wikilink")[格里姆定律](../Page/格里姆定律.md "wikilink")。

## 書寫

一些早期（約公元2世紀）的日耳曼語言發展出自己的[盧恩字母](../Page/盧恩字母.md "wikilink")（**runic
alphabet**，北歐文字），但這些文字相對來說運用並不廣泛。[東日耳曼語支使用](../Page/東日耳曼語支.md "wikilink")[哥特字母](../Page/哥特字母.md "wikilink")，由[烏斐拉主教將](../Page/烏斐拉主教.md "wikilink")[聖經譯為](../Page/聖經.md "wikilink")[哥特語時發展創立](../Page/哥特語.md "wikilink")。其後，因為[基督教神甫與僧侶既講日耳曼語](../Page/基督教.md "wikilink")，也能夠讀說[拉丁語](../Page/拉丁語.md "wikilink")，所以開始用稍加修飾的拉丁字母來書寫日耳曼語言。

除去標準拉丁字母，各種日耳曼語言也使用一些標音符號和其他字母。其中包括[元音變音](../Page/日耳曼語元音變音.md "wikilink")（**umlaut**）、[ß](../Page/ß.md "wikilink")
（*Eszett*）、
[Ø](../Page/Ø.md "wikilink")、[Æ](../Page/Æ.md "wikilink")、[Å](../Page/Å.md "wikilink")、[Ð](../Page/Ð.md "wikilink")、[Ȝ和從如尼文中繼承下來的](../Page/Ȝ.md "wikilink")[Þ及](../Page/Þ.md "wikilink")[Ƿ](../Page/Ƿ.md "wikilink")。傳統的印刷體德語經常用黑體字。

## 語言標記

日耳曼語族一些特徵如下：

1.  將[印歐語系的時態體系削減為過去時與現在時](../Page/印歐語系.md "wikilink")（或一般時）。
2.  附加[齒音後綴](../Page/齒音.md "wikilink")（/d/或/t/）來表示過去時態，而不用[元音變換](../Page/元音變換.md "wikilink")（[印歐元音變換](../Page/印歐元音變換.md "wikilink")）。
3.  有兩種動詞變位：/弱變位（附加齒音後綴）與[不規則變位](../Page/不規則變位.md "wikilink")/強變位（元音交替）。[英語共有](../Page/英語.md "wikilink")161個不規則動詞/強動詞，都屬於英語本語辭源。
4.  使用強[形容詞與弱形容詞](../Page/形容詞.md "wikilink")。近代英語的形容詞一般不改變，除非用在比較級或最高級上。古英語中不同，依據前面是否有冠詞或指示代詞來變化形容詞。
5.  遵循[格林定律的輔音演變](../Page/格林定律.md "wikilink")。
6.  有一些詞的詞源與其他印歐語言很難產生聯繫，但這些詞的變形體卻岀現在幾乎所有日耳曼語言中。
7.  [重音轉移到詞根上](../Page/重音.md "wikilink")。儘管英語的重音位置並不規則，但本語詞源的單詞重音經常是固定的，無論附加什麼詞綴。有人爭論這點是最重要的變化。

## 发展

一般認為所有的日耳曼語言都是從一個假設的[原始日耳曼語發展而來](../Page/原始日耳曼語.md "wikilink")。請注意，日耳曼語族下的分類很難準確定義；大多數都有漸變群的性質，鄰近的[方言互相能夠交流](../Page/方言.md "wikilink")，距離較遠的則不能。

### [历时](../Page/历时.md "wikilink")

### [共时](../Page/共时.md "wikilink")

[歐洲的日耳曼語
](https://zh.wikipedia.org/wiki/File:Europe_germanic-languages_2.PNG "fig:歐洲的日耳曼語          ")
在這裡列岀的，僅有最主要與最不尋常的方言，下方的鏈接中會有更廣泛的語言系譜。比如，下方的[低地撒克遜語中還有很多其他方言](../Page/低地撒克遜語.md "wikilink")，不僅僅是列岀的標準低地撒克遜語和門諾低地德語。

  - [西日耳曼語支](../Page/西日耳曼語支.md "wikilink")
      - [高地德語](../Page/高地德語.md "wikilink")
          - [古高地德語](../Page/古高地德語.md "wikilink")（Old High German）()
          - [德語](../Page/德語.md "wikilink")
              - [古法蘭克語](../Page/古法蘭克語.md "wikilink")（Old Frankish）()
              - [中部德語](../Page/中部德語.md "wikilink")
                  - [東中部德語](../Page/東中部德語.md "wikilink")
                      - [德語](../Page/德語.md "wikilink")，[標準德語](../Page/標準德語.md "wikilink")
                        ()
                      - [低地西里西亞語](../Page/低地西里西亞語.md "wikilink")（Lower
                        Silesian German）()
                      - [上撒克遜語](../Page/上撒克遜語.md "wikilink")（Upper Saxon
                        German）()
                  - [西中部德語](../Page/西中部德語.md "wikilink")
                      - [盧森堡語](../Page/盧森堡語.md "wikilink") ()
                      - [法蘭克語](../Page/法蘭克語.md "wikilink")（Mainfränkisch）()
                      - [巴列丁奈特德語](../Page/巴列丁奈特德語.md "wikilink")
                        （Pfaelzisch）()
                      - [林堡語](../Page/林堡語.md "wikilink") ()
                      - [庫爾施語](../Page/庫爾施語.md "wikilink")（Kölsch）()
                      - [賓夕法尼亞德語](../Page/賓夕法尼亞德語.md "wikilink") ()
              - [上德語](../Page/上德語.md "wikilink")
                  - [倫巴底日耳曼語](../Page/倫巴底日耳曼語.md "wikilink") ()
                  - [阿勒曼尼語](../Page/阿勒曼尼語.md "wikilink")
                      - [斯瓦比亞語](../Page/斯瓦比亞語.md "wikilink")（Swabian
                        German）()
                      - [阿勒曼尼殖民語](../Page/阿勒曼尼殖民語.md "wikilink")（German,
                        Colonia Tovar，在委內瑞拉）()
                      - [瑞士德語](../Page/瑞士德語.md "wikilink")（Schwyzerdütsch）()
                      - [瓦爾瑟語](../Page/瓦爾瑟語.md "wikilink")（Walser）()
                  - [奧地利-巴伐利亞語](../Page/奧地利-巴伐利亞語.md "wikilink")（Austro-Bavarian）
                      - [巴伐利亞語](../Page/巴伐利亞語.md "wikilink")（Bavarian）()
                      - [辛布里語](../Page/辛布里語.md "wikilink")（Cimbrian）()
                      - [哈特德語](../Page/哈特德語.md "wikilink")（Hutterite
                        German, 又名 Tirolean）()
                      - [默切諾語](../Page/默切諾語.md "wikilink")（Mócheno）()
          - [意第緒語](../Page/意第緒語.md "wikilink") ()
              - 東意第緒語 ()
              - 西意第緒語 ()
          - Wymysorys ()
      - [低地日耳曼語](../Page/低地日耳曼語.md "wikilink")
          - [低地法蘭克語](../Page/低地法蘭克語.md "wikilink")
              - [古低地法蘭克語](../Page/古低地法蘭克語.md "wikilink")（Old Low
                Franconian）()
              - [中古荷蘭語](../Page/中古荷蘭語.md "wikilink")（Middle Dutch）()
              - [荷蘭語](../Page/荷蘭語.md "wikilink") ()
              - [佛蘭芒語](../Page/佛蘭芒語.md "wikilink")（Vlaams）()
              - [西蘭語](../Page/西蘭語.md "wikilink")（Zeeuws）()
              - [南非荷蘭語](../Page/南非荷蘭語.md "wikilink") ()
          - [低地德語](../Page/低地德語.md "wikilink")
              - [中古低地德語](../Page/中古低地德語.md "wikilink") ()
              - [古撒克遜語](../Page/古撒克遜語.md "wikilink") ()
              - Achterhoeks ()
              - Drents ()
              - Gronings ()
              - [低地撒克遜語](../Page/低地撒克遜語.md "wikilink") ()
              - [門諾低地德語](../Page/門諾低地德語.md "wikilink")（Plautdietsch 或
                Mennonite Low German）()
              - Sallands ()
              - Stellingwerfs ()
              - Twents ()
              - Veluws ()
              - [西發利亞話](../Page/西發利亞話.md "wikilink")（Westphalien，地名譯作[威斯特法倫](../Page/威斯特法倫.md "wikilink")）()
          - [東弗里西語](../Page/東弗里西語.md "wikilink") ()
      - 盎格魯-弗里西語
          - [古弗里西語](../Page/古弗里西語.md "wikilink") ()
          - [弗里西語](../Page/弗里西語.md "wikilink")、[西弗里西語](../Page/西弗里西語.md "wikilink")
            ()
          - [北弗里西語](../Page/北弗里西語.md "wikilink") ()
          - [沙特弗里西語](../Page/沙特弗里西語.md "wikilink")（Sateresisch）()
          - [古英语](../Page/古英语.md "wikilink")、[盎格鲁-撒克遜语](../Page/盎格鲁-撒克遜语.md "wikilink")
            ()
              - [中古英語](../Page/中古英語.md "wikilink") ()
                  - [英语](../Page/英语.md "wikilink") ()
                  - [低地蘇格蘭語](../Page/低地蘇格蘭語.md "wikilink") ()
              - Yola（愛爾蘭東南部）
  - [東日耳曼語支](../Page/東日耳曼語支.md "wikilink")（從[哥特語繼承下來](../Page/哥特語.md "wikilink")）
      - [哥德語](../Page/哥德語.md "wikilink")（已消亡）()
          - [克里米亞哥特語](../Page/克里米亞哥特語.md "wikilink")（1800年代滅亡）
      - [汪達爾語](../Page/汪達爾語.md "wikilink")（滅亡）()
      - [勃艮第日耳曼語](../Page/勃艮第日耳曼語.md "wikilink")（Burgundian，滅亡）
  - [北日耳曼語支](../Page/北日耳曼語支.md "wikilink")（又稱斯堪的納維亞語支，從[古諾爾斯語繼承下來](../Page/古諾爾斯語.md "wikilink")）
      - 西（島嶼）北歐語
          - [古諾爾斯語](../Page/古諾爾斯語.md "wikilink") ()
          - [冰島語](../Page/冰島語.md "wikilink") ()
          - [法羅語](../Page/法羅語.md "wikilink") ()
          - [新挪威語](../Page/新挪威語.md "wikilink")（Nynorsk）()
          - [諾恩語](../Page/諾恩語.md "wikilink")（滅亡）()
      - 東（大陸）北歐語
          - Danish-Swedish
              - [書面挪威語](../Page/書面挪威語.md "wikilink")（Bokmål）()
              - 丹麥語
                  - [丹麥語](../Page/丹麥語.md "wikilink") ()
                  - [日德蘭語](../Page/日德蘭語.md "wikilink")（Jutish，又稱西丹麥語）()
              - [瑞典語](../Page/瑞典語.md "wikilink") ()
          - 哥特蘭語（Gutnish）

## 参考文献

## 外部連結

  - [原始日耳曼語重建組](http://groups.yahoo.com/group/theudiskon)

[日耳曼語族](../Category/日耳曼語族.md "wikilink")
[Category:印欧语系](../Category/印欧语系.md "wikilink")