**日野汽车股份有限公司**（[英文](../Page/英文.md "wikilink")：**Hino Motors,
Ltd.**；[日文](../Page/日文.md "wikilink")：**-{日野自動車}-**）簡稱**日野汽車**或**日野**，是一家位於[日本](../Page/日本.md "wikilink")[东京的](../Page/东京.md "wikilink")[柴油](../Page/柴油.md "wikilink")[貨車](../Page/貨車.md "wikilink")、[巴士和其它车辆的制造商](../Page/巴士.md "wikilink")。日野在日本的中重型柴油卡车制造领域中佔据着领导地位。日野目前是[豐田集團](../Page/豐田集團.md "wikilink")（Toyota
Group）的成員之一，[豐田汽車持有](../Page/豐田汽車.md "wikilink")50.1%股權。
[Headquarters_of_Hino_Motors_in_Hinodai_20141221.jpg](https://zh.wikipedia.org/wiki/File:Headquarters_of_Hino_Motors_in_Hinodai_20141221.jpg "fig:Headquarters_of_Hino_Motors_in_Hinodai_20141221.jpg")

## 历史

1910年，建立东京瓦斯工业公司（1913年改稱[東京瓦斯電力工業公司](../Page/東京瓦斯電力工業.md "wikilink")）。

1918年，生产的第一辆汽车是TGE-A型卡车。

1937年，将其汽车制造部门与「汽车工业股份有限公司」（Automobile Industry Co.,
Ltd）和「共同國產汽車股份有限公司」（Kyodo Kokusan
K.K.）合併成立了「东京汽车工业股份有限公司」（Tokyo Automoble Industry Co., Ltd.）。

1941年，公司改名為「柴油汽车工业股份有限公司」（Diesel Motor Industry Co.,
Ltd.），并最终变成了现在的[五十鈴汽車股份有限公司](../Page/五十鈴汽車.md "wikilink")（Isuzu
Motors Limited）。

1942年，新的实体日野重工股份有限公司（Hino Heavy Industry Co.,
Ltd.）从柴油汽车工业股份有限公司中分离出来，日野的名称就此诞生。[第二次世界大战以後](../Page/第二次世界大战.md "wikilink")，公司停止生产船用大型柴油发动机，并根据条约去掉了公司名称中的“重型”。从此公司以日野工业股份有限公司（Hino
Industry Co., Ltd.）的名称专心制造重型拖掛卡車和[柴油发动机](../Page/柴油发动机.md "wikilink")。

1948年，公司又改名為「日野柴油工业股份有限公司」（Hino Diesel Industry Co., Ltd.）。最初是生產私家車為主，

1967年，成為豐田汽車一部分後，以生產貨車、巴士為主。

## 產品

  - 私家車：早期的日野公司主力是生產私家車，自1967年成為豐田汽車一部分後，便再沒有生產私家車。
  - 貨車：自1967年成為豐田汽車一部分後，日野主要是生產中型和重型貨車為主，到21世紀又同時生產5.5噸輕型貨車，而輕型貨車亦為工商服務業界採用。其中重型拖頭乃日野的「金漆招牌」，而[日野Dutro更加與](../Page/日野Dutro.md "wikilink")[豐田Dyna發揮並駕齊驅之載貨動力](../Page/豐田Dyna.md "wikilink")，前後發展了多個版本，暢銷亞洲各國。同時日野還為北美洲市場研製出前置引擎版本的貨車系列。
  - 巴士：主要有前置和後置引擎為主。

[File:HINO.svg|上一代日野標誌](File:HINO.svg%7C上一代日野標誌) <File:萬華> 1
吳興街176FP.JPG|[欣欣客運HINO](../Page/欣欣客運.md "wikilink")
[RK8JMSA](../Page/日野RK8J.md "wikilink") 233HP(2009年) <File:419FS>
9023(6).JPG|[台北客運HINO](../Page/台北客運.md "wikilink") RN8JSSA 263HP(2009年)
<File:SanChungBus> 362FE
Front.jpg|[三重客運HINO](../Page/三重客運.md "wikilink") LRM2PSA
300HP(2005年)
[File:TaipeiCity_TCPD_BD602_Front-2.jpg|日野巴士也被用於警備車的用途](File:TaipeiCity_TCPD_BD602_Front-2.jpg%7C日野巴士也被用於警備車的用途)
[File:HINO-4CV-01.jpg|日野4CV](File:HINO-4CV-01.jpg%7C日野4CV) <File:Taipei>
City Police Department abandoned Hino truck 20160425a.jpg|警用卡車
[File:Fireengine40.jpg|Profia](File:Fireengine40.jpg%7CProfia)
[File:ADG-RU1ESAA-Fujikyu-T2634.jpg|S\`elega](File:ADG-RU1ESAA-Fujikyu-T2634.jpg%7CS%60elega)
<File:Hino> 700 of Taipei Bus Station 065-UM 20090819.jpg|台北轉運站救援車
<File:FAD-619> 桃園客運國瑞日野低地板.jpg|台灣首次引進日野HINO
[HS8JRVL-UTF低底盤公車](../Page/日野HS8J.md "wikilink")，圖中為[桃園客運車輛](../Page/桃園客運.md "wikilink")，由台灣固亞車體產製造。

## 現有車款

  - 巴士
      - Liésse（小型巴士）
      - Liésse II（小型巴士）
      - Poncho（小型低地台巴士）
      - Melpha
      - Rainbow
      - Rainbow II
      - Blue Ribbon City Hybrid（城市巴士）
      - Blue Ribbon II（城市巴士）
      - S\`elega（豪華旅遊巴士）
      - 前置引擎底盤（FB、FC）- 小巴士
      - 前置引擎底盤（AK、FF、FG）- 大巴士
      - 中置引擎底盤（BG、BX、CG、CM）- 大巴士
      - 後置引擎底盤（RC、RG、RM、RN、[RK](../Page/日野RK8J.md "wikilink")、RU、RF、HT、[HS](../Page/日野HS8J.md "wikilink")、HL）-
        大巴士
  - 貨車
      - [Dutro/300](../Page/日野Dutro.md "wikilink")
      - [Ranger/500](../Page/日野Ranger.md "wikilink")
      - [Profia/700](../Page/日野Profia.md "wikilink")

## 生產與經銷

  - [台灣](../Page/台灣.md "wikilink")：
      - [和泰汽車](../Page/和泰汽車.md "wikilink")（代理）
      - [長源汽車](../Page/和泰汽車.md "wikilink")（銷售）
      - [國瑞汽車](../Page/國瑞汽車.md "wikilink")（製造）
  - [香港](../Page/香港.md "wikilink")、[澳門及](../Page/澳門.md "wikilink")[華南地區](../Page/華南地區.md "wikilink")：皇冠車行有限公司
  - [中国大陆](../Page/中国大陆.md "wikilink")：广汽日野汽车有限公司

## 参考文献

## 外部链接

  -
  -
  -
  -
  -
  -
  -
  -
  -
[category:1942年成立的公司](../Page/category:1942年成立的公司.md "wikilink")

[Category:日本汽車公司](../Category/日本汽車公司.md "wikilink")
[Category:日野汽車](../Category/日野汽車.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:貨車生產商](../Category/貨車生產商.md "wikilink")
[Category:東京都公司](../Category/東京都公司.md "wikilink")
[Category:日野市](../Category/日野市.md "wikilink")
[Category:豐田集團](../Category/豐田集團.md "wikilink")
[Category:豐田汽車](../Category/豐田汽車.md "wikilink")