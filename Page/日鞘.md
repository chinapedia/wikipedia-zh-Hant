[Voyagers_in_the_heliosheath_region.jpg](https://zh.wikipedia.org/wiki/File:Voyagers_in_the_heliosheath_region.jpg "fig:Voyagers_in_the_heliosheath_region.jpg")

**日鞘**（）是在[日球層頂和](../Page/日球層頂.md "wikilink")[終端震波之間的區域](../Page/終端震波.md "wikilink")，是[太陽系外面的邊界](../Page/太陽系.md "wikilink")，分布在[太陽風創造出的氣泡邊緣](../Page/太陽風.md "wikilink")。

日鞘與[太陽的距離在](../Page/太陽.md "wikilink")80到100[天文單位](../Page/天文單位.md "wikilink")，目前还处于工作状态的[旅行者1号和](../Page/旅行者1号.md "wikilink")[旅行者2号正在对日鞘进行研究](../Page/旅行者2号.md "wikilink")。

在2005年5月，[美国宇航局宣布](../Page/美国宇航局.md "wikilink")[航海家一號已經在](../Page/航海家一號.md "wikilink")2004年12月（距離太陽94天文單位的地方）越過終端震波進入日鞘中，而在稍早的2002年8月，在距離85天文單位的報告，则言之過早了。

航海家二號於2007年八月跨越[終端震波並進入日鞘](../Page/終端震波.md "wikilink")。\[1\]

## 参考资料

## 外部連結

  - [Observing
    objectives](https://web.archive.org/web/20071007134225/http://science.nasa.gov/ssl/pad/solar/suess/Interstellar_Probe/ISP-ObservObj.html)
    of [NASA](../Page/NASA.md "wikilink")'s [Interstellar
    Probe](https://web.archive.org/web/20101210010939/http://interstellar.jpl.nasa.gov/).
  - [CNN: NASA: Voyager I enters solar system's final
    frontier](http://www.cnn.com/2005/TECH/space/05/25/voyager.space/index.html)
    - May 25, 2005
  - [*New Scientist*: Voyager 1 reaches the edge of the solar
    system](http://www.newscientist.com/article.ns?id=mg18625015.000) -
    May 25, 2005
  - [Surprises from the Edge of the Solar
    System](https://web.archive.org/web/20070308105257/http://science.nasa.gov/headlines/y2006/21sep_voyager.htm?list33024)
    - Voyager 1 Newest Findings as of September 2006

## 参见

  - [星際介質](../Page/星際介質.md "wikilink")

[de:Heliosphäre\#Heliosheath](../Page/de:Heliosphäre#Heliosheath.md "wikilink")
[en:Heliosphere\#Heliosheath](../Page/en:Heliosphere#Heliosheath.md "wikilink")

[Category:太阳](../Category/太阳.md "wikilink")
[Category:等离子体天体物理学](../Category/等离子体天体物理学.md "wikilink")

1.