**早田山毛櫸**（[学名](../Page/学名.md "wikilink")：**），又名**台灣水青岡**、**台灣山毛櫸**，屬[殼斗科](../Page/殼斗科.md "wikilink")[山毛櫸屬](../Page/山毛櫸屬.md "wikilink")，為[落葉性](../Page/落葉性.md "wikilink")[喬木](../Page/喬木.md "wikilink")，樹高可達20公尺，胸高直徑可達70公分，大多生長在山稜線附近，在[新北市](../Page/新北市.md "wikilink")、[桃園市和](../Page/桃園市.md "wikilink")[宜蘭縣一帶分布最多](../Page/宜蘭縣.md "wikilink")。

由於結實率偏低，種子不易發芽，屬於公告的台灣珍稀植物。日前成立的[插天山自然保留區中](../Page/插天山自然保留區.md "wikilink")，有大面積的山毛櫸林。

學名中的「早田」（はやた;
Hayata）乃為紀念[日治時代到](../Page/日治時代.md "wikilink")[台灣研究的植物學家](../Page/台灣.md "wikilink")[早田文藏](../Page/早田文藏.md "wikilink")。

## 參見

  - [臺灣保育物種列表](../Page/臺灣保育物種列表.md "wikilink")
  - [臺灣自然生態保護區](../Page/臺灣自然生態保護區.md "wikilink")

## 外部連結

  - [Fagus hayatae
    Palib.臺灣山毛櫸](http://catalog.digitalarchives.tw/item/00/5e/65/77.html)數位典藏聯合目錄
  - [追尋珍稀植物－台灣水青岡的蹤跡](https://web.archive.org/web/20160305014001/http://www.forest.gov.tw/ct.asp?xItem=27635&ctNode=486&mp=1)

[Category:水青冈属](../Category/水青冈属.md "wikilink")
[Fh](../Category/臺灣特有植物.md "wikilink")
[Category:台灣自然地景](../Category/台灣自然地景.md "wikilink")