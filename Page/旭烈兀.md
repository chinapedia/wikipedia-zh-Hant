**旭烈兀**（，名字意为“战士”，），[蒙古人](../Page/蒙古.md "wikilink")，[伊儿汗国的建立者](../Page/伊儿汗国.md "wikilink")，西南亚的征服者。[成吉思汗之孫](../Page/成吉思汗.md "wikilink")、[拖雷之子](../Page/拖雷.md "wikilink")、[蒙哥及](../Page/蒙哥.md "wikilink")[忽必烈之弟](../Page/忽必烈.md "wikilink")、[阿里不哥之兄](../Page/阿里不哥.md "wikilink")。旭烈兀的军队大大拓展了[蒙古帝国的西南疆界](../Page/蒙古帝国.md "wikilink")。在他的领导下，蒙古人摧毁了[伊斯兰文明的两大中心](../Page/伊斯兰.md "wikilink")：[巴格达和](../Page/巴格达.md "wikilink")[大马士革](../Page/大马士革.md "wikilink")，使伊斯兰世界的中心转移到了[开罗的埃及](../Page/开罗.md "wikilink")[马木留克王朝](../Page/马木留克王朝.md "wikilink")。

## 背景

旭烈兀是[拖雷的第六子](../Page/拖雷.md "wikilink")，与[蒙哥](../Page/蒙哥.md "wikilink")、[忽必烈](../Page/忽必烈.md "wikilink")、[阿里不哥是同母兄弟](../Page/阿里不哥.md "wikilink")。旭烈兀信奉[佛教](../Page/佛教.md "wikilink")，而他的母亲[唆鲁禾帖尼和妻子](../Page/唆鲁禾帖尼.md "wikilink")[脱古思可敦都是](../Page/脱古思可敦.md "wikilink")[景教徒](../Page/景教.md "wikilink")。

## 军事征服

1251年，旭烈兀的兄长蒙哥被拥立为蒙古[大汗](../Page/大汗.md "wikilink")。1252年，他命令旭烈兀率领庞大的蒙古军队征讨[西南亚的](../Page/西南亚.md "wikilink")[穆斯林国家](../Page/穆斯林.md "wikilink")。蒙哥汗在旭烈兀出征前嘱托他：“从[阿姆河两岸到](../Page/阿姆河.md "wikilink")[埃及尽头的土地都要遵循成吉思汗的习惯和法令](../Page/埃及.md "wikilink")。对于顺从你命令的人要赐予恩惠，对于顽抗的人要让他们遭受屈辱。”后来的事实表明，旭烈兀重点执行了这句指示的最后部分。依据蒙哥汗的命令，旭烈兀集结了五分之一的帝国军队。同年，旭烈兀的先锋[怯的不花带领先头部队出发西征](../Page/怯的不花.md "wikilink")。次年（即1253年），旭烈兀率领主力部队渡过阿姆河。他的军队经历的主要战斗包括：对[伊朗南部](../Page/伊朗.md "wikilink")[卢尔人的征服](../Page/卢尔人.md "wikilink")；消灭[阿萨辛派](../Page/阿萨辛派.md "wikilink")；灭亡[巴格达的](../Page/巴格达.md "wikilink")[阿拔斯王朝](../Page/阿拔斯王朝.md "wikilink")；摧毁[叙利亚的](../Page/叙利亚.md "wikilink")[阿尤布国家](../Page/阿尤布王朝.md "wikilink")；以及最后，对[埃及的](../Page/埃及.md "wikilink")[伯海里系](../Page/伯海里系.md "wikilink")[马木留克王朝的作战](../Page/马木留克王朝.md "wikilink")。

### 册封为伊儿汗

1256年，[蒙哥大汗册封旭烈兀为伊儿汗](../Page/蒙哥.md "wikilink")。旭烈兀所建立的国家正式成为**[伊儿汗国](../Page/伊儿汗国.md "wikilink")**(1256年-1357年)。

### 巴格达之战

[DiezAlbumsFallOfBaghdad_b.jpg](https://zh.wikipedia.org/wiki/File:DiezAlbumsFallOfBaghdad_b.jpg "fig:DiezAlbumsFallOfBaghdad_b.jpg")

1257年，旭烈兀和他麾下大将[郭侃率领的军队抵达巴格达](../Page/郭侃.md "wikilink")。旭烈兀向巴格达的[哈里发](../Page/哈里发.md "wikilink")[穆斯台绥木劝降](../Page/穆斯台绥木.md "wikilink")，[阿拉伯人警告蒙古人说如果他们攻击哈里发就将受到](../Page/阿拉伯.md "wikilink")[真主的惩罚](../Page/安拉.md "wikilink")，拒绝[投降](../Page/投降.md "wikilink")。于是蒙古军队开始攻城。1258年2月10日，巴格达开城投降。蒙古军队展开了长达一个星期的[屠城](../Page/屠城.md "wikilink")，数十万居民在屠杀中丧生，哈里发穆斯台绥木被纵马踏死，阿拉伯文化历史名城巴格达遭受浩劫。这次屠城被认为是[伊斯兰历史中最具破坏性的事件之一](../Page/伊斯兰.md "wikilink")。

### 征服叙利亚

1259年到1260年，旭烈兀的军队（此时除了蒙古军队之外，已经收编了一批当地的[十字军国家军队](../Page/十字军国家.md "wikilink")）进攻阿尤布王朝控制的[叙利亚](../Page/叙利亚.md "wikilink")。1260年1月24日，攻陷[阿勒颇](../Page/阿勒颇.md "wikilink")。3月1日，怯的不花领军攻陷[大马士革](../Page/大马士革.md "wikilink")。这场战争终结了[阿尤布王朝](../Page/阿尤布王朝.md "wikilink")，随着巴格达和大马士革的先后陷落，伊斯兰世界的文明中心移往开罗。

### 阿音札鲁特战役

旭烈兀原先计划接下来将挥师经[巴勒斯坦直扑](../Page/巴勒斯坦.md "wikilink")[开罗](../Page/开罗.md "wikilink")，进攻[马木留克王朝](../Page/马木留克王朝.md "wikilink")。但就在此时，一个突发事件改变了[阿拉伯世界的命运](../Page/阿拉伯世界.md "wikilink")。旭烈兀获悉[蒙哥汗已于](../Page/蒙哥汗.md "wikilink")1259年年底在征戰[南宋時战死](../Page/南宋.md "wikilink")，诸兄弟陷入了汗位的争夺，决定率主力东归，只留下怯的不花率领不足万人的骑兵部队留守叙利亚。1260年9月3日，[怯的不花的蒙古军与](../Page/怯的不花.md "wikilink")[拜巴尔一世的埃及](../Page/拜巴尔一世.md "wikilink")[马木留克军在](../Page/马木留克.md "wikilink")[阿音札魯特交战](../Page/阿音札魯特.md "wikilink")，这场战役最终以埃及军队全胜而告终，这也宣告了蒙古帝国向西方的扩张的终结。

### 后期战争

1262年，旭烈兀在得知[忽必烈继位为大汗之后返回了](../Page/忽必烈.md "wikilink")[波斯](../Page/波斯.md "wikilink")，但此时他已经无暇向马木留克復仇了。他的军队在先前征戰中对巴格达所施的暴行激起了[金帐汗国信奉伊斯兰教的旭烈兀的堂哥](../Page/金帐汗国.md "wikilink")—[别儿哥汗的仇视](../Page/别儿哥.md "wikilink")。金帐汗国因而与马木留克王朝结成了同盟，為了制衡他們，伊兒汗國與[東羅馬帝國透過通婚結成同盟](../Page/拜占庭.md "wikilink")，反对伊斯蘭教。同年11-12月，别儿哥与旭烈兀在[高加索地区开战](../Page/高加索.md "wikilink")，互有胜负。从这以后，旭烈兀再也无法将注意力集中在埃及方向了。

## 统治时期

1256年，旭烈兀接受了[册封](../Page/册封.md "wikilink")，成为伊儿汗。他所建立的国家正式成为**伊儿汗国**。晚年的旭烈兀致力于巩固在波斯的统治，他通过武力胁迫或[联姻的手段](../Page/政治婚姻.md "wikilink")，清除了波斯境内的割据势力，使波斯成为伊儿汗国统治的核心地区。

### 宗教政策

旭烈兀的母亲与妻子信奉[景教](../Page/景教.md "wikilink")，旭烈兀受到她们的影响，在国内施行亲基督教、反穆斯林的宗教政策。此外，由于伊斯兰教的作用，原本当时波斯地区[佛教已基本上消失](../Page/佛教.md "wikilink")，但在旭烈兀統治時代，由于他本人信奉佛教，崇拜[弥勒佛](../Page/弥勒佛.md "wikilink")，当地出現了大量[吐蕃與](../Page/吐蕃.md "wikilink")[畏兀兒的](../Page/畏兀兒.md "wikilink")[喇嘛](../Page/喇嘛.md "wikilink")，佛教活動再次出現。

### 与[元朝的联系](../Page/元朝.md "wikilink")

由于旭烈兀与忽必烈的[血缘关系](../Page/血缘关系.md "wikilink")，[伊儿汗国与](../Page/伊儿汗国.md "wikilink")[元朝的联系远比](../Page/元朝.md "wikilink")[蒙古四大汗国的其他两国要紧密](../Page/蒙古四大汗国.md "wikilink")。[丝绸之路通畅](../Page/丝绸之路.md "wikilink")，中国的[四大发明加快了西传的速度](../Page/四大发明.md "wikilink")，而[回回炮](../Page/回回炮.md "wikilink")、[阿拉伯数字](../Page/阿拉伯数字.md "wikilink")、[阿拉伯历法等也传入中国](../Page/阿拉伯历法.md "wikilink")。旭烈兀对中国历史发展的影响不容忽视。

### 与欧洲的联系

从1262年开始，旭烈兀数次派遣使者携带书信前往[欧洲](../Page/欧洲.md "wikilink")，试图与基督教国家建立[军事同盟](../Page/军事同盟.md "wikilink")。在旭烈兀写给[法国国王](../Page/法国.md "wikilink")[路易九世的信中](../Page/路易九世_\(法兰西\).md "wikilink")，他表示愿意攻下[耶路撒冷作为赠送给](../Page/耶路撒冷.md "wikilink")[教皇的礼物](../Page/教皇.md "wikilink")，作为回报，他希望法国能够派出一支[舰队来攻击埃及](../Page/舰队.md "wikilink")。可是由于各种原因，旭烈兀和他的继任者们最终没能与欧洲建立任何形式的联盟。

## 逝世

1265年2月8日，旭烈兀在[马拉盖逝世](../Page/马拉盖.md "wikilink")。他被安葬在[尔米亚湖中的小岛上](../Page/尔米亚湖.md "wikilink")。他的葬礼是所有伊儿汗中唯一使用了活人陪葬的。之后不久，他的妻子也相继去世。东方基督教各派都因此感到有所损失，他们以深情的话语来悼念他们：“基督教的两颗巨星”、“又一位[君士坦丁](../Page/君士坦丁一世.md "wikilink")，又一位[海伦](../Page/聖海倫納.md "wikilink")”。

## 家庭

### 父母兄弟姐妹

  - 父母

<!-- end list -->

  - 父亲：[拖雷](../Page/拖雷.md "wikilink")，1227年—1229年帝位空缺时担任[大蒙古国](../Page/大蒙古国.md "wikilink")[监国](../Page/监国.md "wikilink")，1232年去世。1251年元宪宗蒙哥登基后追尊**拖雷**为皇帝，为拖雷上[庙号](../Page/庙号.md "wikilink")**睿宗**，[谥号](../Page/谥号.md "wikilink")**英武皇帝**，1266年[元世祖](../Page/元世祖.md "wikilink")[忽必烈改谥为](../Page/忽必烈.md "wikilink")**景襄皇帝**，1310年[元武宗海山加谥为](../Page/元武宗.md "wikilink")**仁圣景襄皇帝**。
  - 母亲：[唆鲁禾帖尼](../Page/唆鲁禾帖尼.md "wikilink")，是[蒙哥](../Page/蒙哥.md "wikilink")，[忽必烈](../Page/忽必烈.md "wikilink")，**旭烈兀**，[阿里不哥四人的生母](../Page/阿里不哥.md "wikilink")，1251年元宪宗蒙哥登基后尊为[皇太后](../Page/皇太后.md "wikilink")，1252年去世。1266年[元世祖上](../Page/元世祖.md "wikilink")[谥号](../Page/谥号.md "wikilink")**庄圣皇后**，1310年[元武宗海山加谥为](../Page/元武宗.md "wikilink")**显懿庄圣皇后**。她的四个儿子皆曾称帝，被后世史学家尊称为“[四帝之母](../Page/四帝之母.md "wikilink")”。

<!-- end list -->

  - 兄弟\[1\]

<!-- end list -->

  - 大哥：[蒙哥](../Page/蒙哥.md "wikilink")，唆鲁禾帖尼所生，[元宪宗](../Page/元宪宗.md "wikilink")，1251年—1259年为第四位[大蒙古国](../Page/大蒙古国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（[蒙古帝国](../Page/蒙古帝国.md "wikilink")[大汗](../Page/大汗.md "wikilink")）。1266年[元世祖忽必烈为](../Page/元世祖.md "wikilink")**蒙哥**上[庙号](../Page/庙号.md "wikilink")**宪宗**，[谥号](../Page/谥号.md "wikilink")**桓肃皇帝**。
  - 二哥：[忽睹都](../Page/忽睹都.md "wikilink")
  - 三哥失其名。
  - 四哥：[忽必烈](../Page/忽必烈.md "wikilink")，唆鲁禾帖尼所生，[元世祖](../Page/元世祖.md "wikilink")，1260年—1294年为第五位[大蒙古国](../Page/大蒙古国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（[蒙古帝国](../Page/蒙古帝国.md "wikilink")[大汗](../Page/大汗.md "wikilink")），[元朝的实际开创者](../Page/元朝.md "wikilink")，1271年将国号“大蒙古国”改为“大元”。
  - 五哥失其名。
  - 七弟：[阿里不哥](../Page/阿里不哥.md "wikilink")，唆鲁禾帖尼所生，1260年—1264年和忽必烈争位，1264年归降忽必烈
  - 八弟：[拨绰](../Page/拨绰.md "wikilink")（不者克）
  - 九弟：[末哥](../Page/末哥.md "wikilink")
  - 十弟：[岁哥都](../Page/岁哥都.md "wikilink")
  - 十一弟：[雪别台](../Page/雪别台.md "wikilink")

<!-- end list -->

  - 姐妹

<!-- end list -->

  - 赵国公主
    [薛不罕下嫁](../Page/薛不罕.md "wikilink")[聂古得](../Page/聂古得.md "wikilink")、[察忽](../Page/察忽.md "wikilink")
  - 鲁国公主
    [也速不花下嫁](../Page/也速不花.md "wikilink")[斡陈](../Page/斡陈.md "wikilink")
  - 鲁国公主
    [薛只干下嫁](../Page/薛只干.md "wikilink")[纳陈](../Page/纳陈.md "wikilink")（[斡陈的弟弟](../Page/斡陈.md "wikilink")）

### 妻妾儿子女儿

  - 妻妾

<!-- end list -->

  - 正妻[脱古思可敦](../Page/脱古思可敦.md "wikilink")
  - [亦孙真哈敦](../Page/亦孙真哈敦.md "wikilink")
  - [忽推哈敦](../Page/忽推哈敦.md "wikilink")

<!-- end list -->

  - 儿子

<!-- end list -->

  - 长子：[阿八哈](../Page/阿八哈.md "wikilink")，[亦孙真哈敦所生](../Page/亦孙真哈敦.md "wikilink")，1265年—1282年为[伊儿汗国第二任君主](../Page/伊儿汗国.md "wikilink")
  - 次子：出木哈儿
  - 三子：[尤舒穆特](../Page/雅失木忒.md "wikilink")，曾孙[速来蛮](../Page/速来蛮.md "wikilink")
  - 四子：帖赫申
  - 五子：[客儿来哥](../Page/塔剌海_\(伊利汗国\).md "wikilink")，子[拜都](../Page/拜都.md "wikilink")，拜都孙[木撒](../Page/木撒.md "wikilink")
  - 六子：秃布申
  - 七子：[贴古迭儿](../Page/贴古迭儿.md "wikilink")，[忽推哈敦所生](../Page/忽推哈敦.md "wikilink")，1282年—1284年为[伊儿汗国第三任君主](../Page/伊儿汗国.md "wikilink")
  - 八子：阿寨
  - 九子：空库斡台
  - 十子：伊苏捷尔
  - 十一子：蒙哥帖木儿，玄孙[麻合马](../Page/麻合马.md "wikilink")
  - 十二子：忽拉珠
  - 十三子：布勤
  - 十四子：托海帖木儿

<!-- end list -->

  - 女儿

## 參考文獻

  - \[波斯\]拉施特，《史集》第三卷，余大钧 译，北京：商务印书馆，1986年11月第1版

  - [Hulagu Khan](http://en.wikipedia.org/wiki/Hulagu_Khan)

  - \[法\]雷纳·格鲁塞，《蒙古帝国史》，北京：商务印书馆，2005

  - \[法\]雷纳·格鲁塞，《草原帝国》，北京：商务印书馆，2007

## 腳注

[X旭](../Category/蒙古族人物.md "wikilink")
[X旭](../Category/蒙古帝國皇族.md "wikilink")
[6](../Category/元睿宗皇子.md "wikilink")
[X旭](../Category/伊儿汗国君主.md "wikilink")
[X旭](../Category/軍事人物.md "wikilink")
[X旭](../Category/開國君主.md "wikilink")

1.  来自《[元史](../Page/元史.md "wikilink")》记载。