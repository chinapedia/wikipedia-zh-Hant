**旭龢道巴士總站**（[英文](../Page/英文.md "wikilink")：**Kotewall Road Bus
Terminus**）是位於[香港](../Page/香港.md "wikilink")[中西區](../Page/中西區_\(香港\).md "wikilink")[半山區的](../Page/半山區.md "wikilink")[巴士總站](../Page/巴士總站.md "wikilink")。

## 總站位置

位於[旭龢道與](../Page/旭龢道.md "wikilink")[大學道交界](../Page/大學道_\(香港大學\).md "wikilink")，為一路邊型巴士總站。

## 總站路線資料

### [新巴13線](../Page/新巴13線.md "wikilink")

  -
提供半山區（旭龢道、柏道、衛城道及堅道）往來中環的巴士服務，回程途經般咸道，於1974年4月16日起投入服務。

### [香港島專線小巴3A線](../Page/香港島專線小巴3A線.md "wikilink")

  -
## 途經路線資料

### [香港島專線小巴3號線](../Page/香港島專線小巴3號線.md "wikilink")

  -
## 鄰近地點

  - [香港大學](../Page/香港大學.md "wikilink")

## 參考資料

  - 《通用乘車地圖》第四版，香港通用圖書有限公司出版
  - 二十世紀港島區巴士路線發展史》，容偉釗編著，BSI出版

## 最新消息

  - 因受到道路工程影響，在旭龢道的3個巴士站可能會被停用。

## 外部連結

  - [新巴網頁－車站資料](http://www.nwfb.com.hk/image/share/busstop/pic.asp?pic=301236.jpg)

[Category:半山區](../Category/半山區.md "wikilink")
[Category:中西區巴士總站](../Category/中西區巴士總站.md "wikilink")