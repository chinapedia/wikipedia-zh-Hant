[Tropaeolum_majus.JPG](https://zh.wikipedia.org/wiki/File:Tropaeolum_majus.JPG "fig:Tropaeolum_majus.JPG")
**旱金蓮**（[學名](../Page/學名.md "wikilink")：*Tropaeolum
majus*）又名**金蓮花**，[十字花目](../Page/十字花目.md "wikilink")[旱金蓮科](../Page/旱金蓮科.md "wikilink")[旱金蓮屬植物](../Page/旱金蓮屬.md "wikilink")，與另一個同名的[金蓮花](../Page/金蓮花.md "wikilink")（*Trollius
chinensis*，[毛茛目](../Page/毛茛目.md "wikilink")[毛茛科](../Page/毛茛科.md "wikilink")[金蓮花屬](../Page/金蓮花屬.md "wikilink")）不同。

在台灣俗稱的金蓮花，實際上是**旱金莲**。

### 原產地

[南美洲](../Page/南美洲.md "wikilink")、[秘魯](../Page/秘魯.md "wikilink")、[玻利維亞至](../Page/玻利維亞.md "wikilink")[哥倫比亞的](../Page/哥倫比亞.md "wikilink")[安地斯山脈中](../Page/安地斯山脈.md "wikilink")。

### 繁殖

繁殖方式有[播種和](../Page/播種.md "wikilink")[扦插兩種](../Page/扦插.md "wikilink")。種子很大，容易播種，發芽適溫為15-20℃，一般來說，[春](../Page/春.md "wikilink")、[秋兩季均是播種適期](../Page/秋.md "wikilink")，但春季太晚播種遇到夏季高溫會不利開花，播種前種子先浸水二十四小時較容易發芽，種子有嫌光性，播後要略為覆土，約十天後可發芽，待本葉二至三枚時即可移植。
　 行扦插繁殖時，於春季剪取嫩莖插於沙床，栽培期間可用速大多稀釋1000倍後每五至十天噴灑一次促進發根。

### 品種

[皮奇.梅爾巴](../Page/皮奇.梅爾巴.md "wikilink")（PeachMelba），株高25～30厘米，花深橙色。米色草莓（StrawberriesandCream），株高25～30厘米，花米黃色，中心有橙色斑點，耐乾旱。

[冰草莓](../Page/冰草莓.md "wikilink")（StrawberriesIce），株高25厘米，花橙色，中心具紅色斑點，葉藍綠色。精華（TIPTOP），株高25～30厘米，花大，花徑6厘米，花色有橙、橙紅、黃和黑色。

[精華](../Page/精華.md "wikilink")（TIPTOP），株高25～30釐米，花大，花徑6釐米，花色有橙、橙紅、黄和黑色。

[湯姆拇指](../Page/湯姆拇指.md "wikilink")（TomThumb），矮生種，株高15～20厘米，花朵緊湊，多色。

半重瓣花:

[非洲寶石](../Page/非洲寶石.md "wikilink")（JewelofAfrica）蔓長1．2～1．5米，葉片有紅色斑紋，花色有鮮紅、橙、黃、棕、米色和雙色等；

[直升飛機](../Page/直升飛機.md "wikilink")（Whirly－bird），株高30厘米，花色玫瑰紅、金黃、橙、紅等；

[米色直升飛機](../Page/米色直升飛機.md "wikilink")（WhirlybirdCream），株高30厘米，花米黃色，葉片深綠色。

觀葉觀花種:

[阿拉斯加](../Page/阿拉斯加.md "wikilink")（Alaska），株高30～40厘米，葉片具黃白色斑紋，花色有橙、黃、杏黃、玫瑰紅、粉紅、鮮紅和米黃色等；

[印度女皇](../Page/印度女皇.md "wikilink")（EmpressofIndia），株高25～30厘米，葉片淡紫色至藍色，花硃紅色，緊湊。

重瓣花:

[重瓣閃光](../Page/重瓣閃光.md "wikilink")（DeubleGleam），株高20厘米，花橙、黃、橙紅等色；

[重瓣矮寶石](../Page/重瓣矮寶石.md "wikilink")（DoubleDwarfJewel），株高30厘米，花有金黃、橙紅、淡黃、深紅和玫瑰紅等色。

常見同屬觀賞種:

[鉤狀金蓮花](../Page/鉤狀金蓮花.md "wikilink")（T．aduncum），花黃色，夏季開花。

[天藍金蓮花](../Page/天藍金蓮花.md "wikilink")（T．azureum），花藍、綠和白色，10月開花。

[小金蓮花](../Page/小金蓮花.md "wikilink")（T．minus），花黃、紅色，夏季開花。

[五葉金蓮花](../Page/五葉金蓮花.md "wikilink")（T．pentapHyllum），花紫色，夏季開花。

[多葉金蓮花](../Page/多葉金蓮花.md "wikilink")（T．polypHyllum），花黃色，6月開花。

[美麗金蓮花](../Page/美麗金蓮花.md "wikilink")（T.speciosum），花紅色，夏季開花。　

### 營養成分和功效

在國外，金蓮花是最具代表性的[食用花之一](../Page/食用花.md "wikilink")，新鮮的[葉和](../Page/葉.md "wikilink")[花可拌](../Page/花.md "wikilink")[沙拉食用](../Page/沙拉.md "wikilink")，具有像似[山葵辛辣味](../Page/山葵.md "wikilink")。，含有豐富的[維他命C和](../Page/維他命C.md "wikilink")[鐵質](../Page/鐵質.md "wikilink")。而由金蓮花提煉出的[精油成分據說有增強元氣](../Page/精油.md "wikilink")，改善情緒的功能。富含的[維生素](../Page/維生素.md "wikilink")、[胡蘿蔔素以及其他微量元素能有效補充細胞的營養](../Page/胡蘿蔔素.md "wikilink")，既可[活血養顏](../Page/活血養顏.md "wikilink")，也有[提神醒腦的作用](../Page/提神醒腦.md "wikilink")。

金蓮花富含[生物鹼和](../Page/生物鹼.md "wikilink")[黃酮類物質](../Page/黃酮類物質.md "wikilink")，在御用配研基礎上經科學研製加工而成的金蓮花茶，具有[消炎止渴](../Page/消炎止渴.md "wikilink")、[清喉利咽](../Page/清喉利咽.md "wikilink")、[清熱解毒](../Page/清熱解毒.md "wikilink")、[滋陰降火](../Page/滋陰降火.md "wikilink")、[排毒養顏](../Page/排毒養顏.md "wikilink")、[養陰清熱和](../Page/養陰清熱.md "wikilink")[消火殺菌的功效](../Page/消火殺菌.md "wikilink")，對[慢性咽炎](../Page/慢性咽炎.md "wikilink")、[喉炎](../Page/喉炎.md "wikilink")、[扁桃體炎和](../Page/扁桃體炎.md "wikilink")[聲音嘶啞者有消炎](../Page/聲音嘶啞者.md "wikilink")，預防和治療作用。，對老年人[便秘有一定療效](../Page/便秘.md "wikilink")。

金蓮花味苦，性寒，無毒，含生物鹼、黃酮類，有[清熱解毒](../Page/清熱解毒.md "wikilink")，治[上呼吸道感](../Page/上呼吸道感.md "wikilink")、[扁桃體炎](../Page/扁桃體炎.md "wikilink")、[咽炎](../Page/咽炎.md "wikilink")、[急性中耳炎](../Page/急性中耳炎.md "wikilink")、[急性鼓膜炎](../Page/急性鼓膜炎.md "wikilink")、[急性淋巴管炎](../Page/急性淋巴管炎.md "wikilink")、[口瘡等功效](../Page/口瘡.md "wikilink")。

### 食用禁忌

金蓮花的功效之清熱解毒：干寒金蓮5克，將金蓮花放入杯中，用沸水沖泡，代茶飲用每日1--2劑，能清熱解毒，同時治[上呼吸道感](../Page/上呼吸道感.md "wikilink")、[扁桃體炎](../Page/扁桃體炎.md "wikilink")、[咽炎](../Page/咽炎.md "wikilink")，主要用於[咽喉腫痛](../Page/咽喉腫痛.md "wikilink")、[慢性扁桃體炎](../Page/慢性扁桃體炎.md "wikilink")、[癰腫瘡毒](../Page/癰腫瘡毒.md "wikilink")、[口瘡](../Page/口瘡.md "wikilink")、[目赤等症](../Page/目赤.md "wikilink")，如遇急性可以加量，但切勿長期加量飲用，長期飲用會[傷腎](../Page/傷腎.md "wikilink")。

## 外部連結

  - [旱金蓮,
    Hanjinlian](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00917)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[Category:旱金蓮科](../Category/旱金蓮科.md "wikilink")