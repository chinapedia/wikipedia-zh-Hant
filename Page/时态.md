**时**或**时态**（tempus、）在[语法裡是表示行为发生的时间和说话时的关系](../Page/语法.md "wikilink")。一般分为[過去时](../Page/過去时.md "wikilink")、[現在时](../Page/現在时.md "wikilink")、[将来时](../Page/将来时.md "wikilink")，通常也有與表示動作進行或終止的進行式和完成式等體貌一起相連用的情況。

時態連同[語氣](../Page/語氣.md "wikilink")、[語態](../Page/語態.md "wikilink")、[體貌和](../Page/体_\(语法\).md "wikilink")[人稱為動詞形式至少可能能夠表現出的](../Page/人稱.md "wikilink")5種語法特性。

有些語言，沒有時態的使用，如[分析語的](../Page/分析語.md "wikilink")[中文](../Page/中文.md "wikilink")，但必要時，仍有時間副詞的輔助。也有些語言，如[日文](../Page/日文.md "wikilink")，形容詞的詞形變化能表達出時間上的資訊，有著類似動詞的時態性質。還有些語言，如[俄語](../Page/俄語.md "wikilink")，一個單詞就能表現出時態和體貌。

## 例子

### 英语

  - 现在时：I eat.（我正進食）
  - 过去时：I ate.（我曾進食）
  - 将来时：I will eat.（我将進食）
  - 过去将来时：I would eat.（我曾将進食）

## 参看

  - [语法范畴](../Page/语法范畴.md "wikilink")
  - [体 (语法)](../Page/体_\(语法\).md "wikilink")

## 參考文獻

  - Bybee, Joan L., Revere Perkins, and William Pagliuca (1994) *The
    Evolution of Grammar: Tense, Aspect, and Modality in the Languages
    of the World*. University of Chicago Press.
  - Comrie, Bernard (1985) *Tense*. Cambridge University Press. \[ISBN
    0-521-28138-5\]
  - Guillaume, Gustave (1929) *Temps et verbe*. Paris: Champion.
  - Hopper, Paul J., ed. (1982) *Tense–Aspect: Between Semantics and
    Pragmatics*. Amsterdam: Benjamins.
  - Smith, Carlota (1997). The Parameter of Aspect. Dordrecht: Kluwer.
  - Tedeschi, Philip, and Anne Zaenen, eds. (1981) *Tense and Aspect*.
    (Syntax and Semantics 14). New York: Academic Press.

## 外部連結

  - [Tense Explained (with
    diagrams)](http://calleteach.wordpress.com/2010/02/03/tense/)
  - [English Aspectual forms in Various
    Tenses](http://www.EnglishTensesWithCartoons.com)
  - [Combinations of Tense, Aspect, and Mood in
    Greek](http://www.bcbsr.com/greek/gtense.html)
  - [English Grammar Overview - Tenses with
    Exercises](http://www.english-tenses.com)

[Category:语法](../Category/语法.md "wikilink")
[\*](../Category/語法時態.md "wikilink")
[Category:語言學的時間](../Category/語言學的時間.md "wikilink")