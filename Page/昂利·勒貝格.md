**昂利·莱昂·勒貝格**（，），[法國數學家](../Page/法國.md "wikilink")，最有名的貢獻是1902年提出的[勒貝格積分](../Page/勒貝格積分.md "wikilink")。勒貝格積分的出现拓宽了[积分学的研究范围](../Page/积分学.md "wikilink")。

## 生平

1875年6月28日，昂利·勒贝格出生于法国[瓦兹省](../Page/瓦兹省.md "wikilink")[博韦](../Page/博韦.md "wikilink")。勒貝格的父亲是[排字員](../Page/排字.md "wikilink")，在勒貝格小時便因[肺結核病逝](../Page/肺結核.md "wikilink")，靠勒贝格的母亲一人支撑家庭。勒貝格的健康也不太好。勒贝格的母亲是一名老师。他在家中从小就能接触到许多书籍。后来他在上小学时表现出数学天赋，他的一位指导老师和社区协商，先后资助他去博韦中学()、[圣路易高中](../Page/路易十四.md "wikilink")()和[路易大帝高中读书](../Page/路易大帝中学.md "wikilink")。\[1\]

1894年，他入讀[巴黎高等師範學院](../Page/巴黎高等師範學院.md "wikilink")，1897年取得文憑。後來，他在圖書館讀到[贝尔關於不連續函數的著作](../Page/勒内-路易·贝尔.md "wikilink")，認為這方面大有發展。

## 研究工作

## 参考资料

## 外部連結

  -
  -
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:数学分析师](../Category/数学分析师.md "wikilink")
[Category:法国数学家](../Category/法国数学家.md "wikilink")
[Category:普瓦捷大學教師](../Category/普瓦捷大學教師.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")

1.