[Ngong_Ping_Cable_Car_Terminal_View_201308.jpg](https://zh.wikipedia.org/wiki/File:Ngong_Ping_Cable_Car_Terminal_View_201308.jpg "fig:Ngong_Ping_Cable_Car_Terminal_View_201308.jpg")

**昂坪纜車站**（），是[昂坪360](../Page/昂坪360.md "wikilink")[纜車系統的終點站](../Page/索道.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[離島區](../Page/離島區.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[昂坪](../Page/昂坪.md "wikilink")，並連接[東涌纜車站](../Page/東涌站_\(昂坪360\).md "wikilink")。

## 車站結構

車站內只有一層，乘客下車後只需向前移步便到達[出口](../Page/出口.md "wikilink")。

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>月台</strong></p></td>
<td></td>
<td><p><a href="../Page/昂坪360.md" title="wikilink">昂坪360落車區域</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td><p>　　←</p></td>
<td><p>架空吊索路軌</p></td>
</tr>
<tr class="odd">
<td><p>↓</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>　　→</p></td>
<td><p>架空吊索路軌</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/昂坪360.md" title="wikilink">昂坪360上車區域</a>，往<a href="../Page/東涌站_(昂坪360).md" title="wikilink">東涌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>大堂</strong></p></td>
<td><p><u><strong>上車前</strong>將途經</u><a href="../Page/昂坪360.md" title="wikilink">昂坪360客務中心</a>、收費閘機<br />
<u><strong>下車後</strong>將途經</u><a href="../Page/昂坪360.md" title="wikilink">昂坪360禮品店</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>出入口</strong></p></td>
<td><p>可步行前往以下目的地：<br />
<a href="../Page/昂坪市集.md" title="wikilink">昂坪市集</a>、<a href="../Page/寶蓮寺.md" title="wikilink">寶蓮寺</a>、<a href="../Page/天壇大彿.md" title="wikilink">天壇大彿</a>、<a href="../Page/心經簡林.md" title="wikilink">心經簡林</a><br />
<a href="../Page/鳳凰徑.md" title="wikilink">鳳凰徑</a>（第三、四段）<br />
<a href="../Page/嶼巴.md" title="wikilink">嶼巴</a><a href="../Page/昂坪.md" title="wikilink">昂坪巴士站</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站大堂

### 車站出口

車站的出入口只有一個，它是位於[昂坪市集的盡頭](../Page/昂坪市集.md "wikilink")。

  - 車站周邊

<!-- end list -->

  - [昂坪市集](../Page/昂坪市集.md "wikilink")
  - [寶蓮寺](../Page/寶蓮寺.md "wikilink")
  - [心經簡林](../Page/心經簡林.md "wikilink")
  - [昂坪公共運輸交匯處](../Page/昂坪公共運輸交匯處.md "wikilink")

## 鄰接車站

## 接駁交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [的士](../Page/香港的士.md "wikilink")

<!-- end list -->

  - 大嶼山的士站

</div>

</div>

## 外部連結

[Category:昂坪](../Category/昂坪.md "wikilink")
[Category:離島區鐵路車站](../Category/離島區鐵路車站.md "wikilink")
[Category:昂坪360](../Category/昂坪360.md "wikilink")