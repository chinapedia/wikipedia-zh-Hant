**昂热**（），又譯為**翁傑**，位于[法国西北部](../Page/法国.md "wikilink")[曼恩河畔](../Page/曼恩河.md "wikilink")，是[卢瓦尔河地区大区](../Page/卢瓦尔河地区大区.md "wikilink")[曼恩-卢瓦尔省的省会城市](../Page/曼恩-卢瓦尔省.md "wikilink")，1999年时人口达151,279。

昂热市位处被[联合国教科文组织列为](../Page/联合国教科文组织.md "wikilink")[世界文化遗产的](../Page/世界遗产.md "wikilink")[卢瓦尔河谷西端](../Page/卢瓦尔河流域.md "wikilink")，是曾经的[法兰西王国的](../Page/法兰西王国.md "wikilink")[安茹行省的首府](../Page/安茹.md "wikilink")。

## 大學

[缩略图](https://zh.wikipedia.org/wiki/File:Palais_UCO.JPG "fig:缩略图")\]\]
昂熱為法國的大學城之一，设有公立的[昂熱大學](../Page/昂熱大學.md "wikilink")（；簡稱：昂大）與“半私立”的[西部天主教大學](../Page/西部天主教大學.md "wikilink")（；；簡稱：昂西或西天）兩所大學。
兩所大學皆有附設法語學習部CUFCo（）與CIDEF（）。除了學費的差別之外，教學環境也有所區分。

## 名人

  - [勒内一世](../Page/勒内一世.md "wikilink")
  - [让·博丹](../Page/让·博丹.md "wikilink")：哲学家、法理学家
  - [米歇尔·欧仁·谢弗勒尔](../Page/米歇尔·欧仁·谢弗勒尔.md "wikilink")：化学家
  - [约瑟夫·普鲁斯特](../Page/约瑟夫·普鲁斯特.md "wikilink")：化学家
  - [普罗斯珀·梅尼埃](../Page/普罗斯珀·梅尼埃.md "wikilink")：医师
  - [勒内·巴赞](../Page/勒内·巴赞.md "wikilink")：作家、教育家
  - [安德烈·巴赞](../Page/安德烈·巴赞.md "wikilink")：电影评论家
  - [埃尔韦·巴赞](../Page/埃尔韦·巴赞.md "wikilink")：作家
  - [亨利·迪蒂耶](../Page/亨利·迪蒂耶.md "wikilink")：作曲家
  - [雅克·卢西耶](../Page/雅克·卢西耶.md "wikilink")：作曲家、爵士钢琴演奏家

## 友好城市

  - [荷兰](../Page/荷兰.md "wikilink")[哈勒姆](../Page/哈勒姆.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[奥斯纳布吕克](../Page/奥斯纳布吕克.md "wikilink")

  - [马里](../Page/马里.md "wikilink")[巴马科](../Page/巴马科.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")[比萨](../Page/比萨.md "wikilink")

  - [英格兰](../Page/英格兰.md "wikilink")[威根](../Page/威根.md "wikilink")

  - [瑞典](../Page/瑞典.md "wikilink")[南泰利耶](../Page/南泰利耶.md "wikilink")

  - [西班牙](../Page/西班牙.md "wikilink")[塞维利亚](../Page/塞维利亚.md "wikilink")

  - [中国](../Page/中国.md "wikilink")[烟台](../Page/烟台.md "wikilink")

## 外部連結

  - [昂热旅游局](http://www.angersloiretourisme.com)

[A](../Category/曼恩-卢瓦尔省市镇.md "wikilink")
[昂热](../Category/昂热.md "wikilink")