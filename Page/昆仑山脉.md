**昆仑山脉**（；）西起[帕米尔高原东部](../Page/帕米尔高原.md "wikilink")，东到[柴达木河上游谷地](../Page/柴达木河.md "wikilink")，于东经97°至99°处与[巴颜喀拉山脉和](../Page/巴颜喀拉山脉.md "wikilink")[阿尼玛卿山](../Page/阿尼玛卿山.md "wikilink")（[积石山](../Page/积石山.md "wikilink")）相接，北邻[塔里木盆地与](../Page/塔里木盆地.md "wikilink")[柴达木盆地](../Page/柴达木盆地.md "wikilink")。山脉全长2,500余公里，宽130至200公里，[平均海拔](../Page/平均海拔.md "wikilink")5,500至6,000米，西窄东宽，总面积达50多万平方公里。一般认为最高峰是[崑崙山](../Page/崑崙山.md "wikilink")（[海拔](../Page/海拔.md "wikilink")6638[米](../Page/米.md "wikilink")），位於[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[和田地区南面](../Page/和田地区.md "wikilink")，但实际上[公格尔山](../Page/公格尔山.md "wikilink")（7719米）最高。

昆仑火山群是周邊山區、高原上罕見的火山山區，有超過70座火山坐落於此，其中最高的一座火山海拔高達5808公尺（），但因其位於高原上，因此實際上火山錐實體僅高於周邊高原約300公尺（僅海拔的5％），但若僅考量絕對高度（海拔）而不考量相對高度而言，該山為亞洲、中國第一高、東半球第二高的火山（僅次於[吉力馬扎羅山](../Page/吉力馬扎羅山.md "wikilink")），也是之一，與伊朗的[德馬峰並論](../Page/德馬峰.md "wikilink")，最後一次爆發時間為1951年5月27日\[1\]。

唐時，西域來的僕從時而被稱作「崑崙奴」（唐傳奇）。

## 名称

[Silk-Road_course.jpg](https://zh.wikipedia.org/wiki/File:Silk-Road_course.jpg "fig:Silk-Road_course.jpg")
[Kunlun_IMG_0541.jpg](https://zh.wikipedia.org/wiki/File:Kunlun_IMG_0541.jpg "fig:Kunlun_IMG_0541.jpg")
最早《[禹贡](../Page/禹贡.md "wikilink")》即记有昆仑，《[山海经](../Page/山海经.md "wikilink")·海内西经》描述说：“海内昆仑之虚，在西北、帝下之都。昆仑之虚，方八百里，高万仞。上有木禾，长五寻，大五围。面有九井，以玉为槛。面有九门，门有开明善守之，百神之所在。”《[淮南子](../Page/淮南子.md "wikilink")》认为：“昆仑之丘，或上倍之，是谓凉风之山，登之而不死；或上倍之，是谓悬圃，登之乃灵，能使风雨；或上倍之，乃维上天，登之乃神，是谓太帝之居。”[干寶](../Page/干寶.md "wikilink")《[搜神記](../Page/搜神記.md "wikilink")》卷十三-{云}-：「崑崙之墟，……其外絕以弱水之深，又環以炎火之山。山上有鳥獸草木，皆生育滋長於炎火之中，故有火浣布。」[王嘉](../Page/王嘉.md "wikilink")《[拾遺記](../Page/拾遺記.md "wikilink")》卷十-{云}-：「崑崙山有昆陵之地，其高出日月之上。山有九層，每層相去萬里。有雲氣，從下望之，如城闕之象。」

《[史记](../Page/史记.md "wikilink")．大宛传》中记载：“汉使穷河源，河源出于寘，其山多玉石，采来，天子案古图书，名河所出山曰崑崙云”。[蔡元定](../Page/蔡元定.md "wikilink")《发微论》：“凡山皆祖昆仑，分枝分脉，愈繁愈细，此万殊而一本也。”陶公《提脉赋》亦-{云}-：“大致察脉起自昆仑”。[崔鸿在](../Page/崔鸿.md "wikilink")《[十六国春秋](../Page/十六国春秋.md "wikilink")》中，称昆仑山为“海上之诸山之祖”、“天下名山僧占多”。《[古今图书集成](../Page/古今图书集成.md "wikilink")》载：“西王母所居宫阙，在龟山春山西那之都，昆仑之圃，阆风之苑。有城千里，玉楼十二，琼华之阙，光碧之堂，九层元室，紫翠丹房；左带瑶池，右环翠水。其山之下，弱水九重，洪涛万丈，非飙车羽轮，不可到也。”

## 地势

崑崙山脉西高东低，按地势分西、中、东3段。

  - 西昆仑山脉位于[帕米尔高原的东缘](../Page/帕米尔高原.md "wikilink")，北起中国、吉尔吉斯斯坦、塔吉克斯坦交界处的[外阿莱山脉](../Page/外阿莱山脉.md "wikilink")。西北-东南走向至[克里雅山口与中昆仑山脉分界](../Page/克里雅山口.md "wikilink")。位于西昆仑山海拔在7000米以上的山峰有3座，6000米以上的山峰有7座，平均海拔为5500～6000米。[公格尔峰海拔](../Page/公格尔峰.md "wikilink")7649米，[公格尔九别峰海拔](../Page/公格尔九别峰.md "wikilink")7530米，[慕士塔格峰](../Page/慕士塔格峰.md "wikilink")7509米。昆仑山脉西侧与之平行的[喀喇昆仑山脉更为高大雄峻](../Page/喀喇昆仑山脉.md "wikilink")，二者之间为海拔3100～3900米[塔什库尔干谷地与](../Page/塔什库尔干谷地.md "wikilink")[叶尔羌河谷](../Page/叶尔羌河.md "wikilink")。
  - 中昆仑山脉西起[克里雅山口与西昆仑山脉分界](../Page/克里雅山口.md "wikilink")，东至[车尔臣河九个大坂山与东昆仑山与](../Page/车尔臣河.md "wikilink")[阿尔金山脉相接](../Page/阿尔金山脉.md "wikilink")。即东经77°～86°。主脉东西向向南略呈弧形。中昆仑山海拔6000米以上的山峰有8座，平均海拔5000～5500米
  - 东昆仑山脉从[喀拉米兰山口自西向东略呈扇形展开](../Page/喀拉米兰山口.md "wikilink")，分为3支：
      - 北支[祁漫塔格山](../Page/祁漫塔格.md "wikilink")，其南隔以[阿牙克库木盆地](../Page/阿牙克库木盆地.md "wikilink")；东延为[唐松乌拉山](../Page/唐松乌拉山.md "wikilink")、[布尔汗布达山](../Page/布尔汗布达山.md "wikilink")；
      - 中支[阿尔格山](../Page/阿尔格山.md "wikilink")，东延为[博卡雷克塔格](../Page/博卡雷克塔格.md "wikilink")、[唐格乌拉山与](../Page/唐格乌拉山.md "wikilink")[布青山](../Page/布青山.md "wikilink")，与[阿尼玛卿山相接](../Page/阿尼玛卿山.md "wikilink")；
      - 南支[可可西里山](../Page/可可西里山.md "wikilink")，与[巴颜喀拉山相接](../Page/巴颜喀拉山.md "wikilink")

[克里雅山口位于昆仑山的西段与中段的山势走向转折点](../Page/克里雅山口.md "wikilink")，西昆仑山自帕米尔高原自西北向东南迤逦而来，在克里雅山口转折为自西向东走势的中昆仑山。克里雅山口西侧的高峰(Kunlun
Goddess
Mt.)，海拔7167米，谷歌地球上的坐标是北纬35度18分51.03秒，东经80度55分00.55秒，是[和田地区](../Page/和田地区.md "wikilink")[玉龙喀什河的源头](../Page/玉龙喀什河.md "wikilink")。如果把更高的公格尔山与慕士塔格山算作帕米尔高山群，而把昆仑神女峰视作昆仑山的最高峰。

昆仑山[克里雅山口西北方向一片辽阔的](../Page/克里雅山口.md "wikilink")[阿什库勒盆地](../Page/阿什库勒盆地.md "wikilink")(即乌鲁克高原盆地)无人区，平均海拔4700余米，面积约740平方公里。盆地内的阿什库勒火山群的14座火山，熔岩面积达180平方公里，其中一号火山是[中国大陆最年轻的火山](../Page/中国大陆.md "wikilink")，海拔4921米，相对高度150米，坐标为北纬35度41分50.09秒，东经81度34分38.45。1951年5月27日上午9时50分曾经火山爆炸式喷发，无熔岩溢出。

中昆仑山、东昆仑山和[阿尔金山的分界](../Page/阿尔金山.md "wikilink")，是[喀拉米兰山口](../Page/喀拉米兰山口.md "wikilink")\[2\]与[车尔臣河谷](../Page/车尔臣河.md "wikilink")。阿尔金山以[元古代地层为主](../Page/元古代.md "wikilink")，而昆仑山以[晚古生代为主](../Page/晚古生代.md "wikilink")。从[地质图上看阿尔金山颜色要比东昆仑山颜色暗很多](../Page/地质图.md "wikilink")，二者的界限是[阿尔金断裂带](../Page/阿尔金断裂带.md "wikilink")。

## 山峰

  - [公格尔山](../Page/公格尔山.md "wikilink")（7649米）

  - [慕士塔格山](../Page/慕士塔格山.md "wikilink")（7509米）

  - [公格尔九别山](../Page/公格尔九别山.md "wikilink")（7530米）

  - （7167米）\[3\]\[4\]

  - [木孜塔格山](../Page/木孜塔格山.md "wikilink")（6973米）

  - [布喀达坂峰](../Page/布喀达坂峰.md "wikilink")（6860米）

  - [慕士山](../Page/慕士山.md "wikilink")（6638米）

  - [玉珠峰](../Page/玉珠峰.md "wikilink")（6178米）

[Cordillère_du_Kunlun.jpg](https://zh.wikipedia.org/wiki/File:Cordillère_du_Kunlun.jpg "fig:Cordillère_du_Kunlun.jpg")

## 河流

  - [叶尔羌河](../Page/叶尔羌河.md "wikilink")
  - [塔里木河](../Page/塔里木河.md "wikilink")
  - [喀拉喀什河](../Page/喀拉喀什河.md "wikilink")
  - [玉龙喀什河](../Page/玉龙喀什河.md "wikilink")
  - [克里雅河](../Page/克里雅河.md "wikilink")
  - [尼雅河](../Page/尼雅河.md "wikilink")
  - [安迪尔河](../Page/安迪尔河.md "wikilink")
  - [车尔臣河](../Page/车尔臣河.md "wikilink")
  - [那仁郭勒河](../Page/那仁郭勒河.md "wikilink")
  - [乌图美仁河](../Page/乌图美仁河.md "wikilink")
  - [格尔木河](../Page/格尔木河.md "wikilink")
  - [柴达木河](../Page/柴达木河.md "wikilink")

## 地质

崑崙山脉与[塔里木盆地和](../Page/塔里木盆地.md "wikilink")[柴达木盆地间均以深大断裂相隔](../Page/柴达木盆地.md "wikilink")。属[南亚陆间区与](../Page/南亚陆间区.md "wikilink")[中轴大陆区交界的北缘](../Page/中轴大陆区.md "wikilink")。崑崙山地区以[前震旦系为基底](../Page/前震旦系.md "wikilink")；经过[古生代海域下沉及](../Page/古生代.md "wikilink")[华力西运动褶皱上升](../Page/华力西运动.md "wikilink")，构成崑崙中轴和山脉中脊；经过[中生代](../Page/中生代.md "wikilink")[燕山运动构成主脊两侧](../Page/燕山运动.md "wikilink")4000米以上的山体。

## 氣候

崑崙山北坡屬[暖溫帶塔里木荒漠和柴達木荒漠](../Page/暖溫帶.md "wikilink")，降水量小；隨著海拔的增高，暖溫帶荒漠過渡為高山荒漠，降水量隨之增加。雪線在海拔5600米至5900米，雪線以上為終年不化的冰川，冰川面積達到3000平方公里以上，是中國的大冰川區之一，冰川溶水是中國幾條主要大河的源頭，包括[長江](../Page/長江.md "wikilink")、[黃河](../Page/黃河.md "wikilink")、[瀾滄江](../Page/瀾滄江.md "wikilink")（湄公河）、[怒江](../Page/怒江.md "wikilink")（薩爾溫江）和[塔里木河](../Page/塔里木河.md "wikilink")。

## 物产

崑崙山区有100多种高等[植物](../Page/植物.md "wikilink")，但一般都是低矮的灌木类。野生[动物都是高原特有的如](../Page/动物.md "wikilink")[藏羚羊](../Page/藏羚羊.md "wikilink")、[野牦牛](../Page/野牦牛.md "wikilink")、[野驴等](../Page/野驴.md "wikilink")。

新疆和田的崑崙山麓出产最高质美[玉](../Page/玉.md "wikilink")——[和田玉](../Page/和阗玉.md "wikilink")，从古代起就是[中原地区玉石主要来源](../Page/中原地区.md "wikilink")，因此《[千字文](../Page/千字文.md "wikilink")》提到“玉出崑岗”。

## 关于昆仑山的神话崇拜

《史记》记载“禹兴于西羌”。《[山海经](../Page/山海经.md "wikilink")》详尽描述了神话中的昆仑山与[西王母](../Page/西王母.md "wikilink")，[华夏族从西北之高原](../Page/华夏族.md "wikilink")，迁向中原，再到沿海。\[5\]《[穆天子传](../Page/穆天子传.md "wikilink")》记载：穆天子见西王母于昆仑丘；\[6\]穆天子至群玉之山，天下高山尽收眼里（通说为帕米尔高原）。昆仑崇拜自古以来都是中国至高崇拜，[汉人的玉石崇拜也是因为昆仑产玉](../Page/汉人.md "wikilink")，玉石在汉文化中成为高贵血统的标示。

有一種說法認為由於[崑崙山脈高聳挺拔](../Page/崑崙山脈.md "wikilink")，為古代中國和西部之間的天然屏障，所以被古代中國人當作世界邊緣，加上其終年積[雪令中國古代以](../Page/雪.md "wikilink")[白色象徵西方](../Page/白色.md "wikilink")\[7\]。

## 參考文獻

## 外部链接

  -
## 参见

  - [昆仑山 (神话)](../Page/昆仑山_\(神话\).md "wikilink")

{{-}}

[昆仑山脉](../Category/昆仑山脉.md "wikilink")
[Category:新疆山脉](../Category/新疆山脉.md "wikilink")
[Category:西藏山脉](../Category/西藏山脉.md "wikilink")
[Category:青海山脉](../Category/青海山脉.md "wikilink")

1.  [全球火山作用計畫 -
    昆仑火山群](http://www.volcano.si.edu/world/volcano.cfm?vnum=1004-03-)
2.  [李明森：《羌塘无人区探秘十七——登上喀拉米兰山口》，[中国科学院地理科学与资源研究所网站](../Page/中国科学院地理科学与资源研究所.md "wikilink")](http://www.igsnrr.ac.cn/kxcb/dlyzykpyd/kxkcsj/wtwrqtm/200611/t20061101_2156127.html)
3.  ["Liushi Shan, China".
    Peakbagger.com.](http://www.peakbagger.com/peak.aspx?pid=14130)
4.  ["Tibet
    Ultra-Prominences"](http://www.peaklist.org/WWlists/ultras/china1.html)
5.  《[山海经](../Page/山海经.md "wikilink")·海內西经》：海内昆仑之虚，在西北，帝之下都。昆仑之虚，方八百里，高万
    仞。上有木禾，长五寻，大五围。面有九井，以玉为槛。面 有九门，门有开明兽守之，百神之所在。在八隅之岩，赤水之际，非 （仁）
    \[夷\]羿莫能上冈之岩。
6.  《穆天子传》：天子西征，至于西王母之邦，吉日甲子，天子宾于西王母。
7.