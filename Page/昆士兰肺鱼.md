**昆士兰肺鱼**（[学名](../Page/学名.md "wikilink"):*Neoceratodus
forsteri*），是澳洲肺鱼科澳洲肺鱼属的[单型种](../Page/单型种.md "wikilink")。长达一米，最大可达1.5米，鳞片大，只有一个鳔，胸鳍和腹鳍有力。只生活在[澳大利亚](../Page/澳大利亚.md "wikilink")[昆士兰州](../Page/昆士兰州.md "wikilink")，所以也叫**昆士兰肺鱼**或**澳洲肺鱼**。比[非洲肺鱼更原始](../Page/非洲肺鱼.md "wikilink")，不能长期干旱。

需要注意的是，英語“barramundi”一詞可指多種大型有鱗的淡水魚類，雖然包括肺魚，但通常餐桌上爲[鱸形總目的](../Page/鱸形總目.md "wikilink")[尖吻鱸](../Page/尖吻鱸.md "wikilink")（或稱盲鰽，學名
*Lates
calcarifer*）。而昆士蘭肺魚爲受[CITES保護的](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")[易危物種](../Page/易危物種.md "wikilink")，在澳大利亞禁止捕撈，僅在有澳大利亞聯邦及昆士蘭州許可的情況下允許以敎學和科硏目的的採集，而將食用的barramundi譯爲“澳洲肺魚”爲誤譯。

## 参考文献

[Category:角齿鱼目](../Category/角齿鱼目.md "wikilink")
[Category:澳洲鱼类](../Category/澳洲鱼类.md "wikilink")