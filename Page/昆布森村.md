**昆布森村**（）為過去位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[釧路支廳中部臨海的行政區劃](../Page/釧路支廳.md "wikilink")，已於1955年1月1日與釧路村[合併為新設的釧路村](../Page/市町村合併.md "wikilink")（現在的[釧路町](../Page/釧路町.md "wikilink")）。過去的轄區為現今釧路町的昆布森地區。主要產業為[漁業](../Page/漁業.md "wikilink")，特產為[昆布](../Page/昆布.md "wikilink")。

## 歷史

  - 1880年：昆布森村成立。
  - 1919年4月1日：與跡永賀村、仙鳳趾村合併為新設的昆布森村，並成為二級村。\[1\]
  - 1955年1月1日：與釧路村合併成為新設的釧路村。

## 相關條目

  - [釧路町](../Page/釧路町.md "wikilink")

## 參考資料

<div style="font-size: 85%">

<references />

</div>

[Category:釧路町](../Category/釧路町.md "wikilink")
[Category:釧路管內](../Category/釧路管內.md "wikilink")

1.