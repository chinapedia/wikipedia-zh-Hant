**小昆西·德莱特·琼斯**（，），生於[美國](../Page/美國.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")，美國[音乐制作人](../Page/音乐制作人.md "wikilink")、电视製作人、[作曲家](../Page/作曲家.md "wikilink")、[作詞家](../Page/作詞家.md "wikilink")、[编曲家](../Page/编曲家.md "wikilink")。

其职业生涯横跨五个年代，创纪录地获得79个格莱美提名和27次获奖，包括1991年他获得的格莱美传奇艺人大奖以及4次有色人種民權促進協會形象獎傑出爵士藝人獎。昆西·瓊斯曾與[麥可·傑克森合作數張大受歡迎的專輯](../Page/麥可·傑克森.md "wikilink")，包括《[Off
the
Wall](../Page/墙外_\(迈克尔·杰克逊专辑\).md "wikilink")》、《[Thriller](../Page/顫慄_\(麥可·傑克森專輯\).md "wikilink")》。1985年制作和指挥了著名的公益歌曲
《[四海一家（We Are the World）](../Page/天下一家.md "wikilink")》。 生平\*/從小出生在芝加哥中南部
其母患有精神疾病 其父是一名工人 昆西在7歲那年在一處廢棄工廠意外發現一架鋼琴 從此打開他與音樂的漫長旅行 他曾對媒體說 音樂解救他的人生
水和音樂足以令人生存

## 個人專輯列表

  - 1964: *Big Band Bossa Nova*
  - 1970: *Gula Matari*
  - 1970: *Walking in Space*
  - 1971: *Smackwater Jack*
  - 1973: *You've Got It Bad, Girl*
  - 1974: *Body Heat*
  - 1975: *Mellow Madness*
  - 1976: *I Heard That\!*
  - 1977: *Roots*
  - 1978: *Sounds...And Stuff Like That\!\!*
  - 1981: *The Dude*
  - 1984: *The Birth of a Band, Vol. 1*
  - 1989: *Back on the Block*
  - 1995: *Q's Jook Joint*
  - 2000: *Basie and Beyond*
  - 2004: *Original Jam Sessions* 1969
  - 2010: *Q Soul Bossa Nostra*

## 外部連結

  - [IMDb - Quincy Jones](../Page/:imdbname:0005065.md "wikilink")

[Category:美國音樂家](../Category/美國音樂家.md "wikilink")
[Category:美國作曲家](../Category/美國作曲家.md "wikilink")
[Category:美国作词家](../Category/美国作词家.md "wikilink")
[Category:美國爵士樂手](../Category/美國爵士樂手.md "wikilink")
[Category:美国爵士作曲家](../Category/美国爵士作曲家.md "wikilink")
[Category:美国音乐制作人](../Category/美国音乐制作人.md "wikilink")
[Category:美国男演员](../Category/美国男演员.md "wikilink")
[Category:美国电视监制](../Category/美国电视监制.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:奥斯卡荣誉奖获得者](../Category/奥斯卡荣誉奖获得者.md "wikilink")
[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")
[Category:芝加哥人](../Category/芝加哥人.md "wikilink")
[Category:非洲裔美国人](../Category/非洲裔美国人.md "wikilink")
[Category:喀麦隆裔美国人](../Category/喀麦隆裔美国人.md "wikilink")
[Category:英格兰裔美国人](../Category/英格兰裔美国人.md "wikilink")
[Category:法国裔美国人](../Category/法国裔美国人.md "wikilink")
[Category:意大利裔美国人](../Category/意大利裔美国人.md "wikilink")
[Category:俄羅斯裔美國人](../Category/俄羅斯裔美國人.md "wikilink")
[Category:瑞士裔美国人](../Category/瑞士裔美国人.md "wikilink")
[Category:威尔士裔美国人](../Category/威尔士裔美国人.md "wikilink")
[Category:摇滚名人堂入选者](../Category/摇滚名人堂入选者.md "wikilink")
[Category:伯克利音樂學院校友](../Category/伯克利音樂學院校友.md "wikilink")
[Category:美国自传作家](../Category/美国自传作家.md "wikilink")
[Category:非洲裔美國指揮家](../Category/非洲裔美國指揮家.md "wikilink")
[Category:Mnet亞洲音樂大獎獲得者](../Category/Mnet亞洲音樂大獎獲得者.md "wikilink")