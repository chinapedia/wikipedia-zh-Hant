**昇元**（或作**升元**\[1\]，937年十月－943年二月）是南唐烈祖[李昪的](../Page/李昪.md "wikilink")[年號](../Page/年號.md "wikilink")，共计7年。

## 紀年

| 昇元                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 937年                           | 938年                           | 939年                           | 940年                           | 941年                           | 942年                           | 943年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") | [戊戌](../Page/戊戌.md "wikilink") | [己亥](../Page/己亥.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [通文](../Page/通文.md "wikilink")（936年三月至939年七月）：閩—[王繼鵬之年號](../Page/王繼鵬.md "wikilink")
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [光天](../Page/光天_\(劉玢\).md "wikilink")（942年四月至943年二月）：南漢—[劉玢之年號](../Page/劉玢.md "wikilink")
      - [永樂](../Page/永樂_\(張遇賢\).md "wikilink")（942年七月至943年十月）：南漢—[張遇賢之年號](../Page/張遇賢.md "wikilink")
      - [永隆](../Page/永隆_\(王延羲\).md "wikilink")（939年閏七月至943年正月）：閩—[王延羲之年號](../Page/王延羲.md "wikilink")
      - [天德](../Page/天德_\(王延政\).md "wikilink")（943年二月至945年八月）：[殷](../Page/闽_\(十国\).md "wikilink")—[王延政之年號](../Page/王延政.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：後蜀—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [鼎新](../Page/鼎新.md "wikilink")：大義寧—楊干真之年號
      - [光聖](../Page/光聖.md "wikilink")：大義寧—楊干真之年號
      - [文德](../Page/文德_\(段思平\).md "wikilink")（938年起）：[大理國](../Page/大理國.md "wikilink")—[段思平之年號](../Page/段思平.md "wikilink")
      - [神武](../Page/神武_\(段思平\).md "wikilink")：大理國—段思平之年號
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [會同](../Page/会同_\(辽朝\).md "wikilink")（938年十一月至947年正月）：契丹—耶律德光之年號
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇之年號](../Page/朱雀天皇.md "wikilink")
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：朱雀天皇與[村上天皇之年號](../Page/村上天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:南唐年号](../Category/南唐年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")
[Category:940年代中国政治](../Category/940年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，145 ISBN 7101025129