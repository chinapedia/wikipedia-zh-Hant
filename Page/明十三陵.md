**明十三陵**，坐落於[北京市](../Page/北京市.md "wikilink")[昌平区](../Page/昌平区.md "wikilink")[十三陵镇](../Page/十三陵镇.md "wikilink")[天壽山下](../Page/天壽山.md "wikilink")40平方公里的小[盆地](../Page/盆地.md "wikilink")，距離北京市区約50公里，是[中国](../Page/中国.md "wikilink")[明朝皇帝的墓葬建築群](../Page/明朝皇帝.md "wikilink")。自[永樂七年五月](../Page/永乐_\(明朝\).md "wikilink")（1409年）起用，直到安葬[崇禎帝後結束](../Page/明思宗.md "wikilink")，歷時230多年，共葬有13位[明朝皇帝](../Page/明朝皇帝.md "wikilink")，23位明朝皇后、2位明朝太子、30餘名[妃嬪](../Page/妃嬪.md "wikilink")、1位[太監](../Page/太監.md "wikilink")，是全球保存完整皇陵墓葬群之一。2003年7月3日被列为[世界文化遗产](../Page/世界文化遗产.md "wikilink")。

## 历史

[明朝共有](../Page/明朝.md "wikilink")16位皇帝，但[北京的明十三陵只有](../Page/北京.md "wikilink")13位皇帝，未入陵者因由各异。明朝开国皇帝[朱元璋早年建都于](../Page/朱元璋.md "wikilink")[南京](../Page/南京.md "wikilink")，死后葬于[南京](../Page/南京.md "wikilink")[紫金山的](../Page/紫金山.md "wikilink")「[孝陵](../Page/明孝陵.md "wikilink")」；继位的[明太祖](../Page/明太祖.md "wikilink")[朱元璋长孙](../Page/朱元璋.md "wikilink")[明惠帝](../Page/明惠帝.md "wikilink")[朱允炆](../Page/朱允炆.md "wikilink")，在其叔父燕王[朱棣](../Page/朱棣.md "wikilink")（即后来的[明成祖](../Page/明成祖.md "wikilink")）发起「[靖难之役](../Page/靖难之变.md "wikilink")」攻破南京之后下落不明，加上永樂帝不承认其帝位，因此没有帝陵；第七帝[明代宗](../Page/明代宗.md "wikilink")[朱祁钰](../Page/朱祁钰.md "wikilink")，在其兄[明英宗于](../Page/明英宗.md "wikilink")[土木堡之变被](../Page/土木堡之变.md "wikilink")[瓦剌所俘之后登上帝位](../Page/瓦剌.md "wikilink")。后来[明英宗](../Page/明英宗.md "wikilink")[复辟](../Page/复辟.md "wikilink")，[朱祁钰速死](../Page/朱祁钰.md "wikilink")，[明英宗不承认其帝位](../Page/明英宗.md "wikilink")，捣毁其在天寿山修建的寿陵。还有一说，寿陵被改建为在位僅29天的[明光宗的陵墓](../Page/明光宗.md "wikilink")，改名为[庆陵](../Page/明庆陵.md "wikilink")。[明憲宗以](../Page/明憲宗.md "wikilink")[亲王身分将他葬于](../Page/亲王.md "wikilink")[北京西郊](../Page/北京.md "wikilink")[金山的](../Page/玉泉山.md "wikilink")[景泰陵](../Page/景泰陵.md "wikilink")。

十三陵的主陵是[朱棣于](../Page/朱棣.md "wikilink")1409年至1413年最早兴建的[明长陵](../Page/明长陵.md "wikilink")，他当时经「车驾临视」，钦定山名为「天寿山」，1424年他于北征[鞑靼途中](../Page/鞑靼_\(蒙古\).md "wikilink")[驾崩](../Page/驾崩.md "wikilink")，后安葬于此，但[明长陵工程直至](../Page/明长陵.md "wikilink")1427年始全部竣工。期后近20年的兴建，形成长达7公里多的完整建筑群。

13座陵墓中，[永樂帝的](../Page/永樂帝.md "wikilink")[明长陵](../Page/明长陵.md "wikilink")、[嘉靖帝的](../Page/嘉靖帝.md "wikilink")[明永陵和](../Page/明永陵.md "wikilink")[万历帝的](../Page/万历帝.md "wikilink")[明定陵](../Page/明定陵.md "wikilink")，均是生前所建，规模亦最大，其馀陵墓则是死后才动工，大约会用半年修建。[崇祯帝因为是亡国之君](../Page/崇祯帝.md "wikilink")，并没有正式建陵，现时的陵墓是以其妃[田氏的墓穴改建](../Page/田秀英.md "wikilink")。

1957年，[北京市人民政府公布十三陵为北京市第一批重点古建文物保护单位](../Page/北京市人民政府.md "wikilink")。1961年，十三陵被公布为全国重点文物保护单位。1982年，[中华人民共和国国务院公布](../Page/中华人民共和国国务院.md "wikilink")[八达岭](../Page/八达岭.md "wikilink")——十三陵风景区为全国44个[重点风景名胜保护区之一](../Page/中國國家級風景名勝區.md "wikilink")。1995年12月，「明十三陵博物馆」成立。2003年7月3日，明十三陵列入[联合国](../Page/联合国.md "wikilink")《[世界遗产名录](../Page/世界遗产名录.md "wikilink")》。

目前，长陵大殿、定陵地宫可供游客参观，其余陵均未开放。

## 風水佈局

[Ming_Tombs_1875-1908.jpg](https://zh.wikipedia.org/wiki/File:Ming_Tombs_1875-1908.jpg "fig:Ming_Tombs_1875-1908.jpg")

十三陵屬於[太行山脈](../Page/太行山脈.md "wikilink")，西通[居庸關](../Page/居庸關.md "wikilink")，北通[黃花鎮](../Page/黃花鎮.md "wikilink")，南向[昌平州](../Page/昌平.md "wikilink")，成為十三陵及京師之北面屏障。太行山起澤州，蜿蜒綿亙北走千百里山脈不斷，至[居庸關](../Page/居庸關.md "wikilink")。明末清初學者[顧炎武曾指](../Page/顧炎武.md "wikilink")：「群山自南來，勢若蛟龍翔；東趾踞盧龍，西脊馳太行；後尻坐黃花（指黃花鎮），前面臨神京；中有萬年宅，名曰康家莊；可容百萬人，豁然開[明堂](../Page/明堂.md "wikilink")。」明代視此為[風水地](../Page/風水地.md "wikilink")，陵區以常綠的[松](../Page/松.md "wikilink")[柏樹為主](../Page/柏樹.md "wikilink")。

明十三陵依山而建，沿襲南京孝陵的模式，即除神道共用外，各陵都是前為祭享區，後為墓冢區。陵墓規格相近，各據山頭，陵與陵之間相距500米至8000米不等。除[思陵偏在西南一隅外](../Page/思陵.md "wikilink")，其餘均成扇面形分列於長陵左右。亦有陵仿孝陵之制，稱有一座叫「[啞巴院](../Page/啞巴院.md "wikilink")」的建築，或更設一座[琉璃](../Page/琉璃.md "wikilink")[照壁作為屏幕障](../Page/照壁.md "wikilink")。

每座陵墓的陵門設有[碑](../Page/碑.md "wikilink")[亭](../Page/亭.md "wikilink")，碑文記載皇帝生前的業績，應由嗣皇帝來撰寫。但自從[明仁宗為其父朱棣寫了一篇](../Page/明仁宗.md "wikilink")3500字的紀功碑文後，再也沒有嗣皇帝續寫。所以現在除了長陵碑外，其餘各陵都成了[無字碑](../Page/無字碑.md "wikilink")。

以規模較大的定陵（萬曆帝墓）為例，其地面建築的總布局，呈前方後圓，象徵「[天圓地方](../Page/天圓地方.md "wikilink")」。地面建築佔地18萬平方米，前有寬闊院落三進，後有高大寶城一座。陵墓有祠祭署、宰牲亭、定陵監、神宮監、神馬房等附屬等建築物300多間，往後是陵園最外面的圍牆－外羅城（圍牆外的圍牆）。

### 其他园寝

  - 东井
  - 西井
  - [万贵妃墓](../Page/万贵妃.md "wikilink")
  - [郑贵妃墓](../Page/郑贵妃.md "wikilink")
  - 世宗妃太子墓
  - [王承恩墓](../Page/王承恩.md "wikilink")

## 耗費

明十三陵雖屬皇家工程，但總體耗費並無全面的精準紀錄，但部分陵寢的紀錄仍可作參考。1584年動工的[萬曆帝陵墓定陵](../Page/萬曆.md "wikilink")，是十三陵中三大陵墓之一，1584年動工，歷時6年，當時共耗用800萬[銀兩](../Page/銀兩.md "wikilink")。

安葬[明穆宗朱載垕及三位皇后的](../Page/明穆宗.md "wikilink")[昭陵](../Page/明昭陵.md "wikilink")，早年施工不周，完工後一年，建築便出現了[地基沉陷的問題](../Page/地基.md "wikilink")。1574年，昭陵神宮監官[陶金等上奏說](../Page/陶金_\(明朝\).md "wikilink")：「六月以來，陰雨二日，本陵棱恩門裡外磚石沉陷。」1575年正月，[明神宗不得不委派](../Page/明神宗.md "wikilink")[工部左侍郎](../Page/工部侍郎.md "wikilink")[陳一松等提督再修昭陵](../Page/陳一松.md "wikilink")。第一次興建地面建築，共動用[庫銀](../Page/庫銀.md "wikilink")390932兩，仍未計算當中木植、[白城磚](../Page/白城磚.md "wikilink")、[大石窩等費用](../Page/大石窩.md "wikilink")。後來又有戶、兵二部動用110119銀兩，總計501050銀兩。其中；[營繕司又用](../Page/營繕司.md "wikilink")204422銀兩，虞衡司
13145兩，都水司118854兩，屯田司164628兩。

第二次修葺的費用，文獻中沒有明確記載，但據《[明熹宗實錄](../Page/明熹宗實錄.md "wikilink")》記載，前後兩次修建共用銀150餘萬兩。這還不算[嘉靖時營建玄宮的費用](../Page/嘉靖.md "wikilink")。如算上嘉靖年間營陵的費用，其總用度至少在200萬兩以上，當時[隆慶年間一年的財政總收入亦只有](../Page/隆慶.md "wikilink")230餘萬兩。

由於營建昭陵需要龐大的錢糧開支，工部庫銀匱乏到了極點。萬曆二年（1574年）八月修繕涿州橋，工部拿不出銀兩，兵部派不出軍匠，最終輔臣[張居正請求明神宗懇請太后解囊捐銀](../Page/張居正.md "wikilink")，僱工修建。

## 明朝皇帝陵列表

[明朝历任](../Page/明朝.md "wikilink")16位皇帝，当中13人葬於明十三陵，但[朱元璋葬于](../Page/朱元璋.md "wikilink")[南京](../Page/南京.md "wikilink")，此后追谥祖上[朱百六](../Page/朱百六.md "wikilink")、[朱四九](../Page/朱四九.md "wikilink")、[朱初一及](../Page/朱初一.md "wikilink")[朱世珍等](../Page/朱世珍.md "wikilink")。[明惠宗](../Page/明惠宗.md "wikilink")[朱允炆失踪](../Page/朱允炆.md "wikilink")，无帝陵，而[景泰帝则葬于](../Page/景泰帝.md "wikilink")[北京西郊金山下](../Page/北京.md "wikilink")。相信是[明英宗](../Page/明英宗.md "wikilink")1457年发动[夺门之变](../Page/夺门之变.md "wikilink")[复辟之后](../Page/复辟.md "wikilink")，不承认景泰帝的帝位，以及景泰帝不肯歸还本屬他的皇位，但並无去其年号。详见[土木堡之变](../Page/土木堡之变.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><div align="center">
<p>序号</p>
</div></td>
<td><div align="center">
<p>陵<br />
名</p>
</div></td>
<td><div align="center">
<p>皇帝名称</p>
</div></td>
<td><div align="center">
<p><a href="../Page/年号.md" title="wikilink">年号</a></p>
</div></td>
<td><div align="center">
<p><a href="../Page/庙号.md" title="wikilink">庙号</a></p>
</div></td>
<td><div align="center">
<p><a href="../Page/谥号.md" title="wikilink">谥号</a></p>
</div></td>
<td><div align="center">
<p>在位年代</p>
</div></td>
<td><div align="center">
<p>世系</p>
</div></td>
<td><div align="center">
<p>享年</p>
</div></td>
<td><div align="center">
<p><a href="../Page/祔葬.md" title="wikilink">祔葬皇后</a></p>
</div></td>
<td><div align="center">
<p>陵址</p>
</div></td>
</tr>
<tr class="even">
<td><div align="center">
<p>1</p>
</div></td>
<td><p><a href="../Page/明祖陵.md" title="wikilink">祖陵</a></p></td>
<td><p><a href="../Page/朱百六.md" title="wikilink">朱百六</a></p></td>
<td></td>
<td><p><a href="../Page/明德祖.md" title="wikilink">明德祖</a></p></td>
<td><p>玄皇帝</p></td>
<td></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖高祖</a></p></td>
<td><p>不詳</p></td>
<td><p>胡氏</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/盱眙县.md" title="wikilink">盱眙县管镇乡</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱四九.md" title="wikilink">朱四九</a></p></td>
<td></td>
<td><p><a href="../Page/明懿祖.md" title="wikilink">明懿祖</a></p></td>
<td><p>恆皇帝</p></td>
<td></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖曾祖</a></p></td>
<td><p>不詳</p></td>
<td><p>侯氏</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱初一.md" title="wikilink">朱初一</a></p></td>
<td></td>
<td><p><a href="../Page/明熙祖.md" title="wikilink">明熙祖</a></p></td>
<td><p>裕皇帝</p></td>
<td></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖祖父</a></p></td>
<td><p>不詳</p></td>
<td><p>王氏</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>2</p>
</div></td>
<td><p><a href="../Page/明皇陵.md" title="wikilink">皇陵</a></p></td>
<td><p><a href="../Page/朱世珍.md" title="wikilink">朱世珍</a></p></td>
<td></td>
<td><p><a href="../Page/明仁祖.md" title="wikilink">明仁祖</a></p></td>
<td><p>淳皇帝</p></td>
<td></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖父親</a></p></td>
<td><p>64岁</p></td>
<td><p>陈氏</p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/凤阳县.md" title="wikilink">凤阳县西南</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>3</p>
</div></td>
<td><p><a href="../Page/明孝陵.md" title="wikilink">孝陵</a></p></td>
<td><p><a href="../Page/朱元璋.md" title="wikilink">朱元璋</a></p></td>
<td><p><a href="../Page/洪武.md" title="wikilink">洪武</a></p></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖</a></p></td>
<td><p>高皇帝</p></td>
<td><p>1368-1398</p></td>
<td></td>
<td><p>71岁</p></td>
<td><p>马氏</p></td>
<td><p><a href="../Page/南京.md" title="wikilink">南京</a><a href="../Page/紫金山.md" title="wikilink">鐘山南麓</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>4</p>
</div></td>
<td><p><a href="../Page/明長陵.md" title="wikilink">長陵</a></p></td>
<td><p><a href="../Page/朱棣.md" title="wikilink">朱棣</a></p></td>
<td><p><a href="../Page/永樂.md" title="wikilink">永樂</a></p></td>
<td><p><a href="../Page/明成祖.md" title="wikilink">明成祖</a></p></td>
<td><p>文皇帝</p></td>
<td><p>1402-1424</p></td>
<td><p><a href="../Page/明太祖.md" title="wikilink">明太祖四子</a></p></td>
<td><p>65岁</p></td>
<td><p>徐氏</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a><a href="../Page/昌平区.md" title="wikilink">昌平区</a><a href="../Page/天壽山.md" title="wikilink">天壽山下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>5</p>
</div></td>
<td><p><a href="../Page/明献陵.md" title="wikilink">獻陵</a></p></td>
<td><p><a href="../Page/朱高炽.md" title="wikilink">朱高炽</a></p></td>
<td><p><a href="../Page/洪熙.md" title="wikilink">洪熙</a></p></td>
<td><p><a href="../Page/明仁宗.md" title="wikilink">明仁宗</a></p></td>
<td><p>昭皇帝</p></td>
<td><p>1424-1425</p></td>
<td><p><a href="../Page/明成祖.md" title="wikilink">明成祖长子</a></p></td>
<td><p>48岁</p></td>
<td><p>张氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/黃山寺.md" title="wikilink">黃山寺一嶺下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>6</p>
</div></td>
<td><p><a href="../Page/明景陵.md" title="wikilink">景陵</a></p></td>
<td><p><a href="../Page/朱瞻基.md" title="wikilink">朱瞻基</a></p></td>
<td><p><a href="../Page/宣德.md" title="wikilink">宣德</a></p></td>
<td><p><a href="../Page/明宣宗.md" title="wikilink">明宣宗</a></p></td>
<td><p>章皇帝</p></td>
<td><p>1425-1435</p></td>
<td><p><a href="../Page/明仁宗.md" title="wikilink">明仁宗长子</a></p></td>
<td><p>37岁</p></td>
<td><p>孫氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/黑山.md" title="wikilink">黑山下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>7</p>
</div></td>
<td><p><a href="../Page/明裕陵.md" title="wikilink">裕陵</a></p></td>
<td><p><a href="../Page/朱祁鎮.md" title="wikilink">朱祁鎮</a></p></td>
<td><p><a href="../Page/正統.md" title="wikilink">正統</a></p></td>
<td><p><a href="../Page/明英宗.md" title="wikilink">明英宗</a></p></td>
<td><p>睿皇帝</p></td>
<td><p>1435-1449</p></td>
<td><p><a href="../Page/明宣宗.md" title="wikilink">明宣宗长子</a></p></td>
<td><p>38岁</p></td>
<td><p>錢氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/石門山.md" title="wikilink">石門山下</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天順.md" title="wikilink">天順</a></p></td>
<td><p>1457-1464</p></td>
<td><p>周氏</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div align="center">
<p>8</p>
</div></td>
<td><p><a href="../Page/景泰陵.md" title="wikilink">景泰陵</a></p></td>
<td><p><a href="../Page/朱祁鈺.md" title="wikilink">朱祁鈺</a></p></td>
<td><p><a href="../Page/景泰.md" title="wikilink">景泰</a></p></td>
<td><p><a href="../Page/明代宗.md" title="wikilink">明代宗</a></p></td>
<td><p>景皇帝</p></td>
<td><p>1449-1457</p></td>
<td><p><a href="../Page/明宣宗.md" title="wikilink">明宣宗二子</a></p></td>
<td><p>30岁</p></td>
<td><p>汪氏</p></td>
<td><p>北京西郊<a href="../Page/玉泉山.md" title="wikilink">金山下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>9</p>
</div></td>
<td><p><a href="../Page/明茂陵.md" title="wikilink">茂陵</a></p></td>
<td><p><a href="../Page/朱见深.md" title="wikilink">朱见深</a></p></td>
<td><p><a href="../Page/成化.md" title="wikilink">成化</a></p></td>
<td><p><a href="../Page/明宪宗.md" title="wikilink">明宪宗</a></p></td>
<td><p>純皇帝</p></td>
<td><p>1464-1487</p></td>
<td><p><a href="../Page/明英宗.md" title="wikilink">明英宗长子</a></p></td>
<td><p>41岁</p></td>
<td><p>王氏、紀氏、邵氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/聚宝山.md" title="wikilink">聚宝山下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>10</p>
</div></td>
<td><p><a href="../Page/明泰陵.md" title="wikilink">泰陵</a></p></td>
<td><p><a href="../Page/朱祐樘.md" title="wikilink">朱祐樘</a></p></td>
<td><p><a href="../Page/弘治.md" title="wikilink">弘治</a></p></td>
<td><p><a href="../Page/明孝宗.md" title="wikilink">明孝宗</a></p></td>
<td><p>敬皇帝</p></td>
<td><p>1487-1505</p></td>
<td><p><a href="../Page/明宪宗.md" title="wikilink">明宪宗三子</a></p></td>
<td><p>36岁</p></td>
<td><p>張氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/筆架山.md" title="wikilink">筆架山下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>11</p>
</div></td>
<td><p><a href="../Page/明显陵.md" title="wikilink">顯陵</a></p></td>
<td><p><a href="../Page/明睿宗.md" title="wikilink">朱祐杬</a></p></td>
<td></td>
<td><p><a href="../Page/明睿宗.md" title="wikilink">明睿宗</a></p></td>
<td><p>獻皇帝</p></td>
<td></td>
<td><p><a href="../Page/明宪宗.md" title="wikilink">明宪宗四子</a></p></td>
<td><p>44岁</p></td>
<td><p>蒋氏</p></td>
<td><p><a href="../Page/湖北.md" title="wikilink">湖北鍾祥市</a><a href="../Page/松林山.md" title="wikilink">松林山</a>（又名純德山）下</p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>12</p>
</div></td>
<td><p><a href="../Page/明康陵.md" title="wikilink">康陵</a></p></td>
<td><p><a href="../Page/朱厚照.md" title="wikilink">朱厚照</a></p></td>
<td><p><a href="../Page/正德.md" title="wikilink">正德</a></p></td>
<td><p><a href="../Page/明武宗.md" title="wikilink">明武宗</a></p></td>
<td><p>毅皇帝</p></td>
<td><p>1505-1521</p></td>
<td><p><a href="../Page/明孝宗.md" title="wikilink">明孝宗长子</a></p></td>
<td><p>31岁</p></td>
<td><p>夏氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/蓮花山.md" title="wikilink">蓮花山下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>13</p>
</div></td>
<td><p><a href="../Page/明永陵.md" title="wikilink">永陵</a></p></td>
<td><p><a href="../Page/朱厚熜.md" title="wikilink">朱厚熜</a></p></td>
<td><p><a href="../Page/嘉靖.md" title="wikilink">嘉靖</a></p></td>
<td><p><a href="../Page/明世宗.md" title="wikilink">明世宗</a></p></td>
<td><p>肃皇帝</p></td>
<td><p>1521-1566</p></td>
<td><p><a href="../Page/明宪宗.md" title="wikilink">明宪宗孙子</a></p></td>
<td><p>60岁</p></td>
<td><p>陈氏、方氏、杜氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/阳翠岭.md" title="wikilink">阳翠岭下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>14</p>
</div></td>
<td><p><a href="../Page/明昭陵.md" title="wikilink">昭陵</a></p></td>
<td><p><a href="../Page/朱載垕.md" title="wikilink">朱載垕</a></p></td>
<td><p><a href="../Page/隆庆.md" title="wikilink">隆庆</a></p></td>
<td><p><a href="../Page/明穆宗.md" title="wikilink">明穆宗</a></p></td>
<td><p>莊皇帝</p></td>
<td><p>1566-1572</p></td>
<td><p><a href="../Page/明世宗.md" title="wikilink">明世宗三子</a></p></td>
<td><p>36岁</p></td>
<td><p>李氏、陳氏、李氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/大峪山.md" title="wikilink">大峪山下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>15</p>
</div></td>
<td><p><a href="../Page/明定陵.md" title="wikilink">定陵</a></p></td>
<td><p><a href="../Page/朱翊鈞.md" title="wikilink">朱翊鈞</a></p></td>
<td><p><a href="../Page/万历.md" title="wikilink">万历</a></p></td>
<td><p><a href="../Page/明神宗.md" title="wikilink">明神宗</a></p></td>
<td><p>显皇帝</p></td>
<td><p>1572-1620</p></td>
<td><p><a href="../Page/明穆宗.md" title="wikilink">明穆宗三子</a></p></td>
<td><p>58岁</p></td>
<td><p>王氏、王氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/大峪山.md" title="wikilink">大峪山下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>16</p>
</div></td>
<td><p><a href="../Page/明庆陵.md" title="wikilink">慶陵</a></p></td>
<td><p><a href="../Page/朱常洛.md" title="wikilink">朱常洛</a></p></td>
<td><p><a href="../Page/泰昌.md" title="wikilink">泰昌</a></p></td>
<td><p><a href="../Page/明光宗.md" title="wikilink">明光宗</a></p></td>
<td><p>貞皇帝</p></td>
<td><p>1620</p></td>
<td><p><a href="../Page/明神宗.md" title="wikilink">明神宗长子</a></p></td>
<td><p>39岁</p></td>
<td><p>郭氏、王氏、劉氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/黃山寺.md" title="wikilink">黃山寺二嶺下</a></p></td>
</tr>
<tr class="odd">
<td><div align="center">
<p>17</p>
</div></td>
<td><p><a href="../Page/明德陵.md" title="wikilink">德陵</a></p></td>
<td><p><a href="../Page/朱由校.md" title="wikilink">朱由校</a></p></td>
<td><p><a href="../Page/天启.md" title="wikilink">天启</a></p></td>
<td><p><a href="../Page/明熹宗.md" title="wikilink">明熹宗</a></p></td>
<td><p>悊皇帝</p></td>
<td><p>1620-1627</p></td>
<td><p><a href="../Page/明光宗.md" title="wikilink">明光宗长子</a></p></td>
<td><p>23岁</p></td>
<td><p>张氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/潭峪岭.md" title="wikilink">潭峪岭下</a></p></td>
</tr>
<tr class="even">
<td><div align="center">
<p>18</p>
</div></td>
<td><p><a href="../Page/明思陵.md" title="wikilink">思陵</a></p></td>
<td><p><a href="../Page/朱由檢.md" title="wikilink">朱由檢</a></p></td>
<td><p><a href="../Page/崇祯.md" title="wikilink">崇祯</a></p></td>
<td><p><a href="../Page/明思宗.md" title="wikilink">明思宗</a></p></td>
<td><p>烈皇帝</p></td>
<td><p>1627-1644</p></td>
<td><p><a href="../Page/明光宗.md" title="wikilink">明光宗五子</a></p></td>
<td><p>35岁</p></td>
<td><p>皇后周氏、皇貴妃田氏</p></td>
<td><p>北京昌平区天壽山<a href="../Page/鹿山.md" title="wikilink">鹿山下</a></p></td>
</tr>
</tbody>
</table>

## 近代大事紀

  - 1929年：1月30日，[河北省委員會第六十一次會議通過](../Page/河北省.md "wikilink")《[河北省昌平縣明陵保護辦法](../Page/河北省昌平縣明陵保護辦法.md "wikilink")》。決定在長陵設護陵警察分駐所
  - 1935年：1月，[北平市政府根據](../Page/北平.md "wikilink")[國民政府保護古物的命令](../Page/國民政府.md "wikilink")，派人勘測長陵，估算修繕經費。3月21日開工修葺，6月27日竣工。工程項目有大紅門、神功聖德碑亭、龍鳳門、長陵陵門、陵內碑亭、祾恩門、祾恩殿、內紅門、[牌樓門](../Page/牌樓.md "wikilink")、明樓、皇牆、神帛爐等
  - 1937年：思陵等先後被各股[土匪盜發](../Page/土匪.md "wikilink")
  - 1937至1948年：獻、景、裕、茂、泰、康、永、昭、慶、德等陵殘壞的祾恩門、祾恩殿，在戰亂中被逐漸拆毀，只餘殘垣斷壁。思陵享殿、碑亭、明樓被[國民革命軍拆除後](../Page/國民革命軍.md "wikilink")，將磚運走修炮樓
  - 1949－1950年：隸屬河北省通縣專署的[昌平縣政府](../Page/昌平縣.md "wikilink")，設立護陵委員會駐景陵村
  - 1952年：10月，河北省文化局指示，改護陵委員會為十三陵文物保管所
  - 1955年：1月，[北京市人民政府根據前政務院關於接管十三陵](../Page/北京市人民政府.md "wikilink")，修繕古建築，植樹造林，闢為公園的指示，派市建築工程局第一建築公司備料施工，修葺長、景、永三陵；9月，明十三陵由河北省昌平縣劃歸[北京市園林局](../Page/北京市園林局.md "wikilink")。
  - 1957年：北京市人民政府公布十三陵為北京市第一批[重點古建文物保護單位](../Page/重點古建文物保護單位.md "wikilink")。5月，打開定陵地下玄宮，出土文物近3000件
  - 1958年：[文化部](../Page/中华人民共和国文化部.md "wikilink")[文物局批准成立定陵博物館](../Page/国家文物局.md "wikilink")。
  - 1959年：10月，定陵正式對外開放（隸屬北京市文化局）
  - 1961年：[國務院公布十三陵為](../Page/中华人民共和国国务院.md "wikilink")[第一批全國重點文物保護單位](../Page/第一批全國重點文物保護單位.md "wikilink")。
  - 1961年至90年代：修復工作進行，其餘陵墓陸續開放
  - 1966年8月24日：“[文化大革命](../Page/文化大革命.md "wikilink")”時期，在“打倒地主階級頭子萬曆”的口號聲中，十幾個[紅衛兵衝進定陵地下博物館](../Page/紅衛兵.md "wikilink")，把「[明神宗朱翊钧](../Page/明神宗.md "wikilink")」、「[孝端显皇后](../Page/孝端显皇后.md "wikilink")」王氏及「[孝靖皇后](../Page/孝靖皇后.md "wikilink")」王氏的三具屍骨拉出來擺放在博物館大紅門前廣場上，進行批鬥，最終付之一炬。
  - 1987年：4月，修缮昭陵工程开工；1992年竣工。
  - 1995年：5月，修繕獻陵工程竣工；7月，神路二期工程竣工；12月「明十三陵博物館」成立。
  - 2003年：7月3日，經聯合國教科文組織審議，明十三陵列入《[世界遺產名錄](../Page/世界遺產名錄.md "wikilink")》。
  - 2001年起，[昌平区在已修缮部分陵寝的基础之上](../Page/昌平区.md "wikilink")，投资上亿元开始了又一轮的大规模修缮，先后修缮了德陵、康陵、庆陵、泰陵、茂陵，到2013年初，最后一座残陵裕陵修缮完工。2013年1月1日起，[十三陵特区办事处将永陵](../Page/十三陵特区办事处.md "wikilink")、康陵、茂陵、泰陵、德陵、庆陵6座陵寝对外开放，但不接待散客。\[1\]

## 图集

<File:Sketch> Map of the Thirteen Ming Tombs.jpg <File:Aerial> view of
Changling of 13 Ming Tombs.jpg <File:Noel> 2005 Pékin tombeaux Ming voie
des âmes 19.jpg <File:Tumbas> ming1.JPG <File:Tumbas> ming2.JPG
<File:明十三陵神道> 大宫门.JPG <File:Ming> tombs beijing spirit way animal
figures.jpg <File:Avenue> leading to the Ming Tombs, North of Peking.jpg
<File:MingDynastyTombsPic2.jpg> <File:Dingling01.jpg> <File:Golden>
Crown Replica of King Wanli.jpg

## 参考文献

## 外部連結

  - [十三陵特区官方网站](http://www.mingtombs.com/)
  - [清代人士的明十三陵與景帝陵情懷](http://www.ihp.sinica.edu.tw/~tangsong/con/Pt16.pdf)
  - [明十三陵－風雨無言的日志](https://web.archive.org/web/20160305000121/http://lu.uan.blog.163.com/blog/static/825600082009610111445560/)

{{-}}

[明十三陵](../Category/明十三陵.md "wikilink")
[Category:十三陵镇](../Category/十三陵镇.md "wikilink")
[Category:北京市墓葬](../Category/北京市墓葬.md "wikilink")
[Category:北京牌坊](../Category/北京牌坊.md "wikilink")

1.  [十三陵明年新开放6座陵寝
    不接待散客遭质疑，凤凰网，2012年12月29日](http://culture.ifeng.com/whrd/detail_2012_12/29/20655049_0.shtml)