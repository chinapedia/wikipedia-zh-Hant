**明夏**是[元朝末年由](../Page/元朝.md "wikilink")[明玉珍在](../Page/明玉珍.md "wikilink")[四川建立的一個割據政權](../Page/四川.md "wikilink")，後被[明朝所滅](../Page/明朝.md "wikilink")。

## 历史

元朝末年[农民起义军天完](../Page/农民起义.md "wikilink")[红巾军统军元帅](../Page/红巾军.md "wikilink")[明玉珍领兵西征](../Page/明玉珍.md "wikilink")，由[巫峡入](../Page/巫峡.md "wikilink")[蜀](../Page/蜀.md "wikilink")，占领[重庆](../Page/重庆.md "wikilink")，並接着击败[川内元朝官军](../Page/四川.md "wikilink")，基本上平定了川蜀。1363年，明玉珍称帝，建都重庆，国号“**[大夏](../Page/大夏.md "wikilink")**”。1366年，明玉珍病逝，其子[明升年幼继位](../Page/明升.md "wikilink")。1371年，[明太祖派兵攻蜀](../Page/明太祖.md "wikilink")，夏兵不敵，明升投降，夏亡，四川归明朝统治。

洪武五年（1372年），明升全家被流放[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")，后被[朝鲜王朝开国国王](../Page/朝鲜王朝.md "wikilink")[李成桂封为华蜀君](../Page/李成桂.md "wikilink")，享受“忠勋世禄”，定居[开城](../Page/开城.md "wikilink")[兴国寺](../Page/兴国寺.md "wikilink")。彭皇后去世后，安葬在[松都](../Page/松都.md "wikilink")[万寿山的肃陵](../Page/万寿山.md "wikilink")，并建有祠宇。

明升在朝鲜育有四子，皆为朝鲜高官，后世为朝鲜望族。现今有4万多明氏后裔在[韩国](../Page/韩国.md "wikilink")，两万多在[朝鲜](../Page/朝鲜.md "wikilink")，而在中国境内的后人多改为[甘姓](../Page/甘姓.md "wikilink")。\[1\]

## 行政区划

## 参考文献

### 引用

### 来源

  -
## 參見

  - [元末民变](../Page/元末民变.md "wikilink")
  - [明玉珍](../Page/明玉珍.md "wikilink")
  - [延安明氏](../Page/延安明氏.md "wikilink")

{{-}}

[Category:元末民变政权](../Category/元末民变政权.md "wikilink")
[明夏](../Category/明夏.md "wikilink")
[Category:四川历史政权](../Category/四川历史政权.md "wikilink")
[Category:1363年建立](../Category/1363年建立.md "wikilink")
[Category:1371年废除](../Category/1371年废除.md "wikilink")

1.  [4万多明氏后裔在韩国，两万多在朝鲜](http://www.cq.xinhuanet.com/news/2009-03/03/content_15840663.htm)