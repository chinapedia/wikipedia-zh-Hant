**明德水庫**，[臺灣一座位於](../Page/臺灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[頭屋鄉明德村的](../Page/頭屋鄉.md "wikilink")[水庫](../Page/水庫.md "wikilink")，群山環抱，湖水清澈，還有遊艇可以遊覽。經水庫管理局多年來經營規劃，是一處知名的度假旅遊景點。環繞水庫的道路為[縣道126的其中一段](../Page/縣道126號.md "wikilink")。

## 概要

明德水庫原名**後龍水庫**，位於[後龍溪支流](../Page/後龍溪.md "wikilink")[老田寮溪上](../Page/老田寮溪.md "wikilink")，主壩為土壩，壩高35.5公尺，有效蓄水量為1670萬立方公尺。

水庫建設可追溯到1952年，成立明德水庫設計勘測隊開始，1966年6月竣工，到1970年5月完工，均由台灣省[水利局辦理](../Page/水利局.md "wikilink")；目前由苗栗水利會管理，主要供應苗栗地區農業、工業用水，及部分民生用水。

明德水庫集水區內，有三個小島各有特色：**鴛鴦島**上有[原住民特色建築樓閣](../Page/原住民.md "wikilink")；**日新島**內廣植果樹；**海棠島**上有[永春宮古廟](../Page/永春宮.md "wikilink")，有吊橋可達其上，景色優美。水庫附近有明德國小、明德派出所、明德水庫管理所、明德淨水廠、教師會館、製茶廠以及勞工育樂中心。

明德水庫也曾是電影[魯冰花的拍片場景](../Page/魯冰花_\(1989年電影\).md "wikilink")。

## 基本資料

<File:Ming> De reservoir Spillway.jpg|溢洪道 <File:Ming> De
reservoir.jpg|海棠島吊橋
[File:Taiwan_MingTe_Reservoir.JPG|明德水庫](File:Taiwan_MingTe_Reservoir.JPG%7C明德水庫)
[File:Taiwan_MingTe_Reservoir_2.JPG|環繞水庫的山上開滿](File:Taiwan_MingTe_Reservoir_2.JPG%7C環繞水庫的山上開滿)[油桐花](../Page/油桐.md "wikilink")

  - 管理機關：苗栗農田水利會
  - 位置：[苗栗縣](../Page/苗栗縣.md "wikilink")[頭屋鄉](../Page/頭屋鄉.md "wikilink")
  - 河系：[後龍溪支流](../Page/後龍溪.md "wikilink")[老田寮溪](../Page/老田寮溪.md "wikilink")
  - 集水面積：61.0[平方公里](../Page/平方公里.md "wikilink")
  - 正常蓄水位：標高65[公尺](../Page/公尺.md "wikilink")
  - 最高洪水位（又稱最大可能供水位，英文簡寫CMS）：標高61.3[公尺](../Page/公尺.md "wikilink")
  - 滿水位面積：170[公頃](../Page/公頃.md "wikilink")
  - 總蓄水量：1770萬[立方公尺](../Page/立方公尺.md "wikilink")
  - 計畫有效蓄水量：1650萬立方公尺
  - 現存有效蓄水量：1244.76萬立方公尺
  - 計畫年供水量：4793萬立方公尺
  - 壩型：土石壩
  - 壩頂：標高65公尺
  - 最大壩身高度：35.5公尺
  - 壩頂長度：187公尺
  - 壩頂寬度：10公尺
  - 壩體積：46萬立方公尺
  - 灌溉：新開闢期作面積3680公頃；增產稻榖5741噸
  - 自來水：每天供給14000立方公尺

## 腳註

## 外部連結

  - [明德水庫 -
    經濟部水利署全球資訊網](https://web.archive.org/web/20140118085313/http://www.wra.gov.tw/ct.asp?xItem=19990&ctNode=4536)

[Category:苗栗縣地理](../Category/苗栗縣地理.md "wikilink")
[屋](../Category/苗栗縣旅遊景點.md "wikilink")
[Category:臺灣水庫](../Category/臺灣水庫.md "wikilink")
[Category:頭屋鄉](../Category/頭屋鄉.md "wikilink")
[Category:1970年完工基礎設施](../Category/1970年完工基礎設施.md "wikilink")