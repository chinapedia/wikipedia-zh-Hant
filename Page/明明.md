是一部[香港](../Page/香港.md "wikilink")[电影](../Page/电影.md "wikilink")，于2007年4月26日上映。由[區雪兒擔任導演](../Page/區雪兒.md "wikilink")。出演演员有[周迅](../Page/周迅.md "wikilink")、[吴彦祖](../Page/吴彦祖.md "wikilink")、[楊祐寧](../Page/楊祐寧.md "wikilink")、[张信哲等人](../Page/张信哲.md "wikilink")。

## 劇情

## 主演

  - [周迅](../Page/周迅.md "wikilink")
  - [吳彥祖](../Page/吳彥祖.md "wikilink")
  - [楊祐寧](../Page/楊祐寧.md "wikilink")
  - [張信哲](../Page/張信哲.md "wikilink")

## 外部連結

  - [官方blog](http://blog.roodo.com/mingmingthefilm)

  - [電影介紹](http://www.sooostar.com/movie/content.asp?ArticleID=102785)

  - [電影預告](http://www.sooostar.com/movie/content.asp?ArticleID=101542)

  -
  - {{@movies|fmhk20885505}}

  -
  -
  -
  -
  -
  -
  - [周迅星窩](http://www.sooostar.com/Jue/)

  - [吳彥祖星窩](http://www.sooostar.com/Daniel-Wu/)

[Category:香港愛情片](../Category/香港愛情片.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[7](../Category/2000年代香港電影作品.md "wikilink")
[Category:殺手主角題材電影](../Category/殺手主角題材電影.md "wikilink")