**明治學院大學**（）是一所[日本的](../Page/日本.md "wikilink")[基督教](../Page/基督教.md "wikilink")[私立大學](../Page/私立大學.md "wikilink")，有[東京和](../Page/東京.md "wikilink")[橫濱兩個校區](../Page/橫濱.md "wikilink")，最早創立於1863年。[詹姆斯·柯蒂斯·赫本](../Page/詹姆斯·柯蒂斯·赫本.md "wikilink")[牧師為該校創辦人之一](../Page/牧師.md "wikilink")，並擔任第一任總裁。明治學院大學的校歌由該校畢業生[島崎藤村創作](../Page/島崎藤村.md "wikilink")。

## 學部與研究所

  - 學部
      - 文學部
      - 經濟學部
      - 社會學部
      - 法學部
      - 國際學部
      - 心理學部
  - 研究所（大學院）
      - 經濟學研究科
      - 社會學研究科
      - 法學研究科
      - 國際學研究科
      - 心理學研究科
      - 法務職研究科

## 外部連結

  - [明治學院大學網站](http://www.meijigakuin.ac.jp/)

  - [明治學院大學網站](http://www.meijigakuin.ac.jp/en/)

  - [明治學院大學網站](http://web.archive.org/web/*/http://www.meijigakuin.ac.jp/index_cn.html)
    (Archive)

  - [明治學院大學網站](http://web.archive.org/web/*/http://www.meijigakuin.ac.jp/index_tw.html)
    (Archive)

[Category:长老会](../Category/长老会.md "wikilink")
[Category:東京都的大學](../Category/東京都的大學.md "wikilink")
[Category:神奈川縣的大學](../Category/神奈川縣的大學.md "wikilink")
[Category:日本私立大學](../Category/日本私立大學.md "wikilink")
[Category:1949年創建的教育機構](../Category/1949年創建的教育機構.md "wikilink")
[Category:港區 (東京都)](../Category/港區_\(東京都\).md "wikilink")
[Category:戶塚區](../Category/戶塚區.md "wikilink")