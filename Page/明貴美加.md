**明貴美加**（）是日本的插畫家、遊戲製作者。過去曾自稱是現役[女高中生的](../Page/女高中生.md "wikilink")[機械設計師](../Page/機械設計師.md "wikilink")，為給予日本全國模型少年夢想的存在。在自身的著作中介紹到，曾經有讀者在謊報的生日當天，贈送蛋糕到當時有著連載的模型雜誌[Model
Graphix的編輯部](../Page/Model_Graphix.md "wikilink")。

## 概要

在[Takara](../Page/Takara.md "wikilink")『[DUAL
MAGAZINE](../Page/DUAL_MAGAZINE.md "wikilink")』雜誌上擔任過以初心者為對象的塑膠模型製作方法單元之後，於高中時代隸屬於伸童舎，以[日昇動畫的機械設計師身分參與了](../Page/日昇動畫.md "wikilink")《[機動戰士Z
GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")》《[機動戰士GUNDAM
ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")》《[城市獵人](../Page/城市獵人.md "wikilink")》《[機動戰士GUNDAM0083](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")》等作品。

由於原本是插畫家、模型玩家的緣故，在模型雜誌[Model
Graphix畫了許多以在GUNDAM作品中登場的機械為主題的](../Page/Model_Graphix.md "wikilink")「[MS少女](../Page/MS少女.md "wikilink")」插圖（→現在的[中原れい在前述](../Page/中原れい.md "wikilink")『DUAL
MAGAZINE』上畫的『』為靈感來源）。此舉大為走紅，對後來的工作方向產生了偌大的影響。其後轉入Red Company（現在的[Red
Entertainment](../Page/Red_Entertainment.md "wikilink")），經手[美少女遊戲](../Page/美少女遊戲.md "wikilink")《[銀河少女傳說](../Page/銀河少女傳說.md "wikilink")》（）的企畫，展開長期的系列作品，包含遊戲、動畫、CD、廣播演出等，全部的插圖工作都由本人一手包辦。

性格上對版權態度相當嚴謹，在網路上支持者所模仿的圖像稀少也是因為本人直接限制的關係。銀河少女傳說系列結束之後，也著手了漫畫的原作。其後在Red
Entertainment經手[櫻花大戰系列的機械設計](../Page/櫻花大戰系列.md "wikilink")。在業界中，和[カトキハジメ與](../Page/角木肇.md "wikilink")[麻宮騎亞等人交情深厚](../Page/麻宮騎亞.md "wikilink")。

### 人物簡歷

自稱女高中生機械設計師，在Model
Graphix雜誌專欄長期扮演女性身分；在[Nakayoshi雜誌則是以](../Page/Nakayoshi.md "wikilink")「」為介紹文；於廣播節目時雖然自稱大小姐作家，但是超低沉的嗓音遭到吐槽，在第一回的播放就早早放棄不男不女的演技。本人是十分普通的成人男性，有著符合男性的外表與略瘦的容貌。

關於MS少女，曾經被[出淵裕](../Page/出淵裕.md "wikilink")「」，以及[GUNDAM原作者的](../Page/GUNDAM.md "wikilink")[富野由悠季](../Page/富野由悠季.md "wikilink")「」唸過很多次，但回顧後來的走紅，也算是對經歷的加分。公司的自己房間內被大量的各種[遊戲機與遊戲軟體所掩埋](../Page/遊戲機.md "wikilink")，是個濃厚的「御宅族部屋」（雖然不能否定職業上有研究需求的說法），在私底下也曾經為[藤島康介代為製作過美少女模型](../Page/藤島康介.md "wikilink")。似乎是位狂熱的[Sony愛好者](../Page/Sony.md "wikilink")，可能是因為自身所設計的（此為先發）和後來的[多羅相當像似而非常喜愛](../Page/井上多羅.md "wikilink")，插畫中經常附有類似多羅（ミルキー）的角色。

以[雷朋的太陽眼鏡為個人註冊商標](../Page/雷朋.md "wikilink")，在初代《銀河少女傳說》製作當時，因為中意哈德森的監製隨身配戴的雷朋眼鏡而開始愛用。此外當時工作討論不順利的明貴，不小心對當時發售遊戲的話題聊得熱絡，沉溺在遊戲沒有工作的事情因此東窗事發。

## 主要作品

### 機械設計

  - **[機動戰士GUNDAM
    0083](../Page/機動戰士GUNDAM_0083:Stardust_Memory.md "wikilink")**[GUNDAM
    GP04G](../Page/GUNDAM_GP04G.md "wikilink")(原作中GP04G只是設定上提及名稱的機體，明貴美加在「假設GP04G未有改修成加貝拉」的基礎上，在模型雜誌中發佈的追加設計。因為設計大受歡迎，成為官方認可的MSV機體。)
  - **[鋼彈前哨戰](../Page/鋼彈前哨戰.md "wikilink")**[GUNDAM
    MK-V](../Page/ORX-013.md "wikilink")(最後在月面被S鋼彈擊破被新吉翁回收改成飆狼登場於ZZ)、京密煞、G-Fortress
    PART A
  - [櫻花大戰3](../Page/櫻花大戰3.md "wikilink")（首席機械設計）
  - [櫻花大戰4](../Page/櫻花大戰4.md "wikilink")（首席機械設計）
  - [櫻花大戰V](../Page/櫻花大戰V.md "wikilink")（首席機械設計）
  - [約會大作戰](../Page/約會大作戰.md "wikilink")（AST裝備設計）
  - [約會大作戰劇場版](../Page/約會大作戰.md "wikilink")（AST裝備設計）

### 插畫提供

  - **[月刊OUT](../Page/月刊OUT.md "wikilink")**（雜誌）
  - **[デュアルマガジン](../Page/デュアルマガジン.md "wikilink")**（雜誌）
  - **[超音速のMS少女](../Page/MS少女.md "wikilink")（連載時タイトルは「帰ってきたMS少女」等）**（雜誌連載・書籍）
  - **[SDガンダム](../Page/SDガンダム.md "wikilink")**（CDインストラクション）
  - **[甲竜伝説ヴィルガスト](../Page/甲竜伝説ヴィルガスト.md "wikilink")**　
  - **[サイバーコミック](../Page/サイバーコミック.md "wikilink")**（アンソロジーコミック表紙）
  - **お嬢様通信**（雜誌連載・書籍）
  - **[GUNDAM FIXギャルーション](../Page/GUNDAM_FIX.md "wikilink")**（雜誌連載）
  - **MEGU**（雜誌連載）
  - **電撃PCエンジン→Gsエンジン→Gsマガジン**（雜誌連載）
  - **[Let\`sナデシコ](../Page/機動戦艦ナデシコ.md "wikilink")**（アンソロジーコミック）
  - **[サクラ大戦 蒸気工廠](../Page/サクラ大戦.md "wikilink")**（ムック）
  - **[サクラ大戦V 蒸気工廠USA](../Page/サクラ大戦.md "wikilink")**（ムック）

### 獨自活動

  - **ホワイトリング**（Figure）人物設計

<!-- end list -->

  -
    和[海洋堂所屬原型師](../Page/海洋堂.md "wikilink")[ボーメ合作](../Page/ボーメ.md "wikilink")。

<!-- end list -->

  - **アグレッシブガール**（Figure）人物設計

<!-- end list -->

  -
    和因為MS少女而成為明貴美加支持者的原型師[みすまる☆ましい合作](../Page/みすまる☆ましい.md "wikilink")。

<!-- end list -->

  - **硬質少女有限公司**（Figure+同人誌）原作・人物設計

<!-- end list -->

  -
    同人誌和美少女Figure結合在一起的原創企畫。

<!-- end list -->

  - **HMX M.A・Collection**（同人誌）未發表插畫集

<!-- end list -->

  -
    以[To Heart為主的畫集](../Page/To_Heart.md "wikilink")。

[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:機甲設計師](../Category/機甲設計師.md "wikilink")