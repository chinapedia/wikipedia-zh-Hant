《**易经**》是中国漢族最古老的[文獻之一](../Page/中國古典典籍.md "wikilink")\[1\]，並被[儒家尊為](../Page/儒家.md "wikilink")「[五經](../Page/五經.md "wikilink")」之始；一般说上古三大奇书包括《[黄帝內经](../Page/黄帝內经.md "wikilink")》、《**易经**》、《[山海经](../Page/山海经.md "wikilink")》，但它们成书都较晚。《易经》以一套符号系统來描述状态的简易、变易、不易，表現了中国古典文化的哲学和宇宙观。它的中心思想，是以演譯自然運行的內在特徵與規律，解讀[阴阳的交替变化描述世间万物](../Page/阴阳.md "wikilink")。《易经》最初用於[占卜和預報天氣](../Page/占卜.md "wikilink")，但它的影响遍及中国的[哲学](../Page/哲学.md "wikilink")、[宗教](../Page/宗教.md "wikilink")、[政治](../Page/政治.md "wikilink")、[經濟](../Page/經濟.md "wikilink")、[医学](../Page/中医.md "wikilink")、[天文](../Page/天文.md "wikilink")、[算术](../Page/算术.md "wikilink")、[文学](../Page/文学.md "wikilink")、[音乐](../Page/中国音乐.md "wikilink")、[艺术](../Page/艺术.md "wikilink")、[军事和](../Page/军事.md "wikilink")[武术等各方面](../Page/武术.md "wikilink")，是一部無所不包的巨著。在[四庫全書中為經部](../Page/四庫全書.md "wikilink")，[十三經中未經](../Page/十三經.md "wikilink")[秦始皇焚書之害](../Page/秦始皇.md "wikilink")，它是**最早哲學書**。自從十七世紀開始，《易經》也被介绍到西方。

《易》原有[三種版本](../Page/三易.md "wikilink")：《[连山](../Page/連山易.md "wikilink")》、《[归藏](../Page/归藏.md "wikilink")》和《周易》\[2\]，《连山》和《归藏》已经失传，一般所称《易經》即源於《**周易**》发展而來。

《易》也叫《易經》，特指原文内容。由于三易之中唯周易有幸传世，因此《易经》通常指《周易》。对《周易》作注解的版本较多，最早期的也是最著名的作品就是《易传》。所以，不要混淆《易经》跟《易传》，这是两个不同的概念。《易传》的上半部是《周易》原文，为《经》部分。下半部《传》由孔子及弟子们联合撰写，对周易进行主观的个人见解注释，其实并不等同《周易》原文的真正内涵。《易传》的下半部《传》的内容包括《彖辞》、《象辞》、《系辞》、《文言》、《序卦》、《说卦》、《杂卦》等篇，其中《彖辞》、《象辞》、《系辞》各分上下，统共十篇，旧称《[十翼](../Page/十翼.md "wikilink")》。

## 名字的由來

  - 「易」字

:\*「易」字有幾種解釋：

:\*\# 易，蜥易（变色龙）象形。\[3\]

:\*\#
必须指出，理解西周之“易”，理当以西周礼乐制度的变革为条件。礼指“从容之节”，易即雅乐，都是统治阶级驾驭黎民百姓，维护[宗法制度的手段和工具](../Page/宗法制度.md "wikilink")。《周易》保存了[西周钟鼓](../Page/西周.md "wikilink")“交响乐”的框架规制，钟鸣鼎食在西周的底层社会是难以想象的。

:\*\#
日月為易，象徵陰陽。取自[东汉](../Page/东汉.md "wikilink")[魏伯阳](../Page/魏伯阳.md "wikilink")《周易参同契》

:\*\#
日出為易。[陳鼓應認為這個意思](../Page/陳鼓應.md "wikilink")，也是「[-{乾}-](../Page/周易六十四卦列表#乾.md "wikilink")」的本義。

:\*\# 易是[占卜之名](../Page/占卜.md "wikilink")。

:\*\# 變易、變化的意思，指[天下萬物是常變的](../Page/天下.md "wikilink")，故此《周易》是教導人面對變易的書。

:\*\# 交易，亦即陰消陽長、陽消陰長的相互變化。如一般的[太極圖所顯示的一樣](../Page/太極圖.md "wikilink")。

:\*\#
『易』即是「[道](../Page/道.md "wikilink")」的運行，恆常的真理卻也不斷更新變化，即使事物隨著時空變幻，仍有內在規律，恆常的道不變。

:\*\#
日升而天明，日落而天黯。明而接黯，黯而續明。終而复始，無始無終。一如生死。一如成敗。一如興亡。合明，生，成，興之類為陽。總黯，死，敗，亡之屬為陰。陰陽相生相剋，萬事周而复始，是謂易。繫辭傳云，生生之謂易。生生者，不絕也。

:\*\# 简易，简约。

:\*\# 不易，不变，变化是现象，不变是规律。

:\*[東漢](../Page/東漢.md "wikilink")[鄭玄的著作](../Page/鄭玄.md "wikilink"){{〈}}易論{{〉}}認為「易一名而含三義：簡易一也；變易二也；不易三也。」這句話總括了易的三種意思：「簡易」、「變易」和「恆常不變」。即是說宇宙的事物存在狀能的是：

:\*\# 順乎自然的，表現出易和簡兩種性質；

:\*\# 時時在變易之中；

:\*\# 又保持一種恆常。

:\*如《[詩經](../Page/詩經.md "wikilink")》所說「日就月將」或「如月之恆，如日之升」，日月的運行表現出一種非人為的自然，這是簡易；其位置、形狀卻又時時變化，這是變易；然而總是東方出、西方落這是「不易」。

  - 「周」字

:\*《周易》一名的「周」字有幾種不同的解釋：

:\*\# 「周」，東漢鄭玄{{〈}}易論{{〉}}，認為「周」是「周普」的意思，即無所不備，周而復始。

:\*\#
[唐代](../Page/唐代.md "wikilink")[孔穎達](../Page/孔穎達.md "wikilink")《[周易正義](../Page/周易正義.md "wikilink")》認為「周」是指中國陝西省[岐山縣](../Page/岐山縣.md "wikilink")（古稱西岐，西周發祥地）地名，是周朝的代稱。

  - 「經」字

:\*《易經》的「經」字是指經典的著作。儒家奉《**周易**》、《[尚書](../Page/尚書.md "wikilink")》、《[詩經](../Page/詩經.md "wikilink")》、《[禮記](../Page/禮記.md "wikilink")》、《[春秋](../Page/春秋.md "wikilink")》為《[五經](../Page/五經.md "wikilink")》。如同前文所說，「經」是後來為了尊稱這些書，而加上的稱呼，原來《五經》只稱為《易》、《詩》、《書》、《禮》、《春秋》。

## 用法

《[汉书·艺文志](../Page/艺文志.md "wikilink")》所载的“术数”中，第四种是“蓍龟”。[龟指龟甲](../Page/龟甲.md "wikilink")，用龟甲占，这种方法叫“卜”。[蓍指蓍草](../Page/蓍.md "wikilink")，用这种草占，叫做“筮”。《易经》本来是为筮用的\[4\]。用蓍草占得某一卦某一爻后，查《易经》，看其卦象，卜定凶吉。

后来，有些人即使不卜卦，也会引用卜辞和爻辞的话，加以引申发挥，作为自己论述的依据。《左传》中记载，在晋国和楚国的战争中，晋国的将领不服命令，擅自渡河追击楚军，知庄子说：“此师殆哉，周易有之，在师[iching-hexagram-07.svg](https://zh.wikipedia.org/wiki/File:iching-hexagram-07.svg "fig:iching-hexagram-07.svg")之临[iching-hexagram-19.svg](https://zh.wikipedia.org/wiki/File:iching-hexagram-19.svg "fig:iching-hexagram-19.svg")曰，师出以律，否臧凶\[5\]”。意思是，出兵以纪律为主，如果部队不遵守纪律，会带来很大的灾祸。在这段记载中，知庄子并没有占卜，而是直接引用师卦的爻辞。

《系辞传》中写道，“是故君子居则观其象而玩其辞，动则观其变而玩其占。是以自天佑之，吉无不利。”意思是，君子什么时候行动，就占个卜预知凶吉；在没有行动的时候，就仔细体会《易经》的卦象以及卦中爻辞的意思，这样就可“吉无不利”。这表明《易经》不仅仅是一个占卜的书，也可作为一本道德教训的书。

[孔子和](../Page/孔子.md "wikilink")[荀子都常引用](../Page/荀子.md "wikilink")《易经》中的卦和爻辞，并将其引申发挥。比如，《论语·子路》中写道：子曰：“南人有言曰：‘人而无恒，不可以作巫医。’善夫！”“不恒其德，或承之羞。”子曰：“不占而已矣。”\[6\]。
其中，“不恒其德，或承之羞”是恒卦[iching-hexagram-32.svg](https://zh.wikipedia.org/wiki/File:iching-hexagram-32.svg "fig:iching-hexagram-32.svg")九三爻的爻辞，孔子以此引证人必须有恒心，又说“不占而已矣”，意为不必占卜就可以引用这一爻作为教训。荀子引用易经的例子可见于《荀子·非相篇》和《荀子·大略篇》。

## 三易

「**三易**」是指[中國古籍記載的三種占卜方法](../Page/中國古典典籍.md "wikilink")，包括《[連山](../Page/連山易.md "wikilink")》、《[歸藏](../Page/歸藏.md "wikilink")》及《[周易](../Page/周易.md "wikilink")》（易经）。共通性都是由[八個经卦](../Page/八卦.md "wikilink")、八個经卦兩兩重叠的[六十四個别卦组成](../Page/六十四卦.md "wikilink")\[7\]。其中《連山》、《歸藏》失傳已久，現存三易的內容以《周易》（易经）为主。不過1993年湖北江陵荊州鎮（現荊州市郢城鎮）出土的王家台秦簡《易占》與輯本《歸藏》相符，被認為《歸藏》古文再現。

## 傳統歷史的說法

### 《周易》(易經)

《**周易**》或稱《**易經**》，在幾種較早期的文獻中（例如《論語》、《莊子》、《左傳》等）稱之為《易》。

「周易」之名最早見於《[周禮](../Page/周禮.md "wikilink")》（原稱《周官》，約成書在[戰國時代](../Page/戰國.md "wikilink")）。

周易的成書時間歷來颇多爭論。傳說遠古的[伏羲創八卦](../Page/伏羲.md "wikilink")、[夏禹將其擴充為六十四卦](../Page/夏禹.md "wikilink")，六十四卦被記載在《[連山](../Page/連山.md "wikilink")》一書，《連山》以「艮」為第一卦。到了[商朝](../Page/商朝.md "wikilink")，六十四卦的次序被重新排列，被記載在《[歸藏](../Page/歸藏.md "wikilink")》一書，以「[坤](../Page/周易六十四卦列表#坤.md "wikilink")」為第一卦。

依據[司馬遷](../Page/司馬遷.md "wikilink")《[史記](../Page/史記.md "wikilink")》的記「囚羑里，蓋益易之八卦為六十四卦。」，後人因此認為《周易》是[商朝末年](../Page/商.md "wikilink")、[西周之初的時候確立](../Page/西周.md "wikilink")，是[周文王奠定了](../Page/周文王.md "wikilink")《周易》以「[-{乾}-](../Page/周易六十四卦列表#乾.md "wikilink")」為第一卦，並為每一卦寫下「卦辭」（卦象的解釋）。周文王之子、[周武王之弟](../Page/周武王.md "wikilink")[周公旦則被認為是](../Page/周公旦.md "wikilink")「爻辭」（每一爻的解釋）的創立者。卦辭和爻辭的內容不單影響周朝的歷史，也影響到「[詩經](../Page/詩經.md "wikilink")」的文學風格。

《周易》起源相當早，相傳「文王拘而演周易」，所以坊間認為西周初年由[文王所著](../Page/周文王.md "wikilink")，因此較[春秋時代的哲學著作](../Page/春秋时期.md "wikilink")（[老子和孔子的論語](../Page/老子.md "wikilink")）為早。

### 《十翼》(易傳)

廣義的《易》包括《周易》和《易傳》。由於《周易》文字含義隨時代演變，内容在春秋戰國時便已不易讀懂，因此孔子撰寫了《[十翼](../Page/十翼.md "wikilink")》，又稱為《[易傳](../Page/易傳.md "wikilink")》，以解讀《周易》。現今學者多認為《周易》七卷書中最早的《易傳》是戰國時代的作品。

根據《[史记](../Page/史记.md "wikilink")》，[春秋時期](../Page/春秋.md "wikilink")[孔子對](../Page/孔子.md "wikilink")《周易》有所闡述與解釋，參與了〈彖傳〉（上下兩篇）、〈象傳〉（上下兩篇）、〈[繫辭傳](../Page/繫辭傳.md "wikilink")〉（上下兩篇）、〈[文言傳](../Page/文言傳.md "wikilink")〉、〈序卦傳〉、〈說卦傳〉、〈雜卦傳〉等篇章的編輯\[8\]。共有十篇，稱為「[十翼](../Page/十翼.md "wikilink")」。

到了[漢武帝以後](../Page/漢武帝.md "wikilink")，「十翼」被稱為《[易傳](../Page/s:易傳.md "wikilink")》，並被列為《易經》的一部分。

《易傳》為孔子所作，但也有學者認為是集體創作。宋代[歐陽修提出](../Page/歐陽修.md "wikilink")《易傳》不是同一人所作。清初學者[惠棟说](../Page/惠棟.md "wikilink")：“栋四世咸通汉学，以汉犹近古，去圣未远故也。《诗》、《礼》毛、郑，《公羊》何休，传注具存；《尚书》、《左传》，伪孔氏全采马、王，杜元凯根本贾、服；唯《周易》一经，汉学全非。”\[9\]。當代的學者認為《易傳》裏面的篇章最早出現於中国[戰國時期](../Page/戰國_\(中國\).md "wikilink")，但亦有部分篇章是[西漢年代所著](../Page/西漢.md "wikilink")。

## 近代考古研究

[I-Ching-chinese-book.jpg](https://zh.wikipedia.org/wiki/File:I-Ching-chinese-book.jpg "fig:I-Ching-chinese-book.jpg")

在近五十年，出現了新的《易經》歷史研究，西方和中國的學者根據商、周朝的占卜用的獸骨和龜甲上的[甲骨文](../Page/甲骨文.md "wikilink")，[青銅器上的](../Page/青銅器.md "wikilink")[鐘鼎文以及其他史料研究](../Page/鐘鼎文.md "wikilink")。1973年，[湖南](../Page/湖南.md "wikilink")[長沙的](../Page/長沙.md "wikilink")[西漢](../Page/西漢.md "wikilink")[馬王堆出土了將近完整的](../Page/馬王堆.md "wikilink")、公元前二世紀的《易經》、《[道德經](../Page/道德經.md "wikilink")》和其他書籍，是現存《易經》的最早版本，並包含了以前一直被認為是孔子所著的〈繫辭傳〉上、下，但並不包括《十翼》其他的部分。

當代的學者懷疑周文王、孔子並非《易經》的全部作者，部分學者更認為六十四卦的概念比八卦更早形成。學者比較過長沙馬王堆出土的《易經》和周朝的鐘鼎文之後，認為《周易》不是周文王一人所著，最可能的成書日期應是[西周後期](../Page/西周.md "wikilink")，大約公元前九世紀末。現時一般認為《易經》是集體創作，《周易》的起源並非任何一個傳說或歷史人物的著作，而是西周時期占筮用的文字編纂而成。

## 《周易》註本書目

有關周易及其研究的善本書目如下：

  - [東周](../Page/東周.md "wikilink")（前770年－前256年）[春秋末期](../Page/春秋.md "wikilink")[魯國](../Page/魯國.md "wikilink")[孔子](../Page/孔子.md "wikilink")：《[十翼](../Page/十翼.md "wikilink")》(《易傳》)
  - [春秋末期](../Page/春秋.md "wikilink")[子夏](../Page/子夏.md "wikilink")：《子夏易傳》
  - [西漢](../Page/西漢.md "wikilink")（前202年－8年）[京房
    (魏郡太守)](../Page/京房_\(魏郡太守\).md "wikilink")：《京氏易傳》
  - [東漢](../Page/東漢.md "wikilink")（25年－220年）[鄭玄](../Page/鄭玄.md "wikilink")：《周易鄭康成註》
  - [曹魏](../Page/曹魏.md "wikilink")（220年－266年）[王弼](../Page/王弼.md "wikilink")、[晉](../Page/晉.md "wikilink")[韓康伯注](../Page/韓康伯.md "wikilink")、[唐](../Page/唐.md "wikilink")[孔穎達疏](../Page/孔穎達.md "wikilink")：《周易正義》（周易註疏、周易兼義）（[五經正義](../Page/五經正義.md "wikilink")、[十三經注疏之一](../Page/十三經注疏.md "wikilink")）
  - [東晉](../Page/東晉.md "wikilink")（317年－420年）[韓康伯註](../Page/韓康伯.md "wikilink")《周易注》（繫辭、說卦、序卦、雜掛）\[10\]
  - [唐](../Page/唐.md "wikilink")（618年－907年）[李鼎祚](../Page/李鼎祚.md "wikilink")：《周易集解》
  - [北宋](../Page/北宋.md "wikilink")（960年—1127年）[程頤](../Page/程頤.md "wikilink")：《伊川易傳》
  - [北宋](../Page/北宋.md "wikilink")（960年—1127年）[蘇軾](../Page/蘇軾.md "wikilink")：《東坡易傳》
  - [北宋](../Page/北宋.md "wikilink")（960年—1127年）[朱震](../Page/朱震.md "wikilink")：《漢上易傳》
  - [南宋](../Page/南宋.md "wikilink")（1127年—1279年）[朱熹](../Page/朱熹.md "wikilink")：《周易本義》
  - [南宋](../Page/南宋.md "wikilink")（1127年—1279年）[楊簡](../Page/楊簡.md "wikilink")：《楊氏易傳》
  - [南宋](../Page/南宋.md "wikilink")（1127年—1279年）[張浚](../Page/張浚.md "wikilink")：《紫巖易傳》
  - [南宋](../Page/南宋.md "wikilink")（1127年—1279年）[楊萬里](../Page/楊萬里.md "wikilink")：《誠齋易傳》
  - [宋](../Page/宋.md "wikilink")（）[王宗傳](../Page/王宗傳.md "wikilink")：《童溪易傳》
  - [清](../Page/清.md "wikilink")（約1636年－1912年）[劉一明](../Page/劉一明.md "wikilink")：《周易闡真》
  - [清](../Page/清.md "wikilink")（約1636年－1912年）[李道平](../Page/李道平.md "wikilink")：《周易集解纂疏》

## 流傳與學派

相傳[秦始皇](../Page/秦始皇.md "wikilink")[焚書坑儒之时](../Page/焚書坑儒.md "wikilink")，[李斯將](../Page/李斯.md "wikilink")《周易》列入醫術占卜之書而得以幸免。之後各個朝代都有人研究《周易》，包括[漢代的](../Page/漢代.md "wikilink")[京房](../Page/京房.md "wikilink")、[鄭玄](../Page/鄭玄.md "wikilink")，[魏晋時代的](../Page/魏晋.md "wikilink")[王弼](../Page/王弼.md "wikilink")，[唐代的](../Page/唐代.md "wikilink")[陸德明](../Page/陸德明.md "wikilink")、[李鼎祚](../Page/李鼎祚.md "wikilink")、[孔穎達](../Page/孔穎達.md "wikilink")，[宋代的](../Page/宋代.md "wikilink")[邵雍](../Page/邵雍.md "wikilink")、[程頤](../Page/程頤.md "wikilink")、[朱熹等等](../Page/朱熹.md "wikilink")。

宋代興起了易圖的研究，像廣為人知的[河圖、洛書](../Page/河圖洛書.md "wikilink")、先天[八卦圖](../Page/八卦.md "wikilink")、後天[八卦圖](../Page/八卦.md "wikilink")、[太極圖](../Page/太極圖.md "wikilink")（含[陰陽魚的圓形圖案](../Page/太極.md "wikilink")）等，都是《易經》原著中所無、後人根據對《易經》的理解添加進去的。

歷代研究周易的大致可分為两個學派：[義理派和](../Page/義理派.md "wikilink")[象數派](../Page/象數派.md "wikilink")。義理派強調從八卦和六十四卦的卦名的涵義來解釋卦爻象和卦辭、爻辭。象數派注重從八卦所象徵的物象來解釋卦爻象和卦辭、爻辭。亦有人認為義理派發掘周易的哲學價值，象數派則著重將周易用於占卜。前者如[王弼](../Page/王弼.md "wikilink")、[程颐](../Page/程颐.md "wikilink")，后者如[京房](../Page/京房.md "wikilink")、[邵雍](../Page/邵雍.md "wikilink")。

[明朝末年](../Page/明朝.md "wikilink")，《易經》被[傳教士翻譯並傳播到西方](../Page/傳教士.md "wikilink")。17世紀末，德國哲學家及數學家[莱布尼茨因](../Page/莱布尼茨.md "wikilink")[漢學大師](../Page/漢學.md "wikilink")[布維](../Page/布維.md "wikilink")（Joachim
Bouvet，漢名[白晉](../Page/白晉.md "wikilink")，1662年－1732年）的介紹、更將《易經》以[二進制解釋](../Page/二進制.md "wikilink")。現在無論[中國大陸](../Page/中國大陸.md "wikilink")、[台灣和西方各國都有不少人研究](../Page/台灣.md "wikilink")《易經》。

根據[江弘遠](../Page/江弘遠.md "wikilink")《京房易學流變考》一書，自從《京氏易傳》出現後，受朱熹、[惠棟](../Page/惠棟.md "wikilink")，以及《[四庫總目提要](../Page/四庫總目提要.md "wikilink")》的推波助瀾，均認為此書及其條例是出自[京房本人之手](../Page/京房_\(魏郡太守\).md "wikilink")。本書以[沈延國論點加以延伸](../Page/沈延國.md "wikilink")，確定京房是以所統領的六十卦反復配六十鐘律，[八宮卦](../Page/六十四卦#八宮六十四卦.md "wikilink")、納甲、納音則是由[荀爽](../Page/荀爽.md "wikilink")、[虞翻](../Page/虞翻.md "wikilink")、[干寶等另一非京氏易學流派演變而來](../Page/干寶.md "wikilink")\[11\]，從《京氏易傳》出現後，遂取代京房原有的模式。

## 爻卦結構

### 爻

易經的內容以「卦」組成，共有六十四卦。每一卦由六層組成，每一層稱為「[爻](../Page/爻.md "wikilink")」（ㄧㄠˊ/yáo）。每一爻以一條長的橫線「—」代表陽，稱為「陽爻」；或以兩條斷開的橫線「--」代表陰，稱為「陰爻」。從最底層數起，總共有六爻，而六爻以不同的陰、陽配搭，形成六十四種不同的組合。

[Yin_Yang.svg](https://zh.wikipedia.org/wiki/File:Yin_Yang.svg "fig:Yin_Yang.svg")

### 卦

六爻可以分為上半部分和下半部分，每一部分稱為一個[單卦](../Page/八卦.md "wikilink")。六個「爻」以不同的陰、陽配搭，形成多種不同的組合。六爻由下而上解最下為：《初、二、三、四、五及上》，由三爻所生的卦「由上而下」為之「上卦」或「外卦」、在下方為之「下卦」或「內卦」。

### 組合

後人以「[無極生太極](../Page/無極.md "wikilink")、[太極生兩儀](../Page/太極.md "wikilink")、[兩儀生四象](../Page/兩儀.md "wikilink")、四象生八卦、[八卦生](../Page/八卦.md "wikilink")[六十四卦](../Page/六十四卦.md "wikilink")」來解釋的卦的構成。

1.  [無極](../Page/無極.md "wikilink")
2.  [太極](../Page/太極.md "wikilink")：（☯）代表一，傳統的[太極圖代表了陰陽互補](../Page/太極圖.md "wikilink")；
3.  [兩儀](../Page/兩儀.md "wikilink")：一分為二，分開了陰和陽，即是兩儀；
4.  四象：二分為四，即是四象：太陽、少陽、少陰、太陰；
5.  [八卦](../Page/八卦.md "wikilink")：四分為八，即是八卦：乾、兌、離、震、巽、坎、艮、坤；
6.  [六十四卦](../Page/六十四卦.md "wikilink")：兩個八卦相疊，即成八八六十四卦。

[Xiantianbagua.png](https://zh.wikipedia.org/wiki/File:Xiantianbagua.png "fig:Xiantianbagua.png")

|    |    |    |    |    |   |   |   |        |
| -- | -- | -- | -- | -- | - | - | - | ------ |
| 八卦 | ☷  | ☶  | ☵  | ☴  | ☳ | ☲ | ☱ | ☰      |
| 八卦 | 坤  | 艮  | 坎  | 巽  | 震 | 離 | 兌 | \-{乾}- |
| 四象 | 太陰 | 少陽 | 少陰 | 太陽 |   |   |   |        |
| 両儀 | 陰  | 陽  |    |    |   |   |   |        |
|    | 太極 |    |    |    |   |   |   |        |

伏羲八卦次序圖

|      |    |    |    |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
| ---- | -- | -- | -- | -- | - | - | - | - | - | - | - | - | -- | - | - | - | - | - | - | - | - | -- | - | - | - | - | - | - | - | - | -- | - | - | - | - | - | - | -- | - | -- | -- | - | -- | -- | - | - | - | -- | - | - | - | -- | -- | - | - | - | - | -- | - | -- | -- | -- | - | - |
| 六十四卦 | 坤  | 剝  | 比  | 觀  | 豫 | 晉 | 萃 | 否 | 謙 | 艮 | 蹇 | 漸 | 小過 | 旅 | 咸 | 遯 | 師 | 蒙 | 坎 | 渙 | 解 | 未濟 | 困 | 訟 | 升 | 蠱 | 井 | 巽 | 恆 | 鼎 | 大過 | 姤 | 復 | 頤 | 屯 | 益 | 震 | 噬嗑 | 隨 | 无妄 | 明夷 | 賁 | 既濟 | 家人 | 豐 | 離 | 革 | 同人 | 臨 | 損 | 節 | 中孚 | 歸妹 | 睽 | 兌 | 履 | 泰 | 大畜 | 需 | 小畜 | 大壯 | 大有 | 夬 | 乾 |
| 三十二  |    |    |    |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
| 十六   |    |    |    |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
| 八卦   | 坤  | 艮  | 坎  | 巽  | 震 | 離 | 兌 | 乾 |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
| 四象   | 太陰 | 少陽 | 少陰 | 太陽 |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
| 両儀   | 陰  | 陽  |    |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |
|      | 太極 |    |    |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |   |   |    |   |   |   |   |   |   |    |   |    |    |   |    |    |   |   |   |    |   |   |   |    |    |   |   |   |   |    |   |    |    |    |   |   |

朱熹『周易本義』伏羲六十四卦次序圖

### 六十四卦

## 易數與二元運算（二進制）

[Diagram_of_I_Ching_hexagrams_owned_by_Gottfried_Wilhelm_Leibniz,_1701.jpg](https://zh.wikipedia.org/wiki/File:Diagram_of_I_Ching_hexagrams_owned_by_Gottfried_Wilhelm_Leibniz,_1701.jpg "fig:Diagram_of_I_Ching_hexagrams_owned_by_Gottfried_Wilhelm_Leibniz,_1701.jpg")得自[白晋的圖文](../Page/白晋.md "wikilink")，時為清康熙四十年）\]\]
[DualerAufbau.JPG](https://zh.wikipedia.org/wiki/File:DualerAufbau.JPG "fig:DualerAufbau.JPG")

17世紀末，[德国](../Page/德国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[数学家](../Page/数学家.md "wikilink")[莱布尼茨在法國傳教士](../Page/戈特弗里德·威廉·莱布尼茨.md "wikilink")[白晉的介紹之下](../Page/白晉.md "wikilink")，得到[邵雍的伏羲先天六十四卦](../Page/邵雍.md "wikilink")〈方圓四分四層圖〉，認為與他所創的二元運算（二進制）相符合，贊揚備至。後來演進成[布爾代數](../Page/布爾代數.md "wikilink")。\[12\]

莱布尼茨以二進制解讀易經的六十四卦。莱布尼茨在《[致德雷蒙信](../Page/致德雷蒙信.md "wikilink")》中指出，易经中的六十四卦图形，恰恰与他在二十多年前发明的二进制计数法相类似：阴爻--
可以用0表示，阳爻—可以用1表示。

莱布尼茨又说以0和1二数可以表示万物，一如易经中阴爻--，阳爻—生生不息，滋生万物。

莱布尼茨还进一步[附会八卦表示上帝七日创造世界](../Page/附会.md "wikilink")，坤
000：为天地洪荒，万物皆空；第一日：艮 001 为上帝造天，第二日：坎
010 为天地同在……第七日：-{乾}- 111乃万物具备，是为[安息日](../Page/安息日.md "wikilink")。

而[中国有广为流传的观点认为现代](../Page/中国.md "wikilink")[计算机的](../Page/计算机.md "wikilink")[二进制来自于中国的八卦](../Page/二进制.md "wikilink")，但这早已被证明是一个传说。对这一错误，郭书春在《古代世界数学泰斗刘徽》一书461页指出：“中国有所谓《周易》创造了二进制的说法，至于[莱布尼茨受](../Page/戈特弗里德·威廉·莱布尼茨.md "wikilink")《周易》八卦的影响创造二进制并用于计算机的传说，更是广为流传。事实是，莱布尼兹先发明了二进制，后来才看到传教士带回的宋代学者重新编排的《周易》八卦，并发现八卦可以用他的二进制来解释。”因此，并不是[莱布尼茨看到阴阳八卦才发明二进制](../Page/莱布尼茨.md "wikilink")。[梁宗巨著](../Page/梁宗.md "wikilink")《数学历史典故》一书14～18页对这一历史公案有更加详尽考察，想进一步了解者可参考。\[13\]

## Unicode

## 參見

  - [無極](../Page/無極.md "wikilink")、[太極](../Page/太極.md "wikilink")、[两仪](../Page/两仪.md "wikilink")、[四象](../Page/四象.md "wikilink")、[六爻](../Page/六爻.md "wikilink")、[八卦](../Page/八卦.md "wikilink")、[六十四卦](../Page/六十四卦.md "wikilink")
  - [阴阳](../Page/阴阳.md "wikilink")、[五行](../Page/五行.md "wikilink")
  - [易传](../Page/易传.md "wikilink")、[易学](../Page/易学.md "wikilink")

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[周礼](../Page/周礼.md "wikilink")》
  - [司馬遷](../Page/司馬遷.md "wikilink")：《[史记](../Page/史记.md "wikilink")》
  - （美）罗特斯坦 著，李晓东 译：《心灵的标符：音乐与数学的内在生命——大美译丛》，吉林人民出版社，2001年，ISBN
    978-7-206-03821-1
  - [刘大钧](../Page/刘大钧.md "wikilink")：《周易概论》，齐鲁书社，1988年，ISBN 753330053
  - [南怀瑾](../Page/南怀瑾.md "wikilink")：《易经繫传别讲》，中国世界语出版社，1996年，ISBN
    7-5052-0181-6
  - [朱谦之](../Page/朱谦之.md "wikilink")：《中国哲学对欧洲的影响》第二章{{〈}}中国哲学与启明运动{{〉}}，河北人民出版社，ISBN
    7-202-02560-4
  - [程石泉](../Page/程石泉.md "wikilink")：《易學新探》、《易辭新詮》、《易學新論》 （「程氏易學三書」）

## 研究書目

  - 李學勤：《周易溯源》（成都：巴蜀書社，2006）。
  - 金谷治：于時化譯：《易的占筮與義理》（濟南：齊魯書社，1990）。
  - 邢文：《帛書周易研究》（北京：人民出版社，1997）。
  - 朱伯崑：《易學哲學史》（北京：北京大學出版社，1986-）。
  - 池田知久：〈[周易与原始儒学](http://www.nssd.org/articles/article_read.aspx?id=6600057)〉。
  - 劉述先：〈[從發展觀點看「周易」時間哲學與歷史哲學之形成](http://readopac3.ncl.edu.tw/nclJournal/GetPDF?tid=A01017989&jid=00000431&eid=3f83faecfe4f898c938aa4e9bf3407a0)〉。
  - 淺野裕一：〈[儒家對《易》的經典化](http://www.ica.org.cn/nlb/content.aspx?nodeid=415&page=ContentPage&contentid=5042)〉。
  - 夏含夷：〈[是筮法还是释法——由清华简《筮法》重新考虑《左传》筮例](http://www.nssd.org/articles/article_read.aspx?id=665372123)〉。
  - 夏含夷：〈[《周易》“元亨利贞”新解——兼论周代习贞习惯与《周易》卦爻辞的形成](http://www.nssd.org/articles/article_read.aspx?id=35930194)〉。
  - 夏含夷：〈[《周易》筮法原无“之卦”考](http://www.nssd.org/articles/article_read.aspx?id=1002685583)〉。
  - 顾明栋：〈[《周易》明象与现代语言哲学及诠释学](http://www.nssd.org/articles/Article_Read.aspx?id=30955470)〉。
  - 小岛祐马：〈[关于《周易》的几个问题](http://www.nssd.org/articles/article_read.aspx?id=1003331987)〉。
  - 赫尔穆特·威廉：〈[左传国语中的易经占筮辞](http://www.nssd.org/articles/article_read.aspx?id=1002258741)〉。
  - 李学勤：〈[《周易》与中国文化](http://www.nssd.org/articles/article_read.aspx?id=20284428)〉。
  - 李零：〈[跳出《周易》看《周易》](http://ccl.pku.edu.cn/chlib/articles/%E8%B7%B3%E5%87%BA%E5%91%A8%E6%98%93%E7%9C%8B%E5%91%A8%E6%98%93%E6%95%B0%E5%AD%97%E5%8D%A6%E7%9A%84%E5%86%8D%E8%AE%A4%E8%AF%86.pdf)〉。
  - 李零：〈[读上博楚简《周易》](http://ccl.pku.edu.cn/chlib/articles/%E8%AF%BB%E4%B8%8A%E5%8D%9A%E6%A5%9A%E7%AE%80%E5%91%A8%E6%98%93.pdf)〉。
  - 李博賢 Lars Bo Christensen: Book of Changes - The Original Core of the
    I Ching (英文) Amazon 2015 〈[预览](http://www.zhouyi.dk)〉。
  - 李鏡池：《周易探源》（北京：中華書局，1987）。
  - [高亨](../Page/高亨.md "wikilink")：《周易古經通說》，華正書局有限公司，2005年2月1日，ISBN
    978-957-580-109-0
  - 高亨：《周易古經今注》
  - 高亨：《周易大傳今注》

## 外部链接

  - [中國哲學書電子化計劃](../Page/中國哲學書電子化計劃.md "wikilink")：[《易經》全文](http://ctext.org/book-of-changes/zh)
      - [簡體](http://ctext.org/book-of-changes/zhs)
      - [繁體](http://ctext.org/book-of-changes/zh)
      - [中英文對照版](http://ctext.org/book-of-changes/zh?en=on)
  - [易經哲學](http://ap6.pccu.edu.tw/encyclopedia/data.asp?id=467&forepage=2)
    - （《中華百科全書》）
  - [马来西亚易经网](https://web.archive.org/web/20100420004835/http://www.iching.com.my/index-ch.html)
  - [易经六十四卦](https://web.archive.org/web/20090706141255/http://staticlake.appspot.com/iching/index.html)
  - [周易](http://guji.artx.cn/Article/695.html)
  - [易经的奥秘](https://web.archive.org/web/20100211091235/http://tansuo.cntv.cn/humanities/yijingdeaomi/videopage/index.shtml)
    -
    [中央电视台](../Page/中央电视台.md "wikilink")，《[百家讲坛](../Page/百家讲坛.md "wikilink")》
      - （百家讲坛官方频道）

      - （百家讲坛官方频道）
  - [易經易卦查詢器](https://web.archive.org/web/20100313145114/http://wisdomfish2jsf2.appspot.com/iching/BaGua.jsf)
    大智若魚
  - [I Ching english/french web edition](http://en.infinitao.com)
  - [易經查詢系統](http://yijing.cdict.info) 易經古籍快速交互查詢
  - [思维易图](http://pan.baidu.com/s/1vNt6R)
  - [读易经](http://duyijing.cn) 周易原文 系辞爻辞象辞交互查询
  - [《周易》在给我们讲故事](http://www.jianshu.com/p/ffa08d8ef429) 用讲故事的方法介绍《周易》

{{-}}

[I](../Category/經部易類.md "wikilink") [经4](../Category/四书五经.md "wikilink")
[I](../Category/十三经.md "wikilink") [易经](../Category/易经.md "wikilink")
[I](../Category/占卜.md "wikilink")
[Category:中國思想](../Category/中國思想.md "wikilink")

1.  [（上海）讲座：周易的智慧](http://www.nlc.cn/sjwhbb/sjjcjz/201608/t20160804_127634.htm)
2.  《周礼‧春官‧大卜》：「掌三易之法，一曰[連山](../Page/連山.md "wikilink")。二曰[歸藏](../Page/歸藏.md "wikilink")，三曰[周易](../Page/周易.md "wikilink")。其經卦皆八，其別皆六十有四。」
3.  [1](http://www.zdic.net/z/1b/xs/6613.htm)
4.
5.
6.
7.  《[周禮](../Page/周禮.md "wikilink")‧春官》：“大卜掌三易之法，一曰[連山](../Page/連山易.md "wikilink")，二曰[歸藏](../Page/歸藏.md "wikilink")，三曰[周易](../Page/周易.md "wikilink")。其[經卦皆八](../Page/八卦.md "wikilink")，其別皆[六十有四](../Page/六十四卦.md "wikilink")。”
8.  《[史记](../Page/史记.md "wikilink")·孔子世家》曰：“孔子晚而喜《易》，[《序》《彖》《繫》《象》《说卦》《文言》](../Page/十翼.md "wikilink")。”
9.  《松崖文钞》卷一《上制军尹元长先生书》
10. 樓宇烈《袁宏與東晉玄學》，收入《國學研究（第一卷）》，67-92頁，北京：北京大學出版社，1993.3
11. 《四库提要》称“发挥汉儒之学，以荀爽，虞翻为主，而参以郑康成、宋咸、干宝诸家之说，皆融会其义，自为注而自疏之。”
12. Die mathematische schriften von Gottfried Wilhelm Leibniz, vol. VII
    C. I. Gerhardt (ed) pp 223-227
    [英文翻譯](http://www.leibniz-translations.com/binary.htm)
13. [数学科普：常识性谬误令人忧](http://my.hoopchina.com/book/blog/2460513.html)