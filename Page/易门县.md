**易门县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省中部](../Page/云南省.md "wikilink")，是[玉溪市下属的一个县](../Page/玉溪市.md "wikilink")；位于[东经](../Page/东经.md "wikilink")101°54'-102°18'，[北纬](../Page/北纬.md "wikilink")24°26'-24°57'。

境内有老黑山（小街乡）、扒河和绿汁江。特产有：野生菌，大龙口酒，豆豉，有滇中水城菌乡易门美誉。易门民风淳朴，酒风彪悍，环境优美，警察敬业，社会治安模范。

## 行政区划

下辖：\[1\] 。

## 参考资料

[易门县](../Category/易门县.md "wikilink") [县](../Category/玉溪区县.md "wikilink")
[玉溪](../Category/云南省县份.md "wikilink")
[Category:中华人民共和国第三批资源枯竭型城市](../Category/中华人民共和国第三批资源枯竭型城市.md "wikilink")

1.