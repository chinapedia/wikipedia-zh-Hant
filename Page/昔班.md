**昔班**（；）是[成吉思汗的孫](../Page/成吉思汗.md "wikilink")、[拔都的兄弟](../Page/拔都.md "wikilink")、[蓝帐汗国第一代可汗](../Page/蓝帐汗国.md "wikilink")。

## 生平

他是[朮赤第五個兒子](../Page/朮赤.md "wikilink")，因為他太年輕，他父親死時，得不到任何土地。他在1241年的匈牙利戰役中表現出色；[阿布哈齊曾經說](../Page/阿布哈齊.md "wikilink")，在俄羅斯戰役中，他曾經向拔都多要五千人，以便打敗俄羅斯人。

[拉施德丁曾說](../Page/拉施德丁.md "wikilink")，如蒙古人留在匈牙利，他會作為總督，他後來分到南[烏拉爾山較低的地區至](../Page/烏拉爾山.md "wikilink")[鄂毕河作為作為封土](../Page/鄂毕河.md "wikilink")，他的汗國名稱為[藍帳汗國](../Page/藍帳汗國.md "wikilink")，封地是在[斡儿答封地之北](../Page/斡儿答.md "wikilink")，包括[薩雷河](../Page/薩雷河.md "wikilink")、[楚河](../Page/楚河.md "wikilink")、[錫爾河](../Page/錫爾河.md "wikilink")，隨著冬天，他在[烏拉爾河南部扎營](../Page/烏拉爾河.md "wikilink")，夏天在[伏爾加河與](../Page/伏爾加河.md "wikilink")[烏拉爾山活動](../Page/烏拉爾山.md "wikilink")。他還送了一萬五千戶給斡兒答。這國家大約在哈薩克的中，西部，西至[車里雅賓斯克](../Page/車里雅賓斯克.md "wikilink")，東至[塞米伊](../Page/塞米伊.md "wikilink")。他汗国在金帐与白帐之间，有一說法[西伯利亞被解作](../Page/西伯利亞.md "wikilink")**昔班**的地方，因為他的封地確實是在西伯利亞。

**[脫脫迷失](../Page/脫脫迷失.md "wikilink")**時代，[白帳汗國向西移](../Page/白帳汗國.md "wikilink")，藍帳汗國向南移，漸漸接近[河中農業區](../Page/河中.md "wikilink")。[昔班尼時代入主河中](../Page/昔班尼.md "wikilink")，維持一百年。直到1599年昔班家族絕後為止。没有移居河中的昔班家族後人和[台不花別吉的後人交替管轄](../Page/台不花別吉.md "wikilink")[西伯利亚汗国](../Page/西伯利亚汗国.md "wikilink")，直至[俄罗斯帝国征服](../Page/俄罗斯帝国.md "wikilink")

## 家庭

昔班生有12子

1.八伊纳儿

2\.[勒哈都尔](../Page/勒哈都尔.md "wikilink")

3.哈答黑

4.巴勒哈

5.扯勒克

6.蔑儿干

7.忽尔都合

8.爱牙赤

9.塞勒罕

10.伯颜察儿

11.马札儿

12.贡齐

## 資料來源

草原帝國：昔班家族成員

[Category:蒙古帝国皇族](../Category/蒙古帝国皇族.md "wikilink")
[Category:蒙古君主](../Category/蒙古君主.md "wikilink")
[Category:金帳汗國人物](../Category/金帳汗國人物.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")