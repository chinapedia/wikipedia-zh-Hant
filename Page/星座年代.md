**星座年代**（Astrological
age）是[星相學家根據](../Page/星相學家.md "wikilink")[星象和當時宗教關係的一種理論](../Page/星象.md "wikilink")。這種理論被學者用來解釋[古埃及及](../Page/古埃及.md "wikilink")[中東地區的](../Page/中東.md "wikilink")[政權交替](../Page/政權.md "wikilink")。

根據[畢華流在他的](../Page/畢華流.md "wikilink")《[畢華流談星座](../Page/畢華流談星座.md "wikilink")2》引述西方星相學家的論說，星座與當時的宗教關係大致如下：

1.  [金牛座年代](../Page/金牛座年代.md "wikilink")：古埃及人開始崇拜[金牛犢的信仰](../Page/金牛犢.md "wikilink")。
2.  [白羊座年代](../Page/白羊座年代.md "wikilink")：[亞伯拉罕的](../Page/亞伯拉罕.md "wikilink")[沙漠一神教的興起](../Page/亞伯拉罕諸教.md "wikilink")，因為他們以[羔羊作祭獻](../Page/羔羊.md "wikilink")。
3.  [雙魚座年代](../Page/雙魚座年代.md "wikilink")：[耶穌基督的](../Page/耶穌基督.md "wikilink")[基督教席捲全球](../Page/基督教.md "wikilink")。另一方面，耶穌的門徒中有幾位本身亦是漁民，而他亦多次以捕漁來形容傳[福音的工作](../Page/福音.md "wikilink")。

因此，他們預計，當[寶瓶座年代來臨時](../Page/寶瓶座年代.md "wikilink")，世人就會轉奉一種和[寶瓶座所代表意義一致的宗教信仰](../Page/寶瓶座.md "wikilink")。

不過，另一方面，有學者持相反意見，指是世間上的星象學家在觀看星座的流傳，認為人的宗教信仰應該與當時的[上升星座相配](../Page/上升星座.md "wikilink")，而星座的轉換就像人間的朝代交替一樣。作為人類，我們應該“順應天意”，隨著天象的轉變而更換宗教信仰。他們根據此理論，解釋了為何在亞伯拉罕的年代，古埃及經歷[饑荒](../Page/饑荒.md "wikilink")，但亞伯拉罕仍然可以得到充足的糧食，原因是亞伯拉罕一族改信了以羔羊為代表的[上帝](../Page/上帝.md "wikilink")，他們去到埃及地購入神牛回家作食糧。而在埃及地，由於他們仍然相信金牛，所以奉牛為神聖而不食用。這事情亦導致古埃及王朝的衰落，以及[喜克索斯人的入侵](../Page/喜克索斯.md "wikilink")。

## 星座年代與文化

在[流行文化當中](../Page/流行文化.md "wikilink")，有不少與星座年代有著含蓄的關係。例如「[新世紀音樂](../Page/新世紀音樂.md "wikilink")」，就被視為代表水瓶座年代的音樂；當中的代表人物是Secret
Garden。

另外，[英國著名品牌Body](../Page/英國.md "wikilink")
Shop的收入，據稱是用來支持「[新纪元运动](../Page/新纪元运动.md "wikilink")」。而日本[花王](../Page/花王.md "wikilink")（Kao）公司的標誌月亮，亦被視為水瓶年代的重要象徵。

| 星座年代                               | Guenther Wachsmuth\[1\] | Neil Mann\[2\] | Shephard Simpson\[3\] |
| ---------------------------------- | ----------------------- | -------------- | --------------------- |
| [獅子座年代](../Page/獅子座.md "wikilink") | 前10500年－前7900年          |                |                       |
| [巨蟹座年代](../Page/巨蟹座.md "wikilink") | 前7900年－前6500年           | 前8600年－前6450年  |                       |
| [雙子座年代](../Page/雙子座.md "wikilink") | 前6500年－前4500年           | 前6450年－前4300年  |                       |
| [金牛座年代](../Page/金牛座.md "wikilink") | 前4500年－前1900年           | 前4300年－前2150年  | 前4525年－前1875年         |
| [白羊座年代](../Page/白羊座.md "wikilink") | 前1900年－前100年            | 前2150年－1年      | 前1875年－前100年          |
| [雙魚座年代](../Page/雙魚座.md "wikilink") | 前100年－2500年             | 1年－2150年       | 前100年－2680年           |
| [水瓶座年代](../Page/水瓶座.md "wikilink") | 2500年－4400年             | 2150年－         | 2680年－                |
|                                    |                         |                |                       |

不同學者所計算的星座年代

## 參考

## 外部連結

  - [THE GRAND AGES AND THE COMING OF
    AQUARIUS](http://www.greatdreams.com/ages.htm)
  - [An Astrological
    Age](https://web.archive.org/web/20041205071602/http://www.geocities.com/astrologyages/astrologicalage.htm)

[fi:Vesimiehen aika](../Page/fi:Vesimiehen_aika.md "wikilink")
[ru:Астрологическая
эра](../Page/ru:Астрологическая_эра.md "wikilink")

[X](../Category/占星术.md "wikilink") [X](../Category/时间单位.md "wikilink")
[Category:新紀元運動](../Category/新紀元運動.md "wikilink")

1.  [The Grand Ages](http://www.greatdreams.com/ages.htm)
2.  Mann, Neil, *[W.B. Yeats and a Vision: The Astrological Great
    Year](http://www.yeatsvision.com/GreatYear.html)*, 2000s
3.  Simpson, Shephard, Dr., *[An Astrological
    Age](https://web.archive.org/web/20091027103009/http://geocities.com/astrologyages/astrologicalage.htm)*,
    2000s