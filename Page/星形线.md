[Astroid.svg](https://zh.wikipedia.org/wiki/File:Astroid.svg "fig:Astroid.svg")
[HypotrochoidOn4.gif](https://zh.wikipedia.org/wiki/File:HypotrochoidOn4.gif "fig:HypotrochoidOn4.gif")

**星形线**（）或稱為**四尖瓣線**（），是一個有四個[尖點的](../Page/尖點.md "wikilink")[內擺線](../Page/內擺線.md "wikilink")，也屬於[超橢圓的一種](../Page/超橢圓.md "wikilink")。所有星形线皆可以依以下的方程式比例縮放而得\[1\]：

\[x^{2/3} + y^{2/3} = 1. \,\]

其英文名稱得名自希臘文的「星星」，星形线幾乎和[橢圓的](../Page/橢圓.md "wikilink")[渐屈线相同](../Page/渐屈线.md "wikilink")。

若讓一個半徑為1/4的圓在一個半徑為1的圓內部，延著圓的圓周旋轉，小圓圓周上的任一點形成的軌跡即為星形线。星形线的[參數方程為](../Page/參數方程.md "wikilink")\[2\]：

\[x=\cos^3\theta,\qquad y=\sin^3\theta.\]

星形线是一個為0[代數曲線的實數軌跡](../Page/代數曲線.md "wikilink")，其方程式如下：

\[(x^2+y^2-1)^3+27x^2y^2=0. \,\]

因此星形线為六次曲線，在實數平面上有四個尖瓣的[奇点](../Page/奇点_\(几何\).md "wikilink")，分別是星形线的四個頂點，在無限遠處還有二個複數的尖瓣的奇點，四個重根的複數奇點，因此星形线共有十個奇点。

星形线的是，其方程式為\(\textstyle x^2 y^2 = x^2 + y^2.\)。星形線的[渐屈线為另一個二倍大的渐屈线](../Page/渐屈线.md "wikilink")。

一個半徑為\(a\)之圓的[內擺線構成的星形线](../Page/內擺線.md "wikilink")，其面積為\(\frac{3}{8} \pi a^2\)，周長為6a。

## 相關條目

  - ：一個有三個尖瓣的內擺線

  - ：星形线在磁學中的應用。

## 參考資料

  -
  -
  -
## 外部連結

  -
  - ["Astroid" at The MacTutor History of Mathematics
    archive](http://www-history.mcs.st-andrews.ac.uk/history/Curves/Astroid.html)

  - ["Astroïde" at Encyclopédie des Formes Mathématiques
    Remarquables](http://www.mathcurve.com/courbes2d/astroid/astroid.shtml)
    (in French)

  - [Article
    on 2dcurves.com](http://www.2dcurves.com/roulette/roulettea.html)

  - [Visual Dictionary Of Special Plane Curves, Xah
    Lee](http://xahlee.org/SpecialPlaneCurves_dir/Astroid_dir/astroid.html)

  - [Bars of an
    Astroid](http://demonstrations.wolfram.com/BarsOfAnAstroid/) by
    Sándor Kabai, The Wolfram Demonstrations Project.

[分類:六次曲線](../Page/分類:六次曲線.md "wikilink")

[pl:Asteroida
(geometria)](../Page/pl:Asteroida_\(geometria\).md "wikilink")

[Category:曲线](../Category/曲线.md "wikilink")

1.
2.