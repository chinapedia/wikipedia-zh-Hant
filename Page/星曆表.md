**星曆表**，簡稱**曆表**，源自希臘文**（ephemeros）**，刊載一個或多個天體每天特定時刻位置的數據表列，通常還附帶其他補充材料；而**天文年曆**也是星曆表的一種。

星曆表最早源於[Johannes
Stadius在](../Page/:en:Johannes_Stadius.md "wikilink")1554年出版的「auctae新星曆表」，該星曆表列出行星位置，但未完全正確。例如在Stadius星曆表中水星位置就有10度以上的週期性誤差。

表中列出每天在特定時刻（正午或子夜）的太陽系天體的視位置（[直角座標系統的地平高度](../Page/直角坐标系.md "wikilink")、赤道座標系的[赤經與](../Page/赤經.md "wikilink")[赤緯](../Page/赤緯.md "wikilink")、黃道座標系的[黃經與](../Page/黃經.md "wikilink")[黃緯等](../Page/黃緯.md "wikilink")）用於高精度測量的星曆表更會列出較亮恆星的位置，因計算之恆星以上萬計，所編成的星曆表亦相當厚。

星曆表至少可以推導過去與未來數個世紀的天體位置。雖然[天體力學計算的精度已很高](../Page/天體力學.md "wikilink")，對不久的未來的位置可依賴計算得知。但長遠而言仍有不確定的因素，例如為數眾多質量仍未知的[小行星所造成的](../Page/小行星.md "wikilink")[攝動是不能被忽略](../Page/攝動.md "wikilink")。星曆表最常用在天體測量時[校對天體的特殊位置](../Page/校對.md "wikilink")，地球上這種差異極小，很多時候不會被注意到，但對於測量接近地球的[小行星或是精確校正](../Page/小行星.md "wikilink")[月球位置時](../Page/月球.md "wikilink")，此時差異就變得很重要，因為這可能意味著一些外在因素使其有這樣的變化出現或者是檢定儀器或人為方面的誤差等。

現在更有用於電腦上，可動態演示位置的天文軟件出現，能列出天上幾乎任何天體，[行星和其](../Page/行星.md "wikilink")[衛星的動態位置](../Page/衛星.md "wikilink")，如果有需要還可列出[彗星或](../Page/彗星.md "wikilink")[小行星](../Page/小行星.md "wikilink")，通常只需幾個點擊就可列出，十分方便；星曆表為[太空船的太空探測](../Page/太空船.md "wikilink")、以及地面望遠鏡對[恆星和](../Page/恆星.md "wikilink")[星系的觀測與定位提供重要資訊](../Page/星系.md "wikilink")。

## 占星學用到的星曆

有些民用的星曆表也會提供天文學家與占星家有興趣的[天象資料](../Page/天象.md "wikilink")，例如[日食](../Page/日食.md "wikilink")、[月食](../Page/月食.md "wikilink")、行星視運動資料、[恆星時](../Page/恆星時.md "wikilink")、[月相](../Page/月相.md "wikilink")，還有一些[小行星在特定時間的位置](../Page/小行星.md "wikilink")。有些還會列出太陽系天體每月位置，其赤道坐標位置、黃經等。

在占星學而言，雖然始終是以地球為中心點計算，但以太陽為中心點計算的占星學正在發展，不過以此標準計算的星曆表還未完善，並且由於這種以太陽為中心的特殊星曆表必須計算和用於取代不適用的以地球為中心的標準[西洋占星學來構成](../Page/占星學.md "wikilink")[天宫图](../Page/天宫图.md "wikilink")。

## 天文年曆

為天文測量而編制的天文年曆幾乎都會包含前述的天體的[赤道坐標位置](../Page/赤道座標系統.md "wikilink")，因為這是在以全天星圖和望遠鏡[天文觀測上常用到的](../Page/天文觀測.md "wikilink")，且編算時所採用的曆元必須標示出來。現今大部分所採用的曆元皆以[J2000.0為編算基準](../Page/J2000.0.md "wikilink")（一些用在高精度測量方面的星曆表甚至以該年年中的曆元為準），一些20世紀編算的天文年曆仍會用上[B1950.0](../Page/B1950.0.md "wikilink")。

天文年曆與星曆表的不同之處在於，天文年曆（尤其是民用的天文年曆）會刊登[天文愛好者更常用到的觀測資料](../Page/天文愛好者.md "wikilink")，包括月球、行星、主要小行星或彗星的赤道座標，還有諸如與太陽角距、亮度、與地球距離、運行速度、視直徑、相位與出沒、上中天時刻等單純與位置有關資訊等等，或者是火星、木星的中央經度、行星衛星的全年位置，[土星更會包括環傾角等](../Page/土星.md "wikilink")，給天文攝影的同好預測拍攝的有利時間。其他方面包括流星雨出沒時間、變星亮度極大的預測時間等。

## 外部連結

### 天文學星曆表

  - [NASA噴氣推進實驗室線上星曆表系統](../Page/噴氣推進實驗室在線曆書系統.md "wikilink")（適用於全世界）及其[介紹](http://iau-comm4.jpl.nasa.gov/README)（英文）
  - [星曆表軟件](http://www.moshier.net/)，Steve Moshier設計
  - 天文愛好者實用[星曆表](http://www.nightskies.net/almanac/almanac.html)─美國《天空和望遠鏡》雜誌

### 占星學星曆表

  - [前600年－2400年星曆表](http://www.khaldea.com/ephemcenter.shtml)，在1900至2050年還包括天體方位。
  - [1891年至2100年](https://web.archive.org/web/20070814062218/http://www.achernar.btinternet.co.uk/fm.html)
    地心編算星曆表
  - [1](http://astrologysoftware.com/ephemeris/monthly.asp) 每月星曆表
  - [2](http://www.astro.com/swisseph/swepha_z.htm) 6,000年星曆表

### 相關論文

  - [小行星的攝動對星曆表精度的長期影響](https://web.archive.org/web/20050820025502/http://www.iers.org/iers/publications/tn/tn29/tn29_061.pdf)（英文）
  - Kharin, A. S. and Yuri B. KolesnikKolesnik, Y.
    B.；*由光學觀測導致的星曆表誤差（英文）*
    (1990),IAU SYMP.141 P.189, 1989.

## 參考書目

  -
<!-- end list -->

  -
[E](../Category/曆法.md "wikilink") [E](../Category/占卜.md "wikilink")
[E](../Category/天文现象.md "wikilink")