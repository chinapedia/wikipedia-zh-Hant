**星際物質**（缩写为）是存在於[星系和](../Page/星系.md "wikilink")[恆星之間的](../Page/恆星.md "wikilink")[物質和](../Page/物質.md "wikilink")[輻射場](../Page/輻射場.md "wikilink")（）的总称。星際物質在天文物理的準確性中扮演著關鍵性的角色，因為它是介於星系和恆星之間的中間角色。恆星在星際物質密度較高的分子雲中形成，並且經由[行星狀星雲](../Page/行星狀星雲.md "wikilink")、[恆星風](../Page/恆星風.md "wikilink")、和[超新星獲得](../Page/超新星.md "wikilink")[能量和物質的重新補充](../Page/能量.md "wikilink")。換個角度看，恆星和星際物質的相互影響，可以協助測量星系中氣體物質的消耗率，也就是恆星形成的活耀期的時間。

以地球的標準，星際物質是極度稀薄的[電漿](../Page/等離子體.md "wikilink")、[氣體](../Page/氣體.md "wikilink")、和[塵埃](../Page/宇宙塵.md "wikilink")，是[離子](../Page/離子.md "wikilink")、[原子](../Page/原子.md "wikilink")、[分子](../Page/分子.md "wikilink")、[塵埃](../Page/宇宙塵.md "wikilink")、[電磁輻射](../Page/電磁輻射.md "wikilink")、[宇宙射線](../Page/宇宙射線.md "wikilink")、和[磁場的混合體](../Page/磁場.md "wikilink")。物質的成分是99%的氣體和1%的塵埃，充滿在星際間的空間。這種極端稀薄的混合物，典型的密度從每立方公尺只有數百到數億個質點，以[太初核合成的結果來看氣體的成分](../Page/太初核合成.md "wikilink")，在數量上應該是90%[氫和](../Page/氫.md "wikilink")10%的[氦](../Page/氦.md "wikilink")，和其他微跡的「[金屬](../Page/金屬量.md "wikilink")」（以天文學說法，除氢和氦以外的元素都是金屬）。

2013年9月12日，美国航空航天局正式宣布，[旅行者1号在](../Page/旅行者1号.md "wikilink")2012年8月25日已经达到了星际物质（ISM），使其成为第一个这样做的人造物体。星际等离子体和灰尘会被研究直到任务结束的2025年。

[Voyager.jpg](https://zh.wikipedia.org/wiki/File:Voyager.jpg "fig:Voyager.jpg")是第一个到达星际物质的人造物体。\]\]

## 星際物質

<table>
<caption>align=center colspan=5|<strong>星際物質（ISM）</strong></caption>
<thead>
<tr class="header">
<th><p>成份</p></th>
<th><p>百分比<br />
（體積）</p></th>
<th><p>溫度<br />
（K）</p></th>
<th><p>密度<br />
（原子／cm³）</p></th>
<th><p>狀態</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/分子雲.md" title="wikilink">分子雲</a></p></td>
<td><p>&lt; 1 %</p></td>
<td><p>20 - 50</p></td>
<td><p>10<sup>2</sup> - 10<sup>6</sup></p></td>
<td><p>氫分子</p></td>
</tr>
<tr class="even">
<td><p>冷中性物質（CNM）</p></td>
<td><p>1-5%</p></td>
<td><p>50 - 100</p></td>
<td><p>20 - 50</p></td>
<td><p>中性氫原子</p></td>
</tr>
<tr class="odd">
<td><p>溫中性物質（WNM）</p></td>
<td><p>10-20%</p></td>
<td><p>6000 - 10000</p></td>
<td><p>0.2 - 0.5</p></td>
<td><p>中性氫原子</p></td>
</tr>
<tr class="even">
<td><p>溫離子物質（WIM）</p></td>
<td><p>20-50%</p></td>
<td><p>8000</p></td>
<td><p>0.2 - 0.5</p></td>
<td><p>游離的氫</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/電離氫區.md" title="wikilink">H II區</a></p></td>
<td><p>&lt; 1%</p></td>
<td><p>8000</p></td>
<td><p>10<sup>2</sup> - 10<sup>4</sup></p></td>
<td><p>游離的氫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日冕.md" title="wikilink">氣體暈</a><br />
熱離子物質（HIM）</p></td>
<td><p>30-70%</p></td>
<td><p>10<sup>6</sup> - 10<sup>7</sup></p></td>
<td><p>10<sup>-4</sup> - 10<sup>-2</sup></p></td>
<td><p>高度游離的<br />
（氫和微跡金屬）</p></td>
</tr>
</tbody>
</table>

這些介質也是造成[消光與](../Page/消光.md "wikilink")[紅化的原因](../Page/消光.md "wikilink")。當光線在穿越這些介質的旅程中，[光強度的衰減程度與觀測的波長有密切的關聯](../Page/光強度.md "wikilink")，這些星際物質造成光子的散射和吸收，使得肉眼觀察的夜晚天空背景變得黑暗。在數千光年範圍內的分子雲對來自銀河盤面的背景星光造成均勻且一致的吸收，使得只有銀河盤面的一些裂縫中才有背景星光能被地球上的人类觀察到。

遠紫外線會被星際物質中性成分吸收，例如[氫原子會吸收](../Page/氫.md "wikilink")121.5奈米的波長的光线，這是來自[來曼α线的能量躍遷](../Page/來曼系.md "wikilink")。因此，距離地球數百光年以外的恆星，在這個波段上所发出的光便幾乎無法看見，因為在前來[地球的漫長旅程中](../Page/地球.md "wikilink")，這個波長幾乎都已經被吸收掉了。

星際物質通常可以依據溫度的差異分成三種狀態：數百萬K的高熱氣體、數千K的溫暖氣體、和數十K的冷氣體，這些狀態是這些氣體在溫度的平衡上所表現出的冷或熱。關於星際物質這三種型態的模型最初是[McKee和](../Page/Christopher_McKee.md "wikilink")[Ostriker在](../Page/Jeremiah_P._Ostriker.md "wikilink")1977年的一編論文中提出來的。經歷了過去四分之一個世紀的研究，在科學界，星際物質在這三種狀態上的相對數量仍然有相當大的爭議。

未來，對星際物質的研究重點是[分子雲](../Page/分子雲.md "wikilink")、[星際雲](../Page/星際雲.md "wikilink")、[超新星遗迹](../Page/超新星遗迹.md "wikilink")、[行星狀星雲](../Page/行星狀星雲.md "wikilink")、和[擴散結構](../Page/擴散結構.md "wikilink")。

## 星雲

[Chandra-crab.jpg](https://zh.wikipedia.org/wiki/File:Chandra-crab.jpg "fig:Chandra-crab.jpg")。這個圖片混合了來自哈伯的光學數據（[紅色](../Page/紅色.md "wikilink")）以及來自錢卓的X光圖片（[藍色](../Page/藍色.md "wikilink")）\]\]
**[星云](../Page/星云.md "wikilink")**就是散布在[银河系内](../Page/银河系.md "wikilink")、[太阳系外的一堆堆非恒星形状的尘埃和](../Page/太阳系.md "wikilink")[气体](../Page/气体.md "wikilink")（**星际物质**），它们的主要成份是[氢](../Page/氢.md "wikilink")，其次是[氦](../Page/氦.md "wikilink")，还含有一定比例的[金属元素和](../Page/金属元素.md "wikilink")[非金属元素](../Page/非金属元素.md "wikilink")。近年来的研究还发现含有[OH](../Page/OH.md "wikilink")、[CO和](../Page/CO.md "wikilink")[CH₄等](../Page/甲烷.md "wikilink")[有机分子](../Page/有机分子.md "wikilink")。

最初所有在宇宙中的云雾状[天体都被称作星云](../Page/天体.md "wikilink")。后来随着[天文望远镜的发展](../Page/天文望远镜.md "wikilink")，人们的观测水平不断提高，才把原来的星云划分为[星团](../Page/星团.md "wikilink")、[星系和星云三种类型](../Page/星系.md "wikilink")。

### 发现

1758年8月28日晚上，当时受雇天文观测的法国天文学家[查尔斯·梅西耶在搜寻](../Page/查尔斯·梅西耶.md "wikilink")[彗星的时候](../Page/彗星.md "wikilink")，在[金牛座发现一个云雾状的斑块](../Page/金牛座.md "wikilink")。为了让其他人不把这些[天体当作彗星](../Page/天体.md "wikilink")，他为此进行了专门的建档。到1784年，他一共找到类似的天体103个，当年在金牛座找到的那个天体被编为[M1](../Page/蟹狀星雲.md "wikilink")。（参看[梅西耶天体列表](../Page/梅西耶天体列表.md "wikilink")）

1781年，梅西耶公布了自己的发现。英国天文学家[威廉·赫歇耳非常重视](../Page/威廉·赫歇耳.md "wikilink")，并且亲自逐一对梅西耶发现的这些天体进行了观测核实。他发现其中有些天体确实是云雾状的，他把这些天体称为“星云”。

### 种类

以形态划分，可分为：

  - [弥漫星云](../Page/弥漫星云.md "wikilink")
  - [行星状星云](../Page/行星状星云.md "wikilink")
  - [超新星殘骸](../Page/超新星殘骸.md "wikilink")。

以发光性质划分，则可分为：

  - [发射星云](../Page/发射星云.md "wikilink")
  - [反射星云](../Page/反射星云.md "wikilink")
  - [暗星云](../Page/暗星云.md "wikilink")。

有的星云是[恒星的出生地](../Page/恒星.md "wikilink")，星云的尘埃在引力下漸漸收縮成为新的星，如[猎户座的](../Page/猎户座.md "wikilink")[M42星云](../Page/M42.md "wikilink")；也有的是老恒星爆炸后的残骸，如[天鹅座的](../Page/天鹅座.md "wikilink")[网状星云](../Page/网状星云.md "wikilink")。由于观测工具的限制，历史上，[星系曾与星云混为一谈](../Page/星系.md "wikilink")。

## 成分

星际物质包括星际气体和[星际尘埃](../Page/星际尘埃.md "wikilink")。星际气体包括气态的[原子](../Page/原子.md "wikilink")、[分子](../Page/分子.md "wikilink")、[电子](../Page/电子.md "wikilink")、[离子等](../Page/离子.md "wikilink")，主要由[氢元素组成](../Page/氢.md "wikilink")，其次是[氦](../Page/氦.md "wikilink")，其元素丰度与[恒星基本一致](../Page/恒星.md "wikilink")。星际尘埃是直径大约为10<sup>-5</sup>厘米的固体颗粒，包括冰状物、[石墨](../Page/石墨.md "wikilink")、[硅酸盐等](../Page/硅酸盐.md "wikilink")，弥散在星际气体当中，质量大约占星际气体的10%。

[银河系中的星际物质主要分布在](../Page/银河系.md "wikilink")[旋臂中](../Page/旋臂.md "wikilink")，占到了银河系总质量的10%，[密度大约为每立方厘米一个](../Page/密度.md "wikilink")[氢原子](../Page/氢.md "wikilink")，这种密度其实很低，在人造的[真空中都无法达到](../Page/真空.md "wikilink")。

## 歷史

"星際的"這個名詞最早出現在1626年，是[弗朗西斯·培根在他的文稿中使用的](../Page/弗朗西斯·培根.md "wikilink")。他寫道："The
Interstellar Skie.. hath .. so much Affinity with the Starre, that there
is a Rotation of that, as well as of the Starre." (*Sylva* §354–5).

自然哲學家[羅伯特·博伊爾在](../Page/羅伯特·博伊爾.md "wikilink")1674年的論述中提到："星際中的空間在享樂主義的觀點中是空無一物的"。直到19世紀，星際物質的本質才受到天文學家和科學家的注意。

在1862年，帕特孫寫道："氣流引發的顫動，或是震動運動，是[以太充塞在空中造成的](../Page/以太.md "wikilink")。"（*Ess.
Hist. & Art*
10）以太的觀念延續到20世紀，有些特性被描述出來。在1912年，[威廉·亨利·皮克林寫道](../Page/威廉·亨利·皮克林.md "wikilink")："造成星際吸收的介質簡單的說就是乙太，他會選擇性的吸收，就如[卡普坦所指出的是一些氣體的特性](../Page/卡普坦.md "wikilink")，還有一些自由的氣體分子，她們可能是由太陽和恆星經常不斷的釋放出來…..."

在1913年，挪威的探險家兼物理學家[克利欣·白克蘭寫道](../Page/克利欣·白克蘭.md "wikilink")："以我們的觀點，假設空間整體充滿了電子，各種電子和離子的飛躍，似乎是自然的結果，因為我們假設恆星系統在演化的過程中，不停的將帶電的微粒拋射入太空中。因此在宇宙各處，也就是"空無一物"的太空中，都能發現物質充塞著，不僅是在太陽系和星雲之中，應該是合情合理的。（See
"Polar Magnetic Phenomena and Terrella Experiments", in The Norwegian
Aurora Polaris Expedition 1902-1903 (publ. 1913, p.720).

在1930年，塞繆爾․L․桑代克記載著： "..
實在很難相信存在於恆星之間的巨大空間會完全的空無一物，地球的極光可能是被來自於太陽帶電粒子，從太陽輻射出來的粒子激發產生的。如果其他數以百萬計的恆星也都發射出離子，如果是毫無疑問的，那麼星系之間便不可能是絕對的真空了。"

## 问题

由于大量星际物质的存在，天体发射出来的光线被吸收、减弱，这称作[星际消光](../Page/消光.md "wikilink")。此外，天体的光线还被[散射](../Page/散射.md "wikilink")，使光线变红，这称作[星际红化](../Page/消光.md "wikilink")。在恒星研究中需要对星际红化进行修正。

## 參考資料

1.  *Physical Processes in the Interstellar Medium*, L. Spitzer, 1978
    (New York: Wiley)
2.  *Physics of the Interstellar Medium*, J. Dyson, 2nd Ed., 1997
    (London: Taylor & Francis)
3.  [Wisconsin H-Alpha Mapper
    Survey](http://www.astro.wisc.edu/wham/survey/)
4.  Pickering, W. H.,
    "\[<http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1912MNRAS>..72..740P
    Solar system, the motion of the, relatively to the interstellar
    absorbing medium\]" (1912) *Monthly Notices of the Royal
    Astronomical Society*, Vol. 72, p.740
5.  Thorndike, S. L., "[Interstellar
    Matter](http://articles.adsabs.harvard.edu//full/seri/PASP./0042//0000099.000.html)"
    (1930) *Publications of the Astronomical Society of the Pacific*,
    Vol. 42, No. 246, p.99

## 相關條目

  - [星際雲](../Page/星際雲.md "wikilink")
  - [宇宙塵](../Page/宇宙塵.md "wikilink")
  - [擴散星際帶](../Page/擴散星際帶.md "wikilink")
  - [星際邁射](../Page/星際邁射.md "wikilink")
  - [星際紅化](../Page/星際紅化.md "wikilink")
  - [日球](../Page/太陽圈.md "wikilink")
  - [星系空間](../Page/星系空間.md "wikilink")
  - [星際分子列表](../Page/星際分子列表.md "wikilink")
  - [外太空](../Page/外太空.md "wikilink")
  - [太陽系](../Page/太陽系.md "wikilink")
  - [恆星系](../Page/恆星系.md "wikilink")
  - [油母質](../Page/油母質.md "wikilink")
  - [托林](../Page/托林.md "wikilink")
  - [恒星形成](../Page/恒星形成.md "wikilink")

[星际物质](../Category/星际物质.md "wikilink")
[Category:天体化学](../Category/天体化学.md "wikilink")
[Category:太空](../Category/太空.md "wikilink")