**春日局**是1989年1月1日－12月17日[NHK所放映的第](../Page/NHK.md "wikilink")27部[大河劇](../Page/大河劇.md "wikilink")。

故事描寫生於戰國亂世的江戶幕府三代將軍德川家光之乳母春日局的一生，而春日局在一般人的印象中是屬於強勢的角色，劇作家橋田壽賀子則給予新的詮釋，將春日局演繹為一位全心獻身於乳母之職的女人。平均收視率為32.4%，來自女性觀眾的支持者相當多。

## 工作人員

  - 原作・劇本：[橋田壽賀子](../Page/橋田壽賀子.md "wikilink")
  - 音樂：[坂田晃一](../Page/坂田晃一.md "wikilink")
  - 旁白：[奈良岡朋子](../Page/奈良岡朋子.md "wikilink")

## 配役

  - [春日局](../Page/春日局.md "wikilink") -
    [安間千紘](../Page/安間千紘.md "wikilink")→[尾羽智加子](../Page/尾羽智加子.md "wikilink")→[大原麗子](../Page/大原麗子.md "wikilink")
  - [德川秀忠](../Page/德川秀忠.md "wikilink") -
    [中村雅俊](../Page/中村雅俊.md "wikilink")
  - 阿安 - [佐久間良子](../Page/佐久間良子.md "wikilink")
  - [德川家康](../Page/德川家康.md "wikilink") -
    [丹波哲郎](../Page/丹波哲郎.md "wikilink")
  - [德川家光](../Page/德川家光.md "wikilink") -
    [比嘉武尊](../Page/比嘉武尊.md "wikilink")→[井出麻衣人](../Page/井出麻衣人.md "wikilink")→[伊藤淳史](../Page/伊藤淳史.md "wikilink")→[館坂優](../Page/館坂優.md "wikilink")→[大澤健](../Page/大澤健.md "wikilink")→[江口洋介](../Page/江口洋介.md "wikilink")
  - [阿勝](../Page/英勝院.md "wikilink") - [東照美](../Page/東照美.md "wikilink")
  - [阿茶局](../Page/阿茶局.md "wikilink") -
    [和田幾子](../Page/和田幾子.md "wikilink")
  - [阿萬之方](../Page/養珠院.md "wikilink") -
    [佐藤真浪](../Page/佐藤真浪.md "wikilink")
  - [阿龜之方](../Page/相應院.md "wikilink") –
    [朝比奈順子](../Page/朝比奈順子.md "wikilink")
  - [阿江與](../Page/崇源院.md "wikilink") -
    [坂上香織](../Page/坂上香織.md "wikilink")→[長山藍子](../Page/長山藍子.md "wikilink")
  - [阿靜](../Page/淨光院.md "wikilink") – [松本友里](../Page/松本友里.md "wikilink")
  - [千姬](../Page/德川千姬.md "wikilink") -
    [荒船麻子](../Page/荒船麻子.md "wikilink")→[小島聖](../Page/小島聖.md "wikilink")→[野村真美](../Page/野村真美.md "wikilink")
  - [阿樂](../Page/寶樹院.md "wikilink") -
    [若村麻由美](../Page/若村麻由美.md "wikilink")
  - [鷹司孝子](../Page/鷹司孝子.md "wikilink") -
    [中田喜子](../Page/中田喜子.md "wikilink")
  - [德川忠長](../Page/德川忠長.md "wikilink") -
    [岩下謙人](../Page/岩下謙人.md "wikilink")→[橋本光成](../Page/橋本光成.md "wikilink")→[雨笠利幸](../Page/雨笠利幸.md "wikilink")→[齊藤隆治](../Page/齊藤隆治.md "wikilink")
  - [德川和子](../Page/德川和子.md "wikilink") -
    [龜井亜由里](../Page/龜井亜由里.md "wikilink")→[片山美穗](../Page/片山美穗.md "wikilink")→[內野真理子](../Page/內野真理子.md "wikilink")→[絝梨沙](../Page/絝梨沙.md "wikilink")
  - [保科正之](../Page/保科正之.md "wikilink") -
    [佐佐木一成](../Page/佐佐木一成.md "wikilink")→[藤井司](../Page/藤井司.md "wikilink")
  - [珠姬](../Page/德川珠姬.md "wikilink") -
    [山邊江梨](../Page/山邊江梨.md "wikilink")
  - [勝姬](../Page/德川勝姬.md "wikilink") -
    [真島彰子](../Page/真島彰子.md "wikilink")→[吉川涼子](../Page/吉川涼子.md "wikilink")
  - [初姬](../Page/德川初姬.md "wikilink") -
    [歌代未奈](../Page/歌代未奈.md "wikilink")
  - [德川義直](../Page/德川義直.md "wikilink") -
    [內大輔](../Page/內大輔.md "wikilink")
  - [德川頼宣](../Page/德川頼宣.md "wikilink") -
    [本田太郎](../Page/本田太郎.md "wikilink")
  - [稻葉正成](../Page/稻葉正成.md "wikilink") -
    [黑樹洋](../Page/黑樹洋.md "wikilink")→[山下真司](../Page/山下真司.md "wikilink")
  - [稻葉正勝](../Page/稻葉正勝.md "wikilink") -
    [高橋壹岐](../Page/高橋壹岐.md "wikilink")→[伊崎充則](../Page/伊崎充則.md "wikilink")→[磯崎洋介](../Page/磯崎洋介.md "wikilink")→[唐澤壽明](../Page/唐澤壽明.md "wikilink")
  - [稻葉正利](../Page/稻葉正利.md "wikilink") -
    [池田貴尉](../Page/池田貴尉.md "wikilink")→[福原學](../Page/福原學.md "wikilink")→[中野慎](../Page/中野慎.md "wikilink")→[丹羽貞仁](../Page/丹羽貞仁.md "wikilink")
  - [稻葉一鐵](../Page/稻葉一鐵.md "wikilink") -
    [大坂志郎](../Page/大坂志郎.md "wikilink")
  - [稻葉重通](../Page/稻葉重通.md "wikilink") -
    [織本順吉](../Page/織本順吉.md "wikilink")
  - [稻葉貞通](../Page/稻葉貞通.md "wikilink") -
    [川津佑介](../Page/川津佑介.md "wikilink")
  - [稻葉正定](../Page/稻葉正定.md "wikilink") -
    [鳥居紀彥](../Page/鳥居紀彥.md "wikilink")→[杉山和幸](../Page/杉山和幸.md "wikilink")→[山浦廣幸](../Page/山浦廣幸.md "wikilink")→[小日向範威](../Page/小日向範威.md "wikilink")→[中村獅童
    (2代目)](../Page/中村獅童_\(2代目\).md "wikilink")→[奈佐健臣](../Page/奈佐健臣.md "wikilink")
  - [稻葉正次](../Page/稻葉正次.md "wikilink") -
    [井手大輔](../Page/井手大輔.md "wikilink")→[布勢優一郎](../Page/布勢優一郎.md "wikilink")→[中村彰良](../Page/中村彰良.md "wikilink")→[高津英樹](../Page/高津英樹.md "wikilink")→[青島健介](../Page/青島健介.md "wikilink")
  - [稻葉通重](../Page/稻葉通重.md "wikilink") -
    [有賀大志](../Page/有賀大志.md "wikilink")
  - [稻葉正則](../Page/稻葉正則.md "wikilink") -
    [真田和幸](../Page/真田和幸.md "wikilink")
  - [土井利勝](../Page/土井利勝.md "wikilink") -
    [中條清志](../Page/中條清志.md "wikilink")
  - [土井利隆](../Page/土井利隆.md "wikilink") -
    [阿部秀一](../Page/阿部秀一.md "wikilink")
  - [本多正信](../Page/本多正信.md "wikilink") -
    [早崎文司](../Page/早崎文司.md "wikilink")
  - [本多正純](../Page/本多正純.md "wikilink") -
    [前田吟](../Page/前田吟.md "wikilink")
  - [本多忠勝](../Page/本多忠勝.md "wikilink") -
    [平泉成](../Page/平泉成.md "wikilink")
  - [松平信綱](../Page/松平信綱.md "wikilink") -
    [關根信行](../Page/關根信行.md "wikilink")→[新井信彥](../Page/新井信彥.md "wikilink")→[三波伸一](../Page/三波伸一.md "wikilink")
  - [松平正綱](../Page/松平正綱.md "wikilink") -
    [大林丈史](../Page/大林丈史.md "wikilink")
  - [大久保忠鄰](../Page/大久保忠鄰.md "wikilink") -
    [石田太郎](../Page/石田太郎.md "wikilink")
  - [酒井忠勝](../Page/酒井忠勝.md "wikilink") -
    [睦五朗](../Page/睦五朗.md "wikilink")
  - [酒井忠世](../Page/酒井忠世.md "wikilink") -
    [宗近晴見](../Page/宗近晴見.md "wikilink")
  - [酒井忠次](../Page/酒井忠次.md "wikilink") -
    [加藤治](../Page/加藤治.md "wikilink")
  - [酒井忠朝](../Page/酒井忠朝.md "wikilink") -
    [澤伸好](../Page/澤伸好.md "wikilink")
  - [堀田正盛](../Page/堀田正盛.md "wikilink") -
    [內田崇吉](../Page/內田崇吉.md "wikilink")→[矢野武](../Page/矢野武.md "wikilink")
  - [榊原康政](../Page/榊原康政.md "wikilink") -
    [中村方隆](../Page/中村方隆.md "wikilink")
  - [三浦正次](../Page/三浦正次.md "wikilink") -
    [山崎有右](../Page/山崎有右.md "wikilink")
  - [青山忠俊](../Page/青山忠俊.md "wikilink") -
    [野村信次](../Page/野村信次.md "wikilink")
  - [阿部忠秋](../Page/阿部忠秋.md "wikilink") -
    [谷門進士](../Page/谷門進士.md "wikilink")
  - [阿部重次](../Page/阿部重次.md "wikilink") -
    [川崎啓一](../Page/川崎啓一.md "wikilink")
  - [太田資宗](../Page/太田資宗.md "wikilink") -
    [渡邊和重](../Page/渡邊和重.md "wikilink")
  - [井伊直政](../Page/井伊直政.md "wikilink") -
    [渡邊輝尚](../Page/渡邊輝尚.md "wikilink")
  - [豐臣秀吉](../Page/豐臣秀吉.md "wikilink") -
    [藤岡琢也](../Page/藤岡琢也.md "wikilink")
  - [寧寧](../Page/高台院.md "wikilink") - [香川京子](../Page/香川京子.md "wikilink")
  - \-{[茶茶](../Page/淀殿.md "wikilink")}- -
    [喜多島舞](../Page/喜多島舞.md "wikilink")→[大空真弓](../Page/大空真弓.md "wikilink")
  - [松之丸](../Page/京極龍子.md "wikilink") -
    [吉野佳子](../Page/吉野佳子.md "wikilink")
  - [豐臣鶴松](../Page/豐臣鶴松.md "wikilink") -
    [長谷川宙](../Page/長谷川宙.md "wikilink")
  - [豐臣秀賴](../Page/豐臣秀賴.md "wikilink") -
    [小磯勝彌](../Page/小磯勝彌.md "wikilink")→[渡邊徹](../Page/渡邊徹.md "wikilink")
  - [豐臣秀長](../Page/豐臣秀長.md "wikilink") -
    [益富信孝](../Page/益富信孝.md "wikilink")
  - [小早川秀秋](../Page/小早川秀秋.md "wikilink") -
    [香川照之](../Page/香川照之.md "wikilink")（本作為正式處女作）
  - [石田三成](../Page/石田三成.md "wikilink") -
    [伊武雅刀](../Page/伊武雅刀.md "wikilink")
  - [大野治長](../Page/大野治長.md "wikilink") -
    [大和田獏](../Page/大和田獏.md "wikilink")
  - [大藏卿局](../Page/大藏卿局.md "wikilink") -
    [馬淵晴子](../Page/馬淵晴子.md "wikilink")
  - [片桐且元](../Page/片桐且元.md "wikilink") -
    [津嘉山正種](../Page/津嘉山正種.md "wikilink")
  - [真田幸村](../Page/真田幸村.md "wikilink") -
    [高橋悅史](../Page/高橋悅史.md "wikilink")
  - [後藤又兵衛](../Page/後藤又兵衛.md "wikilink") -
    [橋本功](../Page/橋本功.md "wikilink")
  - [木村重成](../Page/木村重成.md "wikilink") -
    [竹村健](../Page/竹村健.md "wikilink")
  - [塙團右衛門](../Page/塙團右衛門.md "wikilink") -
    [後藤修](../Page/後藤修.md "wikilink")
  - [織田信長](../Page/織田信長.md "wikilink") -
    [藤岡弘](../Page/藤岡弘.md "wikilink")
  - [阿市之方](../Page/織田市.md "wikilink") -
    [高林由起子](../Page/高林由起子.md "wikilink")
  - [織田信忠](../Page/織田信忠.md "wikilink") -
    [草見潤平](../Page/草見潤平.md "wikilink")
  - [織田信雄](../Page/織田信雄.md "wikilink") -
    [菅谷仁志](../Page/菅谷仁志.md "wikilink")
  - [齋藤利三](../Page/齋藤利三.md "wikilink") -
    [江守徹](../Page/江守徹.md "wikilink")
  - [齋藤利康](../Page/齋藤利康.md "wikilink") -
    [杉本哲太](../Page/杉本哲太.md "wikilink")
  - [齋藤利宗](../Page/齋藤利宗.md "wikilink") -
    [高橋良明](../Page/高橋良明.md "wikilink")→[森田順平](../Page/森田順平.md "wikilink")
  - [齋藤三存](../Page/齋藤三存.md "wikilink") -
    [大城誠晃](../Page/大城誠晃.md "wikilink")→[阿久津健太郎](../Page/阿久津健太郎.md "wikilink")→[阪本良介](../Page/阪本良介.md "wikilink")
  - [明智光秀](../Page/明智光秀.md "wikilink") -
    [五木寬](../Page/五木寬.md "wikilink")
  - [明智秀滿](../Page/明智秀滿.md "wikilink") -
    [磯部勉](../Page/磯部勉.md "wikilink")
  - [森蘭丸](../Page/森蘭丸.md "wikilink") - [內池學](../Page/內池學.md "wikilink")
  - [中川清秀](../Page/中川清秀.md "wikilink") -
    [內藤安彥](../Page/內藤安彥.md "wikilink")
  - [池田恆興](../Page/池田恆興.md "wikilink") -
    [前川哲夫](../Page/前川哲夫.md "wikilink")
  - [池田輝政](../Page/池田輝政.md "wikilink") -
    [秋間登](../Page/秋間登.md "wikilink")
  - [有馬晴信](../Page/有馬晴信.md "wikilink") -
    [佐藤英夫](../Page/佐藤英夫.md "wikilink")
  - [伊達政宗](../Page/伊達政宗.md "wikilink") -
    [金田龍之介](../Page/金田龍之介.md "wikilink")
  - [京極高次](../Page/京極高次.md "wikilink") -
    [長谷川明男](../Page/長谷川明男.md "wikilink")
  - [阿初](../Page/常高院.md "wikilink") -
    [宮澤理惠](../Page/宮澤理惠.md "wikilink")→[松原智恵子](../Page/松原智恵子.md "wikilink")
  - [加藤清正](../Page/加藤清正.md "wikilink") -
    [中康治](../Page/中康治.md "wikilink")
  - [加藤忠廣](../Page/加藤忠廣.md "wikilink") -
    [深水三章](../Page/深水三章.md "wikilink")
  - [鳥居元忠](../Page/鳥居元忠.md "wikilink") -
    [瀧田裕介](../Page/瀧田裕介.md "wikilink")
  - [鳥居忠房](../Page/鳥居忠房.md "wikilink") -
    [安達義也](../Page/安達義也.md "wikilink")
  - [淺野幸長](../Page/淺野幸長.md "wikilink") -
    [河原佐武](../Page/河原佐武.md "wikilink")
  - [福島正則](../Page/福島正則.md "wikilink") -
    [矢島健一](../Page/矢島健一.md "wikilink")
  - [增田長盛](../Page/增田長盛.md "wikilink") -
    [樋浦勉](../Page/樋浦勉.md "wikilink")
  - [高山右近](../Page/高山右近.md "wikilink") -
    [三浦賢二](../Page/三浦賢二.md "wikilink")
  - [水野忠重](../Page/水野忠重.md "wikilink") -
    [小池雄介](../Page/小池雄介.md "wikilink")
  - [安藤重信](../Page/安藤重信.md "wikilink") -
    [木曾秋一](../Page/木曾秋一.md "wikilink")
  - [細川藤孝](../Page/細川藤孝.md "wikilink") -
    [小金井宣夫](../Page/小金井宣夫.md "wikilink")
  - [細川忠興](../Page/細川忠興.md "wikilink") -
    [信達谷圭](../Page/信達谷圭.md "wikilink")
  - [蜂須賀小六](../Page/蜂須賀正勝.md "wikilink") -
    [久遠利三](../Page/久遠利三.md "wikilink")
  - [蜂須賀家政](../Page/蜂須賀家政.md "wikilink") -
    [志賀圭二郎](../Page/志賀圭二郎.md "wikilink")
  - [藤堂高虎](../Page/藤堂高虎.md "wikilink") -
    [卜字隆夫](../Page/卜字隆夫.md "wikilink")
  - [朽木稙綱](../Page/朽木稙綱.md "wikilink") -
    [前川克紀](../Page/前川克紀.md "wikilink")
  - [前田利家](../Page/前田利家.md "wikilink") -
    [嶺田則夫](../Page/嶺田則夫.md "wikilink")
  - [黑田長政](../Page/黑田長政.md "wikilink") -
    [中村修](../Page/中村修.md "wikilink")
  - [山內一豐](../Page/山內一豐.md "wikilink") -
    [相原巨典](../Page/相原巨典.md "wikilink")
  - [茶屋四郎次郎](../Page/茶屋四郎次郎.md "wikilink") -
    [佐佐木敏](../Page/佐佐木敏.md "wikilink")
  - [穴山梅雪](../Page/穴山梅雪.md "wikilink") -
    [今西正男](../Page/今西正男.md "wikilink")
  - [海北友松](../Page/海北友松.md "wikilink") -
    [吉幾三](../Page/吉幾三.md "wikilink")
  - [三條西實條](../Page/三條西實條.md "wikilink") -
    [淺尾和憲](../Page/淺尾和憲.md "wikilink")→[橋爪淳](../Page/橋爪淳.md "wikilink")
  - [三條西公國](../Page/三條西公國.md "wikilink") -
    [佐藤B作](../Page/佐藤B作.md "wikilink")
  - 七澤作兵衛 - [伊東四朗](../Page/伊東四朗.md "wikilink")
  - 東陽坊長盛 - [勝石松](../Page/勝石松.md "wikilink")
  - 志乃 - [石野真子](../Page/石野真子.md "wikilink")
  - 開田孫六 - [千田光雄](../Page/千田光雄.md "wikilink")
  - 阿糸 – [夏樹陽子](../Page/夏樹陽子.md "wikilink")
  - [饗庭局](../Page/饗庭局.md "wikilink") –
    [赤座美代子](../Page/赤座美代子.md "wikilink")
  - 阿國 – [淺利香津代](../Page/淺利香津代.md "wikilink")
  - [信松尼](../Page/武田松姬.md "wikilink") –
    [原知佐子](../Page/原知佐子.md "wikilink")
  - [正榮尼](../Page/正榮尼.md "wikilink") –
    [日向明子](../Page/日向明子.md "wikilink")
  - [祖心尼](../Page/祖心尼.md "wikilink") –
    [上月佐知子](../Page/上月佐知子.md "wikilink")
  - 兼 – [五大路子](../Page/五大路子.md "wikilink")
  - 紫 – [若村麻由美](../Page/若村麻由美.md "wikilink")

[Category:1989年日本電視劇集](../Category/1989年日本電視劇集.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:日本小說改編電視劇](../Category/日本小說改編電視劇.md "wikilink")
[Category:橋田壽賀子劇本作品](../Category/橋田壽賀子劇本作品.md "wikilink")
[Category:安土桃山時代背景電視劇](../Category/安土桃山時代背景電視劇.md "wikilink")
[Category:大奧題材電視劇](../Category/大奧題材電視劇.md "wikilink")