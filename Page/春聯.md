[Rotes_Duilian_aus_Baishuitai.jpg](https://zh.wikipedia.org/wiki/File:Rotes_Duilian_aus_Baishuitai.jpg "fig:Rotes_Duilian_aus_Baishuitai.jpg")
[Printedchuntie.jpg](https://zh.wikipedia.org/wiki/File:Printedchuntie.jpg "fig:Printedchuntie.jpg")春條、橫批\]\]
**春聯**也作**福貼**，-{zh-cn:[粤语地区称](../Page/粤语.md "wikilink")**挥春**;zh-hk:[國語地區稱](../Page/國語.md "wikilink")**春聯**;zh-tw:[粵語地區稱](../Page/粵語.md "wikilink")**揮春**;}-，[閩南語稱](../Page/閩南語.md "wikilink")**春仔**，是一種在[新春和](../Page/新春.md "wikilink")[立春時使用的傳統裝飾物](../Page/立春.md "wikilink")，把[賀年的吉利字詞用漂亮的](../Page/賀年.md "wikilink")[書法](../Page/書法.md "wikilink")，寫在[紙上而成](../Page/紙.md "wikilink")，一般是貼在[牆](../Page/牆.md "wikilink")、[門之上](../Page/門.md "wikilink")，在[華人社會](../Page/華人.md "wikilink")、[朝鮮半島和](../Page/朝鮮半島.md "wikilink")[越南](../Page/越南.md "wikilink")，每逢[過年人們都會貼春聯](../Page/東亞新年.md "wikilink")，以增加[節日的喜慶氣氛](../Page/節日.md "wikilink")。其中寫成[對聯的又叫](../Page/對聯.md "wikilink")**春聯**，但有些地區「春聯」也是各種春聯的統稱\[1\]，而有些地區又把條狀、不成對的春聯稱為**春條**。[日本把新年貼的稱為](../Page/日本.md "wikilink")**正月札**，立春聯的稱為**立春札**或**立春大吉札**。[朝鮮則有](../Page/朝鮮半島.md "wikilink")**立春榜**（）、**立春書**（）、**立春貼**（）等[名稱](../Page/名稱.md "wikilink")，簡稱**春榜**、**春貼**、**春帖**等。

傳統春聯是由人手以[毛筆書寫](../Page/毛筆.md "wikilink")，但現在亦有[機器](../Page/機器.md "wikilink")[印製的春聯](../Page/印刷.md "wikilink")。春聯的種類較多，有街門對、屋門對等，春聯多有[橫批](../Page/橫批.md "wikilink")。

## 各地的春聯

### 華人世界

春聯源於中國的宋朝，其起源可以分為三方面：宜春帖、桃符、延祥詩。

**宜春帖**，是春聯的起源之一，又名「**春書**」，在[南北朝時已經出現](../Page/南北朝.md "wikilink")，是人們祈求招祥納福的物品，是於立春日用色紙剪成「宜春」二字，張貼在[門柱上](../Page/門柱.md "wikilink")，也有一些是寫成單句的其他吉祥話，貼於門楣上。[南朝梁](../Page/南朝梁.md "wikilink")[宗懔所著的](../Page/宗懔.md "wikilink")《[荊楚歲時記](../Page/荊楚歲時記.md "wikilink")》載，當時[立春日](../Page/立春.md "wikilink")[湘](../Page/湘.md "wikilink")、[鄂兩地人們除了用彩色的](../Page/鄂.md "wikilink")[绸布剪成](../Page/绸.md "wikilink")[燕子的形状戴在头上外](../Page/燕子.md "wikilink")，还要写“宜春”二个字贴在家中\[2\]，即宜春帖，又稱春帖。[唐朝](../Page/唐朝.md "wikilink")[孙思邈的](../Page/孙思邈.md "wikilink")《[千金玉令](../Page/千金玉令.md "wikilink")》也有提到人們於立春時張貼宜春帖\[3\]。由於立春與春節接近，而宜春帖的形式又與現在的春條、橫批相似，一些[學者認為現在的春條](../Page/學者.md "wikilink")、橫批是直接源自宜春帖，只是[名稱不同](../Page/名稱.md "wikilink")。

[Chinese_New_Year's_poetry.jpg](https://zh.wikipedia.org/wiki/File:Chinese_New_Year's_poetry.jpg "fig:Chinese_New_Year's_poetry.jpg")與褪色的春聯\]\]
**[桃符](../Page/桃符.md "wikilink")**是春聯的另一個起源。桃符是民間於春節時用桃木板畫上[神荼](../Page/神荼.md "wikilink")、[鬱櫑二](../Page/鬱櫑.md "wikilink")[神驅避鬼怪的物品](../Page/神.md "wikilink")，古代傳說[東海](../Page/東海.md "wikilink")[度朔山有大](../Page/度朔山.md "wikilink")[桃樹](../Page/桃樹.md "wikilink")，桃樹下有主管萬[鬼的神荼和鬱櫑](../Page/鬼.md "wikilink")。遇作祟的鬼就會把它捆起來餵[老虎](../Page/老虎.md "wikilink")。\[4\]。後來人們在桃符上寫上一些吉祥語句。在唐朝時，[楹聯開始出現](../Page/楹聯.md "wikilink")。《[敦煌遺書](../Page/敦煌遺書.md "wikilink")》就記載了一些楹聯，帶有吉祥寓意，部份學者，認為這是最早的楹聯，如[敦煌研究院研究員](../Page/敦煌研究院.md "wikilink")[譚蟬雪於](../Page/譚蟬雪.md "wikilink")《[文史知識](../Page/文史知識.md "wikilink")》1991年4月號一篇題為《我國最早的楹聯》的文章裡提出這種說法，但直至現在仍未有實質證據證明這些對仗的句子是楹聯或春聯\[5\]。到了[五代的時候](../Page/五代.md "wikilink")，蜀主[孟昶親自在桃符上題了](../Page/孟昶.md "wikilink")“新年納餘慶，佳節號長春”一聯，一般人都把它看做是最早的春聯。

一說宋朝桃板為薄木版，長二三尺，寬四五寸，掛在門上，可畫年畫。桃符為長七八寸細木條，書祈福禳災之辭，歲旦插於門左右地而釘之。因此春聯應為桃板演變而來。\[6\]

**[延祥詩](../Page/延祥詩.md "wikilink")**也是春聯來源之一，又稱[春帖子](../Page/春帖子.md "wikilink")、[春帖子词](../Page/春帖子词.md "wikilink")、[帖子詞](../Page/帖子詞.md "wikilink")，是古代朝廷在立春時要[翰林文士写帖子词进献](../Page/翰林.md "wikilink")。延祥詩內容以描寫景物、歌頌昇平或寓意規諫為主，文字工麗，多為五言、七言[絕句](../Page/絕句.md "wikilink")，[周輝的](../Page/周輝.md "wikilink")《清波雜誌》就提及歐陽修、蘇軾的春帖子以規諫為主，[司马光則作出楷式](../Page/司马光.md "wikilink")\[7\]
。這些詩詞會張貼於宮門以及宫苑各殿堂楼阁上，[宋朝時很盛行](../Page/宋朝.md "wikilink")\[8\]。一些成對的聯句到後來也就變成春聯的一種了\[9\]。
[FuDao.jpg](https://zh.wikipedia.org/wiki/File:FuDao.jpg "fig:FuDao.jpg")斗方\]\]
後來的春聯就是民間桃符、宜春帖與宮廷帖子词互相影响的產物，趨吉的宜春帖與避凶的桃符混合為一物，帖子词简化成了桃符的两句话，桃符简化成了帖子词的纸张书写粘贴。宋代造紙術和印刷術較發達，于是在[宋代纸春联的载体就产生了](../Page/宋代.md "wikilink")，並成為一種風俗。[王安石詩中就寫過](../Page/王安石.md "wikilink")：“千門萬戶瞳瞳日，總把新桃換舊符。”。北宋《歲時雜記》載，當時人們把吉祥語句刻在桃木板上\[10\]。[南宋](../Page/南宋.md "wikilink")《[夢梁錄](../Page/夢梁錄.md "wikilink")》記載當時出現一種「**迎春牌兒**」，又說除夕要「貼春牌」\[11\]，這可能是[紙製的桃符](../Page/紙.md "wikilink")。有些人認為**春牌**就是[福字春聯](../Page/福字.md "wikilink")\[12\]。宋代春季常有[瘟疫](../Page/瘟疫.md "wikilink")，為防瘟疫，春聯[橫批](../Page/橫批.md "wikilink")（「天行帖子」）都是「順天行化」。\[13\]

用紅紙書寫桃符始于明朝，亦出現了「春帖」一詞，貼春聯的習俗也在明朝開始盛行，據明朝[陳雲瞻的](../Page/陳雲瞻.md "wikilink")《[簪雲樓雜話](../Page/簪雲樓雜話.md "wikilink")》記載，春聯是因為[明太祖朱元璋提倡而普及的](../Page/明太祖.md "wikilink")\[14\]，此外還有另外一些春聯的傳說是關於朱元璋的，傳說朱元璋在明初一個除夕在當時的[首都](../Page/首都.md "wikilink")[金陵](../Page/金陵.md "wikilink")（今[南京](../Page/南京.md "wikilink")）微服出行，發現其中一戶人家沒貼春聯，問到戶主，得知他們因為沒人會寫字又無人代寫春聯，於是朱元璋就根據戶主當[屠戶的](../Page/屠戶.md "wikilink")[職業寫下](../Page/職業.md "wikilink")「雙手劈開生死路，一刀割斷是非根」的春聯。福字倒貼來由的其中一個說法也跟朱元璋有關，朱元璋有一次用「福」字作為記號殺人，他的[皇后](../Page/皇后.md "wikilink")[馬氏知道後就下令全城人家必須在天明之前在自家門上貼上](../Page/馬皇后_\(明太祖\).md "wikilink")「福」字，其中一戶不識字的人家卻把福字貼倒了。第二天朱元璋發現家家都貼了「福」字，也知道那家人把福字貼倒了，就命令御林軍把那家滿門抄斬。馬皇后看事情不好就忙對朱元璋說，那家人是知道他來訪，故意把福字貼倒了，表示「福到」的意思，皇帝一聽有道理，便下令放人。從此人們一求吉利，二為紀念馬皇后，便將福字倒貼起來了\[15\]。

[Shengyixinglong.jpg](https://zh.wikipedia.org/wiki/File:Shengyixinglong.jpg "fig:Shengyixinglong.jpg")
到清朝時，貼春聯的習俗已經很普及。當時的《[燕京歲時記](../Page/燕京歲時記.md "wikilink")》就描述當時北京城自臘月起就有很多文人墨客寫春聯潤筆，春聯的顏色在當時有等級區分，民間的春聯用朱箋或紅紙，內廷或[宗室則用緣以紅邊](../Page/宗室.md "wikilink")、藍邊的白紙。\[16\]此外還有其他的禁忌和限制，如未服滿三年喪期的不能用紅紙，死者為男性用[青色](../Page/青色.md "wikilink")，死者為女性用[黃色](../Page/黃色.md "wikilink")，內容也改成哀傷之詞\[17\]，也有男用藍色、女用黃色\[18\]。有些地區是不論男女皆用藍色\[19\]。亦有些地區的習俗是喪家第一年用[白色](../Page/白色.md "wikilink")，第二年用黑色，第三年用藍色\[20\]。[梁章鉅編寫了春聯專著](../Page/梁章鉅.md "wikilink")《[檻聯叢話](../Page/檻聯叢話.md "wikilink")》，顯示在當時已成為一種[文學](../Page/文學.md "wikilink")[藝術形式](../Page/藝術.md "wikilink")。

春聯的內容除了有意義的吉祥語句外，一些[文盲也會用紅紙寫上](../Page/文盲.md "wikilink")「十」字作為春聯之用。據1940年代的[陕西](../Page/陕西.md "wikilink")《宜川縣誌》载，人們會寫吉祥語句，不識字的[鄉民則寫](../Page/鄉民.md "wikilink")「十」字\[21\]。

在工業化之後，市面上也有一些機械印製的春聯出售，亦有些不用[紙張](../Page/紙張.md "wikilink")，而用其他物料如[布](../Page/布.md "wikilink")、[塑膠製造](../Page/塑膠.md "wikilink")，可使用多次。亦有一些以紙板製式的立體春聯，部份還有一些卡通圖案，受年輕人歡迎。而傳統式的手寫書法春聯仍然受人喜愛。近年受[西方文化影響](../Page/西方.md "wikilink")，也出現一些從左至右的橫批。

### 日本

日本的[新年和立春都有貼吉祥語句的習俗](../Page/日本新年.md "wikilink")，其中立春聯的稱為**立春札**，又因為經常寫上「立春大吉」四字，所以又稱**立春大吉札**。立春札多由[佛寺](../Page/佛寺.md "wikilink")、[神社發出](../Page/神社.md "wikilink")，以白紙製作，常見的語句為「立春大吉，鎮防火燭」\[22\]。也有些人會自己書寫。

新年貼的稱為**正月札**，顏色並沒有一定限制，常見的語句有「開運招福」、「大入」、「交通安全」、「家内安全」（）、商賣繁昌（）、「千客萬來」（）等\[23\]。

### 朝鮮

[Korean.Folk.Village-Minsokchon-17b.jpg](https://zh.wikipedia.org/wiki/File:Korean.Folk.Village-Minsokchon-17b.jpg "fig:Korean.Folk.Village-Minsokchon-17b.jpg")
貼春聯的習俗也見於[朝鮮半島](../Page/朝鮮半島.md "wikilink")，稱為**立春榜**、**立春書**、**立春帖**、**立春祝**等，[濟州島叫](../Page/濟州島.md "wikilink")（，其中「」為[固有詞](../Page/固有詞.md "wikilink")，[祭祀之意](../Page/祭祀.md "wikilink")）。是春聯的吉祥文句，貼在大門或大[梁](../Page/梁.md "wikilink")、[天井](../Page/天井.md "wikilink")，表達新年的祝福。雖然[朝鮮半島在](../Page/朝鮮半島.md "wikilink")[日常生活已普遍使用](../Page/日常生活.md "wikilink")[諺文多年](../Page/諺文.md "wikilink")，但立春榜仍然用漢字書寫。

與中國的春聯相同，朝鮮立春聯的其中一個源由是延祥詩。[金邁淳](../Page/金邁淳.md "wikilink")《[洌陽歲時記](../Page/洌陽歲時記.md "wikilink")》（）載，[朝鮮王朝時期](../Page/朝鮮王朝.md "wikilink")，[承政院](../Page/承政院.md "wikilink")（）的正三品以下[官员和](../Page/官员.md "wikilink")[侍从每人在立春几天前要给](../Page/侍从.md "wikilink")[國王呈五言绝句一首](../Page/國王.md "wikilink")\[24\]，然後再擇好句貼於宮內柱子上，就是立春帖\[25\]。初期只有[兩班](../Page/兩班.md "wikilink")[貴族府邸和宮廷才會貼立春帖](../Page/貴族.md "wikilink")，後來才流傳到民間。因此朝鮮有句[俗語](../Page/俗語.md "wikilink")「店舖門前的立春帖」，[引申為](../Page/引申.md "wikilink")「格格不入」的意思\[26\]。

至於成對的春聯（），[朝鮮王朝末年實學家](../Page/朝鮮王朝.md "wikilink")[柳得恭所著](../Page/柳得恭.md "wikilink")《[京都杂志](../Page/京都杂志.md "wikilink")》記載，在首都[汉阳地区](../Page/汉阳.md "wikilink")（今[首尔](../Page/首尔.md "wikilink")），人们有在[立春这一天贴对联的习俗](../Page/立春.md "wikilink")，如「父母千年壽，子孫萬代榮」、“国有风云庆，家无桂玉愁”、“灾从春雪消，福逐夏云兴”、“扫地黄金出，开门万福来”、“天上三阳近，人间五福来”、“门迎春夏秋冬福，户纳东西南北财”等\[27\]，多貼於楹柱、門柱上。又有成對的**對句**（）如“寿如山，富如海”、「歲大通，壽無疆」、“去千灾，来百福”、“立春大吉，建阳多庆”、“国泰民安，家给人足”等，貼於門上或樑上，常會直貼或[對稱斜貼呈尖角形](../Page/對稱.md "wikilink")。對句與春聯的差異在於對句不一定合乎[平仄](../Page/平仄.md "wikilink")，並且一般為三至五字，較常見的五至七字春聯短。

[Korea-Seoul-Namsangol-02a.jpg](https://zh.wikipedia.org/wiki/File:Korea-Seoul-Namsangol-02a.jpg "fig:Korea-Seoul-Namsangol-02a.jpg")
此外，也有單句張貼的**短句**（）\[28\]，如「春到門前增富貴」、「春光先到古人家」、「一家和氣滿門楯」、「人情富貴如將得」、「玉洞桃花萬樹春」等\[29\]。也有一些是三至四字的單句如「雨順風調」、「開門萬福」，一些常用對句的立春帖亦可以單句張貼。還有單字的斗方如「龍」、「虎」等。

### 越南

[Tết_Hà_Nội_2009.jpg](https://zh.wikipedia.org/wiki/File:Tết_Hà_Nội_2009.jpg "fig:Tết_Hà_Nội_2009.jpg")
越南人也有春節時貼春聯的習俗，傳統的春聯是用墨把[漢字](../Page/漢字.md "wikilink")（[儒字](../Page/儒字.md "wikilink")）寫在紅紙上的，形式與中國的春聯相若，有門聯、門心、斗方、春條等。另有一種字畫形式的春聯，以單字如「忍」\[30\]、「達」、「心」為主，並且有[落款](../Page/落款.md "wikilink")，有寫在紅紙上的，也有寫在白紙上的。[越南國語字普及後](../Page/越南國語字.md "wikilink")，有些春聯會用[毛筆以漢字書法的](../Page/毛筆.md "wikilink")[筆法](../Page/筆法.md "wikilink")，用國語字寫成，甚至有些是寫上[固有詞如](../Page/固有詞.md "wikilink")「」（表示「新年快樂」的[越南語固有詞](../Page/越南語.md "wikilink")）但仍然有不少越南人喜愛漢字寫的春聯。在歲晚期間，常有一些會寫漢字書法的人在街頭擺[攤檔寫春聯](../Page/攤檔.md "wikilink")，由於現在越南不少年輕人都不懂寫漢字，在街頭賣手寫漢字春聯的大多是[老人家](../Page/老人家.md "wikilink")。

除了傳統手寫春聯外，近年市面上也有一些機印春聯出售，在大城市如[胡志明市可以看到許多售賣印刷春聯的](../Page/胡志明市.md "wikilink")[店舖](../Page/店舖.md "wikilink")。有印上漢字的，也有印上越南文字的。亦有些其他物料製造、可多次使用的新式春聯。

## 常見種類及樣式

[Vietchu.jpg](https://zh.wikipedia.org/wiki/File:Vietchu.jpg "fig:Vietchu.jpg")
春聯可以分為以下種類\[31\]：

  - [斗方](../Page/斗方.md "wikilink")：也叫「斗斤」，用[正方形紙斜放豎立](../Page/正方形.md "wikilink")，尖角向四邊放，書寫字樣。可寫單字如「春」、「[福](../Page/福字.md "wikilink")」、「滿」等，其中「福」字常會倒貼，[山西](../Page/山西.md "wikilink")、[陝西一帶又有倒貼](../Page/陝西.md "wikilink")「有」字斗方的習俗。寫二字的則是直寫，如「大吉」、「常滿」。也有四字的如「花開富貴」。多貼在內門、米缸或花盆上。古代還有一個60多筆的「一」字，表示對新一年萬象更新的期盼\[32\]。
      - [合字斗方](../Page/合字.md "wikilink")：有些是把幾個字的語句寫成[合字斗方春聯](../Page/合字.md "wikilink")，在[宋朝已經出現](../Page/宋朝.md "wikilink")，字形配合字義有「吉祥」的寓意。例如「黃金萬兩」採取縱向組合中的[筆畫搭借](../Page/筆畫.md "wikilink")，猶如[財富積累](../Page/財富.md "wikilink")；「日進斗金」和「招財進寶」的「斗」字十字格和「進」字[辵之底的運用](../Page/辵部.md "wikilink")，形似[車載](../Page/車.md "wikilink")，財源滾滾\[33\]。

[Chinese_Character_zhao1_cai2_jin4_bao3.svg](https://zh.wikipedia.org/wiki/File:Chinese_Character_zhao1_cai2_jin4_bao3.svg "fig:Chinese_Character_zhao1_cai2_jin4_bao3.svg")\]\]

  - **門心**（門葉）：貼於大門兩扉的叫做「門心」或「門葉」，等於古代的「神荼」與「鬱壘」，如「迎春」「接福」、「大吉」「大利」、「百福」「千祥」、「吉祥」「如意」等。
  - **春條**（揮春）：是單條書寫的吉語，如「恭賀新禧」、「抬頭見喜」、「龍馬精神」、「吉慶有餘」等，多貼於房門、牆壁上，呈長條狀。
  - **框對**：貼於大門左右柱或壁上的叫做「框對」，分為上聯和下聯，即一般俗稱的「春聯」，格律規則與一般[對聯相同](../Page/對聯.md "wikilink")，張貼時上聯在右，下聯在左。
  - **橫彩**（彩條）：一種條狀的長方形春聯，上面印有吉祥字句（如「富貴吉祥」、「五福臨門」、「招財進寶」等）及財神、童子、八仙的圖案，稱作的名稱有橫彩、彩條或彩帶，為門錢（門簽）的一種，常張貼於橫批門楣下方來增加節慶氣氛及趨吉辟邪的功能。
  - [橫批](../Page/橫批.md "wikilink")：也作「[橫披](../Page/橫披.md "wikilink")」，橫貼於門楣的橫木或[壁上的叫做](../Page/壁.md "wikilink")「橫批」，如「鴻禧」、「出入平安」、「吉星高照」。
  - [福符](../Page/五福符.md "wikilink")：也作門錢、吊錢、掛錢，貼於門楣的橫木或[壁上](../Page/壁.md "wikilink")，也有貼在窗戶上。
  - [字畫](../Page/字畫.md "wikilink")：常見於越南，寫大字於紅紙或黃紙、白紙上，常見的有「忍」、「達」、「心」、「福」、「壽」、「吉祥」等。

以上屬於較為常見的傳統類型，現代的春聯也有其他形式的。

### 樣式

[Schreiben_eines_Spruchbandes.jpg](https://zh.wikipedia.org/wiki/File:Schreiben_eines_Spruchbandes.jpg "fig:Schreiben_eines_Spruchbandes.jpg")
春聯最初出現時是寫在白紙上，這種[風俗傳到](../Page/風俗.md "wikilink")[朝鮮半島並保留下來](../Page/朝鮮半島.md "wikilink")，至今[朝鮮族](../Page/朝鮮族.md "wikilink")（包括[韓國](../Page/韓國.md "wikilink")、[北朝鮮](../Page/北朝鮮.md "wikilink")）的春聯仍然於白紙上，並仍然使用[漢字寫春聯](../Page/漢字.md "wikilink")。[中國自](../Page/中國.md "wikilink")[明朝起用](../Page/明朝.md "wikilink")[紅色的紙寫春聯](../Page/紅色.md "wikilink")，後來以紅紙寫春聯的習俗也傳到[越南](../Page/越南.md "wikilink")，但近年[大中華地區也流行以金色紙作春聯](../Page/大中華地區.md "wikilink")。越南傳統上也用[漢字寫春聯](../Page/漢字.md "wikilink")，[國語字出現後](../Page/國語字.md "wikilink")，有些人也改用國語字寫或印製春聯。

在歲末至春節期間，街頭常有[路邊攤出售即席揮毫的春聯](../Page/路邊攤.md "wikilink")。一些人認為家中貼上[運氣好的人寫的春聯會令一家行好運](../Page/運氣.md "wikilink")。

近幾十年在[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[越南又出現機印春聯](../Page/越南.md "wikilink")，常會於[文具店出售](../Page/文具店.md "wikilink")，有紅底黑字的，也有紅底燙金字的，亦有其他顏色、字形的。也有一些專門生產、售賣春聯的公司。

不少大[企業也有印上](../Page/企業.md "wikilink")[公司名字及](../Page/公司.md "wikilink")[商標的春聯](../Page/商標.md "wikilink")，派送給客戶。若果顧客喜歡在家中貼此類春聯，可以收到[廣告](../Page/廣告.md "wikilink")、[宣傳效果](../Page/宣傳.md "wikilink")，香港一些[區議員](../Page/區議員.md "wikilink")、[立法會](../Page/立法會.md "wikilink")[議員](../Page/議員.md "wikilink")，[台灣一些](../Page/台灣.md "wikilink")[民意代表](../Page/民意代表.md "wikilink")、[地方政府](../Page/地方政府.md "wikilink")，以及一些[廟宇](../Page/廟宇.md "wikilink")，也會印製春聯派發給民眾來提高知名度和達到宣傳目的。由於，此類揮春的[廣告](../Page/廣告.md "wikilink")[宣傳效果強](../Page/宣傳.md "wikilink")，所以有極多企業通過不同的途徑，派送給客戶，例如[銀行分行](../Page/銀行.md "wikilink")、隨[報紙附送](../Page/報紙.md "wikilink")、[鐵路出口等](../Page/鐵路.md "wikilink")。也有一些[公司專門承接企業或個人訂製印有公司名或人名的宣傳性質春聯](../Page/公司.md "wikilink")。

## 常見的春聯語句

[Korea-Andong-Hahoe_Folk_Village-16.jpg](https://zh.wikipedia.org/wiki/File:Korea-Andong-Hahoe_Folk_Village-16.jpg "fig:Korea-Andong-Hahoe_Folk_Village-16.jpg")
春聯的語句以吉祥意義的字詞為主，寄寓了人們希冀美好[生活的](../Page/生活.md "wikilink")[情感](../Page/情感.md "wikilink")，與人們的生活習慣往往息息相關，透射出對生活的期盼。由於事事順心、生活[平安](../Page/平安.md "wikilink")、[家庭和睦](../Page/家庭.md "wikilink")、[國家繁榮](../Page/國家.md "wikilink")、[健康長壽](../Page/健康.md "wikilink")、[富貴豐足等是從古到今不少人的願望](../Page/富貴.md "wikilink")，因此「心想事成」、「從心所欲」、「百無禁忌」、「萬事勝意」、「如意吉祥」、「事事亨通」、「福壽無疆」、「四季平安」、「平安大吉」、「長命富貴」、「一團和氣」、「家給人足」、「年年有餘」、「豐衣足食」、「財源廣進」、「招財進寶」、「國泰民安」、「福壽康寧」、「身壯力健」、「龍馬精神」、「福」、「壽」等都是歷久不衰的語句。也有一些春聯是寄寓了人們對自身的志向以及[道德修養方面的](../Page/道德.md "wikilink")[目標](../Page/目標.md "wikilink")，如「忍」、「達」等。

有些春聯純粹是表現出[新年的喜慶和對人的](../Page/新年.md "wikilink")[祝福](../Page/祝福.md "wikilink")，如「迎春接福」、「恭賀新禧」、「恭喜發財」、「立春大吉」、「大吉大利」、「週年旺相」、「一元復始」、「萬象更新」、「大吉」、「春」、「喜」、「炮竹一聲除舊，桃符萬象更新」、「天增歲月人增壽，春滿乾坤福滿門」等。

有的春聯會搭配該年的[生肖](../Page/生肖.md "wikilink")（動物），以該動物或同音、諧音字創作吉句，如「金猴獻瑞」、「金雞報喜」等，又如「猴（活）力充沛
」、「雞（吉）祥如意」、「十犬（全）十美」等，並搭配可愛、喜氣的卡通動物圖案，更添趣味。

傳統的[房屋還會規定不同形狀的春聯要貼於不同的位置](../Page/房屋.md "wikilink")，如中國、越南春聯的對聯貼於[楹柱](../Page/楹柱.md "wikilink")、門柱，橫批貼於門楣，朝鮮立春帖的對句貼於門楣或門扉呈尖頂形等。但現代[建築的](../Page/建築.md "wikilink")[設計不同](../Page/室內設計.md "wikilink")，也就沒有硬性規定如何張貼春聯了，所以現在也常有人在屋內的[牆壁上貼對聯](../Page/牆壁.md "wikilink")、橫批、對句等。

此外，在不同的[位置也有專門配合該位置的春聯字句](../Page/位置.md "wikilink")，如「花開富貴」貼於[年花的](../Page/年花.md "wikilink")[花盆](../Page/花盆.md "wikilink")、[花瓶上](../Page/花瓶.md "wikilink")，「大吉」斗方貼於[年桔盆上](../Page/年桔.md "wikilink")，「常滿」、「滿」斗方貼於米缸上面，「出入平安」、「開門見喜」、「開門萬福」貼於門口，「滿院春光」、「滿院生輝」貼於[院子等](../Page/院子.md "wikilink")，如果把字句貼到不適當的位置往往會鬧出笑話。清朝人[李光庭的](../Page/李光庭.md "wikilink")《[乡言解颐](../Page/乡言解颐.md "wikilink")》就記載了一個不識字的人把「人口平安」貼在[豬圈口](../Page/豬圈.md "wikilink")，把「肥豬滿圈」貼在[臥室內](../Page/臥室.md "wikilink")\[34\]。

[Yu_xia_da_xin.JPG](https://zh.wikipedia.org/wiki/File:Yu_xia_da_xin.JPG "fig:Yu_xia_da_xin.JPG")[洪聖廟內的](../Page/洪聖廟.md "wikilink")「魚蝦大信」橫批\]\]
不同[身份](../Page/身份.md "wikilink")、[職業](../Page/職業.md "wikilink")、[喜好的人也會貼上不同的春聯](../Page/喜好.md "wikilink")。如做[生意的人會貼上](../Page/生意.md "wikilink")「生意興隆」、「大展鴻圖」，[投資者會貼](../Page/投資者.md "wikilink")「一本萬利」，開[店售賣貨品或從事](../Page/店.md "wikilink")[銷售](../Page/銷售.md "wikilink")、[貿易工作的人會貼上](../Page/貿易.md "wikilink")「貨如輪轉」、「客似雲來」，[農民會貼](../Page/農民.md "wikilink")「五穀豐收」、「風調雨順」，[漁民會貼](../Page/漁民.md "wikilink")「魚蝦大信」、「一帆風順」，[畜牧業者會貼](../Page/畜牧業.md "wikilink")「六畜興旺」，受薪者會貼「步步高陞」，[學生會貼](../Page/學生.md "wikilink")「學業進步」，家有[小孩的會貼](../Page/小孩.md "wikilink")「快高長大」，[駕駛者會貼](../Page/司機.md "wikilink")「路路亨通」，[賭徒會貼](../Page/賭徒.md "wikilink")「橫財就手」等。

春聯語句會隨[時代](../Page/時代.md "wikilink")、[地域而有所差異](../Page/地域.md "wikilink")，也反映人們的生活。從前[糧食較少](../Page/糧食.md "wikilink")，人們以吃飽飯為願望，就常常會在米缸上貼上「滿」或「常滿」的斗方，以祈經常有[米吃](../Page/米.md "wikilink")。現代的[港澳地區](../Page/港澳.md "wikilink")，由於流行[賽馬](../Page/賽馬.md "wikilink")、[賽狗](../Page/賽狗.md "wikilink")、[六合彩等](../Page/六合彩.md "wikilink")[賭博活動](../Page/賭博.md "wikilink")，因此常看到「狗馬亨通」、「六合常中」之類的春條。近年[城市又流行](../Page/城市.md "wikilink")[減肥瘦身](../Page/減肥.md "wikilink")，因此「減肥成功」也是常見的春條。另外，隨著[基督宗教的傳入和普及](../Page/基督宗教.md "wikilink")，一些[教會和](../Page/教會.md "wikilink")[教徒也會書寫或](../Page/基督徒.md "wikilink")[印刷一些內容與基督宗教](../Page/印刷.md "wikilink")[教義相關的](../Page/教義.md "wikilink")[福音春條](../Page/福音.md "wikilink")、春聯，如「天主保佑」、「神愛世人」、「主恩常在」、「榮神益人」、「主賜平安」、「福杯滿溢」\[35\]、「天賜十條誡，門迎八福春」、「送舊歲除舊事，迎新年作新人」、「救恩長存念天主，春光早到耀世人」、「哲人寒冬報佳訊，瑞氣暖春思福音」等\[36\]，派發給教友作祝福、勉勵，也會藉派發福音春聯給非教徒來[傳教](../Page/傳教.md "wikilink")，或作為[籌款](../Page/籌款.md "wikilink")[義賣用途](../Page/義賣.md "wikilink")，也會於[基督教書室出售](../Page/基督教書室.md "wikilink")。

## 参见

  - [門聯](../Page/門聯.md "wikilink")
  - [對聯](../Page/對聯.md "wikilink")
  - [糨糊](../Page/糨糊.md "wikilink")

## 参考文献

### 網站

  - [合体字自宋代起流行于民间　春联春贴春条贴门窗寓吉祥（图）](http://news.163.com/07/0216/10/37EP8E0K000120GU.html)
  - [春帖子](https://web.archive.org/web/20080205182907/http://www.oldbj.com/bjlife/bjfolklore/bjfolk25.htm)
  - [立春大吉…입춘방
    써드려요](https://archive.is/20130426231135/http://kjdaily.com/read.php3?aid=1201532400102956r6)
  - [입춘（立春）- 봄의 시작](http://ujuhim.co.kr/main521-1.htm)
  - [입춘（立春）을 맞으면서](http://www.bulgyofocus.net/news/read.php?idxno=49763)

### 書籍

  - 《韓國的民俗與文化》，[臺灣商務印書館](../Page/臺灣商務印書館.md "wikilink") ISBN 9570521163
  - 《春節文化》，山西古籍出版社 ISBN 7805985227
  - 《春節》，中國社會出版社 ISBN 7508713559
  - 《吉瑞中國節》，內蒙古人民出版社 ISBN 7204070682
  - 《節趣》，學林出版社 ISBN 7806165150
  - 《歲時節日裡的中國》，[中華書局](../Page/中華書局.md "wikilink") ISBN 7101050549
  - 《越南》，社會科學文獻出版社 ISBN 7801903978

{{-}}

[Category:新春習俗](../Category/新春習俗.md "wikilink")
[Category:華人新春習俗](../Category/華人新春習俗.md "wikilink")
[Category:台灣新春習俗](../Category/台灣新春習俗.md "wikilink")
[Category:越南新春習俗](../Category/越南新春習俗.md "wikilink")
[Category:朝鮮新春習俗](../Category/朝鮮新春習俗.md "wikilink")
[Category:東亞書法](../Category/東亞書法.md "wikilink")
[Category:日本新年習俗](../Category/日本新年習俗.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.  [《[台灣大百科全書](../Page/台灣大百科全書.md "wikilink")》：春聯](http://taipedia.cca.gov.tw/Entry/EntryDetail.aspx?EntryId=23048)


2.  宗懔《荊楚歲時記》：「立春之日，悉剪彩为燕以戴之，贴『宜春』二字。」

3.  孫思邈《千金玉令》：「立春日，贴宜春字于门。」

4.  [對聯](http://w3.tkgsh.tn.edu.tw/chinese/%E9%AB%98%E4%B8%AD%E9%83%A8/%E6%87%89%E7%94%A8%E6%96%87%E5%AD%97/%E5%B0%8D%E8%81%AF.htm)

5.  [春聯](http://big5.china.com.cn/zhuanti2005/txt/2003-01/24/content_5266097.htm)

6.  李開周 過一個歡樂的宋朝新年 桃板不是春聯，桃符更不是春聯 p122

7.  周辉《清波杂志》：「翰林书待诏请春词，以立春日剪贴于禁中门帐皇帝阁。……春端帖子，不特咏景物为观美。欧阳文忠公尝寓规风其向，苏东坡亦然。司马温公自著日录，特书此四诗，为玉堂之楷式。自政宣以后，第形容太平盛事，语言工丽，以相夸。殆若唐人宫词耳。」

8.  [立春风俗](http://www.nongli.com/Doc/0501/27161931.htm)

9.  [從桃符到春聯](http://www.tw001.net/novels/zatan/uyhywxywdjn/mwh09.html)

10. 《歲時雜記》：「桃符之制，以薄木板長二三尺、大四五寸為之……或寫春詞或書祝禱之語，歲旦則更之。」

11. 《夢梁錄》：「歲旦在邇，席鋪百貨，畫門神桃符，迎春牌兒……士庶家不論大小，俱灑掃門閭，去塵穢，淨庭戶，換門神，掛鐘旭，釘桃符，貼春牌，祭把祖宗。」

12. [倒貼福字](http://www.cctv.com/special/shunian/20080122/101142.shtml)

13.

14. 陳雲瞻《簪雲樓雜話》：“春联之设自明太祖始。帝都金陵，除夕前传旨，公卿士庶家，门口须加春联一副，帝微行出观。”

15.
16. 《燕京歲時記》：「自入臘以後，即有文人墨客在市肆檐下書寫春聯，以圖潤筆。祭灶之後，則漸次粘掛。千門萬戶，煥然一新。或用朱箋，或用紅紙。唯內廷及宗室王公等例用白紙，緣以紅邊藍邊，非宗室不得擅用。」

17. 蕭放、許明堂《春節》，中國社會出版社，ISBN 7508713559

18. [春聯](http://nrch.culture.tw/twpedia.aspx?id=11654)

19. [年前的準備](http://www.tj.xinhuanet.com//shkj/2005-01/25/content_3626159.htm)


20. [春節](http://chinesefestival.tripod.com/spring.html)

21. 《宜川縣誌》：「间有用纸书『立新春鸿禧』或『立新春大吉大利，万事亨通』等字者；乡民如不能写字，则画一『十』字代之。」

22. [〔御札〕立春大吉／鎮防火燭](http://www.seizansha.com/item_detail/itemCode,1338/)


23. [縁起物、熊手、正月飾り材料部品のお札飾り](http://engimonotakeya.jp/ofuda.html)

24. [立春榜](http://preview.britannica.co.kr/bol/topic.asp?article_id=b18a1943a)


25. [입춘(立春)을 맞아 입춘방(立春榜)을 써
    드립니다](http://www.ehyundai.com/portal/depart/webzinMain.jsp?pBoardId=202&pSeq=510900&pBranchSelect=510900&pBranchSelectName=510900&pSiteMapId=0103011002&swfseq=1002&pBoardState=2)

26. [《韓國的民俗與文化》第237頁](http://books.google.com/books?id=4JickhZxpBkC&pg=RA1-PA237&lpg=RA1-PA237&dq=%22%E9%96%80%E5%89%8D%E7%9A%84%E7%AB%8B%E6%98%A5%E5%B8%96%22&source=web&ots=Y5dnUyyG0P&sig=JHTvnHx81dS3cEJKHOtoDSqFdY4)

27. 《[东方日报](../Page/东方日报.md "wikilink")·海东见闻录·5》，2006年2月2日\]

28.

29. [입춘방(입춘첩)](http://chajara.co.kr/dic/dayview.asp?hMonth=2&hDay=4)

30. [越南「春聯」](http://ny.xmu.edu.cn/Article/ShowArticle.asp?ArticleID=1593)

31. [春聯的典故](http://www.musictown.idv.tw/yazhai/spring/original-spring%20couplets.htm)


32. [喜慶年合體字好日子](http://news.big5.enorth.com.cn/system/2007/02/16/001552296_01.shtml)

33.
34. 李光庭《鄉言解頤》：「桃符以画，春联以书。书较画为省便，复有斗方、横披、小单条之类。乡人不识字，有以人口平安与肥猪满圈互易者。」

35. [影音使團-
    DIY自製「天使BB」電子揮春](http://www.media.org.hk/others/faichun2006/index.asp)

36. [談談基督教的對聯](http://www.goldenlampstand.org/glb/readglb.php?GLID=06908)