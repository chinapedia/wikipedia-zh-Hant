**春雷龍屬**（[屬名](../Page/屬.md "wikilink")：）意為「古代的雷聲」，是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[梁龍超科的一屬](../Page/梁龍超科.md "wikilink")，化石發現於[美國](../Page/美國.md "wikilink")[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[莫里遜組](../Page/莫里遜組.md "wikilink")，年代為上[侏儸紀](../Page/侏儸紀.md "wikilink")。這些化石是在1999年到2000年的挖掘活動中挖掘出來的，並由J.D.
Harris與[彼得·達德森](../Page/彼得·達德森.md "wikilink")（Peter
Dodson）在2004年所敘述。這些關節脫落的化石是部份骨骸，包括部分[脊椎與四肢骨頭](../Page/脊椎.md "wikilink")。
[Suuwassea_emiliae3.JPG](https://zh.wikipedia.org/wiki/File:Suuwassea_emiliae3.JPG "fig:Suuwassea_emiliae3.JPG")
因為化石發現於[美國原住民的](../Page/美國原住民.md "wikilink")[阿布薩羅卡族的古代領地](../Page/阿布薩羅卡族.md "wikilink")，因此春雷龍是以該族語言中的*Suuwassa*為名，意為「春天的第一道雷聲」，*suu*意為「雷聲」，而*wassa*意為「古代」。種名*emilieae*是以挖掘活動的贊助者為名。

春雷龍是種基礎[梁龍超科恐龍](../Page/梁龍超科.md "wikilink")，身長估計為14到15公尺，牠們的頭顱骨與脊軸與[叉龍科](../Page/叉龍科.md "wikilink")、[梁龍科相比較為原始](../Page/梁龍科.md "wikilink")，而不能歸類於這兩個[演化支](../Page/演化支.md "wikilink")。這種[草食性恐龍與叉龍科的差別在於未癒合固定的](../Page/草食性.md "wikilink")[額骨](../Page/額骨.md "wikilink")，與梁龍科的差別在[枕骨大孔](../Page/枕骨大孔.md "wikilink")（Foramen
magnum）周圍骨頭的排列方式，但春雷龍與梁龍科擁有較多的相似處。

相較於莫里遜組南部所發現的大型動物，埃米莉春雷龍與其他[莫里遜組北部的中等](../Page/莫里遜組.md "wikilink")[蜥腳下目化石發現於同一時期](../Page/蜥腳下目.md "wikilink")。這些體型上的差異可能導因於[森丹斯海在中](../Page/森丹斯海.md "wikilink")[侏儸紀時期往北延伸](../Page/侏儸紀.md "wikilink")。一個蜥腳下目的[系統發生學研究對叉龍科與梁龍科的](../Page/系統發生學.md "wikilink")[自新徵提出懷疑](../Page/自新徵.md "wikilink")，認為這些特徵可能是不同科所保有的不同[祖徵](../Page/祖徵.md "wikilink")。叉龍科所保有的[勞亞大陸梁龍超科特徵](../Page/勞亞大陸.md "wikilink")，也增加了梁龍超科祖先同時起源於勞亞大陸與[岡瓦納大陸的可能性](../Page/岡瓦納大陸.md "wikilink")。

## 參考資料

  - Harris, J.D. and Dodson, P. 2004. A new diplodocoid sauropod
    dinosaur from the Upper Jurassic Morrison Formation of Montana, USA.
    *Acta Palaeontologica Polonica 49 (2):* 197–210.

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:叉龍科](../Category/叉龍科.md "wikilink")