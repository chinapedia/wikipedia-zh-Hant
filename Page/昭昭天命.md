[American_Progress_(John_Gast_painting).jpg](https://zh.wikipedia.org/wiki/File:American_Progress_\(John_Gast_painting\).jpg "fig:American_Progress_(John_Gast_painting).jpg")表現手法。在圖中，一個天使般的女人（有時被視為[哥倫比亞](../Page/哥倫比亞_\(美國\).md "wikilink")，美國19世紀時的擬人化象徵）帶著『文明』之光與拓荒者一同西行，在路程中串起[電報線](../Page/电报.md "wikilink")。[印地安人以及野生動物竄逃入前方的黑暗中](../Page/印第安人.md "wikilink")。\]\]

**昭昭天命**（\[1\]），為一個慣用措詞，是19世紀[美國所持的一種信念](../Page/美國.md "wikilink")，他們認為[美國被賦予了向西擴張至橫跨](../Page/美國.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")[大陸的天命](../Page/大陸.md "wikilink")。昭昭天命的拥护者們认为，美国在领土和影响力上的扩张不仅明显（Manifest），且本諸不可違逆之[天數](../Page/命運.md "wikilink")。昭昭天命最初為19世纪時的政治[標語](../Page/標語.md "wikilink")，後來成為標準的歷史名辭，意義通常等於[美國領土擴張橫貫北美洲](../Page/美國領土擴張.md "wikilink")，直達[太平洋](../Page/太平洋.md "wikilink")。

昭昭天命一直是籠統的觀念而非特定政策。若另加領土擴張主義，此詞也包含[美國卓異主義](../Page/美國例外主義.md "wikilink")、[羅馬式國家主義與所謂](../Page/羅馬式國家主義.md "wikilink")[盎格魯撒克遜民族優越性等信念](../Page/白人盎格魯-撒克遜新教徒.md "wikilink")。許多白人在論說昭昭天命時專注於最初的美式擴張主義；另有人認為該詞所表達之概念為，拓展疆域為美國對這個世界的「使命」。其使命的內容在不同的年代對不同的人有不同的意義。如：約翰·歐蘇利文
(John.O'Sullivan)「美國吞併德克薩斯共和國，不僅因為德克薩斯州希望這樣做，而是因為我們美國有『昭昭天命』」\[2\]，[約翰·昆西·亞當斯對於橫跨北美洲大陸的擴張使命和宣揚美國價值的重要性](../Page/約翰·昆西·亞當斯.md "wikilink")，“這似乎是注定的”他說。\[3\]歷史學家沃爾特麥克杜格爾
(Walter McDougall)
稱“昭昭天命”使[門羅主義成為必然結果](../Page/門羅主義.md "wikilink")\[4\]；[恩斯特·李·圖弗森](../Page/恩斯特·李·圖弗森.md "wikilink")（Ernest
Lee
Tuveson）總結這些各式各樣的可能意義，寫道：「『昭昭天命』一詞涵括廣闊繁複的觀念、政策與作法。彼此間既無預期般的相容，出處也各自不同。」\[5\]

「昭昭天命」一詞最初由1840年代[傑克遜民主的信徒所使用](../Page/傑克遜民主.md "wikilink")，用以宣傳兼併今日的[美西地區](../Page/美西地區.md "wikilink")（[奧勒岡屬地](../Page/奧勒岡屬地.md "wikilink")、[兼併德州與](../Page/兼併德州.md "wikilink")[墨西哥割讓](../Page/墨西哥割讓.md "wikilink")）。該詞於1890年代復由[共和黨支持者用以在理論上作為美國介入北美洲以外事務的理由](../Page/共和黨_\(美國\).md "wikilink")。昭昭天命曾是政治人物的慣用語，但許多評論員相信，其觀念在廿世紀持續影響了美國政治觀。\[6\]

本條目並非[美國領土擴充史](../Page/美國領土擴張.md "wikilink")，或[拓荒者西向墾殖美國](../Page/开拓者.md "wikilink")[邊疆的故事](../Page/邊疆.md "wikilink")。昭昭天命是對西向擴充運動的一種辯解或理由；又或者是一種促發其進程的[意識形態或](../Page/意識形態.md "wikilink")[學說](../Page/學說.md "wikilink")。本條目為昭昭天命此一觀念之沿革，及其對美國對外擴張之影響。

## 詞語的起源

[John_O'Sullivan.jpg](https://zh.wikipedia.org/wiki/File:John_O'Sullivan.jpg "fig:John_O'Sullivan.jpg")。不過現在大家只記得他所用來提倡兼併[得克萨斯州及](../Page/得克萨斯州.md "wikilink")[俄勒冈州的警句](../Page/俄勒冈州.md "wikilink")"昭昭天命"。\]\]

這個用以表彰顯而易見（或無可否認）的命運之詞句由[紐約市記者](../Page/纽约.md "wikilink")[約翰·歐蘇利文](../Page/約翰·歐蘇利文.md "wikilink")（John
L. O'Sullivan）於其《民主評論》（*Democratic
Review*）雜誌1845年七─八月號刊杜撰而出。在一篇名為《兼併》（*Annexation*）的文章中，歐蘇利文呼籲美國將[德克薩斯共和國併入聯邦](../Page/德克薩斯共和國.md "wikilink")，他寫道：「吾等盡取神賜之洲以納年年倍增之萬民自由發展之昭昭天命」。\[7\]
論戰之後不久，[兼併德州成真](../Page/兼併德州.md "wikilink")，但歐蘇利文首次寫出的「昭昭天命」一詞並未引人注意。\[8\]

歐蘇利文之第二次使用此詞則變得極富影響力。他於1845年12月27日在其《紐約晨報》論及與[大英帝國在](../Page/大英帝國.md "wikilink")[奧勒岡州持續不斷的邊界糾紛](../Page/俄勒冈州.md "wikilink")。歐蘇利文論斷美國有權主張索求「奧勒岡全境」：

也就是說，歐蘇利文相信[上帝的旨意](../Page/上帝的旨意.md "wikilink")（Divine
Providence）賦予美國廣佈[共和民主遍及北美洲之使命](../Page/共和制.md "wikilink")（大行自由權利，"the
great experiment of
liberty"）。對歐蘇利文來說，既然大英帝國不知利用奧勒岡以廣佈民主，不列顛對這塊土地的權利主張可以忽視。歐蘇利文認為昭昭天命為[道德觀念](../Page/道德.md "wikilink")（天條，"higher
law"），足以蓋過一切顧慮，包含國際法與國際協定。\[9\]

歐蘇利文原來對昭昭天命的構想並非強取豪奪以擴充領土。他相信美式民主的拓展勢不可當，會隨著白人（或[盎格魯撒克遜民族](../Page/盎格魯-撒克遜.md "wikilink")）遷移各處而出現而無需涉及軍事行為。歐蘇利文不贊同於1846年爆發的[美墨戰爭](../Page/美墨戰爭.md "wikilink")，然而他相信戰爭的結果將惠及雙方。\[10\]

歐蘇利文並未始創昭昭天命的概念；其措詞雖為此一於1840年代頗得人心的情懷提供有力的稱號，其概念自身並非新創。且歐蘇利文雖為鼓吹此種思想的先鋒之一，其他另有多名作者曾使用不同的詞句來描述同一概念。美國報業於1840年代的成長，尤其是煽動性的[八卦報刊](../Page/八卦報刊.md "wikilink")（penny
press），是昭昭天命等概念廣為散播的重要因素。

當時歐蘇利文自己並不曉得自己創造出了新的警句。此詞由[輝格黨引用以反對執政的](../Page/輝格黨.md "wikilink")[詹姆斯·诺克斯·波尔克總統後](../Page/詹姆斯·诺克斯·波尔克.md "wikilink")，廣為流傳。1846年1月3日，[羅伯·查爾斯·溫梭普](../Page/羅伯·查爾斯·溫梭普.md "wikilink")（Robert
Charles
Winthrop）眾議員於[眾議院內嘲弄這個觀念](../Page/眾議院.md "wikilink")，他說道：「我想，除了全[洋基國以外的任何國家](../Page/洋基.md "wikilink")，都不會容許散播昭昭天命之義。」對於昭昭天命的擁護者們引用「神賜之洲」來為基於俗世利益辯護的作法，在眾多批評者中，溫梭普是首位發難者。

美国不顧這些批評而接受該警句。它迅速流行，其出於歐蘇利文杜撰一事終為人所遺忘。歐蘇利文於1895年默默無聞地撒手人寰，約當其警句為人復用之時。直至1927年，方有歷史學家確定此一警句出自歐蘇利文之手。\[11\]

## 主題與影響

史學家威廉威克斯（William E. Weeks）注意到，擁護昭昭天命者們通常依附在三條關鍵主題之上：

1.  **[優越性](../Page/優越性.md "wikilink")**：美國人民與其體制的長處。
2.  **[使命](../Page/使命.md "wikilink")**：廣佈其體制，從而以美國的觀點解救並重建世界。
3.  **[天意](../Page/天意.md "wikilink")**：貫徹天命。\[12\]

這三條主題，又稱[美國卓異主義](../Page/美國例外主義.md "wikilink")，通常可回溯至美國的[清教徒傳統](../Page/清教徒.md "wikilink")，[約翰·溫索普](../Page/約翰·溫索普.md "wikilink")1630年廣為人知的佈道詞《[山上之城](../Page/山上之城.md "wikilink")》（City
upon a
Hill）的部分內容。他在這次證道中呼籲建立對[舊世界大放異彩的道德社會](../Page/舊世界.md "wikilink")。[湯姆斯·潘恩在其](../Page/托马斯·潘恩.md "wikilink")1776年具影響力的小冊《[常識](../Page/常識.md "wikilink")》中回應此見解，論說[美國革命提供機會以創建更好的新社會](../Page/美國革命.md "wikilink")：

許多美國人贊同潘恩，相信美國已然試行自由與民主——並摒棄舊世界的[君主制](../Page/君主制.md "wikilink")——這是世界史上的大事。[亞伯拉罕·林肯總統對美國的形容](../Page/亚伯拉罕·林肯.md "wikilink")：「全球最後的、最佳的寄託」就是這種觀念的著名表述。林肯在《[蓋茨堡演說](../Page/蓋茲堡演說.md "wikilink")》中奮力詮釋帶有美式理想的國家是否得以存續。史家羅伯·喬漢森（Robert
Johannsen）稱之為：「對美國昭昭天命論與使命感最恆久的聲明。」 \[13\]

美國身具使命，以擴張領土來廣佈其體制與理想的信念——[安德魯·傑克森於](../Page/安德鲁·杰克逊.md "wikilink")1843年之著名表述為「延展自由之域」——為昭昭天命論的基本觀點。許多人相信美式民主毋需合眾國政府之力便得以散播。美國的拓荒者們心懷信念橫越北美大陸，其綱領為北美與世界其他國家會盡力與美式體制相仿。[托马斯·杰斐逊最初並不認為美國必須向外擴張](../Page/托马斯·杰斐逊.md "wikilink")，他相信北美洲將成立其他類似於合眾國之共和政體，形成他所說的「自由帝國」。然而，在1803年的[路易西安娜購地之後](../Page/路易西安納購地.md "wikilink")，他接受了版圖膨脹。其後數十年間，隨著合眾國領土不斷外擴，「延展自由之域」是否意指延展蓄奴之域，因對美國的「使命」有不同解釋，對立逐漸升高，而成為議題核心。

## 對洲內擴張的影響

「昭昭天命」一詞最常為人聯想者，為美國自1815年至1860年間的[領土擴張](../Page/領土.md "wikilink")。此一時期，自[1812年戰爭結束起至](../Page/1812年战争.md "wikilink")[美國內戰爆發為止](../Page/南北战争.md "wikilink")，人稱「昭昭天命之世」（Age
of Manifest
Destiny）。美國版圖在這段期間擴展至[太平洋](../Page/太平洋.md "wikilink")——[自海至光輝之海](../Page/自海至光輝之海.md "wikilink")（"from
sea to shining
sea"）——基本確立今日[美國本土的邊界](../Page/美國本土.md "wikilink")。昭昭天命論在合眾國與其北方的[英屬北美](../Page/英屬北美.md "wikilink")（後稱[加拿大](../Page/加拿大.md "wikilink")）之間的關係上有其作用；但在其考量[墨西哥問題與引發](../Page/墨西哥.md "wikilink")[美墨戰爭上更具因果關係](../Page/美墨戰爭.md "wikilink")。昭昭天命論中，蔓延各處的[種族主義對美國](../Page/种族主义.md "wikilink")[印地安人而言後果嚴重](../Page/印第安人.md "wikilink")。\[14\]

### 大陸主義

十九世紀中，美國終將吞併北美洲全境的信念，世稱
[「大陸主義」](https://web.archive.org/web/20150509193436/http://www.sociologyindex.com/continental.htm)（continentalism）。此一構想的先期提倡者為[約翰·昆西·亞當斯](../Page/约翰·昆西·亚当斯.md "wikilink")，他是自1803年[路易西安那購地案至](../Page/路易西安納購地.md "wikilink")1840年代[詹姆斯·波克執政期間](../Page/詹姆斯·诺克斯·波尔克.md "wikilink")，美國版圖擴張的領導人物。1811年，他在給父親[約翰·亞當斯的信中寫道](../Page/约翰·亚当斯.md "wikilink")：

亞當斯為推展其理念著力甚深。他起草了[1818年條約](../Page/1818年條約.md "wikilink")（Treaty of
1818），奠定西至[洛磯山脈的美加國界](../Page/洛磯山脈.md "wikilink")，並約定奧勒岡國共管。他磋商出1819年的[亞當斯─歐尼斯條約](../Page/亞當斯─歐尼斯條約.md "wikilink")（Adams-Onís
Treaty）——又稱[貫洲條約](../Page/貫洲條約.md "wikilink")（Transcontinental
Treaty），自[西班牙手中買下](../Page/西班牙.md "wikilink")[佛羅里達並將與](../Page/佛羅里達.md "wikilink")[西屬墨西哥的](../Page/西屬墨西哥.md "wikilink")[邊界延伸至](../Page/邊界.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")。他並且有系統的陳述了1823年的[門羅主義](../Page/门罗主义.md "wikilink")，該說警示歐洲，[西半球不再開放歐洲成立殖民地](../Page/西半球.md "wikilink")。

[門羅主義與昭昭天命論密切相關](../Page/门罗主义.md "wikilink")。沃尔特·麥克道古（Walter
McDougall）稱昭昭天命論為門羅主義的自然結果，既然門羅主義並未明指擴張幅度，不斷擴張便為行使該主義之所必需。美國內部當時考量到，歐洲強權（尤其是[大英帝國](../Page/大英帝國.md "wikilink")）正尋求提升對北美洲的影響力，於是引發以對外擴張以資預防的呼聲。1935年，艾爾伯特·溫伯格（Albert
Weinberg）於其深具影響的對昭昭天命論的論文中寫道：「1840年代勃興的[擴張主義為防禦性作為](../Page/擴張主義.md "wikilink")，以在歐洲侵犯北美之前搶先一步。」\[15\]

### 英屬北美

#### 1815年之前

[美國獨立革命爆發之前](../Page/美國革命.md "wikilink")，[美國革命家們原本希望](../Page/美國革命家.md "wikilink")[法裔加拿大人能夠加入](../Page/法裔加拿大人.md "wikilink")[殖民地十三州以共同推翻](../Page/十三州殖民地.md "wikilink")[大英帝國的統治](../Page/大英帝国.md "wikilink")。[加拿大省](../Page/加拿大省.md "wikilink")[受邀舉派代表參加](../Page/告加拿大居民書.md "wikilink")[大陸議會](../Page/大陆会议.md "wikilink")，且[大陸會議已預先允許加拿大加入合眾國](../Page/大陆会议.md "wikilink")。1775年，當美國在企圖將英國逐出北美的戰爭中，[攻入加拿大時](../Page/美國入侵加拿大（1775）.md "wikilink")，也希望法屬加拿大能共襄盛舉。這些拉攏加拿大靠向十三州的策略，無一達成。在[巴黎和平協商中](../Page/巴黎條約_\(1783年\).md "wikilink")，[班哲明·富蘭克林企圖說服不列顛外交官將加拿大割讓給合眾國](../Page/本傑明·富蘭克林.md "wikilink")，亦未成功。由於大英帝國始終存在於合眾國北界，導致美國於[1812年戰爭期間](../Page/1812年战争.md "wikilink")，第二次北侵[英屬北美](../Page/英屬北美.md "wikilink")，但未成其功。

這些將[大英帝國逐出北美的企圖](../Page/大英帝国.md "wikilink")，時受引據為昭昭天命論付諸行動之先例。然而，加拿大史學家瑞嘉諾德·史都華（Reginald
Stuart）論斷，這些與「昭昭天命時期」（"Era of Manifest
Destiny"）中的行動，性質不同。史都華於1815年之前寫道：「所有看來有如領土擴張主義之舉措，其實是來自守成的想法，而非來自征服與兼併的雄才大略。」此種觀點認為，昭昭天命論並非引發1812年戰爭之因素，而是在戰後才出現，並成為公眾信念。

#### 進入加拿大的義勇兵

儘管[仇英心理繼續於合眾國內散佈](../Page/仇英心理.md "wikilink")，美國人於[1812年戰爭後逐漸接受英屬殖民地於北界接壤的現實](../Page/1812年战争.md "wikilink")。許多美國人，尤其是美加邊界沿線居民，原本希望[1837年起义可以結束大英帝國在北美的統治](../Page/1837年起义.md "wikilink")，並於加拿大建立民主政府。對這些事件，約翰·歐蘇利文寫道：「若自由為一國之最佳福祉，若自治為一國之首要權利，…我們對加拿大人發難之緣由感同身受。」許多美國人，如歐蘇利文一般，視這幾場叛亂為美國革命再現；而且——與當時多數加拿大人的觀點不同——以為加拿大人生活於外來統治者的壓迫之下。\[16\]

儘管感同身受，昭昭天命的信念並未隨著美國人對這場叛亂的反應而廣佈開來，部分是因為這幾次叛亂結束得太快。歐蘇利文個人建議抵制美國調停。若干美國[義勇兵](../Page/義勇兵.md "wikilink")─未經政府授權，自願參戰的軍人，通常是受昭昭天命的信念所激勵─前去加拿大以助一臂之力，但[马丁·范布伦總統派遣](../Page/马丁·范布伦.md "wikilink")[溫菲爾德·史考特將軍逮捕這群義勇兵並保持邊界平靜](../Page/溫菲爾德·史考特.md "wikilink")。若干義勇兵殘存於人稱[獵人草屋](../Page/獵人草屋.md "wikilink")（Hunters'
Lodges）的秘密集團中，嘗試鼓動戰爭以「解放」加拿大─其中的一次即所謂的[愛國者戰爭](../Page/愛國者戰爭.md "wikilink")─但美國的觀點與政府官方政策反對這些行動。美國內戰後的[芬尼安會暴動](../Page/芬尼安會暴動.md "wikilink")（Fenian
raids，芬尼安為十九世紀中葉愛爾蘭爭取民族獨立的反英運動）與獵人草屋的行為有相似性，但與昭昭天命的觀念或美國的領土擴張政策並無關聯。\[17\]

#### 奧勒岡國

在合眾國北界，昭昭天命論在奧勒岡國與大英帝國的邊界紛爭中發揮其最重要的作用。[1818年英美會議](../Page/1818年英美會議.md "wikilink")（Anglo-American
Convention of
1818）約定奧勒岡國共管，而數以千計的美國移民於1840年代經由[奧勒岡小徑移居該處](../Page/奧勒岡小徑.md "wikilink")。英國人回絕[約翰·泰勒總統依](../Page/约翰·泰勒.md "wikilink")[約翰·昆西·亞當斯之前議](../Page/约翰·昆西·亚当斯.md "wikilink")，將該區沿[北緯49度線一分為二的提案](../Page/北緯49度線.md "wikilink")，而堅持將邊界南移至[哥倫比亞河](../Page/哥倫比亞河.md "wikilink")，此舉將今日[華盛頓州的所在地劃歸英屬北美](../Page/华盛顿州.md "wikilink")。昭昭天命的擁護者們誓死反對，並呼籲兼併奧勒岡國全境。總統候選人[詹姆斯·诺克斯·波尔克運用汹湧的民氣自利](../Page/詹姆斯·诺克斯·波尔克.md "wikilink")，民主黨並於[1844年美國總統大選中](../Page/1844年美國總統大選.md "wikilink")，呼籲兼併「全奧勒岡」。

成為總統後，波尔克重提沿北緯49度線將該區一分為二之議，使昭昭天命論最激切的擁護者們心灰意冷。當英國回絕該議後，美國的擴張主義者回以口號[不達54-40就打仗](../Page/不達54-40就打仗.md "wikilink")！（Fifty-Four
Forty or
Fight，北緯54度40分為[奧勒岡北界](../Page/俄勒冈州.md "wikilink")。該口號經常被錯誤地引述為1844年大選中的口號）。當波尔克轉而中止共管協議，英國終於同意沿北緯49度線將該區一分為二，爭端於1846年兩國簽訂[奧勒岡條約後落幕](../Page/奧勒岡條約.md "wikilink")。

雖然早先為了『全奧勒岡』而群情汹湧，該條約於美國頗孚民望，並迅即獲參議院認可，部份原因是由於合眾國當時正與墨西哥交戰。許多美國人相信加拿大諸省遲早會併入合眾國，而這場戰爭對達成命運之付託並無必要，尚且產生不良後果。據瑞嘉諾德·史都華的說法，昭昭天命論最激切的擁護者們之所以未在北方邊界問題上佔上風，「是由於儘管稱之為『大陸主義』，昭昭天命的適用範圍僅達美國西部與西南，而不及於北方。」\[18\]

### 墨西哥與德克薩斯

1836年，[德克薩斯共和國宣告自墨西哥](../Page/得克萨斯共和国.md "wikilink")[獨立](../Page/德克薩斯宣告獨立.md "wikilink")。在[德克薩斯革命之後](../Page/德克薩斯革命.md "wikilink")，更尋求加入合眾國成為新的一州。這正是自傑佛遜以來，至歐蘇利文時代，所提倡的理想擴張程序：可能的新州要求歸屬於合眾國，而非合眾國擴充其治權以凌駕於無此意願的人民頭上。然而，兼併德克薩斯實屬矛盾，因其使聯邦多增一蓄奴州。[安德魯·傑克遜總統與](../Page/安德鲁·杰克逊.md "wikilink")[马丁·范布伦總統因為奴隸制度的爭議威脅到民主黨的整體性](../Page/马丁·范布伦.md "wikilink")，拒絕德克薩斯加入並成為合眾國的一部分。

1844年大選之前，輝格黨候選人[亨利·克雷與據推定應為民主黨候選人的前總統马丁](../Page/亨利·克莱.md "wikilink")·范布伦雙方都宣稱反對兼併德克薩斯，都希望別讓此一棘手的話題成為選戰議題。因此意外地使民主黨放棄范布伦，轉推支持兼併的[波尔克](../Page/詹姆斯·诺克斯·波尔克.md "wikilink")。波尔克將兼併德克薩斯的難題與奧勒岡的邊界紛爭相連繫，而提出一連串與兼併相關的地區性協議。（北方的擴張主義者較傾向於佔有奧勒岡；而南方的擴張主義者主張兼併德克薩斯。）波尔克雖僅以極小差距勝出，卻將自己的勝選當成選民對版圖擴張的付託。

#### 「全墨西哥」

美國國會於波尔克就職前，通過[美國兼併德克薩斯法案](../Page/美國兼併德克薩斯.md "wikilink")。波尔克更進一步佔領墨西哥主張所有權的德克薩斯領土，是為於1846年4月24日爆發的[美墨戰爭之前奏](../Page/美墨戰爭.md "wikilink")。由於美方告捷，1847年夏出現了兼併「全墨西哥」的呼聲，部分來自東部的民主黨人，他們主張將墨西哥納入聯邦為確保該地區永久和平的最佳手段。

這項主張因兩個理由而自我矛盾。首先，約翰·歐蘇利文等理想化的昭昭天命提倡者不斷提及，合眾國的律法不應強加於無此意願的人民頭上，而兼併「全墨西哥」侵犯此一原則。其次，兼併墨西哥意指將合眾國公民權擴及千千萬萬名墨西哥人。支持兼併德克薩斯的南卡羅來納州聯邦參議員[約翰·考宏](../Page/约翰·卡德威尔·卡尔霍恩.md "wikilink")，因著昭昭天命之「使命」，以種族上的理由反對兼併墨西哥。他於1848年1月4日在國會中的演說中闡明其觀點：

這場辯論帶出了昭昭天命論的矛盾之一：一方面，昭昭天命論中的種族觀念暗示，墨西哥人由於非盎格魯撒克遜民族，不具成為美國人的資格；而昭昭天命論中「使命」的部分暗示，墨西哥人將因被納入美式民主而進化（或說重生，後來的說法）。種族觀念曾被用以推行昭昭天命論，但在考宏以及「全墨西哥」運動的例子中，種族觀念亦被用以反對昭昭天命論。

這場矛盾最終以[墨西哥割讓領土解決](../Page/墨西哥割讓領土.md "wikilink")。兩塊人口較墨西哥其他地方稀少的屬地[加利福尼亞與](../Page/加利福尼亞_\(消歧義\).md "wikilink")[新墨西哥加入合眾國](../Page/新墨西哥州.md "wikilink")。「全墨西哥」運動，一如「全奧勒岡」運動般，無疾而終。

### 印地安人

合眾國的陸上擴展通常意為佔據[印地安人所有之地](../Page/印第安人.md "wikilink")。合眾國承襲歐洲人的作為，只有限度的承認[美洲土著的土地權利](../Page/印第安人.md "wikilink")。依一項由[戰爭部長](../Page/戰爭部長.md "wikilink")[亨利·納克斯](../Page/亨利·納克斯.md "wikilink")（Henry
Knox）所大規模擘劃的政策，合眾國政府尋求以合法的印地安土地交易為唯一擴展領土進入西部的管道，鼓勵印地安人售出部落土地並「開化」，意即（連同其他事項）放棄打獵，變成農夫；並使印地安社會以[家庭為單位](../Page/家庭.md "wikilink")，而非氏族或部落。「開化」計畫的擁護者相信，這樣的程序會大大的降低印地安人所需的土地面積，於是有更多的土地可與美國白人交易。[托马斯·杰斐逊相信](../Page/托马斯·杰斐逊.md "wikilink")，既然印地安人的智力與白人不相上下，印地安人理應像白人一樣的生活，或無可避免地被白人推到一旁。傑佛遜根源於[啟蒙時期的想法](../Page/啟蒙時代.md "wikilink")，即印地安人與白人一起創建新國家的理念，於其有生之年未曾實現。他於是開始認為印地安人應移居到[密西西比河的另一側](../Page/密西西比河.md "wikilink")，維持社會分離。此一觀念因1803年的[路易西安那購地案而成為可能](../Page/路易西安納購地.md "wikilink")。

此一觀念，又名[搬遷印地安人](../Page/搬遷印地安人.md "wikilink")，於昭昭天命之世浮出檯面。雖有若干擁護搬遷者基於人道相信印地安人最好是搬離白人的地方，越來越多的美國人認為印地安人只不過是擋在美國西擴之路上的「野蠻人」。

英國在1812年戰爭後了解美國關於佔領印第安人土地的立場之後，一位英國談判代表亨利·古爾本(Henry.Goulburn)曾震驚地說道：

史家瑞吉納德·郝斯曼於其深具影響力的論文「種族與昭昭天命」論斷，種族性修詞於昭昭天命年代大量增加。美國人逐漸相信印地安人會隨著美國的擴張而凋零。這樣的觀點反應在美國首位偉大的史家[佛蘭西斯·帕克曼其](../Page/佛蘭西斯·帕克曼.md "wikilink")1851年印行的皇皇巨著[龐帝克的陰謀中](../Page/龐帝克的陰謀.md "wikilink")。帕克曼寫道，印地安人「註定要在盎格魯美國人其所向無敵的西進力量之浪潮面前消失無踪」。

## 北美洲之外

隨著美國內戰於歷史中淡去，「昭昭天命」一辭再度為人復用了一段短時間。[美國共和黨在](../Page/共和黨_\(美國\).md "wikilink")[1892年美國總統大選中的黨綱宣告](../Page/1892年美國總統大選.md "wikilink")：「本黨重申，贊成[門羅主義](../Page/门罗主义.md "wikilink")，並概括性認可共和政體昭昭天命之功。」由於共和黨敗選，宣言中的「昭昭天命」一辭並無確切定義。然而，共和黨於[1896年美國總統大選後重回白宮](../Page/1896年美國總統大選.md "wikilink")，其後更連霸16年。昭昭天命一辭其時用以推展[美國的海外領土擴張](../Page/美國的海外領土擴張.md "wikilink")。而昭昭天命的這種說法是否符合1840年代的大陸擴張主義，在當時即引起爭論，並延伸至後世，長久不衰。\[19\]

例如說，[威廉·麦金莱總統於](../Page/威廉·麦金莱.md "wikilink")1898年提倡兼併[夏威夷屬地](../Page/夏威夷屬地.md "wikilink")（Territory
of
Hawaii），他說道：「我們需要取得夏威夷並好好經營，更甚於前此之於加利福尼亞，此乃昭昭天命。」另一方面，曾於執政期間阻擋兼併夏威夷的民主黨籍前總統[格罗弗·克利夫兰寫道](../Page/格罗弗·克利夫兰.md "wikilink")，麥肯利的兼併為「歪曲我國之天命」。史學家亦持續兩種觀點之論戰，一派將美國在1890年代的海外擴張解讀為昭昭天命論延伸遠跨[太平洋彼岸](../Page/太平洋.md "wikilink")；另一派視其為昭昭天命論之對立面。\[20\]

### 美西戰爭與菲律賓

1898年，在美軍的[緬因號戰艦於](../Page/緬因號戰艦_\(ACR-1\).md "wikilink")[古巴](../Page/古巴.md "wikilink")[哈瓦那的港口爆炸沉沒後](../Page/哈瓦那.md "wikilink")，美國插手介入古巴反抗軍與西班牙帝國之間，是為[美西戰爭之開端](../Page/美西战争.md "wikilink")。雖說1840年代的昭昭天命論之擁護者古曾籲求兼併古巴，[美國參議院於戰前全體一致通過](../Page/美国参议院.md "wikilink")[鐵勒修正案](../Page/鐵勒修正案.md "wikilink")（Teller
Amendment），承認古巴之「自由獨立」，並放棄所有美國兼併該島之意圖。而戰後通過的[普拉特修正案則實質上承認美國為古巴的](../Page/普拉特修正案.md "wikilink")[保護國](../Page/保护国.md "wikilink")。昭昭天命若意味公然兼併，則自此對古巴不再適用。

與古巴情形不同的是，美國在美西戰爭後兼併[關島](../Page/關島.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、以及[菲律賓](../Page/菲律宾.md "wikilink")。攫取這些島嶼為美國歷史寫下新頁。過去，美國取得領土的目的在於建立新州，取得相同於已有各州之立足點；而這些新取得之各島嶼為[殖民地](../Page/殖民地.md "wikilink")，而非預期建立的新州。此一程序經諸[島嶼釋憲案](../Page/島嶼釋憲案.md "wikilink")（Insular
Cases）確認。[美國最高法院於這一連串釋憲案中裁決](../Page/美国最高法院.md "wikilink")，美利堅合眾國轄下所有領地並非自動完全適用美國憲政權利。依此見解，兼併違反昭昭天命論之傳統義意。據弗雷德里克·默克（Frederick
Merk）所言，「昭昭天命包含考宏與歐蘇利文所能同意的基礎原則─不應併入無資格立州之人民。此一原則被1899年的帝國主義甩到一邊去。」\[21\]

另一方面，昭昭天命的概念中也包含著，「未開化」的人民可在接受美國基督教民主價值之洗禮後進步。麥肯利總統於決定兼併菲律賓後，對此一主題思想評論道：「我們別無可為，只有全盤接收，並教育菲律賓人，並且使他們發展、開化、信奉基督教…」[鲁德亚德·吉卜林之](../Page/魯德亞德·吉卜林.md "wikilink")《[白種人的負擔](../Page/白人的负担.md "wikilink")》一詩，以「合眾國與菲律賓群島」（The
United States and the Philippine
Islands）為副標題，為當時這種公眾情懷之著名表述。然而，許多菲律賓人抗拒此一「發展與開化」，終於在1899年爆發[美菲戰爭](../Page/美菲战争.md "wikilink")。美國海外擴張的反對者，[威廉·詹宁斯·布莱恩於開戰後寫道](../Page/威廉·詹宁斯·布莱恩.md "wikilink")：「天命不若數星期前一般昭昭。」\[22\]

### 其後之慣用法

世紀交替之後，美國不再依靠「天命」推展領土擴張，「昭昭天命」一辭之使用率大減。在[西奥多·罗斯福總統治下](../Page/西奥多·罗斯福.md "wikilink")，美國在新世界中的角色，一如其於1904年對[門羅主義的](../Page/门罗主义.md "wikilink")[羅斯福推論](../Page/羅斯福推論.md "wikilink")（Roosevelt
Corollary），定義為「國際警力」，以確保美國在西半球之利益為要務；羅斯福推論明確摒棄領土擴張。過去，昭昭天命被視為在西半球行使門羅主義之所必需；而今，領土擴張主義已為[干涉主義所取代](../Page/干涉主義.md "wikilink")，成為維護門羅主義之工具。

[伍德羅·威爾遜總統承續干涉主義政策](../Page/伍德罗·威尔逊.md "wikilink")，並嘗試以全球為範圍，重新定義昭昭天命與美國之「使命」。威爾遜總統帶著「這個世界為行民主政體必須穩健可靠」\[23\]
的爭議，引領美國參加[第一次世界大戰](../Page/第一次世界大战.md "wikilink")。戰後，在其1920年對國會發表的諮文中，威爾遜聲明：

這是首次，也是唯一次，在任總統於年度演說中使用「昭昭天命」一辭。威爾遜總統對昭昭天命的說法為，摒棄擴張主義，並為[民族自決](../Page/民族自決.md "wikilink")（作政策上的）背書，強調美利堅合眾國為著民主事業，有成為世界領袖之使命。[第二次世界大戰之後](../Page/第二次世界大战.md "wikilink")，美國自視為[自由世界領袖之看法強力增長](../Page/自由世界.md "wikilink")，卻罕聞其如威爾遜總統之描述一般，稱之為「昭昭天命」\[24\]\[25\]

時至今日，在學術界的標準用法中，『昭昭天命』所描述的是美國史上的一段時期，尤其是1840年代。然而，該辭句有時用以描述美利堅合眾國當代的政策與軍事行動，這種用法通常來自[左派人士](../Page/左派.md "wikilink")，也通常以之為負面觀點。在這種事例上，昭昭天命論通常被視為當代所意識到的[美利堅帝國之潛在成因](../Page/美利坚帝国.md "wikilink")（或起源）。

## 參見

  - [美國領土擴張](../Page/美國領土擴張.md "wikilink")
  - [年輕美國運動](../Page/年輕美國運動.md "wikilink")（Young America movement）

昭昭天命論相關人物：

  - [史帝芬·道格拉斯](../Page/史帝芬·道格拉斯.md "wikilink")（Stephen A. Douglas）
  - [湯瑪斯·哈特·班頓](../Page/湯瑪斯·哈特·班頓.md "wikilink")（Thomas Hart Benton）
  - [喬治·班克勞福](../Page/喬治·班克勞福.md "wikilink")（George Bancroft）
  - [荷瑞斯·葛雷利](../Page/荷瑞斯·葛雷利.md "wikilink")—“到西部去吧，年輕人。”
  - [達夫·葛林](../Page/達夫·葛林.md "wikilink")（Duff Green）

## 注释

## 參考文獻

  - Hayes, Sam W. and Christopher Morris, eds. *Manifest Destiny and
    Empire: American Antebellum Expansionism*. College Station, Texas:
    Texas A\&M University Press, 1997. ISBN 0-89096-756-3.
  - Horsman, Reginald. *Race and Manifest Destiny: The Origins of
    American Racial Anglo-Saxonism*. Cambridge, Massachusetts: Harvard
    University Press, 1981.
  - McDougall, Walter A. *Promised Land, Crusader State: The American
    Encounter with the World Since 1776*. New York: Houghton Mifflin,
    1997.
  - Merk, Frederick. *Manifest Destiny and Mission in American History:
    A Reinterpretation*. New York, Knopf, 1963.
  - Stephanson, Anders. *Manifest Destiny: American Expansionism and the
    Empire of Right*. New York: Hill and Wang, 1995. ISBN 0-8090-1584-6;
    ISBN
    0-89096-756-3.（[review](https://web.archive.org/web/20060104200628/http://www.h-net.org/reviews/showrev.cgi?path=13895872103990)）
  - Stuart, Reginald C. *United States Expansionism and British North
    America, 1775–1871*. Chapel Hill, N.C.: University of North Carolina
    Press, 1988. ISBN 0-8078-1767-8
  - Tuveson, Ernest Lee. *Redeemer Nation: The Idea of America's
    Millennial Role*. Chicago: University of Chicago Press, 1968.
  - Weeks, William Earl. *Building the Continental Empire: American
    Expansion from the Revolution to the Civil War*. Chicago: Ivan R.
    Dee, 1996. ISBN 1-56663-135-1.
  - Weinberg, Albert K. *Manifest Destiny: A Study of Nationalist
    Expansionism in American History*. Baltimore: Johns Hopkins, 1935.
    Cited by many scholars as still the best book on the topic.

## 外部連結

  - [Entry from *The Reader's Companion to American
    History*](http://college.hmco.com/history/readerscomp/rcah/html/rc_056100_manifestdest.htm)
  - [Several Manifest Destiny articles from the PBS website on the
    U.S.–Mexican
    War](https://web.archive.org/web/20051230171527/http://www.pbs.org/kera/usmexicanwar/dialogues/prelude/manifest/manifestdestiny.html)
  - ["Expansion, Continental and
    Overseas"](http://college.hmco.com/history/readerscomp/rcah/html/ah_029400_expansioncon.htm)，article
    by William Appleman Williams.
  - [The March 1845 Inaugural Address of James K.
    Polk](https://web.archive.org/web/20080516105557/http://www.yale.edu/lawweb/avalon/presiden/inaug/polk.htm)

[Category:美加關係](../Category/美加關係.md "wikilink")
[Category:文化圈](../Category/文化圈.md "wikilink")
[Category:19世纪美国政治](../Category/19世纪美国政治.md "wikilink")
[Category:美国扩张史](../Category/美国扩张史.md "wikilink")
[Category:美国西部历史](../Category/美国西部历史.md "wikilink")
[Category:美國外交史](../Category/美國外交史.md "wikilink")
[Category:帝国主义](../Category/帝国主义.md "wikilink")
[Category:美墨關係](../Category/美墨關係.md "wikilink")
[Category:泛運動](../Category/泛運動.md "wikilink")
[Category:政治理論](../Category/政治理論.md "wikilink")
[Category:历史理论](../Category/历史理论.md "wikilink")
[Category:美国领土变迁](../Category/美国领土变迁.md "wikilink")
[Category:美国哲学](../Category/美国哲学.md "wikilink")
[Category:美国白人](../Category/美国白人.md "wikilink")
[Category:美洲原住民历史](../Category/美洲原住民历史.md "wikilink")

1.  又译：**天命论**、**天命观**、**天命昭彰**、**昭彰天命**、**天定命运论**、**美国天命论**、**天赋使命观**、**上帝所命**、**神授天命**、**命定扩张论**、**昭示的命运**、**天赋命运**
2.  Quoted in Thomas R. Hietala,Manifest design: American exceptionalism
    and Empire (2003) p.255
3.  Adams quoted in McDougall 1997,p.78.
4.  McDougall 1997,p.74; Weinberg 1935,p.109.
5.  Tuveson quote, p. 91.
6.  Stephanson's *Manifest Destiny: American Expansionism and the Empire
    of Right* examines the influence of Manifest Destiny in the 20th
    century, particularly as articulated by [Woodrow
    Wilson](../Page/Woodrow_Wilson.md "wikilink") and [Ronald
    Reagan](../Page/罗纳德·里根.md "wikilink").
7.  [*Annexation*](http://web.grinnell.edu/courses/HIS/f01/HIS202-01/Documents/OSullivan.html)："our
    manifest destiny to overspread the continent allotted by Providence
    for the free development of our yearly multiplying millions.",John
    O'Sullivan.
8.  Robert W. Johannsen, "The Meaning of Manifest Destiny", in Hayes, p.
    9.
9.  Weinberg, p. 145; Johannsen p. 9.
10. Johannsen, p. 10.
11. Winthrop quote: Weingberg, p. 143; O'Sullivan's death, later
    discovery of phrase's origin: Stephanson, p. xii.
12. Weeks, p. 61.
13. Haynes, pp. 18–19.
14. Stuart and Weeks call this period the "Era of Manifest Destiny" and
    the "Age of Manifest Destiny", respectively.
15. Adams quoted in McDougall, p. 78.
16. O'Sullivan and the U.S. view of the uprisings: Stuart, pp.128-46.
17. O'Sullivan against intervention: Stuart p. 86; Filibusters: Stuart,
    ch. 6; Fenians unrelated: Stuart 249.
18. Treaty popular: Stuart, p. 104; compass quote p. 84.
19. Republican Party
    [platform](http://www.presidency.ucsb.edu/showplatforms.php?platindex=R1892)
    ; context not clearly defined, Merk p. 241.
20. McKinley quoted in McDougall, pp. 112–13; "anithesis" of Manifest
    Destiny: Merk, p. 257.
21. Merk quote, p. 257.
22. McKinley quoted in McDougall, p. 112; Bryan quoted in Weinberg, p.
    283.
23. 原文:"The world must be made safe for democracy."
24. ["Safe for
    democracy"](http://en.wikisource.org/wiki/President_Wilson's_War_Address);
25. [1920
    message](http://www.presidency.ucsb.edu/ws/index.php?pid=29561);
    Wilson's version of Manifest Destiny: Weinberg, p. 471.