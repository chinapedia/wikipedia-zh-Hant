**昭槤**（），字**汲修**，自號**汲修主人**，另說號**檀樽主人**\[1\]。[滿清](../Page/滿清.md "wikilink")[宗室](../Page/宗室.md "wikilink")。生於[清](../Page/清.md "wikilink")[乾隆四十一年](../Page/乾隆.md "wikilink")（1776年），卒於[清](../Page/清.md "wikilink")[道光九年十二月](../Page/道光.md "wikilink")（合1830年）。

昭槤是[努爾哈赤次子禮親王](../Page/努爾哈赤.md "wikilink")[代善的第六世孫](../Page/代善.md "wikilink")，父名[永恩](../Page/永恩.md "wikilink")，原封康親王。爱好文史，精通[满洲民俗和清朝典章制度](../Page/满洲.md "wikilink")，與[魏源](../Page/魏源.md "wikilink")、[龚自珍](../Page/龚自珍.md "wikilink")、[紀昀](../Page/紀昀.md "wikilink")、[袁枚等名士有往來](../Page/袁枚.md "wikilink")。

[嘉慶二十年](../Page/嘉慶.md "wikilink")（1816年）因虐下获罪\[2\]，革除王爵，圈禁三年。半年后释放，但未復其爵。

道光二年（1822年）賞封候補[主事](../Page/主事.md "wikilink")\[3\]，後病故，其文稿大多散失，由[端方搜集整理](../Page/端方.md "wikilink")，有《[嘯亭雜錄](../Page/嘯亭雜錄.md "wikilink")》十五卷。《[清史稿](../Page/清史稿.md "wikilink")·卷二百一十六
列傳三 諸王二》禮烈親王代善附昭槤傳\[4\]。

## 家族

  - 父：禮恭親王[永恩](../Page/永恩.md "wikilink")
  - 母：繼福晉舒穆祿氏\[5\]
  - 妻妾：\[6\]
      - 嫡妻伊爾根覺羅氏
  - 子：\[7\]
    1.  *[錫濬](../Page/錫濬.md "wikilink")，[茂春子](../Page/茂春.md "wikilink")，母妾夏氏*

## 参考文献

### 引用

### 来源

  - 《[清史稿](../Page/清史稿.md "wikilink")》
      - 列傳三 諸王二
      - 表二 皇子世表二

{{-}}

[-](../Category/禮親王.md "wikilink") [A](../Category/清朝歷史學家.md "wikilink")
[Category:清朝宗室已革亲王](../Category/清朝宗室已革亲王.md "wikilink")
[Category:清朝主事](../Category/清朝主事.md "wikilink")

1.  《骨董瑣記》卷三載，昭槤號檀樽主人

2.  《清仁宗实录》卷312「嘉慶二十年十一月丙午諭內閣」載：昭槤因于大海增租，謀充莊頭，即將程建義革退，並令照于大海加增之數，加找二年租銀。程建義之父程幅海不從，昭槤派護柳長壽前往程幅海家搶割莊稼，拆毀房屋，又將程幅海父子叔姪六人圈禁。昭槤自擲磁瓶於地，用磁片劃傷程建義、程建忠脊背百餘道，至於流血昏暈。

3.
4.  [清史稿](../Page/:s:清史稿.md "wikilink")[卷二百一十六 列傳三
    諸王二](../Page/:s:清史稿/卷216.md "wikilink")[禮烈親王代善](../Page/:s:清史稿/卷216#禮烈親王代善.md "wikilink")

5.
6.
7.