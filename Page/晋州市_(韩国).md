**晉州市**（），是韓國的一個城市。

## 歷史

晉州的歷史最早可以向前追溯到[伽倻時期](../Page/伽倻.md "wikilink")。從[朝鮮王朝](../Page/朝鮮王朝.md "wikilink")[高宗時代的](../Page/朝鮮高宗.md "wikilink")1896年到[日本統治時期的](../Page/朝鲜日治時期.md "wikilink")1925年，該地曾經成為慶尚南道首府。

## 交通

晉州市是未來韓國[中部內陸線的終點站](../Page/中部內陸線.md "wikilink")。鐵路的起點位於[京畿道](../Page/京畿道.md "wikilink")[城南市](../Page/城南市.md "wikilink")。

## 友好城市

  - [尤金](../Page/尤金_\(俄勒岡州\).md "wikilink") （1961年）

  - [北海道](../Page/北海道.md "wikilink")[北見市](../Page/北見市.md "wikilink")
    （1985年）

  - [温尼伯](../Page/温尼伯.md "wikilink") （1992年）

  - [全羅南道](../Page/全羅南道.md "wikilink")[順天市](../Page/順天市.md "wikilink")
    （1998年）

  - [京都市](../Page/京都市.md "wikilink") （1999年）

  - [島根縣](../Page/島根縣.md "wikilink")[松江市](../Page/松江市.md "wikilink")
    （1999年）

  - [河南省](../Page/河南省.md "wikilink")[鄭州市](../Page/鄭州市.md "wikilink")
    （2000年）

  - [忠清南道](../Page/忠清南道.md "wikilink")[牙山市](../Page/牙山市.md "wikilink")
    （2004年）

  - [慶尚北道](../Page/慶尚北道.md "wikilink")[安东市](../Page/安东市.md "wikilink")
    （2004年）

  - [陕西省](../Page/陕西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")（2004年）

  - [首爾特別市](../Page/首爾特別市.md "wikilink")[江南區](../Page/江南區_\(首爾\).md "wikilink")
    （2005年）

  - [俄羅斯](../Page/俄羅斯.md "wikilink")[鄂木斯克](../Page/鄂木斯克.md "wikilink")
    （2007年）

[晉州市_(韓國)](../Category/晉州市_\(韓國\).md "wikilink")