**普利茅斯大学**（Plymouth
University）是[英國](../Page/英國.md "wikilink")[英格兰西南部规模最大的](../Page/英格兰.md "wikilink")[大学](../Page/大学.md "wikilink")，於1862年創立。在校生人数超过三万，从人数上来讲位于全英第九位（算入[英國公開大學](../Page/英國公開大學.md "wikilink")）。

2011年6月改現名，但在權利證書上仍使用University of Plymouth。\[1\]

## 學術

2016/17年[泰晤士高等教育排名將其列在世界第](../Page/泰晤士高等教育排名.md "wikilink")351至400位之間，\[2\]

## 参考文献

[Category:英格兰大学](../Category/英格兰大学.md "wikilink")
[Category:1992年創建的教育機構](../Category/1992年創建的教育機構.md "wikilink")
[Category:1992年英格蘭建立](../Category/1992年英格蘭建立.md "wikilink")

1.
2.