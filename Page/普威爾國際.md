**普威爾國際股份有限公司**（**Proware Multimedia International Co.,
Ltd.**），簡稱**普威爾**、**普社**，是[台灣的一家動畫代理](../Page/台灣.md "wikilink")、商品授權公司。

## 概要

  - 主要代理的範圍為[日本動畫](../Page/日本動畫.md "wikilink")、商品授權、商品肖像代言活動授權、美工圖庫、幼教軟體等。
  - 著名的代理動畫及商品授權有[名偵探柯南及其動畫電影系列](../Page/名偵探柯南.md "wikilink")，固定於每年七到八月（暑假期間）於台灣上映。此外，還有[銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")、[新海誠作品系列](../Page/新海誠.md "wikilink")、[機動戰士Z鋼彈劇場版](../Page/機動戰士Z_GUNDAM.md "wikilink")、[AIR](../Page/AIR.md "wikilink")（電視版）、[Kanon](../Page/Kanon.md "wikilink")（[京都動畫電視重製版](../Page/京都動畫.md "wikilink")）、[K-ON！輕音部](../Page/K-ON！輕音部.md "wikilink")、[庫洛魔法使](../Page/庫洛魔法使.md "wikilink")、[Chobits](../Page/Chobits.md "wikilink")、[新世紀福音戰士等作品](../Page/新世紀福音戰士.md "wikilink")。
  - 授權、經銷及影音產品生產實務由**銘成科技有限公司**（*Nameware Tech. Co.,
    Ltd.*）負責，公司登記代表人亦在普威爾擔任管理要職。

## 代理作品列表

以下為普威爾曾經代理的動畫作品列表。

<div style="-moz-column-count:2; column-count:2;">

### 字母開頭

  - [AIR](../Page/AIR.md "wikilink")
  - [AKIRA](../Page/亞基拉.md "wikilink")
  - [Angel Beats\!](../Page/Angel_Beats!.md "wikilink")
  - [APPLESEED](../Page/APPLESEED.md "wikilink")
  - [Chobits](../Page/Chobits.md "wikilink")
  - [CLANNAD](../Page/CLANNAD.md "wikilink")
  - [Code Geass 反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")
  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink")(命運／停駐之夜)
  - [H<sub>2</sub>O -FOOTPRINTS IN THE
    SAND-](../Page/H2O_-FOOTPRINTS_IN_THE_SAND-.md "wikilink")(H<sub>2</sub>O
    -赤沙的印記-)
  - [IS〈Infinite Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")
  - [Kanon](../Page/Kanon.md "wikilink")(華音)
  - [K-ON！輕音部](../Page/K-ON！輕音部.md "wikilink")
  - [K-ON！！輕音部](../Page/K-ON！輕音部.md "wikilink")
  - [MADLAX](../Page/MADLAX.md "wikilink")(異域天使)
  - [MEMORIES](../Page/MEMORIES.md "wikilink")
  - [NOIR](../Page/Noir.md "wikilink")
  - [PERFECT BLUE](../Page/藍色恐懼.md "wikilink") 劇場版
  - [R.O.D -THE TV-](../Page/R.O.D_-THE_TV-.md "wikilink")
  - [School Days](../Page/School_Days.md "wikilink")
  - [TARI TARI](../Page/TARI_TARI.md "wikilink")
  - [×××HOLiC](../Page/×××HOLiC.md "wikilink")

### 一字部

  - [鴉 KARAS](../Page/鴉_KARAS.md "wikilink")

### 二字部

  - [黑貓](../Page/黑貓.md "wikilink")
  - [人狼](../Page/人狼.md "wikilink")
  - [亂馬1/2劇場版](../Page/亂馬1/2.md "wikilink")
  - [神薙](../Page/神薙.md "wikilink")
  - [日常](../Page/日常.md "wikilink")
  - [窮神](../Page/窮神_\(漫畫\)#.E9.9B.BB.E8.A6.96.E5.8B.95.E7.95.AB.md "wikilink")
  - [農林](../Page/農林.md "wikilink")

### 三字部

  - [櫻通信](../Page/櫻通信.md "wikilink")
  - [備長炭](../Page/備長炭.md "wikilink")
  - [現視研](../Page/現視研.md "wikilink") 系列
  - [捉迷藏KAKURENBO](../Page/捉迷藏KAKURENBO.md "wikilink")
  - [犬夜叉](../Page/犬夜叉.md "wikilink")（劇場版）
  - [老人Z](../Page/老人Z.md "wikilink")
  - [捍衛者](../Page/捍衛者.md "wikilink")
  - [逮捕令](../Page/逮捕令_\(動畫\).md "wikilink")
  - [星之聲](../Page/星之聲.md "wikilink")
  - [幸運☆星](../Page/幸運☆星.md "wikilink")
  - [食靈-零-](../Page/食靈.md "wikilink")
  - [化物語](../Page/化物語.md "wikilink")
  - [緣之空](../Page/緣之空.md "wikilink")
  - [七龍珠](../Page/七龍珠.md "wikilink") (劇場版)

### 四字部

  - [聖魔之血](../Page/聖魔之血.md "wikilink")
  - [狂砂小子](../Page/狂砂小子.md "wikilink")
  - [甜蜜聲優](../Page/甜蜜聲優.md "wikilink")
  - [極速攝殺](../Page/極速攝殺.md "wikilink")
  - [薔薇少女](../Page/薔薇少女.md "wikilink")
  - [曙光少女](../Page/曙光少女.md "wikilink")
  - [神槍少女](../Page/神槍少女.md "wikilink")
  - [文學少女](../Page/文學少女.md "wikilink") 劇場版
  - [奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink")
  - [勇往直前](../Page/飛越巔峰.md "wikilink")
  - [驚爆危機](../Page/驚爆危機.md "wikilink") 系列
  - [強襲魔女](../Page/強襲魔女.md "wikilink")
  - [幸運女神](../Page/幸運女神.md "wikilink")
  - [圓盤皇女](../Page/圓盤皇女.md "wikilink")
  - [銀河天使](../Page/銀河天使.md "wikilink")
  - [愛的魔法](../Page/愛的魔法.md "wikilink")
  - [女子高生](../Page/女子高生.md "wikilink")
  - [機械巨神](../Page/機械巨神.md "wikilink")
  - [相聚一刻](../Page/相聚一刻.md "wikilink")
  - [櫻花大戰](../Page/櫻花大戰.md "wikilink")
  - [真實夢境](../Page/真實夢境.md "wikilink")
  - [東京教父](../Page/東京教父.md "wikilink")
  - [機動警察](../Page/機動警察.md "wikilink")
  - [極速戰警](../Page/極速戰警.md "wikilink")
  - [福星小子](../Page/福星小子.md "wikilink")（OVA、劇場版）
  - [廢棄公主](../Page/廢棄公主.md "wikilink")
  - [翼神世音](../Page/翼神世音.md "wikilink")
  - [世界末日SPRIGGAN](../Page/轟天高校生.md "wikilink") 劇場版
  - [天地無用](../Page/天地無用.md "wikilink")
  - [天國之吻](../Page/天國之吻.md "wikilink")
  - [受讚頌者](../Page/受讚頌者.md "wikilink")
  - [藍色恐懼](../Page/藍色恐懼.md "wikilink")
  - [女王之刃](../Page/女王之刃.md "wikilink")
  - [天降之物](../Page/天降之物.md "wikilink") 系列
  - [奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink")
  - [空之境界](../Page/空之境界.md "wikilink") 劇場版
  - [玉子市場](../Page/玉子市場.md "wikilink")
  - [五星物語](../Page/五星物語.md "wikilink") 劇場版
  - [蘋果核戰](../Page/蘋果核戰.md "wikilink") APPLESEED
  - [蒸氣男孩](../Page/蒸氣男孩.md "wikilink") STEAMBOY
  - [日本鎖國 (電影)](../Page/日本锁国_\(电影\).md "wikilink")
  - [戀曲寫真](../Page/戀曲寫真.md "wikilink")
  - [千年女優](../Page/千年女優.md "wikilink") 劇場版
  - [結界女王Freezing](../Page/結界女王.md "wikilink")

### 五字部

  - [魔法美少女](../Page/魔法美少女.md "wikilink")
  - [魔力女管家](../Page/魔力女管家.md "wikilink")
  - [惑星大怪獸](../Page/惑星大怪獸.md "wikilink")
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")
  - [愛情泡泡糖](../Page/愛情泡泡糖.md "wikilink")
  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")
  - [海底兩萬哩](../Page/冒險少女娜汀亞.md "wikilink")
  - [攻殼機動隊](../Page/攻殼機動隊.md "wikilink")
  - [不平衡抽籤](../Page/不平衡抽籤.md "wikilink")
  - [新暗行御史](../Page/新暗行御史.md "wikilink")
  - [神無月巫女](../Page/神無月巫女.md "wikilink")
  - [神祕的世界](../Page/神祕的世界.md "wikilink")
  - [神樣中學生](../Page/神樣中學生.md "wikilink")
  - [真月譚月姬](../Page/真月譚月姬.md "wikilink")
  - [小公主優希](../Page/小公主優希.md "wikilink")
  - [庫洛魔法使](../Page/庫洛魔法使.md "wikilink")
  - [王立宇宙軍](../Page/王立宇宙軍.md "wikilink") 劇場版
  - [偶像防衛隊](../Page/偶像防衛隊.md "wikilink")
  - [青出於藍～緣～](../Page/青出於藍_\(動畫\).md "wikilink")
  - [星空的邂逅](../Page/星空的邂逅.md "wikilink")
  - [星空的邂逅2](../Page/拜託了雙子星.md "wikilink")
  - [海豚便利屋](../Page/海豚便利屋.md "wikilink")
  - [青空少女隊](../Page/青空少女隊.md "wikilink")
  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")
  - [科學小飛俠](../Page/科學小飛俠.md "wikilink") 劇場版
  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")
  - [秒速五公分](../Page/秒速五公分.md "wikilink")
  - [侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")
  - [境界的彼方](../Page/境界的彼方.md "wikilink")
  - [學園默示錄](../Page/學園默示錄.md "wikilink")
  - [古城荊棘王](../Page/古城荊棘王.md "wikilink") 劇場版
  - [少女與戰車](../Page/少女與戰車.md "wikilink")
  - [轉吧！企鵝罐](../Page/轉吧！企鵝罐.md "wikilink")

### 六字部

  - [最終兵器彼女](../Page/最終兵器彼女.md "wikilink")
  - [戰鬥妖精雪風](../Page/戰鬥妖精雪風.md "wikilink")
  - [戰鬥妖精少女](../Page/戰鬥妖精少女.md "wikilink")
  - [銀河冒險戰記](../Page/銀河冒險戰記.md "wikilink")
  - [吸血鬼獵人D](../Page/吸血鬼獵人D.md "wikilink")
  - [緋彈的亞莉亞](../Page/緋彈的亞莉亞.md "wikilink")
  - [要聽爸爸的話！](../Page/要聽爸爸的話！.md "wikilink")
  - [魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink") 劇場版
  - [鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink") BROTHERHOOD

### 七字部

  - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")
  - [涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")
  - [涼宮春日的消失](../Page/涼宮春日的消失.md "wikilink") (劇場版)
  - [時空轉抄納斯卡](../Page/時空轉抄納斯卡.md "wikilink")
  - [孤獨之城的優娜](../Page/孤獨之城的優娜.md "wikilink")
  - [我的主人愛作怪](../Page/我的主人愛作怪.md "wikilink")
  - [逐漸甦醒的記憶](../Page/逐漸甦醒的記憶.md "wikilink")
  - [你所期望的永遠](../Page/你所期望的永遠.md "wikilink")
  - [無敵鐵金剛凱撒](../Page/魔神皇帝.md "wikilink")
  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")
  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")
  - [無法掙脫的背叛](../Page/無法掙脫的背叛.md "wikilink")
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")
  - [殭屍哪有那麼萌？](../Page/殭屍哪有那麼萌？.md "wikilink")
  - [織田信奈的野望](../Page/織田信奈的野望.md "wikilink")
  - [在那個夏天等待](../Page/在那個夏天等待.md "wikilink")
  - [狼的孩子雨和雪](../Page/狼的孩子雨和雪.md "wikilink")
  - [跳躍吧！時空少女](../Page/跳躍吧！時空少女.md "wikilink")
  - [如果折斷她的旗](../Page/如果折斷她的旗.md "wikilink")

### 多字部

  - [我的太太是魔法少女](../Page/我的太太是魔法少女.md "wikilink")
  - [雲之彼端，約定的地方](../Page/雲之彼端，約定的地方.md "wikilink")
  - [茄子-行李箱的候鳥](../Page/茄子-行李箱的候鳥.md "wikilink")
  - [茄子-安達魯西亞之夏](../Page/茄子-安達魯西亞之夏.md "wikilink")
  - [鋼之鍊金術師 香巴拉的征服者](../Page/鋼之鍊金術師#電影.md "wikilink")
  - [超級機器人大戰O.G.](../Page/超級機器人大戰ORIGINAL_GENERATION_THE_ANIMATION.md "wikilink")（OVA）
  - [魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")
  - [魔法少女奈葉 The MOVIE
    1st](../Page/魔法少女奈葉_The_MOVIE_1st.md "wikilink")(劇場版)
  - [節哀唷♥二之宮同學](../Page/節哀唷♥二之宮同學.md "wikilink")
  - [京四郎與永遠的空](../Page/京四郎與永遠的空.md "wikilink")
  - [血戰：最後的吸血鬼](../Page/血戰：最後的吸血鬼.md "wikilink")
  - [機動戰士Z鋼彈劇場版](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [真無敵鐵金剛 衝擊\! Z篇](../Page/真魔神_衝擊！Z篇.md "wikilink")
  - [櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")
  - [就算是哥哥，有愛就沒問題了，對吧](../Page/就算是哥哥，有愛就沒問題了，對吧.md "wikilink")
  - [美少女死神 還我H之魂！](../Page/美少女死神_還我H之魂！.md "wikilink")
  - [迷茫管家與膽怯的我](../Page/迷茫管家與膽怯的我.md "wikilink")
  - [果然我的青春戀愛喜劇搞錯了。](../Page/果然我的青春戀愛喜劇搞錯了。.md "wikilink")
  - [鎖鎖美小姐＠不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")
  - [亞斯塔蘿黛的後宮玩具](../Page/亞斯塔蘿黛的後宮玩具.md "wikilink")
  - [吊帶襪天使Panty\&Stocking](../Page/吊帶襪天使.md "wikilink")
  - [道別的早晨就用約定之花點綴吧](../Page/於離別之朝束起約定之花.md "wikilink")

</div>

## 參考資料

## 外部連結

  - [普威爾國際](http://www.prowaremedia.com.tw/)

  - [銘成科技](http://www.nameware.com.tw/)

  -
  - ，普威爾公司登記負責人

  - ，銘成公司登記負責人

  - [普威爾國際](https://tv.line.me/proware) - [Line
    TV](../Page/Line_TV.md "wikilink")

  -
[P](../Category/臺灣電子遊戲公司.md "wikilink")
[P](../Category/1997年成立的公司.md "wikilink")
[P](../Category/動畫產業公司.md "wikilink")
[P](../Category/總部位於新北市的工商業機構.md "wikilink")