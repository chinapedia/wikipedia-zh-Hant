**普爾曼**（[英語](../Page/英語.md "wikilink")：**Pullman**）位於[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[惠特曼縣](../Page/惠特曼縣.md "wikilink")，是[華盛頓州立大學主校區的所在地](../Page/華盛頓州立大學.md "wikilink")。根據2000年人口普查，普爾曼總人口約為24,675人。
[WSU_BryanTower.jpg](https://zh.wikipedia.org/wiki/File:WSU_BryanTower.jpg "fig:WSU_BryanTower.jpg")

## 歷史

普爾曼於1886年建立，當時稱「三叉」（Three
Forks），人口只有250人。取名為三叉是因為那裡是三條河流的交匯處，到後來才以火車製造者[喬治·普爾曼來命名](../Page/喬治·普爾曼.md "wikilink")。1961年，普爾曼正式升級成為城市。

## 地區

普爾曼裡共有四座主要的山丘，這四座山把普爾曼分成四個區：

  - 西北軍事山（Military Hill）
  - 東南先峰山（Pioneer Hill）
  - 東北學院山（College Hill）
  - 西南陽光山（Sunnyside Hill）

此外，在城市的北部也有一個107公頃的高科技工業園。而普爾曼以東8[英哩則是姊妹城市](../Page/英哩.md "wikilink")[愛達荷州的](../Page/愛達荷州.md "wikilink")[莫斯科](../Page/莫斯科_\(愛達荷州\).md "wikilink")，那裡也是[愛達荷大學的所在地](../Page/愛達荷大學.md "wikilink")。

## 参考资料

[Category:华盛顿州城市](../Category/华盛顿州城市.md "wikilink")