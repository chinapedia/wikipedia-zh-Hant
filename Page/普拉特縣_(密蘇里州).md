**普拉特县**（**Platte County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州](../Page/密蘇里州.md "wikilink")，西北部的一個縣，西隔[密蘇里河與](../Page/密蘇里河.md "wikilink")[堪薩斯州相望](../Page/堪薩斯州.md "wikilink")。面積1,106平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口73,781人。縣治[普拉特城](../Page/普拉特城.md "wikilink")（Platte
City），最大城市[堪薩斯城](../Page/堪薩斯城_\(密蘇里州\).md "wikilink")。

成立於1838年12月31日，以紀念決定密蘇里州西北界的[普拉特購地](../Page/普拉特購地.md "wikilink")（源於[普拉特河](../Page/普拉特河.md "wikilink")）。

[P](../Category/密苏里州行政区划.md "wikilink")