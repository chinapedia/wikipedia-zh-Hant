**普拉蒂巴·帕蒂尔**（[馬拉地語](../Page/馬拉地語.md "wikilink")：**प्रतिभा
पाटिल**，[印地语](../Page/印地语.md "wikilink")：****，），[印度](../Page/印度.md "wikilink")[律师](../Page/律师.md "wikilink")、[政治家](../Page/政治家.md "wikilink")，[印度国大党成员](../Page/印度国大党.md "wikilink")，曾任[拉贾斯坦邦邦长](../Page/拉贾斯坦邦.md "wikilink")，前任[印度总统](../Page/印度总统.md "wikilink")。

普拉蒂巴·帕蒂尔，1934年12月19日出生于印度中部[马哈拉施特拉邦的一个检察官家庭](../Page/马哈拉施特拉邦.md "wikilink")。她早年毕业于[孟买的政府法律学院](../Page/孟买.md "wikilink")，获法律学士和艺术学硕士学位。1962年，帕蒂尔当选马哈拉施特拉邦议会议员，开始了从政生涯。1986年，她当选为议会联邦院（上院）副议长，1988年担任马哈拉施特拉邦的国大党主席，1991年当选为印度议会人民院（下院）议员。2004年，她出任印度拉贾斯坦邦邦长，成为印度历史上第一位女邦长。

2007年6月14日，帕蒂尔被推举为[印度总统候选人](../Page/印度总统.md "wikilink")。7月21日当选为印度首位女总统。

帕蒂尔的丈夫是一名教育家，也曾担任过议员和市长的职务，有一双儿女。

## 外部链接

  - [President of India Official Site](http://presidentofindia.nic.in/)

[分類:律師出身的總統](../Page/分類:律師出身的總統.md "wikilink")

[Category:印度总统](../Category/印度总统.md "wikilink")
[Category:女性總統](../Category/女性總統.md "wikilink")
[Category:拉賈斯坦邦邦長](../Category/拉賈斯坦邦邦長.md "wikilink")
[Category:印度女性政治人物](../Category/印度女性政治人物.md "wikilink")
[Category:印度國大黨黨員](../Category/印度國大黨黨員.md "wikilink")
[Category:印度律師](../Category/印度律師.md "wikilink")
[Category:女性律師](../Category/女性律師.md "wikilink")
[Category:馬哈拉施特拉邦人](../Category/馬哈拉施特拉邦人.md "wikilink")