[Max_planck.jpg](https://zh.wikipedia.org/wiki/File:Max_planck.jpg "fig:Max_planck.jpg")
在[物理學的](../Page/物理學.md "wikilink")[普朗克單位制裏](../Page/普朗克單位制.md "wikilink")，**普朗克時間**（Planck
time）是[時間的基本單位](../Page/時間.md "wikilink")，是[光波在](../Page/光波.md "wikilink")[真空裏傳播一個](../Page/真空.md "wikilink")[普朗克長度的距離所需的時間](../Page/普朗克長度.md "wikilink")。\[1\]普朗克單位制是一種[自然單位制](../Page/自然單位制.md "wikilink")，因[馬克斯·普朗克而得名](../Page/馬克斯·普朗克.md "wikilink")；普朗克最先提出普朗克單位制的概念。

普朗克時間\(t_P\)以方程式定義為\[2\]

\[t_P = \sqrt{\frac{\hbar G}{c^5}} \approx\] 5.39116(13) ×
10<sup>−44</sup>秒；

其中，\(\hbar=h/2\pi\)是[約化普朗克常數](../Page/約化普朗克常數.md "wikilink")，\(G\)是[引力常數](../Page/引力常數.md "wikilink")，\(c\)是光波傳播於真空的光速，在括號裏的兩個數字是估算值的[標準差](../Page/標準差.md "wikilink")。

## 物理重要性

普朗克時間是光波在真空裏傳播一個[普朗克長度的距離所需的時間](../Page/普朗克長度.md "wikilink")。\[3\]它的數值大約為
5×
10<sup>−44</sup>秒。理論而言，它是最小的可测時間間隔。\[4\]按照當今學術界所了解的物理定律，在這短暫時間間隔裏所發生的任何變化，是無法測量或探測求得。到2010年5月為止，直接測量的時間不確定性最小為12
[阿秒](../Page/原秒.md "wikilink")（1.2 × 10<sup>−17</sup>秒），約為3.7 ×
10<sup>26</sup>個普朗克時間。\[5\]

### 量綱分析

從[引力常數](../Page/引力常數.md "wikilink")、[相對論常數](../Page/光速.md "wikilink")、[量子常數的獨特組合可以得到單位為時間的常數](../Page/普朗克常數.md "wikilink")，即普朗克時間。[量綱分析是](../Page/量綱分析.md "wikilink")[數學物理的一門分支領域](../Page/數學物理.md "wikilink")，專門研究測量單位與物理常數，普朗克單位制是量綱分析的重要基礎結果，量綱分析建議，對於比普朗克時間更為短暫的時間間隔案例，量子力學與引力的效應都很重要，缺一不可，需要用到[量子引力理論](../Page/量子引力.md "wikilink")。

### 宇宙量子化

在[極早期宇宙](../Page/宇宙年表#極早期宇宙.md "wikilink")，光輻射是能量密度的主要成分。假設這時期的宇宙很平坦（曲率為零），只擁有光輻射，則從[弗里德曼方程式](../Page/弗里德曼方程式.md "wikilink")，可以推算出宇宙的能量密度\(\epsilon_r\)與時間\(t\)平方成反比：

\[\epsilon_r(t)\propto 1/t^2\]。

宇宙可以被視為一個[黑體](../Page/黑體.md "wikilink")，在這黑體裏，光輻射遵守[普朗克定律](../Page/普朗克定律.md "wikilink")，因此，可以計算出宇宙溫度\(T\)與時間的平方根成反比，每個光子的平均能量\(E_{mean}\)與時間的平方根成反比。從宇宙的能量密度\(\epsilon_r\)與光子的平均能量\(E_{mean}\)，可以得到光子的數量密度\(n\)與時間的關係為\[6\]

\[n(t)\propto t^{-3/2}\]。

隨著時間趨於零，能量密度\(\epsilon_r\)、平均能量\(E_{mean}\)、數量密度\(n\)都趨於無限大。但是，這些荒謬結果並不正確，因為推導出弗里德曼方程式的[廣義相對論是個經典理論](../Page/廣義相對論.md "wikilink")，廣義相對論假定宇宙能量在任何尺度都具有平滑連續性，不需要量子化。只要[可觀測宇宙內有很多的光子](../Page/可觀測宇宙.md "wikilink")，這假設成立；但是，當可觀測宇宙只含有很少數的光子之時，宇宙能量會呈離散值，因此必須將量子力學的效應納入考量。從[弗里德曼方程式](../Page/弗里德曼方程式.md "wikilink")，可以推算出，[宇宙視界](../Page/宇宙視界.md "wikilink")（cosmological
horizon）距離與時間成正比，宇宙視界體積與時間三方成正比。因此，可觀測宇宙的光子數量與時間的關係為

\[N(t)\propto t^{3/2}\]。

更精確地計算，可以得到

\[N(t)\approx 1.9\left(\frac{t}{t_P}\right)^{3/2}\]；

其中，\(t_P\)是普朗克時間。

所以，在大爆炸之後，當時間到达\(t\approx 0.7t_P\)時，在任意可觀測宇宙內，只存在有1個光子，這時，不能忽略能量的量子化，必需發展與採用[量子引力理論](../Page/量子引力.md "wikilink")。\[7\]

### 超遙遠星體

從分析[哈勃空間望遠鏡在](../Page/哈勃空間望遠鏡.md "wikilink")2003年拍攝的[哈勃超深空影像](../Page/哈勃超深空.md "wikilink")，引起一場辯論，其主要論題是普朗克時間為最短暫時間間隔會產生的天文學效應。有些天文學者提議，由於普朗克尺度的時空漲落，極具猜想性質的[量子引力泡沫理論預測超遙遠星體應該會顯得模糊不清](../Page/量子泡沫.md "wikilink")。\[8\]可是，哈伯影像顯得相當清晰，因此很多學者對這提議產生質疑，\[9\]有些學者爭論，這提議高估了模糊效應10<sup>15</sup>至10<sup>30</sup>倍，因此，觀測到的效應不能有效限制理論："在某些時空泡沫理論裏，時空漲落對於光波的相位干涉所產生的累積效應非常小，無法被觀測到。"\[10\]

## 相關條目

  - [普朗克時期](../Page/普朗克時期.md "wikilink")

## 参考文献

## 外部連結

  - [Shortest time interval
    measured](http://news.bbc.co.uk/2/hi/science/nature/3486160.stm) -
    BBC新聞，關於2004年測量到最短時間間隔的事件。

[Category:时间单位](../Category/时间单位.md "wikilink")
[Category:自然單位](../Category/自然單位.md "wikilink")
[Category:物理常數](../Category/物理常數.md "wikilink")

1.

2.  [CODATA Value: Planck
    Time](http://physics.nist.gov/cgi-bin/cuu/Value?plkt) – The
    [NIST](../Page/NIST.md "wikilink") Reference on Constants, Units,
    and Uncertainty.

3.
4.

5.

6.

7.

8.

9.

10.