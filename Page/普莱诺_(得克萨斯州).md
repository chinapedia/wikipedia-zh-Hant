**布蘭諾**（**Plano**）是位于[美国](../Page/美国.md "wikilink")[得克萨斯州](../Page/得克萨斯州.md "wikilink")[科林县和](../Page/科林县_\(得克萨斯州\).md "wikilink")[登顿县的一座城市](../Page/登顿县_\(得克萨斯州\).md "wikilink")，人口约26万（2010年），居得克萨斯州第9位。布蘭諾位于达拉斯-沃斯堡城市群，是世界上很多大公司的总部，如[爱立信](../Page/爱立信.md "wikilink")(北美),
[J. C. Penney](../Page/J._C._Penney.md "wikilink")
([EN](../Page/:en:J._C._Penney.md "wikilink"))，[必胜客等](../Page/必胜客.md "wikilink")。

布蘭諾被认为是美国西部最适宜人居的城市之一。2008年，[美国人口调查局通过比较全国人口超过](../Page/美国人口调查局.md "wikilink")25万的城市的家庭收入中位数，将布蘭諾列为美国最富有的城市。2010和2011年，[福布斯杂志连续两年将布蘭諾列为美国人口超过](../Page/福布斯.md "wikilink")25万的城市中最安全的城市。

[布蘭諾独立學區经营学校](../Page/布蘭諾独立學區.md "wikilink")。
[科林学院经营](../Page/科林学院.md "wikilink")[社区学院](../Page/社区学院.md "wikilink")。

华裔中文学校(HuaYi Education) 是布蘭諾的中文学校。\[1\]\[2\]

## 参考资料

## 外部链接

  - [布蘭諾](https://web.archive.org/web/20111017155056/http://www.plano.gov/Pages/default.aspx)


[P](../Category/得克萨斯州城市.md "wikilink")

1.  Light, Nanette. "[Editor's Note: Plano program connects kids to
    Chinese
    culture](http://www.dallasnews.com/news/community-news/plano/headlines/20131005-editor-s-note-plano-program-connects-kids-to-chinese-culture.ece?nclick_check=1)."
    *Neighbors Go Plano*. *[The Dallas Morning
    News](../Page/The_Dallas_Morning_News.md "wikilink")*. Published
    October 5, 2013. Updated October 4, 2013. Retrieved on October 29,
    2013.
2.  [Home page](http://www.huayieducation.com/)
    ([Archive](http://archive.is/xKUnA)). HuaYi Education. Retrieved on
    October 29, 2013.