**普通教育高级程度证书**（，简称**GCE
A-Level**或**A-Level**）是学生完成12-13年级的两年制大学[预科的学业后](../Page/预科.md "wikilink")，所取得的由[英国](../Page/英国.md "wikilink")（不包括[苏格兰](../Page/苏格兰.md "wikilink")）或英国海外属地教育部颁发的的普通[中等教育文凭](../Page/中等教育.md "wikilink")，在修读完[中等教育普通證書之后参加](../Page/中等教育普通證書.md "wikilink")，供16-19岁的学生修读，通常为期两年。A-Level在1951年首次推出，用以取代以前的[高等中学毕业会考](../Page/高等中学毕业会考.md "wikilink")（HSC）。完成A-Level学业之后，通常会得到GCE
A-Level证书。

A-Level中文译作“**高级程度**”。在[新加坡](../Page/新加坡.md "wikilink")，有时候也写成*' 'A'
Level*'（中文译成“'A'水准”）。而香港地區正式法定中文名稱為香港高級程度會考。

A-Level是源于英国的通過[考試而取得畢業證書的制度](../Page/考試.md "wikilink")。英国以外的其他一些国家地区，如[新加坡](../Page/新加坡.md "wikilink")、[斯里兰卡和](../Page/斯里兰卡.md "wikilink")[香港](../Page/香港.md "wikilink")，也有A-Level学制，但不一定使用[GCE的名称](../Page/GCE.md "wikilink")。[新加坡](../Page/新加坡.md "wikilink")、[马耳他](../Page/马耳他.md "wikilink")、[南非也会举行同名的考试](../Page/南非.md "wikilink")，但由于教育制度已经发生改变，在这些地方的A-level教育通常已经在风格、系统上有了较大转变，[香港的](../Page/香港.md "wikilink")[HKALE也没有称为GCE](../Page/HKALE.md "wikilink")，而作Hong
Kong Advanced Level Examination 香港高級程度會考。

现在A-Level资格在全球许多非英属地区也可以获得，由于它在全球具有普遍的认可性，使之成为很多国际学校教育体系的选择。[中国大陆很多高中也正在和英国展开](../Page/中国大陆.md "wikilink")2+2的合作。

## 目前使用地區

許多國家使用 A Level 作為離校的資格證明。在英國以及部分國家使用的 A Levels 不完全相同。

### 英國

英國的[A Levels](../Page/普通教育高級程度證書_英國.md "wikilink")
為[英格蘭](../Page/英格蘭.md "wikilink")，[威爾斯](../Page/威爾斯.md "wikilink")，[北愛爾蘭結束中學時期的資格證明](../Page/北愛爾蘭.md "wikilink")。在[蘇格蘭地區](../Page/蘇格蘭.md "wikilink")，主要使用蘇格蘭的[進階高級程度證書](../Page/進階高級程度證書.md "wikilink")(Advanced
Higher)，但也有部分學校使用 A Level。在英國有五個主要的 A Level 試驗委員會，如下所示：

  - 評估與資格聯盟([Assessment and Qualifications
    Alliance](../Page/Assessment_and_Qualifications_Alliance.md "wikilink"))
    (AQA)
  - 牛津，康橋與 RSA 測驗([Oxford, Cambridge and RSA
    Examinations](../Page/Oxford,_Cambridge_and_RSA_Examinations.md "wikilink"))
    (OCR)
  - 艾德斯([Edexcel](../Page/Edexcel.md "wikilink")) (艾德斯‧皮爾森 -
    倫敦試驗(Edexcel Pearson - London Examinations))
  - 威爾斯聯合教育委員會([Welsh Joint Education
    Committee](../Page/Welsh_Joint_Education_Committee.md "wikilink"))
    (WJEC)
  - 課綱，試驗與評估委員會([Council for the Curriculum, Examinations &
    Assessment](../Page/Council_for_the_Curriculum,_Examinations_&_Assessment.md "wikilink"))
    (CCEA)

艾德斯以及[劍橋大學考試委員會國際考試部](../Page/劍橋大學考試委員會國際考試部.md "wikilink")(Cambridge
International Examinations) (CIE) 也提供英國本土以及世界各地的國際版的英國 A Levels 。

英國版的 A/AS levels
也可以在許多[大英國協或是前大英國協的會員國報名參與](../Page/大英國協.md "wikilink")。英國在國外的[國際學校通常提供舉辦](../Page/國際學校.md "wikilink")「艾德斯」(Edexcel)或是劍橋國際測驗(Cambridge
International Examinations)。

### 尼泊爾

在[尼泊爾](../Page/尼泊爾.md "wikilink")，通常由部分私立學校，公立或是國際學校提供[劍橋大學考試委員會國際考試部的](../Page/劍橋大學考試委員會國際考試部.md "wikilink")
A Level 測驗，為尼泊爾教育部門[Higher Secondary Education
Board](http://www.hseb.edu.np) (HSEB) +2 接受。

A-level 在尼泊爾為不少學生的選擇。某些學校也提供 GCE A-level 的課程，包括 [Budhanilkantha
School](../Page/Budhanilkantha_School.md "wikilink"), [Rato Bangala
School](../Page/Rato_Bangala_School.md "wikilink"), [Global College
International](../Page/Global_College_International.md "wikilink"), [St.
Xavier's College,
Maitighar](../Page/St._Xavier's_College,_Maitighar.md "wikilink"),
[Graded English Medium
School](../Page/Graded_English_Medium_School.md "wikilink") (GEMS),
[Little Angels' School](../Page/Little_Angels'_School.md "wikilink")
(LAS), [Chelsea International
Academy](../Page/Chelsea_International_Academy.md "wikilink"),
[Kathmandu University High
School](../Page/Kathmandu_University_High_School.md "wikilink"), Malpi
Institute, Butuwal Public School, [Gandaki Boarding
School](../Page/Gandaki_Boarding_School.md "wikilink") [Saipal
Academy](http://www.saipal.edu.np), 以及 Andrew J. Wild (AJW) college 等學校。

### 巴基斯坦

[巴基斯坦的非政府單位](../Page/巴基斯坦.md "wikilink")，私立機構，[國際文憑組織](../Page/國際文憑組織.md "wikilink")
等也提供 A-levels 試驗。巴基斯坦的 A-levels
試驗由國際性考試委員會，如艾德斯(Edexcel)主導，而課程相當於巴基斯坦的「高中畢業考試」(Higher
Secondary School Certificate)。在巴基斯坦，A-level
也可以在私立學校接受課程。許多學校也會提供相關的課程供學生作為另一項選擇。某些國際型教育機構也會提供
A-level 課程，包括 [Army Burn Hall
College](../Page/Army_Burn_Hall_College.md "wikilink")
[Abbottabad](../Page/Abbottabad.md "wikilink") [The City
School](../Page/The_City_School_\(Pakistan\).md "wikilink"), [Garrison
Academy for Cambridge
Studies](../Page/Garrison_Academy_for_Cambridge_Studies.md "wikilink"),
[Divisional Model College
Faisalabad](../Page/Divisional_Model_College_Faisalabad.md "wikilink")
[Beaconhouse School
System](../Page/Beaconhouse_School_System.md "wikilink"), LACAS (Lahore
College of Arts & Sciences), Lahore Grammar School 以及 [Fazaia Inter
School System](../Page/Fazaia_Inter_School_System.md "wikilink") 和其他學校，
Karachi Grammar School，Southshore School for A level
studies，Generation's School, Bahria College, St. Patrick's High
School,Cadet College Jhang, St. Michael\`s Convent School, The Fahim's
School, Chand Bagh School 以及 The Roots School System。

### 汶萊

在[汶萊](../Page/汶萊.md "wikilink")，由[劍橋大學考試委員會國際考試部提供](../Page/劍橋大學考試委員會國際考試部.md "wikilink")
A Level 考試測驗。某些科目或是課綱為汶萊特有的考試項目。

### 馬來西亞

在[馬來西亞](../Page/馬來西亞.md "wikilink")，由[劍橋大學考試委員會國際考試部提供](../Page/劍橋大學考試委員會國際考試部.md "wikilink")
A Level 考試測驗。某些科目或是課綱為馬來西亞特有的考試項目。舉例來說，在馬來西亞有兩種 A Level 測驗，劍橋 A Level
(由劍橋大學考試委員會國際考試部主辦) 以及艾德斯國際 A-Level (由艾德斯國際主辦)。馬來西亞某些私立大學提供 A Level
課程考試：\[1\]

  - Brickfields Asia College (BAC)
  - [HELP University](../Page/HELP_University.md "wikilink")
  - [INTI International
    University](../Page/INTI_International_University.md "wikilink")
  - KBU International College
  - [KDU University
    College](../Page/KDU_University_College.md "wikilink")
  - Methodist College Kuala Lumpur (MCKL)
  - [SEGi University](../Page/SEGi_University.md "wikilink")
  - [Sunway College](../Page/Sunway_College.md "wikilink")
  - [Taylor's College](../Page/Taylor's_College.md "wikilink")
  - UCSI University

### 模里西斯

[模里西斯](../Page/模里西斯.md "wikilink")， A/AS Level
證書為模里西斯的「高等中學畢業考試」([Higher School
Certificate](../Page/Higher_School_Certificate_\(Mauritius\).md "wikilink"))的一部份，學生必須接受「模里西斯考試」(Mauritius
Examinations
Syndicate)以及[劍橋大學考試委員會的考試都通過後方可畢業](../Page/劍橋大學考試委員會.md "wikilink")。考試提供多種語言，包括[法語](../Page/法語.md "wikilink")，並且與國家教育部門合作訂定標準的考試項目。除此之外，國際艾德斯
A-Level 也可使用，一樣要透過模里西斯的考試委員會報名。

### 塞席爾

在[塞席爾](../Page/塞席爾.md "wikilink")，由[劍橋大學考試委員會國際考試部提供](../Page/劍橋大學考試委員會國際考試部.md "wikilink")
A Level 考試測驗。某些科目或是課綱為塞席爾特有的考試項目。

### 澳門

在[澳門](../Page/澳門.md "wikilink")，大多由非公立學校或國際學校提供A Level考試。

  - [澳門培正中學](../Page/澳門培正中學.md "wikilink")
  - [澳門聖公會中學](../Page/澳門聖公會中學.md "wikilink")
  - [陳瑞祺永援中學](../Page/陳瑞祺永援中學.md "wikilink")
  - [澳門粵華中學](../Page/澳門粵華中學.md "wikilink")
  - [澳門慈幼中學](../Page/慈幼中學.md "wikilink")

### 新加坡

在[新加坡](../Page/新加坡.md "wikilink")，由[新加坡教育部](../Page/新加坡教育部.md "wikilink")、[新加坡考試與評鑑局](../Page/新加坡考試與評鑑局.md "wikilink")(Singapore
Examinations and Assessment
Board)以及[劍橋大學考試委員會國際考試部共同舉辦](../Page/劍橋大學考試委員會國際考試部.md "wikilink")[新加坡-劍橋普通教育證書高級水準](../Page/新加坡-劍橋普通教育證書高級水準.md "wikilink")。

### 斯里蘭卡

在[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")，由公立以及非公立學校提供 A Level 試驗。大部分為本地
A-Level ，而私校提供倫敦 A Levels。本地 A-Level 試驗由斯里蘭卡的考試部門([Department of
Examinations](../Page/Department_of_Examinations.md "wikilink"))提供。申請斯里蘭卡大學通常會要求通過
A Levels 。

### 印度

在[印度](../Page/印度.md "wikilink")，部分私立學校以及國際學校提供[劍橋大學考試委員會國際考試部的](../Page/劍橋大學考試委員會國際考試部.md "wikilink")
A-Level 試驗，作為取代「印度學校畢業資格」(Indian School Certificate)(ISC)
的另一項選擇。\[2\]\[3\]

### 辛巴威

在[辛巴威](../Page/辛巴威.md "wikilink")，由辛巴威考試委員會(Zimbabwe School Examinations
Council)(ZIMSEC)舉辦 A-Level 試驗。\[4\]
在此之前，為考試委員會與[劍橋大學考試委員會國際考試部合作舉辦](../Page/劍橋大學考試委員會國際考試部.md "wikilink")。辛巴威版的
A-Level 考試被認為比英國版還更具挑戰性。

## 前使用地區

### 加勒比地區

[加勒比地區國家脫離使用](../Page/加勒比地區.md "wikilink")
A-Level，改使用加勒比聯合考試委員會([CXC](../Page/Caribbean_Examinations_Council.md "wikilink"))
舉辦的「加勒比高級程度試驗」(Caribbean Advanced Proficiency Examinations)(CAPE)。\[5\]
並且成為一個正式的升大學考試。不過，某些大學也接受[國際文憑組織或是](../Page/國際文憑組織.md "wikilink")[歐洲文憑組織](../Page/歐洲文憑組織.md "wikilink")(European
Baccalaureate)的成績。

### 香港

在[香港](../Page/香港.md "wikilink")，以往中學生完成兩年預科後應考[香港高級程度會考](../Page/香港高級程度會考.md "wikilink")，不過由於[中學學制改革](../Page/三三四高中教育改革.md "wikilink")，於2013年舉辦最後一屆，之後與[香港中學會考一同被](../Page/香港中學會考.md "wikilink")2012年起舉辦的[香港中學文憑](../Page/香港中學文憑.md "wikilink")（HKDSE）考試取代。

## 参见

  - [普通教育证书](../Page/普通教育证书.md "wikilink")
  - [中等教育普通證書](../Page/中等教育普通證書.md "wikilink")
  - [新加坡劍橋普通教育證書（高級水平）會考](../Page/新加坡劍橋普通教育證書（高級水平）會考.md "wikilink")
  - [香港高級程度會考](../Page/香港高級程度會考.md "wikilink")

## 參考資料

[Category:英國教育](../Category/英國教育.md "wikilink")
[Category:考試](../Category/考試.md "wikilink")

1.
2.
3.
4.
5.  [*Caribbean Examinations Council
    Report*](http://www.cxc.org/section.asp?Sec=2&SSec=1&Info=7) .
    Reforming the Examination System. House of Commons, 26 March 2003.
    Retrieved 12 June 2006.