**普雷德拉格·米亚托维奇**（**Predrag "Peđa"
Mijatović**，，），[黑山前足球運動員](../Page/黑山.md "wikilink")，司職中鋒。

## 生平

米積杜域是1990年代南斯拉夫最優秀的球員之一，以優秀球技聞名。他早年是在一些南斯拉夫小型球會打起，至1993年轉投[西班牙球會](../Page/西班牙.md "wikilink")[瓦倫西亞](../Page/瓦倫西亞足球俱樂部.md "wikilink")。打了三季後，於1996年投效[皇家馬德里](../Page/皇家馬德里.md "wikilink")。許多皇馬球迷都清楚米積杜域曾於1998年[歐洲聯賽冠軍杯決賽對](../Page/歐洲聯賽冠軍杯.md "wikilink")[意甲的](../Page/意甲.md "wikilink")[祖雲達斯時](../Page/祖雲達斯.md "wikilink")，他取得了全場唯一的[越位入球](../Page/越位_\(足球\).md "wikilink")，助皇馬贏得自1966年以後，另一個歐冠杯獎牌。

國家隊方面他則代表過國家隊出戰[1998年世界杯和](../Page/1998年世界杯.md "wikilink")2000年[歐洲國家杯](../Page/歐洲國家杯.md "wikilink")。由於國家連年內戰，南斯拉夫無法參與多項大型賽事，包括1992年和1996年歐洲國家杯，以及[1994年世界杯](../Page/1994年世界杯.md "wikilink")。而米積杜域也就未有機會在國際賽大顯身手。

1999年他轉投[意甲的](../Page/意甲.md "wikilink")[費倫天拿](../Page/費倫天拿.md "wikilink")，惟表現已不如以往，至2002年費倫天拿因破產而被罰降級後，米積杜域重返西班牙效力小型球會利雲特，一季後退役。

2006年至2009年間，他是[西甲豪門](../Page/西甲.md "wikilink")[皇家馬德里的體育總監](../Page/皇家馬德里.md "wikilink")，期間一系列的轉會操作惹來爭議。

## 參考文獻

[Category:南斯拉夫足球运动员](../Category/南斯拉夫足球运动员.md "wikilink")
[Category:黑山足球運動員](../Category/黑山足球運動員.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:皇家馬德里球員](../Category/皇家馬德里球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:利雲特球員](../Category/利雲特球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:柏迪遜球員](../Category/柏迪遜球員.md "wikilink")
[Category:樸高利卡球員](../Category/樸高利卡球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:黑山足球主教練](../Category/黑山足球主教練.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")