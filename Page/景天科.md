[Crassula_capitella_2_edit.jpg](https://zh.wikipedia.org/wiki/File:Crassula_capitella_2_edit.jpg "fig:Crassula_capitella_2_edit.jpg")（*Crassula
capitella*）\]\]
**景天科**，是[虎耳草目的一](../Page/虎耳草目.md "wikilink")[科](../Page/科.md "wikilink")，为多年生草本或低矮灌木。原产于世界温暖干燥地区，分布在北半球大部分区域，品种繁多，大约有34[属](../Page/属.md "wikilink")1426[種](../Page/種.md "wikilink")，[中国有](../Page/中国.md "wikilink")13属（2[特有屬](../Page/特有種.md "wikilink")，一[引進屬](../Page/栽培種.md "wikilink")），约233種（129[特有種](../Page/特有種.md "wikilink")，1[引進種](../Page/栽培種.md "wikilink")），\[1\]\[2\]，另有多种引进作为观赏[花卉](../Page/花卉.md "wikilink")。

景天科最初在1930年被Berger描述時包括有六個亞科\[3\]，分別是：

  - [青鎖龍亞科](../Page/青鎖龍亞科.md "wikilink") Crassuloideae
  - [伽藍菜亞科](../Page/伽藍菜亞科.md "wikilink") Kalanchoideae
  - [銀波錦亞科](../Page/銀波錦亞科.md "wikilink") Cotyledonoideae
  - [擬石蓮亞科](../Page/擬石蓮亞科.md "wikilink") Echeverioideae
  - [佛甲草亞科](../Page/佛甲草亞科.md "wikilink") Sedoideae
  - [長生草亞科](../Page/長生草亞科.md "wikilink") Sempervivoideae

其後這個分類經過多次修訂，之下的亞科從兩個到四個不等，但這六個亞科的分類仍然是最常用的分類\[4\]，儘管這些亞科有好些都是多系群。另一個比較常用的分類減至下列三個亞科：

  - [青鎖龍亞科](../Page/青鎖龍亞科.md "wikilink") Crassuloideae
  - [伽藍菜亞科](../Page/伽藍菜亞科.md "wikilink") Kalanchoideae
  - [長生草亞科](../Page/長生草亞科.md "wikilink") Sempervivoideae

## 形態

本[科植物为多年生肉质草本](../Page/科.md "wikilink")，夏秋季开[花](../Page/花.md "wikilink")，花小而繁茂，各种[颜色都有](../Page/颜色.md "wikilink")。表皮腊质厚，气孔下陷，可减少[蒸腾](../Page/蒸腾作用.md "wikilink")([蒸散作用](../Page/蒸散作用.md "wikilink"))，是典型的旱生[植物](../Page/植物.md "wikilink")，无性繁殖力强，所谓「採[葉即能種植生](../Page/葉.md "wikilink")[根](../Page/根.md "wikilink")」（[落地生根](../Page/落地生根.md "wikilink")）。

許多景天科植物被報導為[CAM植物](../Page/景天酸代謝.md "wikilink")（如[长寿花](../Page/长寿花.md "wikilink")），指其[光合作用的](../Page/光合作用.md "wikilink")[代謝途徑與一般的](../Page/代謝.md "wikilink")[C3类植物](../Page/光合作用#C3类植物.md "wikilink")、[C4类植物不同](../Page/C4類二氧化碳固定.md "wikilink")。

## 演化

景天科起源[非洲南部](../Page/非洲.md "wikilink")，隨後向北擴散到[地中海地區](../Page/地中海.md "wikilink")，再蔓延至[亞洲](../Page/亞洲.md "wikilink")、[東歐和](../Page/東歐.md "wikilink")[北歐](../Page/北歐.md "wikilink")，最後擴展至[北美](../Page/北美.md "wikilink")。\[5\]

## 长生草亚科 Sempervivoideae

\=== 八宝族 Telephieae (= Hylotelephium clade) ===

  - [石莲属](../Page/石莲属.md "wikilink") *Sinocrassula* <small>A.
    Berger</small>

  - [孔岩草属](../Page/孔岩草属.md "wikilink") *Kungia* <small>K. T. Fu</small>

  - [四国毛花属](../Page/四国毛花属.md "wikilink") *Meterostachys*
    <small>Nakai</small>

  - [瓦松属](../Page/瓦松属.md "wikilink") *Orostachys* <small>(DC.)
    Fisch.</small>

  - [八宝属](../Page/八宝属.md "wikilink") *Hylotelephium* <small>H.
    Ohba</small>

  - *Perrierosedum* <small>(A. Berger) H. Ohba</small>

\=== 脐景天族 Umbiliceae (=Rhodiola clade) ===

  - *Umbilicus* <small>DC. </small>

  - [六瓣景天属](../Page/六瓣景天属.md "wikilink")／合景天属／假景天属 *Pseudosedum*
    <small>(Boiss.) A. Berger</small>

  - [红景天属](../Page/红景天属.md "wikilink") *Rhodiola* <small>L.</small>

  - *Phedimus* <small>Raf.</small>

\=== 长生草族 Semperviveae (=Sempervivum clade) ===

  - [长生草属](../Page/长生草属.md "wikilink") *Sempervivum* <small>L.</small>

  - *Petrosedum* <small>Grulich</small>

\=== 莲花掌族 Aeonieae (= Aeonium clade) ===

  - ／愛染草屬 *Aichryson* <small>Webb & Berthel.</small>

  - ／单花景天属 *Monanthes* <small>Haw.</small>

  - [莲花掌属](../Page/莲花掌属.md "wikilink") Aeonium <small>Webb &
    Berthel.</small>

### 景天族Sedeae

#### Leucosedum clade

  - ／ 猫爪景天 *Pistorinia* <small>DC.</small>

  - [瓦莲属](../Page/瓦莲属.md "wikilink") *Rosularia* <small>(DC.)
    Stapf</small>

  - <small>(A. Berger) Ohba</small>

  - *Sedella* <small>Britton & Rose</small>

  - *Dudleya* <small>Britton & Rose</small>

#### Acre clade

  - [景天屬](../Page/景天屬.md "wikilink") *Sedum* <small>L.</small>

  - *Villadia* <small>Rose</small>

  - ／纱罗属 *Lenophyllum* <small>Rose</small>

  - *Graptopetalum* <small>Rose</small>

  - 　*Thompsonella* <small>Britton & Rose</small>

  - [拟石莲花属](../Page/拟石莲花属.md "wikilink") *Echeveria* <small>DC.</small>

  - *Pachyphytum* <small>Link, Klotzsch & Otto</small>

\== 伽蓝菜亚科 Kalanchoideae(=Kalanchoe clade) ==

  - *Adromischus* <small>Lemaire</small>

  - ／绒叶景天属 *Cotyledon* <small>Adans.</small>

  - [伽蓝菜属](../Page/伽蓝菜属.md "wikilink")／燈籠草属 *Kalanchoe*
    <small>Toelken</small>

  - *Tylecodon* <small>Tourn. ex L.</small>

\== 青锁龙亚科 Crassuloideae (=Crassula Glade) ==

  - [青锁龙屬](../Page/青锁龙屬.md "wikilink")／肉葉草屬 *Crassula* <small>L.</small>

## 應用

景天科植物植株矮小，由于是肉质，耗水肥很少，因此极易种植观赏。景天科植物由于其矮小抗风，又不需要大量水肥，耐污染，因此成为目前比较流行的屋顶绿化的首选植物。

## 注釋

<references />

## 外部链接

  - [Crassulaceae](http://www.mobot.org/MOBOT/Research/APWeb/orders/Saxifragalesweb.htm#Crassulaceae)
    in Stevens, P. F. (2001 onwards). [Angiosperm Phylogeny Website.
    Version 9](http://www.mobot.org/MOBOT/Research/APWeb/), June 2008
    \[and more or less continuously updated since\].
  - [Crassulaceae](http://delta-intkey.com/angio/www/crassula.htm) in L.
    Watson and M.J. Dallwitz (1992 onwards). [The families of flowering
    plants](http://delta-intkey.com)：descriptions, illustrations,
    identification, and information retrieval.

[\*](../Category/景天科.md "wikilink")

1.  [Flora of
    China 8: 202–268. 2001.](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=10225)
2.  [Flora of
    China 8: 202–268. 2001.中國植物志所收錄的屬](http://www.efloras.org/browse.aspx?flora_id=2&start_taxon_id=10225)
3.
4.
5.  [Phylogenetic relationships and evolution of Crassulaceae inferred
    from matK sequence
    data1](http://www.amjbot.org/content/88/1/76.full)