[Pineapple1.JPG](https://zh.wikipedia.org/wiki/File:Pineapple1.JPG "fig:Pineapple1.JPG")
**景天酸代謝**（Crassulacean acid metabolism，簡稱
**CAM**）是部分植物的一种精巧的碳固定方法。代表性的植物有[仙人掌](../Page/仙人掌.md "wikilink")、[凤梨和](../Page/凤梨.md "wikilink")[长寿花等](../Page/长寿花.md "wikilink")。

要在干旱热带地区生存下来，CAM植物演化出一套生存機制，[二氧化碳的固定将于](../Page/二氧化碳.md "wikilink")[卡尔文循环在时间上分开](../Page/卡尔文循环.md "wikilink")。这样就可以避免水分过快的流失，因为[气孔只在夜间开放以摄取二氧化碳](../Page/气孔.md "wikilink")。

纯粹的C4类植物对二氧化碳固定实行的是*空间分离*（通过两种细胞类型实现，[叶肉细胞和](../Page/叶肉细胞.md "wikilink")[维管束鞘细胞](../Page/维管束鞘细胞.md "wikilink")）。而景天酸代謝植物则服从以下昼夜节律：

  - 晚上：二氧化碳吸收和固定于[磷酸烯醇式丙酮酸](../Page/磷酸烯醇式丙酮酸.md "wikilink")（PEP）。生成的[草酰乙酸](../Page/草酰乙酸.md "wikilink")（OAA）会被还原为[苹果酸](../Page/苹果酸.md "wikilink")，并储存于细胞的液泡中。该过程中伴随有酸化，[pH值降低在日间光反应里产生的还原物质也会在这里发挥作用](../Page/pH值.md "wikilink")。
  - 日间：在[液泡里的酸性物质](../Page/液泡.md "wikilink")（主要是[苹果酸](../Page/苹果酸.md "wikilink")，但也有[天门冬氨酸](../Page/天门冬氨酸.md "wikilink")）会被脱羧。释放的二氧化碳进入[卡尔文循环](../Page/卡尔文循环.md "wikilink")。

景天酸代謝植物必须准备足够的[磷酸烯醇式丙酮酸以供夜间二氧化碳固定使用](../Page/磷酸烯醇式丙酮酸.md "wikilink")。为此植物在日间储存[淀粉](../Page/淀粉.md "wikilink")，晚间它们将通过[丙酮酸转变为](../Page/丙酮酸.md "wikilink")[磷酸烯醇式丙酮酸](../Page/磷酸烯醇式丙酮酸.md "wikilink")。

[CAM.png](https://zh.wikipedia.org/wiki/File:CAM.png "fig:CAM.png")

## 证明

可以通过两个实验验证，某种植物是否为景天酸代謝植物：

1\. 液泡液在晚间呈酸性。可用酸碱指示剂测得。

2\.
[凤梨科植物的](../Page/凤梨科.md "wikilink")[气孔的早晚开闭也可以用实验的方法观察到](../Page/气孔.md "wikilink")：在叶的下表面涂上两层透明的指甲油-
大概1平方厘米 -
然后用显微镜观察。如果[气孔晚间开日间关](../Page/气孔.md "wikilink")，则可以推断该植物为景天酸代謝植物，但还需要进一步证明去确定。

## CAM植物例子

  - [百歲蘭](../Page/百歲蘭.md "wikilink")
  - [澤米科](../Page/澤米科.md "wikilink") (只有Dioon edule)
  - [蘭科部分物種](../Page/蘭科.md "wikilink")
    (如[蝴蝶蘭](../Page/蝴蝶蘭.md "wikilink"))
  - [龍舌蘭科部分物種](../Page/龍舌蘭科.md "wikilink")
    (如[劍麻](../Page/劍麻.md "wikilink"))
  - [菊科部分物種](../Page/菊科.md "wikilink")
    (如[千里光](../Page/千里光.md "wikilink"))
  - [馬齒莧科部分物種](../Page/馬齒莧科.md "wikilink")
    (如[馬齒莧](../Page/馬齒莧.md "wikilink"))
  - [阿福花科部分物種](../Page/阿福花科.md "wikilink")(如[蘆薈屬](../Page/蘆薈屬.md "wikilink"))
  - [大戟科部分物種](../Page/大戟科.md "wikilink")
  - [鳳梨科大部分物種](../Page/鳳梨科.md "wikilink")
  - [景天科大部分物種](../Page/景天科.md "wikilink")
  - [番杏科大部分物種](../Page/番杏科.md "wikilink")
  - [仙人掌科所有物種](../Page/仙人掌科.md "wikilink")

## 另見

  - [C4类植物](../Page/C4类植物.md "wikilink")

## 外部链接

  - [C3-, C4- 和 CAM植物比较](http://www.egbeck.de/skripten/12/bs12-18.htm)

[Category:光合作用](../Category/光合作用.md "wikilink")