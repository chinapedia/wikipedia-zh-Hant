**景行天皇**（；[垂仁天皇](../Page/垂仁天皇.md "wikilink")17年（前13年）-
景行天皇60年11月7日（130年12月23日））是[日本第](../Page/日本.md "wikilink")12代[天皇](../Page/天皇.md "wikilink")（在位71年7月11日－130年11月7日）。

雖然《[日本書紀](../Page/日本書紀.md "wikilink")》和《[古事記](../Page/古事記.md "wikilink")》記載了他的事跡，但其真實性仍有疑問。日本書紀記載他曾率軍遠征[九州的](../Page/九州.md "wikilink")[筑紫國](../Page/筑紫國.md "wikilink")。

## 歷史與傳說

他是[垂仁天皇的第三皇子](../Page/垂仁天皇.md "wikilink")，母親是日葉酢媛命。
亦有人說他是[日本](../Page/日本.md "wikilink")[彌生時代中後期的](../Page/彌生時代.md "wikilink")[倭國王](../Page/倭國.md "wikilink")[帥升](../Page/帥升.md "wikilink")。

## 家族

  - 父：[垂仁天皇](../Page/垂仁天皇.md "wikilink")（日本足彥國押人）
  - 母：[日葉酢媛命](../Page/日葉酢媛命.md "wikilink")

<!-- end list -->

  - 皇后：
      - [播磨稻日大郎姬](../Page/播磨稻日大郎姬.md "wikilink")
      - [八阪入媛](../Page/八阪入媛.md "wikilink")
  - 妃：
      - [水齒郎媛](../Page/水齒郎媛.md "wikilink")
      - [五十河媛](../Page/五十河媛.md "wikilink")
      - [高田媛](../Page/高田媛.md "wikilink")
      - [日向髮長大田根](../Page/日向髮長大田根.md "wikilink")
      - [襲武媛](../Page/襲武媛.md "wikilink")
      - 日向御刀媛
      - 伊那毘若郎女
      - 五十琴姬命

<!-- end list -->

  - 皇子
      - 櫛角別王
      - 大碓皇子
      - 小碓尊（[日本武尊](../Page/日本武尊.md "wikilink")）
      - 五百城入彥皇子
      - [稚足彥尊](../Page/成務天皇.md "wikilink")
      - 忍之別皇子
      - 稚倭根子皇子
      - 大酢別皇子
      - 五十狹城入彥皇子
      - 吉備兄彥皇子
      - [神櫛皇子](../Page/神櫛皇子.md "wikilink")
      - 稻背入彥皇子
      - 武國凝別皇子
      - 日向襲津彥皇子
      - 國乳別皇子
      - 國背別皇子
      - *以下餘37位皇子*\[1\]
  - 皇女：
      - 渟熨斗皇女
      - 渟名城皇女
      - 五百城入姬皇女
      - 麛依姬皇女
      - 高城入姬皇女
      - 弟姬皇女
      - 五百野皇女
      - 銀王
      - *景行天皇有二十六女，但史存者僅八女。*

## 腳註

[S](../Category/古墳時代天皇.md "wikilink")
[S](../Category/前13年出生.md "wikilink")
[Category:130年逝世](../Category/130年逝世.md "wikilink")

1.  請參考[大日本史](http://applepig.idv.tw/kuon/furu/text/dainihonsi/dainihon.htm#ouji)