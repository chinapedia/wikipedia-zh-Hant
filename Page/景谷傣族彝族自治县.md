**景谷傣族彝族自治县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省南部](../Page/云南省.md "wikilink")，是[普洱市下属的一个县](../Page/普洱市.md "wikilink")。位于澜沧江支流[威远江流域](../Page/威远江.md "wikilink")，[横断山脉无量山西南段](../Page/横断山脉.md "wikilink")。

[清](../Page/清.md "wikilink")[雍正二年](../Page/雍正.md "wikilink")（1724年）“改土归流”后，设威远厅；[民国元年](../Page/民国.md "wikilink")（1912年）改为威远县，三年（1914年）改为景谷县；1985年6月11日成立景谷傣族彝族自治县。

## 行政区划

下辖：\[1\] 。

## 旅游景点

景谷佛教遗迹十分丰富，被誉为“无量宝地”

  - “树包塔、塔包树”
  - 勐乃“仙人洞”
  - 帕庄河
  - 迁糯佛寺
  - 大仙人脚
  - 景谷湖
  - 威远江自然保护区

## 参考资料

[景谷傣族彝族自治县](../Category/景谷傣族彝族自治县.md "wikilink")
[Category:普洱区县](../Category/普洱区县.md "wikilink")
[云](../Category/中国傣族自治县.md "wikilink")
[云](../Category/中国彝族自治县.md "wikilink")

1.