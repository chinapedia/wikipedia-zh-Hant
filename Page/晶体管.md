[Transistorer_(croped).jpg](https://zh.wikipedia.org/wiki/File:Transistorer_\(croped\).jpg "fig:Transistorer_(croped).jpg")

**晶体管**（），早期[音译为](../Page/音译.md "wikilink")**穿细丝体**，是一种[类似于](../Page/类比.md "wikilink")[阀门的](../Page/阀门.md "wikilink")[-{zh-cn:固体;
zh-tw:固態;}-](../Page/固体.md "wikilink")[-{zh-cn:半导体器件;
zh-tw:半導體元件;}-](../Page/半导体器件.md "wikilink")，可以用于[放大](../Page/电子放大器.md "wikilink")、[开关](../Page/开关.md "wikilink")、稳压、信号调制和许多其他功能。在1947年，由[約翰·巴丁](../Page/約翰·巴丁.md "wikilink")、[沃爾特·布喇頓和](../Page/沃爾特·布喇頓.md "wikilink")[威廉·肖克利所發明](../Page/威廉·肖克利.md "wikilink")。當時巴丁、布喇頓主要發明半導體三極體；肖克利則是發明[PN二極體](../Page/二極管.md "wikilink")，他們因為半導體及電晶體效應的研究獲得1956年[諾貝爾物理獎](../Page/諾貝爾物理獎.md "wikilink")\[1\]。

電晶體由半導體材料組成，至少有三個對外端點（稱為極），(C)集極、(E)射極、(B)基極，其中(B)基極是控制極，另外兩個端點之間的伏安特性關係是受到控制極的非線性[電阻關係](../Page/電阻.md "wikilink")。晶体管基于输入的電流或[电压](../Page/电压.md "wikilink")，改變輸出端的[阻抗](../Page/阻抗.md "wikilink")，從而控制通過輸出端的[电流](../Page/电流.md "wikilink")，因此晶體管可以作為電流開關，而因為晶体管輸出信號的功率可以大於輸入信號的功率，因此晶体管可以作為电子放大器。

## 歷史

[Replica-of-first-transistor.jpg](https://zh.wikipedia.org/wiki/File:Replica-of-first-transistor.jpg "fig:Replica-of-first-transistor.jpg")

  - 1925年，[加拿大](../Page/加拿大.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")申請[場效應晶體管](../Page/場效應晶體管.md "wikilink")（FET）的專利
  - 1926年，也在美國申請專利，但是他沒有發布過相關的文章，而且當時還沒有製作高品質[半導體的相關技術](../Page/半導體.md "wikilink")。
  - 1934年，[德國發明家](../Page/德國.md "wikilink")申請類似裝置的[專利](../Page/專利.md "wikilink")。
  - 1947年12月，美国[贝尔实验室的](../Page/贝尔实验室.md "wikilink")[肖克利](../Page/肖克利.md "wikilink")、[巴丁和布拉顿组成的研究小组](../Page/巴丁.md "wikilink")，研制出一种点接触型的[锗晶体管](../Page/锗.md "wikilink")。

## 運用及分類

[Transistor_description_zh.svg](https://zh.wikipedia.org/wiki/File:Transistor_description_zh.svg "fig:Transistor_description_zh.svg")
晶体管主要分为两大类：[双极性晶体管](../Page/双极性晶体管.md "wikilink")（BJT）和[场效应晶体管](../Page/场效应管.md "wikilink")（FET）

電晶體一般都有三個極，其中一極兼任輸入及輸出端子，(B)基極不能做輸出，(C)集極不能做輸入之外，其餘兩個極組成輸入及輸出對。
電晶體之所以如此多用途在於其訊號放大能力，當微細訊號加於其中的一對極時便能控制在另一對極較大的訊號，這特性叫[增益](../Page/增益.md "wikilink")。

當電晶體於[線性工作時](../Page/線性.md "wikilink")，輸出的訊號與輸入的訊息成比例，這時電晶體就成了一[放大器](../Page/放大器.md "wikilink")。這是在[模拟电路中的常用方式](../Page/模拟电路.md "wikilink")，例如电子放大器、音频放大器、射频放大器、[稳压电路](../Page/稳压电路.md "wikilink")；

當電晶體的輸出不是完全關閉就是完全導通時，這時電晶體便是被用作[開關使用](../Page/開關.md "wikilink")。這種方式主要用于[数字电路](../Page/数字电路.md "wikilink")，例如数字电路包括[逻辑门](../Page/逻辑门.md "wikilink")、[隨機存取記憶體](../Page/隨機存取記憶體.md "wikilink")（RAM）和[微处理器](../Page/微处理器.md "wikilink")。另外在[开关电源中](../Page/开关电源.md "wikilink")，電晶體也是以這種方式工作。

而以何種形式工作，主要取決於電晶體的特性及外部電路的設計。

双极性晶体管的三个极，射極（Emitter）、基極（Base）和集極（Collector）\[2\];
射极到基極的微小[电流](../Page/电流.md "wikilink")，会使得射極到集極之间的阻抗改變，從而改變流經的电流\[3\]；

场效应晶体管的三个极，源極（Source）、閘（柵）極（Gate）和汲極（Drain）\[4\]。
在閘極與源極之間施加电压能夠改變源極與汲極之間的阻抗，從而控制源極和汲極之间的电流。

晶體管因為有三種極性，所以也有三種的使用方式，分別是[射極接地](../Page/共射極.md "wikilink")（又稱共射放大、CE組態）、[基極接地](../Page/共基極.md "wikilink")（又稱共基放大、CB組態）和[集極接地](../Page/共集極.md "wikilink")（又稱共集放大、CC組態、射極隨隅器）\[5\]。

晶體管在應用上有許多要注意的最大額定值，例如最大電壓、最大電流、最大功率。若在超額的狀態下使用，會破壞晶體管內部的結構。每種型號的晶體管還有像是直流放大率h<sub>FE</sub>、NF噪訊比等特性，可以藉由晶體管得知。

## 重要性

[Transistor_on_portuguese_pavement.jpg](https://zh.wikipedia.org/wiki/File:Transistor_on_portuguese_pavement.jpg "fig:Transistor_on_portuguese_pavement.jpg")葡式碎石路上的晶体管符號\]\]

晶体管被认为是现代历史中最伟大的发明之一，可能是二十世紀最重要的發明\[6\]，它讓[收音機](../Page/收音機.md "wikilink")、[計算器](../Page/計算器.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、以及相關電子產品變得更小、更便宜。

在重要性方面可以与[印刷术](../Page/印刷术.md "wikilink")，[汽车和](../Page/汽车.md "wikilink")[电话等发明相提并论](../Page/电话.md "wikilink")。晶体管是所有现代电器的关键主动（active）元件。晶体管在当今社会如此重要，主要是因为晶体管可以使用高度自动化的过程进行大规模生产的能力，因而可以不可思议地达到极低的单位成本。1947年[貝爾實驗室發明電晶體已被列在](../Page/貝爾實驗室.md "wikilink")[IEEE里程碑列表中](../Page/IEEE里程碑列表.md "wikilink")\[7\]。

虽然数以百万计的单体晶体管还在使用\[8\]，绝大多数的晶体管是和[二极管](../Page/二极管.md "wikilink")，[电阻器](../Page/电阻器.md "wikilink")，[电容器一起被装配在微芯片](../Page/电容器.md "wikilink")（芯片）上制造完整的[电路](../Page/电路.md "wikilink")。可能是模拟的、数字的，或是混合的芯片上。设计和开发复杂芯片的成本是相当高的，但是若分摊到百万个生产单位上，對每个芯片价格的影響就不大的。一个逻辑门包含20个晶体管，而2012年一个高级的微处理器使用的晶体管数量达14亿个。

晶体管的成本，灵活性和可靠性使得其成为非机械任务的通用器件，例如数字计算。晶体管电路在控制电器和机械的應用上，也正在取代电机设备，因为它通常是更便宜而有效，使用电子控制時，可以使用标准集成电路并编写计算机程序来完成一個机械控制同样的任务。

因为晶体管和后来的电子计算机的低成本，開始了数字化信息的浪潮。由于计算机提供快速的查找、分类和处理数字信息的能力，在信息数字化方面投入了越来越多的精力。今天的许多媒体是通过电子形式发布的，最终通过计算机转化和呈现为模拟形式。受到数字化革命影响的领域包括[电视](../Page/电视.md "wikilink")，[广播和](../Page/广播_\(大众媒体\).md "wikilink")[报纸](../Page/报纸.md "wikilink")。

## 和真空管的比較

在電晶體發展之前，[真空管是電子設備中主要的功率元件](../Page/真空管.md "wikilink")。

### 優點

電晶體因為有以下的優點，因此可以在大多數應用中代替真空管：

  - 沒有因加熱[陰極而產生的能量耗損](../Page/陰極.md "wikilink")，應用真空管時產生的橙光是因為加熱造成，有點類似傳統的燈泡。
  - 體積小，重量低，因此有助於電子設備的小型化。
  - 工作[電壓低](../Page/電壓.md "wikilink")，只要用[電池就可以供應](../Page/電池.md "wikilink")。
  - 在供電後即可使用，不需加熱[陰極需要的預熱期](../Page/陰極.md "wikilink")。
  - 可透過半導體技術大量的生產。
  - 放大倍數大\[9\]。

### 限制

相較於真空管，電晶體也有以下的限制：

  - 矽電晶體會老化及失效\[10\]。
  - 高功率，高頻的應用中（例如電視廣播），因真空管中的真空有助提昇[電子移動率](../Page/電子移動率.md "wikilink")，效果會比電晶體要好。
  - 固體電子元件在應用時比較容易出現[靜電放電現象](../Page/靜電放電現象.md "wikilink")。

## 类型

|- style="text-align:center;"
|[BJT_PNP_symbol.svg](https://zh.wikipedia.org/wiki/File:BJT_PNP_symbol.svg "fig:BJT_PNP_symbol.svg")||PNP||[JFET_P-Channel_Labelled.svg](https://zh.wikipedia.org/wiki/File:JFET_P-Channel_Labelled.svg "fig:JFET_P-Channel_Labelled.svg")||P-溝道
|- style="text-align:center;"
|[BJT_NPN_symbol.svg](https://zh.wikipedia.org/wiki/File:BJT_NPN_symbol.svg "fig:BJT_NPN_symbol.svg")||NPN||[JFET_N-Channel_Labelled.svg](https://zh.wikipedia.org/wiki/File:JFET_N-Channel_Labelled.svg "fig:JFET_N-Channel_Labelled.svg")||N-溝道
|- style="text-align:center;" |BJT||||JFET||

|- style="text-align:center;"
|[JFET_P-Channel_Labelled.svg](https://zh.wikipedia.org/wiki/File:JFET_P-Channel_Labelled.svg "fig:JFET_P-Channel_Labelled.svg")||[IGFET_P-Ch_Enh_Labelled.svg](https://zh.wikipedia.org/wiki/File:IGFET_P-Ch_Enh_Labelled.svg "fig:IGFET_P-Ch_Enh_Labelled.svg")||[IGFET_P-Ch_Enh_Labelled_simplified.svg](https://zh.wikipedia.org/wiki/File:IGFET_P-Ch_Enh_Labelled_simplified.svg "fig:IGFET_P-Ch_Enh_Labelled_simplified.svg")||[IGFET_P-Ch_Dep_Labelled.svg](https://zh.wikipedia.org/wiki/File:IGFET_P-Ch_Dep_Labelled.svg "fig:IGFET_P-Ch_Dep_Labelled.svg")||P-溝道
|- style="text-align:center;"
|[JFET_N-Channel_Labelled.svg](https://zh.wikipedia.org/wiki/File:JFET_N-Channel_Labelled.svg "fig:JFET_N-Channel_Labelled.svg")||[IGFET_N-Ch_Enh_Labelled.svg](https://zh.wikipedia.org/wiki/File:IGFET_N-Ch_Enh_Labelled.svg "fig:IGFET_N-Ch_Enh_Labelled.svg")||[IGFET_N-Ch_Enh_Labelled_simplified.svg](https://zh.wikipedia.org/wiki/File:IGFET_N-Ch_Enh_Labelled_simplified.svg "fig:IGFET_N-Ch_Enh_Labelled_simplified.svg")||[IGFET_N-Ch_Dep_Labelled.svg](https://zh.wikipedia.org/wiki/File:IGFET_N-Ch_Dep_Labelled.svg "fig:IGFET_N-Ch_Dep_Labelled.svg")||N-溝道
|- style="text-align:center;" |JFET||colspan="2"|MOSFET enh||MOSFET dep

電晶體可以依以下的方式分類：

  - [半導體材料](../Page/半導體材料.md "wikilink")（最早使用的分類）：[類金屬](../Page/類金屬.md "wikilink")[鍺](../Page/鍺.md "wikilink")（1947）及[矽](../Page/矽.md "wikilink")（1954）—
    [非晶](../Page/非晶矽.md "wikilink")、[多晶及](../Page/多晶硅.md "wikilink")形式）、[化合物半導體有](../Page/化合物半導體.md "wikilink")[砷化鎵](../Page/砷化鎵.md "wikilink")（1966）及[碳化矽](../Page/碳化矽.md "wikilink")（1997）、[矽鍺](../Page/矽鍺.md "wikilink")[合金](../Page/合金.md "wikilink")（1989），2004年開始研究的[碳的同素異形體](../Page/碳的同素異形體.md "wikilink")[石墨烯等](../Page/石墨烯.md "wikilink")。

  - 結構：[BJT](../Page/雙極性電晶體.md "wikilink")、[JFET](../Page/JFET.md "wikilink")、IGFET
    ([MOSFET](../Page/MOSFET.md "wikilink"))、[IGBT等](../Page/IGBT.md "wikilink")。

  - （正電及負電，類似化學[極性](../Page/極性.md "wikilink")）：[n–p–n及](../Page/双极性晶体管#NPN型.md "wikilink")[p–n–p](../Page/双极性晶体管#PNP型.md "wikilink")（BJT），N通道及P通道（FET）

  - 最大：可分為低功率、中功率及高功率。

  - 最大工作頻率：低頻、中頻、高頻、[無線電頻率](../Page/無線電.md "wikilink")（RF）、[微波頻率](../Page/微波.md "wikilink")：電晶體的最大等效頻率是用\(f_\mathrm{T}\)表示，是[過渡頻率的縮寫](../Page/增益頻寬積#晶体管.md "wikilink")，過渡頻率是增益為1時的頻率。

  - 應用：開關、泛用、音頻、[高壓](../Page/高壓.md "wikilink") 等。

  - [封裝](../Page/封裝.md "wikilink")：金屬封裝或塑膠封裝、[表面黏著技術](../Page/表面黏著技術.md "wikilink")、[球柵陣列封裝](../Page/球柵陣列封裝.md "wikilink")、功率晶體等。

  - 增益係數：h<sub>fe</sub>、β<sub>F</sub>\[11\]或g<sub>m</sub>（[跨导](../Page/跨导.md "wikilink")）等。

現在也已发明許多新类型的晶体管。已有在低温下操作的单电子晶体管（single electron transistor
SET）\[12\]，以及单原子晶体管（single atom transistor SAT） \[13\]
，其中，原子是个别地植入。

### 雙極性電晶體（BJT）

[雙極性電晶體同時利用半導體中的多數載子及少數載子導通](../Page/雙極性電晶體.md "wikilink")，因此得名。雙極性電晶體是第一個量產的電晶體，是由二種不同接面的[二極體組成](../Page/二極體.md "wikilink")，其結構可分為二層N型半導體中間夾一層P型半導體的NPN電晶體，以及二層P型半導體中間夾一層N型半導體的PNP電晶體\[14\]。因此會有二個[PN结](../Page/PN结.md "wikilink")，分別是基極-射極接面及基極-集極接面，中間隔著一層的半導體，即為基極。

雙極性電晶體和場效應電晶體不同，雙極性電晶體是低輸入阻抗的元件。當基集極電壓（*V<sub>be</sub>*）提高時，集極射極電流（*I<sub>ce</sub>*）會依肖克基模型及[艾伯斯-莫爾模型](../Page/雙極性電晶體#艾伯斯-莫爾模型.md "wikilink")，以指數形式增加。因此雙極性電晶體的[跨導比FET要高](../Page/跨導.md "wikilink")。

雙極性電晶體也可以設計為受到光照射時導通，因為基極吸收光子會產生光電流，其效應類似基極電流，集極電流一般是光電流的β倍，這類的電晶體一般會在封裝上有一透明窗，稱為[光電晶體](../Page/光電晶體.md "wikilink")。

### 場效應電晶體（FET）

[Threshold_formation_nowatermark.gif](https://zh.wikipedia.org/wiki/File:Threshold_formation_nowatermark.gif "fig:Threshold_formation_nowatermark.gif")的开通。左图为Id-Vg,右图为空间电子密度分布。随着电压增加，导电沟道形成（右图），电流增加（左图），场效应管开通\]\]

[場效應電晶體利用電子](../Page/場效應電晶體.md "wikilink")（N通道FET）或是電洞（P通道FET）導通電流。場效應電晶體都有閘極（gate）、汲極（drain）、源極（source）三個極，若不是[結型場效應管](../Page/結型場效應管.md "wikilink")，還會有一極，稱為體（body）。大部份的場效應電晶體中，體（body）會和源極相連。

在場效應電晶體中，源汲極電流會流過連接源極和汲極之間的通道，導通程度會依閘極和源極之間的電壓產生的電場而定，因此可以利用閘源極電壓控制源汲極電流，做為一個簡單的開關。當閘源極電壓*V<sub>gs</sub>*變大時，若*V<sub>gs</sub>*小於臨界電壓V<sub>T</sub>時，源汲極電流*I<sub>ds</sub>*會指數方式增加，若*V<sub>gs</sub>*大於臨界電壓V<sub>T</sub>時，源汲極電流和閘源極電壓會有以下的平方關係\(I_{ds} \propto (V_{gs}-V_T)^2\)，其中*V<sub>T</sub>*是臨界電壓\[15\]
。不過在一些現代的元件中，觀察不到上述的平方特性，像是[65奈米及以下通道長度的元件](../Page/65奈米.md "wikilink")\[16\]。

場效應電晶體可以分為兩種：分別是[結型場效應管](../Page/結型場效應管.md "wikilink")（JFET）及[絕緣閘極場效電晶體](../Page/絕緣閘極場效電晶體.md "wikilink")（IGFET），後者最常見的是[金屬氧化物半導體場效電晶體](../Page/金屬氧化物半導體場效電晶體.md "wikilink")（MOSFET），其名稱上反映了其原始以金屬（閘極）、氧化物（絕緣層）及半導體組成的架構。結型場效應管在源汲極之間形成了PN二極體。因此N通道的JFET類似真空管的[三極管](../Page/三極管.md "wikilink")，兩者也都是運作在空乏區，都有高輸入阻抗，也都用輸入電壓來控制電流。

## 参见

  - [真空管](../Page/真空管.md "wikilink")
  - [半导体](../Page/半导体.md "wikilink")
  - [-{zh-hans:集成电路; zh-hant:積體電路;}-](../Page/集成电路.md "wikilink")
  - [能隙](../Page/能隙.md "wikilink")
  - [数字电路](../Page/数字电路.md "wikilink")
  - [摩爾定律](../Page/摩爾定律.md "wikilink")
  - [跨導](../Page/跨導.md "wikilink")
  - [超大型積體電路](../Page/超大型積體電路.md "wikilink")

## 外部链接

  - [第一个晶体管的发明](http://psroc.phys.ntu.edu.tw/bimonth/download.php?d=1&cpid=160&did=17)

## 參考來源

  - 引用

<!-- end list -->

  - 書目

<!-- end list -->

  -
  -
[J](../Category/集成电路.md "wikilink") [J](../Category/電子元件.md "wikilink")
[J](../Category/电路.md "wikilink") [J](../Category/电子工程.md "wikilink")
[J](../Category/自動控制.md "wikilink") [J](../Category/電子工業.md "wikilink")
[Category:晶體管](../Category/晶體管.md "wikilink")
[Category:IEEE里程碑](../Category/IEEE里程碑.md "wikilink")

1.

2.

3.
4.
5.
6.

7.

8.  [FETs/MOSFETs: Smaller apps push up surface-mount
    supply](http://www.globalsources.com/gsol/I/FET-MOSFET/a/9000000085806.htm)


9.

10. John Keane and Chris H. Kim, ["Transistor
    Aging,"](http://spectrum.ieee.org/semiconductors/processors/transistor-aging)
    *IEEE Spectrum* (web feature), April 25, 2011.

11.  071003 bcae1.com

12.

13. E. Prati, M. Hori, F. Guagliardo, G. Ferrari, T. Shinada,
    Anderson-Mott transition in arrays of a few dopant atoms in a
    silicon transistor, Nature Nanotechnology 7, pp. 443 - 447 (2012)

14.
15.

16.