**晶門科技有限公司**是一家具有領導地位的[半導體公司](../Page/半導體.md "wikilink")，以自有品牌爲全球客戶提供[顯示器](../Page/顯示器.md "wikilink")[集成電路晶片及系統解决方案](../Page/集成電路.md "wikilink")。集團採用「無晶圓廠」商業模式，專門設計、開發和銷售專有[集成電路晶片及系統解决方案](../Page/集成電路.md "wikilink")，能廣泛應用於各類[智能手機](../Page/智能手機.md "wikilink")、[智能電視及其他智能產品](../Page/智能電視.md "wikilink")，包括[消費電子產品](../Page/消費電子產品.md "wikilink")、便攜式裝置、可穿戴式產品、便攜式裝置及工業用設備。
[集團擁有經驗豐富的優秀設計隊伍](../Page/集團.md "wikilink")，發展自有[知識產權及開發高度集成的](../Page/知識產權.md "wikilink")[顯示晶片和完整的顯示系統解決方案](../Page/顯示晶片.md "wikilink")，包括：

  - [觸摸屏控制器](../Page/觸摸屏.md "wikilink")
  - [MIPI高速顯示器接口控制器](../Page/MIPI.md "wikilink")
  - 高性能[LCD驅動控制器](../Page/LCD.md "wikilink")
  - PMOLED和[AMOLED驅動控制器](../Page/AMOLED.md "wikilink")
  - 雙穩態顯示驅控制動器
  - 大型[TFT](../Page/TFT.md "wikilink")-[LCD驅動器](../Page/LCD.md "wikilink")
  - [LCD控制器](../Page/LCD.md "wikilink")

Solomon Systech (International) Limited
於2004年4月8日在[香港聯合交易所有限公司主板上市](../Page/香港聯合交易所.md "wikilink")，（）。
2016年6月7日，晶門科技於股東大會後，創辦人梁廣偉退任董事會主席，頒發永久榮譽主席，以彰表於1999年創辦該公司經營17年卓越貢獻，而執行董事張惠權也在股東大會後退任\[1\]\[2\]。

## 參考

## 外部連結

  - [晶門科技](http://www.solomon-systech.com/)

[Category:香港製造公司](../Category/香港製造公司.md "wikilink")
[Category:香港電子公司](../Category/香港電子公司.md "wikilink")
[Category:無廠半導體公司](../Category/無廠半導體公司.md "wikilink")
[Category:香港上市資訊科技公司](../Category/香港上市資訊科技公司.md "wikilink")
[Category:1999年成立的公司](../Category/1999年成立的公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:中国电子信息产业集团有限公司](../Category/中国电子信息产业集团有限公司.md "wikilink")
[Category:中華人民共和國國有企業](../Category/中華人民共和國國有企業.md "wikilink")

1.  [晶門科技國際有限公司董事退任及 委任永久榮譽主席、 委任董事會主席、 委任執行董事、 及 董事委員會成員及
    授權代表變更通告](http://www.hkexnews.hk/listedco/listconews/sehk/2016/0607/LTN20160607788_C.pdf)[港交所](../Page/港交所.md "wikilink")
    2016年6月7日
2.  [晶門科技委任永久榮譽主席、主席及執行董事](http://www.solomon-systech.com/zh/press/bod_20160607/)晶門科技有限公司刊發
    2016年6月7日