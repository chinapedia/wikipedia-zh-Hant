**暴力团**，指以暴力或暴力脅迫方式進行犯罪的反社會團體，是[日本警察称呼](../Page/日本警察.md "wikilink")[日本黑道](../Page/日本黑道.md "wikilink")[组织的词汇](../Page/社会组织.md "wikilink")，这些组织一般自称**任侠团体**或**仁侠团体**。

按照《暴力团对策法》被[都道府县](../Page/都道府县.md "wikilink")[公安委员会指定的团体](../Page/公安委员会.md "wikilink")，被称呼为[指定暴力团](../Page/指定暴力团.md "wikilink")。到2006年12月為止，指定暴力团包括六代目[山口组](../Page/山口组.md "wikilink")、[稻川会](../Page/稻川会.md "wikilink")、[住吉会等](../Page/住吉会.md "wikilink")22个团体。

## 歷史

[江戶時代的](../Page/江戶時代.md "wikilink")[黑道大概出自於兩類人](../Page/黑道.md "wikilink")，一為攤販系列，二為賭徒系列。

「攤販」（[的屋](../Page/的屋.md "wikilink")）或「小販」者（**香具師**），奉祀[神農大帝](../Page/神農大帝.md "wikilink")、[天照大神](../Page/天照大神.md "wikilink")。從事經營[賭博事業的](../Page/賭博.md "wikilink")「賭徒」（**博徒**），奉祀[春日權現](../Page/春日權現.md "wikilink")、[八幡大菩薩](../Page/八幡大菩薩.md "wikilink")；他們的生活往往與暴力相關連，是[士大夫所不齒](../Page/士大夫.md "wikilink")，社會地位類似[賤民身分](../Page/賤民.md "wikilink")，如果從事正職，多半是都市中的消防隊員。

[二次大戰](../Page/二次大戰.md "wikilink")[日本投降後的蕭條中](../Page/日本投降.md "wikilink")，形成了「[不良少年集團](../Page/不良少年.md "wikilink")」組織（**[愚連隊](../Page/愚連隊.md "wikilink")**）。攤販、賭徒、愚連隊，此三類為現代「暴力團」雛型。

## 結緣方式（盃事）

  - 神農盃：跡目相続；先代組長跡目的讓渡或繼承
  - 親子盃：父輩與子輩的結緣方式
  - 兄弟盃：同輩的結緣方式
  - 手打盃：兩組織間競賽自決的方式；由第三者仲裁

※世代交替：新代組長必須進行一連串的繼承式〔襲名式〕及結緣式〔親子、兄弟〕，方完成交替。

## 指定暴力团

### 非指定暴力团

  - [寄居分家五代目](../Page/寄居分家.md "wikilink")（群馬縣）
  - 七代目[吉羽會](../Page/吉羽會.md "wikilink")（埼玉縣）
  - 五代目[龜屋一家](../Page/龜屋一家.md "wikilink")（埼玉縣）
  - [竹澤會](../Page/竹澤會.md "wikilink")（千葉縣）
  - [東亞會](../Page/東亞會.md "wikilink")（東京都）
  - 八代目[飯島會](../Page/飯島會.md "wikilink")（東京都）
  - [丁字家會](../Page/丁字家會.md "wikilink")（東京都）
  - [姉ヶ崎會](../Page/姉ヶ崎會.md "wikilink")（東京都）
  - [桝屋會](../Page/桝屋會.md "wikilink")（東京都）
  - [杉東會](../Page/杉東會.md "wikilink")（東京都）
  - [新門連合會](../Page/新門連合會.md "wikilink")（東京都）
  - [上州家會](../Page/上州家會.md "wikilink")（東京都）
  - [醍醐會](../Page/醍醐會.md "wikilink")（東京都）
  - [箸家會](../Page/箸家會.md "wikilink")（東京都）
  - [花又會](../Page/花又會.md "wikilink")（東京都）
  - [岡庭會](../Page/岡庭會.md "wikilink")（東京都）
  - [下谷花島會](../Page/下谷花島會.md "wikilink")（東京都）
  - 五代目[松坂屋一家](../Page/松坂屋一家.md "wikilink")（東京都）
  - [飴德連合會](../Page/飴德連合會.md "wikilink")（神奈川縣）
  - [横濱金子會](../Page/横濱金子會.md "wikilink")（神奈川縣）
  - 五代目[德力一家](../Page/德力一家.md "wikilink")（神奈川縣）
  - [櫻井總家](../Page/櫻井總家.md "wikilink")（静岡縣）
  - [中京神農會](../Page/中京神農會.md "wikilink")（愛知縣）
  - [忠成會](../Page/忠成會.md "wikilink")（兵庫縣）
  - 二代目[松浦組](../Page/松浦組.md "wikilink")（兵庫縣）
  - 二代目[竹中組](../Page/竹中組.md "wikilink")（岡山縣）
  - 二代目[中国高木會](../Page/中国高木會.md "wikilink")（広島縣）
  - [九州樫田會](../Page/九州樫田會.md "wikilink")（福岡縣）
  - 九州三代目[立川會](../Page/立川會.md "wikilink")（福岡縣）
  - [九州神代連會](../Page/九州神代連會.md "wikilink")（佐賀縣）
  - 二代目[熊本會](../Page/熊本會.md "wikilink")（熊本縣）
  - [山心會](../Page/山心會.md "wikilink")（熊本縣）
  - 九州三代目[村上組](../Page/村上組.md "wikilink")（大分縣）

## 參考文獻

  - Bruno, A. (2007). "The Yakuza, the Japanese Mafia" CrimeLibrary:
    Time Warner
  - Kaplan, David, Dubro Alec. (1986). *Yakuza* Addison-Wesley (ISBN
    0-201-11151-9)

## 外部連結

  - 警察庁
      - [組織犯罪対策](https://web.archive.org/web/20121606084300/http://www.npa.go.jp/sosikihanzai/index.htm)
  - [暴力団員による不当な行為の防止等に関する法律](http://law.e-gov.go.jp/htmldata/H03/H03HO077.html)（[暴力団対策法](../Page/暴力団対策法.md "wikilink")）
    - 総務省 法令データ提供システム
  - [松江地区建設業暴力追放対策協議会](http://www.web-sanin.co.jp/gov/boutsui/index.htm)
      - [指定暴力団](http://www.web-sanin.co.jp/gov/boutsui/boutsui4.htm)
      - [暴力団ミニ講座](http://www.web-sanin.co.jp/gov/boutsui/mini00.htm)
  - [全国暴力追放運動推進センター](https://web.archive.org/web/20161008162622/http://www1a.biglobe.ne.jp/boutsui/)

[Category:暴力團](../Category/暴力團.md "wikilink")