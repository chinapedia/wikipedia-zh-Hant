**暴雪娛樂**（）是一家美国[電子遊戲开发商和发行商](../Page/電子遊戲.md "wikilink")，于1991年2月8日由[加利福尼亚大学洛杉矶分校的](../Page/加利福尼亚大学洛杉矶分校.md "wikilink")[迈克尔·莫怀米](../Page/迈克尔·莫怀米.md "wikilink")、[艾伦·阿德汗和](../Page/艾伦·阿德汗.md "wikilink")[弗兰克·皮尔斯三位毕业生](../Page/弗兰克·皮尔斯.md "wikilink")\[1\]以**Silicon
&
Synapse**为名称创立，其總部位於[加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[爾灣](../Page/爾灣.md "wikilink")。\[2\]

暴雪娱乐公司的產品在[PC遊戲界享有極高的評價](../Page/PC游戏.md "wikilink")，出品的遊戲雖然不多，但是多数较受歡迎。其中，如[魔獸爭霸](../Page/魔獸爭霸.md "wikilink")、[星海爭霸](../Page/星海爭霸.md "wikilink")、[暗黑破壞神等遊戲受到玩家好評](../Page/暗黑破壞神.md "wikilink")，並被多個[電子競技賽事列為比賽項目](../Page/電子競技.md "wikilink")。

暴雪娱乐现在是美国电子游戏发行商[动视暴雪的部门](../Page/动视暴雪.md "wikilink")。

## 歷史

  - 1991年2月，由**麥可·莫懷米**、**艾伦·阿德汗**和**弗兰克·皮尔斯**創辦了暴雪的前身Silicon &
    Synapse\[3\]。這距他們獲得[加利福尼亞大學洛杉磯分校的學士學位只有一年](../Page/加利福尼亞大學洛杉磯分校.md "wikilink")\[4\]\[5\]。起初，公司致力於為其他工作室製作遊戲端口，包括了托爾金的《指環王：第一卷》，以及《戰棋Ⅱ：中國象棋》。\[6\]\[7\]
  - 1993年，公司開發了《[摇滚赛车](../Page/摇滚赛车.md "wikilink")》（Rock N'Roll
    Racing）和《[失落的維京人](../Page/失落的維京人.md "wikilink")》（由Interplay
    Production發行）。
  - 1994年，公司改名为**混沌工作室**（Chaos
    Studio），之後因為發現以混沌工作室命名的另一家公司已經存在，公司最終定名为**暴雪娛樂**。同年，公司被經銷商[Davidson
    &
    Associates公司以](../Page/Davidson_&_Associates.md "wikilink")675萬美元收購。\[8\]此後不久，暴雪的突破性大作《[魔獸爭霸：人類與獸人](../Page/魔獸爭霸：人類與獸人.md "wikilink")》上架發售。

暴雪自从被Davidson &
Associates收購後幾經易手：Davidson被[雪乐山在线收購](../Page/雪乐山.md "wikilink")，而雪乐山在线在1996年又被CUC國際收購。此後在1997年，CUC與旅店、房地產和[汽車租賃經銷商HPS合併成立](../Page/汽車租賃.md "wikilink")[Cendant](../Page/Cendant.md "wikilink")。合併之前，CUC的會計詐騙丑闻已出露端倪。此後被廣泛討論的會計醜聞的6個月中Cendant的股票市值下跌80%。1998年，Cendant將他的軟件部門，雪乐山在线和暴雪娱乐出售給法國發行商[Havas](../Page/Havas.md "wikilink")。同年，Havas被[维旺迪集团收購](../Page/维旺迪.md "wikilink")。此后，[雪乐山和暴雪娱乐都成为维旺迪集团旗下的](../Page/雪乐山.md "wikilink")[维旺迪游戏的部门](../Page/维旺迪游戏.md "wikilink")。

  - 1996年，暴雪收購禿鷲遊戲公司，之后禿鷲遊戲一直在為暴雪開發[暗黑破壞神系列](../Page/暗黑破壞神系列.md "wikilink")，禿鷲游戏公司则被重新命名為[北方暴雪](../Page/北方暴雪.md "wikilink")，之後開發了著名的《[暗黑破壞神](../Page/暗黑破壞神.md "wikilink")》、《[暗黑破壞神II](../Page/暗黑破壞神II.md "wikilink")》及其資料片《[暗黑破壞神II：毀滅之王](../Page/暗黑破壞神II：毀滅之王.md "wikilink")》，暴雪北方位於加利福尼亞州的[聖馬刁](../Page/聖馬刁_\(加利福尼亞州\).md "wikilink")。
  - 1997年1月，暴雪推出[動作角色扮演遊戲](../Page/動作角色扮演遊戲.md "wikilink")《暗黑破壞神》，同时一道推出了它的線上遊戲服務[戰網](../Page/戰網.md "wikilink")（Battle.Net）。
  - 1998年1月15日，暴雪推出經典即時戰略遊戲《[星际争霸](../Page/星际争霸_\(遊戲\).md "wikilink")》，成為當年全世界銷售量最大的遊戲。
  - 2002年，暴雪從[Interplay娱乐购回了早年Silicon](../Page/Interplay娱乐.md "wikilink")
    & Synaps的三個作品的版权，並且在[GBA平台上重新發售](../Page/GBA.md "wikilink")。\[9\]
  - 2003年，暴雪推出了《[魔兽争霸III：冰封王座](../Page/魔兽争霸III：冰封王座.md "wikilink")》，成就即时战略类游戏难以企及的高峰。
  - 2004年，暴雪在位於法國巴黎市郊的歐洲辦事處开业，以承擔《[魔獸世界](../Page/魔獸世界.md "wikilink")》在歐洲的線上服務。
  - 2004年11月23日，暴雪娱乐发行了至今仍风靡全球的[大型多人線上角色扮演遊戲](../Page/MMORPG.md "wikilink")——《[魔兽世界](../Page/魔兽世界.md "wikilink")》。
  - 2005年5月16日，暴雪宣佈收購家用機遊戲開發商Swingin'
    Ape工作室，来开发《星际争霸：幽灵》。在《星際爭霸：幽靈》被無限期推遲後，其仍旧正致力於次世代家用機平台的游戏開發。
  - 2005年8月1日，暴雪宣佈將其[北方暴雪合併進位於加利福尼亞州爾灣市的總部](../Page/北方暴雪.md "wikilink")。
  - 2007年12月，暴雪娛樂的母公司[維旺迪遊戲與](../Page/維旺迪遊戲.md "wikilink")[動視宣佈合併組建成新公司](../Page/動視.md "wikilink")[動視暴雪](../Page/動視暴雪.md "wikilink")\[10\]。
  - 2016年5月，暴雪娛樂推出熱門多人射擊遊戲《[鬥陣特攻](../Page/overwatch.md "wikilink")》。遊戲發售後首周，所有平台總銷量超過750萬套，獲得了業界十分正面的評價。遊戲還獲得了2016年度[遊戲大獎的年度遊戲獎項](../Page/遊戲大獎.md "wikilink")。
  - 2016年6月，暴雪娱乐参与投资拍摄的一个以魔兽系列故事为背景的[真人电影上映](../Page/魔兽_\(电影\).md "wikilink")，影片由[传奇影业](../Page/传奇影业.md "wikilink")、[环球影业制作发行](../Page/环球影业.md "wikilink")，导演为[邓肯·琼斯](../Page/邓肯·琼斯.md "wikilink")\[11\]。
  - 2017年3月30日，暴雪全球首座官方[電競館於](../Page/暴雪電競館.md "wikilink")[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[大安區](../Page/大安區_\(臺北市\).md "wikilink")[敦化南路一段開幕](../Page/敦化南路.md "wikilink")。\[12\]

## 主要作品

<table>
<thead>
<tr class="header">
<th><p><strong>Silicon &amp; Synapse</strong>时期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>游戏名称</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>战棋II：中国象棋</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/失落的维京战士.md" title="wikilink">失落的维京战士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/搖滾賽車.md" title="wikilink">搖滾賽車</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><strong>暴雪娱乐</strong>时期</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑色荊棘.md" title="wikilink">黑色荊棘</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔獸爭霸：人類與獸人.md" title="wikilink">魔獸爭霸：人類與獸人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/正義聯盟特遣部隊.md" title="wikilink">正義聯盟特遣部隊</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔獸爭霸II.md" title="wikilink">魔獸爭霸II：黑潮</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暗黑破壞神_(遊戲).md" title="wikilink">暗黑破壞神</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/失落的維京人2.md" title="wikilink">失落的維京人2</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星際爭霸_(遊戲).md" title="wikilink">星海爭霸</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/暗黑破壞神II.md" title="wikilink">暗黑破壞神II</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魔獸爭霸III：混亂之治.md" title="wikilink">魔獸爭霸III：混亂之治</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔獸世界.md" title="wikilink">魔獸世界</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星际争霸II：自由之翼.md" title="wikilink">星际争霸II：自由之翼</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/暗黑破壞神III.md" title="wikilink">暗黑破壞神III</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/爐石戰記：魔獸英雄傳.md" title="wikilink">爐石戰記：魔獸英雄傳</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/风暴英雄.md" title="wikilink">风暴英雄</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鬥陣特攻.md" title="wikilink">-{zh-tw:鬥陣特攻;zh-cn:守望先锋}-</a></p></td>
</tr>
<tr class="even">
<td><p><strong>開發終止遊戲</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星海爭霸：暗影獵殺.md" title="wikilink">星海爭霸：暗影獵殺</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰坦_(暴雪游戏).md" title="wikilink">泰坦</a></p></td>
</tr>
</tbody>
</table>

附註：《魔獸冒險：氏族之王》（Warcraft Adventures: Lord of the
Clans）在1998年5月22日被宣佈取消。《Shattered
Nations》和《星海爭霸：幽靈》（StarCraft:
Ghost）在2006年3月24日被標為「無限期的推遲」，許多業界人士認為它們也被取消了。不过，2007年6月，在對暴雪的遊戲設計副總裁罗布·帕尔多的一次訪談中提到，暴雪還沒有完全放棄《星海爭霸：幽靈》\[13\]。而策划开发了有7年之久的《泰坦》在2014年9月被确认中止开发。\[14\]

## 战网 2.0

暴雪娱乐在2009年发布了其新的战网（Battle.net）服务。这项服务允许那些已经购买了暴雪娱乐游戏产品（《星际争霸》、《星际争霸II》、《暗黑破坏神II》和《魔兽争霸III》及其资料片）的玩家，下载游戏拷贝。他们购买然后下载，而不需要任何物理媒体。在未来，它将会储存玩家的“暴雪等级”（类似玩家分数或者游戏成就）。

2009年11月11日，暴雪娱乐要求所有北美《魔兽世界》玩家将帐户切换到战网帐户。目前所有流行的暴雪娱乐游戏的网站、下载以及帐号登录都是通过战网来完成的。

2009年7月1日，中国大陆的《魔兽世界》运营商[网易开放战网帐号绑定](../Page/网易.md "wikilink")\[15\]，但当时《魔兽世界》大陆地区运营商易主网易，游戏暂时停止运营。于7月30日开服后，只有绑定完成战网通行证的玩家才可进入游戏\[16\]。

2011年5月13日，台湾、香港和澳门地区的《魔兽世界》运营商[智凡迪开放战网帐号绑定](../Page/智凡迪.md "wikilink")\[17\]。至此全球《魔兽世界》均在战网之下运行。

## 前僱員

若干年過去，一些前暴雪僱員已經走出來成立了他們自己的遊戲公司。

  - [旗艦工作室](../Page/旗艦工作室.md "wikilink")\[18\]，公司成立於2003年，《[地獄之門：倫敦](../Page/地獄之門：倫敦.md "wikilink")》的製作者，而另一款作品《[神話](../Page/神話_\(遊戲\).md "wikilink")》在2007年進行封閉BETA測試，原本預計在2008年中期間公開測試，由於旗艦工作室經濟困難早前在2008年關閉，因此而停止開發。在同年未完成工程的作品和版權移交由韓國網路遊戲出版商[HanbitSoft](../Page/HanbitSoft.md "wikilink")。在2009年5月，Hanbitsoft在網站發佈了一個用韓語聲明，已交由T3娛樂（**T3
    Entertainment**）和T3的子公司Redbana繼續發展。
  - ArenaNet\[19\]，公司成立於2000年，《[激戰](../Page/激戰.md "wikilink")》系列的製作者，其中的續作[激戰2](../Page/激戰2.md "wikilink")，已在2012年8月發佈。
  - Ready At
    Dawn工作室\[20\]，公司成立於2003年，《[达克斯特](../Page/达克斯特.md "wikilink")》和[PSP平台](../Page/PSP.md "wikilink")《战神》系列製作者，現在正製作《战神：起源收藏版》，收錄PSP平台的《战神：奥林匹斯之链》和《战神：斯巴达之魂》兩部作品，以[PS3的高畫質呈現](../Page/PS3.md "wikilink")，在2011年9月發佈。
  - Red
    5工作室\[21\]，公司成立於2005年，目前正致力開發於全新形态的动作战争网遊《[Firefall](../Page/Firefall.md "wikilink")》。
  - Castaway Entertainment\[22\]，目前正在開發次世代平台遊戲
  - Click Entertainment\[23\]，《[黑暗王座](../Page/黑暗王座.md "wikilink")》的製作者
  - Carbine Studio\[24\]，對外宣稱目前正在開發大型多人在線遊戲\[25\]。

## 爭議和法律纠纷

### 發售延期

暴雪娛樂除了產品評價高之外，還有另一個經常被人提起的特徵：發售延期。其發佈產品幾乎都出現過延期甚至是數度延期。《暗黑破壞神》曾計畫1996年11月30日發售，但是最終延期至1997年1月，此後欲1997年上市的《-{zh-tw:星海爭霸;zh-cn:星际争霸}-》延至1998年，欲1999年12月上市的《[暗黑破壞神II](../Page/暗黑破壞神II.md "wikilink")》延至2000年，欲2001年上市的《魔獸爭霸Ⅲ》延至2002年，欲2003年聖誕節上市的《魔獸世界》延至2004年初，《魔獸世界》海外版本和修正補丁也多次延期。多次且大量的延期招致大量玩家的不滿，乃至《暗黑破壞神III》和《-{zh-tw:星海爭霸II;zh-cn:星际争霸II}-》公佈後，大量玩家認為這兩部作品絕對會再次出現發售延期的事情。

暴雪娛樂創始人迈克尔·莫莱姆曾回應：反覆的潤色階段是使我們的遊戲與眾不同的原因，最後10%的潤色實際上就是一個好遊戲和一個差遊戲之間的差別。《-{zh-tw:星海爭霸;zh-cn:星际争霸}-》在先前公佈時基本上是基於《魔獸爭霸II》的代碼進行開發，而在延期後數月出現的高完成度Demo版則完全不同，被認為是在延期期間進行了幾乎是推倒重來的修改，故此部分玩家之間認為暴雪娛樂雖然發售延期，但是基本上是為了使產品精益求精而進行的作業，對此雖然不滿，但是可以理解。

### 戰網

戰網是為其遊戲《[暗黑破壞神](../Page/暗黑破壞神_\(遊戲\).md "wikilink")》，《[-{zh-tw:星海爭霸;zh-cn:星际争霸}-](../Page/星海爭霸.md "wikilink")》，《[-{zh-tw:星海争霸：怒火燎原;zh-cn:星际争霸：母巢之战}-](../Page/星海争霸：怒火燎原.md "wikilink")》，《[暗黑破壞神II](../Page/暗黑破壞神II.md "wikilink")》，《[暗黑破壞神Ⅱ：毀滅之王](../Page/暗黑破壞神II：毀滅之王.md "wikilink")》，《[魔獸爭霸II](../Page/魔獸爭霸II.md "wikilink")》戰網版本，《[魔獸爭霸III](../Page/魔獸爭霸III.md "wikilink")》，《[-{zh-tw:魔獸爭霸III：寒冰霸權;zh-cn:魔兽争霸III：冰封王座}-](../Page/魔獸爭霸III：寒冰霸權.md "wikilink")》，《[魔兽世界](../Page/魔兽世界.md "wikilink")》，《[星際爭霸II](../Page/星際爭霸II.md "wikilink")》和《[暗黑破坏神III](../Page/暗黑破坏神III.md "wikilink")》開發的在線遊戲服務。他於1997年1月份與《暗黑破壞神》一同發佈，它發揮著在[互聯網上遊戲的功能](../Page/互聯網.md "wikilink")，以配合玩家對玩家的遊戲，遊戲匹配系統，在線聊天以及其他在線應用方式為特色。戰網服务完全免費，只需要互聯網連接和必要的帳號註冊。

一群遊戲玩家使用[逆向工程破解了戰網和暴雪遊戲使用的網絡協議](../Page/逆向工程.md "wikilink")，並且發佈了面戰網模擬軟件包，稱為[Bnetd](../Page/Bnetd.md "wikilink")。使用Bnetd，玩家將不再需要登錄官方戰網伺服器來玩暴雪遊戲。

在2002年1月，暴雪律師威脅要對Bnetd的開發者將依據《[數字千禧年版權法案](../Page/數字千禧年版權法案.md "wikilink")》採取法律行動。暴雪遊戲就是被設計成只能在暴雪控制的伺服器上運行的，戰網伺服器將包括[CD-key的檢查](../Page/CD-key.md "wikilink")，這也意味著避免的軟件[盜版](../Page/盜版.md "wikilink")。

儘管Bnetd的開發者提議將暴雪的CD-key整合進Bnetd，暴雪還是宣佈這種軟件包的廣泛傳播將促進盜版，並進一步要求依據《數字千禧年版權法案》停止Bnetd項目。因為這個案例是《數字千禧年版權法案》的第一個主要檢驗的案例，在這個問題的協商（而非法律判決）正在進行之時，[電子邊界基金會也捲入其中](../Page/電子邊界基金會.md "wikilink")。協商失敗，然而暴雪贏得官司的全部要求：被告被判決違反了《星際爭霸最終用戶許可協議》和《戰網使用條款》。

該判決被上訴到[聯邦上訴法院第八巡迴審判庭](../Page/聯邦上訴法院第八巡迴審判庭.md "wikilink")，2005年9月1日，其裁決結果也是維持對暴雪和威望迪有利的原判。

### 自由的魔獸爭霸

暴雪在2003年6月20日，給使用魔獸爭霸引擎的開源複製版——被稱作“自由魔獸爭霸（FreeCraft）”的開發者發送了一份終止信函。這個業餘項目拥有与《魔獸爭霸II》一樣的玩法和角色，但是畫面和音樂有所區別。

作為一款類似的遊戲，“自由魔獸爭霸”允許玩家在拥有《魔獸爭霸II》CD的前提下使用其畫面。複製版的程序員毫无争议关闭了他們的網站。不久，他們又重新組織起來，以[Stratagus名字繼續他們的工作](../Page/Stratagus.md "wikilink")\[26\]\[27\]。

### 看護者客戶端

暴雪使用過一種特殊形式的[軟件](../Page/軟件.md "wikilink")，這就是看護者客戶端，當運行的時候，這個客戶端掃瞄個人電腦用以核實《[最終用戶許可協議](../Page/最終用戶許可協議.md "wikilink")》和《使用條款》是否得以遵守。眾所周知，看護者客戶端與暴雪的魔獸世界在線遊戲一起使用，並且所有的玩家通過《最終用戶許可協議》和《使用條款》允許看守者客戶端在魔獸世界運行的時候進行這些掃瞄\[28\]。

看護者客戶端掃瞄[進程名稱](../Page/進程.md "wikilink")，窗口名稱，和運行程序的小部分代碼段以判斷是否有其他第三方的程序在運行。這種判斷是用採集掃瞄到的字符串並將其採集好的值與被認為是相對應的作弊程序的採集值表單比對而得出的\[29\]，看護者客戶端掃瞄所有的在計算機上運行的進程，而不僅僅是魔獸世界遊戲程序，以及可能包括一些被認為是個人隱私信息和其他個人身份信息，正因為這種掃瞄，看守者客戶端被指責為[間諜軟件](../Page/間諜軟件.md "wikilink")，並且在[隱私擁護者中引起糾纏不清的爭論](../Page/隱私.md "wikilink")\[30\]\[31\]\[32\]。

看護者客戶端在正確識別合法行為與非法行為方面的可靠性值得懷疑，因為之前暴雪依據看護者客戶端收集到的信息所作出的處置並不怎麼成功，及其值得提到的是許多玩家被這個程序判定為違反了《最終用戶許可協議》和《使用條款》，並且稍後帳號被封，而實際上他們並沒有作弊。許多[Linux用戶在升級後發生了因為升級了看守者客戶端後導致將](../Page/Linux.md "wikilink")[Cedega進程錯誤的判定為作弊程序而被封號的突發事件](../Page/Cedega.md "wikilink")\[33\]。暴雪發佈了一個聲明聲稱他們已經正確的識別這個問題並且恢復了所有的帳號，並且補償20天的遊戲時間\[34\]。暴雪已經聲稱看護者客戶端並不向主伺服器發送任何信息除了一個違反標記。但是，沒有看護者客戶端發出的特定信息，暴雪不可能將Cedega用戶和實際的作弊者區分開來\[35\]。

暴雪娛樂並不是第一次嘗試窺探他們顧客主機。早在1998年暴雪娛樂已經遭到一系列因未經用戶准許卻非法收集用戶數據，而導致的以非法商業行為傾訴的訴訟\[36\]。

### 侵权诉讼

#### 方正电子

2007年8月14日，北大[方正电子有限公司状告暴雪娱乐侵犯其版权](../Page/方正集团.md "wikilink")，要求赔偿100万元人民币的侵权损失。这起诉讼指控暴雪娱乐在《魔兽世界》中文版中，未经方正电子允许擅自使用其中文字体\[37\]。

#### 刀塔傳奇

2015年间，暴雪娛樂和[Dota
2的开发商](../Page/Dota_2.md "wikilink")[Valve在](../Page/Valve.md "wikilink")[台北地方法院檢察署](../Page/台北地方法院檢察署.md "wikilink")、[北京市海淀区人民法院和美国](../Page/北京市海淀区人民法院.md "wikilink")，就中国大陆公司[龙图游戏和](../Page/龙图游戏.md "wikilink")[莉莉丝游戏的手機遊戲](../Page/莉莉丝游戏.md "wikilink")《[刀塔傳奇（现小冰冰传奇）](../Page/小冰冰传奇.md "wikilink")》，发起了有关侵犯著作权、商标权、专利权、不正当竞争的诉讼。龙图和莉莉丝因而赔偿了一定款项，修改部分美术并将游戏改名。\[38\]\[39\]\[40\]\[41\]

#### 卧龙传说

2014年4月，暴雪娱乐和[网之易公司起诉游易网络称游易网络旗下的游戏](../Page/网之易.md "wikilink")[卧龙传说侵权](../Page/卧龙传说.md "wikilink")，2014年11月暴雪娱乐和[网之易公司胜诉](../Page/网之易.md "wikilink")。\[42\]

#### 全民魔兽

2015年，暴雪娱乐与网易公司将七游、分播时代、动景以《全民魔兽》侵权为由告上法庭。2016年9月，法院判罚七游、分播时代、动景向暴雪娱乐、网易公开道歉并赔偿共计六百万元人民币。\[43\]\[44\]

## 參考文獻

## 相關條目

  - [动视](../Page/动视.md "wikilink")
  - [动视暴雪](../Page/动视暴雪.md "wikilink")
  - [维旺迪游戏](../Page/维旺迪游戏.md "wikilink")

## 外部連結

  - [暴雪娛樂官方網站](http://www.blizzard.com)
  - [暴雪娛樂官方網站](http://tw.blizzard.com/zh-tw/)
  - [戰網Battle.Net](http://www.battle.net)
  - [戰網Battle.Net](http://www.battlenet.com.cn/)

[Category:1991年開業電子遊戲公司](../Category/1991年開業電子遊戲公司.md "wikilink")
[暴雪娛樂](../Category/暴雪娛樂.md "wikilink")
[B](../Category/美國電子遊戲公司.md "wikilink")
[B](../Category/美國品牌.md "wikilink")
[B](../Category/電子遊戲開發公司.md "wikilink")
[B](../Category/電子遊戲發行商.md "wikilink")

1.

2.  [公司介绍](http://tw.blizzard.com/zh-tw/company/about/profile.html)

3.
4.

5.
6.

7.

8.  Dean Takahashi: *[game-development Co-Founder Looks at Chaos in
    Early Stages and Future
    Challenges.](http://articles.latimes.com/print/1994-03-13/business/fi-33584_1_video)*
    In: Los Angeles Times, March 13, 1994. – Interview with Allen Adham.

9.

10.

11.

12.

13. 網元網PC遊戲專區 （[2007年6月](../Page/2007年6月.md "wikilink")）

14.
15.

16.

17.

18.

19. [ArenaNet](http://arenanet.com/games/index.html)

20. [About Ready At Dawn Studios](http://www.readyatdawn.com/ready.asp)

21. [Red 5 Studios](http://red5studios.com/faq/)

22. [About Castaway
    Entertainment](http://www.castawayentertainment.com/about.html)

23. [Click
    Entertainment](http://www.eurogamer.net/article.php?article_id=1453)

24. [Carbine Studios](http://www.carbinestudios.com/news/)

25.

26. [The Linux Game Tome:
    FreeCraft](http://www.happypenguin.org/show?FreeCraft&start=20)

27. [Stratagus | Home](http://stratagus.sourceforge.net/)

28. [WoW -\> Legal -\> Terms of
    Use](http://www.worldofwarcraft.com/legal/termsofuse.html)

29. [rootkit.com](http://www.rootkit.com/blog.php?newsid=358)

30. [WoW's Warden stirs controversy - news -
    play™](http://play.tm/story/6837)

31. [Definitions and Supporting
    Documents](http://www.antispywarecoalition.org/documents/definitions.htm)


32. [Look\! what is Blizzard doing on your pc? - MMOsite News
    Center](http://news.mmosite.com/content/2006-11-26/20061126193343858.shtml)

33. [Linux Users Banned From World of Warcraft? |
    Linuxlookup](http://www.linuxlookup.com/2006/nov/15/linux_users_banned_from_world_of_warcraft)

34.

35. <http://forums.worldofwarcraft.com/thread.aspx?fn=blizzard-archive&t=33&p=1&tmp=1#post33>

36. [Errata: Blizzard
    Entertainment](http://attrition.org/errata/company/blizzard01.html)

37.

38.

39.

40.

41.

42. [网游《卧龙传说》被判抄袭
    开发商获赔33万](http://tech.sina.com.cn/i/2014-11-11/doc-icfkptvx4558727.shtml)

43. [暴雪网易诉《全民魔兽》侵权胜诉，获赔600万元](http://tech.china.com/news/company/892/20160908/23504668.html)

44. [《全民魔兽》手游侵权案
    暴雪与网易获胜诉](http://www.techweb.com.cn/shoujiyouxi/2016-09-08/2389956.shtml)