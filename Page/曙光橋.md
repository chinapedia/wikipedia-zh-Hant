**曙光橋**是位於臺灣花蓮縣[花蓮市](../Page/花蓮市.md "wikilink")[美崙溪河口的一座景觀橋梁](../Page/美崙溪.md "wikilink")，其名稱源自站於其上東望[太平洋可迎接第一道](../Page/太平洋.md "wikilink")[曙光](../Page/曙光.md "wikilink")。\[1\]\[2\]
該橋起造於[日本時代](../Page/台灣日本時代.md "wikilink")，當時為鐵路路線[花蓮港支線的鐵道橋](../Page/花蓮臨港線.md "wikilink")**米崙溪橋**，橫跨美崙溪以聯絡[花蓮港與](../Page/花蓮港.md "wikilink")[花蓮市](../Page/花蓮市.md "wikilink")。\[3\]\[4\]在該鐵道支線廢除後，花蓮縣政府將其改建為木棧橋，供行人及自行車通行。\[5\]

## 沿革

昔日，[花蓮臨港線自舊花蓮車站出發往](../Page/花蓮臨港線.md "wikilink")[花蓮港車站前進](../Page/花蓮港車站.md "wikilink")，途經聯絡[花蓮港與](../Page/花蓮港.md "wikilink")[花蓮市的美崙溪橋](../Page/花蓮市.md "wikilink")，沿途停靠民利、新村、[米崙及花蓮港站](../Page/美崙車站.md "wikilink")\[6\]。隨著[北迴鐵路竣工](../Page/北迴鐵路.md "wikilink")，花蓮車站西遷，整個鐵路路線的業務也跟著遷移，昔日臨港線鐵道及鐵橋隨即停止使用。\[7\]2000年（民國89年），花蓮縣縣長[王慶豐規劃](../Page/王慶豐.md "wikilink")「南北濱自行車道」，以南濱公園為起點沿舊臨港支線至北濱公園，再至花蓮港北側管制站，全長四公里；在獲[行政院環保署經費補助及](../Page/行政院環保署.md "wikilink")[臺灣鐵路管理局同意使用土地後](../Page/臺灣鐵路管理局.md "wikilink")\[8\]，花蓮縣政府將原本鐵道橋鋪上木棧道，成為提供[自行車及行人行走的景觀橋](../Page/自行車.md "wikilink")\[9\]。2005年（民國94年）3月經登錄列為花蓮縣歷史建築。\[10\]\[11\]

[File:曙光橋(舊花蓮臨港線美崙溪橋).JPG|曙光橋](File:曙光橋\(舊花蓮臨港線美崙溪橋\).JPG%7C曙光橋)

## 周遭歷史

美崙溪河口在[清朝時負有運送官兵到](../Page/清朝.md "wikilink")[豐川保護原住民的功能](../Page/豐川.md "wikilink")，因此又稱「陸軍港」\[12\]；這裡也是花蓮歷史上[漢人第一次登陸的地點](../Page/漢人.md "wikilink")。\[13\]

## 參見

  - [花蓮臨港線](../Page/花蓮臨港線.md "wikilink")
  - [臺東線](../Page/台東線.md "wikilink")
  - [花蓮縣文化資產](../Page/花蓮縣文化資產.md "wikilink")

## 註釋

[Category:花蓮縣橋梁](../Category/花蓮縣橋梁.md "wikilink")
[Category:台灣鐵道旅遊景點](../Category/台灣鐵道旅遊景點.md "wikilink")
[Category:台灣日治時期交通建築](../Category/台灣日治時期交通建築.md "wikilink")
[Category:花蓮市](../Category/花蓮市.md "wikilink")
[Category:臺灣鐵路橋](../Category/臺灣鐵路橋.md "wikilink")
[Category:人行桥](../Category/人行桥.md "wikilink")
[Category:梁桥](../Category/梁桥.md "wikilink")
[Category:花蓮縣歷史建築](../Category/花蓮縣歷史建築.md "wikilink")
[Category:1939年完工橋梁](../Category/1939年完工橋梁.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.