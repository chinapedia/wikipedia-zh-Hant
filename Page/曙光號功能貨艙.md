**曙光號功能貨艙**（****）是[國際太空站一個功能貨艙](../Page/國際太空站.md "wikilink")，也是最早發射的一個組件。曙光號功能貨艙於1998年11月20日被[質子號運載火箭從](../Page/質子號運載火箭.md "wikilink")[哈薩克的](../Page/哈薩克.md "wikilink")[白寇努爾太空中心發射進入](../Page/拜科努爾太空中心.md "wikilink")400公里高的軌道中，設計的使用時間至少為15年。

[Category:航天器](../Category/航天器.md "wikilink")
[Category:歐洲太空總署](../Category/歐洲太空總署.md "wikilink")
[Category:國際空間站](../Category/國際空間站.md "wikilink")