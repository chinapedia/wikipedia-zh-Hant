**曲承裕**（，，又稱「**曲先主**」），[中国](../Page/中国.md "wikilink")[唐朝末期](../Page/唐朝.md "wikilink")，授任他為[静海节度使](../Page/静海节度使.md "wikilink")，管辖今[越南北部一带](../Page/越南.md "wikilink")，為日後越南脫離中國独立建国打下基礎。

## 生平

曲承裕出身於[唐代](../Page/唐代.md "wikilink")[安南鴻州](../Page/越南.md "wikilink")（屬今[海陽省寧江縣](../Page/海陽省#行政區劃.md "wikilink")）的富豪世家。905年，安南[靜海節度使](../Page/靜海節度使.md "wikilink")[獨孤損被貶職](../Page/獨孤損.md "wikilink")，流放到[海南島](../Page/海南島.md "wikilink")。與此同時，[中國境內藩鎮割據](../Page/中國.md "wikilink")，政局混亂，而曲承裕則憑著他「宽和愛人」的性格及地方豪族身份，受到當地人支持，自稱靜海節度使、[同平章事](../Page/同平章事.md "wikilink")，以治理安南。其後，曲承裕獲唐朝認可其官職。關於曲承裕開始任靜海節度使的年份，[臺灣學者](../Page/臺灣.md "wikilink")[呂士朋認為是](../Page/呂士朋.md "wikilink")905年，越南學者陳仲金認為是906年。\[1\]

曲承裕任職不久，便於907年去世，其子[曲顥继位](../Page/曲顥.md "wikilink")。\[2\]\[3\]

## 評價

  - [越南封建時代史家](../Page/越南.md "wikilink")[黎嵩讚賞](../Page/黎嵩.md "wikilink")「**曲先主**世為巨族，雄睿智略，因[唐之亡](../Page/唐朝.md "wikilink")，群心愛戴，共推為主，都于[羅城](../Page/河內.md "wikilink")，民安國治，功德永垂。」\[4\]
  - [越共學者認為](../Page/越共.md "wikilink")，曲承裕名義上雖然還是[唐朝的官員](../Page/唐朝.md "wikilink")，但實際上已建立起自主政權。他從[中國封建王朝中奪得政權](../Page/中國.md "wikilink")，令[越南擺脫其枷鎖](../Page/越南.md "wikilink")，因而堪稱是「民族獨立的奠基人之一」。\[5\]

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
  -
  -
  -
  -
## 外部連結

  -
## 參見

  - [越南自主时期](../Page/越南自主时期.md "wikilink")
      - [曲家](../Page/曲家.md "wikilink")
          - [曲顥](../Page/曲顥.md "wikilink")
  - [靜海節度使](../Page/靜海節度使.md "wikilink")

{{-}}

[Category:靜海軍節度使](../Category/靜海軍節度使.md "wikilink")
[Category:曲姓](../Category/曲姓.md "wikilink")

1.  陳仲金《越南史略》第二卷第五章，中譯本，48頁；呂士朋《北屬時期的越南》，香港中文大學新亞研究所，141頁。
2.  陳仲金《越南史略》第二卷第五章，中譯本，48頁。
3.  郭振鐸、張笑梅《越南通史》第三編第七章，235頁。
4.  黎嵩《越鑑通考總論》（《大越史記全書》卷首），86頁。
5.  越南社會科學委員會《越南歷史》第二部份第四章，中譯本，第144頁。