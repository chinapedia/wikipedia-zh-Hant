**曲紋唇魚**（[學名](../Page/學名.md "wikilink")：**），(又稱**波紋鸚鯛**、**波紋唇魚)**，常俗稱**蘇眉鱼**，也有**拿破崙鱼**、**龍王鯛**、**海哥龍王**及**大片仔**等別名，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[隆頭魚科的其中一](../Page/隆頭魚科.md "wikilink")[種](../Page/種.md "wikilink")。目前受到《[瀕危野生動植物種國際貿易公約](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")》（CITES）保護，列為附錄II的物種，表示於所有、管理及進行貿易時，均受到許可證制度管制。

主要棲息區域分布于[红海](../Page/红海.md "wikilink")、[印度洋非洲东岸至太平洋中部](../Page/印度洋.md "wikilink")、[台灣以及](../Page/台灣.md "wikilink")[南海诸岛等](../Page/南海.md "wikilink")，幼鱼多生活于礁盘内侧浅水中以及成鱼常见于礁盘外侧较深的海域。\[1\]

## 特徵

大型隆頭魚，個性溫和。因為高高隆起的額頭，就像[拿破崙戴的帽子](../Page/拿破崙.md "wikilink")，所以有「拿破崙」之稱。

成魚的眼睛很小，位於頭部的上側位，兩眼間隔處甚為隆起，體沿長而呈長卵圓形；頭部輪廓自背部至眼平直，然後凸出；成魚前額突出。口大斜裂，上下頜各具錐形齒一列，前端各有一對大犬齒；前鰓蓋骨邊緣具鋸齒。體被大形圓鱗。尾鰭末緣圓，頭頸部呈墨綠色，體側則呈黃綠色，身體的後半部則具深色的波狀橫紋。幼魚則呈深藍色，頭、腹部色淡有許多朱紅色的細紋，身體的鱗片末緣具有紫色橫紋。頭部有兩平行黑帶通過眼部。背鰭硬棘9枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條8枚。體長可達200公分。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區包括](../Page/太平洋.md "wikilink")[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[綠島](../Page/綠島.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[印度](../Page/印度.md "wikilink")、[越南](../Page/越南.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[加羅林群島](../Page/加羅林群島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[美屬薩摩亞等海域水深](../Page/美屬薩摩亞.md "wikilink")2至10000公尺的地方。

## 生態

本魚通常獨居或成對出現。幼魚常棲息在礁盤內側的淺水區，成魚則常出現在礁區外較深海域。主要以[甲殼類及](../Page/甲殼類.md "wikilink")[軟體動物為食](../Page/軟體動物.md "wikilink")。此外也是熱帶[珊瑚礁內](../Page/珊瑚礁.md "wikilink")[食物鏈中最高層次的一種魚類](../Page/食物鏈.md "wikilink")，透過捕食其他魚類和生物，擔起平衡海洋生態的作用。

## 經濟利用

為觀賞魚，但因撈捕過度，許多海域中已難見蹤影，被[IUCN紅色名錄列為](../Page/IUCN紅色名錄.md "wikilink")[瀕危物種](../Page/瀕危物種.md "wikilink")，且目前受到《[瀕危野生動植物種國際貿易公約](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")》（CITES）保護，列為附錄II的物種，表示管有及進行貿易時均受到許可證制度管制。[世界自然基金會將蘇眉列為受威脅物種](../Page/世界自然基金會.md "wikilink")，並鼓勵市民減少食用蘇眉\[2\]。目前[香港是本魚最主要的進出口地區](../Page/香港.md "wikilink")，雖已進行貿易監察，但非法進口及貿易卻持續危害本魚的可持續性\[3\]
。早前在香港就曾有人舉報[西貢有海鮮酒家非法管有本魚而被充公及罰款](../Page/西貢.md "wikilink")，最高可被罰款港元500萬及監禁兩年\[4\]。中華民國[行政院農業委員會已公告於](../Page/行政院農業委員會.md "wikilink")2014年7月1日起將此魚列入保育類野生動物，禁止捕殺。

## 爭議事件

2016年5月21日，[台東縣](../Page/台東縣.md "wikilink")[綠島鄉](../Page/綠島鄉.md "wikilink")「阿憲民宿」業者陳明憲被媒體披露獵殺龍王鯛\[5\]，一張龍王鯛被放置地上的圖大為流傳，社會一片譁然。陳明憲稱此照片為七年前的照片，卻遭到指出來源圖片的[4G
LTE網路圖示](../Page/4G_LTE.md "wikilink")，以及魚體旁的[Nike拖鞋為](../Page/Nike.md "wikilink")2015年生產款式，即此照片不可能於七年前拍攝。隨後陳明憲又宣稱魚是撿來的，並在拍照完畢後扔回水中。經[海巡署人員調查偵訊後](../Page/海巡署.md "wikilink")，陳明憲終坦承明知龍王鯛是保育類動物不能獵捕，但仍在2016年5月21日將其獵殺，獵殺後拍照在私人版面炫耀，遭到媒體批露，知道事情鬧大，不敢將魚吃下肚，且立即刪除手機與電腦裡相關照片，同時把魚屍拿到[綠島監獄以北約](../Page/綠島監獄.md "wikilink")30公尺的一處工地掩埋。

2016年5月22日中午，陳明憲帶領海巡人員挖出魚屍。\[6\]偵訊後，全案依違反《[野生動物保育法](../Page/野生動物保育法.md "wikilink")》移送[台東地檢署偵辦](../Page/台東地檢署.md "wikilink")。2016年6月，阿憲民宿原地改名「海洋18海哩潛水民宿」開始營業。\[7\]

## 参考文献

## 扩展阅读

## 外部链接

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)
  - [IUCN
    Redlist](http://www.iucnredlist.org/search/details.php/4592/summ)

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[Category:有毒魚類](../Category/有毒魚類.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:台灣動物](../Category/台灣動物.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:印尼動物](../Category/印尼動物.md "wikilink")
[Category:越南動物](../Category/越南動物.md "wikilink")
[Category:印度魚類](../Category/印度魚類.md "wikilink")
[Category:菲律賓動物](../Category/菲律賓動物.md "wikilink")
[undulatus](../Category/唇魚屬.md "wikilink")

1.
2.

3.

4.

5.

6.  [無法狡辯
    民宿業者坦承昨天捕殺龍王鯛](http://www.appledaily.com.tw/realtimenews/article/life/20160522/867901/)

7.  [綠島獵魚高手
    「阿憲民宿」舊址開新店遭網友爆卦](http://news.ebc.net.tw/news.php?nid=24376),2016.06.03
    東森新聞