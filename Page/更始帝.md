**漢更始帝刘玄**（），字**圣公**，[南阳郡](../Page/南阳郡.md "wikilink")[蔡阳县](../Page/蔡阳县.md "wikilink")（今[湖北省](../Page/湖北省.md "wikilink")[枣阳市西南](../Page/枣阳市.md "wikilink")）人，两[汉之际](../Page/汉朝.md "wikilink")[绿林军擁立的](../Page/绿林军.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，或被視為[西漢的最後一位皇帝](../Page/西漢.md "wikilink")。

劉玄原本是[西汉宗室](../Page/西汉.md "wikilink")，是汉景帝[劉啟的後代](../Page/劉啟.md "wikilink")、汉光武帝[刘秀的族兄](../Page/刘秀.md "wikilink")。祖父為[苍梧](../Page/苍梧.md "wikilink")[太守](../Page/太守.md "wikilink")[刘利](../Page/刘利.md "wikilink")，父[刘子张](../Page/刘子张.md "wikilink")，母[何氏](../Page/何氏_\(漢更始帝母\).md "wikilink")。

## 生平

起初，刘玄的弟弟被人杀害，他便结交宾客准备报仇，可不久之后宾客犯罪，刘玄便跑到[平阳躲避官府的追捕](../Page/平阳县_\(山西\).md "wikilink")。官吏抓走他的父亲[刘子张](../Page/刘子张.md "wikilink")，后来刘玄诈死，让人把他的[灵柩运回舂陵](../Page/灵柩.md "wikilink")，官吏才释放了他的父亲。随后刘玄便逃跑藏匿起来。\[1\]

[新莽](../Page/新莽.md "wikilink")[天凤四年](../Page/天凤.md "wikilink")（17年），南方发生饥荒，百姓只得到沼泽中挖[荸荠野菜度日](../Page/荸荠.md "wikilink")。[新市人](../Page/京山县.md "wikilink")[王匡](../Page/王匡_\(更始\).md "wikilink")、[王凤带领](../Page/王凤.md "wikilink")[马武](../Page/马武_\(东汉\).md "wikilink")、[王常](../Page/王常.md "wikilink")、[成丹等人起义](../Page/成丹.md "wikilink")。他们藏身[绿林山中](../Page/绿林山.md "wikilink")，不久队伍就发展到五万多人。\[2\]

[地皇三年](../Page/地皇_\(王莽\).md "wikilink")（22年）[七月](../Page/農曆七月.md "wikilink")，平林人[陈牧](../Page/陈牧.md "wikilink")、[廖湛领导千余人起义](../Page/廖湛.md "wikilink")，号平林兵，以应王匡，正在这里避难的刘玄参加平林军，担任安集[掾](../Page/掾.md "wikilink")。地皇四年（23年）[正月](../Page/農曆正月.md "wikilink")，绿林军诸部合兵击破新莽将领[甄阜](../Page/甄阜.md "wikilink")、[梁丘赐](../Page/梁丘赐.md "wikilink")，遂号刘玄为**更始将军**。[二月辛巳](../Page/農曆二月.md "wikilink")（3月11日），后因其为刘姓宗室，遂在[淯水边被拥立为帝](../Page/淯水.md "wikilink")，大赦天下，建元**[更始](../Page/更始_\(更始帝\).md "wikilink")**。更始元年[六月入都](../Page/農曆六月.md "wikilink")[宛城](../Page/宛城.md "wikilink")，大封[宗室诸将](../Page/宗室.md "wikilink")。他嫉[刘縯](../Page/刘縯.md "wikilink")、[刘秀兄弟威名](../Page/刘秀.md "wikilink")，便诛杀刘縯。起义军在[昆阳之戰获胜后](../Page/昆阳之戰.md "wikilink")，更始帝遣[王匡攻](../Page/王匡.md "wikilink")[洛阳](../Page/洛阳.md "wikilink")，[申屠建](../Page/申屠建.md "wikilink")、[李松攻](../Page/李松.md "wikilink")[武关](../Page/武关.md "wikilink")，[三辅震动](../Page/三辅.md "wikilink")，各地豪强纷纷诛杀新莽的[州牧](../Page/州牧.md "wikilink")[郡守](../Page/郡守.md "wikilink")，用**汉**年号，服从更始政令。更始元年[十月](../Page/農曆十月.md "wikilink")，定都[洛阳](../Page/洛阳.md "wikilink")。[王莽败死后的更始二年](../Page/王莽.md "wikilink")（24年），迁都[长安](../Page/长安.md "wikilink")。\[3\]

### 更始分封

李松和[趙萌進言更始帝](../Page/趙萌.md "wikilink")，将功臣封王。[大司馬](../Page/大司馬.md "wikilink")[朱鮪反对](../Page/朱鮪.md "wikilink")，称[汉高祖的誓言非刘氏不王](../Page/汉高祖.md "wikilink")。更始帝采纳李松之言，以[王匡为比阳王](../Page/王匡.md "wikilink")、[王凤为宜城王](../Page/王凤.md "wikilink")、[朱鲔为胶东王](../Page/朱鲔.md "wikilink")、卫尉大将军[张卬为淮阳王](../Page/张卬.md "wikilink")、廷尉大将军[王常为邓王](../Page/王常.md "wikilink")，执金吾大将军[廖湛为穰王](../Page/廖湛.md "wikilink")、[申屠建为平氏王](../Page/申屠建.md "wikilink")、尚书[胡殷为随王](../Page/胡殷.md "wikilink")、柱天大将军[李通为西平王](../Page/李通.md "wikilink")、五威中郎将[李轶为舞阴王](../Page/李轶.md "wikilink")、水衡大将军[成丹为襄邑王](../Page/成丹.md "wikilink")、大司空[陈牧为阴平王](../Page/陈牧.md "wikilink")、骠骑大将军宋佻为颍阴王、[尹尊为郾王](../Page/尹尊.md "wikilink")。李松昇進[丞相](../Page/丞相.md "wikilink")，和趙萌共理内政。

### 腐敗

更始帝雖恢復漢室，但其性格迂腐，重用小人。一登基便沉湎于[宫廷奢華生活](../Page/宫廷.md "wikilink")，即位后将政事都委托于自己的岳父[赵萌](../Page/赵萌.md "wikilink")，放任其[外戚专权](../Page/外戚.md "wikilink")。[赤眉军进逼长安时](../Page/赤眉.md "wikilink")，更始帝杀害申屠建、陈牧、成丹等起义军重要将领。更始三年（25年）他殺害西漢末代君主[劉嬰](../Page/劉嬰.md "wikilink")。

更始三年（25年）[九月](../Page/農曆九月.md "wikilink")，[赤眉军攻入](../Page/赤眉军.md "wikilink")[长安](../Page/长安.md "wikilink")，更始帝单骑逃走。同年[十月](../Page/農曆十月.md "wikilink")，投降赤眉，将玺绶送给赤眉拥立的皇帝[刘盆子](../Page/刘盆子.md "wikilink")，自己被封为**畏威侯**，不久改封为**长沙王**，刘秀则遥降封之为**淮阳王**\[4\]。赤眉将领[张卬为绝后患](../Page/张卬.md "wikilink")，派人将更始帝[缢死](../Page/缢.md "wikilink")。\[5\]

## 東汉官方对刘玄的地位认定

25年[刘秀建立](../Page/刘秀.md "wikilink")[東漢後](../Page/東漢.md "wikilink")，為確立自己為光復漢室的[正統](../Page/正統.md "wikilink")，劉秀及其后代并不认定刘玄是汉朝皇帝，官修《[东观汉记](../Page/东观汉记.md "wikilink")》直接将刘玄归为列传\[6\]。[张衡提出不同意见](../Page/张衡.md "wikilink")，认为光武之前，更始踐祚，号令天下，光武是继更始之后登上天子之位的，《光武帝纪》之前，应有《更始帝纪》，不過朝廷并没有采纳张衡的意见\[7\]\[8\]，

## 陵墓

漢光武帝刘秀命[邓禹将漢更始帝劉玄葬于](../Page/邓禹.md "wikilink")[霸陵](../Page/霸陵.md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[西安](../Page/西安.md "wikilink")[灞橋區](../Page/灞橋區.md "wikilink")）。\[9\]

## 家族

  - 七世祖：漢景帝與侍女[唐兒](../Page/唐兒.md "wikilink")，生長沙定王[劉發](../Page/劉發.md "wikilink")。
  - 六世祖：劉發，生舂陵節侯[劉買](../Page/劉買_\(舂陵侯\).md "wikilink")。
  - 高祖父：劉買，生舂陵戴侯[劉熊渠和](../Page/劉熊渠.md "wikilink")[鬰林太守](../Page/鬱林郡.md "wikilink")[劉外](../Page/劉外.md "wikilink")。
  - 曾祖父：劉熊渠，生舂陵孝侯[劉仁和蒼梧太守](../Page/劉仁.md "wikilink")[劉利](../Page/劉利.md "wikilink")。
  - 祖父：劉利，生[劉子張](../Page/劉子張.md "wikilink")。
  - 父：劉子張，妻[何氏](../Page/何氏.md "wikilink")，生劉玄和劉騫\[10\]。

### 妻妾

  - 韓夫人，宠姬，嗜酒
  - 赵夫人，有宠，[赵萌女儿](../Page/赵萌.md "wikilink")

### 儿子

  - [刘求](../Page/刘求.md "wikilink")：襄邑侯，承更始帝之祀，后改封为成阳侯
  - [刘歆](../Page/刘歆.md "wikilink")：谷熟侯
  - [刘鲤](../Page/刘鲤.md "wikilink")：寿光侯，怨[刘盆子害其父](../Page/刘盆子.md "wikilink")，因沛献王[刘辅](../Page/刘辅.md "wikilink")，报杀刘盆子之兄式侯[刘恭](../Page/刘恭_\(式侯\).md "wikilink")

## 部將

  - [劉秀](../Page/劉秀.md "wikilink")
  - [劉縯](../Page/劉縯.md "wikilink")
  - [申屠建](../Page/申屠建.md "wikilink")
  - [趙萌](../Page/趙萌.md "wikilink")
  - [李松](../Page/李松.md "wikilink")
  - [成丹](../Page/成丹.md "wikilink")

## 影視形象

  - 中國電視劇《[秀麗江山之長歌行](../Page/秀麗江山之長歌行.md "wikilink")》（2016年）：由[于波飾演劉玄](../Page/于波.md "wikilink")

## 参考文献

  - 《[後漢書](../Page/後漢書.md "wikilink")·[劉玄劉盆子列傳](../Page/s:後漢書/卷11#劉玄.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀第三十一](../Page/s:資治通鑑/卷039.md "wikilink")》

{{-}}    |-    |-   |-

[X玄](../Category/劉姓.md "wikilink")
[Category:漢朝皇帝](../Category/漢朝皇帝.md "wikilink")
[Category:中國被殺帝王](../Category/中國被殺帝王.md "wikilink")
[L](../Category/枣阳人.md "wikilink")

1.

2.
3.
4.  《後漢書·光武帝紀·上》載詔：「更始破敗，棄城逃走，妻子裸袒，流宂道路。朕甚愍之。今封更始為淮陽王。吏人敢有賊害者，罪同大逆。」

5.
6.  《[太平御览](../Page/太平御览.md "wikilink")》卷九十引《东观汉记》：刘玄，字圣公，光武族兄也。

7.

8.  《后汉书》卷五十九：又更始居位，人无异望，光武初为其将，然后即真，宜以更始之号建于光武之初。书数上，竟不听。

9.
10. 《後漢書·安城孝侯傳》[李賢注引](../Page/李賢.md "wikilink")《[續漢書](../Page/續漢書.md "wikilink")》：後十餘歲，亭長子報殺更始弟騫。