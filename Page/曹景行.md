**曹景行**是一位中國電視節目主持人和報刊編輯。

## 生平

[上海市市西中学](../Page/上海市市西中学.md "wikilink")[高中毕业](../Page/高中.md "wikilink")，[复旦大学历史系毕业](../Page/复旦大学.md "wikilink")，曾任[上海社会科学院世界经济研究所助理研究员](../Page/上海社会科学院.md "wikilink")、[凤凰卫视时事评论员](../Page/凤凰卫视.md "wikilink")、凤凰卫视言论部总监、凤凰卫视主持人、[凤凰卫视资讯台副台长](../Page/凤凰卫视资讯台.md "wikilink")、《[亚洲周刊](../Page/亚洲周刊.md "wikilink")》撰述员/编辑/资深编辑/副总编辑、《[明报](../Page/明报.md "wikilink")》主笔、[傳訊電視中天频道总编辑](../Page/傳訊電視.md "wikilink")。在凤凰卫视主持《时事开讲》、《总编辑时间》、《口述历史》、《风范大国民》、《景行长安街》等节目。2005年起，在[北京](../Page/北京.md "wikilink")[清华大学新闻与传播学院担任高级访问学者](../Page/清华大学.md "wikilink")。2009年，主持[浙江电台交通之声](../Page/浙江电台交通之声.md "wikilink")《曹景行有话说》。2012年9月，主持[江西卫视](../Page/江西卫视.md "wikilink")《深度观察》。2013年1月，與[盧秀芳及](../Page/盧秀芳.md "wikilink")[王津元主持](../Page/王津元.md "wikilink")[中天電視與](../Page/中天電視.md "wikilink")[東方衛視合作的節目](../Page/東方衛視.md "wikilink")《雙城記》，之后自2019年起担任《[今晚60分](../Page/今晚60分.md "wikilink")》
嘉宾主持。

其父[曹聚仁是著名报人](../Page/曹聚仁.md "wikilink")，在大陆以[鲁迅好友知名](../Page/鲁迅.md "wikilink")。
其妹[曹雷是著名配音演员](../Page/曹雷.md "wikilink")，在大陆知名度很高。

## 著作

  - 《光圈中的凤凰》：[上海辞书出版社](../Page/上海辞书出版社.md "wikilink")2006年6月出版，ISBN
    7532620182
  - 《香港十年》：上海辞书出版社2007年6月出版，ISBN 9787532621842

## 外部链接

  -
[Category:上海人](../Category/上海人.md "wikilink")
[Category:中國主持人](../Category/中國主持人.md "wikilink")
[Category:中国记者](../Category/中国记者.md "wikilink")
[Category:中華人民共和國編輯](../Category/中華人民共和國編輯.md "wikilink")
[Category:中華人民共和國散文家](../Category/中華人民共和國散文家.md "wikilink")
[Category:凤凰卫视主持人](../Category/凤凰卫视主持人.md "wikilink")
[Category:SMG主持人](../Category/SMG主持人.md "wikilink")
[Category:復旦大學校友](../Category/復旦大學校友.md "wikilink")
[J](../Category/曹姓.md "wikilink")