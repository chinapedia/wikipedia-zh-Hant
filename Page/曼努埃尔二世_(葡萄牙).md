[ManuelIIPortugal.jpg](https://zh.wikipedia.org/wiki/File:ManuelIIPortugal.jpg "fig:ManuelIIPortugal.jpg")
**曼努埃尔二世**（全名**曼努埃尔·玛丽亚·菲利佩·卡洛斯·阿美利奥·路易斯·米格尔·拉菲尔·加布里埃尔·贡扎加·法蘭西斯科·德·阿西西·欧仁尼奥**，；），[葡萄牙第](../Page/葡萄牙.md "wikilink")34位也是最后一位[国王](../Page/葡萄牙君主列表.md "wikilink")，国王[卡洛斯的次子](../Page/卡洛斯一世_\(葡萄牙\).md "wikilink")，1908年卡洛斯与[王储路易斯被激进的共和派刺杀身亡](../Page/路易斯·菲利佩_\(葡萄牙王储\).md "wikilink")，曼努埃尔即位为国王。曼努埃尔二世在1909年被[英国国王授予](../Page/英国.md "wikilink")[嘉德勋章](../Page/嘉德勋章.md "wikilink")。

年轻曼努埃尔二世国王即位后，极力寻求保存[布拉干萨王朝脆弱的统治](../Page/薩克森-科堡-哥達王朝.md "wikilink")，于是在即位当年遣散了独裁者[若昂·佛朗哥和他的整个内阁](../Page/若昂·佛朗哥.md "wikilink")，邀请海军上将[阿马拉尔组阁](../Page/阿马拉尔.md "wikilink")，并邀请复兴党，进步黨参加。这被共和派和社会主义者称为一次巨大胜利。在他统治的两年时间，七次改组内阁，政局一片混乱，终于在1910年10月3日晚爆发了[革命](../Page/1910年10月5日革命.md "wikilink")，曼努埃尔二世的宫殿被轰击，他于次日被废黜，10月5日逃亡到[英国](../Page/英国.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")[直布罗陀](../Page/直布罗陀.md "wikilink")。曼努埃尔二世后定居英国伦敦。

1913年9月4日，在[锡格马林根与](../Page/锡格马林根.md "wikilink")[霍亨索伦公主奥古斯塔](../Page/霍亨索伦王朝.md "wikilink")·维多利亚（1890年－1966年）结婚。没有子嗣。

曼努埃尔二世是一位藏书家，并写了一本中世纪和文艺复兴时期葡萄牙文学指南。

曼努埃尔二世于1932年7月2日逝世于英国托维肯汉姆。由于他没有子嗣，他认可他的表叔祖，[布拉干萨公爵](../Page/布拉干萨公爵.md "wikilink")[杜瓦尔特·努诺为他的继承人](../Page/杜瓦尔特·努诺.md "wikilink")。

|-         |-

[Category:葡萄牙君主](../Category/葡萄牙君主.md "wikilink")
[Category:布拉干薩王朝](../Category/布拉干薩王朝.md "wikilink")
[Category:萨克森-科堡-哥达王朝](../Category/萨克森-科堡-哥达王朝.md "wikilink")
[Category:欧洲废君](../Category/欧洲废君.md "wikilink")
[Category:末代帝王](../Category/末代帝王.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")
[Category:死在英国的外国人](../Category/死在英国的外国人.md "wikilink")
[Category:在英國的葡萄牙人](../Category/在英國的葡萄牙人.md "wikilink")
[Category:葡萄牙天主教徒](../Category/葡萄牙天主教徒.md "wikilink")
[Category:里斯本人](../Category/里斯本人.md "wikilink")