**曼弗雷德·阿爾布雷希特·馮·里希特霍芬男爵**（，）是一名[德國](../Page/德國.md "wikilink")[飛行員](../Page/飛行員.md "wikilink")，被稱為王牌中的王牌，外号**红男爵**（）。他也是戰鬥機聯隊指揮官和[第一次世界大戰擊落最多敵機的戰鬥機](../Page/第一次世界大戰.md "wikilink")[王牌](../Page/王牌飛行員.md "wikilink")，共擊落80架敵機之多\[1\]\[2\]\[3\]。里希特霍芬出生于[贵族家庭](../Page/贵族制.md "wikilink")，亲戚中有许多知名人士。

## 名字

[Wappen-Richthofen.jpg](https://zh.wikipedia.org/wiki/File:Wappen-Richthofen.jpg "fig:Wappen-Richthofen.jpg")
[弗雷尔](../Page/:en:Freiherr.md "wikilink")（*Freiherr*）字面意思自由公爵。但这并不是他的[教名](../Page/教名.md "wikilink")，而是德国一个贵族化的[称谓](../Page/称谓.md "wikilink")，相当其他国家中的[男爵](../Page/男爵.md "wikilink")。其最有名的名字红男爵就因此而来。德国人称他为。尽管在他在世时在德国甚至在他的[自传中一直被称之为](../Page/自传.md "wikilink")*Der
Rote Kampfflieger*（暂译“红色斗士”），但红男爵這個綽號还是在德國廣為流傳。

里希特霍芬於[法語區被稱為](../Page/法語.md "wikilink")“”（紅男爵）、“”（紅魔鬼）、“”（小紅），[英語區中也稱他為](../Page/英語.md "wikilink")“”（紅男爵）。

## 早年生活

里希特霍芬出生於克莱因堡（Kleinburg），接近[德國](../Page/德國.md "wikilink")[西里西亞的小村](../Page/西里西亞.md "wikilink")，家族是旧普鲁士贵族。里希特霍芬9歲時舉家搬到[史威德尼茲](../Page/史威德尼茲.md "wikilink")（Schweidnitz，今波蘭希維德尼察）。年輕的里希特霍芬喜歡打獵、騎馬和体育。在学校他获得了很好的体育成绩。\[4\]
他11岁开始接受正规军事训练\[5\]。1911年受訓完畢後加入[沙皇](../Page/沙皇.md "wikilink")[亞歷山大三世持](../Page/亚历山大三世_\(俄国\).md "wikilink")[矛](../Page/矛.md "wikilink")[騎兵團](../Page/騎兵.md "wikilink")，[西普魯士第](../Page/西普魯士.md "wikilink")1營（Uhlan
Regiment Emperor Alexander III of Russia 1st Regiment, West Prussia,
Uhlan Regiment Number 1）\[6\]。

[第一次世界大戰爆發時](../Page/第一次世界大戰.md "wikilink")，里希特霍芬担任骑兵军官在東西两線作战。但隨著大規模使用[機槍和有刺鐵絲網](../Page/機槍.md "wikilink")，騎兵的存在價值已經不大，持矛騎兵團解散後里希特霍芬被調派到補給團。\[7\]
。1915年5月，他因不满没有在前线作战，申請調派到德意志帝国陆军航空隊服役。在请求得到同意后，他于1915年5月正式开始服役。\[8\]

## 飛行生涯

在偶然機會下，里希特霍芬遇到另一位一戰王牌[奧斯華·波爾克](../Page/奧斯華·波爾克.md "wikilink")（Oswald
Boelcke）後便決定要成為一位飛行員。後來，[波爾克提拔了里希特霍芬加入其精英戰鬥機聯隊](../Page/波爾克.md "wikilink")，第2戰鬥機中隊（Jasta
2）。里希特霍芬於1916年9月17日在[法國](../Page/法國.md "wikilink")[坎巴拉取得他首個戰績](../Page/坎巴拉.md "wikilink")。

取得他首個戰績後，里希特霍芬寫信給[柏林一位珠寶商朋友](../Page/柏林.md "wikilink")，訂製了1隻銀杯，杯上刻有擊落敵機的日期和飛機型號。他一直保持這個傳統，直到德國的銀供應短缺時才停下來，當時他已經擁有60隻銀杯。

當時里希特霍芬的同輩都不覺得他是個天賦異稟的飛行員。反而覺得他的弟弟洛塔才是個有天賦的飛行員，對飛行動作較為熟練。不像里希特霍芬信奉飛行戰術，但飛行技術卻不如很多飛行員，里希特霍芬只視他的戰鬥機為一個射擊平台。

1916年11月23日，駕著[信天翁D-II戰鬥機的里希特霍芬擊落了](../Page/信天翁D戰鬥機.md "wikilink")[英國王牌](../Page/英國.md "wikilink")[納奈·霍克](../Page/納奈·霍克.md "wikilink")。這次險勝令他深信他需要一種更靈活的戰鬥機，但這意味著需要犧牲更多速度。然而直到1917年時，信天翁戰鬥機都是德國陸軍航空兵的組成骨幹，里希特霍芬期間換裝過信天翁D.III和D.V。後來信天翁D.V戰鬥機出現一連串墜毀事故，並發現其低翼設計存在缺憾。他因而換裝哈爾伯施塔特
D.II(Halberstadt
D.II)戰鬥機。到1917年9月，里希特霍芬開始飛行著名的[福克Dr.I三翼戰鬥機](../Page/福克Dr.I戰鬥機.md "wikilink")。

雖然福克
Dr.I幾乎就是里希特霍芬的代名詞，但其實他們並肩作戰的時間就只有他生命最後的2個月，期間共擊落60架敵機。他首架全紅色戰鬥機卻是信天翁D.III。

被認為是[第一次世界大戰時最佳的](../Page/第一次世界大戰.md "wikilink")[德國戰鬥機](../Page/德意志帝國.md "wikilink")—[福克D-VII戰鬥機可能才是紅男爵最希望駕駛的飛機](../Page/福克D-VII戰鬥機.md "wikilink")，他一直有參與研發工作，為設計者提供他的實戰經驗以改善現有德國戰鬥機的缺點。可惜在福克
D.VII投入服役的前一天，里希特霍芬於法國上空被擊落陣亡。

## 飛行馬戲團

1917年1月，在他擊落第16架敵機後，里希特霍芬獲授予當時德國最高榮譽[藍馬克斯勳章](../Page/藍馬克斯勳章.md "wikilink")（Pour
le Mérite）。同月他被委任為第11戰鬥機中隊（Jasta
11）的中隊長，里希特霍芬一手訓練出很多精英飛行員，部份飛行員後來成為了其他中隊的指揮官。

為了於交戰時較容易識別敵我，第11戰鬥機中隊亦跟隨里希特霍芬把戰鬥機塗裝成紅色，並各自加上獨特的標記。

里希特霍芬帶領他的中隊取得空前成功，1917年[血腥四月](../Page/血腥四月.md "wikilink")（Bloody
April）更踏上高峰。這單月中，他擊落了22架英國飛機，令他的擊落數增加到52架。到6月時，他成為新建[第1戰鬥機聯隊](../Page/第1戰鬥機聯隊.md "wikilink")（Jagdgeschwader
1）的聯隊長，聯隊包括有第4、6、10和11戰鬥機中隊。這些高機動性的中隊可以根據需要分派到不同的前線。第1戰鬥機聯隊又因為擁有色彩繽紛的戰鬥機和大帳篷而被起了兩個別號飛行馬戲團和里希特霍芬馬戲團。

但於7月6日，里希特霍芬在一次戰鬥中被擊中，頭部嚴重受傷令他休養了幾周。歸隊後，他時常於飛行後出現嘔吐和頭痛的情況，性格也有所轉變。

里希特霍芬是個戰術高手，深信奧斯華·波爾克飛行戰術。雖然一些同僚不認為如此，但他常常被描述為一個冷淡、缺乏感情和非常嚴肅的人。\[9\]

有說於1918年，里希特霍芬已經成為一個傳奇，德國當局怕他陣亡會打擊[德國國民士氣](../Page/德國.md "wikilink")。他的上級一度要求他退役，但被他拒絕，他說支援地面士兵是他的責任，他沒有其他選擇只能繼續戰鬥。

## 死亡

[MvRichthofenWreckage_(2).jpg](https://zh.wikipedia.org/wiki/File:MvRichthofenWreckage_\(2\).jpg "fig:MvRichthofenWreckage_(2).jpg")
[Richthofen_crashsite.ogv](https://zh.wikipedia.org/wiki/File:Richthofen_crashsite.ogv "fig:Richthofen_crashsite.ogv")
里希特霍芬於1918年4月21日早上11時在[索姆河](../Page/索姆河.md "wikilink")（Somme River
）附近的[莫蘭角](../Page/莫蘭角.md "wikilink")（Morlancourt）飛行時被一顆.303口徑子彈擊中後身亡。

當時里希特霍芬正追逐一架隸屬[英國皇家空軍第](../Page/英國皇家空軍.md "wikilink")209中隊由[加拿大飛行員](../Page/加拿大.md "wikilink")[威勒弗立·美爾](../Page/威勒弗立·美爾.md "wikilink")（Wilfrid
May）駕駛的[索普威思駱駝式戰鬥機](../Page/索普威思駱駝式戰鬥機.md "wikilink")。在轉彎時里希特霍芬發現自己被另一架由[加拿大飛行員](../Page/加拿大.md "wikilink")[羅伊·布朗](../Page/羅伊·布朗.md "wikilink")（Roy
Brown）駕駛的駱駝戰鬥機跟隨。在紅男爵轉頭確認尾隨的駱駝式戰鬥機時，一顆致命的子彈自布朗的方向射出，擊中他的右下腹後對角穿透其胸腔並在他的左乳頭下方留下一個狹長的傷口。

里希特霍芬於一處由澳大利亞皇家部隊（Australian Imperial
Force）控制的地區勉強著陸，著陸後他的福克戰鬥機仍然保持完好。目擊者指當他們趕抵戰機旁時，里希特霍芬仍然活着，一會之後才死去。\[10\]
另一位目擊者，澳洲醫護兵團（Australian Medical Corps）的特德·斯模特（Ted
Smout）中士指里希特霍芬在臨死前說了最後一個字，「kaputt」（損壞或故障的意思）。\[11\]

里希特霍芬的遺體由最近的澳洲航空團第3飛行中隊負責處理。

### 誰殺死了紅男爵？

沒有人知道到底誰擊落紅男爵，[第一次世界大戰時大英帝國部隊的機槍和](../Page/第一次世界大戰.md "wikilink")[步槍均使用](../Page/步槍.md "wikilink").303口徑子彈。皇家空軍把擊落紅男爵的戰績記到阿瑟·布朗名下。但一些歷史學家、醫生和彈道專家則認為紅男爵是被地面的防空炮擊落，從紅男爵的傷口可看到子彈是向上移動，足以證明子彈由地面射出。

很多專家相信該顆致命子彈是由隸屬澳洲第24機槍連的[賽德里克·普堅斯](../Page/賽德里克·普堅斯.md "wikilink")（Cedric
Popkin）中士的[維克斯機槍射出](../Page/維克斯機槍.md "wikilink")\[12\]。普堅斯曾經兩次射擊里希特霍芬的座機：第一次在里希特霍芬座機直飛向他位置的時候，第二次在里希特霍芬座機朝他的位置右方600碼飛越時。後來普堅斯於1935年寄給澳洲官方戰爭史學家的信中聲稱，他相信是他在里希特霍芬座機第一次飛越他的位置時擊中里希特霍芬。但在這個位置射擊的話，子彈應該會擊中戰機前部，而不會導致里希特霍芬身亡\[13\]。然而，在里希特霍芬座機第二次飛越他的位置時，普堅斯的位置是最有可能射出那顆致命子彈的地方。而且，里希特霍芬從被擊中到死亡只有少於1分鐘的時間，普堅斯是唯一一位在那段時間從里希特霍芬右方射擊的地面機槍手。

最新的歷史謎團做進一步的實驗與研究證實，擊中紅男爵駕駛艙右側的並非 Cedric
Popkin，而可能是另一名澳洲的步兵在800碼以內的距離所射擊的，而當時紅男爵航線正返回基地的途中。

### 葬禮

第3飛行中隊的指揮官[大衛·布雷克](../Page/大衛·布雷克.md "wikilink")（David
Blake）最初認為里希特霍芬是被隸屬他的中隊的[RE8擊落](../Page/:en:Royal_Aircraft_Factory_R.E.8.md "wikilink")。但當布雷克目到驗屍的過程後，他堅信里希特霍芬是被地面防空砲火擊斃。

[RoteBaron.JPG](https://zh.wikipedia.org/wiki/File:RoteBaron.JPG "fig:RoteBaron.JPG")
與其他[協約國飛行員一樣](../Page/協約國.md "wikilink")，布雷克非常尊敬里希特霍芬，並為他舉行了一場軍人式葬禮。1918年4月22日，里希特霍芬的靈柩由6名上尉扶靈安葬於[法國](../Page/法國.md "wikilink")[亞眠附近一個墓地](../Page/亞眠.md "wikilink")，儀仗隊在旁鳴槍致敬。一些[協約國飛行中隊亦有致送花圈](../Page/協約國.md "wikilink")。

里希特霍芬的座機很多部份都被拆下來當纪念品。而發動機則捐贈到[倫敦的](../Page/倫敦.md "wikilink")[帝國戰爭博物館](../Page/帝國戰爭博物館.md "wikilink")，到現在仍然在館內展示。

1925年，里希特霍芬的弟弟取回其遺體並帶返家鄉。里希特霍芬的家人本來想把里希特霍芬的靈柩安葬於他的父親和弟弟的墓地旁。但德國當局提出希望將里希特霍芬安葬在[柏林傷殘軍人公墓](../Page/柏林傷殘軍人公墓.md "wikilink")（Invalidenfriedhof
Cemetery）內，與其他德國軍事英雄和領袖長眠。里希特霍芬家人同意。

### 腦部受損論說

2004年9月，[密蘇里大學研究員指里希特霍芬之死很可能是因為他頭部受傷後導致](../Page/密蘇里大學.md "wikilink")[腦部受損而引致](../Page/腦.md "wikilink")。這個論說由一位[德國研究員刊登於](../Page/德國.md "wikilink")[柳葉刀醫學期刊](../Page/柳葉刀醫學期刊.md "wikilink")\[14\]
。里希特霍芬頭部受傷後的行為與一些腦創傷病患無異，
而腦創傷可能令里希特霍芬缺乏判斷能力，因而在敵方領土上低飛和出現[目標定影](../Page/目標定影.md "wikilink")（target
fixation）的現象。

## 擊落數

[第一次世界大戰結束十年後](../Page/第一次世界大戰.md "wikilink")，一些作家開始質疑里希特霍芬的80架戰績，認為他的戰績記錄只是宣傳技倆。也有人認為他把中隊和聯隊的擊落數都計到自己的賬中。到了1998年[英國](../Page/英國.md "wikilink")[歷史學家諾曼](../Page/歷史學家.md "wikilink")·法蘭克和兩名同事於深入研究後出版了一本名為「紅男爵槍下」（*Under
the Guns of the Red
Baron*）的書籍。他們的調查確定了里希特霍芬最少擊落了73架敵機，連同不能證實的戰績里希特霍芬最高擊落數可能高達84架之多。不過，即便以擊落73架敵機計算，也無損於他一次大戰首席飛行王牌之名。

## 親戚

曼弗雷德·冯·里希特霍芬是第二次世界大戰[德國](../Page/納粹德國.md "wikilink")[空軍元帥](../Page/空軍元帥.md "wikilink")[沃爾弗拉姆·馮·里希特霍芬的遠房堂兄](../Page/沃爾弗拉姆·馮·里希特霍芬.md "wikilink")。[英國作家](../Page/英國.md "wikilink")[大衛·赫伯特·勞倫斯的太太](../Page/大衛·赫伯特·勞倫斯.md "wikilink")[弗里達·冯·里希特霍芬也是紅男爵的亲戚](../Page/弗里達·冯·里希特霍芬.md "wikilink")。雖然他們的共同祖先要追溯到1661年，但戰時紅男爵的名聲在英國多少也影響到弗里達。而弗里達的姊姊[伊利絲·冯·里希特霍芬則是](../Page/伊利絲·冯·里希特霍芬.md "wikilink")[德國首位女性](../Page/德國.md "wikilink")[社會科學家](../Page/社會科學.md "wikilink")。

紅男爵的弟弟，[洛塔·冯·里希特霍芬也是一名王牌飛行員](../Page/洛塔·冯·里希特霍芬.md "wikilink")，擊落數40架。他與哥哥同屬第11戰鬥機中隊，1922年死於[空難](../Page/空難.md "wikilink")。

紅男爵的姪孫，[赫爾曼·冯·里希特霍芬博士於](../Page/赫爾曼·冯·里希特霍芬.md "wikilink")1989年至1993年間是[德國駐](../Page/德國.md "wikilink")[英國](../Page/英國.md "wikilink")[大使](../Page/大使.md "wikilink")。

## 電影

  - 在1966年美國[好萊塢就里希特霍芬事績拍成電影](../Page/好萊塢.md "wikilink")《The Blue
    Max》（中譯：[藍徽特攻隊](../Page/藍徽特攻隊.md "wikilink")）。
  - 2008年德国电影[紅男爵](../Page/紅男爵_\(电影\).md "wikilink")

## 其他

  - 納粹德國[國防軍空軍第](../Page/國防軍.md "wikilink")2聯隊和現今[德國空軍第](../Page/德國空軍.md "wikilink")71聯隊以他的名字命名。值得一提的是，第71聯隊首任聯隊長是在二戰中創下352架擊墜紀錄的空前王牌[埃里希·哈特曼](../Page/埃里希·哈特曼.md "wikilink")。
  - [花生漫畫中的](../Page/花生漫畫.md "wikilink")[史努比經常想像自己回到第一次世界大戰跟他對戰](../Page/史努比.md "wikilink")，有款電腦射擊遊戲和[一首歌都叫做](../Page/:en:Snoopy_vs._the_Red_Baron_\(song\).md "wikilink")[史努比大戰紅男爵](../Page/:en:Snoopy_vs._the_Red_Baron.md "wikilink")。
  - 在漫畫"[天空霸者Z](../Page/天空霸者Z.md "wikilink")"中，被塑造成對希特勒盡忠到最後，與主角一路較勁的強敵

## 参考資料

### 来源

### 文献

  - Norman Franks, et al (1998). *Under the Guns of the Red Baron.* Grub
    Street, London.
  - Norman Franks and Alan Bennett (1997). *The Red Baron's Last
    Flight.* Grub Street, London.
  - Henning Allmers: Manfred Freiherr von Richthofen's Medical Record.
    Was the "Red Baron" fit to fly? *Lancet* 1999; 354: 502-4

## 外部連結

  - [Complete text of *The Red Fighter Pilot* by Manfred von
    Richthofen](http://www.richthofen.com/index.htm)
  - [*NOVA* "Who Killed the Red
    Baron?"](http://www.pbs.org/wgbh/nova/redbaron/)
  - [Manfred von Richthofen: 80 Aerial
    victories](http://www.acepilots.com/wwi/ger_richthofen.html)
  - [Manfred Von Richthofen's page on
    theaerodrome.com](http://theaerodrome.com/aces/germany/richthofen2.html)
  - [Article from *Aviation History
    Magazine*](https://web.archive.org/web/20060214000037/http://historynet.com/ahi/blredbaron/index1.html)
  - ["Snoopy vs the Red Baron" pop song
    lyrics](https://web.archive.org/web/20070114182239/http://www.niehs.nih.gov/kids/lyrics/snoopyred.htm)
  - [Prominent People - Manfred von
    Richthofen](http://www.prominentpeople.co.za/people/60.php)
  - [Info, Pictures and Links about
    Richthofen](http://www.freewebs.com/spitfire34/thebluemax.htm)
  - [An English translation of the account of Bolko v. Richthofen's
    journey to retrieve Manfred's
    body](http://www.jastaboelcke.de/aces/m_v_richthofen/coming_home01.htm)
  - [Famous flying ace no hotshot after
    all](http://www.newscientist.com/article.ns?id=dn9637&feedId=online-news_rss20)
    - *New Scientist*
  - [Official Website](http://redbaron-themovie.com/) of the movie *The
    Red Baron (2007)*
  - [The Kaiser's Hero: Manfred von
    Richthofen](http://members.tripod.com/~Nevermore/richthofen.html)
  - [A painting of Richthofen in
    flight](https://web.archive.org/web/20070202015050/http://www.starduststudios.com/Von_Richthofen.htm)

[Category:德国飞行员](../Category/德国飞行员.md "wikilink")
[Category:德国王牌飞行员](../Category/德国王牌飞行员.md "wikilink")
[Category:德國貴族](../Category/德國貴族.md "wikilink")
[Category:战争身亡者](../Category/战争身亡者.md "wikilink")
[Category:德國第一次世界大戰人物](../Category/德國第一次世界大戰人物.md "wikilink")
[Category:西里西亞人](../Category/西里西亞人.md "wikilink")

1.
2.
3.  [Detailed list of Manfred von Richthofen's air
    victories](http://www.waffenhq.de/specials/richthofen.htm)
4.  Wright 1976, p. 31.
5.  Wright 1976, p. 30.
6.  Preußen. Kriegsministerium. Geheime Kriegs-Kanzlei, *Rangliste der
    Königlich Preußischen Armee und des XIII. (Königlich
    Württembergischen) Armeekorps für 1914. Mit den Dienstalterlisten
    der Generale und Stabsoffiziere und einem Anhange enthaltend das
    [Reichsmilitärgericht](../Page/Reichsmilitärgericht.md "wikilink"),
    die Marine-Infanterie, die Kaiserlichen Schutztruppen und die
    Gendarmerie-Brigade in Elsaß-Lothringen. Nach dem Stande vom 6. Mai
    1914. Auf Befehl Seiner Majestät des Kaisers und Königs* (Berlin:
    Ernst Siegfried Mittler und Sohn, 1914), p. 400.
7.  [von Richthofen's autobiography: Early battlefield
    experiences](http://www.richthofen.com/arcdocs/richt_02.htm)
8.  [von Richthofen's autobiography: Transfer to the
    Luftstreitkräfte](http://www.richthofen.com/arcdocs/richt_03.htm)
9.  Hunting With Richtofen by Karl Bodenschatz,ISBN 1-898697-97-3
10. [Dr Geoffrey Miller, 1998, "The Death of Manfred von Richthofen: who
    fired the fatal shot?", in *Sabretache: Journal and Proceedings of
    the Military History Society of Australia*, vol. XXXIX, no.
    2](http://www.lib.byu.edu/~rdh/wwi/comment/richt.htm)
11. Unsolved History: Death of the Red Baron, 2002, Produced by Termite
    Art Productions for Discovery Channel
12. Miller, 1998, *同上*.
13. Unsolved History, 2002, *同上*.
14. *Lancet*. 1999 Aug 7; 354 (9177): 502-4.