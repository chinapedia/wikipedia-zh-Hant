**曼迪傳播有限公司**（**Mighty Media Co.,
Ltd.**），是[台灣的一家](../Page/台灣.md "wikilink")[品牌授權](../Page/品牌.md "wikilink")、[動畫代理發行](../Page/動畫.md "wikilink")[公司](../Page/公司.md "wikilink")。

## 概要

  - 1991年4月1日，**[滿天星傳播有限公司](../Page/滿天星傳播.md "wikilink")**（*Astar Media
    Productions Co., Ltd.*）成立。
  - 1999年7月1日，滿天星傳播成立曼迪傳播，主要代理的範圍為[日本動畫](../Page/日本動畫.md "wikilink")、[特攝作品等](../Page/特攝.md "wikilink")。
  - 2014年6月17日，曼迪傳播第一個[電視頻道](../Page/電視頻道.md "wikilink")「曼迪日本台」於[中華電信MOD第](../Page/中華電信MOD.md "wikilink")95頻道開播(2017年11月1日起為372頻道)，定位為[高畫質日本綜合頻道](../Page/高畫質電視.md "wikilink")。

## 組織架構

  - 台灣

<!-- end list -->

  - 曼迪傳播有限公司

<!-- end list -->

  - 香港

<!-- end list -->

  - 曼迪品牌行銷有限公司（Mighty Brands Co., Ltd.）

<!-- end list -->

  - 亞洲

<!-- end list -->

  - 曼迪三角貿易有限公司（Mighty Delta Investments Limited）

## 日本動畫

<div style="-moz-column-count:2; column-count:2;">

### 字母開頭

  - [another](../Page/another.md "wikilink")
  - [BECK搖滾新樂團](../Page/BECK.md "wikilink")（）
  - [戰國BASARA](../Page/戰國BASARA.md "wikilink")
  - [CANAAN](../Page/CANAAN.md "wikilink")
  - [From I"s](../Page/I"s.md "wikilink")（）
  - [I"s Pure](../Page/I"s.md "wikilink")（I"s Pure）
  - [Fate/Zero](../Page/Fate/Zero.md "wikilink")
  - [GOSICK](../Page/GOSICK.md "wikilink")
  - [K](../Page/K_\(動畫\).md "wikilink")
  - [Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")（）
  - [S·A特優生](../Page/S·A特優生.md "wikilink")
  - [SHIROBAKO](../Page/SHIROBAKO.md "wikilink")（中文正式譯名白箱）
  - [ToHeart 青春紀事](../Page/To_Heart.md "wikilink")（ToHeart ～Remember my
    memories～）
  - [TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink")
  - [YUYU式](../Page/YUYU式.md "wikilink")
  - [AKB0048](../Page/AKB0048.md "wikilink")
  - [Free\!](../Page/Free!.md "wikilink")
  - [GATE 奇幻自衛隊](../Page/GATE_奇幻自衛隊.md "wikilink")（）

### 一字部

  - [紅 Kure-nai](../Page/紅_\(小說\).md "wikilink")（）

### 二字部

  - [舞-HiME](../Page/舞-HiME.md "wikilink")
  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")
  - [爆丸](../Page/爆丸.md "wikilink")（）
  - [双戀](../Page/双戀.md "wikilink")（）
  - [双戀2](../Page/双戀.md "wikilink")（）
  - [月詠](../Page/月詠.md "wikilink")（）
  - [詩片](../Page/詩片.md "wikilink")（）
  - [涼風](../Page/涼風.md "wikilink")
  - [亂馬1/2](../Page/亂馬1/2.md "wikilink")
  - [信蜂](../Page/信蜂.md "wikilink")

### 三字部

  - [天使心](../Page/天使心_\(漫畫\).md "wikilink")（）
  - [棋靈王](../Page/棋靈王.md "wikilink")（）
  - [犬夜叉](../Page/犬夜叉.md "wikilink")
  - [妖逆門](../Page/妖逆門.md "wikilink")

### 四字部

  - [火影忍者](../Page/火影忍者.md "wikilink")（）
      - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")
  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")
  - [音速小子X](../Page/音速小子X.md "wikilink")
  - [花漾明星KIRARIN](../Page/偶像宣言.md "wikilink")（）
  - [交響詩篇](../Page/交響詩篇艾蕾卡7.md "wikilink")（）
  - [魔兵傳奇](../Page/MÄR_魔法世界.md "wikilink")（）
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")（）
  - [熱拳本色](../Page/熱拳本色.md "wikilink")（）
  - [鬥牌傳說](../Page/鬥牌傳說.md "wikilink")（）
  - [風人物語](../Page/風人物語.md "wikilink")
  - [玻璃假面](../Page/玻璃假面.md "wikilink")（）
  - [幽遊白書](../Page/幽遊白書.md "wikilink")（）
  - [天地無用GXP](../Page/天地無用!.md "wikilink")（）
  - [天地無用魎皇鬼](../Page/天地無用!.md "wikilink")（）
  - [南國少年奇小邪](../Page/南國少年奇小邪.md "wikilink")（）
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（GTO）
  - [修羅之刻](../Page/修羅之刻.md "wikilink")（）
  - [變身偵探](../Page/變身偵探.md "wikilink")（）
  - [純愛物語](../Page/真愛故事.md "wikilink")（True Love Story Summer Days, and
    yet...）
  - [哨聲響起](../Page/哨聲響起.md "wikilink")（）
  - [妹妹公主](../Page/妹妹公主.md "wikilink")（）
      - [妹妹公主RePure](../Page/妹妹公主.md "wikilink")（）
  - [超能奇兵](../Page/超能奇兵.md "wikilink")（）
  - [麵包超人](../Page/麵包超人.md "wikilink")（）
  - [愛你寶貝](../Page/愛你寶貝.md "wikilink")（）
  - [鈴鐺貓娘](../Page/鈴鐺貓娘.md "wikilink")（）
  - [戰鬥陀螺G](../Page/戰鬥陀螺.md "wikilink")（）
  - [神鵰俠侶](../Page/神鵰俠侶_\(動畫\).md "wikilink")（）
  - [守護甜心](../Page/守護甜心.md "wikilink")（）
  - [零秒出手](../Page/零秒出手_\(動畫\).md "wikilink")
  - [鐵馬少年](../Page/鐵馬少年.md "wikilink")
  - [戀姬無雙](../Page/戀姬†無雙.md "wikilink")
  - [戀愛班長](../Page/戀愛班長.md "wikilink")（）
  - [神曲奏界](../Page/神曲奏界.md "wikilink")
  - [蒼天之拳](../Page/蒼天之拳.md "wikilink")
  - [玩偶遊戲](../Page/玩偶遊戲.md "wikilink")（）
  - [四葉遊戲](../Page/四葉遊戲.md "wikilink")
  - [白色相簿](../Page/白色相簿.md "wikilink")（）
  - [水晶之焰](../Page/水晶之焰.md "wikilink")（）
  - [冒險遊記](../Page/冒險遊記.md "wikilink")（）
  - [奇蹟列車](../Page/奇蹟列車.md "wikilink")
  - [東之伊甸](../Page/東之伊甸.md "wikilink")
  - [天使特警 Kiddy Girl](../Page/KIDDY_GiRL-AND.md "wikilink")
  - [宇宙兄弟](../Page/宇宙兄弟.md "wikilink")
  - [藍海少女！](../Page/藍海少女！.md "wikilink")（）
  - [動物朋友](../Page/動物朋友.md "wikilink")（）
  - [玩偶遊戲](../Page/玩偶遊戲.md "wikilink")

### 五字部

  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")（）
  - [狼與辛香料](../Page/狼與辛香料.md "wikilink")（）
  - [死亡筆記本](../Page/死亡筆記.md "wikilink")（DEATH NOTE）
  - [灼眼的夏娜](../Page/灼眼的夏娜_\(動畫\).md "wikilink")（）
  - [今日大魔王](../Page/魔之系列#電視動畫.md "wikilink")（）
  - [棒球大聯盟](../Page/棒球大聯盟.md "wikilink")（）
  - [光之美少女](../Page/光之美少女.md "wikilink")（）
      - [光之美少女 Max Heart](../Page/光之美少女.md "wikilink")（）
      - [光之美少女 Splash Star](../Page/光之美少女Splash_Star.md "wikilink")（）
      - [Suite 光之美少女♪](../Page/Suite_光之美少女♪.md "wikilink")（）
  - [蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")（）
  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（）
  - [機動新撰組](../Page/機動新撰組.md "wikilink")（）
  - [陰陽大戰記](../Page/陰陽大戰記.md "wikilink")（）
  - [植木的法則](../Page/植木的法則.md "wikilink")（）
  - [馳風！競艇王](../Page/馳風！競艇王.md "wikilink")（）
      - 馳風！競艇王V（）
  - [學園愛麗絲](../Page/學園愛麗絲.md "wikilink")（）
  - [吟遊默示錄](../Page/吟遊默示錄.md "wikilink")（）
  - [機獸超世紀](../Page/機獸超世紀.md "wikilink")（）
  - [危險調查員](../Page/危險調查員.md "wikilink")（）
  - [我們的仙境](../Page/我們的仙境.md "wikilink")（）
  - [我們的存在](../Page/我們的存在.md "wikilink")（）
  - [美鳥伴身邊](../Page/美鳥日記.md "wikilink")（）
  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（）
  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（）
  - [狗狗向前走](../Page/狗狗向前走.md "wikilink")（）
  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")（）
  - [朝霧的巫女](../Page/朝霧的巫女.md "wikilink")（）
  - [陸上防衛隊](../Page/陸上防衛隊.md "wikilink")（）
  - [成惠的世界](../Page/成惠的世界.md "wikilink")（）
  - [小豬萬萬歲](../Page/小豬萬萬歲.md "wikilink")
  - [戀愛占卜師](../Page/戀愛占卜師.md "wikilink")
  - [暗夜第六感](../Page/暗夜第六感.md "wikilink")
  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（）
  - [浪漫追星社](../Page/浪漫追星社.md "wikilink")（）
  - [夢色蛋糕師](../Page/夢色蛋糕師.md "wikilink")
  - [小超人帕門 (第2作)](../Page/小超人帕門.md "wikilink")（）
  - [黑色五葉草](../Page/黑色五葉草.md "wikilink")（ ブラッククローバー Black Clover ）

### 六字部

  - [奇天烈大百科](../Page/奇天烈大百科.md "wikilink")（）
  - [笑園漫畫大王](../Page/笑園漫畫大王.md "wikilink")（）
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（）
  - [魔法咪路咪路](../Page/魔法咪路咪路.md "wikilink")（）
  - [魔法少女加奈](../Page/魔法少女加奈.md "wikilink")（（TV版））
  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（）
  - [魔法使的新娘](../Page/魔法使的新娘.md "wikilink")（）
  - [夏色的砂時計](../Page/夏色的砂時計.md "wikilink")（）
  - [快樂小丸日記](../Page/快樂小丸日記.md "wikilink")（）
  - [高機動交響曲GPO](../Page/高機動交響曲GPO.md "wikilink")
  - [天才麻將少女](../Page/咲_-Saki-.md "wikilink")（）
  - [二十面相之娘](../Page/二十面相少女.md "wikilink")（二十面相の娘）
  - [格鬥美神 武龍](../Page/格鬥美神_武龍.md "wikilink")
  - [春&夏～春太與千夏的青春～](../Page/春&夏事件簿.md "wikilink")（）

### 七字部

  - [英國戀物語 艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")（）
  - [魔法少女砂沙美](../Page/砂沙美魔法少女俱樂部.md "wikilink")（）
  - [幻想魔傳最遊記](../Page/幻想魔傳最遊記.md "wikilink")（）
  - [夕陽染紅的坡道](../Page/染紅的街道.md "wikilink")（）
  - [學生會長是女僕](../Page/學生會長是女僕.md "wikilink")
  - [電波女與青春男](../Page/電波女與青春男.md "wikilink")
  - [吹響吧！上低音號](../Page/奏響吧！上低音號～歡迎加入北宇治高中管樂團～.md "wikilink")（）
  - [齊木楠雄的災難](../Page/齊木楠雄的災難.md "wikilink")（）

### 八字部

  - [我家有個狐仙大人](../Page/我家有個狐仙大人.md "wikilink")
  - [乃木坂春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")
  - [戰鬥陀螺 鋼鐵奇兵](../Page/戰鬥陀螺_鋼鐵戰魂.md "wikilink")
  - [北斗神拳-天之霸王](../Page/北斗神拳.md "wikilink")
  - [薄櫻鬼 ～新選組奇譚～](../Page/薄櫻鬼_～新選組奇譚～.md "wikilink")
  - [王牌投手 振臂高揮](../Page/王牌投手_振臂高揮.md "wikilink")（）
  - [月刊少女野崎同學](../Page/月刊少女野崎同學.md "wikilink")（）
  - [無彩限的幻影世界](../Page/無彩限的幻影世界.md "wikilink")（）

### 八字部以上

  - [藥師寺涼子之怪奇事件簿](../Page/藥師寺涼子之怪奇事件簿.md "wikilink")

</div>

## 特攝

  - [假面騎士龍騎](../Page/假面騎士龍騎.md "wikilink")（）
  - [假面騎士FAIZ](../Page/假面騎士555.md "wikilink")（）
  - [假面騎士劍](../Page/假面騎士劍.md "wikilink")（）
  - [假面騎士響鬼](../Page/假面騎士響鬼.md "wikilink")（）
  - [假面騎士KABUTO](../Page/假面騎士KABUTO.md "wikilink")（）
  - [假面騎士電王](../Page/假面騎士電王.md "wikilink")（）
  - [假面騎士KIVA](../Page/假面騎士KIVA.md "wikilink")（）

## 日本電影

  - [北之零年](../Page/北之零年.md "wikilink")（）
  - [69](../Page/69_sixty_nine.md "wikilink")（）
  - [海貓](../Page/海貓.md "wikilink")（）
  - [可瑪猫](../Page/可瑪猫.md "wikilink")（）
  - [蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")（）
  - [惡童當街](../Page/惡童當街.md "wikilink")（）
  - [鬼太郎](../Page/鬼太郎.md "wikilink")（）
  - [幸福的三丁目](../Page/幸福的三丁目.md "wikilink")
  - [琴之森](../Page/琴之森.md "wikilink")（）
  - [河童之夏](../Page/河童之夏.md "wikilink")（）
  - [夏日大作戰](../Page/夏日大作戰.md "wikilink")（）
  - [追逐繁星的孩子](../Page/追逐繁星的孩子.md "wikilink")（）
  - [言叶之庭](../Page/言叶之庭.md "wikilink")（）

## 參考資料

## 外部連結

  - [曼迪台灣](http://www.mightymedia.com.tw/)

  - [曼迪香港](http://www.mightymedia.com.hk/)

  -
  -
  -
  - [中華電信MOD
    曼迪日本台](https://web.archive.org/web/20151123000545/http://mod.cht.com.tw/channel/channelinfo.php?chid=095%2F)

[M曼](../Category/動畫產業公司.md "wikilink")
[M曼](../Category/總部位於新北市的工商業機構.md "wikilink")
[M曼](../Category/1999年成立的公司.md "wikilink")