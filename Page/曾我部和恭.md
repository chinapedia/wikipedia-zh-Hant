**曾我部和恭**是[日本資深](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")，[千葉縣出身](../Page/千葉縣.md "wikilink")。他於2000年12月31日退休，之後他的配音角色由[稲田徹和](../Page/稲田徹.md "wikilink")[置鮎龍太郎擔任](../Page/置鮎龍太郎.md "wikilink")。2006年9月17日，因患[食道癌去世](../Page/食道癌.md "wikilink")，享年58歲。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - （**波利瑪**）

  - [宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")（山本明）

  - [機動戰士GUNDAM (初代)](../Page/機動戰士GUNDAM_\(初代\).md "wikilink")

  - [元氣爆發](../Page/元氣爆發.md "wikilink")（黑暗琉斯三世）

  - [聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink")（教皇亞歷士、雙子座撒加、雙子座加隆）

  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")（）

  - [波羅五號](../Page/波羅五號.md "wikilink")（峰一平）

  - [七龍珠Z](../Page/七龙珠.md "wikilink")（Tōma）

  - [世界名作劇場系列](../Page/世界名作劇場.md "wikilink")

      - [尋母三千里](../Page/尋母三千里.md "wikilink")（東尼奧·羅西）
      - [紅髮安妮](../Page/紅髮安妮_\(動畫\).md "wikilink")（艾倫牧師）
      - [愛的小婦人物語](../Page/愛的小婦人物語.md "wikilink")（安東尼·布恩）

  - [美少女戰士](../Page/美少女戰士_\(動畫\).md "wikilink")（**Kunzite**）

  - [北斗神拳](../Page/北斗神拳.md "wikilink")（南斗五車星之‘風’飄逸）

  - [幽遊白書](../Page/幽遊白書.md "wikilink")（画魔，鈴木）

  - [巴黎伊莎貝爾](../Page/巴黎伊莎貝爾.md "wikilink")（維克多）

  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（本・貝克曼）

  - [流浪少女娜兒](../Page/流浪少女娜兒.md "wikilink")（馬里歐·崔恩特（神秘男子））

  - [名偵探福爾摩斯](../Page/名偵探福爾摩斯.md "wikilink")（麥克班恩先生（麥克））

  - [GS美神](../Page/GS美神極樂大作戰.md "wikilink")（**唐巢神父**）

  - [灌籃高手](../Page/灌籃高手.md "wikilink")（杉山祥太）

### OVA

  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")——クリスチアン
  - [吹泡糖危機](../Page/吹泡糖危机.md "wikilink")——ラルゴ
  - [吸血鬼獵人D](../Page/吸血鬼獵人D.md "wikilink")——麗銀星

### 劇場版

  - [機動戰士GUNDAM 〈初代〉](../Page/機動戰士GUNDAM_〈初代〉.md "wikilink")（ワッケイン司令）
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（ライル艦長）
  - [七龍珠Z](../Page/七龙珠.md "wikilink")（人造人13號）

**1987年**

  - [王立宇宙軍～歐尼亞米斯之翼～](../Page/王立宇宙軍.md "wikilink")（馬提）

### 遊戲

  - [超級機器人大戰](../Page/超級機器人大戰.md "wikilink")——峰一平、ジーン、ビショット・ハッタ
  - [潛龍諜影](../Page/潛龍諜影.md "wikilink")——Psycho Mantis

### 廣播

  - [魔神英雄傳](../Page/魔神英雄傳.md "wikilink")——陵候

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")
[Category:罹患食道癌逝世者](../Category/罹患食道癌逝世者.md "wikilink")