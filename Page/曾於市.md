**曾於市**（）是位于[日本](../Page/日本.md "wikilink")[鹿兒島縣東部](../Page/鹿兒島縣.md "wikilink")[城市](../Page/城市.md "wikilink")，成立于2005年7月1日，由舊[曾於郡的](../Page/曾於郡.md "wikilink")[末吉町](../Page/末吉町.md "wikilink")、[財部町](../Page/財部町.md "wikilink")、[大隅町合併而成](../Page/大隅町.md "wikilink")。由於與接鄰的[宮崎縣的](../Page/宮崎縣.md "wikilink")[都城市往來方便](../Page/都城市.md "wikilink")，因而被列入[都城都市圈的範圍](../Page/都城都市圈.md "wikilink")。

轄內的舊財部町和舊末吉町地區的電話區域號與其他鹿兒島縣的地區使用的099字頭不同，而是與宮崎縣和[沖繩縣相同的](../Page/沖繩縣.md "wikilink")098字頭，但舊大隅町地區則是使用鹿兒島縣使用的099字頭。

## 歷史

[明治時代之前](../Page/明治時代.md "wikilink")，曾於市的轄區分別隸屬[日向國和](../Page/日向國.md "wikilink")[大隅國](../Page/大隅國.md "wikilink")，當時兩國的境界，就從現在的轄區內通過。\[1\]

### 年表

  - 1897年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區分別屬於[囎唹郡末吉村](../Page/囎唹郡.md "wikilink")、[岩川村](../Page/岩川村.md "wikilink")、[恒吉村](../Page/恒吉村.md "wikilink")、財部村、[月野村](../Page/月野村.md "wikilink")、[野方村](../Page/野方村.md "wikilink")。
  - 1922年10月1日：末吉村改制為[末吉町](../Page/末吉町.md "wikilink")。
  - 1926年4月1日：財部村改制為[財部町](../Page/財部町.md "wikilink")。
  - 1955年1月20日：岩川町、恒吉村、月野村[合併為](../Page/市町村合併.md "wikilink")[大隅町](../Page/大隅町.md "wikilink")。
  - 1955年4月1日：野方村被廢除，其中的荒股地區被併入大隅町。
  - 1972年4月1日：囎唹郡改名為曾於郡。
  - 2005年7月1日：大隅町、末吉町、財部町合併為**曾於市**。

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年－1910年</p></th>
<th><p>1910年－1930年</p></th>
<th><p>1930年－1950年</p></th>
<th><p>1950年－1970年</p></th>
<th><p>1970年－1990年</p></th>
<th><p>1990年－現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>曾於郡<br />
末吉村</p></td>
<td><p>1922年10月1日<br />
改制為末吉町</p></td>
<td><p>2005年7月1日<br />
合併為曾於市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>曾於郡<br />
財部村</p></td>
<td><p>1926年4月1日<br />
改制為財部町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>曾於郡<br />
岩川村</p></td>
<td><p>1924年4月1日<br />
改制為岩川町</p></td>
<td><p><br />
<br />
<br />
1955年1月20日<br />
合併為大隅町<br />
<br />
<br />
　</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>曾於郡<br />
恒吉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>曾於郡<br />
月野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>曾於郡<br />
野方村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>曾於郡<br />
大崎町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>曾於郡<br />
大崎村</p></td>
<td><p>1936年10月1日<br />
改制為大崎町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[財部車站](../Page/財部車站.md "wikilink")
        - [北俣車站](../Page/北俣車站.md "wikilink") -
        [大隅大川原車站](../Page/大隅大川原車站.md "wikilink")

過去曾有國鐵志布志線通過現在的轄區，但已於1987年停駛。

  - [國鐵](../Page/國鐵.md "wikilink")
      - [志布志線](../Page/志布志線.md "wikilink")：[末吉車站](../Page/末吉車站.md "wikilink")
        - [岩北車站](../Page/岩北車站.md "wikilink") -
        [岩川車站](../Page/岩川車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [東九州自動車道](../Page/東九州自動車道.md "wikilink")：[大隅休息區](../Page/大隅休息區.md "wikilink")
    - [曾於彌五郎交流道](../Page/曾於彌五郎交流道.md "wikilink") -
    [末吉財部交流道](../Page/末吉財部交流道.md "wikilink")

## 教育

### 高等學校

  - [鹿兒島縣立財部高等學校](../Page/鹿兒島縣立財部高等學校.md "wikilink")
  - [鹿兒島縣立末吉高等學校](../Page/鹿兒島縣立末吉高等學校.md "wikilink")
  - [鹿兒島縣立岩川高等學校](../Page/鹿兒島縣立岩川高等學校.md "wikilink")

## 本地出身之名人

  - [山中貞則](../Page/山中貞則.md "wikilink")：前眾議院議員
  - [吉井淳二](../Page/吉井淳二.md "wikilink")：西洋畫家
  - [北別府學](../Page/北別府學.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [東國原英夫](../Page/東國原英夫.md "wikilink")：[宮崎縣](../Page/宮崎縣.md "wikilink")[知事](../Page/知事.md "wikilink")、前[藝人](../Page/藝人.md "wikilink")、生於舊末吉町，成長於宮崎縣都城市。
  - [大園桃子](../Page/大園桃子.md "wikilink")：乃木坂46

## 參考資料

## 外部連結

  - [曾於北部合併協議會](https://web.archive.org/web/20070821043107/http://www.city.soo.kagoshima.jp/sohokubu/index.htm)

<!-- end list -->

1.