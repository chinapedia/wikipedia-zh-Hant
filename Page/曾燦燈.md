**曾燦燈**（1953年2月2日－），[台灣](../Page/台灣.md "wikilink")[南投縣人](../Page/南投縣.md "wikilink")，曾代表[台灣團結聯盟任職](../Page/台灣團結聯盟.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。前任[高苑科技大學校長](../Page/高苑科技大學.md "wikilink")。

## 學歷

  - 美國[北科羅拉多州立大學教育行政管理博士](../Page/北科羅拉多州立大學.md "wikilink")。
  - [國立台灣師範大學教育研究所博士班](../Page/國立台灣師範大學.md "wikilink")。
  - 國立台灣師範大學教育研究所碩士。
  - 國立台灣師範大學教育系學士。

## 經歷

  - 高苑科技大學校長
  - [國立台南大學教育經營與管理研究所教授所長](../Page/國立台南大學.md "wikilink")
  - [國立成功大學企管研究所高階企管碩士EMBA兼任教授](../Page/國立成功大學.md "wikilink")
  - [國立高雄師範大學教育系兼任教授](../Page/國立高雄師範大學.md "wikilink")
  - [長榮大學經營管理研究所兼任教授](../Page/長榮大學.md "wikilink")
  - 中華民國[生命線全國總會副秘書長](../Page/生命線_\(熱線\).md "wikilink")
  - [教育部發展與改進師範教育五年教育計劃起草](../Page/教育部_\(中華民國\).md "wikilink")
  - 教育部各級教育評鑑委員
  - [港都有線電視](../Page/港都有線電視.md "wikilink")《港都焦點》節目主持人

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00151&stage=6)

## 語錄

  - 阿扁（[陳水扁](../Page/陳水扁.md "wikilink")）說要[追求獨立](../Page/台灣獨立.md "wikilink")，請問阿扁現在是哪一國總統？（2007年3月4日，諷刺陳水扁提出的「[四要一沒有](../Page/四要一沒有.md "wikilink")」中的「台灣要獨立」。）

[Z曾](../Category/第6屆中華民國立法委員.md "wikilink")
[Z曾](../Category/台灣團結聯盟黨員.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")