**曾燾**（），[字](../Page/表字.md "wikilink")**煦白**，[筆名](../Page/筆名.md "wikilink")**虛白**，是一位生於[江蘇](../Page/江蘇.md "wikilink")[常熟的](../Page/常熟.md "wikilink")[媒體工作者](../Page/傳媒工作者.md "wikilink")。

## 生平

1894年4月25日出生，為[民初小說家](../Page/民初.md "wikilink")**[曾樸](../Page/曾樸.md "wikilink")**之長子。畢業於[上海](../Page/上海.md "wikilink")[聖約翰大學](../Page/聖約翰大學.md "wikilink")。1927年在[天津參與創辦](../Page/天津.md "wikilink")《**[庸報](../Page/庸報.md "wikilink")**》。1928年在上海與其父創辦[真善美書店](../Page/真善美書店.md "wikilink")。1932年2月在上海創辦《**[大晚報](../Page/大晚報.md "wikilink")**》。曾任國民政府行政院新聞局副局長。1949年遷居台灣，任**[中央通訊社](../Page/中央通訊社.md "wikilink")**社長、**[國立政治大學新聞學系](../Page/國立政治大學.md "wikilink")**主任兼新聞研究所所長。1974年八十大壽時，將各界壽禮捐設成立「**曾虛白先生新聞事業獎基金**」（今**[曾虛白先生新聞獎基金會](../Page/曾虛白先生新聞獎基金會.md "wikilink")**），設置“**[曾虛白先生新聞獎](../Page/曾虛白先生新聞獎.md "wikilink")**”。1986年獲頒[行政院文化獎](../Page/行政院文化獎.md "wikilink")。1992年獲[中國文藝協會頒發](../Page/中國文藝協會.md "wikilink")[中國文藝獎章榮譽獎章](../Page/中國文藝獎章.md "wikilink")。1994年1月5日去世。

## 著作

《中國新聞史》、《民意原理》、《工業民主制度之理論與實踐》等。

[Category:金鐘獎特別貢獻獎得主](../Category/金鐘獎特別貢獻獎得主.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中廣總經理](../Category/中廣總經理.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:上海聖約翰大學校友](../Category/上海聖約翰大學校友.md "wikilink")
[Category:中華民國記者](../Category/中華民國記者.md "wikilink")
[Category:台灣戰後江蘇移民](../Category/台灣戰後江蘇移民.md "wikilink")
[Category:常熟人](../Category/常熟人.md "wikilink")
[Category:中央通訊社社長](../Category/中央通訊社社長.md "wikilink")
[Category:行政院文化獎得主](../Category/行政院文化獎得主.md "wikilink")
[主任委員](../Category/中央通訊社董事長.md "wikilink")
[S虛](../Category/曾姓.md "wikilink")