**曾貴章**（1967年8月1日－），[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[時報鷹](../Page/時報鷹.md "wikilink")，1997年因職棒簽賭案而遭到[中華職棒終身禁賽](../Page/中華職棒.md "wikilink")，退休後任職[台北市政府養工處](../Page/台北市政府.md "wikilink")。

曾貴章曾獨創「不良打法」，類似日本[鈴木一朗的風格](../Page/鈴木一朗.md "wikilink")，1996年創下中職單季最多安打紀錄143支，超越[兄弟象前洋將](../Page/兄弟象.md "wikilink")[路易士保持的](../Page/路易士·度士.md "wikilink")136支安打，此一紀錄高懸11年，2007年由[統一獅](../Page/統一獅.md "wikilink")[高國慶打破](../Page/高國慶.md "wikilink")。

## 經歷

  - [高雄縣五甲國小少棒隊](../Page/高雄縣.md "wikilink")
  - [高雄縣五甲國中青少棒隊](../Page/高雄縣.md "wikilink")
  - [台南縣官田國中青少棒隊](../Page/台南縣.md "wikilink")
  - [臺北縣](../Page/新北市.md "wikilink")[中華中學青棒隊](../Page/中華中學.md "wikilink")（榮工）
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")
  - [台北市政府養工處](../Page/台北市政府.md "wikilink")

## 職棒生涯成績

（**黑體字**為該年聯盟最高紀錄）

| 年度    | 球隊                               | 出賽      | 打數   | 安打      | 全壘打 | 打點  | 盜壘 | 四死  | 三振  | 壘打數 | 雙殺打 | 打擊率       |
| ----- | -------------------------------- | ------- | ---- | ------- | --- | --- | -- | --- | --- | --- | --- | --------- |
| 1993年 | [時報鷹](../Page/時報鷹.md "wikilink") | 85      | 323  | **109** | 7   | 51  | 15 | 23  | 38  | 154 | 3   | **0.337** |
| 1994年 | [時報鷹](../Page/時報鷹.md "wikilink") | 88      | 344  | 117     | 3   | 56  | 11 | 46  | 43  | 152 | 6   | 0.340     |
| 1995年 | [時報鷹](../Page/時報鷹.md "wikilink") | **100** | 415  | 127     | 9   | 70  | 4  | 41  | 55  | 176 | 8   | 0.306     |
| 1996年 | [時報鷹](../Page/時報鷹.md "wikilink") | 99      | 414  | **143** | 10  | 78  | 17 | 36  | 72  | 199 | 6   | 0.345     |
| 1997年 | [時報鷹](../Page/時報鷹.md "wikilink") | 43      | 141  | 63      | 6   | 28  | 7  | 63  | 18  | 18  | 0   | 0.368     |
| 合計    | 5年                               | 415     | 1667 | 559     | 31  | 283 | 59 | 165 | 226 | 777 | 23  | 0.335     |

## 外部連結

[Z](../Category/曾姓.md "wikilink") [Z](../Category/在世人物.md "wikilink")
[Z](../Category/1967年出生.md "wikilink")
[Z](../Category/台灣棒球選手.md "wikilink")
[Z](../Category/時報鷹隊球員.md "wikilink")
[Z](../Category/中華成棒隊球員.md "wikilink")
[Z](../Category/中華職棒打擊王.md "wikilink")
[Z](../Category/中華職棒安打王.md "wikilink")
[Z](../Category/中華職棒年度新人王.md "wikilink")
[Z](../Category/中華職棒單月最有價值球員獎得主.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")