《**最终幻想VII补完计划**》（Compilation of Final Fantasy
VII），是[史克威爾艾尼克斯發行的一系列](../Page/史克威爾艾尼克斯.md "wikilink")[視訊遊戲](../Page/視訊遊戲.md "wikilink")、[動畫及](../Page/動畫.md "wikilink")[短篇小說作品群](../Page/短篇小說.md "wikilink")，所有相关作品均建構於《[最终幻想VII](../Page/最终幻想VII.md "wikilink")》的世界觀與劇情之上。該系列作品由[野村哲也](../Page/野村哲也.md "wikilink")、[北瀨佳範領頭創作](../Page/北瀨佳範.md "wikilink")，在不同平台上开发制作多個不同的作品，但都延伸自《最终幻想VII》的故事核心。\[1\]\[2\]\[3\]

《[最终幻想](../Page/最终幻想.md "wikilink")》系列有一集完結的原則。不過，在2003年發售《[最终幻想X](../Page/最终幻想X.md "wikilink")》的續編《[最终幻想X-2](../Page/最终幻想X-2.md "wikilink")》後，制作团队改變了方針。於是特別受歡迎的《[最终幻想VII](../Page/最终幻想VII.md "wikilink")》相繼發表續編及外傳，這個相關作品系列計劃就被稱为《最终幻想VII补完计划》。\[4\]

关于下文的简称，《最终幻想VII》简称为《VII》、《危机之前 -最终幻想VII-》简称为《危机之前》、《核心危机
-最终幻想VII-》简称为《核心危机》、《最终幻想VII 降临之子》简称为《降临之子》、《最终幻想VII
降临之子完整版》简称为 《降临之子完整版》、《地狱犬的挽歌 -最终幻想VII-》简称为《地狱犬的挽歌》。

## 开发

史克威尔艾尼克斯公司将该计划定义为“公司向‘多元化产品’方向迈出的第一步……”
\[5\]。“补完计划”制作人北瀨佳範说，当因公司的产品多元化实验而终于有机会来扩充以往的最终幻想作品时，他立即就选择了《最终幻想VII》作为对象，原因在于该款游戏在[最终幻想系列历史中的里程碑意义以及在玩家之间有极高的受欢迎程度](../Page/最终幻想系列.md "wikilink")
\[6\]。他进一步解释说，“比起其他游戏，《最终幻想VII》的结尾似乎……给游戏中人物开启了更多故事的可能性。”
\[7\]项目的启动的主要条件之一是重新聚集原《最终幻想VII》的制作人员：包括艺术总监[直良有佑](../Page/Yusuke_Naora.md "wikilink")，作曲家[植松伸夫](../Page/植松伸夫.md "wikilink")，剧本作家[野岛一成等](../Page/野岛一成.md "wikilink")
\[8\]。由野村提出计划时，他最初只计划开展《降临之子》和《危机之前》两个项目。然而，北瀨佳範将计划设想定义成更大的范围从而导致更多的作品开发计划被导入其中
\[9\]\[10\]。北瀨佳範解释说，在制作《降临之子》的一开始，制作团队就一致认为仅仅一部作品是不足以覆盖整个《最终幻想VII》的宏大世界的，因此策划出《危机之前》、《地狱犬的挽歌》和《核心危机》等作品以涵盖更多的方面
\[11\]。最初开发小组预期在不同的开发项目中共享资源和原型素材，然而很快，他们就面临开发困境，野村最终决定为每个作品创建不同的设计概念。他补充说，[系列中第一部游戏的正统续篇作品](../Page/最终幻想系列.md "wikilink")《最终幻想X-2》的制作也使他们在常规的[角色扮演游戏之外考虑更多的作品创作](../Page/角色扮演游戏.md "wikilink")[类型](../Page/电子游戏类型.md "wikilink")
\[12\]。

史克威尔艾尼克斯公司社长[和田洋一曾表示](../Page/和田洋一.md "wikilink")，“补完计划”可能将是一个活跃开发计划，将持续至《最终幻想VII》发布二十周年之际（2017年）\[13\]。2009年，在结束了《[最终幻想VII
降临之子完整版](../Page/最终幻想VII_降临之子完整版.md "wikilink")》的制作之后，制作团队决定从制作计划中暂时抽出休息一下\[14\]，但他们表示对于未来的游戏仍有不少不同的构思
\[15\]。 {{-}}

## 作品

<div class="toccolours" style="width:265px; float:right; margin-right: 0em; margin-left: 1em">

<big>**最终幻想VII补完计划**</big>

**Compilation of Final Fantasy VII**
\----

  - **游戏**
      - **[危机之前 -最终幻想VII-](../Page/危机之前_-最终幻想VII-.md "wikilink")**
      - **[地獄犬的輓歌 -最終幻想VII-](../Page/地獄犬的輓歌_-最終幻想VII-.md "wikilink")**
      - **[地狱犬的挽歌失落的插曲-最终幻想VII](../Page/Dirge_of_Cerberus_Lost_Episode:_Final_Fantasy_VII.md "wikilink")**
      - **[核心危機 -最終幻想VII-](../Page/核心危機_-最終幻想VII-.md "wikilink")**

<!-- end list -->

  - **影片**
      - **[最终幻想VII 降临之子](../Page/最终幻想VII_降临之子.md "wikilink")**
      - **[最终命令 -最终幻想VII-](../Page/最终命令_-最终幻想VII-.md "wikilink")**
      - **[最终幻想VII
        降临之子完整版](../Page/Final_Fantasy_VII:_Advent_Children_Complete.md "wikilink")**
      - **[通向微笑之路-丹塞尔篇](../Page/On_The_Way_To_A_Smile_-_Episode:_Denzel.md "wikilink")**

<!-- end list -->

  - **小说**
      - **[通向微笑之路](../Page/On_The_Way_To_A_Smile.md "wikilink")**

</div>

### 游戏

  - 《[危机之前 -最终幻想VII-](../Page/危机之前_-最终幻想VII-.md "wikilink")》（）:

“补完计划”中最早发售的作品是[手机平台游戏](../Page/手机游戏.md "wikilink")《危机之前》，本作是《最终幻想VII》的前传作品，故事背景被设定于原作游戏发生六年之前，并将玩家视角放在神羅公司的精銳部隊身上，在游戏中執行各種各樣的任務，其中包括他们第一次遇到[雪崩组织](../Page/Characters_of_the_Final_Fantasy_VII_series#AVALANCHE.md "wikilink")
\[16\]\[17\]。分24个章节以供下载 ，2004年9月24日开始在日本[NTT
DoCoMo的](../Page/NTT_DoCoMo.md "wikilink")[FOMA](../Page/Freedom_of_Mobile_Multimedia_Access.md "wikilink")900i系列手机提供完整下载服务\[18\]\[19\]
\[20\] 。

  - 《[地狱犬的挽歌 -最终幻想VII-](../Page/地狱犬的挽歌_-最终幻想VII-.md "wikilink")》（）:

《地狱犬的挽歌
-最终幻想VII-》是一款[第三人称](../Page/第三人称射击游戏.md "wikilink")[射击游戏](../Page/射击游戏.md "wikilink")
\[21\]\[22\]。本作专为[PlayStation
2平台开发](../Page/PlayStation_2.md "wikilink")，故事时间被设定在《最终幻想VII》原作三年之后《降临之子》一年之后，內容圍繞主角Vincent與稱為DG（Deep
Ground SOLDIER）的神秘組織的戰鬥
\[23\]\[24\]。《地狱犬的挽歌》于2006年1月26日在日本发售，2006年8月15日在北美发售
\[25\] 。

  - 《[地狱犬的挽歌失落的插曲-最终幻想VII](../Page/地狱犬的挽歌_-最终幻想VII-#地狱犬的挽歌失落的插曲-最终幻想VII.md "wikilink")》（）:

本作为《地狱犬的挽歌》的移动电话平台对应版本，是一款[第一人称射击游戏](../Page/第一人称射击游戏.md "wikilink")，在2006年8月18日发售于旗下平台的手机
\[26\]。

  - 《[核心危機 -最终幻想VII-](../Page/核心危機_-最终幻想VII-.md "wikilink")》（）:

“补完计划”中最后一个被发售的游戏作品是[PlayStation
Portable平台](../Page/PlayStation_Portable.md "wikilink")[動作角色扮演遊戲](../Page/動作角色扮演遊戲.md "wikilink")《核心危机》。作为原作游戏《最终幻想VII》的前传作品，本作将视点聚焦在[Zack
Fair身上](../Page/Zack_Fair.md "wikilink")，讲述原作事件发生七年前的故事
\[27\]\[28\]。本作游戏于2007年9月13日在日本发售，北美市场于2008年3月25日发售，欧洲市场则于2008年6月20日发售
\[29\] 。

### 影片

  - 《[最终幻想VII 降临之子](../Page/最终幻想VII_降临之子.md "wikilink")》（）:

《降临之子》是“补完计划”中第一部被披露的作品，最早曾于2003年9月在[东京电玩展上亮相](../Page/东京电玩展.md "wikilink")，但实际却是第二个发售的\[30\]\[31\]。
影片的二十五分钟剪辑版本曾在2004年的[威尼斯电影节上放映](../Page/威尼斯电影节.md "wikilink")，但直到次年依然未见到电影的完整版，2005年9月2日，影片参加了当年的威尼斯电影节\[32\]\[33\]。作为原作《最终幻想VII》的一部[续作](../Page/续集.md "wikilink")，《降临之子》的故事被设定在原作游戏的两年后。制品包括[DVD光碟和](../Page/DVD.md "wikilink")[索尼掌上游戏机](../Page/索尼.md "wikilink")[PlayStation
Portable专用](../Page/PlayStation_Portable.md "wikilink")[UMD光碟两种](../Page/UMD.md "wikilink")，最早在日本于2005年9月14日发售，欧洲和北美则于2006年4月25日发售\[34\]\[35\]\[36\]。

  - 《[最终命令 -最终幻想VII-](../Page/最终命令_-最终幻想VII-.md "wikilink")》（）:

日本和北美市场的特别版《降临之子》DVD中还包含由日本动画制作公司[马多浩斯负责制作的](../Page/马多浩斯.md "wikilink")[2D](../Page/2D.md "wikilink")[OVA](../Page/OVA.md "wikilink")《最终命令
-最终幻想VII-》，影片讲述了原作故事五年前神罗公司魔晃炉所在地之一[尼布尔海姆的毁灭](../Page/尼布尔海姆.md "wikilink")。
\[37\]

  - 《[最终幻想VII
    降临之子完整版](../Page/Final_Fantasy_VII:_Advent_Children_Complete.md "wikilink")》（）:

《最终幻想VII
降临之子》的[蓝光高清](../Page/Full_HD.md "wikilink")[导演剪辑版题为](../Page/导演剪辑版.md "wikilink")《最终幻想VII
降临之子完整版》，2009年4月16日在日本以[蓝光光盘为载体发售](../Page/蓝光光盘.md "wikilink")，英文版本也于同年发布。
\[38\]

  - 《[通向微笑之路-丹塞尔篇](../Page/On_The_Way_To_A_Smile_-_Episode:_Denzel.md "wikilink")》（）:

《降临之子完整版》中还包含有由《通向微笑之路》系列小说中《丹塞尔篇》改编的OVA。\[39\]\[40\]

### 小說

  - 《通向微笑之路》（On the Way to a Smile）:

三部[通向微笑之路系列短篇小说均由](../Page/Final_Fantasy_VII:_Advent_Children#On_the_Way_to_a_Smile.md "wikilink")[野島一成所作](../Page/野島一成.md "wikilink")，这些故事描述了《最终幻想VII》故事结束后至《降临之子》故事开始之前的两年间所发生的事。\[41\]

  - 《通向微笑之路-丹塞爾篇》（On the Way to a Smile - Case of
    Denzel）：於官網連載的短篇小說，一共四章。故事講述主角丹塞爾的身世，从《最终幻想VII》到《降临之子》兩年間所發生的事，以及他与蒂法（Tifa）和克勞德（Cloud）的相遇。

<!-- end list -->

  - 《通向微笑之路-蒂法篇》（On the Way to a Smile - Case of
    Tifa）：由野島一成於《最终幻想VII：降临之子序章》刊登的短篇小說。故事主角為蒂法，講述从《最终幻想VII》到《降临之子》兩年間的生活，並說明克勞德、蒂法、丹塞爾和瑪琳（Marlene）的關係。

<!-- end list -->

  - 《通向微笑之路-巴雷特篇》（On the Way to a Smile - Case of
    Barret）：在英文“降临之子限量版珍藏套装”中专为该版本所编写。

在《降临之子完整版》于日本发售的同时，《通向微笑之路》系列的另外四部小说也一并发布：《尤菲篇》、《Red
XIII篇》、《神罗篇》和《生命之泉篇-黑与白》。四部小说被收录进一册中，同一册还收录有前三篇故事。\[42\]

  - 《最终幻想VII外传：Turks-孩子们都还好吧-》（）:

本作于2011年12月15日在日本发售。该小说由野岛一成执笔，负责插图。故事发生在《通向微笑之路》同一时间，以一名[私人侦探埃文](../Page/Private_detective.md "wikilink")·汤森的视角调查一桩涉及Turks的案件。\[43\]

## 评价

尽管《最终幻想VII》曾受到一致好评，但人们对“补完计划”中的作品却是褒贬不一。在2007年7月，《[边缘](../Page/Edge_\(magazine\).md "wikilink")》杂志指出，这些新作“可能看起来品质较高，但同样也将是对原作的一种曲解”
\[44\]。《地狱犬的挽歌》首周出货量39.2万套
\[45\]\[46\]，并获得日本游戏杂志《[Fami通](../Page/Fami通.md "wikilink")》30分的评分（满分为40）
\[47\]。《降临之子》的销量虽然较为乐观，但评价却褒贬不一。日本DVD发行首周售出超过42万份，占当时总发行量的93% \[48\] 。
《危机之前》发布当天注册上线用户达20万，使其成为当时[最畅销的](../Page/最畅销.md "wikilink")[手机游戏](../Page/手机游戏.md "wikilink")
\[49\]。《核心危机》全球卖出310万套 \[50\]。 {{-}}

## 注释

## 相关条目

  - [新的水晶故事 最终幻想](../Page/新的水晶故事_最终幻想.md "wikilink")

[Compilation](../Category/最終幻想VII.md "wikilink")
[VII](../Category/最终幻想系列衍生作品.md "wikilink")
[F](../Category/文明崩潰後世界題材遊戲.md "wikilink")
[F](../Category/蒸汽朋克电子游戏.md "wikilink")
[F](../Category/電子遊戲系列.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.
9.

10.

11.
12.
13.

14.

15.

16.

17.

18.
19.

20.

21.

22.

23.

24.

25.
26.

27.

28.

29.

30.

31.

32.
33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.
46.

47.

48.

49.

50.