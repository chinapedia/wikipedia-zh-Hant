[HK_Wheelock_House.jpg](https://zh.wikipedia.org/wiki/File:HK_Wheelock_House.jpg "fig:HK_Wheelock_House.jpg")
[HK_Wheelock_House_Enterance.jpg](https://zh.wikipedia.org/wiki/File:HK_Wheelock_House_Enterance.jpg "fig:HK_Wheelock_House_Enterance.jpg")
[HK_Central_Pedder_Street_Crosswalk_LV_Citibank_Bus_Taxi.JPG](https://zh.wikipedia.org/wiki/File:HK_Central_Pedder_Street_Crosswalk_LV_Citibank_Bus_Taxi.JPG "fig:HK_Central_Pedder_Street_Crosswalk_LV_Citibank_Bus_Taxi.JPG")口\]\]
[HKMTR_Central_Station_Exit_D_201501.jpg](https://zh.wikipedia.org/wiki/File:HKMTR_Central_Station_Exit_D_201501.jpg "fig:HKMTR_Central_Station_Exit_D_201501.jpg")
 **會德豐大廈**（[英文](../Page/英文.md "wikilink")：**Wheelock
House**），位於[香港](../Page/香港.md "wikilink")[畢打街](../Page/畢打街.md "wikilink")20號，是[香港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[甲級寫字樓](../Page/甲級寫字樓.md "wikilink")，由[會德豐地產及](../Page/會德豐地產.md "wikilink")[長江實業聯手發展](../Page/長江實業.md "wikilink")（地役權歸[會德豐](../Page/會德豐.md "wikilink")，[長實只擁有物業發展權](../Page/長實.md "wikilink")）。大廈建成於1984年，樓高24層，[建築師為](../Page/建築師.md "wikilink")[王歐陽(香港)有限公司](../Page/王歐陽\(香港\)有限公司.md "wikilink")，它的[建築物作品包括](../Page/建築物.md "wikilink")[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")、[沙田第一城](../Page/沙田第一城.md "wikilink")、[怡安華人行等](../Page/怡安華人行.md "wikilink")。\[1\]

會德豐大廈地下設有多間商店。包括[連卡佛旗下的On](../Page/連卡佛.md "wikilink")
Pedder鞋店、[交通銀行分行及開業多年的](../Page/交通銀行.md "wikilink")[花旗银行](../Page/花旗银行.md "wikilink")，其前身租戶曾經是[美國大通銀行](../Page/美國大通銀行.md "wikilink")、[星展銀行等](../Page/星展銀行.md "wikilink")。大廈23樓是當時[上市公司](../Page/上市公司.md "wikilink")[會德豐地產的](../Page/會德豐地產.md "wikilink")[寫字樓總部](../Page/寫字樓.md "wikilink")。
\[2\]

## 沿革

會德豐大廈所處位置，以前是舊渣甸洋行總部——[渣甸大廈](../Page/渣甸大廈.md "wikilink")。據考證，渣甸洋行投得海傍道地皮後，於1841年建成第一代渣甸大廈。而第二代渣甸大廈則於1908年落成，其後又於1956年重建，1970年代業權易手後建成現時的會德豐大廈。

2004年11月27日[花旗銀行以](../Page/花旗銀行.md "wikilink")3.9億元，向[億都（國際控股）購回兩年前以](../Page/億都（國際控股）.md "wikilink")1.84億元售出的畢打街20號會德豐大廈地庫A、地下A連外牆，實用面積8539方呎，呎價4.5萬元。

2016年3月15日，[九龍倉集團以](../Page/九龍倉集團.md "wikilink")50.2億元現金向會德豐收購會德豐大廈3至24樓及以11.41億元現金向吳光正收購會德豐大廈地下C舖，總樓面面積約370.4平方米（3,987平方呎）\[3\]

[有線財經資訊台於](../Page/有線財經資訊台.md "wikilink")2006年在大廈3樓設「中環直播室」，面積約5,000平方呎，並作為新聞台其中一個辦公室，多個財經節目都在該處進行拍攝錄影，包括《Money
Cafe》及《財經即時睇》等，方便在中環工作的嘉賓於該處做節目。2017年4月20日，[永升亞洲將成為有線寬頻大股東後](../Page/永升亞洲.md "wikilink")，九龍倉於同月28日決定將「中環直播室」停止運作。\[4\]

## 鄰近

  - [德成行](../Page/德成行.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[中環站](../Page/中環站.md "wikilink")
  - [中建大廈](../Page/中建大廈.md "wikilink")
  - [環球大廈](../Page/環球大廈.md "wikilink")
  - [置地廣場](../Page/置地廣場.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")
  - [遮打道](../Page/遮打道.md "wikilink")

## 相關

  - [會德豐](../Page/會德豐.md "wikilink")
  - [香港有線電視新聞直播室](../Page/香港有線電視新聞.md "wikilink")
  - [花旗银行](../Page/花旗银行.md "wikilink")
  - [康業信貸快遞](../Page/康業信貸快遞.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[中環站](../Page/中環站.md "wikilink")
  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[香港站](../Page/香港站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/巴士.md "wikilink")：途經德輔道中、畢打街的路線
    [電車](../Page/香港電車.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [會德豐大廈官方網頁](http://www.wheelockpropertieshk.com/property.php?prop=_01+Wheelock+House&type=commercial&status=portfolio&language=ch)

[Category:中西區寫字樓 (香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")
[Category:九龍倉置業物業](../Category/九龍倉置業物業.md "wikilink")

1.  [王歐陽（香港）有限公司的建築物作品列表](http://www.emporis.com/en/cd/cm/?id=wongouyanghkltd-hongkong-china)
2.  [會德豐地產 基本資料](http://hk.biz.yahoo.com/p/hk/profile/index.html?s=0049)
3.  [九倉60億購會德豐大廈 增寫字樓物業
    會德豐買地添子彈](http://news.mingpao.com/pns/dailynews/web_tc/article/20160315/s00004/1457979046953)
    明報，2016年3月15日
4.  [中環在線：九倉話cut就cut有線中環直播室
    《蘋果日報》 2017-04-28](http://hk.apple.nextmedia.com/financeestate/art/20170428/20004402)