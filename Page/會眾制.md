**會眾制（Congregationalism）**是[基督宗教的](../Page/基督宗教.md "wikilink")[教會體制](../Page/教會體制.md "wikilink")（church
polity）之一。會眾制教會的每一個地方教会都是獨立自治的。在[新教歷史中較主要的會眾制](../Page/新教.md "wikilink")[宗派有十七世紀英美兩地](../Page/宗派.md "wikilink")[清教徒運動流傳下來的](../Page/清教徒.md "wikilink")[公理會](../Page/公理會.md "wikilink")（Congregationalist,
现已衰微），[浸禮會](../Page/浸禮會.md "wikilink")（Baptist,是当今美国最大的新教宗派），[門諾會](../Page/門諾會.md "wikilink")（Menonite）和[貴格會](../Page/貴格會.md "wikilink")（Quakers）等。十九世紀以後的[新興宗派或](../Page/新興宗派.md "wikilink")[獨立教會大多採取會眾制](../Page/獨立教會.md "wikilink")。除了會眾制，基督教會還有[主教制及](../Page/主教制.md "wikilink")[長老制兩種治理體制](../Page/長老制.md "wikilink")。

## 基本形式

会众制教会的最大特征是每个地方教会都是[独立的](../Page/独立.md "wikilink")，各自互不控制，没有母会和子会的架构。与之相对的是[主教制和长老制](../Page/主教制.md "wikilink")。在主教制的教会中，一个主教管理诸多个地方教会。在长老制的教会中，各个地方教会派出自己的代表，组成总会，该总会有权管理各个地方教会。当[自由派神学侵入长老制教会之后](../Page/自由主义神学.md "wikilink")，那些想要保守纯正教义、脱离宗派的地方教会必须离开自己买下来的教堂，因为教堂属于总会。而会众制教会则是独立、[自治](../Page/自治.md "wikilink")、[自养的](../Page/自养.md "wikilink")。会众制教会中某些地方教会有时组成松散的[联盟](../Page/联盟.md "wikilink")，彼此合作一些事情，但都是自由加入和退出，如[美南浸信会联会](../Page/美南浸信會.md "wikilink")（the
Southern Baptist
Convention）。会众制教会有一个缺点，就是教会之间的交往和合作较少；但也有一个优点，就是并非铁板一块，所以不会产生一坏俱坏、一损俱损、全盘堕落的重大后果。

有些人误以为会众制教会事事都由会众来决定，牧师没有权柄，但这不符合会众制教会的实际，也不符合圣经。会众制教会，如浸信会，既强调牧师的权柄，因这是符合圣经的，也不压制会众的声音，因圣经中也有会众表达声音的例子，如使徒行传第七章。所以，会众制教会不是纯粹的民主制，不是凡事都由大多数人的意见来决定，而是寻求牧师的权柄与会众的声音取得一个平衡。会众制教会中，会众对牧师有监督的责任；如果牧师犯罪，会众可将之职位解除，并由会众自己选举下一任牧师。

## 浸信会

[浸信会是会众制教会中的代表](../Page/浸信会.md "wikilink")。从[再洗礼派传下来的浸信会](../Page/再洗礼派.md "wikilink")，非常执著地强调各个地方教会的独立和自治。在[教会与国家关系上](../Page/:en:Separation_of_church_and_state.md "wikilink")，浸信会与主教制的教会、长老制的教会也不同。浸信会非常推崇政教分离，传到美国后，也积极推动美国的[政教分离事业](../Page/政教分离.md "wikilink")。[在浸信会的努力下](../Page/:en:Baptists_in_the_history_of_separation_of_church_and_state.md "wikilink")，美国[宪法第一修正案确立了教会与国家分离的原则](../Page/宪法第一修正案.md "wikilink")。

## 参考

  - [公理宗](../Page/公理宗.md "wikilink")
  - [浸信会](../Page/浸信会.md "wikilink")
  - [长老宗](../Page/长老宗.md "wikilink")
  - [主教制](../Page/主教制.md "wikilink")

[Category:基督教組織](../Category/基督教組織.md "wikilink")