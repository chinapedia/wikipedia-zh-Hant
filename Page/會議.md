[ELW2-Besprechungsraum_modified.jpg](https://zh.wikipedia.org/wiki/File:ELW2-Besprechungsraum_modified.jpg "fig:ELW2-Besprechungsraum_modified.jpg")
**會議**
是人類社會的一種[社交](../Page/社交.md "wikilink")、[公關](../Page/公關.md "wikilink")、[政治](../Page/政治.md "wikilink")、[意見](../Page/意見.md "wikilink")[交流](../Page/交流.md "wikilink")、[訊息](../Page/訊息.md "wikilink")[傳播及](../Page/傳播.md "wikilink")[溝通的活動](../Page/溝通.md "wikilink")，由兩位或多位人士參與。\[1\]

會議是一種[社會科學](../Page/社會科學.md "wikilink")，也是一種[人文](../Page/人文.md "wikilink")[藝術](../Page/藝術.md "wikilink")，成功與失敗，在於與會者及各方的誠意及能力。

## 會議種類

  - 不同参与者的会议：
      - 组织或机构内部的会议
          - 例会，组织内人员参加的日常会议。
          - [代表大會](../Page/代表大會.md "wikilink")
              - [党代会](../Page/党代会.md "wikilink")，即[政党的代表大会](../Page/政党.md "wikilink")。
          - [董事會](../Page/董事會.md "wikilink")，企业[董事相关会议](../Page/董事.md "wikilink")。
          - [股東大會](../Page/股東大會.md "wikilink")
          - 政府会议
              - [議會做为](../Page/議會.md "wikilink")[立法机关的形态之一](../Page/立法机关.md "wikilink")，以举行会议的方式，处理各项[政务](../Page/政务.md "wikilink")。
          - [班會](../Page/班會.md "wikilink")，学生参加的[班级会议](../Page/班级.md "wikilink")。
      - 多个组织参与的会议，甚至多国组织参加的国际会议。
  - 各类功能的会议：
      - [記者會](../Page/記者會.md "wikilink")，或称新闻发布会。
      - [研討會](../Page/研討會.md "wikilink")（包含特定主題的教學，中場或結束前提供發問的機會）
      - [協調會](../Page/協調會.md "wikilink")，亦是不定期会议。
      - 选举会，进行[选举的会议](../Page/选举.md "wikilink")，是选举方式之一，如[教宗选举秘密会议](../Page/教宗选举秘密会议.md "wikilink")。
  - 不同举办频率的会议：
      - 以时间区分的会议，早会、晚会、日会、[周会](../Page/星期.md "wikilink")、[月会](../Page/月份.md "wikilink")、[季会](../Page/季度.md "wikilink")、[年会](../Page/年会.md "wikilink")、双年会等。
      - 不定期会议
  - 其它：
      - [餐会](../Page/餐会.md "wikilink")，[就餐的同时完成的会议](../Page/就餐.md "wikilink")，与[宴会的目的不同](../Page/宴会.md "wikilink")。以企业、政府机构日常举行的[早餐会尤为常见](../Page/早餐会.md "wikilink")，是为处理日常事务、提高办公效率而举行\[2\]。另外，台湾有[尾牙餐会的习俗](../Page/尾牙.md "wikilink")。

## 使用設備

傳統的會議大多需要如下設備：

  - [會議室](../Page/會議室.md "wikilink")
  - [投影機](../Page/投影機.md "wikilink")
  - [白板](../Page/白板.md "wikilink")、[白板筆](../Page/白板筆.md "wikilink")
  - [麥克風與](../Page/麥克風.md "wikilink")[擴音器](../Page/揚聲器.md "wikilink")（常用於大型的會議時）

随着互联网的发展，为了进一步提高工作的效率和会议的便捷性，网络视频会议也随之诞生。目前的视频会议也都具备白板，白板笔等显示会议的工具，新兴的网页版网络视频会议还具有“网页剪取”等全新功能。在日常生活中，网络会议的使用率正逐步提升。

## 相關條目

  - [圓桌會議](../Page/圓桌會議.md "wikilink")
  - [投票權](../Page/投票權.md "wikilink")
  - [動議](../Page/動議.md "wikilink")

## 参考

<references/>

[Category:管理學](../Category/管理學.md "wikilink")
[Category:會議](../Category/會議.md "wikilink")

1.  [Meeting – Definition and More from the Free Merriam-Webster
    Dictionary](http://www.merriam-webster.com/dictionary/meeting).
    (n.d.). Dictionary and Thesaurus – Merriam-Webster Online. Retrieved
    April 21, 2010, from
2.