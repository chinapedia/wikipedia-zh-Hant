[Medio-lateral-episiotomy-zh-tw.png](https://zh.wikipedia.org/wiki/File:Medio-lateral-episiotomy-zh-tw.png "fig:Medio-lateral-episiotomy-zh-tw.png")
**會陰切開術**（episiotomy，）是一種剪開[會陰的手術](../Page/會陰.md "wikilink")，用來擴大[陰道以協助](../Page/陰道.md "wikilink")[分娩](../Page/分娩.md "wikilink")。這個切口可以從[女陰後端中線或是旁邊開始剪](../Page/女陰.md "wikilink")，在[局部麻醉下剪](../Page/局部麻醉.md "wikilink")，在分娩之後[縫合](../Page/縫合.md "wikilink")。這是一個普遍施行於產婦的手術，雖然近幾十年來慢慢不再是例行手術。

## 施行

[外科醫師用會陰切開術減輕](../Page/外科醫師.md "wikilink")[產痛](../Page/產痛.md "wikilink")，減低[括約肌所受的傷害以減低產後骨盆底功能障礙](../Page/括約肌.md "wikilink")，減少分娩失血。不過很多時候會陰切開術也會造成這些問題。

出現下列情況，將會考慮剪會陰：

  - 胎兒在產道時出現[胎兒窘迫跡象](../Page/胎兒窘迫.md "wikilink")。
  - 分娩太快，陰道來不及擴張。
  - 孩子的頭對陰道口來說太大。
  - 孩子的肩膀卡住了。
  - [臀位生產或](../Page/臀位生產.md "wikilink")[產鉗生產](../Page/產鉗生產.md "wikilink")。

## 普遍施行之爭議

在很多國家，剪會陰已是行之有年的例行手術。1960年代起，歐美的婦產科醫師與助產士不再偏好會陰切開術。經科學根據的比例小於20%\[1\]。

一項美國研究顯示美國產婦1956年剪會陰的比例是56%，1997年則是31%。（Weber and Meyn，2002）

近年的研究顯示剪會陰不該作為例行手術，因為會增加罹病率。Hartmann等人在2005年回顧文獻，指出剪會陰對一般病人沒有幫助。不過對於產道狹窄與其他上述問題有幫助。

剪會陰會造成產後會陰疼痛還有排泄問題（特別是正中會陰切開術）（Signorello等人，2000）剪會陰還會造成性交問題。

2003年時，[台灣女性生產時会阴侧切的比例高達](../Page/台灣.md "wikilink")98%\[2\]，而2007年台灣女性生產時会阴侧切的比例為93%，是世界第一\[3\]。

## 告知同意

準媽媽在產前照護或是更早之前常常會有「分娩計畫」，而且通常鼓勵準媽和照護者討論她們對剪會陰的意見。在
分娩的最後階段產科醫師與助產士往往沒有時間討論剪會陰的利弊與替代方案。分娩計畫往往會被忽略。跟隨分娩計畫可能會延長醫師的值班，或是其他醫生要來接生。

## 參考

  - Hartmann K, Viswanathan M, Palmieri R, Gartlehner G, Thorp J Jr,
    Lohr KN. *Outcomes of routine episiotomy: a systematic review.*
    [JAMA](../Page/JAMA.md "wikilink") 2005;293:2141-8. PMID 15870418.
  - Signorello LB, Harlow BL, Chekos AK, Repke JT. *Midline episiotomy
    and anal incontinence: retrospective cohort study.*
    [BMJ](../Page/BMJ.md "wikilink") 2000;320:86-90. PMID 10625261.
  - Weber AM, Meyn L. *Episiotomy use in the United States, 1979-1997.*
    Obstet Gynecol 2002;100:1177-82. PMID 12468160.

[Category:助产术](../Category/助产术.md "wikilink")
[Category:会阴](../Category/会阴.md "wikilink")

1.
2.

3.