《**月刊Asuka**》是由[日本](../Page/日本.md "wikilink")[角川書店發行的](../Page/角川書店.md "wikilink")[月刊](../Page/月刊.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")，1985年創刊，主打[少女漫畫](../Page/少女漫畫.md "wikilink")。

著名的連載作品包括[杉崎由綺琉的](../Page/杉崎由綺琉.md "wikilink")《[D・N・Angel](../Page/天使怪盜.md "wikilink")》、[CLAMP的](../Page/CLAMP.md "wikilink")《[X](../Page/X_\(漫畫\).md "wikilink")》、[九條清的](../Page/九條清.md "wikilink")《[聖魔之血](../Page/聖魔之血.md "wikilink")》等。

## 現在主要連載作品

  - [無法掙脫的背叛](../Page/無法掙脫的背叛.md "wikilink")（[小田切螢](../Page/小田切螢.md "wikilink")）

  - （作畫：[松本手毬](../Page/松本手毬.md "wikilink") /
    原作：[喬林知](../Page/喬林知.md "wikilink")）

  - [聖魔之血](../Page/聖魔之血.md "wikilink")（作畫：[九條清](../Page/九條清.md "wikilink")
    / 原作：[吉田直](../Page/吉田直.md "wikilink")）

  - [野球少年](../Page/野球少年.md "wikilink")（作畫：[柚庭千景](../Page/柚庭千景.md "wikilink")
    / 原作：[淺野敦子](../Page/淺野敦子.md "wikilink")）

  - [桃組plus戰記](../Page/桃組plus戰記.md "wikilink")（左近堂繪里）

  - [心靈偵探 八雲](../Page/心靈偵探_八雲.md "wikilink")（作畫：小田すずか /
    原作：[神永學](../Page/神永學.md "wikilink")）

  - （作畫： / 原作:）

  - [吉原花魁道中](../Page/吉原花魁道中.md "wikilink")（音中さわき）

  - [1001](../Page/1001_漫畫.md "wikilink")（[杉崎由綺琉](../Page/杉崎由綺琉.md "wikilink")）

  - [百千家的妖怪王子](../Page/百千家的妖怪王子.md "wikilink")（[硝音綾](../Page/硝音綾.md "wikilink")
    ）

## 現在休載中的作品

  - [X](../Page/X_\(漫畫\).md "wikilink")（[CLAMP](../Page/CLAMP.md "wikilink")）

## 過去連載的主要的作品

  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（CLAMP）
  - [新·安琪莉可](../Page/新·安琪莉可.md "wikilink")（梶山ミカ）
  - [Darker than BLACK
    -黑之契約者-](../Page/黑之契約者.md "wikilink")（[野奇夜](../Page/野奇夜.md "wikilink")）
  - [羅密歐×茱麗葉](../Page/羅密歐×茱麗葉.md "wikilink")（[COM](../Page/COM_\(イラストレーター\).md "wikilink")）
  - 天馬的血族（[竹宮惠子](../Page/竹宮惠子.md "wikilink")）
  - [神的遊戲](../Page/神的遊戲.md "wikilink")（作畫：[吉村工](../Page/吉村工.md "wikilink")
    / 原作：[宮崎柊羽](../Page/宮崎柊羽.md "wikilink")）
  - [Code Geass 反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")（）
  - [封魔少年焰&陣](../Page/封魔少年焰&陣.md "wikilink")（[杉崎由綺琉](../Page/杉崎由綺琉.md "wikilink")）
  - [魔法人力派遣公司](../Page/魔法人力派遣公司.md "wikilink")（作畫： /
    原作：[三田誠](../Page/三田誠.md "wikilink")）
  - [Darker than BLACK
    -漆黑的花-](../Page/黑之契約者.md "wikilink")（[岩原裕二](../Page/岩原裕二.md "wikilink")）
  - [D・N・Angel](../Page/天使怪盜.md "wikilink")（杉崎由綺琉）
  - [搖滾藍薔薇](../Page/搖滾藍薔薇.md "wikilink")（[新條真由](../Page/新條真由.md "wikilink")）
  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（[由羅海里](../Page/由羅海里.md "wikilink")）
  - [薔薇公主之吻](../Page/薔薇公主之吻.md "wikilink")（[硝音綾](../Page/硝音綾.md "wikilink")）

## 外部連結

  - [官方網頁](http://asuka-web.jp/)

[\*](../Category/月刊Asuka.md "wikilink")
[Category:角川書店的漫畫雜誌](../Category/角川書店的漫畫雜誌.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少女漫畫雜誌](../Category/少女漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:1985年日本建立](../Category/1985年日本建立.md "wikilink")