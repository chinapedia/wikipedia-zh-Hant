《**月刊Comic電擊大王**》是[日本](../Page/日本.md "wikilink")[MediaWorks公司所發行的一本少年取向的](../Page/MediaWorks.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")。

原先是以[萬代所發行的漫畫雑誌](../Page/萬代.md "wikilink")《Cyber Comics》作為開端，接著成為《Media
Comics
Dyne》，並在1994年以季刊誌的《Comic電擊大王》為名創刊。在歷經了雙月刊及月刊的演變之後，雜誌名也改為現在的《月刊Comic電擊大王》。

姐妹雜誌有《[電擊萌王](../Page/電擊萌王.md "wikilink")》以及休刊中的《[電擊帝王](../Page/電擊帝王.md "wikilink")》。

## 現在連載中的作品

*更新至2012年9月27日*

### 雜誌原創

  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")（[ばらスィー](../Page/ばらスィー.md "wikilink")）
  - [同班同學(♀)與迷宮的不恰當攻略法](../Page/同班同學\(♀\)與迷宮的不恰當攻略法.md "wikilink")（原名：、作者：[紺矢ユキオ](../Page/紺矢ユキオ.md "wikilink")）
  - [戀戀★學生會](../Page/戀戀★學生會.md "wikilink")（[あらきかなお](../Page/あらきかなお.md "wikilink")）
  - [人生偏差值48的高中生成為了神。](../Page/人生偏差值48的高中生成為了神。.md "wikilink")（原名：、作者：[原つもい](../Page/原つもい.md "wikilink")）
  - [青春謳歌部](../Page/青春謳歌部.md "wikilink")（原名：、作者：[うかみ](../Page/うかみ.md "wikilink")）
  - [相対性モテ論](../Page/相対性モテ論.md "wikilink")（原作：[築地俊彦](../Page/築地俊彦.md "wikilink")
    作画：[前田理想](../Page/前田理想.md "wikilink")）
  - [情色女作家的正義](../Page/情色女作家的正義.md "wikilink")（原名：、作者：[真田鈴](../Page/真田鈴.md "wikilink")）
  - [Doubt\!](../Page/Doubt!.md "wikilink")（[天乃咲哉](../Page/天乃咲哉.md "wikilink")）
  - [虎吻 A School
    Odyssey](../Page/虎吻_A_School_Odyssey.md "wikilink")（原名：、原作：[鴨志田一](../Page/鴨志田一.md "wikilink")
    作画：[トマトマト](../Page/トマトマト.md "wikilink")）
  - [NOT
    LIVES](../Page/NOT_LIVES.md "wikilink")（[烏丸渡](../Page/烏丸渡.md "wikilink")）
  - [反正三島凜絕不相信！](../Page/反正三島凜絕不相信！.md "wikilink")（原名：、作者：[倉薗紀彦](../Page/倉薗紀彦.md "wikilink")）
  - [百合星人奈緒子美眉](../Page/百合星人奈緒子美眉.md "wikilink")
    （[kashmir](../Page/kashmir.md "wikilink")）
  - [四葉妹妹\!](../Page/四葉妹妹!.md "wikilink")（[東清彥](../Page/東清彥.md "wikilink")）
  - [理香和春和溫泉和海豚](../Page/理香和春和溫泉和海豚.md "wikilink")（原名：、作者：[ヒジキ](../Page/ヒジキ.md "wikilink")）
  - [終將成為妳](../Page/終將成為妳.md "wikilink")（原名：やがて君になる、作者：仲谷鳰）

### MediaWorks企劃

  - [瑪姬莎花園
    加速世界外傳](../Page/加速世界.md "wikilink")（原案：[川原礫](../Page/川原礫.md "wikilink")、作画：[笹倉綾人](../Page/笹倉綾人.md "wikilink")）
  - [在那個夏天等待](../Page/在那個夏天等待.md "wikilink")（原作：[I\*Chi\*Ka](../Page/I*Chi*Ka.md "wikilink")、作画：[毒田ペパ子](../Page/毒田ペパ子.md "wikilink")）
  - [現在就想告訴哥哥，我是妹妹！](../Page/現在就想告訴哥哥，我是妹妹！.md "wikilink")（原名：、原作：[fairys](../Page/fairys.md "wikilink")、作画：[おだまさる](../Page/おだまさる.md "wikilink")）
  - [境界线上的地平线](../Page/境界线上的地平线.md "wikilink")（作画：[武中英雄](../Page/武中英雄.md "wikilink")、原作：[川上稔](../Page/川上稔.md "wikilink")、角色設計：[さとやす](../Page/さとやす.md "wikilink")）
  - [境界線上的赫萊子](../Page/境界线上的地平线.md "wikilink")（作画：[羽仁倉雲](../Page/羽仁倉雲.md "wikilink")、原作：[川上稔](../Page/川上稔.md "wikilink")、角色設計：[さとやす](../Page/さとやす.md "wikilink")）
  - [痕](../Page/痕.md "wikilink")（原作：[Leaf](../Page/Leaf.md "wikilink")、作画：[月吉ヒロキ](../Page/月吉ヒロキ.md "wikilink")）
    - 從『電擊黑王』移籍
  - [戀愛與選舉與巧克力SLC](../Page/戀愛與選舉與巧克力.md "wikilink")（原作：[sprite](../Page/Sprite_\(ゲームブランド\).md "wikilink")、作画：[依河和希](../Page/依河和希.md "wikilink")）
  - [青春紀行](../Page/青春紀行.md "wikilink")（原作：[竹宮悠由子](../Page/竹宮悠由子.md "wikilink")、作画：[梅ちゃづけ](../Page/梅ちゃづけ.md "wikilink")、角色設計：[駒都英二](../Page/駒都英二.md "wikilink")）
  - [C³
    -魔幻三次方-](../Page/C³_-魔幻三次方-.md "wikilink")（原作：[水瀨葉月](../Page/水瀨葉月.md "wikilink")、作画：[秋奈つかこ](../Page/秋奈つかこ.md "wikilink")、角色設計：[さそりがため](../Page/さそりがため.md "wikilink")）
  - [STRIKE THE
    BLOOD](../Page/STRIKE_THE_BLOOD.md "wikilink")（原名：ストライク・ザ・ブラッド、原作：[三雲岳斗](../Page/三雲岳斗.md "wikilink")、作画：[TATE](../Page/TATE.md "wikilink")、角色設計：[マニャ子](../Page/マニャ子.md "wikilink")）
  - [すぴぱら girl's
    diary](../Page/すぴぱら.md "wikilink")（[犬居なぎ](../Page/犬居なぎ.md "wikilink")）
  - [天元突破紅蓮螺巖](../Page/天元突破紅蓮螺巖.md "wikilink")（[森小太郎](../Page/森小太郎.md "wikilink")
    原作：[GAINAX](../Page/GAINAX.md "wikilink")、監修：[中島かずき](../Page/中島かずき.md "wikilink")）
    - 從『電撃コミックガオ\!』移籍
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（原作：[鎌池和馬](../Page/鎌池和馬.md "wikilink")、作画：[冬川基](../Page/冬川基.md "wikilink")、角色設計：[はいむらきよたか](../Page/灰村清孝.md "wikilink")）
  - [TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink")（原作：[竹宮悠由子](../Page/竹宮悠由子.md "wikilink")
    作画：[絕叫](../Page/絕叫.md "wikilink")） - 從『電撃コミックガオ\!』移籍
  - [打工吧！魔王大人](../Page/打工吧！魔王大人.md "wikilink")（原作：[和ヶ原聡司](../Page/和ヶ原聡司.md "wikilink")、作画：[柊暁生](../Page/柊暁生.md "wikilink")、角色設計：[029](../Page/029.md "wikilink")）
  - [Muv-Luv
    Alternative](../Page/Muv-Luv_Alternative.md "wikilink")（原作：[アージュ](../Page/アージュ.md "wikilink")、作画：[蒔島梓](../Page/蒔島梓.md "wikilink")）
  - [Muv-Luv Alternative Total Eclipse
    rising](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")（原名：マブラヴ
    オルタネイティヴ トータル・イクリプス
    rising、原作：[吉宗鋼紀](../Page/吉田博彦.md "wikilink")、作画：[ROHGUN](../Page/ROHGUN.md "wikilink")）
  - [魔法科高中的優等生](../Page/魔法科高中的劣等生.md "wikilink")（原作：[佐島勤](../Page/佐島勤.md "wikilink")、作画：[森夕](../Page/森夕.md "wikilink")、角色設計：[石田可奈](../Page/石田可奈.md "wikilink")）
  - [紫色的Qualia](../Page/紫色的Qualia.md "wikilink")（原作：[うえお久光](../Page/うえお久光.md "wikilink")、作画：[綱島志朗](../Page/綱島志朗.md "wikilink")）
  - [Rewrite:Side-R](../Page/Rewrite.md "wikilink")（原作：[Key](../Page/Key_\(公司\).md "wikilink")、作画：[川上修一](../Page/川上修一.md "wikilink")）
  - [記錄的地平線外傳 Honey Moon
    Logs](../Page/記錄的地平線.md "wikilink")（原作：[橙乃ままれ](../Page/橙乃ままれ.md "wikilink")、作画：[松モトヤ](../Page/松モトヤ.md "wikilink")）
  - [我家有個狐仙大人](../Page/我家有個狐仙大人.md "wikilink")（原作：[柴村仁](../Page/柴村仁.md "wikilink")
    作画：[松風水蓮](../Page/松風水蓮.md "wikilink")
    角色設計：[放電映像](../Page/放電映像.md "wikilink")） -
    從『電撃コミックガオ\!』移籍
  - [Walkure Romanze
    少女騎士物語](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（原作：[Ricotta](../Page/Ricotta.md "wikilink")
    作画：[蜜キング](../Page/蜜キング.md "wikilink")
    構成協力：[藤代百](../Page/藤代百.md "wikilink")）
  - [扩散性百万亚瑟王：群青的守护者](../Page/扩散性百万亚瑟王：群青的守护者.md "wikilink")(作画：枫月诚 )
  - [來自風平浪靜的明天](../Page/來自風平浪靜的明天.md "wikilink")（原作：[Project-118](../Page/Project-118.md "wikilink")
    漫画：[前田理想](../Page/前田理想.md "wikilink")）

## 休載中

  - [ef - a fairy tale of the
    two.](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink")（[雅樹里](../Page/雅樹里.md "wikilink")
    原作：[御影](../Page/御影.md "wikilink")、[鏡遊](../Page/鏡遊.md "wikilink")） -
    從『電撃コミックガオ\!』移籍
  - [禍神風塵帖](../Page/禍神風塵帖.md "wikilink")（[うたたねひろゆき](../Page/うたたねひろゆき.md "wikilink")）
  - [歡樂課程](../Page/歡樂課程.md "wikilink")（[森真之介](../Page/森真之介.md "wikilink")）
  - [悠久黙示録エイドロンシャドー](../Page/悠久黙示録エイドロンシャドー.md "wikilink")（[漆原智志](../Page/漆原智志.md "wikilink")
    原案：[吉本欣司](../Page/吉本欣司.md "wikilink")）
  - [リスティス](../Page/リスティス.md "wikilink")（うたたねひろゆき）

## 過去連載過的作品

### 雜誌原創

  - （イグナクロス零号駅）
  - （高岩博士の異常な愛情）
  - [笑園漫畫大王](../Page/笑園漫畫大王.md "wikilink")（あずまんが大王）
  - [一擊殺虫！！HOIHOIさん](../Page/一擊殺虫！！HOIHOIさん.md "wikilink")（一撃殺虫\!\!ホイホイさん）
  - （宇宙人プルマー）
  - （演撃少女命）
  - （ANGEL FOYSON）
  - （鍵姫物語 永久アリス輪舞曲）
  - （ガンドライバー）
  - （神無）
  - （くらクラ）
  - （鉄コミュニケイション）
  - （クロノスヘイズ）
  - [心之圖書館](../Page/心之圖書館.md "wikilink")（ココロ図書館）
  - （SISTER RED）
  - （隻眼獣ミツヨシ）
  - （太陽の少女インカちゃん）
  - （TICKTACK GANGAN）
  - （超感覚ANALマン）
  - （D4プリンセス）
  - （電撃ネコミミ侍）
  - （電撃ももえサイズ）
  - （DOLL MASTER）
  - （TRAIN×TRAIN）
  - （なななな）
  - （ニニンがシノブ伝）
  - （PUREまりおねーしょん）
  - （ぷりてぃまにぃず）
  - （まにぃロード）
  - （宵闇眩燈草紙）
  - （シャイナ・ダルク ～黒き月の王と蒼碧の月の姫君～）
  - （超常機動サイレーン）
  - （でじぱら）
  - （電撃テンジカーズ）
  - （となりのだんな様）
  - （トリコロ）
  - （猫神たま）※不定期連載
  - [星空學園](../Page/星空學園.md "wikilink")（はやて×ブレード）
  - （ヒメヤカヒメサマ）
  - （ひよママ）
  - [BLOOD ALONE](../Page/BLOOD_ALONE.md "wikilink")
  - [DARK
    WHISPER](../Page/DARK_WHISPER.md "wikilink")（原名：、作者：[山下いくと](../Page/山下いくと.md "wikilink")）
    - 未完終止連載
  - [明日的今日子
    特別編](../Page/明日的今日子.md "wikilink")（原名：、作者：[いわさきまさかず](../Page/いわさきまさかず.md "wikilink")）
  - [神槍少女](../Page/神槍少女.md "wikilink")（[相田裕](../Page/相田裕.md "wikilink")）
  - [那是突然出現的、生命中的另一半](../Page/那是突然出現的、生命中的另一半.md "wikilink")（原名：、作者：[ヒロモトヒロキ](../Page/ヒロモトヒロキ.md "wikilink")）

### MediaWorks企劃

  - [詩∽片](../Page/詩∽片.md "wikilink")（うた∽かた）
  - [宇宙星路](../Page/宇宙星路.md "wikilink")（宇宙のステルヴィア）
  - [拜託了老師](../Page/拜託了老師.md "wikilink")（おねがい☆ティーチャー）
  - [拜託了雙子星](../Page/拜託了雙子星.md "wikilink")（おねがい☆ツインズ）
  - [Kanon](../Page/Kanon.md "wikilink")
  - [神是中學生](../Page/神是中學生.md "wikilink")（かみちゅ\!）
  - （ガンパレード・マーチ）
  - [機動戰士GUNDAM0079](../Page/機動戰士GUNDAM0079.md "wikilink")（）
  - （G.G.F ～Gamers Guardian Fairies～）
  - （ゴクドーくん漫遊記）
  - [漫畫派對](../Page/漫畫派對.md "wikilink")（こみっくパーティー）
  - （木漏れ日の並木道）
  - （G-onらいだーす）
  - [妹妹公主](../Page/妹妹公主.md "wikilink")（シスター・プリンセス）
  - [神魂合體 GODANNAR\!\!](../Page/神魂合體.md "wikilink")（）
  - （スターシップ・オペレーターズ）
  - （ストラトス・フォー）
  - [ZEGAPAIN外傳](../Page/ZEGAPAIN.md "wikilink")（ZEGAPAIN
    -ゼーガペイン-|ゼーガペイン外伝）
  - [星界的紋章](../Page/星界的紋章.md "wikilink")（星界の紋章）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（蒼穹のファフナー）
  - （ダブルブリッド）
  - [To Heart](../Page/To_Heart.md "wikilink")
  - （ToHeart Remember my memories）
  - [變形金剛](../Page/變形金剛.md "wikilink")（トランスフォーマー キスぷれ）
  - （フィギュア17 つばさ&ヒカル）
  - （ブギーポップ・デュアル 負け犬たちのサーカス）
  - [雙戀](../Page/雙戀.md "wikilink")
  - （フタコイ オルタナティブ）
  - （プリズム・アーク）
  - （ヘキサムーン・ガーディアンズ）
  - （ベターマン）
  - （マブラヴ）
  - （マブラヴ アンリミテッド）
  - （魔法遊戯）
  - （無限のリヴァイアス）
  - （メルクリウスプリティ）
  - [勇者王](../Page/勇者王.md "wikilink")（勇者王ガオガイガー）
  - （ゆめりあ）
  - [萊姆色戰奇譚](../Page/萊姆色戰奇譚.md "wikilink")（らいむいろ戦奇譚）
  - [鍊金三級魔法少女](../Page/鍊金三級魔法少女.md "wikilink")（錬金3級 まじかる？ぽか～ん）
  - [sola](../Page/sola.md "wikilink")
  - [ADVANCE OF Ζ
    提坦斯的旗下](../Page/ADVANCE_OF_Ζ_提坦斯的旗下.md "wikilink")（ADVANCE
    OF Ζ ティターンズの旗のもとに）
  - [少女愛上姐姐](../Page/少女愛上姐姐.md "wikilink")（乙女はお姉さまに恋してる）
  - [校園烏托邦 學美向前衝！](../Page/校園烏托邦_學美向前衝！.md "wikilink")（がくえんゆーとぴあ
    まなびストレート\!）
  - [女生愛女生](../Page/女生愛女生.md "wikilink")（かしまし ～ガール・ミーツ・ガール～）
  - （ガンパレード・オーケストラ）
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（灼眼のシャナ）
  - [真月譚月姬](../Page/真月譚月姬.md "wikilink")（真月譚 月姫）
  - [超級機械人大戰原創世紀
    -聖十字軍戰爭-](../Page/超級機器人大戰原創世紀.md "wikilink")（スーパーロボット大戦OG
    -ディバイン・ウォーズ-）
  - [To Heart 2](../Page/To_Heart_2.md "wikilink")
  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（夜明け前より瑠璃色な）
  - [神的記事本](../Page/神的記事本.md "wikilink")（作画：[Tiv](../Page/Tiv.md "wikilink")、原作：[杉井光](../Page/杉井光.md "wikilink")、角色原案：[岸田梅爾](../Page/岸田梅爾.md "wikilink")）
  - [野狼大神與七位夥伴](../Page/野狼大神.md "wikilink")（原作：[沖田雅](../Page/沖田雅.md "wikilink")、作画：[珠洲城くるみ](../Page/珠洲城くるみ.md "wikilink")、角色設計：[うなじ](../Page/うなじ_\(イラストレーター\).md "wikilink")）

## 外部連結

  - [月刊Comic電擊大王](https://web.archive.org/web/20110424204139/http://daioh.dengeki.com/)　公式網頁
  - [電擊帝王](https://web.archive.org/web/20070207021639/http://www.mediaworks.co.jp/magazine/enter/tei.php)　姐妹雜誌「電擊帝王」公式網頁
  - [電擊萌王](https://web.archive.org/web/20070314005556/http://www.mediaworks.co.jp/special/moeoh/index_d.php)　姐妹雜誌「電擊萌王」公式網頁

[\*](../Category/月刊Comic電擊大王.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:青年漫畫雜誌](../Category/青年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:1994年創辦的雜誌](../Category/1994年創辦的雜誌.md "wikilink")
[Category:1994年日本建立](../Category/1994年日本建立.md "wikilink")