[Shone_ace.jpg](https://zh.wikipedia.org/wiki/File:Shone_ace.jpg "fig:Shone_ace.jpg")

《**月刊少年Ace**》（[日文原名](../Page/日文.md "wikilink")：）是[角川書店發行的](../Page/角川書店.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")，於1994年3月創刊，其前身為《[月刊Comic
Comptiq](../Page/月刊Comic_Comptiq.md "wikilink")》，因為角川的內部紛爭而停刊。最初發行時多以前身的書籍發行。[台灣東販曾經以](../Page/台灣東販.md "wikilink")《**頂尖少年Ace**》的名義發行中文版。

## 作品一覽

### 連載中的作品

*更新至2012年11月26日*

  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")（[吉崎觀音](../Page/吉崎觀音.md "wikilink")）
  - [成惠的世界](../Page/成惠的世界.md "wikilink")（[丸川友廣](../Page/丸川友廣.md "wikilink")）
  - [強殖裝甲GUYVER](../Page/強殖裝甲.md "wikilink")（[高屋良樹](../Page/高屋良樹.md "wikilink")）
  - [新世紀福音戰士：碇真嗣育成計畫](../Page/新世紀福音戰士：碇真嗣育成計畫_\(漫畫\).md "wikilink")（[高橋脩](../Page/高橋脩.md "wikilink")、[Khara](../Page/Khara工作室.md "wikilink")・[GAINAX](../Page/GAINAX.md "wikilink")（原作））
  - [涼宮春日的憂鬱](../Page/涼宮春日系列.md "wikilink")（[ツガノガク](../Page/都賀野岳.md "wikilink")、[谷川流](../Page/谷川流.md "wikilink")（原作）、[伊東雜音](../Page/伊東雜音_\(1999年出道\).md "wikilink")（角色原案））
  - [Fate/stay
    night](../Page/Fate/stay_night.md "wikilink")（[西脇だっと](../Page/西脇だっと.md "wikilink")、[TYPE-MOON](../Page/TYPE-MOON.md "wikilink")（原作））
  - [天降之物](../Page/天降之物.md "wikilink")（[水無月嵩](../Page/水無月嵩.md "wikilink")）
  - [死囚樂園](../Page/死囚樂園.md "wikilink")（[片岡人生](../Page/片岡人生.md "wikilink")、[近藤一馬](../Page/近藤一馬.md "wikilink")）
  - [RATMAN](../Page/RATMAN.md "wikilink")（[犬威赤彥](../Page/犬威赤彥.md "wikilink")）
  - [小凉宫春日的忧郁](../Page/小凉宫春日的忧郁.md "wikilink")（[ぷよ](../Page/ぷよ.md "wikilink")、谷川流（原作）、伊東雜音（角色原案））
  - [東京事件](../Page/東京事件.md "wikilink")（[菅野博之](../Page/菅野博之.md "wikilink")、[大塚英志](../Page/大塚英志.md "wikilink")（原作））
  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")（[まったくモー助](../Page/まったくモー助.md "wikilink")、[夢唄](../Page/夢唄.md "wikilink")、[井上堅二](../Page/井上堅二.md "wikilink")（原作）、[葉賀唯](../Page/葉賀唯.md "wikilink")（角色原案））
  - [笨蛋，測驗，召喚獸chu](../Page/笨蛋，測驗，召喚獸.md "wikilink")（[KOIZUMI](../Page/KOIZUMI.md "wikilink")、井上堅二（原作）、葉賀唯（角色原案））
  - [東京ESP](../Page/東京ESP.md "wikilink")（[瀬川はじめ](../Page/瀬川はじめ.md "wikilink")）
  - [東京闇鴉](../Page/東京闇鴉.md "wikilink")、（[鈴見敦](../Page/鈴見敦.md "wikilink")、あざの耕平（原作）、すみ兵（角色原案））
  - [Code
    Geass漆黑的連夜](../Page/Code_Geass漆黑的連夜.md "wikilink")（[たくま朋正](../Page/たくま朋正.md "wikilink")、[谷口悟朗](../Page/谷口悟朗.md "wikilink")（原案・脚本））
  - [Trauma量子結晶](../Page/Trauma量子結晶.md "wikilink")、（[青木ハヤト](../Page/青木ハヤト.md "wikilink")）
  - [狐之惡魔與黑魔導書](../Page/狐之惡魔與黑魔導書.md "wikilink")、（橘由宇）
  - [残念博士](../Page/残念博士.md "wikilink")（[瀨野反人](../Page/瀨野反人.md "wikilink")）
  - [UPOTTE\!\!](../Page/UPOTTE!!.md "wikilink")、（[天王寺狐](../Page/天王寺狐.md "wikilink")）
  - [鍾愛短劍流](../Page/鍾愛短劍流.md "wikilink")、[竹刀苦短，少女戀愛吧！](../Page/竹刀苦短，少女戀愛吧！.md "wikilink")（[神崎かるな](../Page/神崎かるな.md "wikilink")、[黑神遊夜](../Page/黑神遊夜.md "wikilink")（原作））
  - [Steins;Gate
    史上最強的輕度狂熱](../Page/Steins;Gate.md "wikilink")（[森田柚花](../Page/森田柚花.md "wikilink")、[5pb](../Page/5pb.md "wikilink")×[Nitro+](../Page/Nitro+.md "wikilink")（原作）、とねね（劇本））
  - [BIG
    ORDER](../Page/BIG_ORDER.md "wikilink")、（[えすのサカエ](../Page/えすのサカエ.md "wikilink")）
  - [まほマほ](../Page/まほマほ.md "wikilink")（[兒玉樹](../Page/兒玉樹.md "wikilink")）
  - [棺姬嘉依卡](../Page/棺姬嘉依卡.md "wikilink")、（[茶菓山しん太](../Page/茶菓山しん太.md "wikilink")、[榊一郎](../Page/榊一郎.md "wikilink")（原作）、なまにくATK（角色原案））
  - [のろガール\!](../Page/のろガール!.md "wikilink")（[久遠まこと](../Page/久遠まこと.md "wikilink")）
  - [なまちゅー。](../Page/なまちゅー。.md "wikilink")（[井冬良](../Page/井冬良.md "wikilink")）
  - [氷菓](../Page/氷菓.md "wikilink")（[タスクオーナ](../Page/タスクオーナ.md "wikilink")、[米澤穗信](../Page/米澤穗信.md "wikilink")（原作）、[西屋太志](../Page/西屋太志.md "wikilink")（角色原案））
  - [交響詩篇AO](../Page/交響詩篇AO.md "wikilink")（[加藤雄一](../Page/加藤雄一.md "wikilink")、[BONES](../Page/BONES_\(動畫製作公司\).md "wikilink")（原作））
  - [BEATLESS
    -dystopia-](../Page/BEATLESS_-dystopia-.md "wikilink")（[鶯神楽](../Page/鶯神楽.md "wikilink")、[長谷敏司](../Page/長谷敏司.md "wikilink")（原作）、[redjuice](../Page/redjuice.md "wikilink")（角色原案））
  - [約會大作戰](../Page/約會大作戰.md "wikilink")（[犬威赤彦](../Page/犬威赤彦.md "wikilink")、[橘公司](../Page/橘公司.md "wikilink")（原作）、[つなこ](../Page/つなこ.md "wikilink")（角色原案））
  - [狗與剪刀必有用](../Page/狗與剪刀必有用.md "wikilink")、（[大庭下門](../Page/大庭下門.md "wikilink")、[更伊俊介](../Page/更伊俊介.md "wikilink")（原作）、[鍋島テツヒロ](../Page/鍋島テツヒロ.md "wikilink")（角色原案））
  - [ふたりは牛頭馬頭](../Page/ふたりは牛頭馬頭.md "wikilink")（[瀨野反人](../Page/瀨野反人.md "wikilink")）
  - [ROBOTICS;NOTES side Junna
    小さな夏のものがたり](../Page/ROBOTICS;NOTES.md "wikilink")（[大庭下門](../Page/大庭下門.md "wikilink")、[5pb.](../Page/5pb..md "wikilink")（原作））

### 連載完畢的作品

  - [幸運☆星](../Page/幸運☆星.md "wikilink")（[美水鏡](../Page/美水鏡.md "wikilink")），其它雜誌仍在連載中

  - [ANGELIC
    LAYER](../Page/ANGELIC_LAYER.md "wikilink")（[CLAMP](../Page/CLAMP.md "wikilink")）

  - [がぁーでぃあんHearts](../Page/がぁーでぃあんHearts.md "wikilink")（[天津冴](../Page/天津冴.md "wikilink")）

  - [女孩萬歲](../Page/女孩萬歲.md "wikilink")（[まりお金田](../Page/まりお金田.md "wikilink")）

  - [交響詩篇艾蕾卡7](../Page/交響詩篇艾蕾卡7.md "wikilink")（[片岡人生](../Page/片岡人生.md "wikilink")、[近藤一馬](../Page/近藤一馬.md "wikilink")、[BONES](../Page/BONES_\(動畫製作公司\).md "wikilink")）

  - [機動戰士CROSSBONE
    GUNDAM](../Page/機動戰士CROSSBONE_GUNDAM.md "wikilink")（[長谷川裕一](../Page/長谷川裕一.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")）

  - [機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")
    R（[戶田泰成](../Page/戶田泰成.md "wikilink")、[千葉智宏](../Page/千葉智宏.md "wikilink")、[矢立肇](../Page/矢立肇.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")）

  - [Canvas2
    ～彩虹色的圖畫～](../Page/Canvas2.md "wikilink")（[兒玉樹](../Page/兒玉樹.md "wikilink")）

  - [美食小天王](../Page/美食小天王.md "wikilink")（[蚵仔煎](../Page/蚵仔煎_\(漫畫家\).md "wikilink")）

  - [鋼鐵天使](../Page/鋼鐵天使.md "wikilink")（[介錯](../Page/介錯_\(漫畫家\).md "wikilink")）

  - [琉球武士瘋雲錄](../Page/琉球武士瘋雲錄.md "wikilink")（[ゴツボマサル](../Page/ゴツボマサル.md "wikilink")、[manglobe](../Page/manglobe.md "wikilink")）

  - [仕切るの?春日部さん](../Page/仕切るの?春日部さん.md "wikilink")（[竹内元紀](../Page/竹内元紀.md "wikilink")）

  - [還想踢波](../Page/好想踢波.md "wikilink")（ゴツボ×リュウジ）

  - [好想踢波](../Page/好想踢波.md "wikilink")（[ゴツボ×リュウジ](../Page/ゴツボ×リュウジ.md "wikilink")）

  - [新・萬能文化貓娘](../Page/萬能文化貓娘.md "wikilink")（[高田裕三](../Page/高田裕三.md "wikilink")）連載中止

  - [Dr.リアンが診てあげる](../Page/Dr.リアンが診てあげる.md "wikilink")（[竹內元紀](../Page/竹內元紀.md "wikilink")）

  - [鋼鐵神兵](../Page/鋼鐵神兵.md "wikilink")（[車田正美](../Page/車田正美.md "wikilink")）

  - [ブリザードYuki](../Page/ブリザードYuki.md "wikilink")（[安西真](../Page/安西真.md "wikilink")、[吉岡平](../Page/吉岡平.md "wikilink")）

  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（[片倉真二](../Page/片倉真二.md "wikilink")、GJK（GAINAX、[J.C.STAFF](../Page/J.C.STAFF.md "wikilink")、角川書店））

  - [遊撃宇宙戰艦Nadesico](../Page/機動戰艦.md "wikilink")（[麻宮騎亞](../Page/麻宮騎亞.md "wikilink")）

  - [歡迎加入NHK\!](../Page/歡迎加入NHK!.md "wikilink")（[瀧本龍彥](../Page/瀧本龍彥.md "wikilink")、[大岩ケンヂ](../Page/大岩ケンヂ.md "wikilink")）

  - [食靈](../Page/食靈.md "wikilink")（[瀬川はじめ](../Page/瀬川はじめ.md "wikilink")）

  - [未来日記](../Page/未来日記.md "wikilink")（[えすのサカエ](../Page/えすのサカエ.md "wikilink")）

  - [伊甸少年](../Page/伊甸少年.md "wikilink")（[天王寺狐](../Page/天王寺狐.md "wikilink")）

  - [愛的省錢大作戰](../Page/愛的省錢大作戰.md "wikilink")（[瑪俐歐金田](../Page/瑪俐歐金田.md "wikilink")）

  - [多重人格探偵サイチョコ](../Page/多重人格探偵サイチョコ.md "wikilink")（[ひらりん](../Page/ひらりん.md "wikilink")）

  - [探偵儀式](../Page/探偵儀式.md "wikilink")（[箸井地圖](../Page/箸井地圖.md "wikilink")、[大塚英志](../Page/大塚英志.md "wikilink")、[清涼院流水](../Page/清涼院流水.md "wikilink")）

  - [低俗靈DAYDREAM](../Page/低俗靈DAYDREAM.md "wikilink")（[目黑三吉](../Page/目黑三吉.md "wikilink")、[奥瀬サキ](../Page/奥瀬サキ.md "wikilink")）

  - [超・超・大魔法峠](../Page/大魔法峠.md "wikilink")（[大和田秀樹](../Page/大和田秀樹.md "wikilink")）

  - [BLOOD+](../Page/BLOOD+.md "wikilink")（[桂明日香](../Page/桂明日香.md "wikilink")、[Production
    I.G](../Page/Production_I.G.md "wikilink")、[Aniplex](../Page/Aniplex.md "wikilink")）

  - （天津冴）

  - [Battle Spirits
    少年激霸彈](../Page/Battle_Spirits_少年激霸彈.md "wikilink")（原作：[SUNRISE](../Page/SUNRISE.md "wikilink")，漫畫：東川祥樹）

  - [永恆紀元+](../Page/永恆紀元.md "wikilink")（[真辺広史](../Page/真辺広史.md "wikilink")、[NC
    Japan](../Page/NC_Japan.md "wikilink")（原案））

  - [YUYURU執行部](../Page/YUYURU執行部.md "wikilink")、（[bomi](../Page/bomi.md "wikilink")）

  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（[阿倍野茶子](../Page/阿倍野茶子.md "wikilink")、[三雲岳斗](../Page/三雲岳斗.md "wikilink")（原作）、Gユウスケ（角色原案））

  - [BLOOD-C](../Page/BLOOD-C.md "wikilink")（[琴音らんまる](../Page/琴音らんまる.md "wikilink")、[Production
    I.G](../Page/Production_I.G.md "wikilink")・[CLAMP](../Page/CLAMP.md "wikilink")（原作））

  - [日常](../Page/日常_\(漫畫\).md "wikilink")（[あらゐけいいち](../Page/あらゐけいいち.md "wikilink")）

## 相關條目

  - [GUNDAM ACE](../Page/GUNDAM_ACE.md "wikilink")
  - [ACE桃組](../Page/ACE桃組.md "wikilink")

## 外部連結

  - [官方網站](http://web-ace.jp/shonenace/)

[\*](../Category/月刊少年Ace.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[-](../Category/月刊Newtype.md "wikilink")
[A](../Category/角川書店的漫畫雜誌.md "wikilink")
[Category:1994年創辦的雜誌](../Category/1994年創辦的雜誌.md "wikilink")
[Category:1994年日本建立](../Category/1994年日本建立.md "wikilink")