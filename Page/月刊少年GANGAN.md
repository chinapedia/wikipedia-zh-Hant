《**月刊少年GANGAN**》（日語：月刊少年ガンガン），由日本[Square
Enix發行的月刊少年](../Page/Square_Enix.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")。
因是知名電玩遊戲公司旗下的漫畫月刊，連載過不少知名電玩改編而成的漫畫。母公司有時也根據一些連載的漫畫推出相關的電子遊戲。

由於月刊的厚度比其他漫畫雜誌還要厚，常常連載作品總頁數超過1000頁，因此又有所謂的「殺人電話簿」或是「月刊少年鈍器」這類戲稱。

## 連載中的作品

*更新至2015年12月12日*

  - [藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")（[藤代健](../Page/藤代健.md "wikilink")）2002年6月号-

  - [紅心王子](../Page/紅心王子.md "wikilink")（[桑原草太](../Page/桑原草太.md "wikilink")）2007年5月号-

  - [魔法禁書目錄](../Page/魔法禁書目錄.md "wikilink")（原作：[鎌池和馬](../Page/鎌池和馬.md "wikilink")、作画：[近木野中哉](../Page/近木野中哉.md "wikilink")）2007年5月號-

  - [BAMBOO BLADE
    B](../Page/BAMBOO_BLADE_B.md "wikilink")（原作：[土塚理弘](../Page/土塚理弘.md "wikilink")
    作画：[五十嵐あぐり](../Page/五十嵐あぐり.md "wikilink")）2009年2月号-

  - [加油！菜鳥老師](../Page/加油！菜鳥老師.md "wikilink")（[尾高純一](../Page/尾高純一.md "wikilink")）2009年4月号-

  - [王國之心
    358/2天](../Page/王國之心_358/2天.md "wikilink")（[天野シロ](../Page/天野シロ.md "wikilink")）2009年9月号-

  - [Red Raven](../Page/Red_Raven.md "wikilink")
    （[藤本新太](../Page/藤本新太.md "wikilink")） 2010年5月号-

  - [海貓鳴泣之時散 episode7 - Requiem of the golden
    witch](../Page/海貓鳴泣之時散_episode7_-_Requiem_of_the_golden_witch.md "wikilink")
    （原作・監修：[竜騎士07](../Page/竜騎士07.md "wikilink")、作画：[水野英多](../Page/水野英多.md "wikilink")）
    2011年5月号-

  - [咲-Saki-阿知賀編 episode of side-A](../Page/咲-Saki-.md "wikilink")
    （原作：[小林立](../Page/小林立.md "wikilink")、作画：[五十嵐あぐり](../Page/五十嵐あぐり.md "wikilink")）
    2011年9月号-

  - [ピース\*2](../Page/ピース*2.md "wikilink")
    （[鳴海けい](../Page/鳴海けい.md "wikilink")） 2011年9月号-

  - （[若林稔弥](../Page/若林稔弥.md "wikilink")） 2012年4月号-

  - [最終幻想零式外傳 冰劍的死神](../Page/最終幻想_零式.md "wikilink")
    （[塩沢天人志](../Page/塩沢天人志.md "wikilink")、監修：[野村哲也](../Page/野村哲也.md "wikilink")）
    2012年5月号-

  - [ROBOTICS;NOTES Dream Seeker](../Page/ROBOTICS;NOTES.md "wikilink")
    （原作：[5pb.](../Page/5pb..md "wikilink")、作画：[知乃綴](../Page/知乃綴.md "wikilink")）
    2012年10月号-

  - [元氣囝仔](../Page/元氣囝仔.md "wikilink") （ヨシノサツキ）2014年8月号-

  - （[岸本聖史](../Page/岸本聖史.md "wikilink")） 2014年11月号-

  - [再世のファンタズマ](../Page/再世のファンタズマ.md "wikilink") （赤人義一） 2015年8月号-

## 連載過的著名作品

  - [鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")（[荒川弘](../Page/荒川弘.md "wikilink")）2001年8月号-2010年6月號
  - [我的主人愛作怪番外編](../Page/我的主人愛作怪.md "wikilink")（原作：[まっつー](../Page/まっつー.md "wikilink")
    　漫画：[椿あす](../Page/椿あす.md "wikilink")）
  - [噬魂者](../Page/噬魂者.md "wikilink")（[大久保篤](../Page/大久保篤.md "wikilink")）
  - [屍姬](../Page/屍姬.md "wikilink")（[赤人義一](../Page/赤人義一.md "wikilink")）
  - [BLOODY CROSS](../Page/BLOODY_CROSS.md "wikilink")
    （ブラッディ・クロス、[米山シヲ](../Page/米山シヲ.md "wikilink")）
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")（原作：[城平京](../Page/城平京.md "wikilink")
    、構成：[左有秀](../Page/左有秀.md "wikilink")、作画：[彩崎廉](../Page/彩崎廉.md "wikilink")）
  - [罪惡王冠](../Page/罪惡王冠.md "wikilink")
    （構成：[宮城陽亮](../Page/宮城陽亮.md "wikilink")、作画：[満月シオン](../Page/満月シオン.md "wikilink")）
  - [JUDGE](../Page/JUDGE.md "wikilink")
    (ジャッジ[外海良基](../Page/外海良基.md "wikilink"))
  - [Soul Eater
    Not\!](../Page/Soul_Eater_Not!.md "wikilink")（[大久保篤](../Page/大久保篤.md "wikilink")）
  - [666～サタン～](../Page/666～撒旦～.md "wikilink")（[岸本聖史](../Page/岸本聖史.md "wikilink")）
  - [里見☆八犬伝](../Page/里見☆八犬伝.md "wikilink")（[よしむらなつき](../Page/よしむらなつき.md "wikilink")）
  - [少年偵探～推理之絆～](../Page/少年偵探.md "wikilink")（原作：[城平京](../Page/城平京.md "wikilink")
    作画：[水野英多](../Page/水野英多.md "wikilink")）
  - [少年偵探～螺旋重現～](../Page/少年偵探.md "wikilink")（原作：[城平京](../Page/城平京.md "wikilink")
    作画：[水野英多](../Page/水野英多.md "wikilink")）
  - [勇者鬥惡龍](../Page/勇者鬥惡龍.md "wikilink")（[藤原カムイ](../Page/藤原カムイ.md "wikilink")）
  - [勇者鬥惡龍
    伊甸的戰士](../Page/勇者鬥惡龍.md "wikilink")（[藤原カムイ](../Page/藤原カムイ.md "wikilink")）
  - [勇者鬥惡龍
    怪獸仙境PLUS＋](../Page/勇者鬥惡龍.md "wikilink")（[吉崎觀音](../Page/吉崎觀音.md "wikilink")）
  - [魔偵探洛基](../Page/魔偵探洛基.md "wikilink")（[木下さくら](../Page/木下さくら.md "wikilink")）
  - [咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣.md "wikilink")（[衛藤ヒロユキ](../Page/衛藤ヒロユキ.md "wikilink")）
  - [寒蟬鳴泣之時](../Page/寒蟬鳴泣之時.md "wikilink")（原作：[竜騎士07](../Page/竜騎士07.md "wikilink")）
  - [热带雨林的爆笑生活](../Page/热带雨林的爆笑生活.md "wikilink")（[金田一蓮十郎](../Page/金田一蓮十郎.md "wikilink")）
  - [ひょっとこスクール](../Page/ひょっとこスクール.md "wikilink")（[武凪知](../Page/武凪知.md "wikilink")）
  - [トライピース](../Page/トライピース.md "wikilink")（[丸智之](../Page/丸智之.md "wikilink")）
  - [RUN day
    BURST](../Page/RUN_day_BURST.md "wikilink")（[長田悠幸](../Page/長田悠幸.md "wikilink")）
  - [仕立屋工房 Artelier
    Collection](../Page/仕立屋工房_Artelier_Collection.md "wikilink")（[日丘円](../Page/日丘円.md "wikilink")）
  - [SCRAMBLE\!](../Page/SCRAMBLE!.md "wikilink")（[柴田亞美](../Page/柴田亞美.md "wikilink")）2009年7月号-
  - [ごちそうシネマした\!\!](../Page/ごちそうシネマした!!.md "wikilink")（[IKARING](../Page/IKARING.md "wikilink")）
  - [HEROMAN](../Page/HEROMAN.md "wikilink")（原作：[スタン・リー](../Page/スタン・リー.md "wikilink")、作画：[太田多門](../Page/太田多門.md "wikilink")、動畫製作：[BONES](../Page/BONES_\(動畫製作公司\).md "wikilink")）
  - [アルティメットドールズ](../Page/アルティメットドールズ.md "wikilink")（[三好輝](../Page/三好輝.md "wikilink")）
  - [ばのてん\!](../Page/ばのてん!.md "wikilink")（[河添太一](../Page/河添太一.md "wikilink")）
  - [獸神演武](../Page/獸神演武.md "wikilink")（原案：[黄金周](../Page/黄金周_\(作者\).md "wikilink")
    腳本：[社綾](../Page/社綾.md "wikilink")
    作画：[荒川弘](../Page/荒川弘.md "wikilink")）
  - [漫畫家和助手](../Page/漫畫家和助手.md "wikilink")（[ヒロユキ](../Page/ヒロユキ.md "wikilink")）

## 連載結束的作品

  - [吸血十字界](../Page/吸血十字界.md "wikilink")（原作：[城平京](../Page/城平京.md "wikilink")
    作画：[木村有里](../Page/木村有里.md "wikilink")）2003年9月号-2007年3月号

## 外部链接

  - [ガンガンNET（日）](http://gangan.square-enix.co.jp/)
  - [ガンガン連載作品（GanganSearch）（日）](http://www.gangansearch.com/kikaku/review/gangan/)

[\*](../Category/月刊少年GANGAN.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[G](../Category/史克威爾艾尼克斯的漫畫雜誌.md "wikilink")