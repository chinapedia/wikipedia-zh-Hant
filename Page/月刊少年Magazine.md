《**月刊少年Magazine**》（）是[日本](../Page/日本.md "wikilink")[講談社發行的一本](../Page/講談社.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")[月刊](../Page/月刊.md "wikilink")。専門連載[少年漫畫](../Page/少年漫畫.md "wikilink")。1965年創刊時名《別冊少年Magazine》。其後經過一段休刊期間，於1974年復刊，1975年改名《月刊少年Magazine》。

在少年漫畫雑誌當中，銷量排在[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")、[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")、、[週刊少年Sunday後的第](../Page/週刊少年Sunday.md "wikilink")5位。特徵為長期連載作品比較多。

## 刊載漫畫列表

### 連載中作品

  - }}

<table>
<thead>
<tr class="header">
<th><p>作品名稱（中文）</p></th>
<th><p>作品名稱（日文）</p></th>
<th><p>作者（作畫）</p></th>
<th><p>原作</p></th>
<th><p>起載號</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/C.M.B.森羅博物館之事件目錄.md" title="wikilink">C.M.B.森羅博物館之事件目錄</a></p></td>
<td></td>
<td><p><a href="../Page/加藤元浩.md" title="wikilink">加藤元浩</a></p></td>
<td></td>
<td><p>2005年10月號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/前川剛.md" title="wikilink">前川剛</a></p></td>
<td></td>
<td><p>2006年9月號</p></td>
<td><p>《鐵拳小子》系列續篇</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南瓜剪刀.md" title="wikilink">南瓜剪刀</a></p></td>
<td></td>
<td><p><a href="../Page/岩永亮太郎.md" title="wikilink">岩永亮太郎</a></p></td>
<td></td>
<td><p>2006年11月號</p></td>
<td><p>從《<a href="../Page/Magazine_+.md" title="wikilink">GREAT</a>》移籍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/假面骑士SPIRITS.md" title="wikilink">新假面騎士SPIRITS</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/石之森章太郎.md" title="wikilink">石之森章太郎</a></p></td>
<td><p>2009年8月號</p></td>
<td><p>從《》移籍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/真白之音.md" title="wikilink">真白之音</a></p></td>
<td></td>
<td><p><a href="../Page/羅川真里茂.md" title="wikilink">羅川真里茂</a></p></td>
<td></td>
<td><p>2010年5月號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/流浪神差.md" title="wikilink">流浪神差</a></p></td>
<td></td>
<td><p><a href="../Page/安達渡嘉.md" title="wikilink">安達渡嘉</a></p></td>
<td></td>
<td><p>2011年1月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舞動青春.md" title="wikilink">舞動青春</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>2011年12月號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>魚糕鎮的推薦</p></td>
<td></td>
<td><p>平野直樹</p></td>
<td></td>
<td><p>2012年5月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2014年1月號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>隱瞞之事</p></td>
<td></td>
<td><p><a href="../Page/久米田康治.md" title="wikilink">久米田康治</a></p></td>
<td></td>
<td><p>2016年1月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍帥之翼.md" title="wikilink">龍帥之翼 史記·留侯世家異傳</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>2016年4月號</p></td>
<td><p>未在電子版中刊載</p></td>
</tr>
<tr class="even">
<td><p>再見了我的克拉默</p></td>
<td></td>
<td><p><a href="../Page/新川直司.md" title="wikilink">新川直司</a></p></td>
<td></td>
<td><p>2016年6月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍狼傳.md" title="wikilink">龍狼傳 霸王立國篇</a></p></td>
<td></td>
<td><p><a href="../Page/山原義人.md" title="wikilink">山原義人</a></p></td>
<td></td>
<td><p>2016年8月號</p></td>
<td><p>從2018年10月號開始隔月連載</p></td>
</tr>
<tr class="even">
<td><p>努力喜歡</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2017年2月號</p></td>
<td><p>短期集中連載</p></td>
</tr>
<tr class="odd">
<td><p>今夜月美願君亡</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2017年5月號</p></td>
<td><p>從《》移籍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/終結的熾天使.md" title="wikilink">終結的熾天使：一瀨紅蓮，破滅的16歲</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/鏡貴也.md" title="wikilink">鏡貴也</a></p></td>
<td><p>2017年7月號</p></td>
<td><p>《終結的熾天使》外傳作品</p></td>
</tr>
<tr class="odd">
<td><p>Change!</p></td>
<td><p>{{lang|ja|Change</p></td>
<td><p>}}</p></td>
<td><p><a href="../Page/曾田正人.md" title="wikilink">曾田正人</a></p></td>
<td></td>
<td><p>2017年12月號</p></td>
</tr>
<tr class="even">
<td><p>廢柴女友愛撒驕</p></td>
<td></td>
<td><p><a href="../Page/吉田渚經.md" title="wikilink">吉田渚經</a></p></td>
<td></td>
<td><p>2018年2月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>菲爾瑪的料理</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2018年10月號</p></td>
<td><p>不定期連載</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/灌籃少年.md" title="wikilink">灌籃少年 ACT4</a></p></td>
<td></td>
<td><p><a href="../Page/八神浩樹.md" title="wikilink">八神浩樹</a></p></td>
<td></td>
<td><p>2018年11月號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>{{lang|ja|ボイスラ</p></td>
<td><p>}}</p></td>
<td><p>Octo</p></td>
<td></td>
<td><p>2019年1月號</p></td>
</tr>
<tr class="even">
<td><p>Goodbye！異世界轉生</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/齊藤健二.md" title="wikilink">齊藤健二</a></p></td>
<td><p>2019年3月號</p></td>
<td></td>
</tr>
</tbody>
</table>

### 過去連載過作品

  - [BECK](../Page/BECK.md "wikilink")（[ハロルド作石](../Page/ハロルド作石.md "wikilink")）
  - [龍狼傳](../Page/龍狼傳.md "wikilink")（[山原義人](../Page/山原義人.md "wikilink")）
  - [櫻花大戰](../Page/櫻花大戰.md "wikilink")
  - [俏妞老師](../Page/俏妞老師.md "wikilink")（日：）（[上村純子](../Page/上村純子.md "wikilink")）
  - [宇宙鐵人兄弟](../Page/宇宙鐵人兄弟.md "wikilink")（日：）（[石森章太郎](../Page/石森章太郎.md "wikilink")）
  - [灌籃少年
    ACT3](../Page/灌籃少年.md "wikilink")（[八神浩樹](../Page/八神浩樹.md "wikilink")）
  - [ALIVE
    最终进化的少年](../Page/ALIVE_最终进化的少年.md "wikilink")（[河島正](../Page/河島正.md "wikilink")・[あだちとか](../Page/あだちとか.md "wikilink")）
  - [ANGEL
    BEAT](../Page/ANGEL_BEAT.md "wikilink")（[安原依若](../Page/安原依若.md "wikilink")）
  - [Oh\!
    透明人間](../Page/Oh!_透明人間.md "wikilink")（[中西廣居](../Page/中西廣居.md "wikilink")）
  - [海皇紀](../Page/海皇紀.md "wikilink")（[川原正敏](../Page/川原正敏.md "wikilink")）
  - [野球太保](../Page/野球太保.md "wikilink")（[七三太朗](../Page/七三太朗.md "wikilink")・[川三番地](../Page/川三番地.md "wikilink")）
  - [鬼太郎](../Page/鬼太郎.md "wikilink")（日：）鬼太郎[地獄編](../Page/地獄.md "wikilink")（[水木茂](../Page/水木茂.md "wikilink")）
  - [青春巧克力](../Page/青春巧克力.md "wikilink")（日：）原作（[大槻ケンヂ](../Page/大槻ケンヂ.md "wikilink")）作画（[佐佐木勝彦與](../Page/佐佐木勝彦.md "wikilink")[清水澤亮](../Page/清水澤亮.md "wikilink")）
  - [俠義少年王](../Page/俠義少年王.md "wikilink")（[沢田ひろふみ](../Page/沢田ひろふみ.md "wikilink")）
  - [修羅之刻](../Page/修羅之刻.md "wikilink")（川原正敏）
  - [修羅之門](../Page/修羅之門.md "wikilink")（川原正敏）
  - [修羅之門
    第貳門](../Page/修羅之門.md "wikilink")（日：）（[川原正敏](../Page/川原正敏.md "wikilink")）
  - [問題小子孫六](../Page/問題小子孫六.md "wikilink")（日：）（[さだやす圭](../Page/さだやす圭.md "wikilink")）
  - [超速球](../Page/超速球.md "wikilink")（日：）（[七三太朗](../Page/七三太朗.md "wikilink")・[川三番地](../Page/川三番地.md "wikilink")）
  - [修羅之門異傳](../Page/修羅之門異傳.md "wikilink")（日：）（川原正敏・[飛永宏之](../Page/飛永宏之.md "wikilink")）
  - [スーパーくいしん坊](../Page/スーパーくいしん坊.md "wikilink")（[ビッグ錠](../Page/ビッグ錠.md "wikilink")）
  - [ソップ型](../Page/ソップ型.md "wikilink")（[西本英雄](../Page/西本英雄.md "wikilink")）
  - [大暴走](../Page/大暴走.md "wikilink")（[手塚治虫](../Page/手塚治虫.md "wikilink")）
  - [鐵拳小子](../Page/鐵拳小子.md "wikilink")（日：）（[前川剛](../Page/前川剛.md "wikilink")）
  - [四月是你的謊言](../Page/四月是你的謊言.md "wikilink")（[新川直司](../Page/新川直司.md "wikilink")）2011年5月号
    -
  - [天才笨蛋阿松](../Page/天才笨蛋阿松.md "wikilink")（日：）（[赤塚不二夫](../Page/赤塚不二夫.md "wikilink")）
  - [どっきんロリポップ](../Page/どっきんロリポップ.md "wikilink")（[井澤まさみ](../Page/井澤まさみ.md "wikilink")）
  - [バイオレンスジャック](../Page/バイオレンスジャック.md "wikilink")（[永井豪與](../Page/永井豪.md "wikilink")[ダイナミックプロ](../Page/ダイナミックプロ.md "wikilink")）
  - [ハートキャッチいずみちゃん](../Page/ハートキャッチいずみちゃん.md "wikilink")（[遠山光](../Page/遠山光.md "wikilink")）
  - [ぼくのブラジャー・アイランド](../Page/ぼくのブラジャー・アイランド.md "wikilink")（[井澤滿](../Page/井澤滿.md "wikilink")・[いがらしゆみこ](../Page/いがらしゆみこ.md "wikilink")）
  - [魔馬](../Page/魔馬.md "wikilink")（日：）（[手塚治虫](../Page/手塚治虫.md "wikilink")）
  - [F.C.ジンガ](../Page/F.C.ジンガ.md "wikilink")（[とだ勝之](../Page/とだ勝之.md "wikilink")）
  - [名門\!多古西應援團](../Page/名門!多古西應援團.md "wikilink")（[所十三](../Page/所十三.md "wikilink")）
  - [變形故事集](../Page/變形故事集.md "wikilink")（日：）（[手塚治虫](../Page/手塚治虫.md "wikilink")）
  - [ヤンキー烈風隊](../Page/ヤンキー烈風隊.md "wikilink")（[もとはしまさひで](../Page/もとはしまさひで.md "wikilink")）
  - [レッケン\!](../Page/レッケン!.md "wikilink")（[吉谷やしよ](../Page/吉谷やしよ.md "wikilink")）
  - [RiN](../Page/RiN.md "wikilink")（[ハロルド作石](../Page/ハロルド作石.md "wikilink")）
  - [FIRE
    BALL\!](../Page/FIRE_BALL!.md "wikilink")（[龍幸伸](../Page/龍幸伸.md "wikilink")）
  - [火箭人](../Page/火箭人.md "wikilink")（日：）（[加藤元浩](../Page/加藤元浩.md "wikilink")）
  - 秘石战记（日：）（宇野比呂士）
  - [金の彼女
    銀の彼女](../Page/金の彼女_銀の彼女.md "wikilink")（[赤衣丸歩郎](../Page/赤衣丸歩郎.md "wikilink")）
  - [フィールドの花子さん](../Page/フィールドの花子さん.md "wikilink")（[千田純生](../Page/千田純生.md "wikilink")）

## 關連項目

  - [週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")

## 外部連結

  - [月刊少年マガジンWEB](http://www.gmaga.co/)

[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[\*](../Category/月刊少年Magazine.md "wikilink")
[-](../Category/週刊少年Magazine.md "wikilink")
[M](../Category/講談社的漫畫雜誌.md "wikilink")