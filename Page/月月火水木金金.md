《**月月火水木金金**》是日本的一首[海軍軍歌](../Page/海軍.md "wikilink")，在日文中一週是以[七曜來表示](../Page/七曜.md "wikilink")，也就是[日](../Page/日曜日.md "wikilink")、[月](../Page/月曜日.md "wikilink")、[火](../Page/火曜日.md "wikilink")、[水](../Page/水曜日.md "wikilink")、[木](../Page/木曜日.md "wikilink")、[金](../Page/金曜日.md "wikilink")、[土](../Page/土曜日.md "wikilink")。這首軍歌歌名的意義代表著一週七天並沒有所謂的[星期六和](../Page/星期六.md "wikilink")[星期日](../Page/星期日.md "wikilink")，慣用表現為「星期六、日也要工作」（）的意思。

以下主要說明的是[第二次世界大戰之前的日本軍歌](../Page/第二次世界大戰.md "wikilink")《月月火水木金金》。

## 由來

  - 最初由[大日本帝國海軍開始使用](../Page/大日本帝國海軍.md "wikilink")。大日本帝國海軍於[日俄戰爭勝利後](../Page/日俄戰爭.md "wikilink")，因「束緊勝利之兜的系帶（）」在休日中仍然進行猛烈操練，有部分海軍[士官向同僚說出](../Page/士官.md "wikilink")「這樣不就是星期一一二三四五五嗎（）」。這句話很就快在海軍中廣為流傳，該語氣看來就令「月月火水木金金」流露著海軍精神，而一般來說其意味是在猛烈操練下想退縮的海軍官兵自嘲之開端。
  - 於[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")，本曲有讚美勤務的意味，因此在日本民間廣泛地使用。

## 軍歌

  - 根據上述所記敘成為日本的[軍歌](../Page/軍歌.md "wikilink")。作詞者為每日在海上艦隊勤務工作且像其他日本[軍艦上的男兒勇渡](../Page/軍艦.md "wikilink")[太平洋的](../Page/太平洋.md "wikilink")[高橋俊策](../Page/高橋俊策.md "wikilink")，而[作曲為海軍](../Page/作曲.md "wikilink")[軍樂隊出身的江口源吾](../Page/軍樂隊.md "wikilink")（[江口夜詩](../Page/江口夜詩.md "wikilink")）。
  - 該曲在1940年（昭和15年）11月，作為[海軍省海軍軍事普及部推薦曲由](../Page/海軍省.md "wikilink")[內田榮一主唱並由](../Page/內田榮一.md "wikilink")[Polydor
    Records發售](../Page/Polydor_Records.md "wikilink")，但據說當時並不暢銷。但是，由於[日本放送協會職員偶然錯誤播放成為契機](../Page/日本放送協會.md "wikilink")，令本曲瞬間成為[流行歌並在日本民間廣為認識而成為逸話](../Page/流行歌.md "wikilink")。
  - [若山彰](../Page/若山彰.md "wikilink")、[伊藤久男亦有演奏過本曲](../Page/伊藤久男.md "wikilink")。別名「艦隊勤務」（在[JASRAC數據庫中](../Page/JASRAC.md "wikilink")，「月月火水木金金」為正式題名，而「艦隊勤務」則是副題）。
  - 現在為[海上自衛隊所演奏的樂曲之一](../Page/海上自衛隊.md "wikilink")。
  - 為[富士電視系](../Page/富士電視系.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")「」主題「」的原譜換詞歌中有名，但在初期亦是使用該曲的原譜換詞。再者，為[The
    Drifters](../Page/The_Drifters.md "wikilink")（）擅長的軍歌，並也唱著該曲。

## 其他

  - 在[波麗佳音發售的鴨子圓舞曲中](../Page/波麗佳音.md "wikilink")（作詞：[横尾嘉信](../Page/横尾嘉信.md "wikilink")／作曲：[横尾嘉信](../Page/横尾嘉信.md "wikilink")／歌：），雖然歌詞含有「月月火水木金土」，但與此曲的表演方式有不少差異。

## 參考資料

  - [](https://archive.is/20130501083456/dic.yahoo.co.jp/dsearch?enc=UTF-8&p=%E6%9C%88%E6%9C%88%E7%81%AB%E6%B0%B4%E6%9C%A8%E9%87%91%E9%87%91&dtype=0&dname=0na&stype=0&pagenum=1&index=05870305612900/)

  -
## 外部連結

  -
  - [試聽](http://www.youtube.com/watch?v=8hJmOf9viL8&mode=related&search=)

[Ketsuketsukasuimokukinkin](../Category/大日本帝國海軍.md "wikilink")
[Ketsuketsukasuimokukinkin](../Category/日本軍歌.md "wikilink")
[Category:海军音乐](../Category/海军音乐.md "wikilink")
[Category:日本流行语](../Category/日本流行语.md "wikilink")