[Imbrium_location.jpg](https://zh.wikipedia.org/wiki/File:Imbrium_location.jpg "fig:Imbrium_location.jpg")\]\]
[Clementine_albedo_simp750.jpg](https://zh.wikipedia.org/wiki/File:Clementine_albedo_simp750.jpg "fig:Clementine_albedo_simp750.jpg")
[lang=zh](https://zh.wikipedia.org/wiki/File:Moon_names.svg "fig:lang=zh")
**月海**是[月球上大块呈黑色的](../Page/月球.md "wikilink")[玄武岩平原](../Page/玄武岩.md "wikilink")，推測是古代[火山爆发的产物](../Page/火山爆发.md "wikilink")。\[1\]

## 名称来源

所谓的月海，并非[月球上面的海洋](../Page/月球.md "wikilink")。其之所以被称之为“海”，是因为早期的观察者，发现到月面有部分地区较暗。而在当时无法清晰观察到月球表面的情况下，观察者们按照其对地球的认识，猜测该地区为海洋，因為其反光度比其他地方较低。相对地，其他比较光亮的地方也就被称之为[月陆了](../Page/月陆.md "wikilink")。此外，还有被称为湖的“[月湖](../Page/月湖\(月球\).md "wikilink")”；被称为湾的“[月湾](../Page/月灣_\(月海\).md "wikilink")”；被称为沼的“[月沼](../Page/月沼.md "wikilink")”。

## 形成

比较多人认为月海是[小天体撞击月球时](../Page/太阳系小天体.md "wikilink")，撞破月壳，使月幔流出，[玄武岩岩浆覆盖了低地](../Page/玄武岩.md "wikilink")，形成了月海。但也有科学家根据对月球各类岩石成份、构造与形成年龄的研究，认为月球约形成于45.6亿年前。月球形成后曾发生过较大规模的岩浆洋事件，通过岩浆的熔离过程和内部物质调整，于41亿年前形成了斜长岩[月壳](../Page/月壳.md "wikilink")、[月幔和](../Page/月幔.md "wikilink")[月核](../Page/月核.md "wikilink")。在40至39亿年前，月球曾遭受到小天体的剧烈撞击，形成广泛分布的月海盆地，称为雨海事件。在39至31.5亿年前，月球发生过多次剧烈的玄武岩喷发事件，大量玄武岩填充了月海，厚度达0.5至2.5千米，称为月海泛滥事件。月海因此而成。两个观点的分别在于，后一观点认为小天体的撞击和玄武岩的喷发分别发生在两个年代；而另一观点则认为是同时发生的。

## 地理特征及分布

月海的地势一般较低，类似地球上的盆地，月海比月球平均水准面低1-2公里。个别最低的海如雨海的东南部甚至比周围低6000米。月球表面有雨海、静海、危海、澄海、丰富海等23个月海（详细请参考[月海列表](../Page/月海列表.md "wikilink")）。大多数月海分布在月球近地面（由于月球是[同轉卫星](../Page/同轉卫星.md "wikilink")，所以月球的一面永远面向地球。详见[月球](../Page/月球.md "wikilink")）。月球的远地面仅有3个月海，还有4个在边缘地区。形成月海分布如此不均的原因相信是地球的[引力](../Page/引力.md "wikilink")。由于月球是的一面永远面向地球，历经亿万年的引力影响之后，科学家相信月球的质心比形心更接近地球。所以[月幔更容易从近地面流出](../Page/月幔.md "wikilink")，使近地面的撞击坑更容易被玄武岩岩浆所“灌溉”，从而导致了分布不均的现象。在月球近地面，月海面積約佔整個半球表面的一半。最大的月海叫“风暴洋”，位于月球的东北部，面积达500万平方公里，约等于9个法国的面积。雨海面积约为90万平方公里；月面中央的[静海约有](../Page/静海_\(月球\).md "wikilink")26万平方公里。月海的面积占月面总面积的16%此外，較大的還有澄海、豐富海、危海、雲海等。美国“[阿波罗](../Page/阿波罗太空船.md "wikilink")”宇宙飞船曾6次在月海上登陆，如“阿波罗-11”号、“阿波罗-17”号着陆于[静海](../Page/静海_\(月球\).md "wikilink")，“阿波罗-12”号着落于风暴洋。宇航员身穿[宇航服](../Page/宇航服.md "wikilink")，在“海面”上行走，并留下一串串约3厘米深的脚印。发现月面的尘土是近于灰色的纤细粉末，有点像带有粘性的木炭屑。

## 月海的资源

填充月海的玄武岩就犹如一个巨大的[钛铁矿的储存库](../Page/钛铁矿.md "wikilink")。据专家的模式计算，共约有体积为106万立方公里的玄武岩分布在月海平原或盆地上。通过已有的探测结果，特别是“[克莱门汀](../Page/克莱门汀.md "wikilink")”号月球探测器的多光谱探测数据，配以目前地球上钛铁矿开采的品位为参考值，可计算出这些玄武岩中钛铁矿达到开发程度的资源量超过100万亿吨。尽管这样所得的结果带着很大的推测性与不确定性。但可以肯定的是，月海玄武岩确实蕴藏着丰富的钛铁矿。而且，钛铁矿不仅是生产金属铁、钛的原料，还是生产[水和火箭燃料](../Page/水.md "wikilink")——[液氧的主要原料](../Page/液氧.md "wikilink")。这意味着对月海玄武岩的探测尤为重要。但遗憾的是，目前对月海玄武岩厚度的探测程度很低，影响了月海玄武岩总体积的计算精度，进而影响了钛铁矿开发利用前景评估的可靠性。

## 参看

  - [月球](../Page/月球.md "wikilink")
  - [月海列表](../Page/月海列表.md "wikilink")
  - [月球地質](../Page/月球地質.md "wikilink")
  - [撞击坑](../Page/撞击坑.md "wikilink")
  - [月球地理学](../Page/月球地理学.md "wikilink")
  - [熔岩平原](../Page/熔岩平原.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - <https://web.archive.org/web/20050312220839/http://database.cpst.net.cn/popul/front/astro/artic/10603110648.html>
  - <http://www.moon.com.cn/moon/scene5.htm>
  - <http://news.xinhuanet.com/st/2003-05/09/content_863787.htm>
  - <https://archive.is/20071008010245/http://www.cdkjx.com/web4/extensive/shownews.asp?newsid=1275>

[\*](../Category/月海.md "wikilink")
[Category:月球科學](../Category/月球科學.md "wikilink")

1.