[BirchReductionScheme.svg](https://zh.wikipedia.org/wiki/File:BirchReductionScheme.svg "fig:BirchReductionScheme.svg")\]\]
**有机氧化还原反应**(Organic redox
reaction)指[有机反应中的](../Page/有机反应.md "wikilink")[氧化还原反应](../Page/氧化还原反应.md "wikilink")，是**有机氧化反应**和**有机还原反应**的统称。在很多有机氧化还原反应中，[电子转移并不实际发生](../Page/电子转移.md "wikilink")，不同于[电化学中的概念](../Page/电化学.md "wikilink")
\[1\]。

常以[氧化数或](../Page/氧化数.md "wikilink")[氧化态作为碳原子](../Page/氧化态.md "wikilink")[氧化程度的判断](../Page/氧化.md "wikilink")：

  - [烷烃](../Page/烷烃.md "wikilink")：-4
  - [烯烃](../Page/烯烃.md "wikilink")、[醇](../Page/醇.md "wikilink")、[卤代烃](../Page/卤代烃.md "wikilink")、[胺](../Page/胺.md "wikilink")：-2
  - [炔烃](../Page/炔烃.md "wikilink")、[酮](../Page/酮.md "wikilink")、[醛](../Page/醛.md "wikilink")、[偕](../Page/偕.md "wikilink")[二醇](../Page/二醇.md "wikilink")：0
  - [羧酸](../Page/羧酸.md "wikilink")、[酰胺](../Page/酰胺.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")：+2
  - [二氧化碳](../Page/二氧化碳.md "wikilink")、[四氯甲烷](../Page/四氯甲烷.md "wikilink")：+4

氧化数升高的反应称为氧化反应，分子中电子云密度降低，氧化数降低称为还原反应，既有升高也有降低的反应称为[歧化反应](../Page/歧化反应.md "wikilink")。例如，烷烃中的[甲烷燃烧氧化](../Page/甲烷.md "wikilink")，可得到[二氧化碳](../Page/二氧化碳.md "wikilink")，氧化数由-4升高到+4。常见的[官能团氧化还原反应包括](../Page/官能团.md "wikilink")：[炔烃到](../Page/炔烃.md "wikilink")[烯烃](../Page/烯烃.md "wikilink")、[烯烃再到](../Page/烯烃.md "wikilink")[烷烃的还原](../Page/烷烃.md "wikilink")；及[醇到](../Page/醇.md "wikilink")[醛](../Page/醛.md "wikilink")，[醛再到](../Page/醛.md "wikilink")[羧酸的氧化](../Page/羧酸.md "wikilink")。

很多有机氧化还原[偶联反应常涉及](../Page/偶联反应.md "wikilink")[自由基中间体](../Page/自由基.md "wikilink")。真正的有机物氧化还原反应存在于一些[电化学合成反应中](../Page/电化学合成.md "wikilink")，例如[Kolbe电解](../Page/Kolbe电解.md "wikilink")。\[2\]

在歧化反应中，反应物的氧化还原在同一个物质中，并形成2个独立的化合物。

[不对称催化还原反应及](../Page/不对称催化还原反应.md "wikilink")[不对称催化氧化反应是](../Page/不对称催化氧化反应.md "wikilink")[不对称合成中的重要内容](../Page/不对称合成.md "wikilink")。

## 有机还原反应

有机还原反应有以下几种主要机理：

  - 直接电子转移—[单电子还原](../Page/单电子还原.md "wikilink")，如[Birch还原](../Page/Birch还原.md "wikilink")
  - [氢负离子转移](../Page/氢负离子.md "wikilink")，如[Meerwein-Ponndorf-Verley还原反应和](../Page/Meerwein-Ponndorf-Verley还原反应.md "wikilink")[氢化铝锂参与的还原反应](../Page/氢化铝锂.md "wikilink")
  - 加[氢还原](../Page/氢.md "wikilink")，比如[Rosenmund还原反应](../Page/Rosenmund还原反应.md "wikilink")，催化剂如[Lindlar催化剂](../Page/Lindlar催化剂.md "wikilink")、[Adkins催化剂](../Page/Adkins催化剂.md "wikilink")
  - [歧化反应](../Page/歧化反应.md "wikilink")，如[Cannizzaro反应](../Page/Cannizzaro反应.md "wikilink")

不符合以上机理的还原反应如[Wolff-Kishner-Huang反应](../Page/沃尔夫－凯惜纳－黄鸣龙还原反应.md "wikilink")。

## 有机氧化反应

有机氧化反应有以下几种主要机理：

  - 单电子转移
  - 经由酸酯中间体，如[铬酸](../Page/铬酸.md "wikilink")、[四氧化锇](../Page/四氧化锇.md "wikilink")、[高锰酸钾](../Page/高锰酸钾.md "wikilink")
  - 氢原子转移，如[自由基卤化反应](../Page/自由基卤化反应.md "wikilink")
  - [氧气氧化](../Page/氧气.md "wikilink")，如[燃烧反应](../Page/燃烧反应.md "wikilink")
  - [臭氧或](../Page/臭氧.md "wikilink")[过氧化物氧化](../Page/过氧化物.md "wikilink")，如[臭氧化反应](../Page/臭氧化反应.md "wikilink")
  - 机理涉及[消除反应](../Page/消除反应.md "wikilink")，如[Swern氧化反应](../Page/Swern氧化反应.md "wikilink")、[Kornblum氧化反应](../Page/Kornblum氧化反应.md "wikilink")，及[IBX酸和](../Page/IBX酸.md "wikilink")[Dess-Martin氧化剂参与的反应](../Page/Dess-Martin氧化剂.md "wikilink")
  - 由[Fremy盐或](../Page/Fremy盐.md "wikilink")[TEMPO等](../Page/四甲基哌啶氧化物.md "wikilink")[亚硝基](../Page/亚硝基.md "wikilink")[自由基氧化](../Page/自由基.md "wikilink")

## 参考资料

<references/>

[\*](../Category/有机氧化还原反应.md "wikilink")

1.  March Jerry; (1885). Advanced Organic Chemistry reactions,
    mechanisms and structure (3rd ed.). New York: John Wiley & Sons,
    inc. ISBN 0-471-85472-7
2.  www.electrosynthesis.com
    [Link](http://www.electrosynthesis.com/news/m6watts.html)