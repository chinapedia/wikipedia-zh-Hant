**有益野草列表**詳列所有被列作野草的[有益野草](../Page/有益野草.md "wikilink")，按其用途而分類。

## 有藥效

  - [寬葉車前](../Page/寬葉車前.md "wikilink")（[Broadleaf
    plantain](../Page/:en:Broadleaf_plantain.md "wikilink")）：[多年生中藥材](../Page/多年生植物.md "wikilink")。
  - [田灌草](../Page/田灌草.md "wikilink")：一種生長在田邊的[草前草](../Page/草前草.md "wikilink")，有藥用功效。
  - [崩大碗](../Page/崩大碗.md "wikilink")
  - [牛蒡](../Page/牛蒡.md "wikilink")：二年生植物。
  - [金錢薄荷](../Page/金錢薄荷.md "wikilink")（[Creeping
    Charlie](../Page/:en:Creeping_Charlie.md "wikilink")）
  - [蒲公英](../Page/蒲公英.md "wikilink")：二年生草本植物，風媒，生長快速，能忍受乾旱。
  - [野葛](../Page/野葛.md "wikilink")（[Kudzu](../Page/:en:Kudzu.md "wikilink")）：多年生植物。
  - [含羞草](../Page/含羞草.md "wikilink")（Bashful mimosa）
      - 紅骨的含羞草可治骨刺。
  - [鴉蒜](../Page/鴉蒜.md "wikilink")（Crow
    garlic）：又名[鴉蔥](../Page/鴉蔥.md "wikilink")，一種野生蒜，可食，有驅蟲功效。
  - [酸模屬植物](../Page/酸模屬.md "wikilink")（Rumex）：例如：[羊蹄](../Page/羊蹄.md "wikilink")，一般在[蕁麻的附近出現](../Page/蕁麻.md "wikilink")。。
  - [香附](../Page/香附.md "wikilink")：塊根可入藥
      - 香附子：香附的塊根，為藥用。
  - [龍葵](../Page/龍葵.md "wikilink")：有輕微毒性，為藥用。

## 可食用

  - [牛蒡](../Page/牛蒡.md "wikilink")：二年生植物，根部可吃。在英國傳統上會用牛蒡和蒲公英的根部釀製飲料。
  - [蒲公英](../Page/蒲公英.md "wikilink")：二年生草本植物，風媒，生長快速，能忍受乾旱。在[維多利亞女王時期的](../Page/維多利亞女王.md "wikilink")[英國](../Page/英國.md "wikilink")，蒲公英曾是當地的主要食物來源\[1\]。《[本草纲目](../Page/本草纲目.md "wikilink")》亦說：「蒲公英嫩苗可食，生食治感染性疾病尤佳。」\[2\]不過，要留意採摘蒲公英，最好选在嫩葉發起時，否則到開花時，葉子會很苦，使可食用性大減\[3\]\[4\]。此外，在英國亦有用蒲公英的花來釀酒，並以根部釀製飲料。
  - [野葛](../Page/野葛.md "wikilink")（[Kudzu](../Page/:en:Kudzu.md "wikilink")）：多年生植物。
  - [泰國含羞草](../Page/泰國含羞草.md "wikilink")：常拿来炒食。
  - [矢車菊](../Page/矢車菊.md "wikilink")：有多種顏色，可直接食用或用來作沙律的伴碟。
  - [繁縷](../Page/繁縷.md "wikilink")：又名[魚腸菜](../Page/魚腸菜.md "wikilink")，是英國的一種常見可食用野草，一般會切碎後用來拌沙律，或與蒲公英一起食用\[5\]。它亦能很好地保護地表。
  - [灰菜](../Page/灰菜.md "wikilink")（藜，[Lambsquarter](../Page/:en:Lamb's_quarter.md "wikilink")）：叶片及嫩芽可以生食，它可以防止[水土流失](../Page/水土流失.md "wikilink")，同时还能驱散寄生在附近作物的[潜叶虫](../Page/潜叶虫.md "wikilink")。
  - [荠菜](../Page/荠菜.md "wikilink")：荠菜的种子、叶和根都可以食用。荠菜还可以入药，用于止血。不過懷有身孕或哺乳中的婦女忌食，有心肺疾病的患者在服用時亦應小心\[6\]。
  - [荨麻](../Page/荨麻.md "wikilink")（[Stinging
    nettle](../Page/:en:Stinging_nettle.md "wikilink")）：浸泡后可以去除其毒性食用，尤其是嫩芽非常可口，含有高蛋白，适宜作汤或作为蔬菜，用荨麻叶做汤在北欧尤其流行。
  - [马齿苋](../Page/马齿苋.md "wikilink")：可生吃或炒熟吃。
  - [西洋菜](../Page/西洋菜.md "wikilink")（Watercress）：一種非常粗生的植物，基本上只要有水就可以種植。在外國通常被當作野草來處理。可生吃或煮菜。
  - [马兰](../Page/馬蘭_\(植物\).md "wikilink")：菊科马兰属多年生草本植物。嫩叶可供食用，称“马兰头”，在中国江南地区常用做蔬菜。
  - [拟石莲](../Page/拟石莲属.md "wikilink")（[Painter's brush
    weed](../Page/:en:Painter's_brush_weed.md "wikilink")，笔刷草）：景天科拟石莲属草本植物。叶片肥厚。

## 有驅蟲功效

  - [鴉蒜](../Page/鴉蔥.md "wikilink")（Crow garlic）：又名鴉蔥，一種野生蒜，可食，有驅蟲功效。
  - [苍耳](../Page/苍耳.md "wikilink")（[Cocklebur](../Page/:en:Cocklebur.md "wikilink")）：能夠驅除[粘蟲](../Page/粘蟲.md "wikilink")（armyworm）。
  - [一枝黄花](../Page/一枝黄花属.md "wikilink")（[Goldenrod](../Page/:en:Goldenrod.md "wikilink")）：能夠驅除害蟲，並為益蟲提供居所。
  - [乳草](../Page/乳草.md "wikilink")（[Milkweed](../Page/:en:Milkweed.md "wikilink")）：能夠驅除[叩頭蟲](../Page/叩頭蟲.md "wikilink")（wireworm）。
  - [大戟](../Page/大戟.md "wikilink")（[Caper
    Spurge](../Page/:en:Caper_Spurge.md "wikilink")）：能夠驅除[鼴鼠](../Page/鼴鼠.md "wikilink")。
  - [宁树](../Page/印度苦楝树.md "wikilink")（[Neem](../Page/:en:Neem.md "wikilink")）：能夠驅除吃葉的昆蟲。
  - [小花蔓澤蘭](../Page/小花蔓澤蘭.md "wikilink")：本是外來入侵種，令林務單位、業者頗為頭疼，且乳汁具有毒性，意讓過敏者不適。然近日[中華民國](../Page/中華民國.md "wikilink")[國立中興大學的農業研究所發現小花蔓澤蘭的萃取液可以使蚊蟲忌避](../Page/國立中興大學.md "wikilink")，尤其是飛[蚋](../Page/蚋.md "wikilink")([小黑蚊](../Page/小黑蚊.md "wikilink"))有顯著的功效。是少數百大侵入物種中目前唯一具有益處的植物。

## 能為益蟲提供生長環境

  - [四葉草](../Page/四葉草.md "wikilink")：能夠吸引昆蟲中的[捕食者](../Page/捕食者.md "wikilink")，亦對泥土有好處。
  - [茄子](../Page/茄屬.md "wikilink")：能夠為捕食[蚜蟲的](../Page/蚜蟲.md "wikilink")[步甲蟲提供屏障](../Page/步甲蟲.md "wikilink")。
  - [野莧](../Page/苋属.md "wikilink")（[Amaranthus](../Page/:en:Amaranthus.md "wikilink")）
  - 野胡萝卜（[Queen Anne's lace](../Page/:en:Daucus_carota.md "wikilink") ）
  - [歐州益母草](../Page/歐州益母草.md "wikilink")：能夠吸引蜜蜂。
  - [野芥末](../Page/野芥末.md "wikilink")：能夠保護昆蟲中的[捕食者](../Page/捕食者.md "wikilink")。
  - [紫澤蘭](../Page/澤蘭屬.md "wikilink")（[Joe-Pye
    weed](../Page/:en:Joe-Pye_weed.md "wikilink")）

## 提供蔽護

  - [阔叶马齿苋](../Page/阔叶马齿苋.md "wikilink")：能夠保護泥土，以免水土流失。可食用，而且花朵顏色鮮艷，所以近年有人用來裝飾庭園\[7\]。

## 吸引害虫

  - [金蓮花](../Page/金蓮花.md "wikilink")（Nasturtium）：又名[旱金蓮](../Page/旱金蓮.md "wikilink")，一般種在[捲心菜及](../Page/捲心菜.md "wikilink")[生菜的旁邊](../Page/生菜.md "wikilink")，能夠吸引害蟲（例如蚜蟲或毛蟲）在其上產卵，從而使其他農作物避免蟲害。

## 能保護農作物

  - [雷公藤](../Page/雷公藤.md "wikilink")

## 觀賞用

  - [拖鞋蘭](../Page/拖鞋蘭.md "wikilink")：在[澳大利亞被列為野草](../Page/澳大利亞.md "wikilink")。
  - [野西瓜苗](../Page/野西瓜苗.md "wikilink")（[Hibiscus
    trionum](../Page/:en:Hibiscus_trionum.md "wikilink")）：又名[香鈴草](../Page/香鈴草.md "wikilink")，[錦葵科](../Page/錦葵科.md "wikilink")，一年生草本植物。原生於[地中海東部](../Page/地中海.md "wikilink")，但後來在南歐擴散。雖然被當作野草處理，亦有當作觀賞植物種在後園的。

## 參考

  - Peterson, L.A. & Peterson, R.T. (1999). A Field Guide to Edible Wild
    Plants: Eastern and central North America. Houghton-Mifflin.
  - Duke, J.A., Foster, S., & Peterson, R.T. (1999). A Field Guide to
    Medicinal Plants and Herbs: Of Eastern and Central North America.
  - Houghton-Mifflin. Gibbon, E. (1988). Stalking the Wild Asparagus.
    Alan C. Hood & Company .
  - 《[本草從新](../Page/本草從新.md "wikilink")》

## 參看

  - [野草](../Page/野草.md "wikilink")
  - [中國可食用野菜列表](../Page/中國可食用野菜列表.md "wikilink")

<!-- end list -->

1.

2.
3.
4.  又見日語維基相關條目及電視節目。在日本的省錢節目裡，多次有參加者在公園裡找蒲公英來充飢的場面。

5.
6.

7.  <http://blog.yuanlinwang.net/?uid-1008-action-viewspace-itemid-69>