**有經者**（阿拉伯語：اهل الكتاب；希伯來語：עם
הספר）各個宗教定義不同，[猶太教的](../Page/猶太教.md "wikilink")「有經者」只限[猶太人自身](../Page/猶太人.md "wikilink")。在[伊斯蘭教指的是受](../Page/伊斯蘭教.md "wikilink")[啟示者](../Page/啟示.md "wikilink")，常指[亚伯拉罕诸教的信徒](../Page/亚伯拉罕诸教.md "wikilink")，包括[猶太人](../Page/猶太人.md "wikilink")（[律法書](../Page/律法書.md "wikilink")）、[基督徒](../Page/基督徒.md "wikilink")（[新約聖經](../Page/新約聖經.md "wikilink")）和[拜星教徒](../Page/拜星教.md "wikilink")，有时也包括[拜火教甚至是](../Page/拜火教.md "wikilink")[佛教徒](../Page/佛教徒.md "wikilink")。\[1\]\[2\]

## 有經者的特徵

  - 信仰[唯一真神與](../Page/唯一真神.md "wikilink")[神創論](../Page/神創論.md "wikilink")
  - 共同的[先知](../Page/先知.md "wikilink")，如[亞伯拉罕](../Page/亞伯拉罕.md "wikilink")、[摩西](../Page/摩西.md "wikilink")（[天主教称梅瑟](../Page/天主教.md "wikilink")，[伊斯兰教称穆萨](../Page/伊斯兰教.md "wikilink")）
  - 信[世界末日](../Page/世界末日.md "wikilink")、死後[復活](../Page/復活.md "wikilink")、[審判](../Page/審判.md "wikilink")、[天使](../Page/天使.md "wikilink")、[撒旦](../Page/撒旦.md "wikilink")、天堂、地獄

## 影響

由於各個流派對於《[古蘭經](../Page/古蘭經.md "wikilink")》的解讀並不一致，故各派的[穆斯林對](../Page/穆斯林.md "wikilink")[猶太人及](../Page/猶太人.md "wikilink")[基督徒的態度](../Page/基督徒.md "wikilink")，亦時有不同。在[穆斯林國家](../Page/穆斯林國家.md "wikilink")，有經者屬於[受保護的次等公民](../Page/齊米.md "wikilink")，至於[多神教與](../Page/多神教.md "wikilink")[無神論者則不在被保護的範圍內](../Page/無神論.md "wikilink")，不過會依據當地政府的規定而時寬時嚴，甚至[佛教徒有時都會被認定為有經者](../Page/佛教.md "wikilink")。值得注意的是，[穆罕默德曾以武力淨化](../Page/穆罕默德.md "wikilink")[阿拉伯半島的信仰](../Page/阿拉伯半島.md "wikilink")，接納有經者的教義是向外擴張、出了阿拉伯半島後才實施的。反之穆罕默德重返麥加前曾說過，對於宗教絕無強迫，卻在重返麥加後根除多神教（這里的有經者的信仰是指一神教信仰）。

## 参考

## 参见

  - [亚伯拉罕诸教](../Page/亚伯拉罕诸教.md "wikilink")
  - [齊米](../Page/齊米.md "wikilink")

[Category:伊斯兰教](../Category/伊斯兰教.md "wikilink")
[Category:亚伯拉罕诸教](../Category/亚伯拉罕诸教.md "wikilink")

1.
2.  <http://www.oxfordreference.com/view/10.1093/acref/9780195125580.001.0001/acref-9780195125580-e-74>