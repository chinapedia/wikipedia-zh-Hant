**有線18台**（i-CABLE Channel
18），是[香港有線電視的其中一個博彩頻道](../Page/香港有線電視.md "wikilink")。主要提供[香港賽馬及財經資訊](../Page/香港賽馬.md "wikilink")，包括賽馬晨操片段，賽馬預測、賽後檢討及馬圈人士訪問等，在香港賽馬日（每年九月份至翌年七月份的星期三、星期六或星期日）會直播[跑馬地馬場或](../Page/跑馬地馬場.md "wikilink")[沙田馬場賽事](../Page/沙田馬場.md "wikilink")，賽前會以文字播映最新賽馬[賠率](../Page/賠率.md "wikilink")。在星期一至五香港股市交易時段，則以文字播映股票價格、最新指數及外匯價位等資料。早年18台有一深入民心的口號:「有線18台，陪您發大財」，觀眾數量隨著有線電視訂戶的增加而逐漸上升。

2018年5月24日早上6時，台號由第18/218頻道改為第618頻道。

## 介紹

18台於1996年賽馬季度首創「L型」平面設計，當馬匹未全部入閘時，及賽事於直路一段的重播後，有線電視會將現場畫面稍為縮小並移至電視畫面的靠右上角，騰出的一些空間（狀如英文字母L）用來播放文字式的即時馬會賽馬賠率，此設計後來被其他電視台於直播賽馬時爭相仿傚使用。18台的另一優點是播放賽馬的時間，比另一電視台充足，有線電視通常以此（播放賽事一場不漏）作為招徠；另一電視台因為廣播條例所限，未能播足所有賽事，而為馬迷所詬病。惟自[數碼電視啟播後](../Page/數碼電視.md "wikilink")，[亞洲電視亦仿效](../Page/亞洲電視.md "wikilink")18台，提供一專用頻道全程播放賽馬賽事。[香港有線電視18台更再接再厲](../Page/香港有線電視18台.md "wikilink")，於2010年10月起，於[高清603台（hd603）台先推出高清版](../Page/香港有線電視體育平台.md "wikilink")《晨操逐格睇》、再於2010年11月28日賽馬日起推出高清版《賽事傳真》及《賽馬結果》節目，務求打造[高清603台（hd603）台成為全港獨有的高清足球](../Page/香港有線電視體育平台.md "wikilink")/賽馬資訊台，帶來耳目一新的視聽享受。2011年1月13日，新增Ch503播放有線18台聲音版本。與本台相類似的頻道包括[now寬頻電視的](../Page/now寬頻電視.md "wikilink")[now668](../Page/now668.md "wikilink")。

## 現任馬評人

  - [方駿暉](../Page/方駿暉.md "wikilink")
  - [黃嘉豪](../Page/黃嘉豪_\(馬評人\).md "wikilink")
  - [積奇](../Page/積奇.md "wikilink")
  - [黃志康](../Page/黃志康.md "wikilink")
  - [章名](../Page/章名.md "wikilink")
  - [文東](../Page/文東.md "wikilink")
  - [吳嵩](../Page/吳嵩.md "wikilink")
  - [朱鎮輝](../Page/朱鎮輝.md "wikilink")
  - [匡公](../Page/匡公.md "wikilink")
  - [心悅](../Page/心悅.md "wikilink")
  - [陳思琦](../Page/陳思琦.md "wikilink")

## 前任馬評人

  - [洪維德](../Page/洪維德.md "wikilink")
  - [黃子豐](../Page/黃子豐.md "wikilink")
  - [馬達](../Page/馬達.md "wikilink")
  - [貝子健](../Page/貝子健.md "wikilink")
  - [卡洛斯](../Page/卡洛斯_\(主持\).md "wikilink")
  - [丹尼斯](../Page/丹尼斯.md "wikilink")
  - [王若舜](../Page/王若舜.md "wikilink")
  - [安德魯](../Page/安德魯.md "wikilink")
  - [陳華棟](../Page/陳華棟.md "wikilink")
  - [珊翠絲](../Page/珊翠絲.md "wikilink")
  - [駱穎豪](../Page/駱穎豪.md "wikilink")

## 節目

賽事傳真：https://c6.staticflickr.com/1/511/31388023821_55b575875a_h.jpg

  - 《晨操逐格睇》
  - 《競馬天下》
  - 《人氣馬場》
  - 《賽馬預測》
  - 《睇片做功課》
  - 《Horse App》
  - 《Smart Punter》
  - 《賽事傳真》
  - 《賽馬結果》
  - 《18賠率版》
  - 《18派彩版》
  - 《18對碰Win》
  - 《贏馬攻略》
  - 《贏馬檔案》
  - 《試閘檔案》
  - 《金融時段》

## 參考文獻

  - [香港有線電視月刊](../Page/香港有線電視.md "wikilink")
  - [香港有線電視網站](../Page/香港有線電視.md "wikilink")

## 外部連結

  - [有線寬頻相關網站 -
    有線18-{台}-](http://epg.i-cable.com/new/ch_content.php?ch=618)
  - [有線電視服務相關網站 -
    有線18-{台}-](http://www.cabletv.com.hk/ct/cabletv.php?id=2&cid=61)
  - [寬頻18馬網](https://web.archive.org/web/20130726051859/http://ihorse.i-cable.com/)

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:香港有線電視](../Category/香港有線電視.md "wikilink")
[Category:香港賽馬](../Category/香港賽馬.md "wikilink")
[Category:粤语电视频道](../Category/粤语电视频道.md "wikilink")