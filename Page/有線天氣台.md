**有線天氣-{台}-**（\[1\]）是[香港有線電視擁有的一條天氣資訊頻道](../Page/香港有線電視.md "wikilink")，該頻道由[香港有線新聞有限公司提供](../Page/香港有線新聞有限公司.md "wikilink")。

有線天氣-{台}-成立後，各電視台紛紛效法，成立類似性質的頻道，包括[now寬頻電視的](../Page/now寬頻電視.md "wikilink")[天氣頻道](../Page/now寬頻電視天氣頻道.md "wikilink")。

有線天氣-{台}-二十四小時無間斷循環播放各類天氣資訊，包括[世界主要](../Page/世界.md "wikilink")[城市的天氣資訊](../Page/城市.md "wikilink")，和[香港即時天氣](../Page/香港.md "wikilink")、各分區天氣、[空氣污染指數](../Page/香港空氣污染指數.md "wikilink")、[潮汐漲退](../Page/潮汐.md "wikilink")、[日出](../Page/日出.md "wikilink")[日落時間及未來幾日的天氣預測](../Page/日落.md "wikilink")。

## 參考

  - 有線電視月刊
  - 有線寬頻網站

## 參考資料

## 外部連結

  - [有線寬頻相關網站 -
    有線天氣-{台}-](http://epg.i-cable.com/new/ch_content.php?ch=006)
  - [有線電視服務相關網站 -
    有線天氣-{台}-](http://www.cabletv.com.hk/ct/cabletv.php?id=1&cid=1)

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:2003年成立的電視台或電視頻道](../Category/2003年成立的電視台或電視頻道.md "wikilink")
[天氣台](../Category/香港有線電視.md "wikilink")

1.