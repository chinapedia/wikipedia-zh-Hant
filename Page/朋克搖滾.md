**朋克搖滾**（）是起源於1974－1976年的[美國](../Page/美國.md "wikilink")、[英國](../Page/英國.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")，是[搖滾樂的分支](../Page/搖滾樂.md "wikilink")[音樂類型](../Page/音樂類型.md "wikilink")，有「反[建制原則](../Page/建制.md "wikilink")」（anti-establishment）的特色。[倫敦的](../Page/倫敦.md "wikilink")[性手槍](../Page/性手槍.md "wikilink")（Sex
Pistols）、[衝擊合唱團](../Page/衝擊合唱團.md "wikilink")；[紐約的](../Page/紐約.md "wikilink")[雷蒙斯合唱團](../Page/雷蒙斯合唱團.md "wikilink")（Ramones）被視為朋克搖滾的先鋒。

## 參考資料

[Category:摇滚乐類型](../Category/摇滚乐類型.md "wikilink")
[Category:龐克搖滾音樂類型](../Category/龐克搖滾音樂類型.md "wikilink")
[Category:朋克文化](../Category/朋克文化.md "wikilink")