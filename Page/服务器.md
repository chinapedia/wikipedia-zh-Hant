**服务器**（）指：

  - 一个管理资源并为用户提供服务的计算机软件，通常分为[文件服务器](../Page/文件服务器.md "wikilink")（能使用户在其它计算机存取[文件](../Page/文件.md "wikilink")），数据库服务器和[应用程序服务器](../Page/应用程序服务器.md "wikilink")。
  - 运行以上软件的[计算机](../Page/计算机.md "wikilink")，或稱為**[網路主機](../Page/主机_\(网络\).md "wikilink")**（Host）。

[Rack001.jpg](https://zh.wikipedia.org/wiki/File:Rack001.jpg "fig:Rack001.jpg")
[Inside_and_Rear_of_Webserver.jpg](https://zh.wikipedia.org/wiki/File:Inside_and_Rear_of_Webserver.jpg "fig:Inside_and_Rear_of_Webserver.jpg")內部\]\]
[Dell_PowerEdge_Servers.jpg](https://zh.wikipedia.org/wiki/File:Dell_PowerEdge_Servers.jpg "fig:Dell_PowerEdge_Servers.jpg")PowerEdge[機架伺服器(Rack)](../Page/刀鋒伺服器.md "wikilink")\]\]

一般来说，服务器通过网络对外提供服务。可以通过[Intranet对内网提供服务](../Page/Intranet.md "wikilink")，也可以通过[Internet对外提供服务](../Page/Internet.md "wikilink")。伺服器的最大特點，就是運算能力須非常強大，在短時間內就要完成所有運算工作，即使是一部簡單的伺服器系統，至少就要有兩顆中央處理器同時工作。20世纪90年代（公元1990年代）之後，在電話數據機MODEM技術成熟後，由窄頻的電話撥接，升級成為寬頻資料，這代表網路新時代來臨，由慢跑的速度改變而成開車的速度，也同時改變電腦使用者習慣，更大大普及網路聯絡傳訊的方式，以往只能文字傳訊提升至影音傳輸；而雲端、大資料時代造就了各種新型態行業，如網路商店、網路電商、網路拍賣、網路銷售、網路遊戲、網路設計及架設，以及越來越普遍性的雲端資料庫或備份庫，標準伺服器(Server)及檔案伺服器(NAS)的普及正在時時優化及改變現有人類的生活。

## 定义

有时，这两种定义会引起混淆，如[网页服务器](../Page/网页服务器.md "wikilink")。它可能是指用于[网站的计算机](../Page/网站.md "wikilink")，也可能是指像[Apache这样的软件](../Page/Apache.md "wikilink")，运行在这样的计算机上以管理网页组件和回应[网页浏览器的请求](../Page/网页浏览器.md "wikilink")。
伺服器的构成包括处理器、硬盘、内存、系统总线等，和通用的计算机架构类似，但是由于需要提供高可靠的服务，因此在处理能力、稳定性、可靠性、安全性、可扩展性、可管理性等方面要求较高。
在网络环境下，根据服务器提供的服务类型不同，分为文件服务器，数据库服务器，应用程序服务器，WEB服务器等。

## 服务器（硬件）

服务器作为[硬件来说](../Page/硬件.md "wikilink")，通常是指那些具有较高计算能力，能够提供给多个用户使用的计算机。服务器与[PC机的不同点很多](../Page/PC.md "wikilink")，例如[PC机在一个时刻通常只为一个用户服务](../Page/PC.md "wikilink")。服务器与主機不同，主机是通过[终端给用户使用的](../Page/终端.md "wikilink")，服务器是通过网络给[客户端用户使用的](../Page/客户端.md "wikilink")，所以除了要有擁有終端裝置，還要利用網路才能使用伺服器電腦，但用戶連上線後就能使用伺服器上的特定服務了。

和普通的[個人電腦相比](../Page/個人電腦.md "wikilink")，
服务器需要连续的工作在7X24小时环境。这就意味着服务器需要更多的稳定性技术[RAS](../Page/可靠性、可用性和可維護性.md "wikilink")，比如支持使用ECC内存。並通常會有多部連接在一起運作。

根据不同的计算能力，服务器又分为工作组级服务器，部门级服务器和企业级服务器。服务器操作系统是指运行在服务器硬件上的操作系统。服务器[操作系统需要管理和充分利用服务器硬件的计算能力并提供给服务器硬件上的软件使用](../Page/操作系统.md "wikilink")。

现在，市场上有很多为服务器作平台的操作系统。[类Unix操作系统是](../Page/类Unix.md "wikilink")[Unix的后代](../Page/Unix.md "wikilink")，大多为作服务器平台
设计。常见的此类[类Unix服务器操作系统有各种](../Page/类Unix.md "wikilink")[Linux发行版](../Page/Linux发行版.md "wikilink")（如[红帽企业Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")、[SUSE](../Page/SUSE.md "wikilink")）、[AIX](../Page/AIX.md "wikilink")、[HP-UX](../Page/HP-UX.md "wikilink")、[IRIX](../Page/IRIX.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Mac
OS X
Server](../Page/Mac_OS_X.md "wikilink")、[OpenBSD](../Page/OpenBSD.md "wikilink")、[NetBSD](../Page/NetBSD.md "wikilink")、和SCO
OpenServer。[微软也出版了](../Page/微软.md "wikilink")[Microsoft
Windows服务器版本](../Page/Microsoft_Windows.md "wikilink")，像早期的[Windows
NT Server](../Page/Windows_NT_Server.md "wikilink")，后来的[Windows 2000
Server和](../Page/Windows_2000_Server.md "wikilink")[Windows Server
2003和](../Page/Windows_Server_2003.md "wikilink")[Windows Server
2008](../Page/Windows_Server_2008.md "wikilink")，乃至现在的[Windows Server
2012](../Page/Windows_Server_2012.md "wikilink")。微软还在2016年10月12日推出[Windows
Server 2016](../Page/Windows_Server_2016.md "wikilink")\[1\]

## 服务器（[软件](../Page/软件.md "wikilink")）

服务器[软件的定义如前面所述](../Page/软件.md "wikilink")，服务器软件工作在[客户端-服务器或](../Page/客户端-服务器.md "wikilink")[浏览器-服务器的方式](../Page/浏览器-服务器.md "wikilink")，有很多形式的服务器，常用的包括：

  - [文件服务器](../Page/文件服务器.md "wikilink")（File
    Server）或[網路儲存設備](../Page/網路儲存設備.md "wikilink")（Network
    Attached
    Storage）——如[Novell的](../Page/Novell.md "wikilink")[NetWare](../Page/Novell_NetWare.md "wikilink")

  - [数据库服务器](../Page/数据库服务器.md "wikilink")（Database
    Server）——如[Oracle数据库服务器](../Page/Oracle.md "wikilink")，[MySQL](../Page/MySQL.md "wikilink")，[MariaDB](../Page/MariaDB.md "wikilink")，[PostgreSQL](../Page/PostgreSQL.md "wikilink")，[Microsoft
    SQL
    Server](../Page/Microsoft_SQL_Server.md "wikilink")，[MongoDB](../Page/MongoDB.md "wikilink")，[Redis等](../Page/Redis.md "wikilink")

  - （Mail
    Server）——[Sendmail](../Page/Sendmail.md "wikilink")、[Postfix](../Page/Postfix.md "wikilink")、[Qmail](../Page/Qmail.md "wikilink")、[Microsoft
    Exchange](../Page/Microsoft_Exchange.md "wikilink")、[Lotus
    Domino](../Page/Lotus_Domino.md "wikilink")、[dovecot等](../Page/dovecot.md "wikilink")

  - [網頁伺服器](../Page/網頁伺服器.md "wikilink")（Web
    Server）——如[Apache](../Page/Apache.md "wikilink")、[lighttpd](../Page/lighttpd.md "wikilink")、[nginx](../Page/nginx.md "wikilink")、[微软的](../Page/微软.md "wikilink")[IIS等](../Page/Internet_Information_Server.md "wikilink")

  - [FTP服务器](../Page/文件传输协议.md "wikilink")（FTP
    Server）——[Pureftpd](../Page/Pureftpd.md "wikilink")、[Proftpd](../Page/Proftpd.md "wikilink")、[WU-ftpd](../Page/WU-ftpd.md "wikilink")、[Serv-U](../Page/Serv-U.md "wikilink")、[vs-ftpd等](../Page/vs-ftpd.md "wikilink")。

  - [域名服务器](../Page/域名服务器.md "wikilink")（DNS
    Server）——如[Bind等](../Page/Bind.md "wikilink")

  - [應用程式伺服器](../Page/應用程式伺服器.md "wikilink")（Application Server/AP
    Server）——如Bea公司的[WebLogic](../Page/WebLogic.md "wikilink")、[JBoss](../Page/JBoss.md "wikilink")、[Sun的](../Page/Sun.md "wikilink")[GlassFish](../Page/GlassFish.md "wikilink")

  - [代理服务器](../Page/代理服务器.md "wikilink")（Proxy Server）——如[Squid
    cache](../Page/Squid_cache.md "wikilink")

  - [電腦名稱轉換伺服器](../Page/電腦名稱轉換伺服器.md "wikilink")——如[微軟的](../Page/微軟.md "wikilink")[WINS伺服器](../Page/Windows_Internet_Name_Service.md "wikilink")

  - 其他，如[Minecraft遊戲伺服器等](../Page/Minecraft.md "wikilink")。

## 外型

伺服器常見的外型有四種：塔式伺服器、機架伺服器(Rack)、[刀鋒伺服器](../Page/刀鋒伺服器.md "wikilink")（Blade
Server）、机柜式。

## 安全性

伺服器常遭到駭客攻擊，所以伺服器都要不定時的去做檢查。

若無有效防火牆管理，或是未做系統漏洞更新，伺服器本身因為固定IP的關係，更易成为攻击者的目标，因此新聞常見網路勒索綁架檔案。除了家用主機之外，工作場所的伺服器是不法者所特別注視的目標。

## 参见

  - [点对点](../Page/点对点.md "wikilink")
  - [客户端-服务器模型](../Page/客户端-服务器.md "wikilink")
  - [根服务器](../Page/根服务器.md "wikilink")
  - [计算机硬件历史](../Page/计算机硬件历史.md "wikilink")
  - [CORBA](../Page/CORBA.md "wikilink")
  - [網路器械公司](../Page/網路器械公司.md "wikilink")

## 参考文献

[服务器](../Category/服务器.md "wikilink")
[Category:系统软件](../Category/系统软件.md "wikilink")
[Category:网络软件](../Category/网络软件.md "wikilink")
[Category:網路硬體](../Category/網路硬體.md "wikilink")
[Category:電腦的類別](../Category/電腦的類別.md "wikilink")

1.   ZDNet|accessdate=2016-10-16|last=Foley|first=Mary Jo|work=ZDNet}}