在[物理学和](../Page/物理学.md "wikilink")[化学中](../Page/化学.md "wikilink")，**朗德\(g\)因子**是[阿尔佛雷德·朗德试图解释反常](../Page/阿尔佛雷德·朗德.md "wikilink")[塞曼效应时](../Page/塞曼效应.md "wikilink")，于1921年提出的一个[无量纲物理量](../Page/无量纲.md "wikilink")\[1\]\[2\]\[3\]\[4\]，反映了塞曼效应中[磁矩与](../Page/磁矩.md "wikilink")[角动量之间的联系](../Page/角动量.md "wikilink")。其定义后来被推广到其它领域，在[粒子物理学中常常被简称为](../Page/粒子物理学.md "wikilink")\(g\)因子。

## 塞曼效应

塞曼效应中的朗德\(g\)因子由下式给出\[5\]

\[g_J=1+\frac{J(J+1)-L(L+1)+S(S+1)}{2J(J+1)}\]

式中\(L,S,J\)分别是[原子能态](../Page/原子.md "wikilink")（光谱支项）的[角量子数](../Page/角量子数.md "wikilink")、[自旋量子数和内](../Page/自旋量子数.md "wikilink")[量子数](../Page/量子数.md "wikilink")。

### 导引

朗德假定\[6\]，当两个角动量\(\mathbf{L}\hbar\)与\(\mathbf{S}\hbar\)[耦合时](../Page/角动量耦合.md "wikilink")，它们的相互作用能由下式给出：

\[E_{\text{interaction}}=\Gamma(\mathbf{L}\cdot\mathbf{S}),\quad \Gamma\,\text{ constant}\]

令

\[\mathbf{J}\hbar=\mathbf{L}\hbar+\mathbf{S}\hbar\]
为耦合后的总角动量，则可以证明\[7\]，在上述形式的相互作用能下，\(\mathbf{L}\hbar\)与\(\mathbf{S}\hbar\)将绕向量\(\mathbf{J}\hbar\)[进动](../Page/进动.md "wikilink")。

在外加磁场的作用下，带电粒子的角动量会绕外加磁场的方向进动。在这种情况下，是\(\mathbf{J}\hbar\)进行进动。朗德采用了一种简化处理的方法，即认为外磁场中的原子的能量仅仅与向量\(\mathbf{L}\hbar\)与\(\mathbf{S}\hbar\)的长时间[平均值有关](../Page/平均值.md "wikilink")，而后者恰好就是它们在\(\mathbf{J}\hbar\)方向上的[投影](../Page/投影.md "wikilink")，即\[8\]

\[(\mathbf{L})_{\text{av}}=\frac{\mathbf{J}(\mathbf{L}\cdot\mathbf{J})}{J^2},\quad (\mathbf{S})_{\text{av}}=\frac{\mathbf{J}(\mathbf{S}\cdot\mathbf{J})}{J^2}\]

随后，朗德进一步假定，角动量\(\mathbf{L}\hbar\)贡献的由经典的公式给出，并假定\(\mathbf{J}\hbar\)是量子化的，其沿着磁场方向的分量由磁量子数\(M\)确定，即

\[E_{\text{magnetic},L}=-\boldsymbol{\mu}\cdot\mathbf{B}=\left(\frac {e}{2m}(\mathbf{L})_{\text{av}}\hbar\right)\cdot\mathbf{B}
=(\mathbf{L})_{\text{av}}\cdot\mathbf{B}\mu_B=\frac{M(\mathbf{L}\cdot\mathbf{J})}{J^2}\mu_BB\]

式中\(\boldsymbol{\mu}\)是[磁矩](../Page/磁矩.md "wikilink")，而\(\mu_B\)為[波耳磁子](../Page/波耳磁子.md "wikilink")。类似地，朗德写出了角动量\(\mathbf{S}\hbar\)带来的能量贡献，但他发现为了与实验结果相一致，需要加上额外的因子2。当时朗德并不清楚为什么\[9\]，现在我们知道这就是电子的自旋\(g\)因子。即：

\[E_{\text{magnetic},S}=\frac{M(\mathbf{S}\cdot\mathbf{J})}{J^2}2\mu_BB\]

将上面结果加起来，朗德得到下列的表达式，并引入符号\(g\)\[10\]，这就是朗德\(g\)因子的最早来源：

\[E_{\text{magnetic}}=\frac{M[(\mathbf{L}+2\mathbf{S})\cdot\mathbf{J}]}{J^2}\mu_BB=gM\mu_BB\]

利用关系式\(\mathbf{L}\)+\(2 \mathbf{S}\)=\(\mathbf{J}\)+\(\mathbf{S}\)，朗德得到：

\[g=\frac{(\mathbf{L}+2\mathbf{S})\cdot\mathbf{J}}{J^2}=1+\frac{\mathbf{S}\cdot\mathbf{J}}{J^2}=1+\frac{J^2-L^2+S^2}{2J^2}\]

但是，朗德发现，为了与实验结果相符，这一表达式需要修改为下式，当时朗德并不清楚原因\[11\]。现在来看，只要将上面的角动量矢量都作为[算符来处理](../Page/算符.md "wikilink")，然后将对应的[角动量平方算符用其](../Page/角动量算符.md "wikilink")[本征值取代](../Page/本征值.md "wikilink")，得出这个结果是很自然的。

\[g=1+\frac{J(J+1)-L(L+1)+S(S+1)}{2J(J+1)}\]

### 推广

从上面的导引可见，定义朗德\(g\)因子的式子是

\[E_{\text{magnetic}}=gM\mu_BB\]

上式可以等价地表述为：

\[\boldsymbol{\mu}_J=g\frac{e}{2m}\mathbf{J}\]

很自然的推广是将两边的\(J\)同时换成\(L,S\)等，并对不同的粒子将\(m\)换成对应粒子的质量。这就是现在广泛使用的朗德\(g\)因子。

## 粒子物理学

粒子物理学中的\(g\)因子是自旋\(g\)因子，根据自旋角动量和自旋磁矩按照上面的形式定义。

### 电子

上面的导引已经给出了电子自旋\(g\)因子的定义。在实际使用中，它的符号有两种取法，用不同的符号表记：

\[g_{\rm e}\approx -2.002319, g_S=|g_{\rm e}|=-g_{\rm e}\]

#### 歷史沿革

歷史上，它的理論值有過變動：

  - 在[非相對論量子力學理論下考慮](../Page/非相對論量子力學.md "wikilink")[自旋-軌道作用時](../Page/自旋-軌道作用.md "wikilink")，等效地說，\(g_s\)為1。
      - 若再額外考慮[狹義相對論](../Page/狹義相對論.md "wikilink")[時間展長效應下的](../Page/時間展長.md "wikilink")[湯瑪斯進動修正](../Page/湯瑪斯進動.md "wikilink")（1927年），\(g_s\)變為2，方合乎當代實驗觀測值。
  - 在[相對論量子力學](../Page/相對論量子力學.md "wikilink")，也就是指[保羅·狄拉克所提出的理論](../Page/保羅·狄拉克.md "wikilink")（1928年），\(g_s\)恰恰為2；並不如前者採外加修正的方法，是具有一致性的理論可導出的自然結果。
  - 在[量子電動力學](../Page/量子電動力學.md "wikilink")（QED）中，因為電子與[真空能量的電磁漲落交互作用](../Page/真空能量.md "wikilink")，可表為[單環費因曼圖](../Page/單環費因曼圖.md "wikilink")，提出QED的[朱利安·施温格等人](../Page/朱利安·施温格.md "wikilink")（1947年）所得的\(g_s\)理論值为\(\left( 2 + \frac{\alpha}{2\pi} + O(\alpha^2) \right) \approx 2.002\ 319\ 304\ 402\)\[12\]；α目前被視為是自然常數之一，其值約為\(\frac{1}{137.035\ 999\ 11(46)}\)。

[威利斯·蘭姆等人實驗觀測到的](../Page/威利斯·蘭姆.md "wikilink")[蘭姆位移效應](../Page/蘭姆位移.md "wikilink")，所得\(g_s\)觀測值为\(2.002\ 319\ 304\ 376\ 8(86)\)，與理論相符精準度達小數點下第9位，展現出量子電動力學等現代物理理論所能達到的驚人精準預測程度。

### 其它粒子

一些粒子的朗德\(g\)因子列表如下：

| [粒子](../Page/粒子.md "wikilink") | 朗德\(g\)因子              | [{{math](../Page/不确定性.md "wikilink") |
| ------------------------------ | ---------------------- | ------------------------------------ |
| [电子](../Page/电子.md "wikilink") | \-2.002 319 304 361 53 | 0.000 000 000 000 53                 |
| [中子](../Page/中子.md "wikilink") | \-3.826 085 45         | 0.000 000 90                         |
| [质子](../Page/质子.md "wikilink") | 5.585 694 713          | 0.000 000 046                        |
| [μ子](../Page/μ子.md "wikilink") | \-2.002 331 8418       | 0.000 000 0013                       |

[NIST](../Page/NIST.md "wikilink") 提供的朗德\(g\)因子的值\[13\]

## 注釋

## 參考文獻

## 参见

  - [波耳磁子](../Page/波耳磁子.md "wikilink")
  - [湯瑪斯進動](../Page/湯瑪斯進動.md "wikilink")
  - [量子電動力學](../Page/量子電動力學.md "wikilink")
  - [電子自旋共振](../Page/電子自旋共振.md "wikilink")

[D](../Category/量子力学.md "wikilink")

1.

2.

3.

4.

5.  *Quantum Chemistry: Fifth Edition*, Ira N. Levine, 2000

6.

7.
8.
9.
10.
11.
12. V. W. Hughes and T. Kinoshita "Anomalous *g* values of the electron
    and muon" *Review of Modern Physics* 71, 133（1999）

13.