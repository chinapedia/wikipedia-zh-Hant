**望都县**处于[北纬](../Page/北纬.md "wikilink")38°30'46"至38°48'30"
[东经](../Page/东经.md "wikilink")115°01'116"至115°18'13"之间，在[中国](../Page/中国.md "wikilink")[河北省中部偏西](../Page/河北省.md "wikilink")、[唐河流域](../Page/唐河.md "wikilink")，是[保定市下辖的一个县](../Page/保定.md "wikilink")，平均海拔41米，年均日照2677.88小时，年均气温为11.8℃，年际间气温差异不大。年均地面温度为19.6℃；年均降水量为508.9毫米。望都位于保定和[石家庄之间](../Page/石家庄.md "wikilink")，[京广铁路](../Page/京广铁路.md "wikilink")、[107国道](../Page/107国道.md "wikilink")、[G4高速公路纵贯全县南北](../Page/G4高速公路.md "wikilink")。

县政府驻[望都镇中华街](../Page/望都镇.md "wikilink")80号。

## 历史沿革

望都古属[冀州](../Page/冀州_\(古代\).md "wikilink")，为尧帝受封故土，故以尧母名之为庆都。[春秋和](../Page/春秋.md "wikilink")[战国初期属](../Page/战国.md "wikilink")[中山国](../Page/中山国.md "wikilink")，战国后期为[赵国](../Page/赵国.md "wikilink")**庆都邑**，[秦时属](../Page/秦朝.md "wikilink")[恒山郡](../Page/恒山郡.md "wikilink")；[高祖六年](../Page/汉高祖.md "wikilink")（前201年）设**望都县**，属[冀州刺史部](../Page/冀州刺史部.md "wikilink")[中山郡](../Page/中山郡.md "wikilink")；[东汉](../Page/东汉.md "wikilink")、[曹魏](../Page/曹魏.md "wikilink")、[西晋皆为县](../Page/西晋.md "wikilink")，属[中山国](../Page/中山郡.md "wikilink")。

[北魏属](../Page/北魏.md "wikilink")[定州](../Page/定州.md "wikilink")[北平郡](../Page/北平郡.md "wikilink")；[北齐](../Page/北齐.md "wikilink")[天保七年](../Page/天保_\(北齐\).md "wikilink")（556年）省入北平县；[隋](../Page/隋朝.md "wikilink")[开皇六年复置县](../Page/开皇.md "wikilink")，大定初又废\[1\]。

[唐](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年）重置，属定州，县城初在[安喜故城](../Page/安喜.md "wikilink")，[贞观八年](../Page/贞观_\(唐朝\).md "wikilink")（634年）移于现址\[2\]；[宋属](../Page/宋朝.md "wikilink")[河北西路](../Page/河北西路.md "wikilink")[定州](../Page/定州.md "wikilink")；[金属](../Page/金朝.md "wikilink")[河北西路](../Page/河北西路.md "wikilink")[中山府](../Page/中山府.md "wikilink")，[大定十三年](../Page/大定_\(金朝\).md "wikilink")（1173年）改为**庆都县**；[元初属](../Page/元朝.md "wikilink")[真定路](../Page/真定路.md "wikilink")，[元太宗十一年](../Page/元太宗.md "wikilink")（1239年）改属[顺天路](../Page/顺天路.md "wikilink")；[明属](../Page/明朝.md "wikilink")[保定府](../Page/保定府.md "wikilink")；[清](../Page/清朝.md "wikilink")[乾隆十一年](../Page/乾隆.md "wikilink")（1749年），[高宗因庆都为尧母之名](../Page/清高宗.md "wikilink")，复改为**望都**。[民国以来因袭之](../Page/民国.md "wikilink")。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [望都县人民政府网](http://www.wdx.gov.cn/)

## 参考文献

[望都县](../Page/category:望都县.md "wikilink")
[县](../Page/category:保定区县市.md "wikilink")
[保定](../Page/category:河北省县份.md "wikilink")

1.  [隋书.地理志](http://www.guoxue.com/shibu/24shi/oldtangsu/jts_043.htm)
2.  [旧唐书.地理志](http://www.guoxue.com/shibu/24shi/oldtangsu/jts_043.htm)