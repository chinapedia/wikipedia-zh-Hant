**朝鮮世宗**（；），即**朝鮮世宗莊宪大王**、**朝鮮莊宪國王**，[朝鲜王朝的第](../Page/朝鲜王朝.md "wikilink")4代国王，1418年至1450年在位。名諱**李祹**（），字**元正**（），[庙号](../Page/庙号.md "wikilink")**世宗**，[明朝賜](../Page/明朝.md "wikilink")[諡号](../Page/諡号.md "wikilink")「莊宪」，朝鮮加諡曰「**莊宪英文睿武仁聖明孝大王**」（）。

在位期間，世宗發明了[訓民正音](../Page/訓民正音.md "wikilink")，對韓國之後的語言和文化發展帶來深遠影響。[韓國人認為他對國家貢獻巨大](../Page/韓國人.md "wikilink")，後世的[韓國史學家通常都尊稱他為](../Page/韓國.md "wikilink")**世宗大王**（），同时，他也被称为“海东堯舜”。

## 生平经历

世宗生于太祖六年（1397年）四月十日\[1\]，是[朝鲜太宗與](../Page/朝鲜太宗.md "wikilink")[元敬王后閔氏的第三个儿子](../Page/元敬王后.md "wikilink")。永乐六年，12歲時被封为**忠寧君**，和沈氏（后来的[昭宪王后](../Page/昭憲王后.md "wikilink")）结婚。永乐十一年，進封**忠寧大君**。

世宗才华横溢，很受太宗喜爱。相反世宗的两位兄长却毫无王者风范，并粗鲁阻挠太宗传位给世宗。两人后来被逐出宫。太宗长子[让宁大君流浪深山](../Page/让宁大君.md "wikilink")，二子则作了[和尚](../Page/和尚.md "wikilink")。1418年8月，太宗退位两个月后，世宗登基。不过太宗在宫内仍保留一定权力，并掌握军事大权直到他1422年去世。

世宗在位期间，朝鲜社会文化得到长足发展，国家繁荣强大。世宗被认为是朝鲜王朝的最出色的国王之一，因此被尊称为“世宗大王”。

1450年，世宗去世，初葬[广州](../Page/瑞草區.md "wikilink")[献陵](../Page/獻仁陵.md "wikilink")，[睿宗元年移葬](../Page/朝鮮睿宗.md "wikilink")[骊州](../Page/驪州市.md "wikilink")[城山](../Page/城山.md "wikilink")[英陵](../Page/英陵.md "wikilink")。

## 主要成就

### 巩固军事

1419年5月，世宗在太宗的建议下，發起[己亥东征](../Page/应永外寇.md "wikilink")。此次东征的目的是清除[对马海峡](../Page/对马海峡.md "wikilink")[倭寇的侵扰](../Page/倭寇.md "wikilink")。此次东征朝鲜擊斃700名倭寇，逮捕110名倭寇，并释放了至少140名被倭寇抓走的中国人。朝鲜方面则有180人阵亡。1419年9月，对马海峡倭寇首领[平真盛被捉获](../Page/平真盛.md "wikilink")，并押到朝鲜王宫。1443年，[癸亥条约签订](../Page/癸亥条约.md "wikilink")，[对马海峡倭寇首领接受朝鲜王朝对对马海峡的宗主权](../Page/对马海峡.md "wikilink")。朝鲜方面也给与平氏倭寇在对马海峡与朝鲜的贸易优先权。\[2\]

在北部边疆，世宗下令修建了[四郡六镇以加强边防](../Page/四郡六镇.md "wikilink")。世宗还制定了诸多巩固国防的法规，并倡导新武器的发明和发展。在世宗的带领下，朝鲜研制出了许多[火箭及火炮](../Page/火箭_\(古代\).md "wikilink")，如對[神機箭的改造](../Page/神機箭.md "wikilink")\[3\]。

1433年，世宗派[金宗瑞击败](../Page/金宗瑞.md "wikilink")[女真](../Page/女真.md "wikilink")[兀狄哈部落](../Page/兀狄哈.md "wikilink")，巩固了对[朝鲜半岛东北部](../Page/朝鲜半岛.md "wikilink")[咸镜道等地方的统治](../Page/咸镜道.md "wikilink")。\[4\]
目前朝鲜半岛的领土基本上与世宗时期相符。

### 发展科技

[Korean_Waterclock.jpg](https://zh.wikipedia.org/wiki/File:Korean_Waterclock.jpg "fig:Korean_Waterclock.jpg")为世宗研制的[水鐘模型](../Page/水鐘.md "wikilink")\]\]
[Korean_celestial_globe.jpg](https://zh.wikipedia.org/wiki/File:Korean_celestial_globe.jpg "fig:Korean_celestial_globe.jpg")和[日晷](../Page/日晷.md "wikilink")\]\]
世宗时期朝鲜的科学技术得到迅猛发展。世宗下令让人整理朝鲜半岛各地的农耕技术，并编定成书，以帮助农民提高农业产量。1429年在世宗监督下写成的《[农事直说](../Page/农事直说.md "wikilink")》是朝鲜第一部农书，记录了种植、收获、播种和土壤处理等[农业技术](../Page/农业技术.md "wikilink")。\[5\]世宗还根据朝鲜的经济状况，调节农税。使农民可以用心耕作，不用担心农民税。宫廷储粮有盈余时，世宗还会将余粮发放给穷人。

世宗时期朝鲜有一位有名的发明家[蒋英实](../Page/蒋英实.md "wikilink")。蒋英实天资聪明但出身低微。世宗的父亲太宗发现蒋英实的才华后立即将其叫到宫中授予官职，并命其进行发明。太宗此举受到宫内大臣的抗议，认为像蒋英实出身这么低微的人是不可以给与此地位的。但世宗却对蒋英实充满信心。在世宗的支持下，蒋英实参考中国和阿拉伯典籍研製出朝鲜王朝自己的[水鐘](../Page/水鐘.md "wikilink")、[浑天仪和](../Page/浑象.md "wikilink")[日晷](../Page/日晷.md "wikilink")。\[6\]

蒋英实最著名的发明是他1442年发明的朝鲜半岛历史上第一个[雨量计](../Page/雨量计.md "wikilink")，一个用于测量雨水的标准体积的容器。不过他的发明并没有保留下来。现存最早的雨量计是1770年[朝鮮英祖时期制作的](../Page/朝鮮英祖.md "wikilink")。据《[承政院日记](../Page/承政院日记.md "wikilink")》记载，朝鮮英祖希望恢复朝鲜在世宗时期的繁荣。在世宗时期的年鉴中，英祖发现了雨量计，于是下令复制。

世宗还下令改革[历法](../Page/历法.md "wikilink")，将[朝鲜历法纬度从](../Page/朝鲜历法.md "wikilink")[北京改为](../Page/北京市.md "wikilink")[汉城](../Page/首爾.md "wikilink")。新的历法使朝鲜天文学家更加准确预定[日食和](../Page/日食.md "wikilink")[月食的时间](../Page/月食.md "wikilink")。

世宗时期，[朝鲜传统医学也得到很大发展](../Page/朝鲜传统医学.md "wikilink")。《[医方类聚](../Page/医方类聚.md "wikilink")》和《[乡药集成方](../Page/乡药集成方.md "wikilink")》的发表被一些历史学者认为是朝鲜传统医学与[中医的分水岭](../Page/中医.md "wikilink")。

### 文化

#### 语言文学

[Hunminjeongum.jpg](https://zh.wikipedia.org/wiki/File:Hunminjeongum.jpg "fig:Hunminjeongum.jpg")》\]\]
世宗大力倡导朝鲜臣民学习[文学](../Page/文学.md "wikilink")。世宗在[景福宮建立](../Page/景福宮_\(韓國\).md "wikilink")[集贤殿](../Page/集贤殿.md "wikilink")，亲自选拔人才进行各种的学术研究。

其中最著名的是1443年的《[训民正音](../Page/训民正音.md "wikilink")》的编辑和[谚文书写系统的发明](../Page/谚文.md "wikilink")。\[7\]\[8\]

朝鲜[谚文的发明大大提高了朝鲜平民的文化普及](../Page/谚文.md "wikilink")，世宗并为朝鲜民族終於创造出独特且方便的书写文字，逐漸形成今天的[朝鮮語](../Page/朝鮮語.md "wikilink")。

世宗本人的文学水平很高，著有《[龙飞御天歌](../Page/龍飛御天歌.md "wikilink")》及《释谱详节》等书。

#### 取缔伊斯兰教

1427年农历四月初四，禮曹建议取缔[伊斯兰教服装](../Page/伊斯兰教.md "wikilink")、祝頌禮，世宗表示赞成。\[9\][高丽时期](../Page/高麗_\(918年－1392年\).md "wikilink")，[元朝](../Page/元朝.md "wikilink")[穆斯林随](../Page/穆斯林.md "wikilink")[蒙古人进入高丽](../Page/蒙古人.md "wikilink")，传播伊斯兰教，李朝初期，沿袭了高丽的做法，允许伊斯兰教礼仪朝贺，惟因憂慮影響朝鮮的[儒教文化](../Page/儒教_\(宗教\).md "wikilink")，至此被取缔。此后伊斯蘭教在韓國幾乎不存在。在20世紀[大韓民國成立後](../Page/大韓民國.md "wikilink")，為引進外來勞工才重新引入。

## 家庭成员

### 王后

<table>
<thead>
<tr class="header">
<th><p>稱號</p></th>
<th><p>生卒年</p></th>
<th><p>本貫</p></th>
<th><p>父母</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/昭憲王后.md" title="wikilink">昭憲王后沈氏</a></p></td>
<td><p>1395－1446</p></td>
<td><p>青松</p></td>
<td><p>靑川府院君<a href="../Page/沈溫.md" title="wikilink">沈溫</a><br />
三韓國大夫人順興安氏</p></td>
<td><p>誕八子二女。</p></td>
</tr>
</tbody>
</table>

### 後宮

<table>
<thead>
<tr class="header">
<th><p>稱號</p></th>
<th><p>生卒年</p></th>
<th><p>本貫</p></th>
<th><p>父母</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/慎嬪金氏.md" title="wikilink">慎嬪金氏</a></p></td>
<td><p>1406－1464</p></td>
<td><p>淸州</p></td>
<td><p>金元<br />
朔寧高氏</p></td>
<td><p>誕六子二女。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/惠嬪楊氏.md" title="wikilink">愍貞嬪楊氏</a><br />
（惠嬪楊氏）</p></td>
<td><p>？－1455</p></td>
<td><p>淸州</p></td>
<td><p>楊景<br />
李氏</p></td>
<td><p>誕三子。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/令嬪姜氏.md" title="wikilink">令嬪姜氏</a></p></td>
<td><p>？－1483</p></td>
<td><p>晉州</small></p></td>
<td></td>
<td><p>誕一子。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貴人朴氏.md" title="wikilink">貴人朴氏</a></p></td>
<td><p>？－？</p></td>
<td><p>密陽</p></td>
<td><p>朴剛生<br />
坡平尹氏</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>貴人崔氏</p></td>
<td><p>？－？</p></td>
<td><p>全州</p></td>
<td><p>崔士儀<br />
順天朴氏</p></td>
<td><p>初封明懿宮主，世宗十年改封貴人[10]。</p></td>
</tr>
<tr class="even">
<td><p>淑儀曺氏</p></td>
<td><p>？－？</p></td>
<td></td>
<td></td>
<td><p>[11]</p></td>
</tr>
<tr class="odd">
<td><p>昭容洪氏</p></td>
<td><p>？－？</p></td>
<td></td>
<td></td>
<td><p>兄長<a href="../Page/洪有勤.md" title="wikilink">洪有勤</a>[12][13]</p></td>
</tr>
<tr class="even">
<td><p>淑容洪氏</p></td>
<td><p>？－1452[14]</p></td>
<td></td>
<td></td>
<td><p>[15]</p></td>
</tr>
<tr class="odd">
<td><p>淑媛李氏</p></td>
<td><p>？－？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尚寢宋氏.md" title="wikilink">尚寢宋氏</a></p></td>
<td><p>1396－1463</p></td>
<td></td>
<td></td>
<td><p>[16]</p></td>
</tr>
<tr class="odd">
<td><p>司記車氏</p></td>
<td><p>？－1444</p></td>
<td></td>
<td></td>
<td><p>世宗26年遭雷擊喪命。</p></td>
</tr>
</tbody>
</table>

### 子女

#### 子

<table>
<thead>
<tr class="header">
<th><p>序[17]</p></th>
<th><p>稱號</p></th>
<th><p>名</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>生母</p></th>
<th><p>配偶</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>嫡長子</p></td>
<td><p><a href="../Page/朝鮮文宗.md" title="wikilink">文宗大王</a></p></td>
<td><p>珦</p></td>
<td><p>1414</p></td>
<td><p>1452</p></td>
<td><p>昭憲王后</p></td>
<td><p><a href="../Page/顯德王后.md" title="wikilink">顯德王后安東權氏</a></p></td>
<td><p>朝鮮第五代君主。</p></td>
</tr>
<tr class="even">
<td><p>嫡二子</p></td>
<td><p><a href="../Page/朝鮮世祖.md" title="wikilink">世祖大王</a></p></td>
<td><p>瑈</p></td>
<td><p>1417</p></td>
<td><p>1468</p></td>
<td><p><a href="../Page/貞熹王后.md" title="wikilink">貞熹王后坡平尹氏</a></p></td>
<td><p>朝鮮第七代君主。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>嫡三子</p></td>
<td><p><a href="../Page/安平大君.md" title="wikilink">安平大君</a></p></td>
<td><p>瑢</p></td>
<td><p>1418[18]</p></td>
<td><p>1453[19]</p></td>
<td><p>府夫人迎日鄭氏<br />
(？-1453年)</p></td>
<td><p>字淸之，號匪懈堂、琅玕居士。<a href="../Page/朝鮮端宗.md" title="wikilink">端宗元年賜死</a>，<a href="../Page/朝鮮英祖.md" title="wikilink">英祖年間復官</a>，諡章昭。<a href="../Page/朝鮮正祖.md" title="wikilink">正祖時配享於莊陵忠臣壇</a>。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>嫡四子</p></td>
<td><p><a href="../Page/臨瀛大君.md" title="wikilink">臨瀛大君</a></p></td>
<td><p>璆</p></td>
<td><p>1420[20]</p></td>
<td><p>1469[21]</p></td>
<td><p>宜寧南氏<br />
齊安府夫人全州崔氏</p></td>
<td><p>字獻之，諡貞簡。外孫女是<a href="../Page/燕山君.md" title="wikilink">燕山君之妻</a><a href="../Page/廢妃慎氏_(燕山君).md" title="wikilink">廢妃慎氏</a>，外曾孫女是<a href="../Page/朝鮮中宗.md" title="wikilink">中宗元妃</a><a href="../Page/端敬王后.md" title="wikilink">端敬王后</a>。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>嫡五子</p></td>
<td><p><a href="../Page/廣平大君.md" title="wikilink">廣平大君</a></p></td>
<td><p>璵</p></td>
<td><p>1425[22]</p></td>
<td><p>1444[23]</p></td>
<td><p>永嘉府夫人平山申氏<br />
(1426年-1498年)</p></td>
<td><p>①字煥之，號明誠堂，諡章懿。<br />
②過繼給叔公<a href="../Page/撫安大君.md" title="wikilink">撫安大君李芳蕃</a>（太祖之子，早夭）為嗣。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>庶長子</p></td>
<td><p><a href="../Page/和義君.md" title="wikilink">和義君</a></p></td>
<td><p>瓔</p></td>
<td><p>1425[24]</p></td>
<td><p>？[25][26]</p></td>
<td><p>令嬪姜氏</p></td>
<td><p>郡夫人密陽朴氏</p></td>
<td><p>字良之。世祖三年遭流放，<a href="../Page/朝鮮中宗.md" title="wikilink">中宗年間復官</a>，<a href="../Page/朝鮮英祖.md" title="wikilink">英祖年間諡忠景</a>。正祖時配享於莊陵忠臣壇。</p></td>
</tr>
<tr class="odd">
<td><p>嫡六子</p></td>
<td><p><a href="../Page/錦城大君.md" title="wikilink">錦城大君</a></p></td>
<td><p>瑜</p></td>
<td><p>1426[27]</p></td>
<td><p>1457[28]</p></td>
<td><p>昭憲王后</p></td>
<td><p>完山府夫人全州崔氏</p></td>
<td><p>試圖幫助端宗復位，於世祖三年賜死，英祖年間諡貞愍。正祖時配享於莊陵忠臣壇。</p></td>
</tr>
<tr class="even">
<td><p>庶二子</p></td>
<td><p><a href="../Page/桂陽君.md" title="wikilink">桂陽君</a></p></td>
<td><p>璔</p></td>
<td><p>1427[29]</p></td>
<td><p>1464[30]</p></td>
<td><p>愼嬪金氏</p></td>
<td><p>旌善郡夫人淸州韓氏<br />
(1426年-1480年)</p></td>
<td><p>字顯之，諡忠昭。妻子是<a href="../Page/昭惠王后.md" title="wikilink">昭惠王后之姊</a>。</p></td>
</tr>
<tr class="odd">
<td><p>嫡七子</p></td>
<td><p><a href="../Page/平原大君.md" title="wikilink">平原大君</a></p></td>
<td><p>琳</p></td>
<td><p>1427[31]</p></td>
<td><p>1445[32]</p></td>
<td><p>昭憲王后</p></td>
<td><p>江寧府夫人南陽洪氏</p></td>
<td><p>①字珍之，號謹行堂。初諡靖德，後改定憲。<br />
②早逝無子，以姪孫<a href="../Page/齊安大君.md" title="wikilink">齊安大君李琄為嗣</a>。</p></td>
</tr>
<tr class="even">
<td><p>庶三子</p></td>
<td><p><a href="../Page/李玒.md" title="wikilink">義昌君</a></p></td>
<td><p>玒</p></td>
<td><p>1428</p></td>
<td><p>1460[33]</p></td>
<td><p>愼嬪金氏</p></td>
<td><p>梁源郡夫人延安金氏</p></td>
<td><p>諡剛悼。外孫為<a href="../Page/朝鮮成宗.md" title="wikilink">成宗女婿高原尉申沆</a>。</p></td>
</tr>
<tr class="odd">
<td><p>庶四子</p></td>
<td><p><a href="../Page/漢南君.md" title="wikilink">漢南君</a></p></td>
<td></td>
<td><p>1429</p></td>
<td><p>1459[34]</p></td>
<td><p>惠嬪楊氏</p></td>
<td><p>安東郡夫人權氏</p></td>
<td><p>世祖三年遭流放，五年病逝。英祖年間諡貞悼。正祖時配享於莊陵忠臣壇。</p></td>
</tr>
<tr class="even">
<td><p>庶五子</p></td>
<td><p><a href="../Page/密城君.md" title="wikilink">密城君</a></p></td>
<td><p>琛</p></td>
<td><p>1430[35]</p></td>
<td><p>1479[36]</p></td>
<td><p>愼嬪金氏</p></td>
<td><p>豊德郡夫人驪興閔氏<br />
(1428年-1497年)</p></td>
<td><p>字文之，初諡章孝，後改孝僖。</p></td>
</tr>
<tr class="odd">
<td><p>庶六子</p></td>
<td><p><a href="../Page/壽春君.md" title="wikilink">壽春君</a></p></td>
<td><p>玹</p></td>
<td><p>1431</p></td>
<td><p>1455[37]</p></td>
<td><p>惠嬪楊氏</p></td>
<td><p>榮川郡夫人迎日鄭氏</p></td>
<td><p>諡安悼。</p></td>
</tr>
<tr class="even">
<td><p>庶七子</p></td>
<td><p><a href="../Page/翼峴君.md" title="wikilink">翼峴君</a></p></td>
<td><p>璭</p></td>
<td><p>1431[38]</p></td>
<td><p>1463[39]</p></td>
<td><p>愼嬪金氏</p></td>
<td><p>金堤郡夫人平壤趙氏</p></td>
<td><p>字光之，諡忠成。</p></td>
</tr>
<tr class="odd">
<td><p>嫡八子</p></td>
<td><p><a href="../Page/永膺大君.md" title="wikilink">永膺大君</a></p></td>
<td><p>琰</p></td>
<td><p>1434[40]</p></td>
<td><p>1467[41]</p></td>
<td><p>昭憲王后</p></td>
<td><p>帶方府夫人礪山宋氏<br />
春城府夫人海州鄭氏</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>庶八子</p></td>
<td><p><a href="../Page/永豊君.md" title="wikilink">永豊君</a></p></td>
<td><p>瑔</p></td>
<td><p>1434[42]</p></td>
<td><p>1457</p></td>
<td><p>惠嬪楊氏</p></td>
<td><p>郡夫人順天朴氏</p></td>
<td><p>妻子是<a href="../Page/死六臣.md" title="wikilink">死六臣之一的</a><a href="../Page/朴彭年.md" title="wikilink">朴彭年之女</a>。世祖三年遭流放，肅宗時期復官，後諡貞烈。正祖時期配享於莊陵忠臣壇。</p></td>
</tr>
<tr class="odd">
<td><p>庶九子</p></td>
<td><p><a href="../Page/寧海君.md" title="wikilink">寧海君</a></p></td>
<td><p>瑭</p></td>
<td><p>1435[43]</p></td>
<td><p>1477[44]</p></td>
<td><p>愼嬪金氏</p></td>
<td><p>林川郡夫人平山申氏</p></td>
<td><p>初名璋，後改瑭。諡安悼。</p></td>
</tr>
<tr class="even">
<td><p>庶十子</p></td>
<td><p><a href="../Page/潭陽君.md" title="wikilink">潭陽君</a></p></td>
<td><p>璖</p></td>
<td><p>1439[45]</p></td>
<td><p>1450[46]</p></td>
<td><p>無</p></td>
<td><p>早卒。初諡懷簡，後改夷哀。姪子江陽君李潚為後嗣。</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 女

<table>
<thead>
<tr class="header">
<th><p>序[47]</p></th>
<th><p>稱號</p></th>
<th><p>名</p></th>
<th><p>生年</p></th>
<th><p>卒年</p></th>
<th><p>生母</p></th>
<th><p>配偶</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>嫡長女</p></td>
<td><p><a href="../Page/貞昭公主.md" title="wikilink">貞昭公主</a></p></td>
<td></td>
<td><p>1412</p></td>
<td><p>1424</p></td>
<td><p>昭憲王后</p></td>
<td><p>無</p></td>
<td><p>早卒追贈公主。</p></td>
</tr>
<tr class="even">
<td><p>嫡二女</p></td>
<td><p><a href="../Page/貞懿公主.md" title="wikilink">貞懿公主</a></p></td>
<td></td>
<td><p>1415</p></td>
<td><p>1477</p></td>
<td><p>延昌尉安孟聃<br />
(1415年-1462年)</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>庶長女</p></td>
<td><p><a href="../Page/貞顯翁主.md" title="wikilink">貞顯翁主</a></p></td>
<td></td>
<td><p>1425</p></td>
<td><p>1480</p></td>
<td><p>尙寢宋氏</p></td>
<td><p>鈴川君尹師路<br />
(1423年-1463年)</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>翁主[48]</p></td>
<td></td>
<td><p>？</p></td>
<td><p>1429</p></td>
<td></td>
<td><p>無</p></td>
<td><p>早夭，無封號。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>翁主[49]</p></td>
<td></td>
<td><p>1430</p></td>
<td><p>1431</p></td>
<td><p>司記車氏</p></td>
<td><p>無</p></td>
<td><p>早夭，無封號。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>翁主[50]</p></td>
<td></td>
<td><p>？</p></td>
<td><p>？</p></td>
<td><p>慎嬪金氏</p></td>
<td><p>無</p></td>
<td><p>早夭，無封號。</p></td>
</tr>
<tr class="odd">
<td><p>庶二女</p></td>
<td><p><a href="../Page/貞安翁主_(朝鮮世宗).md" title="wikilink">貞安翁主</a></p></td>
<td></td>
<td><p>？</p></td>
<td><p>1461</p></td>
<td><p>淑媛李氏</p></td>
<td><p>靑城尉沈安義<br />
(1438年-1476年)</p></td>
<td><p>[51][52]</p></td>
</tr>
</tbody>
</table>

## 后世纪念

[Sejong_tomb_1.jpg](https://zh.wikipedia.org/wiki/File:Sejong_tomb_1.jpg "fig:Sejong_tomb_1.jpg")
[10000_won_serieVI_obverse.jpeg](https://zh.wikipedia.org/wiki/File:10000_won_serieVI_obverse.jpeg "fig:10000_won_serieVI_obverse.jpeg")上的世宗头像\]\]
由於世宗對韓國的影響之大，今日的韓國，有許多事物以紀念世宗大王來命名。一万[韓圓紙幣的正面图案即为世宗大王头像](../Page/韓圓.md "wikilink")\[53\]。在韓國首都[首爾](../Page/首爾.md "wikilink")，以世宗命名的設施有[世宗文化會館](../Page/世宗文化會館.md "wikilink")、[世宗路等](../Page/世宗路.md "wikilink")。

[韓國國軍的](../Page/韓國國軍.md "wikilink")[世宗大王級驅逐艦以世宗命名](../Page/世宗大王級驅逐艦.md "wikilink")。

韩国的南极科学考察站[世宗科學基地以世宗命名](../Page/世宗科學基地.md "wikilink")。

韓國從2007年起籌建的新[行政首都也以世宗之名稱為](../Page/行政首都.md "wikilink")「[世宗特別自治市](../Page/世宗特別自治市.md "wikilink")」。

國際[跆拳道聯盟共有二十四套拳法](../Page/跆拳道.md "wikilink")，其中的一個套路也為紀念他而命名為「世宗」。

## 相關影視作品

  - 世宗大王：1970年[KBS電視劇](../Page/韓國放送公社.md "wikilink")，[南一祐飾](../Page/南一祐.md "wikilink")
  - 世宗大王：1978年電影，飾
  - [朝鮮王朝五百年](../Page/朝鮮王朝五百年.md "wikilink")－：1983年[MBC電視劇](../Page/文化廣播公司.md "wikilink")，飾
  - [龍之淚](../Page/龍之淚.md "wikilink")：1996年KBS電視劇，[安在模飾](../Page/安在模.md "wikilink")
  - [王與妃](../Page/王與妃.md "wikilink")：1998年KBS電視劇，飾
  - [大王世宗](../Page/大王世宗.md "wikilink")：2008年KBS電視劇，[李玹雨](../Page/李玹雨.md "wikilink")、[金相慶飾](../Page/金相庆.md "wikilink")
  - [神機箭](../Page/神機箭_\(電影\).md "wikilink")：2008年電影，[安聖基飾](../Page/安聖基.md "wikilink")
  - [根深蒂固的樹](../Page/樹大根深.md "wikilink")：2011年[SBS電視劇](../Page/SBS_\(韓國\).md "wikilink")，[宋仲基](../Page/宋仲基.md "wikilink")、[韓石圭飾](../Page/韓石圭.md "wikilink")
  - [仁粹大妃](../Page/仁粹大妃_\(電視劇\).md "wikilink")：2011年[JTBC電視劇](../Page/JTBC.md "wikilink")，[全茂松飾](../Page/全茂松.md "wikilink")
  - [我是王](../Page/我是王.md "wikilink")：2012年電影，[朱智勳飾](../Page/朱智勳.md "wikilink")
  - [撲通撲通LOVE](../Page/撲通撲通LOVE.md "wikilink")：2015年[MBC特別劇](../Page/文化廣播_\(韓國\).md "wikilink")，[尹斗俊飾](../Page/尹斗俊.md "wikilink")
  - [六龍飛天](../Page/六龍飛天.md "wikilink")：2015年KBS電視劇，[南多凜飾](../Page/南多凜.md "wikilink")
  - [蔣英實](../Page/蔣英實_\(電視劇\).md "wikilink")：2016年KBS電視劇，[金相慶飾](../Page/金相慶.md "wikilink")

## 参考资料

## 相关条目

  - [大王世宗](../Page/大王世宗.md "wikilink")
  - [訓民正音](../Page/訓民正音.md "wikilink")

## 外部連結

  - [世宗大王紀念事業會](http://www.sejongkorea.org)

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**朝鲜世宗** |-

[Category:文字發明者](../Category/文字發明者.md "wikilink")
[Category:世宗](../Category/世宗.md "wikilink")
[3](../Category/朝鮮太宗王子.md "wikilink")
[Category:亞洲紙幣上的人物](../Category/亞洲紙幣上的人物.md "wikilink")
[Category:諡莊憲](../Category/諡莊憲.md "wikilink")
[Category:韩国之最](../Category/韩国之最.md "wikilink")
[Category:朝鮮王朝君主](../Category/朝鮮王朝君主.md "wikilink")

1.  《朝鲜实录·世宗实录》卷1：“世宗莊憲英文睿武仁聖明孝大王諱裪，字元正，太宗恭定大王第三子也。母元敬王后閔氏，以太祖六年丁丑四月壬辰，生於漢陽俊秀坊潛邸，實大明太祖高皇帝洪武三十年也。英明剛果，沈毅重厚，寬裕仁慈，恭儉孝友，出於天性。太宗八年戊子二月，封忠寧君，娶右副代言沈溫之女，封敬淑翁主。”
2.  [계해조약](http://preview.britannica.co.kr/bol/topic.asp?article_id=b01g3496a)

3.  [주화(走火) -
    문화콘텐츠닷컴](http://www.culturecontent.com/content/contentView.do?search_div=CP_THE&search_div_id=CP_THE009&cp_code=cp0208&index_id=cp02081148&content_id=cp020811480001&search_left_menu=3)
4.  \<<책한권으로 읽는 세종대왕실록>\>(Learning Sejong Silok in one book) ISBN
    89-01-07754-X
5.  Kim, Yung Sik. (1998). "Problems and Possibilities in the Study of
    the History of Korean Science," Osiris (2nd series, Volume 13,
    1998): 48–79.
6.  [장영실 蔣英實](http://www.bueb125.com.ne.kr/san311.htm)
7.  [Introduction to Sejong the
    Great](http://urimal.cs.pusan.ac.kr/edu_sys_new/new/docu/history/sejong/default.asp)
8.  Kim Jeong Su(1990), 《한글의 역사와 미래》(History and Future of Hangul) ISBN
    8930107230
9.  李朝实录1427年四月初四：又啓: "回回之徒, 衣冠殊異, 人皆視之, 以爲非我族類, 羞與爲婚。 旣爲我國人民, 宜從我國衣冠,
    不爲別異, 則自然爲婚矣。 且回回大朝會祝頌之禮, 亦宜停罷。"皆從之。
10. [世宗實錄40卷,10年（1428）6月16日記錄3](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11006016_003&tabid=w)
11. [世祖實錄8卷，3年（1457）7月10日紀錄2](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_10307010_002&tabid=w)
12. [世宗實錄64卷，16年（1434）6月3日記錄5](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11606003_005&tabid=w)
13. [世宗實錄30卷，7年（1425）11月6日紀錄2](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10711006_002&tabid=w)
14. [文宗實錄12卷，2年（1452）2月4日紀錄1](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wea_10202004_001&tabid=w)
15. 《世宗實錄》111卷，28年（1446）3月27日
    後宮兩貴人兩東室兩別室, 竝齊衰期年, 服與承徽同。 **淑容洪氏**、尙食黃氏、典贊朴氏, 竝齊衰期年, 服與承徽同
16. [尚寢宋氏墓誌銘](http://www.ionyang.com/data/newsData/1317183561&&.jpg)
17. 參考《璿源錄》、《璿源系譜記略》，早夭、無封號的王子不列入。
18. [《世宗實錄》1卷，世宗卽位年(1418年) 9月19日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10009019_001&tabid=w)
19. [《端宗實錄》8卷，端宗元年(1453年) 10月18日紀錄二](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wfa_10110018_002&tabid=w)
20. [《世宗實錄》7卷，世宗2年(1420年) 1月6日紀錄四](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10201006_004&tabid=w)
21. [《睿宗實錄》3卷，睿宗元年(1469年) 1月21日紀錄二](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wha_10101021_002&tabid=w)
22. [《世宗實錄》28卷，世宗7年(1425年) 5月2日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10705002_001&tabid=w)
23. [《世宗實錄》106卷，世宗26年(1444年) 12月7日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_12612007_001&tabid=w)
24. [《世宗實錄》29卷，世宗7年(1425年) 9月5日紀錄四](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10709005_004&tabid=w)
25. 卒年不詳，不過可以確定他至少活到65歲。
26. [《成宗實錄》228卷，成宗20年(1489年)5月9日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wia_12005009_001&tabid=w)
27. [《世宗實錄》31卷，世宗8年(1426年) 3月28日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10803028_001&tabid=w)
28. [《世祖實錄》9卷，世祖3年(1457年) 10月21日紀錄二](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_10310021_002&tabid=w)
29. [《世宗實錄》37卷，世宗9年(1427年) 8月12日紀錄五](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10908012_005&tabid=w)
30. [《世祖實錄》34卷，世祖10年(1464年) 8月16日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_11008016_001&tabid=w)
31. [《世宗實錄》38卷，世宗9年(1427年) 11月18日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_10911018_001&tabid=w)
32. [《世宗實錄》107卷，世宗27年(1445年) 1月16日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_12701016_001&tabid=w)
33. [《世祖實錄》19卷，世祖6年(1460年) 2月27日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_10602027_001&tabid=w)
34. [《世祖實錄》16卷，世祖5年(1459年) 5月29日紀錄五](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_10505029_005&tabid=w)
35. [《密城君神道碑》](http://gsm.nricp.go.kr/_third/user/search/KBD007.jsp?ksmno=3645)
36. [《成宗實錄》100卷，成宗10年(1479年) 1月1日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wia_11001001_001&tabid=w)
37. [《端宗實錄》14卷，端宗3年(1455年) 6月5日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wfa_10306005_001&tabid=w)
38. 《宗班行蹟·王子翼峴君贈諡忠成公墓表》
39. [《世祖實錄》30卷，世祖9年(1463年) 5月4日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_10905004_001&tabid=w)
40. [《世宗實錄》64卷，世宗16年(1434年) 4月15日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11604015_001&tabid=w)
41. [《世祖實錄》41卷，世祖13年(1467年) 2月2日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wga_11302002_001&tabid=w)
42. [《世宗實錄》65卷，世宗16年(1434年) 8月17日紀錄四](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11608017_004&tabid=w)
43. [《世宗實錄》67卷，世宗17年(1435年) 3月20日紀錄三](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11703020_003&tabid=w)
44. [《成宗實錄》80卷，成宗8年(1477年) 5月5日紀錄三](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wia_10805005_003&tabid=w)
45. [《世宗實錄》84卷，世宗21年(1439年) 1月8日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_12101008_001&tabid=w)
46. [《文宗實錄》1卷，文宗卽位年(1450年) 3月10日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wea_10003010_001&tabid=w)
47. 參考《璿源錄》、《璿源系譜記略》，早夭、無封號的王女不列入。
48. [《世宗實錄》43卷,世宗11年(1429年)2月21日1번째기사](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11102021_001&tabid=w)
49. [《世宗實錄》53卷，世宗13年(1431年)7月6日紀錄四](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_11307006_004&tabid=w)
50. 依據[《世宗實錄》84卷，21年（1439）1月27日紀錄2](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wda_12101027_002&tabid=w)，慎嬪育有六男二女，二女皆夭折，六男皆存，故此女為慎嬪之女兒
51. [《文宗實錄》13卷，文宗2年(1452年) 4月27日紀錄三](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wea_10204027_003&tabid=w)
52. [《端宗實錄》4卷，端宗卽位年(1452年)10月21日紀錄一](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wfa_10010021_001&tabid=w)
53. [Tourguide - Tomb of Sejong the
    Great](http://www.tourguide.co.kr/local/local_detail.htm?pCode=CULTTOMB0262)