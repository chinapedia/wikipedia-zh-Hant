[韩战结束以后的](../Page/韩战.md "wikilink")20世纪下半叶，**[韩国基督教](../Page/韩国基督教.md "wikilink")**迅速发展，信徒数量持续多年呈爆炸性的增长，但是，自1995年以后的十年间，新教信徒略有下降，而[天主教信徒增长了](../Page/天主教.md "wikilink")70%\[1\]\[2\]。目前改新教和天主教分别有信徒860万人和510万人\[3\]。

[韓國首都](../Page/韓國.md "wikilink")[首爾是全国](../Page/首爾.md "wikilink")[基督教的大本营](../Page/基督教.md "wikilink")，[教堂林立](../Page/教堂.md "wikilink")，拥有世界上最大的12个基督教堂会中的11个。夜间无数闪烁霓虹灯的[十字架是该市的突出景观](../Page/十字架.md "wikilink")。韩国天主教会也是世界上成年人信仰皈依最多的教会，每年都有超过15万以上的成人进教。

现在韩国教会还在海外进行传教活动，是世界上第2大传教士派出国（仅次于美国）。

另外，基督教的一个异端派别——[文鲜明的](../Page/文鲜明.md "wikilink")[统一教扩展也很迅速](../Page/统一教.md "wikilink")。

基督教在教育领域具有重要影响。[延世大学是韩国享有盛誉的常春藤盟校之一](../Page/延世大学.md "wikilink")。基督教对于韩国文化的影响显而易见。

许多韩国基督徒，包括[赵镛基](../Page/赵镛基.md "wikilink")，规模庞大的[汝矣岛纯福音教会牧师引起了全世界的瞩目](../Page/汝矣岛纯福音教会.md "wikilink")。1984年[教宗](../Page/教宗.md "wikilink")[若望保禄二世在](../Page/若望保禄二世.md "wikilink")[罗马封圣的韩国圣徒人数创一次性封圣的最高人数](../Page/罗马.md "wikilink")。韩国的天主教圣徒人数居世界第四位。

1945年以后，韩国教会只在南方（[大韓民國](../Page/大韓民國.md "wikilink")）有影響力。在[韩战](../Page/韩战.md "wikilink")（1950年–1953年）以前，该国三分之二的基督徒分布在北方，但大部分都逃到了南方
。统治[北韩的共產主義政權](../Page/北韩.md "wikilink")[勞動黨政府反對一切宗教](../Page/勞動黨.md "wikilink")，故北韓基督徒實際人數不明。

目前在東亞各國中，相較於[日本](../Page/日本.md "wikilink")、[中國和](../Page/中國.md "wikilink")[蒙古國境內基督教依舊屬於少數人信仰的宗教](../Page/蒙古國.md "wikilink")，在[韓國的基督教勢力就顯得非常龐大](../Page/韓國.md "wikilink")，雖然韓國依舊以佛教或儒家信仰為主，但在政治方面以基督教的影響力最大，1987年[韓國民主化後歷任的韓國總統也多信仰基督教為主的特色與](../Page/韓國民主化.md "wikilink")[台灣相似](../Page/台灣.md "wikilink")。

## 早期的挫折：1593-1784

基督教在将近200年的时间里，历经许多挫折，才於1784年植根于朝鮮半島土壤，直到20世纪，基督徒才在人口构成中占据重要地位。在起初阶段，基督教在朝鮮似乎没有什么前途，但最终被韩国社会所普遍接受。

最早来到韩国的基督徒是[耶稣会神甫Gregorious](../Page/耶稣会.md "wikilink") de
Cespedes，于在[朝鮮王朝](../Page/朝鮮王朝.md "wikilink")[宣祖四十二年](../Page/朝鮮宣祖.md "wikilink")（1593年）到达朝鮮向流亡在當地的[日本人傳教](../Page/日本.md "wikilink")，但不允许向朝鮮人传教。
10年以后，韩国使臣[李光鍾](../Page/李光鍾.md "wikilink")（이광종）从[北京带回一张世界地图和](../Page/北京.md "wikilink")[利玛窦写的几本神学书籍](../Page/利玛窦.md "wikilink")。
利玛窦著作立即激起学术界的争论；17世纪初，宫廷学者[李睟光](../Page/李睟光.md "wikilink")（이수광）和大臣[柳夢寅著书批评利玛窦著作](../Page/柳夢寅.md "wikilink")。在随后的200年间，学术界对基督教信仰的批评持续不衰。

1644年[清兵入关](../Page/清兵入关.md "wikilink")，[昭顯世子九月到十一月作为人质在北京](../Page/昭顯世子.md "wikilink")[紫禁城](../Page/紫禁城.md "wikilink")[文渊阁居住了](../Page/文渊阁.md "wikilink")70天，与耶稣会传教士[汤若望会面](../Page/汤若望.md "wikilink")，了解西方科学技术、天主教，汤若望送他[天文](../Page/天文.md "wikilink")、[数学](../Page/数学.md "wikilink")、[天主教书籍及](../Page/天主教.md "wikilink")[地球仪](../Page/地球仪.md "wikilink")、天主像等，昭顯世子不胜欣喜，打算归故国后用于宫中，甚至颁行于世。汤若望同昭显世子商量以后决定以清朝皇帝名义下赐昭显世子带回朝鲜一些前明宦官和宫女，他们都是已受洗的天主教徒。昭顯世子于1645年回国，当年5月21日暴毙昌庆宫欢庆殿，世子带来的西洋文物没有在朝鲜发挥影响，所有物品都在其死后被销毁，而信教前明宦官李邦詔、張三畏、劉仲林、谷豊登、竇文芳和前明宫女也被遣返回中国。

## 增长的基础

这一时期朝鮮王朝闭关锁国，有大批帮助天主教传教士的朝鮮信徒[殉道](../Page/殉道.md "wikilink")。其中最著名的是[金大建神父](../Page/金大建.md "wikilink")，25岁时被處斬。

### 实学派学者的同情

一些学者比较同情基督教。[实学派深受基督教的](../Page/实学派.md "wikilink")[平等价值观所吸引](../Page/平等.md "wikilink")。
實學派提倡才学比出身更重要的觀點常常受到反对，他們发现基督教可以支持他们的理论。因此[天主教最终于](../Page/天主教.md "wikilink")1784年（[正祖九年](../Page/朝鮮正祖.md "wikilink")）傳到朝鮮時已经得到了知识份子的舆论支持
- 这对天主教信仰在1790年代的的传播至关重要。 1801年研究表明55％的天主教徒出身实学派家族有关。
显然那时推动基督教增长的首要因素是同情基督教的少数士大夫。

### 平信徒领袖

基督教在朝鮮从一开始就是一个當地信徒自發的傳入，而非由外国神职人员影响。1784年，一名已经在北京南堂受洗的朝鲜使臣[李承薰在](../Page/李承薰.md "wikilink")[平壤建立了第一个天主教祈祷所](../Page/平壤.md "wikilink")。
1786年，Yi proceeded to establish a hierarchy of lay-priests.
，爾後更於1787年自行建立了天主教團體明道會，且仿造傳道書內容自行擔任[神職人員](../Page/神職人員.md "wikilink")，直到他們發現這些神職職位的任命違反天主教規範，於是在1789年時派遣望教信徒年尹持忠前往北京拜訪北京主教教湯士選，才終於在1795年派遣遣使會會士[周文謨神父入韓](../Page/周文謨_\(清朝\).md "wikilink")，而基督教是由本土平信徒传入韩国，而非外国神职人员。

### 导入朝鮮传统

第三，朝鮮教会有可能利用朝鮮传统。朝鮮的[萨满教有獨一造物主](../Page/萨满教.md "wikilink")[桓因](../Page/桓因.md "wikilink")
。根据古代传说，桓因有一个儿子名叫[桓雄](../Page/桓雄.md "wikilink")，依次类推，在前2333年生下了人类的祖先[檀君](../Page/檀君.md "wikilink")。
  檀君创建了韩国，上千年来，这个故事告诉韩国人文明的起源。  这个神话有几个版本，其中一个描述檀君由处女所生
。一些现代神学家企图根据檀君神话的三种神圣特性解释基督教的[三位一体概念](../Page/三位一体.md "wikilink")。
尽管只是一个神话，在心理上为韩国人理解基督教的[道成肉身教义作了准备](../Page/道成肉身.md "wikilink")。教会将基督教神学嫁接到既有信仰上是其增长的一个关键因素。

### 使用諺文

第四，基督徒对韓語和易学的[諺文的使用激活了信仰在精英阶层以外的传播](../Page/諺文.md "wikilink")。此后諺文得到普及，而天主教会是第一个正式承认其价值的组织。早在1780年代，用諺文出版了一部分福音书，在1790年代用諺文出版了教义书籍如[主教要旨](../Page/主教要旨.md "wikilink")（주교요지），大约1800年用諺文出版了一本天主教赞美诗集。

### 新教和现代教育机构的建立

1884年，2位美国宣教士：（[监理会](../Page/监理会.md "wikilink")）和（Horace Grant
Underwood，[长老会](../Page/长老会.md "wikilink")）将[新教传入韩国](../Page/新教.md "wikilink")。
1881年－1887年，在中国东北[奉天的](../Page/奉天.md "wikilink")[东关教会传教的](../Page/沈阳东关教会.md "wikilink")[苏格兰长老会传教士](../Page/苏格兰.md "wikilink")[罗约翰](../Page/罗约翰.md "wikilink")（John
Ross）牧师将圣经翻译成韓語。新教还建立了韩朝鮮第一个现代教育机构。长老会[培材男校成立于](../Page/培材大学.md "wikilink")1885年，监理会[梨花女校成立于](../Page/梨花女子大学.md "wikilink")1886年。此后不久类似的学校纷纷成立，推动新教在平民中迅速扩展，这一时期新教超过天主教成为基督教在韩国的主流。

### 参与韩国爱国运动

在[朝鮮日治时期](../Page/朝鮮日治时期.md "wikilink")，基督教与爱国运动的关联也是它被朝鮮人广泛接受的重要原因之一。这时期朝鮮人遭受痛苦，700万人流亡国外。1938年，甚至韓語也被宣布为非法。

1919年3月1日，33名宗教领袖和专业人士集会，通过了独立宣言（[三一运动](../Page/三一运动.md "wikilink")）。尽管是由[天道教宗教领袖组织](../Page/天道教.md "wikilink")，但是33名签字者中有15人是基督徒
-许多人后来被捕入狱。1919年还成立了著名的天主教独立运动[義民團](../Page/義民團.md "wikilink")，一名监理会教徒[李承晚成立了](../Page/李承晚.md "wikilink")[在中国的流亡政府](../Page/大韩民国临时政府.md "wikilink")。许多韩国人眼中把基督教与爱国事业联系在一起，拒绝参与當時强制推行的敬拜[日本天皇仪式](../Page/日本天皇.md "wikilink")。
 尽管这主要是出于信仰原因而非政治原因，许多基督徒由于坚持信仰而入狱，在许多韩国人眼中，带有爱国主义和抵抗日本占领的意味。

## 基督教与韩国社会

基督教曾在韩国从[封建社会向现代社会的转化中扮演重要角色](../Page/封建社会.md "wikilink")。

### 文化教育

基督教对教育的早期影响前面已经提及。通过基督教文学的传播，教会团体所建立的学校，语音学和简单易学的[諺文得到推广](../Page/諺文.md "wikilink")，使得人们识字率激增。虽然諺文早在1446年[世宗大王时期就由宫廷学者们创制](../Page/世宗大王.md "wikilink")，但在几个世纪中基本没有廣泛应用，書面上主要使用漢字。天主教会最先认识到諺文的价值，主教伯纽克斯（1886年殉难）命令向所有的教内儿童教习諺文。新教教会也把諺文读写能力作为领受圣餐的先决条件。妇女的识字率极大地提高，而传统上妇女沒有機會接受正式教育。

### 可能的经济效果

韩国宪法保证宗教自由，实行政教分离，政府提倡基督教及佛教等宗教信，将宗教作为反对[共产主义的](../Page/共产主义.md "wikilink")[意识形态](../Page/意识形态.md "wikilink")[壁垒](../Page/壁垒.md "wikilink")。根据[美国政府统计](../Page/美国政府.md "wikilink")，1995年韩国大约26%的人口是基督徒
[3](http://www.state.gov/g/drl/rls/irf/2004/35403.htm)
一个新教研究小组的1995年宗教年鉴认为比例高达40%
[4](http://www.geocities.com/~iarf/tedesco1.html)。许多韩国基督徒认为其宗教信仰是韩国过去30年间戏剧性经济增长的重要因素，相信成功和繁荣是上帝祝福的标志。统计数字的增长、庞大的组织和突出的建筑物都给人留下深刻的印象
[5](http://www.us.omf.org/content.asp?id=23595)。调查显示\[[https://web.archive.org/web/20070513094150/http://www.zogbyworldwide.com/int/readnewswire.cfm?ID=724\]韩国基督徒在宗教方面非常活跃，每星期常常在出席聚会人数的百份比方面胜过美国的基督徒（美國約有30%-40%的人年每星期出席教堂](https://web.archive.org/web/20070513094150/http://www.zogbyworldwide.com/int/readnewswire.cfm?ID=724%5D韩国基督徒在宗教方面非常活跃，每星期常常在出席聚会人数的百份比方面胜过美国的基督徒（美國約有30%-40%的人年每星期出席教堂)）。

### 社会关系

社会关系可能是受到基督教影响最大的範疇。传统韩国社会是按照[儒家学说的原则](../Page/儒家.md "wikilink")，按等级划分，并臣服于半人半神的[天子之下](../Page/天子.md "wikilink")，妇女毫无社会权利，儿女完全服从父母，个人权利必须由社会制度规定。这种社会结构受到基督教关于“神按照自己的形象造人”（创世纪1、26-27）及人具有绝对价值这一教导的根本挑战，强调私有财产权既与这一观念紧密相连。基督徒把皇帝看作仅仅是一个在上帝面前只代表他自己的个体，上帝的权威高于皇帝的权威。基督教价值观的传播也对解放妇女儿童作出了贡献。天主教会一开始（1784年）就允许妇女自由[改嫁](../Page/改嫁.md "wikilink")、禁止[同居](../Page/同居.md "wikilink")、纳[妾](../Page/妾.md "wikilink")，反对[一夫多妻制](../Page/一夫多妻制.md "wikilink")，不准虐待、遗弃妻子；教会教导父母要把儿女当作上帝赐下的礼物，应当教育他们；不准包办小孩的婚姻，也不准[重男轻女](../Page/重男轻女.md "wikilink")。可是几百年前的中世纪欧洲基督教社会，像个人权利、妇女权利、儿童权利之类比起近几个世纪同样也是百般受限。

### 民众神学与人权抗争

基督教关于个人价值的观念是在长期的争取民主、人权的努力中找到其表达方式的，近些年来，[民众神学就是这种表达方式的体现](../Page/民众神学.md "wikilink")。以“神的形象”这一概念为基础，结合韩国传统的「[恨](../Page/恨.md "wikilink")」觀念，民众神学阐述了韩国人民是自己命运的合法主人的思想，并且同时强调民族独立与人权，它在韩国社会的左派和右派中都极具吸引力，两位著名的韓國政治领袖—[金泳三](../Page/金泳三.md "wikilink")、[金大中都赞同民众神学的观点](../Page/金大中.md "wikilink")，两人都为反抗军人政府奋斗了几十年，经常被捕入狱。金泳三是[长老会教徒](../Page/长老会.md "wikilink")，金大中是[天主教徒](../Page/天主教.md "wikilink")，两人都在1988年韩国恢复民主之后先后担任总统。

民众神学的一次具体实践是在[朴正熙主政的晚期](../Page/朴正熙.md "wikilink")，发生了几次基督教徒要求增加工资和改善工作条件的示威运动，如天主教农民运动和（新教）城市产业工人运动，政府认为这些活动对社会安定是一种威胁，许多运动领导人遭到逮捕和关押。这些抗争运动正好处在那个普遍混乱的时期，1979年10月26日，朴正熙被刺杀，社会动荡发展到顶点。

## 争论

一些教會領袖因其品德等問題等而受外界批評，例如與教友發生[婚外情](../Page/婚外情.md "wikilink")。[韓國基督教總聯合會領袖](../Page/韓國基督教總聯合會.md "wikilink")（장효희）於2003年12月1日從辦公室跳樓身亡其原因據說是與一名已婚女教友發生婚外情，並被該名女教友的丈夫撞破，這宗案件引起了公眾注意。

另外在大型教會也曾出現一些以「父傳子」形式移交教會擁有權及領導權的事件，不少基督徒均不接受這種做法。

一些比較極端的[基要派基督徒曾被指強行對](../Page/基督教基要主义.md "wikilink")[佛寺](../Page/佛寺.md "wikilink")、[廟宇](../Page/廟宇.md "wikilink")、[神像等進行破壞](../Page/神像.md "wikilink")\[4\]，其中一宗案例，是一群基要派基督徒破壞了一尊[檀君像](../Page/檀君.md "wikilink")\[5\]。

## 摘要

基督教经过将近200年不断的挫折，最终立足朝鮮半島。起初只是“实学派”知识分子中间的一种世俗活动，他们把基督教作为其平等主义价值观的催化剂。经过与朝鮮文化互相融合、吸收，基督教有效的利用了其传统文化。日本40
年占领期间，众多基督徒因拒绝崇拜天皇而被关押，朝鮮人对教会的鲜明性格及态度因此得到强化、坚固。这一立场使得朝鮮教徒们从此能够称基督教为“自己的信仰”，而不再是“外国宗教”。

## 展望

南韩从40年的独裁政治到1988年转变为自由、民主制度，让韩国教会迎来了新的挑战。教会所持坚定的民主、人权立场成为吸引民众的重要部分，也推动了韩国教会在70、80年代的成倍增长。在民主化后，基督教前所未有地成了一种权力机构，许多基督徒感到教会成了得胜后的牺牲品，自由繁荣导致普遍的满足现状(complacency)，教会的“锋芒”也磨钝了。近十年来教会增长速度放慢。

然而，21世纪後许多韓國基督徒重新确立了[传福音的目标](../Page/传福音.md "wikilink")，那就是要把朝鲜半岛变成基督教在亚洲大陆的坚固堡垒，北朝鲜不可避免的在这一异像的范围之内，南韩教会正在制定应对意外事件的计划，准备为北方将来可能出现的政治、经济大崩溃提供宗教及人道主义援助。

## 参考文献

### 引用

<references />

1.  Patrick Johnstone and Jason Mandryk, [Operation
    World](../Page/Operation_World.md "wikilink"), Carlisle, Cumbria,
    and Waynesboro, GA., 2001, pp. 387–390.

2.  CHOI Suk-woo, 'Korean Catholicism Yesterday and Today', <u>Korean
    Journal</u> XXIV, 8, August 1984, p. 4.

3.  KIM Han-sik, 'The Influence of Christianity', <u>Korean Journal</u>
    XXIII, 12, December 1983, p. 5.

4.  Ibid., pp. 6–7.

5.  Ibid., p. 6.

6.  KIM Ok-hy, 'Women in the History of Catholicism in Korea', [Korean
    Journal](../Page/Korean_Journal.md "wikilink") XXIV, 8, August 1984,
    p. 30.

7.  CHOI Suk-Woo, pp. 5–6.

8.  National Unification Board, [The Identity of the Korean
    People,](../Page/The_Identity_of_the_Korean_People,.md "wikilink")
    Seoul, 1983, pp. 132–136.

9.  Seoul International Publishing House, [Focus on Korea, Korean
    History](../Page/Focus_on_Korea,_Korean_History.md "wikilink"),
    Seoul, 1983, pp. 7–8.

10. Seoul International Publishing House, [Focus on Korea, Korean
    History](../Page/Focus_on_Korea,_Korean_History.md "wikilink"),
    Seoul, 1983, pp. 7–8.

11. [The Identity of the Korean
    People](../Page/The_Identity_of_the_Korean_People.md "wikilink"),
    pp. 132–136.

12. [Ilyon](../Page/Ilyon.md "wikilink"), tr. HA Tae-hung and Grafton K.
    Minz, [Samguk Yusa](../Page/Samguk_Yusa.md "wikilink"), Seoul 1972,
    pp. 32–33.

13. [Marguerite Johnson](../Page/Marguerite_Johnson.md "wikilink"), 'The
    Culture', in [Pico Iyer](../Page/Pico_Iyer.md "wikilink") (ed.) 'An
    Ancient Nation on the Eve of a Modern Spectacle: SOUTH KOREA',
    [Time](../Page/Time_Magazine.md "wikilink") CXXXII, 10, 5 September
    1988, p. 48.

14. Ibid., p. 48.

15. <u>Focus on Korea,</u> pp. 7–8.

16. CHO Kwang, 'The Meaning of Catholicism in Korean History', <u>Korean
    Journal</u> XXIV, 8, August 1984, pp. 20–21.

17. [Colin Whittaker](../Page/Colin_Whittaker.md "wikilink"), [Korea
    Miracle](../Page/Korea_Miracle.md "wikilink"), Eastbourne, 1988,
    p. 133.

18. Andrew C. Nah, <u>A Panorama of 5000 Years: Korean History,</u>
    Seoul, 1983, p. 81.

19. Whittaker, p. 62.

20. Ibid., p. 65.

21. Ibid., p. 63.

22. CHOI Suk-woo, p. 10.

23. <u>[Encyclopedia
    Americana](../Page/Encyclopedia_Americana.md "wikilink"),</u> Vol.
    23, Danbury, Conn., 1988, p. 464.

24. CHO Kwang, p. 11.

25. Whittaker, p. 65.

26. <u>[Merit Students
    Encyclopedia](../Page/Merit_Students_Encyclopedia.md "wikilink"),</u>
    Vol. 10, New York and London, 1980, p. 440.

27. Whittaker, p. 34.

28. CHO Kwang, pp. 20–21.

29. Whittaker, p. 40.

30. KIM Ok-hy, p. 34.

31. CHO Kwang, pp. 16–18.

32. Ibid., pp. 18–19.

33. KIM Han-sik, pp. 11–12.

34. CHOI Suk-woo, p. 7.

35. CHO Kwang, pp. 16–18.

36. Ibid., pp. 18–19.

37. Ibid., pp. 16–19.

38. Michael Lee, 'Korean Churches Pursue Social and Political Justice',
    in Brian Heavy (Ed.), <u>Accent</u> III, 3 Auckland, May 1988,
    pp. 19–20.

39. [Kessing's Contemporary
    Archives](../Page/Kessing's_Contemporary_Archives.md "wikilink"),
    London, 25 April 1980, p. 30216.

40. J. Earnest Fisher, <u>Pioneers of Modern Korea,</u> Seoul, 1977,
    pp. 65–74.

### 来源

  - 书籍

<!-- end list -->

  -
  -
  -
  - *[Encyclopedia
    Americana](../Page/Encyclopedia_Americana.md "wikilink")* (1986).
    Vol. 23, Danbury, Conn.: Grolier. ISBN 978-0-7172-0117-4 (set).

  -
  -
<!-- end list -->

  - *[Merit Students
    Encyclopedia](../Page/Merit_Students_Encyclopedia.md "wikilink")*
    (1980). Vol. 10, New York: Macmillan Educational.

<!-- end list -->

  - 期刊文章

<!-- end list -->

  -
  -
  - *Focus on Korea* (1986). Vol. 2, "Korean History", Seoul: Seoul
    International Pub. House.

  - Keesing's (1979). *Keesing's Contemporary Archives* **25**:
    p. 30216. ISSN 0022-9679.

  -
  -
  -
<!-- end list -->

  - 新闻报道

<!-- end list -->

  -
## 外部链接

  - [韩国基督教广播](http://www.cbs.co.kr/)
  - [Pressure of Buddhism from Christianity in
    Korea](http://www.geocities.com/volodyatikhonov/darwinism)
  - [Profile:History of the growing fundamental Christian bodies in the
    world](http://www.buddhistnews.tv/current/vital-link-b-bodhi-N.php)
  - [Effects of Christianity on the
    Koreans](http://66.218.71.225/search/cache?p=Korea+buddhist+christian+tension&ei=UTF-8&fl=0&b=21&u=irbulletin.0catch.com/pdf/ir24.pdf&w=korea+buddhist+christian+tension&d=FFE3B6004B&icp=1&.intl=us)
  - [Questions for Buddhist and Christian Cooperation in
    Korea](http://www.geocities.com/~iarf/tedesco1.html)
  - [CIA The World Factbook - Korea,
    South](https://www.cia.gov/cia/publications/factbook/geos/ks.html)
  - [Constitution of the Republic of Korea (South
    Korea)](https://archive.is/20120710041912/http://korea.assembly.go.kr/res/low_01_read.jsp?boardid=1000000035)
  - [Confucian No
    Longer](http://www.cwnews.com/news/viewstory.cfm?recnum=27485)

## 参见

  - [朝鮮半島天主教](../Page/朝鮮半島天主教.md "wikilink")
  - [朝鲜半岛基督教新教](../Page/朝鲜半岛基督教新教.md "wikilink")
  - [韩国正教会](../Page/韩国正教会.md "wikilink")
  - [韩国基督教](../Page/韩国基督教.md "wikilink")
  - [朝鲜民主主义人民共和国基督教](../Page/朝鲜民主主义人民共和国基督教.md "wikilink")

{{-}}

[Category:东亚基督教](../Category/东亚基督教.md "wikilink")
[朝鮮半島基督教](../Category/朝鮮半島基督教.md "wikilink")
[Category:朝鲜半岛宗教](../Category/朝鲜半岛宗教.md "wikilink")

1.  [1](http://www.kosis.kr/OLAPN/Analysis/stat_OLAP.jsp?tbl_id=DT_1IN0505&org_id=101&vwcd=MT_ZTITLE&path=%C1%D6%C1%A6%BA%B0%20%3E%20%C0%CE%B1%B8%A1%A4%B0%A1%B1%B8%20%3E%20%C0%CE%B1%B8%C3%D1%C1%B6%BB%E7%20%3E%20%C0%CE%B1%B8%BA%CE%B9%AE%20%3E%20%C3%D1%C1%B6%BB%E7%C0%CE%B1%B8\(2005\)%20%3E%20%C0%FC%BC%F6%BA%CE%B9%AE&item=&keyword=&lang_mode=kor&list_id=A11I1&olapYN=)

2.  [2](http://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=102&oid=262&aid=0000000521)

3.
4.  [Research and Reflections by Frank M.
    Tedesco](http://www.buddhapia.com/eng/tedesco/index.html)

5.  [Religious peace threatened in South
    Korea](http://www.iht.com/articles/2008/10/14/asia/buddhist.php)