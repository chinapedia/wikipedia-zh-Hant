**朝鮮顯宗**（；）是[朝鲜王朝的第](../Page/朝鲜王朝.md "wikilink")18代君主，1659年至1674年在位。諱**李棩**（），字**景直**。[孝宗李淏與](../Page/朝鮮孝宗.md "wikilink")[仁宣王后張氏的嫡長子](../Page/仁宣王后.md "wikilink")。

## 生平

朝鮮仁祖十九年二月四日，[鳳林大君李淏之妻](../Page/朝鮮孝宗.md "wikilink")[豐安府夫人張氏於](../Page/仁宣王后.md "wikilink")[瀋陽質館生下長子李棩](../Page/瀋陽.md "wikilink")。隨著[昭顯世子的死亡](../Page/昭顯世子.md "wikilink")，李淏被仁祖冊立為世子，李棩亦被封為世孫。

仁祖死後，李淏繼位，即孝宗，並封李棩為世子。孝宗二年（1651年），世子李棩與大臣金佑明之女行嘉禮。孝宗死後，李棩於昌徳宮仁政殿即位。顯宗十五年八月十八日昇遐於昌德宮齋廬，葬於[杨州](../Page/杨州.md "wikilink")[崇陵](../Page/崇陵.md "wikilink")。上諡號**純文肅武敬仁彰孝**，廟號為**顯宗**，清朝賜諡**莊恪**，英祖四十八年加上尊號**昭休衍慶敦德綏成**。

## 與東寧鄭氏關係

朝鮮顯宗对待清朝并不像其[父亲孝宗那般敌视](../Page/父亲.md "wikilink")，也与其子孙崇明反清思想不同。1667年[农历五月初十日](../Page/农历.md "wikilink")，[台湾](../Page/台湾.md "wikilink")[鄭氏王朝](../Page/鄭氏王朝.md "wikilink")4艘货船从台湾起程，前往[日本贸易](../Page/日本.md "wikilink")，以及请求日本出兵帮助台湾反攻大陆。途中遇风，1艘货船于五月二十三日漂到朝鲜，二十五日在[济州岛大静县猊来里浦浦口东边歧头处](../Page/济州岛.md "wikilink")，被当地政府发现。船上95人，自称[明朝官商](../Page/明朝.md "wikilink")，引起朝鲜兴趣，随即他们被带往[汉城](../Page/汉城.md "wikilink")。如何处置该船及人员在朝廷上引发争执，很多朝鲜[士大夫听说](../Page/士大夫.md "wikilink")“明朝还存在”，异常激动。但由于怕得罪[清朝](../Page/清朝.md "wikilink")，朝鲜显宗最终还是力排众议，决定把他们送交清朝。结果清朝将95人全部杀害。此后朝鲜对台湾明鄭漂来船舶改变办法，不准其登陆，就地遣返，以避免再次发生此类悲剧。132年后，[朝鲜正祖专门设祭坛](../Page/朝鲜正祖.md "wikilink")，哀悼95人。\[1\]

## 家庭

### 王后

<table>
<thead>
<tr class="header">
<th><p>稱號</p></th>
<th><p>生卒年</p></th>
<th><p>本貫</p></th>
<th><p>父母</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/明聖王后.md" title="wikilink">明聖王后金氏</a></p></td>
<td><p>1642年－1683年</p></td>
<td><p>清風</p></td>
<td><p>清風府院君<a href="../Page/金佑明.md" title="wikilink">金佑明</a><br />
德恩府夫人恩津宋氏</p></td>
<td><p>　又稱「顯烈王大妃」。</p></td>
</tr>
</tbody>
</table>

### 子女

<table>
<thead>
<tr class="header">
<th><p>稱號</p></th>
<th><p>名</p></th>
<th><p>生卒年</p></th>
<th><p>生母</p></th>
<th><p>配偶</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>公主</p></td>
<td></td>
<td><p>1658年[2][3]</p></td>
<td><p>明聖王后</p></td>
<td><p>無</p></td>
<td><p>　早卒，無封號。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/明善公主.md" title="wikilink">明善公主</a></p></td>
<td></td>
<td><p>1659年[4]－1673年</p></td>
<td><p>明聖王后</p></td>
<td><p>無</p></td>
<td><p>　顯宗十四年八月初二日逝世。[5]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朝鮮肅宗.md" title="wikilink">肅宗大王</a></p></td>
<td><p>焞</p></td>
<td><p>1661年－1720年</p></td>
<td><p>明聖王后</p></td>
<td><p><a href="../Page/仁敬王后.md" title="wikilink">仁敬王后光山金氏</a><br />
<a href="../Page/仁顯王后.md" title="wikilink">仁顯王后驪興閔氏</a><br />
<a href="../Page/仁元王后.md" title="wikilink">仁元王后慶州金氏</a></p></td>
<td><p>　朝鮮第19代君主。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/明惠公主.md" title="wikilink">明惠公主</a></p></td>
<td></td>
<td><p>1662年－1673年</p></td>
<td><p>明聖王后</p></td>
<td><p>無</p></td>
<td><p>　顯宗三年十二月四日誕生，[6]顯宗十四年四月二十七日逝世。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/明安公主.md" title="wikilink">明安公主</a></p></td>
<td><p>溫姬[7]</p></td>
<td><p>1665年－1687年</p></td>
<td><p>明聖王后</p></td>
<td><p>海昌尉<a href="../Page/吳泰周.md" title="wikilink">吳泰周</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 附註

## 參考資料

  - [朝鮮王朝實錄](http://sillok.history.go.kr/main/main.jsp)
  - [《承政院日記》](http://sjw.history.go.kr/main/main.jsp)
  - 《明安公主墓誌銘》

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**朝鲜顯宗** |-

[Y棩](../Category/李姓.md "wikilink")
[Category:朝鮮孝宗王子](../Category/朝鮮孝宗王子.md "wikilink")
[Category:朝鮮王朝世孫](../Category/朝鮮王朝世孫.md "wikilink")
[Category:諡莊恪](../Category/諡莊恪.md "wikilink")
[Category:朝鮮王朝君主](../Category/朝鮮王朝君主.md "wikilink")

1.  孙卫国 从丁未漂流人事件看朝鲜王朝之尊明貶清文化心态
2.  《承政院日記》孝宗9年（1658年）4月28日「藥房都提調左議政臣元斗杓，提調判尹臣尹絳，副提調行都承旨臣趙啓遠啓曰，世子嬪解娩平安，不勝喜幸之至。……」
3.  《承政院日記》孝宗9年（1658年）6年3日「政院啓曰，郡主阿只氏喪事，禮曹郞廳及歸厚署官員，依例治喪事，分付之意,
    敢啓。傳曰，自內爲之矣，勿爲分付。」
4.  《承政院日記》顯宗即位年（1659年）11月15日「大妃殿，藥房都提調臣鄭太和口傳啓曰，今此中外想望之際，中宮殿誕生公主，臣民之缺然，有不可言，而閭閻間産婦，或有以男女之輕重，致傷心慮，仍成疾病者，侍側宮人，今若以缺望之意，有所煩說，則不無仍此致傷之患。伏願嚴飭侍側之人，毋露缺然之色，每陳寬譬之言，以爲保護之地，不勝幸甚。答曰，啓辭知道。豈無缺然之心哉？恐有如此之患，故每爲寬慰矣。」
5.  雪峯遺稿卷之二十一·城南錄·明善公主挽
6.  《胎峯謄錄》庚戌正月十六日……觀象監官員，以領事提調意啓曰，次知內官，招致地官韓必雄分付內，壬寅年十二月初四日丑時生明惠公主阿只氏藏胎事，傳敎矣。……
7.  [顯宗御筆](http://www.emuseum.go.kr/relic.do?action=view_d&mcwebmno=9755)