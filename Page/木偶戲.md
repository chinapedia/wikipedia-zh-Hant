[Puppettheatre.JPG](https://zh.wikipedia.org/wiki/File:Puppettheatre.JPG "fig:Puppettheatre.JPG")
[Puppet_Bleeckie_sliding.jpg](https://zh.wikipedia.org/wiki/File:Puppet_Bleeckie_sliding.jpg "fig:Puppet_Bleeckie_sliding.jpg")上出現。\]\]
**木偶戲**，又叫**木偶劇**、**傀儡戲**、**人偶戲**，是一種不以真人演出，而以操控傀儡的方式演出的戲劇，有許多種類。

## 歷史

### 亞洲

根據出土文物與歷史資料顯示，傀儡戲是[中國歷史上最早出現具有表演功能的戲劇](../Page/中國歷史.md "wikilink")。據說古代[葬禮中用來](../Page/葬禮.md "wikilink")[殉葬的](../Page/殉葬.md "wikilink")「[俑](../Page/俑.md "wikilink")」，與傀儡戲有直接而深遠的關係。《[通典](../Page/通典.md "wikilink")》則云：“《窟礌子》作偶人以戲，善歌舞，本喪家樂也。漢末始用之于嘉會。”《樂府雜錄》以為起于漢高祖被[冒顿圍於平城](../Page/冒顿.md "wikilink")，平城正西是冒顿的阏氏把守，謀臣[陳平知阏氏爱忌妒](../Page/陳平.md "wikilink")，製造了會動的美人木偶來解圍\[1\]。唐代[謝觀為此作了](../Page/謝觀.md "wikilink")《漢以木女解平城圍賦》。[杜佑](../Page/杜佑.md "wikilink")《笔麈》中的记载：“傀儡子，汉末使用于嘉会，北齐高纬尤好之”，“今俗悬丝而戏，谓之偶人，以手持其末，出其帏帐之上(外)。”“郭郎”与“鲍老”是傀儡戏史中对傀儡的两个称谓\[2\]\[3\]。《后山诗话》载宋[楊億](../Page/楊億.md "wikilink")《傀儡诗》：“鲍老当筵笑郭郎，笑他舞袖太郎当，若教鲍老当筵舞，转觉郎当舞袖长。”\[4\]
《都城纪胜·瓦舍众伎》中提到了4种傀儡：“弄悬丝傀儡（起于陈平六奇解围）、杖头傀儡、水傀儡、肉傀儡（以小儿后生辈为之）。凡傀儡敷演烟粉灵怪故事、铁骑公案之类，其话本或如杂剧，或如崖词，大抵多虚少实，如巨灵神朱姬大仙之类是也。”\[5\]

在亞洲不少地區，傀儡戲原本最重要的功能是驅除邪煞，這種[習俗一直延續下來](../Page/習俗.md "wikilink")。到現在驅除邪煞變成民間傀儡戲主要的功能之一。這種特殊的[社會功能](../Page/社會功能.md "wikilink")，使傀儡戲具有神秘的宗教色彩，這也是傀儡戲跟其他種類的戲劇非常不同的地方。

## 種類

  - [中國大陸](../Page/中國大陸.md "wikilink"):[皮影戲](../Page/皮影戲.md "wikilink")、[懸絲傀儡](../Page/懸絲傀儡.md "wikilink")、[水傀儡](../Page/水傀儡.md "wikilink")、[布袋戲](../Page/布袋戲.md "wikilink")、[药发傀儡](../Page/药发傀儡.md "wikilink")、杖頭傀儡等
  - [臺灣](../Page/臺灣.md "wikilink"):[皮影戲](../Page/皮影戲.md "wikilink")、[布袋戲](../Page/布袋戲.md "wikilink")、[懸絲傀儡](../Page/懸絲傀儡.md "wikilink")
  - [越南](../Page/越南.md "wikilink"):[水傀儡](../Page/水傀儡.md "wikilink")（[越南文](../Page/越南文.md "wikilink")：）
  - [日本](../Page/日本.md "wikilink"):[文樂](../Page/文樂.md "wikilink")（[日語](../Page/日語.md "wikilink")：，[平假名](../Page/平假名.md "wikilink")：）又稱[人形淨琉璃](../Page/人形淨琉璃.md "wikilink")（日語：，平假名：）
  - [朝鮮半島](../Page/朝鮮半島.md "wikilink"):
  - [泰國](../Page/泰國.md "wikilink"):Hun Krabok
  - [越南](../Page/越南.md "wikilink"):[水上木偶戲](../Page/水上木偶戲.md "wikilink")
  - [爪哇](../Page/爪哇.md "wikilink"):wayang
    kulit（[皮影戲的一種](../Page/皮影戲.md "wikilink")）

## 特攝

木偶戲被攝制後，用於電視、電影放映，一般被歸入[特攝](../Page/特攝.md "wikilink")。

## 注釋

## 外部連結

  - [Puppetry Art Center of Taipei
    台北偶戲館](http://www.pact.org.tw/index.aspx)
  - [台灣的傀儡戲 -
    政治大學圖書館](http://nccuir.lib.nccu.edu.tw/bitstream/140.119/35525/9/15150409.pdf)
  - [傀儡戲 -
    中華百科全書](http://ap6.pccu.edu.tw/Encyclopedia_media/main-art.asp?id=8091)
  - [公益财团法人东洋文库
    中国木偶戏关联照片数据库](http://124.33.215.236/cnkosyomokugugidb/cnkosyomokugugi_srch.php?lang=c)

[傀儡戲](../Page/category:傀儡戲.md "wikilink")

[zh-min-nan:Ka-lé-hì](../Page/zh-min-nan:Ka-lé-hì.md "wikilink")

[M](../Category/中国非物质文化遗产.md "wikilink")
[表演藝術](../Category/中華民國無形文化資產傳統藝術類.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.  [段安节撰](../Page/段安节.md "wikilink")《乐府杂录·傀儡子》载：“自昔传云：‘起于汉祖在平城为冒顿所围，其城一面，即冒顿妻阏氏，兵强于三面。垒中绝食，陈平访知阏氏妒忌，即造木偶人，运于机关，舞于郫间。阏氏望，谓是生人，虑其城下，冒顿必纳妓女，遂退军。’……后乐家翻为戏。”
2.  《顏氏家訓·書證》篇：“或問：‘俗名傀儡子為郭禿，有故實乎？’答曰：‘《風俗通》云：諸郭皆諱禿，當是前世有姓郭而病禿者，滑稽調戲，故后人為其像，呼為郭禿。’”《乐府杂录》“傀儡子”条载：“其引歌舞有郭郎者，发正秃，善优笑，闾里呼为郭郎，凡戏场必在俳儿之首也。”
3.  [徐嘉瑞](../Page/徐嘉瑞.md "wikilink")《金元戏曲方言考补遗》：“鲍老，滑稽脚色。曲牌有《鲍老催》、《耍鲍老》。农村演《大头和尚戏柳翠》，大头和尚臃肿郎当即鲍老也……
    昆明 之语谓臃肿郎当之人曰鲍老。”
4.  《渔隐丛话》前集卷五五
5.  《都城纪胜·瓦舍众伎》