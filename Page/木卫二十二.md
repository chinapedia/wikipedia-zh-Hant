**木卫二十二**又稱為「哈爾帕呂刻」(S/2000 J 5,
Harpalyke，[希腊语](../Page/希腊语.md "wikilink")：)，是环绕[木星运行的一颗](../Page/木星.md "wikilink")[逆行](../Page/逆行.md "wikilink")[不規則卫星](../Page/不規則卫星.md "wikilink")。在2000年[斯科特·謝帕德領導的天文小組於](../Page/斯科特·謝帕德.md "wikilink")[夏威夷大學發現](../Page/夏威夷大學.md "wikilink")，並暫時給他一個叫做**S/2000
J 5**的[臨時編號](../Page/臨時編號.md "wikilink")，\[1\]\[2\]
直到2003年才以[希臘神話中的女神](../Page/希臘神話.md "wikilink")[哈爾帕呂刻](../Page/哈爾帕呂刻.md "wikilink")（Harpalyke）命名，而祂是[克呂墨諾斯](../Page/克呂墨諾斯.md "wikilink")（Klymenos）的女兒。\[3\]
木卫二十二屬於[阿南刻群](../Page/阿南刻群.md "wikilink")，是一種傾斜角介於145.7°到154.8°，而且距離木星1930000[公里到](../Page/公里.md "wikilink")2270000公里的[木星衛星群](../Page/木星的衛星.md "wikilink")。木卫二十二距離木星21,105,000公里，[半徑](../Page/半徑.md "wikilink")2.2公里，平均[密度約](../Page/密度.md "wikilink")2.6[g](../Page/g.md "wikilink")/[cm](../Page/cm.md "wikilink")<sup>3</sup>，[公轉木星一圈需要](../Page/公轉.md "wikilink")623.34[地球日](../Page/地球日.md "wikilink")，[離心率為](../Page/離心率.md "wikilink")0.2259，[軌道傾角](../Page/軌道傾角.md "wikilink")148.644°，[反照率](../Page/反照率.md "wikilink")4%，[光度約](../Page/光度.md "wikilink")22.2。\[4\]

## 参见

  - [木星的卫星](../Page/木星的卫星.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[M22](../Category/木星的卫星.md "wikilink")
[Category:阿南刻衛星群](../Category/阿南刻衛星群.md "wikilink")

1.  [IAUC 7555: *Satellites of
    Jupiter*](http://cfa-www.harvard.edu/iauc/07500/07555.html)
    2001-01-05 (discovery)
2.  [MPEC 2001-A28: *S/2000 J 2, S/2000 J 3, S/2000 J 4, S/2000 J 5,
    S/2000 J 6*](http://cfa-www.harvard.edu/mpec/K01/K01A28.html)
    2001-01-05 (discovery and ephemeris)Jupiter 2002-10-22 (naming the
    moon)
3.  [IAUC 7998: *Satellites of
    Jupiter*](http://cfa-www.harvard.edu/iauc/07900/07998.html)
    2002-10-22 (naming the moon)
4.