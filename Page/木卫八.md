|                                                                                    |
| :--------------------------------------------------------------------------------: |
| [Pasiphaé.jpg](https://zh.wikipedia.org/wiki/File:Pasiphaé.jpg "fig:Pasiphaé.jpg") |
|                                         发现                                         |
|                                        发现者                                         |
|                                        发现日                                         |
|                                        轨道特性                                        |
|                         轨道平均[半径](../Page/半径.md "wikilink")                         |
|                          [離心率](../Page/離心率.md "wikilink")                          |
|                                        近木点                                         |
|                                        远木点                                         |
|                         [轨道周期](../Page/轨道周期.md "wikilink")                         |
|                                       平均公转速度                                       |
|                        轨道与[黄道交角](../Page/黄道.md "wikilink")                         |
|         轨道与[木星](../Page/木星.md "wikilink")[赤道交角](../Page/赤道.md "wikilink")          |
|                                         行星                                         |
|                                        物理特性                                        |
|                          平均[半径](../Page/半径.md "wikilink")                          |
|                                        表面面积                                        |
|                           [体积](../Page/体积.md "wikilink")                           |
|                           [质量](../Page/质量.md "wikilink")                           |
|                          平均[比重](../Page/比重.md "wikilink")                          |
|                                      表面重力加速度                                       |
|                                        逃离速度                                        |
|                                        反光率                                         |
|                          表面[温度](../Page/温度.md "wikilink")                          |
|                          [大气压](../Page/大气压.md "wikilink")                          |

**木卫八**

**木卫八**又稱為「[帕西法爾](../Page/帕西法爾.md "wikilink")」(Pasiphae)，是环绕[木星运行的一颗](../Page/木星.md "wikilink")[卫星](../Page/卫星.md "wikilink")。

## 概述

木衛八[帕西法爾](../Page/帕西法爾.md "wikilink")，直徑68公里，是一顆[逆行的](../Page/逆行.md "wikilink")[不規則衛星](../Page/不規則衛星.md "wikilink")，他是在1908年被[菲利伯特·梅洛特發現](../Page/菲利伯特·雅克·梅洛特.md "wikilink")，由希臘神話中的[帕西法爾命名](../Page/帕西法爾.md "wikilink")。
木衛八直到1975年才有正式的名稱（[帕西法爾](../Page/帕西法爾.md "wikilink")），在那之前，人們只叫它**木星VIII**。

## 軌道

[TheIrregulars_JUPITER_retro.svg](https://zh.wikipedia.org/wiki/File:TheIrregulars_JUPITER_retro.svg "fig:TheIrregulars_JUPITER_retro.svg")
木衛八屬於帕西法爾群，因此不但它的[離心率很高而且軌道也極為的傾斜](../Page/離心率.md "wikilink")，[帕西法爾群離木星距離](../Page/帕西法爾群.md "wikilink")22.8到24.1百萬[公里不等](../Page/公里.md "wikilink")，而它們偏離黃道面144.5°到158.3°之間。

## 参见

  - [木星的卫星](../Page/木星的卫星.md "wikilink")

[M08](../Category/木星的卫星.md "wikilink")
[Category:帕西法爾衛星群](../Category/帕西法爾衛星群.md "wikilink")