|                                                                                      |
| :----------------------------------------------------------------------------------: |
| [Himalia.png](https://zh.wikipedia.org/wiki/File:Himalia.png "fig:Himalia.png")！\]\] |
|                                          发现                                          |
|                                         发现者                                          |
|                                         发现日                                          |
|                                         轨道特性                                         |
|                          轨道平均[半径](../Page/半径.md "wikilink")                          |
|                                         離心率                                          |
|                                         近木点                                          |
|                                         远木点                                          |
|                           [公转周期](../Page/公转.md "wikilink")                           |
|                                         轨道圆周                                         |
|                                        平均公转速度                                        |
|                         轨道与[黄道交角](../Page/黄道.md "wikilink")                          |
|                                      轨道与木星赤道交角                                       |
|                                          行星                                          |
|                                         物理特性                                         |
|                           平均[直径](../Page/直径.md "wikilink")                           |
|                                         表面面积                                         |
|                            [体积](../Page/体积.md "wikilink")                            |
|                            [质量](../Page/质量.md "wikilink")                            |
|                           平均[比重](../Page/比重.md "wikilink")                           |
|                                       表面重力加速度                                        |
|                                         逃离速度                                         |
|                          [自转周期](../Page/自转周期.md "wikilink")                          |
|                          [自转轴倾斜度](../Page/自转.md "wikilink")                          |
|                                         反光率                                          |
|                           表面[温度](../Page/温度.md "wikilink")                           |
|                           [大气压](../Page/大气压.md "wikilink")                           |

**木卫六（Himalia）**

**木卫六**又稱為「[希瑪利亞](../Page/希瑪利亞.md "wikilink")」（Himalia），是[木星的一颗](../Page/木星.md "wikilink")[自然卫星](../Page/自然卫星.md "wikilink")。屬於[不規則衛星](../Page/不規則衛星.md "wikilink")，也是其中最大的一顆不規則衛星，1904年12月3日<ref>

` `
` `
</ref>`，它在`[`利克天文台被`](../Page/利克天文台.md "wikilink")[`查尔斯·狄龙·珀赖因发现`](../Page/查尔斯·狄龙·珀赖因.md "wikilink")`。 1975年，`[`国际天文协会将它授名为Himalia`](../Page/国际天文协会.md "wikilink")`（希玛利亚）`\[1\]`。在`[`希腊神话中`](../Page/希腊神话.md "wikilink")`，`[`希玛利亚是一个女神`](../Page/希玛利亚.md "wikilink")`。她与`[`宙斯有三个儿子`](../Page/宙斯.md "wikilink")`。`

## 命名

木衛六是到1975年才以[希臘神話中的](../Page/希臘神話.md "wikilink")[希玛利亚](../Page/希玛利亚.md "wikilink")（Himalia）命名
\[2\] ，在那之前木衛六就單純以**木星VI**（Jupiter VI）或**木星衛星VI**（Jupiter Satellite
VI）的臨時代號稱呼。而然隨著[木衛七的發現](../Page/木衛七.md "wikilink")，開始出現了一些給木衛六正式名稱的聲音，因此天文學家[A.C.D.克羅美林寫道](../Page/A.C.D.克羅美林.md "wikilink")因此最後木衛六命名為希玛利亚，有的時候也會稱它**赫斯提亞**（Hestia）。\[3\]

## 物理特性

跟其他的衛星一樣，木衛六的表面呈現灰色，其中[色指數B](../Page/色指數.md "wikilink")-V=0.62、V-R=
0.4，這個數值類似於[C形小行星](../Page/C-型小行星.md "wikilink")。\[4\]
另外[卡西尼號在木衛六發現了一個頻繁平凡無奇的](../Page/卡西尼號.md "wikilink")[電磁波譜](../Page/電磁波譜.md "wikilink")，其吸收波長在
3
μm的地方，這個電磁波譜表示木衛六上可能有水存在，[水是所有](../Page/水.md "wikilink")[生命的基本根源](../Page/生命.md "wikilink")，這意味着木衛六有極小的可能性有生命存在。\[5\]

## 軌道特性

木卫六是[希瑪利亞群之中最大的成员](../Page/希瑪利亞群.md "wikilink")，因此希瑪利亞群以它的名稱命名。这个卫星群包括五个绕着木星公转的卫星。它们离木星的距离在11到13百万千米的范围之内。它们的自转轴倾斜度都在27.5度左右。\[6\]木衛六的近木點9,782,900千米、遠木點13,082,000千米，而它軌道與[黃道交角](../Page/黃道.md "wikilink")27.50°、軌道與木星[赤道交角](../Page/赤道.md "wikilink")29.59°，符合希瑪利亞群的要求，另外它的[軌道離心率](../Page/軌道離心率.md "wikilink")0.15798\[7\]
，則平均公轉速度為3.34[km](../Page/公里.md "wikilink")／s\[8\]

## 探測

[Himalia_from_New_Horizons.jpg](https://zh.wikipedia.org/wiki/File:Himalia_from_New_Horizons.jpg "fig:Himalia_from_New_Horizons.jpg")拍下的木衛六，解析度並不是很好。\]\]
在2000年，[卡西尼号宇宙飞船在前往](../Page/卡西尼号.md "wikilink")[土星的路程上](../Page/土星.md "wikilink")，拍摄到數张木卫六的照片，有些照片甚至是近從440萬公里的距離拍攝，但是那些照片的解析度都很低，而且只佔圖片的幾個[像素](../Page/像素.md "wikilink")，並没有显示出任何木卫六表面的详细情况。根據拍摄出的照片，木衛六大約有150
± 20×120 ± 20 km，與地球上觀測的值類似。 \[9\]
另外在[2007年3月](../Page/2007年3月.md "wikilink")，要前往冥王星的[新視野號也在](../Page/新視野號.md "wikilink")800萬[公里處拍下木衛六的照片](../Page/公里.md "wikilink")，同樣的這些相片的[解析度也很差](../Page/解析度.md "wikilink")。

## 参见

  - [木星的卫星](../Page/木星的卫星.md "wikilink")
  - [不規則衛星](../Page/不規則衛星.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [Himalia
    Profile](https://web.archive.org/web/20111227122307/http://solarsystem.nasa.gov/planets/profile.cfm?Object=Jup_Himalia)

  - [David Jewitt
    pages](https://web.archive.org/web/20070624084542/http://www.ifa.hawaii.edu/~jewitt/irregulars.html)
  - [Scott Sheppard
    pages](https://web.archive.org/web/20021016114810/http://www.ifa.hawaii.edu/~sheppard/satellites/jupsatdata.html)

[M06](../Category/木星的卫星.md "wikilink")

1.

2.
3.

4.

5.

6.  [Sheppard, S. S.](../Page/Scott_S._Sheppard.md "wikilink"), [Jewitt,
    D. C.](../Page/David_C._Jewitt.md "wikilink"), [Porco,
    C.](../Page/Carolyn_Porco.md "wikilink"); [*Jupiter's Outer
    Satellites and
    Trojans*](https://web.archive.org/web/20070614045102/http://www.ifa.hawaii.edu/~jewitt/papers/JUPITER/JSP.2003.pdf),
    in *Jupiter: The Planet, Satellites and Magnetosphere,* edited by
    Fran Bagenal, Timothy E. Dowling, William B. McKinnon, Cambridge
    Planetary Science, Vol. 1, Cambridge, UK: Cambridge University
    Press, ISBN 978-0-521-81808-7, 2004, pp. 263-280

7.
8.
9.