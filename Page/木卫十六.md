|                                                                     |
| :-----------------------------------------------------------------: |
| [centre](https://zh.wikipedia.org/wiki/File:Metis.JPG "fig:centre") |
|                                 發現                                  |
|                                 發現者                                 |
|                                發現日期                                 |
|                  [軌道根數](../Page/軌道.md "wikilink")                   |
|                  平均[半徑](../Page/半徑.md "wikilink")                   |
|                  [離心率](../Page/離心率.md "wikilink")                   |
|                                 近木點                                 |
|                                 遠木點                                 |
|                                公轉週期                                 |
|                  公轉[速度](../Page/速度.md "wikilink")                   |
|                 [軌道傾科角](../Page/軌道傾角.md "wikilink")                 |
|                                所屬行星                                 |
|                                物理性質                                 |
|                                 大小                                  |
|                   [體積](../Page/體積.md "wikilink")                    |
|                   [質量](../Page/質量.md "wikilink")                    |
|                平均[密度](../Page/密度.md "wikilink")（估計）                 |
|                  表面[重力](../Page/重力.md "wikilink")                   |
|                 [逃逸速度](../Page/宇宙速度.md "wikilink")                  |
|                                自轉週期                                 |
|                  [反照率](../Page/反照率.md "wikilink")                   |
|                  表面[溫度](../Page/溫度.md "wikilink")                   |
|                                                                     |

'''木衛十六

**木卫十六**（Metis），是环绕[木星运行的一颗](../Page/木星.md "wikilink")[卫星之一](../Page/卫星.md "wikilink")，它是在1979年被[航海家一號發現](../Page/航海家一號.md "wikilink")。一開始，它只有一個叫做**S/1979
J
3**的[臨時編號](../Page/臨時編號.md "wikilink")，是直到1983年它才以[希臘神話中的女神](../Page/希臘神話.md "wikilink")[墨提斯命名](../Page/墨提斯.md "wikilink")。

## 軌道

木衛十六屬於[木星的](../Page/木星.md "wikilink")[内圈衛星群](../Page/木星的衛星.md "wikilink")，是第四小也是最接近木星的[衛星](../Page/衛星.md "wikilink")，它離木星只有
128,000
[公里](../Page/公里.md "wikilink")（1.79個[木星](../Page/木星.md "wikilink")[半徑](../Page/半徑.md "wikilink")），這是在[木星環的](../Page/木星環.md "wikilink")[主環以內](../Page/木星環#主環.md "wikilink")，而且它的[離心率和](../Page/離心率.md "wikilink")[軌道傾角常非小](../Page/軌道傾角.md "wikilink")，離心率大約只有0.0002，而軌道傾角也只有0.06°。木衛十六在[木星的](../Page/木星.md "wikilink")[同步軌道以內](../Page/同步軌道.md "wikilink")（[木衛十五也是](../Page/木衛十五.md "wikilink")），所以它會受到木星的[潮汐力影響而慢慢的縮小軌道](../Page/潮汐力.md "wikilink")，這導致有一天它會墜入木星。

## 物理性質

木衛十六非常不規則，大小是60×40×34
km，木衛十六的[質量和](../Page/質量.md "wikilink")[密度並不是很確定](../Page/密度.md "wikilink")，但根據同類型的[衛星估計](../Page/衛星.md "wikilink")，它的密度大約0.86g/cm<sup>3</sup>（跟[木衛五類似](../Page/木衛五.md "wikilink")），則它的質量大約是7kg。木衛十六表面有許多坑洞，而木卫十六多半呈現暗紅色的。木卫十六[公轉與自轉同步](../Page/同轉衛星.md "wikilink")，也就是說它的一面永遠朝向[木星](../Page/木星.md "wikilink")。這跟[木星的](../Page/木星.md "wikilink")[内圈衛星群類似](../Page/木星的衛星.md "wikilink")。它的背對[木星的一面比面對](../Page/木星.md "wikilink")[木星的一面的亮度多](../Page/木星.md "wikilink")1.3倍，可能是因為面跟背對[木星的一面比較容易會發生](../Page/木星.md "wikilink")[小行星撞擊](../Page/小行星.md "wikilink")，而被撞擊時會噴出較亮的[物質](../Page/物質.md "wikilink")（[冰等](../Page/冰.md "wikilink")），因此比面對[木星的那一面會比較亮](../Page/木星.md "wikilink")。

## 與木星環的關係

木衛十六是在木星環[主環的](../Page/木星環#主環.md "wikilink")1000[公里以內](../Page/公里.md "wikilink")，而且它的軌道就在一個500[公里寬的](../Page/公里.md "wikilink")[木星環縫隙](../Page/木星環.md "wikilink")。最近研究發現，[木星環絕大部分的成分都是由木衛十六或](../Page/木星環.md "wikilink")[木星的](../Page/木星.md "wikilink")[内圈衛星群來的](../Page/木星的衛星.md "wikilink")，這可能是因為[小行星撞擊木星衛星](../Page/小行星.md "wikilink")，而較小的木星衛星[引力不大不容易收回](../Page/引力.md "wikilink")，則剩下的物質就環繞木星形成[木星環](../Page/木星環.md "wikilink")。

## 参见

  - [木星的卫星](../Page/木星的卫星.md "wikilink")

[M16](../Category/木星的卫星.md "wikilink")