**木崎站**（）是一個位於[群馬縣](../Page/群馬縣.md "wikilink")[太田市新田木崎町](../Page/太田市.md "wikilink")，屬於[東武鐵道](../Page/東武鐵道.md "wikilink")[伊勢崎線的](../Page/伊勢崎線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。車站編號是**TI
20**。

另外，此站分岐的「[德川河岸線](../Page/#德川河岸線.md "wikilink")」亦會記述。

## 歷史

  - 1910年（[明治](../Page/明治.md "wikilink")43年）3月27日－開業。
  - 1984年（[昭和](../Page/昭和.md "wikilink")59年）2月1日－貨運業務廢止。
  - 2006年（[平成](../Page/平成.md "wikilink")18年）3月18日－太田～伊勢崎間的[單人駕駛服務開始](../Page/單人駕駛.md "wikilink")。

## 車站結構

[島式月台](../Page/島式月台.md "wikilink")1面2線的地面車站。

### 月台配置

| 月台 | 路線                                                                                                                                                           | 方向                                  | 目的地                                       |
| -- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------- | ----------------------------------------- |
| 1  | [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg") 伊勢崎線 | 上行                                  | [太田方向](../Page/太田站_\(群馬縣\).md "wikilink") |
| 2  | 下行                                                                                                                                                           | [伊勢崎方向](../Page/伊勢崎站.md "wikilink") |                                           |

## 使用情況

2016年度1日平均上下車人次為2,520人\[1\]。近年1日平均上下車人次的推移如下表｡

| 年度           | 1日平均上下車人次\[2\] |
| ------------ | -------------- |
| 2003年（平成15年） | 2,262          |
| 2004年（平成16年） | 2,261          |
| 2005年（平成17年） | 2,279          |
| 2006年（平成18年） | 2,251          |
| 2007年（平成19年） | 2,343          |
| 2008年（平成20年） | 2,462          |
| 2009年（平成21年） | 2,431          |
| 2010年（平成22年） | 2,449          |
| 2011年（平成23年） | 2,447          |
| 2012年（平成24年） | 2,484          |
| 2013年（平成25年） | 2,556          |
| 2014年（平成26年） | 2,473          |
| 2015年（平成27年） | 2,513          |
| 2016年（平成28年） | 2,520          |

## 相鄰車站

  - [Tōbu_Tetsudō_Logo.svg](https://zh.wikipedia.org/wiki/File:Tōbu_Tetsudō_Logo.svg "fig:Tōbu_Tetsudō_Logo.svg")
    東武鐵道
    [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    伊勢崎線
      - 特急「」停車站
    <!-- end list -->
      -

        普通

          -
            [細谷](../Page/細谷站_\(群馬縣\).md "wikilink")（TI-19）－**木崎（TI-20）**－[世良田](../Page/世良田站.md "wikilink")（TI-21）

### 過去存在的路線

  - 東武鐵道
    德川河岸線
      -

          -
            **木崎**－（貨）

## 參考資料

## 延伸閱讀

  -
  - 東武鉄道社史編纂室編集『東武鉄道百年史』東武鉄道、1998年。

## 外部連結

  - [東武鐵道 木崎站](http://www.tobu.co.jp/station/info/1802.html)

[Zaki](../Category/日本鐵路車站_Ki.md "wikilink")
[Category:群馬縣鐵路車站](../Category/群馬縣鐵路車站.md "wikilink")
[Category:伊勢崎線車站](../Category/伊勢崎線車站.md "wikilink")
[Category:太田市](../Category/太田市.md "wikilink")
[Category:1910年啟用的鐵路車站](../Category/1910年啟用的鐵路車站.md "wikilink")

1.
2.