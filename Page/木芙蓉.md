**木芙蓉**（[学名](../Page/学名.md "wikilink")：**），通常被称作**芙蓉**，是一种原产于[中国的植物](../Page/中国.md "wikilink")。

## 形态

木芙蓉为落叶[灌木或小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，高1\~3m。枝条较密并生有星状毛。叶为互生，呈阔卵圆形或圆卵形，掌状3\~5浅裂，先端尖或渐尖，两面有星状绒毛。木芙蓉花朵较大，单生于枝端叶腋，有红、粉红、白等色，與山芙蓉有单瓣或重瓣之分（木芙蓉為重瓣），花期为8\~10月。蒴果扁球形、10\~11月成熟。
[Hibiscus_mutabilis,_changing_colors.jpg](https://zh.wikipedia.org/wiki/File:Hibiscus_mutabilis,_changing_colors.jpg "fig:Hibiscus_mutabilis,_changing_colors.jpg")

## 分布

原产中国，在[长江流域及其以南都有栽培](../Page/长江.md "wikilink")，在[湖南](../Page/湖南.md "wikilink")、[四川两省较为常见](../Page/四川.md "wikilink")。湖南境内因为广植木芙蓉，湖南因此被称为“芙蓉国”。木芙蓉在[成都一带也十分常见](../Page/成都.md "wikilink")，因此成都又称“蓉城”，而木芙蓉也是成都市的[市花](../Page/市花.md "wikilink")。

## 品种

  - [白芙蓉](../Page/白芙蓉.md "wikilink")
  - [粉芙蓉](../Page/粉芙蓉.md "wikilink")
  - [红芙蓉](../Page/红芙蓉.md "wikilink")
  - [黄芙蓉](../Page/黄芙蓉.md "wikilink")
  - [醉芙蓉](../Page/醉芙蓉.md "wikilink")：又名“三醉芙蓉”，花色一日三变：清晨为白色，中午为桃红色，傍晚为深红色。为稀有的名贵品种

## 参考文献

## 外部链接

  -
  - [木芙蓉
    Mufurong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00590)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[Category:花卉](../Category/花卉.md "wikilink")
[mutabilis](../Category/木槿属.md "wikilink")
[mutabilis](../Category/美丽芙蓉组.md "wikilink")