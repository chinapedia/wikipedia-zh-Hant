**木蓝**（[学名](../Page/学名.md "wikilink")：），亦作**槐藍**、**馬棘**\[1\]，[豆科植物](../Page/豆科.md "wikilink")。

## 概述

木蓝是向陽性植物，耐旱耐濕，極易繁殖，羽状复叶，小叶9－13枚，葉片呈羽狀偏小，成熟葉片乾燥後成灰藍色，蝶形花冠，花為淺紅色居多，总状花序，线状圆柱形[荚果](../Page/荚果.md "wikilink")，一年可二至三獲，以沉澱法製作[靛藍](../Page/靛藍.md "wikilink")[染料](../Page/染料.md "wikilink")。木蓝是[中國傳統服飾所使用的染料](../Page/中國.md "wikilink")。[大成藍所使用的顏色](../Page/大成藍.md "wikilink")；木蓝又供药用，有泻肝散郁火、凉血解毒作用。

## 分布

木蓝多生長在平地跟河边，在[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶廣泛生長](../Page/亞熱帶.md "wikilink")，分布於[印度](../Page/印度.md "wikilink")、中國南方的[廣東](../Page/廣東.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[雲南及](../Page/雲南.md "wikilink")[台灣](../Page/台灣.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、中南美洲的[墨西哥](../Page/墨西哥.md "wikilink")、[瓜地馬拉](../Page/瓜地馬拉.md "wikilink")、[祕魯與](../Page/祕魯.md "wikilink")[非洲的](../Page/非洲.md "wikilink")[奈及利亞等地](../Page/奈及利亞.md "wikilink")。

在中國，它在[廣東中部的](../Page/廣東.md "wikilink")[四邑地區亦有生長](../Page/四邑.md "wikilink")，也被認為是藍植物中藍色素成份含量最多，品種最優秀的。

## 參看

  - [染料](../Page/染料.md "wikilink")
  - [藍染植物](../Page/藍染植物.md "wikilink")

## 參考

<references />

## 外部連結

  - [植物通：木藍屬](https://web.archive.org/web/20070312072554/http://www.plant.ac.cn/latin/Leguminosae/Indigofera.htm)
  - [木藍
    Mulan](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00565)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [靛玉紅
    Indirubin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00020)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[灌](../Category/藥用植物.md "wikilink")
[Category:豆科](../Category/豆科.md "wikilink")
[Category:天然染料](../Category/天然染料.md "wikilink")
[Category:經濟作物](../Category/經濟作物.md "wikilink")

1.  《[辭海](../Page/辭海.md "wikilink")》(1947年版)，**馬棘**條，p.1500/3