[Mokugyo.jpg](https://zh.wikipedia.org/wiki/File:Mokugyo.jpg "fig:Mokugyo.jpg")

**木魚**是一種木製的[敲擊樂器](../Page/敲擊樂器.md "wikilink")，形狀通常為類近[拳頭狀的圓球體](../Page/拳頭.md "wikilink")，中間部份為空心，以作為聲音[共振和](../Page/共振.md "wikilink")[擴音之用](../Page/擴音.md "wikilink")。圓球體一邊留有音孔，作用和[小提琴的F型孔相同](../Page/小提琴.md "wikilink")，都是讓經擴音後的聲音可以傳開；另一邊則另加上長而扁的外邊，以方便用[手或固定在支撐架上](../Page/手.md "wikilink")。演奏方法以利用鼓棍或小木鎚，敲打樂器外側的共鳴區以發出聲響。

木魚的尺寸可有不同大小，體積越大音高越低。

## 用途

### 宗教方面

[moktak.jpg](https://zh.wikipedia.org/wiki/File:moktak.jpg "fig:moktak.jpg")
[Chapin_Mill_Mokugyu_Drum.JPG](https://zh.wikipedia.org/wiki/File:Chapin_Mill_Mokugyu_Drum.JPG "fig:Chapin_Mill_Mokugyu_Drum.JPG")
由於木魚在[佛教禮儀中是其中一樣重要的樂器及](../Page/佛教.md "wikilink")[法器](../Page/法器.md "wikilink")，在不少[亞洲](../Page/亞洲.md "wikilink")[國家的寺廟中](../Page/國家.md "wikilink")，都很容易找到體積較大的木魚，用於較隆重的儀式唸經等場合，另外較小體積的，則常於一般早晚或晚課時使用。在廟宇中使用的木魚，會以厚軟墊置妥，便能保持木魚的共振。

### 中國或亞洲音樂

木魚在[國樂團的敲擊樂器中亦都佔有相當重要的位置](../Page/國樂團.md "wikilink")，現時較常使用的是一套5個不同大小的木魚，以支撐架將木魚固定好，並按木魚的大小和高低音來排列；近代亦有生產商嘗試製造如西樂中的[鐃鈸](../Page/鐃鈸_\(西樂\).md "wikilink")，研製出一套有固定音高的木魚群，但並不流行。

### 西洋音樂

[Modern_Templeblocks.jpg](https://zh.wikipedia.org/wiki/File:Modern_Templeblocks.jpg "fig:Modern_Templeblocks.jpg")
木魚於[二十世紀亦引入在西方音樂中](../Page/二十世紀.md "wikilink")，不過它名稱在不同國家上有很大的差異，初期分別有稱為“Wooden
Block(s)”、“Temple Block(s)”、“Chinese Wooden
Block(s)”等不同的叫法，後來慢慢地形成了共識，“Wooden
Block(s)”泛指方型小木箱式設計，而木魚則稱為“Temple Block(s)”、“Chinese Temple
Block(s)”或“Chinese Wooden
Block(s)”，\[1\]但後者則除了個別[英國作曲家](../Page/英國.md "wikilink")（如[布烈頓](../Page/布烈頓.md "wikilink")）外，近代已經甚少再使用這個名字。

## 注釋

[Category:東亞打擊樂器](../Category/東亞打擊樂器.md "wikilink")
[Category:佛教音樂](../Category/佛教音樂.md "wikilink")
[Category:打擊音樂](../Category/打擊音樂.md "wikilink")

1.  Blades, J. (1992). *Percussion Instruments and Their History*. p.
    115, pp.394-395.