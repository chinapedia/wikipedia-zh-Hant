《**未央歌**》是[华裔](../Page/华裔.md "wikilink")[作家](../Page/作家.md "wikilink")[鹿桥的著名小说](../Page/鹿桥.md "wikilink")，1945年完成，直到1967年在[台湾由](../Page/台湾.md "wikilink")[商務印書館发行](../Page/商務印書館.md "wikilink")\[1\]，立刻引起轰动，争相购阅，新文学的代表作品之一。

这部作品以[抗战时代](../Page/抗战.md "wikilink")[雲南](../Page/雲南.md "wikilink")[昆明](../Page/昆明.md "wikilink")[西南联大为背景](../Page/西南联大.md "wikilink")，描写了年轻学生的生活和理想的故事。书中人物个性鲜活，情节感人，风格清新、活泼，情调自信乐观。以积极的人生态度和真挚情感，谱成了一个真、善、美的永恒篇章。不以重大的题材，不以曲折的情节来吸引读者，而是以乐观的情调和亲和的哲理来感染读者，是《未央歌》重要的艺术成就，也是它的鲜明特色。台湾歌手[黄舒骏曾以这部作品的故事題材写过同名歌曲](../Page/黄舒骏.md "wikilink")。

## 主要人物

**藺燕梅**，就讀外語系，出生富有家庭卻能平等待人，個性天真、單純，擅長舞蹈、歌唱。缺點是不太謙虛。曾在全校面前扮演修女，歌唱《玫瑰三願》，在校園極具人氣。

**童孝賢**，就讀生物系，誠懇活潑，很喜歡動物，在不知不覺中獲得了藺燕梅的青睞。

**伍寶笙**，就讀生物系，溫文爾雅，冷靜、有腦筋，以姊姊的姿態守護藺燕梅。

**余孟勤**，就讀哲學系，正直剛毅，表現天行健的精神。是眾人中公認最適合藺燕梅的人選。

除四大主角外，書中還描繪通達敏慧的**史宣文**，勇敢坦蕩的**淩希慧**，羸弱美麗的**喬倩垠**，刻苦向學的**朱石樵**，淡泊名利的**宴取中**，有明智的**馮新銜**，有平和的沈家姐妹(**沈蒹**、**沈葭**)，有隨和的梁家姑娘(**梁崇榕**、**梁崇槐**)，乃至聰敏佻躂的**范寬怡**，優渥自信的**范寬湖**，庸俗自卑的**宋捷軍**，還有淵博的陸先生，曠達的金先生，幽然的顧先生和風趣的顧太太，慈祥而不失童心的女生舍監趙先生，以及社會上的眾多各色善良的人物，構成一個戰亂當頭、異域相聚、患難與共的友愛世界。

## 參考文獻

## 外部链接

  - [台灣商務印書館](https://web.archive.org/web/20091201194720/http://www.cptw.com.tw/BookDetail.aspx?BOKNO=55100020)
  - [《未央歌》預覽](http://books.google.com.tw/books?id=fsT2LboS3xoC&rview=1&source=gbs_navlinks_s)
    Google圖書
  - [未央歌（歌曲）](https://web.archive.org/web/20070603193134/http://www.joyaudio.idv.tw/forum/topic.asp?TOPIC_ID=6704&whichpage=2)

[Category:中国小说](../Category/中国小说.md "wikilink")
[Category:国立西南联合大学](../Category/国立西南联合大学.md "wikilink")

1.