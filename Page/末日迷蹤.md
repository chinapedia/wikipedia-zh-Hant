**《末日迷蹤》〈Left Behind〉**是末世小說系列中的小說，由黎曦庭（Tim LaHaye）及曾健時（Jerry B.
Jenkins）所著。這是整個系列中的第一本書，2014年改编成电影《[末日迷蹤](../Page/末日迷蹤_\(2014年電影\).md "wikilink")》。

## 情節介紹

## 情節摘要

## 主要人物

  - 史雷(Rayford Steele) – 主要人物，航空公司的飛機師
  - 史雪(Chloe Steele/Chloe Steele Williams)– 史雷的女兒
  - 「波哥」黃金麟(Cameron "Buck" Williams) – 新聞記者和羅錢的好友
  - 白保誠(Bruce Barnes) – 新希望教會的助理牧師
  - 史愛蓮(Irene Steele ) – 史雷被提的妻子
  - Joshua Todd-Cothran – international financier, head of London Stock
    Exchange
  - Jonathan Stonagal – international financier
  - Ken Ritz\]\]
  - 羅錢　–　著名植物學家
  - 龐士(Steve Plank) – 黃金麟的上司
  - 杜虹虹(Hattie Durham) – 史雷的舊情人
  - 賈龍（Nicolae Jetty Carpathia）
  - 史偉 – 史雷被提的兒子

## 主要主題

## 外部連結

  - [官方網站](http://www.leftbehind.com)

[da:Left Behind](../Page/da:Left_Behind.md "wikilink") [de:Left
Behind](../Page/de:Left_Behind.md "wikilink") [en:Left
Behind](../Page/en:Left_Behind.md "wikilink") [fr:Left
Behind](../Page/fr:Left_Behind.md "wikilink") [nl:De laatste
bazuin](../Page/nl:De_laatste_bazuin.md "wikilink") [nn:Left
Behind](../Page/nn:Left_Behind.md "wikilink") [pt:Left
Behind](../Page/pt:Left_Behind.md "wikilink") [sv:Lämnad
kvar](../Page/sv:Lämnad_kvar.md "wikilink")

[Category:反乌托邦小说](../Category/反乌托邦小说.md "wikilink")
[Category:末世小說系列](../Category/末世小說系列.md "wikilink")
[Category:宗教題材作品](../Category/宗教題材作品.md "wikilink")
[Category:1995年美國小說](../Category/1995年美國小說.md "wikilink")