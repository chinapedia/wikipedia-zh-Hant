**-{zh:班氏試劑;zh-hans:本尼迪克特试剂;zh-hk:本納德試劑;zh-tw:本氏液;}-**（），也称-{zh:**本氏液**、**本尼迪克試劑**、**本尼迪克試液**、**本納德試劑**或**本尼迪特試劑**;zh-hans:**班氏试剂**、**本氏液**、**本尼迪克试液**、**本纳德试剂**或**本尼迪特试剂**;zh-hk:**班氏試劑**、**本氏液**、**本尼迪克試液**、**本尼迪克特試劑**或**本尼迪特試劑**;zh-tw:**班氏試劑**、**本尼迪克試劑**、**本尼迪克試液**、**本納德試劑**或**本尼迪特試劑**;}-，是一种浅蓝色[化学](../Page/化学.md "wikilink")[试剂](../Page/试剂.md "wikilink")。其命名来自于一位美国化学家[斯坦利·本尼迪克](../Page/斯坦利·本尼迪克.md "wikilink")\[1\]。

班氏试剂主要用来检测[还原性糖](../Page/还原糖.md "wikilink")，包括除[蔗糖之外基本上所有的](../Page/蔗糖.md "wikilink")[单糖和](../Page/单糖.md "wikilink")[双糖](../Page/双糖.md "wikilink")。班氏试剂由[碳酸钠](../Page/碳酸钠.md "wikilink")、[柠檬酸钠和](../Page/柠檬酸钠.md "wikilink")[硫酸铜配制而成](../Page/硫酸铜.md "wikilink")\[2\]。

## 鉴定

首先，将需测试的样本溶解于水中，加入少量的班氏试剂，摇均后将此混合物在沸水中加热。[反应时间约为](../Page/反应速率.md "wikilink")3分钟。如果测试样本是还原糖，混合物中会形成砖红色的沉淀物。这是因为还原糖会将硫酸铜中的二价铜离子(Cu<sup>2+</sup>)还原成一价铜离子(Cu<sup>+</sup>)，并以[氧化亚铜](../Page/氧化亚铜.md "wikilink")(Cu<sub>2</sub>O)的形式沉淀出来。

如果溶液中还原糖含量较低，产生的氧化亚铜便会相应减少，因此试验后可能只会出现[绿色](../Page/绿色.md "wikilink")、混浊的[黄色或](../Page/黄色.md "wikilink")[橙色沉淀物](../Page/橙色.md "wikilink")。

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:铜化合物](../Category/铜化合物.md "wikilink")
[Category:化学鉴定](../Category/化学鉴定.md "wikilink")
[Category:配位化合物](../Category/配位化合物.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")
[试](../Category/糖化学.md "wikilink")
[Category:人名试剂](../Category/人名试剂.md "wikilink")
[Category:分析试剂](../Category/分析试剂.md "wikilink")

1.
2.