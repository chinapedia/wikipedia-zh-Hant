[Upwelling_image1.jpg](https://zh.wikipedia.org/wiki/File:Upwelling_image1.jpg "fig:Upwelling_image1.jpg")
**本格拉寒流**是[南大西洋东海岸沿](../Page/南大西洋.md "wikilink")[南非和](../Page/南非.md "wikilink")[纳米比亚西海岸从南向北流动的寒冷的水流](../Page/纳米比亚.md "wikilink")，最终汇入[南赤道暖流](../Page/南赤道暖流.md "wikilink")。

## 范围

本格拉寒流构成[南大西洋亚热带洋流的东部](../Page/南大西洋.md "wikilink")。水源包括印度洋和南大西洋亚热带[温跃层海水](../Page/温跃层.md "wikilink")；高盐分、低含氧的热带大西洋海水；和寒冷、较淡的深海水。本格拉寒流宽约2-300千米，向北流动时还进一步变宽。其西部界限不甚清晰，有许多季节性临时漩流。

## 名稱

本格拉寒流(The Benguela Current)又名本吉拉寒流，早期又叫那米比寒流。

## 影响

本格拉寒流与温暖的、向南流的[阿古拉斯洋流混合](../Page/阿古拉斯洋流.md "wikilink")，使得[好望角附近拥有丰富的海洋生态系统](../Page/好望角.md "wikilink")，也造成气流不稳。

本格拉寒流使得[纳米比亚海岸形成沙漠](../Page/纳米比亚.md "wikilink")，降水极为稀少，并使得纳米比亚的骷髅海岸（Skeleton
Coast）经常浓雾不散，而且更令該區全年氣溫都徘徊在十至二十攝氏度之間。本格拉[厄尔尼诺现象已被发现](../Page/厄尔尼诺.md "wikilink")，不过强度和频繁程度都逊于[太平洋](../Page/太平洋.md "wikilink")。

## 外部链接

  - [本格拉寒流](http://oceancurrents.rsmas.miami.edu/atlantic/benguela.html)

[Category:洋流](../Category/洋流.md "wikilink")
[Category:大西洋](../Category/大西洋.md "wikilink")