[Railways_in_Angola.svg](https://zh.wikipedia.org/wiki/File:Railways_in_Angola.svg "fig:Railways_in_Angola.svg")
**本格拉鐵路**（）是由本格拉鐵路公司（經營的、連接位於[安哥拉](../Page/安哥拉.md "wikilink")[大西洋岸的](../Page/大西洋.md "wikilink")[洛比托和](../Page/洛比托.md "wikilink")[贊比亞及](../Page/贊比亞.md "wikilink")[剛果民主共和國](../Page/剛果民主共和國.md "wikilink")[加丹加的鐵路](../Page/加丹加.md "wikilink")。

經過贊比亞可以到達[印度洋岸的](../Page/印度洋.md "wikilink")[莫桑比克](../Page/莫桑比克.md "wikilink")[貝拉和](../Page/貝拉.md "wikilink")[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")[達累斯薩拉姆](../Page/達累斯薩拉姆.md "wikilink")，因此是[非洲橫貫鐵路的一部分](../Page/非洲橫貫鐵路.md "wikilink")。另外又可以間接連接[南非](../Page/南非.md "wikilink")。

## 沿革

1899年，[葡萄牙政府開始興建深入內陸](../Page/葡萄牙.md "wikilink")、最終到達[比屬剛果的豐富礦藏](../Page/比屬剛果.md "wikilink")。1902年，工程由白人殖民者[赛西尔·罗兹的朋友羅伯特](../Page/赛西尔·罗兹.md "wikilink")·威廉士爵士（Sir
Robert Williams）接手並在1929年完成了到達剛果邊境的路段。鐵路十分成功並帶來了豐厚的利潤。原設計時速30公里。\[1\]

安哥拉獨立後，由於內戰的關係，鐵路停駛，基礎設施受破壞。2005年安贊兩國開始討論恢復兩地鐵路運輸的工作。2007年7月开工，由[中铁建](../Page/中鐵建.md "wikilink")20局等承建，全长1344公里，轨距1067毫米，设67座车站。2011年8月29日，[洛比托至](../Page/洛比托.md "wikilink")[万博开通客运](../Page/万博.md "wikilink")。2012年3月31日，向东延伸202公里开通客运至[奎托](../Page/奎托.md "wikilink")。2012年5月25日，通车至[卢埃纳](../Page/卢埃纳.md "wikilink")。2013年8月通车至刚果边界。经7年施工于2014年8月12日全线竣工。2015年2月14日正式通车\[2\]。重建工程改善了100多處的彎度，把設計時速提升至90公里。\[3\]

## 参看

  - [坦贊鐵路](../Page/坦贊鐵路.md "wikilink")

## 参考资料

## 外部链接

  - [早期歷史](http://mikes.railhistory.railfan.net/r011.html)

[category:剛果民主共和國鐵路](../Page/category:剛果民主共和國鐵路.md "wikilink")

[Category:安哥拉鐵路](../Category/安哥拉鐵路.md "wikilink")

1.

2.  [中国在海外修建最长铁路通车
    横贯安哥拉全境](http://news.163.com/15/0214/23/AIEV6F7F00014JB6.html)

3.