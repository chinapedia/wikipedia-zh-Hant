**本生**（[梵语](../Page/梵语.md "wikilink")：，Jātaka），音译作**阇多伽**、**阇陀**，又稱**佛說本生經**、**生經**、**本生譚**、**本生故事**，是記錄佛陀還未成佛，即因地為菩薩時的前世故事。是[九分教和](../Page/九分教.md "wikilink")[十二分教之一](../Page/十二分教.md "wikilink")。

## 概論

佛陀前世故事稱作本生，佛陀的弟子的前世故事則稱作[本事](../Page/本事.md "wikilink")。如《[成实论](../Page/成实论.md "wikilink")》卷一说：「阇陀伽者，因现在事说过去事。」《[佛地經論](../Page/佛地經論.md "wikilink")》：「先世相應所有餘事。名為本事。先世所受生類差別名為本生。」\[1\]

南傳《本生經》有22篇、547則；依形式、內容等異同而類形化，大體在435則上下的數目。其中，相當於漢譯和梵文者有160餘則，巴利語獨特者有270餘則\[2\]。有些故事散見於各類[巴利文經典中](../Page/巴利文.md "wikilink")。

在漢地[敦煌石窟中刻有大量的本生故事](../Page/敦煌石窟.md "wikilink")，如：毗楞竭梨王身钉千钉、九色鹿拯救溺人、月光王施头。

## 修波羅蜜最後一世

在[上座部佛教](../Page/上座部佛教.md "wikilink")，[本緣部之中的最後一部經](../Page/大正新脩大藏經#.E6.9C.AC.E7.B7.A3.E9.83.A8.md "wikilink")，名為《[毘输安呾啰王子本生史經](../Page/毘输安呾啰王子本生史經.md "wikilink")》的經典，經中的毘输安呾啰王子即是[世尊修](../Page/世尊.md "wikilink")[波羅蜜最後一世](../Page/波羅蜜.md "wikilink")。

根據因緣譚記載:

此處的**[毘输安呾啰](../Page/毘输安呾啰.md "wikilink")**王子，即是[北傳佛教的](../Page/北傳佛教.md "wikilink")**[須大拏](../Page/須達拿太子.md "wikilink")**王子。《[佛學大辭典](../Page/佛學大辭典.md "wikilink")》-{云}-:「太子名。舊云蘇達拏太子是也。寄歸傳四曰：東印度月官大士作毘輸安呾囉太子歌，詞人皆舞詠遍五天矣，舊云蘇達拏太子者是也。」

## 參考條目

  - [佛所行讚](../Page/佛所行讚.md "wikilink")

## 參考文獻

## 外部連結

  - 东山健吾：〈[敦煌石窟本生故事画的形式——以睒子本生图为中心](http://www.nssd.org/articles/article_read.aspx?id=37716713)〉。
  - [從對讀觀點看佛典中的「本生經」](http://www.fuyan.org.tw/download/FBS_vol7-3.pdf)
  - [《南傳大藏經·佛本生》初探](http://www.chinabuddhism.com.cn/a/fxyj/1992/199201f06.htm)
  - 本生經，悟醒譯：[漢譯南傳大藏經](http://tripitaka.cbeta.org/N)（臺灣，高雄，元亨寺；[CBETA](../Page/CBETA.md "wikilink")）／經藏／小部
    ／[N31](http://tripitaka.cbeta.org/N31) \~
    [N42](http://tripitaka.cbeta.org/N42)（第31冊至第42冊）

[Category:本緣部](../Category/本緣部.md "wikilink")
[Category:小部](../Category/小部.md "wikilink")
[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:巴利經藏](../Category/巴利經藏.md "wikilink")

1.
2.