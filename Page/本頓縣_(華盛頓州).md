**本頓縣**（[英語](../Page/英語.md "wikilink")：**Benton
County**）是[美國](../Page/美國.md "wikilink")[華盛頓州南部的一個縣](../Page/華盛頓州.md "wikilink")，北、東、南為[哥倫比亞河所圍繞](../Page/哥倫比亞河.md "wikilink")，南鄰[俄勒岡州](../Page/俄勒岡州.md "wikilink")。面積4,559平方公里。根據[美國2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口175,177人。\[1\]
縣治[普羅瑟](../Page/普羅瑟_\(華盛頓州\).md "wikilink")，最大城市[肯纳威克](../Page/肯纳威克_\(华盛顿州\).md "wikilink")。

於1905年3月8日由[雅基馬縣分出](../Page/雅基馬縣.md "wikilink")。縣名紀念代表密蘇里州的[聯邦參議員](../Page/美國參議院.md "wikilink")[湯瑪斯·哈特·本頓](../Page/湯瑪斯·哈特·本頓.md "wikilink")（Thomas
Hart Benton）。

## 地理

根據[美国人口调查局](../Page/美国人口调查局.md "wikilink")，本郡的總面積為，其中陸地面積占、水域面積占
（3.24%）。

### 地理特色

  - [哥倫比亞河](../Page/哥倫比亞河.md "wikilink")
  - [雅基馬河](../Page/雅基馬河.md "wikilink")

### 主要公路

  - [I-82.svg](https://zh.wikipedia.org/wiki/File:I-82.svg "fig:I-82.svg")
    [82號州際公路](../Page/82號州際公路.md "wikilink")
  - [I-182.svg](https://zh.wikipedia.org/wiki/File:I-182.svg "fig:I-182.svg")
    [182號州際公路](../Page/182號州際公路.md "wikilink")
  - [US_12.svg](https://zh.wikipedia.org/wiki/File:US_12.svg "fig:US_12.svg")
    [美國國道12](../Page/美國國道12.md "wikilink")
  - [US_395.svg](https://zh.wikipedia.org/wiki/File:US_395.svg "fig:US_395.svg")
    [美國國道95](../Page/美國國道95.md "wikilink")
  - [WA-14.svg](https://zh.wikipedia.org/wiki/File:WA-14.svg "fig:WA-14.svg")
    [14號華盛頓州州道](../Page/14號華盛頓州州道.md "wikilink")
  - [WA-240.svg](https://zh.wikipedia.org/wiki/File:WA-240.svg "fig:WA-240.svg")
    [240號華盛頓州州道](../Page/240號華盛頓州州道.md "wikilink")

### 毗鄰郡

  - 東－[瓦拉瓦拉縣](../Page/瓦拉瓦拉縣_\(華盛頓州\).md "wikilink")
  - 東北 - [富蘭克林縣](../Page/富蘭克林縣_\(華盛頓州\).md "wikilink")
  - 北－[格蘭特縣](../Page/格蘭特縣_\(華盛頓州\).md "wikilink")
  - 西－[雅基馬縣](../Page/雅基馬縣.md "wikilink")
  - 西／西南－[克里基塔特縣](../Page/克里基塔特縣.md "wikilink")
  - 南／西南－[莫羅縣 (俄勒岡州)](../Page/莫羅縣_\(俄勒岡州\).md "wikilink")
  - 南／東南－[尤馬蒂拉縣 (俄勒岡州)](../Page/尤馬蒂拉縣_\(俄勒岡州\).md "wikilink")

### 國家保護區

  - [漢福德流域國家紀念區](../Page/漢福德流域國家紀念區.md "wikilink")（Hanford Reach
    National Monument）（部分）
  - [沙鐸山國家野生動物保護區](../Page/沙鐸山國家野生動物保護區.md "wikilink")（Saddle Mountain
    National Wildlife Refuge）（部分）
  - [尤馬蒂拉國家野生動物保護區](../Page/尤馬蒂拉國家野生動物保護區.md "wikilink")（Umatilla
    National Wildlife Refuge）（部分）

## 資料來源

## 延伸閱讀

  - William Denison Lyman, *History of the Yakima Valley, Washington:
    Comprising Yakima, Kittitas, and Benton Counties.* In Two Volumes.
    Chicago: S.J. Clarke Publishing Co., 1919.
    [Volume 1](http://archive.org/details/historyofyakimav01lyma) |
    [Volume 2](http://archive.org/details/historyofyakimav02lyma)

## 外部連結

  - [本頓縣官方網站](http://www.co.benton.wa.us/)

  - [東本頓縣歷史博物館](http://www.owt.com/ebchs/)

[B](../Category/华盛顿州行政区划.md "wikilink")

1.