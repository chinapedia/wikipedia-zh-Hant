**札幌村**（）是過去位於[北海道](../Page/北海道.md "wikilink")[札幌支廳北部的一個](../Page/札幌支廳.md "wikilink")[村落](../Page/村落.md "wikilink")，已於1955年3月1日[併入](../Page/市町村合併.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")，過去的轄區相當於現在的札幌市[東區的範圍](../Page/東區_\(札幌市\).md "wikilink")。

## 歷史

  - 1902年4月1日：札幌村、雁來村、苗穗村、丘珠村合併為札幌村，並成為北海道二級村。\[1\]
  - 1910年4月1日：部分轄區被併入札幌市。
  - 1923年4月1日：札幌村成為北海道一級村。
  - 1934年4月1日：部分轄區被併入札幌市。
  - 1950年4月1日：部分轄區被併入札幌市。
  - 1955年3月1日：琴似町、札幌村、篠路村被併入札幌市。

[SapporoMuraKyodoKinenkan2005-3.jpg](https://zh.wikipedia.org/wiki/File:SapporoMuraKyodoKinenkan2005-3.jpg "fig:SapporoMuraKyodoKinenkan2005-3.jpg")

## 參考資料

<div class="references-small">

<references />

[Category:東區 (札幌市)](../Category/東區_\(札幌市\).md "wikilink")
[Category:石狩管內](../Category/石狩管內.md "wikilink")

1.