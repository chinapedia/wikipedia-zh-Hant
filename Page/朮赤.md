**赤**（，1181年－1225年）是[成吉思汗的长子](../Page/成吉思汗.md "wikilink")，他的母亲是[孛儿帖](../Page/孛儿帖.md "wikilink")。

## 生平

[鐵木真與](../Page/鐵木真.md "wikilink")[孛兒帖結婚時](../Page/孛兒帖.md "wikilink")，三姓[蔑兒乞部的首領](../Page/蔑兒乞.md "wikilink")[脫黑脫阿](../Page/脫黑脫阿.md "wikilink")，為報其弟赤列都的未婚妻[訶額侖當年被鐵木真的父親](../Page/訶額侖.md "wikilink")[也速該搶親之辱](../Page/也速該.md "wikilink")，突襲了鐵木真的營帳。在混戰中，鐵木真逃進了不兒罕山（今[肯特山](../Page/肯特山.md "wikilink")），他的妻子和異母卻當了脫黑脫阿的俘虜。等救回时孛兒帖已有身孕，朮赤可能是蔑儿乞人的孩子，而「朮赤」二字是「客人」的意思。因此，从他的出生开始就不断有人怀疑朮赤的血统，尤其是二弟[察合臺常因此與其衝突](../Page/察合臺.md "wikilink")。术赤的后代y染色体测出有
C-F1756，与鐵木真第六子[阔列坚的后代相同](../Page/阔列坚.md "wikilink")，但[内蒙的](../Page/内蒙.md "wikilink")[黄金家族后代测出有O](../Page/黄金家族.md "wikilink")-F444，外蒙的[达延汗后代测出有](../Page/达延汗.md "wikilink")
C-M407。

朮赤多次随他父亲或奉他父亲的命令出征，1207年奉命出征森林部落\[1\]，1211年和1213年两次攻[金](../Page/金朝.md "wikilink")，1217年伐[吉利吉思](../Page/吉利吉思.md "wikilink")，1219年征[訛答剌和](../Page/訛答剌.md "wikilink")[花剌子模](../Page/花剌子模.md "wikilink")，戰果豐碩。惟中途進攻[玉龍傑赤時](../Page/烏爾根奇.md "wikilink")，因故與察合臺有糾紛，影響到攻城的進度，引起成吉思汗的憤怒。爾後因西征有功，成吉思汗将他封到距離[蒙古草原本土最遠的](../Page/蒙古草原.md "wikilink")[欽察做](../Page/欽察.md "wikilink")[欽察汗](../Page/金帳汗國.md "wikilink")（[欽察位于今日](../Page/欽察.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[鹹海和](../Page/鹹海.md "wikilink")[裏海之北](../Page/裏海.md "wikilink")）。

1223年成吉思汗召他覲见，朮赤因病未能遠行，有人诬告说他並没有生病，而且還能打獵，成吉思汗聞訊大怒，懷疑朮赤不遵從他的號令，决定西征，卻在出征前獲知朮赤因病辭世的消息。

朮赤的汗位由次子拔都繼承。拔都西征后于1242年建立了[金帐汗国](../Page/金帐汗国.md "wikilink")。他与另外十三兄弟生活在金帐汗国。

## 家庭

朮赤有最少 14 個兒子\[2\] 和2個女兒，詳列如下：

  - 長子[斡兒答](../Page/斡兒答.md "wikilink")(Orda，1204年-1280年)，后裔统领[白帐汗国](../Page/白帐汗国.md "wikilink")。
  - 次子[拔都](../Page/拔都.md "wikilink")(Batu，1205年-1255年)
  - 三子[別兒哥](../Page/別兒哥.md "wikilink")：金帳汗國可汗，1257年-1267年在位\[3\]。
  - 四子伯克迭儿Berkhechir
  - 五子[昔班](../Page/昔班.md "wikilink")(Shiban)：后裔统领[蓝帐汗国](../Page/蓝帐汗国.md "wikilink")，[布哈拉汗国创立人始祖](../Page/布哈拉汗国.md "wikilink")
  - 六子[唐古特](../Page/唐古特.md "wikilink")
  - 七子[土斡耳](../Page/土斡耳.md "wikilink")(Teval 或
    Buval)：後裔[那海](../Page/那海.md "wikilink")、[恰卡](../Page/恰卡.md "wikilink")
  - 八子赤老温（Chilagun）
  - 九子升库尔（Sinqur）
  - 十子沉白（Chimbay）
  - 十一子伯剌·摩诃末（Muhammed）
  - 十二子兀都儿（Udur）
  - 十三子[禿花帖木兒](../Page/禿花帖木兒.md "wikilink")(Tuq-timur)：[克里米亞汗國](../Page/克里米亞汗國.md "wikilink")、[喀山汗國](../Page/喀山汗國.md "wikilink")、[哈萨克汗国創立人始祖](../Page/哈萨克汗国.md "wikilink").
  - 十四子真古木（Shingum）
  - 一女[火雷嫁](../Page/火魯.md "wikilink")[斡亦剌惕部首領](../Page/斡亦剌.md "wikilink")[忽都合別乞之子合歹](../Page/忽都合別乞.md "wikilink")。\[4\]
  - 一女嫁[葛逻禄酋长斡匝儿](../Page/葛逻禄.md "wikilink")。

## 参考文献

  - 引用

<!-- end list -->

  - 書籍

<!-- end list -->

  - 《蒙古秘史》，余大鈞譯，河北人民出版社，2001年5月出版

{{-}}

[S术](../Category/蒙古帝国皇族.md "wikilink")
[1](../Category/元太祖皇子.md "wikilink")
[S术](../Category/1185年出生.md "wikilink")
[S术](../Category/1225年逝世.md "wikilink")
[Category:金帐汗国君主](../Category/金帐汗国君主.md "wikilink")
[Category:蒙古族人物](../Category/蒙古族人物.md "wikilink")
[S术](../Category/射鵰英雄傳角色.md "wikilink")
[射](../Category/金庸筆下真實人物.md "wikilink")

1.  《蒙古秘史》，余大鈞譯，394頁
2.  H.H.Howorth-History of the Mongols, part.II div.II, p.35
3.  David Morgan, *The Mongols*, p. 224
4.  《蒙古秘史》，余大鈞譯，397頁