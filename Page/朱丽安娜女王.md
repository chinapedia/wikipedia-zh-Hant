**朱丽安娜女王**（**Juliana Louise Emma Marie
Wilhelmina**，），全名**朱丽安娜·路易丝·艾玛·玛丽·威廉明娜**，荷兰女王[贝娅特丽克丝的母亲](../Page/贝娅特丽克丝_\(荷兰\).md "wikilink")。1948年9月[登基](../Page/登基.md "wikilink")，任[荷兰女王](../Page/荷兰女王.md "wikilink")，1980年4月[內禪让位给女儿](../Page/內禪.md "wikilink")[贝娅特丽克丝](../Page/贝娅特丽克丝.md "wikilink")。

朱丽安娜在位時見證荷蘭兩大殖民地—[荷屬東印度和](../Page/荷屬東印度.md "wikilink")[荷屬圭亞那先後獨立為](../Page/荷屬圭亞那.md "wikilink")[印度尼西亞和](../Page/印度尼西亞.md "wikilink")[蘇里南](../Page/蘇里南.md "wikilink")。在她駕崩時，她以94歲之齡成為世上擔任過君主中最年長的人。

## 生平

朱丽安娜出生于[荷兰](../Page/荷兰.md "wikilink")[海牙的](../Page/海牙.md "wikilink")[努兒登堡宮](../Page/努兒登堡宮.md "wikilink")，是[威廉明娜女王和](../Page/威廉明娜女王.md "wikilink")[亨德里克亲王的獨女](../Page/亨德里克亲王_\(梅克伦堡-什未林\).md "wikilink")，故甫出生已是荷蘭王位推斷繼承人。

二战期间[德国侵占荷兰后](../Page/德国.md "wikilink")，荷兰王室於1940年5月到[英国和](../Page/英国.md "wikilink")[加拿大避难](../Page/加拿大.md "wikilink")，朱丽安娜与4个女儿中的3个在加拿大。戰爭結束後朱麗安娜等人於1945年回国，1948年9月威廉明娜女王因健康原因退位，39岁的朱丽安娜继承王位。
[Koningin_Juliana_bezoekt_de_Ir_Jonkie_school_te_Hwijk,_Bestanddeelnr_910-8472.jpg](https://zh.wikipedia.org/wiki/File:Koningin_Juliana_bezoekt_de_Ir_Jonkie_school_te_Hwijk,_Bestanddeelnr_910-8472.jpg "fig:Koningin_Juliana_bezoekt_de_Ir_Jonkie_school_te_Hwijk,_Bestanddeelnr_910-8472.jpg")
剛登基的朱丽安娜急需處理荷蘭最重要的殖民地—荷屬東印度獨立的事宜，她登基後一年，荷屬東印度的大部分便正式獨立成印尼；僅餘的[荷屬新畿內亞也於](../Page/荷屬新畿內亞.md "wikilink")13年後（1962年）在爭議中被迫交出，並併入印尼。而這片前殖民地在獨立後仍帶予女王和國家麻煩—爭取脫離印尼獨立的[南摩鹿加激進份子曾嘗試綁架女王](../Page/南摩鹿加共和國.md "wikilink")，以求迫使荷蘭承認南摩鹿加主權。綁架行動最後失敗，但仍導致國內發生多宗類似綁架事件。\[1\]

此外，在她任內，荷蘭王國的政治架構於1954年出現重大變革：原本的殖民地[荷屬安的列斯和](../Page/荷屬安的列斯.md "wikilink")[蘇里南去殖民化](../Page/蘇里南.md "wikilink")，成為與本土平起平坐的[構成國](../Page/構成國.md "wikilink")，並實現自治。惟後者於1975年完全獨立。

朱丽安娜在荷兰受到广泛的拥戴，被誉为“人民的女王”，每年4月30日她的生日是荷兰公共假日“女王日”。她的丈夫[贝恩哈德亲王曾经是荷兰军队总司令](../Page/贝恩哈德亲王_\(利珀-比斯特费尔德\).md "wikilink")，因1976年卷入[洛克希德贿赂案而放弃所有的军衔和军职](../Page/洛克希德.md "wikilink")。她71岁生日时把王位传给長女碧翠丝。2004年，94岁的太上女王朱丽安娜因[肺炎於](../Page/肺炎.md "wikilink")3月20日在[莎士迪克宮逝世](../Page/莎士迪克宮.md "wikilink")。

## 子嗣

[De_prinsessen_Beatrix,_Irene,_Margriet_en_Christina_wandelen_in_het_park_van_het,_Bestanddeelnr_255-7522.jpg](https://zh.wikipedia.org/wiki/File:De_prinsessen_Beatrix,_Irene,_Margriet_en_Christina_wandelen_in_het_park_van_het,_Bestanddeelnr_255-7522.jpg "fig:De_prinsessen_Beatrix,_Irene,_Margriet_en_Christina_wandelen_in_het_park_van_het,_Bestanddeelnr_255-7522.jpg")
朱丽安娜和丈夫[贝恩哈德亲王共有](../Page/貝恩哈德親王_\(利珀-比斯特費爾德\).md "wikilink")4个女儿：

1.  [贝娅特丽克丝女王](../Page/荷兰贝娅特丽克丝女王.md "wikilink")－1938年1月31日生于蘇斯臺克宮。
2.  [伊莲公主](../Page/伊莲公主_\(荷兰\).md "wikilink")－1939年8月5日生。
3.  [玛格丽特公主](../Page/玛格丽特公主_\(荷兰\).md "wikilink")－1943年1月19日生于[加拿大首都](../Page/加拿大.md "wikilink")[渥太華的渥太華醫院](../Page/渥太華.md "wikilink")(Ottawa,
    Ontario)的[国际](../Page/国际.md "wikilink")[租借地](../Page/租借地.md "wikilink")。
4.  [克里斯蒂娜公主](../Page/克里斯蒂娜公主_\(荷兰\).md "wikilink")－1947年2月18日生于蘇斯臺克宮。

## 皇家稱號

  - 1909-1937 朱丽安娜公主殿下，荷兰公主，奥兰治-拿騷公主，梅克倫堡-什未林女公爵
  - 1937-1948 朱丽安娜公主殿下，荷兰公主，奥兰治-拿騷公主，梅克倫堡-什未林女公爵，利珀-比斯特菲尔德王妃
  - 1948-1980 朱丽安娜女王陛下，荷兰女王，奥兰治-拿騷公主，梅克倫堡-什未林女公爵，利珀-比斯特菲尔德王妃
  - 1980-2004 朱丽安娜公主殿下，荷兰公主，奥兰治-拿騷公主，梅克倫堡-什未林女公爵，利珀-比斯特菲尔德王妃

## 参见

  - [荷蘭君主列表](../Page/荷蘭君主列表.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [荷蘭王室官方網頁](http://koninklijkhuis.nl/)

{{-}}

[Category:荷兰君主](../Category/荷兰君主.md "wikilink")
[Category:荷兰公主](../Category/荷兰公主.md "wikilink")
[Category:欧洲女性君主](../Category/欧洲女性君主.md "wikilink")
[Category:新教君主](../Category/新教君主.md "wikilink")
[Category:奧倫治-拿索王朝](../Category/奧倫治-拿索王朝.md "wikilink")
[Category:梅克伦堡王朝](../Category/梅克伦堡王朝.md "wikilink")
[Category:萊頓大學校友](../Category/萊頓大學校友.md "wikilink")
[Category:海牙人](../Category/海牙人.md "wikilink")
[Category:德国裔荷兰人](../Category/德国裔荷兰人.md "wikilink")
[Category:葬于代尔夫特新教堂王室墓地](../Category/葬于代尔夫特新教堂王室墓地.md "wikilink")

1.