**朱佐敬**（），[明朝第三代](../Page/明朝.md "wikilink")[靖江王](../Page/靖江王.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[安徽](../Page/安徽.md "wikilink")[鳳陽](../Page/鳳陽.md "wikilink")，父靖江王[朱贊儀](../Page/朱贊儀.md "wikilink")、母耿氏，永樂九年（1411年）襲封，初時用銀製印璽，[明宣宗時改用金塗印璽](../Page/明宣宗.md "wikilink")。朱佐敬愛好讀書、書法，常登[獨秀峰](../Page/獨秀峰.md "wikilink")，信仰佛教。在[明英宗時](../Page/明英宗.md "wikilink")，與弟弟奉國將軍**朱佐敏**的關係很差，以至常常互相向皇帝上奏誣告對方，英宗大怒，訓斥朱佐敬兄弟。成化五年薨，[諡](../Page/諡.md "wikilink")**莊簡**。王妃沈氏，子靖江懷順王[朱相承](../Page/朱相承.md "wikilink")。\[1\]

<center>

</center>

## 参考资料

  - 《[明史](../Page/明史.md "wikilink")·诸王·靖江王传》

## 注释

[Category:明朝宗室](../Category/明朝宗室.md "wikilink")
[Category:明朝靖江國藩王](../Category/明朝靖江國藩王.md "wikilink")
[Category:諡莊簡](../Category/諡莊簡.md "wikilink")

1.  《[明史·诸王·靖江王传](../Page/s:明史/卷118.md "wikilink")》:“庄简王佐敬成化五年薨。子[相承先卒](../Page/朱相承.md "wikilink")，孙昭和王[规裕嗣](../Page/朱规裕.md "wikilink")，弘治二年薨。子端懿王约麒嗣。”