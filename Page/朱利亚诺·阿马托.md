**朱利亚诺·阿马托**（，），生于意大利[都灵](../Page/都灵.md "wikilink")，政治家，曾任意大利總理。

## 生平

阿马托1960年毕业于[比萨大学法律系](../Page/比萨大学.md "wikilink")，后又获得美国[哥伦比亚大学法学硕士学位](../Page/哥伦比亚大学.md "wikilink")。

1983年当选众议员，1992年至1993年以社会党人身份担任意大利总理，領導由[意大利天主教民主黨](../Page/意大利天主教民主黨.md "wikilink")、[意大利共和黨](../Page/意大利共和黨.md "wikilink")、意大利自由黨、意大利社會黨及意大利民主社會黨最後一屆五黨聯合政府（[橄榄树联盟](../Page/橄榄树联盟.md "wikilink")）。1993年因貪腐醜聞而下台，天民黨等五黨結束近半個世紀的統治。

1999年5月，接替出任總統的[卡洛·阿泽利奥·钱皮](../Page/卡洛·阿泽利奥·钱皮.md "wikilink")，擔任国库部长。

2000年[马西莫·达莱马下台後](../Page/马西莫·达莱马.md "wikilink")，第二次出任意大利总理，直至2001年中右翼聯盟領導人[西尔维奥·贝卢斯科尼重新上台執政](../Page/西尔维奥·贝卢斯科尼.md "wikilink")。

2006年中左翼聯盟重新上台後，他在[罗马诺·普罗迪第二次內閣出任內政部長](../Page/罗马诺·普罗迪.md "wikilink")，直至2008年中右翼聯盟領導人西尔维奥·贝卢斯科尼重新上台執政。

[Category:美國文理科學院院士](../Category/美國文理科學院院士.md "wikilink")
[Category:意大利总理](../Category/意大利总理.md "wikilink")
[Category:義大利外交部長](../Category/義大利外交部長.md "wikilink")
[Category:義大利內政部長](../Category/義大利內政部長.md "wikilink")
[Category:義大利社會主義者](../Category/義大利社會主義者.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:比薩大學校友](../Category/比薩大學校友.md "wikilink")
[Category:都靈人](../Category/都靈人.md "wikilink")
[Category:意大利罗马天主教徒](../Category/意大利罗马天主教徒.md "wikilink")
[Category:欧洲大学学院教师](../Category/欧洲大学学院教师.md "wikilink")
[Category:意大利民主党党员](../Category/意大利民主党党员.md "wikilink")
[Category:意大利社会党党员](../Category/意大利社会党党员.md "wikilink")