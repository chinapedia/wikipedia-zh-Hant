[Axelrod.jpg](https://zh.wikipedia.org/wiki/File:Axelrod.jpg "fig:Axelrod.jpg")
**朱利叶斯·阿克塞尔罗德**（，），[美国](../Page/美国.md "wikilink")[生物化学家](../Page/生物化学.md "wikilink")。他与[伯纳德·卡茨](../Page/伯纳德·卡茨.md "wikilink")、[乌尔夫·冯·奥伊勒一起获得](../Page/乌尔夫·冯·奥伊勒.md "wikilink")1970年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

## 早年和教育

1912年5月30日，朱利叶斯·阿克塞尔罗德出生于[纽约](../Page/纽约.md "wikilink")[曼哈顿](../Page/曼哈顿.md "wikilink")[下东城的一所冷水公寓中](../Page/下东城.md "wikilink")。父亲伊萨多和母亲莫莉都是[加利西亚犹太人移民](../Page/加利西亚_\(中欧\).md "wikilink")，在纽约相识。伊萨多是为杂货商制作篮子的工人，朱利叶斯小时候经常要在星期六和父亲一起送货。尽管阿克塞尔罗德家并无浓厚的犹太教氛围，朱利叶斯从小学会了[意地绪语](../Page/意地绪语.md "wikilink")。每天要上完公立学校后，宗教学校的训练是他必做的功课。\[1\]

## 参考资料

  - [诺贝尔官方网站关于朱利叶斯·阿克塞尔罗德介绍](http://nobelprize.org/nobel_prizes/medicine/laureates/1970/axelrod-bio.html)

[A](../Category/諾貝爾生理學或醫學獎獲得者.md "wikilink")
[A](../Category/美國生物化學家.md "wikilink")
[A](../Category/紐約市立學院校友.md "wikilink")
[A](../Category/喬治·華盛頓大學校友.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")

1.