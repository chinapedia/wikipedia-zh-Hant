**朱國祚**，字兆隆，號養淳，[浙江省](../Page/浙江省.md "wikilink")[嘉興府](../Page/嘉興府.md "wikilink")[秀水縣人](../Page/秀水縣.md "wikilink")，明朝政治人物，萬曆癸未[狀元](../Page/狀元.md "wikilink")。官至戶部尚書、武英殿大學士。

## 生平

萬曆十一年（1583年）癸未科第一甲第一名進士\[1\]\[2\]。授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")。進[洗馬](../Page/洗馬.md "wikilink")，為皇長子侍班官，不久進諭德。萬曆二十六年（1598年），破格提拔為[禮部右侍郎](../Page/禮部右侍郎.md "wikilink")。後隱退。

[光宗即位](../Page/明光宗.md "wikilink")，國祚因曾侍潛邸，特旨拜[禮部尚書兼](../Page/禮部尚書.md "wikilink")[東閣大學士](../Page/東閣大學士.md "wikilink")，入閣參與機務。天啟元年（1621年）六月還朝。不久，加[太子太保](../Page/太子太保.md "wikilink")，進[文淵閣大學士](../Page/文淵閣大學士.md "wikilink")。天啟三年（1623年），進少保、太子太保、戶部尚書，改[武英殿大學士](../Page/武英殿大學士.md "wikilink")。連上十三疏乞休，詔加少傅兼太子太傅，乘傳歸。次年卒。贈太傅，謚文恪。\[3\]

## 家族

曾祖[朱恭](../Page/朱恭.md "wikilink")；祖[朱彩](../Page/朱彩.md "wikilink")，贈登仕佐郎[太醫院吏目](../Page/太醫院.md "wikilink")
；父[朱儒](../Page/朱儒.md "wikilink")，修職郎[太醫院御醫](../Page/太醫院.md "wikilink")；嫡母唐氏；生母王氏。

朱國祚生有六子，長子[朱大競](../Page/朱大競.md "wikilink")，為[朱彝尊曾祖父](../Page/朱彝尊.md "wikilink")。從子[朱大啟](../Page/朱大啟.md "wikilink")，文選郎中，終刑部左侍郎。

## 註釋

{{-}}

[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林院修撰](../Category/明朝翰林院修撰.md "wikilink")
[Category:明朝戶部尚書](../Category/明朝戶部尚書.md "wikilink")
[Category:明朝東閣大學士](../Category/明朝東閣大學士.md "wikilink")
[Category:明朝武英殿大學士](../Category/明朝武英殿大學士.md "wikilink")
[Category:明朝文渊阁大学士](../Category/明朝文渊阁大学士.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝詹事府右諭德](../Category/明朝詹事府右諭德.md "wikilink")
[Category:明朝太子太保](../Category/明朝太子太保.md "wikilink")
[Category:明朝少保](../Category/明朝少保.md "wikilink")
[Category:明朝追贈少傅](../Category/明朝追贈少傅.md "wikilink")
[Category:明朝追贈太子太傅](../Category/明朝追贈太子太傅.md "wikilink")
[Category:秀水人](../Category/秀水人.md "wikilink")
[Category:太醫院籍進士](../Category/太醫院籍進士.md "wikilink")
[G國](../Category/朱姓.md "wikilink")
[Category:諡文恪](../Category/諡文恪.md "wikilink")

1.  《萬曆十一年癸未科進士登科錄》：[太醫院官籍](../Page/太醫院.md "wikilink")，治《[書經](../Page/書經.md "wikilink")》，年二十五歲中式萬曆十一年癸未科第一甲第一名進士。八月二十五日生，行三，曾祖[朱恭](../Page/朱恭.md "wikilink")；祖[朱彩](../Page/朱彩.md "wikilink")，贈登仕佐郎[太醫院吏目](../Page/太醫院.md "wikilink")
    ；父[朱儒](../Page/朱儒.md "wikilink")，修職郎[太醫院御醫](../Page/太醫院.md "wikilink")；嫡母唐氏；生母王氏。[具慶下](../Page/具慶下.md "wikilink")，妻何氏，兄國禎；國賢；國祥；國士；國臣，弟國禮。由順天府學生中式順天府鄉試第十九名[舉人](../Page/舉人.md "wikilink")，[會試中式第二百四名](../Page/會試.md "wikilink")。
2.  《棗林雜俎》：萬歷癸未，秀水朱文恪進士第一，臚傳日文恪父太醫院使東山公緋服侍班。
3.  《明史·[卷二百四十](../Page/s:明史/卷240.md "wikilink")》：朱國祚傳