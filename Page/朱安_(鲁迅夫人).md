[Zhu_An_in_Beijing.jpg](https://zh.wikipedia.org/wiki/File:Zhu_An_in_Beijing.jpg "fig:Zhu_An_in_Beijing.jpg")
**朱安**（），[浙江省](../Page/浙江省.md "wikilink")[绍兴府](../Page/绍兴府.md "wikilink")[山阴县人](../Page/山阴县_\(浙江省\).md "wikilink")，中国作家[鲁迅](../Page/鲁迅.md "wikilink")（[周树人](../Page/周树人.md "wikilink")）的[元配](../Page/元配.md "wikilink")，亦是[包办婚姻妻子](../Page/包办婚姻.md "wikilink")，比鲁迅大三岁，是一位传统[纏足女性](../Page/纏足.md "wikilink")：会[烹饪](../Page/烹饪.md "wikilink")，会[縫紉](../Page/縫紉.md "wikilink")，不[识字](../Page/识字.md "wikilink")，性格温顺。

1906年，远在[日本](../Page/日本.md "wikilink")[醫學院](../Page/醫學院.md "wikilink")[留学的鲁迅被母亲](../Page/留学.md "wikilink")[鲁瑞用](../Page/鲁瑞.md "wikilink")“母病速归”的[电报召回](../Page/电报.md "wikilink")，回家的第二天（7月26日）就举行了[婚礼](../Page/婚礼.md "wikilink")。新婚后第四天鲁迅就又和二弟[周作人等东渡日本](../Page/周作人.md "wikilink")。鲁迅很不喜欢这位传统的妻子，无论是在浙江绍兴老家还是在[北京](../Page/北京.md "wikilink")，鲁迅都极力避免和朱安见面，而朱安大部分时间就是照顾婆婆。

1927年10月鲁迅和[许广平在](../Page/许广平.md "wikilink")[上海](../Page/上海.md "wikilink")[同居](../Page/同居.md "wikilink")。1936年10月19日，鲁迅因[肺結核在](../Page/肺結核.md "wikilink")[上海病逝](../Page/上海.md "wikilink")。1943年鲁迅的母亲在[北平病逝](../Page/北平.md "wikilink")。1947年6月29日，朱安在北平病逝，身边没有一个人。朱安生前希望自己死后能葬在鲁迅身边的愿望也没有实现，她葬在鲁迅母亲墓的旁边。

## 书籍

《我也是鲁迅的遗物·朱安传》是鲁迅原配夫人朱安的传记\[1\]。

## 參考資料

[Z朱](../Category/中華民國大陸時期人物.md "wikilink")
[Z朱](../Category/鲁迅.md "wikilink")
[Z朱](../Category/绍兴人.md "wikilink")
[Category:朱姓](../Category/朱姓.md "wikilink")

1.