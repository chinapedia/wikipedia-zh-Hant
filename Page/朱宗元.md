**朱宗元**（），卒年不详。字维城，[浙江](../Page/浙江.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")。他是，是西学东渐的先驱之一。[顺治](../Page/顺治.md "wikilink")[贡生](../Page/贡生.md "wikilink")，顺治五年中[举人](../Page/举人.md "wikilink")。

## 介绍

*节录自方豪著《中国天主教史人物传》第二册*

“著有答客问，同学张成、义能信订正，1697年[福建林文英作序](../Page/福建.md "wikilink")，有-{云}-：“古越朱子维城精其学，著答客问，今苏先生为
之重梓，问序于予，予不敏，何敢轻为赘笔？……考朱子之著篇也，年方二十二耳。超超见道，岁何其早，而力何其坚！殆斯教当兴而天主早授夫明道之人耶？……噫！朱子之尊天主也至矣！其欲正人心也明矣！读是篇者，不必歧之为西学，最其大经而合之为吾儒当奉之教，教固可行，道可一，而风亦可同也。”可见宗元信教必23岁前，且对传教有极大热诚。

按当时教士的西文书札中，都称朱宗元为**朱葛斯默**，可知在省城受洗，又到[杭州延请神父到](../Page/杭州.md "wikilink")[宁波去的葛斯默](../Page/宁波.md "wikilink")，
即朱宗元。

宗元后来又写了一部拯世略说，在自叙中，历述他求道，得道和传道的经过。1640年阳玛诺（E.
Diaz）译轻世金书宗元之润色[校订](../Page/校订.md "wikilink")，用[尚书谟体](../Page/尚书.md "wikilink")，以显其高古，文字赡奥艰深，非深通经书者不办。宗元又撰轻世金书直解
，未见传本。1642年，玛诺又有一天主圣教十诫直诠之作，宗元亦为作序。

[崇祯](../Page/崇祯.md "wikilink")1642年，孟儒望（J.
Monteiro）神父在宁波出版天学略义，解释使徒信经，宗元与李魏学濂同为较正。1659年夏，贾宜睦（J.
de Gravina）神父提正编亦完成，宗元又与李祖白、何世贞共预[参校](../Page/校對.md "wikilink") 。”

## 学生

经朱宗元介绍，[刘宗周的学生](../Page/刘宗周.md "wikilink")[张能信成为天主教徒](../Page/张能信.md "wikilink")。

## 重要著作

《答客问》、《拯世略说》、《天主圣教豁疑论》等

## 参考文献

  - [朱宗元介绍](https://web.archive.org/web/20071012145115/http://archives.catholic.org.hk/books/author/chu.htm)

{{-}}

[Z](../Category/明朝天主教人物.md "wikilink")
[Z](../Category/清朝天主教徒.md "wikilink")
[Z](../Category/鄞县人.md "wikilink")
[Z宗元](../Category/朱姓.md "wikilink")
[Category:順治五年戊子科浙江鄉試舉人](../Category/順治五年戊子科浙江鄉試舉人.md "wikilink")