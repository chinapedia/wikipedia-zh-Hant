**朱少麟**，本名朱蕙麟，出生於[台灣](../Page/台灣.md "wikilink")[嘉義市](../Page/嘉義市.md "wikilink")，祖籍[湖北](../Page/湖北.md "wikilink")，為台灣當代女性作家。

## 簡傳

朱少麟，曾就讀[曉明女中](../Page/曉明女中.md "wikilink")、[輔仁大學英國語文學系畢業](../Page/輔仁大學.md "wikilink")，早先是在從事政治公關公司工作之餘，挑燈寫下第一本小說《[傷心咖啡店之歌](../Page/傷心咖啡店之歌.md "wikilink")》，獲得廣大好評，被譽為「天生的作家」。第二部小說《[燕子](../Page/燕子_\(小說\).md "wikilink")》之後則專職寫作，五年多後方有第三部小說《[地底三萬呎](../Page/地底三萬呎.md "wikilink")》出版。

媒體曾指出，作者寫第一本書時，是想要叛離生活，寫第二本時是想試試自己是否能夠持續寫作，而第三本《地底三萬呎》，則是為了想寫出一本自己心目中的最佳作品、想挑戰自己能夠超越的藝術成就。

[朱少麟留給吟陸咖啡的明信片（左上角二）](https://zh.wikipedia.org/wiki/File:吟陸咖啡的明信片_朱少麟.jpg "fig:朱少麟留給吟陸咖啡的明信片（左上角二）")

2000年，朱少麟獲得[中國文藝協會頒發的](../Page/中國文藝協會.md "wikilink")[中國文藝獎章小說創作獎](../Page/中國文藝獎章.md "wikilink")。

## 著作

目前出版了三本[長篇小說](../Page/長篇小說.md "wikilink")，皆由[九歌出版社出版](../Page/九歌出版社.md "wikilink")：

  - 《[傷心咖啡店之歌](../Page/傷心咖啡店之歌.md "wikilink")》（1996年）\[1\]
  - 《[燕子](../Page/燕子_\(小說\).md "wikilink")》（1999年）
  - 《[地底三萬呎](../Page/地底三萬呎.md "wikilink")》（2005年）。
  - 其三本巨著的簡体字版亦陸續於中國正式發行。

## 關於傷心咖啡店

關於傷心咖啡店，朱少麟是以素人演員[陸弈靜開設](../Page/陸弈靜.md "wikilink")\[2\]的[臺北市](../Page/臺北市.md "wikilink")[景美](../Page/景美.md "wikilink")[景中街](../Page/景中街.md "wikilink")「[吟陸商號](../Page/吟陸商號.md "wikilink")」（吟陸咖啡，舊名「芙陸咖啡」、「聯禾咖啡」）為藍圖，朱少麟也常在吟陸商號寫作《[傷心咖啡店之歌](../Page/傷心咖啡店之歌.md "wikilink")》一書。對於此說，朱少麟沒有直接承認，但曾有報導指出他習慣在隱密的狀態動筆，應不會在咖啡館裡寫作。

吟陸商號於2006年四月[轉讓](../Page/轉讓.md "wikilink")，不再由陸弈靜經營；據媒體訪談朱少麟內容得知，《傷心咖啡店之歌》一書所提及的人物，除了海安之外，均真有其人。

## 外部連結

  - [朱少麟部落格](http://blog.yam.com/chiuko30000)
  - 博客來專訪朱少麟：[從地底三萬呎的深沉夢境站上現實世界（圖文）](http://www.books.com.tw/activity/under/visit.htm)
  - 自由時報專訪：[她寫了一顆深水炸彈─專訪朱少麟（圖文）](http://www.libertytimes.com.tw/2005/new/jul/29/life/article-1.htm)

## 參考文獻

[Z](../Category/臺灣女性小說家.md "wikilink")
[Z](../Category/輔仁大學校友.md "wikilink")
[Z朱](../Category/臺中市私立曉明女子高級中學校友.md "wikilink")
[Category:嘉義市人](../Category/嘉義市人.md "wikilink")
[Category:朱姓](../Category/朱姓.md "wikilink")

1.
2.  參考