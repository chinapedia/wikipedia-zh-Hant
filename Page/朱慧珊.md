**朱慧珊**（，），[香港主持人](../Page/香港.md "wikilink")，前[亞洲電視及](../Page/亞洲電視.md "wikilink")[無綫電視女藝員](../Page/無綫電視.md "wikilink")。曾在1985年參選由亞洲電視舉辦的第一屆[亞洲小姐競選並奪得由傳媒投票選出的](../Page/亞洲小姐競選.md "wikilink")「和平小姐」\[1\]。

## 生平

朱慧珊中學時就讀大埔正而學校和[王肇枝中學](../Page/王肇枝中學.md "wikilink")，後來參加1985年[亞洲小姐](../Page/亞洲小姐.md "wikilink")，獲得和平小姐名銜。其後簽約[亞洲電視](../Page/亞洲電視.md "wikilink")，為亞視藝人，演出多部電視劇。1989年轉投[無綫電視](../Page/無綫電視.md "wikilink")，主力主持《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》。

1999年，朱慧珊重返亞視，更與[何守信一同出任](../Page/何守信.md "wikilink")[亞視新聞](../Page/亞視新聞.md "wikilink")[主播](../Page/新聞主播.md "wikilink")。後期多專注資訊節目工作。2009年，朱慧珊不滿亞視藝人簽約新政策\[2\]，決定離開亞視，並拒絕加入[仁美清敍](../Page/仁美清敍.md "wikilink")，而是轉投[至八會](../Page/至八會.md "wikilink")，從而回歸無綫電視出任[公關人員](../Page/公關.md "wikilink")。2011年9月，朱慧珊又一次重返亞視，並隨即主持大型跳舞比賽《[亞洲星光大道4](../Page/亞洲星光大道4.md "wikilink")》。

隨著[亞視不獲續牌](../Page/亞洲電視欠薪及不獲續牌事件.md "wikilink")，2015年9月，朱慧珊再度離開亞視\[3\]。亞視後來以[OTT數碼形式重新開台](../Page/OTT.md "wikilink")，至2018年9月5日，她與[吳雲甫](../Page/吳雲甫.md "wikilink")、[梁思浩及前](../Page/梁思浩.md "wikilink")[無綫新聞主播之一](../Page/無綫新聞.md "wikilink")[周嘉儀和](../Page/周嘉儀.md "wikilink")[黃紫盈等人出席](../Page/黃紫盈.md "wikilink")[大埔工業邨亞視總台全新資訊節目](../Page/大埔工業邨.md "wikilink")《香港追擊搜》的宣傳活動。\[4\]

### 個人

1995年，朱慧珊嫁給法律顧問葉順榮，婚後10個月閃電離婚；2008年，跟拍拖11年的商人賈鈞鵬於[關島註冊結婚](../Page/關島.md "wikilink")。

## 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - [群芳頌](../Page/群芳頌.md "wikilink") (1985)
  - [冒險家樂園](../Page/冒險家樂園_\(電視劇\).md "wikilink") (1986)
  - [斷腸人在天涯](../Page/斷腸人在天涯.md "wikilink") (1986)
  - [時光倒流70年](../Page/時光倒流70年_\(電視劇\).md "wikilink") (1986)
  - [紅塵](../Page/紅塵_\(電視劇\).md "wikilink") (1987) 飾 梅淑韻（Ada）
  - [鐵血藍天](../Page/鐵血藍天.md "wikilink") (1987)
  - [滿清十三皇朝之努爾哈赤](../Page/滿清十三皇朝.md "wikilink") (1987) 飾 佟佳氏
  - [天橋](../Page/天橋_\(電視劇\).md "wikilink") (1987) 飾 cathy
  - [黑夜](../Page/黑夜_\(電視劇\).md "wikilink") (1988) 飾 楊允芝
  - [愛火奔流](../Page/愛火奔流.md "wikilink") (1988)
  - [上海風雲](../Page/上海風雲.md "wikilink")（1989） 飾 孫小冬

## 電影

  - [脂粉雙雄](../Page/脂粉雙雄.md "wikilink")（1990）飾 朱慧文（女警）

## 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [橫財三千萬](../Page/橫財三千萬.md "wikilink")（1991）飾 李麗霞
  - [兄兄我我](../Page/兄兄我我.md "wikilink")（1992）飾 歐嘉麗

## 主持（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 1986年：[綜合86-300次](../Page/綜合86-300次.md "wikilink")
  - 1989年: [亞視煙花照萬家](../Page/亞視煙花照萬家.md "wikilink")
  - 1999年：[亞視七點半新聞](../Page/亞視七點半新聞.md "wikilink")
  - 1999年：[亞洲早晨](../Page/亞洲早晨.md "wikilink")
  - 2000年：[亞視](../Page/亞視.md "wikilink")[六點鐘新聞](../Page/六點鐘新聞.md "wikilink")
  - 2001年：[世界不平常](../Page/世界不平常.md "wikilink")
  - 2001年：[下午T1T](../Page/下午T1T.md "wikilink")
  - 2002年：[群雄奪寶](../Page/群雄奪寶.md "wikilink")
  - 2002年：[全民大測試](../Page/全民大測試.md "wikilink")
  - 2003年：[抗炎行動](../Page/抗炎行動.md "wikilink")
  - 2003年：[走馬南粵 廣東高層新思維](../Page/走馬南粵_廣東高層新思維.md "wikilink")
  - 2004年：[放眼東北](../Page/放眼東北.md "wikilink")
  - 2005年：[香港風華](../Page/香港風華.md "wikilink")
  - 2005年：[三年零八個月](../Page/三年零八個月.md "wikilink")
  - 2002年－2007年：[慧珊時尚坊](../Page/慧珊時尚坊.md "wikilink")
  - 2007年：[5個半a餐](../Page/5個半a餐.md "wikilink")--[尚·流](../Page/尚·流.md "wikilink")
  - 2008年：[百年基石－香港地產史話](../Page/百年基石－香港地產史話.md "wikilink")
  - 2008年：[文化潮流](../Page/文化潮流.md "wikilink")
  - 2009年：[生活著數台](../Page/生活著數台.md "wikilink")
  - 2011年：[亞洲星光大道4](../Page/亞洲星光大道4.md "wikilink")
  - 2012年：[ATV2012感動香港人物推選](../Page/ATV2012感動香港人物推選.md "wikilink")
  - 2012年：[倫敦奧運開幕大派對](../Page/倫敦奧運開幕大派對.md "wikilink")（[亞視與](../Page/亞視.md "wikilink")[無綫聯合製作](../Page/無綫.md "wikilink")）
  - 2012年: [ATV 2013節目巡禮](../Page/ATV_2013節目巡禮.md "wikilink")
  - 2013年: [ATV 2013人氣春晚 唱紅亞洲](../Page/ATV_2013人氣春晚_唱紅亞洲.md "wikilink")
  - 2013年: [愛．潮流](../Page/愛．潮流.md "wikilink")
  - 2014年: [亞洲小姐競選](../Page/亞洲小姐.md "wikilink")

## 主持（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1989-1994年：[歡樂今宵](../Page/歡樂今宵.md "wikilink")
  - 1990、1993年：寰宇風情 菲律賓、斯里蘭卡、-{馬爾代夫}-特輯
  - 1991年：猛料差館
  - 1993-1994年：不可思議星期二
  - 1993年：大懷舊1993（嘉賓主持）
  - 1993年：深愛著您陳百強
  - 1994年：繽紛居樂部
  - 1995年：奇趣任縱橫

## 綜合節目（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - [亞洲小姐競選](../Page/亞洲小姐競選.md "wikilink")
  - [亞洲先生競選](../Page/亞洲先生競選.md "wikilink")

## 曾獲獎項

  - 1985年[亞洲小姐](../Page/亞洲小姐.md "wikilink")-和平小姐

## 代言廣告

  - [澳門公共汽車有限公司](../Page/澳門公共汽車有限公司.md "wikilink")[18號路線](../Page/澳門巴士18路線.md "wikilink")

## 參考

## 外部連結

  - [【亞姐百人】朱慧珊主持 陳啟泰 問題青年團 - 古卓文
    彭遠揚(1)](http://v.youku.com/v_show/id_XNTQ5MDczNzAw.html) 上传于
    2013-04-28 [优酷网](../Page/优酷网.md "wikilink")

[WAI](../Category/朱姓.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視女藝員](../Category/前亞洲電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前亞洲電視新聞報導員](../Category/前亞洲電視新聞報導員.md "wikilink")
[Category:亞洲小姐](../Category/亞洲小姐.md "wikilink")
[Category:王肇枝中學校友](../Category/王肇枝中學校友.md "wikilink")

1.  [朱慧珊做亞視新聞主播得廿分鐘考慮](http://hk.on.cc/hk/bkn/cnt/entertainment/20161019/bkn-20161019074040284-1019_00862_001.html)
2.  [朱慧珊决不续约亚视
    高层澄清只是减少艺人活动](http://www.tvptv.com/Html/TvInfo/2009/08/info_34899.html)

3.
4.