[Just_Fontaine.jpg](https://zh.wikipedia.org/wiki/File:Just_Fontaine.jpg "fig:Just_Fontaine.jpg")
**朱斯特·方丹**（****，），[法国](../Page/法国.md "wikilink")[足球运动员](../Page/足球.md "wikilink")。生于[摩洛哥](../Page/摩洛哥.md "wikilink")[马拉喀什](../Page/马拉喀什.md "wikilink")。他在[1958年瑞典世界杯中进球数的纪录為](../Page/1958年世界杯足球赛.md "wikilink")13个。

方丹的职业生涯开始于[卡萨布兰卡的一支球队](../Page/卡萨布兰卡.md "wikilink")，他在那里从1950年踢到1953年。之后，他转会到[尼斯足球俱乐部](../Page/尼斯.md "wikilink")，在三个赛季中贡献了44个进球。1956年，他来到[兰斯体育足球俱乐部](../Page/兰斯足球俱乐部.md "wikilink")，取代了[雷蒙·科帕的核心位置](../Page/雷蒙·科帕.md "wikilink")，方丹在之后的6个赛季中共进121球。他在[法甲联赛的总共](../Page/法甲联赛.md "wikilink")200场比赛中共射入165球，并在1958年和1960年两度获得联赛冠军。

方丹在[法国国家队的表现给人留下了更深刻的印象](../Page/法国国家足球队.md "wikilink")。1953年12月17日，他在对[卢森堡队的比赛中上演了](../Page/卢森堡国家足球队.md "wikilink")[帽子戏法](../Page/帽子戏法.md "wikilink")，帮助国家队以8比0狂胜对手。到1960年为止，他在21场国家队比赛中共射入30球。当然，方丹最为人们所称道的还是1958年世界杯上的表现，他在6场比赛中共射入13球，包括对[西德队单场比赛的](../Page/德国国家足球队.md "wikilink")4个入球。这个纪录历经40余年都未能被打破。他本人也当之无愧地荣获[世界杯金靴奖的称号](../Page/世界杯金靴奖.md "wikilink")。

方丹最后一次参赛是在1962年7月，由于他受到伤痛的反复侵扰，不得不提早退役。之后他在1967年挑起了法国国家队主教练的重担，然而却在两场失利后被撤换。

2004年3月，[国际足联评选出历史上](../Page/国际足联.md "wikilink")[125名最伟大球员](../Page/FIFA_100.md "wikilink")，方丹也名列其中。

## 外部链接

  - [方丹个人主页](http://www.justfontaine.com/)

[Category:1958年世界盃足球賽球員](../Category/1958年世界盃足球賽球員.md "wikilink")
[Category:法國國家足球隊主教練](../Category/法國國家足球隊主教練.md "wikilink")
[Category:法國足球主教練](../Category/法國足球主教練.md "wikilink")
[Category:法國國家足球隊球員](../Category/法國國家足球隊球員.md "wikilink")
[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:圖盧茲主教練](../Category/圖盧茲主教練.md "wikilink")
[Category:巴黎聖日耳門主教練](../Category/巴黎聖日耳門主教練.md "wikilink")
[Category:蘭斯球員](../Category/蘭斯球員.md "wikilink")
[Category:尼斯球員](../Category/尼斯球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink") [Category:FIFA
100](../Category/FIFA_100.md "wikilink")
[Category:法国的世界之最](../Category/法国的世界之最.md "wikilink")
[Category:足球之最](../Category/足球之最.md "wikilink")
[Category:马拉喀什人](../Category/马拉喀什人.md "wikilink")
[Category:西班牙裔法國人](../Category/西班牙裔法國人.md "wikilink")