**朱桱**（），明太祖[朱元璋第二十三子](../Page/朱元璋.md "wikilink")，母[李賢妃](../Page/李贤妃_\(明太祖\).md "wikilink")。

洪武二十四年（1391年）受封唐王，在就藩前建造唐王府和[王府山](../Page/王府山_\(南阳市\).md "wikilink")（王府花園的假山）。永樂六年（1408年）就藩[南陽](../Page/南阳市.md "wikilink")（今河南南陽）。永樂十三年薨，諡號定王。子靖王[朱瓊烴嗣](../Page/朱瓊烴.md "wikilink")。

1644年[甲申之變](../Page/甲申之變.md "wikilink")，[流寇](../Page/流寇.md "wikilink")[李自成攻陷](../Page/李自成.md "wikilink")[北京](../Page/北京.md "wikilink")，[明思宗](../Page/明思宗.md "wikilink")[自縊](../Page/自縊.md "wikilink")。1645年南京失守，[弘光帝被清兵所俘](../Page/弘光帝.md "wikilink")，朱桱的八世孫[朱聿鍵](../Page/朱聿鍵.md "wikilink")、[朱聿𨮁先後在](../Page/朱聿𨮁.md "wikilink")[福州和](../Page/福州.md "wikilink")[廣州](../Page/廣州.md "wikilink")[稱帝](../Page/稱帝.md "wikilink")，是為[隆武帝](../Page/隆武帝.md "wikilink")、[紹武帝](../Page/紹武帝.md "wikilink")。

## 家庭

### 妻妾

  - 王妃吴氏，黔国威毅公[吴复之孙女](../Page/吴复.md "wikilink")。
  - 妾孙氏，生新野郡王朱瓊煒和河内郡主。孙[朱芝城曾在](../Page/朱芝城.md "wikilink")[景泰五年](../Page/景泰_\(年号\).md "wikilink")（1454年）为祖母孙氏请求[夫人的封号](../Page/夫人_\(位号\).md "wikilink")\[1\]，后事不详。

### 子女

#### 子

1.  唐靖王 [朱瓊烴](../Page/朱瓊烴.md "wikilink")
2.  唐憲王 [朱瓊炟](../Page/朱瓊炟.md "wikilink")
3.  新野悼懷王 [朱瓊煒](../Page/朱瓊煒.md "wikilink")

#### 女

1.  河内郡主

## 壙志

<div class="NavFrame" style="text-align: left; width:60%">

王讳桱，太祖高皇帝第二十二子也，母贤妃李氏。生于洪武丙寅九月十八日，辛未四月十三日册封为唐王，永乐六年始受命之国。十三年秋八月六日王以疾薨，享年三十。妃吴氏，黔国威毅公吴复之孙女。皇上念骨肉之亲，不胜悼痛。辍视朝十又五日，诏有司治丧葬如礼，赐谥曰定，遣使驰祭。以永乐十三年十二月二十一日葬于紫山之原。呜呼！王以国家至亲，永作藩屏，胡天啬其年，而一旦遽至大故，其可痛也！夫爰述其概，纳诸幽圹中，用垂不朽焉。谨志。

</div>

## 注释

[23](../Category/明太祖皇子.md "wikilink")
[Category:明朝唐國藩王](../Category/明朝唐國藩王.md "wikilink")
[Category:諡定](../Category/諡定.md "wikilink")
[Category:朱姓](../Category/朱姓.md "wikilink")

1.  《明英宗实录·卷之二百四十》○新野王芝城奏臣不幸早孤叨承父爵祖母孙氏生臣父姑二人父封新野郡王姑封河内郡主惟祖母一向未蒙封赠每遇家庙祭祀诸妇皆具冠帔独祖母以常服行礼乞加封赐诏封孙氏为夫人赐之冠服又奏臣所居地方最为偏僻□旅少主货物艰贵乞准令内使一人带校尉二人往苏州等处两平收买回府应用从之......