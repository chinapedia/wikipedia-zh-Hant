**朱玲玲**（，），祖籍[廣東](../Page/廣東.md "wikilink")[台山](../Page/台山.md "wikilink")，出生於[緬甸](../Page/緬甸.md "wikilink")，[香港前](../Page/香港.md "wikilink")[女演員](../Page/女演員.md "wikilink")、女[模特兒](../Page/模特兒.md "wikilink")，1977年[香港小姐競選冠軍及最上鏡小姐](../Page/香港小姐競選.md "wikilink")，[羅康瑞妻子](../Page/羅康瑞.md "wikilink")\[1\]，[霍震霆前妻](../Page/霍震霆.md "wikilink")。

## 简介

朱玲玲生于缅甸，其父母是緬甸華僑。9歲時全家到香港定居，小學畢業於九龍聖德肋撒英文學校，中學畢業於[香港國際學校](../Page/香港國際學校.md "wikilink")，並在[英皇佐治五世學校就讀預科](../Page/英皇佐治五世學校.md "wikilink")。

朱玲玲曾任業餘[模特兒](../Page/模特兒.md "wikilink")，1977年參加香港小姐競選，取得冠軍及最上鏡小姐獎項，是首位奪得兩個獎項的參賽者。當選後只在娛樂圈發展了很短時間，在[無綫電視拍過一部](../Page/無綫電視.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")《[瑪麗關77](../Page/瑪麗關77.md "wikilink")》，客串過[電視劇](../Page/電視劇.md "wikilink")《[狂潮](../Page/狂潮.md "wikilink")》。九个月后即和[霍英東長子](../Page/霍英東.md "wikilink")[霍震霆闪婚](../Page/霍震霆.md "wikilink")，开创了港姐嫁豪门的先河。此外，由於2008年香港小姐冠軍[張舒雅沒有履行香港小姐應盡的責任繼而被無線電視雪藏](../Page/張舒雅.md "wikilink")，因此無線電視便安排她為2009年香港小姐冠軍[劉倩婷進行加冕儀式](../Page/劉倩婷.md "wikilink")，開了沒有上屆冠軍加冕的先例。

2011年1月13日，朱玲玲母親陳如美因心血管病離世，享年83歲\[2\]。2017年4月14日，朱玲玲父親朱奇宗因肺炎病逝，享年94歲\[3\]。

## 婚姻

  - 第一任丈夫：[霍震霆](../Page/霍震霆.md "wikilink")
      - 朱玲玲當選[香港小姐](../Page/香港小姐.md "wikilink")[冠軍後](../Page/冠軍.md "wikilink")，便結識商人[霍震霆](../Page/霍震霆.md "wikilink")。二人認識9個月後，在1978年9月25日舉行盛大[婚禮](../Page/婚禮.md "wikilink")，並於[尖沙咀](../Page/尖沙咀.md "wikilink")[美麗華酒店筵開](../Page/美麗華酒店.md "wikilink")360席。據報霍家以10,000,000港元禮金迎娶朱玲玲，婚紗價值5000[美元](../Page/美元.md "wikilink")，而整個婚宴成本則超過1,000,000港元。[霍震霆較朱玲玲年長](../Page/霍震霆.md "wikilink")14年，婚後朱玲玲共誕下3名兒子。早期朱玲玲淡出社交界，專心照顧3名兒子。至1990年，她曾與其姊合作，在[中環](../Page/中環.md "wikilink")[太子大廈開設售賣來自](../Page/太子大廈.md "wikilink")[英國飾物精品店](../Page/英國.md "wikilink")，但在家族封建壓力下退股。2001年，朱玲玲搬出霍家大宅，獨自居住於[壽臣山](../Page/壽臣山.md "wikilink")。2006年9月，朱玲玲的好友[歌星](../Page/歌星.md "wikilink")[葉蒨文向傳媒披露霍震霆與朱玲玲已經在](../Page/葉蒨文.md "wikilink")2004年離婚，而且早於1997年已分居。

<!-- end list -->

  - 第二任丈夫：[罗康瑞](../Page/罗康瑞.md "wikilink")
      - 2008年11月，朱玲玲與[瑞安集团](../Page/瑞安.md "wikilink")/[瑞安房地產](../Page/瑞安房地產.md "wikilink")[富豪](../Page/富豪.md "wikilink")、人稱上海姑爺的[羅康瑞在](../Page/羅康瑞.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")[四季酒店註冊](../Page/四季酒店.md "wikilink")[結婚](../Page/結婚.md "wikilink")。

## 子女

  - 三子。
      - 均与霍震霆所生，分别是[霍启刚](../Page/霍启刚.md "wikilink")、霍启山、霍启仁。

## 參見

  - [霍英東家族](../Page/霍英東家族.md "wikilink")
  - [罗鹰石家族](../Page/罗鹰石家族.md "wikilink")

## 資料來源

  - [淡泊名利熱心公益的雙料港姐朱玲玲](http://ent.sina.com.cn/m/2001-08-17/54192.html)

[Category:1977年度香港小姐競選參賽者](../Category/1977年度香港小姐競選參賽者.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[Category:缅甸華人](../Category/缅甸華人.md "wikilink")
[Ling](../Category/朱姓.md "wikilink")
[Category:九龍聖德肋撒英文學校校友](../Category/九龍聖德肋撒英文學校校友.md "wikilink")
[Category:英皇佐治五世學校校友](../Category/英皇佐治五世學校校友.md "wikilink")
[Category:罗鹰石家族](../Category/罗鹰石家族.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")

1.  [朱玲玲羅康瑞苦戀8年修成正果-搜狐娛樂](http://yule.sohu.com/s2008/08zhulingling/)
2.
3.