**朱祖祥**（），中国著名土壤学家、农学教育家，中国土壤化学的奠基人，[中国科学院院士](../Page/中国科学院.md "wikilink")。

## 简历

1916年，生于[浙江省](../Page/浙江省.md "wikilink")[宁波市](../Page/宁波市.md "wikilink")[慈溪](../Page/慈溪.md "wikilink")（现属[余姚](../Page/余姚.md "wikilink")）。

1938年，毕业于国立[浙江大学农学院](../Page/浙江大学.md "wikilink")，获学士学位。

1946年，获[美国](../Page/美国.md "wikilink")[密执安州立大学硕士学位](../Page/密执安州立大学.md "wikilink")。

1948年，获美国密执安州立大学博士学位。

1938年－1945年，在浙江大学农学院任教。1948年起先后任浙江大学农学院、浙江农学院、[浙江农业大学教授](../Page/浙江农业大学.md "wikilink")。

1980年－1983年，任[浙江农业大学](../Page/浙江农业大学.md "wikilink")（现[浙江大学华家池校区](../Page/浙江大学华家池校区.md "wikilink")）校长。

1980年，当选为中国科学院院士。

## 参考文献

{{-}}

[Category:中华人民共和国农学家](../Category/中华人民共和国农学家.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[Category:浙江大学校友](../Category/浙江大学校友.md "wikilink")
[Category:密西根州立大學校友](../Category/密西根州立大學校友.md "wikilink")
[Category:浙江农业大学教授](../Category/浙江农业大学教授.md "wikilink")
[Category:浙江农业大学校长](../Category/浙江农业大学校长.md "wikilink")
[Category:余姚人](../Category/余姚人.md "wikilink")
[Category:慈溪人](../Category/慈溪人.md "wikilink")
[Category:中国土壤学家](../Category/中国土壤学家.md "wikilink")
[Category:朱姓](../Category/朱姓.md "wikilink")