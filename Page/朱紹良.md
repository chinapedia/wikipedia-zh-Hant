[Zhu_Shaoliang.jpg](https://zh.wikipedia.org/wiki/File:Zhu_Shaoliang.jpg "fig:Zhu_Shaoliang.jpg")
**朱绍良**（），**字**一民，[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[一級上將](../Page/一級上將.md "wikilink")，[福建省](../Page/福建省.md "wikilink")[福州市人](../Page/福州市.md "wikilink")
[缩略图](https://zh.wikipedia.org/wiki/File:朱紹良.jpg "fig:缩略图")

## 生平

先后就读福建[陆军小学堂](../Page/陆军小学堂.md "wikilink")，南京[陆军第四中学堂](../Page/陆军第四中学堂.md "wikilink")，日本振武学校，日本陆军士官学校。

1910年加入中国[同盟会](../Page/同盟会.md "wikilink")，参加过[武昌起义](../Page/武昌起义.md "wikilink")、[讨袁战争](../Page/讨袁战争.md "wikilink")、[北伐](../Page/北伐.md "wikilink")、[中原大戰](../Page/中原大戰.md "wikilink")、對中共[中央苏区的第一](../Page/中央苏区.md "wikilink")、第二、第三次围剿等戰役。

1931年12月兼驻赣绥靖主任。1933年任[甘肃省主席兼驻甘绥靖主任](../Page/甘肃省.md "wikilink")。1935年兼任西北剿共军第1路总指挥。

1937年3月30日，行政院任命朱紹良[甘肅省政府主席](../Page/甘肅省政府.md "wikilink")\[1\]。

[七七事变后](../Page/七七事变.md "wikilink")，参加[淞沪会战](../Page/淞沪会战.md "wikilink")，任[第八战区司令长官](../Page/第八战区.md "wikilink")。[抗戰结束后](../Page/抗日战争.md "wikilink")，历任[國民政府軍事委員會副参谋总长](../Page/國民政府軍事委員會.md "wikilink")、[重庆](../Page/重庆.md "wikilink")[行辕主任](../Page/行辕.md "wikilink")、[新疆省主席](../Page/新疆省.md "wikilink")。1948年12月30日，重慶行轅主任朱紹良抵達南京\[2\]。

1949年1月18日，國防部遵蔣令，作以下人事任命：一、[湯恩伯專任京滬杭警備總司令](../Page/湯恩伯.md "wikilink")；二、[衢州](../Page/衢州.md "wikilink")[綏靖公署撤銷](../Page/綏靖公署.md "wikilink")，改設福州綏靖公署，派朱紹良為福州綏靖公署主任；三、派[張群為重慶綏靖公署主任](../Page/張群.md "wikilink")；四、廣州綏靖公署主任[宋子文專任](../Page/宋子文.md "wikilink")[廣東省政府主席](../Page/廣東省政府.md "wikilink")，派[余漢謀為廣州綏靖公署主任](../Page/余漢謀.md "wikilink")；五、[台灣警備司令部擴大為](../Page/台灣警備司令部.md "wikilink")[警備總司令部](../Page/警備總司令部.md "wikilink")，[陳誠兼任總司令](../Page/陳誠.md "wikilink")，派[彭孟緝為副總司令](../Page/彭孟緝.md "wikilink")\[3\]1月20日，蔣介石任命朱紹良為[福建省政府主席](../Page/福建省政府.md "wikilink")，[方天為](../Page/方天.md "wikilink")[江西省政府主席](../Page/江西省政府.md "wikilink")\[4\]。1月21日，蔣介石任命朱紹良為福建省政府主席兼綏靖公署主任\[5\]。5月19日，[蔣經國奉命經廈門飛往福州](../Page/蔣經國.md "wikilink")，訪福建省朱紹良主席「於福建省政府」\[6\]。8月至臺湾，後历任[总统府战略顾问](../Page/中華民國總統府戰略顧問.md "wikilink")、[国策顾問](../Page/中華民國總統府國策顧問.md "wikilink")、中國国民党中央党史委员会委员等职。

1963年12月25日在台北病逝，1964年被追晋为[陆军](../Page/中華民國陸軍.md "wikilink")[一级上将](../Page/一级上将.md "wikilink")。

## 参考文献

[Category:中華民國總統府戰略顧問](../Category/中華民國總統府戰略顧問.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中華民國陸軍一級上將](../Category/中華民國陸軍一級上將.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:北伐人物](../Category/北伐人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:福州裔台灣人](../Category/福州裔台灣人.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[S紹](../Category/朱姓.md "wikilink")

1.

2.
3.
4.
5.  秦孝儀總編纂：《總統蔣公大事長編初稿》，卷七（下）

6.  蔣經國〈危急存亡之秋〉，刊《風雨中的寧靜》，台北，[正中書局](../Page/正中書局.md "wikilink")，1988年