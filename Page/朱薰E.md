**朱薰E**是[香港商業電台一個電台節目](../Page/香港商業電台.md "wikilink")，由[朱薰主持](../Page/朱薰.md "wikilink")，初時為逢星期日[早上](../Page/早上.md "wikilink")9時至11時於[商業二台播出](../Page/商業二台.md "wikilink")，於2008年7月13日改為逢星期日[下午](../Page/下午.md "wikilink")3時至5時。2009年4月26日改為下午2時至4時。2009年7月26日為最後一集。

其後朱薰在暫代他人節目時會使用相同的節目名，例如：2010年2月21日下午4至5時、2012年3月29日至30日上午7時至8時、2013年4月19日下午12時至1時等，但純粹為音樂節目，播放音樂。

## 節目內容

本節目首半小時有朱薰教大家一個容易被人以懶音讀出的字，教懶音的字有多少的分別，還有會播一些有該字的歌。每次更會邀請嘉賓上來進行《薰E挑戰站》。

### 薰E挑戰站

  - 《薰E挑戰站》是朱薰會準備一段文字，那段文字有當天教的懶音，給嘉賓挑戰，本環節曾於[少爺占](../Page/少爺占.md "wikilink")、[Donald和](../Page/Donald.md "wikilink")[朱薰的節目](../Page/朱薰.md "wikilink")[萬世巨星同名環節](../Page/萬世巨星.md "wikilink")《薰E挑戰站》中播出。

### Sunday Brunch

  - 每個星期[朱薰會同一位好朋友出去食sunday](../Page/朱薰.md "wikilink") brunch。

## 外部連結

  - [朱薰E節目重溫](http://www.881903.com/Page/ZH-TW/Pro903_22.aspx)

|colspan="3" align=center|香港商業電台 叱咤903商業-{二台}-節目 |- |width=25%
align=center|**上一節目**
[朱薰SPA](../Page/朱薰SPA.md "wikilink")
[乜嘢地獄及](../Page/乜嘢地獄.md "wikilink")[妹妹妹妹](../Page/妹妹妹妹.md "wikilink")
[妹妹妹妹及](../Page/妹妹妹妹.md "wikilink")[朱薰 E](../Page/朱薰_E.md "wikilink")
|width=50% align=center|**朱薰E**
2006年4月16日－2008年7月6日
2008年7月13日－2009年4月19日
2009年4月26日－8月2日 |width=25% align=center|**下一節目**
[一周紅人館](../Page/一周紅人館.md "wikilink")
[朱薰 E及](../Page/朱薰_E.md "wikilink")[O 囧
O](../Page/O_囧_O.md "wikilink")（下午4-6）
[兒童適宜](../Page/兒童適宜.md "wikilink") |-

[Category:叱咤903節目](../Category/叱咤903節目.md "wikilink")