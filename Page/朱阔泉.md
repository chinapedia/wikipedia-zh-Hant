**朱阔泉**，男，满族，著名[相声大师](../Page/相声.md "wikilink")，[数来宝演员](../Page/数来宝.md "wikilink")。绰号“大面包”，第五代相声艺人。师承[相声八德之一的](../Page/相声八德.md "wikilink")[焦德海](../Page/焦德海.md "wikilink")。同门师兄弟中著名的有[张寿臣](../Page/张寿臣.md "wikilink")、[李寿增](../Page/李寿增.md "wikilink")、[常连安等](../Page/常连安.md "wikilink")。

育有三女。早年收养[侯宝林](../Page/侯宝林.md "wikilink")，后收为徒弟。

朱阔泉是首先将“[莲花落](../Page/莲花落.md "wikilink")”的引入相声的艺人之一。

`|width=30% align=center|`**`从师`**
`|width=40% align=center|`**`辈分`**
`|width=30% align=center|`**`收徒`**
`|-`

|width=30% align=center|[焦德海](../Page/焦德海.md "wikilink")

`|width=40% align=center|`**`寿`**
` |width=30% align=center |`[`王凤山`](../Page/王凤山_\(相声演员\).md "wikilink")`、`[`李宝麒`](../Page/李宝麒.md "wikilink")`、`[`侯宝林`](../Page/侯宝林.md "wikilink")`、`[`王宝童`](../Page/王宝童.md "wikilink")`、`[`马志明`](../Page/马志明.md "wikilink")` `

|-

[5](../Category/相声演员.md "wikilink")
[Category:满族人](../Category/满族人.md "wikilink")