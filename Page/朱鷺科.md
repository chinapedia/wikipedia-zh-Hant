**朱鷺科**（Threskiornithidae），别名**鹮科**，属于[鹈形目](../Page/鹈形目.md "wikilink")，全世界共有34种。共分兩個亞科，34個種。本科當中的[朱鷺亞科的種既可稱作朱鹭](../Page/朱鷺亞科.md "wikilink")，也可以叫做鹮，而[琵鷺亞科的則稱作琵鷺](../Page/琵鷺亞科.md "wikilink")。

## [屬](../Page/屬.md "wikilink")

  - [朱鷺亞科](../Page/朱鷺亞科.md "wikilink") Threskiornithinae
      - [鹮属](../Page/鹮属.md "wikilink") *Threskiornis*
      - [黑鹮属](../Page/黑鹮属.md "wikilink") *Pseudibis*
      - [大鹮属](../Page/大鹮属.md "wikilink") *Thaumatibis*
      - [隐鹮属](../Page/隐鹮属.md "wikilink") *Geronticus*
      - [朱鹮属](../Page/朱鹮属.md "wikilink") *Nipponia*
      - [白鹮属](../Page/白鹮属.md "wikilink") *Bostrychia*
      - [黄颈鹮属](../Page/黄颈鹮属.md "wikilink") *Theristicus*
      - [长尾鹮属](../Page/长尾鹮属.md "wikilink") *Cercibis*
      - [绿鹮属](../Page/绿鹮属.md "wikilink") *Mesembrinibis*
      - [裸脸鹮属](../Page/裸脸鹮属.md "wikilink") *Phimosus*
      - [美洲鹮属](../Page/美洲鹮属.md "wikilink") *Eudocimus*
      - [彩鹮属](../Page/彩鹮属.md "wikilink") *Plegadis*
      - [凤头彩鹮属](../Page/凤头彩鹮属.md "wikilink") *Lophotibis*

<!-- end list -->

  - [琵鷺亞科](../Page/琵鷺.md "wikilink") Plateinae
      - [琵鹭属](../Page/琵鹭属.md "wikilink") *Platalea*

[Category:鹈形目](../Category/鹈形目.md "wikilink")
[Category:鹮科](../Category/鹮科.md "wikilink")