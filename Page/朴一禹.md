**朴一禹**（[朝鮮語](../Page/朝鮮語.md "wikilink")：**박일우**，），朝鮮政治人物及[共產主義者](../Page/共產主義者.md "wikilink")，[延安派之一](../Page/延安派.md "wikilink")，後來被[金日成處死](../Page/金日成.md "wikilink")。\[1\]

## 生平

原籍[平安道](../Page/平安道.md "wikilink")，生於[間島附近](../Page/間島.md "wikilink")，他早年加入[中国共产党](../Page/中国共产党.md "wikilink")，在[中国东北参加革命](../Page/中国东北.md "wikilink")，1931年“[九一八事变](../Page/九一八事变.md "wikilink")”后前往[延安](../Page/延安.md "wikilink")，进入中共中央党校和[中国人民抗日军政大学学习](../Page/中国人民抗日军政大学.md "wikilink")，担任过敌后抗日根据地宣（化）涿（鹿）怀（来）联合县政府县长和县委书记。1942年7月，赴太行山抗日革命根据地参与创建朝鲜独立同盟和[朝鲜义勇军](../Page/朝鲜义勇军.md "wikilink")，任朝鲜独立同盟中央常务委员、朝鲜义勇军副司令，1945年2月兼任朝鲜革命军政学校副校长，期间当过[陕甘宁边区政府的参议员](../Page/陕甘宁边区.md "wikilink")，是1945年[中国共产党第七次大會的候补代表](../Page/中国共产党.md "wikilink")。

1945年回东北，1946年回朝鲜，当选[朝鲜劳动党中央政治委员会委员](../Page/朝鲜劳动党.md "wikilink")（到1953年8月朝鲜劳动党二届六中全会上被免去），担任中央干部部部长。1947年2月任朝鲜人民委员会内务局长。

[朝鲜民主主义人民共和国成立后](../Page/朝鲜民主主义人民共和国.md "wikilink")，当选为第一届最高人民会议议员，出任朝鲜民主主义人民共和国内阁副首相兼内务省相（1948年9月－1953年3月），被授予中将军衔。

1950年6月[朝鲜战争爆发](../Page/朝鲜战争.md "wikilink")，出任中央军事委员会委员和[中国人民志愿军副司令员兼副政治委员](../Page/中国人民志愿军.md "wikilink")、[中国人民志愿军委员会副书记](../Page/中国人民志愿军.md "wikilink")（实际未到任），11月任朝鲜人民军前线司令部副司令官（1950.11－1952.2），12月任中国人民志愿军和朝鲜人民军联合司令部（中朝联司）副政治委员（1950.12－1952.2）。

1951年金日成以“平壤失守”和“作战不力”为由解除了延安派势力最大的人物、民族保卫省副相兼人民军炮兵司令[武亭的职务](../Page/武亭.md "wikilink")。被认为是[毛泽东个人代表的内务相朴一禹在](../Page/毛泽东.md "wikilink")1953年2月所任的中朝军队联合司令部副政委一职被[崔庸健取代](../Page/崔庸健.md "wikilink")，3月朝鲜内阁改选时也被解除内阁内务相职务，改任递信相（邮政部长），8月朝鲜劳动党二届六中全会上在清洗[朴宪永集团后又被免去中央政治委员会委员职务](../Page/朴宪永.md "wikilink")。1955年4月的[朝鲜劳动党二届十六中全会上](../Page/朝鲜劳动党.md "wikilink")，金日成公开宣布朴一禹作为“反党宗派分子”，被开除出党，撤销党内外一切职务（崔庸健替代了他的位置）。最后被金日成处死。

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:朝鲜人民军次帅](../Category/朝鲜人民军次帅.md "wikilink")
[Category:朝鮮民主主義人民共和國政治人物](../Category/朝鮮民主主義人民共和國政治人物.md "wikilink")
[Category:朝鮮勞動黨中央政治局委員](../Category/朝鮮勞動黨中央政治局委員.md "wikilink")
[Category:朝鮮勞動黨黨員](../Category/朝鮮勞動黨黨員.md "wikilink")
[Category:朝鮮半島籍中國共產黨黨員](../Category/朝鮮半島籍中國共產黨黨員.md "wikilink")
[Category:朝鮮朝鮮戰爭人物](../Category/朝鮮朝鮮戰爭人物.md "wikilink")
[Category:朝鮮第二次世界大戰人物](../Category/朝鮮第二次世界大戰人物.md "wikilink")
[Category:朝鮮日佔時期人物](../Category/朝鮮日佔時期人物.md "wikilink")
[Category:延邊人](../Category/延邊人.md "wikilink")
[Il](../Category/朴姓.md "wikilink")
[Category:被處決的朝鮮人](../Category/被處決的朝鮮人.md "wikilink")
[Category:被朝鮮民主主義人民共和國處決者](../Category/被朝鮮民主主義人民共和國處決者.md "wikilink")

1.  <http://www.singtaonet.com/reveal/200703/t20070328_501738_1.html>