**朴新陽**（，），[韓國](../Page/大韓民國.md "wikilink")[男演員](../Page/演員.md "wikilink")。

## 經歷

朴新陽主要活躍於電影界，出道電影為1996年的《[琉璃](../Page/琉璃_\(電影\).md "wikilink")》，並於同年取得第17屆[青龍獎最佳新人男演員獎](../Page/青龍獎.md "wikilink")。1997年演出電影《[情書](../Page/情書_\(1997年電影\).md "wikilink")》（女主角為[崔真實](../Page/崔真實.md "wikilink")），獲得多項獎項肯定。於1998年出演《[約定](../Page/約定_\(電影\).md "wikilink")》（女主角為[全道嬿](../Page/全道嬿.md "wikilink")），同年取得第19屆[青龍獎最佳男主角獎及人氣獎](../Page/青龍獎.md "wikilink")，從此奠定他在韓國電影界首席男演員的地位。

他於2004年演出的電視劇《[巴黎戀人](../Page/巴黎戀人.md "wikilink")》令他的事業更上一層樓，該劇創下超過50％的收視率，被譽為「國民電視劇」。2007年的電影及電視作品分別為《[耀眼時刻](../Page/耀眼時刻.md "wikilink")》和《[錢的戰爭](../Page/錢的戰爭.md "wikilink")》，後者在韓國獲得非常高的收視及人氣，並讓他擊敗勁敵《[太王四神記](../Page/太王四神記.md "wikilink")》的裴勇俊，獲得有「韓國奧斯卡獎」之稱的第44屆百想藝術大賞電視部門「最佳男演員」的大賞榮譽。

2008年與[文瑾瑩出演SBS水木劇](../Page/文瑾瑩.md "wikilink")《[風之畫師](../Page/風之畫師.md "wikilink")》，飾演朝鮮時代著名畫家[金弘道](../Page/金弘道.md "wikilink")，此劇將是朴新陽從影13年來，第一次挑戰古裝劇集，韓國本國內外，包括朴新陽本人也對此充滿期待。

## 家庭

2002年10月與相差十三歲的現任妻子[白慧珍結婚](../Page/白慧珍.md "wikilink")，現育有一女。

## 演出作品

### 電視劇

  - 1996年：[MBC](../Page/韓國文化廣播公司.md "wikilink")《[蘋果樹香氣](../Page/蘋果樹香氣.md "wikilink")》
  - 1998年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[我心蕩漾](../Page/我心蕩漾.md "wikilink")》飾
    徐俊偉
  - 1998年：SBS《[等你求婚](../Page/等你求婚.md "wikilink")》
  - 2000年：MBC《[若我們相愛](../Page/若我們相愛.md "wikilink")》
  - 2004年：SBS《[巴黎戀人](../Page/巴黎戀人.md "wikilink")》飾 韓啟柱
  - 2007年：SBS《[錢的戰爭](../Page/錢的戰爭.md "wikilink")》飾 金國富
  - 2007年：SBS《[錢的戰爭（番外篇）](../Page/錢的戰爭#錢的戰爭（番外篇）.md "wikilink")》
  - 2008年：SBS《[風之畫師](../Page/風之畫師.md "wikilink")》飾
    [金弘道](../Page/金弘道.md "wikilink")
  - 2011年：SBS《[Sign](../Page/Sign_\(韓國電視劇\).md "wikilink")》飾 尹志勳
  - 2016年：[KBS](../Page/韓國放送公社.md "wikilink")《[鄰家律師趙德浩](../Page/鄰家律師趙德浩.md "wikilink")》飾
    趙德浩
  - 2019年：KBS《[鄰家律師趙德浩2：罪與罰](../Page/鄰家律師趙德浩2：罪與罰.md "wikilink")》飾 趙德浩

### 電影

  - 1996年：《[琉璃](../Page/琉璃_\(電影\).md "wikilink")》（Yuri）
  - 1997年：《[毒藥](../Page/毒藥_\(電影\).md "wikilink")》（Poison）
  - 1997年：《[仙人掌旅館](../Page/仙人掌旅館.md "wikilink")》（Motel Cactus）
  - 1997年：《[情書](../Page/情書_\(1997年電影\).md "wikilink")》（The Letter）
  - 1998年：《[約定](../Page/約定_\(電影\).md "wikilink")》（A Promise）
  - 1999年：《[白色情人節](../Page/白色情人節_\(電影\).md "wikilink")》（White
    Valentine，又譯：白鴿情書）
  - 2000年：《[雙生警賊](../Page/雙生警賊.md "wikilink")》（Kilimanjaro）
  - 2001年：《[純美的相遇](../Page/純美的相遇.md "wikilink")》（Indian Summer，又譯：印地安夏天）
  - 2001年：《[大佬鬥和尚](../Page/大佬鬥和尚.md "wikilink")》（Hi
    Dhrama，又譯：玩轉達摩、我的朋友很野蠻）
  - 2003年：《[四人餐桌](../Page/四人餐桌.md "wikilink")》（The Uninvited）
  - 2004年：《[首爾大劫案](../Page/首爾大劫案.md "wikilink")》（The Big
    Swindle，又譯：犯罪的重構）
  - 2007年：《[耀眼時刻](../Page/耀眼時刻.md "wikilink")》（Shiny Days）
  - 2012年：《[G計畫 Miss GO](../Page/G計畫_Miss_GO.md "wikilink")》（客串）
  - 2013年：《[老大靈不靈](../Page/老大靈不靈.md "wikilink")》（The Gangster Shaman）

## 獎項

  - 1996年：第17屆[青龍獎](../Page/青龍獎.md "wikilink")－最佳新人男演員獎（琉璃）
  - 1997年：第11屆基督教文化大賞－電影類獎
  - 1998年：第18屆影評獎－新人男演員獎、人氣演員獎（情書）
  - 1998年：第21屆－最高人氣獎
  - 1998年：第34屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－人氣獎（情書）
  - 1998年：第19屆[青龍獎](../Page/青龍獎.md "wikilink")－最佳男主角獎及人氣獎（約定）
  - 1999年：第7屆[春史電影賞](../Page/春史電影賞.md "wikilink")－男演員獎（約定）
  - 2004年：Brand Olympic Super Brand品牌奧林匹克超級品牌－男演員部門
  - 2004年：第5屆大韓民國影像大賞－電影演員類　最上鏡頭獎
  - 2004年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－大賞（與[金諪恩](../Page/金諪恩.md "wikilink")）及十大明星獎（巴黎戀人）
  - 2007年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－最高榮譽演技大賞及10大明星賞（錢的戰爭）
  - 2008年：第44屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－電視劇部門最佳男演員 （錢的戰爭）
  - 2016年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－男子最優秀演技賞（鄰家律師趙德浩）

## 外部連結

  - [韓國官方網站](https://web.archive.org/web/20140421064136/http://parkshinyang.com/)


  - [パク・シニャン公式ファンクラブ](http://park-shinyang.com/?m=pc&a=page_o_login)

  - [EPG](https://web.archive.org/web/20070829161650/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=1051)


  -
[P](../Page/category:韓國電視演員.md "wikilink")

[P](../Category/韓國電影演員.md "wikilink")
[P](../Category/東國大學校友.md "wikilink")
[P](../Category/首爾特別市出身人物.md "wikilink")
[P](../Category/朴姓.md "wikilink")
[P](../Category/韓國男演員.md "wikilink")