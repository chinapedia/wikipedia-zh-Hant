**朴海鎮**（，，，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")。2016年因出演電視劇《[奶酪陷阱](../Page/奶酪陷阱.md "wikilink")》中看似完美、內心卻神秘複雜的劉正學長而大受歡迎。

## 簡歷

朴海鎮出生於[釜山](../Page/釜山.md "wikilink")，高中念的是設計專業，畢業後在錄音帶工廠、酒吧等多處工作，而後又從事服飾批發。2004年前往[首爾賣衣服時](../Page/首爾.md "wikilink")，透過朋友介紹給前經紀公司的代表，開啟了他的演員之路。

在2006年憑藉《[家有七公主](../Page/家有七公主.md "wikilink")》正式出道，並以此劇在《[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")》中獲得新人獎，隨後又在[KBS日日劇](../Page/KBS.md "wikilink")《[比天高比地厚](../Page/比天高比地厚.md "wikilink")》中出演男主角，並憑此獲頒日日劇部門男子優秀演技獎。之後出演《[伊甸園之東](../Page/伊甸園之東.md "wikilink")》，首次挑戰反派角色，亦替他贏得了[MBC演技大賞新人獎](../Page/MBC演技大賞.md "wikilink")。2009年，他在[KBS週末連續劇](../Page/KBS週末連續劇.md "wikilink")《[熱血商人](../Page/熱血生意人.md "wikilink")》中擔任男主角，同時跨足藝能界，於《[家族的誕生](../Page/家族的誕生.md "wikilink")》第54期起成為固定成員，在節目裡玩遊戲時常常一直輸，而被家族稱為「不實青年」。

2010年起，朴海鎮開始接拍中國電視劇、在日本以歌手身份出道。2012年，朴海鎮睽違三年重返韓國電視圈，接拍《[我的女兒瑞英](../Page/我的女兒瑞英.md "wikilink")》。2013年底起，接連在《[來自星星的你](../Page/來自星星的你.md "wikilink")》、《[異鄉人醫生](../Page/異鄉人醫生.md "wikilink")》中擔任男二號。當中《[來自星星的你](../Page/來自星星的你.md "wikilink")》李輝京一角,
深情及長情一角, 贏得"國民守護君"的美譽。

2014年，《[壞傢伙們](../Page/壞小子們.md "wikilink")》中演出高智商天才連續殺人犯李正文一角。此劇也是朴海鎮演技上的轉捩點

2016年，朴海鎮因完美詮釋《[奶酪陷阱](../Page/奶酪陷阱.md "wikilink")》中擁有雙面性格的男主角劉正而使其演藝生涯攀向高峰。2017年，香港杜莎夫人蠟像館宣佈韓國男星朴海鎮的蠟像將於今年三月進駐本館並作三個月限定展出\[1\]。同年,
電視劇《[MAN TO
MAN](../Page/Man_to_Man.md "wikilink")》化身隱藏真實感情總是以不苟言笑的形象示人．擁有完美的外貌，高挑的身材與卓越的智力的保鑣。

2018年, 電影方面 朴海鎮再次完美重新在《[奶酪陷阱](../Page/奶酪陷阱.md "wikilink")》演繹劉正學長一角,
是很少數同一角色在電視劇及電影也是同一人演出。電影3月14日在韓國的白色情人節當日上映, 為了支持此電影的播映,
朴海鎮不同國家的粉絲們也聯合攜手安排在首爾明洞以朴海鎮名義進行包場活動提供給不同階層人士免費觀賞。

## 影視作品

### 電視劇

|                                  |                                                                       |                                                  |         |         |
| -------------------------------- | --------------------------------------------------------------------- | ------------------------------------------------ | ------- | ------- |
| **年份**                           | **電視台**                                                               | **劇名**                                           | **角色**  | **備註**  |
| 韓國                               |                                                                       |                                                  |         |         |
| 2006                             | [KBS](../Page/韓國放送公社.md "wikilink")                                   | 《[家有七公主](../Page/家有七公主.md "wikilink")》           | 年幼男     | 三大男主角之一 |
| 2007                             | 《[比天高比地厚](../Page/比天高比地厚.md "wikilink")》                              | 鄭武英                                              | 男主角     |         |
| 2008                             | [MBC](../Page/文化廣播_\(韓國\).md "wikilink")                              | 《[伊甸園之東](../Page/伊甸園之東_\(電視劇\).md "wikilink")》   | 申明勳     | 第三男主角   |
| 2009                             | KBS                                                                   | 《-{[熱血生意人](../Page/熱血生意人.md "wikilink")}-》       | 河柳      | 男主角     |
| 2012                             | 《[親愛的瑞英](../Page/我的女兒㥠榮.md "wikilink")》                               | 李相遇                                              | 三大男主角之一 |         |
| 2013                             | [SBS](../Page/SBS株式會社.md "wikilink")                                  | 《[來自星星的你](../Page/來自星星的你.md "wikilink")》         | 李輝京     | 第二男主角   |
| 2014                             | 《[異鄉人醫生](../Page/異鄉人醫生.md "wikilink")》                                | 韓在俊／李成勛                                          |         |         |
| [OCN](../Page/OCN.md "wikilink") | 《[壞傢伙們](../Page/壞小子們.md "wikilink")》                                  | 李正文                                              | 四大男主角之一 |         |
| 2016                             | [tvN](../Page/TVN.md "wikilink")                                      | 《[奶酪陷阱](../Page/奶酪陷阱.md "wikilink")》             | 劉正      | 男主角     |
| 2017                             | [JTBC](../Page/JTBC.md "wikilink")                                    | 《[Man to Man](../Page/Man_to_Man.md "wikilink")》 | 金蔎雨     |         |
| 2019                             |                                                                       | 《[Secret](../Page/Secret.md "wikilink")》         | 姜山赫     |         |
| 中國                               |                                                                       |                                                  |         |         |
| 2011                             | [湖南衛視](../Page/湖南衛視.md "wikilink")                                    | 《[錢多多嫁人記](../Page/錢多多嫁人記.md "wikilink")》         | 許飛      | 男主角     |
| 2012                             | 《[另一種燦爛生活](../Page/另一種燦爛生活.md "wikilink")》                            | 劉達明                                              | 两大男主角之一 |         |
| 2013                             | 西安電視台                                                                 | 《[戀愛相對論](../Page/戀愛相對論.md "wikilink")》           | 李昂      | 男主角     |
| 2016                             | [广东卫视](../Page/广东卫视.md "wikilink")／[东南卫视](../Page/东南卫视.md "wikilink") | 《[遠得要命的愛情](../Page/遠得要命的愛情.md "wikilink")》       | 沈岸      |         |
| 待播                               |                                                                       | 《[爆米花](../Page/爆米花_\(电视剧\).md "wikilink")》       | 陸信俊     |         |
|                                  | 《[男人帮·朋友](../Page/男人帮·朋友.md "wikilink")》                              | 趙海鵬                                              | 三大男主角之一 |         |
|                                  |                                                                       |                                                  |         |         |

### 網路劇

|        |                                                                                      |                                        |        |           |
| ------ | ------------------------------------------------------------------------------------ | -------------------------------------- | ------ | --------- |
| **年份** | **播放平台**                                                                             | **劇名**                                 | **角色** | **備註**    |
| 2016年  | [NAVER tvcast](../Page/NAVER.md "wikilink")、[YouTube](../Page/YouTube.md "wikilink") | 《[七次的初吻](../Page/七次的初吻.md "wikilink")》 | 朴海鎮    | 第三集－危險的上司 |

### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>上映日期</strong></p></td>
<td><p><strong>電影名稱</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2010年10月28日</p></td>
<td><p>《<a href="../Page/筷子.md" title="wikilink">筷子</a>》</p></td>
<td></td>
<td><p>特别主演</p></td>
</tr>
<tr class="odd">
<td><p>2015年1月8日</p></td>
<td><p>《<a href="../Page/雪海.md" title="wikilink">雪海</a>》</p></td>
<td><p>尚宇</p></td>
<td><p>男主角</p></td>
</tr>
<tr class="even">
<td><p>2018年3月14日<br />
(韓國上映)</p></td>
<td><p>《<a href="../Page/奶酪陷阱_(電影).md" title="wikilink">奶酪陷阱</a>》</p></td>
<td><p>劉正</p></td>
<td><p>男主角[2]</p></td>
</tr>
<tr class="odd">
<td><p>2018年7月14日<br />
(日本上映)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### MV

  - 2007年：The Way《愛...很痛》（與[許怡才](../Page/許怡才.md "wikilink")）

## 音樂專輯

  - 2011年2月9日 ：日本單曲《[命運之轍](../Page/命運之轍.md "wikilink")》

<!-- end list -->

1.  運命の轍 日文
2.  運命の轍 韓文
3.  ひゅるり
4.  運命の轍 伴唱

<!-- end list -->

  - 2011年4月11日 ：中國電視劇《[錢多多嫁人記](../Page/錢多多嫁人記.md "wikilink")》（片頭曲
    為了你我願意）
  - 2011年4月11日 ：中國電視劇《[錢多多嫁人記](../Page/錢多多嫁人記.md "wikilink")》（片尾曲
    一定要這樣嗎）

## 廣告

### 代言

  - 2006年：與-{[新慜娥](../Page/新慜娥.md "wikilink")}- X'mas
    [Baskin-Robbins](../Page/Baskin-Robbins.md "wikilink") IceCream
  - 2007年：[LOTTE](../Page/LOTTE.md "wikilink")
  - 2007年：SIEG FAHRENHEIT 服飾
  - 2009年：[韓國米酒](../Page/韓國.md "wikilink")
  - 2010年：[Giordano](../Page/Giordano.md "wikilink") Essential
    Jeans日常褲系列
  - 2014年：[Dior](../Page/Dior.md "wikilink") Homme 服飾
  - 2014年：中國味千拉麵
  - 2014年：Calvin Klein Jeans
  - 2015年：LOTTE 樂天免稅店
  - 2016年：BEANPOLE ACCESSORY 包包系列
  - 2016年：Centerpole
  - 2016年：JAYJUN COSMETIC
  - 2016年: MIND BRIDGE 服飾(中國)
  - 2017年：Centerpole
  - 2017年：JAYJUN COSMETIC
  - 2017年: MIND BRIDGE 服飾(中國)
  - 2018年：JAYJUN COSMETIC
  - 2018年: MIND BRIDGE 服飾(中國,韓國)

### 形象大使

  - 2007年：[首爾青少年禁菸酒宣傳大使](../Page/首爾.md "wikilink")
  - 2008年：[韓國教育科學技術部學分銀行形象宣傳大使](../Page/韓國.md "wikilink")
  - 2009年：韓國觀光公社宣傳[韓國](../Page/韓國.md "wikilink")（與[朴恩惠](../Page/朴恩惠.md "wikilink")）
  - 2013年：中國TGC文化基金的宣傳大使
  - 2014年：中國「大地之愛‧母親水窖」專項基金宣傳大使
  - 2014年：[首爾龍山區稅務所一日名譽公益室長](../Page/首爾.md "wikilink")

## 綜藝節目

### 韓國

  - [SBS](../Page/SBS_\(韓國\).md "wikilink")《夜心萬萬》：2007年1月15日
  - 《準備好了》：2007年5月6日第一期
  - SBS《-{[家族的誕生](../Page/家族的誕生.md "wikilink")}-》第一季：2008年7月13日、7月20日第5、6集嘉賓
  - [tvN](../Page/TVN.md "wikilink")《[現場脫口秀
    Taxi](../Page/現場脫口秀_Taxi.md "wikilink")》：2009年5月7日
  - SBS《-{家族的誕生}-》第一季：2009年7月5日第54集加入為固定主持
  - [KBS2](../Page/KBS2.md "wikilink")《[Happy
    Together](../Page/歡樂在一起.md "wikilink")》：2009年10月8日
  - [MBC](../Page/MBC.md "wikilink")《美好的一天》：2011年1月11日
  - KBS2《Happy Together》：2012年11月29日
  - SBS《[深夜的TV演藝](../Page/深夜的TV演藝.md "wikilink")》：2014年2月12日
  - SBS《深夜的TV演藝》：2014年5月8日
  - SBS《深夜的TV演藝》：2016年3月2日
  - [KBS](../Page/KBS.md "wikilink")《[演藝家中介](../Page/演藝家中介.md "wikilink")》：2016年3月5日
  - KBS《演藝家中介》：2016年4月2日
  - [JTBC](../Page/JTBC.md "wikilink")《[非首腦會談](../Page/非首腦會談.md "wikilink")》：2016年11月7日
  - JTBC《[請給一頓飯Show](../Page/請給一頓飯Show.md "wikilink")》：2017年5月31日
  - KBS《演藝家中介》：2018年3月16日
  - [Netflix](../Page/Netflix.md "wikilink") 《Busted\!
    [明星來解謎](../Page/明星來解謎.md "wikilink") 》EP5：2018年5月18日
    飾演 島嶼村落的醫生

### 中國

  - 《背後的故事》
  - 《[天天向上](../Page/天天向上.md "wikilink")》：2009年4月20日
  - 《金鷹訪談》：2009年4月19日
  - 《新浪採訪》：2009年4月19日
  - 《搜狐新韓線》：2009年4月20日
  - 《挑戰麥克風》：2009年4月20日
  - 《樂透新聞薄》：2009年4月20日
  - 《跳躍吧菜鳥》：2012年4月1日
  - 《[快樂大本營](../Page/快樂大本營.md "wikilink")》：2014年3月22日
  - 《[快樂大本營](../Page/快樂大本營.md "wikilink")》：2016年4月2日

## 獎項

### 大型頒獎禮獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006</p></td>
<td><ul>
<li>第20屆<a href="../Page/KBS演技大賞.md" title="wikilink">KBS演技大賞</a>－男子新人獎、最佳情侶賞（與<a href="../Page/李泰蘭.md" title="wikilink">李泰蘭</a>）《<a href="../Page/家有七公主.md" title="wikilink">家有七公主</a>》</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><ul>
<li>第43屆<a href="../Page/百想藝術大賞.md" title="wikilink">百想藝術大賞</a>－電視部門最佳男子新人賞《<a href="../Page/家有七公主.md" title="wikilink">家有七公主</a>》</li>
<li>第21屆KBS演技大賞－男子优秀演技賞、最佳情侶賞（與<a href="../Page/韓孝周.md" title="wikilink">韓孝周</a>）《<a href="../Page/比天高比地厚.md" title="wikilink">比天高比地厚</a>》</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><ul>
<li><a href="../Page/MBC演技大賞.md" title="wikilink">MBC演技大賞</a>－男子新人賞《<a href="../Page/伊甸園之東.md" title="wikilink">伊甸園之東</a>》</li>
<li><a href="../Page/SBS演技大賞.md" title="wikilink">SBS演技大賞</a>－最佳合作奖《<a href="../Page/家族诞生.md" title="wikilink">家族诞生</a>》</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><ul>
<li>亞洲模特頒獎典禮－人氣明星獎</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><ul>
<li>LETV電影電視劇頒獎典禮－亞洲最佳明星獎</li>
<li>乐视盛典－亚太地区电视剧最具人气演員獎《多多的婚事》</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><ul>
<li>亞洲模特兒獎頒獎典禮－亞洲特別獎</li>
<li>第7屆<a href="../Page/Style_Icon_Asia.md" title="wikilink">-{Style Icon Awards}-</a>－時尚指標賞、K-STYLE奖</li>
<li>第4届明星公民盛典－演员公民公益大奖</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><ul>
<li>第8屆<a href="../Page/韓國電視劇節.md" title="wikilink">韓國電視劇節</a>－KDA獎《<a href="../Page/壞傢伙們.md" title="wikilink">壞傢伙們</a>》</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><ul>
<li>韩国有线电视台－最佳演技奖《<a href="../Page/奶酪陷阱.md" title="wikilink">奶酪陷阱</a>》</li>
<li>樂視生態共享之夜－年度突破男演员獎</li>
<li>第12屆亞洲模特頒獎典禮－亞洲明星獎</li>
<li>第1屆<a href="../Page/Asia_Artist_Awards.md" title="wikilink">Asia Artist Awards</a>－Best Artist賞《奶酪陷阱》</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><ul>
<li>首爾社會福利大會－首爾市長獎</li>
<li>第2屆<a href="../Page/Asia_Artist_Awards.md" title="wikilink">Asia Artist Awards</a>－Best Artist賞</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EB%B0%95%ED%95%B4%EC%A7%84&sm=tab_txc&ie=utf8&key=PeopleService&os=121837)

  -
  -
[P](../Category/韓國電視演員.md "wikilink")
[P](../Category/韓國男性模特兒.md "wikilink")
[P](../Category/釜山廣域市出身人物.md "wikilink")
[P](../Category/朴姓.md "wikilink") [P](../Category/左撇子.md "wikilink")

1.  [三個月限定版！朴海鎮的蠟像進駐香港蠟像館！](http://www.vlovekpop.com/20170207-park-haejin/)
    vlovekpop
2.  [想念劉正學長嗎？朴海鎮正式
    確定加盟《捕鼠器裡的乳酪》電影版！](http://www.vlovekpop.com/20160526-cheese/)
    vlovekpop