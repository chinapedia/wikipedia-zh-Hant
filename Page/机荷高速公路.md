**机荷高速公路**是[深圳境内的一条高速公路](../Page/深圳.md "wikilink")，呈东西走向，屬於[中国国家高速公路网中的](../Page/中国国家高速公路网.md "wikilink")[沈海高速公路的一部分](../Page/沈海高速公路.md "wikilink")，(编号：**G15**)。

起于[深圳宝安国际机场](../Page/深圳宝安国际机场.md "wikilink")，终于[龙岗区](../Page/龙岗区.md "wikilink")[横岗街道荷坳村](../Page/横岗街道.md "wikilink")，全长44.31公里，设计时速100公里。

工程分东、西两段建设，东段从福民至荷坳，于1995年10月开工，于1997年10月31日通车，西段从机场至福民，于1999年5月通车。

机荷高速公路东连[惠盐高速公路](../Page/惠盐高速公路.md "wikilink")，与[梅观高速公路互通](../Page/梅观高速公路.md "wikilink")，西端与[广深高速公路和](../Page/广深高速公路.md "wikilink")[107国道相接](../Page/107国道.md "wikilink")，将[广深高速公路](../Page/广深高速公路.md "wikilink")、[107国道](../Page/107国道.md "wikilink")、[深汕高速公路](../Page/深汕高速公路.md "wikilink")、[惠盐高速公路和](../Page/惠盐高速公路.md "wikilink")[205国道连成一体](../Page/205国道.md "wikilink")，形成一条在[汕头](../Page/汕头.md "wikilink")、[惠州地区与](../Page/惠州.md "wikilink")[广州](../Page/广州.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[东莞和](../Page/东莞.md "wikilink")[香港之间的交通大动脉](../Page/香港.md "wikilink")。
[Jihe_Expressway.jpg](https://zh.wikipedia.org/wiki/File:Jihe_Expressway.jpg "fig:Jihe_Expressway.jpg")

## 互通枢纽及服务设施

## 注释

[Category:深圳市高速公路](../Category/深圳市高速公路.md "wikilink")