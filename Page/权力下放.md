**权力下放**（devolution）是一种组织技术，旨在于同一个[法人内部](../Page/法人.md "wikilink")，由[中央政府](../Page/中央政府.md "wikilink")[行政機關向其下放部门分配人员和职能](../Page/行政機關.md "wikilink")。原则上由[單一制](../Page/單一制.md "wikilink")[国家使用](../Page/国家.md "wikilink")，如[法国](../Page/法国.md "wikilink")1852年3月28日条例的动机解释部分所说：“我们可以从远处管理得同近处一样好”。它是[权力分散的一个对立和完善](../Page/权力分散.md "wikilink")。

下放的人员和职能由他们的上级负责，等级控制施加于人员（任命、纪律等）以及他们的行为（上级可以取消下属的决定、向他们发布指令或是修改他们的决定）。权力下放后的机关仍然维持一个机构的完整性，使得法人能够介入管理行为中。

因此，它提供国家以更高效和快速的反应能力，如法国十九世纪政治家奥迪隆·巴罗（Odilon
Barrot）所形象化的：“这是用同一把锤子捶，但是我们缩短了它的柄。”

## 应用

### 法国

在[法国](../Page/法国.md "wikilink")，权力下放政策被非常有效地推广，95%的行政人员服务于下放机关，占总经费的三分之二。

它在地理范围内行使：

  - [大区](../Page/大区_\(法国\).md "wikilink")，région 一般法的施行界限
  - [省](../Page/省_\(法国\).md "wikilink") département
  - [区](../Page/区_\(法国\).md "wikilink") arrondissement
  - [乡](../Page/乡_\(法国\).md "wikilink") canton
  - [市镇](../Page/市镇_\(法国\).md "wikilink") municipalité

它同时也在各个领域中施行，如[上诉法院](../Page/上诉法院.md "wikilink")、各学会以及军区。

权力下放的实现在法国由《权力下放宪章》（**）提出的原则所指导，如1992年2月6日法的补充原则规定了下放的权力机关拥有一般法的权能。

### 英國

在[英国](../Page/英国.md "wikilink")，除了位于[伦敦的](../Page/伦敦.md "wikilink")[英国国会外](../Page/英国国会.md "wikilink")，现在[苏格兰](../Page/苏格兰.md "wikilink")、[威尔斯和](../Page/威尔斯.md "wikilink")[北爱尔兰都拥有自己的](../Page/北爱尔兰.md "wikilink")[议会](../Page/议会.md "wikilink")，这些议会中的部分议院是按照[比例代表制选举产生的](../Page/比例代表制.md "wikilink")。虽然这些二级政府拥有部分立法等方面的权限，他们的权力还是无法足够大到能与英国议会抗衡。三个地方议会之间的权限大小也各不相同。例如，[苏格兰议会拥有](../Page/苏格兰议会.md "wikilink")[立法权](../Page/立法权.md "wikilink")（但必需獲得中央政府同意才可以舉行公投），而[威尔斯议会政府则只能决定具体如何使用由中央政府分配给威尔斯的](../Page/威尔斯议会.md "wikilink")[预算](../Page/预算.md "wikilink")。此外，这些议会的存在不受保障，它们的具体权责范围也可由英国国会决定增大或缩小，国会甚至可以單方面取消地方自治的權力。

因此，英国被认为是一个权力下放的[单一制国家](../Page/单一制.md "wikilink")。这与[联邦制国家是不同的](../Page/联邦制.md "wikilink")。在联邦制国家中，邦或州议会的地位得到宪法承认，其权限范围和职责都有明确保障，聯邦议会无权随意剥夺这些权力。

英国政府目前的政策是继续下放中央权力。在未来的几年中，英格兰部分地区的民众将举行[全民公决](../Page/全民公决.md "wikilink")，以决定是否由民主选举产生地方政府领导人。

權力下放政策一直受到爭議，包括被認為危及英國的統一和完整性，而英國自1999年的权力下放政策，被認為加劇[分離主義的勢力](../Page/分離主義.md "wikilink")，例如主張[蘇格蘭獨立的](../Page/蘇格蘭獨立.md "wikilink")[蘇格蘭民族黨](../Page/蘇格蘭民族黨.md "wikilink")，便在2007年取代長期維持優勢的[工黨](../Page/工黨_\(英國\).md "wikilink")，執掌蘇格蘭至今，更推動[2014年蘇格蘭獨立公投](../Page/2014年蘇格蘭獨立公投.md "wikilink")，不過因為結果為55%反對蘇格蘭從英國獨立，而使蘇格蘭民族黨目標失敗，[2015年英國大選後](../Page/2015年英國大選.md "wikilink")，蘇格蘭民族黨取代工黨，取得絕大部分的蘇格蘭席次（59席中的56席），2016年苏格兰议会選舉，蘇格蘭民族黨議席減少而再度組建少數政府，[2017年英國大選](../Page/2017年英國大選.md "wikilink")，蘇格蘭民族黨減少至35席，但仍是蘇格蘭第一大黨。\[1\]

威爾斯方面，自1999年以來，工黨一直執掌地方政府。

北爱尔兰方面，根據[貝爾法斯特協議](../Page/貝爾法斯特協議.md "wikilink")，由統一派（跟愛爾蘭統一）及聯合派（維持與英國組成聯合王國的現狀）四大政黨組成大聯合政府，並且實施與英國本土有所區別的政治與法律制度，英國僅控制財政預算、防務和司法及執法權及「每年一度的各政治和宗教派別遊行」，北愛政府擁有大部分自治權力。但因為北愛局勢，国会多次接管北愛的权力。

## 參見

  - [地方自治](../Page/地方自治.md "wikilink")
  - [地方分权](../Page/地方分权.md "wikilink")
  - [联邦主义](../Page/联邦主义.md "wikilink")
  - [中華聯邦主義](../Page/中華聯邦主義.md "wikilink");
    [条块](../Page/条块.md "wikilink")
  - [澳大利亚行政区划](../Page/澳大利亚行政区划.md "wikilink")
  - [辅助性原则](../Page/辅助性原则.md "wikilink")
  - [美国领地](../Page/美国领地.md "wikilink")
  - [西洛錫安問題](../Page/西洛錫安問題.md "wikilink")

## 註解

## 外部鏈接

  - [Economic and Social Research Council Devolution and Constitutional
    Change research
    programme](https://web.archive.org/web/20060512141837/http://www.devolution.ac.uk/)
  - [An Article from the BBC describing the transfer of powers from the
    UK Parliament to the Welsh
    Assembly](http://www.bbc.co.uk/news/uk-wales-politics-12648649%7Chttp://www.bbc.co.uk)

[Category:地方自治](../Category/地方自治.md "wikilink")
[Category:政治術語](../Category/政治術語.md "wikilink")

1.