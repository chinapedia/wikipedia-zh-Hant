**李·马丁**（，），是一名[英格兰](../Page/英格兰.md "wikilink")[职业](../Page/职业.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，於2009年夏季由[曼联轉投](../Page/曼联.md "wikilink")[英冠球會](../Page/英冠.md "wikilink")[葉士域治](../Page/伊普斯维奇城足球俱乐部.md "wikilink")。

## 生平

  - 曼聯

2003年，曼联青年队从陷入财政困难的[温布尔顿签入马丁](../Page/溫布頓足球會.md "wikilink")，费用估计在100万[英镑](../Page/英镑.md "wikilink")。2006年1月，马丁被租借到[比利时](../Page/比利时.md "wikilink")[皇家安特卫普](../Page/皇家安特卫普足球俱乐部.md "wikilink")，在短短四个月内凭借出色表现在5月获得了俱乐部赛季最佳球员的奖项。

2006年8月，马丁租借到[苏格兰](../Page/苏格兰.md "wikilink")[格拉斯哥流浪](../Page/流浪者足球俱乐部.md "wikilink")<small>\[1\]</small>，开始表现极为出色，但随后不幸连续受伤，最终没有坐稳主力位置。2007年初被租借到[斯托克城队](../Page/斯托克城足球俱乐部.md "wikilink")<small>\[2\]</small>，3月10日在對[修咸頓時取得個人首個入球](../Page/南安普顿足球俱乐部.md "wikilink")<small>\[3\]</small>。

2007/08年赛季，马丁季前隨隊參加「遠東之旅」，在對[广州医药時射入一球](../Page/广州医药足球俱乐部.md "wikilink")，季初更被列入曼联一线阵容，争取第一次代表曼联出场的机会。隨著曼聯在[英格蘭聯賽盃出局](../Page/英格蘭聯賽盃.md "wikilink")，曼聯將部分年青球員外借以爭取出場經驗，2007年10月5日马丁被外借到[普利茅斯](../Page/普利茅斯足球俱乐部.md "wikilink")3個月<small>\[4\]</small>。借用期滿後再被外借到另一支[英冠球隊](../Page/英格蘭足球冠軍聯賽.md "wikilink")[錫菲聯直到季尾](../Page/谢菲尔德联足球俱乐部.md "wikilink")<small>\[5\]</small>，但因傷而僅獲9場上陣機會。

2008/09年曼聯季前的「南非之旅」，马丁亦有隨行，在[沃達丰挑戰賽](../Page/沃達丰.md "wikilink")（Vodacom
Challenge）射入決勝球1-0擊敗奧蘭多海盜（Orlando Pirates）。

2008年8月13日李·馬田再被外借到新升班[英冠的](../Page/英格蘭足球冠軍聯賽.md "wikilink")[諾定咸森林](../Page/诺丁汉森林足球俱乐部.md "wikilink")<small>\[6\]</small>，原定為期1個月，其後延續到12月31日<small>\[7\]</small>。

  - 葉士域治

2009年7月6日由前曼聯[隊長](../Page/隊長_\(足球\).md "wikilink")[堅尼帶領的](../Page/堅尼.md "wikilink")[英冠球會](../Page/英冠.md "wikilink")[葉士域治羅致李](../Page/伊普斯维奇城足球俱乐部.md "wikilink")·馬田，轉會費沒有透露，簽約四年<small>\[8\]</small>。但發展並不理想，於2010年8月6日被借用到[英甲球會](../Page/英甲.md "wikilink")[查爾頓一整個球季](../Page/查尔顿竞技足球俱乐部.md "wikilink")<small>\[9\]</small>，上半季上陣20場及射入兩球。2011年1月20日新任葉士域治領隊[-{zh-hans:保罗·杰维尔;
zh-hk:保羅·祖維爾;}-](../Page/保羅·祖維爾.md "wikilink")（Paul
Jewell）行使24小時召回條款提前召回马丁\[10\]。

## 榮譽

  - 曼聯

<!-- end list -->

  - [英格蘭社區盾](../Page/英格蘭社區盾.md "wikilink")：2007年；

## 參考資料

## 外部連結

  -

  - [曼聯官網簡歷](http://www.manutd.com/default.sps?pagegid=%7B586F3BDA-BAD0-484E-AA1B-C0464DF06DF7%7D&section=playerProfile&teamid=435&bioid=91994)

[Category:英格兰足球运动员](../Category/英格兰足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:曼联球员](../Category/曼联球员.md "wikilink")
[Category:格拉斯哥流浪球員](../Category/格拉斯哥流浪球員.md "wikilink")
[Category:史篤城球員](../Category/史篤城球員.md "wikilink")
[Category:普利茅夫球員](../Category/普利茅夫球員.md "wikilink")
[Category:錫菲聯球員](../Category/錫菲聯球員.md "wikilink")
[Category:諾定咸森林球員](../Category/諾定咸森林球員.md "wikilink")
[Category:葉士域治球員](../Category/葉士域治球員.md "wikilink")
[Category:查爾頓球員](../Category/查爾頓球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")

1.  [Man Utd's Martin moves to
    Rangers](http://news.bbc.co.uk/sport2/hi/football/teams/r/rangers/4783397.stm)
2.  [Stoke sign Man Utd winger on
    loan](http://news.bbc.co.uk/sport2/hi/football/teams/s/stoke_city/6300711.stm)
3.  [Stoke 2-1
    Southampton](http://news.bbc.co.uk/sport2/hi/football/eng_div_1/6415091.stm)
4.  [Plymouth wrap up Martin loan
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/p/plymouth_argyle/7029837.stm)
5.  [Blades Secure loan
    winger](http://www.sufc.premiumtv.co.uk/page/NewsDetail/0,,10418~1211392,00.html)

6.   [Forest complete Martin loan
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/n/nottm_forest/7559302.stm)，2008年8月13日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2008年12月30日查閱
7.
8.
9.
10.