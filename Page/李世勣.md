**李世勣**（，“勣”是“绩”的[异体字](../Page/异体字.md "wikilink")），原名**徐世勣**，或作**世績**，字**懋功**，亦作**茂功**。[唐高祖李渊赐其姓](../Page/唐高祖.md "wikilink")[李](../Page/李姓.md "wikilink")，后避[唐太宗李世民](../Page/唐太宗.md "wikilink")[讳改名为](../Page/避讳.md "wikilink")**李勣**。[曹州离狐](../Page/曹州.md "wikilink")（今[山东](../Page/山东.md "wikilink")[菏泽](../Page/菏泽.md "wikilink")[东明县东南](../Page/东明县.md "wikilink")）人，[唐初名将](../Page/唐朝.md "wikilink")，曾破[东突厥](../Page/东突厥.md "wikilink")、[高句丽](../Page/高句丽.md "wikilink")，与[李靖并称](../Page/李靖.md "wikilink")。歷事[唐高祖](../Page/唐高祖.md "wikilink")、[唐太宗](../Page/唐太宗.md "wikilink")、[唐高宗三朝](../Page/唐高宗.md "wikilink")，深得朝廷信任和重任，朝廷倚之為長城。

## 生平

徐世勣出身富户，大业七年（611年）17岁时正值[隋末农民起事](../Page/隋末农民战争.md "wikilink")，跟随[翟让参加](../Page/翟让.md "wikilink")[瓦岗军](../Page/瓦岗军.md "wikilink")。在[李密成为瓦岗军的首领后](../Page/李密.md "wikilink")，徐世勣因军功被封为东海郡公，驻守[黎阳](../Page/黎阳.md "wikilink")。在李密谋害[翟让时](../Page/翟让.md "wikilink")，被乱军砍伤，后被[王伯当所救](../Page/王伯当.md "wikilink")。[大业十四年](../Page/大业.md "wikilink")（618年）击退了北上的[宇文化及](../Page/宇文化及.md "wikilink")。李密王世充决战后，随李密归降唐朝，守黎阳，被唐高祖诏授黎阳总管，李密在黎陽起事，[徐世勣献黎阳投降](../Page/徐世勣.md "wikilink")，请求收葬李密的尸首，得到李渊的允许。后跟随秦王李世民先后讨伐[刘武周](../Page/刘武周.md "wikilink")、[王世充](../Page/王世充.md "wikilink")、[窦建德](../Page/窦建德.md "wikilink")、[刘黑闼和](../Page/刘黑闼.md "wikilink")[徐圆朗等割据势力](../Page/徐圆朗.md "wikilink")，[武德六年](../Page/武德.md "wikilink")（623年），又跟随赵郡王[李孝恭和李靖讨伐](../Page/李孝恭.md "wikilink")[辅公祏](../Page/辅公祏.md "wikilink")。

入唐后，[唐高祖李渊说徐世勣是](../Page/唐高祖.md "wikilink")“纯臣”，赐他姓李。先封**曹国公**，後封**英国公**。贞观年间，曾参与平定[东突厥](../Page/东突厥.md "wikilink")、[薛延陀](../Page/薛延陀.md "wikilink")、[高句丽的战役](../Page/高句丽.md "wikilink")。[武德八年](../Page/武德.md "wikilink")（625年）[突厥進犯](../Page/突厥.md "wikilink")，李世勣討伐。[貞觀三年](../Page/貞觀.md "wikilink")（629年）大破突厥，俘五萬人而歸，鎮守[并州十六年](../Page/并州.md "wikilink")。（参见[唐与突厥的战争](../Page/唐与突厥的战争.md "wikilink")。）貞觀十五年（641年），在诺真水（今[内蒙古](../Page/内蒙古.md "wikilink")[乌兰察布](../Page/乌兰察布.md "wikilink")）[大败薛延陀](../Page/唐击薛延陀之战.md "wikilink")。貞觀十八年（644年）隨[太宗親征高句麗](../Page/唐太宗征讨高句丽.md "wikilink")。

唐太宗晚年，曾對太子[李治說](../Page/李治.md "wikilink")：“李勣重視恩義，你無恩於他，恐怕日后他無法尽心辅佐你。朕先將他外放，等朕死你登基後，你再將他召回，受以僕射，如此一來，你就有恩於他，他就會對你誓死效忠。”\[1\]为了使李勣忠于李治，唐太宗将他贬为[叠州](../Page/叠州.md "wikilink")（今[甘肃](../Page/甘肃.md "wikilink")[迭部](../Page/迭部.md "wikilink")）都督。[唐高宗即位后根据唐太宗的遗命恢复了他的职位](../Page/唐高宗.md "wikilink")，历任[尚书左仆射](../Page/尚书左仆射.md "wikilink")、[司空](../Page/司空.md "wikilink")。高宗問他：“朕欲立[武昭儀為](../Page/武则天.md "wikilink")[皇后](../Page/皇后.md "wikilink")，你覺得如何？”李勣說道：“此陛下家事，何必要問外人。”\[2\][總章元年](../Page/總章.md "wikilink")（668年），李勣率兵至[鴨綠江](../Page/鴨綠江.md "wikilink")，與[薛仁貴會於](../Page/薛仁貴.md "wikilink")[平壤](../Page/平壤.md "wikilink")，攻克平壤，大胜而歸。（参见[唐与高句丽的战争](../Page/唐与高句丽的战争.md "wikilink")。）總章二年（669年）[十二月初三戊申日](../Page/十二月初三.md "wikilink")，李勣卒，享年七十六歲，唐高宗辍朝七日，赠李勣[太尉](../Page/太尉.md "wikilink")、[扬州](../Page/扬州.md "wikilink")[大都督](../Page/大都督.md "wikilink")，[谥号](../Page/谥号.md "wikilink")**貞武**，陪葬[昭陵](../Page/唐昭陵.md "wikilink")。因后来其孙[徐敬业反对武则天](../Page/徐敬业.md "wikilink")，發動[起事](../Page/起事.md "wikilink")，被[族誅](../Page/族誅.md "wikilink")，李勣也被剖棺戮尸。到後來[唐中宗以](../Page/唐中宗.md "wikilink")[神龍之變](../Page/神龍之變.md "wikilink")[复辟之後才平反昭雪](../Page/复辟.md "wikilink")，以礼改葬\[3\]。

## 家族

### 父

  - 徐盖，字广济，舒国节公\[4\]

### 妻

  - 薛氏，封一品大夫人，英國夫人

### 兄弟姐妹

  - 姐：李某（墓志铭阙字）（隋开皇十一年（591年）—唐显庆二年七月三十日（657年9月13日）），字总持，嫁唐[临清县令琅邪王某](../Page/临清.md "wikilink")
  - 姐：李氏，东平郡君
  - 弟：李弼
  - 弟：李感（隋仁寿四年（604年）—唐武德元年（618年））

### 子孙

  - 长子[李震](../Page/李震.md "wikilink")（617年—665年），字景阳，永徽四年（653年）任[泽州刺史](../Page/泽州.md "wikilink")，显庆二年（657年）转[赵州刺史](../Page/赵州_\(北齐\).md "wikilink")，龙朔二年（662年）任[梓州刺史](../Page/梓州.md "wikilink")，麟德二年三月卒于梓州（在今四川三台县潼川镇），年四十九。谥号定。妻王氏
      - 孙：[李敬业](../Page/李敬业.md "wikilink")，袭英国公，684年起兵反武，兵败被杀
      - 孙：[李敬猷](../Page/李敬猷.md "wikilink")，盩厔令
      - 孙：[李敬真](../Page/李敬真.md "wikilink")
      - 孙：[李思顺](../Page/李思顺.md "wikilink")，字知通，鸿胪寺卿
          - 曾孙：[徐湘](../Page/徐湘.md "wikilink")，字汉津，[寿州刺史](../Page/寿州_\(唐朝\).md "wikilink")
              - 玄孙：[徐弘光](../Page/徐弘光.md "wikilink")，字大明，岐王傅
  - 次子[李思文](../Page/李思文.md "wikilink")，[武则天临朝时被赐姓武氏](../Page/武则天.md "wikilink")
      - 孙：[武钦载](../Page/武钦载.md "wikilink")（665年—679年），字景初，李思文之子，官任岚、饶、润等州刺史，再除太仆少卿，兼知陇西事。又加银青光禄大夫、上柱国、卫县开国公、检校并州大都督府长史、清源道总管，除冀州刺史。调露元年八月四日（679年9月14日）卒于陇西大使之馆，春秋一十有五。垂拱四年（688年）改葬洛阳北邙
      - 孙女：李宝上座，第三女，前夫龙门公之孙、[司农卿王弘福次子王勗](../Page/司农卿.md "wikilink")，后夫中书侍郎[温彦将孙](../Page/温彦将.md "wikilink")、[易州司马温瓒第三子](../Page/易州.md "wikilink")[潞州](../Page/潞州.md "wikilink")[屯留县令温炜](../Page/屯留.md "wikilink")
  - 女，贞观元年（627年）卒
  - 女，嫁[杜怀恭](../Page/杜怀恭.md "wikilink")

## 徐茂公

在《[隋唐演义](../Page/隋唐演义.md "wikilink")》和《[说唐](../Page/说唐.md "wikilink")》中，徐世勣被演繹成[瓦岗寨的](../Page/瓦岗军.md "wikilink")[军师](../Page/军师.md "wikilink")，[诸葛亮一类的](../Page/诸葛亮.md "wikilink")[半仙级人物徐茂公](../Page/半仙.md "wikilink")（又作徐茂功）。

## 李勣碑与墓志铭

李勣碑立于[唐高宗仪凤二年十月六日](../Page/唐高宗.md "wikilink")（677年11月6日），现仍竖立于李勣墓前。碑身高5.70米，宽1.80米，厚54厘米。碑额[篆书](../Page/篆书.md "wikilink")，题《大唐故司空上柱国赠太尉英贞武公碑》十六字，唐高宗[李治撰文并书写](../Page/李治.md "wikilink")，书法为行草体，共31行，每行110余字不等，碑题下“御制御书”四字及文末年款为正书。碑下截部分錾损严重，其余较清晰。碑阴刻宋[游师雄元祐四年](../Page/游师雄.md "wikilink")（1089年）的题记。

李勣墓志铭，1971年出土于陕西醴泉县烟霞乡烟霞新村西约200米处李勣墓中，墓志盖厚15厘米，底边长86厘米，盖面篆书《大唐故司空公太子太师赠太尉扬州大都督上柱国英国公李公墓志之铭》。志石边长82厘米，厚17厘米，[刘祎之奉敕撰文](../Page/刘祎之.md "wikilink")，正书55行，满行54字。

## 注釋

<references/>

## 相关条目

  - [隋末民变](../Page/隋末民变.md "wikilink")
  - [瓦岗军](../Page/瓦岗军.md "wikilink")
  - [虎牢之戰](../Page/虎牢之戰.md "wikilink")
  - [唐与突厥的战争](../Page/唐与突厥的战争.md "wikilink")
  - [唐击薛延陀之战](../Page/唐击薛延陀之战.md "wikilink")
  - [唐太宗征讨高句丽](../Page/唐太宗征讨高句丽.md "wikilink")
  - [唐与高句丽的战争](../Page/唐与高句丽的战争.md "wikilink")

[Category:隋朝民变人物](../Category/隋朝民变人物.md "wikilink")
[Category:唐朝軍事人物](../Category/唐朝軍事人物.md "wikilink")
[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")
[Category:唐朝國公](../Category/唐朝國公.md "wikilink")
[Category:唐朝三公](../Category/唐朝三公.md "wikilink")
[X徐](../Category/唐朝赐李姓者.md "wikilink")
[Category:東明人](../Category/東明人.md "wikilink")
[S世勣](../Category/徐姓.md "wikilink")
[Category:諡貞武](../Category/諡貞武.md "wikilink")
[Category:唐朝尚书左仆射](../Category/唐朝尚书左仆射.md "wikilink")
[Category:凌烟阁二十四功臣](../Category/凌烟阁二十四功臣.md "wikilink")

1.  《[旧唐书](../Page/旧唐书.md "wikilink")》李勣传：“二十三年，太宗寝疾，谓高宗曰：“汝于李勣无恩，我今将责出之。我死后，汝当授以仆射，即荷汝恩，必致其死力。”乃出为叠州都督。”
    《[新唐书](../Page/新唐书.md "wikilink")》李勣传：“帝疾，谓太子曰：“尔于勣无恩，今以事出之，我死，宜即授以仆射，彼必致死力矣！”乃授叠州都督。”
2.  見《资治通鉴》卷一九九，永徽六年九月。清初乾隆對這段史實表示：“唐太宗之待臣下，未尝不任权术，以至于后来‘陛下家事’一语，贻留宗庙社稷之祸，究之权术有例益乎？”
3.  公元705年，[唐中宗认为](../Page/唐中宗.md "wikilink")：「李世勣有功於國，如果因為其孫[敬業反对武则天而被追削官爵](../Page/李敬業.md "wikilink")、剖棺戮屍的話，那是不公平的。所以朕下令：追复李世勣官爵，以礼改葬。」
4.  李勣姐李总持墓志铭称父舒国节公徐康，祖父徐盖。