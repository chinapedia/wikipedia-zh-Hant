**李丞涓**（，），[韓國](../Page/大韓民國.md "wikilink")[女演員兼](../Page/女演員.md "wikilink")[模特兒](../Page/模特.md "wikilink")。幼年在首爾求學，1989年在[仁川的](../Page/仁川.md "wikilink")[仁荷專業大學航空運航學系畢業](../Page/仁荷專業大學.md "wikilink")，並加入[大韓航空成為](../Page/大韓航空.md "wikilink")[空中小姐](../Page/空中小姐.md "wikilink")。

1992年參加「美韓國小姐」（미스코리아
미）選拔賽得獎，並晉身演藝界。1993年於[MBC開始為電視台拍攝電視劇](../Page/MBC.md "wikilink")，後來成為[KBS的藝員](../Page/KBS.md "wikilink")。曾在電影《[Although
Is Hateful Again
2002](../Page/Although_Is_Hateful_Again_2002.md "wikilink")》擔任女主角。2004年2月，她曾意圖以一套出位的寫真集製做突破，可惜弄巧成拙。同月23日，她宣佈由於事件帶來的巨大精神壓力而要接受治療，所以要暫別娛樂圈。同年年中，獲得著名導演[金基德邀請擔當他的新作](../Page/金基德.md "wikilink")《[空屋](../Page/空屋情人.md "wikilink")》的女主角，重新回到演藝界。

## 新聞多多的李丞涓

李丞涓在1998年曾因為非法駕駛而被罰社會服務80小時。

## 慰安婦造型裸照事件

本發生在2004年2月12日，李丞涓在當時推出的寫真集。該輯寫真集以日治時期的慰安婦作為造型參考，書中的李丞涓寫著衣衫不整的內衣在穿著日本軍服的男人旁或蹲或站。記者會當日，李丞涓還站在書中一幀裸露背部的巨型照片前讓傳媒拍照。結果有關新聞出街後，立時引起全國社會反彈，認為是對國內的慰安婦的侮辱。網上的留言版及新聞組齊聲聲討李丞涓，報章的專欄也推波助瀾。不足一星期，李丞涓高調前往[京畿道一個讓戰時慰安婦居住的村莊謝罪](../Page/京畿道.md "wikilink")，並在她們面前聲淚俱下的跪倒叩罪，但仍不獲得她們原諒。由於來自群眾的壓力和攻擊不斷，李丞涓在23日宣佈暫別娛樂圈。

此外，在事件發生期間，[中國中央電視台原訂播放她的電視劇](../Page/中國中央電視台.md "wikilink")《[初戀](../Page/初戀_\(1996年電視劇\).md "wikilink")》，但因為這次裸照事件而被迫取消。

## 演出作品

### 電視劇

  - 1994年：[KBS](../Page/韓國放送公社.md "wikilink")《[員警](../Page/員警_\(電視劇\).md "wikilink")》
  - 1995年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[沙漏](../Page/沙漏_\(電視劇\).md "wikilink")》
  - 1996年：KBS《[初戀](../Page/初戀_\(1996年電視劇\).md "wikilink")》飾演 李曉京
  - 1997年：[KBS2](../Page/KBS第2頻道.md "wikilink")《》飾演 金荷娜
  - 1999年：KBS《[請相愛](../Page/請相愛.md "wikilink")》
  - 2001年：KBS《[東洋劇場](../Page/東洋劇場.md "wikilink")》
  - 2001年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[秋季遇到的男人](../Page/秋季遇到的男人.md "wikilink")》
  - 2002年：KBS《[好想談戀愛](../Page/好想談戀愛.md "wikilink")》
  - 2003年：SBS《[完整的愛](../Page/完整的愛.md "wikilink")》
  - 2006年：SBS《[愛情與野望](../Page/愛情與野望#2006年.md "wikilink")》飾演 慧珠
  - 2007年：MBC《[文熙](../Page/文熙.md "wikilink")》飾演 崔尚美
  - 2010年：MBC《[紅字](../Page/紅字_\(電視劇\).md "wikilink")》飾演 韓京瑞
  - 2012年：[JTBC](../Page/JTBC.md "wikilink")《[Happy
    Ending](../Page/Happy_Ending.md "wikilink")》飾演 洪愛蘭
  - 2012年：SBS《[大風水](../Page/大風水.md "wikilink")》飾演 永芝
  - 2014年：JTBC《[善巖女高偵探團](../Page/善巖女高偵探團.md "wikilink")》飾演 吳幼珍
  - 2015年：OCN《[我的美麗新娘](../Page/我的美麗新娘.md "wikilink")》飾演 李真淑
  - 2015年：On Style《[因為是第一次](../Page/因為是第一次.md "wikilink")》飾演 松兒的阿姨
  - 2018年：MBC《[富家公子](../Page/富家公子.md "wikilink")》飾演 南秀熙
  - 2019年：KBS《[左撇子妻子](../Page/左撇子妻子.md "wikilink")》

### 電影

  - 1996年：《[Piano Man](../Page/Piano_Man.md "wikilink")》
  - 1997年：《[Hercules](../Page/Hercules.md "wikilink")》（配音）
  - 1997年：《[Change](../Page/Change.md "wikilink")》
  - 1998年：《[Saturday,2:00 pm](../Page/Saturday,2:00_pm.md "wikilink")》
  - 2002年：《[Although Is Hateful Again
    2002](../Page/Although_Is_Hateful_Again_2002.md "wikilink")》
  - 2004年：《[空屋](../Page/空屋情人.md "wikilink")》
  - 2014年：《[愛麗絲：從仙境來的少年](../Page/愛麗絲：從仙境來的少年.md "wikilink")》

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p>年度</p></td>
<td><p>頒獎典禮</p></td>
<td><p>獎項</p></td>
<td><p>作品</p></td>
<td><p>結果</p></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/MBC演技大獎.md" title="wikilink">MBC演技大賞</a></p></td>
<td><p>連續劇部門 助演獎</p></td>
<td><p><a href="../Page/富家公子.md" title="wikilink">富家公子</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考列表

1.  [Empas：李丞涓個人資料](http://star.empas.com/people/info/le/es/leeseungyoun/)
2.  [李丞涓的新作“空屋”](http://www.krmdb.com/stills/2004/3Iron-2004/index.b5.shtml)
3.  [新浪網：李丞涓“慰安妇”写真激起韩人怒
    恐被全面封杀](http://ent.sina.com.cn/2004-02-16/0937303656.html)
4.  [新浪网：影音娱乐：慰安妇题材裸体写真风波](http://ent.sina.com.cn/f/lcyltxz/index.shtml)
5.  [網上反李丞涓的留言版](http://cafe.daum.net/antilee)
6.  [李丞涓主演的“Again 2002”](http://www.krmdb.com/stills/2002/Again2002-2002/review.b5.shtml)
7.  [朝鮮日報2001-08-08：李丞涓与GM签下空白合同](http://chinese.chosun.com/site/data/html_dir/2001/08/08/20010808000009.html)

## 外部連結

  - [EPG](https://web.archive.org/web/20070912053142/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=1005)


[L](../Category/韩国電視演员.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國女性模特兒.md "wikilink")
[L](../Category/韓國佛教徒.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")