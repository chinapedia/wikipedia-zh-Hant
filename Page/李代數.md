[数学上](../Page/数学.md "wikilink")，**李代数**是一个代数结构，主要用于研究象[李群和微分](../Page/李群.md "wikilink")[流形之类的几何对象](../Page/流形.md "wikilink")。李代数因研究[无穷小变换的概念而引入](../Page/无穷小变换.md "wikilink")。“李代数”（以[索菲斯·李命名](../Page/索菲斯·李.md "wikilink")）一词是由[赫尔曼·外尔在](../Page/赫尔曼·外尔.md "wikilink")1930年代引入的。在旧文献中，**无穷小群**指的就是李代数。

## 定义

**李代數**是一个在[域](../Page/域_\(数学\).md "wikilink") *F*
上的[向量空間](../Page/向量空間.md "wikilink")
\(\mathfrak{g}\)，具有满足以下条件的[二元運算](../Page/二元運算.md "wikilink")
\([\cdot,\cdot]:\mathfrak{g}\times \mathfrak{g}\to \mathfrak{g}\)（稱為**李括號**）：

  - [雙線性](../Page/双线性映射.md "wikilink")：

\[\forall a,b \in F,\,\forall x,y,z \in\mathfrak{g}\]

\[[a x + b y, z] = a [x, z] + b [y, z], \quad  [z, a x + b y] = a[z, x] + b [z, y]\]

  - [交错性](../Page/交错性.md "wikilink")：

\[\forall x \in \mathfrak{g}\]

\[[x,x]=0\]

  - [雅可比恒等式](../Page/雅可比恒等式.md "wikilink")：

\[x, y, z \in \mathfrak{g}\]

  -

      -
        \([x,[y,z]] + [y,[z,x]] + [z,[x,y]] = 0\)

用双线性来展开李括号\([x+y,x+y]\)，并用交错性来证明对所有*x*,
*y*属于\(\mathfrak{g}\)，均有\([x,y] + [y,x]=0\\)，我们可以从双线性和交错性推出反交换律：

  - [反交换律](../Page/反交换律.md "wikilink")

\[[x,y] = -[y,x], (\forall x, y \in\mathfrak{g})\]
\(([x-y,x] = -[y,x] = [x-y,x-y+y] = [x-y,y] = [x,y])\)。

反过来说，当*F* 的[特徵不是](../Page/域的特征.md "wikilink")
2时，反交换律也蕴含交错性（不过，当特征为2时，对于任何\(x \in \mathfrak{g}, 2x\)恒为零，故不能用\([x,x]=-[x,x]\)得到\([x,x]=0\)）。

用**李括號**表達的乘法不一定符合[結合律](../Page/結合律.md "wikilink")。即
\([[x_,__y_],__z_]\)_與_\([x,_[y,_z|x ,  y ],  z ]\) 與 \([x, [y, z]]\)
不一定相等。因此李代數通常並非[環或結合代數](../Page/環.md "wikilink")。

## 例子

1\. 如果我们定义李括号等于\(0\)，则每个向量空间自然成为一个平凡的交换李代数。

2\.
如果选李括号为[向量的](../Page/向量.md "wikilink")[叉乘](../Page/叉乘.md "wikilink")，[欧几里得空间](../Page/欧几里得空间.md "wikilink")\(\mathbb{R}^3\)是一个李代数。

3\.
若一个[结合代数](../Page/结合代数.md "wikilink")\(A\)给定乘法\(*\)，它可以通过定义\([x,y]=x*y-y*x\)而成为李代数。这个表达式称为\(x\)和\(y\)的换位子。相反的，每个李代数可以嵌入到一个以这个方式从结合代数得到的李代数中。参看[泛包络代数](../Page/泛包络代数.md "wikilink")。

4\.
另一个李代数的重要例子来自于[微分几何](../Page/微分几何.md "wikilink")：可微[流形上的](../Page/流形.md "wikilink")[光滑](../Page/光滑函数.md "wikilink")[向量场在把](../Page/向量场.md "wikilink")[李导数作为李括号的时候成为一个无穷维李代数](../Page/李导数.md "wikilink")。李导数把向量场\(X\)等同为作用在任何光滑标量场\(f\)上的偏微分算子，这是通过令\(X(f)\)为\(f\)在\(X\)方向的[方向导数达成的](../Page/李导数.md "wikilink")。这样，在表达式\((YX)(f)\)中，并列\(YX\)表示偏微分算子的[复合](../Page/函数复合.md "wikilink")。然后，李括号\([X,Y]\)定义为

  -
    \([X,Y]f=(XY-YX)f\)

对于流形上的每个光滑函数\(f\)。

这是流形的[微分同胚集合构成的无穷维李群的李代数](../Page/微分同胚.md "wikilink")。

5\.
[李群的左不变向量场组成的向量空间在李括号这个操作下是闭的](../Page/李群.md "wikilink")，因而是一个有限维李代数。或者，可以把属于一个李群的李代数的向量空间看成是该群的幺元的切空间。乘法就是群在幺元的微分的[换位子](../Page/换位子.md "wikilink")，\((a,b)\mapsto aba^{-1}b^{-1}\)。

6\.
作为一个具体的例子，考虑李群\(\mathrm{SL}(n,\mathbb{R})\)，所有实系数行列式为\(1\)的\(n\times n\)矩阵。单位矩阵的切空间可以和所有迹为\(0\)的实\(n\times n\)矩阵等同起来，其来自于李群的李代数结构和来自矩阵乘法的交换子的相同。

更多李群和它们相应的李代数，请参看[李群条目](../Page/李群.md "wikilink")。

## 同态，子代数，和理想

在同样基域\(F\)上的李代数\(\mathfrak{g}\)和\(\mathfrak{h}\)之间的一个[同态](../Page/同态.md "wikilink")\(\phi: \mathfrak{g} \to \mathfrak{h}\)是一个[\(F\)-线性映射](../Page/线性映射.md "wikilink")，使得对于所有\(\mathfrak{g}\)中的\(x\)和\(y\)有\([\phi(x),\phi(y)]=\phi([x,y])\)。这样的同态的复合也是同态，而域\(F\)上的李代数，和这些[态射一起](../Page/态射.md "wikilink")，组成了一个[范畴](../Page/范畴论.md "wikilink")。如果一个同态是[双射](../Page/双射.md "wikilink")，它称为[同构](../Page/同构.md "wikilink")，而两个李代数\(\mathfrak{g}\)和\(\mathfrak{h}\)称为*同构*的。对于所有的应用目的，同构的李代数是相同的。

李代数\(\mathfrak{g}\)的一个[子代数是](../Page/子代数.md "wikilink")\(\mathfrak{h}\)的一个[线性子空间](../Page/线性子空间.md "wikilink")\(\mathfrak{g}\)使得\([x,y]\in\mathfrak{h}\)对于所有\(x,y\in\mathfrak{h}\)成立。则这个子代数自身是一个李代数。

李代数\(\mathfrak{g}\)的[理想是](../Page/理想_\(环论\).md "wikilink")\(\mathfrak{g}\)的一个子空间\(\mathfrak{h}\)，使得\([a,y]\in\mathfrak{h}\)对于所有\(a\in\mathfrak{g}\)和\(y\in\mathfrak{h}\)成立。所有理想都是子代数。若\(\mathfrak{h}\)是\(\mathfrak{g}\)的一个理想，则商空间\(\mathfrak{g}/\mathfrak{h}\)成为一个李代数，这是通过定义\([x + \mathfrak{h}, y + \mathfrak{h}] = [x, y] + \mathfrak{h}\)为对于所有\(x, y \in\mathfrak{g}\)成立。理想刚好就是同态的[核](../Page/同态核.md "wikilink")，而[同态基本定理对于李代数是适用的](../Page/同态基本定理.md "wikilink")。

## 李代数的分类

实和复李代数可以分类到某种程度，而这个分类是李群分类的重要一步。每个有限维实或复李代数作为一个唯一的实或复[单连通李群的李代数出现](../Page/单连通.md "wikilink")（Ado定理），但是可能有一个以上的群，甚至一个以上的连通群，有这个相同的李代数。例如，群
SO(3)（[行列式值为](../Page/行列式.md "wikilink")1的 3×3 正交群）和SU(2) （行列式为1的 2×2
酉矩阵）有相同的李代数，就是 **R**<sup>3</sup>，以叉乘为李括号。

李代数是“交换的”，如果李括号为0，也就是 \[*x*, *y*\] = 0 对于所有 *x* 和 *y*。更一般的，一个李代数
\(\mathfrak{g}\)
是零幂（nilpotent）的，如果[低中心序列](../Page/低中心序列.md "wikilink")（lower
central series）

\[\mathfrak{g} > [\mathfrak{g},\mathfrak{g}] > [[\mathfrak{g},\mathfrak{g}],\mathfrak{g}] > [[[\mathfrak{g},\mathfrak{g}],\mathfrak{g}],\mathfrak{g}] > ...\]

最终为 0。按照[Engel定理](../Page/Engel定理.md "wikilink")，李代数零幂当且仅当对每个
\(\mathfrak{g}\) 中的 *u* 映射

\[ad(u):\mathfrak{g} \to \mathfrak{g}, \quad \operatorname{ad}(u)v=[u,v]\]

是零幂的。更一般的，李代数 \(\mathfrak{g}\)
是[可解的若](../Page/可解.md "wikilink")[导序列](../Page/导序列.md "wikilink")(derived
series)

\[\mathfrak{g} > [\mathfrak{g},\mathfrak{g}] > [[/mathfrak{g},/mathfrak{g}],[/mathfrak{g},/mathfrak{g}|\mathfrak{g},\mathfrak{g}],[\mathfrak{g},\mathfrak{g}]] > [[[/mathfrak{g},/mathfrak{g}],[/mathfrak{g},/mathfrak{g}|[\mathfrak{g},\mathfrak{g}],[\mathfrak{g},\mathfrak{g}]],[[/mathfrak{g},/mathfrak{g}],[/mathfrak{g},/mathfrak{g}|\mathfrak{g},\mathfrak{g}],[\mathfrak{g},\mathfrak{g}]]]  > ...\]

最终成为0。 极大可解子代数成为[波莱尔子代数](../Page/波莱尔子代数.md "wikilink")。

李代数 *g* 称为[半单](../Page/半单李代数.md "wikilink") 如果 \(\mathfrak{g}\)
唯一的可解理想是平凡的。等价的，\(\mathfrak{g}\)
是半单的当且仅当[基灵型](../Page/基灵型.md "wikilink")
*K*(*u*,*v*) = tr(ad(*u*)ad(*v*)) 是非退化的；这里 tr
表示[迹算子](../Page/矩阵的迹.md "wikilink")。当域 *F* 的特征数为 0，
\(\mathfrak{g}\)
半单单当且仅当每个[表示都是完全可约的](../Page/李代数表示.md "wikilink")，也就是对于每个表示的不变子空间，有一个不变的补空间（[外尔定理](../Page/外尔定理.md "wikilink")
Weyl's theorem）.

李代数是[单的](../Page/单李代数.md "wikilink")，如果它没有非平凡理想并且非交换。特别的有，一个单李代数是半单的，更一般的，半单李代数是单李代数的直和。

半单复李代数可通过它们的[根系分类](../Page/根系.md "wikilink")。

## 范畴理论定义

使用[范畴论的语言](../Page/范畴论.md "wikilink")，**李代数**可以定义为[向量空间范畴中的对象](../Page/向量空间范畴.md "wikilink")
*A* 和[态射](../Page/态射.md "wikilink")
\([\cdot, \cdot]: A \otimes A \to A\) 使得

  - \([\cdot, \cdot] \circ (\mathrm{id} + \tau_{A,A}) = 0\)
  - \([\cdot, \cdot] \circ ([\cdot, \cdot] \otimes \mathrm{id}) \circ (\mathrm{id} + \sigma + \sigma^2) = 0\)

其中\(\tau(a \otimes b) :=  b \otimes a\) 而 σ 是复合
\((\mathrm{id} \otimes \tau_{A,A})\circ(\tau_{A,A} \otimes \mathrm{id})\)的[循环枚举](../Page/循环枚举.md "wikilink")。用交换图形式:

<center>

[<File:Liealgebra.png>](https://zh.wikipedia.org/wiki/File:Liealgebra.png "fig:File:Liealgebra.png")

</center>

## 參看

  - [泊松括號](../Page/泊松括號.md "wikilink")
  - [李代数表示](../Page/李代数表示.md "wikilink")
  - [李代数伴随表示](../Page/李代数伴随表示.md "wikilink")
  - [李超代数](../Page/李超代数.md "wikilink")
  - [李余代数](../Page/李余代数.md "wikilink")
  - [李双代数](../Page/李双代数.md "wikilink")(Lie bialgebra)
  - [泊松代数](../Page/泊松代数.md "wikilink")
  - [anyonic李代数](../Page/anyonic李代数.md "wikilink")
  - [基灵型](../Page/基灵型.md "wikilink")
  - [李代数上同调](../Page/李代数上同调.md "wikilink")

## 参考

  - Humphreys, James E. *Introduction to Lie Algebras and Representation
    Theory*, Second printing, revised. Graduate Texts in Mathematics, 9.
    Springer-Verlag, New York, 1978. ISBN 0-387-90053-5
  - Jacobson, Nathan, *Lie algebras*, Republication of the 1962
    original. Dover Publications, Inc., New York, 1979. ISBN
    0-486-63832-4

[L](../Category/抽象代数.md "wikilink") [L](../Category/微分几何.md "wikilink")
[L](../Category/李代數.md "wikilink")