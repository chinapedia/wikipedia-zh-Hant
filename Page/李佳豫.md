**李佳豫**（，），[台灣](../Page/台灣.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")、[電視節目](../Page/電視節目.md "wikilink")[主持人](../Page/主持人.md "wikilink")，暱稱「小豫兒」。畢業於[華岡藝校舞蹈科](../Page/臺北市私立華岡藝術學校.md "wikilink")（[芭蕾舞](../Page/芭蕾舞.md "wikilink")、[民族舞蹈](../Page/民族舞蹈.md "wikilink")、[現代舞](../Page/現代舞.md "wikilink")、[佛朗明哥](../Page/佛朗明哥.md "wikilink")）。出道前以拍[廣告為主](../Page/廣告.md "wikilink")，現在則活躍在[主持與演戲方面](../Page/主持.md "wikilink")。

## 個人經歷

  - 2003年正式出道。初開始主持[三立都會台旅遊節目](../Page/三立都會台.md "wikilink")《[中國那麼大](../Page/中國那麼大.md "wikilink")》，以「小豫兒」之名主持。並首次參與[華視](../Page/華視.md "wikilink")[偶像劇](../Page/偶像劇.md "wikilink")《[單身宿舍連環泡](../Page/單身宿舍連環泡.md "wikilink")》演出。
  - 2004年由《中國那麼大》製作群聯手推出的紀錄書《五朵金花豔闖邊疆》，以她為[封面人物](../Page/封面人物.md "wikilink")。8月[客串](../Page/客串.md "wikilink")[TVBS-G偶像劇](../Page/TVBS-G.md "wikilink")《[香草戀人館](../Page/香草戀人館.md "wikilink")》，也接拍[東森戲劇台偶像劇](../Page/東森戲劇台.md "wikilink")《[極速傳說2](../Page/極速傳說2.md "wikilink")：愛戀2000米》。10月首度與[陽帆合作主持](../Page/陽帆.md "wikilink")[東森綜合台](../Page/東森綜合台.md "wikilink")《[全能估價王](../Page/全能估價王.md "wikilink")》，並暫別《中國那麼大》主持工作。10月底，被報原已報考[北京](../Page/北京.md "wikilink")[中央戲劇學院並一路過關斬將至最後關卡](../Page/中央戲劇學院.md "wikilink")，她卻擔心不知如何對家人、公司交代，最後臨陣退縮硬是錯過考試。11月底，因為檔期問題辭去《全能估價王》主持工作。
  - 2005年初接替[渟渟主持](../Page/郭婷婷.md "wikilink")[中視電玩節目](../Page/中視.md "wikilink")《[數位遊戲王](../Page/數位遊戲王.md "wikilink")》。1月底，《中國那麼大》節目更名為《[世界那麼大](../Page/世界那麼大.md "wikilink")》，她則在4月播出之「[陝西行](../Page/陝西.md "wikilink")」中復出該節目的主持工作。4月赴[尼泊爾錄影](../Page/尼泊爾.md "wikilink")，由於情勢誤判使得製作群在四天內爬了十六座山，號稱是她出外景來最接近死神的一次體驗。
  - 2006年主演東森自製偶像劇《[極道學園](../Page/極道學園.md "wikilink")》。6月《世界那麼大》發行[DVD](../Page/DVD.md "wikilink")，她的部份已陸續發行〈西藏篇〉、〈雲南篇〉、〈西班牙篇〉、〈夏威夷篇〉、〈尼泊爾篇〉、〈峇里島篇〉、〈荷蘭篇〉等七篇。7月個人首本隨筆旅遊書籍《李佳豫愛出走》發行並大賣三萬多本，10月她和[張宇文](../Page/張宇文.md "wikilink")、[張宇豪三人共同主持](../Page/張宇豪.md "wikilink")[公視節目](../Page/公視.md "wikilink")《同學少年都不賤》。10月中，受到[日本](../Page/日本.md "wikilink")[岩手縣官方欽點](../Page/岩手縣.md "wikilink")，隨《世界那麼大》赴岩手縣拍攝節目。12月發行由她、[柯以柔](../Page/柯以柔.md "wikilink")、[黃曉玫共同創作之旅遊書籍](../Page/黃曉玫.md "wikilink")《世界那麼大：小豫兒、柯以柔的旅行交換日記》。
  - 2007年再度與[陽帆合作](../Page/陽帆.md "wikilink")，共同主持[東風衛視](../Page/東風衛視.md "wikilink")《[估價王](../Page/估價王.md "wikilink")》。
  - 2008年12月，結束《[數位遊戲王](../Page/數位遊戲王.md "wikilink")》主持工作，由[瑤瑤接替](../Page/郭書瑤.md "wikilink")。
  - 2012年1月，結束《[世界那麼大](../Page/世界那麼大.md "wikilink")》主持工作。

## 演藝經歷

### 主持

|                       |                                                                           |                                            |
| --------------------- | ------------------------------------------------------------------------- | ------------------------------------------ |
| 日期                    | 頻道                                                                        | 節目名稱                                       |
| 2003年1月－2012年1月6日     | [三立都會台](../Page/三立都會台.md "wikilink")                                      | [世界那麼大](../Page/世界那麼大.md "wikilink")       |
| 2004年10-11月           | [東森綜合台](../Page/東森綜合台.md "wikilink")                                      | [全能估價王](../Page/全能估價王.md "wikilink")       |
| 2005年1月－2008年12月27日   | [中視](../Page/中視主頻道.md "wikilink")                                         | [數位遊戲王](../Page/數位遊戲王.md "wikilink")       |
| 2006年10-12月           | [公視](../Page/公視主頻道.md "wikilink")                                         | [同學少年都不賤](../Page/同學少年都不賤.md "wikilink")   |
| 2007年1-10月            | [東風衛視](../Page/東風衛視.md "wikilink")                                        | [估價王](../Page/估價王.md "wikilink")           |
| 2010年10月3日－2011年4月10日 | 公視、[人間衛視](../Page/人間衛視.md "wikilink")                                     | [就ㄐㄧㄤˋ玩科學](../Page/就ㄐㄧㄤˋ玩科學.md "wikilink") |
| 2012年1月－2013年6月       | [中天亞洲台](../Page/中天亞洲台.md "wikilink")、[PPS影音](../Page/PPS影音.md "wikilink") | [星星帶你去旅行](../Page/星星帶你去旅行.md "wikilink")   |

### 電視劇

|                                                                     |                                                                     |                                                  |           |
| ------------------------------------------------------------------- | ------------------------------------------------------------------- | ------------------------------------------------ | --------- |
| 年份                                                                  | 頻道                                                                  | 劇名                                               | 飾演        |
| 2003                                                                | [華視](../Page/華視.md "wikilink")                                      | 《[單身宿舍連環泡](../Page/單身宿舍連環泡.md "wikilink")》       | 翠萍        |
| 2004                                                                | [TVBS-G](../Page/TVBS-G.md "wikilink")                              | 《[香草戀人館](../Page/香草戀人館.md "wikilink")》           | 客串一集      |
| [東森](../Page/東森.md "wikilink")                                      | 《[極速傳說2](../Page/極速傳說2.md "wikilink")-愛戀2000米》                      | MIX                                              |           |
| 2006                                                                | [華視](../Page/華視.md "wikilink")                                      | 《[極道學園](../Page/極道學園.md "wikilink")》             | 洛亭（綽號：一毛） |
| 2007                                                                | [公視](../Page/公視.md "wikilink")                                      | 《人生劇展：[時光照應](../Page/時光照應.md "wikilink")》        |           |
| 2008                                                                | [中視](../Page/中視.md "wikilink")                                      | 《[牽牛花開的日子](../Page/牽牛花開的日子.md "wikilink")》       | 李阿寶       |
| 2009                                                                | [公視](../Page/公視.md "wikilink")                                      | 《[我的這一班](../Page/我的這一班_\(台灣電視劇\).md "wikilink")》 | 李家瑜       |
| [中視](../Page/中視.md "wikilink")                                      | 《[閃亮的日子](../Page/閃亮的日子_\(中視電視劇\).md "wikilink")》                    | 陳玉婷                                              |           |
| 2011                                                                | [中天娛樂台](../Page/中天娛樂台.md "wikilink")                                | 《[戀戀阿里山](../Page/戀戀阿里山.md "wikilink")》           | 楊美英       |
| 2012                                                                | [壹電視](../Page/壹電視.md "wikilink")                                    | 《[幸福蒲公英](../Page/幸福蒲公英.md "wikilink")》           | 高廣美       |
| [大愛](../Page/大愛.md "wikilink")                                      | 《長情劇展-[大英家之味](../Page/大英家之味.md "wikilink")》                         | 蔡淑鈺                                              |           |
| 《長情劇展-[白袍的約定](../Page/白袍的約定.md "wikilink")》                         | 陳紅燕                                                                 |                                                  |           |
| 2013                                                                | [八大電視](../Page/八大電視.md "wikilink")                                  | 《[愛的生存之道](../Page/愛的生存之道.md "wikilink")》         | Coco      |
| [民視](../Page/民視.md "wikilink")                                      | 《[廉政英雄](../Page/廉政英雄.md "wikilink")》                                | 石可心                                              |           |
| [台視](../Page/台視.md "wikilink")                                      | 《[結婚好嗎？](../Page/結婚好嗎？.md "wikilink")》                              | 張儀娟                                              |           |
| 2014                                                                | 《[神仙·老師·狗](../Page/神仙·老師·狗.md "wikilink")》                          | 伍悅亮                                              |           |
| 2015                                                                | [台視](../Page/台視.md "wikilink")/[三立都會台](../Page/三立都會台.md "wikilink") | 《[聽見幸福](../Page/聽見幸福.md "wikilink")》             | 王曉琳       |
| [台視](../Page/台視.md "wikilink")/[八大綜合台](../Page/八大綜合台.md "wikilink") | 《[新世界](../Page/新世界_\(電視劇\).md "wikilink")》                          | 羅詠玫                                              |           |
| [台視](../Page/台視.md "wikilink")                                      | 《[天若有情](../Page/天若有情_\(2015年電視劇\).md "wikilink")》                   | 李曉佩                                              |           |
| 2016                                                                | [三立](../Page/三立.md "wikilink")                                      | 《[大人情歌](../Page/大人情歌_\(電視劇\).md "wikilink")》     | 吳姿姿       |
| [公共電視](../Page/公共電視.md "wikilink")                                  | 《[今晚，你想點什麼？](../Page/今晚，你想點什麼？.md "wikilink")》                      | LiLi                                             |           |
| [三立](../Page/三立.md "wikilink")                                      | 《[獨家保鑣](../Page/獨家保鑣.md "wikilink")》                                | 蔣文莉                                              |           |
| 2017                                                                | [公視+](../Page/公共電視文化事業基金會.md "wikilink")                            | 《[城市情歌](../Page/城市情歌.md "wikilink")》             | 陳家福       |
| [芒果TV](../Page/芒果TV.md "wikilink")                                  | 《[那刻的怦然心動](../Page/那刻的怦然心動.md "wikilink")》                          | 淑惠                                               |           |
| 2019                                                                | [大愛](../Page/大愛電視劇.md "wikilink")                                   | 《[最美的相遇](../Page/最美的相遇.md "wikilink")》           | 彭瑞芬       |
| 2019                                                                | [三立都會台](../Page/三立都會台.md "wikilink")                                | 《[月村歡迎你](../Page/月村歡迎你.md "wikilink")》           | 夏鷗        |

### 書籍

  - 《李佳豫愛出走》，沃爾文化，2006年7月5日，ISBN 9861276831
  - 《世界那麼大：小豫兒、柯以柔的旅行交換日記》，[平裝本出版](../Page/皇冠文化.md "wikilink")，2006年12月8日，ISBN
    9578036094

### DVD

  - 世界那麼大／李佳豫《祕魯》，采昌國際多媒體
  - 世界那麼大／李佳豫《荷蘭》，采昌國際多媒體
  - 世界那麼大／李佳豫《夏威夷》，采昌國際多媒體
  - 世界那麼大／李佳豫《佛教精選》，采昌國際多媒體
  - 世界那麼大／李佳豫《雲南》，采昌國際多媒體
  - 世界那麼大／李佳豫《峇里島–神仙的花園》，采昌國際多媒體
  - 世界那麼大／李佳豫《西班牙》，采昌國際多媒體
  - 世界那麼大／李佳豫《尼泊爾》，采昌國際多媒體

### 廣告

  - [肯德基](../Page/肯德基.md "wikilink")-外帶全家餐篇
  - [肯德基](../Page/肯德基.md "wikilink")-蛋塔篇
  - [肯德基](../Page/肯德基.md "wikilink")-墨西哥雞肉捲
  - [中華電信](../Page/中華電信.md "wikilink")
  - [和信電訊](../Page/和信電訊.md "wikilink")
  - 2002年[全家便利商店](../Page/全家便利商店.md "wikilink")
  - 2003年[萬通銀行彈跳篇](../Page/萬通銀行.md "wikilink")
  - 2003年[泛亞電信預付卡](../Page/泛亞電信.md "wikilink")
  - 2004年[統一企業泡麵](../Page/統一企業.md "wikilink")
  - [洽洽食品洽洽香瓜子](../Page/洽洽食品.md "wikilink")-招財貓篇

### MV

  - [游鴻明](../Page/游鴻明.md "wikilink")〈靠近一點〉

### 受訪

  - [Colalin](../Page/Colalin.md "wikilink")([林穎璇](../Page/林穎璇.md "wikilink"))主持的網路廣播([Garena聊聊](../Page/Garena聊聊.md "wikilink"))節目《[Cola
    IN了沒](../Page/Cola_IN了沒.md "wikilink")》，2013年7月3日

### 電影

  - 2017年《[大釣哥](../Page/大釣哥.md "wikilink")》飾演 藍大釣妻子
  - 2017年《[請愛我的女朋友](../Page/請愛我的女朋友.md "wikilink")》飾演 小梅

### 節目通告

  - [綜藝大集合](../Page/綜藝大集合.md "wikilink")
    ([民視](../Page/民視.md "wikilink"))
  - [天才衝衝衝](../Page/天才衝衝衝.md "wikilink")
    ([中華電視公司](../Page/中華電視公司.md "wikilink"))
  - [綜藝大熱門](../Page/綜藝大熱門.md "wikilink")
    ([三立都會台](../Page/三立都會台.md "wikilink"))
  - [小明星大跟班](../Page/小明星大跟班.md "wikilink")
    ([中天綜合台](../Page/中天綜合台.md "wikilink").[中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [天天樂財神](../Page/天天樂財神.md "wikilink")
    ([中天娛樂台](../Page/中天娛樂台.md "wikilink"))
  - [飢餓遊戲](../Page/飢餓遊戲_\(電視節目\).md "wikilink")
    ([中國電視公司](../Page/中國電視公司.md "wikilink"))

## 參考資料

## 外部連結

  - [李佳豫=小豫兒 隨意窩 Xuite日誌](http://blog.xuite.net/a06085247564/twblog)

  -
  -
  -
  -
[L李](../Category/台灣电视女演員.md "wikilink")
[L李](../Category/台灣綜藝節目主持人.md "wikilink")
[L李](../Category/臺北市私立華岡藝術學校校友.md "wikilink")
[J佳豫](../Category/李姓.md "wikilink")
[Category:河南裔台灣人](../Category/河南裔台灣人.md "wikilink")