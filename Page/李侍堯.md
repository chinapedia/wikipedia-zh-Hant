[Li_Shiyao.jpg](https://zh.wikipedia.org/wiki/File:Li_Shiyao.jpg "fig:Li_Shiyao.jpg")
**李侍堯**（），字**欽齋**，漢軍[鑲黃旗人](../Page/鑲黃旗.md "wikilink")，二等伯[李永芳四世孫](../Page/李永芳.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[封疆大吏](../Page/封疆大吏.md "wikilink")，[伯爵](../Page/伯爵.md "wikilink")。歷任[尚書](../Page/尚書.md "wikilink")、[總督等](../Page/總督.md "wikilink")，兩度犯下死罪，乾隆帝不忍殺他，卒諡**恭毅**。

## 簡介

[乾隆八年](../Page/乾隆.md "wikilink")，以[蔭生授印務](../Page/蔭生.md "wikilink")[章京](../Page/章京.md "wikilink")。

二十年，升[工部](../Page/工部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")。

二十二年，代理[鶴年署](../Page/鶴年.md "wikilink")[兩廣總督](../Page/兩廣總督.md "wikilink")。二十四年，奉旨接替[陳弘謀擔任兩廣總督](../Page/陳弘謀.md "wikilink")。

二十六年，擢[戶部尚書](../Page/戶部尚書.md "wikilink")。

二十八年，授[湖廣總督](../Page/湖廣總督.md "wikilink")，加[太子太保](../Page/太子太保.md "wikilink")。

二十九年，回任[兩廣總督](../Page/兩廣總督.md "wikilink")。三十二年，襲二等**昭信伯**。

三十八年，拜[武英殿](../Page/武英殿.md "wikilink")[大學士](../Page/大學士.md "wikilink")。

四十二年，遷[雲貴總督](../Page/雲貴總督.md "wikilink")；翌年，暫在[軍機處行走](../Page/軍機處.md "wikilink")，不久還總督本任。

四十五年，[雲南糧儲道](../Page/雲南.md "wikilink")[海寧揭李貪縱](../Page/海寧.md "wikilink")，[乾隆帝派](../Page/乾隆帝.md "wikilink")[錢灃](../Page/錢灃.md "wikilink")、[和珅赴](../Page/和珅.md "wikilink")[滇徹查](../Page/滇.md "wikilink")，擬[斬監候](../Page/斬監候.md "wikilink")；翌年，[甘肅爆發](../Page/甘肅.md "wikilink")[撒拉族](../Page/撒拉族.md "wikilink")[蘇四十三動亂](../Page/蘇四十三.md "wikilink")，[帝遣](../Page/帝.md "wikilink")[阿桂督師](../Page/阿桂.md "wikilink")，特旨「予侍堯三品頂戴、孔雀翎，赴甘肅治軍事」；旋[甘肅冒賑案事發](../Page/甘肅冒賑案.md "wikilink")，[陝甘總督](../Page/陝甘總督.md "wikilink")[勒爾謹獲罪](../Page/勒爾謹.md "wikilink")，命李攝[總督](../Page/總督.md "wikilink")。

四十九年，撒拉族[田五動亂](../Page/田五.md "wikilink")，亂久未定，上責「玩延怯懦」，遭革職，命「隨軍效力督餉」；[福康安至軍](../Page/福康安.md "wikilink")，發侍堯「玩愒貽誤諸罪」諸罪，狀擬[斬立決](../Page/斬立決.md "wikilink")，帝仍從寬改斬監候。五十年，再次復起，署[正黃旗漢軍都統](../Page/正黃旗.md "wikilink")，旋署[戶部尚書](../Page/戶部尚書.md "wikilink")，又署[湖廣總督](../Page/湖廣總督.md "wikilink")。

五十二年，臺灣[林爽文事件爆發](../Page/林爽文事件.md "wikilink")，調[閩浙總督](../Page/閩浙總督.md "wikilink")，次年與福康安、[海蘭察協力平亂](../Page/海蘭察.md "wikilink")，襲[伯爵](../Page/伯爵.md "wikilink")。五十三年十月廿三（1788年11月20日）卒，諡**恭毅**。

[清史稿評](../Page/:s:zh:清史稿/卷323.md "wikilink")：「侍堯短小精敏，過目成誦。見屬僚，數語即辨其才否。擁幾高坐，語所治肥瘠利害，或及其陰事，若親見。人皆悚懼。屢以貪黷坐法，上（乾隆帝）終憐其才，為之曲赦。」

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")

{{-}}

[L](../Category/清朝工部侍郎.md "wikilink")
[L](../Category/清朝兩廣總督.md "wikilink")
[L](../Category/清朝戶部尚書.md "wikilink")
[L](../Category/清朝武英殿大學士.md "wikilink")
[L](../Category/軍機大臣.md "wikilink")
[L](../Category/漢軍鑲黃旗人.md "wikilink")
[S](../Category/李姓.md "wikilink")
[Category:諡恭毅](../Category/諡恭毅.md "wikilink")
[Category:清朝二等伯](../Category/清朝二等伯.md "wikilink")