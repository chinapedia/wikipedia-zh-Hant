**李保田**，中國表演艺术家、影視、戲劇演員。[江蘇](../Page/江蘇.md "wikilink")[徐州](../Page/徐州.md "wikilink")[贾汪人](../Page/贾汪.md "wikilink")，祖籍[山東](../Page/山東.md "wikilink")[威海](../Page/威海.md "wikilink")[文登](../Page/文登.md "wikilink")。1960年2月進徐州地區梆子劇團學演丑角，後轉入徐州地區[文工團](../Page/文工團.md "wikilink")，1978年入[中央戲劇學院導演幹部進修班](../Page/中央戲劇學院.md "wikilink")，畢業後留校任教。另外在电视艺术领域取得突出成就。同時也是[中國電視藝術家協會會員](../Page/中國電視藝術家協會.md "wikilink")，[中國電影家協會會員](../Page/中國電影家協會.md "wikilink")，[中國電影評論學會影視名人研究會名譽會長](../Page/中國電影評論學會.md "wikilink")。

## 家庭

著名演員李彧為其兒子。\[1\]\[2\]\[3\]

## 作品

### 电影

| 年份                                      | 中文名                                            | 角色      | 备注 |
| --------------------------------------- | ---------------------------------------------- | ------- | -- |
| 1983                                    | 《[闯江湖](../Page/闯江湖.md "wikilink")》             | 张乐天     |    |
| 1985                                    | 《[流浪汉与天鹅](../Page/流浪汉与天鹅.md "wikilink")》       | 流浪汉抹桌   |    |
| 《[老君寨奇闻](../Page/老君寨奇闻.md "wikilink")》  | 客串                                             |         |    |
| 1987                                    | 《[别叫我疤痢](../Page/别叫我疤痢.md "wikilink")》         | 客串      |    |
| 《[贞女](../Page/贞女.md "wikilink")》        | 客串                                             |         |    |
| 《[人鬼情](../Page/人鬼情.md "wikilink")》      | 秋芸的父亲                                          |         |    |
| 1988                                    | 《[荒原杀手](../Page/荒原杀手.md "wikilink")》           | 赵枫      |    |
| 1990                                    | 《[菊豆](../Page/菊豆.md "wikilink")》               | 杨天青     |    |
| 1992                                    | 《[过年](../Page/过年_\(电影\).md "wikilink")》        | 父亲      |    |
| 1993                                    | 《[中国人](../Page/中国人_\(电影\).md "wikilink")》      | 客串      |    |
| 《[葛老爷子](../Page/葛老爷子.md "wikilink")》    | 葛爷                                             |         |    |
| 1994                                    | 《[凤凰琴](../Page/凤凰琴.md "wikilink")》             | 余校长     |    |
| 《[火船](../Page/火船_\(电影\).md "wikilink")》 | 客串                                             |         |    |
| 1996                                    | 《[摇啊摇，摇到外婆桥](../Page/摇啊摇，摇到外婆桥.md "wikilink")》 | 唐老大     |    |
| 《[有话好好说](../Page/有话好好说.md "wikilink")》  | 张秋生                                            |         |    |
| 1997                                    | 《[离婚了，就别来找我](../Page/离婚了，就别来找我.md "wikilink")》 | 客串      |    |
| 2006                                    | 《[马背上的法庭](../Page/马背上的法庭.md "wikilink")》       | 客串      |    |
| 2012                                    | 《[夜莺](../Page/夜莺_\(电影\).md "wikilink")》        | 志根      |    |
| 2015                                    | 《[北京时间](../Page/北京時間_\(電影\).md "wikilink") 》   | 时长工(老年) |    |
| 2018                                    | 《[大路朝天](../Page/大路朝天.md "wikilink")》           |         |    |

### 电视剧

| 年份                                           | 中文名                                               | 角色                             |
| -------------------------------------------- | ------------------------------------------------- | ------------------------------ |
| 1986                                         | 《[葛掌柜](../Page/葛掌柜.md "wikilink")》                |                                |
| 1988                                         | 《[师魂](../Page/师魂.md "wikilink")》                  |                                |
| 1989                                         | 《[好男好女](../Page/好男好女.md "wikilink")》              |                                |
| 1991                                         | 《[大路朝天](../Page/大路朝天.md "wikilink")》              |                                |
| 1992                                         | 《[山不转水转](../Page/山不转水转.md "wikilink")》            |                                |
| 1995                                         | 《[宰相刘罗锅](../Page/宰相刘罗锅.md "wikilink")》            | [刘墉](../Page/刘墉.md "wikilink") |
| 1997                                         | 《[鸦片战争演义](../Page/鸦片战争演义.md "wikilink")》          | 道光                             |
| 1998                                         | 《[烟壶](../Page/烟壶_\(电视剧\).md "wikilink")》          |                                |
| 1999                                         | 《[生死两周半](../Page/生死两周半.md "wikilink")》            |                                |
| 《[大清药王](../Page/大清药王.md "wikilink")》         |                                                   |                                |
| 《[村主任李四平](../Page/村主任李四平.md "wikilink")》     |                                                   |                                |
| 2000                                         | 《[石瀑布](../Page/石瀑布.md "wikilink")》                | 林连长                            |
| 《[警察李酒瓶](../Page/警察李酒瓶.md "wikilink")》       | 李久平                                               |                                |
| 2001                                         | 《[神医喜来乐](../Page/神医喜来乐.md "wikilink")》            | 喜来乐                            |
| 2002                                         | 《[跃龙门](../Page/跃龙门.md "wikilink")》                | 张伯行                            |
| 2003                                         | 《[王保长新篇](../Page/王保长新篇.md "wikilink")》            | 王保长                            |
| 2004                                         | 《[御前双雄](../Page/御前双雄.md "wikilink")》              | 九王爷                            |
| 2005                                         | 《[巡城御史鬼难缠](../Page/巡城御史鬼难缠.md "wikilink")》        | 桂阑栅                            |
| 《[钦差大臣](../Page/钦差大臣_\(电视剧\).md "wikilink")》 |                                                   |                                |
| 《[厨子当官](../Page/厨子当官.md "wikilink")》         | 石竹香                                               |                                |
| 2007                                         | 《[王保长新篇2](../Page/王保长新篇2.md "wikilink")》          | 王保长                            |
| 2009                                         | 《[永不回头](../Page/永不回头_\(2010年电视剧\).md "wikilink")》 |                                |
| 《[南北大状](../Page/南北大状.md "wikilink")》         |                                                   |                                |
| 2010                                         | 《[丑角爸爸](../Page/丑角爸爸.md "wikilink")》              |                                |
| 2011                                         | 《[杨乃武与小白菜冤案](../Page/杨乃武与小白菜冤案.md "wikilink")》    | 刘锡彤                            |
| 《[小洋楼](../Page/小洋楼.md "wikilink")》           |                                                   |                                |
| 2013                                         | 《[神医喜来乐传奇](../Page/神医喜来乐传奇.md "wikilink")》        | 喜来乐                            |

## 奖项

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品</p></th>
<th><p>奖项</p></th>
<th><p>是否得奖</p></th>
<th><p>备注（授奖对象）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1985</p></td>
<td><p>《流浪汉与天鹅》</p></td>
<td><p><a href="../Page/中华人民共和国文化部.md" title="wikilink">中华人民共和国文化部优秀影片奖</a></p></td>
<td></td>
<td><p>该片</p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p>《葛掌柜》</p></td>
<td><p><a href="../Page/飞天奖.md" title="wikilink">第8届中国电视剧飞天奖最佳男主角</a></p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="odd">
<td><p>《人鬼情》</p></td>
<td><p><a href="../Page/中国电影金鸡奖.md" title="wikilink">第8届中国电影金鸡奖最佳男配角</a></p></td>
<td></td>
<td><p>个人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《师魂》</p></td>
<td><p><a href="../Page/飞天奖.md" title="wikilink">中国电视剧飞天奖一等奖</a></p></td>
<td></td>
<td><p>该片</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p>《过年》</p></td>
<td><p><a href="../Page/大众电影百花奖.md" title="wikilink">大众电影百花奖最佳故事片</a></p></td>
<td></td>
<td><p>该片</p></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p>《菊豆》</p></td>
<td><p><a href="../Page/戛纳电影节.md" title="wikilink">第43届法国戛纳电影节特别奖</a></p></td>
<td></td>
<td><p>该片</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芝加哥国际电影节.md" title="wikilink">芝加哥国际电影节·金雨果奖</a></p></td>
<td></td>
<td><p>该片</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/东京国际电影节.md" title="wikilink">第4届东京国际电影节评委特别奖</a></p></td>
<td></td>
<td><p>该片</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p>《葛老爷子》</p></td>
<td><p><a href="../Page/长春电影节.md" title="wikilink">长春电影节最佳影片金奖</a></p></td>
<td></td>
<td><p>该片</p></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p>《凤凰琴》</p></td>
<td><p><a href="../Page/华表奖最佳男演员.md" title="wikilink">华表奖最佳男演员</a></p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p>《凤凰琴》</p></td>
<td><p>第17届大众电影百花奖最佳男演员</p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="even">
<td><p>第14届中国电影金鸡奖最佳男主角</p></td>
<td></td>
<td><p>个人</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p>《宰相刘罗锅》</p></td>
<td><p><a href="../Page/中国电视金鹰奖.md" title="wikilink">中国电视金鹰奖最佳男主角</a></p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p>《有话好好说》</p></td>
<td><p>第21届大众电影百花奖最佳男配角</p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td></td>
<td><p>北京市十大老艺术家</p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>第18届中国电视金鹰奖观众最喜爱优秀演员奖</p></td>
<td></td>
<td><p>个人</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>第18届中国电视金鹰奖观众最喜爱的艺术家</p></td>
<td></td>
<td><p>个人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p>《警察李酒瓶》</p></td>
<td><p>第19届中国电视金鹰奖观众最喜爱的男演员</p></td>
<td></td>
<td><p>个人</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><em>神医喜来乐</em></p></td>
<td><p>第23届飞天奖优秀男演员</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第21届金鹰奖最喜爱的男演员</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第21届金鹰奖最具人气男演员</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第21届金鹰奖最佳表演艺术男演员</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 获奖

  - 2001年被评为中国电视金鹰奖观众最喜爱的优秀演员奖。
  - 第八届电影[金鸡奖最佳男配角奖](../Page/金鸡奖.md "wikilink")；
  - 第十四届中国电影"金鸡奖"最佳男主角奖；
  - 第八届电视剧[飞天奖最佳男主角奖](../Page/飞天奖.md "wikilink")；
  - 第二十三届电视剧飞天奖优秀男演员奖；
  - 第四届[东京国际电影节](../Page/东京国际电影节.md "wikilink")"评委特别奖"；
  - 1993年华表奖最佳男演员奖；
  - 第十七届《大众电影》[百花奖最佳男演员奖](../Page/百花奖.md "wikilink")。
  - 2000年被评为北京市十大老艺术家、中国电视金鹰奖观众最喜爱的优秀演员奖；
  - 《流浪汉与天鹅》获85年文化部优秀影片奖；
  - 电影《菊豆》获第43届法国嘎纳电影节特别奖、芝加哥国际电影节金雨果奖；
  - 电视剧《师魂》被评为88年全国电视剧“飞天奖”一等奖；

## 参考文献

## 外部链接

  -
  -
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[Category:中國電影男演員](../Category/中國電影男演員.md "wikilink")
[Category:中央戲劇學院校友](../Category/中央戲劇學院校友.md "wikilink")
[Category:威海人](../Category/威海人.md "wikilink")
[Category:文登人](../Category/文登人.md "wikilink")
[Category:徐州人](../Category/徐州人.md "wikilink")
[B](../Category/李姓.md "wikilink")
[Category:江苏演员](../Category/江苏演员.md "wikilink")

1.
2.
3.