**李修贤**（****，），原名**李修德**\[1\]，[香港電影演員](../Page/香港.md "wikilink")、監製，以擅於製作[警匪片見稱](../Page/警匪片.md "wikilink")。\[2\]

## 生平

李修賢於1952年出生于[上海](../Page/上海.md "wikilink")，祖籍[廣東](../Page/廣東.md "wikilink")[汕頭](../Page/汕頭.md "wikilink")，1955年隨家人移居[香港](../Page/香港.md "wikilink")。少時李修賢家庭環境欠佳，畢業於[華德學校下午校](../Page/華德學校.md "wikilink")（現為[九龍塘天主教華德學校](../Page/九龍塘天主教華德學校.md "wikilink")）\[3\]。後只讀到[瑪利諾中學中四年級便輟學](../Page/瑪利諾中學.md "wikilink")，曾當過[鋁窗安裝工人](../Page/鋁窗.md "wikilink")、救生員和酒樓侍應。

1970年加入[邵氏電影公司](../Page/邵氏電影公司.md "wikilink")，憑著好動及敏捷的身手，獲大導演[張徹賞識](../Page/張徹.md "wikilink")，欽點成為其作品中的男主角。他在七十年代常在電影中亮相，起先通常都是在武俠電影中以武打形象出現，但一般都不是主角。首部擔任主角的電影則是1973年由[張曾澤執導的](../Page/張曾澤.md "wikilink")《[江湖行](../Page/江湖行.md "wikilink")》。七十年代中以後，李修賢亦有參演一些[喜劇如](../Page/喜劇.md "wikilink")《[中國超人](../Page/中國超人.md "wikilink")》和《[猩猩王](../Page/猩猩王.md "wikilink")》。當時他已有一定名氣，但遜於當時同屬邵氏的[狄龍](../Page/狄龍.md "wikilink")、[姜大衛](../Page/姜大衛.md "wikilink")、[陳觀泰等等](../Page/陳觀泰.md "wikilink")。

1977年他開始學習[導演](../Page/導演.md "wikilink")、[剪接](../Page/剪接.md "wikilink")、[配音等幕後工作](../Page/配音.md "wikilink")，並於1978年開辦獨立製片人公司。其間他作過多方面嘗試，最後以警匪片作為定位。1982年他執導由[王青](../Page/王青.md "wikilink")、[鄭則仕參演的喜劇](../Page/鄭則仕.md "wikilink")《[Friend過打Band](../Page/Friend過打Band.md "wikilink")》，成功掀起了一股警匪片熱潮。其後他在[楊群旗下的鳳鳴影業公司任導演](../Page/楊群.md "wikilink")。1984年憑著一部以[香港警察的生活為藍本](../Page/香港警察.md "wikilink")，自編自導自演《[公僕](../Page/公僕_\(電影\).md "wikilink")》，贏得第四屆[香港電影金像獎影帝及第二十一屆](../Page/香港電影金像獎.md "wikilink")[金馬獎最佳男主角](../Page/金馬獎.md "wikilink")。自此李修賢的警探形象逐漸定格下來。

1986年他成立「萬能影業」出任公司主席，出品了多部警匪片如《[霹靂先鋒](../Page/霹靂先鋒.md "wikilink")》、《[鐵血騎警](../Page/鐵血騎警.md "wikilink")》、《[壯志雄心](../Page/壯志雄心_\(電影\).md "wikilink")》、《[喋血双雄](../Page/喋血雙雄_\(1989年電影\).md "wikilink")》等等。其中《[喋血雙雄](../Page/喋血雙雄_\(1989年電影\).md "wikilink")》被稱為國際導演[吳宇森最完美的作品](../Page/吳宇森.md "wikilink")，1999年美國《時代周刊》將之列為「本世紀亞洲十大影片」之一。由於李修賢為一個擅長演出、編導、監製警匪片的專家，故此有「李Sir」之稱。在他旗下的萬能影業，曾挖堀了[成奎安](../Page/成奎安.md "wikilink")、[黃秋生與](../Page/黃秋生.md "wikilink")[周星馳三大巨星](../Page/周星馳.md "wikilink")。其中周星馳第一部影片《[霹靂先鋒](../Page/霹靂先鋒.md "wikilink")》正是由李修賢監製；成奎安惡漢形象也在那時奠定；而當時演藝生涯停滯不前的[黃秋生](../Page/黃秋生.md "wikilink")，更憑李修賢監製的《[八仙飯店之人肉叉燒包](../Page/八仙飯店之人肉叉燒包.md "wikilink")》一片，躍升成為[金像獎最佳男主角](../Page/香港電影金像獎.md "wikilink")。

九十年代電影市道持續不景氣，使李修賢在電影方面也少了投資。後期李修賢多數是在[加拿大](../Page/加拿大.md "wikilink")[溫哥華生活](../Page/溫哥華.md "wikilink")。至近年他開始進軍內地市場，也參與了電視連續劇的演出。他第一部連續劇是[無綫電視製作的](../Page/無綫電視.md "wikilink")《雷霆第一關》，與無綫鎮台之寶[汪明荃首度合作](../Page/汪明荃.md "wikilink")。

## 獎項

【第四屆香港電影金像獎最佳男主角】 —— 阿B 《公僕》 【第二十一屆金馬獎最佳男主角】—— 阿B 《公僕》

## 參與作品

### 電影

*以香港當地原始片名為主：*

#### 導演

| 年份   | 片名                                                 | 英譯片名                 |
| ---- | -------------------------------------------------- | -------------------- |
| 1981 | [單程路](../Page/單程路.md "wikilink")                   | One Way Only         |
| 1982 | [Friend過打Band](../Page/Friend過打Band.md "wikilink") | Funny Boys           |
| 1983 | [摩登衙門](../Page/摩登衙門.md "wikilink")                 | Oh My Cop            |
| 1984 | [公僕](../Page/公僕_\(電影\).md "wikilink")              | Law with Two Phases  |
| 1985 | [拖錯車](../Page/拖錯車.md "wikilink")                   | Cop Busters          |
| 1986 | [皇家飯](../Page/皇家飯.md "wikilink") 台譯:公家飯            | Law Enforcer         |
| 1990 | [鐵血騎警](../Page/鐵血騎警.md "wikilink")                 | Road Warriors        |
| 1991 | [龍的傳人](../Page/龍的傳人_\(電影\).md "wikilink")          | Legend Of The Dragon |
| 1992 | [羔羊醫生](../Page/羔羊醫生.md "wikilink")                 | Doctor Lamb          |
| 1995 | [賊王](../Page/賊王_\(電影\).md "wikilink")              | Twist                |

#### 監製

| 年份    | 片名                                              | 英譯片名                                  |
| ----- | ----------------------------------------------- | ------------------------------------- |
| 1988年 | [霹靂先鋒](../Page/霹靂先鋒.md "wikilink")              | Final Justice                         |
| 1989年 | [壯志雄心](../Page/壯志雄心_\(電影\).md "wikilink")       | Thank You Sir                         |
| 1990年 | [風雨同路](../Page/風雨同路_\(1990年電影\).md "wikilink")  | Unmatchable Match                     |
| 1990年 | [摩登襯家](../Page/摩登襯家.md "wikilink")              | It Takes Two Mingle                   |
| 1990年 | [朋黨](../Page/鐵血騎警II朋黨.md "wikilink") 台譯：鐵血騎警２朋黨 | Against All                           |
| 1991年 | [雷霆掃穴](../Page/雷霆掃穴.md "wikilink") 台譯：轟天至尊      | Red Shield                            |
| 1992年 | [車神](../Page/車神_\(電影\).md "wikilink")           | The Night Rider                       |
| 1992年 | [羔羊醫生](../Page/羔羊醫生.md "wikilink")              | Doctor Lamb                           |
| 1992年 | [伴我縱橫](../Page/伴我縱橫.md "wikilink")              | Rhythm of Destiny                     |
| 1993年 | [八仙飯店之人肉叉燒飽](../Page/八仙飯店之人肉叉燒飽.md "wikilink")  | The Untold Story                      |
| 1994年 | [初生之犢](../Page/初生之犢.md "wikilink")              | Fearless Match                        |
| 1995年 | [賊王](../Page/賊王_\(電影\).md "wikilink")           | Twist                                 |
| 1995年 | [月黑風高](../Page/月黑風高.md "wikilink") 台譯：掰王出差      | Case of The Cold Fish                 |
| 1995年 | [公僕２](../Page/公僕２.md "wikilink")                | City Cop 2                            |
| 1999年 | [化骨龍與千年蟲](../Page/化骨龍與千年蟲.md "wikilink") 台譯：龍與蟲 | He is enemy , partner & father in law |
| 1999年 | [四人幫之錢唔夠洗](../Page/四人幫之錢唔夠洗.md "wikilink")      | The Untold Story 3                    |
| 2002年 | [反收數特遣隊](../Page/反收數特遣隊.md "wikilink")          | Shark Buster                          |

#### 編劇

| 年份    | 片名                                                 | 英譯片名                           |
| ----- | -------------------------------------------------- | ------------------------------ |
| 1982年 | [Friend過打Band](../Page/Friend過打Band.md "wikilink") | Funny Boys                     |
| 1983年 | [摩登衙門](../Page/摩登衙門.md "wikilink")                 | Oh My Cop                      |
| 1984年 | [公僕](../Page/公僕_\(電影\).md "wikilink")              | Law with Two Phases            |
| 1986年 | [皇家飯](../Page/皇家飯.md "wikilink") 台譯：公家飯            | Law Enforcer                   |
| 1987年 | [鐵血騎警](../Page/鐵血騎警.md "wikilink")                 | Road Warriors                  |
| 1994年 | [Ｏ記重案實錄](../Page/Ｏ記重案實錄.md "wikilink") 台譯：重案實錄     | Organized Crime & Triad Bureau |
| 1995年 | [賊王](../Page/賊王_\(電影\).md "wikilink")              | Twist                          |

#### 演員

| 年份    | 片名                                                       | 英譯片名                                  |
| ----- | -------------------------------------------------------- | ------------------------------------- |
| 1971年 | [雙俠](../Page/雙俠.md "wikilink")（客串）                       | The Deadly Duo                        |
| 1972年 | [愛情雷風雨](../Page/愛情雷風雨.md "wikilink")                     |                                       |
| 1972年 | [大刀王五](../Page/大刀王五.md "wikilink")                       | Iron Bodyguard                        |
| 1972年 | [水滸傳](../Page/水滸傳_\(1972年電影\).md "wikilink")（客串）         | Water Margin                          |
| 1973年 | [刺馬](../Page/刺馬.md "wikilink")（客串）                       | The Blood Brother                     |
| 1973年 | [七十二家房客](../Page/七十二家房客_\(1973年電影\).md "wikilink")（客串）   | House of 72 Tenants                   |
| 1973年 | [蕩寇誌](../Page/蕩寇誌.md "wikilink")                         | All Men are Brothers                  |
| 1973年 | [江湖行](../Page/江湖行.md "wikilink")                         | River of Fury                         |
| 1974年 | [香港73](../Page/香港73.md "wikilink")                       | Hong Kong 73                          |
| 1974年 | [小孩與狗](../Page/小孩與狗.md "wikilink")                       | Love                                  |
| 1974年 | [成記茶樓](../Page/成記茶樓.md "wikilink")                       | The Tea House                         |
| 1974年 | [五虎將](../Page/五虎將_\(電影\).md "wikilink")                  | Savage Five                           |
| 1974年 | [多咀街](../Page/多咀街.md "wikilink")                         | Gossip Street                         |
| 1974年 | [愛心千萬萬](../Page/愛心千萬萬.md "wikilink")                     | It's All In The Family                |
| 1975年 | [搭錯線](../Page/搭錯線.md "wikilink")                         | All Mixed Up                          |
| 1975年 | [惡霸](../Page/惡霸.md "wikilink")                           | Gambling Syndicate                    |
| 1975年 | [七面人](../Page/七面人.md "wikilink")                         | The Imposter                          |
| 1975年 | [中國超人](../Page/中國超人.md "wikilink")                       | The Super Inframan                    |
| 1975年 | [嬉．笑．怒．罵](../Page/嬉．笑．怒．罵.md "wikilink")                 | Temperament of Life                   |
| 1975年 | [大老千](../Page/大老千.md "wikilink")                         | Queen Hustler                         |
| 1975年 | [大劫案](../Page/大劫案.md "wikilink")                         | Big Hold Up                           |
| 1975年 | [李小龍與我](../Page/李小龍與我.md "wikilink")                     | Bruce Lee and I                       |
| 1976年 | [流星蝴蝶劍](../Page/流星蝴蝶劍_\(電影\).md "wikilink")              | Killer Clans                          |
| 1976年 | [無法無天飛車黨](../Page/無法無天飛車黨.md "wikilink")（客串）             | Killers on Wheels                     |
| 1976年 | [色香味](../Page/色香味.md "wikilink")                         | Erotic Nights                         |
| 1976年 | [油鬼子](../Page/油鬼子_\(電影\).md "wikilink")                  | Oily Maniac                           |
| 1977年 | [包剪碴](../Page/包剪碴.md "wikilink") 台譯：石頭剪刀布                | Melody of Love                        |
| 1977年 | [猩猩王](../Page/猩猩王.md "wikilink")                         | The Mighty Peking Man                 |
| 1977年 | [天龍八部](../Page/天龍八部_\(1977年電影\).md "wikilink")           | The Battle Wizard                     |
| 1977年 | [幽靈](../Page/幽靈_\(電影\).md "wikilink")（客串）                |                                       |
| 1977年 | [應召名冊](../Page/應召名冊.md "wikilink")                       | Call Girls                            |
| 1977年 | [射鵰英雄傳](../Page/射鵰英雄傳_\(1977年電影\).md "wikilink")         | The Brave Archer                      |
| 1978年 | [子曰：食色性也](../Page/子曰：食色性也.md "wikilink")（客串）             | Sensual Pleasures                     |
| 1978年 | [射鵰英雄傳續集](../Page/射鵰英雄傳續集.md "wikilink")                 | The Brave Archer 2                    |
| 1978年 | [不擇手段](../Page/不擇手段.md "wikilink")                       |                                       |
| 1979年 | [差人、大佬、博命仔](../Page/差人、大佬、博命仔.md "wikilink")             | The Brothers                          |
| 1979年 | [大命撈家](../Page/大命撈家.md "wikilink")                       | He Who Never Dies                     |
| 1979年 | [三山五嶽人馬](../Page/三山五嶽人馬.md "wikilink")                   |                                       |
| 1979年 | [孔雀皇朝](../Page/孔雀皇朝.md "wikilink")                       |                                       |
| 1980年 | [少林英雄](../Page/少林英雄.md "wikilink")                       | The Shaolin Heroes                    |
| 1980年 | [風流殘劍血無痕](../Page/風流殘劍血無痕.md "wikilink")                 | Mask of Vengeance                     |
| 1980年 | [八絕](../Page/八絕.md "wikilink")（客串）                       |                                       |
| 1980年 | [金手指](../Page/金手指_\(電影\).md "wikilink")                  | The Informer                          |
| 1981年 | [單程路](../Page/單程路_\(電影\).md "wikilink")                  | One Way Only                          |
| 1981年 | [刺客列傳](../Page/刺客列傳.md "wikilink")                       | Night of The Assassins                |
| 1981年 | [小李飛刀](../Page/小李飛刀_\(電影\).md "wikilink")                | Flying Sword Lee                      |
| 1981年 | [王牌大老千](../Page/王牌大老千.md "wikilink")                     | The Great Cheat                       |
| 1981年 | [流氓千王](../Page/流氓千王.md "wikilink")                       | Gambler's Delights                    |
| 1981年 | [頂爺](../Page/頂爺.md "wikilink")                           | Big Boss                              |
| 1981年 | [執法者別名](../Page/執法者.md "wikilink")：警網雙雄                  | The Executor                          |
| 1982年 | [踩綫](../Page/踩綫.md "wikilink")                           | Murderer Pursues                      |
| 1982年 | [獸心](../Page/獸心.md "wikilink")                           | Behind The Storm                      |
| 1982年 | [Friend過打Band](../Page/Friend過打Band.md "wikilink")       | Funny Boys                            |
| 1982年 | [沙煲兄弟](../Page/沙煲兄弟.md "wikilink")》                      | Dirty Angel                           |
| 1982年 | [神鵰俠侶](../Page/神鵰俠侶_\(1982年電影\).md "wikilink") （客串）      | The Brave Archer and His Mate         |
| 1982年 | [龍虎雙霸天](../Page/龍虎雙霸天.md "wikilink")                     |                                       |
| 1982年 | [殺出西營盤](../Page/殺出西營盤.md "wikilink")（客串）                 | Coolie Killer                         |
| 1982年 | [賭王千王群英會](../Page/賭王千王群英會.md "wikilink")                 | The Stunning Gambling                 |
| 1983年 | [佳人有約](../Page/佳人有約_\(電影\).md "wikilink")（客串）            | Perfect Match                         |
| 1983年 | [叔侄縮窒](../Page/叔侄縮窒.md "wikilink")                       | The Sensational Pair                  |
| 1983年 | [摩登衙門](../Page/摩登衙門.md "wikilink")                       | Oh My Cop                             |
| 1984年 | [公僕](../Page/公僕_\(電影\).md "wikilink")                    | Law With Two Phases                   |
| 1984年 | [大小不良](../Page/大小不良.md "wikilink")                       | Double Trouble                        |
| 1985年 | [上海灘十三太保](../Page/上海灘十三太保.md "wikilink")（客串）             | All The Professional                  |
| 1985年 | [花心紅杏](../Page/花心紅杏.md "wikilink")                       | Fascinating Affairs                   |
| 1985年 | [流氓公僕](../Page/流氓公僕.md "wikilink") 台譯：公僕與公敵              | Cop of The Town                       |
| 1985年 | [吉人天相](../Page/吉人天相.md "wikilink")                       | Chase a Fortune                       |
| 1986年 | [代客泊車](../Page/代客泊車.md "wikilink") 台譯：代客停車               | Parking Service                       |
| 1986年 | [皇家飯](../Page/皇家飯.md "wikilink") 台譯：公家飯                  | Law Enforcer                          |
| 1986年 | [兄弟](../Page/兄弟_\(1986年電影\).md "wikilink")               | The Brotherhood                       |
| 1986年 | [流氓英雄](../Page/流氓英雄.md "wikilink")                       | The Innocent Interloper               |
| 1987年 | [大蛇王](../Page/大蛇王.md "wikilink")                         | King of Snake                         |
| 1987年 | [義本無言](../Page/義本無言.md "wikilink")（客串）                   | Code of Honour                        |
| 1987年 | [江湖情](../Page/江湖情.md "wikilink")（客串）                     | Rich and Famous                       |
| 1987年 | [英雄好漢](../Page/英雄好漢.md "wikilink")                       | Tragic Hero                           |
| 1987年 | [鐵血騎警](../Page/鐵血騎警.md "wikilink")                       | Road Warriors                         |
| 1987年 | [龍虎風雲](../Page/龍虎風雲.md "wikilink")                       | City on Fire                          |
| 1988年 | [龍虎智多星](../Page/龍虎智多星.md "wikilink")                     | Criminal Hunter                       |
| 1988年 | [赤膽情](../Page/赤膽情.md "wikilink")                         | No Compromise                         |
| 1988年 | [霹靂先鋒](../Page/霹靂先鋒.md "wikilink")                       | Final Justice                         |
| 1989年 | [新最佳拍檔](../Page/新最佳拍檔.md "wikilink") 台譯：新最佳拍檔之兵馬俑風雲 （客串） | Aces Go Place 5                       |
| 1989年 | [義膽群英](../Page/義膽群英.md "wikilink")                       | Just Heros                            |
| 1989年 | [喋血雙雄](../Page/喋血雙雄_\(1989年電影\).md "wikilink")           | The Killer                            |
| 1989年 | [壯志雄心](../Page/壯志雄心_\(電影\).md "wikilink")                | Thank You Sir                         |
| 1990年 | [喋血江湖](../Page/喋血江湖.md "wikilink")                       | No Way Back                           |
| 1990年 | [風雨同路](../Page/風雨同路_\(1990年電影\).md "wikilink") （客串）      | Unmatchable Match                     |
| 1990年 | [摩登襯家](../Page/摩登襯家.md "wikilink")》                      | Take's Two to Mingle                  |
| 1990年 | [聖戰風雲](../Page/聖戰風雲.md "wikilink")                       | Undeclared War                        |
| 1990年 | [老虎出更2](../Page/老虎出更2.md "wikilink")                     | Tiger On Beat 2                       |
| 1990年 | [絕橋智多星](../Page/絕橋智多星.md "wikilink") 台譯：龍虎大老千            | Big Score                             |
| 1990年 | [朋黨](../Page/鐵血騎警II朋黨.md "wikilink") 台譯：鐵血騎警２朋黨          | Against All                           |
| 1991年 | [雷霆掃穴](../Page/雷霆掃穴.md "wikilink") 台譯：轟天至尊               | Red Shield                            |
| 1991年 | [藍色霹靂火](../Page/藍色霹靂火.md "wikilink")                     | Blue Lighting                         |
| 1992年 | [戰龍在野](../Page/戰龍在野.md "wikilink")                       | Invincible                            |
| 1992年 | [四大探長](../Page/四大探長_\(電影\).md "wikilink")                | Powerful Four                         |
| 1992年 | [車神](../Page/車神_\(電影\).md "wikilink")                    | The Night Rider                       |
| 1992年 | [羔羊醫生](../Page/羔羊醫生.md "wikilink")                       | Doctor Lamb                           |
| 1992年 | [伴我縱橫](../Page/伴我縱橫.md "wikilink")                       | Rhythm of Destiny                     |
| 1993年 | [八仙飯店之人肉叉燒飽](../Page/八仙飯店之人肉叉燒飽.md "wikilink")           | The Untold Story                      |
| 1993年 | [虐之戀](../Page/虐之戀.md "wikilink") 台譯：虐妻                   | Love To Kill                          |
| 1993年 | [新碧血劍](../Page/新碧血劍.md "wikilink")                       | The Sword Stained With Royal Blood    |
| 1993年 | [機密檔案之烏鼠](../Page/機密檔案之烏鼠.md "wikilink")（客串）             | Run and Kill                          |
| 1994年 | [Ｏ記重案實錄](../Page/Ｏ記重案實錄.md "wikilink") 台譯：重案實錄           | Organized Crime & Triad Bureau        |
| 1994年 | [初生之犢](../Page/初生之犢.md "wikilink")                       | Fearless Match                        |
| 1995年 | [賊王](../Page/賊王_\(電影\).md "wikilink")                    | Twist                                 |
| 1995年 | [月黑風高](../Page/月黑風高.md "wikilink") 台譯：掰王出差 （客串）          | Case of The Cold Fish                 |
| 1995年 | [特警急先鋒](../Page/特警急先鋒.md "wikilink")                     | Asian Connection                      |
| 1995年 | [公僕2](../Page/公僕2.md "wikilink")                         | City Cop 2                            |
| 1996年 | [金榜提名](../Page/金榜提名.md "wikilink") 台譯：英雄太保               | To be Number One                      |
| 1997年 | [奪舍](../Page/奪舍.md "wikilink")                           | Walk In                               |
| 1998年 | [98古惑仔之龍爭虎鬥](../Page/98古惑仔之龍爭虎鬥.md "wikilink")（客串）       | Young and Dangerous                   |
| 1999年 | [化骨龍與千年蟲](../Page/化骨龍與千年蟲.md "wikilink") 台譯：龍與蟲          | He Is Enemy , Partner & Father In Law |
| 2000年 | [七條命](../Page/七條命.md "wikilink")                         | Untouchable Maniac                    |
| 2000年 | [旺角的天空３終極邊緣](../Page/旺角的天空３終極邊緣.md "wikilink")（客串）       | Man Wanted 3                          |
| 2000年 | [白色風暴](../Page/白色風暴.md "wikilink")                       | White Storm                           |
| 2000年 | [浪漫鎗聲](../Page/浪漫鎗聲.md "wikilink")                       | Romantic Bullet                       |
| 2000年 | [黑社會.com](../Page/黑社會.com.md "wikilink")                 | Mafia.com                             |
| 2000年 | [天殺](../Page/天殺.md "wikilink")                           | Killer With The Lonely Heart          |
| 2001年 | [野獸童黨２之少年鎗黨](../Page/野獸童黨２之少年鎗黨.md "wikilink")           | Hong Kong History Y                   |
| 2001年 | [英雄人物](../Page/英雄人物.md "wikilink")                       | Hero of The City                      |
| 2001年 | [歲月風雲](../Page/歲月風雲_\(電影\).md "wikilink")                |                                       |
| 2002年 | [狂野臥底](../Page/狂野臥底.md "wikilink")                       | Psychedelic Cop                       |
| 2002年 | [反收數特遣隊](../Page/反收數特遣隊.md "wikilink")                   | Shark Buster                          |
| 2003年 | [失鎗72小時](../Page/失鎗72小時.md "wikilink")                   | Unarm 72 Hours                        |
| 2003年 | [九龍的天空之墮落街](../Page/九龍的天空之墮落街.md "wikilink")             | A Fallen Angel                        |
| 2003年 | [九龍的天空之錯愛馬伕](../Page/九龍的天空之錯愛馬伕.md "wikilink")           | Seductive Love                        |
| 2004年 | [重案黐孖GUN](../Page/重案黐孖GUN.md "wikilink") 台譯：衝鋒陷陣         | Heat Team                             |
| 2004年 | [極速先鋒](../Page/極速先鋒.md "wikilink")                       | The Vegeance Of Laer                  |
| 2005年 | [逃亡者的末日](../Page/逃亡者的末日.md "wikilink")                   | A Deadline of Ten Days                |
| 2008年 | [奪帥](../Page/奪帥.md "wikilink")                           | Fatal Move                            |
| 2008年 | [花花型警](../Page/花花型警.md "wikilink")（客串）                   | Playboy Cops                          |
| 2009年 | [红河](../Page/红河_\(电影\).md "wikilink")                    |                                       |
| 2010年 | [月滿軒尼詩](../Page/月滿軒尼詩.md "wikilink")                     |                                       |

#### 策劃

  - 1985年 《[流氓公僕](../Page/流氓公僕.md "wikilink")》（台譯：公僕與公敵）Cop of The Town
  - 1986年 《[代客泊車](../Page/代客泊車.md "wikilink")》（台譯：代客停車）Parking Service

#### 武術指導

  - 1982年 《[獸心](../Page/獸心.md "wikilink")》 Behind the Storm

### 電視劇

*以香港當地原始片名為主：*

#### 演員

|        |                                                      |          |
| ------ | ---------------------------------------------------- | -------- |
| **年代** | **片名**                                               | **飾演**   |
| 1993年  | [極度凶靈之入夢](../Page/極度凶靈之入夢.md "wikilink")             |          |
| 1995年  | [重案實錄](../Page/重案實錄.md "wikilink")                   | 李Sir     |
| 2000年  | [雷霆第一關](../Page/雷霆第一關.md "wikilink")                 | \-{涂}-令山 |
| 2000年  | [南國大案](../Page/南國大案.md "wikilink")                   |          |
| 2001年  | [雄心密令](../Page/雄心密令.md "wikilink")                   | 高秋       |
| 2001年  | [義蓋雲天](../Page/義蓋雲天.md "wikilink")                   | 龍浩雲      |
| 2003年  | [雄心密令](../Page/雄心密令.md "wikilink")                   | 高天祐      |
| 2003年  | [執法群英II](../Page/執法群英II.md "wikilink")               | 曾國明      |
| 2006年  | [生死孽戀](../Page/生死孽戀.md "wikilink")                   | 高秋       |
| 2007年  | [福星遊龍二王爺](../Page/福星遊龍二王爺.md "wikilink")             | 葛坤       |
| 2007年  | [廉政行動2007](../Page/廉政行動2007.md "wikilink")           | 簡Sir     |
| 2008年  | [東邊西邊](../Page/東邊西邊.md "wikilink")                   |          |
| 2012年  | [百媚千嬌](../Page/百媚千嬌.md "wikilink")                   |          |
| 2013年  | 婚姻之痒(2008年拍攝)                                        | 陈博之      |
| 2013年  | 铁腕行动                                                 |          |
| 2014年  | [血色迷情](../Page/血色迷情.md "wikilink")                   |          |
| 2014年  | [湄公河大案](../Page/湄公河大案.md "wikilink")                 | 蒙洪       |
| 2015年  | [我的青春道馆](../Page/我的青春道馆.md "wikilink")(网剧)           | 律师       |
| 2015年  | [警察日记](../Page/警察日记.md "wikilink")(2010年拍攝)          |          |
| 2015年  | [迫在眉睫](../Page/迫在眉睫_\(電視劇\).md "wikilink")           | 贺台烟      |
| 2016年  | [浴血重生](../Page/浴血重生.md "wikilink")                   | 古亭轩      |
| 2016年  | [卿本佳人](../Page/卿本佳人.md "wikilink")                   | 商父(客串)   |
| 2017年  | [暗戰風雲](../Page/暗戰風雲.md "wikilink")(2013年拍攝,原名:站在你背後) | 张子全      |
| 2017年  | [新刑警本色](../Page/新刑警本色.md "wikilink")                 | 白局       |

## 参考文献

## 外部連結

  -
  -
  -
  -
  -
  -
{{-}}

[港](../Category/金馬獎最佳男主角獲得者.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港導演](../Category/香港導演.md "wikilink")
[Category:香港電影導演](../Category/香港電影導演.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:大温华裔加拿大人](../Category/大温华裔加拿大人.md "wikilink")
[Category:溫哥華人](../Category/溫哥華人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:汕头人](../Category/汕头人.md "wikilink")
[Sau](../Category/李姓.md "wikilink")
[Category:瑪利諾中學校友](../Category/瑪利諾中學校友.md "wikilink")

1.  [瑪利諾中學 30周年校慶特刊，第46頁，1996年](http://www.mssch.edu.hk/publication/40th/image102.html)
2.  [1](https://hk.lifestyle.appledaily.com/nextplus/magazine/article/20170820/2_540641_0/-%E6%9C%89%E5%9E%8B%E6%9D%8ESir%E5%8E%BB%E5%92%97%E9%82%8A-65%E6%AD%B2%E5%8D%8A%E7%A7%83%E6%9D%8E%E4%BF%AE%E8%B3%A2%E8%B8%8E%E8%8C%B6%E9%A4%90%E5%BB%B3%E5%A5%BD%E9%BA%BB%E7%94%A9-)
3.