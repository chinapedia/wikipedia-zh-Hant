**李偉文**（**Lee Wai
Man**，1973年8月18日－），生於[香港](../Page/香港.md "wikilink")，已退役[香港足球運動員](../Page/香港.md "wikilink")，是[香港球壇上少有的中後場全能球員](../Page/香港.md "wikilink")，其左右腳均技術純熟而且具備良好的心理質素，曾5次入選[香港足球明星選舉最佳十一人](../Page/香港足球明星選舉.md "wikilink")，前[香港足球代表隊隊長並以](../Page/香港足球代表隊.md "wikilink")68場國際A級賽上陣紀錄正式其終結[香港代表隊生涯](../Page/香港足球代表隊.md "wikilink")，曾是代表[香港次數最多的紀錄保持者](../Page/香港足球代表隊.md "wikilink")，現為[香港足球教練及無綫電視足球評述員](../Page/香港.md "wikilink")。

## 球員生涯

李偉文生於[香港](../Page/香港.md "wikilink")，父親為[警察訓練學校教官](../Page/警察訓練學校.md "wikilink")，他畢業於[港島區學界體育名校](../Page/港島區.md "wikilink")[聖若瑟書院](../Page/聖若瑟書院.md "wikilink")，於11歲時獲得[銀禧體育中心選出進行職業訓練](../Page/銀禧體育中心.md "wikilink")，由於擁有天份很快就獲時任教練[黎新祥安排與](../Page/黎新祥.md "wikilink")[丘建威隨大兩年的](../Page/丘建威.md "wikilink")[歐偉倫及](../Page/歐偉倫.md "wikilink")[譚兆偉等一同操練](../Page/譚兆偉.md "wikilink")，李偉文本來打算於畢業後跟從父親投考加入[香港警務處](../Page/香港警務處.md "wikilink")，然而後來於17歲之時加盟[麗新青年軍](../Page/花花足球會.md "wikilink")，翌年轉投[東方以](../Page/東方足球隊.md "wikilink")18歲之齡獲時任教練[陳鴻平選派其擔任正選](../Page/陳鴻平.md "wikilink")[左後衛其生涯從此平步青雲](../Page/左後衛.md "wikilink")，於1993年1月舉行的[第15屆省港盃](../Page/省港盃.md "wikilink")，未滿20歲的李偉文首次入選[香港足球代表隊並且在兩回合賽事均有上陣](../Page/香港足球代表隊.md "wikilink")，李偉文合共代表[香港代表隊上陣](../Page/香港足球代表隊.md "wikilink")68場取得兩個入球，曾是代表[香港隊次數最多的紀錄保持者](../Page/香港足球代表隊.md "wikilink")（紀錄於2017年6月7日被[李志豪打破](../Page/李志豪_\(足球運動員\).md "wikilink")），他於2007年正式宣佈退出[香港隊](../Page/香港足球代表隊.md "wikilink")，其後曾效力[流浪](../Page/香港流浪足球會.md "wikilink")、[星島及](../Page/星島體育會.md "wikilink")[愉園等多支](../Page/愉園體育會.md "wikilink")[港甲球會](../Page/港甲.md "wikilink")，在球會和[香港隊的球衣都是](../Page/香港足球代表隊.md "wikilink")18號。

## 教練生涯

2007年，李偉文應前[流浪及](../Page/香港流浪足球會.md "wikilink")[愉園班主](../Page/愉園體育會.md "wikilink")[林大輝邀請](../Page/林大輝.md "wikilink")，加盟當時仍在[丙組聯賽角逐的](../Page/香港丙組\(地區\)足球聯賽.md "wikilink")[沙田擔任教練兼球員](../Page/沙田體育會足球隊.md "wikilink")，成功帶領球隊實現「三年計劃」升上[甲組聯賽角逐](../Page/香港甲組足球聯賽.md "wikilink")，於2010年夏天轉投[乙組球隊](../Page/香港乙組足球聯賽.md "wikilink")[港迪擔任教練](../Page/淦源足球會.md "wikilink")，李偉文在2010年開始執教地區青少年球隊[東區U17](../Page/東區體育會.md "wikilink")，於2010/11球季成功帶領[東區殺入有多支球會青年軍參賽的NIKE超級盃](../Page/東區體育會.md "wikilink")8強，他擅於與青少年溝通深受年青球員愛戴，在2015年開始執教學校[優才書院](../Page/優才（楊殷有娣）書院.md "wikilink")，並帶領該校丙組於2016年奪得學界亞軍，至於乙組惜於小組賽末場以0–3大敗於[聖芳濟書院未能進入淘汰賽](../Page/聖芳濟書院_\(香港\).md "wikilink")，於2017年帶領[優才書院乙組足球隊殺入四強並奪得殿軍](../Page/優才（楊殷有娣）書院.md "wikilink")，同年帶領[優才書院丙組再次進軍足球學界決賽](../Page/優才（楊殷有娣）書院.md "wikilink")，成功以2–0擊敗保良局唐乃勤初中書院奪得冠軍。

## 個人生活

在2012年，李偉文透過[港協暨奧委會的](../Page/中國香港體育協會暨奧林匹克委員會.md "wikilink")《香港運動員就業及教育計劃》接受職業配對獲得[香港駕駛學院聘請成為駕駛教練](../Page/香港駕駛學院.md "wikilink")，退役後擔任足球教練及球賽評述員，亦是教車師傅及[的士司機](../Page/的士司機.md "wikilink")，近年亦有從事金融投資，李偉文於2017年5月25日晚近9時步出[紅磡](../Page/紅磡.md "wikilink")[置富都會商場時突遭](../Page/置富都會.md "wikilink")6至7名[南亞裔持刀男子襲擊並搶去其行李篋](../Page/南亞裔.md "wikilink")，李偉文不甘損失與賊人糾纏惟寡不敵眾，賊人得手後登上一輛客貨車逃去，據悉被搶去一個內有1000萬[日圓現金約值](../Page/日圓.md "wikilink")70萬[港元以及](../Page/港元.md "wikilink")24條約值750萬元金條，事後李偉文受輕傷初步敷治後毋須送院，最終4名[南亞裔男子於](../Page/南亞裔.md "wikilink")2017年6月6日被補。\[1\]

曾任[無綫電視足球評述](../Page/無綫電視.md "wikilink")，2019年3月曾結束合約。不過因無綫電視收費頻道Mytv
Gold再次有足球體育頻道上架關係, 再次回歸為[無綫電視作足球評論員](../Page/無綫電視.md "wikilink")。

## 個人榮譽

  - [香港足總盃](../Page/香港足總盃.md "wikilink") 最佳後衛（2004、2005年）
  - [皇家馬德里](../Page/皇家馬德里.md "wikilink")[亞洲之旅](../Page/亞洲.md "wikilink")[香港站](../Page/香港.md "wikilink")
    最有價值球員（2003年）
  - [香港足球明星選舉](../Page/香港足球明星選舉.md "wikilink")
    最佳十一人（1993–94、1994–95、2003–04、2004–05、2005–06季度）
  - [香港傑出運動員選舉](../Page/香港傑出運動員選舉.md "wikilink") 傑出青少年運動（1991、2003年）

## 電視劇

  - [三個女人一個「因」](../Page/三個女人一個「因」.md "wikilink")

## 參考資料

## 外部連結

  - [香港足球總會網頁球員註冊資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=102)

[category:東方球員](../Page/category:東方球員.md "wikilink")
[category:流浪球員](../Page/category:流浪球員.md "wikilink")
[category:星島球員](../Page/category:星島球員.md "wikilink")
[category:愉園球員](../Page/category:愉園球員.md "wikilink")
[category:沙田球員](../Page/category:沙田球員.md "wikilink")

[Category:李姓](../Category/李姓.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:麗新球員](../Category/麗新球員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:沙田教練](../Category/沙田教練.md "wikilink")
[Category:淦源教練](../Category/淦源教練.md "wikilink")
[Category:聖若瑟書院校友](../Category/聖若瑟書院校友.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港體院足球運動員](../Category/香港體院足球運動員.md "wikilink")
[Category:香港仔工業學校校友](../Category/香港仔工業學校校友.md "wikilink")
[Category:香港足球代表隊隊長](../Category/香港足球代表隊隊長.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")

1.  [前港腳李偉文遇劫　南亞幫擸24條金及巨款](http://hk.on.cc/hk/bkn/cnt/news/20170524/bkn-20170524214805776-0524_00822_001.html)
    【on.cc東網】2017年05月24日