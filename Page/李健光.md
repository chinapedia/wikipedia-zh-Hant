**李健光**（，），[台灣](../Page/台灣.md "wikilink")[宜蘭縣人](../Page/宜蘭縣.md "wikilink")，前台灣[英語](../Page/英語.md "wikilink")[電視新聞](../Page/電視新聞.md "wikilink")[主播](../Page/主播.md "wikilink")。

現已離開新聞界，以接公關主持活動及現場口譯為主。

## 學歷

  - [天母](../Page/天母.md "wikilink")[台北美國學校](../Page/台北美國學校.md "wikilink")
  - Curtin大學
  - 澳洲Deakin大學

## 經歷

  - [中視](../Page/中視.md "wikilink")《中視英語新聞》主播
  - [CNN台灣特派](../Page/CNN.md "wikilink")[記者](../Page/記者.md "wikilink")
  - [新傳媒電視台灣特派記者](../Page/新傳媒電視.md "wikilink")
  - [東森亞洲衛視主播](../Page/東森亞洲衛視.md "wikilink")
  - [東森新聞台](../Page/東森新聞台.md "wikilink")《東森早安新聞》東森新聞台
  - 東森新聞台《東森整點新聞》主播
  - [東森電視新聞部國際新聞中心](../Page/東森電視.md "wikilink")[編譯](../Page/編譯.md "wikilink")
  - [東森財經新聞台](../Page/東森財經新聞台.md "wikilink")《東森英語新聞》主播
  - [2017年夏季世界大學運動會](../Page/2017年夏季世界大學運動會.md "wikilink")《開幕、閉幕式》司儀

## 佚事

  - 2008年初，在為[艷照門事件中](../Page/艷照門事件.md "wikilink")[陳冠希記者會致歉宣言的同步](../Page/陳冠希.md "wikilink")[翻譯中](../Page/翻譯.md "wikilink")，情緒激昂，效果夸張，被廣大網友更加熟識。
  - 2009年7月至8月，擔任[華視訓練中心](../Page/華視訓練中心.md "wikilink")「英語小主播[夏令營](../Page/夏令營.md "wikilink")」講師。

## 參考資料

## 外部連結

[J健](../Category/李姓.md "wikilink")
[Category:宜蘭人](../Category/宜蘭人.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:台灣電視主播](../Category/台灣電視主播.md "wikilink")
[Category:中視主播](../Category/中視主播.md "wikilink")
[Category:東森主播](../Category/東森主播.md "wikilink")