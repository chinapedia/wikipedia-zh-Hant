**李兆基**（，），[香港電影演員](../Page/香港電影.md "wikilink")，以專門飾演黑幫人物而見稱。

## 生平

李兆基外號「高飛」，出道前（1970年代）曾為童黨組織「[慈雲山十三太保](../Page/慈雲山十三太保.md "wikilink")」成員之一\[1\]\[2\]，曾有吸毒紀錄，後來戒毒成功，重投社會。1980年代他被導演[林嶺東邀請演出電影](../Page/林嶺東.md "wikilink")，成功脫離黑道，也曾在[無綫電視劇集參與演出](../Page/無綫電視.md "wikilink")。

人稱「基哥」的李兆基因著其出身背景，加以外形甚具草根味道，因之他飾演的電影角色，一般都是一些有勇無謀的人物，或者黑幫小頭目，甚少演出大人物。他多數都是配角，最為人熟悉的演出是《[古惑仔電影系列](../Page/古惑仔電影系列.md "wikilink")》裡的墻頭草「吹水基」。同時他也在[周星馳電影中亮相](../Page/周星馳.md "wikilink")，如《[食神](../Page/食神_\(1996年電影\).md "wikilink")》、《[算死草](../Page/算死草.md "wikilink")》、《[行運一條龍](../Page/行運一條龍.md "wikilink")》。

因為他的黑道外形和演技，與[黃光亮](../Page/黃光亮.md "wikilink")、[成奎安和](../Page/成奎安.md "wikilink")[何家駒三人並列為](../Page/何家駒_\(香港演員\).md "wikilink")「[香港影圈四大惡人](../Page/香港影圈四大惡人.md "wikilink")」。

近年他不但是電影演員，也是一間電影投資公司的負責人，任監製、編劇等等，題材多與黑道或江湖有關。

約於2015年開始因[中風而鮮有在大型影視節目露面](../Page/中風.md "wikilink")\[3\]。

## 演出作品

### 電影

  - 《[唐伯虎點秋香2之四大才子](../Page/唐伯虎點秋香2之四大才子.md "wikilink")》飾 基老爺
  - 《[最危險人物](../Page/最危險人物.md "wikilink")》飾 肥波
  - 《[妃子笑](../Page/妃子笑.md "wikilink")》飾 武狀元
  - 《[殺人度假屋](../Page/殺人度假屋.md "wikilink")》
  - 《[奸人世家](../Page/奸人世家.md "wikilink")》
  - 《[千王之王2000](../Page/千王之王2000.md "wikilink")》飾 基哥
  - 《[江湖告急](../Page/江湖告急.md "wikilink")》飾 基哥
  - 《[賭俠1999](../Page/賭俠1999.md "wikilink")》飾 DEE王基
  - 《[黑獄斷腸歌2之無期徒刑](../Page/黑獄斷腸歌2.md "wikilink")》 飾
    囚犯基哥、李兆基(電影開頭數分鐘由講解香港監獄制度)
  - 《[龍在邊緣](../Page/龍在邊緣.md "wikilink")》
  - 《[旺角靚妹仔](../Page/旺角靚妹仔.md "wikilink")》
  - 《[喜劇之王](../Page/喜劇之王.md "wikilink")》飾 基哥
  - 《[夜半無人屍語時](../Page/夜半無人屍語時.md "wikilink")》
  - 《[龍在江湖](../Page/龍在江湖.md "wikilink")》
  - 《[風雲之雄霸天下](../Page/風雲之雄霸天下.md "wikilink")》飾 武當子
  - 《[新古惑仔之少年激鬥篇](../Page/新古惑仔之少年激鬥篇.md "wikilink")》飾 吹水基（基哥）
  - 《[行運一條龍](../Page/行運一條龍.md "wikilink")》
  - 《[愛您愛到殺死您](../Page/愛您愛到殺死您.md "wikilink")》
  - 《[精裝難兄難弟](../Page/精裝難兄難弟.md "wikilink")》
  - 《愛上百分百英雄》
  - 《[超級無敵追女仔 II之狗仔雄心](../Page/超級無敵追女仔_II之狗仔雄心.md "wikilink")》
  - 《[高度戒備](../Page/高度戒備.md "wikilink")》
  - 《[香港奇案之強姦](../Page/香港奇案之強姦.md "wikilink")》（台譯：赤裸羔羊2性追緝令）Raped by an
    Angel
  - 《[算死草](../Page/算死草.md "wikilink")》飾 洪十八
  - 《[黑獄斷腸歌之砌生豬肉](../Page/黑獄斷腸歌之砌生豬肉.md "wikilink")》 飾 囚犯口水南
  - 《[古惑仔3之隻手遮天](../Page/古惑仔3之隻手遮天.md "wikilink")》 飾 吹水基（基哥）
  - 《[食神](../Page/食神_\(1996年電影\).md "wikilink")》 飾 鵝頭
  - 《[紅燈區](../Page/紅燈區_\(電影\).md "wikilink")》
  - 《[一千零一夜之夢中人](../Page/一千零一夜之夢中人.md "wikilink")》
  - 《[慈雲山十三太保](../Page/慈雲山十三太保.md "wikilink")》
  - 《[人皮燈籠](../Page/人皮燈籠.md "wikilink")》
  - 《[至尊三十六計之偷天換日](../Page/至尊三十六計之偷天換日.md "wikilink")》飾 囚犯基哥
  - 《[五億探長雷洛傳](../Page/五億探長雷洛傳.md "wikilink")》飾 警校教官
  - 《[摩登如來神掌](../Page/摩登如來神掌.md "wikilink")》飾 高籬八
  - 《[奪命金](../Page/奪命金.md "wikilink")》飾 基哥
  - 《[O記三合會檔案](../Page/O記三合會檔案.md "wikilink")》飾 肥寶
  - 《[超級學校霸王](../Page/超級學校霸王.md "wikilink")》飾 水電維修工人
  - 《[港督最後一個保鏢](../Page/港督最後一個保鏢.md "wikilink")》飾 沙膽飛
  - 《[賭城大亨II之至尊無敵](../Page/賭城大亨II之至尊無敵.md "wikilink")》飾 寸標

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>劇名</p></th>
<th><p>角色</p></th>
<th><p>播映電視台</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/霓虹姊妹花.md" title="wikilink">霓虹姊妹花</a></p></td>
<td><p>陳先生，夜店客人</p></td>
<td><p><a href="../Page/香港無綫電視.md" title="wikilink">香港無綫電視</a></p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/烈火狂奔.md" title="wikilink">烈火狂奔</a></p></td>
<td><p>徐長武</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/縱橫四海_(電視劇).md" title="wikilink">縱橫四海</a></p></td>
<td><p>崔瑞基</p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港</a><a href="../Page/亞洲電視.md" title="wikilink">亞洲電視</a></p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/影城大亨.md" title="wikilink">影城大亨</a></p></td>
<td><p>蔡球</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美麗傳說.md" title="wikilink">美麗傳說</a></p></td>
<td><p>香細</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/縱橫天下.md" title="wikilink">縱橫天下</a></p></td>
<td><p>茅輝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/法網群英.md" title="wikilink">法網群英</a></p></td>
<td><p>徐振聲<br />
（第7集）</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [慈雲山十三太保](../Page/慈雲山十三太保.md "wikilink")

## 參考文獻

## 外部連結

  -
  -
  -
  -
  -
[siu](../Category/李姓.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:慈雲山十三太保](../Category/慈雲山十三太保.md "wikilink")
[Category:香港三合會成員](../Category/香港三合會成員.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港電影人](../Category/香港電影人.md "wikilink")
[Category:香港罪犯](../Category/香港罪犯.md "wikilink")

1.
2.
3.  [【獨家●兩袖清風】61歲古惑佬李兆基中風 靠社團兄弟接濟 ：我冇環境
    冇入息](http://nextplus.nextmedia.com/news/latest/20170208/476502)