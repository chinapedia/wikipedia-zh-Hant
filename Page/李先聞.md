**李先聞**（）\[1\]，字達聰\[2\]，[四川省](../Page/四川省.md "wikilink")[江津縣](../Page/江津縣.md "wikilink")（**今属[重庆市](../Page/重庆市.md "wikilink")[江津区](../Page/江津区.md "wikilink")**）長沖場登桿坡人，祖籍[廣東省](../Page/廣東省.md "wikilink")[梅縣](../Page/梅縣.md "wikilink")。[農學家](../Page/農學.md "wikilink")，1914年進入清華留美預備學校，1923年赴美，1926年畢業於[普渡大學園藝系](../Page/普渡大學.md "wikilink")，1929年獲得[康乃爾大學](../Page/康乃爾大學.md "wikilink")[遺傳學博士學位](../Page/遺傳學.md "wikilink")。回國後，1931年擔任[東北大學教授](../Page/東北大學.md "wikilink")，1932年轉任[河南大學教授](../Page/河南大學.md "wikilink")，1935至1938年任[武漢大學農藝系教授](../Page/武漢大學.md "wikilink")、系主任，1938年任職四川省農業改進所，1946年擔任[中央研究院植物研究所研究員](../Page/中央研究院.md "wikilink")。1948年底到[臺灣](../Page/臺灣.md "wikilink")，任職於[臺灣糖業公司農場](../Page/臺灣糖業公司.md "wikilink")，1954年重新籌建中央研究院植物研究所，1962年至1971年擔任所長，1971年因病退休\[3\]。

他的專長為[植物細胞](../Page/植物細胞.md "wikilink")[遺傳學](../Page/遺傳學.md "wikilink")，在水稻、甘蔗的育種改良方面貢獻良多。1948年4月當選為第一屆[中央研究院院士](../Page/中央研究院院士.md "wikilink")。1976年7月4日逝世於[臺北](../Page/臺北.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

[Category:中央研究院生命科學組院士](../Category/中央研究院生命科學組院士.md "wikilink")
[Category:中央研究院研究員](../Category/中央研究院研究員.md "wikilink")
[Category:台灣農學家](../Category/台灣農學家.md "wikilink")
[Category:臺灣遺傳學家](../Category/臺灣遺傳學家.md "wikilink")
[Category:武漢大學教授](../Category/武漢大學教授.md "wikilink")
[Category:河南大學教授](../Category/河南大學教授.md "wikilink")
[Category:東北大學教授](../Category/東北大學教授.md "wikilink")
[Category:普渡大學校友](../Category/普渡大學校友.md "wikilink")
[Category:康乃爾大學校友](../Category/康乃爾大學校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:台灣戰後廣東移民](../Category/台灣戰後廣東移民.md "wikilink")
[Category:廣東客家人](../Category/廣東客家人.md "wikilink")
[Category:江津人](../Category/江津人.md "wikilink")
[Xian先](../Category/李姓.md "wikilink")
[Category:臺灣糖業公司人物](../Category/臺灣糖業公司人物.md "wikilink")

1.  [中央研究院
    李先聞院士基本資料](https://db1n.sinica.edu.tw/textdb/ioconas/02.php?func=22&_op=?ID:LD020)
2.  《中國科學社社員分股名錄》 民國廿二年一月，中國科學社，第32頁
3.  [生物通 李先聞－中國植物細胞遺傳學的奠基人
    (2008年6月29日查詢)](http://www.ebiotrade.com/newsf/2006-4/20064395154.htm)