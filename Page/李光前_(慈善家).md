[Statue_of_Dr_Lee_Kong_Chian.jpg](https://zh.wikipedia.org/wiki/File:Statue_of_Dr_Lee_Kong_Chian.jpg "fig:Statue_of_Dr_Lee_Kong_Chian.jpg")李光前像\]\]

[丹斯里](../Page/丹斯里.md "wikilink")[拿督](../Page/拿督.md "wikilink")**李光前**（；），是[新加坡与](../Page/新加坡.md "wikilink")[马来西亚一带的](../Page/马来西亚.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")，世界十大富商之一。其[岳父](../Page/岳父.md "wikilink")[陈嘉庚是當地有名的慈善家和](../Page/陈嘉庚.md "wikilink")[华侨](../Page/华侨.md "wikilink")[企业家](../Page/企业家.md "wikilink")。

## 早年生活

李光前1893年于[中国](../Page/中国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[南安市出生](../Page/南安市.md "wikilink")，并在当地接受[小學教育](../Page/小學.md "wikilink")。1903年，他随着其父亲李国专移居新加坡。在1909年返回中国，因为获得了[奖学金并欲在中国完成学业](../Page/奖学金.md "wikilink")，1911年入读唐山路矿学堂。

## 年鉴

1916年，因受新加坡华侨[陈嘉庚赏识](../Page/陈嘉庚.md "wikilink")，加入其谦益公司。
1920年，同陈嘉庚先生女兒陈爱礼結婚。
1928年8月8日，与襟兄林忠国（[林义顺之子](../Page/林义顺.md "wikilink")）开设南益树胶公司，日后因此事業被称为橡胶大王。
1932年，接办陈嘉庚的《[南洋商报](../Page/南洋商报.md "wikilink")》，并出任董事主席。
1934年，接任岳父成为东南亚华文中学[南洋华侨中学董事长至](../Page/华侨中学_\(新加坡\).md "wikilink")1955年。
1937年，出任[华侨银行](../Page/华侨银行.md "wikilink")（OCBC）[主席至](../Page/主席.md "wikilink")1967年病逝。同年与[陈六使合组橡胶公会](../Page/陈六使.md "wikilink")，并出任主席。
1939年，当选为第22届[新加坡中华总商会会长](../Page/新加坡中华总商会.md "wikilink")。
1941年，[二战前夕](../Page/二战.md "wikilink")，其[南益集团再新马已拥有超过](../Page/南益集团.md "wikilink")30,000英亩的[橡胶园与](../Page/橡胶.md "wikilink")[黄梨园](../Page/黄梨.md "wikilink")，在新马泰印共有32间胶厂及36,000名[员工与](../Page/员工.md "wikilink")[职员](../Page/职员.md "wikilink")。同年[太平洋战争爆发](../Page/太平洋战争.md "wikilink")，因在[美国出席](../Page/美国.md "wikilink")[世界树胶大会](../Page/世界树胶大会.md "wikilink")，因此滞留于美国。在美近四年间出任军政人员训练所[讲师](../Page/讲师.md "wikilink")，为他们讲解[东南亚的](../Page/东南亚.md "wikilink")[地理与风土民情](../Page/地理.md "wikilink")。
由于二战间其园地被[日军视为](../Page/日军.md "wikilink")“敌产”而被没收。他于1945年11月回到新加坡时，南益受到严重的破坏。他用了约三年时间恢复原貌。

### 50年代之後

1952年3月29日，创立「[李氏基金](../Page/李氏基金.md "wikilink")」。据其子资料显示，至1993年已为社会捐助约七亿[马币](../Page/马币.md "wikilink")。
1953年陈六使先生建立[南洋大学后](../Page/南洋大学.md "wikilink")，李光前承诺认捐所有捐款总数的10％，1,045,688新元，并很早把捐款付清。
因大力资助[福建会馆建立的](../Page/新加坡福建会馆.md "wikilink")[小学](../Page/小学.md "wikilink")，福建会馆把学校取名为“光前学校”，但后来李光前知道后坚决不肯，派人把学校招牌的“前”字摘下，后会馆把学校易名为“光华学校”。
1954年宣布退休，长子[李成义继承](../Page/李成义.md "wikilink")[南益集团总裁](../Page/南益集团.md "wikilink")，次子[李成智](../Page/李成智.md "wikilink")、三子[李成伟分别继承黄梨与银行业](../Page/李成伟.md "wikilink")。据估计，南益当时资金已高达3亿新币。
1955年，与岳父合资捐助的中国[厦门大学落成](../Page/厦门大学.md "wikilink")。
1957年，受[柔佛州](../Page/柔佛州.md "wikilink")[苏丹封赐](../Page/苏丹.md "wikilink")**拿督**勋衔。
1960年，成立马来西亚李氏基金会。
1962年受新加坡元首邀请，出任[新加坡大学的首任](../Page/新加坡大学.md "wikilink")[校长](../Page/校长.md "wikilink")。并于6月12日举行典礼，正式就职。
1964年受马来西亚最高[元首封赐](../Page/元首.md "wikilink")**丹斯里**（PMN）高级勋衔。是少有获得该勋衔的[新加坡人](../Page/新加坡人.md "wikilink")。同年，将其下所有南益集团的[股权](../Page/股权.md "wikilink")（48%），[捐献给李氏基金](../Page/捐献.md "wikilink")。
1965年，设立[香港李氏基金会](../Page/香港李氏基金会.md "wikilink")。同年被诊断为[肝癌](../Page/肝癌.md "wikilink")。之後回[中国治病](../Page/中国.md "wikilink")。治病期间，受[中国总理](../Page/中国.md "wikilink")[周恩来的接见](../Page/周恩来.md "wikilink")。
1967年，南益集团已有26间[子公司](../Page/子公司.md "wikilink")，包括[南益橡胶](../Page/南益橡胶.md "wikilink")、[华侨银行与](../Page/华侨银行.md "wikilink")[大东方人寿保险](../Page/大东方人寿保险.md "wikilink")。
1967年6月2日，丹斯里拿督李光前逝世，享年74岁。在[遗嘱中阐明丧事从简](../Page/遗嘱.md "wikilink")。[出殡当天](../Page/出殡.md "wikilink")，有来自各机构共5,000人送行，队伍长达二公里。长子[李成义接任南益集团总裁兼李氏基金会主席](../Page/李成义.md "wikilink")。

## 外部参考

  -
  -
  -
[L](../Category/國立暨南大學校友.md "wikilink")
[Category:新加坡企業家](../Category/新加坡企業家.md "wikilink")
[Category:新加坡慈善家](../Category/新加坡慈善家.md "wikilink")
[Category:新加坡億萬富豪](../Category/新加坡億萬富豪.md "wikilink")
[Category:閩南裔新加坡人](../Category/閩南裔新加坡人.md "wikilink")
[Category:南安人](../Category/南安人.md "wikilink")
[Category:罹患肝癌逝世者](../Category/罹患肝癌逝世者.md "wikilink")
[G光](../Category/李姓.md "wikilink")