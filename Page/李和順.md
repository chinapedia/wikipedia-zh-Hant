**李和順**（）[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[政治人物](../Page/政治人物.md "wikilink")，曾任[臺南縣議長](../Page/臺南縣.md "wikilink")、[國民黨第五屆不分區立委](../Page/國民黨.md "wikilink")（辭職）及[無黨團結聯盟第六屆](../Page/無黨團結聯盟.md "wikilink")[台南縣選區](../Page/台南縣.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

2008年[立委選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")，單一選區兩票制實施，李和順終於[臺南縣第二選舉區以](../Page/臺南市第二選舉區_\(2008年立法委員\).md "wikilink")29,460票差距敗給尋求連任的[黃偉哲](../Page/黃偉哲.md "wikilink")，連任失敗後，轉往商界發展。

## 選舉

### 2004年立法委員選舉

| 2004年臺南縣選舉區立法委員選舉結果 |
| ------------------- |
| 應選8席                |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |
| 5                   |
| 6                   |
| 7                   |
| 8                   |
| 9                   |
| 10                  |
| 11                  |
| 12                  |
| 13                  |
| 14                  |
| 15                  |
| 16                  |

### 2008年立法委員選舉

| 2008年[臺南縣第二選舉區](../Page/臺南市第二選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

## 參考資料

## 相關條目

  - [2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表](../Page/2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表.md "wikilink")

## 外部連結

  - [立法院全球資訊網－立法委員－李和順委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00028&stage=5)
  - [立法院全球資訊網－立法委員－李和順委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00039&stage=6)
  - [線上大國民 Mighty People Online - 立法委員資料 –
    李和順](https://web.archive.org/web/20071224151730/http://www.mightypeople.org/congress/congress.php?id=479)

[L李](../Category/第5屆中華民國立法委員.md "wikilink")
[L李](../Category/第6屆中華民國立法委員.md "wikilink")
[L李](../Category/中國國民黨黨員.md "wikilink")
[L李](../Category/無黨團結聯盟黨員.md "wikilink")
[Category:遠東科技大學校友](../Category/遠東科技大學校友.md "wikilink")
[H和](../Category/李姓.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")