[李四光.png](https://zh.wikipedia.org/wiki/File:李四光.png "fig:李四光.png")

**李四光**（），字**仲揆**，[湖北省](../Page/湖北省.md "wikilink")[黄冈縣人](../Page/黄冈縣.md "wikilink")，[蒙古族](../Page/蒙古族.md "wikilink")\[1\]。祖父库里是蒙古族人，通汉文，以在乡间设私塾为生，与汉族妇女结婚，后代取李姓。\[2\]。[中国著名](../Page/中国.md "wikilink")[地质学家和古生物学家](../Page/地质学.md "wikilink")，大清[工科進士](../Page/模板:宣統三年游學畢業進士.md "wikilink")\[3\]，中华民國的[中央研究院院士](../Page/中央研究院院士.md "wikilink")，中华人民共和国的[中国科学院院士](../Page/中国科学院院士.md "wikilink")。

李四光是中国古生物学、地质学、第四纪地质学研究的开拓者\[4\]，创立大地构造理论[地质力学](../Page/地质力学.md "wikilink")，对亚欧大陆东部山脉体系的形成原因提出了自己独特的观点，是现代板块构造理论出现之前的大地构造理论之一。然而其对不同学术观点的压制，特别是在1949年以后利用权力对其地质力学理论和第四纪冰川理论进行的推广和对其他观点的压制，也被认为是政治对科学发展和传播产生负面影响的典型例子\[5\]。

## 生平

李四光原名**李仲揆**，十三岁（1902年冬）赴武昌求学小学报名时误将[年龄十四填入姓名一栏](../Page/虚岁.md "wikilink")，增添几笔遂改成李四光\[6\]，取“光被四表”之意，而以仲揆為字。光绪三十年五月1904年赴[日本留学](../Page/日本.md "wikilink")，同年参加[同盟会](../Page/同盟会.md "wikilink")，先后在[东京弘文学院](../Page/东京弘文学院.md "wikilink")、[大阪高等工业学校学习](../Page/大阪高等工业学校.md "wikilink")，选学“舶用机关”。1911年毕业回中国。1911年9月到京师参加“海归”考试，以最优等成绩获赏“[工科進士](../Page/模板:宣統三年游學畢業進士.md "wikilink")”。1913年赴[英国](../Page/英国.md "wikilink")[伯明翰大学先学采矿](../Page/伯明翰大学.md "wikilink")，后转学[地质学](../Page/地质学.md "wikilink")，1918年5月，他以《中国之地质》的优秀论文获得了自然科学硕士学位，接着到欧洲大陆做地质考察\[7\]。1919年获硕士学位。同年回中国，任[北京大学地质系教授兼系主任](../Page/北京大学.md "wikilink")。后再赴英国深造，1927年获伯明翰大学[博士学位](../Page/博士.md "wikilink")，同年回中国。1928年7月至1938年4月任[国立武汉大学建筑筹备委员长](../Page/国立武汉大学.md "wikilink")，后在[南京参与创建](../Page/南京.md "wikilink")[中央研究院](../Page/中央研究院.md "wikilink")，并任地质研究所研究员兼所长，直至1948年。同年，獲任為中華民國中央研究院第一屆(數理科學組)院士\[8\]。1932年曾出任[国立中央大学代理校长](../Page/国立中央大学_\(南京\).md "wikilink")。1949年后，历任[中华人民共和国的全国地质工作计划指导委员会主任](../Page/中华人民共和国.md "wikilink")、[中国科学院副院长](../Page/中国科学院.md "wikilink")、[地质部部长](../Page/中华人民共和国地质部.md "wikilink")。1952年筹建[北京地质学院](../Page/中国地质大学.md "wikilink")。1954年当选为[中国人民政治协商会议全国委员会副主席](../Page/中国人民政治协商会议全国委员会副主席.md "wikilink")。1955年当选中国科学院院士。1958年当选为[中国科学技术协会首任主席](../Page/中国科学技术协会.md "wikilink")。1971年4月29日在[北京逝世](../Page/北京.md "wikilink")，享年82岁。

  - 民國20年（1931年）3月30日，特派西陲學術考察團理事會理事\[9\]。
  - 民國21年（1932年）9月1日，派管理中英庚款董事會董事\[10\]。
  - 民國22年（1933年）4月20日，派管理中英庚款董事會董事\[11\]。
  - 民國25年（1936年）3月28日，派管理中英庚款董事會董事\[12\]。
  - 民國28年（1939年）5月30日，派管理中英庚款董事會董事\[13\]。
  - 民國31年（1942年）5月19日，連任管理中英庚款董事會董事\[14\]。

## 学术贡献

[李四光地球仪等.jpg](https://zh.wikipedia.org/wiki/File:李四光地球仪等.jpg "fig:李四光地球仪等.jpg")

  - 对中国北部[䗴类](../Page/䗴.md "wikilink")[化石及其地层意义的研究是中国最早进行的䗴类及石炭二叠系研究](../Page/化石.md "wikilink")。其创立的“䗴”字，用来翻译Fusulinid（一种早已灭绝的有孔虫，属于原生动物），为现在的中国古生物学界所沿用\[15\]。
  - 提出了[华东](../Page/华东.md "wikilink")[第四纪](../Page/第四纪.md "wikilink")[冰川存在的理论](../Page/冰川.md "wikilink")。
  - 运用[地质力学对地壳运动及其与矿产分布的规律的研究](../Page/地质力学.md "wikilink")，创建“地质力学”和“[构造体系](../Page/构造体系.md "wikilink")”的概念。
  - 预测[新华夏构造体系三个](../Page/新华夏构造体系.md "wikilink")[沉降带存有](../Page/沉降带.md "wikilink")[石油](../Page/石油.md "wikilink")，后[大庆](../Page/大庆油田.md "wikilink")、[胜利等油田的发现证实其预测](../Page/胜利油田.md "wikilink")。但是对于这一点，在学术界一直有争论，如黄汲清认为是他的大地构造理论指导了大庆油田的发现。\[16\]

## 家庭

[Li_Siguang_family.jpg](https://zh.wikipedia.org/wiki/File:Li_Siguang_family.jpg "fig:Li_Siguang_family.jpg")与女婿[邹承鲁的婚礼](../Page/邹承鲁.md "wikilink")\]\]

  - 父亲：李卓侯，是[私塾老师](../Page/私塾.md "wikilink")，[秀才](../Page/秀才.md "wikilink")，早年参加过[中国同盟会](../Page/中国同盟会.md "wikilink")，曾与[孙中山](../Page/孙中山.md "wikilink")、[黄兴等人多次聚首](../Page/黄兴.md "wikilink")，思想开放、知识渊博，是“林氏三兄弟”——[林育南](../Page/林育南.md "wikilink")、[林育英](../Page/林育英.md "wikilink")、[林彪的启蒙恩师](../Page/林彪.md "wikilink")\[17\]。

## 参考文献

## 外部链接

  - [新华网简介](http://news.xinhuanet.com/ziliao/2003-01/17/content_694572.htm)

{{-}}

[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[L李](../Category/中华人民共和国地质学家.md "wikilink")
[Category:中华人民共和国古生物学家](../Category/中华人民共和国古生物学家.md "wikilink")
[Category:中華民國大陸時期古生物學家](../Category/中華民國大陸時期古生物學家.md "wikilink")
[Category:中国科学院生物学地学部学部委员](../Category/中国科学院生物学地学部学部委员.md "wikilink")
[Category:中华人民共和国党和国家领导人
(中国科学院院士)](../Category/中华人民共和国党和国家领导人_\(中国科学院院士\).md "wikilink")
[Category:中华人民共和国蒙古族党和国家领导人](../Category/中华人民共和国蒙古族党和国家领导人.md "wikilink")
[Category:中华人民共和国地质部部长](../Category/中华人民共和国地质部部长.md "wikilink")
[Category:中国共产党第九届中央委员会委员](../Category/中国共产党第九届中央委员会委员.md "wikilink")
[Category:第四届国民参政会参政员](../Category/第四届国民参政会参政员.md "wikilink")
[Category:中華民國大陸時期部聘教授](../Category/中華民國大陸時期部聘教授.md "wikilink")
[Category:國立武漢大學校長](../Category/國立武漢大學校長.md "wikilink")
[Category:中央大学校长](../Category/中央大学校长.md "wikilink")
[Category:南京大学校长](../Category/南京大学校长.md "wikilink")
[Category:中国地质大学校长](../Category/中国地质大学校长.md "wikilink")
[L李](../Category/武汉大学校友.md "wikilink")
[L李](../Category/大阪大學校友.md "wikilink")
[L李](../Category/伯明翰大學校友.md "wikilink")
[Category:中国蒙古族科学家](../Category/中国蒙古族科学家.md "wikilink")
[L李](../Category/团风人.md "wikilink") [S四](../Category/李姓.md "wikilink")
[Category:中国科学技术协会主席](../Category/中国科学技术协会主席.md "wikilink")
[Category:苏联科学院外籍院士](../Category/苏联科学院外籍院士.md "wikilink")

1.  <http://www.cnki.com.cn/Article/CJFDTotal-MZTJ201107028.htm>
    李四光(蒙古族)
2.  樊洪业：《李四光与丁文江的恩恩怨怨》，南方周末，2012年1月30日，B5副刊，http://www.infzm.com/content/97874
3.  《宣統政紀》卷62，宣統三年九月初六日庚午，李四光等賞給工科進士
4.  C. Croneis, The Journal of Geology 37 (2), 187 (1929).
5.  张林源，中国东部第四季冰川争论问题及其哲学意义，见：王子贤主编，《地学与哲学》
6.  [李四光](http://cpc.people.com.cn/GB/34136/2568890.html)
7.  樊洪业：《李四光与丁文江的恩恩怨怨》，南方周末，2012年1月30日，B5副刊，http://www.infzm.com/content/97874
8.  [李四光
    逝世院士一覽表](https://academicians.sinica.edu.tw/index.php?func=1-D)中央研究院
9.  《國民政府公報》，第734號
10. 《國民政府公報》，第19號
11. 《國民政府公報》，第1111號
12. 《國民政府公報》，第2008號
13. 《國民政府公報》，第157號
14. 《國民政府公報》，第467號
15. C. Croneis, The Journal of Geology 37 (2), 187 (1929).
16. [大庆油田发现真相](http://njrb.njnews.cn/html/2010-05/20/content_520757.htm)
    ，南京日报2010年5月20日。
17. 《林彪的这一生》少华、游胡，湖北人民出版社，2003年4月，ISBN 7-216-01446-4。第2节“林彪长到十三岁”。