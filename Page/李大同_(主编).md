[Voa_chinese_Li_Datong_28jun07_300.jpg](https://zh.wikipedia.org/wiki/File:Voa_chinese_Li_Datong_28jun07_300.jpg "fig:Voa_chinese_Li_Datong_28jun07_300.jpg")
[BingDianStory.jpg](https://zh.wikipedia.org/wiki/File:BingDianStory.jpg "fig:BingDianStory.jpg")
**李大同**（）生於[四川省](../Page/四川省.md "wikilink")[南充市](../Page/南充市.md "wikilink")，前《[中國青年報](../Page/中國青年報.md "wikilink")》記者，《[冰點](../Page/冰点_\(中国青年报\).md "wikilink")》周刊創刊編輯。

1954年與雙親遷至[北京生活](../Page/北京.md "wikilink")，1968年被下放至[内蒙古生产建设兵团](../Page/内蒙古生产建设兵团.md "wikilink")；1979年7月起任《中國青年報》駐[內蒙古記者](../Page/內蒙古.md "wikilink")，歷任報社內學校教育部、科學部主任、高級编辑。

1989年[六四天安門事件發生前夕曾聯同](../Page/六四天安門事件.md "wikilink")1013名記者在報社與[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")[胡啟立對話](../Page/胡啟立.md "wikilink")（1989年5月11日），後被撤消職務並調至報社內新聞研究所賦閒至1994年。1995年在報內創立《冰點》時評欄目，翌年起成為報內之品牌欄目。

《冰點》周刊在2005年末因刊出[袁偉時文章](../Page/袁偉時.md "wikilink")《現代化與歷史教科書》為導火線，導致《冰點》一度被[中共中央宣傳部停刊](../Page/中共中央宣傳部.md "wikilink")，並導致他與另一副主編[盧躍剛被調至報社內新聞研究所](../Page/盧躍剛.md "wikilink")。轉職後撰寫《冰點》辦刊[回憶錄](../Page/回憶錄.md "wikilink")，並前往[日本作該方面專題演講](../Page/日本.md "wikilink")，亦定期為[香港](../Page/香港.md "wikilink")《[明報](../Page/明報.md "wikilink")》副刊撰稿。2007年8月28日與來華訪問的[德国总理](../Page/德国总理.md "wikilink")[安格拉·默克爾見面](../Page/安格拉·默克爾.md "wikilink")。

李大同已婚，其妻[江菲亦為](../Page/江菲.md "wikilink")《中國青年報》資深記者，育有一子。

## 著作

  - 《冰點'98——尋回心靈深處的感動》[中國林業出版社](../Page/中國林業出版社.md "wikilink")1998年初版，ISBN
    7-5038-2069-1
  - 《中国青年报：冰点'03》[西苑出版社](../Page/西苑出版社.md "wikilink")2004年2月初版，ISBN
    7801088859
  - 《冰點故事》[廣西師範大學出版社](../Page/廣西師範大學出版社.md "wikilink")2005年11月初版，ISBN
    7-5633-5699-1 /G.3339，
    [日語版](../Page/日語.md "wikilink")《》[武吉次朗監譯](../Page/武吉次朗.md "wikilink")、[久保井真愛翻譯](../Page/久保井真愛.md "wikilink")，[日本僑報社](../Page/日本僑報社.md "wikilink")2006年11月初版，ISBN
    4-86185-040-1
  - 《用新聞影響今天——〈冰點〉週刊紀事》香港[泰德時代出版社](../Page/泰德時代出版社.md "wikilink")2006年7月初版，ISBN
    988-98616-0-7
  - 《》（中譯：[《〈冰點〉停刊的舞台後面》](http://duan.jp/item/037.html)）[三潴正道](../Page/三潴正道.md "wikilink")
    監譯、[而立會](../Page/而立會.md "wikilink") 譯，日本僑報社2006年6月初版，ISBN
    4-86185-037-1

## 相關條目

  - [冰点 (中国青年报)](../Page/冰点_\(中国青年报\).md "wikilink")

## 外部連結

[L](../Category/中国报纸编辑.md "wikilink")
[L](../Category/中国记者.md "wikilink")
[Da](../Category/李姓.md "wikilink")