**李娜**（），出生于[河南省](../Page/河南省.md "wikilink")[郑州市](../Page/郑州市.md "wikilink")，原名牛志红，出家后法名释昌圣，是一位[女歌手](../Page/歌手.md "wikilink")。毕业于[河南省戏曲学校](../Page/河南省戏曲学校.md "wikilink")，毕业后曾从事于[豫剧演出](../Page/豫剧.md "wikilink")，1986年进入河南省歌舞团，进军歌坛。\[1\]

## 主要作品

  - 1989年 电视剧 《[篱笆、女人和狗](../Page/篱笆、女人和狗.md "wikilink")》主题曲《苦篱笆》\[2\]
  - 1990年 电视剧 《[渴望](../Page/渴望_\(电视剧\).md "wikilink")》主题曲《好人一生平安》
  - 1991年 电视剧 《[赵尚志](../Page/赵尚志.md "wikilink")》主题曲《嫂子颂》
  - 1993年 电视剧 《男人是太阳》主题曲《女人是老虎》
  - 1994年 电视剧
    《[天路](../Page/天路.md "wikilink")》主题曲《[青藏高原](../Page/青藏高原_\(歌曲\).md "wikilink")》
  - 1997年 录音室专辑 《李娜（牛志红）影视歌曲精选》
  - 1997年 专辑 《[苏武牧羊](../Page/苏武.md "wikilink")》 上海声像出版社出版发行
  - 2002年 录音室专辑 《李娜（牛志红）·念佛仪规》

[Tian_Menshan_Mountain_11.jpg](https://zh.wikipedia.org/wiki/File:Tian_Menshan_Mountain_11.jpg "fig:Tian_Menshan_Mountain_11.jpg")

## 释昌圣

李娜（牛志红）于1997年5月23日游览[张家界市](../Page/张家界市.md "wikilink")[天门山](../Page/天门山.md "wikilink")，6月6日将[户口迁移到张家界永定区](../Page/户口.md "wikilink")，一月之后在山西[五台山](../Page/五台山.md "wikilink")[普寿寺](../Page/普寿寺.md "wikilink")[出家为](../Page/出家.md "wikilink")[尼](../Page/尼姑.md "wikilink")，[法名释昌圣](../Page/法名.md "wikilink")。1998年去美国。

## 参考文献

[Category:中國女歌手](../Category/中國女歌手.md "wikilink")
[Category:郑州人](../Category/郑州人.md "wikilink")
[Category:中原文化艺术学院校友](../Category/中原文化艺术学院校友.md "wikilink")
[Category:豫剧演员](../Category/豫剧演员.md "wikilink")
[Category:河南歌手](../Category/河南歌手.md "wikilink")
[NA](../Category/李姓.md "wikilink")
[Category:中国佛教徒](../Category/中国佛教徒.md "wikilink")

1.
2.