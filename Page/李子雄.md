**李子雄**（英文名：**Waise
Lee**，），生於[香港](../Page/香港.md "wikilink")，資深影視演員，為前[無綫電視部頭合約男藝員](../Page/無綫電視.md "wikilink")，妻子為中國大陸藝人[王雅琦](../Page/王雅琦.md "wikilink")。

## 個人簡歷

李子雄在1977年畢業於[中華基督教會基新中學](../Page/中華基督教會基新中學.md "wikilink")（中五）。後來出道於第11期[無綫電視藝員訓練班](../Page/無綫電視藝員訓練班.md "wikilink")（1982年），同期畢業的藝員包括有[梁朝偉](../Page/梁朝偉.md "wikilink")、[吳鎮宇](../Page/吳鎮宇.md "wikilink")、[張兆輝等等](../Page/張兆輝.md "wikilink")。1985年獲邀到[徐克的電影工作室試鏡](../Page/徐克.md "wikilink")，拍攝廣告而給發掘，成為旗下演員。1986年正式開始其電影事業，憑首部電影《英雄本色》於1987年榮獲[香港電影金像獎](../Page/香港電影金像獎.md "wikilink")「最佳新人獎」、「最佳男配角獎」提名，並提名[台灣金馬獎](../Page/台灣金馬獎.md "wikilink")「最佳男配角」；在戲裡多擔當反派或配角，其中他在《[英雄本色](../Page/英雄本色.md "wikilink")》所飾演的反派人物「大哥成」（譚成）較出名。此後數年間他常都出現在電影中，包括《[倩女幽魂II人間道](../Page/倩女幽魂II人間道.md "wikilink")》、《[跛豪](../Page/跛豪.md "wikilink")》、《四大探長》等等，而在《[城市特警](../Page/城市特警.md "wikilink")》、《[喋血街頭](../Page/喋血街頭.md "wikilink")》、《[老貓](../Page/老貓_\(電影\).md "wikilink")》則更擔任主角。1992年，李參演三級片《性奴》，唯票房不佳。

至於他在電視劇較為人熟悉的是1993年《[開心華之里](../Page/開心華之里.md "wikilink")》裡的[女婿角色](../Page/女婿.md "wikilink")，其後于
[古裝劇](../Page/古裝劇.md "wikilink")《[神雕俠侶](../Page/神雕俠侶_\(1995年電視劇\).md "wikilink")》中飾演何沅君的丈夫陸展元、時裝劇《[天地男兒](../Page/天地男兒.md "wikilink")》亦忠亦奸的葉永昌以及《[烈火雄心](../Page/烈火雄心.md "wikilink")》由忠變奸的消防隊長駱曉峰。

2001年初期，他開始逐漸減少在無線的劇集演出，如他曾在[亞視演出了](../Page/亞視.md "wikilink")《[縱橫天下](../Page/縱橫天下.md "wikilink")》。與此同時，他亦有主力在[中國大陸拍劇](../Page/中國大陸.md "wikilink")。

2013年宣佈以自由身份重新返回[無綫電視](../Page/無綫電視.md "wikilink")，首部回巢無綫的重頭劇是《[忠奸人](../Page/忠奸人_\(電視劇\).md "wikilink")》；並在劇中飾演地產界富商杜以鏗，該劇於2014年7月14日隆重首播，2014年8月22日大結局播畢。

## 影視作品

### 電視劇

※<span style="font-size:smaller;">**粗體字為擔任重要配角角色**</span>

#### 電視劇（[無綫電視](../Page/電視廣播有限公司.md "wikilink")）

  - 1990年：[水鄉危情](../Page/水鄉危情.md "wikilink")
  - 1990年：[又是冤家又聚頭](../Page/又是冤家又聚頭.md "wikilink")
  - 1993年：**[開心華之里](../Page/開心華之里.md "wikilink")** 飾 **楊左繼**
  - 1995年：**[神雕俠侶](../Page/神鵰俠侶_\(1995年電視劇\).md "wikilink")** 飾 **陸展元**
  - 1995年：[廉政英雌之火槍柔情](../Page/廉政英雌之火槍柔情.md "wikilink") 飾 卓明
  - 1996年：**[天地男兒](../Page/天地男兒.md "wikilink")** 飾 **葉永昌**
  - 1998年：**[烈火雄心](../Page/烈火雄心.md "wikilink")** 飾 **駱曉峰**
  - 1999年：[狀王宋世傑II](../Page/狀王宋世傑II.md "wikilink") 飾
    [恭親王](../Page/恭親王.md "wikilink")
  - 2000年：**[大囍之家](../Page/大囍之家.md "wikilink")** 飾 **連天樂**
  - 2000年：**[男親女愛](../Page/男親女愛.md "wikilink") 飾 Herman (單元男主角)**
  - 2001年：**[尋秦記](../Page/尋秦記_\(電視劇\).md "wikilink")** 飾 **趙穆**
  - 2004年：[大唐雙龍傳](../Page/大唐雙龍傳_\(電視劇\).md "wikilink") 飾
    [宇文化及](../Page/宇文化及.md "wikilink")
  - 2004年：[天涯俠醫](../Page/天涯俠醫.md "wikilink") 飾 齊嘉尚
  - 2005年：[學警雄心](../Page/學警雄心.md "wikilink") 飾 鍾志華
  - 2007年：**[律政新人王II](../Page/律政新人王II.md "wikilink")** 飾 **崔正平**
  - 2014年：**[忠奸人](../Page/忠奸人_\(電視劇\).md "wikilink")** 飾 **杜以鏗**
  - 2018年：**[波士早晨](../Page/波士早晨.md "wikilink")** 飾 **宋禮勤**

#### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1990年：[特警90 III之明日天涯](../Page/特警90_III之明日天涯.md "wikilink")
  - 1990年：[天涯路客](../Page/天涯路客.md "wikilink")

#### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 2001年：[縱橫天下](../Page/縱橫天下_\(電視劇\).md "wikilink") 飾 雷洛天

#### 電視劇（中國大陸）

  - 2003年：[亞洲英雄](../Page/亞洲英雄.md "wikilink") 飾 田國棟
  - 2007年：[八月一日](../Page/八月一日.md "wikilink") 飾 汪精衛
  - 2008年：[鳳穿牡丹](../Page/鳳穿牡丹.md "wikilink")
  - 2009年：[杭州王爺](../Page/杭州王爺.md "wikilink") 飾 乾隆
  - 2010年：[秋霜](../Page/秋霜.md "wikilink")
  - 2010年：[特戰先鋒](../Page/特戰先鋒.md "wikilink")
  - 2011年：[水滸傳](../Page/水滸傳_\(2011年電視劇\).md "wikilink") 飾
    [高俅](../Page/高俅.md "wikilink")
  - 2012年：鐵甲艦上的男人飾方伯謙
  - 2012年：[溫州一家人](../Page/溫州一家人.md "wikilink") 飾 胡文躍
  - 2012年：[冷風暴](../Page/冷風暴.md "wikilink")
  - 2012年：[廚窗外的天空](../Page/廚窗外的天空.md "wikilink") (2009年拍攝)
  - 2013年：[追魚傳奇](../Page/追魚傳奇.md "wikilink") 飾 宰相金寵
  - 2013年：[姥爺的抗戰](../Page/姥爺的抗戰.md "wikilink")
  - 2014年：[羅龍鎮女人](../Page/羅龍鎮女人.md "wikilink")
  - 2015年：[抓住彩虹的男人](../Page/抓住彩虹的男人.md "wikilink")
  - 2015年：[半為蒼生半美人](../Page/半為蒼生半美人.md "wikilink") 飾 縣令
  - 2016年：[最美是你](../Page/最美是你.md "wikilink") 飾 陳以誠
  - 2016年：[東方戰場](../Page/東方戰場.md "wikilink") 飾
    [施肇基](../Page/施肇基.md "wikilink")
  - 2016年：[杜心五傳奇](../Page/杜心五傳奇.md "wikilink") 飾 加藤進一
  - 2016年：[西涯俠](../Page/西涯俠.md "wikilink")(網絡劇) 飾 秦朔
  - 2017年：[滄海一粟](../Page/滄海一粟.md "wikilink") (2008年拍攝)飾 蔣懷忠
  - 2017年：[紅門兄弟](../Page/紅門兄弟.md "wikilink") (2013年拍攝)飾 秦正國
  - 2017年：[新俠客行](../Page/新俠客行.md "wikilink") 飾
    [貝海石](../Page/貝海石.md "wikilink")
  - 2017年：[兒科醫生](../Page/儿科医生_\(电视剧\).md "wikilink") 飾 申父(客串)
  - 2017年：[國士無雙黃飛鴻](../Page/國士無雙黃飛鴻.md "wikilink") 飾 莫文卿(客串)
  - 2018年：[將夜](../Page/将夜_\(网络剧\).md "wikilink")(網絡劇) 飾 燕王
  - 待播中：[天下鹽商](../Page/天下鹽商.md "wikilink")
  - 待播中：[驚天岳雷](../Page/驚天岳雷.md "wikilink") 飾
    [金兀術](../Page/金兀術.md "wikilink")
  - 待播中：[馮子材](../Page/馮子材_\(電視劇\).md "wikilink")
  - 待播中：逆光

#### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>片名</strong></p></td>
<td><p><strong>年代</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>合作</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英雄本色.md" title="wikilink">英雄本色</a></p></td>
<td><p>1986</p></td>
<td><p>譚成</p></td>
<td><p><a href="../Page/周潤發.md" title="wikilink">周潤發</a>、<a href="../Page/狄龍.md" title="wikilink">狄龍</a>、<a href="../Page/張國榮.md" title="wikilink">張國榮</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大丈夫日記.md" title="wikilink">大丈夫日記</a></p></td>
<td><p>1988</p></td>
<td><p>志雄</p></td>
<td><p>周潤發、<a href="../Page/王祖賢.md" title="wikilink">王祖賢</a>、<a href="../Page/葉蒨文.md" title="wikilink">葉蒨文</a>、<a href="../Page/吳家麗.md" title="wikilink">吳家麗</a>、<a href="../Page/鄭則士.md" title="wikilink">鄭則士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/城市特警.md" title="wikilink">城市特警</a></p></td>
<td><p>黃維邦</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天羅地網_(1988年電影).md" title="wikilink">天羅地網</a></p></td>
<td></td>
<td><p><a href="../Page/梁家輝.md" title="wikilink">梁家輝</a>、<a href="../Page/鄭浩南.md" title="wikilink">鄭浩南</a>、<a href="../Page/胡大為.md" title="wikilink">胡大為</a>、<a href="../Page/鄭少秋.md" title="wikilink">鄭少秋</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西環的故事.md" title="wikilink">西環的故事</a></p></td>
<td><p>1990</p></td>
<td><p>莊鵬</p></td>
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a>、<a href="../Page/鄭浩南.md" title="wikilink">鄭浩南</a>、<a href="../Page/張敏.md" title="wikilink">張敏</a>、<a href="../Page/羅美薇.md" title="wikilink">羅美薇</a>、<a href="../Page/午馬.md" title="wikilink">午馬</a>、<a href="../Page/董驃.md" title="wikilink">董驃</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/倩女幽魂II人間道.md" title="wikilink">倩女幽魂II人間道</a></p></td>
<td><p>左千戶</p></td>
<td><p><a href="../Page/張國榮.md" title="wikilink">張國榮</a>、<a href="../Page/張學友.md" title="wikilink">張學友</a>、王祖賢</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/喋血街頭.md" title="wikilink">喋血街頭</a></p></td>
<td><p>細榮</p></td>
<td><p><a href="../Page/梁朝偉.md" title="wikilink">梁朝偉</a>、張學友</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/客途秋恨_(1990年電影).md" title="wikilink">客途秋恨</a></p></td>
<td><p>葵子之夫</p></td>
<td><p><a href="../Page/陸小芬.md" title="wikilink">陸小芬</a>、<a href="../Page/田豐_(演員).md" title="wikilink">田豐</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙灘仔與周師奶.md" title="wikilink">沙灘仔與周師奶</a></p></td>
<td><p>1991</p></td>
<td><p>李南</p></td>
<td><p>梁朝偉、<a href="../Page/吳倩蓮.md" title="wikilink">吳倩蓮</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a></p></td>
<td><p>台：專吊老千</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/表姐，你好嘢！2.md" title="wikilink">表姐，你好嘢！2</a></p></td>
<td><p>阿雄</p></td>
<td><p><a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/張堅庭.md" title="wikilink">張堅庭</a>、<a href="../Page/林蛟.md" title="wikilink">林蛟</a>、<a href="../Page/周文健.md" title="wikilink">周文健</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/跛豪.md" title="wikilink">跛豪</a></p></td>
<td><p>陳大文</p></td>
<td><p><a href="../Page/呂良偉.md" title="wikilink">呂良偉</a>、<a href="../Page/葉童.md" title="wikilink">葉童</a>、<a href="../Page/吳啟華.md" title="wikilink">吳啟華</a>、<a href="../Page/鄭則士.md" title="wikilink">鄭則士</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/四大探長_(電影).md" title="wikilink">四大探長</a></p></td>
<td><p>姚雄</p></td>
<td><p><a href="../Page/李修賢.md" title="wikilink">李修賢</a>、<a href="../Page/任達華.md" title="wikilink">任達華</a>、鄭則士</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/整蠱專家.md" title="wikilink">整蠱專家</a></p></td>
<td><p>金默基</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/周星馳.md" title="wikilink">周星馳</a>、<a href="../Page/關之琳.md" title="wikilink">關之琳</a>、<a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a>、<a href="../Page/鮑漢琳.md" title="wikilink">鮑漢琳</a></p></td>
<td><p>台：整人專家<br />
角色：麥克基</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情義心.md" title="wikilink">情義心</a></p></td>
<td><p>李子豪</p></td>
<td><p><a href="../Page/王敏德.md" title="wikilink">王敏德</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越青.md" title="wikilink">越青</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/老貓.md" title="wikilink">老貓</a></p></td>
<td><p>1992</p></td>
<td><p>衛斯理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/笑傲江湖II東方不敗.md" title="wikilink">笑傲江湖II東方不敗</a></p></td>
<td><p>服部千軍</p></td>
<td><p><a href="../Page/林青霞.md" title="wikilink">林青霞</a>、<a href="../Page/錢嘉樂.md" title="wikilink">錢嘉樂</a>、<a href="../Page/任世官.md" title="wikilink">任世官</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/性奴_(電影).md" title="wikilink">性奴</a></p></td>
<td><p>李sir</p></td>
<td><p><a href="../Page/大島由加利.md" title="wikilink">大島由加利</a>、<a href="../Page/刑小路.md" title="wikilink">刑小路</a>、<a href="../Page/趙敏.md" title="wikilink">趙敏</a>、<a href="../Page/徐寶麟.md" title="wikilink">徐寶麟</a></p></td>
<td><p>三級片，全裸演出</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/血在風上.md" title="wikilink">血在風上</a></p></td>
<td><p>1993</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李洛夫奇案.md" title="wikilink">李洛夫奇案</a></p></td>
<td><p>李財法</p></td>
<td><p><a href="../Page/呂良偉.md" title="wikilink">呂良偉</a>、<a href="../Page/吳家麗.md" title="wikilink">吳家麗</a>、<a href="../Page/劉兆銘.md" title="wikilink">劉兆銘</a> 、<a href="../Page/袁詠儀.md" title="wikilink">袁詠儀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廉政第一擊.md" title="wikilink">廉政第一擊</a></p></td>
<td><p>陸大潮</p></td>
<td><p><a href="../Page/狄龍.md" title="wikilink">狄龍</a>、<a href="../Page/任達華.md" title="wikilink">任達華</a>、<a href="../Page/張曼玉.md" title="wikilink">張曼玉</a>、<a href="../Page/許志安.md" title="wikilink">許志安</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/咏春_(电影).md" title="wikilink">咏春</a></p></td>
<td><p>1994</p></td>
<td><p>黄学州</p></td>
<td><p><a href="../Page/杨紫琼.md" title="wikilink">杨紫琼</a>、<a href="../Page/甄子丹.md" title="wikilink">甄子丹</a>、<a href="../Page/苑琼丹.md" title="wikilink">苑琼丹</a>、<a href="../Page/徐少强.md" title="wikilink">徐少强</a>、<a href="../Page/洪欣.md" title="wikilink">洪欣</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虎度門.md" title="wikilink">虎度門</a></p></td>
<td><p>1995</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/怒海威龍.md" title="wikilink">怒海威龍</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賭俠1999.md" title="wikilink">賭俠1999</a></p></td>
<td><p>1999</p></td>
<td><p>省鏡</p></td>
<td><p>劉德華、<a href="../Page/張家輝.md" title="wikilink">張家輝</a>、<a href="../Page/高捷_(演員).md" title="wikilink">高捷</a>、<a href="../Page/黃子揚.md" title="wikilink">黃子揚</a>、<a href="../Page/吳志雄.md" title="wikilink">吳志雄</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暗戰.md" title="wikilink">暗戰</a></p></td>
<td></td>
<td><p>劉德華、<a href="../Page/劉青雲.md" title="wikilink">劉青雲</a> 、<a href="../Page/林雪.md" title="wikilink">林雪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賭神之神.md" title="wikilink">賭神之神</a></p></td>
<td><p>2002</p></td>
<td><p>高俊</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無間道III：終極無間.md" title="wikilink">無間道III：終極無間</a></p></td>
<td><p>2003</p></td>
<td><p>陳俊</p></td>
<td><p><a href="../Page/黎明.md" title="wikilink">黎明</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戰神再現.md" title="wikilink">戰神再現</a></p></td>
<td><p>2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/竊聽風雲.md" title="wikilink">竊聽風雲</a></p></td>
<td><p>2009</p></td>
<td><p>羅耀明</p></td>
<td><p><a href="../Page/劉青雲.md" title="wikilink">劉青雲</a>、<a href="../Page/古天樂.md" title="wikilink">古天樂</a>、<a href="../Page/吳彥祖.md" title="wikilink">吳彥祖</a>、<a href="../Page/朱慧敏.md" title="wikilink">朱慧敏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我的美女老闆.md" title="wikilink">我的美女老闆</a></p></td>
<td><p>2010</p></td>
<td><p>大雄父親</p></td>
<td><p><a href="../Page/何潤東.md" title="wikilink">何潤東</a>、<a href="../Page/吳辰君.md" title="wikilink">吳辰君</a>、<a href="../Page/吳大維.md" title="wikilink">吳大維</a>、<a href="../Page/印小天.md" title="wikilink">印小天</a>、<a href="../Page/陶虹.md" title="wikilink">陶虹</a>、<a href="../Page/包小柏.md" title="wikilink">包小柏</a>、<a href="../Page/范明.md" title="wikilink">范明</a>、<a href="../Page/薑超.md" title="wikilink">薑超</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戰國_(電影).md" title="wikilink">戰國</a></p></td>
<td><p>2011</p></td>
<td><p><a href="../Page/齊國.md" title="wikilink">齊國</a><a href="../Page/宣政吏.md" title="wikilink">宣政吏</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四大名捕_(電影).md" title="wikilink">四大名捕</a></p></td>
<td><p>2012</p></td>
<td><p>王爺</p></td>
<td><p><a href="../Page/鄧超.md" title="wikilink">鄧超</a>、<a href="../Page/劉亦菲.md" title="wikilink">劉亦菲</a>、<a href="../Page/鄭中基.md" title="wikilink">鄭中基</a>、<a href="../Page/鄒兆龍.md" title="wikilink">鄒兆龍</a>、<a href="../Page/黃秋生.md" title="wikilink">黃秋生</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我們畢業的夏天.md" title="wikilink">我們畢業的夏天</a></p></td>
<td><p>2014</p></td>
<td></td>
<td><p><a href="../Page/張嘉倪.md" title="wikilink">張嘉倪</a>、<a href="../Page/吳建飛.md" title="wikilink">吳建飛</a>、<a href="../Page/曾江.md" title="wikilink">曾江</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>分手季2</p></td>
<td><p>2014</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紅髏.md" title="wikilink">紅髏</a></p></td>
<td><p>2015</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寒戰II.md" title="wikilink">寒戰II</a></p></td>
<td><p>2016</p></td>
<td><p>黎永廉</p></td>
<td><p>梁家輝、<a href="../Page/張國柱_(演員).md" title="wikilink">張國柱</a>、<a href="../Page/周潤發.md" title="wikilink">周潤發</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/封神傳奇.md" title="wikilink">封神傳奇</a></p></td>
<td><p>2016</p></td>
<td><p>東海龍王</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江湖之義字當先.md" title="wikilink">江湖之義字當先</a></p></td>
<td><p>2016</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情不等式.md" title="wikilink">愛情不等式</a></p></td>
<td><p>2016</p></td>
<td><p>周啟明</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>電競之王</p></td>
<td><p>2016</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奪路而逃.md" title="wikilink">奪路而逃</a></p></td>
<td><p>2016</p></td>
<td><p>老李</p></td>
<td></td>
<td><p><a href="../Page/張一山.md" title="wikilink">張一山</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龍門劫案.md" title="wikilink">龍門劫案</a></p></td>
<td><p>2016</p></td>
<td><p>(特別出演)</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>隔壁的他</p></td>
<td><p>2016</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>擇日救贖</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>殺手離沫</p></td>
<td><p>2017</p></td>
<td><p>元良</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>一樹一樹紫花開</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>貼身臥底</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>職場三國</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>千術2千王之戰</p></td>
<td><p>2017</p></td>
<td><p>一隻朵</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>海闊天空</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>英雄戰警</p></td>
<td><p>2017</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龍之戰.md" title="wikilink">龍之戰</a></p></td>
<td><p>2017</p></td>
<td><p>彭玉麟</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑白迷宮.md" title="wikilink">黑白迷宮</a></p></td>
<td><p>2017</p></td>
<td><p>九哥</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>千术3决战澳门</p></td>
<td><p>2018</p></td>
<td><p>一隻耳</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>复制情人之意识转移</p></td>
<td><p>2018</p></td>
<td><p>龙川</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>赌局风云2017</p></td>
<td><p>2018</p></td>
<td><p>陈金城</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>贴身卧底</p></td>
<td><p>2018</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>朕不想登基</p></td>
<td><p>2018</p></td>
<td><p>宁王</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>神探风云</p></td>
<td><p>2018</p></td>
<td><p>顾老九</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="even">
<td><p>一条叫招财的鱼</p></td>
<td><p>2018</p></td>
<td><p>金老板</p></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td><p>大微商</p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>極限速遞</p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>江湖喜事</p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>校園武林榜</p></td>
<td><p>待上映</p></td>
<td></td>
<td></td>
<td><p>網絡電影</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 電視節目

  - 1989年：[零時話風情2](../Page/零時話風情2.md "wikilink")（第13集）
  - 1992年：[歡樂滿東華](../Page/歡樂滿東華.md "wikilink")
  - 1993年：[卿撫你的心](../Page/卿撫你的心.md "wikilink")（第4集）
  - 1994年：[江山如此多FUN第](../Page/江山如此多FUN.md "wikilink")2季（第7集）
  - 1994年：[歡樂滿東華](../Page/歡樂滿東華.md "wikilink")
  - 1995年：[三峽狀元爭霸戰](../Page/三峽狀元爭霸戰.md "wikilink")（第5-6集）
  - 1995年：[超級無敵獎門人](../Page/超級無敵獎門人.md "wikilink")（第5集）
  - 1996年：[寰宇風情](../Page/寰宇風情.md "wikilink")（[西雅圖特輯](../Page/西雅圖.md "wikilink")）
    主持
  - 1996年：[為食到瑞士](../Page/為食到瑞士.md "wikilink") 主持
  - 1996年：[為食到荷蘭](../Page/為食到荷蘭.md "wikilink") 主持
  - 1998年：[98世界盃開幕典禮](../Page/98世界盃開幕典禮.md "wikilink")
  - 1998年：[天下無敵獎門人](../Page/天下無敵獎門人.md "wikilink")（第3集）
  - 2000年：[公益開心百萬SHOW](../Page/公益開心百萬SHOW.md "wikilink")（第6集）
  - 2001年：[一筆out消](../Page/一筆out消.md "wikilink")
  - 2005年：[繼續無敵獎門人](../Page/繼續無敵獎門人.md "wikilink")（第29集）
  - 2014年7月28日：[今日VIP](../Page/今日VIP.md "wikilink")
  - 2018年5月3及4日：[Good night
    show全民星戰](../Page/Good_night_show全民星戰.md "wikilink")(第4集及第5集)

## 參考資料

  - [李子雄再婚
    娶內地女星](http://www1.hk.apple.nextmedia.com/realtime/art_main.php?iss_id=20101210&sec_id=7018882&art_id=14752028)
  - [新《水滸》人物照--李子雄飾高俅](http://ent.sina.com.cn/v/p/2010-12-14/09393176342.shtml)
  - [李子雄寫真照片](http://www.tpwang.net/star/%E6%9D%8E%E5%AD%90%E9%9B%84)

## 外部連結

  -
[chi](../Category/李姓.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:無綫電視藝員訓練班](../Category/無綫電視藝員訓練班.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")