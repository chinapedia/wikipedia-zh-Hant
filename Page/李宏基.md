**李宏基**（，），1973年出任[天主教香港教區署理主教](../Page/天主教香港教區.md "wikilink")，後來正式出任天主教香港教區主教（1973年－1974年）。

李宏基1922年3月29日生於廣州，1945年－1952年在[香港仔](../Page/香港仔.md "wikilink")[華南總修院攻讀神哲學課程](../Page/華南總修院.md "wikilink")。1952年7月6日晉升為神父，隨後在西貢小修院任教。1961年任[聖神修院副院長](../Page/聖神修院.md "wikilink")，李宏基曾任[香港聖母無原罪主教座堂主任司鐸](../Page/香港聖母無原罪主教座堂.md "wikilink")（1961年－1968年、1970年－1971年），1968年－1969年任香港仔聖伯多祿堂主任司鐸。

在1969年，李宏基被被委任為香港教區[副主教](../Page/副主教.md "wikilink")，並任[聖神修院院長](../Page/聖神修院.md "wikilink")。其後在1971年7月3日，李宏基被擢升為香港教區[輔理主教](../Page/輔理主教.md "wikilink")，並在1971年9月8日祝聖為[主教](../Page/主教.md "wikilink")。1973年[徐誠斌主教因](../Page/徐誠斌.md "wikilink")[心臟病去世](../Page/心臟病.md "wikilink")，李宏基被任命為香港教區署理主教，其後在被[教廷委任他為香港教區](../Page/教廷.md "wikilink")[主教](../Page/主教.md "wikilink")，1974年4月22日在[香港聖母無原罪主教座堂舉行就職典禮](../Page/香港聖母無原罪主教座堂.md "wikilink")。李宏基於1974年7月23日上午9時許突感胸口劇痛\[1\]，由[堅道主教座堂一位](../Page/聖母無原罪主教座堂_\(香港\).md "wikilink")[神父伴往](../Page/神父.md "wikilink")[聖保祿醫院](../Page/聖保祿醫院.md "wikilink")\[2\]，惟李旋即於10時45分因突發[心臟病在醫院逝世](../Page/心臟病.md "wikilink")\[3\]，終年52歲\[4\]。

[Coat_of_arms_of_Peter_Lei_Wang_Kei.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Peter_Lei_Wang_Kei.svg "fig:Coat_of_arms_of_Peter_Lei_Wang_Kei.svg")

## 資料來源

{{-}}  |width=25% align=center|**前任：**
[徐誠斌主教](../Page/徐誠斌.md "wikilink") |width=50% align=center
colspan="2"|**[香港教區主教](../Page/香港教區主教.md "wikilink")**
1973年－1974年 |width=25% align=center|**繼任：**
[胡振中樞機](../Page/胡振中.md "wikilink")

[Category:天主教香港教区主教](../Category/天主教香港教区主教.md "wikilink")
[H](../Category/李姓.md "wikilink")
[Category:1922年出生](../Category/1922年出生.md "wikilink")
[Category:1974年逝世](../Category/1974年逝世.md "wikilink")

1.

2.
3.
4.