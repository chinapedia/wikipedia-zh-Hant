**李寶臣**（），本名**張忠志**，字**为辅**，[范陽](../Page/范陽.md "wikilink")（今[北京](../Page/北京.md "wikilink")、[保定一帶](../Page/保定.md "wikilink")）人，[唐朝](../Page/唐朝.md "wikilink")[藩鎮](../Page/藩鎮.md "wikilink")，[成德節度使](../Page/成德節度使.md "wikilink")。

原名不详，为范阳内属的[奚族人](../Page/奚族.md "wikilink")。他善骑射，范阳将[张锁高收养](../Page/张锁高.md "wikilink")，[冒张姓](../Page/冒姓_\(行為\).md "wikilink")，名忠志。曾为卢龙府[果毅](../Page/果毅.md "wikilink")，又为[安禄山射生](../Page/安禄山.md "wikilink")。[天宝中](../Page/天宝_\(唐朝\).md "wikilink")，随安禄山入朝，留为射生子弟，出入[禁中](../Page/禁中.md "wikilink")。安禄山在天宝十四年（755年）反唐后，张忠志逃走，又被安禄山收养。更劫持太原尹[杨光翙](../Page/杨光翙.md "wikilink")。效忠[安庆绪](../Page/安庆绪.md "wikilink")，任命他为恒州[刺史](../Page/刺史.md "wikilink")。九位节度使带兵围攻相州，张忠志恐惧，于是复归順唐朝。[唐肃宗即授故官](../Page/唐肃宗.md "wikilink")，封密云[郡公](../Page/郡公.md "wikilink")。[史思明渡河](../Page/史思明.md "wikilink")，张忠志再度叛唐。史思明死，张忠志不肯效忠[史朝义](../Page/史朝义.md "wikilink")。再度投向唐朝政府。

[安史之亂結束後](../Page/安史之亂.md "wikilink")，唐朝為了籠絡河北舊部，將張忠志升为[礼部尚书](../Page/礼部尚书.md "wikilink")，封赵[国公](../Page/国公.md "wikilink")，赐[铁券](../Page/铁券.md "wikilink")，[賜名](../Page/賜名.md "wikilink")**李寶臣**，封為[成德節度使](../Page/成德節度使.md "wikilink")，統[恒州](../Page/恒州.md "wikilink")、[赵州](../Page/赵州.md "wikilink")（今[河北](../Page/河北.md "wikilink")[趙縣](../Page/趙縣.md "wikilink")）、[深州](../Page/深州.md "wikilink")（今[河北](../Page/河北.md "wikilink")[深州市](../Page/深州市.md "wikilink")）、[定州](../Page/定州.md "wikilink")（今[河北](../Page/河北.md "wikilink")[定州市](../Page/定州市.md "wikilink")）、[易州](../Page/易州.md "wikilink")（今[河北](../Page/河北.md "wikilink")[易縣](../Page/易縣.md "wikilink")）五州，駐恒州（今[河北](../Page/河北.md "wikilink")[正定](../Page/正定.md "wikilink")），與[田承嗣](../Page/田承嗣.md "wikilink")、[李懷仙稱](../Page/李懷仙.md "wikilink")“[河朔三鎮](../Page/河朔三鎮.md "wikilink")”。[永泰二年](../Page/永泰_\(唐朝\).md "wikilink")（766年），为赞颂他的政绩功德建有[大唐清河郡王纪功载政之颂碑](../Page/大唐清河郡王纪功载政之颂碑.md "wikilink")。

田承嗣一向自负，看不起李寶臣和淄青節度使[李正己](../Page/李正己.md "wikilink")。李寶臣之弟李寶正原是田承嗣的女婿，一日李寶正在魏州坐客，陪田承嗣之子田維打马球时，马因受惊撞死田维，李寶正被田承嗣所杖杀，由此两镇关系恶化。\[1\]大曆十年（775年），田承嗣突發兵攻陷相州（今河南安陽），四月，唐廷下令贬田承嗣为永州刺吏，并令河东、成德、幽州、淄青、淮两、水平、汴宋、河阳、泽潞各路节度使派兵前往魏博。又命李寶臣和幽州节度使[朱滔以及及河东节度使](../Page/朱滔.md "wikilink")[薛兼训从北向南攻](../Page/薛兼训.md "wikilink")，李正己与淮西节度使[李忠臣等从南向北攻](../Page/李忠臣.md "wikilink")。五月初三，田承嗣部将[霍荣国在磁州投降](../Page/霍荣国.md "wikilink")。十五日，李正己部攻占德州(治安德，今山东陵县)。六月初九，田承嗣派其驍将[裴志清进攻冀州](../Page/裴志清.md "wikilink")，不料裴志清却投降李寶臣。田承嗣亲自率兵进围冀州，又被李寶臣击败，只得烧毁辎重逃遁。承嗣再派[盧子期進攻磁州](../Page/盧子期.md "wikilink")。九月，李寶臣和李正己在枣强会兵，進攻貝州（治清河，今河北清河西北），田承嗣率兵去救，二李退兵。十月，李寶臣與昭義留後[李承昭共同出兵救磁州](../Page/李承昭.md "wikilink")，\[2\]大破盧子期於清水。盧子期被生俘，押送京師斬首。田承嗣之侄[田悅在陳留](../Page/田悅.md "wikilink")（今河南開封東南）為河南諸將所敗。朝廷派宦官[马承倩赴前線慰劳李寶臣](../Page/马承倩.md "wikilink")。马承倩将返程，李宝臣親赠缣帛百匹，但马承倩却嫌礼薄，掷於路上，並肆意辱罵。兵马使[王武俊劝说](../Page/王武俊.md "wikilink"):“今公在军中新立功，竖子尚尔，况寇平之后，以一幅诏书召归阙下，一匹夫耳，不如释承嗣以为己资。”李寶臣遂轉為消極進攻。

李宝臣早有兼并幽州之心，但久为幽州节度使朱滔控制。田承嗣遂以谶语挑撥離間李寶臣與朱滔，後來李寶臣派兵偷襲朱滔未果，雙方交惡。大曆十一年田承嗣暗中勾結李正己，将境内户口、甲兵、谷帛等交给李正己，於是李按兵不動，其危遂解。是年十二月，承嗣又上表謝罪，李正己亦屢向朝廷擔保田承嗣，事遂不了了之。大曆十二年（777年），李宝臣拥有劲卒五万，封陇西郡王，又迁[左仆射](../Page/左仆射.md "wikilink")，封[检校](../Page/检校.md "wikilink")[司空](../Page/司空.md "wikilink")、[同中书门下平章事](../Page/同中书门下平章事.md "wikilink")。[唐德宗即位](../Page/唐德宗.md "wikilink")，拜司空，兼[太子太傅](../Page/太子太傅.md "wikilink")。

[建中二年](../Page/建中_\(唐朝\).md "wikilink")（781年）一月，寶臣死，终年六十四岁。唐德宗为之[废朝三日](../Page/废朝.md "wikilink")，[追赠](../Page/追赠.md "wikilink")[太傅](../Page/太傅.md "wikilink")。次子行軍司馬[李惟岳謀襲父位](../Page/李惟岳.md "wikilink")，[朝廷不允](../Page/朝廷.md "wikilink")，李惟岳、田承嗣的侄子[田悅等起兵叛亂](../Page/田悅.md "wikilink")。

## 家族

  - 长子[李惟誠](../Page/李惟誠.md "wikilink")，庶出，[濮州](../Page/濮州.md "wikilink")[刺史](../Page/刺史.md "wikilink")
  - 次子[李惟岳](../Page/李惟岳.md "wikilink")，嫡出，行軍司馬
  - 三子[李惟簡](../Page/李惟簡.md "wikilink")，[鳳翔節度使](../Page/鳳翔節度使.md "wikilink")，檢校戶部尚書、武安郡王。李惟簡五子：[李元孫](../Page/李元孫.md "wikilink")，三原尉；[李元質](../Page/李元質.md "wikilink")，[濛陽尉](../Page/濛陽.md "wikilink")；[李元立](../Page/李元立.md "wikilink")，[興平尉](../Page/興平.md "wikilink")；[李元本](../Page/李元本.md "wikilink")，河南府參軍；[李銖](../Page/李銖.md "wikilink")。

李宝臣也有几个女儿，[王士真](../Page/王士真.md "wikilink")、[杨荣国](../Page/杨荣国.md "wikilink")（其妻为李惟岳姐）、[李纳](../Page/李纳.md "wikilink")（其妻与李惟誠同胞）、[令狐建都是他的女婿](../Page/令狐建.md "wikilink")。

## 人物评价

  - [李泌](../Page/李泌.md "wikilink")：“贼之骁将，不过史思明、安守忠、田乾真、张忠志、阿史那承庆等数人而已。”
  - [陆贽](../Page/陆贽.md "wikilink")：“往岁为天下所患，咸谓除之则可致升平者，李正巳、李宝臣、梁崇义、田悦是也。”
  - [刘昫](../Page/刘昫.md "wikilink")：“土运中微，群盗孔炽。宝臣附丽安、史，流毒中原，终窃土疆，为国蟊贼。加以武俊之狠狡，为其腹心，或叛或臣，见利忘义，蛇吞蝮吐，垂二百年。哀哉，王政不纲，以至于此。若使明皇不懈于开元之政，姚崇久握于阿衡，讵有柳城一胡，敢窥佐伯，况其下者哉！”
  - [王夫之](../Page/王夫之.md "wikilink")：田承嗣、薛嵩、李宝臣之流，“非有雄武机巧之足以抗天下，而唐之君臣，目睨之而不能动摇其毫发。非诸叛臣之能也，河北之骄兵悍民、气焰已成，而不可扑也。”

## 注釋

## 參考資料

  - 《[旧唐书](../Page/旧唐书.md "wikilink")·卷一百四十二·列传第九十二·李宝臣传》
  - 《[新唐书](../Page/新唐书.md "wikilink")·卷二百一十一·列传第一百三十六·藩镇镇冀·李宝臣传》
  - 《[资治通鉴](../Page/资治通鉴.md "wikilink")·卷二百一十七·唐纪三十三》
  - 《资治通鉴·卷二百一十九·唐纪三十五》
  - 《资治通鉴·卷二百二十二·唐纪三十八》
  - 《资治通鉴·卷二百二十三·唐纪三十九》
  - 《资治通鉴·卷二百二十五·唐纪四十一》
  - 《资治通鉴·卷二百二十六·唐纪四十二》

[Category:唐朝果毅](../Category/唐朝果毅.md "wikilink")
[Category:大燕官员](../Category/大燕官员.md "wikilink")
[Category:唐朝礼部尚书](../Category/唐朝礼部尚书.md "wikilink")
[Category:成德軍節度使](../Category/成德軍節度使.md "wikilink")
[Category:唐朝尚书左仆射](../Category/唐朝尚书左仆射.md "wikilink")
[Category:唐朝同中书门下平章事](../Category/唐朝同中书门下平章事.md "wikilink")
[Category:唐朝司空](../Category/唐朝司空.md "wikilink")
[Category:唐朝太子太傅](../Category/唐朝太子太傅.md "wikilink")
[Category:唐朝追赠太傅](../Category/唐朝追赠太傅.md "wikilink")
[Category:唐朝异姓郡王](../Category/唐朝异姓郡王.md "wikilink")
[Category:唐朝赐李姓及名字者](../Category/唐朝赐李姓及名字者.md "wikilink")
[Category:唐朝奚族人](../Category/唐朝奚族人.md "wikilink")
[Category:中国皇帝养子](../Category/中国皇帝养子.md "wikilink")
[B](../Category/李姓.md "wikilink") [Z](../Category/张姓.md "wikilink")

1.  《[新唐书](../Page/新唐书.md "wikilink")》卷211《李宝臣传》
2.  《旧唐书》作昭义节度使。