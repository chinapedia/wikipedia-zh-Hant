[Democrats-fail-to-regain-veto-power-6.jpg](https://zh.wikipedia.org/wiki/File:Democrats-fail-to-regain-veto-power-6.jpg "fig:Democrats-fail-to-regain-veto-power-6.jpg")
**李彭廣**博士（Dr. Li
Pang-kwong），[倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink")[政治學博士](../Page/政治學.md "wikilink")，現任[智經研究中心研究總監](../Page/智經研究中心.md "wikilink")，前任[嶺南大學](../Page/嶺南大學_\(香港\).md "wikilink")[公共管治研究部主任及](../Page/公共管治.md "wikilink")[政治及社會學系副教授](../Page/政治及社會學系.md "wikilink")（由
2009年1月起已經停薪留職）。

他是[香港政治時事分析專材](../Page/香港政治.md "wikilink")，專長選舉事務分析，見報率頗高，常接受香港新聞傳媒訪問。曾在多家電子傳媒工作，包括[now財經台](../Page/now財經台.md "wikilink")、[香港電台及](../Page/香港電台.md "wikilink")[商業電台主持烽煙節目](../Page/商業電台.md "wikilink")，並為多家報刊撰寫政論文章。

## 參見

  - [now寬頻電視](../Page/now寬頻電視.md "wikilink")

## 外部參考

  - [新報](../Page/新報.md "wikilink") 2009 年 1 月 19 日 - 李彭廣入智經 嶺大停薪留職
  - [李彭廣博士 A9.com](http://a9.com/%22%E6%9D%8E%E5%BD%AD%E5%BB%A3%22)
  - [嶺南大學：李彭廣副教授](http://www.ln.edu.hk/polsci/staff-li.php#research)

[分類:香港嶺南大學教授](../Page/分類:香港嶺南大學教授.md "wikilink")
[category:香港主持人](../Page/category:香港主持人.md "wikilink")

[Category:香港作家](../Category/香港作家.md "wikilink")
[Category:香港時事評論員](../Category/香港時事評論員.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[P](../Category/李姓.md "wikilink")