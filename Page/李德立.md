**李德立**（，1864年－1939年）[英国人](../Page/英国人.md "wikilink")。毕业于[剑桥等大学](../Page/劍橋大學.md "wikilink")。1886年22岁时来华[传教](../Page/传教.md "wikilink")，1895年，李德立从中国官府得到为期999年的租契，开始把[庐山山顶气候凉爽的牯牛岭长冲](../Page/庐山.md "wikilink")（东谷）开发为外侨避暑地，并根据英文（清凉）的音译，把牯牛岭简称为[牯岭](../Page/牯岭.md "wikilink")。

他组织了[董事会和类似租界工部局的牯岭公司房进行管理](../Page/董事会.md "wikilink")，编号出售土地，派设[警察](../Page/警察.md "wikilink")，进行完善的市政建设，沿河滩种植了上万株树木，并规定了建筑密度。因此形成溪水潺潺，绿树成荫，一派田园城市风格的风景优美的居住环境。这块避暑地在同一时期西方传教士开发的中国几大避暑地中规模和影响最大，经营也最成功。

1899年，李德立被聘为上海英资[卜内门洋行的首任总经理](../Page/卜内门公司.md "wikilink")。1904年－1906年间当选为[上海公共租界工部局董事](../Page/上海公共租界工部局.md "wikilink")。曾竭力主张在[租界禁绝](../Page/租界.md "wikilink")[鸦片](../Page/鸦片.md "wikilink")、支持华董进入工部局董事会。1911年[辛亥革命时](../Page/辛亥革命.md "wikilink")，曾以上海外商代表身份，并作为中间人与六国[领事一起参加](../Page/领事.md "wikilink")[清政府与革命党在上海的南北和谈](../Page/清政府.md "wikilink")，同时得到[袁世凯和](../Page/袁世凯.md "wikilink")[孙中山的和平勋章](../Page/孙中山.md "wikilink")。1921年－1923年为[澳大利亚驻华商务代表](../Page/澳大利亚.md "wikilink")。1929年，李德立离开中国，前往[新西兰的](../Page/新西兰.md "wikilink")[凯里凯里开辟新的旅游胜地](../Page/凯里凯里.md "wikilink")。
[Nanbeihetan_1911.png](https://zh.wikipedia.org/wiki/File:Nanbeihetan_1911.png "fig:Nanbeihetan_1911.png")宣布退位當天，[南北议和代表](../Page/南北议和.md "wikilink")[唐绍仪](../Page/唐绍仪.md "wikilink")(左)、[伍廷芳](../Page/伍廷芳.md "wikilink")(右)和英国商人李德立合影，摄于[上海](../Page/上海.md "wikilink")[戈登路（今江宁路）的李德立寓所](../Page/戈登路_\(上海\).md "wikilink")。照片下為各人英文簽名。\[1\]\]\]
[庐山礼拜堂.jpg](https://zh.wikipedia.org/wiki/File:庐山礼拜堂.jpg "fig:庐山礼拜堂.jpg")

## 註釋

[L](../Category/英国人.md "wikilink")
[L](../Category/在华基督教传教士.md "wikilink")
[L](../Category/庐山.md "wikilink")
[L](../Category/1864年出生.md "wikilink")
[L](../Category/1939年逝世.md "wikilink")

1.  原照片一式三张，唐绍仪、伍廷芳、李德立三人分别在三张照片上签名后各存一张。本照片為唐绍仪舊藏，現存[珠海市博物馆](../Page/珠海市博物馆.md "wikilink")。见[珍贵历史照片
    唐绍仪与南北议和，珠海鄉音總第41期，1998年12月](http://www.zhuhai-voice.com/magazine/41/30.html)