**李思浩**（1882年－1968年，一说1881年生），[清朝末年](../Page/清朝.md "wikilink")[民国时期财政要人](../Page/民国.md "wikilink")，曾出任[中国银行](../Page/中国银行.md "wikilink")[总裁](../Page/总裁.md "wikilink")。[字](../Page/字.md "wikilink")**晓沧**，[号](../Page/号.md "wikilink")**赞侯**，[浙江省](../Page/浙江省.md "wikilink")[慈溪县](../Page/慈溪.md "wikilink")（今[宁波](../Page/宁波.md "wikilink")[余姚市](../Page/余姚市.md "wikilink")）人。为中华民国北京政府时期财政金融中枢。一生任职于[清廷](../Page/大清.md "wikilink")、[北洋政府](../Page/北洋政府.md "wikilink")、[国民政府](../Page/国民政府.md "wikilink")、日占上海、[中华人民共和国](../Page/中华人民共和国.md "wikilink")，是近现代中国传奇财经人物。任北洋财长时，建议用[美元结算外债](../Page/美元.md "wikilink")，是美元国际结账的肇始。擅[书法](../Page/书法.md "wikilink")。

## 生平简历

### 清

早年曾中舉人。就读于[京师大学堂](../Page/京师大学堂.md "wikilink")（今[北京大学](../Page/北京大学.md "wikilink")）。授[户部](../Page/户部.md "wikilink")[主事](../Page/主事.md "wikilink")，历数职。曾在清廷的盐政和税务机构担任职务。

### 北京政府

[民国成立后](../Page/民国.md "wikilink")，于元年出任民国临时政府的盐务署科长和厅长。民国二年，担任国家税务筹备处委员。1916年，出任[北洋政府财政次长](../Page/北洋政府.md "wikilink")，并兼任盐务署署长。1917年，出任[北洋政府代理财政部长](../Page/北洋政府.md "wikilink")。1917年6月，兼任[中国银行](../Page/中国银行.md "wikilink")[总裁](../Page/总裁.md "wikilink")。1919年，出任政府财政总长，并兼任盐务署督办。1919年，[徐树铮在](../Page/徐树铮.md "wikilink")[北京创立边业银行](../Page/北京.md "wikilink")，李思浩出任银行[总经理](../Page/总经理.md "wikilink")。

1920年，[直皖战争爆发](../Page/直皖战争.md "wikilink")，皖派[军阀战败](../Page/军阀.md "wikilink")，李思浩因“战争罪”遭受通缉。1924年，[段祺瑞再次得势上台](../Page/段祺瑞.md "wikilink")，李思浩官复原职，出任财政部长，并兼任盐务署督办。1925年，出任关税特别会议委员会委员。

### 国民政府

1933年，[天津大中银行改组](../Page/天津.md "wikilink")，李思浩出任总经理。1934年，主持迁移大中银行总管理处到上海，成为大中银行的业务中心。1936年，民办四明银行被改组成为官商合办银行，出任四明银行商股董事。

[抗日战争爆发](../Page/抗日战争.md "wikilink")，转到[香港避战](../Page/香港.md "wikilink")。香港沦陷，于1941年12月，被日军拘捕囚禁。1942年2月，返回上海，发起创办阜通银行，并出任[董事长](../Page/董事长.md "wikilink")。

### 日占上海

1943年，日伪上海政府试图改组四明银行，李思浩出任汪伪四明银行董事长，并兼任[中国通商银行董事和](../Page/中国通商银行.md "wikilink")[交通银行董事](../Page/交通银行.md "wikilink")。1944年，出任日伪上海市政府政务咨询委员会主任。出任《新闻报》社董事和社长。

### 中华人民共和国

1949年5月，出任上海临时联合救济委员会副主任。1958年，被聘为上海市人民政府顾问。曾担任上海佛教居士林名誉主任。曾任中国上海市[政协委员](../Page/政协.md "wikilink")。1968年1月28日，在上海去世，享年87岁（满86周岁）。

## 荣誉

按年代先后顺序：

  - 五等嘉禾章
  - 四等嘉禾章（晋给）
  - 三等嘉禾章
  - 二等嘉禾章
  - 二等大绶嘉禾章
  - 二等大绶宝光嘉禾章
  - 一等大绶嘉禾章
  - 二等文虎章。

## 家庭

李思浩为[宋朝](../Page/宋朝.md "wikilink")[参知政事](../Page/参知政事.md "wikilink")[李光之第二十七世孙](../Page/李光.md "wikilink")，李自立之二十二世孙，李能（字钟秀）的长子。娶妻余氏。有子李荫枌。有女三人，长女嫁[朱祖经](../Page/朱祖经.md "wikilink")，次女嫁[林景帆](../Page/林景帆.md "wikilink")，幼女嫁[冯保年](../Page/冯保年.md "wikilink")。
李思浩下有弟五人，分别为：

  - 李思瀚（1884年－1909年）。号月波。前清举人。英年早逝，终[同知衔](../Page/同知.md "wikilink")、奉政大夫。
  - 李思沅（1888年－？）。号湘帆。举县学第一。历任两浙[盐运使](../Page/盐运使.md "wikilink")、四川知事、山东盐运使、山东知事、吉黑（吉林、黑龙江两省）榷运局长、滨江总仓仓长等职。授五等嘉禾章，次年晋给四等嘉禾章。
  - 李思瀛（1899年－？）。号阆仙。曾授五等嘉禾章。
  - 李思澐（1901年－？）。号梯青。曾任职于北洋政府国务院秘书厅。曾授六等嘉禾章。
  - 李思洵（1907年－？）。号仰苏。

## 参考

  - [浙江文献网 李思浩 简介](http://zjwx.zju.edu.cn/zjxr_show.jsf?cid=1485)

[L李](../Category/清朝戶部主事.md "wikilink")
[L李](../Category/中國銀行總裁.md "wikilink")
[L李](../Category/京师大学堂校友.md "wikilink")
[L李](../Category/中国书法家.md "wikilink")
[L李](../Category/余姚人.md "wikilink")
[L李](../Category/慈溪人.md "wikilink")
[L李](../Category/宁波裔上海人.md "wikilink")
[S李思浩](../Category/三江李氏.md "wikilink")
[L李](../Category/1882年出生.md "wikilink")
[L李](../Category/1968年逝世.md "wikilink")
[S](../Category/李姓.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:中國佛教徒](../Category/中國佛教徒.md "wikilink")