**李思蓓**（，），原名**李慧萍**，於1995年[無線電視藝員訓練班第八期藝員進修班畢業後加入無線電視](../Page/無線電視藝員訓練班.md "wikilink")，同期的正式學員分別有[雷穎怡](../Page/雷晴雯.md "wikilink")、[伍慧珊](../Page/伍慧珊.md "wikilink")、[羅貫峰](../Page/羅貫峰.md "wikilink")、[李天翔](../Page/李天翔.md "wikilink")、[劉永晉](../Page/劉永晉.md "wikilink")、[盧卓南](../Page/盧卓南.md "wikilink")、[孫恩明](../Page/孫恩明.md "wikilink")、[盧慶輝和](../Page/盧慶輝.md "wikilink")[蓋世寶等](../Page/蓋世寶.md "wikilink")，同班旁聽生有[湯盈盈](../Page/湯盈盈.md "wikilink")、[姚瑩瑩](../Page/姚瑩瑩.md "wikilink")、[滕麗名](../Page/滕麗名.md "wikilink")、[劉永健](../Page/劉永健.md "wikilink")、[關可維](../Page/關可維.md "wikilink")、[孫大龍](../Page/孫大龍.md "wikilink")、[李詠儀及](../Page/李詠儀.md "wikilink")[張潔蓮等](../Page/張潔蓮.md "wikilink")。

1998年與[雷穎怡參與主持當屆世界盃而同被稱為](../Page/雷晴雯.md "wikilink")「世界妹」，為及後歷屆「世界妹」之始祖之一。同年再與[雷穎怡](../Page/雷晴雯.md "wikilink")、[汪曉文](../Page/汪曉文.md "wikilink")（後易名[汪琳](../Page/汪琳.md "wikilink")）、[梁佩盈](../Page/梁佩盈.md "wikilink")、[翟佩雲](../Page/翟佩雲.md "wikilink")、[梁雪湄](../Page/梁雪湄.md "wikilink")、[任港秀](../Page/任港秀.md "wikilink")、[姚樂怡等組成](../Page/姚樂怡.md "wikilink")[電波少女](../Page/電波少女.md "wikilink")（「愛心少女」前身，現已解散）。後簽約[漢傳媒](../Page/漢傳媒.md "wikilink")，曾參與多部TVB電視劇，及後進軍電影圈，並且把工作重心轉移於電影上。

2005年起，李思蓓進軍商界，經營美容及按摩業務，為泰式按摩連鎖店泰殿之東主。

2012年年初，低調簽約上海[汪子琦工作室成為旗下藝人](../Page/汪子琦工作室.md "wikilink")，轉往中國大陸發展。

## 個人生活

李思蓓曾經和藝員進修班同班同學[李天翔拍拖](../Page/李天翔.md "wikilink")4年，後來和平分手，至今維持[好友關係](../Page/好友.md "wikilink")。

## 節目主持

  - 1998年：[第十六屆世界盃](../Page/第十六屆世界盃.md "wikilink")
  - 1999年：[城市追擊](../Page/城市追擊.md "wikilink")
  - 1999年：可口可樂最佳運動員選舉
  - 2000年：TVB音樂節目[音樂幹線](../Page/音樂幹線.md "wikilink")
  - 2001年：TVB音樂節目[音樂幹線](../Page/音樂幹線.md "wikilink")

## 电视剧

  - 1996年：TVB劇集《[O記實錄II](../Page/O記實錄II.md "wikilink")》 飾 汶汶
  - 1998年：TVB劇集《[天地豪情](../Page/天地豪情.md "wikilink")》 飾 護士
  - 1998年：TVB劇集《[妙手仁心](../Page/妙手仁心.md "wikilink")》 飾 崔姑娘
  - 1999年：TVB劇集《[鑑證實錄II](../Page/鑑證實錄II.md "wikilink")》 飾 梁雪麗
    Cherry（「唐詠心分屍案」及「金妙玲、葉冠榮命案」）
  - 1999年：TVB劇集《[創世紀](../Page/創世紀_\(電視劇\).md "wikilink")》飾 女明星（第18集）
  - 1999年：TVB劇集《[創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")》飾
    Suki（第94集）
  - 2000年：TVB劇集《[男親女愛](../Page/男親女愛.md "wikilink")》飾 售貨員（第89集）
  - 2000年：TVB劇集《[FM701](../Page/FM701.md "wikilink")》飾
    [設計師](../Page/設計師.md "wikilink")（第20集）
  - 2002年：TVB劇集《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》飾
    [唱片](../Page/唱片.md "wikilink")[騎師](../Page/騎師.md "wikilink")（第34集）
  - 2003年：TVB剧集《[冲上云霄](../Page/冲上云霄.md "wikilink")》饰 莉莉（第1-2集）
  - 2003年：TVB剧集《[律政新人王](../Page/律政新人王.md "wikilink")》饰 葉翠珊
  - 2003年：TVB剧集《[花樣中年](../Page/花樣中年.md "wikilink")》饰 Janet
  - 2004年：TVB剧集《[无名天使3D](../Page/无名天使3D.md "wikilink")》饰 CTU警员
  - 2004年：TVB剧集《[棟篤神探](../Page/棟篤神探.md "wikilink")》饰 Eva
  - 2005年：ATV剧集《[美丽传说2星愿](../Page/美丽传说2星愿.md "wikilink")》
  - 2005年：ATV剧集《[情陷夜中环](../Page/情陷夜中环.md "wikilink")》饰 Barbie
  - 2006年：ATV剧集《[香港奇案实录](../Page/香港奇案实录.md "wikilink")》饰 廖雪蓉／廖雪珊
  - 2006年：ATV剧集《[情陷夜中环II](../Page/情陷夜中环II.md "wikilink")》饰 黄红红
  - 2006年：TVB剧集《[转世惊情](../Page/转世惊情.md "wikilink")》饰 袁小小
  - 2007年：TVB剧集《[天下第一](../Page/天下第一.md "wikilink")》饰 利秀公主
  - 2008年：ATV劇集《[火蝴蝶](../Page/火蝴蝶.md "wikilink")》饰 錢美美
  - 2013年：中國大陸《[土地公土地婆](../Page/土地公土地婆.md "wikilink")》饰 崔妤
  - 2015年：中國大陸《[仙侠剑](../Page/仙侠剑.md "wikilink")》饰 邓柔柔
  - 2015年：中國大陸《[新济公活佛之宫中风云](../Page/新济公活佛.md "wikilink")》饰 红梅霜
  - 2016年：中國大陸《[天天有喜之人间有爱](../Page/天天有喜之人间有爱.md "wikilink")》饰 劉利
  - 待播中：中國大陸《[情谜睡美人之欲望的姐妹](../Page/情谜睡美人之欲望的姐妹.md "wikilink")》饰

## 電影

  - 1998年：《[青春援助交際 PR Girls](../Page/青春援助交際_PR_Girls.md "wikilink")》
  - 1999年：《[摩登姑婆屋](../Page/摩登姑婆屋.md "wikilink")》
  - 1999年：《[殺手風雲之買大開細](../Page/殺手風雲之買大開細.md "wikilink")》
  - 2000年：《[古惑女2](../Page/古惑女2.md "wikilink")》
  - 2000年：《[怪異集：你回來吧！](../Page/怪異集：你回來吧！.md "wikilink")》
  - 2000年：《[中華賭俠](../Page/中華賭俠.md "wikilink")》
  - 2002年：《[艷星秘密生活之她的一生](../Page/艷星秘密生活之她的一生.md "wikilink")》
  - 2002年：《[皇家保鏢](../Page/皇家保鏢.md "wikilink")》
  - 2002年：《[蛇蠍情人](../Page/蛇蠍情人.md "wikilink")》
  - 2003年：《[百分百感覺2003](../Page/百分百感覺2003.md "wikilink")》
  - 2004年：《[寬頻聊齋](../Page/寬頻聊齋.md "wikilink")》
  - 2005年：《[雀聖2自摸天后](../Page/雀聖2自摸天后.md "wikilink")》飾 波波
  - 2005年：《[瘦身](../Page/瘦身.md "wikilink")》
  - 2005年：《[最後的晚餐](../Page/最後的晚餐.md "wikilink")》（日本）
  - 2006年：《[愛上屍新娘](../Page/愛上屍新娘.md "wikilink")》飾 吸血鬼Mary
  - 2006年：《[情意拳拳](../Page/情意拳拳.md "wikilink")》（客串）
  - 2006年：《[臥虎](../Page/臥虎.md "wikilink")》飾 按摩女郎
  - 2007年：《[妻骨未寒](../Page/妻骨未寒.md "wikilink")》
  - 2007年：《[美女食神](../Page/美女食神.md "wikilink")》飾 瑪麗蓮
  - 2008年：《[黑勢力](../Page/黑勢力.md "wikilink")》飾 YoYo
  - 2010年：《[撕票風雲](../Page/撕票風雲.md "wikilink")》
  - 2012年：《[爛賭夫鬥爛賭妻](../Page/爛賭夫鬥爛賭妻.md "wikilink")》 飾 舒青

## 資料來源／參考文獻

  -
  -
  -
  -
## 相關網站

  - [汪子琦工作室新浪認証微博](http://weibo.com/momowangziqi)
  - [李思蓓新浪認証微博](http://weibo.com/u/2625385767)

[si](../Category/李姓.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")