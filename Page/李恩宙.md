**李恩宙**（，），[韩国](../Page/韩国.md "wikilink")[女演员](../Page/女演员.md "wikilink")，姓名常誤寫為**李恩珠**、**李銀珠**。

## 生平

李恩宙在[全羅北道](../Page/全羅北道.md "wikilink")[群山市出生](../Page/群山市.md "wikilink")，[首爾的](../Page/首爾.md "wikilink")[檀國大學戲劇電影學系畢業](../Page/檀國大學.md "wikilink")。高中時，曾擔任制服形像[模特兒](../Page/模特兒.md "wikilink")。1998年，首次主演電視劇，在[SBS電視台的](../Page/SBS_\(韓國\).md "wikilink")《[白夜3.98](../Page/白夜3.98.md "wikilink")》演出。1999年，首度參與電影，於《[紅鱒](../Page/紅鱒.md "wikilink")》演出。2001年，她透過試鏡成為當時著名新進導演[洪尚秀的作品](../Page/洪尚秀.md "wikilink")《[處女心經](../Page/處女心經.md "wikilink")》（）的女主角，並憑著本片奪得[大鐘賞的新人獎](../Page/大鐘賞.md "wikilink")。2004年，她在現時韓國史上第二入場觀看率的電影《[太極旗飄揚](../Page/太極旗飄揚.md "wikilink")》中擔任女主角，飾演[朝鲜战争中為求家庭生計而參與](../Page/朝鲜战争.md "wikilink")[共產黨集會並被因此被南方政府槍殺的角色](../Page/朝鲜劳动党.md "wikilink")。

2005年2月22日，在[京畿道](../Page/京畿道.md "wikilink")[城南市](../Page/城南市.md "wikilink")[盆唐區的公寓中被家人發現留下遺書](../Page/盆唐區.md "wikilink")，並且同時[割腕及](../Page/割腕.md "wikilink")[上吊雙料自殺](../Page/上吊.md "wikilink")。由於李恩宙是[基督徒](../Page/基督徒.md "wikilink")，她的自殺事件使人感到奇怪。有報導指她在2004年拍攝電影《[紅字](../Page/红字_\(电影\).md "wikilink")》（*the
Scarlet
Letter*，意思即“朱紅色的字”）時由於有裸露及與性愛場面，因而受到精神壓力長期失眠。這個說法受到經理人公司否認；而且，她早在《[處女心經](../Page/處女心經.md "wikilink")》時都已經有裸露及做愛的場面（但鏡頭經過黑白處理）。然而，另有一個說法，指李恩宙自从《[爱的蹦极](../Page/爱的蹦极.md "wikilink")》（又譯《[情約笨豬跳](../Page/情約笨豬跳.md "wikilink")》）開始，到《[向左愛向右愛](../Page/向左愛向右愛.md "wikilink")》及《[太極旗飄揚](../Page/太極旗飄揚.md "wikilink")》，她所演的角色不是意外早死、就是因病早逝，都是紅顏薄命，所以指她可能是不能從之前劇集中抽離角色。

2月22日也是她在《情約笨豬跳》角色的死亡日期。

正確生日是陽曆1980年12月22日（農曆11月16日），常被媒體誤將「農曆」生日刊登成「陽曆」生日。

李恩宙的遺體[火葬](../Page/火葬.md "wikilink")。根據韓國人的[習俗](../Page/習俗.md "wikilink")，白頭人送黑頭人的喪事會用火葬，以使死者得以安心。

## 演出作品

### 電視劇

  - 1997年：SBS《[Start](../Page/Start_\(電視劇\).md "wikilink")》
  - 1998年：SBS《[白夜3.98](../Page/白夜3.98.md "wikilink")》
  - 1999年：KBS《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》
  - 2004年：MBC《[火鳥](../Page/火鳥_\(韓國電視劇\).md "wikilink")》飾演 李智恩

### 電影

  - 1999年：《[紅鱒](../Page/紅鱒.md "wikilink")》
  - 2000年：《》（，又譯《噢！水晶》）
  - 2000年：《[到海邊去](../Page/到海邊去.md "wikilink")》（ ）（客串）
  - 2001年：《[情約笨豬跳](../Page/情約笨豬跳.md "wikilink")》（，又譯：《爱的蹦极》）
  - 2002年：《[向左愛向右愛](../Page/向左愛向右愛.md "wikilink")》（；漢字表記：*戀愛小說*）
  - 2002年：《[凶房](../Page/凶房.md "wikilink")》（）
  - 2003年：《[天上的庭園](../Page/天上的庭園.md "wikilink")》（，又譯："藍天花園"）
  - 2004年：《[太極旗飄揚](../Page/太極旗飄揚.md "wikilink")》（）
  - 2004年：《[再見UFO](../Page/再見UFO.md "wikilink")》（）
  - 2004年：《[紅字](../Page/红字_\(电影\).md "wikilink")》（）

### MV

  - 1999年：金章勳《悲傷禮物》【[高洙](../Page/高洙.md "wikilink")】
  - 2001年：朱永勳《願望》【[金勝友](../Page/金勝友.md "wikilink")】
  - 2003年：[Bobo](../Page/姜成妍.md "wikilink")《遲來的後悔》【[金太祐](../Page/金太祐.md "wikilink")、[朴正哲](../Page/朴正哲.md "wikilink")】
  - 2004年：Tim《我很感激》【[俞承豪](../Page/俞承豪.md "wikilink")】

## 外部連結

  - 李恩宙的簡介
      - [韓國映畫](http://www.krmdb.com/artist/LeeEunjoo/index.b5.shtml)
      - [imdb的簡介](http://us.imdb.com/name/nm0497249/)
      - [為甚麼是“李恩宙”而不是“李恩珠”](http://bbs.org.hk/bbscon.php?board=Entertainment&id=4215)
      - [EPG](https://web.archive.org/web/20070829163520/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=122)

      - [Yahoo
        Korea](http://kr.search.yahoo.com/search?p=%BF%B5%C8%AD%B9%E8%BF%EC+%C0%CC%C0%BA%C1%D6+&fr=fr_iy)

  - 李恩宙的死訊
      - [朝鮮日報](http://chinese.chosun.com/big5/site/data/html_dir/2005/02/22/20050222000009.html)
      - [李恩宙最後化粧品廣告](http://www.enprani.co.kr/mov/enprani_30s.mpg)（MPEG，5.13MB）
      - [有關公司對李恩宙的死訊所發的新聞稿](https://web.archive.org/web/20050301060110/http://www.enprani.co.kr/special/050224-popup.htm)（韓語）

[Category:大鐘獎獲獎者](../Category/大鐘獎獲獎者.md "wikilink")
[Category:韩国電影演员](../Category/韩国電影演员.md "wikilink")
[Category:韓國女性模特兒](../Category/韓國女性模特兒.md "wikilink")
[Category:韓國新教徒](../Category/韓國新教徒.md "wikilink")
[Category:韩国自杀艺人](../Category/韩国自杀艺人.md "wikilink")
[Category:女性自殺者](../Category/女性自殺者.md "wikilink")
[Category:檀國大學校友](../Category/檀國大學校友.md "wikilink")
[Category:群山市出身人物](../Category/群山市出身人物.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")