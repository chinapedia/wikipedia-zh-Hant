**李文雄**（），[臺灣](../Page/臺灣.md "wikilink")[演化生物學家](../Page/演化生物學.md "wikilink")，[屏東縣人](../Page/屏東縣.md "wikilink")。[中央研究院院士](../Page/中央研究院院士.md "wikilink")，[美國藝術與科學院院士](../Page/美國藝術與科學院.md "wikilink")，[美國國家科學院院士](../Page/美國國家科學院.md "wikilink")。曾任[中央研究院生物多樣性研究中心主任](../Page/中央研究院生物多樣性研究中心.md "wikilink")。

## 教育经历

1965年[中原理工學院土木系畢業](../Page/中原大學.md "wikilink")。1968年獲[國立中央大學地球物理學碩士學位](../Page/國立中央大學.md "wikilink")。此後赴美，1972年獲[布朗大學應用數學博士學位](../Page/布朗大學.md "wikilink")。1973年執教[德州大學醫學中心](../Page/德州大學.md "wikilink")，講授[遺傳學](../Page/遺傳學.md "wikilink")。1998年到[芝加哥大學](../Page/芝加哥大學.md "wikilink")[演化生物學系](../Page/演化生物學.md "wikilink")，受聘為[喬治·比德爾講座教授](../Page/喬治·韋爾斯·比德爾.md "wikilink")。

## 研究專長

演化生物學、遺傳學、基因組學。

## 獲得榮譽

  - 中央研究院院士（1998年）

  - （2003年）

  - 國立中央大學傑出校友（2006年）

## 參考資料

## 外部連結

  - [李文雄在中央研究院網頁](http://biodiv.sinica.edu.tw/research.php?index_id=3)

[Category:中央研究院生命科學組院士](../Category/中央研究院生命科學組院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:芝加哥大學教師](../Category/芝加哥大學教師.md "wikilink")
[Category:布朗大學校友](../Category/布朗大學校友.md "wikilink")
[理](../Category/國立中央大學校友.md "wikilink")
[Category:中原大學校友](../Category/中原大學校友.md "wikilink")
[Category:屏東人](../Category/屏東人.md "wikilink")
[W文](../Category/李姓.md "wikilink")
[Category:台灣生物學家](../Category/台灣生物學家.md "wikilink")
[Category:演化生物學家](../Category/演化生物學家.md "wikilink")
[Category:群体遗传学家](../Category/群体遗传学家.md "wikilink")