**李昌鈺**（，），著名[偵探](../Page/偵探.md "wikilink")、[刑事鑑識學專家](../Page/刑事鑑識學.md "wikilink")，生於[江蘇](../Page/江蘇省_\(中華民國\).md "wikilink")[如皋](../Page/如皋.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[中央警官學校](../Page/中央警察大學.md "wikilink")(現[中央警察大學](../Page/中央警察大學.md "wikilink")）畢業，[紐約市立大學約翰傑刑事司法學院刑事科學系](../Page/紐約市立大學.md "wikilink")[學士](../Page/學士.md "wikilink")，[美國](../Page/美國.md "wikilink")[紐約大學](../Page/紐約大學.md "wikilink")[生物化學](../Page/生物化學.md "wikilink")[碩士](../Page/碩士.md "wikilink")、[博士](../Page/博士.md "wikilink")，曾任[康乃狄克州警政廳長](../Page/康乃狄克州.md "wikilink")、現任康州[紐黑文大学終身](../Page/紐黑文大学.md "wikilink")[教授](../Page/教授.md "wikilink")，擁有中華民國與美國雙重國籍\[1\]\[2\]。李昌鈺目前定居美國[康乃狄克州](../Page/康乃狄克州.md "wikilink")，亦於中華民國[新北市設籍](../Page/新北市.md "wikilink")。

## 簡歷

李昌鈺1938年生於[中華民國江蘇省如皋縣一個富裕鹽商家庭](../Page/中華民國_\(大陸時期\).md "wikilink")。1949年前後，他隨母親和其他家人一同遷往臺灣，其父則在搭乘[太平輪前往台灣時不幸遇難身亡](../Page/太平輪沉沒事件.md "wikilink")。李昌鈺自言，父親的過世-{致}-使家道中落，促使他選擇投考公費的警校。初中就讀於[強恕中學](../Page/強恕中學.md "wikilink")\[3\]。高中畢業後，參加大專聯考錄取分發至台灣省立海專航海科（今[國立臺灣海洋大學商船學系](../Page/國立臺灣海洋大學.md "wikilink")）；爾後，李考上中央警官學校，並前往就讀。

1960年於中央警官學校（現在的[中央警察大學](../Page/中央警察大學.md "wikilink")）取得[警察](../Page/警察.md "wikilink")[科學學士](../Page/科學.md "wikilink")[學位](../Page/學位.md "wikilink")，並於[臺北市政府警察局工作](../Page/臺北市政府警察局.md "wikilink")；同年，他獲晉升為[警長](../Page/警長.md "wikilink")，成為中華民國臺灣史上最年輕的警長。

1964年，赴美国留学，並于1972年[紐約市立大學約翰](../Page/紐約市立大學.md "wikilink")·杰伊學院刑事科學系取得學士學位。接著又先后取得[纽约大学](../Page/纽约大学.md "wikilink")[生物化学及分子化学硕士學位](../Page/生物化学.md "wikilink")（1974年）和[博士学位](../Page/博士.md "wikilink")（1975年）。

李昌钰现任[美国康乃狄克州科学咨询中心的榮誉主任](../Page/美国.md "wikilink")（），并且是[紐海文大學](../Page/紐黑文大學.md "wikilink")[法医学的全职教授](../Page/法医学.md "wikilink")，尚协助该大学设立了“李昌钰法医学[研究所](../Page/研究所.md "wikilink")”。在此之前，他曾担任康乃狄克州[公共安全委员](../Page/公共安全.md "wikilink")、康乃狄克州法医实验室主任，和1979年至2000年的首席犯罪学专家。

他鑑識過幾個重大的案件，如JonBenét
Ramsey命案、[OJ·辛普森案](../Page/辛普森案.md "wikilink")、Laci
Peterson謀殺案、美軍華裔士兵[陳宇暉自殺案](../Page/陳宇暉.md "wikilink")，甚至參與調查了美國司法上最疑點重重的兩件命案：[甘迺迪總統被](../Page/甘迺迪總統.md "wikilink")[刺殺案及其胞弟](../Page/刺殺.md "wikilink")[羅伯特·弗朗西斯·甘迺迪被刺案](../Page/羅伯特·弗朗西斯·甘迺迪.md "wikilink")。

李与[美国政治高层关系良好](../Page/美国政治.md "wikilink")，与前总统[克林顿私交甚密](../Page/比爾·柯林頓.md "wikilink")，曾参与调查[莱温斯基事件](../Page/莱温斯基事件.md "wikilink")，以及[九一一恐怖襲擊事件後的鑑識工作](../Page/九一一恐怖襲擊事件.md "wikilink")。李也數度回台灣協助幾個重大刑案的鑑識，包括[桃園縣縣長](../Page/桃園縣縣長.md "wikilink")[劉邦友血案](../Page/劉邦友血案.md "wikilink")、[彭婉如命案](../Page/彭婉如命案.md "wikilink")、[白曉燕命案](../Page/白曉燕命案.md "wikilink")、[三一九槍擊案](../Page/三一九槍擊案.md "wikilink")、[蘇建和案等](../Page/蘇建和案.md "wikilink")。

2006年李昌鈺獲選為[Discovery頻道](../Page/Discovery.md "wikilink")《台灣人物誌》的6名主角之1。

## 桌腳理論

其將偵查[犯罪中所謂的](../Page/犯罪.md "wikilink")「現場、[人證](../Page/人證.md "wikilink")、物證以及[運氣](../Page/運氣.md "wikilink")」四項列為偵破的要項，稱之為桌腳理論。

## 家庭

首任妻子為宋妙娟（1940年4月18日—2017年7月31日）。育有一子李孝約和一女李孝美\[4\]。2018年12月1日在美國康乃迪克州與揚州企業家蔣霞萍再婚\[5\]。

## 作品

### 著作

| 年份         | 書名                                                     | 作者                                                   | 譯者        | 出版社                                | ISBN               |
| ---------- | ------------------------------------------------------ | ---------------------------------------------------- | --------- | ---------------------------------- | ------------------ |
| 1995年初版    | 《刑案現場蒐證》                                               | 李昌鈺 原著                                               | 林茂雄 翻譯    | 中央警察大學                             | ISBN 9578764782    |
| 1998年初版    | 《神探李昌鈺破案實錄》                                            | 李昌鈺 口述                                               | 鄧-{洪}- 整理 | [時報文化](../Page/時報文化.md "wikilink") | ISBN 9571327360    |
| 2000年初版    | 《讓證據說話：神探李昌鈺破案實錄Ⅱ》                                     | 李昌鈺、劉永毅 合著                                           |           | 時報文化                               | ISBN 9571331996    |
| 2002年初版    | 《重返犯案現場：神探李昌鈺破案實錄3》（*Famous crimes revisited*）         | 李昌鈺、傑瑞·拉比歐拉（Jerry Labriola）合著                        | 郭玉芬 譯     | 時報文化                               | ISBN 9571336319    |
| 2003年初版    | 《犯罪現場：李昌鈺刑事鑑定指導手冊》（*Henry Lee's crime scene handbook*） | 李昌鈺、提姆西·龐巴（Timothy Palmbach）、瑪琍琳·米勒（Marilyn Miller）著 | 李俊億 譯     | 商周出版                               | ISBN 9867747976    |
| 2005年初版    | 《重返319槍擊現場：神探李昌鈺破案實錄4》（*Cracking more cases*）          | 李昌鈺、湯瑪斯·歐尼爾（Thomas W. O'Neil）、夏珍 作                   | 侯秀琴、楊明暐 譯 | 時報文化                               | ISBN 9571343013    |
| 2007年3月初版  | 《非自願消失：神探李昌鈺破案實錄5》（*Dr. Henry Lee's forensic files*）   | 李昌鈺、傑瑞·拉比歐拉（Jerry Labriola）著                         | 周樹芬、侯秀琴 譯 | 時報文化                               | ISBN 9789571346199 |
| 2014年12月初版 | 《化不可能為可能：李昌鈺的鑑識人生》                                     | 李昌鈺 著                                                |           | 平安文化                               | ISBN 9789578039377 |

## 電視節目

### 台灣

| 年份         | 節目名稱                                             | 電視頻道                           | 導演                                                                | 備註        |
| ---------- | ------------------------------------------------ | ------------------------------ | ----------------------------------------------------------------- | --------- |
| 2019年3月27日 | [鑑識英雄II 正義之戰](../Page/鑑識英雄II_正義之戰.md "wikilink") | [中視](../Page/中視.md "wikilink") | [張佳賢](../Page/張佳賢.md "wikilink")、[蔣凱宸](../Page/蔣凱宸.md "wikilink") | 擔任主演兼任總指導 |
|            |                                                  |                                |                                                                   |           |

### 中國大陸

| 年份           | 節目名稱                                 | 電視頻道                                                                   | 主持人 | 備註       |
| ------------ | ------------------------------------ | ---------------------------------------------------------------------- | --- | -------- |
| 2015年\~2017年 | [挑戰不可能](../Page/挑戰不可能.md "wikilink") | [中央電視台](../Page/中央電視台.md "wikilink")[綜合頻道](../Page/綜合頻道.md "wikilink") | 撒貝寧 | 擔任三季節目評委 |

## 參考文獻

[Category:台灣生物化學家](../Category/台灣生物化學家.md "wikilink")
[Category:美國生物化學家](../Category/美國生物化學家.md "wikilink")
[Category:臺灣警務人員](../Category/臺灣警務人員.md "wikilink")
[Category:紐約大學校友](../Category/紐約大學校友.md "wikilink")
[Category:紐約市立大學校友](../Category/紐約市立大學校友.md "wikilink")
[Category:國立臺灣海洋大學校友](../Category/國立臺灣海洋大學校友.md "wikilink")
[Category:中央警察大學校友](../Category/中央警察大學校友.md "wikilink")
[Category:臺北市立大同高級中學校友](../Category/臺北市立大同高級中學校友.md "wikilink")
[Category:台北市榮譽市民](../Category/台北市榮譽市民.md "wikilink")
[Category:江苏人](../Category/江苏人.md "wikilink")
[Category:歸化美國公民的中華民國人](../Category/歸化美國公民的中華民國人.md "wikilink")
[Category:百人会会员](../Category/百人会会员.md "wikilink")
[Category:台灣戰後江蘇移民](../Category/台灣戰後江蘇移民.md "wikilink")
[Category:犯罪學學者](../Category/犯罪學學者.md "wikilink")
[Category:纽黑文大学教师](../Category/纽黑文大学教师.md "wikilink")
[Category:美国法医科学家](../Category/美国法医科学家.md "wikilink")
[Category:臺灣法醫](../Category/臺灣法醫.md "wikilink")
[Category:如皋人](../Category/如皋人.md "wikilink")
[Chang昌](../Category/李姓.md "wikilink")

1.  [獨／國際鑑識專家李昌鈺
    遷籍新北市林口區](https://udn.com/news/story/7323/3097416)，[聯合報](../Page/聯合報.md "wikilink")，2018年4月20日
2.
3.  [神探李昌鈺破案實錄，ISBN 9571327360。](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=mcdb\(SellItems\)&id=TC0001&p=excerpt&exid=35910)
4.  [李昌鈺悼念夫人：少了她，我無法獨自工作](https://www.worldjournal.com/5104606/article-%E6%9D%8E%E6%98%8C%E9%88%BA%E6%82%BC%E5%BF%B5%E5%A4%AB%E4%BA%BA%EF%BC%9A%E5%B0%91%E4%BA%86%E5%A5%B9%EF%BC%8C%E6%88%91%E7%84%A1%E6%B3%95%E7%8D%A8%E8%87%AA%E5%B7%A5%E4%BD%9C/)，世界日報，2017年8月4日
5.  [神探李昌鈺康州完婚 5克拉鑽戒用畫的](https://udn.com/news/story/6813/3513452)，世界日報，2018年12月2日