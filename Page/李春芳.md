**李春芳**（），[字](../Page/表字.md "wikilink")**子实**，[号](../Page/号.md "wikilink")**石麓**，[直隸](../Page/南直隶.md "wikilink")[兴化縣](../Page/兴化縣.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[兴化市](../Page/兴化市.md "wikilink")）人，祖籍[句容縣](../Page/句容縣.md "wikilink")，人稱好好先生，句容相國。[明朝狀元](../Page/明朝.md "wikilink")、政治人物，官至[內閣首輔](../Page/內閣首輔.md "wikilink")。

## 生平

李春芳生于[明](../Page/明.md "wikilink")[正德五年](../Page/正德_\(明朝\).md "wikilink")（1510年），少年家貧，在句容茅山華陽洞借燈讀書。后來富貴，重修廟宇以為報答。[嘉靖二十六年](../Page/嘉靖.md "wikilink")（1547年）丁未科[状元](../Page/状元.md "wikilink")\[1\]\[2\]\[3\]，與[張居正同科](../Page/張居正.md "wikilink")，授[翰林学士](../Page/翰林学士.md "wikilink")，累官[礼部尚书](../Page/礼部尚书.md "wikilink")\[4\]。性恭谨刻苦，[隆庆初年拜](../Page/隆庆.md "wikilink")[首辅](../Page/首辅.md "wikilink")，与[严讷](../Page/严讷.md "wikilink")、[郭朴](../Page/郭朴.md "wikilink")、[袁炜同有](../Page/袁炜.md "wikilink")“青词宰相”之目。卒于[万历十二年](../Page/万历.md "wikilink")（1584年）。贈太師，諡文定。

## 著作

著有《贻安堂集》十卷，一說李春芳是《[西游记](../Page/西游记.md "wikilink")》的作者\[5\]\[6\]。

## 家族

李春芳曾祖父李秀、祖父李旭、父李镗，母徐氏\[7\]。後代繁衍成[兴化望族](../Page/兴化.md "wikilink")，其孙[李思诚官任礼部尚书](../Page/李思诚.md "wikilink")，四世孙[李滢尤邃于经学](../Page/李滢.md "wikilink")，五世孙[李清擔任刑科给事中](../Page/李清_\(崇禎進士\).md "wikilink")，李清子[左都御史](../Page/左都御史.md "wikilink")[李枏曾为](../Page/李枏.md "wikilink")[孔尚任](../Page/孔尚任.md "wikilink")《[桃花扇](../Page/桃花扇.md "wikilink")》作序，“子孙曾玄五世，男女数十百人，官至[尚书](../Page/尚书.md "wikilink")、卿寺以及[知府](../Page/知府.md "wikilink")、[知县者十数人](../Page/知县.md "wikilink")，而[布衣](../Page/布衣.md "wikilink")、[诸生以诗文名于世者数人](../Page/诸生.md "wikilink")”\[8\]\[9\]。

## 注釋

[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝武英殿大学士](../Category/明朝武英殿大学士.md "wikilink")
[Category:明朝翰林院學士](../Category/明朝翰林院學士.md "wikilink")
[Category:明朝翰林院修撰](../Category/明朝翰林院修撰.md "wikilink")
[Category:明朝太常寺少卿](../Category/明朝太常寺少卿.md "wikilink")
[Category:明朝禮部右侍郎](../Category/明朝禮部右侍郎.md "wikilink")
[Category:明朝禮部左侍郎](../Category/明朝禮部左侍郎.md "wikilink")
[Category:明朝吏部左侍郎](../Category/明朝吏部左侍郎.md "wikilink")
[Category:明朝禮部尚書](../Category/明朝禮部尚書.md "wikilink")
[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")
[Category:明朝太子三師](../Category/明朝太子三師.md "wikilink")
[Category:明朝三公](../Category/明朝三公.md "wikilink")
[Category:興化人](../Category/興化人.md "wikilink")
[C](../Category/李姓.md "wikilink")
[Category:諡文定](../Category/諡文定.md "wikilink")

1.
2.
3.
4.  [清](../Page/清.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷193）：“李春芳，字子實，揚州興化人。嘉靖二十六年舉進士第一，除修撰。簡入西苑撰青詞，
    大被帝眷，與侍讀嚴訥超擢翰林學士。尋遷太常少卿，拜禮部右侍郎，俱兼學士，直西苑
    如故。佐理部事，進左侍郎，轉吏部，代訥為禮部尚書。時宗室蕃衍，歲祿苦不繼。春芳
    考故事，為書上之。諸吉凶大禮及歲時給賜，皆嚴為之制。帝嘉之，賜名宗藩條例。尋加太子太保。四十四年命兼武英殿大學士，與訥並參機務。世宗眷侍直諸臣厚，凡遷除皆出
    特旨。春芳自學士至柄政，凡六遷，未嘗一由廷推。”
5.  汪浚：《吳承恩與〈西遊記〉》一文最先猜測"華陽洞天主人"是李春芳。
6.  沈承庆：《话说吴承恩———〈西游记〉作者问题揭秘》，北京图书馆出版社
7.
8.  康熙《兴化县志》
9.  [清](../Page/清.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷193）：“齊康之劾徐階也，語侵春芳。春芳疏辨求去，帝慰留之。及代階為首輔，益務以安靜，
    稱帝意。時同列者陳以勤、張居正。以勤端謹，而居正恃才凌物，視春芳蔑如也。始階以
    人言罷，春芳歎曰：「徐公尚爾，我安能久，容旦夕乞身耳。」居正遽曰：「如此，庶保令名。」
    春芳愕然，三疏乞休，帝不允。既而趙貞吉入代以勤，剛而負氣。及高拱再入直，凌春芳
    出其上，春芳不能與爭，謹自飭而已。俺答款塞求封，春芳偕拱、居正即帝前決之。會貞吉
    為拱逐，拱益張，修階故怨。春芳嘗從容為階解，拱益不悅。時春芳已累加少師兼太子太
    師，進吏部尚書，改中極殿，度拱輩終不容己，兩疏請歸養，不允。南京給事中王禎希拱意，
    疏詆之，春芳求去益力。賜敕乘傳，遣官護行，有司給夫廩如故事。閱一歲，拱復為居正所
    擠，幾不免。而春芳歸，父母尚無恙，晨夕置酒食為樂，鄉里豔之。父母歿數年乃卒，年七十五。贈太師，諡文定。”