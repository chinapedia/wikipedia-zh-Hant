**李晓华**（），生于[北京](../Page/北京.md "wikilink")，中国富豪。[福布斯中国富豪榜中](../Page/福布斯中国富豪榜.md "wikilink")1998年和1999年第2名，2000年第11名，2001年第16名。[胡润百富榜中](../Page/胡润百富榜.md "wikilink")，1999年第4位，2003年第23位，2007年第328位。

## 生平

1968年下放到[黑龙江生产建设兵团](../Page/黑龙江生产建设兵团.md "wikilink")。1976年回北京当锅炉工人。

80年代初在[北戴河卖饮料](../Page/北戴河.md "wikilink")、开录像厅开始发迹。1985年到[日本通过代理](../Page/日本.md "wikilink")[章光101毛发再生精获得巨额财富](../Page/章光101毛发再生精.md "wikilink")。1988年在[香港创立](../Page/香港.md "wikilink")[华达国际投资集团公司任董事局主席](../Page/华达国际投资集团公司.md "wikilink")。

1994和1995年，被授予“全国百名优秀企业家”和“中国十佳优秀民营企业家”称号。1998年当选为[全国政协委员](../Page/全国政协委员.md "wikilink")\[1\]\[2\]

## 家庭

李晓华和[李晓林是兄弟](../Page/李晓林.md "wikilink")。

女儿李响

## 影响

1996年3月1日，由[中国科学院](../Page/中国科学院.md "wikilink")[紫金山天文台发现并提名](../Page/紫金山天文台.md "wikilink")，[国际小行星中心和](../Page/国际小行星中心.md "wikilink")[国际小行星命名委员会审议通过](../Page/国际小行星命名委员会.md "wikilink")，“鉴于李晓华为世界慈善事业作出的巨大贡献”，将一颗国际永久编号为3556号，将国际编号3556号小行星命名为“李晓华星”。

1992年李晓华成为中国第一个拥有[法拉利轿车的人](../Page/法拉利.md "wikilink")，车牌号是京A00001，他和那辆红色法拉利出现在天安门广场的照片曾经被悬挂在[法国](../Page/法国.md "wikilink")[巴黎机场和](../Page/巴黎机场.md "wikilink")[里昂广场](../Page/里昂广场.md "wikilink")，照片下面写着“一个来自红色资本主义的挑战者”。

2007年日本[NHK纪录片](../Page/NHK.md "wikilink")《[激流中国](../Page/激流中国.md "wikilink")·富人与农民工》中，李晓华作为主角之一，片中有他的生日宴会（出席者有全国政协副主席[周铁农等](../Page/周铁农.md "wikilink")）、价值数百万元的新轿车和数千万元的豪华别墅、操纵股市获利等情节。李晓华再一次引起世人关注。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中華人民共和國企業家](../Category/中華人民共和國企業家.md "wikilink")
[L](../Category/中華人民共和國億萬富豪.md "wikilink")
[L](../Category/第九屆全國政協委員.md "wikilink")
[L](../Category/北京人.md "wikilink") [X晓华](../Category/李姓.md "wikilink")

1.  [李晓华简历，搜狐财经频道](http://business.sohu.com/90/53/article212315390.shtml)
2.  [商界巨子李晓华，传媒青年网](http://youth.cuc.edu.cn/html/gzdt/192131161.htm)