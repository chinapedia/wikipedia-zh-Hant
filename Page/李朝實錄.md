[Joseon_Wangjo_Sillok_and_its_case_in_museum.jpg](https://zh.wikipedia.org/wiki/File:Joseon_Wangjo_Sillok_and_its_case_in_museum.jpg "fig:Joseon_Wangjo_Sillok_and_its_case_in_museum.jpg")

《**李朝实录**》，又稱《**朝鮮王朝實錄**》或《**朝鮮封建王朝實錄**》，是[李氏朝鲜](../Page/李氏朝鲜.md "wikilink")《[太祖大王实录](../Page/朝鮮太祖.md "wikilink")》到《[哲宗大王实录](../Page/朝鮮哲宗.md "wikilink")》的一个统编，是李氏朝鲜（又称“朝鮮王朝”）472年的历代[实录](../Page/实录.md "wikilink")。是第151號[韓國國寶](../Page/韓國國寶.md "wikilink")。1990年代開始，韓國基金會將其翻譯成現代韓文並進行[數位化](../Page/數位化.md "wikilink")\[1\]。1997年10月，《李朝实录》登錄為[世界紀錄遺產](../Page/世界紀錄遺產.md "wikilink")\[2\]\[3\]。韓國[國史編纂委員會在](../Page/國史編纂委員會.md "wikilink")1993年完成《朝鮮王朝實錄》翻譯成韓文，2006年提供原文（[漢文](../Page/漢文.md "wikilink")）和[韓文本的網路閱覽服務](../Page/韓文.md "wikilink")，並預計在2033年完成英譯本\[4\]。

## 概要

《李朝實錄》記載了由初代[太祖到](../Page/朝鮮太祖.md "wikilink")[哲宗的](../Page/朝鮮哲宗.md "wikilink")25代[国王](../Page/国王.md "wikilink")、472年（1392年－1863年）间[历史事实的年月日順](../Page/历史.md "wikilink")[编年体](../Page/编年体.md "wikilink")[汉文记录](../Page/汉文.md "wikilink")；若連最後兩任君主的紀錄也包括在內的話，那包含總共27代君主、519年（1392年－1910年），共1893-{卷}-，888冊，總共約6400萬字。

朝鮮王朝前期，《李朝實錄》正本存放於[漢陽](../Page/漢陽.md "wikilink")（今[首爾](../Page/首爾.md "wikilink")）[春秋馆](../Page/春秋馆.md "wikilink")，副本存放於[忠州](../Page/忠州.md "wikilink")、[星州](../Page/星州.md "wikilink")、[全州的史库](../Page/全州.md "wikilink")。[壬辰倭乱中](../Page/壬辰倭乱.md "wikilink")，除全州本之外，《李朝實錄》正本和其餘副本全部被毁。[宣祖三十六年](../Page/朝鮮宣祖.md "wikilink")，依据全州本重新编修，[印刷五部](../Page/印刷.md "wikilink")，分别存於春秋馆、[摩尼山史库](../Page/摩尼山.md "wikilink")、[太白山史库](../Page/太白山.md "wikilink")、[妙香山史库](../Page/妙香山.md "wikilink")、[五台山史库](../Page/韩国五台山.md "wikilink")。妙香山本後來移藏於[赤裳山](../Page/赤裳山.md "wikilink")，摩尼山本後來移藏於[鼎足山](../Page/鼎足山.md "wikilink")。1905年，春秋馆本移存於[奎章阁](../Page/奎章阁.md "wikilink")。

1910年[日韓合邦後](../Page/日韓合邦.md "wikilink")，奎章阁图书由日本[朝鮮总督府收集](../Page/朝鮮总督府.md "wikilink")。1911年，太白山本和鼎足山本也移交朝鲜总督府，赤裳山本收藏於[藏书阁](../Page/藏书阁.md "wikilink")，五台山本被“赠送”给[东京帝国大学](../Page/东京帝国大学.md "wikilink")。1923年[关东大地震](../Page/关东大地震.md "wikilink")，五台山本大部份被烧毁，剩餘74冊\[5\]。1930年，朝鲜总督府将太白山本和鼎足山本送给[京城帝国大学](../Page/京城帝国大学.md "wikilink")；[日本投降后](../Page/日本投降.md "wikilink")，移交韩国，存放於[首尔大学](../Page/首尔大学.md "wikilink")。[朝鲜战争期间](../Page/朝鲜战争.md "wikilink")，鼎足山本被[朝鲜人民军缴获](../Page/朝鲜人民军.md "wikilink")，移至[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")，现存放在[金日成综合大学](../Page/金日成综合大学.md "wikilink")；太白山本则被[韩国政府转移至](../Page/韩国政府.md "wikilink")[釜山](../Page/釜山.md "wikilink")，至今仍存放於韩国[國家記錄院轄下的](../Page/國家記錄院.md "wikilink")[歷史檔案館](../Page/歷史檔案館.md "wikilink")。

現存的《李朝實錄》有三個版本：

  - 鼎足山本有1181冊；
  - 太白山本有848冊；
  - 五台山本有27冊；

其他散逸本有21冊，合共2,077冊。

《李朝實錄》包括太祖、[定宗](../Page/朝鮮定宗.md "wikilink")、[太宗直至哲宗的](../Page/朝鮮太宗.md "wikilink")23部实录，以及[燕山君和](../Page/燕山君.md "wikilink")[光海君的兩部日记](../Page/光海君.md "wikilink")（体裁与实录相同）。日本朝鲜总督府曾编纂了《高宗实录》和《纯宗实录》，记载[高宗和](../Page/朝鮮高宗.md "wikilink")[純宗兩代所發生的事](../Page/朝鮮純宗.md "wikilink")，但不為[韓國及朝鮮民主主義人民共和國的](../Page/大韓民國.md "wikilink")[歷史學家所承認](../Page/歷史學家.md "wikilink")。在韩国，不包含这些最后二朝的实录。

《李朝实录》关于朝鮮王朝25代君主472年的[政治](../Page/政治.md "wikilink")、[外交](../Page/外交.md "wikilink")、[军事](../Page/军事.md "wikilink")、[经济方面的庞大](../Page/经济.md "wikilink")[史料](../Page/史料.md "wikilink")，被认为是研究[李氏朝鲜的基本史料](../Page/李氏朝鲜.md "wikilink")\[6\]。《李朝实录》中也包含不少有关[中国歷史](../Page/中国歷史.md "wikilink")、[女真歷史與](../Page/女真歷史.md "wikilink")[日本歷史的史料](../Page/日本歷史.md "wikilink")，中国历史學家[吴晗曾经编纂](../Page/吴晗.md "wikilink")《[朝鲜李朝实录中的中国史料](../Page/朝鲜李朝实录中的中国史料.md "wikilink")》一書\[7\]。

1997年10月，《李朝实录》被[联合国教科文组织登记为](../Page/联合国教科文组织.md "wikilink")[世界纪录遗产](../Page/世界纪录遗产.md "wikilink")（此為韓國用詞，[中國稱為](../Page/中國.md "wikilink")[世界记忆项目](../Page/世界记忆项目.md "wikilink")）\[8\]，亦被列入[韓國國寶第](../Page/韓國國寶.md "wikilink")151號。

## 目錄

<table>
<thead>
<tr class="header">
<th><p>順序</p></th>
<th><p>名稱</p></th>
<th><p>卷</p></th>
<th><p>冊</p></th>
<th><p>編撰</p></th>
<th><p>原名</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/朝鮮太祖.md" title="wikilink">太祖實錄</a><br />
（）</p></td>
<td><p>15</p></td>
<td><p>3</p></td>
<td><p>1413年<br />
（太宗13年）</p></td>
<td><p>太祖康獻大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/朝鮮定宗.md" title="wikilink">定宗實錄</a><br />
（）</p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>1426年<br />
（世宗8年）</p></td>
<td><p>恭靖王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/朝鮮太宗.md" title="wikilink">太宗實錄</a><br />
（）</p></td>
<td><p>36</p></td>
<td><p>16</p></td>
<td><p>1431年<br />
（世宗13年）</p></td>
<td><p>太宗恭定大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/朝鮮世宗.md" title="wikilink">世宗實錄</a><br />
（）</p></td>
<td><p>163</p></td>
<td><p>67</p></td>
<td><p>1454年<br />
（端宗2年）</p></td>
<td><p>世宗莊憲大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/朝鮮文宗.md" title="wikilink">文宗實錄</a><br />
（）</p></td>
<td><p>13</p></td>
<td><p>6</p></td>
<td><p>1455年<br />
（世祖元年）</p></td>
<td><p>文宗恭順大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/朝鮮端宗.md" title="wikilink">端宗實錄</a><br />
（）</p></td>
<td><p>14</p></td>
<td><p>6</p></td>
<td><p>1469年<br />
（睿宗元年）</p></td>
<td><p>魯山君日記<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/朝鮮世祖.md" title="wikilink">世祖實錄</a><br />
（）</p></td>
<td><p>49</p></td>
<td><p>18</p></td>
<td><p>1471年<br />
（成宗2年）</p></td>
<td><p>世祖惠莊大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/朝鮮睿宗.md" title="wikilink">睿宗實錄</a><br />
（）</p></td>
<td><p>8</p></td>
<td><p>3</p></td>
<td><p>1472年<br />
（成宗3年）</p></td>
<td><p>睿宗襄悼大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/朝鮮成宗.md" title="wikilink">成宗實錄</a><br />
（）</p></td>
<td><p>297</p></td>
<td><p>47</p></td>
<td><p>1499年<br />
（燕山君5年）</p></td>
<td><p>成宗康靖大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/燕山君.md" title="wikilink">燕山君日記</a><br />
（）</p></td>
<td><p>63</p></td>
<td><p>17</p></td>
<td><p>1509年<br />
（中宗4年）</p></td>
<td><p>燕山君日記<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/朝鮮中宗.md" title="wikilink">中宗實錄</a><br />
（）</p></td>
<td><p>105</p></td>
<td><p>53</p></td>
<td><p>1550年<br />
（明宗5年）</p></td>
<td><p>中宗恭僖徽文昭武欽仁誠孝大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/朝鮮仁宗.md" title="wikilink">仁宗實錄</a><br />
（）</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>1550年<br />
（明宗5年）</p></td>
<td><p>仁宗榮靖獻文懿武章肅欽孝大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/朝鮮明宗.md" title="wikilink">明宗實錄</a><br />
（）</p></td>
<td><p>34</p></td>
<td><p>21</p></td>
<td><p>1571年<br />
（宣祖4年）</p></td>
<td><p>明宗大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/朝鮮宣祖.md" title="wikilink">宣祖實錄</a><br />
（）</p></td>
<td><p>221</p></td>
<td><p>116</p></td>
<td><p>1616年<br />
（光海君8年）</p></td>
<td><p>宣祖昭敬大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>宣祖修訂實錄<br />
（）</p></td>
<td><p>42</p></td>
<td><p>8</p></td>
<td><p>1657年<br />
（孝宗8年）</p></td>
<td><p>宣祖昭敬大王修訂實錄<br />
（）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><a href="../Page/光海君.md" title="wikilink">光海君日記</a><br />
（）</p></td>
<td><p>187</p></td>
<td><p>64</p></td>
<td><p>1633年<br />
（仁祖11年）</p></td>
<td><p>光海君日記<br />
（）</p></td>
<td><p>重抄本（太白山本）（）</p></td>
</tr>
<tr class="odd">
<td><p>187</p></td>
<td><p>40</p></td>
<td><p>1653年<br />
（孝宗4年）</p></td>
<td><p>正抄本（鼎足山本）（）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/朝鮮仁祖.md" title="wikilink">仁祖實錄</a><br />
（）</p></td>
<td><p>50</p></td>
<td><p>50</p></td>
<td><p>1653年<br />
（孝宗4年）</p></td>
<td><p>仁祖大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/朝鮮孝宗.md" title="wikilink">孝宗實錄</a><br />
（）</p></td>
<td><p>21</p></td>
<td><p>22</p></td>
<td><p>1661年<br />
（顯宗2年）</p></td>
<td><p>孝宗大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/朝鮮顯宗.md" title="wikilink">顯宗實錄</a><br />
（）</p></td>
<td><p>22</p></td>
<td><p>23</p></td>
<td><p>1677年<br />
（肅宗3年）</p></td>
<td><p>顯宗純文肅武敬仁彰孝大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>顯宗改修實錄<br />
（）</p></td>
<td><p>28</p></td>
<td><p>29</p></td>
<td><p>1683年<br />
（肅宗9年）</p></td>
<td><p>顯宗純文肅武敬仁彰孝大王改修實錄<br />
（）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p><a href="../Page/朝鮮肅宗.md" title="wikilink">肅宗實錄</a><br />
（</p></td>
<td><p>65</p></td>
<td><p>73</p></td>
<td><p>1728年<br />
（英祖4年）</p></td>
<td><p>肅宗顯義…大王實錄<br />
（）</p></td>
<td><p>另有《肅宗實錄補闕正誤》（）</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p><a href="../Page/朝鮮景宗.md" title="wikilink">景宗實錄</a><br />
（）</p></td>
<td><p>15</p></td>
<td><p>7</p></td>
<td><p>1732年<br />
（英祖8年）</p></td>
<td><p>景宗德文翼武純仁宣孝大王實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>景宗修訂實錄<br />
（）</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>1781年<br />
（正祖5年）</p></td>
<td><p>景宗德文翼武純仁宣孝大王修訂實錄<br />
（）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/朝鮮英祖.md" title="wikilink">英祖實錄</a><br />
（）</p></td>
<td><p>127</p></td>
<td><p>83</p></td>
<td><p>1781年<br />
正祖5年</p></td>
<td><p>英宗至行…顯孝大王實錄<br />
（）</p></td>
<td><p>原名〈英宗實錄〉，追尊後改名〈英祖實錄〉。</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/朝鮮正祖.md" title="wikilink">正祖實錄</a><br />
（）</p></td>
<td><p>54</p></td>
<td><p>56</p></td>
<td><p>1805年<br />
純祖5年</p></td>
<td><p>正宗文成武烈聖仁莊孝大王實錄<br />
（）</p></td>
<td><p>原名〈正宗實錄〉，追尊後改名〈正祖實錄〉。</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><a href="../Page/朝鮮純祖.md" title="wikilink">純祖實錄</a><br />
（）</p></td>
<td><p>34</p></td>
<td><p>36</p></td>
<td><p>1838年<br />
憲宗4年</p></td>
<td><p>純宗宣恪…成孝大王實錄<br />
（）</p></td>
<td><p>原名〈純宗實錄〉，追尊後改名〈純祖實錄〉。</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p><a href="../Page/朝鮮憲宗.md" title="wikilink">憲宗實錄</a><br />
（）</p></td>
<td><p>16</p></td>
<td><p>9</p></td>
<td><p>1851年<br />
哲宗2年</p></td>
<td><p>憲宗經文緯武明仁哲孝大王實錄<br />
（）</p></td>
<td><p><a href="../Page/大韓帝國.md" title="wikilink">大韓帝國成立後</a>，又再改諡號，省略。見<a href="../Page/朝鮮憲宗.md" title="wikilink">憲宗條目</a>。</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p><a href="../Page/朝鮮哲宗.md" title="wikilink">哲宗實錄</a><br />
（）</p></td>
<td><p>15</p></td>
<td><p>9</p></td>
<td><p>1865年<br />
朝鮮高宗2年</p></td>
<td><p>哲宗熙倫…英孝大王實錄<br />
（）</p></td>
<td><p>大韓帝國成立後，又再改諡號，省略。見<a href="../Page/朝鮮哲宗.md" title="wikilink">哲宗條目</a>。</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p><a href="../Page/朝鮮高宗.md" title="wikilink">高宗實錄</a><br />
（）</p></td>
<td><p>52</p></td>
<td><p>52</p></td>
<td><p>1934年</p></td>
<td><p>高宗純天…太皇帝實錄<br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p><a href="../Page/朝鮮純宗.md" title="wikilink">純宗實錄</a><br />
（）</p></td>
<td><p>22</p></td>
<td><p>8</p></td>
<td><p>1934年</p></td>
<td><p>純宗文溫…孝皇帝實錄<br />
（）</p></td>
<td></td>
</tr>
</tbody>
</table>

## 出版

1953年，《李朝实录》由[日本学习院东洋文化研究所影印出版](../Page/日本学习院.md "wikilink")，共五十册，该版《李朝实录》未包含〈高宗实录〉和〈纯宗实录〉。

1959年，《李朝实录》中的〈高宗实录〉和〈纯宗实录〉由[中国科学院和](../Page/中国科学院.md "wikilink")[朝鲜科学院合作根据](../Page/朝鲜科学院.md "wikilink")“[金柜秘本](../Page/金柜秘本.md "wikilink")”影印出版。
朝鲜文版《李朝实录》由[朝鲜社会科学院民族古典研究所翻译出版](../Page/朝鲜社会科学院.md "wikilink")。

## 女真族問題

在《李朝實錄》對外國或外族的記錄中，最值得一提的是，對女真族的記載最為詳細，比起《明實錄》或中國其他史書對女真族的記載還要完整。《李朝實錄》可說是一般研究女真歷史文化的重要史料。

## [倭寇問題](../Page/倭寇.md "wikilink")

在〈世宗實錄〉中，敘述「前朝之際，[倭寇興行](../Page/倭寇.md "wikilink")，民不聊生。然倭人不過一二，而本国之民，反著倭服，成党作乱。」〈太宗實錄〉又寫「或時，反倭為寇。」紀錄寇邊的[倭寇](../Page/倭寇.md "wikilink")，其組成實以朝鮮人為主。

## 註釋

## 参考文献

## 外部連結

  -
  -
  -
[Category:實錄](../Category/實錄.md "wikilink")
[Category:朝鲜王朝官修典籍](../Category/朝鲜王朝官修典籍.md "wikilink")
[Category:朝鮮漢文典籍](../Category/朝鮮漢文典籍.md "wikilink")
[Category:朝鮮古代史書](../Category/朝鮮古代史書.md "wikilink")
[Category:世界记忆名录](../Category/世界记忆名录.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.  『조선왕조실록, 보존을 위한 기초 조사연구(1)』(송기중 신병주 박지선 이인성, 서울대학교출판부)