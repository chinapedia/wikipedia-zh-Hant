**李棟旭**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")，在[東亞地區名字常被錯寫為](../Page/東亞地區.md "wikilink")**李東旭**。

韓國男演員「李棟旭」從18歲就開始參與戲劇演出，出道近20年來的戲劇人生擁有不少經典作品，包括耳熟能詳的2005的《[我的女孩](../Page/我的女孩.md "wikilink")》、2011年《[女人的香氣](../Page/女人的香氣.md "wikilink")》，以及2016年底的《[孤單又燦爛的神－鬼怪](../Page/孤單又燦爛的神－鬼怪.md "wikilink")》。李棟旭在《鬼怪》劇中飾演的地獄使者一角，當年造成全亞洲轟動，劇中與劉仁娜演出虐心情侶的癡情形象，讓許多影迷因此更為他癡狂，李棟旭因為《鬼怪》成為新一代國民老公，而堪稱地表最帥的地獄使者近期再度與劉仁娜合作最新作品－《[觸及真心](../Page/觸及真心.md "wikilink")》。

## 個人生活簡介

  - 1999年因為參加模特兒大賽出道。
  - 18 歲出道的李棟旭，既是演員、模特兒也身兼主持人，在短短 7 年中就參與了 16
    部電視劇演出，不論角色大小他都十分珍惜，也因此累積了許多演出經驗，直到
    2005 年終於有了代表作。他和李多海主演的《[我的女孩 My
    Girl](../Page/我的女孩_My_Girl.md "wikilink")》大受好評，當年甚至被票選為最佳螢幕情侶！
  - 2009年8月24日正式入伍，在忠清南道的陸軍訓練所服兵役兩年，在入伍前最後幾天，完成新電影《大醬》的拍攝，並於2009年底上映。他選擇在人氣正旺的時期入伍，離開了螢幕一段時間。
  - 2011年6月20日正式結束兵役生活後便立即投入復出作品《[女人的香氣](../Page/女人的香氣.md "wikilink")》拍攝，並於退伍一個月後首播。戲裡的眼淚演技也讓李棟旭因為這部劇人氣再次衝上新高，他也獲得
    2011 年 SBS 演技大賞週末劇部門的最佳男主角獎！
  - 2012
    年李棟旭首度挑戰擔當主持人，與申東燁搭檔主持綜藝節目《[強心臟](../Page/強心臟.md "wikilink")》。因為是新手，剛開始主持時常有一些呆萌反應，也為節目增添了笑料，看似木訥的他還在當年的
    SBS 演藝大賞榮獲新人 MC 獎的認可呢！
  - 2014年4月5日《[Hotel
    King](../Page/Hotel_King.md "wikilink")》是[韓國](../Page/韓國.md "wikilink")[MBC製作](../Page/文化廣播_\(韓國\).md "wikilink")，在《酒店之王》飾演飯店總經理車載莞，這部劇也是李棟旭與李多海繼《My
    Girl》後時隔8年再度搭檔主演。
  - 2014年5月4日《[Roommate](../Page/Roommate.md "wikilink")》是韓國[SBS電視台的一檔真人實境綜藝節目](../Page/SBS株式會社.md "wikilink")。
  - 2014年2014年9月10日出演《[鋼鐵人](../Page/鋼鐵人.md "wikilink")》為[韓國](../Page/韓國.md "wikilink")[KBS](../Page/韓國放送公社.md "wikilink")，講述內心的憤怒和傷痛會化成刀刃從身體爆發出來的男子（[李棟旭飾演](../Page/李棟旭.md "wikilink")）與能夠包容他的女人（[申世景飾演](../Page/申世景.md "wikilink")）之間的愛情故事。
  - 2016年出演《孤單又燦爛的神－鬼怪》，他說看過了《鬼怪》的劇本後被吸引，主動表示就算只是男配角也願意演出，用精采的演技再次吸引了觀眾目光。2016年因為《鬼怪》在第五屆DramaFever
    Awards獲得最佳男配角賞。
  - 2018年在13th Soompi Awards獲得最佳男配角賞。
  - 2018年3月9日公開與[裴秀智交往中](../Page/裴秀智.md "wikilink")，二人在2018年7月2日證實分手\[1\]。

## 影视作品

### 電視劇

|                                      |                                                    |                                              |     |     |
| ------------------------------------ | -------------------------------------------------- | -------------------------------------------- | --- | --- |
| 年份                                   | 電視台                                                | 劇名                                           | 角色  | 備註  |
| 1999年                                | [MBC](../Page/文化廣播_\(韓國\).md "wikilink")           | 《BEST劇場－在路之外還有世界》                            | 李瑞俊 |     |
| 2000年                                | 《[秘密](../Page/秘密_\(2000年電視劇\).md "wikilink")》      | 姜鉉秀                                          |     |     |
| [KBS](../Page/韓國放送公社.md "wikilink")  | 《[湖水](../Page/湖水_\(韓國電視劇\).md "wikilink")》         |                                              |     |     |
| 《[學校2](../Page/學校2.md "wikilink")》   | 李江山                                                |                                              |     |     |
| 2001年                                | 《[學校3](../Page/學校3.md "wikilink")》                 |                                              |     |     |
| 《[純情](../Page/純情.md "wikilink")》     | 張鎬九                                                |                                              |     |     |
| 《Drama City－快樂天堂》                    |                                                    |                                              |     |     |
| 《[小氣家族](../Page/小氣家族.md "wikilink")》 | 才賢                                                 |                                              |     |     |
| [SBS](../Page/SBS有限公司.md "wikilink") | 《[剛剛好](../Page/剛剛好.md "wikilink")》                 | 李棟旭                                          | 客串  |     |
| 2002年                                | KBS                                                | 《[人魚公主](../Page/人魚公主_\(電視劇\).md "wikilink")》 | 李珉  |     |
| SBS                                  | 《[Let's go](../Page/Let's_go.md "wikilink")》       | 李棟旭                                          | 客串  |     |
| 2003年                                | 《[認真生活](../Page/認真生活.md "wikilink")》               |                                              |     |     |
| MBC                                  | 《[迴轉木馬](../Page/迴轉木馬_\(電視劇\).md "wikilink")》       | 朴成彪                                          |     |     |
| SBS                                  | 《[酒國](../Page/酒國_\(電視劇\).md "wikilink")》           | 宋道一                                          |     |     |
| 2004年                                | 《[小島老師](../Page/小島老師.md "wikilink")》               | 張哲圖                                          |     |     |
| 2005年                                | KBS                                                | 《[致父親母親](../Page/致父親母親.md "wikilink")》       | 安政桓 |     |
| SBS                                  | 《中秋節特別單元劇－河內新娘》                                    | 朴恩宇                                          |     |     |
| 《[我的女孩](../Page/我的女孩.md "wikilink")》 | 薛功燦                                                |                                              |     |     |
| 2008年                                | MBC                                                | 《[甜蜜的人生](../Page/甜蜜的人生.md "wikilink")》       | 李俊洙 |     |
| 2009年                                | KBS                                                | 《-{[夥伴們](../Page/夥伴們.md "wikilink")}-》       | 李泰祖 |     |
| 2011年                                | SBS                                                | 《[女人的香氣](../Page/女人的香氣.md "wikilink")》       | 姜志旭 |     |
| 2012年                                | KBS                                                | 《[暴力羅曼史](../Page/暴力羅曼史.md "wikilink")》       | 朴茂烈 |     |
| 2013年                                | 《[天命：朝鮮版逃亡者故事](../Page/天命：朝鮮版逃亡者故事.md "wikilink")》 | 崔元                                           |     |     |
| 2014年                                | SBS                                                | 《[江口故事](../Page/江口故事.md "wikilink")》         | 金景泰 |     |
| MBC                                  | 《[Hotel King](../Page/Hotel_King.md "wikilink")》   | 車載皖                                          |     |     |
| KBS                                  | 《[鋼鐵人](../Page/鋼鐵人_\(電視劇\).md "wikilink")》         | 朱弘斌                                          |     |     |
| 2015年                                | [tvN](../Page/TVN.md "wikilink")                   | 《[泡泡糖](../Page/泡泡糖_\(電視劇\).md "wikilink")》   | 朴利煥 |     |
| 2016年                                | 《[孤單又燦爛的神－鬼怪](../Page/孤單又燦爛的神－鬼怪.md "wikilink")》   | 王黎（陰間使者）                                     |     |     |
| 2017年                                | [OnStyle](../Page/OnStyle.md "wikilink")           | 《[Love is](../Page/Love_is.md "wikilink")》   |     | 迷你劇 |
| 2018年                                | [JTBC](../Page/JTBC.md "wikilink")                 | 《[Life](../Page/Life_\(電視劇\).md "wikilink")》 | 芮鎮宇 |     |
| 2019年                                | tvN                                                | 《[觸及真心](../Page/觸及真心.md "wikilink")》         | 權正祿 |     |

### 電影

  - 2006年：《[鬼魅兒](../Page/鬼魅兒.md "wikilink")》飾演 賢基
  - 2007年：《[最強羅曼史](../Page/最強羅曼史.md "wikilink")》飾演 姜在赫
  - 2008年：《[那男人的書第198頁](../Page/那男人的書第198頁.md "wikilink")》飾演 金俊吾
  - 2009年：《大醬》
  - 2015年：《[愛上變身情人](../Page/愛上變身情人.md "wikilink")》飾演 金禹鎮

### MV

  - 2005年：[Monday Kiz](../Page/Monday_Kiz.md "wikilink")《Bye Bye
    Bye》（與[張新英](../Page/張新英.md "wikilink")）
  - 2006年：李在勳《愛著》
  - 2006年：Suho《春夏秋冬》
  - 2007年：《在愛的森林裡迷失方向》（與[史熙](../Page/史熙.md "wikilink")）
  - 2009年：Suho（feat. [金範洙](../Page/金範洙.md "wikilink")）《未接電話》
  - 2010年：ZoZo（feat.  of ）《Only You》
  - 2011年：《Replay》（與[黃承言](../Page/黃承言.md "wikilink")）
  - 2013年：[金泰宇](../Page/金泰宇.md "wikilink")《Cosmic Girl》
  - 2017年：[韶宥](../Page/韶宥.md "wikilink") X
    [成始璄](../Page/成始璄.md "wikilink")《顯而易見的離別》

## 主持

  - 2012年：SBS《[演技大賞](../Page/SBS演技大賞.md "wikilink")》（與[鄭麗媛](../Page/鄭麗媛.md "wikilink")）
  - 2012年4月10日－2013年2月12日：SBS《[強心臟](../Page/強心臟.md "wikilink")》
  - 2015年：《[首爾國際電視節](../Page/首爾國際電視節.md "wikilink")》（與[金諪恩](../Page/金諪恩.md "wikilink")）
  - 2016年4月28日－2016年6月30日：[On Style](../Page/On_Style.md "wikilink")《My
    Bodyguard》（與[趙胤熙](../Page/趙胤熙.md "wikilink")）
  - 2019年：[Mnet](../Page/Mnet.md "wikilink")《[ProduceX101](../Page/ProduceX101.md "wikilink")》（國民製作人代表）

## 綜藝節目

  - 2011：SBS《[Healing Camp](../Page/Healing_Camp.md "wikilink")》（第11集）
  - 2011：tvN《[現場脫口秀Taxi](../Page/現場脫口秀Taxi.md "wikilink")》（第220集）
  - 2013：SBS《[星期天真好](../Page/星期天真好.md "wikilink")－[Running
    Man](../Page/Running_Man.md "wikilink")》（第133、134、136集）
  - 2013：MBC《[無限挑戰](../Page/無限挑戰.md "wikilink")》（介·寂·朋派對）
  - 2013：tvN《現場脫口秀Taxi》（第313集）
  - 2013：KBS《[-{Happy Together 3}-](../Page/歡樂在一起.md "wikilink")》（第295集）
  - 2014：SBS《星期天真好－Running Man》（第179、180集）
  - 2014－2015：SBS《星期天真好－[Roommate](../Page/Roommate.md "wikilink")》（第一、二季固定成員）
  - 2015：SBS《星期天真好－Running Man》（第263集）
  - 2016：KBS《[超人回來了](../Page/超人歸來_\(韓國綜藝\).md "wikilink")》（第126、127、137、138集）
  - 2017：KBS《[超人回來了](../Page/超人歸來_\(韓國綜藝\).md "wikilink")》（第207集）
  - 2018：MBC《[無限挑戰](../Page/無限挑戰.md "wikilink")》（第552集）

## 獲獎

  - 1999年：V-NESS選拔大會大獎
  - 2002年：IYV韓國最佳人氣演藝人大賞受獎式－新人賞
  - 2003年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－新星獎
  - 2011年：第18屆韓日文化大賞－文化外交部門 大賞
  - 2011年：SBS演技大賞－十大明星獎、周末連續劇部門最優秀演技獎《[女人的香氣](../Page/女人的香氣.md "wikilink")》
  - 2012年：[SBS演藝大賞](../Page/SBS演藝大賞.md "wikilink")－新人MC獎、最佳搭檔獎（與[申東燁](../Page/申東燁.md "wikilink")）《強心臟》
  - 2017年：第5屆－最佳男配角獎《[孤單又燦爛的神－鬼怪](../Page/孤單又燦爛的神－鬼怪.md "wikilink")》

## 代言

  - 2000年\~2010年：[農心拉麵](../Page/農心.md "wikilink")、男士古龍香水、ZIPPY
    ZIGGY服飾、寶令製藥、浪漫季節服飾、LITMUS服飾、JASPER服飾
  - 2011年：SPECIAL FORCES 特別服飾
  - 2011\~2012年：GREENJOY服飾
  - 2012年：DONG WON午餐肉、CASS LIGHT
    啤酒、[LOCASTE優閑服](../Page/拉科斯特_\(品牌\).md "wikilink")、[CHEVROLET汽車](../Page/雪佛蘭.md "wikilink")、ANYMODE-COVER
    WITH STAR電話套
  - 2012\~2013年：ECHOROBA登山服
  - 2013年：[Samsung](../Page/Samsung.md "wikilink") GALAXY NOTE 2
  - 2013\~2014年：DOWNY衣物柔順劑
  - 2014年：MIND BRIDGE服飾、AIGLE戶外服飾
  - 2015年：PAT服飾
  - 2016-2017年：Oliver Sweeney奧利弗·斯威尼服飾、LIZK-FIRST C PURE VITAMIN C護膚品
  - 2017年：KB國民Card信用卡、Al volo食品、JIMMY CHOO MAN ICE男仕香水、Post
    'CornFlake'穀物
  - 2017-2018年：[正官莊高麗蔘精保健飲料](../Page/正官莊.md "wikilink")、Coin one電子錢
  - 2018年：Bruno Baffi服飾、Zigband房地產應用軟體科技
  - 2018年：代言CHANEL香奈兒男士化妝品「Boy de Chanel」
  - 2019年：四月代言手游《降臨》

## 其他

  - 2006年：與[李多海被韓國網友票選為最佳銀幕情侶](../Page/李多海.md "wikilink")（[My
    Girl](../Page/我的女孩.md "wikilink")）
  - 2007年：泰國當地網友評選為最受歡迎韓星第一名

## 參考資料

## 外部連結

  - [官方網頁](http://www.leedongwook.co.kr/)

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EC%9D%B4%EB%8F%99%EC%9A%B1&sm=tab_txc&ie=utf8&key=PeopleService&os=94756)


  - [李棟旭me2day](https://web.archive.org/web/20110728071623/http://me2day.net/actor_wook)

  -
  -
  -
[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國男演員.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L](../Category/韓國新教徒.md "wikilink")
[L](../Category/世宗大學校友.md "wikilink")
[L](../Category/李姓.md "wikilink")

1.