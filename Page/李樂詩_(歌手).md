**李樂詩**（，\[1\]），香港女歌手，[祖籍](../Page/祖籍.md "wikilink")[廣東](../Page/廣東.md "wikilink")，生於加拿大[愛民頓](../Page/愛民頓.md "wikilink")，畢業於[加拿大](../Page/加拿大.md "wikilink")[多倫多](../Page/多倫多.md "wikilink")[皇家音樂學院](../Page/皇家音樂學院.md "wikilink")。
此外，她擁有多年[芭蕾舞及](../Page/芭蕾舞.md "wikilink")[爵士舞經驗](../Page/爵士舞.md "wikilink")。

## 個人履歷

### 早年生活

1973年，李乐诗在加拿大出生，她自小已經對音樂情有獨鍾，5歲開始學彈鋼琴彈到十級，連大學時期都在[加拿大](../Page/加拿大.md "wikilink")[多倫多](../Page/多倫多.md "wikilink")[皇家音樂學院主修音樂](../Page/皇家音樂學院_\(多倫多\).md "wikilink")。1992年，在一次機緣巧合之下，於香港演藝學院劇院參與音樂劇《萬世巨星》演出，並飾演女主角Mary
Magdalene，之後得香港[嘉音唱片賞識](../Page/嘉音唱片.md "wikilink")，並與唱片公司簽約。

### 入行初期（1992－1993年）

1992年，Joyce在[嘉音唱片推出首張個人專輯](../Page/嘉音唱片.md "wikilink")《李樂詩》，第一主打歌為《Love
is Forever》，此歌更令到她拿到新城電台FM Select
最佳新人獎、商業電台提名全年最佳新人獎。但這張專輯較為人熟識的歌有《無伴了》、《風流》、《入夜》，以及與[倫永亮合唱的](../Page/倫永亮.md "wikilink")《你是誰》。

1993年，推出第二張個人專輯《Beloved》，其中主打歌《不知今夜如何過》和黃凱芹的《危情》是同曲改編的，而《唯有今夜》以及其英文版《Back
Again》獲得香港[CASH流行曲創作大賽前十名](../Page/CASH流行曲創作大賽.md "wikilink")。之後由於合約問題，轉投[飛圖唱片](../Page/飛圖唱片.md "wikilink")。

### 走紅期（1994－1995年）

1994年，Joyce在[飛圖唱片推出第三張個人專輯](../Page/飛圖唱片.md "wikilink")《感動你》，成績突飛猛進，碟中部份歌曲均出自Joyce的手筆，這張專輯較為人熟識的歌有《總有一天感動你》、《逐漸逐段在夢裡消失去》和《今年流行什麼》。其中《**總有一天感動你**》穩據各流行榜的首位達數週之久，讓她的歌唱事業更上一層樓，並讓她榮獲香港[無線電視](../Page/無線電視.md "wikilink")[十大勁歌金曲頒獎典禮頒發](../Page/十大勁歌金曲頒獎典禮.md "wikilink")「**十大金曲獎**」、香港[CASH及香港電台頒發](../Page/CASH.md "wikilink")「**最佳原創歌曲獎**」，及香港商業電台全年全港播放率最高五首歌曲之一。《感動你》專輯獲得白金唱片銷量。此外，她又奪得[十大勁歌金曲頒獎典禮的最受歡迎唱作歌手銅獎](../Page/十大勁歌金曲頒獎典禮.md "wikilink")、[新城電台金曲台金心情歌頒獎禮的金心卓越進步獎鑽石大獎](../Page/新城電台.md "wikilink")。

1995年，推出第四張個人專輯《吾愛被愛》，這張專輯和《感動你》一樣部份歌曲均出自Joyce的手筆，較為人熟識的歌有《吾愛被愛》、《無悔愛你一生》及[劉德華填詞的](../Page/劉德華.md "wikilink")《夢醒好嗎》。其中《無悔愛你一生》為香港無線電視劇集《[真情](../Page/真情.md "wikilink")》主題曲，此歌當年頗為街知巷聞。

### 下跌期（1996－1997年）

1996年，在幾乎亳無宣傳的情況下，無聲無色地推出了在[飛圖唱片的最後一張個人專輯](../Page/飛圖唱片.md "wikilink")《放鬆》，主打歌為《放鬆》、《早餐》及與李克勤對唱的《情意萬縷》；由於當時[飛圖唱片面臨人事改組](../Page/飛圖唱片.md "wikilink")，導致此專輯銷量不佳。隨後於同年中轉投[正東唱片](../Page/正東唱片.md "wikilink")，並推出第六張個人專輯《真情細說》，《真情細說》為《無悔愛你一生》的重新編曲版（無線電視劇集《[真情](../Page/真情.md "wikilink")》主題曲），可惜專輯反應不大、宣傳亦較弱。此後，正東只安排她灌錄單曲，未有再為她推出專輯。

1997年，聯同[蘇永康](../Page/蘇永康.md "wikilink")、[陳潔儀](../Page/陳潔儀_\(新加坡\).md "wikilink")、[雷頌德與](../Page/雷頌德.md "wikilink")[香港管弦樂團合作舉辦](../Page/香港管弦樂團.md "wikilink")「港樂四星。完美的聲空」演唱會，獲得了廣泛的好評，並在美國大西洋城演出。同年與[蘇永康在](../Page/蘇永康.md "wikilink")[馬來西亞舉行演唱會](../Page/馬來西亞.md "wikilink")。

### 衰落期（1998－2004年）

1998年，在[美國](../Page/美國.md "wikilink")[內華達州舉行](../Page/雷諾_\(內華達州\).md "wikilink")《李樂詩真情演唱會》，獲得了廣泛的好評。
其後，由於合約問題，正東唱片特意將她雪藏，令她沉寂了一段時間。在這段期間，只推出過無線電視劇集《[緣來沒法擋](../Page/緣來沒法擋.md "wikilink")》主題曲《有你有轉機》、無線電視劇集《[苗翠花](../Page/苗翠花.md "wikilink")》主題曲《小小女子半邊天》，以及《抱緊你》、《你如果明白我》等單曲，1998年，與正東唱片解約。

此後，她幾乎絕跡香港樂壇，這段期間她除了創作歌曲及擔任歌唱老師之外，也曾在世界各地登台演唱（包括多倫多、大西洋城、中國大陸等）。1999年在[香港電台電視部擔任電視節目](../Page/香港電台.md "wikilink")《英語遊縱》主持人，2000年於香港大會堂參與歌劇《國王與我》演出並飾演Tuptim一角。2002年擔任黃凱芹《好久不見演唱會》的表演嘉賓，亦曾為黃凱芹復出後首支單曲《好久不見》參與MV的演出。

### 復出樂壇（2005年至今）

2005年5月，Joyce宣佈復出樂壇，並於同年7月自資推出第七張專輯《Fantasy》，由[維高文化代理發行](../Page/維高文化.md "wikilink")。碟內所有歌曲以古巴拉丁音樂為主，有6首Joyce創作的新歌及5首翻唱歌，其中主打歌《Fantasy》是將[譚詠麟的成名作](../Page/譚詠麟.md "wikilink")《雨夜的浪漫》加上一些新歌詞，以拉丁音樂方式重新演繹。同年8月於香港理工大學綜藝館舉行一連兩場《李樂詩fantasy演唱會》。

2006年，李樂詩加入[Different
Music](../Page/Different_Music.md "wikilink")，推出國語單曲《依賴他的》及《思憶》，本計劃收錄於9月推出的首張國語專輯\[2\]，但後來不了了之。

2008年6月29日，李樂詩於[亞洲電視節目](../Page/亞洲電視.md "wikilink")《[樂壇風雲50年](../Page/樂壇風雲50年.md "wikilink")》舉行婚禮，並推出單曲《原來愛》。

2009年10月9日凌晨，李樂詩於[跑馬地](../Page/跑馬地.md "wikilink")[港安醫院](../Page/港安醫院.md "wikilink")，順產誕下6磅8安士的長子李然熙（Maxton）。

2012年8月14日，李樂詩於[跑馬地](../Page/跑馬地.md "wikilink")[港安醫院](../Page/港安醫院.md "wikilink")，順產再誕下7磅半二子李然正（Aleksander）。

2014年2月12日，李樂詩與[王馨平](../Page/王馨平.md "wikilink")、[李幸倪合作舉行慈善音樂會](../Page/李幸倪.md "wikilink")——《「盛載愛」音樂會》\[3\]，收益撥歸[香港傷健協會](../Page/香港傷健協會.md "wikilink")\[4\]

2016年5月14日，李樂詩透過[微博及](../Page/微博.md "wikilink")[instagram宣布再度懷孕](../Page/instagram.md "wikilink")，同年10月29日，在[明德國際醫院產下孻子李然隆](../Page/明德國際醫院.md "wikilink")（Theodore）。

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/李樂詩_(專輯).md" title="wikilink">李樂詩</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1992年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/Beloved_(專輯).md" title="wikilink">Beloved</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1993年5月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/感動你.md" title="wikilink">感動你</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1994年10月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4</p></td>
<td style="text-align: left;"><p><a href="../Page/吾愛被愛.md" title="wikilink">吾愛被愛</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1995年7月13日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5</p></td>
<td style="text-align: left;"><p><a href="../Page/最感動你原創精選.md" title="wikilink">最感動你原創精選</a></p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1995年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>6</p></td>
<td style="text-align: left;"><p><a href="../Page/放鬆.md" title="wikilink">放鬆</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1996年5月10日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>7</p></td>
<td style="text-align: left;"><p><a href="../Page/Joyce_best_17.md" title="wikilink">Joyce best 17</a></p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1996年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>8</p></td>
<td style="text-align: left;"><p><a href="../Page/真情細說.md" title="wikilink">真情細說</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/正東唱片.md" title="wikilink">正東唱片</a></p></td>
<td style="text-align: left;"><p>1996年12月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>9</p></td>
<td style="text-align: left;"><p><a href="../Page/Fantasy_(李樂詩專輯).md" title="wikilink">Fantasy</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/Joyce_Artist.md" title="wikilink">Joyce Artist</a><br />
<a href="../Page/大國文化.md" title="wikilink">大國文化</a></p></td>
<td style="text-align: left;"><p>2005年7月7日</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 現場錄音專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/港樂四星完美的聲空.md" title="wikilink">港樂四星完美的聲空</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/正東唱片.md" title="wikilink">正東唱片</a></p></td>
<td style="text-align: left;"><p>1998年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/拉闊音樂－范曉萱＋李樂詩＋陳潔儀.md" title="wikilink">拉闊音樂<br />
范曉萱＋李樂詩＋陳潔儀</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/正東唱片.md" title="wikilink">正東唱片</a></p></td>
<td style="text-align: left;"><p>1998年</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 單曲

  - 1997年：《抱緊你》([海馬牌廣告主題曲](../Page/海馬牌.md "wikilink"))（收錄於《大製作 (群星)》合輯》）
  - 1998年：《有你有轉機》（無綫電視劇集《[緣來沒法檔](../Page/緣來沒法檔.md "wikilink")》主題曲）（收錄於《大製作
    (群星)》合輯》）
  - 1998年：《你如果明白我》（收錄於《[拉闊音樂－范曉萱＋李樂詩＋陳潔儀](../Page/拉闊音樂－范曉萱＋李樂詩＋陳潔儀.md "wikilink")》現場錄音專輯》）
  - 2002年：《半首歌》(收錄於[賈思樂](../Page/賈思樂.md "wikilink")《My Love For
    Music》專輯)

### 未出版歌曲

  - 1997年：《Hello\! Hello\!》
  - 1998年：《情深天地》
  - 1998年：《小小女子半邊天》（無綫電視劇集《[苗翠花](../Page/苗翠花.md "wikilink")》主題曲）
  - 1998年：《天涯路》（無綫電視劇集《[苗翠花](../Page/苗翠花.md "wikilink")》插曲
  - 2006年：《依賴他的》
  - 2006年：《思憶》
  - 2008年：《原來愛》

### 非個人作品列表

  - 1997年：[沈芳如](../Page/沈芳如.md "wikilink") - 愛人（曲）
  - 2000年：[劉虹嬅](../Page/劉虹嬅.md "wikilink") - 時間滴答滴（曲）

## 非個人作品列表

### 1997年

  - [沈芳如](../Page/沈芳如.md "wikilink") - 愛人（曲）

### 2000年

  - [劉虹嬅](../Page/劉虹嬅.md "wikilink") - 時間滴答滴（曲）

## 派台歌曲成績

| **派台歌曲四台上榜最高位置**                                                                  |
| --------------------------------------------------------------------------------- |
| 唱片                                                                                |
| **1992年**                                                                         |
| [李樂詩](../Page/李樂詩_\(專輯\).md "wikilink")                                           |
| 李樂詩                                                                               |
| 李樂詩                                                                               |
| 李樂詩                                                                               |
| **1993年**                                                                         |
| [Beloved](../Page/Beloved_\(專輯\).md "wikilink")                                   |
| Beloved                                                                           |
| Beloved                                                                           |
| Beloved                                                                           |
| **1994年**                                                                         |
| [感動你](../Page/感動你.md "wikilink")                                                  |
| 感動你                                                                               |
| 感動你                                                                               |
| 感動你                                                                               |
| 感動你                                                                               |
| **1995年**                                                                         |
| [吾愛被愛](../Page/吾愛被愛.md "wikilink")                                                |
| 吾愛被愛                                                                              |
| 吾愛被愛                                                                              |
| 吾愛被愛                                                                              |
| 吾愛被愛                                                                              |
| **1996年**                                                                         |
| [放鬆](../Page/放鬆.md "wikilink") / [Hackenation](../Page/Hackenation.md "wikilink") |
| 放鬆                                                                                |
| 放鬆                                                                                |
| 放鬆                                                                                |
| [真情細說](../Page/真情細說.md "wikilink")                                                |
| 真情細說                                                                              |
| 真情細說                                                                              |
| **1997年**                                                                         |
| 大製作 (群星)                                                                          |
|                                                                                   |
| **1998年**                                                                         |
|                                                                                   |
| **2002年**                                                                         |
| [My Love For Music](../Page/My_Love_For_Music.md "wikilink")                      |
| **2005年**                                                                         |
| [Fantasy](../Page/Fantasy_\(李樂詩專輯\).md "wikilink")                                |
| **2006年**                                                                         |
|                                                                                   |
| **2008年**                                                                         |
|                                                                                   |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 曾獲獎項

  - 1992年：
      - 香港作曲作詞人協會（CASH）歌曲創作比賽前十名：Back Again
      - 香港新城電台FM Select最佳新人獎

<!-- end list -->

  - 1994年：
      - 香港TVB電視台十大勁歌金曲頒獎典禮「十大金曲獎」：終有一天感動你
      - 香港TVB電視台十大勁歌金曲頒獎典禮「最受歡迎創作歌手銅獎」
      - 香港TVB電視台勁歌金曲第四季季選得獎歌曲：終有一天感動你
      - 香港作曲作詞人協會（CASH）「最佳原創歌曲獎」：終有一天感動你
      - 香港電台「最佳中文(流行)歌曲獎」：終有一天感動你
      - 香港商業電台全年全港播放率最高五首歌曲之一：終有一天感動你

<!-- end list -->

  - 1999年：
      - 壹週刊最受歡迎電視劇主題曲：真情細說

## 備註

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  -
  -
  - [李樂詩個人官網](http://www.joyceleemusic.com)

[Category:香港作曲家](../Category/香港作曲家.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:多伦多皇家音乐学院校友](../Category/多伦多皇家音乐学院校友.md "wikilink")
[L](../Category/李姓.md "wikilink")

1.  李樂詩預產期與生日同日(19:26)
2.  《性感出擊 事業發圍　李樂詩失戀狂擦暴脹8磅》，《[太陽報](../Page/太陽報.md "wikilink")》，2006年8月4日
3.  《喜見父親王羽康復 王馨平 為慈善不收歌酬》，《[新報](../Page/新報.md "wikilink")》，2014年1月11日
4.  《Gin Lee 實力強 王馨平唔輸得》，《[明報](../Page/明報.md "wikilink")》，2014年1月11日