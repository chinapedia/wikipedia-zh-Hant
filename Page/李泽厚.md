**李泽厚**（）\[1\]，中国[湖南](../Page/湖南.md "wikilink")[長沙](../Page/長沙.md "wikilink")[寧鄉人](../Page/寧鄉.md "wikilink")。[哲学家](../Page/哲学.md "wikilink")、[美学家](../Page/美学.md "wikilink")、[中国思想史学家](../Page/中国思想史.md "wikilink")。曾担任[中国社会科学院研究员](../Page/中国社会科学院.md "wikilink")、[华东师范大学](../Page/华东师范大学.md "wikilink")[思勉人文高等研究院](../Page/思勉人文高等研究院.md "wikilink")、[德国](../Page/德国.md "wikilink")[图宾根大学](../Page/图宾根大学.md "wikilink")、美国[威斯康星大学](../Page/威斯康星大学.md "wikilink")、[密歇根大学](../Page/密歇根大学.md "wikilink")、[科罗拉多学院](../Page/科罗拉多学院.md "wikilink")、[斯沃斯莫尔学院客席教授](../Page/斯沃斯莫尔学院.md "wikilink")、客席讲座教授，台北[中央研究院客席讲座研究等职](../Page/中央研究院.md "wikilink")。曾當選為[中華人民共和國第七屆全國人民代表](../Page/第七届全国人民代表大会.md "wikilink")，是第七屆全國人大教育科學文化衛生委員會的委員\[2\]。

他在1988年当选院士，1998年获美国科罗拉多学院荣誉人文学博士学位\[3\]。

汉学家[余英时曾评价他](../Page/余英时.md "wikilink")：“通过（他的）书籍，他使得一整代中国青年知识分子从[共产主义的](../Page/共产主义.md "wikilink")[意识形态之中解放了出来](../Page/意识形态.md "wikilink")\[4\]。」

## 生平

李泽厚1948年畢業於[湖南省立第一師範](../Page/湖南省立第一師範.md "wikilink")。1954年毕业于[北京大学哲學系](../Page/北京大学.md "wikilink")。在1955年的[美学大讨论中崭露头角](../Page/美学大讨论.md "wikilink")，1950年代以重實踐、尚“人化”的“
客觀性與社會性相統一”的[美學觀卓然成家](../Page/美學觀.md "wikilink")。[文革以後在哲學](../Page/文革.md "wikilink")、美學和思想史方面均有所建樹。出版了《批判哲學的批判》、《[美的歷程](../Page/美的歷程.md "wikilink")》、《華夏美學》《中國古代、近代、現代思想史論》等作品。影響巨大。1989年[六四事件中](../Page/六四事件.md "wikilink")，因批评[中国政府的处理方式](../Page/中国政府.md "wikilink")，其作品被禁，本人受到中國政府和文人批判。1992年初獲准移居[美國](../Page/美國.md "wikilink")，曾任教于美国[科罗拉多学院](../Page/科罗拉多学院.md "wikilink")（[科羅拉多學院](../Page/科羅拉多學院.md "wikilink")）。1999年退休，现居[美国](../Page/美国.md "wikilink")[科罗拉多](../Page/科罗拉多.md "wikilink")。

## 思想

李泽厚宣揚[儒家](../Page/儒家.md "wikilink")[主情論](../Page/主情論.md "wikilink")，以“[告別革命](../Page/告別革命.md "wikilink")”說為轟轟烈烈的1980年代[啟蒙運動劃上了一個句號](../Page/中国启蒙运动.md "wikilink")。李澤厚曾表示：“[民族主義](../Page/民族主義.md "wikilink")，是很危險的事情。今天在世界任何地方都要反對民族主義。民族主義是最容易煽動民眾感情的一種主義。”\[5\]2005年12月06日但也有人認為李澤厚雖然反對民族主義，可沒有擺脫傳統“[華夷之辨](../Page/華夷之辨.md "wikilink")”的思想。2006年在有关[施琅问题的讨论中](../Page/施琅.md "wikilink")，他提出了[李自成如果胜利了](../Page/李自成.md "wikilink")，要比少數民族建立的[清朝强很多的观点](../Page/清朝.md "wikilink")。\[6\]\[7\]\[8\]其全部哲學思想與美學體系曾受到知識界的強烈質疑（在香港中文大學、廈門大學、貴州大學和武漢一些高校屢遭當地學報列據反駁）。

## 著作

李泽厚著有《美的历程》、《美学四讲》、《华夏美学》（合称《[美学三书](../Page/美学三书.md "wikilink")》）、《中国（古代、近代、现代）思想史论》、《批判哲学的批判》、《走我自己的路》、《李澤厚哲學美學文選》等。發表論文百餘篇，主要有《孔子再評價》、《漫述莊禪》、《啟蒙與救亡的雙重變奏》、《漫說西體中用》、《關於中國美學史的幾個問題》。香港[天地图书公司](../Page/天地图书.md "wikilink")1995年出版《告别革命——二十世纪中国对谈录》，为李泽厚与[刘再复的对话录](../Page/刘再复.md "wikilink")，由[刘再复执笔](../Page/刘再复.md "wikilink")，在中国大陆与海外学界都产生巨大影响。该书于1999年由[台北](../Page/台北.md "wikilink")[麦田出版公司编入](../Page/麦田出版公司.md "wikilink")“[麦田人文](../Page/麦田人文.md "wikilink")”图书系列，[王德威主编](../Page/王德威.md "wikilink")。

## 家庭

  - 妻子：與李澤厚育有1子；
  - 兒子（1977年-）：計算機專業畢業，從事電腦軟件工作；

## 参考文献

## 外部链接

  - [与李泽厚对话：“中国要过封建资本主义这一关”](http://www.ftchinese.com/story/001030572)，FT中文网，2009年12月31日

[Category:1930年出生](../Category/1930年出生.md "wikilink")
[Category:宁乡县人](../Category/宁乡县人.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")
[Category:宁乡四中校友](../Category/宁乡四中校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:湖南第一师范学院校友](../Category/湖南第一师范学院校友.md "wikilink")
[Category:中华人民共和国哲学家](../Category/中华人民共和国哲学家.md "wikilink")
[Category:六四人物](../Category/六四人物.md "wikilink")
[Category:中华人民共和国持不同政见者](../Category/中华人民共和国持不同政见者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.