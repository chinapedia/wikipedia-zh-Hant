**李洪宽**（英文：Li
Hongkuan，1963－）是一名[中华人民共和国持不同政见者](../Page/中华人民共和国持不同政见者.md "wikilink")，[海外华人圈中知名的](../Page/海外华人.md "wikilink")[反共人士](../Page/反共.md "wikilink")。出生于[山东](../Page/山东.md "wikilink")[德州](../Page/德州.md "wikilink")，现居美国首都[华盛顿](../Page/华盛顿.md "wikilink")。

毕业于[南京大学和](../Page/南京大学.md "wikilink")[中科院](../Page/中国科学院.md "wikilink")[上海生物化学研究所](http://www.sibcb.ac.cn/)。后来在[北京医科大学做教师](../Page/北京医科大学.md "wikilink")。1989年参加了[六四事件的抗议活动](../Page/六四事件.md "wikilink")。1991年至1994年在美国[阿尔伯特·爱因斯坦医学院](../Page/阿尔伯特·爱因斯坦医学院.md "wikilink")（[Albert
Einstein College of
Medicine](../Page/w:en:Albert_Einstein_College_of_Medicine.md "wikilink")）就读分子生物学博士。

1997年下旬开始以《[大参考](../Page/大参考_\(网络杂志\).md "wikilink")》的名字发布网络杂志。1998年又创办了类似的新闻日刊《小参考》。目的是突破[中共的](../Page/中共.md "wikilink")[防火长城](../Page/防火长城.md "wikilink")，向国内[电子邮箱传送新闻和](../Page/电子邮箱.md "wikilink")[中國民主運動的宣传](../Page/中國民主運動.md "wikilink")。亦曾接受[美国之音](../Page/美国之音.md "wikilink")、[自由亚洲电台](../Page/自由亚洲电台.md "wikilink")、[大纪元等媒体的采访](../Page/大纪元.md "wikilink")，在一些知名的节目中义务担任[时事评论员](../Page/时事评论员.md "wikilink")。

由于资金缺乏，《大参考》和《小参考》于2005年5月底停刊。

李洪宽也是中国最大反对党以及三千个[县委书记计划的主要缔造者](../Page/县委书记.md "wikilink")。

[郭文贵爆料事件发生后](../Page/郭文贵.md "wikilink")，李洪宽先是由支持郭文贵转变为反对郭文贵。遭到郭文贵于2018年1月26日在其所谓的“爆料”一周年之际在[马里兰联邦法院控告诽谤罪](../Page/马里兰.md "wikilink")，目前李洪宽正在准备应诉与反诉郭文贵，最近被爆出在一家餐馆打工。

其个人[推特简介为](../Page/推特.md "wikilink")：
“《大參考》創辦者。[六四受迫害](../Page/六四.md "wikilink")。[推牆鼻祖之一](../Page/中华人民共和国网络审查.md "wikilink")。高舉[普世價值旗幟](../Page/普世價值.md "wikilink")，为实现[宪政民主](../Page/宪政.md "wikilink")，衝鋒在前，呼唤同道人抱团取暖，集思广益。凝聚力量，准备接管[土共](../Page/中国共产党.md "wikilink")[政权](../Page/中华人民共和国政府.md "wikilink")，甘做铺路石，接盘侠。做未来民主建政的接生婆。山東人。現定居首都華盛頓地區。”

## 外部链接

  -
[L](../Category/中国持不同政见者.md "wikilink")
[L](../Category/1963年出生.md "wikilink")
[L](../Category/南京大学校友.md "wikilink")
[H](../Category/李姓.md "wikilink")