**李源**（），原名**李相协**，生于[大韩民国](../Page/大韩民国.md "wikilink")[首爾](../Page/首爾.md "wikilink")[鐘路區](../Page/鐘路區.md "wikilink")[惠化洞](../Page/惠化洞.md "wikilink")，是[朝鮮王朝的遺裔](../Page/李氏朝鲜.md "wikilink")。

李源是[朝鮮高宗的](../Page/朝鮮高宗.md "wikilink")[曾孙](../Page/曾孙.md "wikilink")、义亲王[李堈的孙儿](../Page/李堈.md "wikilink")，李堈的第十子[李鉀的長子](../Page/李鉀.md "wikilink")，过继给怀隐太孙[李玖](../Page/李玖.md "wikilink")。2005年7月16日[李玖去世后被选为](../Page/李玖.md "wikilink")[朝鮮王朝李氏家族首领](../Page/全州李氏.md "wikilink")，称**皇嗣孫**（）或**奉祀孫**（）。

當時他的家族首領地位受到姑母[李海瑗和時任大宗會會長的叔父](../Page/李海瑗.md "wikilink")[李錫的挑戰](../Page/李錫_\(韓國皇族\).md "wikilink")。2006年9月29日，李海瑗亦自行加冕成为朝鮮王朝家族首领，称大韩女皇。

李源现于[現代汽車公司工作](../Page/現代汽車.md "wikilink")，住在[京畿道的](../Page/京畿道.md "wikilink")[高陽市](../Page/高陽市.md "wikilink")，育有二子。

## 参考文献

## 参见

  - [全州李氏](../Page/全州李氏.md "wikilink")
      - [全州李氏大同宗約院](../Page/全州李氏大同宗約院.md "wikilink")
  - [李氏朝鲜君主列表](../Page/李氏朝鲜君主列表.md "wikilink")
  - [李王家](../Page/李王家.md "wikilink")
  - [大韩帝国皇族会](../Page/大韩帝国皇族会.md "wikilink")（2006年5月5日結成，9月29日改称“[大韓帝國皇室](../Page/大韓帝國.md "wikilink")”）

{{-}}  |-style="text-align: center; background: \#FFE4E1;"
|align="center" colspan="3"|**李源**

[Category:朝鮮王朝家族首領](../Category/朝鮮王朝家族首領.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L李源](../Category/韓國人.md "wikilink")
[Y源](../Category/李姓.md "wikilink")