**李煜**（），或稱**李後主**，為[南唐的末代](../Page/南唐.md "wikilink")[君主](../Page/君主.md "wikilink")，祖籍[徐州](../Page/徐州.md "wikilink")。李煜原名**從嘉**，[字](../Page/表字.md "wikilink")**重光**，[號](../Page/号.md "wikilink")**鍾山隱士**、**鍾峰隠者**、**白蓮居士**、**蓮峰居士**等。史書描述其政治上毫无建树，李煜在南唐灭亡后被[北宋俘虏](../Page/北宋.md "wikilink")，但是却成为了[中国历史上首屈一指的](../Page/中国历史.md "wikilink")[词人](../Page/词人.md "wikilink")，獲誉为「詞聖」、「千古詞帝」，作品千古流传。

## 生平

李煜“为人仁孝，善属文，工书画，而廣顙丰额骈齿，一目[重瞳子](../Page/雙瞳.md "wikilink")”\[1\]，是[南唐元宗](../Page/李璟_\(南唐\).md "wikilink")（南唐中主）[李璟的第六子](../Page/李璟_\(南唐\).md "wikilink")。由于[李璟的第二子到第五子均早死](../Page/李璟.md "wikilink")，故李煜长兄[李弘冀为皇太子时](../Page/李弘冀.md "wikilink")，其为事实上的第二子。[李弘冀](../Page/李弘冀.md "wikilink")“为人猜忌严刻”\[2\]，时为安定公的李煜因而惶恐，不敢参与政事，每天只醉心研究典籍，以读书为乐。

959年[李弘冀在毒死](../Page/李弘冀.md "wikilink")[李景遂后不久亦死](../Page/李景遂.md "wikilink")。[李璟欲立李煜为太子](../Page/李璟_\(南唐\).md "wikilink")，[钟谟说](../Page/钟谟.md "wikilink")“从嘉德轻志懦，又酷信释氏，非人主才。[从善果敢凝重](../Page/李從善.md "wikilink")，宜为嗣。”[李璟怒](../Page/李璟_\(南唐\).md "wikilink")，将钟谟贬为[国子监](../Page/国子监.md "wikilink")[司](../Page/司.md "wikilink")，流放到饶州。封李煜为吴王、尚书令、知政事，令其住在东宫，就近學習處理政事。

[宋](../Page/宋朝.md "wikilink")[建隆二年](../Page/建隆.md "wikilink")（961年），[李璟迁都](../Page/李璟.md "wikilink")[南昌并立李煜为](../Page/南昌.md "wikilink")[太子](../Page/储君.md "wikilink")、[監國](../Page/監國.md "wikilink")，令其留在[金陵](../Page/金陵.md "wikilink")。六月[李璟死后](../Page/李璟.md "wikilink")，李煜在[金陵登基即位](../Page/金陵.md "wikilink")。李煜“性骄侈，好声色，又喜[浮图](../Page/浮图.md "wikilink")，为高谈，不恤政事。”\[3\]笃信[佛教](../Page/佛教.md "wikilink")，“酷好浮屠，崇塔庙，度僧尼不可胜算。罢朝，辄造佛屋，易服膜拜，颇废政事。”\[4\]

971年宋军灭[南汉后](../Page/南汉.md "wikilink")，李煜为了表示他不对抗宋，对宋称臣，将自己的称呼改为江南国主。

973年，宋太祖令李煜至[汴京](../Page/汴京.md "wikilink")，李煜托病不往。宋太祖遂派[曹彬领军攻南唐](../Page/曹彬.md "wikilink")。

974年12月，[曹彬攻克金陵](../Page/曹彬.md "wikilink")，南唐灭亡。李煜在位十五年，後世称**李后主**或**南唐后主**。

975年，李煜被俘后，在[汴京被封为](../Page/汴梁.md "wikilink")**[违命侯](../Page/违命侯.md "wikilink")**，拜左千牛卫将军。

976年，宋太祖逝世，弟[赵光义继位为宋太宗](../Page/宋太宗.md "wikilink")，改封**隴國公**。嘗與[金陵舊宮人書寫](../Page/金陵.md "wikilink")：「此中日夕，以淚珠洗面」。宋人笔记上說趙光義多次逼迫[-{小周后}-侍寢](../Page/小周后.md "wikilink")\[5\]。李煜在痛苦鬱悶中，寫下《[望江南](../Page/望江南.md "wikilink")》、《子夜歌》、《[虞美人](../Page/虞美人_\(词牌\).md "wikilink")》等名曲。

978年，[徐铉奉宋太宗之命探视李煜](../Page/徐铉.md "wikilink")，李煜對徐鉉叹息：“当初我错杀[潘佑](../Page/潘佑.md "wikilink")、[李平](../Page/李平_\(南唐\).md "wikilink")，悔之不已！”徐铉退而告之，宋太宗闻之大怒。史載三年七月初七（978年8月13日），农历七夕，当李煜在其42岁生日那天与后妃们聚会，李煜卒，年四十二。一說李煜因寫“故国不堪回首月明中”、“恰似一江春水向東流”之词，宋太宗再也不能容忍，用牵机毒杀之。\[6\][牽機藥或說是中藥](../Page/牽機藥.md "wikilink")[馬錢子](../Page/牛眼馬錢.md "wikilink")，其主要成分[番木鳖碱有劇毒](../Page/番木鳖碱.md "wikilink")，服後會破壞[中樞神經系統](../Page/中樞神經系統.md "wikilink")，全身[抽搐](../Page/抽搐.md "wikilink")，腳往腹部縮，頭亦彎至腹部，狀極痛苦。\[7\]李煜死后，葬洛陽[北邙山](../Page/邙山.md "wikilink")\[8\]，-{小周后}-悲痛欲絕，不久也隨之死去。

李煜“生于深宫之中，长于妇人之手”\[9\]，雖無力治國，然“性宽恕，威令不素著”\[10\]，好生戒杀，性格出了名的善良，故在他死后，江南人闻之，“皆巷哭为斋”。

## 艺术、文學成就

李煜在艺术方面具有很高的成就。[劉毓盤说李后主](../Page/劉毓盤.md "wikilink")“于富贵时能作富贵语，愁苦时能作愁苦语，无一字不真。”

### 词

李煜词本有集，已失传。现存词四十四首。其中几首前期作品或为他人所作，可以确定者仅三十八首。李煜的词的风格可以以975年被俘而分为两个时期：

李煜亡國前的詞，透插富麗奢華的宮廷生活，言詞多溫軟綺麗，卿卿我我，呈現「[花間詞](../Page/花间派.md "wikilink")」氣息。根据内容可大致分为两类：一类是描写富丽堂皇的宫廷生活和风花雪月的男女情事，如《菩萨蛮》：

又如《[一斛珠](../Page/一斛珠.md "wikilink")》：

李煜亡國後，晚年的詞寫家國之恨，拓展了詞的題材，感慨既深，詞益悲壯。李煜詞最大特色，是自然真率，醇厚率真，情感真摯。喜用白描手法，通俗生動，語言精鍊而明淨洗煉，接近口語，與「[花間詞](../Page/花间派.md "wikilink")」縷金刻翠，堆砌華麗詞藻的作風迥然不同。李煜后期的词由于生活的巨变，以一首首泣尽以血的绝唱，使亡国之君成为千古词坛的“南面王”（[清](../Page/清.md "wikilink")[沈雄](../Page/沈雄.md "wikilink")《[古今词话](../Page/古今词话.md "wikilink")》语），正是“国家不幸诗家幸，话到沧桑语始工”。这些后期词作，凄凉悲壮，意境深远，为词史上承前启后的大宗师。至于其语句的清丽，音韵的和谐，更是空前绝后。如

《破阵子》：  《虞美人》：  《浪淘沙令》：

### 书画

他能书善画，对其[书法](../Page/书法.md "wikilink")：[陶穀](../Page/陶穀.md "wikilink")《[清異錄](../Page/清異錄.md "wikilink")》曾云：“后主善书，作颤笔樛曲之状，遒劲如寒松霜竹，谓之‘[金错刀](../Page/金錯刀_\(筆法\).md "wikilink")’。作大字不事笔，卷帛书之，皆能如意，世谓‘[撮襟书](../Page/撮襟书.md "wikilink")’。”。\[11\]对其的画，[宋代](../Page/宋朝.md "wikilink")[郭若虛的](../Page/郭若虛.md "wikilink")《[图书见闻志](../Page/图书见闻志.md "wikilink")》曰：“江南后主李煜，才识清赡，书画兼精。尝观所画林石、飞鸟，远过常流，高出意外。”。
[赵干江行初雪图首题.jpg](https://zh.wikipedia.org/wiki/File:赵干江行初雪图首题.jpg "fig:赵干江行初雪图首题.jpg")
[Zhaoyebaitu_by_Han_Gan.jpg](https://zh.wikipedia.org/wiki/File:Zhaoyebaitu_by_Han_Gan.jpg "fig:Zhaoyebaitu_by_Han_Gan.jpg")

## 评价

### 文學

[歐陽修在](../Page/欧阳修.md "wikilink")《[新五代史](../Page/新五代史.md "wikilink")》中描述李煜：“煜字重光，初名從嘉，景第六子也。煜為人仁孝，善屬文，工書畫，而豐額駢齒，一目重瞳子。”
\[12\]

《漁隱叢話前集·西清詩話》提到[宋太祖征服南唐统一中国后感叹](../Page/宋太祖.md "wikilink")：“李煜若以作诗词工夫治国家，岂为吾所俘也！”
近代学者[王国维认为](../Page/王国维.md "wikilink")：“温飞卿之词，句秀也；韦端己之词，骨秀也；李重光之词，神秀也。”\[13\]“词至李后主而眼界始大，感慨遂深，遂变伶工之词而为士大夫之词。周介存置诸温、韦之下，可谓颠倒黑白矣。”\[14\]。此最后一句乃是针对[周济在](../Page/周濟_\(嘉慶進士\).md "wikilink")《[介存斋论词杂著](../Page/介存斋论词杂著.md "wikilink")》中所道：“毛嫱、西施，天下美妇人也，严妆佳，淡妆亦佳，粗服乱头不掩国色。飞卿，严妆也；端己，淡妆也；后主，则粗服乱头矣。”王氏认为此评乃扬温、韦，抑后主。而学术界亦有观点认为，周济的本意是指李煜在词句的工整对仗等修饰方面不如[温庭筠](../Page/温庭筠.md "wikilink")、[韦庄](../Page/韦庄.md "wikilink")，然而在词作的生动和流畅度方面，则前者显然更为生机勃发，浑然天成，“粗服乱头不掩国色”。
李煜词摆脱了《[花间集](../Page/花间派.md "wikilink")》的浮靡，他的词不假雕饰，语言明快，形象生动，性格鲜明，用情真挚，亡国后作更是题材广阔，含意深沉，超过晚唐五代的词，不但成为宋初[婉约派词的开山](../Page/婉约派.md "wikilink")，也为[豪放派打下基础](../Page/豪放派.md "wikilink")，後世尊稱他為「詞聖」。
后代念及李煜的诗词中以清朝袁枚引《南唐雜詠》最有名：“作個才人真絕代，可憐薄命作君王。”

### 為政

《宋史·潘慎修傳》記載：南唐滅亡後，一些南唐舊臣開始批評李煜為人愚昧懦弱，添油加醋地成份越來越多。[宋真宗問潘慎修李煜是不是真的如此](../Page/宋真宗.md "wikilink")，潘慎修回答：「如果李煜真的這麼愚昧懦弱的話，他怎麼能治國十餘年？」

另一位南唐舊臣[徐鉉在](../Page/徐鉉.md "wikilink")《大宋左千牛衛上將軍追封吴王隴西公墓誌銘》中評價李煜：「以厭兵之俗當用武之世，孔明罕應變之略，不成近功；偃王，躬仁義之行，終于亡國，道有所在，復何媿歟？」\[15\]

### 其它

  - [徐铉](../Page/徐铉.md "wikilink")：“王以世嫡嗣服，以古道驭民，钦若彝伦，率循先志。奉蒸尝、恭色养，必以孝；事耇老、宾大臣，必以礼。居处服御必以节，言动施舍必以时。至于荷全济之恩，谨藩国之度，勤修九贡，府无虚月，祗奉百役，知无不为。十五年间，天眷弥渥。”“精究六经，旁综百氏。常以周孔之道不可暂离，经国化民，发号施令，造次于是，始终不渝。”“酷好文辞，多所述作。一游一豫，必以颂宣。载笑载言，不忘经义。洞晓音律，精别雅郑；穷先王制作之意，审风俗淳薄之原，为文论之，以续《乐记》。所著文集三十卷，杂说百篇，味其文、知其道矣。至于弧矢之善，笔札之工，天纵多能，必造精绝。”“本以恻隐之性，仍好竺干之教。草木不杀，禽鱼咸遂。赏人之善，常若不及；掩人之过，惟恐其闻。以至法不胜奸，威不克爱。以厌兵之俗当用武之世，孔明罕应变之略，不成近功；偃王躬仁义之行，终于亡国。道有所在，复何愧欤！”
  - [郑文宝](../Page/郑文宝.md "wikilink")：“后主奉竺乾之教，多不茹晕，常买禽鱼为放生。”“后主天性纯孝，孜孜儒学，虚怀接下，宾对大臣，倾奉中国，惟恐不及。但以著述勤于政事，至于书画皆尽精妙。然颇耽竺乾之教，果于自信，所以奸邪得计。排斥忠谠，土地曰削，贡举不充。越人肆谋，遂为敌国。又求援于北虏行人设谋，兵遂不解矣。”（《江表志》）
  - [陆游](../Page/陆游.md "wikilink")：“后主天资纯孝......专以爱民为急，蠲赋息役，以裕民力。尊事中原，不惮卑屈，境内赖以少安者十有五年。”“然酷好浮屠，崇塔庙，度僧尼不可胜算。罢朝辄造佛屋，易服膜拜，以故颇废政事。兵兴之际，降御札移易将帅，大臣无知者。虽仁爱足以感其遗民，而卒不能保社稷。”（《南唐书·卷三·后主本纪第三》）
  - [龙衮](../Page/龙衮.md "wikilink")：“后主自少俊迈，喜肄儒学，工诗，能属文，晓悟音律。姿仪风雅，举止儒措，宛若士人。”（《江南野史·卷三后主、宜春王》）
  - [陈彭年](../Page/陈彭年.md "wikilink")：“（后主煜）幼而好古，为文有汉魏风。”（《江南别录》）
  - [欧阳修](../Page/欧阳修.md "wikilink")：“煜性骄侈，好声色，又喜浮图，为高谈，不恤政事。”
  - [王世贞](../Page/王世贞.md "wikilink")：“花间犹伤促碎，至南唐李王父子而妙矣。”（《弇州山人词评》）
  - [胡应麟](../Page/胡应麟.md "wikilink")：“后主目重瞳子，乐府为宋人一代开山。盖温韦虽藻丽，而气颇伤促，意不胜辞。至此君方为当行作家，清便宛转，词家王、孟。”（《诗薮·杂篇》）
  - [纳兰性德](../Page/纳兰性德.md "wikilink")：“花间之词，如古玉器，贵重而不适用；宋词适用而少质重，李后主兼有其美，更饶烟水迷离之致。”（《渌水亭杂识·卷四》）
  - [王夫之](../Page/王夫之.md "wikilink")：“（李璟父子）无殃兆民，绝彝伦淫虐之巨惹。”“生聚完，文教兴，犹然彼都人士之余风也。”（《读通鉴论》）
  - [余怀](../Page/余怀.md "wikilink")：“李重光风流才子，误作人主，至有入宋牵机之恨。其所作之词，一字一珠，非他家所能及也。”（《玉琴斋词·序》）
  - [沈谦](../Page/沈谦.md "wikilink")：“男中李后主，女中李易安，极是当行本色。”（徐釚《词苑丛谈》引语）“后主疏于治国，在词中犹不失南面王。”（沈雄《古今词话·词话》卷上引语）
  - [郭麐](../Page/郭麐.md "wikilink")：“作个才子真绝代，可怜薄命作君王。”（清代[袁枚](../Page/袁枚.md "wikilink")《随园诗话补遗》引郭麐《南唐杂咏》）
  - [周济](../Page/周济.md "wikilink")：“李后主词如生马驹，不受控捉。”“毛嫱西施，天下美妇人也。严妆佳，淡妆亦佳，粗服乱头，不掩国色。飞卿，严妆也；端己，淡妆也；后主则粗服乱头矣。”（《介存斋论词杂著》）
  - [周之琦](../Page/周之琦.md "wikilink")：“予谓重光天籁也，恐非人力所及。”
  - [陈廷焯](../Page/陈廷焯.md "wikilink")：“后主词思路凄惋，词场本色，不及飞卿之厚，自胜牛松卿辈。”“余尝谓后主之视飞卿，合而离者也；端己之视飞卿，离而合者也。”“李后主、晏叔原，皆非词中正声，而其词无人不爱，以其情胜也。”（《白雨斋词话·卷一》）
  - [王鹏运](../Page/王鹏运.md "wikilink")：“莲峰居士（李煜）词，超逸绝伦，虚灵在骨。芝兰空谷，未足比其芳华；笙鹤瑶天，讵能方兹清怨？后起之秀，格调气韵之间，或月日至，得十一于千首。若小晏、若徽庙，其殆庶几。断代南流，嗣音阒然，盖间气所钟，以谓词中之大成者，当之无愧色矣。”（《半塘老人遣稿》）
  - [冯煦](../Page/冯煦.md "wikilink")：“词至南唐，二主作于上，正中和于下，诣微造极，得未曾有。宋初诸家，靡不祖述二主。”（《宋六十一家词选·例言》）
  - [王国维](../Page/王国维.md "wikilink")：“温飞卿之词，句秀也；韦端己之词，骨秀也；李重光之词，神秀也。”“词至李后主而眼界始大，感慨遂深，遂变伶工之词而为士大夫之词。”“词人者，不失其赤子之心者也。故生于深宫之中，长于妇人之手，是后主为人君所短处，亦即为词人所长处。”“主观之诗人，不必多阅世，阅世愈浅，则性情愈真，李后主是也。”“尼采谓一切文字，余爱以血书者，后主之词，真所谓以血书者也。宋道君皇帝《燕山亭》词，亦略似之。然道君不过自道身世之感，后主则俨有释迦、基督担荷人类罪恶之意，其大小固不同矣。”“唐五代之词，有句而无篇；南宋名家之词，有篇而无句。有篇有句，唯李后主之作及永叔、少游、美成、稼轩数人而已。”（《人间词话》）
  - [毛泽东](../Page/毛泽东.md "wikilink")：“南唐李后主虽多才多艺，但不抓政治，终于亡国。”（毛泽东评价历史人物）
  - [柏杨](../Page/柏杨.md "wikilink")：“南唐皇帝李煜先生词学的造诣，空前绝后，用在填词上的精力，远超过用在治国上。”（《浊世人间》）
  - [叶嘉莹](../Page/叶嘉莹.md "wikilink")：“李后主的词是他对生活的敏锐而真切的体验，无论是享乐的欢愉，还是悲哀的痛苦，他都全身心的投入其间。我们有的人活过一生，既没有好好的体会过快乐，也没有好好的体验过悲哀，因为他从来没有以全部的心灵感情投注入某一件事，这是人生的遗憾。”（《唐宋名家词赏析》）

## 家庭

### 皇后

  - [大周-{后}-](../Page/大周后.md "wikilink")（小字娥皇\[16\]）
  - [-{小周后}-](../Page/小周后.md "wikilink")

### 妃嫔

  - [保仪黄氏](../Page/黄保仪.md "wikilink")
    （记载于[馬令](../Page/馬令.md "wikilink")《[南唐书](../Page/南唐書_\(馬令\).md "wikilink")》）
  - [嫔御流珠](../Page/嫔御.md "wikilink")
  - [宫人乔氏](../Page/宫人.md "wikilink")
  - [宫人秋水](../Page/宫人.md "wikilink")
  - [宫人](../Page/宫人.md "wikilink")[窅娘](../Page/窅娘.md "wikilink")
  - [宫人](../Page/宫人.md "wikilink")[臧氏](../Page/臧贵妃.md "wikilink")

### 子女

#### 子

1.  清源郡公 [李仲寓](../Page/李仲寓.md "wikilink")
2.  岐懷獻王 [李仲宣](../Page/李仲宣.md "wikilink")

## 相關戲劇

### 電影

|       |                                        |                                  |
| ----- | -------------------------------------- | -------------------------------- |
| 年份    | 片名                                     | 演員                               |
| 1968年 | [李煜](../Page/李後主_\(電影\).md "wikilink") | [任剑辉](../Page/任剑辉.md "wikilink") |
| 1983年 | [封神劫](../Page/封神劫.md "wikilink")       | [陈家奇](../Page/陈家奇.md "wikilink") |

### 粵劇

|       |                                              |                                        |
| ----- | -------------------------------------------- | -------------------------------------- |
| 年份    | 劇名                                           | 演員                                     |
| 1982年 | [李後主 (粵劇)](../Page/李後主_\(粵劇\).md "wikilink") | [龙剑笙](../Page/龙剑笙.md "wikilink")\[17\] |

### 歌仔戲

|       |                                                                        |                                       |
| ----- | ---------------------------------------------------------------------- | ------------------------------------- |
| 年份    | 劇名                                                                     | 演員                                    |
| 1992年 | 華視葉青[歌仔戲](../Page/歌仔戲.md "wikilink")《[玉楼春](../Page/玉楼春.md "wikilink")》 | [葉青](../Page/葉青_\(臺灣\).md "wikilink") |

### 電視劇

|       |                                              |                                       |
| ----- | -------------------------------------------- | ------------------------------------- |
| 年份    | 劇名                                           | 演員                                    |
| 1978年 | [李煜](../Page/李煜.md "wikilink")(麗的電視)         | [陳振華](../Page/陳振華.md "wikilink")      |
| 1986年 | [绝代双雄](../Page/绝代双雄.md "wikilink")           | [李文海](../Page/李文海.md "wikilink")      |
| 1996年 | [大宋王朝赵匡胤](../Page/赵匡胤_\(电视剧\).md "wikilink") | [沈保平](../Page/沈保平.md "wikilink")      |
| 1996年 | [情剑山河](../Page/情剑山河.md "wikilink")           | [秦风](../Page/秦风_\(演員\).md "wikilink") |
| 2005年 | [问君能有几多愁](../Page/问君能有几多愁.md "wikilink")     | [吳奇隆](../Page/吳奇隆.md "wikilink")      |
| 2015年 | [大宋传奇之赵匡胤](../Page/大宋传奇之赵匡胤.md "wikilink")   | [张亚希](../Page/张亚希.md "wikilink")      |

## 参考文献

## 研究書目

  - 陳葆真：《李後主和他的時代：南唐藝術與歷史》（台北：石頭出版股份有限公司，2007）。
  - 徐玮：〈[从魂迷春梦到浮生如梦——李煜梦词探微](http://www.nssd.org/articles/Article_Read.aspx?id=44539820)〉。

## 外部链接

  - [诗词总汇](https://web.archive.org/web/20160307044711/http://www.sczh.com/sczh/showpoet.asp?%E4%BD%9C%E8%80%85%E7%BC%96%E5%8F%B7=274)

[3](../Category/南唐皇帝.md "wikilink")
[Category:五代十國詞人](../Category/五代十國詞人.md "wikilink")
[Category:中国末代君主](../Category/中国末代君主.md "wikilink")
[Category:北宋國公](../Category/北宋國公.md "wikilink")
[Category:北宋将军](../Category/北宋将军.md "wikilink")
[Category:宋朝被杀害人物](../Category/宋朝被杀害人物.md "wikilink")
[Category:徐州人](../Category/徐州人.md "wikilink")
[Category:中國被殺帝王](../Category/中國被殺帝王.md "wikilink")
[Category:中國被毒死人物](../Category/中國被毒死人物.md "wikilink")

1.  《新五代史》卷62《南唐世家第二》

2.  《资治通鉴》卷第二百九十四

3.
4.
5.  宋人王铚在《默记》中说：“李国主小周后，随后主归朝，封郑国夫人，例随命妇入宫，每一入辄数日，而出必大泣，骂后主，声闻于外，后主多婉转避之。”

6.  [陈霆](../Page/陳霆_\(明朝\).md "wikilink")《唐余记传》载：“煜以七夕日生，是日燕饮声伎，彻于禁中。太祖衔其有‘故国不堪回首’之词，至是又愠其酣畅，乃命楚王元佐等携觞就其第而助之欢。酒阑中，煜中牵机药毒而死。”

7.  [王銍](../Page/王銍.md "wikilink")《默記》：“服之前卻數十回，頭足相就如牽機狀也。”

8.  据《宋史》李煜“葬北邙山”；徐铉曾为〈李煜墓志铭〉写道：李煜墓地“二室南峙，三川东注，瞻上阳之宫阙，望北邙之灵树”。《河南府志》记载，“李煜葬洛阳城北十里。”

9.  [王国维](../Page/王国维.md "wikilink")：《人间词话》

10. [文莹](../Page/文莹.md "wikilink")：《湘山野录》

11. [十國春秋·卷十七·後主本紀](../Page/s:十國春秋/卷十七#後主本紀.md "wikilink")

12.

13.
14. 《[人间词话](../Page/人间词话.md "wikilink")》

15.

16. [陸游](../Page/陸游.md "wikilink")《[南唐书](../Page/南唐書_\(陸游\).md "wikilink")
    昭惠-{后}-传》后主昭惠国-{后}-周氏，小名娥皇，司徒宗之女，十九歳来归。

17. 【戲曲之旅】第138期A 26-27 頁《李後主》開山演出、第139期B 40-41頁《李後主》幕後、第140期A
    48-49頁《李後主》揚威拉斯維加斯