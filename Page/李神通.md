**李神通**，名**寿**，[以字行](../Page/以字行.md "wikilink")。為[唐高祖李淵之從父弟](../Page/李渊.md "wikilink")。唐朝初年大臣。

## 簡傳

李神通於隋朝末年停留在京師，當時李淵與[李世民等人從](../Page/李世民.md "wikilink")[太原起兵](../Page/太原起兵.md "wikilink")，李神通與親人為了躲避官吏追捕，逃入[鄠縣山區南部](../Page/鄠縣.md "wikilink")，與[史萬寶](../Page/史萬寶.md "wikilink")、[裴勣](../Page/裴勣.md "wikilink")、[柳崇禮等人舉兵響應](../Page/柳崇禮.md "wikilink")，事後李淵授以[光祿大夫](../Page/光祿大夫.md "wikilink")，跟隨李淵一同平定京師後改授[宗正卿](../Page/宗正卿.md "wikilink")。李氏宗親於外地響應者唯有李神通一支。

[武德元年](../Page/武德.md "wikilink")，李神通被封為[鄭国公](../Page/国公.md "wikilink")，不久改封[永康](../Page/永康郡.md "wikilink")[郡王](../Page/郡王.md "wikilink")，再改封[淮安](../Page/淮安郡.md "wikilink")[郡王](../Page/郡王.md "wikilink")，以[右翊衛大將軍一職擔任](../Page/右翊衛大將軍.md "wikilink")[山東道](../Page/山東道.md "wikilink")[安撫大使](../Page/安撫大使.md "wikilink")，山東地區唐軍皆受到李神通的管制。此時李氏宗親除李淵諸子以外，唯李神通勢力最大、任務最重，從李淵將山東一帶軍事委由李神通處置，可見他對李神通之信賴。

李神通攻伐[竇建德](../Page/竇建德.md "wikilink")，雖兵敗遭擄，卻以唐帝血親的關係被待以客禮。武德四年，竇建德失敗，李神通被授命為[河北道](../Page/河北道.md "wikilink")[行臺](../Page/行臺.md "wikilink")[尚書左僕射](../Page/尚書左僕射.md "wikilink")，仍在撫慰山東之重任。事後與[秦王李世民討伐](../Page/秦王.md "wikilink")[劉黑闥](../Page/劉黑闥.md "wikilink")，改遷[左武衛大將軍](../Page/左武衛大將軍.md "wikilink")。天下平定後，李神通遂少見用。

武德九年[玄武門之變爆發](../Page/玄武門之變.md "wikilink")，雖然未見李神通策劃與響應，然而李神通素與李世民相善，事發之前仍與李世民夜飲，並護送李世民返回府邸，應屬於支持李世民一派。

[貞觀元年](../Page/貞觀_\(唐朝\).md "wikilink")，拜[開府儀同三司](../Page/開府儀同三司.md "wikilink")，賜[實封五百戶](../Page/實封.md "wikilink")。諸將相不敢論功敘勳於李神通之上，[唐太宗李世民調降宗室郡王皆為縣公時](../Page/唐太宗.md "wikilink")，李神通亦以功勳得以不降，可見李神通的功勳及地位對於李世民節制宗室、功臣有所用處。

貞觀四年（630年），李神通薨，年五十四，[追赠](../Page/追赠.md "wikilink")[司空](../Page/司空.md "wikilink")，諡曰靖。

貞觀十四年，唐太宗詔淮安王李神通與[河間王](../Page/河間王.md "wikilink")[李孝恭](../Page/李孝恭.md "wikilink")、贈[陝州大行臺](../Page/陝州.md "wikilink")[右僕射](../Page/右僕射.md "wikilink")[鄖節公](../Page/国公.md "wikilink")[殷開山](../Page/殷開山.md "wikilink")、贈[民部尚書](../Page/民部尚書.md "wikilink")[渝襄公](../Page/国公.md "wikilink")[劉政會配饗唐高祖李淵廟庭](../Page/劉政會.md "wikilink")。

## 李神通墓志铭

李神通墓志铭1973年出土于陕西[三原县焦村的李神通墓中](../Page/三原县.md "wikilink")。墓志盖篆书“大唐故司空公上柱国淮安靖王墓志铭”。

## 家世

  - 祖父 [李虎](../Page/李虎.md "wikilink")
  - 父
    [李亮](../Page/李亮_\(隋朝\).md "wikilink")，[海州](../Page/海州.md "wikilink")[刺史](../Page/刺史.md "wikilink")，追封郑孝王
  - 弟 [李神符](../Page/李神符.md "wikilink")，襄邑[郡王](../Page/郡王.md "wikilink")

有子十一人：

  - [李道彥](../Page/李道彥.md "wikilink")，长子，武德五年，封胶东王。武德年间，其兄弟七人皆封郡王，唐太宗执政时期，宗室郡王降爵，所以道彥等人降为公爵。
  - [李孝詧或李孝察](../Page/李孝詧.md "wikilink")\[1\]，次子，高密王
      - [李瑒](../Page/李瑒.md "wikilink")，房忠易三州刺史，封狄道郡公
          - [李延明](../Page/李延明.md "wikilink")，中散大夫、睦饶二州司马、德赵二州长史、右卫率府郎将
              - 李氏，河南府河阳县丞、上柱国庞夷远妻
  - [李孝同](../Page/李孝同.md "wikilink")，淄川王
      - [李璲](../Page/李璲.md "wikilink")，左衛將軍
          - [李廣業](../Page/李廣業.md "wikilink")，[劍州長史](../Page/劍州.md "wikilink")
              - [李國貞](../Page/李國貞.md "wikilink")，字南華，[戶部尚書](../Page/戶部尚書.md "wikilink")
                  - [李錡](../Page/李錡.md "wikilink")
                  - [李鎰](../Page/李鎰.md "wikilink")，[蘄州刺史](../Page/蘄州.md "wikilink")
                  - [李豐士](../Page/李豐士.md "wikilink")，[鄠令](../Page/鄠.md "wikilink")
                  - [李豐器](../Page/李豐器.md "wikilink")，長安尉
              - [李若冰](../Page/李若冰.md "wikilink")，右金吾將軍
                  - [李銛](../Page/李銛.md "wikilink")，[京兆尹](../Page/京兆尹.md "wikilink")
      - [李璩](../Page/李璩.md "wikilink")，[徐州刺史](../Page/徐州.md "wikilink")
      - 李氏，嫁左卫郎将检校左武卫将军上骑都尉于谦
  - [李孝慈](../Page/李孝慈.md "wikilink")，廣平郡王
  - [李孝友](../Page/李孝友.md "wikilink")，河間郡王，尚书左丞
      - [李珍](../Page/李珍.md "wikilink")，[工部尚书](../Page/工部尚书.md "wikilink")
  - [李孝節](../Page/李孝節.md "wikilink")，清河郡王，[灵州都督](../Page/灵州.md "wikilink")
      - [李頊](../Page/李頊.md "wikilink")，[汝州刺史](../Page/汝州.md "wikilink")
      - [李琬](../Page/李琬.md "wikilink")，[幽州都督](../Page/幽州.md "wikilink")
      - [李瑜](../Page/李瑜.md "wikilink")，[鄭州刺史](../Page/鄭州.md "wikilink")
          - [李昇](../Page/李昇.md "wikilink")，[光祿卿](../Page/光祿卿.md "wikilink")
              - [李遘](../Page/李遘.md "wikilink")
          - [李暠](../Page/李暠.md "wikilink")，武都郡公、[吏部尚書](../Page/吏部尚書.md "wikilink")
              - [李造](../Page/李造.md "wikilink")，[起居舍人](../Page/起居舍人.md "wikilink")
          - [李暈](../Page/李暈.md "wikilink")，[太僕卿](../Page/太僕卿.md "wikilink")
              - [李遵](../Page/李遵.md "wikilink")，太子少傅、鄭國公
                  - [李謂](../Page/李謂.md "wikilink")，字伯英，太常少卿
                  - [李诵](../Page/李诵.md "wikilink")、[李諲](../Page/李諲.md "wikilink")、[李谔](../Page/李谔.md "wikilink")
              - [李遇](../Page/李遇.md "wikilink")，[御史中丞](../Page/御史中丞.md "wikilink")、東畿採訪使
                  - [李說](../Page/李說.md "wikilink")，字巖甫，[河東節度使](../Page/河東節度使.md "wikilink")
                      - [李公敏](../Page/李公敏.md "wikilink")，太子通事捨人
                      - [李公度](../Page/李公度_\(唐朝\).md "wikilink")，[朔方節度使](../Page/朔方節度使.md "wikilink")
                      - [李公輔](../Page/李公輔.md "wikilink")
                      - [李公佐](../Page/李公佐.md "wikilink")，千牛备身
                      - [李公宥](../Page/李公宥.md "wikilink")
              - [李勛](../Page/李勛.md "wikilink")，[絳州刺史](../Page/絳州.md "wikilink")、祕書監
                  - [李真宰](../Page/李真宰.md "wikilink")
              - [李進](../Page/李進.md "wikilink")，[兵部侍郎](../Page/兵部侍郎.md "wikilink")
                  - [李少和](../Page/李少和.md "wikilink")，江西觀察使
  - [李孝義](../Page/李孝義.md "wikilink")，膠西郡王、司农卿
      - [李璥](../Page/李璥.md "wikilink")，襲公，[南州司馬](../Page/南州_\(黔中道\).md "wikilink")
          - [李孟犨](../Page/李孟犨.md "wikilink")（677年—731年），字公悅，[泗州刺史](../Page/泗州.md "wikilink")。妻[清河崔氏](../Page/清河崔氏.md "wikilink")（684年—756年），父崔惟明
              - [李權](../Page/李權.md "wikilink")，[金州刺史](../Page/金州.md "wikilink")
              - [李衡](../Page/李衡.md "wikilink")，次子，[清漳尉](../Page/清漳.md "wikilink")。夫人[彭城刘氏](../Page/彭城刘氏.md "wikilink")（714年—771年三月），左散骑常侍[刘知几女](../Page/刘知几.md "wikilink")
              - [李樞](../Page/李樞.md "wikilink")，檢校虞部員外郎兼侍御史
              - [李軫](../Page/李軫.md "wikilink")，[歙州別駕](../Page/歙州.md "wikilink")
              - [李房](../Page/李房.md "wikilink")，[渭南令](../Page/渭南.md "wikilink")
                  - [李萬鈞](../Page/李萬鈞.md "wikilink")
                      - [李○](../Page/李○.md "wikilink")，字耀山
                  - [李千鈞](../Page/李千鈞.md "wikilink")
                      - [李磁](../Page/李磁.md "wikilink")，字景山
                      - [李蟾](../Page/李蟾.md "wikilink")，初名○
                      - [李蚡](../Page/李蚡.md "wikilink")，字漢山
                          - [李廷璧](../Page/李廷璧.md "wikilink")，字冠祥
                      - [李蠙](../Page/李蠙.md "wikilink")，字懿川
                          - [李昭裔](../Page/李昭裔.md "wikilink")，字延誨
                          - [李昭業](../Page/李昭業.md "wikilink")，字延章
                          - [李昭圖](../Page/李昭圖.md "wikilink")，字延○
                  - [李仁鈞](../Page/李仁鈞.md "wikilink")
                      - [李硎](../Page/李硎.md "wikilink")，字次山
                      - [李礎](../Page/李礎.md "wikilink")
              - [李翼](../Page/李翼.md "wikilink")，字則之，宗正卿、太子賓客、隴西縣子
                  - 李洪钧（751年—755年8月4日），字家奴，长子
                  - [李良鈞](../Page/李良鈞.md "wikilink")，大理評事
                  - [李正鈞](../Page/李正鈞.md "wikilink")，[陸渾尉](../Page/陸渾.md "wikilink")
                  - [李執鈞](../Page/李執鈞.md "wikilink")
                  - [李臣鈞](../Page/李臣鈞.md "wikilink")，左千牛衛兵曹參軍
                  - [李直鈞](../Page/李直鈞.md "wikilink")，一子出身
  - [李孝锐](../Page/李孝锐.md "wikilink")，[鹽州刺史](../Page/鹽州.md "wikilink")
      - [李璟](../Page/李璟.md "wikilink")，[陇州司仓](../Page/陇州.md "wikilink")，赠[弘农郡太守](../Page/弘农郡.md "wikilink")
          - [李齊物](../Page/李齊物.md "wikilink")，[刑部尚書](../Page/刑部尚書.md "wikilink")
              - [李復](../Page/李復.md "wikilink")，鄭滑節度使、檢校右僕射
              - [李條](../Page/李條.md "wikilink")，字堅，司農少卿
                  - [李昌](../Page/李昌.md "wikilink")
      - [李琇](../Page/李琇.md "wikilink")，字琇，淮安忠公、宗正卿
          - [李齊晏](../Page/李齊晏.md "wikilink")，陝王府戶曹參軍
          - [李齊古](../Page/李齊古.md "wikilink")，少府監
          - [李暐](../Page/李暐.md "wikilink")，文部侍郎
              - [李汶](../Page/李汶.md "wikilink")，御史中丞
          - [李曄](../Page/李曄.md "wikilink")，[刑部侍郎](../Page/刑部侍郎.md "wikilink")
          - [李旴](../Page/李旴.md "wikilink")，符寶郎
  - [李孝逸](../Page/李孝逸.md "wikilink")，梁郡公，给事中
  - [李孝廉](../Page/李孝廉.md "wikilink")，[越州都督](../Page/越州.md "wikilink")
      - [李洵](../Page/李洵.md "wikilink")，朝议大夫、左屯卫录事
          - [李乔卿](../Page/李乔卿.md "wikilink")，朝议郎、守[魏州司马](../Page/魏州.md "wikilink")、[上柱国](../Page/上柱国.md "wikilink")

有女：有史可查者一人

  - 李氏（619年—673年），封怀德县主，夫司马缄

## 注释

## 參考資料

  - [劉昫](../Page/劉昫.md "wikilink")，《[旧唐书](../Page/旧唐书.md "wikilink")·卷六十·列传第十》
  - [歐陽修](../Page/歐陽修.md "wikilink")、[宋祁](../Page/宋祁.md "wikilink")，《[新唐書](../Page/新唐書.md "wikilink")·列传第三·宗室》
  - [司馬光等](../Page/司馬光.md "wikilink")，《[資治通鑑](../Page/資治通鑑.md "wikilink")》
  - 吴钢．《全唐文补遗》 第五辑：三秦出版社，1994

[Category:唐朝宗室郡王](../Category/唐朝宗室郡王.md "wikilink")
[Category:晋阳起兵人物](../Category/晋阳起兵人物.md "wikilink")
[Category:唐朝光禄大夫](../Category/唐朝光禄大夫.md "wikilink")
[Category:唐朝大使](../Category/唐朝大使.md "wikilink")
[Category:唐朝都督](../Category/唐朝都督.md "wikilink")
[Category:唐朝宗正卿](../Category/唐朝宗正卿.md "wikilink")
[翊](../Category/唐朝右卫大将军.md "wikilink")
[Category:唐朝行台](../Category/唐朝行台.md "wikilink")
[Category:唐朝尚书左仆射](../Category/唐朝尚书左仆射.md "wikilink")
[Category:唐朝左武卫大将军](../Category/唐朝左武卫大将军.md "wikilink")
[Category:唐朝开府仪同三司](../Category/唐朝开府仪同三司.md "wikilink")
[Category:唐朝上柱国](../Category/唐朝上柱国.md "wikilink")
[Category:唐朝追赠司空](../Category/唐朝追赠司空.md "wikilink")
[Category:大唐雙龍傳登場人物](../Category/大唐雙龍傳登場人物.md "wikilink")
[S神通](../Category/李姓.md "wikilink")
[Category:諡靖](../Category/諡靖.md "wikilink")

1.  《[旧唐书](../Page/旧唐书.md "wikilink")·卷六十·列传第十》