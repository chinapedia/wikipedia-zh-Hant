**李素妍**（[韩语](../Page/韩语.md "wikilink")：**이소연**，）是[大韓民國的第一位太空人](../Page/大韓民國.md "wikilink")，她在2008年4月8日乘坐俄羅斯的太空船到達[國際太空站](../Page/國際太空站.md "wikilink")。生於[光州廣域市](../Page/光州廣域市.md "wikilink")、在[韩国科学技术院取得博士學位的李素妍](../Page/韩国科学技术院.md "wikilink")，亦是亞洲第二位進入太空的女性\[1\]\[2\]\[3\]\[4\]\[5\]。

## 個人簡介

李素妍在光州出生及長大。高中在[光州科學高等學校](../Page/光州科學高等學校.md "wikilink")（）就讀，之後升讀[韓國科學技術院機械工程學系及研究院並畢業](../Page/韓國科學技術院.md "wikilink")，並於2008年3月1日取得[生物系統博士資格](../Page/生物系統.md "wikilink")。

## 南韓太空人計畫

[Korean_astronauts-Space_station_training-01.jpg](https://zh.wikipedia.org/wiki/File:Korean_astronauts-Space_station_training-01.jpg "fig:Korean_astronauts-Space_station_training-01.jpg")一同於[詹森太空中心的](../Page/林頓·约翰遜太空中心.md "wikilink")[太空艙模擬設備參與太空站硬件訓練](../Page/太空艙模擬設備.md "wikilink")。\]\]
[大韓民國科學技術部](../Page/大韓民國教育部.md "wikilink")（）\[6\]，於2000年12月與
[俄羅斯共同設立](../Page/俄羅斯.md "wikilink")[宇宙飛行員養成計劃](../Page/宇宙飛行員養成計劃.md "wikilink")。2006年4月發表宇宙飛行員募集公告以來，在12月25日從3萬6206名申請人裡選出她和[高山二人成為候補](../Page/高山_\(宇航員\).md "wikilink")。二人於2007年3月開始到俄羅斯的[加加林太空人訓練中心接受太空適應及太空科學實驗修行等高等訓練](../Page/加加林宇航员培训中心.md "wikilink")。

2007年9月5日，南韓當局原先選定由高山擔任宇航員，並由李素妍擔任候補，因為高山在俄羅斯的訓練中取得較好的成績\[7\]\[8\]。不過，在2008年3月7日，由於高山多次違反訓練的保安規定，把受訓資料從俄國寄返韓國，因而被取銷了正選的資格，由李素妍補上，而高山成為了李素妍的候補。3月10日，有關決定得到教育科學技術部的確認並公布\[9\]\[10\]\[11\]\[12\]。

2008年4月8日，李素妍與另外兩名俄羅斯太空人搭乘[联盟号宇宙飞船前往國際太空站](../Page/联盟号宇宙飞船.md "wikilink")。她成為了繼來自日本的[向井千秋之後另一位來自亞洲的女太空人](../Page/向井千秋.md "wikilink")。在全世界來說，李素妍亦是第49位女太空人、全世界第475位太空人。

## 方式

由於韓國與俄國政府的合作是透過商業合約方式，所以李素妍在聯合號太空船上的身份，根據俄羅斯[聯盟號宇宙飛船和](../Page/聯盟號宇宙飛船.md "wikilink")[NASA](../Page/NASA.md "wikilink")
文件上和簡短的新聞，只是一位參與者\[13\]。

## 任務內容

在太空站期間，李素妍將為[韩国航空宇宙研究院進行](../Page/韩国航空宇宙研究院.md "wikilink")18項科學實驗，並在[國際太空站接受傳媒實時訪問](../Page/國際太空站.md "wikilink")。李素妍甚至隨身擕帶了一件備有空氣調節裝置的特製容器，其中容納了上千隻果蠅。在這項[建國大學設計的實驗中](../Page/建國大學.md "wikilink")，李將記錄下重力以及其他環境因素對果蠅的行為以及其基因組所造成的變化。其他的實驗項目還包括植物在太空中的成長記錄，李素妍本人的心臟活動研究，以及重力變化對其眼壓和臉型所造成的影響—李將會一天六次使用[三星特製的](../Page/三星集團.md "wikilink")[3D相機](../Page/3D相機.md "wikilink")(Stereo
camera)對自己的面部進行拍攝，以觀察不同重力下臉型的膨脹程度。她將觀測地球，尤其是關注自中國北部而來影響韓國的沙塵暴活動，也同時檢測[國際太空站站內的噪音水平](../Page/國際太空站.md "wikilink")。

韓國科學家特地為他們的首位太空人設計了一種低熱量高維生素的特製[泡菜](../Page/韓國泡菜.md "wikilink")，供其在太空站享用\[14\]。

## 公開行程

李素妍回航後，會與高山一同在KARI進行研究工作，並共同擔任太空大使。另外，已經有廣告公司向她接洽，在電視廣告中演出。

## 家庭

李素妍的母親鄭金順（59歲）\[15\]\[16\]她是家里的長女，她還有一名弟弟和妹妹。

李素妍从2012年开始休假到[美国进修经营学](../Page/美国.md "wikilink")，并与一名[韩裔美国人结婚](../Page/韩裔美国人.md "wikilink")。\[17\]

## 相關條目

  - [各国宇航员首次上天时间表](../Page/各国宇航员首次上天时间表.md "wikilink")

## 參考文獻

## 外部連結

  - [*Spacefacts* biography of Yi
    So-yeon](http://www.spacefacts.de/bios/international/english/lee_so-hyun.htm)
  - [A series of interviews with Seoul Glow](http://www.seoulglow.com/)

[Category:韩国宇航员](../Category/韩国宇航员.md "wikilink")
[Category:女太空人](../Category/女太空人.md "wikilink")
[Category:生物工程师](../Category/生物工程师.md "wikilink")
[L](../Category/光州廣域市出身人物.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")
[Category:韓國科學技術院校友](../Category/韓國科學技術院校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")

1.  <http://www.koreatimes.co.kr/www/news/tech/2008/04/133_22148.html>
2.
3.  <http://www.voanews.com/english/2008-04-08-voa28.cfm>
4.
5.  <http://www.space.com/missionlaunches/080408-exp17-launch.html>
6.  現在由[大韓民國教育科學技術部](../Page/大韓民國教育科學技術部.md "wikilink")(主管)
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17. [宇航员李素妍的变身](http://chinese.donga.com/gb/srv/service.php3?biid=2014062760548)