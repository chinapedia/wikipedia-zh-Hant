**李翊云**（Yiyun
Li，）\[1\]是一位在[美国用](../Page/美国.md "wikilink")[英文写作的](../Page/英文.md "wikilink")[华人女](../Page/华人.md "wikilink")[作家](../Page/作家.md "wikilink")。生于[北京](../Page/北京.md "wikilink")，1991年毕业于[北京四中](../Page/北京四中.md "wikilink")，1996年毕业于[北京大学生命科学学院](../Page/北京大学生命科学学院.md "wikilink")。2000年获美国[爱荷华大学免疫专业硕士](../Page/爱荷华大学.md "wikilink")。2002年入[爱荷华作家工作坊](../Page/爱荷华作家工作坊.md "wikilink")，開始發表[英文](../Page/英文.md "wikilink")[短篇小说作品](../Page/短篇小说.md "wikilink")。2005年获[藝術創作碩士](../Page/藝術創作碩士.md "wikilink")学位\[2\]。其創作獲得許多獎項，在美歐文壇聲譽鵲起。她現居[加州](../Page/加州.md "wikilink")[屋崙市](../Page/奥克兰_\(加利福尼亚州\).md "wikilink")。除了寫作之外，她也在[戴維斯加利福尼亞大學授課](../Page/戴維斯加利福尼亞大學.md "wikilink")。

## 文學創作

1996年李翊云在[美国](../Page/美国.md "wikilink")[爱荷华大学攻读](../Page/爱荷华大学.md "wikilink")[免疫学](../Page/免疫学.md "wikilink")[博士学位时候丈夫不在身边](../Page/博士.md "wikilink")，实验功课又重，她选择了用英文写作来放松神经。她在1997年纯粹為兴趣加入了一个社区写作培训班\[3\]。後來发现自己很喜欢写作，于是放弃免疫学专业，不再攻读[免疫学](../Page/免疫学.md "wikilink")[博士學位](../Page/博士.md "wikilink")，於2000年時以[免疫学](../Page/免疫学.md "wikilink")[碩士学位畢業](../Page/碩士.md "wikilink")。在医院里工作了两年，同时试着写些作品。2002年進入曾經培育[王文興](../Page/王文興.md "wikilink")、[白先勇等著名作家的爱荷华大学作家工作坊](../Page/白先勇.md "wikilink")，攻讀[藝術創作碩士](../Page/藝術創作碩士.md "wikilink")，开始发表作品\[4\]。

她早期的创作均为[英文](../Page/英文.md "wikilink")[短篇小说](../Page/短篇小说.md "wikilink")，曾在《[纽约客](../Page/纽约客.md "wikilink")》和《巴黎评论》（）上发表。2005年结集出版的《千年修得共枕眠》（）。其中收录了《多余》(Extra)、《不朽》(Im-mortality)等十篇故事，大多以[改革开放后的](../Page/改革开放.md "wikilink")[中国为背景](../Page/中国.md "wikilink")，写的是小人物的悲欢——贵族学校的清洁女工，股票市场中失意的退休教师，毕业分配回老家教书的英语老师，内蒙插队知青的后代等。故事很宿命
(fatalistic)\[5\]。2009年出版《漂泊者》（The Vagrants）長篇小說。2010年出版《金童玉女》（Gold Boy,
Emerald Girl）短篇小說集，收入八个短篇和一个中篇\[6\]。

## 獲獎

  - 2004年获得[美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")《巴黎评论》年度新人奖
  - 2006年懷丁作家獎
  - 2005年[爱尔兰的](../Page/爱尔兰.md "wikilink")[弗兰克·奥康纳国际短篇小说奖](../Page/弗兰克·奥康纳国际短篇小说奖.md "wikilink")（）-《千年修得共枕眠》<ref name="世界奖金最高奖">

</ref>

  - 2006年[美国笔会海明威奖](../Page/美国笔会.md "wikilink")（）-《千年修得共枕眠》
  - 2006年[英國](../Page/英國.md "wikilink")[衛報新人獎](../Page/衛報.md "wikilink")
    () -《千年修得共枕眠》
  - 加州書獎（）
  - 2007年3月被英国老牌文学杂志《格兰塔》(Granta)杂志评为美国最傑出的21位35歲以下青年小说家。
  - 2010年《[纽约客](../Page/纽约客.md "wikilink")》選為最傑出的20位40歲以下青年小说家\[7\]。

## 出版作品

  - 《A Thousand Years of Good Prayers》《千年修得共枕眠》，
    [藍燈書屋](../Page/藍燈書屋.md "wikilink"), 2005年.
  - 《The Vagrants》《漂泊者》，[藍燈書屋](../Page/藍燈書屋.md "wikilink"), 2009年.
  - 《Gold Boy, Emerald Girl》《金童玉女》，[藍燈書屋](../Page/藍燈書屋.md "wikilink"),
    2010年.

### 文选

  - 最佳美國短篇小說集 《Best American Short Stories》2006 "After a Life"

  - 最佳美國短篇小說集 《Best American Short Stories》2009 "A Man Like Him"

  -
## 電影

李翊云的作品曾被拍成電影：

  - 《A Thousand Years of Good Prayers》(2007)<ref>

</ref>\[8\]，[王颖](../Page/王颖.md "wikilink")[导演](../Page/导演.md "wikilink")，第55届[西班牙圣塞巴斯蒂安电影节上夺得最佳电影金贝壳奖](../Page/西班牙.md "wikilink")，飾演男主角的
Henry O 获得最佳男演员银贝壳奖\[9\]。

  - 《The Princess of Nebraska》(2007) <ref>

</ref>[王颖](../Page/王颖.md "wikilink")[导演](../Page/导演.md "wikilink")。

## 評論

[英國](../Page/英國.md "wikilink")[觀察家報](../Page/觀察家報.md "wikilink")
Stephanie Merritt：

[英國](../Page/英國.md "wikilink")[獨立報Ian](../Page/獨立報.md "wikilink")
Thomson：

[旧金山纪事报吴帆](../Page/旧金山纪事报.md "wikilink"):

## 家庭

出生于北京，父亲是核子物理學者，母亲是教师\[10\]。已婚，育有二子\[11\]。

## 外部链接

  - [正式网站](http://www.yiyunli.com/)

## 參考資料

[Category:美国华人作家](../Category/美国华人作家.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:艾奧瓦大學校友](../Category/艾奧瓦大學校友.md "wikilink")
[Category:爱荷华作家工作坊校友](../Category/爱荷华作家工作坊校友.md "wikilink")
[Category:北京四中校友](../Category/北京四中校友.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Yi翊](../Category/李姓.md "wikilink")
[Category:欧·亨利奖获得者](../Category/欧·亨利奖获得者.md "wikilink")

1.

2.

3.
4.
5.
6.

7.
8.

9.

10.
11.