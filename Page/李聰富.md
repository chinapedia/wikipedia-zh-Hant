**李聰富**（1972年10月13日－）為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[時報鷹](../Page/時報鷹.md "wikilink")，守備位置為[外野手](../Page/外野手.md "wikilink")，後來因為[職棒簽賭案而遭到](../Page/職棒簽賭案.md "wikilink")[中華職棒終身禁賽](../Page/中華職棒.md "wikilink")。

## 經歷

  - [屏東縣復興國小少棒隊](../Page/屏東縣.md "wikilink")
  - [屏東縣](../Page/屏東縣.md "wikilink")[美和中學青少棒隊](../Page/美和中學青少棒隊.md "wikilink")
  - [屏東縣美和中學青棒隊](../Page/屏東縣.md "wikilink")
  - [台灣電力棒球隊](../Page/台灣電力公司.md "wikilink")
  - [時報鷹成棒隊](../Page/時報鷹.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")（1993年－1997年）

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 打數   | 安打  | 全壘打 | 打點  | 盜壘 | 四死 | 三振  | 壘打數 | 雙殺打 | 打擊率   |
| ----- | -------------------------------- | --- | ---- | --- | --- | --- | -- | -- | --- | --- | --- | ----- |
| 1993年 | [時報鷹](../Page/時報鷹.md "wikilink") | 88  | 284  | 75  | 12  | 33  | 0  | 15 | 58  | 132 | 4   | 0.264 |
| 1994年 | [時報鷹](../Page/時報鷹.md "wikilink") | 49  | 118  | 30  | 2   | 8   | 8  | 0  | 29  | 43  | 2   | 0.254 |
| 1995年 | [時報鷹](../Page/時報鷹.md "wikilink") | 82  | 280  | 75  | 7   | 50  | 28 | 2  | 52  | 116 | 10  | 0.268 |
| 1996年 | [時報鷹](../Page/時報鷹.md "wikilink") | 86  | 256  | 80  | 9   | 51  | 19 | 2  | 61  | 124 | 5   | 0.313 |
| 1997年 | [時報鷹](../Page/時報鷹.md "wikilink") | 46  | 159  | 40  | 6   | 19  | 12 | 2  | 36  | 65  | 6   | 0.252 |
| 合計    | 5年                               | 351 | 1097 | 300 | 36  | 161 | 6  | 82 | 236 | 480 | 27  | 0.273 |

## 外部連結

[C](../Category/李姓.md "wikilink") [L](../Category/在世人物.md "wikilink")
[L](../Category/1972年出生.md "wikilink")
[L](../Category/台灣棒球選手.md "wikilink")
[L](../Category/時報鷹隊球員.md "wikilink")
[L](../Category/中華成棒隊球員.md "wikilink")
[L](../Category/美和中學青少棒隊.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")