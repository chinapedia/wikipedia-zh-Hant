**李臨秋**（），是一位出身[日治臺灣](../Page/日治台灣.md "wikilink")[臺北的](../Page/臺北.md "wikilink")[流行音樂](../Page/流行音樂.md "wikilink")[作詞者](../Page/作詞人.md "wikilink")，最有名的作品為《[望春風](../Page/望春風.md "wikilink")》。

## 生平

李臨秋父親本為富商，後因故家道中落。1924年，李臨秋受親戚介紹進入[永樂町](../Page/永樂町.md "wikilink")[永樂座戲院擔任職員](../Page/永樂座.md "wikilink")。1934年，他以閩南語為兩部上海電影《懺悔》、《倡門賢母》填了兩首同名廣告歌曲的詞。雖然該歌詞只是為了吸引民眾前來觀賞電影，不過如同性質的《[桃花泣血記](../Page/桃花泣血記_\(電影配樂\).md "wikilink")》相同，頗受歡迎。之後，他再接再厲，為知名歌曲《望春風》作詞，不但成為他的代表作，也成為台灣最受歡迎的歌謠之一，迄今仍為人傳唱。

除了《望春風》之外，他替上海電影歌曲作的《[一個紅蛋](../Page/一個紅蛋.md "wikilink")》（描寫為女性申冤）、《人道》等廣告歌曲亦相同極受歡迎。除此，他另外所作的《[四季紅](../Page/四季紅.md "wikilink")》，《[補破網](../Page/補破網.md "wikilink")》等均為知名歌曲。不過因為戰後遷徙至台灣的[中華民國政府並不重視台語歌曲](../Page/中華民國.md "wikilink")，這些歌曲的作詞者常誤列為「佚名」。\[1\]歌曲的作詞作曲者記載為私人的商業行為政府並不介入。
[大稻埕公園李臨秋銅像.jpg](https://zh.wikipedia.org/wiki/File:大稻埕公園李臨秋銅像.jpg "fig:大稻埕公園李臨秋銅像.jpg")
1960年代，他就職的永樂戲院被拆後，他就退休在家不再創作。僅於1977年發表《相思海》。這首歌，也是他的最後知名遺作。\[2\]

## 家族

李臨秋有六個兒子\[3\]。\[4\]

## 作品

李臨秋傳世知名作品如下：

  - 《懺悔》（1932年）：[蘇桐作曲](../Page/蘇桐.md "wikilink")
  - 《倡門賢母》（1932年）：蘇桐作曲
  - 《[望春風](../Page/望春風.md "wikilink")》（1933年）：[鄧雨賢作曲](../Page/鄧雨賢.md "wikilink")
  - 《[一個紅蛋](../Page/一個紅蛋.md "wikilink")》（1933年）
  - 《人道》（1933年）
  - 《[四季紅](../Page/四季紅.md "wikilink")》（1935年）：鄧雨賢作曲
  - 《四時春》
  - 《對花》（1938年）：鄧雨賢作曲\[5\]
  - 《[補破網](../Page/補破網.md "wikilink")》（1948年）：[王雲峰作曲](../Page/王雲峰.md "wikilink")
  - 《相思海》（1977年）

[李臨秋故居.jpg](https://zh.wikipedia.org/wiki/File:李臨秋故居.jpg "fig:李臨秋故居.jpg")
此外尚有《瓶中花》、《一夜差錯》、《野玫瑰》、《送君詞》、《我愛你》、《閒花嘆》、《不敢叫》、《漂浪花》、《心酸酸》、《水蓮花》、《一夜恨》、《織布歌》、《小陽春》、《半暝行》、《飄風夜花》、《後悔前非》、《月照窗》、《想舊情》、《幽谷蘭》、《學海一孤舟》、《冤獄》、《夢中寄相思》、
《運呀！命呀！》、《新婚之夜》、《讚揚雪梅》、《浮雲》、《桃花恨》及《江上清風》等作品。\[6\]

## 參考文獻

## 相關條目

  - 活躍於臺灣日治時期（20世紀）重要的臺語流行音樂人（包含作詞人、作曲人、歌手），有：[林清月](../Page/林清月.md "wikilink")、[王雲峰](../Page/王雲峰_\(作曲家\).md "wikilink")、[許丙丁](../Page/許丙丁.md "wikilink")、[鄧雨賢](../Page/鄧雨賢.md "wikilink")、[陳君玉](../Page/陳君玉.md "wikilink")、[姚讚福](../Page/姚讚福.md "wikilink")、[江中清](../Page/江中清.md "wikilink")、**李臨秋**、[蘇桐](../Page/蘇桐.md "wikilink")、[周添旺](../Page/周添旺.md "wikilink")、[陳秋霖](../Page/陳秋霖.md "wikilink")、[陳水柳](../Page/陳水柳.md "wikilink")、[陳達儒](../Page/陳達儒.md "wikilink")、[郭玉蘭](../Page/郭玉蘭.md "wikilink")（又名雪蘭，女歌手）、[林氏好](../Page/林氏好.md "wikilink")（女歌手）、[純純](../Page/純純_\(歌手\).md "wikilink")（女歌手）、[愛愛](../Page/愛愛_\(歌手\).md "wikilink")（女歌手）、[吳晉淮](../Page/吳晉淮.md "wikilink")、[呂泉生](../Page/呂泉生.md "wikilink")、[吳成家](../Page/吳成家.md "wikilink")、[那卡諾](../Page/那卡諾.md "wikilink")、[許石](../Page/許石.md "wikilink")、[楊三郎](../Page/楊三郎_\(作曲家\).md "wikilink")、[葉俊麟](../Page/葉俊麟.md "wikilink")、[洪一峰等人](../Page/洪一峰.md "wikilink")。

## 延伸閱讀

  - 《李臨秋與望春風的年代》，黃信彰，台北市文獻委員會，2009年4月1日

[L](../Category/台灣作詞家.md "wikilink")
[李](../Category/閩南裔臺灣人.md "wikilink")
[L](../Category/李姓.md "wikilink")
[Category:台灣閩南語文學作家](../Category/台灣閩南語文學作家.md "wikilink")

1.  [這些人與那些年:
    ─台灣70處名人故居](https://books.google.com.tw/books?id=-zkdAwAAQBAJ&pg=PA115)，第115頁，行遍天下記者群，宏碩文化事業股份有限公司，2013-02-21
2.
3.  [台語很優美](http://blog.udn.com/novalyne/2965187?f_ORDER_BY=DESC&pno=0&#reply_list),[網路城邦](../Page/網路城邦.md "wikilink"),2009.
4.
5.  [李臨秋，高雄市立空中大學教學網站](http://www2.ouk.edu.tw/wester/composer/Chinese/composer05.htm)

6.  [吳國禎，《李臨秋作品之詞彙使用情形試探》](http://www.gec.ttu.edu.tw/lilinqiu/images/article/05.pdf)