**李茂貞**（），原名**宋文通**，深州博野（今[河北](../Page/河北省.md "wikilink")[蠡縣](../Page/蠡縣.md "wikilink")）人。[唐末](../Page/唐朝.md "wikilink")[藩鎮之一](../Page/藩鎮.md "wikilink")，[五代時期](../Page/五代.md "wikilink")[岐國君主](../Page/岐.md "wikilink")。

## 生平

宋文通祖父宋铎，祖母张氏；父宋端，母卢氏。

### 早期迅速擴張

[唐僖宗](../Page/唐僖宗.md "wikilink")[乾符年間](../Page/乾符.md "wikilink")（874年－879年），宋文通時為鎮州（今河北[正定](../Page/正定县.md "wikilink")，成德節度首邑）轄下博野軍士卒，派駐守衛京師[長安](../Page/長安.md "wikilink")（今[陝西](../Page/陕西省.md "wikilink")[西安](../Page/西安市.md "wikilink")），後遷鳳翔（今陝西[鳳翔](../Page/鳳翔府.md "wikilink")），曾擊敗[黃巢部將](../Page/黃巢.md "wikilink")[尚讓](../Page/尚讓.md "wikilink")，因功升為神策軍指揮使。他任右神策统军时，为诸军所恨，当权宦官[田令孜想趁召见之机杀之](../Page/田令孜.md "wikilink")，但见面后却高兴地养他为子，起名**田彦宾**。[光啟元年](../Page/光啟.md "wikilink")（885年），河東[節度使](../Page/節度使.md "wikilink")[李克用進軍](../Page/李克用.md "wikilink")[關中擊潰靜難節度使](../Page/關中.md "wikilink")[朱玫](../Page/朱玫_\(唐朝\).md "wikilink")、鳳翔節度使[李昌符](../Page/李昌符_\(凤翔节度使\).md "wikilink")，僖宗逃出京師，朱玫欲挾持僖宗，宋文通時為神策軍扈蹕都將，率軍抵擋朱玫手下大將[王行瑜之追兵](../Page/王行瑜.md "wikilink")。不久，朱玫亂平，宋文通因功受任命為武定節度使，為僖宗賜姓名為李茂貞，[字正臣](../Page/表字.md "wikilink")，编入大郑王（[李元懿](../Page/李元懿.md "wikilink")）房宗籍。光啟三年（887年），擊殺李昌符，再因功拜鳳翔節度使。

[唐昭宗](../Page/唐昭宗.md "wikilink")[大順元年](../Page/大順_\(唐朝\).md "wikilink")（890年），李茂貞受封為**隴西郡王**。[景福元年](../Page/景福_\(唐朝\).md "wikilink")（892年），攻克[宦官](../Page/宦官.md "wikilink")[楊復恭的根據地興元府](../Page/楊復恭.md "wikilink")（今陝西[漢中](../Page/漢中.md "wikilink")），遂兼併山南西道，成為關中一帶最具實力的藩鎮，此後逐漸驕傲自大，因昭宗以放弃凤翔作为让其领山南西道的条件而上表直斥昭宗和宰相。景福二年（893年），因昭宗對李茂貞的跋扈不能容忍，於是命嗣覃王[李嗣周率新组建的禁军討伐](../Page/李嗣周.md "wikilink")，但禁军反為李茂貞及[静难节度使](../Page/邠宁节度使.md "wikilink")[王行瑜所敗](../Page/王行瑜.md "wikilink")。李茂贞、王行瑜指宰相[杜让能为自己与昭宗失和的祸首](../Page/杜让能.md "wikilink")，迫使昭宗诏贬杜让能，李茂贞、王行瑜仍不满，昭宗只得赐死杜让能。

### 中期發展受挫

[乾寧二年](../Page/乾寧.md "wikilink")（895年），護國節度使（河中節度使）[王重盈去世](../Page/王重盈.md "wikilink")，王重盈侄[王珂与王重盈子](../Page/王珂_\(唐朝\).md "wikilink")[王珙爭位](../Page/王珙.md "wikilink")，王珂為李克用之婿，故李克用為其請求唐政府任命為河中節度使；而李茂貞與王行瑜、匡國節度使[韓建支持王珙](../Page/韓建.md "wikilink")，但為昭宗所拒，三帥遂聯軍進京挾持昭宗，迫使昭宗贬斥宰相[韦昭度](../Page/韦昭度.md "wikilink")、[李磎](../Page/李磎.md "wikilink")，并不经昭宗同意杀害二相，更图谋废帝另立皇兄吉王[李保](../Page/李保.md "wikilink")。不久，李克用率軍[勤王](../Page/勤王.md "wikilink")，敗聯軍，殺王行瑜，奪回昭宗，李茂貞的勢力第一次受挫，惟昭宗担心李克用无人可制而命其停止讨伐李茂贞和韩建。待李克用退兵後，李茂貞復驕橫如故，再度佔據河西州縣，夺取静难军、泾原军等。

乾寧三年（896年），李茂貞藉口昭宗命宗室諸親王建立軍隊一事，乃為了攻己，因此進軍京師，昭宗出奔華州（今陝西[華縣](../Page/華縣.md "wikilink")），依韓建。李茂貞的鳳翔軍進入長安後，宮室、房舍全部付之一炬。

數年後，昭宗歸長安，李茂貞與唐政府和好，並於[光化四年](../Page/光化.md "wikilink")（901年）被進封為[尚书令](../Page/尚书令.md "wikilink")、[侍中](../Page/侍中.md "wikilink")、[岐王](../Page/岐王.md "wikilink")。同年稍後，宣武節度使[朱全忠進軍京師](../Page/朱全忠.md "wikilink")，李茂貞有[挾天子以令諸侯之心](../Page/挟天子以令诸侯.md "wikilink")，遂脅持昭宗至鳳翔，并迫使昭宗嫡女[平原公主下嫁自己的儿子](../Page/平原公主_\(唐昭宗\).md "wikilink")[李继侃](../Page/李繼侃.md "wikilink")。但朱全忠不久兵圍鳳翔，鳳翔被圍困甚久，糧食不繼，李茂貞於是在[天復三年](../Page/天复_\(唐朝\).md "wikilink")（903年）向朱全忠請和，歸還昭宗，辞尚书令，仍为中书令，并送还平原公主。而經此一役，李茂貞所據[秦嶺以南州縣為西川節度使](../Page/秦嶺.md "wikilink")[王建趁機占領](../Page/王建_\(前蜀\).md "wikilink")，關中州縣亦為朱全忠所併，實力因此受到重挫，以後亦無法恢復。但王建在判官[冯涓建议下](../Page/冯涓.md "wikilink")，为免与强大的中原政权相争而没有趁机消灭李茂贞，而是将其保存下来作为屏障，还与其盟好结亲。

### 晚期力圖自保

[唐哀帝](../Page/唐哀帝.md "wikilink")[天祐四年](../Page/天祐_\(唐朝\).md "wikilink")（907年）朱全忠篡唐，建立[後梁](../Page/後梁.md "wikilink")。李茂貞仍用昭宗天复、哀帝天祐[年號](../Page/年號.md "wikilink")，以示與[後梁對抗](../Page/後梁.md "wikilink")，但亦開岐王府，設置百官，以其所居為宮殿，其[妻稱](../Page/妻.md "wikilink")-{[皇后](../Page/皇后.md "wikilink")}-，各種儀示其實都跟[皇帝一樣](../Page/皇帝.md "wikilink")。然而實力大不如前，后梁大将[刘知俊来投时](../Page/刘知俊.md "wikilink")，他已因地盘狭小无法任其为节度使，后来才任其为[泾原节度使](../Page/泾原节度使.md "wikilink")。李茂贞联合养子静难节度使[李继徽与刘知俊对](../Page/李继徽.md "wikilink")[朔方军和](../Page/朔方节度使.md "wikilink")[定难军的军事行动也都无果](../Page/定难节度使.md "wikilink")。天复十一年（911年），李茂贞与王建建立的[前蜀冲突](../Page/前蜀.md "wikilink")，虽由从子[李继崇和刘知俊取得青泥岭之战的胜利](../Page/李继崇.md "wikilink")，但仍被击退。十五年（915年），李继徽被儿子[李彦鲁毒死](../Page/李彦鲁.md "wikilink")，李继徽养子[李保衡又杀李彦鲁并以静难军投降后梁](../Page/李保衡.md "wikilink")，年末天雄军亦被前蜀攻占，李继崇、刘知俊相继投降前蜀。唐亡後的十餘年間，李茂貞屢為後梁、前蜀所敗，將叛城降，疆域日蹙。

岐國天祐二十年（923年），[後唐滅後梁](../Page/後唐.md "wikilink")，李茂貞面對此一強大的新興勢力，無法繼續並肩稱雄，於是在次年（924年）向後唐稱臣，李存勗則將李茂貞改封秦王，具有獨立性質的岐王國因此消失，數月後李茂貞去世，[諡忠敬王](../Page/谥号.md "wikilink")（秦忠敬王）。其子[李从曮继位](../Page/李繼曮.md "wikilink")。

## 評價

李茂貞事母至孝，母親去世時，哀慟不已，聽到的人皆有所稱讚。又很有智略，軍旅之事尤其過目不忘。李茂貞個性仁慈寬大，所以[士卒都能服從](../Page/士卒.md "wikilink")，但是也因此軍紀敗壞。在朱全忠擊滅各大小藩鎮，席捲[中原並建立後梁之際](../Page/中原.md "wikilink")，李茂貞雖實力日衰，但因其地處一隅而能倖免，不僅曾[稱孤道寡](../Page/君主.md "wikilink")，本人又能善終，實屬唐末以來割據的藩鎮[軍閥中較幸運的一位](../Page/軍閥.md "wikilink")。

## 家庭

### 妻

  - [刘皇后](../Page/刘皇后_\(李茂贞\).md "wikilink")

### 子女

  - 儿子
      - [李繼曮](../Page/李從曮.md "wikilink")，第六子\[1\]，926年改李從曮
      - [李繼昶](../Page/李從昶.md "wikilink")，926年改[李從昶](../Page/李從昶.md "wikilink")
      - [李繼照](../Page/李從照.md "wikilink")，926年改[李從照](../Page/李從照.md "wikilink")
      - [李繼暐](../Page/李繼暐.md "wikilink")
      - [李繼侃或李侃](../Page/李繼侃.md "wikilink")，902年一度作[宋侃](../Page/宋侃.md "wikilink")，903年娶妻[平原公主](../Page/平原公主_\(唐昭宗\).md "wikilink")。改姓宋是为避与平原公主（李姓）结婚触犯[同姓不婚的禁忌](../Page/同姓不婚.md "wikilink")。另有说其本为李茂贞的养子。
      - 李某
      - [李继崇](../Page/李继崇.md "wikilink")，陇西郡王，或作从子
  - 养子
      - [李繼臻](../Page/李繼臻.md "wikilink")
      - [李繼密](../Page/李继密.md "wikilink")，本名[王万弘](../Page/李继密.md "wikilink")，902年降王建并恢复本名，后自杀
      - [李繼鵬](../Page/李繼鵬.md "wikilink")，本名[阎-{珪}-](../Page/閻珪.md "wikilink")，895年为李茂贞所诛
      - [李繼顒](../Page/李繼顒.md "wikilink")，895年为[王宗侃所杀](../Page/王宗侃.md "wikilink")
      - [李繼雍](../Page/李繼雍.md "wikilink")
      - [李繼徽](../Page/李繼徽.md "wikilink")，本名[楊崇本](../Page/楊崇本.md "wikilink")，901年降朱全忠并恢复本名，904年重归李茂贞并改回李继徽，914年被子[李彦鲁毒杀](../Page/李彦鲁.md "wikilink")
      - [李繼昭](../Page/符道昭.md "wikilink")，本名[符道昭](../Page/符道昭.md "wikilink")，902年降朱全忠
      - [李繼瑭](../Page/李繼瑭.md "wikilink")
      - [李繼寧](../Page/李繼寧.md "wikilink")，897年被王建俘虏
      - [李繼溥](../Page/李繼溥.md "wikilink")，897年降王建
      - [李繼筠](../Page/李繼筠.md "wikilink")，903年被李茂贞所诛
      - [李繼忠](../Page/李繼忠.md "wikilink")
      - [李繼鐐](../Page/李繼鐐.md "wikilink")，902年被朱全忠俘虏
      - [李繼欽](../Page/李繼欽.md "wikilink")
      - [李繼直](../Page/李繼直.md "wikilink")，909年被[韩逊所杀](../Page/韩逊.md "wikilink")
      - [李繼夔](../Page/李繼夔.md "wikilink")
      - [李繼岌](../Page/桑弘志.md "wikilink")，本名[桑弘志](../Page/桑弘志.md "wikilink")，916年降王建并恢复本名
      - [李繼陟](../Page/李繼陟.md "wikilink")
      - [李繼诲](../Page/李繼诲.md "wikilink")，本名[周承诲](../Page/周承诲.md "wikilink")，903年被李茂贞所诛
      - [李繼宠](../Page/李繼宠.md "wikilink")，903年降朱全忠
  - 女儿，李茂贞墓志记五女，其妻刘氏墓志记三女。两位墓志记载的女儿情况并不相同，或因墓志有误，或因部份女兒非劉氏所出，或因女儿[改嫁](../Page/改嫁.md "wikilink")，已无法考证。
      - 长女，出嫁柳氏
      - 次女，出嫁卢氏
      - 又次女，出嫁卢氏
      - 又次女，出嫁郭氏
      - 又次女，出嫁路氏
      - 刘氏长女，出嫁卢氏
      - 刘氏次女，出嫁凤翔节度判官韩昉
      - 刘氏三女，出嫁凤翔节度推官张居逊

## 参考文献

[Category:岐](../Category/岐.md "wikilink")
[L李](../Category/武定軍節度使.md "wikilink")
[L李](../Category/鳳翔節度使.md "wikilink")
[L李茂贞](../Category/五代十国君主.md "wikilink")
[L](../Category/山南西道節度使.md "wikilink")
[Category:唐朝异姓郡王](../Category/唐朝异姓郡王.md "wikilink")
[Category:唐朝赐李姓及名字者](../Category/唐朝赐李姓及名字者.md "wikilink")
[L李](../Category/保定人.md "wikilink") [M茂](../Category/李姓.md "wikilink")
[Category:諡忠敬](../Category/諡忠敬.md "wikilink")

1.  《五代史补》：“李曮，岐王之子，昆仲间第六，官至中书令，世谓之六令公。”