**李锡**（이석，1941年8月30日－），幼名李海锡，本貫[全州李氏](../Page/全州李氏.md "wikilink")，是[大韓帝國末代皇帝高宗](../Page/大韓帝國.md "wikilink")[李熙的孙子](../Page/朝鮮高宗.md "wikilink")、义亲王[李堈第十子](../Page/李堈.md "wikilink")，生母是側室洪貞順（홍정순）。

在[全州大學擔任历史学的客座教授](../Page/全州大學.md "wikilink")，同時是現任[全州李氏宗親會會長](../Page/全州李氏宗親會.md "wikilink")。[紐約時報在](../Page/紐約時報.md "wikilink")2006年稱其為韓國皇室「最後的王位覬覦者」\[1\]
，然而全州李氏後裔從來沒有認可過如此稱謂。\[2\]

## 生平

### 教育

  - 首尔昌庆小学毕业
  - 首尔京东中学毕业
  - 首尔京东高中毕业
  - 韩国外国语大学西班牙语系[学士毕业](../Page/學士.md "wikilink")（待求证，有来源称其被迫退学）

### 经历

## 參考來源

## 外部链接

  - [대한제국 의친왕 숭모회](http://cafe.daum.net/daehan815/)
  - [우리 황실 사랑회](http://www.sihk.org/)
  - [황실 보존 국민 연합회](http://cafe.daum.net/epna)
  - [전주이씨 대동종약원](http://www.rfo.co.kr/)
  - [Daum 데이터 정보](http://100.daum.net/encyclopedia/view/33XXXXX18473)

[Category:朝鮮王朝家族](../Category/朝鮮王朝家族.md "wikilink")
[L](../Category/韓國人.md "wikilink")
[L](../Category/全羅北道出身人物.md "wikilink")

1.  "[A Prince Nestled Once More in Korea's
    Embrace](https://www.nytimes.com/2006/05/20/world/20yiseok.html)",
    *The New York Times*, May 20, 2006.
2.  Choi Sung-Un, "[Reviving
    Joseon](http://www.investkorea.org/InvestKoreaWar/work/journal/content/content_print.jsp?code=4670408)
    ", *Invest Korea Journal*, Mar–Apr 2010.