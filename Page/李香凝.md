**李香凝**（，），武打巨星[李小龙之女](../Page/李小龙.md "wikilink")\[1\]，電影演員。

## 生平

李香凝出生於[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")，祖籍[廣東](../Page/廣東.md "wikilink")[順德](../Page/順德.md "wikilink")。

1973年李小龍去世時，她只有4歲，對於[父親的印象都是後来從](../Page/父親.md "wikilink")[照片和](../Page/照片.md "wikilink")[電影以及](../Page/電影.md "wikilink")[媽媽](../Page/媽媽.md "wikilink")、哥哥[李國豪的回憶中得到](../Page/李國豪_\(演員\).md "wikilink")。她的身份使她有一定的壓力，所以在[美國上大學時選讀了演藝專業](../Page/美國.md "wikilink")，並在父親生前好友的幫助下苦練武功。在[洛杉磯電視台做過短期節目主持之後](../Page/洛杉磯.md "wikilink")，便飛回[香港參加由](../Page/香港.md "wikilink")[嘉禾公司投資的](../Page/嘉禾公司.md "wikilink")《[渾身是膽](../Page/渾身是膽.md "wikilink")》的拍攝工作，這也是她第一部[動作片](../Page/動作片.md "wikilink")。

## 參考

## 外部連結

  -
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:美裔混血兒](../Category/美裔混血兒.md "wikilink")
[X](../Category/李姓.md "wikilink")
[Category:杜蘭大學校友](../Category/杜蘭大學校友.md "wikilink")
[Category:李小龍](../Category/李小龍.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:美國電視主持人](../Category/美國電視主持人.md "wikilink")
[Category:美國搖滾歌手](../Category/美國搖滾歌手.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")

1.  ["Lee, Bruce, (1940–1973) Martial Arts Master and Film
    Maker"](http://www.historylink.org/essays/output.cfm?file_id=3999),
    HistoryLink.org, accessed January 1, 2011.