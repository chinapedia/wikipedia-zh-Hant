**李鵬飛**（，），人稱「飛哥」，祖籍[山東](../Page/山東.md "wikilink")，在抗日战争時期沦陷的[煙台出生](../Page/煙台.md "wikilink")，現在是[香港商界及政界的知名人士](../Page/香港.md "wikilink")，前[立法會及](../Page/香港立法會.md "wikilink")[行政局議員](../Page/香港行政會議.md "wikilink")，[自由黨首屆主席](../Page/香港自由黨.md "wikilink")，第九、十屆[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")；曾主持[香港有線電視](../Page/香港有線電視.md "wikilink")[財經資訊台節目](../Page/香港有線電視財經資訊台.md "wikilink")《[飛常政經](../Page/飛常政經.md "wikilink")》、[香港電台電視部節目](../Page/香港電台.md "wikilink")《[議事論事](../Page/議事論事.md "wikilink")》，及[now財經台之時事節目](../Page/now財經台.md "wikilink")《[大鳴大放](../Page/大鳴大放_\(電視節目\).md "wikilink")》，於2018年7月宣布退休，不再主持節目。\[1\]

## 早年生涯

李鵬飛生於[山東](../Page/山東.md "wikilink")[煙台](../Page/煙台.md "wikilink")，1940年居於[上海](../Page/上海.md "wikilink")，1954年到[香港定居](../Page/香港.md "wikilink")。早年在[美國](../Page/美國.md "wikilink")[密西根大學取得理學士](../Page/密西根大學.md "wikilink")（工程數學），並於1966年加入[洛歇公司成為測試工程主管](../Page/洛歇公司.md "wikilink")。1972年返回香港發展。李與太太育有兩子一女。

## 政治生涯

1982年李獲委任為[香港生產力促進局主席](../Page/香港生產力促進局.md "wikilink")。

1978年，李鵬飛被[港督](../Page/港督.md "wikilink")[麥理浩委任進入立法局](../Page/麥理浩.md "wikilink")，1985年被港督[尤德委任為行政局議員](../Page/尤德.md "wikilink")，1987年至1989年擔任[廣播事務管理局主席](../Page/廣播事務管理局#歷任主席.md "wikilink")。1988年更被任命為立法局首席議員。1990年2月6日凌晨，李鵬飛返回[沙田](../Page/沙田.md "wikilink")[九肚山寓所時](../Page/九肚山.md "wikilink")，在門前遭5名男子持刀斬傷頭部\[2\]，送院救治，幸傷勢不重。1990年6月，李鵬飛獲香港理工學院（現時的[香港理工大學](../Page/香港理工大學.md "wikilink")）頒授名譽博士學位\[3\]。1991年，李與[張鑑泉和](../Page/張鑑泉.md "wikilink")[周梁淑怡等人創立](../Page/周梁淑怡.md "wikilink")[啟聯資源中心](../Page/啟聯資源中心.md "wikilink")，抗衡[港同盟在立法局的影響力](../Page/港同盟.md "wikilink")。前保守黨黨魁[彭定康被任為港督後](../Page/彭定康.md "wikilink")，李鵬飛與一眾行政局議員立即總辭。1993年創立[自由黨](../Page/自由黨.md "wikilink")，成為創黨主席。自此與香港政府管治階層漸行漸遠。彭定康的普選成份較高的「三違反」政改方案不為中方接受，他與自由黨於是提出與香港政府對抗而[中共領導人接受的修正案](../Page/中共領導人.md "wikilink")「九四方案」，但被否決。\[4\]

李在[1995年香港立法局選舉中](../Page/1995年香港立法局選舉.md "wikilink")，贏得新界東北選區的席次。後來被任為[臨時立法會的議員](../Page/臨時立法會.md "wikilink")，在[1998年立法會選舉中](../Page/1998年立法會選舉.md "wikilink")，李的名單在新界東選區得3萬多票，在餘額計票下被當時寂寂無名的[前綫](../Page/前綫_\(1996年\).md "wikilink")[何秀蘭擊敗](../Page/何秀蘭.md "wikilink")，未能取得席次，於是他辭去了自由黨主席一職，自由黨亦在這次選舉的地方選區中全軍盡墨，自由黨直到2004年才再次取得立法會的地區直選席次，後來他亦退出自由黨，政治立場亦由[親建制派轉為溫和的](../Page/親建制派.md "wikilink")[泛民主派](../Page/泛民主派.md "wikilink")。

失去立法會席次後，他淡出香港政壇，間中替商業電台主持節目，直至2005年被「封咪」。

李於1998年至2008年擔任全國人大代表。

2003年政府擱置國家安全條例草案（[廿三條](../Page/香港基本法第二十三條.md "wikilink")）後，他以「血洗[中環](../Page/中環.md "wikilink")」形容若政府在當年7月9日二讀三讀該草案，場外將不免出現非常混亂的情況，甚至暴力衝突。

李曾長期擔任[香港電台論政節目](../Page/香港電台.md "wikilink")《[議事論事](../Page/議事論事.md "wikilink")》的節目主持，主要負責訪問嘉賓。

2013年4月24日，李鵬飛參與由前政務司司長[陳方安生牽頭成立的組織](../Page/陳方安生.md "wikilink")「[香港2020](../Page/香港2020.md "wikilink")」，討論[香港政治制度改革方案](../Page/香港政治制度改革.md "wikilink")，推動2017年落實行政長官普選及2020年立法會普選。\[5\]

李鵬飛自建制派轉爲親泛民後多次批評親建制派議員的言行，並在時任自由黨副主席[張宇人提出的二十元最低工資標準時表示後悔創黨並稱](../Page/張宇人.md "wikilink")：「當日唔創黨好過創黨！」

## 成綬三／陳壽山事件

成綬三／陳壽山事件發生於2004年5月，[成綬三在](../Page/成綬三.md "wikilink")5月18日晚上致電給李鵬飛，大談李某家人的近況，說他「太太很賢淑」、「女兒很漂亮」。結果翌日，李鵬飛認為中央政府想派人要脅他不要在電台發表反對中央和特區政府的言論，並即辭去電台節目主持的職務。

到了2004年5月27日，李鵬飛出席立法會的特別會議，在會上他公開了這件事，並表示他以為來電者是一位叫陳壽山的退休內地高官。事後，成綬三則公開表示他就是電話中的「陳壽山」，目的只是為了敍舊，並否認是由中央指派。\[6\]

但李鵬飛不同意成某的說法，並指他根本不認識成某。

2004年7月28日，李鵬飛在電台節目[風波裏的茶杯上責罵](../Page/風波裏的茶杯.md "wikilink")[俞琤](../Page/俞琤.md "wikilink")，但被俞琤稱為「棄兵曳甲的長者」。

## 榮譽

### 勳銜

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（1980）
  - [OBE](../Page/OBE.md "wikilink")（1982）
  - [CBE](../Page/CBE.md "wikilink")（1988）

### 榮譽博士

  - [香港理工學院工程學榮譽博士](../Page/香港理工學院.md "wikilink")（1990）
  - [香港中文大學法學榮譽博士](../Page/香港中文大學.md "wikilink")（1990）

## 名下馬匹

李鵬飛為[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，曾經擁有多匹馬，包括個人名下的山東名將、山東文豪、東海龍、草原團體名下的好好機會、草原勇士。

## 參考

{{-}}

[Category:第九届全国人大代表](../Category/第九届全国人大代表.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:前香港立法會議員](../Category/前香港立法會議員.md "wikilink")
[L](../Category/前香港立法局議員.md "wikilink")
[Category:前香港行政會議成員](../Category/前香港行政會議成員.md "wikilink")
[Category:香港自由黨前成員](../Category/香港自由黨前成員.md "wikilink")
[Category:香港泛民主派人士](../Category/香港泛民主派人士.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")
[Category:香港時事評論員](../Category/香港時事評論員.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[L](../Category/香港培英中學校友.md "wikilink")
[L李](../Category/烟台人.md "wikilink")
[P](../Category/李姓.md "wikilink")
[L](../Category/CBE勳銜.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:逃港者](../Category/逃港者.md "wikilink")
[Category:洛克希德](../Category/洛克希德.md "wikilink")

1.  [不再主持《議事論事》
    李鵬飛寄語林鄭﹕畀下屬發揮](https://news.mingpao.com/pns/dailynews/web_tc/article/20180716/s00002/1531678741608)
2.
3.
4.
5.  [「香港2020」成立
    望年內提政改方案](http://hk.news.yahoo.com/%E9%A6%99%E6%B8%AF2020-%E6%88%90%E7%AB%8B-%E6%9C%9B%E5%B9%B4%E5%85%A7%E6%8F%90%E6%94%BF%E6%94%B9%E6%96%B9%E6%A1%88-210649997.html;_ylt=AmBUL2kRFBfoa65TvHMfEKarVsd_;_ylu=X3oDMTNsN2RsM3Z1BG1pdANUb3BTdG9yeSBGUARwa2cDMzk3ZTY5MmItODk3Ny0zMjQxLTg2OTYtZjk2YmNiNDhhMTQxBHBvcwMyBHNlYwN0b3Bfc3RvcnkEdmVyA2UxMjg4YTgwLWFkMjItMTFlMi1iZmJiLTMyNDE5MjEzMWFmNA--;_ylg=X3oDMTFxbXNsczBqBGludGwDaGsEbGFuZwN6aC1oYW50LWhrBHBzdGFpZAMEcHN0Y2F0A.aWsOiBnummlumggQRwdANwbWg-;_ylv=3)
6.