**村**是指[人類群居之地](../Page/人類.md "wikilink")，通常以一個[地理區或](../Page/地理.md "wikilink")[社區為範圍](../Page/社區.md "wikilink")；在[英文的定義中](../Page/英文.md "wikilink")，規模通常略大於[村鎮](../Page/村鎮.md "wikilink")，而小於[市鎮](../Page/市鎮.md "wikilink")，與[集鎮類似](../Page/集鎮.md "wikilink")。現代多作為[行政區劃單位](../Page/行政區劃.md "wikilink")。

## 中華民國

**村**（**-{里}-**）為[中華民國之第四級](../Page/中華民國.md "wikilink")[行政區劃名稱](../Page/中華民國行政區劃.md "wikilink")；亦是最基層之[地方自治單位](../Page/地方自治.md "wikilink")。隸屬於[區](../Page/區_\(中華民國\).md "wikilink")、[縣轄市](../Page/縣轄市.md "wikilink")、[鎮者稱為](../Page/鎮_\(中華民國\).md "wikilink")-{里}-；隸屬於[鄉者稱為村](../Page/鄉_\(中華民國\).md "wikilink")。村和-{里}-下還有「**鄰**」之編組。設「村（里）長」、「村（里）幹事」各一名，處理行政事務。村（里）長由村（里）民投票產生，村（里）幹事則為鄉鎮市區公所之行政公務員。

## 中華人民共和國

村（行政村）为中国大陆地方行政体系中，最小的基层自治单位。非行政区划意义上的村则称为自然村。一个行政村可能由数个较小的自然村组成；一些较大的自然村可能划分为若干行政村。

## 日本

在[日本](../Page/日本.md "wikilink")，「**村**」一詞原本即指[鄉村](../Page/鄉村.md "wikilink")，[明治時代後才成為行政區劃單位](../Page/明治時代.md "wikilink")。根據《[日本国憲法](../Page/日本国憲法.md "wikilink")》第92條和《地方自治法》，**村**（；亦稱為「**行政村**」）為[地方公共團體之一](../Page/地方政府.md "wikilink")，相當於中國與台灣的「[鄉](../Page/鄉.md "wikilink")」，隸屬於[都道府縣之下](../Page/都道府縣.md "wikilink")，與[市](../Page/市.md "wikilink")、[町同級](../Page/町.md "wikilink")。

## 韩国、朝鲜

朝鲜、韩国的农村基层政区称作里。

## 美国

村的含义在美国各州定义不同。在部分州，村为正式的基层政区。

## 參見

  - [鄉村](../Page/鄉村.md "wikilink")
  - [行政村](../Page/行政村.md "wikilink")

{{-}}

[Category:依類型劃分的聚居地](../Category/依類型劃分的聚居地.md "wikilink")
[村落](../Category/村落.md "wikilink")
[Category:农村](../Category/农村.md "wikilink")