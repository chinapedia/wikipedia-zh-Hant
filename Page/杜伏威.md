**杜伏威**（），名**尧**，字**伏威**，[以字行](../Page/以字行.md "wikilink")\[1\]，[齐州](../Page/齐州_\(北魏\).md "wikilink")[章丘县](../Page/章丘县.md "wikilink")（今[山东省](../Page/山东省.md "wikilink")[济南市](../Page/济南市.md "wikilink")[章丘区](../Page/章丘区.md "wikilink")）人。[隋朝末期农民起义军领导者之一](../Page/隋朝.md "wikilink")。

## 生平

自幼家贫，惯于偷盗，與[辅公祏是刎颈之交](../Page/辅公祏.md "wikilink")。辅公祏常从姑家里偷羊送给杜伏威吃。因盗案案发，官府追捕，两人落草为寇，时年十六。[大业九年](../Page/大业_\(年号\).md "wikilink")（613年）逃窜到长白山（今山东[章丘东北](../Page/章丘.md "wikilink")）地区，组织山贼，与辅公祏率众起义。后转战到[淮南](../Page/淮南.md "wikilink")，渐渐扩张势力，自称[将军](../Page/將軍_\(中國古代\).md "wikilink")，陆续合并[苗海潮部](../Page/苗海潮.md "wikilink")、[赵破阵部等起义军](../Page/赵破阵.md "wikilink")，势力大增，屯併[六合](../Page/六合县.md "wikilink")，威胁[江都](../Page/江都县.md "wikilink")（在今江苏省扬州市境），江都[留守派校尉](../Page/留守_\(官職\).md "wikilink")[宋颢前来镇压](../Page/宋颢.md "wikilink")，杜伏威用计将其引入芦苇荡中，放火将其烧死。

大业十二年（616年）七月，[隋炀帝派虎牙郎将](../Page/隋炀帝.md "wikilink")[公孙上哲率军前往鎮壓](../Page/公孙上哲.md "wikilink")，双方战于盐城（今江苏[盐城](../Page/盐城.md "wikilink")），公孙上哲军队被全歼。次年（617年）伏威又击败名將[陈稜率领的隋军](../Page/陈稜.md "wikilink")，乘胜破[高邮](../Page/高邮.md "wikilink")，之后在淮南的[历阳](../Page/历阳.md "wikilink")（今[安徽省](../Page/安徽省.md "wikilink")[和县](../Page/和县.md "wikilink")）自称[总管](../Page/总管.md "wikilink")，以辅公祏为[长史](../Page/长史.md "wikilink")，又合并江淮各部，占有江淮间广大地区。伏威善治军，能與将士同甘共苦，每次战斗，将士皆奋勇冲杀，争效死力。所获财物，皆赏将士；战死者，以其妻妾[殉葬](../Page/殉葬.md "wikilink")。故将士皆为他战，所向无敌。伏威又實施减赋税、废殉葬、惩贪污等措施，逐渐赢得了民众的信任。

大业十四年（618年），杜伏威向东都（今河南洛阳）越王[杨侗称臣](../Page/杨侗.md "wikilink")，獲封为楚王，拜东道[大总管](../Page/大总管.md "wikilink")。[唐高祖](../Page/唐高祖.md "wikilink")[武德二年](../Page/武德.md "wikilink")（619年），由于[唐朝实力强大](../Page/唐朝.md "wikilink")，便归安于唐朝，授[东南道](../Page/东南道.md "wikilink")[行台](../Page/行台.md "wikilink")[尚书令](../Page/尚书令.md "wikilink")、江淮以南安抚大使、[上柱国](../Page/上柱国.md "wikilink")，封吴王，[赐姓李](../Page/赐姓.md "wikilink")，预[宗正属籍](../Page/宗正.md "wikilink")，封其子杜德俊为山阳公。次年，击败[李子通](../Page/李子通.md "wikilink")，将根据地迁至江南的[丹阳](../Page/丹阳.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[南京](../Page/南京.md "wikilink")）。武德四年，唐軍攻圍[洛陽](../Page/洛陽.md "wikilink")，伏威遣兵助戰。

武德五年（622年），秦王[李世民鎮壓](../Page/李世民.md "wikilink")[河北](../Page/河北.md "wikilink")[劉黑闥](../Page/劉黑闥.md "wikilink")、[徐圓朗](../Page/徐圓朗.md "wikilink")，連下十餘城，淮北震動，七月，召杜伏威入朝，臨行前以辅公祏留守[丹陽](../Page/丹陽.md "wikilink")，将兵权交给右将军[王雄诞](../Page/王雄诞.md "wikilink")，辅公祏心生不满。武德六年（623年）春正月\[2\]，朝廷以伏威為[太子太保兼](../Page/太子太保.md "wikilink")[行台尚書令](../Page/行台尚書令.md "wikilink")，位在齐王[李元吉之上](../Page/李元吉.md "wikilink")，以示宠遇。同年八月，辅公祏伪称接到杜的命令起兵反唐，兵败被杀。此时距杜伏威和辅公祏共同起事已十三年。兵败后，辅公祏伪造的书信被发现，唐高祖不辨真假便除去杜伏威之官，并[籍没其家眷](../Page/籍没.md "wikilink")。武德六年三月廿七日（623年5月2日），杜伏威因为忧惧而死，虚岁四十一。贞观五年二月廿一日（631年3月29日），[唐太宗诏令以国公礼安葬杜伏威](../Page/唐太宗.md "wikilink")，派别将戴士文监护丧事，当年四月庚寅廿日己酉（631年5月26日）将杜伏威安葬于雍州万年县义善乡少陵原\[3\]。

## 其它

[武俠](../Page/武俠.md "wikilink")[小說](../Page/小說.md "wikilink")[作家](../Page/作家.md "wikilink")[黃易於其作品](../Page/黃易.md "wikilink")《[大唐雙龍傳](../Page/大唐雙龍傳.md "wikilink")》中，將杜伏威塑造成老謀深算、精明幹練而且武功非凡的黑道霸主、義軍領袖。他頭頂高冠，容貌古樸，性格凶殘，而且殺人如麻，唯獨對主角[寇仲及](../Page/寇仲.md "wikilink")[徐子陵有著複雜的感情](../Page/徐子陵.md "wikilink")，既討厭而又欣賞他們，起初為了「楊公寶庫」而脅持二人，考慮到方便在江湖上並行而迫令二人稱自己為爹，但杜伏威大勢已去後，雄心不再，反倒懷念二人之情，直接成為了二人的乾爹。外號「袖裡乾坤」，喜歡穿著寬袍大袖，兩袖裡暗藏尺許長的護臂，招式詭祕莫測，於小說中是當代高手之一。

## 参考资料

[category:章丘人](../Page/category:章丘人.md "wikilink")

[Category:隋朝民变领袖](../Category/隋朝民变领袖.md "wikilink")
[楚](../Category/隋朝异姓国王.md "wikilink")
[Category:隋朝大总管](../Category/隋朝大总管.md "wikilink")
[吴](../Category/唐朝异姓国王.md "wikilink")
[Category:唐朝行台尚书令](../Category/唐朝行台尚书令.md "wikilink")
[Category:唐朝安抚大使](../Category/唐朝安抚大使.md "wikilink")
[Category:唐朝上柱国](../Category/唐朝上柱国.md "wikilink")
[Category:唐朝太子太保](../Category/唐朝太子太保.md "wikilink")
[Category:唐朝赐李姓者](../Category/唐朝赐李姓者.md "wikilink")
[Category:唐朝非正常死亡人物](../Category/唐朝非正常死亡人物.md "wikilink")
[F伏](../Category/杜姓.md "wikilink")
[Category:大唐雙龍傳登場人物](../Category/大唐雙龍傳登場人物.md "wikilink")

1.

2.  《[旧唐书](../Page/旧唐书.md "wikilink")·本纪第一·高祖》（武德）六年春正月，吴王杜伏威为太子太保。二月辛亥......

3.