**杜如晦**（），字**克明**，[京兆郡](../Page/京兆郡.md "wikilink")[杜陵县](../Page/杜陵县.md "wikilink")（今[陝西省](../Page/陝西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")[長安区](../Page/长安区_\(西安市\).md "wikilink")）人，[唐朝初期大臣](../Page/唐朝.md "wikilink")。他是[李世民奪取](../Page/李世民.md "wikilink")[政權](../Page/政權.md "wikilink")、開創[貞觀之治中的主要謀臣之一](../Page/貞觀之治.md "wikilink")，深受李世民的重用。与[房玄龄人称](../Page/房玄龄.md "wikilink")“房谋杜断”。

[唐太宗时的](../Page/唐太宗.md "wikilink")[宰相](../Page/宰相.md "wikilink")：贞观二年（628年）正月至三年（629年）二月为[侍中](../Page/侍中.md "wikilink")，三年二月至十二月为尚书右仆射。

## 生平

杜如晦自少聪悟，好谈文史，是个典型的彬彬书生。在[隋朝](../Page/隋朝.md "wikilink")[大业年间起家为](../Page/大业.md "wikilink")[雍州从事](../Page/雍州.md "wikilink")，吏部侍郎高孝基很是器重他，赞扬他有应变之才，是栋梁之才。[隋炀帝前往](../Page/隋炀帝.md "wikilink")[江都游玩时](../Page/江都.md "wikilink")，代替王使君判留守事。秦王[李世民平定京城时](../Page/李世民.md "wikilink")，引为秦王府兵曹参军。当时的太子[李建成对杜如晦非常忌晦](../Page/李建成.md "wikilink")，他对齐王[李元吉说](../Page/李元吉.md "wikilink")：“秦王府中可惮之人，惟杜如晦与[房玄龄耳](../Page/房玄龄.md "wikilink")。”被外调出秦王府。经[房玄龄周旋](../Page/房玄龄.md "wikilink")，李世民又将他调回幕府。此后跟随李世民东征西讨，为其参谋军国大事，平定了[薛仁果](../Page/薛仁杲.md "wikilink")、[刘武周](../Page/刘武周.md "wikilink")、[王世充](../Page/王世充.md "wikilink")、[窦建德等诸路诸侯](../Page/窦建德.md "wikilink")。以功勋累迁陕东道大行台司勋郎中，封建平县男，食邑三百户。不久以本官兼任文学馆学士。

李世民的[天策府建立后](../Page/天策府.md "wikilink")，杜如晦任从事中郎，被画像纪念的天策府[十八学士中](../Page/十八学士.md "wikilink")，以杜如晦为首，其画像上的讚語为：“建平文雅，休有烈光。怀忠履义，身立名扬。”

[武德九年六月四日](../Page/武德.md "wikilink")[玄武门之变后](../Page/玄武门之变.md "wikilink")，李世民被立为皇太子，杜如晦任太子右庶子（唐书为左庶子，杜如晦碑为右庶子），不久迁任[兵部尚书](../Page/兵部尚书.md "wikilink")，进封**蔡国公**，赐实封一千三百户。

贞观二年，杜如晦以本官摄[侍中](../Page/侍中.md "wikilink")，兼[吏部尚书](../Page/吏部.md "wikilink")，总监[东宫兵马事务](../Page/东宫.md "wikilink")。

贞观三年，杜如晦代[长孙无忌为](../Page/长孙无忌.md "wikilink")[尚书右仆射](../Page/尚书右仆射.md "wikilink")，與左仆射[房玄齡共掌朝政](../Page/房玄齡.md "wikilink")，制定了唐朝各个部门的规模及典章，被人们赞为良相，史有「房謀杜斷」之稱\[1\]。同年冬天杜如晦因病辞职，唐太宗对他的病情极为关注，频频派人慰问，给他寻找名医和治病药物，到[贞观四年五月终因病重而去世](../Page/贞观.md "wikilink")，年仅四十六。唐太宗对他去世极为痛心，追赠他为[司空](../Page/司空.md "wikilink")，徙封**萊国公**，谥曰**成**（杜如晦碑载为谥曰**诚**）。

## 杜如晦碑

杜如晦碑，[唐太宗命](../Page/唐太宗.md "wikilink")[虞世南撰写碑文](../Page/虞世南.md "wikilink")，[欧阳询隶书](../Page/欧阳询.md "wikilink")。又称《赠司空杜如晦碑》。贞观四年立于醴泉县昭陵。
著录首见宋[赵明诚](../Page/赵明诚.md "wikilink")《金石录目》第五五九。

## 家庭

  - 参见[京兆杜氏](../Page/京兆杜氏.md "wikilink")

### 家世

  - 高祖：[杜秀](../Page/杜秀.md "wikilink")，北魏辅国将军，赠豫州刺史。
  - 曾祖：[杜皎](../Page/杜皎.md "wikilink")，北周仪同三司、武都郡守，赠开府仪同、大将军、遂州刺史。
  - 伯祖：[杜果](../Page/杜果.md "wikilink")，隋兵部尚书，义兴县公。
  - 祖父：[杜徽](../Page/杜徽.md "wikilink")，字晔，怀州长史。（据《金石录》杜如晦碑）
  - 父亲：[杜吒](../Page/杜吒.md "wikilink")，隋昌州长史。
  - 叔父：[杜淹](../Page/杜淹.md "wikilink")，安吉郡公。

### 弟

  - [杜楚客](../Page/杜楚客.md "wikilink")，后任魏王[李泰府中长史](../Page/李泰.md "wikilink")，拜工部尚书，掌管魏王泰府事。拥戴李泰与[李承-{乾}-争夺太子之位](../Page/李承乾.md "wikilink")，贞观十七年被免死废为庶人，寻授处化县令。

### 子

  - [杜構](../Page/杜構.md "wikilink")，承襲莱国公爵位，官至[刺史](../Page/刺史.md "wikilink")。後因弟弟杜荷的牽連而被流放嶺南，死於南方。
  - [杜荷](../Page/杜荷.md "wikilink")，娶唐太宗之女[城陽公主](../Page/城陽公主.md "wikilink")，赐爵襄阳郡公，贞观十七年，捲入皇太子[李承-{乾}-案被處斩](../Page/李承乾.md "wikilink")。

## 参考资料

[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")
[Category:唐朝国公](../Category/唐朝国公.md "wikilink")
[Category:西安人](../Category/西安人.md "wikilink")
[R如](../Category/京兆杜氏.md "wikilink")
[Category:諡成](../Category/諡成.md "wikilink")
[Category:唐朝尚书右仆射](../Category/唐朝尚书右仆射.md "wikilink")
[Category:凌烟阁二十四功臣](../Category/凌烟阁二十四功臣.md "wikilink")

1.  《旧唐书·房玄龄杜如晦传论》：世传太宗尝与文昭图事，则曰：“非如晦莫能筹之。”及如晦至焉，竟从玄龄之策也。盖房知杜之能断大事，杜知房之善建嘉谋。