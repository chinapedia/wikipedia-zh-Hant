**杜姓**是[中文的](../Page/中文.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第129位，2006年中国人口中杜姓人口排名第47位。

## 姓氏起源

  - 出自[祁姓](../Page/祁姓.md "wikilink")，以[邑為氏](../Page/邑.md "wikilink")。[周成王將唐杜氏移於杜城](../Page/周成王.md "wikilink")（在今[陝西省](../Page/陝西省.md "wikilink")[西安市東南](../Page/西安市.md "wikilink")），居者以地名「杜」為氏，據《[通志](../Page/通志.md "wikilink")．氏族略》云：杜氏亦日唐杜氏，祁姓。帝堯之後。
    建國於劉，為陶唐氏，裔孫劉累能擾龍，事孔甲。故在夏為御龍氏。在周為唐杜氏，成王滅唐。而封虞，乃遷唐氏於杜，是為杜伯。居杜城者為杜氏。

<!-- end list -->

  - 為[南北朝時](../Page/南北朝.md "wikilink")[鮮卑族姓氏所改](../Page/鮮卑族.md "wikilink")。據《[魏書](../Page/魏書.md "wikilink")．官氏志》所載，[北魏有代北三字姓獨孤渾氏](../Page/北魏.md "wikilink")，隨孝文帝遷都洛陽，
    改為漢字單姓杜氏。

<!-- end list -->

  -
## 名人

### 歷史人物

  - [杜周](../Page/杜周.md "wikilink")：西漢酷吏。
  - [杜延年](../Page/杜延年.md "wikilink")：杜周之子，西漢名臣，[京兆杜氏始祖之一](../Page/京兆杜氏.md "wikilink")。
  - [杜詩](../Page/杜詩_\(東漢官員\).md "wikilink")：東漢官員。
  - [杜篤](../Page/杜篤.md "wikilink")：東漢文人。
  - [杜畿](../Page/杜畿.md "wikilink")：東漢末及三國時曹魏官吏及將領。
  - [杜恕](../Page/杜恕.md "wikilink")：杜畿之子，三國時曹魏名臣。
  - [杜預](../Page/杜預.md "wikilink")：杜恕之子，西晉著名政治家、軍事家、學者。
  - [杜錫](../Page/杜錫.md "wikilink")：杜預之子，西晉尚書左丞。
  - [杜乂](../Page/杜乂.md "wikilink")：杜錫之子，西晉丹陽丞，著名美男子。
  - [杜陵陽](../Page/杜陵陽.md "wikilink")：杜乂之女，晉成帝司馬衍的皇后。
  - [杜如晦](../Page/杜如晦.md "wikilink")：唐朝名臣，與另一名臣[房玄齡並列](../Page/房玄齡.md "wikilink")。
  - [杜審言](../Page/杜審言.md "wikilink")：杜甫之祖父，唐朝詩人，「文章四友」的一員。
  - [杜甫](../Page/杜甫.md "wikilink")：杜審言之孫，唐朝著名詩人，有「詩聖」的美譽。
  - [杜黃裳](../Page/杜黃裳.md "wikilink")：「慘綠少年」之詞由來
  - [杜佑](../Page/杜佑.md "wikilink")：唐朝名臣，著有《[通典](../Page/通典.md "wikilink")》。
  - [杜環](../Page/杜環.md "wikilink")：杜佑之侄。
  - [杜牧](../Page/杜牧.md "wikilink")：杜佑之孫，唐朝詩人，與[李商隱齊名](../Page/李商隱.md "wikilink")。
  - [杜悰](../Page/杜悰.md "wikilink")：杜佑之孫，杜牧堂兄，官至宰相。
  - [杜荀鶴](../Page/杜荀鶴.md "wikilink")：唐末社會寫實詩人，與[皮日休等齊名](../Page/皮日休.md "wikilink")。
  - [杜秋娘](../Page/杜秋娘.md "wikilink")：唐朝歌妓。
  - [杜紅兒](../Page/杜紅兒.md "wikilink")：唐末官妓，能歌善舞，[羅虯以](../Page/羅虯.md "wikilink")《示紅兒詩》貽之。
  - [杜心五](../Page/杜心五.md "wikilink")：清末民初時期，中國武術大師。
  - [杜高智](../Page/杜高智.md "wikilink")：[越戰時期](../Page/越戰.md "wikilink")[南越陸軍](../Page/南越陸軍.md "wikilink")[將領](../Page/將領.md "wikilink")，在戰爭期間以軍事指揮能力和強硬風格著稱。

### 現代人物

  - [杜月笙](../Page/杜月笙.md "wikilink")：上海名流
  - [杜聿明](../Page/杜聿明.md "wikilink")：中華民國[軍事家](../Page/軍事家.md "wikilink")
  - [杜均衡](../Page/杜均衡.md "wikilink")：第1屆中華民國立法委員
  - [杜振榮](../Page/杜振榮.md "wikilink")：第3屆中華民國立法委員
  - [杜文卿](../Page/杜文卿.md "wikilink")：第5屆及第6屆中華民國立法委員
  - [杜聰明](../Page/杜聰明.md "wikilink")：[台灣醫學之父](../Page/台灣.md "wikilink")、台灣第一位[博士](../Page/博士.md "wikilink")。
  - [杜正勝](../Page/杜正勝.md "wikilink")：前[中華民國教育部部長](../Page/中華民國教育部.md "wikilink")（2004-2009）
  - [杜青林](../Page/杜青林.md "wikilink")：中國大陸[全國政協副主席](../Page/全國政協.md "wikilink")、[中共中央統戰部部長](../Page/中共中央統戰部.md "wikilink")
  - [杜本文](../Page/杜本文.md "wikilink")：香港前[市政局議員](../Page/市政局_\(香港\)#臨時市政局議員名單.md "wikilink")、前[東區區議員](../Page/東區區議會.md "wikilink")
  - [杜麗](../Page/杜麗.md "wikilink")：中國大陸[射擊運動員](../Page/射擊.md "wikilink")
  - [杜宇航](../Page/杜宇航.md "wikilink")：香港演員、知名[武術家](../Page/武術家.md "wikilink")
  - [杜琪峰](../Page/杜琪峰.md "wikilink")：香港電影導演、監製
  - [杜國威](../Page/杜國威.md "wikilink")：香港舞台劇編劇
  - [杜成义](../Page/杜成义.md "wikilink")：新加坡歌手，藝名「[阿杜](../Page/阿杜.md "wikilink")」
  - [杜詩梅](../Page/杜詩梅.md "wikilink")：臺灣藝人
  - [杜浚斌](../Page/杜浚斌.md "wikilink")：香港[新城電台DJ](../Page/新城電台.md "wikilink")
  - [杜海濤](../Page/杜海濤.md "wikilink")：中國大陸主持人
  - [杜江](../Page/杜江_\(演员\).md "wikilink")：中国大陆演员
  - [杜書伍](../Page/杜書伍.md "wikilink")：台灣聯強國際總裁
  - [杜少榛](../Page/杜少榛.md "wikilink")：台灣藝人
  - [杜德偉](../Page/杜德偉.md "wikilink")：香港歌手及演員。

杜雨露 ：中国著名演员 杜志国：中国著名演员 杜文泽：中国香港演员

### 虚构人物

  - [杜丽娘](../Page/杜丽娘.md "wikilink")：明代剧作家汤显祖代表作《牡丹亭》的女主人公。
  - [杜十娘](../Page/杜十娘.md "wikilink")：明清小说[警世通言中](../Page/警世通言.md "wikilink")《杜十娘怒沉百宝箱》的女主人公。杜泽羽

## 参见

  - [京兆杜氏](../Page/京兆杜氏.md "wikilink")
  - [杜陵杜氏](../Page/杜陵杜氏.md "wikilink")

[D杜](../Category/漢字姓氏.md "wikilink") [杜姓](../Category/杜姓.md "wikilink")
[Category:朝鮮語姓氏](../Category/朝鮮語姓氏.md "wikilink")