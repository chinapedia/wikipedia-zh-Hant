**杜尚别**（）是[中亞國家](../Page/中亞.md "wikilink")[塔吉克斯坦的](../Page/塔吉克斯坦.md "wikilink")[首都](../Page/首都.md "wikilink")，位於[北緯](../Page/北緯.md "wikilink")38.5度、[東經](../Page/東經.md "wikilink")68.8度，人口在2000年統計約有53萬6千人。又譯做**杜桑貝**、**杜夏貝**、**杜山比**。杜尚别在塔吉克語解作「星期一」，即指杜尚别著名的星期一市場。

杜尚别曾是Ibrahim
Bek（一位曾與[布尔什维克作戰過的塔吉克籍領導人](../Page/布尔什维克.md "wikilink")）的重要基地。1929年至1961年，杜尚别成為了[塔吉克苏维埃社会主义共和国的首都](../Page/塔吉克苏维埃社会主义共和国.md "wikilink")，并以[约瑟夫·斯大林的名字命名为斯大林纳巴德](../Page/约瑟夫·斯大林.md "wikilink")（意为“斯大林城”）。目前杜尚别有一所大學，也是塔吉克科學學院所在地。杜尚别周邊有[煤礦](../Page/煤.md "wikilink")、[鉛礦和](../Page/鉛.md "wikilink")[砷礦等矿产](../Page/砷.md "wikilink")。此外也是頂尖的[棉製](../Page/棉.md "wikilink")[纖維生產中心](../Page/纖維.md "wikilink")、杜尚别也產[絲綢](../Page/絲綢.md "wikilink")、衣服或皮革製品、食品和機器零件等。

1990年，杜尚貝發生暴亂，原因是反對政府把[亞美尼亞難民遷入城中](../Page/亞美尼亞.md "wikilink")。在1992年至1997年間，杜尚貝在[内战中被戰火破壞](../Page/塔吉克斯坦内战.md "wikilink")。

## 氣候

杜尚別屬於[溫帶大陸性氣候區](../Page/溫帶.md "wikilink")，由於受四週高山所包圍，導致杜尚別的年一溫差十分明顯。冬季寒冷並有雪，春秋兩季多雨潮濕，夏季相對乾燥和炎熱但有時午後至晚上間中會出現短暫強[對流雨天氣](../Page/對流雨.md "wikilink")。

## 姐妹城市

杜尚别共有15个[姐妹城市](../Page/姐妹城市.md "wikilink")。

  - [路沙卡](../Page/路沙卡.md "wikilink")（1966年）

  - [薩那](../Page/薩那.md "wikilink")（1967年6月25日）

  - [莫纳斯提尔](../Page/莫纳斯提尔.md "wikilink")（1967年11月24日）

  - [克拉根福](../Page/克拉根福.md "wikilink")（1972年）

  - [拉合爾](../Page/拉合爾.md "wikilink")（1976年9月15日）

  - [博爾德](../Page/博爾德_\(科羅拉多州\).md "wikilink")（1987年5月8日）

  - [马扎里沙里夫](../Page/马扎里沙里夫.md "wikilink")（1991年7月13日）

  - [罗伊特林根](../Page/罗伊特林根.md "wikilink")（1991年10月5日）

  - [聖彼得堡](../Page/聖彼得堡.md "wikilink")（1991年10月6日）

  - [設拉子](../Page/設拉子.md "wikilink")（1992年8月16日）

  - [明斯克](../Page/明斯克.md "wikilink")（1998年7月21日）

  - [乌鲁木齐市](../Page/乌鲁木齐市.md "wikilink")（1999年9月10日）

  - [德黑兰](../Page/德黑兰.md "wikilink")（2001年3月12日）

  - [安卡拉](../Page/安卡拉.md "wikilink")（2003年12月11日）

  - [厦门市](../Page/厦门市.md "wikilink")（2013年6月20日）

## 参考文献

<references/>

[Category:塔吉克城市](../Category/塔吉克城市.md "wikilink")
[Category:杜尚貝](../Category/杜尚貝.md "wikilink")