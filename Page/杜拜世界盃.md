**杜拜世界盃**是[阿拉伯聯合酋長國](../Page/阿拉伯聯合酋長國.md "wikilink")[杜拜一個主要](../Page/杜拜.md "wikilink")[賽馬賽事](../Page/賽馬.md "wikilink")，是由1996年起每年舉辦一次，通常於三月最後一個星期六舉行，路程是泥地2000米(左轉)。

賽事最初於[詩柏馬場舉行](../Page/詩柏馬場.md "wikilink")；由於配合杜拜的城市發展，詩柏馬場於2009年賽事舉行完畢後拆卸，2010年後的賽事將會遷移到鄰近新建造的[美丹馬場舉行](../Page/美丹馬場.md "wikilink")。

杜拜世界盃由杜拜的[埃米爾](../Page/埃米爾.md "wikilink")[麥通酋長創辦](../Page/麥通酋長.md "wikilink")，麥通酋長擁有達利馬房（Darley
Stud），是世界其中一個前領育馬及競賽馬房。杜拜世界盃是世界上最高獎金的单场賽馬，自2004年起總獎金達6百萬美元，自2010年起总奖金为一千万美元。2019年總獎金為一千二百萬美元。

杜拜世界盃首屆冠軍是其後著名，曾連勝16場並列入[美國賽馬名人堂](../Page/美國.md "wikilink")（National
Museum of Racing and Hall of
Fame）的馬王[雪茄](../Page/雪茄_\(馬\).md "wikilink")。

杜拜世界盃每年皆由[衛星作](../Page/衛星.md "wikilink")[電視直播至世界多個國家](../Page/電視.md "wikilink")。2006年起美國是由[美國廣播公司作直播](../Page/美國廣播公司.md "wikilink")，[香港是由](../Page/香港.md "wikilink")[有線電視和](../Page/香港有線電視.md "wikilink")[無線電視以英語及廣東話雙語麗音直播](../Page/無線電視.md "wikilink")。

## 配磅

三歲南半球出生馬 54公斤；四歲以上馬 57公斤。雌馬受讓2公斤。

## 紀錄

**時間:**

  - 2:01.61 (膠沙地) - [非洲傳說](../Page/非洲傳說.md "wikilink") (2014) 於
    [美丹馬場](../Page/美丹馬場.md "wikilink")
  - 2:01.38 (泥地) - [轟雷暴雪](../Page/轟雷暴雪.md "wikilink") (2018) 於
    [美丹馬場](../Page/美丹馬場.md "wikilink")
  - 1:59.50 - [杜拜千禧](../Page/杜拜千禧.md "wikilink") (2000) 於
    [詩柏馬場](../Page/詩柏馬場.md "wikilink")

**最多頭馬的馬主:**

  - 7 - [高多芬](../Page/高多芬.md "wikilink") (2000, 2002, 2003, 2006, 2012,
    2014, 2018)

**最多頭馬的騎師:**

  - 4 - 比利 (1996, 1997, 2001, 2002)

**最多頭馬的練馬師:**

  - 8 - [蘇萊](../Page/蘇萊.md "wikilink") (1999, 2000, 2002, 2003, 2006,
    2014, 2015, 2018)

## 歷屆冠軍

  - 1996年－[雪茄](../Page/雪茄_\(馬\).md "wikilink")（美國）
  - 1997年－[談唱劇](../Page/談唱劇.md "wikilink")（[英國](../Page/英國.md "wikilink")）
  - 1998年－[銀色魅力](../Page/銀色魅力.md "wikilink")（美國）
  - 1999年－[愛莫他求](../Page/愛莫他求.md "wikilink")（[阿聯酋](../Page/阿聯酋.md "wikilink")）
  - 2000年－[杜拜千禧](../Page/杜拜千禧.md "wikilink")（阿聯酋）
  - 2001年－[施隊長](../Page/施隊長.md "wikilink")（美國）
  - 2002年－[街頭口號](../Page/街頭口號.md "wikilink")（阿聯酋）
  - 2003年－[月光曲](../Page/月光曲_\(馬\).md "wikilink")（阿聯酋）
  - 2004年－[稱心如意](../Page/稱心如意_\(馬\).md "wikilink")（美國）
  - 2005年－[五月玫瑰](../Page/五月玫瑰.md "wikilink")（美國）
  - 2006年－[電極使者](../Page/電極使者.md "wikilink")（阿聯酋）
  - 2007年－[攻擊手](../Page/攻擊手.md "wikilink")（美國）
  - 2008年－[機靈駿馬](../Page/機靈駿馬.md "wikilink")（美國）
  - 2009年－[裝備齊全](../Page/裝備齊全.md "wikilink")（美國）
  - 2010年－[冠軍風采](../Page/冠軍風采.md "wikilink")（法國）
  - 2011年－[比薩勝駒](../Page/比薩勝駒.md "wikilink")（日本）
  - 2012年－[蒙泰羅索](../Page/蒙泰羅索.md "wikilink")（阿聯酋）
  - 2013年－[動物王國](../Page/動物王國_\(馬\).md "wikilink")（美國）
  - 2014年－[非洲傳說](../Page/非洲傳說.md "wikilink")（阿聯酋）
  - 2015年－[樞機主教](../Page/樞機主教.md "wikilink")（阿聯酋）
  - 2016年－[閃亮加州](../Page/閃亮加州.md "wikilink")（美國）
  - 2017年－[霸道駒](../Page/霸道駒.md "wikilink")（美國）
  - 2018年－[轟雷暴雪](../Page/轟雷暴雪.md "wikilink")（阿聯酋）
  - 2019年－[轟雷暴雪](../Page/轟雷暴雪.md "wikilink")（阿聯酋）

## 外部連結

  - [杜拜世界盃官方網站](https://web.archive.org/web/20060402201544/http://dubaiworldcup.com/)

[Category:賽馬](../Category/賽馬.md "wikilink")
[Category:杜拜](../Category/杜拜.md "wikilink")
[Category:阿聯酋體育](../Category/阿聯酋體育.md "wikilink")
[Category:阿联酋航空集团](../Category/阿联酋航空集团.md "wikilink")