**杜父魚科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目的其中一科](../Page/鮋形目.md "wikilink")。

## 分類

**杜父魚科**下分60幾個屬，如下：

### 雀杜父魚屬（*Alcichthys*）

  - [長體雀杜父魚](../Page/長體雀杜父魚.md "wikilink")（*Alcichthys elongatus*）

### 安德烈杜父魚屬（*Andriashevicottus*）

  - [大頭安德烈杜父魚](../Page/大頭安德烈杜父魚.md "wikilink")（*Andriashevicottus
    megacephalus*）

### 反足杜父魚屬（*Antipodocottus*）

  - [華麗反足杜父魚](../Page/華麗反足杜父魚.md "wikilink")（*Antipodocottus elegans*）
  - [加氏反足杜父魚](../Page/加氏反足杜父魚.md "wikilink")（*Antipodocottus galatheae*）
  - [大眼反足杜父魚](../Page/大眼反足杜父魚.md "wikilink")（*Antipodocottus megalops*）
  - [印尼反足杜父魚](../Page/印尼反足杜父魚.md "wikilink")（*Antipodocottus
    mesembrinus*）

### 始杜父魚屬（*Archistes*）

  - [白令海始杜父魚](../Page/白令海始杜父魚.md "wikilink")（*Archistes biseriatus*）
  - [始杜父魚](../Page/始杜父魚.md "wikilink")(*Archistes plumarius*)

### 銀杜父魚屬（*Argyrocottus*）

  - [贊氏銀杜父魚](../Page/贊氏銀杜父魚.md "wikilink")（*Argyrocottus zanderi*）

### 阿蒂迪魚屬(*Artediellichthys*)

  - [黑鰭阿蒂迪魚](../Page/黑鰭阿蒂迪魚.md "wikilink")(*Artediellichthys
    nigripinnis*)

### 阿蒂迪杜父魚屬（*Artediellina*）

  - [鄂霍次克海阿蒂迪杜父魚](../Page/鄂霍次克海阿蒂迪杜父魚.md "wikilink")（*Artediellina
    antilope*）

### 似阿蒂迪杜父魚屬（*Artedielloides*）

  - [似阿蒂迪杜父魚](../Page/似阿蒂迪杜父魚.md "wikilink")（*Artedielloides
    auriculatus*）

### 鉤杜父魚屬（*Artediellus*）

  - [無孔鉤杜父魚](../Page/無孔鉤杜父魚.md "wikilink")（*Artediellus aporosus*）
  - [大西洋鉤杜父魚](../Page/大西洋鉤杜父魚.md "wikilink")（*Artediellus atlanticus*）
  - [無紋鉤杜父魚](../Page/無紋鉤杜父魚.md "wikilink")（*Artediellus camchaticus*）
  - [戴氏鉤杜父魚](../Page/戴氏鉤杜父魚.md "wikilink")（*Artediellus dydymovi*）
  - [紡錘鉤杜父魚](../Page/紡錘鉤杜父魚.md "wikilink")（*Artediellus fuscimentus*）
  - [戈氏鉤杜父魚](../Page/戈氏鉤杜父魚.md "wikilink")(*Artediellus gomojunovi*)
  - [強鉤杜父魚](../Page/強鉤杜父魚.md "wikilink")（*Artediellus ingens*）
  - [小刺鉤杜父魚](../Page/小刺鉤杜父魚.md "wikilink")（*Artediellus miacanthus*）
  - [長胸鉤杜父魚](../Page/長胸鉤杜父魚.md "wikilink")（*Artediellus minor*）
  - [尼氏鉤杜父魚](../Page/尼氏鉤杜父魚.md "wikilink")（*Artediellus neyelovi*）
  - [蟲紋鉤杜父魚](../Page/蟲紋鉤杜父魚.md "wikilink")（*Artediellus ochotensis*）
  - [太平洋鉤杜父魚](../Page/太平洋鉤杜父魚.md "wikilink")（*Artediellus pacificus*）
  - [粗糙鉤杜父魚](../Page/粗糙鉤杜父魚.md "wikilink")（*Artediellus scaber*）
  - [舒氏鉤杜父魚](../Page/舒氏鉤杜父魚.md "wikilink")（*Artediellus schmidti*）
  - [加拿大鉤杜父魚](../Page/加拿大鉤杜父魚.md "wikilink")(*Artediellus uncinatus*)

### 阿氏杜父魚屬（*Artedius*）

  - [珊瑚阿氏杜父魚](../Page/珊瑚阿氏杜父魚.md "wikilink")（*Artedius corallinus*）
  - [阿拉斯加阿氏杜父魚](../Page/阿拉斯加阿氏杜父魚.md "wikilink")（*Artedius fenestralis*）
  - [鱗首阿氏杜父魚](../Page/鱗首阿氏杜父魚.md "wikilink")（*Artedius harringtoni*）
  - [光首阿氏杜父魚](../Page/光首阿氏杜父魚.md "wikilink")（*Artedius lateralis*）
  - [硬頭阿氏杜父魚](../Page/硬頭阿氏杜父魚.md "wikilink")（*Artedius notospilotus*）

### 裸腹杜父魚屬（*Ascelichthys*）

  - [玫瑰裸腹杜父魚](../Page/玫瑰裸腹杜父魚.md "wikilink")（*Ascelichthys rhodorus*）

### 吻杜父魚屬（*Asemichthys*）

  - [泰氏棘吻杜父魚](../Page/泰氏棘吻杜父魚.md "wikilink")（*Asemichthys taylori*）

### 星杜父魚屬（*Astrocottus*）

  - [粗鱗星杜父魚](../Page/粗鱗星杜父魚.md "wikilink")（*Astrocottus leprops*）
  - [松原星杜父魚](../Page/松原星杜父魚.md "wikilink")（*Astrocottus matsubarae*）
  - [鼻棘星杜父魚](../Page/鼻棘星杜父魚.md "wikilink")（*Astrocottus oyamai*）
  - [日本星杜父魚](../Page/日本星杜父魚.md "wikilink")(*Astrocottus regulus*)

### 異杜父魚屬（*Atopocottus*）

  - [異杜父魚](../Page/異杜父魚.md "wikilink")（*Atopocottus tribranchius*）

### 穗瓣杜父魚屬（*Bero*）

  - [美麗穗瓣杜父魚](../Page/美麗穗瓣杜父魚.md "wikilink")（*Bero elegans*）

### 白令杜父魚屬（*Bolinia*）

  - [寬鰭白令杜父魚](../Page/寬鰭白令杜父魚.md "wikilink")（*Bolinia euryptera*）

### 粗背杜父魚屬（*Chitonotus*）

  - [墨西哥粗背杜父魚](../Page/墨西哥粗背杜父魚.md "wikilink")（*Chitonotus pugetensis*）

### 斜杜父魚屬（*Clinocottus*）

  - [尖吻斜杜父魚](../Page/尖吻斜杜父魚.md "wikilink")（*Clinocottus acuticeps*）
  - [臀斜杜父魚](../Page/臀斜杜父魚.md "wikilink")（*Clinocottus analis*）
  - [雜色斜杜父魚](../Page/雜色斜杜父魚.md "wikilink")（*Clinocottus embryum*）
  - [圓頭斜杜父魚](../Page/圓頭斜杜父魚.md "wikilink")（*Clinocottus globiceps*）
  - [貪食斜杜父魚](../Page/貪食斜杜父魚.md "wikilink")（*Clinocottus recalvus*）

### 細杜父魚屬（*Cottiusculus*）

  - [日本細杜父魚](../Page/日本細杜父魚.md "wikilink")（*Cottiusculus gonez*）
  - [日本海細杜父魚](../Page/日本海細杜父魚.md "wikilink")（*Cottiusculus
    nihonkaiensis*）
  - [斑鰭細杜父魚](../Page/斑鰭細杜父魚.md "wikilink")（*Cottiusculus schmidti*）

### 杜父魚屬（*Cottus*）

  - [阿留申杜父魚](../Page/阿留申杜父魚.md "wikilink")（*Cottus aleuticus*）
  - [薩哈林杜父魚](../Page/薩哈林杜父魚.md "wikilink")（*Cottus amblystomopsis*）
  - [多棘杜父魚](../Page/多棘杜父魚.md "wikilink")（*Cottus asper*）
  - [糙皮杜父魚](../Page/糙皮杜父魚.md "wikilink")（*Cottus asperrimus*）
  - [阿氏杜父魚](../Page/阿氏杜父魚.md "wikilink")(*Cottus aturi*)
  - [黑杜父魚](../Page/黑杜父魚.md "wikilink")（*Cottus baileyi*）
  - [巴氏杜父魚](../Page/巴氏杜父魚.md "wikilink")（*Cottus bairdii*）：又稱似糙杜父魚。
  - [貝氏杜父魚](../Page/貝氏杜父魚.md "wikilink")（*Cottus beldingi*）
  - [本氏杜父魚](../Page/本氏杜父魚.md "wikilink")(*Cottus bendirei*)
  - [淡黑杜父魚](../Page/淡黑杜父魚.md "wikilink")(*Cottus caeruleomentum*)
  - [卡羅來納杜父魚](../Page/卡羅來納杜父魚.md "wikilink")（*Cottus carolinae*）
  - [北美杜父魚](../Page/北美杜父魚.md "wikilink")(*Cottus chattahoochee*)
  - [粘杜父魚](../Page/粘杜父魚.md "wikilink")（*Cottus cognatus*）
  - [短頭杜父魚](../Page/短頭杜父魚.md "wikilink")（*Cottus confusus*）
  - [謝氏杜父魚](../Page/謝氏杜父魚.md "wikilink")（*Cottus czerskii*）
  - [德氏杜父魚](../Page/德氏杜父魚.md "wikilink")(*Cottus duranii*)
  - [黏滑杜父魚](../Page/黏滑杜父魚.md "wikilink")（*Cottus dzungaricus*）
  - [猶他杜父魚](../Page/猶他杜父魚.md "wikilink")（*Cottus echinatus*）
  - [熊湖杜父魚](../Page/熊湖杜父魚.md "wikilink")（*Cottus extensus*）
  - [杰氏杜父魚](../Page/杰氏杜父魚.md "wikilink")（*Cottus girardi*）
  - [鮈杜父魚](../Page/鮈杜父魚.md "wikilink")（*Cottus gobio*）
  - [格氏杜父魚](../Page/格氏杜父魚.md "wikilink")（*Cottus greenei*）
  - [貪食杜父魚](../Page/貪食杜父魚.md "wikilink")(*Cottus gulosus*)
  - [黑穆氏杜父魚](../Page/黑穆氏杜父魚.md "wikilink")(*Cottus haemusi*)
  - [圖們江杜父魚](../Page/圖們江杜父魚.md "wikilink")（*Cottus hangiongensis*）
  - [西班牙杜父魚](../Page/西班牙杜父魚.md "wikilink")(*Cottus hispaniolensis*)
  - [赫氏杜父魚](../Page/赫氏杜父魚.md "wikilink")(*Cottus hubbsi*)
  - [高杜父魚](../Page/高杜父魚.md "wikilink")（*Cottus hypselurus*）
  - [無斑杜父魚](../Page/無斑杜父魚.md "wikilink")(*Cottus immaculatus*)
  - [臨河杜父魚](../Page/臨河杜父魚.md "wikilink")（*Cottus kanawhae*）
  - [西刺杜父魚](../Page/西刺杜父魚.md "wikilink")（*Cottus kazika*）
  - [石紋杜父魚](../Page/石紋杜父魚.md "wikilink")（*Cottus klamathensis*）
  - [克雷馬杜父魚](../Page/克雷馬杜父魚.md "wikilink")(*Cottus kolymensis*)
  - [朝鮮杜父魚](../Page/朝鮮杜父魚.md "wikilink")(*Cottus koreanus*)
  - [科氏杜父魚](../Page/科氏杜父魚.md "wikilink")(*Cottus koshewnikowi*)
  - [庫氏杜父魚](../Page/庫氏杜父魚.md "wikilink")(*Cottus kuznetzovi*)
  - [滑蓋杜父魚](../Page/滑蓋杜父魚.md "wikilink")（*Cottus leiopomus*）
  - [飾邊杜父魚](../Page/飾邊杜父魚.md "wikilink")（*Cottus marginatus*）
  - [米塔氏杜父魚](../Page/米塔氏杜父魚.md "wikilink")(*Cottus metae*)
  - [小口杜父魚](../Page/小口杜父魚.md "wikilink")(*Cottus microstomus*)
  - [大吻杜父魚](../Page/大吻杜父魚.md "wikilink")（*Cottus nasalis*）
  - [大棘杜父魚](../Page/大棘杜父魚.md "wikilink")（*Cottus nozawae*）
  - [細小杜父魚](../Page/細小杜父魚.md "wikilink")(*Cottus paulus*)
  - [大斑杜父魚](../Page/大斑杜父魚.md "wikilink")(*Cottus perifretum*)
  - [網紋杜父魚](../Page/網紋杜父魚.md "wikilink")（*Cottus perplexus*）
  - [佩氏杜父魚](../Page/佩氏杜父魚.md "wikilink")（*Cottus petiti*）
  - [皮特杜父魚](../Page/皮特杜父魚.md "wikilink")（*Cottus pitensis*）
  - [花足杜父魚](../Page/花足杜父魚.md "wikilink")（*Cottus poecilopus*）
  - [鈍頭杜父魚](../Page/鈍頭杜父魚.md "wikilink")（*Cottus pollux*）
  - [青頭杜父魚](../Page/青頭杜父魚.md "wikilink")（*Cottus princeps*）：又稱克湖杜父魚。
  - [賴氏杜父魚](../Page/賴氏杜父魚.md "wikilink")（*Cottus reinii*）
  - [萊因河杜父魚](../Page/萊因河杜父魚.md "wikilink")(*Cottus rhenanus*)
  - [急流杜父魚](../Page/急流杜父魚.md "wikilink")（*Cottus rhotheus*）
  - [里氏杜父魚](../Page/里氏杜父魚.md "wikilink")（*Cottus ricei*）
  - [朗氏杜父魚](../Page/朗氏杜父魚.md "wikilink")(*Cottus rondeleti*)
  - [沙巴杜父魚](../Page/沙巴杜父魚.md "wikilink")(*Cottus sabaudicus*)
  - [紅褐杜父魚](../Page/紅褐杜父魚.md "wikilink")(*Cottus scaturigo*)
  - [蘇氏杜父魚](../Page/蘇氏杜父魚.md "wikilink")(*Cottus schitsuumsh*)
  - [西伯利亞杜父魚](../Page/西伯利亞杜父魚.md "wikilink")（*Cottus sibiticus*）
  - [穴居杜父魚](../Page/穴居杜父魚.md "wikilink")(*Cottus specus*)
  - [多刺杜父魚](../Page/多刺杜父魚.md "wikilink")（*Cottus spinulosus*）
  - [蒙古杜父魚](../Page/蒙古杜父魚.md "wikilink")(*Cottus szanaga*)
  - [塔氏杜父魚](../Page/塔氏杜父魚.md "wikilink")(*Cottus tallapoosae*)
  - [細尾杜父魚](../Page/細尾杜父魚.md "wikilink")（*Cottus tenuis*）
  - [特氏杜父魚](../Page/特氏杜父魚.md "wikilink")(*Cottus transsilvaniae*)
  - [伏氏杜父魚](../Page/伏氏杜父魚.md "wikilink")(*Cottus volki*)

### 鰓棘杜父魚屬（*Daruma*）

  - [鰓棘杜父魚](../Page/鰓棘杜父魚.md "wikilink")（*Daruma sagamia*）

### 強棘杜父魚屬（*Enophrys*）

  - [布法羅強棘杜父魚](../Page/布法羅強棘杜父魚.md "wikilink")（*Enophrys bison*）
  - [強棘杜父魚](../Page/強棘杜父魚.md "wikilink")（*Enophrys diceraus*）
  - [勒氏強棘杜父魚](../Page/勒氏強棘杜父魚.md "wikilink")（*Enophrys lucasi*）
  - [韌皮強棘杜父魚](../Page/韌皮強棘杜父魚.md "wikilink")（*Enophrys taurinus*）

### 叉杜父魚屬（*Furcina*）

  - [石川氏叉杜父魚](../Page/石川氏叉杜父魚.md "wikilink")（*Furcina ishikawae*）
  - [日本寬叉杜父魚](../Page/日本寬叉杜父魚.md "wikilink")（*Furcina osimae*）

### 裸棘杜父魚屬（*Gymnocanthus*）

  - [寬額裸棘杜父魚](../Page/寬額裸棘杜父魚.md "wikilink")（*Gymnocanthus detrisus*）
  - [盔裸棘杜父魚](../Page/盔裸棘杜父魚.md "wikilink")（*Gymnocanthus galeatus*）
  - [凹尾裸棘杜父魚](../Page/凹尾裸棘杜父魚.md "wikilink")（*Gymnocanthus
    herzensteini*）
  - [中間裸棘杜父魚](../Page/中間裸棘杜父魚.md "wikilink")（*Gymnocanthus intermedius*）
  - [截尾裸棘杜父魚](../Page/截尾裸棘杜父魚.md "wikilink")（*Gymnocanthus pistilliger*）
  - [三叉裸棘杜父魚](../Page/三叉裸棘杜父魚.md "wikilink")（*Gymnocanthus tricuspis
    tricuspis*）
  - [范氏裸棘杜父魚](../Page/范氏裸棘杜父魚.md "wikilink")（*Gymnocanthus vandesandei*）

### 雜鱗杜父魚屬（*Hemilepidotus*）

  - [吉氏雜鱗杜父魚](../Page/吉氏雜鱗杜父魚.md "wikilink")（*Hemilepidotus gilberti*）
  - [腹斑雜鱗杜父魚](../Page/腹斑雜鱗杜父魚.md "wikilink")（*Hemilepidotus
    hemilepidotus*）
  - [喬氏雜鱗杜父魚](../Page/喬氏雜鱗杜父魚.md "wikilink")（*Hemilepidotus jordani*）
  - [橫帶雜鱗杜父魚](../Page/橫帶雜鱗杜父魚.md "wikilink")（*Hemilepidotus papilio*）
  - [大棘鱗杜父魚](../Page/大棘鱗杜父魚.md "wikilink")（*Hemilepidotus spinosus*）
  - [長鰭雜鱗杜父魚](../Page/長鰭雜鱗杜父魚.md "wikilink")（*Hemilepidotus zapus*）

### 擬冰杜父魚屬（*Icelinus*）

  - [北方擬冰杜父魚](../Page/北方擬冰杜父魚.md "wikilink")（*Icelinus borealis*）
  - [伯氏擬冰杜父魚](../Page/伯氏擬冰杜父魚.md "wikilink")（*Icelinus burchami*）
  - [凹頭擬冰杜父魚](../Page/凹頭擬冰杜父魚.md "wikilink")（*Icelinus cavifrons*）
  - [絲鰭擬冰杜父魚](../Page/絲鰭擬冰杜父魚.md "wikilink")（*Icelinus filamentosus*）
  - [纓擬冰杜父魚](../Page/纓擬冰杜父魚.md "wikilink")（*Icelinus fimbriatus*）
  - [日本擬冰杜父魚](../Page/日本擬冰杜父魚.md "wikilink")（*Icelinus japonicus*）
  - [林博氏擬冰杜父魚](../Page/林博氏擬冰杜父魚.md "wikilink")(*Icelinus limbaughi*)
  - [眼斑擬冰杜父魚](../Page/眼斑擬冰杜父魚.md "wikilink")（*Icelinus oculatus*）
  - [皮氏擬冰杜父魚](../Page/皮氏擬冰杜父魚.md "wikilink")(*Icelinus pietschi*)
  - [黃頜擬冰杜父魚](../Page/黃頜擬冰杜父魚.md "wikilink")（*Icelinus quadriseriatus*）
  - [斑鰭擬冰杜父魚](../Page/斑鰭擬冰杜父魚.md "wikilink")（*Icelinus tenuis*）

### 冰杜父魚屬（*Icelus*）

  - [橫帶冰杜父魚](../Page/橫帶冰杜父魚.md "wikilink")（*Icelus armatus*）
  - [雙角冰杜父魚](../Page/雙角冰杜父魚.md "wikilink")（*Icelus bicornis*）
  - [鋸鱗冰杜父魚](../Page/鋸鱗冰杜父魚.md "wikilink")（*Icelus canaliculatus*）
  - [鱗棘冰杜父魚](../Page/鱗棘冰杜父魚.md "wikilink")（*Icelus cataphractus*）
  - [光頭冰杜父魚](../Page/光頭冰杜父魚.md "wikilink")（*Icelus ecornis*）
  - [寬眼冰杜父魚](../Page/寬眼冰杜父魚.md "wikilink")（*Icelus euryops*）
  - [大頭冰杜父魚](../Page/大頭冰杜父魚.md "wikilink")（*Icelus gilberti*）
  - [突額冰杜父魚](../Page/突額冰杜父魚.md "wikilink")（*Icelus mandibularis*）
  - [鄂霍次克冰杜父魚](../Page/鄂霍次克冰杜父魚.md "wikilink")（*Icelus ochotensis*）
  - [貝氏冰杜父魚](../Page/貝氏冰杜父魚.md "wikilink")（*Icelus perminovi*）
  - [似耙冰杜父魚](../Page/似耙冰杜父魚.md "wikilink")（*Icelus rastrinoides*）
  - [塞克冰杜父魚](../Page/塞克冰杜父魚.md "wikilink")(*Icelus sekii*)
  - [匙形冰杜父魚](../Page/匙形冰杜父魚.md "wikilink")（*Icelus spatula*）
  - [鋸棘冰杜父魚](../Page/鋸棘冰杜父魚.md "wikilink")（*Icelus spiniger*）
  - [狹體冰杜父魚](../Page/狹體冰杜父魚.md "wikilink")（*Icelus stenosomus*）
  - [富山冰杜父魚](../Page/富山冰杜父魚.md "wikilink")(*Icelus toyamensis*)
  - [阿拉斯加冰杜父魚](../Page/阿拉斯加冰杜父魚.md "wikilink")（*Icelus uncinalis*）

### 喬氏杜父魚屬（*Jordania*）

  - [長鰭喬氏杜父魚](../Page/長鰭喬氏杜父魚.md "wikilink")（*Jordania zonope*）

### 滑杜父魚屬（*Leiocottus*）

  - [滑杜父魚](../Page/滑杜父魚.md "wikilink")（*Leiocottus hirundo*）

### 鱗舌杜父魚屬（*Lepidobero*）

  - [中華鱗舌杜父魚](../Page/中華鱗舌杜父魚.md "wikilink")（*Lepidobero sinensis*）

### 鹿角杜父魚屬（*Leptocottus*）

  - [太平洋鹿角杜父魚](../Page/太平洋鹿角杜父魚.md "wikilink")（*Leptocottus armatus*）

### 大杜父魚屬（*Megalocottus*）

  - [扁頭大杜父魚](../Page/扁頭大杜父魚.md "wikilink")（*Megalocottus platycephalus
    platycephalus*）
  - [條帶大杜父魚](../Page/條帶大杜父魚.md "wikilink")(*Megalocottus platycephalus
    taeniopterus*)

### 中杜父魚屬（*Mesocottus*）

  - [黑龍江中杜父魚](../Page/黑龍江中杜父魚.md "wikilink")（*Mesocottus haitej*）

### 細眉杜父魚屬（*Micrenophrys*）

  - [細眉杜父魚](../Page/細眉杜父魚.md "wikilink")（*Micrenophrys lilljeborgii*）

### 小杜父魚屬（*Microcottus*）

  - [千島群島小杜父魚](../Page/千島群島小杜父魚.md "wikilink")(*Microcottus matuaensis*)
  - [項鞍小杜父魚](../Page/項鞍小杜父魚.md "wikilink")（*Microcottus sellaris*）

### 床杜父魚屬（*Myoxocephalus*）

  - [銅色床杜父魚](../Page/銅色床杜父魚.md "wikilink")（*Myoxocephalus aenaeus*）
  - [布氏床杜父魚](../Page/布氏床杜父魚.md "wikilink")（*Myoxocephalus brandtii*）
  - [床杜父魚](../Page/床杜父魚.md "wikilink")（*Myoxocephalus incitus*）
  - [淺色床杜父魚](../Page/淺色床杜父魚.md "wikilink")（*Myoxocephalus jaok*）
  - [松原床杜父魚](../Page/松原床杜父魚.md "wikilink")（*Myoxocephalus matsubarai*）
  - [黑床杜父魚](../Page/黑床杜父魚.md "wikilink")（*Myoxocephalus niger*）
  - [奧霍塔床杜父魚](../Page/奧霍塔床杜父魚.md "wikilink")（*Myoxocephalus ochotensis*）
  - [多刺床杜父魚](../Page/多刺床杜父魚.md "wikilink")（*Myoxocephalus
    octodecemspinosus*）
  - [棘頭床杜父魚](../Page/棘頭床杜父魚.md "wikilink")（*Myoxocephalus
    polyacanthocephalus*）
  - [四角床杜父魚](../Page/四角床杜父魚.md "wikilink")(*Myoxocephalus quadricornis*)
  - [北極床杜父魚](../Page/北極床杜父魚.md "wikilink")（*Myoxocephalus scorpioides*）
  - [短角床杜父魚](../Page/短角床杜父魚.md "wikilink")（*Myoxocephalus scorpius*）
  - [中華床杜父魚](../Page/中華床杜父魚.md "wikilink")（*Myoxocephalus sinensis*）
  - [斯氏床杜父魚](../Page/斯氏床杜父魚.md "wikilink")（*Myoxocephalus stelleri*）
  - [深水床杜父魚](../Page/深水床杜父魚.md "wikilink")（*Myoxocephalus thompsonii*）
  - [瘤床杜父魚](../Page/瘤床杜父魚.md "wikilink")(*Myoxocephalus tuberculatus*)
  - [柵床杜父魚](../Page/柵床杜父魚.md "wikilink")（*Myoxocephalus yesoensis*）

### 蜥杜父魚屬（*Ocynectes*）

  - [暗紋蜥杜父魚](../Page/暗紋蜥杜父魚.md "wikilink")（*Ocynectes maschalis*）
  - [光滑蜥杜父魚](../Page/光滑蜥杜父魚.md "wikilink")（*Ocynectes modestus*）

### 寡杜父魚屬（*Oligocottus*）

  - [斑紋寡杜父魚](../Page/斑紋寡杜父魚.md "wikilink")（*Oligocottus maculosus*）
  - [鞍背寡杜父魚](../Page/鞍背寡杜父魚.md "wikilink")（*Oligocottus rimensis*）
  - [玫瑰寡杜父魚](../Page/玫瑰寡杜父魚.md "wikilink")（*Oligocottus rubellio*）
  - [斯氏寡杜父魚](../Page/斯氏寡杜父魚.md "wikilink")（*Oligocottus snyderi*）

### 直杜父魚屬（*Orthonopias*）

  - [獅鼻直杜父魚](../Page/獅鼻直杜父魚.md "wikilink")（*Orthonopias triacis*）

### 棘背杜父魚屬（*Paricelinus*）

  - [棘背杜父魚](../Page/棘背杜父魚.md "wikilink")（*Paricelinus hopliticus*）

### 莖杜父魚屬（*Phallocottus*）

  - [鈍頭莖杜父魚](../Page/鈍頭莖杜父魚.md "wikilink")（*Phallocottus obtusus*）

### 鬼杜父魚屬（*Phasmatocottus*）

  - [梳鰭鬼杜父魚](../Page/梳鰭鬼杜父魚.md "wikilink")（*Phasmatocottus
    ctenopterygius*）

### 鉤棘杜父魚屬（*Porocottus*）

  - [艾氏鉤棘杜父魚](../Page/艾氏鉤棘杜父魚.md "wikilink")（*Porocottus allisi*）
  - [大眼鉤棘杜父魚](../Page/大眼鉤棘杜父魚.md "wikilink")(*Porocottus camtschaticus*)
  - [花冠鉤棘杜父魚](../Page/花冠鉤棘杜父魚.md "wikilink")（*Porocottus coronatus*）
  - [日本鉤棘杜父魚](../Page/日本鉤棘杜父魚.md "wikilink")（*Porocottus japonicus*）
  - [細身鉤棘杜父魚](../Page/細身鉤棘杜父魚.md "wikilink")(*Porocottus leptosomus*)
  - [白令海鉤棘杜父魚](../Page/白令海鉤棘杜父魚.md "wikilink")(*Porocottus mednius*)
  - [微鉤棘杜父魚](../Page/微鉤棘杜父魚.md "wikilink")（*Porocottus minutus*）
  - [四線鉤棘杜父魚](../Page/四線鉤棘杜父魚.md "wikilink")（*Porocottus quadrifilis*）
  - [觸指鉤棘杜父魚](../Page/觸指鉤棘杜父魚.md "wikilink")（*Porocottus tentaculatus*）

### 鳚杜父魚屬（*Pseudoblennius*）

  - [銀色鳚杜父魚](../Page/銀色鳚杜父魚.md "wikilink")(*Pseudoblennius argenteus*)
  - [銀帶鳚杜父魚](../Page/銀帶鳚杜父魚.md "wikilink")（*Pseudoblennius cottoides*）
  - [斑紋鳚杜父魚](../Page/斑紋鳚杜父魚.md "wikilink")（*Pseudoblennius marmoratus*）
  - [鱸形鳚杜父魚](../Page/鱸形鳚杜父魚.md "wikilink")（*Pseudoblennius percoides*）
  - [吻棘鳚杜父魚](../Page/吻棘鳚杜父魚.md "wikilink")（*Pseudoblennius totomius*）
  - [帶斑鳚杜父魚](../Page/帶斑鳚杜父魚.md "wikilink")（*Pseudoblennius zonostigma*）

### 擬刮食杜父魚(*Radulinopsis*)

  - [德氏擬刮食杜父魚](../Page/德氏擬刮食杜父魚.md "wikilink")(*Radulinopsis derjavini*)
  - [塔氏擬刮食杜父魚](../Page/塔氏擬刮食杜父魚.md "wikilink")(*Radulinopsis taranetzi*)

### 刮食杜父魚屬（*Radulinus*）

  - [細體刮食杜父魚](../Page/細體刮食杜父魚.md "wikilink")（*Radulinus asprellus*）
  - [刮食杜父魚](../Page/刮食杜父魚.md "wikilink")（*Radulinus boleoides*）
  - [索狀刮食杜父魚](../Page/索狀刮食杜父魚.md "wikilink")（*Radulinus vinculus*）

### 小耙杜父魚屬（*Rastrinus*）

  - [白令海小耙杜父魚](../Page/白令海小耙杜父魚.md "wikilink")（*Rastrinus scutiger*）

### 底杜父魚屬（*Ricuzenius*）

  - [裸胸底杜父魚](../Page/裸胸底杜父魚.md "wikilink")（*Ricuzenius nudithorax*）
  - [松木底杜父魚](../Page/松木底杜父魚.md "wikilink")（*Ricuzenius pinetorum*）

### 吠杜父魚屬（*Ruscarius*）

  - [克氏吠杜父魚](../Page/克氏吠杜父魚.md "wikilink")（*Ruscarius creaseri*）
  - [米氏吠杜父魚](../Page/米氏吠杜父魚.md "wikilink")（*Ruscarius meanyi*）

### 鮋杜父魚屬（*Scorpaenichthys*）

  - [雲斑鮋杜父魚](../Page/雲斑鮋杜父魚.md "wikilink")（*Scorpaenichthys marmoratus*）

### 弓杜父魚屬（*Sigmistes*）

  - [莖狀弓杜父魚](../Page/莖狀弓杜父魚.md "wikilink")（*Sigmistes caulias*）
  - [史氏弓杜父魚](../Page/史氏弓杜父魚.md "wikilink")（*Sigmistes smithi*）

### 疣杜父魚屬（*Stelgistrum*）

  - [阿留申群島疣杜父魚](../Page/阿留申群島疣杜父魚.md "wikilink")(*Stelgistrum
    beringianum*)
  - [強壯疣杜父魚](../Page/強壯疣杜父魚.md "wikilink")(*Stelgistrum concinnum*)
  - [疣杜父魚](../Page/疣杜父魚.md "wikilink")（*Stelgistrum stejnegeri*）

### 粗鱗鯒屬（*Stlengis*）

  - [雙列粗鱗鯒](../Page/雙列粗鱗鯒.md "wikilink")（*Stlengis distoechus*）
  - [三崎粗鱗鯒](../Page/三崎粗鱗鯒.md "wikilink")（*Stlengis misakia*）
  - [高知粗鱗鯒](../Page/高知粗鱗鯒.md "wikilink")（*Stlengis osensis*）

### 連鰭杜父魚屬（*Synchirus*）

  - [連鰭杜父魚](../Page/連鰭杜父魚.md "wikilink")（*Synchirus gilli*）

### 牛杜父魚屬（*Taurocottus*）

  - [伯氏牛杜父魚](../Page/伯氏牛杜父魚.md "wikilink")（*Taurocottus bergii*）

### 牛首杜父魚屬（*Taurulus*）

  - [牛首杜父魚](../Page/牛首杜父魚.md "wikilink")（*Taurulus bubalis*）

### 甲杜父魚屬（*Thyriscus*）

  - [白令海甲杜父魚](../Page/白令海甲杜父魚.md "wikilink")（*Thyriscus anoplus*）

### 松江鱸屬（*Trachidermus*）

  - [松江鱸](../Page/松江鱸.md "wikilink")（*Trachidermus fasciatus*）

### 毛杜父魚屬（*Trichocottus*）

  - [布氏毛杜父魚](../Page/布氏毛杜父魚.md "wikilink")（*Trichocottus brashnikovi*）

### 鮄杜父魚屬（*Triglops*）

  - [桃樂鮄杜父魚](../Page/桃樂鮄杜父魚.md "wikilink")(*Triglops dorothy*)
  - [尖尾鮄杜父魚](../Page/尖尾鮄杜父魚.md "wikilink")（*Triglops forficatus*）
  - [叉尾鮄杜父魚](../Page/叉尾鮄杜父魚.md "wikilink")（*Triglops jordani*）
  - [粗棘鮄杜父魚](../Page/粗棘鮄杜父魚.md "wikilink")（*Triglops macellus*）
  - [高額鮄杜父魚](../Page/高額鮄杜父魚.md "wikilink")（*Triglops metopias*）
  - [牟氏鮄杜父魚](../Page/牟氏鮄杜父魚.md "wikilink")（*Triglops murrayi*）
  - [尼氏鮄杜父魚](../Page/尼氏鮄杜父魚.md "wikilink")（*Triglops nybelini*）
  - [平氏鮄杜父魚](../Page/平氏鮄杜父魚.md "wikilink")（*Triglops pingelii*）
  - [大眼鮄杜父魚](../Page/大眼鮄杜父魚.md "wikilink")（*Triglops scepticus*）
  - [外胸鮄杜父魚](../Page/外胸鮄杜父魚.md "wikilink")（*Triglops xenostethus*）

### 尖頭杜父魚屬（*Vellitor*）

  - [尖頭杜父魚](../Page/尖頭杜父魚.md "wikilink")（*Vellitor centropomus*）
  - [小尖頭杜父魚](../Page/小尖頭杜父魚.md "wikilink")（*Vellitor minutus*）

### 軟首杜父魚屬（*Zesticelus*）

  - [深水軟首杜父魚](../Page/深水軟首杜父魚.md "wikilink")（*Zesticelus bathybius*）
  - [鄂霍次克軟首杜父魚](../Page/鄂霍次克軟首杜父魚.md "wikilink")（*Zesticelus
    ochotensis*）
  - [極深軟首杜父魚](../Page/極深軟首杜父魚.md "wikilink")（*Zesticelus profundorum*）

[\*](../Category/杜父魚科.md "wikilink")