**杜维明**（），祖籍[广东](../Page/广东.md "wikilink")[南海](../Page/南海.md "wikilink")，出生于[云南](../Page/云南.md "wikilink")[昆明](../Page/昆明.md "wikilink")，[孟子研究院名誉院长](../Page/孟子研究院.md "wikilink")，学者，第三代[新儒家代表人物](../Page/新儒家.md "wikilink")，[哈佛大学教授](../Page/哈佛大学.md "wikilink")，[中央研究院院士](../Page/中央研究院院士.md "wikilink")。

## 生平

1957年进入台湾[东海大学](../Page/東海大學_\(台灣\).md "wikilink")，师事[徐复观](../Page/徐复观.md "wikilink")，亦受[牟宗三思想影响](../Page/牟宗三.md "wikilink")。1961年大学毕业，次年获「哈佛－燕京奖学金」赴美国[哈佛大学就读](../Page/哈佛大学.md "wikilink")，1968年获哈佛大学哲学博士学位。曾任教于[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")、[柏克莱加州大学](../Page/柏克莱加州大学.md "wikilink")，1981年起任[哈佛大学中国历史和哲学教授](../Page/哈佛大学.md "wikilink")，并曾担任过该校宗教研究会主席、东亚语言和文明系主任。1988年成为美国人文、艺术及科学院院士。曾任美国夏威夷东西文化交流中心文化交流所
(Institute of Culture and Communication) 主任
(1990-1991)。1996至2008年間，出任[哈佛燕京學社社長](../Page/哈佛燕京學社.md "wikilink")。

杜维明早年受[徐复观](../Page/徐复观.md "wikilink")、[牟宗三等新儒家思想的影响](../Page/牟宗三.md "wikilink")，以后在美国又系统地研习过西方哲学，自述从1966年起，决心全力从事对儒家精神作长期的探索，并以此作为自己专业研究工作。他把儒学看成是「哲学的人类学」、「[宗教哲学](../Page/宗教哲学.md "wikilink")」，试图从文化认同的意义上说明儒家传统的历史和价值。认为儒家思想的原初形式是环绕着[孔子的仁学而展开的](../Page/孔子.md "wikilink")，这套思想具有成熟的道德理性、浓厚的人文关切和强烈的入世精神。[孔子的仁学属于伦理学的范畴](../Page/孔子.md "wikilink")，其核心思想是探索如何做人的道理，仁学的兴起象征古代中国人文意识逐渐取代宗教神学而成为中华民族的主导思想。儒学具有「道统」、「学统」和「政统」，是可以转化社会政治和道德的精神源泉。他指出，应该区分「儒家传统」和「儒教中国」，前者是一种涵盖性很强的人文主义精神，具有历久长新的恒常价值；后者是以政治化的儒家伦理为主导思想的中国传统封建社会的意识形态，是封建遗毒；[五四新文化运动批判的实际上是](../Page/五四新文化运动.md "wikilink")「儒教中国」，从对「儒家传统」的发扬来说这种批判是有利的。他发挥[牟宗三关于儒学发展的](../Page/牟宗三.md "wikilink")「三期说」理论，提出了「儒学第三期发展的前景」问题。认为从先秦到西汉是儒学发展的第一期，他用[雅斯贝爾斯的](../Page/雅斯贝爾斯.md "wikilink")「[轴心时代](../Page/轴心时代.md "wikilink")」的理论来说明这一期儒学出现和发展的原因；宋元明清是儒学发展的第二期，它的出现是针对佛教文化挑战的「一个创造性的回应」，并用[岛田虔次的说法指出这一期儒学是东亚文明的集中体现](../Page/岛田虔次.md "wikilink")；而第三期儒学的发展，则是对西方文化挑战的回应，认为儒学在二十一世纪是否还有生命力，主要取决于儒学是否能经过纽约、巴黎、东京最后回到中国，儒学只有在这些文化中生根，才能以康庄的姿态回到中国，他的估计是有可能的。杜维明还认为，儒学要想在现时代得到真正的发展，确立起新儒家[哲学人类学](../Page/哲学人类学.md "wikilink")，必须与西方对话，在超越层面上与基督教对话，在社会政治层面上与马克思主义对话，在深度心理学层面上与[弗洛依德主义对话](../Page/弗洛依德主义.md "wikilink")，还须酌取[现象学](../Page/现象学.md "wikilink")、[解釋學的方法](../Page/解釋學.md "wikilink")。

## 著作

杜维明的著作主要有:《[今日儒家伦理](../Page/今日儒家伦理.md "wikilink")》、《[现代精神与儒家传统](../Page/现代精神与儒家传统.md "wikilink")》、《[人性与自我修养](../Page/人性与自我修养.md "wikilink")》、《[儒家思想：创造转化的人格](../Page/儒家思想：创造转化的人格.md "wikilink")》、《[新加坡的挑战](../Page/新加坡的挑战.md "wikilink")》等。

## 家庭

杜维明的前妻為郝若山(Rosanne Hall)，现在的妻子是[艾蓓](../Page/艾蓓.md "wikilink")。

## 相關條目

  - [波士頓儒家](../Page/波士頓儒家.md "wikilink")

[Category:美国儒学学者](../Category/美国儒学学者.md "wikilink")
[Category:民国儒学学者](../Category/民国儒学学者.md "wikilink")
[Category:新儒家學者](../Category/新儒家學者.md "wikilink")
[Category:哈佛大学教师](../Category/哈佛大学教师.md "wikilink")
[Category:普林斯顿大学教师](../Category/普林斯顿大学教师.md "wikilink")
[Category:哈佛大学校友](../Category/哈佛大学校友.md "wikilink")
[Category:东海大学校友](../Category/东海大学校友.md "wikilink")
[Category:臺北市立建国高级中学校友](../Category/臺北市立建国高级中学校友.md "wikilink")
[Category:归化美国公民的中华民国人](../Category/归化美国公民的中华民国人.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:百人会会员](../Category/百人会会员.md "wikilink")
[Category:台灣戰後雲南移民](../Category/台灣戰後雲南移民.md "wikilink")
[Category:台灣戰後廣東移民](../Category/台灣戰後廣東移民.md "wikilink")
[Category:南海人](../Category/南海人.md "wikilink")
[Category:昆明人](../Category/昆明人.md "wikilink")
[W维](../Category/杜姓.md "wikilink")
[Category:香港嶺南大學榮譽博士](../Category/香港嶺南大學榮譽博士.md "wikilink")
[Category:中央研究院第三十二屆院士](../Category/中央研究院第三十二屆院士.md "wikilink")
[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")