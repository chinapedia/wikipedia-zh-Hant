**杜蘭杜蘭**（），是1980年代至今歷久不衰英美流行音樂著名耀眼樂團之一，開創所謂[流行電音和](../Page/流行電音.md "wikilink")[新浪潮樂風之地位](../Page/新浪潮.md "wikilink")。\[1\]他們樂風以新浪潮為主要旋律，過往30年他們有21首單曲登上美國流行音樂[告示牌百大單曲榜](../Page/告示牌百大單曲榜.md "wikilink")，30首登上[英國單曲榜前](../Page/英國單曲榜.md "wikilink")40名，全球銷售超過1億張唱片輝煌紀錄，在[MTV全球電視音樂網掀起](../Page/MTV.md "wikilink")[英國第二次入侵美國流行音樂樂壇的態勢](../Page/英倫入侵.md "wikilink")。

取名自《[太空英雌巴巴麗娜](../Page/太空英雌巴巴麗娜.md "wikilink")》中的反派Doctor
Durand-Durand；唱腔早期頗脂粉味，近年可能年齡已升格為50歲「大叔」已略為陽剛；歌詞則多豔敘述男女歡愛與遊戲，頗有頹廢旖旎之風。

值得一提的是此樂團無論是MV或是舞台現場表演，極為誇大及戲劇化，或甚至是[惡搞](../Page/惡搞.md "wikilink")／搞笑始祖，例如：團員數名英美男歌手學[滿清](../Page/滿清.md "wikilink")[格格排排蓮步搖曳生姿走路](../Page/格格.md "wikilink")[單曲](../Page/單曲.md "wikilink")，但亦有氣勢磅礡之搖滾史詩風格。

幾位第一、二代團員離開另組新團，例如：，也維持一貫風格多樣化文藝卻又狂野樂風，是英美[流行音樂排行榜常勝軍樂團](../Page/流行音樂.md "wikilink")。

## 參考資料

## 外部連結

  -
[Category:英國搖滾乐团](../Category/英國搖滾乐团.md "wikilink")
[Category:搖滾乐团](../Category/搖滾乐团.md "wikilink")
[Category:葛萊美獎獲得者](../Category/葛萊美獎獲得者.md "wikilink")
[Category:帕洛風唱片歌手](../Category/帕洛風唱片歌手.md "wikilink")

1.