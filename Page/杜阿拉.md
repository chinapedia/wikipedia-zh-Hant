**杜阿拉**（[英語](../Page/英語.md "wikilink")、[法語](../Page/法語.md "wikilink")：**Douala**）是[喀麦隆最大的城市](../Page/喀麦隆.md "wikilink")，喀麦隆[利托拉省的省会](../Page/利托拉省.md "wikilink")。同时是喀麦隆最大的[港口](../Page/港口.md "wikilink")，港口位于西南沿海平原，距[大西洋](../Page/大西洋.md "wikilink")25公里。宽阔的[武里河和](../Page/武里河.md "wikilink")[大西洋相连](../Page/大西洋.md "wikilink")，形成一个天然良港。城市拥有一个主要的机场[杜阿拉机场离市区](../Page/杜阿拉机场.md "wikilink")12公里，杜阿拉是喀麦隆的「金融之都」，手握国家的出口命脉，是喀麦隆全国最大的工商业中心及交通枢纽，主要工业和出口有[石油](../Page/石油.md "wikilink")，[可可和](../Page/可可.md "wikilink")[咖啡](../Page/咖啡.md "wikilink")，这些物品很多都出口去[乍得](../Page/乍得.md "wikilink")。

城市建在武里河沿岸，两岸由BONABERI桥作连接。城市的人口在1991年时有160万，而现今已经突破了200万，城市的气候炎热潮湿。

## 历史

在大约1472年，[葡萄牙人抵达过这里](../Page/葡萄牙.md "wikilink")，这是[欧洲人首次来到这片土地](../Page/欧洲.md "wikilink")。
到了1650年内陆说[杜阿拉语的移民在这里建立了定居点](../Page/杜阿拉语.md "wikilink")，在18世纪这里成为了[大西洋](../Page/大西洋.md "wikilink")[黑奴贸易的中心](../Page/黑奴贸易.md "wikilink")。之后到了1884年这里成为[德国殖民者的统治范围](../Page/德国.md "wikilink")，也从原来的喀麦隆镇逐渐办成了Kamarunstadt（喀麦隆市），并且成为了德属喀麦隆的首都。1907年更名为现在的杜阿拉，在1919年城市变成了法属喀麦隆的一部分。城市在1940年至1946年成为喀麦隆的首都。

## 设施

Akwa是杜阿拉德夜生活中心，Bonanjo则是商业和行政中心。这一地区有着城市的主干道以及一些喀麦隆最高档的餐厅，咖啡店和法式点心坊；在城市的滨水区域可以找到一些酒吧和夜总会；Eko市场则是喀麦隆国内最大型的是市场。杜阿拉的人居消费水平是[非洲最高的城市](../Page/非洲.md "wikilink")，在全球排第27位，名列[台北和](../Page/台北.md "wikilink")[洛杉矶之前](../Page/洛杉矶.md "wikilink")。\[1\]
近年来，杜阿拉也逐渐成为在喀麦隆[中国人最集中的地方](../Page/中国人.md "wikilink")。据不完全统计，目前有大约2000名中国商人在杜阿拉从事鞋帽箱包、服装等批发和[零售业务](../Page/零售.md "wikilink")。在杜阿拉市中心不足200米的阿玛卡伊久大街两侧开设了多家中国商城。\[2\]

## 杜阿拉港

### 发展

[Kribi_-_port_de_pêche.JPG](https://zh.wikipedia.org/wiki/File:Kribi_-_port_de_pêche.JPG "fig:Kribi_-_port_de_pêche.JPG")
[Partie_du_Marché_de_Mélong_(Littoral).jpg](https://zh.wikipedia.org/wiki/File:Partie_du_Marché_de_Mélong_\(Littoral\).jpg "fig:Partie_du_Marché_de_Mélong_(Littoral).jpg")商店街\]\]
杜阿拉作为非洲中部地区的大港，吞吐量占喀麦隆全国总吞吐量的96%；而且喀麦隆80％的工业和95％的进出口贸易都汇集杜阿拉，所以[港口是杜阿拉非常重要的城市建设](../Page/港口.md "wikilink")。\[3\]
在1998年港口进行了管理改制和港区秩序整顿，在基础设施建设和管理水平都得到显著改善。为了改善航运条件，1998年7月起开始疏浚了杜阿拉25公里长的内河航道；新增高架起重机加快货轮卸货进度，清理[集装箱码头使得进出港区更为便捷](../Page/集装箱.md "wikilink")；也提高了集装箱通关效率。另外，港口的冷藏、清淤、导航等服务项目已逐步转变为由私营企业租赁经营，效率明显提高。喀麦隆总统[保罗·比亚决定](../Page/保罗·比亚.md "wikilink")，在国家财政给于支持的情况下将包括港口在内的杜阿拉市建设成为「中非地区的橱窗」，从而达到吸引世界各国投资者在杜阿拉投资的目的。\[4\]

### 劣势

杜阿拉的不足之处也是十分明显的。城市缺乏商务区，电力、用水等问题也供应不良，尤其突出的问题是港区航道吃水线不够。目前航道仅深7．5米，即使涨潮时也只有9米多。不仅疏浚航道每年需要花费40多亿[非洲法郎](../Page/非洲法郎.md "wikilink")，即使将现有航道掘深到10米，外国远洋巨轮也难以进入港口，这极大地限制了杜阿拉港的发展。\[5\]

2019年有報導稱進出口商不滿杜阿拉港官員貪腐嚴重、[官僚手續繁瑣和收費高昂](../Page/繁文縟禮.md "wikilink")，深受其害的內陸國家[乍得和](../Page/乍得.md "wikilink")[中非共和國的商家正在物色其它國家的港口代替杜阿拉港](../Page/中非共和國.md "wikilink")。\[6\]

## 天气

杜阿拉属[热带雨林气候](../Page/热带雨林气候.md "wikilink")。年平均气温24-28[℃](../Page/攝氏.md "wikilink")。每年11月至次年1月为雾季，每月平均有雾六天。全年平均降雨量约3000毫米。平均潮高：高潮为2.5[米](../Page/米.md "wikilink")，低潮为0.5米。\[7\]

## 交通

杜阿拉拥有抵达[雅温得](../Page/雅温得.md "wikilink")，[恩冈代雷](../Page/恩冈代雷.md "wikilink")，[昆巴和](../Page/昆巴.md "wikilink")[恩康桑巴的铁路线](../Page/恩康桑巴.md "wikilink")。而[公路可以直达中非首都](../Page/公路.md "wikilink")[班吉和乍得首都](../Page/班吉.md "wikilink")[恩贾梅纳](../Page/恩贾梅纳.md "wikilink")。

航空方面杜阿拉拥有一个[国际机场](../Page/杜阿拉国际机场.md "wikilink")。

## 体育

在杜阿拉足球是非常普及的体育活动，培养出多位[喀麦隆国脚](../Page/喀麦隆国家足球队.md "wikilink")，现[巴塞罗那球星](../Page/巴塞罗那足球俱乐部.md "wikilink")[萨缪埃尔·埃托奥就出自这个城市的球会卡吉体育学院](../Page/萨缪埃尔·埃托奥.md "wikilink")。城市有三支甲级联赛球员，分别是[卡吉体育学院](../Page/卡吉体育学院足球俱乐部.md "wikilink")，[杜阿拉星和](../Page/杜阿拉星足球俱乐部.md "wikilink")[杜阿拉联合](../Page/杜阿拉联合足球俱乐部.md "wikilink")。其中杜阿拉联合曾经获得过非洲足球俱乐部最高荣誉[非洲俱乐部冠军杯和](../Page/非洲俱乐部冠军杯.md "wikilink")[非洲优胜者杯的冠军](../Page/非洲优胜者杯.md "wikilink")。

## 姊妹城市

  - [宾西法尼亚州](../Page/宾西法尼亚州.md "wikilink")[费城](../Page/费城.md "wikilink")

  - [山西省](../Page/山西省.md "wikilink")[太原市](../Page/太原市.md "wikilink")

## 参考资料

<references />

## 外部链接

  - [Google
    Earth杜阿拉衛星地圖](http://maps.google.com/maps?f=q&hl=en&q=douala,+cameroon&ie=UTF8&z=12&ll=4.071467,9.709854&spn=0.154448,0.346069&t=h&om=1&iwloc=A%7CSatelite)
  - [FallingRain
    Map地图](http://www.fallingrain.com/world/CM/5/Douala2.html)
  - [文中译名采用中国官方—世界政区喀麦隆](https://web.archive.org/web/20061230223531/http://www.xzqh.org/waiguo/africa/3026.htm)
    www.xzqh.org 2007年1月27日造访

[Category:喀麦隆城市](../Category/喀麦隆城市.md "wikilink")
[Category:大西洋沿海城市](../Category/大西洋沿海城市.md "wikilink")
[Category:港口](../Category/港口.md "wikilink")

1.  [Cost of Living Survey - Worldwide
    Rankings 2006](http://www.finfacts.com/costofliving.htm) -
    www.planetworldcup.com - 2007年1月27日造访
2.  [开拓喀麦隆市场
    中国要靠自主品牌](http://202.84.17.25/www/Article/200610972710-1.shtml)
     - 新华网 - 原载《[经济参考报](../Page/经济参考报.md "wikilink")》 2006年10月8日 袁晔著
3.  [商务手册－世界产油国概况](http://www.oilchina.com/syswsc/sjcygkk/kml.htm)  -
    中国石油商务网－2007年1月28日造访
4.  [杜阿拉港迅速崛起——喀麦隆纪行第五段](http://www.people.com.cn/GB/guoji/2290786.html)
    - 人民网 - 原载《[人民日报](../Page/人民日报.md "wikilink")》 2004年1月12日 第三版 赵章云著
5.  [杜阿拉港迅速崛起——喀麦隆纪行第六段](http://www.people.com.cn/GB/guoji/2290786.html)
    - 人民网 -
6.
7.  [杜阿拉口岸物流咨讯平台](http://www.easipass.com/ytsce/gk/ytsce_gksy_fz_14.htm)
     - 亿同网 - 2007年1月28日造访