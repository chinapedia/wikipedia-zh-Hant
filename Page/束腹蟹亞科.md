[thumb](../Page/file:Cave_crab_\(Arachnothelphusa_rhadamanthysi\).jpg.md "wikilink")''
Cave crab, [Borneo](../Page/Borneo.md "wikilink")\]\]
**束腰蟹亞科**或**束腹蟹亞科**()， 舊作**束腰蟹科**或**束腹蟹科**()，
原是[短尾下目](../Page/短尾下目.md "wikilink")（[螃蟹](../Page/螃蟹.md "wikilink")）[澤蟹總科之下兩個科的其中一個科](../Page/澤蟹總科.md "wikilink")。2009年，被降格成為[澤蟹科之下的一個](../Page/澤蟹科.md "wikilink")[亞科](../Page/亞科.md "wikilink")。本亞科物種分佈於[南亞及](../Page/南亞.md "wikilink")[東南亞](../Page/東南亞.md "wikilink")，但亦見於[亞洲其他地區及](../Page/亞洲.md "wikilink")[澳大利亞](../Page/澳大利亞.md "wikilink")。

## 分類

本亞科包括40個屬，分別如下：

  - *[Adelaena](../Page/Adelaena.md "wikilink")*
  - *[Arachnothelphusa](../Page/Arachnothelphusa.md "wikilink")*
  - *[Austrothelphusa](../Page/Austrothelphusa.md "wikilink")*
  - *[Bakousa](../Page/Bakousa.md "wikilink")*
  - *[Bassiathelphusa](../Page/Bassiathelphusa.md "wikilink")*
  - *[Ceylonthelphusa](../Page/Ceylonthelphusa.md "wikilink")*
  - *[Clinothelphusa](../Page/Clinothelphusa.md "wikilink")*
  - *[Coccusa](../Page/Coccusa.md "wikilink")*
  - *[Currothelphusa](../Page/Currothelphusa.md "wikilink")*
  - *[Esanthelphusa](../Page/Esanthelphusa.md "wikilink")*
  - *[Geelvinkia](../Page/Geelvinkia.md "wikilink")*
  - *[Geithusa](../Page/Geithusa.md "wikilink")*
  - *[Heterothelphusa](../Page/Heterothelphusa.md "wikilink")*
  - *[Holthuisana](../Page/Holthuisana.md "wikilink")*
  - *[Irmengardia](../Page/Irmengardia.md "wikilink")*
  - *[Mahatha](../Page/Mahatha.md "wikilink")*
  - *[Mainitia](../Page/Mainitia.md "wikilink")*
  - *[Mekhongthelphusa](../Page/Mekhongthelphusa.md "wikilink")*
  - *[Migmathelphusa](../Page/Migmathelphusa.md "wikilink")*
  - *[Nautilothelphusa](../Page/Nautilothelphusa.md "wikilink")*
  - *[Niasathelphusa](../Page/Niasathelphusa.md "wikilink")*
  - *[Oziotelphusa](../Page/Oziotelphusa.md "wikilink")*
  - *[Parathelphusa](../Page/Parathelphusa.md "wikilink")*
  - *[Pastilla](../Page/Pastilla_\(crab\).md "wikilink")*
  - *[Perbrinckia](../Page/Perbrinckia.md "wikilink")*
  - *[Perithelphusa](../Page/Perithelphusa.md "wikilink")*
  - *[Rouxana](../Page/Rouxana.md "wikilink")*
  - *[Salangathelphusa](../Page/Salangathelphusa.md "wikilink")*
  - *[Sartoriana](../Page/Sartoriana.md "wikilink")*
  - *[Sayamia](../Page/Sayamia.md "wikilink")*
  - *[Sendleria](../Page/Sendleria.md "wikilink")*
  - *[Siamthelphusa](../Page/Siamthelphusa.md "wikilink")*
  - *[Somanniathelphusa](../Page/Somanniathelphusa.md "wikilink")*
  - *[Spiralothelphusa](../Page/Spiralothelphusa.md "wikilink")*
  - *[Stygothelphusa](../Page/Stygothelphusa.md "wikilink")*
  - *[Sundathelphusa](../Page/Sundathelphusa.md "wikilink")*
  - *[Syntripsa](../Page/Syntripsa.md "wikilink")*
  - *[Terrathelphusa](../Page/Terrathelphusa.md "wikilink")*
  - *[Thelphusula](../Page/Thelphusula.md "wikilink")*
  - *[Torhusa](../Page/Torhusa.md "wikilink")*

## 參考文獻

## 外部連結

  -

[束腹蟹亞科](../Category/束腹蟹亞科.md "wikilink")