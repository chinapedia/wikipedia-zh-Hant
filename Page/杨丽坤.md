**杨丽坤**（），[中国电影女演员](../Page/中国.md "wikilink")，[彝族人](../Page/彝族.md "wikilink")。

## 生平

杨丽坤出生于[云南省](../Page/云南省.md "wikilink")[普洱县](../Page/宁洱哈尼族彝族自治县.md "wikilink")[磨黑镇](../Page/磨黑镇.md "wikilink")，自幼喜爱文艺。她有兄弟姐妹11个，排行第9，人们都叫她“小九儿”。1954年入[云南省歌舞团作独舞演员](../Page/云南省歌舞团.md "wikilink")。1959年18岁时被[长春电影制片厂的导演](../Page/长春电影制片厂.md "wikilink")[王家乙选中](../Page/王家乙.md "wikilink")，主演反映[大跃进中](../Page/大跃进.md "wikilink")[白族人进行](../Page/白族.md "wikilink")[社会主义建设的爱情喜剧影片](../Page/社会主义.md "wikilink")《[五朵金花](../Page/五朵金花.md "wikilink")》中的女主角副社长金花。影片于1959年12月公演，立刻轰动，在46个国家上映\[1\]，获1960年埃及第二届亚非电影节最佳电影银鹰奖，杨丽坤也获得最佳女演员银鹰奖。之后几年，杨丽坤受到过[毛泽东](../Page/毛泽东.md "wikilink")、[周恩来](../Page/周恩来.md "wikilink")、[陈毅等领导人的接见](../Page/陈毅.md "wikilink")，并多次随歌舞团出国演出。1964年主演了反映古代云南彝族[撒尼人爱情故事的影片](../Page/撒尼人.md "wikilink")《[阿诗玛](../Page/阿诗玛_\(电影\).md "wikilink")》中的女主角[阿诗玛](../Page/阿诗玛.md "wikilink")。

1965年，在《阿诗玛》上映前夕，[文化部因为影片](../Page/中华人民共和国文化部.md "wikilink")“不歌颂社会主义革命的大好局面，为死人作传”、“宣扬爱情至上充满[资产阶级情调](../Page/资产阶级.md "wikilink")”停止发行《阿诗玛》，拷贝封存\[2\]\[3\]。她在此之前因恋爱受人阻挠，曾一度精神失常，经过住院治疗，已经痊愈\[4\]。但这次成了“黑苗子”、“黑线人物”。在[文化大革命开始后迫害折磨](../Page/文化大革命.md "wikilink")，患[精神分裂症](../Page/精神分裂症.md "wikilink")，曾一度失踪流浪到边陲小镇[镇沅](../Page/镇沅.md "wikilink")，被找到后病情稍有缓解。1970年又因为一些政治言论被[扣帽子成为](../Page/扣帽子.md "wikilink")“现行反革命分子”，精神彻底崩溃。

1973年杨丽坤和[籍贯](../Page/籍贯.md "wikilink")[上海的唐风楼结婚](../Page/上海.md "wikilink")。文革结束后1978年到上海居住，名义上任[上海电影制片厂演员](../Page/上海电影制片厂.md "wikilink")。她过上了比较安定的生活，病情有所缓解。2000年7月21日因长期的病痛折磨和身体机能的衰退，在上海家中病逝。她的墓碑在上海和[昆明各有一座](../Page/昆明.md "wikilink")，[骨灰一边一半](../Page/骨灰.md "wikilink")\[5\]。

杨丽坤和唐风楼育有一对[双胞胎儿子](../Page/双胞胎.md "wikilink")。

## 注释

<div class="references-small">

<references/>

</div>

[Y](../Category/中国女演员.md "wikilink") [Y](../Category/宁洱人.md "wikilink")
[Y](../Category/彝族人.md "wikilink") [L丽坤](../Category/杨姓.md "wikilink")

1.  也有说35个国家，参见
2.  [杨丽坤的悲惨遭遇](http://www.gmw.cn/01wzb/2005-02/24/content_185041.htm)
3.  《阿诗玛》在1978年首映。
4.  [杨丽坤的艺术生涯与悲剧性遭遇](http://unn.people.com.cn/GB/channel23/178/881/882/200010/19/2864.html)
5.  [《重訪》——五朵金花（上，下）](http://www.cctv.com/program/chongfang/20060608/104387.shtml)