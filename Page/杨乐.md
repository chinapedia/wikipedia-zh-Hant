**杨乐**（），[中国](../Page/中国.md "wikilink")[数学家](../Page/数学家.md "wikilink")，[中国科学院院士](../Page/中国科学院.md "wikilink")，[华罗庚数学奖得主](../Page/华罗庚数学奖.md "wikilink")，[江苏](../Page/江苏.md "wikilink")[南通人](../Page/南通.md "wikilink")。

## 生平

杨乐1956年毕业于[江苏省南通中学](../Page/江苏省南通中学.md "wikilink")，1962年毕业于[北京大学数学系](../Page/北京大学.md "wikilink")，后与[张广厚一同考入著名数学家](../Page/张广厚.md "wikilink")[熊庆来教授门下](../Page/熊庆来.md "wikilink")，进行复分析研究。1977年，他同[张广厚合作](../Page/张广厚.md "wikilink")，通过对[整函数与](../Page/整函数.md "wikilink")[亚纯函数](../Page/亚纯函数.md "wikilink")[亏值与](../Page/亏值.md "wikilink")[波莱尔方向的深入研究](../Page/波莱尔方向.md "wikilink")，证明了[张杨定理](../Page/张杨定理.md "wikilink")，建立了这两个基本概念之间具体的联系。1980年当选为中国科学院数学物理学部学部委员（院士）。1982年担任中国科学院数学研究所副所长，1987年任所长。1997年，获得[华罗庚数学奖](../Page/华罗庚数学奖.md "wikilink")，[陈嘉庚奖数理科学奖](../Page/陈嘉庚奖.md "wikilink")。

## 个人生活

杨乐还是著名水利专家[黄万里的女婿](../Page/黄万里.md "wikilink")。

{{-}}

[Category:华罗庚数学奖获得者](../Category/华罗庚数学奖获得者.md "wikilink")
[Category:中国科学院数学物理学部院士](../Category/中国科学院数学物理学部院士.md "wikilink")
[Y杨](../Category/中国数学家.md "wikilink")
[Y杨](../Category/南通人.md "wikilink")
[Y杨](../Category/北京大学校友.md "wikilink")
[Category:中国数学会理事长](../Category/中国数学会理事长.md "wikilink")
[Category:杨姓](../Category/杨姓.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")