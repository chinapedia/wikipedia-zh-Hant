**杨小燕**（，），著名[美籍华裔](../Page/美籍华裔.md "wikilink")[桥牌大师](../Page/桥牌.md "wikilink")，现任[中国桥牌协会顾问](../Page/中国桥牌协会.md "wikilink")。

杨祖籍[湖南](../Page/湖南.md "wikilink")[新化县](../Page/新化县.md "wikilink")，出生于[北京](../Page/北京.md "wikilink")，其父是[燕京大学教授](../Page/燕京大学.md "wikilink")[杨开道](../Page/杨开道.md "wikilink")，1949年移居[美国](../Page/美国.md "wikilink")。不久，杨在父母安排下与[沈昌瑞](../Page/沈昌瑞.md "wikilink")（[沈昌焕之弟](../Page/沈昌焕.md "wikilink")）结婚，婚后开始学习桥牌。1967年杨与沈离婚，转而嫁给美籍华人企业家[魏重庆](../Page/魏重庆.md "wikilink")。魏也是桥牌高手，以[精确叫牌法的发明者闻名国际牌坛](../Page/精準制.md "wikilink")。在魏的鼓励下，杨师从多位名师，很快成为国际高手。

1969年，魏重庆和杨小燕代表[中華民國出战](../Page/中華民國.md "wikilink")[百慕大杯](../Page/百慕大杯.md "wikilink")，一举击败美国队，最终夺得亚军。1971年，杨辞去工作，成为专职牌手。此后，杨曾经三次代表美国国家桥牌队获得[威尼斯杯世界冠军](../Page/威尼斯杯.md "wikilink")，两次获得世界队式锦标赛冠军，成为[世界桥牌联合会的终身大师](../Page/世界桥牌联合会.md "wikilink")。

1987年，魏重庆去世，1992年，杨与现任丈夫建筑师桑德结婚，婚后随丈夫皈依[犹太教](../Page/犹太教.md "wikilink")。杨在1981年首次访问[中华人民共和国](../Page/中华人民共和国.md "wikilink")，此后又多次访华，与[邓小平](../Page/邓小平.md "wikilink")、[万里等中国领导人成为密友](../Page/万里.md "wikilink")。后来并成为中国女桥牌队的教练和中国桥牌协会的顾问。

## 参考资料

  - [杨小燕——桥牌女皇的传奇人生](http://www.2008red.com/member_pic_108/files/bridge123456/html/article_4603_1.shtml)

[Category:中国桥牌选手](../Category/中国桥牌选手.md "wikilink")
[Category:美国运动员](../Category/美国运动员.md "wikilink")
[Category:歸化美國公民的中華民國人](../Category/歸化美國公民的中華民國人.md "wikilink")
[Category:美国华裔运动员](../Category/美国华裔运动员.md "wikilink")
[Category:美国改信犹太教者](../Category/美国改信犹太教者.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:新化县人](../Category/新化县人.md "wikilink")
[X](../Category/杨姓.md "wikilink")
[Category:中国桥牌协会顾问](../Category/中国桥牌协会顾问.md "wikilink")