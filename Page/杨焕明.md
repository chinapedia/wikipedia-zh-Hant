**杨焕明**（），[中国](../Page/中国.md "wikilink")[基因组学科学家](../Page/基因组学.md "wikilink")，曾任[中国科学院北京基因组研究所所长](../Page/中国科学院.md "wikilink")，现任[华大基因理事长](../Page/华大基因.md "wikilink")。

## 生平

1952年出生于[浙江](../Page/浙江.md "wikilink")[温州](../Page/温州.md "wikilink")[乐清](../Page/乐清.md "wikilink")。1975年至1978年，就读于[杭州大学生物系](../Page/杭州大学.md "wikilink")。1978年至1979年，任[温州医学院生物教研室任助教](../Page/温州医学院.md "wikilink")。1979年至1982年，在[南京铁道医学院](../Page/南京铁道医学院.md "wikilink")（今[东南大学医学院](../Page/东南大学.md "wikilink")）攻读生物学硕士学位。1982年至1984年，任南京铁道医学院生物系任讲师。1984年至1988年，在[丹麦](../Page/丹麦.md "wikilink")[哥本哈根大学遗传医学研究所攻读博士学位](../Page/哥本哈根大学.md "wikilink")。1988年至1994年，先后在法国马赛免疫中心、美国[哈佛大学医学院及](../Page/哈佛大学.md "wikilink")[加利福尼亚大学洛杉矶分校医学院从事博士后研究](../Page/加利福尼亚大学洛杉矶分校.md "wikilink")。1994年至1998年，于中国医学科学院[协和医科大学任教授](../Page/协和医科大学.md "wikilink")、[博导](../Page/博士生导师.md "wikilink")。1998年至2003年，任中国科学院遗传所人类基因组中心主任。1999年起，任北京华大基因研究中心主任。2003年至2008年，任中国科学院北京基因组研究所所长。现任深圳华大基因研究所董事长。\[1\]

2007年当选为[中国科学院院士](../Page/中国科学院.md "wikilink")，2008年当选为[发展中国家科学院](../Page/发展中国家科学院.md "wikilink")（TWAS）院士，2009年当选为印度国家科学院外籍院士，2012年当选为[德国科学院外籍院士](../Page/德国科学院.md "wikilink")，2014年当选为[美国国家科学院外籍院士](../Page/美国国家科学院.md "wikilink")。\[2\]2017年5月，获首届[全国创新争先奖章](../Page/全国创新争先奖.md "wikilink")\[3\]。

## 参考文献

## 参见

  - [炎黄计划](../Page/炎黄计划.md "wikilink")

{{-}}

[Category:中华人民共和国遗传学家](../Category/中华人民共和国遗传学家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:杭州大学校友](../Category/杭州大学校友.md "wikilink")
[Category:东南大学校友](../Category/东南大学校友.md "wikilink")
[Category:哥本哈根大學校友](../Category/哥本哈根大學校友.md "wikilink")
[Category:浙江科学家](../Category/浙江科学家.md "wikilink")
[Category:乐清人](../Category/乐清人.md "wikilink")
[H](../Category/杨姓.md "wikilink")
[Category:中国科学院生命科学和医学学部院士‎](../Category/中国科学院生命科学和医学学部院士‎.md "wikilink")
[Category:发展中国家科学院院士‎](../Category/发展中国家科学院院士‎.md "wikilink")
[Category:全国创新争先奖获得者‎](../Category/全国创新争先奖获得者‎.md "wikilink")

1.
2.
3.