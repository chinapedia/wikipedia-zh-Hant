**楊至成**，原名**杨序清**，[貴州](../Page/貴州.md "wikilink")[三穗縣人](../Page/三穗縣.md "wikilink")，[侗族](../Page/侗族.md "wikilink")。[中國人民解放軍上將](../Page/中國人民解放軍.md "wikilink")。

杨至成是[黄埔军校第五期毕业](../Page/黄埔军校.md "wikilink")，参加[南昌起义](../Page/南昌起义.md "wikilink")。[第一次国共内战时期](../Page/第一次国共内战.md "wikilink")，历任工农革命军第4军28团连长、井冈山留守处主任、[红四军副官长](../Page/红四军_\(红一方面军\).md "wikilink")、[紅十二軍副官長](../Page/紅十二軍.md "wikilink")、[红军大学校务部部长](../Page/红军大学.md "wikilink")，红军总兵站站长，[军委总供给部部长兼政治委员](../Page/军委.md "wikilink")，参加[长征](../Page/长征.md "wikilink")。到达陕北后，任[红一方面军后勤部部长](../Page/红一方面军.md "wikilink")，军委后勤部部长兼[红军前敌总指挥部总兵站部部长](../Page/红军前敌总指挥部.md "wikilink")，黄河两延卫戍司令员。[抗日战争时期](../Page/抗日战争.md "wikilink")，任[抗日军政大学校务部部长](../Page/抗日军政大学.md "wikilink")。1938年赴[蘇聯](../Page/蘇聯.md "wikilink")[伏龙芝军事学院学习](../Page/伏龙芝军事学院.md "wikilink")。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，任[东北民主联军总后勤部政治委员](../Page/东北民主联军.md "wikilink")，[东北野战军军需部部长](../Page/东北野战军.md "wikilink")，[第四野战军军需部部长](../Page/第四野战军.md "wikilink")，[中南军政委员会轻工业部部长](../Page/中南军政委员会.md "wikilink")。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，任中南军区第一副参谋长兼后勤部部长、[中国人民解放军武装力量監察部副部長](../Page/中国人民解放军武装力量監察部.md "wikilink")、[军事科学院副院长](../Page/军事科学院.md "wikilink")、[高等军事学院副院长等职](../Page/高等军事学院.md "wikilink")。

## 生平

### 早期经历

1919年在贵州甲种农业学校学习期间，曾参加过贵州学生声援五四运动的游行活动。农校毕业后加入滇黔联军\[1\]。1926年3月，考入[黄埔军校第五期](../Page/黄埔军校.md "wikilink")，在军校经[周逸群介绍加入](../Page/周逸群.md "wikilink")[中国共产主义青年团](../Page/中国共产主义青年团.md "wikilink")，次年3月转人[中国共产党](../Page/中国共产党.md "wikilink")，接着在[国民革命军第二十军任连指导员](../Page/国民革命军第二十军.md "wikilink")，参加[南昌起义](../Page/南昌起义.md "wikilink")。1928年1月，杨至成参加[湘南起义](../Page/湘南起义.md "wikilink")，在作战中右腿受伤。同年4月随[朱德](../Page/朱德.md "wikilink")、[陈毅到井冈山](../Page/陈毅.md "wikilink")，任工农革命军第4军28团连长，在战争中身负重伤。1928年7月，他担任井冈山留守处主任。1929年，任[红四军副官长](../Page/红四军_\(红一方面军\).md "wikilink")\[2\]。

1930年任[紅十二軍副官長](../Page/紅十二軍.md "wikilink").
后任[红军大学校务部部长](../Page/红军大学.md "wikilink")，红军总兵站站长，[军委总供给部部长兼政治委员](../Page/军委.md "wikilink")，组织兵工、军需、医药生产和物资供给。1934年10月参加[长征](../Page/长征.md "wikilink")，1935年1月任军委先遣工作团主任。到达陕北后，任[红一方面军后勤部部长](../Page/红一方面军.md "wikilink")，军委后勤部部长兼[红军前敌总指挥部总兵站部部长](../Page/红军前敌总指挥部.md "wikilink")\[3\]。

### 抗日战争与第二次国共内战时期

1937年6月，杨至成任黄河两延卫戍司令员，此后又任[抗日军政大学校务部部长](../Page/抗日军政大学.md "wikilink")。1938年赴[蘇聯治病養傷](../Page/蘇聯.md "wikilink")，此后进入苏共远东局党校、[伏龙芝军事学院学习](../Page/伏龙芝军事学院.md "wikilink")\[4\]。1946年，随[李立三回國](../Page/李立三.md "wikilink")。1946年1月回国后，任[东北民主联军总后勤部政治委员](../Page/东北民主联军.md "wikilink")，组织[四平战役的后勤供给](../Page/四平战役.md "wikilink")。其后，在[佳木斯](../Page/佳木斯.md "wikilink")、[哈尔滨](../Page/哈尔滨.md "wikilink")、[牡丹江](../Page/牡丹江.md "wikilink")、[齐齐哈尔和](../Page/齐齐哈尔.md "wikilink")[鸡西等地组织领导军工生产](../Page/鸡西.md "wikilink")。1948年任[东北野战军军需部部长](../Page/东北野战军.md "wikilink")\[5\]，负责[辽沈战役](../Page/辽沈战役.md "wikilink")、[平津战役的物资保障](../Page/平津战役.md "wikilink")\[6\]。1949年，任[第四野战军军需部部长](../Page/第四野战军.md "wikilink")，[中南军政委员会轻工业部部长](../Page/中南军政委员会.md "wikilink")。

### 中华人民共和国成立后

1954年2月，杨至成被任命为中南军区第一副参谋长兼后勤部部长。同年9月任[中国人民解放军武装力量監察部副部長](../Page/中国人民解放军武装力量監察部.md "wikilink")。1955年被授銜[上將](../Page/上將.md "wikilink")，获一级[八一勋章](../Page/八一勋章_\(1955年\).md "wikilink")、二级[独立自由勋章](../Page/独立自由勋章.md "wikilink")、一级[解放勋章](../Page/解放勋章.md "wikilink")。1958年，杨至成任[军事科学院副院长](../Page/军事科学院.md "wikilink")\[7\]。1962年初，杨至成调任[高等军事学院副院长](../Page/高等军事学院.md "wikilink")。曾任2、3屆[國防委員會委員](../Page/中华人民共和国国防委员会.md "wikilink")、第3屆[全國人民代表大會常務委員會委員](../Page/全國人民代表大會常務委員會.md "wikilink")，1967年2月2日因病在[北京逝世](../Page/北京.md "wikilink")\[8\]。

## 参考资料

[Category:中国人民解放军上将](../Category/中国人民解放军上将.md "wikilink")
[Category:三穗县人](../Category/三穗县人.md "wikilink")
[Category:侗族人](../Category/侗族人.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")
[Category:黄埔军校第五期](../Category/黄埔军校第五期.md "wikilink")
[Category:伏龙芝军事学院校友](../Category/伏龙芝军事学院校友.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.