**杭州—瑞丽高速公路**，简称**杭瑞高速**，[中国国家高速公路网](../Page/中国国家高速公路网.md "wikilink")\[1\]\[2\]编号为**G56**，起点在[杭州](../Page/杭州.md "wikilink")，途经[黄山](../Page/黄山市.md "wikilink")、[景德镇](../Page/景德镇.md "wikilink")、[九江](../Page/九江.md "wikilink")、[咸宁](../Page/咸宁.md "wikilink")、[岳阳](../Page/岳阳.md "wikilink")、[常德](../Page/常德.md "wikilink")、[吉首](../Page/吉首.md "wikilink")、[遵义](../Page/遵义.md "wikilink")、[毕节](../Page/毕节.md "wikilink")、[六盘水](../Page/六盘水.md "wikilink")、[曲靖](../Page/曲靖.md "wikilink")、[昆明](../Page/昆明.md "wikilink")、[大理](../Page/大理白族自治州.md "wikilink")，终点在[瑞丽](../Page/瑞丽.md "wikilink")，全长3405公里。规划路线[曲靖至](../Page/曲靖.md "wikilink")[昆明段与](../Page/昆明.md "wikilink")[**G60**沪昆高速重线](../Page/沪昆高速公路.md "wikilink")。

其中自[云南省](../Page/云南省.md "wikilink")[昆明市石虎关立交桥至](../Page/昆明市.md "wikilink")[云南省](../Page/云南省.md "wikilink")[瑞丽市路段是](../Page/瑞丽市.md "wikilink")的一部分。

## 分省介绍

  - [浙江段](../Page/浙江.md "wikilink")：全通
  - [安徽段](../Page/安徽.md "wikilink")：全通
  - [江西段](../Page/江西.md "wikilink")：全通

[G56_in_Hubei.svg](https://zh.wikipedia.org/wiki/File:G56_in_Hubei.svg "fig:G56_in_Hubei.svg")

  - [湖北段](../Page/湖北.md "wikilink")：全通
  - [湖南段](../Page/湖南.md "wikilink")：临岳段(临湘大界到岳阳君山)未通。包括岳常高速（2013年12月30日通车）、常吉高速（2008年12月18日通车）、吉茶高速。其中岳常段位于洞庭湖平原区，而常吉段、吉茶段均位于湖南西部的武陵山区，桥隧比重大，吉茶段有著名的[矮寨大桥](../Page/矮寨大桥.md "wikilink")，常吉段隧道众多，且每个隧道口都进行了不同的设计。目前全通
  - [贵州段](../Page/贵州.md "wikilink")：[毕节至](../Page/毕节.md "wikilink")[遵义段已通](../Page/遵义.md "wikilink")
  - [云南段](../Page/云南.md "wikilink")：全通

## 图集

201901 G56 Expressway in Sanyang,
Shexian.jpg|[安徽](../Page/安徽.md "wikilink")[歙县三阳段](../Page/歙县.md "wikilink")
2凉水井隧道.jpeg|湖南段常吉高速凉水井隧道 G56 Ruili Direction Hunan
Section.jpg|G56湖南[湘西州段](../Page/湘西州.md "wikilink") G56
Hangzhou Direction Guizhou Tongren Section
1.jpg|G56[贵州](../Page/贵州.md "wikilink")[铜仁段](../Page/铜仁.md "wikilink")
G56 Hangzhou Direction Guizhou Tongren Section
2.jpg|G56[贵州](../Page/贵州.md "wikilink")[铜仁段](../Page/铜仁.md "wikilink")
G56 Hangzhou Direction Guizhou Tongren Section
3.jpg|G56[贵州](../Page/贵州.md "wikilink")[铜仁段](../Page/铜仁.md "wikilink")
G56 Ruili Direction Tongren Daxing Toll Gate.jpg|G56铜仁大兴主线收费站（湘黔界）
亚洲公路14号线昆明附近路牌.jpg|亚洲公路14号线与G56并行路段，[云南省](../Page/云南省.md "wikilink")[昆明市附近路牌](../Page/昆明市.md "wikilink")。

## 参考文献

## 参见

  - [中国公路命名与编号](../Page/中国公路命名与编号.md "wikilink")
  - [中国国家高速公路网](../Page/中国国家高速公路网.md "wikilink")
  - [沪瑞高速公路](../Page/沪瑞高速公路.md "wikilink")
  - [杭徽高速公路](../Page/杭徽高速公路.md "wikilink")
  - [黄塔桃高速公路](../Page/黄塔桃高速公路.md "wikilink")

[Category:国家高速公路网横向线](../Category/国家高速公路网横向线.md "wikilink")
[Category:贵州省高速公路](../Category/贵州省高速公路.md "wikilink")
[Category:云南省高速公路](../Category/云南省高速公路.md "wikilink")
[Category:德宏交通](../Category/德宏交通.md "wikilink")
[Category:杭州交通](../Category/杭州交通.md "wikilink")

1.  [国家高速公路网规划简介](http://zfxxgk.ndrc.gov.cn/PublicItemView.aspx?ItemID=%7B4f589ce2-c398-4770-858d-3036b98fa405%7D)

2.  [国家公路网规划(2013年-2030年)](http://zfxxgk.ndrc.gov.cn/PublicItemView.aspx?ItemID=%7B93c7d13b-aa0d-4beb-955e-268adade8a8f%7D)