**杭立武**（），[中華民國教育家](../Page/中華民國.md "wikilink")、政治学家、政治家、外交家、社会活动家。于[安徽省](../Page/安徽省.md "wikilink")[滁州出生](../Page/滁州.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[杭州](../Page/杭州.md "wikilink")。一生历任教育、文化、外交、社团等各方要职，晚年仍积极不懈地从事国民外交、保障人权和救助难民等工作。

## 早年經歷

1923年，毕业于[金陵大学文学院](../Page/金陵大学.md "wikilink")。同年以第一名的成绩考取安徽省公费留学名额，赴英国[伦敦大学深造](../Page/伦敦大学.md "wikilink")，其间赴美国[威斯康辛大学担任名誉研究员](../Page/威斯康辛大学.md "wikilink")，获硕士学位后返英国继续学习，1929年获伦敦大学政治学博士学位。回国后任[国立中央大学政治系教授](../Page/国立中央大学.md "wikilink")，曾兼任系主任。

1931年，任中英庚款董事会总干事。1932年，创立中国政治学会并兼任总干事。1933年，在南京成立中英文化协会，此后中法、中比、中瑞、中美等文化协会亦相继成立。

## 中年經歷

[抗战期间](../Page/中國抗日戰爭.md "wikilink")，任[国民参政会参议员](../Page/国民参政会.md "wikilink")、美国联合援华会会长。1944年，任[国民政府教育部常务次长](../Page/国民政府.md "wikilink")，1946年转任政务次长。

1947年6月2日，教育部次長杭立武抵武漢大學調查。\[1\]7月28日，[交通大學校長](../Page/交通大學.md "wikilink")[吳保豐辭職後](../Page/吳保豐.md "wikilink")，是日成立整理委員會，教育部次長杭立武擔任主任委員，決定限期一個月，將校務整理就緒。\[2\]9月3日，[聯合國教科文組織遠東區基本教育研究會在南京召開](../Page/聯合國教科文組織.md "wikilink")，中國代表杭立武當選為主席；會議主題是關於推進基本教育。\[3\]同年任[國立中央博物院籌備處主任](../Page/國立中央博物院.md "wikilink")，并在[安徽省](../Page/安徽省.md "wikilink")[滁縣當選為](../Page/滁縣.md "wikilink")[第一屆國民大會安徽省代表](../Page/第一屆國民大會安徽省代表.md "wikilink")。

1948年8月16日，教育部次長杭立武到[漢口](../Page/漢口.md "wikilink")，處理湖北師範學院及流亡學生問題\[4\]。

1949年，就任教育部长。4月25日，教育部長杭立武到達廣州\[5\]。5月10日，教育部長杭立武向美國註廣州領事館證實，互不信任繼續損害反共力量之團結，蔣在李宗仁證明有死戰到底之決心前，不敢完全信任李\[6\]。政府撤台后籌組並出任[國立中央博物圖書院館聯合管理處主任委員](../Page/國立中央博物圖書院館聯合管理處.md "wikilink")。1949年5月至1950年2月，任[國立編譯館館長](../Page/國立編譯館.md "wikilink")。

1953年，参与美国教会联合会筹办[东海大学](../Page/東海大學_\(台灣\).md "wikilink")，作为大陆十三所教会大学之延续。1956年起，历任中華民国驻[泰国](../Page/泰国.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[菲律宾及](../Page/菲律宾.md "wikilink")[希腊诸国大使](../Page/希腊.md "wikilink")，兼[联合国教科文组织会议首席代表](../Page/联合国教科文组织.md "wikilink")。

## 晚年經歷

1965年，发起成立“亚洲与世界社”（AWI）。其间改组“港澳之友会”为“中华港澳之友协会”，协助港澳同胞继续拥有自由权益；同时积极维护及争取[台湾原住民权益](../Page/台湾原住民.md "wikilink")。1972年，任[國立政治大學國際關係研究中心主任](../Page/國立政治大學國際關係研究中心.md "wikilink")。1979年，创立[中国人权协会](../Page/中国人权协会.md "wikilink")。

1991年2月26日，逝世於[台北市](../Page/台北市.md "wikilink")。

## 作品

  - 著作

<!-- end list -->

  - 杭立武 原著，《中華文物播遷記》，臺灣商務印書館 1980年初版。

## 參考資料

[Category:台灣政治學家](../Category/台灣政治學家.md "wikilink")
[Category:中華民國駐泰國大使](../Category/中華民國駐泰國大使.md "wikilink")
[Category:中華民國駐菲律賓大使](../Category/中華民國駐菲律賓大使.md "wikilink")
[Category:中華民國駐希臘大使](../Category/中華民國駐希臘大使.md "wikilink")
[Category:中華民國教育部部長](../Category/中華民國教育部部長.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:第一届国民参政会参政员](../Category/第一届国民参政会参政员.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")
[Category:第三届国民参政会参政员](../Category/第三届国民参政会参政员.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:東海大學校史](../Category/東海大學校史.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[法](../Category/國立中央大學教授.md "wikilink")
[Category:南京大學教席學者](../Category/南京大學教席學者.md "wikilink")
[Category:倫敦大學校友](../Category/倫敦大學校友.md "wikilink")
[Category:金陵大學校友](../Category/金陵大學校友.md "wikilink")
[Category:浙江裔台灣人](../Category/浙江裔台灣人.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")
[Category:滁州人](../Category/滁州人.md "wikilink")
[Li立](../Category/杭姓.md "wikilink")
[南京](../Category/國立故宮博物院院長.md "wikilink")
[Category:行政院文化獎得主](../Category/行政院文化獎得主.md "wikilink")
[Category:南京安全区国际委员会成员](../Category/南京安全区国际委员会成员.md "wikilink")

1.

2.
3.
4.
5.

6.