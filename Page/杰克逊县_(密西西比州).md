**傑克遜縣**（**Jackson County,
Mississippi**）是位於[美國](../Page/美國.md "wikilink")[密西西比州東南角的一個縣](../Page/密西西比州.md "wikilink")。南臨海，東鄰[亞拉巴馬州](../Page/亞拉巴馬州.md "wikilink")。面積1,331平方公里。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年統計，共有人口131,420人。縣治[帕斯卡古拉
(密西西比州)](../Page/帕斯卡古拉_\(密西西比州\).md "wikilink") (Pascagoula)。

成立於1812年12月14日，紀念後來成為第七任總統的[安德魯·傑克遜](../Page/安德魯·傑克遜.md "wikilink")。

[J](../Category/密西西比州行政區劃.md "wikilink")