**杰夫·施特拉塞尔**（[德语](../Page/德语.md "wikilink")：****，1974年10月5日，[卢森堡城](../Page/卢森堡城.md "wikilink")
-），[卢森堡足球运动员](../Page/卢森堡.md "wikilink")，司职后卫，目前效力于[瑞士足球超级联赛](../Page/瑞士足球超级联赛.md "wikilink")[苏黎世草蜢足球俱乐部和](../Page/苏黎世草蜢足球俱乐部.md "wikilink")[卢森堡国家足球队](../Page/卢森堡国家足球队.md "wikilink")。

[Category:1974年出生](../Category/1974年出生.md "wikilink")
[Category:卢森堡足球运动员](../Category/卢森堡足球运动员.md "wikilink")
[Category:梅斯球員](../Category/梅斯球員.md "wikilink")
[Category:凱沙羅頓球員](../Category/凱沙羅頓球員.md "wikilink")
[Category:慕遜加柏球員](../Category/慕遜加柏球員.md "wikilink")
[Category:斯特拉斯堡球員](../Category/斯特拉斯堡球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:凱沙羅頓主教練](../Category/凱沙羅頓主教練.md "wikilink")
[Category:德乙主教练](../Category/德乙主教练.md "wikilink")