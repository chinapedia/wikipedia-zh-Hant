**杰尔**（；、或；，**拉布**；；；或；）是[匈牙利西北部的一座城市](../Page/匈牙利.md "wikilink")，[杰尔-莫松-肖普朗州首府](../Page/杰尔-莫松-肖普朗州.md "wikilink")。它是匈牙利的第六大城市，也是该国五个地区中心之一，中欧其中一条主要道路途经当地。

## 历史

杰尔所在地自古已有人聚居，公元前5世纪[凯尔特人首次大规模定居当地](../Page/凯尔特人.md "wikilink")，他们称该城为“Arrabona”，其简称至今在一些语言中仍然通用，例如德语的“”和斯洛伐克语的“”。

一些罗马商人在公元前1世纪到该城定居，罗马帝国在公元10年左右占领西匈牙利的北部，直至公元4世纪撤出。此后，[斯拉夫人](../Page/斯拉夫人.md "wikilink")、[伦巴底人和](../Page/伦巴底人.md "wikilink")[阿瓦尔人分别于](../Page/阿瓦尔人.md "wikilink")500年、547年和568年起定居当地。880年至894年间是[大摩拉维亚公国的一部分](../Page/大摩拉维亚公国.md "wikilink")，约900年被[匈牙利人占领](../Page/匈牙利人.md "wikilink")，罗马人废弃的堡垒被加固。匈牙利第一位国王[伊什特万一世在当地成立主教辖区](../Page/伊什特万一世.md "wikilink")，该城改名杰尔，并开始受到匈牙利历史上的种种波折影响。1241－1242年间杰尔被蒙古人攻占，1271年被捷克军队摧毁。[莫哈奇战役结束后](../Page/莫哈奇战役.md "wikilink")，[纳达什迪·陶马什和](../Page/纳达什迪·陶马什.md "wikilink")[切斯奈基·捷尔吉为](../Page/切斯奈基·捷尔吉.md "wikilink")[斐迪南一世占领杰尔](../Page/斐迪南一世_\(神圣罗马帝国\).md "wikilink")。[奥斯曼帝国占据今天的匈牙利期间](../Page/奥斯曼帝国.md "wikilink")（1541年至17世纪末），指挥官兰贝格·克里什托夫认为已经不可能成功抵御土耳其军队的入侵，遂将杰尔城焚烧殆尽，土耳其人到达后只见一片废墟，因此把当地称为“”（土耳其语意为“烧掉的城市”）。

后来杰尔由当时意大利的顶尖建筑师重建，新城被城堡和城墙包围。匈牙利步兵司令官[切斯奈基·亚诺什在](../Page/切斯奈基·亚诺什.md "wikilink")1594年死后，土耳其军队占领该城，但在1598年被匈牙利和奥地利军队收服。随后几个世纪，杰尔发展蓬勃，1743年获奥地利女大公[玛丽亚·特蕾西娅提升为](../Page/玛丽亚·特蕾西娅.md "wikilink")[帝国自由城市](../Page/帝国自由城市.md "wikilink")。不少宗教组织如[耶稣会和](../Page/耶稣会.md "wikilink")[加尔默罗会开始在杰尔立足](../Page/加尔默罗会.md "wikilink")，他们曾兴建学校、教堂、医院和修道院。[拿破仑占领杰尔城堡并炸毁部分城墙后](../Page/拿破仑.md "wikilink")，该城的统治者意识到这些城墙已经失去作用，便拆毁其中大部分，以扩建城市。

1800年代中期[多瑙河上开始有汽船航行](../Page/多瑙河.md "wikilink")，杰尔随之成为贸易城市，但它的重要性在1861年[布达佩斯至](../Page/布达佩斯.md "wikilink")[瑙吉考尼饶的铁路开通后逐渐下降](../Page/瑙吉考尼饶.md "wikilink")。于是统治者决定实行工业化，使杰尔的经济保持繁荣至[第二次世界大战爆发](../Page/第二次世界大战.md "wikilink")。

[纳粹德国占领期间](../Page/纳粹德国.md "wikilink")，占杰尔人口12.6%的犹太人于1944年5月13日被迫迁往犹太人区，1个月后被运离该城，获豁免得以留下的人在1945年3月26日被屠杀。

## 友好城市

杰尔的友好城市如下：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/爱尔福特.md" title="wikilink">爱尔福特</a></p></li>
<li><p><a href="../Page/芬兰.md" title="wikilink">芬兰</a><a href="../Page/库奥皮奥.md" title="wikilink">库奥皮奥</a></p></li>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/辛德尔芬根.md" title="wikilink">辛德尔芬根</a></p></li>
<li><p><a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/科尔马.md" title="wikilink">科尔马</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/罗马尼亚.md" title="wikilink">罗马尼亚</a><a href="../Page/布拉索夫.md" title="wikilink">布拉索夫</a></p></li>
<li><p><a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/上拿撒勒.md" title="wikilink">上拿撒勒</a></p></li>
<li><p><a href="../Page/中国.md" title="wikilink">中国</a><a href="../Page/武汉.md" title="wikilink">武汉</a></p></li>
<li><p><a href="../Page/波兰.md" title="wikilink">波兰</a><a href="../Page/波兹南.md" title="wikilink">波兹南</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部链接

  - [官方网站](http://www.gyor.hu/)

  -
[Category:杰尔-莫雄-肖普朗州](../Category/杰尔-莫雄-肖普朗州.md "wikilink")
[Category:匈牙利各州首府](../Category/匈牙利各州首府.md "wikilink")