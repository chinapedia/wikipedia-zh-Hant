<table>
<thead>
<tr class="header">
<th><p>盾徽</p></th>
<th><p>地图</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>统计表</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/意大利的地区.md" title="wikilink">地区</a>：</p></td>
<td><p><a href="../Page/西西里岛.md" title="wikilink">西西里岛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/意大利的省份.md" title="wikilink">省份</a>：</p></td>
<td><p><a href="../Page/卡尔塔尼塞塔省.md" title="wikilink">卡尔塔尼塞塔</a>（<a href="../Page/Italian_license_plates.md" title="wikilink"><code>CL</code></a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/位置.md" title="wikilink">位置</a>：</p></td>
<td><p>.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/面积.md" title="wikilink">面积</a>：</p></td>
<td><p>276 平方公里</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>：</p></td>
<td><p>72,444</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口密度.md" title="wikilink">人口密度</a>：</p></td>
<td><p>262／平方公里</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Fractions.md" title="wikilink">Fractions</a>：</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海拔.md" title="wikilink">海拔</a>：</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/意大利邮政编码列表.md" title="wikilink">邮编</a>：</p></td>
<td><p>93012</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/意大利区号.md" title="wikilink">区号</a>：</p></td>
<td><p><a href="../Page/Italian_area_code_0933.md" title="wikilink">0933</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Istituto_Nazionale_di_Statistica.md" title="wikilink">ISTAT</a> 代码：</p></td>
<td><p>085007</p></td>
</tr>
<tr class="odd">
<td><p>财政代码：</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>居民姓名：</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>网址：</p></td>
<td><p><a href="http://www.comune.gela.cl.it">www.comune.gela.cl.it</a></p></td>
</tr>
<tr class="even">
<td><p>政治</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/市长.md" title="wikilink">市长</a>：</p></td>
<td></td>
</tr>
</tbody>
</table>

**杰拉**（**Gela**）是[卡尔塔尼塞塔省的一个镇](../Page/卡尔塔尼塞塔.md "wikilink")，位于[意大利的](../Page/意大利.md "wikilink")[西西里岛](../Page/西西里岛.md "wikilink")。

杰拉是[卡尔塔尼塞塔省的一个城市](../Page/卡尔塔尼塞塔.md "wikilink")，在[意大利的](../Page/意大利.md "wikilink")[西西里岛南部](../Page/西西里岛.md "wikilink")。它位于地中海上，距[卡尔塔尼塞塔大约有](../Page/卡尔塔尼塞塔.md "wikilink")84公里。2001年约有72444名居民，多於該省首府，而面積排名為該省第二。

杰拉是一座重要的工业城市和港市。在这里有一个重要的工业就是汽油精炼厂。从其他城市（像是卡尔塔尼塞塔省的锡拉库扎）可以乘坐火车到达这里。
[It-map.png](https://zh.wikipedia.org/wiki/File:It-map.png "fig:It-map.png")

## 历史概况

公元前688年左右，在锡拉库扎建立45年后，从罗得斯和克里特岛来的殖民者建立了这座城市。城市以[杰拉河命名](../Page/杰拉河.md "wikilink")。希腊人在如今意大利的南部有很多殖民地，并且控制这里长达几个世纪之久。\[1\]公元前456年[埃斯庫羅斯死于这座城市](../Page/埃斯庫羅斯.md "wikilink")。

公元前405年，[迦太基侵略了这座城市](../Page/迦太基.md "wikilink")。公元282年这座城市开始没

## 周边城镇

  - [Acate](../Page/Acate.md "wikilink") (RG)
  - [Butera](../Page/Butera.md "wikilink")
  - [Caltagirone](../Page/Caltagirone.md "wikilink") (CT)
  - [Mazzarino](../Page/Mazzarino.md "wikilink")
  - [Niscemi](../Page/Niscemi.md "wikilink")

## 引用

## 人口历史

  - 1996年：72,535
  - 2001年：72,444

## 友好城市

  - [埃莱夫西纳](../Page/埃莱夫西纳.md "wikilink")

  - [维廷根](../Page/维廷根.md "wikilink")

## 外部链接

  - [Piccolo, Salvatore. *Gela.* Ancient History Encyclopedia. December
    20, 2017.](https://www.ancient.eu/Gela/)

  - [Mapquest -
    Gela](http://www.mapquest.com/maps/map.adp?tb=1&city=Gela&country=IT)

  - [- Very extensive information on archeology at
    Gela](http://www.regione.sicilia.it/beniculturali/dirbenicult/attika/index.html)

[G](../Category/卡尔塔尼塞塔省市镇.md "wikilink")

1.  E. Zuppardo, Salvatore Piccolo (2005), *Terra Mater: Sulle sponde
    del Gela greco*, Betania ed., Caltanissetta.