**杰梅恩·彭南特**（，），出生於[英格兰](../Page/英格兰.md "wikilink")[諾定咸](../Page/諾定咸.md "wikilink")，司職[翼鋒](../Page/中場.md "wikilink")。现效力于[新加坡职业足球联赛球队](../Page/新加坡职业足球联赛.md "wikilink")[淡濱尼流浪](../Page/淡濱尼流浪足球會.md "wikilink")。他在2009年夏天與[利物浦解約](../Page/利物浦足球俱樂部.md "wikilink")，並加盟[西甲剛季升級球會](../Page/西甲.md "wikilink")[薩拉戈薩](../Page/皇家薩拉戈薩.md "wikilink")，於短暫借用到[史篤城後](../Page/斯托克城足球俱乐部.md "wikilink")，由於表現出色，在2011年[轉會窗重開後正式加盟](../Page/轉會窗.md "wikilink")。

## 生平

### 球會

賓納是[諾士郡的青訓球員](../Page/諾士郡.md "wikilink")，是一名出波型右翼。1999年他以16岁之齢轉会至[英超球會](../Page/英超.md "wikilink")[阿仙奴](../Page/阿仙奴.md "wikilink")（[轉會費為](../Page/轉會費.md "wikilink")250萬[英鎊](../Page/英鎊.md "wikilink")），首次聯賽上陣對[修咸頓便](../Page/修咸頓.md "wikilink")[連中三元](../Page/連中三元.md "wikilink")，但六年間先後被外借到[屈福特](../Page/屈福特.md "wikilink")、[列斯聯](../Page/列斯聯.md "wikilink")、[伯明翰各兩次](../Page/伯明翰.md "wikilink")。2005年正式被[伯明翰收購](../Page/伯明翰.md "wikilink")。一季後被老牌球會[利物浦以](../Page/利物浦足球俱乐部.md "wikilink")670萬[英鎊](../Page/英鎊.md "wikilink")（但根據[伯明翰與](../Page/伯明翰.md "wikilink")[阿仙奴的協議](../Page/阿仙奴.md "wikilink")，[伯明翰要將這筆共以](../Page/伯明翰.md "wikilink")670萬[英鎊的轉會費中的](../Page/英鎊.md "wikilink")25%，即167.5万[英鎊給予](../Page/英鎊.md "wikilink")[阿仙奴](../Page/阿仙奴.md "wikilink")）將賓納特帶到[晏菲路球場效力](../Page/晏菲路球場.md "wikilink")。加盟僅一季已成為利物浦的重要球員，並於對[車路士的聯賽中入了一球精彩遠射](../Page/車路士.md "wikilink")。

2008/09年球季賓納上陣的次數大減，於2009年1月20日被外借到[樸茨茅夫直到球季結束](../Page/朴茨茅斯足球俱乐部.md "wikilink")<small>\[1\]</small>。季後遭利物浦棄用，於2009年7月9日離開[英格蘭加盟](../Page/英格蘭.md "wikilink")[西班牙球會](../Page/西甲.md "wikilink")[薩拉戈薩](../Page/皇家薩拉戈薩.md "wikilink")<small>\[2\]</small>，球隊整季在聯賽榜下游掙扎，他在操練時需要翻譯才明白教練的指示，更因兩週三次遲到而被送回家\[3\]。

2010年夏季[轉會窗關閉前](../Page/轉會窗.md "wikilink")，賓納獲借用返回[英格蘭的](../Page/英格蘭.md "wikilink")[史篤城直到翌年](../Page/斯托克城足球俱乐部.md "wikilink")1月\[4\]，期間表現出色，賓納表示希望繼續留效\[5\]，但領隊[東尼·佩利斯認為薩拉戈薩的](../Page/東尼·佩利斯.md "wikilink")600萬[英鎊開價過高](../Page/英鎊.md "wikilink")\[6\]，12月29日最終雙方達成協議，以170萬英鎊成交，金額可增加至280萬英鎊，賓納簽署兩年半合約正式加盟\[7\]。

2012年10月12日，[英冠球會](../Page/英冠.md "wikilink")[狼隊向](../Page/伍尔弗汉普顿流浪足球俱乐部.md "wikilink")[英超](../Page/英超.md "wikilink")[史篤城借用賓納](../Page/斯托克城足球俱乐部.md "wikilink")，為期三個月\[8\]。

2013年6月6日，[史篤城宣佈有](../Page/斯托克城足球俱乐部.md "wikilink")7名球員不獲新合約可自由轉會，賓納為其中之一\[9\]\[10\]。

2013年6月18日，賓納被放棄後12天，新任[史篤城領隊](../Page/斯托克城足球俱乐部.md "wikilink")[馬克·曉士重簽賓納](../Page/馬克·曉士.md "wikilink")，合約為期一年\[11\]。

### 國家隊

雖然賓納很早就展開其足球生涯，不過就从未入選過[英格兰国家足球隊](../Page/英格兰国家足球隊.md "wikilink")，只曾在[英格蘭青年軍效力](../Page/英格蘭21歲以下足球代表隊.md "wikilink")。當發現其[祖父是](../Page/祖父.md "wikilink")[愛爾蘭人後](../Page/愛爾蘭.md "wikilink")，賓納於2011年3月主動接觸[愛爾蘭足球協會關於他入選](../Page/愛爾蘭足球協會.md "wikilink")[愛爾蘭國家隊的可能性](../Page/愛爾蘭國家足球隊.md "wikilink")\[12\]。

## 醉駕入獄

2005年1月23日賓納將他的[-{zh-hans:奔驰;zh-hk:平治;zh-tw:朋馳;}-座駕在](../Page/梅塞德斯-朋馳.md "wikilink")[艾爾斯伯里撞上燈柱後被拘捕及控告醉酒駕駛及沒有](../Page/艾爾斯伯里.md "wikilink")[保險](../Page/保險.md "wikilink")，而當時他仍在罰停駕駛的16個月期間。於3月1日被判罪名成立及入獄90日<small>\[13\]</small>。入獄30日後獲准[假釋](../Page/假釋.md "wikilink")，賓納隨即返回[伯明翰城繼續披甲](../Page/伯明翰城足球俱乐部.md "wikilink")，但在假釋期間就算上場比賽仍需帶上電子監察器。

## 榮譽

  - 阿仙奴

<!-- end list -->

  - 2001年[英格蘭足總青年盃冠軍](../Page/英格蘭足總青年盃.md "wikilink")
  - 2004年[英格蘭社區盾冠軍](../Page/英格蘭社區盾.md "wikilink")

<!-- end list -->

  - 利物浦

<!-- end list -->

  - 2006年[英格蘭社區盾冠軍](../Page/英格蘭社區盾.md "wikilink")
  - 2007年[歐洲聯賽冠軍盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink")

## 參考資料

## 外部連結

  - [Thisisanfield.com player
    profile](http://www.thisisanfield.com/players/pennant)

  - [Official Liverpool
    profile](https://web.archive.org/web/20060815050452/http://www.liverpoolfc.tv/team/squad/pennant/)

  -
  - [LFChistory.net player
    profile](http://www.lfchistory.net/player_profile.asp?player_id=1159)

[Category:牙買加裔英格蘭人](../Category/牙買加裔英格蘭人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:諾士郡球員](../Category/諾士郡球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:屈福特球員](../Category/屈福特球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:伯明翰城球員](../Category/伯明翰城球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:樸茨茅夫球員](../Category/樸茨茅夫球員.md "wikilink")
[Category:薩拉戈薩球員](../Category/薩拉戈薩球員.md "wikilink")
[Category:史篤城球員](../Category/史篤城球員.md "wikilink")
[Category:狼隊球員](../Category/狼隊球員.md "wikilink")
[Category:浦那城球員](../Category/浦那城球員.md "wikilink")
[Category:韋根球員](../Category/韋根球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:西班牙外籍足球运动员](../Category/西班牙外籍足球运动员.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.