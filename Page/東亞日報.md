《**東亞日報**》（）是[大韓民國的一家](../Page/大韓民國.md "wikilink")[報紙](../Page/報紙.md "wikilink")。由韓國的媒體人[金性洙](../Page/金性洙.md "wikilink")、[宋鎭禹創刊于](../Page/宋鎭禹.md "wikilink")1920年4月1日。總部位于[首爾特別市](../Page/首爾特別市.md "wikilink")[鍾路區](../Page/鍾路區.md "wikilink")。日發行量2,068,647份（韓國[發行公信會統計](../Page/發行公信會.md "wikilink")，2003年10月－12月）。

與《[朝鮮日報](../Page/朝鮮日報.md "wikilink")》、《[中央日報](../Page/中央日报_\(韩国\).md "wikilink")》、《[文化日報](../Page/文化日報.md "wikilink")》并為韓國四大報紙。東亞日報現與《中央日報》競爭發行量第2位。

2018年1月26日，《東亞日報》發行第30000號\[1\]。

## 姊妹報紙

  - 《東亞科學紙》（dongascience）
  - 《新東亞》
  - 《東亞電磁紙》（dongaeconomy）

[Donga_kim.JPG](https://zh.wikipedia.org/wiki/File:Donga_kim.JPG "fig:Donga_kim.JPG")

## 参考文献

## 外部連結

  - [《東亞日報》](http://www.donga.com/)
  - [《東亞日報》](http://chinese.donga.com/)

## 参见

  - [朝鮮日報](../Page/朝鮮日報.md "wikilink")
  - [中央日報](../Page/中央日报_\(韩国\).md "wikilink")
  - [文化日報](../Page/文化日報.md "wikilink")
  - [金性洙](../Page/金性洙.md "wikilink")
  - [宋鎭禹](../Page/宋鎭禹.md "wikilink")
  - [朱成夏](../Page/朱成夏.md "wikilink")

[Category:韓國公司](../Category/韓國公司.md "wikilink")
[Category:韓國報紙](../Category/韓國報紙.md "wikilink")
[Category:1920年建立](../Category/1920年建立.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.  [「《東亞日報》報齡3萬號紀念儀式」在首爾一民美術館舉行](http://chinese.donga.com/Home/3/all/28/1205904/1)