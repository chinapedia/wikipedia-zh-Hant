[HK_WC_Bank_of_East_Asia_Harbour_View_Centre_1.JPG](https://zh.wikipedia.org/wiki/File:HK_WC_Bank_of_East_Asia_Harbour_View_Centre_1.JPG "fig:HK_WC_Bank_of_East_Asia_Harbour_View_Centre_1.JPG")
[HK_WC_Bank_of_East_Asia_Harbour_View_Centre_2.JPG](https://zh.wikipedia.org/wiki/File:HK_WC_Bank_of_East_Asia_Harbour_View_Centre_2.JPG "fig:HK_WC_Bank_of_East_Asia_Harbour_View_Centre_2.JPG")
**東亞銀行港灣中心**（**Bank of East Asia Harbour View
Centre**），前稱**第一太平銀行中心**，是[香港](../Page/香港.md "wikilink")[灣仔北的一座](../Page/灣仔北.md "wikilink")[商業](../Page/商業.md "wikilink")[大廈](../Page/大廈.md "wikilink")。大廈樓高29樓，基座4層是[停車場](../Page/停車場.md "wikilink")，5樓以上為[辦公室](../Page/辦公室.md "wikilink")，每層約7.8千平方呎。大廈於1991年落成，發展商是[華人置業有限公司](../Page/華人置業.md "wikilink")。

## 位置

東亞銀行港灣中心位於[灣仔](../Page/灣仔.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")49至57號，其南邊是灣仔[謝斐道](../Page/謝斐道.md "wikilink")，東鄰[浙江第一銀行大廈及](../Page/浙江第一銀行大廈.md "wikilink")[盧押道](../Page/盧押道.md "wikilink")，西鄰[分域街及](../Page/分域街.md "wikilink")[馬來西亞大廈](../Page/馬來西亞大廈.md "wikilink")，北望[香港演藝學院](../Page/香港演藝學院.md "wikilink")、灣仔[電訊大廈及](../Page/電訊大廈.md "wikilink")[稅務大樓等](../Page/稅務大樓.md "wikilink")。

## 使用狀況

東亞銀行港灣中心25樓為[瑞典駐港總領事館](../Page/瑞典.md "wikilink")。

曾兩次參與[香港特別行政區行政長官競選的](../Page/香港特別行政區行政長官.md "wikilink")[曾蔭權](../Page/曾蔭權.md "wikilink")，兩次參選均分別在25樓（[2005年](../Page/2005年香港特別行政區行政長官選舉.md "wikilink")）及28樓（[2007年](../Page/2007年香港特別行政區行政長官選舉.md "wikilink")）設立競選辦公室。這與他的競選辦主任為[東亞銀行主席](../Page/東亞銀行.md "wikilink")[李國寶有關](../Page/李國寶.md "wikilink")。

據土地註冊處資料顯示，思源基金會於2013年12月中，斥資約1.3569億元，購入26樓全層，樓面近8000方呎。
陳曾燾為董事之一的Segen's Praise
Limited，2012年也曾斥資1.09億元購入該座商廈的24樓全層，當時呎價約1.38萬元

## 參見

  - [東亞銀行](../Page/東亞銀行.md "wikilink")
  - [李國寶](../Page/李國寶.md "wikilink")
  - [2005年香港特別行政區行政長官選舉](../Page/2005年香港特別行政區行政長官選舉.md "wikilink")
  - [2007年香港特別行政區行政長官選舉](../Page/2007年香港特別行政區行政長官選舉.md "wikilink")
  - [玻璃幕牆](../Page/玻璃幕牆.md "wikilink")
  - [大廈命名權](../Page/大廈命名權.md "wikilink")

## 外部連結

  - [東亞銀行港灣中心的建築規格](http://www.goagroup.com/ver3/zh/property/propertyinfo_wanchai_eastasia.php)
  - [港島灣仔東亞銀行港灣中心地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=835692&cy=815554&zm=2&mx=835692&my=815554&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)
  - [香港傳媒預料曾競選辦公室將會再租
    港島灣仔東亞港灣中心](http://hk.news.yahoo.com/070110/12/1zpkt.html)

[category:東亞銀行](../Page/category:東亞銀行.md "wikilink")

[Category:灣仔區寫字樓](../Category/灣仔區寫字樓.md "wikilink")
[Category:灣仔北](../Category/灣仔北.md "wikilink")
[Category:華人置業物業](../Category/華人置業物業.md "wikilink")