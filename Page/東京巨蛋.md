**東京巨蛋**（，）位於[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[文京區](../Page/文京區.md "wikilink")，是一座有約46,000個座位的多功能[體育館](../Page/體育館.md "wikilink")（演唱會時可容納逾57,000人）。

[1988年](../Page/1988年.md "wikilink")（昭和63年）3月18日開場\[1\]，是日本第一座[巨蛋型球場](../Page/巨蛋.md "wikilink")\[2\]，亦是[株式會社東京巨蛋所營運的](../Page/東京巨蛋_\(企業\).md "wikilink")[東京巨蛋城核心施設](../Page/東京巨蛋城.md "wikilink")。愛稱為「**BIG
EGG**」\[3\]。

是[日本職棒](../Page/日本職棒.md "wikilink")[讀賣巨人的主場](../Page/讀賣巨人.md "wikilink")，曾經舉辦過[籃球與](../Page/籃球.md "wikilink")[美式足球比賽](../Page/美式足球.md "wikilink")，還有[職業摔角](../Page/職業摔角.md "wikilink")、[綜合武術](../Page/綜合武術.md "wikilink")、[K-1賽事或音樂表演](../Page/K-1.md "wikilink")。其蛋型屋頂為具彈性的薄膜，一般會把巨蛋內的氣壓控制在比巨蛋外高0.3%以維持蛋頂外型。

東京巨蛋的原址為1949年啟用、1972年關閉的[後樂園自行車場](../Page/後樂園自行車場.md "wikilink")。1988年3月17日落成啟用，以取代鄰近的[後樂園球場](../Page/後樂園球場.md "wikilink")。
[Tokyo_Dome_2007-2.jpg](https://zh.wikipedia.org/wiki/File:Tokyo_Dome_2007-2.jpg "fig:Tokyo_Dome_2007-2.jpg")
[Tokyo_Dome_night.jpg](https://zh.wikipedia.org/wiki/File:Tokyo_Dome_night.jpg "fig:Tokyo_Dome_night.jpg")
東京巨蛋亦為日本「五大巨蛋」之一，其他四者為[福岡巨蛋](../Page/福岡巨蛋.md "wikilink")、[札幌巨蛋](../Page/札幌巨蛋.md "wikilink")、[名古屋巨蛋](../Page/名古屋巨蛋.md "wikilink")、[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")。

## 主題公園

東京巨蛋除了是比賽場之外，同時也有一座[主題樂園](../Page/主題樂園.md "wikilink")，名為[東京巨蛋城](../Page/東京巨蛋城.md "wikilink")（Tokyo
Dome City）（前身為後樂園遊樂場），詳細內容如下：

  - 東京巨蛋
      - [野球體育博物館](../Page/野球體育博物館.md "wikilink")
  - 東京巨蛋樂園（後樂園遊樂園）
      - LaQua溫泉
  - [MEETS PORT](../Page/MEETS_PORT.md "wikilink")
      - JCB HALL（又被稱做**後樂園表演廳2**）
  - 東京圓頂飯店
  - 黃建築
      - 後樂園表演廳
  - 紅建築
      - WINS後樂園
      - offt後樂園
  - JUMP SHOP（[週刊少年Jump的商店](../Page/週刊少年Jump.md "wikilink")）
  - 福祿壽／小石川七福神

## 重要節目

### 體育活动

目前為[日本職棒](../Page/日本職棒.md "wikilink")[讀賣巨人主場](../Page/讀賣巨人.md "wikilink")，此外，在2006年後，由於[世界棒球經典賽開辦](../Page/世界棒球經典賽.md "wikilink")，加上球季初或球季末時會邀請國外球隊進行交流比賽，因此也成為[日本武士隊常態性及主場](../Page/日本棒球代表隊.md "wikilink")。

[芝加哥小熊隊與](../Page/芝加哥小熊.md "wikilink")[紐約大都會隊曾在這裡打了兩場](../Page/紐約大都會.md "wikilink")2000年開幕戰，這也是首次有[美國職棒大聯盟球隊在亞洲地區打正規球季賽](../Page/美國職棒大聯盟.md "wikilink")。[紐約洋基隊與](../Page/紐約洋基.md "wikilink")[坦帕灣魔鬼魚隊在](../Page/坦帕灣魔鬼魚.md "wikilink")2004年三月在此打了兩場開幕戰。[西雅图水手队与](../Page/西雅图水手.md "wikilink")[奥克兰运动家队在](../Page/奥克兰运动家.md "wikilink")2012年3月在此打了两场揭幕战。

在[2005年8月](../Page/2005年8月.md "wikilink")，[亞特蘭大獵鷹隊在東京巨蛋以](../Page/亞特蘭大獵鷹.md "wikilink")27-20擊敗了[印第安那小馬隊](../Page/印第安那小馬.md "wikilink")，這是[美國職業美式足球聯盟在該年度於體育館內進行的第一場](../Page/美國職業美式足球聯盟.md "wikilink")[會前賽](../Page/會前賽.md "wikilink")。

東京巨蛋也舉辦過幾場拳擊冠軍戰，包括在1990年的2月10日的[重量級冠軍戰](../Page/重量級.md "wikilink")，也就是[麥克·泰森被戰績](../Page/麥克·泰森.md "wikilink")42勝1敗的[詹姆士·道格拉斯](../Page/詹姆士·道格拉斯.md "wikilink")（）在第十回合擊倒奪去去冠軍頭銜的比賽。

### 演唱會

東京巨蛋曾經舉行過許多場著名的[演唱會](../Page/演唱會.md "wikilink")，[美空雲雀是首位以個人名義在東京巨蛋舉行演唱會的日本歌手](../Page/美空雲雀.md "wikilink")，其他歌手包括[X
Japan](../Page/X_Japan.md "wikilink")、[EXILE](../Page/EXILE.md "wikilink")、[嵐](../Page/嵐.md "wikilink")、[AKB48](../Page/AKB48.md "wikilink")、[東方神起](../Page/東方神起.md "wikilink")、[Super
Junior](../Page/Super_Junior.md "wikilink")、[BIGBANG](../Page/BIGBANG.md "wikilink")、[EXO](../Page/EXO.md "wikilink")、[瑪麗亞·凱莉](../Page/瑪麗亞·凱莉.md "wikilink")、[泰勒絲](../Page/泰勒絲.md "wikilink")、[桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")、[μ's等](../Page/μ's.md "wikilink")，都打破了當時的東京巨蛋最高觀眾人數記錄，[EXO更是成為出道最短時間內就登上東京巨蛋的海外男團](../Page/EXO.md "wikilink")。2006年9月20日和21日，[美國女歌手](../Page/美國.md "wikilink")[麦当娜在東京巨蛋舉行其個人世界巡迴演唱會的最後一站](../Page/瑪丹娜.md "wikilink")，兩場全場爆滿。2007年7月22日，[KinKi
Kids在此舉行出道十周年紀念公演](../Page/KinKi_Kids.md "wikilink")，容納約67,000名觀眾，創下東京巨蛋「單場最多觀眾人數」的新紀錄。目前，KinKi
Kids也是東京巨蛋「累計開唱最多場次」以及「連續公演年數最長」的紀錄保持人。而著名女子組合[桃色幸運草Z打破了成員最少的男女子組合團隊的人數可以在東京巨蛋進行演唱會](../Page/桃色幸運草Z.md "wikilink")，並在演唱會中最年輕的一班成員(平均24歲)可以在東京巨蛋進行演唱會，更成功在五大巨蛋都開過演唱會。兩場演唱會合共動員9萬人次入場！

東京巨蛋史上最快門票完售紀錄，是在1997年12月31日 X JAPAN 解散演唱會 THE LAST LIVE
，55000張於2分鐘內完售，至今無人打破。

2007年12月22日[Hey\! Say\!
JUMP以團體平均年齡](../Page/Hey!_Say!_JUMP.md "wikilink")15.2歲成為東京巨蛋的最年輕表演者。2009年5月[KAT-TUN連續八天](../Page/KAT-TUN.md "wikilink")（2009年5月15日至22日）東京巨蛋舉行演唱會，創下紀錄，並在6月追加兩埸公演（2009年6月14日和15日)，成為史上第一組在一個月內在東京巨蛋舉行共十埸演唱會的藝人。

2011年12月聲優[水樹奈奈首次在此舉行演唱會](../Page/水樹奈奈.md "wikilink")，成為史上第一個[聲優在此公演](../Page/聲優.md "wikilink")，並且創下「東京巨蛋史上的第一場全天域星象投影表演」的紀錄。2016年4月9、10日，她第二次在此舉行演唱會，成為東京巨蛋開唱次數最多的日本個人女歌手之一。

2016年3月31日至4月1日動畫團體[μ's在此舉行最終演唱會](../Page/μ's.md "wikilink")「µ's Final
LoveLive\! ～µ'sic Forever♪♪♪♪♪♪♪♪♪～」，成為首位登上東京巨蛋的動畫組合。

2018年5月22日至23日，[桃色幸運草Z在此舉行出道](../Page/桃色幸運草Z.md "wikilink")10周年演唱會，並在兩日內打破82000人動員次數。更打破了成員最少的男女子組合團隊的人數(四個)可以在東京巨蛋開演唱會，並在演唱會中最年輕的一班成員(平均24歲)可以在東京巨蛋開演唱會

## 軼聞

  - [臺灣的圓頂球場或大型屋內競技場之所以稱為](../Page/臺灣.md "wikilink")**巨蛋**，是源自於直譯了東京巨蛋的愛稱BIG
    EGG\[4\]。

## 鄰近車站

  - [水道橋站](../Page/水道橋站.md "wikilink")
      - [JR_logo_(east).svg](https://zh.wikipedia.org/wiki/File:JR_logo_\(east\).svg "fig:JR_logo_(east).svg")
        [中央、總武緩行線](../Page/中央、總武緩行線.md "wikilink")
      - [Subway_TokyoMita.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoMita.png "fig:Subway_TokyoMita.png")
        [都營地下鐵](../Page/都營地下鐵.md "wikilink")[三田線](../Page/都營地下鐵三田線.md "wikilink")－車站編號
        **I-11**
  - [春日站](../Page/春日站_\(東京都\).md "wikilink")
      - [Subway_TokyoOedo.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoOedo.png "fig:Subway_TokyoOedo.png")
        [都營地下鐵](../Page/都營地下鐵.md "wikilink")[大江戶線](../Page/都營地下鐵大江戶線.md "wikilink")－車站編號
        **E-07**
      - [Subway_TokyoMita.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoMita.png "fig:Subway_TokyoMita.png")
        [都營地下鐵](../Page/都營地下鐵.md "wikilink")[三田線](../Page/都營地下鐵三田線.md "wikilink")－車站編號
        **I-12**
  - [後樂園站](../Page/後樂園站.md "wikilink")
      - [Subway_TokyoMarunouchi.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoMarunouchi.png "fig:Subway_TokyoMarunouchi.png")
        [東京地下鐵](../Page/東京地下鐵.md "wikilink")[丸之内線](../Page/東京地下鐵丸之内線.md "wikilink")－車站編號
        **M-22**
      - [Subway_TokyoNamboku.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoNamboku.png "fig:Subway_TokyoNamboku.png")
        [東京地下鐵](../Page/東京地下鐵.md "wikilink")[南北線](../Page/東京地下鐵南北線.md "wikilink")－車站編號
        **N-11**

## 腳註

## 外部連結

  - [東京巨蛋](http://www.tokyo-dome.co.jp)

[分類:文京區](../Page/分類:文京區.md "wikilink")

[Category:日本棒球場](../Category/日本棒球場.md "wikilink")
[Category:東京體育場地](../Category/東京體育場地.md "wikilink")
[Category:東京都觀光地](../Category/東京都觀光地.md "wikilink")
[Category:巨蛋](../Category/巨蛋.md "wikilink")
[Category:1988年完工體育場館](../Category/1988年完工體育場館.md "wikilink")
[Category:世界棒球經典賽球場](../Category/世界棒球經典賽球場.md "wikilink")
[Category:日本職業摔角場館](../Category/日本職業摔角場館.md "wikilink")
[Category:日本拳擊場館](../Category/日本拳擊場館.md "wikilink")
[Category:日本地標](../Category/日本地標.md "wikilink")
[Category:BCS獎](../Category/BCS獎.md "wikilink")

1.  “東京ドームが盛大に完工式”. [北海道新聞](../Page/北海道新聞.md "wikilink") (北海道新聞社).
    (1988年3月18日)

2.  “広い！高い！東京ドーム 上棟式で初公開”. [北海道新聞](../Page/北海道新聞.md "wikilink")
    (北海道新聞社). (1987年7月22日)

3.
4.  [多功能巨蛋體育館之營運管理實務(2014)](http://www.ycrc.com.tw/yangchih/A8708.html)
    314頁