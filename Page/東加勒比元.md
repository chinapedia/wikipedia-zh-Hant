**東加勒比元**([符號](../Page/貨幣符號.md "wikilink"): **$**;
[代碼](../Page/ISO_4217.md "wikilink"):
**XCD**)，為[北美洲](../Page/北美洲.md "wikilink")[加勒比海地區的數個前](../Page/加勒比海.md "wikilink")[英國殖民地國家與英國殖民地所通用的貨幣](../Page/英國.md "wikilink")，使用國家地區包括[安圭拉](../Page/安圭拉.md "wikilink")、[安地卡及巴布達](../Page/安地卡及巴布達.md "wikilink")、[多米尼克](../Page/多米尼克.md "wikilink")、[格瑞那達](../Page/格瑞那達.md "wikilink")、[蒙特塞拉特](../Page/蒙特塞拉特.md "wikilink")、[聖克里斯多福及尼維斯](../Page/聖克里斯多福及尼維斯.md "wikilink")、[聖露西亞和](../Page/聖露西亞.md "wikilink")[聖文森及格瑞那丁](../Page/聖文森及格瑞那丁.md "wikilink")。紙幣面額有5、10、20、50、100元。硬幣面值有1、2、5、10、25分和1元。1元=100分。貨幣符號為XCD。該貨幣實行固定匯率制，1[美元](../Page/美元.md "wikilink")=2.688東加勒比元。

## 流通中的貨幣

### 硬幣

  - 1 分
      - 正面：[伊麗莎白二世](../Page/伊麗莎白二世.md "wikilink")
      - 背面：面值、[稻草](../Page/稻草.md "wikilink")、國家名稱、年份
  - 2 分
      - 正面：伊麗莎白二世
      - 背面：面值、稻草、國家名稱、年份
  - 5 分
      - 正面：伊麗莎白二世
      - 背面：面值、稻草、國家名稱、年份
  - 10 分
      - 正面：伊麗莎白二世
      - 背面：面值、[帆船](../Page/帆船.md "wikilink")、國家名稱、年份
  - 25 分
      - 正面：伊麗莎白二世
      - 背面：面值、帆船、國家名稱、年份
  - 1 元
      - 正面：伊麗莎白二世
      - 背面：面值、帆船、國家名稱、年份
  - 2 元
      - 正面：伊麗莎白二世
      - 背面：面值、[樹木](../Page/樹木.md "wikilink")、國家名稱、年份

### 紙幣

[缩略图](https://zh.wikipedia.org/wiki/File:East_Caribbean_dollar_banknote_set.jpg "fig:缩略图")

  - 10 元
      - 正面：[伊丽莎白二世](../Page/伊丽莎白二世.md "wikilink")
      - 背面：[貝基亞島的海灣](../Page/貝基亞島.md "wikilink")、[東加勒比國家組織的地圖](../Page/東加勒比國家組織.md "wikilink")、[褐鵜鶘](../Page/褐鵜鶘.md "wikilink")、熱帶魚
  - 20 元{{\#ifexist:}}
      - 正面：伊丽莎白二世
      - 背面：[蒙特塞拉特總督府](../Page/蒙特塞拉特.md "wikilink")、東加勒比國家組織的地圖、[肉豆蔻](../Page/肉豆蔻.md "wikilink")
  - 50 元
      - 正面：伊丽莎白二世
      - 背面：[硫磺山要塞國家公園](../Page/硫磺山要塞國家公園.md "wikilink")、東加勒比國家組織的地圖、一座[火山的遠景](../Page/火山.md "wikilink")、熱帶魚
  - 100 元
      - 正面：伊丽莎白二世
      - 背面：[威廉·阿瑟·刘易斯](../Page/威廉·阿瑟·刘易斯.md "wikilink")、東加勒比國家組織的地圖、東加勒比中央銀行、熱帶魚

## 匯率

[東加勒比元](../Category/各國貨幣.md "wikilink")
[Category:中美洲](../Category/中美洲.md "wikilink")