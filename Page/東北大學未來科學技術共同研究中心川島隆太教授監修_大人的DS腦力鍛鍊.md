是由[任天堂與從事腦力鍛鍊研究多年的](../Page/任天堂.md "wikilink")[東北大學未來科學技術共同研究中心教授](../Page/东北大学_\(日本\).md "wikilink")[川島隆太合作製作發行的](../Page/川島隆太.md "wikilink")[益智遊戲軟體](../Page/益智遊戲.md "wikilink")。

本作剛開始發售的時候並未被看好；然而這部由五個人僅用了三個多月時間就完成了開發工作的作品，在上市之後獲得熱烈回響。在日本國內甚至形成了口耳相傳的社會效應，許多過去從來沒有接觸過遊戲的人為了體驗一下這款傳說中的遊戲購入了[任天堂DS主機](../Page/任天堂DS.md "wikilink")，從而令軟硬體皆出現了異常的熱銷景象。也有人认为其冗长的作品标题名也是受到人们关注的焦点之一。\[1\]

截至2006年10月底為止，本作及其續作《[東北大學未來科學技術共同研究中心川島隆太教授監修
更多的大人的DS腦力鍛鍊](../Page/東北大學未來科學技術共同研究中心川島隆太教授監修_更多的大人的DS腦力鍛鍊.md "wikilink")》在日本國內銷售量均達到300萬套之譜。

## 参考资料

## 外部連結

  - [官方網站](http://www.nintendo.co.jp/ds/andj/)
  - [巴哈姆特](http://newodin.gamer.com.tw/7/8597.html)

[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:益智游戏](../Category/益智游戏.md "wikilink")
[Category:Touch\!
Generations](../Category/Touch!_Generations.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:Wii U遊戲](../Category/Wii_U遊戲.md "wikilink") [Category:Wii U
Virtual Console游戏](../Category/Wii_U_Virtual_Console游戏.md "wikilink")
[Category:數獨](../Category/數獨.md "wikilink") [Category:東北大學
(日本)](../Category/東北大學_\(日本\).md "wikilink")

1.