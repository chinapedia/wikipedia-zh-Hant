## 法政界

  - [楊鐵樑](../Page/楊鐵樑.md "wikilink") －
    廣東中山人，1929年6月30日生於上海。1946年至1949年肄業於上海東吳大學法學系，後轉往英國倫敦大學法學系，1953年考取法學學士銜、1954年考取英國格雷法學院大律師資格。1956年任香港裁判司，1963年晉升高級裁判司、1968年升任地方法院法官、1975年升任香港最高法院按察司（最高法院大法官）、1980年升任香港法院上訴庭按察司（上訴庭大法官）。1988年獲英女王伊利沙伯二世冊封為勳爵，同年3月升任首席按察司（首席大法官）、1988年至1992年間任汶萊上訴庭庭長，1990年獲汶萊政府頒授高級爵士榮譽勳章。1999年獲香港特別行政區政府頒授大紫荊勳章。楊鐵樑於擔任首席按察司期間，積極參與改革香港的普通法制度，又改變了已沿用152年歷史的法律傳統，於1995年將中文引入香港法院。他又曾在1997年至2002年間出任香港特別行政區政府行政會議非官守成員。

  - [楊兆龍](../Page/楊兆龍.md "wikilink") － 中華民國法學家。

  - [蔣孝嚴](../Page/蔣孝嚴.md "wikilink") － 立法委員、前外交部長、前行政院副院長、前總統府秘書長。

  - [章孝慈](../Page/章孝慈.md "wikilink") －
    前東吳大學校長、[國民大會代表](../Page/國民大會.md "wikilink")。

  - [城仲模](../Page/城仲模.md "wikilink") －
    大法官、前[司法院副院長](../Page/司法院.md "wikilink")、前法務部長、「台灣民政府」前任主席。

  - [劉三錡](../Page/劉三錡.md "wikilink") －
    前行政院主計長、教育部會計長、[育達商業技術學院校長](../Page/育達商業技術學院.md "wikilink")，[佛光大學副校長](../Page/佛光大學.md "wikilink")。

  - [盧仁發](../Page/盧仁發.md "wikilink") －
    前[最高法院檢察署檢察總長](../Page/最高法院檢察署.md "wikilink")。

  - [吳英昭](../Page/吳英昭.md "wikilink") －
    前[最高法院檢察署檢察總長](../Page/最高法院檢察署.md "wikilink")。

  - [陸潤康](../Page/陸潤康.md "wikilink") － 前財政部長（1984－1985）、大安銀行董事長。

  - [白培英](../Page/白培英.md "wikilink") － 前財政部長（1992－1993）、中原大學董事會董事長。

  - [王榮周](../Page/王榮周.md "wikilink") －
    [財政部次長](../Page/財政部.md "wikilink")、財政部[國庫署署長](../Page/國庫署.md "wikilink")、[法務部](../Page/法務部.md "wikilink")[調查局第二位文人局長](../Page/法務部調查局.md "wikilink")

  - [程家瑞](../Page/程家瑞.md "wikilink") －
    東吳大學法學院院長、駐希臘大使、[亞洲國際航空法暨太空法學會會長](../Page/亞洲國際航空法暨太空法學會.md "wikilink")、巴黎[國際太空法高等研究院院士](../Page/國際太空法高等研究院.md "wikilink")

  - [胡正堯](../Page/胡正堯.md "wikilink") －
    外交官、駐[巴拿馬大使](../Page/巴拿馬.md "wikilink")、駐[巴拉圭大使](../Page/巴拉圭.md "wikilink")、駐[玻利維亞代表](../Page/玻利維亞.md "wikilink")、外交部中南美司司長（政治系59級）

  - [李德武](../Page/李德武.md "wikilink") － 前行政院退輔會副主委、行政院七組組長、交通部首席參事、司長。

  - [李念祖](../Page/李念祖.md "wikilink") －
    中華民國、美國律師、[理律法律事務所副執行長](../Page/理律法律事務所.md "wikilink")。

  - － 中華民國駐菲律賓大使、行政院僑委會主委、海基會副秘書長、國立成功大學政治經濟學研究所教授

  - [陳金讓](../Page/陳金讓.md "wikilink") －
    總統府資政、國民大會秘書長、[考選部部長](../Page/考選部.md "wikilink")。

  - [謝志偉](../Page/謝志偉_\(台灣\).md "wikilink") －
    東吳大學德文系教授、前新聞局長（2007－2008）、前駐德代表（2005.5－2007）。

  - [黃文圝](../Page/黃文圝.md "wikilink") －
    [司法院司法人員研習所所長](../Page/司法院.md "wikilink")、司法院副秘書長。

  - [賴杉桂](../Page/賴杉桂.md "wikilink") －
    經濟部中小企業處處長（2001年－）、經濟部商業司副司長（1998年－2000年）。

  - [葉維銓](../Page/葉維銓.md "wikilink") －
    總統府第一局局長、行政院研考會綜合計畫處處長、國家檔案局籌備處副主任。

  - [詹志宏](../Page/詹志宏.md "wikilink") －
    前[陸委會企劃處長兼海基會副秘書長](../Page/陸委會.md "wikilink")。

  - [鹿篤瑾](../Page/鹿篤瑾.md "wikilink") －
    [行政院主計處副主計長](../Page/行政院主計處.md "wikilink")（2008－）。

  - [顏宗明](../Page/顏宗明.md "wikilink") －
    行政院國科會[新竹科學園區管理局局長](../Page/新竹科學園區.md "wikilink")、[行政院研究發展考核委員會專門委員](../Page/行政院研究發展考核委員會.md "wikilink")、[行政院勞工委員會科長](../Page/行政院勞工委員會.md "wikilink")

  - [戴立寧](../Page/戴立寧.md "wikilink") －
    前財政部金融司司長、前首席參事、前保險司司長、前證管會主委、前財政部次長、[華僑商業銀行前任董事長](../Page/華僑商業銀行_\(台灣\).md "wikilink")、現任中國上海[上投摩根基金管理公司獨立董事](../Page/上投摩根基金管理公司.md "wikilink")(經由上投摩根股票型基金的招募說明書中得知)。

  - [羅淑蕾](../Page/羅淑蕾.md "wikilink") －
    台灣會計師，[台北市會計師公會理事長](../Page/台北市會計師公會.md "wikilink")，2007年遞補擔任[親民黨不分區立委](../Page/親民黨.md "wikilink")，2008年起任國親聯盟不分區立委。

  - [鄭文龍](../Page/鄭文龍.md "wikilink") － 法律扶助基金會發起人、2008年陳水扁家庭密帳案委任律師。

  - [陳雲南](../Page/陳雲南.md "wikilink") － 法務部參事、南投、新竹地檢署檢察長。

  - [鄧振中](../Page/鄧振中.md "wikilink") －
    前經濟部部長、中華民國駐世界貿易組織代表團副代表、駐美國代表處副代表、行政院大陸委員會副主委（法律系63級）

  - [洪昭男](../Page/洪昭男.md "wikilink") －
    監察院監察委員（2008年－）、立法委員（1981年－2005年）、外交部科長、駐美國領事。

  - [卓榮泰](../Page/卓榮泰.md "wikilink") － 前總統府秘書長。

  - [徐鴻志](../Page/徐鴻志.md "wikilink") － 曾任桃園縣長、桃園縣議員。

  - [許新枝](../Page/許新枝.md "wikilink") － 曾任內政部政務次長、桃園縣長。

  - [林政則](../Page/林政則.md "wikilink") －
    前新竹市長、前行政院政務委員兼任臺灣省政府省主席，中國國民黨副主席、[中華大學創辦人之一](../Page/中華大學.md "wikilink")、[康寧大學創辦人之一](../Page/康寧大學.md "wikilink")。

  - [張宏陸](../Page/張宏陸.md "wikilink") － 前板橋市市長（政治系）

  - [張廖萬堅](../Page/張廖萬堅.md "wikilink") － 台中市市議員（政治系）

  - [陳莉茵](../Page/陳莉茵.md "wikilink")
    －財團法人罕見疾病基金會常務董事、台灣弱勢病患權益促進會理事、美國小麥協會台灣事處執行秘書（政治系）

  - [徐中雄](../Page/徐中雄.md "wikilink") － 立法委員。

  - [洪玉欽](../Page/洪玉欽.md "wikilink") － 立法委員。

  - [彭紹瑾](../Page/彭紹瑾.md "wikilink") － 立法委員。

  - [許淵國](../Page/許淵國.md "wikilink") － 立法委員。

  - [周伯倫](../Page/周伯倫.md "wikilink") － 立法委員。

  - [張清芳](../Page/張清芳.md "wikilink") － 立法委員。

  - [鄭三元](../Page/鄭三元.md "wikilink") － 立法委員。

  - [黃重諺](../Page/黃重諺.md "wikilink") －
    臺灣總統府發言人、民進黨新聞部主任、黨副秘書長陳其邁辦公室主任、政策會副執行長、青年部主任

  - [吳水木](../Page/吳水木.md "wikilink") － 台北地方法院院長。

  - [黃文玲](../Page/黃文玲.md "wikilink") － 前立法委員。

  - [李來希](../Page/李來希.md "wikilink") －
    [勞動部簡任第十二職等技監](../Page/中華民國勞動部.md "wikilink")、中華民國公務人員協會理事長、[總統府](../Page/中華民國總統府.md "wikilink")[國家年金改革委員會委員](../Page/國家年金改革委員會.md "wikilink")（法律系69級）

  - [韓國瑜](../Page/韓國瑜.md "wikilink") －
    高雄市第3屆市長、國民黨高雄主委。曾任立法委員、中和市副市長、北農總經理。

  - [蔣絜安](../Page/蔣絜安.md "wikilink") － 立法委員。曾任桃園市政府客家事務局局長。（中文系）

  - [許秀雯](../Page/許秀雯.md "wikilink") － 社會運動者、執業律師。（法律系）

## 工商界

  - [辜濂松](../Page/辜濂松.md "wikilink") － 企業家、總統府顧問、中華民國（台灣）無任所大使。（會計系46級）
  - [辜仲諒](../Page/辜仲諒.md "wikilink") －
    企業家、[中國信託商業銀行總經理](../Page/中國信託商業銀行.md "wikilink")。
  - [林博義](../Page/林博義.md "wikilink") －
    華泰商業銀行董事長、前中國信託商業銀行首席執行副總、前[中信鯨棒球隊董事長](../Page/中信鯨棒球隊.md "wikilink")。
  - [劉吉人](../Page/劉吉人.md "wikilink") －
    富蘭克林證券投資顧問股份有限公司總裁、富蘭克林證券投資顧問股份有限公司董事長暨總經理、坦伯頓國際股份有限公司中國首席代表暨董事總經理。（政治系66級校友）
  - [杜英宗](../Page/杜英宗.md "wikilink") －
    [花旗環球台灣區董事長](../Page/花旗環球.md "wikilink")、美國高盛公司（[Goldman
    Sachs](../Page/Goldman_Sachs.md "wikilink")）紐約總部副總裁、[和運租車股份有限公司董事長](../Page/和運租車.md "wikilink")。
  - [王金山](../Page/王金山.md "wikilink") － 會計師、Deloitte
    台灣[勤業眾信會計師事務所集團副董事長](../Page/勤業眾信會計師事務所.md "wikilink")。
  - [王正一](../Page/王正一.md "wikilink") － 桂冠實業董事長。
  - [宋文琪](../Page/宋文琪.md "wikilink") －
    怡富資產管理台灣區總經理、JP摩根集團董事總經理、怡富大中華區共同基金行銷部經理。
  - [宋光夫](../Page/宋光夫.md "wikilink") － 奇美食品、奇菱科技董事長。
  - [何月欣](../Page/何月欣.md "wikilink") －
    [喬山健康科技副董事長](../Page/喬山健康科技.md "wikilink")。
  - [杜總輝](../Page/杜總輝.md "wikilink") －
    前[建弘投信董事長](../Page/建弘投信.md "wikilink")。
  - [張良士](../Page/張良士.md "wikilink") －
    [中華航空公司副總經理](../Page/中華航空公司.md "wikilink")。
  - [張清添](../Page/張清添.md "wikilink") － 台塑環保科技股份有限公司總經理。
  - [洪啟仁](../Page/洪啟仁.md "wikilink") － 安侯建業聯合會計師事務所執行董事。
  - [高文津](../Page/高文津.md "wikilink") － 高-{杰}-建設公司董事長。（政治系64級）
  - [高樹煌](../Page/高樹煌.md "wikilink") －
    前[統一證券總經理](../Page/統一證券.md "wikilink")。
  - [許誠洲](../Page/許誠洲.md "wikilink") －
    [玉山銀行副財務長](../Page/玉山銀行.md "wikilink")。(企管系74級)
  - [高志誠](../Page/高志誠.md "wikilink") － 台灣聯合物流公司總經理。
  - [蔡木霖](../Page/蔡木霖.md "wikilink") － 台灣省菸酒公賣局董事長（2006年－）
  - [翁建仁](../Page/翁建仁.md "wikilink") － 宏碁全球資深副總裁。
  - [張教華](../Page/張教華.md "wikilink") － 味全公司總經理、味全食品事業群副總經理。
  - [黃彥鈞](../Page/黃彥鈞.md "wikilink") － 達美樂披薩台灣區總經理。
  - [陳盛山](../Page/陳盛山.md "wikilink") － 華信航空董事長（政治系75級）
  - [陳調鋌](../Page/陳調鋌.md "wikilink") －
    科準科技董事長、工研院課長、美商益華科技研發經理、國科會國家系統晶片設計中心研發組長、思源科技副總、思源科技基金會執行長
  - [陳永清](../Page/陳永清.md "wikilink") － 會計師、PwC 台灣
    [資誠聯合會計師事務所副所長](../Page/資誠聯合會計師事務所.md "wikilink")。
  - [賴春田](../Page/賴春田.md "wikilink") － 會計師、PwC 台灣
    [資誠聯合會計師事務所所長](../Page/資誠聯合會計師事務所.md "wikilink")、亞太固網董事長、前國防工業發展基金會監事、資誠會計師事務所合夥主席。
  - [薛明玲](../Page/薛明玲.md "wikilink") － 會計師、PwC 台灣
    [資誠聯合會計師事務所所長](../Page/資誠聯合會計師事務所.md "wikilink")。
  - [李燕松](../Page/李燕松.md "wikilink") － 會計師、PwC 台灣
    [資誠聯合會計師事務所副所長](../Page/資誠聯合會計師事務所.md "wikilink")。
  - [蔡慧貞](../Page/蔡慧貞.md "wikilink") －
    [知本形象設計公司總經理](../Page/知本形象設計公司.md "wikilink")
  - [葉志勇](../Page/葉志勇.md "wikilink") － 前群益證券董事長兼總經理。(企管系)
  - [魏永篤](../Page/魏永篤.md "wikilink") －
    會計師、[勤業眾信會計師事務所總裁](../Page/勤業眾信會計師事務所.md "wikilink")。
  - [黃明富](../Page/黃明富.md "wikilink") －
    [中租迪和董事長](../Page/中租迪和.md "wikilink")、[台塑財務經理](../Page/台塑.md "wikilink")、[台工銀科技顧問董事長](../Page/台工銀科技顧問.md "wikilink")。
  - [蕭裔芬](../Page/蕭裔芬.md "wikilink") －
    [中天新聞主播](../Page/中天新聞.md "wikilink")、鴻海精密工業集團總裁特助、[永齡教育慈善基金會發言人](../Page/永齡教育慈善基金會.md "wikilink")。（英文系）
  - [吳文中](../Page/吳文中.md "wikilink") －
    台北市[太陽系MTV總經理](../Page/太陽系MTV.md "wikilink")、《影響電影雜誌》總編輯、[漫畫王連鎖店創辦人](../Page/漫畫王.md "wikilink")、[戰略高手副董事長](../Page/戰略高手.md "wikilink")。(會計系)
  - [楊思漢](../Page/楊思漢.md "wikilink") －
    [上海虹橋高爾夫俱樂部有限公司創辦人](../Page/上海虹橋高爾夫俱樂部有限公司.md "wikilink")、上海浩福置業董事長、美怡投資有限公司董事長。(經濟系)
  - [周耿堂](../Page/周耿堂.md "wikilink") —
    [Cynics職業球團創辦人](../Page/Cynics職業球團.md "wikilink")、現任總監。（社工系）

## 文學藝術界

  - [阿盛](../Page/阿盛.md "wikilink") － 中文系畢，中國時報副刊編輯、名作家。（中文系）
  - [吳繼文](../Page/吳繼文.md "wikilink") － 作家、翻譯家。（中文系）
  - [王宣一](../Page/王宣一.md "wikilink") － 作家。（中文系）
  - [路寒袖](../Page/路寒袖.md "wikilink") －
    作家、[國家文化總會](../Page/國家文化總會.md "wikilink")《新活水》總編輯（2005－），曾任[台中市政府文化局局長](../Page/台中市政府文化局.md "wikilink")、[高雄市政府文化局局長](../Page/高雄市政府文化局.md "wikilink")、[台灣日報副總編輯兼藝文中心主任](../Page/台灣日報.md "wikilink")（1995－2005）、國家台灣文學館《台灣文學館通訊》總編輯。（中文系）
  - [羊子喬](../Page/羊子喬.md "wikilink") － 詩人、台灣文學研究者。（中文系）
  - [謝旺霖](../Page/謝旺霖.md "wikilink") － 作家。（法律系、政治系）
  - [管仁健](../Page/管仁健.md "wikilink") － 文史工作者、文學、時事評論家。（中文系）
  - [孫梓評](../Page/孫梓評.md "wikilink") － 編輯、作家。（中文系）
  - [張維中](../Page/張維中.md "wikilink") － 編輯、作家。（英文系）
  - [施福珍](../Page/施福珍.md "wikilink") － 兒童文學作家、第六屆鐸聲獎音樂教育特別貢獻獎（1988）。
  - [朱延平](../Page/朱延平.md "wikilink") － 電影、電視導演。（外文系）
  - [萬仁](../Page/萬仁.md "wikilink") －
    電影導演，《兒子的大玩偶》（1983）、金馬獎編劇獎作品《油麻菜籽》（1984）。被視為台灣新電影重要導演之一。（外文系）
  - [張乾琦](../Page/張乾琦.md "wikilink") －
    攝影家、馬格蘭協會會員（Magnum）、世界新聞攝影比賽（World
    Press Photo）首獎、尤金·史密斯獎（W.Eugene Smith Award）。（英文系）
  - [邱瑗](../Page/邱瑗.md "wikilink")－
    [台中國家歌劇院藝術總監](../Page/台中國家歌劇院.md "wikilink")，曾任[國家交響樂團執行長](../Page/國家交響樂團.md "wikilink")（2006－2018）、加拿大溫哥華英皇學院學務長。（音樂系）
  - [姚若龍](../Page/姚若龍.md "wikilink") － 名作詞人，至目前為止發表國、台語歌詞共一千餘首。（會計系）
  - [陳中申](../Page/陳中申.md "wikilink") －
    [音樂家](../Page/音樂家.md "wikilink")、台視《[五燈獎](../Page/五燈獎.md "wikilink")》〈笛子演奏比賽〉五度五關得主、[臺北市立國樂團指揮](../Page/臺北市立國樂團.md "wikilink")（1992－2003）、1992年全國十大傑出青年、1998年中興文藝獎音樂獎。（音樂系）
  - [蘇顯達](../Page/蘇顯達.md "wikilink") － 小提琴家、台北愛樂管弦樂團首席。（音樂系）
  - [賀曾慶](../Page/賀曾慶.md "wikilink") －
    中文系畢，香港《教聲》周報總編輯。2013年參與製作《聖公會諸聖座堂120周年紀念》特刊，獲香港基督教出版聯會第六屆「金書獎」（教會紀念特刊組）。（參「[基督教金書獎](https://acpbookawards.hk/2015/07/13/2013-mb/)」）
  - [陳昭賢](../Page/陳昭賢.md "wikilink") －
    台灣著名歌仔戲團明華園，新生代小生，其母為風靡全台的[孫翠鳳](../Page/孫翠鳳.md "wikilink")（企管系）

## 媒體娛樂界

  - [于美人](../Page/于美人.md "wikilink") － 中文系畢，電台主持人、電視主持人。
  - [夏立民](../Page/夏立民.md "wikilink") － 記者、主持人、人權倡議者。（人權碩士學程）
  - [王崑義](../Page/王崑義.md "wikilink") －
    政治系畢，[自由時報撰述委員](../Page/自由時報.md "wikilink")、[台灣戰略學會理事長](../Page/台灣戰略學會.md "wikilink")、[旺報社論作者](../Page/旺報.md "wikilink")、[今日新聞網資深新聞總監](../Page/今日新聞網.md "wikilink")、淡江大學國際事務與戰略研究所兼任教授、世新大學新聞學系暨通識中心兼任教授、國立臺灣海洋大學通識中心兼任教授等。
  - [王淑麗](../Page/王淑麗.md "wikilink") －
    德文系畢，前TVBS文字記者、新聞主播與氣象主播、三立記者，現任[東森新聞氣象主播](../Page/東森新聞.md "wikilink")。
  - [林昆鋒](../Page/林昆鋒.md "wikilink") －
    政治系畢，[年代新聞晚報主播](../Page/年代新聞.md "wikilink")，[週末新聞追追追主持人](../Page/週末新聞追追追.md "wikilink")，政論節目[年代高峰會主持人](../Page/年代高峰會.md "wikilink")，[台北市政府](../Page/台北市政府.md "wikilink")[秘書處](../Page/臺北市政府秘書處.md "wikilink")[媒體事務組發言人](../Page/臺北市政府秘書處媒體事務組.md "wikilink")，[柯文哲競選辦公室發言人團長](../Page/柯文哲.md "wikilink")，現為[台北市政府](../Page/台北市政府.md "wikilink")[秘書處](../Page/臺北市政府秘書處.md "wikilink")[媒體事務組發言人](../Page/臺北市政府秘書處媒體事務組.md "wikilink")。
  - [王浩](../Page/王浩_\(台灣主播\).md "wikilink") － 中文系畢，台北市議員、中視新聞部記者。
  - [廖福順](../Page/廖福順.md "wikilink") － 中天電視新聞部總監。
  - [石怡潔](../Page/石怡潔.md "wikilink") －
    國貿系畢，現為八大第1台主播、新聞部採訪中心副主任、前台視記者主播、環球電視記者主播。
  - [邱秀珍](../Page/邱秀珍.md "wikilink") －
    哲學系畢，傳訊電視中天頻道、環球電視、三立新聞台、東森新聞台主播、新聞節目主持人，其夫為[龐建國](../Page/龐建國.md "wikilink")。
  - [黃致祥](../Page/黃致祥.md "wikilink") －
    [英文中國郵報](../Page/英文中國郵報.md "wikilink")（China
    Post）社長兼發行人。
  - [周盛淵](../Page/周盛淵.md "wikilink") － 中時媒體集團總經理、中國時報發行人、中天電視董事長、中視總經理。
  - [陳靜蘭](../Page/陳靜蘭.md "wikilink") － 年代新聞主播、前台視新聞部文字記者及主播、中天新聞主播。
  - [謝明智](../Page/謝明智.md "wikilink") －
    日文系畢，中視新聞部[日語](../Page/日語.md "wikilink")[新聞](../Page/新聞.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")、[主播](../Page/主播.md "wikilink")。
  - [顏蘭權](../Page/顏蘭權.md "wikilink") －
    哲學系畢，新銳導演，作品為紀錄片《無米樂》（2004）、《地震紀念冊》（2002）。
  - [粟奕倩](../Page/粟奕倩.md "wikilink") －
    中文系畢，曾主持華視《莒光園地》、《每日一字》，是[中華民國國軍官兵最熟悉的主持人之一](../Page/中華民國國軍.md "wikilink")。
  - [葉佳修](../Page/葉佳修.md "wikilink") －
    作曲人、[校園民歌奠基人之一](../Page/校園民歌.md "wikilink")。
  - [蔡郁潔](../Page/蔡郁潔.md "wikilink") － 經濟系畢，曾任TVBS記者主播、華視記者及主播、東森新聞主播。
  - [顧名儀](../Page/顧名儀.md "wikilink") －
    [TVBS新聞主播](../Page/TVBS.md "wikilink")，曾任飛碟電台記者、編播。
  - [李聖傑](../Page/李聖傑.md "wikilink") － 中文系畢，音樂人，網球國家代表隊選手。
  - [阮丹青](../Page/阮丹青.md "wikilink") －
    知名廣播節目主持人，音樂人，[上華唱片](../Page/上華唱片.md "wikilink")1998年度新人。
  - [利菁](../Page/利菁.md "wikilink") － 英文系肄業（大二休學），節目主持人，藝人。
  - [劉謙](../Page/劉謙_\(魔術師\).md "wikilink") － 日文系畢，知名魔術師，藝人。
  - [李佑群](../Page/李佑群.md "wikilink") － 日文系畢，知名造型師、時尚雜誌總編輯。
  - [趙虹喬](../Page/趙虹喬.md "wikilink") －
    會計系畢，[喬傑立娛樂旗下女團](../Page/喬傑立娛樂.md "wikilink")[七朵花的團長](../Page/七朵花.md "wikilink")，前R\&B組合的團員，藝人。
  - [田金益](../Page/田金益.md "wikilink") － 《台灣法學雜誌》總經理、總編輯。
  - [林廷遙](../Page/林廷遙.md "wikilink") －
    《中國法律評論》主編。前《台灣法學雜誌》執行總編輯。曾任威京總部(Corepacific)集團、誠品(Eslite)集團法務長。
  - [蔡緯嘉](../Page/蔡阿嘎.md "wikilink") － 網路名人、Youtuber（社工系96級）
  - [魏蔓](../Page/魏蔓.md "wikilink") － 國貿系畢業，台灣女演員和模特兒
  - [黃宏成台灣阿成世界偉人財神總統](../Page/黃宏成台灣阿成世界偉人財神總統.md "wikilink") －
    法律系畢業，行為藝術家。
  - [蕭有為](../Page/蕭有為.md "wikilink") － 日文系畢業，台灣男演員和動作指導
  - [張乃文 (配音員)](../Page/張乃文_\(配音員\).md "wikilink") -
    日文系畢業，台灣女性配音員、翻譯、廣播節目《動漫我要聽》助理主持人及企劃、電波里書店店長。
  - [萱野可芬](../Page/萱野可芬.md "wikilink") -
    演員、[2\*Sweet成員](../Page/2*Sweet.md "wikilink")

## 學術界

  - [楊其銑](../Page/楊其銑.md "wikilink") －
    東吳大學校長（1983-1992）、名譽校長。東吳在台復校後第一屆（47級）外國語文學系畢業生，美國加州大學洛杉磯校區（UCLA）語言學碩士。歷任本校外文系教授、系主任、文學院院長、教務長、副校長、校長。曾任中華民國駐美文化參事、北美事務協調委員會文化組組長等職。
  - [石超庸](../Page/石超庸.md "wikilink") －
    東吳大學校長（1957-1968）、歷任東吳大學法學院院長，曾任[中國國民黨中央評議委員](../Page/中國國民黨.md "wikilink")。
  - [王煦棋](../Page/王煦棋.md "wikilink") － 東吳大學法律學系專任教授
  - [成永裕](../Page/成永裕.md "wikilink") － 東吳大學法律學系專任副教授
  - [周質平](../Page/周質平.md "wikilink") －
    美國[普林斯頓大學東亞研究中心](../Page/普林斯頓大學.md "wikilink")(East
    Asian Studies Program) 主任、教授。
  - [潘維大](../Page/潘維大.md "wikilink") － 現任東吳大學校長。東吳大學法學院教授、東吳大學前學生事務處長。
  - [胡楚生](../Page/胡楚生.md "wikilink") －
    新加坡[南洋大学助理教授](../Page/南洋大學_\(新加坡\).md "wikilink")、[國立中興大學中文系教授](../Page/國立中興大學.md "wikilink")，著有《訓詁學大綱》。
  - [孫同文](../Page/孫同文.md "wikilink") －
    [香港中文大學政治與行政學系助理教授](../Page/香港中文大學.md "wikilink")（1992－1996年）、[國立暨南國際大學公行系教授](../Page/國立暨南國際大學.md "wikilink")。（政治系70級校友）
  - [袁鶴翔](../Page/袁鶴翔.md "wikilink") －
    [香港中文大學英語系主任](../Page/香港中文大學.md "wikilink")、教授、前國立政治大學、國立台灣大學、東吳大學講座教授。
  - [梁國源](../Page/梁國源.md "wikilink") －
    [寶華綜合經濟研究院院長](../Page/寶華綜合經濟研究院.md "wikilink")、清大經濟學系系主任、經濟研究所所長。（經濟所校友）
  - [許舒翔](../Page/許舒翔.md "wikilink") －
    [環球科技大學校長](../Page/環球科技大學.md "wikilink")。（政治系校友）
  - [李肇修](../Page/李肇修.md "wikilink") －
    [國立台南藝術學院校長](../Page/國立台南藝術學院.md "wikilink")（2007-）、台灣史上最年輕國立大學校長。
  - [王鳳生](../Page/王鳳生.md "wikilink") －
    [國立高雄大學前副校長](../Page/國立高雄大學.md "wikilink")。（經濟系校友）
  - [陳建勝](../Page/陳建勝.md "wikilink") －
    [朝陽科技大學副校長](../Page/朝陽科技大學.md "wikilink")、[致遠管理學院代理校長](../Page/致遠管理學院.md "wikilink")。（商數系校友）
  - [曾育裕](../Page/曾育裕.md "wikilink") －
    [國立台北護理學院副校長](../Page/國立台北護理學院.md "wikilink")、前總務長、前學務長。
  - [胡淑芬](../Page/胡淑芬.md "wikilink") － 國立師範大學物理系教授。
  - [鄧衍森](../Page/鄧衍森.md "wikilink") － 東吳大學法學院教授。
  - [成永裕](../Page/成永裕.md "wikilink") － 東吳大學法學院副教授、前任院長。
  - [李復甸](../Page/李復甸.md "wikilink") －
    [世新大學法學院院長](../Page/世新大學.md "wikilink")。
  - [王毓正](../Page/王毓正.md "wikilink") －
    [國立成功大學法律學系暨研究所教授](../Page/國立成功大學.md "wikilink")。（法律系）
  - [朱心蘅](../Page/朱心蘅.md "wikilink") －
    [逢甲大學財務金融學系教授](../Page/逢甲大學.md "wikilink")。(經濟系校友)
  - [謝登隆](../Page/謝登隆.md "wikilink") －
    [東海大學前國際貿易學系系主任](../Page/東海大學_\(台灣\).md "wikilink")。(商數系校友)
  - [王偉勇](../Page/王偉勇.md "wikilink") －
    [國立成功大學文學院院長](../Page/國立成功大學.md "wikilink")、通識教育中心主任、副教務長、藝術研究所所長、中國文學學系主任；東吳大學總務長、主任秘書。
  - [高玉泉](../Page/高玉泉.md "wikilink") －
    [國立中興大學法政學院院長](../Page/國立中興大學法政學院.md "wikilink")、中華民國國際法學會理事。
  - [劉昭辰](../Page/劉昭辰.md "wikilink") －
    [國立中興大學法政學院法律學系教授](../Page/國立中興大學.md "wikilink")。
  - [王韶濱](../Page/王韶濱.md "wikilink") －
    [國立中興大學會計學研究所助理教授](../Page/國立中興大學.md "wikilink")。
  - [吳經熊](../Page/吳經熊.md "wikilink") －
    著名法學家，立法院憲法草案起草委員會副委員長，中華民國駐教廷公使，東吳大學法學院院長，中國文化學院院長。（東吳大學法科）
  - [吳志光](../Page/吳志光.md "wikilink") －
    [天主教輔仁大學法律學系公法學教授](../Page/天主教輔仁大學.md "wikilink")。
  - [吳重禮](../Page/吳重禮.md "wikilink") －
    [中央研究院政治學研究所研究員](../Page/中央研究院.md "wikilink")、前[國立中正大學政治系教授](../Page/國立中正大學.md "wikilink")。
  - [吳明上](../Page/吳明上.md "wikilink") －
    [義守大學大眾傳播學系教授](../Page/義守大學.md "wikilink")。
  - [呂炳寬](../Page/呂炳寬.md "wikilink") －
    東海大學主任秘書、公行系副教授、前[中台科技大學副教授](../Page/中台科技大學.md "wikilink")。
  - [邱志淳](../Page/邱志淳.md "wikilink") － 世新大學行政管理學系助理教授。
  - [江惜美](../Page/江惜美.md "wikilink") －
    銘傳大學應用中文系主任、臺北市立師範學院語文教育系教授、輔導組主任。
  - [沈有忠](../Page/沈有忠.md "wikilink") － 東海大學政治系副教授、助理教授。
  - [胡龍騰](../Page/胡龍騰.md "wikilink") －
    世新大學行政管理學系教授、[台灣透明組織知識管理部主任](../Page/台灣透明組織.md "wikilink")。
  - [尹章華](../Page/尹章華.md "wikilink") －
    [國立台灣海洋大學法律研究所代所長](../Page/國立台灣海洋大學.md "wikilink")、[消費者文教基金會執行董事兼](../Page/消費者文教基金會.md "wikilink")[消費者報導雜誌社社長](../Page/消費者報導.md "wikilink")。
  - [余啟民](../Page/余啟民.md "wikilink") －
    東吳大學法律系副教授、[東森國際網路股份有限公司副總經理兼法務處主管](../Page/東森國際網路股份有限公司.md "wikilink")。（法律系）
  - [李金城](../Page/李金城.md "wikilink") －
    [國立高雄師範大學中文系教授](../Page/國立高雄師範大學.md "wikilink")
  - [李建良](../Page/李建良_\(法學家\).md "wikilink") －
    [中央研究院法律研究所籌備處研究員](../Page/中央研究院.md "wikilink")、[國立台灣大學法律系兼任教授](../Page/國立台灣大學.md "wikilink")（法律系）
  - [林誠二](../Page/林誠二.md "wikilink") －
    東吳大學法律系[民法教授](../Page/民法.md "wikilink")、前[國立中興大學法律學系教授兼系主任](../Page/國立中興大學.md "wikilink")。
  - [林瓊珠](../Page/林瓊珠.md "wikilink") － 東吳大學政治系助理教授。
  - [林建隆](../Page/林建隆.md "wikilink") －
    東吳大學英文系教授、詩人、「流氓教授」（曾管訓、提報流氓）。（英文系）
  - [武永生](../Page/武永生.md "wikilink") －
    [銘傳大學法學院院長](../Page/銘傳大學.md "wikilink")。
  - [莊修田](../Page/莊修田.md "wikilink") －
    [中原大學室內設計系副教授](../Page/中原大學.md "wikilink")、台灣室內空間設計學會（創會）理事長。
  - [莊國銘](../Page/莊國銘.md "wikilink") －
    [暨南國際大學公共行政與政策學系助理教授](../Page/暨南國際大學.md "wikilink")、[中國文化大學政治系助理教授](../Page/中國文化大學.md "wikilink")。
  - [張弘遠](../Page/張弘遠.md "wikilink") －
    [致理科技大學國際貿易系副教授](../Page/致理科技大學.md "wikilink")。（國貿系）
  - [張五岳](../Page/張五岳.md "wikilink") －
    [淡江大學中國大陸研究所教授](../Page/淡江大學.md "wikilink")。（政治系）
  - [王素彎](../Page/王素彎.md "wikilink") －
    [中華經濟研究院第三研究所副所長](../Page/中華經濟研究院.md "wikilink")。(經濟系、經濟所校友)
  - [連文榮](../Page/連文榮.md "wikilink") －
    [中華經濟研究院第二研究所副研究員](../Page/中華經濟研究院.md "wikilink")。(經濟系、經濟所校友)
  - [呂正惠](../Page/呂正惠.md "wikilink") －
    曾任[國立清華大學中文系教授](../Page/國立清華大學.md "wikilink")、[淡江大學中文系教授](../Page/淡江大學.md "wikilink")。（中研所博士班）
  - [李立信](../Page/李立信.md "wikilink") －
    [香港珠海學院中國文學與歷史研究所教授](../Page/香港珠海學院.md "wikilink")，曾任[東海大學中文系教授](../Page/東海大學.md "wikilink")。（中研所博士班）
  - [王金凌](../Page/王金凌.md "wikilink") －
    [輔仁大學中國文學系教授](../Page/輔仁大學.md "wikilink")。（中研所博士班）
  - [周彥文](../Page/周彥文.md "wikilink") －
    [淡江大學中國文學系榮譽教授](../Page/淡江大學.md "wikilink")。（中研所博士班）
  - [張火慶](../Page/張火慶.md "wikilink") －
    [國立中興大學中文系退休教授](../Page/國立中興大學.md "wikilink")。（中研所博士班）
  - [胡幼峰](../Page/胡幼峰.md "wikilink") －
    [輔仁大學中文系退休副教授](../Page/輔仁大學.md "wikilink")。（中研所博士班）
  - [張瑞芬](../Page/張瑞芬_\(教授\).md "wikilink") －
    [逢甲大學中文系教授](../Page/逢甲大學.md "wikilink")。（中研所博士班）
  - [陳大為](../Page/陳大為.md "wikilink") －
    [國立台北大學中國文學系教授](../Page/國立台北大學.md "wikilink")、作家。（中文系碩士班）
  - [陳葆文](../Page/陳葆文.md "wikilink") －
    [國立台北教育大學語文與創作學系退休副教授](../Page/國立台北教育大學.md "wikilink")。（中研所博士班）
  - [丁亞傑](../Page/丁亞傑.md "wikilink") －
    [國立中央大學中文系副教授](../Page/國立中央大學.md "wikilink")。（中研所博士班）
  - [莊宜文](../Page/莊宜文.md "wikilink") －
    [國立中央大學中文系副教授](../Page/國立中央大學.md "wikilink")。（中研所博士班）
  - [林聰明](../Page/林聰明_\(教授\).md "wikilink") －
    [逢甲大學中國文學系退休教授](../Page/逢甲大學.md "wikilink")、[東海大學中文系兼任教授](../Page/東海大學.md "wikilink")。（中文系）
  - [張群](../Page/張群.md "wikilink") －
    [國立臺中科技大學應用華語系助理教授](../Page/國立臺中科技大學.md "wikilink")。
  - [張曉風](../Page/張曉風.md "wikilink") －
    [國立陽明大學教授](../Page/國立陽明大學.md "wikilink")、國家文藝獎、[中國時報文學獎](../Page/中國時報.md "wikilink")、[聯合報文學獎](../Page/聯合報.md "wikilink")、中山文藝獎得獎人。
  - [張曼娟](../Page/張曼娟.md "wikilink") －
    曾任[東吳大學](../Page/東吳大學.md "wikilink")[中國文學系教授](../Page/中國文學系.md "wikilink")、[香港中文大學助理教授](../Page/香港中文大學.md "wikilink")。[教育部](../Page/教育部.md "wikilink")「文藝創作」小說首獎、中華文學獎得獎人。
  - [鹿憶鹿](../Page/鹿憶鹿.md "wikilink") －
    [東吳大學](../Page/東吳大學.md "wikilink")[中國文學系教授](../Page/中國文學系.md "wikilink")。
  - [廖文煥](../Page/廖文煥.md "wikilink") －
    [中國文化大學法律系主任教授](../Page/中國文化大學.md "wikilink")。
  - [廖元豪](../Page/廖元豪.md "wikilink") －
    [國立政治大學法律學系助理教授](../Page/國立政治大學.md "wikilink")。（法律系）
  - [廖玉蕙](../Page/廖玉蕙.md "wikilink") －
    [國立台北教育大學](../Page/國立台北教育大學.md "wikilink")[語文與創作學系退休教授](../Page/語文與創作學系.md "wikilink")、中国文藝協會文藝獎章、中山文藝創作獎、中興文藝獎章。
  - [廖森茂](../Page/廖森茂.md "wikilink") －
    [中原大學電子學系主任](../Page/中原大學.md "wikilink")、教授。
  - [廖興中](../Page/廖興中.md "wikilink") －
    [世新大學公行系助理教授](../Page/世新大學.md "wikilink")。
  - [張堂錡](../Page/張堂錡.md "wikilink") －
    [國立政治大學中文系副教授](../Page/國立政治大學.md "wikilink")。（中研所博士班）
  - [徐仁輝](../Page/徐仁輝.md "wikilink") －
    [財政部政務次長](../Page/財政部.md "wikilink")、前[世新大學經濟學系主任兼管理學院院長](../Page/世新大學.md "wikilink")、前[台北縣政府財政局局長](../Page/台北縣政府.md "wikilink")。
  - [徐耀南](../Page/徐耀南.md "wikilink")
    －[銘傳大學經濟系系主任](../Page/銘傳大學.md "wikilink")。
  - [黃常仁](../Page/黃常仁.md "wikilink") －
    [國立高雄大學法律系刑事法學教授](../Page/國立高雄大學.md "wikilink")、前主任。
  - [黃介正](../Page/黃介正.md "wikilink") －
    [淡江大學戰略所所長](../Page/淡江大學.md "wikilink")、陸委會副主委、美國戰略暨國際研究中心(CSIS)資深研究員、美國布魯金斯研究院研究員（政治系）。
  - [黃煥榮](../Page/黃煥榮.md "wikilink") －
    [銘傳大學公共事務學系教授](../Page/銘傳大學.md "wikilink")、主任。
  - [趙順文](../Page/趙順文.md "wikilink") －
    [國立台灣大學日本語文學系退休教授](../Page/國立台灣大學.md "wikilink")、[開南大學應用日語學系講座教授](../Page/開南大學.md "wikilink")。（日文系）
  - [董保城](../Page/董保城.md "wikilink") －
    [國立政治大學前總務長](../Page/國立政治大學.md "wikilink")、法律系教授。考試院考選部部長。
  - [詹炳耀](../Page/詹炳耀.md "wikilink") －
    [國立雲林科技大學科技法律研究所助理教授](../Page/國立雲林科技大學.md "wikilink")
  - [蔡定平](../Page/蔡定平.md "wikilink") －
    [國立台灣大學物理系教授](../Page/國立台灣大學.md "wikilink")（物理系72級）。
  - [蔡志方](../Page/蔡志方.md "wikilink") －
    [國立成功大學法律學系暨研究所教授](../Page/國立成功大學.md "wikilink")，前所長。
  - [蔡偉銑](../Page/蔡偉銑.md "wikilink") －
    [東海大學行政管理暨政策學系助理教授](../Page/東海大學.md "wikilink")。
  - [周天](../Page/周天_\(教授\).md "wikilink") －
    [國立高雄科技大學科技法律研究所副教授](../Page/國立高雄科技大學.md "wikilink")。
  - [袁鶴齡](../Page/袁鶴齡.md "wikilink") －
    [國立中興大學國際政治研究所所長](../Page/國立中興大學.md "wikilink")、中華台商研究學會副理事長、夏潮基金會董事、若水堂圖書公司總監、東海大學政治所兼教授
  - [卓惇慧](../Page/卓惇慧.md "wikilink") －
    [文藻外語學院英文系教授](../Page/文藻外語學院.md "wikilink")。
  - [楊志弘](../Page/楊志弘.md "wikilink") －
    [銘傳大學大眾傳播學系主任](../Page/銘傳大學.md "wikilink")、所長、傳播學院院長、前中時報系記者、撰述委員、執行副總編輯。
  - [楊楨](../Page/楊楨.md "wikilink") －
    [東吳大學法律系教授](../Page/東吳大學.md "wikilink")、前法學院長、前發展處長
  - [游志誠](../Page/游志誠.md "wikilink") －
    [國立彰化師範大學國文系教授](../Page/國立彰化師範大學.md "wikilink")、前主任。
  - [向明恩](../Page/向明恩.md "wikilink") －
    [國立台北大學法律學系民法學副教授](../Page/國立台北大學.md "wikilink")。
  - [程明修](../Page/程明修.md "wikilink") －
    [東吳大學法律學系行政法學副教授](../Page/東吳大學.md "wikilink")。
  - [陳子平](../Page/陳子平.md "wikilink") －
    [國立高雄大學法律學系刑法學教授](../Page/國立高雄大學.md "wikilink")。
  - [陳立剛](../Page/陳立剛.md "wikilink") －
    [東吳大學政治系副教授](../Page/東吳大學.md "wikilink")、東吳大學發展處處長。
  - [陳俊明](../Page/陳俊明.md "wikilink") －
    [理論與政策季刊總編輯](../Page/理論與政策.md "wikilink")、國家發展研究院研究處處長、[世新大學行政管理學系教授](../Page/世新大學.md "wikilink")。
  - [陳麗君](../Page/陳麗君.md "wikilink") －
    [國立成功大學台灣文學系副教授](../Page/國立成功大學.md "wikilink")。
  - [陳瑞崇](../Page/陳瑞崇.md "wikilink")（[陳睿耆](../Page/陳睿耆.md "wikilink")） －
    [東吳大學政治學系副教授](../Page/東吳大學.md "wikilink")。
  - [陳俊宏](../Page/陳俊宏.md "wikilink") －
    [國家人權博物館館長](../Page/國家人權博物館.md "wikilink")、東吳大學政治學系教授。
  - [陳榮傳](../Page/陳榮傳.md "wikilink") －
    [國立台北大學法律學系教授](../Page/國立台北大學.md "wikilink")。
  - [陳美華](../Page/陳美華.md "wikilink") －
    [國立中山大學社會學系教授](../Page/國立中山大學.md "wikilink")。
  - [陳炳崑](../Page/陳炳崑.md "wikilink") －
    [世新大學日文系主任](../Page/世新大學.md "wikilink")、教授。
  - [陳豔紅](../Page/陳豔紅.md "wikilink") －
    [中央警察大學通識教育中心主任](../Page/中央警察大學.md "wikilink")、教授。
  - [蔣美華](../Page/蔣美華.md "wikilink") －
    [國立彰化師範大學國文系教授](../Page/國立彰化師範大學.md "wikilink")。
  - [蔣麗君](../Page/蔣麗君.md "wikilink") －
    [國立成功大學政治系教授](../Page/國立成功大學.md "wikilink")、樹德科技大學國際企業與貿易系主任。
  - [潘懷宗](../Page/潘懷宗.md "wikilink") －
    [東吳大學兼任教授](../Page/東吳大學.md "wikilink")，國立陽明大學副總務長、儀器中心主任、主任秘書、副教授、教授。（化學系）
  - [蘇文郎](../Page/蘇文郎.md "wikilink") －
    [國立政治大學日本語文系教授](../Page/國立政治大學.md "wikilink")、前東吳大學推廣部日文班主任。
  - [蘇伯顯](../Page/蘇伯顯.md "wikilink") －
    [國立政治大學企管系教授兼政大公企中心主任](../Page/國立政治大學.md "wikilink")（政治系）。
  - [鍾芳珍](../Page/鍾芳珍.md "wikilink") －
    [淡江大學日本語文學系副教授](../Page/淡江大學.md "wikilink")、成人教育部日語中心主任。
  - [羅清俊](../Page/羅清俊.md "wikilink") －
    [國立台北大學公行系教授](../Page/國立台北大學.md "wikilink")、前[淡江大學公行系副教授](../Page/淡江大學.md "wikilink")。
  - [劉兆祐](../Page/劉兆祐.md "wikilink") －
    [國立台北大學](../Page/國立台北大學.md "wikilink")、[台北市立師範學院教授](../Page/台北市立師範學院.md "wikilink")、主任、所長、東吳大學講座教授。
  - [劉如熹](../Page/劉如熹.md "wikilink") －
    [國立台灣大學化學系教授](../Page/國立台灣大學.md "wikilink")、[工業技術研究院工業材料研究所副研究員](../Page/工業技術研究院.md "wikilink")、研究員、主任。
  - [劉書彬](../Page/劉書彬.md "wikilink") －
    [東吳大學政治系副教授](../Page/東吳大學.md "wikilink")。
  - [鄭冠宇](../Page/鄭冠宇.md "wikilink") －
    [東吳大學法律系副教授](../Page/東吳大學.md "wikilink")、前中華比較法學會秘書長、[中國文化大學法律系專任副教授](../Page/中國文化大學.md "wikilink")。（法律系）
  - [閻振瀛](../Page/閻振瀛.md "wikilink") －
    [國立成功大學外文系教授](../Page/國立成功大學.md "wikilink")、前文學院院長。
  - [朱慶琪](../Page/朱慶琪.md "wikilink") －
    [國立中央大學物理系教授](../Page/國立中央大學.md "wikilink")。（物理系）
  - [孫玉傑](../Page/孫玉傑.md "wikilink") －
    社會學系畢業，[香港城市大學專業進修學院社會及人文科學部講師](../Page/香港城市大學.md "wikilink")。著有《駕馭焦慮─「認知治療」自學
    / 輔助手冊》（ 黃富強、孫玉傑 天健 ，2007-03）
  - [馬成龍](../Page/馬成龍.md "wikilink") －
    英文系畢業，[香港浸會大學傳理學院副院長](../Page/香港浸會大學.md "wikilink")、傳播系教授，曾獲美國國家傳播學會國際及跨文化傳播組1997年度傑出研究獎（因1996年出版之“最佳論文”），美國中華傳播研究學會2005年度卓越服務獎，浸大2013年度傑出服務校長獎、2007/2009/2011/2012傳理學院傑出教學獎。2001年，應邀為《國際媒體與傳播百科全書》（2003）撰寫“台灣媒體的地位”一文。馬教授曾在加拿大和美國任教，並於2000年在紐約州立大學弗里多尼亞分校晉升至教授職級。2011年，在台灣國科會資助下，在台灣政治大學傳播學院擔任為期六個月的客座教授。
  - [鄧穎懋](../Page/鄧穎懋.md "wikilink") －
    [義守大學國際商務學系副教授](../Page/義守大學.md "wikilink")，國立高雄大學法律系、政法系及財法系兼任副教授。
    (法律系)
  - [潘惠森](../Page/潘惠森.md "wikilink") － 1979年英文系畢業、 M.A. (1982) in Asian
    Studies, University of California, Santa
    Barbara.現任[香港演藝學院戲劇學院教授](../Page/香港演藝學院.md "wikilink")、署理院長，香港知名舞台劇劇本創作人。潘惠森加入香港演藝學院戲劇學院之前，受聘於香港新域劇團任藝術總監(1993-2012)。他的第一部編劇作品《榕樹蔭下的森林》，由香港話劇團於八十年代中演出；其後創作不輟，並獲得多個獎項。作為劇本創作人，他曾與香港及海外多個藝團合作，包括進劇場演出《闖進一棵橡樹的年輪》(1996)，新加坡實踐劇場演出《貓城記》(2000)，劇場組合演出《男人之虎》(2005)，德國杜賽爾夫劇院舞台朗讀《在天台上冥想的蜘蛛》(2009)，以及香港話劇團演出《都是龍袍惹的禍》(2013)等；他自任導演的作品則有香港話劇團演出《敦煌‧流沙‧包》(2009)，新域劇團演出《人間煙火》(2009)，及香港藝術節演出《示範單位》(2012)
    等。
  - [甯應斌](../Page/甯應斌.md "wikilink") －
    [國立中央大學哲學研究所特聘教授](../Page/國立中央大學.md "wikilink")。（企管系）
  - [陳玲玲](../Page/陳玲玲.md "wikilink")=[洪祖玲](../Page/洪祖玲.md "wikilink") －
    [國立臺北藝術大學戲劇學系教授](../Page/國立臺北藝術大學.md "wikilink")。（外文系）
  - [陸愛玲](../Page/陸愛玲.md "wikilink") －
    [國立臺北藝術大學戲劇學系教授](../Page/國立臺北藝術大學.md "wikilink")、劇場編導。（中文系）
  - [黃永武](../Page/黃永武.md "wikilink") －
    作家、[國立高雄師範大學大學國文學系教授](../Page/國立高雄師範大學.md "wikilink")、[國立中興大學](../Page/國立中興大學.md "wikilink")、[國立成功大學文學院院長](../Page/國立成功大學.md "wikilink")。（中文系）
  - [林蕙瑛](../Page/林蕙瑛.md "wikilink") －
    [東吳大學心理學系兼任副教授](../Page/東吳大學.md "wikilink")、婚姻諮商、性諮商專家。（英文系）
  - [徐興慶](../Page/徐興慶.md "wikilink") －
    [中國文化大學校長](../Page/中國文化大學.md "wikilink")、[國立台灣大學日文學系兼任教授](../Page/國立台灣大學.md "wikilink")。（日文系）
  - [周志文](../Page/周志文_\(作家\).md "wikilink") －
    作家、[國立台灣大學中國文學系退休教授](../Page/國立台灣大學.md "wikilink")。（中文系）
  - [林慶彰](../Page/林慶彰.md "wikilink") －
    [中央研究院中國文哲研究所兼任研究員](../Page/中央研究院.md "wikilink")、[東吳大學中國文學系兼任教授](../Page/東吳大學.md "wikilink")。（中文系）
  - [王次澄](../Page/王次澄.md "wikilink") －
    [國立中央大學中國文學系退休教授](../Page/國立中央大學.md "wikilink")。（中文系）
  - [周虎林](../Page/周虎林.md "wikilink") －
    [國立高雄師範大學國文學系退休教授](../Page/國立高雄師範大學.md "wikilink")。（中文系）
  - [周全](../Page/周全.md "wikilink") －
    [國立台北教育大學語文與創作學系退休副教授](../Page/國立台北教育大學.md "wikilink")。（中文系）
  - [阮斐娜](../Page/阮斐娜.md "wikilink") －
    美國[科羅拉多大學波德分校亞洲語言文化學系教授](../Page/科羅拉多大學.md "wikilink")。（日文系）
  - [吳佩珍](../Page/吳佩珍.md "wikilink") －
    [國立政治大學台灣文學研究所副教授兼所長](../Page/國立政治大學.md "wikilink")。（日文系）
  - [吳堅立](../Page/吳堅立.md "wikilink") － 華語教學教師，獲頒澳大利亞勳位獎章（OAM）。（中文系）
  - [于乃明](../Page/于乃明.md "wikilink") －
    [國立政治大學日本研究學位學程教授兼系主任](../Page/國立政治大學.md "wikilink")。（日文系）
  - [陳逢源](../Page/陳逢源_\(教授\).md "wikilink") －
    [國立政治大學中國文學系特聘教授](../Page/國立政治大學.md "wikilink")。（中文系）
  - [游勝冠](../Page/游勝冠.md "wikilink") －
    [國立成功大學台灣文學系教授](../Page/國立成功大學.md "wikilink")。（中文系）
  - [李奭學](../Page/李奭學.md "wikilink") －
    [中央研究院中國文哲研究所研究員](../Page/中央研究院.md "wikilink")。（英文系）
  - [呂健忠](../Page/呂健忠.md "wikilink") －
    [東吳大學英文學系兼任副教授](../Page/東吳大學.md "wikilink")、西洋文學經典翻譯者。（英文系）
  - [李順興](../Page/李順興.md "wikilink") －
    [國立中興大學外國語文學系教授](../Page/國立中興大學.md "wikilink")。（英文系）
  - [須文蔚](../Page/須文蔚.md "wikilink") －
    [國立東華大學華文文學系教授](../Page/國立東華大學.md "wikilink")、文學評論者。（法律系）
  - [王國良](../Page/王國良.md "wikilink") －
    [國立台北大學中國文學系名譽教授](../Page/國立台北大學.md "wikilink")（中文系）
  - [朱孟庭](../Page/朱孟庭.md "wikilink") －
    [國立台北大學中國文學系教授兼系主任](../Page/國立台北大學.md "wikilink")（中文系）
  - [孫劍秋](../Page/孫劍秋.md "wikilink") －
    [國立台北教育大學語文與創作學系教授](../Page/國立台北教育大學.md "wikilink")（中文系）
  - [劉南芳](../Page/劉南芳.md "wikilink") －
    [國立成功大學台灣文學系副教授](../Page/國立成功大學.md "wikilink")、歌仔戲編劇。（中文系）
  - [何淮中](../Page/何淮中.md "wikilink") －
    [中央研究院統計科學研究所研究員](../Page/中央研究院.md "wikilink")、[國立台灣大學財務金融學系合聘教授](../Page/國立台灣大學.md "wikilink")。（數學系）
  - [金培懿](../Page/金培懿.md "wikilink") －
    [國立臺灣師範大學國文學系教授](../Page/國立臺灣師範大學.md "wikilink")。（中文系）
  - [張文朝](../Page/張文朝.md "wikilink") －
    [中央研究院中國文哲研究所副研究員](../Page/中央研究院.md "wikilink")、[國立臺灣師範大學文學院日語學程兼任副教授](../Page/國立臺灣師範大學.md "wikilink")。（日文系）
  - [黃東陽](../Page/黃東陽.md "wikilink") －
    [國立中興大學中國文學系副教授](../Page/國立中興大學.md "wikilink")。（中文系）
  - [姚紹基](../Page/姚紹基.md "wikilink") －
    [國立政治大學歐洲語文學系副教授兼系主任](../Page/國立政治大學.md "wikilink")。（德文系）

## 東吳大學香港校友會

前身為1952年由蘇州、上海東吳大學校友創立的「東吳大學香港同學會」，該會首任會長為曾任香港中華基督教青年會總幹事的彭紹賢。東吳大學香港同學會於1952年11月7日獲社團登記處批准註冊，成為香港合法社團；1965年註冊成立「東吳大學香港同學會有限公司」。

1996年5月30日，東吳大學香港同學會有限公司解散；臺灣東吳大學的香港校友黃德全、李國明、孫玉傑、鄒偉民及賀曾慶，此時組成「東吳大學香港校友會籌備委員會」，孫玉傑又到臺北向東吳大學校方解釋，獲校長劉源俊授權，向皇家香港警察社團事務處申請註冊成立「東吳大學香港校友會」。1996年12月27日，社團事務處發出通知書，批准東吳大學香港校友會成立。該會於1997年2月15日舉行成立典禮暨第一屆會員大會，東吳大學發展處處長楊楨教授代表校長劉源俊來港主持授旗儀式。

東吳大學香港校友會設有「東吳大學香港校友會獎學金」，獎勵學業及操行良好之東吳大學在學香港學生，每年又與東吳大學合作，在香港舉辦「新生入學說明會」，輔導獲分發入讀東吳大學的香港學生。

東吳大學香港校友會同時為「台灣各大學香港校友會總會」（前稱「臺灣大專香港校友會總會」）和「全國基督教大學（香港）有限公司」成員。

　　

[\*](../Category/東吳大學校友.md "wikilink")
[Category:臺灣大專院校校友列表](../Category/臺灣大專院校校友列表.md "wikilink")
[Category:中国高校校友列表](../Category/中国高校校友列表.md "wikilink")