[Polk_County_Minnesota_Incorporated_and_Unincorporated_areas_East_Grand_Forks_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Polk_County_Minnesota_Incorporated_and_Unincorporated_areas_East_Grand_Forks_Highlighted.svg "fig:Polk_County_Minnesota_Incorporated_and_Unincorporated_areas_East_Grand_Forks_Highlighted.svg")
**東大福克斯**（East Grand Forks,
Minnesota）是[美國](../Page/美國.md "wikilink")[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")[波爾克縣的一座城市](../Page/波爾克縣_\(明尼蘇達州\).md "wikilink")，位於該州西部[北紅河和](../Page/北紅河.md "wikilink")[紅湖河匯流處東部](../Page/紅湖河.md "wikilink")，城市被後者劃為南北兩部分。2000年人口7,501人。\[1\]與河對岸的[大福克斯構成的](../Page/大福克斯_\(北達科他州\).md "wikilink")[都會區](../Page/大大福克斯.md "wikilink")（Grand
Forks, ND-MN Metropolitan Statistical Area，又稱Greater Grand Forks或The
Grand Cities）2006年人口96,523人。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[E](../Category/明尼蘇達州城市.md "wikilink")

1.  [East Grand Forks city, Minnesota- Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFFacts?_event=Search&geo_id=&_geoContext=&_street=&_county=East+Grand+Forks&_cityTown=East+Grand+Forks&_state=04000US27&_zip=&_lang=en&_sse=on&pctxt=fph&pgsl=010&show_2003_tab=&redirect=Y)
2.