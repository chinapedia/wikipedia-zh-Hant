**東山服務區**位於[台南市](../Page/台南市.md "wikilink")[東山區](../Page/東山區_\(臺南市\).md "wikilink")，是[福爾摩沙高速公路自](../Page/福爾摩沙高速公路.md "wikilink")[基金交流道](../Page/基金交流道.md "wikilink")（起點）以來的第六個服務區，里程為319.9k，南北共站，高速公路局號稱其為東南亞最大的服務區，2001年10月15日成立，出口處設有24小時台亞加油站。此服務區設有駕駛人休息室，讓駕駛人可短暫睡眠，以避免疲勞駕駛釀成事故。\[1\]

目前經營廠商為南仁湖育樂股份有限公司。2009年11月營業收入達到兩千兩百萬，僅次於[清水服務區排名第二](../Page/清水服務區.md "wikilink")，在台灣的高速公路界有著「南東山」的美譽，與[關西服務區](../Page/關西服務區.md "wikilink")、[清水服務區並列為](../Page/清水服務區.md "wikilink")「北關西中清水南東山」。

東山區民眾可藉[縣道165號接區道](../Page/縣道165號.md "wikilink")[南103線再經由東山服務區便道進出](../Page/臺南市區道列表.md "wikilink")[福爾摩沙高速公路](../Page/福爾摩沙高速公路.md "wikilink")\[2\]。

## 規劃

## 基本資料

  - 地址：[台南市](../Page/台南市.md "wikilink")[東山區科里里枋子林](../Page/東山區_\(臺南市\).md "wikilink")74-6號
  - 里程：[福爾摩沙高速公路](../Page/福爾摩沙高速公路.md "wikilink")320公里處。

## 位置

## 外部連結

  - [南仁湖企業東山服務區](http://www.nanrenhu.com.tw/hiwayshopping/index.php?controller=dongshan)

## 參考來源

[D](../Category/2001年完工交通基礎設施.md "wikilink")
[D](../Category/2001年台灣建立.md "wikilink")
[D](../Category/台灣高速公路服務區.md "wikilink")
[D](../Category/國道三號_\(中華民國\).md "wikilink")
[D](../Category/東山區_\(臺南市\).md "wikilink")
[D](../Category/台南市旅遊景點.md "wikilink")
[D](../Category/台南市旅遊人文景點.md "wikilink")
[國](../Category/台南市道路.md "wikilink")

1.  [國道駕駛人休息室(含簡易休憩區)服務資訊](http://www.freeway.gov.tw/Publish.aspx?cnid=2293&p=6656)
2.  [東山服務區便道](https://goo.gl/maps/4Gjwhk6E6yx)