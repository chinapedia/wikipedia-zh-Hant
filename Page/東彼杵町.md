**東彼杵町**（）是位于[日本](../Page/日本.md "wikilink")[長崎縣中部的](../Page/長崎縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[東彼杵郡](../Page/東彼杵郡.md "wikilink")。

過去曾經盛行[捕鯨業](../Page/捕鯨業.md "wikilink")，現在的主要產業為漁業與農業。

## 地理

### 隣接市町村

  - 東彼杵郡[川棚町](../Page/川棚町.md "wikilink")
  - [大村市](../Page/大村市.md "wikilink")
  - [佐賀縣](../Page/佐賀縣.md "wikilink")[嬉野市](../Page/嬉野市.md "wikilink")

## 历史

過去為[長崎街道的宿場](../Page/長崎街道.md "wikilink")。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：彼杵村與千綿村。
  - 1940年11月3日：彼杵村改制為彼杵町。
  - 1959年5月1日：彼杵町與千綿村[合併為](../Page/市町村合併.md "wikilink")**東彼杵町**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>彼杵村</p></td>
<td><p>1940年11月3日<br />
彼杵町</p></td>
<td><p>1969年5月1日<br />
東彼杵町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>千綿村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [大村線](../Page/大村線.md "wikilink")：[彼杵車站](../Page/彼杵車站.md "wikilink")
        - [千綿車站](../Page/千綿車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [長崎自動車道](../Page/長崎自動車道.md "wikilink")：[東彼杵交流道](../Page/東彼杵交流道.md "wikilink")
    - [大村灣休息區](../Page/大村灣休息區.md "wikilink")

## 觀光景點

  - [日本二十六聖人乘船場遺跡](../Page/日本二十六聖人.md "wikilink")

## 本地出身之名人

  - [大平成一](../Page/大平成一.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [仲里依紗](../Page/仲里依紗.md "wikilink")：[女演員](../Page/女演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")

## 參考資料

## 外部連結

  - [東彼杵町商工會](http://www.shokokai-nagasaki.or.jp/h-sonogi/)