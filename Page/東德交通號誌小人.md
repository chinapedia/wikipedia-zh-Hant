[Ampelmann.svg](https://zh.wikipedia.org/wiki/File:Ampelmann.svg "fig:Ampelmann.svg")
[Ampelmaenner.jpg](https://zh.wikipedia.org/wiki/File:Ampelmaenner.jpg "fig:Ampelmaenner.jpg")

**東德交通號誌小人**（）是前[東德和現在](../Page/東德.md "wikilink")[德國東部行人過路](../Page/德國.md "wikilink")[交通燈上的圖案](../Page/交通燈.md "wikilink")。紅色、立正的小人代表「停止」，绿色、大步走的小人代表「可前行」。

Ampelmännchen的形狀，概念上和其他地區使用的沒分別。起源于东德的Ampelmännchen是男性，戴着一頂扁帽。在東德，Ampelmännchen成為了教育司機道路知識的電視節目內的角色。

[德国統一後](../Page/兩德統一.md "wikilink")，曾有人提議統一東西德的交通燈圖案，但在反對聲之下，這個小人還是保留下來。Ampelmännchen成為[東德情結](../Page/東德情結.md "wikilink")（Ostalgie）的象徵物之一。

受此影響，在某些西德地區如[薩爾布呂肯](../Page/薩爾布呂肯.md "wikilink")，豎立起本來不屬於它的Ampelmännchen交通燈。2004年，[茨維考市內出現了女性小人Ampelmädchen](../Page/茨維考.md "wikilink")。現在[德累斯頓亦有其芳蹤](../Page/德累斯頓.md "wikilink")。

## 歷史

Ampelmännchen是道路心理學家[卡爾·佩格勞在](../Page/卡爾·佩格勞.md "wikilink")1961年設計的。他相信友善的角色比起無意義的顏色，更易令道路使用者接收其訊息。儘管佩格勞曾擔心，那頂小帽會被指有[小資產階級色彩](../Page/小資產階級.md "wikilink")，而不獲[東德政府通過](../Page/東德政府.md "wikilink")。但相关负责人表示，这种担心毫无必要。

## 相册

<File:Traffic> light - female (aka).jpg|交通號誌小人的女性版（Ampelmädchen）
<File:Ampelmaennchen> Ost Fahrrad.jpg|自行车灯上的交通號誌小人 <File:Ampelmännchen>
mit Regenschirm.JPG|打伞的交通號誌小人
[File:Ampelmaennchen_Ost_Warnlicht.jpg|警告灯上的交通號誌小人](File:Ampelmaennchen_Ost_Warnlicht.jpg%7C警告灯上的交通號誌小人)
[File:Geschäft_im_Hackeschen_Hof.JPG|纪念品商店](File:Geschäft_im_Hackeschen_Hof.JPG%7C纪念品商店)
<File:East> Berlin traffic lights3.jpg|遊客紀念品 |柏林纪念品商店附近的雕塑

## 參見

  - [行人倒數計時顯示器](../Page/行人倒數計時顯示器.md "wikilink")

[Category:德國交通](../Category/德國交通.md "wikilink")
[Category:東德文化](../Category/東德文化.md "wikilink")
[Category:东德交通](../Category/东德交通.md "wikilink")