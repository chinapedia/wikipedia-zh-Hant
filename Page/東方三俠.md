是1993年[杜琪峰所導演的香港動作科幻電影](../Page/杜琪峰.md "wikilink")，[程小東擔任動作導演](../Page/程小東.md "wikilink")，[梅豔芳](../Page/梅豔芳.md "wikilink")、[楊紫瓊與](../Page/楊紫瓊.md "wikilink")[張曼玉三大女星破天荒合作演出](../Page/張曼玉.md "wikilink")，[劉松仁與](../Page/劉松仁.md "wikilink")[黃秋生等人亦參演](../Page/黃秋生.md "wikilink")。梅艷芳主唱的《女人心》是當年香港電影金像獎的最佳電影歌曲。

## 劇情介紹

東東（[梅豔芳](../Page/梅豔芳.md "wikilink")
飾）與青青/陳三/心靖（[楊紫瓊](../Page/楊紫瓊.md "wikilink")
飾）原本由一位武林高人所收養，教她們習武，希望二人能成為一對行俠仗義的女俠。可是，在青青一次練功時因無力攀上山頂而掉下，從此杳無音訊。東東則成為女飛俠，平常是警察劉啟文（[劉松仁](../Page/劉松仁.md "wikilink")
飾）的妻子。

故事始於連警察也束手無策的「盜嬰」事件。陳三披著博士發明的隱身衣，向警方下戰書，並在盜嬰過程中，為女飛俠所傷。女捕頭陳七（[張曼玉](../Page/張曼玉.md "wikilink")
飾），為了幫局長取回嬰兒潛入魔宮，但被公公重傷，也遇見昔日好友陳三，後由女飛俠所救。公公（[任世官](../Page/任世官.md "wikilink")
飾）盜嬰是為令中國有皇帝，並且要陳三取得博士的隱身衣，但陳三卻以隱身衣不能在日光下使用來拖延。公公派陳九挾持車站人質來迫陳七與女飛俠現身，又要陳三殺死博士。陳三找陳七求死，由女飛俠所救，二人相認後前往營救博士，可惜博士早知自己命不久矣。最後三人一齊對付公公，經過一番激戰後將公公消滅，並救回所有被盜走的嬰兒。

## 人物角色

|                                         |            |                                  |                                                          |
| --------------------------------------- | ---------- | -------------------------------- | -------------------------------------------------------- |
| **演員**                                  | **角色**     | **粵語配音**                         | **關係**                                                   |
| [梅豔芳](../Page/梅豔芳.md "wikilink")        | 東東         | [龍寶鈿](../Page/龍寶鈿.md "wikilink") | 女飛俠，劉啟文之妻。                                               |
| [張曼玉](../Page/張曼玉.md "wikilink")        | 陳七         | [沈小蘭](../Page/沈小蘭.md "wikilink") | 賞金獵人（女捕頭）；原屬陳公公部下，被陳三放過一命脫離魔掌。花了十年時間，終於把自己當成人類，成為女捕頭。    |
| [楊紫瓊](../Page/楊紫瓊.md "wikilink")        | 心靖、陳三或青青   | [盧素娟](../Page/盧素娟.md "wikilink") | 陳公公部下，喜歡博士，受陳公公所束縛。                                      |
| [劉松仁](../Page/劉松仁.md "wikilink")        | 劉啟文（劉 Sir） | [梁政平](../Page/梁政平.md "wikilink") | 警官，東東之夫                                                  |
| [白千石](../Page/白千石.md "wikilink")        | 李之博士       | [黃志成](../Page/黃志成.md "wikilink") | 擁有隱形衣，與心靖互有情愫                                            |
| [秦沛](../Page/秦沛.md "wikilink")          | 局長         | [黃子敬](../Page/黃子敬.md "wikilink") | 警察局長                                                     |
| [任世官](../Page/任世官.md "wikilink")        | 陳公公        | [袁淑珍](../Page/袁淑珍.md "wikilink") | 公公，認為“中國要有皇帝”而派人去活捉男嬰，有特異功能。                             |
| [黃秋生](../Page/黃秋生.md "wikilink")        | 陳九         |                                  | 公公的傀儡                                                    |
| [朱咪咪](../Page/朱咪咪.md "wikilink")        | 神婆         | [徐愛貞](../Page/徐愛貞.md "wikilink") | 洞悉被擄嬰孩皆有皇帝命格                                             |
| [姜皓文](../Page/姜皓文.md "wikilink")        | 瘋子         | [陸頌愚](../Page/陸頌愚.md "wikilink") | 醫院的人醫死他初出生的兒子，導致精神分裂。登報紙向警方挑戰，誓要將醫院內的所有嬰兒殺光陪葬，最後被劉sir制服。 |
| [徐濤](../Page/徐濤.md "wikilink")          |            |                                  |                                                          |
| [阮兆祥](../Page/阮兆祥.md "wikilink")        | 地產經紀       | [阮兆祥](../Page/阮兆祥.md "wikilink") |                                                          |
| [黃一飛](../Page/黃一飛.md "wikilink")        | 偷車賊        | [陸頌愚](../Page/陸頌愚.md "wikilink") |                                                          |
| [陳卓欣](../Page/陳卓欣.md "wikilink")        | 東東 (童年)    |                                  |                                                          |
| [李兆基](../Page/李兆基_\(演員\).md "wikilink") | 銀行劫匪       | [陳偉權](../Page/陳偉權.md "wikilink") |                                                          |
| [鄭藩生](../Page/鄭藩生.md "wikilink")        | 銀行劫匪       |                                  |                                                          |

## 電影歌曲

| 曲別    | 歌曲         | 作詞                             | 作曲                               | 編曲                               | 演唱                               | 專輯            |
| ----- | ---------- | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- | ------------- |
| 國語主題曲 | **《春之祭》**  | [林夕](../Page/林夕.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [金智娟](../Page/金智娟.md "wikilink") | 《四季》1992-11   |
| 粵語主題曲 | **《莫問一生》** | [林夕](../Page/林夕.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [梅艷芳](../Page/梅艷芳.md "wikilink") | 《戲劇人生》1993-03 |
| 粵語插曲  | **《女人心》**  | [林夕](../Page/林夕.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [梅艷芳](../Page/梅艷芳.md "wikilink") | 《戲劇人生》1993-03 |
| 國語插曲  | **《你的女人》** | [林夕](../Page/林夕.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [羅大佑](../Page/羅大佑.md "wikilink") | [何超儀](../Page/何超儀.md "wikilink") | 《造反》          |

## 獲獎提名

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1994年香港電影金像獎.md" title="wikilink">1994年香港電影金像獎</a></p></td>
<td><p>最佳電影歌曲</p></td>
<td><p>《女人心》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳動作指導</p></td>
<td><p><a href="../Page/程小東.md" title="wikilink">程小東</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳服裝造型設計</p></td>
<td><p><a href="../Page/余家安.md" title="wikilink">余家安</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳電影配樂</p></td>
<td><p><a href="../Page/胡偉立.md" title="wikilink">胡偉立</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳電影歌曲</p></td>
<td><p>《莫問一生》</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 续集

  - [现代豪侠传](../Page/现代豪侠传.md "wikilink")

## 相關

  - [素心蘭](../Page/素心蘭.md "wikilink")

## 外部連結

  - {{@movies|fJatm0865028}}

  -
  -
  -
  -
  -
  -
[Category:香港科幻動作片](../Category/香港科幻動作片.md "wikilink")
[3](../Category/1990年代香港電影作品.md "wikilink")
[Category:杜琪峰電影](../Category/杜琪峰電影.md "wikilink")
[Category:香港電影金像獎最佳原創電影歌曲獲獎電影](../Category/香港電影金像獎最佳原創電影歌曲獲獎電影.md "wikilink")
[Category:胡伟立配乐电影](../Category/胡伟立配乐电影.md "wikilink")