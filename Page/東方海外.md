**東方海外（國際）有限公司**（OOIL，）是一家在[香港交易所上市的綜合企業公司](../Page/香港交易所.md "wikilink")，由已故船王[董浩雲創辦](../Page/董浩雲.md "wikilink")，[董浩雲家族一直控制公司至](../Page/董浩雲家族.md "wikilink")2018年，之後由[中遠海運收購成為旗下公司](../Page/中遠海運.md "wikilink")。

## 主要業務

東方海外主要業務是國際運輸及[物流](../Page/物流.md "wikilink")、經營[貨櫃碼頭](../Page/貨櫃碼頭.md "wikilink")、投資物業及中國業務及[證券投資](../Page/證券.md "wikilink")。公司在[百慕達註冊](../Page/百慕達.md "wikilink")。2006年[資產淨值為](../Page/資產淨值.md "wikilink")2,727,206,000[美元](../Page/美元.md "wikilink")，[純利為](../Page/純利.md "wikilink")580,603,000美元。

## 歷史

東方海外在1969年成立\[1\]。1981年[董浩雲逝世](../Page/董浩雲.md "wikilink")，東方海外交予[董建華管理](../Page/董建華.md "wikilink")。1985年，東方海外瀕臨破產。1986年，[霍英東注資](../Page/霍英東.md "wikilink")，使東方海外渡過難關。

2005年8月，東方海外旗下的**東方海外房產集團**一個位於[上海](../Page/上海.md "wikilink")[徐家匯的豪華屋苑](../Page/徐家匯.md "wikilink")**東方曼哈頓**在[颱風馬莎吹襲期間](../Page/颱風馬莎.md "wikilink")，地庫停車場嚴重水浸，導致停車場內約180多輛名貴房車受水浸損毀，合共損失一共高達五仟萬元人民幣。業主指稱停車場的排水系統有問題，引致水浸，要求發展商賠償損失，但發展商經獨立技術檢查，顯示一切符合要求，水浸事故與發展商的設計及施工無關。期間該批業主一度考慮到董建華住所附近抗議，最後不了了之\[2\]。

2006年11月21日，東方海外與[安大略教師退休金計劃達成協議](../Page/安大略.md "wikilink")，以代價約二十四億美元出售碼頭部門。該部門持有四個碼頭，包括於[溫哥華港的](../Page/溫哥華港.md "wikilink")**Deltaport**與**Vanterm**及位於[紐約港及](../Page/紐約港.md "wikilink")[新澤西港的](../Page/新澤西港.md "wikilink")**New
York Container Terminal**及**Global Container Terminal**非貨櫃運輸業務之組成部分。

2010年1月18日，東方海外與[新加坡](../Page/新加坡.md "wikilink")[嘉德置地](../Page/嘉德置地.md "wikilink")（CapitaLand）達成協議，以代價約二十四億美元出售「[東方海外發展（中國）](../Page/東方海外發展.md "wikilink")」現時在內地七個房地產項目，涉及樓面面積約一百四十五萬平方米，七個項目分別位於上海、昆山和天津項目；其中半數為住宅物業，其餘為寫字樓、商鋪與酒店。東方海外發展賬面淨值約11.15億美元，或86.96億港元。故此，東方海外預計，出售東方海外發展可以帶來10.55億美元，或82.3億港元收益。\[3\]

2012年4月4日東方海外簽訂新租約，租用[長堤港Middle](../Page/長堤港.md "wikilink")
Harbor，為期40年租賃合約，價值46億美元，這將是美國歷來最大的一次海港交易。

2016年4月21日[長榮海運](../Page/長榮海運.md "wikilink")、[法國達飛海運集團](../Page/法國達飛海運集團.md "wikilink")、[中國遠洋及香港東方海外等四家海運業者組成](../Page/中國遠洋.md "wikilink")「[大洋聯盟](../Page/大洋聯盟.md "wikilink")」（OCEAN
Alliance），各成員共投入350艘新型貨櫃輪，佔全球運力27%，2017年4月起運作。

2017年7月，[中遠海運控股與](../Page/中遠海控.md "wikilink")[上港集團向東方海外股東提出附先決條件的](../Page/上港集團.md "wikilink")[收購建議](../Page/收購.md "wikilink")，作價為每股78.67港元現金，较7月7日收盘价溢价31%，如建議獲全數股份接納，總代價將為約492億港元。收購方表示有意在收購成功後保留東方海外在港交所的上市地位。\[4\]董建華家族現持有東方海外約68.7%股權，其中中遠海控將收購58.8%，而上港集團收購餘下9.9%。董家已承諾賣股，涉及338億港元。

2018年7月，收購完成。

## 旗下公司

  - [东方海外货柜航运公司](../Page/东方海外货柜航运公司.md "wikilink")

## 參考

## 外部連結

  - [東方海外（國際）有限公司官方網站](http://www.ooilgroup.com)

[Category:董浩云](../Category/董浩云.md "wikilink")
[Category:中國物流公司](../Category/中國物流公司.md "wikilink")
[Category:中国航运公司](../Category/中国航运公司.md "wikilink")
[Category:香港物流公司](../Category/香港物流公司.md "wikilink")
[Category:香港船公司](../Category/香港船公司.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:前恒生指數成份股](../Category/前恒生指數成份股.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:董浩雲家族](../Category/董浩雲家族.md "wikilink")
[Category:1969年成立的公司](../Category/1969年成立的公司.md "wikilink")
[Category:中國遠洋海運集團](../Category/中國遠洋海運集團.md "wikilink")
[Category:1969年香港建立](../Category/1969年香港建立.md "wikilink")
[Category:中華人民共和國國有企業](../Category/中華人民共和國國有企業.md "wikilink")

1.  [东方海外国际有限公司](http://shop.jctrans.com/15FD1E0F-10B2-41D9-BF31-C24C4CEF656E.html)
2.  [東方曼哈頓停車場水浸相關報導](http://the-sun.on.cc/channels/news/20060116/20060116032028_0000.html)2006年11月26日，[太陽報](../Page/太陽報_\(香港\).md "wikilink")
3.  [東方海外賣物業套172億淡出內房](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100119&sec_id=15307&subsec=15320&art_id=13640555)
    蘋果日報，2010年1月19日
4.  [聯合公告 - (1) 瑞銀代表聯席要約人提出附先決條件的自願性全面現金要約以收購東方海外國際的所有已發行股份 (2)
    控股股東接納要約的不可撤回承諾及(3)
    中遠海運控股的非常重大收購事項](http://www.hkexnews.hk/listedco/listconews/sehk/2017/0709/LTN20170709008_C.pdf)，2017-07-09。