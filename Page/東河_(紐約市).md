[New_York_City_-_Manhattan_-_East_River.PNG](https://zh.wikipedia.org/wiki/File:New_York_City_-_Manhattan_-_East_River.PNG "fig:New_York_City_-_Manhattan_-_East_River.PNG")

**东河**（），[美国](../Page/美国.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[纽约市市内的一条潮汐型](../Page/纽约市.md "wikilink")[海峡](../Page/海峡.md "wikilink")，北接[长岛海湾](../Page/长岛海湾.md "wikilink")，南接[上纽约湾](../Page/上纽约湾.md "wikilink")，將位于[长岛](../Page/长岛_\(紐約\).md "wikilink")（[布鲁克林和](../Page/布鲁克林.md "wikilink")[皇后区](../Page/皇后区.md "wikilink")）与[曼哈顿岛以及位于](../Page/曼哈顿岛.md "wikilink")[北美洲大陆的](../Page/北美洲.md "wikilink")[布朗克斯分開](../Page/布朗克斯.md "wikilink")。過去有段時間曾因為東河與長島海灣的相對位置而被稱為南河（South
River）。

## 歷史

[Brooklyn_Manhattan_Williamsburg_Bridges.jpg](https://zh.wikipedia.org/wiki/File:Brooklyn_Manhattan_Williamsburg_Bridges.jpg "fig:Brooklyn_Manhattan_Williamsburg_Bridges.jpg")、[曼哈頓大橋](../Page/曼哈頓大橋.md "wikilink")、[威廉斯堡大橋](../Page/威廉斯堡大橋.md "wikilink")。\]\]
東河大約於一萬一千年前[冰河時期末期時形成](../Page/冰河時期.md "wikilink")\[1\]。東河的地形受到冰河明顯的影響，可以以分為上半段與下半端。上半段為從長島海灣到地獄門之間的河道，這段河流方向與冰河行進方向垂直，河道較寬並且非常曲折，河岸邊有著許多細長且深的河灣，這是因為河道受到了冰河的挖蝕。下半段為從地獄門到紐約灣之間的南北向河道，這段河道的河流方向與冰河行進方向平行，而河道較窄直，河岸邊的河灣也較平直且寬深，但現今已被填平。

## 參考文獻

## 外部链接

  - [East River page from the Greater Astoria Historical
    Society](http://www.eastrivernyc.org)
  - [LIC Community Boathouse site for free paddling on the East
    River](http://www.licboathouse.org)
  - [Western Queens waterfront information
    page](https://web.archive.org/web/20051220083611/http://astoriawaterfront.org/)

[E](../Category/紐約市地理.md "wikilink")
[Category:沿海水道](../Category/沿海水道.md "wikilink")

1.  [The Queens Gazette: The East River Flows From Prehistoric Times To
    Today](http://www.qgazette.com/news/2005/0720/features/049.html)