[Wpdms_east_west_new_jersey.png](https://zh.wikipedia.org/wiki/File:Wpdms_east_west_new_jersey.png "fig:Wpdms_east_west_new_jersey.png")

**東澤西省**（****）與**西澤西省**（****）為兩個分別管理但皆隸屬於[澤西省之下的兩個省](../Page/紐澤西州.md "wikilink")。兩省之總和比今日之[紐澤西州略為大一點](../Page/紐澤西州.md "wikilink")。於1674年至1702年之間，這種兩省分治的方式延續了28年之久。其當時之省政府坐落於[Perth
Amboy](../Page/:en:Perth_Amboy,_New_Jersey.md "wikilink")。

西澤西與東澤西之間界線的劃分在以前常是爭執的焦點，不過今天這兩省的分界線大致上與[南澤西和](../Page/南澤西.md "wikilink")[北澤西的界線相等](../Page/北澤西.md "wikilink")。

在與有單一目標地想建立一個貴格會殖民地的[西澤西殖民者相比](../Page/西澤西.md "wikilink")，東澤西的文化與傳統的組成更較為複雜。除了由英國殖民者在1664年的來到之後所帶來的英國文化之外，在那之前，東澤西亦被[荷蘭人所佔領過](../Page/荷蘭.md "wikilink")，但是由於當地印地安人的抵抗，荷蘭人不久後便放棄了位於[哈德遜河以西的東澤西地區](../Page/哈德遜河.md "wikilink")。

在歐洲白人殖民者的到來之前，東澤西的德拉瓦河谷主要是雷納佩族（Lenape）的居住地，直到[荷蘭人](../Page/荷蘭.md "wikilink")、[瑞典人](../Page/瑞典.md "wikilink")、以及[英國人於](../Page/英國.md "wikilink")1609年來到此區域為止，而相較之下，西澤西要一直等到1664年英國人征服荷蘭人的新阿姆斯特丹才會有歐洲殖民者的蹤影。

荷蘭人曾經在[特拉華河流域建立過一兩個居住點](../Page/特拉華河.md "wikilink")，但是在1620年左右之前，大部分的荷蘭人殖民者都已搬到[新尼德蘭殖民地的中心地點](../Page/新尼德蘭.md "wikilink")——[曼哈頓](../Page/曼哈頓.md "wikilink")。

在德拉瓦河下游的[新瑞典殖民地在](../Page/新瑞典.md "wikilink")1638年開始發展。瑞典殖民者在今日的[威明頓建立了一個堡壘](../Page/威明頓_\(德拉瓦州\).md "wikilink")，這個堡壘位於以瑞典皇后克利斯汀娜皇后命名的克利斯汀娜河（[Christina
River](../Page/:en:Christina_River.md "wikilink")）河口（這是現今德拉瓦河的一條小分支）。大部分的瑞典殖民地人口都位於德拉瓦河的西邊，但在新荷蘭的納蘇保壘（Fort
Nassau）重建完成後，因為受到荷蘭人的威脅，瑞典人在現今的紐澤西州沙崙市建立了[Fort Nya
Elfsborg以防備荷蘭人的入侵](../Page/:en:Fort_Nya_Elfsborg.md "wikilink")；但是新瑞典終究還是再1655年時被荷蘭人所佔領。

在1670年後時期時，許多[貴格會的成員遷移到](../Page/貴格會.md "wikilink")[沙崙](../Page/沙崙_\(紐澤西州\).md "wikilink")，後來移至不久後成為西澤西省府所在地的[伯靈頓](../Page/伯靈頓_\(紐澤西州\).md "wikilink")。

## 其他資訊

  - [紐澤西州長列表](../Page/紐澤西州長列表.md "wikilink")
  - [East Jersey](../Page/:en:East_Jersey.md "wikilink")
  - [West Jersey](../Page/:en:West_Jersey.md "wikilink")
  - [Province of Jersey](../Page/:en:Province_of_Jersey.md "wikilink")

## 外部連結

  - [West Jersey and South Jersey
    Heritage](https://archive.is/20040109074738/http://207.36.134.158/)
  - [West Jersey History Project](http://westjerseyhistory.org)
  - [Maps](https://web.archive.org/web/20051015053015/http://www.pym.org/exhibit/p06.html)
  - [1677 Charter](http://www.lonang.com/exlibris/organic/1677-cnj.htm)
  - [1681
    Regulations](https://web.archive.org/web/20070102120721/http://www.yale.edu/lawweb/avalon/states/nj08.htm)

[category:新澤西州歷史](../Page/category:新澤西州歷史.md "wikilink")

[Category:歷史上的美國領地及地區](../Category/歷史上的美國領地及地區.md "wikilink")
[Category:十三殖民地历史](../Category/十三殖民地历史.md "wikilink")
[Category:英属殖民美国](../Category/英属殖民美国.md "wikilink")
[Category:新英格兰自治领](../Category/新英格兰自治领.md "wikilink")
[Category:英格兰前殖民地](../Category/英格兰前殖民地.md "wikilink")