<table>
<caption><strong>Eastern Kentucky University</strong></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><em><a href="../Page/校训.md" title="wikilink">校训</a>：Where Students and Learning Come First</em><br />
學生為本，以學為主</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>学校类型</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>宗教关系</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>建立时间</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>所在地</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>学生数量</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>主页</p></td>
</tr>
</tbody>
</table>

**东肯塔基大学**（Eastern Kentucky
University），是位于[美国](../Page/美国.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[里士满的一所公立大学](../Page/里士满_\(肯塔基州\).md "wikilink")。

## 历史

## 知名校友

  - [他信·钦那瓦](../Page/他信·钦那瓦.md "wikilink")：前泰国首相

[CATEGORY:肯塔基州大学](../Page/CATEGORY:肯塔基州大学.md "wikilink")

[Category:1874年建立](../Category/1874年建立.md "wikilink")