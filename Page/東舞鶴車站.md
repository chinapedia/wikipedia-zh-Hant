**東舞鶴車站**（）是一位於[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[舞鶴市](../Page/舞鶴市.md "wikilink")、隸屬於[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）的[鐵路車站](../Page/鐵路車站.md "wikilink")。除了是舞鶴市的主車站之外，由於緊鄰在昔日曾是重要軍港所在地的邊，在過去該站的貨物運輸業務也不比客運業務來得遜色。東舞鶴是日本在[日本海沿岸唯一的軍事據點](../Page/日本海.md "wikilink")，因此以1901年設置的[舞鶴鎮守府為中心](../Page/舞鶴鎮守府.md "wikilink")，很快地就發展成一個軍事都市。在[第二次世界大戰前與戰爭期間](../Page/第二次世界大戰.md "wikilink")，[日本皇室曾多次搭乘鐵路列車拜訪東舞鶴](../Page/日本皇室.md "wikilink")，因此東舞鶴車站內有少見的設計。然而1996年時伴隨當時的路線高架化工程，東舞鶴車站也一同改建成為今日的[高架車站型式](../Page/高架車站.md "wikilink")。

至於原本等支線撤除之後留下的停車側線，則在車站改建的同時一併拆除，並打算用來作為高層[住宅大樓的用地](../Page/住宅大樓.md "wikilink")。

## 車站構造

[thumb](../Page/file:JR_West_Higashi-Maizuru_Station_Platform_2.jpg.md "wikilink")
東舞鶴車站為高架車站設計，站內配有[島式月台一座與兩條乘車線](../Page/島式月台.md "wikilink")。東舞鶴車站同時是舞鶴線與小濱線兩條鐵路線的終點站，在這裡兩條路線的軌道雖然是互通的，但實際上並沒有任何從舞鶴線直行進入小濱線的列車。由於這緣故，同一條乘車線的兩端會同時有不同路線的列車停靠，其中小濱線那頭的列車在抵達東舞鶴後會回頭朝敦賀方向離去，而來自舞鶴線[綾部與西舞鶴方向的列車](../Page/綾部車站.md "wikilink")，抵站後則是回頭朝原本的方向離去。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1、2</p></td>
<td><p><strong></strong> 舞鶴線</p></td>
<td><p>往<a href="../Page/西舞鶴站.md" title="wikilink">西舞鶴</a>、<a href="../Page/綾部站.md" title="wikilink">綾部</a>、<a href="../Page/福知山站.md" title="wikilink">福知山</a>、<a href="../Page/京都站.md" title="wikilink">京都方向</a></p></td>
</tr>
<tr class="even">
<td><p>小濱線</p></td>
<td><p>往、<a href="../Page/敦賀站.md" title="wikilink">敦賀方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 歷史

[Higashi-Maizuru_Station_19880728_b.jpg](https://zh.wikipedia.org/wiki/File:Higashi-Maizuru_Station_19880728_b.jpg "fig:Higashi-Maizuru_Station_19880728_b.jpg")

  - 1904年（[明治](../Page/明治.md "wikilink")37年）11月3日－以[日本國有鐵道的](../Page/日本國有鐵道.md "wikilink")**新舞鶴車站**身份開始營運。並即刻借予作為客運與貨運車站使用。
  - 1907年（明治40年）8月1日－阪鶴鐵道收歸國有。
  - 1919年（[大正](../Page/大正.md "wikilink")8年）7月21日－通往的支線通車。
  - 1939年（[昭和](../Page/昭和.md "wikilink")14年）6月1日－改名為**東舞鶴車站**。
  - 1972年（昭和47年）11月1日－中舞鶴線廢線。
  - 1980年（昭和55年）9月30日－貨運服務停止。
  - 1987年（昭和62年）4月1日－[日本國鐵分割民營化](../Page/日本國鐵分割民營化.md "wikilink")，東舞鶴車站的所有權與營運權由JR西日本繼承。
  - 1996年（平成8年）7月13日－改建為高架車站。

## 其他

  - 東舞鶴車站曾在2003年時，獲選列入第4回[近畿車站百選](../Page/近畿車站百選.md "wikilink")（），被認為是最能代表近畿地方的百座車站之一\[1\]。

## 相鄰車站

  - 西日本旅客鐵道
    **** 舞鶴線
      - 特急「[舞鶴](../Page/舞鶴號列車.md "wikilink")」到發站
    <!-- end list -->
      -

        快速、普通

          -
            [西舞鶴](../Page/西舞鶴站.md "wikilink")－**東舞鶴**
    小濱線
      -

          -
            **東舞鶴**－

### 曾經存在的路線

  - 日本國有鐵道
    舞鶴線中舞鶴支線（中舞鶴線）
      -
        **東舞鶴**－

## 參考文獻與註釋

  - 《JR時刻表，2007年4月號》，交通新聞社（日本，東京）

<references/>

## 外部連結

  - [東舞鶴駅（JR西日本）](http://www.jr-odekake.net/eki/top.php?id=0631605)

[GashiMaizuru](../Category/日本鐵路車站_Hi.md "wikilink")
[Category:京都府鐵路車站](../Category/京都府鐵路車站.md "wikilink")
[Category:小濱線車站](../Category/小濱線車站.md "wikilink")
[Category:1904年启用的铁路车站](../Category/1904年启用的铁路车站.md "wikilink")
[Category:舞鶴市](../Category/舞鶴市.md "wikilink")
[Category:舞鶴線車站](../Category/舞鶴線車站.md "wikilink")
[Category:中舞鶴線車站](../Category/中舞鶴線車站.md "wikilink")

1.  [近畿車站百選（大阪日日新聞）](http://www.nnn.co.jp/dainichi/rensai/machinoeki/index.html)