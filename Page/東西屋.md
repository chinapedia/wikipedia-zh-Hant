[Chindonya_Okubo_Tokyo.jpg](https://zh.wikipedia.org/wiki/File:Chindonya_Okubo_Tokyo.jpg "fig:Chindonya_Okubo_Tokyo.jpg")
**東西屋**（**Chindon'ya**、チンドン屋），又稱為 **Japanese marching
band**，在[日本舊時代](../Page/日本.md "wikilink")，又稱**tōzaiya**（東西屋）**hiromeya**（**広目屋**或**披露目屋**）是日本一種經過精心刻意打扮的街頭音樂家，受委託為所在地區的商品或商店打廣告。表演者所標榜的開放新的商店及其他場地，或晉升的特別活動，如價格折扣等。如今，這種活動在日本已屬少見。

## 外部連結

  - [The sunny side of the
    street](http://www.shukosha.com/radar/metro/Feature/index.html)
  - [Interview with a
    *Chindonya*](https://web.archive.org/web/20070927185142/http://japundit.com/archives/2006/05/22/2562/)
  - [*Chindonya* group *u-stage*](http://www.u-stage.com/indexE.html)
  - [*Chindonya* group *Yamatoya* (in
    Japanese)](https://web.archive.org/web/20070227222511/http://kimama.moo.jp/yamatoya)
  - [New York City *Chindonya* group
    *HappyFunSmile*](http://www.happyfunsmile.com)
  - [*Okinawanpop & Chindon* group *Ryukyu Chimdon Gakudan* (in Okinawa
    Japan)](http://www.ryuchim.jp)

[Category:日本音樂](../Category/日本音樂.md "wikilink")
[Category:廣告](../Category/廣告.md "wikilink")
[Category:街头艺术](../Category/街头艺术.md "wikilink")