**東諸縣郡**（）是[日本](../Page/日本.md "wikilink")[宮崎縣轄下的一個](../Page/宮崎縣.md "wikilink")[郡](../Page/郡.md "wikilink")。

現轄有以下2町：

  - [綾町](../Page/綾町.md "wikilink")
  - [國富町](../Page/國富町.md "wikilink")

過去的轄區還包含現在[宮崎市的部份地區](../Page/宮崎市.md "wikilink")。

## 歷史

  - 1883年：[宮崎縣從](../Page/宮崎縣.md "wikilink")[鹿兒島縣分出重新設縣](../Page/鹿兒島縣.md "wikilink")，原[諸縣郡被分割為兩部份](../Page/諸縣郡.md "wikilink")，隸屬宮崎縣的成為[北諸縣郡](../Page/北諸縣郡.md "wikilink")，留在鹿兒島縣的成為[南諸縣郡](../Page/南諸縣郡.md "wikilink")。
  - 1884年：[西諸縣郡和](../Page/西諸縣郡.md "wikilink")**東諸縣郡**從北諸縣郡分出。
  - 1889年5月1日：實施[町村制](../Page/町村制.md "wikilink")，東諸縣郡下設置[倉岡村](../Page/倉岡村.md "wikilink")、本庄村、[八代村](../Page/八代村.md "wikilink")、[木脇村](../Page/木脇村.md "wikilink")、綾村、高岡村、[穆佐村](../Page/穆佐村.md "wikilink")。（7村）
  - 1919年3月1日：本庄村改制為[本庄町](../Page/本庄町.md "wikilink")。（1町6村）
  - 1920年4月1日：高岡村改制為[高岡町](../Page/高岡町.md "wikilink")。（2町5村）
  - 1932年10月1日：綾村改制為[綾町](../Page/綾町.md "wikilink")。\[1\]（3町4村）
  - 1948年4月1日：高岡町野崎地區被併入[宮崎郡田野村](../Page/宮崎郡.md "wikilink")（現已併入宮崎市）。
  - 1948年9月15日：高岡町的東原地區和鹿毛地區被併入宮崎郡田野村。
  - 1951年3月25日：倉岡村被併入[宮崎市](../Page/宮崎市.md "wikilink")。（3町3村）
  - 1955年4月1日：高岡町和穆佐村[合併為](../Page/市町村合併.md "wikilink")[高岡町](../Page/高岡町.md "wikilink")。（3町2村）
  - 1956年9月30日：本庄町和八代村合併為[國富町](../Page/國富町.md "wikilink")。\[2\]（3町1村）
  - 1957年3月31日：木脇村被併入國富町。（3町）
  - 2006年1月1日：高岡町被併入宮崎市。\[3\]（2町）

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年5月1日</p></th>
<th><p>1889年－1944年</p></th>
<th><p>1945年－1954年</p></th>
<th><p>1955年－1989年</p></th>
<th><p>1989年－現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>高岡村</p></td>
<td><p>1920年4月1日<br />
改制為高岡町</p></td>
<td><p>1955年4月1日<br />
合併為高岡町</p></td>
<td><p>2006年1月1日<br />
併入宮崎市</p></td>
<td><p>宮崎市</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>穆佐村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>倉岡村</p></td>
<td><p>1951年3月25日<br />
併入宮崎市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>本庄村</p></td>
<td><p>1919年3月1日<br />
改制為本庄町</p></td>
<td><p>1956年9月30日<br />
合併為國富町</p></td>
<td><p>國富町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>八代村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>木脇村</p></td>
<td><p>1957年3月31日<br />
併入國富町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>綾村</p></td>
<td><p>1932年10月1日<br />
改制為綾町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

[Category:宮崎縣的郡](../Category/宮崎縣的郡.md "wikilink")

1.
2.
3.