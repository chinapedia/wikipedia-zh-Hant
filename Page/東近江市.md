**東近江市**（）是[滋賀縣的](../Page/滋賀縣.md "wikilink")[市](../Page/市.md "wikilink")。成立于2005年2月11日，由[八日市市](../Page/八日市市.md "wikilink")、[神崎郡](../Page/神崎郡_\(滋賀縣\).md "wikilink")[永源寺町](../Page/永源寺町.md "wikilink")・[五個莊町](../Page/五個莊町.md "wikilink")、[愛知郡愛東町](../Page/愛知郡_\(滋賀縣\).md "wikilink")・湖東町1市4町合併而成。2006年1月1日，[神崎郡](../Page/神崎郡_\(滋賀縣\).md "wikilink")[能登川町](../Page/能登川町.md "wikilink")・[蒲生郡](../Page/蒲生郡.md "wikilink")[蒲生町編入該市](../Page/蒲生町_\(滋賀縣\).md "wikilink")。

[Eigen-ji_(Rinzai_temple).jpg](https://zh.wikipedia.org/wiki/File:Eigen-ji_\(Rinzai_temple\).jpg "fig:Eigen-ji_(Rinzai_temple).jpg")的[本堂](../Page/本堂.md "wikilink")\]\]

## 交通

### 機場

雖然現在市內沒有機場，但過去曾有大日本帝國陸軍用的。

### 鐵道

作為該市代表車站的是[近江鐵道](../Page/近江鐵道.md "wikilink")。

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[琵琶湖線](../Page/琵琶湖線.md "wikilink")
      - [能登川站](../Page/能登川車站.md "wikilink")
  - 近江鐵道
      -   - \-  - 八日市站 -  - [大學前站](../Page/大學前站.md "wikilink") -  -  -
            [朝日大塚站](../Page/朝日大冢站.md "wikilink") -
            [朝日野站](../Page/朝日野站.md "wikilink")

      -   - 八日市站 -  -  -  - [平田站](../Page/平田站.md "wikilink")

※
其他，[東海道新幹線](../Page/東海道新幹線.md "wikilink")（[東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")）在[米原站](../Page/米原車站.md "wikilink")
-
[京都站間通過該市的五個莊地區](../Page/京都車站.md "wikilink")，並在近江鐵道本線五箇莊站南側不遠處與近江鐵道本線交差。

## 外部連結

  - [東近江市](http://www.city.higashiomi.shiga.jp/)