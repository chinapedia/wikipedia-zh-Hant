[Greatrift.jpg](https://zh.wikipedia.org/wiki/File:Greatrift.jpg "fig:Greatrift.jpg")[西奈半島](../Page/西奈半島.md "wikilink")，上方是[死海和](../Page/死海.md "wikilink")[約旦河](../Page/約旦河.md "wikilink")\]\]

**東非大裂谷**（Great Rift
Valley），位於[非洲東部](../Page/非洲.md "wikilink")，是一個在3500萬年前由非洲板塊的[地殼運動所形成的地理奇觀](../Page/地殼.md "wikilink")，縱貫東非的大裂谷是世界上最大的斷裂帶，属于生长边界。其所形成的生態、地理和人類文化都相當獨特，目前觀光的主要景點則由[肯亞進入](../Page/肯亞.md "wikilink")。

## 地理

東非大裂谷的整個形狀可畫成不規則[三角形](../Page/三角形.md "wikilink")，最深達2000公尺，寬30～100公里，全長6000公里（3700英里），是世界最長的不連續谷，由探險家[約翰·華特·古格-{里}-所命名](../Page/約翰·華特·古格里.md "wikilink")。東非大裂谷的詳細地理位置以三角形的三個點來描述的話，南點在[莫三比克入海口](../Page/莫三比克.md "wikilink")，西北點則遠到[蘇丹](../Page/苏丹共和国.md "wikilink")[約旦河](../Page/約旦河.md "wikilink")，北點則可進入[死海](../Page/死海.md "wikilink")。中間有相當多個湖泊、[火山群](../Page/火山.md "wikilink")。

## 東部及西部

一般會將大裂谷區分成東部和西部，在西部裂谷，又稱為[艾伯丁裂谷](../Page/艾伯丁裂谷.md "wikilink")，主要由高山群組成，包括有Virunga山，Mitumba山，Ruwenzori山區和Rift
Valley湖區，其中有世界最深淡水湖之一的[坦干依喀湖](../Page/坦干依喀湖.md "wikilink")（1470公尺深），以及世界上第二大的[淡水湖](../Page/淡水湖.md "wikilink")－[維多利亞湖](../Page/維多利亞湖.md "wikilink")。西部裂谷的形狀是比較破碎的。

位於肯亞的東部裂谷部分，則是整個東非大裂谷區較深的部份，其中最深處在肯亞的[奈洛比北部](../Page/奈洛比.md "wikilink")，這一部份的裂谷區沒有出海，因此所造成的湖泊則較淺、所含礦物和鹽份也較高。如[馬加迪湖幾乎都是碳酸鈉組成](../Page/馬加迪湖.md "wikilink")。[埃爾門泰塔湖](../Page/埃爾門泰塔湖.md "wikilink")（Lake
Elmenteita）、[巴林哥湖](../Page/巴林哥湖.md "wikilink")、[柏哥利亞湖和](../Page/柏哥利亞湖.md "wikilink")[納庫魯湖](../Page/納庫魯湖.md "wikilink")（Lake
Nakuru）則鹼性相當高。[奈瓦夏湖則是小規模的淡水湖](../Page/奈瓦夏湖.md "wikilink")。

## 火山

目前東非大裂谷依然在活動當中，地質學家預測幾百萬年後，東非可能會分裂成不同的版塊形狀。目前地殼活動較旺盛的區域在肯亞和坦桑尼亞的火山區，包括有非洲最高峰[吉力馬扎羅山](../Page/吉力馬扎羅山.md "wikilink")、[卡-{里}-辛比山](../Page/卡里辛比山.md "wikilink")（[卡里辛比火山](../Page/卡里辛比火山.md "wikilink")，非洲中東部雅龍加火山山脈最高峰）、[尼拉貢戈火山](../Page/尼拉貢戈火山.md "wikilink")（Nyiragongo）、[梅盧山](../Page/梅盧山.md "wikilink")（[梅魯火山](../Page/梅魯火山.md "wikilink")）、[埃爾貢山](../Page/埃爾貢山.md "wikilink")、世界上其中一座位海平面以下的[爾塔阿雷火山](../Page/爾塔阿雷火山.md "wikilink")（[Erte
Ale](../Page/尔塔阿雷火山.md "wikilink")）、著名的[倫蓋火山](../Page/倫蓋火山.md "wikilink")（[Ol
Doinyo
Lengai](../Page/:en:Ol_Doinyo_Lengai.md "wikilink")，高2890米，世界上唯一一座[熔岩溫度不高](../Page/熔岩.md "wikilink")，並以[鈉和](../Page/鈉.md "wikilink")[碳酸鹽成份居多的火山](../Page/碳酸鹽.md "wikilink")）等。

## 大裂谷底部的湖泊

東非大裂谷底部的[湖泊依成因主要分為兩種](../Page/湖泊.md "wikilink")：

1.  [斷層湖](../Page/斷層湖.md "wikilink")：是東非大裂谷斷裂時形成的，水深狀長，如[坦干依喀湖](../Page/坦干依喀湖.md "wikilink")
2.  [構造湖](../Page/構造湖.md "wikilink")：因地殼運動使地面下沉而形成的集水盆地，湖廣水淺，如[維多利亞湖](../Page/維多利亞湖.md "wikilink")

## 原始人類

東非大裂谷的另一個特色是，它可能是人類文明最早的發源地，1950年代到1970年代考古學家曾在東非大裂谷中發現了200萬年前、290萬年前人類的頭骨，而引起世人注目的則是1975年，在[坦桑尼亞和肯尼亞交界挖出了](../Page/坦桑尼亞.md "wikilink")350萬年前的人類遺骨，以及足跡化石，這是目前為止所發現最古老的史前人類證據。

## 另見

  - [貝加爾裂谷區](../Page/貝加爾裂谷區.md "wikilink")
  - [維多利亞湖](../Page/維多利亞湖.md "wikilink")
  - [Northern Cordilleran Volcanic
    Province](../Page/Northern_Cordilleran_Volcanic_Province.md "wikilink")
  - [West Antarctic Rift](../Page/West_Antarctic_Rift.md "wikilink")
  - [West and Central African Rift
    System](../Page/West_and_Central_African_Rift_System.md "wikilink")

## 參考資料

## 外部連結

  - [用Google看大裂谷的圖片](http://images.google.com/images?q=Great+Rift+Valley&hl=en&btnG=Google+Search)

  - [Article on
    geology.com](http://geology.com/articles/east-africa-rift.shtml)

  - [Geological Structure of the Dead
    Sea](http://www.wysinfo.com/Dead_Sea/dead_sea_geo_structure.htm)

  - [Birds Without
    Boundaries](http://www.wysinfo.com/Migratory_Birds/Migratory_Birds_Without_Boundaries.htm#Migration_paths)

  -
[\*](../Category/东非大裂谷.md "wikilink")
[Category:非洲地理](../Category/非洲地理.md "wikilink")
[Category:非洲地形](../Category/非洲地形.md "wikilink")
[Category:地理之最](../Category/地理之最.md "wikilink")
[Category:非洲地標](../Category/非洲地標.md "wikilink")
[Category:非洲地质](../Category/非洲地质.md "wikilink")
[Category:地形區](../Category/地形區.md "wikilink")