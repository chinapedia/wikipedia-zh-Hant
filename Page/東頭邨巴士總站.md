**東頭邨巴士總站**（[英文](../Page/英文.md "wikilink")：**Tung Tau Estate Bus
Terminus**）是位於[香港](../Page/香港.md "wikilink")[九龍黃大仙區新蒲崗西部和](../Page/九龍.md "wikilink")[九龍城區的](../Page/九龍城區.md "wikilink")[九龍城](../Page/九龍城.md "wikilink")[九龍寨城東北方之間的屋邨巴士總站](../Page/九龍寨城.md "wikilink")。由於東頭一邨和二邨分別由[香港房屋委員會和](../Page/香港房屋委員會.md "wikilink")[香港房屋協會管理](../Page/香港房屋協會.md "wikilink")，加上巴士總站位離二邨甚遠，故此又稱**東頭（一）邨巴士總站**。現時有一條九巴路線和專線小巴以該地為總站，以及一條路經之九巴路線服務該區的居民和商戶。

## 總站路線資料

### [九龍巴士2D線](../Page/九龍巴士2D線.md "wikilink")

  -
1960年9月1日起投入服務，途經[九龍城](../Page/九龍城.md "wikilink")、[九龍仔](../Page/九龍仔.md "wikilink")、[九龍塘](../Page/九龍塘.md "wikilink")、[花墟](../Page/花墟.md "wikilink")、[大坑東](../Page/大坑東.md "wikilink")、[石硤尾及](../Page/石硤尾.md "wikilink")[白田](../Page/白田.md "wikilink")。

### [九龍區專線小巴25M線](../Page/九龍區專線小巴25M線.md "wikilink")

  -
途經[九龍城廣場](../Page/九龍城廣場.md "wikilink")、[聖德肋撒醫院](../Page/聖德肋撒醫院.md "wikilink")、[喇沙書院](../Page/喇沙書院.md "wikilink")、[香港浸會大學及](../Page/香港浸會大學.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[九龍塘站](../Page/九龍塘站_\(香港\).md "wikilink")。

## 鄰近地點

  - 東頭（一、二）邨
  - 九龍寨城公園
  - [美東邨](../Page/美東邨.md "wikilink")
  - [衙前圍村](../Page/衙前圍村.md "wikilink")

[Category:黃大仙區巴士總站](../Category/黃大仙區巴士總站.md "wikilink")
[Category:新蒲崗](../Category/新蒲崗.md "wikilink")