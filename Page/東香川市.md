**東香川市**（）是位於[日本](../Page/日本.md "wikilink")[香川縣東部的](../Page/香川縣.md "wikilink")[城市](../Page/城市.md "wikilink")。

東南側以[讚岐山脈與](../Page/讚岐山脈.md "wikilink")[德島縣相鄰](../Page/德島縣.md "wikilink")，東北側為[瀨戶內海](../Page/瀨戶內海.md "wikilink")。

過去日本有九成的[手套在此生產](../Page/手套.md "wikilink")\[1\]
，但在1990年代起為了降低成本，工廠逐漸外移到[中國和](../Page/中國.md "wikilink")[東南亞地區](../Page/東南亞.md "wikilink")，現改以生產技術性的特殊手套為主。

## 歷史

### 年表

  - 1890年2月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [大內郡](../Page/大內郡.md "wikilink")：相生村、小海村、引田村、松原村、福榮村、白鳥村、三本松村、譽水村、丹生村。
      - [寒川郡](../Page/寒川郡_\(香川縣\).md "wikilink")：五名山村。
  - 1898年2月11日：三本松村改制為[三本松町](../Page/三本松町.md "wikilink")。
  - 1899年4月1日：大內郡和寒川郡合併為[大川郡](../Page/大川郡.md "wikilink")。
  - 1909年10月1日：引田村改制为[引田町](../Page/引田町.md "wikilink")。
  - 1910年1月1日：松原村改制为[白鸟本町](../Page/白鸟本町.md "wikilink")。
  - 1923年4月1日：五名山村更名为五名村。
  - 1954年4月1日：譽水村、丹生村[合併為](../Page/市町村合併.md "wikilink")[大內町](../Page/大內町.md "wikilink")。
  - 1955年3月15日：大内町和三本松町合併為新设置的大内町。
  - 1955年4月1日：引田町、相生村、小海村合併為新设置的引田町。
  - 1955年7月1日：白鸟本町、白鸟村、五名村、福荣村合併為[白鳥町](../Page/白鳥町_\(香川縣\).md "wikilink")。
  - 2003年4月1日：白鸟町、引田町、大内町合併為**东香川市**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1890年2月15日</p></th>
<th><p>1890年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>相生村</p></td>
<td><p>1955年4月1日<br />
合併為引田町</p></td>
<td><p>2003年4月1日<br />
合併為東香川市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>小海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>引田村</p></td>
<td><p>1909年10月1日<br />
引田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三本松町</p></td>
<td><p>1898年2月11日<br />
三本松町</p></td>
<td><p>1955年3月15日<br />
合併為大內町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>譽水村</p></td>
<td><p>1954年4月1日<br />
合併為大內町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>丹生村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>松原村</p></td>
<td><p>1910年1月1日<br />
白鳥本町</p></td>
<td><p>1955年7月1日<br />
合併為白鳥町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>福榮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>白鳥村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>五名山村</p></td>
<td><p>1923年4月1日<br />
更名為五名村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  中條弘矩（2003年4月28日～2007年4月26日，一任）
2.  藤井秀城（2007年4月27日～現任，現為第二任任期）

## 交通

[JRShikoku-Kotoku-line-T10-Hiketa-station-building-20100803.jpg](https://zh.wikipedia.org/wiki/File:JRShikoku-Kotoku-line-T10-Hiketa-station-building-20100803.jpg "fig:JRShikoku-Kotoku-line-T10-Hiketa-station-building-20100803.jpg")

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [高德線](../Page/高德線.md "wikilink")：[丹生車站](../Page/丹生車站.md "wikilink")
        - [三本松車站](../Page/三本松車站_\(香川縣\).md "wikilink") -
        [讚岐白鳥車站](../Page/讚岐白鳥車站.md "wikilink") -
        [引田車站](../Page/引田車站.md "wikilink") -
        [讚岐相生車站](../Page/讚岐相生車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [高松自動車道](../Page/高松自動車道.md "wikilink")：[引田交流道](../Page/引田交流道.md "wikilink")
    - [白鳥大內交流道](../Page/白鳥大內交流道.md "wikilink")

### 港口

  - [安戶港](../Page/安戶港.md "wikilink")
  - [白鳥港](../Page/白鳥港.md "wikilink")
  - [引田港](../Page/引田港.md "wikilink")

## 觀光資源

[Yodaji03s3872.jpg](https://zh.wikipedia.org/wiki/File:Yodaji03s3872.jpg "fig:Yodaji03s3872.jpg")
[Shakuoji_04.jpg](https://zh.wikipedia.org/wiki/File:Shakuoji_04.jpg "fig:Shakuoji_04.jpg")

  - [與田寺](../Page/與田寺.md "wikilink")：[四國八十八箇所總奧之院](../Page/四國八十八箇所.md "wikilink")
  - [釋王寺](../Page/釋王寺.md "wikilink")
  - [白鳥神社](../Page/白鳥神社_\(東香川市\).md "wikilink")：白鳥神社的松原被列為[日本白砂青松百選](../Page/日本白砂青松百選.md "wikilink")。
  - [絹島及](../Page/絹島.md "wikilink")[丸龜島](../Page/丸龜島.md "wikilink")
  - 引田街道

### 祭事・行事

  - [引田雛祭](../Page/引田雛祭.md "wikilink")：3月
  - [風之港祭](../Page/風之港祭.md "wikilink")：7月

## 教育

  - 高等學校

<!-- end list -->

  - [香川縣立三本松高等學校](../Page/香川縣立三本松高等學校.md "wikilink")

## 姊妹、友好城市

### 日本

  - [大內町](../Page/大內町_\(秋田縣\).md "wikilink")（[秋田縣](../Page/秋田縣.md "wikilink")[由利郡](../Page/由利郡.md "wikilink")）
      -
        過去原為合併前的大內町與其締結，現秋田縣大內町也於2005年3月22日合併為[由利本莊市](../Page/由利本莊市.md "wikilink")。
  - [郡上市](../Page/郡上市.md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")）
      -
        過去原為合併前的白鳥町與其締結，2004年3月1日岐阜縣白鳥町合併為郡上市。

## 本地出身之名人

  - [笠置靜子](../Page/笠置靜子.md "wikilink")：歌手、女演員。
  - [南原繁](../Page/南原繁.md "wikilink")：政治學者、前[東京大學校長](../Page/東京大學.md "wikilink")。
  - [久米通賢](../Page/久米通賢.md "wikilink")：[江戶時代的發明家](../Page/江戶時代.md "wikilink")、西洋學者，亦精通測量、曆法。
  - [三土忠造](../Page/三土忠造.md "wikilink")：政治家，曾任貴族院議員、眾議院議員、内務大臣、運輸大臣、樞密顧問官、鐵道大臣、遞信大臣、財務大臣、文部大臣、內閣書記官長。
  - [保井コノ](../Page/保井コノ.md "wikilink")：日本第一位女性理學博士。
  - [林康子](../Page/林康子.md "wikilink")：[女高音歌手](../Page/女高音.md "wikilink")。
  - [藤猪省太](../Page/藤猪省太.md "wikilink")：柔道家、曾獲得[世界柔道錦標賽四面金牌](../Page/世界柔道錦標賽.md "wikilink")。
  - [植田辰哉](../Page/植田辰哉.md "wikilink")：[排球教練](../Page/排球.md "wikilink")，現任[日本國家男子排球隊教練](../Page/日本國家男子排球隊.md "wikilink")
  - [神風正一](../Page/神風正一.md "wikilink")：前[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")，最高位為[關脇](../Page/關脇.md "wikilink")、現為大相撲評論員。

## 參考資料

## 外部連結

  - [東香川市商工會](http://www.shokokai-kagawa.or.jp/higashikagawa/)

  - [東香川導航](https://web.archive.org/web/20121113080612/http://ilove-higashikagawa.com/)

  - [東香川市教育委員會](http://www.higashikagawa.ed.jp/)

  - [大川廣域行政組合](http://www.o-kouiki.jp/)

<!-- end list -->

1.
2.