**Panasonic Lumix DMC-FZ50**
是[松下](../Page/松下.md "wikilink")[Lumix系列的一款准专业长焦](../Page/Lumix.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，于[2006年7月发布](../Page/2006年7月.md "wikilink")。为前作[Panasonic
Lumix DMC-FZ30的后续产品](../Page/Panasonic_Lumix_DMC-FZ30.md "wikilink")。

[萊卡亦推出此型號的雙生版本](../Page/萊卡.md "wikilink")，名為V-Lux。

## 参见

  - [Panasonic Lumix](../Page/Lumix.md "wikilink")
      - [Panasonic Lumix
        DMC-FZ30](../Page/Panasonic_Lumix_DMC-FZ30.md "wikilink")

## 外部链接

  - [品味长焦利器 松下FZ50真机试用手记](http://review.fengniao.com/40/409197.html)－蜂鸟网

[Category:Panasonic數位相機](../Category/Panasonic數位相機.md "wikilink")