**松仔園**（**Tsung Tsai
Yuen**）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[大埔區](../Page/大埔區.md "wikilink")，鄰近[大埔公路的](../Page/大埔公路.md "wikilink")[大埔滘段十四咪半](../Page/大埔滘.md "wikilink")，又稱[猛鬼橋](../Page/猛鬼橋.md "wikilink")。[香港政府於](../Page/港英政府.md "wikilink")1920年代在這裡進行[植樹](../Page/植樹.md "wikilink")，主要為[馬尾松](../Page/馬尾松.md "wikilink")，所以稱為松仔園<small>\[1\]</small>。

## 得名猛鬼橋的原因

傳聞有班童子軍，在上址露營，忽然遇洪水，卻被洪水沖去， 由于尋不獲這班童子軍， 加上附近中文大學， 睌上聽到上述有人步操， 懷疑是鬼魂，
因此稱猛鬼橋，
可是，直至現時仍未能確定猛鬼橋的得名原因。但據香港大公報1949年6月7日第4版的一宗交通事故所載，當時該處已被稱為猛鬼橋。

## 大埔滘山洪暴發事件

[Stone_plague_1955.jpg](https://zh.wikipedia.org/wiki/File:Stone_plague_1955.jpg "fig:Stone_plague_1955.jpg")

松仔園鄰近[九廣鐵路](../Page/九廣鐵路.md "wikilink")[大埔滘站及](../Page/大埔滘站.md "wikilink")[大埔公路](../Page/大埔公路.md "wikilink")
，交通方便，而且兩邊山峰綠樹成蔭，中央連綿數公里長的河溪水澗，當年闊約15米，可供行山漫步、野餐、游泳等活動，成為郊遊的熱門地點。

在1955年8月28日的下午約1時30分，一群師生正在溪澗旁午膳，突然下雨，他們走到橋樑的底部避雨，誰知[山洪突至](../Page/山洪.md "wikilink")，大部分人走避不及，被洪水沖走，遇難者經過大水渠（現為[滌濤山](../Page/滌濤山.md "wikilink")）再被沖出大海。事後點算，一共28人罹難，死者中約20人是在[聖雅各學校就讀](../Page/聖雅各學校.md "wikilink")。事後[大埔七約鄉公所立](../Page/大埔七約鄉公所.md "wikilink")《怒水橋洪流肇禍記》石碑以誌此慘劇<small>\[2\]</small>。

## 現況

松仔園的風光仍然吸引觀光客，它現在是「[大埔滘自然護理區](../Page/大埔滘自然護理區.md "wikilink")」的一部分。事後，[香港政府在溪澗上游建造水壩蓄水](../Page/香港政府.md "wikilink")，降低山洪的洪峰強度。原有的「猛鬼橋」經多次改建道路工程，已不存在，由近年新建的[鋼筋](../Page/鋼筋.md "wikilink")[混凝土汽車大橋取代](../Page/混凝土.md "wikilink")。那塊《怒水橋洪流肇禍記》被遷移至「松仔園」的「大埔滘自然護理區」之入口附近的[大埔滘花園的草坪上](../Page/大埔滘花園.md "wikilink")。發現28具屍體的下游地點，現時已建築了[滌濤山私人住宅群落](../Page/滌濤山.md "wikilink")。

## 前往交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [大埔公路近松仔園下車](../Page/大埔公路.md "wikilink")。

<!-- end list -->

  - [小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參見

  - [大埔公路雙層巴士側翻事故](../Page/大埔公路雙層巴士側翻事故.md "wikilink")（初時新聞所報，誤報為松仔園，實情傷亡在[大埔尾](../Page/大埔尾_\(香港\).md "wikilink")。）

## 參考資料

  - 互聯網

<div class="references-small">

[1955年8月28日剪報](http://n2.hk/u/attachments/day_110707/20110707_1a5acc76a7fe9f33207aSdP8t3LMo1Og.jpg)

<references />

</div>

  -
<!-- end list -->

  - 碑記

<!-- end list -->

  - 《怒水橋洪流肇禍記》的碑文內容

[石碑](http://n2.hk/u/attachments/day_090828/20090828_f941a8bb59773fc90decOhnQTupGryYN.jpg)

[Category:大埔滘](../Category/大埔滘.md "wikilink")

1.
    [oasistrek：大埔滘](http://www.oasistrek.com/tai_po_kau.htm)，於2008年11月11日查閱
2.   [死亡人數最多的一次郊野活動意外災難](http://www.hkfca.org.hk/data/draft/9808lpm.htm)