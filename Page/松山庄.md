**松山-{庄}-**（），為1920年－1938間存在之行政區，轄屬[台北州](../Page/臺北州.md "wikilink")[七星郡](../Page/七星郡.md "wikilink")。1938年4月1日廢-{庄}-併入[台北市](../Page/臺北市_\(州轄市\).md "wikilink")。今台北市[松山區及](../Page/松山區_\(臺北市\).md "wikilink")[信義區](../Page/信義區_\(臺北市\).md "wikilink")。

## 街-{庄}-原名

原屬大加蚋堡之錫口街、中陂-{庄}-、五份埔-{庄}-、三張犂-{庄}-、興雅庄、頂東勢-{庄}-、下塔悠-{庄}-、上塔悠-{庄}-、舊里族-{庄}-

  - 錫口街改稱松山\[1\]

## 行政區劃

松山-{庄}-轄域內分為[松山](../Page/錫口.md "wikilink")、[頂東勢](../Page/頂東勢.md "wikilink")、[下塔悠](../Page/下塔悠.md "wikilink")、[上塔悠](../Page/上塔悠.md "wikilink")、[舊里族](../Page/舊里族.md "wikilink")、[中坡](../Page/中坡.md "wikilink")、[五分埔](../Page/五分埔.md "wikilink")、[三張犁](../Page/三張犁.md "wikilink")、[興雅九個](../Page/興雅.md "wikilink")[大字](../Page/大字_\(行政區劃\).md "wikilink")。

1938年併入[台北市後](../Page/臺北市_\(州轄市\).md "wikilink")，此地設有[台北市役所松山出張事務所](../Page/台北市役所.md "wikilink")。1945年12月國民政府接收時，松山出張事務所管轄範圍沒有改變\[2\]。

1946年2月8日設松山區時，轄域是上述九個大字，加上原本不屬於松山-{庄}-的**中崙**。

1990年[臺北市](../Page/臺北市.md "wikilink")16區改為12區時，[縱貫鐵路以南的](../Page/縱貫線_\(鐵路\).md "wikilink")**中坡**、**五分埔**、**三張犁**、**興雅**獨立組成[信義區](../Page/信義區_\(臺北市\).md "wikilink")。

## 庄長

| 任數 | 姓名                                      | 肖像 | 出身                              | 就任       | 離任        |
| -- | --------------------------------------- | -- | ------------------------------- | -------- | --------- |
| 1  | [陳茂松](../Page/陳茂松.md "wikilink")        |    | [台北](../Page/台北州.md "wikilink") | 1920年12月 | 1925年     |
| 2  | [陳復禮](../Page/陳復禮_\(臺灣\).md "wikilink") |    | 台北                              | 1925年    | 1930年     |
| 3  | 五十嵐喜一郎                                  |    | [新潟](../Page/新潟縣.md "wikilink") | 1930年    | 1933年     |
| 4  | 中村安藏                                    |    | [熊本](../Page/熊本縣.md "wikilink") | 1933年    | 1938年4月1日 |

## 參考來源

[Category:臺北州](../Category/臺北州.md "wikilink") [Category:信義區
(臺北市)](../Category/信義區_\(臺北市\).md "wikilink") [Category:松山區
(臺灣)](../Category/松山區_\(臺灣\).md "wikilink")

1.  《新舊對照管轄便覽》
2.  《台北文物》雜誌第三卷第一期