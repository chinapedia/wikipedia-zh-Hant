[Tunnel_of_Guia_mountain_NAPE.jpg](https://zh.wikipedia.org/wiki/File:Tunnel_of_Guia_mountain_NAPE.jpg "fig:Tunnel_of_Guia_mountain_NAPE.jpg")
[松山隧道.jpg](https://zh.wikipedia.org/wiki/File:松山隧道.jpg "fig:松山隧道.jpg")
**松山隧道**（）是[澳門最長的行車](../Page/澳門.md "wikilink")[隧道](../Page/隧道.md "wikilink")，位於[二龍喉街和](../Page/二龍喉街.md "wikilink")[羅理基博士大馬路之間](../Page/羅理基博士大馬路.md "wikilink")，呈西北-東南走向。長284.85米，高7.22米，寬10.5米，設雙線車道，每小時可通700輛車\[1\]，收費全免。兩端皆連接行車天橋。[中葡友好紀念物之一的](../Page/中葡友好紀念物.md "wikilink")「蓮花」位於隧道羅理基博士大馬路出口。

## 歷史

1989年2月18日，松山隧道正式動工，由葡國偉龍建築公司（Soares
Costa）及森美（Somec）聯合集團承投，[中国铁道建筑总公司二十局集团](../Page/中国铁道建筑总公司.md "wikilink")（原第二十工程局）施工，9月18日贯通\[2\]\[3\]。1990年11月17日正式開通\[4\]。但近年澳門車輛數目激增，用量達到飽和，情況在[澳門大賽車期間尤為嚴重](../Page/澳門大賽車.md "wikilink")。2003年，[澳門土地工務運輸局在](../Page/澳門土地工務運輸局.md "wikilink")[羅理基博士大馬路盡頭](../Page/羅理基博士大馬路.md "wikilink")，在[新口岸水塘南角旁開闢新路](../Page/新口岸.md "wikilink")，即現時的水塘新路用以分流松山隧道的負擔。

### 破壞古代遺存

松山隧道在興建過程中曾發現[明朝和](../Page/明朝.md "wikilink")[清朝時期的墓葬](../Page/清朝.md "wikilink")，但在未得到應有的處理前被摧毀\[5\]。

## 附近街道

### [新口岸出口](../Page/新口岸.md "wikilink")

  - [羅理基博士大馬路](../Page/羅理基博士大馬路.md "wikilink")
  - [友誼大馬路](../Page/友誼大馬路.md "wikilink")

### [高士德出口](../Page/高士德.md "wikilink")

  - [二龍喉街](../Page/二龍喉街.md "wikilink")
  - [得勝馬路](../Page/得勝馬路.md "wikilink")
  - [士多鳥拜斯大馬路](../Page/士多鳥拜斯大馬路.md "wikilink")
  - [高士德大馬路](../Page/高士德大馬路.md "wikilink")

## 相關條目

  - [九澳隧道](../Page/九澳隧道.md "wikilink")\[6\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [土地工務運輸局 - 工程檔案
    - 1992松山隧道](http://www.dssopt.gov.mo/tc/projectfile/index.php?id=139)

[S](../Category/中国公路隧道.md "wikilink")
[Category:澳門交通](../Category/澳門交通.md "wikilink")
[Category:澳門地方](../Category/澳門地方.md "wikilink")
[Category:澳门建筑之最](../Category/澳门建筑之最.md "wikilink")

1.  澳門百科全書《松山隧道》條。
2.  澳門百科全書 附錄一：澳門大事紀
3.  中国铁道建筑总公司，[中國脊梁——大事紀](http://www.sasac.gov.cn/zgjl/zgjl_crccg/14dsj.htm)，2006年12月11日。
4.  澳門百科全書 附錄一：澳門大事紀
5.
6.