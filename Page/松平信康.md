**松平信康**（）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代武將](../Page/安土桃山時代.md "wikilink")。父親是[松平元康](../Page/松平元康.md "wikilink")（[德川家康](../Page/德川家康.md "wikilink")）。母親是[關口氏廣的女兒](../Page/關口親永.md "wikilink")[築山殿](../Page/築山殿.md "wikilink")（[今川義元的外甥女](../Page/今川義元.md "wikilink")）。妻子是[織田信長長女](../Page/織田信長.md "wikilink")[德姬](../Page/織田五德.md "wikilink")，生有[登久姬](../Page/登久姬.md "wikilink")（[小笠原秀政正室](../Page/小笠原秀政.md "wikilink")）與[熊姬](../Page/熊姬.md "wikilink")（[本多忠政室](../Page/本多忠政.md "wikilink")）。因為擔任[松平宗家的居城](../Page/松平氏.md "wikilink")[岡崎城的城主](../Page/岡崎城.md "wikilink")，因此與祖父[松平廣忠同被稱為](../Page/松平廣忠.md "wikilink")**岡崎三郎**。別稱**次郎三郎**。

不少史書記載為**德川信康**，理由是進入[江戶時代後](../Page/江戶時代.md "wikilink")，[江戶幕府許可將軍家和](../Page/江戶幕府.md "wikilink")[御三家都以](../Page/御三家.md "wikilink")「德川」為姓，以後表記再被統一，而信康則以德川家康的嫡長男身份而稱「德川信康」。

## 生平

在[永祿](../Page/永祿.md "wikilink")2年（[1559年](../Page/1559年.md "wikilink")）3月6日於[駿府城出生](../Page/駿府城.md "wikilink")，家中嫡長子。作為[今川氏的人質](../Page/今川氏.md "wikilink")，幼年在駿府度過，不過在[桶狹間之戰後](../Page/桶狹間之戰.md "wikilink")，德川軍以俘虜[鵜殿氏長和](../Page/鵜殿氏長.md "wikilink")[鵜殿氏次交換](../Page/鵜殿氏次.md "wikilink")，竹千代前往[岡崎城居住](../Page/岡崎城.md "wikilink")。

永祿5年（[1562年](../Page/1562年.md "wikilink")），家康和[織田信長的](../Page/織田信長.md "wikilink")[清洲同盟成立](../Page/清洲同盟.md "wikilink")。在永祿10年（[1567年](../Page/1567年.md "wikilink")）5月與信長的女兒[五德姬結婚](../Page/五德姬.md "wikilink")，兩人都是9歲並以夫婦的形式在[岡崎城生活](../Page/岡崎城.md "wikilink")。同年6月家康把居城移往[濱松城](../Page/濱松城.md "wikilink")，把岡崎城讓給竹千代。在7月元服時，從信長接受[偏諱](../Page/偏諱.md "wikilink")「信」字並改名為**信康**。[元龜元年](../Page/元龜.md "wikilink")（[1570年](../Page/1570年.md "wikilink")）正式成為岡崎城的城主。

信康自小就勇猛果敢，初陣在[天正元年](../Page/天正_\(日本\).md "wikilink")（[1573年](../Page/1573年.md "wikilink")）完成。

在天正3年（[1575年](../Page/1575年.md "wikilink")）的[長篠之戰中以大將身份參戰](../Page/長篠之戰.md "wikilink")，之後在與[武田氏的戰鬥中獲得軍功](../Page/武田氏.md "wikilink")，在戰鬥中受到注目。特別在天正5年（[1577年](../Page/1577年.md "wikilink")）8月的[遠江横須賀之戰中的退卻戰中擔任](../Page/遠江横須賀之戰.md "wikilink")[殿軍](../Page/殿軍.md "wikilink")，令武田軍不能越過[大井川](../Page/大井川.md "wikilink")。率領[岡崎眾輔助家康](../Page/岡崎眾.md "wikilink")。

天正7年（[1579年](../Page/1579年.md "wikilink")）8月3日，家康到訪岡崎城，翌日信康離開岡崎城並被移至[大濱城](../Page/大濱城.md "wikilink")。之後信康被移至[遠江的](../Page/遠江.md "wikilink")[堀江城](../Page/堀江城.md "wikilink")、[二俁城](../Page/二俁城.md "wikilink")，在9月15日被家康命令[切腹](../Page/切腹.md "wikilink")。享年21歲。

信康切腹的事件在《[三河物語](../Page/三河物語.md "wikilink")》中有詳細的記述。根據此書記載，信長的女兒[五德姬和跟](../Page/五德姬.md "wikilink")[今川氏有關係的](../Page/今川氏.md "wikilink")[築山殿不和](../Page/築山殿.md "wikilink")，與信康的關係都相當差，天正7年（1579年），五德向父親[信長寫了](../Page/织田信长.md "wikilink")12項事情的信書，並拜託德川家重臣[酒井忠次作為使者送信給信長](../Page/酒井忠次.md "wikilink")。信中包括了自己與信康不和及築山殿與[武田勝賴内通的事情](../Page/武田勝賴.md "wikilink")。信長質問使者忠次，忠次完全沒有庇護信康並確認書信所寫的是事實。結果，信長要求家康讓信康切腹。

在德川家中反對把信康[賜死的人有很多](../Page/賜死.md "wikilink")，甚至有家臣主張與信長[斷交](../Page/斷交.md "wikilink")。信康的傅役[平岩親吉想負上責任](../Page/平岩親吉.md "wikilink")，要求把自己的首級送給信長。但是家康判斷形勢後，認為這些小手段不能平息信長的怒火，於是決定處決信康。8月29日，首先是築山殿被護送前往[二俣城](../Page/二俣城.md "wikilink")（守將是[大久保忠世](../Page/大久保忠世.md "wikilink")）途中，在[佐鳴湖湖畔被德川家家臣](../Page/佐鳴湖.md "wikilink")[岡本時仲和](../Page/岡本時仲.md "wikilink")[野中重政殺死](../Page/野中重政.md "wikilink")。之後的9月15日，事件發生以來一直被幽禁在二俣城的信康被命令切腹。[介錯是](../Page/介錯.md "wikilink")[服部正成](../Page/服部正成.md "wikilink")，但是正成悲痛萬分，無法把主人[斬首](../Page/斬首.md "wikilink")，最後由負責檢死的[天方道綱](../Page/天方道綱.md "wikilink")[介錯](../Page/介錯.md "wikilink")。享年21歲。

關於這次切腹事件的原由有諸多說法，包括父子不和、家中派系鬥爭、與家臣團對立等。

## 墓所、祭祀

信康死後，家康建立的信康廟所[清瀧寺](../Page/清瀧寺_\(濱松市\).md "wikilink")，現今在該寺域中有信康的廟。在祭祀首塚的[若宮八幡宮中](../Page/若宮八幡宮_\(岡崎市\).md "wikilink")，信康是祭神，而與信康有深厚關係的人亦建立了數個寺院。

  - [西念寺](../Page/西念寺_\(新宿區\).md "wikilink")（[新宿區](../Page/新宿區.md "wikilink")）岡崎三郎信康供養塔－由[服部正成建立](../Page/服部正成.md "wikilink")。
  - [隆岩寺](../Page/隆岩寺_\(古河市\).md "wikilink")（[古河市](../Page/古河市.md "wikilink")）－由女婿[小笠原秀政建立](../Page/小笠原秀政.md "wikilink")
  - [萬松院](../Page/萬松院_\(小田原市\).md "wikilink")（[小田原市](../Page/小田原市.md "wikilink")）－由[大久保忠隣建立](../Page/大久保忠隣.md "wikilink")
  - 在[江淨寺](../Page/江淨寺.md "wikilink")（[靜岡市](../Page/靜岡市.md "wikilink")）的供養塔－由[榊原清政建立](../Page/榊原清政.md "wikilink")
  - 在高野山[金剛峯寺](../Page/金剛峯寺.md "wikilink")（[高野町](../Page/高野町.md "wikilink")）的岡崎三郎信康墓所－[平岩親吉建立](../Page/平岩親吉.md "wikilink")

## 人物

  - 家康對信康死非常悲傷，[關原之戰時三男秀忠遲到未能參戰時](../Page/關原之戰.md "wikilink")，家康說了「信康還在的話一定不會如此」（）。
  - [酒井忠次因為嫡男](../Page/酒井忠次.md "wikilink")[家次的所領太少](../Page/酒井家次.md "wikilink")，於是向家康提出不滿，反被家康諷刺「你也會疼愛自己的兒子啊？」（）。還有一次，與忠次父子一起觀賞[幸若舞時](../Page/幸若舞.md "wikilink")，看到為了主公而獻出自己兒子首級的場面時落淚，家康說「你們看看啊」（），當場令忠次父子非常恐懼。
  - 對於被冷遇的異母弟[結城秀康表示憐憫](../Page/結城秀康.md "wikilink")，與父親家康不同，顯示出重感情的一面。
  - 在[切腹後有](../Page/切腹.md "wikilink")[殉死者](../Page/殉死.md "wikilink")（在[濱松市的](../Page/濱松市.md "wikilink")[清瀧寺中有殉死家臣](../Page/清瀧寺.md "wikilink")[吉良於初](../Page/吉良於初.md "wikilink")（初之丞）的墳墓）出現，因此應該頗有人望。
  - 在[大久保忠教的](../Page/大久保忠教.md "wikilink")《[三河物語](../Page/三河物語.md "wikilink")》中形容，信康說的總是戰爭話題，經常去騎馬和獵鷹，被描繪為典型的武者。
  - 《[德川實紀](../Page/德川實紀.md "wikilink")》中如此評價信康「東照公（家康）的公子們中，**岡崎三郎君**（信康）、越前黄門（[結城秀康](../Page/結城秀康.md "wikilink")）、薩摩中將（[松平忠吉](../Page/松平忠吉.md "wikilink")）等人，都繼承了父親勇武的稟性，他們的軍事雄略在世間中都是十分傑出」（）。

在《三河物語》中記述「沒有這樣的殿下（信康）」（）「上下萬民都發出聲音，沒有不悲傷的」（）。信康是武勇優秀的人，在與武田軍對陣時擔任[殿軍並寸步不讓](../Page/殿軍.md "wikilink")，家康亦對此驚嘆並說「真是勇將。即使勝賴引十萬士兵來對陣都無需驚慌」（）（《[大川三志](../Page/大川三志.md "wikilink")》）。而且還在劣勢時向家康進言與勝賴決戰（《松平物語》）。如此勇猛的信康亦有溫柔的一面，在知道異母弟弟秀康被家康討厭，在邀請家康前來岡崎城後，把秀康抱到家康的膝上，令秀康被家康承認為兒子（《[貞享松平越前守書上](../Page/貞享松平越前守書上.md "wikilink")》）。

## 信康切腹之謎

出自《[三河物語](../Page/三河物語.md "wikilink")》有一說法為：信長見信康儀表不凡，唯恐自家地位不保，故意捏造信函逼迫家康除掉嫡長子。但此說漸不被採信，因為在另一本史書《[松平記](../Page/松平記.md "wikilink")》中則是提及：「讓其（信康）自殺之事，在天正七年八月朔日向信長報告了。若信長也因此而震怒的話，則任由他的意思來決定了。」這就是完全相反的記載了，將[織田信長由主導者](../Page/織田信長.md "wikilink")，改為被通知者的角色。

此外所謂家中權力爭奪說，則是《三河物語》強調了[酒井忠次在信康之死的責任問題](../Page/酒井忠次.md "wikilink")，歸咎給[酒井忠次未有效替信康辯護或加罪於信康](../Page/酒井忠次.md "wikilink")，但在流傳下來的《信光明寺文書》則收錄了家康寫與[織田信長側近](../Page/織田信長.md "wikilink")[堀秀政的書信中表明](../Page/堀秀政.md "wikilink")：「此次派左衛門督（忠次）（向信長）報告之事，得到信長的懇切回應，實不勝感謝，而有關三郎不自重之事，已在去（八月）四日把他趕出岡崎城了。」

可見在信康之死一事中，真正起作用的主導者還是[德川家康本人](../Page/德川家康.md "wikilink")。至於何以德川家的父子相爭會演變至不死不休的局面，則有兩個不同說法，其一為武田內應說，此乃延續過去[織田信長主導論的理由](../Page/織田信長.md "wikilink")，雖然當時德川家中確有零星與武田家內通情事，但就信康本人來說並無實據。

另外就是比較陰謀論的家康追放說，由於德川家長期支援織田家的戰事，對三河造成很大的經濟壓力。因此以[石川數正為首的三河派的重臣打算擁立信康繼任](../Page/石川數正.md "wikilink")，才招致[德川家康反撲](../Page/德川家康.md "wikilink")。此說亦無實證，只是從事後三河重臣大量變動進行推測。整體而言，信康之死的起因仍是未知數。

## 登場作品

  - 電影

<!-- end list -->

  - 『』（[東映](../Page/東映.md "wikilink") 1961年
    監督：[伊藤大輔](../Page/伊藤大輔.md "wikilink")
    演員：[中村錦之助](../Page/中村錦之助.md "wikilink")）

<!-- end list -->

  - 電視劇

<!-- end list -->

  - 『[德川家康](../Page/德川家康_\(大河劇\).md "wikilink")』（[NHK大河劇](../Page/NHK大河劇.md "wikilink")
    1983年 演員：[宅麻伸](../Page/宅麻伸.md "wikilink")）
  - 『』（[TBS電視台](../Page/TBS電視台.md "wikilink") 1988年
    演員：[野村宏伸](../Page/野村宏伸.md "wikilink")）
  - 『[信長KING OF
    ZIPANGU](../Page/信長KING_OF_ZIPANGU.md "wikilink")』（NHK大河劇
    1992年 演員：[早川亮](../Page/早川亮.md "wikilink")）
  - 『』（[朝日電視台](../Page/朝日電視台.md "wikilink") 1992年
    演員：[石橋保](../Page/石橋保.md "wikilink")）
  - 『』（[東京電視台](../Page/東京電視台.md "wikilink") 1994年
    演員：[赤羽秀之](../Page/赤羽秀之.md "wikilink")）
  - 『[秀吉](../Page/秀吉_\(大河劇\).md "wikilink")』（NHK大河劇 1996年
    演員：[佐藤真一郎](../Page/佐藤真一郎.md "wikilink")）
  - 『』（東京電視台 1997年 演員：[池上幸司](../Page/池上幸司.md "wikilink")）
  - 『[利家與松](../Page/利家與松.md "wikilink")』（NHK大河劇 2002年
    演員：[關根豐和](../Page/關根豐和.md "wikilink")）
  - 『』（朝日電視台 2008年 演員：[柏原收史](../Page/柏原收史.md "wikilink")）
  - 『[女城主 直虎](../Page/女城主_直虎.md "wikilink")』（NHK大河劇 2017年
    演員：[平埜生成](../Page/平埜生成.md "wikilink")）

<!-- end list -->

  - 歌謠

<!-- end list -->

  - 『』（[三波春夫](../Page/三波春夫.md "wikilink")）

## 外部連結

  - [古河市観光協会　こがナビ 隆岩寺](http://www.kogakanko.jp/top.php?ID=39&cID=33)

## 相關條目

  - [松平氏](../Page/松平氏.md "wikilink")

[Category:德川家康](../Category/德川家康.md "wikilink")
[Category:駿河國出身人物](../Category/駿河國出身人物.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:日本人物神](../Category/日本人物神.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:安祥松平氏](../Category/安祥松平氏.md "wikilink")