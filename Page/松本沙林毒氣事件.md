**-{松}-本沙林事件**（）是指發生於[日本](../Page/日本.md "wikilink")[長野縣](../Page/長野縣.md "wikilink")[松本市的](../Page/松本市.md "wikilink")[恐怖襲擊事件](../Page/恐怖襲擊.md "wikilink")，日本[警察廳稱為](../Page/警察廳.md "wikilink")「」。發動襲擊的是[奧姆真理教](../Page/奧姆真理教.md "wikilink")。並於1994年到1995年，奧姆真理教教徒在教主[麻原彰晃的指使下](../Page/麻原彰晃.md "wikilink")，策劃制造了多宗沙林毒氣事件。[日本時間](../Page/日本時間.md "wikilink")1994年6月27日黃昏至翌日6月28日9名兇徒於清晨在長野縣松本市[北深-{志}-的住宅街內散布](../Page/北深志.md "wikilink")[沙林毒氣](../Page/沙林.md "wikilink")，導致8人死亡，660人受傷。\[1\]。

根據[東京地方裁判所綜合](../Page/東京地方裁判所.md "wikilink")9名當日散布[沙林的被告的證言](../Page/沙林.md "wikilink")，松本沙林事件發生前，奧姆真理教於長野縣松本市的教團被業主要求收回[土地](../Page/土地.md "wikilink")，雙方提出[訴訟](../Page/訴訟.md "wikilink")。原教主麻原彰晃自知官司勝算不高，因而指使信徒對[長野地方裁判所松本分部的](../Page/長野地方裁判所.md "wikilink")[法官和團部周圍的居民下毒手](../Page/法官.md "wikilink")，到松本市內散布毒氣。麻原彰晃則一直否認與事件有關。

## 冤案

附近居民[河野義行是事件中第一名報案者](../Page/河野義行.md "wikilink")，一度被日本警方視為頭號[嫌疑犯](../Page/嫌疑犯.md "wikilink")。[長野縣警察於其住所內搜出少量](../Page/長野縣警察.md "wikilink")[農藥](../Page/農藥.md "wikilink")（Fenitrothion）及底片顯像劑（[氰化钾](../Page/氰化钾.md "wikilink")、[氰化銀](../Page/氰化銀.md "wikilink")），更使嫌疑加深（後來警察科學調査証實這些農藥無法製造沙林）。大眾傳播及警方情報外泄令河野義行幾乎被認定為犯人。直至翌年[東京地鐵沙林毒氣事件](../Page/東京地鐵沙林毒氣事件.md "wikilink")，發現兩次事件主謀均為奧姆真理教。河野義行無罪。事後，當時日本[國家公安委員會委員長](../Page/國家公安委員會.md "wikilink")[野中廣務代表長野縣警察公開](../Page/野中廣務.md "wikilink")[道歉](../Page/道歉.md "wikilink")。

## 判決

其中7名受害者的8名家屬代表1995年8月聯名控告奧姆真理教，並索賠5億4500萬[日圓](../Page/日圓.md "wikilink")。由於法院1996年12月決定先受理對麻原彰晃的[刑事訴訟](../Page/刑事訴訟.md "wikilink")，松本沙林毒氣事件的索賠訴訟因此中斷了3年10個月。2000年10月，松本沙林事件賠償訴訟重新被受理。東京地方法院最終作出的判決賠償總額為4億6700萬日圓，其中的1億日圓由策劃實施松本沙林事件的9名被告支付。

## 參見

  - [東京地鐵沙林毒氣事件](../Page/東京地鐵沙林毒氣事件.md "wikilink")

[Category:1994年日本](../Category/1994年日本.md "wikilink")
[Category:奧姆真理教事件](../Category/奧姆真理教事件.md "wikilink")
[Category:1994年恐怖活動](../Category/1994年恐怖活動.md "wikilink")
[Category:日本恐怖活動](../Category/日本恐怖活動.md "wikilink")
[Category:1994年6月](../Category/1994年6月.md "wikilink")
[Category:長野縣歷史](../Category/長野縣歷史.md "wikilink")
[Category:日本刑事冤案](../Category/日本刑事冤案.md "wikilink")
[Category:日本無差別殺人事件](../Category/日本無差別殺人事件.md "wikilink")
[Category:化学武器攻击](../Category/化学武器攻击.md "wikilink")
[Category:日本屠殺事件](../Category/日本屠殺事件.md "wikilink")

1.  [独家揭秘沙林：叙利亚化武袭击中的毒气之王](http://news.ifeng.com/a/20170407/50900888_0.shtml)