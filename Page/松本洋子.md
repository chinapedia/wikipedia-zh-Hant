**松本
洋子**。日本[漫畫家](../Page/漫畫家.md "wikilink")。[長崎縣出身](../Page/長崎縣.md "wikilink")。代表作是《[可愛同盟](../Page/可愛同盟.md "wikilink")》、《[迷魂之屋](../Page/迷魂之屋.md "wikilink")》\[1\]。

《[迷魂之屋](../Page/迷魂之屋.md "wikilink")》被指控是涉嫌抄襲盜作。\[2\]

## 作品

  - [魔物語](../Page/魔物語.md "wikilink")、全1冊、原名《》 ISBN 9577969380
  - [旅館殺人事件](../Page/旅館殺人事件.md "wikilink")、全1冊、原名《》 ISBN 9577967256
  - [魔力男孩](../Page/魔力男孩.md "wikilink")、全1冊、原名《》 ISBN 9576317584
  - [草莓偵探團](../Page/草莓偵探團.md "wikilink")、全1冊、原名《》 ISBN 957251315X
  - [黑色迷宮](../Page/黑色迷宮.md "wikilink")、全1冊、原名《》 ISBN 9572517172
  - [白玫瑰葬禮](../Page/白玫瑰葬禮.md "wikilink")、全1冊、原名《》 ISBN 9572513982
  - [黑色圓舞曲](../Page/黑色圓舞曲.md "wikilink")、全1冊、原名《》 ISBN 9572514083
  - [黑色組曲](../Page/黑色組曲.md "wikilink")、全1冊、原名《》 ISBN 9572513141
  - [闇的側影](../Page/闇的側影.md "wikilink")、全1冊、原名《》 ISBN 9572513958
  - [浪漫殺人通告](../Page/浪漫殺人通告.md "wikilink")、全2冊、原名《》
  - [第六感殺機](../Page/第六感殺機.md "wikilink")、全2冊、原名《》

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9572514156

:\# ISBN 9572514164

</div>

  - [可愛同盟](../Page/可愛同盟.md "wikilink")、全7冊、原名《》

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9572513176

:\# ISBN 9572514016

:\# ISBN 9572514024

:\# ISBN 9572514032

:\# ISBN 9572514261

:\# ISBN 957251427X

:\# ISBN 9572514288

</div>

  - [迷魂之屋](../Page/迷魂之屋.md "wikilink")、8冊待續、原名《》

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9577969925

:\# ISBN 9575371275

:\# ISBN 9575373103

:\# ISBN 9575374800

:\# ISBN 9575377400

:\# ISBN 9575378555

:\# ISBN 9576311071

:\# ISBN 9576311918

</div>

### 原作[安芸永里子](../Page/安芸永里子.md "wikilink")

  - [闇夜窺探](../Page/闇夜窺探.md "wikilink")、全1冊、原名《》 ISBN 9572513168

### 原作[赤川次郎](../Page/赤川次郎.md "wikilink")

  - [死神的問候](../Page/死神的問候.md "wikilink")、全1冊、原名《》 ISBN 9572513184
  - [怪奇博物館](../Page/怪奇博物館.md "wikilink")、全1冊、原名《》 ISBN 9572514180
  - [校園驚見事件簿](../Page/校園驚見事件簿.md "wikilink")、全2冊、原名《》

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9572513966

:\# ISBN 9572513974

</div>

## 参考

## 外部連結

  -
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:長崎縣出身人物](../Category/長崎縣出身人物.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")

1.
2.