[140721_Matsumoto_Seicho_Memorial_Museum_Kitakyushu_Japan01bs3.jpg](https://zh.wikipedia.org/wiki/File:140721_Matsumoto_Seicho_Memorial_Museum_Kitakyushu_Japan01bs3.jpg "fig:140721_Matsumoto_Seicho_Memorial_Museum_Kitakyushu_Japan01bs3.jpg")。\]\]

**松本清张**（）是[日本著名的作家](../Page/日本.md "wikilink")。其作品的特点是用[推理小说的方法](../Page/推理小说.md "wikilink")，探索追究[犯罪的社会根源](../Page/犯罪學.md "wikilink")，揭露社会的矛盾和恶习，反映人们潜在矛盾和苦恼。他的创作打破了早年日本侦探小说界[本格派和](../Page/本格派.md "wikilink")[变格派的固定模式](../Page/变格派.md "wikilink")，开创了[社会派推理小说领域](../Page/社会派推理小说.md "wikilink")。其作品开创了日本[犯罪小说一个新的传统](../Page/犯罪小说.md "wikilink")，虽然情节往往落于通俗，但将着眼点落于人的心理和日常的生活元素。于是他的作品往往反映了一个更广泛的社会背景，并扩大了战后的[虚无主义的范围](../Page/虚无主义.md "wikilink")。

松本清张的侦探小说巩固了他作为一名作家的的声誉。他除了侦探小说外，也有部分[历史小说和](../Page/历史小说.md "wikilink")[非虛構作品](../Page/非虛構作品.md "wikilink")，表明他对现代[日本历史](../Page/日本历史.md "wikilink")，古代历史也有相当的研究。

他于1950年发表的的[处女作](../Page/处女作.md "wikilink")《[西乡纸币](../Page/西乡纸币.md "wikilink")》获得[直木奖提名](../Page/直木奖.md "wikilink")。他曾于1952年被授予[芥川龍之介獎](../Page/芥川龍之介獎.md "wikilink")，1970年被授予[菊池寬獎](../Page/菊池寬獎.md "wikilink")，以及1957年的[日本推理作家协会奖](../Page/日本推理作家协会奖.md "wikilink")。此外他在1963年至1971年间主持[日本推理作家協會总裁一职](../Page/日本推理作家協會.md "wikilink")。

他常與[柯南·道爾](../Page/柯南·道爾.md "wikilink")、[阿加莎·克里斯蒂一同被譽為](../Page/阿加莎·克里斯蒂.md "wikilink")「世界推理小说三巨匠」\[1\]\[2\]。

## 生平

[缩略图](https://zh.wikipedia.org/wiki/File:Seichō_Matsumoto_\(1925,_16_years_old\).jpg "fig:缩略图")
1909年，松本清張出生於日本[福岡縣](../Page/福岡縣.md "wikilink")[北九州市](../Page/北九州市.md "wikilink")[小倉北區的一個貧困家庭中](../Page/小倉北區.md "wikilink")，他的兩個姐姐都很早去世，使之成為家中獨子\[3\]。

青年時期的松本清張在一家電器公司工作，但後來因公司倒閉而[失業](../Page/失業.md "wikilink")，使他不得不販賣[年糕維持生活](../Page/年糕.md "wikilink")。1928年改至印刷廠當學徒。之後又到《[朝日新聞](../Page/朝日新聞.md "wikilink")》的福岡分社工作，但工作因[二戰到來而中斷](../Page/二戰.md "wikilink")，松本清張加入軍隊，被派往[朝鮮](../Page/朝鮮.md "wikilink")\[4\]。

戰後的1950年，《朝日新聞》舉辦了一場小說[比賽](../Page/比賽.md "wikilink")，松本清張藉機發表了他的处女作《西鄉紙幣》，終獲得比賽第三名及獎金十萬元。1952年的小說《[某〈小倉日記〉傳](../Page/某〈小倉日記〉傳.md "wikilink")》則獲得了第二十八屆[芥川獎](../Page/芥川獎.md "wikilink")\[5\]。

1957年，松本清張的第一部推理小說《[點與線](../Page/點與線.md "wikilink")》連載於《旅途雜誌》，並於次年與另一部作品《[眼之壁](../Page/眼之壁.md "wikilink")》正式出版，均獲得了成功，兩本書在三個月內售出近五十萬冊。自此之後的四十多年內，松本清張又連接推出了多部推理小說，甚一度曾同時為15家報刊雜誌創作\[6\]，共完成了近八百部的作品。當時候松本清張曾讓各報刊的編輯們在其住所一樓等候，自己則在二樓寫作，寫完之後便用籃子把稿子吊下來交給眾編輯拿去排印\[7\]。

1992年，松本清張因[肝癌去世](../Page/肝癌.md "wikilink")\[8\]。

## 家系

    　　　　
    　　　　　　　　　　　　　　松本米吉（鳥取縣米子市）
    　　　　　　　　　　　　　　　　↑
    　　　　　　　　　　　　　　　　↑（養子）　　　　
    　　　　　　　　　　　　　　　　↑　　　　
    　　　　　　田中雄三郎　┏━━峯太郎（→廣島縣廣島市）　　　　
    　（鳥取縣日南町）┃　　┃　　　┃
    　　　　　　　　　┣━━┫　　　┃
    　　　　　　　　　┃　　┃　　　┣━━━━━━━清張
    　　　　　　　　阿豐（とよ）　　┃　　阿谷（タニ）
    　　　　　　　　　　　　┃　　　　　　　　　　　　　
    　　　　　　　　　　　　┗━━嘉三郎（→東京都杉並区荻窪）　
    　　　　　　　　　　　　　　　　┃
    　　　　　　　　　　　　　　　　┃
    　　　　　　　　　　　　　　璃羽（りう）

## 作品

### 全集

  - 松本清張全集（共66巻、文藝春秋出版）
  - 松本清張短編全集（全11卷、カッパ・ノベルス・光文社文庫出版）

### 小說

  - 西鄉紙幣《西郷札》（1950年）
  - [某〈小倉日記〉傳](../Page/某〈小倉日記〉傳.md "wikilink")《或る「小倉日記」伝》（1952年）
  - [點與線](../Page/點與線.md "wikilink")《点と線》（1958年）
  - [眼之壁](../Page/眼之壁.md "wikilink")《眼の壁》（1958年、光文社）
  - 蒼白的描点（又译《蓝色描点》）《蒼い描点》（1959年、光文社）
  - 小說帝銀事件（1959年、文藝春秋）
  - [零的焦点](../Page/零的焦点.md "wikilink")《ゼロの焦点》（1959年、カッパ・ノベルス）
  - [天城山奇案](../Page/天城山奇案.md "wikilink")《天城越え》（1959年11月、『サンデー毎日』特別号）
  - 黑色樹海《黒い樹海》（1960年、講談社）
  - 黑色画集《黒い画集》（1960年、カッパ・ノベルス）
  - [浪花上的灯塔](../Page/浪花上的灯塔.md "wikilink")（又译《波之塔》）《波の塔》（1960年、カッパ・ノベルス）
  - 歪曲的複寫《歪んだ複写》（1961年、新潮社）
  - [霧之旗](../Page/霧之旗.md "wikilink")《霧の旗》（1961年、中央公論社）
  - [黑影地带](../Page/黑影地带.md "wikilink")《影の地帯》（1961年、カッパ・ノベルス）
  - 黄色的風土《黄色い風土》（1961年、講談社）
  - 考える葉（1961年、角川書店）
  - [砂之器](../Page/砂之器.md "wikilink")（又译《砂器》）《砂の器》（1961年、カッパ・ノベルス）
  - [坏人们](../Page/坏人们.md "wikilink")《わるいやつら》（1961年、新潮社）
  - [黑色福音](../Page/黑色福音.md "wikilink")《黒い福音》（1961年、中央公論社）
  - 高校杀人事件（1961年、カッパ・ノベルス）
  - 風的視線（1962年、カッパ・ノベルス）
  - 不安之演奏（1962年、ポケット文春）
  - 深層海流（1962年、文藝春秋）
  - 連環（1962年、講談社）
  - 落差（1963年、文藝春秋）
  - 神と野獣の日（1963年、カッパ・ノベルス）
  - 火縄（1963年、講談社）
  - [野兽之道](../Page/野兽之道.md "wikilink")《けものみち》（1964年、新潮社）
  - 絢爛たる流離（1964年、中央公論社）
  - 花実のない森（1964年、カッパ・ノベルス）
  - 北方詩人（1964年、中央公論社）
  - 草の陰刻（1965年、講談社）
  - 溺谷（1966年、新潮社）
  - 蒼ざめた礼服（1966年、カッパ・ノベルス）
  - [半生記](../Page/半生記.md "wikilink")《半生札记》（1966年、河出書房新社）
  - 花冰（1966年、講談社）
  - 荒漠之盐（1967年、中央公論社）
  - 二重葉脈（1967年、カッパ・ノベルス）
  - [D之複合](../Page/D之複合.md "wikilink")（1968年、カッパ・ノベルス）
  - 中央流沙（1968年、河出書房新社）
  - 火與汐（1968年、文藝春秋）
  - 小說東京帝国大学（1969年、新潮社）

<!-- end list -->

  - 内海之轮《内海の輪》（1969年、カッパ・ノベルス）
  - 分離的時間（1969年、カッパ・ノベルス）
  - 人間水域（1970年、ノンブック）
  - 強き蟻（1971年、文藝春秋）
  - 喪失的儀礼（1972年、新潮社）
  - 風的气息（1974年、朝日新聞社）
  - 火之路（1975年、文藝春秋）
  - 黑色回廊（1976年、文藝春秋）
  - 渡された場面（1976年、新潮社）
  - 象徵的設計（1976年、文藝春秋）
  - 西海道談綺（1976-77年、文藝春秋）
  - 棲息分布（1977年、講談社）
  - 屈折的回路（1977年、文藝春秋）
  - [黑点漩涡](../Page/黑点漩涡.md "wikilink")《渦》（1977年、日本経済新聞社）
  - 風紋（1978年、講談社）
  - 空城（1978年、文藝春秋）
  - 水之肌肤（1978年、新潮社）
  - [天才女畫家](../Page/天才女畫家.md "wikilink")《天才画の女》（1979年、新潮社）
  - 白与黑之革命（1979年、文藝春秋）
  - [黑皮记事本](../Page/黑皮记事本.md "wikilink")（又译《黑色笔记》《黑色皮革手册》）《黒革の手帖》（1980年、新潮社）
  - 十万分之一偶然（1981年、文藝春秋）
  - [夜光的階梯](../Page/夜光的階梯.md "wikilink")《夜光の階段》（又译《女性阶梯》，1981年、新潮社）
  - 殺人行おくのほそ道（1982年、講談社ノベルス）
  - 死亡的発送（1982年、カドカワ・ノベルズ）
  - 湖底的光芒（1983年、講談社ノベルス）
  - [彩色的河流](../Page/彩色的河流.md "wikilink")《彩り河》（1983年、文藝春秋）
  - 迷走地図（1983年、新潮社）
  - 翳った旋舞（1984年、カドカワノベルズ）
  - 被涂乱的书《塗られた本》（1984年、講談社ノベルス）
  - 網（1984年、光文社文庫）
  - 热手帕《熱い絹》（1985年、講談社）
  - 霧中会議（1987年、文藝春秋）
  - [黑色的天空](../Page/黑色的天空.md "wikilink")《黒い空》（1988年、朝日新聞社）
  - 红色冰河期《赤い氷河期》（1989年、新潮社）
  - 一九五二年日航機坠毁事件（1992年、角川書店）
  - 犯罪的报复（1992年、角川書店）
  - 隠花的平原（1993年、新潮社）
  - 神之纷乱之心（1997年、文藝春秋）

### 歷史論

  - 日本的黑霧《日本の黒い霧》（1960年、文藝春秋）
  - 現代官僚論（1963-66年、文藝春秋）
  - 昭和史發掘（1965-72年、文藝春秋）
  - 逃亡（1966年、カッパ・ノベルス）
  - 《ミステリーの系譜》（1968年、新潮社）

## 影像化作品

### 電視

  - 【1961年】波之塔{{〈}}CX{{〉}}
  - 【1962年】[砂之器](../Page/砂之器.md "wikilink"){{〈}}TBS{{〉}}
  - 【1964年】波之塔{{〈}}EX{{〉}}
  - 【1967年】霧之旗{{〈}}EX{{〉}}
  - 【1969年】霧之旗{{〈}}CX{{〉}}
  - 【1970年】波之塔{{〈}}TBS{{〉}}
  - 【1970年】波之塔{{〈}}NHK{{〉}}
  - 【1972年】霧之旗{{〈}}NHK{{〉}}
  - 【1977年】白い闇{{〈}}TBS{{〉}}
  - 【1977年】[砂之器](../Page/砂之器.md "wikilink"){{〈}}CX{{〉}}
  - 【1978年】天城越え{{〈}}NHK{{〉}}
  - 【1980年】空の城{{〈}}NHK{{〉}}
  - 【1982年】黑革記事本{{〈}}EX{{〉}}
  - 【1982年】獸道{{〈}}NHK{{〉}}
  - 【1983年】零的焦点{{〈}}TBS{{〉}}
  - 【1983年】波之塔{{〈}}NHK{{〉}}
  - 【1983年】霧之旗{{〈}}NTV{{〉}}
  - 【1984年】黑色福音{{〈}}TBS{{〉}}
  - 【1985年】壞蛋們{{〈}}NTV{{〉}}
  - 【1991年】西郷札{{〈}}TBS{{〉}}
  - 【1991年】黒色畫集・坂道之家{{〈}}TBS{{〉}}
  - 【1991年】波之塔{{〈}}CX{{〉}}
  - 【1991年】霧之旗{{〈}}EX{{〉}}
  - 【1991年】[砂之器](../Page/砂之器.md "wikilink"){{〈}}EX{{〉}}
  - 【1991年】獸道{{〈}}NTV{{〉}}
  - 【1992年】黒色畫集・證言{{〈}}TBS{{〉}}
  - 【1992年】迷走地図{{〈}}TBS{{〉}}
  - 【1993年】某「小倉日記」傳{{〈}}TBS{{〉}}
  - 【1995年】零的焦点{{〈}}NHK{{〉}}
  - 【1997年】霧之旗{{〈}}CX{{〉}}
  - 【1998年】[天城山奇案](../Page/天城山奇案.md "wikilink"){{〈}}TBS{{〉}}
  - 【2000年】危険な斜面{{〈}}TBS{{〉}}
  - 【2001年】壞蛋們{{〈}}TX{{〉}}
  - 【2003年】霧之旗{{〈}}TBS{{〉}}
  - 【2004年】[砂之器](../Page/砂之器.md "wikilink"){{〈}}TBS{{〉}}
  - 【2004年】黑革記事本{{〈}}EX{{〉}}
  - 【2005年】黒革の手帖スペシャル～白い闇{{〈}}EX{{〉}}
  - 【2005年】黑色樹海{{〈}}CX{{〉}}
  - 【2006年】獸道{{〈}}EX{{〉}}
  - 【2006年】蒼色瞄點{{〈}}CX{{〉}}
  - 【2006年】波之塔{{〈}}TBS{{〉}}
  - 【2007年】壞蛋們{{〈}}EX{{〉}}
  - 【2007年】買地方報紙的女人{{〈}}NTV{{〉}}
  - 【2004年】塗られた本{{〈}}TBS{{〉}}
  - 【2007年】點與線{{〈}}EX{{〉}}
  - 【2007年】殺人行おくのほそ道{{〈}}CX{{〉}}
  - 【2008年】疑惑
  - 【2009年】[夜光的階梯](../Page/夜光的階梯.md "wikilink")
  - 【2009年】中央流沙{{〈}}TBS{{〉}}
  - 【2009年】火與汐{{〈}}TBS{{〉}}
  - 【2009年】臉{{〈}}NHK{{〉}}
  - 【2010年】松本清張誕辰100年紀念二部作～霧之旗/書道教授{{〈}}NTV{{〉}}
  - 【2010年】球形的荒野{{〈}}CX{{〉}}
  - 【2011年】[砂之器](../Page/砂之器.md "wikilink"){{〈}}EX{{〉}}
  - 【2012年】松本清張逝世20周年二部作～十萬分之一的偶然/熱空氣{{〈}}朝日電視台{{〉}}

### 電影

  - 【1961年、2009年】零的焦點
  - 【1971年】内海の輪
  - 【1974年】砂之器

## 獎項

  - 1950年 周刊朝日「百萬人的小說」入選三等賞《西鄉紙幣》
  - 1951年 第25屆[直木獎入圍](../Page/直木獎.md "wikilink")《西鄉紙幣》
  - 1952年 第28屆[直木獎入圍](../Page/直木獎.md "wikilink")《某〈小倉日記〉傳》
  - 1953年 第28屆[芥川龍之介獎](../Page/芥川龍之介獎.md "wikilink")《某〈小倉日記〉傳》
  - 1953年 第1屆ALL新人盃佳作第一位〈啾啾吟〉
  - 1957年 第10屆日本偵探作家俱樂部賞〈顏〉
  - 1959年 第16屆文藝春秋讀者賞《小說帝銀事件》
  - 1963年 第5屆日本記者會議賞《日本的黑霧》、《深層海流》、《現代官僚論》
  - 1966年 第5屆婦人公論讀者賞《沙漠之鹽》
  - 1967年 第1屆[吉川英治文學獎](../Page/吉川英治文學獎.md "wikilink")《昭和史發掘》、《花冰》、《逃亡》
  - 1970年 第18屆[菊池寬獎](../Page/菊池寬獎.md "wikilink")《昭和史發掘》
  - 1971年 第3屆小說現代Golden讀者賞〈留守宅事件〉
  - 1978年 第29屆NHK放送文化賞
  - 1985年 文春Best Mystery 100 第3名《點與線》、第15名《零的焦點》、第53名《砂之器》、第73名《黑色畫集》
  - 1990年 89年度朝日賞 社會派推理小說創始、現代史研究

## 参见

  - [推理小说](../Page/推理小说.md "wikilink")
  - [北九州市立松本清張紀念館](../Page/北九州市立松本清張紀念館.md "wikilink")

## 註釋

## 參考資料

<references />

## 外部連結

  - [松本清張紀念館](http://www.kid.ne.jp/seicho/html/)

  -
[Category:日本推理小說作家](../Category/日本推理小說作家.md "wikilink")
[Category:芥川獎獲獎者](../Category/芥川獎獲獎者.md "wikilink")
[Category:日本推理作家協會獎獲得者](../Category/日本推理作家協會獎獲得者.md "wikilink")
[Category:菊池宽奖获奖者](../Category/菊池宽奖获奖者.md "wikilink")

1.

2.

3.

4.
5.
6.
7.

8.