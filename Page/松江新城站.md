**松江新城站**位于[上海](../Page/上海.md "wikilink")[松江区](../Page/松江区.md "wikilink")[松江新城嘉松南路](../Page/松江新城.md "wikilink")，为[上海轨道交通9号线一期的地下](../Page/上海轨道交通9号线.md "wikilink")[岛式车站](../Page/岛式站台.md "wikilink")，於2007年12月29日啟用。車站以橙色作為佈置的主要色彩。在9号线松江南延伸段开通前，作为其西端终点。

## 车站構造

[島式月台](../Page/島式月台.md "wikilink")1面2線地下車站。

  - 9号线月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th></th>
<th><p><a href="../Page/松江南站站.md" title="wikilink">松江南站方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td></td>
<td><p><a href="../Page/佘山站.md" title="wikilink">佘山</a>・<a href="../Page/徐家汇站.md" title="wikilink">徐家汇</a>・<a href="../Page/世纪大道站_(上海).md" title="wikilink">世纪大道</a>・<a href="../Page/曹路站.md" title="wikilink">曹路方向</a></p></td>
</tr>
</tbody>
</table>

## 公交换乘

松江12路、松江14路、松江16路、松江20路、松江22路、松江23路、松江25路

## 车站出口

  - 1、2号口：通跃路西侧，绿城路和思贤路间
  - 3、4号口：嘉松南路东侧，绿城路和思贤路间

## 鄰近車站

  - **現行營運模式**

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:松江区地铁车站](../Category/松江区地铁车站.md "wikilink")