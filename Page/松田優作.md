**松田優作**（，），日本知名電視、電影演員、歌星，[動作片演員](../Page/動作片演員.md "wikilink")，公認為日本英年早逝演員的代表人物之一。兩位兒子[松田龍平](../Page/松田龍平.md "wikilink")、[松田翔太也都是日本知名演員](../Page/松田翔太.md "wikilink")。

## 概要

松田優作為日韓混血兒（父親為日本人，母親為[在日韓國人](../Page/在日韓國人.md "wikilink")）\[1\]\[2\]\[3\]，出生於[山口縣](../Page/山口縣.md "wikilink")[下關市](../Page/下關市.md "wikilink")，[豐南高等學校夜間部普通科畢業](../Page/豐南高等學校.md "wikilink")、文學部肄業，曾於美國居住。

他有兩段婚姻，第一任妻子[松田美智子](../Page/松田美智子.md "wikilink")（原姓：熊本）為作家，1975年結婚、1981年離婚；第二任妻子[松田美由紀](../Page/松田美由紀.md "wikilink")（原姓：熊谷）亦為演員，兩人於1983年結婚，並生下二男一女。

自1973年起，因演出電視劇《[向太陽怒吼](../Page/向太陽怒吼_\(日劇\).md "wikilink")》、1977年《[大都會
PARTII](../Page/大都會_PARTII.md "wikilink")》的刑警角色，在劇中身手矯健俐落，奠定知名度；1979年的《探偵物語》，則是他的電視劇代表作品。1989年更因美國電影《[黑雨](../Page/黑雨_\(美國電影\).md "wikilink")》裡飾演的日本黑道堂主一角打開國際知名度；惜因[膀胱癌於拍完](../Page/膀胱癌.md "wikilink")《黑雨》未久即告病逝，留給影迷無限唏噓與懷念。

## 電影

  - 1973年
    《[狼的紋章](../Page/狼的紋章.md "wikilink")》導演：[松本正志](../Page/松本正志.md "wikilink")
  - 1977年
    《[人間的證明](../Page/人間的證明.md "wikilink")》導演：[佐藤純彌](../Page/佐藤純彌.md "wikilink")
  - 1983年
    《[探偵物語](../Page/探偵物語_\(1983年電影\).md "wikilink")》導演：[根岸吉太郎](../Page/根岸吉太郎.md "wikilink")
  - 1985年
    《[從今以後](../Page/從今以後.md "wikilink")》導演：[森田芳光](../Page/森田芳光.md "wikilink")
  - 1989年
    《[黑雨](../Page/黑雨_\(美國電影\).md "wikilink")》導演：[雷利史考特](../Page/雷利史考特.md "wikilink")，與[迈克尔·道格拉斯](../Page/迈克尔·道格拉斯.md "wikilink")、[高倉健合演](../Page/高倉健.md "wikilink")

### 电视剧

  - 1973年
    《[向太陽怒吼](../Page/向太陽怒吼_\(日劇\).md "wikilink")》飾柴田純　獲[石原裕次郎贊賞提攜](../Page/石原裕次郎.md "wikilink")
  - 1977年 《[大都會 PARTII](../Page/大都會_PARTII.md "wikilink")》飾德吉功
  - 1979年－1980年 《[探偵物語](../Page/探偵物語.md "wikilink")》飾工藤俊作

## 家族

  - 元配：[松田美智子](../Page/松田美智子.md "wikilink")（原姓：熊本）
  - 妻：[松田美由紀](../Page/松田美由紀.md "wikilink")（原姓：熊谷）
  - 長子：[松田龍平](../Page/松田龍平.md "wikilink")
  - 次子：[松田翔太](../Page/松田翔太.md "wikilink")
  - 長女：Yuki（）

## 參考來源

[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:山口縣出身人物](../Category/山口縣出身人物.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:電影旬報十佳獎最佳男主角得主](../Category/電影旬報十佳獎最佳男主角得主.md "wikilink")
[Category:韓裔日本人](../Category/韓裔日本人.md "wikilink")
[Category:橫濱電影節最佳男主角得主](../Category/橫濱電影節最佳男主角得主.md "wikilink")
[Category:報知電影獎最佳男主角得主](../Category/報知電影獎最佳男主角得主.md "wikilink")
[Category:罹患膀胱癌逝世者](../Category/罹患膀胱癌逝世者.md "wikilink")

1.
2.
3.