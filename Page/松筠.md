**松筠**（，），號**湘浦**，一說晚號**百二老人**，**瑪拉特**氏，[蒙古](../Page/蒙古.md "wikilink")[正藍旗人](../Page/正藍旗.md "wikilink")，[清朝名臣](../Page/清朝.md "wikilink")。歷[乾隆](../Page/乾隆.md "wikilink")、[嘉慶](../Page/嘉慶.md "wikilink")、[道光三朝共五十二年](../Page/道光.md "wikilink")。

## 生平

[筆帖式出身](../Page/筆帖式.md "wikilink")、歷任[內閣學士](../Page/內閣學士.md "wikilink")、[內務府大臣](../Page/內務府.md "wikilink")、[兩江總督](../Page/兩江總督.md "wikilink")、[兩廣總督](../Page/兩廣總督.md "wikilink")、[協辦大學士](../Page/協辦大學士.md "wikilink")、[東閣大學士](../Page/東閣大學士.md "wikilink")、[武英殿大學士](../Page/武英殿大學士.md "wikilink")、吏、戶、禮、兵、工五部尚書，三度在[軍機處上行走](../Page/軍機處.md "wikilink")。松筠半生在邊疆地區度過，出任[庫倫辦事大臣](../Page/庫倫辦事大臣.md "wikilink")、[駐藏辦事大臣](../Page/駐藏大臣.md "wikilink")、[陝甘總督](../Page/陝甘總督.md "wikilink")、[伊犁將軍](../Page/伊犁將軍.md "wikilink")，對當地的貿易、吏治、水利、農業頗有貢獻，邊功卓著，惟性情鯁直，不隨時俯仰，仕途屢起屢蹶，多次遭褫職，晚年益多挫折。道光十五年，松筠卒，諡**文清**。

松筠的學術造詣頗高，著有《綏服紀略》、《西招紀行詩》、《西招圖略》、《古品節錄》等著作，並主持纂修《新疆識略》十二卷、《西陲總統事略》十二卷。《[衛藏通志](../Page/衛藏通志.md "wikilink")》一書，亦有學者認爲是松筠任駐藏大臣時所作。滿文著作則有《百二老人語錄》八卷。藝術成就則以書法為最，擅長寫大虎字，時人甚至認為其字可以辟邪。

在乾隆晚年軍機大臣任內，曾經奉乾隆皇帝之命，送乾隆五十八年來華的英國特使團歸國，據正使馬加爾尼的描述，他對於這次特使團提出的七點商貿要求未果非常同情，他是贊成的，並認為這樣有助於兩國的交流與了解。

葬於[松王墳](../Page/松王墳.md "wikilink")。

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")
  - 中見立夫.關於《百二老人語錄》的各種抄本.吳雪娟.滿文文獻研究.7105064889.
  - 村上信明.松筠著『百二老人語錄』鈔本4種のテクスト比較.楠木賢道.清朝におけゐ滿．蒙．漢の政治統合よ文化變容.
  - 蔡名哲. 本計是圖，審所先務－《百二老人語錄》中的認同與記憶. 國立中正大學，2010年.
  - 國朝書人輯略
  - [續碑傳集](../Page/續碑傳集.md "wikilink")
  - 松文清公升官錄

[M玛](../Category/清朝兩廣總督.md "wikilink")
[M玛](../Category/軍機大臣.md "wikilink")
[M玛](../Category/清朝歷史學家.md "wikilink")
[M玛](../Category/駐藏大臣.md "wikilink")
[M玛](../Category/蒙古正蓝旗人.md "wikilink")
[Category:庫倫辦事大臣](../Category/庫倫辦事大臣.md "wikilink")
[Category:伊犁將軍](../Category/伊犁將軍.md "wikilink")
[Category:熱河都統](../Category/熱河都統.md "wikilink")
[Category:清朝協辦大學士](../Category/清朝協辦大學士.md "wikilink")
[Category:清朝東閣大學士](../Category/清朝東閣大學士.md "wikilink")
[Category:清朝武英殿大學士](../Category/清朝武英殿大學士.md "wikilink")
[Category:清朝書法家](../Category/清朝書法家.md "wikilink")
[Category:清朝工部尚書](../Category/清朝工部尚書.md "wikilink")
[Category:清朝兵部尚書](../Category/清朝兵部尚書.md "wikilink")
[Category:清朝禮部尚書](../Category/清朝禮部尚書.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:諡文清](../Category/諡文清.md "wikilink")
[Category:正黃旗滿洲都統](../Category/正黃旗滿洲都統.md "wikilink")
[Category:正紅旗滿洲都統](../Category/正紅旗滿洲都統.md "wikilink")
[Category:鑲白旗滿洲都統](../Category/鑲白旗滿洲都統.md "wikilink")
[Category:正藍旗滿洲都統](../Category/正藍旗滿洲都統.md "wikilink")
[Category:鑲藍旗滿洲都統](../Category/鑲藍旗滿洲都統.md "wikilink")
[Category:中国旅游作家](../Category/中国旅游作家.md "wikilink")