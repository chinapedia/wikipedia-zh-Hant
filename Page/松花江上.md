《**松花江上**》是[中国抗日战争的著名歌曲之一](../Page/中国抗日战争.md "wikilink")，由[张寒晖作曲作詞](../Page/张寒晖.md "wikilink")。歌曲描述[九一八事變後](../Page/九一八事變.md "wikilink")[東北三省被](../Page/東北三省.md "wikilink")[日軍佔領後東北居民的苦況和收復失地的盼望](../Page/日軍.md "wikilink")。

九一八事变以后，[东北军不抵抗退出东北后](../Page/东北军.md "wikilink")，东北沦陷。东北军后被调至[陕西](../Page/陕西.md "wikilink")、[甘肃一带围剿](../Page/甘肃.md "wikilink")[红军](../Page/中國工農紅軍.md "wikilink")。与此同时，大量东北流亡学生也聚集到[西安](../Page/西安.md "wikilink")。在东北军士兵和东北流亡学生中，酝酿着思念沦陷的故土，思念家乡父老的情绪，以及抗日救亡，打回东北去的思想。1936年5月，[陕西省立西安二中的教师张寒晖创作了这首歌曲](../Page/陕西省立西安二中.md "wikilink")，歌曲在西安广泛传唱，后传唱于长城内外，大江南北，成为经典的抗日救亡歌曲。

关于这首歌曲的曲调，张寒晖曾经说「我把北方『娘们』在坟上哭丈夫、哭儿子的那种哭声变成《松花江上》的曲调了」。

1964年在北京公演的大型音樂舞蹈史詩《[東方紅](../Page/東方紅_\(音樂舞蹈史詩\).md "wikilink")》將《松花江上》的歌詞略為改動，將「爹娘啊，爹娘啊，什麼時候才能歡聚在一堂」改為「同胞啊，同胞啊，什麼時候才能收回我家鄉」。

## 衍生作品

1967年，中国著名钢琴家、作曲家[崔世光以歌曲](../Page/崔世光.md "wikilink")《松花江上》为素材改编创作一首同名钢琴独奏曲，乐曲结构采用了比较常见的带再现的[复二部曲式](../Page/复二部曲式.md "wikilink")，包括七小节引子、呈示部、发展部、再现部和尾声等部分\[1\]。

## 参考文献

## 深入阅读

  - 《东亚三国的近现代史》，<span style="font-size:smaller;">《东亚三国的近现代史》</span>编委会
    社会科学文献出版社 2005年6月 ISBN 7801906462

## 外部链接

  - [](https://web.archive.org/web/20070809105655/http://www.sygz.dqt.com.cn/yangxudong/gqxs-2004-4-7-songhuajiangshang.htm)
  - [youtube松花江上](http://www.youtube.com/watch?v=lkzeGHaXDIQ)
  - [松花江上（优酷）](http://v.youku.com/v_show/id_XMjA2OTgxNjUy.html)

[Category:中华民国大陆时期歌曲作品](../Category/中华民国大陆时期歌曲作品.md "wikilink")
[Category:抗日战争题材歌曲](../Category/抗日战争题材歌曲.md "wikilink")
[Category:九一八事變](../Category/九一八事變.md "wikilink")

1.