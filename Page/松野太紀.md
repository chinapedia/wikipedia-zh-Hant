**松野太紀**，本名、舊藝名**松野
達也**，[日本男性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。[青二Production所屬](../Page/青二Production.md "wikilink")，[東京都出身](../Page/東京都.md "wikilink")。

代表作是《[金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")》金田一一。

## 演出作品

### 動畫作品

**1978年**

  - [星星的王子殿下 小王子](../Page/星星的王子殿下_小王子.md "wikilink")（**王子**）

**1982年**

  - [十五少年漂流記](../Page/十五少年漂流記.md "wikilink")（ブリアン）

**1984年**

  - [猿飛小忍者](../Page/猿飛小忍者.md "wikilink")（服部優一郎）
  - [チックンタックン](../Page/チックンタックン.md "wikilink")（キザオ）

**1987年**

  - [世紀末救世傳說 北斗神拳2](../Page/北斗神拳.md "wikilink")（ヨハン）

**1988年**

  - [麵包超人](../Page/麵包超人.md "wikilink")（ナッツくん、ケトルマン、くすだまん、やきのりくん）

**1990年**

  - [嚕嚕米 冒險日記](../Page/嚕嚕米.md "wikilink")（ビリー）

**1991年**

  - [おばけのホーリー](../Page/おばけのホーリー.md "wikilink")（アイスくん）
  - [金魚注意報](../Page/金魚注意報.md "wikilink")（加藤高広（タカピー））

**1993年**

  - [ドラゴンリーグ](../Page/ドラゴンリーグ.md "wikilink")（カズ）

**1994年**

  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（拉斯卡爾／拉斯凱爾）

**1995年**

  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（學生會長）
  - [美少女戰士SuperS](../Page/美少女戰士SuperS.md "wikilink")（艾里斯）

**1996年**

  - [鬼太郎（第四作）](../Page/鬼太郎.md "wikilink")（さざえ鬼、倉ぼっこ、こうもり猫）
  - [水色時代](../Page/水色時代.md "wikilink")（井川先生）
  - [魔法少女砂沙美](../Page/魔法少女砂沙美.md "wikilink")（真嶋廣人）
  - [神劍闖江湖 -明治剣客浪漫譚-](../Page/神劍闖江湖.md "wikilink")（西脇、癋見、六助、ヤス、月鬼）

**1997年**

  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（**金田一一**）
  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（ジョン）
  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")（龜田則正、白鳥純、他）
  - [玩偶遊戲](../Page/玩偶遊戲.md "wikilink")（夢園又太）
  - [魔神英雄傳](../Page/魔神英雄傳.md "wikilink")（マリン）

**1998年**

  - [こっちむいて\!みい子](../Page/こっちむいて!みい子.md "wikilink")（**江口龍平**）
  - [青澀之戀](../Page/青澀之戀.md "wikilink")（佐佐木哲郎）
  - [ふたり暮らし](../Page/ふたり暮らし.md "wikilink")（**青山ケンジ**）
  - [ふしぎ魔法ファンファンファーマシィー](../Page/ふしぎ魔法ファンファンファーマシィー.md "wikilink")（パン屋）
  - [ヘリタコぷーちゃん](../Page/ヘリタコぷーちゃん.md "wikilink")（かえる先生、ナレーション）

**1999年**

  - [星界的紋章](../Page/星界的紋章.md "wikilink")（クードゥリン）

**2000年**

  - [勝負師傳説 哲也](../Page/哲也-雀聖と呼ばれた男.md "wikilink")（中）

**2001年**

  - [犬夜叉](../Page/犬夜叉.md "wikilink")（鋼牙）
  - [チャンス〜トライアングルセッション〜](../Page/チャンス〜トライアングルセッション〜.md "wikilink")（荒西ケイジ）
  - [第一神拳](../Page/第一神拳.md "wikilink")（小橋健太）

**2002年**

  - [我們這一家](../Page/我們這一家.md "wikilink")（青年）
  - [最終兵器少女](../Page/最終兵器少女.md "wikilink")（リョーヘイ）
  - [スーパークマさん](../Page/アニマックス#スーパークマさん.md "wikilink")（**クマさん**）
  - [炎之蜃氣樓](../Page/炎之蜃氣樓.md "wikilink")（成田譲）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（ライスライス）

**2003年**

  - [エアマスター](../Page/エアマスター.md "wikilink")（深道信彦）
  - [最遊記RELOAD](../Page/最遊記.md "wikilink")（藍巴）
  - [住めば都のコスモス荘
    すっとこ大戦ドッコイダー](../Page/住めば都のコスモス荘_すっとこ大戦ドッコイダー.md "wikilink")（梅木パパ）
  - [トッポ・ジージョ劇場](../Page/トッポ・ジージョ劇場.md "wikilink")（**トッポジージョ**）
  - [魔獣戦線 THE
    APOCALYPSE](../Page/魔獣戦線_THE_APOCALYPSE.md "wikilink")（天外富三郎）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink") （ライスライス、アラン、ラフィット）

**2004年**

  - [B傳說！戰鬥彈珠人](../Page/B傳說！戰鬥彈珠人.md "wikilink")（比亞斯）
  - [光之美少女](../Page/光之美少女.md "wikilink")（守護者）
  - [ボボボーボ・ボーボボ](../Page/ボボボーボ・ボーボボ.md "wikilink")（ツル・ツルリーナ4世）
  - [まっすぐにいこう。](../Page/まっすぐにいこう。.md "wikilink")（ジャック）
  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（萬丈目準）

**2005年**

  - [おでんくん](../Page/おでんくん.md "wikilink")（ジャラ助）
  - ふたりはプリキュア Max Heart（ウィズダム）

**2006年**

  - [怪 〜ayakashi〜](../Page/怪_〜ayakashi〜.md "wikilink")「四谷怪談」（小佛小平）
  - [erico](../Page/erico.md "wikilink")（さとる）
  - [銀色的歐林西斯](../Page/銀色的歐林西斯.md "wikilink")（ボルフ）
  - [數碼寶貝拯救隊](../Page/數碼寶貝拯救隊.md "wikilink")（**[亞古獸](../Page/亞古獸.md "wikilink")/暴龍獸/機械暴龍獸/閃光暴龍獸**、旁白）
  - [真仙魔大戰](../Page/真仙魔大戰.md "wikilink")（魔弾ラミバッカス）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（笹本安太郎 ※第439話）

**2007年**

  - [鬼太郎（第五作）](../Page/鬼太郎.md "wikilink")（さざえ鬼）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（カンパチーノ）
  - [金田一少年之事件簿SP](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（**金田一一**）

**2008年**

  - [鬼太郎（第五作）](../Page/鬼太郎.md "wikilink")（さら小僧）
  - [はたらキッズ マイハム組](../Page/はたらキッズ_マイハム組.md "wikilink")（浜村浩太）
  - [卡片鬥士翔](../Page/卡片鬥士翔.md "wikilink")（**間狩徹**、フライングパイン）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（希爾頓）

**2009年**

  - [犬夜叉 完結篇](../Page/犬夜叉.md "wikilink")（**鋼牙**）
  - [哆啦A夢 (第三版（2005年4月以後）)](../Page/哆啦A夢.md "wikilink")（ヒデキ）
  - [FRESH光之美少女！](../Page/FRESH光之美少女！.md "wikilink")（**塔多**）

**2010年**

  - [數碼寶貝合體戰爭](../Page/數碼寶貝合體戰爭.md "wikilink")（六翅獸）

**2012年**

  - [BRAVE10](../Page/BRAVE10.md "wikilink")（[鬼庭綱元](../Page/鬼庭綱元.md "wikilink")）
  - [料理偶像](../Page/料理偶像.md "wikilink")（瑪茲瑪茲）
  - [數碼獸合體戰爭](../Page/數碼獸合體戰爭.md "wikilink")（シャイングレイモン）
  - [探險托里蘭托](../Page/探險托里蘭托.md "wikilink")（ブリリアン）
  - [聖鬥士星矢Ω](../Page/聖鬥士星矢Ω.md "wikilink")（席勒）

**2013年**

  - [探検ドリランド -1000年の真宝](../Page/探險托里蘭托.md "wikilink")（♨っぴい）
  - ぢべたぐらし あひるの生活（あひる)

**2014年**

  - [金田一少年之事件簿R](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（**金田一一**）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（托雷波爾）

**2015年**

  - [デュエル・マスターズVSR](../Page/デュエル・マスターズ_\(漫画\).md "wikilink")（寿司屋の大将）
  - [ONE PIECE エピソードオブサボ 〜3兄弟の絆
    奇跡の再会と受け継がれる意志〜](../Page/ONE_PIECE_\(動畫\).md "wikilink")（托雷波爾）
  - [金田一少年之事件簿R （第2期）](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（**金田一一**）

### OVA

  - カナリア 〜この想いを歌に乗せて〜（八朔洋平）
  - カメレオン (漫画)（蜂屋未来）
  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")
    「黒魔術殺人事件」（**金田一一**）※20周年コミックス3・4巻DVD付き限定版\[1\]
  - [星界的戰旗III](../Page/星界的戰旗.md "wikilink")（クー・ドゥリン）
  - 同窓会 (ゲーム)（**久保達也**）
  - [炎之蜃氣樓 〜みなぎわの反逆者〜](../Page/炎之蜃氣樓.md "wikilink")（成田譲）

### 特攝

  - **天空忍者手裏劍者**（[忍風戰隊破裏劍者](../Page/忍風戰隊破裏劍者.md "wikilink")）
  - **デーボ・バティシエ**（[獸電戰隊強龍者](../Page/獸電戰隊強龍者.md "wikilink")）
  - [假面騎士Ghost](../Page/假面騎士Ghost.md "wikilink")（畫材眼魔）

### 遊戲

  - [真·三國無雙系列](../Page/真·三國無雙系列.md "wikilink")（[凌統](../Page/凌統.md "wikilink")（[4代開始](../Page/真·三國無雙4.md "wikilink")））、（[劉禪](../Page/劉禪.md "wikilink")（[6代開始](../Page/真·三國無雙6.md "wikilink")））
  - [無雙OROCHI系列](../Page/無雙OROCHI系列.md "wikilink")（[凌統](../Page/凌統.md "wikilink")、[劉禪](../Page/劉禪.md "wikilink")）
  - [純愛手札 Girl's Side Premium ～3rd
    Story～](../Page/心跳回憶_Girl's_Side_3rd_Story.md "wikilink")（蓮見達也）
  - [人中之龍
    登場！](../Page/人中之龍_登場！.md "wikilink")（[吉岡傳七郎](../Page/吉岡傳七郎.md "wikilink")）

### 劇場版

  - [金田一少年之事件簿2 殺戮的深藍](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（**金田一一**）
  - [×××HOLiC劇場版](../Page/×××HOLiC.md "wikilink")：仲夏夜之夢（房屋精靈）
  - [海綿寶寶](../Page/海綿寶寶.md "wikilink")（海綿寶寶）
  - [光之美少女 All Stars
    春之嘉年華♪](../Page/光之美少女_All_Stars_春之嘉年華♪.md "wikilink")（タルト）

### CD

  - [街 第Qの男
    〜QはquestionのQ〜](../Page/街_〜運命の交差点〜#ラジオドラマ.md "wikilink")（鯨井、消防士2）
  - [摩陀羅・転生編](../Page/魍魎戦記MADARAシリーズ.md "wikilink")（キリコ）
  - ドラマCD[異度傳說OUTER FILEVol](../Page/異域傳說系列.md "wikilink").1 - 3）
  - ドラマCD [生物性危害](../Page/生物性危害.md "wikilink") ドラマアルバム 〜運命のラクーンシティー(2)〜
  - ドラマCD [狂野歷險](../Page/狂野歷險.md "wikilink")（ハンペン）
  - 劇団[隔世遺伝](../Page/隔世遺伝_\(劇団\).md "wikilink") 童話CD「でんでん」
  - [DARK EDGE](../Page/DARK_EDGE.md "wikilink") はじまりの予鈴（吉国鉱一）
  - [「金田一少年の事件簿」オールスターズ・ソングアルバム\~THE SCHOOL
    DAYS](../Page/金田一少年之事件簿.md "wikilink")（金田一一）

## 参考资料

## 外部連結

  - [事務所公開簡歷](http://www.aoni.co.jp/actor/ma/matsuno-taiki.html)
  - [個人網誌－ゾクッ太紀汚染](http://blog.livedoor.jp/taikeymania/)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:青二Production](../Category/青二Production.md "wikilink")

1.