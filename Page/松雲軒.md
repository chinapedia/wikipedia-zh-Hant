**松雲軒**是[台灣第一家](../Page/台灣.md "wikilink")[印刷機構](../Page/印刷.md "wikilink")，出現於[清治](../Page/台灣清治時期.md "wikilink")[道光初年](../Page/道光.md "wikilink")，位於[臺灣府城上橫街統領巷](../Page/臺灣府城.md "wikilink")，據[石暘睢之考察](../Page/石暘睢.md "wikilink")，即今[陳德聚堂左邊](../Page/陳德聚堂.md "wikilink")。

## 沿革

松雲軒約是在道光初年，由[盧崇玉創立於臺灣府城](../Page/盧崇玉.md "wikilink")，使用的技術是[雕版印刷](../Page/雕版印刷.md "wikilink")。其營業項目主要是印刷[善書經文](../Page/善書.md "wikilink")，但也有出版治臺輿圖、科考範文、詩文集、童蒙讀本、譜牒籤詩等物。

松雲軒在[臺灣日治時期大正年間仍繼續營業](../Page/臺灣日治時期.md "wikilink")，此時經營者為盧乙。盧乙除了是松雲軒的經營者外，他也是西來庵意誠堂的正鸞生，在[西來庵事件中](../Page/西來庵事件.md "wikilink")，[余清芳即是利用他所印刷的善書來暗中宣傳抗日思想](../Page/余清芳.md "wikilink")。余清芳的抗日行動失敗後，盧乙也受到波及，之後最晚在大正十年（1922年）前他便將松雲軒的書版轉讓給高砂町昌仁堂的店主黃來成。後來由於[第二次世界大戰的緣故](../Page/第二次世界大戰.md "wikilink")，這些書版在昭和二十年（1945年）3月時毀於臺南市區的轟炸之中。

## 出版品

松雲軒刻印坊曾經刻印有：《[高王真經](../Page/高王真經.md "wikilink")》、《[金剛經注解](../Page/金剛經注解.md "wikilink")》、《[白衣神咒](../Page/白衣神咒.md "wikilink")》、《[文亭文集](../Page/文亭文集.md "wikilink")》、《[三字經](../Page/三字經.md "wikilink")》、《[潘公免災寶卷](../Page/潘公免災寶卷.md "wikilink")》、《[澄懷園唱和集](../Page/澄懷園唱和集.md "wikilink")》、《[一肚皮集](../Page/一肚皮集.md "wikilink")》等。

## 參考資料

  - 辛廣偉，2000年，《台灣出版史》，[河北教育出版社](../Page/河北教育出版社.md "wikilink")

[S松](../Category/臺灣已結業出版社.md "wikilink")
[Category:台灣清治時期文化](../Category/台灣清治時期文化.md "wikilink")
[Category:台灣日治時期組織](../Category/台灣日治時期組織.md "wikilink")
[Category:台灣之最](../Category/台灣之最.md "wikilink")
[Category:台南市歷史](../Category/台南市歷史.md "wikilink")
[Category:中西區 (臺灣)](../Category/中西區_\(臺灣\).md "wikilink")