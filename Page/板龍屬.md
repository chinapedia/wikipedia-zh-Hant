**板龍属**（[屬名](../Page/屬.md "wikilink")：）是[蜥臀目](../Page/蜥臀目.md "wikilink")[蜥脚形亚目植食性](../Page/蜥脚形亚目.md "wikilink")[恐龙的一属](../Page/恐龙.md "wikilink")。板龍生存於晚[三疊紀](../Page/三疊紀.md "wikilink")[諾利階到](../Page/諾利階.md "wikilink")[瑞提階的](../Page/瑞提階.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，約2億1600萬年前到1億9900萬年前。目前已有兩個已承認種：**恩氏板龍**（*P.
engelhardti*）與**長頭板龍**（*P. longiceps*），過去曾經還有其他的種，但已被歸類於其他屬。

板龍的化石發現於1834年，並在1837年被敘述，牠們是最早被命名的恐龍之一，但不包含在最早用來定義為[恐龍總目的三個屬之中](../Page/恐龍總目.md "wikilink")，因為當時對於板龍的了解有限。

板龍是體積龐大的二足、[草食性恐龍](../Page/草食性.md "wikilink")，擁有小頭部、長頸部、用來壓碎植物的銳利牙齒，前肢明显较小但有大型[拇趾尖爪](../Page/拇趾.md "wikilink")，這些尖爪可能用來防衛與幫助進食。

## 敘述

[Human-plateosaurus_size_comparison(V2).png](https://zh.wikipedia.org/wiki/File:Human-plateosaurus_size_comparison\(V2\).png "fig:Human-plateosaurus_size_comparison(V2).png")
[Plateosaurus_skull.jpg](https://zh.wikipedia.org/wiki/File:Plateosaurus_skull.jpg "fig:Plateosaurus_skull.jpg")
板龍是已知最大的[三叠纪恐龙](../Page/三叠纪.md "wikilink")，也是三叠纪最大的陆生动物，身長可達6到10公尺，\[1\]
體重估計有7頓。板龍屬於[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，該下目是一群早期[草食性恐龍](../Page/草食性.md "wikilink")；板龍的體型比其他類似動物還要強壯，例如[近蜥龍](../Page/近蜥龍.md "wikilink")。板龍擁有長頸部，由9個[頸椎所構成](../Page/頸椎.md "wikilink")，身體結實而呈[梨狀](../Page/梨.md "wikilink")。板龍的尾巴由至少14個[尾椎所構成](../Page/尾椎.md "wikilink")，可作為長頸部與前部身體的平衡工具。

板龍的[頭顱骨比大多數](../Page/頭顱骨.md "wikilink")[原蜥腳類恐龍還要堅固](../Page/原蜥腳類.md "wikilink")、縱深；但是與板龍的身體大小相比時，仍然小型、狹窄。板龍的頭顱骨有4對洞孔；包括鼻孔、[眶前孔](../Page/眶前孔.md "wikilink")、眼眶、[下顳孔](../Page/下顳孔.md "wikilink")。板龍擁有長口鼻部，許多小型、葉狀、位在齒槽中的牙齒，頜部關節的位置低，可使下頜提供肌肉更大的力量，以上特徵顯示板龍只以植物為食。\[2\]
牠們的[眼睛朝向兩側](../Page/眼睛.md "wikilink")，而非前方，形成全範圍的視線範圍，可警戒、注意掠食者。有些化石保存了[鞏膜環](../Page/鞏膜環.md "wikilink")。

板龍的上頜與下頜擁有許多小型牙齒，[前上頜骨有](../Page/前上頜骨.md "wikilink")5到6顆，[上頜骨有](../Page/上頜骨.md "wikilink")24到30顆，[齒骨上有](../Page/齒骨.md "wikilink")21到28顆。這些牙齒有鋸齒狀、葉狀的齒冠，適合消化植物。板龍被認為擁有狹窄的頰囊，可避免食物在進食時溢出嘴部。

## 發現與種

[Plateosaurus_ottoneum.jpg](https://zh.wikipedia.org/wiki/File:Plateosaurus_ottoneum.jpg "fig:Plateosaurus_ottoneum.jpg")[卡塞爾的](../Page/卡塞爾.md "wikilink")[奧托諾伊姆自然歷史博物館](../Page/奧托諾伊姆自然歷史博物館.md "wikilink")\]\]
在1834年，[物理學家約翰](../Page/物理學家.md "wikilink")·腓特烈·恩格爾哈特（Johann Friedrich
Engelhardt）在[德國](../Page/德國.md "wikilink")[紐倫堡附近的Heroldsberg發現了一些](../Page/紐倫堡.md "wikilink")[脊椎與腿部骨頭](../Page/脊椎.md "wikilink")。三年後，[德國](../Page/德國.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[汪邁爾](../Page/汪邁爾.md "wikilink")（Christian
Erich Hermann von
Meyer），根據這些化石建立了新屬，**板龍**（*Plateosaurus*），模式種是**恩氏板龍**（*P.
engelhardti*）。\[3\][属名在](../Page/属.md "wikilink")[希臘文的意思是](../Page/希臘文.md "wikilink")“平坦表面的蜥蜴”，\[4\]
意指牠們的平坦骨頭；而種名是以發現者恩格爾哈特為名。\[5\]

在1910年代到30年代期間，[薩克森-安哈特的一個](../Page/薩克森-安哈特.md "wikilink")[黏土礦坑挖掘出了](../Page/黏土.md "wikilink")32到45個骨骸，這些化石屬於板龍、[理理恩龍](../Page/理理恩龍.md "wikilink")、以及[敏捷龍](../Page/敏捷龍.md "wikilink")。有些化石被歸類於板龍的第二個種，長頸龍（*P.
longiceps*），是由Otto Jaekel在1913年所敘述，\[6\]
同一時期在[特洛辛根的](../Page/特洛辛根.md "wikilink")[屍骨層也發現了數個板龍化石](../Page/屍骨層.md "wikilink")，但大部分被歸類於可疑種或無效種。\[7\]

在1997年，[北海北端Snorre油田的鑽油工人在探鑽](../Page/北海_\(大西洋\).md "wikilink")[砂岩層時](../Page/砂岩.md "wikilink")，因為碰到一個長圓柱狀[岩芯](../Page/岩芯.md "wikilink")（Drillcore）而受到阻礙，而停在海平面以下2,256公尺，該長圓柱狀岩石是個化石，在當時被認為是個[植物化石](../Page/植物.md "wikilink")。在2003年，這個標本被交給[奧斯陸大學的](../Page/奧斯陸大學.md "wikilink")[古生物學家Jørn](../Page/古生物學家.md "wikilink")
Harald
Hurum。[波恩大學的古生物學家使用](../Page/波恩大學.md "wikilink")[顯微鏡檢驗這個化石](../Page/顯微鏡.md "wikilink")，發現這化石擁有纖維化的骨頭組織，為一個壓碎的膝蓋骨化石，並鑑定屬於板龍，\[8\]
並使牠們成為[北海第一個發現的恐龍](../Page/北海_\(大西洋\).md "wikilink")，並被譽為「世界最深的恐龍」。\[9\]

在2007年8月，一個業餘古生物學家在[瑞士Frick自治市附近發現了一大群化石](../Page/瑞士.md "wikilink")，包含約300個骨頭，由兩個板龍個體所組成。波恩大學的古生物學家Martin
Sander指出這個挖掘地點的範圍可達1.5公里，是歐洲最大型的化石挖掘地點。該地點被估計為每平方公尺有一個恐龍化石。\[10\]

## 分類

板龍是第一個被敘述的原蜥腳下目恐龍，並為[板龍科的](../Page/板龍科.md "wikilink")[模式屬](../Page/模式屬.md "wikilink")，也是板龍科的名稱來源。板龍最初的所知有限，並被分類到[蜥類](../Page/蜥類.md "wikilink")（Sauria），可能是任何一種[爬行動物](../Page/爬行動物.md "wikilink")。\[11\]
在1845年，汪邁爾建立了Pachypodes生物群，包含板龍、[禽龍](../Page/禽龍.md "wikilink")、[斑龍](../Page/斑龍.md "wikilink")、[林龍](../Page/林龍.md "wikilink")。\[12\]
但是[理查·歐文已經建立了](../Page/理查·歐文.md "wikilink")[恐龍總目](../Page/恐龍總目.md "wikilink")，包含了後三屬，範圍與Pachypodes相同。

在1895年，[奧塞內爾·查利斯·馬什](../Page/奧塞內爾·查利斯·馬什.md "wikilink")（Othniel Charles
Marsh）提出了板龍科，並置於[獸腳亞目內](../Page/獸腳亞目.md "wikilink")。\[13\]
之後由汪邁爾重新分類到[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，\[14\]
並被大多數研究人員所接受。\[15\]\[16\]
許多年來，板龍科[演化支僅包括板龍屬](../Page/演化支.md "wikilink")，但最近有兩個屬被歸類到此科：[鞍龍](../Page/鞍龍.md "wikilink")、\[17\][黑水龍](../Page/黑水龍.md "wikilink")。\[18\]

## 種

[DSC01708_Scheletro_dinosauro_-_Museo_di_storia_naturale,_Milano_-_Foto_di_G._Dall'Orto_-_20-12-2006.jpg](https://zh.wikipedia.org/wiki/File:DSC01708_Scheletro_dinosauro_-_Museo_di_storia_naturale,_Milano_-_Foto_di_G._Dall'Orto_-_20-12-2006.jpg "fig:DSC01708_Scheletro_dinosauro_-_Museo_di_storia_naturale,_Milano_-_Foto_di_G._Dall'Orto_-_20-12-2006.jpg")

### 有效種

板龍屬目前只有兩個有效種。[模式種是恩氏板龍](../Page/模式種.md "wikilink")（*P.
engelhardti*），化石有至少10個頭顱骨，以及超過100個骨骸，狀態有破碎的也有完整的，主要發現於[德國](../Page/德國.md "wikilink")[巴伐利亞州](../Page/巴伐利亞州.md "wikilink")。第二個種為長頭板龍（*P.
longiceps*），是較為了解的種，在德國Knollenmergel發現了50個標本。長頭板龍與恩氏板龍的差別在於，較長的口鼻部與四肢，身體結構較輕型。\[19\]

### 重新歸類種

第三個種是纖細板龍（*P.
gracilis*），但缺乏板龍的特定特徵，已經成立了個別的[鞍龍屬](../Page/鞍龍.md "wikilink")。
\[20\] 其他的種包含：特洛辛根板龍（*P. trossingensis*）、弗氏板龍（*P. fraasianus*）、整體板龍（*P.
integer*），都已經被列為長頭板龍的異名。\[21\]

### 可疑種

數個板龍的種因為化石的破碎與保存不佳狀態，而被認為是[無資格名稱](../Page/無資格名稱.md "wikilink")（Nomina
nuda）或[疑名](../Page/疑名.md "wikilink")。例如厄倫堡板龍（*P.
erlenbergiensis*）的[正模標本缺乏](../Page/正模標本.md "wikilink")[薦骨](../Page/薦骨.md "wikilink")、[腸骨](../Page/腸骨.md "wikilink")、[恥骨近側](../Page/恥骨.md "wikilink")，因此很難與其他種作比較。
\[22\]

## 發現處

板龍是最著名的[原蜥腳下目恐龍](../Page/原蜥腳下目.md "wikilink")，也是[歐洲最常見的恐龍之一](../Page/歐洲.md "wikilink")，已在[西歐超過](../Page/西歐.md "wikilink")50個[三疊紀](../Page/三疊紀.md "wikilink")[砂岩層發現了板龍化石](../Page/砂岩.md "wikilink")，包含超過100個標本，與數十個保存良好的骨骸。板龍的化石主要屬於長頭板龍，已在[德國南部](../Page/德國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、以及[格陵蘭等地發現](../Page/格陵蘭.md "wikilink")；而恩氏板龍僅發現於德國[巴伐利亞州](../Page/巴伐利亞州.md "wikilink")。\[23\]
在格陵蘭Flemming Fjord地層發現的標本最初被認為屬於恩氏板龍，\[24\] 但後來的研究將牠們歸類於長頭板龍。\[25\]

## 古生態學

板龍發現於[歐洲](../Page/歐洲.md "wikilink")，該地區在三疊紀時期的氣候環境類似[沙漠](../Page/沙漠.md "wikilink")。
在某些地點，發現了由完整個體構成的化石群。這顯示板龍以群體行動，穿越[三疊紀乾旱](../Page/三疊紀.md "wikilink")、類似沙漠的[歐洲地區以尋找新的食物來源](../Page/歐洲.md "wikilink")。

另一個對於這化石群的解釋是，個別的板龍居住於乾燥的高地上，當牠們死亡時，沙漠環境中的間歇性洪水將遺體沖刷、堆積在一起。許多個體的屍體在沙漠低處邊緣的河道末端堆積起來。\[26\]

板龍與以下動物生存於同一地區：[原蜥腳下目的](../Page/原蜥腳下目.md "wikilink")[鞍龍](../Page/鞍龍.md "wikilink")、中等體型的[獸腳亞目](../Page/獸腳亞目.md "wikilink")[理理恩龍與](../Page/理理恩龍.md "wikilink")[敏捷龍](../Page/敏捷龍.md "wikilink")、小型的[主龍類](../Page/主龍類.md "wikilink")[原美頜龍](../Page/原美頜龍.md "wikilink")、已知最早的[烏龜](../Page/烏龜.md "wikilink")[原頜龜](../Page/原頜龜.md "wikilink")、[兩棲類的](../Page/兩棲類.md "wikilink")[離片錐目](../Page/離片錐目.md "wikilink")、[堅蜥目](../Page/堅蜥目.md "wikilink")、原始[哺乳類](../Page/哺乳類.md "wikilink")、[翼龍目](../Page/翼龍目.md "wikilink")、[喙頭蜥目](../Page/喙頭蜥目.md "wikilink")、以及[魚類](../Page/魚類.md "wikilink")。\[27\]

## 古生物學

由於板龍的化石少、不完整，過去一個世紀很少板龍的研究。直到近年的新發現化石，才讓科學家深入研究板龍的移動方式、食性、代謝。

### 食性

板龍的小型、葉狀牙齒顯示牠們為[草食性](../Page/草食性.md "wikilink")，並且為最早的大型草食性恐龍之一，以高大[植被為食](../Page/植被.md "wikilink")，例如[針葉樹與](../Page/針葉樹.md "wikilink")[蘇鐵](../Page/蘇鐵.md "wikilink")。如同牠們的近親[大椎龍](../Page/大椎龍.md "wikilink")，板龍可能吞食[胃石以協助消化食物](../Page/胃石.md "wikilink")，因為牠們缺乏咀嚼用[頰齒](../Page/頰齒.md "wikilink")。

### 姿勢與步態

[Muja_01.jpg](https://zh.wikipedia.org/wiki/File:Muja_01.jpg "fig:Muja_01.jpg")阿斯圖里亞斯侏羅紀博物館\]\]
如同所有的原蜥腳下目恐龍，板龍的前肢比後肢短許多，而且牠們擁有明顯的手指，以及[拇指尖爪](../Page/拇指.md "wikilink")。一個對於板龍前肢的生理構造研究，證實前肢的運動範圍，排除了板龍習慣於四足步態的可能。如同獸腳亞目，板龍與其他相關的原蜥腳類不能旋轉牠們的手掌，所以牠們的手掌往下垂，而且不能以前肢支撐重量或行走。這個研究避免了關於板龍有限的轉動手掌能力爭議，而直接排除了板龍以指關節觸地行走與其他形式的運動方式的可能性。\[28\]
所以，雖然板龍的體型可能屬於四足動物，但牠們的前肢結構限制牠們以後腿行走移動。牠們的前肢可能用來進食時降低樹肢、用來抓取、或用來防禦。\[29\]
板龍的手部骨頭非常大，有五個手指。第四、第五指非常小。

### 代謝

一個最近針對化石沉積層的研究，顯示板龍的個體間有非常大的體型差異。\[30\]
而且，骨頭上的生長層顯示個體的成長週期變化與所處環境相關。有些板龍在12歲時達到最大體型，而其他的則可以成長到30多歲。成年標本的大小也有不同，有些成年體的身長為4到6公尺，而其他的則可達到10公尺。板龍的標本眾多，足以研究牠們骨頭的[組織學](../Page/組織學.md "wikilink")；但因為缺乏身長小於4.8公尺的個體標本，所以很難研究板龍的[個體發生學](../Page/個體發生學.md "wikilink")。如同其他恐龍，板龍擁有高成長速率，顯示牠們有進階的恐龍生理，但值得注意的是牠們嚴重受到環境影響。這個研究的作者假設板龍的新陳代謝介於[冷血動物與](../Page/冷血動物.md "wikilink")[溫血動物之間](../Page/溫血動物.md "wikilink")。

## 大眾文化

[Plateosaurus.jpg](https://zh.wikipedia.org/wiki/File:Plateosaurus.jpg "fig:Plateosaurus.jpg")[巴塞爾](../Page/巴塞爾.md "wikilink")\]\]
板龍曾短暫出現於電影《[歷險小恐龍 2](../Page/歷險小恐龍.md "wikilink")》（The Land Before Time
II: The Great Valley Adventure）的開場段落。

另外，板龍也在[BBC的電視節目](../Page/BBC.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（Walking
with Dinosaurs）第一集中短暫出現，用來說明恐龍的成功。

可於[杜賓根大學](../Page/杜賓根大學.md "wikilink")、[柏林](../Page/柏林.md "wikilink")[亨波特博物館](../Page/亨波特博物館.md "wikilink")、以及[斯圖加特的自然歷史博物館等地看到已架設的板龍骨骸](../Page/斯圖加特.md "wikilink")。

板龍也出現於[微軟的遊戲](../Page/微軟.md "wikilink")《動物園大亨：侏羅紀》（Zoo Tycoon: Dinosaur
Digs），板龍在遊戲裡是一隻可抚養的恐龍。

## 參考資料

## 外部链接

  - [Enchanted
    learning](http://www.enchantedlearning.com/subjects/dinosaurs/dinos/Plateosaurus.shtml)
  - [Dinosauricon](https://web.archive.org/web/20050512074013/http://dinosauricon.com/genera/plateosaurus.html)
  - [DinoData](http://www.dinodata.net/Dd/Namelist/Tabp/P093.htm)

[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:原蜥腳下目](../Category/原蜥腳下目.md "wikilink")
[Category:三疊紀恐龍](../Category/三疊紀恐龍.md "wikilink")

1.  Sander, M., and Klein, N. (2005). Developmental plasticity in the
    life history of a prosauropod dinosaur. *Science* 16 December
    2005:1800-1802.

2.  Cox, Barry, R.J.G. Savage, and others. (1999). Encyclopedia of
    Dinosaurs & prehistoric creatures. 14 July 2007:pg 124.

3.  Von Meyer, H. (1837). Mitteilung an Prof. Bronn (*Plateosaurus
    engelhardti*). *Neues Jahrbuch für Geologie und Paläontologie*
    **316**. \[German\]

4.

5.
6.  Jaekel, O. (1913-14). Über die Wirbeltierfunde in der oberen Trias
    von Halberstadt. *Paläontologische Zeitschrift* **1**:155-215.
    \[German\]

7.
8.  Hurum, J.H., Bergan, M., Müller, R., Nystuen, J.P., and Klein, N.
    (2006). A Late Triassic dinosaur bone, offshore Norway. *Norwegian
    Journal of Geology* **86**:117-123.

9.  <http://news.nationalgeographic.com/news/2006/04/0426_060426_dinos.html>

10. <http://www.cbc.ca/technology/story/2007/08/10/dinosaur-switzerland.html>

11.
12. H. v. Meyer. 1845. System der fossilen Saurier \[Taxonomy of fossil
    saurians\]. *Neues Jahrbuch für Mineralogie, Geognosie, Geologie und
    Petrfakten-Kunde* **1845**:278-285

13. O. C. Marsh. 1895. On the affinities and classification of the
    dinosaurian reptiles. *American Journal of Science* 50(300):483-498

14. F. v. Huene. 1926. On several known and unknown reptiles of the
    order Saurischia from England and France. *Annals and Magazine of
    Natural History, series 9* **17**:473-489

15. C.-C. Young. 1941. A complete osteology of Lufengosaurus huenei
    Young (gen. et sp. nov.) from Lufeng, Yunnan, China. *Palaeontologia
    Sinica, New Series C, Whole Series No. 121* **7**:1-59

16. J. F. Bonaparte. 1971. Los tetrápodos del sector superior de la
    Formación Los Colorados, La Rioja, Argentina (Triásico Superior)
    \[The tetrapods of the upper part of the Los Colorados Formation, La
    Rioja, Argentina (Upper Triassic)\]. *Opera Lilloana* **22**:1-183

17. F. v. Huene. 1905. Trias-Dinosaurier Europas \[European Triassic
    dinosaurs\]. *Deutschen Geologischen Gesellschaft*
    **1905(17)**:345-349

18. L. A. Leal, S. A. K. Azevedo, A. W. A. Kellner and A. A. S. Da Rosa.
    2004. A new early dinosaur (Sauropodomorpha) from the Caturrita
    Formation (Late Triassic), Paraná Basin, Brazil. Zootaxa
    **690**:1-24

19.
20.
21. P. M. Galton. 2001. The prosauropod dinosaur Plateosaurus Meyer,
    1837 (Saurischia: Sauropodomorpha; Upper Triassic). II. Notes on the
    referred species. *Revue Paléobiologie, Genève* **20(2)**:435-502

22.
23.

24. F. A. Jenkins, Jr., N. H. Shubin, W. W. Amaral, S. M. Gatesy, C. R.
    Schaff, L. B. Clemmensen, W. R. Downs, A. R. Davidson, N. Bonde and
    F. Osbaeck. 1994. Late Triassic continental vertebrates and
    depositional environments of the Fleming Fjord Formation, Jameson
    Land, East Greenland. *Meddelelser om Grønland, Geoscience*
    **32**:1-25

25.
26.
27. Lucas, S. G., 1998. Global Triassic tetrapod biostratigraphy and
    biochronology. *Palaeogeography, Palaeoclimatology, Palaeoecology*
    **143**: 347-384.

28. Bonnan M, Senter P. (2007). "Were the basal sauropodomorph dinosaurs
    *Plateosaurus* and *Massospondylus* habitual quadrupeds?"; pp.
    139-155 in Barrett, P.M. and Batten, D.J. (eds.), *Evolution and
    Palaeobiology of Early Sauropodomorph Dinosaurs*. *Special Papers in
    Palaeontology* **77**.

29.
30.