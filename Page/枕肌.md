**枕肌**（Occipitalis
muscle）是一塊薄薄的人體[肌肉](../Page/肌肉.md "wikilink")，呈四邊形。其開端於[枕骨](../Page/枕骨.md "wikilink")[上項線兩側三分之二處的腱纖維和](../Page/上項線.md "wikilink")[顳部的乳突部分](../Page/顳部.md "wikilink")，而其終端於[帽狀腱膜中](../Page/帽狀腱膜.md "wikilink")。

某些資料並不列枕肌自身為一塊肌肉，而是[枕額肌的其中一部分](../Page/枕額肌.md "wikilink")。

## 圖片

<File:Occipitalis> muscle animation small.gif|枕肌的部分(顯示為紅色)

## 參見

  - [額肌](../Page/額肌.md "wikilink")
  - [枕額肌](../Page/枕額肌.md "wikilink")

## 外部連結

  - [解剖學(Anatomy)-肌肉(muscle)-Muscles of Facial
    Expression(顏面表情肌)](http://smallcollation.blogspot.com/2013/02/anatomy-muscle-muscles-of-facial.html)

  - [PTCentral](https://web.archive.org/web/20050804024427/http://www.ptcentral.com/muscles/musclehead.html#occipitalis)

  -
[Category:头颈肌肉](../Category/头颈肌肉.md "wikilink")