<table>
<tbody>
<tr class="odd">
<td><table>
<caption><font size="+1"><strong>林兆枢</strong></font></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>职位</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>任职时段</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>前任</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>继任</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>出生时间</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>出生地点</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/政党.md" title="wikilink">政党</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

**林兆枢**（），原[中華全國歸國華僑聯合會主席](../Page/中華全國歸國華僑聯合會.md "wikilink")。

## 官方简历

林兆枢，男，1940年2月出生，福建福州人。

1956年毕业于[福州第三中学](../Page/福州第三中学.md "wikilink")。

1959年毕业于[福建省建筑工程学校](../Page/福建省建筑工程学校.md "wikilink")，现为[福建工程学院](../Page/福建工程学院.md "wikilink")。

1960年加入中国共产党。

1992年出任[中國共產黨中央紀律檢查委員會委員](../Page/中國共產黨中央紀律檢查委員會.md "wikilink")、中共福建省纪委书记。

1995年10月至2001年12月任中共福建省委副书记

1999年7月当选第六届中国侨联主席

2003年3月當選為十屆[全國人大常委會委員](../Page/全國人大常委會.md "wikilink")、[全國人大華僑委員會副主任委員](../Page/全國人大.md "wikilink")

2004年7月當選第七屆中國僑聯主席

2008年3月当选为十一届[全国政协常委](../Page/全国政协.md "wikilink")，第十一届[全国政协港澳台侨委员会副主任](../Page/全国政协.md "wikilink")

## 参考文献

  - [林兆枢简历，新华网，于2011-10-23查阅](http://news.xinhuanet.com/ziliao/2002-02/28/content_295201.htm)

[Z兆](../Category/林姓.md "wikilink") [L林](../Category/福州人.md "wikilink")
[Category:中共福建省纪委书记](../Category/中共福建省纪委书记.md "wikilink")
[Category:第十届全国人大常委会委员](../Category/第十届全国人大常委会委员.md "wikilink")
[Category:第十一届全国政协常委会组成人员](../Category/第十一届全国政协常委会组成人员.md "wikilink")