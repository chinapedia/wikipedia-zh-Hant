**林全**（），[臺灣學者](../Page/臺灣.md "wikilink")、[無黨籍](../Page/無黨籍.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，現任[總統府資政](../Page/總統府資政.md "wikilink")，生於[高雄](../Page/高雄.md "wikilink")[左營](../Page/左營.md "wikilink")[海軍](../Page/海軍.md "wikilink")[眷村自治新村](../Page/眷村.md "wikilink")\[1\]，[外省第二代](../Page/外省.md "wikilink")，籍貫[江蘇](../Page/江蘇省_\(中華民國\).md "wikilink")[淮安](../Page/淮安.md "wikilink")\[2\]。[經濟學者出身](../Page/經濟.md "wikilink")，具[貨幣學](../Page/貨幣學.md "wikilink")、[財政學專業](../Page/財政學.md "wikilink")，曾任[臺大](../Page/臺大.md "wikilink")[教授](../Page/教授.md "wikilink")。

林全出身[深藍家庭](../Page/泛藍.md "wikilink")，原為知名學者，自[陳水扁任](../Page/陳水扁.md "wikilink")[臺北市長時](../Page/臺北市長.md "wikilink")，出掌[臺北市政府財政局局長](../Page/臺北市政府財政局.md "wikilink")，直到[2000年中華民國總統選舉為陳水扁草擬](../Page/2000年中華民國總統選舉.md "wikilink")「財經白皮書」，從實務到理念，被視為[民主進步黨核心](../Page/民主進步黨.md "wikilink")[財經智囊](../Page/財經.md "wikilink")\[3\]。

曾任[行政院院長](../Page/行政院院長.md "wikilink")、行政院[主計長](../Page/主計長.md "wikilink")、[財政部部長](../Page/中華民國財政部.md "wikilink")\[4\]，亦曾任世界先進積體電路公司[董事長及](../Page/董事長.md "wikilink")[智庫執行長](../Page/智庫.md "wikilink")。

## 經歷

林全為[臺灣外省人第二代](../Page/臺灣外省人.md "wikilink")，來自[深藍家庭](../Page/泛藍.md "wikilink")\[5\]，籍貫中華民國江蘇省。

林全的[父親](../Page/父親.md "wikilink")[林守一是江蘇](../Page/林守一.md "wikilink")[淮陰人](../Page/淮陰.md "wikilink")，[1949年與妻子](../Page/1949年.md "wikilink")[林許永清自淪陷的](../Page/林許永清.md "wikilink")[上海市緊急撤離](../Page/上海市.md "wikilink")，[移民至臺灣後](../Page/移民.md "wikilink")，加入[中華民國海軍](../Page/中華民國海軍.md "wikilink")，直到海軍[中校退役](../Page/中校.md "wikilink")\[6\]。

林全出生於[高雄市](../Page/高雄市.md "wikilink")[左營區的中華民國](../Page/左營區.md "wikilink")[海軍](../Page/海軍.md "wikilink")[眷村自治新村](../Page/眷村.md "wikilink")，\[7\]先後畢業於中華民國海軍子弟學校（今「高雄市立永清國小」）、海青初級中學（今「[海青工商](../Page/高雄市立海青高級工商職業學校.md "wikilink")」）、[省立左營高中](../Page/高雄市立左營高級中學.md "wikilink")、[天主教輔仁大學](../Page/天主教輔仁大學.md "wikilink")[經濟學系學士](../Page/經濟學.md "wikilink")、[國立政治大學](../Page/國立政治大學.md "wikilink")[財政](../Page/財政.md "wikilink")[研究所](../Page/研究所.md "wikilink")[碩士](../Page/碩士.md "wikilink")、[美國](../Page/美國.md "wikilink")[伊利諾大學經濟學](../Page/伊利諾大學.md "wikilink")[博士](../Page/博士.md "wikilink")\[8\]\[9\]\[10\]。

曾任[中華經濟研究院副研究員](../Page/中華經濟研究院.md "wikilink")\[11\]、[財政部](../Page/財政部.md "wikilink")「賦稅改革委員會」[顧問](../Page/顧問.md "wikilink")、[國立政治大學財政學系暨財政研究所](../Page/國立政治大學.md "wikilink")[教授](../Page/教授.md "wikilink")、[國立臺灣大學經濟學系專任教授](../Page/國立臺灣大學.md "wikilink")、[建築暨城鄉研究所兼任教授](../Page/建築.md "wikilink")、[臺北市政府財政局局長](../Page/臺北市政府.md "wikilink")、[行政院](../Page/行政院.md "wikilink")[主計長](../Page/主計長.md "wikilink")、[財政部部長](../Page/財政部.md "wikilink")\[12\]、世界先進積體電路股份有限公司[董事長](../Page/董事長.md "wikilink")。\[13\]現任國立臺灣大學經濟學系兼任教授。

2016年，成為候任總統[蔡英文任命的](../Page/蔡英文.md "wikilink")「政權交接小組」召集人之一\[14\]，後接受蔡英文的邀請出任行政院院長，定位為「財經內閣」、「改革內閣」，力拚臺灣[經濟發展](../Page/經濟發展.md "wikilink")、[產業升級](../Page/產業.md "wikilink")。\[15\]\[16\]\[17\]\[18\]

2018年1月，接任東洋藥品董事長\[19\]。

## 觀點主張

  - 2015年12月14日，時任民進黨智庫[新境界文教基金會執行長林全支持開放](../Page/新境界文教基金會.md "wikilink")[台積電到江蘇南京成立](../Page/台積電.md "wikilink")12吋[晶圓廠](../Page/晶圓.md "wikilink")。\[20\]2016年2月3日[投審會正式核可台積電申請到中國南京設立生產主流](../Page/投審會.md "wikilink")16[奈米矽晶圓廠](../Page/奈米.md "wikilink")。\[21\]

<!-- end list -->

  - 2015年12月14日，針對中國大陸[紫光集團](../Page/紫光集團.md "wikilink")12月11日擬收購[矽品精密等三家IC封裝測試大廠](../Page/矽品精密.md "wikilink")，林全表示，紫光負責人對併購案的發言逾越了企業應有的角色，這種不恰當發言出現後再來談併購，氣氛不對，建議經濟部應暫緩審查此投資案。\[22\]同日，[日月光正式向矽品提出合意併購邀約](../Page/日月光.md "wikilink")，向矽品董事會提議現金收購矽品百分之百股權。\[23\]2016年4月28日，矽品宣布終止紫光集團參與私募案。\[24\]

<!-- end list -->

  - 2017年6月24日，林全接受[日本經濟新聞專訪時表示](../Page/日本經濟新聞.md "wikilink")，台灣不會以加入某一個區域的自由貿易為唯一目標，而是平等看待，「包含過去所看到的[ECFA的簽訂](../Page/ECFA.md "wikilink")，或是其他更進一步的去降低雙方貿易障礙的措施，從經濟互惠原則來看，都是好的」。\[25\]

## 行政院長

### 任內民調

| 行政院長林全施政表現民調\[26\]                                                                                                                                                                                                                                                                                              |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 民調來源                                                                                                                                                                                                                                                                                                            |
| [台灣指標](http://www.tisr.com.tw/?p=6812)                                                                                                                                                                                                                                                                          |
| [趨勢](http://www.taiwanthinktank.org/chinese/page/5/61/3139/0)                                                                                                                                                                                                                                                   |
| [台灣指標](http://www.tisr.com.tw/?p=6889)                                                                                                                                                                                                                                                                          |
| [TVBS](https://web.archive.org/web/20160827003847/http://other.tvbs.com.tw/export/sites/tvbs/file/other/poll-center/0506141.pdf)                                                                                                                                                                                |
| [蘋果日報](http://www.appledaily.com.tw/realtimenews/article/politics/20160618/888659/applesearch/%E3%80%90%E5%B0%B1%E8%81%B7%E6%BB%BF%E6%9C%88%E3%80%91%E3%80%8A%E8%98%8B%E6%9E%9C%E3%80%8B%E6%B0%91%E8%AA%BF%E3%80%80%E8%94%A1%E8%8B%B1%E6%96%87%E6%BB%BF%E6%84%8F%E5%BA%A654.17%E3%80%80%E6%9E%97%E5%85%A845.89) |
| [趨勢](http://www.taiwanthinktank.org/chinese/page/5/61/3143/0)                                                                                                                                                                                                                                                   |
| [艾普羅](https://www.youtube.com/watch?v=JnLJsAYGHH0)                                                                                                                                                                                                                                                              |
| [壹電視](http://www.nexttv.com.tw/news/realtime/politics/11591067/%E5%B0%B1%E8%81%B7%E6%BB%BF%E6%9C%88%EF%BC%81%E5%B0%8F%E8%8B%B1%E6%BB%BF%E6%84%8F%E5%BA%A658.2%EF%BC%85%E3%80%80%E6%9E%97%E5%85%A841.9%EF%BC%85)                                                                                                 |
| [台灣指標](http://www.tisr.com.tw/?p=6936)                                                                                                                                                                                                                                                                          |
| [台灣指標](http://www.tisr.com.tw/?p=6980)                                                                                                                                                                                                                                                                          |
| [台灣指標](http://www.tisr.com.tw/?p=7015)                                                                                                                                                                                                                                                                          |
| [趨勢](http://www.taiwanthinktank.org/chinese/page/5/61/3149/0)                                                                                                                                                                                                                                                   |
| [台灣指標](http://www.tisr.com.tw/?p=7058)                                                                                                                                                                                                                                                                          |
| [遠見](http://www.gvm.com.tw/webonly_content_10889.html)                                                                                                                                                                                                                                                          |
| [壹電視](http://www.nexttv.com.tw/news/realtime/politics/11643854/%E5%9F%B7%E6%94%BF3%E6%9C%88%E6%88%90%E7%B8%BE%E5%96%AE%E3%80%80%E5%B0%8F%E8%8B%B1%E6%BB%BF%E6%84%8F%E5%BA%A6%E3%80%8C%E8%B7%8C%E7%A0%B45%E6%88%90%E3%80%8D)                                                                                     |
| [大社會](http://www.cna.com.tw/news/aipl/201608220158-1.aspx)                                                                                                                                                                                                                                                      |
| [趨勢](http://news.ltn.com.tw/news/politics/breakingnews/1807009)                                                                                                                                                                                                                                                 |
| [蘋果日報](http://www.appledaily.com.tw/appledaily/article/headline/20160826/37360441/%E5%9F%B7%E6%94%BF%E7%99%BE%E5%A4%A9%E3%80%8A%E8%98%8B%E6%9E%9C%E3%80%8B%E6%B0%91%E8%AA%BF%E8%94%A1%E4%B8%8D%E6%BB%BF%E5%BA%A6%E9%81%8E%E5%8D%8A)                                                                             |
| [TVBS](https://web.archive.org/web/20160827014114/http://other.tvbs.com.tw/export/sites/tvbs/file/other/poll-center/0508221.pdf)                                                                                                                                                                                |
| [財訊](https://web.archive.org/web/20160911165206/http://www.wealth.com.tw/article_in.aspx?nid=8762&pg=1)                                                                                                                                                                                                         |
| [聯合報](http://udn.com/news/story/10277/1922333)                                                                                                                                                                                                                                                                  |
| [台灣指標](http://www.tisr.com.tw/?p=7107)                                                                                                                                                                                                                                                                          |
| [十方](http://www.storm.mg/article/163339)                                                                                                                                                                                                                                                                        |
| [台灣指標](http://www.tisr.com.tw/?p=7165)                                                                                                                                                                                                                                                                          |
| [台灣指標](http://www.tisr.com.tw/?p=7180)                                                                                                                                                                                                                                                                          |
| [台灣指標](http://www.tisr.com.tw/?p=7212)                                                                                                                                                                                                                                                                          |
| [TVBS](http://news.tvbs.com.tw/politics/679512)                                                                                                                                                                                                                                                                 |
| [大社會](http://www.cna.com.tw/news/aipl/201610230073-1.aspx)                                                                                                                                                                                                                                                      |
| [台灣指標](http://www.tisr.com.tw/?p=7256)                                                                                                                                                                                                                                                                          |
|                                                                                                                                                                                                                                                                                                                 |

### 争议

  - 2016年6月3日，林全在立法院回應質詢稱：“[慰安妇问题有很多原因](../Page/慰安妇.md "wikilink")，慰安婦那麼多，有人也許是自願，有人也許是強迫，不能一定都說是自願或強迫。”\[27\]后林全于6月4日道歉，認為無論在任何時空環境下，女性都不應該因為體制暴力或是社會結構的因素，而傷害到身心的自主與尊嚴。\[28\]\[29\]
  - 林全在行政院長任內任內推動不當黨產條例、一例一休、長期照顧計畫2.0、電業法修正、年金改革、前瞻基礎建設計畫、2025非核家園等政策；皆造成部分民意極大反彈，另擬開放日本核災縣市食品進口，後又宣布暫緩\[30\]。

## 主要著作

| 年份    | 書名          | 作者                                                                                               | 出版社        | ISBN               |
| ----- | ----------- | ------------------------------------------------------------------------------------------------ | ---------- | ------------------ |
| 2010年 | 《經濟學》（六版）   | 毛慶生、[朱敬一](../Page/朱敬一.md "wikilink")、林全、許松根、[陳昭南](../Page/陳昭南_\(中央研究院院士\).md "wikilink")、陳添枝、黃朝熙 | 華泰文化事業有限公司 | ISBN 9789574171224 |
| 2010年 | 《經濟學概要》（五版） | 毛慶生、朱敬一、林全、許松根、陳昭南、陳添枝、黃朝熙                                                                       | 華泰文化事業有限公司 | ISBN 9789574171248 |
| 2010年 | 《基礎經濟學》（五版） | 毛慶生、朱敬一、林全、許松根、陳昭南、陳添枝、黃朝熙                                                                       | 華泰文化事業有限公司 | ISBN 9789574171231 |
| 2010年 | 《經濟學的視野》    | 朱敬一、林全                                                                                           | 聯經出版事業公司   | ISBN 9789570835656 |
|       |             |                                                                                                  |            |                    |

## 軼事

  - [披薩是林全的最愛](../Page/披薩.md "wikilink")，覺得披薩「好吃又營養」。林全曾向記者說，只要「最低稅負制」能立法通過，他就「全天候、無限制供應披薩」。\[31\]林全也喜歡[電玩](../Page/電玩.md "wikilink")。\[32\]
  - 林全育有二女，亦有兩段婚姻，現任太太吳佩凌\[33\]曾是林全的學生，林全和吳佩凌相差19歲，2人不只曾是師生，還是上司和秘書，在林全擔任臺北市財政局長的時候，吳佩凌還在政大念碩士，當時她被推薦變成林全的小秘書，沒想到就這樣促成一段姻緣。\[34\]作風瀟灑、自認不求官名，林全不因擔心外界[流言](../Page/流言.md "wikilink")，而委屈自己的感情，在政壇上被歸類為「名士派」。\[35\]\[36\]
  - 愛穿高領毛衣搭配西裝外套的閣揆林全，品味雅痞，總是斯文現身，40多年前，在高雄左營念書的林全，就已經是校內風雲人物。\[37\]
  - 林全在寫自己的橫式簽名時，兩個「木」分得太開，右邊那個「木」卻與「全」字貼的太近，因而被網友戲稱為「木栓」。\[38\]\[39\]

## 評價

  - 律師[陳長文在林全未上任行政院長前](../Page/陳長文.md "wikilink")，對林全抱有很大期許語多肯定，「無黨籍的林全，出身高雄左營眷村，統獨立場不明顯，兼有專業及施政能力，產、官、學經歷豐富且風評頗佳。」\[40\]
  - 林全在陳水扁政府財政部長任內最知名的就是推動最低稅負制，積極推動稅改，被當時民進黨立委簡錫堦稱讚是「唯一成功加稅而沒下台的財政部長」。\[41\]
  - 因為[一例一休政策引起的爭議](../Page/一例一休.md "wikilink")，[謝金河批林全](../Page/謝金河.md "wikilink")：「習慣喝紅酒、當大學教授、坐領千萬獨董年薪的林院長，不知底層民眾生活」\[42\]。

## 參考文獻

### 腳註

### 外部連結

  - [林全](https://www.facebook.com/LinChuanTW/)的Facebook專頁
  - [行政院林全院長](https://www.ey.gov.tw/Page/FA4291966BB3CF65/8535fca0-76f8-42df-a56f-227948fdd7e3)

### 內部連結

  - [行政院院長](../Page/行政院院長.md "wikilink")
  - [林全內閣](../Page/林全內閣.md "wikilink")
  - [蔡英文政府](../Page/蔡英文政府.md "wikilink")

{{-}}   |- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")
[行政院](../Page/行政院.md "wikilink")** |-    |-    |-

[Category:台灣經濟學家](../Category/台灣經濟學家.md "wikilink")
[Category:行政院院長](../Category/行政院院長.md "wikilink")
[Category:中華民國財政部部長](../Category/中華民國財政部部長.md "wikilink")
[Category:中華民國行政院主計處主計長](../Category/中華民國行政院主計處主計長.md "wikilink")
[Category:臺北市財政局局長](../Category/臺北市財政局局長.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:伊利諾大學校友](../Category/伊利諾大學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:江蘇裔台灣人](../Category/江蘇裔台灣人.md "wikilink")
[Category:左營人](../Category/左營人.md "wikilink")
[Chuan全](../Category/林姓.md "wikilink")

1.
2.
3.

4.

5.

6.

7.
8.
9.
10.
11.
12.
13.
14.

15.

16.

17.

18.

19. [行政院前院長林全轉戰生技
    出任東洋董座](https://money.udn.com/money/story/5641/2930822?f=fcmpush)

20.

21.

22.

23.

24. [矽品宣布
    終止紫光參與私募案](http://www.cna.com.tw/news/afe/201604280134-1.aspx)，中央社，2016-4-28

25.

26. [新任總統閣揆施政滿意度追蹤與預測](http://tsjh301.blogspot.tw/p/blog-page.html?m=12016)，無情真實的未來事件

27.

28.

29.

30.

31.

32.

33.
34.

35.

36.

37.

38.

39.

40.

41.
42.