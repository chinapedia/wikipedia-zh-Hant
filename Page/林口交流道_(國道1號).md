**林口交流道**位於[新北市](../Page/新北市.md "wikilink")[林口區和](../Page/林口區.md "wikilink")[桃園市](../Page/桃園市.md "wikilink")[龜山區的交界](../Page/龜山區.md "wikilink")，分為文化一路、文化北路出口，指標為41k。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
<th><p>南下</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:TW_CHW105.svg" title="fig:TW_CHW105.svg">TW_CHW105.svg</a></p></th>
<th><p><strong>龜山</strong> <strong>林口</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北上</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_CHW105.svg" title="fig:TW_CHW105.svg">TW_CHW105.svg</a></p></td>
<td><p><strong>林口</strong> <strong>龜山</strong> <strong>文化一路</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>南下</p></td>
<td></td>
<td><p><strong>龜山</strong> <strong>林口</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北上</p></td>
<td></td>
<td><p><strong>林口</strong> <strong>龜山</strong> <strong>文化北路</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 道路連結

  - 文化一路（[市道105號](../Page/市道105號.md "wikilink")，林口／龜山）
  - 文化二路（林口）／（龜山）
  - 文化三路（林口）／（龜山）
  - 文化北路（林口）／忠義路（龜山）
  - 八德路（林口，與交流道平行）
  - 龜山一路（龜山，與交流道平行）

## 鄰近交流道

## 註釋

[Category:新北市交流道](../Category/新北市交流道.md "wikilink")
[Category:桃園市交流道](../Category/桃園市交流道.md "wikilink")
[Category:林口區](../Category/林口區.md "wikilink")
[Category:龜山區](../Category/龜山區.md "wikilink")