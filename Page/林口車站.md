**林口車站**位於[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[林口區](../Page/林口區.md "wikilink")，曾為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[林口線](../Page/林口線.md "wikilink")（現已廢止）的[鐵路車站](../Page/鐵路車站.md "wikilink")，位於[桃園站起](../Page/桃園車站.md "wikilink")18.4公里處，是一座運煤專用的貨運車站。

## 車站構造

  - 僅有場站及三條專用貨運側線，以供卸下煤炭。
  - 有一座站房但從未正式啟用過，目前站房做為垃圾丟棄所使用。
  - 站內有一座階梯。

## 利用狀況

本站自設站之後一直到廢站為止，一直都是純貨運車站的用途，但曾經有開放區間車。主要的功能為載運自[臺中港卸貨的](../Page/臺中港.md "wikilink")[燃煤](../Page/煤.md "wikilink")，轉送供[林口火力發電廠使用](../Page/林口火力發電廠.md "wikilink")。

## 車站週邊

  - [臺灣海峽](../Page/臺灣海峽.md "wikilink")
  - [TW_PHW61.svg](https://zh.wikipedia.org/wiki/File:TW_PHW61.svg "fig:TW_PHW61.svg")[台61線](../Page/台61線.md "wikilink")
  - [TW_PHW15.svg](https://zh.wikipedia.org/wiki/File:TW_PHW15.svg "fig:TW_PHW15.svg")[台15線](../Page/台15線.md "wikilink")
  - [林口火力發電廠](../Page/林口發電廠.md "wikilink")
  - 林口海湖靶場

## 歷史

  - 1968年1月1日：名義上設站(實際上迄今仍尚未啟用)，通車時稱「**下寮**」。
  - 1968年3月18日：改稱，今名已與區名相稱，但距[新北市](../Page/新北市.md "wikilink")[林口區中心仍遠](../Page/林口區.md "wikilink")。
  - 2012年12月28日：隨者林口線停駛而廢止。

## 鄰近車站

## 參考資料

  -
[Category:林口區鐵路車站](../Category/林口區鐵路車站.md "wikilink")
[Category:2012年關閉的鐵路車站](../Category/2012年關閉的鐵路車站.md "wikilink")
[Category:林口線車站](../Category/林口線車站.md "wikilink")
[Category:1968年启用的铁路车站](../Category/1968年启用的铁路车站.md "wikilink")