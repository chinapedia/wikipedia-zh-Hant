[Tongeren_Innenstadt.jpg](https://zh.wikipedia.org/wiki/File:Tongeren_Innenstadt.jpg "fig:Tongeren_Innenstadt.jpg")\]\]

**林堡省**（[荷兰语](../Page/荷兰语.md "wikilink")：****）是位于[比利时](../Page/比利时.md "wikilink")[弗拉芒大區](../Page/弗拉芒大區.md "wikilink")（[弗拉芒社群](../Page/弗拉芒社群.md "wikilink")）东部的一个省，首府[哈瑟尔特](../Page/哈瑟尔特.md "wikilink")，人口860,204（2015年）\[1\]，面積2,422.1平方公里。

## 林堡省轄下行政區

  - [哈瑟爾特區](../Page/哈瑟爾特區.md "wikilink")
  - [馬塞克區](../Page/馬塞克區.md "wikilink")
  - [通厄倫區](../Page/通厄倫區.md "wikilink")

## 林堡省市镇

  - [馬斯梅赫倫](../Page/馬斯梅赫倫.md "wikilink")（Maasmechelen）
  - [哈瑟尔特](../Page/哈瑟尔特.md "wikilink")（Hasselt）
  - [迪潘比克](../Page/迪潘比克.md "wikilink")（Diepenbeek）
  - [根克](../Page/根克.md "wikilink")（Genk）
  - [拉那肯](../Page/拉那肯.md "wikilink")（Lanaken）
  - [洛默爾](../Page/洛默爾.md "wikilink")（Lommel）
  - [马泽克](../Page/马泽克.md "wikilink")（Maaseik）
  - [铁森德路](../Page/铁森德路.md "wikilink")（Tessenderlo）
  - [通厄伦](../Page/通厄伦.md "wikilink")（Tongeren）

## 名人

  - [金·克莱斯特尔斯](../Page/金·克莱斯特尔斯.md "wikilink")（Kim Clijsters）
  - [阿克塞拉·瑞德](../Page/阿克塞拉·瑞德.md "wikilink")（Axelle Red）
  - [扬·范·艾克](../Page/扬·范·艾克.md "wikilink")（Jan van Eyck）

## 参见

  - [哈瑟尔特大学](../Page/哈瑟尔特大学.md "wikilink")

## 參考來源

[林堡省](../Category/比利时省份.md "wikilink")

1.