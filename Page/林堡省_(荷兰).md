**林堡省**（）位于[荷兰东南部的一个行政省](../Page/荷兰.md "wikilink")，东边与[德国接壤](../Page/德国.md "wikilink")，西南边与[比利时接壤](../Page/比利时.md "wikilink")。省会城市为[马斯特里赫特](../Page/马斯特里赫特.md "wikilink")，人口1,135,962（2005年）。

## 名稱由來

林堡省的名稱是源於其境內的城堡-[林堡而出名](../Page/林堡.md "wikilink")，其靠近葦德爾河河谷與[比利時](../Page/比利時.md "wikilink")[列日省接壤](../Page/列日省.md "wikilink")，該地即中世紀林堡公國(Duchy
of
Limburg)的領地沿著[馬士河延伸而去可以到列日](../Page/馬士河.md "wikilink")(城市)，然而環繞著林堡公國的還包含了[布拉班特公國](../Page/布拉班特公國.md "wikilink")、於利希公國(Duchy
of Jülich)、高爾登公國(Duchy of Guelders)以及列日大主教領地(Prince-Bishopric of
Liège)，多元文化的影響使該省至今保留獨特的方言-[林堡語](../Page/林堡語.md "wikilink")。

## 地理

該省的南部同時也是荷蘭國境之南，也是該國稀有的山丘地形存在地，也是荷蘭海拔最高處－[佛羅斯柏格峰的所在地](../Page/佛羅斯柏格峰.md "wikilink")。佛羅斯柏格峰位於荷蘭、比利時與德國之交界，該地區最重要的河流即[馬士河](../Page/馬士河.md "wikilink")，其流域貫穿該省由南至北，林堡省的地貌也大多被馬士河所影響，該河的土壤沉積成黏土和壚土，造就開採這些土讓的工業興起，該省主要的城市－[馬斯垂克及其廣大的郊區](../Page/馬斯垂克.md "wikilink")，例如：南部的斯塔德(Sittard)－吉里(Geleen)以及帕金史丹林堡(包含了海爾倫)，而北部則有[芬洛](../Page/芬洛.md "wikilink")，[國際標準化組織把該省編定為](../Page/國際標準化組織.md "wikilink")[ISO
3166-2:NL](../Page/ISO_3166-2:NL.md "wikilink")-LI。

## 语言

林堡省有其自身的语言，称为[林堡语](../Page/林堡语.md "wikilink")（[荷兰语](../Page/荷兰语.md "wikilink")：****）。林堡语为地区性的小语种，被收入[欧洲小语种保护法](../Page/欧洲小语种保护法.md "wikilink")。大约有一百六十万左右的[荷兰人](../Page/荷兰.md "wikilink")、[比利时人及](../Page/比利时.md "wikilink")[德国人能够讲这种语言](../Page/德国.md "wikilink")。

## 政府

省議會一席一共63個席次，該省[女王專員有別於荷蘭其他省分稱做總督](../Page/女王專員.md "wikilink")，省議會由該省公民直選，總督由內閣推薦經女王任命，現任總督為萊昂菲森(Leon
Frissen)，而省議會中[自由黨 (荷蘭)是最大黨](../Page/自由黨_\(荷蘭\).md "wikilink")。

省務大多由省行政部門所辦理，而行政部門是由總督所領導，行政代表(*gedeputeerden*)通常依自身專長領域有專門負責的事務。

## 基层政权

  - [贝克Beek](../Page/贝克_\(荷兰林堡省\).md "wikilink")
  - [贝瑟尔Beesel](../Page/贝瑟尔.md "wikilink")
  - [贝亨Bergen](../Page/贝亨_\(荷兰林堡省\).md "wikilink")
  - [布林瑟姆Brunssum](../Page/布林瑟姆.md "wikilink")
  - [埃赫特 - 苏斯特伦Echt](../Page/埃赫特_-_苏斯特伦.md "wikilink")-Susteren
  - [艾斯登 - 马赫拉滕Eijsden](../Page/艾斯登_-_马赫拉滕.md "wikilink")-Margraten
  - [亨讷普Gennep](../Page/亨讷普.md "wikilink")
  - [许尔彭 - 维特姆Gulpen](../Page/许尔彭_-_维特姆.md "wikilink")-Wittem
  - [海尔伦Heerlen](../Page/海尔伦.md "wikilink")
  - [马斯河畔霍尔斯特Horst](../Page/马斯河畔霍尔斯特.md "wikilink") aan de Maas
  - [凯尔克拉德Kerkrade](../Page/凯尔克拉德.md "wikilink")

<!-- end list -->

  - [兰德赫拉夫Landgraaf](../Page/兰德赫拉夫.md "wikilink")
  - [勒达尔Leudal](../Page/勒达尔.md "wikilink")
  - [马斯豪Maasgouw](../Page/马斯豪.md "wikilink")
  - [马斯特里赫特Maastricht](../Page/马斯特里赫特.md "wikilink")
  - [梅尔森Meerssen](../Page/梅尔森.md "wikilink")
  - [莫克和米德拉尔Mook](../Page/莫克和米德拉尔.md "wikilink") en Middelaar
  - [下韦尔特Nederweert](../Page/下韦尔特.md "wikilink")
  - [尼特Nuth](../Page/尼特_\(荷兰林堡省\).md "wikilink")
  - [翁德尔班肯Onderbanken](../Page/翁德尔班肯.md "wikilink")
  - [皮尔恩马斯Peel](../Page/皮尔恩马斯.md "wikilink") en Maas
  - [鲁尔达伦Roerdalen](../Page/鲁尔达伦.md "wikilink")

<!-- end list -->

  - [鲁尔蒙德Roermond](../Page/鲁尔蒙德.md "wikilink")
  - [斯欣嫩Schinnen](../Page/斯欣嫩.md "wikilink")
  - [辛珀尔费尔德Simpelveld](../Page/辛珀尔费尔德.md "wikilink")
  - [锡塔德 - 赫伦Sittard](../Page/锡塔德_-_赫伦.md "wikilink")-Geleen
  - [斯泰恩Stein](../Page/斯泰恩_\(荷兰林堡省\).md "wikilink")
  - [法尔斯Vaals](../Page/法尔斯_\(荷兰林堡省\).md "wikilink")
  - [赫尔河畔法尔肯堡Valkenburg](../Page/赫尔河畔法尔肯堡.md "wikilink") aan de Geul
  - [芬洛Venlo](../Page/芬洛.md "wikilink")
  - [芬拉伊Venray](../Page/芬拉伊.md "wikilink")
  - [富伦达尔Voerendaal](../Page/富伦达尔.md "wikilink")
  - [韦尔特Weert](../Page/韦尔特.md "wikilink")

## 經濟

從前林堡地區以開採[泥炭](../Page/泥炭.md "wikilink")、[礫石和](../Page/礫石.md "wikilink")[煤為主](../Page/煤.md "wikilink")，而這些開採業都是國有企業－[帝斯曼的經營項目](../Page/帝斯曼.md "wikilink")，該企業主要業務是家化學工業，目前仍在林堡執行業務，汽車工業泊爾(Born)和印刷機製造工業奧西(Océ)以前也在林堡有經營。

此外在起伏的南部地區到人口稠密的東南部，北部和西南部馬斯特里赫特在斯塔德海爾倫/凱爾克拉特之間的三角，有大約四啤酒的啤酒廠。

林堡也是荷蘭全境內唯二之一的水果種植區。

這四十年以來大量的果樹種植區被馬士河的河水所侵襲或取代。

## 圖片集

|                                                                                                                                       |                                                                                                                                             |                                                                                                   |                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| [Brunssummerheideoverview2.jpg](https://zh.wikipedia.org/wiki/File:Brunssummerheideoverview2.jpg "fig:Brunssummerheideoverview2.jpg") | [Ravensbosjsjtraobaekwkped07.JPG](https://zh.wikipedia.org/wiki/File:Ravensbosjsjtraobaekwkped07.JPG "fig:Ravensbosjsjtraobaekwkped07.JPG") | [Gulp-Slenaken.jpg](https://zh.wikipedia.org/wiki/File:Gulp-Slenaken.jpg "fig:Gulp-Slenaken.jpg") | [Netherlands_Grote_Peel_lake.jpg](https://zh.wikipedia.org/wiki/File:Netherlands_Grote_Peel_lake.jpg "fig:Netherlands_Grote_Peel_lake.jpg") |

[\*](../Category/林堡省_\(荷兰\).md "wikilink")