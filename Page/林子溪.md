**林子溪**（公司田溪），為一位於[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[淡水區的獨立水系](../Page/淡水區.md "wikilink")，發源於[大屯山西側](../Page/大屯山.md "wikilink")，經楓樹湖、泉州厝、林子、崁頂、大庄埔，於港子坪南側注入[台灣海峽](../Page/台灣海峽.md "wikilink")。幹流長度13.50公里，流域面積24.32平方公里\[1\]。

## 歷史

在[台灣荷西殖民時期](../Page/台灣荷西殖民時期.md "wikilink")，林子溪流域下游地區有許多屬於[荷蘭東印度公司的田](../Page/荷蘭東印度公司.md "wikilink")，故該溪又名**公司田溪**\[2\]。

## 相關條目

  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:台灣縣市管河川水系](../Category/台灣縣市管河川水系.md "wikilink")
[Category:新北市河川](../Category/新北市河川.md "wikilink")
[Category:淡水區](../Category/淡水區.md "wikilink")

1.  台灣地區水資源史 第二篇，台灣省文獻委員會，2000年
2.  [臺北縣立淡水古蹟博物館官方網站](http://www.tshs.tpc.gov.tw/tai2_16.asp?H1m_sn=14)