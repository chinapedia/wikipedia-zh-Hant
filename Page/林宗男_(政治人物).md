**林宗男**（），[台灣](../Page/台灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，曾任[南投縣縣長](../Page/南投縣縣長.md "wikilink")。

## 生平

林宗男於[逢甲大學合作經濟學系畢業後](../Page/逢甲大學.md "wikilink")，赴[日本攻讀](../Page/日本.md "wikilink")[明治大學](../Page/明治大學.md "wikilink")[商學碩士](../Page/商學.md "wikilink")。1997年，[民主進步黨提名林宗男參選南投縣縣長](../Page/民主進步黨.md "wikilink")，敗給從民進黨退黨參選的[彭百顯](../Page/彭百顯.md "wikilink")。[2009年三合一選舉](../Page/2009年三合一選舉.md "wikilink")，林宗男一度想爭取民進黨再次提名參選南投縣縣長，後因健康因素退選。2010年11月19日，林宗男因[肺腺癌病逝](../Page/肺腺癌.md "wikilink")，享壽69歲\[1\]。

林宗男的長子[林耘生曾任](../Page/林耘生.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

## 從政

### 1997年南投縣長選舉

| 1997年[南投縣縣長選舉結果](../Page/南投縣縣長.md "wikilink")''' |
| ------------------------------------------------ |
| 號次                                               |
| 1                                                |
| 2                                                |
| 3                                                |
| 4                                                |
| 5                                                |
|                                                  |

### 2001年南投縣長選舉

| 2001年[南投縣縣長選舉結果](../Page/南投縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| 4                                             |
| 5                                             |
| 6                                             |
| 7                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### 2005年南投縣長選舉

| 2005年[南投縣縣長選舉結果](../Page/南投縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| 4                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

## 參考文獻

|colspan="3"
style="text-align:center;"|**[Seal_of_Nantou_County.svg](https://zh.wikipedia.org/wiki/File:Seal_of_Nantou_County.svg "fig:Seal_of_Nantou_County.svg")[南投縣政府](../Page/南投縣.md "wikilink")**


[Category:南投縣縣長](../Category/南投縣縣長.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:第9屆臺灣省議員](../Category/第9屆臺灣省議員.md "wikilink")
[Category:第8屆臺灣省議員](../Category/第8屆臺灣省議員.md "wikilink")
[Category:草屯鎮鎮長](../Category/草屯鎮鎮長.md "wikilink")
[Category:前民主進步黨黨員](../Category/前民主進步黨黨員.md "wikilink")
[Category:民主進步黨中常委](../Category/民主進步黨中常委.md "wikilink")
[Category:東海大學教授](../Category/東海大學教授.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")
[Category:逢甲大學校友](../Category/逢甲大學校友.md "wikilink")
[Category:草屯人](../Category/草屯人.md "wikilink")
[Zong宗男](../Category/林姓.md "wikilink")
[Category:罹患肺腺癌逝世者](../Category/罹患肺腺癌逝世者.md "wikilink")

1.