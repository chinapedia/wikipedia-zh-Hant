**林家瑩**是[臺中市](../Page/臺中市.md "wikilink")[大里區人](../Page/大里區.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[田徑運動員](../Page/田徑運動員.md "wikilink")，擅長項目[鉛球](../Page/鉛球.md "wikilink")，為臺灣女子鉛球（4公斤）全國紀錄保持保持人。

## 生平

林家瑩於2007年秋至2008年[北京奧運前曾與](../Page/北京奧運.md "wikilink")[張銘煌一同接受](../Page/張銘煌.md "wikilink")[德國](../Page/德國.md "wikilink")[柏林投擲教練](../Page/柏林.md "wikilink")[Werner
Goldmann指導](../Page/Werner_Goldmann.md "wikilink")。林家瑩在2010年3月11日至2010年11月10日與男子鉛球選手[張銘煌一同於](../Page/張銘煌.md "wikilink")[美國](../Page/美國.md "wikilink")[喬治亞州](../Page/喬治亞州.md "wikilink")[雅典城](../Page/雅典_\(喬治亞州\).md "wikilink")[喬治亞大學接受投擲教練Donald](../Page/喬治亞大學.md "wikilink")
Babbitt指導。

林家瑩代表臺灣參加2010年[廣州](../Page/廣州.md "wikilink")[亞運田徑女子鉛球賽事擲出](../Page/亞運.md "wikilink")17.06公尺獲第四名,
並再次創新臺灣女子鉛球新紀錄，也是臺灣第一次女子鉛球突破17公尺。廣州亞運結束後，林家瑩以專任運動教練資格進入[國立體育大學田徑隊擔任投擲助理教練](../Page/國立體育大學.md "wikilink")。

林家瑩於2012年5月26日臺灣國際田徑錦標賽擲出17.38米再破全國紀錄，並取得[倫敦奧運參賽資格](../Page/2012年夏季奧林匹克運動會.md "wikilink")(B標)。林家瑩於2012倫敦夏季奧運會資格賽中擲出17公尺43再破全國紀錄。

2014年仁川亞運，林家瑩擲出17公尺48再破全國紀錄，以第四名作收。

## 工作

  - [國立體育大學田徑投擲隊專任運動教練](../Page/國立體育大學.md "wikilink") 2011年\~迄今

## 學歷

  - [臺中縣立大里高中國中部](../Page/臺中市立大里高中.md "wikilink")
  - [臺中縣](../Page/臺中縣.md "wikilink")[大甲高中](../Page/大甲高中.md "wikilink")
  - [國立台北師院體育系](../Page/國立臺北教育大學.md "wikilink")
  - [國立體育大學](../Page/國立體育大學.md "wikilink")[運動技術研究所](../Page/運動技術研究所.md "wikilink")(2009年畢業)

## 教練

  - 洪春成1996－1998年
  - 林啟仲1998－2001年
  - 水心蓓2001－2005年
  - [呂景義](../Page/呂景義.md "wikilink")2005－2007年
  - [Werner Goldmann](../Page/Werner_Goldmann.md "wikilink") 2007－2008年
  - [呂景義](../Page/呂景義.md "wikilink")2008－2009年
  - [Donald Babbitt](../Page/Donald_Babbitt.md "wikilink")
    2010年3月\~2010年11月

## 曾獲獎項

  - 2012年[臺灣國際田徑錦標賽女子鉛球金牌](../Page/臺灣國際田徑錦標賽.md "wikilink")(17.38m)
  - 2001年[亞洲青年田徑錦標賽女子鉛球亞軍](../Page/亞洲青年田徑錦標賽.md "wikilink")。
  - 2006年[杜哈](../Page/杜哈.md "wikilink")[亞運女子鉛球銅牌](../Page/亞運.md "wikilink")。
  - 2008.6.28 Gothaer Schloss Kugelstossmeeting德國Gotha城堡鉛球賽第三名（16.79公尺）
  - 2009.5.7 [全國大學運動會女子鉛球金牌](../Page/全國大學運動會.md "wikilink") (16.91公尺)
  - 2009 [亞洲田徑錦標賽女子鉛球銅牌](../Page/亞洲田徑錦標賽.md "wikilink")
  - 2009 [東亞運動會女子鉛球銀牌](../Page/東亞運動會.md "wikilink")

## 參考資料

## 外部連結

  -
  - [Gothaer Schloss
    Kugelstossmeeting](http://www.gothaer-schlossmeeting.de)

  - <http://tw.sports.yahoo.com/article/aurl/d/a/120527/2/8m1y.html>
    中時電子報\]

[Category:林姓](../Category/林姓.md "wikilink")
[Category:大里人](../Category/大里人.md "wikilink")
[Category:國立大甲高級中學校友](../Category/國立大甲高級中學校友.md "wikilink")
[Category:國立臺北教育大學校友](../Category/國立臺北教育大學校友.md "wikilink")
[Category:國立體育大學校友](../Category/國立體育大學校友.md "wikilink")
[Category:臺灣田徑運動員](../Category/臺灣田徑運動員.md "wikilink")
[Category:台灣女子運動員](../Category/台灣女子運動員.md "wikilink")
[Category:台灣之最](../Category/台灣之最.md "wikilink")
[Category:亞洲運動會田徑獎牌得主](../Category/亞洲運動會田徑獎牌得主.md "wikilink")
[Category:台灣奧運田徑運動員](../Category/台灣奧運田徑運動員.md "wikilink")
[Category:2006年亞洲運動會田徑運動員](../Category/2006年亞洲運動會田徑運動員.md "wikilink")
[Category:2006年亚洲运动会铜牌得主](../Category/2006年亚洲运动会铜牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會田徑運動員](../Category/2008年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2010年亞洲運動會田徑運動員](../Category/2010年亞洲運動會田徑運動員.md "wikilink")
[Category:2012年夏季奧林匹克運動會田徑運動員](../Category/2012年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2014年亞洲運動會田徑運動員](../Category/2014年亞洲運動會田徑運動員.md "wikilink")
[Category:2018年亞洲運動會田徑運動員](../Category/2018年亞洲運動會田徑運動員.md "wikilink")