**林家駿**主教（，），字少彬，生於[香港](../Page/香港.md "wikilink")，祖籍[廣東](../Page/廣東.md "wikilink")[新會](../Page/新會.md "wikilink")。林家駿為[天主教澳門教區第二十二任](../Page/天主教澳門教區.md "wikilink")[主教](../Page/主教.md "wikilink")，也是[天主教澳門教區首位華人主教](../Page/天主教澳門教區.md "wikilink")；由於林家駿在位為主教時處於[澳門回歸的過渡期](../Page/澳門回歸.md "wikilink")，因此有「過渡期的主教」之稱。2009年因[胃癌病逝](../Page/胃癌.md "wikilink")。

## 生平

林家駿於1928年4月9日在[香港出生](../Page/香港.md "wikilink")，同年5月6日於[香港](../Page/香港.md "wikilink")[聖母無原罪主教座堂領受](../Page/聖母無原罪主教座堂.md "wikilink")[聖洗](../Page/聖洗.md "wikilink")。及後，林家駿到澳門定居，年少時就讀於[聖若瑟中學及](../Page/聖若瑟教區中學第二、三校.md "wikilink")[無原罪學校](../Page/慈幼中學.md "wikilink")；1941年小學畢業後於[聖若瑟修院攻讀外](../Page/聖若瑟修院.md "wikilink")，開展傳教工作、舉辦主日學，1947年完成中學課程，並於1949年及1953年分別完成[哲學及考取神哲學士學位](../Page/哲學.md "wikilink")\[1\]。

[O_Domingos_Lam_em_1953.jpg](https://zh.wikipedia.org/wiki/File:O_Domingos_Lam_em_1953.jpg "fig:O_Domingos_Lam_em_1953.jpg")

### 晉鐸

1953年12月27日，林家駿在[天主教澳門教區](../Page/天主教澳門教區.md "wikilink")[羅若望](../Page/羅若望.md "wikilink")[主教主禮下於](../Page/主教.md "wikilink")[聖嘉辣小堂晉鐸](../Page/聖嘉辣小堂.md "wikilink")，也被任命為聖若瑟修院導師外，也被母校聖若瑟中學校長何心源神父邀請在中學部兼任教師。1955年，林家駿和聖若瑟修院的華籍神父合辦《院聲月刊》（後易名為《晨曦月刊》）並任主編。林家駿於1958年9月被委任為[聖若瑟中學第二校主任](../Page/聖若瑟教區中學第二、三校.md "wikilink")，並在[聖羅撒女子中學](../Page/聖羅撒女子中學.md "wikilink")、[粵華中學等學校授教](../Page/粵華中學.md "wikilink")。

1959年10月24日，林家駿奉[高德華主教委任為](../Page/高德華.md "wikilink")[氹仔](../Page/氹仔.md "wikilink")[嘉模聖母堂主任司鐸及](../Page/嘉模聖母堂.md "wikilink")[聖善學校校長](../Page/聖善學校.md "wikilink")；並於1960年至1962年期間被[澳葡政府委任為海島市市政委員](../Page/澳葡政府.md "wikilink")。1963年，[戴維理主教調任林家駿至](../Page/戴維理.md "wikilink")[新加坡出任新加坡聖若瑟堂助理主任司鐸及堂區署理主任等職務外](../Page/新加坡.md "wikilink")，他還兼任該堂區英文月刊《團結月刊（）》社長及主編。1973年年底，林家駿被調回澳門並被委任為聖若瑟修院院長；翌年3月31日，被[高秉常主教委任為署理主教執行秘書](../Page/高秉常.md "wikilink")，同年年底兼任[聖若瑟修院院長](../Page/聖若瑟修院及聖堂.md "wikilink")\[2\]。

### 擢升副主教

1976年4月20日，[高秉常](../Page/高秉常.md "wikilink")[主教擢升林家駿為](../Page/主教.md "wikilink")[天主教澳門教區](../Page/天主教澳門教區.md "wikilink")[副主教](../Page/副主教.md "wikilink")，負責教區內部事務；1978年起兼任澳門教區事務統籌處主任外、創辦《[晨曦](../Page/晨曦.md "wikilink")》週刊外，獲得澳葡政府邀請出任為[澳門東亞大學建校諮詢委員會委員](../Page/澳門東亞大學.md "wikilink")。翌年升任紅衣團團長外，並奉高秉常主教訓令協助改組合併[聖若瑟教區中學及擔任](../Page/聖若瑟教區中學.md "wikilink")[澳門中華學生協會名譽顧問](../Page/澳門中華學生協會.md "wikilink")。

林家駿於1981年至1991年出任澳門東亞大學校董會委員；1982年起為聖若瑟中學留港校友會永遠顧問；1983年被委任為[澳門明愛委員](../Page/澳門明愛.md "wikilink")。他出任副主教期間還成立了澳門公教婚姻輔導會（即今澳門美滿家庭協進會），並被選為該會主席至2003年榮休\[3\]。

### 真除主教

[31_de_Julho_de_2009_em_Sé,_Macau.JPG](https://zh.wikipedia.org/wiki/File:31_de_Julho_de_2009_em_Sé,_Macau.JPG "fig:31_de_Julho_de_2009_em_Sé,_Macau.JPG")
教宗[若望保祿二世於](../Page/若望保祿二世.md "wikilink")1987年6月27日任命林家駿為天主教澳門教區[助理主教](../Page/助理主教.md "wikilink")；同年9月8日晉牧。1988年10月6日，[高秉常](../Page/高秉常.md "wikilink")[主教獲准卸除主教職務](../Page/主教.md "wikilink")，林家駿同日真除為天主教澳門教區第22任正權主教，成為天主教澳門教區成立以來首位華人主教；同年被委任為[澳門基本法起草委員會委員](../Page/澳門基本法.md "wikilink")。1989年起擔任[羅馬宗座烏爾龐大學附屬學院的](../Page/羅馬宗座烏爾龐大學.md "wikilink")[香港聖神修院神哲學院校董](../Page/香港聖神修院.md "wikilink")。

林家駿出任主教期間除在澳門六個堂區外設立了三個準堂區外\[4\]，還致力保存古典宗教藝術文化、培育天主教教友及神職人員；他設立[澳門天主教教友協進會](../Page/澳門天主教教友協進會.md "wikilink")、[澳門聖經協會等教友組織外](../Page/澳門聖經協會.md "wikilink")，並興建[聖若瑟勞工主保堂](../Page/聖若瑟勞工主保堂.md "wikilink")、[竹灣會議中心](../Page/竹灣會議中心.md "wikilink")、重修[路環](../Page/路環.md "wikilink")[聖依撒伯爾靜修院等設施](../Page/聖依撒伯爾靜修院.md "wikilink")。1997年，為澳門教區和[葡萄牙天主教大學合作創辦](../Page/葡萄牙天主教大學.md "wikilink")[澳門高等校際學院外](../Page/澳門高等校際學院.md "wikilink")，也設立兩年制[大專程度的](../Page/大專.md "wikilink")「教理教師課程」和「教友神學課程」；又鼓勵及資助教友和[澳門主教府職工進修等等](../Page/澳門主教府.md "wikilink")\[5\]。

### 榮休及辭世

2003年6月30日，林家駿獲得[教宗](../Page/教宗.md "wikilink")[若望保祿二世批准辭去主教職務](../Page/若望保祿二世.md "wikilink")，林家駿宣布同日榮休；正權主教由助理主教[黎鴻昇於同日傍晚真除接任](../Page/黎鴻昇.md "wikilink")。

2009年3月，林家駿被確診[胃癌](../Page/胃癌.md "wikilink")；同年5月11日入院治療\[6\]。林家駿於同年7月27日下午於[鏡湖醫院病逝](../Page/鏡湖醫院.md "wikilink")\[7\]；7月31日下午，天主教澳門教區假[聖母聖誕主教座堂由黎鴻昇主教主祭](../Page/聖母聖誕主教座堂.md "wikilink")，並由[陳日君](../Page/陳日君.md "wikilink")[樞機](../Page/樞機.md "wikilink")、[天主教香港教區](../Page/天主教香港教區.md "wikilink")[湯漢主教等約](../Page/湯漢.md "wikilink")60位司鐸共祭為林家駿舉行[逾越聖祭](../Page/逾越聖祭.md "wikilink")（即大禮安息彌撒）\[8\]\[9\]；同日下葬於[聖味基墳場](../Page/聖味基墳場.md "wikilink")。

## 牧徽

[Coat_of_arms_of_Domingos_Lam_Ka_Tseung.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Domingos_Lam_Ka_Tseung.svg "fig:Coat_of_arms_of_Domingos_Lam_Ka_Tseung.svg")[牧徽](../Page/牧徽.md "wikilink")\]\]
林家駿的教區主教牧徽的中間是一個[盾牌](../Page/盾牌.md "wikilink")，內飾以[十誡約版的形狀](../Page/十誡.md "wikilink")；盾牌上由左至右分為兩列寫著中文格言「誠信桓祥，天道永昌」八個中文字；盾牌的正中上方有一個[十字架](../Page/十字架.md "wikilink")，而十字架上有代表主教的[主教帽](../Page/主教帽.md "wikilink")；盾牌的左上角有彌撒時用的禮冠，而盾牌的右上角則有一個牧杖，象徵主教的權威。盾徽下有一布條寫著[葡萄牙文格言](../Page/葡萄牙文.md "wikilink")「真誠（）」、「正義（）」、「和平（）」。

## 家庭

林家駿出生於[天主教家庭](../Page/天主教.md "wikilink")，父親林永彬，母親謝雅怡。[天主教廣州教區的林日昇神父](../Page/天主教廣州教區.md "wikilink")、林日廣為神父均為林家駿伯父，[天主教新加坡總教區劉潤高副主教](../Page/天主教新加坡總教區.md "wikilink")、劉志超神父、劉志剛神父、劉德慶神父等都是為林家駿祖母的表親；天主教澳門教區的謝扳雲神父、謝禧雲神父是為林家駿外玄伯伯外，陳伯祿神父為其表舅父；而[天主教香港教區的陳子殷神父為林家駿表兄](../Page/天主教香港教區.md "wikilink")，而表弟吳智勳神父是[耶穌會港澳區神父](../Page/耶穌會.md "wikilink")。

## 榮譽

林家駿於1988年獲[澳門童軍總會授予銅星勳章](../Page/澳門童軍總會.md "wikilink")；林家駿也被[葡萄牙政府於](../Page/葡萄牙政府.md "wikilink")1990年、1996年、1999年分別授予[殷皇子軍官級勳銜](../Page/殷皇子軍官級勳銜.md "wikilink")、[殷皇子大十字勳銜以及](../Page/殷皇子大十字勳銜.md "wikilink")[基督勳銜外](../Page/基督勳銜.md "wikilink")，[澳葡政府也於](../Page/澳葡政府.md "wikilink")1999年授予甲級勞績勳章；回歸後的[澳門特區政府也於](../Page/澳門特區政府.md "wikilink")2002年向林家駿授予[金蓮花勳章](../Page/金蓮花勳章.md "wikilink")，以表揚他對於澳門教育、社會等各方面的貢獻。

## 參考文獻

<div class="references-small">

<references />

  - [號角報](../Page/號角報.md "wikilink")，2009年7月31日

  -
  - 《不卑不亢 眼看將來林家駿帶領敎區過渡》，鄧耀榮著

</div>

## 外部連結

  - [何厚鏵發唁函哀悼林家駿榮休主教逝世](http://news.xinhuanet.com/gangao/2009-07/29/content_11794732.htm)

{{-}}

[Category:新會人](../Category/新會人.md "wikilink")
[Category:澳門天主教徒](../Category/澳門天主教徒.md "wikilink")
[K](../Category/林姓.md "wikilink") [L林](../Category/土葬者.md "wikilink")

1.  何劉一星，《我們永遠懷念的林家駿》——澳門一教友眼中的林家駿，[晨曦](../Page/晨曦.md "wikilink")，2009年7月31日．

2.
3.
4.

5.  《我們永遠懷念的林家駿》——黎鴻昇眼中的林家駿，[晨曦](../Page/晨曦.md "wikilink")，2009年7月31日

6.

7.

8.  [Nº 7/2009通告](http://www.catholic.org.mo/02-bispo/Cir72009.pdf)，[主教公署](../Page/主教公署.md "wikilink")，2009年7月30日

9.  《晨曦》特刊，[澳門主教公署事務統籌秘書處](../Page/澳門主教公署.md "wikilink")，2009年7月31日