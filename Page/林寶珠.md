**林寶珠**，[香港](../Page/香港.md "wikilink")[粵劇演員](../Page/粵劇.md "wikilink")，[林錦堂的姪女](../Page/林錦堂.md "wikilink")。

## 生平

林寶珠自小跟隨[任大勳學習北派](../Page/任大勳.md "wikilink")，[王粵生學習唱腔](../Page/王粵生.md "wikilink")。早年曾加入[彩龍鳳劇團](../Page/彩龍鳳劇團.md "wikilink")、[頌新聲劇團](../Page/頌新聲劇團.md "wikilink")、[慶鳳鳴](../Page/慶鳳鳴劇團.md "wikilink")、[鳳和鳴](../Page/鳳和鳴劇團.md "wikilink")、[錦添花](../Page/錦添花劇團.md "wikilink")、[非凡晌](../Page/非凡晌劇團.md "wikilink")、[日月星](../Page/日月星劇團.md "wikilink")、[新群英](../Page/新群英劇團.md "wikilink")、[龍嘉鳳等劇團](../Page/龍嘉鳳劇團.md "wikilink")。她曾隨團遠赴[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[美國](../Page/美國.md "wikilink")、[加拿大等地演出](../Page/加拿大.md "wikilink")。參演的劇目有《[西樓錯夢](../Page/西樓錯夢.md "wikilink")》、《[還魂記](../Page/還魂記.md "wikilink")》、《青蛇與白蛇》\[1\]、《[風雨泣萍姬](../Page/風雨泣萍姬.md "wikilink")》、《[白兔會](../Page/白兔會.md "wikilink")》、《[雙仙拜月亭](../Page/雙仙拜月亭.md "wikilink")》\[2\]、《[紅樓夢](../Page/紅樓夢.md "wikilink")》\[3\]、《[荊釵記](../Page/荊釵記.md "wikilink")》\[4\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [文學粵劇：還魂記夢](https://web.archive.org/web/20071203165800/http://www.lcsd.gov.hk/CE/CulturalService/Programme/b5/chinese_opera/aug06/dream1.html)

[L林](../Page/category:粵劇演員.md "wikilink")
[L林](../Page/category:香港粵劇演員.md "wikilink")

1.  [金玉堂劇團與廣州紅豆粵劇團攜手合作演出《青蛇與白蛇》](http://www.lcsd.hk/b5/ppr_release_det.php?pd=20020124&ps=04)，[康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")
2.  [吳仟峰尹飛燕率日月星劇團演三齣粵劇戲寶](http://www.lcsd.hk/b5/ppr_release_det.php?pd=20040302&ps=03)
    ，[康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")
3.  [大喜慶交出誠意之作](http://paper.wenweipo.com/2007/07/24/XQ0707240004.htm)，[文匯報](../Page/文匯報.md "wikilink")
4.  [文千歲汪明荃主演新編粵劇《荊釵記》](http://www.lcsd.gov.hk/b5/ppr_release_det.php?pd=20030613&ps=01)
    ，[康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")