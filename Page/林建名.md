**林建名**（）\[1\]，人稱「林大少」，[廣東](../Page/廣東.md "wikilink")[潮陽人](../Page/潮陽.md "wikilink")，香港豪門[林百欣家族後人](../Page/林百欣家族.md "wikilink")，[林百欣與元配賴元芳長子](../Page/林百欣.md "wikilink")，[香港](../Page/香港.md "wikilink")[商人](../Page/商人.md "wikilink")，大名娛樂有限公司東主，[香港超級聯賽](../Page/香港超級聯賽.md "wikilink")[東方體育會會長](../Page/東方體育會.md "wikilink")，前[广州市](../Page/广州市.md "wikilink")[天河区](../Page/天河区.md "wikilink")[政协委员](../Page/政协委员.md "wikilink")。

## 簡歷

林建名先後就讀於[拔萃男書院](../Page/拔萃男書院.md "wikilink")、[英國倫敦市學院商科及倫敦貿易學院紡織系](../Page/英國.md "wikilink")，於1955年畢業返回香港，協助父親林百欣打理家族製衣業務，惟其後同父異母之弟[林建岳返回香港協助父親](../Page/林建岳.md "wikilink")，林建名被投閒置散。

1993年12月，林建名出任[麗新集團董事](../Page/麗新集團.md "wikilink")，其後出任副主席。

提到林建名最轟動的事件，莫過於1994年，他於台北遭綁匪潛入別墅，被迷暈後遭拍下裸照，有指還被綁匪拍下疑雞姦的錄影帶。事後，林建名被勒索100萬美元（約780萬港元）。他返港後，收到有關照片，
於是先下手為強，自動將相片提供予傳媒刊登，事後6名匪徒被繩之於法。

2001年5月，林建名被[廉政公署起訴](../Page/廉政公署_\(香港\).md "wikilink")，指控他作為[旺角區文娛康樂體育會有限公司主席](../Page/旺角區.md "wikilink")，涉嫌與[成元興等體育界人士串謀將](../Page/成元興.md "wikilink")[林百欣中心裝修費誇大](../Page/林百欣中心.md "wikilink")，以騙取[香港賽馬會](../Page/香港賽馬會.md "wikilink")20萬港元的資助費用。

2007年5月10日，69歲的林建名爆出跟年輕47年的[姚韵譜爺孫戀](../Page/姚韵.md "wikilink")。

2014年4月21日，76歲的林建名爆出跟年輕45年的[苟芸慧譜父女戀](../Page/苟芸慧.md "wikilink")。

2014年7月18日，76歲的林建名爆出跟年輕51年的[何傲兒譜爺孫戀](../Page/何傲兒.md "wikilink")。

2016年6月10日，78歲的林建名爆出跟年輕54年的台妹施小姐譜爺孫戀。

2018年8月15日，81歲的林建名爆出跟年輕60年的蘿莉女友譜爺孫戀。

## 軼事

林建名曾經在[香港傳媒自行揭秘其風流艷史](../Page/香港傳媒.md "wikilink")，成為城中熱門話題\[2\]。

## 名下馬匹

林建名、林煒珊父女為[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")、馬主，名下馬匹多以「大少」命名，包括退役馬皇家至寶、恩威、開心大少、嘻哈大少、-{瀟洒大少}-、威威大少、精靈大少及現役馬匹娛樂大少、鱷臨天下。

## 參看

  - [林百欣家族](../Page/林百欣家族.md "wikilink")
  - [鱷魚恤](../Page/鱷魚恤.md "wikilink")

## 参考

## 外部链接

  - [林百欣中心案](https://web.archive.org/web/20070929145158/http://www.epochtimes.com/b5/1/5/14/n88404.htm)
  - [林建名名下馬匹](http://www.hkjc.com/chinese/racing/ownersearch.asp?horseowner=林建名)
  - [鱷魚恤有限公司官方網址](http://www.crocodile.com.hk/)
  - [大名娛樂 Big Honor Entertainment](http://www.bighonor.com.hk/)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港足球界人士](../Category/香港足球界人士.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:潮商](../Category/潮商.md "wikilink")
[Category:广州市天河区政协委员](../Category/广州市天河区政协委员.md "wikilink")
[Category:拔萃男書院校友](../Category/拔萃男書院校友.md "wikilink")
[Category:港九潮州公會中學校友](../Category/港九潮州公會中學校友.md "wikilink")
[Category:潮阳人](../Category/潮阳人.md "wikilink")
[Category:香港潮汕人](../Category/香港潮汕人.md "wikilink")
[Jian建](../Category/林百欣家族.md "wikilink")
[J](../Category/林姓.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")

1.  [趙碩之化身馮程程賀壽](http://orientaldaily.on.cc/cnt/entertainment/20120901/00282_046.html)
2.