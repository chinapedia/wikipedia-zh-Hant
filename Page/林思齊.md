**林思齊**，[OC](../Page/加拿大官佐勳章.md "wikilink")，[CVO](../Page/皇家維多利亞司令勳章.md "wikilink")，<small></small>（****，），[加拿大](../Page/加拿大.md "wikilink")[商人及](../Page/商人.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")，1988年至1995年間出任[卑詩省](../Page/卑詩省.md "wikilink")[省督](../Page/卑詩省省督.md "wikilink")，是[加拿大史上首位](../Page/加拿大.md "wikilink")[華人](../Page/華人.md "wikilink")[省督](../Page/省督_\(加拿大\).md "wikilink")，以及第二位非[高加索人省督](../Page/高加索人种.md "wikilink")。

作為[嘉華銀行創辦人](../Page/嘉華銀行.md "wikilink")[林子豐博士的次子](../Page/林子豐.md "wikilink")，林思齊原本在[香港生活](../Page/香港.md "wikilink")，並在嘉華銀行任職。在1967年的「[六七暴動](../Page/六七暴動.md "wikilink")」後，他與妻女移民加拿大，經過一番努力，在[房地產市場上賺取豐厚利潤](../Page/房地產.md "wikilink")。林思齊是一位虔誠的[基督徒](../Page/基督徒.md "wikilink")，晚年特別專注慈善公益，在加拿大和卑詩省熱心捐款和支持文教發展，嗣後獲委任為卑詩省省督。他任內的主要貢獻包括致力於推動當地[種族融和](../Page/種族融和.md "wikilink")、參與創立[卑詩省勳章](../Page/卑詩省勳章.md "wikilink")、以及在1994年招待到訪出席[英聯邦運動會的](../Page/英聯邦運動會.md "wikilink")[英女皇](../Page/英國君主.md "wikilink")[伊利沙伯二世等等](../Page/伊利沙伯二世.md "wikilink")。另外在1991年，他有份在背後出面，勸退捲入[利益衝突醜聞的時任](../Page/利益衝突.md "wikilink")[卑詩省省長](../Page/卑詩省省長.md "wikilink")[溫德心](../Page/溫德心.md "wikilink")。

## 生平

### 早年生涯

林思齊祖籍[廣東](../Page/廣東.md "wikilink")[汕頭](../Page/汕頭.md "wikilink")[揭陽縣](../Page/揭陽縣.md "wikilink")[金坑](../Page/金坑.md "wikilink")，\[1\]1923年9月2日生於[香港](../Page/香港.md "wikilink")。\[2\]來自[潮州人](../Page/潮州人.md "wikilink")[浸信會家庭的林思齊](../Page/浸信會.md "wikilink")，祖父[林紹勳是浸信會](../Page/林紹勳.md "wikilink")[牧師和鄉村教師](../Page/牧師.md "wikilink")，\[3\]父親[林子豐博士是香港](../Page/林子豐.md "wikilink")[銀行家與](../Page/銀行家.md "wikilink")[教育家](../Page/教育家.md "wikilink")，1922年創辦[嘉華銀行](../Page/嘉華銀行.md "wikilink")，後來先後創立[香港浸會學院和](../Page/香港浸會學院.md "wikilink")[浸會醫院](../Page/浸會醫院.md "wikilink")，亦曾出任浸信會的[香港培正中學校長](../Page/香港培正中學.md "wikilink")。\[4\]林子豐與妻子陳植亭共育有七子兩女，其中林思齊是家中次子。林思齊的長兄[林思顯博士](../Page/林思顯.md "wikilink")（1922年－2008年）先後擔任過香港[市政局議員和](../Page/市政局_\(香港\).md "wikilink")[立法局非官守議員](../Page/香港立法局.md "wikilink")、嘉華銀行董事局主席和[香港浸會大學校董會主席等職](../Page/香港浸會大學.md "wikilink")。\[5\]

林思齊少時居於[九龍塘](../Page/九龍塘.md "wikilink")，\[6\]入讀[香港培正小學](../Page/香港培正中學.md "wikilink")，中學就讀於[民生書院](../Page/民生書院.md "wikilink")。\[7\]1936年，本身已懂[廣東話和](../Page/廣東話.md "wikilink")[潮州話的林思齊隨父母到](../Page/潮州話.md "wikilink")[福建](../Page/福建.md "wikilink")[鼓浪嶼參加](../Page/鼓浪嶼.md "wikilink")《[聖經](../Page/聖經.md "wikilink")》查經班，逗留數月，學會了[國語和](../Page/國語.md "wikilink")[閩南話](../Page/閩南話.md "wikilink")。\[8\]在1940年，他奉父親之命到[越南](../Page/越南.md "wikilink")[西貢公幹](../Page/胡志明市.md "wikilink")，期間被[法國軍車意外撞倒](../Page/法國.md "wikilink")，腿部受傷一度有生命危險。\[9\]返港後，林思齊入讀[廣州](../Page/廣州.md "wikilink")[嶺南大學](../Page/嶺南大學_\(廣州\).md "wikilink")，主修[經濟](../Page/經濟.md "wikilink")，1947年畢業。\[10\]他在畢業後負笈[美國](../Page/美國.md "wikilink")[賓夕凡尼亞州](../Page/賓夕凡尼亞州.md "wikilink")，於[天普大學攻讀](../Page/天普大學.md "wikilink")[工商管理碩士](../Page/工商管理碩士.md "wikilink")，\[11\]1948年畢業後再於[紐約大學研究經濟學](../Page/紐約大學.md "wikilink")，但未及畢業，便中途於1950年返回香港。\[12\]

### 加國發展

林思齊返港後居於[沙田](../Page/沙田.md "wikilink")，\[13\]主要負責打理父親的嘉華銀行和出任總經理一職，另外亦從事工商和教育等各方面的工作，亦曾在[1957年香港市政局選舉中參選](../Page/1957年香港市政局選舉.md "wikilink")，但落敗。\[14\]可是在1967年，香港爆發由[左派發動的](../Page/左派.md "wikilink")「[六七暴動](../Page/六七暴動.md "wikilink")」，林思齊有感香港前途暗淡，遂決定與妻女隻身移民[加拿大發展](../Page/加拿大.md "wikilink")。\[15\]\[16\]至於林子豐家族亦無心經營嘉華銀行，先在1970年將銀行轉售予[美國財團](../Page/美國.md "wikilink")，後來再於1974年落到[新加坡僑商](../Page/新加坡.md "wikilink")[劉燦松手上](../Page/劉燦松.md "wikilink")，林子豐家族逐漸淡出銀行業務。\[17\]

其實早於1961年，林思齊曾遊歷加拿大[卑詩省省會](../Page/卑詩省.md "wikilink")[維多利亞](../Page/維多利亞_\(卑詩\).md "wikilink")，深被當地美麗的景緻吸引，因此，林思齊一家移民加拿大後，亦決定定居卑詩省。可是，林思齊移民加拿大時已經44歲，\[18\]要在當地創一番事業，有一定困難。起初，他與家人租住只有一個睡房的套房，帶來的六萬元[加拿大元很快便花了一半](../Page/加拿大元.md "wikilink")，再加上初來的時候找不到工作，令生活一度陷於拮据。\[19\]\[20\]

雖然如此，本身在香港從事銀行業務的林思齊立志投身[房地產市場](../Page/房地產.md "wikilink")，他除了閱讀大量關於房地產方面的書籍外，\[21\]又報讀[卑詩省大學夜校的房地產估值課程](../Page/卑詩省大學.md "wikilink")，隨後於1972年考獲文憑。\[22\]另一方面，他與友人[陳俊在](../Page/陳俊_\(商人\).md "wikilink")1968年創立[新利華地産發展公司](../Page/新利華地産發展公司.md "wikilink")（Sunneyvale
Development
Ltd.），正式進軍加國地產市場。\[23\]透過家族關係，他成功遊說包括[霍英東](../Page/霍英東.md "wikilink")、[李兆基和](../Page/李兆基.md "wikilink")[馮景禧在內的多位香港商人](../Page/馮景禧.md "wikilink")，\[24\]由他們出資投資加國地產，再成立20多家地產公司，讓他們各自出任公司主席，自己則出任總經理，親自打理業務，從而掌握大筆資金，在房地產市場上賺取豐厚利潤。\[25\]

[Chinese_Garden.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Garden.jpg "fig:Chinese_Garden.jpg")[中山公園](../Page/中山公園_\(溫哥華\).md "wikilink")\]\]
在1972年，林思齊正式取得[加拿大公民身份](../Page/加拿大公民.md "wikilink")，\[26\]並在翌年創立[加拿大國際房地産公司](../Page/加拿大國際房地産公司.md "wikilink")（Canadian
International Properties
Ltd.）。\[27\]當時正值國際[石油危機](../Page/石油危機.md "wikilink")，[美國房地產市道低迷](../Page/美國.md "wikilink")，林思齊遂趁機打算以400萬[美元購入](../Page/美元.md "wikilink")[三藩市市中心的](../Page/三藩市.md "wikilink")[保險交易大廈](../Page/保險交易大廈_\(三藩市\).md "wikilink")（Insurance
Exchange
Building），可是，正要購入大廈前夕，他卻被一名[猶太商人捷足先登](../Page/猶太.md "wikilink")，出價425萬美元買入大廈。\[28\]九個月後，猶太富商再放售大廈，這次林思齊向富商提出以年息七厘、七年借期的形式借入540萬美元，然後再以同等價錢購入大廈。\[29\]林思齊的建議獲得富商接納，最終令大廈落入他的手上。\[30\]未幾，石油危機化解，房地產市道急速反彈，林思齊再以2,250萬美元把保險交易大廈售予[英國地產發展商](../Page/英國.md "wikilink")，前後的帳面收入高達1,710萬美元。\[31\]\[32\]

林思齊投身房地產市場的十多年間，業務擴展至整個[北美西部](../Page/北美.md "wikilink")，累積總收入逾一億加元，\[33\]成為卑詩省首富之一。\[34\]但是在1982年，年屆60歲的林思齊選擇全身而退，\[35\]成立林思齊博士夫婦基金會（Dr.
and Mrs. David Lam
Foundation），專注於慈善公益事業。\[36\]\[37\]在數年之間，他向卑詩省大學、[維多利亞大學和](../Page/維多利亞大學.md "wikilink")[維真神學院等多間學府分別捐贈](../Page/維真神學院.md "wikilink")100萬元，\[38\]另外又在1983年出資為溫哥華籌建[中山公園](../Page/中山公園_\(溫哥華\).md "wikilink")、\[39\]1985年為翌年舉行的[溫哥華世界博覽會出資興建加拿大館](../Page/1986年世界博覽會.md "wikilink")、\[40\]以及在1988年為[西門菲莎大學成立國際交流中心](../Page/西門菲莎大學.md "wikilink")，該中心後改稱[林思齊中心](../Page/林思齊中心.md "wikilink")，以答謝他對中心的資助。\[41\]

林思齊對卑詩省大學的捐助尤其繁多，他在1985年捐建林思齊管理研究圖書館（David Lam Management Research
Library），圖書館在1992年再獲林思齊捐助，擴建成為林思齊管理研究中心（David Lam Management Research
Centre）；\[42\]熱愛園藝的他又為該校植物園捐建林思齊亞洲花園（David C. Lam Asian
Garden），校內其他獲林思齊資助的，還包括林思齊多元文化教育教席（David Lam Chair
in Multicultural Education）、以及林陳坤儀特殊教育教席（Dorothy Lam Chair in Special
Education）等。\[43\]在香港方面，林思齊也曾出資興建[浸會學院的東西學術交流中心](../Page/浸會學院.md "wikilink")，中心後來於1993年11月更名為[林思齊東西學術交流研究所](../Page/林思齊東西學術交流研究所.md "wikilink")。\[44\]

### 省督生涯

由於在商界上的突出表現，以及對加拿大社會作重要貢獻，林思齊在1988年於[渥太華獲聯邦政府授予](../Page/渥太華.md "wikilink")[加拿大員佐勳章](../Page/加拿大員佐勳章.md "wikilink")，同年7月，他獲[加拿大總理](../Page/加拿大總理.md "wikilink")[莫朗尼提名出任](../Page/马丁·布赖恩·马尔罗尼.md "wikilink")[卑詩省省督](../Page/卑詩省省督.md "wikilink")，\[45\]林思齊隨後於9月9日正式上任，\[46\]成為加拿大全國歷來首位[華人省督](../Page/華人.md "wikilink")，\[47\]也是加國第二位非[白人省督](../Page/白人.md "wikilink")，\[48\]而加拿大首位非白人省督則是在1985年至1991年間出任[安大略省省督的](../Page/安大略省.md "wikilink")[林肯·亞歷山大](../Page/林肯·亞歷山大.md "wikilink")（Lincoln
Alexander）。

林思齊在任內熱心履行省督職務，出席的公開活動多不勝數，招待各方賓客多達80,000人，\[49\]發表演說次數亦不下千次。\[50\]此外，他還致力於推動種族融和，並探訪卑詩省內的每一個社區，加深各族裔相互的了解。\[51\]\[52\]\[53\]當時適值[香港主權移交前的](../Page/香港.md "wikilink")[移民潮高峰期](../Page/移民潮.md "wikilink")，林思齊遂協助初到加國的[華人融入新生活](../Page/華人.md "wikilink")，在1989年，他委託影星[岳華與](../Page/岳華.md "wikilink")[恬妮夫婦拍攝關於加國的短片](../Page/恬妮.md "wikilink")，短片在加拿大駐港領事館播放，規定獲准移民的[港人必須收看](../Page/港人.md "wikilink")，以便對加國生活有進一步了解。\[54\]

林思齊其他的任內事蹟還包括在1988年應[卑詩省省長](../Page/卑詩省省長.md "wikilink")[溫德心](../Page/溫德心.md "wikilink")（Bill
Vander
Zalm）建議，創立[卑詩省勳章](../Page/卑詩省勳章.md "wikilink")，並兼任勳章總管（Chancellor），該勳章是卑詩省政府所能頒授的最高級民事榮譽，勳章只設一等，專門授予對卑詩省有傑出貢獻的人士。在1989年，熱愛[園藝的林思齊邀請公眾和志願者參與栽種](../Page/園藝.md "wikilink")[省督府花園的植物](../Page/省督府_\(卑詩省\).md "wikilink")，修復早前冬天寒冷天氣對花園帶來的破壞。\[55\]\[56\]

在七年省督生涯中，林思齊先後經歷[溫德心](../Page/溫德心.md "wikilink")、[莊思頓](../Page/莊思頓.md "wikilink")（Rita
Johnston）和[哈葛](../Page/哈葛.md "wikilink")（Michael
Harcourt）三任省長。\[57\]其中在1990年，溫德心因出售持有的[奇幻花園](../Page/奇幻花園.md "wikilink")（Fantasy
Gardens）遊樂場予[台灣富商](../Page/台灣.md "wikilink")，捲入轟動一時的[利益衝突醜聞](../Page/利益衝突.md "wikilink")。\[58\]事件一度令卑詩省政府陷入憲制危機，林思齊諮詢法律意見後，決定在背後出面勸退溫德心，最終促使溫德心在1991年4月下野。\[59\]

在1994年8月，卑詩省省會維多利亞主辦[英聯邦運動會](../Page/英聯邦運動會.md "wikilink")，林思齊期間負責招待到訪出席的[英女皇](../Page/英國君主.md "wikilink")[伊利沙伯二世](../Page/伊利沙伯二世.md "wikilink")、皇夫[菲臘親王和](../Page/菲臘親王.md "wikilink")[愛德華王子](../Page/愛德華王子_\(威塞克斯伯爵\).md "wikilink")，並獲英女皇授予[CVO勳銜](../Page/皇家維多利亞司令勳章.md "wikilink")。\[60\]林思齊在1995年8月21日卸任省督一職，\[61\]為表揚其任內貢獻，他在同年再獲聯邦政府授予[加拿大官佐勳章](../Page/加拿大官佐勳章.md "wikilink")，另外又獲勳[卑詩省勳章](../Page/卑詩省勳章.md "wikilink")。\[62\]卑詩省內亦有多處地方以他命名，當中包括位於[耶魯鎮的](../Page/耶魯鎮.md "wikilink")[林思齊公園](../Page/林思齊公園.md "wikilink")（David
Lam Park）、[道格拉斯學院](../Page/道格拉斯學院.md "wikilink")（Douglas
College）位於[高貴林的林思齊校園](../Page/高貴林.md "wikilink")（David
Lam Campus）和維多利亞大學的林思齊演講廳（David Lam Auditorium）等。\[63\]\[64\]\[65\]

### 晚年生涯

林思齊晚年仍熱心於公益活動，並繼續參與[中僑互助會的活動](../Page/中僑互助會.md "wikilink")。雖然他在2005年證實患上[前列腺癌](../Page/前列腺癌.md "wikilink")，但仍堅持參與慈善事業，除了在2007年出資修復[史丹利公園外](../Page/史丹利公園.md "wikilink")，\[66\]又在2009年出資為[2010年溫哥華奧林匹克冬季運動會興建](../Page/2010年冬季奧林匹克運動會.md "wikilink")[速滑館](../Page/列治文奧林匹克橢圓速滑館.md "wikilink")，以及在館旁種植[櫻花樹](../Page/櫻花.md "wikilink")。\[67\]林思齊的病情由2009年起開始惡化，[心瓣膜更出現功能異常要接受大型](../Page/心瓣膜.md "wikilink")[手術](../Page/手術.md "wikilink")，\[68\]同年，他當選為傑出華裔創業家，但因病無法親自到[多倫多領獎](../Page/多倫多.md "wikilink")。\[69\]林思齊在2010年11月22日零時20分，因病在溫哥華家中病逝，終年87歲，死時三名女兒皆陪伴在側。\[70\]

他去世後，[加拿大總理](../Page/加拿大總理.md "wikilink")[史蒂芬·哈珀](../Page/史蒂芬·哈珀.md "wikilink")、卑詩省省督[史蒂芬·波因特](../Page/史蒂芬·波因特.md "wikilink")（Steven
Point）、省長[戈登·坎贝尔](../Page/戈登·坎贝尔.md "wikilink")（Gordon
Campbell）和[溫哥華市長](../Page/溫哥華.md "wikilink")[羅品信](../Page/羅品信.md "wikilink")（Gregor
Robertson）皆發聲明致悼，\[71\]\[72\]\[73\]而省內多處學府亦下半旗致哀。\[74\]林思齊的喪禮在11月27日於[列治文舉行](../Page/列治文.md "wikilink")，\[75\]遺體隨後進行[火化](../Page/火化.md "wikilink")，骨灰與亡妻一樣撒落到溫哥華的[英吉列灣](../Page/英吉列灣.md "wikilink")。\[76\]\[77\]

## 個人生活

林思齊在1954年於香港娶[陳坤儀為妻](../Page/陳坤儀.md "wikilink")，\[78\]陳坤儀畢業於[香港大學](../Page/香港大學.md "wikilink")，任職[教師](../Page/教師.md "wikilink")，\[79\]夫婦兩人共育有三名女兒，分別名慕佳、慕芬和慕慈。\[80\]林思齊在1988年至1995年擔任省督期間，林陳坤儀亦以省督夫人身心熱心出席公開場合，兩人在1991年和1995年分別一同獲[西門菲莎大學和](../Page/西門菲莎大學.md "wikilink")[維多利亞大學頒授榮譽法學博士學位](../Page/維多利亞大學.md "wikilink")。\[81\]\[82\]林陳坤儀在1997年因肺癌逝世，終年67歲，其逝世對林思齊的晚年構成一大打擊。\[83\]

林思齊是虔誠的[浸信會教徒](../Page/浸信會.md "wikilink")，且愛好[園藝和講故事](../Page/園藝.md "wikilink")。另一方面，在1986年的溫哥華世博期間，林思齊率先為加國引入[龍舟競渡](../Page/龍舟.md "wikilink")。在1989年，他復與商人[黃光遠成立](../Page/黃光遠.md "wikilink")[加拿大國際龍舟節](../Page/加拿大國際龍舟節.md "wikilink")（Canadian
International Dragon Boat
Festival），自此，每年6月舉辦龍舟競渡成為溫哥華的一項傳統。\[84\]在晚年，林思齊又捐建龍舟中心，促進龍舟活動的發展。\[85\]

[David_Lam_Arms.svg](https://zh.wikipedia.org/wiki/File:David_Lam_Arms.svg "fig:David_Lam_Arms.svg")

## 榮譽

### 殊勳

  - 以下列出榮譽全稱及縮寫：
      - [加拿大員佐勳章](../Page/加拿大員佐勳章.md "wikilink")（C.M.） （1988年）
      - [聖約翰爵級司令勳章](../Page/聖約翰爵級司令勳章.md "wikilink")（K.St.J） （1988年）
      - [皇家維多利亞司令勳章](../Page/皇家維多利亞司令勳章.md "wikilink")（C.V.O.）
        （1994年8月15日<ref>"Issue 53798", *London Gazette*, 23
        September 1994, p.1.

</ref>）

  -   - [加拿大官佐勳章](../Page/加拿大官佐勳章.md "wikilink")（O.C.） （1995年）

      - （O.B.C.） （1995年）

### 榮譽學位

  - [卑詩省大學](../Page/卑詩省大學.md "wikilink") （1987年\[86\]）
  - [貝勒大學](../Page/貝勒大學.md "wikilink") （美國，1990年\[87\]）
  - [西門菲莎大學](../Page/西門菲莎大學.md "wikilink") （1991年\[88\]）
  - [東部學院](../Page/東部大學_\(美國\).md "wikilink") （美國，今東部大學，1991年\[89\]）
  - [皇家漢樑軍事學院](../Page/皇家漢樑大學.md "wikilink") （今皇家漢樑大學，1991年\[90\]）
  - [香港浸會學院](../Page/香港浸會學院.md "wikilink") （香港，今香港浸會大學，1992年\[91\]）
  - [維多利亞大學](../Page/維多利亞大學.md "wikilink") （1995年）

### 以他命名的事物

  - 林思齊博士夫婦基金會（Dr. and Mrs. David Lam Foundation）
  - [林思齊公園](../Page/林思齊公園.md "wikilink")（David Lam
    Park）：位於[耶魯鎮](../Page/耶魯鎮.md "wikilink")
  - [林思齊中心](../Page/林思齊中心.md "wikilink")（David Lam
    Centre）：位於[西門菲莎大學](../Page/西門菲莎大學.md "wikilink")
  - 林思齊管理研究中心（David Lam Chair in Multicultural
    Education）：位於[卑詩省大學](../Page/卑詩省大學.md "wikilink")，舊稱林思齊管理研究圖書館
  - 林思齊亞洲花園（David C. Lam Asian Garden）：位於卑詩省大學植物園內
  - 林思齊多元文化教育教席（David Lam Chair in Multicultural
    Education）及林陳坤儀特殊教育教席（Dorothy Lam Chair in
    Special Education）：由卑詩省大學設立
  - 林思齊演講廳（David Lam
    Auditorium）：位於[維多利亞大學](../Page/維多利亞大學.md "wikilink")
  - 林思齊伉儷學生綜合宿舍（David and Dorothy Lam Family Student Housing
    Complex）：位於維多利亞大學，由英聯邦運動會前選手村改建，宿舍旁邊設有林思齊迴旋處（Lam
    Circle）\[92\]
  - 林思齊校園（David Lam Campus）：位於高貴林，屬於道格拉斯學院
  - [林思齊東西學術交流研究所](../Page/林思齊東西學術交流研究所.md "wikilink")（David C. Lam
    Institute for East-West
    Studies）：位於[香港浸會大學](../Page/香港浸會大學.md "wikilink")
  - 思齊樓（David C.Lam Building）：位於香港浸會大學

## 相關條目

  - [卑詩省大學](../Page/卑詩省大學.md "wikilink")
  - [林佐民](../Page/林佐民.md "wikilink")
  - [李紹麟](../Page/李紹麟.md "wikilink")
  - [伍冰枝](../Page/伍冰枝.md "wikilink")
  - [林子豐](../Page/林子豐.md "wikilink")
  - [嘉華銀行](../Page/嘉華銀行.md "wikilink")

## 注腳

## 參考資料

### 英文資料

<div style="font-size:small">

  - *[Ka Wah Bank - Standing Tall in Hong
    Kong](http://cslow.files.wordpress.com/2010/05/kah-wah-euromoney-1984.pdf)*.
    Hong Kong: Ka Wah Bank, June 1984.
  - "[Honorary Degree Citations - The degree of Doctor of Laws, honoris
    causa, conferred on Dr. David See-Chai Lam and Dorothy Kwan- Yee Tan
    Lam](http://www.sfu.ca/content/dam/sfu/ceremonies/HDRs/honorary-degrees/David_See-Chai_Lam_and_Dorothy_Kwan-Yee_Tan_Lam_Citation.pdf)",
    *SIMON FRASER UNIVERSITY*, 6 June 1991.
  - "[Issue 53798](https://www.thegazette.co.uk/London/issue/53798/page/13403)",
    *London Gazette*, 23 September 1994, p. 1.
  - "[Head of
    State](http://books.google.com.hk/books?id=G1opLEH8OmIC&pg=PA208&dq=%22Lam,+David%22++%22governor%22&hl=zh-TW&ei=5ZvvTI-uH4K3cM6R_f0J&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCoQ6AEwAA#v=onepage&q=%22Lam%2C%20David%22%20%20&f=false)",
    *Saltwater City: An Illustrated History of the Chinese in
    Vancouver*, Douglas & McIntyre, 2006.
  - "[Former B.C. lieutenant-governor David Lam
    dies](https://web.archive.org/web/20130623080843/http://www.cbc.ca/news/canada/british-columbia/story/2010/11/22/bc-david-lam.html)",
    *CBC News*, 22 November 2010.
  - "[Lam, Canada's first ethnic Chinese L-G dies,
    age 87](http://www.theglobeandmail.com/news/british-columbia/lam-canadas-first-ethnic-chinese-l-g-dies-age-87/article1315080/)",
    *The Globe and Mail*, 22 November 2010.
  - "[Tribute to David
    Lam](http://communications.uvic.ca/uvicinfo/announcement.php?id=450)",
    *University of Victoria*, 22 November 2010.
  - "[Sauder mourns loss of friend and supporter David
    Lam](https://web.archive.org/web/20110907081514/http://www.sauder.ubc.ca/News/News_Listing/News_Item_-_David_Lam)",
    *Sauder School of Business*, 23 November 2010.
  - "[Douglas mourns passing of David
    Lam](https://web.archive.org/web/20110102170037/http://www.douglas.bc.ca/news/passing-david-lam.html)",
    *Douglas College*, 23 November 2010.
  - "<https://web.archive.org/web/20110212003237/http://dragonboatbc.ca/who-we-are/history/>
    History\]", *The Canadian International Dragon Boat Festival
    Society*, retrieved on 28 November 2010.
  - "[Background](https://web.archive.org/web/20100918020306/http://www.hkbu.edu.hk/~lewi/about_abtlewi.html)",
    *David C. Lam Institute for East-West Studies*, retrieved on 28
    November 2010.
  - "[British
    Columbia](http://www.worldstatesmen.org/Canada_Provinces_A-O.html#British)",
    *WorldStatesmen.org*, retrieved on 28 November 2010.
  - "[In Memoriam](http://www.sfu.ca/davidlamcentre/)", *David Lam
    Centre*, retrieved on 28 November 2010.
  - "[David See-Chai
    Lam](https://web.archive.org/web/20120319215253/http://www.ltgov.bc.ca/ltgov/former/ltgov/DavidLam.htm)",
    *Government House, Victoria, British Columbia, Canada*, retrieved on
    28 November 2010.
  - "[Lam, David
    See-Chai](https://web.archive.org/web/20150702054920/http://www.thecanadianencyclopedia.ca/en/article/david-see-chai-lam/)",
    *Canadian Encyclopedia*, retrieved on 28 November 2010.
  - "[David Lam - A Man who made a
    Difference](https://web.archive.org/web/20091125131600/http://www.concordpacific.com/livingmagazine/living2/volume1%2C_issue_2.html)",
    *Where Vancouver is Going\!*, retrieved on 28 November 2010.

</div>

### 中文資料

<div style="font-size:small">

  - 〈[林思齊博士：貢獻是我最大的成就](https://web.archive.org/web/20130323152002/http://newnews.ca/?action-viewnews-itemid-2644)〉，《加拿大傑出華裔創業家2009》，2009年。
  - 〈[林思齊：加國首位華裔及第二位非白人省督](https://web.archive.org/web/20130323152002/http://newnews.ca/?action-viewnews-itemid-2644)〉，《加拿大新聞商業網》，2009年4月25日。
  - 〈[1967年香港暴動，攜妻女移居溫市](http://news.singtao.ca/calgary/2010-01-04/canada1262603374d2240944.html)〉，《星島日報》卡加利版，2010年1月4日。
  - 〈[環球華報人物專訪：加國首位華裔省督傳奇人生](https://web.archive.org/web/20100421092547/http://www.gcpnews.com/zh-tw/articles/2010-03-11/C1063_50132.html)〉，《[環球華報](../Page/環球華報.md "wikilink")》，2010年3月11日。
  - 〈[體會人生、慢慢想通、林思齊小史](http://news.singtao.ca/toronto/2010-11-22/headline1290422796d2859533.html)〉，《[星島日報](../Page/星島日報.md "wikilink")》多倫多版，2010年11月22日。
  - 趙永泰，〈[前卑詩省省督林思齊博士病逝、終年87歲、「回到主身邊好得無比」](http://upwill.org/article/5403lam.html)〉，《可圈可點》，2010年11月23日。
  - 〈[林思齊病逝：首位華人省督
    締移民傳奇](https://web.archive.org/web/20110131143349/http://www.mingpaovan.com/htm/news/20101123/vaa1h.htm)〉，《[明報](../Page/明報.md "wikilink")》加西版，2010年11月23日。
  - 〈[林思齊典範長存，享年87歲，周六舉殯](http://news.singtao.ca/vancouver/2010-11-23/headline1290512802d2862515.html)〉，《星島日報》溫哥華版，2010年11月23日。
  - 〈[林思齊遺產預計超10億，每年都捐百萬元](https://archive.is/20130425041212/http://www.bcbay.com/news/newsViewer.php?nid=34049&id=44760&language=big5)〉，《星島日報》溫哥華版，2010年11月23日。
  - 〈[卑詩前省督林思齊安息禮拜今天舉行](http://news.singtao.ca/toronto/2010-11-27/headline1290853644d2869743.html)〉，《星島日報》多倫多版，2010年11月27日。

</div>

## 延伸閱讀

  - ROY, Reginald H., *David Lam : A Biography*, Douglas & McIntyre,
    1996. ISBN 1-55054-511-6

## 外部連結

  - [加拿大總理悼詞](https://archive.is/20130101050200/http://www.pm.gc.ca/eng/media.asp?category=3&featureId=6&pageId=49&id=3801)
  - [卑詩省省督悼詞](https://web.archive.org/web/20110516215016/http://www.ltgov.bc.ca/events/media-releases/2010/nov/statement_Lam.pdf)
  - [卑詩省大學悼詞](https://web.archive.org/web/20120316145914/http://president.ubc.ca/2010/11/26/david-c-lam/)
  - [維多利亞大學悼詞](http://communications.uvic.ca/uvicinfo/announcement.php?id=450)
  - [道格拉斯學院悼詞](https://web.archive.org/web/20110102170037/http://www.douglas.bc.ca/news/passing-david-lam.html)
  - 〈[林思齊走了](http://news.singtao.ca/vancouver/2010-11-23/headline1290512857d2862517.html)〉，《星島日報》溫哥華版，2010年11月23日

[Category:加拿大华裔政治人物](../Category/加拿大华裔政治人物.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:加拿大省督](../Category/加拿大省督.md "wikilink")
[Category:加拿大企業家](../Category/加拿大企業家.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[Category:天普大學校友](../Category/天普大學校友.md "wikilink")
[Category:紐約大學校友](../Category/紐約大學校友.md "wikilink")
[Category:民生書院校友](../Category/民生書院校友.md "wikilink") [Category:嶺南大學校友
(廣州)](../Category/嶺南大學校友_\(廣州\).md "wikilink")
[Category:加拿大基督徒](../Category/加拿大基督徒.md "wikilink")
[林](../Category/香港新教徒.md "wikilink")
[Category:歸化加拿大公民](../Category/歸化加拿大公民.md "wikilink")
[Category:温哥華人](../Category/温哥華人.md "wikilink")
[Category:揭阳人](../Category/揭阳人.md "wikilink")
[Category:香港潮汕人](../Category/香港潮汕人.md "wikilink")
[Category:海外港人](../Category/海外港人.md "wikilink")
[S思齊](../Category/林姓.md "wikilink")
[Category:林子豐家族](../Category/林子豐家族.md "wikilink")
[Category:加拿大房地产商](../Category/加拿大房地产商.md "wikilink")

1.
2.  "Lam, David See-Chai", *Canadian Encyclopedia*, retrieved on 28
    November 2010.

3.  趙永泰，〈前卑詩省省督林思齊博士病逝、終年87歲、「回到主身邊好得無比」〉，《可圈可點》，2010年11月23日。

4.
5.  *Ka Wah Bank - Standing Tall in Hong Kong*, June 1984.

6.  〈1967年香港暴動，攜妻女移居溫市〉，《星島日報》卡加利版，2010年1月4日。

7.
8.  〈體會人生、慢慢想通、林思齊小史〉，《[星島日報](../Page/星島日報.md "wikilink")》多倫多版，2010年11月22日。

9.
10.
11. "Honorary Degree Citations - The degree of Doctor of Laws, honoris
    causa, conferred on Dr. David See-Chai Lam and Dorothy Kwan- Yee Tan
    Lam", *SIMON FRASER UNIVERSITY*, 6 June 1991.

12.
13.
14. 〈林思齊遺產預計超10億，每年都捐百萬元〉，《星島日報》溫哥華版，2010年11月23日。

15.
16.
17.
18.
19.
20.
21.
22.
23.
24. 〈環球華報人物專訪：加國首位華裔省督傳奇人生〉，《[環球華報](../Page/環球華報.md "wikilink")》，2010年3月11日。

25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38. "Head of State", *Saltwater City: An Illustrated History of the
    Chinese in Vancouver*, Douglas & McIntyre, 2006.

39.
40.
41. "In Memoriam", *David Lam Centre*, retrieved on 28 November 2010.

42. "Sauder mourns loss of friend and supporter David Lam", *Sauder
    School of Business*, 23 November 2010.

43.
44. "Background", *David C. Lam Institute for East-West Studies*,
    retrieved on 28 November 2010.

45. 〈林思齊博士：貢獻是我最大的成就〉，《加拿大傑出華裔創業家2009》，2009年。

46. "British Columbia", *WorldStatesmen.org*, retrieved on 28 November
    2010.

47. "Lam, Canada’s first ethnic Chinese L-G dies, age 87", *The Globe
    and Mail*, 22 November 2010.

48.
49.
50.
51.
52. "David Lam - A Man who made a Difference", *Where Vancouver is
    Going\!*, retrieved on 28 November 2010.

53. "Former B.C. lieutenant-governor David Lam dies", *CBC News*, 22
    November 2010.

54.
55.
56.
57.
58.
59.
60.
61.
62.
63.
64.
65.
66.
67.
68.
69.
70.
71.
72.
73.
74. "Douglas mourns passing of David Lam", *Douglas College*, 23
    November 2010.

75. 〈林思齊典範長存，享年87歲，周六舉殯〉，《星島日報》溫哥華版，2010年11月23日。

76. 〈林思齊病逝：首位華人省督
    締移民傳奇〉，《[明報](../Page/明報.md "wikilink")》加西版，2010年11月23日。

77. 〈卑詩前省督林思齊安息禮拜今天舉行〉，《星島日報》多倫多版，2010年11月27日。

78.
79.
80.
81.
82. "Tribute to David Lam", *University of Victoria*, 22 November 2010.

83.
84. "History", *The Canadian International Dragon Boat Festival
    Society*, retrieved on 28 November 2010.

85.
86. "David See-Chai Lam", *Government House, Victoria, British Columbia,
    Canada*, retrieved on 28 November 2010.

87.
88.
89.
90.
91.
92.