**林懷民**（），臺灣[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉人](../Page/新港鄉.md "wikilink")，現代[舞蹈表演團體](../Page/舞蹈.md "wikilink")[雲門舞集創辦人與藝術總監](../Page/雲門舞集.md "wikilink")，[作家](../Page/作家.md "wikilink")、[舞蹈家與](../Page/舞蹈家.md "wikilink")[編舞家](../Page/編舞家.md "wikilink")。林懷民為[國立政治大學](../Page/國立政治大學.md "wikilink")[新聞](../Page/新聞.md "wikilink")[學士](../Page/學士.md "wikilink")、[美國](../Page/美國.md "wikilink")[愛荷華大學](../Page/愛荷華大學.md "wikilink")[藝術](../Page/藝術.md "wikilink")[碩士](../Page/碩士.md "wikilink")。2006年獲選為[Discovery頻道](../Page/Discovery.md "wikilink")《台灣人物誌》的6名主角之1。

## 生平

  - 1947年，出生於臺灣臺南縣嘉義區新港鄉（今[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉](../Page/新港鄉.md "wikilink")），曾祖父林維朝祖籍漳州龍溪，1887年（清光緒十三年）[秀才](../Page/秀才.md "wikilink")。祖父林開泰為留學[日本的醫生](../Page/日本.md "wikilink")\[1\]，父親[林金生則為臺灣首任](../Page/林金生.md "wikilink")[嘉義縣縣長](../Page/嘉義縣.md "wikilink")。
  - 1961年，14歲，就讀[台中一中](../Page/國立台中第一高級中學.md "wikilink")。該年[美國](../Page/美國.md "wikilink")現代舞團來臺表演。啟蒙了林懷民對舞蹈的熱愛。此時林懷民也開始寫作，作品「兒歌」刊登在《[聯合報](../Page/聯合報.md "wikilink")》的[副刊上](../Page/副刊.md "wikilink")。他用稿費上了生平第一次的舞蹈課，為期兩個月
  - 1962年，15歲，考進私立[衛道中學高中部](../Page/衛道中學.md "wikilink")。
  - 1965年，18歲，考上[國立政治大學法律系](../Page/國立政治大學.md "wikilink")。
  - 1966年，19歲，從法律系轉到新聞系就讀。並開始間斷的習舞。曾師事旅美舞蹈家[黃忠良](../Page/黃忠良.md "wikilink")\[2\]
  - 1967年，臺灣舞蹈家[王仁璐首度引進現代舞大師](../Page/王仁璐.md "wikilink")[瑪莎·葛蘭姆的技巧](../Page/瑪莎·葛蘭姆.md "wikilink")，並在[西門町附近的](../Page/西門町.md "wikilink")[中山堂](../Page/中山堂.md "wikilink")，舉辦臺灣第一次現代舞蹈發表會，啟蒙了林懷民對於瑪莎·葛蘭姆現代舞編舞理念的喜好。
  - 1969年，22歲，出版中短篇[小說集](../Page/小說.md "wikilink")《蟬》，畢業後留學美國，唸[密蘇里大學新聞系碩士班](../Page/密蘇里大學.md "wikilink")，並正式在瑪莎·葛蘭姆以及[摩斯·康寧漢舞蹈學校研習現代舞](../Page/摩斯·康寧漢.md "wikilink")。
  - 1972年，25歲，[愛荷華大學英文系小說創作班畢業](../Page/愛荷華大學.md "wikilink")，獲[藝術創作碩士學位](../Page/藝術創作碩士.md "wikilink")。
  - 1973年，回臺北創辦臺灣第一個現代舞劇團「雲門舞集」。
  - 1980年，獲第六届臺灣[國家文藝獎及第三届](../Page/國家文藝獎.md "wikilink")[吳三連文藝獎](../Page/吳三連.md "wikilink")。
  - 1983年，創辦[台灣國立藝術學院舞蹈系](../Page/國立台北藝術大學.md "wikilink")，為第一任系主任，研究所所長。
  - 1983年，獲第一届[世界十大傑出青年](../Page/世界十大傑出青年.md "wikilink")。
  - 1993年，出版「說舞」與「擦肩而過」兩本書，由遠流出版社出版。
  - 1996年，獲[紐約市政府文化局](../Page/紐約市.md "wikilink")“亞洲藝術家終生成就獎”。
  - 1996年，赴[奧地利](../Page/奧地利.md "wikilink")[葛拉茲歌劇院](../Page/葛拉茲歌劇院.md "wikilink")，導演歌劇「[羅生門](../Page/羅生門.md "wikilink")」
  - 1997年，獲[香港演藝學院榮譽院士](../Page/香港.md "wikilink")。
  - 1999年，獲頒有「亞洲諾貝爾獎」之稱的[麥格塞塞獎](../Page/麥格塞塞獎.md "wikilink")\[3\]。在[柬埔寨協助當地舞者組構教案](../Page/柬埔寨.md "wikilink")，推廣瀕臨失傳的古典舞。
  - 2000年，獲[歐洲舞蹈雜誌選為](../Page/歐洲舞蹈雜誌.md "wikilink")“二十世紀編舞名家”。[國際芭蕾雜誌列為](../Page/國際芭蕾雜誌.md "wikilink")“年度人物”。
  - 2002年，獲[國立交通大學榮譽博士學位](../Page/國立交通大學.md "wikilink")。
  - 2003年，獲[行政院文化獎](../Page/行政院文化獎.md "wikilink")。
  - 2005年，獲選[Discovery頻道臺灣人物誌](../Page/Discovery頻道.md "wikilink")。
  - 2005年，上美國[時代（TIME）雜誌的](../Page/時代雜誌.md "wikilink")2005年亞洲英雄榜。
  - 2006年，獲[國立臺灣大學榮譽博士學位](../Page/國立臺灣大學.md "wikilink")。
  - 2009年，五月十二日，[德國](../Page/德國.md "wikilink")[舞動國際舞蹈節頒給](../Page/舞動國際舞蹈節.md "wikilink")[終身成就獎](../Page/終身成就獎.md "wikilink")\[4\]\[5\]。
  - 2010年，出版《高處眼亮 林懷民舞蹈歲月告白》
  - 2013年，[美國舞蹈節終身成就獎](../Page/美國舞蹈節.md "wikilink")\[6\]、一等[景星勳章](../Page/景星勳章.md "wikilink")\[7\]

## 家族

  - 曾祖父[林維朝](../Page/林維朝.md "wikilink")[祖籍福建](../Page/祖籍.md "wikilink")[漳州龍溪](../Page/漳州.md "wikilink")，1887年（清光緒十三年）[秀才](../Page/秀才.md "wikilink")。
  - 祖父[林開泰為留學](../Page/林開泰.md "wikilink")[日本的醫生](../Page/日本.md "wikilink")。
  - 父親[林金生則為臺灣首任](../Page/林金生.md "wikilink")[嘉義縣長](../Page/嘉義縣.md "wikilink")，第三、四屆[雲林縣長](../Page/雲林縣長.md "wikilink")，[內政部長](../Page/內政部長.md "wikilink")，[交通部長](../Page/交通部長.md "wikilink")。

## 編舞作品

  - 1974年——[寒食 (舞)](../Page/寒食_\(舞\).md "wikilink")
  - 1975年9月2日——[白蛇傳](../Page/白蛇傳_\(現代舞\).md "wikilink")
  - 1978年——[薪傳](../Page/薪傳.md "wikilink")\[8\]
  - 1979年——[廖添丁](../Page/廖添丁_\(現代舞\).md "wikilink")
  - 1983年——[紅樓夢](../Page/紅樓夢_\(現代舞\).md "wikilink")
  - 1985年——[夢土](../Page/夢土.md "wikilink")
  - 1984年——[春之祭禮．台北一九八四](../Page/春之祭禮．台北一九八四.md "wikilink")
  - 1991年——[我的鄉愁，我的歌](../Page/我的鄉愁，我的歌.md "wikilink")
  - 1993年——[九歌](../Page/九歌_\(現代舞\).md "wikilink")
  - 1994年——[流浪者之歌](../Page/流浪者之歌_\(現代舞\).md "wikilink")
  - 1997年——[家族合唱](../Page/家族合唱.md "wikilink")
  - 1998年——[水月](../Page/水月.md "wikilink")
  - 1999年——[焚松](../Page/焚松.md "wikilink")
  - 2001年——[竹夢](../Page/竹夢.md "wikilink")
  - 2001年——[行草](../Page/行草_\(現代舞\).md "wikilink")
  - 2002年——[烟](../Page/烟.md "wikilink")\[9\]\[10\]
  - 2003年——[松煙](../Page/松煙.md "wikilink")（原名行草貳）
  - 2004年——[風景](../Page/風景.md "wikilink")
  - 2005年——[狂草](../Page/狂草_\(現代舞\).md "wikilink")
  - 2006年——[白·美麗島](../Page/白·美麗島.md "wikilink")
  - 2008年——[花語](../Page/花語_\(現代舞\).md "wikilink")
  - 2010年——[聽河](../Page/聽河.md "wikilink")、[屋漏痕](../Page/屋漏痕.md "wikilink")
  - 2011年——[如果沒有你](../Page/如果沒有你_\(現代舞\).md "wikilink")
  - 2013年——[稻禾](../Page/稻禾.md "wikilink")
  - 2014年——[白水 微塵](../Page/白水_微塵.md "wikilink")
  - 2017年-----【關於島嶼】

## 其他作品

  - [公共電視文化事業基金會生態](../Page/公共電視文化事業基金會.md "wikilink")[紀錄片](../Page/紀錄片.md "wikilink")《[返家八千-{里}-](../Page/返家八千里.md "wikilink")》國語[旁白](../Page/旁白.md "wikilink")
  - [中華航空公司形象廣告](../Page/中華航空公司.md "wikilink")「華航不一樣了\!」\[11\]

## 參看書籍

  - 楊孟瑜，飆舞－林懷民與雲門傳奇，天下文化出版社，1998：ISBN 9576214971

## 參考文獻

## 外部連結

[Category:臺灣男舞者](../Category/臺灣男舞者.md "wikilink")
[Category:台灣作家](../Category/台灣作家.md "wikilink")
[Category:國立臺中第一高級中學校友](../Category/國立臺中第一高級中學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:愛荷華大學校友](../Category/愛荷華大學校友.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:台灣藝團團長](../Category/台灣藝團團長.md "wikilink")
[Category:臺中市私立衛道高級中學校友](../Category/臺中市私立衛道高級中學校友.md "wikilink")
[Category:世界傑出青年](../Category/世界傑出青年.md "wikilink")
[Category:雲門舞集](../Category/雲門舞集.md "wikilink")
[Category:國家文藝獎得主](../Category/國家文藝獎得主.md "wikilink")
[Category:行政院文化獎得主](../Category/行政院文化獎得主.md "wikilink")
[Category:吳三連獎得主](../Category/吳三連獎得主.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:獲頒授景星勳章者](../Category/獲頒授景星勳章者.md "wikilink")
[Category:麥格塞塞獎獲得者](../Category/麥格塞塞獎獲得者.md "wikilink")
[Category:香港浸會大學榮譽博士](../Category/香港浸會大學榮譽博士.md "wikilink")
[Category:國立臺灣大學名譽博士](../Category/國立臺灣大學名譽博士.md "wikilink")
[Category:台灣男同性戀者](../Category/台灣男同性戀者.md "wikilink")
[Category:LGBT舞者](../Category/LGBT舞者.md "wikilink")
[Category:臺灣編舞家](../Category/臺灣編舞家.md "wikilink")
[Category:国际写作计划校友](../Category/国际写作计划校友.md "wikilink")
[Category:爱荷华作家工作坊校友](../Category/爱荷华作家工作坊校友.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")
[Category:新港人](../Category/新港人.md "wikilink")
[H](../Category/林姓.md "wikilink")

1.

2.
3.

4.  [德國舞蹈節 林懷民獲終身成就獎](http://udn.com/NEWS/NATIONAL/NATS1/4904256.shtml)

5.  [林懷民獲國際終身成就獎](http://www.libertytimes.com.tw/2009/new/may/14/today-life6.htm)

6.

7.  [總統頒贈勳章
    林懷民謙稱慚愧](http://www.cna.com.tw/news/aedu/201311180130-1.aspx)

8.
9.  [風傳媒：林懷民色彩最強烈、情慾最重之作《烟》登台灣舞台](http://www.storm.mg/article/69424)

10. [風傳媒：雲門舞集作品《烟》舞蹈追憶逝水年華](http://www.storm.mg/article/63572)

11. <https://www.youtube.com/watch?v=ykDlJIplivw>