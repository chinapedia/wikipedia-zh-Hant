**林日曦**（，），本名**徐家豪**\[1\]，[香港](../Page/香港.md "wikilink")[填詞人](../Page/填詞人.md "wikilink")、[作家及創作單位](../Page/作家.md "wikilink")《[黑紙](../Page/黑紙.md "wikilink")》創辦人，創辦[毛記電視](../Page/毛記電視.md "wikilink")、雜誌《[黑紙](../Page/黑紙.md "wikilink")》、雜誌《[100毛](../Page/100毛.md "wikilink")》及[白卷出版社](../Page/白卷出版社.md "wikilink")，別稱**林日A**\[2\]或**腦細**\[3\]，居於[屯門](../Page/屯門.md "wikilink")\[4\]。

## 簡歷

[Lam_Yat_Hei_in_Hong_Kong_Book_Fair_2016.jpg](https://zh.wikipedia.org/wiki/File:Lam_Yat_Hei_in_Hong_Kong_Book_Fair_2016.jpg "fig:Lam_Yat_Hei_in_Hong_Kong_Book_Fair_2016.jpg")為讀者簽名\]\]

林日曦早年在[葵涌](../Page/葵涌.md "wikilink")[大窩口邨成長](../Page/大窩口邨.md "wikilink")\[5\]，為家中獨子。他曾就讀[佛教林炳炎紀念學校](../Page/佛教林炳炎紀念學校.md "wikilink")，後來分別入讀過四所中學，包括[聖公會林護紀念中學](../Page/聖公會林護紀念中學.md "wikilink")（中一至中三）、[葵涌循道中學](../Page/葵涌循道中學.md "wikilink")（中四）、[循道衞理聯合教會李惠利中學](../Page/循道衞理聯合教會李惠利中學.md "wikilink")（中五）以及[珠海學院](../Page/珠海學院.md "wikilink")（中學部）。在學時多無心向學。\[6\]林日曦是香港著名[填詞人](../Page/填詞.md "wikilink")[林夕的徒弟之一](../Page/林夕.md "wikilink")\[7\]，其配偶為香港獨立一人樂隊[The
Pancakes成員蔡明麗](../Page/The_Pancakes.md "wikilink")，兩人於2006年結婚。

林日曦於[香港專業教育學院](../Page/香港專業教育學院.md "wikilink")（IVE）[設計系中途輟學加入](../Page/設計.md "wikilink")[商業電台負責聲音剪接及網站管理](../Page/商業電台.md "wikilink")，及後自薦參與電台雷霆881及叱吒903的幕後創作\[8\]，曾擔任[天比高創作伙伴創作總監](../Page/天比高創作伙伴.md "wikilink")\[9\]。

2007年林日曦參與電影《[東京鐵塔：我的父親母親](../Page/東京鐵塔：我的母親父親#电影.md "wikilink")》宣傳，《(東京鐵塔下)
gulugulu》的歌詞成為他第一首發表作，歌曲入選當年叱吒十大。至今發表作品近100首。

2009年林日曦與[陳強及](../Page/陳強.md "wikilink")[阿Bu創辦創作單位](../Page/阿Bu.md "wikilink")《[黑紙](../Page/黑紙.md "wikilink")》，開始參與不同類型的創作，如出版雜誌、MV導演及平面設計\[10\]。翌年1月推出首度只得一頁紙的《黑紙》雜誌。

2011年起林日曦於報紙撰寫專欄。2012年出版第一本著作《白痴》\[11\]2013年出版第二本著作《青筋》，2014年出版《黑面》及第一本小說《快樂有限》。

2013年3月《[100毛](../Page/100毛.md "wikilink")》雜誌創刊，不足半年已達收支平衡。同年並創辦[白卷出版社](../Page/白卷出版社.md "wikilink")，專門出版小說、散文、圖文集等流行讀物。\[12\]
在2013年之前亦常為[無綫電視劇集主題曲填詞](../Page/無綫電視.md "wikilink")。

2014年起林日曦於[Now
TV擔任](../Page/Now_TV.md "wikilink")《電視節目有好多種》主持，同年接受商業電台節目訪問，因面露難色被指為經常「黑面」。\[13\]

2015年9月15日，香港近一兩年備受年輕讀者和觀眾追捧的《[100毛](../Page/100毛.md "wikilink")》雜誌以及毛記電視的大本營，當中的核心人物，創辦人之一林日曦就在其中一間沒有大班椅，沒有L型檯，更加沒有海景view的老闆房內接受jobsDB的專訪。
\[14\]

2017年5月12日，由《[100毛](../Page/100毛.md "wikilink")》及[毛記電視製作](../Page/毛記電視.md "wikilink")、[香港四大天王之一](../Page/香港四大天王.md "wikilink")[黎明代言的](../Page/黎明.md "wikilink")[雀巢正宗越南咖啡廣告](../Page/雀巢.md "wikilink")，在《[100毛](../Page/100毛.md "wikilink")》、等多個[facebook專頁廣傳](../Page/facebook.md "wikilink")。黎明親自演繹改編自其經典作品《對不起，我愛你》的改編歌曲《對不起，我愛越南啡》，MV更邀得堅．黎明\[15\]、林日曦、[堅．劉江](../Page/劉江_\(香港\).md "wikilink")\[16\]、林夕
及[毛記電視偽員](../Page/毛記電視#偽員.md "wikilink")「肥Ben」、「黃慘盈」、「龍志權」、
合作拍攝。林日曦飾演「腦細」，其他人飾演職員。\[17\]\[18\]廣告在《[100毛](../Page/100毛.md "wikilink")》Facebook專頁版本已有近三百萬瀏覽量，網民爭相分享，，掀起熱潮。

## 填詞作品列表

### 2007年

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/Freeze.md" title="wikilink">Freeze</a> - 踩鋼綫</li>
<li>Freeze - Maria</li>
<li><a href="../Page/小肥.md" title="wikilink">小　肥</a> - 時光機</li>
</ul></td>
<td><ul>
<li><a href="../Page/有耳非文.md" title="wikilink">有耳非文</a> -（東京鐵塔下）gulugulu</li>
<li>有耳非文 - 最後今天（月亮版）</li>
</ul></td>
</tr>
</tbody>
</table>

### 2008年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/At17.md" title="wikilink">At17</a> - 那天約你上太空</li>
<li><a href="../Page/I_Love_You_Boyz.md" title="wikilink">I Love You Boyz</a>、<a href="../Page/HotCha.md" title="wikilink">HotCha</a> - 火熱悶棍La La La</li>
<li><a href="../Page/Square_(組合).md" title="wikilink">Square</a> - ASAP</li>
<li><a href="../Page/文恩澄.md" title="wikilink">文恩澄</a> - 娛人娛己</li>
<li><a href="../Page/有耳非文.md" title="wikilink">有耳非文</a> - 不眠優伶</li>
</ul></td>
<td><ul>
<li><a href="../Page/李卓庭.md" title="wikilink">李卓庭</a> - 人釘人</li>
<li><a href="../Page/孫耀威.md" title="wikilink">孫耀威</a> - 七百萬人的故事</li>
<li><a href="../Page/側田.md" title="wikilink">側　田</a> - 雲</li>
<li><a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a> - 啦啦跨啦啦</li>
<li><a href="../Page/盧冠廷.md" title="wikilink">盧冠廷</a> Feat. <a href="../Page/農夫.md" title="wikilink">農夫</a> - 2050</li>
</ul></td>
</tr>
</tbody>
</table>

### 2009年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/方力申.md" title="wikilink">方力申</a> - 匆匆</li>
<li><a href="../Page/方力申.md" title="wikilink">方力申</a> - 撲翼蝴蝶</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 羅茲威爾</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 摸黑</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 南北極</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 1+1</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 情詩三百首</li>
<li><a href="../Page/江若琳.md" title="wikilink">江若琳</a> - 有請下位</li>
</ul></td>
<td><ul>
<li><a href="../Page/唐素琪.md" title="wikilink">唐素琪</a> - 清空</li>
<li><a href="../Page/翁瑋盈.md" title="wikilink">翁瑋盈</a> - A計畫</li>
<li><a href="../Page/麥家瑜.md" title="wikilink">麥家瑜</a> - 神探</li>
<li><a href="../Page/彭秀慧.md" title="wikilink">彭秀慧</a> - goodbme</li>
<li><a href="../Page/彭秀慧.md" title="wikilink">彭秀慧</a> - 月球下的人</li>
<li><a href="../Page/樂瞳.md" title="wikilink">樂　瞳</a> - 微笑的魚</li>
<li>樂　瞳 - 死亡倒數</li>
<li><a href="../Page/李昊嘉.md" title="wikilink">李昊嘉</a>、<a href="../Page/黃貫中.md" title="wikilink">黃貫中</a>、<a href="../Page/叱咤903.md" title="wikilink">叱咤903</a> DJ - 萬歲師表</li>
</ul></td>
</tr>
</tbody>
</table>

### 2010年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/ToNick.md" title="wikilink">ToNick</a> - T.O.N.I.C.K</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 東西</li>
<li><a href="../Page/車盈霏.md" title="wikilink">車盈霏</a> - 戰利品</li>
<li><a href="../Page/車盈霏.md" title="wikilink">車盈霏</a> - 天氣女郎</li>
<li><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a> - 日出前讓愛戀延續</li>
</ul></td>
<td><ul>
<li><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a> - 一代宗師</li>
<li><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a> - 越過高山越過谷</li>
<li><a href="../Page/樂瞳.md" title="wikilink">樂　瞳</a> - 雪人</li>
<li>樂　瞳 - 空氣人形</li>
<li>樂　瞳 - 聖誕路人</li>
<li>樂　瞳 Feat. <a href="../Page/陸永權.md" title="wikilink">陸永@農夫</a> - 乾物女皇</li>
</ul></td>
</tr>
</tbody>
</table>

### 2011年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/HotCha.md" title="wikilink">HotCha</a> - 未習慣</li>
<li><a href="../Page/ToNick.md" title="wikilink">ToNick</a> - 發你個夢</li>
<li><a href="../Page/太極樂隊.md" title="wikilink">太　極</a> - 今朝有酒</li>
<li><a href="../Page/羽翹.md" title="wikilink">羽　翹</a> - 大華麗家</li>
<li><a href="../Page/狄易達.md" title="wikilink">狄易達</a> - 無城</li>
<li><a href="../Page/林欣彤.md" title="wikilink">林欣彤</a>、<a href="../Page/羅孝勇.md" title="wikilink">羅孝勇</a>、<a href="../Page/吳業坤.md" title="wikilink">吳業坤</a>、<a href="../Page/何雁詩.md" title="wikilink">何雁詩</a>、<a href="../Page/許廷鏗.md" title="wikilink">許廷鏗</a>、<a href="../Page/劉威煌.md" title="wikilink">劉威煌</a> - 飛聲</li>
<li><a href="../Page/林欣彤.md" title="wikilink">林欣彤</a> - 上善若水</li>
</ul></td>
<td><ul>
<li><a href="../Page/徐子珊.md" title="wikilink">徐子珊</a> - 半圓</li>
<li><a href="../Page/張靈.md" title="wikilink">張　靈</a> - 能捨能離</li>
<li><a href="../Page/許廷鏗.md" title="wikilink">許廷鏗</a> - 上善若水</li>
<li><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a> - 上善若水</li>
<li><a href="../Page/謝天華.md" title="wikilink">謝天華</a> - 獨行</li>
<li><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a>、<a href="../Page/吳卓羲.md" title="wikilink">吳卓羲</a> - 傷城記</li>
<li><a href="../Page/關菊英.md" title="wikilink">關菊英</a> - 各安天命</li>
</ul></td>
</tr>
</tbody>
</table>

### 2012年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 魅影世紀</li>
<li><a href="../Page/狄易達.md" title="wikilink">狄易達</a>、<a href="../Page/蔚雨芯.md" title="wikilink">蔚雨芯</a> - 傻瓜機</li>
<li><a href="../Page/林欣彤.md" title="wikilink">林欣彤</a> - 三生有幸</li>
<li><a href="../Page/林峯.md" title="wikilink">林　峯</a> - 幼稚完</li>
<li>林　峯 - 同林</li>
<li><a href="../Page/雨僑.md" title="wikilink">雨　僑</a> - 逃亡</li>
<li><a href="../Page/胡杏兒.md" title="wikilink">胡杏兒</a>、<a href="../Page/李思捷.md" title="wikilink">李思捷</a> - 交換快樂</li>
<li><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a> - 連續劇</li>
<li><a href="../Page/陳僖儀.md" title="wikilink">陳僖儀</a> - 嘈</li>
</ul></td>
<td><ul>
<li><a href="../Page/陳僖儀.md" title="wikilink">陳僖儀</a>- 千秋</li>
<li><a href="../Page/陳偉霆.md" title="wikilink">陳偉霆</a> - 女皇</li>
<li><a href="../Page/陳偉霆.md" title="wikilink">陳偉霆</a> Feat. <a href="../Page/泳兒.md" title="wikilink">泳兒</a> - 我是誰（Rap詞：<a href="../Page/馮曦妤.md" title="wikilink">馮曦妤</a>）</li>
<li><a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a>、<a href="../Page/胡定欣.md" title="wikilink">胡定欣</a> - 日月</li>
<li><a href="../Page/薛凱琪.md" title="wikilink">薛凱琪</a> - 此時此刻</li>
<li><a href="../Page/謝天華.md" title="wikilink">謝天華</a>、<a href="../Page/周麗淇.md" title="wikilink">周麗淇</a> - 愛從心</li>
<li><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a> - 幸福歌</li>
<li><a href="../Page/關淑怡.md" title="wikilink">關淑怡</a> - 相濡以沫</li>
</ul></td>
</tr>
</tbody>
</table>

### 2013年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/J.Arie.md" title="wikilink">J.Arie</a> - 每一次都是你</li>
<li><a href="../Page/王梓軒.md" title="wikilink">王梓軒</a> - 越夜越快樂</li>
<li><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a> - 續集</li>
<li><a href="../Page/周麗淇.md" title="wikilink">周麗淇</a> - 心變</li>
<li><a href="../Page/林欣彤.md" title="wikilink">林欣彤</a> - 怪獸大學</li>
<li><a href="../Page/林峯.md" title="wikilink">林　峯</a> - BB（與莊冬昕、林峯合填）</li>
<li><a href="../Page/蕭正楠.md" title="wikilink">蕭正楠</a> - 圍牆</li>
</ul></td>
<td><ul>
<li><a href="../Page/許廷鏗.md" title="wikilink">許廷鏗</a> - 遺物</li>
<li><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a> - 最後祝福</li>
<li><a href="../Page/蔡俊濤.md" title="wikilink">蔡俊濤</a> - 一點通</li>
<li><a href="../Page/蔚雨芯.md" title="wikilink">蔚雨芯</a> - 無話可說</li>
<li><a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a> - 同學</li>
<li><a href="../Page/陳展鵬.md" title="wikilink">陳展鵬</a>、<a href="../Page/蕭正楠.md" title="wikilink">蕭正楠</a> - 巨輪</li>
<li><a href="../Page/劉威煌.md" title="wikilink">劉威煌</a> - 不棄也不離</li>
<li><a href="../Page/關淑怡.md" title="wikilink">關淑怡</a> - 實屬巧合</li>
</ul></td>
</tr>
</tbody>
</table>

### 2014年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/林欣彤.md" title="wikilink">林欣彤</a> - 安全地帶</li>
<li><a href="../Page/馮凱淇.md" title="wikilink">馮凱淇</a> - 暫停服務</li>
</ul></td>
<td><ul>
<li><a href="../Page/蔚雨芯.md" title="wikilink">蔚雨芯</a> - 玩具</li>
</ul></td>
</tr>
</tbody>
</table>

### 2015年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/林海峰_(香港).md" title="wikilink">林海峰</a> - 歲月毛情</li>
<li><a href="../Page/林峯.md" title="wikilink">林　峯</a> - 心有靈犀</li>
<li><a href="../Page/陳詩欣_(香港).md" title="wikilink">陳詩欣</a> - 感情戲</li>
<li>陳詩欣 - 蠢</li>
<li><a href="../Page/鄭融.md" title="wikilink">鄭　融</a> - 黑面神</li>
<li><a href="../Page/鄧智偉.md" title="wikilink">鄧智偉</a> - 史詩式</li>
</ul></td>
<td><ul>
<li><a href="../Page/黎曉陽.md" title="wikilink">黎曉陽</a> - 快樂很慢</li>
<li>黎曉陽 - 永遠很近</li>
<li>黎曉陽 - 升降機</li>
<li>黎曉陽 - 最陌生的熟悉人</li>
<li>黎曉陽 - 香港傑出廢青</li>
<li>黎曉陽 - 邊度起身邊度跌返低</li>
<li>黎曉陽 - 真偽文青</li>
</ul></td>
</tr>
</tbody>
</table>

### 2016年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/明福俠.md" title="wikilink">明福俠</a> - 我唔係死蠢</li>
<li><a href="../Page/陳詩欣_(香港).md" title="wikilink">陳詩欣</a> - 層層疊</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 八公里</li>
</ul></td>
<td><ul>
<li><a href="../Page/黃慘盈.md" title="wikilink">黃慘盈</a> - 慘情歌（國）[19]</li>
<li><a href="../Page/黎曉陽.md" title="wikilink">黎曉陽</a> - 請關掉手提電話</li>
<li>黎曉陽 - 敗部</li>
</ul></td>
</tr>
</tbody>
</table>

### 2017年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/余文樂.md" title="wikilink">余文樂</a> - 春嬌救志明</li>
<li><a href="../Page/享樂團.md" title="wikilink">享樂團</a>、<a href="../Page/陳奐仁.md" title="wikilink">陳奐仁</a> - 一年又一年</li>
</ul></td>
<td><ul>
<li><a href="../Page/黎曉陽.md" title="wikilink">黎曉陽</a> - I will be alright</li>
<li><a href="../Page/專家Dickson.md" title="wikilink">專家Dickson</a> - Earth vely danger</li>
</ul></td>
</tr>
</tbody>
</table>

### 2018年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/東方昇.md" title="wikilink">東方昇</a> feat. <a href="../Page/毛記電視.md" title="wikilink">毛記群奀星</a> - 愛是香港是愛</li>
</ul></td>
<td><ul>
<li><a href="../Page/黎明.md" title="wikilink">黎明</a> - 下半場</li>
</ul></td>
</tr>
</tbody>
</table>

### 2019年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/側田.md" title="wikilink">側田</a> - 懷很舊的舊</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

  - [專家Dickson](../Page/專家Dickson.md "wikilink") - 公開道歉

## 出版雜誌

  - 《[黑紙](../Page/黑紙.md "wikilink")》（2010-2016）
  - 《[100毛](../Page/100毛.md "wikilink")》（2013-）

## 著作

  - 《白痴》（2012）
  - 《青筋》（2013）
  - 《黑面》（2014）
  - 《快樂有限》（2014）
  - 《肉麻》（2014）
  - 《灰鳩》（2015）
  - 《金毛》（2015）
  - 《木獨》（2016）
  - 《感情戲》（2017）

## 專欄

  - [爽報專欄](../Page/爽報.md "wikilink")-白紙（已停刊）
  - [蘋果日報專欄](../Page/蘋果日報.md "wikilink")-書本以外全部沉沒
  - [明報周刊專欄](../Page/明報周刊.md "wikilink")-感情戲
  - [路訊通專欄](../Page/路訊通.md "wikilink")-將愛情餞行到底
  - [號外專欄](../Page/號外.md "wikilink")-bookstory
  - [東方日報專欄](../Page/東方日報.md "wikilink")-不癡白不癡
  - [經濟日報專欄](../Page/經濟日報.md "wikilink")-題外話
  - [都市日報專欄](../Page/都市日報.md "wikilink")-黑面書

## 廣告

  - Häagen-Dazs
    [「特別腦細新聞報道」](https://www.facebook.com/100most/videos/758963294231320/)（2015）
  - LANEIGE
    [「不能沒有妝備前度篇」](https://www.facebook.com/LaneigeHongKong/videos/946664345369066/)（2016）
  - 雀巢極品白咖啡
    [「黎明情深咖啡未曾飲」](https://www.facebook.com/100most/videos/902063266587988/)（2016）
  - GATSBY止汗香體巾
    [《十大令人滴汗嘅腦細戇X語錄》](https://www.facebook.com/100most/videos/941167159344265/)（2016）
  - 雀巢正宗越南咖啡
    [《對不起，我愛越南啡》](https://www.facebook.com/100most/videos/1227653367362308/)（2017）

## 電視節目

### [nowTV](../Page/nowTV.md "wikilink")

  - [電視節目有好多種](../Page/電視節目有好多種.md "wikilink") （2014年）主持人
      - 拍擋：[陳強](../Page/陳強_\(主持人\).md "wikilink")、[阿Bu](../Page/阿Bu.md "wikilink")

### [ViuTV](../Page/ViuTV.md "wikilink")

  - [跟住矛盾去旅行](../Page/跟住矛盾去旅行.md "wikilink")（2016年）參加者
      - 同行人士：[蔣麗芸](../Page/蔣麗芸.md "wikilink")

### \[亞洲電視數碼媒體\]

  - [百萬富翁](../Page/百萬富翁.md "wikilink")(2018年)參加者

## 參考文獻

## 另見

  - [毛記電視](../Page/毛記電視.md "wikilink")
  - [黑紙](../Page/黑紙.md "wikilink")
  - [100毛](../Page/100毛.md "wikilink")
  - [香港商業電台](../Page/香港商業電台.md "wikilink")
  - [天比高創作伙伴](../Page/天比高創作伙伴.md "wikilink")

## 外部連結

  -
  -
  -
  - [林日曦歌詞](http://blog.sina.com.cn/roylinrixi)

  - [毛記電視官網](http://www.tvmost.com.hk)

  - [黑紙官網](http://www.blackpaper.com.hk)

  - [100毛官網](http://www.100most.com.hk)

[Category:香港填詞人](../Category/香港填詞人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:聖公會林護紀念中學校友](../Category/聖公會林護紀念中學校友.md "wikilink")
[Category:徐姓](../Category/徐姓.md "wikilink")

1.

2.

3.  [堅•搵錢！毛記年賺800萬　腦細唔上市](http://hk.apple.nextmedia.com/nextmag/art/20160129/19469940)

4.  [毛記葵涌有限公司上市申請書
    第44頁](http://www.hkexnews.hk/app/SEHK/2017/2017072502/Documents/SEHK201707260012_c.pdf)


5.

6.

7.  商業電台節目《叱咤強——填詞新力軍\!\! 》。2009年12月22日

8.

9.

10.

11.

12.

13.

14.

15. 根據毛記電視傳統，現實公眾人物會於名稱前方加上「真」字或「堅」字，以便與毛記幕前偽員區分。

16.
17. [facebook：100毛 -
    黎明《情深咖啡未曾飲》](https://www.facebook.com/100most/videos/902063266587988/)

18. [蘋果日報即時新聞：【識唱跟住唱】毛記搵黎明開金口改歌詞
    《情深咖啡未曾飲》](http://hk.apple.nextmedia.com/enews/realtime/20160514/55104057)，2016年5月14日。

19. 因歌詞反覆出現“愛你”、“不愛我”，MV顯示填詞人為“Copy & Paste：林日曦”。