**林明美**（／）；[日本漢字設定與](../Page/日本漢字.md "wikilink")[香港](../Page/香港.md "wikilink")《漫畫週刊》版譯名為「**鈴明美**」，香港[亞洲電視譯名](../Page/亞洲電視.md "wikilink")「**林明明**」，日本初期設定名為「**齊明美**」（），是[日本動畫](../Page/日本動畫.md "wikilink")《[超時空要塞](../Page/超時空要塞.md "wikilink")》及其[美国改编版](../Page/美国.md "wikilink")《[太空堡垒](../Page/太空堡垒.md "wikilink")》裡面的人物。

## 剧场版《超时空要塞：可曾记得爱》的剧情

林明美出生于1993年10月10日，自幼與母親於[日本的](../Page/日本.md "wikilink")[横滨中华街生活](../Page/横滨中华街.md "wikilink")。她的夢想是成為一位歌星，不惜為此與家人斷絕關係而離家。2009年第一次宇宙战争中，林明美登上战艦[Macross](../Page/SDF-1_Macross.md "wikilink")，Macross受到[傑特拉帝人襲擊](../Page/傑特拉帝人.md "wikilink")；Macross以超時空航法Fold逃脫，因在地面施行造成Fold失敗而到了[冥王星](../Page/冥王星.md "wikilink")。

在此後不久，林明美競選Macross小姐得到冠軍；Macross返航途中在[木星附近遇到傑特拉帝人襲擊](../Page/木星.md "wikilink")，[一條輝在任務途中返航支援母艦](../Page/一條輝.md "wikilink")，卻被[早瀨未沙斥回](../Page/早瀨未沙.md "wikilink")，回歸任務中發現敵人入侵而前去幫忙，在事故中救了林明美，卻不幸掉入一未知區域，兩人在此擦出火花。

之後傑特拉帝人施行俘虜微米人之計劃，此時一條輝以訓練機帶著因工作而煩惱著的林明美在[土星光环約會](../Page/土星光环.md "wikilink")，兩人與早瀨未沙和林凱風及洛福卡隨即被抓走，但在美爾特拉帝人的攻擊之下，一條輝與早瀨未沙逃脫，在波特爾沙欲消滅美爾特拉帝人的計劃下，拿出原始文化所遺留的歌曲碎片，在地球利用林明美成功地驅散了攻擊Macross的美爾特拉帝人。

得知早瀨未沙與一條輝相戀的林明美因此逃離了一條輝，在受到傑特拉帝人基幹艦隊的攻擊之下，早瀨未沙提出歌聲攻擊一案，但林明美失蹤。一條輝在觀望臺找到了林明美，請林明美為了自己的夢想而歌。林明美以最深切的愛唱出鼓舞及引导人类的歌〈爱，还记得吗〉。布利泰因此感到文化的重要，與美爾特拉帝殘存艦隊一同協助Macross打敗波特爾沙，取得胜利。

2012年8月，林明美舉行告别[地球演唱会](../Page/地球.md "wikilink")。2012年9月，林明美随人类历史上第一次超长距离移民船队**大未來**（MEGAROAD-1）（メガロード-01）出航，离开了地球。2016年，在[銀河中心附近失去連絡](../Page/銀河.md "wikilink")。

## 配音

  - 1982年
    \<[超时空要塞](../Page/超时空要塞.md "wikilink")-[Macross](../Page/w:en:Macross.md "wikilink")\>中的林明美（日文）配音是[饭岛真理](../Page/饭岛真理.md "wikilink")
  - 1985年\<[超时空要塞](../Page/超时空要塞.md "wikilink")-[Macross](../Page/w:en:Macross.md "wikilink")\>[亞洲電視中文翻译版中的林明美](../Page/亞洲電視.md "wikilink")（[粵語](../Page/粵語.md "wikilink")）配音是[林錦燕](../Page/林錦燕.md "wikilink")
  - 1985年
    \<[太空堡垒](../Page/太空堡垒.md "wikilink")-[ROBOTECH](../Page/w:en:Robotech.md "wikilink")\>中的林明美（英文）配音是[Rebecca
    Forstadt](../Page/w:en:Rebecca_Forstadt.md "wikilink")（昵称 Reba West）
  - 1991年
    \<[太空堡垒](../Page/太空堡垒.md "wikilink")-[ROBOTECH](../Page/w:en:Robotech.md "wikilink")\>[上海电视台中文翻译版中的林明美](../Page/上海电视台.md "wikilink")（中文普通话）配音是[金琳](../Page/金琳.md "wikilink")
  - 2006年
    \<[超时空要塞](../Page/超时空要塞.md "wikilink")-[Macross](../Page/w:en:Macross.md "wikilink")\>[A.D.
    Vision美国发行版中的林明美](../Page/A.D._Vision.md "wikilink")（英文）配音是[饭岛真理](../Page/饭岛真理.md "wikilink")

## 參見

[Category:超时空要塞](../Category/超时空要塞.md "wikilink")
[Category:女性日本人動漫角色](../Category/女性日本人動漫角色.md "wikilink")
[Category:虛構中國人](../Category/虛構中國人.md "wikilink")
[Category:虛構歌手](../Category/虛構歌手.md "wikilink")
[Category:虛擬偶像](../Category/虛擬偶像.md "wikilink")