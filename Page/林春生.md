**林春生**（）台灣罪犯，綽號**春生仔**、**阿生**，[白曉燕命案的主嫌之一](../Page/白曉燕命案.md "wikilink")，逃亡途中被警方圍捕，被子彈擊中頭部兩槍身亡，雙方總計開了數十槍。\[1\]

## 生平

在1997年與[陳進興](../Page/陳進興_\(台灣罪犯\).md "wikilink")、[高天民共同犯下](../Page/高天民.md "wikilink")[白曉燕命案震驚全台](../Page/白曉燕命案.md "wikilink")，1997年6月6日又與陳進興、高天民綁架[臺北縣議會](../Page/臺北縣議會.md "wikilink")[議員](../Page/議員.md "wikilink")[蔡明堂](../Page/蔡明堂.md "wikilink")，8月8日再度綁架一名[北投陳姓商人](../Page/北投區.md "wikilink")。

1997年8月19日與高天民在[台北市](../Page/台北市.md "wikilink")[中山區](../Page/中山區_\(台北市\).md "wikilink")[五常街遭遇警方](../Page/五常街.md "wikilink")，展開槍戰，警員曹立民殉職。高天民立刻搶奪民眾的[摩托車逃逸](../Page/摩托車.md "wikilink")。林春生卻跑錯方向，被困在防火巷中，被警方圍堵。林春生身中六槍，卻仍舊頑抗，後因傷勢過重，體力不支，舉槍自決。

林春生死前留下[絕筆信](../Page/遺書.md "wikilink")，承認白案是他與陳進興、高天民所犯。

## 參考文獻

[L林](../Page/category:台灣謀殺犯.md "wikilink")

[L林](../Category/臺灣自殺者.md "wikilink")
[Category:林姓](../Category/林姓.md "wikilink")

1.  [中天新聞》白曉燕案五常街槍戰 巷弄圍捕槍響 中時電子報
    觀看次數：199,319](https://www.youtube.comwatch?v=cCS7RHiDRPM)