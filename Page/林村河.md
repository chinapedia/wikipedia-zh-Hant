[Lam_Tsuen_River_1.jpg](https://zh.wikipedia.org/wiki/File:Lam_Tsuen_River_1.jpg "fig:Lam_Tsuen_River_1.jpg")的一段林村河\]\]
[Lam_Tsuen_River_201401.jpg](https://zh.wikipedia.org/wiki/File:Lam_Tsuen_River_201401.jpg "fig:Lam_Tsuen_River_201401.jpg")附近的一段林村河\]\]

**林村河**
（）是一條位於[香港](../Page/香港.md "wikilink")[新界中部的河流](../Page/新界.md "wikilink")，位於[大埔以西](../Page/大埔_\(香港\).md "wikilink")，源自[大帽山及](../Page/大帽山.md "wikilink")[觀音山](../Page/觀音山.md "wikilink")。林村河是香港典型的中型河流，在河流中能找到很多不同種類的生物，包括稀有生物[香港蠑螈](../Page/香港蠑螈.md "wikilink")。林村河發源於**大帽山北巔四方山的猴岩頂，海拔約740米**，上游由[梧桐寨瀑布開始](../Page/梧桐寨.md "wikilink")，流經[林村谷](../Page/林村谷.md "wikilink")，中游流經[寨氹](../Page/寨氹.md "wikilink")、橫過[林錦公路轉向東北的](../Page/林錦公路.md "wikilink")[麻蒲尾和](../Page/麻蒲尾.md "wikilink")[新塘](../Page/新塘.md "wikilink")、[放馬莆](../Page/放馬莆.md "wikilink")、[川背龍的農地](../Page/川背龍.md "wikilink")，到[圍頭村附近作](../Page/圍頭村.md "wikilink")90度轉向東南，經梅樹坑流向[大埔新市鎮](../Page/大埔新市鎮.md "wikilink")，匯合[大埔河後在](../Page/大埔河.md "wikilink")[廣福邨附近流入](../Page/廣福邨.md "wikilink")[吐露港](../Page/吐露港.md "wikilink")。

## 水系及地質

林村河集水區面積廣大，約有18.5[平方公里](../Page/平方公里.md "wikilink")，每年總水量平均達14.6百萬立方米，河流長度約為10.8公里，平均坡度為1:14.5。林村河為[樹枝狀水系](../Page/樹枝狀水系.md "wikilink")，流域形狀呈長方形，[幹流蜿蜒流過](../Page/幹流.md "wikilink")[林村谷中央](../Page/林村谷.md "wikilink")。林村河是河流沿著[弱線](../Page/弱線.md "wikilink")[侵蝕的好例子](../Page/侵蝕.md "wikilink")，它的幹流正是在林村斷層這條弱線之上。根據地質及沿途沖積物分佈推斷，林村河原為[梧桐河的支流](../Page/梧桐河.md "wikilink")，河流越過[圍頭村再連接梧桐河直到](../Page/圍頭村.md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")，但在約24,000至85,000年前的一次斷層移動後，圍頭村附近的地殼上升，切斷林村河中游與梧桐河的河道，河水作90度轉向穿過[梅樹坑的鞍形山口與現時的林村河下游連接](../Page/梅樹坑.md "wikilink")，是[河川襲奪](../Page/河川襲奪.md "wikilink")（river
capture）的一個例子。

## 上游

[120px](../Page/檔案:梧桐寨.JPG.md "wikilink")
上游處於香港最高山脈[大帽山的東麓](../Page/大帽山.md "wikilink")，全是狹窄而陡峭的石澗，在下蝕主導的情況下，主要地貌特徵有[V形河谷](../Page/河谷.md "wikilink")（V-shaped
valley）、[交錯山咀](../Page/交錯山咀.md "wikilink")（interlocking
spurs）、[急流](../Page/急流.md "wikilink")（rapids）、[瀑布](../Page/瀑布.md "wikilink")、[跌水潭](../Page/跌水潭.md "wikilink")（plunge
pools）和[峽谷](../Page/峽谷.md "wikilink")，[梧桐寨瀑布是當中的好例子](../Page/梧桐寨.md "wikilink")。

## 中游

林村河流過山區後，中游河段坡度突趨平緩，進入較平坦而開闊的[林村谷](../Page/林村谷.md "wikilink")，是寬達800米的[沖積平原](../Page/沖積平原.md "wikilink")。河段侵蝕主要是側蝕，形成[曲流](../Page/曲流.md "wikilink")（meander），由[塘面村至](../Page/塘面村.md "wikilink")[較寮下的河段就是其中一個曲流例子](../Page/較寮下.md "wikilink")。

## 下游及河口

[HK_LamTsuenRiver_MuiShueHang.JPG](https://zh.wikipedia.org/wiki/File:HK_LamTsuenRiver_MuiShueHang.JPG "fig:HK_LamTsuenRiver_MuiShueHang.JPG")一段人工化河道，中間可見昔日的江心洲\]\]
原本的林村河河口位於現時[廣福橋附近](../Page/廣福橋.md "wikilink")，河水到達此處就流出大埔海（即[吐露港](../Page/吐露港.md "wikilink")）。在1970年代，由於[大埔新市鎮的發展](../Page/大埔新市鎮.md "wikilink")，大埔海部份的地方開展了填海工程，令林村河延長到[廣福邨附近才流出大海](../Page/廣福邨.md "wikilink")，並且在廣福邨附近和[大埔河匯合](../Page/大埔河.md "wikilink")，形成了今日的面貌。現時林村河下游由[大埔頭抽水站以東至吐露港已完全建成人工河道](../Page/大埔頭抽水站.md "wikilink")，失去所有天然河流地貌，但在[梅樹坑還可找到昔日分流和兩座](../Page/梅樹坑.md "wikilink")[江心洲等下游地貌遺址](../Page/江心洲.md "wikilink")，現時該處已發展成[梅樹坑公園](../Page/梅樹坑公園.md "wikilink")。

林村河近大埔一段有5座外型帶有中國建築特式的行人天橋橫跨河道，分別於1984年至1992年間落成，分別為[錦石橋](../Page/錦石橋.md "wikilink")、[錦和橋](../Page/錦和橋.md "wikilink")、[太和橋](../Page/太和橋.md "wikilink")、[廣福橋和](../Page/廣福橋.md "wikilink")[寶鄉橋](../Page/寶鄉橋.md "wikilink")。

### 大埔頭抽水站

由於林村河水量豐富，政府在梅樹坑附近的[大埔頭興建抽水站抽取河水作食水之用](../Page/大埔頭.md "wikilink")，抽水站內設有一條人造纖維的可充氣水壩，充氣後成為一道堵截河水的水壩，蓄積河水，再經由地下水管將河水輸往[船灣淡水湖](../Page/船灣淡水湖.md "wikilink")。當出現嚴重暴雨時，水壩會放氣讓洪水排出吐露港。

## 河流生態

林村河的水生生物主要出現在林村河中游，如[寬鰭鱲](../Page/寬鰭鱲.md "wikilink")、[異鱲等魚類及稀有](../Page/異鱲.md "wikilink")[兩棲動物](../Page/兩棲動物.md "wikilink")[香港蠑螈等](../Page/香港蠑螈.md "wikilink")，下游則因河流遭人工化而破壞了整個下游生態系統。林村河流域是大量蜻蜓品種的棲息地，包括多種罕見的[華麗扇蟌](../Page/華麗扇蟌.md "wikilink")、[緒方華扁蟌及梧桐寨獨有的](../Page/緒方華扁蟌.md "wikilink")[郁異偽蜻及](../Page/郁異偽蜻.md "wikilink")[克氏頭蜓](../Page/克氏頭蜓.md "wikilink")，是繼[沙羅洞後香港最重要的蜻蜓棲息地](../Page/沙羅洞.md "wikilink")。

[梧桐寨河谷擁有一片茂密的樹林](../Page/梧桐寨.md "wikilink")，是香港其中一處植物種類最繁多的地方，當中包括一些稀有和受保護品種，如[巢蕨](../Page/巢蕨.md "wikilink")、[剌桫欏等](../Page/剌桫欏.md "wikilink")，所以梧桐寨河谷早於1979年已列為[具特殊科學價值地點](../Page/具特殊科學價值地點.md "wikilink")。

<File:HK_LamTsuenRiver_PingLong.JPG>|[坪朗附近一段林村河](../Page/坪朗.md "wikilink")
<File:HK_LamTsuenRiver_TaiPoTau.JPG>|[大埔頭抽水站附近一段林村河](../Page/大埔頭抽水站.md "wikilink")
[File:HK_LamTsuenRiver_TaiPoTauNylonDam.JPG|大埔頭抽水站的充氣水壩](File:HK_LamTsuenRiver_TaiPoTauNylonDam.JPG%7C大埔頭抽水站的充氣水壩)
<File:Lam> Tsuen River (Hong
Kong).jpg|林村河近[大埔中心一段](../Page/大埔中心.md "wikilink")。河水因大雨將上游泥土沖下，變得一片混黃。

## 橋樑

  - [錦石橋](../Page/錦石橋.md "wikilink")
  - [錦和橋](../Page/錦和橋.md "wikilink")
  - [太和橋](../Page/太和橋.md "wikilink")
  - [廣福橋](../Page/廣福橋.md "wikilink")
  - [寶鄉橋](../Page/寶鄉橋.md "wikilink")

## 參考資料

  - [大埔林村河淤泥積存異味四散](https://web.archive.org/web/20070929092923/http://www.dab.org.hk/tr/main.jsp?content=article-content.jsp&articleId=4320&categoryId=1364)
  - [渠務署保護林村河不力](https://web.archive.org/web/20070207075509/http://www.singpao.com/20050711/local/734716.html)
  - [探索林村河源頭](https://web.archive.org/web/20071117060909/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20061023&sec_id=4104&subsec_id=11867&art_id=6434122)
  - [綠色力量 -
    從「河」而來教育計劃：林村河](http://www.greenpower.org.hk/new/river/part3/drainage_content.html)
  - [小工程，大改善 －
    林村河改善工程](http://www.dsd.gov.hk/TC/Files/publications_publicity/newsletter/news29/29-pg05.htm)
  - 綠色力量，2006，《從河而來—林村河》

[Category:林村谷](../Category/林村谷.md "wikilink") [Category:大埔
(香港)](../Category/大埔_\(香港\).md "wikilink")
[Category:香港河流](../Category/香港河流.md "wikilink")