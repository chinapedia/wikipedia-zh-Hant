**林甸县**位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[黑龙江省西部](../Page/黑龙江省.md "wikilink")、[松嫩平原北部](../Page/松嫩平原.md "wikilink")，是[大庆市下辖的一个县](../Page/大庆市.md "wikilink")。北邻[富裕县](../Page/富裕县.md "wikilink")、[依安县](../Page/依安县.md "wikilink")，南接[杜尔伯特蒙古族自治县和](../Page/杜尔伯特蒙古族自治县.md "wikilink")[大庆市](../Page/大庆市.md "wikilink")，西连[齐齐哈尔市](../Page/齐齐哈尔市.md "wikilink")，东与[安达市](../Page/安达市.md "wikilink")、[青冈县和](../Page/青冈县.md "wikilink")[明水县相邻](../Page/明水县.md "wikilink")。总面积3493平方公里，2010年[第六次全国人口普查时全县常住人口为](../Page/第六次全国人口普查.md "wikilink")244578人。\[1\]政府驻地[林甸镇](../Page/林甸镇.md "wikilink")。该县目前为[国家级贫困县](../Page/国家级贫困县.md "wikilink")。

## 地理

林甸县地处[松嫩平原](../Page/松嫩平原.md "wikilink")，地势平坦。草原占全县土地面积的1/3。年平均气温2.5℃，年均降水量427毫米。林甸县是黑龙江省第二大[芦苇生产基地](../Page/芦苇.md "wikilink")，同时还是黑龙江省重要乳品基地。林甸县有独特的特大型中低温地热田，被誉为“中国温泉之乡”、“世界温泉养生基地”。同时，[扎龙湿地自然保护区也有一部分位于林甸县境内](../Page/扎龙湿地自然保护区.md "wikilink")。\[2\]

## 行政区划

截止2012年底，林甸县下辖4个镇、4个乡：

  - 镇：[林甸镇](../Page/林甸镇.md "wikilink")、[红旗镇](../Page/红旗镇_\(林甸县\).md "wikilink")、[花园镇](../Page/花园镇_\(林甸县\).md "wikilink")、[四季青镇](../Page/四季青镇.md "wikilink")
  - 乡：[东兴乡](../Page/东兴乡_\(林甸县\).md "wikilink")、[宏伟乡](../Page/宏伟乡_\(林甸县\).md "wikilink")、[三合乡](../Page/三合乡_\(林甸县\).md "wikilink")、[四合乡](../Page/四合乡_\(林甸县\).md "wikilink")

## 经济

2012年，林甸县全县实现地区生产总值55.6亿元，其中三次产业结构比例为31:51:18。以种植业和畜牧产为支柱产业。此外温泉旅游发展迅速，已经有了一定的知名度。\[3\]

## 参考资料

## 外部链接

  - [林甸县人民政府网](http://www.lindian.gov.cn/)

## 参见

  - [黑龙江扎龙国家级自然保护区](../Page/黑龙江扎龙国家级自然保护区.md "wikilink")

[林甸县](../Page/category:林甸县.md "wikilink")

[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.
2.
3.