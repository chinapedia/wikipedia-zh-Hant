**林素英**（，曾譯為「林曉英」、「林昭英」，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")、[模特](../Page/模特.md "wikilink")，曾在中國留學，為圓夢返國。2005年至2007年間為[JYP娛樂旗下女演員](../Page/JYP娛樂.md "wikilink")\[1\]，2006年曾與[朴軫永及](../Page/朴軫永.md "wikilink")[Rain共同演出BMW的](../Page/Rain.md "wikilink")20分鐘音樂微電影「BMW
meets truth」作為宣傳，在父親過世後協議解約。

## 演出作品

### 電視劇

  - 2006年：MBC《[朱蒙](../Page/朱蒙_\(電視劇\).md "wikilink")》飾 芙英
  - 2007年：MBC Dramanet《別巡檢1》
  - 2010年：MBC《[快樂我的家](../Page/快樂我的家.md "wikilink")》

### 電影

  - 2010年：《感應》

### MV

  - 2006年：Neol《全部都是你》
  - 2008年：[JOO](../Page/JOO.md "wikilink")《因為男人》

## 外部連結

  - [個人網站](http://www.jype.com/main.star.profile.screen?p_menu_id=1&p_artist_id=soyoung)

  - [林素英於經紀公司網站的個人資料](https://web.archive.org/web/20070921205803/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=14503)

  - [EPG](https://web.archive.org/web/20070921205803/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=14503)


[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國佛教徒.md "wikilink")
[L](../Category/韩国女演员.md "wikilink")

1.