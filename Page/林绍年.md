**林绍年**（），字**赞虞**，[福建](../Page/福建.md "wikilink")[闽县人](../Page/闽侯县.md "wikilink")。

[清](../Page/清.md "wikilink")[同治十三年](../Page/同治.md "wikilink")（1874年）[进士](../Page/进士.md "wikilink")，授[翰林院](../Page/翰林院.md "wikilink")[编修](../Page/编修.md "wikilink")。[光绪十四年](../Page/光绪.md "wikilink")（1888年）任浙江道[監察御史](../Page/監察御史.md "wikilink")，以极谏[慈禧动用](../Page/慈禧.md "wikilink")[海军经费修](../Page/海军.md "wikilink")[颐和园](../Page/颐和园.md "wikilink")，名噪四海。二十六年（1900年）迁[云南](../Page/云南.md "wikilink")[布政使](../Page/布政使.md "wikilink")，就擢[巡抚](../Page/巡抚.md "wikilink")，兼署[云贵总督](../Page/云贵总督.md "wikilink")。三十年（1904年）上奏[朝廷](../Page/朝廷.md "wikilink")，呼吁实行[立宪改革](../Page/清末新政.md "wikilink")。光绪三十一年九月十一，由代[贵州巡抚调任](../Page/贵州巡抚.md "wikilink")[廣西巡撫](../Page/廣西巡撫.md "wikilink")。光绪三十二年九月二十（1906年11月6日），解任以侍郎用。\[1\]。三十二年（1906年）内召，以侍郎充[军机大臣](../Page/军机大臣.md "wikilink")，兼署邮传部[尚书](../Page/尚书.md "wikilink")，授度支部[侍郎](../Page/侍郎.md "wikilink")。支持改革。三十三年（1907年）因御史[赵启霖劾](../Page/赵启霖.md "wikilink")[段芝贵案](../Page/段芝贵.md "wikilink")，林绍年替言官赵启霖报不平，跟庆亲王[奕劻闹翻](../Page/奕劻.md "wikilink")，称病退出军机，外出任河南巡抚。1910年调回中央任学部侍郎，后改任[弼德院顾问大臣](../Page/弼德院.md "wikilink")，随即告病回乡，1916年病逝于[福州故里](../Page/福州.md "wikilink")\[2\]。

## 參考資料

## 外部連結

  -   - \[<http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%AAL%B2%D0%A6>\~
        林紹年\]

{{-}}

[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝監察御史](../Category/清朝監察御史.md "wikilink")
[Category:清朝河南巡撫](../Category/清朝河南巡撫.md "wikilink")
[Category:清朝雲貴總督](../Category/清朝雲貴總督.md "wikilink")
[Category:郵傳部尚書](../Category/郵傳部尚書.md "wikilink")
[Category:弼德院顧問大臣](../Category/弼德院顧問大臣.md "wikilink")
[Category:军机大臣](../Category/军机大臣.md "wikilink")
[Category:闽侯人](../Category/闽侯人.md "wikilink")
[S绍](../Category/林姓.md "wikilink")
[Category:諡文直](../Category/諡文直.md "wikilink")

1.
2.  陳三立. 《清誥授光祿大夫頭品頂戴經筵講官弼德院顧問大臣予諡文直閩縣林公神道碑銘》.