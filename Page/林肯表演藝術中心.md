[Metropolitan_Opera_House_At_Lincoln_Center.jpg](https://zh.wikipedia.org/wiki/File:Metropolitan_Opera_House_At_Lincoln_Center.jpg "fig:Metropolitan_Opera_House_At_Lincoln_Center.jpg")\]\]
[Avery_Fisher_Hall.jpg](https://zh.wikipedia.org/wiki/File:Avery_Fisher_Hall.jpg "fig:Avery_Fisher_Hall.jpg")的駐紮地，林肯中心的[大衛·格芬廳](../Page/大衛·格芬廳.md "wikilink")\]\]

**林肯表演藝術中心**（**Lincoln Center for the Performing
Arts**），簡稱**林肯中心**（**Lincoln
Center**），是位於[美國](../Page/美國.md "wikilink")[紐約市佔地](../Page/紐約市.md "wikilink")
61,000 m² 的綜合藝術表演中心，總共有12個表演團體以此為駐紮地。

## 概要

林肯中心建於1960年代由[羅伯·摩斯](../Page/羅伯·摩斯.md "wikilink")（Robert
Moses）所主導的[都市更新計劃時期](../Page/都市更新.md "wikilink")，在[約翰·洛克菲勒三世](../Page/約翰·洛克菲勒三世.md "wikilink")（John
D. Rockefeller
III）的資助下開始興建。它是美國境內第一個將主要的文藝機構集中於一地的表演中心，是全世界最大的藝術會場，總共可以同時容納
18,000
位觀眾，共耗資1億6千8百萬美元。主建築座落於[曼哈頓](../Page/曼哈頓.md "wikilink")[上西城的哥倫布](../Page/上西城.md "wikilink")、阿姆斯特丹大道及西第62、66大街的交界處。除了主建築外，林肯中心的設施也不斷地向外擴充。例如於2004年，在距林肯中心南方數條街的[時代華納中心](../Page/時代華納中心.md "wikilink")（Time
Warner
Center）專供[爵士樂表演的](../Page/爵士樂.md "wikilink")[林肯中心爵士樂社](../Page/林肯中心爵士樂社.md "wikilink")（Jazz
at Lincoln
Center）就新設立了[弗雷德里克·羅斯廳](../Page/弗雷德里克·羅斯廳.md "wikilink")（Frederick
P. Rose Hall）。

林肯中心主要以環繞噴泉廣場的3棟劇院為主——包含[紐約州立劇院](../Page/紐約州立劇院.md "wikilink")（New York
State Theater）、[大都會歌劇院](../Page/大都會歌劇院.md "wikilink")（Metropolitan Opera
House）、[大衛·格芬廳](../Page/大衛·格芬廳.md "wikilink")（David Geffen
Hall）。東部有[紐約表演藝術公共圖書館](../Page/紐約公共圖書館.md "wikilink")（New
York Public Library for the Performing
Arts）、[艾莉絲·塔利廳](../Page/艾莉絲·塔利廳.md "wikilink")（Alice
Tully
Hall），以及世界上最頂尖的表演藝術學校之一的[朱利亞德學院](../Page/朱利亞德學院.md "wikilink")（Juilliard
School）。

除了一般性的表演節目，林肯中心也定期舉行特別表演節目系列（稱為「林肯中心呈現」（Lincoln Center
Presents），包含有美國歌本（American Songbook）、偉大表演者（Great
Performers）、林肯中心音樂節（Lincoln Center Festival）、林肯中心戶外（Lincoln Center Out
of Doors）、仲夏夜搖擺（Midsummer Night Swing）、莫札特音樂節（the Mostly Mozart
Festival）、以及多次獲艾美獎肯定的「林肯中心現場」系列（Live From Lincoln Center）。

2006年7月，林肯中心宣佈將與出版公司 John Wiley & Sons, Inc.
合作出版至少15本關於表演藝術的書，並提升林肯中心機構的教育性質與收藏。

從2006年8月起至2009年，林肯中心發起第65大街重新發展計劃，內容為建造新的人行步道以改善園區內的親和力與美感\[1\]。

[福特漢姆大學亦有林肯中心校區](../Page/福特漢姆大學.md "wikilink")，其法學院、商學院等均位于此。

## 主要表演設施

  - [艾莉絲·塔利廳](../Page/艾莉絲·塔利廳.md "wikilink")（Alice Tully
    Hall）—擁有1,095個座位的演奏廳，位於[朱利亞德學院之內](../Page/朱利亞德學院.md "wikilink")，[林肯中心室內樂協會](../Page/林肯中心室內樂協會.md "wikilink")（The
    Chamber Music Society of Lincoln Center）的駐紮地。
  - [大衛·格芬廳](../Page/大衛·格芬廳.md "wikilink")（David Geffen
    Hall）—擁有2,738個座位的音樂廳，前稱為「愛樂廳」（Philharmonic
    Hall），[紐約愛樂的駐紮地](../Page/紐約愛樂.md "wikilink")。
  - [大都會歌劇院](../Page/大都會歌劇院.md "wikilink")（The Metropolitan Opera
    House）—擁有3,900個座位的歌劇院，[大都會歌劇團的主場地](../Page/大都會歌劇團.md "wikilink")。
  - [紐約州立劇院](../Page/紐約州立劇院.md "wikilink")（The New York State
    Theater）—擁有2,713個座位的芭蕾舞劇院，原為[紐約市立芭蕾舞團的主場地](../Page/紐約市立芭蕾舞團.md "wikilink")，現也為[紐約市立歌劇團的主場地](../Page/紐約市立歌劇團.md "wikilink")。

## 外部連結

  -
## 參考文獻

[Category:曼哈顿建筑物](../Category/曼哈顿建筑物.md "wikilink")
[Category:美國音樂場地](../Category/美國音樂場地.md "wikilink")
[Category:福坦莫大学](../Category/福坦莫大学.md "wikilink")
[Category:美國地標](../Category/美國地標.md "wikilink")
[Category:曼哈頓文化](../Category/曼哈頓文化.md "wikilink")
[Category:曼哈顿旅游景点](../Category/曼哈顿旅游景点.md "wikilink")

1.  [On 65th Street, Glimpsing Lincoln Center’s
    Future](http://www.nytimes.com/2006/08/17/arts/design/17linc.html) -
    The New York Times