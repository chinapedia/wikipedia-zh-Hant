**林雅婷**（****，），前[香港](../Page/香港.md "wikilink")[女](../Page/女.md "wikilink")[配音員](../Page/配音員.md "wikilink")。

## 概要

  - 任職配音員之前，曾經接受過[突破機構的](../Page/突破機構.md "wikilink")[DJ訓練](../Page/DJ.md "wikilink")，並在電台擔任過嘉賓主持\[1\]。

<!-- end list -->

  - 2004年入讀第3期[無綫電視粵語配音藝員訓練班](../Page/無綫電視粵語配音藝員訓練班.md "wikilink")，同年11月1日加入[無綫電視配音組](../Page/無綫電視.md "wikilink")\[2\]。

<!-- end list -->

  - 2007年7月離職並退出配音界。

## 配音作品

※<span style="font-size:smaller;">**粗體**表示者為作品中的主角或要角</span>

### 電視動畫／OVA

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/帝皇戰紀.md" title="wikilink">帝皇戰紀</a></p></td>
<td><p>札寶利·瑪莉安娜</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:まっすぐにいこう。.md" title="wikilink">大城小狗</a></p></td>
<td><p>阿空</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/馭龍少年.md" title="wikilink">馭龍少年</a></p></td>
<td><p>美歌奴</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/妹妹公主.md" title="wikilink">妹妹公主</a></p></td>
<td><p><strong>雛子</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妹妹公主_RePure.md" title="wikilink">妹妹公主新角度</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東京地底奇兵.md" title="wikilink">東京地底奇兵</a></p></td>
<td><p>瑠離沙拉沙</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多啦A夢_(動畫).md" title="wikilink">多啦A夢</a></p></td>
<td><p>鐮松美寶子</p></td>
<td><p>第1747集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我們這一家.md" title="wikilink">我們這一家</a></p></td>
<td><p>梶井</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勇者王終極任務.md" title="wikilink">勇者王終極任務</a></p></td>
<td><p><strong>露娜·加迪夫·獅子王</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:レジェンズ_甦る竜王伝說.md" title="wikilink">龍王傳說</a></p></td>
<td><p><strong>美琪·斯賓高</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/唱K小魚仙.md" title="wikilink">唱K小魚仙</a></p></td>
<td><p>美美<br />
可可</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/魔界女王候補生.md" title="wikilink">魔界女王候補生</a></p></td>
<td><p><strong>芭莉娜·苗雨（愛須芭莉娜）</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/光之美少女.md" title="wikilink">光之美少女</a></p></td>
<td><p>米波</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Keroro軍曹_(電視動畫).md" title="wikilink">Keroro軍曹</a></p></td>
<td><p>霜月彌生</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陰陽大戰記.md" title="wikilink">陰陽大戰記</a></p></td>
<td><p>瑞樹</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/妮嘉尋親記.md" title="wikilink">妮嘉尋親記</a></p></td>
<td><p><strong>妮嘉·蘋果園</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甲蟲王者.md" title="wikilink">甲蟲王者</a></p></td>
<td><p><strong>白梅</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捉鬼天狗幫.md" title="wikilink">捉鬼天狗幫</a></p></td>
<td><p>羅莎莉<br />
阿雅</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/至NET奇兵.md" title="wikilink">至NET奇兵</a></p></td>
<td><p>尤露達<br />
愛娜</p></td>
<td><p>代配第43集<br />
第45集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/翼之奇幻旅程.md" title="wikilink">翼之奇幻旅程</a></p></td>
<td><p><strong>白色蒙哥拿</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小沙彌大智慧.md" title="wikilink">小沙彌大智慧</a></p></td>
<td><p>青青</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔豆傳奇.md" title="wikilink">魔豆傳奇</a></p></td>
<td><p><strong>那布</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Keroro軍曹_(電視動畫).md" title="wikilink">Keroro軍曹II</a></p></td>
<td><p>霜月彌生</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈姆太郎.md" title="wikilink">哈姆太郎</a></p></td>
<td><p>瑪莉亞<br />
虎妹</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寵物小精靈超世代.md" title="wikilink">寵物小精靈超世代</a></p></td>
<td><p>毒薔薇<br />
泉美</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麵包超人.md" title="wikilink">麵包超人</a></p></td>
<td><p><strong>牛油妹妹</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喜羊羊與灰太狼.md" title="wikilink">喜羊羊與灰太狼</a></p></td>
<td><p><strong>美羊羊</strong></p></td>
<td><p>第1季及第2季</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/機動戰士特種命運.md" title="wikilink">機動戰士特種命運</a></p></td>
<td><p>飛鳥·真由</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/貓狗寵物街_(第三輯).md" title="wikilink">貓狗寵物街</a></p></td>
<td><p><strong>寶露</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/機動戰士終極特種命運.md" title="wikilink">機動戰士終極特種命運</a></p></td>
<td><p>飛鳥·真由</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奇幻魔法Melody.md" title="wikilink">奇幻魔法Melody</a></p></td>
<td><p><strong>Melody</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 劇場版／動畫電影

| 播映年份     | 作品名稱                                          | 配演角色 | 備註 |
| -------- | --------------------------------------------- | ---- | -- |
| **2006** | [多啦A夢大電影：惑星之謎](../Page/大雄與惑星之迷.md "wikilink") | 猩太郎  |    |
|          |                                               |      |    |

### 特攝片

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/忍風戰隊.md" title="wikilink">忍風戰隊</a></p></td>
<td><p>假藍忍風</p></td>
<td><p>N/A</p></td>
<td><p>第48集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超人力斯.md" title="wikilink">超人力斯</a></p></td>
<td><p>齊田里子<br />
貝娜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/爆龍戰隊.md" title="wikilink">爆龍戰隊</a></p></td>
<td><p>利利安<br />
ぷりぷりんせす三妹</p></td>
<td><p><a href="../Page/:ja:宮崎瑠依.md" title="wikilink">宮崎瑠依</a><br />
<a href="../Page/:ja:槇ひろ子.md" title="wikilink">槇ひろ子</a>（老年）<br />
<a href="../Page/前場莉奈.md" title="wikilink">前場莉奈</a></p></td>
<td><p>第6集<br />
第22-23集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/假面騎士555.md" title="wikilink">-{zh-hans:假面骑士555;zh-hk:幪面超人555;zh-tw:假面騎士555;}-</a></p></td>
<td><p>長田道子</p></td>
<td><p><a href="../Page/:ja:大久保綾乃.md" title="wikilink">大久保綾乃</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/特搜戰隊.md" title="wikilink">特搜戰隊</a></p></td>
<td><p><strong>胡堂小梅（特搜粉紅戰士）</strong></p></td>
<td><p><a href="../Page/菊地美香.md" title="wikilink">菊地美香</a></p></td>
<td><p>只配至第22集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幪面超人劍.md" title="wikilink">-{zh-hans:假面骑士劍;zh-hk:幪面超人劍;zh-tw:假面騎士劍;}-</a></p></td>
<td><p>生原羽美</p></td>
<td><p><a href="../Page/:ja:東海林愛美.md" title="wikilink">東海林愛美</a></p></td>
<td><p>第35-36集</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 日劇

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/:ja:鬼嫁日記.md" title="wikilink">鬼嫁日記</a></p></td>
<td><p>中川由紀</p></td>
<td><p><a href="../Page/加賀美早紀.md" title="wikilink">加賀美早紀</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女王的教室.md" title="wikilink">女王的教室</a></p></td>
<td><p>田中桃</p></td>
<td><p><a href="../Page/伊藤沙莉.md" title="wikilink">伊藤沙莉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/利家與松.md" title="wikilink">利家與松</a></p></td>
<td><p>前田幸</p></td>
<td><p><a href="../Page/矢端吏結.md" title="wikilink">矢端吏結</a>（7歲）<br />
<a href="../Page/青木千奈美.md" title="wikilink">青木千奈美</a>（10歲）<br />
<a href="../Page/小島莉子.md" title="wikilink">小島莉子</a>（12歲）<br />
（成年）</p></td>
<td><p>大河劇</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一公升的眼淚_(電視劇).md" title="wikilink">一公升眼淚</a></p></td>
<td><p>松村早希</p></td>
<td><p><a href="../Page/:ja:松本華奈.md" title="wikilink">松本華奈</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:アタックNo.1.md" title="wikilink">新排球女將</a></p></td>
<td><p>石松真理</p></td>
<td><p><a href="../Page/:jp:森田彩華.md" title="wikilink">森田彩華</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/海猿.md" title="wikilink">海猿</a></p></td>
<td><p>星野怜</p></td>
<td><p><a href="../Page/:jp:臼田あさ美.md" title="wikilink">臼田麻美</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飆風引擎.md" title="wikilink">飆風引擎</a></p></td>
<td><p>金村俊太</p></td>
<td><p><a href="../Page/:jp:小室優太.md" title="wikilink">小室優太</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/不能結婚的男人.md" title="wikilink">不能結婚的男人</a></p></td>
<td><p>木崎</p></td>
<td><p><a href="../Page/:ja:立花彩野.md" title="wikilink">立花彩野</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女人心機.md" title="wikilink">不信的時候</a></p></td>
<td><p>伊籐真由美</p></td>
<td><p><a href="../Page/福田沙紀.md" title="wikilink">福田沙紀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 韓劇

| 播映年份     | 作品名稱                                      | 配演角色 | 角色演員                             | 備註    |
| -------- | ----------------------------------------- | ---- | -------------------------------- | ----- |
| **2005** | [悲傷戀歌](../Page/悲傷戀歌.md "wikilink")        | 姜欣怡  |                                  |       |
| **2006** | [宮](../Page/宮_\(2006年電視劇\).md "wikilink") | 金純英  | [李恩](../Page/李恩.md "wikilink")   |       |
| **2007** | [薯童謠](../Page/薯童謠.md "wikilink")          | 銀進   | [具惠善](../Page/具惠善.md "wikilink") | 只配演前期 |
|          |                                           |      |                                  |       |

### 中／港劇

| 播映年份                               | 作品名稱                               | 配演角色                             | 角色演員                             | 備註 |
| ---------------------------------- | ---------------------------------- | -------------------------------- | -------------------------------- | -- |
| **2006**                           | [棟篤狀王](../Page/棟篤狀王.md "wikilink") | 自在                               | [李丹霞](../Page/李丹霞.md "wikilink") |    |
| [籃球部落](../Page/籃球部落.md "wikilink") | **羅敏敏**                            | [郭芯其](../Page/郭芯其.md "wikilink") |                                  |    |
|                                    |                                    |                                  |                                  |    |

### 台劇

| 播映年份                                   | 作品名稱                                 | 配演角色                             | 角色演員                             | 備註 |
| -------------------------------------- | ------------------------------------ | -------------------------------- | -------------------------------- | -- |
| **2005**                               | [MVP情人](../Page/MVP情人.md "wikilink") | **明涓**                           | [林立雯](../Page/林立雯.md "wikilink") |    |
| [香草戀人館](../Page/香草戀人館.md "wikilink")   | 易爾珊                                  |                                  |                                  |    |
| **2006**                               | [惡作劇之吻](../Page/惡作劇之吻.md "wikilink") | 林純美                              | [楊佩婷](../Page/楊佩婷.md "wikilink") |    |
| [王子變青蛙](../Page/王子變青蛙.md "wikilink")   | 蘇立欣                                  | [蘇立欣](../Page/蘇立欣.md "wikilink") |                                  |    |
| [大醫院小醫師](../Page/大醫院小醫師.md "wikilink") | 牙牙                                   |                                  |                                  |    |
| **2007**                               | [愛情經紀約](../Page/愛情經紀約.md "wikilink") | 宣夢跳                              | [梁心頤](../Page/梁心頤.md "wikilink") |    |
| [愛殺17](../Page/愛殺17.md "wikilink")     | 謝愛琳                                  | [鍾欣怡](../Page/鍾欣怡.md "wikilink") |                                  |    |
| [白色巨塔](../Page/白色巨塔.md "wikilink")     | 劉心萍                                  | [張惠春](../Page/張惠春.md "wikilink") |                                  |    |
| **2009**                               | [天下第一味](../Page/天下第一味.md "wikilink") | 劉慧雯                              | [周幼婷](../Page/周幼婷.md "wikilink") |    |
|                                        |                                      |                                  |                                  |    |

### 歐美劇

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/鬼線人.md" title="wikilink">鬼線人</a></p></td>
<td><p><strong>杜雅莉</strong></p></td>
<td><p><a href="../Page/:en:Sofia_Vassilieva.md" title="wikilink">Sofia Vassilieva</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/鬼線人.md" title="wikilink">鬼線人II</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/OC_(第三季).md" title="wikilink">OC III</a></p></td>
<td><p>顧琪麗<br />
滕天蘭</p></td>
<td><p><a href="../Page/薇拉·賀蘭德.md" title="wikilink">薇拉·賀蘭德</a><br />
<a href="../Page/:en:Autumn_Reeser.md" title="wikilink">Autumn Reeser</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逃.md" title="wikilink">逃</a></p></td>
<td><p><strong>唐慧嘉</strong></p></td>
<td><p><a href="../Page/羅賓·東尼.md" title="wikilink">羅賓·東尼</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:en:Invasion_(TV_series).md" title="wikilink">來者不善</a></p></td>
<td><p><strong>靳姬娜</strong></p></td>
<td><p><a href="../Page/:en:Alexis_Dziena.md" title="wikilink">Alexis Dziena</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/逃.md" title="wikilink">逃II</a></p></td>
<td><p><strong>唐慧嘉</strong></p></td>
<td><p><a href="../Page/羅賓·東尼.md" title="wikilink">羅賓·東尼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

| 播映年份                                  | 作品名稱                               | 配演角色                                     | 角色演員                                                       | 備註 |
| ------------------------------------- | ---------------------------------- | ---------------------------------------- | ---------------------------------------------------------- | -- |
| **2006**                              | [死神來了](../Page/死神來了.md "wikilink") | 嘉黛                                       | [Lisa Marie Caruk](../Page/Lisa_Marie_Caruk.md "wikilink") |    |
| [驚兆](../Page/天兆_\(電影\).md "wikilink") | **夏寶**                             | [艾碧姬·比絲蓮](../Page/艾碧姬·比絲蓮.md "wikilink") |                                                            |    |
|                                       |                                    |                                          |                                                            |    |

### 綜藝及資訊節目／紀錄片

| 播映年份     | 作品名稱                                 | 主要配演 | 備註                                    |
| -------- | ------------------------------------ | ---- | ------------------------------------- |
| **2007** | [古典魔力客](../Page/古典魔力客.md "wikilink") | 魔力客  | 接替離職的[周文瑛](../Page/周文瑛.md "wikilink") |
|          |                                      |      |                                       |

## 外部連結

<references/>

[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港配音員](../Category/香港配音員.md "wikilink")
[Category:前無綫電視配音員](../Category/前無綫電視配音員.md "wikilink")
[Category:林姓](../Category/林姓.md "wikilink")

1.  雜誌**天馬行空**：[聲聲相識配音員-林雅婷](http://i927.photobucket.com/albums/ad112/Dubpark/M_2005_skyteen/Sky_teens_yorki-1.jpg)，刊於2006年（摘自留聲館藏：香港粵語配音員資料庫）
2.  雜誌**天馬行空**：[聲聲相識配音員-林雅婷](http://i927.photobucket.com/albums/ad112/Dubpark/M_2005_skyteen/Sky_teens_yorki-1.jpg)，刊於2006年（摘自留聲館藏：香港粵語配音員資料庫）