**枚方市**（）是位於[日本](../Page/日本.md "wikilink")[大阪府東北部的](../Page/大阪府.md "wikilink")[都市](../Page/都市.md "wikilink")，與[京都府](../Page/京都府.md "wikilink")、[奈良縣接鄰](../Page/奈良縣.md "wikilink")，大致位於[大阪市和](../Page/大阪市.md "wikilink")[京都市的中間](../Page/京都市.md "wikilink")，以[七夕傳說和用菊花做的偶人展聞名](../Page/七夕.md "wikilink")。為大阪府轄內第4大城市，人口約40萬，次於[大阪市](../Page/大阪市.md "wikilink")、[堺市與](../Page/堺市.md "wikilink")[東大阪市](../Page/東大阪市.md "wikilink")。

轄區東部為的山區，最東側為高度約100公尺的山丘，往西高度逐漸降低為丘陵地，最西側為[近畿平原一的部分](../Page/近畿平原.md "wikilink")，並以[淀川為西界](../Page/淀川.md "wikilink")。丘陵地區的地層為從約200萬年前開始歷經[冰河時期和](../Page/冰河時期.md "wikilink")[間冰期堆積的地層](../Page/間冰期.md "wikilink")，在此曾發掘出東洋象和[梅花鹿等的](../Page/梅花鹿.md "wikilink")[化石](../Page/化石.md "wikilink")。在生駒山地和近畿平原的交界處還有[活斷層](../Page/活斷層.md "wikilink")，其中又包含田口斷層、交野斷層、枚方斷層以南北向穿過轄區。

市區內有多條河川自東側的生駒山地發源，向西穿越市區後匯入東側的淀川。

氣候屬於[瀨戶內海式氣候](../Page/瀨戶內海式氣候.md "wikilink")，溫暖雨量少；夏天則由於[熱島現象的影響](../Page/熱島.md "wikilink")，來自[大阪灣的海風在經過](../Page/大阪灣.md "wikilink")[大阪市市區附近時被加熱](../Page/大阪市.md "wikilink")，使得氣溫比大阪市內略高。

以本地為主場的職業球隊有：[V聯賽的](../Page/V聯賽.md "wikilink")[松下黑豹](../Page/松下黑豹.md "wikilink")。

## 歷史

「枚方」的地名最早在8世紀完成的[古事記](../Page/古事記.md "wikilink")、[日本書紀和](../Page/日本書紀.md "wikilink")[播磨國風土記中就有提到](../Page/播磨國風土記.md "wikilink")，在令制國時代此地屬於[河內國](../Page/河內國.md "wikilink")[交野郡和](../Page/交野郡.md "wikilink")[茨田郡](../Page/茨田郡.md "wikilink")。此地過去是被稱為「交野原」的台地和丘陵地區域，有許多鳥獸棲息，距離[京都又近](../Page/京都.md "wikilink")，因此也一度成為貴族們的狩獵場地\[1\]。[桓武天皇也曾在此設置](../Page/桓武天皇.md "wikilink")，並畫設天皇專用的狩獵區，此區域的地名後來也因此成為「禁野」（現在的枚方市禁野本町）。

在[江戶時代此地設有](../Page/江戶時代.md "wikilink")，為的[宿場](../Page/宿場.md "wikilink")\[2\]，現在的枚方市轄區在江戶時代時為旗本、[小田原藩和](../Page/小田原藩.md "wikilink")的領地。\[3\]

19世紀明治維新後，這些區域曾先後隸屬大坂裁判所（後來的大阪府）、[河內縣](../Page/河內縣_\(日本\).md "wikilink")、[堺縣](../Page/堺縣.md "wikilink")，直到1881年才隨著堺縣被併入[大阪府](../Page/大阪府.md "wikilink")。1889年實施[町村制](../Page/町村制.md "wikilink")，現在的枚方市轄區在當時分屬**枚方町**、、、、、、、、、，共10個行政區劃。到1930年代，這些行政區劃被整併為位於西部的**枚方町**，和東部的兩個；枚方町在1947年先升格為**枚方市**\[4\]，津田町再於1955年併入枚方市，形成現在的枚方市轄區範圍。

20世紀初，枚方市曾經成為軍需產業的要地，[大阪工廠曾在此設置](../Page/大阪砲兵工廠.md "wikilink")生產砲彈、火藥，並在一旁設置，東京第二陸軍造兵廠也在此設有香里製造所生產火藥。

戰後轄內開設了枚方中小企業工業園區（現在的枚方企業工業園區）、枚方成衣工業園區（現在地枚方西服工業園區）、枚方家具工業園區，引入了大量中小企業進駐。

2000年日本開始實施[特例市制度](../Page/特例市.md "wikilink")，枚方市在2001年即成為特例市，2014年進一步成為[中核市](../Page/中核市.md "wikilink")。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1896年4月1日</p></th>
<th><p>1896年 - 1912年</p></th>
<th><p>1912年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1988年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>枚方町</p></td>
<td><p>1938年11月3日<br />
合併為枚方町</p></td>
<td><p>1947年8月1日<br />
改制為枚方市</p></td>
<td><p>枚方市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>蹉跎村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>牧野村</p></td>
<td><p>1935年2月11日<br />
合併為殿山町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>招提村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川越村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>樟葉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>津田村</p></td>
<td><p>1940年11月15日<br />
合併為津田町</p></td>
<td><p>1955年10月15日<br />
併入枚方市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>冰室村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>菅原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任首長

  - 枚方市長\[5\]

<table>
<thead>
<tr class="header">
<th><p>屆</p></th>
<th><p>姓名</p></th>
<th><p>上任日期</p></th>
<th><p>卸任日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1～2</p></td>
<td></td>
<td><p>1947年8月1日</p></td>
<td><p>1955年4月30日</p></td>
<td><p>原枚方町長、改制後直接擔任市長</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>畠山晴文</p></td>
<td><p>1955年5月1日</p></td>
<td><p>1959年4月30日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4～5</p></td>
<td><p>寺嶋宗一郎</p></td>
<td><p>1959年5月1日</p></td>
<td><p>1967年4月30日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6～8</p></td>
<td><p>山村富造</p></td>
<td><p>1967年5月1日</p></td>
<td><p>1975年8月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9～12</p></td>
<td><p>北牧一雄</p></td>
<td><p>1975年8月31日</p></td>
<td><p>1991年8月30日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>大鹽和男</p></td>
<td><p>1991年8月31日</p></td>
<td><p>1995年4月30日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>14～17</p></td>
<td></td>
<td><p>1995年5月1日</p></td>
<td><p>2007年9月9日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18～19</p></td>
<td><p>竹內脩</p></td>
<td><p>2007年9月23日</p></td>
<td><p>2015年9月22日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>伏見隆</p></td>
<td><p>2015年9月23日</p></td>
<td><p>現任</p></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

[京阪電氣鐵道](../Page/京阪電氣鐵道.md "wikilink")[京阪本線自轄區西側以南北向穿過轄內主要市區](../Page/京阪本線.md "wikilink")，京阪本線往南可進入大阪市市區，往北可達京都市市區，其中在[枚方市車站另有](../Page/枚方市車站.md "wikilink")[交野線在此分出](../Page/交野線.md "wikilink")，往東南方向進入交野市市區；而[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[片町線則穿過東部轄區](../Page/片町線.md "wikilink")，往東可接往[奈良市](../Page/奈良市.md "wikilink")。

往西跨越[淀川僅有](../Page/淀川.md "wikilink")一條道路，過去在1930年代枚方大橋完成以前，曾有多處設有渡船作為往返淀川兩岸的交通工具。

### 鐵路

  - [京阪電氣鐵道](../Page/京阪電氣鐵道.md "wikilink")

<!-- end list -->

  - [Number_prefix_Keihan_lines.png](https://zh.wikipedia.org/wiki/File:Number_prefix_Keihan_lines.png "fig:Number_prefix_Keihan_lines.png")
    [京阪本線](../Page/京阪本線.md "wikilink")：（←[寢屋川市](../Page/寢屋川市.md "wikilink")）
    -  -  - [枚方市站](../Page/枚方市站.md "wikilink") -  -  -  -
    （[八幡市](../Page/八幡市.md "wikilink")→）
  - [Number_prefix_Keihan_lines.png](https://zh.wikipedia.org/wiki/File:Number_prefix_Keihan_lines.png "fig:Number_prefix_Keihan_lines.png")
    [交野線](../Page/交野線.md "wikilink")：枚方市站 -  -  -  -
    （[交野市](../Page/交野市.md "wikilink")）

<!-- end list -->

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）

<!-- end list -->

  - [片町線](../Page/片町線.md "wikilink")（學研都市線）：（←[京田邊市](../Page/京田邊市.md "wikilink")）
    -  -  -  - （交野市→）

<File:Kozenji> west entrance.jpg| 光善寺車站 <File:Hirakatakoen> station east
entrance.jpg| 枚方公園車站 <File:Hirakata-shi> Station Minami Entrance.jpg|
枚方市車站 <File:Keihan> Makino station west entrance.jpg| 牧野車站
<File:KH-KuzuhaStation.jpg>| 樟葉車站 <File:Keihan> Hoshigaoka stn.jpg| 星丘車站
<File:Murano> stn 1.jpg| 村野車站 <File:津田駅> - panoramio.jpg| 津田車站

### 公路

  - 快速道路

<!-- end list -->

  - ：（[京田邊市](../Page/京田邊市.md "wikilink")） -
    [枚方東交流道](../Page/枚方東交流道.md "wikilink") -
    [枚方學研交流道](../Page/枚方學研交流道.md "wikilink") -
    （[交野市](../Page/交野市.md "wikilink")） -
    [交野南交流道](../Page/交野南交流道.md "wikilink") - （交野市）

## 觀光資源

位於西南部隸屬[京阪電氣鐵道集團的](../Page/京阪電氣鐵道.md "wikilink")為於1912年設立的[遊樂園](../Page/遊樂園.md "wikilink")，至今已有超過百年歷史，過去也曾每年固定舉辦以菊花製作人偶為主的，但在2005年由於此工藝技術人員逐漸老化，欠缺後繼傳承者，且展覽帶來的經濟效益有限，故枚方大菊人形展已停止固定舉辦，改為不定期舉辦。

在南側與[交野市交界處過去在](../Page/交野市.md "wikilink")[平安時代被稱為](../Page/平安時代.md "wikilink")「交野原」的區域，過去來自中國的[渡來人從中國帶來了](../Page/渡來人.md "wikilink")[七夕的故事](../Page/七夕.md "wikilink")，並在此區域規劃了對應七夕故事的地名，貫穿交野市和枚方市的由於日文發音與「天之川」相同，而「天之川」在日文中即為[銀河](../Page/銀河.md "wikilink")，位於天野川北側的（交野市境內）祭祀的是[織女](../Page/織女.md "wikilink")，南側的牽牛石則視為[牛郎](../Page/牛郎.md "wikilink")（枚方市境內），而在天野川上則有「逢合橋」（交野市境內）和「鵲橋」（枚方市境內）兩座橋梁。也因此七夕的故事被枚方市和交野市作為共同推展觀光的主題，每年固定此此為主題在七夕期間舉辦活動。\[6\]\[7\]

<File:Hirakata> Park.jpg| 枚方公園內的摩天輪 <File:Vkiku2007.jpg>| 枚方大菊人形展的展出內容
<File:Amanogawa> - panoramio.jpg| 天野川

## 教育

[Osaka_Dental_University.JPG](https://zh.wikipedia.org/wiki/File:Osaka_Dental_University.JPG "fig:Osaka_Dental_University.JPG")
[KMU_Campus.jpg](https://zh.wikipedia.org/wiki/File:KMU_Campus.jpg "fig:KMU_Campus.jpg")

枚方市轄內的高等教育學校包括：、、[关西外国语大学](../Page/关西外国语大学.md "wikilink")、[关西外国语大学短期大学部](../Page/关西外国语大学短期大学部.md "wikilink")、[攝南大學枚方校區](../Page/攝南大學.md "wikilink")、[大阪工業大學枚方校區](../Page/大阪工業大學.md "wikilink")、枚方校區。

高中方面，擁有兩所[橄欖球運動的著名學校](../Page/橄欖球.md "wikilink")，和，這兩所學校在共獲得超過10次之優勝。

此外轄內還有一間，如同名稱，該校是由[大阪市所設立並營運的公立高中](../Page/大阪市.md "wikilink")，也是大阪的市立高中之中，唯一不位於大阪市的。

## 姊妹、友好城市

### 日本

  - [四萬十市](../Page/四萬十市.md "wikilink")（[高知縣](../Page/高知縣.md "wikilink")）
      -
        1974年4月枚方市與[中村市締結為友好都市](../Page/中村市.md "wikilink")，中村市於2005年合併為為四萬十市後，兩地於2008年再次締結為友好都市。
  - [別海町](../Page/別海町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")）
      -
        兩地於1987年2月締結為友好都市。
  - [高松市](../Page/高松市.md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")）
      -
        兩地於1987年2月締結為友好都市。
  - [名護市](../Page/名護市.md "wikilink")（[沖繩縣](../Page/沖繩縣.md "wikilink")）
      -
        兩地於1997年7月締結為友好都市。
  - [伊達市](../Page/伊達市_\(北海道\).md "wikilink")（[北海道](../Page/北海道.md "wikilink")）
      -
        1999年7月與[大瀧村共同發表市民交流都市宣言](../Page/大瀧村_\(北海道\).md "wikilink")，大瀧村於2006年被併入伊達市。
  - [天川村](../Page/天川村.md "wikilink")（[奈良縣](../Page/奈良縣.md "wikilink")）
      -
        兩地於1999年7月共同發表市民交流都市宣言。
  - [波佐見町](../Page/波佐見町.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")）
      -
        兩地於1999年7月共同發表市民交流都市宣言。

### 海外

  - [長寧區](../Page/長寧區.md "wikilink")（[中國](../Page/中國.md "wikilink")[上海市](../Page/上海市.md "wikilink")）

      -
        兩地於1987年12月締結為友好都市。

  - [洛根市](../Page/洛根市_\(昆士蘭州\).md "wikilink")（[澳大利亞](../Page/澳大利亞.md "wikilink")[昆士蘭州](../Page/昆士蘭州.md "wikilink")）

      -
        兩地於1995年3月締結為友好都市。

  - [靈岩郡](../Page/靈岩郡.md "wikilink")（[韓國](../Page/韓國.md "wikilink")[全羅南道](../Page/全羅南道.md "wikilink")）

      -
        兩地於2008年3月締結為友好都市。

## 本地出身之名人

  - ：演員、作曲家、作詞家、歌手

  - [岡田准一](../Page/岡田准一.md "wikilink")：藝人

  - [宓多里](../Page/宓多里.md "wikilink")：小提琴演奏家

  - [片渕須直](../Page/片渕須直.md "wikilink")：動畫導演

  - [橋本哲](../Page/橋本哲.md "wikilink")：藝人

  - [小笠原茉由](../Page/小笠原茉由.md "wikilink")：藝人

  - [谷內伸也](../Page/谷內伸也.md "wikilink")：歌手

  - [清水藍里](../Page/清水藍里.md "wikilink")：模特兒、演員

  - [尾形春水](../Page/尾形春水.md "wikilink")：歌手

  - ：前足球選手

  - [中村光](../Page/中村光_\(棋手\).md "wikilink")：西洋棋棋士

  - [本並健治](../Page/本並健治.md "wikilink")：前足球選手

  - ：足球選手

  - [武田洋平](../Page/武田洋平.md "wikilink")：足球選手

  - ：足球選手

  - ：足球選手

## 參考資料

## 相關條目

  -
  -
## 外部連結

  - [枚方文化觀光協會](http://www.hirakata-kanko.org/)

  -

  -

  -
[Category:枚方市](../Category/枚方市.md "wikilink")

1.

2.
3.

4.
5.  [第46回枚方市統計書（平成28年版）](http://www.city.hirakata.osaka.jp/cmsfiles/contents/0000011/11922/18-01-01.pdf)

6.

7.