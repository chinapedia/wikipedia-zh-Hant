**枝蕨綱**是[植物的一個類群](../Page/植物.md "wikilink")，目前已絕種，被認為是[蕨類與](../Page/蕨類.md "wikilink")[木賊](../Page/木賊.md "wikilink")\[1\]的祖先。

枝蕨綱的植物有一個主幹，主幹上頭接有數條橫枝。此類植物的化石出現於[泥盆紀和](../Page/泥盆紀.md "wikilink")[石炭紀期間](../Page/石炭紀.md "wikilink")，大多數都只有莖部。

枝蕨綱的分類學還不是很清楚，只知主要的分類包含兩個目－「[枝蕨目](../Page/枝蕨目.md "wikilink")」和「[叉葉蕨目](../Page/叉葉蕨目.md "wikilink")」。

[泥盆紀中期時枝蕨綱的](../Page/泥盆紀中期.md "wikilink")「[瓦蒂薩](../Page/瓦蒂薩屬.md "wikilink")」完整不缺的化石於2007年被確定，化石外形顯示其曾為一顆樹。

## 參考文獻

  - [UC Museum of
    Palentology](http://www.ucmp.berkeley.edu/plants/cladoxylopsida/cladoxylopsida.html)
  - Stein, W. E., F. Mannolini, L. V. Hernick, E. Landling, and C. M.
    Berry. 2007. [Giant cladoxylopsid trees resolve the enigma of the
    Earth's earliest forest stumps at
    Gilboa](http://www.nature.com/nature/journal/v446/n7138/abs/nature05705.html)。Nature,
    446:904-907.

[Category:古植物](../Category/古植物.md "wikilink")
[Category:泥盆紀生物](../Category/泥盆紀生物.md "wikilink")

1.  [藝術與建築索引典—枝蕨綱](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300310156)
    於2010年9月13日查閱