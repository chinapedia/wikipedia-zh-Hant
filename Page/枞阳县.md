**阳县**位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[安徽省中南部](../Page/安徽省.md "wikilink")、[长江北岸](../Page/长江.md "wikilink")，是[铜陵市下辖的一个](../Page/铜陵市.md "wikilink")[县](../Page/县_\(中华人民共和国\).md "wikilink")。境内有名胜[浮山](../Page/浮山.md "wikilink")，林壑幽静。

## 历史

[西周时为宗子国](../Page/西周.md "wikilink")；[汉武帝](../Page/汉武帝.md "wikilink")[元封五年](../Page/元封.md "wikilink")（前106年）置枞阳县，属[庐江郡](../Page/庐江郡.md "wikilink")；隋[开皇十八年](../Page/开皇.md "wikilink")（598年）改同安县，属[同安郡](../Page/同安郡.md "wikilink")；唐[至德二年](../Page/至德.md "wikilink")（757年）改[桐城县](../Page/桐城县.md "wikilink")。

1949年2月18日，划[桐城东](../Page/桐城.md "wikilink")、南乡，[庐江](../Page/庐江.md "wikilink")、[无为两县少量地区设制](../Page/无为.md "wikilink")[桐庐县](../Page/桐庐县_\(安徽省\).md "wikilink")，县治初设[项铺镇](../Page/项铺镇.md "wikilink")，后移[汤家沟](../Page/汤沟镇_\(枞阳县\).md "wikilink")；1951年2月24日更名[湖东县](../Page/湖东县.md "wikilink")；1955年7月1日更名为枞阳县，先属[安庆专区](../Page/安庆专区.md "wikilink")、[安庆地区](../Page/安庆地区.md "wikilink")，后属[安庆市](../Page/安庆市.md "wikilink")。

2015年，国务院批复同意对安徽省部分行政区划进行调整，将安庆市枞阳县划归铜陵市管辖。

## 地理

### 地貌

枞阳属著名的庐（江）枞（阳）火山岩盆地。[中生代以来的构造运动奠定了地貌](../Page/中生代.md "wikilink")，形成了北高南低，中部低平，低山丘陵岗冲相间，滨江环湖，分4个亚区，丘陵、湖泊、平原依次排列的格局：

  - 东北部低山区：三公山为最高峰（674.9米），其他高于400米的低山有拔茅山、龙王尖、黄梅尖等，外围多为400米左右的丘陵。
  - 西北部低丘岗地平原区：除西北隅岱鳌山（245米）、东南面浮山（165米）和南端低丘外，大面积系[黄土形成的漫岗](../Page/黄土.md "wikilink")，地面超伏较小。
  - 中西部丘陵冲区：该区濒临菜子湖，西北邻低丘岗地平原区，北界低山区，南、东与江湖洲圩平原区相连。
  - 东南部江湖洲圩平原区：长江绕县境自西南至东北环行，连城、白荡、陈瑶、枫沙等湖沿长江内侧平行分布，是优质[棉产区](../Page/棉.md "wikilink")。

### 水文

枞阳属[长江流域水系](../Page/长江流域.md "wikilink")，境内[陈瑶湖](../Page/陈瑶湖.md "wikilink")、[白荡湖](../Page/白荡湖.md "wikilink")、[菜子湖](../Page/菜子湖.md "wikilink")、和“两赛”（[神灵赛湖](../Page/神灵赛湖.md "wikilink")、[羹脍赛湖](../Page/羹脍赛湖.md "wikilink")）4个水系，河网密度0.22公里/平方公里。流域面积：白荡湖为775平方公里，陈瑶湖为183.3平方公里，菜子湖为397.5平方公里，“两赛”为68.5平方公里。境内河流纵横，水系发达。

### 气候

属北亚热带湿润气候区，四季分明、日照充足、热量丰富、雨量充沛、无霜期长。年均气温16．5℃，年均降水量1326.5毫米，冬季降水少，夏季（梅雨季）雨量集中，约占全年的40％。年均日照数2065.9小时，无霜期251天。东北风为主导风向，年均风速3.2米/秒。

## 区划

下辖16镇，6个乡：

  - 镇：[枞阳镇](../Page/枞阳镇.md "wikilink")、[𠙶山镇](../Page/𠙶山镇.md "wikilink")、[汤沟镇](../Page/汤沟镇_\(枞阳县\).md "wikilink")、[横埠镇](../Page/横埠镇.md "wikilink")、[项铺镇](../Page/项铺镇.md "wikilink")、[钱桥镇](../Page/钱桥镇.md "wikilink")、[麒麟镇](../Page/麒麟镇_\(枞阳县\).md "wikilink")、[义津镇](../Page/义津镇.md "wikilink")、[浮山镇](../Page/浮山镇.md "wikilink")、[会宫镇](../Page/会宫镇.md "wikilink")、[官埠桥镇](../Page/官埠桥镇.md "wikilink")、[钱铺镇](../Page/钱铺镇.md "wikilink")、[白柳镇](../Page/白柳镇.md "wikilink")
  - 乡：铁铜乡、凤仪乡、长沙乡、金社乡、白梅乡、雨坛乡。

## 文化

### 历史名人

[桐城派主要代表人物](../Page/桐城派.md "wikilink")[方苞](../Page/方苞.md "wikilink")、[刘大櫆](../Page/刘大櫆.md "wikilink")、[姚鼐](../Page/姚鼐.md "wikilink")

### 方言

方言系[汉语](../Page/汉语.md "wikilink")[江淮官话](../Page/江淮官话.md "wikilink")[黄孝片的安庆话](../Page/黄孝片.md "wikilink")。

## 外部链接

## 参考文献

[安庆](../Page/category:安徽县份.md "wikilink")

[枞阳县](../Category/枞阳县.md "wikilink") [县](../Category/铜陵区县.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")