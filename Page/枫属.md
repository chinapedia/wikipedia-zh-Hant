**枫属**（[学名](../Page/学名.md "wikilink")：）又名**槭屬**，是[无患子目](../Page/无患子目.md "wikilink")[无患子科的一个](../Page/无患子科.md "wikilink")[属](../Page/属.md "wikilink")，通称为**枫树**、**槭树**，舊時歸類於舊科名[槭树科](../Page/槭树科.md "wikilink")（Aceraceaedie），但是最近[分子生物学研究结果表明](../Page/分子生物学.md "wikilink")，它应该归到[无患子科中](../Page/无患子科.md "wikilink")。

本属大概有110到200种，幾乎都位在北半球，廣泛分佈於溫帶副熱帶地區，南半球僅一物種。

## 描述

[Bi-colored_Maple_Tree.jpg](https://zh.wikipedia.org/wiki/File:Bi-colored_Maple_Tree.jpg "fig:Bi-colored_Maple_Tree.jpg")
枫是夏绿[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")。[叶对生](../Page/叶.md "wikilink")。如梣叶枫（*A.
negundo*）的叶是羽状的。枫树中有雌雄同株，也有雌雄异株的种。有的种为虫媒，有的则是风媒。花有五瓣，雄蕊部分退化。[子房上位](../Page/子房.md "wikilink")。花底部有一圆盘状结构，在非风媒的树种上，这是用作吸引昆虫的结构。[花程式为](../Page/花程式.md "wikilink")：\(* K 5 \; C 5 \; A_{5-8} \; G_{\underline(2)}\)。果实为[离果](../Page/翅果.md "wikilink")。

## 源地

自[冰河時期起](../Page/冰河時期.md "wikilink")，在[歐洲就只有少部分楓樹存餘](../Page/歐洲.md "wikilink")。大部分種都是来自於[亞洲和](../Page/亞洲.md "wikilink")[美洲](../Page/美洲.md "wikilink")。

### 園藝

楓被屋主、企業和市政局等種植作[觀賞樹](../Page/園藝植物.md "wikilink")。[挪威楓](../Page/挪威楓.md "wikilink")*（Acer
platanoides）*就是常見的例子，因為挪威楓生長迅速及特別耐寒。此外，挪威楓亦是一些地區的[入侵物種](../Page/入侵物種.md "wikilink")。尤其是小或稀有的楓樹品種，常被製作成標本。\[1\]

#### 盆景

[Bonsai_Federahorn.jpg](https://zh.wikipedia.org/wiki/File:Bonsai_Federahorn.jpg "fig:Bonsai_Federahorn.jpg")\]\]
把楓用作[盆景藝術是一個常見的選擇](../Page/盆景.md "wikilink")。雖然大多品種的楓都可以作盆景，但是常見的選擇有[日本楓](../Page/日本楓.md "wikilink")、[三角枫](../Page/三角枫.md "wikilink")、[茶条槭](../Page/茶条槭.md "wikilink")、[田園楓](../Page/田園楓.md "wikilink")、[彼楓](../Page/彼楓.md "wikilink")，因為上述品種有良好的葉子還原和[衍生的技能](../Page/衍生物.md "wikilink")。\[2\]

#### 栽培種

因為楓能夠以[無性生殖傳播](../Page/無性生殖.md "wikilink")，例如[插枝](../Page/插枝.md "wikilink")、[組織培養](../Page/組織培養.md "wikilink")、發芽或[移植](../Page/移植_\(植物\).md "wikilink")，所以楓有許多[栽培種](../Page/栽培種.md "wikilink")。單是日本楓*（Acer
palmatum）*就已經有超過1000個栽培種，大多在日本種植，它們大都不需要長時間繁殖或不能在[西方國家進行培植](../Page/西方世界.md "wikilink")。一些需小心處理的栽培種多數會在花盆中生長及很少高度會達到多於50-100厘米。

### 商業用途

楓是糖漿和木材的重要原料。乾木會被用作[燻食物](../Page/燻.md "wikilink")。楓也種植作觀賞植物及為[旅遊業和](../Page/旅遊業.md "wikilink")[農業帶來收益](../Page/農業.md "wikilink")。

#### 木材

[Riegelahorn.JPG](https://zh.wikipedia.org/wiki/File:Riegelahorn.JPG "fig:Riegelahorn.JPG")
[Maple7951.JPG](https://zh.wikipedia.org/wiki/File:Maple7951.JPG "fig:Maple7951.JPG")的花\]\]
[岩枫](../Page/假挪威槭.md "wikilink")（*Acer
pseudoplatanus*）的木材是最有价值的高级木材之一。黄白色或是白色，[年轮虽可辨](../Page/年轮.md "wikilink")，但是边材和中心部分却是因为颜色相同而很难区分。重量适中，有弹性，坚韧结实的木材不易萎缩。抗屈能力强。一直以来枫树材只用在内部扩建的工程和家具中。其表面加工容易，抛光，酸洗和着色难度小。上漆也很容易。

#### 楓樹糖漿

[糖楓](../Page/糖楓.md "wikilink")（*Acer
saccharum*）含有豐富的[樹液](../Page/樹液.md "wikilink")，把樹液煮沸後會產業楓樹糖漿或製成楓糖、楓太妃糖。製成一升楓樹糖漿需要40升的糖楓樹液。糖楓的近親品種也可以製成糖漿，但產量比糖楓少。

#### 食物

楓的種子有時會放入水煮，以去除其苦味，加入咖啡中。

#### 農業

在早春時，楓早於大部分的植物開花，楓成為是[花粉的主要原料](../Page/花粉.md "wikilink")，所以楓對在晚春和夏季扮演重要角色的[蜜蜂的生存非常重要](../Page/蜜蜂.md "wikilink")。

## 种

在枫属下大约有100到200个种。

<File:Illustration> Acer
platanoides1.jpg|[挪威枫](../Page/挪威枫.md "wikilink")
<File:Acer> opalus
obtusatum0.jpg|[意大利枫](../Page/意大利枫.md "wikilink")（*Acer
opalus*）的树叶 <File:Acer>
tataricum1.jpg|[鞑靼枫](../Page/鞑靼枫.md "wikilink")（*A.
tataricum*） <File:Canadian> maple.jpg|路邊的加拿大楓樹 <File:Canadian> Maple
Leaf.JPG|加拿大楓葉
[File:ChinaBenXiMaple.jpg|中国辽宁本溪楓葉](File:ChinaBenXiMaple.jpg%7C中国辽宁本溪楓葉)

## 注釋

## 外部链接

  - [Komplette Liste der Ahornarten bei
    en.wikipedia](http://en.wikipedia.org/wiki/List_of_Acer_species)
  - [einige Ahornsorten
    (engl.)](http://www.richsfoxwillowpines.com/HTML%20Files/Tree%20Pages/Acer-Maple%20Page.htm)
  - [Systematik: Eine Gliederung der Gattung
    Acer](https://web.archive.org/web/20051123081608/http://www.inh.co.jp/~hayasida/Etaxono1.html)
  - [楓與楓香辨正](http://tnl.twbbs.org/article/taxa/maple/maple.htm)
  - [攝影人應該知道的「楓樹」知識](http://flora-tw.tumblr.com/post/36252191957)

[枫属](../Category/枫属.md "wikilink")
[Category:七葉樹亞科](../Category/七葉樹亞科.md "wikilink")
[Category:落葉植物](../Category/落葉植物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  van Gelderen, C. J. & van Gelderen, D. M. (1999). *Maples for
    Gardens: A Color Encyclopedia*

2.