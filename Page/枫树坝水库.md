**楓樹壩水庫**位於[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[河源市](../Page/河源市.md "wikilink")[龍川縣](../Page/龍川縣.md "wikilink")[楓樹壩鎮的](../Page/楓樹壩鎮.md "wikilink")[東江上游干流上](../Page/東江.md "wikilink")，是集防洪、發電、供水於一體的綜合性[水庫](../Page/水庫.md "wikilink")，水庫的大壩高91.5[米](../Page/米.md "wikilink")，壩頂長418米，屬空腹重力型壩。總庫容19.4億立方米，控制流域面積5150平方公里，是廣東第二大的[人工湖](../Page/人工湖.md "wikilink")。

楓樹壩水電站是目前廣東第三大水力發電站，水電站的總裝機容量15.0萬千瓦。1970年5月正式動工，兩台機組分別於1973年12月26日和1974年11月29日正式發電。

## 參考文獻

[F](../Category/广东发电厂.md "wikilink") [F](../Category/河源水库.md "wikilink")
[F](../Category/龙川县.md "wikilink") [F](../Category/东江水库.md "wikilink")