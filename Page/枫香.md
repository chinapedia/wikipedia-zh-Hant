**楓香樹**（[學名](../Page/學名.md "wikilink")：）\[1\]，別稱**台灣香膠樹**、**楓樹**、**楓子樹**、**楓仔樹**、**楓仔**、**香楓**、**白楓**、**白膠香**、**雞楓樹**、**雞爪楓**、**靈楓**、**大葉楓**、**香菇木**、**路路通**等\[2\]\[3\]\[4\]\[5\]\[6\]，為[楓香科](../Page/楓香科.md "wikilink")[楓香樹屬植物](../Page/楓香樹屬.md "wikilink")\[7\]，[種加詞意為formosana](../Page/種加詞.md "wikilink")「臺灣的
」\[8\]。

## 別名

由於楓香樹的圓球形[果序](../Page/果序.md "wikilink")，於[蒴果開裂時種子從多處掉下](../Page/蒴果.md "wikilink")，看似管道很多，故而有路路通之名\[9\]。

## 植物學史

楓香樹的[模式標本於](../Page/模式標本.md "wikilink")1864年由採集家[奧德漢](../Page/奧德漢.md "wikilink")(Oldham,
Richard)在[台灣](../Page/台灣.md "wikilink")[淡水採集](../Page/淡水區.md "wikilink")，並將大部份標本送往[邱园](../Page/英國皇家邱植物園.md "wikilink")，另一部份送往[大英博物館](../Page/大英博物館.md "wikilink")，交由植物學家進行研究，正式學名由植物學[漢斯](../Page/漢斯.md "wikilink")(Hance)
賦予，在1866年發表於[法國自然科學年報](../Page/法國自然科學年報.md "wikilink")，一直未曾動搖沿用至今，現時模式標本存放於大英博物館\[10\]。

## 分佈

楓香樹原產於[中國南部及](../Page/中國.md "wikilink")[台灣](../Page/台灣.md "wikilink")\[11\]，現分佈於[老撾](../Page/老撾.md "wikilink")、[越南北部](../Page/越南.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[朝鮮南部等地](../Page/朝鮮.md "wikilink")\[12\]，[中國國內分佈於](../Page/中國.md "wikilink")[海南](../Page/海南.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[雲南東南部](../Page/雲南.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[香港](../Page/香港.md "wikilink")\[13\]、[福建](../Page/福建.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[四川](../Page/四川.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")、[河南](../Page/河南.md "wikilink")、[陝西](../Page/陝西.md "wikilink")、[甘肅等省份](../Page/甘肅.md "wikilink")\[14\]\[15\]。本種為陽性[植物](../Page/植物.md "wikilink")，喜好陽光\[16\]、耐蔭、耐火\[17\]、抗有毒氣體\[18\]及抗風寒\[19\]，野外分佈於[海拔](../Page/海拔.md "wikilink")2000米以下溝谷、山坡、次生林中\[20\]或溪河沿岸等地\[21\]\[22\]，深根性，喜生於濕潤、肥沃的[酸性及](../Page/酸性.md "wikilink")[中性壤土](../Page/中性.md "wikilink")，粘重黃泥土或沙質礫土亦能生長\[23\]。可於[冬季或](../Page/冬季.md "wikilink")[春季繁殖](../Page/春季.md "wikilink")，多使用條播或撒播等方式播種\[24\]。楓香樹的[種子每](../Page/種子.md "wikilink")[公斤約有](../Page/公斤.md "wikilink")180,000－320,000顆，播重量為每畝0.5－1[公斤](../Page/公斤.md "wikilink")\[25\]。

## 形態特徵

[Liquidambar_formosana_01.jpg](https://zh.wikipedia.org/wiki/File:Liquidambar_formosana_01.jpg "fig:Liquidambar_formosana_01.jpg")
楓香樹是一種落葉大[喬木](../Page/喬木.md "wikilink")\[26\]，樹形呈圓錐形\[27\]，高約20－40[米](../Page/米.md "wikilink")\[28\]，高可達40[米](../Page/米.md "wikilink")\[29\]，胸徑最大可達1[米以上](../Page/米.md "wikilink")\[30\]。幼年及中年時[樹皮淡灰色](../Page/樹皮.md "wikilink")，平滑\[31\]，老時樹皮灰褐色\[32\]，粗糙而厚，淺縱向溝裂\[33\]\[34\]，方塊狀剝落\[35\]；小枝灰色\[36\]，披柔毛，略有皮孔，乾後灰色；[芽體橢圓狀卵形](../Page/芽.md "wikilink")\[37\]，略披棕色柔毛\[38\]，長約1[厘米](../Page/厘米.md "wikilink")；鱗狀苞片5－6枚\[39\]，常有[樹脂](../Page/樹脂.md "wikilink")，乾後棕黑色，有光澤\[40\]\[41\]。

### 葉

[葉為單葉互生](../Page/葉.md "wikilink")\[42\]或叢生枝端\[43\]，心形\[44\]或闊卵狀三角形\[45\]，薄革質\[46\]，掌狀3裂或幼時5－7裂\[47\]\[48\]成三角形\[49\]及不裂呈卵形\[50\]，兩側裂片較短，平展，頂端尾狀漸尖\[51\]；中央裂片較長，卵形\[52\]或卵狀三角形\[53\]，頂端尾尖或漸尖\[54\]；基部微心形、心形或平截\[55\]；表面綠色，光亮\[56\]，乾後灰綠色，不發亮，顏色隨季節轉變，入[秋後轉變成黃色至翌年](../Page/秋.md "wikilink")[春季落葉時轉變成紅色](../Page/春季.md "wikilink")\[57\]\[58\]；背部無毛或幼時披短柔毛\[59\]，後脫落\[60\]，僅沿主脈或脈腋間披短柔毛\[61\]；掌狀脈3－5條，表面及背面均明顯，網脈顯著\[62\]；[葉緣鋸齒狀](../Page/葉緣.md "wikilink")，齒尖有腺狀突\[63\]，長約6－13[厘米](../Page/厘米.md "wikilink")，寬約6－15\[64\][厘米](../Page/厘米.md "wikilink")\[65\]。[葉柄圓柱形](../Page/葉柄.md "wikilink")\[66\]，常披短柔毛，長約3\[67\]－12\[68\][厘米](../Page/厘米.md "wikilink")；[托葉](../Page/托葉.md "wikilink")2枚\[69\]，線狀，紅褐色，披柔毛，離生或略與葉柄合生，早落，長約1－1.5\[70\][厘米](../Page/厘米.md "wikilink")。

### 花

[花單性](../Page/花.md "wikilink")，雌雄同株，異花\[71\]\[72\]，無[花被](../Page/花被.md "wikilink")\[73\]；雄性花由短穗狀、葇荑\[74\]或頭狀[花序掛列成總狀花序式](../Page/花序.md "wikilink")\[75\]，淡黃綠色\[76\]，頂生\[77\]，具有黑色[鱗片](../Page/鱗片.md "wikilink")\[78\]，像長有頭髮\[79\]，無[花瓣及](../Page/花瓣.md "wikilink")[花萼](../Page/花萼.md "wikilink")\[80\]，[雄蕊多數](../Page/雄蕊.md "wikilink")，[花絲不等長](../Page/花絲.md "wikilink")，[花藥紅色](../Page/花藥.md "wikilink")，比花絲略短；雌性花掛列成圓球形的頭狀花序\[81\]\[82\]，具有黑色鱗片\[83\]，無花瓣\[84\]\[85\]，每花序有花22\[86\]－43朵\[87\]\[88\]，直徑約1.5[厘米](../Page/厘米.md "wikilink")\[89\]；具細長總梗\[90\]，[花序柄偶有皮孔](../Page/花序柄.md "wikilink")，無[腺體](../Page/腺體.md "wikilink")，長約3－6[厘米](../Page/厘米.md "wikilink")\[91\]\[92\]；[萼齒](../Page/萼齒.md "wikilink")4－7枚\[93\]，針形\[94\]，長約4－10[毫米](../Page/毫米.md "wikilink")\[95\]；[子房](../Page/子房.md "wikilink")2室\[96\]，每室[胚珠多數](../Page/胚珠.md "wikilink")\[97\]，下半部藏於花序軸之內，上半部遊離，連[花柱披柔毛](../Page/花柱.md "wikilink")\[98\]；[花柱](../Page/花柱.md "wikilink")2分叉\[99\]，柱頭常卷曲\[100\]，紫紅色\[101\]，長約6－10[毫米](../Page/毫米.md "wikilink")\[102\]。

### 果

[果為](../Page/果.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，由多枚[蒴果聚合成頭狀](../Page/蒴果.md "wikilink")[果序](../Page/果序.md "wikilink")\[103\]\[104\]，圓球形，密生星芒狀刺\[105\]，形如[流星鎚](../Page/流星鎚.md "wikilink")\[106\]\[107\]\[108\]，木質\[109\]，連宿存花柱直徑約2.5－5\[110\][厘米](../Page/厘米.md "wikilink")\[111\]\[112\]；每果序內有果22－43枚\[113\]；2瓣開裂\[114\]\[115\]，2淺裂\[116\]，下半部藏於木質花序軸之內\[117\]，宿存[花柱及刺針狀萼齒](../Page/花柱.md "wikilink")。[種子橢圓形](../Page/種子.md "wikilink")\[118\]，多數，扁平\[119\]，多角形，有膜質窄翅\[120\]\[121\]，成熟時深褐色\[122\]。

## 用途

楓香樹的枝幹可採[樹脂供製](../Page/樹脂.md "wikilink")[線香及藥用](../Page/線香.md "wikilink")\[123\]；枝幹可供培植[香菇](../Page/香菇.md "wikilink")\[124\]\[125\]\[126\]，[木材堅硬兼能抗](../Page/木材.md "wikilink")[白蟻](../Page/白蟻.md "wikilink")，是良好的建築及傢俱材料\[127\]\[128\]；[葉可供飼養](../Page/葉.md "wikilink")[天蠶蛾](../Page/天蠶蛾.md "wikilink")\[129\]\[130\]\[131\]，落葉壓乾後可作[書籤](../Page/書籤.md "wikilink")\[132\]；[蒴果乾後可作乾燥花素材](../Page/蒴果.md "wikilink")\[133\]。春夏季時綠蔭遮天，可作為行道樹、庭園樹或[盆景樹種](../Page/盆景.md "wikilink")\[134\]\[135\]\[136\]。於[台灣分別為](../Page/台灣.md "wikilink")[長尾水青蛾及](../Page/長尾水青蛾.md "wikilink")[四黑目天蠶蛾等數種](../Page/四黑目天蠶蛾.md "wikilink")[蛾類幼蟲的食物來源](../Page/蛾.md "wikilink")\[137\]。

## 醫藥用途

### 樹脂

楓香樹以乾燥[樹脂入藥](../Page/樹脂.md "wikilink")\[138\]，味辛、微苦，性平\[139\]，歸肝、脾經\[140\]，中藥名為楓香脂\[141\]，別名楓脂、膠香、白膠、白膠香、芸香等\[142\]，始載於《新修本草》\[143\]，《[本草綱目拾遺](../Page/本草綱目拾遺.md "wikilink")》記載為「一切痈疽瘡疥，金瘡吐衄咯血，活血生肌，止痛解毒。燒過揩牙，永無牙疾。」\[144\]，[藥材主產於](../Page/藥材.md "wikilink")[福建](../Page/福建.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[江西](../Page/江西.md "wikilink")、[浙江](../Page/浙江.md "wikilink")[慶元及](../Page/慶元.md "wikilink")[龍泉等地](../Page/龍泉.md "wikilink")\[145\]\[146\]，為[中醫臨床用藥](../Page/中醫.md "wikilink")。具活血止痛、止血、生肌、涼血、解毒等功效\[147\]\[148\]\[149\]，主治外傷出血、跌打損傷、癰疽腫痛、牙痛、衄血、吐血、咯血、[金瘡出血等症狀](../Page/金瘡.md "wikilink")\[150\]\[151\]，現代藥理研究表明楓香脂具止血、抗[血栓形成](../Page/血栓.md "wikilink")、抗[血小板聚集](../Page/血小板.md "wikilink")、抗心律失常、抗缺氧、擴張[冠狀動脈等作用](../Page/冠狀動脈.md "wikilink")，臨床應用於急性[腸胃炎等治療上](../Page/腸胃炎.md "wikilink")\[152\]。

### 葉

楓香樹以[葉入藥](../Page/葉.md "wikilink")，具清熱解毒、收斂止血等功效\[153\]。[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[旗山區一帶的原住民](../Page/旗山區.md "wikilink")，從前利用楓香樹的[葉直接貼於擦傷處作止血治療](../Page/葉.md "wikilink")\[154\]。

### 果序

楓香樹以乾燥成熟[果序入藥](../Page/果序.md "wikilink")\[155\]，味苦，性平\[156\]，歸肝、腎經
\[157\]，[孕婦忌用](../Page/孕婦.md "wikilink")\[158\]，中藥名為路路通\[159\]，別名楓香果、楓球、楓果、楓實、九室子、狼眼、狼目等\[160\]，始載於《[本草綱目拾遺](../Page/本草綱目拾遺.md "wikilink")》，原文記載為「
辟瘴却瘟，明目，除濕，舒筋絡拘彎，周身痹痛，手腳及腰痛。」\[161\]，[藥材主產於](../Page/藥材.md "wikilink")[福建](../Page/福建.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[陝西](../Page/陝西.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[安徽等地](../Page/安徽.md "wikilink")\[162\]\[163\]，為[中醫臨床用藥](../Page/中醫.md "wikilink")，歸袪風濕、風寒濕藥。具利水通經、消腫、祛風活絡、除濕\[164\]、疏[肝等功效](../Page/肝.md "wikilink")\[165\]\[166\]\[167\]，主治關節痹痛、胃痛\[168\]、水腫脹滿、麻木拘攣、乳少、經閉、[濕疹](../Page/濕疹.md "wikilink")\[169\]\[170\]等症狀，現代藥理研究表明路路通具抗肝損傷、調節[免疫](../Page/免疫.md "wikilink")、抗[炎等作用](../Page/炎.md "wikilink")\[171\]\[172\]，臨床應用於產後缺乳、不[射精症](../Page/射精.md "wikilink")、増生性骨[關節炎](../Page/關節炎.md "wikilink")、突發性耳聾等治療上\[173\]。因路路通具有良好的護肝作用，於[台灣常被用作防治肝炎的藥物](../Page/台灣.md "wikilink")\[174\]。

## 藥材鑑定

本品載錄於《[中國藥典](../Page/中國藥典.md "wikilink")》2005年版，定為[中藥楓香脂及路路通的法定原植物來源種](../Page/中藥.md "wikilink")，主要透過[高效液相色譜法等鑑定法測定](../Page/高效液相色譜法.md "wikilink")，規定[中藥](../Page/中藥.md "wikilink")[路路通中的路路通酸含量不可少於](../Page/路路通.md "wikilink")0.15%，以控制藥材的質量\[175\]。

### 樹脂

[中藥楓香脂以楓香樹的乾燥](../Page/中藥.md "wikilink")[樹脂入](../Page/樹脂.md "wikilink")[藥](../Page/藥.md "wikilink")，於[夏季](../Page/夏季.md "wikilink")7－8月間\[176\]選擇樹齡逾20年以上的粗壯大樹\[177\]，從樹根起每隔15－20[厘米交錯割裂鑿開一洞](../Page/厘米.md "wikilink")\[178\]，使[樹脂從樹幹割裂處流出](../Page/樹脂.md "wikilink")，直到10月至翌年4月間採收\[179\]，自然乾燥或曬乾\[180\]\[181\]。本品質地脆弱易碎，斷面具[玻璃樣光澤](../Page/玻璃.md "wikilink")\[182\]呈類圓顆粒狀或不規則塊狀，大小不一，表面淡黃色至黃棕色，半透明或不透明，直徑多在0.5－1[厘米之間](../Page/厘米.md "wikilink")，少數為3[厘米](../Page/厘米.md "wikilink")\[183\]。氣清香，燃燒時更濃，味淡\[184\]\[185\]。

### 果序

[中藥路路通以楓香樹的乾燥](../Page/中藥.md "wikilink")[果序入藥](../Page/果序.md "wikilink")，於[冬季](../Page/冬季.md "wikilink")[果實成熟後採收](../Page/果實.md "wikilink")\[186\]\[187\]，採收後洗淨除去雜質後曬乾\[188\]。本品質地堅硬，不易破開\[189\]\[190\]；[果序呈圓球形](../Page/果序.md "wikilink")，由多枚小[蒴果聚合而成](../Page/蒴果.md "wikilink")，表面灰棕色至棕褐色，有多數尖刺狀宿存萼齒及[鳥喙狀](../Page/鳥喙.md "wikilink")[花柱](../Page/花柱.md "wikilink")，彎曲或常折斷，去除後呈現多數[蜂窩狀小孔](../Page/蜂窩.md "wikilink")，直徑約2－3[厘米](../Page/厘米.md "wikilink")\[191\]\[192\]；基部具圓柱形[果柄](../Page/果柄.md "wikilink")，常折斷或僅有果柄痕，長約3－4.5[厘米](../Page/厘米.md "wikilink")\[193\]\[194\]；小[蒴果頂端開裂](../Page/蒴果.md "wikilink")，形成蜂窩狀小孔，可見[種子多數](../Page/種子.md "wikilink")，種子發育不全者多角形，細小，直徑約1[毫米](../Page/毫米.md "wikilink")，種子發育完全者扁平長圓形，少數，黃棕色至棕褐色，具翅\[195\]\[196\]；體輕，氣微香，味淡\[197\]\[198\]。傳統經驗則認為以色黃、個大及無果梗者為佳\[199\]。

## 化學成份

[文山國小楓樹伯公2.jpg](https://zh.wikipedia.org/wiki/File:文山國小楓樹伯公2.jpg "fig:文山國小楓樹伯公2.jpg")\]\]

### 樹皮

楓香樹的[樹皮中主要含](../Page/樹皮.md "wikilink")[環烯醚萜苷類成分](../Page/環烯醚萜苷類.md "wikilink")，如有
6α-羥基京尼平苷 (6α-hydroxygeniposide)、6β-羥基京尼平苷
(6β-hydroxygeniposide)、[水晶蘭苷甲酯](../Page/水晶蘭苷甲酯.md "wikilink")
(monotropein methyl ester)及[水晶蘭苷等](../Page/水晶蘭苷.md "wikilink")\[200\]。

### 樹脂

楓香樹的[樹脂中主要含](../Page/樹脂.md "wikilink")[揮發油及](../Page/揮發油.md "wikilink")[齊墩果烷型三萜類成分](../Page/齊墩果烷型三萜類.md "wikilink")，[揮發油成分如有](../Page/揮發油.md "wikilink")[樟腦萜](../Page/樟腦萜.md "wikilink")
(camphene)、[異松油烯](../Page/異松油烯.md "wikilink")
(terpinolene)、[丁香烯](../Page/丁香烯.md "wikilink")
(caryophyllene)及[醋酸龍腦酯](../Page/醋酸龍腦酯.md "wikilink")(bornyl
acetate)；[齊墩果烷型三萜類成分如有](../Page/齊墩果烷型三萜類.md "wikilink")[楓香酸](../Page/楓香酸.md "wikilink")
(forucosolic acid)、[路路通醛](../Page/路路通醛.md "wikilink")
(liquidambronal)、[路路通醛酸](../Page/路路通醛酸.md "wikilink")(liquidambronic
acid)、[齊墩果酸](../Page/齊墩果酸.md "wikilink")、[愛勃龍醛](../Page/愛勃龍醛.md "wikilink")
(ambronal)、[阿波酮酸](../Page/阿波酮酸.md "wikilink")(ambronic
acid)、[阿波醇酸](../Page/阿波醇酸.md "wikilink")(ambrolic
acid)、[阿姆布二醇酸](../Page/阿姆布二醇酸.md "wikilink") (ambradiolic
acid)及28-hydroxy-β-amyrone等\[201\]。

### 葉

楓香樹的[葉中主要含](../Page/葉.md "wikilink")[黃酮類](../Page/黃酮類.md "wikilink")、[鞣質類](../Page/鞣質類.md "wikilink")、[環烯醚萜苷類成分](../Page/環烯醚萜苷類.md "wikilink")，[黃酮類成分如有](../Page/黃酮類.md "wikilink")[異槲皮素](../Page/異槲皮素.md "wikilink")
(isoquercitrin)、[蘆丁](../Page/蘆丁.md "wikilink")
(rutin)、楊梅樹素-3-O-（6”-沒食子酰）葡萄糖苷
\[myricetin-3-O-(6”-galloyl)-glucoside\]、[黃芪苷](../Page/黃芪苷.md "wikilink")
(astragalin)、[三葉草苷](../Page/三葉草苷.md "wikilink")
(trifolin)、[金絲桃苷](../Page/金絲桃苷.md "wikilink")
(hyperin)、楊梅樹皮素-3-O-葡萄糖苷(myricetin-3-O-glucoside)及
tellimoside等；[鞣質類成分如有](../Page/鞣質類.md "wikilink")[楓香鞣質](../Page/楓香鞣質.md "wikilink")
(liquidambin)、[木麻黃鞣質](../Page/木麻黃鞣質.md "wikilink")
(casuariin)、[木麻黃鞣寧](../Page/木麻黃鞣寧.md "wikilink")
(casuarinin)、[木麻黃鞣亭](../Page/木麻黃鞣亭.md "wikilink")
(casuarictin)、[赤芍素](../Page/赤芍素.md "wikilink")(pedunculagin)、[新嗩吶草素](../Page/新嗩吶草素.md "wikilink")
I、II (tellimagrandins I-II)及[異皺褶菌素](../Page/異皺褶菌素.md "wikilink") A、B、D
(isorugosins A-B,
D)等；[環烯醚萜苷類成分如有](../Page/環烯醚萜苷類.md "wikilink")[水晶蘭苷](../Page/水晶蘭苷.md "wikilink")
(monotropein)等\[202\]。

### 果實

楓香樹的[果實中主要含](../Page/果實.md "wikilink")[揮發油及](../Page/揮發油.md "wikilink")[齊墩果烷型三萜類等](../Page/齊墩果烷型三萜類.md "wikilink")，[揮發油成分如有](../Page/揮發油.md "wikilink")[檸檬烯](../Page/檸檬烯.md "wikilink")
(limonene)、[桃金娘醛](../Page/桃金娘醛.md "wikilink") (myrtenal)、β-、γ-松油烯 (β-,
γ-terpinenes)及β-蒎烯
(β-pinene)等；[齊墩果烷型三萜類成分如有](../Page/齊墩果烷型三萜類.md "wikilink")[羥基齊墩果內酯](../Page/羥基齊墩果內酯.md "wikilink")
(hydroxyoleanolic lactone)、[路路通內酯](../Page/路路通內酯.md "wikilink")
(liquidambaric lactone)、[路路通酸](../Page/路路通酸.md "wikilink")
(liquidambaric acid, betulonic acid)、[齊墩果酸](../Page/齊墩果酸.md "wikilink")
(oleanolic acid)、[馬纓丹酸](../Page/馬纓丹酸.md "wikilink") (lantanolic
acid)、[熊果酸](../Page/熊果酸.md "wikilink") (ursolic
acid)及[阿江欖仁酸](../Page/阿江欖仁酸.md "wikilink") (arjunolic
acid)等；另還含有[蘇合香素環氧化物](../Page/蘇合香素環氧化物.md "wikilink") (styracin
epoxide)、[異蘇合香素環氧化物](../Page/異蘇合香素環氧化物.md "wikilink") (isostyracin
epoxide)及[左旋桂皮酸龍腦酯](../Page/左旋桂皮酸龍腦酯.md "wikilink") \[(-)-bornyl
cinnamate\]等成份\[203\]。

## 參考

## 外部連結

  -
  -
  -
  -
  - [楓香 台灣野生植物資料庫
    特有生物研究保育中心](http://plant.tesri.gov.tw/plant100/WebPlantDetail.aspx?tno=403005010)

  - [楓香樹
    Fengxiangshu](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00307)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [路路通
    Lulutong](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00192)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [路路通 Lu Lu
    Tong](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00364)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [楓香脂
    Fengxiangzhi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00363)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [楓香脂 Feng Xiang
    Zhi](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00528)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[樹](../Category/藥用植物.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:枫香树属](../Category/枫香树属.md "wikilink")
[Category:行道樹](../Category/行道樹.md "wikilink")
[Category:中國植物](../Category/中國植物.md "wikilink")
[Category:台灣植物](../Category/台灣植物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:老撾植物](../Category/老撾植物.md "wikilink")
[Category:越南植物](../Category/越南植物.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.
14.
15.
16.

17.

18.
19.

20.

21.
22.
23.
24.
25.

26.
27.
28.
29.
30.

31.
32.
33.
34.

35.
36.
37.

38.
39.
40.
41.

42.
43.

44.
45.

46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.

58.

59.
60.
61.
62.
63.
64.
65.
66.
67.
68.
69.
70.
71.
72.
73.
74.
75.
76.
77.
78.
79.

80.
81.
82.
83.
84.
85.
86.
87.
88.
89.
90.
91.
92.
93.
94.
95.
96.
97.
98.
99.
100.
101.
102.
103.
104.
105.
106.
107.
108.
109.
110.
111.
112.
113.
114.
115.
116.
117.
118.
119.
120.
121.
122.
123.
124.
125.
126.

127.
128.
129.
130.
131.
132.
133.
134.
135.
136.
137.
138.

139.

140.

141.
142.
143.
144.
145.
146.

147.
148.
149.
150.
151.
152.
153.
154.
155.
156.

157.

158.

159.
160.
161.
162.

163.

164.
165.
166.
167.
168.
169.
170.
171.
172.
173.
174.
175.
176.
177.
178.
179.
180.
181.
182.
183.
184.
185.
186.
187.
188.
189.
190.
191.
192.
193.
194.
195.
196.
197.
198.
199.
200.
201.
202.
203.