[Chongqing_Road.jpg](https://zh.wikipedia.org/wiki/File:Chongqing_Road.jpg "fig:Chongqing_Road.jpg")的[南北高架路](../Page/上海南北高架路.md "wikilink")（[重庆南路](../Page/重庆南路_\(上海\).md "wikilink")[淮海中路口](../Page/淮海中路.md "wikilink")）\]\]
[South_entrance_of_Zheng_Qi_Overpass_20110114.jpg](https://zh.wikipedia.org/wiki/File:South_entrance_of_Zheng_Qi_Overpass_20110114.jpg "fig:South_entrance_of_Zheng_Qi_Overpass_20110114.jpg")[信義區的](../Page/信義區_\(臺北市\).md "wikilink")[正氣橋](../Page/正氣橋.md "wikilink")，為[市民大道](../Page/市民大道.md "wikilink")、[麥帥一橋](../Page/麥帥一橋.md "wikilink")、[基隆路及](../Page/基隆路.md "wikilink")[南京東路等多條幹道的連接要徑](../Page/南京東路_\(台北\).md "wikilink")。\]\]
[BAB_1.JPG](https://zh.wikipedia.org/wiki/File:BAB_1.JPG "fig:BAB_1.JPG")

**架空道路**，又稱-{zh-tw:**架空道路**（香港）;zh-cn:**架空道路**（香港）;zh-hk:**高架道路**（中國大陸與台灣）;}-
、**-{行車天橋}-**（澳門、香港和新加坡）、**高-{}-架橋**（台灣與中國大陸），或簡稱為**高架**，是主要或僅供[車輛行駛使用的立體式](../Page/車輛.md "wikilink")[道路](../Page/道路.md "wikilink")，為[高架橋的形式之一](../Page/高架橋.md "wikilink")，常運用於[高速公路或](../Page/高速公路.md "wikilink")[快速道路](../Page/快速道路.md "wikilink")。

## 簡介

高架道路主要為提升行車速度、或解決道路與[鐵路或](../Page/鐵路.md "wikilink")[行人動線交會的安全問題而](../Page/行人.md "wikilink")[建築](../Page/建築.md "wikilink")，全线不设置红绿灯，通过[匝道与地面道路连接](../Page/匝道.md "wikilink")，同時以[立體交叉的方式避免與地面道路的交匯](../Page/立體交叉.md "wikilink")。在[都市地帶](../Page/都市.md "wikilink")，为了满足快速出入都市中心区域的需要，也会有长达数十公里的道路全部都在高架道路上的设计。

行車天橋的設施通常是[行人路與行車道明顯分隔](../Page/行人路.md "wikilink")，或者不設行人道、巴士站、[斑馬線](../Page/斑馬線.md "wikilink")、[交通燈等](../Page/交通燈.md "wikilink")，甚至不設[單車徑](../Page/單車徑.md "wikilink")。行車天橋上的設施與高速公路或快速道路無異，有[街燈](../Page/街燈.md "wikilink")、指示牌、[路標](../Page/路標.md "wikilink")、防護欄等。

为解决[铁路分割市区](../Page/铁路.md "wikilink")、铁路[平交道安全隐患的问题](../Page/平交道.md "wikilink")，而與鐵路立體跨越的架空道路，也称为「跨线桥」。若以公路在地面、铁路从上方跨越公路的高架橋形式，即为[铁路高架化](../Page/铁路高架化.md "wikilink")。

除了行車天橋，[行人天橋也是解決](../Page/行人天橋.md "wikilink")「人車爭路」的方案之一。

## 公路-{zh-hans:高架;zh-sg:架空;zh-hk:架空;zh-tw:高架;}-案例

  -   - [臺北市](../Page/臺北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")：[汐止五股高架道路](../Page/汐止五股高架道路.md "wikilink")、[新店八里快速公路](../Page/台64線.md "wikilink")、[新北環河快速道路](../Page/新北環河快速道路.md "wikilink")
      - [基隆市](../Page/基隆市.md "wikilink")：[東岸高架道路](../Page/東岸高架道路.md "wikilink")、[基隆港西岸聯外道路](../Page/基隆港西岸聯外道路.md "wikilink")、[基隆港東岸聯外道路](../Page/基隆港東岸聯外道路.md "wikilink")
      - [嘉義市](../Page/嘉義市.md "wikilink")：[垂楊大橋](../Page/垂楊大橋.md "wikilink")
      - [臺南市](../Page/臺南市.md "wikilink")：[台南關廟快速公路](../Page/台86線.md "wikilink")、[國道八號部分路段](../Page/國道八號_\(中華民國\).md "wikilink")、[新港社大道](../Page/新港社大道.md "wikilink")
      - [高雄市](../Page/高雄市.md "wikilink")：[高雄潮州快速公路](../Page/台88線.md "wikilink")、[高雄都會區快速道路](../Page/高雄都會區快速道路.md "wikilink")、[國道十號左營仁武段](../Page/國道十號_\(中華民國\).md "wikilink")、[中博臨時高架橋](../Page/中博臨時高架橋.md "wikilink")
      - [屏東縣](../Page/屏東縣.md "wikilink")：[高雄潮州快速公路](../Page/台88線.md "wikilink")、[國道三號九如林邊段](../Page/福爾摩沙高速公路.md "wikilink")

  - ：[東區走廊](../Page/東區走廊.md "wikilink")、[觀塘繞道](../Page/觀塘繞道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[深港西部通道](../Page/深港西部通道.md "wikilink")、[銅鑼灣行車天橋](../Page/銅鑼灣行車天橋.md "wikilink")、[港珠澳大橋](../Page/港珠澳大橋.md "wikilink")

  - ：[西灣大橋](../Page/西灣大橋.md "wikilink")、[港珠澳大橋](../Page/港珠澳大橋.md "wikilink")、[嘉樂庇總督大橋](../Page/嘉樂庇總督大橋.md "wikilink")、[友誼大橋](../Page/友誼大橋.md "wikilink")

  -   - [上海](../Page/上海.md "wikilink")：[内环高架路](../Page/上海内环线.md "wikilink")、[南北高架路](../Page/上海南北高架路.md "wikilink")、[东西高架路](../Page/上海延安高架路.md "wikilink")
      - [武漢](../Page/武漢.md "wikilink")：[武漢内环线](../Page/武漢内环线.md "wikilink")
      - [廣州](../Page/廣州.md "wikilink")：[廣州內環路](../Page/廣州內環路.md "wikilink")、[東濠涌高架](../Page/東濠涌高架.md "wikilink")、[人民路高架路](../Page/人民路高架路.md "wikilink")

  -   - [東京](../Page/東京.md "wikilink")：[首都高速道路大部分路線](../Page/首都高速道路.md "wikilink")
      - [大阪](../Page/大阪.md "wikilink")：[阪神高速道路大部分路線](../Page/阪神高速道路.md "wikilink")

  - ：[聯邦大橋](../Page/聯邦大橋.md "wikilink")

## 铁路-{zh-hans:高架;zh-sg:架空;zh-hk:架空;zh-tw:高架;}-案例

###

#### 臺北

  - [臺北捷運](../Page/臺北捷運.md "wikilink")[文湖線](../Page/文湖線.md "wikilink")（全線除[松山機場站及](../Page/松山機場站.md "wikilink")[大直站外均採高架形式](../Page/大直站.md "wikilink")）
  - 臺北捷運[淡水信義線](../Page/淡水信義線.md "wikilink")（[圓山站至](../Page/圓山站.md "wikilink")[北投站段](../Page/北投站.md "wikilink")）\[1\]
  - [縱貫線
    (北段)](../Page/汐止七堵鐵路高架化專案.md "wikilink")（臺灣鐵路管理局縱貫線[五堵車站至](../Page/五堵車站.md "wikilink")[汐科車站間路段](../Page/汐科車站.md "wikilink")）

#### 宜蘭

  - [東部鐵路後續改善計畫](../Page/臺鐵高架化#東部鐵路後續改善計畫.md "wikilink")（臺灣鐵路管理局宜蘭線[冬山車站路段](../Page/冬山車站.md "wikilink")）

#### 臺中

  - [臺鐵西部幹線山線](../Page/臺中都會區鐵路高架捷運化計畫.md "wikilink")（臺灣鐵路管理局西部幹線山線[豐原車站至](../Page/豐原車站.md "wikilink")[大慶車站間路段](../Page/大慶車站.md "wikilink")）

#### 彰化

  - [臺鐵西部幹線縱貫線南段](../Page/員林市區鐵路高架化計畫.md "wikilink")（臺灣鐵路管理局西部幹線縱貫線南段北勢路平交道北方約930公尺至[員林大排北方約](../Page/員林大排.md "wikilink")100公尺間路段）

#### 臺南

  - [臺鐵沙崙線](../Page/沙崙線.md "wikilink")（全線除[中洲車站外均採高架形式](../Page/中洲車站.md "wikilink")）

#### 高雄

  - [高雄捷運紅線](../Page/高雄捷運紅線.md "wikilink")（[世運站至](../Page/世運站.md "wikilink")[橋頭火車站段](../Page/橋頭車站.md "wikilink")）

#### 屏東

  - [臺鐵西部幹線屏東線與](../Page/高雄潮州鐵路捷運化計畫.md "wikilink")[臺鐵西部幹線屏東線(林邊鄉)](../Page/林邊鄉鐵路高架化計畫.md "wikilink")（臺灣鐵路管理局西部幹線屏東線[屏東車站至](../Page/屏東車站.md "wikilink")[潮州車站及](../Page/潮州車站.md "wikilink")[鎮安車站至](../Page/鎮安車站.md "wikilink")[佳冬車站間路段](../Page/佳冬車站.md "wikilink")）

###

  - [香港](../Page/香港.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")（[錦上路站至](../Page/錦上路站.md "wikilink")[屯門站段](../Page/屯門站_\(西鐵綫\).md "wikilink")）
  - 香港港鐵[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")（全線除[石門站至](../Page/石門站_\(香港\).md "wikilink")[大水坑站一段外均採高架形式](../Page/大水坑站.md "wikilink")）

###

#### 上海

  - [上海轨道交通1号线](../Page/上海轨道交通1号线.md "wikilink")（[汶水路站至](../Page/汶水路站.md "wikilink")[富锦路站](../Page/富锦路站.md "wikilink")）
  - [上海轨道交通3号线](../Page/上海轨道交通3号线.md "wikilink")（除[铁力路站全线](../Page/铁力路站.md "wikilink")）
  - [上海轨道交通5号线](../Page/上海轨道交通5号线.md "wikilink")（除[莘庄站外全线](../Page/莘庄站.md "wikilink")）
  - [上海轨道交通6号线](../Page/上海轨道交通6号线.md "wikilink")（[港城路站西至](../Page/港城路站.md "wikilink")[五莲路站南段](../Page/五莲路站.md "wikilink")）
  - [上海轨道交通7号线](../Page/上海轨道交通7号线.md "wikilink")（[美兰湖站至](../Page/美兰湖站.md "wikilink")[罗南新村站段](../Page/罗南新村站.md "wikilink")）
  - [上海轨道交通8号线](../Page/上海轨道交通8号线.md "wikilink")（[芦恒路站至](../Page/芦恒路站.md "wikilink")[航天博物馆站段](../Page/航天博物馆站.md "wikilink")）
  - [上海轨道交通9号线](../Page/上海轨道交通9号线.md "wikilink")（[九亭站至](../Page/九亭站.md "wikilink")[松江大学城站段](../Page/松江大学城站.md "wikilink")）
  - [上海轨道交通11号线](../Page/上海轨道交通11号线.md "wikilink")（[南翔站至](../Page/南翔站.md "wikilink")[嘉定北站](../Page/嘉定北站.md "wikilink")，[昌吉东路站至](../Page/昌吉东路站.md "wikilink")[安亭站段](../Page/安亭站.md "wikilink")）

#### 广州

  - [广州地铁4号线](../Page/广州地铁4号线.md "wikilink")（[新造站至](../Page/新造站.md "wikilink")[金洲站](../Page/金洲站.md "wikilink")）
  - [广州地铁5号线](../Page/广州地铁5号线.md "wikilink")（部分路段）
  - [广州地铁6号线](../Page/广州地铁6号线.md "wikilink")（部分路段）

###

#### 芝加哥

  - [芝加哥捷運](../Page/芝加哥捷運.md "wikilink")（大部分路線）

## 資料來源

## 相關條目

  - [高架鐵路](../Page/高架鐵路.md "wikilink")
  - [立體交匯](../Page/立體交匯.md "wikilink")
  - [交流道](../Page/交流道.md "wikilink")

[\*](../Category/架空道路.md "wikilink")
[Category:道路型式](../Category/道路型式.md "wikilink")

1.