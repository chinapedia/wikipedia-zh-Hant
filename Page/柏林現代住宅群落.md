**柏林現代聚落**於2008年7月為[聯合國教科文組織指定](../Page/聯合國教科文組織.md "wikilink")[世界文化遺產](../Page/世界文化遺產.md "wikilink")。包含六個聚落區，呈現當年柏林不同都市分區結構。這些地區約於1913至1934年起建，建築師以現代風格設計。聚落區位在今日柏林的朋斯多夫（Bohnsdorf），布里茲（Britz），[夏洛特堡](../Page/夏洛特堡.md "wikilink")（Charlottenburg），普雷茲勞爾山（Prenzlauer
Berg），萊尼肯多夫（Reinickendorf）與威頂（Wedding）區等。他們代表的是戰後社群住宅區的時空意向。清楚與新穎的造型是當時社會期待的樣式，他決定二十世紀建築與都市房屋的走向。

當時具代表性的設計規劃者為[布魯諾·陶特](../Page/布魯諾·陶特.md "wikilink")（Bruno
Taut），[馬丁·瓦格纳](../Page/馬丁·瓦格纳.md "wikilink")（Martin
Wagner），當然[漢斯·夏隆](../Page/漢斯·夏隆.md "wikilink")（Hans
Scharoun）或[沃爾特·格羅佩斯](../Page/沃爾特·格羅佩斯.md "wikilink")（Walter
Gropius）亦參與規劃。柏林現代聚落最老的區是由陶特設計的（Gartenstadt
Falkenberg），1930年代初新即物主義風潮影響了白城與西門子大聚落城的規劃風格。

## 住宅群落列表

<table style="width:10%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 1%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"><p><span style="color:#000000;">住宅名称</span></p></th>
<th style="text-align: left;"><p><span style="color:#000000;">所在地（区）</span></p></th>
<th><p><span style="color:#000000;">建筑年份</span></p></th>
<th style="text-align: left;"><p><span style="color:#000000;">设计师</span></p></th>
<th style="text-align: left;"><p><span style="color:#000000;">建筑师</span></p></th>
<th><p><span style="color:#000000;">图片</span></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>法尔肯贝格花园<br />
<em>(Tuschkastensiedlung)</em></p></td>
<td style="text-align: left;"><p>柏林伯恩斯多夫<br />
（特雷普托-科佩尼克）</p></td>
<td><p>1913–1916</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特<br />
海因里希·特森诺夫</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tuschkastensiedlung-023.jpg" title="fig:Tuschkastensiedlung-023.jpg">Tuschkastensiedlung-023.jpg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>席勒公园群落</p></td>
<td style="text-align: left;"><p>柏林维丁<br />
（米特区）</p></td>
<td><p>1924–1930</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特<br />
马克斯·陶特（重建）<br />
汉斯·霍夫曼（扩充）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:20080715_14995_DSC01769_Siedlung_Schillerpark_Bristolstraße_5_bis_1.JPG" title="fig:20080715_14995_DSC01769_Siedlung_Schillerpark_Bristolstraße_5_bis_1.JPG">20080715_14995_DSC01769_Siedlung_Schillerpark_Bristolstraße_5_bis_1.JPG</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>胡斐森群落<br />
</p></td>
<td style="text-align: left;"><p>柏林布里茨<br />
（新科隆区）</p></td>
<td><p>1925–1930</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特<br />
马丁·瓦格纳</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hufeisensiedlung_rotefront.jpg" title="fig:Hufeisensiedlung_rotefront.jpg">Hufeisensiedlung_rotefront.jpg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>卡尔-勒基恩居住区</p></td>
<td style="text-align: left;"><p>普雷兹劳尔山<br />
（潘科夫区）</p></td>
<td><p>1928–1930</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特</p></td>
<td style="text-align: left;"><p>布鲁诺·陶特<br />
弗兰茨·希林格尔</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Berlin_C_Legien_Trachtenbrodtstr_20.jpg" title="fig:Berlin_C_Legien_Trachtenbrodtstr_20.jpg">Berlin_C_Legien_Trachtenbrodtstr_20.jpg</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>白城</p></td>
<td style="text-align: left;"><p>柏林莱尼肯多夫<br />
（莱尼肯多夫区）</p></td>
<td><p>1929–1931</p></td>
<td style="text-align: left;"><p>奥托·鲁道夫·萨尔维斯贝格<br />
马丁·瓦格纳（总规划）</p></td>
<td style="text-align: left;"><p>奥托·鲁道夫·萨尔维斯贝格<br />
布鲁诺·阿伦斯<br />
威廉·毕宁</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:20080716_14995_DSC02001_Weiße_Stadt_Aroser_Allee.JPG" title="fig:20080716_14995_DSC02001_Weiße_Stadt_Aroser_Allee.JPG">20080716_14995_DSC02001_Weiße_Stadt_Aroser_Allee.JPG</a></p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/西门子大聚落城.md" title="wikilink">西门子大聚落城</a><br />
<em>（环形聚落城）</em></p></td>
<td style="text-align: left;"><p>柏林夏洛特堡<br />
（夏洛特堡-维尔默斯多夫）</p></td>
<td><p>1929–1934</p></td>
<td style="text-align: left;"><p>汉斯·夏隆<br />
马丁·瓦格纳（总规划）</p></td>
<td style="text-align: left;"><p>汉斯·夏隆<br />
沃尔特·格罗佩斯<br />
奥托·巴特宁<br />
弗雷德·弗巴特<br />
胡格·哈林<br />
保罗·鲁道夫·亨宁</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Berlin_GS_Siemensstadt_Panzerkreuzer.jpg" title="fig:Berlin_GS_Siemensstadt_Panzerkreuzer.jpg">Berlin_GS_Siemensstadt_Panzerkreuzer.jpg</a></p></td>
</tr>
</tbody>
</table>

## 参考资料

  - Jörg Haspel, Annemarie Jaeggi: *Siedlungen der Berliner Moderne*.
    Deutscher Kunstverlag, München 2007. ISBN 978-3-422-02091-7.
  - Landesdenkmalamt Berlin im Auftrag der Senatsverwaltung für
    Stadtentwicklung Berlin (Hrsg.), Winfried Brenne (Projektleitung):
    *Siedlungen der Berliner Moderne. Nominierung zur Aufnahme in die
    Welterbeliste der UNESCO*. Berlin: Braun Verlagshaus, 2007, ISBN
    3-938780-20-7.

## 外部链接

  - [柏林20年代住宅群](http://www.stadtentwicklung.berlin.de/denkmal/denkmale_in_berlin/de/weltkulturerbe/siedlungen/index.shtml)
  - [柏林现代住宅群落](http://www.initiative-welterbe.de/)

[S](../Category/德国世界遗产.md "wikilink")
[Category:柏林](../Category/柏林.md "wikilink")
[Category:現代主義建築](../Category/現代主義建築.md "wikilink")
[Category:集合住宅](../Category/集合住宅.md "wikilink")