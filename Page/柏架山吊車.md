[1919-1920_View_from_Mount_Parker.jpg](https://zh.wikipedia.org/wiki/File:1919-1920_View_from_Mount_Parker.jpg "fig:1919-1920_View_from_Mount_Parker.jpg")）\]\]
**柏架山吊車**（或稱**柏架山纜車**）（）（1892年－1932年）是[香港一個已經拆卸的](../Page/香港.md "wikilink")[吊車系統](../Page/索道.md "wikilink")，僅運行了40年\[1\]，來往[香港島](../Page/香港島.md "wikilink")[東部的](../Page/東區_\(香港\).md "wikilink")[柏架山和](../Page/柏架山.md "wikilink")[鰂魚涌](../Page/鰂魚涌.md "wikilink")。

## 歷史

柏架山吊車於1892年修建，原本是[英商](../Page/英國.md "wikilink")[太古洋行為其職員提供的交通工具](../Page/太古洋行.md "wikilink")，作接載職員往返柏架山上的宿舍和山下的太古企業之用。在非繁忙時段，吊車也會接載遊人和附近居民。\[2\]吊車起點在[祐民街](../Page/祐民街.md "wikilink")（近[過海隧道巴士116線的巴士總站](../Page/過海隧道巴士116線.md "wikilink")），終點站是柏架山上的[大風坳](../Page/大風坳_\(畢拿山\).md "wikilink")，有2架可坐6人的吊車相向行駛。

## 記載

有書曾記載，[清朝時有一人士坐過吊車遊覽](../Page/清朝.md "wikilink")，大為驚嘆並賦詩以記其感受，可見當時的吊車是新鮮事物。後因使用率低而停用，其歷史只有40年。

清朝光緒年間，畫家[吳友如曾將柏架山吊車畫進其畫內](../Page/吳友如.md "wikilink")，並稱之為「銅鑼飛棧」（可能誤認此處為銅鑼灣），他寫道﹕「振衣直上者恆苦跋履不易，乃太古糖局竟建屋於其上……於是製成一桶，可容五六人。其上也如匹練之升空，其下也[燭之武之見秦師](../Page/燭之武退秦師.md "wikilink")。錘而出之，上下自如，不費足力，人感嘆其法之巧」\[3\]。

## 參考

<div class="references-small">

<references />

</div>

## 相關條目

  - [索道](../Page/索道.md "wikilink")

## 外部連結

[category:香港吊車](../Page/category:香港吊車.md "wikilink")
[category:鰂魚涌](../Page/category:鰂魚涌.md "wikilink")

[Category:香港已拆卸交通建築](../Category/香港已拆卸交通建築.md "wikilink")
[Category:1892年建立](../Category/1892年建立.md "wikilink")
[Category:1932年廢除](../Category/1932年廢除.md "wikilink")

1.
2.
3.