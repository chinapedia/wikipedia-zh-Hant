《**柏楊版資治通鑑**》是[台灣作家](../Page/台灣.md "wikilink")[柏楊將史學名著](../Page/柏楊.md "wikilink")《[資治通鑑](../Page/資治通鑑.md "wikilink")》重新以現代[白话文翻譯的一部叢書](../Page/白话文.md "wikilink")，由[遠流出版事業股份有限公司出版](../Page/遠流出版事業股份有限公司.md "wikilink")。在中国大陆由[万卷出版公司发行](../Page/万卷出版公司.md "wikilink")。\[1\]

## 出版緣起

《資治通鑑》是第一部官修的[編年體](../Page/編年體.md "wikilink")[通史](../Page/通史.md "wikilink")，記載前403年至959年，共一千三百六十三年的[中國歷史](../Page/中國歷史.md "wikilink")。由[十一世紀](../Page/十一世紀.md "wikilink")[北宋](../Page/北宋.md "wikilink")[司馬光負責主持編書](../Page/司馬光.md "wikilink")，書中內容以歷代君臣事蹟、各朝代的興衰為主，以期給帝王鑑戒，是一部足以瞭解中國[政治運作](../Page/政治.md "wikilink")、權力遊戲的歷史鉅著，是古代帝王鑑於往事、資於治道的必讀史書。

柏楊認為《資治通鑑》問世九百年之久，古老的[文言文對現代人而言](../Page/文言文.md "wikilink")，已顯得過度生澀艱深，假使再沒有現代語文本問世，將有塵封的厄運。於是，1983年，他開始翻譯《資治通鑑》。當時的遠流出版總編輯[詹宏志將書名定為](../Page/詹宏志.md "wikilink")《柏楊版資治通鑑》，以“整體規劃，分冊出版”的[雜誌形式發行](../Page/雜誌.md "wikilink")。同年，《柏楊版資治通鑑》獲選為[台灣最具影響力的一部書](../Page/台灣.md "wikilink")。發行初期定目標為36冊譯完，後改為72冊。

1993年3月，柏楊日紀念特輯《歷史走廊──十年柏楊（1983\~1993）》出版。3月7日，遠流出版公司定本日為「柏楊日」，為柏楊慶生，並慶祝《柏楊版資治通鑑》全部完成（平裝72冊，精裝36冊）。

寫完《柏楊版資治通鑑》的柏楊，形容這十年來的生活像在“[勞改營](../Page/勞改營.md "wikilink")”中度過一樣。有一次執行編輯前來拿稿，柏楊與他聊著聊著，居然當著客人的面睡著了，其疲憊可知。《柏楊版資治通鑑》第七十二冊有一篇[跋](../Page/跋.md "wikilink")，柏楊自述其中的辛勞，「……平均下來，我每個月至少都要閱讀四萬字左右的文言文（包括[標點和](../Page/標點.md "wikilink")[註解](../Page/註解.md "wikilink")）原文，寫出七萬五千字左右的初稿，和繳出十五萬字左右的[校稿](../Page/校稿.md "wikilink")，以及所必需的[地圖](../Page/地圖.md "wikilink")、附錄，和《通鑑廣場》。十年如一日，沒有[星期天](../Page/星期天.md "wikilink")，沒有例[假日](../Page/假日.md "wikilink")；沒有陰，沒有晴……」。

## 《柏楊版資治通鑑》全書名

| 集數 | 書名                                                               | 出版日期       | 頁數   | 備註                                                                                                                                                        |
| -- | ---------------------------------------------------------------- | ---------- | ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1  | 《[戰國時代](../Page/戰國時代.md "wikilink")》                             | 1983年9月1日  | 352頁 | **記載年份：**前403年－前291年。                                                                                                                                     |
| 2  | 《[吞併六國](../Page/秦滅六國之戰.md "wikilink")》                           | 1983年10月1日 | 304頁 | **記載年份：**前290年－前211年。                                                                                                                                     |
| 3  | 《[楚漢相爭](../Page/楚汉战争.md "wikilink")》                             | 1983年11月1日 | 240頁 | **記載年份：**前210年－前201年。                                                                                                                                     |
| 4  | 《[匈奴崛起](../Page/匈奴.md "wikilink")》                               | 1983年12月1日 | 288頁 | **記載年份：**前200年－前161年。                                                                                                                                     |
| 5  | 《[黃老之治](../Page/文景之治.md "wikilink")》                             | 1984年1月1日  | 240頁 | **記載年份：**前160年－前121年。                                                                                                                                     |
| 6  | 《[開疆拓土](../Page/汉武帝#對外關係.md "wikilink")》                         | 1984年2月1日  | 272頁 | **記載年份：**前120年－前91年。                                                                                                                                      |
| 7  | 《宮廷鬪爭》                                                           | 1984年3月1日  | 288頁 | **記載年份：**前90年－前61年。                                                                                                                                       |
| 8  | 《[萬里誅殺](../Page/陳湯.md "wikilink")》                               | 1984年4月1日  | 320頁 | **記載年份：**前60年－前21年。                                                                                                                                       |
| 9  | 《昏君輩出》                                                           | 1984年5月1日  | 304頁 | **記載年份：**前20年－前1年。                                                                                                                                        |
| 10 | 《[王莽](../Page/王莽.md "wikilink")[篡奪](../Page/禪讓制.md "wikilink")》  | 1984年6月1日  | 304頁 | **記載年份：**1年－23年。                                                                                                                                          |
| 11 | 《[全國混戰](../Page/劉秀.md "wikilink")》                               | 1984年7月1日  | 256頁 | **記載年份：**24年－34年。                                                                                                                                         |
| 12 | 《[馬援之死](../Page/馬援.md "wikilink")》                               | 1984年8月1日  | 320頁 | **記載年份：**35年－79年。                                                                                                                                         |
| 13 | 《[燕然勒石](../Page/燕然勒石.md "wikilink")》                             | 1984年10月1日 | 320頁 | **記載年份：**80年－119年。                                                                                                                                        |
| 14 | 《[跋扈將軍](../Page/梁冀.md "wikilink")》                               | 1984年11月1日 | 320頁 | **記載年份：**120年－159年。                                                                                                                                       |
| 15 | 《[黃巾民變](../Page/黃巾之亂.md "wikilink")》                             | 1984年12月1日 | 320頁 | **記載年份：**160年－189年。                                                                                                                                       |
| 16 | 《[東漢瓦解](../Page/漢獻帝.md "wikilink")》                              | 1985年1月1日  | 304頁 | **記載年份：**190年－203年。                                                                                                                                       |
| 17 | 《[赤壁之戰](../Page/赤壁之戰.md "wikilink")》                             | 1985年2月1日  | 272頁 | **記載年份：**204年－220年。                                                                                                                                       |
| 18 | 《[三國鼎立](../Page/三國.md "wikilink")》                               | 1985年3月1日  | 272頁 | **記載年份：**221年－235年。                                                                                                                                       |
| 19 | 《[壽春三叛](../Page/壽春三叛.md "wikilink")》                             | 1985年4月1日  | 304頁 | **記載年份：**236年－259年。                                                                                                                                       |
| 20 | 《[司馬奪權](../Page/曹髦.md "wikilink")》                               | 1985年5月1日  | 304頁 | **記載年份：**260年－288年。                                                                                                                                       |
| 21 | 《[八王之亂](../Page/八王之亂.md "wikilink")》                             | 1985年6月1日  | 336頁 | **記載年份：**289年－309年。                                                                                                                                       |
| 22 | 《大分裂》                                                            | 1985年7月1日  | 304頁 | **記載年份：**310年－319年。                                                                                                                                       |
| 23 | 《[五胡亂華](../Page/五胡十六國.md "wikilink")》                            | 1985年8月1日  | 288頁 | **記載年份：**320年－336年。                                                                                                                                       |
| 24 | 《[石虎肆暴](../Page/石虎.md "wikilink")》                               | 1985年9月1日  | 288頁 | **記載年份：**337年－354年。                                                                                                                                       |
| 25 | 《[苻堅大帝](../Page/苻堅.md "wikilink")》                               | 1985年10月1日 | 288頁 | **記載年份：**355年－376年。                                                                                                                                       |
| 26 | 《[肥水之戰](../Page/肥水之戰.md "wikilink")》                             | 1985年11月1日 | 304頁 | **記載年份：**377年－393年。                                                                                                                                       |
| 27 | 《[參合殺俘](../Page/參合陂之戰.md "wikilink")》                            | 1985年12月1日 | 304頁 | **記載年份：**394年－402年。                                                                                                                                       |
| 28 | 《[王始帝國](../Page/王始.md "wikilink")》                               | 1986年1月1日  | 288頁 | **記載年份：**403年－414年。                                                                                                                                       |
| 29 | 《[統萬碑文](../Page/统万城.md "wikilink")》                              | 1986年2月1日  | 320頁 | **記載年份：**415年－428年。                                                                                                                                       |
| 30 | 《[自毀長城](../Page/檀道濟.md "wikilink")》                              | 1986年3月1日  | 320頁 | **記載年份：**429年－449年。                                                                                                                                       |
| 31 | 《[南北朝](../Page/南北朝.md "wikilink")》                               | 1986年4月1日  | 304頁 | **記載年份：**450年－465年。                                                                                                                                       |
| 32 | 《[劉彧詔書](../Page/劉彧.md "wikilink")》                               | 1986年6月1日  | 288頁 | **記載年份：**466年－479年。                                                                                                                                       |
| 33 | 《[全盤漢化](../Page/孝文漢化.md "wikilink")》                             | 1986年7月1日  | 288頁 | **記載年份：**480年－494年。                                                                                                                                       |
| 34 | 《[蕭鸞眼淚](../Page/蕭鸞.md "wikilink")》                               | 1986年9月1日  | 272頁 | **記載年份：**495年－502年。                                                                                                                                       |
| 35 | 《[洛陽暴動](../Page/洛陽.md "wikilink")》                               | 1986年11月1日 | 320頁 | **記載年份：**503年－522年。                                                                                                                                       |
| 36 | 《[河陰屠殺](../Page/河阴之变.md "wikilink")》                             | 1987年1月1日  | 288頁 | **記載年份：**523年－531年。                                                                                                                                       |
| 37 | 《遍地血腥》                                                           | 1987年4月1日  | 288頁 | **記載年份：**532年－547年。                                                                                                                                       |
| 38 | 《[餓死宮城](../Page/侯景之亂.md "wikilink")》                             | 1987年5月1日  | 352頁 | **記載年份：**548年－554年。                                                                                                                                       |
| 39 | 《[禽獸王朝](../Page/北齊.md "wikilink")》                               | 1987年6月1日  | 320頁 | **記載年份：**555年－562年。                                                                                                                                       |
| 40 | 《[黃龍湯](../Page/黃龍湯.md "wikilink")》                               | 1987年7月1日  | 304頁 | **記載年份：**563年－575年。                                                                                                                                       |
| 41 | 《[突厥](../Page/突厥.md "wikilink")[可汗](../Page/可汗.md "wikilink")》   | 1987年9月1日  | 288頁 | **記載年份：**576年－588年。                                                                                                                                       |
| 42 | 《[南北統一](../Page/隋朝#统一中国.md "wikilink")》                          | 1987年11月1日 | 336頁 | **記載年份：**589年－608年。                                                                                                                                       |
| 43 | 《[官逼民反](../Page/隋煬帝#暴政.md "wikilink")》                           | 1987年12月1日 | 240頁 | **記載年份：**609年－617年。                                                                                                                                       |
| 44 | 《[江都政變](../Page/宇文化及.md "wikilink")》                             | 1988年1月1日  | 320頁 | **記載年份：**618年－621年。                                                                                                                                       |
| 45 | 《[玄武門](../Page/玄武門之變.md "wikilink")》                             | 1988年4月1日  | 304頁 | **記載年份：**622年－629年。                                                                                                                                       |
| 46 | 《[貞觀之治](../Page/貞觀之治.md "wikilink")》                             | 1988年5月1日  | 304頁 | **記載年份：**630年－643年。                                                                                                                                       |
| 47 | 《[黃金時代](../Page/永徽之治.md "wikilink")》                             | 1988年7月1日  | 304頁 | **記載年份：**644年－664年。                                                                                                                                       |
| 48 | 《[武照奪權](../Page/武曌.md "wikilink")》                               | 1988年9月1日  | 272頁 | **記載年份：**665年－688年。                                                                                                                                       |
| 49 | 《恐怖世界》                                                           | 1988年11月1日 | 288頁 | **記載年份：**689年－705年。                                                                                                                                       |
| 50 | 《[惡妻](../Page/韋后.md "wikilink")[惡女](../Page/安乐公主.md "wikilink")》 | 1989年1月1日  | 272頁 | **記載年份：**706年－718年。                                                                                                                                       |
| 51 | 《[開元盛世](../Page/開元之治.md "wikilink")》                             | 1989年5月1日  | 288頁 | **記載年份：**719年－744年。                                                                                                                                       |
| 52 | 《[范陽兵變](../Page/安史之亂.md "wikilink")》                             | 1989年8月1日  | 272頁 | **記載年份：**745年－756年。                                                                                                                                       |
| 53 | 《[睢陽之圍](../Page/張巡#死守睢陽城.md "wikilink")》                         | 1989年9月1日  | 272頁 | **記載年份：**757年－763年。                                                                                                                                       |
| 54 | 《[皇后失蹤](../Page/睿真皇太后.md "wikilink")》                            | 1989年11月1日 | 272頁 | **記載年份：**764年－780年。                                                                                                                                       |
| 55 | 《[涇原兵變](../Page/涇原兵變.md "wikilink")》                             | 1990年2月1日  | 288頁 | **記載年份：**781年－784年。                                                                                                                                       |
| 56 | 《[豬皇帝](../Page/唐德宗.md "wikilink")》                               | 1990年4月1日  | 272頁 | **記載年份：**785年－799年。                                                                                                                                       |
| 57 | 《[元和中興](../Page/元和中興.md "wikilink")》                             | 1990年6月1日  | 272頁 | **記載年份：**800年－815年。                                                                                                                                       |
| 58 | 《[牛李黨爭](../Page/牛李黨爭.md "wikilink")》                             | 1990年8月1日  | 288頁 | **記載年份：**816年－826年。                                                                                                                                       |
| 59 | 《[甘露事變](../Page/甘露之變.md "wikilink")》                             | 1990年10月1日 | 304頁 | **記載年份：**827年－843年。                                                                                                                                       |
| 60 | 《[大中之治](../Page/大中之治.md "wikilink")》                             | 1991年1月1日  | 272頁 | **記載年份：**844年－866年。                                                                                                                                       |
| 61 | 《[黃巢民變](../Page/黃巢民變.md "wikilink")》                             | 1991年3月1日  | 272頁 | **記載年份：**867年－881年。                                                                                                                                       |
| 62 | 《狼虎谷》                                                            | 1991年6月1日  | 304頁 | **記載年份：**882年－888年。                                                                                                                                       |
| 63 | 《[軍閥混戰](../Page/藩鎮割據.md "wikilink")》                             | 1991年8月1日  | 288頁 | **記載年份：**889年－895年。                                                                                                                                       |
| 64 | 《大黑暗》                                                            | 1991年10月1日 | 272頁 | **記載年份：**896年－901年。                                                                                                                                       |
| 65 | 《[五代時代](../Page/五代十國.md "wikilink")》                             | 1991年12月1日 | 256頁 | **記載年份：**902年－907年。                                                                                                                                       |
| 66 | 《小分裂》                                                            | 1992年2月1日  | 256頁 | **記載年份：**908年－916年。                                                                                                                                       |
| 67 | 《千里-{}-白骨》                                                       | 1992年4月16日 | 272頁 | **記載年份：**917年－925年。                                                                                                                                       |
| 68 | 《[半截英雄](../Page/李存勗.md "wikilink")》                              | 1992年7月1日  | 272頁 | **記載年份：**926年－932年。                                                                                                                                       |
| 69 | 《[兒皇帝](../Page/石敬瑭.md "wikilink")》                               | 1992年11月1日 | 288頁 | **記載年份：**933年－941年。                                                                                                                                       |
| 70 | 《[橫挑強鄰](../Page/后晋#石重贵反辽亡国.md "wikilink")》                       | 1993年1月20日 | 272頁 | **記載年份：**942年－947年。                                                                                                                                       |
| 71 | 《[高平之戰](../Page/高平之戰.md "wikilink")》                             | 1993年2月1日  | 264頁 | **記載年份：**948年－954年。                                                                                                                                       |
| 72 | 《[分裂尾聲](../Page/柴榮.md "wikilink")》                               | 1993年3月1日  | 272頁 | **記載年份：**955年－960年。司馬光原著記載至959年，而960年一節，柏楊表示這譯自[清朝人](../Page/清朝.md "wikilink")[畢沅的](../Page/畢沅.md "wikilink")《[續資治通鑑](../Page/續資治通鑑.md "wikilink")》\[2\]。 |

## 相關作品

### 柏楊曰

《柏楊曰》全名為《柏楊曰－讀通鑑，論歷史》。1998年8月，柏楊將《柏楊版資治通鑑》中，仿效「臣光曰」（「司馬光曰」）史評形式而寫的讀史心得「柏楊曰」部份抽出，編成《柏楊曰》六冊，單獨發行。全套《柏楊版資治通鑑》中的「柏楊曰」部份一共有862則。\[3\]

| 集數 | 書名       | 出版日期      | 頁數   | 備註              |
| -- | -------- | --------- | ---- | --------------- |
| 1  | 《柏楊曰（一）》 | 1998年8月1日 | 304頁 | ISBN 9573235404 |
| 2  | 《柏楊曰（二）》 | 1998年8月1日 | 320頁 | ISBN 9573235412 |
| 3  | 《柏楊曰（三）》 | 1998年8月1日 | 320頁 | ISBN 9573235420 |
| 4  | 《柏楊曰（四）》 | 1998年8月1日 | 272頁 | ISBN 9573235439 |
| 5  | 《柏楊曰（五）》 | 1998年8月1日 | 272頁 | ISBN 9573235447 |
| 6  | 《柏楊曰（六）》 | 1998年8月1日 | 320頁 | ISBN 9573235455 |

### 柏楊版通鑑紀事本末

1999年，鑒於《[資治通鑑](../Page/資治通鑑.md "wikilink")》「編年體」的體例常常無法呈現事件全貌，柏楊再以事件為主題，1999年起翻譯[南宋人](../Page/南宋.md "wikilink")[袁樞的](../Page/袁樞.md "wikilink")《[通鑑紀事本末](../Page/通鑑紀事本末.md "wikilink")》，並主編《柏楊版通鑑紀事本末》共38冊（袁樞原著有42卷，大陆出版为十九本三十八册），於2001年全套出齊。

| 集數 | 書名                                                                                           | 出版日期       | 頁數   | 備註              |
| -- | -------------------------------------------------------------------------------------------- | ---------- | ---- | --------------- |
| 1  | 《[范睢漂亮復仇](../Page/范睢.md "wikilink")》                                                         | 1999年2月1日  | 272頁 | ISBN 9573236532 |
| 2  | 《汗血馬戰爭》                                                                                      | 1999年5月1日  | 288頁 | ISBN 9573236885 |
| 3  | 《[巫蠱恐怖](../Page/巫蠱之禍.md "wikilink")》                                                         | 1999年5月1日  | 288頁 | ISBN 9573236893 |
| 4  | 《床上巨星[趙合德](../Page/趙合德.md "wikilink")》                                                       | 1999年5月1日  | 256頁 | ISBN 9573236907 |
| 5  | 《逐鹿型大混戰》                                                                                     | 1999年5月1日  | 240頁 | ISBN 9573236931 |
| 6  | 《慘烈窩裡鬥》                                                                                      | 1999年5月1日  | 224頁 | ISBN 9573236974 |
| 7  | 《[第一次宦官時代](../Page/第一次宦官時代.md "wikilink")》                                                   | 1999年5月1日  | 224頁 | ISBN 9573236966 |
| 8  | 《[三國](../Page/三國.md "wikilink")[周郎](../Page/周瑜.md "wikilink")[赤壁](../Page/赤壁.md "wikilink")》 | 1999年9月1日  | 256頁 | ISBN 9573237660 |
| 9  | 《[諸葛亮北伐挫敗](../Page/諸葛亮.md "wikilink")》                                                       | 1999年9月1日  | 304頁 | ISBN 9573237679 |
| 10 | 《十三王之亂》                                                                                      | 1999年9月1日  | 240頁 | ISBN 9573237687 |
| 11 | 《[華亂五胡](../Page/五胡亂華.md "wikilink")》                                                         | 1999年9月1日  | 256頁 | ISBN 9573237768 |
| 12 | 《[祖逖擊楫渡江](../Page/祖逖.md "wikilink")》                                                         | 1999年9月1日  | 240頁 | ISBN 9573237776 |
| 13 | 《星墜五將山》                                                                                      | 1999年9月1日  | 224頁 | ISBN 9573237784 |
| 14 | 《[桓玄篡位鬧劇](../Page/桓玄.md "wikilink")》                                                         | 1999年9月1日  | 256頁 | ISBN 9573238063 |
| 15 | 《[慕容超傳奇](../Page/慕容超.md "wikilink")》                                                         | 1999年9月1日  | 288頁 | ISBN 9573239264 |
| 16 | 《王師北定中原日》                                                                                    | 2000年6月1日  | 240頁 | ISBN 9573240009 |
| 17 | 《[鮮卑羨慕中華](../Page/鮮卑.md "wikilink")》                                                         | 2000年6月1日  | 240頁 | ISBN 9573240033 |
| 18 | 《南北亂成一團》                                                                                     | 2000年6月1日  | 272頁 | ISBN 9573240041 |
| 19 | 《[最美麗的蠢女人](../Page/靈太后.md "wikilink")》                                                       | 2000年6月1日  | 288頁 | ISBN 9573240238 |
| 20 | 《最嚴重一次叛變》                                                                                    | 2000年6月1日  | 256頁 | ISBN 9573240262 |
| 21 | 《[人渣家族](../Page/北齊.md "wikilink")》                                                           | 2000年6月1日  | 288頁 | ISBN 9573240254 |
| 22 | 《[驢老爺](../Page/隋煬帝.md "wikilink")、你贏了！》                                                      | 2000年9月16日 | 304頁 | ISBN 9573241323 |
| 23 | 《改朝換代大混戰》                                                                                    | 2000年9月16日 | 272頁 | ISBN 957324148X |
| 24 | 《[唐王朝一再奪嫡](../Page/唐朝.md "wikilink")》                                                        | 2000年9月16日 | 232頁 | ISBN 9573241560 |
| 25 | 《[貞觀對](../Page/貞觀_\(唐朝\).md "wikilink")》                                                     | 2000年9月16日 | 256頁 | ISBN 9573241625 |
| 26 | 《恐怖帝國》                                                                                       | 2000年9月16日 | 288頁 | ISBN 9573241757 |
| 27 | 《[安史之亂](../Page/安史之亂.md "wikilink")》                                                         | 2001年1月16日 | 240頁 | ISBN 9573242729 |
| 28 | 《第二次宦官時代》                                                                                    | 2001年1月16日 | 264頁 | ISBN 9573242737 |
| 29 | 《大黑暗來臨》                                                                                      | 2001年1月16日 | 304頁 | ISBN 9573242931 |
| 30 | 《兵變．兵變．再兵變》                                                                                  | 2001年1月16日 | 272頁 | ISBN 9573242974 |
| 31 | 《[牛李兩黨殊死鬥](../Page/牛李党争.md "wikilink")》                                                      | 2001年1月16日 | 288頁 | ISBN 9573242982 |
| 32 | 《[黃巢終結狼虎谷](../Page/黃巢.md "wikilink")》                                                        | 2001年1月16日 | 256頁 | ISBN 9573242958 |
| 33 | 《獨柳下，天才之辯》                                                                                   | 2001年3月1日  | 264頁 | ISBN 9573243059 |
| 34 | 《大屠殺與小分裂》                                                                                    | 2001年3月1日  | 224頁 | ISBN 9573243067 |
| 35 | 《狗崽長大咬死人》                                                                                    | 2001年3月1日  | 272頁 | ISBN 9573243075 |
| 36 | 《英雄與流氓》                                                                                      | 2001年3月1日  | 296頁 | ISBN 9573243113 |
| 37 | 《致命的橫挑強鄰》                                                                                    | 2001年3月1日  | 288頁 | ISBN 9573243121 |
| 38 | 《[陳橋兵變](../Page/陳橋兵變.md "wikilink")》                                                         | 2001年3月1日  | 256頁 | ISBN 957324313X |

## 優劣

### 特色

  - 地名今註：

<!-- end list -->

  -
    柏楊在古地名之後夾註今地名（在當時仍[戒嚴的時代](../Page/戒嚴.md "wikilink")，柏楊大膽使用[中華人民共和國制定之現時地名](../Page/中華人民共和國.md "wikilink")，而非法統上的[中華民國地名](../Page/中華民國.md "wikilink")），以提高讀者的認知度；並親手增繪地圖，力倡「史地不分家」。

<!-- end list -->

  - 官名今譯：

<!-- end list -->

  -
    柏楊使用現代人能夠立刻瞭解的現代中華民國官職名稱，夾註原稱，使能確知其權力地位。

<!-- end list -->

  - 西元紀元：

<!-- end list -->

  -
    在台灣史學界堅持使用帝王年號的傳統仍根深蒂固下，柏楊大膽使用西元紀年，以使讀者能明確時間距離；西元為主之外，輔以各國君主紀元，讓讀者能對當時歷史時空有更深入的理解。

<!-- end list -->

  - 續全書至後周末年：

<!-- end list -->

  -
    司馬光因為手上史料不足或政治忌諱原因，沒有把[後周恭帝即位至](../Page/後周恭帝.md "wikilink")[宋太祖兵變奪位的歷史](../Page/宋太祖.md "wikilink")（[陳橋兵變](../Page/陳橋兵變.md "wikilink")）寫入通鑑內，柏楊把[續資治通鑑的部份引入補完](../Page/續資治通鑑.md "wikilink")。

<!-- end list -->

  - 證補史事闕漏：

<!-- end list -->

  -
    「通鑑」書成之後，數百年間，仍有不少史事陸續被發現，柏楊於譯本內獨力補錄，以存信史。

<!-- end list -->

  - 《柏楊曰》：柏楊以現代中國人的觀點，析論歷史成敗因果。評論較符合現代人品味，推崇民主思想，同情失敗者與抨擊成王敗寇的[價值觀](../Page/價值觀.md "wikilink")。且對司馬光一些無緣無故的道德說教作出提醒。
  - 除了通鑑司馬光原作內已有的史論（如[司馬遷](../Page/司馬遷.md "wikilink")、[習鑿齒](../Page/習鑿齒.md "wikilink")、[孫盛](../Page/孫盛.md "wikilink")、[揚雄](../Page/揚雄.md "wikilink")、[裴子野等](../Page/裴子野.md "wikilink")）之外，有時並補充其他各家的觀點，從而與《柏楊曰》一起點綴通鑑全文。
  - 嘗試從實戰角度考察並描述戰爭，並對古代的一些戰爭描述表示質疑。

### 爭議

  - 《柏楊曰》書中多使用「某某先生」的敬稱，然而敬稱有時出現，有時柏楊寫作時又不帶敬稱。這種情況曾被讀者批評，但在書中依然沒有改善。
  - 「官名今譯」所指出的古今官位關係並不準確。且有時書中只書**某某郡郡長**而不附書古代官名（可能因出現太多而省略），反而混淆讀者對各朝官位的認識（綜觀各朝均不設所謂**郡長**，**郡長**只為官職的職能今譯）。
  - 《柏楊曰》有部份的觀點過於偏激，此亦為柏楊作品的特色之一。
  - 初版時，校稿錯誤百出，也是其特色。

## 反響

对于这本名作，当然引来了无数同行人士的口水，批评家喁喁而至。

《柏楊版資治通鑑》頗受[李敖](../Page/李敖.md "wikilink")、[何懷碩以及](../Page/何懷碩.md "wikilink")[香港學者](../Page/香港.md "wikilink")[孫國棟](../Page/孫國棟.md "wikilink")、[李明德](../Page/李明德.md "wikilink")、菲律賓華人作家[江樺等人批評](../Page/江樺.md "wikilink")，例如：把“水衡都尉”（掌管[上林苑](../Page/上林苑.md "wikilink")）錯譯成“水利部長”，“[大司農](../Page/大司農.md "wikilink")”（掌管國家財政收入）錯譯為“農林部長”；柏楊又將“罷”與“免”混用（在《通鑑》中，「免」和「罷」不同：有過失的用「免」，沒有過失而離職的用「罷」），將二者皆譯為「免職」等。延续了《[中国人史纲](../Page/中国人史纲.md "wikilink")》当中姓氏不分、姓氏连用、姓名连读的自创方法，完全违背先秦时代男性用氏、女性用姓的习惯。

李明德表示：「以原典第一冊開宗明義『臣光曰』的一千字，與《柏楊版》對照，柏楊完全沒有弄清楚司馬溫公到底『曰』了什麼。上面列舉出的二百字，是錯到匪夷所思的地步，足以判定柏楊走在地雷上，只有炸死一途。」

香港一些文化界人士提議組織「修柏會」，「修柏」即是「修改《柏楊版資治通鑑》」的意思。

江樺從1999年6月起在菲律賓《世界日報》連載《評〈柏楊版資治通鑑〉》，指出了許多學者沒注意到的錯譯。

孫國棟晚年與柏楊論戰，反對柏楊的「[醜陋的中國人](../Page/醜陋的中國人.md "wikilink")」之說，也批評《柏楊版資治通鑑》的膚淺\[4\]。

儘管《柏楊版資治通鑑》有許多不善之處，但也有帶起讀書及[古籍今譯風氣之功](../Page/古籍今譯.md "wikilink")。歷史學家[唐德剛表示](../Page/唐德剛.md "wikilink")，如果有人要他列學生必讀書目，由於《[資治通鑑](../Page/資治通鑑.md "wikilink")》是他認為最重要的經典：「看不懂原文，那就讀柏楊版白話通鑑。」唐德剛認為，柏楊堪稱是穿過歷史三峽驚濤駭浪的好漢，「以嚴謹的學術標準看來，柏楊不一定被公認為舉足輕重的歷史學者。但他從喜好為文打抱不平，到親身受冤獄壓迫後，對歷史的反芻，卻使他的歷史書寫與著作，有著另一層重要的意義。」\[5\]

## 參考資料

  - 註釋

<!-- end list -->

  - 文獻

<!-- end list -->

  - [孫國棟](../Page/孫國棟.md "wikilink")，《一本超前绝后的译作————評〈柏楊版資治通鑑〉》（《[明報月刊](../Page/明報月刊.md "wikilink")》，1987年4月）
  - 孫國棟，《評柏楊》，[明報出版社](../Page/明報出版社.md "wikilink")，1989年。
  - [李明德](../Page/李明德.md "wikilink")，《行走在地雷上，英雄的作品。——評〈柏楊資治通鑑〉》，《明報月刊》1985年12月號。
  - [沈超群](../Page/沈超群.md "wikilink")，《柏楊與柏楊傳─從新聞評議到白色恐怖的探討》，台北：東吳大學歷史研究所碩士論文，2006年。

[Category:编年体](../Category/编年体.md "wikilink")
[Category:資治通鑑](../Category/資治通鑑.md "wikilink")

1.  此外在中国大陆还有北岳文艺出版社版、湖南人民出版社版，但这些版本连同万卷出版公司旧版都无法在市面上见到。
2.  [資治通鑑72分裂尾聲內容大要](http://www.ylib.com/search/ShowBook.asp?BookNo=B1072)
3.  [1](http://nrch.cca.gov.tw/ccahome/website/site4/BY_Collect/b9000e0/e012/cca220003-li-wpkbbyb9000e012toc-0000-u.html)
4.
5.  [「臣光曰」「柏楊曰」，針鋒相對，各有千秋](http://cw.nmtl.gov.tw/index.php?option=com_whsearch&view=detail&id=13556&Itemid=4)，1993-03-07，聯合報，唐德剛