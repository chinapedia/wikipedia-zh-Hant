**柏科**（[学名](../Page/学名.md "wikilink")：）[植物广泛分布在全世界](../Page/植物.md "wikilink")，包括27-30个[属](../Page/属.md "wikilink")，（其中有17个属只有单一[种](../Page/种.md "wikilink")），大约有130-140[种](../Page/种.md "wikilink")。

柏科植物是分布最广的针叶植物，几乎遍布除[南极洲以外的世界各地](../Page/南极洲.md "wikilink")，从[北纬](../Page/北纬.md "wikilink")70°
[挪威](../Page/挪威.md "wikilink")[北极区域的](../Page/北极.md "wikilink")[刺柏到](../Page/刺柏.md "wikilink")[南纬](../Page/南纬.md "wikilink")55°[智利的](../Page/智利.md "wikilink")[智利柏](../Page/智利柏.md "wikilink")，生活在[西藏](../Page/西藏.md "wikilink")[海拔](../Page/海拔.md "wikilink")5200[米地区的](../Page/米.md "wikilink")[滇藏方枝柏是已知能在海拔最高区域存活的](../Page/滇藏方枝柏.md "wikilink")[木本植物](../Page/木本植物.md "wikilink")。

## 形态

绝大部分是雌雄同株的，也有很少种是雌雄异株的，一般都是[乔木](../Page/乔木.md "wikilink")，最高的可达130[米](../Page/米.md "wikilink")，也有少数种是[灌木](../Page/灌木.md "wikilink")。树皮都是发[红](../Page/红色.md "wikilink")[棕色](../Page/棕色.md "wikilink")，绳状结构，脱落时都是竖条形，只有少数种例外。

绝大部分种为常绿植物，叶子可成活2－10年，但三个属（落羽杉属、水杉属和水松属）是[落叶树](../Page/落叶树.md "wikilink")。

柏科植物的[叶呈螺旋状排列](../Page/叶.md "wikilink")，芽叶一般是尖针状，成熟后呈鳞片状，但也有的种终生为针状。球花的[雄蕊及具胚珠的鳞片亦交互对生](../Page/雄蕊.md "wikilink")，很少三个轮生；雄球花的雄蕊有2－6药囊，雌球花全部或部分种鳞具有一至多数胚珠、苞鳞和种鳞结合。

卵形或圆球形球果当年或第二年成熟；果实一般为[坚果](../Page/坚果.md "wikilink")，也有[浆果](../Page/浆果.md "wikilink")（如[刺柏属](../Page/刺柏属.md "wikilink")）；鳞片木质扁平或盾形，发育的鳞片具有一至多个[种子](../Page/种子.md "wikilink")，种子具翅或无翅。

## 分类

最新的分类法将以前单独分为“[杉科](../Page/杉科.md "wikilink")”的植物划入柏科，只是其中的[金松属被单独分为](../Page/金松属.md "wikilink")[金松科](../Page/金松科.md "wikilink")。

柏科被划分为7个[亚科](../Page/亚科.md "wikilink")：

  - [杉亚科](../Page/杉木属.md "wikilink")（Cunninghamioideae）：[杉属](../Page/杉属.md "wikilink")；
  - [密叶杉亚科](../Page/密叶杉属.md "wikilink")（Athrotaxidoideae）：[密叶杉属](../Page/密叶杉属.md "wikilink")；
  - [台湾杉亚科](../Page/台灣杉屬.md "wikilink")（Taiwanioideae）：[台湾杉属](../Page/台湾杉属.md "wikilink")；
  - [紅杉亚科](../Page/紅杉亚科.md "wikilink")（Sequoioideae）：[红杉属](../Page/红杉属.md "wikilink")，[巨杉属](../Page/巨杉.md "wikilink")，[水杉属](../Page/水杉属.md "wikilink")；
  - [落羽杉亚科](../Page/落羽杉亚科.md "wikilink")（Taxodioideae）：[落羽杉属](../Page/落羽杉属.md "wikilink")，[水松属](../Page/水松属.md "wikilink")，[柳杉属](../Page/柳杉属.md "wikilink")；
  - [柏松亚科](../Page/柏松亚科.md "wikilink")（Callitroideae）：[柏松属](../Page/柏松属.md "wikilink")，[澳洲柏松属](../Page/澳洲柏松属.md "wikilink")，[新喀里多尼亚柏松属](../Page/新喀里多尼亚柏松.md "wikilink")，[非洲柏松属](../Page/非洲柏松属.md "wikilink")，[塔斯马尼亚柏松属](../Page/塔斯马尼亚柏松属.md "wikilink")，[智利肖柏属](../Page/智利肖柏属.md "wikilink")，[智利雪松属](../Page/智利雪松属.md "wikilink")，[肖楠属](../Page/肖楠属.md "wikilink")，[智利柏属](../Page/智利柏属.md "wikilink")，[巴布亚柏松属](../Page/巴布亚柏松属.md "wikilink")；
  - [柏亚科](../Page/柏亚科.md "wikilink")（Cupressoideae）：[崖柏属](../Page/崖柏属.md "wikilink")，[罗汉柏属](../Page/罗汉柏属.md "wikilink")，[扁柏属](../Page/扁柏属.md "wikilink")，[福建柏属](../Page/福建柏属.md "wikilink")，[翠柏属](../Page/翠柏属.md "wikilink")，[四鳞柏属](../Page/四鳞柏属.md "wikilink")，[小侧柏属](../Page/小侧柏属.md "wikilink")，[侧柏属](../Page/侧柏属.md "wikilink")，[澳洲柏属](../Page/澳洲柏属.md "wikilink")，[柏木属](../Page/柏木属.md "wikilink")，[刺柏属](../Page/刺柏属.md "wikilink")。

## 世界之最

[Cupressaceae.PNG](https://zh.wikipedia.org/wiki/File:Cupressaceae.PNG "fig:Cupressaceae.PNG")
柏科植物包括世界上最大的、最高的、木材最硬的和第二长寿的物[种](../Page/种.md "wikilink")：

  - 最大的 – 巨杉属的[世界爷](../Page/世界爷.md "wikilink")；
  - 最高的 – 红杉属的[红杉](../Page/加州红木.md "wikilink")；
  - 最粗的 - 落羽杉属的[墨西哥落羽杉](../Page/墨西哥落羽杉.md "wikilink")；
  - 寿命第二长的 -
    智利肖柏属的[智利柏](../Page/智利柏.md "wikilink")（仅次于[刺果松](../Page/刺果松.md "wikilink")）。

## 用途

[PlatycladusOrientalis.jpg](https://zh.wikipedia.org/wiki/File:PlatycladusOrientalis.jpg "fig:PlatycladusOrientalis.jpg")
柏科植物很多种类是主要的木材原料，其中重要的是柳杉属、杉属、落羽杉属、翠柏属、红杉属、柏木属、扁柏属和崖柏属的品种。此外在园艺上也是重要的植物种群，尤其是刺柏属植物，被培植出[蓝色](../Page/蓝色.md "wikilink")、[灰色](../Page/灰色.md "wikilink")、[黄色的各种品系](../Page/黄色.md "wikilink")；崖柏属的植物也被培植出多种矮化品种；[水杉](../Page/水杉.md "wikilink")、[美国扁柏和](../Page/美国扁柏.md "wikilink")[側柏等也是被广泛种植的园艺品种](../Page/側柏.md "wikilink")，还有些品种被种植作为[圣诞树](../Page/圣诞树.md "wikilink")。
[日本柳杉](../Page/日本柳杉.md "wikilink") (*Cryptomeria
japonica*)是[日本的国树](../Page/日本.md "wikilink")；[墨西哥落羽杉](../Page/墨西哥落羽杉.md "wikilink")
(*Taxodium
mucronatum*)是[墨西哥的国树](../Page/墨西哥.md "wikilink")；[巨杉和](../Page/巨杉.md "wikilink")[红杉是](../Page/红杉.md "wikilink")[加利福尼亚州的并列的州树](../Page/加利福尼亚州.md "wikilink")；[落羽杉](../Page/落羽杉.md "wikilink")（*Taxodium
distichum*）是[路易斯安那州的州树](../Page/路易斯安那州.md "wikilink")；[北美白松](../Page/北美白松.md "wikilink")（*Juniperus
virginiana*）木材可以防虫蛀，用来做家具；[刺柏浆果](../Page/刺柏.md "wikilink")（杜松子）是造[金酒的原料](../Page/金酒.md "wikilink")。

[北美翠柏](../Page/北美翠柏.md "wikilink")（*Calocedrus
decurrens*）的木材是制造[铅笔的主要用材](../Page/铅笔.md "wikilink")；福建柏的[根可以提炼芳香油](../Page/根.md "wikilink")，用于药和化妆品生产。

柏科植物的[花粉容易让部分人产生过敏致病](../Page/花粉.md "wikilink")，日本柳杉曾经造成问题。

刺柏是[胶锈菌属](../Page/胶锈菌属.md "wikilink")（*Gymnosporangium*）[真菌的宿主](../Page/真菌.md "wikilink")，这种菌可以造成[苹果](../Page/苹果.md "wikilink")、[梨等果树发生锈病](../Page/梨.md "wikilink")。

## 参考文献

<references/>

  - Farjon, A. 1998. *World Checklist and Bibliography of Conifers*.
    Royal Botanic Gardens, Kew. 300 p. ISBN 1-900347-54-7.
  - Gadek, P. A., Alpers, D. L., Heslewood, M. M., & Quinn, C. J. 2000.
    Relationships within Cupressaceae sensu lato: a combined
    morphological and molecular approach. *American Journal of Botany*
    87: 1044–1057. [Available
    online](http://www.amjbot.org/cgi/reprint/87/7/1044.pdf)。
  - Farjon, A., Hiep, N. T., Harder, D. K., Loc, P. K., & Averyanov, L.
    2002. A new genus and species in the Cupressaceae (Coniferales) from
    northern Vietnam, Xanthocyparis vietnamensis. *Novon* 12: 179–189.
  - Little, D. P., Schwarzbach, A. E., Adams, R. P. & Hsieh, Chang-Fu.
    2004. The circumscription and phylogenetic relationships of
    *Callitropsis* and the newly described genus *Xanthocyparis*
    (Cupressaceae). *American Journal of Botany* 91 (11): 1872–1881.
    [Available
    online](http://watson.ecs.baylor.edu/Juniperus/RApaperspdf/179-2004AJB91-1872.pdf)。

## 外部链接

  - [坚果图片](http://www.pinetum.org/cones/mpfcones.htm)
  - [柏科植物介绍](http://www.conifers.org/cu/index.htm)
  - [北美柏科植物](https://web.archive.org/web/20060901135826/http://flora.huh.harvard.edu:8080/flora/browse.do?flora_id=1&taxon_id=10237)
  - [德克萨斯州柏科植物图片](http://www.csdl.tamu.edu/FLORA/imaxxcup.htm)

[Category:植物保護](../Category/植物保護.md "wikilink")
[\*](../Category/柏科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")