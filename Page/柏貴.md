**柏貴**（），[額哲忒氏](../Page/額哲忒氏.md "wikilink")，[清朝官員](../Page/清朝.md "wikilink")。

[道光二年](../Page/道光.md "wikilink")（1822年），任[實錄館](../Page/實錄館.md "wikilink")[謄錄官](../Page/謄錄官.md "wikilink")。十年，任[甘肅省](../Page/甘肅省.md "wikilink")[隴西縣](../Page/隴西縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")。十一年，[兵部](../Page/兵部.md "wikilink")[筆帖式上](../Page/筆帖式.md "wikilink")[行走](../Page/行走.md "wikilink")。十五年，任[廣東省](../Page/廣東省_\(清\).md "wikilink")[普寧縣知縣](../Page/普宁县_\(明朝\).md "wikilink")。十六年調任[龍門縣知縣](../Page/龍門縣.md "wikilink")。十九年，任[東莞縣知縣](../Page/東莞縣.md "wikilink")。二十一年，任[南雄直隸州](../Page/南雄州.md "wikilink")[知州](../Page/知州.md "wikilink")。二十六年，任[四川省](../Page/四川省.md "wikilink")[敘州府](../Page/叙州府.md "wikilink")[知府](../Page/知府.md "wikilink")。二十七年，任廣東[督糧道](../Page/督糧道.md "wikilink")。二十九年，任廣東[按察使](../Page/按察使.md "wikilink")，同年改廣東[布政使](../Page/承宣布政使司布政使.md "wikilink")。[咸豐二年](../Page/咸丰_\(年号\).md "wikilink")（1852年），任[河南巡撫](../Page/河南巡撫.md "wikilink")，同年署理[廣東巡撫](../Page/廣東巡撫.md "wikilink")。七年，署理[欽差大臣](../Page/欽差大臣.md "wikilink")，九年，接替[王慶雲署理](../Page/王慶雲_\(清朝\).md "wikilink")[兩廣總督](../Page/兩廣總督.md "wikilink")。

1858年1月，自[英法聯軍](../Page/第二次鸦片战争.md "wikilink")[入侵廣州後](../Page/廣州城戰役.md "wikilink")，為了恢復城內秩序，逮捕两广总督[叶名琛](../Page/葉名琛.md "wikilink")，组建“联军委员会”。羈留於觀音山的柏貴不久即被英法總局復職並一同管理廣州全城。柏貴也成為了中國第一位被西方殖民者操控的傀儡。为维持傀儡地位，柏贵不断说服同僚放弃抗战主张，为侵略者开脱罪责。遂遭朝廷内外有识之士强烈反对，亦失去咸丰帝之信任，加以英法侵略者的抑制，忧郁成疾，1859年病死[广州](../Page/广州府.md "wikilink")\[1\]。

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[清史稿](../Page/清史稿.md "wikilink")》

{{-}}

[Category:第二次鴉片戰爭人物](../Category/第二次鴉片戰爭人物.md "wikilink")
[B柏](../Category/清朝两广總督.md "wikilink")
[Category:清朝河南巡撫](../Category/清朝河南巡撫.md "wikilink")
[Category:清朝廣東巡撫](../Category/清朝廣東巡撫.md "wikilink")
[Category:清朝廣東布政使](../Category/清朝廣東布政使.md "wikilink")
[Category:清朝廣東按察使](../Category/清朝廣東按察使.md "wikilink")
[Category:清朝東莞縣知縣](../Category/清朝東莞縣知縣.md "wikilink")

1.  茅海建：《苦命天子 咸豐皇帝奕詝》。聯經出版事業股份有限公司，2008年4月：頁179─180。ISBN
    978-957-08-3260-0