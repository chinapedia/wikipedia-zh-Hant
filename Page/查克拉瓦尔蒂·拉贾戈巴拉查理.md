**查克拉瓦尔蒂·拉贾戈巴拉查理**（[泰米尔文](../Page/泰米尔文.md "wikilink")：**சக்ரவர்தி
ராஜகோபாலாச்சாரி**，**Chakravarti
Rajagopalachari**，）律师、作家、政治家、[印度教降神师](../Page/印度教.md "wikilink")。他是[印度獨立後唯一的](../Page/印度.md "wikilink")[印度人總督](../Page/印度总督.md "wikilink")，后任[马达拉斯首席部长](../Page/马达拉斯.md "wikilink")（[泰米尔纳德邦](../Page/泰米尔纳德邦.md "wikilink")）。

拉贾戈巴拉查理后出任[尼赫鲁总理的内阁成员](../Page/尼赫鲁.md "wikilink")，任内政部长。1952年至1954年任马达拉斯（Madras）首席部长。1959年成為[自由獨立黨的創建人和領導者](../Page/自由獨立黨.md "wikilink")。离开政府后，他成为印度政府的最高荣誉奖——巴域·维那（Bharat
Ratna）的首位获奖者。

[Category:印度总督](../Category/印度总督.md "wikilink")
[Category:印度國大黨黨員](../Category/印度國大黨黨員.md "wikilink")
[Category:印度獨立運動人物](../Category/印度獨立運動人物.md "wikilink")
[Category:印度律師](../Category/印度律師.md "wikilink")
[Category:印度翻譯家](../Category/印度翻譯家.md "wikilink")
[Category:印度作家](../Category/印度作家.md "wikilink")
[Category:泰米爾語作家](../Category/泰米爾語作家.md "wikilink")
[Category:英語作家](../Category/英語作家.md "wikilink")
[Category:印度反戰人士](../Category/印度反戰人士.md "wikilink")
[Category:印度反共主義者](../Category/印度反共主義者.md "wikilink")
[Category:印度素食主義者](../Category/印度素食主義者.md "wikilink")
[Category:印度印度教徒](../Category/印度印度教徒.md "wikilink")
[Category:印度泰米爾人](../Category/印度泰米爾人.md "wikilink")
[Category:泰米爾納德邦人](../Category/泰米爾納德邦人.md "wikilink")