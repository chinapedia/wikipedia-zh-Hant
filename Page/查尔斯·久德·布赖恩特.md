**查尔斯·久德·布赖恩特**（，），2003年10月14日宣誓就任[利比里亚全国过渡政府主席直至](../Page/利比里亚.md "wikilink")2006年1月16日。

布赖恩特在1984年创建[利比里亚行动党](../Page/利比里亚行动党.md "wikilink")，1992年任该党主席。1997年前总统[查尔斯·泰勒执政后](../Page/查尔斯·泰勒.md "wikilink")，对内实行独裁统治，利国内局势一直动荡不安。2003年8月11日，迫于内外压力，泰勒总统向副总统[摩西·布拉移交权力](../Page/摩西·布拉.md "wikilink")，流亡[尼日利亚](../Page/尼日利亚.md "wikilink")。布拉就任临时总统。8月18日，布拉政府与[利比里亚联合人民党](../Page/利比里亚联合人民党.md "wikilink")、利民运和各政党社团共同签署《阿克拉和平协定》，并于10月中旬组建全国过渡政府。商人出身的布赖恩特由于在政治上相对中立而被各个政治阵营所接受\[1\]，最终被推举为全国过渡政府主席，并于10月14日宣誓就职。

在[2005年利比里亚总统大选中](../Page/2005年利比里亚总统大选.md "wikilink")，[埃伦·约翰逊-瑟利夫胜出](../Page/埃伦·约翰逊-瑟利夫.md "wikilink")，并于2006年1月从布赖恩特手中接过大权，就职成为利比里亚第24任[總統](../Page/利比里亚总统列表.md "wikilink")，她也是首位非洲女总统\[2\]。

2014年4月16日，布赖恩特在利比里亚首都[蒙罗维亚的](../Page/蒙罗维亚.md "wikilink")因病去世。\[3\]\[4\]

## 挪用公款

2007年1月，布赖恩特由于涉嫌腐败而接受了警方的问讯\[5\]，并于2月27日因涉嫌挪用公款而被起诉。据称，他任职期间政府非法挪用了超过100万[美元的公款](../Page/美元.md "wikilink")\[6\]。3月12日有关方面发出对布赖恩特的[逮捕令](../Page/逮捕令.md "wikilink")\[7\]
。次日布赖恩特被逮捕，但随后很快被[保释](../Page/保释.md "wikilink")\[8\]。在法庭上，被告辩护人坚持布赖恩特作为国家首脑，其行为在宪法上享有[豁免权](../Page/豁免权.md "wikilink")；控方则表示，《阿克拉和平协定》的签订使得布赖恩特于2003年8月在宪法基础以外被任命，因此他不应享有豁免权\[9\]。2007年8月24日最高法院裁决布赖恩特不享有豁免权，决定正式对其涉嫌挪娜公款展开审讯\[10\]。

2007年12月7日\[11\]\[12\]，由于布赖恩特坚持他享有豁免权\[13\]而在当周早些时候拒绝到庭\[14\]\[15\]
，从而触犯了保释法令并因此被捕。当他被带往[蒙罗维亚的看守所时](../Page/蒙罗维亚.md "wikilink")，他说：“这是利比里亚非常、非常黑暗的一天。这就是我们恢复国家和平与民主后所得到的回报。\[16\]”12月10日，他最终同意签署一份保证书声明未来将按时到庭，并获得释放\[17\]。

## 参考文献

## 外部連結

  - [利比里亞新領導人宣誓就職](http://news.bbc.co.uk/chinese/trad/hi/newsid_3180000/newsid_3189400/3189456.stm)

{{-}}

[Category:利比里亚总统](../Category/利比里亚总统.md "wikilink")

1.

2.

3.

4.

5.  Jonathan Paye-Layleh, ["Liberia ex-leader probed on
    graft"](http://news.bbc.co.uk/2/hi/africa/6278909.stm), *BBC News
    Online*, 2007年1月19日 .

6.  ["Liberia's ex-leader 'stole
    $1m'"](http://news.bbc.co.uk/2/hi/africa/6403923.stm), *BBC News
    Online*, 2007年2月28日.

7.  ["Liberia to arrest ex-interim
    president"](http://www.int.iol.co.za/index.php?sf=86&set_id=1&click_id=86&art_id=nw20070313123746707C920485),
    [法新社](../Page/法新社.md "wikilink") (*Independent Online*), 2007年3月13日
    .

8.  Naomi Schwarz, ["Former Liberian Transitional President Arrested for
    Corruption"](http://www.voanews.com/english/archive/2007-03/2007-03-13-voa51.cfm),
    *[美国之音新闻](../Page/美国之音.md "wikilink")*, 2007年3月17日 .

9.  ["Liberia: le parquet général dénie toute immunité à l'ex-président
    Bryant"](http://www.jeuneafrique.com/fluxafp/fil_info.asp?reg_id=0&art_cle=36468),
    [法新社](../Page/法新社.md "wikilink") (*Jeuneafrique.com*), 2007年4月25日 .

10. ["Liberia: la Cour suprême donne son feu vert pour juger
    l'ex-président
    Bryant"](http://www.jeuneafrique.com/fluxafp/fil_info.asp?art_cle=38841),
    [法新社](../Page/法新社.md "wikilink") (*Jeuneafrique.com*), 2007年8月24日 .

11. Nico Colombant, ["Liberia's Former Leader Bryant Is Arrested in
    Corruption
    Probe"](http://www.voanews.com/english/2007-12-07-voa26.cfm), VOA
    News, 2007年12月7日 .

12. ["Ex-Liberia president
    arrested"](http://english.aljazeera.net/NR/exeres/71BC58B1-581A-461F-99FE-047EA455894B.htm),
    Al Jazeera, 2007年12月7日 .

13. ["Liberia's ex-president
    held"](http://www.news24.com/News24/Africa/News/0,,2-11-1447_2234877,00.html)
    , News24.com, 2007年12月7日 .

14.
15.
16.
17. ["Ex-Liberian president released from
    jail"](http://www.iht.com/articles/ap/2007/12/08/africa/AF-GEN-Liberia-Ex-President.php),
    美联社 (*International Herald Tribune*), 2007年12月8日 .