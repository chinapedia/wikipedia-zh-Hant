**查尔斯·桑德斯·皮尔士**（，；中文常译為皮尔斯，实际上读音应该是“purse”，“珀斯”）是[美国的](../Page/美国.md "wikilink")[通才](../Page/通才.md "wikilink")，实用主义学家。

## 生平

1839年生于[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[坎布里奇](../Page/坎布里奇.md "wikilink")，早年在[哈佛大学接受教育](../Page/哈佛大学.md "wikilink")，后任教于[霍普金斯大学](../Page/霍普金斯大学.md "wikilink")。

皮尔士在其一生中被很大程度上忽视了，直到[第二次世界大战对他的研究文献仍很缺乏](../Page/第二次世界大战.md "wikilink")。他的很多巨著仍未出版中譯本。他是[数学](../Page/数学.md "wikilink")、[研究方法论](../Page/研究.md "wikilink")、[科学哲学](../Page/科学哲学.md "wikilink")、[知识论和](../Page/知识论.md "wikilink")[形而上学领域中的改革者](../Page/形而上学.md "wikilink")，他自认为首先是[逻辑学家](../Page/逻辑学家.md "wikilink")。尽管他主要对形式逻辑做出重要贡献，他的「逻辑」所含盖的很多内容现在被称做了科学哲学和知识论。他发现并创建了作为[符號學分支的逻辑学](../Page/符號學.md "wikilink")，他发现逻辑运算可以用电子开关电路完成，因此预见了电子计算机。

他被[威廉·詹姆士認為是美國當代](../Page/威廉·詹姆士.md "wikilink")[實用主義的奠基人](../Page/實用主義.md "wikilink")。

## 參考資料

<references />

<div class="references-small">

  - Anellis, I.H. (1995) "Peirce Rustled, Russell Pierced: How Charles
    Peirce and Bertrand Russell Viewed Each Other's Work in Logic, and
    an Assessment of Russell's Accuracy and Role in the Historiography
    of Logic,"
    [Eprint](http://www.cspeirce.com/menu/library/aboutcsp/anellis/csp&br.htm)
    *Modern Logic 5*: 270–328.
  - [Aristotle](../Page/Aristotle.md "wikilink")，"The Categories,"
    Harold P. Cooke (trans.), pp. 1–109 in *Aristotle, Volume 1*, [Loeb
    Classical
    Library](../Page/Loeb_Classical_Library.md "wikilink")。London:
    [William
    Heinemann](../Page/Heinemann_\(book_publisher\).md "wikilink")，1938.
  - Aristotle, "On Interpretation," Harold P. Cooke (trans.), pp.
    111–179 in *Aristotle, Volume 1*, Loeb Classical Library. London:
    William Heinemann, 1938.
  - Aristotle, "[Prior
    Analytics](../Page/Prior_Analytics.md "wikilink")，" Hugh
    Tredennick (trans.), pp. 181–531 in *Aristotle, Volume 1*, Loeb
    Classical Library. London: William Heinemann, 1938.
  - [Boole, George](../Page/George_Boole.md "wikilink") (1854) *An
    Investigation of the Laws of Thought on Which are Founded the
    Mathematical Theories of Logic and Probabilities*,
    [Macmillan](../Page/Macmillan_Publishers.md "wikilink")。Reprinted
    1958 with corrections, New York: [Dover
    Publications](../Page/Dover_Publications.md "wikilink")。
  - [Stanford Encyclopedia of
    Philosophy](../Page/Stanford_Encyclopedia_of_Philosophy.md "wikilink")：
      - Atkin, Albert (2006) "[Peirce's Theory of
        Signs](http://plato.stanford.edu/entries/peirce-semiotics/)".
  - Brent, Joseph (1998), Charles Sanders Peirce: A Life. Revised and
    enlarged edition, Indiana University Press, Bloomington, IN.
      - Burch, Robert (2001) "[Charles Sanders
        Peirce,](http://plato.stanford.edu/archives/win2005/entries/peirce)".
        Revised, Winter 2005.
  - [Dewey, John](../Page/John_Dewey.md "wikilink") (1910) *How We
    Think*, Lexington MA: [D.C.
    Heath](../Page/D.C._Heath.md "wikilink")。Reprinted 1991, Buffalo
    NY: Prometheus Books.
  - [Haack, Susan](../Page/Susan_Haack.md "wikilink") (1998) *Manifesto
    of a Passionate Moderate*. Chicago IL: University of Chicago Press.
  - Houser, Nathan (1989) " [The Fortunes and Misfortunes of the Peirce
    Papers,](http://www.cspeirce.com/menu/library/aboutcsp/houser/fortunes.htm)"
    *Fourth Congress of the International Association for Semiotic
    Studies*, Perpignan, France, 1989. Published, pp. 1259–1268 in
    *Signs of Humanity*, vol. 3, Michel Balat and Janice
    Deledalle-Rhodes (eds.), Gérard Deledalle (gen. ed.), Mouton de
    Gruyter, Berlin, Germany, 1992.
  - [Liddell, Henry
    George](../Page/Henry_George_Liddell.md "wikilink")，and [Scott,
    Robert](../Page/Robert_Scott.md "wikilink") (1889) *[An Intermediate
    Greek-English
    Lexicon](http://www.perseus.tufts.edu/cgi-bin/ptext?doc=Perseus:text:1999.04.0058;query=toc)*,
    Oxford UK: [Oxford University
    Press](../Page/Oxford_University_Press.md "wikilink")。Reprinted
    1991.
  - [Mac Lane, Saunders](../Page/Saunders_Mac_Lane.md "wikilink") (1971)
    *Categories for the Working Mathematician*, New York:
    [Springer-Verlag](../Page/Springer_Science+Business_Media.md "wikilink")。Second
    edition, 1998.
  - Peirce, C.S. (1877) "[The Fixation of
    Belief,](http://www.peirce.org/writings/p107.html)" *Popular Science
    Monthly 12*: 1–15. Reprinted CP 5.358-387.
  - Peirce, C.S. (1878) "[How to Make Our Ideas
    Clear,](http://en.wikisource.org/wiki/How_to_Make_Our_Ideas_Clear)"
    *Popular Science Monthly 12*: 286–302. Reprinted CP 5.388-410.
  - Peirce, C.S. (1899) " [F.R.L. (First Rule of
    Logic),](http://www.princeton.edu/~batke/peirce/frl_99.htm)"
    unpaginated manuscript. Reprinted CP 1.135–140.
  - Peirce, C.S., "Application of C.S. Peirce to the Executive Committee
    of the Carnegie Institution, July 15, 1902." Published in Eisele,
    Carolyn, ed. (1976) "Parts of Carnegie Application (L75)" in *The
    New Elements of Mathematics by Charles S. Peirce, Vol. 4,
    Mathematical Philosophy*. The Hague, Netherlands: Mouton Publishers:
    13–73. [Eprint version edited by Joseph
    Ransdell](http://www.cspeirce.com/menu/library/bycsp/l75/l75.htm)
  - Peirce, C.S. (1992) *The Essential Peirce, Selected Philosophical
    Writings, Vol. 1 (1867–1893)*, Nathan Houser and Christian Kloesel,
    eds. Bloomington and Indianapolis, IN: Indiana University Press.
  - Peirce, C.S. (1998) *The Essential Peirce, Selected Philosophical
    Writings, Volume 2 (1893–1913)*, Peirce Edition Project, eds.
    Bloomington and Indianapolis, IN: Indiana University Press.
  - Robin, Richard S. (1967) *[Annotated Catalogue of the Papers of
    Charles S. Peirce](http://www.iupui.edu/~peirce/robin/robin.htm)*.
    Amherst MA: University of Massachusetts Press.
  - Taylor, Barry N., ed. (2001) *[The International System of Units,
    NIST Special
    Publication 330](http://physics.nist.gov/Pubs/SP330/sp330.pdf)*.
    Washington DC: Superintendent of Documents.
  - [van Heijenoort, Jean](../Page/Jean_van_Heijenoort.md "wikilink")
    (1967) "Logic as Language and Logic as Calculus," *Synthese 17*:
    324–30.

</div>

[Category:美国数学家](../Category/美国数学家.md "wikilink")
[Category:美国哲学家](../Category/美国哲学家.md "wikilink")
[Category:逻辑学家](../Category/逻辑学家.md "wikilink")
[Category:大地測量學家](../Category/大地測量學家.md "wikilink")
[Category:约翰·霍普金斯大学校友](../Category/约翰·霍普金斯大学校友.md "wikilink")
[Category:形而上学学者](../Category/形而上学学者.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:格理论学家](../Category/格理论学家.md "wikilink")
[Category:模态逻辑学家](../Category/模态逻辑学家.md "wikilink")