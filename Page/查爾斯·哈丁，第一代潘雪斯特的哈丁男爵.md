[Charles_Hardinge.jpg](https://zh.wikipedia.org/wiki/File:Charles_Hardinge.jpg "fig:Charles_Hardinge.jpg")
**查理斯·哈丁，第一代潘雪斯特的哈丁男爵**<span style="font-size:smaller">，[GCB](../Page/巴斯勳章.md "wikilink")，[GCSI](../Page/印度之星勳章.md "wikilink")</span><span style="font-size:smaller">，[GCIE](../Page/印度帝國勳章.md "wikilink")，[GCVO](../Page/皇家維多利亞勳章.md "wikilink")</span><span style="font-size:smaller">，[PC](../Page/英國樞密院.md "wikilink")</span>（，）是[英國外交官](../Page/英國.md "wikilink")、[印度总督](../Page/印度总督.md "wikilink")。

他是1844年至1848年任印度總督的[亨利·哈丁爵士之孫](../Page/亨利·哈丁，第一代哈丁子爵.md "wikilink")。1880年進入外交界。

[H](../Category/印度总督.md "wikilink")
[H](../Category/聯合王國男爵.md "wikilink")
[H](../Category/嘉德騎士.md "wikilink")
[H](../Category/帝國服務勳章.md "wikilink")
[H](../Category/哈羅公學校友.md "wikilink")
[H](../Category/劍橋大學三一學院校友.md "wikilink")
[Category:英國駐法國大使](../Category/英國駐法國大使.md "wikilink")