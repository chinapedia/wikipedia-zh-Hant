**查爾斯·奧古斯都·林德伯格**（，又译**-{zh-hant:林德伯格;
zh-hans:林白;}-**，）是一位著名[美國](../Page/美國.md "wikilink")[飛行員](../Page/飛行員.md "wikilink")、[作家](../Page/作家.md "wikilink")、[發明家](../Page/發明家.md "wikilink")、[探險家與](../Page/探險家.md "wikilink")[社會活動家](../Page/社會活動家.md "wikilink")。他於1927年駕駛[單引擎飛機](../Page/單引擎飛機.md "wikilink")[聖路易斯精神號](../Page/聖路易斯精神號.md "wikilink")（Spirit
of St.
Louis.），從[紐約市](../Page/紐約市.md "wikilink")[羅斯福飛行場橫跨](../Page/羅斯福飛行場.md "wikilink")[大西洋飛至](../Page/大西洋.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[巴黎－勒布爾熱機場](../Page/巴黎－勒布爾熱機場.md "wikilink")，成為歷史上首位成功完成單人[不着陆飛行橫跨](../Page/不着陆飛行.md "wikilink")[大西洋的人](../Page/大西洋.md "wikilink")，並因此獲贈[榮譽勳章](../Page/榮譽勳章.md "wikilink")。美國[聖地亞哥林德伯格國際機場即以他的名字命名](../Page/聖地牙哥國際機場.md "wikilink")。

## 早年

查爾斯·奧古斯都·林德伯格生於美國[密西根州](../Page/密西根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")，是[瑞典](../Page/瑞典.md "wikilink")[移民的後代](../Page/移民.md "wikilink")，在[利特尔福尔斯成長](../Page/利特尔福尔斯_\(明尼苏达州\).md "wikilink")。其父[查爾斯·奧古斯特·林德伯格](../Page/查爾斯·奧古斯特·林德伯格.md "wikilink")（Charles
August
Lindbergh）是[律師](../Page/律師.md "wikilink")，他從1907年至1917年擔任[國會議員](../Page/國會議員.md "wikilink")，曾反對美國參與[第一次世界大戰並因此受到批評](../Page/第一次世界大戰.md "wikilink")\[1\]。其母艾格林·林德伯格是底特律[卡斯技職高中的](../Page/卡斯技職高中.md "wikilink")[化學教師](../Page/化學.md "wikilink")\[2\]。早年林德伯格曾對[藥物有興趣](../Page/藥物.md "wikilink")。林德伯格後來畢業於[華盛頓](../Page/華盛頓.md "wikilink")[西德維爾·佛瑞德斯學校](../Page/西德維爾·佛瑞德斯學校.md "wikilink")\[3\]與[加州](../Page/加州.md "wikilink")[雷東多高中](../Page/雷東多高中.md "wikilink")\[4\]
，他後來也移居到加州。他在1920年進入[威斯康辛大學麥迪遜分校就讀](../Page/威斯康辛大學麥迪遜分校.md "wikilink")，但是後來於1922年2月停止[機械工程的課程](../Page/機械工程.md "wikilink")，開始參加了[飛機師和](../Page/飛機師.md "wikilink")[機械師訓練](../Page/機械師.md "wikilink")\[5\]。

## 展開飛行生涯

林白在1922年成為[內布拉斯加州飛機公司飛行學校學生](../Page/內布拉斯加州.md "wikilink")，並於1922年4月9日進行生涯首次飛行，當時[教練機](../Page/教練機.md "wikilink")[駕駛為](../Page/駕駛.md "wikilink")[奧托·蒂姆](../Page/奧托·蒂姆.md "wikilink")\[6\]。林白於1923年5月進行生涯第一次單人飛行，地點是在[喬治亞州](../Page/喬治亞州.md "wikilink")[阿梅里克斯](../Page/阿梅里克斯.md "wikilink")。他也購買了私人飛機[柯蒂斯
JN-4](../Page/柯蒂斯_JN-4.md "wikilink")（），並決定要獨自進行飛行。他在1923年於[阿肯色州湖村完成生涯首次夜間飛行](../Page/阿肯色州.md "wikilink")\[7\]。

1924年3月19日，他開始隨[美國陸軍航空隊](../Page/美國陸軍航空隊.md "wikilink")，接受一年的飛行員訓練\[8\]。在畢業8天前，林德伯格在1925年3月5日經歷了嚴重飛行事故，他與另一架飛機在空中相撞，被迫[跳傘逃生](../Page/跳傘.md "wikilink")\[9\]。

他在3月以第一名成績畢業後，成為一位[航空軍事預備隊飛行師](../Page/航空軍事預備隊.md "wikilink")，成為[少尉](../Page/少尉.md "wikilink")\[10\]。他於1920年代在[聖路易斯線工作](../Page/聖路易斯線.md "wikilink")，在1925年11月晉升為[中尉](../Page/中尉.md "wikilink")\[11\]。

1925年10月，林白進入[聖路易](../Page/聖路易.md "wikilink")[羅伯遜飛機公司](../Page/羅伯遜飛機公司.md "wikilink")（RAC）工作，提供聖路易和[芝加哥之間的服務](../Page/芝加哥.md "wikilink")，中繼站是[伊利諾伊州](../Page/伊利諾伊州.md "wikilink")[斯普林菲爾德及](../Page/斯普林菲爾德.md "wikilink")[皮奧里亞](../Page/皮奥里亚_\(伊利诺伊州\).md "wikilink")（梅伍德場）\[12\]。林白繼續CAM-2的總飛行師，直到1927年2月中旬為止。當時他前往加利福尼亞州[聖地亞哥](../Page/聖地亞哥.md "wikilink")，監督[聖路易斯精神號的設計和建造](../Page/聖路易斯精神號.md "wikilink")\[13\]。

## 首次單人不着陆橫跨大西洋飛行

[Spirit_of_St._Louis_Smithsonian.JPG](https://zh.wikipedia.org/wiki/File:Spirit_of_St._Louis_Smithsonian.JPG "fig:Spirit_of_St._Louis_Smithsonian.JPG")

1927年5月20日，林德伯格駕駛[單引擎飛機聖路易斯精神號](../Page/單引擎飛機.md "wikilink")（機型為[萊安NYP-1](../Page/萊安NYP-1.md "wikilink")），從[紐約市羅斯福飛行場起飛](../Page/紐約市.md "wikilink")，目的地是[巴黎](../Page/巴黎.md "wikilink")，準備飛越[大西洋](../Page/大西洋.md "wikilink")，進行不着陆單人飛行。在接下來的33.5小時中，林白面臨許多挑戰，包括以10,000英尺（3,000米）的高度掠過兩個[積雨雲](../Page/積雨雲.md "wikilink")，也在10英尺（3.0米）的高度飛越海面。他曾在[霧中迷航好幾個小時](../Page/霧.md "wikilink")。查爾斯·奧古斯都·林德伯格僅靠著[天文航海技術及](../Page/天文航海技術.md "wikilink")[航位推算法](../Page/航位推算法.md "wikilink")（Dead
reckoning）來引導飛行方向\[14\]。

他於5月21日22:22順利抵達[巴黎－勒布爾熱機場](../Page/巴黎－勒布爾熱機場.md "wikilink")，守候在當地的觀眾估計有150,000人之多。林德伯格在警察及軍隊的保護下才得以從人群中脫身\[15\]。林德伯格立刻成為世界著名的人物，更因此獲[奧特洛獎](../Page/奧特洛獎.md "wikilink")（Orteig
Prize）。

## 第二次世界大戰

[LindberghStLouis.jpg](https://zh.wikipedia.org/wiki/File:LindberghStLouis.jpg "fig:LindberghStLouis.jpg")

在[歐洲](../Page/歐洲.md "wikilink")[法西斯主義盛行時](../Page/法西斯主義.md "wikilink")，林德伯格數次奉美軍之命往[德國](../Page/德國.md "wikilink")。1938年，[赫尔曼·戈林授林德伯格德國榮譽勳章](../Page/赫尔曼·戈林.md "wikilink")，林德伯格接受，表示了林德伯格和[納粹親近](../Page/納粹.md "wikilink")。林德伯格更拒絕將勳章交還德國，聲稱這樣做是對納粹首領「不必要的侮辱」。

[納粹德國展開](../Page/納粹德國.md "wikilink")[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，林德伯格得到[孤立主義和親德國政治派系的支持](../Page/孤立主義.md "wikilink")。林德伯格於1940年末成為是[美國第一委員會發言人](../Page/美國第一委員會.md "wikilink")，然後很快就成為最具份量的發言人\[16\]，在紐約市[麥迪遜廣場花園和](../Page/麥迪遜廣場花園.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")[士兵球場的演說吸引無數觀眾](../Page/士兵球場.md "wikilink")。1941年1月23日，林德伯格在國會前提案建議美國和[希特勒建立中立關係](../Page/希特勒.md "wikilink")。1941年9月11日在[愛荷華州](../Page/愛荷華州.md "wikilink")[迪莫伊的一個集會中](../Page/迪莫伊.md "wikilink")，林德伯格批評[猶太人背後操縱](../Page/猶太人.md "wikilink")，要美國加入[同盟國](../Page/同盟國.md "wikilink")。他同時表明他認為[美籍猶太人不太愛國](../Page/美籍猶太人.md "wikilink")。雖然林德伯格不曾將其勳章交還，但當總統[富蘭克林·羅斯福質問其忠誠時](../Page/富蘭克林·羅斯福.md "wikilink")，林德伯格从[美國陸軍航空團辞职](../Page/美國陸軍航空團.md "wikilink")。

可是在1941年的[珍珠港事件後](../Page/珍珠港事件.md "wikilink")，他想回到航空團，卻得不到同意。結果他一直以[平民顧問協助二戰中的美國](../Page/平民.md "wikilink")。美國[陸軍航空隊雖然與他合作](../Page/陸軍航空隊.md "wikilink")，林德伯格以他的經驗提供其他飛行員如何進行長距離飛行以及延長飛機的航程，他並且以平民的身分飛行過50次的戰鬥任務，其中一次還擊落過一架日本戰機，只是這個紀錄當時沒有公開。

## 晚年

[Charles-lindberg-grave-overall.jpg](https://zh.wikipedia.org/wiki/File:Charles-lindberg-grave-overall.jpg "fig:Charles-lindberg-grave-overall.jpg")

第二次世界大戰結束後，他居住在[康乃狄克州](../Page/康乃狄克州.md "wikilink")，同時擔任[泛美航空及](../Page/泛美航空.md "wikilink")[美國空軍的顧問](../Page/美國空軍.md "wikilink")。1954年[艾森豪威尔將林德伯格晉升為](../Page/艾森豪威尔.md "wikilink")[准將](../Page/准將.md "wikilink")。之後林白知悉有一個研究火箭的團隊，立刻予以金援，並替其謀得美國軍方高層的眼界，這個研究團隊的領導者即是「美國火箭之父」[戈達德](../Page/戈達德.md "wikilink")。

1953年，他出版[自傳](../Page/自傳.md "wikilink")《[聖路易斯精神號](../Page/聖路易斯精神號.md "wikilink")》。這本書後來獲得了1954年[普利茲獎](../Page/普利茲獎.md "wikilink")\[17\]
，2004年[太空船1號
16P](../Page/太空船1號_16P.md "wikilink")（）將《聖路易斯精神號》送上太空。從1960年代開始，林白參與保護[瀕危物種運動](../Page/瀕危物種.md "wikilink")，例如[座頭鯨和](../Page/座頭鯨.md "wikilink")[藍鯨](../Page/藍鯨.md "wikilink")\[18\]。

1968年12月，他在歷史上首次載人航天飛行前一天，訪問了[阿波羅8號的成員](../Page/阿波羅8號.md "wikilink")。1969年7月16日，林白與前瑞安飛公司總裁[T·克勞德·瑞安](../Page/T·克勞德·瑞安.md "wikilink")（曾建造聖路易斯精神號）在[卡納維拉爾角觀看](../Page/卡納維拉爾角.md "wikilink")[阿波羅11號發射](../Page/阿波羅11號.md "wikilink")\[19\]。

晚年，林德伯格移居到[夏威夷](../Page/夏威夷.md "wikilink")[毛伊岛生活](../Page/毛伊岛.md "wikilink")。他在1974年因[淋巴癌去世](../Page/淋巴癌.md "wikilink")\[20\]
，葬於教堂。

## 家庭生活

林德伯格的[妻子是](../Page/妻子.md "wikilink")[作家](../Page/作家.md "wikilink")[安妮·默洛·林德伯格](../Page/安妮·默洛·林德伯格.md "wikilink")（Anne
Morrow
Lindbergh），他們在1929年結婚，共育有六名子女。其長子[小查尔斯·奥古斯特·林德伯格在](../Page/小查尔斯·奥古斯特·林德伯格.md "wikilink")1932年新澤西家中被[綁架](../Page/綁架.md "wikilink")，最初兇手要求五萬美金贖金，後來提高到七萬。但是最後小查爾斯·林德伯格遭到撕票，該案件震驚美國，被視為「世紀犯罪」。記者H·L·門肯將之形容為「耶穌復活以來最重大的犯罪」\[21\]。兇手後來被認定是一位德國的移民[布魯諾·豪夫曼](../Page/布魯諾·豪夫曼.md "wikilink")（），他最終被判處[死刑](../Page/死刑.md "wikilink")，1936年4月3日於[紐澤西州立監獄執行](../Page/紐澤西州立監獄.md "wikilink")\[22\]，但許多人相信他只是倒霉的替死鬼。英國[偵探小說作家](../Page/偵探小說.md "wikilink")[阿嘉莎·克莉斯蒂](../Page/阿嘉莎·克莉斯蒂.md "wikilink")（Agatha
Mary Clarissa
Christie）據此疑案發揮寫成《[東方快車謀殺案](../Page/東方快車謀殺案.md "wikilink")》（*Murder
on the Orient Express*）。

林德伯格從1957年開始至到逝世之前，一直和比他小24年的[德國帽子製造者](../Page/德國.md "wikilink")存在親密關係。他們育有三名子女，但是一直將掩蓋這件事情，甚至其子女也不知父親的真實身分。女兒後來閱讀到雜誌上有關林德伯格的文章，並發現林德伯格寫給的一百多封[書信](../Page/書信.md "wikilink")。她在和安妮·林德伯格都去世2年後，於2003年將此事件公開。

2002年，查爾斯·奧古斯都·林德伯格的孫子埃里克·林德伯格為了紀念林白成功飛越大西洋75年，重複了一次林德伯格1927年橫跨大西洋的飛行路線\[23\]。

## 相關作品

  - [菲力浦·羅斯的虛構歷史小說](../Page/菲力浦·羅斯.md "wikilink")《[反美陰謀](../Page/反美陰謀.md "wikilink")》（）。
  - 1957年導演[比利·懷德的傳記電影](../Page/比利·懷德.md "wikilink")《[林白征空記](../Page/林白征空記.md "wikilink")》（原名，即聖路易斯精神號；又譯《壯志凌雲》），由另一位真實的空軍英雄[詹姆斯·史都華主演](../Page/詹姆斯·史都華.md "wikilink")。

## 參考

## 參考文獻

  - Lindbergh, Charles A. *Charles A. Lindbergh: Autobiography of
    Values*. New York: Harcourt Brace Jovanovich, 1977. ISBN
    0-15-110202-3.
  - Lindbergh, Charles A. *Spirit of St. Louis*. New York: Scribners,
    1953.
  - Lindbergh, Charles A. *The Wartime Journals of Charles A.
    Lindbergh*. New York: Harcourt, Brace, Jovanovich, 1970. ISBN
    978-0-15-194625-9.
  - Lindbergh, Charles A. *"WE"* (with an appendix entitled *"A Little
    of what the World thought of Lindbergh"* by Fitzhugh Green,
    pp. 233–318). New York & London: G.P. Putnam's Sons (The
    Knickerbocker Press), July 1927.

## 外部連結

  - [Lindbergh foundation](http://www.lindberghfoundation.org/)

  -
  - Retrieved: June 17, 2009.

  - The [Lindbergh Family Papers, including some materials of the famous
    aviator, are available for research use at the Minnesota Historical
    Society](http://www.mnhs.org/library/findaids/P1675.xml)

  - [Minnesota Historical Society](http://www.mnhs.org)

  - [Lindbergh Related Items in the Missouri History Museum
    Collection](http://collections.mohistory.org/search/custom_search?reset=1&text=Lindbergh&images_only=1)

  - [Listen to the story of Charles Lindbergh online – The American
    Storyteller Radio
    Journal](http://www.theamericanstoryteller.com/story-details.cfm?story=289)

  - [Lindbergh's first solo
    flight](http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-3125&sug=y/)

  - ：[](http://www.charleslindbergh.com/)

  - [林德伯格從巴黎到比利時的飛行，慶祝他跨大西洋的飛行](http://upload.wikimedia.org/wikipedia/commons/2/29/Charles_Lindbergh_flight_to_Brussels.ogg)

[Category:美國飛行員](../Category/美國飛行員.md "wikilink")
[Category:普利茲獎獲獎者](../Category/普利茲獎獲獎者.md "wikilink")
[Category:美國陸軍榮譽勳章獲得者](../Category/美國陸軍榮譽勳章獲得者.md "wikilink")
[Category:美國空軍准將](../Category/美國空軍准將.md "wikilink")
[Category:時代年度風雲人物](../Category/時代年度風雲人物.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:密西根州人](../Category/密西根州人.md "wikilink")
[Category:美国探险家](../Category/美国探险家.md "wikilink")

1.  Larson 1973, pp. 208–209.
2.  Larson 1973, pp. 31–32.
3.  Lindbergh 1927, pp. 19–22.
4.  Berg 1998, p. 22.
5.  Lindbergh 1927, pp. 22-25.
6.  Lindbergh 1927, p. 25.
7.  Lindbergh 1927, pp. 63–65.
8.  Berg 1998, p. 73.
9.  Lindbergh 1927, pp. 144–148.
10. Moseley 1976, p. 56.
11. ["Charles Lindbergh: An American
    Aviator."](http://www.charleslindbergh.com/timeline/index.asp)
    *charleslindbergh.com.* Retrieved: February 15, 2010.
12. ["Robertson Aircraft
    Corporation."](http://www.charleslindbergh.com/mystory/thompson.asp)
    *charleslindbergh.com.* Retrieved: February 15, 2010.
13. Lindbergh 1953, p. 79.
14. Lindbergh 1927, pp. 218–222.
15. Lindbergh 1927, pp. 224–226.
16. Mosley 1976, p. 257.
17. ["1954 Winners."](http://www.pulitzer.org/awards/1954) *The Pulitzer
    Prizes*. Retrieved: November 22, 2011.
18. Bower, Bruce.
    \[<http://www.thefreelibrary.com/The+strange+case+of+the+Tasaday:+were+they+primitive+hunter-gatherers>...-a07593059
    "The strange case of the Tasaday: were they primitive
    hunter-gatherers or rain-forest phonies?"\] *Science News*, May 6,
    1989.
19. "Private Pilot Textbook GFD." *Jeppesen*. Retrieved: January 19,
    2011.
20. ["Choosing Life: Living Your Life While Planning for Death: Charles
    Lindbergh."](http://www.cancersupportivecare.com/plan.html)
    *cancersupportivecare.com.* Retrieved: January 19, 2011.
21. Newton 2012,
    [p. 197.](http://www.mcfarlandbooks.com/book-2.php?id=978-0-7864-6620-7)

22. "Hoffman Carries Fight to Critics; Insists Lindbergh Case Not Fully
    Solved." *The New York Times,* April 6, 1936, p. 42.
23. Silverman, Steven M. ["Another Lucky Lindy Lands in
    Paris."](http://www.people.com/people/article/0,,623920,00.html)
    *People,* May 3, 2002. Retrieved: January 22, 2011.