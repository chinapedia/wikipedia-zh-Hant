[Charles_Jenkinson,_1st_Earl_of_Liverpool_by_George_Romney.jpg](https://zh.wikipedia.org/wiki/File:Charles_Jenkinson,_1st_Earl_of_Liverpool_by_George_Romney.jpg "fig:Charles_Jenkinson,_1st_Earl_of_Liverpool_by_George_Romney.jpg")
**查尔斯·詹金逊**（；）[英格蘭](../Page/英格蘭.md "wikilink")[政治家](../Page/政治家.md "wikilink")，父亲查尔斯·詹金逊上校（d.
1750）是一名軍官，他是长子，生于[温切斯特](../Page/温切斯特.md "wikilink")。这个家族是[安东尼·詹金逊船长](../Page/安东尼·詹金逊.md "wikilink")（d.
1611）的后裔，安东尼·詹金逊是商人和旅行者，第一个进入中亚的英国人。

詹金逊就读于切特豪斯学校（Charterhouse
School）和[牛津大学学院](../Page/牛津大学.md "wikilink")，1752年硕士毕业。1760年成為[喬治三世龐臣](../Page/乔治三世_\(英国\).md "wikilink")[標得伯爵](../Page/约翰·斯图尔特.md "wikilink")（Bute）的私人祕書，他赢得喬治三世的喜爱，標得伯爵退休后詹金逊[英国下议院](../Page/英国下议院.md "wikilink")“国王朋友”的领袖。1763年[乔治·格伦维尔任命他为财政秘书](../Page/乔治·格伦维尔.md "wikilink")，1766年短暂退休后，他担任和[财政大臣](../Page/财政大臣_\(英國\).md "wikilink")，1778年任，1786年到1803年任和[蘭開斯特公爵領地事務大臣](../Page/蘭開斯特公爵領地事務大臣.md "wikilink")，1786年他被授于**霍克斯伯里男爵**（Baron
Hawkesbury）爵位，十年后再次授**[利物浦伯爵](../Page/利物浦伯爵.md "wikilink")**（Earl of
Liverpool）爵位。

利物浦伯爵有两次婚姻：第一次同阿梅莉娅（Amelia） (d.
1770)，威廉·瓦茨的女儿。第二次同凯瑟琳（Catherine），塞西尔·比索夫爵士（Cecil
Bisshoff）的女儿，两次婚姻各有一个儿子，大儿子[罗伯特·班克斯·詹金逊](../Page/罗伯特·班克斯·詹金逊.md "wikilink")（Robert
Banks Jenkinson）是著名的政治家，后成为[英国首相](../Page/英国首相.md "wikilink")。

[澳大利亚](../Page/澳大利亚.md "wikilink")[新南威尔士州的](../Page/新南威尔士州.md "wikilink")[霍克斯堡河](../Page/霍克斯堡河.md "wikilink")（霍克斯伯里河）和[加拿大](../Page/加拿大.md "wikilink")[安大略省东部的霍克斯伯里镇即以他的爵号](../Page/安大略省.md "wikilink")**霍克斯伯里男爵**命名。

[L](../Category/英國政治人物.md "wikilink")
[L](../Category/牛津大学大学学院校友.md "wikilink")
[L](../Category/大不列顛伯爵.md "wikilink")