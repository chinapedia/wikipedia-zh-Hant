**柬埔寨王国**（；），通称**柬埔寨**（，），位于[东南亚](../Page/东南亚.md "wikilink")[中南半岛](../Page/中南半岛.md "wikilink")，首都[金邊](../Page/金邊.md "wikilink")，為[联合国会员国及](../Page/联合国会员国.md "wikilink")[东南亚国家联盟成员国](../Page/东南亚国家联盟.md "wikilink")。

柬埔寨西部及西北部与[泰国接壤](../Page/泰国.md "wikilink")，东北部与[老挝交界](../Page/老挝.md "wikilink")，东部及东南部与[越南毗邻](../Page/越南.md "wikilink")，南部则面向[暹羅灣](../Page/暹羅灣.md "wikilink")。柬埔寨领土为碟狀[盆地](../Page/盆地.md "wikilink")，三面受[丘陵與](../Page/丘陵.md "wikilink")[山脈環繞](../Page/山脈.md "wikilink")；中部為廣阔而富庶的[平原](../Page/平原.md "wikilink")，占全國面積四分之三以上。境内有[湄公河和东南亚最大的](../Page/湄公河.md "wikilink")[淡水湖](../Page/淡水湖.md "wikilink")－[洞里萨湖](../Page/洞里萨湖.md "wikilink")（又稱金邊湖）。

## 历史

柬埔寨為[中南半島之文明古國](../Page/中南半島.md "wikilink")，有2000年以上之歷史。

[Bayon_Angkor_frontal.jpg](https://zh.wikipedia.org/wiki/File:Bayon_Angkor_frontal.jpg "fig:Bayon_Angkor_frontal.jpg")
昔稱[扶南](../Page/扶南.md "wikilink")，素與[秦](../Page/秦.md "wikilink")、[漢通商交流](../Page/漢.md "wikilink")。《[后汉书](../Page/后汉书.md "wikilink")》称为究不事，《[隋书](../Page/隋书.md "wikilink")》称为[真腊](../Page/真腊.md "wikilink")，《[唐书](../Page/唐书.md "wikilink")》称为吉蔑、阁蔑（均Khmer对音），[元朝称为甘勃智](../Page/元朝.md "wikilink")，《[明史](../Page/明史.md "wikilink")》稱甘武者，[明朝](../Page/明朝.md "wikilink")[万历后](../Page/万历.md "wikilink")，音譯柬埔寨。“究不事”、“甘勃智”、“甘武者”、“柬埔寨”實為Kambuja对音。

西元一世紀時，柬有扶南國，奉[印度教為](../Page/印度教.md "wikilink")[國教](../Page/國教.md "wikilink")，故受[印度文化影響甚深](../Page/印度文化.md "wikilink")。扶南國都是毗耶陀補羅城（Vyadhapura，梵文：獵人城），在今[波羅勉省附近](../Page/波羅勉省.md "wikilink")，有湄公河西北流東入海。公元七世紀中葉，扶南國為北方崛起的真臘所滅，扶南王子流亡[爪哇](../Page/爪哇.md "wikilink")，建立[山帝王朝](../Page/山帝王朝.md "wikilink")。

[真臘攻灭扶南后](../Page/真臘.md "wikilink")，国势更加强盛；至阇耶跋摩一世统治时期，又征服了老挝的中部和北部地区，使国境北接[南诏](../Page/南诏.md "wikilink")，南抵湄公河之下游，包括了今之柬埔寨、老挝以及越南南部。阇耶跋摩一世无嗣，卒后由侄女阇耶提黛维继位，[國家竟告分裂](../Page/國家分裂.md "wikilink")，史称水陆真腊，陷入内乱。

公元787年前后，水真腊太阳王朝的都城桑比补罗被[爪哇海盗攻陷](../Page/爪哇.md "wikilink")，国王摩希婆提跋摩被杀，王子（后来的[阇耶跋摩二世](../Page/阇耶跋摩二世.md "wikilink")）被虏。水真腊由此受到爪哇的[夏连特拉王朝](../Page/夏连特拉王朝.md "wikilink")（山帝王朝）统治，直至阇耶跋摩二世从爪哇逃回，称王独立。阇耶跋摩二世统一了水、陆真腊，定都[吴哥东北约三十公里的荔枝山](../Page/吴哥.md "wikilink")，建立了[吴哥王朝](../Page/吴哥王朝.md "wikilink")。

[吳哥王朝](../Page/吳哥王朝.md "wikilink")，又稱[高棉帝國](../Page/高棉帝國.md "wikilink")，至12世纪國勢臻盛，文化燦爛，版圖包括今日柬埔寨全境以及[泰](../Page/泰國.md "wikilink")、[寮](../Page/老撾.md "wikilink")、[越三國之部份地區](../Page/越南.md "wikilink")。13世纪，[上座部佛教由](../Page/上座部佛教.md "wikilink")[斯里兰卡传来](../Page/斯里兰卡.md "wikilink")。明宣德五年（1430年）[暹羅入侵柬埔寨](../Page/暹羅.md "wikilink")，包圍吳哥城七個月，最後攻破吳哥。由於吳哥太近於暹羅，故放棄吳哥，遷都金邊。此後，柬埔寨國勢衰敗，备受[越南](../Page/越南.md "wikilink")、[暹羅](../Page/暹羅.md "wikilink")（[泰國](../Page/泰國.md "wikilink")）這兩個鄰國的侵略，史稱[柬埔寨黑暗時代](../Page/柬埔寨黑暗時代.md "wikilink")。[下柬埔寨](../Page/下柬埔寨.md "wikilink")（即[湄公河三角洲](../Page/湄公河三角洲.md "wikilink")）亦在这个时期为越南所占据，衍生柬、越長年的世仇。

[Indochine_française_(1913).jpg](https://zh.wikipedia.org/wiki/File:Indochine_française_\(1913\).jpg "fig:Indochine_française_(1913).jpg")时期地图，1863-1953\]\]
歐美列強侵略殖民地時期，1863年淪為[法國](../Page/法國.md "wikilink")[保護國](../Page/保護國.md "wikilink")。[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")，1942至1945年，日軍佔領柬埔寨。戰後[柬埔寨於](../Page/柬埔寨保護國.md "wikilink")1953年11月9日脫離法國獨立。初時仍為君權體制，即柬埔寨王國（[西哈努克第一次執政時期](../Page/諾羅敦·施漢諾.md "wikilink")）。

1960年代中開始，中、美、蘇三國均進駐[印度支那半島侵略與擴張勢力](../Page/印度支那半島.md "wikilink")。政治博弈下，柬埔寨開始發生內戰。1970年3月18日，由親美的[龍诺將軍发动政变](../Page/龍诺.md "wikilink")，成立[高棉共和國](../Page/高棉共和國.md "wikilink")，廢止君主制，親中親共的[西哈努克親王流亡於](../Page/諾羅敦·施漢諾.md "wikilink")[北京](../Page/北京.md "wikilink")，其時為[越戰期間](../Page/越戰.md "wikilink")，[南越軍](../Page/南越軍.md "wikilink")、[美軍](../Page/美軍.md "wikilink")、龍诺政府軍和北越軍[赤柬軍交戰導致柬埔寨國內民不聊生](../Page/赤柬.md "wikilink")。

1975年4月17日，[红色高棉](../Page/红色高棉.md "wikilink")（或稱赤柬）在[美軍撤離柬埔寨首都](../Page/鷹遷行動.md "wikilink")[金邊後予以攻佔](../Page/金邊.md "wikilink")，建立[民主柬埔寨政權](../Page/民主柬埔寨.md "wikilink")，施亞努親王並重新自[北韓](../Page/北韓.md "wikilink")[平壤歸國](../Page/平壤.md "wikilink")，不過迅速遭到[红色高棉軟禁](../Page/红色高棉.md "wikilink")。1976年，[红色高棉波布當權後宣佈廢除貨幣](../Page/红色高棉.md "wikilink")，把全國所有城鎮居民驅趕到鄉郊，強迫集體務農，並屠殺所有知識分子。當政的三年八個月期間，實行極左恐怖統治，全國五分一以上人口死於饑荒、勞役、疾病或迫害等原因，是20世紀中最血腥暴力的人為災難之一。

[红色高棉波布政權由於和越南政權因歷史疆界土地糾紛和越南政權屢起齟齬](../Page/红色高棉.md "wikilink")，而又因和中國友好且交流密切，時值中蘇交惡衝突緊張對抗期間，所以形成柬聯中以對抗越蘇態勢。

1978年12月，在蘇聯支持下越南進攻柬埔寨，[红色高棉早已失去民心](../Page/红色高棉.md "wikilink")，[越南人民軍很快推進並於](../Page/越南人民軍.md "wikilink")1979年1月7日攻佔金邊，建立親越南的[傀儡政權](../Page/傀儡政權.md "wikilink")，也就是[柬埔寨人民共和國](../Page/柬埔寨人民共和國.md "wikilink")，同時中國為幫助[红色高棉波布政權而在北方入侵越南以做牽制](../Page/红色高棉.md "wikilink")，即為所謂[懲越戰爭](../Page/懲越戰爭.md "wikilink")。不過越南此一建立親越南的[傀儡政權行為未獲國際社會多數國家承認](../Page/傀儡政權.md "wikilink")，因此暫時保住了[民主柬埔寨在聯合國的席位](../Page/民主柬埔寨.md "wikilink")。但[红色高棉對自己人民犯下的暴行的證據](../Page/红色高棉.md "wikilink")，在各地被發現，越南人將所有證據公之於眾，[柬共亦失去國際社會支持](../Page/柬埔寨共產黨.md "wikilink")，赤棉敗走柬埔寨西北地區，靠近[泰國邊境割據一方](../Page/泰國.md "wikilink")。

1989年1月6日，[越南人民军从柬埔寨全數撤军](../Page/越南人民军.md "wikilink")。柬埔寨人民共和国更改国名为[柬埔寨国](../Page/柬埔寨国.md "wikilink")，以获得更多的国际认同。1991年，[冷战结束](../Page/冷战.md "wikilink")，柬埔寨放弃[一党制的政治体制和](../Page/一党制.md "wikilink")[马克思列宁主义](../Page/马克思列宁主义.md "wikilink")。1992年，赤棉的武装势力投降。1993年，柬埔寨獲[聯合國援助](../Page/聯合國.md "wikilink")，舉行全國大選，親王室的[奉辛比克黨大勝](../Page/奉辛比克黨.md "wikilink")，[洪森領導的人民黨屈居第二](../Page/洪森.md "wikilink")。經洪森威脅，兩黨組織[大聯合政府](../Page/大聯合政府.md "wikilink")。1993年9月24日，柬埔寨制宪议会通过新宪法，恢复君主制及國名[柬埔寨王國](../Page/柬埔寨王國.md "wikilink")，三度回国的[西哈努克重新登基为国王](../Page/諾羅敦·施漢諾.md "wikilink")。后来洪森發動政變，人民黨开始控制新政府。近年来，柬埔寨社會日趨安定，经济恢复，旅游业发展迅速。

2008年8月，[柬埔寨人民党赢得第四届国会选举](../Page/柬埔寨人民党.md "wikilink")（90席）。2013年7月，人民党赢得123个议席中的68个，延续[洪森](../Page/洪森.md "wikilink")28年来的执政和第五個首相任期，反对党[救国党赢得](../Page/柬埔寨救国党.md "wikilink")55个议席，比上届大选的29个议席几乎多一倍\[1\]。

2018年3月3日，据柬埔寨国家选举委员会公布的第四届参议院选举正式结果，执政党柬埔寨人民党赢得参议院全部58个议席。此前[柬埔寨救国党遭到柬埔寨政府解散](../Page/柬埔寨救国党.md "wikilink")，至此柬埔寨成为[一党制的獨裁国家](../Page/一党制.md "wikilink")。\[2\]。由于柬埔寨救国党是唯一能够挑战人民党执政地位的反对党，因该党被解散，也意味着柬埔寨进入“一党专政”。\[3\]

## 地理

[Satellite_image_of_Cambodia_in_January_2002.jpg](https://zh.wikipedia.org/wiki/File:Satellite_image_of_Cambodia_in_January_2002.jpg "fig:Satellite_image_of_Cambodia_in_January_2002.jpg")
全國總面積181,040平方公里，全國劃分為24個行政省，北鄰泰國、寮國，南與越南接壤，西南濱臨[暹羅灣](../Page/暹羅灣.md "wikilink")，海岸線長443公里，境內的[洞里薩湖是全東南亞最大的淡水湖](../Page/洞里薩湖.md "wikilink")，湖中富饒的水產，更是柬國人民饮食的重要來源，其餘境內有豐富的水資源，沼澤多處可見。源自中國[瀾滄江而流經東南亞五國的湄公河](../Page/瀾滄江.md "wikilink")，以流經柬埔寨境內河段的地均水資源量（275萬立方米／平方公里）最大，可供開發的水電資源也高達83,000兆瓦以上。因過度的開發與砍植，目前[森林的覆蓋率已經從戰前的](../Page/森林.md "wikilink")71%降至46%；气候為典型的[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")，6-11月為雨季，平均氣溫在[攝氏](../Page/攝氏.md "wikilink")28度。

柬埔寨位於中南半島西南部，佔地181,035平方公里，20%為農業用地。全國最南端至西邊區域地處熱帶區域，北方以扁擔山脈與泰國柯叻交界，東邊的臘塔納基里台地和Chhlong高地與[越南中央高地相鄰](../Page/越南中央高地.md "wikilink")。西邊是狹窄的海岸平原：面對暹邏灣的西哈努克海。扁擔山脈在洞里薩流域北邊，由泰國的柯叻台地南部陡峭懸崖構成，是泰國和柬埔寨國界。

與鄰國比較之下，柬埔寨領土不大，全國區分為24省（其中4省鄰海），另有3個自治市、172個區、1547個地方。國家海岸線長435公里，並有部分尚未開發的紅樹林。

柬埔寨國境內最醒目的地標，是坐落於國土中心的洞里薩湖、巴薩河和貫穿全國南北的湄公河。中央平原佔國土3/4面積，和東南和西部的象山、荳蔻山；延伸自泰國柯叻台地的扁擔山國界；以及東部越南中央高地交界處的Chhlong高地比較起來，是樹林叢聚，人口稀少的區域。

洞里薩湖─湄公低地區主要是低於海拔100公尺的平原，海拔升高時，地勢會更加崎嶇不平。西南方荳蔻山高於1500公呎以上，呈西北至東南走勢。[奥拉尔山](../Page/奥拉尔山.md "wikilink")（Phnom
Aural）是柬國最高山峰，位於東部，高1771公尺。

源自荳蔻山的象山，向南與東南延伸，高度介於500\~1000公尺。這兩個區域地勢多為500\~700公尺。　北方的荳蔻山和西方的扁擔山之間，是洞里薩河的延伸區域，廣至泰國境內平原，是前往曼谷的便道。

柬國最長的湄公河，為國家帶來豐沛用水。湄公河發源自中國，流經緬甸、寮國、泰國後，才進入柬國。在金邊，南方Bassak河，以及西北方聯結洞里薩湖的洞里薩河，繼續往東南方，從越南進入南中國海。

## 政治

### 政府

[National_Assembly_of_Cambodia.jpg](https://zh.wikipedia.org/wiki/File:National_Assembly_of_Cambodia.jpg "fig:National_Assembly_of_Cambodia.jpg")
1993年宪法规定，柬埔寨是[君主立宪制王国](../Page/君主立宪制.md "wikilink")，立法、行政、司法[三权分立](../Page/三权分立.md "wikilink")。国王是终身国家元首、国家军队最高司令、国家统一和永存的象征，有权宣布大赦。根据[首相建议](../Page/柬埔寨首相.md "wikilink")，并征得国民议会主席同意后，可解散国会。国王因故不能视事或不在国内期间，由参议院议长代理国家元首职务。国王去世后，由首相、佛教两派僧王、参议院和国民议会议长、副议长组成的9人[王位委员会](../Page/柬埔寨王位委员会.md "wikilink")，从王族后裔中，推选产生新国王。

[国民议会是柬埔寨全国最高权力机构和立法机构](../Page/柬埔寨国民议会.md "wikilink")，共有123个议席，每届任期五年。[参议院为国家立法机构](../Page/柬埔寨参议院.md "wikilink")，有权审议国民议会通过的法案，每届任期6年。柬宪法规定，国家法案须经国民议会、参议院和宪法委员会审议通过，最后由国王签署生效。2004年7月，柬埔寨颁布实施新增宪法条款。该条款规定，国民议会可以通过投票方式，决定国民议会领导层人选和批准新政府。2004年10月，柬埔寨国民议会审议并批准了关于选举王位继承人的王位委员会组織法和执行法，规定在国王去世、退休或退位后7天内，以选举产生柬埔寨新国王。

### 外交

中国是柬埔寨的最大援助国\[4\]。[柬埔寨首相](../Page/柬埔寨首相.md "wikilink")[洪森對外奉行親華的外交政策](../Page/洪森.md "wikilink")，因此柬埔寨被視為中國的盟國和勢力範圍。

因[柏威夏寺和歷史等主權爭議問題](../Page/柏威夏寺.md "wikilink")，和[泰國的關係緊張](../Page/泰國.md "wikilink")，並多次爆發小規模戰事。[柬埔寨首相](../Page/柬埔寨首相.md "wikilink")[洪森曾和前](../Page/洪森.md "wikilink")[泰國首相](../Page/泰國首相.md "wikilink")[塔信關係密切](../Page/塔信.md "wikilink")，曾於2009至2010年間聘任他為經濟顧問。\[5\]

### 行政区划

[<File:Cambodia_provinces_numbered.png>](https://zh.wikipedia.org/wiki/File:Cambodia_provinces_numbered.png "fig:File:Cambodia_provinces_numbered.png")

柬埔寨國內共計由1市24省组成，首都及最大城市为[金边市](../Page/金边市.md "wikilink")，而第二大城市为[马德望](../Page/马德望.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>省（, khet）<br />
直轄市（, krong）</p></th>
<th><p>nowrap|圖號</p></th>
<th><p>人口（2005）</p></th>
<th><p>nowrap|面積（k㎡）</p></th>
<th><p>省會和主要市鎮</p></th>
<th><p>人口（1998）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/班迭棉吉省.md" title="wikilink">班迭棉吉省</a>（, Banteay Meanchey）</p></td>
<td><p>1</p></td>
<td><p>773,100</p></td>
<td><p>6,679</p></td>
<td><p><a href="../Page/班迭棉吉省.md" title="wikilink">诗梳风Sisŏphŏn</a></p></td>
<td><p>98,848</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬德望省.md" title="wikilink">馬德望省</a>（, Battambang）</p></td>
<td><p>2</p></td>
<td><p>997,800</p></td>
<td><p>11,702</p></td>
<td><p><a href="../Page/馬德望.md" title="wikilink">馬德望Băt</a> Dambang</p></td>
<td><p>139,964</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/磅湛省.md" title="wikilink">磅湛省</a>（, Kampong Cham）</p></td>
<td><p>3</p></td>
<td><p>1,857,500</p></td>
<td><p>9,799</p></td>
<td><p><a href="../Page/磅湛.md" title="wikilink">磅湛Kampong</a> Cham</p></td>
<td><p>45,354</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磅清揚省.md" title="wikilink">磅清揚省</a>（, Kampong Chhnang）</p></td>
<td><p>4</p></td>
<td><p>513,200</p></td>
<td><p>5,521</p></td>
<td><p><a href="../Page/磅清揚.md" title="wikilink">磅清揚Kampong</a> Chhnăng</p></td>
<td><p>41,703</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/磅士卑省.md" title="wikilink">磅士卑省</a>（, Kampong Spoe）</p></td>
<td><p>5</p></td>
<td><p>729,600</p></td>
<td><p>7,017</p></td>
<td><p><a href="../Page/磅士卑.md" title="wikilink">磅士卑Kampong</a> Spoe</p></td>
<td><p>41,478</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磅通省.md" title="wikilink">磅通省</a>（, Kampong Thom）</p></td>
<td><p>6</p></td>
<td><p>681,700</p></td>
<td><p>13,814</p></td>
<td><p>磅同Kampong Thum</p></td>
<td><p>66,014</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貢布省.md" title="wikilink">貢布省</a>（, Kampot）</p></td>
<td><p>7</p></td>
<td><p>602,600</p></td>
<td><p>4,873</p></td>
<td><p><a href="../Page/貢布.md" title="wikilink">貢布Kampot</a></p></td>
<td><p>33,126</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/甘丹省.md" title="wikilink">甘丹省</a>（, Kandal）</p></td>
<td><p>8</p></td>
<td><p>1,242,500</p></td>
<td><p>3,568</p></td>
<td><p><a href="../Page/大金歐.md" title="wikilink">大金歐Ta</a> Khmau</p></td>
<td><p>58,264</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戈公省.md" title="wikilink">戈公省</a>（, Koh Kong）</p></td>
<td><p>9</p></td>
<td><p>191,500</p></td>
<td><p>11,160</p></td>
<td><p><a href="../Page/普明市.md" title="wikilink">普明市Krŏng</a> Kaoh Kŏng</p></td>
<td><p>29,329</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/桔井省.md" title="wikilink">桔井省</a>（, Kratié）</p></td>
<td><p>11</p></td>
<td><p>333,800</p></td>
<td><p>11,094</p></td>
<td><p>桔井Kratié</p></td>
<td><p>79,123</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒙多基里省.md" title="wikilink">蒙多基里省</a>（, Mondulkiri）</p></td>
<td><p>12</p></td>
<td><p>42,400</p></td>
<td><p>14,288</p></td>
<td><p><a href="../Page/森莫諾隆.md" title="wikilink">森莫諾隆Sen</a> Monorom</p></td>
<td><p>7,032</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金邊市.md" title="wikilink">金邊市</a>（, Phnom Penh）</p></td>
<td><p>15</p></td>
<td><p>1,313,900</p></td>
<td><p>290</p></td>
<td><p><a href="../Page/金邊市.md" title="wikilink">金邊市Phnom</a> Penh</p></td>
<td><p>570,155</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柏威夏省.md" title="wikilink">柏威夏省</a>（, Preah Vihear）</p></td>
<td><p>17</p></td>
<td><p>152,400</p></td>
<td><p>13,788</p></td>
<td><p><a href="../Page/特崩棉則.md" title="wikilink">特崩棉則Phnom</a> Tbeng Meanchey</p></td>
<td><p>21,580</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波羅勉省.md" title="wikilink">波羅勉省</a>（, Prey Veng）</p></td>
<td><p>19</p></td>
<td><p>1,044,400</p></td>
<td><p>4,883</p></td>
<td><p><a href="../Page/波羅勉.md" title="wikilink">波羅勉Prey</a> Veng</p></td>
<td><p>55,054</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/菩薩省.md" title="wikilink">菩薩省</a>（, Pursat）</p></td>
<td><p>18</p></td>
<td><p>428,200</p></td>
<td><p>12,692</p></td>
<td><p>菩薩Poŭthĭsăt</p></td>
<td><p>57,523</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉達那基里省.md" title="wikilink">拉達那基里省</a>（, Ratanakiri）</p></td>
<td><p>20</p></td>
<td><p>121,000</p></td>
<td><p>10,782</p></td>
<td><p>隆發Lumphăt</p></td>
<td><p>16,999</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暹粒省.md" title="wikilink">暹粒省</a>（, Siem Reap）</p></td>
<td><p>21</p></td>
<td><p>861,200</p></td>
<td><p>10,299</p></td>
<td><p>暹粒Siĕm Reap</p></td>
<td><p>119,528</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西哈努克省.md" title="wikilink">西哈努克省</a>（, Krŏng Preah Sihanouk）</p></td>
<td><p>16</p></td>
<td><p>209,000</p></td>
<td><p>868</p></td>
<td><p><a href="../Page/西哈努克市.md" title="wikilink">西哈努克市Krŏng</a> Preah Sihanouk</p></td>
<td><p>155,690</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上丁省.md" title="wikilink">上丁省</a>（, Stung Treng）</p></td>
<td><p>22</p></td>
<td><p>103,900</p></td>
<td><p>11,092</p></td>
<td><p>上丁Stŏeng Treng</p></td>
<td><p>24,493</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柴楨省.md" title="wikilink">柴楨省</a>（, Svay Rieng）</p></td>
<td><p>23</p></td>
<td><p>538,200</p></td>
<td><p>2,966</p></td>
<td><p>柴楨Svay Riĕng</p></td>
<td><p>21,205</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/茶膠省.md" title="wikilink">茶膠省</a>（, Takéo）</p></td>
<td><p>24</p></td>
<td><p>900,900</p></td>
<td><p>3,563</p></td>
<td><p>茶膠Takev</p></td>
<td><p>39,186</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥多棉芷省.md" title="wikilink">奥多棉芷省</a>（, Oddar Meancheay）</p></td>
<td><p>13</p></td>
<td><p>97,800</p></td>
<td><p>6,158</p></td>
<td><p>三隆Samraong</p></td>
<td><p>22,361</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白馬市(省級行政區).md" title="wikilink">白馬市(省級行政區)</a>（, Krŏng Kep）</p></td>
<td><p>10</p></td>
<td><p>37,800</p></td>
<td><p>336</p></td>
<td><p><a href="../Page/白馬市(省級行政區).md" title="wikilink">白馬市(省級行政區)Krŏng</a> Keb</p></td>
<td><p>10,319</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拜林市(省級行政區).md" title="wikilink">拜林市(省級行政區)</a>（, Krŏng Paĭlin）</p></td>
<td><p>14</p></td>
<td><p>32,700</p></td>
<td><p>803</p></td>
<td><p><a href="../Page/拜林市(省級行政區).md" title="wikilink">拜林市(省級行政區)Krŏng</a> Paĭlin</p></td>
<td><p>8,510</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/特本克蒙省.md" title="wikilink">特本克蒙省</a>（, Tbong Khmum）</p></td>
<td><p>25</p></td>
<td><p>754,000</p></td>
<td><p>4,928</p></td>
<td><p><a href="../Page/三州府市.md" title="wikilink">三州府市Suong</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 國際排名

| 組織                                         | 調查                                                 | 排名           | 評分     |
| ------------------------------------------ | -------------------------------------------------- | ------------ | ------ |
| [世界銀行集團](../Page/世界銀行集團.md "wikilink")     | [各國經商容易度列表](../Page/各國經商容易度列表.md "wikilink")（2012） | 183個國家中的138名 | 75.4%  |
| [透明國際](../Page/透明國際.md "wikilink")         | [貪污感知指數](../Page/貪污感知指數.md "wikilink")（2012）       | 184個國家中的164名 | 89.13% |
| [聯合國開發計劃署](../Page/聯合國開發計劃署.md "wikilink") | [人類發展指數](../Page/人類發展指數.md "wikilink")（2012）       | 184個國家中的139名 | 75.5%  |
| [世界黃金協會](../Page/世界黃金協會.md "wikilink")     | [黃金儲備](../Page/黃金儲備.md "wikilink")（2010）           | 110個國家中的65名  | 60%    |
| [無國界記者](../Page/無國界記者.md "wikilink")       | [新聞自由指數](../Page/新聞自由指數.md "wikilink")（2012）       | 179個國家中的117名 | 65.3%  |
| [美國傳統基金會](../Page/美國傳統基金會.md "wikilink")   | [經濟自由度指數](../Page/經濟自由度指數.md "wikilink")（2012）     | 179個國家中的102名 | 57%    |
| [全球競爭力報告](../Page/全球競爭力報告.md "wikilink")   | [世界經濟論壇](../Page/世界經濟論壇.md "wikilink")（2012）       | 142個國家中的97名  | 68.3%  |
| [全球和平指數](../Page/全球和平指數.md "wikilink")     | [学院经济学与和平指数](../Page/全球和平指数.md "wikilink")（2012）   | 142個國家中的108名 | 68.3%  |
| [聯合國](../Page/聯合國.md "wikilink")           | [教育指數](../Page/教育指數.md "wikilink")（2012）           | 179個國家中的132名 | 73.7%  |

## 经济

[Phnom_Penh_French_Colonial.jpg](https://zh.wikipedia.org/wiki/File:Phnom_Penh_French_Colonial.jpg "fig:Phnom_Penh_French_Colonial.jpg")
[Kraing_Tbong_VB_01_06_6.JPG](https://zh.wikipedia.org/wiki/File:Kraing_Tbong_VB_01_06_6.JPG "fig:Kraing_Tbong_VB_01_06_6.JPG")
[Fishsale.jpg](https://zh.wikipedia.org/wiki/File:Fishsale.jpg "fig:Fishsale.jpg")

柬埔寨经济的四大支柱为[旅游业](../Page/旅游.md "wikilink")、加工业（制衣业）、建筑业和[农业](../Page/农业.md "wikilink")。作為一个[新興發展國家](../Page/新興發展國家.md "wikilink")，由於多年來受到戰爭等因素影響，柬埔寨的經濟发展相對比較缓慢，每年經濟增長約達6%。近年來，隨著柬埔寨大力推動旅遊業，已取得有效的成績，並隨著政局稳定，成功吸引中國、韩国、日本等國家前往投資進駐，成為東南亞的新兴投資點之一。

柬埔寨的貨幣稱為[瑞爾](../Page/柬埔寨瑞爾.md "wikilink")，但同時流通[美元](../Page/美元.md "wikilink")，許多商店均以美金計價。在一般買賣，美金與瑞爾可同時使用，大致為1美元＝4,000至4,200瑞爾。

國內的貧富懸殊問題、基礎設施不足、健康衛生問題和人民缺乏知識水平等社會問題十分嚴重，是為柬國發展經濟的同時，所遇到的嚴峻考驗。

2006年，柬埔寨的外籍旅客人数逾170万\[6\]。2005年，柬埔寨领海内发现[石油和](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")，商业开发预计于2009年或2010年开始，将对柬埔寨经济产生重要影响。\[7\]

2008年，柬埔寨外贸总额达114.7亿[美元](../Page/美元.md "wikilink")，出口总额为48.09亿美元，[进口总额为](../Page/进口.md "wikilink")66.61亿美元，其中[服装出口总额为](../Page/服装.md "wikilink")27.8亿美元，主要市场为[欧洲](../Page/欧洲.md "wikilink")、[美国和](../Page/美国.md "wikilink")[日本等地](../Page/日本.md "wikilink")。

2012年4月18日上午9时09分，随着柬埔寨首相[洪森在电视讲话中](../Page/洪森.md "wikilink")，敲响股市开市鐘，是柬埔寨股市历史上，首个交易日。开市仅有[金邊水務局一支股票上市](../Page/金邊水務局.md "wikilink")，其募股總數為1304.6萬股，佔總股本的15%，融資總額約在2000萬美元左右。

| 年份   | 收入(美元)   |
| ---- | -------- |
| 2009 | 457.568  |
| 2010 | 561.514  |
| 2011 | 584.862  |
| 2012 | 678.358  |
| 2013 | 858.203  |
| 2014 | 983.774  |
| 2015 | 1093.465 |
| 2016 | 1228.506 |
| 2017 | 1434     |
| 2018 | 1560     |

人均收入



## 社会

柬埔寨擁有特別的[高棉文化](../Page/高棉文化.md "wikilink")，較接近[泰國](../Page/泰國.md "wikilink")，並帶有濃厚的[上座部佛教文化氛圍](../Page/上座部佛教.md "wikilink")，可從高棉建築文化看到柬埔寨从古至今也受印度文化影响。

### 人口

柬埔寨[人口超過](../Page/人口.md "wikilink")1550萬人，其中90%左右為[高棉族](../Page/高棉族.md "wikilink")，而少數民族則包括1%的[華人](../Page/柬埔寨華人.md "wikilink")、9%的[京族](../Page/京族.md "wikilink")、[佬族](../Page/佬族.md "wikilink")、[泰族](../Page/泰族.md "wikilink")、[占族等](../Page/占族.md "wikilink")20餘部，當中有一小部分屬於深居山地的[原住民部落](../Page/原住民.md "wikilink")。柬埔寨的人均壽命目前是71.41歲。

#### 城市人口

西哈努克省 |pop_7 = 89,447 | city_8 = 磅士卑 | div_8 = 磅士卑省 |pop_8 = 54,505
| city_9 = 贡布 | div_9 = 贡布省 |pop_9 = 48,274 | city_10 = 磅清扬 |
div_10 = 磅清扬省 |pop_10 = 43,130 }}

### 語言

大多數的柬埔寨國民使用[高棉語和](../Page/高棉語.md "wikilink")[高棉文字](../Page/高棉文字.md "wikilink")。高棉語且有自己的[高棉數字](../Page/高棉數字.md "wikilink")，在柬埔寨使用高棉數字比[阿拉伯數字更流通](../Page/阿拉伯數字.md "wikilink")；[華人主要使用](../Page/柬埔寨華人.md "wikilink")[潮州話等漢語方言](../Page/潮州話.md "wikilink")，[占族人主要使用](../Page/占族.md "wikilink")[占語](../Page/占語.md "wikilink")，越南人则以越南语为主，其他各民族有各自的语言，由於[法國曾長期殖民柬埔寨](../Page/法國.md "wikilink")，[法语曾經是继高棉語之後的第二大語言](../Page/法语.md "wikilink")，如今已不普遍。僅有少數人尚通曉法語以及视其為[母语](../Page/母语.md "wikilink")。近年因经济进步，[漢語普通話](../Page/漢語.md "wikilink")、[英语等在柬国很受欢迎](../Page/英语.md "wikilink")。

### 交通

柬埔寨由於多年來受到戰爭影響，所以交通並不完善，直至近数十年再度重建，才逐渐得到改善。道路建設主要集中在首都一帶。公路方面，共有7條国道，以首都[金邊为中心](../Page/金邊.md "wikilink")，向全國各地伸展，不過大多不平坦。鐵路有2条路线，主要由金邊向西方及南方伸向，全612公里，2009年起停止營運。現在已重新營運金邊-[西哈努克港的綫路](../Page/西哈努克.md "wikilink")，
但是由於地形關係及科技未完善，整段路程都只能於30-40km/r行駛，以致需時7小時才能駛畢全程，並且只有限度地於星期五至日提供服務。现在尚在興建多条路线；柬埔寨尚未規劃城市地铁、高速铁路。国民主要以三輪摩托車、摩托車代步，另有長短程的巴士在各大小城镇穿梭。富裕人士則以進口車代步，且因价格较高，尚未普及。旅客則大多搭乘計程車及長短途巴士旅行。

柬埔寨主要有2處國際機場，分別是首都的[金邊國際機場以及著名景點吳哥窟附近的](../Page/金邊國際機場.md "wikilink")[暹粒－吴哥国际机场](../Page/暹粒－吴哥国际机场.md "wikilink")，另有多個國內規模较小的機場。

柬埔寨的汽車以自[日本及](../Page/日本.md "wikilink")[南韓進口的二手車為主](../Page/南韓.md "wikilink")。

### 宗教

柬埔寨将[佛教定为](../Page/佛教.md "wikilink")[国教](../Page/国教.md "wikilink")，目前有超过95%的人信仰，当中以高棉族人為最多，而他們當中大多信奉[上座部佛教](../Page/上座部佛教.md "wikilink")，與鄰國[泰國相同](../Page/泰國.md "wikilink")。上座部佛教自11世紀由西南方的[斯里兰卡傳入柬埔寨](../Page/斯里兰卡.md "wikilink")，並動搖了原本盛行的[婆羅門教的信仰地位](../Page/婆羅門教.md "wikilink")，直至14世紀取代[濕婆與](../Page/濕婆.md "wikilink")[大乘佛教後](../Page/大乘佛教.md "wikilink")，逐漸成為柬埔寨的新的主要信仰。此外，當地[京族和](../Page/京族.md "wikilink")[華族以信奉](../Page/华人.md "wikilink")[大乘佛教](../Page/大乘佛教.md "wikilink")、[天主教和](../Page/天主教.md "wikilink")[基督新教等居多](../Page/基督新教.md "wikilink")，而[占族则以信奉](../Page/占族.md "wikilink")[伊斯蘭教为主](../Page/伊斯蘭教.md "wikilink")，另有小部分人為原始[萬物有靈崇拜](../Page/萬物有靈.md "wikilink")。
[BuddhistInstitute_Phnom_Penh_2005_1.JPG](https://zh.wikipedia.org/wiki/File:BuddhistInstitute_Phnom_Penh_2005_1.JPG "fig:BuddhistInstitute_Phnom_Penh_2005_1.JPG")

### 飲食

柬埔寨擁有特別的高棉菜，口味较偏甜及较辛辣，该国出产胡椒、鱼露。用上大量香料、辣椒、胡椒和鱼露等烹調，口味与泰菜接近。由於受到法国殖民统治，所以菜肴有混入了[法国的元素](../Page/法国.md "wikilink")，也受到[中國和](../Page/中國.md "wikilink")[越南等地的的口味影响](../Page/越南.md "wikilink")。

### 艺术

柬埔寨舞蹈有三大类，包括：高棉古典舞、民间舞和社交舞。而最代表柬埔寨的舞蹈是高棉古典舞。

### 旅遊

各地有不同的特色，在柬埔寨北部的[暹粒市近郊有舉世聞名的世界遗产](../Page/暹粒市.md "wikilink")[吳哥窟](../Page/吳哥窟.md "wikilink")。在中部的[金邊有大量的佛教寺廟](../Page/金邊.md "wikilink")，西部有東南亞最大的淡水湖[洞里薩湖可搭船遊覽湖面上的](../Page/洞里薩湖.md "wikilink")[水上人家](../Page/水上人家.md "wikilink")，南部的[西哈努克市可享受陽光與海灘](../Page/西哈努克市.md "wikilink")。柬埔寨有3處聯合國所列入的國家文化遺產：

  - [吳哥古蹟](../Page/吳哥古蹟.md "wikilink")
  - [柏威夏寺](../Page/柏威夏寺.md "wikilink")
  - [三波布雷科](../Page/三波布雷科.md "wikilink")

[Samraong.jpg](https://zh.wikipedia.org/wiki/File:Samraong.jpg "fig:Samraong.jpg")屬於最落後地區\]\][Cambo_169.jpg](https://zh.wikipedia.org/wiki/File:Cambo_169.jpg "fig:Cambo_169.jpg")
[Apsara_dance.jpg](https://zh.wikipedia.org/wiki/File:Apsara_dance.jpg "fig:Apsara_dance.jpg")

### 媒体

#### 广播电台

  - [柬埔寨国家广播电台](../Page/柬埔寨国家电视台.md "wikilink")（AM 918、FM 105.7）
  - 柬埔寨皇家军队广播电台（FM 98.0）
  - Sweet电台（FM 88.0）
  - 仙女电台（FM 97.0）
  - 家庭电台（FM 99.5）
  - 金边广播电台（FM 103.0）
  - Hang Meas电台（FM 104.5）
  - [蜂巢电台](../Page/蜂巢电台.md "wikilink")（FM 105.0）
  - 高棉广播电台（FM 107.0）

#### 電視台

**免費台**

  - [金邊三台TV3](http://khmertv.co/channel/tv3.html)
  - [金邊亞洲台](../Page/金邊亞洲台.md "wikilink")
  - [東南亞電視台](http://khmertv.co/channel/seatv.html)
  - [金邊無線電視台CTV8頻道](../Page/金邊無線電視台CTV8頻道.md "wikilink")
  - [柬埔寨國家電視台](../Page/柬埔寨國家電視台.md "wikilink")
  - [CTN電視台](http://khmertv.co/channel/ctn.html)
  - [CNC新聞台](http://khmertv.co/channel/cnc.html)
  - [MY TV娛樂台](http://khmertv.co/channel/mytv.html)
  - [BTV新闻台](http://khmertv.co/channel/btv-news.html)
  - [ETV娱乐台](http://khmertv.co/channel/etv.html)
  - [高棉高清台TV9](http://khmertv.co/channel/ctv9.html)
  - [金鳳高清台](http://khmertv.co/channel/hang-meas-hdtv.html)
  - [金鳳之光高清台](http://khmertv.co/channel/rhm.html)
  - [巴戎電視台](http://khmertv.co/channel/bayon-tv.html)
  - [仙女電視台](http://khmertv.co/channel/apsara-tv.html)
  - [軍隊台TV5](http://khmertv.co/channel/tv5.html)
  - [PNN電視台](http://khmertv.co/channel/pnn.html)

**收費台**

  - [金邊無線電視台 PPCTV](../Page/金邊無線電視台_PPCTV.md "wikilink")
  - [柬埔寨無線電視台 CCTV](../Page/柬埔寨無線電視台_CCTV.md "wikilink")

#### 纸媒

  - 華文報紙

<!-- end list -->

  - [華商日報](../Page/華商日報.md "wikilink")，當地華人財團主辦，偏重財經資訊報導，柬埔寨历史最悠久的華報，在柬埔寨华人圈享有较高知名度。在新媒体方面积极探索，率先推出全柬第一家微信平台。目前粉丝已过万。
  - [柬華日報](../Page/柬華日報.md "wikilink")，柬埔寨最大華人社團「[柬華理事總會](../Page/柬華理事總會.md "wikilink")」的機關報，於2000年8月10日創刊。经理：鍾耀輝、總編輯：安佳。发行全柬，销量已經超過6000份。
  - [*星洲日報*](http://www.sinchew-i.com/cambodia/)，是馬來西亞[星洲集團在柬埔寨開辦的子報](../Page/星洲集團.md "wikilink")，偏重民生，銷量已經超過5000份。
  - [高棉日报](../Page/高棉日报.md "wikilink")，柬埔寨新闻最全面的中文报。
  - [吴哥时報柬埔寨唯一没有发行报纸的媒體](../Page/吴哥时報.md "wikilink")，也是柬埔寨唯一只通过微信平台的媒體，
  - [国公时報](https://www.facebook.com/pages/%E5%9B%BD%E5%85%AC%E6%97%B6%E6%8A%A5-Koh-Kong-Time/1622685591334265?ref=bookmarks)柬埔寨也是唯一没有发行报纸的媒體，也是柬埔寨唯一只通过Facebook专页平台的纷丝与转载中柬文媒體，
  - [金边晚报是由柬埔寨中国商会主办的华文媒體](../Page/金边晚报.md "wikilink")，

<!-- end list -->

  - 高棉文報紙

<!-- end list -->

  - *[Sralagn'高棉](http://cn.thekhmerdaily.com/)*。
  - *[Chakraval日报](../Page/Chakraval日报.md "wikilink")*。
  - *[柬埔寨特梅日报](../Page/柬埔寨特梅日报.md "wikilink")*。
  - *柬埔寨Thnai内斯*（*柬埔寨今天*）。
  - *[Kanychok Sangkhum](../Page/Kanychok_Sangkhum.md "wikilink")*。
  - [*Ka-set*](http://ka-set.info/kh/khmer.html)。
  - *[Koh Santepheap](../Page/Koh_Santepheap.md "wikilink")*（*和平岛*）。
  - *[Moneaksekar Khmer](../Page/Moneaksekar_Khmer.md "wikilink")*
    (*高棉良知*) - 发布[桑兰西党](../Page/桑兰西.md "wikilink")。
  - *[Rasmei
    Kampuchea](../Page/Rasmei_Kampuchea.md "wikilink")*（*柬埔寨之光*）
    - 柬埔寨最大的日报，它循环约18000副本。
  - *[Samleng
    Yuvachun](../Page/Samleng_Yuvachun.md "wikilink")*（*高棉青年之声*）。
  - *[Udomkate Khmer](../Page/Udomkate_Khmer.md "wikilink")*（*高棉的理想选择*）。
  - *[Wat Phnom Daily](../Page/Wat_Phnom_Daily.md "wikilink")*

<!-- end list -->

  - 英文報紙

<!-- end list -->

  - ''金边邮报 ''，每两周在金边发表的[英文报纸](../Page/英文.md "wikilink")。
  - *柬埔寨日报*，柬埔寨唯一的[英文日报报纸](../Page/英文.md "wikilink")。

<!-- end list -->

  - 法文報紙

<!-- end list -->

  - [*柬埔寨晚报*](http://www.cambodgesoir.info/)
  - [*家集*](http://ka-set.info/)

## 相关条目

  - [柬埔寨王國 (1953年－1970年)](../Page/柬埔寨王國_\(1953年－1970年\).md "wikilink")
  - [吳哥古跡](../Page/吳哥古跡.md "wikilink")
  - [柬埔寨內戰](../Page/柬埔寨內戰.md "wikilink")
  - [柬埔寨軍事](../Page/柬埔寨軍事.md "wikilink")

## 参考文献

## 外部链接

  - [柬埔寨王國 - Cambodia
    e-Gov](https://web.archive.org/web/20061005044434/http://www.cambodia.gov.kh/unisql1/egov/english/home.view.html)

  - [柬埔寨电子签证 - Cambodia e-Visa](https://www.evisa.gov.kh/?lang=ChiS)

  - [柬埔寨旅遊部官方網站](http://www.tourismcambodia.org/index.php?lang=zh-tw)

  - [柬埔寨旅游指南](http://www.southeastasiatrip.com/cambodia/)

  -
  -
  -
  -
  - [維客旅行上的](../Page/維客旅行.md "wikilink")[柬埔寨](http://wikitravel.org/zh/柬埔寨)

  -
  - \[//maps.google.com/maps?q=Cambodia 谷歌地圖\]

{{-}}



[\*](../Category/柬埔寨.md "wikilink") [C](../Category/王國.md "wikilink")
[Cambodia](../Category/君主立憲國.md "wikilink")

1.
2.  [柬埔寨人民党赢得参议院全部议席](http://sh.qihoo.com/pc/93229e41adb7d3040?sign=360_e39369d1)
3.
4.
5.
6.  [1.7
    million](http://english.people.com.cn/200701/03/eng20070103_337920.html)
7.  [广西对柬埔寨投资呈爆发式增长](http://news.xinhuanet.com/local/2016-05/23/c_129006092.htm)news.xinhuanet.com