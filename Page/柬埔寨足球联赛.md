{{ infobox football league | name = 柬埔寨足球聯賽 | another_name = Cambodian
League | image = | pixels = | country =  | confed =
[AFC](../Page/亞洲足球聯合總會.md "wikilink")（[亞洲](../Page/亞洲.md "wikilink")）
| founded = 1982年 | first = | teams = 12 隊 | relegation =
[柬埔寨乙組足球聯賽](../Page/柬埔寨乙組足球聯賽.md "wikilink") | level =
第 1 級 | domestic_cup = [柬埔寨國王盃](../Page/柬埔寨國王盃.md "wikilink")
[洪森盃](../Page/洪森盃.md "wikilink")
[CNCC盃](../Page/CNCC盃.md "wikilink") | international_cup =
[亞洲足協盃](../Page/亞洲足協盃.md "wikilink") | season = 2018 |
champions = [金界娛乐城](../Page/金界娛乐城足球會.md "wikilink") |
most_successful_club = [金邊皇冠](../Page/金邊皇冠足球會.md "wikilink") |
no_of_titles = 6 次 | website = [官方網站](http://www.cambodialeague.com)
}}

**柬埔寨足球聯賽**（**Cambodian
League**）是[柬埔寨最高级别的足球聯賽](../Page/柬埔寨.md "wikilink")，創辦於1982年，由[柬埔寨足球協會主辦](../Page/柬埔寨足球協會.md "wikilink")，一共有
12 隊角逐。

## 參賽球隊

2018年柬埔寨足球聯賽參賽隊伍共有 12 支。

| 中文名稱                                    | 英文名稱                          | 所在地                                | 上季成績     |
| --------------------------------------- | ----------------------------- | ---------------------------------- | -------- |
| [萬吉吳哥](../Page/萬吉吳哥足球會.md "wikilink")   | Boeung Ket Angkor FC          | [磅湛省](../Page/磅湛省.md "wikilink")   | 第 1 位    |
| [柴楨](../Page/柴楨足球會.md "wikilink")       | Svay Rieng FC                 | [柴楨省](../Page/柴楨省.md "wikilink")   | 第 2 位    |
| [金界](../Page/金界足球會.md "wikilink")       | Nagaworld FC                  | [磅士卑省](../Page/磅士卑省.md "wikilink") | 第 3 位    |
| [國家防衛軍](../Page/國家防衛軍足球會.md "wikilink") | National Defense Ministry FC  | [金邊](../Page/金邊.md "wikilink")     | 第 4 位    |
| [金邊皇冠](../Page/金邊皇冠足球會.md "wikilink")   | Phnom Penh Crown FC           | [金邊](../Page/金邊.md "wikilink")     | 第 5 位    |
| [國家警察隊](../Page/國家警察隊足球會.md "wikilink") | National Police Commissary FC | [金邊](../Page/金邊.md "wikilink")     | 第 6 位    |
| [吳哥老虎](../Page/吳哥老虎足球會.md "wikilink")   | Angkor Tiger FC               | [暹粒省](../Page/暹粒省.md "wikilink")   | 第 7 位    |
| [西部金邊](../Page/西部金邊足球會.md "wikilink")   | Western Phnom Penh FC         | [金邊](../Page/金邊.md "wikilink")     | 第 8 位    |
| [柬埔寨電力](../Page/柬埔寨電力足球會.md "wikilink") | Electricite du Cambodge FC    | [金邊](../Page/金邊.md "wikilink")     | 第 9 位    |
| [歐亞聯](../Page/歐亞聯足球會.md "wikilink")     | Asia Euro United              | [干丹省](../Page/干丹省.md "wikilink")   | 第 10 位   |
| [維莎卡](../Page/維莎卡足球會.md "wikilink")     | Visakha FC                    | [金邊](../Page/金邊.md "wikilink")     | 乙組，第 1 位 |
| [索迪圖吳哥](../Page/索迪圖吳哥足球會.md "wikilink") | Soltilo Angkor FC             | [暹粒省](../Page/暹粒省.md "wikilink")   | 乙組，第 2 位 |

## 历届冠军

  - 1982年：柬埔寨商业部足球队
  - 1983年：柬埔寨商业部足球队
  - 1984年：柬埔寨商业部足球队
  - 1985年：柬埔寨国防部足球队
  - 1986年：柬埔寨国防部足球队
  - 1987年：柬埔寨卫生部足球队
  - 1988年：磅湛省足球俱乐部
  - 1989年：柬埔寨交通部足球队
  - 1990年：柬埔寨交通部足球队
  - 1991年：柬埔寨市政建设部足球队
  - 1992年：柬埔寨市政建设部足球队
  - 1993年：柬埔寨国防部足球队
  - 1994年：柬埔寨民用航空足球队
  - 1995年：柬埔寨民用航空足球队
  - 1996年：护卫俱乐部
  - 1997年：护卫俱乐部
  - 1998年：皇家海豚足球俱乐部
  - 1999年：皇家海豚足球俱乐部
  - 2000年：柬埔寨国家警察足球俱乐部 (Nokorbal Cheat)
  - 2001年：*未举办*
  - 2002年：[萨马联](../Page/Hello足球俱乐部.md "wikilink")
  - 2003年：*未举办*
  - 2004年：*未举办*
  - 2005年：[金边卡玛拉足球俱乐部](../Page/金边卡玛拉足球俱乐部.md "wikilink")
  - 2006年：[金边卡玛拉足球俱乐部](../Page/金边卡玛拉足球俱乐部.md "wikilink")
  - 2017年：[万吉吳哥足球俱乐部](../Page/万吉吳哥足球俱乐部.md "wikilink")

[亚](../Category/國家頂級足球聯賽.md "wikilink")
[Cambodia](../Category/亞洲各國足球聯賽.md "wikilink")
[Category:亞洲頂級足球聯賽](../Category/亞洲頂級足球聯賽.md "wikilink")