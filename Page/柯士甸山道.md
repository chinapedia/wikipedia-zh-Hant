**柯士甸山道**（）是[香港島](../Page/香港島.md "wikilink")[太平山山頂上位於最高的一條車路](../Page/太平山_\(香港\).md "wikilink")，單線雙向行車，上山斜坡路。柯士甸山道街口在[爐峰峽](../Page/爐峰峽.md "wikilink")[凌霄閣以西](../Page/凌霄閣.md "wikilink")，沿途經過[柯士甸山遊樂場](../Page/柯士甸山遊樂場.md "wikilink")、[舊總督山頂別墅守衛室及](../Page/舊總督山頂別墅守衛室.md "wikilink")[山頂公園](../Page/山頂公園.md "wikilink")，止於[無線電站](../Page/無線電.md "wikilink")。柯士甸山道沿途有住宅[岫雲](../Page/岫雲.md "wikilink")、山景花園別墅、[柯士甸山道8號](../Page/柯士甸山道8號.md "wikilink")、山頂花園、[耕耘草廬及豐林閣](../Page/耕耘草廬.md "wikilink")。

## 特色

在柯士甸山道8號北面，往[盧吉道](../Page/盧吉道.md "wikilink")26C路口，有一風景點，遊人多在此下車拍照。此處可以遠眺[灣仔](../Page/灣仔.md "wikilink")、[銅鑼灣和](../Page/銅鑼灣.md "wikilink")[北角的樓景](../Page/北角.md "wikilink")，包括[合和中心](../Page/合和中心.md "wikilink")、[中環廣場及](../Page/中環廣場.md "wikilink")[維多利亞東海峽](../Page/維多利亞.md "wikilink")，及[九龍半島](../Page/九龍半島.md "wikilink")[尖沙咀及](../Page/尖沙咀.md "wikilink")[紅磡](../Page/紅磡.md "wikilink")。

<File:HK> Peak Mt Austin Road pavilion view.JPG|柯士甸山道中段觀景亭所見之風景 (2006年)
<File:Mount> Austin Road Viewing Pavilion 2016.jpg|柯士甸山道8號的觀景亭
<File:Mount> Austin Road View1 2016.jpg|柯士甸山道近柯士甸山道22號 <File:Mount>
Austin Road View2 201607.jpg|柯士甸山道近[山頂公園](../Page/山頂公園.md "wikilink")

## 相關

  - [同樂徑](../Page/同樂徑.md "wikilink")
  - [夏力道](../Page/夏力道.md "wikilink")

## 外部連結

  - [柯士甸山道地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=833284&cy=815067&zm=5&mx=833284&my=815067&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[Category:太平山街道](../Category/太平山街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")