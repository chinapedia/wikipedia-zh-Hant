**柯清輝**（英文名：**Raymond
Or**）（），[福建](../Page/福建.md "wikilink")[廈門出生](../Page/廈門.md "wikilink")，[香港](../Page/香港.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")[思捷環球](../Page/思捷環球.md "wikilink")()執行主席兼[執行董事](../Page/執行董事.md "wikilink")\[1\]\[2\]，國際資源控股()前[副主席](../Page/副主席.md "wikilink")\[3\]，直至2017年5月26日退任\[4\]，中策集團()非執行主席、[非執行董事及前](../Page/非執行董事.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")\[5\]，[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")()獨立[非執行董事](../Page/非執行董事.md "wikilink")\[6\]，[恆生銀行前副董事長兼行政總裁](../Page/恆生銀行.md "wikilink")。

## 生平

柯清輝7歲來[香港](../Page/香港.md "wikilink")，就讀於[北角的](../Page/北角.md "wikilink")[蘇浙小學](../Page/蘇浙小學.md "wikilink")，中學時期就讀於[英皇書院](../Page/英皇書院.md "wikilink")，其後於[香港大學畢業](../Page/香港大學.md "wikilink")，取得[經濟及](../Page/經濟.md "wikilink")[心理學學士銜](../Page/心理學.md "wikilink")。1972年加入[香港上海滙豐銀行](../Page/香港上海滙豐銀行.md "wikilink")，任見習行政人員，曾任職於人事部、[證券部及](../Page/證券.md "wikilink")[零售銀行部](../Page/零售銀行.md "wikilink")，1980年出任信貸經理，1995年升任副總經理，2000年2月1日獲升任為總經理，2005年1月1日成為香港上海滙豐銀行執行董事。2005年5月25日，被滙豐調任為恒生銀行副董事長兼行政總裁。柯清輝亦曾任[香港銀行公會主席](../Page/香港銀行公會.md "wikilink")。政府於2009年7月1日公佈柯清輝獲頒授銀紫荊星章。\[7\]

## 家庭

柯清輝已婚，有四個子女，喜好與家人去[旅行](../Page/旅行.md "wikilink")。

## 參考

<references />

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:汇丰人物](../Category/汇丰人物.md "wikilink")
[Category:恒生银行人物](../Category/恒生银行人物.md "wikilink")
[Category:蘇浙小學校友](../Category/蘇浙小學校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[Category:厦门人](../Category/厦门人.md "wikilink")
[Category:香港銀行家](../Category/香港銀行家.md "wikilink")
[Category:香港城市大學榮譽博士](../Category/香港城市大學榮譽博士.md "wikilink")
[Category:電視廣播有限公司人物](../Category/電視廣播有限公司人物.md "wikilink")
[C](../Category/柯姓.md "wikilink")

1.
2.
3.  [國際資源\<01051.HK\>高層人士變動，柯清輝任副主席](http://finance.sina.com.hk/cgi-bin/nw/show.cgi/4739/2/1/1741238/1.html)
4.  [國際資源集團有限公司獨立非執行董事之退任 及
    撤回將於股東週年大會上提呈之第 2(ii)項普通決議案公告](http://www.hkexnews.hk/listedco/listconews/sehk/2017/0529/LTN20170529551_C.pdf)
5.  [伙博智購南壽
    中策佔股八成](http://finance.sina.com.hk/cgi-bin/nw/show.cgi/4/2/1/2072234/1.html)
6.  [[電視廣播有限公司委任獨立](../Page/電視廣播有限公司.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")](http://i1.web.vip.hk1.tvb.com/www.tvb.com/affairs/faq/corporate/pdf/announcement/c-Announcement_20121205.pdf)
7.  [二○○九年授勳名單](http://www.info.gov.hk/gia/general/200907/01/P200906300300.htm)