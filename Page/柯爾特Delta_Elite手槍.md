**柯爾特Delta
Elite**（）是以[M1911A1改進而成的](../Page/M1911手槍.md "wikilink")[10毫米Auto口徑](../Page/10毫米Auto.md "wikilink")[手槍](../Page/手槍.md "wikilink")。

## 設計

Delta
Elite以M1911系列改進而成，因些它的內部結構及運作方式非常相似，[不鏽鋼制造及](../Page/不鏽鋼.md "wikilink")處理，彈匣可裝8發子彈，它的版本可作射擊比賽用途，現在全部Delta系列已停產。

## 使用國

  - ：被一些州份的警察部門所採用。

## 参见

  - [布倫十式半自動手槍](../Page/布倫十式半自動手槍.md "wikilink")—第一種[10毫米Auto口徑手槍](../Page/10毫米Auto.md "wikilink")
  - [格洛克20](../Page/格洛克20.md "wikilink")—另一種10毫米Auto口徑手槍
  - [M1911A1](../Page/M1911手槍.md "wikilink")—Delta Elite的原型
  - [STI 5.0戰術型](../Page/STI_5.0戰術型手槍.md "wikilink")

## 外部链接

  - [Delta
    Elite官方使用手冊（PDF格式）](https://web.archive.org/web/20090320152658/http://www.coltsmfg.com/cmci/downloads/Manuals/LoRes/MKIV%20Series%2080%20%26%2090%20Pistols.pdf)
  - [Delta Gold
    Cup官方使用手冊（PDF格式）](https://web.archive.org/web/20070928024756/http://www.coltsmfg.com/cmci/downloads/Manuals/LoRes/MKIV%20Series%2080%20Pistols.pdf)
  - [槍炮世界—柯尔特三角精英手枪](http://firearmsworld.net/usa/colt/delta/delta.htm)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:美國半自動手槍](../Category/美國半自動手槍.md "wikilink")
[Category:M1911衍生型](../Category/M1911衍生型.md "wikilink")
[Category:10毫米Auto槍械](../Category/10毫米Auto槍械.md "wikilink")