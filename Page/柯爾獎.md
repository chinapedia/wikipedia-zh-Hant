**柯爾獎**（**Cole Prize** ，全名：**Frank Nelson Cole
Prize**），由[美國數學學會授奖](../Page/美國數學學會.md "wikilink")。分別有**數論獎**（1931年開始）和**代數獎**（1928年開始），奖励[数论和](../Page/数论.md "wikilink")[代數领域的成果](../Page/代數.md "wikilink")。它是为纪念為學會服務了25年的[弗兰克·纳尔逊·柯尔教授而设立](../Page/弗兰克·纳尔逊·柯尔.md "wikilink")。

虽然這個獎并不是完全國際化的，但是它既頒給有卓越贡献的學會成員，也颁给在[美國期刊上發表了出色文章者](../Page/美國.md "wikilink")。

## 代數獎得獎者

  - 1928 [雷奥那德·尤金·迪克逊](../Page/雷奥那德·尤金·迪克逊.md "wikilink")
  - 1939 [A. Adrian Albert](../Page/A._Adrian_Albert.md "wikilink")
  - 1944 [奥斯卡·扎里斯基](../Page/奥斯卡·扎里斯基.md "wikilink")
  - 1949 [Richard Brauer](../Page/Richard_Brauer.md "wikilink")
  - 1954 [Harish-Chandra](../Page/Harish-Chandra.md "wikilink")
  - 1960 [塞爾日·蘭](../Page/塞爾日·蘭.md "wikilink")，[Maxwell A.
    Rosenlicht](../Page/Maxwell_A._Rosenlicht.md "wikilink")
  - 1965
    [瓦尔特·法伊特](../Page/瓦尔特·法伊特.md "wikilink")，[约翰·格里格斯·汤普森](../Page/约翰·格里格斯·汤普森.md "wikilink")
  - 1970 [John R.
    Stallings](../Page/John_R._Stallings.md "wikilink")，[Richard G.
    Swan](../Page/Richard_G._Swan.md "wikilink")
  - 1975 [Hyman
    Bass](../Page/Hyman_Bass.md "wikilink")，[丹尼尔·奎伦](../Page/丹尼尔·奎伦.md "wikilink")
  - 1980 [Michael
    Aschbacher](../Page/Michael_Aschbacher.md "wikilink")，[Melvin
    Hochster](../Page/Melvin_Hochster.md "wikilink")
  - 1985 [George Lusztig](../Page/George_Lusztig.md "wikilink")
  - 1990 [森重文](../Page/森重文.md "wikilink")
  - 1995 [米歇尔·雷诺](../Page/米歇尔·雷诺.md "wikilink")，[David
    Harbater](../Page/David_Harbater.md "wikilink")
  - 2000 [Andrei Suslin](../Page/Andrei_Suslin.md "wikilink")，[Aise
    Johan de Jong](../Page/Aise_Johan_de_Jong.md "wikilink")
  - 2003 [中島啓](../Page/中島啓.md "wikilink")
  - 2006 [János Kollár](../Page/János_Kollár.md "wikilink")
  - 2009 [Christopher Hacon](../Page/Christopher_Hacon.md "wikilink"),
    [James McKernan](../Page/James_McKernan.md "wikilink")
  - 2012 [亚历山大·麦克尔耶夫](../Page/亚历山大·麦克尔耶夫.md "wikilink")
  - 2015 [皮特·舒尔策](../Page/皮特·舒尔策.md "wikilink")
  - 2018 Robert Guralnick

## 數論獎得獎者

  - 1931 [H. S. Vandiver](../Page/H._S._Vandiver.md "wikilink")
  - 1941 [Claude Chevalley](../Page/Claude_Chevalley.md "wikilink")
  - 1946 [H. B. Mann](../Page/H._B._Mann.md "wikilink")
  - 1951 [保羅·艾狄胥](../Page/保羅·艾狄胥.md "wikilink")
  - 1956 [约翰·泰特](../Page/约翰·泰特.md "wikilink")
  - 1962
    [岩澤健吉](../Page/岩澤健吉.md "wikilink")，[伯纳德·M·德沃克](../Page/伯纳德·德沃克.md "wikilink")
  - 1967 [James B. Ax](../Page/James_B._Ax.md "wikilink")，[Simon B.
    Kochen](../Page/Simon_B._Kochen.md "wikilink")
  - 1972 [Wolfgang M.
    Schmidt](../Page/Wolfgang_M._Schmidt.md "wikilink")
  - 1977 [志村五郎](../Page/志村五郎.md "wikilink")
  - 1982 [罗伯特·朗兰兹](../Page/罗伯特·朗兰兹.md "wikilink")，[Barry
    Mazur](../Page/Barry_Mazur.md "wikilink")
  - 1987 [多利安·哥德费尔德](../Page/多利安·哥德费尔德.md "wikilink")，[Benedict H.
    Gross](../Page/Benedict_H._Gross.md "wikilink")，[Don B.
    Zagier](../Page/Don_B._Zagier.md "wikilink")
  - 1992 [Karl Rubin](../Page/Karl_Rubin.md "wikilink")，[Paul
    Vojta](../Page/Paul_Vojta.md "wikilink")
  - 1997 [安德魯·懷爾斯](../Page/安德魯·懷爾斯.md "wikilink")
  - 2002 [Henryk
    Iwaniec](../Page/Henryk_Iwaniec.md "wikilink")，[理查·泰勒](../Page/理查·泰勒_\(數學家\).md "wikilink")
  - 2005 [Peter Sarnak](../Page/Peter_Sarnak.md "wikilink")
  - 2008 [曼久尔·巴尔加瓦](../Page/曼久尔·巴尔加瓦.md "wikilink")
  - 2011 [Chandrashekhar
    Khare](../Page/Chandrashekhar_Khare.md "wikilink")，[让-皮埃尔·温唐贝热](../Page/让-皮埃尔·温唐贝热.md "wikilink")
  - 2014 [張益唐](../Page/張益唐.md "wikilink")， [Daniel
    Goldston](../Page/Daniel_Goldston.md "wikilink")， [János
    Pintz](../Page/János_Pintz.md "wikilink")， [Cem
    Yıldırım](../Page/Cem_Yıldırım.md "wikilink")
  - 2017 Henri Darmon

## 外部連結

  - [Frank Nelson Cole Prize in
    Algebra](http://www.ams.org/prizes/cole-prize-algebra.html)
  - [Frank Nelson Cole Prize in Number
    Theory](http://www.ams.org/prizes/cole-prize-number-theory.html)

[K](../Category/數學獎項.md "wikilink")
[Category:以人名命名的奖项](../Category/以人名命名的奖项.md "wikilink")
[Category:美国数学学会奖项](../Category/美国数学学会奖项.md "wikilink")
[Category:数论](../Category/数论.md "wikilink")
[Category:1928年建立的奖项](../Category/1928年建立的奖项.md "wikilink")
[Category:1928年美國建立](../Category/1928年美國建立.md "wikilink")