[Pussy_Willow_Stem_2000px.jpg](https://zh.wikipedia.org/wiki/File:Pussy_Willow_Stem_2000px.jpg "fig:Pussy_Willow_Stem_2000px.jpg")
**柳**，或**柳樹**，是對**柳屬**（Salix）植物的統稱，其下共有四百多種[物種](../Page/物種.md "wikilink")\[1\]，常見於[北半球的](../Page/北半球.md "wikilink")[寒帶及](../Page/寒帶.md "wikilink")[溫帶](../Page/溫帶.md "wikilink")。

柳屬植物要進行雜交非常容易，不論是天然或是人工的混種都很普遍。*Salix ×
sepulcralis*就是由來自[歐洲的](../Page/歐洲.md "wikilink")[白柳](../Page/白柳.md "wikilink")（）跟來自[中國的](../Page/中國.md "wikilink")[垂柳雜交而得](../Page/垂柳.md "wikilink")。

## 文化

古代的中國人視柳為軍營之樹，或者是與[隱士相伴的一種樹](../Page/隱士.md "wikilink")。因為柳的生命力強，生長得也快，經常種在軍營周圍來作屏障。所以將軍的軍營營帳又被稱作柳營。如今，中國各地都有以[柳城為名的](../Page/柳城.md "wikilink")。

### 軍事象徵

由[契丹建立起來的](../Page/契丹.md "wikilink")[遼](../Page/遼.md "wikilink")，有射柳之禮之王朝儀式。[清朝初期](../Page/清朝.md "wikilink")，為抵擋[蒙古人的入侵](../Page/蒙古人.md "wikilink")，修建了小規模的城牆，植柳為棚，稱作柳條邊牆。\[2\]

### 隱士象徵

垂柳因其鬱鬱蔥蔥受到古代隱士的喜愛。

[西晉](../Page/西晉.md "wikilink")[竹林七賢之一的](../Page/竹林七賢.md "wikilink")[嵇康既是隱士也是個叛逆者](../Page/嵇康.md "wikilink")。夏天時，他喜歡在院中大柳樹周圍擺上水，然後蹲在樹陰底下鑄劍，[司隸校尉](../Page/司隸校尉.md "wikilink")[鍾會盛禮前去拜訪他](../Page/鍾會.md "wikilink")，他照樣埋頭冶煉，不理不睬。後被鍾會構所陷害。

柳也是很多[詩人所愛引用的對象](../Page/詩人.md "wikilink")，如[東晉詩人](../Page/東晉.md "wikilink")[陶淵明著](../Page/陶淵明.md "wikilink")《[五柳先生傳](../Page/五柳先生傳.md "wikilink")》以明志。[唐朝](../Page/唐朝.md "wikilink")[李白](../Page/李白.md "wikilink")、[宋朝](../Page/宋朝.md "wikilink")[陸放翁等皆有與柳樹相關的詩句](../Page/陸放翁.md "wikilink")。

### 文学作品

  - 因「柳」與「留」、「[九](../Page/九.md "wikilink")」 二
    字谐音，所以古人常以此暗喻离别很久。如「今宵酒醒何处？杨柳岸，晓风残月」句中表现了柳永對高雅戀人或雅士英才的思念。
  - 古時人常以楊柳作和朋友道別之用，因為楊柳枝葉細長，代表友情的長久。
  - 柳多种于檐前屋后，常作[故乡的象征](../Page/故乡.md "wikilink")。
  - [柳絮飘忽不定](../Page/柳絮.md "wikilink")，常作遣愁的凭借。

## 參考資料

[Category:杨柳科](../Category/杨柳科.md "wikilink")
[Category:柳属](../Category/柳属.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.
2.  [宮崎市定](../Page/宮崎市定.md "wikilink")、[青木正兒等](../Page/青木正兒.md "wikilink")，2005年，《對中國文化的鄉愁》，上海：復旦大學出版社