[一列高速通過柳營車站的[台鐵E1000型推拉式電車](../Page/台鐵E1000型推拉式電車.md "wikilink")|thumb](https://zh.wikipedia.org/wiki/File:EMU1000_passing_LinFongYing_Station.jpg "fig:一列高速通過柳營車站的台鐵E1000型推拉式電車|thumb")
**柳營車站**位於[台灣](../Page/台灣.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")[柳營區](../Page/柳營區.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[縱貫線沿線](../Page/縱貫線_\(南段\).md "wikilink")[鐵路車站之一](../Page/鐵路車站.md "wikilink")。在過去此站也曾是[臺灣糖業股份有限公司柳營線](../Page/臺灣糖業股份有限公司.md "wikilink")（已廢線）上的車站。

## 車站構造

  - [岸式月台兩座](../Page/岸式月台.md "wikilink")。

## 營運狀況

  - 目前為[簡易站](../Page/臺灣鐵路管理局車站等級#簡易站.md "wikilink")，停靠車種為[區間車](../Page/臺鐵區間車.md "wikilink")。
  - 柳營車站距柳營鬧區稍遠。戰後應旅客需要才設立，比鄰近各站遲50年。
  - 原站務冷清，奇美醫院設立柳營分院後客運大增，備有免費接駁車往來院區\[1\]。
  - 站內設有「鐵路之旅 - 小站巡禮紀念章」之戳章，可供旅客蓋戳留念。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通](../Page/一卡通_\(台灣\).md "wikilink")、[icash2.0以及](../Page/Icash.md "wikilink")[HappyCash進出站](../Page/有錢卡.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 162       |
| 1999 | 222       |
| 2000 | 427       |
| 2001 | 435       |
| 2002 | 450       |
| 2003 | 433       |
| 2004 | 479       |
| 2005 | 547       |
| 2006 | 567       |
| 2007 | 650       |
| 2008 | 732       |
| 2009 | 767       |
| 2010 | 862       |
| 2011 | 950       |
| 2012 | 988       |
| 2013 | 1,041     |
| 2014 | 1,080     |
| 2015 | 1,013     |
| 2016 | 951       |
| 2017 | 1,024     |

歷年旅客人次紀錄

## 歷史

### 臺鐵柳營車站

  - 1960年6月1日：啟用，為簡易站，由[新營站管理](../Page/新營車站.md "wikilink")。
  - 2013年5月30日：啟用多卡通刷卡機。

### 臺糖柳營車站

  - 位於新營副產加工廠內，於臺鐵柳營車站北方1.5㎞，為貨運用途。

## 車站週邊

  - 教育單位

<!-- end list -->

  - [臺南市柳營區柳營國民小學](../Page/臺南市柳營區柳營國民小學.md "wikilink")
  - [臺南市立柳營國民中學](../Page/臺南市立柳營國民中學.md "wikilink")
  - [臺南市私立鳳和高級中學](../Page/臺南市私立鳳和高級中學.md "wikilink")
  - [敏惠醫護管理專科學校](../Page/敏惠醫護管理專科學校.md "wikilink")
  - [臺南市私立新榮高級中學](../Page/臺南市私立新榮高級中學.md "wikilink")

<!-- end list -->

  - 政府單位

<!-- end list -->

  - 柳營區公所
  - [臺南市政府消防局第一大隊柳營分隊](../Page/臺南市政府消防局.md "wikilink")
  - [臺南市政府警察局新營分局柳營分駐所](../Page/臺南市政府警察局.md "wikilink")
  - 臺南市衛生局柳營衛生所

<!-- end list -->

  - 醫院

<!-- end list -->

  - [奇美醫院柳營院區](../Page/奇美醫院.md "wikilink")

## 公車路線

|                                                                      |
| -------------------------------------------------------------------- |
| **[大台南公車](../Page/大台南公車.md "wikilink")**                             |
| 編號                                                                   |
| **[<font color=#FF9900>黃幹線</font>](../Page/大台南公車黃幹線.md "wikilink")** |
| <font color=#FF9900>黃3</font>                                        |
| <font color=#FF9900>黃5</font>                                        |

## 註釋

## 鄰近車站

  - **廢止營運模式**

<!-- end list -->

  - **現行營運模式**

## 外部連結

[Category:縱貫線車站 (南段)](../Category/縱貫線車站_\(南段\).md "wikilink")
[Category:台南市鐵路車站](../Category/台南市鐵路車站.md "wikilink")
[Liouying](../Category/台灣糖業鐵路車站.md "wikilink")
[Category:1960年启用的铁路车站](../Category/1960年启用的铁路车站.md "wikilink")
[Category:以區命名的臺灣鐵路車站](../Category/以區命名的臺灣鐵路車站.md "wikilink")

1.  [奇美醫院免費接駁車時刻表](http://www.chimei.org.tw/main/right/right02/clh_department/72500/timetable03.html)