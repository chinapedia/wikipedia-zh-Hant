**柳秀榮**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。2013年在韓劇《Two
Weeks》和女演員[朴河宣合作相識](../Page/朴河宣.md "wikilink")，2017年1月22日，柳秀榮和交往兩年的朴河宣舉行婚禮，成為劇壇知名的銀色夫妻檔。

## 演出作品

### 電視劇

  - 2002年：[SBS](../Page/SBS株式會社.md "wikilink")《[開朗少女成功記](../Page/開朗少女成功記.md "wikilink")》飾演
    吳俊泰
  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[迴旋木馬](../Page/迴轉木馬_\(電視劇\).md "wikilink")》飾演
    全秀明
  - 2003年：SBS《[初戀](../Page/初戀_\(2003年電視劇\).md "wikilink")》
  - 2004年：SBS《[最後的舞請與我一起](../Page/最後的舞請與我一起.md "wikilink")》飾演 鄭泰民
  - 2005年：[KBS](../Page/韓國放送公社.md "wikilink")《[18．29](../Page/18．29.md "wikilink")》飾演
    姜尚永
  - 2005年：MBC《[還生-NEXT](../Page/還生-NEXT.md "wikilink")》飾演
    閔基范／尹明真／卡沙里／琴錫虎／秦修伯
  - 2006年：KBS《[漢城1945](../Page/漢城1945.md "wikilink")》飾演 崔雲赫
  - 2007年：SBS《[不良情侶](../Page/不良情侶.md "wikilink")》飾演 崔啟贊
  - 2008年：MBC《[大韓民國的律師](../Page/大韓民國的律師.md "wikilink")》飾演 邊赫
  - 2011年：MBC《[My Princess](../Page/My_Princess.md "wikilink")》飾演 南廷宇
  - 2011年：KBS《[烏鵲橋兄弟](../Page/烏鵲橋兄弟.md "wikilink")》飾演 黃泰範
  - 2012年：MBC《[兒子傢伙們](../Page/兒子傢伙們.md "wikilink")》飾演 柳旻基
  - 2013年：KBS《Drama Special－我舊錢包裡的回憶》飾演 李英宰
  - 2013年：MBC《[Two Weeks](../Page/Two_Weeks.md "wikilink")》飾演 林承佑
  - 2014年：SBS《[無盡的愛](../Page/無盡的愛_\(2014年電視劇\).md "wikilink")》飾演 韓光勳
  - 2015年：KBS《[Blood](../Page/Blood_\(電視劇\).md "wikilink")》飾演 朴賢瑞（特別出演）
  - 2015年：KBS《[奇怪的兒媳](../Page/奇怪的兒媳.md "wikilink")》飾演 車明錫
  - 2016年：KBS《[鄰家律師趙德浩](../Page/鄰家律師趙德浩.md "wikilink")》飾演 申志旭
  - 2017年：KBS《[爸爸好奇怪](../Page/爸爸好奇怪.md "wikilink")》飾演 車正煥
  - 2018年：SBS《[善良魔女傳](../Page/善良魔女傳.md "wikilink")》飾演 宋宇鎮
  - 2019年：MBC《[悲傷時愛你](../Page/悲傷時愛你.md "wikilink")》飾演 姜仁旭

### 電影

  - 2001年：《[愛的色放](../Page/愛的色放.md "wikilink")》
  - 2003年：《[Blue](../Page/Blue_\(電影\).md "wikilink")》
  - 2013年：《[辯護人](../Page/辯護人_\(電影\).md "wikilink")》

### 音樂劇

  - 2013年：《小姐與流氓》《 2013-11-01 \~ 2014-01-05》BBC劇場

## 綜藝節目

  - 2004年：SBS《[X-Man](../Page/X-Man_\(韓國節目\).md "wikilink")》（Ep.17）
  - 2013年：MBC《[真正的男人](../Page/真正的男人.md "wikilink")》

## 腳註

## 外部連結

  - [EPG](https://web.archive.org/web/20071225102528/http://epg.epg.co.kr/star/profile/index.asp?actor_id=5107)

  - [柳秀榮](http://instagram.com/suyoung_ryu)的[Instagram](../Page/Instagram.md "wikilink")

[R](../Category/韓國電視演員.md "wikilink")
[R](../Category/韓國電影演員.md "wikilink")
[R](../Category/韓國男演員.md "wikilink")
[R](../Category/明知大學校友.md "wikilink")
[R](../Category/京畿道出身人物.md "wikilink")
[R](../Category/富川市出身人物.md "wikilink")
[R](../Category/韓國新教徒.md "wikilink")
[R](../Category/魚姓.md "wikilink")