**柳莺属**（学名：*Phylloscopus*）是[雀形目](../Page/雀形目.md "wikilink")[莺科的一个](../Page/莺科.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")，有时也被列入[柳莺科](../Page/柳莺科.md "wikilink")。约有50-60种，主要包括以下一些：

  - [黄腹柳莺](../Page/黄腹柳莺.md "wikilink") *Phylloscopus affinis*
  - [棕眉柳莺](../Page/棕眉柳莺.md "wikilink") *Phylloscopus armandii*
  - [波氏柳莺](../Page/波氏柳莺.md "wikilink") *Phylloscopus bonelli*
  - [极北柳莺](../Page/极北柳莺.md "wikilink") *Phylloscopus borealis*
  - [伊比利亚柳莺](../Page/伊比利亚柳莺.md "wikilink") *Phylloscopus brehmii*
  - [乌干达柳莺](../Page/乌干达柳莺.md "wikilink") *Phylloscopus budongoensis*
  - [加那利柳莺](../Page/加那利柳莺.md "wikilink") *Phylloscopus canariensis*
  - [叽咋柳莺](../Page/叽咋柳莺.md "wikilink") *Phylloscopus collybita*
  - [冕柳莺](../Page/冕柳莺.md "wikilink") *Phylloscopus coronatus*
  - [白斑尾柳莺](../Page/白斑尾柳莺.md "wikilink") *Phylloscopus davisoni*
  - [烟柳莺](../Page/烟柳莺.md "wikilink") *Phylloscopus fuligiventer*
  - [暗黑柳莺](../Page/暗黑柳莺.md "wikilink") *Phylloscopus fuscatus*
  - [灰柳莺](../Page/灰柳莺.md "wikilink") *Phylloscopus griseolus*
  - [海南柳莺](../Page/海南柳莺.md "wikilink") *Phylloscopus hainansis*
  - [黑帽柳莺](../Page/黑帽柳莺.md "wikilink") *Phylloscopus herberti*
  - [黄眉柳莺](../Page/黄眉柳莺.md "wikilink") *Phylloscopus inornatus*
  - [甘肃柳莺](../Page/甘肃柳莺.md "wikilink") *Phylloscopus kansuensis*
  - [红脸柳莺](../Page/红脸柳莺.md "wikilink") *Phylloscopus laetus*
  - [劳氏柳莺](../Page/劳氏柳莺.md "wikilink") *Phylloscopus laurae*
  - [灰喉柳莺](../Page/灰喉柳莺.md "wikilink") *Phylloscopus maculipennis*
  - [乌嘴柳莺](../Page/乌嘴柳莺.md "wikilink") *Phylloscopus magnirostris*
  - [平原柳莺](../Page/平原柳莺.md "wikilink") *Phylloscopus neglectus*
  - [华西柳莺](../Page/华西柳莺.md "wikilink") *Phylloscopus occisinensis*
  - [东方波氏柳莺](../Page/东方波氏柳莺.md "wikilink") *Phylloscopus orientalis*
  - [双斑绿柳莺](../Page/双斑绿柳莺.md "wikilink") *Phylloscopus plumbeitarsus*
  - [黄腰柳莺](../Page/黄腰柳莺.md "wikilink") *Phylloscopus proregulus*
  - [橙斑翅柳莺](../Page/橙斑翅柳莺.md "wikilink") *Phylloscopus pulcher*
  - [冠纹柳莺](../Page/冠纹柳莺.md "wikilink") *Phylloscopus reguloides*
  - [黑眉柳莺](../Page/黑眉柳莺.md "wikilink") *Phylloscopus ricketti*
  - [黄喉柳莺](../Page/黄喉柳莺.md "wikilink") *Phylloscopus ruficapilla*
  - [巨嘴柳莺](../Page/巨嘴柳莺.md "wikilink") *Phylloscopus schwarzi*
  - [林柳莺](../Page/林柳莺.md "wikilink") *Phylloscopus sibilatrix*
  - [四川柳莺](../Page/四川柳莺.md "wikilink")（云南柳莺）*Phylloscopus sichuanensis*
  - [东方叽咋柳莺](../Page/东方叽咋柳莺.md "wikilink") *Phylloscopus sindianus*
  - [棕腹柳莺](../Page/棕腹柳莺.md "wikilink") *Phylloscopus subaffinis*
  - [灰脚柳莺](../Page/灰脚柳莺.md "wikilink") *Phylloscopus tenellipes*
  - [暗绿柳莺](../Page/暗绿柳莺.md "wikilink") *Phylloscopus trochiloides*
  - [欧柳莺](../Page/欧柳莺.md "wikilink") *Phylloscopus trochilus*
  - [褐柳莺](../Page/褐柳莺.md "wikilink") *Phylloscopus umbrovirens*
  - [云南柳莺](../Page/云南柳莺.md "wikilink") *Phylloscopus yunnanensis*

[Category:莺科](../Category/莺科.md "wikilink")
[\*](../Category/柳莺属.md "wikilink")