《**柴犬奇蹟物語**》（）是一套改編自真人真事的[日本電影](../Page/日本.md "wikilink")，2007年12月18日在日本首播。原作是[桑原眞二](../Page/桑原眞二.md "wikilink")、[大野一興的](../Page/大野一興.md "wikilink")「山古志村のマリと三匹の子犬」（[文藝春秋刊](../Page/文藝春秋刊.md "wikilink")）

## 故事概要

故事發生在[新潟縣的](../Page/新潟縣.md "wikilink")（已於2005年4月1日被併入[長岡市](../Page/長岡市.md "wikilink")），在當地時間2004年10月23日17時56分，發生了6.8級[新潟縣中越地震](../Page/新潟縣中越地震.md "wikilink")。石川家的爺爺石川優造及小女孩石川彩，受了重傷被困在瓦礫中，就在此時他們聽到了家中[柴犬麻里的聲音](../Page/柴犬.md "wikilink")，牠更不顧安危，一邊照顧自己的孩子「包」、「剪」、「揼」，一邊叫喚優造和彩給予他們生存的勇氣。最後[自衛隊趕到把優造和彩](../Page/自衛隊.md "wikilink")，還有其餘所有居民救了出來，可是卻遺留麻里和三隻小狗在災區。

在沒有食物下，麻里仍然盡力守護著三隻小狗，在避難所的彩和哥哥亮太得知暴風雨快來臨，兩人決定開始展開他們的拯救計畫。

## 角色

  - 石川優一（村辦公室職員、40歳）：[船越英一郎](../Page/船越英一郎.md "wikilink")（粤语配音：[梁家輝](../Page/梁家輝.md "wikilink")）
  - 長谷川冴子（優一的妻妹、35歳）：[松本明子](../Page/松本明子.md "wikilink")（粤语配音：[梁詠琪](../Page/梁詠琪.md "wikilink")）
  - 石川亮太（優一的兒子、10歳）：[廣田亮平](../Page/廣田亮平.md "wikilink")（粤语配音：[王樹熹](../Page/王樹熹.md "wikilink")）
  - 石川彩（優一的女兒、5歳）：[佐佐木麻緒](../Page/佐佐木麻緒.md "wikilink")
  - 安田啓一（陸軍自衛隊二等曹）：[高嶋政伸](../Page/高嶋政伸.md "wikilink")（粤语配音：[孫耀威](../Page/孫耀威.md "wikilink")）
  - 關根博美（亮太的老師）：[小林麻央](../Page/小林麻央.md "wikilink")
  - 兒島忠志（山古志村村長）：[小野武彦](../Page/小野武彦.md "wikilink")（模仿[長島忠美即舊山古志村村長](../Page/長島忠美.md "wikilink")）
  - 石川優造（優一的父親、75歳）：[宇津井健](../Page/宇津井健.md "wikilink")（粤语配音：[胡楓](../Page/胡楓.md "wikilink")）

## 香港配音

此電影在香港由[梁家輝](../Page/梁家輝.md "wikilink")、[梁詠琪](../Page/梁詠琪.md "wikilink")、[孫耀威](../Page/孫耀威.md "wikilink")、[胡楓和](../Page/胡楓.md "wikilink")[王樹熹聲演](../Page/王樹熹.md "wikilink")。

## 主題歌

  - [平原綾香](../Page/平原綾香.md "wikilink")「今、風の中で」

## 關連項目

  - [新潟縣中越地震 (2004年)](../Page/新潟縣中越地震_\(2004年\).md "wikilink")

## 外部連結

  - {{@movies|fajp49705161}}

  -
  -
  -
  -
  -
　

[X](../Category/日本電影作品.md "wikilink")
[Category:真人真事改編電影](../Category/真人真事改編電影.md "wikilink")
[Category:新潟縣背景電影](../Category/新潟縣背景電影.md "wikilink")
[Category:狗電影](../Category/狗電影.md "wikilink")
[Category:日本電視台製作的電影](../Category/日本電視台製作的電影.md "wikilink")