**柴田町**（）是位于[宮城縣南部](../Page/宮城縣.md "wikilink")[柴田郡的一](../Page/柴田郡.md "wikilink")[町](../Page/町.md "wikilink")。

## 教育

[Sendai_University.JPG](https://zh.wikipedia.org/wiki/File:Sendai_University.JPG "fig:Sendai_University.JPG")

### 大学

  - [仙台大学](../Page/仙台大学.md "wikilink")

### 高校

  - [宮城県柴田高等学校](../Page/宮城県柴田高等学校.md "wikilink")

### 中学校

  - [柴田町立槻木中学校](../Page/柴田町立槻木中学校.md "wikilink")
  - [柴田町立船岡中学校](../Page/柴田町立船岡中学校.md "wikilink")
  - [柴田町立船迫中学校](../Page/柴田町立船迫中学校.md "wikilink")

### 小学校

  - [柴田町立槻木小学校](../Page/柴田町立槻木小学校.md "wikilink")
  - [柴田町立柴田小学校](../Page/柴田町立柴田小学校.md "wikilink")
  - [柴田町立船迫小学校](../Page/柴田町立船迫小学校.md "wikilink")
  - [柴田町立東船岡小学校](../Page/柴田町立東船岡小学校.md "wikilink")
  - [柴田町立船岡小学校](../Page/柴田町立船岡小学校.md "wikilink")
  - [柴田町立西住小学校](../Page/柴田町立西住小学校.md "wikilink")

### 特殊学校

  - [宮城県立船岡支援学校](../Page/宮城県立船岡支援学校.md "wikilink")

## 交通

[JR_Funaoka_sta_001.jpg](https://zh.wikipedia.org/wiki/File:JR_Funaoka_sta_001.jpg "fig:JR_Funaoka_sta_001.jpg")\]\]
[Tshukinoki-station.jpg](https://zh.wikipedia.org/wiki/File:Tshukinoki-station.jpg "fig:Tshukinoki-station.jpg")\]\]

### 鐵道

  - [東日本旅客鉄道](../Page/東日本旅客鉄道.md "wikilink")（JR東日本）
      - [東北本線](../Page/東北本線.md "wikilink")：[船岡站](../Page/船岡站_\(宮城縣\).md "wikilink")
        - [槻木站](../Page/槻木站.md "wikilink")
  - [阿武隈急行](../Page/阿武隈急行.md "wikilink")
      - [阿武隈急行線](../Page/阿武隈急行線.md "wikilink")：[東船岡站](../Page/東船岡站.md "wikilink")
        - [槻木站](../Page/槻木站.md "wikilink")

## 姊妹都市

  - ：[北上市](../Page/北上市.md "wikilink")（[岩手縣](../Page/岩手縣.md "wikilink")）

      - 1980年（昭和55年）1月25日結交。

  - ：[伊達市](../Page/伊達市_\(北海道\).md "wikilink")（[北海道](../Page/北海道.md "wikilink")）

      - 1988年（昭和63年）5月30日成為歴史友好都市。

  - ：[阿西斯梭白裡安市](../Page/阿西斯梭白裡安市.md "wikilink")

      - 1981年（昭和56年）4月13日結交。

  - ：[丹陽市](../Page/丹陽市.md "wikilink")

      - 1994年（平成6年）2月23日結交。