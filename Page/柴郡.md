**柴郡**（），舊稱**切斯特郡**（），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西北部的](../Page/西北英格蘭.md "wikilink")[郡](../Page/英格蘭的郡.md "wikilink")。柴郡的西半部（[埃爾斯米爾港-內斯頓](../Page/埃爾斯米爾港-內斯頓.md "wikilink")、[切斯特兩區](../Page/切斯特區.md "wikilink")）位於[威勒爾半島上](../Page/威勒爾半島.md "wikilink")，北邊對岸是著名城市[利物浦](../Page/利物浦.md "wikilink")。[切斯特是](../Page/切斯特.md "wikilink")[郡治](../Page/郡治.md "wikilink")。以人口計算，[沃靈頓是第](../Page/沃靈頓.md "wikilink")1大鎮（Town），[克魯排第](../Page/克魯.md "wikilink")2，[埃爾斯米爾港排第](../Page/埃爾斯米爾港.md "wikilink")3。

2009年4月之前，柴郡是34個[非都市郡之一](../Page/非都市郡.md "wikilink")，實際管轄6個[非都市區](../Page/非都市區.md "wikilink")，[佔地](../Page/面積.md "wikilink")2,083[平方公里](../Page/平方公里.md "wikilink")（[第25](../Page/英格蘭的非都市郡列表_\(以面積排列\).md "wikilink")），有686,300[人口](../Page/人口.md "wikilink")（[第14](../Page/英格蘭的非都市郡列表_\(以人口排列\).md "wikilink")）；如待成48個[名譽郡之一](../Page/名譽郡.md "wikilink")，它名義上包含多2個[單一管理區](../Page/單一管理區.md "wikilink")─[霍爾頓區](../Page/霍爾頓區.md "wikilink")、[沃靈頓](../Page/沃靈頓.md "wikilink")，[佔地增至](../Page/面積.md "wikilink")2,343[平方公里](../Page/平方公里.md "wikilink")（[第25](../Page/英格蘭的名譽郡列表_\(以面積排列\).md "wikilink")），[人口增至](../Page/人口.md "wikilink")999,800（[第19](../Page/英格蘭的名譽郡列表_\(以人口排列\).md "wikilink")）。

2009年4月，柴郡保有[名譽郡](../Page/名譽郡.md "wikilink")，廢除了[非都市郡](../Page/非都市郡.md "wikilink")，改制為4個[單一管理區](../Page/單一管理區.md "wikilink")。

## 行政區劃

### 1972-2009

[ 1. [埃爾斯米爾港-內斯頓](../Page/埃爾斯米爾港-內斯頓.md "wikilink")
2\. [切斯特](../Page/切斯特區.md "wikilink")
3\. [克魯-楠特威奇](../Page/克魯-楠特威奇.md "wikilink")
4\. [康格爾頓](../Page/康格爾頓區.md "wikilink")
5\. [麥克爾斯菲爾德](../Page/麥克爾斯菲爾德區.md "wikilink")
6\. [韋爾羅亞爾](../Page/韋爾羅亞爾.md "wikilink")
7\.
[霍爾頓](../Page/霍爾頓區.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
8\.
[沃靈頓](../Page/沃靈頓.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:Cheshire_Ceremonial_Numbered.png "fig: 1. 埃爾斯米爾港-內斯頓 2. 切斯特 3. 克魯-楠特威奇 4. 康格爾頓 5. 麥克爾斯菲爾德 6. 韋爾羅亞爾 7. 霍爾頓（單一管理區） 8. 沃靈頓（單一管理區） ")
柴郡是[非都市郡](../Page/非都市郡.md "wikilink")，實際管轄6個[非都市區](../Page/非都市區.md "wikilink")：[埃爾斯米爾港-內斯頓](../Page/埃爾斯米爾港-內斯頓.md "wikilink")（Ellesmere
Port and
Neston）、[切斯特](../Page/切斯特區.md "wikilink")、[克魯-楠特威奇](../Page/克魯-楠特威奇.md "wikilink")（Crewe
and
Nantwich）、[康格爾頓](../Page/康格爾頓區.md "wikilink")（Congleton）、[麥克爾斯菲爾德](../Page/麥克爾斯菲爾德區.md "wikilink")（Macclesfield）、[韋爾羅亞爾](../Page/韋爾羅亞爾.md "wikilink")（Vale
Royal）；如看待成[名譽郡](../Page/名譽郡.md "wikilink")，它名義上包含多2個[單一管理區](../Page/單一管理區.md "wikilink")：[霍爾頓](../Page/霍爾頓區.md "wikilink")（Halton）、[沃靈頓](../Page/沃靈頓.md "wikilink")（Warrington）。

### 2009-

2009年4月1日，英格蘭地方政府結構變化正式實施，7個[英格蘭的郡改革行政區劃](../Page/英格蘭的郡.md "wikilink")，產生9個新的[單一管理區](../Page/單一管理區.md "wikilink")。柴郡的[非都市郡廢除](../Page/非都市郡.md "wikilink")，改制為4個[單一管理區](../Page/單一管理區.md "wikilink")。
[ 1.
[柴郡西-切斯特](../Page/柴郡西-切斯特.md "wikilink")（[單一管理區](../Page/單一管理區.md "wikilink")）
2\.
[柴郡東](../Page/柴郡東.md "wikilink")（[單一管理區](../Page/單一管理區.md "wikilink")）
3\.
[沃靈頓](../Page/沃靈頓.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
4\.
[霍爾頓](../Page/霍爾頓區.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:New_Cheshire_Ceremonial_Numbered.png "fig: 1. 柴郡西-切斯特（單一管理區） 2. 柴郡東（單一管理區） 3. 沃靈頓（單一管理區） 4. 霍爾頓（單一管理區） ")

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。柴郡西北與[默西賽德郡相鄰](../Page/默西賽德郡.md "wikilink")，東北與[大曼徹斯特郡相鄰](../Page/大曼徹斯特.md "wikilink")，東與[打比郡相鄰](../Page/打比郡.md "wikilink")，東南與[斯塔福德郡相鄰](../Page/斯塔福德郡.md "wikilink")，南與[什羅普郡相鄰](../Page/什羅普郡.md "wikilink")。

## 外部連結

  - [柴郡議會](https://web.archive.org/web/20070227152139/http://www.cheshire.gov.uk/)

[\*](../Category/柴郡.md "wikilink")
[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[Category:單一管理區](../Category/單一管理區.md "wikilink")