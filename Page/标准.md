**标准**，根据[中国国家标准化委员会的定义](../Page/中国国家标准化委员会.md "wikilink")，是“为了在一定的范围内获得最佳秩序，经协商一致制定并由公认机构批准，共同使用的和重复使用的一种规范性文件。”\[1\]

標準原意為目的，也就是標靶。其後由於標靶本身的特性，衍生出一個「如何與其他事物區別的規則」的意思。會衍生出這個意思也不難理解。標靶是「用來判定技術或成果好不好的根據」，標準是另一個說法。將「用來判定技術或成果好不好的根據」廣泛化，就得到了「用來判定是不是某一事物的根據」

技术意义上的**标准**就是一种以文件形式发布的统一协定，其中包含可以用来为某一范围内的活动及其结果制定规则、导则或特性定义的技术规范或者其他精确准则，其目的是确保[材料](../Page/材料.md "wikilink")、产品、过程和服务能够符合需要。一般而言，标准文件的制定都经过协商过程，并经一个公认机构批准。标准往往对应该严肃对待的方面（比如[机器和工具的安全](../Page/机器.md "wikilink")、可靠性和效率，[玩具](../Page/玩具.md "wikilink")，[医学设备](../Page/医学设备.md "wikilink")）有深远影响。

## 参见

  - [协作问题](../Page/协作问题.md "wikilink")\[2\]

  -
  - [国防标准](../Page/国防标准.md "wikilink")

<!-- end list -->

  - [标准组织](../Page/标准组织.md "wikilink")
      - [国际标准组织](../Page/标准组织#国际标准组织.md "wikilink")
      - [国家标准机构](../Page/标准组织#国家标准团体.md "wikilink")

<!-- end list -->

  - [开放标准](../Page/开放标准.md "wikilink")
  - [国际单位制](../Page/国际单位制.md "wikilink")
  - [国家标准](../Page/国家标准.md "wikilink")
  - [标准化](../Page/标准化.md "wikilink")

## 参考文献

## 外部链接

  - [ISO对Standard的定义](http://www.ansi.org/about_ansi/faqs/faqs.aspx?menuid=1)

[\*](../Page/category:标准.md "wikilink")

[cs:Standard](../Page/cs:Standard.md "wikilink")

1.  GB/T 20000.1-2002，定义2.3.2
2.  Edna Ullmann-Margalit: *The Emergence of Norms*, Oxford Un. Press,
    1977. (or Clarendon Press 1978)