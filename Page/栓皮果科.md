**栓皮果科**有2[属](../Page/属.md "wikilink")3[种](../Page/种.md "wikilink")（*Strasburgeria
calliantha*、*Strasburgeria robusta*和*Ixerba
brexioides*），仅生长在[新喀里多尼亚](../Page/新喀里多尼亚.md "wikilink")，是当地的特有种。

本科[植物为常绿](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，叶大，有锯齿和托叶；[花大](../Page/花.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")5数，肉质，雄蕊10；[果实为肉质](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，圆形前端尖。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[山茶科中](../Page/山茶科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为无法将其列在任何一个](../Page/APG_分类法.md "wikilink")[目中](../Page/目.md "wikilink")，属于系属不清的[科](../Page/科.md "wikilink")，2003年经过修订的[APG
II
分类法将其直接放在](../Page/APG_II_分类法.md "wikilink")[蔷薇分支之下](../Page/蔷薇分支.md "wikilink")，2006年5月7日的进一步修订将本科列在[缨子木目中](../Page/缨子木目.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](http://delta-intkey.com/angio/)中的[栓皮果科](http://delta-intkey.com/angio/www/strasbur.htm)
  - [APG网站中的栓皮果科](http://www.mobot.org/MOBOT/Research/APWeb/orders/crossosomatalesweb.htm#Strasburgeriaceae)

[\*](../Category/栓皮果科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")