[HK_Parklane_Avenue.jpg](https://zh.wikipedia.org/wiki/File:HK_Parklane_Avenue.jpg "fig:HK_Parklane_Avenue.jpg")

**栢麗購物大道**（簡稱**栢麗大道**；[英語](../Page/英語.md "wikilink")：**Park Lane
Shopper's
Boulevard**）是[香港一條購物街](../Page/香港.md "wikilink")，位於香港[九龍油尖旺區](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道](../Page/彌敦道.md "wikilink")，[九龍公園東側](../Page/九龍公園.md "wikilink")，全長約300米，由香港建築師[嚴迅奇設計](../Page/嚴迅奇.md "wikilink")，1986年落成。整條街道林立逾50間[商店](../Page/商店.md "wikilink")，主要是本地或國際[連鎖式的](../Page/連鎖式.md "wikilink")[服裝店](../Page/服裝.md "wikilink")。在商舖之外的行人路廣寬，寬達10米，保留了不少大樹，更擺放了一座藝術[雕塑](../Page/雕塑.md "wikilink")。該雕塑由[九龍西區扶輪社](../Page/九龍西區扶輪社.md "wikilink")[贊助](../Page/贊助.md "wikilink")。栢麗購物大道的[商店營業時間由上午](../Page/商店.md "wikilink")11:00至晚上10:00。

## 天台花園

彌敦道栢麗大道購物中心的[天台](../Page/天台.md "wikilink")，裝置為綠化的花園草地，內有少量樹木和露天長椅。天台花園與九龍公園的[建築風格一致](../Page/建築風格.md "wikilink")，是[康文署管理的](../Page/康文署.md "wikilink")[公共空間](../Page/公共空間.md "wikilink")\[1\]。

<File:HK> Parklane Avenue
Statue.jpg|[文樓作品](../Page/文樓.md "wikilink")《請》藝術雕塑
<File:HK> Parklane Shop.jpg|每間商店樓高兩層

## 2009年的租戶(部份)

<File:HK> TST Nathan Road Park Lane SB 07 Walker Shop 12-2009.JPG
<File:HK> TST Nathan Road Park Lane SB shop 03 GEOX
12-2009.JPG|[Geox](../Page/Geox.md "wikilink") <File:HK> TST Nathan Road
Park Lane SB shop 05 Lam Chan Kee
12-2009.JPG|[林贊記](../Page/林贊記.md "wikilink") <File:HK> TST
Nathan Road Park Lane SB shop 06 New Vision 12-2009.JPG <File:HK> TST
Nathan Road Park Lane SB shop 08 Michel Rene
12-2009.JPG|[馬獅龍](../Page/馬獅龍.md "wikilink") <File:HK> TST
Nathan Road Park Lane SB shop 09 Ash Worth 12-2009.JPG <File:HK> TST
Nathan Road Park Lane SB shop 11 Royal Sporting House
12-2009.JPG|[皇家運動專門店](../Page/皇家運動專門店.md "wikilink")
<File:HK> TST Nathan Road Park Lane SB shop 12-2009 01 Tissot watch
shop.JPG|[瑞士天梭表](../Page/瑞士天梭表.md "wikilink") <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 02 COBO shop.JPG <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 03 Clarks shoes shop.JPG <File:HK> TST Nathan
Road Park Lane SB shop 12-2009 04 FILA shop.JPG <File:HK> TST Nathan
Road Park Lane SB shop 12-2009 05 SWAROVSKI
shop.JPG|[施華洛世奇](../Page/施華洛世奇.md "wikilink")
<File:HK> TST Nathan Road Park Lane SB shop 12-2009 06 Yue Hwa
Outlet.JPG|[裕華國貨](../Page/裕華國貨.md "wikilink") <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 07 Fancl House.JPG <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 08 UNION.JPG <File:HK> TST Nathan Road Park
Lane SB shop 12-2009 09 唯一禮品 Exclusive.JPG <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 10 Chevignon.JPG <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 11 亮視點
LensCrafters.JPG|[亮視點](../Page/亮視點.md "wikilink") <File:HK>
TST Nathan Road Park Lane SB shop 12-2009 C01 Jipi Japa.JPG <File:HK>
TST Nathan Road Park Lane SB shop 12-2009 C02 Acupuncture London.JPG
<File:HK> TST Nathan Road Park Lane SB shop 12-2009 C03 City
Chain.JPG|[時間廊](../Page/寶光實業.md "wikilink") <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 C06 Sphere.JPG <File:HK> TST Nathan Road Park
Lane SB shop 12-2009 C07 中僑.JPG|[中僑國貨](../Page/中僑國貨.md "wikilink")
<File:HK> TST Nathan Road Park Lane SB shop 12-2009 C08 CREY.JPG
<File:HK> TST Nathan Road Park Lane SB shop 12-2009 C09 POLO Santa
Roberta.JPG <File:HK> TST Nathan Road Park Lane SB shop 12-2009 C10 沙馳
Satchi 2.JPG|[沙馳](../Page/沙馳.md "wikilink") <File:HK> TST Nathan Road
Park Lane SB shop 12-2009 C11 clothing.JPG

## 2010年

<File:HK> TST night 柏麗購物大道 Park Lane Shopper's Boulevard 巴西 Brasil 森巴舞娘
Samba Dancers Nov-2010 06.JPG |迎[聖誕節前黃金購物月](../Page/聖誕節.md "wikilink")
<File:HK> TST night 柏麗購物大道 Park Lane Shopper's Boulevard 巴西 Brasil 森巴舞娘
Samba Dancers Nov-2010 07 Urban
Group.JPG|[富城集團協辦](../Page/富城集團.md "wikilink")
<File:HK> TST night 柏麗購物大道 Park Lane Shopper's Boulevard 巴西 Brasil 森巴舞娘
Samba female dancers Nov-2010 03.JPG |[森巴舞助慶](../Page/森巴舞.md "wikilink")
<File:HK> TST night 柏麗購物大道 Park Lane Shopper's Boulevard 巴西 Brasil 森巴舞娘
Samba female dancers Nov-2010 R1.JPG

## 鄰近

  - [港鐵](../Page/港鐵.md "wikilink")[尖沙咀站A](../Page/尖沙咀站.md "wikilink")1出口
  - [九龍清真寺](../Page/九龍清真寺.md "wikilink")
  - 尖沙咀警署
  - [美麗華酒店](../Page/美麗華酒店.md "wikilink")
  - [美麗華商場](../Page/美麗華商場.md "wikilink")
  - [古物古蹟辦事處](../Page/古物古蹟辦事處.md "wikilink")
  - [加連威老道](../Page/加連威老道.md "wikilink")
  - [金巴利道](../Page/金巴利道.md "wikilink")
  - [天文台道](../Page/天文台道.md "wikilink")
  - [山林道](../Page/山林道.md "wikilink")

## 公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: orange; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: orange; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: orange; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[尖沙咀站](../Page/尖沙咀站.md "wikilink")、[佐敦站](../Page/佐敦站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 意外

2012年7月19日，一棵被列入[古樹名冊](../Page/古樹名冊.md "wikilink")\[2\]的[細葉榕疑感染](../Page/細葉榕.md "wikilink")[褐根病](../Page/褐根病.md "wikilink")，突然連根拔起折斷塌向栢麗大道對開的行人路，一男四女途人走避不及被大樹壓傷\[3\]。

## 外部參考

[Category:香港特色街道](../Category/香港特色街道.md "wikilink")
[Category:油尖旺區商場](../Category/油尖旺區商場.md "wikilink")
[Category:尖沙咀](../Category/尖沙咀.md "wikilink")
[Category:彌敦道](../Category/彌敦道.md "wikilink")

1.  [栢麗購物大道的天台花園](http://www.lcsd.gov.hk/parks/kp/b5/roof_gardens.php)
2.  [油尖旺區
    樹木登記資料](http://www.trees.gov.hk/filemanager/content/attachments/Yau%20Tsim%20Mong.pdf)樹木管理辦事處
3.  [栢麗大道百年榕樹倒塌五傷](http://hk.apple.nextmedia.com/news/art/20120720/16531311)《蘋果日報》2012年7月20日