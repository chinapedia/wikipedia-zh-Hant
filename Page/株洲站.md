**株洲站**位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[株洲市](../Page/株洲市.md "wikilink")[人民南路](../Page/人民南路.md "wikilink")，于1910年启用，为[中国铁路总公司旗下](../Page/中国铁路总公司.md "wikilink")[中国铁路广州局集团有限公司直属的客运](../Page/中国铁路广州局集团有限公司.md "wikilink")[特等站](../Page/特等站.md "wikilink")。经过铁路有[京广铁路](../Page/京广铁路.md "wikilink")、[沪昆铁路](../Page/沪昆铁路.md "wikilink")、[长株潭城际铁路](../Page/长株潭城际铁路.md "wikilink")。株洲站的铁路枢纽地位，使株洲有北[郑（州）](../Page/郑州市.md "wikilink")，南株（洲）之美名。

## 历史

[201712_Intercity_Yard_of_Zhuzhou_Station.jpg](https://zh.wikipedia.org/wiki/File:201712_Intercity_Yard_of_Zhuzhou_Station.jpg "fig:201712_Intercity_Yard_of_Zhuzhou_Station.jpg")
株洲的首个火车站于1905年12月12日[株萍铁路通车时建成](../Page/株萍铁路.md "wikilink")，其站址位于[湘江岸边](../Page/湘江.md "wikilink")，后改称株洲南站，抗日战争后称株洲西站，株洲的第二个火车站则是1911年1月开通的[粤汉铁路株洲北站](../Page/粤汉铁路.md "wikilink")，站址位于今株洲站行包房处。\[1\]株萍路株洲车站与粤汉路株洲北站相距1.5公里，但互不相连，直至1913年5月才修建一条联络线，互通车辆。

1928年11月，[中華民國鐵道部令株萍路并入粤汉铁路湘鄂段统一管理](../Page/中華民國鐵道部.md "wikilink")，当时的株洲北站随之成为联运枢纽，1936年改称株洲车站，原株洲南站同时改为岔道站。1937年9月，株萍路并入[浙赣铁路](../Page/浙赣铁路.md "wikilink")，南岳岭下新建株洲总站（亦称北站），与粤汉铁路株洲车站平面相对。\[2\]同时，株洲东站在株洲总站以东1.98公里处建成，株洲形成设4个火车站的格局，这四个站即粤汉路株洲站，浙赣路株洲总站、东站、南站。\[3\]彼时的粤汉路株洲车站只有5条股道，一座面积100平方米、只能容纳100人的站房，一座长60米的月台以及北侧的车房。浙赣路株洲总站（北站）也只有4条股道、一座面积150平方米的站房、一座月台、一座钢架天桥，以及车站北侧供机车换向的三角线。株洲南站、东站也各有两股道。\[4\]

1949年9月，株洲的四个火车站合并，统称株洲车站，由[衡阳铁路局管辖](../Page/衡阳铁路局.md "wikilink")，其后原粤汉铁路株洲站办理客货运业务，原株洲南站改为办理整车货运的货房，原株洲总站改为运转室，原株洲东站于1950年11月1日撤销。\[5\]1954年，株洲站增加到发线2股及编组线1股，接发列车能力提高44.7%，容车量提高28辆，一年后又新建站房833平方米及旅客站台围墙800米，同时增铺到发线2股、旅客列车给水管道421米。1956年，株洲站新建旅客第二站台，架设天桥一座，增铺南头牵出线一条。\[6\]同年，[五里墩车站划归株洲车站管理](../Page/五里墩站_\(湖南省\).md "wikilink")。\[7\]1957年，株洲站新建北货场，铺设7股货物线。彼时正是[第一个五年计划期间](../Page/第一个五年计划_\(中国\).md "wikilink")，粤汉铁路原有的线路设备已然无法满足经济发展的需要，武昌至衡阳复线工程随即上马，其中株洲境内长75公里，1958年开工。\[8\]京广南端复线引入株洲站的工程于1969年开工，次年8月完工，株洲站站场则在1970年10月至1972年6月间改建，股道延长。\[9\]株洲站站场电气集中工程于1971年3月15日完工。

1974年，[铁道部和湖南省决定在株洲车站原址兴建株洲新客站](../Page/中华人民共和国铁道部.md "wikilink")，占地1.2万平方米，一期工程包括主楼和北附楼（售票厅）、软席候车室和人防地道的建设，于1975年12月开工，1981年9月完工。1980年设计、施工的二期工程包括南附楼（行包房）、天桥、地道、雨棚等，其中天桥于1981年完工，行包房1983年投产，一、二站台及雨棚于1984年延长，地道和雨棚则于1985年完工。\[10\]1987年8月9日，株洲站开始建设6502型电气集中信号楼，设备于1989年6月6日安装调试完毕。彼时株洲车站的行车信号全部采用色灯信号，联锁设备为电气集中，站内正线和到发线的道岔均与有关信号机联锁。1990年，株洲站的客运量为988.81万人，货运量221.6万吨。\[11\]

2017年8月，铁路总公司批复株洲站改造计划，改扩建后的株洲火车站采用线测式+线上式综合站型，站房由东、西子站房及高架候车室组成，站房面积约为现有5倍。\[12\]

2016年12月16日，长株潭城际铁路开通运营，在株洲站设2站台4股道。由于株洲站站改尚未竣工，故各次列车通过不停车。

## 现状

[201609_Platform_2_of_Zhuzhou_Station.jpg](https://zh.wikipedia.org/wiki/File:201609_Platform_2_of_Zhuzhou_Station.jpg "fig:201609_Platform_2_of_Zhuzhou_Station.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Zhuzhou_Railway_Station_2014.03.04_15-29-50.jpg "fig:缩略图")
[201609_Tracks_at_Zhuzhou_Station_(Beijingxi_Direction).jpg](https://zh.wikipedia.org/wiki/File:201609_Tracks_at_Zhuzhou_Station_\(Beijingxi_Direction\).jpg "fig:201609_Tracks_at_Zhuzhou_Station_(Beijingxi_Direction).jpg")
[201609_YW25T-686072_from_Z99_at_Zhuzhou_Station.jpg](https://zh.wikipedia.org/wiki/File:201609_YW25T-686072_from_Z99_at_Zhuzhou_Station.jpg "fig:201609_YW25T-686072_from_Z99_at_Zhuzhou_Station.jpg")停靠在株洲站站台\]\]
[CA25K_892719@ZZQ_(20160324082643).jpg](https://zh.wikipedia.org/wiki/File:CA25K_892719@ZZQ_\(20160324082643\).jpg "fig:CA25K_892719@ZZQ_(20160324082643).jpg")停靠在株洲站\]\]
作为南北走向的[京广铁路及東西走向的](../Page/京广铁路.md "wikilink")[沪昆铁路的枢纽车站](../Page/沪昆铁路.md "wikilink")，株洲站是[中國中南地区客运量较大的车站之一](../Page/中國.md "wikilink")，衔接、、、Ⅲ场和株洲北Ⅳ场5个方向。\[13\]株洲站建有候车站台4座，共有11条股道，在建的[长株潭城际铁路位于现有站场的东侧](../Page/长株潭城际铁路.md "wikilink")，设2站台4股道。

株洲站2015年发送旅客约585万人次\[14\]。2018年春运株洲站共发送旅客57.6万人次，最高日发送旅客2.1万人次\[15\]。2019年4月5日，株洲站发送旅客3.8万人次。

<File:Zhuzhou> Railway Station 20170716 064621.jpg|进站大厅 <File:Zhuzhou>
Railway Station 20170716 065403.jpg|进站天桥 <File:Zhuzhou> Railway Station
20170716 065541.jpg|一站台和站房之间的通道 <File:Zhuzhou> Railway Station 20170712
041755.jpg|出站地道 <File:Exit> of Zhuzhou Railway Station
(20160324082800).jpg|出站口

### 旅客列车目的地

目前每天有188班旅客列车在株洲站停靠。在春节、清明、国庆等客流高峰，会增开20～30辆旅客列车。

<table>
<thead>
<tr class="header">
<th><p>列车所属路局</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、（和金温公司交替担当）、、、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、（集通公司担当）</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、<a href="../Page/南昌站_(江西).md" title="wikilink">南昌</a>、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、、、、 &lt;!--|-</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>（淡季停運）、、、、、、、、、（<a href="../Page/金温公司.md" title="wikilink">金温公司担当</a>）、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、（隔日）</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、</p></td>
</tr>
</tbody>
</table>

## 负面评价

[HXD1D_0168_at_Zhuzhou_Turnaround_Depot_(20160324082100).jpg](https://zh.wikipedia.org/wiki/File:HXD1D_0168_at_Zhuzhou_Turnaround_Depot_\(20160324082100\).jpg "fig:HXD1D_0168_at_Zhuzhou_Turnaround_Depot_(20160324082100).jpg")
作为京广铁路和沪昆铁路交汇点的株洲站在站线布置方面存在一定缺陷：由东沪昆下行前往京广下行的列车需在株洲站南部切割京广正线，进株洲站折角运行，而沪昆正线下行与京广上行在株洲-间共线，且株洲站自身站台不足，接发车能力紧张，再加上不少列车要在此站换挂机车，交换司机，导致列车在株洲站停车时间较长\[16\]，在这种情况下东沪昆下行列车一般要在位于株洲东部[五里墩乡的](../Page/五里墩乡.md "wikilink")[五里墩站扣点或是在五里墩](../Page/五里墩站_\(湖南省\).md "wikilink")-株洲区间机外停车（进港的[Z99除外](../Page/沪九直通车.md "wikilink")，因晚点时间太长会被禁止入港），造成许多本应正点到达株洲站的列车因在五里墩站停留时间过长而严重晚点。此现象在上海南到贵阳的[K111次列车尤为明显](../Page/K111/112次列车.md "wikilink")，该站也因此被中华人民共和国大陆境内的铁道迷所诟病，称之为“株洲[黑洞](../Page/黑洞.md "wikilink")”或者是“五里墩黑洞”。据统计，仅2014年3月底，株洲站每日办理晚点旅客列车98列（其中株洲分界口为每日32列），平均每列增加晚点时间9.3分钟，旅客列车正点率仅51%，严重影响运输秩序，降低干线运输效率，成为京广、沪昆大动脉的运输瓶颈。\[17\]自2014年下半年起，部分原先进株洲换向的列车改经[衡茶吉铁路](../Page/衡茶吉铁路.md "wikilink")（如[K537/538次](../Page/T25/26次列车.md "wikilink")）或[赣韶铁路](../Page/赣韶铁路.md "wikilink")（如[T169/170次](../Page/T169/170次列车.md "wikilink")），这种分流措施在一定程度上减轻了株洲枢纽的拥堵问题。\[18\]
[Outline_of_Zhuzhou_Railway_Hub_(Tianxin-Wulidun).png](https://zh.wikipedia.org/wiki/File:Outline_of_Zhuzhou_Railway_Hub_\(Tianxin-Wulidun\).png "fig:Outline_of_Zhuzhou_Railway_Hub_(Tianxin-Wulidun).png")

## 临近车站

|-    |-

## 参见

  - [株洲西站](../Page/株洲西站.md "wikilink")

## 参考文献

[Category:京广铁路车站](../Category/京广铁路车站.md "wikilink")
[Category:沪昆铁路车站](../Category/沪昆铁路车站.md "wikilink")
[Category:长株潭城际铁路车站](../Category/长株潭城际铁路车站.md "wikilink")
[Category:株洲市铁路车站](../Category/株洲市铁路车站.md "wikilink")
[Category:1910年啟用的鐵路車站](../Category/1910年啟用的鐵路車站.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.

13.
14.

15.

16.
17.

18.