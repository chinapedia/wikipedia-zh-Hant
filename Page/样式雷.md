**样式雷**是[清代负责主持皇家](../Page/清代.md "wikilink")[建筑设计的](../Page/建筑设计.md "wikilink")[雷氏建筑世家](../Page/雷姓.md "wikilink")，因长期掌管[样式房而得名](../Page/样式房.md "wikilink")。目前[中国世界遗产的建筑设计中有六分之一出自雷氏家族](../Page/中国世界遗产.md "wikilink")，自始祖[雷发达](../Page/雷发达.md "wikilink")（字明所，[江西省](../Page/江西省.md "wikilink")[南康府](../Page/南康府.md "wikilink")[建昌县人](../Page/建昌县_\(东汉\).md "wikilink")）起家族7代世袭清朝样式房掌案。

2007年，清代“样式雷”建筑图档入选[联合国教科文组织的](../Page/联合国教科文组织.md "wikilink")[世界记忆名录](../Page/世界记忆名录.md "wikilink")。\[1\]

## 雷氏家族历代样式房掌案

  - [雷发达](../Page/雷发达.md "wikilink")（1619年－1693年），[康熙二十二年](../Page/康熙.md "wikilink")（1683年）与堂弟雷发宣应募至北京。70岁左右退休回南京，葬于[江宁县](../Page/江宁县.md "wikilink")[安德门外项宝石的家族墓地](../Page/安德门.md "wikilink")。据考证，墓葬地点当在今[雨花台区](../Page/雨花台区.md "wikilink")[西善桥街道油坊村贾东一带](../Page/西善桥街道.md "wikilink")\[2\]。
  - [雷金玉](../Page/雷金玉.md "wikilink")（1659年－1729年），同样归葬于江宁县安德门外的家族墓地。
  - [雷声澂](../Page/雷声澂.md "wikilink")（1729年－1792年）
  - [雷家玺](../Page/雷家玺.md "wikilink")（1764年－1825年）
  - [雷景修](../Page/雷景修.md "wikilink")（1803年－1866年）
  - [雷思起](../Page/雷思起.md "wikilink")（1826年－1876年）
  - [雷廷昌](../Page/雷廷昌.md "wikilink")（1845年－1907年）

## 参考文献

[L](../Category/清朝家族.md "wikilink")
[Y](../Category/清朝皇家建筑.md "wikilink")
[Category:清朝建筑师](../Category/清朝建筑师.md "wikilink")
[Category:雷姓](../Category/雷姓.md "wikilink")
[Category:江西家族](../Category/江西家族.md "wikilink")
[Category:江苏家族](../Category/江苏家族.md "wikilink")

1.
2.