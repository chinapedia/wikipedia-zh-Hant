[RNase_P.png](https://zh.wikipedia.org/wiki/File:RNase_P.png "fig:RNase_P.png")

**核糖核酸酶P**（Ribonuclease P，简写为**RNase
P**）是一种[核糖核酸酶](../Page/核糖核酸酶.md "wikilink")。核糖核酸酶P也是一种[核酶](../Page/核酶.md "wikilink")，即由一个[RNA分子发挥](../Page/RNA.md "wikilink")[催化活性](../Page/催化.md "wikilink")，它是第一个被发现的[蛋白质以外具有催化活性的](../Page/蛋白质.md "wikilink")[生物大分子](../Page/生物大分子.md "wikilink")。它的功能是剪切[tRNA分子中RNA上多余的或前体的多余序列](../Page/tRNA.md "wikilink")。\[1\]它的发现者[悉尼·奥尔特曼在](../Page/悉尼·奥尔特曼.md "wikilink")1970年代在研究[前体tRNA中发现它可以剪切RNA的](../Page/tRNA.md "wikilink")5'端，悉尼·奥尔特曼也因此获得了1989年度的[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。\[2\]近期的研究发现核糖核酸酶P也具有一些新的功能，\[3\]例如人类[细胞核中的核糖核酸酶P对于正常并有效地](../Page/细胞核.md "wikilink")[转录多种](../Page/转录.md "wikilink")[非編碼RNA](../Page/非編碼RNA.md "wikilink")（包括tRNA、5S
rRNA、SRP RNA和U6
snRNA）的[基因是必要的](../Page/基因.md "wikilink")，\[4\]这些基因是由[RNA聚合酶III](../Page/RNA聚合酶.md "wikilink")（人类细胞中三类主要的RNA聚合酶之一）来进行转录。

## 细菌中

在[细菌中](../Page/细菌.md "wikilink")，如[大肠杆菌](../Page/大肠杆菌.md "wikilink")，核糖核酸酶P具有两个组分：一条RNA链（称为M1-RNA）和一个蛋白质（称为C5蛋白）。\[5\]\[6\]在细胞内，这两个组分对于功能的发挥都是必要的；但在体外（*in
vitro*），M1-RNA可以独自发挥催化的功能。\[7\]C5蛋白的作用主要是增强[底物结合的亲和力](../Page/底物.md "wikilink")，而[活性位点金属离子结合亲和力的提高很可能能够加强M](../Page/活性位点.md "wikilink")1-RNA的催化速率。细菌核糖核酸酶P的晶体结构显示它具有一个由共轴堆积的螺旋结构域构成的平整表面，这样一个平的表面有助于底物前体tRNA分子的结合和剪切。\[8\]

## 古菌中

在古菌中，核糖核酸酶P含有4-5个结合RNA的蛋白质亚基。体外重构实验显示，这些亚基中的每一个都通过核糖核酸酶P中的RNA组分参与对于tRNA的处理。\[9\]\[10\]\[11\]古菌核糖核酸酶P中的蛋白质亚基结构已经通过[X射线晶体学和](../Page/X射线晶体学.md "wikilink")[NMR被解析](../Page/NMR.md "wikilink")，进一步揭示了这些亚基结构与功能的关系。

## 真核生物中

在真核生物中，如人类和[酵母](../Page/酵母.md "wikilink")，核糖核酸酶P的RNA链与细菌中所发现的在结构上相似，\[12\]并含有9-10个与RNA链结合的[蛋白质亚基](../Page/蛋白质亚基.md "wikilink")，\[13\]\[14\]，其中5个亚基与古菌核糖核酸酶P中的5个亚基具有同源性。这些亚基同时也是核糖核酸酶MRP（[RNase
MRP](../Page/:en:RNase_MRP.md "wikilink")，一种催化性[核糖核蛋白](../Page/核糖核蛋白.md "wikilink")，参与细胞核中[核糖体RNA的剪切](../Page/核糖体RNA.md "wikilink")）的组成亚基。\[15\]\[16\]\[17\]真核生物的核糖核酸酶P中的RNA组分直到最近才被证明是核酶；\[18\]其蛋白质亚基自身对于底物tRNA的剪切只具有次要作用，\[19\]这些亚基可能是在核糖核酸酶P和核糖核酸酶MRP的其他功能（如基因转录和[细胞周期](../Page/细胞周期.md "wikilink")）中发挥主要作用。\[20\]\[21\]

## 参考文献

## 外部链接

  - [RNase P 数据库](http://www.mbio.ncsu.edu/RNaseP/threed.html)

  -
  -
[Category:核糖核酸酶](../Category/核糖核酸酶.md "wikilink")
[Category:核酶](../Category/核酶.md "wikilink")
[Category:RNA剪接](../Category/RNA剪接.md "wikilink")
[Category:核糖核蛋白](../Category/核糖核蛋白.md "wikilink")

1.

2.  [1989年度诺贝尔化学奖](http://nobelprize.org/chemistry/laureates/1989/)

3.

4.

5.

6.

7.
8.
9.

10.

11.

12.

13.
14.

15.
16.

17.

18.

19.

20.
21.