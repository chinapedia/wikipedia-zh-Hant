[Diagram_human_cell_nucleus_(zh-cn).svg](https://zh.wikipedia.org/wiki/File:Diagram_human_cell_nucleus_\(zh-cn\).svg "fig:Diagram_human_cell_nucleus_(zh-cn).svg")

**核膜**（ 或
），又称**核被膜**或**核封套**（）是包圍[真核细胞](../Page/真核细胞.md "wikilink")[細胞核](../Page/細胞核.md "wikilink")，分隔開细胞核和[细胞质的](../Page/细胞质.md "wikilink")[生物膜](../Page/生物膜.md "wikilink")。

## 结构和功能

核膜由两層[磷脂雙分子層构成](../Page/磷脂雙分子層.md "wikilink")，厚度共約20-100[nm](../Page/納米.md "wikilink")。其中兩層磷脂雙層膜分別稱作和，其中的空隙稱作[核周隙](../Page/核周隙.md "wikilink")（核周腔，perinuclear
space），其與[內質網空腔是共通的](../Page/內質網.md "wikilink")。

外核膜向細胞質的一側有[核糖體附著](../Page/核糖體.md "wikilink")，同時也可見到外膜與內質網膜有相連接的部位，因而使[核周隙](../Page/核周隙.md "wikilink")（perinuclear
space）與[粗面内质网腔相溝通](../Page/粗面内质网.md "wikilink")。由於核膜與內質網膜是連續的，核膜可通過與內質網的流動而得以生長，迅速擴大或收縮（詳見[流體鑲嵌模型](../Page/流動鑲嵌模型.md "wikilink")）
。而这种[內膜系統](../Page/內膜系統.md "wikilink")，磷脂雙層膜的共有共通性正是將細胞核膜（或稱核封套nuclear
envelope）、內質網、高基氏體、溶體、液泡和細胞膜歸類於內膜系統的原因。這些胞器的磷脂雙層膜是可以較輕易共通的，且磷脂雙層膜都是由內質網所製造的，而葉綠體與线粒体不被歸類於內膜系統，即是因為膜來源不是來自內質網，且和其他的膜系統少有共有共通的現象。

核膜圍繞細胞核，其外層與[內質網的相連接](../Page/內質網.md "wikilink")。核膜控制小[分子](../Page/分子.md "wikilink")[物質](../Page/物質.md "wikilink")（如[水](../Page/水.md "wikilink")、[無機鹽或](../Page/無機鹽.md "wikilink")[氨基酸等](../Page/氨基酸.md "wikilink")）的進出。但因有[核孔的存在](../Page/核孔.md "wikilink")，這些物質也可以自由進入細胞核內。

### 核孔

核膜上的[核孔控制](../Page/核孔.md "wikilink")[大分子物質](../Page/大分子.md "wikilink")（如[糖類](../Page/糖類.md "wikilink")、[蛋白質等](../Page/蛋白質.md "wikilink")）在細胞核與[細胞質間的進出](../Page/細胞質.md "wikilink")，使得這些物質無需通過[内吞作用便可進入細胞核內部](../Page/内吞作用.md "wikilink")，參與細胞核內的物質反應。同時核孔也是[信使RNA（mRNA）進出細胞核的通道](../Page/信使RNA.md "wikilink")。

{{-}}

[Category:核膜](../Category/核膜.md "wikilink")
[Category:核亚结构](../Category/核亚结构.md "wikilink")