**核酸結構**（）是指如[DNA与](../Page/DNA.md "wikilink")[RNA的](../Page/RNA.md "wikilink")[核酸](../Page/核酸.md "wikilink")。从化学角度上讲，DNA与RNA是非常相似的。[DNA与](../Page/DNA.md "wikilink")[RNA的结构常分为四个不同水平](../Page/RNA.md "wikilink")：一级、二级、三级及四级。

能夠直接計算出DNA機械性質的實驗技術相對較新，且要在溶液中進行高解析度觀察也較困難，不過科學家仍然解出了許多關於DNA機械性質的數據。關於DNA機械性質的研究，包括不同型態的DNA雙螺旋、[DNA超螺旋](../Page/DNA超螺旋.md "wikilink")、非螺旋型態、鹼基配對鍵結、[熔點等](../Page/熔點.md "wikilink")。

## 一级结构

一级结构是由通过[磷酸二酯键连接在一起的核苷酸的线性序列](../Page/磷酸二酯键.md "wikilink")。它是核苷酸的这种线性序列组成[DNA或](../Page/DNA.md "wikilink")[RNA的一级结构](../Page/RNA.md "wikilink")。核苷酸由3个组成部分组成：

1.  含氮基
    1.  [腺嘌呤](../Page/腺嘌呤.md "wikilink")
    2.  [鸟嘌呤](../Page/鸟嘌呤.md "wikilink")
    3.  [胞嘧啶](../Page/胞嘧啶.md "wikilink")
    4.  [胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")（仅存在于DNA中）
    5.  [尿嘧啶](../Page/尿嘧啶.md "wikilink")（仅存在于RNA中）
2.  被称为脱氧核糖（在[DNA中发现](../Page/DNA.md "wikilink")）和核糖（在[RNA中发现](../Page/RNA.md "wikilink")）的5碳糖。
3.  一个或多个磷酸基\[1\]。

## 二级结构

二级结构是[碱基之间的相互作用集合](../Page/碱基.md "wikilink")，即链的哪些部分彼此结合。
在DNA双螺旋中，两条DNA链通过[氢键连在一起](../Page/氢键.md "wikilink")。
一条链碱基上的[核苷酸与另一条链上的核苷酸成对配对](../Page/核苷酸.md "wikilink")。
二级结构负责核酸所承受的形状。DNA中的碱基被分类为[嘌呤和](../Page/嘌呤.md "wikilink")[嘧啶](../Page/嘧啶.md "wikilink")。嘌呤是[腺嘌呤和](../Page/腺嘌呤.md "wikilink")[鸟嘌呤](../Page/鸟嘌呤.md "wikilink")。嘌呤由双环结构，含氮的六元环和五元环组成。嘧啶是[胞嘧啶和](../Page/胞嘧啶.md "wikilink")[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")。它有一个单一的环状结构，有一个含氮六元环。嘌呤碱基总是与嘧啶碱基（[鸟嘌呤](../Page/鸟嘌呤.md "wikilink")（G）与[胞嘧啶](../Page/胞嘧啶.md "wikilink")（C），与[腺嘌呤](../Page/腺嘌呤.md "wikilink")（A）与[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")（T）或[尿嘧啶](../Page/尿嘧啶.md "wikilink")（U））配对。DNA的二级结构主要通过彼此缠绕的两条多核苷酸链的[碱基对来确定](../Page/碱基对.md "wikilink")，以形成[雙股螺旋](../Page/雙股螺旋.md "wikilink")。

## 三级结构

三级结构是指原子在三维空间中的位置，考虑到几何和[位阻效应空间限制](../Page/位阻效应.md "wikilink")。
它比二级结构更高一级，其中发生线性聚合物的大规模折叠，并且整个链条被折叠成特定的三维形状。DNA的结构形式有4个不同区域。

1.  手性 - 右手性或左手性
2.  螺旋匝的长度
3.  每匝碱基对数
4.  主凹槽与次凹槽之间的尺寸差异

DNA的[双股螺旋空间的三级排列包括B](../Page/双股螺旋.md "wikilink")-DNA，[A-DNA和](../Page/A-DNA.md "wikilink")[Z-DNA](../Page/Z-DNA.md "wikilink")。

## 四级结构

核酸的四级结构与[蛋白质四级结构相似](../Page/蛋白质四级结构.md "wikilink")。
虽然一些概念不完全相同，但四级结构是指较高级别的核酸组织。
此外，它是指核酸与其他分子的相互作用。
核酸的较高级组织的最常见形式以[染色质的形式出现](../Page/染色质.md "wikilink")，这导致其与小蛋白质[组蛋白的相互作用](../Page/组蛋白.md "wikilink")。
此外，四元结构是指[核糖体或](../Page/核糖体.md "wikilink")[剪接体中分离的RNA单元之间的相互作用](../Page/剪接体.md "wikilink")\[2\]。

## DNA螺旋幾何

据信在自然界中发现了至少三种DNA构象，即A-DNA，B-DNA和Z-DNA。

[Dnaconformations.png](https://zh.wikipedia.org/wiki/File:Dnaconformations.png "fig:Dnaconformations.png")
[B\&Z\&A_DNA_formula.jpg](https://zh.wikipedia.org/wiki/File:B&Z&A_DNA_formula.jpg "fig:B&Z&A_DNA_formula.jpg")

<table>
<caption>三種主要DNA型態的結構特色</caption>
<thead>
<tr class="header">
<th><p>幾何學特性</p></th>
<th><p>A-DNA</p></th>
<th><p>B-DNA</p></th>
<th><p>Z-DNA</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>螺旋方向</p></td>
<td><p>向右</p></td>
<td><p>向右</p></td>
<td><p>向左</p></td>
</tr>
<tr class="even">
<td><p>重複單位</p></td>
<td><p>1 bp</p></td>
<td><p>1 bp</p></td>
<td><p>2 bp</p></td>
</tr>
<tr class="odd">
<td><p>每单位转角</p></td>
<td><p>33.6°</p></td>
<td><p>35.9°</p></td>
<td><p>60°/2bp</p></td>
</tr>
<tr class="even">
<td><p>平均旋转一周所用单位数</p></td>
<td><p>10.7</p></td>
<td><p>10.0</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p>Inclination of bp to axis</p></td>
<td><p>+19°</p></td>
<td><p>-1.2°</p></td>
<td><p>-9°</p></td>
</tr>
<tr class="even">
<td><p>轴向每单位位移</p></td>
<td><p>2.3 Å</p></td>
<td><p>3.32 Å</p></td>
<td><p>3.8 Å</p></td>
</tr>
<tr class="odd">
<td><p>每旋转一周位移</p></td>
<td><p>24.6 Å</p></td>
<td><p>33.2 Å</p></td>
<td><p>45.6 Å</p></td>
</tr>
<tr class="even">
<td><p>Mean propeller twist</p></td>
<td><p>+18°</p></td>
<td><p>+16°</p></td>
<td><p>0°</p></td>
</tr>
<tr class="odd">
<td><p>糖基角</p></td>
<td><p>anti</p></td>
<td><p>anti</p></td>
<td><p><strong>C</strong>: anti,<br />
<strong>G</strong>: syn</p></td>
</tr>
<tr class="even">
<td><p>Sugar pucker</p></td>
<td><p>C3'-endo</p></td>
<td><p>C2'-endo</p></td>
<td><p><strong>C</strong>: C2'-endo,<br />
<strong>G</strong>: C3'-endo</p></td>
</tr>
<tr class="odd">
<td><p>直径</p></td>
<td><p>25.5 Å</p></td>
<td><p>23.7 Å</p></td>
<td><p>18.4 Å</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参看

  -
  - [双股螺旋](../Page/双股螺旋.md "wikilink")

  -
  - [DNA纳米技术](../Page/DNA纳米技术.md "wikilink")

  -
  - [核酸热力学](../Page/核酸热力学.md "wikilink")

  - [DNA超螺旋](../Page/DNA超螺旋.md "wikilink")

  - [蛋白质结构](../Page/蛋白质结构.md "wikilink")

## 参考文献

[Category:DNA](../Category/DNA.md "wikilink")
[Category:RNA](../Category/RNA.md "wikilink")

1.
2.