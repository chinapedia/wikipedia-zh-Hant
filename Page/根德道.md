[Kent_Road_No_27_2018.jpg](https://zh.wikipedia.org/wiki/File:Kent_Road_No_27_2018.jpg "fig:Kent_Road_No_27_2018.jpg")
**根德道**（）是[香港](../Page/香港.md "wikilink")[九龍一條道路](../Page/九龍.md "wikilink")，為[九龍塘的主要道路](../Page/九龍塘.md "wikilink")，是為[單程路](../Page/單程路.md "wikilink")，北起[歌和老街](../Page/歌和老街.md "wikilink")，南至[多福道及](../Page/多福道.md "wikilink")[沙福道交界](../Page/沙福道.md "wikilink")。沿路有[耀中國際學校及其他豪宅區等](../Page/耀中國際學校.md "wikilink")。根德道也是很多[時鐘酒店及交通的所在地](../Page/時鐘酒店.md "wikilink")。

在繁忙時間根德道十分擠塞，在近[九龍塘站路段因小巴多於此處落客](../Page/九龍塘站_\(香港\).md "wikilink")，過馬路前往對面鐵路車站的人龍很多，而且沒有[交通燈指導橫過馬路](../Page/交通燈.md "wikilink")，以致險象環生。根德道與[森麻實道的交匯處也是著名的約人地點](../Page/森麻實道.md "wikilink")，因地處交通方便，多架巴士，地鐵及火車可以到達，也有很多[內地人在此等候直通車前往內地](../Page/內地人.md "wikilink")。

根德道以英國的[根德郡](../Page/根德郡.md "wikilink")（又稱肯特郡）命名。

## 沿路景點

  - [根德道花園](../Page/根德道花園.md "wikilink")
  - [又一城](../Page/又一城.md "wikilink") -
    可沿[東鐵綫天橋前往](../Page/東鐵綫.md "wikilink")
  - [理想酒店](../Page/理想酒店.md "wikilink")
  - [九龍塘站](../Page/九龍塘站_\(香港\).md "wikilink")
  - [九龍塘公共交通交匯處](../Page/九龍塘公共交通交匯處.md "wikilink")

## 道路支路

由北至南數起。

  - [森麻實道](../Page/森麻實道.md "wikilink") - 往耀中國際學校
  - [真光里](../Page/真光里.md "wikilink") -
    往[九廣鐵路天橋](../Page/九廣鐵路.md "wikilink")，[又一城](../Page/又一城.md "wikilink")，[九龍真光中學](../Page/九龍真光中學.md "wikilink")
  - [沙福道](../Page/沙福道.md "wikilink") -
    往[窩打老道](../Page/窩打老道.md "wikilink")
  - [多福道](../Page/多福道.md "wikilink") -
    往[羅福道及](../Page/羅福道.md "wikilink")[律倫街住宅區](../Page/律倫街.md "wikilink")

## 相關條目

  - [九龍塘公共交通交匯處](../Page/九龍塘公共交通交匯處.md "wikilink")

[Category:九龍塘街道](../Category/九龍塘街道.md "wikilink")