**根纳季·伊万诺维奇·亚纳耶夫**（，\[1\]），[俄罗斯政治家](../Page/俄罗斯.md "wikilink")，[蘇聯首任](../Page/蘇聯.md "wikilink")[副總統](../Page/副總統.md "wikilink")\[2\]，[八一九事件策劃者之一](../Page/八一九事件.md "wikilink")。

## 生平

亚纳耶夫出生於俄羅斯[高尔基州彼列沃兹村](../Page/下諾夫哥羅德州.md "wikilink")，1962年加入[苏联共产党](../Page/苏联共产党.md "wikilink")\[3\]，1968年任[苏联共青团中央委员会主席](../Page/苏联共青团.md "wikilink")，1990年7月进入[蘇聯共產黨中央政治局](../Page/蘇聯共產黨中央政治局.md "wikilink")，12月出任苏联副总统。

1991年8月19日，亚纳耶夫在苏共党内强硬派的支持下发动[八一九政变](../Page/八一九事件.md "wikilink")，宣布[蘇聯總統](../Page/蘇聯總統.md "wikilink")[米哈伊尔·戈尔巴乔夫因病停职](../Page/米哈伊尔·戈尔巴乔夫.md "wikilink")，自己出任苏联代总统。21日戈爾巴喬夫重新就任總統，政变失败\[4\]。亚纳耶夫等人因叛國罪被逮捕\[5\]，並被關押在[莫斯科](../Page/莫斯科.md "wikilink")“水兵寂静”看守所\[6\]。

1994年2月获得[俄罗斯国家杜马](../Page/俄罗斯国家杜马.md "wikilink")[大赦出狱](../Page/大赦.md "wikilink")\[7\]。之後據媒體報道，亚纳耶夫曾担任国家机关老战士和残疾人委员会顾问，俄罗斯国际旅游学院俄国历史和国际关系教研室主任。2010年9月24日病逝于俄羅斯莫斯科一家醫院，终年73岁。\[8\]

## 參考資料

[分類:罹患肺癌逝世者](../Page/分類:罹患肺癌逝世者.md "wikilink")

[Category:勞動紅旗勳章獲得者](../Category/勞動紅旗勳章獲得者.md "wikilink")
[Category:榮譽徽章勳章獲得者](../Category/榮譽徽章勳章獲得者.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:蘇聯共產黨中央政治局委員](../Category/蘇聯共產黨中央政治局委員.md "wikilink")
[Category:冷戰人物](../Category/冷戰人物.md "wikilink")
[Category:下諾夫哥羅德州人](../Category/下諾夫哥羅德州人.md "wikilink")
[Category:被开除苏联共产党党籍者](../Category/被开除苏联共产党党籍者.md "wikilink")

1.

2.

3.
4.
5.

6.

7.
8.