**格伦·L·马丁公司**（Glenn L. Martin
Company）是[格伦·卢瑟·马丁于](../Page/格伦·卢瑟·马丁.md "wikilink")1912年8月16日创办的一家[飞行器公司](../Page/飞行器.md "wikilink")。马丁公司在创办初期在[加利福尼亚的](../Page/加利福尼亚.md "wikilink")[圣塔安娜制造军用教练机](../Page/圣塔安娜.md "wikilink")。1916年同莱特公司合并成为**[莱特－马丁飞行器公司](../Page/莱特－马丁公司.md "wikilink")**。但马丁后来离开并于1917年9月10日在[俄亥俄的](../Page/俄亥俄.md "wikilink")[克利夫兰创办了第二家格伦](../Page/克利夫兰.md "wikilink")·L·马丁公司。马丁公司的第一个成功产品是名为MB-1的双翼轰炸机。[美国陆军在](../Page/美国陆军.md "wikilink")1918年1月17日定购了这种飞机。[威廉·爱德华·波音](../Page/威廉·爱德华·波音.md "wikilink")，[詹姆士·史密斯·麦克唐纳](../Page/詹姆士·史密斯·麦克唐纳.md "wikilink")，[唐纳德·维尔斯·道格拉斯](../Page/老唐纳德·威尔士·道格拉斯.md "wikilink")，[劳伦斯·贝尔等著名航空企业家都曾经是格伦](../Page/劳伦斯·贝尔.md "wikilink")·L·马丁公司的雇员。

## 历史

  - 1924年， 马丁公司获得了由柯蒂斯设计侦察轰炸机SC-1的订单，这种飞机最终生产了404架。
  - 1929年，马丁公司离开克利夫兰，在[马里兰](../Page/马里兰.md "wikilink")[巴尔的摩东北的Middle](../Page/巴尔的摩.md "wikilink")
    River建立了新的工厂。
  - [20世纪30年代](../Page/20世纪30年代.md "wikilink")，马丁公司为[美国海军生产了水上飞机](../Page/美国海军.md "wikilink")，并为美国陆军生产了B-10轰炸机。这一段时间，马丁公司还为[泛美航空公司的](../Page/泛美航空公司.md "wikilink")[旧金山到](../Page/旧金山.md "wikilink")[马尼拉航线生产了著名的](../Page/马尼拉.md "wikilink")**中国快船**水上飞机。
  - [第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，马丁公司的成功产品包括B-26掠夺者型轰炸机、A-22马里兰型轰炸机等。战后的著名产品包括B-57堪培拉型夜间轰炸机等。
  - 马丁公司于1961年同美国玛丽埃塔公司合并成为[马丁·玛丽埃塔公司](../Page/马丁·玛丽埃塔.md "wikilink")，最终于1995年同[洛克西德公司合并成为](../Page/洛克西德公司.md "wikilink")[洛克西德·马丁](../Page/洛克西德·马丁.md "wikilink")。

[Category:已结业飞机制造商](../Category/已结业飞机制造商.md "wikilink")
[Category:美国飞机公司](../Category/美国飞机公司.md "wikilink")
[Category:1912年成立的公司](../Category/1912年成立的公司.md "wikilink")
[Category:1961年廢除](../Category/1961年廢除.md "wikilink")