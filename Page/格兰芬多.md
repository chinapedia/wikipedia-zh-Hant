<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gryffindor.jpg" title="fig:Gryffindor crest">Gryffindor crest</a><br />
</p>
<center>
<p><small>葛萊分多徽</p></td>
</tr>
<tr class="odd">
<td><p><strong>-{格蘭芬多}-</strong><br />
<strong>-{葛萊分多}-</strong></p></td>
</tr>
<tr class="even">
<td><p>學院創立者</p></td>
</tr>
<tr class="odd">
<td><p><strong>學院<a href="../Page/圖騰.md" title="wikilink">圖騰</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong>學院院長</strong></p></td>
</tr>
<tr class="odd">
<td><p>學院顏色</p></td>
</tr>
<tr class="even">
<td><p><strong>選擇<a href="../Page/學生.md" title="wikilink">學生方式</a></strong></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**葛萊分多**（），-{zh-hans:台湾译名; zh-hant:中國大陸譯名;}-為-{zh-hans:**葛萊分多**;
zh-hant:**格蘭芬多**;}-，是奇幻小說《[哈利·波特](../Page/哈利·波特.md "wikilink")》系列中，[霍格華茲魔法與巫術學院的四大學院之一](../Page/霍格華茲魔法與巫術學院.md "wikilink")，象徵[四大元素](../Page/四大元素.md "wikilink")，[火](../Page/火.md "wikilink")。由[高錐客·葛萊分多創立並以其命名](../Page/高錐客·葛萊分多.md "wikilink")。以勇敢為擇生條件，培養多著名[巫師](../Page/巫師.md "wikilink")，如：[阿不思·鄧不利多](../Page/阿不思·鄧不利多.md "wikilink")。書中主角[哈利·波特亦屬於該學院](../Page/哈利·波特_\(小說角色\).md "wikilink")。

## 位置

葛萊分多塔的入口位於五樓胖女士畫像，而交誼廳（公共休息室）則於葛萊分多塔八樓。

## 物品

葛萊分多寶劍是妖精製造，屬於高錐客·葛萊分多的遺物。在[消失的密室中](../Page/消失的密室.md "wikilink")，[哈利波特從分類帽拔出葛萊分多寶劍](../Page/哈利波特.md "wikilink")，並利用它來擊敗蛇妖。在[混血王子和死神的聖物中](../Page/混血王子.md "wikilink")，[鄧不利多和](../Page/鄧不利多.md "wikilink")[哈利波特都用它來破壞](../Page/哈利波特.md "wikilink")[佛地魔的](../Page/佛地魔.md "wikilink")[分靈體](../Page/分靈體.md "wikilink")。葛來分多寶劍只會在真正傳人面前出現，但傳人並非單指一個人，而是他有足夠勇氣時劍就會出現在他的面前，例如[哈利波特和](../Page/哈利波特.md "wikilink")[奈威·隆巴頓](../Page/奈威·隆巴頓.md "wikilink")。

## 代表人物

  - [高錐客·葛萊分多](../Page/高錐客·葛來分多.md "wikilink")
  - [阿不思·鄧不利多](../Page/阿不思·鄧不利多.md "wikilink")
  - [哈利·波特](../Page/哈利·波特_\(小說角色\).md "wikilink")

[category:哈利波特](../Page/category:哈利波特.md "wikilink")

[ca:Residències de
Hogwarts\#Gryffindor](../Page/ca:Residències_de_Hogwarts#Gryffindor.md "wikilink")
[cs:Škola čar a kouzel v
Bradavicích\#Nebelvír](../Page/cs:Škola_čar_a_kouzel_v_Bradavicích#Nebelvír.md "wikilink")
[da:Hogwarts\#Gryffindor](../Page/da:Hogwarts#Gryffindor.md "wikilink")
[de:Handlungsorte der
Harry-Potter-Romane\#Gryffindor](../Page/de:Handlungsorte_der_Harry-Potter-Romane#Gryffindor.md "wikilink")
[el:Τοποθεσίες της σειράς Χάρι
Πότερ\#Γκρίφιντορ](../Page/el:Τοποθεσίες_της_σειράς_Χάρι_Πότερ#Γκρίφιντορ.md "wikilink")
[en:Hogwarts\#Gryffindor](../Page/en:Hogwarts#Gryffindor.md "wikilink")
[he:הוגוורטס\#גריפינדור](../Page/he:הוגוורטס#גריפינדור.md "wikilink")
[hr:Domovi u
Hogwartsu\#Gryffindor](../Page/hr:Domovi_u_Hogwartsu#Gryffindor.md "wikilink")
[hu:Roxforti
házak\#Griffendél](../Page/hu:Roxforti_házak#Griffendél.md "wikilink")
[id:Asrama
Hogwarts\#Gryffindor](../Page/id:Asrama_Hogwarts#Gryffindor.md "wikilink")
[it:Case di
Hogwarts\#Grifondoro](../Page/it:Case_di_Hogwarts#Grifondoro.md "wikilink")
[ms:Rumah
Hogwarts\#Gryffindor](../Page/ms:Rumah_Hogwarts#Gryffindor.md "wikilink")
[no:Husene ved Galtvort høyere skole for hekseri og
trolldom\#Griffing](../Page/no:Husene_ved_Galtvort_høyere_skole_for_hekseri_og_trolldom#Griffing.md "wikilink")
[simple:Hogwarts\#Gryffindor](../Page/simple:Hogwarts#Gryffindor.md "wikilink")
[tr:Hogwarts\#Gryffindor](../Page/tr:Hogwarts#Gryffindor.md "wikilink")

[Category:霍格華茲](../Category/霍格華茲.md "wikilink")