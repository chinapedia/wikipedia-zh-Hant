[Gerhard_Gentzen.jpg](https://zh.wikipedia.org/wiki/File:Gerhard_Gentzen.jpg "fig:Gerhard_Gentzen.jpg")
**格哈德·根岑**（，）是[德国的](../Page/德国.md "wikilink")[数学家和](../Page/数学家.md "wikilink")[逻辑学家](../Page/逻辑学家.md "wikilink")。

他生于德国的[格赖夫斯瓦尔德](../Page/格赖夫斯瓦尔德.md "wikilink")，在1929年到1933年期间是[赫尔曼·外尔在](../Page/赫尔曼·魏尔.md "wikilink")[哥廷根大学的学生之一](../Page/哥廷根大学.md "wikilink")。在1934年到1943年間他是[大卫·希尔伯特在哥廷根大学的助手](../Page/大卫·希尔伯特.md "wikilink")。從1943年起他是布拉格大學的教授。\[1\]他的主要工作是[数学基础中的](../Page/数学基础.md "wikilink")[证明论](../Page/证明论.md "wikilink")，特别是[自然演绎和](../Page/自然演绎.md "wikilink")[相继式演算](../Page/相继式演算.md "wikilink")。他的[切消定理是](../Page/切消定理.md "wikilink")[证明论语义的基石](../Page/证明论语义.md "wikilink")，《逻辑演绎研究》中的某些哲学评论和[维特根斯坦的格言](../Page/维特根斯坦.md "wikilink")"意义是使用"一起建立了[推论角色语义的基础](../Page/推论角色语义.md "wikilink")。

他是[納粹黨和](../Page/納粹黨.md "wikilink")[沖鋒隊的成員](../Page/納粹衝鋒隊.md "wikilink")，在1945年5月7日隨所有在布拉格的德國人一起被逮捕之后，饿死于[布拉格附近的战俘营中](../Page/布拉格.md "wikilink")。

## 引用

  - Eckart Menzler-Trott. *Gentzens Problem: Mathematische Logik im
    nationalsozialistischen Deutschland.* Birkhäuser Verlag, 2001. ISBN
    3-7643-6574-9. An English translation is planned.

<!-- end list -->

  - M. E. Szabo. *Collected Papers of Gerhard Gentzen.* North-Holland,
    1969.

## 外部链接

  -
[Category:20世紀數學家](../Category/20世紀數學家.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:納粹德國人物](../Category/納粹德國人物.md "wikilink")
[Category:德国数学家](../Category/德国数学家.md "wikilink")
[Category:逻辑学家](../Category/逻辑学家.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:梅克倫堡-前波門人](../Category/梅克倫堡-前波門人.md "wikilink")

1.  [Gerhard
    Gentzen](http://math.muni.cz/math/biografie/gerhard_gentzen.html) at
    math.muni.cz