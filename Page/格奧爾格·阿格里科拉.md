[Georg_Agricola.jpg](https://zh.wikipedia.org/wiki/File:Georg_Agricola.jpg "fig:Georg_Agricola.jpg")
**格奥爾格乌斯·阿格里科拉**（，原名\[1\]，將姓名[拉丁化乃當時一种風尚](../Page/拉丁化.md "wikilink")。），[德國學者](../Page/德國.md "wikilink")，被誉为“[矿物学之父](../Page/矿物学.md "wikilink")”。

## 生平

阿格里科拉生於[萨克森的](../Page/萨克森.md "wikilink")[格勞豪](../Page/格勞豪.md "wikilink")，早年在[萊比錫學習古典語文](../Page/萊比錫.md "wikilink")，後到[義大利學醫](../Page/義大利.md "wikilink")，回國後將興趣轉到礦物學。1530年移居到採礦業發達的[開姆尼茨作研究](../Page/開姆尼茨.md "wikilink")，曾擔任當地的市醫、市長，1555年卒於當地。時[新教運動在開姆尼茨如火如荼](../Page/宗教改革.md "wikilink")，由於阿格里科拉堅持天主教信仰，死後受當地新教徒的反對而無法葬於當地。

1556年，阿格里科拉的遺作《[論礦冶](../Page/論礦冶.md "wikilink")》出版，這部著作被譽為西方礦物學的開山之作，[汤若望曾译成中文](../Page/汤若望.md "wikilink")，名为《[坤舆格致](../Page/坤舆格致.md "wikilink")》。1912年，當時還是工程師的[胡佛將此書譯成](../Page/赫伯特·胡佛.md "wikilink")[英文](../Page/英文.md "wikilink")，刊於《礦冶雜志》（*Mining
Magazine*）上。

## 外部連結

<references/>

  - [《論礦冶》的拉丁文原文](https://web.archive.org/web/20110926233013/http://archimedes.mpiwg-berlin.mpg.de/cgi-bin/toc/toc.cgi?dir=agric_remet_001_la_1556)

[Category:德国文献学家](../Category/德国文献学家.md "wikilink")
[Category:德國礦物學家](../Category/德國礦物學家.md "wikilink")
[Category:德國地質學家](../Category/德國地質學家.md "wikilink")
[Category:萊比錫大學校友](../Category/萊比錫大學校友.md "wikilink")
[Category:薩克森人](../Category/薩克森人.md "wikilink")

1.  阿格里科拉研究中心: "Wer ist Georgius Agricula?"