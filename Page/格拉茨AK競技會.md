**格拉茨AK競技會**（簡稱：格拉茨AK，英文：Grazer AK，德語：Grazer Athletik
Klub）創於1902年8月18日，當時稱為Grazer Athletik-Sport
Klub（在[奧地利](../Page/奧地利.md "wikilink")"GAK" 及
"Athletiker"這兩個簡稱是比較常用），位於[施蒂利亞州的](../Page/施蒂利亞州.md "wikilink")[格拉茨市內](../Page/格拉茨.md "wikilink")，是奧地利最著名的[俱樂部之一](../Page/俱樂部.md "wikilink")，在1995至2005年間是格拉茨AK最成功的時期。而格拉茨AK設有[籃球](../Page/籃球.md "wikilink")、[跳水及](../Page/跳水.md "wikilink")[乒乓部](../Page/乒乓.md "wikilink")。

## 球會破產

在2006-2007球季，格拉茨AK進入管理上失調狀態，被[奧地利足球協會扣掉](../Page/奧地利足球協會.md "wikilink")28分，俱樂部將會再不參加[頂級聯賽](../Page/奧地利足球超級聯賽.md "wikilink")。\[1\]

## 球會榮譽

| <font color="white"> **榮譽**                      | <font color="white"> **名次** | <font color="white"> **次數** | <font color="white"> **年度** |
| ------------------------------------------------ | --------------------------- | --------------------------- | --------------------------- |
| **[奧地利足球超級聯賽](../Page/奧地利足球超級聯賽.md "wikilink")** | **冠軍**                      | **1**                       | 2004                        |
| **亞軍**                                           | **2**                       | 2003, 2005                  |                             |
| **[奧地利盃](../Page/奧地利盃.md "wikilink")**           | **冠軍**                      | **1**                       | 2004                        |
| **亞軍**                                           | **2**                       | 1962, 1968                  |                             |
| **[奧地利超級盃](../Page/奧地利超級盃.md "wikilink")**       | **冠軍**                      | **2**                       | 2000, 2002                  |
| **亞軍**                                           | **1**                       | 2004                        |                             |

## 球員名單

## 重要統計

### 歐洲賽

<table style="width:133%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 19%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 47%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font color="white"> <strong>球季</strong></p></td>
<td style="text-align: center;"><p><font color="white"> <strong>比賽</strong></p></td>
<td style="text-align: center;"><p><font color="white"> <strong>階段</strong></p></td>
<td style="text-align: center;"><p><font color="white"> <strong>對手</strong></p></td>
<td style="text-align: center;"><p><font color="white"> <strong>比數</strong></p></td>
<td style="text-align: center;"><p><font color="white"> <strong>備注</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font color="white">主場</p></td>
<td style="text-align: center;"><p><font color="white">客場</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1962/63</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/歐洲盃賽冠軍盃.md" title="wikilink">歐洲盃賽冠軍盃</a></strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/堡魯本1909.md" title="wikilink">堡魯本1909</a></p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"><p>3-5</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1964/65</p></td>
<td style="text-align: center;"><p><strong>城市博覽盃</strong>（<strong><a href="../Page/歐洲足協盃.md" title="wikilink">歐洲足協盃</a></strong>前身）</p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/薩格勒布.md" title="wikilink">薩格勒布</a></p></td>
<td style="text-align: center;"><p>0-6</p></td>
<td style="text-align: center;"><p>2-3</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1968/69</p></td>
<td style="text-align: center;"><p><strong>歐洲盃賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/ADO海牙.md" title="wikilink">海牙</a></p></td>
<td style="text-align: center;"><p>0-2</p></td>
<td style="text-align: center;"><p>1-4</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1973/74</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/彭拿夏基.md" title="wikilink">彭拿夏基</a></p></td>
<td style="text-align: center;"><p>0-1</p></td>
<td style="text-align: center;"><p>1-2</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1981/82</p></td>
<td style="text-align: center;"><p><strong>歐洲盃賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/第比利斯戴拿模.md" title="wikilink">第比利斯戴拿模</a></p></td>
<td style="text-align: center;"><p>2-2</p></td>
<td style="text-align: center;"><p>0-2</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1982/83</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/胡內杜華拉.md" title="wikilink">胡內杜華拉</a></p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"><p>0-3</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1996/97</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>資格賽</p></td>
<td style="text-align: center;"><p><a href="../Page/伏伊伏丁那足球俱樂部.md" title="wikilink">禾獲甸拿</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>5-1</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/熱米納爾比爾肖特.md" title="wikilink">比斯查</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>1-3</p></td>
<td style="text-align: center;"><p>格拉茨AK憑<a href="../Page/作客入球優惠.md" title="wikilink">作客入球優惠晉級</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/國際米蘭.md" title="wikilink">國際米蘭</a></p></td>
<td style="text-align: center;"><p>1-0</p></td>
<td style="text-align: center;"><p>0-1</p></td>
<td style="text-align: center;"><p>國際米蘭馮互射12碼以5-3晉級</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1998/99</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>資格賽</p></td>
<td style="text-align: center;"><p><a href="../Page/VPS瓦薩.md" title="wikilink">VPS華沙</a></p></td>
<td style="text-align: center;"><p>3-0</p></td>
<td style="text-align: center;"><p>0-0</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/列迪斯洛維治.md" title="wikilink">列迪斯洛維治</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/摩納哥足球俱樂部.md" title="wikilink">摩納哥</a></p></td>
<td style="text-align: center;"><p>3-3</p></td>
<td style="text-align: center;"><p>0-4</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1999/00</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>資格賽</p></td>
<td style="text-align: center;"><p><a href="../Page/KI克拉克斯維克.md" title="wikilink">拉克斯域</a></p></td>
<td style="text-align: center;"><p>4-0</p></td>
<td style="text-align: center;"><p>5-0</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/泰拿華斯巴達.md" title="wikilink">泰拿華斯巴達</a></p></td>
<td style="text-align: center;"><p>3-0</p></td>
<td style="text-align: center;"><p>1-2</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/彭拿典奈高斯.md" title="wikilink">彭拿典奈高斯</a></p></td>
<td style="text-align: center;"><p>2-1</p></td>
<td style="text-align: center;"><p>0-1</p></td>
<td style="text-align: center;"><p>彭拿典奈高斯憑作客入球優惠晉級</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2000/01</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/MFK高斯錫.md" title="wikilink">MFK高斯錫</a></p></td>
<td style="text-align: center;"><p>0-0</p></td>
<td style="text-align: center;"><p>3-2</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/愛斯賓奴.md" title="wikilink">愛斯賓奴</a></p></td>
<td style="text-align: center;"><p>1-0</p></td>
<td style="text-align: center;"><p>0-4</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2001/02</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>資格賽</p></td>
<td style="text-align: center;"><p><a href="../Page/HB拖錫雲.md" title="wikilink">HB拖錫雲</a></p></td>
<td style="text-align: center;"><p>4-0</p></td>
<td style="text-align: center;"><p>2-2</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/烏德勒支足球會.md" title="wikilink">烏德勒支</a></p></td>
<td style="text-align: center;"><p>3-3</p></td>
<td style="text-align: center;"><p>0-3</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2002/03</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/歐洲聯賽冠軍盃.md" title="wikilink">歐洲聯賽冠軍盃</a></strong></p></td>
<td style="text-align: center;"><p>外圍賽第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/舒列夫.md" title="wikilink">舒列夫</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>4-1</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲聯賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>外圍賽第三圈</p></td>
<td style="text-align: center;"><p><a href="../Page/莫斯科火車頭.md" title="wikilink">莫斯科火車頭</a></p></td>
<td style="text-align: center;"><p>0-2</p></td>
<td style="text-align: center;"><p>3-3</p></td>
<td style="text-align: center;"><p>降到歐洲足協盃第一圈繼續參賽</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/尼科西亞希臘人競技足球俱樂部.md" title="wikilink">阿普爾</a></p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"><p>0-2</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2003/04</p></td>
<td style="text-align: center;"><p><strong>歐洲聯賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>外圍賽第二圈</p></td>
<td style="text-align: center;"><p><a href="../Page/地拉拿.md" title="wikilink">地拉拿</a></p></td>
<td style="text-align: center;"><p>2-1</p></td>
<td style="text-align: center;"><p>5-1</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲聯賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>外圍賽第三圈</p></td>
<td style="text-align: center;"><p><a href="../Page/阿積士.md" title="wikilink">阿積士</a></p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"><p>1-2*</p></td>
<td style="text-align: center;"><p>*阿積士馮<a href="../Page/銀球.md" title="wikilink">銀球制優惠以總比數</a>3-2晉級<br />
降到歐洲足協盃第一圈繼續參賽</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/華拉倫加.md" title="wikilink">華拉倫加</a></p></td>
<td style="text-align: center;"><p>1-1</p></td>
<td style="text-align: center;"><p>0-0</p></td>
<td style="text-align: center;"><p>華拉倫加憑作客入球優惠晉級</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2004/05</p></td>
<td style="text-align: center;"><p><strong>歐洲聯賽冠軍盃</strong></p></td>
<td style="text-align: center;"><p>外圍賽第三圈</p></td>
<td style="text-align: center;"><p><a href="../Page/利物浦足球俱樂部.md" title="wikilink">利物浦</a></p></td>
<td style="text-align: center;"><p>0-2</p></td>
<td style="text-align: center;"><p>1-0</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第三圈</p></td>
<td style="text-align: center;"><p><a href="../Page/列迪斯洛維治.md" title="wikilink">列迪斯洛維治</a></p></td>
<td style="text-align: center;"><p>5-0</p></td>
<td style="text-align: center;"><p>0-1</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>分組賽</p></td>
<td style="text-align: center;"><p><a href="../Page/歐塞爾足球俱樂部.md" title="wikilink">歐塞爾</a></p></td>
<td style="text-align: center;"><p>/</p></td>
<td style="text-align: center;"><p>0-0</p></td>
<td style="text-align: center;"><p>分組賽只打<a href="../Page/單循環.md" title="wikilink">單循環</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>分組賽</p></td>
<td style="text-align: center;"><p><a href="../Page/阿馬卡.md" title="wikilink">阿馬卡</a></p></td>
<td style="text-align: center;"><p>3-1</p></td>
<td style="text-align: center;"><p>/</p></td>
<td style="text-align: center;"><p>分組賽只打<a href="../Page/單循環.md" title="wikilink">單循環</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>分組賽</p></td>
<td style="text-align: center;"><p><a href="../Page/流浪者足球俱樂部.md" title="wikilink">格拉斯哥浪流</a></p></td>
<td style="text-align: center;"><p>/</p></td>
<td style="text-align: center;"><p>0-3</p></td>
<td style="text-align: center;"><p>分組賽只打<a href="../Page/單循環.md" title="wikilink">單循環</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>分組賽</p></td>
<td style="text-align: center;"><p><a href="../Page/AZ阿爾克馬爾.md" title="wikilink">阿爾克馬爾</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>/</p></td>
<td style="text-align: center;"><p>分組賽只打<a href="../Page/單循環.md" title="wikilink">單循環</a><br />
格拉茨AK以小組第三名晉級</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>三十二強</p></td>
<td style="text-align: center;"><p><a href="../Page/米杜士堡.md" title="wikilink">米杜士堡</a></p></td>
<td style="text-align: center;"><p>2-2</p></td>
<td style="text-align: center;"><p>1-2</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2005/06</p></td>
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>資格賽</p></td>
<td style="text-align: center;"><p><a href="../Page/奧泰斯.md" title="wikilink">奧泰斯</a></p></td>
<td style="text-align: center;"><p>2-0</p></td>
<td style="text-align: center;"><p>1-0</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>歐洲足協盃</strong></p></td>
<td style="text-align: center;"><p>第一圈</p></td>
<td style="text-align: center;"><p><a href="../Page/斯特拉斯堡足球俱樂部.md" title="wikilink">斯特拉斯堡</a></p></td>
<td style="text-align: center;"><p>0-2</p></td>
<td style="text-align: center;"><p>0-5</p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 打吡戰

以下是[格拉茨AK與同市](../Page/格拉茨AK.md "wikilink")[格拉茨](../Page/格拉茨.md "wikilink")（SK
Sturm Graz）的比賽統計。（截止2005年8月23日）

  - 勝出 | 46場
  - 賽和 | 42場
  - 落敗 | 42場
  - 進球 | 174球
  - 失球 | 168球

## 注釋

[Category:奧地利足球俱樂部](../Category/奧地利足球俱樂部.md "wikilink")

1.