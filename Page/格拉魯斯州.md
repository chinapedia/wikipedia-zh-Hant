**格拉魯斯州**（、、、）是[瑞士東部德語區的一個州](../Page/瑞士.md "wikilink")，首府設於[格拉魯斯](../Page/格拉魯斯.md "wikilink")。

## 地理

連特河（Linth）發源於格拉魯斯州，然後注入北部的[瓦倫湖](../Page/瓦倫湖.md "wikilink")，河流切開的深谷橫貫州境。州內地形山巒交疊，最高點是海拔3614米高的Tödi峰，其他高峰有Hausstock峰（3158m）及Glärnisch峰（2910m）。格拉魯斯州面積為685平方公里，佈滿森林，伐木業是州內重要的經濟支柱之一。

## 歷史

[Glarus_Stadtkirche.jpg](https://zh.wikipedia.org/wiki/File:Glarus_Stadtkirche.jpg "fig:Glarus_Stadtkirche.jpg")
[Glarus_Stadtkirche_Langhaus.jpg](https://zh.wikipedia.org/wiki/File:Glarus_Stadtkirche_Langhaus.jpg "fig:Glarus_Stadtkirche_Langhaus.jpg")
[宗教對格拉魯斯州的歷史影響深遠](../Page/宗教.md "wikilink")。[連特河谷的居民大約於公元](../Page/連特河.md "wikilink")[六世紀被](../Page/六世紀.md "wikilink")[愛爾蘭](../Page/愛爾蘭.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")[聖弗里多林](../Page/聖弗里多林.md "wikilink")（Saint
Fridolin）傳教下皈依[基督教](../Page/基督教.md "wikilink")。他於[巴澤爾附近創立了](../Page/巴澤爾.md "wikilink")[塞京根修道院](../Page/塞京根修道院.md "wikilink")（Säckingen
Abbey），從[九世紀開始](../Page/九世紀.md "wikilink")，這間修道院擁有[格拉魯斯附近的土地](../Page/格拉魯斯.md "wikilink")。於1288年，[神聖羅馬帝國的](../Page/神聖羅馬帝國.md "wikilink")[哈布斯堡王朝為了擴張勢力](../Page/哈布斯堡王朝.md "wikilink")，慢慢蠶食了修道院的地權。之後，格拉魯斯人民於1352年選擇加入[瑞士邦聯](../Page/瑞士.md "wikilink")。

在1506年至之後十年間，著名的宗教改革家[慈運理當上了格拉魯斯的牧師](../Page/慈運理.md "wikilink")，到了1564年，[慈運理的追隨者被全滅](../Page/慈運理.md "wikilink")，但這並沒有停止了[新教徒與](../Page/新教徒.md "wikilink")[天主教徒之間的爭端](../Page/天主教徒.md "wikilink")。為了維持和平，於是在1623年決定[天主教徒和](../Page/天主教徒.md "wikilink")[新教徒有各自的議會](../Page/新教徒.md "wikilink")（Landsgemeinde）。在後來的1683年，[新教徒與](../Page/新教.md "wikilink")[天主教徒開始各自有自己的法庭](../Page/天主教.md "wikilink")。

1798至1803年間，格拉魯斯地區成為[拿破崙建立的](../Page/拿破崙.md "wikilink")[赫爾維蒂共和國的連特州的一部份](../Page/赫爾維蒂共和國.md "wikilink")。1836年通過的[憲法把分裂的議會統一](../Page/憲法.md "wikilink")。

## 經濟

[Naefels_Freulerpalast.jpg](https://zh.wikipedia.org/wiki/File:Naefels_Freulerpalast.jpg "fig:Naefels_Freulerpalast.jpg")
格拉魯斯州多山的地形有助於[工業化](../Page/工業化.md "wikilink")，棉紗工業在十八世紀很重要，補助了傳統的羊毛紡紗。工業化還帶來了棉布印染、[水力發電站和後來的冶金](../Page/水力發電站.md "wikilink")、機械工廠及造紙廠。雖然如此，飼養牛隻及產奶行業在山區的草地上仍然重要。

## 州徽

底色為紅色，上面有[愛爾蘭](../Page/愛爾蘭.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")[聖弗里多林](../Page/聖弗里多林.md "wikilink")，可見他對格拉魯斯州的重要性。

## 行政區劃

不設分區，州轄下有25個市(Ortsgemeinden):

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/Betschwanden.md" title="wikilink">Betschwanden</a></li>
<li><a href="../Page/Bilten.md" title="wikilink">Bilten</a></li>
<li><a href="../Page/Braunwald.md" title="wikilink">Braunwald</a></li>
<li><a href="../Page/Elm,_Switzerland.md" title="wikilink">Elm</a></li>
<li><a href="../Page/Engi.md" title="wikilink">Engi</a></li>
<li><a href="../Page/Ennenda.md" title="wikilink">Ennenda</a></li>
<li><a href="../Page/Filzbach.md" title="wikilink">Filzbach</a></li>
<li><a href="../Page/Glarus.md" title="wikilink">Glarus</a></li>
<li><a href="../Page/Haslen.md" title="wikilink">Haslen</a>（由<a href="../Page/Leuggelbach.md" title="wikilink">Leuggelbach</a>、<a href="../Page/Nidfurn.md" title="wikilink">Nidfurn及</a> Haslen合併而成）</li>
<li><a href="../Page/Linthal.md" title="wikilink">Linthal</a></li>
<li><a href="../Page/Luchsingen.md" title="wikilink">Luchsingen</a>（由<a href="../Page/Diesbach.md" title="wikilink">Diesbach</a>、<a href="../Page/Hätzingen.md" title="wikilink">Hätzingen及</a> Luchsingen合併而成）</li>
<li><a href="../Page/Matt,_Switzerland.md" title="wikilink">Matt</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Mitlödi.md" title="wikilink">Mitlödi</a></li>
<li><a href="../Page/Mollis.md" title="wikilink">Mollis</a></li>
<li><a href="../Page/Mühlehorn.md" title="wikilink">Mühlehorn</a></li>
<li><a href="../Page/Näfels.md" title="wikilink">Näfels</a></li>
<li><a href="../Page/Netstal.md" title="wikilink">Netstal</a></li>
<li><a href="../Page/Niederurnen.md" title="wikilink">Niederurnen</a></li>
<li><a href="../Page/Oberurnen.md" title="wikilink">Oberurnen</a></li>
<li><a href="../Page/Obstalden.md" title="wikilink">Obstalden</a></li>
<li><a href="../Page/Riedern.md" title="wikilink">Riedern</a></li>
<li><a href="../Page/Rüti,_Glarus.md" title="wikilink">Rüti</a></li>
<li><a href="../Page/Schwanden,_Glarus.md" title="wikilink">Schwanden</a></li>
<li><a href="../Page/Schwändi.md" title="wikilink">Schwändi</a></li>
<li><a href="../Page/Sool,_Switzerland.md" title="wikilink">Sool</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [官方統計數據](http://www.statistik.admin.ch/stat_ch/ber00/ekan_gl.htm)
  - [州官方網站](http://www.gl.ch/)（德文）
  - [連特河谷地圖-Google
    Maps](http://maps.google.com/maps?ll=46.983032,9.009476&spn=0.312458,0.481407&t=k&hl=en)

[Category:瑞士行政區劃](../Category/瑞士行政區劃.md "wikilink")