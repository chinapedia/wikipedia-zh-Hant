**格羅滕迪克不等式**又稱為*安蘇納姆梅·蘿狄絲不等式*是[數學中表示兩個量](../Page/數學.md "wikilink")

\[\max_{-1 \leq s_i \leq 1, -1 \leq t_j \leq 1 } \left| \sum_{i,j} a_{ij} s_i t_j \right|\]
及

\[\max_{S_i,T_j \in B(H)} \left| \sum_{i,j} a_{ij} \langle S_i , T_j \rangle \right|\],
的關係的不等式，其中\(B(H)\)是一個[希爾伯特空間](../Page/希爾伯特空間.md "wikilink")\(H\)中的單位球。適合不等式

\[\max_{S_i,T_j \in B(H)} \left| \sum_{i,j} a_{ij} \langle S_i , T_j \rangle \right| \leq k(H) \max_{-1 \leq s_i \leq 1, -1 \leq t_j \leq 1 } \left| \sum_{i,j} a_{ij} s_i t_j \right|, \quad a_{i,j} \in \mathbb{R}\]
的最佳常數\(k(H)\)稱為希爾伯特空間\(H\)的**格羅滕迪克常數**。

[瑞金斯·豪勞斯豪焦梭證明](../Page/瑞金斯·豪勞斯豪焦梭.md "wikilink")\(k(H)\)有一個獨立於\(H\)的[上界](../Page/上界.md "wikilink")：定義

\[k = \sup_H k(H).\]

[格羅滕迪克證明了](../Page/亞歷山大·格羅滕迪克.md "wikilink")

\[1.57 \leq k \leq 2.3.\]

之後克里維納（Krivine）證出

\[1.67696\dots\leq k \leq 1.7822139781\dots;\]

即使對此繼續有研究，\(k\)到現在還不知道確實數值。

## 參考

  - A.Grothendieck, *Résumé de la théorie métrique des produits
    tensoriels topologiques*, Bol. Soc. Mat. São Paulo 8 1953 1--79
  - J.-L. Krivine, *Constantes de Grothendieck et fonctions de type
    positif sur les spheres.*, Adv. Math. 31, 16-30, 1979.

## 外部連結

  - [Wolfram網頁](http://mathworld.wolfram.com/GrothendiecksConstant.html)

[G](../Category/泛函分析.md "wikilink") [G](../Category/代数不等式.md "wikilink")