**格萨尔王传**（，），流行在[西藏和](../Page/西藏.md "wikilink")[中亚地区的著名](../Page/中亚.md "wikilink")[史诗](../Page/史诗.md "wikilink")，目前在[西藏](../Page/西藏.md "wikilink")、[蒙古和](../Page/蒙古.md "wikilink")[土族中间尚有](../Page/土族.md "wikilink")140位演唱艺人在说唱这部史诗。

格萨尔王传已经存在有一千多年，长达60万诗行，相当于印度史诗《[摩诃婆罗多](../Page/摩诃婆罗多.md "wikilink")》3部长，《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》15部，是世界最长的史诗。讲述传说中的[岭国国王](../Page/岭国.md "wikilink")[格萨尔的故事](../Page/格萨尔.md "wikilink")，对[藏传佛教影响很大](../Page/藏传佛教.md "wikilink")。

## 故事梗概

故事从创世时期开始，讲述三位伟大的[佛教国师将西藏从野蛮时期转化为帝国时期](../Page/佛教.md "wikilink")（大约公元前7世纪－9世纪），其中最著名的是[蓮華生大师](../Page/蓮華生.md "wikilink")，他用誓言约束当时西藏的混乱，但由于许多魔鬼并没有完全被降伏，其后又发生动乱，西藏被吃人的魔鬼、游牧部落所统治，分为许多小国，各国国王都是邪恶和贪婪的。

为了缓解这种形势，包括[佛](../Page/佛.md "wikilink")、[梵天](../Page/梵天.md "wikilink")、[普贤菩萨以及地下的神和](../Page/普贤菩萨.md "wikilink")[龙族都决定必须从天上送下一位英雄去制服这些鬼怪](../Page/龙.md "wikilink")，最终让仓巴噶波（白梵天）的最小的儿子闻喜下凡，但他非常勉强，最后还是同意了。

他下凡后降生为一个龙女的儿子，叫做觉如，成为岭国的王子。岭国在[西藏的东部](../Page/西藏.md "wikilink")，[长江和](../Page/长江.md "wikilink")[雅砻江之间](../Page/雅砻江.md "wikilink")。

觉如有一个异母弟弟，是一位伟大的勇士，在战斗中牺牲了，有的版本说他是[中國皇帝的孙子](../Page/中國皇帝.md "wikilink")。他有两个叔叔，一位非常聪明并支持他；另一位晁通非常贪婪，想夺取王位，但没有成功，以后私通敌人。
[Gesar_Gruschke.jpg](https://zh.wikipedia.org/wiki/File:Gesar_Gruschke.jpg "fig:Gesar_Gruschke.jpg")

一开始觉如因为行为不端，母子两人被放逐到[黄河上游地区](../Page/黄河.md "wikilink")，过着茹毛饮血的生活。

当觉如12岁时，回到岭国参加了一场决定继承王位的赛马，赢得王位，被尊称为世界雄狮大王格萨尔罗布扎堆。并和珠牡结婚。

格萨尔称王以后，首先杀死了北方吃人的黑魔鲁赞。他的妻子被霍尔国的白帐王抢走，格萨尔王杀死白帐王，夺回妻子。然后他又征服姜国（[云南](../Page/云南.md "wikilink")）和门国（[喜马拉雅山麓](../Page/喜马拉雅山.md "wikilink")）。

此外他又建立18个功劳（有的版本说是40个），征服了[塔吉克和](../Page/塔吉克.md "wikilink")[穆斯林等地](../Page/穆斯林.md "wikilink")。

格萨尔王到80岁时，到地狱救出自己的妻子和母亲，最终离开地上王国，和妻子一起回到了天上。

## 历史

格萨尔王传可能起源于[佛教第二次传入](../Page/佛教.md "wikilink")[西藏时期](../Page/西藏.md "wikilink")，（[噶當派形成期间](../Page/噶當派.md "wikilink")），可能是起源于[苯教的故事](../Page/苯教.md "wikilink")。最早的手抄本可能是11世纪期间佛教和尚写下的。1860年代由[藏族](../Page/藏族.md "wikilink")[哲学家米梵姜扬嘉措监制整理](../Page/哲学家.md "wikilink")，大圆满寺（卓千寺）的和尚刻版印刷成书。

格萨尔王的故事在[蒙古族中也流传很广](../Page/蒙古族.md "wikilink")，称为“格斯尔王”，并且随着[卡尔梅克人传播到](../Page/卡尔梅克.md "wikilink")[里海和](../Page/里海.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")。[不丹的第二位国王将聆听说唱格萨尔王传当作宫廷中主要的娱乐活动和最重要的教诲](../Page/不丹.md "wikilink")。

法国学者石泰安提出，格萨尔（Gesar）一名可能来源于[古罗马的皇帝头衔](../Page/古罗马.md "wikilink")[凯撒](../Page/凯撒.md "wikilink")（Ceasar）。\[1\]这个王称后来东传，不但罗马后继者拜占庭帝国使用，跟拜占庭打过交道的突厥人和蒙古人也都先后引入该词。[唃厮啰](../Page/唃厮啰.md "wikilink")（gyalsras）一词也与格萨尔或凯撒同源。

格萨尔故事的直接来源很可能是八世纪[巴克特里亚](../Page/巴克特里亚.md "wikilink")（Bactria）[健陀罗地区的突厥统治者](../Page/健陀罗.md "wikilink")，他们使用的头衔是From
Kesar，即罗马凯撒（拉丁语 Roma \>\> 伊朗语
frōm-hrōm）。这个故事本是罗马凯撒打败阿拉伯军队，在藏族占据拉达克地区后流入西藏，情节不断添加，佛教化，最终藏化为格萨尔王的故事。

根据故事的描述和当地人民的传说，格萨尔王的出生地很可能就是[中國](../Page/中國.md "wikilink")[四川](../Page/四川.md "wikilink")[甘孜](../Page/甘孜藏族自治州.md "wikilink")[德格县的](../Page/德格县.md "wikilink")[阿须乡](../Page/阿须乡.md "wikilink")。他灵魂安息的山就是[阿尼玛卿山的](../Page/阿尼玛卿山.md "wikilink")[果洛峰](../Page/果洛峰.md "wikilink")。甘孜一带的[林葱土司奉格萨尔王为始祖](../Page/林葱土司.md "wikilink")。

## 相关轶事

在2018年3月20日举行的[第十三届全国人民代表大会第一次会议闭幕会上](../Page/第十三届全国人民代表大会.md "wikilink")，中共中央总书记、国家主席[习近平在发言中将](../Page/习近平.md "wikilink")“格萨尔王”错念成了“萨格尔王”。稍后“萨格尔王”成为[敏感词并禁止搜索](../Page/敏感词.md "wikilink")。随后中国官方发布的影片将音轨剪切修改成正确的形式\[2\]。

## 参考文献

## 外部链接

  - [简要的全文（中文）](https://web.archive.org/web/20060515080805/http://zt.tibet.cn/zt/gesaer/gs_quanzhuan.htm)

## 参见

  - [王沂暖](../Page/王沂暖.md "wikilink")

{{-}}

[Category:西藏国家级非物质文化遗产](../Category/西藏国家级非物质文化遗产.md "wikilink")
[Category:青海国家级非物质文化遗产](../Category/青海国家级非物质文化遗产.md "wikilink")
[Category:甘肃国家级非物质文化遗产](../Category/甘肃国家级非物质文化遗产.md "wikilink")
[Category:四川国家级非物质文化遗产](../Category/四川国家级非物质文化遗产.md "wikilink")
[Category:云南国家级非物质文化遗产](../Category/云南国家级非物质文化遗产.md "wikilink")
[Category:内蒙古国家级非物质文化遗产](../Category/内蒙古国家级非物质文化遗产.md "wikilink")
[Category:新疆国家级非物质文化遗产](../Category/新疆国家级非物质文化遗产.md "wikilink")
[Category:史诗](../Category/史诗.md "wikilink")
[Category:藏族神话](../Category/藏族神话.md "wikilink")
[Category:苯教](../Category/苯教.md "wikilink")
[Category:中国的世界之最](../Category/中国的世界之最.md "wikilink")
[Category:中国人类非物质文化遗产代表作](../Category/中国人类非物质文化遗产代表作.md "wikilink")
[Category:佛教神話](../Category/佛教神話.md "wikilink")
[Category:藏族文学](../Category/藏族文学.md "wikilink")

1.
2.