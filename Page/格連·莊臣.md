**格伦·约翰逊**（，），[英格蘭](../Page/英格蘭.md "wikilink")[足球員](../Page/足球員.md "wikilink")，出生於[倫敦](../Page/倫敦.md "wikilink")[格林威治](../Page/格林威治.md "wikilink")，司職[右閘](../Page/後衛_\(足球\).md "wikilink")，曾效力[史篤城](../Page/史篤城足球會.md "wikilink")。

## 生平

格伦·约翰逊出身於英國老牌球會[韋斯咸](../Page/韋斯咸.md "wikilink")，曾被外借至[英格蘭冠軍聯賽的](../Page/英格蘭冠軍聯賽.md "wikilink")[米禾爾](../Page/米尔沃尔足球俱乐部.md "wikilink")。他曾入選過英格蘭青年國家隊出賽。當2003年韋斯咸從英超降級後，格伦·约翰逊便於2003年以600萬[英鎊加盟英超球會](../Page/英鎊.md "wikilink")[車路士](../Page/車路士.md "wikilink")，成為車路士成員之一，當時他僅僅是上陣過[英超十場有多](../Page/英超.md "wikilink")，外界普遍認為不值600萬英鎊身價。

加盟車路士第一季，格伦·约翰逊以凌厲的速度成為正選球員，並入選了[英格蘭國家足球隊](../Page/英格蘭國家足球隊.md "wikilink")。但由於作為後衛，始終經驗不足，並常有失誤，近幾季多次被外借，至2007年終被出售到英格蘭的樸茨茅夫\[1\]。

在2009年6月26日，格連·莊臣以1,750萬[英鎊加盟](../Page/英鎊.md "wikilink")[利物浦](../Page/利物浦足球俱乐部.md "wikilink")，簽下為期4年的合約\[2\]。

## 史篤城

2015年7月12日簽約[史篤城](../Page/史篤城足球會.md "wikilink")2年。\[3\]

## 國家隊

在[2012年歐洲國家盃分組賽對陣法國](../Page/2012年歐洲國家盃.md "wikilink")，高唱國歌後便開心地跑去自己位置，完全忘記合照這回事，使英格蘭隊賽前合照只有10個人\[4\]。

## 榮譽

  - 樸茨茅夫

<!-- end list -->

  - **[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")**冠軍：2008年；

<!-- end list -->

  - 利物浦

<!-- end list -->

  - [英格蘭聯賽盃冠軍](../Page/英格蘭聯賽盃.md "wikilink")：2011/12年

## 參考資料

## 外部連結

  -
  - [英格蘭足總：格連·莊臣](http://www.thefa.com/England/All-Teams/Players/J/Glen-Johnson)

  - [FootballDatabase
    profile](http://www.footballdatabase.com/site/players/index.php?dumpPlayer=1261)

  - [BBC
    profile](http://news.bbc.co.uk/sport1/shared/bsp/hi/football/statistics/players/j/johnson_249096.stm)

[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:韋斯咸球員](../Category/韋斯咸球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:米禾爾球員](../Category/米禾爾球員.md "wikilink")
[Category:樸茨茅夫球員](../Category/樸茨茅夫球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:史篤城球員](../Category/史篤城球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英格蘭足球代表隊球員](../Category/英格蘭足球代表隊球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.   [Portsmouth sign full-back
    Johnson](http://news.bbc.co.uk/sport2/hi/football/teams/p/portsmouth/6972186.stm)，2007年8月31日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2008年12月22日查閱
2.
3.
4.  [Spot the missing England player\!\!\! Only 10 lined up for official
    photo pre France Euro 2012
    match](http://www.101greatgoals.com/blog/spot-missing-england-player-10-lined-official-photo-pre-france-euro-2012-match/)