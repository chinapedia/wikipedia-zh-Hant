**格里納普县**（**Greenup County,
Kentucky**）是位於[美國](../Page/美國.md "wikilink")[肯塔基州東北部的一個縣](../Page/肯塔基州.md "wikilink")，北、東隔[俄亥俄河和](../Page/俄亥俄河.md "wikilink")[俄亥俄州相望](../Page/俄亥俄州.md "wikilink")。面積918平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口36,891人。縣治[格里納普](../Page/格里納普_\(肯塔基州\).md "wikilink")（Greenup）。

成立於1803年12月12日。縣名紀念[聯邦眾議員](../Page/美國眾議院.md "wikilink")、第三任[肯塔基州州長](../Page/肯塔基州州長.md "wikilink")[克里斯多福·格里納普](../Page/克里斯多福·格里納普.md "wikilink")（Christopher
Greenup）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/肯塔基州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.