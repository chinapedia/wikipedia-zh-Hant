**格雷里格列車出軌事故**發生於2007年2月23日，地點為[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[坎布里亞郡](../Page/坎布里亞郡.md "wikilink")（Grayrigg）村。

## 事發經過

2007年2月23日英國時間約下午20時15分，一列由[倫敦](../Page/倫敦.md "wikilink")[尤斯頓車站前往](../Page/尤斯頓車站.md "wikilink")[格拉斯哥的](../Page/格拉斯哥.md "wikilink")[英鐵390型列車](../Page/英鐵390型電力動車組.md "wikilink")（編號：033，2002年出廠），載著111名乘客和4名職員，於上以的速度行駛，在駛至坎布里亞郡格雷里格村時，列車突然[出軌](../Page/出軌.md "wikilink")。由於衝力巨大，9節車廂全數出軌，並衝落堤壩下，釀成1死77傷。有乘客表示，列車駛經肇事地點前，已感到列車不尋常地顛簸及擺動，且擺動越來越厲害，不久後列車便出軌。

## 起因

經（Rail Accident Investigation
Branch）調查後，證實肇事路段其中一個軌枕上的3個扣件出現鬆脫或遺失，因而令列車車輪無法緊貼路軌邊行走，繼而引發出軌事故。

## 影響

肇事列車的首節車廂於2007年3月1日傍晚開始移走，3天後（即3月4日）最後一節車廂被移走，A685號公路亦重新開放[1](http://www.thisisthelakedistrict.co.uk/display.var.1235443.0.last_rails_cars_and_crane_shipped_out.php)。肇事路段的列車服務於3月12日恢復正常，但途經事發地點增設了80km/h（50mph）車速限制，以策安全[2](https://archive.is/20070704015553/http://news.sky.com/skynews/article/0,,30000-1255245,00.html)。。

## 外部連結

[英安全火車出軌 1死77傷警方指是奇迹](http://hk.apple.nextmedia.com/international/art/20070225/6847325)（2007年2月25日香港[蘋果日報的報導](../Page/蘋果日報_\(香港\).md "wikilink")）

[Category:英國鐵路事故](../Category/英國鐵路事故.md "wikilink")
[Category:2007年鐵路事故](../Category/2007年鐵路事故.md "wikilink")
[Category:2007年英国](../Category/2007年英国.md "wikilink")