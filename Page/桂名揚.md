**桂名揚**()，生於[廣東省](../Page/廣東省.md "wikilink")[南海縣](../Page/南海縣.md "wikilink")，祖籍[浙江](../Page/浙江.md "wikilink")[寧波](../Page/寧波.md "wikilink")，原名**桂銘揚**，著名[粵劇老倌](../Page/粵劇.md "wikilink")。父親[桂東原](../Page/桂東原.md "wikilink")，叔父[桂南屏](../Page/桂南屏.md "wikilink")，都是[清末的所謂](../Page/清.md "wikilink")「經學家」。

桂名揚在11歲開始學戲，他的師傅姓潘，是「優天影」班的管事，雖不出名，但舞臺藝術知識很豐富，所以桂名揚的基本功學得極好。他擅長飾演小武戲，[馬師曾在組成](../Page/馬師曾.md "wikilink")“大羅天”班後，看到桂名揚造詣不差，便以他充三幫小武。他能把[馬師曾](../Page/馬師曾.md "wikilink")、[薛覺先的表演藝術融會貫通並自成一家](../Page/薛覺先.md "wikilink")，人稱“馬形薛腔”。他所演的[趙子龍](../Page/趙子龍.md "wikilink")，其威勢及風度大大超過馬師曾。他的表演頓挫鮮明，氣勢威猛，節奏緊湊，創造了一種名為“鑼邊滾花”，用高亢急驟的鑼鼓音樂配合上場身段的程式，讓人耳目一新，現已成為粵劇常用的出場程式。

1931年桂名揚率“大中華男女劇團”在美公演，大受歡迎，紐約安良堂贈他一面十四両重的金牌，上鎸“四海名揚”四字，開粵劇界男伶在美國獲贈金牌先河。
”1932年桂名揚從[美洲回來](../Page/美洲.md "wikilink")，便和[廖俠懷](../Page/廖俠懷.md "wikilink")、[曾三多](../Page/曾三多.md "wikilink")、[陳錦棠組成](../Page/陳錦棠.md "wikilink")「日月星」班，一登臺便得到廣大觀眾讚賞。演出《[火燒阿房宮](../Page/火燒阿房宮.md "wikilink")》等劇，更獲得很高的聲譽。一些文武生如[任劍輝](../Page/任劍輝.md "wikilink")、[麥炳榮](../Page/麥炳榮.md "wikilink")、[呂玉郎](../Page/呂玉郎.md "wikilink")、[羅家寶](../Page/羅家寶.md "wikilink")，桂名揚的[徒弟有](../Page/徒弟.md "wikilink")[盧海天](../Page/盧海天.md "wikilink")、[梁蔭堂](../Page/梁蔭堂.md "wikilink")、[祁筱英等](../Page/祁筱英.md "wikilink")，都對桂名揚的表演藝術均有借鑒或學藝。桂名揚對[徒弟要求十分嚴格](../Page/徒弟.md "wikilink")，梁蔭棠也勤學苦練。

桂名揚於1957年10月26日(星期六)早上，與么子桂仲川、徒弟鍾惠芳(12歲)，從[香港移居](../Page/香港.md "wikilink")[廣州市](../Page/廣州市.md "wikilink")\[1\]。當晚已立刻去看[馬師曾演出的](../Page/馬師曾.md "wikilink")《鬥氣姑爺》。[廣東粵劇團在](../Page/廣東粵劇團.md "wikilink")1957年10月28日(星期一)開會歡迎剛從香港移居廣州的「金牌武生」桂名揚，由[馬師曾](../Page/馬師曾.md "wikilink")、[紅線女等都在會上致詞表示歡迎](../Page/紅線女.md "wikilink")。桂名揚移居廣州後即在廣東粵劇團任職藝術指導，指導該劇團每齣劇的排演，並將整理他過去30多年來演出的首本戲，例如：《冷-{面}-皇夫》、《狄青三取珍珠旗》、《情放莽將軍》等，準備將來給該團演出。桂名揚住在他的養女粵劇花旦[劉美卿](../Page/劉美卿.md "wikilink")(原名：桂美寶)在西關的家。廣東粵劇團以桂名揚旅途勞頓，特地給假讓他[休息一個時期](../Page/休息.md "wikilink")。在休假期間，桂名揚白天多是在家裡休息，中午到廣州酒家暍茶，晚上去看粵劇\[2\]。

桂名揚長期患[肺結核](../Page/肺結核.md "wikilink")、[心臟病](../Page/心臟病.md "wikilink")、[胃病等](../Page/胃病.md "wikilink")，1957年10月26日移居廣州後，廣東粵劇團為照顧他的健康，送他入醫院療養，經數月醫治，於1958年6月15日晚上9時45分因患[肺結核在](../Page/肺結核.md "wikilink")[廣東省幹部療養院病逝](../Page/廣東省幹部療養院.md "wikilink")，享年49歲。桂名揚病逝後，廣州市文化局、[中國戲劇家協會廣州分會](../Page/中國戲劇家協會.md "wikilink")、廣東粵劇團以及廣州市粵劇人士及桂名揚生前友好已組成治喪委員會，為他料理後事，定1958年6月17日(星期二)上午11時出殯。治喪委員會委員名單：[丁波](../Page/丁波.md "wikilink")、[衛少芳](../Page/衛少芳.md "wikilink")、[文覺非](../Page/文覺非.md "wikilink")、[白駒榮](../Page/白駒榮.md "wikilink")、[白超鴻](../Page/白超鴻.md "wikilink")、[關子光](../Page/關子光.md "wikilink")、[李門](../Page/李門.md "wikilink")、[李翠芳](../Page/李翠芳.md "wikilink")、[呂王郎](../Page/呂王郎.md "wikilink")、[陳小茶](../Page/陳小茶.md "wikilink")、[林榆](../Page/林榆.md "wikilink")、[林韻](../Page/林韻.md "wikilink")、[羅品超](../Page/羅品超.md "wikilink")、[鄭達](../Page/鄭達.md "wikilink")、[郎筠玉](../Page/郎筠玉.md "wikilink")、[馬師曾](../Page/馬師曾.md "wikilink")、[紅線女](../Page/紅線女.md "wikilink")、[陸雲飛](../Page/陸雲飛.md "wikilink")、[梁國風](../Page/梁國風.md "wikilink")、[梅重清](../Page/梅重清.md "wikilink")、[黃不滅](../Page/黃不滅.md "wikilink")、[黃寧嬰](../Page/黃寧嬰.md "wikilink")、[崔子超](../Page/崔子超.md "wikilink")、[曾三多](../Page/曾三多.md "wikilink")、[新珠](../Page/新珠.md "wikilink")、[靚少英](../Page/靚少英.md "wikilink")、[靚少佳](../Page/靚少佳.md "wikilink")、[譚玉真](../Page/譚玉真.md "wikilink")\[3\]。

## 參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [香港電影資料館有關桂名楊參演電影的記錄](http://ipac.hkfa.lcsd.gov.hk/ipac20/ipac.jsp?session=B24O353500T73.50&profile=hkfa&uri=link=3100036@!5646@!3100024@!3100036&menu=search&submenu=basic_search&source=192.168.110.61@!horizon)

  - [江楓粵樂苑－粵樂知識](https://web.archive.org/web/20080115173549/http://hk.geocities.com/gongfung/yyzs.htm)

  - [逝影留聲（五）：桂名楊](https://web.archive.org/web/20050228181424/http://chinafoshan.net/history/3c/200207260049.html)

  - [粤剧的五大流派--桂名扬](https://web.archive.org/web/20051205002130/http://culture.china.com/zh_cn/art/feel/drama/391/20010404/10003915.html)

  -
  -
[G](../Category/宁波人.md "wikilink") [G](../Category/香港宁波人.md "wikilink")
[Category:廣東人](../Category/廣東人.md "wikilink")
[桂](../Category/粵劇演員.md "wikilink")
[桂](../Category/香港粵劇演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港粵語片演員](../Category/香港粵語片演員.md "wikilink")
[M](../Category/慈城桂氏.md "wikilink")
[M](../Category/桂姓.md "wikilink")

1.  1957年10月26日(星期六)，香港《大公報》，第二張；第五版。
2.  1957年10月31日(星期四)，香港《大公報》，第一張；第三版。
3.  1958年6月17日(星期二)，香港《大公報》，第一張；第四版。