**桂太郎**（），[長州藩出身](../Page/長州藩.md "wikilink")（今[山口縣](../Page/山口縣.md "wikilink")），曾任[台灣日治時期第](../Page/台灣日治時期.md "wikilink")2任[總督](../Page/台灣總督.md "wikilink")，後來三度出任[日本内阁总理大臣](../Page/日本内阁总理大臣.md "wikilink")（1901年－1906年；1908年－1911年；1912年－1913年），是[日本有史以来任职时间最长的首相](../Page/日本內閣總理大臣在任時間排行.md "wikilink")，[元老之一](../Page/元老_\(日本\).md "wikilink")。

任內締結[英日同盟](../Page/英日同盟.md "wikilink")，進行[日俄戰爭](../Page/日俄戰爭.md "wikilink")，並策劃吞併[朝鮮半島](../Page/朝鮮半島.md "wikilink")，推動[日韓合併](../Page/日韓合併.md "wikilink")。

[台灣協會學校](../Page/台灣協會學校.md "wikilink")（今[拓殖大學](../Page/拓殖大學.md "wikilink")）的創立者及初代校長。

## 生平

  - 1870年，留学[德国](../Page/德国.md "wikilink")。
  - 1873年，回国。
  - 1884年，随陆军卿[大山岩赴](../Page/大山岩.md "wikilink")[欧洲考察军制](../Page/欧洲.md "wikilink")。
  - 1886年，任陆军次官，辅佐[山县有朋实行军事改革](../Page/山县有朋.md "wikilink")。
  - 1890年，兼任军务局长。
  - 1891年，任第三师团长。
  - 1896年，被任命為[台灣總督](../Page/台灣總督.md "wikilink")，任職期間發生[雲林大屠殺事件](../Page/雲林大屠殺.md "wikilink")。
  - 1898年－1901年，连任四届[内阁陆军大臣](../Page/日本陸軍大臣.md "wikilink")，成为仅次于[伊藤博文](../Page/伊藤博文.md "wikilink")、[山县有朋的](../Page/山县有朋.md "wikilink")[长州藩阀的核心人物](../Page/长州藩.md "wikilink")。
  - 1901年－1913年，与[西园寺公望交替组阁](../Page/西园寺公望.md "wikilink")，俗称[桂园时代](../Page/桂園時代.md "wikilink")。
  - 1913年2月1日，任[立宪同志会创立委员长](../Page/立宪同志会.md "wikilink")。

## 系譜

### 系圖

## 相關條目

  - [台灣日治時期](../Page/台灣日治時期.md "wikilink")
  - [台灣總督](../Page/台灣總督.md "wikilink")
  - [拓殖大學](../Page/拓殖大學.md "wikilink")

## 外部連結

  - [歴代総理の写真と経歴（首相官邸ホームページ）](http://www.kantei.go.jp/jp/rekidai/souri/11.html)
  - [桂氏系譜](http://www2.harimaya.com/sengoku/html/m_katura.html)
  - [桂太郎肖像](http://www.ndl.go.jp/portrait/datas/52.html?c=0)
  - [明治宰相列伝 ： 桂太郎 |
    国立公文書館](http://www.archives.go.jp/exhibition/digital/2007_01/taro_katsura/)
  - [拓殖大学](http://www.takushoku-u.ac.jp/g_info/founder.html)
  - [国立国会図書館 憲政資料室
    桂太郎関係文書（所蔵）](https://web.archive.org/web/20080612212206/http://www.ndl.go.jp/jp/data/kensei_shiryo/kensei/katsuratarou1.html)
  - [国立国会図書館 憲政資料室
    桂太郎関係文書（MF：当室蔵・早稲田大学図書館蔵・宮内庁書陵部蔵ほか）](https://web.archive.org/web/20080612212211/http://www.ndl.go.jp/jp/data/kensei_shiryo/kensei/katsuratarou2.html)
  - [桂太郎（明治の首相）が、軍人として名古屋に来たとき知り合った女性（のちに夫人となる）について知りたい。](http://crd.ndl.go.jp/GENERAL/servlet/detail.reference?id=1000043121)
    - リファレンス共同データベース

[Category:日本內閣總理大臣](../Category/日本內閣總理大臣.md "wikilink")
[Category:日本外務大臣](../Category/日本外務大臣.md "wikilink")
[Category:貴族院公爵議員](../Category/貴族院公爵議員.md "wikilink")
[Category:貴族院侯爵議員](../Category/貴族院侯爵議員.md "wikilink")
[Category:臺灣總督](../Category/臺灣總督.md "wikilink")
[Category:長門國出身人物](../Category/長門國出身人物.md "wikilink")
[Category:長州藩](../Category/長州藩.md "wikilink")
[Category:日本外务大臣](../Category/日本外务大臣.md "wikilink")
[Category:戊辰戰爭人物](../Category/戊辰戰爭人物.md "wikilink")
[Category:甲午戰爭人物](../Category/甲午戰爭人物.md "wikilink")
[Category:日俄戰爭人物](../Category/日俄戰爭人物.md "wikilink")
[Category:日本子爵](../Category/日本子爵.md "wikilink")
[Category:日本伯爵](../Category/日本伯爵.md "wikilink")
[Category:桂氏](../Category/桂氏.md "wikilink")
[Category:勳一等瑞寶章獲得者](../Category/勳一等瑞寶章獲得者.md "wikilink")
[Category:巴斯勳章](../Category/巴斯勳章.md "wikilink")
[Category:日本陸軍大將](../Category/日本陸軍大將.md "wikilink")
[Category:日本陆军大将](../Category/日本陆军大将.md "wikilink")
[Category:軍人出身的政府首腦](../Category/軍人出身的政府首腦.md "wikilink")