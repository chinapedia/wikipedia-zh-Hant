**桂宮**，為日本皇室過去曾存在的[宮家](../Page/宮家.md "wikilink")，同時也是四世襲親王家之一，於[安土桃山時代的](../Page/安土桃山時代.md "wikilink")[後陽成天皇的](../Page/後陽成天皇.md "wikilink")[天正十七年](../Page/天正.md "wikilink")（1589年）創設，往後延續到[明治](../Page/明治.md "wikilink")14年（1881年），一共延續了**二百九十二年**之久。而在這近乎三個世紀之長的時期，桂宮家面臨數次的絕嗣，並都由當時的天皇指定養子以入繼桂宮家，現已廢除，而家業皆為花道。

## 沿革

桂宮家是正親町天皇的第一皇子[陽光院誠仁親王的第六皇子](../Page/誠仁親王.md "wikilink")—[智仁親王所創設](../Page/智仁親王.md "wikilink")，智仁親王原為[豐臣秀吉之猶子](../Page/豐臣秀吉.md "wikilink")，之後[豐臣秀吉的](../Page/豐臣秀吉.md "wikilink")[側室](../Page/側室.md "wikilink")—[淀殿在](../Page/淀殿.md "wikilink")1589年生下了[豐臣鶴松](../Page/豐臣鶴松.md "wikilink")，因而解除了和[智仁親王之間的養父子關係](../Page/智仁親王.md "wikilink")，並由[豐臣秀吉奏請朝廷](../Page/豐臣秀吉.md "wikilink")，在同年12月由朝廷賜予[智仁親王領地及](../Page/智仁親王.md "wikilink")「**八條宮**」的宮號，這是桂宮家的開端。[智仁親王的別莊便是今日有名的](../Page/智仁親王.md "wikilink")[桂離宮](../Page/桂離宮.md "wikilink")。

八条宮家到了第五代時，當時的當主[尚仁親王過世後](../Page/尚仁親王.md "wikilink")，沒有遺下任何子嗣，第六代當主便由[靈元天皇的第](../Page/靈元天皇.md "wikilink")10皇子[作宮繼承](../Page/作宮.md "wikilink")，並由[靈元天皇賜下新宮號](../Page/靈元天皇.md "wikilink")—**常磐井宮**，但[作宮繼承當主後沒多久](../Page/作宮.md "wikilink")，便以四歲之齡薨亡，第七代當主便由[作宮的兄長](../Page/作宮.md "wikilink")—[文仁親王繼承](../Page/文仁親王.md "wikilink")，又改宮號為**京極宮**。

第九代的當主[公仁親王過世後](../Page/公仁親王.md "wikilink")，京極宮家又再度絕嗣，當主一位便由[光格天皇的第](../Page/光格天皇.md "wikilink")4皇子—未滿一歲的[盛仁親王繼承](../Page/盛仁親王.md "wikilink")，並再改宮號為**桂宮**。

在[文化](../Page/文化.md "wikilink")7年（1810年）[盛仁親王繼承桂宮家沒多久](../Page/盛仁親王.md "wikilink")，次年5月17日便以2歲之齡薨亡，桂宮家再度絕嗣。

絕嗣后，桂宮家第十一代當主的位置由[盛仁親王的兄長](../Page/盛仁親王.md "wikilink")——[仁孝天皇第](../Page/仁孝天皇.md "wikilink")6皇子[節仁親王繼承](../Page/節仁親王.md "wikilink")，但翌年[節仁親王即薨亡](../Page/節仁親王.md "wikilink")，得年只有4歲。桂宮家在[節仁親王逝世後](../Page/節仁親王.md "wikilink")，當主之位空置，依[仁孝天皇之命](../Page/仁孝天皇.md "wikilink")，由[節仁親王的異母姐](../Page/節仁親王.md "wikilink")—[淑子內親王繼承第十二代桂宮家當主](../Page/淑子內親王.md "wikilink")。

淑子內親王以女性之身繼承宮家，是非常稀罕且破例的事情，雖然在[明治維新以前](../Page/明治維新.md "wikilink")，女性皇族可以繼承皇位，但女性繼承宮家可是絕無僅有的第一例。總之，淑子內親王在[文久](../Page/文久.md "wikilink")2年（1862年）繼承第十二代桂宮後，一直活到了[明治](../Page/明治.md "wikilink")14年（1881年），以五十三歲之齡過世。

[昭和](../Page/昭和.md "wikilink")63年（1988年），[三笠宮](../Page/三笠宮.md "wikilink")[崇仁親王的第二皇子](../Page/崇仁親王.md "wikilink")—[宜仁親王創設同名的宮家](../Page/宜仁親王.md "wikilink")「桂宮」，但和上述的宮家並沒有直接關係。

## 系譜

### 八条宮

  - 【1】
    [智仁親王](../Page/智仁親王.md "wikilink")（[誠仁親王第](../Page/誠仁親王.md "wikilink")6皇子、[後陽成天皇皇弟](../Page/後陽成天皇.md "wikilink")）
  - 【2】
    [智忠親王](../Page/智忠親王.md "wikilink")（[智仁親王第](../Page/智仁親王.md "wikilink")1王子）
  - 【3】
    [穩仁親王](../Page/穩仁親王.md "wikilink")（[後水尾天皇第](../Page/後水尾天皇.md "wikilink")13皇子）
  - 【4】
    [長仁親王](../Page/八条宮長仁親王.md "wikilink")（[後西天皇第](../Page/後西天皇.md "wikilink")1皇子）
  - 【5】
    [尚仁親王](../Page/八条宮尚仁親王.md "wikilink")（後西天皇第8皇子、[長仁親王之弟](../Page/八条宮長仁親王.md "wikilink")）

### 常磐井宮

  - 【6】 [作宮](../Page/作宮.md "wikilink")

### 京極宮

  - 【7】
    [文仁親王](../Page/京極宮文仁親王.md "wikilink")（靈元天皇第6皇子、[作宮皇兄](../Page/作宮.md "wikilink")）
  - 【8】
    [家仁親王](../Page/京極宮家仁親王.md "wikilink")（[文仁親王第](../Page/京極宮文仁親王.md "wikilink")1王子）
  - 【9】
    [公仁親王](../Page/京極宮公仁親王.md "wikilink")（[家仁親王第](../Page/京極宮家仁親王.md "wikilink")1王子）

### 桂宮

  - 【10】
    [盛仁親王](../Page/桂宮盛仁親王.md "wikilink")（[光格天皇第](../Page/光格天皇.md "wikilink")4皇子）
  - 【11】
    [節仁親王](../Page/桂宮節仁親王.md "wikilink")（[仁孝天皇第](../Page/仁孝天皇.md "wikilink")6皇子）
  - 【12】
    [淑子内親王](../Page/桂宮淑子内親王.md "wikilink")（仁孝天皇第3皇女、[節仁親王皇姐](../Page/桂宮節仁親王.md "wikilink")）

## 桂宮家系譜

<font size="-50%" color="red">`106`</font>` 　　　　　　　　　`<font size="-50%" color="red">`107`</font>` 　　　　`<font size="-50%" color="red">`108`</font>` 　　　　`<font size="-50%" color="red">`111`</font>`　　　`<font color="darkgreen"><font size="-50%">`八條宮`</font>`四`</font>
`正親町天皇─誠仁親王┬後陽成天皇─後水尾天皇┬後西天皇┬長仁親王`
`　　　　　　　　　　│`<font color="darkgreen"><font size="-50%">`八條宮`</font>`一`</font>` 　`<font color="darkgreen"><font size="-50%">`八條宮`</font>`二`</font>`　　　│`<font color="darkgreen"><font size="-50%">`八條宮`</font>`三`</font><font size="-60%">`　`</font>`│`<font color="darkgreen"><font size="-50%">`八條宮`</font>`五`</font>
`　　　　　　　　　　└智仁親王─智忠親王　　├穩仁親王└尚仁親王`
`┌─────────────────────┘`
`│`<font size="-50%" color="red">`百十二`</font>`　　　`<font size="-50%" color="red">`113`</font>`　　 `<font size="-50%">`閑院宮一`</font>`　　`<font size="-50%">`閑院宮二`</font>`　　`<font size="-50%" color="red">`119`</font>` 　　 `<font size="-50%" color="red">`120`</font>`　　 `<font color="darkgreen"><font size="-50%">`桂宮三`</font>`十二`</font>
`└霊元天皇┬東山天皇─直仁親王─典仁親王─光格天皇┬仁孝天皇┬淑子内親王`
`　　　　　│`<font color="darkgreen"><font size="-50%">`京極宮一`</font>`七　`</font><font color="darkgreen"><font size="-50%">`京極宮二`</font>`八　`</font><font color="darkgreen"><font size="-50%">`京極宮三`</font>`九 `</font>`　　　　 │`<font color="darkgreen"><font size="-50%">`桂宮一`</font>`十`</font><font size="-60%">`　`</font>`│`<font color="darkgreen"><font size="-50%">`桂宮二`</font>`十一`</font>
`　　　　　├文仁親王─家仁親王─公仁親王　　　　　└盛仁親王└節仁親王`
`　　　　　│`<font color="darkgreen"><font size="-50%">`常磐井宮`</font>`六`</font>`　　 　　　　┃`
`　　　　　└作宮　　　　　　　　壽子(紀伊藩`[`德川宗直之女`](../Page/德川宗直.md "wikilink")`)`

[桂宮](../Category/桂宮.md "wikilink")