**桂崇基**（），[江西](../Page/江西.md "wikilink")[贵溪人](../Page/贵溪.md "wikilink")。早年留学[美国](../Page/美国.md "wikilink")。历任[中國國民黨中央执行委员会秘书长](../Page/中國國民黨.md "wikilink")、国民党中央宣传部部长、[國立中央大學校长](../Page/國立中央大學.md "wikilink")、[國民政府的](../Page/國民政府.md "wikilink")[考試院副院长](../Page/考試院.md "wikilink")。1947年於原籍當選為[第一屆國民大會代表](../Page/第一屆國民大會代表.md "wikilink")。1949年隨[中華民國政府抵](../Page/中華民國政府.md "wikilink")[臺](../Page/臺灣.md "wikilink")，而後曾任[東吳大學校长](../Page/東吳大學_\(臺灣\).md "wikilink")。

## 紀念

桂崇基曾擔任[東吳大學校長](../Page/東吳大學_\(臺灣\).md "wikilink")，故該校[法學院大樓以其名命名以為紀念](../Page/東吳大學法學院.md "wikilink")。

## 參見

  - [東吳大學 (臺灣)](../Page/東吳大學_\(臺灣\).md "wikilink")

[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[G桂](../Category/考試院副院長.md "wikilink")
[G桂](../Category/東吳大學校長.md "wikilink")
[Category:桂姓](../Category/桂姓.md "wikilink")
[Category:中國國民黨秘書長](../Category/中國國民黨秘書長.md "wikilink")
[Category:江西裔台灣人](../Category/江西裔台灣人.md "wikilink")
[Category:贵溪人](../Category/贵溪人.md "wikilink")
[Category:國立中央大學校長](../Category/國立中央大學校長.md "wikilink")
[Category:中国国民党第三届中央执行委员会候补委员](../Category/中国国民党第三届中央执行委员会候补委员.md "wikilink")
[Category:中国国民党第四届中央执行委员会委员](../Category/中国国民党第四届中央执行委员会委员.md "wikilink")