**桂柳戰役**，中国称为**桂柳反攻作戰**。\[1\]，於1945年4月至8月，中國第2、第3方面軍在[廣西省](../Page/廣西省_\(中華民國\).md "wikilink")[龍州](../Page/龙州县.md "wikilink")、[南丹](../Page/南丹县.md "wikilink")、[全州](../Page/全州县.md "wikilink")、[陽朔地區對日軍](../Page/阳朔县.md "wikilink")[第6方面軍展開反攻作戰](../Page/第6方面軍_\(日本陸軍\).md "wikilink")。當時盟軍已攻佔[硫磺島及](../Page/硫磺島.md "wikilink")[琉球群島](../Page/琉球群島.md "wikilink")，日本本土日形危急。4月，日軍根據其[第6方面軍縮短防線的計劃](../Page/第6方面軍_\(日本陸軍\).md "wikilink")，將[第22](../Page/第22師團.md "wikilink")、[第58師團及](../Page/第58師團.md "wikilink")[第13師團一部撤退](../Page/第13師團.md "wikilink")[西江兩岸及](../Page/西江.md "wikilink")[南寧](../Page/南寧.md "wikilink")，以集中兵力，防止中國守軍反攻。

[蔣介石乃下令東南戰場在陸軍總司令](../Page/蔣介石.md "wikilink")[何應欽指揮下](../Page/何應欽.md "wikilink")，發動桂柳反攻作戰。[張發奎的第二方面軍以第](../Page/張發奎.md "wikilink")46軍一部攻佔[都安後](../Page/都安.md "wikilink")，出[都陽山脈奪取](../Page/都陽山脈.md "wikilink")[邕寧](../Page/邕寧.md "wikilink")；[湯恩伯的第三方面軍一部直取](../Page/湯恩伯.md "wikilink")[柳州](../Page/柳州.md "wikilink")，越[城嶺山脈攻略](../Page/城嶺山脈.md "wikilink")[桂林](../Page/桂林.md "wikilink")。國軍於5月27日克邕寧，日軍向龍州、[柳州撤退](../Page/柳州.md "wikilink")。國軍於6月30日收復柳州。尔后，第3方面军第20、第29军兵分3路沿桂柳公路和湘桂铁路(衡阳至南宁)向桂林并进，至7月17日，克复雒容、中渡和黄冕，日军退守永福，凭险顽抗；24日，攻克桂林南方门户永福；此时，一部沿桂柳公路克荔浦、白沙、阳朔，直逼桂林近郊；另一部攻克百寿，遂三面会攻桂林。第94军向义宁，第26军向全县、兴安间攻击前进，7月10日袭取南圩，26日克义宁，向桂林近郊推进。在各路部队总攻下，28日收复桂林，续向东追击。\[2\]。8月9日國軍往桂林東北進軍收復[全縣](../Page/全州县.md "wikilink")，不過在8月12日遭埋伏在全縣周遭的日軍逆襲敗退後撤，全縣在8月13日遭日軍奪回。
8月14日，国军第5师再克白沙；第133师将侵入咸水的日军击退。此时，第20军再次发动攻击，日军逐次向东北方向撤退。16日，第133师主力攻击马鞍岭、美女梳妆高地的日军。同日上午，东线国军攻入梧州，生擒城内日军100多人，广西东部全境光复。
17日拂晓，国军沿公路向全县攻击，上午10时许攻入全县城，进至全县南郊的预备第11师亦随后攻入，日军向黄沙河及东安撤退。
8月18日，第134师克复黄沙河，至此广西全境光复。国军挺进湖南境内。21日，第26军第42师收复东安；第134师追击队收复零陵，残余日军向衡阳撤退。

桂柳战役后期，[美國于](../Page/美國.md "wikilink")8月9日對日本長崎投下[原子彈](../Page/原子彈.md "wikilink")，8月15日日本天皇宣布投降。8月21日，零陵光复，桂柳反攻作戰結束。\[3\]
\[4\]隨後攻擊部隊改變任務進行日軍繳械任務。

## 參考文獻

## 外部連結

[最後の勝利～独立混成第八八旅団（沖天）戦記～](http://www2u.biglobe.ne.jp/~surplus/tokushu36.htm)


[Category:1945年抗日战争战役](../Category/1945年抗日战争战役.md "wikilink")
[Category:抗日战争主要战役](../Category/抗日战争主要战役.md "wikilink")
[Category:广西抗日战争战役](../Category/广西抗日战争战役.md "wikilink")

1.  何應欽《日軍侵華八年抗戰史》，台北市：黎民文化出版，2016年，二六九頁。
2.  何應欽《日軍侵華八年抗戰史》，台北市：黎民文化出版，2016年，二七一頁
3.  何應欽《日軍侵華八年抗戰史》，台北市：黎民文化出版，2016年，二七二頁。
4.  [1945年桂柳反攻战役简介](http://zhidao.baidu.com/question/68030401.html)，百度知道。