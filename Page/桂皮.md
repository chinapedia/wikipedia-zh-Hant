[Cinnamomum_verum.jpg](https://zh.wikipedia.org/wiki/File:Cinnamomum_verum.jpg "fig:Cinnamomum_verum.jpg")
**桂皮**又称**肉桂**、**月桂**、**官桂**或**香桂**，为[樟科植物](../Page/樟科.md "wikilink")[天竺桂](../Page/桂皮#.E5.A4.A9.E7.AB.BA.E6.A1.82.md "wikilink")、[阴香](../Page/桂皮#.E9.98.B4.E9.A6.99.md "wikilink")、[细叶香桂](../Page/桂皮#.E7.BB.86.E5.8F.B6.E9.A6.99.E6.A1.82.md "wikilink")、[肉桂或](../Page/桂皮#.E8.82.89.E6.A1.82.md "wikilink")[川桂等树皮的通称](../Page/川桂.md "wikilink")。本品为常用[中药](../Page/中药.md "wikilink")，又为食品[香料或](../Page/香料.md "wikilink")[烹饪](../Page/烹饪.md "wikilink")[调料](../Page/调料.md "wikilink")。商品桂皮的原植物比较复杂，约有十余种，均为樟科樟属植物。

## 历史

桂皮即《[本草拾遗](../Page/本草拾遗.md "wikilink")》之月桂，《[海南本草](../Page/海南本草.md "wikilink")》之天竺桂。《[本草图经](../Page/本草图经.md "wikilink")》载："天竺桂，生西湖，功用似桂，不过烈，今亦稀有"。《[本草纲目](../Page/本草纲目.md "wikilink")》载："此即今闽、粤、浙[中山桂也](../Page/中山桂.md "wikilink")，而[台州天竺最多](../Page/台州.md "wikilink")，故名，大树繁花，结实如[莲子状](../Page/莲子.md "wikilink")，天竺僧人称为月桂是矣"。以上描述，与今之[樟科植物天竺桂](../Page/樟科.md "wikilink")（*Cinnamomum
japonicum*
Sieb.）的形态及产地吻合。本种的皮在[江西](../Page/江西.md "wikilink")、[福建](../Page/福建.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[湖北部分地区使用](../Page/湖北.md "wikilink")。今市售桂皮是樟科樟属樟组（Sect.
*Cinnamomum*）中多种供药用或[香料用植物的树皮](../Page/香料.md "wikilink")，有些地区将药用桂皮又称为官桂。

## 品种

### 肉桂

肉桂（[学名](../Page/学名.md "wikilink")：**，异名**）为樟科[常绿乔木](../Page/常绿乔木.md "wikilink")，又名玉桂、牡桂，高达10米以上，树皮灰褐色，树皮厚可达13毫米，具强烈辛辣芳香味。叶互生或近对生；长椭圆形，或椭圆披针形，长8-20厘米，宽3-5.5厘米；顶端急尖，叶基宽楔形；全缘，具[离基三出脉](../Page/离基三出脉.md "wikilink")。圆锥花序顶生或腋生；花小，径5毫米，花被裂片椭圆形，长3毫米。果椭圆形，长10毫米，径7-8毫米，熟时紫黑色。

植物各部，如其树皮、枝、叶、果、[花梗都可提取](../Page/花梗.md "wikilink")[芳香油或桂油](../Page/芳香油.md "wikilink")，用于食品、饮料、香烟及医药，但常用作香料、化妆品、日用品的香精。树皮出油率为2.15%，桂枝出油率为0.35%，桂叶出油率为0.39%，桂子（幼果）出油率为2.04%。树皮被叫作桂皮。

肉桂原产[中国](../Page/中国.md "wikilink")，分布于[广西](../Page/广西.md "wikilink")、[广东](../Page/广东.md "wikilink")、[福建](../Page/福建.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")、[云南等湿热地区](../Page/云南.md "wikilink")，其中尤以广西最多。[越南](../Page/越南.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[印度尼西亚等地亦有分布](../Page/印度尼西亚.md "wikilink")。肉桂大多为人工栽培，且以种子繁殖为主，这样可使其后代保持亲本的特性，以获得枝下较高的树干，有利于剥取桂皮，因此在生产上很少用无性繁殖方法培育苗木种植。多于秋季剥取，刮去栓皮、阴干。因剥取部位及品质的不同而加工成多种规格，常见的有企边桂、板桂、油板桂、桂通等。生用。

### 锡兰肉桂

锡兰肉桂（[学名](../Page/学名.md "wikilink")：**，异名**）是[双子叶植物药](../Page/双子叶植物.md "wikilink")[樟科](../Page/樟科.md "wikilink")[樟属植物](../Page/樟属.md "wikilink")，原产[斯里兰卡](../Page/斯里兰卡.md "wikilink")，在[广东](../Page/广东.md "wikilink")[海南](../Page/海南.md "wikilink")、[广西](../Page/广西.md "wikilink")[南宁](../Page/南宁.md "wikilink")、[台湾均有栽培](../Page/台湾.md "wikilink")。其树皮，亦称锡兰肉桂。

在[欧美](../Page/欧美.md "wikilink")，[英文所称](../Page/英文.md "wikilink")
Cinnamon 多指此类。

### 阴香

阴香（[学名](../Page/学名.md "wikilink")：**）属樟科，为常绿高大乔木。叶不规则对生或散生，革质，卵形至长卵形，长6－10厘米，宽2.5－4厘米；叶顶短渐尖，基部阔楔形；具明显离基三出脉，[脉腋无](../Page/脉腋.md "wikilink")[腺体](../Page/腺体.md "wikilink")，以此与[香樟区别](../Page/香樟.md "wikilink")。圆锥花序生枝顶或叶腋，花被长5毫米，内外均被毛。果卵形，长8毫米，果托齿裂，齿顶端平。

其叶可作芳香植物原料，亦可入药（味辛，气香，能祛风）。此树也提供木材，常在园林作[绿化树](../Page/绿化树.md "wikilink")、[行道树](../Page/行道树.md "wikilink")。分布于海南，广东、广西、江西、浙江、福建、云南等地。

阴香皮乃阴香的树皮，又名广东桂皮（《中国树木分类学》、坎香草、阴草《生草药性备要》、山肉桂、山玉桂、香胶叶《岭南采药录》、胶桂、土肉桂、假桂枝、山桂、月桂、野玉桂、鸭母桂、香胶仔、潺桂。

### 细叶香桂

细叶香桂（[学名](../Page/学名.md "wikilink")：**）属樟科，为常绿乔木，高达20米，胸径达50厘米；树皮灰色，片状剥落。叶互生或近对生；叶片椭圆形，卵状披针形，长3.5－13厘米，宽2－6厘米；叶全缘，三出脉明显。圆锥花序腋生，花淡黄色，长3－4毫米；花被片近椭圆形，长3毫米。果椭圆形，长7毫米，径5毫米，熟时蓝黑色。树皮含芳香油，作食品加工调料。

### 天竺桂

天竺桂（[学名](../Page/学名.md "wikilink")：**），又名普陀樟，星散分布于中国东部沿海的狭窄范围，因种子间歇结实，加上人为干扰频繁，林下很少见到幼苗幼树。主要分布于[上海](../Page/上海.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[新竹](../Page/新竹.md "wikilink")、[台北等](../Page/台北.md "wikilink")。

## 中医学

[中医认为桂皮味辛热有小毒](../Page/中医.md "wikilink")，入脾肺经。被用来除冷、健胃，有补火助阳，散寒止痛，温经通脉的功效。

桂皮中的化学物质可以通过改善胰岛素敏感性和增加血糖摄取来帮助糖尿病患者。\[1\]

## 外部連結

  - [Cassia
    Gum](https://web.archive.org/web/20111225010946/http://www.foodchem.com/Thickeners/Cassia_gum)
    in China.

[hu:Fahéjfa](../Page/hu:Fahéjfa.md "wikilink")

[Category:調味料](../Category/調味料.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:樟属](../Category/樟属.md "wikilink")

1.