，是一部由[蕭麗紅的](../Page/蕭麗紅.md "wikilink")[小說作品](../Page/小說.md "wikilink")《桂花巷》為基礎改編拍攝的[電影作品](../Page/電影.md "wikilink")。

## 劇情

剔紅是一位自小生長於[漁村的少女](../Page/漁業.md "wikilink")；她的命運似乎一出生就注定了，一如她的[斷掌紋](../Page/断掌.md "wikilink")。

在十二歲那年，剔紅的父母雙雙[過世](../Page/過世.md "wikilink")。十六歲時，弟弟剔江也意外[溺死於](../Page/溺死.md "wikilink")[大海](../Page/大海.md "wikilink")；那年，她決心改變自己已定的[命運](../Page/命運.md "wikilink")，發誓遺忘曾挨過的[貧窮](../Page/貧窮.md "wikilink")、無依。

後來，她憑藉一雙出名的[繡花巧手及一對硬綁出來的](../Page/繡花.md "wikilink")[三寸金蓮入嫁村上豪富之家](../Page/三寸金蓮.md "wikilink")──桂花巷，放棄了原本在漁村的[戀人阿海](../Page/戀人.md "wikilink")。

二十歲那年，她終於擊敗周圍所有蠻橫的[親長](../Page/長輩.md "wikilink")，成為桂花巷一家之主，在自己的王國坐擁一切；但是，命運未曾改變她的一切。

廿三歲，她的老公過世；那年，她的[兒子才五歲](../Page/兒子.md "wikilink")。在[喪夫後](../Page/守寡.md "wikilink")，她因難忍[情慾煎熬而造成](../Page/情慾.md "wikilink")[生命中唯一的](../Page/生命.md "wikilink")[出軌](../Page/出軌.md "wikilink")。

後來，連自己親生的兒子都離她而去。在她六十五歲[即將瞑目而去時](../Page/彌留狀態.md "wikilink")，她回顧自己一生孤獨的命運，感到這些事似乎一出生就注定了，一如她的斷掌紋一般......。　　

## 人物角色

| 角色名稱 | 演員                               | 備註          |
| ---- | -------------------------------- | ----------- |
| 高剔紅  | [陸小芬](../Page/陸小芬.md "wikilink") | 女主角         |
| 楊春樹  | [庹宗華](../Page/庹宗華.md "wikilink") | 捲菸人；剔紅私生女之父 |
| 辛瑞雨  | [周華健](../Page/周華健.md "wikilink") | 高剔紅的丈夫      |
| 秦江海  | [任達華](../Page/任達華.md "wikilink") | 高剔紅的鄰居      |
| 辛惠池  | [李志希](../Page/李志希.md "wikilink") | 辛瑞雨的兒子      |
| 新月   | [林秀玲](../Page/林秀玲.md "wikilink") | 高剔紅的丫環      |
| 高剔紅  | [李淑楨](../Page/李淑楨.md "wikilink") | 幼時剔紅        |
|      |                                  |             |

## 拍攝場景

該片全在澎湖縣[湖西鄉菓葉村](../Page/湖西鄉.md "wikilink")\[1\]及[西嶼鄉池西村與小門村](../Page/西嶼鄉.md "wikilink")[鯨魚洞](../Page/鯨魚洞.md "wikilink")\[2\]取景，以攝取其[閩南式傳統建築特色並呈現出早期臺灣貧苦漁村](../Page/閩南.md "wikilink")[意象](../Page/意象.md "wikilink")。\[3\]

## 獎項

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="入圍">入圍</h3>
<ul>
<li><a href="../Page/第24屆金馬獎.md" title="wikilink">第24屆金馬獎</a></li>
</ul>
<p>:*「最佳劇情片獎」</p>
<p>:*「最佳女配角獎」：<a href="../Page/林秀玲.md" title="wikilink">林秀玲</a></p>
<p>:*「最佳原著音樂獎」：<a href="../Page/陳揚_(作曲家).md" title="wikilink">陳揚</a></p></td>
<td><h3 id="榮獲">榮獲</h3>
<ul>
<li><a href="../Page/第24屆金馬獎.md" title="wikilink">第24屆金馬獎</a></li>
</ul>
<p>:*「最佳電影插曲獎」：陳揚、<a href="../Page/吳念真.md" title="wikilink">吳念真</a>[4]</p>
<ul>
<li><a href="../Page/亞太影展.md" title="wikilink">亞太影展</a></li>
</ul>
<p>:*「最佳女主角獎」：<a href="../Page/陸小芬.md" title="wikilink">陸小芬</a>[5]</p></td>
</tr>
</tbody>
</table>

## 逸事

澎湖縣[望安鄉中社村曾被誤傳為影片的拍攝地點](../Page/望安鄉.md "wikilink")，加上曾被[導遊與解說員以訛傳訛](../Page/導遊.md "wikilink")，造成許多人長年誤以為該地為取景之處。\[6\]

## 參考及備註

<references/>

## 外部連結

  - {{@movies|fEatm0863008|桂花巷}}

  -
  -
  -
  -
  -
  - [桂花巷（Osmanthus
    Alley）](https://web.archive.org/web/20041121032624/http://cinema.nccu.edu.tw/cinemaV2/film_show.htm?SID=1923)，{{〈}}[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink"){{〉}}

[分類:1980年代劇情片](../Page/分類:1980年代劇情片.md "wikilink")
[分類:澎湖縣取景電影](../Page/分類:澎湖縣取景電影.md "wikilink")

[7](../Category/1980年代台灣電影作品.md "wikilink")
[Category:台灣劇情片](../Category/台灣劇情片.md "wikilink")
[Category:台灣小說改編電影](../Category/台灣小說改編電影.md "wikilink")
[Category:中影公司電影](../Category/中影公司電影.md "wikilink")

1.
2.
3.
4.
5.
6.