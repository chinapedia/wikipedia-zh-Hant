**桃叶珊瑚科**只有**桃叶珊瑚属**（*Aucuba*）一[属](../Page/属.md "wikilink")，大约有3-10[种](../Page/种.md "wikilink")，都是生长在[东亚](../Page/东亚.md "wikilink")，分布从[喜马拉雅山东端到](../Page/喜马拉雅山.md "wikilink")[日本](../Page/日本.md "wikilink")，是常绿[灌木或小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")（有2-13[米高](../Page/米.md "wikilink")），单[叶对生](../Page/叶.md "wikilink")，革质，外观类似[月桂树](../Page/月桂树.md "wikilink")，有8-25厘米长，2-7厘米宽，雌雄异株，[花直径为](../Page/花.md "wikilink")4-8毫米，[花瓣](../Page/花瓣.md "wikilink")4；[果实为](../Page/果实.md "wikilink")[红色](../Page/红色.md "wikilink")[浆果](../Page/浆果.md "wikilink")，[直径约为](../Page/直径.md "wikilink")1厘米。

## 分類

  - 种

以前的分类只有三种（桃叶珊瑚、密毛桃叶珊瑚和青木），但最新的分类认为可以分为十种：

  - 斑叶珊瑚 *Aucuba albopunctifolia*
    2-6米高灌木，生长在[中国南方](../Page/中国.md "wikilink")；
  - 桃叶珊瑚 *Aucuba chinensis*
    3-6米高灌木，生长在[中国南方](../Page/中国.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[越南北方](../Page/越南.md "wikilink")；
  - 缘花桃叶珊瑚 *Aucuba chlorascens*
    7米高灌木，生长在[中国](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")；
  - 密花桃叶珊瑚 *Aucuba confertiflora*
    4米高灌木，生长在[中国](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")；
  - 琵琶叶珊瑚 *Aucuba eriobotryifolia*
    13米高小[树](../Page/树.md "wikilink")，生长在[中国](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")；.
  - 纤尾桃叶珊瑚*Aucuba filicauda* 4米高灌木，生长在[中国南方](../Page/中国.md "wikilink")；
  - 密毛桃叶珊瑚 *Aucuba himalaica*
    8-10米高小树，生长在[中国南方](../Page/中国.md "wikilink")，[喜马拉雅山东部](../Page/喜马拉雅山.md "wikilink")，缅甸北方；
  - [青木](../Page/青木.md "wikilink") *Aucuba japonica*
    4米高灌木，生长在[日本南方](../Page/日本.md "wikilink")，[韩国南方](../Page/韩国.md "wikilink")，台湾，中国[浙江](../Page/浙江.md "wikilink")；
  - 倒心叶珊瑚 *Aucuba obcordata* 4米高灌木，生长在[中国南方](../Page/中国.md "wikilink")；
  - 粗梗桃叶珊瑚 *Aucuba robusta*
    灌木，生长在[中国](../Page/中国.md "wikilink")[广西](../Page/广西.md "wikilink")；

1981年的[克朗奎斯特分类法将](../Page/克朗奎斯特分类法.md "wikilink")**桃叶珊瑚属**列入[山茱萸科](../Page/山茱萸科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将桃叶珊瑚属单独列为一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，放在[绞木目下](../Page/绞木目.md "wikilink")，2003年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
II
分类法认为本科可以和](../Page/APG_II_分类法.md "wikilink")[绞木科有选择地合并](../Page/绞木科.md "wikilink")。

## 用途

目前已经作为观赏[植物被各地引进](../Page/植物.md "wikilink")，其中最著名的是洒金桃叶珊瑚（*Aucuba
japonica* Variegata），其叶面有[黄色星点](../Page/黄色.md "wikilink")，是著名的观叶植物。

## 外部链接

  - [《中国植物》中的桃叶珊瑚科](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=103138)

[\*](../Category/桃叶珊瑚属.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")