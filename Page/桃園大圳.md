[桃園大圳工事平面圖<small>（1924年繪）</small>，可見12條支線串連埤塘](https://zh.wikipedia.org/wiki/File:桃園大圳工事平面圖.jpg "fig:桃園大圳工事平面圖（1924年繪），可見12條支線串連埤塘")
**桃園大圳**，原稱八塊厝中壢附近埤圳，為[台灣日治時期重要水利工程之一](../Page/台灣日治時期.md "wikilink")，由總督府工程師[八田與一與](../Page/八田與一.md "wikilink")[狩野三郎等設計](../Page/狩野三郎.md "wikilink")，興建幹線及12條支線串連[埤塘](../Page/池塘.md "wikilink")。

1913年（[大正](../Page/大正.md "wikilink")2年），桃園發生嚴重旱災\[1\]，1916年（大正5年）動土，1924年（大正13年）竣工\[2\]，翌年5月22日舉行通水式，1928年（[昭和](../Page/昭和.md "wikilink")3年）各蓄水池與給水幹線的工程正式完工。灌溉區域涵蓋[桃園市](../Page/桃園市.md "wikilink")[大溪區](../Page/大溪區.md "wikilink")、[八德區](../Page/八德區.md "wikilink")、[桃園區](../Page/桃園區.md "wikilink")、[中壢區](../Page/中壢區.md "wikilink")、[楊梅區](../Page/楊梅區.md "wikilink")、[新屋區](../Page/新屋區.md "wikilink")、[蘆竹區](../Page/蘆竹區.md "wikilink")、[觀音區](../Page/觀音區.md "wikilink")、[大園區](../Page/大園區.md "wikilink")。

## 主要設施

### 取水口

#### 早期

早先桃園大圳的取水口位於現今[石門水庫依山閣一帶](../Page/石門水庫_\(台灣\).md "wikilink")，引[大漢溪](../Page/大漢溪.md "wikilink")（大嵙崁溪）的溪水灌溉。\[3\]

#### 後期

1953年（民國42年）桃園乾旱，大嵙崁溪水位不足，促使[石門水庫興建](../Page/石門水庫_\(台灣\).md "wikilink")，水庫完工後同時建構了[石門大圳灌溉系統](../Page/石門大圳.md "wikilink")，並將桃園大圳的取水口遷移至石門水庫[後池堰西北側](../Page/後池堰.md "wikilink")。\[4\]

## 歲修

為維持大圳的灌溉及供水功能，桃園大圳於每年11月起至翌年的1月停水、進行圳道及沿線圳路的歲修，自1996年起，桃園農田水利會利用停水期間舉行大圳健行。

## 地名

  - 大圳邊：桃園市八德區的地名，位在桃園大圳旁邊而得名。

## 圖庫

<File:Taoyuan> Canal.jpg|大圳進水口 <File:Taoyuan> Canal - The Water
Mill.jpg|水車

## 相關條目

  - [石門水庫](../Page/石門水庫_\(台灣\).md "wikilink")
  - [石門大圳](../Page/石門大圳.md "wikilink")

## 參考來源

## 延伸閱讀

  -
## 外部連結

  - [桃園農田水利會](http://www.tia.org.tw/)
  - [桃園大圳及其支線埤塘](http://maps.google.com.tw/maps/ms?ie=UTF8&hl=zh-TW&brcurrent=3,0x34683d0eceda863f:0x54e44f52583a486a,0&msa=0&msid=117248738816591999671.0004892556cb9f6677130&z=11)

[Category:台灣日治時期產業建築](../Category/台灣日治時期產業建築.md "wikilink")
[Category:台灣圳道](../Category/台灣圳道.md "wikilink")
[Category:桃園市建築物](../Category/桃園市建築物.md "wikilink")
[Category:台湾供水](../Category/台湾供水.md "wikilink")

1.
2.  [](http://www.epochtimes.com/gb/12/12/11/n3750081.htm)，[桃園台地水圳灌區分佈圖](http://i.epochtimes.com/assets/uploads/2012/12/1212110208522063-600x350.jpg)

3.
4.