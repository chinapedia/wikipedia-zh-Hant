<table>
<caption><font size="+1"><a href="../Page/湖南省.md" title="wikilink">湖南省桃江县</a></font></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>桃江县在<a href="../Page/湖南省.md" title="wikilink">湖南省的位置</a><br />
   </p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>行政地位</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>政府驻地</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>行政区划</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">经济<br />
<a href="../Page/国内生产总值.md" title="wikilink">GDP</a>（2007年）</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/面积.md" title="wikilink">面积</a><br />
- 总面积</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2"><a href="../Page/人口.md" title="wikilink">人口</a>（2007年）<br />
- 总人口</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>语言</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/邮政编码.md" title="wikilink">邮政编码</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/电话区号.md" title="wikilink">电话区号</a></p></td>
</tr>
</tbody>
</table>

**桃江县**在[湖南省北部](../Page/湖南省.md "wikilink")，是[益阳市管辖的一个县](../Page/益阳市.md "wikilink")。1952年自[益阳县析置桃江县](../Page/益阳县.md "wikilink")，以境内桃花江得名。

桃江县素称桃花江，20世纪30年代，一首《桃花江是美人窝》的歌曲，唱响[东南亚](../Page/东南亚.md "wikilink")。2000年，桃江县辖12个镇、11个乡、1个民族乡。总人口775444人。

## 地理

桃江县位于湘中偏北,
[资江下游](../Page/资江.md "wikilink"),属山、岗、丘、平原并有的丘陵地，呈[山丘形地貌特征](../Page/山丘形.md "wikilink")，有得天独厚的自然资源和区位优势。

## 物产

享有「中国[楠竹之乡](../Page/楠竹.md "wikilink")」、「有色金属之乡」、「建材之乡」等誉称。[锰矿储量达](../Page/锰.md "wikilink")770多万吨，[锑矿储量仅次于锑都锡矿山](../Page/锑.md "wikilink")，[花岗石储量](../Page/花岗石.md "wikilink")400亿立方米；楠竹面积70万亩，居全国第三、全省之首。

## 交通

水陆交通四通八达，县城至益阳市城区20公里，有超一等级公路相连，距省会[长沙市](../Page/长沙市.md "wikilink")94公里，有[高速公路贯通](../Page/高速公路.md "wikilink")，西南大通道[怀桃公路](../Page/怀桃公路.md "wikilink")、[长石铁路](../Page/长石铁路.md "wikilink")、[洛湛铁路和](../Page/洛湛铁路.md "wikilink")[资江水域等交通干线横贯全县](../Page/资江水域.md "wikilink")。

## 经济

电力、通信、城建、水利等基础设施日趋完善。

## 旅游

旅游资源十分丰富，已形成「桃花江竹海」、「楚南名山浮邱山」、「桃花江森林公园」等十大旅游景区，「竹香飘四海，桃花映九州」折射出桃江深厚的文化底蕴，「竹外桃花三两枝」的优美意境吸引着无数游人流连忘返。

浮邱山位于桃江县西南12公里，主峰海拔752米，面积58平方公里，人称"小南岳"。浮邱山有48峰，山顶有一处佛道同居的浮邱古寺。

## 人物

桃江的民俗风气，古尚俭朴。明万历《益阳县志》载：“民尚朴素，勤于衣桑，拙干商贾。”桃江历史上名人辈出，当代比较著名的人士有因《将军吟》而荣获首届[茅盾文学奖的](../Page/茅盾.md "wikilink")[莫应丰先生](../Page/莫应丰.md "wikilink")、著名数学家中国科学院院士[丁夏畦先生](../Page/丁夏畦.md "wikilink")、著名水利专家中国工程院院士[文伏波先生](../Page/文伏波.md "wikilink")、著名基因[遗传学家中国工程院院士](../Page/遗传学.md "wikilink")[夏家辉先生等](../Page/夏家辉.md "wikilink")。历史上著名人物有明朝[江西巡撫](../Page/江西巡撫.md "wikilink")[郭都賢](../Page/郭都賢.md "wikilink")、清朝两广总督[陶澍](../Page/陶澍.md "wikilink")、清朝[湘軍名将](../Page/湘軍.md "wikilink")[胡林翼](../Page/胡林翼.md "wikilink")（益阳人，陶澍女婿，湘军领袖）。

## 来源参考

[桃江县](../Category/桃江县.md "wikilink")
[县](../Category/益阳区县市.md "wikilink")
[益阳](../Category/湖南省县份.md "wikilink")