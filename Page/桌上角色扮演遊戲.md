{{ Request translation|en}}

**桌上角色扮演游戏**（**Tabletop Role Playing
Game**，[縮寫](../Page/縮寫.md "wikilink")：*'
TRPG*'）是一种最初的[角色扮演游戏](../Page/角色扮演游戏.md "wikilink")。[龙与地下城的出现使它曾经流行过](../Page/龙与地下城.md "wikilink")。属于广义的[桌上游戏的一种](../Page/桌上游戏.md "wikilink")。

桌上角色扮演游戏一般需要3－5人参与，其中一人为[遊戲主持者](../Page/遊戲主持者.md "wikilink")（Game
Master，簡稱GM），在《[龙与地下城](../Page/龙与地下城.md "wikilink")》等战棋TRPG中称为[地下城主](../Page/地下城主.md "wikilink")，在《[黑暗世界](../Page/黑暗世界.md "wikilink")》等扮演向TRPG中称为「故事叙述者」（Storyteller，簡稱ST），在泛用系统（[GURPS](../Page/GURPS.md "wikilink")）等TRPG中则称为[游戏主持人](../Page/游戏主持人.md "wikilink")，负责解说剧情，扮演[非玩家角色](../Page/非玩家角色.md "wikilink")，如村民、怪物等，记录时间，对玩家的行为作出裁决等。其他的人则是玩家，扮演游戏中的角色。進行遊戲的過程被稱為「[跑團](../Page/跑團.md "wikilink")」。

以《龍與地下城》為例，玩家首先选择角色姓名，然后用掷[骰子的方法决定角色的能力](../Page/骰子.md "wikilink")，最后选择角色的职业。然后，游戏主持人开始介绍游戏的剧情，游戏开始。在游戏中，玩家需要购买装备、杀死怪物，了解线索。玩一次TRPG需要很多的时间进行游戏，短则一星期，长则数月。当然也有很短的剧情，可以在一天之内完成。

而在黑暗世界跟GURPS是用能力點數購買能力背景的方式來創人物，根據買的背景在遊戲進行時要注意自己角色的身份來扮演，遊戲方式也不是單純的殺怪解任務，玩家可能陷入很複雜的陰謀中，但是單純打鬥的團也不是不行的。

除了這兩類，網路或是一些角色扮演同好出版的規則可能會有更不同的玩法。

玩TRPG时，游戏主持人的权力非常大，主导着游戏的风格与走向。参与者既是故事的阅读者也是故事的创作者。

## 著名的桌上角色扮演游戏

  - [龙与地下城](../Page/龙与地下城.md "wikilink")（*Dungeons & Dragons*）
  - [黑暗世界系列](../Page/黑暗世界系列.md "wikilink")（*The World of Darkness*）
  - [克苏鲁的呼唤](../Page/克苏鲁的呼唤.md "wikilink")（*The Call of Cthulhu*）
  - [北欧传奇](../Page/北欧传奇.md "wikilink")（*Runequest*）
  - [洞穴与巨人](../Page/洞穴与巨人.md "wikilink")（*Tunnels and Trolls*）
  - [劍之世界](../Page/劍之世界.md "wikilink")（）
  - [暗夜魔法使](../Page/暗夜魔法使.md "wikilink")（）

## 用語

  - 跑團
    一般进行TRPG的游戏过程俗称跑团。原因在于TRPG需要多位玩家组团才能开始游戏。跑团通常需要有一个人扮演[DM或其他名称的游戏主持者](../Page/DM.md "wikilink")，其他的人扮演[玩家](../Page/玩家.md "wikilink")。除了一般传统面对面开始进行游戏（俗称面團），还有在網絡上通过[即時通訊軟件](../Page/即時通訊軟件.md "wikilink")、[聊天室或](../Page/聊天室.md "wikilink")[網路討論區等网络形式进行的TRPG游戏过程](../Page/網路討論區.md "wikilink")（俗称网团）。

## 参考文献

1.  *孙百英主编，游戏之王：纵横电脑游戏世界，科学普及出版社，1998年 ISBN 7110044939*

## 参看

  - [泛用无界角色扮演系统](../Page/泛用无界角色扮演系统.md "wikilink")

## 外部連結

  - [香港桌上角色扮演遊戲同好會](http://www.trpg.org.hk/)
  - [幻想的國度 DOI
    TRPG](https://web.archive.org/web/20110629181009/http://www.doitrpg.org/)
  - [幻想的國度臉書推廣頁](https://www.facebook.com/doitrpg)
  - [香港TRPG面團專區](http://www.goddessfantasy.net/bbs/index.php?board=1400.0)
  - [The Ring of Wonder](http://trow.cc/forum)
  - [純美蘋果園](http://www.goddessfantasy.net)
  - [Pen & Paper](http://www.pen-paper.net/)
  - [RPGnet](http://www.rpg.net/)
  - [馬場秀和ライブラリ](http://www004.upp.so-net.ne.jp/babahide/index.html)

[桌上角色扮演遊戲](../Category/桌上角色扮演遊戲.md "wikilink")
[T](../Category/角色扮演遊戲.md "wikilink")