**桑塔納盜龍屬**（[學名](../Page/學名.md "wikilink")：*Santanaraptor*）是[獸腳亞目下的一](../Page/獸腳亞目.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於[白堊紀早期的](../Page/白堊紀.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")，相當於[阿爾比階](../Page/阿爾比階.md "wikilink")\[1\]或[阿普第階](../Page/阿普第階.md "wikilink")\[2\]。某些古生物學家認為，桑塔納盜龍屬於[暴龍超科](../Page/暴龍超科.md "wikilink")，是第一個發現於南方各大陸的暴龍類恐龍\[3\]\[4\]。桑塔納盜龍可能是[虛骨龍類的基礎物種](../Page/虛骨龍類.md "wikilink")\[5\]。

牠的[化石在](../Page/化石.md "wikilink")1996年於[巴西被發現](../Page/巴西.md "wikilink")，身長約1.25公尺。這個標本是個幼年個體，主要是身體後半部的骨頭，上有礦化的軟組織\[6\]。成年個體的身長可能達2.5公尺\[7\]。

## 分類

[正模標本](../Page/正模標本.md "wikilink")（編號MN
4802-V）是三節[尾椎](../Page/尾椎.md "wikilink")、[人字形骨](../Page/人字形骨.md "wikilink")、[坐骨](../Page/坐骨.md "wikilink")、[股骨](../Page/股骨.md "wikilink")、[脛骨](../Page/脛骨.md "wikilink")、[腓骨](../Page/腓骨.md "wikilink")，以及[表皮](../Page/表皮.md "wikilink")、[肌肉纖維](../Page/肌肉.md "wikilink")、可能還有[血管](../Page/血管.md "wikilink")…等[軟組織](../Page/軟組織.md "wikilink")\[8\]。

[模式種是](../Page/模式種.md "wikilink")*S. placidus*，是由A. W. A.
Kellner於1999年描述、命名的\[9\]。屬名意為「[桑塔納組的盜賊](../Page/桑塔納組.md "wikilink")」；種名則是以Placido
Cidade Nuvens為名，他曾在當地創建一家博物館\[10\]。

桑塔那盜龍最初被認為是種[手盜龍類](../Page/手盜龍類.md "wikilink")，但科學家目前根據股骨的數個特徵，認為桑塔那盜龍是種基礎[虛骨龍類](../Page/虛骨龍類.md "wikilink")。根據坐骨顯示，桑塔那盜龍可能是[嗜鳥龍的近親](../Page/嗜鳥龍.md "wikilink")，嗜鳥龍是種[侏儸紀晚期的虛骨龍類](../Page/侏儸紀.md "wikilink")。\[11\]

## 參考

## 外部連結

  - [*Thescelosaurus\!* (under Coelurosauria
    *i.s.*)](https://web.archive.org/web/20110717025057/http://www.thescelosaurus.com/coelurosauria.htm)

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:暴龍超科](../Category/暴龍超科.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.
2.

3.
4.  Holtz, Thomas R. Jr. (2011) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages,*
    [Winter 2010
    Appendix.](http://www.geol.umd.edu/~tholtz/dinoappendix/HoltzappendixWinter2010.pdf)

5.
6.
7.  <http://www.dinohunters.com/History/Santana%20Raptor.htm>

8.

9.

10.
11.