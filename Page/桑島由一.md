**桑島由一**（、），日本的男性[輕小說作家](../Page/輕小說.md "wikilink")，也是遊戲[劇本作家](../Page/劇本作家.md "wikilink")、[插畫家與](../Page/插畫家.md "wikilink")[作詞家](../Page/作詞家.md "wikilink")。[東京都](../Page/東京都.md "wikilink")[目黑區出身](../Page/目黑區.md "wikilink")。

遊戲的代表作為與[山口昇](../Page/山口昇.md "wikilink")（ヤマグチノボル）合作劇本的[Green
Green](../Page/青青校樹_\(遊戲\).md "wikilink")，中譯為青青校樹、或鐘之音學園，
曾移植到PS2與XBOX上，2003年也改編為TV動畫與OVA。小說代表作為『[神樣家族](../Page/神樣家族.md "wikilink")』在2006年被動畫化。

## 主要作品

### 小說

  - [MEDIA
    FACTORY](../Page/MEDIA_FACTORY.md "wikilink")・[MF文庫J](../Page/MF文庫J.md "wikilink")

<!-- end list -->

  - [神樣家族](../Page/神樣家族.md "wikilink")
  - [南青山少女ブックセンター](../Page/南青山少女ブックセンター.md "wikilink")
  - [Green Green](../Page/Green_Green.md "wikilink") 青青校樹、鐘之音學園
  - [Green Green](../Page/Green_Green.md "wikilink") 男孩女孩

<!-- end list -->

  - [集英社](../Page/集英社.md "wikilink")・[スーパーダッシュ文庫](../Page/スーパーダッシュ文庫.md "wikilink")

<!-- end list -->

  - [TO THE CASTLE](../Page/TO_THE_CASTLE.md "wikilink")
  - [TO THE CASTLE DISCO
    UNDERGROUND](../Page/TO_THE_CASTLE_DISCO_UNDERGROUND.md "wikilink")

<!-- end list -->

  - [角川書店](../Page/角川書店.md "wikilink")・[角川スニーカー文庫](../Page/角川スニーカー文庫.md "wikilink")

<!-- end list -->

  - [大沢さんに好かれたい。](../Page/大沢さんに好かれたい。.md "wikilink")

<!-- end list -->

  - [マイクロマガジン](../Page/マイクロマガジン.md "wikilink")・[二次元ドリームノベルズ](../Page/二次元ドリームノベルズ.md "wikilink")

<!-- end list -->

  - [魔法少女リオ](../Page/魔法少女リオ.md "wikilink")

### 書籍

  - 宅到家完全手冊（英知出版）
  - [MOETAN～萌單Ⅱ](../Page/MOETAN～萌單Ⅱ.md "wikilink")～上卷（劇情故事執筆）（三才BOOKS）
  - [MOETAN～萌單Ⅱ](../Page/MOETAN～萌單Ⅱ.md "wikilink")～下卷（劇情故事執筆）（三才BOOKS）

### 作詞

  - Brand New Morning ([神樣家族OP](../Page/神樣家族.md "wikilink"))
  - 図書館では教えてくれない、天使の秘密（[神樣家族ED](../Page/神樣家族.md "wikilink")）
  - きっとずっときっとね（神樣家族 應援願望的主題歌）
  - Green Green ([Green GreenOP](../Page/Green_Green.md "wikilink"))
  - 星空（[Green Green](../Page/Green_Green.md "wikilink")）
  - 男の子（[Green Green](../Page/Green_Green.md "wikilink")）
  - 空に届く（[Green Green](../Page/Green_Green.md "wikilink")）
  - スクールバス（[Green Green](../Page/Green_Green.md "wikilink")）
  - 彩り（[Green Green](../Page/Green_Green.md "wikilink")）
  - 樂園之翼([灰色的果實OP](../Page/灰色的果實.md "wikilink"))

### 遊戲作品

  - [Canary ～この想いを歌に乗せて～](../Page/Canary_～この想いを歌に乗せて～.md "wikilink")
  - [青青校樹](../Page/青青校樹_\(遊戲\).md "wikilink")
  - [魔女的茶會](../Page/魔女的茶會.md "wikilink")
  - [私立秋葉原學園](../Page/私立秋葉原學園.md "wikilink")
  - [ガッデーム&ジュテーム](../Page/ガッデーム&ジュテーム.md "wikilink")
  - [CARNIVAL](../Page/CARNIVAL.md "wikilink")
  - [Green Green2
    恋のスペシャルサマー](../Page/Green_Green2_恋のスペシャルサマー.md "wikilink")
  - [Re:](../Page/Re:.md "wikilink")
  - [ボンバーヘッヘ](../Page/ボンバーヘッヘ.md "wikilink")
  - [灰色的果實](../Page/灰色的果實.md "wikilink")
  - [灰色的迷宮](../Page/灰色的迷宮.md "wikilink")
  - [灰色的樂園](../Page/灰色的樂園.md "wikilink")
  - [Innocent Girl](../Page/Innocent_Girl.md "wikilink")

## 外部連結

  - [クリアラバーソウル](http://www2s.biglobe.ne.jp/~kook/)（公式網站）

[Category:日本輕小說作家](../Category/日本輕小說作家.md "wikilink")
[Category:日本成人遊戲編劇](../Category/日本成人遊戲編劇.md "wikilink")
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")