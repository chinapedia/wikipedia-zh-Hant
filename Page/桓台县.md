**桓台县**在[中国](../Page/中国.md "wikilink")[山东省中部偏北](../Page/山东省.md "wikilink")、[小清河南岸](../Page/小清河.md "wikilink")，是[淄博市所辖的一个](../Page/淄博市.md "wikilink")[县](../Page/县.md "wikilink")；位于山东半岛中部，地处鲁中山区和鲁北平原的结合地带。

## 历史

[金哀宗](../Page/金哀宗.md "wikilink")[正大五年](../Page/正大.md "wikilink")（1228年）置新城县。1914年易名耏水县，旋改桓台县；因境内有[齐桓公戏马台而名](../Page/齐桓公.md "wikilink")。

## 行政区划

桓台县辖2个街道，7个镇

  - 索镇街道、少海街道、起凤镇、田庄镇、荆家镇、马桥镇、新城镇、唐山镇、果里镇。

## 参考文献

## 外部链接

  - [淄博市桓台县政府网站](http://www.huantai.gov.cn/)
  - [桓台县桓台信息港](http://www.huantai.net/)

{{-}}

[淄博](../Category/山东省县份.md "wikilink")
[桓台县](../Category/桓台县.md "wikilink")
[县](../Category/淄博区县.md "wikilink")