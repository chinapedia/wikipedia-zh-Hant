**梁守槃**（），[福建省](../Page/福建省.md "wikilink")[福州市人](../Page/福州市.md "wikilink")，[中国科学院院士](../Page/中国科学院院士.md "wikilink")，中国著名[导弹总体和](../Page/导弹.md "wikilink")[火箭技术专家](../Page/火箭.md "wikilink")，中国导弹与[航天技术的重要开拓者之一](../Page/航天.md "wikilink")。中国第一枚液体燃料短程[弹道导弹](../Page/弹道导弹.md "wikilink")、“海鹰”飞航式[反舰导弹](../Page/反舰导弹.md "wikilink")、“[鹰击八号](../Page/鹰击8反舰导弹.md "wikilink")”系列反舰导弹系统的总设计师，被尊称为“中国海防导弹之父”\[1\]。与[任新民](../Page/任新民.md "wikilink")、[屠守锷](../Page/屠守锷.md "wikilink")、[黄纬禄并称为中国](../Page/黄纬禄.md "wikilink")“航天四老”。

## 生平

1916年4月13日出生于福建省福州市。1933年考取[清华大学机械系航空组](../Page/清华大学.md "wikilink")，1937年毕业获工程[学士学位](../Page/学士.md "wikilink")，1938年赴[美国获](../Page/美国.md "wikilink")[麻省理工学院航空工程](../Page/麻省理工学院.md "wikilink")[硕士学位](../Page/硕士.md "wikilink")。1940年至1942年在昆明[西南联合大学航空系和机械系任教](../Page/西南联合大学.md "wikilink")，1945年到1952年任[浙江大学航空系系主任](../Page/浙江大学.md "wikilink")。

1952年9月，奉调到[哈尔滨军事工程学院空军工程系任教](../Page/哈尔滨军事工程学院.md "wikilink")。1956年被授予[中国人民解放军](../Page/中国人民解放军.md "wikilink")[上校军衔](../Page/上校.md "wikilink")。
1956年调赴[国防部第五研究院](../Page/国防部第五研究院.md "wikilink")；1965年转为[第七机械工业部三院](../Page/第七机械工业部.md "wikilink")（三院被定为专攻海防导弹），曾担任七机部总工程师。1980年当选为[中国科学院](../Page/中国科学院.md "wikilink")[学部委员](../Page/学部委员.md "wikilink")（院士）。1985年当选为国际宇航科学院院士。曾任中国航空航天工业部、航天工业公司高级技术顾问，[中国航天科技集团公司和](../Page/中国航天科技集团公司.md "wikilink")[中国航天科工集团公司的高级技术顾问](../Page/中国航天科工集团公司.md "wikilink")。

2006年10月，与[钱学森](../Page/钱学森.md "wikilink")、[任新民](../Page/任新民.md "wikilink")、[屠守锷](../Page/屠守锷.md "wikilink")、[黄纬禄等共](../Page/黄纬禄.md "wikilink")5位专家获“[中国航天事业五十年最高荣誉奖](../Page/中国航天.md "wikilink")”\[2\]。2009年9月5日，因病医治无效在北京逝世，享年93岁。\[3\]

## 参考资料

<references />

## 参看

  - [中国航天史](../Page/中国航天史.md "wikilink")

## 外部链接

  - [中国海防导弹之父——中国科学院院士梁守槃](http://tech.sina.com.cn/d/2006-05-15/1705937993.shtml)
  - [航天人物——梁守槃](https://web.archive.org/web/20150923205110/http://www.cnsa.gov.cn/n615708/n620172/n620642/167961.html)，国家航天局网站

[L梁](../Category/中国物理学家.md "wikilink")
[L梁](../Category/中国航天.md "wikilink")
[L梁](../Category/清华大学校友.md "wikilink")
[L梁](../Category/國立清華大学校友.md "wikilink")
[L梁](../Category/麻省理工學院校友.md "wikilink")
[L梁](../Category/国立西南联合大学校友.md "wikilink")
[Category:浙江大学教授](../Category/浙江大学教授.md "wikilink")
[L梁](../Category/福州人.md "wikilink")
[Shou守槃](../Category/梁姓.md "wikilink")

1.  [闽籍院士
    梁守槃](http://www.fjrs.gov.cn/xxgk/cszy/yszjw/ysfc/mjys/201307/t20130701_601357.htm)
    ，福建院士专家网。
2.  [钱学森任新民屠守锷黄纬禄梁守槃获“中国航天事业五十年最高荣誉奖”](http://www.gmw.cn/01gmrb/2006-10/09/content_489376.htm)，光明网。
3.  [告别“中国飞鱼”之父](http://www.gmw.cn/content/2009-09/14/content_980943.htm)，光明日报，2009-09-13。