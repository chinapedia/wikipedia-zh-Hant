**梁家榮**（），暱稱"榮總"，現任[香港特區政府](../Page/香港特區政府.md "wikilink")[廣播處長](../Page/廣播處長.md "wikilink")，曾任[亞視新聞高級副總裁](../Page/亞視新聞.md "wikilink")、[無綫新聞經理與記者](../Page/無綫新聞.md "wikilink")。

## 簡介

梁家榮1975年畢業於[香港浸會學院](../Page/香港浸會學院.md "wikilink")[傳理系](../Page/傳理系.md "wikilink")\[1\]。70年代中在[佳藝電視](../Page/佳藝電視.md "wikilink")（佳視）擔任記者及主播。佳視於1978年8月22日倒閉後，梁家榮跟[蘇凌峰及](../Page/蘇凌峰.md "wikilink")[趙應春轉職](../Page/趙應春.md "wikilink")[無線電視](../Page/無線電視.md "wikilink")，並於1980年出任[無線新聞主播](../Page/無線新聞.md "wikilink")，其淡定、穩重作風甚為觀眾歡迎。梁後來在1990年移居[美國](../Page/美國.md "wikilink")，在[三藩市與同為無線記者的](../Page/三藩市.md "wikilink")[斯美玲在當地中文電視台](../Page/斯美玲.md "wikilink")[KTSF合作報導](../Page/KTSF.md "wikilink")[粵語新聞節目](../Page/粵語新聞.md "wikilink")。

梁於1993年底返回香港，並於1994年返回[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")[新聞及資訊部出任新聞經理一職並兼任](../Page/無綫新聞.md "wikilink")《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》主播，期間改革無線新聞有關中英文譯名的制度（該制度一直沿用至今）。[羅燦於](../Page/羅燦.md "wikilink")2000年接任新聞經理，梁轉職《[香港經濟日報](../Page/香港經濟日報.md "wikilink")》屬下的網上財經新聞網站「ETVision
Limited」。後來ETVision
Limited因[科網泡沫爆破而停止營運](../Page/科網泡沫.md "wikilink")，而梁則被調任同系的[經濟日報出版社](../Page/經濟日報出版社.md "wikilink")。

### 亞視新聞部副總裁

梁家榮於2006年加入[亞洲電視成為新聞部顧問](../Page/亞洲電視.md "wikilink")，2007年七月初成為新聞部副總裁，其後退下新聞主播一職（而[馮兆寧則重返](../Page/馮兆寧.md "wikilink")《[六點鐘新聞](../Page/六點鐘新聞.md "wikilink")》作主播，但梁於2009年2月12日解僱馮）。梁曾於2007年10月29日再次出任主播\[2\]。

梁家榮於2008年12月8日辭任亞視新聞部副總裁\[3\]。2008年12月15日，梁家榮表示，留任亞視新聞及公共事務部\[4\]，代由[譚衛兒出任亞視新聞部副總裁](../Page/譚衛兒.md "wikilink")。而因亞視新聞部高級副總裁[關偉已於](../Page/關偉.md "wikilink")2008年12月8日辭任、重返無綫出任總經理助理，梁家榮2008年12月18日接替關偉晉升亞視新聞部高級副總裁。

2011年9月5日，亞洲電視宣佈，梁家榮以私人理由請辭亞視「高級副總裁（新聞及公共事務）」，即日生效，其職位則由副總裁（新聞及公共事務二部）[劉瀾昌接任](../Page/劉瀾昌.md "wikilink")。亞視當晚的《六點鐘新聞》報道消息時播放梁家榮的錄音訪問，梁家榮解釋，因為盡了所有努力仍然未能阻止前[中共中央總書記](../Page/中共中央總書記.md "wikilink")[江澤民的](../Page/江澤民.md "wikilink")[誤報死訊新聞出街](../Page/亞洲電視誤報江澤民逝世事件.md "wikilink")，他引咎辭職\[5\]。言下之意，應該指有亞視高層施壓，要求該報導當日非播出不可。亞視新聞部疑被干預事件也引起國際關注，[英國廣播公司](../Page/英國廣播公司.md "wikilink")(BBC)也有報道梁家榮辭職事件\[6\]

梁家榮辭任亞視新聞部工作後，從事顧問工作，並於[香港中文大學新聞與傳播學院擔任兼任講師](../Page/香港中文大學.md "wikilink")。\[7\]

### 廣播處長

2015年8月6日，香港特區政府宣布，經過公開及內部招聘後，委任梁家榮接替[鄧忍光擔任](../Page/鄧忍光.md "wikilink")[廣播處長](../Page/廣播處長.md "wikilink")，8月7日履新。\[8\]

## 曾監製節目

  - [亞洲電視](../Page/亞洲電視.md "wikilink")[本港台](../Page/本港台.md "wikilink")/[亞洲台](../Page/亞洲電視亞洲台.md "wikilink")
      - [時事追擊](../Page/時事追擊.md "wikilink")
      - [金錢世界](../Page/金錢世界.md "wikilink")
      - [主播天下](../Page/主播天下.md "wikilink")
      - [ATV焦點](../Page/ATV焦點.md "wikilink")
  - [國際台](../Page/亞洲電視國際台.md "wikilink")/[亞洲台](../Page/亞洲電視亞洲台.md "wikilink")
      - [時事內幕追擊](../Page/時事內幕追擊.md "wikilink")

## 個人軼事

梁家榮的妻子是前[電訊盈科公關](../Page/電訊盈科.md "wikilink")[胡雪姬](../Page/胡雪姬.md "wikilink")，[小姨是](../Page/親屬.md "wikilink")[香港記者協會前主席](../Page/香港記者協會.md "wikilink")[胡麗雲](../Page/胡麗雲.md "wikilink")。梁家榮曾經為昔日舊同事[伍晃榮](../Page/伍晃榮.md "wikilink")（已故）的書《波係圓嘅：阿盲44年傳媒生涯回憶錄》寫序，也曾接受亞洲電視節目《[電視風雲50年](../Page/電視風雲50年.md "wikilink")》訪問。\[9\]

2008年12月28日下午，梁家榮駕駛私家車駕經[旺角](../Page/旺角.md "wikilink")[彌敦道](../Page/彌敦道.md "wikilink")664號對開近[亞皆老街交界時撞傷一名年約](../Page/亞皆老街.md "wikilink")44歲的女途人，女途人受傷送院，梁家榮則無礙。\[10\]\[11\]

## 註釋

<div style="font-size:small" class="references-2column">

<references />

</div>

## 參考

  - [掌港台在望
    梁家榮接受品格審查](http://news.sina.com.hk/cgi-bin/nw/show.cgi/256/3/1/792868/1.html)
  - [老行尊回歸 梁家榮亞視出山](http://hk.news.yahoo.com/070115/12/1zzqe.html)
  - [梁家榮留任亞視新聞部](http://hk.news.yahoo.com/article/081215/4/9qxg.html)

[Category:香港傳媒工作者](../Category/香港傳媒工作者.md "wikilink")
[Category:香港記者](../Category/香港記者.md "wikilink")
[Category:前亞洲電視新聞報導員](../Category/前亞洲電視新聞報導員.md "wikilink")
[Category:前無綫電視新聞報導員](../Category/前無綫電視新聞報導員.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:香港政府官員](../Category/香港政府官員.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")
[L](../Category/香港培正中學校友.md "wikilink")
[L](../Category/香港中文大學教師.md "wikilink")
[K](../Category/梁姓.md "wikilink")
[Category:佳藝電視](../Category/佳藝電視.md "wikilink")

1.
2.  自2007年10月29日《六點鐘新聞》(請參見[aTV亞視新聞網頁](http://www.hkatvnews.com/v5) )
3.
4.
5.
6.
7.
8.
9.  [電視風雲50年 電視台新聞主播](http://www.56.com/u28/v_MzA1MDI1Nzc.html)
    2008年3月9日
10.
11.