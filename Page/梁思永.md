**梁思永**（）\[1\]\[2\]，中国著名[考古學家](../Page/考古學家.md "wikilink")，[梁啟超第二子](../Page/梁啟超.md "wikilink")。

## 生平

原藉[廣東省](../Page/廣東省.md "wikilink")[新會縣](../Page/新會縣.md "wikilink")，出生於[澳門](../Page/澳門.md "wikilink")\[3\]，\[4\]\[5\]。幼年曾在日本念小學，回國後進入清華學校留美班，1923年畢業後赴[美國留學](../Page/美國.md "wikilink")，攻讀[考古學及](../Page/考古學.md "wikilink")[人類學](../Page/人類學.md "wikilink")，1930年獲得[哈佛大學碩士學位後](../Page/哈佛大學.md "wikilink")，回國進入[中央研究院史語所考古組](../Page/中央研究院.md "wikilink")。1930年至1931年先後參與[黑龍江](../Page/黑龍江.md "wikilink")[昂昂溪遺址](../Page/昂昂溪.md "wikilink")、[通遼河](../Page/通遼河.md "wikilink")[新石器時代遺址](../Page/新石器時代.md "wikilink")、[河南](../Page/河南.md "wikilink")[安陽小屯和後岡](../Page/安陽.md "wikilink")、[山東](../Page/山東.md "wikilink")[歷城](../Page/歷城.md "wikilink")[龍山鎮](../Page/龍山鎮.md "wikilink")[城子崖等地的考古發掘工作](../Page/城子崖.md "wikilink")。隨後因患烈性肋膜炎臥病兩年，1934年漸癒後即赴安陽主持西北岡的發掘工作，直至抗日戰爭爆發始隨史語所遷往[四川](../Page/四川.md "wikilink")\[6\]。1954年4月2日，长期带病坚持工作的他心脏病发作在[北京逝世](../Page/北京.md "wikilink")，终年50岁，逝世后被安葬于北京[八宝山革命公墓](../Page/八宝山革命公墓.md "wikilink")。一年之后，嫂子[林徽因病逝](../Page/林徽因.md "wikilink")，也被梁家人安葬在相隔仅几米的另一处墓地。\[7\]。

他在[殷墟第四次發掘過程中](../Page/殷墟.md "wikilink")，確認柱礎石、窖穴等考古遺跡，復原建築遺址，確認了[仰韶文化](../Page/仰韶文化.md "wikilink")、[龍山文化和](../Page/龍山文化.md "wikilink")[商代文化的疊壓關係](../Page/商代.md "wikilink")，成為中國考古學的典範。1948年與其兄长[梁思成](../Page/梁思成.md "wikilink")（建築學家）同時獲選為第一屆[中央研究院院士](../Page/中央研究院院士.md "wikilink")。

配偶李福曼為梁思成舅舅的女兒，因為吃穿全由姑母李仙蕙家資助，後來嫁給梁思永。

## 著作

  - 《城子崖遺址發掘報告》
  - 《梁思永考古論文集》
  - 《侯家庄》（高去尋補輯）

## 參考資料

<div class="references-small">

<references />

</div>

[category:1904年出生](../Page/category:1904年出生.md "wikilink")
[category:1954年逝世](../Page/category:1954年逝世.md "wikilink")

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中央研究院研究員](../Category/中央研究院研究員.md "wikilink")
[Category:中華民國考古學家](../Category/中華民國考古學家.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:新會人](../Category/新會人.md "wikilink")
[Category:澳門人](../Category/澳門人.md "wikilink")
[Si思](../Category/梁啟超家族.md "wikilink")
[Si思永](../Category/梁姓.md "wikilink")
[Category:葬于北京市八宝山革命公墓](../Category/葬于北京市八宝山革命公墓.md "wikilink")

1.  [中央研究院
    梁思永院士基本資料](https://db1n.sinica.edu.tw/textdb/ioconas/02.php?func=22&_op=?ID:HD017)

2.  梁思永1904年11月13日誕生在澳門，他是王桂荃婆的長子，是梁啟超的次子，比二舅小4歲。
    吳荔明著.《梁啟超和他的兒女們》.北京大學出版社,2009.01.第188頁

3.  亦說上海、日本橫濱

4.  [中央研究院史語所 世紀考古大發現 殷墟史話 學者小傳
    梁思永](http://www.sinica.edu.tw/~dmuseum/4/Liang%20Si-yong.htm)

5.  [科學領域網站 考古學家 梁思永](http://www.21kexue.cn/html/2/20080215/1913.html)

6.
7.