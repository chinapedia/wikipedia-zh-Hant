**梁材遠**（****），人稱「材叔」，現為香港[無綫電視監製兼戲劇製作部經理](../Page/無綫電視.md "wikilink")。

## 背景

梁材遠在1969年畢業於[慈幼英文學校](../Page/慈幼英文學校.md "wikilink")，1975年於[香港浸會大學歷史學系畢業](../Page/香港浸會大學.md "wikilink")，同年加入[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")，在《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》當見習編劇及資料搜集。於1979年開始參與多部經典電視劇編導工作《[抉擇](../Page/抉擇_\(電視劇\).md "wikilink")》、《[網中人](../Page/網中人.md "wikilink")》、《[火玫瑰](../Page/火玫瑰.md "wikilink")》等等。1985年晉升監製，製作青春劇《[中四丁班](../Page/中四丁班.md "wikilink")》，獲得「紐約電影電視節銅獎」。其後製作不少話題性的劇集，包括《[誓不低頭](../Page/誓不低頭.md "wikilink")》、《[成功路上](../Page/成功路上.md "wikilink")》、《[回到未嫁時](../Page/回到未嫁時.md "wikilink")》等等。當中與韋家輝合作的《誓不低頭》，播出後創下高收視，震撼廣大觀眾，惟劇情牽涉警界黑暗面，遭政府機關抗議，香港電視廣播局罰款警告無線。《誓》劇於海外錄影帶市場獲選為88年最受歡迎十大無線劇之一。
1991年，梁材遠曾轉投[亞洲電視](../Page/亞洲電視.md "wikilink")。

梁材遠於1995年升任製作經理，轉為策畫作品，策畫《[情濃大地](../Page/情濃大地.md "wikilink")》、《[茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")》、《[酒是故鄉醇](../Page/酒是故鄉醇.md "wikilink")》等劇。於2004年重回監製職位，而他的風格轉為製作溫情倫理的成本電視劇，題材多元化，包括《[舞動全城](../Page/舞動全城.md "wikilink")》、《[金石良緣](../Page/金石良緣.md "wikilink")》、《[老友狗狗](../Page/老友狗狗_\(電視劇\).md "wikilink")》、《[情越雙白線](../Page/情越雙白線.md "wikilink")》、《[隔離七日情](../Page/隔離七日情.md "wikilink")》，以及具劇力的古裝劇，包括《[滙通天下](../Page/滙通天下_\(電視劇\).md "wikilink")》、《[紫禁驚雷](../Page/紫禁驚雷.md "wikilink")》、《[盛世仁傑](../Page/盛世仁傑.md "wikilink")》、《[造王者](../Page/造王者.md "wikilink")》等，當中的《滙通天下》更獲新浪『2007粵港十年網娛盛典』最佳電視劇劇本獎。而《[紫禁驚雷](../Page/紫禁驚雷.md "wikilink")》更是2011年度收視第二高的清裝劇（僅次於[萬凰之王](../Page/萬凰之王.md "wikilink")）。

[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")、[馬浚偉](../Page/馬浚偉.md "wikilink")、[鄭則士](../Page/鄭則士.md "wikilink")、[黎耀祥](../Page/黎耀祥.md "wikilink")、[陳錦鴻](../Page/陳錦鴻.md "wikilink")、[黃浩然](../Page/黃浩然.md "wikilink")、[陳展鵬](../Page/陳展鵬.md "wikilink")、[唐詩詠](../Page/唐詩詠.md "wikilink")、[郭羨妮](../Page/郭羨妮.md "wikilink")、[黃嘉樂](../Page/黃嘉樂.md "wikilink")、[李天翔](../Page/李天翔.md "wikilink")、[李詩韻](../Page/李詩韻.md "wikilink")、[姚子羚及](../Page/姚子羚.md "wikilink")[鄭嘉穎等是他的御用演員](../Page/鄭嘉穎.md "wikilink")，[鄭則士回巢後參與的電視劇更全部是由梁材遠監製](../Page/鄭則士.md "wikilink")，特別是[姚子羚和](../Page/姚子羚.md "wikilink")[龔嘉欣是御用無綫女藝員](../Page/龔嘉欣.md "wikilink")。

梁遠材所監製作品與編審如[吳肇銅](../Page/吳肇銅.md "wikilink")、[伍立光等合作較多](../Page/伍立光.md "wikilink")。

## 作品列表

### 電視劇（[無線電視](../Page/無線電視.md "wikilink")）

  - 1985年：《[種計](../Page/種計_\(電視劇\).md "wikilink")》
    《[中四丁班](../Page/中四丁班.md "wikilink")》
    《[開心女鬼](../Page/開心女鬼.md "wikilink")》
  - 1986年：《[孖寶太子](../Page/孖寶太子.md "wikilink")》
    《[城市故事](../Page/城市故事.md "wikilink")》
  - 1988年：《[誓不低頭](../Page/誓不低頭_\(無綫電視劇\).md "wikilink")》
    《[不再少年時](../Page/不再少年時.md "wikilink")》
  - 1989年：《[吉星報喜](../Page/吉星報喜.md "wikilink")》
    《[相愛又如何](../Page/相愛又如何.md "wikilink")》
  - 1990年：《[成功路上](../Page/成功路上.md "wikilink")》
    《[回到未嫁時](../Page/回到未嫁時.md "wikilink")》
    《[走路新郎哥](../Page/走路新郎哥.md "wikilink")》
  - 1994年：《[方世玉與乾隆皇](../Page/方世玉與乾隆皇.md "wikilink")》
  - 2000年：《[大囍之家](../Page/大囍之家.md "wikilink")》
  - 2006年：《[飛短留長父子兵](../Page/飛短留長父子兵.md "wikilink")》
    《[滙通天下](../Page/滙通天下_\(電視劇\).md "wikilink")》
  - 2007年：《[寫意人生](../Page/寫意人生.md "wikilink")》
    《[舞動全城](../Page/舞動全城.md "wikilink")》
  - 2008年：《[金石良緣](../Page/金石良緣.md "wikilink")》
  - 2009年：《[老友狗狗](../Page/老友狗狗_\(電視劇\).md "wikilink")》
  - 2010年：《[蒲松齡](../Page/蒲松齡_\(電視劇\).md "wikilink")》
    《[情越雙白線](../Page/情越雙白線.md "wikilink")》
  - 2011年：《[隔離七日情](../Page/隔離七日情.md "wikilink")》
    《[紫禁驚雷](../Page/紫禁驚雷.md "wikilink")》
    《[蓋世孖寶](../Page/蓋世孖寶.md "wikilink")》
  - 2012年：《[盛世仁傑](../Page/盛世仁傑.md "wikilink")》
    《[造王者](../Page/造王者_\(電視劇\).md "wikilink")》
  - 2013年：《[初五啟市錄](../Page/初五啟市錄.md "wikilink")》
    《[情越海岸線](../Page/情越海岸線.md "wikilink")》
  - 2014年：《[守業者](../Page/守業者.md "wikilink")》
  - 2015年：《[張保仔](../Page/張保仔_\(2014年電視劇\).md "wikilink")》
  - 2016年：《[火線下的江湖大佬](../Page/火線下的江湖大佬.md "wikilink")》
    《[為食神探](../Page/為食神探.md "wikilink")》
  - 2017年：《[我瞞結婚了](../Page/我瞞結婚了.md "wikilink")》
  - 2018年：《[果欄中的江湖大嫂](../Page/果欄中的江湖大嫂.md "wikilink")》
  - 未播映：《[街坊財爺](../Page/街坊財爺.md "wikilink")》
  - 拍攝中：《[大醬園](../Page/大醬園.md "wikilink")》

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 1991年：《[再造繁榮](../Page/再造繁榮.md "wikilink")》
    《[豪門](../Page/豪門_\(電視劇\).md "wikilink")》

### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1994年：[虎膽虹威](../Page/虎膽虹威.md "wikilink")
  - 1995年：[末路危情](../Page/末路危情.md "wikilink")、[功夫特警](../Page/功夫特警.md "wikilink")、[都市風暴](../Page/都市風暴.md "wikilink")
  - 1996年：[孽海狂花](../Page/孽海狂花.md "wikilink")、[黑鳥](../Page/黑鳥.md "wikilink")、[飛虎雄風](../Page/飛虎雄風.md "wikilink")、[歲月情真](../Page/歲月情真.md "wikilink")、[的士810
    III](../Page/的士810_III.md "wikilink")
  - 1997年：[賊至尊](../Page/賊至尊.md "wikilink")
  - 2003年：[妒火線](../Page/妒火線.md "wikilink")
  - 2004年：[親子大綁架](../Page/親子大綁架.md "wikilink")、[冥約](../Page/冥約.md "wikilink")、[歷劫驚濤](../Page/歷劫驚濤.md "wikilink")、[伴我驕陽](../Page/伴我驕陽.md "wikilink")

## 常用主要演員

<small>只計在任監製時期（1980年代至今），含參與客串</small>

  - 11部：[李天翔](../Page/李天翔.md "wikilink")\*
  - 9部：[張國强](../Page/張國強_\(香港演員\).md "wikilink")
  - 8部：[龔嘉欣](../Page/龔嘉欣.md "wikilink")
  - 7部：[楊明](../Page/楊明.md "wikilink") 、[許家傑](../Page/許家傑.md "wikilink")
  - 6部：[黃浩然](../Page/黃浩然.md "wikilink")
    、[黃嘉樂](../Page/黃嘉樂.md "wikilink")
    、[程可為](../Page/程可為.md "wikilink")
    、[羅樂林](../Page/羅樂林.md "wikilink")
    、[姚子羚](../Page/姚子羚.md "wikilink")
    、[麥長青](../Page/麥長青.md "wikilink")
    、[樂瞳](../Page/樂瞳.md "wikilink")\*
  - 5部：[鄭則仕](../Page/鄭則仕.md "wikilink")
    、[盧宛茵](../Page/盧宛茵.md "wikilink")
    、[樊亦敏](../Page/樊亦敏.md "wikilink")
    、[李國麟](../Page/李國麟_\(演員\).md "wikilink")
    、[朱璇](../Page/朱璇.md "wikilink")\*
    、[郭峰](../Page/郭峰_\(演員\).md "wikilink")\*
  - 4部：[苑瓊丹](../Page/苑瓊丹.md "wikilink")
    、[衛志豪](../Page/衛志豪.md "wikilink")
    、[李成昌](../Page/李成昌.md "wikilink")
    、[陳展鵬](../Page/陳展鵬.md "wikilink")
    、[岳華](../Page/岳華.md "wikilink")\*
    、[馬浚偉](../Page/馬浚偉.md "wikilink")\*
    、[郭羨妮](../Page/郭羨妮.md "wikilink")\*
    、[陳錦鴻](../Page/陳錦鴻.md "wikilink")\*
  - 3部：[岑麗香](../Page/岑麗香.md "wikilink")\*
    、[邵美琪](../Page/邵美琪.md "wikilink")
    、[陳煒](../Page/陳煒.md "wikilink")
    、[王卓淇](../Page/王卓淇.md "wikilink")
    、[唐詩詠](../Page/唐詩詠.md "wikilink")
    、[曹永廉](../Page/曹永廉.md "wikilink")
    、[鄧健泓](../Page/鄧健泓.md "wikilink")\*
  - 2部：[黃智雯](../Page/黃智雯.md "wikilink")
    、[黃翠如](../Page/黃翠如.md "wikilink")
    、[黃光亮](../Page/黃光亮.md "wikilink")
    、[洪永城](../Page/洪永城.md "wikilink")
    、[傅嘉莉](../Page/傅嘉莉.md "wikilink")
    、[馬國明](../Page/馬國明.md "wikilink")
    、[陳山聰](../Page/陳山聰.md "wikilink")
    、[蔣志光](../Page/蔣志光.md "wikilink")
    、[李施嬅](../Page/李施嬅.md "wikilink")
    、[黎耀祥](../Page/黎耀祥.md "wikilink")
    、[王子涵](../Page/王子涵.md "wikilink")
    、[吳岱融](../Page/吳岱融.md "wikilink")
    、[江嘉敏](../Page/江嘉敏.md "wikilink")
    、[何廣沛](../Page/何廣沛.md "wikilink")

<small>\*為已退出TVB</small>

## 獎項列表

  - 新浪『2007粵港十年
    網娛盛典』：最佳電視劇劇本獎（《[滙通天下](../Page/滙通天下_\(電視劇\).md "wikilink")》）

## 傳聞

有傳前[香港](../Page/香港.md "wikilink")[無綫電視監製](../Page/無綫電視.md "wikilink")[鄺業生於](../Page/鄺業生.md "wikilink")2004年離職是因戲劇分部副總監曾勵珍為保梁材遠才放棄鄺業生。根據報導，梁材遠的回應是「我一向宗旨是交到貨，有收視、保持水準及幫老闆賺錢。」\[1\]

## 参考

<references />

## 外部

[Category:梁姓](../Category/梁姓.md "wikilink")
[Category:無綫電視監製](../Category/無綫電視監製.md "wikilink")

1.  [《血薦軒轅》收視創新低，鄺業生9月底離職](http://www.mingpaoweekly.com/htm/20040414/mbd1.htm)