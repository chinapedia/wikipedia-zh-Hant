**梁章鉅**（），字閎中，又字茝林，號茝鄰，晚號退庵。祖籍[福建](../Page/福建.md "wikilink")[長樂縣](../Page/长乐县_\(唐朝\).md "wikilink")，生于[福州](../Page/福州.md "wikilink")\[1\]。清朝政治人物、学者。

## 生平

梁章鉅幼時穎悟，九歲能詩，[乾隆五十九年](../Page/乾隆.md "wikilink")（1794年）中舉，[嘉慶七年](../Page/嘉慶.md "wikilink")（1802年）[進士](../Page/進士.md "wikilink")。選[翰林院](../Page/翰林院.md "wikilink")[庶吉士](../Page/庶吉士.md "wikilink")。嘉慶十年（1805年），任禮部主事。嘉慶十二年（1807年），掌浦城浦南書院講席，次年，入福建巡撫張師誠幕府。嘉慶二十三年（1818年），經考選任軍機章京。道光二年（1822年），授[湖北](../Page/湖北.md "wikilink")[荊州知府兼荊宜施道](../Page/荊州.md "wikilink")，升淮海河務兵備道，調署江蘇按察使。[道光五年](../Page/道光.md "wikilink")（1825年），管理盤運漕糧總局，任[山東](../Page/山東.md "wikilink")[按察使](../Page/按察使.md "wikilink")。次年，調任[江蘇](../Page/江蘇.md "wikilink")[布政使](../Page/布政使.md "wikilink")。在江蘇任職八年，曾四次代理巡撫，政績斐然。道光十一年（1831年），江淮大水災，他率屬捐廉募款。同年，修復練湖牌壩，籌款興修孟瀆、得勝、澡港三河水利。道光十二年（1832年）四月，奏請回福州養病。道光十五年（1835年），奉召入京，授[甘肅布政使](../Page/甘肅.md "wikilink")。次年，升[廣西](../Page/廣西.md "wikilink")[巡撫兼署學政](../Page/巡撫.md "wikilink")。道光十八年（1838年），上疏主張重治[鴉片囤販之地](../Page/鴉片.md "wikilink")，強調“行法必自官始”，並積極配合[林則徐嚴令](../Page/林則徐.md "wikilink")[梧州](../Page/梧州.md "wikilink")、[潯州官員捉拿煙販](../Page/潯州.md "wikilink")，采取10家連保法，杜絕復種罌粟。道光二十一年（1841年），親自帶兵防守梧州，並增兵潯州、[南寧](../Page/南寧.md "wikilink")，運送大炮支援[廣州防務](../Page/廣州.md "wikilink")。曾上疏抨擊琦善在廣東“開門揖盜”，歌頌三元裏人民抗英鬥爭，第一個向朝廷提出以“收[香港為首務](../Page/香港.md "wikilink")”。同年，調任江蘇巡撫，帶兵到[上海會同](../Page/上海.md "wikilink")[江南](../Page/江南.md "wikilink")[提督](../Page/提督.md "wikilink")[陳化成部署抗英](../Page/陳化成.md "wikilink")，組織[寶山](../Page/寶山區_\(上海市\).md "wikilink")、[上海](../Page/上海.md "wikilink")、[川沙](../Page/川沙.md "wikilink")、[太倉](../Page/太倉.md "wikilink")、[南匯](../Page/南匯.md "wikilink")、[嘉定等地興辦團練](../Page/嘉定.md "wikilink")，嚴密設防，使英軍未敢妄動。同年八月，署理兩江總督兼兩淮鹽政。十一月病發，專折奏請開缺調理。先後寓居[揚州](../Page/揚州.md "wikilink")、[浦城](../Page/浦城.md "wikilink")、[溫州](../Page/溫州.md "wikilink")。道光二十九年（1849年）病逝。

## 著述

梁章鉅平生縱覽群籍，能詩善書，學識淵博，精鑒賞，富收藏，好[金石](../Page/金石.md "wikilink")。諳於掌故，善作筆記小品，50餘年著作有70餘種，為清代各省督撫中著作最多者。著有《樞垣紀略》、《楹聯叢話》十二卷、《楹聯續話》四卷、《楹聯三話》二卷、《吊谱集成》等。比較出名的有《歸田瑣記》、《浪跡叢談》、《退庵隨筆》。尤其是《浪跡叢談》保存了豐富的小說史料。

## 後代

  - 长子[梁逢辰](../Page/梁逢辰.md "wikilink")，字吉甫，1800年生。
      - 孫[梁億年](../Page/梁億年.md "wikilink")
  - 次子[梁丁辰](../Page/梁丁辰.md "wikilink")，字平仲，1811年生。
  - 三子[梁恭辰](../Page/梁恭辰.md "wikilink")，字敬叔，1814年生。
  - 四子[梁映辰](../Page/梁映辰.md "wikilink")，1817年生。
  - 五子[梁敬辰](../Page/梁敬辰.md "wikilink")，1821年生。

有四个女儿梁兰省、梁兰台、梁兰芳、梁兰衡。

有曾孫[梁鴻志](../Page/梁鴻志.md "wikilink")，為近代中國的政治家。

## 參考文獻

  - 《清史列傳》，卷38，605-608
  - [林則徐](../Page/林則徐.md "wikilink")，《誥授資政大夫兵部侍郎都察院右副都御史江蘇巡撫梁公墓志銘》，《碑傳集補》，冊1卷14，843-855

## 外部連結

  - [梁章鉅](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%B1%E7%B3%B9%B9d)
    中研院史語所

[Category:乾隆五十九年甲寅恩科舉人](../Category/乾隆五十九年甲寅恩科舉人.md "wikilink")
[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝禮部主事](../Category/清朝禮部主事.md "wikilink")
[Category:清朝禮部員外郎](../Category/清朝禮部員外郎.md "wikilink")
[Category:清朝荊州府知府](../Category/清朝荊州府知府.md "wikilink")
[Category:清朝道員](../Category/清朝道員.md "wikilink")
[Category:清朝江蘇按察使](../Category/清朝江蘇按察使.md "wikilink")
[Category:清朝山東按察使](../Category/清朝山東按察使.md "wikilink")
[Category:清朝山東布政使](../Category/清朝山東布政使.md "wikilink")
[Category:清朝江西按察使](../Category/清朝江西按察使.md "wikilink")
[Category:清朝江蘇布政使](../Category/清朝江蘇布政使.md "wikilink")
[Category:清朝江蘇巡撫](../Category/清朝江蘇巡撫.md "wikilink")
[Category:清朝甘肅布政使](../Category/清朝甘肅布政使.md "wikilink")
[Category:清朝直隸布政使](../Category/清朝直隸布政使.md "wikilink")
[Category:清朝廣西巡撫](../Category/清朝廣西巡撫.md "wikilink")
[Category:清朝兩江總督](../Category/清朝兩江總督.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[Category:长乐人](../Category/长乐人.md "wikilink")
[Z](../Category/梁姓.md "wikilink")

1.  其先祖在清初徒居福州，自稱福州人