**梁赞**（****，俄语拉丁字母拼写：Ryazan'，英语：Ryazan）位于[俄罗斯](../Page/俄罗斯.md "wikilink")[中部联邦管区](../Page/中部联邦管区.md "wikilink")[奥卡河畔](../Page/奥卡河.md "wikilink")，是[梁赞州的行政中心](../Page/梁赞州.md "wikilink")。[拔都攻擊俄羅斯第一地方就是這裡](../Page/拔都.md "wikilink")。

## 友好城市

  - [明斯特](../Page/明斯特.md "wikilink")

  - [洛维奇](../Page/洛维奇.md "wikilink")

  - [徐州](../Page/徐州.md "wikilink")

  - [布雷敘爾](../Page/布雷敘爾.md "wikilink")

  - [開城](../Page/開城特級市.md "wikilink")

  - [克魯舍瓦茨](../Page/克魯舍瓦茨.md "wikilink")

  - [洛維奇](../Page/洛維奇.md "wikilink")

  - [新阿豐](../Page/新阿豐.md "wikilink")

  - [奧米什](../Page/奧米什.md "wikilink")

  - [馬佐夫舍地區奧斯特魯夫](../Page/馬佐夫舍地區奧斯特魯夫.md "wikilink")

## 名人

  - [伊万·巴甫洛夫](../Page/伊万·巴甫洛夫.md "wikilink")：[心理学家](../Page/心理学家.md "wikilink")，[条件反射学说的创立者](../Page/经典条件反射.md "wikilink")
  - [康斯坦丁·齐奥尔科夫斯基](../Page/康斯坦丁·齐奥尔科夫斯基.md "wikilink")：[火箭的发明者之一](../Page/火箭.md "wikilink")
  - [谢尔盖·叶赛宁](../Page/谢尔盖·叶赛宁.md "wikilink")：诗人
  - [安德雷·马尔可夫](../Page/安德雷·马尔可夫.md "wikilink")：数学家，[马尔可夫链创立者](../Page/马尔可夫链.md "wikilink")

## 相关条目

  - [梁赞大公](../Page/梁赞大公.md "wikilink")

## 參考文獻

## 外部連結

[R](../Category/梁赞州城市.md "wikilink")