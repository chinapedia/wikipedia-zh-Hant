[Liangyuchun.jpg](https://zh.wikipedia.org/wiki/File:Liangyuchun.jpg "fig:Liangyuchun.jpg")
**梁遇春**（1906年－1932年），[福建](../Page/福建.md "wikilink")[闽侯人](../Page/闽侯.md "wikilink")，中国現代[作家](../Page/作家.md "wikilink")。

1924年进入[北京大学英文系学习](../Page/北京大学.md "wikilink")。1928年秋毕业后曾到[上海](../Page/上海.md "wikilink")[暨南大学任教](../Page/暨南大学.md "wikilink")。翌年返回北京大学图书馆工作。后因染急性[猩红热](../Page/猩红热.md "wikilink")，猝然去世。

## 作品

主要為散文與翻译西方文学作品。1926年开始陆续在《语丝》、《奔流》、《骆驼草》、《现代文学》、《新月》等刊物上发表散文，后大部分收入《春醪集》和《泪与笑》。

## 评价

文学史家[唐弢在](../Page/唐弢.md "wikilink")《晦庵书话》里指出：“我喜欢遇春的文章，认为文苑里难得有像他那样的才气，像他那样的绝顶聪明，像他那样的顾盼多姿的风格。每读《春醪集》和《泪与笑》，不免为这个死去的天才惋惜。”
梁遇春的北大外文系老师[温源宁回忆说](../Page/:溫源寧.md "wikilink")，梁遇春为人谦虚，散文是一流的。\[1\]

## 书目

  - 《春醪集》，[北新书局](../Page/北新书局.md "wikilink")，1930。
  - 《泪与笑》，[开明书局](../Page/开明书局.md "wikilink")，1934。

<!-- end list -->

  - [秦賢次編](../Page/秦賢次.md "wikilink")，《梁遇春散文集》，台北：[洪範書店](../Page/洪範書店.md "wikilink")，1979。
  - 《梁遇春散文选集》，[百花文艺出版社](../Page/百花文艺出版社.md "wikilink")，1983。

## 翻译书目

  - \[英\]狄更生著，《近代论坛》（论文），上海：春潮书局，1929。
  - 《英国诗歌选》，北新书局，1931。
  - \[苏\][高尔基著](../Page/马克西姆·高尔基.md "wikilink")，《草原上》（小说），北新书局，1931。
  - \[英\][笛福著](../Page/笛福.md "wikilink")，《荡妇自传》（小说），北新書局，1931。（再版改名《摩尔。弗兰德斯》）
  - \[俄\][伽尔逊著](../Page/伽尔逊.md "wikilink")，《红花》（小说），北新书局，1931。
  - \[波兰\][康拉德著](../Page/康拉德.md "wikilink")，《吉姆爷》（小说），[商务印书馆](../Page/商务印书馆.md "wikilink")，1934。
  - \[英\][笛福著](../Page/笛福.md "wikilink")，《鲁滨孙漂流记》（小说），北新書局，1931。

[Category:中華民國散文家](../Category/中華民國散文家.md "wikilink")
[Category:中華民國翻译家](../Category/中華民國翻译家.md "wikilink")
[Category:闽侯人](../Category/闽侯人.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:梁姓](../Category/梁姓.md "wikilink")

1.  "The Late Mr. Liang Yu-ch'un 梁遇春, A Chinese Elia", Wen Yuan-ning,
    "Imperfect Understanding: Intimate Portraits of Modern Chinese
    Celebrities", Christopher Rea编 (Amherst, MA: Cambria Press, 2018),
    页53-56.