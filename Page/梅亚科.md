**桃亚科**（[学名](../Page/学名.md "wikilink")：），亦作**李亞科**或**梅亚科**，為[薔薇科下的一亚科](../Page/薔薇科.md "wikilink")，下有[桃属](../Page/桃属.md "wikilink")、[杏属](../Page/杏属.md "wikilink")、[李属](../Page/李属.md "wikilink")、[樱属](../Page/樱属.md "wikilink")、[稠李属和](../Page/稠李属.md "wikilink")[桂樱属等多个属](../Page/桂樱属.md "wikilink")。\[1\]

這一類植物的特色是果實為[核果](../Page/核果.md "wikilink")，中心有一顆被堅硬內果皮包裹的種子。重要的经济物种有[梅](../Page/梅.md "wikilink")、[樱桃](../Page/樱桃.md "wikilink")、[杏](../Page/杏.md "wikilink")、[李](../Page/李.md "wikilink")、[桃以及](../Page/桃.md "wikilink")[扁桃](../Page/扁桃.md "wikilink")。

## 属

该亚科分类存在争议，可能包括：\[2\]

  - *[Adenostoma](../Page/Adenostoma.md "wikilink")*
  - *[Amelanchier](../Page/Amelanchier.md "wikilink")*
  - *[Aria](../Page/Aria.md "wikilink")*
  - *[Aronia](../Page/Aronia.md "wikilink")*
  - *[Aruncus](../Page/Aruncus.md "wikilink")*
  - *[Chaenomeles](../Page/Chaenomeles.md "wikilink")*
  - *[Chamaebatiaria](../Page/Chamaebatiaria.md "wikilink")*
  - *[Chamaemeles](../Page/Chamaemeles.md "wikilink")*
  - *[Chamaemespilus](../Page/Chamaemespilus.md "wikilink")*
  - *[Coleogyne](../Page/Coleogyne.md "wikilink")*
  - *[Cormus](../Page/Sorbus_domestica.md "wikilink")*
  - *[Cotoneaster](../Page/Cotoneaster.md "wikilink")*
  - *[Crataegus](../Page/Crataegus.md "wikilink")*
  - *[Cydonia](../Page/Cydonia_\(genus\).md "wikilink")*
  - *[Dichotomanthes](../Page/Dichotomanthes.md "wikilink")*
  - *[Docynia](../Page/Docynia.md "wikilink")*
  - *[Docyniopsis](../Page/Docyniopsis.md "wikilink")*
  - *[Eriobotrya](../Page/Eriobotrya.md "wikilink")*
  - *[Eriolobus](../Page/Eriolobus.md "wikilink")*
  - *[Exochorda](../Page/Exochorda.md "wikilink")*
  - *[Gillenia](../Page/Gillenia.md "wikilink")*
  - *[Hesperomeles](../Page/Hesperomeles.md "wikilink")*
  - *[Heteromeles](../Page/Heteromeles.md "wikilink")*
  - *[Holodiscus](../Page/Holodiscus.md "wikilink")*
  - *[Kageneckia](../Page/Kageneckia.md "wikilink")*
  - *[Kelseya](../Page/Kelseya.md "wikilink")*
  - *[Kerria](../Page/Kerria.md "wikilink")*
  - *[Lindleya](../Page/Lindleya.md "wikilink")*
  - *[Luetkea](../Page/Luetkea.md "wikilink")*
  - *[Lyonothamnus](../Page/Lyonothamnus.md "wikilink")*
  - *[Malacomeles](../Page/Malacomeles.md "wikilink")*
  - *[Malus](../Page/Malus.md "wikilink")*
  - *[Mespilus](../Page/Mespilus.md "wikilink")*
  - *[Neillia](../Page/Neillia.md "wikilink")*
  - *[Neviusia](../Page/Neviusia.md "wikilink")*
  - *[Oemleria](../Page/Oemleria.md "wikilink")*
  - *[Osteomeles](../Page/Osteomeles.md "wikilink")*
  - *[Peraphyllum](../Page/Peraphyllum.md "wikilink")*
  - *[Petrophytum](../Page/Petrophytum.md "wikilink")*
  - *[Photinia](../Page/Photinia.md "wikilink")*
  - *[Physocarpus](../Page/Physocarpus.md "wikilink")*
  - *[Prinsepia](../Page/Prinsepia.md "wikilink")*
  - *[Prunus](../Page/Prunus.md "wikilink")*
  - *[Pseudocydonia](../Page/Pseudocydonia.md "wikilink")*
  - *[Pyracantha](../Page/Pyracantha.md "wikilink")*
  - *[Pyrus](../Page/Pyrus.md "wikilink")*
  - *[Rhaphiolepis](../Page/Rhaphiolepis.md "wikilink")*
  - *[Rhodotypos](../Page/Rhodotypos.md "wikilink")*
  - *[Sibiraea](../Page/Sibiraea.md "wikilink")*
  - *[Sorbaria](../Page/Sorbaria.md "wikilink")*
  - *[Sorbus](../Page/Sorbus.md "wikilink")*
  - *[Spiraea](../Page/Spiraea.md "wikilink")*
  - *[Spiraeanthus](../Page/Spiraeanthus.md "wikilink")*
  - [†](../Page/灭绝.md "wikilink")*[Stonebergia](../Page/Stonebergia.md "wikilink")*（早始新世）\[3\]
  - *[Stranvaesia](../Page/Stranvaesia.md "wikilink")*
  - *[Torminalis](../Page/Sorbus_torminalis.md "wikilink")*
  - *[Vauquelinia](../Page/Vauquelinia.md "wikilink")*
  - *[Xerospiraea](../Page/Xerospiraea.md "wikilink")*

## 参考文献

[\*](../Category/梅亚科.md "wikilink")
[Category:树](../Category/树.md "wikilink")

1.  [中国植物志](http://frps.eflora.cn/frps/PRUNOIDEAE)第38卷 蔷薇科 Rosaceae 4.
    李亚科 PRUNOIDEAE FOCKE
2.
3.