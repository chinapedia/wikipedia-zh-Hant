**梅吉姆路斯卡縣**（）是[克羅地亞最北部的一個縣](../Page/克羅地亞.md "wikilink")。西鄰[斯洛文尼亞](../Page/斯洛文尼亞.md "wikilink")，東鄰[匈牙利](../Page/匈牙利.md "wikilink")。地理上屬[阿爾卑斯山脈和](../Page/阿爾卑斯山脈.md "wikilink")[潘諾尼亞平原的過渡地帶](../Page/潘諾尼亞平原.md "wikilink")。面積729平方公里，人口118,426（2001年）。首府[察科韋茨](../Page/察科韋茨.md "wikilink")。

下分三市、二十二鎮。縣議會有41席。

## 參考文獻

[Category:克羅地亞行政區劃](../Category/克羅地亞行政區劃.md "wikilink")