**梅小惠**（****，），生於[香港](../Page/香港.md "wikilink")，籍貫[廣東](../Page/廣東.md "wikilink")[台山](../Page/台山.md "wikilink")，前[無綫電視女藝員](../Page/無綫電視.md "wikilink")，[無綫電視劇集監製](../Page/無綫電視.md "wikilink")[梅小青胞妹](../Page/梅小青.md "wikilink")。梅小惠早年居住在[九龍](../Page/九龍.md "wikilink")[旺角通菜街](../Page/旺角.md "wikilink")58號，畢業於香港漢文師範同學會附屬學校（1979年第28屆下午校，其胞姊梅小玲、胞兄梅尚智與梅尚仁亦分別於1974年、1976年和1979年畢業）和[聖瑪加利女書院](../Page/聖瑪加利女書院.md "wikilink")（九龍分校）。在1985年參加[健美小姐](../Page/健美小姐.md "wikilink")。其后轉投[亞洲電視](../Page/亞洲電視.md "wikilink")，曾在[梅小青監製的几部劇集中參與演出](../Page/梅小青.md "wikilink")，梅小惠的代表作有《[他來自江湖](../Page/他來自江湖.md "wikilink")》、《[皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")》等，其「傻大姐」性格與招牌「馬笑聲」，皆受到[香港觀眾的歡迎](../Page/香港.md "wikilink")。其著名金句為:
"梅小惠, 世上最美麗"。

梅小惠於2004年在無綫電視完成了《皆大歡喜》拍攝後，已開始淡出演藝界投身設計公司，但仍會在一些大型綜藝節目或清談節目中出現，如每年[歡樂滿東華梅亦擔任司儀](../Page/歡樂滿東華.md "wikilink")，也不時在一些[港產片出現](../Page/港產片.md "wikilink")。

梅在香港演藝圈中的好友包括[阮兆祥](../Page/阮兆祥.md "wikilink")、[鄧兆尊](../Page/鄧兆尊.md "wikilink")、[曾志偉等](../Page/曾志偉.md "wikilink")。

梅曾與演員[曾偉權拍拖](../Page/曾偉權.md "wikilink")，不過傳聞因為著名的[馬來西亞](../Page/馬來西亞.md "wikilink")[艺人](../Page/艺人.md "wikilink")[葉麗儀的介入而分手](../Page/葉麗儀_\(馬來西亞\).md "wikilink")。

## 音樂作品

### EP

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/是祥惠的.md" title="wikilink">是祥惠的</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/藝能動音.md" title="wikilink">藝能動音</a></p></td>
<td style="text-align: left;"><p>1994年</p></td>
<td style="text-align: left;"><ol>
<li>94世界杯瘋狂"QK"版（阮兆祥/梅小惠合唱）</li>
<li>十二萬火急熱線（阮兆祥/梅小惠合唱）</li>
<li>衣食住行玩新人（阮兆祥/梅小惠合唱）</li>
<li>我的母親（阮兆祥/梅小惠合唱）</li>
</ol></td>
</tr>
</tbody>
</table>

## 演出作品

### 電視劇

|                                                 |                                              |           |
| ----------------------------------------------- | -------------------------------------------- | --------- |
| **首播**                                          | **劇名**                                       | **角色**    |
| 1986年                                           | [滿堂紅](../Page/滿堂紅.md "wikilink")             |           |
| [通靈](../Page/通靈.md "wikilink")                  |                                              |           |
| [柔情點三八](../Page/柔情點三八.md "wikilink")            |                                              |           |
| 1987年                                           | [季節](../Page/季節_\(電視劇\).md "wikilink")       | 客串演出      |
| 1988年                                           | [鬥氣一族](../Page/鬥氣一族.md "wikilink")           | 莹 莹       |
| [狙擊神探](../Page/狙擊神探.md "wikilink")              | \-                                           |           |
| [大都會](../Page/大都會_\(電視劇\).md "wikilink")        | 梁日翠                                          |           |
| [都市方程式](../Page/都市方程式.md "wikilink")            | Gigi                                         |           |
| [阿德也瘋狂](../Page/阿德也瘋狂.md "wikilink")            | 石燕燕                                          |           |
| 1989年                                           | [吉星報喜](../Page/吉星報喜.md "wikilink")           | 跳楼女子/詐騙女子 |
| [蓋世豪俠](../Page/蓋世豪俠.md "wikilink")              | 阿　蘭                                          |           |
| [萬家傳說](../Page/萬家傳說.md "wikilink")              | 李大妹                                          |           |
| [義不容情](../Page/義不容情.md "wikilink")              | 阿　珍                                          |           |
| [無冕急先鋒](../Page/無冕急先鋒.md "wikilink")            | \-                                           |           |
| [淘氣雙子星](../Page/淘氣雙子星.md "wikilink")            | 李西西                                          |           |
| [公私三文治](../Page/公私三文治.md "wikilink")            | 張佩麗（二妹）                                      |           |
| [他来自江湖](../Page/他来自江湖.md "wikilink")            | 司徒佩芝（甩頭芝）                                    |           |
| [天涯歌女](../Page/天涯歌女_\(1989年電視劇\).md "wikilink") | 秦　莉                                          |           |
| 1990年                                           | [笑傲在明天](../Page/笑傲在明天.md "wikilink")         | 謝西西       |
| 1991年                                           | [大家族](../Page/大家族.md "wikilink")             | 蔣健兒（DoDo） |
| 1992年                                           | [武林幸運星](../Page/武林幸運星.md "wikilink")         | 田寵兒       |
| [巨人](../Page/巨人_\(1992年電視劇\).md "wikilink")     | 方潔儀                                          |           |
| [兄兄我我](../Page/兄兄我我.md "wikilink")              | 歐嘉寶                                          |           |
| 1993年                                           | [龍兄鼠弟](../Page/龍兄鼠弟_\(電視劇\).md "wikilink")   | 太　太（第30集） |
| 1994年                                           | [成日受傷的男人](../Page/成日受傷的男人.md "wikilink")     | 吳碧惠       |
| 1995年                                           | [娛樂插班生](../Page/娛樂插班生.md "wikilink")         | 陸其鐮       |
| [天降奇緣](../Page/天降奇緣.md "wikilink")              | 鄧如珠                                          |           |
| 1996年                                           | [殭屍福星](../Page/殭屍福星.md "wikilink")           | \-        |
| 1997年                                           | [當女人愛上男人](../Page/當女人愛上男人.md "wikilink")     | 楊美蘭       |
| [樂壇插班生](../Page/樂壇插班生.md "wikilink")            | 陸明霞                                          |           |
| [香港人在廣州](../Page/香港人在廣州.md "wikilink")          | 王　凰                                          |           |
| 1998年                                           | [師奶强人](../Page/師奶强人.md "wikilink")           | 慕容珠       |
| 1999年                                           | [千里姻緣兜錯圈](../Page/千里姻緣兜錯圈.md "wikilink")     | 齊學琴（琴姐）   |
| [命轉情真](../Page/命轉情真.md "wikilink")              | 方家希                                          |           |
| 2000年                                           | [夢想成真](../Page/夢想成真.md "wikilink")           | 葉綺夢       |
| 2001年                                           | [錦繡良緣](../Page/錦繡良緣.md "wikilink")           | 鄭　艷       |
| [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")    | 石　美（猪頭美）                                     |           |
| 2003年-2004年                                     | [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink") | 石　美       |
| 2005年                                           | [聊斋](../Page/聊斋_\(2005年电视剧\).md "wikilink")  | 朱夫人       |

### 其他演出（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [歡樂今宵](../Page/歡樂今宵.md "wikilink")（1987-1994）（曾以怪馬笑聲出位演出）
  - [點解咁好笑](../Page/點解咁好笑.md "wikilink")（1988）
  - 1992年度[香港小姐競選決賽](../Page/香港小姐競選.md "wikilink")（1992）
  - 新春萬事joke（1994）（[阮兆祥](../Page/阮兆祥.md "wikilink")、[區海倫](../Page/區海倫.md "wikilink")）
  - 三峽狀元爭霸戰（1995）（[黃霑](../Page/黃霑.md "wikilink")）
  - 非常勁秋[鄭少秋](../Page/鄭少秋.md "wikilink")（1996）（[曾華倩](../Page/曾華倩.md "wikilink")）
  - 電視大贏家（1997-1998）（[汪明荃](../Page/汪明荃.md "wikilink")、[江欣燕](../Page/江欣燕.md "wikilink")）
  - [鄭秀文看鄭秀文](../Page/鄭秀文.md "wikilink")（2004）（[鄧梓峰](../Page/鄧梓峰.md "wikilink")）
  - [飛越大長針](../Page/飛越大長針.md "wikilink")（2005）飾 皇后
  - [飛越大長針](../Page/飛越大長針.md "wikilink") (2006) 飾 大妃
  - [飛越大長針](../Page/飛越大長針.md "wikilink")（2006）飾 惠大妃/大王大妃
  - [歡樂滿東華](../Page/歡樂滿東華.md "wikilink")
  - [美女廚房 (第二輯)](../Page/美女廚房_\(第二輯\).md "wikilink")（2009）（第10集演出）
  - [星星同學會](../Page/星星同學會.md "wikilink")（2009）（第26集）

### 電影

#### 1990年

  - 《[嘩鬼住正隔籬](../Page/嘩鬼住正隔籬.md "wikilink")》 飾 Yummy
  - 《[靚足100分](../Page/靚足100分.md "wikilink")》 飾 青青
  - 《[瘦虎肥龍](../Page/瘦虎肥龍.md "wikilink")》 飾
  - 《[師兄撞鬼](../Page/師兄撞鬼.md "wikilink")》 飾 Beautiful
  - 《[至尊計狀元才](../Page/至尊計狀元才.md "wikilink")》 飾 Sexy
  - 《[無敵幸運星](../Page/無敵幸運星.md "wikilink")》 飾
    主角[黃秋生的妻子](../Page/黃秋生.md "wikilink")
  - 《[摩登如來神掌](../Page/摩登如來神掌.md "wikilink")》 飾 小蠻

#### 1991年

  - 《[我老婆唔係人](../Page/我老婆唔係人.md "wikilink")》 飾

#### 1992年

  - 《[漫畫威龍](../Page/漫畫威龍.md "wikilink")》 飾 朱朱

#### 1994年

  - 《[昨夜長風](../Page/昨夜長風.md "wikilink")》 飾 徐玉圓

#### 2005年

  - 《[妃子笑](../Page/妃子笑.md "wikilink")》飾 阿史

#### 2009年

  - 《[矮仔多情](../Page/矮仔多情.md "wikilink")》飾 Mary姐

### 電台節目

  - 《[開心日報](../Page/開心日報.md "wikilink")》（2003-2005）（[香港電台第一台](../Page/香港電台第一台.md "wikilink")，當主持人[車淑梅放假時任客席主持](../Page/車淑梅.md "wikilink")）
  - 《月光光呵呵呵》（2015年9月4日-2016年7月1日）（[雷霆881](../Page/雷霆881.md "wikilink")，與[梁泰來及](../Page/梁泰來.md "wikilink")[鄔家麟主持](../Page/鄔家麟.md "wikilink")）

## 參考

[Category:1966年出生](../Category/1966年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[Siu](../Category/梅姓.md "wikilink")