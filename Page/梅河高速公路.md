[Longchuan007.jpg](https://zh.wikipedia.org/wiki/File:Longchuan007.jpg "fig:Longchuan007.jpg")
**梅河高速公路**是[广东省连接](../Page/广东省.md "wikilink")[河源市及](../Page/河源市.md "wikilink")[梅州市一条](../Page/梅州市.md "wikilink")[高速公路](../Page/高速公路.md "wikilink")。在[中国国家高速公路网编制下](../Page/中国国家高速公路网.md "wikilink")，程江至柳城段编为G25，属[长深高速公路的一部分](../Page/长深高速公路.md "wikilink")，坭陂至龙川段编为G78，属[汕昆高速公路的一部分](../Page/汕昆高速公路.md "wikilink")。

起于[梅州市](../Page/梅州市.md "wikilink")[梅县区](../Page/梅县区.md "wikilink")[程江镇](../Page/程江镇.md "wikilink")，与[梅州环城高速公路及](../Page/梅州环城高速公路.md "wikilink")[梅龙高速公路相接](../Page/梅龙高速公路.md "wikilink")，经[兴宁市](../Page/兴宁市.md "wikilink")，[五华县](../Page/五华县.md "wikilink")，[河源市](../Page/河源市.md "wikilink")[龙川县](../Page/龙川县.md "wikilink")，止于[东源县](../Page/东源县.md "wikilink")[蓝口镇五星村](../Page/蓝口镇.md "wikilink")，与[河龙高速公路相接](../Page/河龙高速公路.md "wikilink")。全长118.410公里，批复概算48.35亿人民币。全线按山岭重丘区高速公路设计，采用双向4车道，设计行车速度为每小时80公里。

2003年9月8日开工，2005年10月30日通车。通车后广州至梅州缩短至4小时，实现广州到所有省内地级市全程高速。\[1\]

## 互通枢纽及服务设施

[Guangdong_G25.svg](https://zh.wikipedia.org/wiki/File:Guangdong_G25.svg "fig:Guangdong_G25.svg")
[Guangdong_G35.svg](https://zh.wikipedia.org/wiki/File:Guangdong_G35.svg "fig:Guangdong_G35.svg")
[Guangdong_G78.svg](https://zh.wikipedia.org/wiki/File:Guangdong_G78.svg "fig:Guangdong_G78.svg")


## 注释

[category:梅州交通](../Page/category:梅州交通.md "wikilink")
[category:河源交通](../Page/category:河源交通.md "wikilink")

[Category:广东省高速公路](../Category/广东省高速公路.md "wikilink")

1.  [梅河高速公路全线建成通车](http://gd.news.sina.com.cn/local/2005-10-31/1847638.html)