**梅田站**（）位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[北區](../Page/北區_\(大阪市\).md "wikilink")[梅田](../Page/梅田.md "wikilink")，是[阪急電鐵](../Page/阪急電鐵.md "wikilink")、[阪神電氣鐵道](../Page/阪神電氣鐵道.md "wikilink")、[大阪市營地下鐵](../Page/大阪市營地下鐵.md "wikilink")[御堂筋線的](../Page/御堂筋線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。而[日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")（JR貨物）的梅田車站已於2013年3月15日關閉\[1\]。

梅田站與[JR西日本](../Page/JR西日本.md "wikilink")[大阪站的位置相同](../Page/大阪站.md "wikilink")，事實上更是屬於同一個車站構造內。需要注意的是，梅田站主要是由[私鐵](../Page/私鐵.md "wikilink")、[地下鐵系統使用](../Page/地下鐵.md "wikilink")，而大阪站為專供[JR使用](../Page/JR.md "wikilink")。

## 車站構造

### 阪神電氣鐵道

[File:Hanshin-Dept-Umeda-Store-01.jpg|車站外觀](File:Hanshin-Dept-Umeda-Store-01.jpg%7C車站外觀)
<File:Hanshin> Umeda station1.JPG|月台

### 阪急電鐵

<File:Hankyu> Umeda station4.JPG|3F驗票口 <File:Hankyu> Umeda
station8.JPG|2F中央驗票口 <File:Hankyu> Umeda station7.JPG|2F茶屋町驗票口
<File:Hankyu> Umeda station2.JPG|月台

### 大阪市營地下鐵

<File:Subway> Umeda.jpg|驗票口
[File:Subway-midosujiline-umeda-lighting.jpg|千里中央方向月台](File:Subway-midosujiline-umeda-lighting.jpg%7C千里中央方向月台)

## 外部連結

  - [阪神電車（梅田站）](https://web.archive.org/web/20070930181413/http://rail.hanshin.co.jp/station/01_umeda/index.html)
  - [阪急電鐵（梅田站）](https://web.archive.org/web/20071014010247/http://rail.hankyu.co.jp/station/umeda/index.html)
  - [大阪市交通局（梅田站）](https://web.archive.org/web/20070522035106/http://www.kotsu.city.osaka.jp/information/station/station/kakueki/midousuji/umeda.html)

[Meda](../Category/日本鐵路車站_U.md "wikilink")
[Category:梅田](../Category/梅田.md "wikilink") [Category:北區鐵路車站
(大阪市)](../Category/北區鐵路車站_\(大阪市\).md "wikilink")
[Category:日本貨物鐵道廢站](../Category/日本貨物鐵道廢站.md "wikilink")
[Category:1928年启用的铁路车站](../Category/1928年启用的铁路车站.md "wikilink")
[Category:2013年關閉的鐵路車站](../Category/2013年關閉的鐵路車站.md "wikilink")
[Category:梅田貨物線車站](../Category/梅田貨物線車站.md "wikilink")
[Category:大阪府廢站](../Category/大阪府廢站.md "wikilink")

1.