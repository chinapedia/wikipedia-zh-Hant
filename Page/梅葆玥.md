**梅葆玥**（），是[京剧](../Page/京剧.md "wikilink")[老生女演员](../Page/老生.md "wikilink")，祖籍[江苏](../Page/江苏.md "wikilink")[泰州](../Page/泰州.md "wikilink")，1930年9月28日出生于北京。京剧大师[梅兰芳的唯一女儿](../Page/梅兰芳.md "wikilink")，家中排行第七。有一子，范梅强，1957年出生。

  - 1943年起在其母福芝芳的师妹李桂芬女士指导下开始学习京剧老生。
  - 1946年首次与其弟[梅葆玖在上海皇后大戏院登台演出](../Page/梅葆玖.md "wikilink")《[四郎探母](../Page/四郎探母.md "wikilink")》。
  - 1953年被分配到中国戏曲学校任国文教员，后又拜[王少楼为师](../Page/王少楼_\(京剧老生\).md "wikilink")。
  - 1954年被调往[中国京剧院](../Page/中国京剧院.md "wikilink")，开始了长达45年的演员生活。
  - 1955年为参加在波兰华沙举行的世界青年联欢会，中国京剧院派出一支由青年演员组成的代表团访问[北欧](../Page/北欧.md "wikilink")，她也在其中。
  - 1956年梅兰芳以文化大使的身份率中国京剧团访日，梅葆玥随行。
  - 1958年被调到梅剧团与其弟梅葆玖一起在其父梅兰芳身边工作，随父在全国各地巡回演出。
  - 1961年其父梅兰芳因心脏病突发逝世，她与其弟梅葆玖继承梅剧团的运作。
  - 1966年“[文革](../Page/文革.md "wikilink")”开始，与其弟等遭[批斗](../Page/批斗.md "wikilink")、接受“再教育”，并[下放到北京](../Page/下放.md "wikilink")[天堂河农场劳动](../Page/天堂河.md "wikilink")。
  - 1983年与其弟率领梅剧团访日。这是梅剧团第四次、也是“文革”后首次访日。
  - 1987年退休。
  - 1993年与大陆众多京剧艺术家组成京剧团赴[台湾演出](../Page/台湾.md "wikilink")，其间拜会了[陈立夫](../Page/陈立夫.md "wikilink")、[蒋纬国](../Page/蒋纬国.md "wikilink")、[张学良](../Page/张学良.md "wikilink")、[辜振甫等各界人士](../Page/辜振甫.md "wikilink")。
  - 1997年应邀参加了内地京剧团赴港庆祝[香港主權移交的演出](../Page/香港主權移交.md "wikilink")。
  - 1998年[中国大陆南方遭遇特大水灾](../Page/中国大陆.md "wikilink")，梅葆玥参加了募捐义演。
  - 2000年5月23日因乳腺癌发作，在北京逝世，享年70岁。

## 梅兰芳家系

[M梅](../Page/category:京剧演员.md "wikilink")
[M梅](../Page/category:昆曲演员.md "wikilink")
[M梅](../Page/category:泰州人.md "wikilink")

[M梅](../Category/生行演员.md "wikilink")
[M梅](../Category/罹患乳腺癌逝世者.md "wikilink")
[B](../Category/梅姓.md "wikilink")