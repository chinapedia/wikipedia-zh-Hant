**梅超風**是[金庸小說](../Page/金庸小說.md "wikilink")《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》的人物，原名梅若華，「[東邪](../Page/東邪.md "wikilink")」[黃藥師的三弟子](../Page/黃藥師.md "wikilink")、[黃蓉的師姐](../Page/黃蓉.md "wikilink")，但與同門師兄兼丈夫[陳玄風背叛了師門](../Page/陳玄風.md "wikilink")。

武功高強，指功凌厲，鞭法了得，臉色是黑黝黝的，外號「**鐵屍**」。

登場回數:(射)4,6,10-11,13-15,25-26

## 生平

她多年前盜去《[九陰真經](../Page/九陰真經.md "wikilink")》，背叛了師門。離開了[桃花島後](../Page/桃花島.md "wikilink")，修習《九陰真經》裏的武功。憑[九陰白骨爪名震江湖](../Page/九陰白骨爪.md "wikilink")，武功高強，並將之傳授[完顏康](../Page/完顏康.md "wikilink")。

在[蒙古大漠和](../Page/蒙古.md "wikilink")「[江南七怪](../Page/江南七怪.md "wikilink")」的激戰中，陳玄風殺死[張阿生但不慎被六歲的](../Page/張阿生.md "wikilink")[郭靖](../Page/郭靖.md "wikilink")（小說男主角）的匕首刺中罩門肚臍致死。故恨透[江南七怪及](../Page/江南七怪.md "wikilink")[郭靖](../Page/郭靖.md "wikilink")，欲報殺夫之仇。

後來於牛家村不敵[全真七子](../Page/全真七子.md "wikilink")，給「東邪」黃藥師出手相救。雙方酣鬥之際，「[西毒](../Page/西毒.md "wikilink")」[歐陽鋒以畢生功力向黃藥師施以偷襲](../Page/歐陽鋒.md "wikilink")，梅超風捨命為恩師擋了歐陽鋒一擊身亡。

黃藥師在她臨死時，重新收她為徒。

梅超風素來殺人如麻，卻一直對與偷取恩師經書一事而內疚，對小師妹黃蓉亦能以禮相待。

## 新版改动

[金庸在新修订的第三版中加入了](../Page/金庸.md "wikilink")[黄药师与梅超风之间的深厚情誼](../Page/黄药师.md "wikilink")，[黄药师一遍遍抄录](../Page/黄药师.md "wikilink")[欧阳脩](../Page/欧阳脩.md "wikilink")《[望江南](../Page/望江南.md "wikilink")》词“恁时相见早留心，何况到如今”
，藉以抒发其对梅超风的喜爱及情同父女之意。梅超风舍命为[黄药师挡了](../Page/黄药师.md "wikilink")[欧阳锋用上十成功力的](../Page/欧阳锋.md "wikilink")[蛤蟆功](../Page/蛤蟆功.md "wikilink")、使得[黄药师因梅超风死去而伤心提供了一个合理的解释](../Page/黄药师.md "wikilink")。\[1\]\[2\]

## 武功

  - 彈指神通

右手中指曲起，扣在拇指之下彈出，手法精微奧妙，射程甚遠。

  - 摧心掌

雖名「摧心」，但中者五臟六腑皆會被震爛，骨骼卻不折斷

  - 白蟒鞭法

使用極長的白蟒鞭，如靈蛇出洞，伸縮自如，靈動之極。

  - 九陰白骨爪

五指發勁，出爪後摧敵首腦，受此功夫死亡者頭頂五個指洞，無堅不摧，無固不破，摧毀大岩，如穿腐土，出爪時爪心有強大的吸力可隔空取物或吸取他人功力，爪指有強大的透勁可隔空傷人。

## 影视形象

### 电视剧

  - 1976[张敏婷香港佳视电视剧](../Page/张敏婷.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1983[黄文慧香港无线电视剧](../Page/黄文慧.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1988[陈丽华台湾中视电视剧](../Page/陈丽华.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1994[麦翠娴香港无线电视剧](../Page/麦翠娴.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2003[杨丽萍中國电视剧](../Page/杨丽萍.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2008[孔维中國电视剧](../Page/孔维.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2014[杨蓉中國电视剧](../Page/杨蓉.md "wikilink")《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 2016[米露中國电视剧](../Page/米露.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》

### 电影

  - 1958[陈立品香港峨嵋电影](../Page/陈立品.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1977[马海伦香港邵氏电影](../Page/马海伦.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》

## 註腳

<div class="references-small">

</div>

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 参考书目

  - 鄭吟韜，《一代文宗歐陽修》p.59－60
  - <http://news.xinmin.cn/rollnews/2011/08/06/11642304.html>
  - 《金庸作品集：[射雕英雄传](../Page/射雕英雄传.md "wikilink")》，[金庸](../Page/金庸.md "wikilink")，2009年9月1日，[广州出版社](../Page/广州出版社.md "wikilink")，ISBN：9787546201603。
  - 《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》修订版，[金庸](../Page/金庸.md "wikilink")，2008年3月1日，[广州出版社](../Page/广州出版社.md "wikilink")，ISBN：9787806553312。

[M梅](../Page/category:金庸筆下虛構角色.md "wikilink")

[en:List of The Legend of the Condor Heroes
characters\#Peach_Blossom_Island](../Page/en:List_of_The_Legend_of_the_Condor_Heroes_characters#Peach_Blossom_Island.md "wikilink")

[M梅](../Category/射鵰英雄傳角色.md "wikilink")

1.
2.