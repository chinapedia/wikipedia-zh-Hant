**條紋長鱸**（[學名](../Page/學名.md "wikilink")：**''Liopropoma
susumi**''），又稱**條紋高鱸**、**孫氏長鱸**，俗名為郭仔，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[鮨科的一個](../Page/鮨科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[萊恩群島](../Page/萊恩群島.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[日本南部](../Page/日本.md "wikilink")、[新喀里多尼亞等海域](../Page/新喀里多尼亞.md "wikilink")。

## 深度

水深5至60公尺。

## 特徵

本魚頭背部斜直；眶間區平坦或略凹陷。吻較長而略突出。上頜骨末端延伸至眼中部之下方；上下頜具齒。前鰓蓋骨下緣無前向棘。體被細小櫛鱗，上頜骨具鱗；側線鱗孔數44至49枚。體色[暗紅色](../Page/暗紅色.md "wikilink")，體側具有數條[黑色縱帶](../Page/黑色.md "wikilink")，各鰭之顏色略為[紅色](../Page/紅色.md "wikilink")。具2枚背鰭，有硬棘8枚，軟條12枚；臀鰭硬棘3枚，軟條8枚；腹鰭腹位，末端不及肛門開口；胸鰭短於頭部長；尾鰭內凹形，尖端圓形。體長可達15公分。

## 生態

多棲息在珊瑚礁區內之洞穴或縫隙內，非常隱密，夜間才外出覓食。肉食性，以[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，體型小，肉不多，但肉質細嫩，適宜油炸或煎食，味道頗佳。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[susumi](../Category/長鱸屬.md "wikilink")