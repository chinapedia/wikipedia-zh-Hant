[信号处理领域中](../Page/信号处理.md "wikilink")，**梳状滤波器**（，又稱**梳形濾波器**）使一个[信号与它的延时信号叠加](../Page/信号.md "wikilink")，从而产生相位抵消。梳状滤波器的[频率响应由一系列规律分布的峰组成](../Page/频率响应.md "wikilink")，看上去与[梳子类似](../Page/梳子.md "wikilink")。

[离散时间系统中的梳状滤波器满足下式](../Page/离散时间系统.md "wikilink")：

\[y[n] = ax[n] + bx[n - \tau] + cy[n - \tau]\]

其中*τ*
是一个表示延时的常量。梳状滤波器也可以在[连续时间系统上实现](../Page/连续时间系统.md "wikilink")。它的频率响应为：

\[H(\omega) = \frac{a + be^{-i \omega \tau}} {1 - ce^{-i \omega \tau}}\]

频谱中的梳状峰值是因为系统周期的不连续性（[极点](../Page/极点.md "wikilink")），极点的位置满足：

\[\cos(\omega \tau) = \frac{1+c^2}{2c}\]

## 应用

[NTSC制式的电视信号解码器中以硬件](../Page/NTSC.md "wikilink")（偶尔也有软件）实现了二维和三维梳状滤波器，以减轻杂色讯（[dot
crawl](../Page/:en:dot_crawl.md "wikilink")）等效应。梳状滤波器也被应用在地面无线通信系统中。梳状滤波器可以产生[回声效应](../Page/回声.md "wikilink")，若将延时设置为几个[毫秒](../Page/毫秒.md "wikilink")，则将此滤波器加在音频信号上，就可以作为圆柱形谐振腔的模型。因为这种[谐振腔能够放大与它宽度相关的](../Page/谐振腔.md "wikilink")[驻波对应的频率分量](../Page/驻波.md "wikilink")。

## 频率响应的计算

梳状滤波器是一个[线性时不变系统](../Page/线性时不变系统.md "wikilink")，因此[指数函数是这一系统的](../Page/指数函数.md "wikilink")[特征函数](../Page/特征函数.md "wikilink")。所以当输入信号*x*(*n*)
为指数函数的形式时

\[x(n) = e^{i \omega n}\]

输出信号*y(n)* 的形式为：

\[y(n) = H(\omega) e^{i \omega n}\]

代入上文中梳妆滤波器频响满足的条件式，可得：

\[H(\omega)e^{i \omega n} = ae^{i \omega n} + be^{i \omega (n-\tau)} + cH(\omega)e^{i \omega (n-\tau)}\]

\[H(\omega)e^{i \omega n} = ae^{i \omega n} + be^{-i \omega \tau}e^{i \omega n} + cH(\omega)e^{-i \omega \tau}e^{i \omega n}\]

由于指数函数非零，因此有：

\[H(\omega) = a + be^{-i \omega \tau} + cH(\omega)e^{-i \omega \tau}\]

解出\(H(\omega)\) 可得：

\[H(\omega) = \frac{a + be^{-i \omega \tau}} {1 - ce^{-i \omega \tau}}\]

[Category:线性滤波器](../Category/线性滤波器.md "wikilink")