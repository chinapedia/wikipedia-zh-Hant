**棕榈目**是[单子叶植物](../Page/单子叶植物.md "wikilink")[鸭跖草类分支下的一个](../Page/鸭跖草类.md "wikilink")[目](../Page/目.md "wikilink")。本目曾经只有一[科](../Page/科.md "wikilink")，即[棕榈科](../Page/棕榈科.md "wikilink")。2016年发表的[APG
IV
分类法将曾经直接置于鸭跖草类分支下的](../Page/APG_IV_分类法.md "wikilink")[多须草科放入本目](../Page/多须草科.md "wikilink")，基于一些研究反映多须草科为[棕榈科的](../Page/棕榈科.md "wikilink")[姐妹群](../Page/姐妹群.md "wikilink")。\[1\]

## 分类

### 支序分类学

棕榈目属于[被子植物](../Page/被子植物.md "wikilink")，是[单子叶植物](../Page/单子叶植物.md "wikilink")[鸭跖草类的分支之一](../Page/鸭跖草类.md "wikilink")，与[禾本目](../Page/禾本目.md "wikilink")、[姜目及](../Page/姜目.md "wikilink")[鸭跖草目的亲缘关系最近](../Page/鸭跖草目.md "wikilink")。

本目与

## 参考文献

[category:植物学](../Page/category:植物学.md "wikilink")
[\*](../Page/category:棕榈目.md "wikilink")

[Category:生物分类学](../Category/生物分类学.md "wikilink")

1.