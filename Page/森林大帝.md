《**森林大帝**》（原題：，又译《**小白狮**》、《**小白獅王**》、《**小獅王**》）原本是[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")[手塚治虫在](../Page/手塚治虫.md "wikilink")1950年至1954年創作的[漫畫作品](../Page/日本漫畫.md "wikilink")。\[1\]中文版是由[臺灣東販代理發售](../Page/臺灣東販.md "wikilink")，譯名則直譯為《森林大帝》。\[2\]

[富士電視台在](../Page/富士電視台.md "wikilink")1965年根據[手塚治虫原作漫畫將其改編](../Page/手塚治虫.md "wikilink")，首次[動畫化](../Page/動畫.md "wikilink")，於1965年10月6日～1966年9月28日期間播出，成為首部日本製作的彩色[電視動畫](../Page/電視動畫.md "wikilink")，全52集。之後分別於1966年、1989年、1997年以及2009年四度動畫化，另有配合周邊商品與活動所製作的多部短片版本。

自1960年代以来，动画版在世界各地广为流行，特别是在[澳大利亚](../Page/澳大利亚.md "wikilink")、[美国](../Page/美国.md "wikilink")、[欧洲](../Page/欧洲.md "wikilink")，乃至[中东](../Page/中东.md "wikilink")，尤其是[阿拉伯观众](../Page/阿拉伯.md "wikilink")。由[NBC電視網購入播映的](../Page/NBC.md "wikilink")[美國版本於](../Page/美國.md "wikilink")1974年引入香港時，譯名為《小白獅》。[台灣在](../Page/台灣.md "wikilink")1976年於[台視播出時譯名為](../Page/台視.md "wikilink")《[小獅王](../Page/小獅王.md "wikilink")》（1966年的續篇則為《[大獅王](../Page/大獅王.md "wikilink")》），而動畫在台灣首播前時期與近年發行的授權版DVD譯名則作《小白獅王》。本片直至1980年代初期，才引入[中国大陸播出](../Page/中国大陸.md "wikilink")。

## 作品年表

  - 1950年 - 本作於《漫画少年》雜誌開始連載。
  - 1965年 -
    10月，由-{虫}-製作的彩色電視動畫《[小獅王](../Page/小獅王.md "wikilink")》於[富士電視台開始播出](../Page/富士電視台.md "wikilink")，並以《Kimba
    the White Lion》片名自美國開始，在世界各國放映。
  - 1966年 -
    獲頒第4屆電視會賞特別賞。厚生省中央兒童文化財部会年間優秀電視節目第一名。厚生大臣児童福祉文化賞受賞。電影版於東寶院線上映。冨田勲作曲的「交響詩
    森林大帝」（，石丸寛指揮，日本愛樂交響樂團演奏）LP唱片發售
    (成為非原聲帶方式，將動畫音樂重新編曲的交響詩專輯濫觴）。故事相當於原作後半的動畫版《[大獅王](../Page/大獅王.md "wikilink")》（）於[富士電視台開始播出](../Page/富士電視台.md "wikilink")。
  - 1967年 -
    電影版獲頒[威尼斯影展的聖馬可銀獅子獎](../Page/威尼斯影展.md "wikilink")。《大獅王》第14集「咆哮的冰河」（）獲頒第6屆日本電視影片技術賞。
  - 1970年 - 以往多次刊行中斷的原作漫畫單行本，於新書版的小学館版「手塚治虫全集」中首度刊行完結篇。
  - 1989年 -
    2月，[手塚治虫去世](../Page/手塚治虫.md "wikilink")。10月，由手塚所拍攝的新作電視動畫版《森林大帝》於[東京電視台播映](../Page/東京電視台.md "wikilink")。
  - 1991年 - 1月，[OVA](../Page/OVA.md "wikilink")《動畫交響詩 森林大帝》（）發售。
  - 1994年 -
    [迪士尼動畫電影](../Page/迪士尼.md "wikilink")《[獅子王](../Page/獅子王.md "wikilink")》上映。因為與本作有諸多酷似之處而成為話題。
  - 1997年 -
    手塚製作的新作動畫電影版《森林大帝》於[松竹院線上映](../Page/松竹.md "wikilink")。在《大獅王》中與原作不同的結局，在此版中以沿襲原作的方式重新製作。
  - 2009年 - 9月，富士電視台開台50周年企畫的單元特別篇動畫《森林大帝 －勇氣能改變未來－》（）於富士電視台播出。

## 相關條目

  - [小獅王](../Page/小獅王.md "wikilink")
  - [大獅王](../Page/大獅王.md "wikilink")

## 參考來源

## 外部链接

  - [手塚治虫官網内作品介紹頁](http://tezukaosamu.net/jp/manga/186.html)

  -
[Category:手塚治虫作品](../Category/手塚治虫作品.md "wikilink")
[Category:改编成电影的日本漫画](../Category/改编成电影的日本漫画.md "wikilink")
[Category:月刊Comic NORA](../Category/月刊Comic_NORA.md "wikilink")
[Category:獅主角故事](../Category/獅主角故事.md "wikilink")
[Category:非洲背景作品](../Category/非洲背景作品.md "wikilink")
[Category:富士電視台動畫](../Category/富士電視台動畫.md "wikilink")
[Category:1989年日本電視動畫](../Category/1989年日本電視動畫.md "wikilink")
[Category:東京電視網動畫](../Category/東京電視網動畫.md "wikilink")
[Category:2009年日本電視動畫](../Category/2009年日本電視動畫.md "wikilink")
[Category:1966年日本劇場動畫](../Category/1966年日本劇場動畫.md "wikilink")
[Category:1967年日本劇場動畫](../Category/1967年日本劇場動畫.md "wikilink")
[Category:台視外購動畫](../Category/台視外購動畫.md "wikilink")
[Category:虚构狮子](../Category/虚构狮子.md "wikilink")
[Category:東森電視外購動畫](../Category/東森電視外購動畫.md "wikilink")
[Category:2009年电视电影](../Category/2009年电视电影.md "wikilink")

1.  [Jungle Taitei
    (manga)](http://www.animenewsnetwork.com/encyclopedia/manga.php?id=1342)[Anime
    News Network](../Page/Anime_News_Network.md "wikilink")
2.  [森林大帝3(完)](http://m.sanmin.com.tw/Product/Index/000348233)三民網路書店