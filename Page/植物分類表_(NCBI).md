NCBI是[美国国家生物技术信息中心](../Page/美国国家生物技术信息中心.md "wikilink")，其分类方法一般吸收了最新的科学进展，能被全世界的生物界所接受。

本列表僅列出[有胚植物](../Page/有胚植物.md "wikilink")（Embryophyta）部分，所有分類單元均分至科。以下所有有胚植物亦俱屬於[鏈形植物](../Page/鏈形植物.md "wikilink")（Streptophytina），格式方便起見另起段落。绿色植物的其他部分参见[藻类分类表](../Page/藻类分类表.md "wikilink")。

## [角苔植物門](../Page/角苔植物門.md "wikilink")(Anthocerotophyta)

### [角苔綱](../Page/角苔綱.md "wikilink")（Anthocerotopsida）

#### [角苔亚纲](../Page/角苔亚纲.md "wikilink")（Anthocerotidae）

  - [角苔目](../Page/角苔目.md "wikilink")（Anthocerotales）
      - [角苔科](../Page/角苔科.md "wikilink")（Anthocerotaceae）

#### [树角苔亚纲](../Page/树角苔亚纲.md "wikilink")（Dendrocerotidae）

  - [树角苔目](../Page/树角苔目.md "wikilink")（Dendrocerotales）
      - [树角苔科](../Page/树角苔科.md "wikilink")（Dendrocerotaceae）
  - [目](../Page/目.md "wikilink")（Phymatocerotales）
      - [科](../Page/科.md "wikilink")（Phymatocerotaceae）

#### [短角苔亚纲](../Page/短角苔亚纲.md "wikilink")（Notothylidae）

  - [短角苔目](../Page/短角苔目.md "wikilink")（Notothyladales）
      - [短角苔科](../Page/短角苔科.md "wikilink")（Notothyladaceae）

## [苔蘚植物門](../Page/苔蘚植物門.md "wikilink")(Bryophyta)

### [黑真藓纲](../Page/黑真藓纲.md "wikilink")(Andreaeobryopsida)

  - [黑真藓目](../Page/黑真藓目.md "wikilink")（Andreaeobryales）
      - [黑真藓科](../Page/黑真藓科.md "wikilink")（Andreaeobryaceae）

### [黑蘚綱](../Page/黑蘚綱.md "wikilink")(Andreaeopsida)

  - [黑蘚目](../Page/黑蘚目.md "wikilink")（Andreaeales）
      - [黑蘚科](../Page/黑蘚科.md "wikilink")（Andreaeaceae）

### [真蘚植物綱](../Page/真蘚植物綱.md "wikilink")(Bryopsida)

#### [真蘚亞綱](../Page/真蘚亞綱.md "wikilink")(Bryidae)

  - [真蘚目](../Page/真蘚目.md "wikilink")（Bryales）
      - [皱蒴藓科](../Page/皱蒴藓科.md "wikilink")（Aulacomniaceae）
      - [珠藓科](../Page/珠藓科.md "wikilink")（Bartramiaceae）
      - [真蘚科](../Page/真蘚科.md "wikilink")（Bryaceae）
      - [科](../Page/科.md "wikilink")（Leptostomataceae）
      - [提灯藓科](../Page/提灯藓科.md "wikilink")（Mniaceae）
      - [科](../Page/科.md "wikilink")（Orthodontiaceae）
      - [科](../Page/科.md "wikilink")（Phyllodrepaniaceae）
  - [虎尾藓目](../Page/虎尾藓目.md "wikilink")（Hedwigiales）
      - [虎尾藓科](../Page/虎尾藓科.md "wikilink")（Hedwigiaceae）
      - [科](../Page/科.md "wikilink")（Helicophyllaceae）
      - [科](../Page/科.md "wikilink")（Rhacocarpaceae）
  - [油藓目](../Page/油藓目.md "wikilink")（Hookeriales）
      - [科](../Page/科.md "wikilink")（Adelotheciaceae）
      - [雉尾藓科](../Page/雉尾藓科.md "wikilink")（Daltoniaceae）
      - [科](../Page/科.md "wikilink")（Garovagliaceae）
      - [油藓科](../Page/油藓科.md "wikilink")（Hookeriaceae）
      - [孔雀藓科](../Page/孔雀藓科.md "wikilink")（Hypopterygiaceae）
      - [白藓科](../Page/白藓科.md "wikilink")（Leucomiaceae）
      - [科](../Page/科.md "wikilink")（Pilotrichaceae）
      - [棱蒴藓科](../Page/棱蒴藓科.md "wikilink")（Ptychomniaceae）
  - [灰藓目](../Page/灰藓目.md "wikilink")（Hypnales）
      - [柳叶藓科](../Page/柳叶藓科.md "wikilink")（Amblystegiaceae）
      - [牛舌藓科](../Page/牛舌藓科.md "wikilink")（Anomodontaceae）
      - [青藓科](../Page/青藓科.md "wikilink")（Brachytheciaceae）
      - [科](../Page/科.md "wikilink")（Calliergonaceae）
      - [科](../Page/科.md "wikilink")（Campyliaceae）
      - [科](../Page/科.md "wikilink")（Catagoniaceae）
      - [万年藓科](../Page/万年藓科.md "wikilink")（Climaciaceae）
      - [隐蒴藓科](../Page/隐蒴藓科.md "wikilink")（Cryphaeaceae）
      - [科](../Page/科.md "wikilink")（Echinodiaceae）
      - [绢藓科](../Page/绢藓科.md "wikilink")（Entodontaceae）
      - [碎米藓科](../Page/碎米藓科.md "wikilink")（Fabroniaceae）
      - [水藓科](../Page/水藓科.md "wikilink")（Fontinalaceae）
      - [科](../Page/科.md "wikilink")（Helodiaceae）
      - [塔藓科](../Page/塔藓科.md "wikilink")（Hylocomiaceae）
      - [灰藓科](../Page/灰藓科.md "wikilink")（Hypnaceae）
      - [船叶藓科](../Page/船叶藓科.md "wikilink")（Lembophyllaceae）
      - [薄齿藓科](../Page/薄齿藓科.md "wikilink")（Leptodontaceae）
      - [科](../Page/科.md "wikilink")（Lepyrodontaceae）
      - [薄罗藓科](../Page/薄罗藓科.md "wikilink")（Leskeaceae）
      - [白齿藓科](../Page/白齿藓科.md "wikilink")（Leucodontaceae）
      - [蔓藓科](../Page/蔓藓科.md "wikilink")（Meteoriaceae）
      - [科](../Page/科.md "wikilink")（Myriniaceae）
      - [金毛藓科](../Page/金毛藓科.md "wikilink")（Myuriaceae）
      - [平藓科](../Page/平藓科.md "wikilink")（Neckeraceae）
      - [科](../Page/科.md "wikilink")（Orthorrhynchiaceae）
      - [带藓科](../Page/带藓科.md "wikilink")（Phyllogoniaceae）
      - [棉藓科](../Page/棉藓科.md "wikilink")（Plagiotheciaceae）
      - [科](../Page/科.md "wikilink")（Pleuroziopsidaceae）
      - [毛藓科](../Page/毛藓科.md "wikilink")（Prionodontaceae）
      - [科](../Page/科.md "wikilink")（Pterigynandraceae）
      - [蕨藓科](../Page/蕨藓科.md "wikilink")（Pterobryaceae）
      - [科](../Page/科.md "wikilink")（Regmatodontaceae）
      - [垂枝藓科](../Page/垂枝藓科.md "wikilink")（Rhytidiaceae）
      - [科](../Page/科.md "wikilink")（Rigodiaceae）
      - [科](../Page/科.md "wikilink")（Rutenbergiaceae）
      - [锦藓科](../Page/锦藓科.md "wikilink")（Sematophyllaceae）
      - [硬叶藓科](../Page/硬叶藓科.md "wikilink")（Stereophyllaceae）
      - [刺果藓科](../Page/刺果藓科.md "wikilink")（Symphyodontaceae）
      - [鳞藓科](../Page/鳞藓科.md "wikilink")（Theliaceae）
      - [羽藓科](../Page/羽藓科.md "wikilink")（Thuidiaceae）
      - [科](../Page/科.md "wikilink")（Trachylomataceae）
  - [木灵藓目](../Page/木灵藓目.md "wikilink")（Orthotrichales）
      - [木灵藓科](../Page/木灵藓科.md "wikilink")（Orthotrichaceae）
  - [桧藓目](../Page/桧藓目.md "wikilink")（Rhizogoniales）
      - [科](../Page/科.md "wikilink")（Calomniaceae）
      - [科](../Page/科.md "wikilink")（Cyrtopodaceae）
      - [树灰藓科](../Page/树灰藓科.md "wikilink")（Hypnodendraceae）
      - [科](../Page/科.md "wikilink")（Mitteniaceae）
      - [科](../Page/科.md "wikilink")（Pterobryellaceae）
      - [卷柏藓科](../Page/卷柏藓科.md "wikilink")（Racopilaceae）
      - [桧藓科](../Page/桧藓科.md "wikilink")（Rhizogoniaceae）
      - [木毛藓科](../Page/木毛藓科.md "wikilink")（Spiridentaceae）
  - [壶藓目](../Page/壶藓目.md "wikilink")（Splachnales）
      - [科](../Page/科.md "wikilink")（Catoscopiaceae）
      - [寒藓科](../Page/寒藓科.md "wikilink")（Meesiaceae）
      - [壶藓科](../Page/壶藓科.md "wikilink")（Splachnaceae）

#### [曲尾藓亚纲](../Page/曲尾藓亚纲.md "wikilink")(Dicranidae)

  - [无轴藓目](../Page/无轴藓目.md "wikilink")（Archidiales）
      - [无轴藓科](../Page/无轴藓科.md "wikilink")（Archidiaceae）
  - [曲尾藓目](../Page/曲尾藓目.md "wikilink")（Dicranales）目
      - [科](../Page/科.md "wikilink")（Bruchiaceae）
      - [虾藓科](../Page/虾藓科.md "wikilink")（Bryoxiphiaceae）
      - [科](../Page/科.md "wikilink")（Dicnemonaceae）
      - [曲尾藓科](../Page/曲尾藓科.md "wikilink")（Dicranaceae）
      - [牛毛藓科](../Page/牛毛藓科.md "wikilink")（Ditrichaceae）
      - [树生藓科](../Page/树生藓科.md "wikilink")（Erpodiaceae）
      - [科](../Page/科.md "wikilink")（Eustichiaceae）
      - [凤尾藓科](../Page/凤尾藓科.md "wikilink")（Fissidentaceae）
      - [白发藓科](../Page/白发藓科.md "wikilink")（Leucobryaceae）
      - [科](../Page/科.md "wikilink")（Nanobryaceae）
      - [科](../Page/科.md "wikilink")（Rhabdoweisiaceae）
      - [刺藓科](../Page/刺藓科.md "wikilink")（Rhachitheciaceae）
      - [光藓科](../Page/光藓科.md "wikilink")（Schistostegaceae）
  - [紫萼藓目](../Page/紫萼藓目.md "wikilink")（Grimmiales）
      - [科](../Page/科.md "wikilink")（Drummondiaceae）
      - [紫萼藓科](../Page/紫萼藓科.md "wikilink")（Grimmiaceae）
      - [缩叶藓科](../Page/缩叶藓科.md "wikilink")（Ptychomitriaceae）
      - [科](../Page/科.md "wikilink")（Scouleriaceae）
  - [丛藓目](../Page/丛藓目.md "wikilink")（Pottiales）
      - [科](../Page/科.md "wikilink")（Bryobartramiaceae）
      - [花叶藓科](../Page/花叶藓科.md "wikilink")（Calymperaceae）
      - [科](../Page/科.md "wikilink")（Cinclidotaceae）
      - [夭命藓科](../Page/夭命藓科.md "wikilink")（Ephemeraceae）
      - [丛藓科](../Page/丛藓科.md "wikilink")（Pottiaceae）
      - [科](../Page/科.md "wikilink")（Serpotortellaceae）
  - [细叶藓目](../Page/细叶藓目.md "wikilink")（Seligeriales）
      - [细叶藓科](../Page/细叶藓科.md "wikilink")（Seligeriaceae）
      - [科](../Page/科.md "wikilink")（Wardiaceae）

#### [短颈藓亚纲](../Page/短颈藓亚纲.md "wikilink")(Diphysciidae)

  - [短颈藓目](../Page/短颈藓目.md "wikilink")（Diphysciales）
      - [短颈藓科](../Page/短颈藓科.md "wikilink")（Diphysciaceae）

#### [葫芦藓亚纲](../Page/葫芦藓亚纲.md "wikilink")(Funariidae)

  - [大帽藓目](../Page/大帽藓目.md "wikilink")（Encalyptales）
      - [大帽藓科](../Page/大帽藓科.md "wikilink")（Encalyptaceae）
  - [葫芦藓目](../Page/葫芦藓目.md "wikilink")（Funariales）
      - [科](../Page/科.md "wikilink")（Disceliaceae）
      - [葫芦藓科](../Page/葫芦藓科.md "wikilink")（Funariaceae）
      - [科](../Page/科.md "wikilink")（Gigaspermaceae）
  - [美姿藓目](../Page/美姿藓目.md "wikilink")（Timmiales）
      - [美姿藓科](../Page/美姿藓科.md "wikilink")（Timmiaceae）

### [金发藓纲](../Page/金发藓纲.md "wikilink")(Polytrichopsida)

  - [金发藓目](../Page/金发藓目.md "wikilink")（Polytrichales）
      - [金发藓科](../Page/金发藓科.md "wikilink")（Polytrichaceae）
  - [四齿藓目](../Page/四齿藓目.md "wikilink")（Tetraphidales）
      - [烟杆藓科](../Page/烟杆藓科.md "wikilink")（Buxbaumiaceae）
      - [长台藓科](../Page/长台藓科.md "wikilink")（Oedipodiaceae）
      - [四齿藓科](../Page/四齿藓科.md "wikilink")（Tetraphidaceae）

### [泥炭蘚綱](../Page/泥炭蘚綱.md "wikilink")

  - [泥炭蘚目](../Page/泥炭蘚目.md "wikilink")（Sphagnales）
      - [科](../Page/科.md "wikilink")（Ambuchananiaceae）
      - [泥炭蘚科](../Page/泥炭蘚科.md "wikilink")（Sphagnaceae）

### [藻苔綱](../Page/藻苔綱.md "wikilink") (Takakiopsida)

  - [藻苔目](../Page/藻苔目.md "wikilink")
      - [藻苔科](../Page/藻苔科.md "wikilink")（Takakiaceae）

## [地錢植物門](../Page/地錢植物門.md "wikilink")(Marchantiophyta)

### [葉苔綱](../Page/葉苔綱.md "wikilink")(Jungermanniopsidae)

#### [葉苔亞綱](../Page/葉苔亞綱.md "wikilink")(Jungermanniidae)

  - [葉苔目](../Page/葉苔目.md "wikilink")（Jungermanniales）
      - [兔耳苔亞目](../Page/兔耳苔亞目.md "wikilink")（Antheliineae）
          - [兔耳苔科](../Page/兔耳苔科.md "wikilink")（Antheliaceae）
      - [直蒴苔亚目](../Page/直蒴苔亚目.md "wikilink")（Balantiopsidineae）
          - [直蒴苔科](../Page/直蒴苔科.md "wikilink")（Balantiopsidaceae）
      - [亞目](../Page/亞目.md "wikilink")（Brevianthineae）
          - [科](../Page/科.md "wikilink")（Brevianthaceae）
          - [科](../Page/科.md "wikilink")（Chonecoleaceae）
      - [大萼苔亚目](../Page/大萼苔亚目.md "wikilink")（Cephaloziineae）
          - [隐蒴苔科](../Page/隐蒴苔科.md "wikilink")（Adelanthaceae）
          - [大萼苔科](../Page/大萼苔科.md "wikilink")（Cephaloziaceae）
          - [拟大萼苔科](../Page/拟大萼苔科.md "wikilink")（Cephaloziellaceae）
      - [剪叶苔亚目](../Page/剪叶苔亚目.md "wikilink")（Herbertineae）
          - [剪叶苔科](../Page/剪叶苔科.md "wikilink")（Herbertaceae）
          - [拟复叉苔科](../Page/拟复叉苔科.md "wikilink")（Pseudolepicoleaceae）
          - [科](../Page/科.md "wikilink")（Trichotemnomataceae）
      - [叶苔亚目](../Page/叶苔亚目.md "wikilink")（Jungermanniineae）
          - [钱袋苔科](../Page/钱袋苔科.md "wikilink")（Gymnomitriaceae）
          - [叶苔科](../Page/叶苔科.md "wikilink")（Jungermanniaceae）
          - [科](../Page/科.md "wikilink")（Mesoptychiaceae）
          - [合叶苔科](../Page/合叶苔科.md "wikilink")（Scapaniaceae）
      - [指叶苔亚目](../Page/指叶苔亚目.md "wikilink")（Lepidoziinae）
          - [护蒴苔科](../Page/护蒴苔科.md "wikilink")（Calypogeiaceae）
          - [指叶苔科](../Page/指叶苔科.md "wikilink")（Lepidoziaceae）
      - [圆萼苔亚目](../Page/圆萼苔亚目.md "wikilink")（Lophocoleineae）
          - [顶苞苔科](../Page/顶苞苔科.md "wikilink")（Acrobolbaceae）
          - [齿萼苔科](../Page/齿萼苔科.md "wikilink")（Geocalycaceae）
          - [圆萼苔科](../Page/圆萼苔科.md "wikilink")（Gyrothyraceae）
          - [羽苔科](../Page/羽苔科.md "wikilink")（Plagiochilaceae）
  - [复叉苔目](../Page/复叉苔目.md "wikilink")（Lepicoleales）
      - [复叉苔亚目](../Page/复叉苔亚目.md "wikilink")（Lepicoleineae）
          - [复叉苔科](../Page/复叉苔科.md "wikilink")（Lepicoleaceae）
          - [南帽苔科](../Page/南帽苔科.md "wikilink")（Vetaformataceae）
      - [多囊苔亚目](../Page/多囊苔亚目.md "wikilink")（Lepidolaenineae）
          - [科](../Page/科.md "wikilink")（Jubulopsidaceae）
          - [多囊苔科](../Page/多囊苔科.md "wikilink")（Lepidolaenaceae）
          - [科](../Page/科.md "wikilink")（Neotrichocoleaceae）
          - [绒苔科](../Page/绒苔科.md "wikilink")（Trichocoleaceae）
      - [蚌葉苔亞目](../Page/蚌葉苔亞目.md "wikilink")（Perssoniellineae）
          - [岐舌苔科](../Page/岐舌苔科.md "wikilink")（Schistochilaceae）
      - [毛葉苔亞目](../Page/毛葉苔亞目.md "wikilink")（Ptilidiineae）
          - [科](../Page/科.md "wikilink")（Chaetophyllopsidaceae）
          - [须苔科](../Page/须苔科.md "wikilink")（Mastigophoraceae）
          - [毛叶苔科](../Page/毛叶苔科.md "wikilink")（Ptilidiaceae）
  - [紫叶苔目](../Page/紫叶苔目.md "wikilink")（Pleuroziales）
      - [紫叶苔科](../Page/紫叶苔科.md "wikilink")（Pleuroziaceae）
  - [光萼苔目](../Page/光萼苔目.md "wikilink")（Porellales）
      - [毛耳苔亚目](../Page/毛耳苔亚目.md "wikilink")（Jubulineae）
          - [科](../Page/科.md "wikilink")（Goebeliellaceae）
          - [毛耳苔科](../Page/毛耳苔科.md "wikilink")（Jubulaceae）
          - [细鳞苔科](../Page/细鳞苔科.md "wikilink")（Lejeuneaceae）
      - [光萼苔亚目](../Page/光萼苔亚目.md "wikilink")（Porellineae）
          - [光萼苔科](../Page/光萼苔科.md "wikilink")（Porellaceae）
  - [扁萼苔目](../Page/扁萼苔目.md "wikilink")（Radulales）
      - [扁萼苔科](../Page/扁萼苔科.md "wikilink")（Radulaceae）

#### [叉苔亚纲](../Page/叉苔亚纲.md "wikilink")(Metzgeriidae)

  - [壶苞苔目](../Page/壶苞苔目.md "wikilink")（Blasiales）
      - [壶苞苔科](../Page/壶苞苔科.md "wikilink")（Blasiaceae）
  - [小叶苔目](../Page/小叶苔目.md "wikilink")（Fossombroniales）
      - [小叶苔亚目](../Page/小叶苔亚目.md "wikilink")（Fossombroniineae）
          - [苞片苔科](../Page/苞片苔科.md "wikilink")（Allisoniaceae）
          - [小叶苔科](../Page/小叶苔科.md "wikilink")（Fossombroniaceae）
      - [溪苔亚目](../Page/溪苔亚目.md "wikilink")（Pelliineae）
          - [溪苔科](../Page/溪苔科.md "wikilink")（Pelliaceae）
  - [裸蒴苔目](../Page/裸蒴苔目.md "wikilink")（Haplomitriales）
      - [裸蒴苔科](../Page/裸蒴苔科.md "wikilink")（Haplomitriaceae）
  - [叉苔目](../Page/叉苔目.md "wikilink")（Metzgeriales）
      - [科](../Page/科.md "wikilink")（Codoniaceae）
      - [叉苔亚目](../Page/叉苔亚目.md "wikilink")（Metzgeriineae）
          - [绿片苔科](../Page/绿片苔科.md "wikilink")（Aneuraceae）
          - [叉苔科](../Page/叉苔科.md "wikilink")（Metzgeriaceae）
      - [带叶苔亚目](../Page/带叶苔亚目.md "wikilink")（Pallaviciniineae）
          - [科](../Page/科.md "wikilink")（Hymenophytaceae）
          - [南溪苔科](../Page/南溪苔科.md "wikilink")（Makinoaceae）
          - [带叶苔科](../Page/带叶苔科.md "wikilink")（Pallaviciniaceae）
  - [陶氏苔目](../Page/陶氏苔目.md "wikilink")（Treubiales）
      - [对瓣苔亚目](../Page/对瓣苔亚目.md "wikilink")（Phylothallineae）
          - [对瓣苔科](../Page/对瓣苔科.md "wikilink")（Phyllothalliaceae）
      - [陶氏苔亚目](../Page/陶氏苔亚目.md "wikilink")（Treubiineae）
          - [陶氏苔科](../Page/陶氏苔科.md "wikilink")（Treubiaceae）

### [地錢綱](../Page/地錢綱.md "wikilink")(Marchantiopsida)

#### [地錢亞綱](../Page/地錢亞綱.md "wikilink")(Marchantiidae)

  - [地錢目](../Page/地錢目.md "wikilink")（Marchantiales）
      - [花地錢亞目](../Page/花地錢亞目.md "wikilink")（Corsiniineae）
          - [花地钱科](../Page/花地钱科.md "wikilink")（Corsiniaceae）
          - [光苔科](../Page/光苔科.md "wikilink")（Cyathodiaceae）
      - [地錢亞目](../Page/地錢亞目.md "wikilink")（Marchantiineae）
          - [疣冠苔科](../Page/疣冠苔科.md "wikilink")（Aytoniaceae）
          - [星孔苔科](../Page/星孔苔科.md "wikilink")（Cleveaceae）
          - [蛇苔科](../Page/蛇苔科.md "wikilink")（Conocephalaceae）
          - [短托苔科](../Page/短托苔科.md "wikilink")（Exormothecaceae）
          - [半月苔科](../Page/半月苔科.md "wikilink")（Lunulariaceae）
          - [地錢科](../Page/地錢科.md "wikilink")（Marchantiaceae）
          - [单月苔科](../Page/单月苔科.md "wikilink")（Monosoleniaceae）
          - [魏氏苔科](../Page/魏氏苔科.md "wikilink")（Wiesnerellaceae）
      - [皮叶苔亚目](../Page/皮叶苔亚目.md "wikilink")（Targioniineae）
          - [皮叶苔科](../Page/皮叶苔科.md "wikilink")（Targioniaceae）
  - [单片苔目](../Page/单片苔目.md "wikilink")（Monocleales）
      - [单片苔科](../Page/单片苔科.md "wikilink")（Monocleaceae）
  - [钱苔目](../Page/钱苔目.md "wikilink")（Ricciales）
      - [假钱苔科](../Page/假钱苔科.md "wikilink")（Oxymitraceae）
      - [钱苔科](../Page/钱苔科.md "wikilink")（Ricciaceae）

#### [囊果苔亚纲](../Page/囊果苔亚纲.md "wikilink")(Sphaerocarpidae)

  - [囊果苔目](../Page/囊果苔目.md "wikilink")（Sphaerocarpales）
      - [纽苔科](../Page/纽苔科.md "wikilink")（Riellaceae）
      - [囊果苔科](../Page/囊果苔科.md "wikilink")（Sphaerocarpaceae）

## [石松植物門](../Page/石松植物門.md "wikilink")(Lycopodiophyta)

### [水韭綱](../Page/水韭綱.md "wikilink")(Isoetopsida)

  - [水韭目](../Page/水韭目.md "wikilink")（Isoetales）
      - [水韭科](../Page/水韭科.md "wikilink")（Isoetaceae）

### [石松綱](../Page/石松綱.md "wikilink")(Lycopodiopsida)

  - [無葉舌亞綱](../Page/無葉舌亞綱.md "wikilink")（Eligulatae）
      - [鐮木目](../Page/鐮木目.md "wikilink")（已絕種）
      - [始鱗木目](../Page/始鱗木目.md "wikilink")（已絕種）
      - [石松目](../Page/石松目.md "wikilink")（Lycopodiales）
          - [石松科](../Page/石松科.md "wikilink")（Lycopodiaceae）
  - [葉舌亞綱](../Page/葉舌亞綱.md "wikilink")（Ligulatae）
      - [鱗木目](../Page/鱗木目.md "wikilink")（已絕種）
      - [卷柏目](../Page/卷柏目.md "wikilink")（Selaginellales）
          - [卷柏科](../Page/卷柏科.md "wikilink")（Selaginellaceae）

## [有節植物門](../Page/有節植物門.md "wikilink")(Equisetophyta)

### [楔葉綱](../Page/楔葉綱.md "wikilink")(Sphenopsida)

  - [木賊目](../Page/木賊目.md "wikilink")（Equisetales）
      - [木賊科](../Page/木賊科.md "wikilink")（Equisetaceae）

## [真蕨植物門](../Page/真蕨植物門.md "wikilink")(Filicophyta)

### [薄囊蕨綱](../Page/薄囊蕨綱.md "wikilink")(Filicopsida)

  - [蕨目](../Page/蕨目.md "wikilink")（Filicales）
      - [鐵角蕨科](../Page/鐵角蕨科.md "wikilink")（Aspleniaceae）
      - [蹄盖蕨科](../Page/蹄盖蕨科.md "wikilink")（Athyriaceae）
      - [烏毛蕨科](../Page/烏毛蕨科.md "wikilink")（Blechnaceae）
      - [燕尾蕨科](../Page/燕尾蕨科.md "wikilink")（Cheiropleuriaceae）
      - [桫欏蕨科](../Page/桫欏蕨科.md "wikilink")（Cyatheaceae）
      - [骨碎補科](../Page/骨碎補科.md "wikilink")（Davalliaceae）
      - [碗蕨科](../Page/碗蕨科.md "wikilink")（Dennstaedtiaceae）
      - [蚌殼蕨科](../Page/蚌殼蕨科.md "wikilink")（Dicksoniaceae）
      - [雙扇蕨科](../Page/雙扇蕨科.md "wikilink")（Dipteridaceae）
      - [鱗毛蕨科](../Page/鱗毛蕨科.md "wikilink")（Dryopteridaceae）
          - [鱗毛蕨亞科](../Page/鱗毛蕨亞科.md "wikilink")（Dryopteridoidaceae）
          - [三叉蕨亞科](../Page/三叉蕨亞科.md "wikilink")（Tectarioideae）
      - [裡白蕨科](../Page/裡白蕨科.md "wikilink")（Gleicheniaceae）
      - [禾葉蕨科](../Page/禾葉蕨科.md "wikilink")（Grammitidaceae）
      - [膜蕨科](../Page/膜蕨科.md "wikilink")（Hymenophyllaceae）
      - [无孔蕨科](../Page/无孔蕨科.md "wikilink")（Hymenophyllopsidaceae）
      - [鱗始蕨科](../Page/鱗始蕨科.md "wikilink")（Lindsaeaceae）
      - [羅蔓藤蕨科](../Page/羅蔓藤蕨科.md "wikilink")（Lomariopsidaceae）
      - [毛囊蕨科](../Page/毛囊蕨科.md "wikilink")（Lophosoriaceae）
      - [偏环蕨科](../Page/偏环蕨科.md "wikilink")（Loxomataceae）
      - [马通蕨科](../Page/马通蕨科.md "wikilink")（Matoniaceae）
      - [蚌桫蕨科](../Page/蚌桫蕨科.md "wikilink")（Metaxyaceae）
      - [稀子蕨科](../Page/稀子蕨科.md "wikilink")（Monachosoraceae）
      - [腎蕨科](../Page/腎蕨科.md "wikilink")（Nephrolepidaceae）
      - [蓧蕨科](../Page/蓧蕨科.md "wikilink")（Oleandraceae）
      - [紫其科](../Page/紫其科.md "wikilink")（Osmundaceae）
      - [瘤足蕨科](../Page/瘤足蕨科.md "wikilink")（Plagiogyriaceae）
      - [水龍骨科](../Page/水龍骨科.md "wikilink")（Polypodiaceae）
      - [鳳尾蕨科](../Page/鳳尾蕨科.md "wikilink")（Pteridaceae）
      - [莎草蕨科](../Page/莎草蕨科.md "wikilink")（Schizaeaceae）
      - [金星蕨科](../Page/金星蕨科.md "wikilink")（Thelypteridaceae）
      - [書帶蕨科](../Page/書帶蕨科.md "wikilink")（Vittariaceae）
      - [蹄蓋蕨科](../Page/蹄蓋蕨科.md "wikilink")（Woodsiaceae）
  - [蘋目](../Page/蘋目.md "wikilink")（Hydropteridales）
      - [滿江紅科](../Page/滿江紅科.md "wikilink")（Azollaceae）
      - [田字草科](../Page/田字草科.md "wikilink")（Marsileaceae）
      - [槐葉蘋科](../Page/槐葉蘋科.md "wikilink")（Salviniaceae）

### [厚囊蕨綱](../Page/厚囊蕨綱.md "wikilink")(Eusporangiopsida)

  - [觀音座蓮目](../Page/觀音座蓮目.md "wikilink")（Marattiales）
      - [觀音座蓮科](../Page/觀音座蓮科.md "wikilink")（Marattiaceae）
  - [瓶爾小草目](../Page/瓶爾小草科.md "wikilink")（Ophioglossales）
      - [瓶爾小草科](../Page/瓶爾小草科.md "wikilink")（Ophioglossaceae）

## [帚蕨植物門](../Page/帚蕨植物門.md "wikilink")(Psilotophyta)

### [松葉蕨綱](../Page/松葉蕨綱.md "wikilink")(Psilopsida)

  - [松葉蕨目](../Page/松葉蕨目.md "wikilink")（Psilotales）
      - [松葉蕨科](../Page/松葉蕨科.md "wikilink")（Psilotaceae）

## [松柏門](../Page/松柏門.md "wikilink")(Coniferophyta)

### [松柏綱](../Page/松柏綱.md "wikilink")(Coniferopsida)

  - [松柏目](../Page/松柏目.md "wikilink")（Coniferales）
      - [南洋杉科](../Page/南洋杉科.md "wikilink")（Araucariaceae）
      - [柏科](../Page/柏科.md "wikilink")（Cupressaceae）
      - [松科](../Page/松科.md "wikilink")（Pinaceae）
      - [金松科](../Page/金松科.md "wikilink")（Sciadopityaceae）
  - [紅豆杉目](../Page/紅豆杉目.md "wikilink")（Taxales）
      - [羅漢松科](../Page/羅漢松科.md "wikilink")（Podocarpaceae）
      - [三尖杉科](../Page/三尖杉科.md "wikilink")（Cephalotaxaceae）
      - [紅豆杉科](../Page/紅豆杉科.md "wikilink")（Taxaceae）

## [蘇鐵門](../Page/蘇鐵門.md "wikilink")(Cycadophyta)

### [蘇鐵綱](../Page/蘇鐵綱.md "wikilink")(Cycadopsida)

  - [蘇鐵目](../Page/蘇鐵目.md "wikilink")（Cycadales）
      - [蘇鐵科](../Page/蘇鐵科.md "wikilink")（Cycadaceae）
      - [托葉鐵科](../Page/托葉鐵科.md "wikilink")（Stangeriaceae）
      - [澤米鐵科](../Page/澤米鐵科.md "wikilink")（Zamiaceae）

## [銀杏門](../Page/銀杏門.md "wikilink")(Ginkgophyta)

### [銀杏綱](../Page/銀杏綱.md "wikilink")(Ginkgoopsida)

#### [銀杏目](../Page/銀杏目.md "wikilink")（Ginkgoales）

  - [銀杏科](../Page/銀杏科.md "wikilink")（Ginkgoaceae）

## [买麻藤门](../Page/买麻藤门.md "wikilink")(Gnetophyta)

### [買麻藤綱](../Page/買麻藤綱.md "wikilink")(Gnetopsida)

#### [麻黃目](../Page/麻黃目.md "wikilink")(Ephedrales)

  - [麻黃科](../Page/麻黃科.md "wikilink")（Ephedraceae）

#### [買麻藤目](../Page/買麻藤目.md "wikilink")(Gnetales)

  - [買麻藤科](../Page/買麻藤科.md "wikilink")（Gnetaceae）買麻藤属

#### [百歲蘭目](../Page/百歲蘭目.md "wikilink")(Welwitschiales)

  - [百歲蘭科](../Page/百歲蘭科.md "wikilink")（Welwitschiaceae）

## [被子植物門](../Page/被子植物門.md "wikilink")(Magnoliophyta)

参见[克朗奎斯特分类法和](../Page/克朗奎斯特分类法.md "wikilink")[APG II
分类法](../Page/APG_II_分类法.md "wikilink")

### [雙子葉植物綱](../Page/雙子葉植物綱.md "wikilink")(Dicotyledoneae)

#### 原雙子葉植物

##### [金魚藻目](../Page/金魚藻目.md "wikilink")(Ceratophyllales)

  - [金魚藻科](../Page/金魚藻科.md "wikilink")（Ceratophyllaceae）

##### [互叶梅目](../Page/互叶梅目.md "wikilink")(Amborellales)

  - [互叶梅科](../Page/互叶梅科.md "wikilink")（Amborellaceae）

##### [木蘭藤目](../Page/木蘭藤目.md "wikilink")(Austrobaileyales)

  - [木蘭藤科](../Page/木蘭藤科.md "wikilink")（Austrobaileyaceae）
  - [五味子科](../Page/五味子科.md "wikilink")（Schisandraceae）
  - [腺齒木科](../Page/腺齒木科.md "wikilink")（Trimeniaceae）

##### [金粟蘭目](../Page/金粟蘭目.md "wikilink")(Chloranthales)

  - [金粟蘭科](../Page/金粟蘭科.md "wikilink")（Chloranthaceae）

##### [睡蓮目](../Page/睡蓮目.md "wikilink")(Nymphaeales)

  - [蒓菜科](../Page/蒓菜科.md "wikilink")（Cabombaceae）
  - [睡蓮科](../Page/睡蓮科.md "wikilink")（Nymphaeaceae）

##### [白樟目](../Page/白樟目.md "wikilink")(Canellales)

  - [白樟科](../Page/白樟科.md "wikilink")（Canellaceae）
  - [林仙科](../Page/林仙科.md "wikilink")（Winteraceae）

##### [樟目](../Page/樟目.md "wikilink")(Laurales)

  - [香皮茶科](../Page/香皮茶科.md "wikilink")（Atherospermataceae）
  - [臘梅科](../Page/臘梅科.md "wikilink")（Calycanthaceae）
  - [腺蕊花科](../Page/腺蕊花科.md "wikilink")（Gomortegaceae）
  - [蓮葉桐科](../Page/蓮葉桐科.md "wikilink")（Hernandiaceae）
  - [樟科](../Page/樟科.md "wikilink")（Lauraceae）
  - [杯轴花科](../Page/杯轴花科.md "wikilink")（Monimiaceae）
  - [罎罐花科](../Page/罎罐花科.md "wikilink")（Siparunaceae）

##### [木蘭目](../Page/木蘭目.md "wikilink")(Magnoliales)

  - [番荔枝科](../Page/番荔枝科.md "wikilink")（Annonaceae）
  - [單心木蘭科](../Page/單心木蘭科.md "wikilink")（Degeneriaceae）
  - [帽花木科](../Page/帽花木科.md "wikilink")（Eupomatiaceae）
  - [舌蕊花科](../Page/舌蕊花科.md "wikilink")（Himantandraceae）
  - [木蘭科](../Page/木蘭科.md "wikilink")（Magnoliaceae）
  - [肉豆蔻科](../Page/肉豆蔻科.md "wikilink")（Myristicaceae）

##### [胡椒目](../Page/胡椒目.md "wikilink")(Piperales)

  - [馬兜鈴科](../Page/馬兜鈴科.md "wikilink")（Aristolochiaceae）
  - [菌花科](../Page/菌花科.md "wikilink")（Hydnoraceae）
  - [短蕊花科](../Page/短蕊花科.md "wikilink")（Lactoridaceae）
  - [胡椒科](../Page/胡椒科.md "wikilink")（Piperaceae）
  - [三白草科](../Page/三白草科.md "wikilink")（Saururaceae）

#### [真雙子葉植物](../Page/真雙子葉植物.md "wikilink")

##### [黃楊目](../Page/黃楊目.md "wikilink")(Buxales)

  - [黃楊科](../Page/黃楊科.md "wikilink")（Buxaceae）
  - [雙頰果科](../Page/雙頰果科.md "wikilink")（Didymelaceae）

##### [山龍眼目](../Page/山龍眼目.md "wikilink")(Proteales)

  - [蓮科](../Page/蓮科.md "wikilink")（Nelumbonaceae）
  - [懸鈴木科](../Page/懸鈴木科.md "wikilink")（Platanaceae）
  - [山龍眼科](../Page/山龍眼科.md "wikilink")（Proteaceae）

##### [毛茛目](../Page/毛茛目.md "wikilink")(Ranunculales)

  - [小檗科](../Page/小檗科.md "wikilink")（Berberidaceae）
  - [星葉科](../Page/星葉科.md "wikilink")（Circaeasteraceae）
  - [領春木科](../Page/領春木科.md "wikilink")（Eupteleaceae）
  - [紫堇科](../Page/紫堇科.md "wikilink")（Fumariaceae）
  - [獨葉草科](../Page/獨葉草科.md "wikilink")（Kingdoniaceae）
  - [木通科](../Page/木通科.md "wikilink")（Lardizabalaceae）
  - [防己科](../Page/防己科.md "wikilink")（Menispermaceae）
  - [罌粟科](../Page/罌粟科.md "wikilink")（Papaveraceae）
  - [蕨葉草科](../Page/蕨葉草科.md "wikilink")（Pteridophyllaceae）
  - [毛茛科](../Page/毛茛科.md "wikilink")（Ranunculaceae）

##### [石竹目](../Page/石竹目.md "wikilink")(Caryophyllales)

  - [透鏡籽科](../Page/透鏡籽科.md "wikilink")（Achatocarpaceae）
  - [番杏科](../Page/番杏科.md "wikilink")（Aizoaceae）
  - [莧科](../Page/莧科.md "wikilink")（Amaranthaceae）
  - [鈎枝藤科](../Page/鈎枝藤科.md "wikilink")（Ancistrocladaceae）
  - [翼萼茶科](../Page/翼萼茶科.md "wikilink")（Asteropeiaceae）
  - [節柄科](../Page/節柄科.md "wikilink")（Barbeuiaceae）
  - [落葵科](../Page/落葵科.md "wikilink")（Basellaceae）
  - [仙人掌科](../Page/仙人掌科.md "wikilink")（Cactaceae）
  - [石竹科](../Page/石竹科.md "wikilink")（Caryophyllaceae）
  - [龍樹科](../Page/龍樹科.md "wikilink")（Didiereaceae）
  - [雙鈎葉科](../Page/雙鈎葉科.md "wikilink")（Dioncophyllaceae）
  - [茅膏菜科](../Page/茅膏菜科.md "wikilink")（Droseraceae）
  - [露叶毛毡苔科](../Page/露叶毛毡苔科.md "wikilink")（Drosophyllaceae）
  - [瓣鳞花科](../Page/瓣鳞花科.md "wikilink")（Frankeniaceae）
  - [吉粟草科](../Page/吉粟草科.md "wikilink")（Gisekiaceae）
  - [浜藜叶科](../Page/浜藜叶科.md "wikilink")（Halophytaceae）
  - [粟米草科](../Page/粟米草科.md "wikilink")（Molluginaceae）
  - [豬籠草科](../Page/豬籠草科.md "wikilink")（Nepenthaceae）
  - [紫茉莉科](../Page/紫茉莉科.md "wikilink")（Nyctaginaceae）
  - [非洲桐科](../Page/非洲桐科.md "wikilink")（Physenaceae）
  - [商陸科](../Page/商陸科.md "wikilink")（Phytolaccaceae）
  - [藍雪科](../Page/藍雪科.md "wikilink")（Plumbaginaceae）
  - [蓼科](../Page/蓼科.md "wikilink")（Polygonaceae）
  - [馬齒莧科](../Page/馬齒莧科.md "wikilink")（Portulacaceae）
  - [棒木科](../Page/棒木科.md "wikilink")（Rhabdodendraceae）
  - [肉叶刺茎藜科](../Page/肉叶刺茎藜科.md "wikilink")（Sarcobataceae）
  - [油蠟樹科](../Page/油蠟樹科.md "wikilink")（Simmondsiaceae）
  - [閉籽花科](../Page/閉籽花科.md "wikilink")（Stegnospermataceae）
  - [檉柳科](../Page/檉柳科.md "wikilink")（Tamaricaceae）

##### [洋二仙草目](../Page/洋二仙草目.md "wikilink")(Gunnerales)

  - [洋二仙草科](../Page/洋二仙草科.md "wikilink")（Gunneraceae）
  - [香灌木科](../Page/香灌木科.md "wikilink")（Myrothamnaceae）

##### [檀香目](../Page/檀香目.md "wikilink")(Santalales)

  - [蛇菰科](../Page/蛇菰科.md "wikilink")（Balanophoraceae）
  - [桑寄生科](../Page/桑寄生科.md "wikilink")（Loranthaceae）
  - [羽毛果科](../Page/羽毛果科.md "wikilink")（Misodendraceae）
  - [鐵青樹科](../Page/鐵青樹科.md "wikilink")（Olacaceae）
  - [山柚科](../Page/山柚科.md "wikilink")（Opiliaceae）
  - [檀香科](../Page/檀香科.md "wikilink")（Santalaceae）
  - [青皮木科](../Page/青皮木科.md "wikilink")（Schoepfiaceae）

##### [虎耳草目](../Page/虎耳草目.md "wikilink")(Saxifragales)

  - [楓香科](../Page/楓香科.md "wikilink")（Altingiaceae）
  - [膠藤科](../Page/膠藤科.md "wikilink")（Aphanopetalaceae）
  - [連香樹科](../Page/連香樹科.md "wikilink")（Cercidiphyllaceae）
  - [景天科](../Page/景天科.md "wikilink")（Crassulaceae）
  - [鎖陽科](../Page/鎖陽科.md "wikilink")（Cynomoriaceae）
  - [交讓木科](../Page/交讓木科.md "wikilink")（Daphniphyllaceae）
  - [茶藨子科](../Page/茶藨子科.md "wikilink")（Grossulariaceae）
  - [小二仙草科](../Page/小二仙草科.md "wikilink")（Haloragaceae）
  - [金縷梅科](../Page/金縷梅科.md "wikilink")（Hamamelidaceae）
  - [鼠刺科](../Page/鼠刺科.md "wikilink")（Iteaceae）
  - [芍藥科](../Page/芍藥科.md "wikilink")（Paeoniaceae）
  - [扯根菜科](../Page/扯根菜科.md "wikilink")（Penthoraceae）
  - [圍盤樹科](../Page/圍盤樹科.md "wikilink")（Peridiscaceae）
  - [齒蕊科](../Page/齒蕊科.md "wikilink")（Pterostemonaceae）
  - [虎耳草科](../Page/虎耳草科.md "wikilink")（Saxifragaceae）
  - [四果木科](../Page/四果木科.md "wikilink")（Tetracarpaeaceae）

##### [傘形目](../Page/傘形目.md "wikilink")(Apiales)

  - [傘形科](../Page/傘形科.md "wikilink")（Apiaceae）
  - [五加科](../Page/五加科.md "wikilink")（Araliaceae）
  - [裂果红科](../Page/裂果红科.md "wikilink")（Myodocarpaceae）
  - [海桐花科](../Page/海桐花科.md "wikilink")（Pittosporaceae）
  - [夷茱萸科](../Page/夷茱萸科.md "wikilink")（Griseliniaceae）
  - [科](../Page/科.md "wikilink")（Pennantiaceae）
  - [鞘柄木科](../Page/鞘柄木科.md "wikilink")（Torricelliaceae）

##### [冬青目](../Page/冬青目.md "wikilink")(Aquifoliales)

  - [冬青科](../Page/冬青科.md "wikilink")（Aquifoliaceae）
  - [心翼果科](../Page/心翼果科.md "wikilink")（Cardiopteridaceae）
  - [青莢葉科](../Page/青莢葉科.md "wikilink")（Helwingiaceae）
  - [葉茶藨科](../Page/葉茶藨科.md "wikilink")（Phyllonomaceae）
  - [金檀木科](../Page/金檀木科.md "wikilink")（Stemonuraceae）

##### [菊目](../Page/菊目.md "wikilink")(Asterales)

  - [假海桐科](../Page/假海桐科.md "wikilink")（Alseuosmiaceae）
  - [雪葉科](../Page/雪葉科.md "wikilink")（Argophyllaceae）
  - [菊科](../Page/菊科.md "wikilink")（Asteraceae）
  - [頭花草科](../Page/頭花草科.md "wikilink")（Calyceraceae）
  - [桔梗科](../Page/桔梗科.md "wikilink")（Campanulaceae）
  - [陀螺果科](../Page/陀螺果科.md "wikilink")（Donatiaceae）
  - [草海桐科](../Page/草海桐科.md "wikilink")（Goodeniaceae）
  - [睡菜科](../Page/睡菜科.md "wikilink")（Menyanthaceae）
  - [五膜草科](../Page/五膜草科.md "wikilink")（Pentaphragmataceae）
  - [石冬青科](../Page/石冬青科.md "wikilink")（Phellinaceae）
  - [腕帶花科](../Page/腕帶花科.md "wikilink")（Rousseaceae）
  - [花柱草科](../Page/花柱草科.md "wikilink")（Stylidiaceae）

##### [川續斷目](../Page/川續斷目.md "wikilink")(Dipsacales)

  - [五福花科](../Page/五福花科.md "wikilink")（Adoxaceae）
  - [忍冬科](../Page/忍冬科.md "wikilink")（Caprifoliaceae）
  - [黃錦帶科](../Page/黃錦帶科.md "wikilink")（Diervillaceae）
  - [川續斷科](../Page/川續斷科.md "wikilink")（Dipsacaceae）
  - [北極花科](../Page/北極花科.md "wikilink")（Linnaeaceae）
  - [刺蔘科](../Page/刺蔘科.md "wikilink")（Morinaceae）
  - [敗醬科](../Page/敗醬科.md "wikilink")（Valerianaceae）

##### [山茱萸目](../Page/山茱萸目.md "wikilink")(Cornales)

  - [山茱萸科](../Page/山茱萸科.md "wikilink")（Cornaceae）
  - [柯茱萸科](../Page/柯茱萸科.md "wikilink")（Curtisiaceae）
  - [假石南科](../Page/假石南科.md "wikilink")（Grubbiaceae）
  - [綉球科](../Page/綉球科.md "wikilink")（Hydrangeaceae）
  - [水穗草科](../Page/水穗草科.md "wikilink")（Hydrostachyaceae）
  - [刺蓮花科](../Page/刺蓮花科.md "wikilink")（Loasaceae）
  - [藍果樹科](../Page/藍果樹科.md "wikilink")（Nyssaceae）

##### [杜鵑花目](../Page/杜鵑花目.md "wikilink")(Ericales)

  - [獼猴桃科](../Page/獼猴桃科.md "wikilink")（Actinidiaceae）
  - [鳳仙花科](../Page/鳳仙花科.md "wikilink")（Balsaminaceae）
  - [山柳科](../Page/山柳科.md "wikilink")（Clethraceae）
  - [翅萼樹科](../Page/翅萼樹科.md "wikilink")（Cyrillaceae）
  - [巖梅科](../Page/巖梅科.md "wikilink")（Diapensiaceae）
  - [柿樹科](../Page/柿樹科.md "wikilink")（Ebenaceae）
  - [杜鵑花科](../Page/杜鵑花科.md "wikilink")（Ericaceae）
  - [刺樹科](../Page/刺樹科.md "wikilink")（Fouquieriaceae）
  - [玉蕊科](../Page/玉蕊科.md "wikilink")（Lecythidaceae）
  - [杜莖山科](../Page/杜莖山科.md "wikilink")（Maesaceae）
  - [蜜囊花科](../Page/蜜囊花科.md "wikilink")（Marcgraviaceae）
  - [帽蕊草科](../Page/帽蕊草科.md "wikilink")（Mitrastemonaceae）
  - [紫金牛科](../Page/紫金牛科.md "wikilink")（Myrsinaceae）
  - [假紅樹科](../Page/假紅樹科.md "wikilink")（Pellicieraceae）
  - [五列木科](../Page/五列木科.md "wikilink")（Pentaphylacaceae）
  - [花荵科](../Page/花荵科.md "wikilink")（Polemoniaceae）
  - [報春花科](../Page/報春花科.md "wikilink")（Primulaceae）
  - [捕蠅幌科](../Page/捕蠅幌科.md "wikilink")（Roridulaceae）
  - [山欖科](../Page/山欖科.md "wikilink")（Sapotaceae）
  - [瓶子草科](../Page/瓶子草科.md "wikilink")（Sarraceniaceae）
  - [肋果茶科](../Page/肋果茶科.md "wikilink")（Sladeniaceae）
  - [安息香科](../Page/安息香科.md "wikilink")（Styracaceae）
  - [山礬科](../Page/山礬科.md "wikilink")（Symplocaceae）
  - [厚皮香科](../Page/厚皮香科.md "wikilink")（Ternstroemiaceae）
  - [四籽樹科](../Page/四籽樹科.md "wikilink")（Tetrameristaceae）
  - [山茶科](../Page/山茶科.md "wikilink")（Theaceae）
  - [假輪葉科](../Page/假輪葉科.md "wikilink")（Theophrastaceae）

##### [絞木目](../Page/絞木目.md "wikilink")(Garryales)

  - [桃葉珊瑚科](../Page/桃葉珊瑚科.md "wikilink")（Aucubaceae）
  - [杜仲科](../Page/杜仲科.md "wikilink")（Eucommiaceae）
  - [絞木科](../Page/絞木科.md "wikilink")（Garryaceae）

##### [龍膽目](../Page/龍膽目.md "wikilink")(Gentianales)

  - [夾竹桃科](../Page/夾竹桃科.md "wikilink")（Apocynaceae）
  - [胡蔓藤科](../Page/胡蔓藤科.md "wikilink")（Gelsemiaceae）
  - [龍膽科](../Page/龍膽科.md "wikilink")（Gentianaceae）
  - [馬錢科](../Page/馬錢科.md "wikilink")（Loganiaceae）
  - [茜草科](../Page/茜草科.md "wikilink")（Rubiaceae）

##### [脣形目](../Page/脣形目.md "wikilink")(Lamiales)

  - [爵牀科](../Page/爵牀科.md "wikilink")（Acanthaceae）
  - [紫葳科](../Page/紫葳科.md "wikilink")（Bignoniaceae）
  - [腺毛草科](../Page/腺毛草科.md "wikilink")（Byblidaceae）
  - [蒲包花科](../Page/蒲包花科.md "wikilink")（Calceolariaceae）
  - [香茜科](../Page/香茜科.md "wikilink")（Carlemanniaceae）
  - [盘果木科](../Page/盘果木科.md "wikilink")（Cyclocheilaceae）
  - [苦苣苔科](../Page/苦苣苔科.md "wikilink")（Gesneriaceae）
  - [脣形科](../Page/脣形科.md "wikilink")（Lamiaceae）
  - [狸藻科](../Page/狸藻科.md "wikilink")（Lentibulariaceae）
  - [母草科](../Page/母草科.md "wikilink")（Linderniaceae）
  - [角胡麻科](../Page/角胡麻科.md "wikilink")（Martyniaceae）
  - [木犀科](../Page/木犀科.md "wikilink")（Oleaceae）
  - [列當科](../Page/列當科.md "wikilink")（Orobanchaceae）
  - [泡桐科](../Page/泡桐科.md "wikilink")（Paulowniaceae）
  - [胡麻科](../Page/胡麻科.md "wikilink")（Pedaliaceae）
  - [透骨草科](../Page/透骨草科.md "wikilink")（Phrymaceae）
  - [車前科](../Page/車前科.md "wikilink")（Plantaginaceae）
  - [環生籽科](../Page/環生籽科.md "wikilink")（Plocospermataceae）
  - [夷地黄科](../Page/夷地黄科.md "wikilink")（Schlegeliaceae）
  - [玄蔘科](../Page/玄蔘科.md "wikilink")（Scrophulariaceae）
  - [密穗草科](../Page/密穗草科.md "wikilink")（Stilbaceae）
  - [四棱果科](../Page/四棱果科.md "wikilink")（Tetrachondraceae）
  - [馬鞭草科](../Page/馬鞭草科.md "wikilink")（Verbenaceae）

##### [茄目](../Page/茄目.md "wikilink")(Solanales)

  - [旋花科](../Page/旋花科.md "wikilink")（Convolvulaceae）
  - [田基麻科](../Page/田基麻科.md "wikilink")（Hydrophyllaceae）
  - [山醋李科](../Page/山醋李科.md "wikilink")（Montiniaceae）
  - [茄科](../Page/茄科.md "wikilink")（Solanaceae）
  - [楔瓣花科](../Page/楔瓣花科.md "wikilink")（Sphenocleaceae）

##### [燧體木目](../Page/燧體木目.md "wikilink")(Crossosomatales)

  - [燧體木科](../Page/燧體木科.md "wikilink")（Crossosomataceae）
  - [旌節花科](../Page/旌節花科.md "wikilink")（Stachyuraceae）
  - [省沽油科](../Page/省沽油科.md "wikilink")（Staphyleaceae）

##### [衛矛目](../Page/衛矛目.md "wikilink")(Celastrales)

  - [衛矛科](../Page/衛矛科.md "wikilink")（Celastraceae）
  - [洋酢浆草科](../Page/洋酢浆草科.md "wikilink")（Lepidobotryaceae）
  - [微形草科](../Page/微形草科.md "wikilink")（Lepuropetalaceae）
  - [梅花草科](../Page/梅花草科.md "wikilink")（Parnassiaceae）

##### [葫蘆目](../Page/葫蘆目.md "wikilink")(Cucurbitales)

  - [四柱木科](../Page/四柱木科.md "wikilink")（Anisophylleaceae）
  - [秋海棠科](../Page/秋海棠科.md "wikilink")（Begoniaceae）
  - [馬桑科](../Page/馬桑科.md "wikilink")（Coriariaceae）
  - [棒果木科](../Page/棒果木科.md "wikilink")（Corynocarpaceae）
  - [葫蘆科](../Page/葫蘆科.md "wikilink")（Cucurbitaceae）
  - [野麻科](../Page/野麻科.md "wikilink")（Datiscaceae）
  - [四數木科](../Page/四數木科.md "wikilink")（Tetramelaceae）

##### [豆目](../Page/豆目.md "wikilink")(Fabales)

  - [豆科](../Page/豆科.md "wikilink")（Fabaceae）
  - [遠志科](../Page/遠志科.md "wikilink")（Polygalaceae）
  - [皂皮樹科](../Page/皂皮樹科.md "wikilink")（Quillajaceae）
  - [海人樹科](../Page/海人樹科.md "wikilink")（Surianaceae）

##### [殼斗目](../Page/殼斗目.md "wikilink")(Fagales)

  - [樺木科](../Page/樺木科.md "wikilink")（Betulaceae）
  - [木麻黃科](../Page/木麻黃科.md "wikilink")（Casuarinaceae）
  - [殼斗科](../Page/殼斗科.md "wikilink")（Fagaceae）
  - [胡桃科](../Page/胡桃科.md "wikilink")（Juglandaceae）
  - [楊梅科](../Page/楊梅科.md "wikilink")（Myricaceae）
  - [南青岡科](../Page/南青岡科.md "wikilink")（Nothofagaceae）
  - [馬尾樹科](../Page/馬尾樹科.md "wikilink")（Rhoipteleaceae）
  - [太果木科](../Page/太果木科.md "wikilink")（Ticodendraceae）

##### [金虎尾目](../Page/金虎尾目.md "wikilink")(Malpighiales)

  - [鐘花科](../Page/鐘花科.md "wikilink")（Achariaceae）
  - [橡子木科](../Page/橡子木科.md "wikilink")（Balanopaceae）
  - [多子科](../Page/多子科.md "wikilink")（Bonnetiaceae）
  - [多柱樹科](../Page/多柱樹科.md "wikilink")（Caryocaraceae）
  - [金殼果科](../Page/金殼果科.md "wikilink")（Chrysobalanaceae）
  - [藤黃科](../Page/藤黃科.md "wikilink")（Clusiaceae）
  - [垂籽樹科](../Page/垂籽樹科.md "wikilink")（Ctenolophonaceae）
  - [毒鼠子科](../Page/毒鼠子科.md "wikilink")（Dichapetalaceae）
  - [溝繁縷科](../Page/溝繁縷科.md "wikilink")（Elatinaceae）
  - [古柯科](../Page/古柯科.md "wikilink")（Erythroxylaceae）
  - [大戟科](../Page/大戟科.md "wikilink")（Euphorbiaceae）
  - [合丝花科](../Page/合丝花科.md "wikilink")（Euphroniaceae）
  - [毛藥樹科](../Page/毛藥樹科.md "wikilink")（Goupiaceae）
  - [香膏科](../Page/香膏科.md "wikilink")（Humiriaceae）
  - [黏木科](../Page/黏木科.md "wikilink")（Ixonanthaceae）
  - [裂藥花科](../Page/裂藥花科.md "wikilink")（Lacistemataceae）
  - [亞麻科](../Page/亞麻科.md "wikilink")（Linaceae）
  - [五翼果科](../Page/五翼果科.md "wikilink")（Lophopyxidaceae）
  - [王冠草科](../Page/王冠草科.md "wikilink")（Malesherbiaceae）
  - [金虎尾科](../Page/金虎尾科.md "wikilink")（Malpighiaceae）
  - [水母柱科](../Page/水母柱科.md "wikilink")（Medusagynaceae）
  - [金蓮木科](../Page/金蓮木科.md "wikilink")（Ochnaceae）
  - [翅子藤科](../Page/翅子藤科.md "wikilink")（Pandaceae）
  - [西番蓮科](../Page/西番蓮科.md "wikilink")（Passifloraceae）
  - [葉下珠科](../Page/葉下珠科.md "wikilink")（Phyllanthaceae）
  - [苦胡桃科](../Page/苦胡桃科.md "wikilink")（Picrodendraceae）
  - [川苔草科](../Page/川苔草科.md "wikilink")（Podostemaceae）
  - [羽葉樹科](../Page/羽葉樹科.md "wikilink")（Quiinaceae）
  - [大花草科](../Page/大花草科.md "wikilink")（Rafflesiaceae）
  - [紅樹科](../Page/紅樹科.md "wikilink")（Rhizophoraceae）
  - [楊柳科](../Page/楊柳科.md "wikilink")（Salicaceae）
  - [三角果科](../Page/三角果科.md "wikilink")（Trigoniaceae）
  - [時鐘花科](../Page/時鐘花科.md "wikilink")（Turneraceae）
  - [堇菜科](../Page/堇菜科.md "wikilink")（Violaceae）

##### [酢漿草目](../Page/酢漿草目.md "wikilink")(Oxalidales)

  - [瓣裂果科](../Page/瓣裂果科.md "wikilink")（Brunelliaceae）
  - [土瓶草科](../Page/土瓶草科.md "wikilink")（Cephalotaceae）
  - [牛栓藤科](../Page/牛栓藤科.md "wikilink")（Connaraceae）
  - [火把樹科](../Page/火把樹科.md "wikilink")（Cunoniaceae）
  - [杜英科](../Page/杜英科.md "wikilink")（Elaeocarpaceae）
  - [酢漿草科](../Page/酢漿草科.md "wikilink")（Oxalidaceae）

##### [薔薇目](../Page/薔薇目.md "wikilink")(Rosales)

  - [鈎毛樹科](../Page/鈎毛樹科.md "wikilink")（Barbeyaceae）
  - [大麻科](../Page/大麻科.md "wikilink")（Cannabaceae）
  - [八瓣果科](../Page/八瓣果科.md "wikilink")（Dirachmaceae）
  - [胡穨子科](../Page/胡穨子科.md "wikilink")（Elaeagnaceae）
  - [桑科](../Page/桑科.md "wikilink")（Moraceae）
  - [鼠李科](../Page/鼠李科.md "wikilink")（Rhamnaceae）
  - [薔薇科](../Page/薔薇科.md "wikilink")（Rosaceae）
  - [榆科](../Page/榆科.md "wikilink")（Ulmaceae）
  - [蕁麻科](../Page/蕁麻科.md "wikilink")（Urticaceae）

##### [十字花目](../Page/十字花目.md "wikilink")(Brassicales)

  - [疊珠樹科](../Page/疊珠樹科.md "wikilink")（Akaniaceae）
  - [腌藜草科](../Page/腌藜草科.md "wikilink")（Bataceae）
  - [十字花科](../Page/十字花科.md "wikilink")（Brassicaceae）
  - [鐘萼木科](../Page/鐘萼木科.md "wikilink")（Bretschneideraceae）
  - [山柑科](../Page/山柑科.md "wikilink")（Capparaceae）
  - [番木瓜科](../Page/番木瓜科.md "wikilink")（Caricaceae）
  - [醉蝶花科](../Page/醉蝶花科.md "wikilink")（Cleomaceae）
  - [澳遠志科](../Page/澳遠志科.md "wikilink")（Emblingiaceae）
  - [環蕊科](../Page/環蕊科.md "wikilink")（Gyrostemonaceae）
  - [刺枝樹科](../Page/刺枝樹科.md "wikilink")（Koeberliniaceae）
  - [沼花科](../Page/沼花科.md "wikilink")（Limnanthaceae）
  - [辣木科](../Page/辣木科.md "wikilink")（Moringaceae）
  - [瘤藥樹科](../Page/瘤藥樹科.md "wikilink")（Pentadiplandraceae）
  - [木犀草科](../Page/木犀草科.md "wikilink")（Resedaceae）
  - [刺茉莉科](../Page/刺茉莉科.md "wikilink")（Salvadoraceae）
  - [夷白花菜科](../Page/夷白花菜科.md "wikilink")（Setchellanthaceae）
  - [烈味三叶草科](../Page/烈味三叶草科.md "wikilink")（Tovariaceae）
  - [旱金蓮科](../Page/旱金蓮科.md "wikilink")（Tropaeolaceae）

##### [十齒花目](../Page/十齒花目.md "wikilink")(Huerteales)

  - [十齒花科](../Page/十齒花科.md "wikilink")（Dipentodontaceae）
  - [瘿椒樹科](../Page/瘿椒樹科.md "wikilink")（Tapisciaceae）

##### [錦葵目](../Page/錦葵目.md "wikilink")(Malvales)

  - [紅木科](../Page/紅木科.md "wikilink")（Bixaceae）
  - [半日花科](../Page/半日花科.md "wikilink")（Cistaceae）
  - [彎子木科](../Page/彎子木科.md "wikilink")（Cochlospermaceae）
  - [大花草科](../Page/大花草科.md "wikilink")（Cytinaceae）
  - [地果蓮木科](../Page/地果蓮木科.md "wikilink")（Diegodendraceae）
  - [龍腦香科](../Page/龍腦香科.md "wikilink")（Dipterocarpaceae）
  - [錦葵科](../Page/錦葵科.md "wikilink")（Malvaceae）
  - [文定果科](../Page/文定果科.md "wikilink")（Muntingiaceae）
  - [沙莓科](../Page/沙莓科.md "wikilink")（Neuradaceae）
  - [旋花樹科](../Page/旋花樹科.md "wikilink")（Sarcolaenaceae）
  - [球萼樹科](../Page/球萼樹科.md "wikilink")（Sphaerosepalaceae）
  - [瑞香科](../Page/瑞香科.md "wikilink")（Thymelaeaceae）

##### [無患子目](../Page/無患子目.md "wikilink")(Sapindales)

  - [漆樹科](../Page/漆樹科.md "wikilink")（Anacardiaceae）
  - [薰倒牛科](../Page/薰倒牛科.md "wikilink")（Biebersteiniaceae）
  - [橄欖科](../Page/橄欖科.md "wikilink")（Burseraceae）
  - [番苦木科](../Page/番苦木科.md "wikilink")（Kirkiaceae）
  - [楝科](../Page/楝科.md "wikilink")（Meliaceae）
  - [白刺科](../Page/白刺科.md "wikilink")（Nitrariaceae）
  - [駱駝蓬科](../Page/駱駝蓬科.md "wikilink")（Peganaceae）
  - [芸香科](../Page/芸香科.md "wikilink")（Rutaceae）
  - [無患子科](../Page/無患子科.md "wikilink")（Sapindaceae）
  - [苦木科](../Page/苦木科.md "wikilink")（Simaroubaceae）
  - [旱霸王科](../Page/旱霸王科.md "wikilink")（Tetradiclidaceae）

##### [牻牛兒苗目](../Page/牻牛兒苗目.md "wikilink")(Geraniales)

  - [花莖草科](../Page/花莖草科.md "wikilink")（Francoaceae）
  - [牻牛兒苗科](../Page/牻牛兒苗科.md "wikilink")（Geraniaceae）
  - [高柱花科](../Page/高柱花科.md "wikilink")（Hypseocharitaceae）
  - [杜香果科](../Page/杜香果科.md "wikilink")（Ledocarpaceae）
  - [蜜花科](../Page/蜜花科.md "wikilink")（Melianthaceae）
  - [曲胚科](../Page/曲胚科.md "wikilink")（Vivianiaceae）

##### [桃金孃目](../Page/桃金孃目.md "wikilink")(Myrtales)

  - [雙翼果科](../Page/雙翼果科.md "wikilink")（Alzateaceae）
  - [使君子科](../Page/使君子科.md "wikilink")（Combretaceae）
  - [隱翼科](../Page/隱翼科.md "wikilink")（Crypteroniaceae）
  - [異裂果科](../Page/異裂果科.md "wikilink")（Heteropyxidaceae）
  - [千屈菜科](../Page/千屈菜科.md "wikilink")（Lythraceae）
  - [野牡丹科](../Page/野牡丹科.md "wikilink")（Melastomataceae）
  - [穀木科](../Page/穀木科.md "wikilink")（Memecylaceae）
  - [桃金孃科](../Page/桃金孃科.md "wikilink")（Myrtaceae）
  - [方枝樹科](../Page/方枝樹科.md "wikilink")（Oliniaceae）
  - [柳葉菜科](../Page/柳葉菜科.md "wikilink")（Onagraceae）
  - [管萼木科](../Page/管萼木科.md "wikilink")（Penaeaceae）
  - [裸木科](../Page/裸木科.md "wikilink")（Psiloxylaceae）
  - [喙萼花科](../Page/喙萼花科.md "wikilink")（Rhynchocalycaceae）
  - [蠟燭樹科](../Page/蠟燭樹科.md "wikilink")（Vochysiaceae）

##### 目未定

  - [單果樹科](../Page/單果樹科.md "wikilink")（Aphloiaceae）
  - [四棱果科](../Page/四棱果科.md "wikilink")（Geissolomataceae）
  - [西兰木科](../Page/西兰木科.md "wikilink")（Ixerbaceae）
  - [美洲苦木科](../Page/美洲苦木科.md "wikilink")（Picramniaceae）
  - [栓皮果科](../Page/栓皮果科.md "wikilink")（Strasburgeriaceae）
  - [葡萄科](../Page/葡萄科.md "wikilink")（Vitaceae）
  - [蒜樹科](../Page/蒜樹科.md "wikilink")（Huaceae）
  - [刺球果科](../Page/刺球果科.md "wikilink")（Krameriaceae）
  - [蒺藜科](../Page/蒺藜科.md "wikilink")（Zygophyllaceae）
  - [清風藤科](../Page/清風藤科.md "wikilink")（Sabiaceae）
  - [水青樹科](../Page/水青樹科.md "wikilink")（Tetracentraceae）
  - [昆欄樹科](../Page/昆欄樹科.md "wikilink")（Trochodendraceae）
  - [鱗枝樹科](../Page/鱗枝樹科.md "wikilink")（Aextoxicaceae）
  - [離花科](../Page/離花科.md "wikilink")（Apodanthaceae）
  - [智利藤科](../Page/智利藤科.md "wikilink")（Berberidopsidaceae）
  - [五椏果科](../Page/五椏果科.md "wikilink")（Dilleniaceae）
  - [刻球花科](../Page/刻球花科.md "wikilink")（Bruniaceae）
  - [彎藥樹科](../Page/彎藥樹科.md "wikilink")（Columelliaceae）
  - [漠茸草科](../Page/漠茸草科.md "wikilink")（Eremosynaceae）
  - [南鼠刺科](../Page/南鼠刺科.md "wikilink")（Escalloniaceae）
  - [八蕊樹科](../Page/八蕊樹科.md "wikilink")（Paracryphiaceae）
  - [多香木科](../Page/多香木科.md "wikilink")（Polyosmaceae）
  - [楔蕊花科](../Page/楔蕊花科.md "wikilink")（Sphenostemonaceae）
  - [智利木科](../Page/智利木科.md "wikilink")（Tribelaceae）
  - [紫草科](../Page/紫草科.md "wikilink")（Boraginaceae）
  - [茶茱萸科](../Page/茶茱萸科.md "wikilink")（Icacinaceae）
  - [五蕊茶科](../Page/五蕊茶科.md "wikilink")（Oncothecaceae）
  - [二歧草科](../Page/二歧草科.md "wikilink")（Vahliaceae）

### [單子葉植物綱](../Page/單子葉植物綱.md "wikilink")（Liliopsida）（百合綱）

#### [菖蒲目](../Page/菖蒲目.md "wikilink")(Acorales)

  - [菖蒲科](../Page/菖蒲科.md "wikilink")（Acoraceae）

#### [澤瀉目](../Page/澤瀉目.md "wikilink")(Alismatales)

  - [澤瀉科](../Page/澤瀉科.md "wikilink")（Alismataceae）
  - [水蕹科](../Page/水蕹科.md "wikilink")（Aponogetonaceae）
  - [天南星科](../Page/天南星科.md "wikilink")（Araceae）
  - [花藺科](../Page/花藺科.md "wikilink")（Butomaceae）
  - [絲粉藻科](../Page/絲粉藻科.md "wikilink")（Cymodoceaceae）
  - [水鱉科](../Page/水鱉科.md "wikilink")（Hydrocharitaceae）
  - [水麥冬科](../Page/水麥冬科.md "wikilink")（Juncaginaceae）
  - [沼鱉科](../Page/沼鱉科.md "wikilink")（Limnocharitaceae）
  - [海王草科](../Page/海王草科.md "wikilink")（Posidoniaceae）
  - [眼子菜科](../Page/眼子菜科.md "wikilink")（Potamogetonaceae）
  - [流蘇菜科](../Page/流蘇菜科.md "wikilink")（Ruppiaceae）
  - [冰沼草科](../Page/冰沼草科.md "wikilink")（Scheuchzeriaceae）
  - [巖菖蒲科](../Page/巖菖蒲科.md "wikilink")（Tofieldiaceae）
  - [大葉藻科](../Page/大葉藻科.md "wikilink")（Zosteraceae）

#### [天門冬目](../Page/天門冬目.md "wikilink")(Asparagales)

  - [百子蓮科](../Page/百子蓮科.md "wikilink")（Agapanthaceae）
  - [龍舌蘭科](../Page/龍舌蘭科.md "wikilink")（Agavaceae）
  - [蔥科](../Page/蔥科.md "wikilink")（Alliaceae）
  - [石蒜科](../Page/石蒜科.md "wikilink")（Amaryllidaceae）
  - [星捧月科](../Page/星捧月科.md "wikilink")（Aphyllanthaceae）
  - [天門冬科](../Page/天門冬科.md "wikilink")（Asparagaceae）
  - [日光蘭科](../Page/日光蘭科.md "wikilink")（Asphodelaceae）
  - [芳香草科](../Page/芳香草科.md "wikilink")（Asteliaceae）
  - [香水花科](../Page/香水花科.md "wikilink")（Blandfordiaceae）
  - [澳韭兰科](../Page/澳韭兰科.md "wikilink")（Boryaceae）
  - [矛花科](../Page/矛花科.md "wikilink")（Doryanthaceae）
  - [萱草科](../Page/萱草科.md "wikilink")（Hemerocallidaceae）
  - [西麗草科](../Page/西麗草科.md "wikilink")（Hesperocallidaceae）
  - [風信子科](../Page/風信子科.md "wikilink")（Hyacinthaceae）
  - [仙茅科](../Page/仙茅科.md "wikilink")（Hypoxidaceae）
  - [鳶尾科](../Page/鳶尾科.md "wikilink")（Iridaceae）
  - [鳶尾蒜科](../Page/鳶尾蒜科.md "wikilink")（Ixiolirionaceae）
  - [毛石蒜科](../Page/毛石蒜科.md "wikilink")（Lanariaceae）
  - [異蕊草科](../Page/異蕊草科.md "wikilink")（Laxmanniaceae）
  - [蘭科](../Page/蘭科.md "wikilink")（Orchidaceae）
  - [假葉樹科](../Page/假葉樹科.md "wikilink")（Ruscaceae）
  - [藍星科](../Page/藍星科.md "wikilink")（Tecophilaeaceae）
  - [紫燈花科](../Page/紫燈花科.md "wikilink")（Themidaceae）
  - [刺葉樹科](../Page/刺葉樹科.md "wikilink")（Xanthorrhoeaceae）
  - [血剑草科](../Page/血剑草科.md "wikilink")（Xeronemataceae）

#### [薯蕷目](../Page/薯蕷目.md "wikilink")(Dioscoreales)

  - [水玉簪科](../Page/水玉簪科.md "wikilink")（Burmanniaceae）
  - [薯蕷科](../Page/薯蕷科.md "wikilink")（Dioscoreaceae）
  - [金光花科](../Page/金光花科.md "wikilink")（Nartheciaceae）

#### [百合目](../Page/百合目.md "wikilink")(Liliales)

  - [六出花科](../Page/六出花科.md "wikilink")（Alstroemeriaceae）
  - [金梅草科](../Page/金梅草科.md "wikilink")（Campynemataceae）
  - [秋水仙科](../Page/秋水仙科.md "wikilink")（Colchicaceae）
  - [美麗腐草科](../Page/美麗腐草科.md "wikilink")（Corsiaceae）
  - [百合科](../Page/百合科.md "wikilink")（Liliaceae）
  - [菝葜木科](../Page/菝葜木科.md "wikilink")（Luzuriagaceae）
  - [黑藥花科](../Page/黑藥花科.md "wikilink")（Melanthiaceae）
  - [刺藤科](../Page/刺藤科.md "wikilink")（Petermanniaceae）
  - [垂花科](../Page/垂花科.md "wikilink")（Philesiaceae）
  - [菝葜藤科](../Page/菝葜藤科.md "wikilink")（Ripogonaceae）
  - [菝葜科](../Page/菝葜科.md "wikilink")（Smilacaceae）

#### [露兜樹目](../Page/露兜樹目.md "wikilink")(Pandanales)

  - [環花草科](../Page/環花草科.md "wikilink")（Cyclanthaceae）
  - [露兜樹科](../Page/露兜樹科.md "wikilink")（Pandanaceae）
  - [百部科](../Page/百部科.md "wikilink")（Stemonaceae）
  - [黴草科](../Page/黴草科.md "wikilink")（Triuridaceae）
  - [翡若翠科](../Page/翡若翠科.md "wikilink")（Velloziaceae）

#### [棕櫚目](../Page/棕櫚目.md "wikilink")(Arecales)

  - [棕櫚科](../Page/棕櫚科.md "wikilink")（Arecaceae）

#### [鴨蹠草目](../Page/鴨蹠草目.md "wikilink")(Commelinales)

  - [鴨蹠草科](../Page/鴨蹠草科.md "wikilink")（Commelinaceae）
  - [血皮草科](../Page/血皮草科.md "wikilink")（Haemodoraceae）
  - [匍莖草科](../Page/匍莖草科.md "wikilink")（Hanguanaceae）
  - [田蔥科](../Page/田蔥科.md "wikilink")（Philydraceae）
  - [雨久花科](../Page/雨久花科.md "wikilink")（Pontederiaceae）

#### [禾本目](../Page/禾本目.md "wikilink")(Poales)

  - [苞穗草科](../Page/苞穗草科.md "wikilink")（Anarthriaceae）
  - [鳳梨科](../Page/鳳梨科.md "wikilink")（Bromeliaceae）
  - [刺鱗草科](../Page/刺鱗草科.md "wikilink")（Centrolepidaceae）
  - [莎草科](../Page/莎草科.md "wikilink")（Cyperaceae）
  - [二柱草科](../Page/二柱草科.md "wikilink")（Ecdeiocoleaceae）
  - [穀精草科](../Page/穀精草科.md "wikilink")（Eriocaulaceae）
  - [鬚葉藤科](../Page/鬚葉藤科.md "wikilink")（Flagellariaceae）
  - [排水草科](../Page/排水草科.md "wikilink")（Hydatellaceae）
  - [假蘆葦科](../Page/假蘆葦科.md "wikilink")（Joinvilleaceae）
  - [燈心草科](../Page/燈心草科.md "wikilink")（Juncaceae）
  - [花水蘚科](../Page/花水蘚科.md "wikilink")（Mayacaceae）
  - [禾本科](../Page/禾本科.md "wikilink")（Poaceae）
  - [偏穗草科](../Page/偏穗草科.md "wikilink")（Rapateaceae）
  - [帚燈草科](../Page/帚燈草科.md "wikilink")（Restionaceae）
  - [黑三棱科](../Page/黑三棱科.md "wikilink")（Sparganiaceae）
  - [硅草科](../Page/硅草科.md "wikilink")（Thurniaceae）
  - [香蒲科](../Page/香蒲科.md "wikilink")（Typhaceae）
  - [黃眼草科](../Page/黃眼草科.md "wikilink")（Xyridaceae）

#### [薑目](../Page/薑目.md "wikilink")(Zingiberales)

  - [美人蕉科](../Page/美人蕉科.md "wikilink")（Cannaceae）
  - [閉鞘薑科](../Page/閉鞘薑科.md "wikilink")（Costaceae）
  - [蠍尾蕉科](../Page/蠍尾蕉科.md "wikilink")（Heliconiaceae）
  - [蘭花蕉科](../Page/蘭花蕉科.md "wikilink")（Lowiaceae）
  - [竹芋科](../Page/竹芋科.md "wikilink")（Marantaceae）
  - [芭蕉科](../Page/芭蕉科.md "wikilink")（Musaceae）
  - [旅人蕉科](../Page/旅人蕉科.md "wikilink")（Strelitziaceae）
  - [薑科](../Page/薑科.md "wikilink")（Zingiberaceae）

#### 目未定

  - [多鬚草科](../Page/多鬚草科.md "wikilink")（Dasypogonaceae）
  - [無葉蓮科](../Page/無葉蓮科.md "wikilink")（Petrosaviaceae）

## 外部連結

  - [The NCBI Taxonomy](http://www.ncbi.nlm.nih.gov/Taxonomy/)

[Category:植物](../Category/植物.md "wikilink")