\[\[[File:Vegetation-no-legend.PNG|thumb|400px|right|世界植被分區圖](File:Vegetation-no-legend.PNG%7Cthumb%7C400px%7Cright%7C世界植被分區圖)

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

\]\]

**植被**是[地球表面所覆盖的](../Page/地球.md "wikilink")[植物的总称](../Page/植物.md "wikilink")。它是一个[植物学](../Page/植物学.md "wikilink")、[生态学](../Page/生态学.md "wikilink")、[农学和](../Page/农学.md "wikilink")[地球科学的名词](../Page/地球科学.md "wikilink")。

植被可以因为生长环境的不同而被分类，譬如[高山植被](../Page/高山植被.md "wikilink")、[草原植被](../Page/草原植被.md "wikilink")、[海岛植被等](../Page/海岛植被.md "wikilink")。

環境因素如[光照](../Page/太阳光.md "wikilink")、[溫度和](../Page/溫度.md "wikilink")[雨量等會影響植物的生長和分布](../Page/雨量.md "wikilink")，因此形成了不同的植被。

## [熱帶雨林](../Page/熱帶雨林.md "wikilink")

[Forest_on_Barro_Colorado.png](https://zh.wikipedia.org/wiki/File:Forest_on_Barro_Colorado.png "fig:Forest_on_Barro_Colorado.png")的熱帶雨林\]\]
多位於[低緯度](../Page/低緯度.md "wikilink")[熱帶地區](../Page/熱帶.md "wikilink")，南北緯約10度之間，如[南美洲](../Page/南美洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[東南亞附近的熱帶雨林](../Page/東南亞.md "wikilink")。雨量充足，年雨量在2,000公厘以上，無明顯乾溼季之分；溫差小，平均溫約攝氏25
\~
27度。以常綠闊葉樹為主。森林層次複雜，可以分成[地被層](../Page/地被層.md "wikilink")、[草本層](../Page/草本層.md "wikilink")、[灌木層](../Page/灌木層.md "wikilink")、[亞喬木層](../Page/亞喬木層.md "wikilink")、[喬木層和](../Page/喬木層.md "wikilink")[樹冠層等](../Page/樹冠層.md "wikilink")。為爭取陽光，有些樹種可以長到80公尺以上。

## 熱帶旱林

[Chacachacare_dry_forest_3.JPG](https://zh.wikipedia.org/wiki/File:Chacachacare_dry_forest_3.JPG "fig:Chacachacare_dry_forest_3.JPG")的熱帶旱林\]\]
位於緯度約10 \~
25度之間，主要分布於熱帶雨林南北兩側，如非洲、南美洲雨林的南北兩側及[印度的森林](../Page/印度.md "wikilink")。和熱帶雨林比較，熱帶旱林有明顯的乾、溼季之分；隨著季節變化，林相會做出不同的改變。乾季時，樹木的大部份樹葉會掉光，以減少水份散失；當雨季來臨時，充沛的雨量會迅速將森林重新綠化起來。

## [熱帶草原](../Page/熱帶草原.md "wikilink")

[Oldoinyolengai.jpg](https://zh.wikipedia.org/wiki/File:Oldoinyolengai.jpg "fig:Oldoinyolengai.jpg")北部的熱帶草原\]\]
位於緯度約10 \~
20度之間，但較熱帶旱林更為乾燥，如非洲的疏林草原。由於雨季短、乾季長，使得森林不易生長，而形成草原的自然景觀。雨季多在夏季，其餘時間通常久旱無雨。時常發生的火災使得該區沒有茂密的森林，草地在火災過後很快生成，留存下來的樹木通常有著相當程度的防火能力。

## [荒漠](../Page/荒漠.md "wikilink")

[Kalahari_E02_00.jpg](https://zh.wikipedia.org/wiki/File:Kalahari_E02_00.jpg "fig:Kalahari_E02_00.jpg")\]\]
荒漠主要分為兩個區域，一個為北緯30度，另一個為南緯30度。其主要分布於大陸內陸區或背風的山區，雨量在300公厘以下，[蒸發量大於降水量](../Page/蒸發.md "wikilink")，日夜溫差大，如[撒哈拉沙漠](../Page/撒哈拉沙漠.md "wikilink")、[戈壁沙漠等](../Page/戈壁沙漠.md "wikilink")。隨著地點的不同，降雨量和氣溫也有很大的不同，如智利[阿塔卡马沙漠位於熱帶](../Page/阿塔卡马沙漠.md "wikilink")，且幾乎全年無雨，北美的[索诺兰沙漠卻有每年近](../Page/索诺兰沙漠.md "wikilink")300公釐的雨量，而蒙古的戈壁沙漠冬季均溫則可降至攝氏零下20度。主要植物為[仙人掌](../Page/仙人掌.md "wikilink")、矮灌木及草本植物等耐旱植物。

## [地中海灌木林](../Page/地中海灌木林.md "wikilink")

[Flowering-garrigue4.JPG](https://zh.wikipedia.org/wiki/File:Flowering-garrigue4.JPG "fig:Flowering-garrigue4.JPG")的硬葉灌木地\]\]
分布於南北緯30 \~
40度左右的地區，夏乾熱，秋冬春涼且潮溼，全年氣候溫和，如[地中海地區](../Page/地中海.md "wikilink")、美國[加州](../Page/加州.md "wikilink")、南非[開普敦地區等](../Page/開普敦.md "wikilink")。主要植物以硬葉灌木植物為主。夏季的旱季時常發生火災，因此此區的樹木都有相當的防火能力；灌木則生長週期短暫，在濕季迅速生長，旱季死亡，以避開旱災和火災。

## [溫帶草原](../Page/溫帶草原.md "wikilink")

[Konza1.jpg](https://zh.wikipedia.org/wiki/File:Konza1.jpg "fig:Konza1.jpg")的溫帶草原\]\]
分布緯度與溫帶森林相似，但年雨量較少，約300 \~
1,000公厘，如美國中西部、[阿根廷及](../Page/阿根廷.md "wikilink")[東歐](../Page/東歐.md "wikilink")、[中亞至](../Page/中亞.md "wikilink")[蒙古的大草原](../Page/蒙古.md "wikilink")。降雨量不足以使樹林生長，但也不致於形成沙漠，主要植物以[禾本科及](../Page/禾本科.md "wikilink")[菊科等](../Page/菊科.md "wikilink")[草本植物為主](../Page/草本植物.md "wikilink")。

## [溫帶森林](../Page/溫帶森林.md "wikilink")

[Autumn_leaves,_Talcott_Mountain_State_Park.jpg](https://zh.wikipedia.org/wiki/File:Autumn_leaves,_Talcott_Mountain_State_Park.jpg "fig:Autumn_leaves,_Talcott_Mountain_State_Park.jpg")的溫帶森林\]\]
位於緯度約30 \~ 55度之間，然而，[生物群系主要分布緯度](../Page/生物群系.md "wikilink")40 \~
50度之間，年雨量約650 \~
3,000公厘以上，如[東亞](../Page/東亞.md "wikilink")、[西歐及](../Page/西歐.md "wikilink")[美國東部的溫帶闊葉林](../Page/美國.md "wikilink")。以闊葉樹為主，如[楓](../Page/楓.md "wikilink")、[橡](../Page/橡.md "wikilink")、[榆等](../Page/榆.md "wikilink")。樹葉在秋、冬季時會凋落，以減少水份及能量喪失，因此被稱為落葉樹。

這個地區是受到人類影響最大的地方。數千年來，人類砍伐樹木，發展[農業](../Page/農業.md "wikilink")。今天，世界大部份的溫帶原始森林均已消失；在北美西部，只有1
\~ 2%的溫帶原始森林仍然留存。

## [北方針葉林](../Page/北方針葉林.md "wikilink")

[Mixed_Picea_(Spruce)_forest_from_Vestfold_county_in_Norway.jpg](https://zh.wikipedia.org/wiki/File:Mixed_Picea_\(Spruce\)_forest_from_Vestfold_county_in_Norway.jpg "fig:Mixed_Picea_(Spruce)_forest_from_Vestfold_county_in_Norway.jpg")的針葉林\]\]
分布於高緯度地區，如[歐亞大陸北部及](../Page/歐亞大陸.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")、[阿拉斯加的針葉林](../Page/阿拉斯加.md "wikilink")。冬季長而嚴寒、生長季短。年雨量約200
\~
600公厘。以[裸子植物為主](../Page/裸子植物.md "wikilink")，如[松](../Page/松.md "wikilink")、[柏](../Page/柏.md "wikilink")、[杉等](../Page/杉.md "wikilink")。終年常綠。樹葉呈針狀，可以減少水份散失。

## [苔原](../Page/苔原.md "wikilink")

[Wrangel_Island_tundra.jpg](https://zh.wikipedia.org/wiki/File:Wrangel_Island_tundra.jpg "fig:Wrangel_Island_tundra.jpg")的苔原\]\]
又稱為寒原或凍原。分布於極地高緯度地區。降水量低、生長季短。主要植物為[苔蘚](../Page/苔蘚.md "wikilink")、[地衣及矮小灌木等](../Page/地衣.md "wikilink")。夏季短，植物在短暫的生長季節迅速開花結果。

## [山地](../Page/山地.md "wikilink")

[Mt._Karasawa_(200608).jpg](https://zh.wikipedia.org/wiki/File:Mt._Karasawa_\(200608\).jpg "fig:Mt._Karasawa_(200608).jpg")[涸沢岳附近的高山植物](../Page/涸沢岳.md "wikilink")\]\]
山地的環境和植被隨著[海拔的變化而有所不同](../Page/海拔.md "wikilink")。海拔越高，氣溫也越低。一些低緯度地區的高山上，常可以見到類似高緯度地區的植被。

在年平均溫度低於10℃的高山地帶，樹木不易生長，在森林所能到達的最高海拔，稱為[林線](../Page/林線.md "wikilink")。高山地區由於常年低溫、地形陡峭、表面土壤稀少、強風吹襲、烈日照射等自然環境因素，不適合樹木生長，因此[高山植物大多為矮小的](../Page/高山植物.md "wikilink")[灌木或](../Page/灌木.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")。由於生長季節短、動物媒介少，高山寒原的植物大多具有鮮豔的花朵或果實，以吸引動物媒介傳播花粉、種子。

## 延伸閱讀

  - Breckle, S.-W. 2002. *Walter's Vegetation of the Earth*. 4th ed. New
    York: Springer-Verlag.

## 另見

  - [生物圈](../Page/生物圈.md "wikilink")

[植被](../Category/植被.md "wikilink")
[Category:生態學](../Category/生態學.md "wikilink")
[Category:植物學](../Category/植物學.md "wikilink")
[Category:地球科学](../Category/地球科学.md "wikilink")