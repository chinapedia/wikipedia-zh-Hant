**橢圓星系**（Elliptical
galaxy）是[哈伯](../Page/星系分類.md "wikilink")[星系分類中的一種類型](../Page/星系.md "wikilink")，具有下列的物理特徵：
[NGC4881_HST_1995-07.jpg](https://zh.wikipedia.org/wiki/File:NGC4881_HST_1995-07.jpg "fig:NGC4881_HST_1995-07.jpg")（在左上方的球狀發光體）\]\]

  - 恆星的運動是以不規則的運動為主，不同於[漩渦星系的以自轉運動為主](../Page/漩渦星系.md "wikilink")，只有少許的不規則運動。
  - 只有少許的[星際物質](../Page/星際物質.md "wikilink")、年輕的恆星很少、[疏散星團的數量也不多](../Page/疏散星團.md "wikilink")。
  - 恆星多是年老的，屬於[第二星族的恆星](../Page/星族.md "wikilink")。
  - 較大的橢圓星系，都有以老年恆星為主的[球狀星團](../Page/球狀星團.md "wikilink")。

橢圓星系的傳統形象是最初的爆發之後，[恆星形成過程已經結束的星系](../Page/恆星形成.md "wikilink")，只留下衰老中的恆星仍在閃爍著光輝，但偶爾仍會有少量的[恆星形成](../Page/恆星形成.md "wikilink")。通常，橢圓星系看起來是黃色或紅色，與在旋臂上有高熱的年輕恆星，發出淡藍色調的[螺旋星系對比有很大的差異](../Page/旋渦星系.md "wikilink")。

橢圓星系的質量和尺度有很大的範圍：从几百[秒差距到十萬](../Page/秒差距.md "wikilink")[秒差距不等](../Page/秒差距.md "wikilink")，質量從10<sup>7</sup>到接近10<sup>13</sup>太陽質量。最小的[矮橢球星系可能不會比典型的](../Page/矮橢球星系.md "wikilink")[球狀星團大](../Page/球狀星團.md "wikilink")，但因為擁有相當數量的[暗物質](../Page/暗物質.md "wikilink")，所以不能歸類為星團。大部份這些小的橢圓星系都與其它的橢圓星系沒有關聯性。已知最大的單一星系[M87](../Page/M87.md "wikilink")（NGC4486）是橢圓星系。橢圓星系的尺度比任何其他種類的星系都更為寬廣。

橢圓球的形狀曾經被認為是球體被拉長的程度不同所造成的，所以[哈伯星系分類將橢圓星系依照扁率從非常接近球狀的E](../Page/星系分類.md "wikilink")0，到非常扁平的E7。現在則認為橢圓星系的扁平率都相差不大，哈伯分類上的差異只是觀測的角度不同造成的結果。

橢圓星系有兩種不同的物理類型："盒狀的"巨大橢圓星系，是因為有些區域的不規則運動（非對稱的隨機運動）比其他的區域明顯造成的；和"盤狀的"普通大小和低亮度，有著各項同性的隨機運動，並且被星系自轉拉平的橢圓星系。

[矮橢圓星系可能不是真的橢圓星系](../Page/矮橢圓星系.md "wikilink")，她們的一些特徵像是不規則星系與晚期的旋渦星系，許多天文學家現在因此稱呼她們為「矮橢球」（注意：這仍然是有爭議的題目）。

橢圓形和星系盤的核心突起有相似的特徵，並且一般都視為相同的物理現象。橢圓星系傾向集中在[星系團的核心和存在於](../Page/星系團.md "wikilink")[緊密集團的星系](../Page/緊密集團.md "wikilink")。最近的一些觀測發現少數的橢圓星系中存在著藍色的年輕[星團](../Page/星團.md "wikilink")，這可以解釋為星系的發展和演變中發生[星系合併的現象](../Page/星系誕生和演化.md "wikilink")。目前的想法是：橢圓星系是兩個形態可能不同，但質量相當的星系發生碰撞與經歷長期合併作用的結果。

這種型態的[星系合併在早期應該是很普遍的現象](../Page/星系誕生和演化.md "wikilink")，而且發生的頻率比現在高。較小的[星系合併涉及兩個質量非常不同的星系](../Page/星系誕生和演化.md "wikilink")，並且橢圓星系的質量是沒有限制的。例如，我們的[銀河系現在正在合併一個小的星系](../Page/銀河系.md "wikilink")（人馬座矮星系）。

## 橢圓星系的例子

  - [M32](../Page/M32.md "wikilink")
  - [M49](../Page/M49.md "wikilink")
  - [M59](../Page/M59.md "wikilink")
  - [M60](../Page/M60_\(橢圓星系\).md "wikilink")（NGC 4649）
  - [M87](../Page/M87.md "wikilink")（NGC 4486）
  - [M89](../Page/M89.md "wikilink")
  - [M105](../Page/M105.md "wikilink")（NGC 3379）
  - [M110](../Page/M110.md "wikilink")

## 參見

  - [活躍星系](../Page/活躍星系.md "wikilink")
  - [棒旋星系](../Page/棒旋星系.md "wikilink")
  - [矮星系](../Page/矮星系.md "wikilink")
  - [矮橢圓星系](../Page/矮橢圓星系.md "wikilink")
  - [矮橢球星系](../Page/矮橢球星系.md "wikilink")
  - [星系分類](../Page/星系分類.md "wikilink")
  - [星系誕生和演化](../Page/星系誕生和演化.md "wikilink")
  - [星系集團](../Page/星系集團.md "wikilink")
  - [不規則星系](../Page/不規則星系.md "wikilink")
  - [透鏡星系](../Page/透鏡星系.md "wikilink")
  - 星系表
  - [最近的星系列表](../Page/最近的星系列表.md "wikilink")
  - [環狀星系](../Page/環星系.md "wikilink")
  - [螺旋星系](../Page/螺旋星系.md "wikilink")
  - [星爆星系](../Page/星爆星系.md "wikilink")
  - [西佛星系](../Page/西佛星系.md "wikilink")
  - [星系、星系團、大尺度結構年表](../Page/星系、星系團、大尺度結構年表.md "wikilink")

## 外部連結

  - [Elliptical
    Galaxies](https://web.archive.org/web/20060809193224/http://www.seds.org/messier/elli.html)，SEDS
    Messier pages
  - [Elliptical
    Galaxies](http://csep10.phys.utk.edu/astr162/lect/galaxies/elliptical.html)

[\*](../Category/橢圓星系.md "wikilink")