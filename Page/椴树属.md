**椴树属**（[学名](../Page/学名.md "wikilink")：）植物统称**椴树**，属于[锦葵目](../Page/锦葵目.md "wikilink")[锦葵科](../Page/锦葵科.md "wikilink")[椴树亚科](../Page/椴树亚科.md "wikilink")，旧时分类于[椴樹科](../Page/椴樹科.md "wikilink")（）或田麻科。

## 形态

椴树是落叶[乔木](../Page/乔木.md "wikilink")，高可达30米，直径可达1米；单叶互生，常有星状毛或单毛，有锯齿或缺齿，通常基部斜歪；两性花，复聚伞花序；明顯的特徵是在总柄之基部与膜质、舌状的苞片合生；[坚果或](../Page/坚果.md "wikilink")[浆果](../Page/浆果.md "wikilink")。

## 分布

椴树只分布在北半球；分布范围包括[欧洲](../Page/欧洲.md "wikilink")，亞洲和北美東部，而以亞洲的品種最多。

## 疾病与虫害

城市里生长在路边的椴树会经常被[椴叶螨](../Page/椴叶螨.md "wikilink")（*Eotetranychus
tiliarum*）侵袭。严重的话椴树树叶会早在七月份就全部脱落。这些路边的椴树也会经常被[小椴树叶蜂](../Page/小椴树叶蜂.md "wikilink")（*Caliroa
annulipes*）侵扰。

## 文化

椴树被[日耳曼人尊奉为爱情与幸运女神](../Page/日耳曼人.md "wikilink")[弗蕾亞](../Page/弗蕾亞.md "wikilink")。以前中欧的很多地方每个村落中心都有一棵椴树，椴树下经常是聚会、信息交流或者是举行婚礼的地点。五月初大部分的舞蹈节都在椴树下举行。

由於日耳曼人在椴樹下舉行集會（Thing）的傳統，这里也经常成为村庄法院，所以椴树也常被稱作“法院树”或者“法院椴树”。
与[欧洲橡木相对](../Page/欧洲橡木.md "wikilink")，椴树常被认为是女性的植物，由于其名字与德语"柔和"一词"lind"音近。在日耳曼人心中，椴树是神圣的。

瑞典著名博物学家、“植物学之父”[林奈](../Page/林奈.md "wikilink") (Carl von
Linné)，其姓氏也是来自瑞典语的椴树（lind）。而德國的[萊比錫](../Page/萊比錫.md "wikilink")、俄國的[利佩茨克和拉脫維亞的](../Page/利佩茨克.md "wikilink")[利耶帕亞三市的名稱在各自的語言中都是](../Page/利耶帕亞.md "wikilink")「椴樹」的意思。其中後兩者的市徽都有椴樹的圖案。

## 用途

[DorflindeHerzogenreuth.jpg](https://zh.wikipedia.org/wiki/File:DorflindeHerzogenreuth.jpg "fig:DorflindeHerzogenreuth.jpg")
椴木木质软，易切割，是制作木质餐具、碟子、碗和其他家用物品的主要木材。椴树叶是一种[草药](../Page/草药.md "wikilink")，芽及嫩葉均可生食。椴樹的花有香味，可用作草藥或[草本茶](../Page/草本茶.md "wikilink")，重瓣的品種則通常用於製造[香水](../Page/香水.md "wikilink")。椴樹同時是[蜂蜜的主要来源之一](../Page/蜂蜜.md "wikilink")，其蜜糖色淺而味道濃郁。藥用的椴樹通常以小葉椴為主，因其有效成份的濃度較高。

## 分类

[缩略图](https://zh.wikipedia.org/wiki/File:Bombus_terrestris_queen_-_Tilia_cordata_-_Keila.jpg "fig:缩略图")
本属大约有46个种，以下是其中的部分：

  - [美洲椴树](../Page/美洲椴树.md "wikilink") (*Tilia americana* )
  - [紫椴](../Page/紫椴.md "wikilink") (*Tilia amurensis*)
  - *Tilia caucasica*
  - [长苞椴](../Page/长苞椴.md "wikilink") (*Tilia chenmoui*)
  - [華椴](../Page/華椴.md "wikilink") (*Tilia chinensis*)
  - *Tilia chingiana*
  - [小叶椴](../Page/小叶椴.md "wikilink") (*Tilia cordata*)
  - *Tilia* 'Harold Hillier'
  - [毛糯米椴](../Page/毛糯米椴.md "wikilink") (*Tilia henryana*)
  - [湖北毛椴](../Page/湖北毛椴.md "wikilink") (*Tilia hupehensis*)
  - *Tilia insularis*
  - *Tilia intonsa*
  - [华东椴](../Page/华东椴.md "wikilink") (*Tilia japonica*)
  - *Tilia kiusiana*
  - *Tilia koreana*
  - *Tilia laetevirens*
  - *Tilia ledbourii*
  - [辽椴](../Page/辽椴.md "wikilink") (*Tilia mandshurica*)
  - *Tilia maximowicziana*
  - *Tilia mexicana*
  - [南京椴](../Page/南京椴.md "wikilink") (*Tilia miqueliana*)
  - *Tilia* 'Moltkei'
  - [蒙椴](../Page/蒙椴.md "wikilink") (*Tilia mongolica*)
  - [粉椴](../Page/粉椴.md "wikilink") (*Tilia oliveri*)
  - [石椴](../Page/石椴.md "wikilink") (*Tilia parvifolia*)
  - *Tilia paucicostata*
  - *Tilia petiolaris*
  - [阔叶椴](../Page/阔叶椴.md "wikilink") (*Tilia platyphyllos*)
  - *Tilia tarquetii*
  - [銀毛椴](../Page/銀毛椴.md "wikilink") (*Tilia tomentosa*)
  - *Tilia tuan*
  - [克里米亚椴树](../Page/克里米亚椴树.md "wikilink") (*Tilia x euchlora*)
  - [荷兰椴树](../Page/荷兰椴树.md "wikilink") (*Tilia x intermedia, T. x
    europaea, T. x vulgaris*)
  - *Tilia x varsaviensis*

## 图集

## 参考文献

[D](../Page/分類:蜜源植物.md "wikilink")

[Category:椴树属](../Category/椴树属.md "wikilink")
[Category:树](../Category/树.md "wikilink")
[Category:中草藥](../Category/中草藥.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")