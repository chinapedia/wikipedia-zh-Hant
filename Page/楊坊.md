**楊坊**（）[字](../Page/表字.md "wikilink")**啟堂**，又字**憩棠**，[浙江](../Page/浙江.md "wikilink")[寧波](../Page/寧波.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")，富商；清朝官吏。
曾命美國人[華飛烈](../Page/華飛烈.md "wikilink")（Frederick
Ward）組織[常胜军](../Page/常胜军.md "wikilink")，抵抗太平軍。

## 生平

杨坊（1810－1865）浙江[鄞县人](../Page/鄞县.md "wikilink")。早年在[宁波当绸布店店员](../Page/宁波.md "wikilink")，后入教会学校习英语，继因[赌博欠债流浪到](../Page/赌博.md "wikilink")[上海](../Page/上海.md "wikilink")，1843年到[上海從商](../Page/上海.md "wikilink")，初於上海最大的洋行——
英國[怡和洋行](../Page/怡和洋行.md "wikilink")[買辦](../Page/買辦.md "wikilink")，也因為[英文造詣佳](../Page/英文.md "wikilink")，也教授上海地區清朝官吏英語；後任「四明公所」（公司）[董事](../Page/董事.md "wikilink")，蘇松太道[觀察使兼署](../Page/觀察使.md "wikilink")（代理）江蘇[布政使](../Page/布政使.md "wikilink")[吳煦搭擋](../Page/吳煦.md "wikilink")，

1853年（咸丰三年）9月上海[小刀会起事](../Page/小刀会.md "wikilink")，占领县城，俘虜[苏松太道](../Page/苏松太道.md "wikilink")[觀察使](../Page/觀察使.md "wikilink")[吴健彰后](../Page/吴健彰.md "wikilink")，伙同美商裴福洋行将吴从囚禁地，偷偷救出藏于[租界内新银号钱庄](../Page/租界.md "wikilink")。

翌年10月，受江苏巡抚[吉尔杭阿命与英](../Page/吉尔杭阿.md "wikilink")、法两国[领事密谋策划镇压](../Page/领事.md "wikilink")[小刀会起义军](../Page/小刀会.md "wikilink")，在城外修筑围墙，切断城内外交通，断绝起义军接济，终使起义军弃城突围归于失败。由同知升为[道员](../Page/道员.md "wikilink")。1856年加盐运使衔。

第二次[鸦片战争期间](../Page/鸦片战争.md "wikilink")，常将从外国商人处了解的英、法联军的意图转报清廷，充当外国侵略者迫使清廷妥协投降的工具。翌年[太平军进军上海](../Page/太平军.md "wikilink")，亲自出面要求法军代守[上海城](../Page/上海城.md "wikilink")，又以美國人[腓特烈·華爾](../Page/腓特烈·華爾.md "wikilink")（Frederick
Ward）為隊長，招[菲律賓人](../Page/菲律賓人.md "wikilink")100餘人為[士卒](../Page/士卒.md "wikilink")，佈防於上海[租界外圍防範](../Page/租界.md "wikilink")[太平軍進襲](../Page/太平軍.md "wikilink")，五月洋槍隊擊退太平軍[李秀成部於](../Page/李秀成.md "wikilink")[松江](../Page/松江.md "wikilink")；楊並將女兒嫁給華爾，改華爾名為[漢名](../Page/漢名.md "wikilink")**華飛烈**，並透過華飛烈之弟至美國購買[軍艦拱衛上海](../Page/軍艦.md "wikilink")，遭到當時江蘇[巡撫](../Page/巡撫.md "wikilink")[李鴻章阻撓](../Page/李鴻章.md "wikilink")。

1862年3月向清廷保举華飛烈入中国籍，并奉命将洋枪队改名“[常胜军](../Page/常胜军.md "wikilink")”，与華飛烈同被委为管带。是年8月華飛烈與太平军的戰鬥中陣亡之後，遭華飛烈继任者[白齐文搶劫](../Page/白齐文.md "wikilink")，庫中4万余[銀元被劫走](../Page/銀元.md "wikilink")，事后，被清廷“暂行革职”。从此郁闷不欢，1865年病死。

## 参考链接

  - 上海市地方志办公室：[五、洋行买办](http://www.shtong.gov.cn/newsite/node2/node2245/node74728/node74749/node74773/node74775/userobject1ai89589.html)

[Y](../Page/category:宁波商帮.md "wikilink")
[F坊](../Page/category:鄞县杨氏.md "wikilink")
[Y楊](../Page/category:中國企業家.md "wikilink")
[Y楊](../Page/category:清朝人.md "wikilink")
[F坊](../Page/category:楊姓.md "wikilink")
[Y楊](../Page/category:鄞县人.md "wikilink")
[Y楊](../Page/category:宁波裔上海人.md "wikilink")

[Y](../Category/甬帮买办.md "wikilink")