**楊天發**（），出生於[日治台灣](../Page/日治台灣.md "wikilink")[臺中州](../Page/臺中州.md "wikilink")[豐原郡](../Page/豐原郡.md "wikilink")[大雅庄](../Page/大雅庄.md "wikilink")（今[臺中市](../Page/臺中市.md "wikilink")[大雅區](../Page/大雅區.md "wikilink")）橫山村，臺灣企業家，為[興農集團創辦人](../Page/興農集團.md "wikilink")，曾經是[中華職棒大聯盟](../Page/中華職棒大聯盟.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")[總裁](../Page/總裁.md "wikilink")。其弟為[長億關係企業與](../Page/長億關係企業.md "wikilink")[朝陽科技大學的創辦人](../Page/朝陽科技大學.md "wikilink")[楊天生](../Page/楊天生.md "wikilink")。

楊天發畢業於[后里農校](../Page/后里農校.md "wikilink")，早年曾經營[農場](../Page/農場.md "wikilink")、以及在[台灣糖業公司工作一段時間](../Page/台灣糖業公司.md "wikilink")。1954年，楊天發與朋友合資開設製冰廠，正式跨足商界。

1957年，楊天發創立[興農](../Page/興農.md "wikilink")；其後興農成為臺灣最大的[農藥製造廠](../Page/農藥.md "wikilink")，並展開[多角化經營](../Page/多角化經營.md "wikilink")，集團版圖橫跨多國以及多種不同的事業。1967年被選為第一屆[十大傑出企業家](../Page/十大傑出企業家.md "wikilink")。

1980年，楊天發獲[美國聯合大學](../Page/美國聯合大學.md "wikilink")[經濟學](../Page/經濟學.md "wikilink")[榮譽博士](../Page/榮譽博士.md "wikilink")。

1988年，興農成立[興農超市](../Page/興農超市.md "wikilink")，成為[臺灣中部頗有規模的](../Page/臺灣中部.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[超市體系](../Page/超市.md "wikilink")。

1990年，楊天發將興農集團交給其子[楊文彬經營](../Page/楊文彬.md "wikilink")，但仍對興農營運方向有很大影響力。

1995年，楊天發買下[中華職棒](../Page/中華職棒.md "wikilink")[俊國熊](../Page/俊國熊.md "wikilink")，改名為[興農熊](../Page/興農熊.md "wikilink")，後又更名為[興農牛](../Page/興農牛.md "wikilink")；該隊於2004年首度獲得中華職棒總冠軍。

2014年3月25日，楊天發逝世，享年93歲。\[1\]

## 參考資料

<references/>

## 外部連結

  - [興農股份有限公司](http://www.sinon.com.tw/)

|- |colspan="3"
style="text-align:center;"|**[中華職業棒球大聯盟](../Page/中華職業棒球大聯盟.md "wikilink")**
|-

[Y](../Category/台灣企業家.md "wikilink")
[Y](../Category/中華職棒會長.md "wikilink")
[Tian](../Category/杨姓.md "wikilink")

1.