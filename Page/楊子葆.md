**楊子葆**（），[中華民國](../Page/中華民國.md "wikilink")（[臺灣](../Page/臺灣.md "wikilink")）政治人物，現任駐愛爾蘭代表。[臺灣外省人第二代](../Page/臺灣外省人.md "wikilink")，[臺大畢業](../Page/臺大.md "wikilink")，取得法國[國立橋路學校](../Page/國立橋路學校.md "wikilink")[博士](../Page/博士.md "wikilink")，為[土木工程學者](../Page/土木工程.md "wikilink")、[葡萄酒專家](../Page/葡萄酒.md "wikilink")、作家。[民主進步黨執政時期](../Page/民主進步黨.md "wikilink")，出任[中華民國外交部政務次長](../Page/中華民國外交部.md "wikilink")、[代理部長](../Page/代理.md "wikilink")、中華民國駐[法國代表](../Page/法國.md "wikilink")（[大使](../Page/大使.md "wikilink")）、[僑委會副委員長](../Page/僑委會.md "wikilink")、[文化部政務次長等要職](../Page/文化部.md "wikilink")；另亦曾任[新竹市副市長](../Page/新竹市.md "wikilink")、[中華航空副總經理](../Page/中華航空.md "wikilink")。

## 簡介

1989年，自[國立臺灣大學土木工程研究所碩士班畢業後](../Page/國立臺灣大學.md "wikilink")，赴法國國立橋路學校攻獲博士學位，曾任職於「巴黎市公共運輸局」。返國後於鼎漢國際工程顧問公司擔任[總工程師](../Page/總工程師.md "wikilink")，並兼任[臺北捷運公司顧問](../Page/臺北捷運公司.md "wikilink")。

1999年，出任[新竹市副市長](../Page/新竹市.md "wikilink")。

2000年，擔任[國際合作發展基金會副秘書長](../Page/國際合作發展基金會.md "wikilink")；2002年，升任秘書長。2003年，當選[中華民國十大傑出青年](../Page/中華民國十大傑出青年.md "wikilink")。2004年，擔任中華民國僑務委員會副委員長；2005年出使[法國](../Page/法國.md "wikilink")，任中華民國駐法國代表；2006年擔任[外交部政務次長](../Page/中華民國外交部.md "wikilink")。

2008年5月6日，時任外交部長[黃志芳為](../Page/黃志芳.md "wikilink")[巴紐外交費遭掮客詐騙侵吞一案負政治責任而請辭](../Page/巴紐案.md "wikilink")，曾以政務次長身份代理[部長職務](../Page/部長.md "wikilink")14天。

2008年8月起，擔任[天主教輔仁大學客座](../Page/天主教輔仁大學.md "wikilink")[教授](../Page/教授.md "wikilink")，2012年2月起兼任該校國際教育長。出版數本捷運、城市與葡萄酒著作，並於臺灣與[香港報章雜誌上主筆城市觀察與飲食文化相關專欄](../Page/香港.md "wikilink")。

2009年9月至2016年6月，曾於國立臺灣大學「領導學程」開設「國際領導能力發展」課程。另2013年1月起至2016年12月，曾於[News98](../Page/News98.md "wikilink")《九八講堂》主持「從捷運看天下」廣播節目。

2013年3月起任職於[中華航空](../Page/中華航空.md "wikilink")，擔任副總經理至2016年5月。

2016年5月20日至2018年6月1日，擔任文化部政務次長。

2018年6月1日起擔任駐愛爾蘭代表。

## 著作

  - 可移動的文化饗宴：72座城市的捷運風景 (1998), ISBN：9578286104
  - 七個小矮人 (譯作, 1999), ISBN：957918481X
  - 看不見的巴黎 (2001), ISBN：986789099X
  - 捷運公共藝術拼圖 (2002), ISBN：986120072X
  - 迷戀捷運 (2002), ISBN：9570406593
  - 都市傳奇 (譯作, 2003), ISBN：986769144X
  - 世界經典捷運建築 (2004), ISBN：9867475224
  - 捷運就在郵票裡 (2004), ISBN：9867475208
  - 街道家具與城市美學 (2005), ISBN：9789867487421
  - 藝術進站 捷運公共藝術 (2005), ISBN：9860008566
  - 葡萄酒文化密碼 (2007), ISBN：9789866165245
  - 文化多樣性與永續發展 (主編, 英文版, Cultural Diversity and Sustainable development,
    2007), ISBN：9789868083547
  - 葡萄酒文化想像 (2009), ISBN：9789867247995
  - 微醺之後, 味蕾之間 (2011), ISBN：9789861209753
  - 葡萄酒ABC (2012), ISBN：9789867120502
  - 軟實力與文化外交 (法文版, Soft power et diplomatie culturelle, 2014),
    ISBN：9782806101655
  - 味無味集 (2014), ISBN：9789865813406
  - 城市的36種表情 (2016), ISBN：9789865722791
  - 喫東西集 (2016), ISBN：9789865813727
  - 味什麼集 (2017), ISBN：9789865813871
  - 不接地氣 (2018), ISBN：9789865813932
  - 城市的36個觀點 (韓文版, 도시의 36가지 표정, 2018), ISBN：9791188331260

|- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[Category:輔仁大學教授](../Category/輔仁大學教授.md "wikilink")
[Category:中華民國駐法國代表](../Category/中華民國駐法國代表.md "wikilink")
[Category:台灣土木工程師](../Category/台灣土木工程師.md "wikilink")
[Category:台灣作家](../Category/台灣作家.md "wikilink")
[Category:國立臺灣大學工學院校友](../Category/國立臺灣大學工學院校友.md "wikilink")
[Category:國立橋路學校校友](../Category/國立橋路學校校友.md "wikilink")
[Category:台灣外省人](../Category/台灣外省人.md "wikilink")
[Category:花蓮人](../Category/花蓮人.md "wikilink")
[Z子](../Category/楊姓.md "wikilink")
[Category:中華民國僑務委員會副委員長](../Category/中華民國僑務委員會副委員長.md "wikilink")
[Category:中華民國文化部次長](../Category/中華民國文化部次長.md "wikilink")
[Category:中華民國外交部次長](../Category/中華民國外交部次長.md "wikilink")