**楊文岳**（），字斗望，四川[南充人](../Page/南充.md "wikilink")。明末官员，死于[李自成军下](../Page/李自成.md "wikilink")。

## 生平

[萬歷四十七年](../Page/萬歷.md "wikilink")（1619年）進士，授[行人](../Page/行人.md "wikilink")。天啟五年，擢兵科[給事中](../Page/給事中.md "wikilink")，屢遷禮科[都給事中](../Page/都給事中.md "wikilink")。崇禎二年，出為江西右[參政](../Page/參政.md "wikilink")，歷湖廣、廣西按察使，雲南、山西左右[布政使](../Page/布政使.md "wikilink")，以右[副都御史巡撫登](../Page/副都御史.md "wikilink")、萊。崇禎十二年擢兵部右侍郎，總督保定、山東、河北軍務。崇禎十四年正月，李自成陷洛陽，犯開封，文岳率總兵[虎大威赴援](../Page/虎大威.md "wikilink")，趨鄧州，斬賊首一條龍、一只龍。

是年九月丁丑，[傅宗龍駐軍](../Page/傅宗龍.md "wikilink")[新蔡](../Page/新蔡.md "wikilink")，與楊文岳軍會合。己卯（八日），遇[李自成部](../Page/李自成.md "wikilink")，[賀人龍](../Page/賀人龍.md "wikilink")、[鄭嘉棟不戰而走](../Page/鄭嘉棟.md "wikilink")，[李國奇戰不勝](../Page/李國奇.md "wikilink")，亦走[項城](../Page/項城.md "wikilink")，於是明軍大潰於孟家莊（今平輿境內），再潰於火燒店，宗龍被圍，九日，部將挾文岳夜入於項城，文岳走陳州，與[丁啟睿駐軍](../Page/丁啟睿.md "wikilink")[汝寧](../Page/汝寧.md "wikilink")。事後文岳被革職，充為事官，以期戴罪自贖。李自成部遂破[葉縣](../Page/葉縣.md "wikilink")、[泌陽](../Page/泌陽.md "wikilink")，乘勝陷[南陽](../Page/南陽.md "wikilink")，殺唐王。連下鄧州等十四城，再圍開封。

[崇禎十五年](../Page/崇禎.md "wikilink")（1642年）文岳馳救開封，論功復官，九月，在汝寧夜襲賊營有功。閏十一月中旬，[孫傳庭戰死](../Page/孫傳庭.md "wikilink")，李自成率[羅汝才等部](../Page/羅汝才.md "wikilink")，合圍汝寧。文岳戰事不利，退入城中固守，李自成以[人海戰術猛攻不休](../Page/人海戰術.md "wikilink")，城陷被俘，[傅汝為自殺](../Page/傅汝為.md "wikilink")。楊文岳拒降，破口大罵，被綁在城南三里鋪，以大砲擊之，洞胸糜骨而死。

[Category:明朝行人司行人](../Category/明朝行人司行人.md "wikilink")
[Category:明朝給事中](../Category/明朝給事中.md "wikilink")
[Category:明朝江西布政使司參政](../Category/明朝江西布政使司參政.md "wikilink")
[Category:明朝湖廣按察使](../Category/明朝湖廣按察使.md "wikilink")
[Category:明朝廣西按察使](../Category/明朝廣西按察使.md "wikilink")
[Category:明朝雲南布政使](../Category/明朝雲南布政使.md "wikilink")
[Category:明朝山西布政使](../Category/明朝山西布政使.md "wikilink")
[Category:明朝保定總督](../Category/明朝保定總督.md "wikilink")
[Category:明朝登萊巡撫](../Category/明朝登萊巡撫.md "wikilink")
[Category:明朝戰爭身亡者](../Category/明朝戰爭身亡者.md "wikilink")
[Category:南充人](../Category/南充人.md "wikilink")
[W文](../Category/楊姓.md "wikilink")