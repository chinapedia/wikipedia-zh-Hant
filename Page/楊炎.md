**楊炎**（），字**公南**，[凤翔府](../Page/凤翔府.md "wikilink")[天兴县人](../Page/天兴县_\(唐朝\).md "wikilink")（今[陕西省](../Page/陕西省.md "wikilink")[凤翔县](../Page/凤翔县.md "wikilink")），[中國](../Page/中國.md "wikilink")[唐朝中期的政治家](../Page/唐朝.md "wikilink")，[兩稅法的創造和推行者](../Page/兩稅法.md "wikilink")。

## 生平

楊炎風骨峻峙，文藻雄麗，早年即有盛名。通過[科舉](../Page/科舉.md "wikilink")[釋褐試後](../Page/釋褐試.md "wikilink")，[河西節度使](../Page/河西節度使.md "wikilink")[呂崇賁聘為](../Page/呂崇賁.md "wikilink")[書記官](../Page/書記官.md "wikilink")，官至[中書舍人](../Page/中書舍人.md "wikilink")，後因與宰相[元載同鄉](../Page/元載.md "wikilink")，受重用為[吏部](../Page/吏部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")，[大曆十二年](../Page/大曆.md "wikilink")（777年）元載失勢被殺，朝廷派[劉晏處理此案](../Page/劉晏.md "wikilink")，楊炎坐貶[道州](../Page/道州.md "wikilink")[司馬](../Page/司馬.md "wikilink")，因此對劉晏有怨恨。[唐德宗即位](../Page/唐德宗.md "wikilink")（779年），被宰相[崔祐甫推薦](../Page/崔祐甫.md "wikilink")，任命為[門下侍郎](../Page/門下侍郎.md "wikilink")，[同中書門下平章事](../Page/同中書門下平章事.md "wikilink")（即宰相），次年建議推行[兩稅法](../Page/兩稅法.md "wikilink")，每年分夏、秋兩季徵收（稱夏輸、秋輸），全部以[金錢徵輸](../Page/金錢.md "wikilink")，廢止[徭役](../Page/徭役.md "wikilink")。兩稅法使當時受[藩鎮割據而財用不足的唐朝政府有所助益](../Page/藩鎮.md "wikilink")。

後崔祐甫因病淡出政壇，楊炎開始獨權，陷害有過節的財政專家[劉晏](../Page/劉晏.md "wikilink")，朝廷罷免了劉晏所兼的轉運、租庸、鹽鐵、常平等諸使。[建中元年](../Page/建中_\(唐朝\).md "wikilink")（780年）初，楊炎以劉晏奏事不實爲由，貶其爲[忠州](../Page/忠州.md "wikilink")（今[四川](../Page/四川.md "wikilink")[忠縣](../Page/忠縣.md "wikilink")）刺史。楊炎的親信荊南節度使[庾準更上奏德宗](../Page/庾準.md "wikilink")，稱劉晏誹謗朝廷，並稱劉晏曾致書[朱泚](../Page/朱泚.md "wikilink")，語言怨望，並召集士卒，打算造反。楊炎為此親自作了偽證，最終劉晏被誅殺，抄家時卻僅發現“雜書兩乘，米麥數斛”，天下冤之。此事使德宗起了殺死楊炎之心\[1\]，於是起用[盧杞為相以分權](../Page/盧杞.md "wikilink")。楊炎鄙視盧杞，恥與為伍，往往托病不和對方一起進餐（每托疾息于他阁，多不会食），使盧杞懷恨在心；加上楊炎意圖集權中央的政策為各藩鎮所不滿，造成河北、河南藩鎮聯合叛亂，盧杞以[李希烈遲不出兵討伐](../Page/李希烈.md "wikilink")[梁崇義一事](../Page/梁崇義.md "wikilink")，歸咎楊炎。德宗聽信盧杞之言，於建中二年十月將之貶為[崖州](../Page/崖州.md "wikilink")（海南島）[司馬](../Page/司馬.md "wikilink")，途中有詩-{云}-：“一去一萬-{里}-，千知千不還。崖州何處是，生度鬼門關。”杨炎並在途中被[賜死](../Page/賜死.md "wikilink")，年五十五歲。\[2\]

楊炎有才幹，但心胸狹隘，無法接受他人意見。德宗對楊炎的條陳提出不同意見時，楊炎動輒忿然作色，不肯繼續討論，“無復君臣之禮”。[李泌與德宗討論盧杞之惡](../Page/李泌.md "wikilink")，曾說楊炎罪不致死，實乃盧杞構陷，德宗表示：“卿所言確有其事。但楊炎把朕當作小孩般對待，每次有事上奏商討，朕批-{准}-他的要求便告退，若朕不准奏，楊炎便以辭官作要挾，朕並非只因盧杞陷害才厭惡他的。”\[3\]\[4\]宋[孫甫](../Page/孫甫.md "wikilink")《唐史論斷》稱：“炎頗有才而心不公，故不能成就功業，卒至禍敗。”

工於繪畫，[张彦远评其山水画](../Page/张彦远.md "wikilink")“余观杨公山水图，想见其为人，魁岸洒落也。”《[全唐詩](../Page/全唐詩.md "wikilink")》收錄楊炎詩二首。

子[杨朗](../Page/杨朗.md "wikilink")，[殿中侍御史](../Page/殿中侍御史.md "wikilink")。

## 注釋

## 影视作品

  - 1994年電視劇《[人面桃花](../Page/人面桃花_\(1994年電視劇\).md "wikilink")》
    [龐祥麟](../Page/龐祥麟.md "wikilink") 飾 楊炎
  - 2012年電視劇《[紫釵奇緣](../Page/紫釵奇緣.md "wikilink")》
    [李耀敬](../Page/李耀敬.md "wikilink") 飾 楊炎

## 参考资料

  - 《[旧唐书](../Page/旧唐书.md "wikilink")·列传第六十八》
  - 《[新唐书](../Page/新唐书.md "wikilink")·列传第七十》

## 參見

  - [兩稅法](../Page/兩稅法.md "wikilink")

[Category:唐朝尚书左仆射](../Category/唐朝尚书左仆射.md "wikilink")
[Category:唐朝中书舍人](../Category/唐朝中书舍人.md "wikilink")
[Category:唐朝吏部侍郎](../Category/唐朝吏部侍郎.md "wikilink")
[Category:唐朝州司马](../Category/唐朝州司马.md "wikilink")
[Category:唐朝门下侍郎](../Category/唐朝门下侍郎.md "wikilink")
[Category:唐朝同中书门下平章事](../Category/唐朝同中书门下平章事.md "wikilink")
[Category:唐朝被賜死人物](../Category/唐朝被賜死人物.md "wikilink")
[Category:唐朝畫家](../Category/唐朝畫家.md "wikilink")
[Category:唐朝詩人](../Category/唐朝詩人.md "wikilink")
[Category:鳳翔人](../Category/鳳翔人.md "wikilink")
[Y炎](../Category/楊姓.md "wikilink")
[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")

1.  《[资治通鉴](../Page/资治通鉴.md "wikilink")》載：“上闻而恶之，由是有诛炎之志，隐而未发。”
2.  [苏鹗](../Page/苏鹗.md "wikilink")《杜阳杂编》卷上记杨炎杀刘宴，后被赐死之事：“公南既杀刘晏，士庶莫不寃痛之。明年，公南得罪，赐死崖州，时人谓刘相公宽报矣。”后注：“《实录》云：七月庚午，晏已受诛，使回，云至乙丑，下诏杀之。”
3.  《[新唐書](../Page/新唐書.md "wikilink")·李泌傳》：（李泌）對曰：「陛下能覺杞之惡，安致建中禍邪？......又楊炎罪不至死，杞擠陷之而相關播。......」帝曰：「卿言誠有之。然楊炎視朕如三尺童子，有所論奏，可則退，不許則辭官，非特杞惡之也。......」
4.  《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷二三三貞元四年二月