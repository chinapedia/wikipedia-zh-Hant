**楊耀忠**（，），是第九、十、十一、十二屆[中國](../Page/中國.md "wikilink")[全國人大代表](../Page/全國人大.md "wikilink")，香港前立法會議員，[民主建港協進聯盟成員](../Page/民主建港協進聯盟.md "wikilink")，[香港教育工作者聯會會長](../Page/香港教育工作者聯會.md "wikilink")。榮休[天水圍香島中學校長後](../Page/天水圍香島中學.md "wikilink")，他擔任[天水圍香島中學校監](../Page/天水圍香島中學.md "wikilink")。

## 簡介

於1976年於中文大學畢業，修讀政治與行政學系。

作為香港親北京教育社團的領袖，楊耀忠多次代表教聯與[教協歷任會長](../Page/教協.md "wikilink")[司徒華及](../Page/司徒華.md "wikilink")[張文光競逐前](../Page/張文光.md "wikilink")[立法局的教育界代表議席](../Page/立法局.md "wikilink")，但都落敗。[香港主權移交前](../Page/香港主權移交.md "wikilink")，獲委任為[臨時立法會議員](../Page/臨時立法會.md "wikilink")。1996年9月:楊耀忠出任香島中學校長一職。回歸後，屬於[泛民陣營的張文光議員](../Page/泛民主派.md "wikilink")「落車」，他在教育界的代表由楊耀忠接任。其後，在第一、第二屆立法會選舉中，楊耀忠循[選舉委員會進入立法會](../Page/選舉委員會.md "wikilink")。2001年楊耀忠出任天水圍香島中學創校校長校長。[2003年區議會選舉](../Page/2003年區議會選舉.md "wikilink")，他在[美孚選區慘敗在民主黨候選人](../Page/美孚.md "wikilink")、前[商業電台高級監製](../Page/商業電台.md "wikilink")[王德全手上](../Page/王德全.md "wikilink")，使他失去進軍直選的橋頭堡，2004年，他不再尋求連任。2011年,楊耀忠接任將軍澳香島中學校監一職。由1996年他在香島多間分校當過教學及行政工作。至2015年，楊在香島中學擔任了十九年校長。2015年在[天水圍香島中學校長一職退下後](../Page/天水圍香島中學.md "wikilink")，他現任[天水圍香島中學校監](../Page/天水圍香島中學.md "wikilink")。

[皇后碼頭清拆事件中](../Page/皇后碼頭.md "wikilink")，楊耀忠等人為求使政府拆卸天星碼頭而把它評定為三級文物，他更明言，指皇后碼頭的價值不大，結果委員會決議把皇后碼頭被評為一級文物。

## 榮譽

  - [銅紫荊星章](../Page/銅紫荊星章.md "wikilink")（2001年）
  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2004年）

## 參考

  - [傳楊耀忠拒續掌教聯會](http://orientaldaily.on.cc/cnt/news/20130920/00176_093.html)
  - [教聯會年底換屆
    楊耀忠「退下火線」](http://paper.wenweipo.com/2013/09/20/HK1309200020.htm)

<references />

[Category:前香港立法會議員](../Category/前香港立法會議員.md "wikilink")
[Category:香港中學校長](../Category/香港中學校長.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Y](../Category/楊姓.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:第九屆港區全國人大代表](../Category/第九屆港區全國人大代表.md "wikilink")
[Category:第十屆港區全國人大代表](../Category/第十屆港區全國人大代表.md "wikilink")
[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:第十二屆港區全國人大代表](../Category/第十二屆港區全國人大代表.md "wikilink")