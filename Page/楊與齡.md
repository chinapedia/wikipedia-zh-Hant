**楊與齡**（），[四川](../Page/四川.md "wikilink")[綦江縣人](../Page/綦江縣.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[法律學者](../Page/法律.md "wikilink")，[中華民國前](../Page/中華民國.md "wikilink")[司法院](../Page/司法院.md "wikilink")[大法官](../Page/司法院大法官.md "wikilink")。

## 生平

1945年，楊與齡初入學[中央政治學校](../Page/中央政治學校.md "wikilink")（[國立政治大學](../Page/國立政治大學.md "wikilink")）法政系，其後法政系改組分家為政治系和法律系，選擇專攻法律，為法律系第十五期畢業生，之後從[革命實踐研究院及國防研究院畢業](../Page/革命實踐研究院.md "wikilink")。1950年，於[司法官考試以第一名的成績及格](../Page/司法官.md "wikilink")；其後歷任[法院推事](../Page/法院.md "wikilink")（[法官](../Page/法官.md "wikilink")）、[庭長](../Page/庭長.md "wikilink")，最高至[最高法院推事之職](../Page/中華民國最高法院.md "wikilink")。後又曾任[司法行政部](../Page/司法行政部.md "wikilink")（[法務部](../Page/中華民國法務部.md "wikilink")）民事司司長一職。1976年起擔任中華民國第四屆大法官；1985年連任為第五屆大法官；1994年任滿退職。

在學術方面，自1963年起，曾於[逢甲大學](../Page/逢甲大學.md "wikilink")、[中國醫藥學院](../Page/中國醫藥學院.md "wikilink")、國立政治大學、[中國文化大學](../Page/中國文化大學.md "wikilink")、[東吳大學](../Page/東吳大學.md "wikilink")、[銘傳大學](../Page/銘傳大學.md "wikilink")、[政治作戰學校及](../Page/政治作戰學校.md "wikilink")[中央警察大學等校兼任副教授](../Page/中央警察大學.md "wikilink")、[教授之職](../Page/教授.md "wikilink")。

楊與齡亦因其民事法之專長，先後被延攬為[中華民國的](../Page/中華民國.md "wikilink")[民法](../Page/民法.md "wikilink")、[民事訴訟法](../Page/民事訴訟法.md "wikilink")、[強制執行法及](../Page/強制執行法.md "wikilink")[破產法之修正委員會委員](../Page/破產法.md "wikilink")。就台灣民事執行之領域而言，其見解有一定之權威地位，至2006年為止，楊與齡仍在擬具債務清理機制相關法規，為[台灣因](../Page/台灣.md "wikilink")[信用卡卡債所生的](../Page/信用卡.md "wikilink")「[卡奴](../Page/卡奴.md "wikilink")」問題，提供寶貴意見。

## 參考資料

  - 《大法官釋憲史料》，[司法院編](../Page/司法院.md "wikilink")，1998年。
  - [國立政治大學校園新聞](../Page/國立政治大學.md "wikilink") - [從中央政治學校到政治大學
    大法官楊與齡回憶半甲子法律生涯](http://www.nccu.edu.tw/news/detail.php?news_id=1837)
  - 自由電子報 -
    [司院催生還債機制](http://www.libertytimes.com.tw/2006/new/feb/27/today-so4.htm個人破產法)

[Y楊](../Category/台湾法学家.md "wikilink")
[Y楊](../Category/司法院大法官.md "wikilink")
[Y楊](../Category/國立政治大學教授.md "wikilink")
[Y楊](../Category/國立政治大學校友.md "wikilink")
[Category:四川人](../Category/四川人.md "wikilink")
[Category:杨姓](../Category/杨姓.md "wikilink")
[Category:綦江人](../Category/綦江人.md "wikilink")