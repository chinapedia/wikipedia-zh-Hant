**楊雅惠**（），[台灣基督教](../Page/台灣基督教.md "wikilink")[牧師](../Page/牧師.md "wikilink")，畢業於[台南神學院](../Page/台南神學院.md "wikilink")，是[台灣](../Page/台灣.md "wikilink")[同志權利運動的活躍人物](../Page/LGBT權利運動.md "wikilink")。

## 簡介

她在1992年赴[美國就讀](../Page/美國.md "wikilink")[芝加哥神學院](../Page/芝加哥神學院.md "wikilink")，返回台灣後在[台灣基督長老教會新竹大專學生中心擔任輔導](../Page/台灣基督長老教會.md "wikilink")。她在1995年11月成立第一個[同志基督徒](../Page/LGBT.md "wikilink")[團契](../Page/團契.md "wikilink")「**約拿單團契**」，1996年創立[同志教徒專屬的](../Page/LGBT.md "wikilink")[同光同志長老教會](../Page/同光同志長老教會.md "wikilink")。她對同志教徒的開放態度，在台灣並不見容於保守的基督教教會團體。

1996年7月31日，她離開台灣基督長老教會新竹大專學生中心。1996年8月4日，她就任同光教會第一任駐堂牧師。1998年6月，她因為在[牧會工作上面臨瓶頸](../Page/牧會.md "wikilink")，離開同光同志長老教會。離開同光同志長老教會之後，她輾轉前往[台灣基督長老教會七星中會暖暖教會](../Page/台灣基督長老教會.md "wikilink")、台灣基督長老教會彰化中會中山教會牧會。2006年，她在台灣基督長老教會台中中會吉峰教會牧會。2007年2月，她離開吉峰教會。

2008年年初，她與[台灣教會公報社聯繫](../Page/台灣教會公報.md "wikilink")，希望該社代印出版一本[自傳](../Page/自傳.md "wikilink")《背著十字架的女牧師：楊雅惠牧師的信仰歷程》。

## 自殺身亡

2008年5月20日，她把自傳所有稿件交給台灣教會公報社，並繳清代印費，隨即失去聯繫。2008年5月21日，她在家[燒炭自殺](../Page/燒炭自殺.md "wikilink")，死時46歲。2008年5月23日，她的同工破門進入她家，才發現她已陳屍家中，研判她應是在同月21日凌晨燒炭自殺。她自殺前三星期，曾在家裡被人發現有意識不清的現象，緊急送往醫院救治；她出院後，台灣基督長老教會台中中會即委請台灣基督長老教會台中大專學生中心特別代為照顧。

2008年5月27日，台灣基督長老教會台中中會在[台中市立殯儀館舉行她的](../Page/台中市立殯儀館.md "wikilink")[追思會](../Page/追思會.md "wikilink")，她的遺體[火化後由家人安葬於家族墓園](../Page/火化.md "wikilink")。

## 外部連結

  - [楊雅惠基本資料](http://acts.pct.org.tw/datasearch/Pastor.aspx?strPID=80294)
  - [哀悼楊雅惠](https://web.archive.org/web/20080602175336/http://www.atj.org.tw/newscon1.asp?number=5753)
  - [同光同志長老教會創辦人
    爭議女牧師燒炭身亡](http://chinese.gospelherald.com/news/soc-9961-0/)

[Category:臺灣LGBT權利運動者](../Category/臺灣LGBT權利運動者.md "wikilink")
[Category:臺灣女性自殺者](../Category/臺灣女性自殺者.md "wikilink")
[Category:台灣長老會牧師](../Category/台灣長老會牧師.md "wikilink")
[Y雅](../Category/楊姓.md "wikilink")
[Category:燒炭自殺者](../Category/燒炭自殺者.md "wikilink")
[Category:女性傳教士](../Category/女性傳教士.md "wikilink")