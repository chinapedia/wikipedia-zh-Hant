是由[韓國公司](../Page/韓國.md "wikilink")[Nexon開發的](../Page/Nexon.md "wikilink")[任天堂DS遊戲](../Page/任天堂DS.md "wikilink")。一個從[Windows平台上的](../Page/Microsoft_Windows.md "wikilink")[網路遊戲](../Page/網路遊戲.md "wikilink")《[楓之谷](../Page/楓之谷.md "wikilink")》所衍生出在上的版本。遊戲於2006年的[E3發表](../Page/E3.md "wikilink")，2010年4月15日在韩国發售。

2007年1月9日，Nexon和新成立的[任天堂韓國在首次展示了](../Page/任天堂韓國.md "wikilink")《楓之谷DS》。

《冒险岛DS》不是[大型多人在線角色扮演遊戲](../Page/大型多人在線角色扮演遊戲.md "wikilink")。David
Lee在一次採訪中表示，即將發行的《冒险岛DS》是根據Windows版本的[楓之谷製作的携带版本](../Page/楓之谷.md "wikilink")。[網路功能可能會有所限制](../Page/網路.md "wikilink")，遊戲主要以單人為主。

## 外部連結

  -
[Category:2010年电子游戏](../Category/2010年电子游戏.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")
[Category:韩国开发电子游戏](../Category/韩国开发电子游戏.md "wikilink")
[DS](../Category/楓之谷.md "wikilink")