**楚威王**（？），**芈**姓，**熊**氏，名**商**，[楚宣王之子](../Page/楚宣王.md "wikilink")，[战国时期](../Page/战国时期.md "wikilink")[楚国著名国君](../Page/楚国.md "wikilink")，被称为楚国的[中兴之主](../Page/中兴.md "wikilink")。他在位期间，楚国的疆域达到了最大规模，囊括了长江中下游以及支流众多的淮河流域。苏秦曾对楚威王说：“西有[黔中](../Page/黔中.md "wikilink")、[巫郡](../Page/巫郡.md "wikilink")，东有[夏州](../Page/夏州.md "wikilink")、[海阳](../Page/海阳.md "wikilink")，南有[洞庭](../Page/洞庭.md "wikilink")、[苍梧](../Page/苍梧.md "wikilink")，北有陉塞、[郇阳](../Page/郇阳.md "wikilink")。地方五千余里，带甲百万，车千乘，骑万匹，粟支十年。此霸王之资也。”前333年（楚威王七年），楚国大軍伐[齊](../Page/齊.md "wikilink")，與齊將[申縛戰於](../Page/申縛.md "wikilink")[泗水](../Page/泗水郡.md "wikilink")，進圍[徐州](../Page/徐州.md "wikilink")，大敗申縛。威王十一年而卒，子[楚懷王熊槐繼位](../Page/楚懷王.md "wikilink")。

## 生平

### 初期

[战国形势图（前350年）（简）.png](https://zh.wikipedia.org/wiki/File:战国形势图（前350年）（简）.png "fig:战国形势图（前350年）（简）.png")

前338年（楚威王二年），[秦孝公去世](../Page/秦孝公.md "wikilink")，[秦国内部发生动乱](../Page/秦国.md "wikilink")，[商鞅被害](../Page/商鞅.md "wikilink")。[楚国](../Page/楚国.md "wikilink")、[韩国和](../Page/韩国.md "wikilink")[赵国奉行联秦以制魏的方针](../Page/赵国.md "wikilink")，前337年，楚威王、[韩昭侯](../Page/韩昭侯.md "wikilink")、[赵肃侯都派使者前往秦国](../Page/赵肃侯.md "wikilink")，向[秦惠文君致贺](../Page/秦惠文王.md "wikilink")。与之同时，[蜀王使者也前往秦国向秦惠文君致贺](../Page/古蜀.md "wikilink")。《[史记](../Page/史记.md "wikilink")
秦本纪》记载：“惠文君元年，楚、韩、赵、蜀人来朝”。

当时，[齐国在](../Page/齐国.md "wikilink")[齐威王的治理下已经日益强盛](../Page/齐威王.md "wikilink")，最终在[马陵之战击败了魏国](../Page/马陵之战.md "wikilink")。前334年（楚威王六年），[齐威王与](../Page/齐威王.md "wikilink")[魏惠王](../Page/魏惠王.md "wikilink")“会[徐州相王](../Page/徐州相王.md "wikilink")”，相约并力讨伐楚国。此事引起了楚国的极大愤怒。

前333年（楚威王七年），楚威王以[景翠为楚师元帅](../Page/景翠.md "wikilink")，歼灭越师主力，尽取越人所占吴地。越人从此离散，成为楚国的附庸。最终，[越国在](../Page/越国.md "wikilink")[楚怀王时彻底灭亡](../Page/楚怀王.md "wikilink")。

### 顶峰

在楚国灭掉越国的同一年，即前333年（楚威王七年），景翠在攻灭越国后，移师北上，与齐师大战于徐州，击败齐国，进围徐州，取得了楚威王在位期间的最大胜利。此战之后，楚国达到了他在[战国时期的顶峰](../Page/战国时期.md "wikilink")，其版图，西起[大巴山](../Page/大巴山.md "wikilink")、[巫山](../Page/巫山.md "wikilink")、[武陵山](../Page/武陵山.md "wikilink")，东至大海，南起五岭，北至汝、颖、沂、泗，囊括了[长江中下游以及支流众多的](../Page/长江.md "wikilink")[淮河流域](../Page/淮河.md "wikilink")，根据《史记
苏秦列传》记载，[苏秦曾对楚威王说](../Page/苏秦.md "wikilink")：“楚，天下之强国也；王，天下之贤王也。......地方五千余里，带甲百万，车千乘，骑万匹，粟支十年。此霸王之资也。”

### 后期

《史记
苏秦列传》记载，楚威王曾对苏秦说：“寡人之国西与秦接境，秦有举巴蜀并汉中之心。秦，虎狼之国，不可亲也。而韩、魏迫于秦患，不可与深谋，与深谋恐反人以入于秦，故谋未发而国已危矣。寡人自料，以楚当秦，不见胜也；内与群臣谋，不足恃也。寡人卧不安席，食不甘味，心摇摇然如县旌而无所终薄。”由此可以反映出楚威王对当时楚国所处局势的清醒认识。

前332年（楚威王八年），魏国将[阴晋](../Page/阴晋.md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[华阴东](../Page/华阴.md "wikilink")）献给秦国，从此掀起了割地赂秦之风。阴晋易手之后两年，[河西魏地全部被秦国攻占](../Page/河西.md "wikilink")。又一年后，秦国又开始侵占[河东的魏地了](../Page/河东.md "wikilink")。

前329年（楚威王十一年），楚威王去世，在位十一年，子[熊槐即位](../Page/熊槐.md "wikilink")，是为[楚怀王](../Page/楚怀王.md "wikilink")。

## 参见

  - [楚国](../Page/楚国.md "wikilink")

<!-- end list -->

  - [齐威王](../Page/齐威王.md "wikilink")

<!-- end list -->

  - [苏秦](../Page/苏秦.md "wikilink")

## 参考资料

张正明 著 《楚史》 中国人民大学出版社 2010年7月出版 ISBN 978-7-300-12250-2

[Category:楚國君主](../Category/楚國君主.md "wikilink")
[王](../Category/諡威.md "wikilink")