**楚格州**（[德語](../Page/德語.md "wikilink")、[羅曼語](../Page/羅曼語.md "wikilink")：Zug,
[法語](../Page/法語.md "wikilink")：Zoug,
[意大利語](../Page/意大利語.md "wikilink")：Zugo）位於[瑞士中部](../Page/瑞士.md "wikilink")，是該國面積的第三小的州份，僅比[巴塞爾城市州及](../Page/巴塞爾城市州.md "wikilink")[內阿彭策爾州兩個州大](../Page/內阿彭策爾州.md "wikilink")。

## 地理

楚格州西鄰[阿爾高州及](../Page/阿爾高州.md "wikilink")[琉森州](../Page/琉森州.md "wikilink")，北接[蘇黎世州](../Page/蘇黎世州.md "wikilink")，南面與東面與[施維茨州接壤](../Page/施維茨州.md "wikilink")。楚格州位於丘陵起伏的[高原上](../Page/高原.md "wikilink")，州內最高點為韋爾特舒匹茲峰（Wildspitz）（海拔1580米）。境內主要的[湖泊計有](../Page/湖泊.md "wikilink")[楚格湖](../Page/楚格湖.md "wikilink")（Zugersee）及[阿格利湖](../Page/阿格利湖.md "wikilink")（Ägerisee）。楚格州的主要[河流是羅澤河](../Page/河流.md "wikilink")（Lorze），它把[楚格湖和](../Page/楚格湖.md "wikilink")[阿格利湖連接起來](../Page/阿格利湖.md "wikilink")，然後流向西北方與[羅伊斯河](../Page/羅伊斯河.md "wikilink")（Reuss）滙合。

## 人口

該州2002年[人口大約](../Page/人口.md "wikilink")100,900，主要信奉[羅馬天主教](../Page/羅馬天主教.md "wikilink")，主要語言是瑞士德語的瑞士中部[方言](../Page/方言.md "wikilink")。在1814年以前楚格州屬於康斯坦斯教區，後來被編入[巴澤爾教區](../Page/巴澤爾.md "wikilink")。

## 經濟

主要工業有金屬品製造業、紡織業、機械製造業、電子業及製造Kirsch（一種含酒精飲品）。
楚格州的稅率為全國最低，因此吸引了不少企業到其首府[楚格市成立總部](../Page/楚格.md "wikilink")。

## 交通

楚格州有鐵路連接[琉森及](../Page/琉森.md "wikilink")[蘇黎世](../Page/蘇黎世.md "wikilink")，兩段鐵路於Arth-Goldau滙合，Arth-Goldau是貫通德瑞意的聖哥達鐵路的其中一站。

## 市鎮

由於楚格州面積太小，所以沒有分區，而[市鎮級行政區共有](../Page/瑞士的市镇.md "wikilink")11個，包括[楚格市](../Page/楚格市.md "wikilink")（Zug），[上阿格利](../Page/上阿格利.md "wikilink")（Oberägeri），[下阿格利](../Page/下阿格利.md "wikilink")（Unterägeri），[孟辛根](../Page/孟辛根.md "wikilink")（Menzingen），[巴阿](../Page/巴阿.md "wikilink")（Baar），[卡姆](../Page/卡姆.md "wikilink")（Cham），[休倫堡](../Page/休倫堡.md "wikilink")（Hünenberg），[施坦豪森](../Page/施坦豪森.md "wikilink")（Steinhausen），[里希](../Page/里希.md "wikilink")（Risch），[瓦爾希韋](../Page/瓦爾希韋.md "wikilink")（Walchwil），[新海姆](../Page/新海姆.md "wikilink")（Neuheim）

## 外部連結

  - [楚格州官方網站](http://www.zug.ch/)（[德語](../Page/德語.md "wikilink")）
  - [官方統計數字](https://web.archive.org/web/20061015172853/http://www.bfs.admin.ch/bfs/portal/en/index/regionen/regionalportraets/zug/blank/kennzahlen.html)
  - [楚格州與蘇黎世州邊境的照片](http://www.pbase.com/karibaer/finstersee)

[Category:瑞士行政區劃](../Category/瑞士行政區劃.md "wikilink")