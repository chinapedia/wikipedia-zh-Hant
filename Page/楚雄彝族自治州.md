**楚雄彝族自治州**，简称**楚雄州**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省下辖的](../Page/云南省.md "wikilink")[自治州](../Page/自治州.md "wikilink")，位于云南省中部。东接[昆明市](../Page/昆明市.md "wikilink")，南邻[玉溪市](../Page/玉溪市.md "wikilink")、[普洱市](../Page/普洱市.md "wikilink")，西界[大理州](../Page/大理州.md "wikilink")，北与[丽江市及](../Page/丽江市.md "wikilink")[四川省](../Page/四川省.md "wikilink")[攀枝花市](../Page/攀枝花市.md "wikilink")、[凉山州毗邻](../Page/凉山州.md "wikilink")。地处滇中高原，[乌蒙山与](../Page/乌蒙山.md "wikilink")[哀牢山之间](../Page/哀牢山.md "wikilink")，[金沙江流经州境北部](../Page/金沙江.md "wikilink")，还有[万马河](../Page/万马河.md "wikilink")、[龙川江](../Page/龙川江.md "wikilink")、[勐果河](../Page/勐果河.md "wikilink")、[礼社江等主要河流](../Page/礼社江.md "wikilink")。总面积2.84万平方公里，人口273.3万，[汉族人口比例约](../Page/汉族.md "wikilink")67%，[彝族人口比例约](../Page/彝族.md "wikilink")27%。

## 历史

古为[氐羌](../Page/氐羌.md "wikilink")、[百越](../Page/百越.md "wikilink")、[百濮等部族居住之地](../Page/百濮.md "wikilink")。[汉代属](../Page/汉代.md "wikilink")[越嶲](../Page/越嶲郡.md "wikilink")、[益州两郡](../Page/益州郡.md "wikilink")。[三国](../Page/三国.md "wikilink")[蜀汉](../Page/蜀汉.md "wikilink")[建兴三年](../Page/建兴.md "wikilink")（225年），丞相[诸葛亮平定](../Page/诸葛亮.md "wikilink")[南中](../Page/南中.md "wikilink")，分[建宁](../Page/建宁郡.md "wikilink")、越巂、[永昌三郡置](../Page/永昌郡.md "wikilink")[云南郡](../Page/云南郡.md "wikilink")，郡治[云南县](../Page/云南县.md "wikilink")（今[祥云县](../Page/祥云县.md "wikilink")[云南驿镇](../Page/云南驿镇.md "wikilink")），属[庲降都督](../Page/庲降都督.md "wikilink")；东部与北部分属建宁郡和越嶲郡。[西晋改庲降都督为](../Page/西晋.md "wikilink")[宁州](../Page/宁州.md "wikilink")。[东晋](../Page/东晋.md "wikilink")[成帝年间](../Page/晋成帝.md "wikilink")，分云南郡置[兴宁郡](../Page/兴宁郡.md "wikilink")，治[弄栋县](../Page/弄栋县.md "wikilink")（今[姚安县西北旧城](../Page/姚安县.md "wikilink")）。[咸康八年](../Page/咸康.md "wikilink")（342年），今楚雄市有“爨酋威楚筑城硪碌赕居之”，故有“威楚”之称。[南朝齐移兴宁郡治至](../Page/南朝齐.md "wikilink")[青蛉县](../Page/青蛉县.md "wikilink")（今[大姚县](../Page/大姚县.md "wikilink")）。[南朝梁末废兴宁郡](../Page/南朝梁.md "wikilink")。[唐初于境内置](../Page/唐朝.md "wikilink")[戎州](../Page/戎州.md "wikilink")（今市境北部）和[姚州](../Page/姚州.md "wikilink")（今[姚安县境](../Page/姚安县.md "wikilink")）都督府。[南诏时置](../Page/南诏.md "wikilink")[弄栋节度](../Page/弄栋节度.md "wikilink")，治今姚安县；东部属[拓东节度](../Page/拓东节度.md "wikilink")。[大理国时置](../Page/大理国.md "wikilink")**[威楚府](../Page/威楚府.md "wikilink")**（治今[楚雄市](../Page/楚雄市.md "wikilink")）和[弄栋府](../Page/弄栋府.md "wikilink")（治今姚安县）。

[元初置](../Page/元朝.md "wikilink")[威楚万户府](../Page/威楚万户府.md "wikilink")（治今楚雄市）和[罗婺万户府](../Page/罗婺万户府.md "wikilink")（治今[武定县东](../Page/武定县.md "wikilink")）。至元八年（1271年）改威楚万户府为[威楚路](../Page/威楚路.md "wikilink")。至元十一年（1275年）改大姚堡千户所置[大姚县](../Page/大姚县.md "wikilink")，隶[大理路](../Page/大理路.md "wikilink")。至元十二年（1275年）改罗婺万户府为[武定路](../Page/武定路.md "wikilink")；改统矢千户所复置[姚州](../Page/姚州.md "wikilink")（治今姚安县）；改牟州千户所为[定远州](../Page/定远州.md "wikilink")（治今[牟定县吕交城](../Page/牟定县.md "wikilink")），属威楚路；改摩刍千户所为[南安州](../Page/南安州.md "wikilink")（治今[双柏县](../Page/双柏县.md "wikilink")[云龙镇](../Page/云龙镇.md "wikilink")），属威楚路，并改路赕千户所置[广通县](../Page/广通县.md "wikilink")（治今禄丰县[广通镇](../Page/广通镇.md "wikilink")），属南安州；改𥔲嘉千户所为[𥔲嘉县](../Page/𥔲嘉县.md "wikilink")（治今双柏县西南），属威楚路；置[罗次州](../Page/罗次州.md "wikilink")（治今禄丰县[碧城镇](../Page/碧城镇.md "wikilink")），属[中庆路](../Page/中庆路.md "wikilink")；割安宁千户所之碌琫（治今[禄丰县](../Page/禄丰县.md "wikilink")[金山镇](../Page/金山镇.md "wikilink")）等地置[禄丰县](../Page/禄丰县.md "wikilink")，属中庆路[安宁州](../Page/安宁州.md "wikilink")。至元十五年（1278年）置[威州](../Page/威州_\(云南\).md "wikilink")（治今楚雄市）。至元十六年（1279年）置[元谋县](../Page/元谋县.md "wikilink")（治今元谋县[老城乡](../Page/老城乡.md "wikilink")），属武定路[和曲州](../Page/和曲州.md "wikilink")。至元二十一年（1284年）降威州置[威楚县](../Page/威楚县.md "wikilink")，属威楚路。至元二十二年（1285年）置[镇南州](../Page/镇南州.md "wikilink")（治今[南华县](../Page/南华县.md "wikilink")），属威楚路。至元二十四年（1287年）降罗次州为[罗次县](../Page/罗次县.md "wikilink")。至元二十六年（1289年）置[南甸县为武定路治](../Page/南甸县.md "wikilink")。[天历元年](../Page/天历.md "wikilink")（1328年）升姚州为[姚安路](../Page/姚安路.md "wikilink")。

[明](../Page/明朝.md "wikilink")[洪武十五年](../Page/洪武.md "wikilink")（1382年），因南雄侯[赵庸略地记功](../Page/赵庸.md "wikilink")，遂取威楚的“楚”与南雄侯的“雄”而改威楚路为**[楚雄府](../Page/楚雄府.md "wikilink")**，改威楚县为**[楚雄县](../Page/楚雄县.md "wikilink")**；同年改姚安路为[姚安府](../Page/姚安府.md "wikilink")，以姚州为附郭；改武定路置[武定军民府](../Page/武定军民府.md "wikilink")。洪武二十七年（1394年）升姚安府为[姚安军民府](../Page/姚安军民府.md "wikilink")。[万历中改武定军民府为](../Page/万历.md "wikilink")[武定府](../Page/武定府.md "wikilink")。[正德元年](../Page/正德.md "wikilink")（1506年）省南甸县。[隆庆四年](../Page/隆庆.md "wikilink")（1570年）武定府移治今武定县城。[清](../Page/清朝.md "wikilink")[康熙八年](../Page/康熙.md "wikilink")（1669年）裁𥔲嘉县入南安州。[乾隆三十五年](../Page/乾隆.md "wikilink")（1770年）改武定府为[武定直隶州](../Page/武定直隶州.md "wikilink")；罢姚安府，姚州改属楚雄府。[光绪四年](../Page/光绪.md "wikilink")（1878年）元谋县治由老城迁至马街（今[元马镇](../Page/元马镇.md "wikilink")）。

[民国元年](../Page/民国.md "wikilink")（1912年）由姚州析置[盐丰县](../Page/盐丰县.md "wikilink")，治白盐井（今大姚县[石羊镇](../Page/石羊镇.md "wikilink")）。民国二年（1913年）由定远县、广通县析置[盐兴县](../Page/盐兴县.md "wikilink")，治黑盐井（今禄丰县[黑井镇](../Page/黑井镇.md "wikilink")）；同年废府、州改县，计有楚雄、大姚、武定、姚安、定远、南安、广通、罗次、禄丰、元谋、镇南、盐丰、盐兴等县，各县属[滇中道](../Page/滇中道.md "wikilink")。1914年，因南安县与[福建省](../Page/福建省.md "wikilink")[南安县重名](../Page/南安县.md "wikilink")，易名[摩刍县](../Page/摩刍县.md "wikilink")；定远县与[安徽省](../Page/安徽省.md "wikilink")[定远县重名](../Page/定远县.md "wikilink")，取“牟州”、“定远”首字，更名[牟定县](../Page/牟定县.md "wikilink")；同年大姚、镇南、姚安、盐丰4县改属[腾越道](../Page/腾越道.md "wikilink")。1929年废道制，各县由省政府直辖；同年摩刍县恢复汉代之名[双柏县](../Page/双柏县.md "wikilink")；析大姚县设[永仁县](../Page/永仁县.md "wikilink")，取境内“永定”与“仁和”两集镇各一字为名。民国三十五年（1946年）部分县划入云南省第八行政督察区，专署驻姚安县。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，1950年分置[楚雄专区](../Page/楚雄专区.md "wikilink")（辖楚雄、双柏、镇南、牟定、姚安、大姚、盐丰、永仁、禄丰、广通、盐兴11县）和[武定专区](../Page/武定专区.md "wikilink")（辖元谋、武定、[安宁](../Page/安宁县.md "wikilink")、罗次、[禄劝](../Page/禄劝县.md "wikilink")、[富民](../Page/富民县.md "wikilink")6县）。1953年武定专区并入楚雄专区。1954年改镇南县为[南华县](../Page/南华县.md "wikilink")。1957年将安宁县划归[昆明市](../Page/昆明市.md "wikilink")。1957年10月批准设立[楚雄彝族自治州](../Page/楚雄彝族自治州.md "wikilink")，次年4月正式成立。1958年将南华、牟定、双柏3县并入楚雄县；姚安、盐丰、永仁3县并入大姚县；盐兴、罗次、广通3县并入禄丰县；元谋县并入武定县；富民县划归昆明市。永仁、姚安、南华、双柏、牟定、元谋6县后于1959至1961年间复置（双柏县迁驻[妥甸镇](../Page/妥甸镇.md "wikilink")）。1983年9月，撤销楚雄县，改设[楚雄市](../Page/楚雄市.md "wikilink")；同年10月将禄劝县划归昆明市。

## 地理

### 位置

楚雄彝族自治州地处康滇地轴南段的偏西一方，跨东经100º43′～102º30′，北纬24º13′～26º30′之间，属[云贵高原西部](../Page/云贵高原.md "wikilink")，[滇中高原的主体部位](../Page/滇中高原.md "wikilink")。东靠[昆明市](../Page/昆明市.md "wikilink")，西接[大理白族自治州](../Page/大理白族自治州.md "wikilink")，南连[思茅地区和](../Page/思茅地区.md "wikilink")[玉溪市](../Page/玉溪市.md "wikilink")，北临[四川省](../Page/四川省.md "wikilink")[攀枝花市和](../Page/攀枝花市.md "wikilink")[凉山彝族自治州](../Page/凉山彝族自治州.md "wikilink")，西北隔[金沙江与](../Page/金沙江.md "wikilink")[丽江地区相望](../Page/丽江地区.md "wikilink")。自古为省垣屏障、滇中走廊、川滇通道。全州总面积29,258平方公里，州境地势大致由西北向东南倾斜，东西最大横距175公里，南北最大纵距247.5公里。

### 地貌

境内多山，[山地面积占总面积的](../Page/山地.md "wikilink")90%以上，素有“九分山水一分坝”之称。[乌蒙山虎踞东部](../Page/乌蒙山.md "wikilink")，[哀牢山盘亘西南](../Page/哀牢山.md "wikilink")，[百草岭雄峙西北](../Page/百草岭.md "wikilink")，构成三山鼎立之势；[金沙江](../Page/金沙江.md "wikilink")、[元江两大水系以州境中部为分水岭各奔南北](../Page/元江.md "wikilink")，形成二水分流之态。州府所在地[鹿城海拔](../Page/鹿城.md "wikilink")1,773米，大致为全州坝区的一般海拔高度。在群山环抱之间，有104个面积在1平方公里以上的[盆地](../Page/盆地.md "wikilink")（坝子）星罗棋布，形成州内一个个规模不同、独具特色的经济、文化区域。

### 气候

楚雄州境气候属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，但由于山高谷深，气候垂直变化明显。全州总的气候特征是冬夏季短、春秋季长；日温差大、年温差小；冬无严寒、夏无酷暑；干湿分明、雨热同季；日照充足，霜期较短；降水偏少，春夏旱重。年均气温为14.
8～21.9
摄氏度。绝大多数地区最冷月（1月）平均气温在7.4摄氏度；最热月（6月）平均气温21.4摄氏度。极端最高气温42摄氏度（1963年5月31日），极端最低气温-8.4摄氏度（1974年1月1日）。全州降水量偏少，年均[降水量](../Page/降水量.md "wikilink")800～1000毫米，且主要集中在7月至10月。楚雄州地处云南省日照高值区，年均日照为2,450小时，从西北向东南呈递减分布。全州的蒸发量年平均为2,432毫米，为年降雨量的3倍多。

## 资源

### 土地资源

楚雄州土地总面积4,388.7万亩，其中耕地238.36万亩，水田123.95万亩。土壤共有19个类，其中耕作土壤类14个，自然土壤类5个，以[紫色土分布最广](../Page/紫色土.md "wikilink")，[红壤次之](../Page/红壤.md "wikilink")。此外，[水稻土是最主要的耕作土壤](../Page/水稻土.md "wikilink")，全州有128万亩，主要分布在平坝地区。

### 矿产资源

州内地质构造复杂，[矿产资源丰富](../Page/矿.md "wikilink")，种类涉及41个矿种，产地和矿化地达431处。其中，[铜](../Page/铜.md "wikilink")、[铁](../Page/铁.md "wikilink")、[砷](../Page/砷.md "wikilink")、[岩盐](../Page/岩盐.md "wikilink")、[芒硝](../Page/芒硝.md "wikilink")、[石膏等可称优势矿种](../Page/石膏.md "wikilink")，[煤](../Page/煤.md "wikilink")、铁、[石油](../Page/石油.md "wikilink")、[天然气等储量较丰富](../Page/天然气.md "wikilink")，其他还分布有[金](../Page/金.md "wikilink")、[银](../Page/银.md "wikilink")、[铅](../Page/铅.md "wikilink")、[大理石](../Page/大理石.md "wikilink")、[石棉](../Page/石棉.md "wikilink")、[磷](../Page/磷.md "wikilink")、[铂等矿藏](../Page/铂.md "wikilink")。

### 动植物资源

楚雄州的植物种类有6000多种，主要是[森林](../Page/森林.md "wikilink")、[中草药](../Page/中草药.md "wikilink")、[野生食用菌等](../Page/野生食用菌.md "wikilink")。州内常见的树种有[云南松](../Page/云南松.md "wikilink")、[华山松](../Page/华山松.md "wikilink")、[滇油杉](../Page/滇油杉.md "wikilink")、[金丝桃](../Page/金丝桃.md "wikilink")、[滇橄榄](../Page/滇橄榄.md "wikilink")、[杜鹃](../Page/杜鹃.md "wikilink")、[冬瓜树等](../Page/冬瓜树.md "wikilink")。[草本植物以](../Page/草本植物.md "wikilink")[香茅](../Page/香茅.md "wikilink")、[龙须草](../Page/龙须草.md "wikilink")、[野古草](../Page/野古草.md "wikilink")、[金球花为最多](../Page/金球花.md "wikilink")。药用植物以[薄荷](../Page/薄荷.md "wikilink")、[大黄](../Page/大黄.md "wikilink")、[黄连](../Page/黄连.md "wikilink")、[茯苓最为有名](../Page/茯苓.md "wikilink")。州境有野生[哺乳动物](../Page/哺乳动物.md "wikilink")110多种、[鸟类有](../Page/鸟类.md "wikilink")390多种、[爬行类](../Page/爬行类.md "wikilink")66种、[两栖类](../Page/两栖类.md "wikilink")34种、[鱼类](../Page/鱼类.md "wikilink")85种。

## 政治

### 现任领导

<table>
<caption>楚雄彝族自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党楚雄彝族自治州委员会.md" title="wikilink">中国共产党<br />
楚雄彝族自治州委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/楚雄彝族自治州人民代表大会.md" title="wikilink">楚雄彝族自治州人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/楚雄彝族自治州人民政府.md" title="wikilink">楚雄彝族自治州人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议楚雄彝族自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
楚雄彝族自治州委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/杨斌_(1966年).md" title="wikilink">杨斌</a>[1]</p></td>
<td><p><a href="../Page/任锦云.md" title="wikilink">任锦云</a>[2]</p></td>
<td><p><a href="../Page/迟中华.md" title="wikilink">迟中华</a>[3]</p></td>
<td><p><a href="../Page/杨静_(1962年).md" title="wikilink">杨静</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/彝族.md" title="wikilink">彝族</a></p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>彝族</p></td>
<td><p>彝族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td></td>
<td></td>
<td></td>
<td><p>云南省<a href="../Page/大姚县.md" title="wikilink">大姚县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年8月</p></td>
<td><p>2016年6月</p></td>
<td><p>2017年12月</p></td>
<td><p>2016年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

下辖1个[县级市](../Page/县级市.md "wikilink")、9个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 县级市：[楚雄市](../Page/楚雄市.md "wikilink")
  - 县：[双柏县](../Page/双柏县.md "wikilink")、[牟定县](../Page/牟定县.md "wikilink")、[南华县](../Page/南华县.md "wikilink")、[姚安县](../Page/姚安县.md "wikilink")、[大姚县](../Page/大姚县.md "wikilink")、[永仁县](../Page/永仁县.md "wikilink")、[元谋县](../Page/元谋县.md "wikilink")、[武定县](../Page/武定县.md "wikilink")、[禄丰县](../Page/禄丰县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>楚雄彝族自治州行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>镇</p></td>
</tr>
<tr class="even">
<td><p>532300</p></td>
</tr>
<tr class="odd">
<td><p>532301</p></td>
</tr>
<tr class="even">
<td><p>532322</p></td>
</tr>
<tr class="odd">
<td><p>532323</p></td>
</tr>
<tr class="even">
<td><p>532324</p></td>
</tr>
<tr class="odd">
<td><p>532325</p></td>
</tr>
<tr class="even">
<td><p>532326</p></td>
</tr>
<tr class="odd">
<td><p>532327</p></td>
</tr>
<tr class="even">
<td><p>532328</p></td>
</tr>
<tr class="odd">
<td><p>532329</p></td>
</tr>
<tr class="even">
<td><p>532331</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>楚雄彝族自治州各市（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>楚雄彝族自治州</p></td>
<td><p>2684169</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>楚雄市</p></td>
<td><p>588620</p></td>
<td><p>21.93</p></td>
</tr>
<tr class="even">
<td><p>双柏县</p></td>
<td><p>159867</p></td>
<td><p>5.96</p></td>
</tr>
<tr class="odd">
<td><p>牟定县</p></td>
<td><p>208726</p></td>
<td><p>7.78</p></td>
</tr>
<tr class="even">
<td><p>南华县</p></td>
<td><p>236133</p></td>
<td><p>8.80</p></td>
</tr>
<tr class="odd">
<td><p>姚安县</p></td>
<td><p>197676</p></td>
<td><p>7.36</p></td>
</tr>
<tr class="even">
<td><p>大姚县</p></td>
<td><p>273315</p></td>
<td><p>10.18</p></td>
</tr>
<tr class="odd">
<td><p>永仁县</p></td>
<td><p>109304</p></td>
<td><p>4.07</p></td>
</tr>
<tr class="even">
<td><p>元谋县</p></td>
<td><p>215795</p></td>
<td><p>8.04</p></td>
</tr>
<tr class="odd">
<td><p>武定县</p></td>
<td><p>271963</p></td>
<td><p>10.13</p></td>
</tr>
<tr class="even">
<td><p>禄丰县</p></td>
<td><p>422770</p></td>
<td><p>15.75</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")2684174人\[8\]。同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加141709人，增长5.57%。平均每年增加13729人，年平均增长率为0.54%。其中，男性为1374455人，占总人口的51.21%；女性为1309719人，占总人口的48.79%。总人口性别比（以女性为100）为104.94。0－14岁的人口为474124人，占总人口的17.66%；15－64岁的人口为1981881人，占总人口的73.84%；65岁及以上的人口为228169人，占总人口的8.5%。居住在城镇的人口为859473人，占总人口的32%；居住在乡村的人口为1824701人，占总人口的68%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")1796852人，占总人口的66.9%；各[少数民族人口为](../Page/少数民族.md "wikilink")887322人，占总人口的33.1%。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [彝族](../Page/彝族.md "wikilink") | [傈僳族](../Page/傈僳族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | [傣族](../Page/傣族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [白族](../Page/白族.md "wikilink") | [哈尼族](../Page/哈尼族.md "wikilink") | [壮族](../Page/壮族.md "wikilink") | [纳西族](../Page/纳西族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | ---- |
| 人口数          | 1796847                        | 716627                         | 53114                            | 46295                          | 21521                          | 21127                          | 16474                          | 6176                             | 1287                           | 759                              | 3942 |
| 占总人口比例（%）    | 66.94                          | 26.70                          | 1.98                             | 1.72                           | 0.80                           | 0.79                           | 0.61                           | 0.23                             | 0.05                           | 0.03                             | 0.15 |
| 占少数民族人口比例（%） | \---                           | 80.76                          | 5.99                             | 5.22                           | 2.43                           | 2.38                           | 1.86                           | 0.70                             | 0.15                           | 0.09                             | 0.44 |

**楚雄彝族自治州民族构成（2010年11月）**\[9\]

## 名胜古迹

素有“西南第一山”之称的[武定](../Page/武定县.md "wikilink")[狮子山](../Page/武定狮子山.md "wikilink")，风景秀丽，元代建筑[正续禅寺古刹庄严](../Page/正续禅寺.md "wikilink")，为云南省级风景名胜区；与武定相邻的[元谋县](../Page/元谋.md "wikilink")，便是著名的“[元谋猿人](../Page/元谋猿人.md "wikilink")”化石发掘地；[永仁](../Page/永仁.md "wikilink")[方山山顶平阔](../Page/方山.md "wikilink")，松涛呼啸，极目远眺，金沙江蜿蜓于群山峡谷之间；[大姚的](../Page/大姚.md "wikilink")[唐代](../Page/唐代.md "wikilink")[白塔](../Page/大姚白塔.md "wikilink")、[明代的](../Page/明代.md "wikilink")[石羊孔庙是早期](../Page/石羊孔庙.md "wikilink")[佛教和](../Page/佛教.md "wikilink")[儒家文化在此流传的](../Page/儒家文化.md "wikilink")[历史遗存](../Page/历史.md "wikilink")；楚雄[紫溪山于](../Page/紫溪山.md "wikilink")[明](../Page/明.md "wikilink")[清时为省内佛教圣地](../Page/清.md "wikilink")，成为旅游和避暑胜地；被誉为“化石王国”的[禄丰](../Page/禄丰.md "wikilink")，曾发掘出世界上第一个[腊玛古猿头骨化石](../Page/腊玛古猿.md "wikilink")，是研究人类起源的重要区域，又是著名的[恐龙化石产地](../Page/恐龙.md "wikilink")，建有[恐龙化石](../Page/恐龙化石.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")。

## 友好地区

  - \[10\]

## 参考资料

## 外部链接

  - [云南楚雄彝族自治州网站](http://www.cxz.gov.cn/)

[楚雄](../Category/楚雄.md "wikilink")
[Category:云南自治州](../Category/云南自治州.md "wikilink")
[云](../Category/彝族自治州.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.