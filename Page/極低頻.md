[Clam_Lake_ELF.jpg](https://zh.wikipedia.org/wiki/File:Clam_Lake_ELF.jpg "fig:Clam_Lake_ELF.jpg")
**極低頻**（**E**xtremely **L**ow
**F**requency，**ELF**）\[1\]是指[頻率由](../Page/頻率.md "wikilink")3Hz至30Hz\[2\]，[波長](../Page/波長.md "wikilink")10,000公里至100,000公里的[無線電波](../Page/無線電波.md "wikilink")\[3\]\[4\]
。

極低頻[無線電波是](../Page/無線電波.md "wikilink")[閃電和自然擾動在](../Page/閃電.md "wikilink")[地球磁場中產生的](../Page/地球磁場.md "wikilink")，因此是[大氣科學中研究的主題之一](../Page/大氣科學.md "wikilink")。由於難以製造可以輻射這種長波的[天線](../Page/天線.md "wikilink")，因此極低頻[無線電波極少用於](../Page/無線電波.md "wikilink")[通信系統中](../Page/通信.md "wikilink")。ELF波可以穿透[海水](../Page/海水.md "wikilink")，這使它們可以用於與[潛艇的通信](../Page/潛艇.md "wikilink")。[美國](../Page/美國.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[印度和](../Page/印度.md "wikilink")[中華人民共和國是目前僅有已知建造ELF通信設施的國家](../Page/中華人民共和國.md "wikilink")。\[5\]\[6\]\[7\]\[8\]\[9\]\[10\]\[11\]\[12\]

## 應用

基於無線電於[鹹水的傳送特性](../Page/鹹水.md "wikilink")，除了極低頻和[超低頻外](../Page/超低頻.md "wikilink")，通常無線電波不容易在[海中傳送](../Page/海.md "wikilink")。雖然它可以在水中容易地傳送，但是ELF每分鐘可以傳送的訊號相對地較小，所以只是給美軍用作指示潛水艇進入／離開海底。另外要有效傳送極低頻信號需要大型的[天線](../Page/天線.md "wikilink")，所以極低頻的使用不太普遍。

一些國家的[電氣化鐵路使用低於一般](../Page/電氣化鐵路.md "wikilink")50/60
Hz[工頻的極低頻交流電](../Page/工頻.md "wikilink")，例如[德國](../Page/德國.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[挪威和](../Page/挪威.md "wikilink")[瑞典使用](../Page/瑞典.md "wikilink")15,000V
16.67Hz，美國使用11,000V或12,500V 25Hz的交流電。

## 参考文献

[Category:電磁波譜](../Category/電磁波譜.md "wikilink")

1.
2.
3.
4.
5.   at the [Federation of American Scientists
    website](http://www.fas.org/)
6.
7.
8.
9.
10.
11.
12.