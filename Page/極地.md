[LocationPolarRegions.png](https://zh.wikipedia.org/wiki/File:LocationPolarRegions.png "fig:LocationPolarRegions.png")
[Frozenground.gif](https://zh.wikipedia.org/wiki/File:Frozenground.gif "fig:Frozenground.gif")

[地球的極地為於地球兩極附近的地區](../Page/地球.md "wikilink")（[緯度](../Page/緯度.md "wikilink")66.5°以上）。[北極和](../Page/北極.md "wikilink")[南極為其中心地](../Page/南極.md "wikilink")；北極的[北冰洋和南極的](../Page/北冰洋.md "wikilink")[南極大陸皆被大量的冰層包圍](../Page/南極大陸.md "wikilink")。現時位於兩極的海冰正因人為[全球暖化而在溶化](../Page/全球暖化.md "wikilink")。

## 北極附近的地區

北極地區有為數不少的定居點。在北極地區擁有領土的國家包括[美國](../Page/美國.md "wikilink")（[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")）、[加拿大](../Page/加拿大.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")（[格陵蘭島](../Page/格陵蘭島.md "wikilink")）、[挪威和](../Page/挪威.md "wikilink")[俄羅斯](../Page/俄羅斯.md "wikilink")。在北極地區生活的人口和它們所屬的[國家都不太相同](../Page/國家.md "wikilink")，而生活在北極地區不同部分的人口更有著不一樣的生活文化。

## 南極和南冰洋

南極區沒有永久居住的居住點。美國的[麥克默多站為位於該處最大](../Page/麥克默多站.md "wikilink")[研究站](../Page/研究站.md "wikilink")，其他知名的研究站如美國的帕默站和[阿蒙森-史科特南極站](../Page/阿蒙森-史科特南極站.md "wikilink")，[阿根廷的](../Page/阿根廷.md "wikilink")[埃斯佩蘭薩基地](../Page/埃斯佩蘭薩基地.md "wikilink")，[紐西蘭的](../Page/紐西蘭.md "wikilink")[史科特基地和俄羅斯的](../Page/史科特基地.md "wikilink")[沃斯托克站](../Page/沃斯托克站.md "wikilink")。

雖然該地沒有人類的獨特文化，但卻有著複雜的生態系統，特別是南極地區的沿海地區。其沿海的[上升流為當地的](../Page/上升流.md "wikilink")[磷蝦提供大量的養份](../Page/磷蝦.md "wikilink")，而磷蝦則為當地大量的動物提供食物，如[企鵝和](../Page/企鵝.md "wikilink")[藍鯨](../Page/藍鯨.md "wikilink")。

## 其他行星的極地

[Mars_23_aug_2003_hubble.jpg](https://zh.wikipedia.org/wiki/File:Mars_23_aug_2003_hubble.jpg "fig:Mars_23_aug_2003_hubble.jpg")
除了地球，其他行星和衛星皆有有別於地球的極地特徵。月球的極地曾被推測擁有不少的冰；由於冰層反射陽光，結果使該地不能被看見。[火星則擁有明顯的極地冰層](../Page/火星.md "wikilink")，和地球類似。[天王星的極地與上述極地更為不同](../Page/天王星.md "wikilink")；由於其97.77°的傾角，其極地則交替地面向[太陽](../Page/太陽.md "wikilink")。

## 相關連結

### 北極

  - [北極地區](../Page/北極地區.md "wikilink")
  - [北極圈](../Page/北極圈.md "wikilink")
  - [北冰洋](../Page/北冰洋.md "wikilink")
  - [加拿大北極群島](../Page/北極群島.md "wikilink")

### 極

  - [兩極](../Page/兩極.md "wikilink")
      - [北極點](../Page/北極點.md "wikilink")
  - [南極點](../Page/南極點.md "wikilink")
  - [極地低壓](../Page/極地低壓.md "wikilink")
  - [極光](../Page/極光.md "wikilink")

## 外部連結

  -
  - [The Polar
    Regions](http://www.barrameda.com.ar/ecology/the-polar-regions.htm)

  - [International Polar Foundation](http://www.polarfoundation.org/)

  - [Arctic Environmental Atlas
    (UNDP)](https://web.archive.org/web/20061230092300/http://maps.grida.no/arctic/)

  - [Earth's Polar Regions on Windows to the
    Universe](http://www.windows.ucar.edu/polar.html)

  - [Arctic Studies Center, Smithsonian
    Institution](http://www.mnh.si.edu/arctic/index.html)

  - [Scott Polar Research Institute, University of
    Cambridge](http://www.spri.cam.ac.uk/)

  - [WWF:The Polar
    Regions](http://www.panda.org/news_facts/education/middle_school/habitats/polar_regions/index.cfm)
    \*[World Environment Day 2007 "Melting Ice" image gallery at The
    Guardian](http://www.guardian.co.uk/environment/gallery/2007/jun/05/photography?picture=329977040)
    \*[Polar Discovery](http://polardiscovery.whoi.edu/)

## 畫廊

Image:WhiteNightKinsarvik.jpg|北極地區 Image:Transantarctic mountain
hg.jpg|南極洲的山 Image:Polar Bear
2004-11-15.jpg|生活於北極地區的[北極熊](../Page/北極熊.md "wikilink")
Image:Pygoscelis papua.jpg|生活於南極的[巴布亞企鵝](../Page/巴布亞企鵝.md "wikilink")
Image:Polar scenes.jpg|極地一景 Image:Greenland ice stream.jpg|極地冰河
Image:The Earth seen from Apollo 17.jpg|地球的南極清晰可見
Image:Aurora2.jpg|紫色的[極光](../Page/極光.md "wikilink")

[Category:北极](../Category/北极.md "wikilink")
[Category:南极洲](../Category/南极洲.md "wikilink")
[Category:北冰洋](../Category/北冰洋.md "wikilink")
[Category:南冰洋](../Category/南冰洋.md "wikilink")