[Discrete_probability_distrib.svg](https://zh.wikipedia.org/wiki/File:Discrete_probability_distrib.svg "fig:Discrete_probability_distrib.svg")
在[概率论中](../Page/概率论.md "wikilink")，**概率质量函数**（probability mass
function，简写为**pmf**）是[离散随机变量在各特定取值上的概率](../Page/离散随机变量.md "wikilink")。**概率质量函数**和[概率密度函数不同之处在于](../Page/概率密度函数.md "wikilink")：概率质量函数是对[离散随机变量定义的](../Page/离散随机变量.md "wikilink")，本身代表该值的概率；概率密度函数是对[连续随机变量定义的](../Page/连续随机变量.md "wikilink")，本身不是概率，只有对连续随机变量的[概率密度函数在某区间内进行](../Page/概率密度函数.md "wikilink")[积分后才是概率](../Page/积分.md "wikilink")。

## 数学定义

假设*X*是一个定义在[可数](../Page/可数.md "wikilink")[样本空间](../Page/样本空间.md "wikilink")*S*上的离散随机变量 *S*
⊆ **R**，则其**概率质量函数** *f*<sub>*X*</sub>(*x*) 为

\[f_X(x) = \begin{cases} \Pr(X = x), &x\in S,\\0, &x\in \mathbb{R}\backslash S.\end{cases}\]
注意这在所有[实数上](../Page/实数.md "wikilink")，包括那些*X*不可能等于的实数值上，都定义了 *f*<sub>*X*</sub>(*x*)。在那些*X*不可能等于的实数值上， *f*<sub>*X*</sub>(*x*)取值为0 ( *x*
∈ **R**\\*S*，取Pr(*X* = *x*) 为0)。

[离散随机变量](../Page/离散随机变量.md "wikilink")**概率质量函数**的不连续性决定了其[累积分布函数也不连续](../Page/累积分布函数.md "wikilink")。

## 例子

假设*X*是抛硬币的结果，反面取值为0，正面取值为1。则在状态空间{0,
1}(这是一个[伯努利(Bernoulli)随机变量](../Page/伯努利分布.md "wikilink"))中，*X*
= *x*的概率是0.5，所以**概率质量函数**是

\[f_X(x) = \begin{cases}\frac{1}{2}, &x \in \{0, 1\}, \\0, &x \in \mathbb{R}\backslash\{0, 1\}.\end{cases}\]

**概率质量函数**可以定义在任何[离散随机变量上](../Page/离散随机变量.md "wikilink")，包括[常数分布](../Page/离散型均匀分布.md "wikilink"),
[二项分布](../Page/二项分布.md "wikilink")（包括[伯努利(Bernoulli)分布](../Page/伯努利分布.md "wikilink")）,
[负二项分布](../Page/负二项分布.md "wikilink"),
[泊松(Poisson)分布](../Page/泊松分布.md "wikilink"),
[几何分布以及](../Page/几何分布.md "wikilink")[超几何分布随机变量上](../Page/超几何分布.md "wikilink").

[Category:概率论](../Category/概率论.md "wikilink")
[Category:函数](../Category/函数.md "wikilink")