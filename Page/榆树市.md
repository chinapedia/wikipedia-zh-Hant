**榆树市**是[吉林省](../Page/吉林省.md "wikilink")[长春市东北部下辖的一个县级市](../Page/长春市.md "wikilink")，邻接[黑龙江省](../Page/黑龙江省.md "wikilink")。

榆树市为[中国肉类生产第五大县市](../Page/中国肉类生产百强县列表.md "wikilink")，境内有[陶榆铁路，榆舒铁路通过](../Page/陶榆铁路，榆舒铁路.md "wikilink")。

榆树市是产粮大市，盛产[大豆](../Page/大豆.md "wikilink")、[玉米等](../Page/玉米.md "wikilink")；榆树市生产的[冰峰啤酒](../Page/冰峰啤酒.md "wikilink")，清洌爽口，食品厂的[冰晶汽水也在市民中享有盛誉](../Page/冰晶汽水.md "wikilink")。

## 历史沿革

[宣统元年](../Page/宣统.md "wikilink")(1909年)6月2日设榆树直隶厅，隶于吉林省西北路分巡兵备道；1913年3月改称榆树县，隶于吉林省[滨江道](../Page/滨江道.md "wikilink")。

## 行政区划

下辖4个街道、16个镇、20个乡、1个民族乡：

  - 街道办事处：[华昌街道](../Page/华昌街道.md "wikilink")、[正阳街道](../Page/正阳街道.md "wikilink")、[培英街道](../Page/培英街道.md "wikilink")、[城郊街道](../Page/城郊街道.md "wikilink")。
  - 镇：[泗河镇](../Page/泗河镇.md "wikilink")、[大岭镇](../Page/大岭镇.md "wikilink")、[大坡镇](../Page/大坡镇.md "wikilink")、[怀家镇](../Page/怀家镇.md "wikilink")、[土桥镇](../Page/土桥镇.md "wikilink")、[新立镇](../Page/新立镇.md "wikilink")、[黑林镇](../Page/黑林镇.md "wikilink")、[五棵树镇](../Page/五棵树镇.md "wikilink")、[闵家镇](../Page/闵家镇.md "wikilink")、[向阳镇](../Page/向阳镇.md "wikilink")、[弓棚镇](../Page/弓棚镇.md "wikilink")、[保寿镇](../Page/保寿镇.md "wikilink")、[秀水镇](../Page/秀水镇.md "wikilink")、[刘家镇](../Page/刘家镇.md "wikilink")、[八号镇](../Page/八号镇.md "wikilink")、[新庄镇](../Page/新庄镇.md "wikilink")。
  - 乡：[环城乡](../Page/环城乡.md "wikilink")、[城发乡](../Page/城发乡.md "wikilink")、[李合乡](../Page/李合乡.md "wikilink")、[于家乡](../Page/于家乡.md "wikilink")、[青顶乡](../Page/青顶乡.md "wikilink")、[十四户乡](../Page/十四户乡.md "wikilink")、[光明乡](../Page/光明乡.md "wikilink")、[谢家乡](../Page/谢家乡.md "wikilink")、[福安乡](../Page/福安乡.md "wikilink")、[大于乡](../Page/大于乡.md "wikilink")、[先锋乡](../Page/先锋乡.md "wikilink")、[前进乡](../Page/前进乡.md "wikilink")、[武龙乡](../Page/武龙乡.md "wikilink")、[大岗乡](../Page/大岗乡.md "wikilink")、[双井乡](../Page/双井乡.md "wikilink")、[红星乡](../Page/红星乡.md "wikilink")、[恩育乡](../Page/恩育乡.md "wikilink")、[太安乡](../Page/太安乡.md "wikilink")、[育民乡](../Page/育民乡.md "wikilink")、[青山乡](../Page/青山乡.md "wikilink")。其中双井乡已经并入环城乡。
  - 民族乡：[延和朝鲜族乡](../Page/延和朝鲜族乡.md "wikilink")。

## 名人

  - [回良玉](../Page/回良玉.md "wikilink")
  - [高严](../Page/高严.md "wikilink")
  - [尹成杰](../Page/尹成杰.md "wikilink")

[榆树市](../Category/榆树市.md "wikilink")
[市](../Category/长春区县市.md "wikilink")
[长春](../Category/吉林县级市.md "wikilink")