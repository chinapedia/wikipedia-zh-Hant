**榆科**共有8[属约](../Page/属.md "wikilink")35[种](../Page/种.md "wikilink")，主要分布在北半球[温带区域](../Page/温带.md "wikilink")，大部分在[亚洲](../Page/亚洲.md "wikilink")，没有原生于[澳洲的品种](../Page/澳洲.md "wikilink")。[中国有](../Page/中国.md "wikilink")8属50余种，分布于全国。\[1\]\[2\]
1981年的[克朗奎斯特分类法将其分在](../Page/克朗奎斯特分类法.md "wikilink")[荨麻目中](../Page/荨麻目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG分类法认为应该分在](../Page/APG分类法.md "wikilink")[蔷薇目](../Page/蔷薇目.md "wikilink")，并将[朴属](../Page/朴属.md "wikilink")（*Celtis*）及相似的朴族几[属另划入](../Page/属.md "wikilink")[大麻科](../Page/大麻科.md "wikilink")（*Cannabaceae*）\[3\]

## 形态

  - [乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，顶芽通常早死，其下的腋芽代替，具芽鳞，稀裸露。
  - 单[叶互生](../Page/叶.md "wikilink")，稀对生，常二列，羽状脉或基部3出脉，稀基部5出脉或掌状3出脉，有柄；托叶常呈膜质，侧生或柄内生，早落。
  - 单被花，少数或多数排成聚伞花序，或呈簇生状，或单生，生叶腋、近新枝下部、近基部的苞腋；花被4～8裂片，常呈覆瓦状排列；雄蕊在蕾中直立，稀内曲，常与花被裂片同数、对生，花药2室，纵裂，花粉2～5孔或沟，扁圆形或扁球形；[雌蕊由](../Page/雌蕊.md "wikilink")2心皮连合而成，花柱极短，柱头2，[子房上位](../Page/子房.md "wikilink")，通常1室，具1枚倒生[胚珠](../Page/胚珠.md "wikilink")。
  - [核果或小](../Page/核果.md "wikilink")[坚果](../Page/坚果.md "wikilink")，有时小坚果具翅或附属物，顶端常有宿存的柱头；无[胚乳](../Page/胚乳.md "wikilink")。

## 用途

许多品种的[木材坚硬](../Page/木材.md "wikilink")，适合做家具或农具，树皮、树叶和嫩果可食，美国红榆的树皮可以作为药物镇痛剂，有的品种可以作为观赏树种。

## 参考文献

<references />

## 外部链接

  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](http://delta-intkey.com/angio/)中的[榆科](http://delta-intkey.com/angio/www/ulmaceae.htm)
  - [APG II
    网站中的榆科](http://www.mobot.org/MOBOT/Research/APweb/orders/rosalesweb.htm#Ulmaceae)

[\*](../Category/榆科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.

2.

3.