**榊原良子**，[日本資深女性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")、[演員](../Page/演員.md "wikilink")、[旁白](../Page/旁白.md "wikilink")。出身於[千葉縣](../Page/千葉縣.md "wikilink")\[1\]。[身高](../Page/身高.md "wikilink")158cm。B型[血](../Page/血型.md "wikilink")\[2\]。

## 簡介

現為[自由身](../Page/自由職業.md "wikilink")，原屬[Production
baobab](../Page/Production_baobab.md "wikilink")、[東京俳優生活協同組合（簡稱俳協）](../Page/東京俳優生活協同組合.md "wikilink")、[Combination](../Page/Combination_\(聲優事務所\).md "wikilink")（2017年11月1日離所）\[3\]。、[桐朋學園藝術短期大學畢業](../Page/桐朋學園藝術短期大學.md "wikilink")。

為人熟悉的代表配音作品有動畫《[六神合體](../Page/六神合體.md "wikilink")》的芙蘿蕾\[4\]、《[貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")》的淺谷光子\[5\]、《[風之谷](../Page/風之谷.md "wikilink")》的庫夏娜\[6\]\[7\]、《[機動戰士Z鋼彈](../Page/機動戰士Z_GUNDAM.md "wikilink")》的[哈曼·坎恩](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")\[8\]&瑪雅·法拉歐、《[SPACE
COBRA](../Page/哥普拉.md "wikilink")》女主角亞美女／Lady
Armaroid、《[機動警察](../Page/機動警察.md "wikilink")》的\[9\]\[10\]、《[攻殼機動隊
S.A.C.](../Page/攻殼機動隊_S.A.C._2nd_GIG.md "wikilink")》的茅葺總理〈茅葺洋子〉、《[機動新撰組
萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")》的阿涼、《》的葵、《[銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")》的[菲列特利加·格林希爾](../Page/銀河英雄傳說角色列表.md "wikilink")、《[美少女戰士SuperS](../Page/美少女戰士SuperS.md "wikilink")》的妮赫蕾妮亞、《[逮捕令](../Page/逮捕令_\(動畫\).md "wikilink")》系列的木下薰子\[11\]、《[Go！Princess
光之美少女](../Page/Go！Princess_光之美少女.md "wikilink")》的迪斯比婭、《[網球甜心](../Page/網球甜心.md "wikilink")》的龍崎麗香及《[Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")》西澤桃華的母親〈西澤櫻華〉等；和電視新聞節目《[NEWS
STATION](../Page/NEWS_STATION.md "wikilink")》和《》的旁白\[12\]\[13\]。

### 來歷

小學時期開始對[芝居產生興趣](../Page/芝居.md "wikilink")，國、高中時期參加過話劇社，大學時期就進演劇科系學習。據本人的專訪時表示，她在接演了各種舞台戲劇的時候，有配音員率團當時成立了新的經紀公司，接受舞台戲劇表演者的丈夫、也是後來-{榊}-原的恩師的邀請。之後榊原更收到經紀公司社長和配音業界前輩的邀請之下，全面投入配音業界\[14\]。

1981年，-{榊}-原以[電視動畫](../Page/電視動畫.md "wikilink")《[六神合體](../Page/六神合體.md "wikilink")》（聲演芙蘿蕾）成為她的配音出道作\[15\]。

\-{榊}-原除了從事動畫的配音之外，大多從事新聞節目的旁白解說工作，特別是由[朝日電視台播出的](../Page/朝日電視台.md "wikilink")「[NEWS
STATION](../Page/NEWS_STATION.md "wikilink")」是她擔任主要旁白解說超過10年的節目\[16\]。

2015年，-{榊}-原在[朝日放送播出](../Page/朝日放送.md "wikilink")、[東映動畫製作的電視動畫](../Page/東映動畫.md "wikilink")《[Go！Princess
光之美少女](../Page/Go！Princess_光之美少女.md "wikilink")》擔任過反派組織首領迪斯比婭的配音，而這也是她自從1995年的《[美少女戰士SuperS](../Page/美少女戰士SuperS.md "wikilink")》擔任反派組織首領妮赫蕾妮亞的配音以來，第2次在[朝日電視網播出](../Page/全日本新聞網.md "wikilink")、東映動畫製作的[魔法少女動畫中聲演反派的作品](../Page/魔法少女.md "wikilink")。

2017年1月9日，-{榊}-原在朝日電視台播放的節目《200位人氣聲優認真票選！聲優總選舉3小時SP》的排名中得到第17名\[17\]。

### 人物、特色

據本人表示，為海外電影《[殉情記](../Page/殉情記.md "wikilink")》主役演員[奧莉花·荷西進行日語配音是她印象最深刻的配音作品](../Page/奧莉花·荷西.md "wikilink")。她認為，能夠在學生時期做些當時根本無法做的事情，對自己來說是最好的回憶\[18\]。還有，負責茱麗葉一角日語配音的[音響監督](../Page/音響監督.md "wikilink")聽了-{榊}-原的聲音之後，進而邀請她參演《》和《[風之谷](../Page/風之谷.md "wikilink")》\[19\]，從此之後，她幫「強悍型女性」的配音機會增加。

\-{榊}-原的聲線低沉，時常演出成熟女性或反派角色，年輕時也曾配過女主角但為數不多，還包含喜劇角色等等。1982年，-{榊}-原配音生涯的第一部電影動畫《[SPACE
ADVENTURE
COBRA](../Page/哥普拉.md "wikilink")》主演時，母親在跟她母女2人一起收看這部動畫電影之後，母親則對她說「[藤田淑子的配音表現非常不錯](../Page/藤田淑子.md "wikilink")，妳的話還要再接再厲」\[20\]。

在鋼彈系列中，-{榊}-原雖然以[哈曼·坎恩這位要角而成名](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")，儘管她的表現初出茅廬，但總導演[富野由悠季卻對她不斷做出下跪之後連手帶腳地指導](../Page/富野由悠季.md "wikilink")、指摘她並叫她重來等等讓她覺得很可怕的行為。此外，在她的話題中很討厭談到作品之中「庸俗」的名台詞，這是因為每當我說一句「庸俗」相關台詞的時候會有內疚感。

\-{榊}-原從《[機動警察](../Page/機動警察.md "wikilink")》動畫系列聲演主要角色以來，經常參加導演[押井守執導的作品](../Page/押井守.md "wikilink")，並有好幾次擔任重要的角色，如押井擔任原作、編劇的廣播劇「」裡聲演主角。及《[攻殼機動隊](../Page/攻殼機動隊_\(電影\).md "wikilink")》的部分重製版「攻殻機動隊2.0」裡替代首映版由[家弓家正聲演的傀儡木偶](../Page/家弓家正.md "wikilink")。另一方面，[漫畫家](../Page/漫畫家.md "wikilink")[高橋留美子原作改編動畫](../Page/高橋留美子.md "wikilink")《[福星小子](../Page/福星小子.md "wikilink")》、《[相聚一刻](../Page/相聚一刻.md "wikilink")》、《[犬夜叉](../Page/犬夜叉.md "wikilink")》等作品的配音她都有參加，包含高橋擔任人物原案的《[機動新撰組
萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")》中聲演阿涼。

興趣有自種[家庭菜園](../Page/果菜園.md "wikilink")、[書法](../Page/書法.md "wikilink")、[料理](../Page/料理.md "wikilink")\[21\]。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1980年

<!-- end list -->

  - [凡爾賽玫瑰](../Page/凡爾賽玫瑰.md "wikilink")（侍女）

<!-- end list -->

  - 1981年

<!-- end list -->

  - [福星小子](../Page/福星小子.md "wikilink")（狐狸可樂餅的阿銀、女王陛下、魑魅亭的阿玉、口蝦蛄貝的入江的美女）
  - [六神合體Godmars](../Page/六神合體.md "wikilink")（芙蘿蕾）

<!-- end list -->

  - 1982年

<!-- end list -->

  - （疾風、冬子）

  - [SPACE COBRA](../Page/哥普拉.md "wikilink")（**亞美女**／**Lady
    Armaroid**\[22\]）

<!-- end list -->

  - 1983年

<!-- end list -->

  - [未來警察浦島人](../Page/未來警察浦島人.md "wikilink")（約瑟芬·坎茲伯格）
  - [貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")（**淺谷光子**）

<!-- end list -->

  - 1984年

<!-- end list -->

  - [玻璃假面 (1984年版)](../Page/玻璃假面.md "wikilink")

  - （**艾拉·穆**）

  - [大自然的魔獸巴奇](../Page/大自然的魔獸巴奇.md "wikilink")（村莊女性）

  - （相撲姊妹）

  - （麻美）

<!-- end list -->

  - 1985年

<!-- end list -->

  - [機動戰士Z鋼彈](../Page/機動戰士Z_GUNDAM.md "wikilink")（**[哈曼·坎恩](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")**、瑪雅·法拉歐）
  - [莎拉公主](../Page/小公主莎拉.md "wikilink")（佩琪的母親）
  - [搞怪拍檔](../Page/搞怪拍檔.md "wikilink")（菲爾）

<!-- end list -->

  - 1986年

<!-- end list -->

  - （帕拉諾亞\[23\]、海倫·特洛陽尼）

  - [機動戰士鋼彈ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")（哈曼·坎恩\[24\]）

  - [相聚一刻](../Page/相聚一刻.md "wikilink")（彩子）

<!-- end list -->

  - 1987年

<!-- end list -->

  - [超能力魔美](../Page/超能力魔美.md "wikilink")（佐倉菜穗子）

  - [橙路](../Page/橙路.md "wikilink")（[鮎川圓的母親](../Page/鮎川圓.md "wikilink")、牙醫）

  - （布萊頓夫人\[25\]）

<!-- end list -->

  - 1988年

<!-- end list -->

  - [城市獵人2](../Page/城市獵人.md "wikilink")（立木小百合）
  - [小公子西迪](../Page/小公子西迪.md "wikilink")（哈里斯夫人）

<!-- end list -->

  - 1989年

<!-- end list -->

  - [偶像英里子傳說](../Page/偶像英里子傳說.md "wikilink")（朝霧良子）

  - [機動警察](../Page/機動警察.md "wikilink")（****）

  - （女王）

  - [麵包超人](../Page/麵包超人.md "wikilink")（火〈初代〉）

<!-- end list -->

  - 1990年

<!-- end list -->

  - [偶像天使陽子](../Page/小俏妞.md "wikilink")（松島啓子）

  - （渡し守）

  - [長腿叔叔](../Page/長腿叔叔_\(動畫\).md "wikilink")（帕特森夫人）

<!-- end list -->

  - 1991年

<!-- end list -->

  - [快樂的嚕嚕米一家](../Page/姆明.md "wikilink")（艾蓮）

  - [崔普一家物語](../Page/崔普一家物語.md "wikilink")（蘿拉修女）

  - （芙蘿拉的學姊）

<!-- end list -->

  - 1994年

<!-- end list -->

  - [BLUE SEED](../Page/碧奇魂.md "wikilink")（**松平梓**）

<!-- end list -->

  - 1995年

<!-- end list -->

  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink")（少華）
  - [美少女戰士SuperS](../Page/美少女戰士SuperS.md "wikilink")（妮赫蕾妮亞）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [玩偶遊戲](../Page/孩子們的遊戲.md "wikilink")（蜜雪兒·哈米爾頓）
  - [逮捕令](../Page/逮捕令_\(動畫\)#第1期.md "wikilink")（木下薰子）
  - [聖天空戰記](../Page/聖天空戰記.md "wikilink")（瓦莉耶·法尼爾）
  - [美少女戰士Sailor Stars](../Page/美少女戰士Sailor_Stars.md "wikilink")（妮赫蕾妮亞）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（卡薩布蘭卡）
  - [科學情人](../Page/科學情人.md "wikilink")（山川的母親）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [女棒甲子園](../Page/女棒甲子園.md "wikilink")（冰室桂子\[26\]）
  - [夢幻拉拉](../Page/夢幻拉拉.md "wikilink")（篠原真實子）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（津久茂和美 ）

<!-- end list -->

  - 1999年

<!-- end list -->

  - 「被季節淘汰的聖誕節」（母親）

<!-- end list -->

  - 2001年

<!-- end list -->

  - （瑞秋·林克斯\[27\]）

  - [逮捕令 SECOND SEASON](../Page/逮捕令_\(動畫\)#第2期.md "wikilink")（木下薰子）

  - [HELLSING](../Page/Hellsing.md "wikilink")（**因特古拉·范布魯克·溫格慈·赫爾辛格**\[28\]）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [天地無用！GXP](../Page/天地無用！.md "wikilink")（柾木美砂樹）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [聖槍修女](../Page/聖槍修女.md "wikilink")（凱特修女\[29\]）

  - （月之女王）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [攻殼機動隊 S.A.C. 2nd
    GIG](../Page/攻殼機動隊_S.A.C._2nd_GIG.md "wikilink")（茅葺總理〈茅葺洋子〉）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [機動新撰組 萌之劍 TV](../Page/機動新撰組_萌之劍.md "wikilink")（**阿涼**）

  - （**葵**\[30\]）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")（西澤桃華的母親〈西澤櫻華〉）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [犬夜叉 完结篇](../Page/犬夜叉.md "wikilink")（殺生丸的母親）
  - [KIDDY GiRL-AND](../Page/KIDDY_GiRL-AND.md "wikilink")（亞妮絲）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [學園默示錄 HIGHSCHOOL OF THE DEAD](../Page/學園默示錄.md "wikilink")（高城百合子）
  - [COBRA THE ANIMATION](../Page/哥普拉.md "wikilink")（**亞美女**／**Lady
    Armaroid**）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [X戰警](../Page/X戰警.md "wikilink") （佐佐木結衣）
  - [日常](../Page/日常.md "wikilink")（第15話預告旁白）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [來自新世界](../Page/來自新世界.md "wikilink")（朝比奈富子）（朝比奈富子）
  - [聖鬥士星矢Ω](../Page/聖鬥士星矢Ω.md "wikilink")（梅蒂亞）
  - [PSYCHO-PASS](../Page/PSYCHO-PASS.md "wikilink")（禾生壌宗）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [惡魔的謎語](../Page/惡魔的謎語.md "wikilink")（百合目一）
  - [靈裝戰士](../Page/靈裝戰士.md "wikilink")（月光·月詠）
  - PSYCHO-PASS 2（禾生壌宗）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [晨曦公主](../Page/晨曦公主.md "wikilink")（祈莞船長）
  - [艦隊Collection -艦Colle-](../Page/艦隊收藏.md "wikilink")（旁白、飛行場姬、中間棲姬）
  - [GANGSTA.](../Page/GANGSTA..md "wikilink")（吉娜·帕可麗\[31\]）
  - [Go！Princess
    光之美少女](../Page/Go！Princess_光之美少女.md "wikilink")（迪斯比婭\[32\]）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [少年女僕](../Page/少年女僕.md "wikilink")（鷹取一砂\[33\]）
  - [啟航吧！編舟計畫](../Page/啟航吧！編舟計畫.md "wikilink")（佐佐木薰\[34\]）

<!-- end list -->

  - 2017年

<!-- end list -->

  - （旁白\[35\]\[36\]）

  - [從零開始的魔法書](../Page/從零開始的魔法書.md "wikilink")（索雷娜）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [妖精森林的小不點](../Page/妖精森林的小不點.md "wikilink")（綠尾老）
  - [邪神與廚二病少女](../Page/邪神與廚二病少女.md "wikilink")（電視旁白）
  - [RErideD -穿越時空的德希達-](../Page/RErideD_-穿越時空的德希達-.md "wikilink")（瑪蓮娜）

### OVA

  - 1983年

<!-- end list -->

  - （**梅琳達·哈斯特**）

<!-- end list -->

  - 1985年

<!-- end list -->

  - [戰區88](../Page/戰區88.md "wikilink")（安田妙子）

  - （松沼薰）

  - （米莫·柯泰爾）

  - [VAMPIRE HUNTER D](../Page/吸血鬼獵人D.md "wikilink")（蛇女）

<!-- end list -->

  - 1986年

<!-- end list -->

  - （里緒）

  - （）

  - [無限地帶23 PARTII 要·保·密·喔](../Page/無限地帶23.md "wikilink")（玲奈）

<!-- end list -->

  - 1987年

<!-- end list -->

  - [風與木之詩 三聖頌](../Page/風與木之詩#OVA.md "wikilink")（羅斯馬林）

  - [超獸機神斷空我 GOD BLESS DANCOUGA](../Page/超獸機神.md "wikilink")（敷島麗美）

  - [吹泡糖危機](../Page/吹泡糖危機.md "wikilink")（**西莉雅·史汀古尼**）

  - BUBBLEGUM CRASH！（**西莉雅·史汀古尼**）

  - （西貝爾）

  - （凱洛琳小姐）

<!-- end list -->

  - 1988年

<!-- end list -->

  - [網球甜心2](../Page/網球甜心.md "wikilink")（**龍崎麗香**\[37\]）
  - [機動警察 (前期OVA版)](../Page/機動警察#前期OVA版.md "wikilink")（南雲忍）
  - [哭泣殺神](../Page/哭泣殺神.md "wikilink")（巴格那克）

<!-- end list -->

  - 1989年

<!-- end list -->

  - 網球甜心 Final Stage（**龍崎麗香**）

  - [機動戰士鋼彈0080
    口袋裡的戰爭](../Page/機動戰士GUNDAM_0080：口袋裡的戰爭.md "wikilink")（老師\[38\]、女）

  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")（**[菲列特利加·格林希爾](../Page/銀河英雄傳說角色列表.md "wikilink")**\[39\]）

  - （塔妮雅少佐）

  - （**神秘女性**）

  - [沙羅曼蛇](../Page/沙羅曼蛇_\(OVA\).md "wikilink")（史蒂芬妮的母親）

<!-- end list -->

  - 1990年

<!-- end list -->

  - （海拉／堤麻彌子）

  - （小夜子）

  - [SD鋼彈外傳III
    阿爾嘉斯騎士團](../Page/SD_GUNDAM外傳.md "wikilink")（侍女哈曼、咒語師丘貝雷／美杜莎丘貝雷）

  - （[哈曼·坎恩](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")、玖邊麗）

  - 機動戰士SD鋼彈 MARK-V（丘貝雷）

  - [永井豪的Q版世界](../Page/永井豪的Q版世界.md "wikilink")（賽蓮）

  - （莎拉）

  - [惡魔人 妖鳥死麗濡篇](../Page/惡魔人.md "wikilink")（賽蓮）

  - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")（卡拉、蕾莉亞）

<!-- end list -->

  - 1991年

<!-- end list -->

  - （波拉·普雷利）

  - [機動戰士鋼彈0083：Stardust
    Memory](../Page/機動戰士GUNDAM_0083：Stardust_Memory.md "wikilink")（哈曼·坎恩）

  - [橙路
    (第3期)：口紅留言](../Page/橙路#第三期.md "wikilink")（[鮎川圓的母親](../Page/鮎川圓.md "wikilink")）

  - [聖傳 -RG VEDA-](../Page/聖傳_\(漫畫\).md "wikilink")（迦樓羅王）

  - [忍者龍劍傳](../Page/忍者外傳系列.md "wikilink")（莎拉）

<!-- end list -->

  - 1992年

<!-- end list -->

  - 突走！（西莉雅·史汀古尼）

  - [天地無用！魎皇鬼](../Page/天地無用！.md "wikilink")（美砂樹）

<!-- end list -->

  - 1993年

<!-- end list -->

  - [超時空世紀歐卡斯02](../Page/超時空世紀.md "wikilink")（米蘭）

  - （牧子）

<!-- end list -->

  - 1995年

<!-- end list -->

  - （小暮女）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [機動戰士鋼彈 第08MS小隊](../Page/機動戰士GUNDAM第08MS小隊.md "wikilink")（）

  - （榊原京子）

  - [BLUE SEED2](../Page/碧奇魂.md "wikilink")（松平梓）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [特勤機甲隊·計劃α](../Page/特勤機甲隊.md "wikilink")（戴寶拉·休斯）

<!-- end list -->

  - 1999年

<!-- end list -->

  - （菲布）

<!-- end list -->

  - 2001年

<!-- end list -->

  - （瑞秋·林克斯）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [機動新撰組 萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")（阿涼）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [攻殼機動隊 S.A.C. Solid State
    Society](../Page/攻殼機動隊_S.A.C._Solid_State_Society.md "wikilink")（茅葺首相）
  - [HELLSING](../Page/Hellsing.md "wikilink")（**因特古拉·范布魯克·溫格慈·赫爾辛格**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [COBRA THE ANIMATION](../Page/哥普拉.md "wikilink")（**亞美女**／**Lady
    Armaroid**\[40\]）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [DARKER THAN BLACK -黑之契約者-
    外傳](../Page/DARKER_THAN_BLACK.md "wikilink")（光）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [SUPERNATURAL：THE ANIMATION](../Page/邪惡力量.md "wikilink")（美女〈阿薩謝爾〉）

### 電影動畫

  - 1982年

<!-- end list -->

  - [SPACE ADVENTURE COBRA](../Page/哥普拉.md "wikilink")（**亞美女**／**Lady
    Armaroid**\[41\]）

<!-- end list -->

  - 1983年

<!-- end list -->

  - （**艾爾**）

<!-- end list -->

  - 1984年

<!-- end list -->

  - [風之谷](../Page/風之谷.md "wikilink")（**庫夏娜**\[42\]）

<!-- end list -->

  - 1986年

<!-- end list -->

  - （羅阿羅）

  - 哀惑星（安特瓦涅特）

<!-- end list -->

  - 1987年

<!-- end list -->

  - （抜刀惠）

<!-- end list -->

  - 1988年

<!-- end list -->

  - [超能力魔美：星空上的跳舞娃娃](../Page/超能力魔美.md "wikilink")（佐倉魔美的媽媽〈佐倉菜穗子〉）

  - [機動戰士鋼彈 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（\[43\]）

  - （[哈曼·坎恩](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")）

  - [相聚一刻 完結篇](../Page/相聚一刻.md "wikilink")（黑木小夜子）

<!-- end list -->

  - 1989年

<!-- end list -->

  - [SD戰國傳 暴終空城之章](../Page/SD戰國傳_武者七人眾篇.md "wikilink")（玖邊麗）
  - [機動警察 the Movie](../Page/機動警察.md "wikilink")（****）
  - 機動戰士SD鋼彈：逆襲狂風暴雨的學園祭（哈曼·坎恩）

<!-- end list -->

  - 1990年

<!-- end list -->

  - （火）

<!-- end list -->

  - 1991年

<!-- end list -->

  - （冷泉寺貴緒）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [機動戰士鋼彈0083：吉翁的殘光](../Page/機動戰士GUNDAM_0083：Stardust_Memory.md "wikilink")（哈曼·坎恩）

<!-- end list -->

  - 1993年

<!-- end list -->

  - 機動警察2 the Movie（**南雲忍**）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [銀河鐵道999：永恒幻想](../Page/銀河鐵道999#劇場版.md "wikilink")（海爾瑪莎利亞）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [機動戰士鋼彈 第08MS小隊 米拉的報告書](../Page/機動戰士GUNDAM第08MS小隊.md "wikilink")（）

<!-- end list -->

  - 1999年

<!-- end list -->

  - [逮捕令 the MOVIE](../Page/逮捕令_\(動畫\)#劇場版.md "wikilink")（木下薰子）
  - [劇場版 數碼寶貝大冒險](../Page/數碼寶貝大冒險#劇場版.md "wikilink")（八神太一的母親〈八神裕子〉）

<!-- end list -->

  - 2000年

<!-- end list -->

  - [Ghiblies](../Page/Ghiblies.md "wikilink")（由佳里小姐）

<!-- end list -->

  - 2001年

<!-- end list -->

  - BLUE REMAINS（梅莎米克）

<!-- end list -->

  - 2002年

<!-- end list -->

  - （**南雲忍**）\[44\]

<!-- end list -->

  - 2004年

<!-- end list -->

  - [攻殼機動隊2 INNOCENCE](../Page/攻殼機動隊2_INNOCENCE.md "wikilink")（哈樂葳驗屍官）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [劇場版 機動戰士Z鋼彈II：A New Translation
    -戀人們-](../Page/機動戰士Z_GUNDAM劇場版.md "wikilink")（**哈曼·坎恩**）
  - [名偵探柯南：水平線上的陰謀](../Page/名偵探柯南：水平線上的陰謀.md "wikilink")（秋吉美波子\[45\]）

<!-- end list -->

  - 2006年

<!-- end list -->

  - 劇場版 機動戰士Z鋼彈III：A New Translation -星之鼓動是愛-（**哈曼·坎恩**）

  - （榊原芳子）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [攻殼機動隊2.0](../Page/攻殼機動隊_\(電影\).md "wikilink")（傀儡師）
  - [空中殺手 The Sky Crawlers](../Page/空中殺手#劇場版動畫.md "wikilink")（笹倉永久）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [劇場版
    交響詩篇艾蕾卡7：口袋裏的彩虹](../Page/交響詩篇#交響詩篇艾蕾卡7_彩虹滿載.md "wikilink")（老年的安妮莫奈）

<!-- end list -->

  - 2010年

<!-- end list -->

  - （雀蜂女王、彭巧的媽媽、部蘭卡的媽媽）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [花之詩女 GOTHICMADE](../Page/花之詩女_GOTHICMADE.md "wikilink")（旁白\[46\]）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [劇場版 PSYCHO-PASS](../Page/PSYCHO-PASS.md "wikilink")（禾生壌宗）
  - [和諧](../Page/和諧_\(小說\).md "wikilink")（奧斯卡·史陶芬堡\[47\]）
  - [名偵探柯南：業火的向日葵](../Page/名偵探柯南：業火的向日葵.md "wikilink")（圭子·安德森\[48\]）

### 網路動畫

  - 2014年

<!-- end list -->

  - [Bonjour♪ 戀味點心學園](../Page/Bonjour♪_戀味點心學園.md "wikilink")（皆川撫子）

### 遊戲

  - 1991年

<!-- end list -->

  - （不破沙織）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [夢幻戰士](../Page/夢幻戰士.md "wikilink")（瓦莉雅）
  - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")（卡拉）\[49\]

<!-- end list -->

  - 1994年

<!-- end list -->

  - 羅德斯島戰記 英雄戰爭（卡拉）

<!-- end list -->

  - 1995年

<!-- end list -->

  - [3×3EYES ～吸精公主～](../Page/三只眼.md "wikilink")（蘭妃）

<!-- end list -->

  - 1996年

<!-- end list -->

  - （漢娜）

  - GOTHA II 天空騎士（沃莉絲）

  - 3×3EYES ～吸精公主～ S（蘭妃）

  - 少女之夢（鈴無響子）

<!-- end list -->

  - 1997年

<!-- end list -->

  - SD鋼彈 GCENTURY（哈曼·坎恩、瑪雅·法拉歐）

  - [機動戰士Z鋼彈](../Page/機動戰士Z_GUNDAM.md "wikilink")（瑪雅·法拉歐）

  - 機動戰士Z鋼彈 前篇 Z的鼓動（瑪雅·法拉歐）

  - 機動戰士Z鋼彈 後篇 在宇宙中馳騁（哈曼·坎恩、瑪雅·法拉歐）

  - （**[菲列特利加·格林希爾](../Page/銀河英雄傳說角色列表.md "wikilink")**）

  - [超級機器人大戰F](../Page/超級機器人大戰F.md "wikilink")（哈曼·坎恩）

  - [BS火焰之纹章 阿卡內亞戰記 第2話
    紅龍騎士](../Page/BS火焰之纹章_阿卡內亞戰記.md "wikilink")（彌涅耳瓦／龍騎士）

  - BS火焰之纹章 阿卡內亞戰記 第3話 正義的盗賊集團（瑪莉絲／傭兵）

  - （阿哈德·卡非）

<!-- end list -->

  - 1998年

<!-- end list -->

  - （工藤桂子）

  - [SD鋼彈 G世代](../Page/SD_GUNDAM_G世代.md "wikilink")（哈曼·坎恩、瑪雅·法拉歐）

  - SD鋼彈 GCENTURY S（哈曼·坎恩、瑪雅·法拉歐）

  - [機動戰士鋼彈 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（娜奈·米格爾）

  - （庫絲可·艾爾）

  - [櫻花大戰2
    ～願君平安～](../Page/櫻花大戰2_～願君平安～.md "wikilink")（蘇麗妲·織姬的母親〈卡麗諾·蘇麗妲〉）

  - [超級機器人大戰F完結篇](../Page/超級機器人大戰F.md "wikilink")（哈曼·坎恩、瑪雅·法拉歐）

<!-- end list -->

  - 1999年

<!-- end list -->

  - SD鋼彈 G世代-ZERO（哈曼·坎恩、瑪雅·法拉歐、娜奈·米格爾、庫絲可·艾爾）

  - [北方戀曲 ～White Illumination～](../Page/北方戀曲.md "wikilink")（椎名薫）

  - 北方戀曲 ～Photo Memories～（椎名薰）

  - （永堀佳美）

  - [超級機器人大戰完全版](../Page/超級機器人大戰完全版.md "wikilink")（哈曼·坎恩、瑪雅·法拉歐、娜奈·米格爾）

  - [D之食卓2](../Page/D之食卓2.md "wikilink")（Lucy）

<!-- end list -->

  - 2000年

<!-- end list -->

  - SD鋼彈 G世代-F（哈曼·坎恩、瑪雅·法拉歐）
  - 機動戰士鋼彈 基連的野望 吉翁的系譜（哈曼·坎恩、瑪雅·法拉歐、庫絲可·艾爾）
  - THE LOST ONE Last chapter of EVE（工藤桂子）
  - [超級機器人大戰α](../Page/超級機器人大戰α.md "wikilink")（哈曼·坎恩、瑪雅·法拉歐）

<!-- end list -->

  - 2001年

<!-- end list -->

  - SD鋼彈 G世代-F.I.F（哈曼·坎恩、瑪雅·法拉歐）
  - 機動戰士Z鋼彈 Typing Zeta（哈曼·坎恩、瑪雅·法拉歐）
  - [COBRA 魂打](../Page/哥普拉.md "wikilink")（亞美女／Lady Armaroid）
  - [超級機器人大戰α外傳](../Page/超級機器人大戰α外傳.md "wikilink")（瑪雅·法拉歐）

<!-- end list -->

  - 2002年

<!-- end list -->

  - （偉大代表者）

  - SD鋼彈 G世代-NEO（哈曼·坎恩、瑪雅·法拉歐）

  - [機動新撰組 萌之劍](../Page/機動新撰組_萌之劍.md "wikilink")（阿良）

  - （哈曼·坎恩、庫絲可·艾爾、）

  - [機動戰士鋼彈戰記 Lost War
    Chronicles](../Page/機動戰士GUNDAM戰記_Lost_War_Chronicles.md "wikilink")（）

  - （哈曼·坎恩、娜奈·米格爾）

  - （帕茵、德克薩斯）

  - [命運傳奇2](../Page/命運傳奇2.md "wikilink")（**艾露蓮**）

  - （蕾卡）

<!-- end list -->

  - 2003年

<!-- end list -->

  - （哈曼·坎恩、瑪雅·法拉歐）

  - [第2次超級機器人大戰α](../Page/第2次超級機器人大戰α.md "wikilink")（哈曼·坎恩、娜奈·米格爾）

<!-- end list -->

  - 2004年

<!-- end list -->

  - SD鋼彈 G世代-SEED（哈曼·坎恩、瑪雅·法拉歐）

  - （哈曼·坎恩、瑪雅·法拉歐）

  - （哈曼·坎恩、瑪雅·法拉歐）

  - （拉德麗奴·米莉雅·歐文）

<!-- end list -->

  - 2005年

<!-- end list -->

  - （亞美女）

  - （波莉絲）

  -
<!-- end list -->

  - 2006年

<!-- end list -->

  - SD鋼彈 G世代 PORTABLE（哈曼·坎恩、瑪雅·法拉歐、娜奈·米格爾）

  - [機動戰士鋼彈 CLIMAX
    U.C.](../Page/MOBILE_SUIT_GUNDAM_CLIMAX_U.C..md "wikilink")（哈曼·坎恩、娜奈·米格爾）

  - （自由風人）

  - （淀大人）

  - [最终幻想XII](../Page/最终幻想XII.md "wikilink")（维埃拉之長：尤特）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [ASH -ARCHAIC SEALED HEAT-](../Page/遠古封印之炎.md "wikilink")（艾絲辛女王）

  - SD鋼彈 G世代 SPIRITS（哈曼·坎恩、娜奈·米格爾）

  - [鋼彈激鬥年代記](../Page/GUNDAM激鬥年代記.md "wikilink")（哈曼·坎恩）

  - [鋼彈無雙](../Page/GUNDAM無雙.md "wikilink")（哈曼·坎恩）

  - （哈曼·坎恩）

<!-- end list -->

  - 2008年

<!-- end list -->

  - （哈曼·坎恩）

  - 鋼彈無雙2（哈曼·坎恩）

  - [Crysis Warhead](../Page/末日之戰：獵殺悍將.md "wikilink")（艾默森中佐）

  - （蒂雅朵拉）

  - [白騎士物語](../Page/白騎士物語.md "wikilink")（馴龍專家）

  - [超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink")（哈曼·坎恩）

<!-- end list -->

  - 2009年

<!-- end list -->

  - SD鋼彈 G世代 WARS（哈曼·坎恩、娜奈·米格爾）

  - （哈曼·坎恩）

<!-- end list -->

  - 2010年

<!-- end list -->

  - （哈曼·坎恩）

  - 鋼彈無雙3（哈曼·坎恩）

  - （哈曼·坎恩）

<!-- end list -->

  - 2011年

<!-- end list -->

  - SD鋼彈 G世代 WORLD（哈曼·坎恩、娜奈·米格爾）

  - SD鋼彈 G世代 3D（哈曼·坎恩、娜奈·米格爾）

  - （哈曼·坎恩、娜奈·米格爾、、庫絲可·艾爾）

<!-- end list -->

  - 2012年

<!-- end list -->

  - （哈曼·坎恩）

  - （黑衣女性）

  - （艾露蓮\[50\]）

  - Dragon Age II（騎士團長梅瑞德斯）

  - [夢幻之星Online2](../Page/夢幻之星在線.md "wikilink")（**角色追加配音3**、希爾達·洛·卡美茲）

<!-- end list -->

  - 2013年

<!-- end list -->

  - Splinter Cell Blacklist（派翠西亞·柯德威爾\[51\]）

  - （\[52\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - （哈曼·坎恩）

  - [第3次超級機器人大戰Z 時獄篇](../Page/第3次超級機器人大戰Z.md "wikilink")（哈曼·坎恩）

<!-- end list -->

  - 2015年

<!-- end list -->

  - Shooting Girl（校長\[53\]）

  - 第3次超級機器人大戰Z 天獄篇（哈曼·坎恩）

  - （波拉里斯）

<!-- end list -->

  - 2016年

<!-- end list -->

  - SD鋼彈 G世代 GENESIS（哈曼·坎恩）
  - [OVERWATCH 鬥陣特攻](../Page/鬥陣特攻.md "wikilink")（**安娜·艾瑪莉**\[54\]）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [火焰之纹章 回聲 另一位英雄王](../Page/火焰之纹章_回聲_另一位英雄王.md "wikilink")（米拉\[55\]）

### 外語配音

※作品依英文名稱及英文原名排序。

#### 演員

  - **[派翠西婭·克拉克森](../Page/派翠西婭·克拉克森.md "wikilink")**
      - （**Wendy Shields**〈[Patricia
        Clarkson](../Page/派翠西婭·克拉克森.md "wikilink") 飾演〉）

      - 移動迷宮系列（Ava Paige）

          - [移動迷宮](../Page/移動迷宮_\(電影\).md "wikilink")
          - [移動迷宮：焦土試煉](../Page/移動迷宮：焦土試煉.md "wikilink")

#### 電影

  - [朝九晚五](../Page/朝九晚五_\(1980年電影\).md "wikilink")（Roz Keith〈Elizabeth
    Wilson 飾演〉）

  - ※[朝日電視台版](../Page/朝日電視台.md "wikilink")。

  - [異形](../Page/異形_\(電影\).md "wikilink")（Joan Lambert〈Veronica
    Cartwright
    飾演〉）※VHS、舊DVD版、（Mother的配音）※[LD版](../Page/LD.md "wikilink")。

  - [美國風情畫](../Page/美國風情畫.md "wikilink")
    ※[富士電視台版](../Page/富士電視台.md "wikilink")。

  -
  - [二八佳人花公子](../Page/二八佳人花公子.md "wikilink")（Susan Johnson〈Jill
    Eikenberry 飾演〉）

  - [邊緣日記](../Page/邊緣日記.md "wikilink")（的母親〈[Lorraine
    Bracco](../Page/羅林·布蘭考.md "wikilink") 飾演〉）

  - （Sal〈[Tilda Swinton](../Page/蒂達·史雲頓.md "wikilink")
    飾演〉）※[日本電視台版](../Page/日本電視台.md "wikilink")。

  - （Eva）※LD版。

  - （Rachel Varney〈Joan Severance 飾演〉）※影碟版。

  - （Karen）※富士電視台版\[56\]。

  - [火燒圓明園](../Page/火燒圓明園.md "wikilink")（[東太后](../Page/慈安太后.md "wikilink")〈陳燁
    飾演〉）※[TBS版](../Page/TBS電視台.md "wikilink")。

  - （Sara〈[Courteney Cox](../Page/柯特妮·考克斯.md "wikilink") 飾演〉）※VHS、DVD版。

  - （Linny Raven〈Tisha Sterling 飾演〉）※日本電視台版\[57\]。

  - （Katia 'K.C.' Koslovska〈[Nastassja
    Kinski](../Page/娜妲莎·金斯基.md "wikilink") 飾演〉）

  - 鱷魚先生系列（Sue Charlton〈**[Linda
    Kozlowski](../Page/琳達·柯兹羅斯基.md "wikilink")**
    主演〉）※富士電視台版。

      - [鱷魚先生](../Page/鱷魚先生.md "wikilink")

      -
  - [DC擴展宇宙](../Page/DC擴展宇宙.md "wikilink")（[Queen
    Hippolyta](../Page/希波呂忒.md "wikilink")〈[Connie
    Nielsen](../Page/康妮·尼爾森.md "wikilink")〉）

      - [正義聯盟](../Page/正義聯盟_\(電影\).md "wikilink")\[58\]
      - [神力女超人
        (2017年電影)](../Page/神力女超人_\(2017年電影\).md "wikilink")（[Hippolyta女王](../Page/希波吕忒_\(DC漫畫\).md "wikilink")〈[Connie
        Nielsen](../Page/康妮·尼爾森.md "wikilink") 飾演〉）\[59\]）

  - [象人](../Page/象人_\(電影\).md "wikilink")（Ann Treves〈Hannah Gordon
    飾演〉）※TBS版\[60\]。

  - [碧血長天](../Page/最後的計時.md "wikilink")（Laurel Scott〈Katharine Ross
    飾演〉）※TBS版。

  - （Cathy〈Chelsea Field 飾演〉）

  - （Angela〈[Jennifer Beals](../Page/珍妮佛·貝爾.md "wikilink") 飾演〉）

  - （**Ginny Field**〈Amy Steel 主演〉）※日本電視台版。

  - ※富士電視台版。

  - 系列（旁白）

      - 地球交響曲3
      - 地球交響曲4
      - 地球交響曲5

  - [巨人](../Page/巨人_\(電影\).md "wikilink")（Luz Benedict II世〈Carroll Baker
    飾演〉）※朝日電視台版。

  - （**Donna Hamilton**〈Dona Speir 主演〉）※東京電視台版。

  - （**Donna**〈Dona Speir 主演〉）

  - （Lissa Chestnut〈[Diane Keaton](../Page/黛安·基頓.md "wikilink")
    飾演〉）※電視播出版。

  - （**Betsy Nolan**／**Donna Korman**〈[Sarah Jessica
    Parker](../Page/莎拉·潔西卡·帕克.md "wikilink") 主演〉）

  - （Elise De Longvalle Adler〈Daria Nicolodi 飾演〉）※東京電視台版。

  - [詹姆士·龐德系列](../Page/占士邦.md "wikilink")

      - [詹姆士·龐德大戰金槍客](../Page/鐵金剛大戰金槍客.md "wikilink")（Saida）※TBS版。
      - [詹姆士·龐德：勇破海底城](../Page/鐵金剛勇破海底城.md "wikilink")（圓木小屋的女性）※TBS舊錄製版。
      - [詹姆士·龐德：勇破爆炸黨](../Page/鐵金剛勇破爆炸黨.md "wikilink")（Magda〈Kristina
        Wayborn 飾演〉）※TBS版。

  -
  - （**Charlie**〈[Diane Keaton](../Page/黛安·基頓.md "wikilink") 主演〉）

  - （**Margaret Lewin**〈[Jessica Lange](../Page/潔西卡·蘭芝.md "wikilink")
    主演〉）

  - （**Terry McKay**〈[Annette Bening](../Page/安妮特·班寧.md "wikilink") 主演〉）

  - Avis de mistral／My Summer in Provence（Sophie）

  - （Mag）※富士電視台版。

  - [霹靂煞](../Page/霹靂煞.md "wikilink")

  - （Maggie〈[Glenn Close](../Page/格倫·克洛斯.md "wikilink") 飾演〉）

  - [三不管地帶](../Page/三不管地帶_\(電影\).md "wikilink")（Ann Varrick〈Lara Harris
    飾演〉）※朝日電視台版。

  - （Alice Collins〈Marita Geraghty 飾演〉）

  - ※朝日電視台版。

  - （Liles）※VHS、DVD版。

  - （**Christina Hawk**〈Susan Blakely 主演〉）※富士電視台版。

  - （Susan〈Harley Jane Kozak 飾演〉）※VHS版。

  - [愛國者遊戲](../Page/愛國者遊戲_\(電影\).md "wikilink")（Annette〈Polly Walker
    飾演〉）※富士電視台版。

  - （Laura Dickinson〈Melody Thomas Scott 飾演〉）※TBS版\[61\]。

  - （Bonnie Sherow〈Cynthia Stevenson 飾演〉）※朝日電視台版。

  - [警察故事](../Page/警察故事.md "wikilink")（Selina方〈[林青霞](../Page/林青霞.md "wikilink")
    主演〉）※富士電視台版。

  - [A計劃續集](../Page/A計劃續集.md "wikilink")（**白影紅**〈[關之琳](../Page/關之琳.md "wikilink")
    主演〉)※富士電視台版。

  - （Marge Duval〈Marie Laforêt 飾演〉）※朝日電視台版。

  - （**Rose**〈Marilyn Chambers
    主演〉）※[東京電視台版](../Page/東京電視台.md "wikilink")\[62\]。

  - [殉情記](../Page/殉情記.md "wikilink")（Juliet〈[Olivia
    Hussey](../Page/奧莉花·荷西.md "wikilink") 主演〉）※富士電視台版。

  - [薩爾瓦多](../Page/薩爾瓦多_\(電影\).md "wikilink") ※富士電視台版。

  - [成功的祕密](../Page/成功的祕密.md "wikilink")（**Christy Wills**〈Helen Slater
    主演〉）※富士電視台版\[63\]。

  - （Jennifer）

  - （Emily Hardin〈Sharon Gless 飾演〉）※電視播出版\[64\]。

  - （**Katie Chandler**〈[Jodie Foster](../Page/茱迪·科士打.md "wikilink")
    主演〉）

  - （Annelle Dupuy Desoto〈[Daryl Hannah](../Page/戴露·漢娜.md "wikilink")
    飾演〉）

  - [超人II](../Page/超人II.md "wikilink") ※朝日電視台版。

  -
  - （Altieri警部〈Carola Stagnaro 飾演〉）※TBS版\[65\]。

  - （Erin）

  - [劍·花·煙雨·江南](../Page/劍·花·煙雨·江南.md "wikilink")（纖纖）

  - （Ruth Lanier〈Darlanne Fluegel 飾演〉）※TBS版。

  - （Penelope Peaks〈Kristin Holby 飾演〉）※日本電視台版。

  - （**Dr. Scott**〈Christine Lahti 主演〉）※電視播出版。

  - （**Tracy Tzu**〈Ariane Koizumi 主演〉）※TBS版。

  - （Susan Barrett〈Auretta Gay 飾演〉）※電視播出版\[66\]。

#### 電視影集

  - [地球百子](../Page/地球百子.md "wikilink")（Abby Griffin〈Paige Turco 飾演〉）

  - [大偵探波洛](../Page/大偵探波洛.md "wikilink")（Marie Marvell）－第18集、（Megan
    Barnard）－第31集。

  - [艾莉的異想世界 (第2季)](../Page/艾莉的異想世界.md "wikilink")（Post律師）

  - [緊診室的春天 (第4－11季)](../Page/仁心仁術.md "wikilink")（**Elizabeth
    Corday醫師**〈[Alex Kingston](../Page/阿莉克絲·金斯頓.md "wikilink")
    主演〉）

  - （Jennifer Daulton〈Elizabeth Morehead 飾演〉）

  - （Joyce Davenport〈Veronica Hamel 飾演〉）

  - Joe Dancer：The Big Black

  - [霹靂遊俠 (第2季)](../Page/霹靂遊俠.md "wikilink")（Amelia Clermont & Camera
    Clermont〈Deborah Alison 飾演〉）－第3集。

  - （Billie Newman〈Linda Kelsey 飾演〉）

  - [百戰天龍](../Page/百戰天龍.md "wikilink")（Ellen Wong）

  - （Rachel Rose〈[Nancy Allen](../Page/南茜·艾倫.md "wikilink") 飾演〉）

  -
  - [銀河飛龍](../Page/銀河飛龍.md "wikilink")（Anna、Kamala 等）

  - [星艦奇航記：重返地球](../Page/星艦奇航記：重返地球.md "wikilink")（）

  - [雙峰](../Page/雙峰_\(電視劇\).md "wikilink")（Shelly）

  - [超人力霸王葛雷](../Page/超人力霸王葛雷.md "wikilink")（Jean Echo隊員）\[67\]

#### 動畫

  - [地球超人](../Page/地球超人.md "wikilink")（**Dr. Blight**）
  - [貓頭鷹守護神](../Page/貓頭鷹守護神.md "wikilink")（Nyra〈[Helen
    Mirren](../Page/海倫·美蘭.md "wikilink") 配音〉）
  - [史努比的驚險夏令營](../Page/史努比.md "wikilink")（[Violet](../Page/范蕾特.md "wikilink")）
  - [辛普森一家](../Page/辛普森一家.md "wikilink")（Melon）

#### 國產電影

  - （／日語配音〈 飾演〉，[押井守執導](../Page/押井守.md "wikilink")）

### 布偶劇

  - [雷鳥神機隊系列](../Page/雷鳥神機隊.md "wikilink")（Min-min）※VHS版。
      - 雷鳥神機隊
      - 劇場版 雷鳥神機隊

### 廣播劇CD

  - （赫拉）

  - （日明蘭）

  - [G奇幻漫畫CD精選 火焰之紋章 暗黑龍與光之劍](../Page/火焰之紋章_暗黑龍與光之劍.md "wikilink")（）

  - （九尾社長）

  - [寄生前夜](../Page/寄生前夜.md "wikilink")（伊芙）

  - [晨曦公主](../Page/晨曦公主.md "wikilink")（祈莞船長）

  - （宰相局）

### 朗讀·有聲書

  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")
    尤里安·敏兹的伊謝爾倫日記（[菲列特利加·格林希爾](../Page/銀河英雄傳說角色列表.md "wikilink")）

### 旁白

  -
  - （1998諾貝爾獎·迎向21世紀的英知）

  -
  - （今天是什麼日子）

  - [NNN今日消息](../Page/NNN今日消息.md "wikilink")

  - 神話動物們

  - （[馬塞爾·杜象](../Page/馬塞爾·杜象.md "wikilink") ～創造神秘的藝術家～）

  - 中國絕景

  -
  - [NEWS STATION](../Page/NEWS_STATION.md "wikilink")

  -
  - 花

  - 在歐洲湖畔散步

  -
### 廣播

  -
  - My Classic

  - [神谷浩史、小野大輔的DearGirl
    ～Stories～](../Page/神谷浩史、小野大輔的DearGirl～Stories～.md "wikilink")（[哈曼·坎恩黏土公仔的配音](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")）

### 電視劇

  - （1979年，[日本電視台](../Page/日本電視台.md "wikilink")／[東寶](../Page/東寶.md "wikilink")）－瀨戶年子

  - （1994年，[東海電視台](../Page/東海電視台.md "wikilink")·[富士電視網](../Page/富士電視網.md "wikilink")）－旁白

### 電影

  - （1979年）－女服務生

  - [二百三高地](../Page/二百三高地.md "wikilink")（1980年）

  - 我的骨頭（2001年）－巡禮的媽媽們的配音

### 廣告

  - ·New（露面出演）

  - （旁白）

  - 交換卡片 GUNDAM WAR（哈曼·坎恩）

  - 電（露面出演）

  - 日清庵（露面出演）

  -
### 其它

  - 卡式錄音帶

      - Blue Heaven（派翠西婭·福克納）

      - [機動戰士鋼彈 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（梅絲塔·梅斯亞）

      - [機動戰士鋼彈ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")（[哈曼·坎恩](../Page/機動戰士Z_GUNDAM#阿克西斯（Axis）.md "wikilink")）

      - 大冒 夢帝司御用心（卡拉葳·伊方）

      - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink") 命運的魔術師（卡拉、蕾莉亞）

      - 羅德斯島戰記 魔石的眩惑（卡拉、蕾莉亞）

  - 錄影帶

      - [吹泡糖危機](../Page/吹泡糖危機.md "wikilink") HURRICANE LIVE 2033 Tinsel
        City Rhapsody
      - 吹泡糖危機 Bye2 Knight Sabers HOLIDAY in BALI

  - 廣播劇

      - （**瑪姬·史陶芬堡大尉**）

      - [玻璃假面](../Page/玻璃假面.md "wikilink")（姬川亞弓）

      - [最終幻想戰略版Advance](../Page/最終幻想戰略版Advance.md "wikilink")（雷夢迪女王）

  - [博物館](../Page/博物館.md "wikilink")

      - [鹿兒島市立科學館天象儀復活紀念節目](../Page/鹿兒島市.md "wikilink")「瑠璃色的訊息」（諾亞）

  - [人偶劇](../Page/木偶戲.md "wikilink")

      -
      - 「猴子的殿下」（旁白）

      - 「[稻草富翁](../Page/稻草富翁.md "wikilink")」（旁白、千金）

      - 「」（天女）

      - 「[三年寢太郎](../Page/三年寢太郎.md "wikilink")」

      - 「[動物城](../Page/美女與野獸.md "wikilink")」

      - 「白」

      - 「」

  - 『WANDER AGENTS』系列（Agent Master）\[68\]

  - [Go！Princess 光之美少女音樂劇Show](../Page/Go！Princess_光之美少女.md "wikilink")
    ！（迪絲比婭）

  - Android

      - MAPLUS資料庫\[69\]

  - （2018年5月5日，[NHK BS Premium](../Page/NHK_BS_Premium.md "wikilink")）

## 腳註

### 注釋

### 來源

## 外部連結

  - [株式會社Combination公式官網的聲優簡介](http://combination.jp/talent/sakakibara.html)

  - [ANN
    Encyclopedia](http://www.animenewsnetwork.com/encyclopedia/people.php?id=1022)

  - [kikubon 田中芳樹公認 銀河英雄傳說的朗讀 有聲書配信官網](http://kikubon.jp/)

[pt:Anexo:Lista de
seiyū\#S](../Page/pt:Anexo:Lista_de_seiyū#S.md "wikilink")

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")
[Category:桐朋學園藝術短期大學校友](../Category/桐朋學園藝術短期大學校友.md "wikilink")

1.

2.
3.

4.
5.
6.
7.

8.
9.
10.
11.
12.
13.
14.
15.
16.
17.

18.
19. 《聲優物語》藤津亮太，2017年發行，[一迅社](../Page/一迅社.md "wikilink")，第8－9頁。

20.

21.
22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44. 《[機動警察 WXIII](../Page/機動警察.md "wikilink")》併映作品。

45.

46.

47.

48.

49. [PC Engine版](../Page/PC_Engine.md "wikilink")。

50.

51. 《[電擊PlayStation](../Page/電擊PlayStation.md "wikilink")》Vol.543，[ASCII
    Media Works](../Page/ASCII_Media_Works.md "wikilink")，2013年5月30日。

52. 《電擊PlayStation》Vol.527 2012年9月27日號

53.

54.

55.

56. 後來以BD的方式收錄發行。

57.
58.

59.

60.
61. 後來以DVD的方式收錄發行。

62.
63. 後來以回憶復刻版BD的方式收錄發行。

64.
65. 後來以HD高畫質DVD\&HD高畫質特別版BD的方式收錄發行。

66.
67. [圓谷Production在澳洲拍攝的電視影集](../Page/圓谷製作.md "wikilink")。

68. <http://www.wonderland.gr.jp/agents/>

69.