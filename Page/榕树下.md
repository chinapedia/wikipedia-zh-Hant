**榕樹下**是中國大陸地區歷史最悠久的文學網站，最初為朱威廉的個人網站。\[1\]
1999年7月，上海榕树下计算机有限公司成立，榕樹下轉為商業網站。\[2\]
目前，榕樹下由[榕树下信息技术有限公司負責運營](../Page/盛大文學子公司列表.md "wikilink")，而榕树下信息技术有限公司為[盛霆信息技术（上海）有限公司旗下控股子公司](../Page/盛霆信息技术（上海）有限公司.md "wikilink")。

## 網站沿革

## 代表人物

  - [朱威廉](../Page/朱威廉.md "wikilink")

<!-- end list -->

  - [安妮寶貝](../Page/安妮寶貝.md "wikilink")

<!-- end list -->

  - [甯財神](../Page/甯財神.md "wikilink")

<!-- end list -->

  - [李寻欢（路金波）](../Page/李寻欢（路金波）.md "wikilink")

<!-- end list -->

  - [今何在](../Page/今何在.md "wikilink")

## 代表作品

  - 《生命的宣言》（又名：《死亡宣言》）：陸幼青著

<!-- end list -->

  - 《艾滋手記》：黎家明著

## 相關詞條

  - [盛霆信息技术（上海）有限公司](../Page/盛霆信息技术（上海）有限公司.md "wikilink")

## 參考文獻

## 外部連結

[Category:中国网站](../Category/中国网站.md "wikilink")
[Category:中华人民共和国文学](../Category/中华人民共和国文学.md "wikilink")
[Category:網路文學網站](../Category/網路文學網站.md "wikilink")
[Category:1997年建立的网站](../Category/1997年建立的网站.md "wikilink")
[Category:阅文集团](../Category/阅文集团.md "wikilink")

1.
2.