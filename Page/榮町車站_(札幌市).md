**榮町車站**（）是一位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[東區北](../Page/東區_\(札幌市\).md "wikilink")41條東15丁目、隸屬於[札幌市交通局的](../Page/札幌市交通局.md "wikilink")[地下鐵車站](../Page/鐵路車站.md "wikilink")。車站編號是**H-01**。

## 車站構造

1面2線[島式月台的](../Page/島式月台.md "wikilink")[地下車站](../Page/地下車站.md "wikilink")。

### 月台配置

| 月台    | 路線                                                                                                                    | 目的地                                                                                                                   | 備註      |
| ----- | --------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------- | ------- |
| **1** | [Subway_SapporoToho.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoToho.svg "fig:Subway_SapporoToho.svg") 東豐線 | [札幌](../Page/札幌站_\(札幌市營地下鐵\).md "wikilink")、[大通](../Page/大通站_\(北海道\).md "wikilink")、[福住方向](../Page/福住站.md "wikilink") | （主要乘車用） |
| **2** | （主要下車用）                                                                                                               |                                                                                                                       |         |

## 車站周邊

  - 自衛隊丘珠駐屯地
  - [丘珠空港](../Page/丘珠空港.md "wikilink")

## 历史

  - 1988年12月2日－伴隨市營地下鐵東豐線的通車，榮町車站啟用。

## 相鄰車站

  - [ST_Logo.svg](https://zh.wikipedia.org/wiki/File:ST_Logo.svg "fig:ST_Logo.svg")
    札幌市營地下鐵
    [Subway_SapporoToho.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoToho.svg "fig:Subway_SapporoToho.svg")
    東豐線
      -
        **榮町（H01）**－[新道東](../Page/新道東站.md "wikilink")（H02）

## 相關條目

  - [榮町車站](../Page/榮町車站.md "wikilink")：其他使用相同站名的車站。

## 外部連結

  - [榮町站內配置圖（札幌市交通局）](https://web.archive.org/web/20070710020503/http://www.city.sapporo.jp/st/ekikounaizu/35sakaemati.pdf)

[Kaemachi](../Category/日本鐵路車站_Sa.md "wikilink") [Category:東區鐵路車站
(札幌市)](../Category/東區鐵路車站_\(札幌市\).md "wikilink")
[Category:東豐線車站](../Category/東豐線車站.md "wikilink")
[Category:1988年啟用的鐵路車站](../Category/1988年啟用的鐵路車站.md "wikilink")