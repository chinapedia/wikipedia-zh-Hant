**樂基兒**（，），本名**黎嘉儀**，中越[混血兒](../Page/混血兒.md "wikilink")（父為[中國人](../Page/中國人.md "wikilink")，母為[越南人](../Page/越南人.md "wikilink")），生於[澳門](../Page/澳門.md "wikilink")，成長於[美國](../Page/美國.md "wikilink")
，[美籍華人](../Page/美籍華人.md "wikilink")，[香港著名女](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。主要在[香港發展演藝事業](../Page/香港.md "wikilink")，是香港[歌手](../Page/歌手.md "wikilink")[黎明前妻](../Page/黎明.md "wikilink")。

## 簡歷

樂基兒多出席時裝表演及參與演出廣告，出道時有「翻版[舒淇](../Page/舒淇.md "wikilink")」之稱。2005年年初，樂基兒坦白承認自己曾經[隆胸](../Page/隆胸.md "wikilink")，被認為是十分勇敢，之後因為脊骨負荷過重而還原。2005年年底，傳聞與[黎明戀愛](../Page/黎明.md "wikilink")。

2008年3月15日，傳聞樂基兒與黎明於[馬爾代夫結婚](../Page/馬爾代夫.md "wikilink")。3月17日，兩人皆否認。2009年8月30日，香港傳媒揭發樂基兒與黎明於2008年3月9日在[拉斯維加斯註冊結婚](../Page/拉斯維加斯.md "wikilink")\[1\]。傳聞的馬爾地代夫結婚則實際為度蜜月。2012年10月3日，兩人宣布「分手」\[2\]。

2017年8月，美國時間27日，在大約60多名親友見證下，樂基兒於加州與相識兩年多、從事有機食物網上生意的Ian Chu再婚。\[3\]

## 綜藝節目

  - 2015年《[真心英雄](../Page/真心英雄_\(电视节目\).md "wikilink")》

## 電影

  - 2000年《[戀性世代](../Page/戀性世代.md "wikilink")》
  - 2004年《[甜絲絲](../Page/甜絲絲.md "wikilink")》飾演白雪雪
  - 2017年《[女士復仇](../Page/女士復仇.md "wikilink")》

## 註釋

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:澳門人](../Category/澳門人.md "wikilink")
[Category:中國女性模特兒](../Category/中國女性模特兒.md "wikilink")
[\~](../Category/黎姓.md "wikilink")
[Category:京族裔混血兒](../Category/京族裔混血兒.md "wikilink")
[Category:越南裔香港人](../Category/越南裔香港人.md "wikilink")
[Category:左撇子](../Category/左撇子.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")

1.
2.
3.  [37歲樂基兒美國再婚 讚老公100分
    生B順其自然](http://bka.mpweekly.com/focus/local/37%E6%AD%B2%E6%A8%82%E5%9F%BA%E5%85%92%E7%BE%8E%E5%9C%8B%E5%86%8D%E5%A9%9A-%E8%AE%9A%E8%80%81%E5%85%AC100%E5%88%86-%E7%94%9Fb%E9%A0%86%E5%85%B6%E8%87%AA%E7%84%B6?cat=2)