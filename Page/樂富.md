[Lok_Fu_2010.jpg](https://zh.wikipedia.org/wiki/File:Lok_Fu_2010.jpg "fig:Lok_Fu_2010.jpg")
[LoFuNgamSubStation.jpg](https://zh.wikipedia.org/wiki/File:LoFuNgamSubStation.jpg "fig:LoFuNgamSubStation.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:1970_MTR_route_map_zh.png "fig:缩略图")
**樂富**（）位於[香港](../Page/香港.md "wikilink")[九龍北部](../Page/九龍.md "wikilink")，原名「**老虎岩**」（），區域行政上屬於[黃大仙區](../Page/黃大仙區.md "wikilink")，[樂富公園同屬樂富一部分](../Page/樂富公園.md "wikilink")，但在[十八區分區中該公園被劃入](../Page/香港行政區劃.md "wikilink")[九龍城區範圍](../Page/九龍城區.md "wikilink")。樂富位於[獅子山山腳](../Page/獅子山_\(香港\).md "wikilink")，即[九龍城以北](../Page/九龍城.md "wikilink")、[橫頭磡以南](../Page/橫頭磡.md "wikilink")、[九龍仔以東](../Page/九龍仔.md "wikilink")。全區以[樂富邨為中心](../Page/樂富邨.md "wikilink")。

## 歷史

樂富的前身為老虎岩徙置區，在1960至1970年代舊稱「**老虎岩**」，相傳老虎岩從前是一個山頭的岩洞,因有老虎出沒過而得名。

後來因為[老虎這名字剎氣大](../Page/老虎.md "wikilink")，兼且不吉利，所以[香港政府便以](../Page/香港政府.md "wikilink")「老虎」諧音「樂富」取替，亦含居民能夠安樂及富足之意。在1970年提出建造地下鐵路系統的建議時，當時樂富的位置仍稱老虎岩站。以前老虎岩有著名的老虎岩木屋區，其後徙置大廈在1957年至1963年間落成，成為老虎岩徙置區。老虎岩徙置區1963年被更名為老虎岩新區；1971年再被更名為樂富新區。為配合1973年[香港房屋委員會的成立及接管後](../Page/香港房屋委員會.md "wikilink")，再於同年改稱[樂富邨](../Page/樂富邨.md "wikilink")。

由於[橫頭磡部分屋邨及屋苑](../Page/橫頭磡.md "wikilink")，包括[橫頭磡邨及](../Page/橫頭磡邨.md "wikilink")[德強苑被劃分在樂富分區](../Page/德強苑.md "wikilink")，因此人們經常將樂富北面的橫頭磡視為樂富的一部份。

## 房屋

### 公共屋邨

[樂富邨是樂富唯一的公共屋邨](../Page/樂富邨.md "wikilink")，早在1957年落成，共23座，重建後的樂富邨則在1984年落成，共11座，截至2017年12月31日提供約3600個單位，人口為9,900。
\[1\]

### 居者有其屋計劃

[德強苑](../Page/德強苑.md "wikilink")、[嘉強苑](../Page/嘉強苑.md "wikilink")、[富強苑和](../Page/富強苑.md "wikilink")[康強苑屬樂富居屋屋苑](../Page/康強苑.md "wikilink")，分別於1991年和1999年落成，提供約1370個單位和640個單位\[2\]。

## 公共設施

[Lok_Fu_Recreation_Ground_Water_feature_2016.jpg](https://zh.wikipedia.org/wiki/File:Lok_Fu_Recreation_Ground_Water_feature_2016.jpg "fig:Lok_Fu_Recreation_Ground_Water_feature_2016.jpg")\]\]
[The_Hong_Kong_Buddhist_Hospital_2016.jpg](https://zh.wikipedia.org/wiki/File:The_Hong_Kong_Buddhist_Hospital_2016.jpg "fig:The_Hong_Kong_Buddhist_Hospital_2016.jpg")\]\]

### 醫療服務

  - [香港佛教醫院](../Page/香港佛教醫院.md "wikilink")（公營醫院，屬九龍中醫院聯網）

### 文娛康樂

  - [摩士公園](../Page/摩士公園.md "wikilink")
  - [樂富公園](../Page/樂富公園.md "wikilink")
  - [樂富遊樂場](../Page/樂富遊樂場.md "wikilink")
  - 樂富公共圖書館

### 教育

  - 幼稚園

<!-- end list -->

  - [基督教佈道中心樂富幼稚園](http://www.lokfukg.edu.hk)（1971年創辦）
  - [香港伯特利教會基甸幼稚園](http://www.bcgideon.edu.hk)（1990年創辦）

<!-- end list -->

  - 小學

<!-- end list -->

  - 華德學校（1963年創辦）
  - 聖博德學校（1965年創辦）

#### 已結束的學校

  - 伯特利學校（樂富邨4座天台）
  - 生命堂小學（樂富邨6座及17座天台）
  - 救世軍小學（樂富邨8座天台）
  - 使徒信心會小學（樂富邨8座天台）
  - 福德幼稚園（樂富邨16座地下）
  - 張祝珊幼稚園（樂富邨17座地下）
  - 佛教普賢幼稚園（樂富邨18座天台）
  - 慕光小學（樂富邨20座地下）
  - 慕光幼稚園（樂富邨20座天台）
  - 張祝山記念小學（樂富邨23座地下）
  - 基甸幼稚園（樂富邨23座天台）

### 商場

[Lok_Fu_Plaza_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lok_Fu_Plaza_\(Hong_Kong\).jpg "fig:Lok_Fu_Plaza_(Hong_Kong).jpg")\]\]
位於[聯合道與](../Page/聯合道.md "wikilink")[橫頭磡道的](../Page/橫頭磡道.md "wikilink")[樂富廣場是樂富最大的商場](../Page/樂富廣場.md "wikilink")，由[領展](../Page/領展.md "wikilink")（舊稱「領滙」）負責管理，樓面面積接近70萬平方呎，零售商戶約160間，餐飲商戶約40間。商場內亦有大型日資百貨公司「UNY生活創庫」及傳統乾貨及鮮活街市\[3\]。在樂富廣場內的平台花園更保留了「八仙圖」大型壁畫，供遊人欣賞\[4\]

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/黃大仙站.md" title="wikilink">黃大仙站B</a>1、C1出口、<a href="../Page/樂富站.md" title="wikilink">樂富站</a></li>
<li><font color="{{屯馬綫色彩}}">█</font><a href="../Page/屯馬綫.md" title="wikilink">屯馬綫</a>：<a href="../Page/宋皇臺站.md" title="wikilink">宋皇臺站B出口</a> (步行約15-20分鐘，2019年通車)</li>
</ul>
<dl>
<dt><a href="../Page/樂富巴士總站.md" title="wikilink">樂富巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/樂富廣場.md" title="wikilink">樂富廣場二期專線小巴總站</a></dt>

</dl>
<ul>
<li>樂富廣場至<a href="../Page/廣播道.md" title="wikilink">廣播道線</a></li>
<li>樂富廣場至<a href="../Page/筆架山.md" title="wikilink">筆架山線</a></li>
</ul>
<dl>
<dt>宏德樓/宏順樓專線小巴總站</dt>

</dl>
<dl>
<dt><a href="../Page/聯合道.md" title="wikilink">聯合道</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至</a><a href="../Page/九龍城.md" title="wikilink">九龍城</a>/<a href="../Page/黃大仙.md" title="wikilink">黃大仙線</a> (下午至晚上時段)[5]</li>
</ul>
<dl>
<dt><a href="../Page/富美街.md" title="wikilink">富美街</a></dt>

</dl>
<dl>
<dt><a href="../Page/鳳舞街.md" title="wikilink">鳳舞街</a>/<a href="../Page/杏林街.md" title="wikilink">杏林街</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至</a><a href="../Page/九龍城.md" title="wikilink">九龍城</a>/<a href="../Page/黃大仙.md" title="wikilink">黃大仙線</a> (下午至晚上時段)</li>
</ul>
<dl>
<dt><a href="../Page/龍翔道.md" title="wikilink">龍翔道</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 區議會議席分佈

為方便比較，以下列表以[東頭村道](../Page/東頭村道.md "wikilink")、[東正道](../Page/東正道.md "wikilink")、[太子道東](../Page/太子道.md "wikilink")、[彩虹道](../Page/彩虹道.md "wikilink")、[大成街](../Page/大成街.md "wikilink")、東頭村道、[鳳舞街](../Page/鳳舞街.md "wikilink")、[龍翔道](../Page/龍翔道.md "wikilink")、[竹園道](../Page/竹園道.md "wikilink")、[聯合道為範圍](../Page/聯合道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/聯合道.md" title="wikilink">聯合道</a>、<a href="../Page/杏林街.md" title="wikilink">杏林街</a>、<a href="../Page/鳳舞街.md" title="wikilink">鳳舞街</a>、<a href="../Page/東頭村道.md" title="wikilink">東頭村道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東頭村道.md" title="wikilink">東頭村道</a>、<a href="../Page/東正道.md" title="wikilink">東正道</a>、<a href="../Page/太子道.md" title="wikilink">太子道東</a>、<a href="../Page/彩虹道.md" title="wikilink">彩虹道</a>、<a href="../Page/大成街.md" title="wikilink">大成街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聯合道.md" title="wikilink">聯合道</a>、<a href="../Page/竹園道.md" title="wikilink">竹園道</a>、<a href="../Page/龍翔道.md" title="wikilink">龍翔道</a>、<a href="../Page/鳳舞街.md" title="wikilink">鳳舞街</a>、<a href="../Page/杏林街.md" title="wikilink">杏林街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參考

## 外部連結

  - [其他屋邨歷史研究篇(1)-老虎岩、樂富名字的由來\[揭開老虎岩改名為樂富之謎](http://blog.yahoo.com/Memory_of_Childhood/articles/1179923/index)

{{-}}

{{-}}

[\*](../Category/樂富.md "wikilink")
[Category:黃大仙區](../Category/黃大仙區.md "wikilink")
[Category:新九龍](../Category/新九龍.md "wikilink")

1.
2.  [香港房屋委員會(2012),物業位置及資料,居屋/私人機構參建居屋計劃屋苑,黃大仙區](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3038)
3.  都市日報(2011),"樂富廣場變身98%租出",都市日報,pp.06,2011年4月26日
4.  [樂富廣場,關於我們,社區及文化](http://www.lokfuplaza.com/hk/aboutus/communityandculture.aspx)
5.  [荃灣荃灣街市街　—　黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)