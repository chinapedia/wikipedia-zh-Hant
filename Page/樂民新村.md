[Lok_Man_Sun_Chuen.jpg](https://zh.wikipedia.org/wiki/File:Lok_Man_Sun_Chuen.jpg "fig:Lok_Man_Sun_Chuen.jpg")
[HK_LokManSunChuen.JPG](https://zh.wikipedia.org/wiki/File:HK_LokManSunChuen.JPG "fig:HK_LokManSunChuen.JPG")
[Lok_Man_Sun_Chuen_Plant_Nursery_and_Children's_Playground.jpg](https://zh.wikipedia.org/wiki/File:Lok_Man_Sun_Chuen_Plant_Nursery_and_Children's_Playground.jpg "fig:Lok_Man_Sun_Chuen_Plant_Nursery_and_Children's_Playground.jpg")
**樂民新村**（**Lok Man Sun
Chuen**），舊稱**樂民邨**，是[香港房屋協會第一個自資興建的甲類](../Page/香港房屋協會.md "wikilink")[出租屋邨](../Page/出租屋邨.md "wikilink")，位於九龍[土瓜灣](../Page/土瓜灣.md "wikilink")[靠背壟道](../Page/靠背壟道.md "wikilink")，建於1970年代初，由[司徒惠建築師事務所設計](../Page/司徒惠.md "wikilink")，分三期興建，於1970年到1974年分期完成入伙，樓宇設計屬於舊長形屋邨，共有9幢樓宇，佔地面積
411,615.36
[平方呎](../Page/平方呎.md "wikilink")。現由[香港房屋協會負責屋邨管理](../Page/香港房屋協會.md "wikilink")\[1\]。

## 屋邨資料

樂民新村是[香港房屋協會轄下屋邨](../Page/香港房屋協會.md "wikilink")，共有9座樓宇，由1970年到1974年分三期落成，其中第三期的兩座大廈（樂智樓及樂愛樓）是用當時最新的「預製組件」興建。

### 樓宇

| 樓宇名稱（座別） | 落成年份     | 層數    |
| -------- | -------- | ----- |
| 樂基樓（A座）  | 1970年    | 樓高15層 |
| 樂基樓（B座）  |          |       |
| 樂德樓（C座）  |          |       |
| 樂豐樓（D座）  |          |       |
| 樂群樓（E座）  |          |       |
| 樂鄰樓（F座）  | 1971年    |       |
| 樂善樓（G座）  |          |       |
| 樂智樓（H座）  | 1973-74年 | 樓高23層 |
| 樂愛樓（I座）  | 樓高17層    |       |

### 層數、升降機服務及單位數目

|                      |                                                                 |       |
| -------------------- | --------------------------------------------------------------- | ----- |
| 期數(座數)               | 升降機服務                                                           | 單位數目  |
| 第一期（樂鄰樓及樂善樓）         | 樂鄰樓3/F，8/F，13/F；樂善樓G/F，4/F，8/F，13/F                             | 1223個 |
| 第二期（樂基樓、樂德樓、樂豐樓及樂群樓） | 樂基樓2/F，8/F，13/F；樂德樓，樂豐樓，樂群樓2/F，5/F，8/F，13/F                     | 1542個 |
| 第三期（樂智樓及樂愛樓）         | 樂智樓G/F，3/F，5/F，9/F，13/F，17/F，21/F；樂愛樓LG/F，2/F，4/F，8/F，12/F，16/F | 896個  |

（另有10間年長者單位及2間年長者床位宿舍）

### 出租單位面積及編配標凖

|          |       |               |
| -------- | ----- | ------------- |
| 單位款式     | 編配標凖  | 室內樓面面積        |
| 年長者單位    | 1-2人  | 約18.4平方米      |
| 小型單位     | 1-3人  | 約26.5-29.5平方米 |
| 中型單位     | 4-6人  | 約32.6-37平方米   |
| 大型單位     | 6人或以上 | 約42.5-56.5平方米 |
| 男女床位宿舍單位 | 4-6人  | 約44.1-50.1平方米 |

## 設施

  - 樂基樓與樂善樓之間有一條私家道路連接山上多座大廈，也作停車場用途
  - 樂基樓向東南面對出的一塊約4400平方米的空地用作學校用途（[中華基督教會灣仔堂基道小學](../Page/中華基督教會灣仔堂基道小學.md "wikilink")）
  - 樂善樓地下作福利用途（[香港小童群益會樂民兒童及家庭綜合活動中心](https://lokman.bgca.org.hk)）
  - 樂智樓2樓作學校用途（[救世軍樂民幼兒學校](https://www.salvationarmy.org.hk/esd/lmns/)）
  - 樂鄰樓一樓作學校用途（[太陽島英文幼稚園（樂民）](http://www.sunisland.edu.hk)）
  - 樂善樓、樂智樓和樂愛樓的地下均有商店和社區設施（例：[惠康超級市場](../Page/惠康超級市場.md "wikilink")）

## 名人住客

  - [曾航生](../Page/曾航生.md "wikilink")：香港男藝人（已搬離）（H座）
  - [鄭伊健](../Page/鄭伊健.md "wikilink")：香港男藝人（已搬離）（H座）
  - [葉偉信](../Page/葉偉信.md "wikilink")：香港電影導演（已搬離）\[2\]
  - [鄭啟泰](../Page/鄭啟泰.md "wikilink")：香港節目主持人、新城電台唱片騎師（H座）

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{屯馬綫色彩}}">█</font>[屯馬綫](../Page/屯馬綫.md "wikilink")：[土瓜灣站](../Page/土瓜灣站.md "wikilink")
    (2019年啟用)

<!-- end list -->

  - [高山道](../Page/高山道.md "wikilink")

<!-- end list -->

  - [美善同道](../Page/美善同道.md "wikilink")

<!-- end list -->

  - [馬頭圍道](../Page/馬頭圍道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 油塘至佐敦道線\[3\]
  - 藍田至佐敦道線\[4\]
  - 秀茂坪至佐敦道線\[5\]
  - 觀塘至佐敦道線 (24小時服務)\[6\]
  - 牛頭角至佐敦道線 (上午服務)\[7\]
  - 慈雲山至灣仔線 (平日上午服務)\[8\]
  - 慈雲山至銅鑼灣線 (下午至深夜服務)\[9\]
  - 銅鑼灣至觀塘線 (下午至通宵服務)\[10\]
  - 中環/灣仔至觀塘線 (晚上至深夜服務)\[11\]
  - 西貢至銅鑼灣線 (晚上至深夜服務)\[12\]
  - 觀塘至西環線 (上午服務)\[13\]
  - 觀塘至香港仔線 (黃昏服務)\[14\]

</div>

</div>

## 注釋

## 參考資料

<references />

## 外部連結

  - [房協之友—樂民新村](https://web.archive.org/web/20070928030203/http://www.hkhs.com/chi/business/08.asp?contentid=1&estid=08)

[Category:土瓜灣](../Category/土瓜灣.md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.  [免長者行斜路樓梯
    樂民新村建升降機大樓](http://the-sun.on.cc/cnt/news/20101228/00407_081.html)，太陽報
2.
3.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
4.  [佐敦道寧波街　—　藍田](http://www.16seats.net/chi/rmb/r_k55.html)
5.  [秀茂坪　—　佐敦道北海街](http://www.16seats.net/chi/rmb/r_k95.html)
6.  [觀塘同仁街　—　佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
7.  [牛頭角站　—　佐敦道吳松街](http://www.16seats.net/chi/rmb/r_k56.html)
8.  [慈雲山　＞　銅鑼灣及灣仔](http://www.16seats.net/chi/rmb/r_kh32.html)
9.  [慈雲山　—　銅鑼灣鵝頸橋](http://www.16seats.net/chi/rmb/r_kh16.html)
10. [銅鑼灣鵝頸橋　—　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
11. [中環／灣仔　＞　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)
12. [西貢市中心　—　銅鑼灣登龍街](http://www.16seats.net/chi/rmb/r_hn98.html)
13. [觀塘宜安街　＞　西環卑路乍街,
    西環修打蘭街　＞　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
14. [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)