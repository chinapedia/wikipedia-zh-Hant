[LRT_Locwood_Stop_201801_Platform_1.jpg](https://zh.wikipedia.org/wiki/File:LRT_Locwood_Stop_201801_Platform_1.jpg "fig:LRT_Locwood_Stop_201801_Platform_1.jpg")
[LRT_Locwood_Stop_201801_Platform_2.jpg](https://zh.wikipedia.org/wiki/File:LRT_Locwood_Stop_201801_Platform_2.jpg "fig:LRT_Locwood_Stop_201801_Platform_2.jpg")
**樂湖站**（），是[香港輕鐵車站](../Page/香港輕鐵.md "wikilink")，車站代號是448，屬於單程車票[第4收費區](../Page/輕鐵第4收費區.md "wikilink")。該站位於[香港](../Page/香港.md "wikilink")[天水圍](../Page/天水圍.md "wikilink")[嘉湖山莊樂湖居和賞湖居之間](../Page/嘉湖山莊.md "wikilink")，為樂湖居及周邊地區居民提供服務。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/新北江商場.md" title="wikilink">新北江商場</a>、<a href="../Page/嘉湖山莊.md" title="wikilink">嘉湖山莊樂湖居</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/天逸站.md" title="wikilink">天逸</a>|}}</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 天水圍循環線（往<a href="../Page/天耀站.md" title="wikilink">天耀方向</a>）{{!}}  往<a href="../Page/元朗站_(輕鐵).md" title="wikilink">元朗</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/嘉湖山莊.md" title="wikilink">嘉湖山莊賞湖居</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 附近地方

  - [樂湖居](../Page/嘉湖山莊#樂湖居_\(第1期\).md "wikilink")
  - [賞湖居](../Page/嘉湖山莊#賞湖居_\(第2期\).md "wikilink")
  - [嘉湖新北江商場](../Page/嘉湖新北江商場.md "wikilink")
  - 天瑞路公園

## 接駁交通

<table>
<thead>
<tr class="header">
<th><p>接駁交通列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<ul>
<li><a href="../Page/港鐵巴士.md" title="wikilink">港鐵巴士</a>（<a href="../Page/八達通.md" title="wikilink">八達通免費轉乘優惠</a>）：
<ul>
<li><a href="../Page/港鐵巴士K75P綫.md" title="wikilink">K75P</a>－</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 歷史

### 車站命名

天水圍支線第一期通車前，這個站原本計劃命名為「**嘉湖站**」，後因顧及往後有更多九廣輕鐵站，設於[嘉湖山莊各處](../Page/嘉湖山莊.md "wikilink")，故通車時已使用現在車站名稱。

## 鄰接車站

## 外部連結

  - [港鐵公司－輕鐵街道圖](http://www.mtr.com.hk/archive/ch/services/maps/12.gif)
  - [港鐵公司－輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:1993年启用的铁路车站](../Category/1993年启用的铁路车站.md "wikilink")
[Category:元朗區鐵路車站](../Category/元朗區鐵路車站.md "wikilink")