**樂高機器人 Mindstorms
NXT**是樂高集團所製造第二代的可程式即可組合[機器人玩具](../Page/機器人.md "wikilink")(第一代為RCX，第三代為EV3)。整組的套件包含了感測器及連接線。[樂高](../Page/樂高.md "wikilink")（LEGO）已于2006年9月上旬推出樂高公司和美國[麻省理工學院共同開發的機器人組件新款](../Page/麻省理工學院.md "wikilink")“教育用LEGO
Mindstorms
NXT”。Mindstorms是將配備微處理器的LEGO公司的塑膠積木組裝起來，通過[個人電腦制作的程式來控制的](../Page/個人電腦.md "wikilink")[機器人](../Page/機器人.md "wikilink")。最新的版本為2009年8月的
Lego Mindstorms NXT 2.0. 在台灣樂高機器人NXT主機 (P/N 9841) 可取得價格快速滑落已不到 150元美金左右。
[Nxt-brique.jpg](https://zh.wikipedia.org/wiki/File:Nxt-brique.jpg "fig:Nxt-brique.jpg")

## [RCX與NXT及](../Page/RCX.md "wikilink")[EV3的比較](../Page/EV3.md "wikilink")

## 電子零件

[Lego_Mindstorms_NXT_2.0-_Stein_und_Sensoren.png](https://zh.wikipedia.org/wiki/File:Lego_Mindstorms_NXT_2.0-_Stein_und_Sensoren.png "fig:Lego_Mindstorms_NXT_2.0-_Stein_und_Sensoren.png")
NXT可外接[馬達](../Page/馬達.md "wikilink")、[感應器等組件](../Page/感應器.md "wikilink")，以下[樂高原廠提供的組件](../Page/樂高.md "wikilink")，此外，也有樂高以外的廠商製作相容於NXT的感應器。

  - 可程式控制積木(NXT)
  - 伺服馬達
  - 光感應器
  - 聲音感應器
  - 顏色感應器
  - 觸碰感應器
  - 超聲波感應器
  - 溫度感應器

## Lego NXT 支援的程式語言

  - [NXT-G](../Page/樂高Mindstorms_NXT.md "wikilink")

  - [LeJOS](../Page/LeJOS.md "wikilink")

  - [Not eXactly C（NXC）](../Page/Not_eXactly_C.md "wikilink")

  - [Robolab](../Page/乐高Mindstorms.md "wikilink")

  -
  - [RobotC](../Page/RobotC.md "wikilink")

  - [LabVIEW](../Page/LabVIEW.md "wikilink")

  - [Lua](../Page/卢阿.md "wikilink")

  -
  - [Ch interpreter, cross-platform C/C++
    interpreter](../Page/Ch_interpreter,_cross-platform_C/C++_interpreter.md "wikilink")

  - [Ch Mindstorms NXT Control
    Package](../Page/Ch_Mindstorms_NXT_Control_Package.md "wikilink")

  - [C and C++, under brickOS, formerly
    LegOS](../Page/C_and_C++,_under_brickOS,_formerly_LegOS.md "wikilink")

  - [C and Assembly, under the GCC open source firmware kit
    NXTGCC](../Page/C_and_Assembly,_under_the_GCC_open_source_firmware_kit_NXTGCC.md "wikilink")

  - [Interactive C, C-like language used in robotics
    competitions](../Page/Interactive_C,_C-like_language_used_in_robotics_competitions.md "wikilink")

  - [Java, under leJOS or
    TinyVM](../Page/Java,_under_leJOS_or_TinyVM.md "wikilink")

  - [pbFORTH, extensions to
    Forth](../Page/pbFORTH,_extensions_to_Forth.md "wikilink")

  - [pbLua, version of Lua](../Page/pbLua,_version_of_Lua.md "wikilink")

  - [Visual Basic, via the COM+ interface supplied on the
    CD](../Page/Visual_Basic,_via_the_COM+_interface_supplied_on_the_CD.md "wikilink")

## 參見

  - [樂高](../Page/樂高.md "wikilink")
  - [樂高Mindstorms](../Page/樂高Mindstorms.md "wikilink")
  - [樂高Mindstorms RCX](../Page/樂高Mindstorms_RCX.md "wikilink")
  - [樂高Mindstorms EV3](../Page/樂高Mindstorms_EV3.md "wikilink")

## 参考文献

## 外部連結

  - [樂高Mindstorms產品官方網站](http://mindstorms.lego.com)
  - [Hitechnic官方網站](http://www.hitechnic.com/)
  - [Mindsensors官方網站](http://www.mindsensors.com/)

[ja:MINDSTORMS\#レゴマインドストーム
NXT](../Page/ja:MINDSTORMS#レゴマインドストーム_NXT.md "wikilink")
[nl:LEGO Mindstorms\#Mindstorms
NXT](../Page/nl:LEGO_Mindstorms#Mindstorms_NXT.md "wikilink")

[Category:乐高](../Category/乐高.md "wikilink")
[Category:2006年面世的產品](../Category/2006年面世的產品.md "wikilink")