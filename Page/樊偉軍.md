**樊偉軍**（**Fan
Weijun**，），出生於[上海](../Page/上海.md "wikilink")，[中國](../Page/中國.md "wikilink")[足球運動員](../Page/足球.md "wikilink")，可擔任前鋒、防守中場等位置。

## 球員生涯

出身於[上海浦東](../Page/上海浦东足球俱乐部.md "wikilink")，曾先後過效力[四川全興](../Page/四川全興.md "wikilink")、[北京寬利](../Page/北京寬利.md "wikilink")、[青島海利豐](../Page/青島海利豐.md "wikilink")、[上海九城等多支國內球會](../Page/无锡中邦.md "wikilink")。

樊偉軍於2005年到[香港發展](../Page/香港.md "wikilink")，以國援身份加盟[流浪](../Page/香港流浪足球會.md "wikilink")。2007年，轉投[南華](../Page/南華足球隊.md "wikilink")，至2009年夏天不獲續約，遂加盟[四海流浪](../Page/四海體育會.md "wikilink")。

2012年夏天，樊偉軍轉投[香港乙組足球聯賽球隊](../Page/香港乙組足球聯賽.md "wikilink")[愉園](../Page/愉園體育會.md "wikilink")，協助球隊於2013年重返甲組，並擔任助教兼球員，至2014年1月7日因為涉嫌打假波而被[廉政公署拘捕](../Page/廉政公署_\(香港\).md "wikilink")\[1\]。

## 參考資料

## 外部連結

  - [香港足球總會網站球員資料](http://www.hkfa.com/en/player_view.php?player_id=44)
  - [愉園足球隊球員資料](https://web.archive.org/web/20160304085313/http://tw.hvaafc.org/18-271462055336557.html)

[Category:上海籍足球运动员](../Category/上海籍足球运动员.md "wikilink")
[Category:四川全兴球员](../Category/四川全兴球员.md "wikilink")
[Category:青岛海利丰球员](../Category/青岛海利丰球员.md "wikilink")
[Category:上海浦东中邦球员](../Category/上海浦东中邦球员.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:南華球員](../Category/南華球員.md "wikilink")
[Category:四海球員](../Category/四海球員.md "wikilink")
[Category:愉園球員](../Category/愉園球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:陕西中新球员](../Category/陕西中新球员.md "wikilink")
[W](../Category/樊姓.md "wikilink")

1.  [假波醜聞：廉署拘9人包括6愉園球員](http://hk.on.cc/hk/bkn/cnt/news/20140107/bkn-20140107210325392-0107_00822_001.html)