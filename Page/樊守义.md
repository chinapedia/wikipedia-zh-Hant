**樊守义**（）清[康熙帝出使](../Page/康熙帝.md "wikilink")[西洋的使者](../Page/西洋.md "wikilink")。[康熙二十一年](../Page/康熙.md "wikilink")（1682年）生于[山西省](../Page/山西省.md "wikilink")[平阳府](../Page/平阳郡.md "wikilink")[绛州](../Page/绛州.md "wikilink")（今[新绛县](../Page/新绛县.md "wikilink")）。

## 生平

康熙四十六年（1707年），[罗马教皇](../Page/罗马教皇.md "wikilink")[特使](../Page/教宗特使.md "wikilink")[铎罗在华](../Page/铎罗.md "wikilink")-{zh-cn:发;zh-tw:發}-布[南京教令](../Page/中國禮儀之爭#教皇極終禁令.md "wikilink")，禁止中国天主教徒敬拜祖先。[康熙帝大怒](../Page/康熙帝.md "wikilink")，下令驱逐铎罗，遣送澳门关押。为向罗马教皇澄清[中国礼仪之争](../Page/中国礼仪之争.md "wikilink")，康熙帝派[耶稣会法国传教士](../Page/耶稣会.md "wikilink")[艾若瑟出使罗马](../Page/艾若瑟.md "wikilink")，中国青年天主教徒樊守义随行。

樊守义随同艾若瑟，从[葡萄牙占领的](../Page/葡萄牙.md "wikilink")[澳门启航](../Page/澳门.md "wikilink")，经[婆罗洲](../Page/婆罗洲.md "wikilink")、[马六甲](../Page/马六甲.md "wikilink")、[苏门答腊](../Page/苏门答腊.md "wikilink")，入大洋，航行三四月抵达葡萄牙首都[里斯本](../Page/里斯本.md "wikilink")，获[葡萄牙国王](../Page/葡萄牙国王.md "wikilink")[若昂五世召见](../Page/若昂五世.md "wikilink")。（葡萄牙国王被罗马教皇授予东亚传教的[保教权](../Page/保教权.md "wikilink")。）

在里斯本停留四个月后，于康熙四十八年（1709年）正月再启程，经[直布罗陀海峡](../Page/直布罗陀.md "wikilink")，遇风停留在西班牙[安达鲁西亚](../Page/安达鲁西亚.md "wikilink")，停留后再启程前往[意大利](../Page/意大利.md "wikilink")。二月下旬，抵达[热那亚](../Page/热那亚.md "wikilink")，取陆路经[比萨](../Page/比萨.md "wikilink")、[锡耶纳等地](../Page/锡耶纳.md "wikilink")，抵达[罗马](../Page/罗马.md "wikilink")，晋见[罗马教皇](../Page/罗马教皇.md "wikilink")[克勉十一世](../Page/克勉十一世.md "wikilink")。艾若瑟和樊守义将[康熙帝关于](../Page/康熙帝.md "wikilink")[铎罗来华](../Page/铎罗.md "wikilink")、[中国礼节问题和西洋教务问题的旨意](../Page/中国礼仪之争.md "wikilink")，详细向教皇呈述。教皇听后，双目含泪说，“我从来没有令鐸羅如此发言行事”\[1\]。

在罗马期间，艾若瑟和樊守义受到克勉十一世接待，参观宫殿和图书馆。樊守义居意大利9年，先后在[都灵](../Page/都灵.md "wikilink")、罗马学习，并遍游意大利名城[弗拉斯卡蒂](../Page/弗拉斯卡蒂.md "wikilink")、[蒂沃利](../Page/蒂沃利.md "wikilink")、[那波利](../Page/那波利.md "wikilink")、[卡普阿](../Page/卡普阿.md "wikilink")、[博洛尼亚](../Page/博洛尼亚.md "wikilink")、[摩德纳](../Page/摩德纳.md "wikilink")、[帕尔马](../Page/帕尔马.md "wikilink")、[帕维亚](../Page/帕维亚.md "wikilink")、[米兰](../Page/米兰.md "wikilink")、[福尔切利](../Page/福尔切利.md "wikilink")、[都灵](../Page/都灵.md "wikilink")、[皮埃蒙特等地](../Page/皮埃蒙特.md "wikilink")。

1718年（康熙五十七年），罗马教皇收到康熙皇帝朱笔文书，召见艾若瑟、樊守义，口谕：“你们可以回国，我将另遣使臣前往，向中国皇帝逐条陈奏”。樊守义随艾若瑟东还，途中经过[葡萄牙](../Page/葡萄牙.md "wikilink")，再次获得葡萄牙国王接见，赐问良久，并获赐黄金百两。归途中艾若瑟不幸在[好望角前往](../Page/好望角.md "wikilink")[印度舟中病故](../Page/印度.md "wikilink")，樊守义独自归国，六月十三日抵达广州，九月十一日奉诏到[热河](../Page/热河.md "wikilink")，叩见皇上，蒙康熙帝赐见，赐问良久。樊守义著有《[身见录](../Page/身见录.md "wikilink")》，记述前往西洋的经历。

樊守义在[乾隆十八年](../Page/乾隆.md "wikilink")（1753年）逝世。中国历史学家[阎宗临将清康熙年间的樊守义与](../Page/阎宗临.md "wikilink")[東晉](../Page/東晉.md "wikilink")[法显相比](../Page/法显.md "wikilink")，二人都是[平阳县人](../Page/平阳.md "wikilink")，法显最早往佛教国[天竺取经](../Page/天竺.md "wikilink")，著有《[佛国记](../Page/佛国记.md "wikilink")》；樊守义则远渡重洋，出使[天主教](../Page/天主教.md "wikilink")[罗马教廷](../Page/罗马教廷.md "wikilink")，著有《[身见录](../Page/身见录.md "wikilink")》，具有特殊历史意义\[2\]。

## 注释

## 参考文献

### 引用

### 书籍

  - 阎宗临 《中西交通史·身见录校注》 2007 广西师范大学出版社 ISBN 7563365104

## 参见

  - [在华耶稣会士列表](../Page/在华耶稣会士列表.md "wikilink")
  - [中国礼仪之争](../Page/中国礼仪之争.md "wikilink")
  - 《[身见录](../Page/身见录.md "wikilink")》

{{-}}

[S守](../Category/樊姓.md "wikilink") [F樊](../Category/新绛人.md "wikilink")
[Category:中国籍耶稣会士](../Category/中国籍耶稣会士.md "wikilink")
[F樊](../Category/清朝天主教徒.md "wikilink")
[F樊](../Category/清朝外交官.md "wikilink")
[F樊](../Category/中國旅行家.md "wikilink")
[F樊](../Category/中外交通史人名.md "wikilink")
[Category:澳門中外交流史](../Category/澳門中外交流史.md "wikilink")

1.  樊守义 《呈广东巡抚奏文》，见[阎宗临](../Page/阎宗临.md "wikilink") 《中西交通史》 第118页 2007
    广西师范大学出版社 ISBN 7-5633-6510-4
2.  阎宗临《身见录校注·后记》