**標準時間**是在同一[時區內的不同地區](../Page/時區.md "wikilink")，捨棄地區性的[子午線定出的](../Page/子午線.md "wikilink")[太陽時或](../Page/太陽日.md "wikilink")[地方平時](../Page/地方平時.md "wikilink")，而共同採用的同步[時間](../Page/時間.md "wikilink")。這種時間計算制度是由[世界時衍生出來的](../Page/世界時.md "wikilink")。有些國家及地區會使用[日光節約時間](../Page/夏時制.md "wikilink")，但標準時間的表記上也許不會提到日光節約時間。

## 歷史

### 英國

早在1847年12月11日，[英國的](../Page/英國.md "wikilink")[鐵路業者就使用標準時間](../Page/英國鐵路運輸.md "wikilink")，當時是以[格林威治平時做標準](../Page/格林尼治標準時間.md "wikilink")。到了1855年，英國大部分供公眾使用的[時鐘都使用格林威治平時顯示時間](../Page/時鐘.md "wikilink")。

### 北美洲

1883年之前，[北美洲普遍都採用](../Page/北美洲.md "wikilink")[地方平時](../Page/地方平時.md "wikilink")，不僅造成當地時間的紊亂無序，也導致地方和全國的火車[時刻表非常複雜](../Page/時刻表.md "wikilink")。1879年2月8日，[加拿大的](../Page/加拿大.md "wikilink")[史丹佛·佛萊明在](../Page/史丹佛·佛萊明.md "wikilink")[加拿大皇家學院的會議上提出標準時間的提案](../Page/加拿大皇家學院.md "wikilink")，以交會在[芝加哥為首的主要鐵路採用了標準時間的系統](../Page/芝加哥.md "wikilink")。在鐵路如此做之後，許多的州几乎都立刻跟進採用這套新的系統，但[美國聯邦政府差不多在](../Page/美國聯邦政府.md "wikilink")40年之後才接納這套系統。

2007年，美國聯邦政府頒布一項聯邦法律，正式以[協調世界時做為標準時間的依據](../Page/協調世界時.md "wikilink")，由[美國經貿部](../Page/美國經貿部.md "wikilink")（轄下的[國家標準技術研究所](../Page/國家標準技術研究所.md "wikilink")）和[美國海軍](../Page/美國海軍.md "wikilink")（轄下的[美國海軍天文臺](../Page/美國海軍天文臺.md "wikilink")）來播送標準時間（H.R.
2272：2007年的21世紀競爭力法）\[1\]。

## 相關條目

  - [日光節約時間](../Page/夏時制.md "wikilink")
  - [地方平時](../Page/地方平時.md "wikilink")
  - [太陽時](../Page/太陽日.md "wikilink")
  - [時區](../Page/時區.md "wikilink")
  - [世界時](../Page/世界時.md "wikilink")

## 參考資料

### 引用

### 參考書目

  -
  -
  -
[時區](../Page/category:時區.md "wikilink")

[時標](../Category/時標.md "wikilink")

1.  21st Century Competitiveness Act of 2007, Section 3013.
    [H.R. 2272: 110th CONGRESS House
    Bills](http://frwebgate.access.gpo.gov/cgi-bin/getdoc.cgi?dbname=110_cong_bills&docid=h2272enr),
    January 4, 2007.