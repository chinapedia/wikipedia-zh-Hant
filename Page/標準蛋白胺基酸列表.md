**標準氨基酸**（英語：**Standard amino
acids**）或稱**[蛋白氨基酸](../Page/蛋白氨基酸.md "wikilink")**（**proteinogenic
amino
acids**），是生物[細胞中用來合成](../Page/細胞.md "wikilink")[蛋白質的共](../Page/蛋白質.md "wikilink")20種[氨基酸](../Page/氨基酸.md "wikilink")。本列表主要描述其名稱、標示方法、結構與性質。还包括次要编码氨基酸，[硒半胱氨酸和](../Page/硒半胱氨酸.md "wikilink")[吡咯赖氨酸](../Page/吡咯赖氨酸.md "wikilink")，分别用通常的終止[密码子UGA和UAG编码](../Page/密码子.md "wikilink")，出现在少数[蛋白质中](../Page/蛋白质.md "wikilink")。参见[*第21和第22种氨基酸*](http://hxtb.icas.ac.cn/col/2003/pdf/c03051.pdf)。

## 名稱與符號

下表中顯示各中文名稱與英文名稱、三字母符號與單字母符號。（\*为次要编码氨基酸）

| 中文名稱                                   | 英文名稱                      | 短寫 | 縮寫  |
| -------------------------------------- | ------------------------- | -- | --- |
| [丙氨酸](../Page/丙氨酸.md "wikilink")       | Alanine                   | A  | Ala |
| [苯丙氨酸](../Page/苯丙氨酸.md "wikilink")     | Phenylalanine             | F  | Phe |
| [半胱氨酸](../Page/半胱氨酸.md "wikilink")     | Cysteine                  | C  | Cys |
| [硒半胱氨酸](../Page/硒半胱氨酸.md "wikilink")\* | Selenocysteine            | U  | Sec |
| [天冬氨酸](../Page/天冬氨酸.md "wikilink")     | Aspartic acid / Aspartate | D  | Asp |
| [天冬醯胺](../Page/天冬醯胺.md "wikilink")     | Asparagine                | N  | Asn |
| [谷氨酸](../Page/谷氨酸.md "wikilink")       | Glutamic acid / Glutamate | E  | Glu |
| [谷氨酰胺](../Page/谷氨酰胺.md "wikilink")     | Glutamine                 | Q  | Gln |
| [甘氨酸](../Page/甘氨酸.md "wikilink")       | Glycine                   | G  | Gly |
| [組氨酸](../Page/組氨酸.md "wikilink")       | Histidine                 | H  | His |
| [亮氨酸](../Page/亮氨酸.md "wikilink")       | Leucine                   | L  | Leu |
| [異亮氨酸](../Page/異亮氨酸.md "wikilink")     | Isoleucine                | I  | Ile |
| [離氨酸](../Page/離氨酸.md "wikilink")       | Lysine                    | K  | Lys |
| [吡咯賴氨酸](../Page/吡咯賴氨酸.md "wikilink")\* | Pyrrolysine               | O  | Pyl |
| [蛋氨酸](../Page/蛋氨酸.md "wikilink")       | Methionine                | M  | Met |
| [脯氨酸](../Page/脯氨酸.md "wikilink")       | Proline                   | P  | Pro |
| [精氨酸](../Page/精氨酸.md "wikilink")       | Arginine                  | R  | Arg |
| [絲氨酸](../Page/絲氨酸.md "wikilink")       | Serine                    | S  | Ser |
| [蘇氨酸](../Page/蘇氨酸.md "wikilink")       | Threonine                 | T  | Thr |
| [纈氨酸](../Page/纈氨酸.md "wikilink")       | Valine                    | V  | Val |
| [色氨酸](../Page/色氨酸.md "wikilink")       | Tryptophan                | W  | Trp |
| [酪氨酸](../Page/酪氨酸.md "wikilink")       | Tyrosine                  | Y  | Tyr |

除此之外，还有一些三字母或单字母符號可用來表示未明確定義的縮寫：

  - Asx、B可代表天冬氨酸（Asp、D）或天冬醯胺（Asn、N）。
  - Glx、Z可代表谷氨酸（Glu、E）或谷氨酰胺（Gln、Q）。
  - Xle、J可代表亮氨酸（Leu、L）或異亮氨酸（Ile、I）。
  - Xaa（亦用Unk）、X可代表任意氨基酸或未知氨基酸。

## 結構

以下是標準[遺傳密碼所用來直接合成蛋白質的](../Page/遺傳密碼.md "wikilink")20種氨基酸的結構及表示符號。

<File:L-alanine-skeletal.png>|[<small>L</small>-丙氨酸](../Page/丙氨酸.md "wikilink")
(Ala / A)
<File:L-arginine-skeletal-(tall).png>|[<small>L</small>-精氨酸](../Page/精氨酸.md "wikilink")
(Arg / R)
<File:L-asparagine-skeletal.png>|[<small>L</small>-天冬醯胺](../Page/天冬醯胺.md "wikilink")
(Asn / N)
<File:L-aspartic-acid-skeletal.png>|[<small>L</small>-天冬氨酸](../Page/天冬氨酸.md "wikilink")
(Asp / D)
<File:L-cysteine-skeletal.png>|[<small>L</small>-半胱氨酸](../Page/半胱氨酸.md "wikilink")
(Cys / C)
<File:L-glutamic-acid-skeletal.png>|[<small>L</small>-谷氨酸](../Page/谷氨酸.md "wikilink")
(Glu / E)
<File:L-glutamine-skeletal.png>|[<small>L</small>-谷氨酰胺](../Page/谷氨醯胺.md "wikilink")
(Gln / Q) <File:Glycine-skeletal.png>|[甘氨酸](../Page/甘氨酸.md "wikilink")
(Gly / G)
<File:L-histidine-skeletal.png>|[<small>L</small>-組氨酸](../Page/組氨酸.md "wikilink")
(His / H)
<File:L-isoleucine-skeletal.png>|[<small>L</small>-異亮氨酸](../Page/異亮氨酸.md "wikilink")
(Ile / I)
<File:L-leucine-skeletal.png>|[<small>L</small>-亮氨酸](../Page/亮氨酸.md "wikilink")
(Leu / L)
<File:L-lysine-skeletal.png>|[<small>L</small>-賴氨酸](../Page/賴氨酸.md "wikilink")
(Lys / K)
<File:L-methionine-skeletal.png>|[<small>L</small>-蛋氨酸](../Page/蛋氨酸.md "wikilink")
(Met / M)
<File:L-phenylalanine-skeletal.png>|[<small>L</small>-苯丙氨酸](../Page/苯丙氨酸.md "wikilink")
(Phe / F)
<File:L-proline-skeletal.png>|[<small>L</small>-脯氨酸](../Page/脯氨酸.md "wikilink")
(Pro / P)
<File:L-serine-skeletal.png>|[<small>L</small>-絲氨酸](../Page/絲氨酸.md "wikilink")
(Ser / S)
<File:L-threonine-skeletal.png>|[<small>L</small>-蘇氨酸](../Page/蘇氨酸.md "wikilink")
(Thr / T)
<File:L-tryptophan-skeletal.png>|[<small>L</small>-色氨酸](../Page/色氨酸.md "wikilink")
(Trp / W)
<File:L-tyrosine-skeletal.png>|[<small>L</small>-酪氨酸](../Page/酪氨酸.md "wikilink")
(Tyr / Y)
<File:L-valine-skeletal.png>|[<small>L</small>-纈氨酸](../Page/纈氨酸.md "wikilink")
(Val / V)
<File:L-selenocysteine-2D-skeletal.png>|[<small>L</small>-硒半胱胺酸](../Page/硒半胱胺酸.md "wikilink")
(Sec / U)
<File:Pyrrolysine.svg>|[<small>L</small>-吡咯賴胺酸](../Page/吡咯賴胺酸.md "wikilink")
(Pyl / O)

## 主要性質

有關於主要化學性質、支鏈性質、基因表現與生物化學這些屬性，請參照[胺基酸條目](../Page/胺基酸.md "wikilink")。

## 參考文獻

  -
  -
[ar:قائمة الأحماض
الأمينية](../Page/ar:قائمة_الأحماض_الأمينية.md "wikilink")
[en:List of standard amino
acids](../Page/en:List_of_standard_amino_acids.md "wikilink")

[氨基酸](../Category/氨基酸.md "wikilink")
[Category:营养学](../Category/营养学.md "wikilink")
[Category:生物學列表](../Category/生物學列表.md "wikilink")
[Category:化學列表](../Category/化學列表.md "wikilink")