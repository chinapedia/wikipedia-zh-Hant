**標籤效應**是[教育心理學](../Page/教育心理學.md "wikilink")、[政治學和](../Page/政治學.md "wikilink")[社會學的名詞](../Page/社會學.md "wikilink")。標籤效應是一個[自然人](../Page/自然人.md "wikilink")、一個[組織](../Page/组织_\(社会学\).md "wikilink")、一個[地區給別人](../Page/地區.md "wikilink")[貼上標籤之後所產生的效應](../Page/貼標籤.md "wikilink")，包括[強化](../Page/強化.md "wikilink")、[自我認同](../Page/自我認同.md "wikilink")、[刻板印象](../Page/刻板印象.md "wikilink")。例如把[中國製造的商品與](../Page/中國製造.md "wikilink")[黑心商品劃上等號](../Page/黑心商品.md "wikilink")，把[非法移民與不守規矩](../Page/非法移民.md "wikilink")、禮儀劃上等號等。

## 被標籤物（部份）

  - [中國製造](../Page/中國製造.md "wikilink")
  - [新移民](../Page/新移民.md "wikilink")、[外勞](../Page/外勞.md "wikilink")
  - [原住民](../Page/原住民.md "wikilink")
  - [悲情城市](../Page/悲情城市.md "wikilink")
  - [精神病](../Page/精神病.md "wikilink")、[智障](../Page/智能障礙.md "wikilink")
  - [左派](../Page/左派.md "wikilink")、[右派](../Page/右派.md "wikilink")
  - [政治人物](../Page/政治人物.md "wikilink")
  - [公務人員](../Page/公務人員.md "wikilink")
  - [左膠](../Page/左膠.md "wikilink")
  - [中文中學](../Page/中文中學.md "wikilink")
  - [更生人士](../Page/更生人士.md "wikilink")
  - [上流社會](../Page/上流社會.md "wikilink")
  - [宅男](../Page/宅男.md "wikilink")、[毒男](../Page/毒男.md "wikilink")
  - [腐女](../Page/腐女.md "wikilink")
  - [港女](../Page/港女.md "wikilink")
  - [同性戀](../Page/同性戀.md "wikilink")
  - [基督教](../Page/基督教.md "wikilink")、[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")
  - [難民](../Page/難民.md "wikilink")、[第三世界](../Page/第三世界.md "wikilink")
  - [亞裔](../Page/亞裔.md "wikilink")、[有色人種](../Page/有色人種.md "wikilink")

## 參見

  - [貼標籤](../Page/貼標籤.md "wikilink")
  - [標籤理論](../Page/標籤理論.md "wikilink")
  - [汙名認同](../Page/汙名認同.md "wikilink")
  - [皮格马利翁效应](../Page/皮格马利翁效应.md "wikilink")
  - [罗森汉恩实验](../Page/罗森汉恩实验.md "wikilink")
  - [自证预言](../Page/自证预言.md "wikilink")

## 外部連結

  - [二戰期間，美國心理學家在標籤效應的發現](http://xsc.sdai.edu.cn/Article.asp?articleid=593)
  - [英文中學等同名校的標籤效應](https://web.archive.org/web/20080517060637/http://trans.wenweipo.com/gb/paper.wenweipo.com/2007/07/11/FC0707110001.htm)
  - [教育巧用標籤效應](http://www.hsgj.com.cn/gzb/dyzc/xllz/xldh/200512/3399.html)
  - [高血壓病人的「標籤效應」](http://www.people.com.cn/GB/other4789/20030114/907297.html)
  - [超級女聲的群體標籤效應](http://www.shengyijing.com/NewsHTML/11898/11898_1.htm)
  - [罪犯資料庫的標籤效應](http://paper.wenweipo.com/2006/08/19/WW0608190002.htm)

[Category:社會學術語](../Category/社會學術語.md "wikilink")
[Category:社會學理論](../Category/社會學理論.md "wikilink")
[Category:成見](../Category/成見.md "wikilink")