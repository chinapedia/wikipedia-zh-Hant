是《[模擬市民2](../Page/模擬市民2.md "wikilink")》的第四款[資料片](../Page/資料片.md "wikilink")，亦針對了不同的[電視遊樂器而發行了對應版本](../Page/電視遊樂器.md "wikilink")，由[美商藝電發行](../Page/美商藝電.md "wikilink")。在[電腦平台方面](../Page/電腦.md "wikilink")，[Windows版於](../Page/Microsoft_Windows.md "wikilink")2006年10月18日發行到[北美地區](../Page/北美.md "wikilink")，並於10月20日發行到餘下地區；[Mac
OS
X版則於同年](../Page/Mac_OS_X.md "wikilink")11月7日由Aspyr發行。在電視遊樂器平台方面，[Wii版於](../Page/Wii.md "wikilink")2007年6月13日發行。

## 遊戲特色

### 寵物

模擬市民能飼養狗和貓作為寵物，並訓練自己的寵物來賺錢。市民能以責罵方式阻止寵物進行某些行為，或是以稱讚方式鼓勵和訓練他們的寵物。

以下是本資料片所新增的寵物，當中[貓和](../Page/貓.md "wikilink")[狗一旦被市民領養後便會計入玩家的家庭人數中](../Page/狗.md "wikilink")，每戶家庭最多只可飼養6隻寵物（只限貓狗）。

  - [狗](../Page/狗.md "wikilink")（72種內建品種）
  - [貓](../Page/貓.md "wikilink")（30種內建品種）
  - [天竺鼠](../Page/天竺鼠.md "wikilink")
  - [魚](../Page/魚.md "wikilink")
  - [鳥](../Page/鳥.md "wikilink")

市民可以透過以下的方式領養寵物：

  - 撥電話到領養中心
  - 在寵物創造模式（Create-A-Pet）自創貓狗
  - 收養流浪貓狗
  - 送贈或交易寵物
  - 到寵物店購買
  - 在購買模式下直接購買天竺鼠籠、[鳥籠或](../Page/鳥籠.md "wikilink")[魚缸](../Page/魚缸.md "wikilink")

### 狼人

[狼人是本資料片新增的人種](../Page/狼人.md "wikilink")。狼人只能於夜間生活。在這段時間內，牠們的精力會徐徐上升，但與此同時牠們會較容易飢餓。狼人訓練寵物的能力較常人的強，亦能打擊夜賊。牠們有時會向月光嗥叫，或召喚其他狼群。當狼人嗥叫的時候，在用地上所有模擬市民的如廁度會大大下降。要成為狼人，市民可讓狼群的頭狼（眼睛發光的狼）咬一口，或接觸其他的狼人。設置樹林、雜草和未經修剪的樹籬能吸引狼群。

### 職業

遊戲新增了不同的寵物職業，等同模擬市民的工作，寵物必須履行一些事務來取得晉升。

  - [保全業](../Page/保全.md "wikilink")
  - [娛樂業](../Page/娛樂.md "wikilink")
  - [服務業](../Page/服務業.md "wikilink")

### 追加下载内容

[希拉里·达夫作为追加下载内容出现在本作中](../Page/希拉里·达夫.md "wikilink")\[1\]。

## 音樂

以下是本資料片新增的[流行音樂](../Page/流行音樂.md "wikilink")，並以[模擬市民語重新演唱](../Page/模擬市民語.md "wikilink")：

  - [小野貓](../Page/小野貓.md "wikilink") - "Don't Cha"
  - Aly & AJ - "Chemicals React"
  - Cowboy Troy - "I Play Chicken with the Train"
  - Saving Jane - "Girl Next Door"
  - The Flaming Lips - "Free Radicals"
  - Skye Sweetnam - "Boyhunter"
  - The Format - "The Compromise"
  - The New Amsterdams - "Turn Out the Light"
  - The Films - "Black Shoes"

以下是其他地區語言版本獨有的歌曲（美國英語版除外）：

  - 丹麥語：West End Girls - "Booglurbia"
  - 荷蘭語：Krezip - "Can't You be Mine"
  - 芬蘭語：Roni - "Never Coming Back"
  - 法語：Kisha - "Sowieso"
  - 德語：Kisha - "Sowieso", Krezip - "Can't You be Mine"
  - 意大利語：Finley - "Run Away"
  - 日語：West End Girls - "Booglurbia"
  - 韓語：West End Girls - "Booglurbia"
  - 挪威語：[琳恩·瑪蓮](../Page/琳恩·瑪蓮.md "wikilink") - "What If"
  - 俄羅斯語：The Smokebreakers - "Baby Let's Dance"
  - 西班牙語：La Oreja de Van Gogh - "Dulce Locura"
  - 瑞典語：Lene Marlin - "What If", West End Girls - "Booglurbia"
  - 英國英語：Finley - "Run Away", West End Girls - "Booglurbia"

## 註釋

## 外部連結

  - [模擬市民2：寵物當家官方網站（美國）](https://web.archive.org/web/20070331231738/http://thesims2.ea.com/about/ep4_index.php)

  - [模擬市民2：寵物當家官方網站（台灣）](https://web.archive.org/web/20071011133040/http://thesims2.ea.com.tw/about/EP4.aspx)


[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[24](../Category/模拟人生系列.md "wikilink") [Category:PlayStation
2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:任天堂GameCube游戏](../Category/任天堂GameCube游戏.md "wikilink")
[Category:Wii游戏](../Category/Wii游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:任天堂DS游戏](../Category/任天堂DS游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Game Boy
Advance游戏](../Category/Game_Boy_Advance游戏.md "wikilink")
[Category:资料片](../Category/资料片.md "wikilink")
[Category:有追加下载内容的游戏](../Category/有追加下载内容的游戏.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")
[Category:可選主角性別遊戲](../Category/可選主角性別遊戲.md "wikilink")
[Category:艺电游戏](../Category/艺电游戏.md "wikilink")
[Category:寵物題材作品](../Category/寵物題材作品.md "wikilink")
[Category:官方简体中文化游戏](../Category/官方简体中文化游戏.md "wikilink")

1.