**模擬遊戲**（simulation
game），簡稱**SIM**或**SLG**。以電腦模擬真實世界當中的環境與事件，提供玩者一個近似於現實生活當中可能發生的情境的遊戲，都可以稱作模擬遊戲。除了[戀愛模擬遊戲](../Page/戀愛模擬遊戲.md "wikilink")，這一類的遊戲大多沒有明顯結局。

## 類型

### 战争模拟

  - [命令与征服系列](../Page/命令与征服系列.md "wikilink")

### 城市模拟

城市模擬，大多是沒有明確目標的遊戲，遊戲時間通常在仍有資源可以持續發展城市的前提下，沒有明確結束的時間點。玩家可自由建設或拆除建築物，或是藉由各種災難使城市產生模擬上的壓力。

  - [模擬城市系列](../Page/模擬城市.md "wikilink")
  - [模擬農場](../Page/模擬農場.md "wikilink")
  - [模擬大樓](../Page/模擬大樓.md "wikilink")
  - [Lincity](../Page/Lincity.md "wikilink")
  - [OpenCity](../Page/OpenCity.md "wikilink")
  - [Cities XL](../Page/Cities_XL.md "wikilink")
  - [CIties skylines](../Page/都市：天际线.md "wikilink")

### 经营模拟

  - [OpenTTD](../Page/OpenTTD.md "wikilink")
  - [Simutrans](../Page/Simutrans.md "wikilink")
  - [Bygfoot](../Page/Bygfoot.md "wikilink")
  - [都市运输](../Page/都市运输.md "wikilink")
  - [铁路大亨系列](../Page/铁路大亨.md "wikilink")（[Railroad
    Tycoon](../Page/铁路大亨.md "wikilink")）

### 培育模拟

培育模擬以養寵物，或是培養子女為主。

  - [任天狗](../Page/任天狗.md "wikilink")

  - 模擬山羊

### 人生模拟

  - [模拟人生系列](../Page/模拟人生系列.md "wikilink")（不包括第一代）
  - [第二人生](../Page/第二人生.md "wikilink")
  - [虛擬人生](../Page/虛擬人生.md "wikilink")

### 交通工具模拟

交通工具模擬，在提高遊戲畫面的精緻度與操作擬真的前提下，亦有可能做為該種交通工具的駕駛訓練之用。這相較於使用真實交通工具的駕訓，可以大幅節省費用，以及避免真實駕訓時發生意外的損失。

  - [Rig of Rods](../Page/Rig_of_Rods.md "wikilink")
  - [电车GO\!](../Page/电车GO!.md "wikilink")

#### 舰船模拟

  - [Danger from the Deep](../Page/Danger_from_the_Deep.md "wikilink")
  - Ship simulator 2008
  - Courtney Marchelletta Ship Simulator

#### 飞行模拟

  - [FlightGear](../Page/FlightGear.md "wikilink")
  - [X-Plane](../Page/X-Plane.md "wikilink")
  - [微軟模擬飛行系列](../Page/微軟模擬飛行.md "wikilink")

#### 赛车模拟

参考 [赛车游戏](../Page/赛车游戏.md "wikilink")

#### 其他交通工具模擬

  - [NS training simulator](../Page/NS_training_simulator.md "wikilink")
  - [電車GO！Final](../Page/電車GO！Final.md "wikilink")
  - [gogoMRT](../Page/gogoMRT.md "wikilink")
  - [Bus Simulator 2012](../Page/Bus_Simulator_2012.md "wikilink")
  - [Digger simulator2011](../Page/Digger_simulator2011.md "wikilink")
  - [18輪大卡車系列](../Page/18輪大卡車系列.md "wikilink")
      - [硬盤卡車:18輪大卡車](../Page/硬盤卡車:18輪大卡車.md "wikilink")
      - [18輪大卡車穿越美國](../Page/18輪大卡車穿越美國.md "wikilink")
      - [18輪大卡車踏板的金屬](../Page/18輪大卡車踏板的金屬.md "wikilink")
      - [18輪大卡車:車隊](../Page/18輪大卡車:車隊.md "wikilink")
      - [18輪大卡車:Haulin'](../Page/18輪大卡車:Haulin'.md "wikilink")
      - [18輪大卡車:美國長距](../Page/18輪大卡車:美國長距.md "wikilink")
  - [卡车模拟系列](../Page/卡车模拟系列.md "wikilink")
      - [欧洲卡车模拟](../Page/欧洲卡车模拟.md "wikilink")
      - [欧洲卡车模拟2](../Page/欧洲卡车模拟2.md "wikilink")
      - [美洲卡車模擬](../Page/美洲卡車模擬.md "wikilink")

### 太空交易战斗模拟

  - [X (游戏系列)](../Page/X_\(游戏系列\).md "wikilink")
  - [Vega Strike](../Page/Vega_Strike.md "wikilink")
  - [Oolite](../Page/Oolite.md "wikilink")

### 太空战斗模拟

## 相關條目

  - [戀愛模擬遊戲](../Page/戀愛模擬遊戲.md "wikilink")
  - [電子遊戲類型\#模擬遊戲](../Page/電子遊戲類型#模擬遊戲.md "wikilink")

[\*](../Category/模擬遊戲.md "wikilink")
[Category:电脑游戏](../Category/电脑游戏.md "wikilink")
[Category:电子游戏类型](../Category/电子游戏类型.md "wikilink")