**樹林
伸**（、），日本人，[漫畫原作者](../Page/漫畫原作者.md "wikilink")、[小說家](../Page/小說家.md "wikilink")、[腳本家](../Page/腳本家.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")，男性，O型血。[東京都立武藏高等學校](../Page/東京都立武藏高等學校.md "wikilink")、[早稻田大學](../Page/早稻田大學.md "wikilink")[政治經濟學部畢業](../Page/政治經濟學部.md "wikilink")。其作品許多都被改編成動畫或者日劇。興趣為畫水彩畫以及酒的收藏等。喜歡現代化的舞台設定，作品多為需要動腦的內容以及神秘情節的組合。

## 筆名

因自己的作品種類繁多，所以有很多筆名。

  - 安童夕馬（あんどうゆうま）－多與漫畫家[朝基勝士合作](../Page/朝基勝士.md "wikilink")，作品有[感應少年EIJI](../Page/感應少年EIJI.md "wikilink")、[王牌至尊等](../Page/王牌至尊.md "wikilink")。
  - 青樹佑夜（あおきゆうや）－多與漫畫家[綾峰欄人合作](../Page/綾峰欄人.md "wikilink")，作品有[閃靈二人組等](../Page/閃靈二人組.md "wikilink")。
  - 天樹征丸（あまぎせいまる）－多與漫畫家[佐藤文也合作](../Page/佐藤文也.md "wikilink")，作品有[金田一少年之事件簿系列](../Page/金田一少年之事件簿.md "wikilink")。
  - 有森丈時（ありもり じょうじ）
  - 伊賀大晃（いがの
    ひろあき）－與漫畫家[月山可也合作](../Page/月山可也.md "wikilink")，作品有[足球騎士](../Page/足球騎士.md "wikilink")。
  - 龍門諒（りゅうもん
    りょう）－與漫畫家[惠廣史合作](../Page/惠廣史.md "wikilink")，作品有[血色星期一系列](../Page/血色星期一.md "wikilink")。
  - S.K －英文名Shin Kibayashi的縮寫
  - 紀林（キバヤシ）－來自漫畫[MMR神秘調查班的主人公](../Page/MMR神秘調查班.md "wikilink")「紀林隊長」，多為樹林伸網名使用。
  - 亞樹直（あぎ ただし）

注1：「亞樹直」是樹林伸姊弟共用的筆名，由他們姊弟兩人所組成。有時也以亞樹直A（姊）、亞樹直B（弟）為名。 \[1\]
注2：名字的讀音多以日文的「あ」行開始為其特徵。

## 經歷

1987年進入講談社，在[週刊少年Magazine擔任漫畫編輯](../Page/週刊少年Magazine.md "wikilink")。為漫畫《[MMR神秘調查班](../Page/MMR神秘調查班.md "wikilink")》的主人公「紀林隊長」的原型人物。編輯時擔當的作品有《[足球風雲](../Page/足球風雲.md "wikilink")》、《[麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")》。

擔任過《[金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")》的責任編輯，原案，現為該作品原作者。

1999年從講談社辭職。正式獨立為原作者。

## 主要作品

### 天樹征丸名義

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")
      - File系列（原案）\[2\] 、Short File系列、Akechi File系列、Case系列、新系列、小說系列。
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")
  - [遙控刑警](../Page/遙控刑警.md "wikilink")
  - [東京ゲンジ物語](../Page/東京ゲンジ物語.md "wikilink")
  - [零 影巫女](../Page/零_影巫女.md "wikilink")

### 安童夕馬名義

  - [感應少年EIJI](../Page/感應少年EIJI.md "wikilink")（第二部于2011年4月25日開始連載、漫畫：[朝基勝士](../Page/朝基勝士.md "wikilink")）
  - [東京80年代](../Page/東京80年代.md "wikilink")（原名：東京エイティーズ、漫畫：[大石知征](../Page/大石知征.md "wikilink")）
  - [王牌至尊](../Page/王牌至尊.md "wikilink") -
    2003年（第27回）[講談社漫畫賞少年部門受賞](../Page/講談社漫畫賞.md "wikilink")
  - [少年刑警](../Page/少年刑警.md "wikilink")
  - [偵探犬夏多克](../Page/偵探犬夏多克.md "wikilink")
  - [新宿D×D](../Page/新宿D×D.md "wikilink")（於2月底ＭＡＮＧＡＢＯＸ上連載，與台灣漫畫家[彭傑合作](../Page/彭傑.md "wikilink")）

### 青樹佑夜名義

  - [超異能少年](../Page/超異能少年.md "wikilink")
    （原名：、漫畫：[奈央晃德](../Page/奈央晃德.md "wikilink")》
  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")
  - [鬼若與牛若](../Page/鬼若與牛若.md "wikilink")

### 亞樹直名義

  - [心靈X檔案](../Page/心靈X檔案.md "wikilink")（原名：、漫畫：[的場健](../Page/的場健.md "wikilink")）
  - [心理醫恭介](../Page/心理醫恭介.md "wikilink")（原名：、漫畫：[沖本秀](../Page/沖本秀.md "wikilink")，心理X檔案的續作）
  - [神之雫](../Page/神之雫.md "wikilink")
  - [校園恐怖傳說](../Page/校園恐怖傳說.md "wikilink")（原名：學校の怖い噂、漫畫：疋田美幸）
  - [紅酒怪盜](../Page/紅酒怪盜.md "wikilink")（原名：、漫畫：沖本秀）

### 有森丈時名義

  - [猴王五九](../Page/猴王五九.md "wikilink")（原名：、漫畫：葵露夢）
  - [雪上企鵝](../Page/雪上企鵝.md "wikilink")（原名：スノードルフィン、漫畫：大石知征）

### 伊賀大晃名義

  - [足球騎士](../Page/足球騎士.md "wikilink")（原名：、漫画：月山可也）

### 龍門諒名義

  - [血色星期一系列](../Page/血色星期一.md "wikilink")（漫畫：[惠廣史](../Page/惠廣史.md "wikilink")、週刊少年MAGAZINE）

<!-- end list -->

  -
    1st Season（BLOODY MONDAY）全11卷
    2nd Season（Season2～潘朵拉之盒～）全8卷
    Last Season （最終時刻）全4卷

### 樹林伸名義

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")
      - File系列（原案、編輯）\[**歌劇院殺人事件**-**墓場島殺人事件** 未註明\]
  - 風と雷（漫畫：星野泰視、モーニング短期集中連載）
  - 『リインカーネイション戀愛輪迴』（光文社）
  - 『ビット・トレーダー』（幻冬舎）
  - 『「でっけえ歌舞伎」入門マンガの目で見た市川海老蔵』（講談社）
  - [機兵戰記](../Page/機兵戰記.md "wikilink")、原名《機兵戦記レガシーズ》

### 日劇

  - [感應少年EIJI](../Page/感應少年EIJI.md "wikilink")（改編自漫畫《感應少年EIJI》）
  - [HERO](../Page/HERO_\(日本電視劇\).md "wikilink")(企劃協助)
  - [遙控刑警](../Page/遙控刑警.md "wikilink")（原案、腳本）
  - [ピッキングトリオの事件簿](../Page/ピッキングトリオの事件簿.md "wikilink")（[ネプチューン主演のテレビドラマ](../Page/ネプチューン\(コントグループ\).md "wikilink")）
  - 心理醫恭介之系列構成(改編自漫畫《心理醫生楷恭介》)
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink") （改編自漫畫《偵探學園Q》）
  - [天才駭客F](../Page/天才駭客F.md "wikilink") (改編自漫畫《血色星期一》）
  - [天才駭客F2](../Page/天才駭客F2.md "wikilink") (改編自漫畫《血色星期一2 潘朵拉之盒》）

## 出演

  - 熱血\!ラジカルチャー（星期四，以樹林伸名義）-[日本放送電台](../Page/日本放送電台.md "wikilink")

## 外部連結

  - 公式blog

      - [「神の雫」作者のワイン日記](http://d.hatena.ne.jp/agitadashi/)

      - [天樹征丸日記](http://d.hatena.ne.jp/seiama/)

  - [](https://archive.is/20130501082230/talent.yahoo.co.jp/pf/detail/pp324623)

  -
## 備註

<references/>

[K](../Category/亞樹直.md "wikilink")
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:漫畫原作者](../Category/漫畫原作者.md "wikilink")
[Category:共用筆名](../Category/共用筆名.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")

1.  [2007年4月11日的朝日新聞「朝日.com
    「食」之頁」的連載專欄中，姊姊**亞樹直A**在其訪問中如此回答](http://www.asahi.com/food/column/nommelier/TKY200709060209.html#profile)
2.  使用**樹林伸**名義擔任\[歌劇院殺人事件－墓場島殺人事件\]的原案和編集，漫畫未注明。使用**天樹征丸**名義擔任\[魔術列車殺人事件－速水玲香誘拐殺人事件\]的原案。現使用**天樹征丸**名義擔任其它系列的原作。