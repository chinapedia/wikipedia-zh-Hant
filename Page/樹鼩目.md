**樹鼩目**（[學名](../Page/學名.md "wikilink")：），又称**攀兽目**，生活在[东南亚的热带雨林中](../Page/东南亚.md "wikilink")，有2科5属20种。樹鼩目成员的外形像[松鼠](../Page/松鼠.md "wikilink")，但吻尖而长。樹鼩目成员的齿分化不明显。

## 分类

樹鼩目曾经被置于[食虫目与](../Page/食虫目.md "wikilink")[灵长目](../Page/灵长目.md "wikilink")，樹鼩目成员的习性和外形有[食虫目的特征](../Page/食虫目.md "wikilink")，但其[头骨的特征与某些原始的原猴类相似](../Page/头骨.md "wikilink")，因此科学家曾对于樹鼩目该被置于[食虫目还是](../Page/食虫目.md "wikilink")[灵长目有不少争议](../Page/灵长目.md "wikilink")。后来[食虫目被发现是一个](../Page/食虫目.md "wikilink")[多系群](../Page/多系群.md "wikilink")，其成员陆续独立出来或被分置于其它目，食虫目因而被取消了\[1\]。树鼩目则依分子学研究而被置于[灵长总目之下的](../Page/灵长总目.md "wikilink")[灵长动物分支](../Page/灵长动物.md "wikilink")，成为[皮翼目和](../Page/皮翼目.md "wikilink")[灵长目的近缘](../Page/灵长目.md "wikilink")[旁系群](../Page/旁系群.md "wikilink")。

依灵长形学说，本目与灵长总目各目的关系如下：

### 内部分类

  - **樹鼩目** Scandentia
      - [树鼩科](../Page/树鼩科.md "wikilink") Tupaiidae
          - [南印树鼩属](../Page/南印树鼩属.md "wikilink") *Anathana*
              - [南印树鼩](../Page/南印树鼩.md "wikilink") *Anathana ellioti*
          - [细尾树鼩属](../Page/细尾树鼩属.md "wikilink") *Dendrogale*
              - [婆罗洲细尾树鼩](../Page/婆罗洲细尾树鼩.md "wikilink") *Dendrogale
                melanura*
              - [北细尾树鼩](../Page/北细尾树鼩.md "wikilink") *Dendrogale murina*
          - [树鼩属](../Page/树鼩属.md "wikilink") *Tupaia*
              - [缅甸树鼩](../Page/缅甸树鼩.md "wikilink") *Tupaia belangeri*
              - [金腹树鼩](../Page/金腹树鼩.md "wikilink") *Tupaia chrysogaster*
              - [背纹树鼩](../Page/背纹树鼩.md "wikilink") *Tupaia dorsalis*
              - [普通树鼩](../Page/普通树鼩.md "wikilink") *Tupaia glis*
              - [细树鼩](../Page/细树鼩.md "wikilink") *Tupaia gracilis*
              - [霍氏树鼩](../Page/霍氏树鼩.md "wikilink") *Tupaia javanica*
              - [长脚树鼩](../Page/长脚树鼩.md "wikilink") *Tupaia longipes*
              - [倭树鼩](../Page/倭树鼩.md "wikilink") *Tupaia minor*
              - [卡拉绵树鼩](../Page/卡拉绵树鼩.md "wikilink") *Tupaia
                moellendorffi*
              - [山树鼩](../Page/山树鼩.md "wikilink") *Tupaia montana*
              - [尼科巴树鼩](../Page/尼科巴树鼩.md "wikilink") *Tupaia nicobarica*
              - [巴拉望树鼩](../Page/巴拉望树鼩.md "wikilink") *Tupaia
                palawanensis*
              - [彩树鼩](../Page/彩树鼩.md "wikilink") *Tupaia picta*
              - [红尾树鼩](../Page/红尾树鼩.md "wikilink") *Tupaia splendidula*
              - [大树鼩](../Page/大树鼩.md "wikilink") *Tupaia tana*
          - [菲律宾树鼩属](../Page/菲律宾树鼩属.md "wikilink") *Urogale*
              - [菲律宾树鼩](../Page/菲律宾树鼩.md "wikilink") *Urogale evereti*
      - [筆尾樹鼩科](../Page/筆尾樹鼩科.md "wikilink") Ptilocercidae
          - [筆尾树鼩属](../Page/筆尾树鼩属.md "wikilink") *Ptilocercus*
              - [筆尾树鼩](../Page/筆尾树鼩.md "wikilink") *Ptilocercus lowii*

## 参考文献

[\*](../Category/樹鼩目.md "wikilink")
[Category:統獸總目](../Category/統獸總目.md "wikilink")

1.