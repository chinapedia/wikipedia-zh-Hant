**橋本市**（）是[和歌山縣東北部的](../Page/和歌山縣.md "wikilink")[市](../Page/市.md "wikilink")。面積130.24平方公里，總人口68,525人。

## 历史

  - 1955年1月1日、[伊都郡橋本町](../Page/伊都郡.md "wikilink")、紀見村、山田村、岸上村、隅田村、學文路村合併，[市制施行](../Page/市制_\(日本制度\).md "wikilink")
  - 2006年3月1日、[高野口町合併入該市](../Page/高野口町.md "wikilink")
  - 2006年3月1日、新橋本市誕生

## 外部連結

  - [橋本市](http://www.city.hashimoto.wakayama.jp/)