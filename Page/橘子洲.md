[Mao_Zedong_youth_art_sculpture_4.jpg](https://zh.wikipedia.org/wiki/File:Mao_Zedong_youth_art_sculpture_4.jpg "fig:Mao_Zedong_youth_art_sculpture_4.jpg")\]\]

**橘子洲**位于[湖南省](../Page/湖南省.md "wikilink")[长沙市城区的](../Page/长沙市.md "wikilink")[湘江中](../Page/湘江.md "wikilink")，是一个狭长的[沙洲](../Page/沙洲.md "wikilink")\[1\]，又名**桔子洲**、**桔洲**、**橘洲**、**水陆洲**。南北全长约5公里；东西最宽处约140米，最窄约40米。洲上有居民居住，现因为整体规划已迁出许多\[2\]。橘子洲于2001年成为长沙[岳麓山风景名胜区橘子洲景区](../Page/岳麓山风景名胜区.md "wikilink")。每周六晚8:30～8:50放焰火（高考、中考期间和冬季取消），焰火当天从6:00就会禁止进入橘子洲，并且[长沙地铁2号线会跳过](../Page/长沙地铁2号线.md "wikilink")[橘子洲站而不停站](../Page/橘子洲站.md "wikilink")（有时候[湘江中路站也不停站](../Page/湘江中路站.md "wikilink")）。

2016年8月3日，[国家旅游局宣布撤消橘子洲旅游区](../Page/国家旅游局.md "wikilink")5A景区资质，原因是景区安全隐患严重、环境卫生差、旅游服务功能严重退化和管理不规范\[3\]。

2017年12月18日，经过全国旅游资源规划开发质量评定委员会组织专家组复核，橘子洲景区恢复5A景区资质。\[4\]

## 历史

《[水经注](../Page/水经注.md "wikilink")·湘水》：“[湘水之北径南津城西](../Page/湘水.md "wikilink")，西对橘洲。”《[荆州记](../Page/荆州记.md "wikilink")》：“乃至[夏水怀山](../Page/夏水.md "wikilink")，诸州皆没，橘州独在。”

长沙的名称来历中的一种说法就是因为橘子洲形成之前是江中一道长长的沙滩，因此得名“长沙”。据史料的记载，橘子洲形成于[晋惠帝永兴二年](../Page/晋惠帝.md "wikilink")（公元305年）。[唐朝时洲上盛产南橘](../Page/唐朝.md "wikilink")，在江汉地区盛销。[杜甫有诗](../Page/杜甫.md "wikilink")“桃源人家易制度，橘洲田土仍膏腴”。[潇湘八景中](../Page/潇湘八景.md "wikilink")“江天暮雪”就是描绘橘子洲的。洲上原有水陆寺、拱极楼、江心楼，现已不复存在。据清《长沙县志》记载，橘子洲分上洲、中洲、下洲，即牛头洲、水陆洲、傅家洲。历史上三洲只有在水位极低的情况下才出现首尾相连的情形，所以长沙古谚有-{云}-：“三洲连，出状元。”但近代以来，由于泥沙淤积，现除了傅家洲在水位高时隔断外，三洲基本一体。[清朝末年](../Page/清朝.md "wikilink")1904年长沙对外开放，辟为商埠，自1911年起，洲上开始形成外侨居住区，包括[英国](../Page/英国.md "wikilink")、日本[领事馆](../Page/领事馆.md "wikilink")、海关税务司，以及英、德、美、日商人住宅。[民国时期](../Page/民国.md "wikilink")[唐生智也在此有公馆](../Page/唐生智.md "wikilink")。如今还有少数建筑保存下来。\[5\]

橘子洲头位于橘子洲的最南端，现在是**橘洲公园**的一部分。[朱熹](../Page/朱熹.md "wikilink")、[张栻当年往来于湘江之上](../Page/张栻.md "wikilink")，就绕过橘子洲头。[毛泽东从年轻时到建国后多次来这里观景](../Page/毛泽东.md "wikilink")、游泳。\[6\]1925年他在此写下《[沁园春](../Page/沁园春.md "wikilink")·长沙》，发出“问苍茫大地，谁主沉浮”的感慨。建国后他七次到橘子洲头附近的湘江游泳。1974年，81岁的毛泽东最后一次回到湖南长沙，10月15日仍然来到橘子洲头追思。1982年橘洲公园建成，公园里有毛泽东的“橘子洲头”的题字和《沁园春·长沙》的词一体的碑，亭榭，成片的橘林。

2009年12月20日，[毛泽东青年艺术雕塑在橘子洲矗立](../Page/毛泽东青年艺术雕塑.md "wikilink")。\[7\]\[8\]

从2010年起，橘子洲每年举办“橘子洲国际音乐节”。\[9\]\[10\]\[11\]\[12\]

2014年4月29日，随着[长沙地铁2号线一期工程的开通试运营](../Page/长沙地铁2号线.md "wikilink")，[橘子洲站开放运营](../Page/橘子洲站.md "wikilink")。

## 照片集

[左](https://zh.wikipedia.org/wiki/File:橘子洲头东全景照.jpg "fig:左")
[左](https://zh.wikipedia.org/wiki/File:橘子洲头西全景照.jpg "fig:左")
{| class="wikitable" |- |
[Orange_continent_head_13.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_13.jpg "fig:Orange_continent_head_13.jpg")
|
[Orange_continent_head_12.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_12.jpg "fig:Orange_continent_head_12.jpg")、[蔡和森](../Page/蔡和森.md "wikilink")、[萧三](../Page/萧三.md "wikilink")、[向警予等人的雕像](../Page/向警予.md "wikilink")\]\]
|
[海关公廨旧址.jpg](https://zh.wikipedia.org/wiki/File:海关公廨旧址.jpg "fig:海关公廨旧址.jpg")
|- |
[Orange_continent_head_10.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_10.jpg "fig:Orange_continent_head_10.jpg")
|
[Orange_continent_head_20131121.JPG](https://zh.wikipedia.org/wiki/File:Orange_continent_head_20131121.JPG "fig:Orange_continent_head_20131121.JPG")
|
[Orange_continent_head_3.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_3.jpg "fig:Orange_continent_head_3.jpg")
|- |
[Orange_continent_head_6.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_6.jpg "fig:Orange_continent_head_6.jpg")
|
[Orange_continent_head_8.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_8.jpg "fig:Orange_continent_head_8.jpg")
|
[Orange_continent_head_9.jpg](https://zh.wikipedia.org/wiki/File:Orange_continent_head_9.jpg "fig:Orange_continent_head_9.jpg")
|- |
[橘子洲焰火《你好青春》1.jpg](https://zh.wikipedia.org/wiki/File:橘子洲焰火《你好青春》1.jpg "fig:橘子洲焰火《你好青春》1.jpg")
|
[橘子洲焰火《你好青春》2.jpg](https://zh.wikipedia.org/wiki/File:橘子洲焰火《你好青春》2.jpg "fig:橘子洲焰火《你好青春》2.jpg")
|
[橘子洲焰火《你好青春》3.jpg](https://zh.wikipedia.org/wiki/File:橘子洲焰火《你好青春》3.jpg "fig:橘子洲焰火《你好青春》3.jpg")
|- |
[位于橘子洲景区内的观光车.jpg](https://zh.wikipedia.org/wiki/File:位于橘子洲景区内的观光车.jpg "fig:位于橘子洲景区内的观光车.jpg")
|
[位于橘子洲景区内的导览图.jpg](https://zh.wikipedia.org/wiki/File:位于橘子洲景区内的导览图.jpg "fig:位于橘子洲景区内的导览图.jpg")
|
[长沙地铁2号线橘子洲站.jpg](https://zh.wikipedia.org/wiki/File:长沙地铁2号线橘子洲站.jpg "fig:长沙地铁2号线橘子洲站.jpg")
|}
\== 参考文献 ==

## 外部链接

  -
{{-}}

[长](../Category/长沙公园.md "wikilink")
[湘](../Category/长江流域岛屿.md "wikilink")
[湖南](../Category/湖南历史文化遗迹.md "wikilink")
[岳麓山](../Category/岳麓山风景名胜区.md "wikilink")
[湘](../Category/红色旅游.md "wikilink")
[Category:湖南岛屿](../Category/湖南岛屿.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.