**橙黃金翅雀鯛**，又稱**雷克斯刻齒雀鯛**、**帝王雀鯛**，俗名為厚殼仔、檸檬雀鯛，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[雀鯛科的其中一](../Page/雀鯛科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[澳洲北部](../Page/澳洲.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[新不列顛](../Page/新不列顛.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[所羅門群島](../Page/所羅門群島.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[萬那杜等海域](../Page/萬那杜.md "wikilink")。

## 深度

水深1至20公尺。

## 特徵

本魚體橢圓形而側扁，體呈[橙](../Page/橙色.md "wikilink")[黃色](../Page/黃色.md "wikilink")，體被小鱗。眶前骨無鱗，眶下骨具鱗；前鰓蓋骨後緣平滑。頰部有鱗。頭及背部前端略帶[紫色](../Page/紫色.md "wikilink")，體側每一鱗片中央皆有一藍點，頭部則藍點密佈而使其遠看有如[藍色](../Page/藍色.md "wikilink")，尤其是幼魚中央頭和身體更是藍橙分色。成魚後則色則轉淡，有時藍色亦褪成紫[灰色](../Page/灰色.md "wikilink")，鰓蓋末端具一黑點。吻短而鈍圓。口中型；兩頜齒小而呈圓錐狀。體被櫛鱗；側線之有孔鱗片15至18個。背鰭硬棘13枚、背鰭軟條13至14枚、臀鰭硬棘2枚、臀鰭軟條13至14枚。體長可達10公分。

## 生態

本魚多半在礁緣底部較光禿之岩壁凹入處獨居，為藻食性魚類，以吃食附生在石上的[藻類為食](../Page/藻類.md "wikilink")。

## 經濟利用

體色鮮艷，適合飼養觀賞用，少人食用。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[rex](../Category/刻齿雀鲷属.md "wikilink")