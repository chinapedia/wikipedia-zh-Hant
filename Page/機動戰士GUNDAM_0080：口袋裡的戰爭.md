《**機動戰士GUNDAM0080
口袋裡的戰爭**》（，）是[GUNDAM系列作品當中的第一部以](../Page/GUNDAM系列作品.md "wikilink")[OVA形式發行](../Page/OVA.md "wikilink")，也是第一部監督非[富野由悠季的GUNDAM映像作品](../Page/富野由悠季.md "wikilink")。於1989年製作，全六話。

## 故事

[一年戰爭末期](../Page/一年戰爭.md "wikilink")，[吉恩軍特殊部隊](../Page/吉翁公國.md "wikilink")「獨眼巨人」(Cyclops)查知[地球聯邦軍正著手開發新型的](../Page/地球聯邦軍.md "wikilink")[GUNDAM](../Page/RX-78系列機動戰士.md "wikilink")，為了奪取該機體而襲擊北極的聯邦軍基地，但任務失敗，目標被送上宇宙。後來經由偶然得手的情報，得知新型GUNDAM已被運往中立的[太空殖民地](../Page/太空殖民地.md "wikilink")[Side6的吉翁軍再度派遣獨眼巨人隊潛入](../Page/宇宙殖民地_\(GUNDAM世界\)#Side6里亞.md "wikilink")
Side6，並企圖奪取。\[1\]

本故事在新型GUNDAM奪取作戰的縱軸以及住在Side6，狂熱愛好[機動戰士的小學生阿爾與獨眼巨人隊中的新兵巴尼之間的關係作為橫軸當中交錯的展開](../Page/機動戰士.md "wikilink")。劇中的[RX-78NT1
ALEX雖然在設定上是準備交給](../Page/RX-78系列机动战士.md "wikilink")[阿姆羅·雷的機體](../Page/阿姆羅·雷.md "wikilink")，但是基本上是一部與[機動戰士GUNDAM本篇沒有直接關係的外傳故事](../Page/機動戰士GUNDAM.md "wikilink")。\[2\]

另外，小說與動畫的結局有若干差異。\[3\]在動畫當中，巴尼在駕駛倉被直擊當場死亡；而小說故事中，阿爾父母的對話中提及：「據說巴尼奇蹟獲救」\[4\]。在愛好者之間的評價相當分歧，但是除了結局外，兩個作品的主題並沒有太大的差別。小說版的作者曾說過：這樣的結局會讓一流悲劇變成三流喜劇，但因為巴尼在他心中太過真實的存在著，讓他即使是背上破壞結局的原罪也要創造出一個巴尼存活的平行世界。

## 作品解說

基於[機動戰士Z
GUNDAM以及各代GUNDAM作品在商業上的成功](../Page/機動戰士Z_GUNDAM.md "wikilink")，Sunrise經營層與身為出資者的BANDAI希望能繼續製作新的GUNDAM系列動畫作品。但是經過1985到1987兩年連續的電視版（Z與ZZ）加上1988年3月公開的劇場版（[機動戰士GUNDAM
逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")）Sunrise所屬的動畫員們已經沒有體力再負擔新的電視版作品。為了在兩者之間取得平衡，0080在企畫之初作了兩項大調整，第一是為求縮減動畫員工作量而捨棄電視版，選擇製作期間較短的OVA，第二是讓連續擔任三部作品監督的富野由悠季休息，而起用其他的監督。

作為首部非富野GUNDAM的0080，第一人称视角是一名小學生這點也打破當時為止的GUNDAM系列的慣例。但是由於新颖的故事，以及細密的機械描寫，在當時被認為是劃時代的作品。過去的GUNDAM系列主要角色幾乎都是[新人類或者菁英份子](../Page/新人類.md "wikilink")，而本作品則完全都是普通人，在保持中立的Side6作為局部戰鬥的舞台也讓本作品成為在平穩的生活中產生「戰爭」的實感。將人們決定賭上性命而戰的姿態，以及這樣的行為對大局來說只有些微意義的巨觀視野同時被描寫，來凸顯出戰爭的不合理性，是高山文彥監督本身所希望呈現的人文關懷。应该说这样的反战主题在历史上的很多文学作品中其实已并不罕见。但是抛开本身情节发展略显巧合以及牵强不谈，在一个需要以不断的战争来维系的传统“英雄”中心型的动画系列中，此种直抵人心而现实主义的新式作品首次出现，仍是让人耳目一新而印象深刻。

作品中登場的一年戰爭兵器設計不止經由機械設定[出渕裕親手改良](../Page/出渕裕.md "wikilink")，吉翁軍也有新MS（[MS-18E
肯普法](../Page/MS-18系列機動戰士.md "wikilink")）
登場。這部分的改良作業理論上應該只是把當年初代GUNDAM電視版在技術或成本上無法達到的細部描寫加強，但是最後的成果，在GUNDAM設定研究者之間普遍認為有點修改過頭，不像是一年戰爭兵器，反而還比較像逆襲時代的機體（出渕裕當時也兼任逆襲的機械設定者）而初代本傳中登場的[MS-06
-{zh-hans:扎古;zh-hk:渣古;zh-tw:薩克;}-II](../Page/MS-06系列机动战士.md "wikilink")，[MSM-03
战蟹](../Page/MSM-03.md "wikilink")，[MSM-07
茲寇克](../Page/MSM-07.md "wikilink")，[RGM-79
吉姆等機體在](../Page/RGM-79.md "wikilink")0080也分別被冠上\~Fz，\~C，\~E，\~Commando等頭尾，成為與本傳不同系列的機種。

這些措施雖然從模型商品化的角度來看是很容易理解的，但是從自此之後的鋼彈系列作品中，各式前作的機體被不斷的細分化並且衍生出新機體，而且經常不考慮與其他作品的整合性（到了[機動戰士GUNDAM
第08MS小隊甚至出現了後作將前作的歷史設定推翻的怪現象](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")。）這樣的狀況雖然在愛好者間評價兩極，但是在模型系列開展的意義上，0080佔有非常重要的地位。

## 主要登場人物

  -
    暱稱阿爾（アル），11歲，小學五年生，出身於保持中立的Side6（即故事舞台）。
      -
        香港譯名：亞魯，　粵語配音：[袁淑珍](../Page/袁淑珍.md "wikilink")（星光娛樂 LD 版）
  -
    暱稱克莉絲（クリス），21歲，地球連邦宇宙軍中尉、新類型人專用MS開發G-4部隊測試駕駛員。
      -
        香港譯名：古莉絲，粵語配音：[譚淑英](../Page/譚淑英.md "wikilink")（星光娛樂 LD 版）
  -
    暱稱巴尼（バーニィ），19歲，自護軍突擊機動軍伍長、特殊部隊「獨眼巨人隊」隊員。
      -
        香港譯名：巴列，　粵語配音：[陸頌愚](../Page/陸頌愚.md "wikilink")（星光娛樂 LD 版）
  -
    44歲，軍階為大尉，特殊部隊「獨眼巨人隊」隊長。
  -

    暱稱米夏（ミーシャ）

為阿爾配音的浪川大輔當時年僅12歲，為歷代[GUNDAM系列作品最年少的男主角配音者](../Page/GUNDAM系列作品.md "wikilink")。

## 主要工作人員

  - 監督：[高山文彦](../Page/高山文彦.md "wikilink")
  - 構成：[結城恭介](../Page/結城恭介.md "wikilink")
  - 脚本：[山賀博之](../Page/山賀博之.md "wikilink")
  - 總合設定：[出渕裕](../Page/出渕裕.md "wikilink")
  - 人物設定：[美樹本晴彦](../Page/美樹本晴彦.md "wikilink")
  - MS原案：[大河原邦男](../Page/大河原邦男.md "wikilink")
  - 音樂：[かしぶち哲郎](../Page/かしぶち哲郎.md "wikilink")
  - 原作：[矢立肇](../Page/矢立肇.md "wikilink")/[富野由悠季](../Page/富野由悠季.md "wikilink")
  - 企畫・製作・著作：[Sunrise](../Page/Sunrise.md "wikilink")

## 主題歌

  - 片頭曲『』

作詞、作曲：[椎名恵](../Page/椎名恵.md "wikilink")／編曲：[戶塚修](../Page/戶塚修.md "wikilink")／演唱：椎名恵（[King
Records](../Page/King_Records.md "wikilink")）

  - 片尾曲『』

作詞、作曲：椎名恵／編曲：戶塚修／演唱：椎名恵（King Records）

## 各集標題

<table>
<thead>
<tr class="header">
<th><p>話数</p></th>
<th><p>副标题</p></th>
<th><p>英文标题</p></th>
<th><p>中文标题</p></th>
<th><p>脚本</p></th>
<th><p>故事</p></th>
<th><p>演出</p></th>
<th><p>作画監督</p></th>
<th><p>机械作画監督</p></th>
<th><p>发售日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>How Many Miles to the Battlefield?</p></td>
<td><p>到战场还有几哩？</p></td>
<td><p><a href="../Page/山賀博之.md" title="wikilink">山賀博之</a></p></td>
<td><p><a href="../Page/高山文彦_(アニメ監督).md" title="wikilink">高山文彦</a></p></td>
<td><p><a href="../Page/高松信司.md" title="wikilink">高松信司</a></p></td>
<td><p>斉藤格</p></td>
<td><p><a href="../Page/岩瀧智.md" title="wikilink">岩瀧智</a></p></td>
<td><p>1989年3月25日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>Reflections in a Brown Eye</p></td>
<td><p>茶色眼裡的景象</p></td>
<td><p><a href="../Page/佐藤順一.md" title="wikilink">甚目喜一</a></p></td>
<td><p>横山広行</p></td>
<td><p><a href="../Page/窪岡俊之.md" title="wikilink">窪岡俊之</a></p></td>
<td><p>1989年4月25日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>And at the End of the Rainbow?</p></td>
<td><p>彩虹的尽头是？</p></td>
<td><p>高山文彦<br />
高松信司</p></td>
<td><p>高松信司</p></td>
<td><p><a href="../Page/川元利浩.md" title="wikilink">川元利浩</a><br />
<a href="../Page/富沢和雄.md" title="wikilink">富沢和雄</a></p></td>
<td><p>1989年5月25日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>Over the River and Through the Woods</p></td>
<td><p>越过河流，穿过森林</p></td>
<td><p>高山文彦</p></td>
<td><p>横山広行</p></td>
<td><p>窪岡俊之</p></td>
<td><p>1989年6月25日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>Say it Ain't So, Bernie!</p></td>
<td><p>告诉我你在说谎，巴尼</p></td>
<td><p>甚目喜一</p></td>
<td><p>高松信司</p></td>
<td><p>川元利浩<br />
<a href="../Page/小林利充.md" title="wikilink">小林利充</a><br />
富沢和雄</p></td>
<td><p>1989年7月25日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>War in the Pocket</p></td>
<td><p>口袋里的战争</p></td>
<td><p>高山文彦</p></td>
<td><p>川元利浩</p></td>
<td><p>1989年8月25日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

OVA每盘各收录一集。DVD第1盘收录第1～3集，第2盘收录第4～6集。

## 相关作品

### 小说

  - 机动战士GUNDAM 0080 口袋里的战争

<!-- end list -->

  -
    作者：結城恭介 / 发售日：1989年10月 /
    出版商：[角川書店](../Page/角川書店.md "wikilink")（[角川スニーカー文庫](../Page/角川スニーカー文庫.md "wikilink")）

<!-- end list -->

  -
    由本編的構成結城恭介著作的[小説版](../Page/小説.md "wikilink")。
    封面插图和插图由美树本晴彦负责。基本忠于原作品内容，但故事的细节有所改变。\[5\]。

### 音乐

  - 機動戦士ガンダム 0080 「ポケットの中の戦争」 Sound Sketch 1

<!-- end list -->

  - 機動戦士ガンダム 0080 「ポケットの中の戦争」 Sound Sketch 2

### 漫画

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [公式Web](https://web.archive.org/web/20130310013410/http://www.sunrise-anime.jp/sunrise-inc/works/detail.php?cid=59)SUNRISE
  - [公式HP](http://www.mxtv.co.jp/gundam_ova/)TOKYO MX

[0](../Category/宇宙世紀.md "wikilink")
[Category:1989年日本OVA動畫](../Category/1989年日本OVA動畫.md "wikilink")
[Category:角川Sneaker文庫](../Category/角川Sneaker文庫.md "wikilink")
[0](../Category/GUNDAM系列.md "wikilink")

1.  機動戰士-{GUNDAM}-0080 ～口袋裡的戰爭～ 譯：YOKO 出版：尖端 ISBN 957-10-1793-0
2.  口袋裡的補釘——《0080》設定修補 Ver. 2.0 P.243
3.  後記/1989－秋 P.239
4.  尾聲/0080－春 P.234
5.  著者は後書きにて、アニメの悲劇的な結末を踏まえて、苦悩の末に蛇足と知りつつあえてこうした、と告白している。