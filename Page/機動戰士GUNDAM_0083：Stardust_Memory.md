[Pkg_gundam0083_1.jpg](https://zh.wikipedia.org/wiki/File:Pkg_gundam0083_1.jpg "fig:Pkg_gundam0083_1.jpg")
[機動戰士0083安樂動畫1993.jpeg](https://zh.wikipedia.org/wiki/File:機動戰士0083安樂動畫1993.jpeg "fig:機動戰士0083安樂動畫1993.jpeg")動畫影帶影碟廣告，機動戰士0083粵語版。動畫雜誌《A
Club》封底，第151期（1993年10月1日）\]\] 《**機動戰士GUNDAM0083：STARDUST
MEMORY**》（，**）是[GUNDAM系列作品當中的一部](../Page/GUNDAM系列作品.md "wikilink")[OVA](../Page/OVA.md "wikilink")[动画](../Page/动画.md "wikilink")。一共13集、1991年5月份最初在[日本发行](../Page/日本.md "wikilink")、最后一集在1992年9月份发行完毕。

## 故事

### 事端

[一年战争完結後三年](../Page/一年战争.md "wikilink")，基於[地球聯邦軍重建計畫而被提議的](../Page/地球聯邦軍.md "wikilink")「[GUNDAM開發計劃](../Page/GUNDAM開發計劃.md "wikilink")」開始執行，其中兩台原型機[RX-78GP01與](../Page/GUNDAM開發計劃#RX-78GP01_"Zephyranthes".md "wikilink")[RX-78GP02A為了進行性能測試而搬運至](../Page/GUNDAM開發計劃#RX-78GP02A_"Physalis".md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")"[-{zh-hans:托灵顿;
zh-hant:特林頓;}-](../Page/托灵顿_\(新南威尔士州\).md "wikilink")"基地。[吉翁殘黨查知此項計畫](../Page/吉翁公國.md "wikilink")，於是襲擊基地企圖奪取裝載有[核彈的GP](../Page/核弹.md "wikilink")02A。曾經被稱為「[所羅門的惡夢](../Page/宇宙殖民地_\(GUNDAM世界\)#所羅門.md "wikilink")」的吉翁軍王牌-{zh-hans:机师;
zh-hant:駕駛員;}-[阿納貝爾·卡多](../Page/阿納貝爾·卡多.md "wikilink")[少校成功的搭上了GP](../Page/少校.md "wikilink")02A，但是在企圖脫離的GP02A面前，新手測試駕駛員[浦木宏駕駛了GP](../Page/浦木宏.md "wikilink")01攔阻，揭開了[宇宙世紀](../Page/宇宙世紀.md "wikilink")0083年[迪拉茲紛爭的序幕](../Page/迪拉茲紛爭.md "wikilink")。

### 發展

奪取GP02的[阿納貝爾·卡多前往](../Page/阿納貝爾·卡多.md "wikilink")[非洲金巴萊德礦山基地](../Page/非洲.md "wikilink")，在當地殘黨軍領袖[-{zh-hans:诺渊·比达;
zh-hant:諾恩·比達;}-少將的協助下成功以](../Page/諾巖·彼達.md "wikilink")[HLV脫離](../Page/HLV.md "wikilink")[地球](../Page/地球.md "wikilink")，回到暗礁空域，由[埃丘·迪拉茲](../Page/埃丘·迪拉茲.md "wikilink")[中將所領導的吉翁殘黨](../Page/中將.md "wikilink")（[迪拉茲艦隊](../Page/迪拉茲艦隊.md "wikilink")）基地。並糾集地球圈吉翁軍殘存勢力向地球聯邦宣戰，作戰代號為「星塵」。其後[阿納貝爾·卡多以GP](../Page/阿納貝爾·卡多.md "wikilink")02A襲擊聯邦軍於[金米島](../Page/金米島.md "wikilink")（舊吉翁軍所羅門要塞）宙域舉行的閱艦典禮，並以[核彈摧毀了](../Page/核彈.md "wikilink")2/3的聯邦艦隊，戰鬥中GP01Fb與GP02同歸於盡。同時[西瑪·卡拉豪中校所率領的海軍陸戰部隊](../Page/西瑪·卡拉豪.md "wikilink")，劫持兩座[太空殖民地](../Page/太空殖民地.md "wikilink")，並使其中之一向[月球落下](../Page/月球.md "wikilink")，而後藉由月面都市[馮·布朗的設施令其再改變軌道朝向地球而去](../Page/宇宙殖民地_\(GUNDAM世界\)#馮·布朗\(Von_Braun\).md "wikilink")。

正當迪拉茲艦隊為使殖民地順利落下，準備與聯邦軍軌道機動艦隊決戰時，西瑪在联邦军的许诺下叛變並殺害迪拉茲(實為迪拉茲被西瑪脅持當人質時自盡)。聯邦軍方的加米托夫派也試圖以[SOLAR
SYSTEM
II系統烧毁殖民地](../Page/SOLAR_SYSTEM_II.md "wikilink")。加米托夫派试图把自身塑造成一个最后拯救地球的英雄形象，待吉翁宣佈投降後，打擊聯邦內部崛起的高文派成員。卡多則趁着高文派对已投向加米托夫派的西玛舰队攻击所造成的阵线混乱，率領殘餘部隊突破聯邦軍所布置的[SOLAR
SYSTEM
II防线并成功摧毁天幕II系统控制舰](../Page/SOLAR_SYSTEM_II.md "wikilink")。在UC0083年11月13日凌晨00:34分、殖民地最终撞入地球、但是没有像聯邦軍预期的那样撞到總部-{zh-hans:加布罗;zh-hk:賈布羅;zh-tw:賈布羅;}-，而是落在[北美洲平原穀倉地帶](../Page/北美洲.md "wikilink")。殖民地下落後加米托夫派则改变计划利用新的恐慌建立了[-{zh-hans:泰坦斯;zh-hk:泰坦斯;zh-tw:迪坦斯;}-](../Page/迪坦斯.md "wikilink")，控制了軍隊。

### 结果

  - 宇宙世紀0083年12月4日、地球聯邦軍內激進派高階將領成立特殊武裝部隊[-{zh-hans:T.I.T.A.N.S.;zh-hk:泰坦斯;zh-tw:迪坦斯;}-](../Page/迪坦斯.md "wikilink")，为后續的[機動戰士Z
    GUNDAM构成舞台](../Page/機動戰士Z_GUNDAM.md "wikilink")。
  - 阿鲁比昂被指派新任舰长、[艾帕·席那普斯舰长因违抗上级命令](../Page/艾帕·席那普斯.md "wikilink")、擅自将战舰私有化与攻击友军（西玛舰队）被枪毙。
  - 阿鲁比昂号上人员，除浦木宏、查克·奇斯、摩拉·巴席特外全員收編入[-{zh-hans:泰坦斯;zh-hk:泰坦斯;zh-tw:迪坦斯;}-](../Page/迪坦斯.md "wikilink")，并被配属在亚历山大级重巡洋舰AL-GIZEH号上。
  - [亞納海姆電子冯](../Page/亞納海姆電子.md "wikilink")·布朗基地的负责人歐薩里班董事因为提供AGX-04(RX78-GP04)给西玛以及轉變殖民地動向，害怕被聯邦政府追究而自殺。（一说为[-{zh-hans:泰坦斯;zh-hk:泰坦斯;zh-tw:迪坦斯;}-所暗杀](../Page/迪坦斯.md "wikilink")）
  - 浦木宏因「擅自使用最高機密的GP系列」罪名，被军事法庭判处一年有期徒刑，不久之後RX78-GP系列开发计划被軍方抹消，浦木宏的罪状也因而撤销，撤銷罪狀後浦木宏與奇斯調往北美基地服役。

## 登場人物

## 登場機體

### 地球联邦军

**[機動兵器](../Page/機動兵器_\(GUNDAM\).md "wikilink")**

  - [GUNDAM RX-78GP01
    -{zh-hans:玉兰;zh-hk:傑菲蘭沙斯;}-](../Page/GUNDAM開發計劃#RX-78GP01_傑菲蘭沙斯_"Zephyranthes".md "wikilink")
      - [GUNDAM RX-78GP01Fb
        -{zh-hans:玉兰;zh-hk:全方位推進型;}-](../Page/GUNDAM開發計劃#RX-78GP01-Fb_全方位推進型_"Full_Burnern".md "wikilink")
  - [GUNDAM RX-78GP03D
    -{zh-hans:石斛兰;zh-hk:典多洛比姆;}-](../Page/GUNDAM開發計劃#RX-78GP03_典多洛比姆_"Dendrobium".md "wikilink")
      - [GUNDAM RX-78GP03S
        -{zh-hans:雄蕊;zh-hk:史迪蒙;}-](../Page/GUNDAM開發計劃#RX-78GP03S_典多洛比姆・史迪蒙_"Dendrobium_Stamen".md "wikilink")
      - [GUNDAM RX-78GP03O
        -{zh-hans:兰花;zh-hk:歐契斯;}-](../Page/GUNDAM開發計劃#RX-78GP03S_典多洛比姆・史迪蒙_"Dendrobium_Stamen".md "wikilink")
  - [RGM-79系列机动战士](../Page/RGM-79系列机动战士.md "wikilink")
      - RGM-79C 吉姆改
      - RGM-79 吉姆強化型
      - RGM-79N 吉姆特裝型
      - RGC-83 吉姆加農型Ⅱ
      - RGM-79Q 吉姆镇暴型
  - [RB-79C
    -{zh:Ball;zh-hans:铁球;zh-hk:鐵球;zh-tw:鋼球;}-修改型](../Page/RB-79#RB-79/RB-79C.md "wikilink")
  - MS-06F2 薩克II F2型

**戰艦**

  - LMSD-78/MSC-07 阿爾比昂
  - 麥哲倫級改
  - 薩拉米斯級改

**運輸機**

  - C-88 Medea

### 迪拉茲艦隊

**機動兵器**

  - [GUNDAM RX-78GP02A
    -{zh-hans:酸漿;zh-hk:賽薩里斯;}-](../Page/GUNDAM開發計劃#RX-78GP02A_賽薩里斯_"Physalis".md "wikilink")
  - [AGX-04
    卡貝拉](../Page/GUNDAM開發計劃#RX-78GP04G_加貝拉_"Gerbera".md "wikilink")
  - [MS-06系列机动战士](../Page/MS-06系列机动战士.md "wikilink")
      - MS-06F2 薩克II F2型
  - [MS-09系列机动战士](../Page/MS-09系列机动战士.md "wikilink")
      - MS-09F 德姆F型
      - MS-09F/trop 德姆熱帶型
      - MS-09RII 力克德姆II
  - [MS-14系列機動戰士](../Page/MS-14系列機動戰士.md "wikilink")
      - MS-14F 蓋古克海軍陸戰型
      - MS-14Fs 蓋古克海軍陸戰指揮官專用型
  - MS-21C 特拉傑
  - YMS-16 札梅爾
  - AMX-002 (AMA-X2) 諾耶·吉爾

**戰艦**

  - 格瓦金級 (格瓦典)
  - U-801 尤金級(翻新型)
  - 姆塞級改
  - 桑吉巴爾 II 級(莉莉馬蓮)

**運輸艦**

  - Papua級
  - Pazock級

## 作品解說

本作品如同其標題"0083"主要是敘述[機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（0079）到[機動戰士Z
GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（0087）之間所發生的故事，為歷史的空白作了填補。0083本身在影像質量上就是非常高的作品，由於在系列發售中途就決定藉由再編集的方式製作[劇場版](../Page/劇場版.md "wikilink")，所以系列後半的作畫質量又更加提升。

在作品當中以個人觀點來看待這場戰爭的[妮娜·帕布爾頓與憎恨所謂](../Page/妮娜·帕布爾頓.md "wikilink")「吉翁大義」的西瑪·卡拉豪等人，對於身為「敵人」角色的迪拉茲陣營多所批判。但是在0083的愛好者當中認同迪拉茲思想的比例卻佔了壓倒性的多數。一般認為這是對於迪拉茲本身人格的高潔以及卡多對於其敵手浦木宏的應對博得了純粹的好感所致。其後所製作的劇場版「吉翁的殘光」更刪去了大部分與妮娜有關的戲份，使得整部作品看來更像是迪拉茲艦隊的讚美詩。

聯邦軍隊內部的派系倾轧在整個星塵作戰中起到了巨大的作用，首先在明知對方控制核武的情況下，仍然執行艦隊檢閱，以誘惑迪拉茲艦隊決戰。之後在有可能阻止殖民地落入地球的情況下，由于阿鲁比翁号舰长派系不服从加米托夫派巴斯古的指挥造成防守力不足以及浦木宏等人执意对友军西玛舰队的攻击造成的混乱，给了卡多的特攻的机会。最后卡多成功击毁「天幕II」系统控制舰，使「天幕II」未能完全消灭殖民地以阻止其坠落地球，失去了最后挽回的机会。但同时也藉此打压了联邦军内部的高文派，巩固了其在联邦军内部的地位。

同時商人與投機主義者的戰爭時行爲也值得深思。商人[亚纳海姆电子冯](../Page/亚纳海姆电子.md "wikilink")·布朗基地的负责人，在戰爭中微妙的保持雙方的力量平衡以從中牟利。投機主義者[西瑪·卡拉豪在戰爭中完全處於自己的立場考慮](../Page/西瑪·卡拉豪.md "wikilink")，兩面討好以謀求對自己有利的局面。

本來0083的製作計畫可說是從[Gundam
Sentinel的動畫化企畫所開始](../Page/Gundam_Sentinel.md "wikilink")，所以實際的角色，設定資料，與故事走向與GUNDAM
Sentinel有許多類似之處。經過GUNDAM
Sentinel與0083的洗禮，一部份著重軍武設定與描寫的「軍事派」鋼彈迷抬頭，並且經常以嚴厲的角度來檢視後續GUNDAM作品的寫實性。

## 相關作品

### 劇場版

（吉翁的殘光）於1992年上映。

### 广播剧

  - （倫卡洋-{面}-砲擊戰，1992年）

      -
        描述第七話到第八話之間阿爾比昂戰隊在暗礁空域與迪拉茲艦隊偶發的一場遭遇戰。該戰鬥中由於諸種不利因素無法使用MS作戰，而演變為艦砲互射的砲擊戰。以阿爾比昂砲術長休茲上尉為主角，描述MS駕駛員之外的宇宙軍人心態。

  - （宇宙的蜉蝣，1992年）

      -
        描述西瑪與部下們在一年戰爭期間，奉命以毒瓦斯攻擊殖民地，其後又被吉翁軍高官當作推卸責任的棄卒的經過；以及0083本篇的劇情交替呈現。為西瑪在0083本篇中為何與聯邦軍串通並殺害迪拉茲的背景作了註解。

  - （宇宙的蜉蝣 2，2016年）

      -
        2016年发行的Blu-Ray Box中收录。采用原有旧画面（还包含《默示录 0079》中的画面）+新画面搭配的形式。

### 漫畫版

  - 《機動戰士鋼彈0083 REBELLION》漫畫：[夏元雅人](../Page/夏元雅人.md "wikilink")

<!-- end list -->

  -
    將於2013年8月號的《GUNDAM
    ACE》月刊開始連載（中文版單行本：[台灣角川](../Page/台灣角川.md "wikilink")）。

## 主要工作人員

  - 原作：[矢立肇](../Page/矢立肇.md "wikilink")‧[富野由悠季](../Page/富野由悠季.md "wikilink")
  - 導演：[加瀬充子](../Page/加瀬充子.md "wikilink")（1-7）‧[今西隆志](../Page/今西隆志.md "wikilink")（8-13）
  - 編劇：五武冬史‧遠藤明範‧大熊朝秀（今西隆志）‧[高橋良輔](../Page/高橋良輔.md "wikilink")
  - 音樂：萩田光男
  - [人物設定](../Page/人物設定.md "wikilink")‧總作畫監督：[川元利浩](../Page/川元利浩.md "wikilink")
  - [機械設定](../Page/機械設定.md "wikilink")：[河森正治](../Page/河森正治.md "wikilink")‧‧[明貴美加](../Page/明貴美加.md "wikilink")‧[石津泰志](../Page/石津泰志.md "wikilink")
  - 人物作畫監督：川元利浩‧逢坂浩司‧菅野宏紀
  - 總機械作監：佐野浩敏
  - 美術監督：東潤一
  - 製作：、[Sunrise](../Page/Sunrise.md "wikilink")

## 各集名稱

1.  （高達強奪）

2.  （無止盡的追擊）

3.  （亞爾比翁出擊）

4.  （熱砂的攻防戰）

5.  （高達，飛向星海）

6.  （馮・布朗的戰士）

7.  （蒼藍光輝的火焰）

8.  （策謀的宙域）

9.  （所羅門的惡夢）

10. （激突戰域）

11. （玫瑰人生號）

12. （強襲、阻止臨界點）

13. （奔馳而過的風暴）

## 音乐

### 片頭曲

1.  The Winner - **
2.  MEN OF DESTINY - *[MIO](../Page/MIQ.md "wikilink")*

### 片尾曲

1.  Magic - *Jacob Wheeler*
2.  Evergreen - *[MIO](../Page/MIQ.md "wikilink")*
3.  True Shining - **

### 插曲

1.  BACK TO PARADISE

<!-- end list -->

  -
    《The Winner》的英文版。

## 相关条目

  - [機動戰士GUNDAM0083 專門用語一覽](../Page/機動戰士GUNDAM0083_專門用語一覽.md "wikilink")
  - [GUNDAM開發計劃](../Page/GUNDAM開發計劃.md "wikilink")

## 外部連結

  - [GUNDAM Perfect Web（日本BANDAI官方網站）](http://www.gundam.channel.or.jp/)

[0083](../Category/宇宙世紀.md "wikilink")
[Category:1991年日本OVA動畫](../Category/1991年日本OVA動畫.md "wikilink")
[Category:1992年日本電視動畫](../Category/1992年日本電視動畫.md "wikilink")
[Category:日昇動畫](../Category/日昇動畫.md "wikilink")
[Category:澳大利亞背景作品](../Category/澳大利亞背景作品.md "wikilink")
[Category:核武器題材作品](../Category/核武器題材作品.md "wikilink")
[Category:角川Sneaker文庫](../Category/角川Sneaker文庫.md "wikilink")
[0083](../Category/GUNDAM系列.md "wikilink")