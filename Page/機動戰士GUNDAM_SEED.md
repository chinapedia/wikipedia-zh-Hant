《**機動戰士GUNDAM
SEED**》（，）為[日本的](../Page/日本.md "wikilink")[電視](../Page/電視.md "wikilink")[動畫作品](../Page/動畫.md "wikilink")，屬於[機動戰士GUNDAM動畫系列之一的機械人動畫](../Page/GUNDAM系列作品.md "wikilink")，簡稱『SEED』。[每日放送](../Page/每日放送.md "wikilink")（MBS）投資，自2002年10月5日至2003年9月27日由[TBS系聯播全](../Page/東京廣播公司.md "wikilink")50話。

本作是由[日昇動畫第九工作室以](../Page/日昇動畫.md "wikilink")「面向新世代，可以成為新標準的鋼彈」、「新世紀（21世紀）的初代鋼彈」、「回歸原點」為目標所製作，是鋼彈系列首部以數位化製作VTR影片的電視動畫。

續篇《[機動戰士GUNDAM SEED
DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")》於2004年播映，全50話，兩部電視動畫作品構成一個[C.E.紀元系列](../Page/宇宙纪元.md "wikilink")。

## 故事簡介

[宇宙纪元](../Page/宇宙纪元.md "wikilink")（C.E.）15年，世界知名的天才青年科學家喬治·葛倫在出發前往木星進行科學考察途中，公開宣布自己是經由[基因改造誕生的](../Page/基因改造.md "wikilink")[新人類](../Page/機動戰士鋼彈SEED系列世界觀與設定#術語.md "wikilink")，成為史上第一位公開的[調整者](../Page/機動戰士鋼彈SEED系列世界觀與設定#術語.md "wikilink")（Coordinator）。在公開自己的秘密同時，他也同時把改造人類基因的方法公諸於世，使世界上調整者的人口大幅增加。

與此同時，雖然各國開始立法規管人類基因改造，但仍未能阻止人們對改造自己後代基因，使後代能力更優秀的追求。在調整者的人口不斷增加的同時，卻使一部分沒有進行基因工程改造的普通人類，也是所謂的『自然人（Natural）』開始對調整者產生嫉妬和仇恨。在不斷被自然人排斥和威脅的情況下，調整者逼不得已離開地球，在C.E.44年於[L5宙域的殖民衛星群成立了宇宙殖民地](../Page/拉格朗日点#L5.md "wikilink")[P.L.A.N.T.](../Page/機動戰士鋼彈SEED系列世界觀與設定#P.L.A.N.T.md "wikilink")，並同時組織了政治及軍事自治組織―自由条约黄道联盟（簡稱[Z.A.F.T](../Page/機動戰士鋼彈SEED系列世界觀與設定#ZAFT.md "wikilink")，扎夫特）。之後隨著喬治·葛倫在C.E.53年被自然人谋杀，西蓋爾·克萊因派在P.L.A.N.T.最高評議會逐漸崛起等因素影響，地球與宇宙殖民地關係漸漸轉差。在此期間，本作主角[基拉·大和於C](../Page/基拉·大和.md "wikilink").E.55年誕生（作為悠連·響博士的超級調整者研發計畫的唯一完成品）。

C.E.70年2月5日，在月面都市哥白尼舉行的殖民地與理事國會談會場受到炸彈恐怖袭击（哥白尼惨剧），地球方面理事國代表與[聯合國秘書長以下的聯合國首腦群死亡](../Page/聯合國.md "wikilink")。隨後於2月7日，理事國之一的[大西洋聯邦發表](../Page/機動戰士鋼彈SEED系列世界觀與設定#大西洋聯邦.md "wikilink")《阿拉斯加宣言》，宣称此次恐怖襲擊是P.L.A.N.T.所为，並指襲擊是對地球和自然人的宣戰。大西洋聯邦並宣布成立[地球聯邦](../Page/機動戰士鋼彈SEED系列世界觀與設定#地球聯合.md "wikilink")，取代因首腦全數死亡而癱瘓的聯合國。2月11日，地球聯合軍對P.L.A.N.T.宣戰。從月面的托勒密基地開始侵略攻擊。此外，一名隸屬於激進反調整者組織―[藍色宇宙的軍官下令將一枚核彈秘密地運進MA空母](../Page/機動戰士鋼彈SEED系列世界觀與設定#藍色宇宙.md "wikilink")“羅斯福”號內。雖然P.L.A.N.T.出動了MS部隊將敵軍殲滅，但由於受到突然的核彈攻擊，120座P.L.A.N.T.宇宙殖民地中的一座，農業衛星「尤尼烏斯-7」遭到毀滅性的破壞，造成了24萬3721人死亡，史稱「[血色情人節](../Page/機動戰士鋼彈SEED系列世界觀與設定#血色情人節.md "wikilink")」,也是本作戰爭的起點。

由於「血色情人節」的慘劇，[地球方面與P](../Page/地球.md "wikilink").L.A.N.T.之間的紛爭一口氣升級為正式的武裝衝突。開戰十一個月後，主人翁[煌·大和](../Page/煌·大和.md "wikilink")（Kira
Yamato）所在的中立國[歐普所屬資源殖民衛星](../Page/機動戰士鋼彈SEED系列世界觀與設定#O.R.B..md "wikilink")「海利歐波里斯（Heliopolis）」突遭兒時好友[阿斯蘭·薩拉](../Page/阿斯蘭·薩拉.md "wikilink")（Athrun
Zala）所屬的Z.A.F.T.襲擊，他們的重逢成為故事的開端。

P.L.A.N.T.在血色情人節後發動獨立戰爭，其軍事組織Z.A.F.T.的戰略為散布大量「[中子干擾器](../Page/中子干擾器.md "wikilink")」至地底下，使地球上多數地區無法使用核能，引發世界性的能源危機。再以新型兵器逐步攻下在[赤道附近的](../Page/赤道.md "wikilink")[質量投射器](../Page/質量投射器.md "wikilink")，以期將[地球聯合軍封鎖在](../Page/地球聯合軍.md "wikilink")[地球圈內](../Page/地球.md "wikilink")，再也無法威脅P.L.A.N.T.；由於貌似神話中自食其尾的大蛇，故命名為「Operation
Ouroboros」作戰。大戰爆發初期，誰也未曾懷疑過地球軍將以壓倒性的兵力和資源迅速結束戰爭。然而戰爭膠著了近一年，戰況卻是對聯合軍愈發不利，其主要原因之一就是地球軍對[Z.A.F.T.的新型兵器](../Page/Z.A.F.T..md "wikilink")―[機動戰士](../Page/機動戰士.md "wikilink")（MS）一籌莫展。

為了挽救戰局，地球聯合中最強大的成員國―大西洋聯邦早已開發代號「G兵器」的機動戰士，並秘密地委託奧布的國營軍需企業「[曙光社](../Page/曙光社.md "wikilink")」在[海利歐波里斯協助建造](../Page/海利歐波里斯.md "wikilink")。[Z.A.F.T.的克魯澤隊掌握了此情報](../Page/Z.A.F.T..md "wikilink")，派遣特殊部隊襲擊[海利歐波里斯](../Page/海利歐波里斯.md "wikilink")，強奪了正進行出廠作業的「[G兵器](../Page/G兵器.md "wikilink")」五機中之四機。意外被捲入這場戰鬥的工業大學高材生[基拉·大和竟遭遇已成為](../Page/基拉·大和.md "wikilink")[Z.A.F.T.菁英的兒時好友](../Page/Z.A.F.T..md "wikilink")[阿斯蘭·薩拉](../Page/阿斯蘭·薩拉.md "wikilink")。在一片混亂的街道上，煌意外地乘上了碩果僅存的GAT-X105
Strike。注視著座艙顯示器上的[G兵器系統的全寫名稱](../Page/G兵器.md "wikilink")，[基拉不自覺地脫口而出](../Page/基拉.md "wikilink")：「GUN…DAM…！」。

## 何謂SEED

所謂SEED，全寫為Superior Evolutionary Element
Destined-factor，即「高度進化要素遺傳因子」。在[C.E.紀元的世界裡](../Page/C.E..md "wikilink")，是一種尚未被證實的[遺傳學理論](../Page/遺傳學.md "wikilink")，只有極少數人類（不分[調整者或](../Page/調整者.md "wikilink")[自然人](../Page/自然人.md "wikilink")）基因潛藏的一種天賦。SEED持有者在面臨某些條件，例如極度的憤怒、悲傷，或是面臨死亡威脅下，突發地進入被稱為「覺醒」的狀態中。本作的演出是SEED持有者腦中有一顆代表自己的種子狀物體爆裂，故台灣與中國俗稱「爆種」，香港地區則稱「爆SEED」。此一狀態下的瞳孔會收縮變色（顏色依人不同），其精神力、運動能力、反射速度等各項指標發生飛躍性的提升，直覺亦變得異於常人的敏銳，似乎能感知到遙遠距離外發生的事。不過，本作未清楚交代「覺醒」究竟帶來甚麼影響，對於[MS駕駛員而言](../Page/MS.md "wikilink")，「覺醒」會直接反應在其戰鬥能力上；但對於非戰鬥人員來說，是否會有對智能、判斷力或其他才能的影響則不得而知。總之，「覺醒」是「將自身潛力發揮出來」的象徵。

本作[小說版載明](../Page/小說.md "wikilink")「覺醒」係「精神力提升到一個全新的境界」。在續作（[機動戰士GUNDAM
SEED
DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")）中，[吉伯特·杜蘭朵曾說](../Page/吉伯特·杜蘭朵.md "wikilink")：「持有SEED意味著擁有進化的可能性」。本作科技設定森田繁曾說：「SEED是『想要活下去的力量』」。

### SEED持有者

  - [煌·大和](../Page/煌·大和.md "wikilink")
  - [阿斯蘭·薩拉](../Page/阿斯蘭·薩拉.md "wikilink")
  - [拉克絲·克萊因](../Page/拉克絲·克萊因.md "wikilink")
  - [卡佳里·由拉·阿斯哈](../Page/卡佳里·由拉·阿斯哈.md "wikilink")
  - [真·飛鳥](../Page/真·飛鳥.md "wikilink")（SEED DESTINY）

## 登場角色

## 登場機體

## 登场舰船及其他兵器

## 特別版三部作

「機動戰士GUNDAM SEED SPECIAL
EDITION」將全50話的電視版原作再剪輯並追加局部新作畫面，構成類似精華版的全三部OVA總集編，包括了特別版I《虛空的戰場》（1－21），特別版II《遙遠的拂曉》（22－40）和特別版III《鳴動的宇宙》（41－50），DVD分別於2004年8月27日、9月24日以及10月22日發售。

除了以[電影手法剪輯](../Page/電影.md "wikilink")，大幅縮短故事長度之外，畫面由4:3改為16:9，亦有些許場景與原作全然迥異的，例如[侵略鋼彈本來是在和](../Page/GAT-X370_Raider.md "wikilink")[決鬥鋼彈相互砲擊中被擊破](../Page/GAT-X102.md "wikilink")，在特別版中改為被[暴風鋼彈擊破](../Page/GAT-X103_Buster.md "wikilink")；穆操纵的[攻擊鋼彈在被主天使号击毁后](../Page/GAT-X105_Strike.md "wikilink")，本来穆的头盔作为残骸出现在画面中，在特别版中穆的头盔没有出现，暗示穆仍生还。

| 話數     | 日文標題  | 中譯    | 香港版官方標題 |
| ------ | ----- | ----- | ------- |
| 特別版I   | 虚空の戦場 | 虛空的戰場 |         |
| 特別版II  | 遥かなる暁 | 遙遠的拂曉 |         |
| 特別版III | 鳴動の宇宙 | 鳴動的宇宙 |         |

## HD Remaster

2012年為迎接機動戰士鋼彈SEED十周年，官方於2011年8月26日啟動「HD Remaster
Project（高畫質再製企劃）」，畫面尺寸比將從原先的4:3升級為16:9寬螢幕，[線性PCM音源化](../Page/脈衝編碼調制.md "wikilink")，部分音樂採用混音版，局部畫面重畫、替換和修正，各特效與背景的處理變更，並整合特別版三部作以及「DESTINY」的[賽璐珞片](../Page/賽璐珞.md "wikilink")，再編輯為全48話，收錄至Blu-ray共四卷BOX販售。相較於過去其他GUNDAM作品的Remaster僅是單純的高畫質化，本作有著更全面的提升。「機動戰士鋼彈SEED
HD」於2011年12月23日起在BandaiChannel針對日本國內進行限定放送\[1\]，TV方面則於2012年1月1日起在日本BS11播映。gundam.info同時提供附有簡體中文\[2\]（已不再提供）、繁體中文\[3\]\[4\]、英文\[5\]和韓文\[6\]字幕影片的網路配信。[爱奇艺将本作定名为](../Page/爱奇艺.md "wikilink")“機動戰士-{}-高-{}-達-{}-SEED
HD Remaster”于2011年12月23日11時起播出\[7\]。Blu-ray版會在HD
Remaster電視版方面再進行作畫修正，還加入了部分全新新規作畫場面。

## 書籍

### 漫畫版

《機動戰士鋼彈SEED
Re:》，原作也是[矢立肇](../Page/矢立肇.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")，漫畫為[石口十](../Page/石口十.md "wikilink")，中文版由[台灣角川書店出版](../Page/台灣角川書店.md "wikilink")，只出3冊不明原因腰斬。（第三冊最後有第四冊的劇情預告）

### 小說版

全5卷。全套小說均由[角川書店出版](../Page/角川書店.md "wikilink")，原作也是[矢立肇](../Page/矢立肇.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")，著者為[後藤リウ](../Page/後藤リウ.md "wikilink")。

中文版則由[台灣角川書店出版](../Page/台灣角川書店.md "wikilink")，[章澤儀翻譯](../Page/章澤儀.md "wikilink")。香港由[一代匯集代理](../Page/一代匯集.md "wikilink")，並未在地化翻譯，因此香港購買的小說標題也是稱為「-{鋼彈}-」而非「-{高達}-」。

1.  《機動戰士-{鋼彈}-SEED〈1〉交會而過的羽翼》（），ISBN 9867427270
2.  《機動戰士-{鋼彈}-SEED〈2〉沙漠之虎》（），ISBN 986742753X
3.  《機動戰士-{鋼彈}-SEED〈3〉和平之國》（），ISBN 9867427858
4.  《機動戰士-{鋼彈}-SEED〈4〉飛舞而降之劍》（），ISBN 9867299183
5.  《機動戰士-{鋼彈}-SEED〈5〉飛向無止盡的明天》（），ISBN 9867299590

## 各話標題

<table>
<thead>
<tr class="header">
<th><p>話數[8]</p></th>
<th><p>日文標題[9]</p></th>
<th><p>台譯標題</p></th>
<th><p>香港版官方標題</p></th>
<th><p>簡體版官方標題</p></th>
<th><p>劇本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>虛偽的和平</p></td>
<td><p>虛偽的和平</p></td>
<td><p>虚伪的和平</p></td>
<td><p><a href="../Page/兩澤千晶.md" title="wikilink">兩澤千晶</a></p></td>
<td><p><a href="../Page/福田己津央.md" title="wikilink">福田己津央</a></p></td>
<td><p><a href="../Page/吉本毅.md" title="wikilink">吉本毅</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>其名為鋼-{}-彈</p></td>
<td><p>你的名字是「高-{}-達」</p></td>
<td><p>你的名字是敢达</p></td>
<td><p><a href="../Page/南康宏.md" title="wikilink">南康宏</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>崩壞的大地</p></td>
<td><p>崩壞的大地</p></td>
<td><p>崩溃的大地</p></td>
<td><p><a href="../Page/吉野弘幸.md" title="wikilink">吉野弘幸</a></p></td>
<td><p><a href="../Page/菊池一仁_(動畫導演).md" title="wikilink">菊池一仁</a></p></td>
<td><p><a href="../Page/高田耕一.md" title="wikilink">高田耕一</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>靜音潛航</p></td>
<td><p>靜默的狂奔</p></td>
<td><p>潜行</p></td>
<td><p><a href="../Page/遠藤明範.md" title="wikilink">遠藤明範</a></p></td>
<td><p><a href="../Page/菱川直樹.md" title="wikilink">菱川直樹</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>相轉移裝甲失效</p></td>
<td><p>P.S.裝甲失效</p></td>
<td><p>战火下的友谊</p></td>
<td><p><a href="../Page/こぐれ今日子.md" title="wikilink">こぐれ今日子</a><br />
兩澤千晶</p></td>
<td><p><a href="../Page/黑木冬.md" title="wikilink">黑木冬</a></p></td>
<td><p>吉本毅</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>消失的鋼-{}-彈</p></td>
<td><p>隐形的高-{}-達</p></td>
<td><p>消失的敢达</p></td>
<td><p>遠藤明範<br />
兩澤千晶</p></td>
<td><p><a href="../Page/蜂巣忠太.md" title="wikilink">蜂巣忠太</a></p></td>
<td><p>南康宏</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>宇宙的傷痕</p></td>
<td><p>宇宙的傷痕</p></td>
<td><p>宇宙的伤痕</p></td>
<td><p>吉野弘幸</p></td>
<td><p><a href="../Page/谷田部勝義.md" title="wikilink">谷田部勝義</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>敵軍的歌姬</p></td>
<td><p>敵軍的歌姬</p></td>
<td><p>敌军的歌姬</p></td>
<td><p>兩澤千晶</p></td>
<td><p>菊池一仁</p></td>
<td><p><a href="../Page/佐藤照雄.md" title="wikilink">佐藤照雄</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>消失的光明</p></td>
<td><p>逐漸消失的光明</p></td>
<td><p>消失的光</p></td>
<td><p>こぐれ今日子</p></td>
<td><p>黒木冬</p></td>
<td><p><a href="../Page/三好正人.md" title="wikilink">三好正人</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>分歧的道路</p></td>
<td><p>各自的道路</p></td>
<td><p>各自的道路</p></td>
<td><p><a href="../Page/面出明美.md" title="wikilink">面出明美</a><br />
兩澤千晶</p></td>
<td><p>南康宏<br />
福田己津央</p></td>
<td><p><a href="../Page/西山明樹彥.md" title="wikilink">西山明樹彥</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>覺醒的利刃</p></td>
<td><p>醒覺的刀刃</p></td>
<td><p>觉醒的刀刃</p></td>
<td><p><a href="../Page/大野木寬.md" title="wikilink">大野木寬</a></p></td>
<td><p><a href="../Page/高田耕一.md" title="wikilink">高田耕一</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>-{芙蕾}-的選擇</p></td>
<td><p>-{佩莉}-的決-{}-擇</p></td>
<td><p>佩莉的抉择</p></td>
<td><p>面出明美<br />
兩澤千晶</p></td>
<td><p>菱川直樹</p></td>
<td><p><a href="../Page/鳥羽聰.md" title="wikilink">鳥羽聰</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>宇宙星降</p></td>
<td><p>落在宇宙的隕星</p></td>
<td><p>落在宇宙的陨星</p></td>
<td><p>吉野弘幸</p></td>
<td><p>菊池一仁</p></td>
<td><p>佐藤照雄</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>在無盡的時光中</p></td>
<td><p>無盡頭的時光中</p></td>
<td></td>
<td><p>吉野弘幸<br />
両澤千晶</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>各自的孤獨</p></td>
<td><p>各自孤獨</p></td>
<td><p>各自的孤独</p></td>
<td><p>兩澤千晶</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>燃燒的沙塵</p></td>
<td><p>燃燒的砂塵</p></td>
<td><p>燃烧的沙尘</p></td>
<td><p><a href="../Page/森田繁.md" title="wikilink">森田繁</a></p></td>
<td><p>黒木冬</p></td>
<td><p>三好正人</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>再逢-{卡佳里}-</p></td>
<td><p>再遇卡嘉-{}-蓮</p></td>
<td><p>再遇卡嘉莲</p></td>
<td><p>面出明美<br />
兩澤千晶</p></td>
<td><p>南康宏</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>報復</p></td>
<td><p>代價!</p></td>
<td><p>代价</p></td>
<td><p>大野木寬</p></td>
<td><p><a href="../Page/山口晋.md" title="wikilink">山口晋</a></p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>宿敵的獠牙</p></td>
<td><p>宿敵之牙</p></td>
<td><p>宿敌之牙</p></td>
<td><p>吉野弘幸</p></td>
<td><p>高田耕一</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>平穩的日子</p></td>
<td><p>安穩的日子</p></td>
<td><p>在安稳的日子里</p></td>
<td><p>兩澤千晶</p></td>
<td><p>黒木冬</p></td>
<td><p><a href="../Page/關田修.md" title="wikilink">關田修</a></p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p>砂塵的盡頭</p></td>
<td><p>沙漠的盡頭</p></td>
<td><p>砂尘的尽头</p></td>
<td><p>森田繁</p></td>
<td><p>山口晋</p></td>
<td><p>三好正人</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p>染紅的大海</p></td>
<td><p>被染紅的海</p></td>
<td><p>被染红的海</p></td>
<td><p>大野木寬</p></td>
<td><p><a href="../Page/森邦宏.md" title="wikilink">森邦宏</a></p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>命運的邂逅</p></td>
<td><p>命運安排的相逢</p></td>
<td><p>命运安排的相逢</p></td>
<td><p><a href="../Page/野村祐一.md" title="wikilink">野村祐一</a><br />
兩澤千晶</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>只有兩個人的戰爭</p></td>
<td><p>只是二人的戰爭</p></td>
<td><p>二人的战争</p></td>
<td><p>こぐれ今日子<br />
兩澤千晶</p></td>
<td><p><a href="../Page/大橋譽志光.md" title="wikilink">大橋譽志光</a></p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td></td>
<td><p>通往和平之國</p></td>
<td><p>和平的國度</p></td>
<td><p>和平的国度</p></td>
<td><p>吉野弘幸</p></td>
<td><p>高田耕一<br />
福田己津央</p></td>
<td><p>高田耕一</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td></td>
<td><p>時機</p></td>
<td><p>瞬息間</p></td>
<td></td>
<td><p>森田繁<br />
両澤千晶</p></td>
<td><p><a href="../Page/秋廣泰生.md" title="wikilink">秋廣泰生</a></p></td>
<td><p>福田己津央</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td></td>
<td><p>無止盡的輪舞</p></td>
<td><p>沒終結的回旋曲</p></td>
<td><p>没终结的回旋曲</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td></td>
<td><p>-{煌}-</p></td>
<td><p>-{基拉}-</p></td>
<td><p>基拉</p></td>
<td><p>兩澤千晶</p></td>
<td><p><a href="../Page/松尾衡.md" title="wikilink">松尾衡</a></p></td>
<td><p>關田修</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td></td>
<td><p>命運之楔</p></td>
<td><p>命運的轉捩點</p></td>
<td><p>注定的楔钉</p></td>
<td><p>森田繁</p></td>
<td><p>山口晋</p></td>
<td><p>三好正人</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td></td>
<td><p>閃光的時刻</p></td>
<td><p>閃光的一刻</p></td>
<td><p>闪光的一刻</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p><a href="../Page/松園公.md" title="wikilink">松園公</a></p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td></td>
<td><p>慟哭的天空</p></td>
<td><p>慟哭的天空</p></td>
<td><p>恸哭的天空</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>松尾衡</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td></td>
<td><p>約束之地</p></td>
<td><p>承諾之地</p></td>
<td><p>约束之地</p></td>
<td><p>こぐれ今日子<br />
兩澤千晶</p></td>
<td><p><a href="../Page/西澤晋.md" title="wikilink">西澤晋</a></p></td>
<td><p>高田耕一</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td></td>
<td><p>闇的胎動</p></td>
<td><p>黑暗的胎動</p></td>
<td><p>黑暗的胎动</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>菱川直樹</p></td>
<td><p>關田修</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td></td>
<td><p>放眼所見</p></td>
<td><p>目光之先</p></td>
<td><p>目光所向</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>松尾衡</p></td>
<td><p><a href="../Page/井之川慎太郎.md" title="wikilink">井之川慎太郎</a></p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td></td>
<td><p>飛天翔舞之劍</p></td>
<td><p>舞降之劍</p></td>
<td><p>舞落之剑</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>谷田部勝義<br />
山口晋</p></td>
<td><p>谷田部勝義</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td></td>
<td><p>以正義為名</p></td>
<td><p>在正義之名下</p></td>
<td><p>在正义之名下</p></td>
<td><p><a href="../Page/米谷良知.md" title="wikilink">米谷良知</a></p></td>
<td><p>三好正人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td></td>
<td><p>神之怒</p></td>
<td><p>神雷</p></td>
<td><p>神之雷</p></td>
<td><p>大野木寛<br />
両澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>鳥羽聡</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td></td>
<td><p>決意的砲火</p></td>
<td><p>決意的砲火</p></td>
<td><p>决意的炮火</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p><a href="../Page/とくしまひさし.md" title="wikilink">とくしまひさし</a><br />
<a href="../Page/久行宏和.md" title="wikilink">久行宏和</a></p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td></td>
<td><p>阿斯-{}-蘭</p></td>
<td><p>亞斯-{}-蘭</p></td>
<td><p>亚斯-{}-兰</p></td>
<td><p>野村祐一<br />
両澤千晶</p></td>
<td><p>高田耕一<br />
松園公</p></td>
<td><p>高田耕一</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td></td>
<td><p>破曉的宇宙</p></td>
<td><p>奔向黎明的宇宙</p></td>
<td><p>前往黎明的宇宙</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ</p></td>
<td><p>關田修</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td></td>
<td><p>動亂的世界</p></td>
<td><p>動盪的世界</p></td>
<td><p>动荡的世界</p></td>
<td><p>とくしまひさし</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td></td>
<td><p>拉克-{}-絲出擊</p></td>
<td><p>-{莉古斯}-出擊</p></td>
<td><p>-{莉古斯}-出击</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>井之川慎太郎</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td></td>
<td><p>大敵當前</p></td>
<td><p>阻路的強敵</p></td>
<td><p>阻着去路的人</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>松尾衡<br />
久行宏和</p></td>
<td><p>三好正人</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td></td>
<td><p>螺旋的邂逅</p></td>
<td><p>螺旋的邂逅</p></td>
<td><p>黑螺旋的邂逅</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>とくしまひさし</p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td></td>
<td><p>開啟的門扉</p></td>
<td><p>開門</p></td>
<td><p>开启的门</p></td>
<td><p>兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td></td>
<td><p>靈魂的場所</p></td>
<td><p>靈魂所屬地方</p></td>
<td><p>灵魂的归所</p></td>
<td><p>久行宏和<br />
とくしまひさし</p></td>
<td><p>高田耕一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td></td>
<td><p>惡夢再現</p></td>
<td><p>惡夢再臨</p></td>
<td><p>恶梦再现</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>とくしまひさし</p></td>
<td><p>關田修</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td></td>
<td><p>憤怒之日</p></td>
<td><p>憤怒之日</p></td>
<td><p>愤怒之日</p></td>
<td><p>とくしまひさし<br />
谷田部勝義</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td></td>
<td><p>終末之光</p></td>
<td><p>終末之光</p></td>
<td><p>终末之光</p></td>
<td><p>西澤晋</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td></td>
<td><p>朝向永無止盡的明日</p></td>
<td><p>向永恆的明日進發</p></td>
<td><p>飞向永无止尽的明天</p></td>
<td><p>兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
福田己津央</p></td>
<td><p>三好正人</p></td>
</tr>
<tr class="odd">
<td><p>After-Phase<br />
[10]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>福田己津央</p></td>
<td><p>高田昌宏</p></td>
</tr>
</tbody>
</table>

## 主題歌

### 原版

  - 片頭曲

<!-- end list -->

1.  「[INVOKE－インヴォーク－](../Page/INVOKE.md "wikilink")」（第1話 － 第13話）
      -
        作詞：[井上秋緒](../Page/井上秋緒.md "wikilink") /
        作曲、編曲：[淺倉大介](../Page/淺倉大介.md "wikilink")
        / 主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
2.  「Moment」（第14話 － 第26話）
      -
        作詞：Vivian or Kazuma / 作曲、編曲：[土橋安騎夫](../Page/土橋安騎夫.md "wikilink")
        / 主唱：[Vivian or Kazuma](../Page/Vivian_or_Kazuma.md "wikilink")
3.  「[Believe](../Page/Believe_\(玉置成實單曲\).md "wikilink")」（第27話 － 第40話）
      -
        作詞：西尾佐栄子 / 作曲：あおい吉勇 / 編曲 ：[齋藤真也](../Page/齋藤真也.md "wikilink") /
        主唱：[玉置成實](../Page/玉置成實.md "wikilink")
4.  「[Realize](../Page/Realize_\(玉置成實單曲\).md "wikilink")」（第41話 － 第50話）
      -
        作詞：[BOUNCEBACK](../Page/BOUNCEBACK.md "wikilink") /
        作曲：[大谷靖夫](../Page/大谷靖夫.md "wikilink") /
        編曲：荒井洋明、大谷靖夫 / 主唱：玉置成實

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「」（第1話 － 第26話、虚空的战场）
      -
        作詞：[石川千亞紀](../Page/石川智晶.md "wikilink") /
        作曲、編曲：[梶浦由記](../Page/梶浦由記.md "wikilink")
        / 主唱：[See-Saw](../Page/See-Saw.md "wikilink")
2.  「RIVER」（第27話 － 第39話）
      -
        作詞、作曲：石井龍也 / 編曲：渡辺善太郎 / 主唱：[石井龍也](../Page/石井龍也.md "wikilink")
3.  「[Find the way](../Page/FIND_THE_WAY.md "wikilink")」（第40話 －
    第50話、鸣动的宇宙）
      -
        作詞：中島美嘉 / 作曲：Lori Fine (COLDFEET) /
        編曲：[島健](../Page/島健.md "wikilink") /
        主唱：[中島美嘉](../Page/中島美嘉.md "wikilink")
4.  「」（遥远的拂晓）
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：FictionJunction featuring
        YUUKA（[南里侑香](../Page/南里侑香.md "wikilink")）

<!-- end list -->

  - 插入曲

<!-- end list -->

1.  「」
      -
        作詞：梶浦由記 / 作曲、編曲：[佐橋俊彦](../Page/佐橋俊彦.md "wikilink") /
        主唱：拉克絲·克萊因（[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")）
2.  「」
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：拉克絲·克萊因（田中理惠）
3.  「」
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：FictionJunction featuring
        YUUKA（[南里侑香](../Page/南里侑香.md "wikilink")）
4.  「Meteor-ミーティア-」
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 /
        主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
5.  「Zips」
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 /
        主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")

### HD Remaster

  - 片頭曲

<!-- end list -->

1.  「INVOKE－インヴォーク－」（第1話 － 第13話）
      -
        作詞：[井上秋緒](../Page/井上秋緒.md "wikilink") /
        作曲、編曲：[淺倉大介](../Page/淺倉大介.md "wikilink")
        / 主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
2.  「Moment」（第14話 － 第24話）
      -
        作詞：Vivian or Kazuma / 作曲、編曲：[土橋安騎夫](../Page/土橋安騎夫.md "wikilink")
        / 主唱：[Vivian](../Page/徐若瑄.md "wikilink") or Kazuma
3.  「Believe」（第25話 － 第38話）
      -
        作詞：西尾佐栄子 / 作曲：あおい吉勇 / 編曲：[齋藤真也](../Page/齋藤真也.md "wikilink") /
        主唱：[玉置成實](../Page/玉置成實.md "wikilink")
4.  「Realize」（第39話 － 第48話）
      -
        作詞：BOUNCEBACK / 作曲：大谷靖夫 / 編曲：荒井洋明、大谷靖夫 / 主唱：玉置成實

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「」（第1話 － 第26話）
      -
        作詞：石川千亞紀 / 作曲、編曲：梶浦由記 /
        主唱：[See-Saw](../Page/See-Saw.md "wikilink")
2.  「Distance」（第27話 － 第37話、第39話 － 第43話、第45話 － 第47話 ）
      -
        作詞、作曲、編曲：梶浦由記 /
        主唱：[FictionJunction](../Page/FictionJunction.md "wikilink")
3.  「」（第38話）
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：FictionJunction featuring
        YUUKA（[南里侑香](../Page/南里侑香.md "wikilink")）
4.  「[Find the way](../Page/FIND_THE_WAY.md "wikilink")」（第44話、第48話）
      -
        作詞：中島美嘉 / 作曲：Lori Fine (COLDFEET) /
        編曲：[島健](../Page/島健.md "wikilink") /
        主唱：[中島美嘉](../Page/中島美嘉.md "wikilink")

<!-- end list -->

  - 插入曲

<!-- end list -->

1.  「」
      -
        作詞、編曲：梶浦由記 / 作曲：佐橋俊彦 / 主唱：拉克絲·克萊因（田中理惠）
2.  「」
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：拉克絲·克萊因（田中理惠）
3.  「」
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：FictionJunction featuring
        YUUKA（[南里侑香](../Page/南里侑香.md "wikilink")）
4.  「Meteor-ミーティア-」
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 /
        主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
5.  「Zips」
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 /
        主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")

## 成就與評價

本作作为「21世紀的初代GUNDAM」，播放期間在日本國內取得最低4.6%、最高8%、平均6.2%的高收視率（高達系列動畫史上第二位，僅次於[鋼彈Z系列](../Page/機動戰士Z_GUNDAM.md "wikilink")）\[11\]，無論在日本國內還是海外都擁有巨大人氣。同時，本作在商業上的表現也相當成功，其音樂作品及周邊產品（如DVD，[GUNPLA模型等](../Page/GUNPLA.md "wikilink")）在日本亦獲得了十分驚人的銷售量，並於2004年東京國際動畫博覽會上榮獲「年度最佳動畫賞」、「電視節目部門優秀作品賞」等殊榮。

本作繼承[GUNDAMW的偶像路线](../Page/新機動戰記GUNDAM_W.md "wikilink")，角色設計美形亮麗，吸引為數不少的女性觀眾\[12\]，亦於2004年東京國際動畫博覽會獲得「角色設計賞」。此外在戰鬥場景的表現上，機械作畫監督[大張正己善於讓機械人做出像人一樣的動作](../Page/大張正己.md "wikilink")，被稱為「變形機械」的手法，並刻意增加白刃戰的比重，使戰鬥畫面更添張力。本作針對戰鬥的嶄新表現方式，也為日後[機動戰士GUNDAM
00](../Page/機動戰士GUNDAM_00.md "wikilink")、[機動戰士GUNDAM
AGE和](../Page/機動戰士GUNDAM_AGE.md "wikilink")[機動戰士GUNDAM
鐵血的孤兒相繼沿用](../Page/機動戰士GUNDAM_鐵血的孤兒.md "wikilink")。另外本作機體設計採取符合現代審美觀的九頭身修長身形，也影響了後來作品的機體設計，为GUNDAM系列带来一大群新的年輕爱好者。甚至有外國動漫評論家認為，本作是鋼彈系列動畫史上最好的作品之一\[13\]\[14\]。

本作在放送結束後相當一段時間內仍舊維持高人氣，至2013年為止，本作的DVD已經賣出超過100萬張\[15\],而相關的[GUNPLA模型也賣出超過](../Page/GUNPLA.md "wikilink")1000萬個以上\[16\]。商業上的成功，也使本作成為繼U.C系列後，第二个拥有MSV的系列，其相關產品線也是鋼彈系列繼U.C紀元後的第二大產品線。

實際上，在[機動戰士V
GUNDAM結束後](../Page/機動戰士V_GUNDAM.md "wikilink")，由於人氣不振，使鋼彈系列進入長達十年的摸索期。其間有「平成三部曲」及[機動戰士TURN
A
GUNDAM四部電視動畫](../Page/機動戰士TURN_A_GUNDAM.md "wikilink")，然而未有一部作品的人氣足以形成如U.C.紀元般具有延續性的系列作，使得周邊產品銷量與U.C.鼎盛時期相較大幅下降。而本作周邊產品銷售的成功，超出了製造商BANDAI（[萬代](../Page/萬代.md "wikilink")）的預期，也使該公司重新重視鋼彈系列，故有評論指出SEED系列可謂鋼彈系列東山再起的關鍵
\[17\]。萬代與日昇在本作動畫結束後多年，仍然持續推出不同的周邊產品，如日昇在本作十周年時將整部動畫高清化再製，而萬代也不斷的推出SEED系列的新版模型。

值得一提的是本作留下不少大獲好評的歌曲，许多知名流行歌手助陣，如[西川贵教](../Page/西川贵教.md "wikilink")、[徐若瑄](../Page/徐若瑄.md "wikilink")、[See-Saw和](../Page/See-Saw.md "wikilink")[中岛美嘉等](../Page/中岛美嘉.md "wikilink")，甚至還捧紅了新人歌手[玉置成實](../Page/玉置成實.md "wikilink")，開創動畫史上的商業奇蹟。佐橋俊彥譜寫了如史詩般氣勢磅礡的背景配樂，由於有著即使是單獨聆聽也相當悅耳的高水平，因此OST（電視原聲輯）的銷售量亦取得很好的成績，影響了後來的鋼彈動畫作品，更加重視配樂水平。至今許多人認為SEED系列在歌曲與音樂的表現是GUNDAM動畫史上的高峰。

  - 片頭曲：OP1《[INVOKE](../Page/INVOKE.md "wikilink")》不單是[西川貴教的代表作之一](../Page/西川貴教.md "wikilink")，大受好評後，西川更演唱了短篇外傳動畫[機動戰士GUNDAM
    SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")，以及續作[機動戰士GUNDAM
    SEED
    DESTINYOP與插曲](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")。成為鋼彈系列歷史上唯一參與過三部動畫作品OP的歌手，亦是第一位在鋼彈系列動畫中既演唱歌曲，又出演聲優，甚至擁有相關角色專用機體的男性歌手，奠定他在動漫迷心目中作為著名鋼彈歌手的地位。OP2《moment》則由[徐若瑄與遠藤一馬組成的限定組合](../Page/徐若瑄.md "wikilink")[Vivian
    or
    Kazuma主唱](../Page/Vivian_or_Kazuma.md "wikilink")，除了是鋼彈史上第一次有外國人參與的主題曲，[徐若瑄也是首位參與高達動畫角色聲演的外籍女歌手](../Page/徐若瑄.md "wikilink")。OP3《[Believe](../Page/Believe_\(玉置成實單曲\).md "wikilink")》则是[玉置成实的成名作](../Page/玉置成实.md "wikilink")，於2003年4月23日[Oricon公信榜初登場第](../Page/Oricon公信榜.md "wikilink")5位。OP4《[Realize](../Page/Realize_\(玉置成實單曲\).md "wikilink")》同为玉置成实的名作，於2003年7月24日初登場即達第3位。

<!-- end list -->

  - 片尾曲：ED1《》獲得了很高的銷售量，演唱者[石川智晶在日本節目](../Page/石川智晶.md "wikilink")《》參與VTR的演出，也提到了「住家附近的孩子們都說我成了好厲害的姐姐！」這樣的發言。ED3《[FIND
    THE
    WAY](../Page/FIND_THE_WAY.md "wikilink")》由中島美嘉主唱，在公信榜初登場第4位，之後被收录於专辑《[LØVE
    愛無止盡](../Page/LØVE_愛無止盡.md "wikilink")》，该专辑於2003年11月蟬聯Oricon公信榜雙周冠軍，發行一個月後銷售量總計超過140萬張。這張專輯不僅在日本，在韓國也有很好的銷售成績，為韓國首張突破五萬張的日文專輯；並在第45回日本唱片大賞中，獲得「最佳專輯」、「金賞」以及「作詞賞」。

<!-- end list -->

  - 插入曲：聲優[田中理惠以剧中角色拉克丝](../Page/田中理惠.md "wikilink")·克莱因的身分演唱插曲《》、《》。另一插曲《[晓之车](../Page/晓之车.md "wikilink")》由聲優[南里侑香演唱](../Page/南里侑香.md "wikilink")，於日本Oricon公信榜初登場即取得第12位，此後最高名次為第10位，總計登榜32次；並於2004年『日本格蘭披治獎』得到最佳動畫歌曲榜第三名。

### 反面評價

本作遭[U.C.系列愛好者批判劇情過度商業化](../Page/U.C..md "wikilink")、偶像化，故事到後期逐漸走向英雄主義，戰爭勝負僅靠少數菁英決定，以及愈來愈華麗化的機體設計為人詬病。對U.C.系列愛好者而言，本作悖離作為擬真寫實系的GUNDAM系列主旨，與「超級系」並無二致。

### 播放電視台

## 注釋

## 參考资料

## 外部連結

  - [機動戰士GUNDAM SEED 官方網站](http://www.gundam-seed.net/)

  - [GUNDAM.INFO 官方高達視頻及情報網站](http://hk.gundam.info)

[GUNDAM_SEED](../Category/GUNDAM_SEED.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:2002年日本電視動畫](../Category/2002年日本電視動畫.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")
[Category:緯來電視外購動畫](../Category/緯來電視外購動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:月刊Newtype連載作品](../Category/月刊Newtype連載作品.md "wikilink")
[Category:月刊Magazine Z](../Category/月刊Magazine_Z.md "wikilink")
[Seed](../Category/GUNDAM系列.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  HD Remaster刪減原作舊有的第14和26話，最後為48話

9.

10. 互聯網免費發佈直播的最終話（2012年11月23日），編輯版收錄在藍光BOX最終卷。

11. [機動戦士ガンダムシリーズ平均視聴率](http://1st.geocities.jp/june_2007_taste/saikou.html#No6)

12. [年間売り上げ802億円　ガンダム・ビジネスはいかにして成長？攻めの戦略続けた35年](http://news.livedoor.com/article/detail/8946750/)

13. [L. Tucker, Derrick. "Gundam Seed". THEM Anime
    Reviews.](http://www.themanime.org/viewreview.php?id=293)

14. [Houston, Don (March 19, 2005). "Mobile Suit Gundam Seed –
    Evolutionary Conflict".
    DVDTalk](http://www.dvdtalk.com/reviews/14945/mobile-suit-gundam-seed-evolutionary-conflict/)

15. [機動戦士ガンダムSEED売り上げまとめ](http://dvdbd.wiki.fc2.com/wiki/%E6%A9%9F%E5%8B%95%E6%88%A6%E5%A3%AB%E3%82%AC%E3%83%B3%E3%83%80%E3%83%A0SEED)

16. [「機動戦士ガンダムSEED(シード) DESTINY(デスティニー)」商品をバンダイグループで展開,Press Release of
    BANDAI](http://www.bandai.co.jp/releases/J2004070901.html)

17. [年間売り上げ802億円　ガンダム・ビジネスはいかにして成長？攻めの戦略続けた35年](http://news.livedoor.com/article/detail/8946750/)