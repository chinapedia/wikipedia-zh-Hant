**機動戰士GUNDAM SEED ASTRAY**
是[Sunrise製作的動畫](../Page/Sunrise.md "wikilink")[機動戰士GUNDAM
SEED的正式外傳作品](../Page/機動戰士GUNDAM_SEED.md "wikilink")。「ASTRAY」意思是異常、異端、偏離正道。

本系列採用多位漫畫家於不同的雜誌連載漫畫，而劇本則由[千葉智宏負責](../Page/千葉智宏.md "wikilink")。各個故事有著微妙的關聯、補完正篇沒有解釋的劇情，使故事更加完整。

## 機動戰士GUNDAM SEED ASTRAY 系列

內容分別講述兩位主角羅·裘爾及叢雲劾於[C.E.](../Page/C.E..md "wikilink")70至71的經歷。

  - **機動戰士GUNDAM SEED ASTRAY**（漫畫）[鴇田洸一](../Page/時田洸一.md "wikilink")

<!-- end list -->

  -
    於[Gundam
    Ace連載](../Page/Gundam_Ace.md "wikilink")，主角為羅·裘爾。內容講述他取得異端GUNDAM紅色機的經過和其後的經歷，補完正篇劇情。例如是羅·裘爾救了被[神盾鋼彈](../Page/GAT-X303_Aegis.md "wikilink")[自爆受傷的煌](../Page/自爆.md "wikilink")。而叢雲劾亦以配角登場。單行本全3冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED ASTRAY**（小說）[千葉智宏](../Page/千葉智宏.md "wikilink")

<!-- end list -->

  -
    於[The Sneaker連載](../Page/The_Sneaker.md "wikilink")。單行本全2冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED ASTRAY R**（漫畫）[戶田泰成](../Page/戶田泰成.md "wikilink")

<!-- end list -->

  -
    於[少年 Ace連載](../Page/少年_Ace.md "wikilink")，主角為羅·裘爾。單行本全4冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED ASTRAY
    B**（情景模型小說）[千葉智宏](../Page/千葉智宏.md "wikilink")

<!-- end list -->

  -
    於[電擊HOBBY
    MAGAZINE連載](../Page/電擊HOBBY_MAGAZINE.md "wikilink")，主角為叢雲劾。單行本全1冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED MSV**

<!-- end list -->

  -
    OVA（宣傳短片）谷田部本義

### 登場人物

#### 回收屋

  - 罗·裘尔（CV：[小野坂昌也](../Page/小野坂昌也.md "wikilink")）

<!-- end list -->

  -
    3/26出生、18岁。身高172cm、体重60kg、O型，自然人，是回收屋的中心人物，是绝对的机械天才，只要一眼就能判断机械的问题所在。对职业有着崇高追求的热血男儿。他是在垃圾山被人发现的孤儿，原名是“LOVE”，由于输入错误变成“LOWE”，此後便以此為名稱。

<!-- end list -->

  -
    最初在奧布殖民衛星中執行回收作業時，與傭兵隊「蛇尾」隊長叢雲劾，同時發現[曙光社製作的Astray紅色機及藍色機](../Page/曙光社.md "wikilink")，以及金色機遺下的右臂。此後兩人決定每人分得一部機體（藍色機是叢雲劾為了救援夥伴而從裘尔手中借走，後得贈此機），而紅色機亦成為「羅」的專用MS。

<!-- end list -->

  - 8（CV：[愛河-{里}-花子](../Page/愛河里花子.md "wikilink")）

<!-- end list -->

  -
    「罗」从宇宙空间中漂流的宇宙战斗机中发现的[人工智能电脑](../Page/人工智能.md "wikilink")。由于不是当时普及的[量子电脑](../Page/量子计算机.md "wikilink")，不受OS電腦病毒影響。所以即使遇到OS中毒失靈，8亦可連接機體電腦，重新奪回MS控制權。

<!-- end list -->

  - 利亚姆·贾菲尔德（CV：[速水獎](../Page/速水獎.md "wikilink")）

<!-- end list -->

  -
    12/30出生、Ａ型，20歳，調整者，有一兄長、自然人的双胞胎哥哥希尼斯特。后来在殖民卫星与哥重逢后送哥哥踏上离开地球圈的道路。此後更成為廢物商公會會長。

<!-- end list -->

  - 山吹树里（CV：[豐口惠](../Page/豐口惠.md "wikilink")）

<!-- end list -->

  -
    11/4出生、AB型，16歳，自然人，思考细密，但是非常胆小，经常遏制没头没脑的「罗」，是首位調整者乔治的超級粉絲。但當博士帶來后来代號「GG」（CV：[堀秀行](../Page/堀秀行.md "wikilink")）實際上是搭載喬治腦袋的智能系統後，卻接受不了被增加了幽默人格的GG。

#### 蛇尾

  - 叢雲 劾（CV：[井上和彥](../Page/井上和彥.md "wikilink")）

<!-- end list -->

  -
    傭兵隊「蛇尾」隊長，調整者，曾被聯合軍用作獲取調整者數據，之後為擺脫藥物控制逃走，並開展傭兵生涯。重視同伴，實力非凡，實際上其超卓駕駛技術，更多是來自實戰經驗。因此他亦會按照戰局需要，改造專用MS藍色Astray。

<!-- end list -->

  - 伊萊傑·基爾（CV：[鳥海勝美](../Page/鳥海勝美.md "wikilink")）

<!-- end list -->

  -
    原本隸屬於ZAFT的調整者。雖然擁有非常美麗的容貌，但不知為何，他的身體能力並不具備調整者的特異性。他把這情形視為「自己是徒具外表、沒有內在」，非常在意。

<!-- end list -->

  - 羅蕾塔·亞哲（CV：[西原久美子](../Page/西原久美子.md "wikilink")）

<!-- end list -->

  -
    自然人女性。她是隊上唯一女性，也是有著一個小女兒的單親媽媽。在傭兵間非常受歡迎。她也擁有傭兵的超一流本領，尤其在作戰擬定和爆破任務上更能發揮她卓越的能力。

<!-- end list -->

  - 李德·威勒（CV：[宇垣秀成](../Page/宇垣秀成.md "wikilink")）

<!-- end list -->

  -
    身為自然人的前地球聯合士官，有著因酒品差而被迫辭去軍職的過去。可是他在軍中時有很好的人緣，至今在聯合軍內仍然有很有力的管道。也因為這緣故，很擅長情報收集。

<!-- end list -->

  - 風花·亞哲（CV：[小林優](../Page/小林優.md "wikilink")）

<!-- end list -->

  -
    羅蕾塔帶著的女兒，6歲。是傭兵團重要的成員。曾經作為劾的代表人獨自前往奧布，並與委託人曙光社的艾莉卡訂立契約，因而和「羅」等人認識。作為傭兵團裡和回收屋接觸最多的人，她起到了連結雙方的橋樑般的作用。

#### 歐普

  - 隆德·蜜娜·薩哈克（CV：[勝生真沙子](../Page/勝生真沙子.md "wikilink")）

<!-- end list -->

  -
    歐普五大家族之一薩哈克家族的當家。從為了理想而犧牲國民的烏茲米、將國民當作是奴隸的吉納身上看到這並不是身為一國之君的所作所為，蜜娜得出由人民治理國家才是正道。
    以「天之御柱」作為根據地，以保留實力不參與地球軍與ZAFT軍的戰鬥為首要戰略，與其雙生弟弟的主戰的思想帶有分歧……對殺害吉納的「羅」帶有輕蔑，曾讓「羅」他們自由使用「天之御柱」的工廠設施。

<!-- end list -->

  - 隆德·吉納·薩哈克（CV：[飛田展男](../Page/飛田展男.md "wikilink")）

<!-- end list -->

  -
    歐普五大家族之一薩哈克家族的當家。與阿斯哈一族平起平坐，其養父是GAT計劃和ASTRAY計劃的倡導者，和養父不同的是他反對阿斯哈代表的中立主義；在歐普被地球軍侵略後，和地球連合簽訂密約後靠自己的力量去重建歐普。
    其實是雙胞胎，而且兩姐弟也是協調者，也是作為蜜娜（姐）的替身。
    擁有出雲級一號艦出雲號（二號艦是草薙號）和[金色機](../Page/異端鋼彈#金色機.md "wikilink")，將人民視為奴隸看待，並十分鄙視主角「羅」的存在（即是絆腳石），視他為卑賤的人。
    駕駛金色機天與「羅」的[異端鋼彈紅色機激戰](../Page/異端鋼彈#紅色機.md "wikilink")，雖然折斷其菊一文字，但是紅色機改用從趕來營救的藍色機二型L給予的戰術複合武裝所變成的巨劍切斷機體右臂後，再被藍色機的破甲者直插駕駛艙而死。

<!-- end list -->

  - 索希斯（CV：[斎賀みつき](../Page/斎賀みつき.md "wikilink")）

#### 地球聯合軍

#### ZAFT

  - 古德·威亞（CV：[白鳥哲](../Page/白鳥哲.md "wikilink")）

#### 其他角色

## 機動戰士GUNDAM SEED X ASTRAY

**機動戰士GUNDAM SEED X ASTRAY**是連載於月刊[Gundam
Ace的漫畫作品](../Page/Gundam_Ace.md "wikilink")，由[鴇田洸一負責](../Page/時田洸一.md "wikilink")。本作是機動戰士GUNDAM
SEED ASTRAY的續編，故事發生於動畫[機動戰士GUNDAM
SEED第](../Page/機動戰士GUNDAM_SEED.md "wikilink")47話與第48話之間空白的2個月。主要講述這個故事新登場的普雷亞·雷菲力與卡納得·帕爾斯的對決，而前作的羅·裘爾及叢雲
劾亦以配角登場。單行本全2冊。

## 機動戰士GUNDAM SEED DESTINY ASTRAY 系列

**機動戰士GUNDAM SEED DESTINY ASTRAY**是**機動戰士GUNDAM SEED
ASTRAY**的續編，同樣連載於「月刊Gundam ace」（漫畫），「The
Sneaker」（小說），「電擊HOBBY MAGAZINE」（情景模型小說）。

機動戰士GUNDAM SEED DESTINY ASTRAY以[機動戰士GUNDAM
SEED與](../Page/機動戰士GUNDAM_SEED.md "wikilink")[機動戰士GUNDAM SEED
DESTINY之間約](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")2年作為主要的舞台，本作亦會說明[機動戰士GUNDAM
SEED DESTINY中不被描寫的背景故事](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")。

故事開始在前次大戰結束後，地球圈似乎進入和平時期，但在暗中仍有許多不為人知的行動。攝影記者傑斯在一次採訪中遇到了羅·裘爾，並從他手中得到了異端GUNDAM非規格機（ASTRAY
OUTFRAME）。以追求與傳達真實為目的的傑斯，駕著非規格機奔馳在各處，為了自己的理念而奮戰。

  - **機動戰士GUNDAM SEED DESTINY
    ASTRAY**（漫畫）[鴇田洸一](../Page/時田洸一.md "wikilink")

<!-- end list -->

  -
    於[Gundam Ace連載](../Page/Gundam_Ace.md "wikilink")。單行本全4冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED DESTINY
    ASTRAY**（小說）[千葉智宏](../Page/千葉智宏.md "wikilink")

<!-- end list -->

  -
    於[The Sneaker連載](../Page/The_Sneaker.md "wikilink")。單行本全2冊。

<!-- end list -->

  - **機動戰士GUNDAM SEED DESTINY
    ASTRAY**（情景模型小說）[千葉智宏](../Page/千葉智宏.md "wikilink")

<!-- end list -->

  -
    於[電擊HOBBY
    MAGAZINE連載](../Page/電擊HOBBY_MAGAZINE.md "wikilink")。單行本全2冊。
      - 機動戰士GUNDAM SEED DESTINY ASTRAY 上 -追求真實的人-
      - 機動戰士GUNDAM SEED DESTINY ASTRAY 下 -找尋羈絆的人-

### 登場人物

#### 記者

  - 傑斯·-{里}-布爾（CV：[上田祐司](../Page/上田祐司.md "wikilink")）

駕駛MS的記者，異端鋼彈非規格機（[ZGMF-X12](../Page/ZGMF-X12A_Testament.md "wikilink")）的駕駛

  - 貝爾娜黛特·勒胡

#### 氏族

  - 馬迪亞斯
  - 瑪蒂斯
  - 奶奶

#### 馬戲團

  - 凱特·馬帝坎（CV：[大塚明夫](../Page/大塚明夫.md "wikilink")）
  - 伊爾德·喬拉爾
  - 斥候0646

#### 廢物商

  - 羅·裘爾

<!-- end list -->

  -
    回收屋靈魂人物。曾在創世紀中發現不知名MS的骨架，之後予以修復，並命名為「異端鋼彈非規格機（Astray
    Outframe）」，後來將此MS贈予傑斯，同時留下「8」協助其駕駛。
    在異端鋼彈非規格機被聖約鋼彈大破後，又以Outframe本來設計，重新改修成為「異端鋼彈非規格機D（Astray Outframe D
    ）」。

<!-- end list -->

  - 8

<!-- end list -->

  -
    [歐普曙光社製作的公事包型量子電腦](../Page/歐普.md "wikilink")，由尹世煥所製作。擁有獨立人格。由於8不肯跟隨「羅」前往火星，於是「羅」將8交託予傑斯。

<!-- end list -->

  - 利亚姆·贾菲尔德

<!-- end list -->

  -
    廢物商公會會長、調整者。

<!-- end list -->

  - 普雷亞·雷腓力

他和ZAFT的拉烏·魯·克爾澤一樣，同樣是複製人﹔他是軍方為了得到擁有擁有過人的空間掌握能力的人而進行複製，原型不詳，(目前只知道是梅比烏斯零式小隊其中一人的複製體\[1\])由於“不完全複製”的問題，他的壽命已是風中殘燭。不過作為擁有過人的空間掌握能力的人的複製品，他還是能使用勇士鋼彈（Dreadnought
Gundam）的龍騎兵系統，這在SEED的世界暫時只知道現存有4個人可以使用這種能力。他在負責運送勇士鋼彈到地球時，被劾等人奪去反中子干擾器（Neutron
Jammer
Canceler，簡稱NJC）後，和「羅」一行相遇並接受其幫忙。後來劾將NJC交還後，面對為搶奪NJC而來的卡納得，他駕駛勇士鋼彈使出龍騎兵系統將其擊退。後來為了潑熄卡納得的怒火，他帶著勇士鋼彈投降，但是卡納得的怒火反而燒得更旺了，之後他駕駛X
Astray（勇士鋼彈的完成型態）使用龍騎兵系統的特殊招數擊敗卡納得的超級亥伯龍，救下想自盡的卡納得後，因為陽壽已盡，在醒覺的卡納得懷中安然辭世。

  - GG
  - 尹世煥
  - 山吹樹里
  - 教授

#### 蛇尾

  - 叢雲 劾
  - 伊萊傑·基爾
  - 羅蕾塔·亞哲
  - 李德·威勒
  - 風花·亞哲

#### 傭兵部隊「X」

  - 卡納德·帕爾斯

<!-- end list -->

  -
    一個失敗的超級新人類，在遭受破棄前由響博士（基拉的父親）的一個助手帶走，被地球連合捕獲，進行了大量的實驗。後來成為[歐亞聯盟的亥伯龍高達的駕駛員](../Page/歐亞聯盟.md "wikilink")，性格是“一匹狼”。為了想擊倒基拉·大和成為“完全體”，不斷追尋他。與基拉相反，他既擁有冷靜的一面，也擁有瘋狂的一面。
    在搶奪[中子干擾調解器的任務中](../Page/中子干擾調解器.md "wikilink")，他駕駛的亥伯龍被勇士高達的龍騎兵系統所打敗後，以駕駛該機的普雷亞作為挑戰目標。為了再與勇士高達戰鬥，他甚至帶領特務部隊X叛逃後搶奪了[大西洋聯邦的中子干擾調解器來強化自己的亥伯龍](../Page/大西洋聯邦.md "wikilink")。在最終決戰中被X
    Astray（DREADNOUGHT完全體）的龍騎兵系統所圍困，突圍失敗後想自盡，但是被普雷亞救回，而普雷亞的遺言讓卡納得醒覺。

#### 地球聯邦軍

  - 摩根·雪佛蘭
  - 珍·休斯頓
  - 蕾娜·伊梅里亞
  - 斥候0984
  - 洛德·吉布列
  - 史汀格·歐格雷
  - 奧爾·尼達
  - 史黛菈·路歇

#### 奧布聯合酋長國(O.R.B.)

  - 隆德·蜜納·薩哈克

<!-- end list -->

  -
    奧布五大家族之一薩哈克家族的當家，從為了理想而犧牲國民的烏茲米、將國民當作是奴隸的隆德（弟）身上看到這並不是身為一國之君的所作所為，隆德（姐）得出由人民治理國家才是正道。以「天之御柱」作為根據地，以保留實力不參與地球軍與ZAFT軍的戰鬥為首要戰略，與其孖生弟弟的主戰的思想帶有分歧……對殺害隆德（弟）的「羅」帶有輕蔑，曾讓「羅」他們自由使用「天之御柱」的
    工廠設施。

<!-- end list -->

  - 索基烏斯
  - 量子電腦

<!-- end list -->

  -
    Mark, Micoto, Linda, Cooper, Michelle

#### 三艦同盟

  - 巴瑞·何
  - 姜·凱利
  - 米蕾莉亞·哈烏
  - 艾莉卡·西蒙茨
  - 基拉·大和
  - 拉克絲·克萊茵

#### 南美合眾國

  - 愛德華·哈勒森

#### P.L.A.N.T.

  - 寇特尼·歇洛尼姆特
  - 米哈爾·寇斯特
  - 吉伯特·杜蘭朵

#### Z.A.F.T.

  - 志保·哈尼夫斯
  - 莉卡·席塔 リーカ・シェダー
  - 馬列·斯特勞
  - 真·飛鳥
  - 伊薩克·焦耳
  - 雷·札·巴雷爾

#### 其他人物

  - 塞托娜·溫特
  - 小偷
  - 肯納夫·魯基尼
  - 情報間諜
  - 薩托

### 登场机体

#### [地球聯合](../Page/地球聯合.md "wikilink")──[幻痛](../Page/幻痛_\(GUNDAM\).md "wikilink")

  - [GAT-01 Strike Dagger](../Page/GAT-01_Strike_Dagger.md "wikilink")
      - [GAT-01A1+AQM/E-X02 Sword
        Dagger](../Page/GAT-01A1_Dagger#GAT-01A1_+A_QM/E-X02_Sword_Dagger.md "wikilink")
      - [GAT-01A1+AQM/E-X04 Gunbarrel
        Dagger](../Page/GAT-01A1_Dagger#GAT-01A1_+_AQM/E-X04_Gunbarrel_Dagger.md "wikilink")
  - [GAT-02L2 Dagger L](../Page/GAT-02L2_Dagger_L.md "wikilink")
      - [GAT-02L2+AQM/E-M11 Doppelhorn Dagger
        L](../Page/GAT-02L2_Dagger_L#GAT-02L2_+_AQM/E-M11_Doppelhorn_Dagger_L.md "wikilink")
      - [GAT-02L2+AQM/E-X02 Sword Dagger
        L](../Page/GAT-02L2_Dagger_L#GAT-02L2_+_AQM/E-X02_Sword_Dagger_L.md "wikilink")
      - [GAT-02L2+AQM/E-X03 Launcher Dagger
        L](../Page/GAT-02L2_Dagger_L#GAT-02L2_+_AQM/E-X03_Launcher_Dagger_L.md "wikilink")
  - [GAT-04+AQM/E-X01 Aile
    Windam](../Page/GAT-04_Windam#GAT-04_+_AQM/E-X01_Aile_Windam.md "wikilink")
  - [GAT-333 Raider Full
    Spec](../Page/GAT-333_Raider_Full_Spec.md "wikilink")
  - [GAT-706S Deep
    Forbidden](../Page/GAT-706S_Deep_Forbidden.md "wikilink")
  - [GAT-S02R N Dagger N](../Page/GAT-S02R_N_Dagger_N.md "wikilink")
  - [GAT-X131 Calamity Gundam](../Page/GAT-X131_Calamity.md "wikilink")
      - [GAT-X133 Sword Calamity
        Gundam](../Page/GAT-X133_Sword_Calamity_Gundam.md "wikilink")
  - [GAT-X252 Forbidden
    Gundam](../Page/GAT-X252_Forbidden.md "wikilink")
  - [GAT-X255 Forbidden
    Blue](../Page/GAT-X252_Forbidden#GAT-X255_Forbidden_Blue.md "wikilink")
  - [GAT-X370 Raider Gundam](../Page/GAT-X370_Raider.md "wikilink")
  - [TS-MA4F Exus](../Page/TS-MA4F_Exus.md "wikilink")
  - [TSX-MA717/ZD
    Pergrande](../Page/TSX-MA717/ZD_Pergrande.md "wikilink")
  - [ZGMF-X12A Testament](../Page/ZGMF-X12A_Testament.md "wikilink")
  - [ZGMF-X24S (RGX-01) Chaos
    Gundam](../Page/ZGMF-X24S_Chaos.md "wikilink")
  - [ZGMF-X31S (RGX-02) Abyss
    Gundam](../Page/ZGMF-X31S_Abyss.md "wikilink")
  - [ZGMF-X88S (RGX-03) Gaia
    Gundam](../Page/ZGMF-X88S_Gaia.md "wikilink")
  - [ZGMF-YX21R (RGX-04) Proto-Saviour
    Gundam](../Page/ZGMF-X23S_Saviour#ZGMF-YX21R_Proto-Saviour.md "wikilink")
      - [ZGMF-YX21R+X11A (RGX-04) Proto-Saviour
        Gundam](../Page/ZGMF-X23S_Saviour#ZGMF-YX21R+X11A_Proto-Saviour.md "wikilink")

#### [市民](../Page/市民.md "wikilink")

Mobile Weapons

  - [GAT-01A1 Dagger](../Page/GAT-01A1_Dagger.md "wikilink")
  - [GAT-333 Raider Full
    Spec](../Page/GAT-333_Raider_Full_Spec.md "wikilink")
  - [GAT-X133 Sword
    Calamity](../Page/GAT-X133_Sword_Calamity.md "wikilink")
  - [MBF-P02 Gundam Astray Red Frame Mars
    Jacket](../Page/MBF-P02_Gundam_Astray_Red_Frame_Mars_Jacket.md "wikilink")
  - [MWF-JG71 Raysta](../Page/MWF-JG71_Raysta.md "wikilink")
  - [YMF-X000A/H
    Dreadnought](../Page/YMF-X000A/H_Dreadnought.md "wikilink")

[C.E.](../Page/宇宙纪元.md "wikilink")71年，[扎夫特为提供](../Page/ZAFT.md "wikilink")[反中子干扰器及](../Page/中子干擾調解器.md "wikilink")[龙骑兵系统的测试平台制造了](../Page/機動戰士技術#龍騎兵系統_\(DRAGOON_System\).md "wikilink")[YMF-X000A
勇士鋼彈](../Page/YMF-X000A_Dreadnought.md "wikilink")。测试完毕后，本机落入回收屋之手，由马尔奇欧导师的代表普雷亚·雷腓力驾驶。战争期间，普雷亚与欧亚联邦人员，[亥伯龙鋼彈一号机的驾驶员卡纳德](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")·帕尔斯有数次战斗。在他们的决战中，[亥伯龙鋼彈被摧毁](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")，而普雷亚为了救出机体陷入核暴走的卡纳德牺牲了自己。后来，卡纳德与回收屋驾驶员罗·裘尔将[勇士鋼彈改造为YMF](../Page/YMF-X000A_Dreadnought.md "wikilink")-X000A/H
勇士鋼彈Η。

改造后的本机移除了[勇士鋼彈的一些武器](../Page/YMF-X000A_Dreadnought.md "wikilink")，并增加了[亥伯龙鋼彈的特色](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")。[龙骑兵系统被整套移除](../Page/機動戰士技術#龍騎兵系統_\(DRAGOON_System\).md "wikilink")，由一套光束加农炮/光束军刀所取代，因为卡纳德没有操纵[龙骑兵系统所必需的良好空间感](../Page/機動戰士技術#龍騎兵系統_\(DRAGOON_System\).md "wikilink")。在炮击模式下，加农炮位于肩部上方，与[亥伯龙鋼彈的](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")「吹牛」光束加农炮类似。在剑装模式下，加农炮可于臂下挥动，功能相当于光束军刀，组件后面的对空机关炮则置于肩上。髋甲上的远程制导「鲨」光束感应炮被[亥伯龙鋼彈的](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")「甲胄之光」光波防御屏取代，使用时装于前臂作装甲光罩。最后本机继承了[亥伯龙鋼彈的](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")「Zastava
Stigmate」光束机关炮。

另外，本机名称中的「Η」读作希腊字母「Eta」。由于通常形态下背后的加农炮的形状类似希腊字母「Η」，因此取了这个名字。一说来自[亥伯龙鋼彈名字首字母的](../Page/CAT1-X1/3_Hyperion_Gundam_Unit_1.md "wikilink")「H」。

  - [ZGMF-1001 ZAKU Kaite Madigan
    Custom](../Page/Zaku_\(Cosmic_Era\)#ZGMF-1000_ZAKU_Warrior.md "wikilink")
      - [ZGMF-1017AS GINN Assault
        Type](../Page/ZGMF-1017_GINN#ZGMF-1017AS_GINN_Assault_Type.md "wikilink")
      - [ZGMF-1017M2 GINN High Maneuver Type
        II](../Page/ZGMF-1017_GINN#ZGMF-1017M2_GINN_High_Maneuver_Type_II.md "wikilink")
  - [ZGMF-X12 Gundam Astray Out
    Frame](../Page/ZGMF-X12_Gundam_Astray_Out_Frame.md "wikilink")
  - [ZGMF-X12D Gundam Astray Out Frame
    D](../Page/ZGMF-X12D_Gundam_Astray_Out_Frame_D.md "wikilink")
  - [ZGMF-X12A Testament](../Page/ZGMF-X12A_Testament.md "wikilink")

#### [Z.A.F.T.](../Page/ZAFT.md "wikilink")

  - [ZGMF-X11A Regenerate](../Page/ZGMF-X11A_Regenerate.md "wikilink")
  - [AMF-101 DINN](../Page/AMF-101_DINN.md "wikilink")
      - [AME-WAC01 DINN Special Electronic Installation
        Type](../Page/AMF-101_DINN.md "wikilink")
  - [UMF-04A GOOhN](../Page/UMF-04A_GOOhN.md "wikilink")
  - [YFX-200 CGUE DEEP
    Arms](../Page/YFX-200_CGUE_DEEP_Arms.md "wikilink")
  - [XMF-P192P Proto Chaos](../Page/XMF-P192P_Proto_Chaos.md "wikilink")
  - [ZGMF-600 GuAIZ](../Page/ZGMF-600_GuAIZ.md "wikilink")
      - [ZGMF-601R GuAIZ
        R](../Page/ZGMF-600_GuAIZ#ZGMF-601R_GuAIZ_R.md "wikilink")
  - [ZGMF-1000 ZAKU
    Warrior](../Page/Zaku_\(Cosmic_Era\)#ZGMF-1000_ZAKU_Warrior.md "wikilink")
  - [ZGMF-1001/K Slash ZAKU
    Phantom](../Page/Zaku_\(Cosmic_Era\)#斬擊裝備_Slash_Wizard.md "wikilink")
  - [ZGMF-1001/M Blaze ZAKU
    Phantom](../Page/Zaku_\(Cosmic_Era\)#瞬發裝備_Blaze_Wizard.md "wikilink")
  - [ZGMF-1017 GINN](../Page/ZGMF-1017_GINN.md "wikilink")
  - [ZGMF-X13A Providence
    Gundam](../Page/ZGMF-X13A_Providence.md "wikilink")
  - [ZGMF-X24S (RGX-01) Chaos
    Gundam](../Page/ZGMF-X24S_Chaos.md "wikilink")
  - [ZGMF-X31S (RGX-02) Abyss
    Gundam](../Page/ZGMF-X31S_Abyss.md "wikilink")
  - [ZGMF-X88S (RGX-03) Gaia
    Gundam](../Page/ZGMF-X88S_Gaia.md "wikilink")
  - [ZGMF-X56S Impulse Gundam](../Page/ZGMF-X56S_Impulse.md "wikilink")
  - [ZGMF-X999A ZAKU Mass Production Trial
    Type](../Page/Zaku_\(Cosmic_Era\)#渣古量產試作型.md "wikilink")

#### [O.R.B.](../Page/奧布.md "wikilink")

  - [GAT-X133 Sword
    Calamity](../Page/GAT-X133_Sword_Calamity.md "wikilink")
  - [MBF-M1 M1 Astray](../Page/MBF-M1_M1_Astray.md "wikilink")
  - [MBF-P01-Re2 AMATU Gundam Astray Gold Frame Amatu Mina
    Custom](../Page/MBF-P01-Re2_AMATU_Gundam_Astray_Gold_Frame_Amatu_Mina_Custom.md "wikilink")
  - [MBF-M1A M1A Astray](../Page/MBF-M1_M1_Astray.md "wikilink")
  - [ZGMF-X09A Justice Gundam](../Page/ZGMF-X09A_Justice.md "wikilink")
  - [ZGMF-X10A Freedom Gundam](../Page/ZGMF-X10A_Freedom.md "wikilink")

## 機動戰士GUNDAM SEED C.E.73 Δ（Delta） ASTRAY

**機動戰士GUNDAM SEED C.E.73 Δ ASTRAY**是 **機動戰士GUNDAM SEED DESTINY ASTRAY**
的續編故事，正於Gundam Ace連載中，亦是[鴇田洸一負責](../Page/時田洸一.md "wikilink")。
故事從羅·裘爾他們去了火星開始──一班來自火星的年輕人為了某任務來到地球，而[机动战士GUNDAM SEED
C.E.73
STARGAZER的主角们也有登场](../Page/机动战士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")。

### 登場人物

#### [火星住民](../Page/火星住民.md "wikilink")

  - 亞格尼斯·布拉赫
  - 納厄·赫歇爾
  - 迪亞哥·羅威爾

#### [Z.A.F.T.](../Page/ZAFT.md "wikilink")

  - 艾札克·瑪烏
  - 米哈爾·寇斯特
  - 基巴度·戴蘭達

#### 回收屋

  - 罗·裘尔

#### [地球聯合](../Page/地球聯合.md "wikilink")／[幻痛](../Page/幻痛_\(GUNDAM\).md "wikilink")

  - 史威恩·卡爾·巴亞

### 登场机体

### [地球聯合](../Page/地球聯合.md "wikilink")／[幻痛](../Page/幻痛_\(GUNDAM\).md "wikilink")

  - [GAT-01A1 Dagger](../Page/GAT-01A1_Dagger.md "wikilink")
  - [GAT-01A2R Slaughter
    Dagger](../Page/GAT-01A1_Dagger#GAT-01A2R_+_AQM/E-X01_Slaughter_Dagger.md "wikilink")
  - [GAT-02L2 Dagger L](../Page/GAT-02L2_Dagger_L.md "wikilink")
  - [GAT-02L2+AQM/E-M11 Doppelhorn Dagger
    L](../Page/GAT-02L2_Dagger_L#GAT-02L2_+_AQM/E-M11_Doppelhorn_Dagger_L.md "wikilink")
  - [GAT-X1022 Blu Duel](../Page/GAT-X1022_Blu_Duel.md "wikilink")
  - [GAT-X103AP Verde
    Buster](../Page/GAT-X103AP_Verde_Buster.md "wikilink")
  - [GAT-X105E Strike E](../Page/GAT-X105E_Strike_E.md "wikilink")
  - [GAT-X105E+AQM/E-M1 Strike E
    IWSP](../Page/GAT-X105E+AQM/E-M1_Strike_E_IWSP.md "wikilink")
  - [GAT-X105E+AQM/E-X09S Strike
    Noir](../Page/GAT-X105E_Strike_E#GAT-X105E+AQM/E-X09S_Strike_Noir.md "wikilink")
  - [GAT-X207SR Nero Blitz](../Page/GAT-X207SR_Nero_Blitz.md "wikilink")
  - [GAT-X303AA Rosso
    Aegis](../Page/GAT-X303AA_Rosso_Aegis.md "wikilink")
  - [GFAS-X1 Destroy Gundam](../Page/GFAS-X1_Destroy.md "wikilink")

### [火星住民](../Page/火星住民.md "wikilink")

  - [Astray Mars Jacket](../Page/Astray_Mars_Jacket.md "wikilink")
  - [GSF-YAM01 Δ Astray](../Page/GSF-YAM01_Δ_Astray.md "wikilink")
  - [GSF-YAM02 Guardshell](../Page/GSF-YAM02_Guardshell.md "wikilink")
  - [MMF-JG73L Turn Δ](../Page/MMF-JG73L_Turn_Δ.md "wikilink")

### [Z.A.F.T.](../Page/ZAFT.md "wikilink")

  - [TMF/A-802W2 Kerberos BuCUE
    Hound](../Page/TMF/A-802_BaCUE.md "wikilink")
  - [ZGMF-1000 Kerberos ZAKU
    Warrior](../Page/Zaku_\(Cosmic_Era\).md "wikilink")
  - [ZGMF-1000/A1 Gunner ZAKU
    Warrior](../Page/Zaku_\(Cosmic_Era\)#重炮裝備_Gunner_Wizard.md "wikilink")
  - [ZGMF-1000/M Blaze ZAKU
    Warrior](../Page/Zaku_\(Cosmic_Era\)#瞬發裝備_Blaze_Wizard.md "wikilink")
  - [ZGMF-1001/K Slash ZAKU
    Phantom](../Page/Zaku_\(Cosmic_Era\)#斬擊裝備_Slash_Wizard.md "wikilink")
  - [ZGMF-1001/M Blaze ZAKU
    Phantom](../Page/Zaku_\(Cosmic_Era\)#瞬發裝備_Blaze_Wizard.md "wikilink")

## 機動戰士GUNDAM SEED FRAME ASTRAYS

**機動戰士GUNDAM SEED FRAME ASTRAYS**是**機動戰士GUNDAM SEED C.E.73 Δ
ASTRAY**的續篇。將於11月於電擊Hobby中連載，亦是[鴇田洸一負責](../Page/時田洸一.md "wikilink")。

### 登場人物

#### 蛇尾

  - 叢·雲劾
  - 伊萊傑·基爾
  - 風花·亞哲
  - 蘿莉塔·亞哲

## 機動戰士GUNDAM SEED VS ASTRAY

**機動戰士GUNDAM SEED VS ASTRAY**是**機動戰士GUNDAM SEED FRAME
ASTRAYS**的續篇。將於6月連載。

### 登場人物

#### [廢物回收商工會](../Page/廢物回收商工會.md "wikilink")

  - 羅·裘爾

<!-- end list -->

  -
    廢物回收商公會成員，紅色機的駕駛，於跟另一架紅色機以及強風攻擊鋼彈戰鬥，知道圖書館館員的存在。對於機械有很深的造詣和技術，就這一方面看來都有天才般的資質。好奇心旺盛，對許多事情都喜歡插一腳（甚至成了[煌·大和的救命恩人](../Page/煌·大和.md "wikilink")），雖然很少人能理解，但是有自己的信念。

<!-- end list -->

  - 山吹樹里

<!-- end list -->

  -
    羅的女朋友（自稱）

<!-- end list -->

  - 8

<!-- end list -->

  -
    公事包型擬似人格電腦，羅的好伙伴。

<!-- end list -->

  - 尹·世煥

#### [圖書館員](../Page/圖書館員.md "wikilink")

  - ND HE

<!-- end list -->

  -
    強風攻擊鋼彈的駕駛員，真面目完全不明的-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-，帶著全罩式面具。身上的衣服顯是他是聯合軍的人物，當他脫下面具時，在圖書館員的成員中引起的感情變化更難以預料。在最新劇情中，與吉納出動后，被吉納識破真身為叢雲劾的-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-。

<!-- end list -->

  - 普雷亞·雷腓力

<!-- end list -->

  -
    圖書館員最高司書官，X ASTRAY的主角，原本X
    ASTRAY最後已經死亡，卻以長大後的面貌登場，推測可能是藉由-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-技術復活，吹雪天帝鋼彈的駕駛員，身上的衣服傳得十分像D
    ASTRAY中消滅氏族的馬迪亞斯相同，跟X ASTRAY中的普雷亞一樣，有盡量避免戰鬥的傾向。

<!-- end list -->

  - 費尼斯·索西斯

<!-- end list -->

  -
    雹陣暴風鋼彈的駕駛員，戰鬥用的人造調整者「索希斯」的其中一人，他們都具有相同的容顏以及表情欠缺變化的共通點，但是費尼斯的感情起伏，與一般「索希斯」不同能反抗自然人。在「索希斯」中也有保護世界和平的，他們跟這位已經是叛徒的同族毀產生什麼對峙呢？

<!-- end list -->

  - 隆德·吉納·薩哈克

<!-- end list -->

  -
    異端鋼彈幻惑機的駕駛員，藉由-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-技術復活的吉納，對這個世界抱著復仇的怒火，同時對背叛的姊姊蜜娜以及殺害自己的羅與劾抱持著憎恨，是故事中的危險因素。在最新劇情中駕駛的異端幻惑機再次敗給劾的藍色機，戰敗後的他為了復仇將自己肉體加以改造。

<!-- end list -->

  - 凱特·馬蒂坎

<!-- end list -->

  -
    暴雨決鬥鋼彈的駕駛員，D
    ASTRAY中與駕駛MS「異端鋼彈非規格機」的記者傑斯·-{里}-布爾一同行動的職業MS駕駛員，跟傭兵不同，戰鬥意外的任務也願意接，不過跟MS無關的任務他不會接，不是圖書館員的正式成員。此外還有MS收藏家的一面，連全世界只有一架的試驗機都是他的。

<!-- end list -->

  - 蘊奧

<!-- end list -->

  -
    偽紅色機的駕駛員，是-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-技術以三十歲姿態復活的老人，羅的師父，雖然在圖書館員組織裡，但是似乎想保護羅。後於與莉莉‧薩瓦里一戰中戰死。

<!-- end list -->

  - 莉莉·薩瓦里

<!-- end list -->

  -
    迷霧電擊鋼彈的駕駛員，年輕的女性，雖具有足以匹敵王牌駕駛員的能力，由於年紀過小，因此出現了類似多重人格（精神不安定）的行為，迷霧電擊鋼彈的幻象化粒子瞬移系統只有她才能操控。她穿的是專屬的駕駛服。跟凱特‧馬帝坎同樣不是-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-。有20個莉莉存在，迷霧電擊鋼彈也有20架，為了保護羅決定駕駛金色機天出擊

<!-- end list -->

  - 古德·威亞

<!-- end list -->

  -
    旋風救星鋼彈的駕駛員，有「札夫特的英雄」這個外號，與伊萊傑‧基爾為好友。在過去因為悲劇而死亡，圖書館利用-{zh-hans:复制人;zh-hk:影印人;zh-tw:複製人;}-技術將他姿態復活，他也是人造調整者「索希斯」DNA的原型，目前奪取了伊萊傑的薩克。把自己的座機旋風救星鋼彈留給伊萊傑駕駛。並且脫離圖書館員。

#### 蛇尾

  - 叢雲劾

<!-- end list -->

  -
    藍色機二型改的駕駛，傭兵部隊「蛇尾」隊長

<!-- end list -->

  - 伊萊傑·基爾

<!-- end list -->

  -
    傭兵部隊「蛇尾」成員，有「英雄殺手伊萊傑」的外號，座機是改造自札夫特軍的薩克，後來機體被威亞奪走，改搭乘威亞留下的旋風救星鋼彈。

#### 天之御柱

  - 隆德·蜜娜·薩哈克

<!-- end list -->

  -
    居住在天之御柱的女性調整者，將那裡最為自己的居城居住著。吉納的姊姊，原為歐譜五大氏族的其中一人，用不同於歐普的方式關注世界進展的超然存在

#### [火星居民](../Page/火星居民.md "wikilink")

  - 亞格尼斯·布拉赫

<!-- end list -->

  -
    逆△鋼彈的駕駛者，因為機體被不明敵人破壞而來找羅。羅將逆△鋼彈的\[逆△系統\]移植到自己的紅色機改身上。

#### 傭兵部隊X

  - 卡納德·帕爾斯

### 登場機體

### 圖書館館員

  - [LG-GAT-X105 Gale
    Strike強風攻擊鋼彈](../Page/LG-GAT-X105_Gale_Strike.md "wikilink")

<!-- end list -->

  -
    駕駛員：ND HE

<!-- end list -->

  - [LN-ZGMF-X13A Nix
    Providence吹雪天帝鋼彈](../Page/LN-ZGMF-X13A_Nix_Providence.md "wikilink")

<!-- end list -->

  -
    武裝：龍騎兵型攻擊裝備
    駕駛員：普雷亞·雷腓利

<!-- end list -->

  - [LR-GAT-X102 Regen
    Duel暴雨決鬥鋼彈](../Page/LR-GAT-X102_Regen_Duel.md "wikilink")

<!-- end list -->

  -
    駕駛員：凱特·馬蒂坎

<!-- end list -->

  - [MBF-P05LM Gundam Astray Mirage
    Frame異端鋼彈幻惑機](../Page/MBF-P05LM_Gundam_Astray_Mirage_Frame.md "wikilink")

<!-- end list -->

  -
    駕駛員：隆德·吉納·薩哈克

<!-- end list -->

  - [LH-GAT-X103 Hail
    Buster雹陣暴風鋼彈](../Page/LH-GAT-X103_Hail_Buster.md "wikilink")

<!-- end list -->

  -
    駕駛員：費尼斯·索希斯

<!-- end list -->

  - [LV-ZGMF-X23S Vent
    Saviour旋風救星鋼彈](../Page/ZGMF-X23S_Saviour#LV-ZGMF-X23S_Vent_Saviour.md "wikilink")

<!-- end list -->

  -
    駕駛員：古德·威亞、伊萊傑·基爾

<!-- end list -->

  - [LN-GAT-X207 Nebula
    Blitz迷霧電擊鋼彈](../Page/LN-GAT-X207_Nebula_Blitz.md "wikilink")

<!-- end list -->

  -
    武裝：災禍生太刀、攻擊系統「三犄」、都牟羽太刀、幻象化粒子瞬移系統
    駕駛員：莉莉·薩瓦里

<!-- end list -->

  - [MBF-P05LM2 Gundam Astray Mirage Frame Second
    Issue異端鋼彈幻惑機二期型](../Page/MBF-P05LM2_Gundam_Astray_Mirage_Frame_Second_Issue.md "wikilink")

<!-- end list -->

  -
    駕駛員：隆德·吉納·薩哈克

### 回收屋

  - [MBF-P02 Gundam Astray Red Frame
    Kai異端鋼彈紅色機改](../Page/MBF-P02_Gundam_Astray_Red_Frame_Kai.md "wikilink")

<!-- end list -->

  -
    武裝：菊一文字、虎徹、戰術複合武裝2L
    駕駛員：羅·裘爾

### 蛇尾

  - [MBF-P03 Second Gundam Astray Blue Frame Second
    Revise異端鋼彈藍色機二型改](../Page/MBF-P03_Second_Gundam_Astray_Blue_Frame_Second_Revise.md "wikilink")

<!-- end list -->

  -
    武裝：戰術複合武裝
    駕駛員：叢雲劾

### 天之御柱

  - [MBF-P01 Gundam Astray Gold Frame
    Mina異端鋼彈金色機天蜜娜](../Page/MBF-P01_Gundam_Astray_Gold_Frame_Mina.md "wikilink")

<!-- end list -->

  -
    武裝：三犄改、都牟羽太刀、災禍生太刀
    駕駛員：隆德·蜜娜·薩哈克、莉莉·薩瓦里

### 傭兵部隊X

  - 勇士鋼彈H

<!-- end list -->

  -
    武裝：龍騎兵系統
    駕駛：卡納德·帕爾斯

## 参考文献

## 外部連結

  - [機動戰士GUNDAM SEED ASTRAY系列官方網站](http://www.astrays.net/)
  - [機動戰士GUNDAM SEED
    ASTRAY官方網站](http://www.gundam-seed.net/astray/index.html)
  - [機動戰士GUNDAM SEED DESTINY
    ASTRAY官方網站](http://www.gundam-seed-d.net/d_astray/index.html)
  - [機動戰士GUNDAM SEED C.E.73 DELTA ASTRAY
    官方網站](http://www.delta-astray.net/)
  - [機動戰士GUNDAM SEED FRAME ASTRAYS
    官方網站](http://www.astrays.net/framastrays/index.html)
  - [機動戰士GUNDAM SEED VS
    ASTRAY官方網站](http://www.astrays.net/vsastray/index.html)
  - [ASTRAY
    BLOG（千葉智宏BLOG）](https://web.archive.org/web/20060616090515/http://www.studioorphee.net/blog/)
  - [ときた洸一のムダ話blog(鴇田洸一BLOG)](http://tokita.blog.so-net.ne.jp/)
  - [偏離正軌的道路ASTRAY
    (繁體中文網站)](https://web.archive.org/web/20070921204606/http://web.gds-astray.com/)

[Category:GUNDAM SEED](../Category/GUNDAM_SEED.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:GUNDAM
ACE](../Category/GUNDAM_ACE.md "wikilink")
[Category:月刊少年Ace連載作品](../Category/月刊少年Ace連載作品.md "wikilink")
[Category:GUNDAM系列小說](../Category/GUNDAM系列小說.md "wikilink")
[Category:GUNDAM系列漫畫](../Category/GUNDAM系列漫畫.md "wikilink")

1.  ガンダムの常識 モビルスーツ大百科 機動戦士ガンダムSEED ザフト篇[雙葉社](../Page/雙葉社.md "wikilink")