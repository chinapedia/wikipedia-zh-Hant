《**機動戰士GUNDAM SEED DESTINY**》是《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》的續篇，機動戰士鋼彈系列之一。[MBS製作](../Page/每日放送.md "wikilink")，[TBS系播放](../Page/TBS電視台.md "wikilink")，2004年10月9日至2005年10月1日逢週六下午六時播出。台灣版權由[博英社取得](../Page/博英社.md "wikilink")，並於2005年10月8日起於[中國電視公司逢週六播放](../Page/中國電視公司.md "wikilink")。[香港](../Page/香港.md "wikilink")[無線電視則譯為](../Page/電視廣播有限公司.md "wikilink")《**機動戰士特種命運**》，自2006年6月24日至2007年7月7日於[無線電視](../Page/電視廣播有限公司.md "wikilink")[翡翠台播放](../Page/翡翠台.md "wikilink")；香港[AXN亦有播放](../Page/AXN.md "wikilink")。

2002年《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》獲得空前成功，據監督[福田己津央表示](../Page/福田己津央.md "wikilink")，本來的預定是要循《[新機動戰記鋼彈
W](../Page/新機動戰記鋼彈_W.md "wikilink")》的操作手法，接著推出劇場版；但後來在各方期待下變成再做一部TV續篇。距離前作完結僅一年的時間，本作《**機動戰士GUNDAM
SEED
DESTINY**》即問世，銜接之快可說是前所未有的。兩部TV動畫構成次於[U.C.紀元的第二大世界觀](../Page/U.C..md "wikilink")[C.E.紀元](../Page/宇宙纪元.md "wikilink")，同時也是繼[U.C.紀元之後](../Page/U.C..md "wikilink")，第二个擁有的系列。

## 故事簡介

[C.E.](../Page/C.E..md "wikilink")70年，由於「**血腥情人節**」的慘劇，[地球聯合與](../Page/地球聯合.md "wikilink")[P.L.A.N.T.爆發了全面的武裝衝突](../Page/P.L.A.N.T..md "wikilink")，被稱為「**雅金‧杜威戰役**（或稱血腥情人節戰爭）」。在這場戰爭中，[真·飛鳥家族被捲入](../Page/真·飛鳥.md "wikilink")[大西洋聯邦對](../Page/大西洋聯邦.md "wikilink")[奧布的侵攻](../Page/奧布.md "wikilink")，撤離途中受到流彈波及，其雙親和胞妹葬身在自己眼前。緊握著唯一的遺物‧妹妹的行動電話，抬頭望著呼嘯飛去的[自由鋼彈](../Page/ZGMF-X10A_Freedom.md "wikilink")，真對於戰爭的憎惡以及自身的無力發出悲痛欲絕的嘶吼，失意的他隨後被遣送[P.L.A.N.T.](../Page/P.L.A.N.T..md "wikilink")。

為時一年半的戰爭直至**第二次[雅金·杜威攻防戰](../Page/雅金·杜威.md "wikilink")**落幕，雙方締結「**尤尼烏斯條約**」後方才平息；然而，一紙停戰條約並無法消除[自然人與](../Page/自然人.md "wikilink")[調整者之間紛爭的火種](../Page/調整者.md "wikilink")。停戰後兩年，[C.E.](../Page/C.E..md "wikilink")73年10月2日。為了和[P.L.A.N.T.最高評議會議長](../Page/P.L.A.N.T..md "wikilink")[吉伯特·杜蘭朵的非正式會談](../Page/吉伯特·杜蘭朵.md "wikilink")，以及[Z.A.F.T.新造艦](../Page/Z.A.F.T..md "wikilink")[智慧女神號的下水儀式](../Page/LHM-BB01_Minerva.md "wikilink")，[奧布聯合首長國代表首長](../Page/奧布.md "wikilink")[卡嘉莉·由拉·阿斯哈與其護衛](../Page/卡嘉莉·由拉·阿斯哈.md "wikilink")[阿斯蘭·薩拉一行參訪](../Page/阿斯蘭·薩拉.md "wikilink")[P.L.A.N.T.位於L](../Page/P.L.A.N.T..md "wikilink")7的資源[衛星](../Page/衛星.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。然而突如其來地，[Z.A.F.T.位於軍械庫一號的新型](../Page/Z.A.F.T..md "wikilink")[MS三機遭身分不明者強奪](../Page/MS.md "wikilink")，周圍陷入一片混亂。最後從天而降阻止這一切的是一台全新的[GUNDAM](../Page/GUNDAM.md "wikilink")，其駕駛員見到陷入火海的四周，憤怒地大喊：「又想要挑起戰爭了嗎！？你們這些人！！」他正是成為[Z.A.F.T.戰士的](../Page/Z.A.F.T..md "wikilink")[真·飛鳥](../Page/真·飛鳥.md "wikilink")……！

## 登場角色

## 登場機體

## 登场舰船及其他兵器

## 製作陣容

  - 企畫：[SUNRISE](../Page/日昇動畫.md "wikilink")
  - 原作：[矢立肇](../Page/矢立肇.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")
  - 監督：[福田己津央](../Page/福田己津央.md "wikilink")
  - 系列構成：[兩澤千晶](../Page/兩澤千晶.md "wikilink")
  - 腳本：兩澤千晶、大野木寬、兵頭一步、野村祐一、森田繁、吉野弘幸
  - 人物設定：平井久司
  - 機械設定：大河原邦男、山根公利
  - 設計工程：藤岡建機
  - 首席機械作畫監督： 重田智
  - 色彩設計：柴田亞紀子
  - 美術導演：池田繁美
  - 攝影導演：葛山剛士
  - 編集：野尻由紀子
  - 音樂：[佐橋俊彥](../Page/佐橋俊彥.md "wikilink")
  - 音樂製作：野崎圭一（Victor Entertainment）、篠原廣人（Sony Music
    Entertainment）、真野昇（SUNRISE音樂出版）
  - 音響監督：藤野貞義
  - 執行出品：竹田青滋（每日放送）、宮河恭夫（SUNRISE）
  - 出品：諸富洋史（每日放送）、丸山博雄（每日放送）、佐藤弘幸（SUNRISE）
  - 協助製作：創通Agency/[ADK](../Page/ADK.md "wikilink")
  - 製作：每日放送、SUNRISE

## 主題曲

### 原版

  - 片頭曲

<!-- end list -->

1.  「**ignited -イグナイテッド-**」**（PHASE-01 - PHASE-13）**
      -
        作詞：[井上秋緒](../Page/井上秋緒.md "wikilink") /
        作曲、編曲：[淺倉大介](../Page/淺倉大介.md "wikilink")
        / 主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
2.  「**PRIDE**」**（PHASE-14 - PHASE-24）**
      -
        作詞、作曲、編曲、主唱：[HIGH and MIGHTY
        COLOR](../Page/HIGH_and_MIGHTY_COLOR.md "wikilink")
3.  「**我們的去向**」（）**（PHASE-25 - PHASE-37）**
      -
        作詞：[Yuta
        Nakano](../Page/中野雄太.md "wikilink")+[shungo.](../Page/shungo..md "wikilink")
        / 作曲、編曲：Yuta Nakano / 主唱：[高橋瞳](../Page/高橋瞳.md "wikilink")
4.  「**Wings of Words**」**（PHASE-38 - FINAL PHASE）**
      -
        作詞：[森雪之丞](../Page/森雪之丞.md "wikilink") /
        作曲：[葛谷葉子](../Page/葛谷葉子.md "wikilink")、谷口尚久
        / 編曲：谷口尚久 / 主唱：[CHEMISTRY](../Page/化學超男子.md "wikilink")
5.  「**vestige -ヴェスティージ-**」**（FINAL PLUS）**
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 / 主唱：T.M.Revolution

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「**Reason**」**（PHASE-01 - PHASE-13）**
      -
        作詞：[shungo.](../Page/shungo..md "wikilink") / 作曲：[y@suo
        otani](../Page/大谷靖夫.md "wikilink") /
        編曲：[ats-](../Page/ats-.md "wikilink") /
        主唱：[玉置成實](../Page/玉置成實.md "wikilink")
2.  「**Life Goes On**」**（PHASE-14 - PHASE-25）**
      -
        作詞、主唱：[有坂美香](../Page/有坂美香.md "wikilink") /
        作曲：[梶浦由記](../Page/梶浦由記.md "wikilink") /
        編曲：梶浦由記、[西川進](../Page/西川進.md "wikilink")
3.  「**I Wanna Go To A Place...**」**（PHASE-26 - PHASE-37）**
      -
        作詞、作曲、主唱：[Rie fu](../Page/Rie_fu.md "wikilink") / 編曲：SNORKEL
4.  「****」（你與我相似）**（PHASE-38 - FINAL PLUS、Special Edition IV 自由的代價）**
      -
        作詞：石川智晶 / 作曲、編曲：梶浦由記 /
        主唱：[See-Saw](../Page/See-Saw.md "wikilink")
5.  「**Result**」**（Special Edition I 破碎的世界）**
      -
        作詞：shungo. / 作曲：[藤末樹](../Page/藤末樹.md "wikilink") /
        編曲：[齋藤真也](../Page/齋藤真也.md "wikilink") / 主唱：玉置成實
6.  「**tears**」**（Special Edition II 各自的劍）**
      -
        作詞、作曲：[小峰理紗](../Page/小峰理紗.md "wikilink") /
        編曲：[江口貴勅](../Page/江口貴勅.md "wikilink") /
        主唱：[lisa](../Page/小峰理紗.md "wikilink")
7.  「****」**（Special Edition III 命運的業火）**
      -
        作词、作曲、编曲、主唱：HIGH and MIGHTY COLOR

<!-- end list -->

  - 插入曲

<!-- end list -->

1.  「**Fields of hope**」**（PHASE-7、PHASE-20、PHASE-41、破碎的世界、自由的代價）**
      -
        作詞、作曲、編曲：梶浦由記 /
        主唱：[拉克絲·克萊因](../Page/拉克絲·克萊因.md "wikilink")（[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")）
2.  「****」**（PHASE-10、PHASE-19、破碎的世界）**
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：拉克絲·克萊因（田中理惠）
3.  「**Quiet Night C.E.73**」**（PHASE-17、PHASE-19、PHASE-24、PHASE-26）**
      -
        作詞：梶浦由記 / 作曲：佐橋俊彦 / 編曲：鈴木Daichi秀行 /
        主唱：[蜜雅·坎貝爾](../Page/蜜雅·坎貝爾.md "wikilink")（田中理惠）
4.  「****」（深海的孤獨）**（PHASE-21、PHASE-26、PHASE-30、PHASE-33、命運的業火）**
      -
        作詞、作曲、編曲：梶浦由記 / 主唱：[桑島法子](../Page/桑島法子.md "wikilink")
5.  「**Meteor -ミーティア-**」**（PHASE-23、各自的劍，HD重制版PHASE-23、PHASE-41）**
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 / 主唱：T.M.Revolution
6.  「**vestige
    -ヴェスティージ-**」**（PHASE-39、PHASE-41、PHASE-42、PHASE-49、命運的業火）**
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 / 主唱：T.M.Revolution
7.  「****」**（PHASE-40、PHASE-44 - PHASE-49）**
      -
        作詞、作曲、編曲：[梶浦由記](../Page/梶浦由記.md "wikilink") /
        主唱：[FictionJunction
        YUUKA](../Page/FictionJunction.md "wikilink")
        PHASE-44之后在次回预告中使用。
8.  「**EMOTION**」**（PHASE-47、各自的劍）**
      -
        作詞：清水しょうこ/ 作曲、編曲：鈴木Daichi秀行 / 主唱：蜜雅·坎貝爾（田中理惠）
9.  「**Zips（[UNDER:COVER](../Page/UNDER:COVER.md "wikilink")
    ver.）**」**（破碎的世界）**
      -
        作詞：井上秋緒 / 作曲：淺倉大介 / 編曲：鈴木覚 / 主唱：T.M.Revolution

### HD Remaster

  - 片頭曲

<!-- end list -->

1.  「**ignited -イグナイテッド-**」**（HD重制版PHASE-01 - PHASE-13）**
      -
        作词：[井上秋緒](../Page/井上秋緒.md "wikilink") /
        作曲、編曲：[淺倉大介](../Page/淺倉大介.md "wikilink")
        / 主唱：[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
2.  「**PRIDE**」**（HD重制版PHASE-14 - PHASE-24）**
      -
        作词、作曲、編曲、主唱：[HIGH and MIGHTY
        COLOR](../Page/HIGH_and_MIGHTY_COLOR.md "wikilink")
3.  「**我們的去向**」（）**（HD重制版PHASE-25 - PHASE-37）**
      -
        作词：[Yuta
        Nakano](../Page/中野雄太.md "wikilink")+[shungo.](../Page/shungo..md "wikilink")
        / 作曲、編曲：Yuta Nakano / 主唱：[高橋瞳](../Page/高橋瞳.md "wikilink")
4.  「**vestige -ヴェスティージ-**」**（HD重制版PHASE-38 - FINAL PHASE）**
      -
        作词：井上秋緒 / 作曲、編曲：淺倉大介 / 主唱：T.M.Revolution

<!-- end list -->

  - 片尾曲

<!-- end list -->

1.  「**Reason**」**（HD重制版PHASE-01 - PHASE-11）**
      -
        作词：[shungo.](../Page/shungo..md "wikilink") / 作曲：[y@suo
        otani](../Page/大谷靖夫.md "wikilink") /
        編曲：[ats-](../Page/ats-.md "wikilink") /
        主唱：[玉置成實](../Page/玉置成實.md "wikilink")
2.  「**Result**」**（HD重制版PHASE-12 - PHASE-13）**
      -
        作词：shungo. / 作曲：[藤末樹](../Page/藤末樹.md "wikilink") /
        編曲：[齋藤真也](../Page/齋藤真也.md "wikilink") / 主唱：玉置成實
3.  「**Life Goes On 〜ReMix2013**」**（HD重制版PHASE-14 - PHASE-25）**
      -
        作词、主唱：[有坂美香](../Page/有坂美香.md "wikilink") /
        作曲：[梶浦由記](../Page/梶浦由記.md "wikilink") /
        編曲：梶浦由記、[西川進](../Page/西川進.md "wikilink")
4.  「**I Wanna Go To A Place...**」**（HD重制版PHASE-26 - PHASE-27、PHASE-29 -
    PHASE-31、PHASE-33 - PHASE-37）**
      -
        作词、作曲、主唱：[Rie fu](../Page/Rie_fu.md "wikilink") / 編曲：SNORKEL
5.  「**tears 〜ReMix2013**」**（HD重制版PHASE-28）**
      -
        作词、作曲：[小峰理紗](../Page/小峰理紗.md "wikilink") /
        編曲：[江口貴勅](../Page/江口貴勅.md "wikilink") /
        主唱：[コミネリサ](../Page/小峰理紗.md "wikilink")
6.  「****」**（HD重制版PHASE-32）**
      -
        作词、作曲、編曲：梶浦由記 / 主唱：[桑島法子](../Page/桑島法子.md "wikilink")
7.  「****」**（HD重制版PHASE-41）**
      -
        作词、作曲、编曲、主唱：HIGH and MIGHTY COLOR
8.  「****」（你与我相似〜ReMix2013）**（HD重制版PHASE-38 - PHASE-40、PHASE-42 - FINAL
    PHASE）**
      -
        作詞：石川智晶 / 作曲、編曲：梶浦由記 /
        主唱：[See-Saw](../Page/See-Saw.md "wikilink")

<!-- end list -->

  - 插入曲

<!-- end list -->

1.  「**Meteor -ミーティア-**」**（HD重制版PHASE-23、PHASE-41）**
      -
        作詞：井上秋緒 / 作曲、編曲：淺倉大介 / 主唱：T.M.Revolution
2.  「**Fields of hope 〜ReTracks**」**（HD重制版PHASE-7、PHASE-20、FINAL
    PHASE）**
      -
        作词、作曲、编曲：梶浦由記 /
        主唱：[拉克絲·克萊因](../Page/拉克絲·克萊因.md "wikilink")（[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")）
3.  「****」**（HD重制版PHASE-10、PHASE-29）**
      -
        作词、作曲、编曲：梶浦由記 /
        主唱：[拉克絲·克萊因](../Page/拉克絲·克萊因.md "wikilink")（[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")）
4.  「**Quiet Night C.E.73
    〜ReTracks**」**（HD重制版PHASE-17、PHASE-19、PHASE-24、PHASE-26）**
      -
        作词：梶浦由記 / 作曲：佐橋俊彦 / 编曲：鈴木Daichi秀行 /
        主唱：[蜜雅·坎貝爾](../Page/蜜雅·坎貝爾.md "wikilink")（[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")）
5.  「****」**（HD重制版PHASE-21、PHASE-26、PHASE-30）**
      -
        作词、作曲、编曲：梶浦由記 / 主唱：[桑島法子](../Page/桑島法子.md "wikilink")
6.  「****」**（HD重制版PHASE-40）**
      -
        作词、作曲、编曲：[梶浦由記](../Page/梶浦由記.md "wikilink") /
        主唱：[FictionJunction
        YUUKA](../Page/FictionJunction.md "wikilink")
7.  「**EMOTION 〜ReTracks**」**（HD重制版PHASE-46、PHASE-47）**
      -
        作词：清水しょうこ/ 作曲、编曲：鈴木Daichi秀行 / 主唱：蜜雅·坎貝爾（田中理惠）

## 各話標題

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>日文原名</p></th>
<th><p>香港官方譯名[1]</p></th>
<th><p>台灣中文譯名[2]</p></th>
<th><p>腳本</p></th>
<th><p>| 分鏡</p></th>
<th><p>| 演出</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>憤怒的目光</p></td>
<td><p>憤怒之眼</p></td>
<td><p><a href="../Page/兩澤千晶.md" title="wikilink">兩澤千晶</a></p></td>
<td><p><a href="../Page/福田己津央.md" title="wikilink">福田己津央</a></p></td>
<td><p><a href="../Page/鳥羽聰.md" title="wikilink">鳥羽聰</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>招來戰爭之物</p></td>
<td><p>招來戰爭之物</p></td>
<td><p><a href="../Page/山口晋.md" title="wikilink">山口晋</a><br />
福田己津央</p></td>
<td><p>山口晋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>預兆的砲火</p></td>
<td><p>預兆的砲火</p></td>
<td><p><a href="../Page/西澤晋.md" title="wikilink">西澤晋</a></p></td>
<td><p><a href="../Page/高田昌宏.md" title="wikilink">高田昌宏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>群星的戰場</p></td>
<td><p>星屑的戰場</p></td>
<td><p><a href="../Page/兵頭一歩.md" title="wikilink">兵頭一歩</a><br />
兩澤千晶</p></td>
<td><p><a href="../Page/西山明樹彥.md" title="wikilink">西山明樹彥</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>無法癒合的傷痕</p></td>
<td><p>無法癒合的傷痕</p></td>
<td><p><a href="../Page/野村祐一.md" title="wikilink">野村祐一</a><br />
兩澤千晶</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>世界終結之時</p></td>
<td><p>當世界終結之時</p></td>
<td><p><a href="../Page/下田正美.md" title="wikilink">下田正美</a></p></td>
<td><p><a href="../Page/吉村章.md" title="wikilink">吉村章</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>混亂的大地</p></td>
<td><p>混亂的大地</p></td>
<td><p><a href="../Page/大野木寬.md" title="wikilink">大野木寬</a><br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p><a href="../Page/谷田部勝義.md" title="wikilink">谷田部勝義</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>匯合</p></td>
<td><p>匯集點</p></td>
<td><p>兵頭一歩<br />
兩澤千晶</p></td>
<td><p>下田正美</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>驕傲之牙</p></td>
<td><p>傲慢的獠牙</p></td>
<td><p><a href="../Page/森田繁.md" title="wikilink">森田繁</a><br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>父親的咒縛</p></td>
<td><p>父親的束縛</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>選擇的道路</p></td>
<td><p>選擇的道路</p></td>
<td><p>西澤晋</p></td>
<td><p>吉村章</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>血染大海</p></td>
<td><p>血染大海</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>高田昌宏</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>甦醒之翼</p></td>
<td><p>甦醒之翼</p></td>
<td><p>兵頭一歩<br />
兩澤千晶</p></td>
<td><p><a href="../Page/米たにヨシトモ.md" title="wikilink">米たにヨシトモ</a></p></td>
<td><p>谷田部勝義</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>向明日出航</p></td>
<td><p>航向明日</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>重返戰場</p></td>
<td><p>重返戰場</p></td>
<td><p>大野木寛<br />
兩澤千晶</p></td>
<td><p><a href="../Page/須永司.md" title="wikilink">須永司</a></p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>印度洋的死鬥</p></td>
<td><p>印度洋的死鬥</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>戰士的條件</p></td>
<td><p>戰士的條件</p></td>
<td><p>兵頭一歩<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ</p></td>
<td><p>吉村章</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>討伐堂懷室！</p></td>
<td><p>討伐羅安格林砲台!</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p><a href="../Page/西村大樹.md" title="wikilink">西村大樹</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>看不見的真實！</p></td>
<td><p>看不見的真實</p></td>
<td><p><a href="../Page/吉野弘幸.md" title="wikilink">吉野弘幸</a><br />
兩澤千晶</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>過去</p></td>
<td><p>PAST</p></td>
<td><p>兩澤千晶</p></td>
<td><p>高田昌宏</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p>迷惘的眼神</p></td>
<td><p>迷惘的眼神</p></td>
<td><p>西澤晋</p></td>
<td><p>西山明樹彥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p>蒼天之劍</p></td>
<td><p>蒼天之劍</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>谷田部勝義</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>戰火的陰影</p></td>
<td><p>戰火的陰影</p></td>
<td><p>西澤晋<br />
福田己津央</p></td>
<td><p>高田昌宏</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>錯過的視線</p></td>
<td><p>錯過的視線</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>吉村章</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td></td>
<td><p>罪惡的所在</p></td>
<td><p>罪惡的所在</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td></td>
<td><p>約定</p></td>
<td><p>約束</p></td>
<td><p>野村祐一</p></td>
<td><p>西澤晋</p></td>
<td><p>西村大樹</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td></td>
<td><p>無法傳達的思念</p></td>
<td><p>無法傳遞的思緒</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
鳥羽聰</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td></td>
<td><p>殘存與消逝的生命</p></td>
<td><p>殘存的生命 消散的生命</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>谷田部勝義</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td></td>
<td><p>FATES</p></td>
<td><p>FATES</p></td>
<td><p>兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
高田昌宏</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td></td>
<td><p>剎那之夢</p></td>
<td><p>刹那之夢</p></td>
<td><p><a href="../Page/高橋ナツコ.md" title="wikilink">高橋ナツコ</a><br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td></td>
<td><p>未明之夜</p></td>
<td><p>不明之夜</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
米たにヨシトモ</p></td>
<td><p>西村大樹</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td></td>
<td><p>-{史汀娜}-</p></td>
<td><p>-{史黛菈}-</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>高田昌宏<br />
西澤晋</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td></td>
<td><p>被展現的世界</p></td>
<td><p>被揭發的世界</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
須永司</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td></td>
<td><p>惡夢</p></td>
<td><p>惡夢</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>吉村章</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td></td>
<td><p>混沌之初</p></td>
<td><p>混沌的前兆</p></td>
<td><p>高橋ナツコ<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
米たにヨシトモ</p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td></td>
<td><p>-{亞斯蘭}-逃亡</p></td>
<td><p>-{阿斯蘭}-脫逃</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>西村大樹</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td></td>
<td><p>雷鳴的黑夜</p></td>
<td><p>雷鳴之夜</p></td>
<td><p>大野木寬<br />
吉野弘幸<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
鳥羽聰</p></td>
<td><p><a href="../Page/久保山英一.md" title="wikilink">久保山英一</a></p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td></td>
<td><p>新的旗號</p></td>
<td><p>新的旗幟</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>西澤晋</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td></td>
<td><p>天空的-{基拉}-</p></td>
<td><p>天空的-{煌}-</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
米たにヨシトモ<br />
福田己津央</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td></td>
<td><p>黃金的意志</p></td>
<td><p>黃金的意志</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>西澤晋<br />
高田昌宏</p></td>
<td><p><a href="../Page/いとがしんたろー.md" title="wikilink">いとがしんたろー</a></p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td></td>
<td><p>抑制</p></td>
<td><p>Refrain</p></td>
<td><p><a href="../Page/小倉史科.md" title="wikilink">小倉史科</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td></td>
<td><p>自由與正義</p></td>
<td><p>自由與正義</p></td>
<td><p>大野木寬<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
西澤晋<br />
福田己津央</p></td>
<td><p>鳥羽聰</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td></td>
<td><p>反擊之聲</p></td>
<td><p>反擊的聲音</p></td>
<td><p>大野木寬<br />
高橋ナツコ<br />
兩澤千晶</p></td>
<td><p>米たにヨシトモ<br />
西澤晋<br />
福田己津央</p></td>
<td><p>西村大樹</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td></td>
<td><p>兩個-{莉古絲}-</p></td>
<td><p>兩個-{拉克絲}-</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
西澤晋<br />
福田己津央</p></td>
<td><p>久保山英一</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td></td>
<td><p>變革的序曲</p></td>
<td><p>變革的序曲</p></td>
<td><p>森田繁<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
西澤晋<br />
米たにヨシトモ<br />
福田己津央</p></td>
<td><p>吉村章</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td></td>
<td><p>真實之歌</p></td>
<td><p>真實之歌</p></td>
<td><p>野村祐一<br />
兩澤千晶</p></td>
<td><p>鳥羽聰<br />
西澤晋<br />
福田己津央</p></td>
<td><p>高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td></td>
<td><p>-{美雅}-</p></td>
<td><p>-{蜜雅}-</p></td>
<td><p>兩澤千晶</p></td>
<td><p>西澤晋<br />
高田昌宏<br />
福田己津央</p></td>
<td><p>西山明樹彥</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td></td>
<td><p>往新的世界</p></td>
<td><p>邁向新世界</p></td>
<td><p>吉野弘幸<br />
兩澤千晶</p></td>
<td><p>高田昌宏<br />
鳥羽聰<br />
西澤晋<br />
福田己津央</p></td>
<td><p>西村大樹</p></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td></td>
<td><p>-{尼爾}-</p></td>
<td><p>-{雷}-</p></td>
<td><p>鳥羽聰<br />
西澤晋<br />
米たにヨシトモ<br />
福田己津央</p></td>
<td><p>鳥羽聰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td></td>
<td><p>最後的力量</p></td>
<td><p>最後之力</p></td>
<td><p>兩澤千晶</p></td>
<td><p>西澤晋<br />
米たにヨシトモ<br />
鳥羽聰<br />
高田昌宏<br />
福田己津央</p></td>
<td><p>福田己津央<br />
高田昌宏</p></td>
</tr>
<tr class="odd">
<td><p>Final Plus</p></td>
<td></td>
<td><p>機動戰士終極特種命運</p></td>
<td><p>被選擇的未来</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## FINAL PLUS

在2005年10月1日，原作最終回播放完畢後，觀眾批評草草了事，當時亦有傳出完整的結局含在[DVD推出](../Page/DVD.md "wikilink")。2005年12月25日播出本作的特别节目《FINAL
PLUS》，在原先的最終回PHASE-50基礎上再多出約15分鐘內容，主要交代[真·飛鳥與](../Page/真·飛鳥.md "wikilink")[煌·大和的握手言和](../Page/煌·大和.md "wikilink")，其中同行的[阿斯蘭·薩拉身旁的女伴為](../Page/阿斯蘭·薩拉.md "wikilink")[美玲·霍克](../Page/美玲·霍克.md "wikilink")，以及[拉克絲·克萊因步入PLANT最高評議會等新結局引發不少討論](../Page/拉克絲·克萊因.md "wikilink")。[香港地區](../Page/香港.md "wikilink")[無綫電視則譯為](../Page/無綫電視.md "wikilink")《**機動戰士終極特種命運**》，2007年7月14日和2007年7月21日於[無綫電視翡翠台播放](../Page/無綫電視翡翠台.md "wikilink")。台灣地區則未曾播映過。之後推出的總集編第四部「自由的代償」中，結局又增加些許新內容。

## 特別版四部作

「機動戦士ガンダム SEED DESTINY SPECIAL
EDITION」將全51話的原作再剪輯並追加新作畫面而成全四部的OVA總集編，包括特別版I《破碎的世界》、特別版II《各自的劍》、特別版III《命運的業火》和特別版IV《自由的代價》。結局部份新增身著ZAFT軍白衣登場的煌‧大和。

| 話數     | 日文原名   | 香港官方譯名  | 台灣中文譯名 |
| ------ | ------ | ------- | ------ |
| 特別版I   | 砕かれた世界 | 破碎的世界   | 破碎的世界  |
| 特別版II  | それぞれの剣 | 他們各自的劍  | 他們各自的劍 |
| 特別版III | 運命の業火  | 命運的地獄之火 | 命運的業火  |
| 特別版IV  | 自由の代償  | 自由的代價   | 自由的代價  |

## HD Remaster

為迎接機動戰士鋼彈SEED十周年的2012年，官方於2011年8月26日啟動「HD Remaster
Project（高畫質再製企劃）」。2012年11月25日機動戰士GUNDAM
SEED的HD版在最終回播映完畢後，同時宣布機動戰士Gundam SEED DESTINY HD
Remaster正式啟動。官方網站亦發表一張有著「DESTINY IS
COMING」字樣的宣傳圖；真·飛鳥立於正中，背後是熊熊烈焰幻化而成的命運鋼彈臉部特寫。

網路放送於2013年3月29日開始，TV則於2013年4月7日起在日本BS11開始播映。HD
Remaster版刪除原本TV版撥放的第41話，原本42～50話的編號減1，Final
Plus成為第50話完結篇。Blu-ray版會在HD Remaster電視版方面再進行新規作畫修正。

## 爭議

### 主人翁定位的問題

### 兼用卡與總集編氾濫的問題

SEED系列作品中擔任劇本、系列構成等重要工作的兩澤千晶，於前作後期（2002年）發現卵巢與子宮頸的腫瘤，本作製作開始（2004年）後在劇組和醫院之間頻繁往返；早被業界認為寫稿緩慢、遲筆的兩澤更無法如期繳出劇本。據稱本作每一集的製作經費達3300萬日幣，但因劇本完成後僅有8週時間進行繪製作業（正常為16週左右），為了趕上播映時間只好犧牲製作細節，在繪製和分鏡方面被迫選擇大量使用重複動作模組的剪輯鏡頭（兼用CUT，俗稱[兼用卡](../Page/兼用卡.md "wikilink")）。另外由於監督不周，一些兼用卡錯置的情況也引人詬病；特別是本作的戰鬥畫面大半以兼用卡剪接而成，同時總集編（整整一集內容泰半以兼用卡配上旁白後，重新編輯成類似回憶錄的演出）的次數之多令人髮指，亦嚴重打擊本作收視率。直至本作於2013年推出HD
Remaster版本，部份使用過量的集數，才以新作畫面代替兼用卡。

### 沿用舊作品機械設定的問題

本作以向UC時代致敬為另一賣點，除了以聲演「赤之彗星」[夏亞·阿茲納布爾為人熟悉](../Page/夏亞·阿茲納布爾.md "wikilink")[池田秀一聲演吉伯特](../Page/池田秀一.md "wikilink")·杜蘭朵、曾聲演[基絲莉亞·薩比的](../Page/薩比家.md "wikilink")[小山茉美聲演妲莉雅](../Page/小山茉美.md "wikilink")·古拉迪斯，機械設定也大量沿用過去不同年代的舊[GUNDAM系列作品機體設定](../Page/GUNDAM系列作品.md "wikilink")，例如：

  - [U.C.纪元的](../Page/U.C..md "wikilink")[ZAKU](../Page/MS-05系列机动战士.md "wikilink")、[ZAKU
    II及](../Page/MS-06系列机动战士.md "wikilink")[-{zh-hans:古夫;zh-hk:老虎;zh-tw:古夫;}-均經過外觀上的修改](../Page/MS-07系列机动战士.md "wikilink")，成了[-{zh-hans:扎古;zh-hk:渣古;zh-tw:薩克;}-戰士](../Page/Zaku_\(Cosmic_Era\).md "wikilink")、-{zh-hans:扎古;zh-hk:渣古;zh-tw:薩克;}-幽靈及[古夫烈燄等以新機體的形式登場](../Page/ZGMF-2000_Gouf_Ignited.md "wikilink")。
  - [ZGMF-XX09T Dom
    Trooper的外觀及名稱沿自](../Page/ZGMF-XX09T_Dom_Trooper.md "wikilink")「Dom
    Tropen」，其使用的「喷射气流攻击」战術名稱也直接移植(攻擊方法不同，UC系的德姆是交叉波狀攻勢，本作則是真的噴出粒子氣流)。
  - [曉肩上的](../Page/ORB-01_Akatsuki.md "wikilink")[漢字](../Page/漢字.md "wikilink")、金色外觀等獨特外觀與百式類似。
  - [GFAS-X1 DESTROY
    GUNDAM的外觀與](../Page/GFAS-X1_DESTROY_GUNDAM.md "wikilink")[MRX-009
    Psycho
    Gundam相似](../Page/MRX-009_Psycho_Gundam.md "wikilink")、MA型態也與[MA-08
    Big
    Zam相近](../Page/MA-08_Big_Zam.md "wikilink")。本機處女戰在德國漢堡大肆破壞，也和[MRX-009
    Psycho
    Gundam在新香港](../Page/MRX-009_Psycho_Gundam.md "wikilink")(屯門)在市區開戰的情節有相當程度上的雷同。
  - [MVF-M11C
    Murasame村雨鋼彈](../Page/MVF-M11C_Murasame.md "wikilink")'的變形機構與[MSZ-006系列機動戰士相似](../Page/MSZ-006系列機動戰士.md "wikilink");
    「村雨」一名來自[機動戰士Z
    GUNDAM迪坦斯的新類人研究中心](../Page/機動戰士Z_GUNDAM.md "wikilink")「村雨研究所」（不過正式設定上是取自日本名刀的名稱）。

作為機體設計者的大河原邦男在之後的訪談中被問及為什麼不作創新時，曾說明自己也想要創新，但監督堅持要沿用與U.C.系列機體類似的外觀風格，創新設計只能是次要機體（例如Windam與巴比）。

## 其他資料

「GUNDAM
SEED」監製[福田己津央](../Page/福田己津央.md "wikilink")，曾負責多部動畫作品，例如[GEAR戰士電童](../Page/GEAR戰士電童.md "wikilink")、[閃電霹靂車等作品](../Page/閃電霹靂車.md "wikilink")，在「SEED」中亦曾多次引用這些作品的設定。

「DESTINY」第8集中，阿斯蘭所駕駛的跑車、以及第21集飛鳥真駕駛的機車，分別為[閃電霹靂車SIN中跑車](../Page/閃電霹靂車.md "wikilink")「GSX-Neo」及「[閃電霹靂車](../Page/閃電霹靂車.md "wikilink")」主角風見隼人的機車。而第8集中亦有出現「閃電霹靂車」曾出現過的別墅。第35集更出現了風見隼人同Asurada
AFK-11贏得2016年世界冠軍的海報。

### 播放電視台

## 參考文獻

## 外部連結

  - [(Gundam Seed Destiny日文官方網站)](http://www.gundam-seed-d.net/)

  - [Gundam Perfect Web（日本機動戰士官方網站）](http://www.gundam.channel.or.jp/)

  - [Gundam World
    Web（香港機動戰士官方網站）](https://web.archive.org/web/20060830174702/http://www.g-world.com.hk/)

  - [高达中文机体资料库](https://web.archive.org/web/20150518074701/http://985.so/j7Ah)

[\*](../Category/GUNDAM_SEED.md "wikilink")
[Category:2004年日本電視動畫](../Category/2004年日本電視動畫.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:圓桌騎士題材作品](../Category/圓桌騎士題材作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:Comic BomBom](../Category/Comic_BomBom.md "wikilink")
[Category:月刊Magazine Z](../Category/月刊Magazine_Z.md "wikilink") [Seed
Destiny](../Category/GUNDAM系列.md "wikilink")

1.  [Gundam World Web
    (HK)](http://g-world.com.hk//default/revamp/library/seeddestiny.jsp)

2.  博英社DVD