**機動戰士GUNDAM SEED MSV**是[機動戰士GUNDAM
SEED的正式外傳作品之一](../Page/機動戰士GUNDAM_SEED.md "wikilink")，與「ASTRAY」系列有著微妙的關聯、補完劇情。「MSV」是全寫為MOBILE
SUIT VARIATION，意思是指MS的變化，顯示出存在於該時空背景下，但在正傳動畫中可能未出現的MS。

## OVA作品

OVA作品有4份，其中一套為[機動戰士高達SEED的外傳印象曲](../Page/機動戰士高達SEED.md "wikilink")，其餘動畫都主要講述迷惘高達紅、藍、金色三機的部份行蹤。

  - **機動戰士GUNDAM SEED MSV印象曲 - Zips** (OVA) [T.M.
    Revolution](../Page/T.M._Revolution.md "wikilink")

<!-- end list -->

  -
    先以地球聯邦軍攻擊Z.A.F.T.基地作開頭，印象曲中亦出現了大量外傳故事人物。

<!-- end list -->

  - **機動戰士GUNDAM SEED MSV - 迷惘高達紅色機** (OVA)
  - **機動戰士GUNDAM SEED MSV - 迷惘高達藍色機** (OVA)
  - **機動戰士GUNDAM SEED MSV - 迷惘高達金色機** (OVA)

## 漫畫作品

*MSV漫畫作品*是刊載於[Gundam Game
ACE](../Page/Gundam_Game_ACE.md "wikilink")。內容雖短，但亦可作為其他篇章的前傳。

  - **機動戰士GUNDAM SEED MSV - 黃昏的魔彈** (漫畫)
    [鴇田洸一](../Page/時田洸一.md "wikilink")

<!-- end list -->

  -
    於[機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")(漫畫)單行本第三冊刊載。
    劇情以未獲取迷茫高達藍色機的叢雲
    劾攻擊[Z.A.F.T.的補給基地為基礎](../Page/Z.A.F.T..md "wikilink")，相當於機動戰士GUNDAM
    SEED ASTRAY的前傳。

<!-- end list -->

  - **機動戰士GUNDAM SEED MSV - 開膛手愛德** (漫畫)
    [鴇田洸一](../Page/時田洸一.md "wikilink")

<!-- end list -->

  -
    於Gundam Game ACE Vol.2和[機動戰士GUNDAM SEED DESTINY
    ASTRAY](../Page/機動戰士GUNDAM_SEED_DESTINY_ASTRAY.md "wikilink")(漫畫)單行本第一冊刊載。
    劇情以[PS2遊戲軟體](../Page/PS2.md "wikilink")「機動戰士高達GUNDAM
    SEED無盡的明日」的「MISSION MODE 血染維多利亞宇宙港」為基礎，相當於機動戰士GUNDAM
    SEED DESTINY ASTRAY的前傳。

## MSV戰記

*機動戰士GUNDAM SEED MSV戰記*與*機動戰士GUNDAM SEED DESTINY MSV戰記*
是[Sunrise和](../Page/Sunrise.md "wikilink")[Studio
Orphee的](../Page/Studio_Orphee.md "wikilink")[千葉智宏的監修下](../Page/千葉智宏.md "wikilink")，由[Studio
Nue的森田繁編織的短篇故事](../Page/Studio_Nue.md "wikilink")，配合使用各模型範例作品拍的情景照片而誕生的外傳。該戰記是以年表的記述與對話等把TV版動畫本篇沒有描寫到的不同時間軸和其他戰場作為舞台，當中在另外傳「ASTRAY」方面的活躍人物亦有登場。

  - **機動戰士GUNDAM SEED MSV戰記** (情景小說) [森田繁](../Page/森田繁.md "wikilink")

<!-- end list -->

  -
    於[HJ雜誌和](../Page/HJ雜誌.md "wikilink") 機動戰士GUNDAM SEED
    DESTINY模型專輯Vol.2 刊載。
    共有7話。

<!-- end list -->

  - **機動戰士GUNDAM SEED DESTINY MSV戰記** (情景小說)
    [森田繁](../Page/森田繁.md "wikilink")

<!-- end list -->

  -
    於[HJ雜誌和](../Page/HJ雜誌.md "wikilink") 機動戰士GUNDAM SEED
    DESTINY模型專輯Vol.2 刊載。
    共有3話。

## 登場人物

### 地球聯邦軍

  - 摩根·雪佛蘭, Morgan Chevalier

<!-- end list -->

  -
    绰号“月下狂犬”

<!-- end list -->

  - 珍·休斯頓, Jane Houston

<!-- end list -->

  -
    绰号“白鲸”

<!-- end list -->

  - 蕾娜·伊梅里亞, Rena Imelia

<!-- end list -->

  -
    绰号“乱樱”

<!-- end list -->

  - 愛德華·哈勒森, Edward Harrelson

<!-- end list -->

  -
    绰号“开膛手爱德”

<!-- end list -->

  - 穆·拉·布拉加, Mu La Flaga

<!-- end list -->

  -
    绰号“[恩底弥翁之鹰](../Page/恩底弥翁.md "wikilink")”

### [Z.A.F.T.](../Page/Z.A.F.T..md "wikilink")

  - 志保·哈尼夫斯, Shiho Hahnenfuss

<!-- end list -->

  -
    绰号“凤仙花”

<!-- end list -->

  - 寇特尼·歇洛尼姆特

<!-- end list -->

  - 米哈爾·寇斯特, Mikhail Coast

<!-- end list -->

  -
    绰号“医生”

<!-- end list -->

  - 米蓋爾·艾曼, Miguel Aiman

<!-- end list -->

  -
    绰号“黄昏的魔弹”

<!-- end list -->

  - 安德列·渥特菲德, Andrew Waltfeld

<!-- end list -->

  -
    绰号“沙漠之虎”

<!-- end list -->

  - 葛德·威亚

<!-- end list -->

  -
    绰号“英雄”

### 三艦同盟

  - 巴瑞·何, Barry Ho

<!-- end list -->

  -
    绰号“拳神”

<!-- end list -->

  - 姜·凱利, Jean Carry

<!-- end list -->

  -
    绰号“闪耀凶星‘J’”

### 蛇尾

  - 叢雲 劾（CV：井上和彥）

<!-- end list -->

  -
    傭兵隊「蛇尾」隊長，調整者，曾被聯合軍用作獲取調整者數據，之後為擺脫藥物控制逃走，並開展傭兵生涯。重視同伴，實際上其超卓駕駛技術是來自實戰經驗。因此他亦會按照戰局需要，改造專用MS藍色Astray。

## 登場機體

### 地球聯邦軍

  - [GAT-X399/Q](../Page/GAT-X399/Q.md "wikilink")
  - [GAT-SO2R](../Page/GAT-SO2R.md "wikilink")

### Z.A.F.T.

  - [ZGMF-X999A](../Page/ZGMF-X999A.md "wikilink")
  - [ZGMF-1000/AAL](../Page/ZGMF-1000/AAL.md "wikilink")
  - [XMF-P192P](../Page/XMF-P192P.md "wikilink")
  - [ZGMF-1000/R4](../Page/ZGMF-1000/R4.md "wikilink")
  - [ZGMF-X56S/θ](../Page/ZGMF-X56S/θ.md "wikilink")
  - [ZGMF-X101S](../Page/ZGMF-X101S.md "wikilink")
  - [ZGMF-X2000CQGB\&S](../Page/ZGMF-X2000CQGB&S.md "wikilink")
  - [ZGMF-X3000Q](../Page/ZGMF-X3000Q.md "wikilink")

### 三艦同盟

  - [ZGMF-XX09T](../Page/ZGMF-XX09T.md "wikilink")
  - [MVF-M12](../Page/MVF-M12.md "wikilink")

[Category:GUNDAM SEED](../Category/GUNDAM_SEED.md "wikilink")