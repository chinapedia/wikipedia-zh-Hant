《**機動戰士GUNDAM 逆襲的夏亞**》（，*Mobile Suit Gundam: Char's
Counterattack*）是[GUNDAM系列作品的劇場版](../Page/GUNDAM系列作品.md "wikilink")，於1988年3月劇場放映。在愛好者之間通常簡稱為「**逆夏**」、「**馬反**」或英文**CCA**。本作也是GUNDAM系列第一次完全新作而非剪輯電視版所成的劇場版動畫。主題歌「Beyond
The Time」起用當時日本最具人氣的樂團之一[TM
NETWORK演唱也成為話題](../Page/TM_NETWORK.md "wikilink")。

## 故事

[宇宙世紀](../Page/宇宙世紀.md "wikilink")0093年，成為[新吉翁總帥的](../Page/新吉翁.md "wikilink")[夏亞·阿茲納布爾對依然被地球重力所束縛的人類靈魂感到絕望](../Page/夏亞·阿茲納布爾.md "wikilink")，決心以激烈的手段加以肅正。查知此事的[地球聯邦軍王牌駕駛員](../Page/地球聯邦軍.md "wikilink")[阿姆羅·雷及其所屬的](../Page/阿姆羅·雷.md "wikilink")[隆巴納艦隊一起與夏亚所領導的](../Page/隆巴納.md "wikilink")[新吉翁軍展開最後的決戰](../Page/新吉翁.md "wikilink")……

## 作品解說

隨著劇中時代的進步，[機動戰士的設計也隨之進化](../Page/機動戰士.md "wikilink")，從Z GUNDAM的可變形機構，ZZ
GUNDAM的分離合體機能，到了0093年則回歸純粹人形機動兵器的原點，登場的 MS
都以單體的機構為設計主流。0093年的主要新科技是將精神感應介面(Psychomu)的回路封入金屬粒子內並且形成機體框架的[精神感應框體](../Page/精神感應框體.md "wikilink")(Psycho
Frame)，以及繼吉恩系MS之後也裝配了-{zh-hans:飞翔浮游炮;zh-hk:浮游飛翔砲;zh-tw:感應砲;}-(Funnel)的[RX-93
Nu GUNDAM](../Page/RX-93系列机动战士.md "wikilink")。

人物設定與作畫監督本來是希望由[安彥良和擔任](../Page/安彥良和.md "wikilink")，但被拒絕。故延續ZZ的安排而起用[北爪宏幸](../Page/北爪宏幸.md "wikilink")。機械設定的部分，主角機[Nu-GUNDAM的設計是以多人設計比稿的方式決定](../Page/RX-93系列機動戰士.md "wikilink")。由插畫家[鈴木雅久等人提出多張草稿](../Page/鈴木雅久.md "wikilink")，最終交由[出渕裕負責統合](../Page/出渕裕.md "wikilink")。[新吉翁系MS由出渕裕設計](../Page/新吉翁.md "wikilink")，唯一的例外是聯邦量產機[傑鋼由佐山善則製作完稿](../Page/傑鋼.md "wikilink")。其他機械與戰艦的設計則交由[GAINAX負責](../Page/GAINAX.md "wikilink")。最終完成的品質即使以現代的標準來看也具有相當高的完成度。

## 主要登場人物

### [隆德·貝爾](../Page/U.C.紀元GUNDAM作品機動兵器列表.md "wikilink")

  - [阿姆羅·雷](../Page/阿姆羅·雷.md "wikilink")（聲優 -
    [古谷徹](../Page/古谷徹.md "wikilink")）
  - [布萊特·諾亞](../Page/布萊特·諾亞.md "wikilink")（聲優 -
    [鈴置洋孝](../Page/鈴置洋孝.md "wikilink")）

### [新吉翁](../Page/吉翁公國.md "wikilink")

  - [夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")（聲優-
    [池田秀一](../Page/池田秀一.md "wikilink")）
  - [娜娜伊·米格爾](../Page/娜娜伊·米格爾.md "wikilink")（聲優-
    [榊原良子](../Page/榊原良子.md "wikilink")）
  - [葵絲·帕拉雅/葵絲·艾亞](../Page/葵絲·帕拉雅/葵絲·艾亞.md "wikilink")（聲優-
    [川村万梨阿](../Page/川村万梨阿.md "wikilink")）
  - [裘尼·卡斯](../Page/裘尼·卡斯.md "wikilink")（聲優 -
    [山寺宏一](../Page/山寺宏一.md "wikilink")）
  - [雷森·施耐德](../Page/雷森·施耐德.md "wikilink")（聲優 -
    [伊倉一恵](../Page/伊倉一恵.md "wikilink")）

## 登場MS

### 地球聯邦外圍精銳部隊

### [隆德·貝爾艦隊](../Page/U.C.紀元GUNDAM作品機動兵器列表.md "wikilink")

  - [RX-93 ν GUNDAM](../Page/RX-93系列机动战士.md "wikilink")(ν = Nu為最接近讀音)
  - [RX-93-v2 Hi-ν
    Gundam](../Page/RX-93系列机动战士#RX-93-.CE.BD-2_Hi-.CE.BD_Gundam.md "wikilink")（小說版）
  - [-{zh-hans:RGZ-91 利卡兹;zh-hk:RGZ-91 靈格斯;zh-tw:RGZ-91
    靈格斯;}-](../Page/RGZ-91系列机动战士.md "wikilink")
  - [-{zh-hans:RGM-89 捷刚;zh-hk:RGM-89 積根;zh-tw:RGM-89
    傑鋼;}-](../Page/RGM-89系列機動戰士.md "wikilink")
  - [RGM-86R 吉姆III](../Page/RGM-79系列機動戰士.md "wikilink")

### [新吉翁](../Page/吉翁公國.md "wikilink")

  - [AMS-119 基拉·德卡](../Page/AMS-119系列机动战士.md "wikilink")
  - [MSN-03 亞克托·德卡](../Page/MSN-03系列机动战士.md "wikilink")
  - [MSN-04 沙薩比](../Page/MSN-04型機動戰士.md "wikilink")
  - [-{zh-hans:MSN-04II 夜莺; zh-hant:MSN-04II
    夜鶯;}-](../Page/MSN-04型機動戰士.md "wikilink")（小說版）
  - [RMS-116H 民用型高性能薩克](../Page/RMS-106.md "wikilink")
  - [NZ-333 α·阿基爾](../Page/NZ-333型機動堡壘.md "wikilink")

## 主要工作人員

  - 原案：[矢立肇](../Page/矢立肇.md "wikilink")
  - 原作、總監督、腳本：[富野由悠季](../Page/富野由悠季.md "wikilink")
  - 角色設定：[北爪宏幸](../Page/北爪宏幸.md "wikilink")
  - MS設定：[出渕裕](../Page/出渕裕.md "wikilink")
  - 機械設定：[GAINAX](../Page/GAINAX.md "wikilink")（[庵野秀明](../Page/庵野秀明.md "wikilink")、[增尾昭一](../Page/增尾昭一.md "wikilink")）、[佐山善則](../Page/佐山善則.md "wikilink")
  - 作畫監督：[稲野義信](../Page/稲野義信.md "wikilink")、[北爪宏幸](../Page/北爪宏幸.md "wikilink")、、[大森英敏](../Page/大森英敏.md "wikilink")、[小田川幹雄](../Page/小田川幹雄.md "wikilink")、[仙波隆綱](../Page/仙波隆綱.md "wikilink")
  - 音樂：[三枝成彰](../Page/三枝成彰.md "wikilink")

## 主題歌

  - 主題歌「BEYOND THE TIME～メビウスの宇宙を越えて～」
      - 作詞：[小室光子](../Page/小室光子.md "wikilink")　作曲．編曲：[小室哲哉](../Page/小室哲哉.md "wikilink")　演唱：[TM
        NETWORK](../Page/TM_NETWORK.md "wikilink")

## 票房

日本票房11億6000萬日圓。

## 觀眾數量

進場觀眾103萬人

## 相關作品

### 小說

由富野由悠季撰寫的小說版《逆襲的夏亞》共有兩種版本。

其一是在德間書店出版的動畫月刊《Animage》連載，當初題名為『』，文中插畫則由漫畫家[星野之宣擔綱](../Page/星野之宣.md "wikilink")。後來在Animage文庫刊行時，更改書名為《逆襲的夏亞》。之後再改為原標題並且再版，歸屬於系列。

其二是富野最初為電影創作的劇本稿，在內部審查時由於受到「不想讓觀眾看到娶妻生子的阿姆羅」的意見所以並未成為正式劇本，而後富野以此初稿為基礎寫出小說版《逆襲的夏亞～貝托蒂嘉的子嗣》，由發行。中文版則由[尖端出版](../Page/尖端出版.md "wikilink")，翻譯者為[吳建樟
(YOKO)](../Page/吳建樟_\(YOKO\).md "wikilink")。

#### 有聲書

“”

角川書店所屬的所發行的夢幻有聲劇。在一般認知中的《逆襲的夏亞》有劇場版，《貝托蒂嘉的子嗣》跟《HIGH
STREAMER》三種版本存在，而此有聲書就是以《貝托蒂嘉的子嗣》為藍本而製作，其中最大的特徵就是阿姆羅的戀人[貝托蒂嘉·伊爾瑪的登場](../Page/貝托蒂嘉·伊爾瑪.md "wikilink")，其他劇中的人物與MS名稱也是依照原小說置換。

### 漫畫

在電影上映後，於1988年4月及5月由所畫的漫畫版在講談社雜誌上連載。故事的細節與劇場版本身有若干不同。本作於1999年大都社復刊的《[機動戰士GUNDAM
ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")》漫畫版的第二卷當中重新收錄。

另外1998年10月至1999年2月由[鴇田洸一](../Page/鴇田洸一.md "wikilink")所畫的漫畫版也於上連載，於1999年3月發售單行本。這個版本的故事與劇場版是相同的。

### 映像作品

在3DCG動畫[GUNDAM
EVOLVE](../Page/GUNDAM_EVOLVE.md "wikilink")5當中，以[CG與手繪動畫混合的手法所表現的](../Page/CG.md "wikilink")[RX-93
νGUNDAM與](../Page/RX-93系列机动战士.md "wikilink")[α阿基爾](../Page/α阿基爾.md "wikilink")（α-AZIEL）的戰鬥場面與對話（腳本為富野本人撰寫），與劇場版有相當大的不同。可算是另一個if故事。

## 評價

本作品是[機動戰士GUNDAM所開展的宇宙世紀故事的延續](../Page/機動戰士GUNDAM.md "wikilink")，記述了0093年第二次[新吉翁抗爭的結束](../Page/新吉翁.md "wikilink")，角色以初代兩大主角[阿姆羅·雷與](../Page/阿姆羅·雷.md "wikilink")
[夏亞·阿茲納布爾為主](../Page/夏亞·阿茲納布爾.md "wikilink")。在作品當中大量的使用「[殖民地投下](../Page/質量兵器.md "wikilink")」、「地球圈」和「[新人類](../Page/新人類.md "wikilink")」等等，並延續了初代GUNDAM以來的世界觀，亦為兩位主角的最後決戰畫上句號。

之後的宇宙世紀作品，如[機動戰士GUNDAM
F91以及](../Page/機動戰士GUNDAM_F91.md "wikilink")[機動戰士V
GUNDAM雖繼續沿用宇宙世紀年號](../Page/機動戰士V_GUNDAM.md "wikilink")，但劇情已經與0079/Z/ZZ/逆襲等四部作完全無關。又因為，機動戰士系列主要兩大角色在同歸於盡([機動戰士GUNDAM
UC稱兩者均為](../Page/機動戰士GUNDAM_UC.md "wikilink")「失蹤」)；故絕大部分舊作愛好者將本作視為宇宙世紀GUNDAM正史的終結。在本作品之後的0080[機動戰士GUNDAM0080：口袋裡的戰爭和](../Page/機動戰士GUNDAM_0080：口袋裡的戰爭.md "wikilink")0083[機動戰士GUNDAM0083：星塵回憶等視為補強原作之故事](../Page/機動戰士GUNDAM_0083：Stardust_Memory.md "wikilink")。

## 外部連結

  -
[Category:宇宙世紀](../Category/宇宙世紀.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:Comic BomBom](../Category/Comic_BomBom.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Char](../Category/GUNDAM系列.md "wikilink")