**機殼改裝**（[英文](../Page/英文.md "wikilink")：**Case
Modding**），指的是對於[電腦](../Page/電腦.md "wikilink")[機殼的改裝](../Page/機殼.md "wikilink")。將機殼改裝成非制式的模樣。許多人，特別是[硬體](../Page/硬體.md "wikilink")[發燒友以機殼改裝來展現他們的](../Page/發燒友.md "wikilink")[美學](../Page/美學.md "wikilink")。機殼改裝者通常會舉辦[LAN
party或網路聚會來展示他們的大作](../Page/LAN_party.md "wikilink")。

## 歷史

當[個人電腦成為主流之後](../Page/個人電腦.md "wikilink")，電腦機殼的顏色就以[米白色為主](../Page/白色.md "wikilink")，稱之為「白箱」（beige
box）。有人不滿於這樣呆板的機殼，於是就開始改造他們自己的機殼。機殼改裝在[蘋果電腦的](../Page/蘋果電腦.md "wikilink")[iMac發表之後](../Page/iMac.md "wikilink")，更為盛行。在電腦的價格日趨平民化之後，電腦廠商為了讓自己的產品更有賣點，於是開始有[黑色](../Page/黑色.md "wikilink")、[紅色等多采多姿的](../Page/紅色.md "wikilink")[顏色機殼](../Page/顏色.md "wikilink")。如今機殼改裝已成為電腦產業的一環之一，電腦賣店也開始出售各種改裝工具及用品，且有機殼改裝的專門[網站及比賽出現](../Page/網站.md "wikilink")。

## 常見的機殼改裝方式

  - **開窗改裝**：在機殼上開洞，通常以左側板為主（因為面積大較有發揮的空間，且看得到電腦內部）。在機殼改裝剛興起時，改裝者要以電動工具自行將機殼開洞。現在有機殼廠商將已事先開好洞的機殼出售給改裝者以節省改裝時間。更有廠商出售以透明材質做成的全[透明機殼](../Page/透明.md "wikilink")。
  - **燈光改裝**：改裝者將燈光放入機殼內外，大部份以冷光燈管及高亮度[發光二極管來改裝](../Page/發光二極管.md "wikilink")，甚至加入閃爍等特殊效果。有些機殼內的散熱風扇將外框及扇葉改成透明材質，並裝上LED，以加強改裝效果。還有一些電腦內的線材也加上燈光是塗有[螢光物質的](../Page/螢光.md "wikilink")。燈光改裝通常與開窗改裝是相輔相成的。
  - **散熱改裝**：電腦[超頻的愛好者通常會針對散熱做改裝](../Page/超頻.md "wikilink")，從基本的開孔加裝散熱風扇到改用水冷式散熱都有，改成水冷式散熱改裝有個重要的優點就是可減少電腦發出的噪音。
  - **迷你改裝**：將電腦機殼最小化，利用現有的規格配備例如Mini-ITX、Nano-ITX基板撘配迷你機殼和低高度（Low
    profile）的介面卡以及低瓦數的小體積的電源供應器，利用機殼有限寬間來完成。
  - **利用其他设备的外壳进行改造**：例如有些人使用废旧的[DVD](../Page/DVD.md "wikilink")、[VCD](../Page/VCD.md "wikilink")、[录像机外壳](../Page/录像机.md "wikilink")，修改内部结构，并将计算机的配件装入其中，改造成电脑机壳。

## 少見的機殼改裝方式

[Casemodding_microwave.JPG](https://zh.wikipedia.org/wiki/File:Casemodding_microwave.JPG "fig:Casemodding_microwave.JPG")內的有趣改裝。\]\]

  - **週邊設備改裝**：有些人不滿於制式化的電腦週邊設備，因此就有週邊設備的改裝。常用的手法是將週邊塗上與機殼同色，以及將原有的燈光改成不同顏色。
  - **另類散熱改裝**：有些超頻者只為了破紀錄，選擇更極端的散熱改裝。常見的是加裝冰箱用[壓縮機加強冷卻效果](../Page/壓縮機.md "wikilink")，更有人使用[液態氮做為冷卻](../Page/液態氮.md "wikilink")。這樣的改裝可能會增加電腦噪音及耗電，且价格高昂，但為了追求極致效能的玩家通常樂此不疲。
  - **自製機殼改裝**：有些人自己設計及製作機殼，甚至將機殼做得像玩具熊、木箱、木架等。還有人模仿早期電腦的造型（如蘋果電腦）自製機殼。
  - **整合週邊改裝**：有人為了讓桌上型電腦更好攜帶，將[鍵盤](../Page/鍵盤.md "wikilink")、螢幕等週邊，連同主機板一起放入公事包或皮箱、鋁箱等。

## 外部連結

  - [非藏网（简体中文）](http://www.pcang.com) - 电子、电脑硬件收藏网。
  - [Tom's
    硬體指南（正體中文）](https://web.archive.org/web/20100501185643/http://www.tomshardware.tw/)
  - [Using the case as a heat
    sink](http://www.bestpricecomputers.co.uk/reviews/TNN500/)
  - [bit-tech.net](http://www.bit-tech.net)
  - [Cooler Master](http://www.coolermaster.com) - 知名機殼廠商

[Category:次文化](../Category/次文化.md "wikilink")
[Category:计算机硬件调校](../Category/计算机硬件调校.md "wikilink")