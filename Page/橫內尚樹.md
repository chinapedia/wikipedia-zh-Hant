**橫內尚樹**（，1966年12月3日 -
），[日本人](../Page/日本.md "wikilink")，居住在[北海道的男性](../Page/北海道.md "wikilink")[漫畫家](../Page/漫畫.md "wikilink")，講談社旗下漫畫月刊[Comic
BomBom](../Page/Comic_BomBom.md "wikilink")（已休刊）作者。

## 作品

  - [霹靂酷樂貓](../Page/霹靂酷樂貓.md "wikilink")（日文：サイボーグクロちゃん，Comic BomBom
    1997年9月号～2001年12月号連載，2015年11月份推出新裝版）

<!-- end list -->

  - ウッディケーン（Comic BomBom 2002年3月号～2003年5月号連載 全3集）

<!-- end list -->

  - （Comic BomBom 2002年12月号～2005年12月号連載 原作：橫內尚樹，漫畫：內田淳太（）全3集）

<!-- end list -->

  - [霹靂酷樂貓
    格林機關槍再次上膛](../Page/霹靂酷樂貓_格林機關槍再次上膛.md "wikilink")（日文：サイボーグクロちゃんガトリングセレクション
    リローデッド，講談社、2012年7月23日）

<!-- end list -->

  - 宇宙のガズゥ（2011年於繪圖網站PIXIV不定期刊載的網路漫畫，2016年講談社宣佈單行本化）

## 其他

  - 2009年8月擔任[迪士尼機器人短篇動畫](../Page/迪士尼.md "wikilink")[FIRE
    BALL](../Page/火球_\(动画\).md "wikilink")（ファイヤーボール）官方部落格[LOGO設計以及NEW](../Page/LOGO.md "wikilink")
    TYPE月刊ファイヤーボール的[四格漫畫繪製](../Page/四格漫畫.md "wikilink")，目前最新作品是在PIXIV公開的漫畫『』，2012年1月出版了霹靂酷樂貓漫畫精選集『』，第二部霹靂酷樂貓漫畫精選集『』於2012年7月發行。

## 外部連結

  - [FIRE
    BALL官方部落格](http://www.disneychannel.jp/dc/program/anime/fireball/blog/)
  - [橫內尚樹的PIXIV作品頁面](http://www.pixiv.net/member.php?id=93291)
  - [サイボーグクロちゃん官方TWITTER](http://twitter.com/cykuroPR)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")