**橫瀾燈塔**又稱**橫瀾島燈塔**（），是位於[香港](../Page/香港.md "wikilink")[蒲台群島](../Page/蒲台群島.md "wikilink")[橫瀾島的瀾尾的](../Page/橫瀾島.md "wikilink")[燈塔](../Page/燈塔.md "wikilink")，現為[法定古蹟](../Page/香港法定古蹟.md "wikilink")。\[1\]

## 歷史

橫瀾燈塔於1893年由上海[大清皇家海關總稅務司聘請巴黎Barbier](../Page/大清.md "wikilink") and
Co公司建造、啟用和管理，並取代了[英國於](../Page/英國.md "wikilink")[香港島東南建造的](../Page/香港島.md "wikilink")[鶴咀燈塔的功能](../Page/鶴咀燈塔.md "wikilink")。1898年，根據《[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")》，屬於[新界一部分的橫瀾島亦須租借給英國](../Page/新界.md "wikilink")。因此，燈塔於1901年1月1日起交由香港政府接管繼續操作\[2\]。燈塔於[第二次世界大戰期間遭受嚴重破壞](../Page/第二次世界大戰.md "wikilink")，所以1945年曾進行修葺工程。1989年8月，燈塔改為自動化操作。2000年12月29日，[香港康樂及文化事務署屬下的](../Page/香港康樂及文化事務署.md "wikilink")[香港古物古蹟辦事處宣佈將橫瀾燈塔列為](../Page/香港古物古蹟辦事處.md "wikilink")[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")，同期列入的還有[燈籠洲燈塔](../Page/燈籠洲燈塔.md "wikilink")。

## 特色

橫瀾燈塔是當時亞洲最先進的燈塔，設有一座訊號燈，並以[石油作為燃料推動運作](../Page/石油.md "wikilink")。燈塔的旋轉的照明儀器，則浮於[水銀之上](../Page/水銀.md "wikilink")。

此燈塔現由[海事處管理及操作](../Page/海事處.md "wikilink")。基於保安理由及避免影響日常運作，燈塔並不對外開放。

## 參考

## 參見

  - [香港法定古蹟](../Page/香港法定古蹟.md "wikilink")
  - [鶴咀燈塔](../Page/鶴咀燈塔.md "wikilink")
  - [青洲燈塔](../Page/青洲燈塔.md "wikilink")
  - [燈籠洲燈塔](../Page/燈籠洲燈塔.md "wikilink")
  - [香港天文台](../Page/香港天文台.md "wikilink")

## 外部連結

  - [康樂及文化事務署 - 橫瀾燈塔](http://www.amo.gov.hk/b5/monuments_71.php)

[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")
[Category:香港燈塔](../Category/香港燈塔.md "wikilink")
[Category:蒲台群島](../Category/蒲台群島.md "wikilink")

1.  [Antiquities and Monuments Office:Waglan
    Lighthouse](http://www.amo.gov.hk/en/monuments_71.php)
2.