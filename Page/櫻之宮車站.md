**櫻之宮車站**（）是一由[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")，位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[都島區中野町五丁目](../Page/都島區.md "wikilink")。櫻之宮車站是JR西日本所屬的[大阪環狀線沿線的車站之一](../Page/大阪環狀線.md "wikilink")，也是大阪近郊鐵路路線群[都市網路](../Page/都市網路.md "wikilink")（，Urban
Network）所屬的車站，根據[JR的](../Page/JR.md "wikilink")[特定都區市內制度](../Page/特定都區市內制度.md "wikilink")，櫻之宮車站被劃分為「大阪市內」的車站之一。車站的命名源自於附近的[櫻宮神社](../Page/櫻宮神社.md "wikilink")，雖然日文漢字的寫法與車站名不大相同，但發音（）並無分別。

今日的櫻之宮雖然只有大阪環狀線一條路線通過，但在1913年之前，原本還存在有一條由本站經[網島車站](../Page/網島車站.md "wikilink")（已於1913年廢站）連接至[放出車站的路線](../Page/放出車站.md "wikilink")，名為[櫻之宮線](../Page/櫻之宮線.md "wikilink")（）。除了一般的通勤列車之外，一些包括[關空快速與](../Page/關空快速.md "wikilink")[紀州路快速等車種的快速列車](../Page/紀州路快速.md "wikilink")，皆有在本站停車。

## 車站結構

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[高架車站](../Page/高架車站.md "wikilink")，由於沒有[轉轍器與絕對信號機](../Page/轉轍器.md "wikilink")，被分類為停留所。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>大阪環狀線</p></td>
<td><p>內環</p></td>
<td><p><a href="../Page/大阪車站.md" title="wikilink">大阪</a>、<a href="../Page/西九條車站.md" title="wikilink">西九條方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>外環</p></td>
<td><p><a href="../Page/京橋站_(大阪府).md" title="wikilink">京橋</a>、<a href="../Page/鶴橋車站.md" title="wikilink">鶴橋</a>、<a href="../Page/天王寺車站.md" title="wikilink">天王寺方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 西日本旅客鐵道

    大阪環狀線

      -

        大和路快速、區間快速、關空快速、紀州路快速、快速、直通快速、普通

          -
            [京橋](../Page/京橋站_\(大阪府\).md "wikilink")（JR-O08）－**櫻之宮（JR-O09）**－[天滿](../Page/天滿站.md "wikilink")（JR-O10）

### 曾經存在的路線

  - [內閣鐵道院](../Page/鐵道省.md "wikilink")（國有鐵道）
    櫻之宮線（1913年11月15日廢除路段）
      -

          -

            －**櫻之宮**

## 外部連結

  - [櫻之宮車站（JR西日本）](http://www.jr-odekake.net/eki/top.php?id=0610512)

[Kuranomiya](../Category/日本鐵路車站_Sa.md "wikilink")
[Category:都島區鐵路車站](../Category/都島區鐵路車站.md "wikilink")
[Category:大阪環狀線車站](../Category/大阪環狀線車站.md "wikilink")
[Category:1898年啟用的鐵路車站](../Category/1898年啟用的鐵路車站.md "wikilink")
[Category:以宗教場所命名的鐵路車站](../Category/以宗教場所命名的鐵路車站.md "wikilink")