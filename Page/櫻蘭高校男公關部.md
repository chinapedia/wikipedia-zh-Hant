《**櫻蘭高校男公關部**》（），是[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")[葉鳥螺子](../Page/葉鳥螺子.md "wikilink")（）的一部[愛情](../Page/愛情.md "wikilink")[校園](../Page/校園.md "wikilink")[漫畫](../Page/日本漫畫.md "wikilink")，2003年於《[LaLa](../Page/LaLa.md "wikilink")》開始連載。[台灣中文版由](../Page/台灣.md "wikilink")[東立出版社代理](../Page/東立出版社.md "wikilink")，[香港](../Page/香港.md "wikilink")、[澳門中文版由](../Page/澳門.md "wikilink")[天下出版代理](../Page/天下出版.md "wikilink")，單行本全18卷已發售。

## 劇情內容

庶民學生藤岡春緋是一位以優等生身份利用[獎學金進入](../Page/獎學金.md "wikilink")[東京都](../Page/東京都.md "wikilink")[文京區貴族學校](../Page/文京區.md "wikilink")「私立櫻蘭學院高等部」（簡稱「櫻蘭高校」）的女孩子，由於對自己本身的性別不特別在意，後來因為頭髮被小孩子黏上[口香糖而爽快剪去一頭長髮](../Page/口香糖.md "wikilink")，再加上因買不起學校制服（一套要價30萬日圓）而扮裝隨便，看起來像男生。在某天放學後，本來想找可以安靜自習的地方，卻在無意間走進了男公關部的根據地－－第三音樂教室，並不小心摔破價值800萬日圓的花瓶……為了償還這筆巨額債務，春緋被男公關部部長須王環要求打雜！然而在春緋的眼鏡被拿掉的契機下，春緋被發現其實是名美少年（事實上是少女），再改造之下轉成接待100名客人，從此展開了「她」的男公關生活……。

## 主要角色

聲優標示為動畫配音員／廣播劇配音員。

### 男公關部成員

  -
    生日：2月4日；[水瓶座](../Page/水瓶座.md "wikilink")[O型](../Page/O型.md "wikilink")；身高：155公分；體重：45；班別：一年A組。
    生於一般的平民家庭。[天然呆](../Page/天然呆.md "wikilink")，個性大而化之，不愛依賴別人。待人態度冷淡，但其實很溫柔體貼。不擅長的學科為美術。成績是全年級第一。
    初登場時因外表被誤認為是男的，實際上是女孩。並且因為父親的緣故，對性別不是那麼在意。體型纖瘦。
    成績優異，為了將來能夠跟媽媽一樣成為律師，而考上特別資優生得以進入櫻蘭學院就讀（因為櫻蘭可保送成績優異生升讀法學部）。由於不小心打破了價值800萬元的花瓶，為了償還這筆債而被迫加入男公關社。她是第一個能分出常陸院雙胞胎的人。
    喜歡吃草莓。沒有吃過金槍魚中段壽司，很希望可以吃到。害怕打雷，在爸爸上夜班時，如果遇到打雷的夜晚，會躲進和室桌底。在海邊別墅時曾遇到打雷的夜晚，當時她是躲進衣櫥。
    曾經在看見光、馨與光邦的女裝打扮時開懷大笑。
    原本是個怕麻煩的人，但自從遇上須王家的律師後，明白凡事都要嘗試的道理，開始比以前更熱心學部活動，不過後來變成思考後拒絕行動。
    後來發現自己喜歡環。漫畫結局並未結婚，不過作者在番外篇畫了一幅兩人的結婚圖。
    關於名字的部份，作者曾描述到是因為出生於2月4日（立春→春），而取作「春緋」（在眾多的來信中，這個提議最多）。另外，「春緋」這個名字在初期設定時，給予作者一種「遙陽」的感覺；但描繪到了第4卷時，她覺得那種感覺變成了「春日」。另有一譯名「治斐」。

<!-- end list -->

  -
    生日：4月8日；[牡羊座](../Page/牡羊座.md "wikilink")[A型](../Page/A型.md "wikilink")；身高：183公分；體重：71；班別：二年Ａ組。
    指名率No.1（佔70%）的男公關部社長。白馬王子型，有點自戀，常常耍白痴。被光和馨認為是可以欺負類型的pure者。個性其實是個外熱內冷的人。成績是全年級第二。
    父親是櫻蘭學院的理事長，母親是法國人。14歲以前都和母親在法國，後來因為須王家繼承問題而被祖母叫回日本。後來決定將來要接管須王家的服務業。有養一隻狗，叫作安德瓦內特。
    熱情、容易激動這一點跟春緋的父親相仿。小孩子氣，會把金牛角餅乾套在指尖上吃，生氣時就堅決不看對方，或是不跟對方說話（但可能明明就很想跟對方說話，卻硬是不開口）。
    睡眠原則是一到晚上10點就準時入睡，原因是這樣皮膚才會好。喜歡早起。
    男公關部成員中，最後一個發現春緋是女生。雖然喜歡春緋，但因於情感方面太遲鈍，而一直（不自覺地）刻意把春緋當成女兒看待。常陸院雙胞胎之一曾謂：「一關係到自己，那傢伙就會變得特別遲鈍。」
    於漫畫第68話中，察覺自己對春緋的感覺並非父女的感情。漫畫結局是與春緋在一起，去了外國，並說將來會結婚。
    關於生日的部份，作者曾說，是因為據說4月8日是[釋迦牟尼的生日](../Page/釋迦牟尼.md "wikilink")，覺得不錯，就將環的生日設定在這一天；但其實來信中，讀者提議最多的是4月1日（[愚人節](../Page/愚人節.md "wikilink")）。另有讀者於信中告訴作者，須王此一姓氏包含了一層「努力去做王者」的意思（作者有說覺得這姓氏很威風）。

<!-- end list -->

  -
    生日：11月22日；[天蠍座](../Page/天蠍座.md "wikilink")[AB型](../Page/AB型.md "wikilink")；身高：181公分；體重：69；班別：二年A組。
    男公關部副社長。對無利可尋的事毫無興趣，但實際上心地善良。有戴眼鏡。剛睡醒時會呈現「低血壓魔王」狀態。
    父親是櫻蘭學院的副理事長，有兩個哥哥（大哥鳳悠一、二哥鳳秋人），一個姊姊（矢道芙裕美），成績是全學年級第一。
    自信有不輸兄長的實力，卻認為自己身為三男註定無法得到重視，而只能被限制在既定的框架上。永遠不能搶去兄長風頭，卻又要比兄長們完成更高的要求，一直盡責地扮演努力而低調的三男角色。
    被環點醒，「能超越哥哥們的話就去超越好了」，才知道原來一直被自己所設的框架限制住，什麼都沒做就自動放棄了。
    TV版中，在Eclair小姐的公司收購他父親的公司前，搶先一步以『KO』的化名收購了他父親的公司，成功化解危機。事後終於得到父親的認同。
    打理著男公關部的一切事務，男公關部眾人也習慣聽從他的指揮，是男公關部的實際掌權人，故被稱為「幕後國王」。
    掌管部內經費開支，因為男公關部為了角色扮演總是愛花大錢，所以社團能夠撐得下去，基本上還是歸功於他。
    男公關部內與環關係最好。初中時被父親要求與作為須王財團繼承人的環打好關係，然而內心卻對環不用努力就可以繼承家業的際遇感到妒忌，最後被環無意中開解了自己多年來的心結。此後鏡夜除去對環的虛偽面具，與環成為真正的朋友。
    在漫畫版第47話裏，環問春緋和光︰「用一句話來形容鏡夜?
    」春緋答︰「冷酷或是冷淡」光答︰「陰險」。而環卻說︰「不過我覺得，鏡夜其實是個熱情的傢伙呢」可見鏡夜其實是個外冷內熱的人，不過很少在人前表現出來。
    男公關部成員中，第一個查覺春緋是女生。對春緋有著一種不明情感。埴之塚認為他跟環、光、韾一樣都是喜歡春緋，只不過自己無意識罷了。他自己卻說只會選擇對鳳家有利的對象，而且他已得到無可取代的事物（意指男公關部），並不打算將這一切破壞掉。
    關於生日的部份，作者曾說覺得11月22日（模範夫妻日）與鏡夜最相配。但被最多人提議的是11月3日（文化節）。

<!-- end list -->

  -
    生日：6月9日；[雙子座](../Page/雙子座.md "wikilink")[B型](../Page/B型.md "wikilink")；身高：178.2公分；體重：60；班別：一年A組。
    常陸院雙胞胎中的哥哥，長得跟馨一模一樣，與馨有很深厚的兄弟愛。瀏海大多往左邊。成績是全年級第五。
    十分率直又不太懂掩飾，在感情上比馨遲鈍。嘴巴惡毒，但實際上心地善良；喜歡有趣的事，人生最大的樂趣之一，就是對方產生如自己預料中的反應。公關部成員們曾謂：「千萬不要讓雙胞胎閒閒沒事做！」
    最常與馨一起和客人玩「猜猜誰是光」的遊戲，並以招牌「雙子禁忌之戀」吸引女學生們的目光。
    後來為了讓別人容易分辨出自己與馨，把自己的頭髮先染成粉紅色，隔天又染成藍色。
    目前可以當面認出雙胞胎的有藤岡春緋、須王環及雙胞胎父母。(不過雙胞胎的奶奶曾跟母親抱怨明明分的出來，為何要一直故意猜錯)
    與馨曾因為想跟春緋一起過萬聖節而捉弄她，但後來卻被春緋抓包，顯露出笨拙的一面。
    男公關部成員中，與馨同時是第三個發現春緋是女生的人。後來喜歡上了春緋，並向她告白。

<!-- end list -->

  -
    生日：6月9日；[雙子座](../Page/雙子座.md "wikilink")[B型](../Page/B型.md "wikilink")；身高：178.2公分；體重：60；班別：一年A組。
    常陸院雙胞胎中的弟弟，跟光長得一模一樣，與光有很深厚的兄弟愛。瀏海大多往右邊。成績是全年級第四。
    個性比光更纖細敏感，永遠以光的利益為出發點，思想比光更成熟。和光從小同進同出，原本只活在兩人封閉的世界裡，直到遇見環、並加入公關部之後，才逐漸打開心房。年齡增長後出現「到底是否希望被認出？」的矛盾想法，並經常為此而苦惱。
    與光曾因為想跟春緋一起過萬聖節而捉弄她，但後來卻被春緋抓包，顯露出笨拙的一面。
    男公關部成員中，與光同時是第三個發現春緋是女生的人。雖然比光更早發現自己的情愫，但因為光而放棄對春緋的愛情，甚至還助他一臂之力。

<!-- end list -->

  -

<small>(「埴」：中文讀音同「執」，粵語讀音同「直」，為「黏土」之意)</small>

  -
    生日：2月29日；[雙魚座](../Page/雙魚座.md "wikilink")[AB型](../Page/AB型.md "wikilink")；身高：148公分；體重：41；班別：三年A組。
    [正太一枚](../Page/正太.md "wikilink")，被暱稱為「埴埴」，由於日文發音跟「甜心」相似，因此也被稱「甜心前輩」或「Honey前輩」。
    武術名門出身。中等部時，曾經以空手道和柔道稱霸全國。從外表看不出來是高三學生，嗜吃甜食，常在半夜吃蛋糕。最喜歡可愛的東西，特別喜歡抱著已逝世的祖母做的兔子娃娃「小兔兔」。擅長數學，成績全年級第一。
    與崇是表兄弟，喜歡坐在崇的肩膀上。男公關部成員中，第二個發現春緋是女生。被吵醒或睡醒時，會變成「低血壓怪獸」。
    看似天真活潑，其實有著思想非常纖細的一面。對公關部成員間的戀愛事情很敏感。曾經當過馨的戀愛顧問。
    關於生日的部份，作者曾說，是因為每四年才過一次生日（2月29日），所以每隔四年才能成長一次。

<!-- end list -->

  -

<small>(「銛」：中文讀音同「先」，粵語讀音同「簽」，為「魚叉」之意(與日同))</small>

  -
    生日：5月5日；[金牛座](../Page/金牛座.md "wikilink")[O型](../Page/O型.md "wikilink")；身高：192公分；體重：78；班別：三年A組。
    沉默寡言，成績是全年級第二。
    銛之塚家原本是埴之塚家的家臣，後來兩家通婚成為親戚，但崇還是跟隨著光邦，並在旁照顧著他，視光邦為最重要的人。
    同時跨部參加劍道部，中等部時，劍術比賽稱霸全國；有一個弟弟，名叫悟（動畫版未出現）。
    漫畫版中有飼養一隻雞與浣熊。
    想睡覺時，會突然變成有點多話的陽光男孩。
    男公關部成員中，第四個發現春緋是女生。
    對春緋有許多體貼的一面。

### 部員家族成員

  -
    春緋的父親。在人妖餐廳工作的人妖。雙性戀，一生只愛春緋的母親（片山琴子）一位「女性」。並在春緋9歲時，養成跟蹤春緋的習慣。
  -
    環的父親，櫻蘭學院的理事長，戲弄獨生子環為其興趣之一。想讓春緋跟環結婚。須王為七大名門之一，事業主要是以祖傳的金融業為基礎，在近年向多方面發展，如：洛瓦古洛酒店、櫻都劇場等。
  -
    鏡夜的父親，櫻蘭學院的副理事長。動畫版中最後一集和須王讓說想讓春緋做鏡夜的妻子。
  -
    舊姓「鳳」，鏡夜的姊姊。
  -
    身高：167公分；血型：A型；班別：國中部三年A組。
    光邦的弟弟。櫻蘭學院中等部三年級，空手道部及柔道部的主將。稱自己哥哥為「外星人」，十分討厭哥哥，認為他放縱自己，沒資格繼承埴之塚家，但武藝上仍算尊敬他。空手道水平僅次於兄長，以打敗他為目標。個性拘謹，但其實跟光邦一樣喜歡可愛的東西，尤其是小雞寶寶。秘密夢想是到南極和企鵝一起玩。在生活上稍有犯錯，就會被悟（銛之塚家次子）以竹劍攻擊。
  -
    身高：175公分；血型：O型；班別：國中部三年A組。
    崇的弟弟。櫻蘭學院中等部三年級，劍道部主將（兼空手道部）。是個味覺白痴。非常崇拜自己哥哥，認為崇是銛之塚家的驕傲。覺得銛之塚家代代都是埴之塚家的家臣，所以抱持著「絕不能讓靖睦成為嬌生慣養的大少爺，就算要出賣我的靈魂也在所不惜」的想法跟在靖睦身邊為他著想，只要靖睦在生活上稍有犯錯，就會被他以竹劍攻擊。

### 櫻蘭學院學生

  -
    原居於法國，是一位電玩狂迷，因為發現鏡夜跟她最愛的戀愛回憶遊戲中裡最愛的主角一模一樣，因此自稱是鏡夜的未婚妻和公關部經理，後來喜歡上春緋。
  -
    黑魔法社的社長，[羅曼諾夫王朝的唯一傳人](../Page/俄羅斯帝國.md "wikilink")。高等部（高中部）三年級。平時因為怕光的關係，所以平時只敢走在有陰影的地方，並把全身包成黑色，甚至戴上黑色假髮來掩飾本來的金髮。手上常常拿著貓型的[貝塞涅夫](../Page/貝塞涅夫.md "wikilink")\[1\]
    布偶。
  -
    貓澤梅人的妹妹，幼等部（幼稚園部）學生。喜歡光亮的地方，害怕黑暗和真貓(布偶之類的不會怕)。
  -
    黑道笠野田組的第四代繼承人，天生眼神凶惡，因此被稱為「人肉冰風暴」，發現春緋是女生的同時，愛上了她，最後春緋無意中拒絕了。
  -
    要求環教他令女孩開心的方法，認環為師父。原本稱環為「King」，後來卻比環更能討女孩歡心，因而戲謔地對環說「你真的是King嗎？」
  -
    環以前的常客，因陷害春緋（例：將春緋的書包等物品全丟入中庭水池裡），被環憤而逐出公關部。
  -
    奏子也是公關部的常客，不過不同於大部分客人有固定的指名喜好，奏子出名的是有著"男公關漂泊症"。這次她的目标轉移到藤岡春緋身上。因為亨的關係，所以很喜歡餐具，對餐具有一定程度的瞭解。奏子有一位家裡定下的未婚夫——珠州島亨，亨的個性比較懦弱，不大起眼，一般都以為奏子是因為不喜歡這門婚事，才會周旋在男公關之間表示不滿，但是事實上，她一直很喜歡青梅竹馬的亨，卻没有辦法坦率的說出来，因為她認為亨並不喜歡雙方家長决定的婚事，而且珠州島馬上就要出國留學……所以另奏子非常煩惱。
    "客人的煩惱就是我們的煩惱，客人的微笑就是我們的收穫"，秉持這樣的觀念，公關部利用舞會的契機興風作浪，終於讓有情人终成眷屬，在夜櫻下翩翩起舞。
    附注：春日崎奏子同時以春绯的初吻對象而聞名哦。
  -
    成績優秀，家世一般，相貌普通，優點是認真，缺點是沒有存在感，較為懦弱。與春日崎奏子是青梅竹馬，兩人有雙方家長所訂下的婚姻，喜歡奏子但卻不敢表達出來，以為奏子周旋在男公關之間是因為不喜歡這門婚事，比起自己，認為奏子更適合耀眼而充滿自信的男孩，因而想出國留學借此改變自己，見識外面的世界，變成更加成熟的男人，這樣才配的上奏子。
  -
    跟春緋還有常陸院兄弟同為一年級A班的學生，是班長，是個很怕恐怖事物的人，有著「恐怖電影恐懼症」、「鬼怪故事恐懼症」、「突然被嚇到恐懼症」、「電視上恐怖電影CM恐懼症」，不想平時在人前裝作若無其事卻突然被看穿自己害怕恐怖的東西的事。

### 其他人物

  -
    羅貝莉亞女子學院白百合會的學生，高等部二年級，第一人稱為：僕（ぼく），通稱：紅薔薇君。
  -
    羅貝莉亞女子學院白百合會的學生，高等部二年級，通稱：鈴蘭君。
  -
    羅貝莉亞女子學院白百合會的學生，高等部一年級，通稱：雛菊君。
  -
    原為千堂幫少主，因受不了幫內作風，毅然離家出走。後進入笠野田幫，與笠野田律結為好友。
  -
    藤岡涼二在人妖酒吧認識的朋友，有在輕井澤開民宿。離過婚。有一女，名叫安村芽衣。
  - （只出現於動畫中）
    法國世襲伯爵家族名門托內魯家三女，保姆是環的母親，艾克蕾兒對此好奇，故找上環。動畫中，葛蘭多涅爾公司是一直收購日本國內企業的外資公司。另有一譯名為艾克蕾兒．多涅爾。

## 漫畫

本作原作，由[葉鳥ビスコ执笔的漫画于](../Page/葉鳥ビスコ.md "wikilink")2003年8月5日至2011年4月5日开始在[白泉社的杂志](../Page/白泉社.md "wikilink")《[LaLa](../Page/LaLa.md "wikilink")》上连载，漫画的标题也为《》。\[2\]十八本漫画[单行本于](../Page/单行本.md "wikilink")2003年8月5日至2011年4月5日发售。\[3\]\[4\]

## 電視動畫

《歡迎光臨櫻蘭高校》全26話

  - [日本於](../Page/日本.md "wikilink")2006年4月4日～9月26日在[日本電視台週二深夜播放](../Page/日本電視台.md "wikilink")。
  - [台灣於](../Page/台灣.md "wikilink")2007年1月12日～2月11日在[緯來日本台週一至五晚上八點播放](../Page/緯來日本台.md "wikilink")）。由於首播時成為該台最高收視節目（2月5日至11日），

重播

  -
    2007年3月3日起週六下午三點至五點（每次連播四集共兩小時）
    2007年6月23日起週六下午三點
    2008年2月18日起週一到五晚上七點至八點
    2009年8月3日起，週一至週五晚上七點半
    2012年7月23日起晚上六點半至七點半（每次連播兩集共一小時）

### 每集標題

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>中文標題</p></th>
<th><p>日文標題</p></th>
<th><p>腳本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>從今天起你就是男公關</p></td>
<td></td>
<td><p><a href="../Page/榎戶洋司.md" title="wikilink">榎戶洋司</a></p></td>
<td><p><a href="../Page/五十嵐卓哉.md" title="wikilink">五十嵐卓哉</a></p></td>
<td><p>長谷部敦志</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>高中生男公關的工作</p></td>
<td></td>
<td><p><a href="../Page/佐藤順一.md" title="wikilink">佐藤イチ</a></p></td>
<td><p>安齋剛文</p></td>
<td><p>菅野宏紀</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>當心身體檢查</p></td>
<td></td>
<td><p><a href="../Page/岡村天齋.md" title="wikilink">岡村天齋</a></p></td>
<td><p>飯村正之</p></td>
<td><p><a href="../Page/本橋秀之.md" title="wikilink">本橋秀之</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>女經理來襲</p></td>
<td></td>
<td><p>中津環</p></td>
<td><p>齊藤英子</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>雙胞胎吵架</p></td>
<td></td>
<td><p>角田一樹</p></td>
<td><p><a href="../Page/工藤裕加.md" title="wikilink">工藤裕加</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>叛逆型小學生男公關</p></td>
<td></td>
<td><p><a href="../Page/福田道生.md" title="wikilink">福田道生</a></p></td>
<td><p><a href="../Page/森山雄治.md" title="wikilink">森山雄治</a></p></td>
<td><p><a href="../Page/森山雄治.md" title="wikilink">もりやまゆうじ</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>叢林泳池SOS</p></td>
<td></td>
<td><p><a href="../Page/石平信司.md" title="wikilink">石平信司</a></p></td>
<td><p>安齋剛文</p></td>
<td><p>矢崎優子<br />
長谷部敦志</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>陽光大海男公關社</p></td>
<td><p>太陽と海とホスト部}}</p></td>
<td><p>五十嵐卓哉<br />
飯村正之</p></td>
<td><p>飯村正之</p></td>
<td><p>本橋秀之</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>羅貝莉亞女子學院的挑戰</p></td>
<td></td>
<td><p>金子伸吾</p></td>
<td><p>菅野宏紀</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>藤岡家的日常</p></td>
<td></td>
<td><p><a href="../Page/櫻井弘明.md" title="wikilink">櫻井弘明</a></p></td>
<td><p>信田祐</p></td>
<td><p>鍋田香代子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>哥哥是王子</p></td>
<td></td>
<td><p>石平信司</p></td>
<td><p>松尾慎</p></td>
<td><p>齊藤英子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>Honey前輩沒有甜食的三天</p></td>
<td></td>
<td><p>福田道生</p></td>
<td><p>熨斗谷充孝</p></td>
<td><p>鈴木伸一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>春緋的夢幻仙境</p></td>
<td></td>
<td><p>五十嵐卓哉</p></td>
<td><p><a href="../Page/倉島亞由美.md" title="wikilink">倉島亞由美</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>採訪傳說中的男公關部</p></td>
<td></td>
<td><p><a href="../Page/菱田正和.md" title="wikilink">菱田正和</a></p></td>
<td><p>安齋剛文</p></td>
<td><p>工藤裕加</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>輕井澤清爽的戰鬥</p></td>
<td></td>
<td><p>石平信司</p></td>
<td><p>中村里美</p></td>
<td><p>本橋秀之</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>與光的初次約會大作戰</p></td>
<td></td>
<td><p>角田一樹</p></td>
<td><p>長谷部敦志<br />
菅野宏紀</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>鏡夜出於無奈的假日</p></td>
<td></td>
<td><p>福田道生</p></td>
<td><p>犬川犬夫</p></td>
<td><p>鍋田香代子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>小睦的打倒埴埴宣言</p></td>
<td></td>
<td><p>石平信司</p></td>
<td><p>信田祐</p></td>
<td><p>小平佳幸</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>羅貝利亞女子學院的反攻</p></td>
<td></td>
<td><p>金子伸吾</p></td>
<td><p>斉藤英子<br />
矢崎優子</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>雙胞胎打開的門</p></td>
<td></td>
<td><p><a href="../Page/京田知己.md" title="wikilink">京田知己</a></p></td>
<td><p>中村里美</p></td>
<td><p>本橋秀之</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>直到變成南瓜那天</p></td>
<td></td>
<td><p>数井浩子<br />
石平信司</p></td>
<td><p>佐藤育郎</p></td>
<td><p>桑名郁郎<br />
<a href="../Page/板垣敦.md" title="wikilink">板垣敦</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>成為銛學長的徒弟</p></td>
<td></td>
<td><p>安齋剛文</p></td>
<td><p>工藤裕加</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>環的無意識的憂鬱</p></td>
<td></td>
<td><p>松尾慎</p></td>
<td><p>長谷部敦志<br />
菅野宏紀</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>於是，遇見了鏡夜</p></td>
<td></td>
<td><p>京田知己</p></td>
<td><p>小平佳幸</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>男公關部解散宣言</p></td>
<td></td>
<td><p>石平信司</p></td>
<td><p>金子伸吾</p></td>
<td><p>齊藤英子<br />
工藤裕加</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>這就是我們的櫻蘭祭</p></td>
<td></td>
<td><p>五十嵐卓哉</p></td>
<td><p><a href="../Page/高橋久美子.md" title="wikilink">高橋久美子</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 音樂

  - 片頭曲：「」（櫻之吻）

<!-- end list -->

  -
    作詞：、作曲：M.RIE、編曲：shinya、唱：[河邊千惠子](../Page/河邊千惠子.md "wikilink")

<!-- end list -->

  - 片尾曲：「」（疾走）

<!-- end list -->

  -
    作詞、作曲：安齊龍介、編曲：[LAST
    ALLIANCE](../Page/LAST_ALLIANCE.md "wikilink")、唱：LAST ALLIANCE
    第26話片尾曲：「」（明天再見）、唱：動畫版配音員

<!-- end list -->

  - 插曲：

<!-- end list -->

  -
    第6話：莫札特D大調雙鋼琴奏鳴曲（K.448）
    第19話：河邊千惠子「Little Wing」（收錄由專輯《brilliance》）
    第24話：蕭邦降E大調夜曲（Op.9-2）

<!-- end list -->

  - 角色歌：

<!-- end list -->

  -
    須王環（宮野真守）：「Guilty Beauty Love」
    鳳鏡夜（松風雅也）：「清冷之夜」
    常陸院光、常陸院馨（鈴村健一、藤田圭宣）：「我們的戀愛模式」
    埴之塚光邦（齋藤彩夏）：「七上八下」
    銛之塚崇（桐井大介）：「一直在身邊」

<!-- end list -->

  - 大合唱：

<!-- end list -->

  -
    藤岡春緋（坂本真綾）、須王環（宮野真守）、鳳鏡夜（松風雅也）、常陸院光（鈴村健一）、常陸院馨（藤田圭宣）、埴之塚光邦（齋藤彩夏）、銛之塚崇（桐井大介）：「明天見」

### 製作人員

  - 企劃：[大澤雅彦](../Page/大澤雅彦.md "wikilink")（日本電視台）、[大島滿](../Page/大島滿.md "wikilink")（Vap）、[內山晴人](../Page/內山晴人.md "wikilink")（白泉社）
  - 製作人：[中谷敏夫](../Page/中谷敏夫.md "wikilink")（日本電視台）、[田村学](../Page/田村学.md "wikilink")（Vap）、[南雅彦](../Page/南雅彦.md "wikilink")（BONES）、[山下雅弘](../Page/山下雅弘.md "wikilink")（白泉社）
  - 監督：[五十嵐卓哉](../Page/五十嵐卓哉.md "wikilink")
  - 系列構成、劇本：[榎戶洋司](../Page/榎戶洋司.md "wikilink")
  - [人物設計](../Page/人物設計.md "wikilink")：[高橋久美子](../Page/高橋久美子.md "wikilink")
  - 主設計：[福地仁](../Page/福地仁.md "wikilink")
  - 支柱設計：[宮豐](../Page/宮豐.md "wikilink")
  - 美術監督：[中村典史](../Page/中村典史.md "wikilink")（Green）
  - 色彩設計：[中山志保子](../Page/中山志保子.md "wikilink")
  - 攝影監督：[瓶子修一](../Page/瓶子修一.md "wikilink")
  - 音響監督：[若林和弘](../Page/若林和弘.md "wikilink")
  - 音樂：[平野義久](../Page/平野義久.md "wikilink")
  - 動畫製作：[BONES](../Page/BONES_\(動畫製作公司\).md "wikilink")
  - 製作著作：[日本電視台](../Page/日本電視台.md "wikilink")、Vap、BONES、白泉社

## 廣播劇CD

※声优与动画版不同

  - 「桜蘭高校ホスト部」きらめきCDパック（2003年發行，主要剧情为原作第1话（单行本1卷收录）治斐入部的情节）
  - 「桜蘭高校ホスト部」うきドキCDパック（2004年發行，主要剧情为原作第3话（单行本1卷收录）宝积寺莲华登场的情节）
  - 「桜蘭高校ホスト部」わいガヤCDパック（2005年發行，主要剧情为原作第4话（单行本2卷收录）身体检查的情节）
  - LaLaメモリアル☆ドラマCD（2004年9月号附录）
  - LaLaキラメキ☆ドラマCD（2005年9月号附录）

※声优与动画版相同

  - 「樱兰高校男公关部」动画DRAMA CD（2006年9月發行）
  - LaLaゴージャス☆ドラマCD（2006年11月号附录）
  - LaLa附录：樱兰高校男公关部DRAMA CD（2007年10月号附录）
  - 「桜蘭高校ホスト部」わくわくドラマCD （(1) ホスト部ドラマCDへの道 (2) 呪われたホスト部 (3) 鳳家スタッフ三人衆）
  - LaLa附录：LaLa Premium Drama CD（2008年5月号附录）
  - LaLa附录：LaLa Excellent Drama CD（2008年11月号附录）
  - LaLa附录：LaLaプレシャスドラマCD（2009年11月号附录）
  - 「桜蘭高校ホスト部」クライマックスドラマCD（(1) ホスト部、栄光の歴史を語る\! (2) 鳳家スタッフ三人衆 (3) 声優座談会）

## 電視劇

### 演員表

#### 櫻蘭高校男公關社

  - 藤岡春緋 - [川口春奈](../Page/川口春奈.md "wikilink")
  - 須王環 - [山本裕典](../Page/山本裕典.md "wikilink")
  - 鳳鏡夜 - [大東駿介](../Page/大東駿介.md "wikilink")
  - 常陸院光 - [高木心平](../Page/高木心平.md "wikilink")
  - 常陸院馨 - [高木萬平](../Page/高木萬平.md "wikilink")
  - 埴之塚光邦 - [千葉雄大](../Page/千葉雄大.md "wikilink")
  - 銛之塚崇 - [中村昌也](../Page/中村昌也.md "wikilink")

#### 其他人物

  - 貓澤梅人 - [龍星涼](../Page/龍星涼.md "wikilink")
  - 伽名月麗子 - [杉咲花](../Page/杉咲花.md "wikilink")（第7集～完結篇）
  - 東條千春 - [小川百惠](../Page/小川百惠.md "wikilink")
  - 西條夏樹 - [廣村美津美](../Page/廣村美津美.md "wikilink")
  - 南條秋奈 - [東亞優](../Page/東亞優.md "wikilink")
  - 倉賀野百華 -[清野菜名](../Page/清野菜名.md "wikilink")
  - 櫻塚希美子 - [Nicola](../Page/Nicola.md "wikilink")
  - 男公關部其他常客 -
    [房綠](../Page/房綠.md "wikilink")、[水谷幸惠](../Page/水谷幸惠.md "wikilink")、[繪理南](../Page/繪理南.md "wikilink")、[高橋圓香](../Page/高橋圓香.md "wikilink")、[宮城美壽壽](../Page/宮城美壽壽.md "wikilink")、[小田島渚](../Page/小田島渚.md "wikilink")、[白石岬](../Page/白石岬.md "wikilink")、[安西涼生](../Page/安西涼生.md "wikilink")、[明日美](../Page/明日美.md "wikilink")、[高橋一舞紀](../Page/高橋一舞紀.md "wikilink")
  - 藤岡琴子（本姓片山，春緋的母親。已故） - [鈴木亞美](../Page/鈴木亞美.md "wikilink")（照片、回憶）

#### 客串演出

  - 藤岡春緋幼年 - [石井萌萌果](../Page/石井萌萌果.md "wikilink")
  - 綾小路聖香 - [大川藍](../Page/大川藍.md "wikilink")（第1、完結篇）
  - 野武 - [神保悟志](../Page/神保悟志.md "wikilink")（第2集）
  - 女醫師 - [唐橋由美](../Page/唐橋由美.md "wikilink")（第2集）
  - 男醫師 - [水谷敦](../Page/水谷敦.md "wikilink")（第2集）
  - 妹妹頭護士 - [岩井七世](../Page/岩井七世.md "wikilink")（第2集）
  - 護士 - [吉田智美](../Page/吉田智美.md "wikilink")（第2集）
  - 城之内綾女（春緋的家庭教師） - [中別府葵](../Page/中別府葵.md "wikilink")（第3集、完結篇）
  - 國中部3年級時的導師（回憶） - [本田清澄](../Page/本田清澄.md "wikilink")（第3集）
  - 寶積寺蓮華 - [土屋太鳳](../Page/土屋太鳳.md "wikilink")（第4、6集）
  - 寶積寺渉 - [五王四郎](../Page/五王四郎.md "wikilink")（第4集）
  - 理事長秘書 - [阿部哲子](../Page/阿部哲子.md "wikilink")（第4、8、10集、完結篇）
  - 櫻蘭學院男學生（校內少數的不良系少年） - [櫻野裕己](../Page/櫻野裕己.md "wikilink")（第4集）
  - 非法入侵的男子 -
    [大熊將成](../Page/大熊將成.md "wikilink")、[柳亮](../Page/柳亮.md "wikilink")、[吉水健一](../Page/吉水健一.md "wikilink")（第5集）
  - 私立櫻蘭學院高中部導師 - [藏内秀樹](../Page/藏内秀樹.md "wikilink")（第6集）
  - 女學生 - [安倍花映](../Page/安倍花映.md "wikilink")（第6集）
  - 藤岡涼二 / 蘭花（春緋的父親） - [戸次重幸](../Page/戸次重幸.md "wikilink")（第7集、完結篇）
  - 小女孩 - [恩田乃愛](../Page/恩田乃愛.md "wikilink")（第7集）
  - 須王讓（環的父親、櫻蘭學院理事長） - [升毅](../Page/升毅.md "wikilink")（第8、10集～完結篇）
  - 貓澤霧美（國小部1年級、貓澤梅人的妹妹） - [澤田萌音](../Page/澤田萌音.md "wikilink")（第8集、完結篇）
  - 吳竹 - [吉川麻美](../Page/吉川麻美.md "wikilink")（第8集）
  - 須王静江（讓的母親、環的祖母） - [江波杏子](../Page/江波杏子.md "wikilink")（第9集～完結篇）
  - 荒井 - [秋元龍太朗](../Page/秋元龍太朗.md "wikilink")（第9集）
  - 小松澤明（高中部3年級生、新聞社社長） - [永瀬匡](../Page/永瀬匡.md "wikilink")（第9～10集）
  - 右京朱里（新聞社社員） - [上間美緒](../Page/上間美緒.md "wikilink")（第9～10集）
  - 角力社主將（幫忙新聞社執行計畫的角力社社員） -
    [正義岩倉](../Page/正義岩倉.md "wikilink")（岩倉健伸）（第10集）
  - 鳳敬雄（鏡夜的父親） - [清水昭博](../Page/清水昭博.md "wikilink")（第10集～完結篇）
  - 鳳悠一（鳳家的長男） - [合田雅吏](../Page/合田雅吏.md "wikilink")（第10集）
  - 鳳秋人 - [望月柊成](../Page/望月柊成.md "wikilink")（第10集）

### 製作人員

  - 原作：[葉鳥螺子](../Page/葉鳥螺子.md "wikilink")「」（[白泉社](../Page/白泉社.md "wikilink")）
  - 编剧：[池田奈津子](../Page/池田奈津子.md "wikilink")
  - 副製作人：[伊與田英徳](../Page/伊與田英徳.md "wikilink")、[杉山剛](../Page/杉山剛.md "wikilink")（日本索尼音樂）
  - 製作人：[韓哲](../Page/韓哲.md "wikilink")、[橘康仁](../Page/橘康仁.md "wikilink")
  - 導演：[韓哲](../Page/韓哲.md "wikilink")、[佐藤敦司](../Page/佐藤敦司.md "wikilink")、[芝崎弘記](../Page/芝崎弘記.md "wikilink")
  - 音樂：[仲西匡](../Page/仲西匡.md "wikilink")
  - 特別協助：[日本索尼音樂](../Page/日本索尼音樂.md "wikilink")
  - 製作協助：DREAMAX TELEVISION
  - 製作：[TBS電視](../Page/TBS電視.md "wikilink")

### 音樂

  - 主題曲：[miwa](../Page/miwa.md "wikilink")「[FRiDAY-MA-MAGiC](../Page/FRiDAY-MA-MAGiC.md "wikilink")」（Sony
    Music Records）

### 收視率

| 集數                                                                                                         | 播放日期            | 編劇                   | 導演                    | 收視率  |
| ---------------------------------------------------------------------------------------------------------- | --------------- | -------------------- | --------------------- | ---- |
| Episode0                                                                                                   | 2011年7月15日      | 池田奈津子                | 佐藤敦司                  | 2.7% |
| Episode1                                                                                                   | 2011年7月22日\[5\] | 韓哲                   | <font color=blue>1.1% |      |
| Episode2                                                                                                   | 2011年7月29日      | 3.4%                 |                       |      |
| Episode3                                                                                                   | 2011年8月5日       | 2.7%                 |                       |      |
| Episode4                                                                                                   | 2011年8月12日      | 佐藤敦司                 | 2.3%                  |      |
| Episode5                                                                                                   | 2011年8月19日      | 3.1%                 |                       |      |
| Episode6                                                                                                   | 2011年8月26日\[6\] | 韓哲                   | 3.0%                  |      |
| Episode7                                                                                                   | 2011年9月2日\[7\]  | 3.4%                 |                       |      |
| Episode8                                                                                                   | 2011年9月9日       | 佐藤敦司                 | 2.8%                  |      |
| Episode9                                                                                                   | 2011年9月16日      | <font color=red>4.0% |                       |      |
| Episode10                                                                                                  | 2011年9月23日      | 芝崎弘記                 | 3.1%                  |      |
| Final Episode                                                                                              | 2011年9月30日\[8\] | 韓哲                   | 2.3%                  |      |
| 平均收視率2.84%（[關東地區](../Page/關東地方.md "wikilink")・由[Video Research公司調查](../Page/Video_Research.md "wikilink")） |                 |                      |                       |      |

### 播放時間

| 播放地区                               | 电视台                              | 播放期间       | 播放档期            |
| ---------------------------------- | -------------------------------- | ---------- | --------------- |
| [关东地区](../Page/关东地区.md "wikilink") | [TBS](../Page/TBS.md "wikilink") | 2011年7月22日 | 金曜24:20 - 24:50 |
| [静冈县](../Page/静冈县.md "wikilink")   | [SBS](../Page/SBS.md "wikilink") | 2011年7月22日 | 金曜24:20 - 24:50 |
| [近畿地区](../Page/近畿地区.md "wikilink") | [MBS](../Page/MBS.md "wikilink") | 2011年7月27日 | 水曜26:40 - 27:10 |
| [中京地区](../Page/中京地区.md "wikilink") | [CBC](../Page/CBC.md "wikilink") | 2011年7月29日 | 金曜25:27 - 26:00 |
| [熊本县](../Page/熊本县.md "wikilink")   | [RKK](../Page/RKK.md "wikilink") | 2011年8月1日  | 月曜25:25 - 25:55 |
| [北海道](../Page/北海道.md "wikilink")   | [HBC](../Page/HBC.md "wikilink") | 2011年8月2日  | 火曜24:56 - 25:26 |
| [福冈县](../Page/福冈县.md "wikilink")   | [RKB](../Page/RKB.md "wikilink") | 2011年8月16日 | 火曜25:25 - 25:55 |
| [富山县](../Page/富山县.md "wikilink")   | [TUT](../Page/TUT.md "wikilink") | 2011年9月5日  | 月曜24:00 -       |

## 作品的變遷

## 電影

### 演員表

  - 藤岡春緋 - [川口春奈](../Page/川口春奈.md "wikilink")
  - 須王環 - [山本裕典](../Page/山本裕典.md "wikilink")
  - 鳳鏡夜 - [大東駿介](../Page/大東駿介.md "wikilink")
  - 猫澤梅人 - [龍星涼](../Page/龍星涼.md "wikilink")
  - 銛之塚崇 - [中村昌也](../Page/中村昌也.md "wikilink")
  - 埴之塚光邦 - [千葉雄大](../Page/千葉雄大.md "wikilink")
  - 常陸院光 - [高木心平](../Page/高木心平.md "wikilink")
  - 常陸院馨 - [高木萬平](../Page/高木萬平.md "wikilink")
  - 東條千春 - [小川百惠](../Page/小川百惠.md "wikilink")
  - 西條夏樹 - [廣村美津美](../Page/廣村美津美.md "wikilink")
  - 南條秋奈 - [東亞優](../Page/東亞優.md "wikilink")
  - 倉賀野百華 - [清野菜名](../Page/清野菜名.md "wikilink")
  - 櫻塚希美子 - [Nicola](../Page/Nicola.md "wikilink")
  - 九瀬猛（美式足球社社長） - [市川知宏](../Page/市川知宏.md "wikilink")
  - 東郷院誠（美式足球社副社長） - [菊田大輔](../Page/菊田大輔.md "wikilink")
  - 垂水隼人 - [鈴木勝大](../Page/鈴木勝大.md "wikilink")

<!-- end list -->

  -
    美式足球社社員。

<!-- end list -->

  - 伽名月麗子 - [杉咲花](../Page/杉咲花.md "wikilink")
  - 満山香南 - [板東晴](../Page/板東晴.md "wikilink")
  - 幼年时的春緋 - [石井萌萌果](../Page/石井萌萌果.md "wikilink")
  - 蜜雪兒的秘書 - [Carolina](../Page/Carolina.md "wikilink")
  - 須王家秘書 - [矢吹春奈](../Page/矢吹春奈.md "wikilink")
  - 橘誠三郎 - [楠美聖壽](../Page/楠美聖壽.md "wikilink")
  - 勞倫斯的秘書 - [駿河太郎](../Page/駿河太郎.md "wikilink")
  - 蜜雪兒・江梨華・蒙那（新加坡留學生） -
    [篠田麻里子](../Page/篠田麻里子.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
  - 勞倫斯・新・蒙那（大財團少主，蜜雪兒的哥哥） -
    [Nichkhun](../Page/Nichkhun.md "wikilink")（[2PM](../Page/2PM.md "wikilink")）
  - 安妮・蘇菲 - [財前直見](../Page/財前直見.md "wikilink")（特別演出）
  - 蘭花（藤岡涼二） - [戸次重幸](../Page/戸次重幸.md "wikilink")
  - 藤岡琴子（本姓片山，春緋的母親） - [鈴木亞美](../Page/鈴木亞美.md "wikilink")
  - 鳳敬雄 - [清水昭博](../Page/清水昭博.md "wikilink")
  - 須王讓 - [升毅](../Page/升毅.md "wikilink")
  - 須王静江 - [江波杏子](../Page/江波杏子.md "wikilink")

### 製作人員

  - 原作 - [葉島螺子](../Page/葉島螺子.md "wikilink")「桜蘭高校ホスト部」（白泉社）
  - 导演 - [韓哲](../Page/韓哲.md "wikilink")
  - 脚本 - [池田奈津子](../Page/池田奈津子.md "wikilink")
  - 音乐 - [仲西匡](../Page/仲西匡.md "wikilink")
  - 音乐制作 - [志田博英](../Page/志田博英.md "wikilink")
  - 製作总指揮 - [村松俊亮](../Page/村松俊亮.md "wikilink")
  - 执行制片人 - [田代秀樹](../Page/田代秀樹.md "wikilink")
  - 企劃製作 - [杉山剛](../Page/杉山剛.md "wikilink")
  - 製作 - [伊與田英徳](../Page/伊與田英徳.md "wikilink")
  - 製片 - [橘康仁](../Page/橘康仁.md "wikilink")
  - 副製片 -
    [渡邉敬介](../Page/渡邉敬介.md "wikilink")、[佐藤敦司](../Page/佐藤敦司.md "wikilink")
  - 制片 - [前田利洋](../Page/前田利洋.md "wikilink")
  - 攝影 - [近江正彦](../Page/近江正彦.md "wikilink")
  - 画面 - [古市修文](../Page/古市修文.md "wikilink")
  - 照明 - [横山修司](../Page/横山修司.md "wikilink")
  - 录音 - [植村貴弘](../Page/植村貴弘.md "wikilink")
  - 美術 - [YANG仁榮](../Page/YANG仁榮.md "wikilink")
  - 装飾 - [飯田恵](../Page/飯田恵.md "wikilink")
  - 美術制作 - [川島由依](../Page/川島由依.md "wikilink")
  - 助理導演 - [芝崎弘記](../Page/芝崎弘記.md "wikilink")
  - 製作 -
    [佐藤大樹](../Page/佐藤大樹.md "wikilink")、[吉川和也](../Page/吉川和也.md "wikilink")
  - 製作候補 - [高野英治](../Page/高野英治.md "wikilink")
  - 服裝 - [金順華](../Page/金順華.md "wikilink")
  - 发型和化妆 - [松田麻希](../Page/松田麻希.md "wikilink")
  - 道具 - [中飛沙織](../Page/中飛沙織.md "wikilink")
  - 紀錄 - [小柳菜乃](../Page/小柳菜乃.md "wikilink")
  - 編輯 - [高池徹](../Page/高池徹.md "wikilink")
  - 選曲- [稻川壯](../Page/稻川壯.md "wikilink")
  - 音响效果 - [西村洋一](../Page/西村洋一.md "wikilink")
  - 特效製片 - [鳥尾美里](../Page/鳥尾美里.md "wikilink")
  - 特效導演 - [岩田勝巳](../Page/岩田勝巳.md "wikilink")
  - 審定協助 - [菅原弘文](../Page/菅原弘文.md "wikilink")
  - 企劃協助 - 白泉社 LaLa編集部
  - 製作公司 - DREAMAX TELEVISION
  - 配給 - [索尼影業](../Page/索尼影業.md "wikilink")
  - 製作 -
    電影「樱蘭高校男公關部」製作委員會（[日本索尼音樂](../Page/日本索尼音樂.md "wikilink")、[TBS電視台](../Page/TBS電視台.md "wikilink")、[白泉社](../Page/白泉社.md "wikilink")、[DREAMAX
    TELEVISION](../Page/DREAMAX_TELEVISION.md "wikilink")、[中部日本放送](../Page/中部日本放送.md "wikilink")、[靜岡放送](../Page/靜岡放送.md "wikilink")、[鬱金香電視台](../Page/鬱金香電視台.md "wikilink")、[南日本放送](../Page/南日本放送.md "wikilink")）

## 衍生劇

《櫻蘭高校男公關部〜春緋生日快樂大作戰〜》是au・[LISMO劇](../Page/LISMO_Channel.md "wikilink")2012年1月6日起上傳的衍生劇。全4話。

  - 演員表

<!-- end list -->

  - 川口春奈
  - 山本裕典
  - 龍星涼
  - 中村昌也
  - 千葉雄大
  - 高木心平
  - 高木万平
  - 清野菜名
  - nicola
  - 大東駿介

<!-- end list -->

  - 製作人員

<!-- end list -->

  - 演出 - 韓哲、佐藤敦司

<!-- end list -->

  - 主題歌

<!-- end list -->

  - [miwa](../Page/miwa.md "wikilink")「FRiDAY-MA-MAGiC」（Sony Music
    Records Inc.）

## 廣播劇

2012年1月6日於au・LISMO廣播電台播放。全4話。

## 游戏

2007年4月19日由[Idea
Factory](../Page/Idea_Factory.md "wikilink")（OTOMETE）发行的[PS2遊戲](../Page/PS2.md "wikilink")\[9\]，其后也发行《樱兰高校男公关部DS》的DS移植版遊戲\[10\]。

## 註釋

## 參考

## 外部連結

  - [白泉社漫畫官方網站](https://web.archive.org/web/20121116092621/http://www.hakusensha.co.jp/host_anime/index.html)

  - [NTV動畫官方網站](http://www.ntv.co.jp/host)

  - [木棉花中文版動畫官方網站](http://www.e-muse.com.tw/property/horam_host/introduction/20061102_tv/index.htm)

  -
  - [桜蘭高校ホスト部アニメ2期製作希望](https://web.archive.org/web/20090605081231/http://www.shomei.tv/project-802.html)

  - [桜蘭高校ホスト部TBS官方網站](http://www.tbs.co.jp/ouran2011/index-j.html)

[Category:改编成电影的日本漫画](../Category/改编成电影的日本漫画.md "wikilink")
[Category:LaLa](../Category/LaLa.md "wikilink")
[Category:高中背景漫畫](../Category/高中背景漫畫.md "wikilink")
[Category:少女漫畫](../Category/少女漫畫.md "wikilink")
[Category:後宮型作品](../Category/後宮型作品.md "wikilink")
[Category:男裝作品](../Category/男裝作品.md "wikilink")
[Category:2006年日本電視動畫](../Category/2006年日本電視動畫.md "wikilink")
[Category:高中背景動畫](../Category/高中背景動畫.md "wikilink")
[Category:广播剧CD](../Category/广播剧CD.md "wikilink")
[Category:BONES](../Category/BONES.md "wikilink")
[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:Idea_Factory游戏](../Category/Idea_Factory游戏.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:任天堂DS游戏](../Category/任天堂DS游戏.md "wikilink")
[Category:恋爱冒险游戏](../Category/恋爱冒险游戏.md "wikilink")
[Category:少女游戏](../Category/少女游戏.md "wikilink") [Category:Friday
Break](../Category/Friday_Break.md "wikilink")
[Category:2011年日本電視劇集](../Category/2011年日本電視劇集.md "wikilink")
[Category:2012年亮相的日本網絡劇](../Category/2012年亮相的日本網絡劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:日本愛情劇](../Category/日本愛情劇.md "wikilink")
[Category:日本校園劇](../Category/日本校園劇.md "wikilink")
[Category:高中背景電視劇](../Category/高中背景電視劇.md "wikilink")
[Category:男裝題材電視劇](../Category/男裝題材電視劇.md "wikilink")
[Category:2012年日本電影](../Category/2012年日本電影.md "wikilink")
[Category:2010年代愛情片](../Category/2010年代愛情片.md "wikilink")
[Category:日本愛情片](../Category/日本愛情片.md "wikilink")
[Category:高中背景電影](../Category/高中背景電影.md "wikilink")
[Category:日本校園電影](../Category/日本校園電影.md "wikilink")
[Category:男裝題材電影](../Category/男裝題材電影.md "wikilink")
[Category:文京區背景作品](../Category/文京區背景作品.md "wikilink")
[Category:TBS製作的電影](../Category/TBS製作的電影.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:緯來電視外購動畫](../Category/緯來電視外購動畫.md "wikilink")
[Category:高中題材電子遊戲](../Category/高中題材電子遊戲.md "wikilink")
[Category:Bilibili外购动画](../Category/Bilibili外购动画.md "wikilink")

1.  查目前比較流行的[國語和合本](../Page/國語和合本.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[新標點和合本](../Page/新標點和合本.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[和合本修訂版](../Page/和合本修訂版.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[現代中文譯本](../Page/現代中文譯本.md "wikilink")[聖經](../Page/聖經.md "wikilink")，還有比較古老的[光緒](../Page/光緒.md "wikilink")19年[福州美華](../Page/福州.md "wikilink")[書局活板文理](../Page/書局.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[光緒](../Page/光緒.md "wikilink")34年[上海大](../Page/上海.md "wikilink")[美國](../Page/美國.md "wikilink")[聖經會](../Page/聖經.md "wikilink")[官話串珠](../Page/官話.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[宣統](../Page/宣統.md "wikilink")3年[聖經公會的文理](../Page/聖經.md "wikilink")[聖經](../Page/聖經.md "wikilink")，Satan均譯作「撒但」，不作「撒旦」。
2.
3.
4.
5.  第一話因棒球比賽延長30分鐘至24:50播出
6.  第六話因應映世界電視劇同步延至24:29播出
7.  第七話因應映世界大會期間的特別編[NEWS23X於](../Page/NEWS23X.md "wikilink")24:25播出，故調整於24:30播出
8.  最終話因「中居正広のキンスマスペ」延遲25分鐘於24:45播出
9.
10.