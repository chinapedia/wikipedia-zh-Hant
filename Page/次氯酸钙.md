**次氯酸钙**是[钙的](../Page/钙.md "wikilink")[次氯酸盐](../Page/次氯酸盐.md "wikilink")，[化学式为](../Page/化学式.md "wikilink")[Ca](../Page/钙.md "wikilink")([Cl](../Page/氯.md "wikilink")[O](../Page/氧.md "wikilink"))<sub>2</sub>，是[漂白粉的主要成分之一](../Page/漂白粉.md "wikilink")，有杀菌性及[氧化性](../Page/氧化性.md "wikilink")。与其性质类似的还有[氯气及](../Page/氯气.md "wikilink")[次氯酸钠](../Page/次氯酸钠.md "wikilink")，但是它们都不如次氯酸钙的稳定性高。

漂白粉的组分包含次氯酸钙与[氯化钙](../Page/氯化钙.md "wikilink")、[氢氧化钙和](../Page/氢氧化钙.md "wikilink")[水](../Page/水.md "wikilink")，真正的分子式可以写为Ca(ClO)<sub>2</sub>·CaCl<sub>2</sub>·Ca(OH)<sub>2</sub>·2H<sub>2</sub>O，\[1\]是一种不易吸水的干粉。

## 制备

次氯酸钙的制备方法有：

  - [氯气通入](../Page/氯气.md "wikilink")[氢氧化钙溶液中](../Page/氢氧化钙.md "wikilink")。高浓度时可结晶出Ca(ClO)<sub>2</sub>·2CaCl<sub>2</sub>，大量应用于漂白纸张：

<!-- end list -->

  -
    2 Ca(OH)<sub>2</sub> + 2 Cl<sub>2</sub> → Ca(ClO)<sub>2</sub> +
    CaCl<sub>2</sub> + 2 H<sub>2</sub>O

<!-- end list -->

  - 或氯气通入氢氧化钙与[氢氧化钠混合溶液中](../Page/氢氧化钠.md "wikilink")：

<!-- end list -->

  -
    Ca(OH)<sub>2</sub> + 2 Cl<sub>2</sub> + 2 NaOH → Ca(ClO)<sub>2</sub>
    + 2 H<sub>2</sub>O + 2 NaCl

## 性质

次氯酸钙在水中的[溶解度不大](../Page/溶解度.md "wikilink")。它的稳定性与含水量成反比，分解反应强烈放热，产物是[氯化钙及](../Page/氯化钙.md "wikilink")[一氧化二氯](../Page/一氧化二氯.md "wikilink")。次氯酸钙与[二氧化碳反应生成](../Page/二氧化碳.md "wikilink")[碳酸钙](../Page/碳酸钙.md "wikilink")，放出[氯气](../Page/氯气.md "wikilink")：

  -
    2Ca(ClO)<sub>2</sub> + 2CO<sub>2</sub> → 2CaCO<sub>3</sub> +
    2Cl<sub>2</sub> + O<sub>2</sub>

与[盐酸反应生成](../Page/盐酸.md "wikilink")[氯化钙](../Page/氯化钙.md "wikilink")：

  -
    Ca(ClO)<sub>2</sub> + 4 HCl → CaCl<sub>2</sub> + 2 H<sub>2</sub>O +
    2 Cl<sub>2</sub>

次氯酸钙水解离子方程式:ClO－+H2O→HClO+OH-(可逆)

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:钙化合物](../Category/钙化合物.md "wikilink")
[Category:次氯酸盐](../Category/次氯酸盐.md "wikilink")
[Category:漂白剂](../Category/漂白剂.md "wikilink")

1.  《无机化学丛书》第二卷，铍、碱土金属、硼铝镓分族。北京：科学出版社，1984年。