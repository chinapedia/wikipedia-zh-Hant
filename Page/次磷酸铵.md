**次磷酸銨**一種白色的[晶體](../Page/晶體.md "wikilink")，[分子式為NH](../Page/分子式.md "wikilink")<sub>4</sub>H<sub>2</sub>PO<sub>2</sub>
，容易[潮解](../Page/潮解.md "wikilink")，加熱至240℃分解，釋放出[磷化氫](../Page/磷化氫.md "wikilink")，可以用[黄磷](../Page/黄磷.md "wikilink")、[石灰乳](../Page/石灰乳.md "wikilink")、[氨水经两次](../Page/氨水.md "wikilink")[反应制得](../Page/化學反应.md "wikilink")。通常作生产[聚酰胺的](../Page/聚酰胺.md "wikilink")[催化剂和制造](../Page/催化剂.md "wikilink")[软焊剂](../Page/软焊剂.md "wikilink")（[焊接剛鐵用](../Page/焊接.md "wikilink")）等。

## 外部連結

  - [<http://www.chemyq.com/xz/xz4/38767>
    xonyn.htm](http://www.chemyq.com/xz/xz4/38767xonyn.htm)
  - [<http://www.chemblink.com/products/>
    7803-65-8.htm](http://www.chemblink.com/products/7803-65-8.htm)

[Category:銨鹽](../Category/銨鹽.md "wikilink")
[Category:次磷酸盐](../Category/次磷酸盐.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")