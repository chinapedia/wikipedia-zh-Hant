**欧内斯特·卢瑟福，第一代尼爾森的卢瑟福男爵**，[OM](../Page/功績勳章_\(英國\).md "wikilink")，[FRS](../Page/皇家學會院士.md "wikilink")（，），[新西兰](../Page/新西兰.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，世界知名的[原子核物理學之父](../Page/原子核物理學.md "wikilink")\[1\]。學術界公認他為繼[法拉第之後最偉大的實驗物理學家](../Page/麥可·法拉第.md "wikilink")\[2\]。

卢瑟福首先提出放射性[半衰期的概念](../Page/半衰期.md "wikilink")，證實[放射性涉及從一個元素到另一個元素的](../Page/放射性.md "wikilink")-{A|zh:[遷變](../Page/遷變.md "wikilink");zh-cn:[嬗变](../Page/嬗变.md "wikilink");zh-tw:[遷變](../Page/遷變.md "wikilink");}-。他又將放射性物質按照貫穿能力分類為[α射線與](../Page/α射線.md "wikilink")[β射線](../Page/β射線.md "wikilink")，並且證實前者就是[氦](../Page/氦.md "wikilink")[離子](../Page/離子.md "wikilink")\[3\]。因為「对元素蜕变以及放射化学的研究」，他榮獲1908年[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")\[4\]。

卢瑟福領導團隊成功地證實在[原子的中心有個](../Page/原子.md "wikilink")[原子核](../Page/原子核.md "wikilink")，創建了[卢瑟福模型](../Page/卢瑟福模型.md "wikilink")（行星模型）\[5\]。他最先成功地在[氮與α粒子的](../Page/氮.md "wikilink")[核反應裏將原子分裂](../Page/核反應.md "wikilink")，他又在同實驗裏發現了質子，並且為質子命名\[6\]。第104号元素为纪念他而命名为“[鑪](../Page/鑪.md "wikilink")”。

## 生平

1871年8月30日，卢瑟福生在[紐西蘭](../Page/紐西蘭.md "wikilink")[尼爾森附近的斯普林格罗夫](../Page/尼爾遜.md "wikilink")（现属）\[7\]，家裏有兄弟姊妹12人，他的父親從事生產枕木及切割亞麻的工作，小時候常在家裡的鋸木廠及亞麻廠幫忙，因此教育孩子的責任都落於母親身上。他國中就讀海夫洛克，畢業後他決定爭取尼尔森學院的獎學金，這段就學期間是他一生最難忘的回憶，在他臨終前仍不忘叮嚀他的太太要捐100磅給尼爾森學院，接著1890年他進入[坎特伯雷大学在那裏他遇見了他最敬仰的教授](../Page/坎特伯雷大学.md "wikilink")，在他們的引導下卢瑟福對於科學的研究更加強烈，并已经做了两年电子学的先锋研究。而後1891年他以"電磁研究"獲得科展的獎學金。

1895年，卢瑟福獲得獎學金，據聞這天他正在田裡挖馬鈴薯，卢瑟福得知考上獎學金，將手中的鐵鍬丟掉說：“這是我挖的最後一顆馬鈴薯了”。到[英国](../Page/英国.md "wikilink")[剑桥大学三一学院](../Page/剑桥大学三一学院.md "wikilink")[卡文迪许实验室做博士研究後](../Page/卡文迪许实验室.md "wikilink")，最開心的就是接受[约瑟夫·汤姆孙的指導](../Page/约瑟夫·汤姆孙.md "wikilink")。剛從紐西蘭到劍橋時，整日埋頭苦讀，被看作「光會挖土的野兔子」。在那里他暂短地保持了发现世界最长无线电波（2英里）的纪录，後來跟隨發現電子的[汤姆孙做研究](../Page/约瑟夫·汤姆孙.md "wikilink")。在研究物质放射性期间，他创造了术语：[α（阿尔法）和](../Page/α射线.md "wikilink")[β（贝塔）射线](../Page/β射线.md "wikilink")，又經測定發現[β射线是速度快](../Page/β射线.md "wikilink")、穿透力強的電子。

在1898年，卢瑟福被指派担任加拿大[麦吉尔大学物理系主任](../Page/麦吉尔大学.md "wikilink")，實驗中他發現了[放射性的](../Page/放射性.md "wikilink")[半衰期](../Page/半衰期.md "wikilink")，並將放射性物質命名為[α和](../Page/α射线.md "wikilink")[β射線](../Page/β射线.md "wikilink")，這項實驗被授予1908年的[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")，因為他調查到解體的元素、化學和放射性物質。他证明了放射性是[原子的自然](../Page/原子.md "wikilink")[衰变](../Page/衰变.md "wikilink")。但他不是很高興，因為他自認為是物理學家，而非化學家。他的一个名言是，“科学只有物理一个学科，其他不过相当于集邮活动而已”。他注意到在一个放射性物质样本裡，一半的样本衰变的时间几乎是不变的，
这就是该物质的“[半衰期](../Page/半衰期.md "wikilink")”，并且他还就此现象建立了一个实用的方法，以物质[半衰期作为时钟来检测地球的年龄](../Page/半衰期.md "wikilink")，结果证明地球要比大多数科学间认为的老得多。

在1907年他搬到英國已經是諾貝爾獎得主。1911年，在他的[金箔實驗中](../Page/盖革－马士登实验.md "wikilink")，藉由他發現和解釋[卢瑟福散射](../Page/卢瑟福散射.md "wikilink")，他推測原子的核心有正電荷集中，進而開創了[卢瑟福模型](../Page/卢瑟福模型.md "wikilink")。1909年卢瑟福在英國[曼徹斯特大學同他的學生马士登用](../Page/曼徹斯特大學.md "wikilink")[α粒子撞擊一片薄](../Page/α粒子.md "wikilink")[金箔](../Page/金箔.md "wikilink")，他發現大部分的粒子都能通過金箔，只有極少數會跳回。他笑說這是海軍用15吋巨砲射擊一張紙，但炮彈卻會被彈回而打到自己。最後他提出了一個類似於太陽系行星系統的原子模型，認為原子空間大都是空的，電子像行星圍繞原子核旋轉，推翻了當時所使用的梅子布丁原子模型。1911年3月，卢瑟福在曼徹斯特文學與哲學學會的會議上宣布他的意外發現，同年5月，他將論文發表於「哲學雜誌」。

### 晚年和荣誉

卢瑟福在1914年被授予[爵士爵位](../Page/下級勳位爵士.md "wikilink")。在[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，他研究潜艇探测的实际困难。1916年，他被授予。1919年，他回到卡文迪许实验室，继约瑟夫·汤姆孙之后成为实验室主任和卡文迪许教授。在他的指导下，[詹姆斯·查德威克因为发现了中子于](../Page/詹姆斯·查德威克.md "wikilink")1932年获得[诺贝尔奖](../Page/诺贝尔奖.md "wikilink")。[约翰·考克饶夫和](../Page/约翰·考克饶夫.md "wikilink")[欧内斯特·沃尔顿完成了一个利用](../Page/欧内斯特·沃尔顿.md "wikilink")[粒子加速器分裂原子的实验并在日后获得诺贝尔奖](../Page/粒子加速器.md "wikilink")。爱德华·阿普尔顿证明了[电子层的存在](../Page/电子层.md "wikilink")，并也在日后获得诺贝尔奖。他的學生中总共有丹麥的[玻尔](../Page/尼尔斯·玻尔.md "wikilink")、德國的[哈恩](../Page/奥托·哈恩.md "wikilink")、前蘇聯的[卡皮察等十位諾貝爾獎得主](../Page/彼得·卡皮察.md "wikilink")。1925年至1930年期间，他担任了[皇家学会会长以及后来的](../Page/皇家学会会长列表.md "wikilink")主席，帮助了来自德国的近1000名大学学者难民\[8\]。1925年卢瑟福獲得英國政府頒發[功績勳章](../Page/功績勳章_\(英國\).md "wikilink")，1931年被封為**尼尔森的卢瑟福男爵**\[9\]。他只有一位女兒，故爵位在他死後斷絕。

在早些时候，卢瑟福就患有轻微的[疝气](../Page/疝气.md "wikilink")，但他一直不太重视，没有好好治疗它，最后发展成绞窄性肠梗阻，使得他病得很严重。1937年，尽管在伦敦做过一次紧急手术，卢瑟福还是在剑桥于4天後死于医生所说的“肠麻痹”\[10\]。他的遗体在被火化\[11\]，之后被给以很高荣誉的葬在[西敏寺中](../Page/西敏寺.md "wikilink")，靠近[牛顿和其他著名英国科学家](../Page/艾萨克·牛顿.md "wikilink")\[12\]。

## 科学研究

### 金箔實驗

[Rutherford_gold_foil_experiment_results.svg](https://zh.wikipedia.org/wiki/File:Rutherford_gold_foil_experiment_results.svg "fig:Rutherford_gold_foil_experiment_results.svg")穿过未被扰动的[梅子布丁模型式的](../Page/梅子布丁模型.md "wikilink")[原子](../Page/原子.md "wikilink")。
*底部：*观测到的结果：少量的粒子被偏转，表明有[一个电荷集中的小核](../Page/原子核.md "wikilink")。
这里的图像并不是依照比例，实际中原子核远小于电子层。\]\]
1909年，卢瑟福和[汉斯·盖革以及](../Page/汉斯·盖革.md "wikilink")在英国曼彻斯特大学进行了[盖革－马士登实验](../Page/盖革－马士登实验.md "wikilink")\[13\]
。实验是用α粒子束轰击真空室中的[金箔](../Page/金.md "wikilink")。实验表明了原子具有核的特征。在实验中，卢瑟福灵感所至，要求盖革和马士登寻找具有大偏转角的[α粒子](../Page/α粒子.md "wikilink")。在当时，没有任何一个理论预期过这类粒子。这种稀有的偏向被观测到了，并被证明是平滑的但符合高阶偏向角函数。卢瑟福解释了这些数据，并在1911年总结出原子的[卢瑟福模型的公式](../Page/卢瑟福模型.md "wikilink")\[14\]。该模型认为，包含大部分原子质量的带正[电的](../Page/荷_\(物理\).md "wikilink")[小核被小质量的](../Page/原子核.md "wikilink")[电子](../Page/电子.md "wikilink")[环绕](../Page/轨道_\(力学\).md "wikilink")。

## 参考资料

## 外部链接

  - <http://www.rutherford.org.nz>
  - <http://www.dnzb.govt.nz>
  - <https://web.archive.org/web/20040604051314/http://www.orcbs.msu.edu/radiation/radhistory/ernestrutherford.html>
  - <http://www.pbs.org/wgbh/aso/databank/entries/bpruth.html>
  - <https://web.archive.org/web/20090302102542/http://www.nzedge.com/heroes/rutherford.html>
  - [卢瑟福和原子核的发现](http://psroc.phys.ntu.edu.tw/bimonth/download.php?d=1&cpid=157&did=12)

[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:紐西蘭諾貝爾獎獲得者](../Category/紐西蘭諾貝爾獎獲得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:英国皇家学会会长](../Category/英国皇家学会会长.md "wikilink")
[Category:英国皇家学会院士](../Category/英国皇家学会院士.md "wikilink")
[Category:英国物理学家](../Category/英国物理学家.md "wikilink")
[Category:英國化學家](../Category/英國化學家.md "wikilink")
[Category:紐西蘭物理學家](../Category/紐西蘭物理學家.md "wikilink")
[Category:核物理學家](../Category/核物理學家.md "wikilink")
[Category:麥吉爾大學教師](../Category/麥吉爾大學教師.md "wikilink")
[Category:劍橋大學三一學院校友](../Category/劍橋大學三一學院校友.md "wikilink")
[Category:坎特伯雷大學校友](../Category/坎特伯雷大學校友.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:聯合王國男爵](../Category/聯合王國男爵.md "wikilink")
[Category:紐西蘭裔英國人](../Category/紐西蘭裔英國人.md "wikilink")
[Category:蘇格蘭裔英格蘭人](../Category/蘇格蘭裔英格蘭人.md "wikilink")
[Category:蘇格蘭裔紐西蘭人](../Category/蘇格蘭裔紐西蘭人.md "wikilink")
[Category:英格蘭裔紐西蘭人](../Category/英格蘭裔紐西蘭人.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")
[Category:紙幣上的人物](../Category/紙幣上的人物.md "wikilink")
[Category:马泰乌奇奖章获得者](../Category/马泰乌奇奖章获得者.md "wikilink")
[Category:拉姆福德奖章获得者](../Category/拉姆福德奖章获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")

1.

2.
3.

4.  诺贝尔官方网站关于[欧内斯特·卢瑟福传记](http://nobelprize.org/nobel_prizes/chemistry/laureates/1908/rutherford.html).
    Retrieved on 2011-01-26.

5.

6.  [Ernest
    Rutherford](http://www.nzhistory.net.nz/people/ernest-rutherford).
    Nzhistory.net.nz (1937-10-19). Retrieved on 2011-01-26.

7.

8.

9.

10.

11.
12. [Heilbron, J. L. Ernest Rutherford and the Explosion of Atoms.
    Oxford: Oxford University Press, 2003;
    pp. 123–124.](http://books.google.com/books?id=_vNW1wg9npgC&pg=PA123&dq=ernest+rutherford+death+titled&hl=en&sa=X&ei=QrEDT_7WBMKhtwfruMHRBg&ved=0CDoQ6AEwAQ#v=onepage&q=ernest%20rutherford%20death%20titled&f=false)
    Accessed 3 January 2012.

13.

14.