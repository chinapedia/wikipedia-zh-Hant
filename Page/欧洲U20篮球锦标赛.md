欧洲U20篮球锦标赛自1992年起舉辦，逢[偶數年舉辦](../Page/偶數.md "wikilink")，2004年起改作每年舉辦。

<table>
<caption>Summaries</caption>
<colgroup>
<col style="width: 50%" />
<col style="width: 13%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>Host</p></th>
<th><p>Gold medal game</p></th>
<th><p>Bronze medal game</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>冠軍</p></td>
<td><p>Score</p></td>
<td><p>亞軍</p></td>
<td><p>季軍</p></td>
</tr>
<tr class="even">
<td><p>1992<br />
''<a href="../Page/1992_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/雅典.md" title="wikilink">雅典</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>65–63</strong></p></td>
</tr>
<tr class="odd">
<td><p>1994<br />
''<a href="../Page/1994_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/马里博尔.md" title="wikilink">马里博尔</a>, <a href="../Page/波斯托伊納.md" title="wikilink">波斯托伊納</a> &amp; <a href="../Page/卢布尔雅那.md" title="wikilink">卢布尔雅那</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>96–91</strong></p></td>
</tr>
<tr class="even">
<td><p>1996<br />
''<a href="../Page/1996_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Bursa.md" title="wikilink">Bursa</a> &amp; <a href="../Page/Istanbul.md" title="wikilink">Istanbul</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>85–81</strong></p></td>
</tr>
<tr class="odd">
<td><p>1998<br />
''<a href="../Page/1998_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Trapani.md" title="wikilink">Trapani</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>92–73</strong></p></td>
</tr>
<tr class="even">
<td><p>2000<br />
''<a href="../Page/2000_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Ohrid.md" title="wikilink">Ohrid</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>66–65</strong></p></td>
</tr>
<tr class="odd">
<td><p>2002<br />
''<a href="../Page/2002_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Kaunas.md" title="wikilink">Kaunas</a>, <a href="../Page/Alytus.md" title="wikilink">Alytus</a> &amp; <a href="../Page/Vilnius.md" title="wikilink">Vilnius</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>77–73</strong></p></td>
</tr>
<tr class="even">
<td><p>2004<br />
''<a href="../Page/2004_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Brno.md" title="wikilink">Brno</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>66–61</strong></p></td>
</tr>
<tr class="odd">
<td><p>2005<br />
''<a href="../Page/2005_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Chekhov,_Moscow_Oblast.md" title="wikilink">Chekhov</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>61–53</strong></p></td>
</tr>
<tr class="even">
<td><p>2006<br />
''<a href="../Page/2006_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/İzmir.md" title="wikilink">İzmir</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>64–58</strong></p></td>
</tr>
<tr class="odd">
<td><p>2007<br />
''<a href="../Page/2007_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Nova_Gorica.md" title="wikilink">Nova Gorica</a>)<br />
 (<a href="../Page/Gorizia.md" title="wikilink">Gorizia</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>87–78</strong></p></td>
</tr>
<tr class="even">
<td><p>2008<br />
''<a href="../Page/2008_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Riga.md" title="wikilink">Riga</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>96–89</strong></p></td>
</tr>
<tr class="odd">
<td><p>2009<br />
''<a href="../Page/2009_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Rhodes_(city).md" title="wikilink">Rhodes</a> &amp; <a href="../Page/Ialysos.md" title="wikilink">Ialysos</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>90–85</strong></p></td>
</tr>
<tr class="even">
<td><p>2010<br />
''<a href="../Page/2010_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Zadar.md" title="wikilink">Zadar</a>, <a href="../Page/Crikvenica.md" title="wikilink">Crikvenica</a> &amp; <a href="../Page/Makarska.md" title="wikilink">Makarska</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>73–62</strong></p></td>
</tr>
<tr class="odd">
<td><p>2011<br />
''<a href="../Page/2011_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Bilbao.md" title="wikilink">Bilbao</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>82–70</strong></p></td>
</tr>
<tr class="even">
<td><p>2012<br />
''<a href="../Page/2012_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Ljubljana.md" title="wikilink">Ljubljana</a>, <a href="../Page/Domžale.md" title="wikilink">Domžale</a> &amp; <a href="../Page/Kranjska_Gora.md" title="wikilink">Kranjska Gora</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>50–49</strong></p></td>
</tr>
<tr class="odd">
<td><p>2013<br />
''<a href="../Page/2013_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Tallinn.md" title="wikilink">Tallinn</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>67–60</strong></p></td>
</tr>
<tr class="even">
<td><p>2014<br />
''<a href="../Page/2014_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Heraklion.md" title="wikilink">Heraklion</a> &amp; <a href="../Page/Rethymno.md" title="wikilink">Rethymno</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>65–57</strong></p></td>
</tr>
<tr class="odd">
<td><p>2015<br />
''<a href="../Page/2015_FIBA_Europe_Under-20_Championship.md" title="wikilink">details</a></p></td>
<td><p>(<a href="../Page/Lignano_Sabbiadoro.md" title="wikilink">Lignano Sabbiadoro</a> &amp; <a href="../Page/Latisana.md" title="wikilink">Latisana</a>)</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>70–64</strong></p></td>
</tr>
</tbody>
</table>

## 外部链接

  - [欧洲U20篮球锦标赛](https://web.archive.org/web/20110710235724/http://u20men.fibaeurope.com/en/)
  - [欧洲20岁以下青年篮球锦标赛](https://web.archive.org/web/20070928112119/http://www.u20men2005.com/en/default.asp?cid=%7B4DF37B37-779E-40D3-AD5C-22108ECAFD6A%7D%2F)

[Category:歐洲籃球錦標賽](../Category/歐洲籃球錦標賽.md "wikilink")