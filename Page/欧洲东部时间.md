**欧洲东部时间**（**Eastern European
Time**，缩写**EET**）是比[世界标准时间](../Page/世界标准时间.md "wikilink")（UTC）早二个小时的[时区名称之一](../Page/时区.md "wikilink")。它被部分[欧洲国家](../Page/欧洲.md "wikilink")、[北非国家和](../Page/北非.md "wikilink")[中东国家采用](../Page/中东.md "wikilink")。其中大部分国家夏季采用[欧洲东部夏令时间](../Page/欧洲东部夏令时间.md "wikilink")。

## 使用国家

只有一个国家全年使用**欧洲东部时间**：

  - [利比亚](../Page/利比亚.md "wikilink")

下列国家冬季使用**欧洲东部时间**：

  - [白俄罗斯](../Page/白俄罗斯.md "wikilink")，1922年–1930期间，1991年开始。
  - [保加利亚](../Page/保加利亚.md "wikilink")，1894年开始。
  - [塞浦路斯](../Page/塞浦路斯.md "wikilink")
  - [埃及](../Page/埃及.md "wikilink")
  - [爱沙尼亚](../Page/爱沙尼亚.md "wikilink")，1921年–1940期间，1989年开始。
  - [芬兰](../Page/芬兰.md "wikilink")，1921年开始。
  - [雅典](../Page/雅典.md "wikilink")，1916年开始。
  - [以色列](../Page/以色列.md "wikilink")，1948年开始。
  - [约旦](../Page/约旦.md "wikilink")
  - [拉脱维亚](../Page/拉脱维亚.md "wikilink")，1926年–1940期间，1989年开始。
  - [黎巴嫩](../Page/黎巴嫩.md "wikilink")
  - [立陶宛](../Page/立陶宛.md "wikilink")，1920年，1989年开始。
  - [摩尔多瓦](../Page/摩尔多瓦.md "wikilink")，1924年–1940期间，1991年开始。
  - [巴勒斯坦](../Page/巴勒斯坦.md "wikilink")
  - [罗马尼亚](../Page/罗马尼亚.md "wikilink")，1931年开始。
  - [俄罗斯](../Page/俄罗斯.md "wikilink")
    （[加里宁格勒州](../Page/加里宁格勒州.md "wikilink")），1945年，1991年-2011年期间。
  - [叙利亚](../Page/叙利亚.md "wikilink")
  - [土耳其](../Page/土耳其.md "wikilink")，1910年开始，1978年-1985年暂停。
  - [乌克兰](../Page/乌克兰.md "wikilink")，1924年–1930期间，1990年开始。

[莫斯科在](../Page/莫斯科.md "wikilink")1922年到1930年和1991年到1992年使用欧洲东部时间，[波兰则在](../Page/波兰.md "wikilink")1918年到1922年使用这个时间。

[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，[德国将欧洲中部时间执行到所有它占领的区域](../Page/德国.md "wikilink")。

## 时区划分

地理因素以外，政治因素也是时区划分的原因。因此实际上的时区与经线并不完全吻合。欧洲东部时间（UTC+2）时区，根据地理学术语规定，位置为东经22°30'到东经37°30'之间的区域。而实际上，有些地理在欧洲东部时间/UTC+2时区的地区，使用的是其它时区；与此相反，有些地理上不在欧洲东部时间/UTC+2时区的地区，使用的却是欧洲东部时间。下面列出了这些地区：

### 位于东经22°30'以东，使用欧洲中部时间/UTC+1时区的国家（地理上为欧洲东部时间/UTC+2时区）

  - [马其顿共和国东部](../Page/马其顿共和国.md "wikilink")，包括[斯特鲁米察](../Page/斯特鲁米察.md "wikilink")。
  - [塞尔维亚最东部](../Page/塞尔维亚.md "wikilink")[皮鲁特地区](../Page/皮鲁特.md "wikilink")，包括皮鲁特市。
  - [匈牙利和](../Page/匈牙利.md "wikilink")[斯洛伐克最东部](../Page/斯洛伐克.md "wikilink")，分别与[乌克兰](../Page/乌克兰.md "wikilink")[外喀尔巴阡州的北部和南部相领](../Page/外喀尔巴阡州.md "wikilink")。
  - [波兰最东部](../Page/波兰.md "wikilink")，包括[卢布林和](../Page/卢布林.md "wikilink")[比亚韦斯托克](../Page/比亚韦斯托克.md "wikilink")。
  - [瑞典](../Page/瑞典.md "wikilink")[北博滕省最北部](../Page/北博滕省.md "wikilink")，包括[卡利克斯和](../Page/卡利克斯.md "wikilink")[哈帕兰达](../Page/哈帕兰达.md "wikilink")。
  - [挪威最北部](../Page/挪威.md "wikilink")，位于[芬兰以北](../Page/芬兰.md "wikilink")，包括[芬马克](../Page/芬马克.md "wikilink")。

### 位于东经22°30'以西，使用欧洲东部时间/UTC+2时区的国家（地理上为欧洲中部时间/UTC+1时区）

  - [雅典最西部](../Page/雅典.md "wikilink")，包括[帕特雷](../Page/帕特雷.md "wikilink")、[约阿尼纳和](../Page/约阿尼纳.md "wikilink")[科孚岛](../Page/科孚岛.md "wikilink")。
  - [保加利亚最西部](../Page/保加利亚.md "wikilink")，包括[维丁省和](../Page/维丁.md "wikilink")[丘斯滕迪尔省](../Page/丘斯滕迪尔.md "wikilink")。
  - [罗马尼亚最西部](../Page/罗马尼亚.md "wikilink")，包括[卡拉什-塞维林县](../Page/卡拉什-塞维林县.md "wikilink")、[蒂米什县](../Page/蒂米什县.md "wikilink")、[阿拉德县和](../Page/阿拉德县.md "wikilink")[比霍尔县](../Page/比霍尔县.md "wikilink")，还包括[梅赫丁茨县和](../Page/梅赫丁茨县.md "wikilink")[萨图马雷县的西部](../Page/萨图马雷县.md "wikilink")。
  - [乌克兰与](../Page/乌克兰.md "wikilink")[匈牙利和](../Page/匈牙利.md "wikilink")[斯洛伐克相邻](../Page/斯洛伐克.md "wikilink")[外喀尔巴阡州](../Page/外喀尔巴阡州.md "wikilink")。
  - [俄罗斯](../Page/俄罗斯.md "wikilink")[加里宁格勒大部分地区](../Page/加里宁格勒.md "wikilink")。
  - [立陶宛西部](../Page/立陶宛.md "wikilink")，包括[克莱佩达](../Page/克莱佩达.md "wikilink")、[陶拉盖和](../Page/陶拉盖.md "wikilink")[泰尔西艾](../Page/泰尔西艾.md "wikilink")。
  - [拉脱维亚西部](../Page/拉脱维亚.md "wikilink")，包括[利耶帕亚和](../Page/利耶帕亚.md "wikilink")[文茨皮尔斯](../Page/文茨皮尔斯.md "wikilink")。
  - [爱沙尼亚最西部的岛屿](../Page/爱沙尼亚.md "wikilink")：[萨列马岛和](../Page/萨列马岛.md "wikilink")[希乌马岛](../Page/希乌马岛.md "wikilink")
  - [芬兰的西南海岸](../Page/芬兰.md "wikilink")，包括[图尔库和](../Page/图尔库.md "wikilink")[奥兰群岛](../Page/奥兰_\(芬兰\).md "wikilink")，奥兰群岛也是整个欧洲使用欧洲东部时间最西的地方。

### 位于东经37°30'以东，使用欧洲东部时间/UTC+2时区的国家（地理上为UTC+3时区）

  - [土耳其东部](../Page/土耳其.md "wikilink")，包括[凡城](../Page/凡城.md "wikilink")，[迪亚巴克尔](../Page/迪亚巴克尔.md "wikilink")、[特拉布宗和](../Page/特拉布宗.md "wikilink")[马拉蒂亚](../Page/马拉蒂亚.md "wikilink")；还包括[加济安泰普东部](../Page/加济安泰普.md "wikilink")。从地理的角度讲，那个区域不属于欧洲，但是政治上属于欧洲。土耳其最东的城市是位于东经44°34'的Şemdinli。
  - [乌克兰的最东部](../Page/乌克兰.md "wikilink")，包括[卢甘斯克](../Page/卢甘斯克.md "wikilink")、[顿涅茨克和](../Page/顿涅茨克.md "wikilink")[马里乌波尔](../Page/马里乌波尔.md "wikilink")。

### 位于东经37°30'以西，使用UTC+4时区的国家（地理上为欧洲东部时间/UTC+2时区）

  - [俄罗斯](../Page/俄罗斯.md "wikilink")，[莫斯科以西地区](../Page/莫斯科.md "wikilink")，包括从[摩尔曼斯克到](../Page/摩尔曼斯克.md "wikilink")[别尔哥罗德的区域](../Page/别尔哥罗德.md "wikilink")，其中大城市有[圣彼得堡](../Page/圣彼得堡.md "wikilink")、[大诺夫哥罗德和](../Page/大诺夫哥罗德.md "wikilink")[普斯科夫等](../Page/普斯科夫.md "wikilink")。

由于2011年俄罗斯调整时区，取消冬令时，统一使用夏令时，因此上述地区改为全年使用UTC+4时区。加里宁格勒地区则改为全年使用UTC+3时区。

## 参见

  - [时区](../Page/时区.md "wikilink")
  - [夏令时](../Page/夏令时.md "wikilink")
  - [UTC+1](../Page/UTC+1.md "wikilink")
  - [欧洲西部时间](../Page/欧洲西部时间.md "wikilink")
  - [欧洲中部时间](../Page/欧洲中部时间.md "wikilink")

## 参考

  - [世界时区网-欧洲时区](http://www.worldtimezone.com/time-europe24.php)
  - [世界时区表](http://www.designschulz.de/2002pre1/links_zeitzonen.php)

[Category:歐洲時間](../Category/歐洲時間.md "wikilink")
[Category:中東地理](../Category/中東地理.md "wikilink")