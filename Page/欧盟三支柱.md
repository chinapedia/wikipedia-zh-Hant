[Flag_of_Europe.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Europe.svg "fig:Flag_of_Europe.svg")

\-{zh-hk:歐盟三支柱;zh-cn:欧盟三支柱;zh-tw:歐盟三支柱}-，是根据1992年的《[欧洲联盟条约](../Page/欧洲联盟条约.md "wikilink")》，分別為：

1.  第一支柱为“[欧洲各共同体](../Page/欧洲各共同体.md "wikilink")”，涉及[经济](../Page/经济.md "wikilink")、[社会](../Page/社会.md "wikilink")、[环境等](../Page/环境.md "wikilink")[政策](../Page/政策.md "wikilink")。
2.  第二支柱为“[共同外交与安全政策](../Page/共同外交与安全政策.md "wikilink")”，涉及[外交](../Page/外交.md "wikilink")、[军事等政策](../Page/军事.md "wikilink")。
3.  第三支柱为“[刑事案件的警察與司法合作](../Page/刑事领域警务与司法合作.md "wikilink")”，涉及共同合作打击[刑事](../Page/刑事.md "wikilink")[犯罪](../Page/犯罪.md "wikilink")。该支柱前身是“[司法与内政合作](../Page/司法与内政合作.md "wikilink")”。

隨著2007年簽訂、**2009年12月1日**生效的[里斯本條約將歐盟政治領域事務做了更緊密也更廣泛的安排](../Page/里斯本條約.md "wikilink")，**歐盟三支柱的名稱**不再適合用來描述歐盟任務，因此**不再被使用**。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/欧洲联盟.md" title="wikilink">欧洲联盟</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/欧洲各共同体.md" title="wikilink">欧洲各共同体</a>（EC）</p></td>
</tr>
<tr class="odd">
<td><ul>
<li><a href="../Page/关税同盟.md" title="wikilink">关税同盟和</a><a href="../Page/统一市场.md" title="wikilink">统一市场</a></li>
<li><a href="../Page/共同农业政策.md" title="wikilink">共同农业政策</a></li>
<li><a href="../Page/共同渔业政策.md" title="wikilink">共同渔业政策</a></li>
<li><a href="../Page/经济和货币联盟.md" title="wikilink">经济和货币联盟</a></li>
<li><a href="../Page/欧盟盟籍.md" title="wikilink">欧盟盟籍</a></li>
<li><a href="../Page/教育.md" title="wikilink">教育和</a><a href="../Page/文化.md" title="wikilink">文化</a></li>
<li><a href="../Page/泛欧网络.md" title="wikilink">泛欧网络</a></li>
<li><a href="../Page/消费者保护.md" title="wikilink">消费者保护</a></li>
<li><a href="../Page/保健.md" title="wikilink">保健</a></li>
<li><a href="../Page/研究.md" title="wikilink">研究</a></li>
<li><a href="../Page/环境政策.md" title="wikilink">环境政策</a></li>
<li><a href="../Page/社会政策.md" title="wikilink">社会政策</a></li>
<li><a href="../Page/难民政策.md" title="wikilink">难民政策</a></li>
<li><a href="../Page/申根协定.md" title="wikilink">申根协定</a></li>
<li><a href="../Page/移民政策.md" title="wikilink">移民政策</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p> </p></td>
</tr>
</tbody>
</table>

[Category:欧盟](../Category/欧盟.md "wikilink")