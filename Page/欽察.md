[Premongol-Kipchak.png](https://zh.wikipedia.org/wiki/File:Premongol-Kipchak.png "fig:Premongol-Kipchak.png")

<table>
<tbody>
<tr class="odd">
<td><p>又譯</p></td>
<td><p>克普恰克</p></td>
</tr>
<tr class="even">
<td><p>常見拉丁轉寫</p></td>
<td><p><em>Kipchaks</em>、<em>Kypchaks</em>[1]、<br />
<em>Qipchaqs</em>、<em>Qypchaqs</em></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烏孜別克語.md" title="wikilink">烏孜別克語</a>：</p></td>
<td><p>Qipchoq, Қипчоқ</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈薩克語.md" title="wikilink">哈薩克語</a>：</p></td>
<td><p>Қыпшақ</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉爾吉斯語.md" title="wikilink">吉爾吉斯語</a>：</p></td>
<td><p>Кыпчак</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庫梅克語.md" title="wikilink">庫梅克語</a>：</p></td>
<td><p>Къыпчакъ</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/諾蓋語.md" title="wikilink">諾蓋語</a>：</p></td>
<td><p>Кыпчак</p></td>
</tr>
<tr class="odd">
<td><p>(<em>polovtsy</em>)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>(<em>polovtsy</em>)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拜占庭帝國.md" title="wikilink">拜占庭帝國</a>：</p></td>
<td><p><em>Kuman</em>或<em>Cuman</em>[2]</p></td>
</tr>
</tbody>
</table>

**欽察**是古代[中亞地區的](../Page/中亞.md "wikilink")[突厥語民族之一](../Page/突厥語族.md "wikilink")。欽察部落聯盟在11世紀中葉時曾經佔據[黑海北濱的](../Page/黑海.md "wikilink")[欽察草原](../Page/欽察草原.md "wikilink")，其中一些部落则与**庫曼人**合併，在[突厥遷移中遷移到了](../Page/突厥遷移.md "wikilink")[西伯利亞西部](../Page/西伯利亞.md "wikilink")，後來[蒙古帝國所建立的](../Page/蒙古帝國.md "wikilink")[欽察汗國部分領土即欽察人的原居地](../Page/欽察汗國.md "wikilink")。一些[阿拉伯学者把](../Page/阿拉伯.md "wikilink")[咸海至](../Page/咸海.md "wikilink")[烏克蘭以北稱欽察草原一带的地方稱為庫曼尼亞](../Page/烏克蘭.md "wikilink")，因為這地區幾乎只有欽察-庫曼人居住。在[波斯历史著作](../Page/波斯.md "wikilink")《[史集](../Page/史集.md "wikilink")》中，“钦察”意为空心树。

## 歷史

欽察人祖先是由[烏揭與](../Page/烏揭.md "wikilink")[薩爾馬特人等融合而成](../Page/薩爾馬特人.md "wikilink")，欽察有四支十一部落。四支是[哈拉欽察](../Page/哈拉欽察.md "wikilink")、[契丹欽察](../Page/契丹欽察.md "wikilink")、[庫蘭欽察](../Page/庫蘭欽察.md "wikilink")、[托里欽察](../Page/托里欽察.md "wikilink")。十一部落是[脱克撒巴](../Page/脱克撒巴.md "wikilink")（與[突騎施有關](../Page/突騎施.md "wikilink")）、[葉迪牙](../Page/葉迪牙.md "wikilink")、[不兒只烏格拉](../Page/不兒只烏格拉.md "wikilink")、[额勒别儿里](../Page/额勒别儿里.md "wikilink")（即玉里伯里部）、[晃火兒烏格立](../Page/晃火兒烏格立.md "wikilink")、[安徹烏格立](../Page/安徹烏格立.md "wikilink")、[都鲁惕](../Page/都鲁惕.md "wikilink")、[非剌納烏格立](../Page/非剌納烏格立.md "wikilink")、[者思難](../Page/者思難.md "wikilink")、[哈剌孛兒克里](../Page/哈剌孛兒克里.md "wikilink")。

[蠻族入侵時期](../Page/欧洲民族大迁徙.md "wikilink")，[匈人](../Page/匈人.md "wikilink")[阿提拉軍隊的主力](../Page/阿提拉.md "wikilink")，由匈人、欽察人、[咄陸人](../Page/咄陸.md "wikilink")，與其他突厥人構成。

1237年，[蒙古第二次西征](../Page/蒙古第二次西征.md "wikilink")，[伏爾加河](../Page/伏爾加河.md "wikilink")（窩瓦河）的欽察[斡勒不兒里克部落首領](../Page/斡勒不兒里克.md "wikilink")[八赤蠻拒絕投降](../Page/八赤蠻.md "wikilink")，被蒙古人[腰斬殺死](../Page/腰斬.md "wikilink")，[欽察聯盟解體](../Page/欽察聯盟.md "wikilink")，大部分國土併入[金帳汗國](../Page/金帳汗國.md "wikilink")，欽察人成為[哈薩克人](../Page/哈薩克人.md "wikilink")，而因部長[亦納思包庇](../Page/亦納思.md "wikilink")[蔑兒乞人首腦](../Page/蔑兒乞.md "wikilink")[忽都](../Page/忽都.md "wikilink")，被[速不台所敗](../Page/速不台.md "wikilink")，其人多數逃亡[俄羅斯](../Page/俄羅斯.md "wikilink")，其他的走投無路，逃亡匈牙利，欽察政權永久消失。一部分欽察人被編入[元帝國](../Page/元帝國.md "wikilink")[忽必烈的欽察軍](../Page/忽必烈.md "wikilink")[龙翊卫](../Page/龙翊卫.md "wikilink")，由[土土哈統帥](../Page/土土哈.md "wikilink")。在[埃及](../Page/埃及.md "wikilink")[馬木留克王朝前期](../Page/馬木留克王朝.md "wikilink")，欽察奴隸大受歡迎，這些人中最著名的是[拜巴爾一世](../Page/拜巴爾一世.md "wikilink")。欽察部中的玉里伯里是相當著名的部落，[德里蘇丹國的首一個王朝的蘇丹是出自此部落的奴隸](../Page/德里蘇丹國.md "wikilink")。

而**庫曼人**（）\[3\]，詞意為“淺色皮膚”，他們亦是欽察的一部，位於欽察的西部，即今日的[俄羅斯南部](../Page/俄羅斯.md "wikilink")。他們夏季在[伏尔加保加利亚一帶放牧](../Page/伏尔加保加利亚.md "wikilink")，冬天在[八剌沙袞過冬](../Page/八剌沙袞.md "wikilink")，有不多的人種地。7世紀時臣服於[西突厥](../Page/西突厥.md "wikilink")，9至11世紀臣服於[基馬克人](../Page/基馬克.md "wikilink")。汗國瓦解後并入[克普恰克汗國](../Page/克普恰克汗國.md "wikilink")，以伏爾加河為界，分東西二部。東部庫曼包括[葛邏祿](../Page/葛邏祿.md "wikilink")、[康里](../Page/康里.md "wikilink")、黑契丹（占领了[圖爾蓋草原盆地](../Page/图尔盖河.md "wikilink")，乌拉尔，伏尔加河，由Elbori领导。汗帐在图尔盖，现今哈萨克斯坦阿克托别东部地区）。西部庫曼包括[保加爾人與](../Page/保加爾人.md "wikilink")[巴什基爾人](../Page/巴什基爾人.md "wikilink")。他们最東南至[塔拉斯](../Page/塔拉斯.md "wikilink")（现今的哈萨克斯坦阿拉木图州）、东哈萨克斯坦州、南哈萨克省、科斯塔奈州、阿克莫拉州、巴甫洛达尔州、卡拉干达州、克孜勒欧尔达州，包括从锡尔河、额尔齐斯河和[伊希姆河](../Page/伊希姆河.md "wikilink")，直达西伯利亚丛林。這部落是欽察部中最著名的，有些庫曼人受蒙古人逼迫前往[匈牙利](../Page/匈牙利.md "wikilink")，人數大約4万帐20万人，永久和[馬札兒人同化](../Page/馬札兒人.md "wikilink")。歐洲人也因此以“庫曼”稱呼欽察人。但在俄羅斯及[烏克蘭卻被稱為](../Page/烏克蘭.md "wikilink")**波洛韦茨人**（）。

有幾個證據能說明欽察人不是穆斯林。例如[志費尼的](../Page/志費尼.md "wikilink")《[世界征服者史](../Page/世界征服者史.md "wikilink")》中說一個故事，[元太宗時禁止穆斯林自行宰羊](../Page/元太宗.md "wikilink")，而有個回回人買了一頭羊宰殺，一個欽察人從屋頂跳下把他送官。另一件事是巴托爾德提出他們與[格魯吉亞人聯攻穆斯林](../Page/格魯吉亞.md "wikilink")。第三件事是[鲁不鲁乞出使蒙古時](../Page/鲁不鲁乞.md "wikilink")，看到他們的墳地有一個雕塑，舉起一個酒杯。真正的穆斯林是指在[花剌子模國北方的欽察部落](../Page/花剌子模.md "wikilink")（），其他要等15世紀才接受伊斯蘭教。

現在欽察人分布在[南哈薩克斯坦州與](../Page/南哈薩克斯坦州.md "wikilink")[卡拉干達州](../Page/卡拉干達州.md "wikilink")，人口180萬。

## 語言

欽察人操[突厥語](../Page/突厥語.md "wikilink")，最重要的記錄是一部13世紀後期的庫曼語辭典\[4\]，是一本拉丁語與欽察語的詞典。驻留在埃及、說突厥语的[馬木留克人的存在](../Page/馬木留克.md "wikilink")，也刺激了欽察－阿拉伯语字典的汇编和研讨，這對多種古突厥语的研究有重大幫助。

根據《[突厥语大词典](../Page/突厥语大词典.md "wikilink")》的作者[麻赫穆德·喀什噶里在他的著作裡記載](../Page/麻赫穆德·喀什噶里.md "wikilink")：[基馬克與](../Page/基馬克.md "wikilink")[烏古斯人的突厥語根其他地方的突厥語有一個顯著的分別](../Page/烏古斯人.md "wikilink")，就是他們會把字首的*i*,
*y*讀作*j*（即*dj*）。

## 族群

現在突厥語西北語支名為[欽察語支](../Page/欽察語.md "wikilink")，有些突厥部落是他們的後人：[西伯利亞韃靼人](../Page/西伯利亞韃靼人.md "wikilink")（他們是一部分蒙古時代的[塔塔兒人與欽察及當地土著的混血種](../Page/塔塔兒.md "wikilink")）、[諾蓋人](../Page/諾蓋人.md "wikilink")、[哈薩克人](../Page/哈薩克.md "wikilink")、[克里米亞韃靼人](../Page/克里米亞韃靼人.md "wikilink")、[卡拉恰伊人](../Page/卡拉恰伊人.md "wikilink")、[庫梅克人](../Page/庫梅克人.md "wikilink")、[卡拉伊姆人](../Page/卡拉伊姆人.md "wikilink")、[卡拉卡爾帕克族](../Page/卡拉卡爾帕克族.md "wikilink")、[烏茲別克人](../Page/烏茲別克人.md "wikilink")、[吉爾吉斯人](../Page/吉爾吉斯人.md "wikilink")、[伏爾加韃靼人](../Page/伏爾加韃靼人.md "wikilink")、[巴什基爾人](../Page/巴什基爾人.md "wikilink")。克里米亞韃靼人和[哈萨克人的地理位置](../Page/哈萨克人.md "wikilink")、生活方式、語言（哈薩克語是欽察語的方言），保留了最多欽察人特色。

[哈萨克人與](../Page/哈萨克.md "wikilink")[吉爾吉斯人有欽察部落](../Page/吉爾吉斯.md "wikilink")，克里米亞有一條村名為欽察，欽察一名也出現在[烏古斯可汗傳說的史詩中](../Page/烏古斯可汗.md "wikilink")。蒙古有一部落哈剌赤，據說與土土哈的父親[班都察有關](../Page/班都察.md "wikilink")，他全部投降，這部落善於製做黑馬乳，因此命名為[哈剌赤](../Page/哈剌赤.md "wikilink")。匈牙利現在仍然有一部落自稱欽察（克普恰克）的後人，人數大約20萬。他們是當年隨[忽炭汗前往匈牙利的欽察人之後](../Page/忽炭汗.md "wikilink")。[四川](../Page/四川.md "wikilink")[西昌的部分俞姓被認為是欽察人](../Page/西昌.md "wikilink")[土土哈之後](../Page/土土哈.md "wikilink")，俞是玉里伯里的對音，但人數不詳。钦察也是[回族来源之一](../Page/回族.md "wikilink")。在[準噶爾汗國時代還有欽察人生活在費爾干納](../Page/準噶爾汗國.md "wikilink")。

## 宗教

在近歐的一些欽察人，於11世紀左右時由於曾與[格魯吉亞人聯合來對抗伊斯蘭勢力的入侵](../Page/格魯吉亞人.md "wikilink")，在格魯吉亞人的影響下，有相當數量的人口改信基督教。這些人在格魯吉亞的建國者[大衛四世的要求下](../Page/大衛四世.md "wikilink")[受浸](../Page/受浸.md "wikilink")，而大衛四世本身亦迎娶了欽察汗[Otrok的女兒](../Page/:en:Otrok.md "wikilink")[古兰杜赫特為妻](../Page/古兰杜赫特.md "wikilink")。從1120年開始，欽察汗國以[基督教為國教](../Page/基督教.md "wikilink")，並有自己的國家教會和神職人員。\[5\]不過，到了12世紀至13世紀期間，伊斯蘭勢力已在[花剌子模國北部欽察內深深地扎根了](../Page/花剌子模.md "wikilink")\[6\]。

## 註釋

## 參考文獻

  - [劉迎勝](../Page/劉迎勝.md "wikilink")，《元史》
  - [多桑蒙古史](../Page/多桑蒙古史.md "wikilink")

[Category:古代族群](../Category/古代族群.md "wikilink")
[Category:突厥人](../Category/突厥人.md "wikilink")

1.  Grousset, René. "*The Empire of the Steppes: A History of Central
    Asia*". [Rutgers University
    Press](../Page/Rutgers_University_Press.md "wikilink"), 1988.
    [*page 185*](http://books.google.com/books?id=CHzGvqRbV_IC&pg=PA185&dq=kipchaks+turkic&hl=tr&sig=qIEzuC0Yde1tAmru-ng4DUjmS0U)
2.  [大英百科全書在線](../Page/大英百科全書.md "wikilink") -
    [*Kipchak*](http://www.britannica.com/eb/article-9045588/Kipchak)
3.  Encyclopædia Britannica Online -
    [*Cuman*](http://www.britannica.com/eb/article-9028174/Cuman)
4.  英語維基原文：*Codex Cumanicus*，是否應為《[庫曼人法典](../Page/庫曼人法典.md "wikilink")》？
5.
6.  [Islamic
    Civilization](http://www.islamset.com/islam/civil/kipchak.html)