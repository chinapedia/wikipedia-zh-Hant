**歌德獎**（****）是一項高榮譽的[德國](../Page/德國.md "wikilink")[文學獎](../Page/文學獎.md "wikilink")（並不限制只有作家方能得獎），以德國[作家](../Page/作家.md "wikilink")[歌德命名](../Page/约翰·沃尔夫冈·冯·歌德.md "wikilink")。最初為一年一度頒獎，但後來改為三年一度。

## 得主

以下為歷年得主：

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得主</p></th>
<th><p>国籍</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1927年</p></td>
<td><p><a href="../Page/斯特凡·乔治.md" title="wikilink">斯特凡·乔治</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1928年</p></td>
<td><p><a href="../Page/阿尔伯特·史怀哲.md" title="wikilink">阿尔伯特·史怀哲</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1929年</p></td>
<td><p>Leopold Ziegler</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1930年</p></td>
<td><p><a href="../Page/西格蒙德·佛洛伊德.md" title="wikilink">西格蒙德·佛洛伊德</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1931年</p></td>
<td><p><a href="../Page/格哈特·霍普特曼.md" title="wikilink">格哈特·霍普特曼</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1932年</p></td>
<td><p>Jacques Brosse</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1933年</p></td>
<td><p>Hermann Stehr</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1934年</p></td>
<td><p><a href="../Page/汉斯·普菲茨纳.md" title="wikilink">汉斯·普菲茨纳</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1935年</p></td>
<td><p>Hermann Stegemann</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1936年</p></td>
<td><p><a href="../Page/格奧爾格·科爾貝.md" title="wikilink">格奧爾格·科爾貝</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1937年</p></td>
<td><p>Erwin Guido Kolbenheyer</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1938年</p></td>
<td><p><a href="../Page/漢斯·卡羅薩.md" title="wikilink">漢斯·卡羅薩</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1939年</p></td>
<td><p><a href="../Page/卡爾·博施.md" title="wikilink">卡爾·博施</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1940年</p></td>
<td><p><a href="../Page/阿格涅·米蓋爾.md" title="wikilink">阿格涅·米蓋爾</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1941年</p></td>
<td><p>Wilhelm Schäfer</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1942年</p></td>
<td><p><a href="../Page/里夏德·库恩.md" title="wikilink">里夏德·库恩</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1945年</p></td>
<td><p><a href="../Page/马克斯·普朗克.md" title="wikilink">马克斯·普朗克</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1946年</p></td>
<td><p><a href="../Page/赫尔曼·黑塞.md" title="wikilink">赫尔曼·黑塞</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1947年</p></td>
<td><p><a href="../Page/卡尔·雅斯贝尔斯.md" title="wikilink">卡尔·雅斯贝尔斯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1948年</p></td>
<td><p>Fritz von Unruh</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1949年</p></td>
<td><p><a href="../Page/托马斯·曼.md" title="wikilink">托马斯·曼</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1952年</p></td>
<td><p>Carl Zuckmayer</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1954年</p></td>
<td><p><a href="../Page/提歐多·布魯格施.md" title="wikilink">提歐多·布魯格施</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1955年</p></td>
<td><p><a href="../Page/安妮特·柯爾伯.md" title="wikilink">安妮特·柯爾伯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1958年</p></td>
<td><p><a href="../Page/卡尔·冯·魏茨泽克.md" title="wikilink">卡尔·冯·魏茨泽克</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1960年</p></td>
<td><p>Ernst Beutler</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1961年</p></td>
<td><p><a href="../Page/沃爾特·格羅佩斯.md" title="wikilink">沃爾特·格羅佩斯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1964年</p></td>
<td><p>Benno Reifenberg</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1967年</p></td>
<td><p>Carlo Schmid</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1970年</p></td>
<td><p><a href="../Page/卢卡奇·捷尔吉.md" title="wikilink">卢卡奇·捷尔吉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1973年</p></td>
<td><p>Arno Schmidt</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/英格玛·伯格曼.md" title="wikilink">英格玛·伯格曼</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p><a href="../Page/雷蒙·阿隆.md" title="wikilink">雷蒙·阿隆</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p><a href="../Page/恩斯特·荣格.md" title="wikilink">恩斯特·荣格</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/戈洛·曼.md" title="wikilink">戈洛·曼</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p>Peter Stein</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/維斯拉瓦·辛波絲卡.md" title="wikilink">維斯拉瓦·辛波絲卡</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/恩斯特·贡布里希.md" title="wikilink">恩斯特·贡布里希</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>Hans Zender</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/齐格飞·蓝茨.md" title="wikilink">齐格飞·蓝茨</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>Marcel Reich-Ranicki</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/阿摩斯·奥兹.md" title="wikilink">阿摩斯·奥兹</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/碧娜·鮑許.md" title="wikilink">碧娜·鮑許</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/阿里·艾哈迈德·赛义德·伊斯比尔.md" title="wikilink">阿多尼斯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>Peter von Matt</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [德國文學](../Page/德國文學.md "wikilink")
  - [文學獎列表](../Page/文學獎列表.md "wikilink")
  - [Hanseatic Goethe
    Prize](../Page/Hanseatic_Goethe_Prize.md "wikilink")

[Category:德国文学奖](../Category/德国文学奖.md "wikilink")
[Category:約翰·沃爾夫岡·馮·歌德](../Category/約翰·沃爾夫岡·馮·歌德.md "wikilink")