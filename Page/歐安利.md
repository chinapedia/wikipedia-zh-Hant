**歐安利**（，\[1\]），[中國籍](../Page/中國籍.md "wikilink")[澳門土生葡人](../Page/澳門土生葡人.md "wikilink")。[澳門行政會成員](../Page/澳門行政會.md "wikilink")（2004年-），前[澳門立法會議員及第一秘書](../Page/澳門立法會.md "wikilink")
（1996至2009年\[2\]）；2001年獲[澳門政府授予](../Page/澳門政府.md "wikilink")[專業功績勳章](../Page/專業功績勳章.md "wikilink")、第十一屆[全國政協委員](../Page/全國政協.md "wikilink")。

歐安利職業為[律師和私人](../Page/律師.md "wikilink")[公證員](../Page/公證員.md "wikilink")。曾任第三、四、五屆[澳門立法會議員](../Page/澳門立法會.md "wikilink")。

2004年，歐安利放棄葡萄牙國籍後獲委任為中國全國政協澳區委\[3\]。2017年，在議會任職33年的歐安利決定放棄連任\[4\]。

## 參考文獻

[Category:全國政協委員](../Category/全國政協委員.md "wikilink")
[Category:澳門立法會議員](../Category/澳門立法會議員.md "wikilink")
[Category:土生葡人](../Category/土生葡人.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:澳門行政會委員](../Category/澳門行政會委員.md "wikilink")
[A](../Category/里斯本大學校友.md "wikilink")
[Category:归化中华人民共和国之澳门居民](../Category/归化中华人民共和国之澳门居民.md "wikilink")
[Category:第十屆全國政協委員](../Category/第十屆全國政協委員.md "wikilink")
[Category:第十一屆全國政協委員](../Category/第十一屆全國政協委員.md "wikilink")
[Category:第十二屆全國政協委員](../Category/第十二屆全國政協委員.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.
2.
3.
4.  句號報，《Leonel Alves deixa Assembleia Legislativa no final do
    mandato》，2017年6月21日