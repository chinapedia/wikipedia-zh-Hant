**歐德理**（[德語](../Page/德語.md "wikilink")： Ernst Johann Eitel / Ernest John
Eitel），()，[德國人](../Page/德國.md "wikilink")。 1838年，
歐德理出生於德國的[符騰堡](../Page/符騰堡.md "wikilink")。1860年，[圖賓根大學文學碩士畢業](../Page/圖賓根大學.md "wikilink")。1879年，
加入香港政府， 香港歷史上第一位入[英籍的外國人](../Page/英.md "wikilink")。1897年退休，
並移居[澳洲任牧師](../Page/澳洲.md "wikilink")。1908年辭世，
終年70歲。他是殖民地年代[香港政府中文秘書的華人事務專家](../Page/香港殖民地時期#香港政府.md "wikilink")、「[中國通](../Page/中國通.md "wikilink")」，編有中英字典、廣東話發音專書，愛好中國[風水](../Page/風水.md "wikilink")，曾任洋務參贊，[香港最高法院](../Page/香港最高法院.md "wikilink")[傳譯員](../Page/傳譯員.md "wikilink")，對[保良局的成立有貢獻](../Page/保良局.md "wikilink")。在[軒尼詩港督年代](../Page/軒尼詩.md "wikilink")，他比[伍廷芳更早入英籍](../Page/伍廷芳.md "wikilink")。

## 使命

他服務於[符騰堡福音教會作牧師](../Page/符騰堡.md "wikilink")。他後來到[廣東省](../Page/廣東省.md "wikilink")[新安县傳播福音](../Page/新安县.md "wikilink")。1865年4月，他轉到[廣州](../Page/廣州.md "wikilink")[倫敦会來接管](../Page/倫敦会.md "wikilink")[博羅福音會及](../Page/博羅.md "wikilink")[廣州以外的](../Page/廣州.md "wikilink")[客家村落](../Page/客家.md "wikilink")。
1870年1月，他搬到香港，但仍然掌管博羅福音會。 1875年他成為中文研究主任。1879年4月，他辞任倫敦会牧師一職。

他在香港成為校監，後是[軒尼詩的中文秘書](../Page/軒尼詩.md "wikilink")。1866年，他娶了女性教育會的伊頓女士 (
Mary Anne Winifred Eaton，[曰字樓女館前校長](../Page/曰字樓女館.md "wikilink")
)。1908年他在[澳洲](../Page/澳洲.md "wikilink")[阿德萊德逝世](../Page/阿德萊德.md "wikilink")，享年70歲。

## 粵語詞典

歐德理也在1877年出版了他的[粵語詞典](../Page/粵語.md "wikilink")，名為*Chinese Dictionary in
the Cantonese Dialect* ( 粵語中文字典 ) 。它基於1856年威廉斯 ( Samuel Wells Williams )
的*Tonic Dictionary of the Chinese Language in the Canton Dialect*
。其作品後被訂為粵語標準。其作品被[黃錫凌在其作品](../Page/黃錫凌.md "wikilink")[粵音韻彙中批評](../Page/粵音韻彙.md "wikilink")，指其作品不準确。

## 參考文獻

  - *Feng-Shui, Or, The Rudiments Of Natural Science In China* (1873年)
  - *A Chinese dictionary in the Cantonese dialect* (1877年)
  - *The central school : can it justify its raison d'etre?* (1877年)
  - *Buddhism : its historical, theoretical and popular aspects* (1884年)
  - *Chinese School-Books* (1893年)
  - *Europe In China : The History Of Hongkong From The Beginning To The
    Year 1882* (1895年)
  - 保良局董事局編纂(1978) 《香港保良局百年史略：1878 - 1978》，香港：保良局，第315頁

[Category:香港歷史人物](../Category/香港歷史人物.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:蒂賓根大學校友](../Category/蒂賓根大學校友.md "wikilink")