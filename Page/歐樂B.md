**歐樂B**（）是一個口腔護理品牌，产品包括[牙刷](../Page/牙刷.md "wikilink")、[牙膏](../Page/牙膏.md "wikilink")、[漱口水和](../Page/漱口水.md "wikilink")[牙線](../Page/牙線.md "wikilink")。

## 歷史

### 1980年代前

  - 1921年：[麥斯·百靈在](../Page/麥斯·百靈.md "wikilink")[德國](../Page/德國.md "wikilink")[法蘭克福開創一個電器工場](../Page/法蘭克福.md "wikilink")。
  - 1950年：[加州](../Page/加州.md "wikilink")[牙周病醫生](../Page/牙周病學.md "wikilink")[羅伯·休斯頓設計出首支歐樂B牙刷](../Page/羅伯·休斯頓.md "wikilink")。該支牙刷因擁有60束刷毛而名為歐樂B
    60。休斯頓醫生開設一家小型家族公司售賣牙刷。
  - 1958年：百靈獲得來自[紐約](../Page/紐約.md "wikilink")[現代藝術博物館與](../Page/現代藝術博物館.md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")[世界博覽會的榮譽](../Page/世界博覽會.md "wikilink")。
  - 1963年：百靈開發出名為「Mayadent」的電動牙刷原型。
  - 1967年：吉列公司收購百靈公司。
  - 1969年：歐樂B牙刷在[阿波羅11號任務中首次登陸月球](../Page/阿波羅11號.md "wikilink")。
  - 1978年：首支模擬手動牙刷的左右轉動，名為「D1」的電動牙刷大量生產並在歐洲大賣。

### 1980年代

  - 1981年：歐樂B「Right Angle」發售。
  - 1984年：吉列收購歐樂B，百靈發售首支結合垂直與水平動作的電動牙刷。兒童用[星球大戰牙刷發售](../Page/星球大戰.md "wikilink")。
  - 1986年：兒童用「Muppets」牙刷與牙膏發售。
  - 1987年：首次與百靈合作設計的產品歐樂B「Ultra Plus」發售。
  - 1991年：D5發售，臨床證明比手動手刷更能清潔牙齒。歐樂B發售顯示型刷毛牙刷。歐樂B收購智利Duralon公司。
  - 1992年：擁有電動牙刷與[沖牙機的歐樂B](../Page/沖牙機.md "wikilink")「Plaque Control
    Center OC5」發售。
  - 1993年：每排的刷毛高低不一的歐樂B「全接觸牙刷」（Advantage）牙刷發售。
  - 1994年：D7（擁有顯示型刷毛和防滑握柄）與歐樂B「Compact Interdental」牙刷發售。
  - 1995年：Squish Grip與Gripper牙刷、彈性牙線（UltraFloss）發售，歐樂B收購Pro公司拉丁美州業務。
  - 1996年：歐樂B推出採用微紋刷毛及使用兩種符合人體工學材質握柄的Advantage Control Grip牙刷，D9（歐樂B
    Plaque Remover）與歐樂B Interclean Interdental Plaque Remover發售。
  - 1997年：歐樂B與[Nickelodeon結為合作伙伴](../Page/Nickelodeon.md "wikilink")，歐樂B收購Prudent的印度業務。
  - 1998年：歐樂B「3D Plaque
    Remover」、「多動向動力美白牙刷」（CrossAction）與超滑牙線（SATINfloss）發售。

### 2000年代

  - 2000年：歐樂B「3D超震動電動牙刷」（3D Excel）、「電池式電動牙刷」（Kids' Power）與歐樂B電池式電動牙刷發售。
  - 2001年：歐樂B取得[迪士尼獨家授權發售印有](../Page/華特迪士尼公司.md "wikilink")[米老鼠](../Page/米老鼠.md "wikilink")、[小熊維尼](../Page/小熊維尼.md "wikilink")、[巴斯光年與](../Page/巴斯光年.md "wikilink")[迪士尼公主的手動](../Page/迪士尼公主.md "wikilink")、電動與可充電的牙刷產品。\[1\]

## 產品

歐樂B產品線包括以下：\[2\]

<div style="font-size: 80%">

<table>
<thead>
<tr class="header">
<th><p>電動牙刷</p></th>
<th><p>手動牙刷</p></th>
<th><p><a href="../Page/牙線.md" title="wikilink">牙線與牙間用品</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li>數位極淨電動牙刷（Triumph）</li>
<li>3D智慧控頻電動牙刷（ProfessionalCare）</li>
</ul>
<dl>
<dt></dt>
<dd>5000、7000、8000系列
</dd>
<dd>7900 OxyJet Center
</dd>
</dl>
<ul>
<li>音波SPA電動牙刷（Sonic Complete）</li>
<li>AdvancePower: 400 &amp; 900系列, Kids</li>
<li>多動向動力美白牙刷（CrossAction Power &amp; Power Max）</li>
<li>Hummingbird</li>
<li>替換式刷頭（Power Brushheads）</li>
</ul></td>
<td><ul>
<li>敏感牙齒專用牙刷（Sensitive Advantage）</li>
<li>深層律動牙刷（Pulsar）</li>
<li>多動向牙刷（CrossAction）、活力按摩牙刷（Vitalizer）</li>
<li>全接觸多效能牙刷（Advantage）</li>
</ul>
<dl>
<dt></dt>
<dd>Artica、Plus、Control Grip
</dd>
</dl>
<ul>
<li>顯示型牙刷（Indicator）</li>
<li>迪士尼兒童牙刷（Stages）</li>
<li>特殊牙刷</li>
</ul></td>
<td><ul>
<li>小蜂鳥電動牙線（Hummingbird）</li>
<li>超滑牙線（Satin Floss）</li>
<li>彈性牙線（Ultra Floss）</li>
<li>新改良牙線（Essential Floss）</li>
<li>三合一牙線（Super Floss）</li>
<li>牙間刷系列（Interdental Brush System）</li>
<li>Denture Brush</li>
<li>Ends-Tufted Brush</li>
<li>Sulcus Brush</li>
<li>Orthodontic Brush</li>
<li>Gum Stimulator</li>
</ul></td>
</tr>
<tr class="even">
<td><p>牙膏、漱口水與清潔劑</p></td>
<td><p>旅行庄口腔護理產品</p></td>
<td><p>漂白產品</p></td>
</tr>
<tr class="odd">
<td><ul>
<li>Kids' Fluoride Toothpaste</li>
<li>Mouth Rinse</li>
<li>Amosan Oral Wound Cleanser</li>
</ul></td>
<td><ul>
<li>Brush-Ups</li>
<li>Hummingbird</li>
</ul></td>
<td><ul>
<li>Rembrandt Whitening Products</li>
</ul></td>
</tr>
</tbody>
</table>

</div>

## 參考

  - [牙刷](../Page/牙刷.md "wikilink")
  - [口腔護理](../Page/口腔護理.md "wikilink")
  - [吉列公司](../Page/吉列公司.md "wikilink")
  - [百靈](../Page/百靈_\(企業\).md "wikilink")

## 記事與參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [www.oralb.com](http://www.oralb.com)（官方網站）

  - [www.oralb.com.cn](http://www.oralb.com.cn)（Oral-B 中国）

  - [www.livingartist.com.tw/tag/OralB](https://www.livingartist.com.tw/tag/OralB)（Oral-B
    台灣）

  -
  -
  -
[Category:口腔護理用品](../Category/口腔護理用品.md "wikilink")
[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:寶潔公司品牌](../Category/寶潔公司品牌.md "wikilink")
[Category:1950年成立的公司](../Category/1950年成立的公司.md "wikilink")
[Category:1950年面世的產品](../Category/1950年面世的產品.md "wikilink")

1.
2.