**歐洲高等學院**(*EGS*)位於[瑞士](../Page/瑞士.md "wikilink")，由[歐洲跨學術研究基金會創立](../Page/歐洲跨學術研究基金會.md "wikilink")。

歐洲高等學院創立於1994年，包含三個學術研究所：[媒體](../Page/媒體.md "wikilink")、[傳播及](../Page/傳播.md "wikilink")[藝術研究所](../Page/藝術.md "wikilink")，[科學](../Page/科學.md "wikilink")、科技及[社會研究所](../Page/社會.md "wikilink")，藝術、[醫學及社會研究所](../Page/醫學.md "wikilink")；提供學術後專業研究，並由[瑞士瓦麗思大學授與學位](../Page/瑞士瓦麗思大學.md "wikilink")。

本院之研究師資包含以下院士:

[乔治·阿甘本](../Page/乔治·阿甘本.md "wikilink"), Chantal Akerman, Pierre
Aubenque, Alain Badiou, Lewis Baltz, Jean Baudrillard （布西亞）, Yve-Alain
Bois, Catherine Breillat, Victor Burgin, Judith Butler, Sophie Calle,
Diane Davis, Michel Deguy, Manuel De Landa, Claire Denis, [Jacques
Derrida](../Page/Jacques_Derrida.md "wikilink") （德希達）, Tracey Emin,
Chris Fynsk, Peter Greenaway, Durs Grünbein, Werner Hamacher, Donna
Haraway （哈洛威）, Martin Hielscher, Michel Houellebecq, Shelley Jackson,
Claude Lanzmann, David Lynch （大衛林區）, Christian Marclay, Paul D. Miller
a.k.a. DJ Spooky that Subliminal Kid, Carl Mitcham, Jim Jarmusch,
Jean-Luc Nancy, Klaus Ottmann, Hans Ulrich Obrist, Cornelia Parker,
Avital Ronell, Wolfgang Schirmacher, Volker Schlöndorff （史奇洛多夫）, Michael
Schmidt, Hendrik Speck, Bruce Sterling, Sandy Stone, Fred Ulfers,
Gregory Ulmer, Agnès Varda, Victor J. Vitanza, Hubertus von Amelunxen,
John Waters, Samuel Weber, Krzysztof Zanussi, Siegfried Zielinski,
Slavoj Zizek （希謝克）.

本院並與德國漢堡音樂戲劇研究院建立學術交換及學術認證機制，碩士及博士學位由瑞士瓦麗思省政府及教育部共同認證。

## 外部链接

  - [European Graduate School](http://www.egs.edu/)
  - [The Truth about
    EGS](https://web.archive.org/web/20040612080821/http://www.paultulipana.net/egs/)

[Category:国际组织](../Category/国际组织.md "wikilink")
[Category:1994年創建的教育機構](../Category/1994年創建的教育機構.md "wikilink")