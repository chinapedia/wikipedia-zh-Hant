**歐洲鰻鱺**（[學名](../Page/學名.md "wikilink")：**）\[1\]
為[鰻鱺屬的一種降河迴游產卵魚類](../Page/鰻鱺屬.md "wikilink")，可以長達140公分，但普遍為60至80公分，極少長於1公尺。歐洲鰻鱺在[IUCN紅色名錄內列為](../Page/IUCN紅色名錄.md "wikilink")[極危物種](../Page/極危物種.md "wikilink")。

## 分布

本魚原產於[大西洋](../Page/大西洋.md "wikilink")，包括[地中海](../Page/地中海.md "wikilink")、[波羅的海](../Page/波羅的海.md "wikilink")、[北非](../Page/北非.md "wikilink")[摩洛哥](../Page/摩洛哥.md "wikilink")、[斯堪地那維亞半島等海域](../Page/斯堪地那維亞半島.md "wikilink")。

## 深度

水深0至700公尺。

## 特徵

本魚體圓細長，下頷比上頷長；鱗片隱藏魚皮膚之下；鰓孔在圓形的胸鰭前；幼魚背部體色為橄欖色或灰褐色，腹部為銀色或銀黃色；成魚的背部黑灰綠色，腹部為銀色；延長的背鰭與臀鰭、尾鰭匯和鰭，形成一個獨特的鰭從肛門到背部中央最少有500個軟鰭條。背鰭起點在胸鰭後方遠處；臀鰭起源些微地在肛門後面體長可達140公分。

## 生態

[Eel-life-circle1.svg](https://zh.wikipedia.org/wiki/File:Eel-life-circle1.svg "fig:Eel-life-circle1.svg")
幼、成鰻棲息河川、河口、潟湖，喜鑽洞潛居，以[蝦](../Page/蝦.md "wikilink")、[蟹](../Page/蟹.md "wikilink")、[貝](../Page/貝.md "wikilink")、海蟲維生的肉食者。每年秋季成熟的鰻魚，其眼徑變大，內臟萎縮，生殖腺肥大，體色由黃褐變銀灰色，將開始作長途的產卵洄游作準備，經常選擇一個沒有月亮的夜晚，由河川降海在到[大西洋的](../Page/大西洋.md "wikilink")[馬尾藻海產卵](../Page/馬尾藻海.md "wikilink")，其受精卵會在春季和夏初被發現，[幼體時期](../Page/幼體.md "wikilink")（[柳葉鰻](../Page/柳葉鰻.md "wikilink")
Leptocephalus）則利用三年時間向[歐洲遷移](../Page/歐洲.md "wikilink")。有如[玻璃鰻](../Page/玻璃鰻.md "wikilink")（glass
eels），歐洲鰻鱺可以到達歐洲沿岸及進入河口。進入淡水前歐洲鰻鱺變成鰻線（elvers）。她們把大半生花在淡水中。雖然近期與歐洲鰻鱺有關的[日本鰻鱺](../Page/日本鰻鱺.md "wikilink")（Japanese
eel,*Anguilla
japonica*）研究顯示部分物種族群從未遷移至淡水中生活，而生活在海洋或入海的棲息處。歐洲鰻鱺進入淡水後色素產生變化，其腹部會變成黃色。有假設認為黃色有助成為保護色，可以令獵食者較難注意。其黏滑外層認為可以抵抗[鹽度改變](../Page/鹽度.md "wikilink")。

## 近期數目迅速減少

自從1970年代開始，到達歐洲的歐洲鰻鱺數目下降了約90%（可能甚至為98%）。現在並不清楚此現象為正常的長期周期或是反映鰻魚數量的普遍下降。潛在可能原因包括[過度捕魚](../Page/過度捕魚.md "wikilink")、[寄生蟲如](../Page/寄生蟲.md "wikilink")[粗厚鰾線蟲](../Page/粗厚鰾線蟲.md "wikilink")（Anguillicola
crassus）、河流障礙物如[水力發電廠](../Page/水力發電.md "wikilink")，與及[北大西洋振盪](../Page/北大西洋振盪.md "wikilink")（North
Atlantic
oscillation）、[墨西哥灣暖流](../Page/墨西哥灣暖流.md "wikilink")、[北大西洋漂流的天然轉變](../Page/北大西洋漂流.md "wikilink")。近期研究顯示[多氯聯苯](../Page/多氯聯苯.md "wikilink")（PCB）[污染可能是數目下降的主要原因](../Page/污染.md "wikilink")\[2\]。

## 經濟

幼體或成年歐洲鰻鱺為重要的人類食物來源，如[倫敦東區著名的](../Page/倫敦東區.md "wikilink")[鱔魚凍](../Page/鱔魚凍.md "wikilink")。利用籃的陷阱捕捉幼體鰻魚在歐洲西面的海岸線有顯著的經濟價值。

## 参考文献

## 外部連結

  - [More info at
    fishbase](http://www.fishbase.org/Summary/SpeciesSummary.cfm?ID=35&genusname=Anguilla&speciesname=anguilla)

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[anguilla](../Category/鰻鱺屬.md "wikilink")
[Category:波羅的海魚類](../Category/波羅的海魚類.md "wikilink")
[Category:地中海魚類](../Category/地中海魚類.md "wikilink")

1.
2.