*'瓦胡岛
**（[夏威夷語](../Page/夏威夷語.md "wikilink")：Oahu，，），又譯為**欧胡岛*'，或被称为“聚集之岛”，是[夏威夷群島中面積第三大的](../Page/夏威夷群島.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，也是群島中人口最多的島嶼，聚集了[夏威夷州总人口的大约三分之二](../Page/夏威夷州.md "wikilink")。夏威夷州首府所在地[檀香山](../Page/檀香山.md "wikilink")（又稱為火奴魯魯）坐落于岛屿东南部的海岸边，夏威夷州的空中門戶[丹尼爾·井上國際機場也设在瓦胡岛上](../Page/丹尼爾·井上國際機場.md "wikilink")。如果将福特岛等小面积离岛以及东南方（迎风面）沿岸和卡内奥赫湾内的小岛屿算到一起，则瓦胡岛的总面积1,545.4平方公里（596.7平方英里），成为[美国排名第二十大岛屿](../Page/美国岛屿面积列表.md "wikilink")\[1\]
。与夏威夷群岛内的其他岛屿一起，瓦胡岛是整个[波利尼西亚最靠北端和面积最大的岛屿之一](../Page/波利尼西亚.md "wikilink")。

瓦胡岛是一个火山岛，按照最远距离计算，它的东西长度为44英里（71公里），南北宽度为30英里（48公里），整个岛屿的海岸线绵延227英里（365公里）。岛上分布着两组相对独立的[盾状火山山脉](../Page/盾状火山.md "wikilink")：[柯欧劳山脉和](../Page/柯欧劳山脉.md "wikilink")[怀厄奈山脉](../Page/怀厄奈山脉.md "wikilink")。两山之间的谷地，更确切地说应该是两条山脉的鞍部，被称为中央瓦胡平原。瓦胡岛上的最高点位于怀厄奈山脉当中，称为[卡阿拉峰](../Page/卡阿拉峰.md "wikilink")（Mt.
Kaala），其海拔高度为4,003[英尺](../Page/英尺.md "wikilink")（1,220[米](../Page/米.md "wikilink")）\[2\]。

## 岛屿概况

瓦胡岛总人口约为976,199人，其中大约72%为本岛常住人口，约81%的人口都生活在岛上被称为“城市”区域的一侧。瓦胡岛长期以来一直被称为聚集之岛（The
Gathering Isle）。

## 历史

1845年，[卡美哈梅哈三世将](../Page/卡美哈梅哈三世.md "wikilink")[夏威夷王国的首都从](../Page/夏威夷王国.md "wikilink")[毛伊岛迁至本岛](../Page/毛伊岛.md "wikilink")。

[缩略图](https://zh.wikipedia.org/wiki/File:Oahu_from_the_air_2004.jpg "fig:缩略图")
[Ford_Island_aerial_photo_RIMPAC_1986.JPEG](https://zh.wikipedia.org/wiki/File:Ford_Island_aerial_photo_RIMPAC_1986.JPEG "fig:Ford_Island_aerial_photo_RIMPAC_1986.JPEG")，位在照片中央的是幾乎被軍事設施佔據的。\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Waikiki_Panorama_Boat.jpg "fig:缩略图")。\]\]

## 参考资料

## 參考

[\*](../Category/歐胡島.md "wikilink")
[O](../Category/夏威夷州火山岛.md "wikilink")

1.
2.