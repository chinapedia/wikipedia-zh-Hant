**歐陽翥**（），[字鐵翹](../Page/表字.md "wikilink")，[號天驕](../Page/字号.md "wikilink")。[生物學家](../Page/生物學家.md "wikilink")，[神經](../Page/神經.md "wikilink")[解剖學家](../Page/解剖學.md "wikilink")，古典[詩人](../Page/詩人.md "wikilink")，[書法家](../Page/書法家.md "wikilink")。[湖南](../Page/湖南.md "wikilink")[望城](../Page/望城.md "wikilink")（今[長沙](../Page/長沙.md "wikilink")）人。

## 生平

歐陽翥（翥，音ㄓㄨˋ，zhù）1898年生於湖南望城一個書香家庭。童年時代跟從祖父歐陽笙樓、父歐陽鵬習[四書五經與古文史](../Page/四書五經.md "wikilink")。1919年考入[南京高等師範學校](../Page/南京高等師範學校.md "wikilink")（1921年改為[國立東南大學](../Page/國立東南大學.md "wikilink")），初讀[國文](../Page/國文.md "wikilink")，繼學[教育學](../Page/教育學.md "wikilink")，後又轉攻[心理學及](../Page/心理學.md "wikilink")[動物學等學科](../Page/動物學.md "wikilink")，1924年畢業於東南大學生物學系，獲理學士學位。畢業後留校任助教，研究動物學。1927年曾於[中國科學社生物研究所從事短期研究工作](../Page/中國科學社.md "wikilink")。1929年赴[歐](../Page/歐洲.md "wikilink")[留學](../Page/留學.md "wikilink")，先在[法國](../Page/法國.md "wikilink")[巴黎大學研究](../Page/巴黎大學.md "wikilink")[神經解剖學](../Page/神經解剖學.md "wikilink")，後入[德國](../Page/德國.md "wikilink")[柏林大學攻讀動物學](../Page/柏林大學.md "wikilink")、神經解剖學和[人類學](../Page/人類學.md "wikilink")，1933年獲博士學位。1932年至1934年間在[柏林](../Page/柏林.md "wikilink")[威廉皇家神經學研究所任研究助理](../Page/威廉皇家神經學研究所.md "wikilink")。1934年秋回到中國，任[国立中央大学生物學系教授](../Page/国立中央大学_\(南京\).md "wikilink")，抗战時隨校遷[渝](../Page/重慶.md "wikilink")，其間妻卒，後未再娶；1938年起長期擔任[系主任](../Page/系主任.md "wikilink")，並曾擔任理學院代理院長、師範學院博物系系主任；1949年中央大學更名[南京大學](../Page/南京大學.md "wikilink")，仍任生物系教授兼系主任。1954年春患[眼疾等症](../Page/眼疾.md "wikilink")，並因和新政治社會不合，痛心學問後繼無人，5月25日夜投井自殺\[1\]，時年56嵗。

## 事略

歐陽翥是一位[科學家](../Page/科學家.md "wikilink")，也是一位[國學家](../Page/國學家.md "wikilink")，像[胡先驌一樣](../Page/胡先驌.md "wikilink")，被稱為少有的“橫跨人文藝術與科學兩個領域的大師級人物”。科學方面，他曾致力於[生物學](../Page/生物學.md "wikilink")、[人類學](../Page/人類學.md "wikilink")、[神經學](../Page/神經學.md "wikilink")、[解剖學](../Page/解剖學.md "wikilink")、[心理學等領域](../Page/心理學.md "wikilink")，曾任中國科學社、[中國動物學會](../Page/中國動物學會.md "wikilink")、[中國人類學會](../Page/中國人類學會.md "wikilink")、[中國解剖學會](../Page/中國解剖學會.md "wikilink")、[中國心理學會](../Page/中國心理學會.md "wikilink")、[中國民族學會等多種學會的理事](../Page/中國民族學會.md "wikilink")，同時是[國際人類學會](../Page/國際人類學會.md "wikilink")、[國際神經學會會員](../Page/國際神經學會.md "wikilink")。藝術方面，他曾是享譽江南的[書法家](../Page/書法家.md "wikilink")。在國學領域，他有不少研究成果。他一生創作了大量古典詩作。作品有《退思盦詩草》六卷、《退思盦文稿》兩卷、《退思盦詩抄》十三卷、《退思盦雜綴》三十六卷。

近代西方殖民國家人種論認為中國人大腦不如西方白種人，學者為此發生學術爭論，這是近現代科學史上的著名公案。歐陽翥便是這一公案中力駁此種論調的中國學者。1934年7月，歐陽翥回國前和中國學者[吳定良出席在](../Page/吳定良.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦召開的](../Page/倫敦.md "wikilink")[第二屆國際人類學大會](../Page/第二屆國際人類學大會.md "wikilink")。會上，英國[殖民主義學者Shellshear](../Page/殖民主義.md "wikilink")
Joseph
Lexden作-{zh-hans:《中国人脑与澳洲人脑的比较》;zh-hant:〈中國人腦與澳洲人腦的比較〉}-的論文講演，宣稱「中国人脑有猴沟，曲如彎月，與[猩猩相近](../Page/猩猩.md "wikilink")，进化不如白人高等」。歐陽翥在會前已經在英、法、德、[荷蘭等國搜集](../Page/荷蘭.md "wikilink")[證據](../Page/證據.md "wikilink")，在會上，根據搜集之材料力駁Shellshear的觀點，在科學的證據面前，許多與會專家認為此種[黃種人腦比](../Page/黃種人.md "wikilink")[白種人腦低等的觀點站不住腳](../Page/白種人.md "wikilink")。這位來自中國的神經解剖學家自此聞名於世。回國後，欧阳翥在中大生物係繼續從事神經解剖學的研究，先後發表了-{zh-hans:《人脑直回细胞之区分》;zh-hant:〈人腦直回細胞之區分〉}-、-{zh-hans:《人脑岛回新特种细胞》;zh-hant:〈人腦島回新特種細胞〉}-、-{zh-hans:《关於形细胞之新发现》;zh-hant:〈關於形細胞之新發現〉}-、-{zh-hans:《灵长类视觉皮层构造之变异》;zh-hant:〈靈長類視覺皮層構造之變異〉}-等20餘篇論文，分別在國內以及德、英、[美](../Page/美國.md "wikilink")、法、[瑞士等國發表](../Page/瑞士.md "wikilink")。1936年，歐陽翥在德國發表-{zh-hans:《人脑之种族问题》;zh-hant:〈人腦之種族問題〉}-（Uber
Racsengehirne Zeitsch Ressen Kunde Bd.Ⅲ S,
26.1936）一文，從外形大小、重量到內部結構、顯微解剖等諸多方面，論證了黃種人和白種人的大腦並無顯著差異，雄辯地駁斥了西方[種族主義學者詆毀黃種人腦結構和功能不如白種人的謬論](../Page/種族主義.md "wikilink")，從而改變了部分西方人對中國人的歧視心理。在國內，他常做有關人腦的[演講](../Page/演講.md "wikilink")，[普及科學知識](../Page/科普.md "wikilink")，消除一些國人的民族自卑心理。

## 注釋

<references />

[Category:長沙人](../Category/長沙人.md "wikilink")
[Category:中国生物学家](../Category/中国生物学家.md "wikilink")
[Category:神经解剖学家](../Category/神经解剖学家.md "wikilink")
[Category:中国诗人](../Category/中国诗人.md "wikilink")
[Category:中国书法家](../Category/中国书法家.md "wikilink")
[理](../Category/國立中央大學教席學者.md "wikilink")
[理](../Category/國立中央大學教授.md "wikilink")
[理](../Category/南京大學教席學者.md "wikilink")
[理](../Category/國立中央大学校友.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Z](../Category/欧阳姓.md "wikilink")

1.  見 [〈欧阳翥教授之死〉李刚 《书屋》
    二〇〇四年第八期](http://www.housebook.com.cn/200408/12.htm)