是[Cocktail
Soft](../Page/Cocktail_Soft.md "wikilink")（[F\&C](../Page/F&C.md "wikilink")）在1996年7月26日發售的[PC-9801平台](../Page/PC-9801.md "wikilink")[成人遊戲](../Page/日本成人遊戲.md "wikilink")，[歡迎來到Pia
Carrot\!\!系列的第](../Page/歡迎來到Pia_Carrot!!系列.md "wikilink")1作。後來移植到[PC-FX](../Page/PC-FX.md "wikilink")、[Sega
Saturn](../Page/Sega_Saturn.md "wikilink")，並且改編成OVA共三集。

## 故事

## 角色

  - 木ノ下祐介
    本作的男主角。

  - 森原さとみ（聲優：美都いずみ）
    祐介的青梅竹馬。

  - 稲葉翔子（聲優：長崎みなみ）

  - 小久保麗香（聲優：鳩野比奈）

  - 河合雪子（聲優：夏野蛍）

  - 神無月志保（聲優：歌織）

  - 今井佐織（聲優：児玉さとみ）

  - 立花ゆかり（聲優：鳩野比奈）

  - 北川清美（聲優：芽吹）

  - 木ノ下留美（聲優：琴美）
    祐介的妹妹。

  - 樫倉アイリ（聲優：吉野茜）
    Sega Saturn版的新角色。

## OVA

OVA是由[Triple X製作](../Page/Arms.md "wikilink")，Pink Pineapple發售的成人動畫共三集。

  - 集數列表

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>標題</p></th>
<th><p>發售日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>1997年10月24日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>1998年1月23日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>1998年4月24日</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [歡迎來到Pia Carrot\!\!系列官方網站](http://fandc.co.jp/piacarrot/)

[Category:1996年日本成人遊戲](../Category/1996年日本成人遊戲.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:戀愛模擬遊戲](../Category/戀愛模擬遊戲.md "wikilink")
[Category:PC-9801遊戲](../Category/PC-9801遊戲.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:PC-FX遊戲](../Category/PC-FX遊戲.md "wikilink")
[Category:世嘉土星遊戲](../Category/世嘉土星遊戲.md "wikilink")
[Category:Arms](../Category/Arms.md "wikilink")
[Category:1997年日本成人動畫](../Category/1997年日本成人動畫.md "wikilink")
[Category:F\&C游戏](../Category/F&C游戏.md "wikilink")