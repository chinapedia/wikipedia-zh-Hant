**正始**（504年正月—508年八月）是[北魏的君主宣武帝](../Page/北魏.md "wikilink")[元恪的第二个](../Page/元恪.md "wikilink")[年号](../Page/年号.md "wikilink")，共计4年餘。

## 大事记

  - 景明四年，東[荊州](../Page/荊州.md "wikilink")（今[河南](../Page/河南.md "wikilink")[沁阳市](../Page/沁阳市.md "wikilink")）[蠻族領導](../Page/蠻.md "wikilink")[樊素安反抗北魏被平定](../Page/樊素安.md "wikilink")。[正始元年](../Page/正始.md "wikilink")，素安弟[樊秀英恢復反抗被平定](../Page/樊秀英.md "wikilink")。[葉維庚的](../Page/葉維庚.md "wikilink")《[紀元通考](../Page/紀元通考.md "wikilink")》和[李兆洛](../Page/李兆洛.md "wikilink")《[紀元編](../Page/紀元編.md "wikilink")》的内容被認爲將沒有提及年號的《[北史](../Page/北史.md "wikilink")》内容“……僭帝號。正始元年……”誤讀成“……僭帝，號正始。元年……”，做成樊素安的正始年號。\[1\]

## 出生

## 逝世

## 纪年

| 正始                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 504年                           | 505年                           | 506年                           | 507年                           | 508年                           |
| [干支](../Page/干支纪年.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[正始年號的政權](../Page/正始.md "wikilink")
  - 同期存在的其他政权年号
      - [天監](../Page/天監.md "wikilink")（502年四月—519年十二月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [建明](../Page/建明_\(呂苟兒\).md "wikilink")（506年正月-七月）：[北魏時期](../Page/北魏.md "wikilink")[呂苟兒](../Page/呂苟兒.md "wikilink")、[王法智年号](../Page/王法智.md "wikilink")
      - [聖明](../Page/圣明_\(陈瞻\).md "wikilink")（506年正月-七月）：[北魏時期](../Page/北魏.md "wikilink")[陳瞻年号](../Page/陳瞻.md "wikilink")
      - [建平](../Page/建平_\(北魏京兆王\).md "wikilink")（508年八月—九月）：[北魏](../Page/北魏.md "wikilink")[京兆王](../Page/京兆.md "wikilink")[元愉年号](../Page/元愉.md "wikilink")
      - [太安](../Page/太安_\(柔然\).md "wikilink")（492年-505年）：[柔然政权候其伏代库者可汗](../Page/柔然.md "wikilink")[那盖年号](../Page/那盖.md "wikilink")
      - [始平](../Page/始平.md "wikilink")（506年-507年）：[柔然政权佗汗可汗](../Page/柔然.md "wikilink")[伏图年号](../Page/伏图.md "wikilink")
      - [建昌](../Page/建昌_\(柔然\).md "wikilink")（508年-520年）：[柔然政权豆罗伏跋豆伐可汗](../Page/柔然.md "wikilink")[丑奴年号](../Page/丑奴.md "wikilink")
      - [承平](../Page/承平_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")

## 參考資料

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:500年代中国政治](../Category/500年代中国政治.md "wikilink")

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129