**正德**（761年）是[唐朝岐王李业之子](../Page/唐朝.md "wikilink")[李珍所用的](../Page/李珍_\(唐朝\).md "wikilink")[年号](../Page/年号.md "wikilink")，共计1年。此号仅见于罗振玉《[校增纪元编](../Page/校增纪元编.md "wikilink")》。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|正德||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||761年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[辛丑](../Page/辛丑.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[正德年号](../Page/正德.md "wikilink")
  - 同期存在的其他政权年号
      - [上元](../Page/上元_\(唐肅宗\).md "wikilink")（760年閏四月—761年九月）：[唐肅宗的年号](../Page/唐肅宗.md "wikilink")
      - [显圣](../Page/显圣.md "wikilink")（761年三月-763年正月）：[史朝义的年号](../Page/史朝义.md "wikilink")
      - [黃龍](../Page/黃龍_\(段子璋\).md "wikilink")（761年三月-五月）：唐朝[安史之亂时](../Page/安史之亂.md "wikilink")[段子璋的年号](../Page/段子璋.md "wikilink")
      - [天平寳字](../Page/天平寳字.md "wikilink")（757年八月十八日—765年一月七日）：[奈良時代孝謙天皇](../Page/奈良時代.md "wikilink")、[淳仁天皇](../Page/淳仁天皇.md "wikilink")、[稱德天皇之年號](../Page/稱德天皇.md "wikilink")
      - [大興](../Page/大興_\(大欽茂\).md "wikilink")（738年—794年）：[渤海文王](../Page/渤海国.md "wikilink")[大欽茂之年號](../Page/大欽茂.md "wikilink")
      - [贊普鍾](../Page/贊普鍾.md "wikilink")（752年—768年）：[南詔領袖](../Page/南詔.md "wikilink")[閣羅鳳之年號](../Page/閣羅鳳.md "wikilink")

[Category:唐朝民变政权年号](../Category/唐朝民变政权年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:760年代中国政治](../Category/760年代中国政治.md "wikilink")
[Category:761年](../Category/761年.md "wikilink")