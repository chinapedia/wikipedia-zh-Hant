[Plain_White_Banner.svg](https://zh.wikipedia.org/wiki/File:Plain_White_Banner.svg "fig:Plain_White_Banner.svg")

**正白旗**（），又作「**整白旗**」，[清代](../Page/清代.md "wikilink")[八旗之一](../Page/八旗.md "wikilink")，以旗色纯白而得名。与[镶黄旗](../Page/镶黄旗_\(八旗制度\).md "wikilink")、[正黄旗并称为](../Page/正黄旗.md "wikilink")「上三旗」。

[順治前](../Page/順治.md "wikilink")，上三旗中並無正白旗有正藍旗，因在順治初，[多爾袞將自己所領之正白旗納入上三旗](../Page/多爾袞.md "wikilink")，而將[正藍旗降入下五旗](../Page/正藍旗.md "wikilink")，這以後就成了定制。

## 简介

正白旗分为[满洲](../Page/八旗制度.md "wikilink")、[蒙古](../Page/蒙古八旗.md "wikilink")、[汉军三部分](../Page/汉军八旗.md "wikilink")，最初由睿亲王[多尔衮担任旗主](../Page/多尔衮.md "wikilink")。多尔衮死后被[顺治帝](../Page/顺治帝.md "wikilink")[清算](../Page/清算.md "wikilink")，正白旗也改由皇帝亲自统帅，成为清代八旗上三旗之一。清朝末期，正白旗下辖86个整佐领，兵丁两万六千人，家眷总人口约13万人。

## 正白旗名人

### 满洲

  - [多尔衮](../Page/多尔衮.md "wikilink")
  - [多铎](../Page/多铎.md "wikilink")
  - [董鄂妃](../Page/董鄂妃.md "wikilink")
  - [曹雪芹](../Page/曹雪芹.md "wikilink")（實為[內務府](../Page/內務府.md "wikilink")[旗鼓佐領即](../Page/旗鼓佐領.md "wikilink")[包衣](../Page/包衣.md "wikilink")[漢軍](../Page/漢軍.md "wikilink")）
  - [阿桂](../Page/阿桂.md "wikilink")
  - [多隆阿](../Page/多隆阿.md "wikilink")
  - [荣禄](../Page/荣禄.md "wikilink")
  - [裕德龄](../Page/裕德龄.md "wikilink")
  - [幼蘭](../Page/幼蘭.md "wikilink")
  - [廕昌](../Page/廕昌.md "wikilink")
  - [婉容](../Page/婉容.md "wikilink")
  - [璷貴人](../Page/璷妃.md "wikilink")
  - [婉貴妃](../Page/婉貴妃.md "wikilink")

### 蒙古族

  - [納木扎勒](../Page/納木扎勒.md "wikilink")

### 汉军

  - [朱之琏](../Page/朱之琏.md "wikilink")
  - [陳夢球](../Page/陳夢球.md "wikilink")
  - [富明阿](../Page/富明阿_\(吉林将军\).md "wikilink")
  - [寿山](../Page/寿山_\(清朝\).md "wikilink")
  - [白文選](../Page/白文選.md "wikilink")
  - [商衍鎏](../Page/商衍鎏.md "wikilink")

## 参看

  - [白姓](../Page/白姓.md "wikilink")
  - [八旗制度](../Page/八旗制度.md "wikilink")

## 参考文献

  - [八旗史话](https://web.archive.org/web/20100413170238/http://www.lndangan.gov.cn/HTML/index.html)
  - 陈泽希：[清末北京陆军贵胄学堂](http://szb.bjedu.cn/d4-3-05-5.htm)

{{-}}

[Category:八旗](../Category/八旗.md "wikilink")
[\*](../Category/满洲正白旗人.md "wikilink")
[\*](../Category/蒙古正白旗人.md "wikilink")
[\*](../Category/汉军正白旗人.md "wikilink")
[Category:清朝政府军队](../Category/清朝政府军队.md "wikilink")