**武仙座DQ**（或**武仙座新星
1934**）是一顆在1934年於[武仙座爆發的明亮慢](../Page/武仙座.md "wikilink")[新星](../Page/新星.md "wikilink")，它的[視星等曾達到](../Page/視星等.md "wikilink")1.5等。

座標：

  - [赤經](../Page/赤經.md "wikilink")：18h 07m 30.1s
  - [赤緯](../Page/赤緯.md "wikilink")：+45° 51' 31"

武仙座DQ是在分類上被稱為[中介偏振星的](../Page/中介偏振星.md "wikilink")[激變變星原型](../Page/激變變星.md "wikilink")。該系統的軌道週期有變化，可能代表第三個天體存在\[1\]。

## 次恆星天體

2009年有論文指出系統中有第三顆天體存在可以解釋在這個矮新星系統中軌道週期變化的原因。如果該天體確實存在，則可能是[棕矮星](../Page/棕矮星.md "wikilink")。

## 參考資料

## 外部連結

  - <http://www.daviddarling.info/encyclopedia/N/Nova_Herculis_1934.html>

[Category:新星](../Category/新星.md "wikilink")
[Category:武仙座](../Category/武仙座.md "wikilink")
[Category:中介偏振星](../Category/中介偏振星.md "wikilink")

1.