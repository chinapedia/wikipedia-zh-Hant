**武德**（618年五月—626年十二月）是[唐高祖的](../Page/李渊.md "wikilink")[年号](../Page/年号.md "wikilink")，也是[唐朝的第一个年号](../Page/唐朝.md "wikilink")。唐朝使用武德这个年号一共8年餘。武德是唐朝建国的年代，在武德初期，[中国境内的不同政权有](../Page/中国.md "wikilink")14个之多，而武德年结束时，唐朝已经基本上统一了中国，稳定了其统治。

武德九年八月[唐太宗即位沿用](../Page/唐太宗.md "wikilink")。

## 大事记

  - [武德元年](../Page/618年.md "wikilink")——李渊（唐高祖）建立唐朝，定都**大興**，改名**長安**。
  - 武德元年[三月十一日](../Page/4月14日.md "wikilink")——[隋炀帝在兵变中死亡](../Page/隋炀帝.md "wikilink")，[隋朝灭亡](../Page/隋朝.md "wikilink")。
  - [武德二年](../Page/619年.md "wikilink")——[许灭亡](../Page/宇文化及.md "wikilink")。
  - [武德二年](../Page/619年.md "wikilink")——[王世充在洛阳称帝](../Page/王世充.md "wikilink")，国号郑。
  - [武德三年](../Page/620年.md "wikilink")——[郭子和投降唐朝](../Page/郭子和.md "wikilink")。
  - 武德三年——[沈法兴建立的](../Page/沈法兴.md "wikilink")[梁灭亡](../Page/梁_\(沈法兴\).md "wikilink")。
  - [武德四年](../Page/621年.md "wikilink")——[李世民在](../Page/唐太宗.md "wikilink")[虎牢之战中打败](../Page/虎牢之战.md "wikilink")[窦建德](../Page/窦建德.md "wikilink")，迫使[王世充投降](../Page/王世充.md "wikilink")。
  - 武德四年——[窦建德建立的](../Page/窦建德.md "wikilink")[夏灭亡](../Page/夏_\(窦建德\).md "wikilink")。
  - 武德四年——[王世充建立的](../Page/王世充.md "wikilink")[郑灭亡](../Page/郑_\(王世充\).md "wikilink")。
  - 武德四年——[朱粲建立的](../Page/朱粲.md "wikilink")[楚灭亡](../Page/楚_\(朱粲\).md "wikilink")。
  - [武德五年](../Page/622年.md "wikilink")——[杜伏威降唐](../Page/杜伏威.md "wikilink")。
  - 武德五年——[林士弘建立的](../Page/林士弘.md "wikilink")[楚灭亡](../Page/楚_\(林士弘\).md "wikilink")。
  - [武德七年](../Page/624年.md "wikilink")——[高开道建立的](../Page/高开道.md "wikilink")[燕灭亡](../Page/燕_\(高开道\).md "wikilink")。
  - [武德八年](../Page/625年.md "wikilink")——[突厥进攻](../Page/突厥汗国.md "wikilink")[太原](../Page/太原.md "wikilink")。
  - [武德九年](../Page/626年.md "wikilink")——秦王[李世民在](../Page/唐太宗.md "wikilink")[长安发动](../Page/唐长安城.md "wikilink")[政变](../Page/玄武門之變.md "wikilink")，在[玄武门杀死自己兄弟](../Page/玄武门_\(太极宫\).md "wikilink")——[太子](../Page/储君.md "wikilink")[李建成和齐王](../Page/李建成.md "wikilink")[李元吉](../Page/李元吉.md "wikilink")，囚禁了自己的父亲唐高祖，强迫高祖让位，由自己即位称帝。事成后，李世民处死了李建成和李元吉所有的儿子。

## 出生

**武德二年**

  - [李承乾](../Page/李承乾.md "wikilink")

**武德七年**

  - [武则天](../Page/武则天.md "wikilink")

## 逝世

  - [武德元年](../Page/618年.md "wikilink")[三月十一日](../Page/4月14日.md "wikilink")——[杨广](../Page/隋炀帝.md "wikilink")，隋朝皇帝
  - [武德二年](../Page/619年.md "wikilink")——[宇文化及](../Page/宇文化及.md "wikilink")，隋末唐初自立为帝
  - [武德三年](../Page/620年.md "wikilink")——[沈法兴](../Page/沈法兴.md "wikilink")，隋末唐初自立为王
  - [武德四年](../Page/621年.md "wikilink")——[窦建德](../Page/窦建德.md "wikilink")，隋末唐初自立为帝
  - 武德四年——[朱粲](../Page/朱粲.md "wikilink")，隋末唐初自立为帝
  - [武德五年](../Page/622年.md "wikilink")——[刘武周](../Page/刘武周.md "wikilink")，隋末唐初自立为王
  - 武德五年——[林士弘](../Page/林士弘.md "wikilink")，隋末唐初自立为帝
  - [武德六年](../Page/623年.md "wikilink")——[刘黑闼](../Page/刘黑闼.md "wikilink")，隋末唐初自立为王
  - [武德七年](../Page/624年.md "wikilink")——[高开道](../Page/高开道.md "wikilink")，隋末唐初自立为王
  - [武德九年](../Page/626年.md "wikilink")——[李建成](../Page/李建成.md "wikilink")，唐朝太子
  - 武德九年——[李元吉](../Page/李元吉.md "wikilink")，唐朝王子

## 纪年

| 武德                             | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 618年                           | 619年                           | 620年                           | 621年                           | 622年                           | 623年                           | 624年                           | 625年                           | 626年                           |
| [干支](../Page/干支.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") |

## 參看

  - [中国年号列表](../Page/中国年号列表.md "wikilink")
  - 同期存在的其他政权年号
      - [皇泰](../Page/皇泰.md "wikilink")（618年五月—619年四月）：[隋朝政权](../Page/隋朝.md "wikilink")[杨侗年号](../Page/皇泰主.md "wikilink")
      - [昌達](../Page/昌達.md "wikilink")（615年十二月—619年閏二月）：隋朝末期唐朝初期領袖，[楚政權](../Page/楚.md "wikilink")[朱粲年号](../Page/朱粲.md "wikilink")
      - [太平](../Page/太平_\(林士弘\).md "wikilink")（616年十二月—622年十月）：隋朝末期唐朝初期領袖，[楚政權](../Page/楚.md "wikilink")[林士弘年号](../Page/林士弘.md "wikilink")
      - [丁丑](../Page/丁丑_\(窦建德\).md "wikilink")（617年正月—618年十一月）：隋朝末期唐朝初期領袖，[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [五凤](../Page/五凤_\(窦建德\).md "wikilink")（618年十一月—621年五月）：隋朝末期唐朝初期領袖，[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [永平](../Page/永平_\(李密\).md "wikilink")（617年二月—618年十二月）：隋朝末期唐朝初期領袖，[魏政權](../Page/魏.md "wikilink")[李密年号](../Page/李密_\(隋朝\).md "wikilink")
      - [天兴](../Page/天兴_\(刘武周\).md "wikilink")（617年三月—620年四月）：隋朝末期唐朝初期領袖，[刘武周年号](../Page/刘武周.md "wikilink")
      - [永隆](../Page/永隆_\(梁師都\).md "wikilink")（618年—628年四月）：隋朝末期唐朝初期領袖，[梁政權](../Page/梁.md "wikilink")[梁師都年号](../Page/梁師都.md "wikilink")
      - [正平](../Page/正平_\(郭子和\).md "wikilink")（617年三月—618年七月）：隋朝末期唐朝初期領袖，[永樂王](../Page/永樂.md "wikilink")[郭子和年号](../Page/郭子和.md "wikilink")
      - [秦兴](../Page/秦兴.md "wikilink")（617年四月—618年十一月）：隋朝末期唐朝初期領袖，[秦政權](../Page/秦.md "wikilink")[薛舉年号](../Page/薛舉.md "wikilink")
      - [鸣凤](../Page/鸣凤.md "wikilink")（617年四月—618年十一月）：隋朝末期唐朝初期領袖，[梁政權](../Page/梁.md "wikilink")[蕭銑年号](../Page/蕭銑.md "wikilink")
      - [天壽](../Page/天壽_\(宇文化及\).md "wikilink")（618年九月—619年二月）：隋朝末期唐朝初期領袖，[許政權](../Page/許.md "wikilink")[宇文化及年号](../Page/宇文化及.md "wikilink")
      - [安乐](../Page/安乐.md "wikilink")（617年七月—619年五月）：隋朝末期唐朝初期領袖，[涼政權](../Page/涼.md "wikilink")[李軌年号](../Page/李轨.md "wikilink")
      - [始兴](../Page/始兴_\(高开道\).md "wikilink")（618年十二月—624年二月）：隋朝末期唐朝初期領袖，[燕政權](../Page/燕.md "wikilink")[高开道年号](../Page/高开道.md "wikilink")
      - [法輪](../Page/法輪_\(高曇晟\).md "wikilink")（618年十二月）：隋朝末期唐朝初期領袖，[齊政權](../Page/齊.md "wikilink")[高曇晟年号](../Page/高曇晟.md "wikilink")
      - [开明](../Page/开明_\(王世充\).md "wikilink")（619年四月—621年五月）：唐朝初期領袖，[鄭政權](../Page/鄭.md "wikilink")[王世充年号](../Page/王世充.md "wikilink")
      - [延康](../Page/延康_\(沈法兴\).md "wikilink")（619年九月—620年十二月）：唐朝初期領袖，[梁政權](../Page/梁.md "wikilink")[沈法兴年号](../Page/沈法兴.md "wikilink")
      - [明政](../Page/明政_\(李子通\).md "wikilink")（619年九月—621年十一月）：唐朝初期領袖，[吳政權](../Page/吳.md "wikilink")[李子通年号](../Page/李子通.md "wikilink")
      - [天造](../Page/天造.md "wikilink")（622年正月—623年正月）：唐朝初期領袖，[刘黑闼年号](../Page/刘黑闼.md "wikilink")
      - [天明](../Page/天明_\(輔公祏\).md "wikilink")（623年八月—624年三月）：唐朝初期領袖，[輔公祏年号](../Page/輔公祏.md "wikilink")
      - [進通](../Page/進通.md "wikilink")（623年）：唐朝初期領袖，[王摩沙年号](../Page/王摩沙.md "wikilink")
      - [义和](../Page/义和_\(高昌\).md "wikilink")（614年—619年）：[高昌政权年号](../Page/高昌.md "wikilink")
      - [重光](../Page/重光_\(高昌年號\).md "wikilink")（620年—623年）：高昌君主[麴伯雅政权年号](../Page/麴伯雅.md "wikilink")
      - [延壽](../Page/延壽_\(麴文泰\).md "wikilink")（624年—640年）：高昌政权[麴文泰年号](../Page/麴文泰.md "wikilink")
      - [建福](../Page/建福_\(新羅真平王\).md "wikilink")（584年—634年）：[新羅](../Page/新羅.md "wikilink")[真平王的年號](../Page/真平王.md "wikilink")

## 參考文獻

  - [徐红岚](../Page/徐红岚.md "wikilink")，《中日朝三国历史纪年表》，[辽宁教育出版社](../Page/辽宁教育出版社.md "wikilink")，1998年5月
    ISBN 7538246193
  - [松橋達良](../Page/松橋達良.md "wikilink")，《》，[砂書房](../Page/砂書房.md "wikilink")，1994年7月，ISBN
    4915818276
  - [李崇智](../Page/李崇智.md "wikilink")，《中国历代年号考》，[中华书局](../Page/中华书局.md "wikilink")，2001年1月
    ISBN 7101025129

[category:唐朝年号](../Page/category:唐朝年号.md "wikilink")

[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:610年代中国政治](../Category/610年代中国政治.md "wikilink")
[Category:620年代中国政治](../Category/620年代中国政治.md "wikilink")