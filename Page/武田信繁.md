[TakedaNobushige.jpg](https://zh.wikipedia.org/wiki/File:TakedaNobushige.jpg "fig:TakedaNobushige.jpg")
**武田信繁**（〉是[日本戰國時代的武將](../Page/日本戰國時代.md "wikilink")，甲斐[守護](../Page/守護.md "wikilink")[武田信虎之次男](../Page/武田信虎.md "wikilink")，[武田信玄之胞弟](../Page/武田信玄.md "wikilink")，幼名**次郎**。[元服後任](../Page/元服.md "wikilink")，由於官職對應的唐名為[典厩](../Page/典厩.md "wikilink")，因此一般通稱為**武田典厩信繁**。為了區分繼承典厩之職的長子信豐，後世亦會稱他為**古典厩**。[武田二十四將之首](../Page/武田二十四將.md "wikilink")，擔任其兄**武田信玄**副將，於第四次川中島戰死後，副將之職由[武田四名臣之一的](../Page/武田四名臣.md "wikilink")[內藤昌豐繼任](../Page/內藤昌豐.md "wikilink")。

## 生平

信繁年幼時期，由於能文善武、德才兼備、個性耿直而受到父親信虎的寵愛。[信虎甚至打算廢除嫡長子晴信](../Page/武田信虎.md "wikilink")（亦即日後的[武田信玄](../Page/武田信玄.md "wikilink")），將他立為家督繼承人，即使如此，他和兄長的關係仍然非常好。在兄長驅逐父親而自立後，信繁便開始擔任信玄的副將，長年守護其兄信玄，威望在家中頗高不輸其兄，深受兄長信玄與家臣們的信任，並參加不少武田家的主要戰役。另外，雖然缺乏其他真實可靠的史料支持，但根據《[高白齋記](../Page/高白齋記.md "wikilink")》的記載，信繁在天文20年（1551年）承襲了武田氏庶流的吉田氏之姓。

天文11年，武田家入侵諏訪時，信繁擔任大將，與[宿老](../Page/宿老.md "wikilink")[板垣信方共同主導諏訪出兵](../Page/板垣信方.md "wikilink")，同年9月[高遠賴繼叛亂時](../Page/高遠賴繼.md "wikilink")，擔任鎮壓軍的大將。信方擔任諏訪郡代時，信繁配置於諏訪的同心眾。

天文20年（1551年）7月，擔任進攻[村上義清的先鋒大將](../Page/村上義清.md "wikilink")（「惠林寺舊藏文書」）。
信玄欲將[信濃國納入版圖而採取懷柔政策](../Page/信濃國.md "wikilink")，令信繁之子和相繼成為名族望月氏[望月信雅的養子](../Page/望月信雅.md "wikilink")。

在1561年的第四次[川中島會戰](../Page/川中島會戰.md "wikilink")，信繁率領的部隊遭到上杉軍的突擊。另有一說，信繁為了解救被圍攻的姪子[武田義信而強行闖入上杉軍](../Page/武田義信.md "wikilink")，引開上杉軍的部隊。信繁最終戰死，享年37歲。戰後，不僅信玄及武田家的將士們，就連敵對的[上杉謙信亦對他的死而感到惋惜](../Page/上杉謙信.md "wikilink")。

在信繁戰死之後由[武田四名臣之一的](../Page/武田四名臣.md "wikilink")[內藤昌豐繼信繁之後擔任信玄的副將](../Page/內藤昌豐.md "wikilink")。

信繁留下九十九條的《武田信繁家訓》（亦即[甲州法度次第的原型](../Page/甲州法度次第.md "wikilink")）予嫡子[信豐](../Page/武田信豐.md "wikilink")，[甲陽軍鑑上亦有記載](../Page/甲陽軍鑑.md "wikilink")。

根據《[甲陽軍鑑](../Page/甲陽軍鑑.md "wikilink")》的記載，[武田四名臣之一的](../Page/武田四名臣.md "wikilink")[山縣昌景曾評價他與](../Page/山縣昌景.md "wikilink")[內藤昌豐](../Page/內藤昌豐.md "wikilink")（[武田四名臣的另外一人](../Page/武田四名臣.md "wikilink")）才是真正的副將。後世亦對信繁的評價甚高。在[江戶時代](../Page/江戶時代.md "wikilink")，信繁就被評價為「真誠的武將」而廣受歡迎，而他留下的《武田信繁家訓》亦成為當時武士的心得而膾炙人口。江戶時代的儒學者[室鳩巢亦在其著作](../Page/室鳩巢.md "wikilink")《駿台雜話》中評價他為「天文及永祿年間（指戰國亂世），最賢德者為甲州武田信玄公之弟、典厩信繁公也」。後來真田家的始祖攻彈正[真田幸隆的三子](../Page/真田幸隆.md "wikilink")、被譽為「表裏比興之者」而活耀於[上田城之戰的](../Page/上田城之戰.md "wikilink")[真田昌幸也相當崇敬信繁](../Page/真田昌幸.md "wikilink")，甚至把自己次子也命名為「[信繁](../Page/真田信繁.md "wikilink")」，以希望次子能夠繼承信繁之武名。

## 登場作品

### 影視劇

  - [天與地](../Page/天與地_\(大河劇\).md "wikilink")（1969年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：）
  - [風林火山](../Page/風林火山_\(1969年電影\).md "wikilink")（1969年、東寶、演：[田村正和](../Page/田村正和.md "wikilink")）
  - [武田信玄](../Page/武田信玄_\(大河劇\).md "wikilink")（1988年、NHK大河劇、演：）
  - [天與地](../Page/天與地_\(日本電影\).md "wikilink")（1990年、東映、演：）
  - [武田信玄](../Page/:JA:武田信玄_\(1991年のテレビドラマ\).md "wikilink")（1991年、TBS、演：）
  - [風林火山](../Page/:JA:風林火山_\(1992年のテレビドラマ\).md "wikilink")（1992年、NTV、演：）
  - [風林火山](../Page/:JA:風林火山_\(2006年のテレビドラマ\).md "wikilink")（2006年、EX、演：[市瀨秀和](../Page/市瀨秀和.md "wikilink")）
  - [風林火山](../Page/風林火山_\(大河劇\).md "wikilink")（2007年、NHK大河劇、演：）

## 參考資料

  - 甲陽軍鑑 譯 [筑摩書房](../Page/筑摩書房.md "wikilink") ISBN 4-480-09040-1

[Category:1525年出生](../Category/1525年出生.md "wikilink")
[Category:1561年逝世](../Category/1561年逝世.md "wikilink")
[Category:武田二十四將](../Category/武田二十四將.md "wikilink")
[Category:吉田氏](../Category/吉田氏.md "wikilink")
[Nobushige](../Category/甲斐武田氏.md "wikilink")
[Category:甲斐國出身人物](../Category/甲斐國出身人物.md "wikilink")
[Category:日本战争身亡者](../Category/日本战争身亡者.md "wikilink")