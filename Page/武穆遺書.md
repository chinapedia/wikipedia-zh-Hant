《**武穆遺書**》
（原名《**破金要訣**》），[金庸小說中虛構的兵法奇書](../Page/金庸.md "wikilink")，為[岳飛所著](../Page/岳飛.md "wikilink")。現代屢有《武穆遺書》面世，惟難以證實出自岳飛手筆，兼以武術為主，並非[兵書](../Page/兵書.md "wikilink")，加上[宋代史書對此書全無紀錄](../Page/宋朝.md "wikilink")、歷代兵書亦無收錄，故此兵書純為虛構。

## [金庸小说中的傳承](../Page/金庸.md "wikilink")

### [南宋](../Page/南宋.md "wikilink")

[岳飛死後](../Page/岳飛.md "wikilink")，《武穆遺書》一直藏於[南宋朝廷](../Page/南宋.md "wikilink")[临安皇宮翠寒堂東十五步處水簾石洞下](../Page/臨安府_\(浙江\).md "wikilink")，後被[鐵掌幫幫主上官劍南盜回鐵掌峰](../Page/鐵掌幫.md "wikilink")，並藏於幫中禁地。

《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》一書中，[完顏洪烈見](../Page/完顏洪烈.md "wikilink")[金國跟](../Page/金國.md "wikilink")[蒙古戰鬥屢屢不敵](../Page/蒙古.md "wikilink")，希望得到《武穆遺書》中用兵要訣，以滅[蒙古及](../Page/蒙古.md "wikilink")[大宋](../Page/宋朝.md "wikilink")，故請得「西毒」[歐陽鋒父子及](../Page/歐陽鋒.md "wikilink")[彭連虎](../Page/彭連虎.md "wikilink")、[沙通天等武林高手幫助](../Page/沙通天.md "wikilink")，前往石洞內盜書。惟他們不知《武穆遺書》早已被[鐵掌幫幫主盜去](../Page/鐵掌幫.md "wikilink")，所奪得的只是一個空盒。[郭靖亦為保護此書](../Page/郭靖.md "wikilink")，與歐陽鋒交手而至重傷，幸得[黃蓉相助於密室療傷七天後方癒](../Page/黃蓉.md "wikilink")。

[郭靖與](../Page/郭靖.md "wikilink")[黃蓉因於牛家村](../Page/黃蓉.md "wikilink")[曲三酒館中發現曲藏有](../Page/曲三酒館.md "wikilink")《武穆遺書》所在地的畫，得知《武穆遺書》藏於鐵掌峰，並成功取得。後來[襄陽城被破後](../Page/襄陽城.md "wikilink")，郭靖夫婦與襄陽城一同以身殉國，郭靖將[楊過送予](../Page/楊過.md "wikilink")[郭襄的](../Page/郭襄.md "wikilink")[玄鐵重劍加上西方](../Page/玄鐵重劍.md "wikilink")[精金分別著成](../Page/精金.md "wikilink")[倚天劍與](../Page/倚天劍.md "wikilink")[屠龍刀](../Page/屠龍刀.md "wikilink")（新三版中只有屠龍刀是以[玄鐵溶鑄成](../Page/玄鐵.md "wikilink")，倚天劍改為以[楊過及](../Page/楊過.md "wikilink")[小龍女夫婦佩劍](../Page/小龍女.md "wikilink")[君子劍及淑女劍合鑄而成](../Page/君子劍.md "wikilink")），《武穆遺書》即藏於[屠龍刀之中](../Page/屠龍刀.md "wikilink")。

### [元](../Page/元朝.md "wikilink")[明](../Page/明朝.md "wikilink")

[周芷若於靈蛇島取得倚天劍與屠龍刀](../Page/周芷若.md "wikilink")，刀劍互砍而取得《武穆遺書》，[趙敏從](../Page/趙敏.md "wikilink")[周芷若身上取得](../Page/周芷若.md "wikilink")《武穆遺書》，後來[張無忌再轉交予](../Page/張無忌.md "wikilink")[徐達](../Page/徐達.md "wikilink")。

## 兵書形容

《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》第三十六回：

这书中诸凡定谋、审事、攻伐、守御、练卒、使将、布阵、野战，以及动静安危之势，用正出奇之道，无不详加阐述。

《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》第九回：

[完颜洪烈道](../Page/完颜洪烈.md "wikilink")：“[岳飞无法可施](../Page/岳飞.md "wikilink")，只得把那部兵书贴身藏了，写了四首甚么《菩萨蛮》、《丑奴儿》、《贺圣朝》、《齐天乐》的歪词。这四首词格律不对，平仄不-{叶}-，句子颠三倒四，不知所云。”

“数十年来，这四首歪词收在[大金宫里秘档之中](../Page/金朝.md "wikilink")，无人领会其中含意。”

“哪知其中竟是藏着一个极大的哑谜。小王苦苦思索，终于解明了，原来这四首歪词须得每隔三字的串读，先倒后顺，反复连贯，便即明明白白。”

## 小說歷史

[成吉思汗西](../Page/成吉思汗.md "wikilink")-{征}-：成吉思汗西-{征}-[花剌子模國時](../Page/花剌子模王朝.md "wikilink")，[郭靖以金刀駙馬](../Page/郭靖.md "wikilink")、右軍元帥身份隨軍西-{征}-，並熟練《武穆遺書》中天覆、地載、風揚、雲垂、龍飛、虎翼、鳥翔、蛇蟠八個陣勢，於阻止[蒙古王子內鬥與滅](../Page/蒙古.md "wikilink")[花剌子模國中立下重大功勞](../Page/花剌子模王朝.md "wikilink")，成吉思汗本欲封最優良的封地給予郭靖。

[襄陽保衛戰](../Page/襄陽.md "wikilink")：[郭靖於母親李萍自殺身亡要求兒子忠於](../Page/郭靖.md "wikilink")[大宋後離開](../Page/南宋.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")，成為[南宋保護門戶重鎮襄陽城的重要人物](../Page/南宋.md "wikilink")。於數次[襄陽保衛戰中](../Page/襄陽.md "wikilink")，郭靖與妻子[黃蓉立下了不可磨滅的貢獻](../Page/黃蓉.md "wikilink")，但《武穆遺書》貢獻已成疑。

[明教起義](../Page/明教.md "wikilink")：[張無忌無心大業](../Page/張無忌.md "wikilink")，將《武穆遺書》轉交予[朱元璋手下大將](../Page/朱元璋.md "wikilink")[徐達](../Page/徐達.md "wikilink")（徐達於取得《武穆遺書》前已是赫赫有名的將領）。[趙宋因失](../Page/宋朝.md "wikilink")[岳飛而置神州於龍漠](../Page/岳飛.md "wikilink")，而[朱明則因](../Page/明朝.md "wikilink")《武穆遺書》而復漢人江山，乃[金庸對](../Page/金庸.md "wikilink")[大宋](../Page/宋朝.md "wikilink")[鄂王](../Page/鄂王.md "wikilink")[岳飛的一種間接致敬](../Page/岳飛.md "wikilink")。

## 参考资料

  - 《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - [金庸笔下的《武穆遗书》曝光：实为武功秘籍](http://www.chinanews.com/cul/2013/08-27/5208427.shtml)，武汉晚报，中新网
  - [岳飞后人：《武穆遗书》真有其书 1987年已上交国家](http://news.ifeng.com/history/zhongguoxiandaishi/detail_2013_08/01/28161224_0.shtml)，辽沈晚报，凤凰网
  - [《寻找武穆遗书》](http://jishi.cntv.cn/2014/09/05/VIDA1409899366244539.shtml)，3集，《时代》
    20140908\~0910

[Category:虚构书籍](../Category/虚构书籍.md "wikilink")
[Category:射鵰三部曲](../Category/射鵰三部曲.md "wikilink")