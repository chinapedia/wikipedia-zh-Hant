**武邑县**是[河北省](../Page/河北省.md "wikilink")[衡水市下辖的一个县](../Page/衡水.md "wikilink")。面积830平方千米，总人口31万人（2004年）。邮政编码053400，县政府驻：武邑镇。

## 气候

年均温度12.5℃，年降水量650.7毫米。

## 行政区划

下辖6个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [武邑县政府网站](http://www.wuyi.gov.cn/)
  - [衡水市武邑县商务之窗](https://web.archive.org/web/20080429025704/http://hengshuiwuyi.mofcom.gov.cn/)

[武邑县](../Page/category:武邑县.md "wikilink")
[县](../Page/category:衡水区县市.md "wikilink")

[衡水](../Category/河北省县份.md "wikilink")
[冀](../Category/国家级贫困县.md "wikilink")