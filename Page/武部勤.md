**武部勤**（），出身於[北海道](../Page/北海道.md "wikilink")[斜里町](../Page/斜里町.md "wikilink")，[日本自由民主党主要领导人](../Page/日本自由民主党.md "wikilink")，[山崎派主要成员](../Page/山崎派.md "wikilink")，在第一届小泉内阁中任[农林水产大臣](../Page/农林水产大臣.md "wikilink")，因处置中日农产品贸易纠纷无能被媒体猛烈而在内阁改组中下台。但还是受到[小泉纯一郎的重用](../Page/小泉纯一郎.md "wikilink")，接替因参议院选举下台的[安倍晋三任自民党干事长](../Page/安倍晋三.md "wikilink")，在2005年众议院大选中带领自民党获得空前大胜。在[麻生太郎任首相期间](../Page/麻生太郎.md "wikilink")，武部勤和[加藤紘一](../Page/加藤紘一.md "wikilink")、[中川秀直合作](../Page/中川秀直.md "wikilink")，成为自民党内反麻生活动的主要领导人之一。

[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:日本農林水產大臣](../Category/日本農林水產大臣.md "wikilink")
[Category:第一次小泉內閣閣僚](../Category/第一次小泉內閣閣僚.md "wikilink")
[Category:日本自由民主黨幹事長](../Category/日本自由民主黨幹事長.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:北海道選出日本眾議院議員](../Category/北海道選出日本眾議院議員.md "wikilink")
[Category:北海道比例代表區選出日本眾議院議員](../Category/北海道比例代表區選出日本眾議院議員.md "wikilink")