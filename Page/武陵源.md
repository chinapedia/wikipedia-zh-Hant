**武陵源风景名胜区**地处位于[湖南省西北部的](../Page/湖南.md "wikilink")[武陵山脉中段](../Page/武陵山脉.md "wikilink")[桑植和](../Page/桑植.md "wikilink")[慈利两县交界处](../Page/慈利.md "wikilink")，隶属[张家界市](../Page/张家界.md "wikilink")。武陵源由四大风景区组成，分别为张家界国家森林公园和张家界国家地质公园、[索溪峪](../Page/索溪峪.md "wikilink")、[天子山](../Page/天子山.md "wikilink")、[杨家界三个](../Page/杨家界.md "wikilink")[自然保护区](../Page/自然保护区.md "wikilink")；总面积约391平方公里，核心景区面积超过250平方公里。武陵源具有比较原始的[生态系统](../Page/生态系统.md "wikilink")，有罕见的砂岩峰林地貌景观，有3000多座形状奇异的山峰、800多条溪涧，也有岩溶洞穴、瀑布群，并有天然森林，1980年代初被开辟为旅游景区，1988年被定为[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")，并建立武陵源区政府。1992年12月，被[联合国教科文组织作为自然遗产列入](../Page/联合国教科文组织.md "wikilink")《[世界遗产名录](../Page/世界遗产.md "wikilink")》。\[1\]\[2\]

## 黃石寨五峰全景

[1_zhangjiajie_huangshizhai_wulingyuan_panorama_2012.jpg](https://zh.wikipedia.org/wiki/File:1_zhangjiajie_huangshizhai_wulingyuan_panorama_2012.jpg "fig:1_zhangjiajie_huangshizhai_wulingyuan_panorama_2012.jpg")
[ZhangJiaJie_River_1.jpg](https://zh.wikipedia.org/wiki/File:ZhangJiaJie_River_1.jpg "fig:ZhangJiaJie_River_1.jpg")

## 地貌地质构造

[Zhangjiajie-Hunan.jpg](https://zh.wikipedia.org/wiki/File:Zhangjiajie-Hunan.jpg "fig:Zhangjiajie-Hunan.jpg")
[张家界砂岩峰林.jpg](https://zh.wikipedia.org/wiki/File:张家界砂岩峰林.jpg "fig:张家界砂岩峰林.jpg")
[“天兵天将”石柱.jpg](https://zh.wikipedia.org/wiki/File:“天兵天将”石柱.jpg "fig:“天兵天将”石柱.jpg")
武陵源以[石英砂岩峰林峡谷地貌为其主要特征](../Page/石英砂.md "wikilink")，共有石峰3103座，峰体分布在[海拔](../Page/海拔.md "wikilink")500～1100[米](../Page/米.md "wikilink")，高度由几十米至400米不等，这种特殊的地貌形态被命名为“石英砂柱峰”地貌。石英砂岩峰林地貌的特点是质纯、石厚，石英含量为75％～95％，岩层厚520余米，为国内外所罕见。

### 地质构造起因

区域地质构造处于[新华夏第三隆起带](../Page/新华夏第三隆起带.md "wikilink")。大致经历了武陵-雪峰、印支、燕山、喜山及新构造运动。[武陵-雪峰运动奠定了本区域的基地构造](../Page/武陵-雪峰运动.md "wikilink")，[印支运动塑造了本区的基本地貌构架](../Page/印支运动.md "wikilink")，而喜山及[新构造运动是形成张家界奇特的石英砂峰林地貌景观的最基本因素之一](../Page/新构造运动.md "wikilink")。构成砂岩峰林地貌的地层主要由远[古生界中](../Page/古生界.md "wikilink")、上[泥盆纪云台观组和黄家墩组构成](../Page/泥盆纪.md "wikilink")，地层显示滨海相碎屑岩类特点。岩石质纯、层厚，底状平缓，垂直节理发育，岩石出露于向斜轮廓。外力地质活动作用的流水侵蚀和重力崩坍及生物生化作用、物理风化作用，则成为构造该区域地貌的外部条件。

## 动植物

武陵源地形复杂，气候温和，雨量丰富，森林发育茂盛，生长着原始次森林植物群落，森林覆盖率达88%，有国家[中国一级保护植物](../Page/中国一级保护植物.md "wikilink")4种，二级保护植物19种。木本植物就达到770种，其中[武陵松为只有本地区仅有的植物](../Page/武陵松.md "wikilink")，另外生长着[珙桐](../Page/珙桐.md "wikilink")、[伯乐树](../Page/伯乐树.md "wikilink")、[南方红豆杉](../Page/南方红豆杉.md "wikilink")、[白豆杉](../Page/白豆杉.md "wikilink")、[篦子三尖杉等第三纪孑遗植物](../Page/篦子三尖杉.md "wikilink")。武陵源拥有28种国家级保护动物，还生长着著名的[大鲵](../Page/大鲵.md "wikilink")（娃娃鱼）珍贵动物，还有国家一级保护[动物](../Page/动物.md "wikilink")[云豹](../Page/云豹.md "wikilink")、[金钱豹等大型猛兽](../Page/金钱豹.md "wikilink")。

## 主要景观

### 张家界国家森林公园

张家界，又名青岩山，面积一百三十平方公里，是中国第一个国家森林公园，它地处武陵山中。张家界地貌奇特，有石峰2000多座，形态各异，树木茂盛，森林覆盖率达88％，四周山地环抱，坡陡沟深，气候暖湿，区内景点众多，尤以黄狮寨、砂刀沟、金鞭岩、金鞭溪等最为著名。有黄狮寨、金鞭溪、腰子寨、琵琶溪、砂刀沟、后花园、朝天观七条主要旅游线。是游客的好去处。
[金鞭溪.jpg](https://zh.wikipedia.org/wiki/File:金鞭溪.jpg "fig:金鞭溪.jpg")

### 天子山风景区

天子山风景区位于武陵源北，与张家界、索溪峪山水相依，总面积67平方公里。天子山海拔高1262.5米（昆仑峰），最低534米，环山游览线有40余公里。天子山风景区因南宋末年土家族领袖向王（名大坤）自称天子而得名。区内许多景点也都与此有关，如天子洲、宝剑峰、龙椅岩等。

### 杨家界风景区

1992年在张家界西北角发现新的景区杨家界。这一新景区总面积3400公顷。相传，北宋杨家将围剿向王天子曾在天子山安营扎寨。后因战争旷日持久，杨家便在此地繁衍后代，使这里成了“杨家界”。如今，杨家界还保存有《杨氏族谱》和明清时代的杨家祖墓，有“六郎湾”、“七郎湾”、“宗保湾”、“天波府”等地名。

### 索溪峪风景区

索溪峪自然风景区内有2000多座山峰，还有19道沟壑和6条溪流。主要景点有十里画廊、鸳鸯瀑、南天门、黄龙洞等。

## 登录过程

  - 1992年12月，被[联合国教科文组织作为](../Page/联合国教科文组织.md "wikilink")[自然遗产列入](../Page/自然遗产.md "wikilink")《[世界遗产名录](../Page/世界遗产.md "wikilink")》。
  - 2004年2月13日，被[联合国教科文组织世界地质公园专家评审会批准为首批](../Page/联合国.md "wikilink")[世界地质公园](../Page/世界地质公园.md "wikilink")。

## 参考文献

## 外部链接

  - [China’s Ancient
    Skyline](http://travel.nytimes.com/2007/07/15/travel/15wuling.html)
  - [About
    Wulingyuan](http://www.cn-zhangjiajie.com/?action/viewnews/itemid/376.html)
  - [Zhangjiajie glass
    bridge](http://www.zjjbk.com/2017/news_0407/5030.html)

## 参见

  - [张家界](../Page/张家界.md "wikilink")
  - [中国世界遗产](../Page/中国世界遗产.md "wikilink")
  - [世界遗产列表 (亚洲和大洋洲)](../Page/世界遗产列表_\(亚洲和大洋洲\).md "wikilink")

{{-}}

[Category:张家界旅游](../Category/张家界旅游.md "wikilink")
[W](../Category/湖南旅游景点.md "wikilink")
[湘](../Category/国家5A级旅游景区.md "wikilink")
[湘](../Category/中国世界遗产.md "wikilink")

1.
2.