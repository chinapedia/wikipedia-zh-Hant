**歷高**（，或稱為
，）是[巴西的足球員](../Page/巴西.md "wikilink")，他擅長踢進攻中場的位置及主射自由球，在2005年至2007年效力[香港甲組聯賽球會](../Page/香港甲組足球聯賽.md "wikilink")[晨曦兩個球季後](../Page/晨曦足球會.md "wikilink")，於2007年6月4日轉投[越南聯賽](../Page/越南.md "wikilink")球隊[大南城至](../Page/大南城足球俱樂部.md "wikilink")2007年9月。及後，歷高在9月尾重返香港，轉投至[東方](../Page/東方足球隊.md "wikilink")。

## 外部連結

  - [香港足總球員資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=122)

[Category:晨曦球員](../Category/晨曦球員.md "wikilink")
[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:聖十字球員](../Category/聖十字球員.md "wikilink")
[Category:峴港球員](../Category/峴港球員.md "wikilink")
[Category:阿雷格里港人](../Category/阿雷格里港人.md "wikilink")
[Category:香港巴西籍足球運動員](../Category/香港巴西籍足球運動員.md "wikilink")