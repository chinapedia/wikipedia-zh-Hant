[Death_rate_world_map.PNG](https://zh.wikipedia.org/wiki/File:Death_rate_world_map.PNG "fig:Death_rate_world_map.PNG")

**死亡率**是用來衡量一部分[人口中](../Page/人口.md "wikilink")，一定規模的人口大小、每單位時間的[死亡數目](../Page/死亡.md "wikilink")（整體或歸因於指定因素）。死亡率通常以每年每一千人為單位來表示；因此在死亡率為9.5的10萬人口中，表示這一人口中每年死去950人。死亡率有別於[發病率](../Page/發病率.md "wikilink")，發病率是指一定規模的人口在一定時間內罹患該病新增加例數（發病率）。[患病率是指一定時間一定規模人群中某病新舊病例總和](../Page/患病率.md "wikilink")。

辨別：

1.  **粗略死亡率**，每1000人的死亡總數。
2.  **[新生兒死亡率](../Page/新生兒死亡.md "wikilink")**，每1000個出生未滿一個月的[嬰兒和胎死](../Page/嬰兒.md "wikilink")（[死產兒](../Page/死產.md "wikilink")）的總和。
3.  **[孕產婦死亡率](../Page/孕產婦死亡.md "wikilink")**，每10萬個死於生產過程的死亡人口數。
4.  **[幼兒死亡率](../Page/幼兒死亡.md "wikilink")**，每1000個出生小於一歲的死亡人口數。
5.  ****（SMR）或****（ASMR），特定年齡（例如16-65或65以上）中每1000人的死亡總人口數。

死亡率 = 當區當年死亡人數\*1000/當區當年年中人口數‰。

死亡率可以得知一個地區的[衛生習慣和](../Page/衛生.md "wikilink")[醫療品質](../Page/醫療.md "wikilink")。通常越先進的國家死亡率越低，越落後的國家死亡率則越高。

## 統計

[Infantmortalityrate.jpg](https://zh.wikipedia.org/wiki/File:Infantmortalityrate.jpg "fig:Infantmortalityrate.jpg")的死亡率\]\]
前十大高**嬰兒死亡率**的國家有：

1.  192.50

2.  165.96

3.  145.24

4.  137.08

5.  130.51

6.  122.66

7.  118.52

8.  117.99

9.  112.10

10. 108.72

根據[世界衛生組織](../Page/世界衛生組織.md "wikilink")，2002年十大死亡原因\[1\]為：

1.  12.6%局部缺血心臟病
2.  9.7%腦血管疾病
3.  6.8%下呼吸道感染
4.  4.9% HIV/AIDS
5.  4.8%慢性阻塞性肺病
6.  3.2%腹瀉疾病
7.  2.7%結核病
8.  2.2%瘧疾
9.  2.2%呼吸道/支氣管/肺癌
10. 2.1%道路交通事故

已開發和開發中國家的死亡原因有很大的不同。詳見[致死原因及所占比例列表的全球統計](../Page/致死原因及所占比例列表.md "wikilink")。

## 影響死亡率的因素

  - 人口年齡
  - 營養水準
  - 飲食和居住所
  - 獲取乾淨的飲用水
  - 醫療水準
  - 傳染病程度
  - 暴力犯罪程度
  - 衝突
  - 醫生數量
  - 氣候

## 參考文獻

## 參閱

  - [出生率](../Page/出生率.md "wikilink")
  - [死亡](../Page/死亡.md "wikilink")
  - [人口學](../Page/人口學.md "wikilink")
  - [生命期望](../Page/生命期望.md "wikilink")
  - [發病率](../Page/發病率.md "wikilink")
  - [病死率](../Page/病死率.md "wikilink")
  - [預後](../Page/預後.md "wikilink")

## 外部連結

  - [CIA世界概況 -
    死亡率](https://www.cia.gov/library/publications/the-world-factbook/rankorder/2066rank.html)
  - [來自疾病控制中心的「美國十大死因」](http://webapp.cdc.gov/sasweb/ncipc/leadcaus10.html)
  - [Edmond
    Halley，*人類死亡率梯度的評論*（1693）。](http://www.pierre-marteau.com/editions/1693-mortality.html)
  - [以年齡和死因排序的美國死亡率數據（來自Data360）](http://www.data360.org/graph_group.aspx?Graph_Group_Id=347)

[Category:人口](../Category/人口.md "wikilink")
[Category:人口學](../Category/人口學.md "wikilink")
[Category:流行病學](../Category/流行病學.md "wikilink")
[Category:死亡](../Category/死亡.md "wikilink")
[Category:保險學](../Category/保險學.md "wikilink")
[Category:人口生態學](../Category/人口生態學.md "wikilink")
[Category:精算](../Category/精算.md "wikilink")

1.  <http://www.who.int/entity/whr/2004/annex/topic/en/annex_2_en.pdf>