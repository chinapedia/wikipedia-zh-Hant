[Shu_Yu_Zhou_Zi_Lu.jpg](https://zh.wikipedia.org/wiki/File:Shu_Yu_Zhou_Zi_Lu.jpg "fig:Shu_Yu_Zhou_Zi_Lu.jpg")

《**殊域周咨录**》是[明朝](../Page/明朝.md "wikilink")[行人司行人和刑科右给事中](../Page/行人司.md "wikilink")[严从简](../Page/严从简.md "wikilink")，在[明神宗](../Page/明神宗.md "wikilink")[万历二年](../Page/万历.md "wikilink")（1574年）始撰写的一部关于明朝边疆历史和中外交通史的书籍；於1583年成書。严从简，别号绍峰子，[明代](../Page/明代.md "wikilink")[浙江](../Page/浙江.md "wikilink")[嘉禾人](../Page/嘉兴.md "wikilink")。[嘉靖三十八年进士](../Page/嘉靖.md "wikilink")，曾任[婺源](../Page/婺源.md "wikilink")[县丞和](../Page/县丞.md "wikilink")[扬州](../Page/扬州.md "wikilink")[同知](../Page/同知.md "wikilink")，后入[行人司任行人和刑科右给事中](../Page/行人司.md "wikilink")。

[明朝时中国和周边国家](../Page/明朝.md "wikilink")、海外国家往来密切，[明太祖派使臣出使](../Page/明太祖.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")、[琉球](../Page/琉球.md "wikilink")、[日本](../Page/日本.md "wikilink")、[安南](../Page/越南.md "wikilink")、[真腊](../Page/真腊.md "wikilink")、[三佛齐](../Page/三佛齐.md "wikilink")、[古里](../Page/古里.md "wikilink")、[哈烈](../Page/哈烈.md "wikilink")、[撒马儿罕等国](../Page/撒马儿罕.md "wikilink")；[明成祖派](../Page/明成祖.md "wikilink")[三宝太监](../Page/三宝太监.md "wikilink")[郑和下西洋](../Page/郑和下西洋.md "wikilink")。明太祖设立行人司，掌管外事。

在行人司任职的严从简，接触许多外来使臣，得知海外国家和边疆国家的状况，又掌握大量内部文件和记录，为他撰写此书打下基础。他撰写此书时参考了大量前人的著作，但他认为历史记录可从书中查到，但中外来往的内部文件则是外间难以见到，因此他厚今薄古，偏重记述当代的事件，为明朝出使外国的使臣提供参考。

《殊域周咨录》将[女直列入东北夷](../Page/女直.md "wikilink")，因此在[清朝被列为禁书](../Page/清朝.md "wikilink")。

## 内容

《殊域周咨录》共24卷，以厚今薄古的宗旨，着重叙述当代边疆各国和海外国家的人文、风土、地理、以及和中国的来往，并将周边国家，按地理方位分为[东夷](../Page/东夷.md "wikilink")、[西戎](../Page/西戎.md "wikilink")、[南蛮](../Page/南蛮.md "wikilink")、[北狄](../Page/北狄.md "wikilink")：

  - 东夷：[朝鮮](../Page/朝鮮.md "wikilink")、[日本](../Page/日本.md "wikilink")、[琉球](../Page/琉球.md "wikilink")、
  - 南蛮：[安南](../Page/越南.md "wikilink")、[占城](../Page/占城.md "wikilink")、[真臘](../Page/真臘.md "wikilink")、[暹羅](../Page/暹羅.md "wikilink")、[滿剌加](../Page/滿剌加.md "wikilink")、[爪哇](../Page/爪哇.md "wikilink")、[三佛齊](../Page/三佛齊.md "wikilink")、[浡泥](../Page/浡泥.md "wikilink")、[瑣里](../Page/瑣里.md "wikilink")、[蘇門答剌](../Page/蘇門答剌.md "wikilink")、[錫蘭](../Page/錫蘭.md "wikilink")、[蘇祿](../Page/蘇祿.md "wikilink")、[麻剌](../Page/麻剌.md "wikilink")、[忽魯謨斯](../Page/忽魯謨斯.md "wikilink")、[佛郎機](../Page/佛郎機.md "wikilink")、
  - 西戎：[吐蕃](../Page/吐蕃.md "wikilink")、[拂菻](../Page/拂菻.md "wikilink")、[榜葛剌](../Page/榜葛剌.md "wikilink")、[默德那](../Page/默德那.md "wikilink")、[天方](../Page/天方.md "wikilink")、[土魯番](../Page/土魯番.md "wikilink")、[哈密卫](../Page/哈密卫.md "wikilink")、[赤斤蒙古](../Page/赤斤蒙古.md "wikilink")、[安定卫](../Page/安定卫.md "wikilink")、[阿端卫](../Page/阿端卫.md "wikilink")、[曲先卫](../Page/曲先卫.md "wikilink")、[罕東卫](../Page/罕東卫.md "wikilink")、[火州](../Page/火州.md "wikilink")、[撒馬兒罕](../Page/撒馬兒罕.md "wikilink")、[亦力把力](../Page/亦力把力.md "wikilink")、[于闐](../Page/于闐.md "wikilink")、[哈烈](../Page/哈烈.md "wikilink")
  - 北狄：[韃靼](../Page/韃靼.md "wikilink")、[兀良哈](../Page/兀良哈.md "wikilink")。
  - 东北夷：[女直](../Page/女直.md "wikilink")。\[1\]

## 版本

  - [北京图书馆万历年刻本](../Page/北京图书馆.md "wikilink")。《殊域周咨录》卷二十四将女直称为东北夷，在清代成为禁书，无刻本或抄本。
  - [故宫博物院](../Page/故宫博物院.md "wikilink") 民国十九年铅字本

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
[category:中外交通史文献](../Page/category:中外交通史文献.md "wikilink")
[category:地理書籍](../Page/category:地理書籍.md "wikilink")

[Category:史部地理類](../Category/史部地理類.md "wikilink")
[Category:明朝史書](../Category/明朝史書.md "wikilink")
[Category:1580年代書籍](../Category/1580年代書籍.md "wikilink")

1.  （明）严从简著 《殊域周咨录》余思黎点校 中华书局 ISBN 71010006078