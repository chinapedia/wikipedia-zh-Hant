《**残疾人权利公约**》是[联合国于](../Page/联合国.md "wikilink")2006年12月13日通过的有关保护[残疾人人权的](../Page/身心障礙者人權.md "wikilink")[国际公约](../Page/国际公约.md "wikilink")，亦稱《**身心障礙者權利公約**》（Convention
on the Rights of Persons with
Disabilities），簡稱CRPD。这条公约是由192个会员国代表，与90个非政府组织代表，在2006年8月25日联合国特别会议中达成的草案。2006年12月13日聯合國通過CRPD。

CRPD為21世紀第一個人權公約，影響全球身心障礙者之權利保障，也是联合国所通过的国际性公约中，第一次用於保护残疾人人权的公约。為聯合國促進、保障及確保身心障礙者完全及平等地享有所有人權及基本自由，促進固有尊嚴受到尊重，降低身心障礙者在社會上之不利狀態，以使其得以享有公平機會參與社會之公民、政治、經濟、社會及文化領域。

公约的内容在2007年3月30日向国际間公開。2008年5月3日，《残疾人权利公约》正式生效。\[1\]\[2\]

## 歷史背景

聯合國於1981年到1992年訂為身心障礙者的十年。1987年時，一個集結了各專家的全球性會議建議聯合國大會應起草一個國際公約來消除對於身心障礙人士的歧視。該國際聲明的大綱開始由義大利提出，隨後由瑞典接續工作，可惜沒有達成共識。許多政府的代表，認為當時的人權條款就已經足夠處理身心障礙者的需求。因此倡議性質的身心障礙者機會平等準則(Standard
Rules on the Equalisation of Opportunities for Persons with
Disabilities)於1993年在聯合國大會上被提出。西元2000年，五位國際身心障礙者非政府協會發表了一個聲明，呼籲所有的政府支持該公約。2001年,在聯合國大會上，墨西哥提出了一個請求，要求成立非常設委員會，用以整合出一個容易理解的公約，全面性地去推廣並保護身心障礙者的權利與尊嚴。\[3\]
由相關身心障礙者人權機構，例如國際身心障礙者聯盟來擔任國際非常設身心障礙者幹部會議的整合單位，參與起草公約的過程，試著為身心障礙者訂出一些特定的原則，研究如何為身心障礙者及相關組織訂定身心障礙者公約。\[4\]

## CRPD的8大原則

  - 尊重他人、尊重他人自己做的決定
  - 不歧視
  - 充分融入社會
  - 尊重每個人不同之處，接受身心障礙者是人類多元性的一種
  - 機會均等
  - 無障礙
  - 男女平等
  - 尊重兒童，保障身心障礙兒童的權利

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [残疾人权利公约](http://www.un.org/chinese/disabilities/convention/)
  - [身心障礙者權利公約正體中文版](https://crpd.sfaa.gov.tw/BulletinCtrl?func=downloadFile&type=file&id=682&code=08090608060C01040401060900030904)

[Category:联合国公约和盟约](../Category/联合国公约和盟约.md "wikilink")
[身心障礙權利](../Category/身心障礙權利.md "wikilink")

1.
2.
3.  O'Reilly, A. (2003) [A UN Convention on the Rights of Persons with
    Disabilities: The Next
    Steps](http://www.disabilityworld.org/01-03_03/news/unconvention.shtml)
     Paper presented at the General Assembly Meeting of Rehabilitation
    International Arab Region, 8–9 March 2003, Kingdom of Bahrain.
4.  [Handicap International UK - UN Convention on the Rights of Persons
    with
    Disabilities](http://www.handicap-international.org.uk/what_we_do/disability_rights/un_convention)