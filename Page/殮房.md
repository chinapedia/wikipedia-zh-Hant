[HK_Victoria_Road_34_Sai_Ning_Street_Victoria_Public_Mortuary.JPG](https://zh.wikipedia.org/wiki/File:HK_Victoria_Road_34_Sai_Ning_Street_Victoria_Public_Mortuary.JPG "fig:HK_Victoria_Road_34_Sai_Ning_Street_Victoria_Public_Mortuary.JPG")[西環](../Page/西環.md "wikilink")[域多利亞公眾-{殮房}-](../Page/域多利亞公眾殮房.md "wikilink")\]\]

**-{zh-hans:太平间;zh-hk:殮房;zh-sg:停尸间;zh-tw:太平間;}-**，又稱-{zh-hans:**停尸房**、**殓房**;zh-hk:**停屍間**、**太平間**;zh-sg:**太平间**、**殓房**;zh-tw:**停屍房**、**殮房**;}-、**陈尸所**、**往生室**，是[医院](../Page/医院.md "wikilink")、[殡仪馆或地区停放](../Page/殡仪馆.md "wikilink")[遺體的场所](../Page/遺體.md "wikilink")。可能是一間[房](../Page/房.md "wikilink")、一層[樓](../Page/樓.md "wikilink")，或一幢獨立的大樓。除了[中東地區因宗教習俗大多在](../Page/中東地區.md "wikilink")24小時內下葬，其他地區在一個人[死亡之後](../Page/死亡.md "wikilink")，[遺體很少立即](../Page/屍體.md "wikilink")[火化或](../Page/火化.md "wikilink")[下葬](../Page/下葬.md "wikilink")，而是會在殮房停放上至少兩、三天，原因有很多，如：給予後人有充足的時間安排[葬禮儀式](../Page/葬禮.md "wikilink")、確定死者不是[假死才下葬](../Page/深度昏迷.md "wikilink")、死者身份不明，需要家属[验尸或](../Page/验尸.md "wikilink")[DNA指纹分析](../Page/DNA指纹分析.md "wikilink")、因為[死因不明](../Page/死因.md "wikilink")，家属要求[病理学医师验尸](../Page/病理学.md "wikilink")、警方需要[死因调查](../Page/死因调查.md "wikilink")、当地风俗要求特别手续，如[僵化或](../Page/木乃伊.md "wikilink")[尸体防腐等等](../Page/尸体防腐.md "wikilink")。

## 各地太平間

### 香港

[香港有部分醫院的殮房附設有小禮堂供先人出殯之用](../Page/香港.md "wikilink")，比如說[廣華醫院的](../Page/廣華醫院.md "wikilink")「追思堂」殮房的停屍間有些是設有[雪櫃的](../Page/雪櫃.md "wikilink")，以避免遺體在室溫下[腐敗](../Page/腐敗.md "wikilink")。

### 臺灣

於[台灣](../Page/台灣.md "wikilink")，如果人是在醫院往生，幾乎大部份的大型醫院的[地下室都有整層的太平間](../Page/地下室.md "wikilink")，並提供宗教錄音帶助念的服務。大部份遺體按[台灣喪葬習俗放置在太平間的助念室](../Page/台灣喪葬.md "wikilink")[助念八個小時滿之後](../Page/助念.md "wikilink")，往生者的家屬就會將往生者的遺體運回家中開始準備後事，或者是委託禮儀公司派[禮儀師來協助家屬開始準備後事](../Page/禮儀師.md "wikilink")。如果人是在外面往生，大部份的家屬會直接將往生者的遺體運送到殯儀館存放到冰櫃裡。如果是在家裡往生，都市人的家屬大部份會直接將往生者的遺體運送到殯儀館存放到冰櫃裡，鄉下人的家屬大部份會聯絡禮儀公司送冰櫃到家中來存放遺體\[1\]。

## 参考文献

## 参见

  - [验尸](../Page/验尸.md "wikilink")
  - [殡仪馆](../Page/殡仪馆.md "wikilink")
  - [火葬场](../Page/火葬场.md "wikilink")

[Category:喪葬](../Category/喪葬.md "wikilink")
[Category:建築物](../Category/建築物.md "wikilink")

1.  [生命的終極關懷 （第二章～後事處理）](http://book.bfnn.org/books2/1886.htm)