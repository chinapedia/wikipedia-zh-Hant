**殷允芃**（），[山東](../Page/山東.md "wikilink")[滕縣人](../Page/滕縣.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[記者](../Page/記者.md "wikilink")、[作家](../Page/作家.md "wikilink")，為台灣當代新聞及出版業的傑出代表，現任《[天下雜誌](../Page/天下雜誌.md "wikilink")》群創辦人、[董事長兼總編集長](../Page/董事長.md "wikilink")，曾言：「我以做一位記者為樂」。

## 簡歷

殷允芃成長於臺灣[彰化縣](../Page/彰化縣.md "wikilink")[員林鎮](../Page/員林鎮.md "wikilink")，後於[北一女中](../Page/北一女中.md "wikilink")[高中部](../Page/高中.md "wikilink")、[國立成功大學外文系](../Page/國立成功大學.md "wikilink")[畢業](../Page/畢業.md "wikilink")。她去[美國](../Page/美國.md "wikilink")[愛荷華大學唸](../Page/愛荷華大學.md "wikilink")「新聞傳播」，取得[碩士](../Page/碩士.md "wikilink")[學位後](../Page/學位.md "wikilink")，曾任美國《[費城詢問報](../Page/費城詢問報.md "wikilink")》記者。在那段時期，她曾利用業餘時間訪問了一連串美國的華人代表，其中她最感珍貴的是訪問到了[張愛玲](../Page/張愛玲.md "wikilink")。

返台後，她曾任職於[美國新聞處](../Page/美國新聞處.md "wikilink")，擔任過[合眾國際社記者](../Page/合眾國際社.md "wikilink")、美國《[紐約時報](../Page/紐約時報.md "wikilink")》駐華記者、《[亞洲華爾街日報](../Page/亞洲華爾街日報.md "wikilink")》駐華特派員，並曾任教於[國立政治大學新聞系](../Page/國立政治大學.md "wikilink")。1981年殷允芃與友人共同創辦《[天下雜誌](../Page/天下雜誌.md "wikilink")》，日後殷允芃又陸續創辦《[康健雜誌](../Page/康健雜誌.md "wikilink")》（1998）、《[Cheers快樂工作人雜誌](../Page/Cheers快樂工作人雜誌.md "wikilink")》（2000）、《[親子天下雜誌](../Page/親子天下雜誌.md "wikilink")》（2008）、[天下雜誌出版](../Page/天下雜誌出版.md "wikilink")（2000）與[天下雜誌教育基金會](../Page/天下雜誌教育基金會.md "wikilink")（2002）等事業體，統稱《[天下雜誌](../Page/天下雜誌.md "wikilink")》群。

殷允芃曾任第一、二、三屆[國統會委員](../Page/國統會.md "wikilink")、[國家發展會議委員](../Page/國家發展會議.md "wikilink")、[文復會委員及](../Page/文復會.md "wikilink")[經發會委員](../Page/經發會.md "wikilink")，並於1994年到1996年間應[李遠哲邀請擔任](../Page/李遠哲.md "wikilink")[行政院教育改革審議委員會委員](../Page/行政院教育改革審議委員會.md "wikilink")。

殷允芃曾獲頒愛荷華大學傑出校友、成功大學傑出校友、[中華民國十大傑出女青年](../Page/中華民國十大傑出女青年.md "wikilink")，三次獲頒[金鼎獎](../Page/金鼎獎.md "wikilink")，1987年獲[麥格塞塞獎新聞獎](../Page/麥格塞塞獎.md "wikilink")，1995年被《[亞洲周刊](../Page/亞洲周刊.md "wikilink")》評為亞洲最有影響力的女性之一。2010獲頒政治大學名譽文學博士，並於同年榮獲[卓越新聞獎新聞志業終身成就獎](../Page/卓越新聞獎.md "wikilink")。

2011年，殷允芃首次擔任紀錄片導演及編劇，製作記錄好友社會學家[成露茜燦爛人生的紀錄片](../Page/成露茜.md "wikilink")[《綿延的生命》](http://www.cw.com.tw/article/article.action?id=5031653)，獲得[金穗獎](../Page/金穗獎.md "wikilink")「最佳紀錄片獎」。

2012年，因於新聞出版專業有特殊成就與貢獻，對新聞專業及倫理之堅持與執著，獲頒第36屆金鼎獎[雜誌類特別貢獻獎](http://www.cw.com.tw/article/article.action?id=5033696)。

2016年5月12日，獲總統[馬英九授予二等](../Page/馬英九.md "wikilink")[景星勳章](../Page/景星勳章.md "wikilink")。

## 著作

除英文專作外，主要中文著作為：

1971 《中國人的光輝及其他：當代名人訪問錄》
1974 《新起的一代》
1982 《決策者》
1985 《太平洋世紀的主人》
1987 《等待英雄》
1992 《點燈的人》
1992 《發現台灣》（合著）
1996 《敬天愛人》
1999 《素直的心》
影像作品：
2011 [《綿延的生命
Lucie的人生探索》成露茜紀錄片](http://www.cw.com.tw/article/article.action?id=5031653)
2013 [《發現美麗台灣之春夏秋冬》紀錄片](http://www.youtube.com/watch?v=TZlt_zozH1M)

## 參考資料

<div class="references-small">

</div>

[Category:中華民國記者](../Category/中華民國記者.md "wikilink")
[Category:中華民國女性作家](../Category/中華民國女性作家.md "wikilink")
[Category:台灣女性企業家](../Category/台灣女性企業家.md "wikilink")
[Category:愛荷華大學校友](../Category/愛荷華大學校友.md "wikilink")
[Category:國立成功大學校友](../Category/國立成功大學校友.md "wikilink")
[Category:臺北市立第一女子高級中學校友](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[Category:台灣戰後山東移民](../Category/台灣戰後山東移民.md "wikilink")
[Category:滕州人](../Category/滕州人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Y允](../Category/殷姓.md "wikilink")
[Category:麥格塞塞獎獲得者](../Category/麥格塞塞獎獲得者.md "wikilink")