**殷汝耕**（），[浙江](../Page/浙江.md "wikilink")[温州](../Page/温州.md "wikilink")[平陽人](../Page/平阳县.md "wikilink")（今属苍南），字亦農。中國的財稅官僚與近代政治人物，並曾出任日本扶植的[冀東防共自治政府要職](../Page/冀東防共自治政府.md "wikilink")。

## 早年

殷汝耕於1883年出生，光绪30年12月(1904)官费留學日本(21岁)，宣统元年(1909)入第七高等工科,後加入[同盟會](../Page/中國同盟會.md "wikilink")，曾追隨[黃興參加](../Page/黃興.md "wikilink")[辛亥革命](../Page/辛亥革命.md "wikilink")。1913年[二次革命失敗以後再赴](../Page/二次革命.md "wikilink")[日本留學](../Page/日本.md "wikilink")。1919年從[早稻田大學畢業回國](../Page/早稻田大學.md "wikilink")，進入[北洋政府財政部裡擔任司長](../Page/北洋政府.md "wikilink")，與[殷同](../Page/殷同.md "wikilink")、[袁良](../Page/袁良.md "wikilink")（前北平市長）、[程克](../Page/程克.md "wikilink")（前天津市長）並稱日本通四巨頭。殷汝耕也用妻姓，取了个日本名叫井上耕二。

## 漢奸

1935年，日本與中國在華北的衝突增加。11月24日，在[土肥原賢二慫恿下](../Page/土肥原賢二.md "wikilink")，冀東「非武裝區」專員殷汝耕宣佈「自治」，成立「冀東防共自治委員會」。\[1\]殷汝耕出任冀東防共自治政府委員長，宣佈脫離[國民政府](../Page/國民政府.md "wikilink")，並且將[通縣](../Page/通縣.md "wikilink")、[臨榆](../Page/臨榆.md "wikilink")、[撫寧](../Page/撫寧.md "wikilink")、[昌黎](../Page/昌黎.md "wikilink")、[盧龍](../Page/盧龍.md "wikilink")、[遷安](../Page/遷安.md "wikilink")、[灤縣](../Page/灤縣.md "wikilink")、[樂亭](../Page/樂亭.md "wikilink")、[豐潤等二十二縣納入其轄下](../Page/豐潤.md "wikilink")，因此被國民政府通緝。冀東防共自治政府殷汝耕為行政長官，以[池宗墨任祕書長](../Page/池宗墨.md "wikilink")，下設民政、財政、外交等五廳。國民政府成立[冀察政務委員會與殷汝耕等人進行對抗](../Page/冀察政務委員會.md "wikilink")。

1937年3月2日，殷汝耕飛長春面見[板垣征四郎](../Page/板垣征四郎.md "wikilink")，商談擴大偽組織事宜\[2\]。6月17日，殷汝耕所屬教導隊在冀東玉田縣城北強繳民間自衛槍械，激起人民反抗，參加反抗者共21村\[3\]。6月21日，殷汝耕由通縣至天津，往見日本[華北駐屯軍司令](../Page/華北駐屯軍.md "wikilink")[田代皖一郎](../Page/田代皖一郎.md "wikilink")，議定擴充「保安隊」實力，並重新分配防區\[4\]。殷汝耕後來在1937年7月29日發生的[通州事件中失去了日方的信賴](../Page/通州事件.md "wikilink")。

1938年2月1日冀東防共自治政府被併入[王克敏的](../Page/王克敏.md "wikilink")[中華民國臨時政府](../Page/中华民国临时政府_\(1937–1940\).md "wikilink")。[抗戰勝利後](../Page/抗戰.md "wikilink")，殷汝耕被[重慶國民政府以](../Page/重慶.md "wikilink")[漢奸的罪名逮捕](../Page/漢奸.md "wikilink")，入押在南京[老虎桥监狱](../Page/老虎桥监狱.md "wikilink")，在狱中手抄《[金刚经](../Page/金刚经.md "wikilink")》。

## 身亡

1947年被判處[死刑](../Page/死刑.md "wikilink")。12月1日上午，前冀東自治政府主席殷汝耕在南京老虎桥監獄刑場被枪决\[5\]。臨刑前寫[汪精衛](../Page/汪精衛.md "wikilink")〈飛花〉詩一首：“春來春去有定時，花開花落無盡期，人生代謝亦如此，殺身成仁何所辭。”\[6\]，执行庭设在南京可容万人的朝天宫大殿，这天殿内殿外挤满观看的市民。

## 家族

殷汝耕有一兄長[殷汝驪](../Page/殷汝驪.md "wikilink")，為國民政府前財政部次長。汝骊厌恶汝耕，曾对[陈铭枢说](../Page/陈铭枢.md "wikilink")：“此弟品质极坏，只要有利可图，他就能卖友，甚至会出卖民族。”殷汝驪之子[殷之浩為](../Page/殷之浩.md "wikilink")[大陸工程公司創辦人](../Page/大陸工程公司.md "wikilink")，殷之浩之女[殷琪曾為](../Page/殷琪.md "wikilink")[台灣高鐵董事長](../Page/台灣高速鐵路股份有限公司.md "wikilink")。

## 参考文献

## 外部链接

  - [1947年12月1日：大汉奸殷汝耕被枪决](https://web.archive.org/web/20100205131025/http://bbs.tiexue.net/post_4075068_1.html)

[Category:與大日本帝國合作的中國人](../Category/與大日本帝國合作的中國人.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:温州人](../Category/温州人.md "wikilink")
[Category:被中華民國處決者](../Category/被中華民國處決者.md "wikilink")
[Category:被處決的中華民國人](../Category/被處決的中華民國人.md "wikilink")
[R汝](../Category/殷姓.md "wikilink")

1.  [李新總主編](../Page/李新_\(1918年\).md "wikilink")，[中國社會科學院近代史研究所中華民國史研究室編](../Page/中國社會科學院.md "wikilink")，周天度、鄭則民、齊福霖、李義彬等著，《[中華民國史](../Page/中華民國史.md "wikilink")》第八卷，[北京](../Page/北京.md "wikilink")：[中華書局](../Page/中華書局.md "wikilink")，2011年7月，第378頁

2.

3.
4.
5.
6.  汪精衛：《雙照樓集》