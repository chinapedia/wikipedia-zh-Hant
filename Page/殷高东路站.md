**殷高东路站**\[1\]，位于[上海](../Page/上海.md "wikilink")[杨浦区](../Page/杨浦区.md "wikilink")[淞沪路](../Page/淞沪路.md "wikilink")[殷高路](../Page/殷高路.md "wikilink")，为[上海轨道交通十号线的地下车站](../Page/上海轨道交通十号线.md "wikilink")。于2010年4月10日启用。

## 车站结构

### 楼层分布

## 出口

## 接驳交通

### 公交

168

## 临近车站

## 参考文献

  - [殷高路站建设工程竣工规划验收合格证](http://www.shghj.gov.cn/Ct_4.aspx?ct_id=00070808D12613)

## 外部链接

  -
## 参见

  - [新江湾城街道](../Page/新江湾城街道.md "wikilink")
      - [新江湾城](../Page/新江湾城.md "wikilink")
  - [殷高东路](../Page/殷高东路.md "wikilink")

{{-}}

[Category:杨浦区地铁车站](../Category/杨浦区地铁车站.md "wikilink")
[Category:新江湾城街道](../Category/新江湾城街道.md "wikilink")
[Category:2010年启用的铁路车站](../Category/2010年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")

1.  [10号线一期31车站定名
    一期主线世博前建成](http://www.jfdaily.com/news/xwsh/200902/t20090206_528499.htm)