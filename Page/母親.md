[Japanese_mother_and_sons.jpg](https://zh.wikipedia.org/wiki/File:Japanese_mother_and_sons.jpg "fig:Japanese_mother_and_sons.jpg")
[Charles_Turner04.jpg](https://zh.wikipedia.org/wiki/File:Charles_Turner04.jpg "fig:Charles_Turner04.jpg")

**母親**簡稱**母**，或稱**媽媽**，是一種[親屬關係的](../Page/親屬關係.md "wikilink")[稱謂](../Page/稱謂.md "wikilink")，是[子女對](../Page/子女.md "wikilink")[雙親中的](../Page/雙親.md "wikilink")[女性的稱呼](../Page/女性.md "wikilink")。母親和子女是重要的直系親屬關係之一，通常具有[親密關係](../Page/親密關係.md "wikilink")。

一般指生育小孩的女人（即生母）；但在有些狀況下，僅[養育小孩](../Page/養母.md "wikilink")，或[再婚配偶](../Page/繼母.md "wikilink")、[結誼長輩](../Page/義母.md "wikilink")，或是僅提供[卵子讓](../Page/卵子.md "wikilink")[精子與之結合](../Page/精子.md "wikilink")（例如[捐卵等](../Page/捐卵.md "wikilink")），但並未親自生育小孩的女人也可如此稱呼。

## 定義

在[生物學上](../Page/生物學.md "wikilink")，子女體細胞中成對的[染色體](../Page/染色體.md "wikilink")，有一半是由母親的[卵子的提供](../Page/卵子.md "wikilink")，因此可藉由[DNA分析來辨別親屬關係](../Page/脱氧核糖核酸.md "wikilink")，且[父親](../Page/父親.md "wikilink")[精子與卵子結合時](../Page/精子.md "wikilink")，只有提供細胞核的遺傳物質，因此子女細胞中[粒線體的DNA皆來自母親](../Page/粒線體.md "wikilink")，可由此來判別母系祖譜。

在[社會學上](../Page/社會學.md "wikilink")，母親可能代表了養育與教養子女成長的女性，在[法律上](../Page/法律.md "wikilink")，女性也可以經由合法的管道，領養子女，或與有子女的男性結婚，進而成為該子女的法定母親。經領養而成為母親的稱為養母，與有子女男性結婚而成為母親的則稱為[繼母](../Page/繼母.md "wikilink")、[後母或](../Page/後母.md "wikilink")[晚娘](../Page/晚娘.md "wikilink")。

## 母親的稱呼

在日常口語中，小孩對母親有許多不同的稱呼，包括**媽**、**媽媽**，**妈咪**
有些地區子女（特別是古代）對母親的稱呼是**娘**、**阿娘**或**娘親**，又有**阿母**、**老媽**、**老媽子**（[粵語](../Page/粵語.md "wikilink")，北方話「老媽子」是指中年或老年[女僕](../Page/女僕.md "wikilink")）、**老母**等稱呼，滿語稱**額娘**（），時下更有親暱地稱為**母后**\[1\]\[2\]\[3\]。

對別人稱自己的母親有**家母**、**家慈**；稱已死去的母親為**先慈**、**先妣**\[4\]；對別人母親的尊稱是**令堂**或**令壽堂**（粵語口語中另稱**[伯母](../Page/伯母.md "wikilink")**）。要特別注意，有時候父親或母親的丈夫（繼父）也會用兒女對母親的稱呼——“媽媽”或“孩子他（她）媽”——代表他的妻子。

子女在不同的年龄阶段对其母亲的称呼也有所不同。幼儿及少儿时期，子女通常用一些较亲切的称呼，如**妈妈**等。青年之后，称呼通常为**妈**等。

在古代中國的漢族皇族中，[皇后所出的](../Page/皇后.md "wikilink")[皇子和](../Page/皇子.md "wikilink")[公主稱母親為](../Page/公主.md "wikilink")**母后**，[嬪御所出的子女須認皇后為嫡母](../Page/嬪御.md "wikilink")，稱皇后為母后，稱生母為**母妃**或与其他庶母一样称为**（阿）姨**（有的庶出皇子成为皇帝后，尊在世生母为[皇太后](../Page/皇太后.md "wikilink")，即给予生母父亲正妻的名分），皇子、親王的子女亦稱母親為母妃。[清代的皇子和公主則稱皇后作](../Page/清.md "wikilink")**皇額娘**、生母稱**額娘**。[英語](../Page/英語.md "wikilink")「母后」（Queen
Mother）一詞是指現任[國王或女王的母親](../Page/國王.md "wikilink")，即**王太后**，但王太后包括並非現任君主母親的前任王后。

在[中國的](../Page/中國.md "wikilink")[一夫多妻家庭中](../Page/一夫多妻.md "wikilink")，父親的[正妻相對於](../Page/正妻.md "wikilink")[庶出子女而言是](../Page/庶出.md "wikilink")[嫡母](../Page/嫡母.md "wikilink")，庶出子女稱嫡母為「大媽」、「大娘」。父親的[妾相對於正室或其他妾所生的子女而言是](../Page/妾.md "wikilink")[庶母](../Page/庶母.md "wikilink")，子女會把庶母稱為「小媽」、「○媽」（○為在妻妾中的排行）、「阿姨」、「姨娘」等，「阿姨」、「姨娘」的稱呼是源於一些地區對母親姊妹的稱呼。

在臺灣[鄒族稱母親為](../Page/鄒族.md "wikilink")**伊娜**（Ina）\[5\]。

## 母親角色

[On_Mangyong_Hill_05.JPG](https://zh.wikipedia.org/wiki/File:On_Mangyong_Hill_05.JPG "fig:On_Mangyong_Hill_05.JPG")一個母親\]\]

在任何一個社會文化中，都有對於母親應扮演角色職責的期望和規範，此即母親角色（Motherhood），例如在傳統中國，母親被期望扮演為一個盡心照顧公婆、操持家務、輔助丈夫、教育子女的重要角色。

在[美國](../Page/美國.md "wikilink")1960年代的第二波婦女運動中，[女性主義者開始對於西方傳統價值觀中對於母親角色的假設有所反省](../Page/女性主義.md "wikilink")，認為許多對於母親角色的期望（照顧小孩、負責家務），其實是父權制度的產物。

## 著名的母親

  - 中國古代[孟母三遷的故事](../Page/孟母三遷.md "wikilink")，是描述儒家學者[孟子幼時](../Page/孟子.md "wikilink")，母親為了讓他習得良好的品行，將住家搬到學堂旁，並激勵他努力向學，終於成為一位著名的學者。
  - 中國古代[岳母刺字的故事](../Page/岳母刺字.md "wikilink")：[岳飛的忠勇事蹟廣布民間](../Page/岳飛.md "wikilink")，其中-{岳}-母刺字的故事也是家喻戶曉。

## 衍生用法

在許多語言中都會將自己所屬的事物以「母」稱之，例如將自己的祖國稱為母國，畢業的學校稱為母校，出生成長所學的第一種語言為[母語](../Page/母語.md "wikilink")。

## 參見

  - [母親節](../Page/母親節.md "wikilink")
  - [母愛](../Page/母愛.md "wikilink")
  - [褓姆](../Page/褓姆.md "wikilink")
  - [代母](../Page/代母.md "wikilink")
  - [妻子](../Page/妻子.md "wikilink")
  - [婆媳](../Page/夫家.md "wikilink")
  - [娘家](../Page/妻家.md "wikilink")
  - [父親](../Page/父親.md "wikilink")
  - [母系社會](../Page/母系社會.md "wikilink")

## 參考文獻

[\*](../Category/母亲.md "wikilink")
[Category:女性亲属称谓](../Category/女性亲属称谓.md "wikilink")

1.  [陳奕ChenI 祝福我的母后～
    生日快樂！](http://tw.weibo.com/1764123974/4070485836789234)
2.  [祝母后生日快樂](https://huilamli.wordpress.com/2009/09/11/%E7%A5%9D%E6%AF%8D%E5%90%8E%E7%94%9F%E6%97%A5%E5%BF%AB%E6%A8%82/)
3.  [母后大人的生日](http://blog.xuite.net/keturah/twblog/120368558-%E6%AF%8D%E5%90%8E%E5%A4%A7%E4%BA%BA%E7%9A%84%E7%94%9F%E6%97%A5)
4.  《禮記·曲禮下》:「生曰父、曰母、曰妻，死曰考、曰妣、曰嬪。」
5.