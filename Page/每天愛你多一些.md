《**每天愛你多一些**》是出自[香港](../Page/香港.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")[張學友唱片](../Page/張學友.md "wikilink")《[情不禁](../Page/情不禁.md "wikilink")》的一首冠軍歌曲，由[寶麗金唱片公司首度於](../Page/寶麗金.md "wikilink")1991年1月23日發行，監製為[區丁玉](../Page/區丁玉.md "wikilink")。該曲[歌詞包括](../Page/歌詞.md "wikilink")[粵語和](../Page/粵語.md "wikilink")[國語兩個版本](../Page/國語.md "wikilink")。

## 背景

《每天愛你多一些》原曲為《[真夏的果實](../Page/真夏的果實.md "wikilink")》（），由[日本樂隊](../Page/日本.md "wikilink")[南方之星演唱](../Page/南方之星.md "wikilink")，作曲者為該樂隊的主唱[桑田佳祐](../Page/桑田佳祐.md "wikilink")。在[香港](../Page/香港.md "wikilink")，該曲經[林振強填詞](../Page/林振強.md "wikilink")、[杜自持編曲後](../Page/杜自持.md "wikilink")，更名為《每天愛你多一些》，通過歌手[張學友出色演繹](../Page/張學友.md "wikilink")，此曲逐漸成為[華人地區廣為人知的著名](../Page/華人地區.md "wikilink")[流行歌曲之一](../Page/流行歌曲.md "wikilink")。

在專輯《[情不禁](../Page/情不禁.md "wikilink")》推出后，《每天愛你多一些》成為廣受歡迎的一首單曲，在[香港電台](../Page/香港電台.md "wikilink")「[十大中文金曲排行榜](../Page/十大中文金曲.md "wikilink")」上榜多達32周。

1993年，[臺灣填詞人](../Page/臺灣.md "wikilink")[姚若龍重新以國語填寫歌詞](../Page/姚若龍.md "wikilink")，而歌名依舊為《每天愛你多一些》，也由張學友演唱，收錄于《[吻別](../Page/吻別.md "wikilink")》專輯中，但國語版本的影響力卻始終不及粵語版本，包括一些擅長國語的歌手在翻唱時仍舊選擇演唱粵語版本。

《每天愛你多一些》（粵語版）在推出后不僅迅速搶佔香港市場，也在[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[臺灣及](../Page/臺灣.md "wikilink")[中國大陸等地廣受歡迎](../Page/中國大陸.md "wikilink")。在推出《每天愛你多一些》（國語版）前的數年時間裡，《每天愛你多一些》（粵語版）為粵語流行音樂在其他國家和地區的普及做出了一定的貢獻。

## 派臺成績

| 榜單       | 現時排名 | 最高排名   | 上榜週數 | 冠軍週數   | 備註 |
| -------- | ---- | ------ | ---- | ------ | -- |
| **RTHK** | \-   | **冠軍** | 17   | **2周** | \- |
| **903**  | \-   | **冠軍** | 36   | **4周** | \- |
| **TVB**  | \-   | **冠軍** | 29   | **3周** | \- |

## 獲得獎項

  - 1991年

<!-- end list -->

  - 十大勁歌金曲第一季入選金曲
  - [十大勁歌金曲](../Page/十大勁歌金曲頒獎典禮.md "wikilink")
  - 十大勁歌金曲——金曲金獎
  - 第十四屆[十大中文金曲](../Page/十大中文金曲.md "wikilink")
  - 十大中文金曲IFPI國際大碟大獎──情不禁
  - 十大中文金曲監製大獎
  - 叱咤樂壇男歌手金獎——演唱歌曲：每天愛你多一些
  - 叱咤樂壇至尊歌曲大獎──每天愛你多一些
  - [叱咤樂壇我最喜愛的歌曲大獎](../Page/叱咤樂壇流行榜.md "wikilink")
  - 叱咤樂壇大碟IFPI大獎──情不禁
  - [澳門電台歌中之歌大獎](../Page/澳門電台.md "wikilink")

<!-- end list -->

  - 1997年

<!-- end list -->

  - [商業電台](../Page/商業電台.md "wikilink")「叱咤殿堂至尊歌」
  - [香港電台](../Page/香港電台.md "wikilink")「金曲廿載十大最愛」

<!-- end list -->

  - 1999年

<!-- end list -->

  - [香港電台](../Page/香港電台.md "wikilink")「世紀十大中文金曲」

<!-- end list -->

  - 其它

<!-- end list -->

  - 各大音樂頒獎禮的「最佳歌曲獎」

## 參考文獻

[Category:中文流行歌曲](../Category/中文流行歌曲.md "wikilink")
[Category:1991年歌曲](../Category/1991年歌曲.md "wikilink")
[Category:張學友歌曲](../Category/張學友歌曲.md "wikilink")
[Category:香港歌曲](../Category/香港歌曲.md "wikilink")
[Category:翻唱歌曲](../Category/翻唱歌曲.md "wikilink")
[Category:叱咤樂壇我最喜愛的歌曲](../Category/叱咤樂壇我最喜愛的歌曲.md "wikilink")