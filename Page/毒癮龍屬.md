**毒癮龍屬**（屬名：*Venenosaurus*）是種相當小型的[蜥腳下目](../Page/蜥腳下目.md "wikilink")[巨龍形類](../Page/巨龍形類.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，身長約10公尺。化石分別來自一個成年與幼年個體，發現於[猶他州的](../Page/猶他州.md "wikilink")[雪松山脈組的Poison](../Page/雪松山脈組.md "wikilink")
Strip段，年代為早[白堊紀的](../Page/白堊紀.md "wikilink")[阿普第階](../Page/阿普第階.md "wikilink")。

化石是在1998年由[丹佛自然歷史博物館的Anthony](../Page/丹佛自然歷史博物館.md "wikilink")
DiCroce發現，並在2001年由[肯尼思·卡彭特](../Page/肯尼思·卡彭特.md "wikilink")（Kenneth
Carpenter）等人所敘述、命名。毒癮龍的[正模標本](../Page/正模標本.md "wikilink")（編號DMNH
40932）包括：[頸椎](../Page/頸椎.md "wikilink")、左[肩胛骨](../Page/肩胛骨.md "wikilink")、右[橈骨](../Page/橈骨.md "wikilink")、左[尺骨](../Page/尺骨.md "wikilink")、[指骨](../Page/指骨.md "wikilink")、左[恥骨](../Page/恥骨.md "wikilink")、左右[恥骨](../Page/恥骨.md "wikilink")、[蹠骨](../Page/蹠骨.md "wikilink")、[人字形骨](../Page/人字形骨.md "wikilink")、以及[肋骨](../Page/肋骨.md "wikilink")。[尾椎的近側中段](../Page/尾椎.md "wikilink")，前部有微凸，後部表面平坦。中段尾椎的神經棘向前傾斜，椎體前後端平坦\[1\]。

毒癮龍是以發現地為名，*venenum*在[拉丁語裡意為](../Page/拉丁語.md "wikilink")「毒物」，因為Poison
Strip的直譯即為「毒物地帶」。而*sauros*在[希臘文裡意為](../Page/希臘文.md "wikilink")「蜥蜴」。種名則來自化石的發現者Anthony
DiCroce的姓氏。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:腕龍科](../Category/腕龍科.md "wikilink")

1.  Tidwell, V., Carpenter, K. & Meyer, S. 2001. New Titanosauriform
    (Sauropoda) from the Poison Strip Member of the Cedar Mountain
    Formation (Lower Cretaceous), Utah. In: Mesozoic Vertebrate Life. D.
    H. Tanke & K. Carpenter (eds.). Indiana University Press, Eds. D.H.
    Tanke & K. Carpenter. Indiana University Press. 139-165.