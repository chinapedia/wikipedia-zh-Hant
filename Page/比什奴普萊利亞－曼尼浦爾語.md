**比什奴普萊利亞-曼尼浦爾語**（ইমার ঠার/বিষ্ণুপ্রিয়া
মণিপুরী），是[印度-雅利安语支的語言](../Page/印度-雅利安语支.md "wikilink")\[1\]，使用於[印度東北部](../Page/印度.md "wikilink")，[緬甸](../Page/緬甸.md "wikilink")，[孟加拉一帶](../Page/孟加拉.md "wikilink")。
約有450,000人使用此語言。

## 參考資料

## 参见

  - [印度国家语言列表](../Page/印度国家语言列表.md "wikilink")

## 延伸閱讀

  - *Vasatatvar Ruprekha*/ Dr. K. P. Sinha, Silchar, 1977
  - *Manipuri jaatisotta bitorko: ekti niropekkho paath* /Ashim Kumar
    Singha, Sylhet,2001
  - G. K. Ghose / Tribals and Their Culture in Manipur and Nagaland,
    1982
  - Raj Mohan Nath / The Background of Assamese Culture, 2nd edn, 1978
  - Sir G. A. Grierson / Linguistic Survey of India, Vol-5,1903
  - Dr. K. P. Sinha / An Etymological Dictionary of Bishnupriya
    Manipuri, 1982
  - Dr. M. Kirti Singh / Religious developments in Manipur in the 18th
    and 19th centuuy, Imphal, 1980
  - Singha, Jagat Mohan & Singha, Birendra / The Bishnupriya Manipuris &
    Their Language, silchar, 1976
  - Pocha Ojha -Amar manipur historical version in 1995

## 外部連結

  - [Bishnupriya Manipuri: A brief
    introduction](http://www.languageinindia.com/dec2002/bishnupriya.html)
  - [Details on Bishnupriya Manipuri
    Language](http://manipuri.freeservers.com/)
  - [The Manipuri Blog](http://manipuriblog.blogspot.com)
  - [Archive of Bishnupriya Manipuri
    Literature](http://Manipuri.htmlplanet.com)
  - [Bishnupriya Manipuri forum](http://www.manipuri.org/discus)
  - [Bishnupriya Manipuri society](http://www.bishnupriya-manipuri.org)
  - [A Weblog on Bishnupriya Manipuri people and
    culture](http://manipuri.wordpress.com)
  - [BishnupriyaManipuri Online
    Blog](http://www.bishnupriyamanipuri.blogspot.com)

[category:印度語言](../Page/category:印度語言.md "wikilink")
[category:印度-雅利安语支](../Page/category:印度-雅利安语支.md "wikilink")
[category:緬甸語言](../Page/category:緬甸語言.md "wikilink")

[Category:孟加拉国语言](../Category/孟加拉国语言.md "wikilink")

1.