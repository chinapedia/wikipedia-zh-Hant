**比克爾斯棘龍屬**（[學名](../Page/學名.md "wikilink")：*Becklespinax*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[肉食龍下目](../Page/肉食龍下目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，牠的[模式標本是三個](../Page/模式標本.md "wikilink")[脊椎與](../Page/脊椎.md "wikilink")[神經棘](../Page/神經棘.md "wikilink")，在1884年發現於[英格蘭](../Page/英格蘭.md "wikilink")[薩西克斯](../Page/薩西克斯.md "wikilink")。這些化石被認為是屬於[下白堊紀時代](../Page/白堊紀.md "wikilink")。比克爾斯棘龙可能有8公尺長及1.5公噸重。
[London_-_Crystal_Palace_-_Victorian_Dinosaurs_1.jpg](https://zh.wikipedia.org/wiki/File:London_-_Crystal_Palace_-_Victorian_Dinosaurs_1.jpg "fig:London_-_Crystal_Palace_-_Victorian_Dinosaurs_1.jpg")[水晶宮公園的早期](../Page/水晶宮公園.md "wikilink")[斑龍模型](../Page/斑龍.md "wikilink")，高背部是參考比克爾斯棘龍的化石\]\]
比克爾斯棘龙的分類歷史很複雜。牠的[脊椎原先被假設是與一些](../Page/脊椎.md "wikilink")[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的](../Page/恐龍.md "wikilink")[牙齒有關](../Page/牙齒.md "wikilink")，而被歸類於[斑龍超科的](../Page/斑龍超科.md "wikilink")[頂棘龍](../Page/頂棘龍.md "wikilink")。之後發現這個歸類是沒有理據的，而這些脊椎被[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S. Paul）歸類於[高棘龍的新種](../Page/高棘龍.md "wikilink")，長棘高棘龍（*Acrocanthosaurus
altispinax*）。在1991年，牠被建立為新屬，模式種是**長棘比克爾斯棘龍**（*B.
altispinax*），屬名是為紀念其發現者[塞繆爾·比克爾斯](../Page/塞繆爾·比克爾斯.md "wikilink")（Samuel
h. beckle）。比克爾斯棘龙當時被歸類於新建立的[中華盜龍科](../Page/中華盜龍科.md "wikilink")。

在19世紀末期完成的[倫敦](../Page/倫敦.md "wikilink")[水晶宮公園的早期](../Page/水晶宮公園.md "wikilink")[斑龍模型](../Page/斑龍.md "wikilink")，高背部是參考比克爾斯棘龍的化石\[1\]。

在比克爾斯棘龍的三個已發現背椎，神經棘末端1/3段落有不規則的表面。前兩個神經棘筆直，最接近神經棘的高度是其他兩個的約2/3高度\[2\]。比克爾斯棘龍的第三節神經棘較短，原先被認為可能是因為斷裂造成的。但是，近年發現的[昆卡獵龍的最前兩節臀部脊椎較高](../Page/昆卡獵龍.md "wikilink")，顯示比克爾斯棘龍的神經棘可能也是相同狀況。

## 參考資料

<div class="references-small">

<references>

</references>

  -
  -

</div>

## 外部連結

  - [*Becklespinax*](http://dml.cmnh.org/1997Feb/msg00333.html) Dinosaur
    Mailing List Archive, 18 Feb 1997 15:42, Cleveland Museum of Natural
    History

[Category:肉食龍下目](../Category/肉食龍下目.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")

1.
2.  Molnar, R. E., 2001, Theropod paleopathology: a literature survey:
    In: Mesozoic Vertebrate Life, edited by Tanke, D. H., and Carpenter,
    K., Indiana University Press, p. 337-363.