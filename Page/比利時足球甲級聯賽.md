{{ 足球聯賽資料 | name = 比利時甲級足球聯賽 | another_name = Belgian Pro League |
image = Jlproleague logo left.jpg | pixels = 200px | country =  | confed
= [UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")）
| first = 1895年 | teams = 16 隊 | relegation =
[比利時足球乙級聯賽](../Page/比利時足球乙級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup = [比利時盃](../Page/比利時盃.md "wikilink")、
[比利時超級盃](../Page/比利時超級盃.md "wikilink") | international_cup =
[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")、
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2017–18 | champions =
[布魯日](../Page/皇家布魯日足球會.md "wikilink") | most_successful_club =
[安德列治](../Page/皇家安德列治體育會.md "wikilink") | no_of_titles = 34 次 |
website = [比甲官網](http://www.sport.be/jupilerproleague/) | current = }}
**比利時甲級足球聯賽**（**The Belgian First Division**，或加冠名稱**Jupiler Pro
League**）是比利時最高級別的職業足球聯賽，簡稱「比甲」，聯賽於1895年成立，是世界上歷史最悠久的職業足球聯賽。[首都球會](../Page/首都球會.md "wikilink")[安德列治是聯賽歷史上最成功的球會](../Page/皇家安德列治體育會.md "wikilink")，曾經
34 次奪得聯賽冠軍，其次是[布魯日](../Page/皇家布魯日足球會.md "wikilink")（15
次），[聖基萊錫](../Page/聖基萊錫皇家聯盟.md "wikilink")（11
次）和[標準列治](../Page/皇家標準列治.md "wikilink")（11 次）。

## 參賽球隊

2018–19年比利時足球甲級聯賽參賽隊伍共有 16 支。

| 中文名稱                                      | 英文名稱                           | 所在城市                                   | 上季成績     |
| ----------------------------------------- | ------------------------------ | -------------------------------------- | -------- |
| [布魯日](../Page/皇家布魯日足球會.md "wikilink")     | Club Brugge K.V.               | [布魯日](../Page/布魯日.md "wikilink")       | 第 1 位    |
| [標準列治](../Page/皇家標準列治.md "wikilink")      | Standard Liège                 | [列日](../Page/列日.md "wikilink")         | 第 2 位    |
| [安德列治](../Page/皇家安德列治體育會.md "wikilink")   | R.S.C. Anderlecht              | [布魯塞爾](../Page/布魯塞爾.md "wikilink")     | 第 3 位    |
| [真特](../Page/皇家真特體育會.md "wikilink")       | K.A.A. Gent                    | [根特](../Page/根特.md "wikilink")         | 第 4 位    |
| [亨克](../Page/皇家亨克體育會.md "wikilink")       | K.R.C. Genk                    | [亨克](../Page/亨克.md "wikilink")         | 第 5 位    |
| [查內爾](../Page/皇家沙勒羅瓦體育俱樂部.md "wikilink")  | R. Charleroi S.C.              | [沙勒羅瓦](../Page/沙勒羅瓦.md "wikilink")     | 第 6 位    |
| [哥積克](../Page/皇家哥積克足球會.md "wikilink")     | K.V. Kortrijk                  | [科特賴克](../Page/科特賴克.md "wikilink")     | 第 7 位    |
| [安特衛普](../Page/皇家安特衛普足球俱樂部.md "wikilink") | Royal Antwerp F.C.             | [安特衛普](../Page/安特衛普.md "wikilink")     | 第 8 位    |
| [華雷根](../Page/華雷根祖爾特體育會.md "wikilink")    | S.V. Zulte Waregem             | [索利戈爾斯克](../Page/索利戈爾斯克.md "wikilink") | 第 9 位    |
| [聖圖爾登](../Page/皇家聖圖爾登足球會.md "wikilink")   | Sint-Truidense V.V.            | [聖特雷登](../Page/聖特雷登.md "wikilink")     | 第 10 位   |
| [奧斯坦德](../Page/皇家奧斯坦德足球會.md "wikilink")   | K.V. Oostende                  | [奧斯滕德](../Page/奧斯滕德.md "wikilink")     | 第 11 位   |
| [華斯蘭比華倫](../Page/華斯蘭比華倫.md "wikilink")    | Waasland-Beveren               | [貝弗倫](../Page/貝弗倫.md "wikilink")       | 第 12 位   |
| [洛格倫](../Page/東佛蘭德洛克倫體育俱樂部.md "wikilink") | K.S.C. Lokeren Oost-Vlaanderen | [洛克倫](../Page/洛克倫.md "wikilink")       | 第 13 位   |
| [梅斯干](../Page/皇家梅斯干精英隊.md "wikilink")     | Royal Excel Mouscron           | [穆斯克龍](../Page/穆斯克龍.md "wikilink")     | 第 14 位   |
| [奧伊本](../Page/皇家奧伊本體育會.md "wikilink")     | K.A.S. Eupen                   | [奧伊彭](../Page/奧伊彭.md "wikilink")       | 第 15 位   |
| [布魯日舒高](../Page/布魯日舒高皇家體育會.md "wikilink") | Cercle Brugge K.S.V.           | [布魯日](../Page/布魯日.md "wikilink")       | 乙組，附加賽勝方 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 奪冠次數

### 按球會

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/皇家安德列治體育會.md" title="wikilink">安德列治</a></p></td>
<td><center>
<p>34</p></td>
<td><p>1946–47, 1948–49, 1949–50, 1950–51, 1953–54, 1954–55, 1955–56, 1958–59, 1961–62, 1963–64,<br />
1964–65, 1965–66, 1966–67, 1967–68, 1971–72, 1973–74, 1980–81, 1984–85, 1985–86, 1986–87,<br />
1990–91, 1992–93, 1993–94, 1994–95, 1999–00, 2000–01, 2003–04, 2005–06, 2006–07, 2009–10,<br />
2011–12, 2012–13, 2013–14, 2016–17</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家布魯日足球會.md" title="wikilink">布魯日</a></p></td>
<td><center>
<p>15</p></td>
<td><p>1919–20, 1972–73, 1975–76, 1976–77, 1977–78, 1979–80, 1987–88, 1989–90, 1991–92, 1995–96,<br />
1997–98, 2002–03, 2004–05, 2015–16, 2017–18</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聖基萊錫皇家聯盟.md" title="wikilink">聖基萊錫</a></p></td>
<td><center>
<p>11</p></td>
<td><p>1903–04, 1904–05, 1905–06, 1906–07, 1908–09, 1909–10, 1912–13, 1922–23, 1932–33, 1933–34,<br />
1934–35</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家標準列治.md" title="wikilink">標準列治</a></p></td>
<td><center>
<p>10</p></td>
<td><p>1957–58, 1960–61, 1962–63, 1968–69, 1969–70, 1970–71, 1981–82, 1982–83, 2007–08, 2008–09</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皇家比斯查足球及田徑會.md" title="wikilink">皇家比斯查</a></p></td>
<td><center>
<p>7</p></td>
<td><p>1921–22, 1923–24, 1924–25, 1925–26, 1927–28, 1937–38, 1938–39</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KFC霍迪恩南–范瑞維恩科爾.md" title="wikilink">華域雲基</a></p></td>
<td><center>
<p>6</p></td>
<td><p>1896–97, 1899–00, 1900–01, 1901–02, 1902–03, 1907–08</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列治皇家足球會.md" title="wikilink">列治</a></p></td>
<td><center>
<p>5</p></td>
<td><p>1895–96, 1897–98, 1898–99, 1951–52, 1952–53</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫倫比克競賽勇氣會.md" title="wikilink">布魯塞爾勇氣會</a></p></td>
<td><center>
<p>5</p></td>
<td><p>1911–12, 1913–14, 1920–21, 1935–36, 1936–37</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皇家安特衛普足球俱樂部.md" title="wikilink">安特衛普</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1928–29, 1930–31, 1943–44, 1956–57</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家梅赫倫足球會.md" title="wikilink">梅赫倫</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1942–43, 1945–46, 1947–48, 1988–89</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/利爾斯SK.md" title="wikilink">利亞斯</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1931–32, 1941–42, 1959–60, 1996–97</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家亨克體育會.md" title="wikilink">亨克</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1998–99, 2001–02, 2010–11</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布魯日舒高皇家體育會.md" title="wikilink">布魯日舒高</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1910–11, 1926–27, 1929–30</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貝弗倫皇家體育會.md" title="wikilink">比華倫</a></p></td>
<td><center>
<p>2</p></td>
<td><p>1978–79, 1983–84</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫倫比克競賽會.md" title="wikilink">莫倫比克</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1974–75</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家真特體育會.md" title="wikilink">真特</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2014–15</p></td>
</tr>
</tbody>
</table>

### 按城市

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>冠軍次數</p></th>
<th><p>球會（冠軍次數）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/布魯塞爾.md" title="wikilink">布魯塞爾</a></p></td>
<td><center>
<p>57</p></td>
<td><p><a href="../Page/皇家安德列治體育會.md" title="wikilink">安德列治</a>（34）、<a href="../Page/聖基萊錫皇家聯盟.md" title="wikilink">聖基萊錫</a>（11）、<a href="../Page/KFC霍迪恩南-范瑞维恩科尔.md" title="wikilink">華域雲基</a>（6）、<a href="../Page/莫倫比克競賽勇氣會.md" title="wikilink">布魯塞爾勇氣會</a>（5）、<a href="../Page/莫倫比克競賽會.md" title="wikilink">莫倫比克</a>（1）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布魯日.md" title="wikilink">布魯日</a></p></td>
<td><center>
<p>18</p></td>
<td><p><a href="../Page/皇家布魯日足球會.md" title="wikilink">布魯日</a>（15）、<a href="../Page/布魯日舒高皇家體育會.md" title="wikilink">布魯日舒高</a>（3）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列日.md" title="wikilink">列日</a></p></td>
<td><center>
<p>15</p></td>
<td><p><a href="../Page/皇家標準列治.md" title="wikilink">標準列治</a>（10）、 <a href="../Page/列治皇家足球會.md" title="wikilink">列治</a>（5）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安特衛普.md" title="wikilink">安特衛普</a></p></td>
<td><center>
<p>11</p></td>
<td><p><a href="../Page/皇家比斯查足球及田徑會.md" title="wikilink">皇家比斯查</a>（7）、<a href="../Page/皇家安特衛普足球俱樂部.md" title="wikilink">安特衛普</a>（4）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梅赫倫.md" title="wikilink">梅赫倫</a></p></td>
<td><center>
<p>4</p></td>
<td><p><a href="../Page/皇家梅赫倫足球會.md" title="wikilink">梅赫倫</a>（4）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/利爾.md" title="wikilink">利爾</a></p></td>
<td><center>
<p>4</p></td>
<td><p><a href="../Page/利爾斯SK.md" title="wikilink">利亞斯</a>（4）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亨克.md" title="wikilink">亨克</a></p></td>
<td><center>
<p>3</p></td>
<td><p><a href="../Page/皇家亨克體育會.md" title="wikilink">亨克</a>（3）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貝弗倫.md" title="wikilink">貝弗倫</a></p></td>
<td><center>
<p>2</p></td>
<td><p><a href="../Page/貝弗倫皇家體育會.md" title="wikilink">比華倫</a>（2）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/根特.md" title="wikilink">根特</a></p></td>
<td><center>
<p>1</p></td>
<td><p><a href="../Page/皇家真特體育會.md" title="wikilink">真特</a>（1）</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  -    [The Belgian Football Association official
    website](http://www.footbel.be)

  - [Sport website](http://www.sport.be/fr/jupilerleague) - On the
    Jupiler League

  - [Pluto
    website](https://web.archive.org/web/20090214155218/http://users.skynet.be/pluto/Textfifa/soccer.html)
    - Belgian football history

  - [RSSSF archive](http://www.rsssf.com/tablesb/belghist.html) - All
    time tables

  - [Football
    results](http://www.resultsfromfootball.com/jupilerleague-from-belgium.html)
    - Belgium football statistics

{{-}}

[Category:比利時足球](../Category/比利時足球.md "wikilink")
[欧](../Category/國家頂級足球聯賽.md "wikilink")
[Belgium](../Category/歐洲各國足球聯賽.md "wikilink")

1.