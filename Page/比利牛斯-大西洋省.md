[Pyrénées-Atlantiques-Position.png](https://zh.wikipedia.org/wiki/File:Pyrénées-Atlantiques-Position.png "fig:Pyrénées-Atlantiques-Position.png")
**比利牛斯-大西洋省**（）是[法國](../Page/法國.md "wikilink")[新阿基坦大区所轄的](../Page/新阿基坦大區.md "wikilink")[省份](../Page/省_\(法国\).md "wikilink")，濱[大西洋](../Page/大西洋.md "wikilink")。該省編號為64。

## 另見

  - [比利牛斯-大西洋省市镇列表](../Page/比利牛斯-大西洋省市镇列表.md "wikilink")

<!-- end list -->

  - [General Council
    website](https://web.archive.org/web/19981201044410/http://www.cg64.fr/)

  - [Archives of the Pyrenees-Atlantiques department
    website](http://archives.le64.fr/)

  - [Photography Panoramics 360°
    website](http://www.pyrenees360.fr/index.php?/category/panoramas-des-pyrenees-atlantiques)

  - [Prefecture official
    website](http://www.pyrenees-atlantiques.pref.gouv.fr/)

  - [Pyrenees-Atlantiques Monuments, Villages, Walks and
    Attractions](https://web.archive.org/web/20070405114209/http://www.pyreneesguide.com/cats.asp?cID=17)

  - [Information on living, working and visiting Pyrenees
    Atlantiques](http://www.southaquitaine.angloinfo.com/)

[Category:法国省份](../Category/法国省份.md "wikilink")
[Category:巴斯克地區](../Category/巴斯克地區.md "wikilink")
[P](../Category/新阿基坦大區.md "wikilink")