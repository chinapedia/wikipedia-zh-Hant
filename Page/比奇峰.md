**比奇峰**（Bietschhorn，3934米），位於[瑞士](../Page/瑞士.md "wikilink")，屬[阿爾卑斯山脈部份的](../Page/阿爾卑斯山脈.md "wikilink")[山峰](../Page/山峰.md "wikilink")。比奇峰是位於lötschental山谷的南部，及Bietschtal谷和Baltschiedertal山谷的北端。

比奇峰於1859年8月13日由Leslie Stephen，及導遊Anton Siegen，Johann Siegen，Joseph
Ebener，首次登頂。

2001年[聯合國教科文組織把](../Page/聯合國教科文組織.md "wikilink")[少女峰](../Page/少女峰.md "wikilink")—[阿萊奇冰川](../Page/阿萊奇冰川.md "wikilink")—比奇峰綜合山區列爲[世界自然遺産](../Page/世界自然遺産.md "wikilink")。

## 參看

  - [阿爾卑斯山脈](../Page/阿爾卑斯山脈.md "wikilink")
  - [阿爾卑斯山脈山峰列表](../Page/阿爾卑斯山脈山峰列表.md "wikilink")
  - [瑞士地理](../Page/瑞士地理.md "wikilink")

## 外部链接

  - [瑞士國家旅遊局官方中文網站](http://www.myswitzerland.com.hk/)
  - [Climbing the
    Bietschhorn](https://web.archive.org/web/20070310211211/http://www.aletschhorn.de/berge/berner/bietschhorn.html)

  - [UNESCO World Heritage](http://whc.unesco.org/en/list/1037/)

[Category:瑞士山峰](../Category/瑞士山峰.md "wikilink")
[J](../Category/瑞士世界遺產.md "wikilink")
[Category:阿爾卑斯山脈](../Category/阿爾卑斯山脈.md "wikilink")