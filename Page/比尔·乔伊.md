**威廉·纳尔逊·乔伊**（，），暱称**比尔·乔伊**（），生於[美国](../Page/美国.md "wikilink")[密西根州](../Page/密西根州.md "wikilink")[法明頓山](../Page/法明頓山_\(密歇根州\).md "wikilink")，[计算机科学家與程式設計師](../Page/计算机.md "wikilink")，是[BSD](../Page/BSD.md "wikilink")[作業系統的主要設計者](../Page/作業系統.md "wikilink")，曾創作了包括[vi](../Page/vi.md "wikilink")、[C
Shell等軟體](../Page/C_Shell.md "wikilink")。与[維諾德·柯斯拉](../Page/維諾德·柯斯拉.md "wikilink")、[史考特·麥克里尼和](../Page/史考特·麥克里尼.md "wikilink")[安迪·貝托爾斯海姆一起创立了](../Page/安迪·貝托爾斯海姆.md "wikilink")[昇陽電腦](../Page/昇陽電腦.md "wikilink")（Sun
Microsystems），并作为首席科学家直到2003年。

## 早期经历

乔伊的童年是在[密歇根州的乡村长大的](../Page/密歇根州.md "wikilink")，在[密歇根大学获得](../Page/密歇根大学.md "wikilink")[电气工程学士学位之后](../Page/电气工程.md "wikilink")，于1979年在[加州大学伯克利分校获得电气工程与计算机科学硕士学位](../Page/加州大学伯克利分校.md "wikilink")。学生期间，他开发了BSD操作系统。其他人以BSD为基础发展出了很多现代版本的BSD，最著名的有[FreeBSD](../Page/FreeBSD.md "wikilink")、[OpenBSD和](../Page/OpenBSD.md "wikilink")[NetBSD](../Page/NetBSD.md "wikilink")，苹果电脑的[Mac
OS
X操作系统也在很大程度上基于](../Page/Mac_OS_X.md "wikilink")[BSD](../Page/BSD.md "wikilink")。1986年，乔伊因他在BSD操作系统中所做的工作获得了Grace
Murray Hopper奖。

除了BSD之外，他引人注目的贡献还包括[TCP/IP](../Page/TCP/IP.md "wikilink")、[vi](../Page/vi.md "wikilink")、[NFS和](../Page/NFS.md "wikilink")[C
shell](../Page/C_shell.md "wikilink")，如今这些软件都已经广泛的使用在[Solaris](../Page/Solaris.md "wikilink")、[BSD](../Page/BSD.md "wikilink")、[GNU/Linux等操作系统中](../Page/GNU/Linux.md "wikilink")，而且开放源代码给其他人无偿使用、改进，为[自由软件的发展作出了极大的贡献](../Page/自由软件.md "wikilink")。

## Sun

1982年，Joy作为联合创始人和首席科学家参与了Sun微系统公司的成立，设计了[Sparc微处理器](../Page/Sparc.md "wikilink")，并将之前自己领导开发的BSD继续发展成为[Solaris操作系统](../Page/Solaris.md "wikilink")。另外，他还是[Java和](../Page/Java.md "wikilink")[Jini的主要作者之一](../Page/Jini.md "wikilink")。

## 离开Sun

2003年9月9日，乔伊离开Sun公司，Sun发言人除了宣布Joy辞职的消息外，不愿意发表其他评论。从一些迹象看来，他很关注[机器人](../Page/机器人.md "wikilink")、纳米、[基因工程等可能会改变全人类未来生存发展的技术](../Page/基因工程.md "wikilink")；更加关注科技带来的道德问题：如何不让科技成为一个国家、政府、集体、甚至个人做恶的工具？

## 参考文献

## 外部链接

  - [Interview with Bill
    Joy](https://web.archive.org/web/20120210184000/http://web.cecs.pdx.edu/~kirkenda/joy84.html)
  - [Why the future doesn't need
    us](http://www.wired.com/wired/archive/8.04/joy.html)
  - [BSD简史](https://web.archive.org/web/20100106074627/http://www.4oa.com/Article/html/5/379/381/2005/8276.html)

{{-}}

[Category:TED演讲人](../Category/TED演讲人.md "wikilink")
[Category:美国程序员](../Category/美国程序员.md "wikilink")
[Category:密西根大學校友](../Category/密西根大學校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:美国计算机科学家](../Category/美国计算机科学家.md "wikilink")
[Category:昇陽電腦人物](../Category/昇陽電腦人物.md "wikilink")
[Category:Unix人物](../Category/Unix人物.md "wikilink")