**比拉馬體育會**（**S.C.
Beira-Mar**）是一間位於[阿威羅的](../Page/阿威羅.md "wikilink")[葡萄牙足球球會](../Page/葡萄牙.md "wikilink")，在2005/06球季奪得甲組冠軍，升上超級聯賽。比拉馬的球場叫[阿韋羅市政球場](../Page/阿韋羅市政球場.md "wikilink")，是為了[2004年歐洲國家杯而興建的球場](../Page/2004年歐洲國家杯.md "wikilink")。

比拉馬最著名的球員叫[尤西比奧](../Page/尤西比奧.md "wikilink")（Eusébio），他在1976/77球季期間効力了一季。而另一位著名球員安東尼奧·蘇沙（António
Sousa）和尤西比奧同樣是[葡萄牙國家足球隊的成員](../Page/葡萄牙國家足球隊.md "wikilink")。

## 體育部

比拉馬於1922年1月1日在[阿韋羅成立](../Page/阿韋羅.md "wikilink")，當時他們最著名的運動有[室內足球](../Page/室內足球.md "wikilink")、[籃球和](../Page/籃球.md "wikilink")[拳擊部](../Page/拳擊.md "wikilink")。而足球部當時也正在成長，當時足球部已經經常在頂級聯賽角逐。

### 足球

比拉馬表現最佳的球季是在1990/91球季，而最大的成就則在1998/99球季贏得[葡萄牙杯冠軍](../Page/葡萄牙杯.md "wikilink")，是球會歷史性的第一次。而1990/91球季他們也打入了總決賽。他們也三次贏得乙組聯賽冠軍，分別在1960/61球季、1964/65球季和1970/71球季。甲組聯賽的冠軍也在2005/06球季贏過一次。他們曾於1999/2000球季參與[歐洲足協杯賽事](../Page/歐洲足協杯.md "wikilink")，並曾以2-1擊敗[荷蘭的](../Page/荷蘭.md "wikilink")[維迪斯](../Page/維迪斯.md "wikilink")。

#### 尤西比奧日

六十年代，[尤西比奧是](../Page/尤西比奧.md "wikilink")[葡萄牙和](../Page/葡萄牙.md "wikilink")[賓菲加的標誌](../Page/賓菲加.md "wikilink")。但到了1976年，很多比拉馬球迷懷疑他在十年後是否還有當年的風采。後來他在12場比賽中入了3球，以他的年紀來說已經是相當不錯了。

#### 葡萄牙杯

沒有人會懷疑比拉馬最出眾的葡萄牙杯歷史是在1998/99球季，他們由António
Sousa執教，他是比拉馬前球員，也曾効力[波圖和國家隊](../Page/波圖.md "wikilink")。在對Campomaiorense的決賽賽事中，António
Sousa的兒子Ricardo
Sousa射入了唯一的入球。不過遺憾的是，這一季他們同時要降至甲組作賽。這是球會第二次打入決賽，第一次是在1990/91球季，當時輸給[波圖](../Page/波圖.md "wikilink")。

#### 阿韋羅市政球場

[Nt-Aveiro-Estadio_Beira-Mar.jpg](https://zh.wikipedia.org/wiki/File:Nt-Aveiro-Estadio_Beira-Mar.jpg "fig:Nt-Aveiro-Estadio_Beira-Mar.jpg")
**阿韋羅市政球場**是[阿韋羅的一個足球球場](../Page/阿韋羅.md "wikilink")，它是建築師Tomás
Taveira設計用來於[2004年歐洲國家杯比賽使用](../Page/2004年歐洲國家杯.md "wikilink")。球場色彩十分鮮艷，就像節日一樣。球場能容納30,498人，是比拉馬的主場。因為球場熱情的設計，很多球迷稱這裡為*Circus*。問題是球場由誰真正擁有？是由比拉馬會方還是市政府？這個問題雙方一直僵持不下，也令雙方的關係變得緊張，影響了球會的收入。

#### 马里奥·贾德尔的回歸

马里奥·贾德尔的回歸是2006年夏天中最大的足球新聞。他回到葡萄牙，選擇効力比拉馬，令球迷對他十分期待。因為這位[巴西球員在歐洲曾贏得了金靴獎](../Page/巴西.md "wikilink")(頒給歐洲神射手的一個獎項)，所以他是令球會避免降班的希望。他在第一場比賽中有入球，令球會以2-2和[艾維斯打成平手](../Page/艾維斯.md "wikilink")。

#### 球迷

比拉馬的球迷稱為Paixadores (*漁民*)，也被稱為Auri-negros
(*黃黑色*)，是因為他們的熱情和球會的顏色。比拉馬在全球有很多球迷，不過都難以和阿韋羅相比。很多比拉馬的球迷都明白球賽期門他們的瘋狂，例如拋煙花、唱歌和尖叫，都能令球會注入強心針。

#### 著名球員

  - [尤西比奧](../Page/尤西比奧.md "wikilink")

  - Antonio Sousa

  - Andy Marriott

  - Todor Angelov

  - Mariano Fernández

  - [安托林·阿爾卡拉斯](../Page/安托林·阿爾卡拉斯.md "wikilink")

  - Marian Zemen

  - Mário Jardel

  - Kingsley Sunny Ekeh

  - Clyde Wijnhard

  - Faye Fary

  -  Fernando Aguiar

  - Jorge Leitao

  - Pavel Srníček

  - [张呈栋](../Page/张呈栋.md "wikilink")

  - [王刚（足球运动员）](../Page/王刚.md "wikilink")

## 外部連結

  - [官方網站](http://www.beiramar.pt)
  - [新聞網站](https://web.archive.org/web/20170518052821/http://portuguesefutebol.com/)

[B](../Category/葡萄牙足球俱樂部.md "wikilink")
[Category:1922年建立的足球俱樂部](../Category/1922年建立的足球俱樂部.md "wikilink")