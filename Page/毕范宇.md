**毕范宇**（，），[美南长老会差会](../Page/美南长老会差会.md "wikilink")[传教士](../Page/传教士.md "wikilink")，[美国](../Page/美国.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")，[金陵神学院](../Page/金陵神学院.md "wikilink")[教授](../Page/教授.md "wikilink")，[上海国际礼拜堂](../Page/上海国际礼拜堂.md "wikilink")[牧师](../Page/牧师.md "wikilink")。

1895年2月25日，毕范宇出生于中国[浙江省](../Page/浙江省.md "wikilink")[嘉兴的](../Page/嘉兴.md "wikilink")[美南长老会差会传教士家庭](../Page/美南长老会差会.md "wikilink")。就读于美国Davidson
College、[哥伦比亚大学以及](../Page/哥伦比亚大学.md "wikilink")[耶鲁神学院](../Page/耶鲁神学院.md "wikilink")\[1\]。1922年获耶鲁神学院学士学位.\[2\]，并按立为长老会牧师。1923年，毕范宇回到中国，任教于南京[金陵神学院](../Page/金陵神学院.md "wikilink")，任教授及乡村教会科科长，其间从1926年到1930年回美国攻读[耶鲁大学](../Page/耶鲁大学.md "wikilink")[神学](../Page/神学.md "wikilink")[博士](../Page/博士.md "wikilink")。1929年将[孙中山](../Page/孙中山.md "wikilink")《[三民主义](../Page/三民主义.md "wikilink")》翻译为英文，由[商务印书馆出版](../Page/商务印书馆.md "wikilink")。毕范宇投身于乡村服务项目，在[江宁县](../Page/江宁县.md "wikilink")[淳化镇设立实习农场](../Page/淳化镇.md "wikilink")\[3\]，1934年又在江西[黎川农场实验区](../Page/黎川.md "wikilink")。

[抗日战争期间](../Page/抗日战争.md "wikilink")，毕范宇随金陵神学院内迁成都，曾做过[蒋介石和国民政府的顾问](../Page/蒋介石.md "wikilink")，并赴美为中国政府寻求支持\[4\]。抗战结束后，毕范宇出任[中華基督教會全國總會干事](../Page/中華基督教會全國總會.md "wikilink")、代理[长老会总干事](../Page/长老会.md "wikilink")，并担任[上海国际礼拜堂](../Page/上海国际礼拜堂.md "wikilink")[牧师](../Page/牧师.md "wikilink")。

1949年中共执政后，毕范宇留在上海。在1951年[控诉运动中](../Page/控诉运动.md "wikilink")，毕范宇因担任蒋政府的顾问而遭到控诉，被软禁2年之久。1953年被中华人民共和国政府[驱逐出境](../Page/驱逐出境.md "wikilink")，回到美国，曾担任美国南长老会大会主席、纽约宣教士研究图书馆主任.\[5\]以及纽约协和神学院教授。1974年1月，毕范宇在美国弗吉尼亚州莱克星屯去世，享年78岁。

## 著作

  - 《中国的乡村教会》（1948年）\[6\]
  - 《家》
  - 《金陵神学院史》

## 参考

  - G. Thompson Brown, "[Francis Wilson Price
    (1895-1974)](http://www.bdcconline.net/en/stories/p/price-frank-wilson.php),"
    *Biographical Dictionary of Chinese Christianity*, reprinted from
    Gerald H. Anderson, ed.*Biographical Dictionary of Christian
    Missions*, (New York: Macmillan, 1998)

  -
  - H. McKennie Goodpasture, "China in an American, Frank Wilson Price:
    A Bibliographical Essay;" *Journal of Presbyterian History* 49
    (1971): 353-364.

  - "Biographical Sketch," "Price, Frank (Francis) Wilson Collection,"
    George C. Marshall Foundation,
    [1](https://web.archive.org/web/20110206094515/http://marshallfoundation.org/library/documents/Price_Frank_Wilson.pdf)

[Category:上海国际礼拜堂牧师](../Category/上海国际礼拜堂牧师.md "wikilink")
[Category:在华基督教传教士](../Category/在华基督教传教士.md "wikilink")

1.  Brown, "Francis Wilson Price..."
2.  "Biographical Sketch," Frank (Francis) Wilson Price Collection,
    George Marshall Foundation
3.  John Fairbank *China Bound: A Fifty-year Memoir* (New York, Harper &
    Row, 1982, pp. 91-92
4.  Akio Tsuchida, "China’s 'Public Diplomacy' toward the United States
    before Pearl Harbor," *Journal of American-East Asian Relations*,
    17.1 (2009), 56-88.
5.  Brown, "Francis Wilson Price..."
6.   The original printing plates for the book were destroyed in
    Shanghai in 1940, but this revised version was published in 1948.