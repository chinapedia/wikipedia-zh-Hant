大江系**毛利氏**是日本氏族，原姓大江。是[鎌倉時代](../Page/鎌倉時代.md "wikilink")[大江廣元四男](../Page/大江廣元.md "wikilink")[大江季光的後代](../Page/大江季光.md "wikilink")。最初在[越後國生活](../Page/越後國.md "wikilink")。後來遷往至[安藝國](../Page/安藝國.md "wikilink")。從國人成為了[大名](../Page/大名_\(稱謂\).md "wikilink")，最興盛的時代曾統領達七國。在[關原之戰後](../Page/關原之戰.md "wikilink")，領地只剩下[長門國及](../Page/長門國.md "wikilink")[周防國兩國](../Page/周防國.md "wikilink")，成立[長州藩並維持到幕末](../Page/長州藩.md "wikilink")。[江戶時代末期](../Page/江戶時代.md "wikilink")，長州藩出現許多優秀的志士，是成就[明治維新的原動力](../Page/明治維新.md "wikilink")。

據說長州藩每年新年會的慣例，家臣會詢問藩主「今年倒幕時機如何？」藩主會回答「時機尚早」，此慣例持續至[德川幕府倒台為止](../Page/德川幕府.md "wikilink")。

## 主要當主

  - [毛利興元](../Page/毛利興元.md "wikilink")
  - [毛利元就](../Page/毛利元就.md "wikilink")
  - [毛利隆元](../Page/毛利隆元.md "wikilink")
  - [毛利輝元](../Page/毛利輝元.md "wikilink")

## 家臣團

### 一門眾

  - [宍戸隆家](../Page/宍戸隆家.md "wikilink")
  - [宍戸元續](../Page/宍戸元續.md "wikilink")
  - [吉川元春](../Page/吉川元春.md "wikilink")
  - [吉川元長](../Page/吉川元長.md "wikilink")
  - [吉川廣家](../Page/吉川廣家.md "wikilink")
  - [繁澤元氏](../Page/繁澤元氏.md "wikilink")
  - [小早川隆景](../Page/小早川隆景.md "wikilink")
  - [小早川秀包](../Page/小早川秀包.md "wikilink")
  - [二宮就辰](../Page/二宮就辰.md "wikilink")
  - [穗井田元清](../Page/穗井田元清.md "wikilink")
  - [椙杜元秋](../Page/椙杜元秋.md "wikilink")
  - [出羽元倶](../Page/出羽元倶.md "wikilink")
  - [天野元政](../Page/天野元政.md "wikilink")
  - [末次元康](../Page/末次元康.md "wikilink")

### 庶家眾

  - [志道廣良](../Page/志道廣良.md "wikilink")
  - [志道元保](../Page/志道元保.md "wikilink")
  - [口羽通良](../Page/口羽通良.md "wikilink")
  - [口羽春良](../Page/口羽春良.md "wikilink")
  - [福原廣俊](../Page/福原廣俊.md "wikilink")
  - [福原元俊](../Page/福原元俊.md "wikilink")
  - [福原貞俊](../Page/福原貞俊.md "wikilink")
  - [桂廣澄](../Page/桂廣澄.md "wikilink")
  - [桂元澄](../Page/桂元澄.md "wikilink")
  - [桂廣忠](../Page/桂廣忠.md "wikilink")
  - [坂元貞](../Page/坂元貞.md "wikilink")
  - [椙杜元緣](../Page/椙杜元緣.md "wikilink")
  - [兼重元鎮](../Page/兼重元鎮.md "wikilink")
  - [兼重元宣](../Page/兼重元宣.md "wikilink")
  - [兼重元續](../Page/兼重元續.md "wikilink")

### 國眾

  - [吉川經家](../Page/吉川經家.md "wikilink")
  - [吉川經安](../Page/吉川經安.md "wikilink")
  - [市川經好](../Page/市川經好.md "wikilink")
  - [熊谷信直](../Page/熊谷信直.md "wikilink")
  - [熊谷高直](../Page/熊谷高直.md "wikilink")
  - [熊谷元直](../Page/熊谷元直.md "wikilink")
  - [香川元景](../Page/香川元景.md "wikilink")
  - [清水宗治](../Page/清水宗治.md "wikilink")
  - [阿曾沼廣秀](../Page/阿曾沼廣秀.md "wikilink")
  - [阿曾沼元秀](../Page/阿曾沼元秀.md "wikilink")
  - [粟屋元通](../Page/粟屋元通.md "wikilink")
  - [堅田元慶](../Page/堅田元慶.md "wikilink")
  - [杉原盛重](../Page/杉原盛重.md "wikilink")
  - [乃美宗勝](../Page/乃美宗勝.md "wikilink")
  - [川村重吉](../Page/川村重吉.md "wikilink")
  - [三浦元忠](../Page/三浦元忠.md "wikilink")
  - [林就長](../Page/林就長.md "wikilink")
  - [平賀廣相](../Page/平賀廣相.md "wikilink")
  - [平賀元相](../Page/平賀元相.md "wikilink")
  - [益田藤兼](../Page/益田藤兼.md "wikilink")
  - [益田元祥](../Page/益田元祥.md "wikilink")
  - [國司元相](../Page/國司元相.md "wikilink")
  - [井上春忠](../Page/井上春忠.md "wikilink")
  - [井上元兼](../Page/井上元兼.md "wikilink")
  - [兒玉就忠](../Page/兒玉就忠.md "wikilink")
  - [兒玉就方](../Page/兒玉就方.md "wikilink")
  - [赤川就秀](../Page/赤川就秀.md "wikilink")
  - [赤川元秀](../Page/赤川元秀.md "wikilink")
  - [佐波隆秀](../Page/佐波隆秀.md "wikilink")
  - [天野隆重](../Page/天野隆重.md "wikilink")
  - [吉見正賴](../Page/吉見正賴.md "wikilink")
  - [吉見廣賴](../Page/吉見廣賴.md "wikilink")
  - [多賀山通續](../Page/多賀山通續.md "wikilink")
  - [山內隆通](../Page/山內隆通.md "wikilink")
  - [南方就正](../Page/南方就正.md "wikilink")
  - [中原善左衛門](../Page/中原善左衛門.md "wikilink")
  - [安國寺惠瓊](../Page/安國寺惠瓊.md "wikilink")
  - [村上武吉](../Page/村上武吉.md "wikilink")
  - [村上吉充](../Page/村上吉充.md "wikilink")

## 關連項目

  - [大江氏](../Page/大江氏.md "wikilink")
  - [寒河江氏](../Page/寒河江氏.md "wikilink")（[大江氏嫡流](../Page/大江氏.md "wikilink")）
  - [因幡毛利氏](../Page/因幡毛利氏.md "wikilink")
  - [山崎毛利氏](../Page/山崎毛利氏.md "wikilink")
  - [越後北條氏](../Page/越後北條氏.md "wikilink")
  - [長州藩](../Page/長州藩.md "wikilink")
  - [長州藩家臣團](../Page/長州藩家臣團.md "wikilink")
  - [毛利甲斐守邸跡](../Page/毛利甲斐守邸跡.md "wikilink")
  - [清神社](../Page/清神社.md "wikilink")
  - [毛利水軍](../Page/毛利水軍.md "wikilink")

## 參考資料

  - [武家家傳 毛利氏](http://www2.harimaya.com/sengoku/html/mouri.html)

[category:山口縣歷史](../Page/category:山口縣歷史.md "wikilink")

[Category:日本氏族](../Category/日本氏族.md "wikilink")
[毛利氏](../Category/毛利氏.md "wikilink")