**毛利衛**（）是[日本一位](../Page/日本.md "wikilink")[太空人與](../Page/太空人.md "wikilink")[日本宇宙航空研究開發機構職員](../Page/日本宇宙航空研究開發機構.md "wikilink")，出身於[北海道](../Page/北海道.md "wikilink")[余市町](../Page/余市町.md "wikilink")。後來畢業於[北海道立余市高等學校與](../Page/北海道立余市高等學校.md "wikilink")[北海道大學理學部](../Page/北海道大學.md "wikilink")。毛利衛在1985年被[美國太空總署選為候補太空人](../Page/美國太空總署.md "wikilink")，並於1992年執行過[STS-47任務](../Page/STS-47.md "wikilink")，成為日本首位進入太空的太空人。目前為[日本科學未來館館長](../Page/日本科學未來館.md "wikilink")。

## 參見

  - [比宇宙還遠的地方](../Page/比宇宙還遠的地方.md "wikilink")

## 外部連結

  - [NASA Biography](http://www.jsc.nasa.gov/Bios/htmlbios/mohri.html)
  - [Spacefacts biography of Mamoru
    Mohri](http://www.spacefacts.de/bios/international/english/mohri_mamoru.htm)

[Category:日本宇航员](../Category/日本宇航员.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:北海道大学校友](../Category/北海道大学校友.md "wikilink")
[Category:福林德斯大學校友](../Category/福林德斯大學校友.md "wikilink")
[Category:日本化學家](../Category/日本化學家.md "wikilink")