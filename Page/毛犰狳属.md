**毛犰狳屬**（學名：**）\[1\]是[有甲目的一個小型犰狳物種的](../Page/有甲目.md "wikilink")[屬](../Page/屬_\(生物\).md "wikilink")，是[倭犰狳科](../Page/倭犰狳科.md "wikilink")[六帶犰狳亞科之下的成員](../Page/六帶犰狳亞科.md "wikilink")。

## 分佈

本屬物種都是南美洲大陸的[特有種](../Page/特有種.md "wikilink")，見於南美洲中部及南部，包括[阿根廷](../Page/阿根廷.md "wikilink")、[波利維亞](../Page/波利維亞.md "wikilink")、[智利及](../Page/智利.md "wikilink")[巴拉圭](../Page/巴拉圭.md "wikilink")。

## 物種

包括以下三個物種：

|                                                                                                                                                                                                                    | 中文名稱                               | 學名                          | 分佈                                                                                                               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------- | --------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| [Chaetophractus_vellerosus3.jpg](https://zh.wikipedia.org/wiki/File:Chaetophractus_vellerosus3.jpg "fig:Chaetophractus_vellerosus3.jpg")                                                                          | [長毛犰狳](../Page/長毛犰狳.md "wikilink") | *Chaetophractus vellerosus* | 南美洲的中南部                                                                                                          |
| [Armadello_in_desert.JPG](https://zh.wikipedia.org/wiki/File:Armadello_in_desert.JPG "fig:Armadello_in_desert.JPG")                                                                                              | [披毛犰狳](../Page/披毛犰狳.md "wikilink") | *Chaetophractus villosus*   | The Pampas and Patagonia as far south as Santa Cruz, Argentina and Magallanes, Chile                             |
| [Chaetophractus_nationi,_Oruro,_Bolivia_-_20090824.jpg](https://zh.wikipedia.org/wiki/File:Chaetophractus_nationi,_Oruro,_Bolivia_-_20090824.jpg "fig:Chaetophractus_nationi,_Oruro,_Bolivia_-_20090824.jpg") | [密毛犰狳](../Page/密毛犰狳.md "wikilink") | *Chaetophractus nationi*    | Bolivia, in the region of the Puna; the departments of Oruro, La Paz, and Cochabamba, Bolivia and northern Chile |
|                                                                                                                                                                                                                    |                                    |                             |                                                                                                                  |

有研究認為[密毛犰狳很可能只是](../Page/密毛犰狳.md "wikilink")[長毛犰狳的指名亞種的異名](../Page/長毛犰狳.md "wikilink")，而整個[毛犰狳属亦很可能是一個](../Page/毛犰狳属.md "wikilink")[多系群](../Page/多系群.md "wikilink")\[2\]\[3\]。

## 參考文獻

## 外部連結

[Chaetophractus](../Category/倭犰狳科.md "wikilink")
[毛犰狳屬](../Category/毛犰狳屬.md "wikilink")

1.
2.
3.