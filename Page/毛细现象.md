[Candleburning.jpg](https://zh.wikipedia.org/wiki/File:Candleburning.jpg "fig:Candleburning.jpg")

**毛細現象**（又稱**毛細管作用**）是指[液體在細管狀物體內側](../Page/液體.md "wikilink")，由液體與物體之間的[附著力和因](../Page/黏附.md "wikilink")[內聚力而產生的](../Page/內聚力.md "wikilink")[表面張力組合而成](../Page/表面張力.md "wikilink")，令液體在不需施加外力的情況下，流向細管狀物體的現象，該現象甚至令液體克服[地心引力而上升](../Page/地心引力.md "wikilink")。[植物](../Page/植物.md "wikilink")[根部吸收的水分能夠經由](../Page/根.md "wikilink")[莖內](../Page/莖.md "wikilink")[維管束上升](../Page/維管束.md "wikilink")，即是毛細現象最常見的例子。當[液體和](../Page/液體.md "wikilink")[固體](../Page/固體.md "wikilink")（管壁）之間的[附著力大於液體本身](../Page/黏附.md "wikilink")[內聚力時](../Page/內聚力.md "wikilink")，就會產生毛細現象。[液體在](../Page/液體.md "wikilink")[垂直的細管中時液面呈凹或凸狀](../Page/垂直.md "wikilink")、以及多孔材質物體能吸收[液體皆為此現象所造成的影響](../Page/液體.md "wikilink")。

## 水的毛細現象

[Capillarity.svg](https://zh.wikipedia.org/wiki/File:Capillarity.svg "fig:Capillarity.svg")與[附著力的差異](../Page/附著力.md "wikilink")，[水在毛細管中](../Page/水.md "wikilink")，中央較四周凹下；[汞在毛細管中](../Page/汞.md "wikilink")，中央較四周凸起。\]\]
毛細管常被用來說明毛細現象，當[垂直的細](../Page/垂直.md "wikilink")[玻璃管底部置於](../Page/玻璃.md "wikilink")[液體中](../Page/液體.md "wikilink")（例如[水](../Page/水.md "wikilink")）時，管壁對[水的](../Page/水.md "wikilink")[附著力便會使液面四周稍比中央高出一些](../Page/附著力.md "wikilink")；直到[液體](../Page/液體.md "wikilink")[表面張力已經無法克服其](../Page/表面張力.md "wikilink")[重量時](../Page/重量.md "wikilink")，才會停止繼續上升。在毛細管中，液柱[重量與管徑的](../Page/重量.md "wikilink")[平方成](../Page/平方.md "wikilink")[正比](../Page/正比.md "wikilink")，但是[液體與管壁的接觸](../Page/液體.md "wikilink")[面積只與管徑成正比](../Page/面積.md "wikilink")；這使得較窄的毛細管吸水會比較寬的毛細管來得高。例如，一根管徑0.5毫米的玻璃細管，理論上能夠將水抬升2.8厘米，但實際觀察時其高度會略低些。

## 汞的毛細現象

在某些[液體與](../Page/液體.md "wikilink")[固體的組合中](../Page/固體.md "wikilink")，與毛細管吸水的狀況略為不同，例如細[玻璃管與](../Page/玻璃.md "wikilink")[水銀](../Page/水銀.md "wikilink")（汞），汞柱本身的[原子](../Page/原子.md "wikilink")[內聚力大於汞柱與管壁之間的](../Page/內聚力.md "wikilink")[附著力](../Page/附著力.md "wikilink")，故汞柱液面中央會稍比四周凸起，這和毛細管吸[水的狀況恰為相反](../Page/水.md "wikilink")。

## 毛細現象應用

[TLC_black_ink.jpg](https://zh.wikipedia.org/wiki/File:TLC_black_ink.jpg "fig:TLC_black_ink.jpg")利用了毛細現象。\]\]
[Capillary_papertowel.PNG](https://zh.wikipedia.org/wiki/File:Capillary_papertowel.PNG "fig:Capillary_papertowel.PNG")

  - 在[水文學中](../Page/水文學.md "wikilink")，毛細現象常用來解釋[土壤對](../Page/土壤.md "wikilink")[水的吸引力](../Page/水.md "wikilink")；在[土壤中](../Page/土壤.md "wikilink")，[水分會由較](../Page/水.md "wikilink")[潮溼處移動到](../Page/潮溼.md "wikilink")[乾燥處](../Page/乾燥.md "wikilink")，即是毛細現象所致。
  - 毛細現象也是[眼淚能夠自](../Page/眼淚.md "wikilink")[眼睛不斷流出的必要因素](../Page/眼睛.md "wikilink")。
  - 現今某些材質的[運動衣料](../Page/運動.md "wikilink")，會透過毛細現象吸[汗](../Page/汗.md "wikilink")。
  - [化學家常利用毛細現象來進行](../Page/化學家.md "wikilink")[薄板層析](../Page/薄板層析.md "wikilink")（薄板色譜分析）。
  - [自来水笔的笔管也是通过毛细现象维持笔头湿润](../Page/自来水笔.md "wikilink")
  - [紙巾即是透過毛細現象吸收](../Page/紙巾.md "wikilink")[液體](../Page/液體.md "wikilink")，其充滿細孔的材質使得[液體能夠被紙巾吸收](../Page/液體.md "wikilink")。
  - [海綿有非常多的細小孔洞](../Page/海綿.md "wikilink")（相當於毛細管），這使得[海綿能夠吸收大量的](../Page/海綿.md "wikilink")[液體](../Page/液體.md "wikilink")。
  - [蠟燭芯將蠟引到火附近](../Page/蠟燭.md "wikilink")。

## 公式

液柱上升高度是：

  -
    \(h={{2 \gamma \cos{\theta}}\over{\rho g r}}\)

此處：

  -
    *γ* = [表面張力係數](../Page/表面張力係數.md "wikilink")
    *θ* = 接觸角
    *ρ* = 液體[密度](../Page/密度.md "wikilink")
    *g* = [重力加速度](../Page/重力加速度.md "wikilink")
    *r* = 細管半徑

當*θ*\>90度，這表示彎液面為凸面；同時*h*\<0，表示流體在毛細管下降，即[汞在玻璃管的情況](../Page/汞.md "wikilink")。

對於在[海平面上](../Page/海平面.md "wikilink")，裝了水的玻璃管，

  -
    *γ* = 0.0728 J m<sup>-2</sup>
    *θ* = 20°
    *ρ* = 1000 kg m<sup>-3</sup>
    *g* = 9.8 m s<sup>-2</sup>

液柱高度為：

  -
    \(h\approx {{1.4 \times 10^{-5}}\over r} \ \mbox{m}\) .

根據此方程式，理論上在1米寬的管中，水可以上升0. 000
014米（因此極不容易被察覺）；另外在1厘米寬的管中，水可以上升0.14厘米；而在半徑0.1毫米的毛細管中，水可以上升140毫米。

### 推導

  - 方法一：考慮表面張力的力

<!-- end list -->

  -
    \(2\pi r\gamma\cos\theta = \rho gh \pi r^2 \;\).

其中

  -
    表面張力引起的力為\(F = 2\pi r\gamma\;\)，而其垂直向上的部分為\(F \cdot \cos\theta\;\)；
    升起的液體部分的體積為\(V = \pi r^2 h\;\)，其[重量](../Page/重量.md "wikilink")（[重力的作用力](../Page/重力.md "wikilink")）為\(\rho Vg \;\)；。

<!-- end list -->

  - 方法二：考慮流體內非常接近彎液面的點A和非常接近毛細管外表面的點B的壓力，按[伯努利定律有](../Page/伯努利定律.md "wikilink")：

<!-- end list -->

  -
    \(P_0 - \frac{2\gamma}{R} + \rho g h = P_0\)

其中，*R*為彎液面的半徑，\(R = \frac{r}{\cos\theta}\)；\(P_0=P_A=P_B\)則為大氣壓力。

## 参见

  - [浸润](../Page/浸润.md "wikilink")
  - [虹吸](../Page/虹吸.md "wikilink")

[Category:流体力学](../Category/流体力学.md "wikilink")
[Category:物理现象](../Category/物理现象.md "wikilink")