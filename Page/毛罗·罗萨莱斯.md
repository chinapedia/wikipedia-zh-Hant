**-{zh-hans:毛罗·罗萨莱斯; zh-hant:羅沙里斯;}-**（**Mauro Damián
Rosales**，）是一名現役[阿根廷足球運動員](../Page/阿根廷.md "wikilink")，司職中場，現時效力阿根廷勁旅[河床](../Page/河床體育會.md "wikilink")，羅沙里斯曾經協助阿根廷奪得[2004年夏季奧林匹克運動會金牌](../Page/2004年夏季奧林匹克運動會足球比賽.md "wikilink")。

羅沙里斯成名於阿根廷球會[紐維爾舊生](../Page/紐維爾舊生.md "wikilink")，2004年以180萬歐元加盟荷蘭勁旅[阿積士](../Page/阿積士.md "wikilink")，同年6月在世界盃外圍賽對巴西後備入替[迪加度首次替國家隊上陣](../Page/迪加度.md "wikilink")，最終球隊以1比3不敵對手。羅沙里斯之後代表阿根廷出戰美洲國家盃及奧運會，其中在奧運會決賽對巴拉圭時，成功協助[泰維斯射入致勝入球奪得金牌](../Page/泰維斯.md "wikilink")。

## 榮譽

  - [世青盃](../Page/世青盃.md "wikilink") : 2001年
  - [夏季奧林匹克運動會金牌](../Page/夏季奧林匹克運動會.md "wikilink"): 2004年
  - [荷蘭盃](../Page/荷蘭盃.md "wikilink"): 2006年
  - [荷蘭超級盃](../Page/荷蘭超級盃.md "wikilink"): 2005年, 2006年

## 外部連結

  - [個人檔案](http://www.football-lineups.com/players/player.php?route=844)
  - [個人檔案](http://www.cariverplate.com.ar/tpl.php?cat=es&url=ficha-jugador.php&id=115)

[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:阿根廷足球運動員](../Category/阿根廷足球運動員.md "wikilink")
[Category:河床球員](../Category/河床球員.md "wikilink")
[Category:紐維爾舊生球員](../Category/紐維爾舊生球員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:西雅圖海灣者球員](../Category/西雅圖海灣者球員.md "wikilink")
[Category:阿根廷奧林匹克運動會金牌得主](../Category/阿根廷奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:阿甲球員](../Category/阿甲球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:美國外籍足球運動員](../Category/美國外籍足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:西班牙裔阿根廷人](../Category/西班牙裔阿根廷人.md "wikilink")
[Category:2004年美洲盃球員](../Category/2004年美洲盃球員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:科多巴省人](../Category/科多巴省人.md "wikilink")
[Category:奧林匹克運動會足球獎牌得主](../Category/奧林匹克運動會足球獎牌得主.md "wikilink")