**毫秒脈衝星**（MSP），曾經被稱為**"反覆脈衝星"**，是自轉週期在1-10[毫秒範圍內的](../Page/毫秒.md "wikilink")[脈衝星](../Page/脈衝星.md "wikilink")，他目前僅能在[微波或](../Page/無線電脈衝星.md "wikilink")[X射線的](../Page/X射線脈衝星.md "wikilink")[電磁波頻譜的波段上被觀察到](../Page/電磁波頻譜.md "wikilink")。

毫秒脈衝星的起源依然有些神秘，主導的理論認為它們原本是週期較長的脈衝星，經由[吸積的延長或](../Page/吸積.md "wikilink")[回覆](../Page/回覆.md "wikilink")。基於這個理由，[低質量X射線雙星系特別受到關注](../Page/低質量X射線雙星.md "wikilink")，它們被認為是正在回覆過程中的脈衝星。

像這一類散發出X射線的脈衝星被認為是正在被加速的階段，活躍性正在增加中。它們可能是正在吸收由伴星的[洛希瓣溢出的](../Page/洛希瓣.md "wikilink")[角動量](../Page/角動量.md "wikilink")，使自轉的速度增加至每秒鐘數百轉，而被加速的[中子星](../Page/中子星.md "wikilink")。已經被加速了的毫秒脈衝星，散發出的電磁波頻譜是在長波長的部分。

許多毫秒脈衝星是在[球狀星團內被發現的](../Page/球狀星團.md "wikilink")，因為在這些系統內極端高的恆星密度有利於創造能引起雙星之間質量交換的環境，讓自轉的中子星經由交互作用提高週期成為毫秒脈衝星。目前在[球狀星團內發現的毫秒脈衝星大約有](../Page/球狀星團.md "wikilink")130顆\[1\]，單單在[Terzan
5中就有](../Page/Terzan_5.md "wikilink")33顆，然後是[杜鵑座47有](../Page/杜鵑座47.md "wikilink")22顆，[M28和](../Page/M28.md "wikilink")[M15各有](../Page/M15.md "wikilink")8顆。

毫秒級脈衝星可以高精度定時，優於1997年最佳的[原子鐘](../Page/原子鐘.md "wikilink")\[2\]。這也使它們成為對環境非常敏感的探測器。

## 脈衝星自轉和速度的限制

第一顆毫秒脈衝星，[PSR
B1937+21](../Page/PSR_B1937+21.md "wikilink")，在1982年被發現，轉速為每秒641轉
，它的輻射落在無線電的波段上，但他擁有轉速最快中子星的頭銜只有大約180天。在2005年發現的[PSR
J1748-2446ad](../Page/PSR_J1748-2446ad.md "wikilink")，是迄2006年所知，轉速最快的中子星，每秒鐘轉716次\[3\],
\[4\] 。

以目前中子星結構和演變的理論，預言脈衝星旋轉速度的極限如下：

  - 它們的自轉不能超過每秒1,500轉，超過了中子星的[逃逸速度可能會分裂開來](../Page/逃逸速度.md "wikilink")；
  - 另外，在達到這種高速自轉之前，會輻射出[重力波](../Page/重力波.md "wikilink")，在被進一步加速前抑制轉速的提高。實際上，轉速似乎已經被抑制在每秒1,000轉之內（對應於週期1毫秒的時間）。

的確，到2006年底還沒有[次毫秒脈衝星被發現](../Page/次毫秒脈衝星.md "wikilink")，（如上所述，轉速最快的是716[赫茲](../Page/赫茲.md "wikilink")）。這暗示重力波的輻射造成的能量損失，確實對高速轉動的中子星產生了剎車機制的作用。

然而，在2007年初，來自[羅西X射線時變探測器和](../Page/羅西X射線時變探測器.md "wikilink")[國際伽瑪射線天體物理實驗室卻指出中子星](../Page/國際伽瑪射線天體物理實驗室.md "wikilink")[XTE
J1739-285每秒自轉](../Page/XTE_J1739-285.md "wikilink")1,122次。\[5\]
但是，這個結果在統計上的意義不大，因為落在3[標準差之處](../Page/標準差.md "wikilink")，所以只當它是一個有趣的候選者，結果也暫時僅供參考。但是，重力輻射被相信是扮演著減緩自轉速率的角色，因此早先發現的一個轉速高達每秒599轉的[X射線脈衝星](../Page/X射線脈衝星.md "wikilink")，[IGR
J00291+5934](../Page/IGR_J00291+5934.md "wikilink")，依然是最可能產生[重力波的第一順位候選者](../Page/重力波.md "wikilink")，在將來最有可能被偵測到重力波，因為多數[X射線脈衝星的轉速都只有每秒](../Page/X射線脈衝星.md "wikilink")300轉左右。

## 參考資料

<references/>

  - [How Millisecond Pulsars Spin So
    Fast](http://www.universetoday.com/am/publish/origin_of_millisecond_pulsars_47tucw.html)
    - Universe Today
  - [Fast-Spinning Star Could Test Gravitational
    Waves](http://www.newscientist.com/article.ns?id=dn7052) - New
    Scientist

[Category:脈衝星](../Category/脈衝星.md "wikilink")
[\*](../Category/毫秒脈衝星.md "wikilink")

1.  [1](http://www2.naic.edu/~pfreire/GCpsr.html)
2.
3.  [2](http://arxiv.org/abs/astro-ph/0601337)
4.  [3](http://skyandtelescope.com/news/article_1659_1.asp)
5.  [Spaceflight
    Now](http://www.spaceflightnow.com/news/n0702/19neutronstar/)
    Accessed 20 Feb 2007