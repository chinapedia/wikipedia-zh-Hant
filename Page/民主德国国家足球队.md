<sup>1</sup> |- | colspan=3|<sup>1</sup>
以[德國聯隊名義](../Page/德國聯隊.md "wikilink")
**德意志民主共和國國家足球隊**，是[德意志民主共和國的國家足球隊](../Page/德意志民主共和國.md "wikilink")。由於東西德重新統一，在1990年結束。

他們從未進入過[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")，只參加過[1974年的世界盃決賽週](../Page/1974年世界盃足球賽.md "wikilink")。虽然他們在[漢堡在一场政治味濃厚的分組賽中](../Page/漢堡.md "wikilink")，憑14號的[尤尔根·施帕瓦塞尔的唯一入球](../Page/尤尔根·施帕瓦塞尔.md "wikilink")，以一比零擊敗[西德](../Page/德國國家足球隊.md "wikilink")，後者还是奪去了世界盃冠軍\[1\]。兩年後，他們終於在[1976年夏季奧林匹克運動會奪得了足球冠軍](../Page/1976年夏季奧林匹克運動會.md "wikilink")。

[德國重新統一後](../Page/德國.md "wikilink")，一些[東德的足球員](../Page/東德.md "wikilink")，特別是[-{zh-hant:森馬;
zh-cn:马蒂亚斯·萨默尔;}-和](../Page/马蒂亚斯·萨默尔.md "wikilink")[基斯頓](../Page/伍爾夫·基爾斯滕.md "wikilink")，都在[統一後的國家足球隊佔一席位](../Page/德國國家足球隊.md "wikilink")。

## 世界杯成绩

  - [1954年](../Page/1954年世界杯足球赛.md "wikilink") - *無參賽*
  - [1958年](../Page/1958年世界杯足球赛.md "wikilink") 至
    [1970年](../Page/1970年世界杯足球赛.md "wikilink") - *未能晉級*
  - [1974年](../Page/1974年世界杯足球赛.md "wikilink") - 第二圈
  - [1978年](../Page/1978年世界杯足球赛.md "wikilink") 至
    [1990年](../Page/1990年世界杯足球赛.md "wikilink") - *未能晉級*

## 欧洲杯成绩

  - [1960年](../Page/1960年歐洲國家盃.md "wikilink") 至
    [1988年](../Page/1988年歐洲國家盃.md "wikilink") - *未能晉級*

## 參見

  - [德國國家足球隊](../Page/德國國家足球隊.md "wikilink")

## 参考

[Category:東德體育](../Category/東德體育.md "wikilink")
[DDR](../Category/過去的歐洲國家足球隊.md "wikilink")

1.