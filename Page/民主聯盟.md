**民主聯盟**是[中華民國的一個](../Page/中華民國.md "wikilink")[政黨](../Page/政黨.md "wikilink")。它創立於1998年6月24日，最初登記為全國性政治團體，於同年8月，轉換屬性成為政黨。黨主席為時任第三屆立法委員的[徐成焜](../Page/徐成焜.md "wikilink")，並獲得第三屆立法委員[陳漢強](../Page/陳漢強.md "wikilink")、[杜振榮以及當時已辭去第三屆立法委員的](../Page/杜振榮.md "wikilink")[林志嘉加盟](../Page/林志嘉.md "wikilink")。

民主聯盟於[1998年立法委員選舉推出的許多候選人原為](../Page/1998年立法委員選舉.md "wikilink")[中國國民黨籍](../Page/中國國民黨.md "wikilink")，例如[臺北縣](../Page/新北市.md "wikilink")[林志嘉](../Page/林志嘉.md "wikilink")、[苗栗縣徐成焜](../Page/苗栗縣.md "wikilink")、[台東縣](../Page/台東縣.md "wikilink")[徐慶元](../Page/徐慶元.md "wikilink")、[台南市](../Page/台南市.md "wikilink")[杜振榮等](../Page/杜振榮.md "wikilink")，曾是[綠色本土清新黨籍](../Page/台灣綠黨.md "wikilink")[國大代表的](../Page/國大代表.md "wikilink")[高孟定](../Page/高孟定.md "wikilink")，也在該次選舉中代表民主聯盟參選，最終共當選4席第四屆立法委員（[徐成焜](../Page/徐成焜.md "wikilink")、[林志嘉](../Page/林志嘉.md "wikilink")、[陳進丁](../Page/陳進丁.md "wikilink")、[徐慶元](../Page/徐慶元.md "wikilink")）。

2018年6月12日，內政部公告，民主聯盟自2018年5月22日解散。\[1\]

## 參考資料

## 參照

  - [中華民國政黨列表](../Page/中華民國政黨列表.md "wikilink")

[Category:台灣已解散政黨](../Category/台灣已解散政黨.md "wikilink")
[Category:臺灣政黨](../Category/臺灣政黨.md "wikilink")
[Category:1998年建立的政黨](../Category/1998年建立的政黨.md "wikilink")
[Category:2018年解散的政黨](../Category/2018年解散的政黨.md "wikilink")
[Category:2018年台灣廢除](../Category/2018年台灣廢除.md "wikilink")

1.