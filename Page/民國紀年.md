[ROC_calendar.jpg](https://zh.wikipedia.org/wiki/File:ROC_calendar.jpg "fig:ROC_calendar.jpg")\]\]

**民國紀年**（又稱**中華民國曆**）是[中華民國的國家](../Page/中華民國.md "wikilink")[紀年方式](../Page/紀年.md "wikilink")，由[中華民國政府規範頒布](../Page/中華民國政府.md "wikilink")，表記時稱作**中華民國×××年**，簡稱**民國×××年**、**民×××**。這種紀年方式以[西元](../Page/西元.md "wikilink")[1912年](../Page/1912年.md "wikilink")[中華民國成立為元年](../Page/中華民國歷史#建國之初.md "wikilink")，與西元紀年相差1911年；[月](../Page/月.md "wikilink")、[日](../Page/日.md "wikilink")、[置閏則同](../Page/置閏.md "wikilink")[西曆](../Page/西曆.md "wikilink")。現今民國{{\#expr:-1911}}年為西元年。依照民國紀年制定之[曆法](../Page/曆法.md "wikilink")，又稱[國曆](../Page/國曆.md "wikilink")。目前主要使用於中華民國政府實際統治的[臺澎金馬](../Page/臺灣地區.md "wikilink")。

## 概要

[清](../Page/清朝.md "wikilink")[宣統三年](../Page/宣統.md "wikilink")[辛亥年](../Page/辛亥.md "wikilink")（[1911年](../Page/1911年.md "wikilink")），[革命黨人於](../Page/革命黨.md "wikilink")[武昌起義](../Page/武昌起義.md "wikilink")，推翻清朝帝制。[湖北軍政府的文告和各省響應的宣言不使用年號](../Page/湖北軍政府.md "wikilink")，而使用[黃帝紀元](../Page/黃帝紀元.md "wikilink")；但革命黨人討論成立共和政府時，認爲黃帝紀年也不合[民主共和的宗旨](../Page/民主共和.md "wikilink")。各省代表於十二月二十九日在[南京召開](../Page/南京.md "wikilink")[臨時大總統選舉大會](../Page/1911年中華民國臨時大總統選舉.md "wikilink")，孫中山當選為[中華民國臨時大總統](../Page/中華民國大總統.md "wikilink")。次年（[1912年](../Page/1912年.md "wikilink")）[元旦正式就職](../Page/元旦.md "wikilink")，通電各省，頒訂[國號為中華民國](../Page/國號.md "wikilink")，不再使用帝制的[年號](../Page/年號.md "wikilink")，以黃帝紀元4609年11月13日（1912年[1月1日](../Page/1月1日.md "wikilink")）為中華民國元年元旦，并改用[公曆](../Page/公曆.md "wikilink")。中華民國成立前的[紀年可用](../Page/紀年.md "wikilink")「民前」，例如，民前一年就是1911年。

民國紀年的[起始年份](../Page/元年.md "wikilink")，與[朝鮮民主主義人民共和國的](../Page/朝鮮民主主義人民共和國.md "wikilink")[主體紀年和](../Page/主體曆.md "wikilink")[日本](../Page/日本.md "wikilink")[大正天皇在位期間](../Page/大正天皇.md "wikilink")（1912年至[1926年](../Page/1926年.md "wikilink")）的日本年號「[大正](../Page/大正.md "wikilink")」一致，即中華民國的「民國××年」，同時是[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")「主體××年」和日本「大正××年」（至1926年大正天皇[駕崩為止](../Page/駕崩.md "wikilink")）。這是因為中華民國元年、[朝鮮民主主義人民共和國的創建者](../Page/朝鮮民主主義人民共和國.md "wikilink")[金日成的出生年份](../Page/金日成.md "wikilink")、以及日本大正天皇的登基年份均為[公元](../Page/公元.md "wikilink")1912年的緣故。

民國紀年沒有法定的規範機關，僅有[中華民國內政部](../Page/中華民國內政部.md "wikilink")[民政司的禮制行政科製作國民曆](../Page/內政部民政司.md "wikilink")，以方便民眾查詢曆法之用；相關[法律中也僅有](../Page/中華民國法律.md "wikilink")《[公文程式條例](../Page/:s:公文程式條例.md "wikilink")》的第六條規定[中華民國政府](../Page/中華民國政府.md "wikilink")[公文必須註記國曆日期](../Page/公文.md "wikilink")。2006年，當時執政的[陳水扁政府曾提倡廢除民國紀年](../Page/陳水扁政府.md "wikilink")、全面改採公元紀年，但最後無疾而終。

## 各地使用

### 中華民國當前治理範圍（臺灣、澎湖、金門、馬祖）

1912年[中華民國成立時](../Page/中華民國.md "wikilink")，臺澎地區[由日本統治](../Page/臺灣日治時期.md "wikilink")，故未使用民國紀年，而是與[日本內地一樣採用](../Page/內地_\(大日本帝國\).md "wikilink")[天皇年號](../Page/日本年號.md "wikilink")，直到1945年中華民國接收臺灣後開始採用。隨著國際交流的增加，尤其是1990年代以來，民間轉為民國紀年和[西元紀年並用](../Page/公元.md "wikilink")。但中華民國政府各機關，各式官方文書和[中國國民黨均採用民國紀元來紀年](../Page/中國國民黨.md "wikilink")。近年來臺灣地區有部分人士及團體不用民國紀年，而只使用公元紀年，如[民主進步黨的黨證](../Page/民主進步黨.md "wikilink")、文宣及公文乃至於網站。\[1\]

目前臺灣民間對民國紀年的使用情況依其認同立場或習慣而不一致。以[報紙為例](../Page/台灣報紙列表.md "wikilink")，原自中華民國治臺以來均採用民國紀年來表記日期，但持[本土立場的](../Page/臺灣主體性.md "wikilink")《[自由時報](../Page/自由時報.md "wikilink")》於2005年9月6日起轉用公元紀年，立場偏向[泛藍的](../Page/泛藍.md "wikilink")《[聯合報](../Page/聯合報.md "wikilink")》、《[中國時報](../Page/中國時報.md "wikilink")》及軍系報紙《[青年日報](../Page/青年日報.md "wikilink")》維持使用民國紀年\[2\]，《[蘋果日報](../Page/蘋果日報_\(台灣\).md "wikilink")》與《[臺灣時報](../Page/臺灣時報.md "wikilink")》則在紙本刊頭同時標示公元、民國與農曆3種紀年\[3\]，內頁均採用公元紀年。[臺灣媒體文章常見的](../Page/臺灣媒體.md "wikilink")「○年級」或「○年級生」，則是對民國紀年某[年代出生者的統稱](../Page/年代.md "wikilink")，例如民國50年代（約同西元1960年代）出生為「五年級」、民國60年代（約同西元1970年代）出生為「六年級」等。

此外，二戰後中華民國政府由於試圖消除日本統治台灣的影響，對過去凡是用到日本年號的物品或文獻一律改成民國紀年（如[臺中一中建校紀念碑碑文上的](../Page/臺中一中.md "wikilink")[大正三年被改為民國三年](../Page/大正.md "wikilink")、[碧潭吊橋橋頭的完工日期標記由昭和十二年被改為民國二十五年](../Page/碧潭吊橋.md "wikilink")），2011年[宜蘭縣政府教育處長陳登欽批評此舉對歷史教育或研究造成困擾](../Page/宜蘭縣政府.md "wikilink")，認為日治期間臺灣與民國並無關聯\[4\]。目前新出版的歷史書籍或古蹟解說，或有直接使用日本年號，或在日本年號後附上民國及公元紀年，又或僅採公元紀年以迴避[意識形態爭議](../Page/意識形態.md "wikilink")。

### 香港

[香港在](../Page/香港.md "wikilink")[英治時期一直採用公元紀年](../Page/英屬香港.md "wikilink")。

1997年7月[香港主權交接給](../Page/香港回歸.md "wikilink")[中華人民共和國前](../Page/中華人民共和國.md "wikilink")，部分[反共的](../Page/反共.md "wikilink")[中文報章](../Page/香港報紙列表.md "wikilink")（例如《[星島日報](../Page/星島日報.md "wikilink")》、《[東方日報](../Page/東方日報_\(香港\).md "wikilink")》、《[快報](../Page/快報_\(香港\).md "wikilink")》）的日期，會印有公元、民國紀年（例如：已停刊的《[工商日報](../Page/工商日報.md "wikilink")》、《[華僑日報](../Page/華僑日報.md "wikilink")》、中英[香港前途問題談判前的](../Page/香港前途問題.md "wikilink")《[明報](../Page/明報.md "wikilink")》）及[農曆並列](../Page/農曆.md "wikilink")，而曾由《星島》、《華僑》、《工商》三大報社所主導，已倒閉的[佳藝電視](../Page/佳藝電視.md "wikilink")，新聞報導時也一度採用民國紀年，但香港主權移交後，除民間使用以外均取消。

### 中國大陸

[Advertisement_of_Matou_Ice_Lollipop_in_Nanjing,_Jiangsu.jpg](https://zh.wikipedia.org/wiki/File:Advertisement_of_Matou_Ice_Lollipop_in_Nanjing,_Jiangsu.jpg "fig:Advertisement_of_Matou_Ice_Lollipop_in_Nanjing,_Jiangsu.jpg")的一幅廣告，其中「民國1947年」，應該是「[民國時期的公元](../Page/民國時期.md "wikilink")1947年」。\]\]
1912年1月1日至1949年9月30日的[中華民國](../Page/中華民國.md "wikilink")[大陸時期](../Page/中華民國_\(大陸時期\).md "wikilink")，使用民國紀年。

1949年10月1日，中國共產黨建立的[中华人民共和国成立](../Page/中华人民共和国.md "wikilink")，取代[中華民國政府統治絕大部分的](../Page/中華民國政府.md "wikilink")[中國疆域後](../Page/中国疆域史.md "wikilink")，在其有效統治之領土改用公元紀年，中华人民共和国對於[1912年](../Page/1912年.md "wikilink")[1月1日至](../Page/1月1日.md "wikilink")[1949年](../Page/1949年.md "wikilink")[9月30日的中華民國時期歷史敘述時](../Page/9月30日.md "wikilink")，也只提公元紀年。但學術著作仍可使用民國紀年至[民國38年](../Page/民國38年.md "wikilink")（1949年）9月30日，因為中华人民共和国官方認為中華民國已經於該年[10月1日](../Page/10月1日.md "wikilink")（中华人民共和国建国日）結束。然而中华人民共和国成立之時，[四川](../Page/四川.md "wikilink")、[西康](../Page/西康.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[貴州](../Page/貴州.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[海南](../Page/海南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[福建等地區仍由](../Page/福建.md "wikilink")[中華民國政府控制](../Page/中華民國政府.md "wikilink")，因此仍使用民國紀年。

[1950年](../Page/1950年.md "wikilink")[3月27日](../Page/3月27日.md "wikilink")，[中國人民解放軍佔領](../Page/中國人民解放軍.md "wikilink")[西昌](../Page/西昌市.md "wikilink")；[5月1日](../Page/5月1日.md "wikilink")，中國共產黨攻克[海南島全境](../Page/海南島.md "wikilink")，1951年[将西藏纳入统治](../Page/西藏和平解放.md "wikilink")。

[1955年](../Page/1955年.md "wikilink")2月25日，中華民國政府放棄[大陳列島](../Page/台州列岛.md "wikilink")，實行[大陳島撤退](../Page/大陳島撤退.md "wikilink")，至此，[大陸地區已經完全停用民國紀年](../Page/大陸地區.md "wikilink")。

### 新加坡

[Lat_Pau,_8_March_1917.jpg](https://zh.wikipedia.org/wiki/File:Lat_Pau,_8_March_1917.jpg "fig:Lat_Pau,_8_March_1917.jpg")，上頭印有民國紀元。\]\]
1912年[中華民國建立後](../Page/中華民國.md "wikilink")，[新加坡的華文報紙上頭的日期為民國紀年](../Page/新加坡.md "wikilink")。現今只有公元紀年。

## 標準寫法

根據《[中華民國國家標準法](../Page/中華民國國家標準.md "wikilink")》CNS
7648《資料元及交換格式–資訊交換–日期及時間的表示法》（與[ISO
8601類似](../Page/ISO_8601.md "wikilink")），紀年可用作公元，也可冠以大寫「R.O.C.」字母（簡寫）用民國紀元。例如，2013年（中華民國102年）1月1日可寫作「2013-01-01」或「R.O.C.102-01-01」。

## 紀元轉換

### 民國後

民國後的紀年加上1911即為西元紀年。

<table>
<tbody>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬子</p></td>
<td><p>癸丑</p></td>
<td><p>甲寅</p></td>
<td><p>乙卯</p></td>
<td><p>丙辰</p></td>
<td><p>丁巳</p></td>
<td><p>戊午</p></td>
<td><p>己未</p></td>
<td><p>庚申</p></td>
<td><p>辛酉</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>元年</p></td>
<td><p>二年</p></td>
<td><p>三年</p></td>
<td><p>四年</p></td>
<td><p>五年</p></td>
<td><p>六年</p></td>
<td><p>七年</p></td>
<td><p>八年</p></td>
<td><p>九年</p></td>
<td><p>十年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/明治.md" title="wikilink">明治</a>45年<br />
<a href="../Page/大正.md" title="wikilink">大正元年</a></p></td>
<td><p>2年</p></td>
<td><p>3年</p></td>
<td><p>4年</p></td>
<td><p>5年</p></td>
<td><p>6年</p></td>
<td><p>7年</p></td>
<td><p>8年</p></td>
<td><p>9年</p></td>
<td><p><a href="../Page/大正.md" title="wikilink">大正</a>10年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>1912</p></td>
<td><p>1913</p></td>
<td><p>1914</p></td>
<td><p>1915</p></td>
<td><p>1916</p></td>
<td><p>1917</p></td>
<td><p>1918</p></td>
<td><p>1919</p></td>
<td><p>1920</p></td>
<td><p>1921</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬戌</p></td>
<td><p>癸亥</p></td>
<td><p>|甲子</p></td>
<td><p>乙丑</p></td>
<td><p>丙寅</p></td>
<td><p>丁卯</p></td>
<td><p>戊辰</p></td>
<td><p>己巳</p></td>
<td><p>庚午</p></td>
<td><p>辛未</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>十一年</p></td>
<td><p>十二年</p></td>
<td><p>十三年</p></td>
<td><p>十四年</p></td>
<td><p>十五年</p></td>
<td><p>十六年</p></td>
<td><p>十七年</p></td>
<td><p>十八年</p></td>
<td><p>十九年</p></td>
<td><p>二十年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/大正.md" title="wikilink">大正</a>11年</p></td>
<td><p>12年</p></td>
<td><p>13年</p></td>
<td><p>14年</p></td>
<td><p><a href="../Page/大正.md" title="wikilink">大正</a>15年<br />
<a href="../Page/昭和.md" title="wikilink">昭和元年</a></p></td>
<td><p>2年</p></td>
<td><p>3年</p></td>
<td><p>4年</p></td>
<td><p>5年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>6年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>1922</p></td>
<td><p>1923</p></td>
<td><p>1924</p></td>
<td><p>1925</p></td>
<td><p>1926</p></td>
<td><p>1927</p></td>
<td><p>1928</p></td>
<td><p>1929</p></td>
<td><p>1930</p></td>
<td><p>1931</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬申</p></td>
<td><p>癸酉</p></td>
<td><p>甲戌</p></td>
<td><p>乙亥</p></td>
<td><p>丙子</p></td>
<td><p>丁丑</p></td>
<td><p>戊寅</p></td>
<td><p>己卯</p></td>
<td><p>庚辰</p></td>
<td><p>辛巳</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>二十一年</p></td>
<td><p>二十二年</p></td>
<td><p>二十三年</p></td>
<td><p>二十四年</p></td>
<td><p>二十五年</p></td>
<td><p>二十六年</p></td>
<td><p>二十七年</p></td>
<td><p>二十八年</p></td>
<td><p>二十九年</p></td>
<td><p>三十年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>7年</p></td>
<td><p>8年</p></td>
<td><p>9年</p></td>
<td><p>10年</p></td>
<td><p>11年</p></td>
<td><p>12年</p></td>
<td><p>13年</p></td>
<td><p>14年</p></td>
<td><p>15年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>16年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>1932</p></td>
<td><p>1933</p></td>
<td><p>1934</p></td>
<td><p>1935</p></td>
<td><p>1936</p></td>
<td><p>1937</p></td>
<td><p>1938</p></td>
<td><p>1939</p></td>
<td><p>1940</p></td>
<td><p>1941</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬午</p></td>
<td><p>癸未</p></td>
<td><p>甲申</p></td>
<td><p>乙酉</p></td>
<td><p>丙戌</p></td>
<td><p>丁亥</p></td>
<td><p>戊子</p></td>
<td><p>己丑</p></td>
<td><p>庚寅</p></td>
<td><p>辛卯</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>三十一年</p></td>
<td><p>三十二年</p></td>
<td><p>三十三年</p></td>
<td><p>三十四年</p></td>
<td><p>三十五年</p></td>
<td><p>三十六年</p></td>
<td><p>三十七年</p></td>
<td><p>三十八年</p></td>
<td><p>三十九年</p></td>
<td><p>四十年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>17年</p></td>
<td><p>18年</p></td>
<td><p>19年</p></td>
<td><p>20年</p></td>
<td><p>21年</p></td>
<td><p>22年</p></td>
<td><p>23年</p></td>
<td><p>24年</p></td>
<td><p>25年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>26年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>1942</p></td>
<td><p>1943</p></td>
<td><p>1944</p></td>
<td><p>1945</p></td>
<td><p>1946</p></td>
<td><p>1947</p></td>
<td><p>1948</p></td>
<td><p>1949</p></td>
<td><p>1950</p></td>
<td><p>1951</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬辰</p></td>
<td><p>癸巳</p></td>
<td><p>甲午</p></td>
<td><p>乙未</p></td>
<td><p>丙申</p></td>
<td><p>丁酉</p></td>
<td><p>戊戌</p></td>
<td><p>己亥</p></td>
<td><p>庚子</p></td>
<td><p>辛丑</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>四十一年</p></td>
<td><p>四十二年</p></td>
<td><p>四十三年</p></td>
<td><p>四十四年</p></td>
<td><p>四十五年</p></td>
<td><p>四十六年</p></td>
<td><p>四十七年</p></td>
<td><p>四十八年</p></td>
<td><p>四十九年</p></td>
<td><p>五十年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>27年</p></td>
<td><p>28年</p></td>
<td><p>29年</p></td>
<td><p>30年</p></td>
<td><p>31年</p></td>
<td><p>32年</p></td>
<td><p>33年</p></td>
<td><p>34年</p></td>
<td><p>35年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>36年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>1952</p></td>
<td><p>1953</p></td>
<td><p>1954</p></td>
<td><p>1955</p></td>
<td><p>1956</p></td>
<td><p>1957</p></td>
<td><p>1958</p></td>
<td><p>1959</p></td>
<td><p>1960</p></td>
<td><p>1961</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬寅</p></td>
<td><p>癸卯</p></td>
<td><p>甲辰</p></td>
<td><p>乙巳</p></td>
<td><p>丙午</p></td>
<td><p>丁未</p></td>
<td><p>戊申</p></td>
<td><p>己酉</p></td>
<td><p>庚戌</p></td>
<td><p>辛亥</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>五十一年</p></td>
<td><p>五十二年</p></td>
<td><p>五十三年</p></td>
<td><p>五十四年</p></td>
<td><p>五十五年</p></td>
<td><p>五十六年</p></td>
<td><p>五十七年</p></td>
<td><p>五十八年</p></td>
<td><p>五十九年</p></td>
<td><p>六十年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>37年</p></td>
<td><p>38年</p></td>
<td><p>39年</p></td>
<td><p>40年</p></td>
<td><p>41年</p></td>
<td><p>42年</p></td>
<td><p>43年</p></td>
<td><p>44年</p></td>
<td><p>45年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>46年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>1962</p></td>
<td><p>1963</p></td>
<td><p>1964</p></td>
<td><p>1965</p></td>
<td><p>1966</p></td>
<td><p>1967</p></td>
<td><p>1968</p></td>
<td><p>1969</p></td>
<td><p>1970</p></td>
<td><p>1971</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬子</p></td>
<td><p>癸丑</p></td>
<td><p>甲寅</p></td>
<td><p>乙卯</p></td>
<td><p>丙辰</p></td>
<td><p>丁巳</p></td>
<td><p>戊午</p></td>
<td><p>己未</p></td>
<td><p>庚申</p></td>
<td><p>辛酉</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>六十一年</p></td>
<td><p>六十二年</p></td>
<td><p>六十三年</p></td>
<td><p>六十四年</p></td>
<td><p>六十五年</p></td>
<td><p>六十六年</p></td>
<td><p>六十七年</p></td>
<td><p>六十八年</p></td>
<td><p>六十九年</p></td>
<td><p>七十年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>47年</p></td>
<td><p>48年</p></td>
<td><p>49年</p></td>
<td><p>50年</p></td>
<td><p>51年</p></td>
<td><p>52年</p></td>
<td><p>53年</p></td>
<td><p>54年</p></td>
<td><p>55年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>56年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>1972</p></td>
<td><p>1973</p></td>
<td><p>1974</p></td>
<td><p>1975</p></td>
<td><p>1976</p></td>
<td><p>1977</p></td>
<td><p>1978</p></td>
<td><p>1979</p></td>
<td><p>1980</p></td>
<td><p>1981</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬戌</p></td>
<td><p>癸亥</p></td>
<td><p>|甲子</p></td>
<td><p>乙丑</p></td>
<td><p>丙寅</p></td>
<td><p>丁卯</p></td>
<td><p>戊辰</p></td>
<td><p>己巳</p></td>
<td><p>庚午</p></td>
<td><p>辛未</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>七十一年</p></td>
<td><p>七十二年</p></td>
<td><p>七十三年</p></td>
<td><p>七十四年</p></td>
<td><p>七十五年</p></td>
<td><p>七十六年</p></td>
<td><p>七十七年</p></td>
<td><p>七十八年</p></td>
<td><p>七十九年</p></td>
<td><p>八十年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/昭和.md" title="wikilink">昭和</a>57年</p></td>
<td><p>58年</p></td>
<td><p>59年</p></td>
<td><p>60年</p></td>
<td><p>61年</p></td>
<td><p>62年</p></td>
<td><p>63年</p></td>
<td><p>昭和64年<br />
<a href="../Page/平成.md" title="wikilink">平成元年</a></p></td>
<td><p>2年</p></td>
<td><p>3年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>1982</p></td>
<td><p>1983</p></td>
<td><p>1984</p></td>
<td><p>1985</p></td>
<td><p>1986</p></td>
<td><p>1987</p></td>
<td><p>1988</p></td>
<td><p>1989</p></td>
<td><p>1990</p></td>
<td><p>1991</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬申</p></td>
<td><p>癸酉</p></td>
<td><p>甲戌</p></td>
<td><p>乙亥</p></td>
<td><p>丙子</p></td>
<td><p>丁丑</p></td>
<td><p>戊寅</p></td>
<td><p>己卯</p></td>
<td><p>庚辰</p></td>
<td><p>辛巳</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>八十一年</p></td>
<td><p>八十二年</p></td>
<td><p>八十三年</p></td>
<td><p>八十四年</p></td>
<td><p>八十五年</p></td>
<td><p>八十六年</p></td>
<td><p>八十七年</p></td>
<td><p>八十八年</p></td>
<td><p>八十九年</p></td>
<td><p>九十年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p>平成4年</p></td>
<td><p>5年</p></td>
<td><p>6年</p></td>
<td><p>7年</p></td>
<td><p>8年</p></td>
<td><p>9年</p></td>
<td><p>10年</p></td>
<td><p>11年</p></td>
<td><p>12年</p></td>
<td><p>13年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>1992</p></td>
<td><p>1993</p></td>
<td><p>1994</p></td>
<td><p>1995</p></td>
<td><p>1996</p></td>
<td><p>1997</p></td>
<td><p>1998</p></td>
<td><p>1999</p></td>
<td><p>2000</p></td>
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬午</p></td>
<td><p>癸未</p></td>
<td><p>甲申</p></td>
<td><p>乙酉</p></td>
<td><p>丙戌</p></td>
<td><p>丁亥</p></td>
<td><p>戊子</p></td>
<td><p>己丑</p></td>
<td><p>庚寅</p></td>
<td><p>辛卯</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>九十一年</p></td>
<td><p>九十二年</p></td>
<td><p>九十三年</p></td>
<td><p>九十四年</p></td>
<td><p>九十五年</p></td>
<td><p>九十六年</p></td>
<td><p>九十七年</p></td>
<td><p>九十八年</p></td>
<td><p>九十九年</p></td>
<td><p>一百年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/平成.md" title="wikilink">平成</a>14年</p></td>
<td><p>15年</p></td>
<td><p>16年</p></td>
<td><p>17年</p></td>
<td><p>18年</p></td>
<td><p>19年</p></td>
<td><p>20年</p></td>
<td><p>21年</p></td>
<td><p>22年</p></td>
<td><p>23年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>2002</p></td>
<td><p>2003</p></td>
<td><p>2004</p></td>
<td><p>2005</p></td>
<td><p>2006</p></td>
<td><p>2007</p></td>
<td><p>2008</p></td>
<td><p>2009</p></td>
<td><p>2010</p></td>
<td><p>2011</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>干支紀年</p></td>
<td><p>壬辰</p></td>
<td><p>癸巳</p></td>
<td><p>甲午</p></td>
<td><p>乙未</p></td>
<td><p>丙申</p></td>
<td><p>丁酉</p></td>
<td><p>戊戌</p></td>
<td><p>己亥</p></td>
<td><p>庚子</p></td>
<td><p>辛丑</p></td>
</tr>
<tr class="even">
<td><p>民國紀年</p></td>
<td><p>一〇一年</p></td>
<td><p>一〇二年</p></td>
<td><p>一〇三年</p></td>
<td><p>一〇四年</p></td>
<td><p>一〇五年</p></td>
<td><p>一〇六年</p></td>
<td><p>一〇七年</p></td>
<td><p>一〇八年</p></td>
<td><p>一〇九年</p></td>
<td><p>一一〇年</p></td>
</tr>
<tr class="odd">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/平成.md" title="wikilink">平成</a>24年</p></td>
<td><p>25年</p></td>
<td><p>26年</p></td>
<td><p>27年</p></td>
<td><p>28年</p></td>
<td><p>29年</p></td>
<td><p>30年</p></td>
<td><p><a href="../Page/平成.md" title="wikilink">平成</a>31年<br />
<a href="../Page/令和.md" title="wikilink">令和元年</a></p></td>
<td><p>2年</p></td>
<td><p>3年</p></td>
</tr>
<tr class="even">
<td><p>西元紀年</p></td>
<td><p>2012</p></td>
<td><p>2013</p></td>
<td><p>2014</p></td>
<td><p>2015</p></td>
<td><p>2016</p></td>
<td><p>2017</p></td>
<td><p>2018</p></td>
<td><p>2019</p></td>
<td><p>2020</p></td>
<td><p>2021</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>干支紀年</p></td>
<td><p>壬寅</p></td>
<td><p>癸卯</p></td>
<td><p>甲辰</p></td>
<td><p>乙巳</p></td>
<td><p>丙午</p></td>
<td><p>丁未</p></td>
<td><p>戊申</p></td>
<td><p>己酉</p></td>
<td><p>庚戌</p></td>
<td><p>辛亥</p></td>
</tr>
<tr class="odd">
<td><p>民國紀年</p></td>
<td><p>一一一年</p></td>
<td><p>一一二年</p></td>
<td><p>一一三年</p></td>
<td><p>一一四年</p></td>
<td><p>一一五年</p></td>
<td><p>一一六年</p></td>
<td><p>一一七年</p></td>
<td><p>一一八年</p></td>
<td><p>一一九年</p></td>
<td><p>一二〇年</p></td>
</tr>
<tr class="even">
<td><p>日本紀年</p></td>
<td><p><a href="../Page/令和.md" title="wikilink">令和</a>4年</p></td>
<td><p>5年</p></td>
<td><p>6年</p></td>
<td><p>7年</p></td>
<td><p>8年</p></td>
<td><p>9年</p></td>
<td><p>10年</p></td>
<td><p>11年</p></td>
<td><p>12年</p></td>
<td><p>13年</p></td>
</tr>
<tr class="odd">
<td><p>西元紀年</p></td>
<td><p>2022</p></td>
<td><p>2023</p></td>
<td><p>2024</p></td>
<td><p>2025</p></td>
<td><p>2026</p></td>
<td><p>2027</p></td>
<td><p>2028</p></td>
<td><p>2029</p></td>
<td><p>2030</p></td>
<td><p>2031</p></td>
</tr>
</tbody>
</table>

### 民國前

民國前的紀年以負數形式加上1912即為西元紀年。

| 民國紀年 | 前一年                              | 前二年  | 前三年  | 前四年                              | 前五年  | 前六年  | 前七年  | 前八年  | 前九年  | 前十年  |
| ---- | -------------------------------- | ---- | ---- | -------------------------------- | ---- | ---- | ---- | ---- | ---- | ---- |
| 清朝紀年 | [宣統](../Page/宣統.md "wikilink")3  | 2    | 元    | [光緒](../Page/光緒.md "wikilink")34 | 33   | 32   | 31   | 30   | 29   | 28   |
| 干支紀年 | 辛亥                               | 庚戌   | 己酉   | 戊申                               | 丁未   | 丙午   | 乙巳   | 甲辰   | 癸卯   | 壬寅   |
| 日本紀年 | [明治](../Page/明治.md "wikilink")44 | 43   | 42   | 41                               | 40   | 39   | 38   | 37   | 36   | 35   |
| 西元紀年 | 1911                             | 1910 | 1909 | 1908                             | 1907 | 1906 | 1905 | 1904 | 1903 | 1902 |
| 民國紀年 | 前十一年                             | 前十二年 | 前十三年 | 前十四年                             | 前十五年 | 前十六年 | 前十七年 | 前十八年 | 前十九年 | 前二十年 |
| 清朝紀年 | 光緒27                             | 26   | 25   | 24                               | 23   | 22   | 21   | 20   | 19   | 18   |
| 干支紀年 | 辛丑                               | 庚子   | 己亥   | 戊戌                               | 丁酉   | 丙申   | 乙未   | 甲午   | 癸巳   | 壬辰   |
| 日本紀年 | 明治34                             | 33   | 32   | 31                               | 30   | 29   | 28   | 27   | 26   | 25   |
| 西元紀年 | 1901                             | 1900 | 1899 | 1898                             | 1897 | 1896 | 1895 | 1894 | 1893 | 1892 |

## 同期存在的其他政權之紀年

  - [宣統](../Page/宣統.md "wikilink")（[1909年](../Page/1909年.md "wikilink")1月22日－[1912年](../Page/1912年.md "wikilink")2月12日；[1917年](../Page/1917年.md "wikilink")7月1日－1917年7月12日）：[清宣統帝](../Page/清朝.md "wikilink")[溥儀之](../Page/溥儀.md "wikilink")[年號](../Page/年號.md "wikilink")。
  - [洪宪](../Page/洪宪.md "wikilink")（[1916年](../Page/1916年.md "wikilink")1月1日－1916年3月22日）：[中华帝国皇帝](../Page/中华帝国.md "wikilink")[袁世凯之年號](../Page/袁世凯.md "wikilink")。
  - [共戴](../Page/共戴.md "wikilink")（[1911年](../Page/1911年.md "wikilink")12月16日—[1915年](../Page/1915年.md "wikilink")6月9日；[1921年](../Page/1921年.md "wikilink")2月21日—[1924年](../Page/1924年.md "wikilink")11月26日）:[外蒙古](../Page/外蒙古.md "wikilink")[八世哲布尊丹巴之年號](../Page/第八世哲布尊丹巴呼圖克圖.md "wikilink")。
  - [通志](../Page/通志_\(年号\).md "wikilink")（1915年）：民国初年[察都稱帝使用之年號](../Page/察都.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")（[1868年](../Page/1868年.md "wikilink")1月25日—1912年7月30日）：[大日本帝國](../Page/大日本帝國.md "wikilink")[明治天皇之年號](../Page/明治天皇.md "wikilink")、[日治時台灣](../Page/台灣日治時期.md "wikilink")（1895年－）及朝鮮半島（1910年－）使用的年號。
  - [大正](../Page/大正.md "wikilink")（1912年7月30日—[1926年](../Page/1926年.md "wikilink")12月24日）：大日本帝國[大正天皇之年號](../Page/大正天皇.md "wikilink")、日治時台灣及朝鮮半島使用的年號。
  - [昭和](../Page/昭和.md "wikilink")（1926年12月26日—[1989年](../Page/1989年.md "wikilink")1月7日）：大日本帝国[昭和天皇](../Page/昭和天皇.md "wikilink")（－[1947年](../Page/1947年.md "wikilink")5月2日）、[日本國昭和天皇](../Page/日本.md "wikilink")（1947年5月3日—）、日治台灣及朝鮮半島時（—[1945年](../Page/1945年.md "wikilink")）使用的年號。
  - [平成](../Page/平成.md "wikilink")（[1989年](../Page/1989年.md "wikilink")1月8日—預定[2019年](../Page/2019年.md "wikilink")4月30日）：[日本天皇](../Page/日本天皇.md "wikilink")[明仁之年號](../Page/明仁.md "wikilink")。
  - [令和](../Page/令和.md "wikilink")（預定[2019年](../Page/2019年.md "wikilink")5月1日—）：準[日本天皇](../Page/日本天皇.md "wikilink")[德仁之年號](../Page/德仁.md "wikilink")。
  - [大同](../Page/大同_\(滿洲\).md "wikilink")（[1932年](../Page/1932年.md "wikilink")3月9日—[1934年](../Page/1934年.md "wikilink")2月28日）：[滿洲國之年號](../Page/滿洲國.md "wikilink")。
  - [康德](../Page/康德_\(滿洲\).md "wikilink")（1934年3月1日—1945年8月18日）：[滿洲帝國之年號](../Page/满洲国.md "wikilink")。
  - [檀君紀元](../Page/檀君紀元.md "wikilink")（？－1948年8月14日；1948年9月25日—[1961年](../Page/1961年.md "wikilink")12月31日）：[大韓民國之年號](../Page/大韓民國.md "wikilink")。
  - [大韩民国](../Page/大韓民國_\(紀年\).md "wikilink")（[1948年](../Page/1948年.md "wikilink")8月15日—1948年9月24日）：大韓民國之紀年，由1919年起計算。
  - [主體](../Page/主體曆.md "wikilink")（[1997年](../Page/1997年.md "wikilink")9月9日—）：[朝鲜民主主义人民共和国之紀年](../Page/朝鲜民主主义人民共和国.md "wikilink")，由1912年起计算，1997年使用。
  - [維新](../Page/維新_\(阮朝\).md "wikilink")（[1907年](../Page/1907年.md "wikilink")—1916年）：[越南](../Page/越南.md "wikilink")[阮朝](../Page/阮朝.md "wikilink")[阮福晃之年號](../Page/阮福晃.md "wikilink")。
  - [啟定](../Page/啟定.md "wikilink")（1916年—[1925年](../Page/1925年.md "wikilink")）：越南阮朝[阮福晙之年號](../Page/阮福晙.md "wikilink")。
  - [保大](../Page/保大_\(阮朝\).md "wikilink")（1926年—1945年9月2日）：越南阮朝[阮福晪之年號](../Page/阮福晪.md "wikilink")。

## 因民國紀年而衍生的問題

[民國百年蟲](../Page/民國百年蟲.md "wikilink") -
與[2000年問題一樣](../Page/2000年問題.md "wikilink")，影響使用民國紀年的電腦系統或軟件。

## 参考文献

## 參見

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - [关于中华人民共和国国都、纪年、国歌、国旗的决议](../Page/关于中华人民共和国国都、纪年、国歌、国旗的决议.md "wikilink")
  - [主体纪年](../Page/主体纪年.md "wikilink")

{{-}}

[Category:曆法](../Category/曆法.md "wikilink")
[纪](../Category/紀元.md "wikilink")
[Category:中華民國官方文件](../Category/中華民國官方文件.md "wikilink")
[+](../Category/中华民国历史.md "wikilink")
[Category:台灣戰後時期](../Category/台灣戰後時期.md "wikilink")

1.  [民主進步黨](https://www.dpp.org.tw/)
2.  《青年日報》同時也使用農曆紀年
3.  [每日報刊](http://www.taiwantimes.com.tw/active-f.php)，《臺灣時報》
4.