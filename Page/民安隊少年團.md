**民安隊少年團**（[英文](../Page/英文.md "wikilink")：**C**ivil **A**id **S**ervice
**C**adet
**C**orps，[縮寫](../Page/縮寫.md "wikilink")：**CASCC**）成立於[1968年](../Page/1968年.md "wikilink")，是[民眾安全服務隊架構下的一部分](../Page/民眾安全服務隊.md "wikilink")，隊員年齡由12至17歲，是香港的其中一個青少年制服團體。少年團為團員提供[訓練](../Page/訓練.md "wikilink")[課程](../Page/課程.md "wikilink")、[活動](../Page/活動.md "wikilink")、社會服務，例如[人群管理及郊野](../Page/人群管理.md "wikilink")[巡邏等](../Page/巡邏.md "wikilink")。
現任少年團總指揮為陳振興先生,
[JP](../Page/JP.md "wikilink")。民安隊少年團是香港特別行政區政府民眾安全服務隊管理的一個青少年制服
團體，由20個分隊組群組成，分佈港九新界各區。團員所有的制服及裝備均由民安隊免費借用。少年團透過安排各種活動及紀律訓練，培育青少年的良好品德及在德、智、體、群各方面作全面發展，成為良好公民。

年滿12歲又未滿16歲的青少年可以申請加入民安隊少年團。當少年團團員年滿18歲時，其少年團員身份會被終止，即退伍。

## 歷史

1967年，香港發生了[六七暴動](../Page/六七暴動.md "wikilink")。由於在暴動之中，有滋事分子上街搗亂及大量[土製炸彈的出現](../Page/土製炸彈.md "wikilink")，使到不少學校被逼停課，有部分青少年亦上街參與社會運動及擾亂社會秩序。翌年，[香港政府在民眾安全服務隊的架構中加入少年團](../Page/香港殖民地時期#香港政府.md "wikilink")，目的是要為了讓青少年將課餘時間貢獻[香港社會](../Page/香港社會.md "wikilink")，不上街參與社會運動及擾亂社會秩序。

成立初期，全香港只有兩個支隊：[筲箕灣支隊及](../Page/筲箕灣.md "wikilink")[黃大仙支隊](../Page/黃大仙.md "wikilink")，到後來，民安隊少年團架構不斷擴大，支隊亦越來越多。支隊名稱亦由本來以地區命名改為以英文及數字命名，例如上述的筲箕灣支隊現稱作A4分隊組群，而黃大仙支隊則為B3分隊組群。

## 架構

民安隊少年團由少年團總指揮(現時為陳振興先生)直接指揮，並且向高級助理處長(少年事務)(現時為彭偉昌先生)負責。屬下設一個少年團總部負責統籌整個少年團的行政工作，由民眾安全服務處行動及訓練組
(少年團)
協助日常工作。1999年10月民眾安全服務隊改變組織後分為4個中隊（Company），2018年7月1日起更改為5個中隊。每個中隊下設四個分隊組群（Platoon
Group）：

  - [港島南](../Page/港島南.md "wikilink")，[將軍澳](../Page/將軍澳.md "wikilink")，[秀茂坪及](../Page/秀茂坪.md "wikilink")[港島東屬A中隊](../Page/港島東.md "wikilink")，細分為CA1、CA2、CA3及CA4四個分隊組群;
  - [油麻地](../Page/油麻地.md "wikilink")，[觀塘](../Page/觀塘.md "wikilink")，[黃大仙及](../Page/黃大仙.md "wikilink")[紅磡屬於B中隊](../Page/紅磡.md "wikilink")，細分為CB1、CB2、CB3及CB4四個分隊組群;
  - [新界北](../Page/新界北.md "wikilink")，[沙田](../Page/沙田.md "wikilink")，[馬鞍山及](../Page/馬鞍山.md "wikilink")[大埔屬於C中隊](../Page/大埔.md "wikilink")，細分為CC1、CC2、CC3及CC4四個分隊組群;
  - [元朗](../Page/元朗.md "wikilink")，[屯門北](../Page/屯門.md "wikilink")，[屯門南及](../Page/屯門南.md "wikilink")[天水圍屬於D中隊](../Page/天水圍.md "wikilink")，細分為CD1、CD2、CD3及CD4四個分隊組群;
  - [荃灣](../Page/荃灣.md "wikilink")，[葵青](../Page/葵青.md "wikilink")，[東涌及](../Page/東涌.md "wikilink")[深水埗則屬於E中隊](../Page/深水埗.md "wikilink")，細分為CE1、CE2、CE3及CE4四個分隊組群

### 中隊內容

  - 每中隊由一名中隊指揮官“Company Commander”（OC）（高五級長官 Grade SV Officer）(俗稱風車)負責；
  - 每分隊組群由一名分隊組群指揮官“Platoon Group Commander”（GC） (五級長官 Grade V
    Officer)(俗稱三粒花)負責；
  - 而每一個分隊組群再分為4個分隊（Platoon），合共80個分隊，各由一位分隊指揮官“Platoon Commander”
    （PC）(四級長官 Grade IV Officer)(俗稱一粒花或兩粒花) 指揮。

## 遴選培訓

投考人需要年滿12歲、未滿16歲；持有[香港身份證](../Page/香港身份證.md "wikilink")；能夠操流利的[粵語及閱讀](../Page/粵語.md "wikilink")[中文](../Page/中文.md "wikilink")；獲得[家長或](../Page/家長.md "wikilink")[監護人書面同意](../Page/監護人.md "wikilink")；填妥及交回入隊申請書後，民眾安全服務隊會作出初步的甄選，其後會安排[面試及體格檢驗](../Page/面試.md "wikilink")。通過面試及體格測驗的少年將會接受4堂啟導訓練課程(CIP)。完成後，將會被派遣到就近住所的學校或埸地接受訓練，訓練時間逢週六約下午3至5；另外，每月也有一天為[香港青年獎勵計劃運動日](../Page/AYP.md "wikilink")。

## 階級

| [中文名稱](../Page/中文名.md "wikilink") | [英文名稱](../Page/英文名.md "wikilink") | [肩章](../Page/肩章.md "wikilink") | 縮寫  |
| --------------------------------- | --------------------------------- | ------------------------------ | --- |
| 高級少年領袖                            | Senior Cadet Leader               | 三柴半                            | SCL |
|                                   |                                   |                                |     |
| 少年領袖                              | Cadet Leader                      | 三柴                             | CL  |
|                                   |                                   |                                |     |
| 副少年領袖                             | Deputy Cadet Leader               | 二柴                             | DCL |
|                                   |                                   |                                |     |
| 高級團員                              | Senior Cadet                      | 一柴                             | SC  |
|                                   |                                   |                                |     |
| 團員                                | Cadet                             | 白板                             | C   |
|                                   |                                   |                                |     |

## 「傑出少年團團員」選舉

「傑出少年團團員」選舉(Outstanding Cadet
Selection)於1970年首次舉行，以表揚在各方面均有傑出表現的少年團團員，並為其他少年團團員樹立典範。所有傑出少年團團員均有機會參與民安隊少年團宣傳及交流活動，藉此擴闊視野，增廣見聞。每位傑出少年團團員將獲頒發獎盃/獎座及證書。每年，最多有10名少年團團員獲選為「傑出少年團團員」。

### 甄選程序

  - 初選：由每區的分隊長官會根據該區少年團團員的表現，提名最多5位少年團團員。獲挑選的少年團團員，可以前往中隊進行複選。
  - 複選：通過初選之候選人必須出席複選，並經由中隊長官，從20名初選成功的少年團團員當中，挑選表現優秀的10名前往最後評選。
  - 最後評選：進入最後評選之候選人將獲社會知名人士、青少年∕社區組織代表及贊助單位代表組成的外界評審委員會接見，從而選出該年度之「傑出少年團團員」。

## 參見

  - [民眾安全服務隊](../Page/民眾安全服務隊.md "wikilink")
  - [民安隊少年團花式單車表演隊](../Page/民安隊少年團花式單車表演隊.md "wikilink")

[Category:民眾安全服務隊](../Category/民眾安全服務隊.md "wikilink")