**民族独立运动**，特指15世纪末期[殖民主义在全世界传播以后](../Page/殖民主义.md "wikilink")，被殖民民族反抗[欧洲](../Page/欧洲.md "wikilink")，以及后来的[北美洲](../Page/北美洲.md "wikilink")、[亚洲殖民帝国的行为](../Page/亚洲.md "wikilink")，该独立运动激发了[欠发达国家以及未建立](../Page/欠发达国家.md "wikilink")[国家体制的地区的](../Page/国家.md "wikilink")[民族意识](../Page/民族意识.md "wikilink")，最终使得这些国家和地区演变为[第三世界](../Page/第三世界.md "wikilink")。

[19世纪美洲独立运动](../Page/19世纪美洲独立运动.md "wikilink")

1791年，[海地爆发了反对](../Page/海地.md "wikilink")[宗主国](../Page/宗主国.md "wikilink")[法国的起义](../Page/法国.md "wikilink")，虽然被镇压，但影响深远。

1804年，[海地再次爆发](../Page/海地.md "wikilink")[独立运动](../Page/独立运动.md "wikilink")，发表[海地独立宣言](../Page/海地独立宣言.md "wikilink")，成为[拉丁美洲第一个独立国家以及近代第一个黑人建立的共和国](../Page/拉丁美洲.md "wikilink")。

此后，拉丁美洲各地区纷纷发起武装暴动，与殖民军作战。

[大哥伦比亚](../Page/大哥伦比亚.md "wikilink")（今[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[委内瑞拉](../Page/委内瑞拉.md "wikilink")、[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")、[巴拿马](../Page/巴拿马.md "wikilink")）于1810年独立。

1810年9月16日，[墨西哥发生](../Page/墨西哥.md "wikilink")[多洛雷斯呼声事件](../Page/多洛雷斯呼声.md "wikilink")，成为其**民族独立运动**的起点。

1816年，[拉普拉塔联合省](../Page/拉普拉塔联合省.md "wikilink")（今[阿根廷](../Page/阿根廷.md "wikilink")）独立。同年，[圣马丁从](../Page/圣马丁.md "wikilink")[拉普拉塔联合省进军](../Page/拉普拉塔联合省.md "wikilink")[智利](../Page/智利.md "wikilink")。

1817年，[智利独立](../Page/智利.md "wikilink")。

1819年，[玻利瓦尔的军队在](../Page/玻利瓦尔.md "wikilink")[波亚卡击败](../Page/波亚卡战役.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[殖民军](../Page/殖民军.md "wikilink")，是为[拉丁美洲独立运动爆发以来最大的军事胜利](../Page/拉丁美洲.md "wikilink")。

1821年，[秘鲁](../Page/秘鲁.md "wikilink")、[墨西哥独立](../Page/墨西哥.md "wikilink")。

1822年，[巴西独立](../Page/巴西.md "wikilink")，由[葡萄牙](../Page/葡萄牙.md "wikilink")[王室成员](../Page/王室.md "wikilink")[佩德罗组建](../Page/佩德罗.md "wikilink")[巴西帝国](../Page/巴西帝国.md "wikilink")，[佩德罗成为](../Page/佩德罗.md "wikilink")[巴西皇帝](../Page/巴西皇帝.md "wikilink")[佩德罗一世](../Page/佩德罗一世.md "wikilink")。

1824年，[阿亚库巧战役](../Page/阿亚库巧战役.md "wikilink")。

1823年，[危地马拉](../Page/危地马拉.md "wikilink")、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[哥斯达黎加独立](../Page/哥斯达黎加.md "wikilink")，联手组建[中美联合省](../Page/中美联合省.md "wikilink")。

1825年，[上秘鲁](../Page/上秘鲁.md "wikilink")（今[玻利维亚](../Page/玻利维亚.md "wikilink")）独立。

1838年，[中美联合省分裂为](../Page/中美联合省.md "wikilink")5国。

1844年，[多米尼加独立](../Page/多米尼加.md "wikilink")。[拉丁美洲](../Page/拉丁美洲.md "wikilink")[独立运动暂告一段落](../Page/独立运动.md "wikilink")。

[20世纪非洲独立运动](../Page/20世纪非洲独立运动.md "wikilink")

[苏丹](../Page/苏丹.md "wikilink")：1881年爆发[马赫迪起义](../Page/马赫迪起义.md "wikilink")，反抗[埃及与](../Page/埃及.md "wikilink")[英国对其殖民统治](../Page/英国.md "wikilink")。1885年[喀土穆战役后占领](../Page/喀土穆战役.md "wikilink")[英国](../Page/英国.md "wikilink")[苏丹](../Page/苏丹.md "wikilink")[总督府](../Page/总督府.md "wikilink")，击毙[总督](../Page/总督.md "wikilink")[戈登](../Page/戈登.md "wikilink")。1900年，起义失败。

[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")：1896年在[阿杜瓦战役中击败](../Page/阿杜瓦战役.md "wikilink")[意大利军队](../Page/意大利.md "wikilink")，成为近代非洲国家独立前唯一一次战胜欧洲人的大型战役。

[南非](../Page/南非.md "wikilink")：[布尔战争](../Page/布尔战争.md "wikilink")，[荷兰裔](../Page/荷兰裔.md "wikilink")[南非人](../Page/南非人.md "wikilink")（[布尔人](../Page/布尔人.md "wikilink")）与入侵的[英国](../Page/英国.md "wikilink")[开普殖民地军队作战](../Page/开普殖民地.md "wikilink")（19世纪初——1902年），最终失败。英国于1910年建立[南非联邦](../Page/南非联邦.md "wikilink")。

[亚洲独立运动](../Page/亚洲独立运动.md "wikilink")

## 参见

  - [明治维新](../Page/明治维新.md "wikilink")
  - [美西战争](../Page/美西战争.md "wikilink")
  - [不合作运动](../Page/不合作运动.md "wikilink")、[三一义兵运动](../Page/三一义兵运动.md "wikilink")
  - [五四运动](../Page/五四运动.md "wikilink")
  - [中国抗日战争](../Page/中国抗日战争.md "wikilink")
  - [越南八月革命](../Page/越南八月革命.md "wikilink")
  - [印度尼西亚八月革命](../Page/印度尼西亚八月革命.md "wikilink")
  - [印度支那独立战争](../Page/印度支那独立战争.md "wikilink")
  - [印巴分治](../Page/印巴分治.md "wikilink")
  - [蒙巴顿方案](../Page/蒙巴顿方案.md "wikilink")
  - [不结盟运动](../Page/不结盟运动.md "wikilink")
  - [七十七国集团](../Page/七十七国集团.md "wikilink")
  - [亚非会议](../Page/亚非会议.md "wikilink")

[M](../Page/category:主权运动.md "wikilink")
[M](../Page/category:民族主义.md "wikilink")