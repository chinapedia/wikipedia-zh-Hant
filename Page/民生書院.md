[Munsang_College_2012.jpg](https://zh.wikipedia.org/wiki/File:Munsang_College_2012.jpg "fig:Munsang_College_2012.jpg")

**民生書院**（，MSC）是創校於1926年，屬[英文中學](../Page/香港英文授課中學.md "wikilink")，是[香港少數擁有幼稚園](../Page/香港.md "wikilink")、小學及中學的一條龍[男女學校](../Page/混合性別教育.md "wikilink")，也是少數多於90年歷史，經歷[香港日佔時期的學校](../Page/香港日佔時期.md "wikilink")。民生書院是一所位於[九龍城區的](../Page/九龍城區.md "wikilink")[英文中學](../Page/香港英文授課中學.md "wikilink")，為該區第一所津貼中學。

校園佔地16,800平方米，擁有標準操場、標準跑道、標準足球場、標準籃球場、室內運動場及室外標準游泳池等，設備非常完善。學校以[基督教為學校](../Page/基督教.md "wikilink")[宗教信仰](../Page/宗教.md "wikilink")；但不隸屬於任何宗教團體，中學部是香港教育局特許的114間[英文授課中學之一](../Page/英文授課中學.md "wikilink")。幼稚園更為全球三所獲[福祿貝爾認証的幼稚園之一](../Page/福祿貝爾.md "wikilink")。

現將改建E座（黃映然紀念樓），而H座（新翼大樓）的新樓梯將於2017年的畢業禮前正式啟用。E座（黃映然紀念樓）的改建工程預計將於2019年完工，新禮堂的工程預計將於2023年完工。屆時將會有更多新課室及多用途室、一座新禮堂、全天候標準室内游泳池、新圖書館、標準室内籃球場和羽毛球場等惠及學生的新設施。現在校方正積極籌款，集資興建這些設施。

## 創校歷史

民生書院最初由紳商[曹善允提倡興建](../Page/曹善允.md "wikilink")。1916年曹善允與其他商人組成之啟德營業有限公司正著發展[九龍寨城對開的](../Page/九龍寨城.md "wikilink")[九龍灣北岸地帶](../Page/九龍灣.md "wikilink")。曹善允有感當時[九龍城居民缺乏接受](../Page/九龍城.md "wikilink")[西學的機會](../Page/西學.md "wikilink")，於是開始積極籌款興學。後獲得[啟德公司創辦人之一](../Page/啟德公司.md "wikilink")[區德](../Page/區德.md "wikilink")（又名區澤民）逝世後從遺囑餽贈的10,000港元，及與曹善允同是[啟德公司董事的](../Page/啟德公司.md "wikilink")[莫幹生捐出](../Page/莫幹生.md "wikilink")10,000港元。
在籌得足夠資金,
並得到當時[聖保羅書院校長](../Page/聖保羅書院.md "wikilink")[史超域牧師協助下](../Page/史超域.md "wikilink")，學校在1926年於[啟德濱創辦](../Page/啟德濱.md "wikilink")，首任校長為[黃映然先生](../Page/黃映然.md "wikilink")。

為紀念區德（[區澤民](../Page/區澤民.md "wikilink")）和[莫幹生兩人對創校的貢獻](../Page/莫幹生.md "wikilink")，學校遂取名為民生書院。曹善允獲推舉終身出任校董會主席。學校初期租用[民國政治家](../Page/民國.md "wikilink")[伍朝樞的物業作為校址](../Page/伍朝樞.md "wikilink")，至1939年再遷到九龍城[東寶庭道繼續辦學](../Page/東寶庭道.md "wikilink")。學校於1941-1945年被[日軍侵佔](../Page/香港日佔時期.md "wikilink")，直至1945年[香港重光後恢復辦學](../Page/香港重光.md "wikilink")。

在1999年，[港島民生書院](../Page/港島民生書院.md "wikilink")（Hong Kong Island Munsang
College, iMSC）正式成立。

## 宗旨校訓

  - 民生書院校訓為「光與生命」'Light and Life' 及「人人為我，我為人人」'All for One, One for
    All'。
  - 以基督精神，辦全人教育，引導學生學習耶穌基督完美的人生典範。
  - 重德、智、體、群、美五育發展。
  - 由學生自己去領導各社（德、智、體、群、美及愛）、各學會，俾能發揮領袖才幹、獨立處事能力，及為社會服務的抱負。

## 校園相冊

[File:Munsangentrance.JPG|民生書院正門](File:Munsangentrance.JPG%7C民生書院正門)
[File:Munsangcollege2.JPG|鑽禧紀念樓](File:Munsangcollege2.JPG%7C鑽禧紀念樓)
林子豐紀念樓 <File:Munsang> LimPorYenHall.JPG|林百欣堂
林百欣樓 <File:Munsangcollege> campuses.JPG|林百欣樓
黃映然紀念樓
新翼大樓

## 班級課程

### 幼稚園

幼稚園部共開設30班（幼兒班、幼低班及幼高班各10班），學生人數約為990人，全體教師80位，包括外籍英文老師17位及普通話老師6位。

### 小學

小學部為全日制，共開設36班（小一6班 小二、小三各7班 小四至小五級各6班 小六5班），學生人數約為1400人，教師人數61人。

### 中學

  - 中學部於2018/19年度共開設30班（中一至中六各5班），學生人數為1000人，教師人數為66人。中一至中三設有兩班精英班(A班及B班)。\[1\]

中學及小學每個課節循環有6個上學日。
\*英國語文

  - 中國語文
  - 數學
  - 通識教育
  - 綜合科學（中三級除外）
  - 生物（只限中三級）
  - 化學（只限中三級）
  - 物理（只限中三級）
  - 地理
  - 歷史
  - 中國歷史
  - 商業與經濟 （只限中三級）
  - 聖經
  - 普通話（除了中二級屬精英班的A班）
  - 音樂
  - 視覺藝術
  - 電腦
  - 體育


必須修讀之科目

  - 英國語文
  - 中國語文
  - 數學 （必修部份）
  - 通識教育
  - 聖經
  - 體育
  - 音樂
  - 視覺藝術（非選修科目）


可選修之科目

  - 生物
  - 化學
  - 物理
  - 資訊及通訊科技
  - 經濟
  - 地理
  - 中國歷史
  - 歷史
  - 企業、會計與財務概論
  - 視覺藝術


必須修讀之科目

  - 英國語文
  - 中國語文
  - 數學 （必修部份）
  - 通識教育
  - 聖經
  - 體育


可選修之科目

  - 數學 （延伸單元二）
  - 生物
  - 化學
  - 物理
  - 資訊及通訊科技
  - 經濟
  - 地理
  - 中國歷史
  - 歷史
  - 企業、會計與財務概論
  - 視覺藝術


必須修讀之科目

  - 英國語文
  - 中國語文
  - 數學 （必修部份）
  - 通識教育
  - 體育


可選修之科目

  - 數學 （延伸單元一）
  - 數學 （延伸單元二）
  - 生物
  - 化學
  - 物理
  - 資訊及通訊科技
  - 經濟
  - 地理
  - 中國歷史
  - 歷史
  - 企業、會計與財務概論
  - 視覺藝術


必須修讀之科目

  - 英國語文
  - 中國語文
  - 數學
  - 聖經
  - 體育


可選修之科目

  - 附加數學(M1 及 M2)
  - 生物
  - 化學
  - 物理
  - 地理
  - 歷史
  - 中國歷史
  - 經濟
  - 會計學原理
  - 電腦及資訊科技


必須修讀之科目

  - 英語運用
  - 中國語文及文化
  - 聖經
  - 體育


可選修之科目

  - 純粹數學
  - 生物
  - 化學
  - 物理
  - 地理
  - 歷史
  - 中國歷史
  - 經濟
  - 會計學原理
  - 數學及統計學
  - 通識教育

## 公開考試佳績

2017年DSE文憑試放榜中，該校出產一位狀元。

## 校友名人

民生書院創校多年，歷史悠久，多年來在社會各個領域名人輩出。

### 法學

  - [吳少鵬](../Page/吳少鵬.md "wikilink")：吳少鵬律師事務所國際律師
  - [顧愷仁](../Page/顧愷仁.md "wikilink")：[中銀香港律師](../Page/中銀香港.md "wikilink")
  - [鄭會圻](../Page/鄭會圻.md "wikilink")：[香港調解學院院長](../Page/香港調解學院.md "wikilink")
  - [張達明](../Page/張達明_\(教授\).md "wikilink")：[香港大學法律學院首席講師](../Page/香港大學法律學院.md "wikilink")\[2\]

### 醫學

  - [林樹基](../Page/林樹基.md "wikilink")：[香港浸信會醫院創辦人](../Page/香港浸信會醫院.md "wikilink")
  - [鄧兆華](../Page/鄧兆華.md "wikilink")：[香港大學李嘉誠醫學院精神醫學系系主任](../Page/香港大學李嘉誠醫學院.md "wikilink")
  - [張寬耀](../Page/張寬耀.md "wikilink")：著名腫瘤專科醫生

### 建築

  - [梁志天](../Page/梁志天.md "wikilink")：香港著名建築及室內設計師

### 學術教育

  - [黃麗松](../Page/黃麗松.md "wikilink")：前[新加坡國立大學](../Page/新加坡國立大學.md "wikilink")[南洋大學校長](../Page/南洋大學.md "wikilink")、前[香港大學校長](../Page/香港大學.md "wikilink")
  - [林思顯](../Page/林思顯.md "wikilink")：前[香港浸會大學校董會主席](../Page/香港浸會大學.md "wikilink")、前[嘉華銀行董事局主席](../Page/嘉華銀行.md "wikilink")
  - [夏永豪](../Page/夏永豪.md "wikilink")：前[聖保羅書院校長](../Page/聖保羅書院.md "wikilink")、前[立法局議員](../Page/香港立法會.md "wikilink")
  - [郁德芬](../Page/郁德芬.md "wikilink")：[香港基督教青年會](../Page/香港基督教青年會.md "wikilink")（YMCA）總經理
  - [陳偉光](../Page/陳偉光（音樂系教授）.md "wikilink")：[香港中文大學音樂系教授](../Page/香港中文大學.md "wikilink")、[崇基學院前院長](../Page/崇基學院.md "wikilink")
  - [張百康](../Page/張百康.md "wikilink")：前[香港考試及評核局副主席](../Page/香港考試及評核局.md "wikilink")、前[港島民生書院校長](../Page/港島民生書院.md "wikilink")
  - [嚴志成](../Page/嚴志成.md "wikilink")：[港島民生書院校長](../Page/港島民生書院.md "wikilink")、[聖公會陳融中學校長](../Page/聖公會陳融中學.md "wikilink")
  - [黃觀貴](../Page/黃觀貴.md "wikilink")：[香港高級程度會考](../Page/香港高級程度會考.md "wikilink")「現代地理學導論」作者
  - [魏樹昭](../Page/魏樹昭.md "wikilink")：前[中華傳道會安柱中學校長](../Page/中華傳道會安柱中學.md "wikilink")
  - [林向成](../Page/林向成.md "wikilink")：[保良局馮晴紀念小學校長](../Page/保良局馮晴紀念小學.md "wikilink")

### 工商金融

  - [林百欣](../Page/林百欣.md "wikilink")：慈善家、前[麗新集團主席](../Page/麗新集團.md "wikilink")、前[亞洲電視主席](../Page/亞洲電視.md "wikilink")
  - [林建岳](../Page/林建岳.md "wikilink")：[林百欣二子](../Page/林百欣.md "wikilink")，[麗新集團主席](../Page/麗新集團.md "wikilink")、[香港旅遊發展局主席](../Page/香港旅遊發展局.md "wikilink")
  - [林森池](../Page/林森池.md "wikilink")：[美林證券香港副總裁](../Page/美林證券.md "wikilink")
  - [張賽娥](../Page/張賽娥.md "wikilink")：[香港南華集團創辦人及董事](../Page/香港南華集團.md "wikilink")、金融評論家
  - [王維基](../Page/王維基.md "wikilink")：[香港電視網絡主席](../Page/香港電視網絡.md "wikilink")
  - [莊偉茵](../Page/莊偉茵.md "wikilink")：[中華電力企業發展總裁](../Page/中華電力.md "wikilink")
  - [姚天從](../Page/姚天從.md "wikilink")：[摩托羅拉半導體資深副總裁及亞太區總經理](../Page/摩托羅拉半導體.md "wikilink")
  - [胡民勝](../Page/胡民勝.md "wikilink")：[香港酒店業協會總幹事](../Page/香港酒店業協會.md "wikilink")
  - [楊主光](../Page/楊主光.md "wikilink")：[香港寬頻執行副主席](../Page/香港寬頻.md "wikilink")、前行政總裁

### 政治公眾事業

  - [林思齊](../Page/林思齊.md "wikilink")，[OC](../Page/加拿大勳章.md "wikilink")，[CVO](../Page/皇家維多利亞勳章.md "wikilink")，：前[加拿大](../Page/加拿大.md "wikilink")[卑詩省](../Page/卑詩省.md "wikilink")
  - [張樹榮](../Page/張樹榮.md "wikilink")：[香港科技園公司企業拓展及科技支援副總裁](../Page/香港科技園.md "wikilink")
  - [何安達](../Page/何安達.md "wikilink")：前[特首辦新聞統籌專員](../Page/香港特別行政區行政長官辦公室.md "wikilink")
  - [李卓人](../Page/李卓人.md "wikilink")：前[立法會議員](../Page/第五屆香港立法會.md "wikilink")（新界西）、[工黨主席](../Page/工黨_\(香港\).md "wikilink")、[支聯會主席](../Page/香港市民支援愛國民主運動聯合會.md "wikilink")
  - [鄭和強](../Page/鄭和強.md "wikilink")：[澳門特別行政區政府](../Page/澳門特別行政區政府.md "wikilink")[旅遊局前香港代表](../Page/旅遊局.md "wikilink")

### 文藝與音樂

  - [曾智斌](../Page/曾智斌.md "wikilink")：[維也納兒童合唱團駐團指揮](../Page/維也納兒童合唱團.md "wikilink")、[泛亞交響樂團首席客席指揮](../Page/泛亞交響樂團.md "wikilink")
  - [葉詠詩](../Page/葉詠詩.md "wikilink")：[香港小交響樂團音樂總監](../Page/香港小交響樂團.md "wikilink")
  - [葉羨詩](../Page/葉羨詩.md "wikilink")：[葉氏兒童音樂實踐中心音樂副總監](../Page/葉氏兒童音樂實踐中心.md "wikilink")
  - [凌顯祐](../Page/凌顯祐.md "wikilink")：[香港管弦樂團首席中提琴](../Page/香港管弦樂團.md "wikilink")

### 餐飲

  - [甄文達](../Page/甄文達.md "wikilink")：國際名廚
  - [Dave Yu](../Page/Dave_Yu.md "wikilink")：米芝蓮三星餐廳名廚

### 體育

  - [謝穎紅](../Page/謝穎紅.md "wikilink")：[香港沙排代表](../Page/香港沙排.md "wikilink")

### 出版新聞

  - [吳子敏](../Page/吳子敏.md "wikilink")：[無綫電視記者](../Page/無綫電視.md "wikilink")
  - [曾熙雯](../Page/曾熙雯.md "wikilink")：[無綫新聞財經女主播及記者](../Page/無綫新聞.md "wikilink")

### 影視娛樂

  - [陳秋霞](../Page/陳秋霞.md "wikilink")：1977年[台灣金馬獎](../Page/台灣金馬獎.md "wikilink")「最佳女主角」
  - [林志美](../Page/林志美.md "wikilink")：1985年[香港電影金像獎](../Page/香港電影金像獎.md "wikilink")「最佳原創電影歌曲」
  - [彭羚](../Page/彭羚.md "wikilink")：1995年[無綫電視](../Page/無綫電視.md "wikilink")[十大勁歌金曲](../Page/十大勁歌金曲.md "wikilink")「最受歡迎女歌星」
  - [向華強](../Page/向華強.md "wikilink")：[中國星集團主席](../Page/中國星集團.md "wikilink")
  - [蕭潮順](../Page/蕭潮順.md "wikilink")：好合拍Wellfit多媒體集團有限公司董事總經理
  - [查小欣](../Page/查小欣.md "wikilink")：[香港商業電台唱片騎師](../Page/香港商業電台.md "wikilink")、節目主持
  - [徐詠芝](../Page/徐詠芝.md "wikilink")：[香港商業電台](../Page/香港商業電台.md "wikilink")「一切從音樂開始」節目主持人
  - [周國豐](../Page/周國豐.md "wikilink")：1980年代[童星](../Page/童星.md "wikilink")、[香港電台新媒體及節目協作總監](../Page/香港電台.md "wikilink")
  - [邱家雄](../Page/邱家雄.md "wikilink")：[無綫電視影視編導](../Page/無綫電視.md "wikilink")、監製
  - [艾威](../Page/艾威.md "wikilink")：[香港電視網絡演員](../Page/香港電視網絡.md "wikilink")
  - [劉愷威](../Page/劉愷威.md "wikilink")：歌手、演員
  - [潘卓楠](../Page/潘卓楠.md "wikilink")：模特兒、男子組合「Sky」成員
  - [張崇基](../Page/張崇基.md "wikilink")：歌手、[華納唱片監製](../Page/華納唱片.md "wikilink")、[亞洲電視音樂總監](../Page/亞洲電視.md "wikilink")
  - [張崇德](../Page/張崇德.md "wikilink")：歌手、[音樂人教室及音樂人製作有限公司總監](../Page/音樂人教室及音樂人製作有限公司.md "wikilink")
  - [陳庭威](../Page/陳庭威.md "wikilink")：演員
  - [羅力威](../Page/羅力威.md "wikilink")：歌手、作曲人
  - [陳思捷](../Page/陳思捷.md "wikilink")：作曲人
  - [王仲傑](../Page/王仲傑.md "wikilink")：填詞人
  - [謝天智](../Page/謝天智.md "wikilink")：填詞人
  - [袁劍偉](../Page/袁劍偉.md "wikilink")：導演，演員[林嘉欣丈夫](../Page/林嘉欣.md "wikilink")
  - [潘梓鋒](../Page/潘梓鋒.md "wikilink")：演員、節目主持
  - [蔡惠萍](../Page/蔡惠萍.md "wikilink")：配音員
  - [林奕華](../Page/林奕華_\(香港\).md "wikilink")：香港著名編劇（小學部）\[3\]

## 參考來源

## 外部連結

  - [民生書院幼稚園部](http://munsang.edu.hk/kindergarten)
  - [民生書院小學部](http://munsang.edu.hk/primary)
  - [民生書院中學部](http://munsang.edu.hk/secondary)
  - [港島民生書院](http://www.imsc.edu.hk/)
  - [OpenSchool民生書院小學介紹](https://www.openschool.hk/school/details/%E5%B0%8F%E5%AD%B8/%E6%B0%91%E7%94%9F%E6%9B%B8%E9%99%A2%E5%B0%8F%E5%AD%B8)

[Category:九龍仔](../Category/九龍仔.md "wikilink")
[M](../Category/九龍城區中學.md "wikilink")
[Category:九龍城區小學](../Category/九龍城區小學.md "wikilink")
[M](../Category/香港幼稚園.md "wikilink")
[Category:1926年創建的教育機構](../Category/1926年創建的教育機構.md "wikilink")
[Category:香港基督教新教學校](../Category/香港基督教新教學校.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.  [Curriculum
    Structure](http://www.munsang.edu.hk/secondary/index.php?option=com_content&view=article&id=61&Itemid=67)
2.
3.