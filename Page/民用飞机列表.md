以下是民航机造商列表，以製造商英文名按照字母順序排列。

## [空中巴士](../Page/空中巴士.md "wikilink")（AirBus）

  - [A300](../Page/空中巴士A300.md "wikilink")
  - [A310](../Page/A310.md "wikilink")
  - [A320系列](../Page/A320.md "wikilink")
      - [A318](../Page/A318.md "wikilink")
      - [A319](../Page/A319.md "wikilink")
      - [A320](../Page/A320.md "wikilink")
      - [A321](../Page/A321.md "wikilink")
  - [A330](../Page/A330.md "wikilink")
  - [A340](../Page/A340.md "wikilink")
  - [A350](../Page/A350.md "wikilink")
  - [A380](../Page/A380.md "wikilink")

##

  - ''

## [安托諾夫設計局](../Page/安托諾夫設計局.md "wikilink")（Antonov）

  - [An-124](../Page/An-124.md "wikilink")
  - [An-225](../Page/An-225.md "wikilink")
  - [An-70](../Page/An-70.md "wikilink")
  - [An-24](../Page/An-24.md "wikilink")

## [ATR](../Page/ATR.md "wikilink")

  - [ATR 42](../Page/ATR_42.md "wikilink")
  - [ATR 72](../Page/ATR_72.md "wikilink")

## [阿弗罗](../Page/阿弗罗.md "wikilink")（Avro）

  - **
  - **
  - **

## [英國飛機公司](../Page/英國飛機公司.md "wikilink")（British Aircraft Corporation）

  - ''[BAC1-11](../Page/BAC1-11.md "wikilink") (One-Eleven)
  - ''[協和飛機](../Page/協和飛機.md "wikilink") (Concorde)

## [英國航太公司](../Page/英國航太公司.md "wikilink")（British Aerospace）

  - [BAe 146](../Page/BAe_146.md "wikilink")

  -
## [波音](../Page/波音.md "wikilink")（Boeing）

  - [波音707](../Page/波音707.md "wikilink")（波音720亦為波音707系列的一個型號）
  - [波音717](../Page/波音717.md "wikilink")（原為[麥克唐納-道格拉斯的MD](../Page/麥克唐納-道格拉斯.md "wikilink")-95）
  - [波音727](../Page/波音727.md "wikilink")（原為麥克唐納-道格拉斯的研發產品）
  - [波音737](../Page/波音737.md "wikilink")
  - [波音747](../Page/波音747.md "wikilink")
  - [波音757](../Page/波音757.md "wikilink")
  - [波音767](../Page/波音767.md "wikilink")
  - [波音777](../Page/波音777.md "wikilink")
  - [波音787](../Page/波音787.md "wikilink")
  - [波音40A](../Page/波音40A.md "wikilink")
  - [波音80](../Page/波音80.md "wikilink")
  - [波音221 Monomail](../Page/波音221_Monomail.md "wikilink")
  - [波音247](../Page/波音247.md "wikilink")
  - [波音307 Stratoliner](../Page/波音307_Stratoliner.md "wikilink")
  - [波音314](../Page/波音314.md "wikilink")
  - [波音377](../Page/波音377.md "wikilink")

## 加拿大[龐巴迪宇航公司](../Page/龐巴迪宇航公司.md "wikilink")（Bombardier Aerospace）

  - [CRJ系列](../Page/CRJ.md "wikilink")
      - CRJ-100/200
      - CRJ-440
      - CRJ-700/705
      - CRJ-900/1000
      - 挑戰者800/850
  - [Dash 8](../Page/Dash_8.md "wikilink") (Q 系列)
  - [C系列](../Page/C.md "wikilink")
      - CS100
      - CS300

## [布里斯托飞机公司](../Page/布里斯托飞机公司.md "wikilink")（Bristol Aeroplane Company）

  - ''

## [賽斯納飛行器公司](../Page/塞斯纳飞行器公司.md "wikilink")

  - [塞斯納208](../Page/塞斯納208.md "wikilink")
  - [塞斯納C-172](../Page/塞斯納C-172.md "wikilink")
  - [塞斯納C-170](../Page/塞斯納C-170.md "wikilink")

## [康維爾](../Page/康維爾.md "wikilink")

  - *[康維爾540](../Page/康維爾540.md "wikilink")*
  - *[康維爾660](../Page/康維爾660.md "wikilink")*
  - *[康維爾880](../Page/康維爾880.md "wikilink")*
  - *[康維爾990](../Page/康維爾990.md "wikilink")*
  - *[康維爾240](../Page/康維爾240.md "wikilink")*
  - *[康維爾340](../Page/康維爾340.md "wikilink")*

## [達梭航太](../Page/達梭航太.md "wikilink")

  - [獵鷹10](../Page/達梭獵鷹10.md "wikilink")
  - [獵鷹20](../Page/達梭獵鷹20.md "wikilink")
  - [獵鷹2000](../Page/達梭獵鷹2000.md "wikilink")
  - [獵鷹50](../Page/達梭獵鷹50.md "wikilink")
  - [獵鷹7X](../Page/達梭獵鷹7X.md "wikilink")
  - [獵鷹8X](../Page/達梭獵鷹8X.md "wikilink")

2015年2月6日，法国波尔多，达索猎鹰8X公务机成功首飞。猎鹰8X是达索最新旗舰级公务机，其竞争对手为同等级然而尺寸更大的湾流G550和庞巴迪环球6000。猎鹰8X是典型的长程公务机，其航程远达6400海里。猎鹰8X下线是在2014年12月17日，首飞时间距此不足一个月，表明猎鹰8X进展相当顺利。

  - [獵鷹900](../Page/達梭獵鷹900.md "wikilink")
  - *[水星](../Page/達梭水星.md "wikilink")*

## [德哈維蘭](../Page/德哈維蘭.md "wikilink")

  - ''[哈維蘭彗星型客機](../Page/哈維蘭彗星型客機.md "wikilink")
  - ''
  - ''
  - ''
  - ''
  - ''
  - [加拿大哈維蘭衝鋒8](../Page/Dash_8.md "wikilink")

##

  - ''[多尼爾228](../Page/多尼爾228.md "wikilink")
  - ''[多尼尔328](../Page/多尼尔328.md "wikilink")

## [道格拉斯飛行器公司](../Page/道格拉斯飛行器公司.md "wikilink")

  - [DC-2](../Page/DC-2.md "wikilink")
  - [DC-3](../Page/DC-3.md "wikilink")
  - [DC-4](../Page/DC-4.md "wikilink")
  - [DC-5](../Page/DC-5.md "wikilink")
  - [DC-6](../Page/DC-6.md "wikilink")
  - [DC-7](../Page/DC-7.md "wikilink")
  - [DC-8](../Page/DC-8.md "wikilink")
  - [DC-9](../Page/DC-9.md "wikilink")
  - [DC-10](../Page/DC-10.md "wikilink")

## [巴西航空工業公司](../Page/巴西航空工業公司.md "wikilink")

  - [ERJ-145系列](../Page/ERJ-145系列.md "wikilink")
      - ERJ-135
      - ERJ-140
      - ERJ-145
      - Legacy 600
  - [ERJ-170系列](../Page/ERJ-170系列.md "wikilink")
      - ERJ-170/175
      - ERJ-190/195
      - 血統1000
  - [EMB-110](../Page/EMB-110.md "wikilink")
  - [EMB-120](../Page/EMB-120.md "wikilink")
  - [EMB-121](../Page/EMB-121.md "wikilink")
  - ''

## [福克公司](../Page/福克公司.md "wikilink")

  - [福克F70](../Page/福克F70.md "wikilink")
  - [福克F100](../Page/福克F100.md "wikilink")
  - [福克F27](../Page/福克F27.md "wikilink")
  - [福克F28](../Page/福克F28.md "wikilink")
  - [福克F50](../Page/福克F50.md "wikilink")

## [福特](../Page/福特汽車.md "wikilink")

  - ''

## [亨德里·佩奇公司](../Page/亨德里·佩奇公司.md "wikilink")

  - ''
  - ''
  - ''
  - ''

## [霍克薛利](../Page/霍克薛利.md "wikilink")

  - ''[霍克薛利三叉戟型](../Page/霍克薛利三叉戟型.md "wikilink")
  - ''

## [伊留申](../Page/伊留申.md "wikilink")

  - ''
  - ''[伊留申-62](../Page/伊留申-62.md "wikilink")
  - ''[伊留申-76](../Page/伊留申-76.md "wikilink")
  - ''[伊留申-86](../Page/伊留申-86.md "wikilink")
  - ''[伊留申-96](../Page/伊留申-96.md "wikilink")

## [容克公司](../Page/容克公司.md "wikilink")

  - ''[Junkers F.13](../Page/Junkers_F.13.md "wikilink")
  - ''[Junkers G.38](../Page/Junkers_G.38.md "wikilink")
  - [Ju 52](../Page/Ju_52運輸機.md "wikilink")
  - ''

## [川崎重工](../Page/川崎重工.md "wikilink")

  - ''

## [洛克希德·馬丁](../Page/洛克希德·馬丁.md "wikilink")

  - ''[洛克希德L-188](../Page/洛克希德L-188.md "wikilink")
  - ''[L-1011三星客機](../Page/L-1011三星客機.md "wikilink")
  - ''[洛克希德星座](../Page/洛克希德星座.md "wikilink")

## [三菱重工業](../Page/三菱重工業.md "wikilink")

  - *[三菱MC-20](../Page/三菱MC-20.md "wikilink")*
  - [三菱MRJ](../Page/三菱支線噴射機.md "wikilink")

## [麥克唐納-道格拉斯](../Page/麥克唐納-道格拉斯.md "wikilink")

  - [DC-10](../Page/DC-10.md "wikilink")
  - [MD-11](../Page/MD-11.md "wikilink")
  - [MD-80](../Page/MD-80.md "wikilink")
  - [MD-90](../Page/MD-90.md "wikilink")

##

  - [YS-11](../Page/YS-11.md "wikilink")

## [SAAB](../Page/SAAB.md "wikilink")

  - [薩博340](../Page/薩博340.md "wikilink")
  - [薩博2000](../Page/薩博2000.md "wikilink")

##

  - ''

## [上海飞机制造](../Page/上海飞机制造.md "wikilink")

  - ''[上海 運-10](../Page/運-10.md "wikilink")

## [肖特兄弟](../Page/肖特兄弟.md "wikilink")

  - *[肖特索倫特](../Page/肖特索倫特.md "wikilink")*
  - [肖特330](../Page/肖特330.md "wikilink")
  - [肖特360](../Page/肖特360.md "wikilink")

##

  - *[南部飛機SE 210卡拉維爾](../Page/卡拉維爾客機.md "wikilink")*

##

  - ''

## [圖波列夫](../Page/圖波列夫.md "wikilink")

  - [Tu-104](../Page/圖-104.md "wikilink")

  - [Tu-114](../Page/Tu-114.md "wikilink")

  - [Tu-124](../Page/Tu-124.md "wikilink")

  - [Tu-134](../Page/Tu-134.md "wikilink")

  - [Tu-144](../Page/Tu-144.md "wikilink")

  - [Tu-154](../Page/Tu-154.md "wikilink")

  - [Tu-204](../Page/Tu-204.md "wikilink")

  - [Tu-214](../Page/Tu-214.md "wikilink")

  - [Tu-244](../Page/Tu-244.md "wikilink")

  -
  - [Tu-334](../Page/Tu-334.md "wikilink")

  -
## [維克斯-阿姆斯特朗公司](../Page/維克斯-阿姆斯特朗公司.md "wikilink")

  - ''[Vickers VC-10](../Page/Vickers_VC-10.md "wikilink")
  - ''[子爵型](../Page/子爵型.md "wikilink")
  - ''

## [雅克列夫](../Page/雅克列夫.md "wikilink")

  - ''
  - ''[Yak-42](../Page/Yak-42.md "wikilink")

## [中国商用飛機](../Page/中国商用飞机有限责任公司.md "wikilink")

  - [ARJ21](../Page/ARJ21.md "wikilink")
  - [C919](../Page/C919.md "wikilink")

## [中國航空工業集團](../Page/中國航空工業集團.md "wikilink")

  - [運-7](../Page/運-7.md "wikilink")
  - [運-8](../Page/運-8.md "wikilink")
  - [運-12](../Page/運-12.md "wikilink")
  - [新舟60](../Page/新舟60.md "wikilink")
  - [新舟600](../Page/新舟600.md "wikilink")

{{-}}

[\*](../Category/民航飛機.md "wikilink")
[Category:飞机列表](../Category/飞机列表.md "wikilink")