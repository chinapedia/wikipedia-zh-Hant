**民鲁**（），是[马来西亚](../Page/马来西亚.md "wikilink")[砂拉越州](../Page/砂拉越州.md "wikilink")[民都鲁省的](../Page/民都鲁省.md "wikilink")[省会](../Page/省会.md "wikilink")，州内第四大城，隶属于。该镇东鄰[美里](../Page/美里.md "wikilink")、南傍[加帛](../Page/加帛省.md "wikilink")、西接[诗巫](../Page/诗巫.md "wikilink")、北面[南海](../Page/南海.md "wikilink")；距离州首府[古晋空路距離](../Page/古晋.md "wikilink")349公里（約30分鐘航程），陸路距離612公里（約9小時路程）\[1\]。

十九世纪中叶前，民都鲁还是一个默默无闻的小渔村，至到1867年召开的砂拉越首届立法会议，民都鲁才逐渐的受關注。而随着之后的一波波巅峰发展期，民都鲁在短短的三十年里，从一个小渔村发展成了一座新兴的工业小镇。从民都鲁沿[格盟纳河而上有](../Page/格盟纳河.md "wikilink")[实包勿镇及另一條支流依序為](../Page/实包勿.md "wikilink")[班丹镇](../Page/班丹.md "wikilink")（Pandan）、[拉芒镇](../Page/拉芒.md "wikilink")（Labang）及[都胞镇](../Page/都胞.md "wikilink")（Tubau）。

## 历史

### 早期

在十六世纪前，民都鲁是[马兰诺国](../Page/马兰诺国.md "wikilink")（Malano）的领土。马兰诺国是由马兰诺族建立，而当时是[满者伯夷的一个小属国](../Page/满者伯夷.md "wikilink")。\[2\]由于满者伯夷国在1527年被[淡目國毀滅](../Page/淡目國.md "wikilink")，这让当时也是属国的[文莱有机会扩张其领土](../Page/文莱.md "wikilink")。[文莱第一位苏丹在经过多次与马兰诺人交战后](../Page/文莱.md "wikilink")，占领了马兰诺国，同时包括民都鲁地区。\[3\]

### 十九世纪

1840至1850年期间，砂拉越白人[拉者](../Page/拉者.md "wikilink")[詹姆士·布鲁克侄儿查理斯](../Page/詹姆士·布鲁克.md "wikilink")·詹森·布鲁克的挚友镇压海盗有功，后受勋爵士，1862年在民都鲁建设了木制营寨，并因其獲名开普堡（Fort
Keppel）\[4\]\[5\]，並留有開普路（Jalan Keppel）路名以資紀念\[6\]。

1865至1868年，意大利籍植物学家[奥多阿多·贝卡利](../Page/奥多阿多·贝卡利.md "wikilink")（Odoardo
Beccari）考察于[婆罗洲及](../Page/婆罗洲.md "wikilink")[新几内亚](../Page/新几内亚.md "wikilink")，1867年8月4日自[沐胶登上静心号](../Page/沐胶.md "wikilink")（Heartsease）炮舰前往[纳闽](../Page/纳闽.md "wikilink")，该舰出行使命为缴纳6千元岁币予[文莱苏丹](../Page/文莱苏丹.md "wikilink")，作为1861年起自其取得[沐胶和](../Page/沐胶.md "wikilink")[民都鲁的补偿](../Page/民都鲁.md "wikilink")。8月13日抵达格盟娜河畔，他在记录中提到，当时民都魯已有几間华人店铺，多数为[潮州人](../Page/潮州民系.md "wikilink")，而格盟娜河岸两旁是甘榜民都魯和甘榜日拔（Kampung
Jepak），河上游流域已有殘存的破屋\[7\]\[8\]\[9\]\[10\] ，未知其是否及时赶回，共襄首届州议会。

1867年9月8日，砂拉越首届州立法议会在民都鲁召开，议会地点就是现今的大钟楼位置。参与议会的有查理斯·詹森·布鲁克、五名英籍官员及十六名马来族及马兰诺族地方領袖。

此前[詹姆士·布鲁克为弭平前一年历时两个月的](../Page/詹姆士·布鲁克.md "wikilink")[石隆门华工起义而向](../Page/石隆门华工起义.md "wikilink")貸款5千[英鎊](../Page/英鎊.md "wikilink")，剛平亂就遭其逼債，在重压下于1858年10月21日突然罹患麻痹性中风\[11\]，并已先于1863年9月25日返回[英国伦敦](../Page/伦敦.md "wikilink")，最后于1868年6月11日去世；查理斯·詹森·布鲁克自[詹姆士·布鲁克回国前令其取代布鲁克](../Page/詹姆士·布鲁克.md "wikilink")·布鲁克成为王储后，已实则成为摄政王。

### 近代

1919年2月5日，[民都鲁中华公学创立](../Page/民都鲁中华公学.md "wikilink")，原名「普南學堂」，位於現今的市區碼頭。詩巫福州墾場1910年自詩巫向外發展，1926年至實巴荷，1954年至民都魯。\[12\]

三十年代初，由于世界战事频频，[砂拉越王國](../Page/砂拉越王國.md "wikilink")意识到战争迫在眉睫。他下令在全国五个地点兴建飞机场，其中一个地点是民都鲁。全部机场在1938年竣工，唯民都鲁由于财政的原因被迫于同年十月停建。\[13\]

1939年9月，[第二次世界大战爆发](../Page/第二次世界大战.md "wikilink")，[日军南侵](../Page/日军.md "wikilink")，民都鲁在1941年12月沦陷；在[英屬婆羅洲日佔時期](../Page/英屬婆羅洲日佔時期.md "wikilink")，首都在**東岸州**的[山打根](../Page/山打根.md "wikilink")。原屬[砂拉越王國行政區劃的第三省](../Page/砂拉越王國.md "wikilink")，下轄民都魯的詩巫于1942年8月8日改名為**志布州**\[14\]，州長官由日本北婆罗洲派遣军坐鎮古晉總部以控制局勢。同年6月所委派的千田倪次郎，下轄志布縣（今[詩巫省](../Page/詩巫省.md "wikilink")，應也包含1973年及2002年從詩巫劃出並升格為省的[泗里街省和](../Page/泗里街省.md "wikilink")[沐膠省](../Page/沐膠省.md "wikilink")）和民都魯縣——唯砂拉越於日佔期間所分成的三州，其具體施政細節至今仍未釐清，有待諳日文學者將存放於日本的當時相關文件譯出\[15\]；中华公学則被日军改为日文学校。

1942年9月5日在[古晉赛马场刑场](../Page/古晉.md "wikilink")\[16\]，甫上任两个月的日本北婆罗洲派遣军第37军的首任司令**[前田利为中将](../Page/前田利為.md "wikilink")**（前田的義表弟即义姑的儿子[近卫文麿於](../Page/近卫文麿.md "wikilink")1937—39、1940—41年曾任第34、38—39任[日本首相](../Page/日本首相.md "wikilink")）；近卫的外孙（前田的義表外孫）[细川护熙於](../Page/细川护熙.md "wikilink")1993—94年則成为第79任[日本首相](../Page/日本首相.md "wikilink")）監刑日本憲兵所收押的五名華人青年，他們此前遭判处偷竊汽油的罪名成立并被枪决后；前田于當天下午即搭乘軍機往納閩島，準備為以他命名的機場開幕，卻遲遲未見他出現\[17\]；豈不知於飞向[纳闽途中](../Page/纳闽.md "wikilink")，前田已在民都鲁岸外坠机身亡\[18\]。一个月后，经过日軍海陸空的搜索，終於在民都鲁岸外捞起飞机的残骸和前田利为中将的骸骨。\[19\]

1944—45年期間，民都魯市區先遭大火，焚燬三十多間木板店屋；隔年[同盟國盟军空投](../Page/同盟国_\(第二次世界大战\).md "wikilink")[燃燒彈](../Page/燃燒彈.md "wikilink")，將僅存的二十餘間店屋夷為平地，只留下了今魚市場對面的大伯公廟及[中華公學](../Page/民都鲁中华公学.md "wikilink")。\[20\]同年9月，[澳洲](../Page/澳洲.md "wikilink")[盟军收复砂拉越](../Page/同盟国_\(第二次世界大战\).md "wikilink")。翌年7月，末代拉者查理斯·维纳·布鲁克將砂拉越让渡于[英国](../Page/英国.md "wikilink")，成为英属殖民地。\[21\]

1945年[日本投降后](../Page/日本投降.md "wikilink")，于1942年初成立的[北婆罗洲反日同盟会形式上解散](../Page/北婆罗洲反日同盟会.md "wikilink")，转而在各地成立了中华公会和华侨青年社等左翼组织。
\[22\]1947年4月1日，民都鲁中华公会成立。同年9月10日，民中华公会接获[中华民国](../Page/中华民国.md "wikilink")[国民大会侨局国外国民代表兼第](../Page/国民大会.md "wikilink")35区选举事务所主席俞培钧来函表示[英属婆罗洲](../Page/英属婆罗洲.md "wikilink")（第35选举区）华侨得选国民代表一人。12月12日，民中华公会函中华民国[新加坡领事馆要求将中华公学一至四校个别立案](../Page/新加坡.md "wikilink")，恢复原来的校名即市区的中华公学和尚文小学、[实包勿的中山小学与](../Page/实包勿.md "wikilink")[班丹华小公学](../Page/班丹.md "wikilink")，后也获得总领事馆的批准。

1948年9月5日，中华民国首任驻[古晋领事馆领事陈应榮博士夫妇](../Page/古晋.md "wikilink")（次女[陳香梅的丈夫為](../Page/陳香梅.md "wikilink")[飛虎隊指揮官](../Page/飛虎隊.md "wikilink")[陳納德](../Page/陳納德.md "wikilink")）由[美里乘王长水轮抵第四省民都鲁](../Page/美里.md "wikilink")（当时省会为[美里](../Page/美里.md "wikilink")，至1987年民都鲁升格为省）巡视。\[23\]五十年代期间，民都鲁的木材业正处于蓬勃兴旺时期。

### 独立后

砂拉越于1963年7月22日脱离[英國殖民并独立](../Page/英國.md "wikilink")，同年9月16日与[马来亚](../Page/马来亚.md "wikilink")、[沙巴及](../Page/沙巴.md "wikilink")[新加坡共同组成](../Page/新加坡.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")。1968年在民都鲁外海发现天然气田，但至到1973年才正式开采。60年代末至70年代初，[砂拉越共產黨在](../Page/北加里曼丹共产党.md "wikilink")[拉讓江流域進行武裝活動](../Page/拉讓江.md "wikilink")，因此[詩巫](../Page/詩巫.md "wikilink")、[泗里街](../Page/泗里街.md "wikilink")、[民丹莪等福州人為主的中區居民往外遷移](../Page/民丹莪.md "wikilink")，隨著福州人遷入民都魯，導致早期以潮屬為主的華裔人口結構產生了改變。\[24\]

1975年，确认丹絨峇都（Tanjung
Batu）可作為砂拉越首座深水港口；美里中华商会为了加强第四省商会组织，来函并协助民都鲁组织中华商会，同年5月，中华总商会成立。商会诞生之前，民都鲁只有六、七排两层木板店，10月21日午夜的一场大火，烧毁了26间店屋，使民都鲁一瞬间少了三份之一的商店。\[25\]唯獨大伯公廟不被波及，後有乩童表示大伯公要出遊驅邪，否則民都魯地區將再有災難，所以1976年1月1日，大伯公廟首次舉辦遊神活動。\[26\]

砂拉越州政府在1978年设立民都鲁发展局（BDA），以协调[民都鲁省的各项发展计划](../Page/民都鲁省.md "wikilink")，当时人口已有一万人左右。民都鲁液化天然气厂于1982年竣工，第二分厂于1994年完工。随着民都鲁建设的蓬勃发展，镇里开始出现民房不足问题。为了解决房荒问题，卫星镇第一期于1985年竣工，建了508个单位的[排屋](../Page/排屋.md "wikilink")。

### 升格為省

1987年1月1日，民都鲁升格成民都鲁省，成为砂拉越的**第九省**，省会民都鲁镇。民都鲁野生动植物园在1992年开启，是砂拉越首座野生动植物园。由于发展计划多数是沿海开发，沿岸民宅及海岸線飽受驚濤駭浪的侵蝕，发展局在1996年兴筑[防波堤](../Page/防波堤.md "wikilink")；隔年[壳牌公司](../Page/壳牌公司.md "wikilink")（Shell）综合蒸馏厂（SMDS）所属12座油库其中的2座失火并于[圣诞节晚上](../Page/圣诞节.md "wikilink")10时50分爆炸\[27\]\[28\]，由220名消拯员将火势扑灭\[29\]；经维修于2000年6月重新启用\[30\]。

2005年举办首届婆罗洲国际风筝节；2007年11月民都魯發展聚在山上的總部啟用；2008年2月成为的城镇之一；2017年9月8日为砂拉越首届立法议会在民都鲁召开150周年，同月16日為慶祝[馬來西亞獨立日](../Page/馬來西亞獨立日.md "wikilink")60週年在民都魯中央商圈舉辦石壁彩繪及植樹活動\[31\]；2018年民都鲁发展局成立40周年。

现有购物广场：

  - Citypoint（1992年營業，下同）
  - 麗華大廈（Li Hua Plaza，1997年）
  - 城市花園商場（Parkcity Mall，2007年）
  - 美丹商場（Medan Mall，2014年）
  - 時代廣場（Times Square，2015年）
  - 百乐城（Paragon，2015年）
  - Commerce Square（2016年\[32\]）
  - 巴士总站（Bus Terminal，2017年）
  - 新欣（tHe Spring，原訂2018年11月運營\[33\]，延至12月開張）
  - 富麗華（Boulevard，將於2019年6月運營\[34\]）

另有Crown Pacific、Kidurong Sentral Commercial Complex、Kidurong Setia
Township Mall、SK One Garden City及Town Square Plaza，接續將於2020年前後完成。

發展局放眼民都魯在2025年昇市，屆時達島及實巴荷也將升格為省。\[35\]據英文版[马来西亚选区列表所陳列資料](../Page/马来西亚选区列表.md "wikilink")\[36\]，達島（屬）及實巴荷（屬）升格為省後或都將成為國會選區。

## 政治

民都鲁属于[民都鲁国会议席](../Page/民都鲁国会议席.md "wikilink")，该议席在[马来西亚下议院的代表为来自](../Page/马来西亚下议院.md "wikilink")[民进党的](../Page/民主进步党_\(马来西亚\).md "wikilink")[张庆信](../Page/张庆信.md "wikilink")。\[37\]
同时，民都鲁下有四个州议席，分别是日拔（Jepak）、丹绒峇都（Tanjong
Batu）、格美纳（Kemena）和三马拉如（Samalaju）州议席。以下为各区代表。

### 州议席

自[2016年砂拉越州选举以来](../Page/2016年砂拉越州选举.md "wikilink")，民都鲁的州议员如下：\[38\]

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>州议席</p></th>
<th><p>州议员</p></th>
<th><p>成员党</p></th>
<th><p>补充</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>N67</p></td>
<td><p>日拔区</p></td>
<td><p>達歷賓朱比靂（Talip Zulpilip）</p></td>
<td><p>土保党（PBB）</p></td>
<td><p><a href="../Page/砂拉越.md" title="wikilink">砂拉越首长署部长兼担任该区</a>5任议员（1996年至今）</p></td>
</tr>
<tr class="even">
<td><p>N68</p></td>
<td><p>丹绒峇都（原为「吉都隆」）</p></td>
<td><p>周政新（Chiew Chiu Sing）</p></td>
<td></td>
<td><p>担任该区4任议员（2001年至今）</p></td>
</tr>
<tr class="odd">
<td><p>N69</p></td>
<td><p>格美纳</p></td>
<td><p>倫帝阿納阿東（Stephen Rundi Utom）</p></td>
<td><p>土保党（PBB）</p></td>
<td><p>砂拉越乡区电力与水供部长兼担任该区4任议员（2001年至今）</p></td>
</tr>
<tr class="even">
<td><p>N70</p></td>
<td><p>三马拉如（从「吉都隆」拆分出来的新区）</p></td>
<td><p>玛章冷吉（Majang Renggi）</p></td>
<td><p>人民党（PRS）</p></td>
<td><p>2016年上任</p></td>
</tr>
</tbody>
</table>

## 人文

### 人口

民都鲁是砂拉越第四大城镇，在[古晋](../Page/古晋市.md "wikilink")、[美里和](../Page/美里.md "wikilink")[诗巫之后](../Page/诗巫.md "wikilink")。镇市区人口有114,058人、省级则有183,402人（2010年资料\[39\]），民都鲁镇人口占全省人口超过六成；2018年全省常住人口已達25.1萬人\[40\]；加上20萬外勞及3萬流動人口，則將近50萬人。\[41\]
镇里的人口主要由[伊班人](../Page/伊班族.md "wikilink")、[华人](../Page/华人.md "wikilink")、[马来人](../Page/马来人.md "wikilink")、马兰诺人及外国人组成，外国人多数为[印尼人](../Page/印尼人.md "wikilink")、[中国人及](../Page/中国人.md "wikilink")[韩国人](../Page/韩国人.md "wikilink")。

<table>
<caption>民都鲁民族人口统计 [42]</caption>
<thead>
<tr class="header">
<th><p>行政区划</p></th>
<th><p>总人口</p></th>
<th><p>伊班人</p></th>
<th><p>华人</p></th>
<th><p>马来人</p></th>
<th><p>马兰诺人</p></th>
<th><p>乌鲁人</p></th>
<th><p>其他</p></th>
<th><p>外国人</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>民都鲁市区</p></td>
<td><p>114,058<br />
<em><small>（62.2%）</small></em></p></td>
<td><p>32,992<br />
<em><small>（28.9%）</small></em></p></td>
<td><p>28,512<br />
<em><small>（25.0%）</small></em></p></td>
<td><p>14,945<br />
<em><small>（13.1%）</small></em></p></td>
<td><p>14,179<br />
<em><small>（12.4%）</small></em></p></td>
<td><p>6,068<br />
<em><small>（5.3%）</small></em></p></td>
<td><p>1,598<br />
<em><small>（1.4%）</small></em></p></td>
<td><p>14,939<br />
<em><small>（13.1%）</small></em></p></td>
</tr>
<tr class="even">
<td><p>实包勿市区</p></td>
<td><p>296<br />
<em><small>（0.2%）</small></em></p></td>
<td><p>82<br />
<em><small>（27.7%）</small></em></p></td>
<td><p>171<br />
<em><small>（57.8%）</small></em></p></td>
<td><p>13<br />
<em><small>（4.4%）</small></em></p></td>
<td><p>6<br />
<em><small>（2.0%）</small></em></p></td>
<td><p>2<br />
<em><small>（0.6%）</small></em></p></td>
<td><p>11<br />
<em><small>（3.7%）</small></em></p></td>
<td><p>11<br />
<em><small>（3.7%）</small></em></p></td>
</tr>
<tr class="odd">
<td><p>省余区</p></td>
<td><p>69,048<br />
<em><small>（37.6%）</small></em></p></td>
<td><p>39,735<br />
<em><small>（57.5%）</small></em></p></td>
<td><p>2,148<br />
<em><small>（3.1%）</small></em></p></td>
<td><p>5,078<br />
<em><small>（7.4%）</small></em></p></td>
<td><p>2,844<br />
<em><small>（4.1%）</small></em></p></td>
<td><p>5,351<br />
<em><small>（7.7%）</small></em></p></td>
<td><p>1,003<br />
<em><small>（1.5%）</small></em></p></td>
<td><p>12,943<br />
<em><small>（18.74%）</small></em></p></td>
</tr>
<tr class="even">
<td><p>全省</p></td>
<td><p>183,402<br />
<em><small>（100%）</small></em></p></td>
<td><p>72,809<br />
<em><small>（39.7%）</small></em></p></td>
<td><p>30,831<br />
<em><small>（16.8%）</small></em></p></td>
<td><p>20,036<br />
<em><small>（10.9%）</small></em></p></td>
<td><p>17,029<br />
<em><small>（9.3%）</small></em></p></td>
<td><p>11,421<br />
<em><small>（6.2%）</small></em></p></td>
<td><p>3,383<br />
<em><small>（1.8%）</small></em></p></td>
<td><p>27,893<br />
<em><small>（15.2%）</small></em></p></td>
</tr>
<tr class="odd">
<td><p>* <small>其他民族包括毕达友人及印度人。</small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 历史人口

  -
    {| class="wikitable" style="text-align: center;"

\! 年份 | 1960 || 1970 || 1980 || 1991 || 2000 || 2010 || 2018 |- \! 居民人数
| 5,000\[43\] ||14,000\[44\] || 42,812\[45\] || 51,862\[46\] ||
102,671\[47\] || 114,058\[48\] || 151,617\[49\] |- |}

### 语言

与砂拉越其他地区相同[马来语及](../Page/马来语.md "wikilink")[英语为民都鲁的官方语言](../Page/英语.md "wikilink")，在政府学校教学使用[马来语](../Page/马来语.md "wikilink")，除了华文小学和[中学则使用](../Page/獨立中學.md "wikilink")[华语](../Page/华语.md "wikilink")。市民在日常生活中，在不同民族之间多数使用[官方语言](../Page/官方语言.md "wikilink")，而同族之间则多使用自己的[母语](../Page/母语.md "wikilink")。在华族社群里[汉族](../Page/汉族.md "wikilink")[方言是通用的](../Page/方言.md "wikilink")，如：[福州话](../Page/福州话.md "wikilink")、[潮州话](../Page/潮州话.md "wikilink")、[厦门话](../Page/厦门话.md "wikilink")、[广东话](../Page/广东话.md "wikilink")、[客家话](../Page/客家话.md "wikilink")、[兴化话](../Page/兴化话.md "wikilink")、[会宁话等方言](../Page/会宁.md "wikilink")。由于华文学校里使用华语教学，所以在年轻一代的华族社群里多会使用[华语](../Page/华语.md "wikilink")。其他民族语言有伊班语、马兰诺语、毕达友语、加央语、肯雅语、普南语等。

### 社会团体

民都鲁有许多领域的社会团体如：服務、商业、文化、康乐、球类等。华团则有[籍贯和](../Page/籍贯.md "wikilink")[姓氏公会](../Page/姓氏.md "wikilink")。

### 宗教与信仰

马来人皆是[穆斯林](../Page/穆斯林.md "wikilink")，其他民族则依个人喜好选择信仰。华族多数信仰[佛教](../Page/佛教.md "wikilink")、[基督教](../Page/基督教.md "wikilink")、[道教及](../Page/道教.md "wikilink")[民间信仰等](../Page/民间信仰.md "wikilink")。伊班人、乌鲁人和比达友人较多信仰基督教，而也有一部分保留自己民族信仰。马兰诺人则多数信奉[伊斯兰教](../Page/伊斯兰教.md "wikilink")，有少部分是基督教徒。

## 经济

吉都隆（Kidurong）位於民都魯的東北郊，是一個天然海岬形成的港口，設有[液化天然气工业](../Page/液化天然气.md "wikilink")，首期工程完成于1982年；第二期工程耗資140亿[令吉完成于](../Page/令吉.md "wikilink")1994年；第三期工程耗资80亿[美金完成于](../Page/美金.md "wikilink")2003年。拥有全球最大型的集中式天然气蒸馏厂（MLNG），贸易额为砂州的45%，马来西亚的5%。液化天然气年生产量2,300万[吨](../Page/吨.md "wikilink")。[壳牌公司](../Page/壳牌公司.md "wikilink")（Shell）综合蒸馏厂（SMDS）是耗资18亿令吉的世界第一间综合蒸馏厂。[石油](../Page/石油.md "wikilink")[化学产品年产量](../Page/化学.md "wikilink")50万吨。[尿素](../Page/尿素.md "wikilink")[肥料厂](../Page/肥料.md "wikilink")（ABF）耗资7.7亿令吉，于1985年10月完成，为[东盟](../Page/东盟.md "wikilink")[国家所联营的第二座肥料厂](../Page/国家.md "wikilink")。[股份持有国家为马来西亚](../Page/股份.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[印尼与](../Page/印尼.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")。

三馬拉如工業區（Samalaju Industrial
Park）位於民都魯東北方約50公里的海邊。熔[铝厂耗資](../Page/铝.md "wikilink")69亿令吉，面积8平方公里，由砂州日光与世界最大炼铝公司[力拓集团](../Page/力拓集团.md "wikilink")（Rio
Tinto
Group）融资合作，目標年產量達150萬噸。[多晶矽原料廠由德山株式會社](../Page/多晶矽.md "wikilink")（Tokuyama）耗資650億日圓（折25億令吉）設立。

中密度[纤维板厂耗资](../Page/纤维.md "wikilink")2.2亿令吉，中密度纤维板年生产量10万立方米。

## 教育

民都鲁共有两所大学、三所学院、两家教育机构及一间技术职业学院（Vocational
College）；八间国民中学、一间华文独立中学、一间伊斯兰中学；十五间国民小学、七间华文小学\[50\]、两间国际学校。

### 高等学府

#### 大学

  - [博特拉大学](../Page/博特拉大学.md "wikilink")（UPM-Bintulu）民都鲁分校\[51\]-入口位于丽华花园（Taman
    Li Hua）、雅包（Nyabau）路
  - [马来西亚开放大学](../Page/马来西亚开放大学.md "wikilink")（OUM）民都鲁分校\[52\]-美丹商业区

#### 学院

  - 玛拉工艺学院（Institut Kemahiran Mara）\[53\]-民诗路14哩
  - ADTEC学院（Advanced Technology College）\[54\]-民诗路14哩
  - Sedamai学院\[55\]-城市花园商业区

#### 技职学院

  - 民都鲁技术职业学院（Vocational College Bintulu）

#### 教育机构

  - GES GLOBAL Educational Services（教育代理）\[56\]-美丹商业区
  - Interface Education Group（教育集团）\[57\]-美丹商业区

### 中学

#### 国民中学

  - 民都鲁政府中学（SMK Bintulu）-位于民美路
  - 市镇中学（SMK Bandar）-位于民美路
  - 巴鲁中学（SMK Baru）-位于馬來新村
  - 吉都隆中学（SMK Kidurong）-位于吉都隆
  - 爱砂吉琳中学（SMK Assyakirin）-位于双溪班兰（Sungai Plan）
  - 格美納中学（SMK Kemena），位于日拔村
  - 达岛中学（SMK Tatau）
  - 实巴勿中学（SMK Sebauh）

#### [独立中学](../Page/独立中学.md "wikilink")

  - 民都鲁开智中学\[58\]-民诗路7哩，2000年自诗巫迁入，是民都鲁境内唯一的独中，为国内最新搬迁的独中。

### 小学

#### 华文小学

  - [民都鲁中华公学一校](../Page/民都鲁中华公学.md "wikilink")-丹绒峇都
  - [民都鲁中华公学二校](../Page/民都鲁中华公学.md "wikilink")-吉都隆路
  - 实比河华文小学-民美路
  - [尚文公学](../Page/尚文公学.md "wikilink")-实比遥（Sebiew）路，由[衛理公會於](../Page/衛理公會.md "wikilink")1928年在[实包勿創立](../Page/实包勿.md "wikilink")<ref>[尚仁堂](https://scacarchives.wordpress.com/2017/08/08/%E5%B0%9A%E4%BB%81%E5%A0%82/)

砂拉越华人年议会历史文献部，2017年8月8日</ref>；1993年獲准遷校至民都魯，1995年一年級新生假福建公會屬下彬彬幼稚園上課，1996年全體師生迁入現址\[59\]

  - 开明小学-民诗路7哩开智中学旁，2014年自[民丹莪迁入](../Page/民丹莪.md "wikilink")\[60\]
  - 达岛中华公学
  - [实包勿中山小学](../Page/实包勿.md "wikilink")

### 國際學校

  - 民都鲁国际学校（Bintulu International School）\[61\]-集团于1987年创办，分校2017年起用
  - 吉都隆国际学校（Kidurong International School）\[62\]-1982年起用

### 幼儿园

  - 卫理幼儿园（由[卫理公会成立](../Page/卫理公会.md "wikilink")）
  - 彬彬幼儿园（由[广东公会成立](../Page/广东.md "wikilink")）
  - 圣安东尼幼儿园（由[天主教会成立](../Page/天主教.md "wikilink")）

## 交通

### 海路

民都鲁深水港启用于1983年1月1日，是砂州首座24小时开放的深水[港口](../Page/港口.md "wikilink")。1999年6月耗资4亿[令吉建成](../Page/令吉.md "wikilink")[婆罗洲首座](../Page/婆罗洲.md "wikilink")[货柜专用终站](../Page/货柜.md "wikilink")，2016年货柜吞吐量为27.7万个標準箱货柜\[63\]。

### 陆路

長程[巴士總站于](../Page/巴士.md "wikilink")2017年9月自已使用逾20年的美丹再也（Medan
Jaya）商區原總站遷至民都魯中環（Bintulu
Sentral）商區\[64\]，成為除古晉砂州第一座空調型巴士總站。

該總站營運的業者有Bintang Jaya Bus Express、Bus Asia、Eva Bus Express、Lanang Road
Bus、MTC Express、Rejang Transport、Sungai Merah Bus及Vital Focus
Group（包括Borneo Highway Bus Express、PB Bus Express及Suria Bus
Express）。

Vital Focus Group將于2018年遷至吉都隆中環（Kidurong Sentral）商區，另有Hornbill Bas
Ekspres、Kirata Sdn Bhd、Midsar Transport Sdn Bhd、Wilmas Sdn Bhd、Sapphire
Pacific Sdn Bhd簽署意向書將遷入吉都隆總站\[65\]，屆時民都魯將形成砂州首座擁有雙長程巴士總站的城鎮。

市區有2百餘輛[德士往返載客](../Page/德士.md "wikilink")；現已有[Grab](../Page/Grab.md "wikilink")服務入駐\[66\]，是馬來西亞28個服務點之一\[67\]。

### 空路

[民都鲁机场位于民美路](../Page/民都鲁机场.md "wikilink")23[公里处](../Page/公里.md "wikilink")，飞机跑道长达2.7公里，[空中巴士](../Page/空中巴士.md "wikilink")[A330可直接降落](../Page/A330.md "wikilink")。耗资3亿令吉于2003年开放使用。現有營運的業者包括[马来西亚航空](../Page/马来西亚航空.md "wikilink")（往[吉隆坡](../Page/吉隆坡.md "wikilink")、[古晋](../Page/古晋.md "wikilink")）、[亚洲航空](../Page/亚洲航空.md "wikilink")（同左，另有[新加坡于](../Page/新加坡.md "wikilink")2017年12月26日開航\[68\]，也是民都魯首條國際航線）、[马来西亚之翼航空](../Page/马来西亚之翼航空.md "wikilink")（往古晋、[亞庇](../Page/亞庇.md "wikilink")、[美里](../Page/美里.md "wikilink")、[詩巫](../Page/詩巫.md "wikilink")、[沐膠](../Page/沐膠.md "wikilink")）。

[星巴克於](../Page/星巴克.md "wikilink")2017年10月在民都魯機場開設民都魯第二家分店，在砂拉越的开店顺位继古晉与美里之后；在东马则为第四座城镇（沙巴[亚庇机场亦设有分店](../Page/亚庇机场.md "wikilink")）拥有机场分店。\[69\]

[机场位于市中心](../Page/机场.md "wikilink")，与临近商业区相距不足百米，现已中止服务，成为常年性国际[风筝节的比赛场地](../Page/风筝.md "wikilink")。

Gulf
Golden飞行学院占地45[英畝](../Page/英畝.md "wikilink")，2007年成立于机场旁，已於2010年年底關閉\[70\]。

## 休闲

[运动场所](../Page/运动.md "wikilink")

  - 1间室内[体育馆](../Page/体育.md "wikilink")
  - 1间露天体育馆
  - 1座18洞[高尔夫球场](../Page/高尔夫.md "wikilink")
  - 1间私人运动[俱乐部](../Page/俱乐部.md "wikilink")-Kidurong Club（吉都隆俱乐部）
  - 3座[游泳池](../Page/游泳.md "wikilink")
  - 3座钩球场
  - 4座[排球场](../Page/排球.md "wikilink")
  - 8座[壁球场](../Page/壁球.md "wikilink")
  - 14座[足球场](../Page/足球.md "wikilink")
  - 16座[篮球场](../Page/篮球.md "wikilink")
  - 25座[藤球场](../Page/藤球.md "wikilink")
  - 32座[网球场](../Page/网球.md "wikilink")
  - 40座[羽球场](../Page/羽球.md "wikilink")

[公园](../Page/公园.md "wikilink")

  - 野生動植物園（Taman Tumbina） [Tumbina](http://tumbina.com.my/index.php)
  - 诗美拉遥（Similajau）[国家公园](../Page/国家公园.md "wikilink")
    [Similajau](https://web.archive.org/web/20141122063419/http://www.sarawakforestry.com/htm/snp-np-siminajau.html)

## 名人

### 路名

  - 丁明建路（Jalan Bakeri Ting）\[71\]\[72\]
  - 劉玉順路（Jalan Law Gek Soon）\[73\]\[74\]

### 受勋者

  - [拿督斯里](../Page/拿督斯里.md "wikilink")

<!-- end list -->

  - [张庆信](../Page/张庆信.md "wikilink")（太平局绅）－自1999年起任民都魯區國會議員，2000年3月10日受封於雪蘭莪蘇丹，1996年受封拿督。\[75\]马来西亚首相对东亚特使，前國陣後座議員理事會主席，[民主進步黨黨中央主席](../Page/民主进步党_\(马来西亚\).md "wikilink")（2014—）。
  - 范長錫－受封於2012年10月10日，自2016年起任泗里街卢勃区州会议员。\[76\]
  - 毛文新－受封於2017年1月31日之前，已故前**邦曼查**毛良干长子。\[77\]
  - 池德峰－受封於2017年8月19日之前\[78\]，受封拿督於2011年9月之前\[79\]。

<!-- end list -->

  - [拿督](../Page/拿督.md "wikilink")

<!-- end list -->

  - 国家元首
      - 许长国－2007年6月2日受封\[80\]。
      - 林光美－2011年6月4日受封\[81\]。
      - 谢向义－2011年6月4日受封\[82\]。
      - 包章文－2012年6月2日受封\[83\] 。
      - 黄益隆－2014年6月6日受封\[84\]。
      - 叶绍辉－2014年6月6日受封\[85\]。
      - 卓昭平－2016年6月4日受封\[86\]。
      - 林垂津－2017年9月9日受封\[87\]。
  - 各州[苏丹](../Page/苏丹_\(称谓\).md "wikilink")/元首
      - 张自治－2009年10月23日受封於[砂拉越州元首](../Page/砂拉越州元首.md "wikilink")\[88\]。
      - 貝建安－2011年1月26日受封於[馬六甲州元首](../Page/馬六甲州元首.md "wikilink")\[89\]。
      - 蔡松平－2014年9月受封于砂拉越州元首\[90\]。
      - 许步平－2015年9月12日受封於砂拉越州元首\[91\]。
      - 卢忠义－2015年11月14日受封於马六甲州元首\[92\]。
      - 刘义容－受封於2008年3月之前\[93\]。
      - 俞贤远－受封於2008年3月之前\[94\]。
      - 丁庆利－受封於2016年2月之前\[95\]。
      - 范长祖－受封於2016年2月之前\[96\]。
      - 叶乃鹏－受封於2016年2月之前\[97\]。
      - 吴文惠－受封於2016年7月之前\[98\]。

<!-- end list -->

  - 上議員

<!-- end list -->

  - （前任）许长国
  - （前任）包章文：前砂拉越民主進步黨吉隆區部主席

<!-- end list -->

  - 天猛公

<!-- end list -->

  - 叶绍辉（2011年8月受委）\[99\]
  - 沈剑辉

<!-- end list -->

  - 邦曼查

<!-- end list -->

  - 卓昭平（2011年8月受委）\[100\]
  - 叶绍辉（2011年8月卸任）
  - 林光美（2008年7月卸任）

<!-- end list -->

  - 本固魯

<!-- end list -->

  - 蔡松平（2014年1月受委）\[101\]

<!-- end list -->

  - 甲必丹\[102\]

<!-- end list -->

  - 姚本忠
  - 江小平
  - 林正敏
  - 卢文财
  - 苏健发
  - 包章泰
  - 林咸琳
  - 丁开忠
  - 沈茂美（女）
  - 吳鵬飛－2018年9月27日之前。\[103\]

<!-- end list -->

  - 逝世

<!-- end list -->

  - [敦](../Page/敦_\(头衔\).md "wikilink")
      - （1928—2015），1928年出生於民都魯甘榜日拔，1970—1981年任[砂拉越州首席部长](../Page/砂拉越州首席部长列表.md "wikilink")，1981—1985年任[砂拉越州元首](../Page/砂拉越州元首.md "wikilink")，2015年在[古晉去世](../Page/古晉.md "wikilink")，享年87歲。\[104\]\[105\]

<!-- end list -->

  - [丹斯里](../Page/丹斯里.md "wikilink")
      - 叶明逸-民省首名拿督斯里，地方殷商兼耆老\[106\]，去世于2016年4月30日，得年103歲\[107\]。
  - 拿督
      - 黄致强－受封於2017年5月之前\[108\]，2018年9月16日去世\[109\]。
  - 邦曼查
      - 毛良干
  - 甲必丹
      - 林国谋－2007年12月30日。\[110\]
      - 胡良华－2013年7月3日，终年65岁。\[111\]
      - （實巴荷）陳榕臣－2018年9月19日，閏壽90岁。\[112\]

## 相關條目

### 中文（16）

  - [民都鲁省](../Page/民都鲁省.md "wikilink")
      - [民都鲁县](../Page/民都鲁县.md "wikilink")
      - [达岛县](../Page/达岛县.md "wikilink")
          - [达岛](../Page/达岛.md "wikilink")
      - [实包勿副县](../Page/实包勿副县.md "wikilink")
          - [实包勿](../Page/实包勿.md "wikilink")
  - [民都鲁国会议席](../Page/民都鲁国会议席.md "wikilink")
  - [民都魯機場](../Page/民都魯機場.md "wikilink")
  - [民都鲁中华公学](../Page/民都鲁中华公学.md "wikilink")
  - [格盟纳河](../Page/格盟纳河.md "wikilink")
  - [蝦醬](../Page/蝦醬.md "wikilink")（Belacan）
  - [白鷺](../Page/白鷺.md "wikilink")
  - [奥多阿多·贝卡利](../Page/奥多阿多·贝卡利.md "wikilink")
  - [前田利為](../Page/前田利為.md "wikilink")
  - [英属婆罗洲](../Page/英属婆罗洲.md "wikilink")
  - [马来西亚开放大学](../Page/马来西亚开放大学.md "wikilink")

### 英文（14）

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 馬來文（14）

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 参考文献

  - [古籍](../Page/古籍.md "wikilink")
      - \[印尼\][普腊班扎](../Page/普腊班扎.md "wikilink")（Mpu
        Prapanca）著；\[中国\]徐明月、刘志强（编译），[满者伯夷的](../Page/满者伯夷.md "wikilink")1365年古文献《[爪哇史颂](../Page/爪哇史颂.md "wikilink")》（Nagarakretagama），[北京](../Page/北京.md "wikilink"):[商务印书馆](../Page/商务印书馆.md "wikilink")，2016.10-217页\[113\]

<!-- end list -->

  - 专书
      - \[马来西亚\]蔡宗祥，《砂拉越華人民間信仰》，[诗巫](../Page/诗巫.md "wikilink")：砂拉越華族文化協會，1994-120页\[114\]
      - \[台湾\]黄伟雯，《砂拉越留台同學會之研究（1964-1996）》，[古晋](../Page/古晋.md "wikilink"):[砂拉越留台同学会](../Page/砂拉越.md "wikilink")，2002-194页\[115\]
      - 《民都鲁天恩亭大伯公廟重建落成典禮暨舉辦<第一届全砂大伯公節盛會>纪念特刊》，[民都鲁](../Page/民都鲁.md "wikilink")：民都鲁省華人慈善信托委員會、民都鲁福德祠理事會,
        2010-176頁\[116\]
      - 《民都鲁省华族史料集》，[民都魯](../Page/民都魯.md "wikilink")：民都鲁華總，编撰中。

<!-- end list -->

  - [年鑑](../Page/年鑑.md "wikilink")
      - The Directory and Chronicle for China, Japan, Corea, Indo-China,
        Straits Settlements, Malay States, Siam, Netherlands India,
        Borneo, The Philippins, \&c. \[117\]

<!-- end list -->

  - 诗集
      - \[马来西亚\]吴岸，《达邦树礼赞》页95-97<民都鲁二题>，[古晋](../Page/古晋.md "wikilink")：砂拉越华文作家协会，1991-175页\[118\]

<!-- end list -->

  - 学位论文
      - \[马来西亚\]胡清淼（硕士），[国立彰化师范大学教育研究所](../Page/国立彰化师范大学.md "wikilink")
        成功教育教學模式融入華文教學之研究-以民都魯開智中學為例.2014年\[119\]
      - \[马来西亚\]陈缃（学士），[拉曼大学中华研究院中文系](../Page/拉曼大学.md "wikilink")
        “浴火重生”——论砂拉越民都鲁开智中学之迁校历程与发展 .2016年4月18日\[120\]

<!-- end list -->

  - 期刊论文
      - [砂拉越博物馆馆刊](../Page/砂拉越.md "wikilink")（Sarawak Museum
        Journal）.1911,1.
      - \[台湾\]吴诗兴，台湾政治大学中文系硕士，\<馬來西亞的福德正神信仰探析—以砂拉越的大伯公廟為主要探討\>，《成大宗教与文化学报》，第十三期（台南：2009.12），页97-138\[121\]。
      - \[中国\]廖大珂，厦门大学南洋研究院教授，南京大学中国南海研究协调创新中心研究员，二战后马来西亚的闽商.2013年第2期\[122\]

<!-- end list -->

  - 网路期刊
      - ［新加坡］金进，捷讯论文网，2012年12月8日\[123\]
      - ［香港］谢征达，华文文学期刊，2015第1期\[124\]

<!-- end list -->

  - 電子資料庫
      - 砂拉越憲報（Sarawak Gazette）\[125\]

<!-- end list -->

  - 脚注

## 外部链接

  -
  - 民都鲁网[民都鲁网](http://www.bintulu.org/)

      - [33 Things You Should Know About Bintulu according to
        Shahril](http://www.bintulu.org/2008/05/03/facts-file-33-things-you-should-know-about-bintulu.php)2008年5月3日

  - 民都魯發展局[Reminiscing the old
    Bintulu](http://www.bda.gov.my/modules/web/pages.php?mod=news&sub=news_view&menu_id=&sub_id=&nid=114&m=&y=)2014年9月16日

<!-- end list -->

  - 论坛
      - [佳礼论坛](../Page/佳礼论坛.md "wikilink")
          - [民都鲁有什么美食，在那里???](https://cn.cari.com.my/forum.php?mod=viewthread&tid=1169509)2008年-
          - [〖民都鲁〗Times Square
            Megamall《民省最大型购物商场今开业！》](https://cforum.cari.com.my/forum.php?mod=viewthread&tid=2045310)2010年-
          - [民都鲁已经是城市之一？！](https://cn.cari.com.my/forum.php?mod=viewthread&tid=3816740)2016年-
      - lowyat.net
          - [Bintulu : from village of 5,000 to town of 250,000,
            Sarawak 4th largest
            town](https://forum.lowyat.net/topic/3201865/all)2015年-
      - Skysrapercity
          - [BINTULU | Sarawak | Town, District &
            Division](http://www.skyscrapercity.com/showthread.php?t=200366)2003年-
      - [Youtube](../Page/Youtube.md "wikilink")
          - [Old Bintulu, Sarawak (RARE
            documentary)](https://www.youtube.com/watch?v=sNhJ7iBML1A)2015年2月
          - [The Revolution of Chung Hua
            Bintulu](https://www.youtube.com/watch?v=klktw6kEbUY&t=2s)2017年3月

**[民都鲁省](../Page/民都鲁省.md "wikilink")**

**[砂拉越州](../Page/砂拉越州.md "wikilink")**

[Category:马来西亚市镇](../Category/马来西亚市镇.md "wikilink")
[Category:砂拉越](../Category/砂拉越.md "wikilink")

1.

2.  满者伯夷的古文献《[爪哇史颂](../Page/爪哇史颂.md "wikilink")》（Nagarakretagama）中提到马兰诺国和[文莱国是其属国](../Page/汶莱.md "wikilink")。

3.

4.

5.  [Fort Keppel
    in 1868](https://www.triposo.com/loc/Bintulu/history/background)Triposo

6.

7.
8.

9.

10.

11.

12.

13.

14. [“日本兵来了！”1941年平安夜，古晋沦陷…](http://www.intimes.com.my/write-html/06bau22.htm)國際時報，2006年6月26日

15.

16. [Under the Nippon
    flag](http://www.theborneopost.com/2011/09/16/under-the-nippon-flag/)Bonreo
    Post，2011年9月16日

17.
18.

19.

20. 参大伯公廟中所刻《本廟重建落成碑記》，1985年4月12日

21. [栳叶
    宴客之无字请柬](http://intimes.com.my/write-html/07curios07.htm)國際時報，2007年5月9日

22.

23.

24.

25. [商會簡介](http://bccci.com.my/intro/)民都魯中華總商會，2015

26.

27.

28. [Shell Shuts Down Malaysia Plant After
    Explosion](http://www.business-standard.com/article/specials/shell-shuts-down-malaysia-plant-after-explosion-197122701073_1.html)Business
    Standard 1997年12月27日

29.
30.

31. [Malasia Day -16 September,2017 in
    Bintulu.](http://mysarawak2.blogspot.my/2017/09/malasia-day-16-september2017-in-bintulu.html)Blogspot，2017年9月18日

32. [Too many malls, or a dying
    breed?](http://www.theborneopost.com/2017/05/21/too-many-malls-or-a-dying-breed/)The
    Borneo Post，2017年5月21日

33.

34.

35. 《[星洲日报电子报](../Page/星洲日报.md "wikilink")》砂拉越中区民都鲁版页17，2018年7月30日

36. [List of Malaysian electoral
    districts](https://en.wikipedia.org/wiki/List_of_Malaysian_electoral_districts)Wikipedia，2018年10月8日

37.

38.

39.
40.
41.

42.
43.

44.

45.

46.
47.
48.

49.

50. [民都鲁省华小通讯录](http://shadongzong.org/primary-schools/bintulu-schools/)砂拉越华小董事联合会总会

51. [Kampus Bintulu Sarawak](http://www.btu.upm.edu.my/)Universiti Putra
    Malaysia

52. [Bintulu Learning
    Centre](http://www.oum.edu.my/pages/prospective/prospective/LC/bintulu.html)Open
    University Malaysia

53. [IKM Bintulu](http://bintulu.ikm.edu.my/) Institut Kemahiran Mara

54. [（ADTEC）Bintulu Sarawak](http://www.adtecbintulu.my/v3/) Pusat
    Latihan Teknologi Tinggi

55. [Sedamai College](http://www.sedamai.edu.my/)Sedamai College

56. [Our Branches](http://www.gesglobalgroup.com/contact/)GES GLOBAL
    Educational Services

57. [Interface Education
    Group](http://interfaceeducationgroup.com/bintulu.html)Interface
    Education Group

58. [开智中学](http://www.smkaidee.net/)开智中学

59. [校史](http://sjksiongboon.blogspot.my/p/blog-page_05.html)民都鲁尚文公学，2011年

60. [迁至开智独中校地
    民丹莪开明获准证迁校](http://www.sinchew.com.my/node/1275619)星洲网，2012年6月15日

61. [Bintulu International School](http://www.bis.edu.my/)Bintulu
    International School

62. [KIDURONG INTERNATIONAL
    SCHOOL](http://www.kiduronginternationalschool.net/)Kidurong
    International School

63. [Container
    Handled](http://www.bpsb.com.my/The-Port/Port-Statistic/Container-Handled/)
    Bintulu Port

64. [麗華酒店新張](http://www.pressreader.com/malaysia/sin-chew-daily-sarawak-edition-sibu/20170819/284185105925540)Press
    Reader，2017年8月19日

65. [New bus terminal to transform
    Kidurong](https://www.theborneopost.com/2015/11/29/new-bus-terminal-to-transform-kidurong/amp/)Borneo
    Post online，2015年11月29日

66. [Grab offers discount rides, expands to new
    cities](https://www.stuff.tv/my/news/grab-offers-discount-rides-expands-new-cities)Stuff.tv，2017年6月21日

67. [Malaysia](https://www.grab.com/my/)Grab Car，2017年

68. [亚航民都鲁直飞新加坡航线 12月26日开始！](http://news.seehua.com/?p=317218)詩華日報，2017年11月2日

69. [Store
    Locator](http://www.starbucks.com.my/store-locator/search/location/Bintulu%2C%20Sarawak%2C%20Malaysia/detail/1019454)Starbucks，2017年

70. [Govt urged to intervene in closing of aviation
    school](http://www.theborneopost.com/2012/05/17/govt-urged-to-intervene-in-closing-of-aviation-school/)The
    Borneo Post，2012年5月17日

71.

72.

73.

74.

75.

76.

77.

78.

79.

80. [国家元首端姑米占再纳阿比丁
    册封2317有功臣民](http://www.sinchew.com.my/node/451896)星洲网，2007年6月2日

81. [元首华诞册封1207臣民
    前政府首席秘书阿都拉阿育受封敦](http://www.sinchew.com.my/node/1232367)星洲网，2011年6月4日

82.
83. [国家元首华诞封赐1523臣民．陈宗明荣膺拿督荣衔](http://www.intimes.com.my/nws/jun12/nws12060304.htm)國際日報，2012年6月3日

84. [元首华诞　赐封1622人](http://www.sinchew.com.my/node/1391768)星洲网，2014年6月7日

85. [国家元首册封1622有功臣民
    人联秘书沈桂贤获封拿督荣衔](http://intimes.com.my/index.php/2013-09-08-02-08-40/item/7472-1622/7472-1622)国际日报，2014年6月8日

86. [本地商业教育界名人
    卓昭平受封拿督](http://news.news-com.cn/a/20160605/2268249.shtml)东盟新闻，2016年6月5日

87. [元首冊封1518人
    勞勿斯曾永森三美封敦](http://www.sinchew.com.my/node/1680359)星洲网，2017年9月9日

88.

89.

90.

91.

92.

93.

94.
95.

96.

97.
98.

99.

100.
101.

102.

103. [星洲日报砂拉越中区民都鲁版页](../Page/星洲日报.md "wikilink")21，2018年9月27日

104.

105.

106. [元首华诞　赐封1574人](http://www.orientaldaily.com.my/index.php?option=com_k2&view=item&id=58194:1574&Itemid=113)東方日報，2013年6月1日

107. [享嵩寿103岁
     创省元老叶明逸逝世](http://news.news-com.cn/a/20160430/2263765.shtml)東盟新聞
     2016年4月30日

108.

109. [星洲日报砂拉越中区民都鲁版页](../Page/星洲日报.md "wikilink")33，2018年9月16日

110.

111. [民都鲁华社族群领袖
     甲必丹胡良华病逝](http://www.sinchew.com.my/node/932022)星洲网，2013年7月3日

112. [星洲日报砂拉越中区民都鲁版页](../Page/星洲日报.md "wikilink")25，2018年9月20日

113.

114.

115.

116.

117. [The Directory and Chronicle for China, Japan, Corea, Indo-China,
     Straits Settlements, Malay States, Siam, Netherlands India, Borneo,
     The Philippins,
     \&c.](https://play.google.com/store/books/details/The_Chronicle_and_Directory_for_China_Japan_the_Ph?id=dJMwe7xA6UMC)Google
     Book

118.

119.

120.

121.
122.

123. [从单纯激情到忧郁善思的拉让江畔诗人
     （3）](http://www.jiexunlunwen.com/article/2012/1208/article_10985_3.html)捷讯论文网，2012年12月8日

124. [盾上的痕迹：吴岸诗歌中的本土实践与身份确立](http://qk.laicar.com/Home/Content/214641)华文文学期刊，2015第1期

125. [E-Sarawak
     Gazette](http://www.pustaka-sarawak.com/gazette/home.php)Pustaka
     Sarawak