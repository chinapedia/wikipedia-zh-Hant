[Aerogelflower_filtered.jpg](https://zh.wikipedia.org/wiki/File:Aerogelflower_filtered.jpg "fig:Aerogelflower_filtered.jpg")
[Aerogelbrick.jpg](https://zh.wikipedia.org/wiki/File:Aerogelbrick.jpg "fig:Aerogelbrick.jpg")
[Aerogel.jpg](https://zh.wikipedia.org/wiki/File:Aerogel.jpg "fig:Aerogel.jpg")

**氣凝膠**（Aerogel）是目前已知[密度僅次於](../Page/密度.md "wikilink")[全碳气凝胶的物質](../Page/全碳气凝胶.md "wikilink")。它是由[氣體取代](../Page/氣體.md "wikilink")[液體在](../Page/液體.md "wikilink")[凝膠中的位置製造而成](../Page/凝膠.md "wikilink")。其擁有許多俗名：凍結的煙霧（frozen
smoke）、固態的煙霧（solid smoke）、固態的空氣（solid air）和藍煙（blue
smoke）等，而這些都源自於其透明性與物質中的光線[散射能力](../Page/散射.md "wikilink")。其觸感像是[聚苯乙烯](../Page/聚苯乙烯.md "wikilink")。

Samuel Stephens Kistler在1931年發明氣凝膠。而這一切是因為他與Charles
Learned之間的賭注。他們二人當時正比賽誰先將[凝膠裡的液體成分用氣體取代卻不使發泡的間壁收縮崩塌](../Page/凝膠.md "wikilink")。最後Kistler辦到了。

氣凝膠藉由將凝膠裡頭的液體成分抽出。這種方法會令液體緩慢的被脫出，但不至於使凝膠裡的固體結構因為伴隨的[毛細作用被擠壓破碎](../Page/毛細作用.md "wikilink")。
世界上第一個氣凝膠體的主要成分是[矽膠](../Page/矽膠.md "wikilink")。Kistler隨後又造出了以[鋁](../Page/鋁.md "wikilink")、[鉻](../Page/鉻.md "wikilink")、[氧化錫為基礎物質的凝膠](../Page/二氧化錫.md "wikilink")。第一個[碳凝膠體則遲至](../Page/碳.md "wikilink")1980年代以後才被開發。

## 性質

此物質極輕。其密度大約為3mg/cm<sup>3</sup>僅僅為[空氣的三倍重](../Page/空氣.md "wikilink")。（由[Larry
Hrubesh領導的](../Page/Larry_Hrubesh.md "wikilink")[LLNL實驗室首先製備出世界上密度最小的CO](../Page/LLNL.md "wikilink")<sub>2</sub>氣凝膠）

儘管氣凝膠裡有個膠字，但它其實是堅硬而乾燥的[物質](../Page/物質.md "wikilink")，就其[物理性質與膠體一點也不類似](../Page/物理.md "wikilink")。被稱為膠是由於它的製造過程提取於凝膠。提起指尖輕輕在凝膠表面按壓一下並不會留下痕跡；如果以加重的力道按壓會造成永久的凹陷；而加上足夠的力量會讓它如玻璃般破碎散落成塊。這個便是我們所知道的易脆性。

雖然它具有破碎的傾向，但是以結構來說它仍是非常堅固的。它優秀的負載能力是源於其[分子的組成](../Page/分子.md "wikilink")：大約尺寸為2到5[奈米的球形聚合粒子互相結合成一個個小單元](../Page/奈米.md "wikilink")，進而組合成[樹突狀的微觀立體結構](../Page/樹突.md "wikilink")。這些小單元會形成碎形鏈狀的[三維結構](../Page/三維.md "wikilink")，其間充塞著大量的孔洞，每個孔洞尺寸不大於100奈米。而這些孔洞的聚集密度以及個別的平均大小能夠經由製造過程加以控制。

氣凝膠是優良的[熱絕緣體](../Page/熱.md "wikilink")，氣凝膠幾乎能阻絕由三種[傳熱方式](../Page/傳熱.md "wikilink")（[熱傳導](../Page/熱傳導.md "wikilink")、[熱對流](../Page/熱對流.md "wikilink")、[熱輻射](../Page/熱輻射.md "wikilink")）中的两种帶來的熱量轉移。凝膠中空氣的成分比例至少佔99.8%以上，而空氣為熱的不良導體，故它們是好的熱傳導隔絕材料(不過金屬凝膠在作為絕緣體的用途時就不是那麼有效)。例如矽凝膠，由於矽也為熱的不良導體，其隔絕性能又更加良好了。就熱對流的方式而言，因為空氣無法跨越凝膠表面行對流作用，他們也是好的熱對流隔絕材料。一般的气凝胶对热辐射不起太大的作用。碳凝膠是好的熱輻射隔絕材料，因為碳元素在標準溫度下能夠吸收熱量轉移時散發的紅外線輻射。亦即，現今性質最好的氣凝膠是掺入部分碳元素的矽凝膠。

因為吸濕的自然性質，凝膠摸起來乾乾的，並表現出強大的吸水能力。為此長時間拿著凝膠的人要穿著手套，以預防[皮膚表面出現乾燥脆化的現象](../Page/皮膚.md "wikilink")。

因為[瑞利散射的影響](../Page/瑞利散射.md "wikilink")，凝膠裡的微小樹突結構會過濾[可見光](../Page/可見光.md "wikilink")[光譜中](../Page/光譜.md "wikilink")[波長較短小的部份](../Page/波長.md "wikilink")。而這就產生了它輕柔的顏色：在較暗的環境散發柔柔的藍光，在明亮的環境散發微微的黃光。

氣凝膠本身的性質是具親水性的，但是藉由化學製程可將它改變為具疏水性。凝膠在逐漸吸收水氣的過程中會經歷分子結構的變動，如同尺寸縮水，甚至完全被毀壞。藉由改變凝膠對於水氣的接受度才能改善毀壞的現象。即使有裂縫穿過膠體表面，那些內部被化學製程強化過的樣品對水氣的抵抗作用也會好過於那些只僅僅強化分子表層的樣品。而疏水性製程能促進膠體加工過程的便利性，因為這樣就便於使用水刀切割了。

## 安全性

氣凝膠的安全性取決於其製造的物質成分。部分氣凝膠組成成分中含有[致癌物質或](../Page/致癌物質.md "wikilink")[毒素](../Page/毒素.md "wikilink")。目前以矽為基本材質的氣凝膠還未發現具有致癌或含毒的性質。不過矽氣凝膠會刺激人的[眼睛](../Page/眼睛.md "wikilink")、[皮膚](../Page/皮膚.md "wikilink")、[呼吸道和](../Page/呼吸道.md "wikilink")[消化系統](../Page/消化系統.md "wikilink")，並且一旦接觸會造成皮膚[黏膜的乾澀](../Page/黏膜.md "wikilink")。因此建議當持著矽凝膠時最好配備著護目鏡以及手套以避免受到傷害。

## 發展

[Aerogel_nasa.jpg](https://zh.wikipedia.org/wiki/File:Aerogel_nasa.jpg "fig:Aerogel_nasa.jpg")
20世紀80年代初期，[粒子物理學家認識到SiO](../Page/粒子物理.md "wikilink")<sub>2</sub>氣凝膠將是**氣凝膠契忍可夫計數器**（Aerogel
Cherenkov
Counter）粒子鑑別器（見[契忍可夫效應](../Page/契忍可夫輻射.md "wikilink")）的理想介質材料，試驗需要大量的透明SiO<sub>2</sub>氣凝膠。他們使用TMOS方法，製造了兩個大探測器：一個是在[德國](../Page/德國.md "wikilink")[漢堡的DESY實驗室的TASSO探測器](../Page/漢堡.md "wikilink")，使用了1700升氣凝膠；另一個是[歐洲粒子物理研究所](../Page/歐洲粒子物理研究所.md "wikilink")（CERN）製造的探測器，使用了由[瑞典隆德大學製備的](../Page/瑞典.md "wikilink")1000升氣凝膠。現在[日本](../Page/日本.md "wikilink")[高能加速器研究機構B介子工廠的](../Page/高能加速器研究機構.md "wikilink")[Belle實驗偵測器中也使用](../Page/Belle實驗.md "wikilink")**氣凝膠契忍可夫計數器**。這個偵測器利用的氣凝膠介於液體與氣體之低[折射係數特性以及其高透光度與固態的性質](../Page/折射.md "wikilink")，優於傳統使用低溫液體或是高壓空氣的作法。

氣凝膠在太空偵測上也有多種用途，在[俄羅斯的](../Page/俄羅斯.md "wikilink")[和平號太空站和](../Page/和平號太空站.md "wikilink")[美國的](../Page/美國.md "wikilink")[火星探路者上都有用到這種材料](../Page/火星探路者.md "wikilink")。

## 参见

  - [气溶胶](../Page/气溶胶.md "wikilink")
  - [Aerogel Cerenkov Counter for the BELLE
    Experiment](http://citeseer.ist.psu.edu/89883.html)
  - [KEK加速器簡介](https://web.archive.org/web/20050318004701/http://hepsrv.phy.ncu.edu.tw:8000/2photon/index.html)
  - [中央研究院物理研究所AMS實驗儀器簡介](http://www.sinica.edu.tw/~ams/instrument.html)
  - [MarkeTech International
    Aerogels](https://web.archive.org/web/20100327123558/http://www.mkt-intl.com/aerogels/index.html)

[Q](../Category/材料.md "wikilink") [Q](../Category/粒子物理学.md "wikilink")