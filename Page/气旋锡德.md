**特強氣旋風暴錫德**（，[聯合颱風警報中心編號](../Page/聯合颱風警報中心.md "wikilink")：**06B**），是[2007年北印度洋气旋季第](../Page/2007年北印度洋气旋季.md "wikilink")4个被命名的[热带气旋](../Page/热带气旋.md "wikilink")，11月11日在[孟加拉湾生成](../Page/孟加拉湾.md "wikilink")，其后强度一度达到[萨菲尔-辛普森飓风等级中的五级强度](../Page/萨菲尔-辛普森飓风等级.md "wikilink")（[美国](../Page/美国.md "wikilink")[联合台风警报中心认定](../Page/联合台风警报中心.md "wikilink")）是孟加拉湾有史以来最强烈的被命名的气旋，後來被[氣旋費林超越](../Page/氣旋費林.md "wikilink")。

## 發展過程

在11月11日，一低氣壓在[布萊爾港之西南形成](../Page/布萊爾港.md "wikilink")。第二天，它增強為一氣旋風暴並被命名為Sidr。同日，它增強為一強烈氣旋風暴。同日，它增強為一特強氣旋風暴。11月15日，它在[西孟加拉邦和](../Page/西孟加拉邦.md "wikilink")[奧里薩邦登陸](../Page/奧里薩邦.md "wikilink")，並減弱為一氣旋風暴。第二天，它減弱為一低氣壓，印度氣象部為它發出最後報告。除為孟加拉帶來嚴重傷亡外，亦使[云南](../Page/云南.md "wikilink")[迪庆州](../Page/迪庆州.md "wikilink")[德钦县境内普降暴雪](../Page/德钦县.md "wikilink")。

## 參考資料

[S](../Category/2007年北印度洋气旋季.md "wikilink")
[气Sidr](../Category/五级热带气旋.md "wikilink")
[Category:極強氣旋風暴](../Category/極強氣旋風暴.md "wikilink")