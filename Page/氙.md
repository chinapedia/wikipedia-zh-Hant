**氙**（[注音](../Page/注音符號.md "wikilink")：ㄒㄧㄢ，[漢語拼音](../Page/漢語拼音.md "wikilink")：xiān；舊譯作**氠**\[1\]、**氥**\[2\]、**𣱧**\[3\]）是一種[化學元素](../Page/化學元素.md "wikilink")，[化學符號為](../Page/化學符號.md "wikilink")**Xe**，[原子序為](../Page/原子序.md "wikilink")54。氙是一種無色、無味的[稀有氣體](../Page/稀有氣體.md "wikilink")。[地球大氣層中含有痕量的氙](../Page/地球大氣層.md "wikilink")。
\[4\]雖然氙的化學活性很低，但是它仍然能夠進行[化學反應](../Page/化學反應.md "wikilink")，例如形成[六氟合鉑酸氙](../Page/六氟合鉑酸氙.md "wikilink")──首個被合成的[稀有氣體化合物](../Page/稀有氣體化合物.md "wikilink")。\[5\]\[6\]\[7\]

自然產生的氙由[8種穩定同位素組成](../Page/氙的同位素.md "wikilink")。氙還有40多種能夠進行[放射性衰變的不穩定同位素](../Page/放射性衰變.md "wikilink")。氙同位素的相對比例對研究[太陽系早期歷史有重要的作用](../Page/太陽系.md "wikilink")。\[8\]具放射性的氙-135是[核反應爐中最重要的](../Page/核反應爐.md "wikilink")[中子吸收劑](../Page/中子吸收劑.md "wikilink")，可通過[碘](../Page/碘.md "wikilink")-135的[核衰变產生](../Page/核衰变.md "wikilink")。\[9\]

氙可用在[閃光燈](../Page/氙閃光燈.md "wikilink")\[10\]和[弧燈中](../Page/氙弧燈.md "wikilink")，\[11\]或作[全身麻醉藥](../Page/全身麻醉藥.md "wikilink")。\[12\]最早的[准分子激光設計以氙的](../Page/准分子激光.md "wikilink")[二聚體分子](../Page/二聚體.md "wikilink")（Xe<sub>2</sub>）作為激光介質，\[13\]而早期[激光設計亦用氙閃光燈作激光抽運](../Page/激光.md "wikilink")。\[14\]氙還可以用來尋找[大質量弱相互作用粒子](../Page/大質量弱相互作用粒子.md "wikilink")\[15\]，或作[航天器](../Page/航天器.md "wikilink")[離子推力器的](../Page/離子推力器.md "wikilink")[推進劑](../Page/推進劑.md "wikilink")。\[16\]

## 歷史

[英國化學家](../Page/英國.md "wikilink")[威廉·拉姆齊和](../Page/威廉·拉姆齊.md "wikilink")[莫里斯·特拉弗斯](../Page/莫里斯·特拉弗斯.md "wikilink")（Morris
Travers）在發現了[氪和](../Page/氪.md "wikilink")[氖後](../Page/氖.md "wikilink")，於1898年7月12日在蒸發液態空氣後的殘留物中發現了氙。\[17\]\[18\]拉姆齊建議把這一新元素命名為「xenon」，源自[希臘語](../Page/希臘語.md "wikilink")「」（xenon），即「」（xenos）的中性單數形，意為外來者、陌生人或異客。\[19\]\[20\]1902年，拉姆齊估算氙在地球大氣中的含量為2千萬分之一。\[21\]

1930年代，[美國工程師](../Page/美國.md "wikilink")[哈羅德·尤金·艾杰頓](../Page/哈羅德·尤金·艾杰頓.md "wikilink")（Harold
Eugene
Edgerton）開始為[高速攝影研究](../Page/高速攝影.md "wikilink")[頻閃燈](../Page/頻閃燈.md "wikilink")，并發明了[氙閃光燈](../Page/氙閃光燈.md "wikilink")。在氙閃光燈中，電流短暫通過含有氙氣的玻璃管，使其發光。到了1934年，艾杰頓已經能夠產生1[微秒長的閃光](../Page/微秒.md "wikilink")。\[22\]\[23\]\[24\]

1939年，美國醫生[阿爾伯特·本克](../Page/阿爾伯特·本克.md "wikilink")（）著手研究深海潛水員有「酒醉感」的原因。他在測試對象所呼吸的氣體中調整各種氣體的比例，并發現潛水員對深度的感覺有所變化。他以此推論，氙氣能夠用於[麻醉](../Page/麻醉.md "wikilink")。[俄羅斯毒理學家尼克拉](../Page/俄羅斯.md "wikilink")·拉薩列夫（）曾在1941年研究過氙麻醉藥，但直到1946年美國醫學家約翰·勞倫斯（）才發表了他對老鼠進行的一項實驗研究，首次證實了氙作為麻醉藥的效用。1951年，美國麻醉師斯圖爾特·科林（Stuart
C. Cullen）第一次使用氙麻醉藥，并成功為兩名病人進行了手術。\[25\]

氙以及其他稀有氣體曾一直被認為是完全惰性的，無法形成[化合物](../Page/化合物.md "wikilink")。不過，化學家[尼爾·巴特萊特](../Page/尼爾·巴特萊特.md "wikilink")（Neil
Bartlett）在[不列顛哥倫比亞大學任教時](../Page/不列顛哥倫比亞大學.md "wikilink")，發現[六氟化鉑](../Page/六氟化鉑.md "wikilink")（PtF<sub>6</sub>）氣體是一種強[氧化劑](../Page/氧化劑.md "wikilink")，能夠氧化[氧氣](../Page/氧氣.md "wikilink")（O<sub>2</sub>），形成[六氟合鉑酸氧](../Page/六氟合鉑酸氧.md "wikilink")（O<sub>2</sub><sup>+</sup>\[PtF<sub>6</sub>\]<sup>–</sup>）。\[26\]因為O<sub>2</sub>和氙的第一[電離能幾乎相同](../Page/電離能.md "wikilink")，所以巴特萊特猜想，氙也有可能可以被六氟化鉑氧化。1962年3月23日，他將這兩種氣體混合，產生了第一種稀有氣體化合物[六氟合鉑酸氙](../Page/六氟合鉑酸氙.md "wikilink")。\[27\]\[28\]他當時認為該氣體產物為Xe<sup>+</sup>\[PtF<sub>6</sub>\]<sup>–</sup>，但之後的分析表明該氣體很可能是多種氙鹽的混合物。\[29\]\[30\]\[31\]此後，許多其他的氙化合物也陸續被發現，\[32\]而同時被發現的還包括[氬](../Page/氬.md "wikilink")、[氪和](../Page/氪.md "wikilink")[氡等稀有氣體的化合物](../Page/氡.md "wikilink")，如[氟氬化氫](../Page/氟氬化氫.md "wikilink")（HArF）、\[33\][二氟化氪](../Page/二氟化氪.md "wikilink")（KrF<sub>2</sub>）\[34\]\[35\]及[二氟化氡](../Page/二氟化氡.md "wikilink")。\[36\]到了1971年，已知的氙化合物已經超過了80種。\[37\]\[38\]

## 性质

[Xenon-flash.jpg](https://zh.wikipedia.org/wiki/File:Xenon-flash.jpg "fig:Xenon-flash.jpg")（[可動圖像版](../Page/:Image:Xenon-flash.gif.md "wikilink")）\]\]
氙的[原子序為](../Page/原子序.md "wikilink")54，即每一个氙原子核中共有54顆[質子](../Page/質子.md "wikilink")。在[標準溫度和壓力下](../Page/標準溫度和壓力.md "wikilink")，純氙氣的密度為5.761 kg/m<sup>3</sup>，也就是地球地面大氣密度（1.217 kg/m<sup>3</sup>）的4.5倍左右。\[39\]當處於液態時，氙的密度可高達3.100 g/mL，最高密度在[三相點處達到](../Page/三相點.md "wikilink")。\[40\]固態氙的密度為3.640 g/cm<sup>3</sup>，比[花崗岩的](../Page/花崗岩.md "wikilink")2.75 g/cm<sup>3</sup>更高。\[41\]當[壓力超過](../Page/壓力.md "wikilink")10億[帕斯卡時](../Page/帕斯卡.md "wikilink")，氙會呈金屬態。\[42\]

在大約140 GPa壓力下，固體氙的晶體結構會從[面心立方轉變為](../Page/面心立方.md "wikilink")[六方密排](../Page/六方密排.md "wikilink")，并開始呈現金屬特性。氙在155 GPa壓力以上完全進入金屬態。這時候的氙會吸收紅光，因此會呈天藍色。這一特性在金屬中較為罕見，原因是氙在金屬態下的電子能帶寬度較小。\[43\]\[44\]

氙的[化合價為](../Page/化合價.md "wikilink")0，與其他零價元素同屬於[稀有氣體](../Page/稀有氣體.md "wikilink")，亦稱惰性氣體。氙對大部份化學反應都呈惰性（如燃燒反應），因為它有8個[價電子](../Page/價電子.md "wikilink")。這使外層電子處於最低能量組態，因此非常穩定。\[45\]

當[電弧通過裝有氙氣的玻璃管時](../Page/電弧.md "wikilink")，氙會被激發而發出藍至淡紫色光。氙的發射[譜線橫跨整個](../Page/譜線.md "wikilink")[可見光譜](../Page/可見光譜.md "wikilink")，\[46\]其最強的光譜線位於藍光部份，所以整體發藍光。\[47\]

## 存量及生產

氙是[地球大氣層中的一種微量氣體](../Page/地球大氣層.md "wikilink")，含量約為10億分之87±1（nL/L），亦即1150萬分之一。\[48\]某些天然礦泉也會釋放出含有氙的氣體。\[49\]\[50\]

氙是空氣氮氧分離過程的副產品。這一過程一般在雙柱式分餾塔中進行，所產生的[液氧中會含有少量的氪和氙](../Page/液氧.md "wikilink")。再進行更多的分餾步驟之後，液氧中的氪和氙含量可以提高至0.1至0.2%。這些氪和氙可以通過[硅膠吸附或蒸餾提取出來](../Page/硅膠.md "wikilink")，混合物再經蒸餾分離成氪和氙。\[51\]\[52\]從大氣層中提取一升氙氣需要220[瓦小時的能量](../Page/千瓦小時.md "wikilink")。\[53\]1998年，全球氙產量為5千至7千 m<sup>3</sup>。\[54\]由於含量稀少，氙的價格比其他更輕的稀有氣體高許多：1999年歐洲的氙氣價格為每升10[歐元](../Page/歐元.md "wikilink")，氪氣每升1歐元，氖氣每升0.20歐元，\[55\]

在[太陽系以內](../Page/太陽系.md "wikilink")，氙元素的[核素比例為](../Page/核素.md "wikilink")，質量[豐度為](../Page/化學元素豐度.md "wikilink")63萬分之一。\[56\][太陽大氣層](../Page/太陽.md "wikilink")、[地球](../Page/地球.md "wikilink")、[小行星及](../Page/小行星.md "wikilink")[彗星中的氙含量很低](../Page/彗星.md "wikilink")。[木星大氣層含有異常高的氙元素](../Page/木星.md "wikilink")，含量約為太陽的2.6倍。\[57\]\[58\]這一現象的原因不詳，但有可能是因為在[太陽系形成早期](../Page/太陽系的形成與演化.md "wikilink")[太陽星雲溫度提升之前](../Page/太陽星雲.md "wikilink")，[微行星迅速堆積所致](../Page/微行星.md "wikilink")。\[59\]地球上的氙存量很低，這可能是由於氙和氧在[石英中產生](../Page/石英.md "wikilink")[共價鍵](../Page/共價鍵.md "wikilink")，從而減少氙被釋入大氣層的量。\[60\]

與輕稀有氣體不同的是，[恒星核合成過程無法製造氙元素](../Page/恒星核合成.md "wikilink")。所有包括氙在內比[鐵-56更重的元素經核聚變合成時](../Page/鐵-56.md "wikilink")，會產生凈能量損失，因此無法在恒星內部形成。\[61\]能夠形成氙的自然過程包括：[超新星爆炸](../Page/超新星.md "wikilink")，\[62\][紅巨星用盡氫燃料進入漸近巨星分支後的慢中子捕獲過程](../Page/紅巨星.md "wikilink")（[s-過程](../Page/s-過程.md "wikilink")），\[63\]一般[新星爆炸](../Page/新星.md "wikilink")，\[64\]以及[碘](../Page/碘.md "wikilink")、[鈾和](../Page/鈾.md "wikilink")[鈈等元素的放射性衰變](../Page/鈈.md "wikilink")。
\[65\]

## 同位素

自然形成的氙共由8種[穩定同位素組成](../Page/穩定同位素.md "wikilink")，在各元素中排第二位。第一位是[錫](../Page/錫.md "wikilink")，其穩定同位素共有10個。穩定同位素數量高於7個的元素只有氙和錫。\[66\]同位素<sup>124</sup>Xe和<sup>134</sup>Xe根據預測能夠進行[雙重β衰變](../Page/雙重β衰變.md "wikilink")，但這未經實驗證明，因此這兩種同位素仍被認為是穩定的。\[67\]除這些穩定同位素之外，氙還有40多種不穩定同位素。其中壽命最長的為<sup>136</sup>Xe，它會進行雙β衰變，[半衰期為](../Page/半衰期.md "wikilink")2.11年。\[68\][<sup>129</sup>I在](../Page/碘.md "wikilink")[β衰變後](../Page/β衰變.md "wikilink")，會產生<sup>129</sup>Xe同位素。該反應的半衰期為1600萬年。另外<sup>131m</sup>Xe、<sup>133</sup>Xe、<sup>133m</sup>Xe和<sup>135</sup>Xe都是[<sup>235</sup>U和](../Page/鈾.md "wikilink")[<sup>239</sup>Pu的](../Page/鈈.md "wikilink")[核裂變產物](../Page/核裂變.md "wikilink")，\[69\]因此被用作探測核爆炸的發生。\[70\]

氙的其中兩種穩定同位素<sup>129</sup>Xe和<sup>131</sup>Xe具有非零的固有[角動量](../Page/角動量.md "wikilink")（[自旋](../Page/自旋.md "wikilink")，可用於[核磁共振](../Page/核磁共振.md "wikilink")）。利用圓[極化光和](../Page/極化光.md "wikilink")[銣氣體](../Page/銣.md "wikilink")，氙的核自旋對齊可以超越普通的極化。\[71\]如此產生的[自旋極化能夠超過其最高可能值的](../Page/自旋極化.md "wikilink")50%，遠遠大於[玻爾茲曼分佈的平衡值](../Page/玻爾茲曼分佈.md "wikilink")（在[室溫下通常不超過最高值的](../Page/室溫.md "wikilink")0.001%）。這種非平衡態的自旋對齊是短暫的，稱為[超極化現象](../Page/超極化_\(物理學\).md "wikilink")。對氙進行超極化的過程叫做光抽運（但不同於激光抽運）。\[72\]

由於<sup>129</sup>Xe原子核的自旋為1/2，所以其[電](../Page/電場.md "wikilink")[四極矩為零](../Page/四極矩.md "wikilink")，故<sup>129</sup>Xe核在與其他原子撞擊時，不會有任何四極相互作用。這使得它的超極化狀態能夠持續更長的時間，甚至在激光束關閉及鹼氣體在室溫表面冷凝後，仍能保留該狀態。<sup>129</sup>Xe的自旋極化在[血液中能持續數秒](../Page/血液.md "wikilink")，<ref>{{
cite journal

`|first=J.|last=Wolber`
`|coauthors=Cherubini, A.; Leach, M. O.; Bifone, A.`
`|title = On the oxygenation-dependent `<sup>`129`</sup>`Xe `*`T`*<sub>`1`</sub>` in blood`
`|year = 2000|journal = NMR in Biomedicine`
`|volume = 13|issue = 4|pages = 234–7`
`|doi = 10.1002/1099-1492(200006)13:4<234::AID-NBM632>3.0.CO;2-K`
`|pmid=10867702}}`</ref>`在`[`氣態下持續數小時`](../Page/氣態.md "wikilink")`，`<ref>`{{ cite journal`
`|first=B.|last=Chann|coauthors=Nelson, I. A.; Anderson, L. W.; Driehuys, B.; Walker, T. G.`
`|title=`<sup>`129`</sup>`Xe-Xe molecular spin relaxation`
`|year=2002|journal=Physical Review Letters`
`|volume=88|issue=11|pages=113–201`
`|doi=10.1103/PhysRevLett.88.113201 |bibcode=2002PhRvL..88k3201C}}`</ref>`并在深度冷凍的固態下持續數天。`\[73\]`相比之下，`<sup>`131`</sup>`Xe的核自旋為3/2，四極矩不為零，其`*`T`*<sub>`1`</sub>`弛豫時間位於`[`毫秒至`](../Page/毫秒.md "wikilink")[`秒區間內`](../Page/秒.md "wikilink")`。`\[74\]

氙的某些同位素，如<sup>133</sup>Xe和<sup>135</sup>Xe，可在[核反應爐中對](../Page/核反應爐.md "wikilink")[可以裂變物質進行](../Page/可以裂變物質.md "wikilink")[中子照射產生](../Page/中子.md "wikilink")。\[75\]<sup>135</sup>Xe在核裂變反應爐中具有重要的作用。<sup>135</sup>Xe的[熱中子](../Page/熱中子.md "wikilink")[截面很高](../Page/中子截面.md "wikilink")（2.6×10<sup>6</sup>[靶恩](../Page/靶恩.md "wikilink")），\[76\]因此可用作[中子吸收劑或](../Page/中子吸收劑.md "wikilink")[中子毒物](../Page/中子毒物.md "wikilink")，從而減慢或停止連鎖反應。美國[曼哈頓計劃中用來產生](../Page/曼哈頓計劃.md "wikilink")[鈈元素的最早期反應爐就用到了氙的這一作用](../Page/鈈.md "wikilink")。\[77\]<sup>135</sup>Xe在反應爐中作為中子毒物，對[切爾諾貝爾核事故有著重要的影響](../Page/切爾諾貝爾核事故.md "wikilink")。\[78\]反應爐的關閉或功率的降低可以造成<sup>135</sup>Xe的積聚，使反應爐進入所謂的[氙中毒狀態](../Page/氙中毒.md "wikilink")（又稱氙坑、碘坑）。\[79\]\[80\]

在不利條件下，高濃度的放射性氙同位素可以從核反應爐中釋放出來，來源包括裂變產物從開裂的[燃料棒中釋出](../Page/燃料棒.md "wikilink")，\[81\]或冷卻水中的鈾進行裂變。\[82\]

[隕石中的氙同位素比例可以用來研究](../Page/隕石.md "wikilink")[太陽系的形成和演化](../Page/太陽系的形成和演化.md "wikilink")。[碘氙](../Page/碘氙定年法.md "wikilink")[放射性定年法可以測定](../Page/放射性定年法.md "wikilink")[核合成至](../Page/核合成.md "wikilink")[太陽星雲中固體物體縮合之間的時間](../Page/太陽星雲.md "wikilink")。1960年，物理學家[約翰·雷諾](../Page/約翰·雷諾.md "wikilink")（John
H.
Reynolds）發現某些[隕石中的氙](../Page/隕石.md "wikilink")-129含量異常高。他推斷這是碘-129的[衰變產物](../Page/衰變產物.md "wikilink")。這一同位素可經[宇宙射線散裂和](../Page/宇宙射線散裂.md "wikilink")[核裂變緩慢產生](../Page/核裂變.md "wikilink")，但只有在[超新星爆炸中才能大量產生](../Page/超新星.md "wikilink")。由於<sup>129</sup>I的半衰期（1600萬年）相對宇宙時長來說非常短，因此可推論從超新星爆炸到隕石凝固之間經過的時間很短。一顆超新星在[太陽系形成前不久爆炸](../Page/太陽系.md "wikilink")，產生<sup>129</sup>I同位素之餘，可能也導致了前太陽氣體雲的收縮。\[83\]\[84\]

利用類似的方法，其他氙同位素比例也可以用來研究行星分化和氣體釋放過程，包括<sup>129</sup>Xe/<sup>130</sup>Xe和<sup>136</sup>Xe/<sup>130</sup>Xe。\[85\]例如，[火星大氣層的氙含量與地球相似](../Page/火星大氣層.md "wikilink")，約為百萬分之0.08，\[86\]但其<sup>129</sup>Xe比例比地球和太陽高。這一同位素是由放射性衰變產生的，所以火星很可能在形成後約1億年以內喪失了大部份的原始大氣。\[87\]\[88\]美國[新墨西哥州](../Page/新墨西哥州.md "wikilink")[二氧化碳井氣中所發現的高比例](../Page/二氧化碳.md "wikilink")<sup>129</sup>Xe是地球形成不久後經[地幔核衰變產生的氣體之一](../Page/地幔.md "wikilink")。\[89\]\[90\]

## 化合物

尼爾·巴特萊特在1962年發現氙能夠形成化合物之後，許多其他的氙化合物也陸續被發現和研究。幾乎所有氙化合物都含有[電負性高的](../Page/電負性.md "wikilink")[氟或者](../Page/氟.md "wikilink")[氧](../Page/氧.md "wikilink")。\[91\]

### 鹵化物

[Xenon-tetrafluoride-3D-vdW.png](https://zh.wikipedia.org/wiki/File:Xenon-tetrafluoride-3D-vdW.png "fig:Xenon-tetrafluoride-3D-vdW.png")|alt=平面型分子模型，中間為氙原子，與四個氟原子對稱鍵合。\]\]
[Xenon_tetrafluoride.JPG](https://zh.wikipedia.org/wiki/File:Xenon_tetrafluoride.JPG "fig:Xenon_tetrafluoride.JPG")

氙共有三種已知[氟化物](../Page/氟化物.md "wikilink")：[二氟化氙](../Page/二氟化氙.md "wikilink")（）、[四氟化氙](../Page/四氟化氙.md "wikilink")（）及[六氟化氙](../Page/六氟化氙.md "wikilink")（）。理論預測XeF是不穩定的。\[92\]幾乎所有氙化合物都含有電負性原子氟或氧。\[93\]

二氟化氙（）是一種固體晶體，在氟與氙混合物經[紫外光照射後形成](../Page/紫外光.md "wikilink")，\[94\]（使用一般日光含的紫外光就已足夠。）\[95\]在高溫下用[催化劑長時間加溫會產生](../Page/催化劑.md "wikilink")。\[96\]在[NaF中經](../Page/氟化鈉.md "wikilink")[熱裂解後可以形成高純度](../Page/熱裂解.md "wikilink")。\[97\]

氙的氟化物都可以作為氟離子受體和予體，形成如和等正離子，以及、和等負離子。經氙氣還原後，會形成綠色的[順磁性](../Page/順磁性.md "wikilink")離子。\[98\]

可以與[過渡金屬離子形成](../Page/過渡金屬.md "wikilink")[配合物](../Page/配合物.md "wikilink")。已知的配合物已超過30種。\[99\]

雖然人們對氙的氟化物已有一定的了解，但是對其他的鹵化物則幾乎一無所知。唯一已知的鹵化物為[二氯化氙](../Page/二氯化氙.md "wikilink")（XeCl<sub>2</sub>）。二氯化氙是一種吸熱的無色晶體，在80°C以上會分解成其組成元素。對氙、氟及[四氯化硅或](../Page/四氯化硅.md "wikilink")[四氯化碳的混合物進行高頻率光照射會形成二氯化氙](../Page/四氯化碳.md "wikilink")。\[100\]但是人們未知是確實的化合物，還是由氙原子和分子弱結合形成的[范德華分子](../Page/范德華分子.md "wikilink")。\[101\]理論計算指出，直線型分子比范德華分子較不穩定。\[102\]

### 氧化物及鹵氧化物

氙共有三種已知氧化物：[二氧化氙](../Page/二氧化氙.md "wikilink")（）、[三氧化氙](../Page/三氧化氙.md "wikilink")（）及[四氧化氙](../Page/四氧化氙.md "wikilink")（）。二氧化氙在2011年被發現，[配位數為](../Page/配位數.md "wikilink")4，\[103\]XeO<sub>2</sub>是在四氟化氙與水冰反應後形成的。其晶體結構特殊，有可能能夠取代[硅酸鹽礦物中的](../Page/硅酸鹽.md "wikilink")[硅](../Page/硅.md "wikilink")。\[104\]科學家利用[紅外光譜分析在固體](../Page/紅外光譜學.md "wikilink")[氬當中發現了XeOO](../Page/氬.md "wikilink")<sup>+</sup>正離子。\[105\]固態三氧化氙和四氧化氙都是爆炸性很強的物質。\[106\]\[107\]

氙不會和氧直接進行反應。三氧化氙是經的[水解反應形成的](../Page/水解.md "wikilink")：\[108\]

  -
    <ce>{XeF6}+3{H2O}-\>{XeO3}+6HF</ce>

具弱酸性，會在鹼中溶解成含負離子的不穩定氙酸鹽。這些不穩定鹽會很快[歧化成氙氣和含](../Page/歧化反應.md "wikilink")負離子的[高氙酸鹽](../Page/高氙酸鹽.md "wikilink")。\[109\]

高氙酸鈉或高氙酸鋇在濃[硫酸中會產生淺黃色固態四氧化氙](../Page/硫酸.md "wikilink")：\[110\]

  -
    <ce>{Ba2XeO6}+2{H2SO4}-\>2{BaSO4}+2{H2O}+{XeO4}</ce>

氙有多個已知的氟氧化物，包括二氟一氧化氙（）、[四氟一氧化氙](../Page/四氟一氧化氙.md "wikilink")（）、二氟二氧化氙（）及二氟三氧化氙（）。[二氟化氧](../Page/二氟化氧.md "wikilink")（）與氙氣在低溫下反應會形成；的部份水解也可產生。它在−20℃以上會歧化成和。\[111\]的部份水解會產生\[112\]；與高氙酸鈉（）反應後也可形成。第二種反應會同時產生少量的。與[CsF反應後會形成](../Page/氟化銫.md "wikilink")負離子，\[113\]\[114\]而XeOF<sub>3</sub>會和[KF](../Page/氟化鉀.md "wikilink")、[RbF和CsF反應形成](../Page/氟化銣.md "wikilink")負離子。\[115\]

氙的氟化物都与水发生反应，XeF<sub>2</sub>在水中的溶解度为0.15 mol/L(273K)，能将水氧化。

  -
    <ce>{2XeF_2} + 2H_2O = {2Xe} + {4HF} + O_2</ce>

XeF<sub>4</sub>与水的反应中，一半用于氧化水，另外一半发生了歧化反应。总反应为:

  -
    <ce>{6XeF_4} + 12H_2O = {2XeO_3} + {4Xe} +{3O_2} + 24HF</ce>

XeF<sub>6</sub>有强氧化性，但由于生成的XeO<sub>3</sub>在水中溶解且稳定，XeF<sub>6</sub>在水中只发生水解反应。

  -
    <ce>{XeF_6} + H_2O = {XeOF_4} + 2HF</ce>
    <ce>{XeOF_4} + 2H_2O ={XeO_3} + 4HF</ce>

### 其他化合物

科學家近期開始研究氙能否與電負性比氟和氧低的元素形成化合物，其中特別包括[碳](../Page/碳.md "wikilink")。\[116\]要使這些化合物穩定，必須使用吸電子基團，如經氟取代形成的基團。\[117\]已知的含碳化合物包括：\[118\]\[119\]

  - ，其中的C<sub>6</sub>F<sub>5</sub>是五氟苯基。

  -
  - ，X可以是[CN](../Page/腈.md "wikilink")、F或Cl。

  - ，R可以是或[叔丁基](../Page/丁基.md "wikilink")。

  -
  -
其他含有電負性較低的氙化合物包括和。可以從四氟硼酸[二氧](../Page/二氧基.md "wikilink")在−100 °C溫度下合成。\[120\]\[121\]

[四氙合金(II)離子](../Page/四氙合金\(II\)離子.md "wikilink")（）非常特殊，它含有氙﹣金鍵。\[122\]氙和金都是極不活躍的元素，成鍵時以氙作為過渡金屬配位體。之一離子出現在當中。

含有氙﹣氙鍵，這是兩個元素間已知最長的鍵（308.71 [pm](../Page/皮米.md "wikilink") = 3.0871
[Å](../Page/埃.md "wikilink")）。\[123\]

1995年，[芬蘭](../Page/芬蘭.md "wikilink")[赫爾辛基大學的馬爾庫](../Page/赫爾辛基大學.md "wikilink")·拉薩寧（Markku
Räsänen）等人宣佈成功合成HXeH，并其後宣佈合成HXeOH、HXeCCH以及其他氙化合物分子。\[124\]2008年，利奧尼德·赫里亞切夫（）等人宣佈，他們在低溫氙基體內對水進行[光解後合成了HXeOXeH](../Page/光解.md "wikilink")。\[125\]他們也合成了含[氘的分子](../Page/氘.md "wikilink")，如HXeOD和DXeOH。\[126\]

### 包合物及準分子

除了可以在化合物中形成[化學鍵之外](../Page/化學鍵.md "wikilink")，氙原子還能嵌在另一種化合物的[晶體結構當中](../Page/晶體結構.md "wikilink")，形成[包合物](../Page/包合物.md "wikilink")。這包括[水合氙](../Page/水合氙.md "wikilink")（Xe·5.75
H<sub>2</sub>O），其中氙原子位於水分子形成的晶體結構空隙中。\[127\]這種包合物的熔點為24 °C。\[128\]科學家也改用[氘合成了該包合物](../Page/氘.md "wikilink")。\[129\]這類[水合包合物可以在高壓條件下自然形成](../Page/水合包合物.md "wikilink")，例如[南極洲冰蓋下的](../Page/南極洲.md "wikilink")[沃斯托克湖](../Page/沃斯托克湖.md "wikilink")。\[130\]包合物的形成可以用在分餾過程中，以分離氙、氬和氪。\[131\]

氙還可以形成[內嵌富勒烯化合物](../Page/內嵌富勒烯.md "wikilink")，即內嵌氙原子的[富勒烯分子](../Page/富勒烯.md "wikilink")。富勒烯裡面的氙原子可以通過<sup>129</sup>Xe[核磁共振光譜分析來觀測](../Page/核磁共振.md "wikilink")。科學家可利用這種方法分析富勒烯分子的化學反應，因為其中的氙原子對周圍環境十分敏感，并進行[化學位移](../Page/化學位移.md "wikilink")。然而，氙原子也會對富勒烯的化學活性產生影響。\[132\]

當氙原子處於[基態的時候](../Page/基態.md "wikilink")，會互相排斥，無法成鍵。但當它們受到激發後，就能夠形成[準分子](../Page/準分子.md "wikilink")（激發態二聚體），直到電子回到基態。氙原子一般會填滿其最外[電子層](../Page/電子層.md "wikilink")，鄰近的氙原子就可以為其提供電子。氙準分子的一般存留時長為1至5納秒，其衰變會釋放[波長約為](../Page/波長.md "wikilink")150和173納米的[光子](../Page/光子.md "wikilink")。\[133\]\[134\]氙還能與其他元素結合成準分子，例如[溴](../Page/溴.md "wikilink")、[氯及](../Page/氯.md "wikilink")[氟](../Page/氟.md "wikilink")。\[135\]

## 應用

### 照明及光學

#### 氣體放電燈

氙可用於發光，應用包括：用於攝影的氙[閃光燈](../Page/閃光燈.md "wikilink")，\[136\]激發[激光媒介以產生](../Page/激光媒介.md "wikilink")[相干光](../Page/相干性.md "wikilink")，以及\[137\]殺菌燈等。\[138\]1960年發明的首個固態[激光器](../Page/激光器.md "wikilink")\[139\]及推動[慣性約束聚變的激光器都用到了氙閃光燈作激光抽運](../Page/慣性約束聚變.md "wikilink")。\[140\]

[Xenon_short_arc_1.jpg](https://zh.wikipedia.org/wiki/File:Xenon_short_arc_1.jpg "fig:Xenon_short_arc_1.jpg")
[STS-135_Atlantis_rollout_1.jpg](https://zh.wikipedia.org/wiki/File:STS-135_Atlantis_rollout_1.jpg "fig:STS-135_Atlantis_rollout_1.jpg")\]\]
[Xenon_discharge_tube.jpg](https://zh.wikipedia.org/wiki/File:Xenon_discharge_tube.jpg "fig:Xenon_discharge_tube.jpg")
[氙弧燈能夠連續發光](../Page/氙弧燈.md "wikilink")，其[色溫近似於正午的日光](../Page/色溫.md "wikilink")，因此被用於模擬陽光。1940年氙弧燈進入市場後，開始淘汰壽命較短的[碳弧燈作為](../Page/碳弧燈.md "wikilink")[電影放映機的光源](../Page/電影放映機.md "wikilink")。\[141\]這種光源被用在一般[35毫米膠片](../Page/35毫米膠片.md "wikilink")、[IMAX和新型](../Page/IMAX.md "wikilink")[數碼投影機的電影投影](../Page/數碼投影機.md "wikilink")、[高強度氣體放電燈車頭燈](../Page/高強度氣體放電燈.md "wikilink")、高端戰術電筒以及其他專業用途。這種弧燈能發出短波長[紫外線](../Page/紫外線.md "wikilink")，以及可被用於[夜視鏡的近](../Page/夜視鏡.md "wikilink")[紅外線](../Page/紅外線.md "wikilink")。

[等離子顯示器中的發光體裝有氙和氖](../Page/等離子顯示器.md "wikilink")，并經[電極轉化成](../Page/電極.md "wikilink")[等離子狀態](../Page/等離子.md "wikilink")。該等離子體與電極之間的作用會產生紫外光，從而激發顯示器前部的[磷質塗層](../Page/磷.md "wikilink")，發出可見光。\[142\]\[143\]

氙也被用於啟動高壓[鈉燈](../Page/鈉燈.md "wikilink")。氙的[熱導率和](../Page/熱導率.md "wikilink")[電離能是所有非放射性稀有氣體中最低的](../Page/電離能.md "wikilink")。其化學惰性能避免對化學反應的干預；低熱導率可降低燈在運作時的熱能損失；低電離能則使氙在非高溫狀態下的[擊穿電壓相對較低](../Page/擊穿電壓.md "wikilink")，令燈更容易啟動。\[144\]

#### 激光

1962年，[貝爾實驗室研究人員發現了氙的激光作用](../Page/貝爾實驗室.md "wikilink")，\[145\]又接著發現在激光介質中加入[氦能夠提升激光增益](../Page/氦.md "wikilink")。\[146\]\[147\]首個[準分子激光使用](../Page/準分子激光.md "wikilink")[電子來激發氙的](../Page/電子.md "wikilink")[二聚體](../Page/二聚體.md "wikilink")（Xe<sub>2</sub>），以產生波長為176納米的紫外光，該過程稱為[受激發射](../Page/受激發射.md "wikilink")。\[148\]氯化氙和氟化氙準分子也可用於激光器中。\[149\]例如，皮膚病學就用到氯化氙準分子激光。\[150\]

### 醫學

#### 麻醉

氙是一種[全身麻醉劑](../Page/全身麻醉劑.md "wikilink")。氙較為昂貴，但由於回收循環技術的提升和成本的降低，使用氙的麻醉機將在不久後進入歐洲市場。\[151\]\[152\]

氙會和多種不同[受體和](../Page/受體_\(生物化學\).md "wikilink")[離子通道相互作用](../Page/離子通道.md "wikilink")。根據理論，這種多模態吸入性麻醉劑很可能具互補性。氙是一種具高親和力的[甘氨酸結合部位](../Page/甘氨酸.md "wikilink")[NMDA受體拮抗劑](../Page/NMDA受體拮抗劑.md "wikilink")。\[153\]不過，與其他的NMDA受體拮抗劑不同的是，氙不具神經毒性，且能夠抑制[氯胺酮和](../Page/氯胺酮.md "wikilink")[一氧化二氮的神經毒性](../Page/一氧化二氮.md "wikilink")。\[154\]\[155\]氙不會像氯胺酮和一氧化二氮一樣刺激[伏隔核釋放](../Page/伏隔核.md "wikilink")[多巴胺](../Page/多巴胺.md "wikilink")。\[156\]

氙是[血清素](../Page/血清素.md "wikilink")5-HT3的競爭性抑製劑。這並不產生麻醉或鎮痛的效果，但可以減少麻醉劑相關的噁心和嘔吐感。\[157\]

氙在40歲人體內的[最小肺泡濃度為](../Page/最小肺泡濃度.md "wikilink")72%，所以麻醉效果比N<sub>2</sub>O強44%。\[158\]因此相對氧氣的使用濃度無需太高，有助避免[缺氧](../Page/缺氧.md "wikilink")。另外與一氧化二氮不同的是，氙不是[溫室氣體](../Page/溫室氣體.md "wikilink")，所以較為環保。\[159\]

#### 成像

[放射性同位素](../Page/放射性同位素.md "wikilink")<sup>133</sup>Xe的[伽馬射線可用來對心](../Page/伽馬射線.md "wikilink")、肺和腦進行成像，例如[單光子發射電腦攝影](../Page/單光子發射電腦攝影.md "wikilink")。<sup>133</sup>Xe也被用於測量[血流](../Page/血流.md "wikilink")。\[160\]\[161\]\[162\]

氙是一種很好的[磁共振成像](../Page/磁共振成像.md "wikilink")（MRI）[造影劑](../Page/造影劑.md "wikilink")。氙氣可以用來對多孔組織的空間和肺泡進行成像。[超極化的](../Page/超極化_\(物理學\).md "wikilink")<sup>129</sup>Xe同位素在磁共振成像儀中更易檢測，所以被用於研究包括肺在內的各種器官，例如肺內氣體的流動。\[163\]\[164\]氙可溶於水，又可溶於[疏水性溶劑](../Page/疏水性.md "wikilink")，這有助於對軟組織進行成像。\[165\]\[166\]\[167\]

### 核磁共振波譜法

由於氙擁有較大、較敏感的外電子層，所以其核磁共振光譜會對氙原子周圍的化學條件有相應的變化。例如，溶於水、疏水性溶劑和某些[蛋白質的氙可通過](../Page/蛋白質.md "wikilink")[核磁共振波譜法區分開來](../Page/核磁共振波譜法.md "wikilink")。\[168\]\[169\]

氙也應用在[表面科學中](../Page/表面科學.md "wikilink")。核磁共振一般很難檢測樣本的表面，因為表面底下的大量原子核會完全蓋過有用的信號。超極化的氙氣能夠將自身的[自旋只傳遞到固體表面](../Page/自旋.md "wikilink")，使表面所發出的信號可以與樣本內部的信號區分開來。\[170\]\[171\]

### 其他

[原子核物理學的](../Page/原子核物理學.md "wikilink")[氣泡室可以使用氙](../Page/氣泡室.md "wikilink")。\[172\]氙也可用於任何需要高分子（原子）質量、低反應性物質的用途。[核武器試驗所產生的副產品中有具放射性的氙](../Page/核武器.md "wikilink")-133和氙-135。通過測量這些同位素，人們可以判斷是否有國家進行核試驗，\[173\]其中包括[朝鮮](../Page/朝鮮.md "wikilink")。\[174\]

[Xenon_ion_engine_prototype.png](https://zh.wikipedia.org/wiki/File:Xenon_ion_engine_prototype.png "fig:Xenon_ion_engine_prototype.png")[噴氣對進實驗室進行測試的氙離子發動機](../Page/噴氣對進實驗室.md "wikilink")。\]\]
科學家利用液態氙[熱量計](../Page/熱量計.md "wikilink")\[175\]來測量[伽馬射線](../Page/伽馬射線.md "wikilink")，并用液態氙尋找[大質量弱相互作用粒子](../Page/大質量弱相互作用粒子.md "wikilink")（WIMP）。理論預測，當WIMP撞擊氙原子核，會移除一顆電子，產生閃爍。如果使用氙，這一閃爍可以輕易地從其他由[宇宙射線所造成的能量爆發分辨開來](../Page/宇宙射線.md "wikilink")。\[176\]不過，意大利[大薩索國家實驗室](../Page/大薩索國家實驗室.md "wikilink")（Laboratori
Nazionali del Gran Sasso）的「XENON」實驗以及英國伯比地底實驗室（Boulby Underground
Laboratory）的ZEPLIN-II和ZEPLIN-III實驗都還沒有找到證實WIMP存在的證據。雖然沒有發現WIMP，但這些實驗有助於縮小[暗物質的可能屬性範圍](../Page/暗物質.md "wikilink")，以及改進相關的物理模型。\[177\]\[178\]

氙的[電離能很低](../Page/電離能.md "wikilink")，是一種很好的[航天器](../Page/航天器.md "wikilink")[離子推力器](../Page/離子推力器.md "wikilink")[推進劑](../Page/推進劑.md "wikilink")。氙在[室溫下能夠以液態儲存](../Page/室溫.md "wikilink")，在推力器運作時可輕易轉化為氣體。由於氙的化學惰性，它不會對環境造成破壞，或像[汞或](../Page/汞.md "wikilink")[銫等其他燃料一樣侵蝕離子推進器](../Page/銫.md "wikilink")。1970年代，某些[人造衛星開始使用氙離子推進器](../Page/人造衛星.md "wikilink")。\[179\]美國的[深空一號和](../Page/深空一號.md "wikilink")[曙光號探測器以及歐洲的](../Page/曙光號.md "wikilink")[SMART-1飛行器都用到了氙離子推進器](../Page/SMART-1.md "wikilink")。\[180\]\[181\]

[高氙酸鹽可在](../Page/高氙酸鹽.md "wikilink")[分析化學中用作](../Page/分析化學.md "wikilink")[氧化劑](../Page/氧化劑.md "wikilink")。[二氟化氙是一種](../Page/二氟化氙.md "wikilink")[硅的腐蝕劑](../Page/硅.md "wikilink")，應用在[微機電系統中](../Page/微機電系統.md "wikilink")。\[182\]二氟化氙與[尿嘧啶反應後](../Page/尿嘧啶.md "wikilink")，會產生抗癌藥物[5-氟尿嘧啶](../Page/5-氟尿嘧啶.md "wikilink")。\[183\]氙在[X射線晶體學中可用來研究](../Page/X射線晶體學.md "wikilink")[蛋白質的結構和功用](../Page/蛋白質.md "wikilink")。氙氣在壓力為0.5至5 [MPa](../Page/帕斯卡.md "wikilink")（5至50 [atm](../Page/標準大氣壓.md "wikilink")）的時候，其原子會結合到蛋白質晶體的[疏水性孔穴中](../Page/疏水性.md "wikilink")。這一產物含有更高質量的原子，但不改變原先的晶體結構，因此可被用於解[相位問題](../Page/相位問題.md "wikilink")。\[184\]\[185\]

<div style="clear:both;">

</div>

## 安全

許多含氧的氙化合物都是具有毒性的強氧化劑。同時因為很容易分解成氙元素和氧分子（O<sub>2</sub>），這些化合物還具有爆炸性。\[186\]

氙氣在[標準溫度和壓力下可以安全地存放在一般的玻璃或金屬容器中](../Page/標準溫度和壓力.md "wikilink")。由於氙可溶於大部份[塑料和](../Page/塑料.md "wikilink")[橡膠](../Page/橡膠.md "wikilink")，因此會從這些材料的容器中慢慢逃逸出去。\[187\]氙本身並不具毒性，但它可溶於血，並且可以穿透[血腦屏障](../Page/血腦屏障.md "wikilink")。氙與氧氣混合後吸入，可以達到手術[麻醉劑的效果](../Page/麻醉劑.md "wikilink")。\[188\]

氙氣中的[音速為每秒](../Page/音速.md "wikilink")169米，比空氣中的音速低。這是由於氙原子較氧和氮分子重，因此平均速度較低。當[聲道中充滿氙氣時](../Page/聲道.md "wikilink")，共振頻率會降低。因此吸入氙氣後說話的音色會比正常低沉，與吸入[氦氣後音色提高的現象相反](../Page/氦氣.md "wikilink")。氙的麻醉效果比[一氧化二氮強](../Page/一氧化二氮.md "wikilink")，而過量吸入氙氣也會造成窒息。因此，許多大學在進行有關氣體改變音色的化學演示時，已不再使用氙氣，而改用分子量相近的[六氟化硫氣體](../Page/六氟化硫.md "wikilink")。雖然過量吸入六氟化硫仍會造成窒息，但是它不具麻醉效果。\[189\]

如果氙氣與氧氣混合，而氧氣含量至少有20%，那人體是可以安全吸入的。80%氙氣和20%氧氣的混合氣體會迅速使人失去意識，因此在醫學手術中被用作[全身麻醉劑](../Page/全身麻醉劑.md "wikilink")。呼吸作用會有效地混合不同密度的氣體，所以較重的氙氣並不會積聚在肺的底部，而是會和其他氣體一起呼出。\[190\]然而如果大量氙氣在密閉空間中洩漏出來，會在底部積聚。由於氙無色、無味，所以當人員進入該空間時，很可能會不經意地吸入大量的氙氣。一般的氙氣儲存量并不足以導致這種情況的發生，但在任何缺乏通風的空間中存放氙都具有以上的潛在危險。\[191\]

## 備註

<references group="注" />

## 參考資料

## 外部連結

  - [Xenon](http://www.periodicvideos.com/videos/054.htm) at *The
    Periodic Table of Videos*（諾丁漢大學）
  - [WebElements.com –
    Xenon](http://www.webelements.com/webelements/elements/text/Xe/index.html)
  - [USGS Periodic Table –
    Xenon](http://wwwrcamnl.wr.usgs.gov/isoig/period/xe_iig.html)
  - [EnvironmentalChemistry.com –
    Xenon](http://environmentalchemistry.com/yogi/periodic/Xe.html)
  - [Xenon as an
    anesthetic](http://www.anaesthetist.com/anaes/drugs/xenon.htm)
  - [Sir William Ramsay's Nobel-Prize lecture
    (1904)](http://nobelprize.org/nobel_prizes/chemistry/laureates/1904/ramsay-lecture.html)

[氙](../Category/氙.md "wikilink")
[Category:稀有气体](../Category/稀有气体.md "wikilink")
[5R](../Category/第5周期元素.md "wikilink")
[5R](../Category/化学元素.md "wikilink")
[Category:火箭推进剂](../Category/火箭推进剂.md "wikilink")
[Category:N-甲基-D-天冬氨酸受体拮抗剂](../Category/N-甲基-D-天冬氨酸受体拮抗剂.md "wikilink")

1.  [異體字字典](http://dict.variants.moe.edu.tw/yitib/frb/frb02003.htm)

2.  [異體字字典](http://dict.variants.moe.edu.tw/yitic/frc/frc06056.htm)

3.  [異體字字典](http://dict.variants.moe.edu.tw/yitin/frn/frn00433.htm)

4.

5.

6.  —National Standard Reference Data Service of the USSR. Volume 10.

7.
8.
9.
10.
11.
12.

13.
14.
15.
16.
17.

18.

19.

20.

21.

22.
23.

24.

25.

26.

27.

28.

29.

30. ; translation of *Lehrbuch der Anorganischen Chemie*, originally
    founded by A. F. Holleman, [continued by Egon
    Wiberg](http://books.google.com/books?id=vEwj1WZKThEC&pg=PA395),
    edited by Nils Wiberg, Berlin: de Gruyter, 1995, 34th edition, ISBN
    978-3-11-012641-9.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.
42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.
56.

57.

58. 計算所用的太陽系平均原子質量為1.29 [amu](../Page/原子質量單位.md "wikilink")。

59.

60.

61.

62.

63.

64.

65.
66.

67.

68.
69.

70.

71.

72.

73.

74.

75.
76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.

88.

89.
90.

91.
92.

93.

94.

95.

96.

97.

98.

99.
100.

101.

102.

103.

104.

105.

106.

107.

108.

109.

110.
111.

112.

113.
114. {{ cite journal | title = On the Structure of the
     \[XeOF<sub>5</sub>\]<sup>−</sup> Anion and of Heptacoordinated
     Complex Fluorides Containing One or Two Highly Repulsive Ligands or
     Sterically Active Free Valence Electron Pairs | author = K. O.
     Christe, D. A. Dixon, J. C. P. Sanders, G. J. Schrobilgen, S. S.
     Tsai, W. W. Wilson | journal = Inorg. Chem. | year = 1995 | volume
     = 34 | issue = 7 | pages = 1868–1874 | doi = 10.1021/ic00111a039 }}

115. {{ cite journal | title = Chlorine trifluoride oxide. V. Complex
     formation with Lewis acids and bases | author = K. O. Christe, C.
     J. Schack, D. Pilipovich | journal = Inorg. Chem. | year = 1972 |
     volume = 11 | issue = 9 | pages = 2205–2208 | doi =
     10.1021/ic50115a044 }}

116.

117.
118.
119.

120.
121.

122.

123.

124.

125.

126.

127.  Reprinted as

128.

129.

130.

131.

132.

133.

134.

135.

136.

137.

138.

139.

140.

141.

142.

143.

144.

145.

146.

147.

148.

149.

150.

151.
152.

153.

154.

155.

156.

157.

158.

159.

160.

161.

162.

163.

164.

165.

166.

167.

168.

169.

170.

171.

172.

173.

174.

175.

176.

177.

178.

179.

180.

181.

182.

183.

184.

185.

186.

187.

188.
189.

190.

191.