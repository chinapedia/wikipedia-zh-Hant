**氢化钠**，[化学式为NaH](../Page/化学式.md "wikilink")，是一种[无机盐](../Page/无机盐.md "wikilink")。[有机合成中](../Page/有机合成.md "wikilink")，氢化钠主要被用作强[碱](../Page/碱.md "wikilink")。氢化钠是盐类[氢化物的典型代表](../Page/氢化物.md "wikilink")，即其是由Na<sup>+</sup>和H<sup>−</sup>组成的，不同于[硼烷](../Page/硼烷.md "wikilink")、[甲烷](../Page/甲烷.md "wikilink")、[氨和](../Page/氨.md "wikilink")[水之类的分子型氢化物](../Page/水.md "wikilink")。氢化钠不溶于有机[溶剂](../Page/溶剂.md "wikilink")，溶于[熔融金属](../Page/熔融.md "wikilink")[钠](../Page/钠.md "wikilink")，因此几乎所有与氢化钠有关的反应都于固体表面发生。

## 性质

氢化钠可由[氢气和钠在高温下](../Page/氢气.md "wikilink")[化合形成](../Page/化合反应.md "wikilink")。\[1\]
纯的氢化钠是无色的，然而一般制得的氢化钠会多少带些灰色。

和[氢化锂](../Page/氢化锂.md "wikilink")、[氢化钾](../Page/氢化钾.md "wikilink")、[氢化铷和](../Page/氢化铷.md "wikilink")[氢化铯类似](../Page/氢化铯.md "wikilink")，氢化钠采取氯化钠型[结构](../Page/晶体结构.md "wikilink")，每一个Na<sup>+</sup>被六个H<sup>−</sup>包围形成[八面体](../Page/八面体.md "wikilink")。

## 应用

氢化钠是有机合成中用途很广泛的强碱(pKa\~36)。\[2\]
它可以夺取很多化合物（甚至是弱[Brønsted酸](../Page/酸碱质子理论.md "wikilink")）中的[质子而生成相应的钠化合物](../Page/质子.md "wikilink")。典型的例子有：[醇](../Page/醇.md "wikilink")、[酚](../Page/酚.md "wikilink")、[吡唑和](../Page/吡唑.md "wikilink")[硫醇等](../Page/硫醇.md "wikilink")。

氢化钠最常用于[1,3-二羰基化合物](../Page/1,3-二羰基化合物.md "wikilink")、[马来酸酯及类似化合物的](../Page/马来酸.md "wikilink")[去质子化](../Page/去质子化.md "wikilink")，相应的钠化合物可以被[烷基化](../Page/烷基化.md "wikilink")，从而能够生成各种各样的有机化合物。相应的反应见[Dieckmann缩合反应](../Page/Dieckmann缩合反应.md "wikilink")、[Stobbe缩合反应](../Page/Stobbe缩合反应.md "wikilink")、[Darzens缩合反应和](../Page/Darzens缩合反应.md "wikilink")[Claisen缩合反应](../Page/Claisen缩合反应.md "wikilink")。[二甲基亚砜是常用的溶剂](../Page/二甲基亚砜.md "wikilink")。

氢化钠还被用于合成[硫](../Page/硫.md "wikilink")[叶立德](../Page/叶立德.md "wikilink")，从而实现由[酮向](../Page/酮.md "wikilink")[环氧化合物的转化](../Page/环氧化合物.md "wikilink")。氢化钠可以合成
dithioimidodiphosphinate，这是[乙酰丙酮](../Page/乙酰丙酮.md "wikilink")[配合物中备受关注的领域](../Page/配合物.md "wikilink")。

氢化钠较少用作[还原剂](../Page/还原剂.md "wikilink")，因为负氢太小，不易与其他轨道很好重叠。然而氢化钠可以将Si-Si键和S-S键还原。

## 试剂规格

很多化学试剂公司如[Sigma-Aldrich和](../Page/Sigma-Aldrich.md "wikilink")[ACROS都销售氢化钠](../Page/ACROS.md "wikilink")，通常以[煤油保护](../Page/煤油.md "wikilink")，含60%的氢化钠。纯净的氢化钠可由用[戊烷或](../Page/戊烷.md "wikilink")[四氢呋喃清洗煤油得到](../Page/四氢呋喃.md "wikilink")。
氢化钠的使用需要[惰性环境](../Page/惰性.md "wikilink")，如用[氮气保护](../Page/氮气.md "wikilink")。实际反应时常用的是氢化钠于四氢呋喃中的悬浮液，因为很多钠有机化合物可溶于THF。

## 安全

氢化钠可以在空气中自燃，尤其是[与水接触时](../Page/遇水易燃.md "wikilink")。此反应产生大量[氢气趋向于爆炸](../Page/氢气.md "wikilink")，以及腐蚀性强碱[氢氧化钠](../Page/氢氧化钠.md "wikilink")。

## 参考文献

{{-}}

[Category:金属氢化物](../Category/金属氢化物.md "wikilink")
[Category:钠化合物](../Category/钠化合物.md "wikilink")
[Category:有机化学试剂](../Category/有机化学试剂.md "wikilink")
[Category:碱](../Category/碱.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.  Holleman, A. F.; Wiberg, E. "Inorganic Chemistry" Academic Press:
    San Diego, 2001. ISBN 0-12-352651-5.
2.  Encyclopedia of Reagents for Organic Synthesis (Ed: L. Paquette)
    2004, J. Wiley & Sons, New York. DOI: 10.1002/047084289.