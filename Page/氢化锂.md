**氢化锂**（[化学式](../Page/化学式.md "wikilink")：[Li](../Page/锂.md "wikilink")[H](../Page/氢.md "wikilink")）是[锂的](../Page/锂.md "wikilink")[氢化物](../Page/氢化物.md "wikilink")。它是无色[晶体](../Page/晶体.md "wikilink")，通常带有杂质而呈灰色。氢化锂属于类盐[氢化物](../Page/氢化物.md "wikilink")，[熔点很高](../Page/熔点.md "wikilink")（689°C）且对热稳定。[比热容为](../Page/比热容.md "wikilink")29.73
J/mol\*k，导热性随温度升高而下降，随组成和压力的变化也有不同（10\~5 W/m\*K，400 K）。

氢化锂[可燃](../Page/可燃.md "wikilink")，与水剧烈反应生成[腐蚀性的](../Page/腐蚀性.md "wikilink")[氢氧化锂和](../Page/氢氧化锂.md "wikilink")[氢气](../Page/氢气.md "wikilink")：

  -
    LiH + H<sub>2</sub>O → LiOH + H<sub>2</sub>

## 合成

氢化锂可通过金属锂与氢气反应制取：\[1\]

  -
    2 Li + H<sub>2</sub> → 2 LiH

## 反应

LiH通常在特定的类金属氢化物的合成中做氢化物还原剂。例如，[硅烷可由氢化锂和](../Page/硅烷.md "wikilink")[四氯化硅通过Sundermeyer过程反应得到](../Page/四氯化硅.md "wikilink")：

  -
    4 LiH + SiCl<sub>4</sub> → 4 LiCl + SiH<sub>4</sub>

LiH的氢含量是NaH的3倍，是氢含量最高的氢化物。LiH偶尔会作为储氢材料，但其良好的稳定性阻碍了这方面应用的发展。合成反应需要在远高於700
°C的高温下才能进行，产生H<sub>2</sub>。LiH曾作为模型火箭的燃料组分来测试。\[2\]

## 用途

LiH用途广泛，可用作[干燥剂](../Page/干燥剂.md "wikilink")、[核反应堆中的冷却剂和防护材料](../Page/核反应堆.md "wikilink")，以及制取[氢化铝锂](../Page/氢化铝锂.md "wikilink")、制造氢气发生器和生产[陶瓷材料](../Page/陶瓷.md "wikilink")。氢化锂中氢的质量比是[氢化钠中的三倍](../Page/氢化钠.md "wikilink")，是所有类盐氢化物中最高的，因此氢化锂是很好的氢气发生器原料。

锂的相应[氘化物](../Page/氘.md "wikilink")（LiD，非[放射性](../Page/放射性.md "wikilink")）是[核聚变反应中的燃料](../Page/核聚变.md "wikilink")。

### 复合金属氢化物的前体

LiH可用於生产[有机合成中常用的多种试剂](../Page/有机合成.md "wikilink")，如[氢化铝锂](../Page/氢化铝锂.md "wikilink")（LiAlH<sub>4</sub>）和[硼氢化锂](../Page/硼氢化锂.md "wikilink")
(LiBH<sub>4</sub>）。LiH与[三乙基硼反应生成](../Page/三乙基硼.md "wikilink")[三乙基硼氢化锂](../Page/三乙基硼氢化锂.md "wikilink")（LiBHEt<sub>3</sub>）。\[3\]

## 安全

LiH在空气可燃，与水亦会爆炸性反应，生成腐蚀性的LiOH和氢气。

## 参见

  - [氢化物](../Page/氢化物.md "wikilink")
  - [氢化钠](../Page/氢化钠.md "wikilink")

## 参考文献

## 外部链接

  - [氢化锂（英文）](http://www.webelements.com/webelements/compounds/text/Li/H1Li1-7580678.html)
  - [氢化锂 - Emergency First Aid Treatment
    Guide](https://web.archive.org/web/20041112080544/http://yosemite.epa.gov/oswer/CeppoEHS.nsf/firstaid/7580-67-8?OpenDocument)

[Category:锂化合物](../Category/锂化合物.md "wikilink")
[Category:金属氢化物](../Category/金属氢化物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.
2.  \]\[
3.  Peter Rittmeyer, Ulrich Wietelmann “Hydrides” in Ullmann's
    Encyclopedia of Industrial Chemistry 2002, Wiley-VCH, Weinheim.
    {{|10.1002/14356007.a13_199}}