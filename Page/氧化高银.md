**氧化高银**（[实验式](../Page/实验式.md "wikilink")：[Ag](../Page/银.md "wikilink")[O](../Page/氧.md "wikilink")），确切讲应称为**氧化银（I,III）（，Ag<sup>I</sup>Ag<sup>III</sup>O<sub>2</sub>）**或**四氧化四银（Ag<sub>4</sub>O<sub>4</sub>）**，是黑色或灰色不溶性固体\[1\]。它由[过二硫酸钾在沸腾的](../Page/过二硫酸钾.md "wikilink")[碱性溶液中氧化](../Page/碱性.md "wikilink")[硝酸银或用](../Page/硝酸银.md "wikilink")[臭氧氧化金属](../Page/臭氧.md "wikilink")[银制得](../Page/银.md "wikilink")\[2\]。

注：氧化高银的化学式不能写成Ag<sub>2</sub>O<sub>2</sub>，因为Ag<sub>2</sub>O<sub>2</sub>的化学名称应为“过氧化银”，其中含有Ag(I)和[过氧根离子](../Page/过氧化物.md "wikilink")，两个氧原子以单键连接。

## 结构

尽管AgO中银的[平均氧化态为](../Page/氧化态.md "wikilink")+2，而且一系列证据似乎表明银的确为+2氧化态——AgO溶于酸产生银(II)\[3\]，且它与[CuO同晶型](../Page/氧化铜.md "wikilink")\[4\]，但理论计算表明氧化银(I,III)比氧化银(II)稳定，这可能与银的[第二电离能比](../Page/电离能.md "wikilink")[铜大有关](../Page/铜.md "wikilink")\[5\]。另外，+2氧化态也无法解释它及其碱性溶液的[反磁性](../Page/反磁性.md "wikilink")\[6\]。[中子衍射结果也表明它的结构式应为Ag](../Page/中子衍射.md "wikilink")<sup>I</sup>Ag<sup>III</sup>O<sub>2</sub>\[7\]或者
Ag<sub>2</sub>O·Ag<sub>2</sub>O<sub>3</sub>。

## 参见

  - [氧化银](../Page/氧化银.md "wikilink") -
    [二氟化银](../Page/二氟化银.md "wikilink")

## 参考资料

[Category:银化合物](../Category/银化合物.md "wikilink")
[Category:氧化物](../Category/氧化物.md "wikilink")
[Category:混合价态化合物](../Category/混合价态化合物.md "wikilink")

1.

2.
3.  Peter Fischer, Martin Jansen "Electrochemical Syntheses of Binary
    Silver Oxides" 1995, vol. 30, pp. 50–55.

4.
5.  David Tudela "Silver(II) Oxide or Silver(I,III) Oxide?" J. Chem.
    Educ., 2008, volume 85, p 863.

6.
7.