**氨基甲酸銨**是一種白色的晶體，[分子式為](../Page/分子式.md "wikilink")
NH<sub>2</sub>COONH<sub>4</sub>，在35°C開始分解，並會在59°C時完全分解成[氨氣和](../Page/氨氣.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")。\[1\]
氨基甲酸銨是化學工業上尿素生産過程的生成物，加熱會变为[尿素](../Page/尿素.md "wikilink")。

## 參見

  - [二硫代氨基甲酸铵](../Page/二硫代氨基甲酸铵.md "wikilink")
  - [乙撑双二硫代氨基甲酸铵](../Page/乙撑双二硫代氨基甲酸铵.md "wikilink")
  - [n-二甲氨基二硫代甲酸铵](../Page/n-二甲氨基二硫代甲酸铵.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:氮化合物](../Category/氮化合物.md "wikilink")
[Category:有机酸铵盐](../Category/有机酸铵盐.md "wikilink")
[Category:羧酸盐](../Category/羧酸盐.md "wikilink")
[Category:一碳有机物](../Category/一碳有机物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.  [www.jtbaker.com/msds/englishhtml/a5688.htm](http://www.jtbaker.com/msds/englishhtml/a5688.htm)