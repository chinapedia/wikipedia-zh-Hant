[AminoAcidball.svg](https://zh.wikipedia.org/wiki/File:AminoAcidball.svg "fig:AminoAcidball.svg")

[Amino_Acids.svg](https://zh.wikipedia.org/wiki/File:Amino_Acids.svg "fig:Amino_Acids.svg")發現的21种[蛋白原α氨基酸](../Page/蛋白胺基酸.md "wikilink")，根據其側鏈的[pK<sub>a</sub>值和在生理](../Page/pKa.md "wikilink")[生理pH
7.4值攜帶電荷分組](../Page/pH#Living_systems.md "wikilink")\]\]

[Protein_primary_structure.svg](https://zh.wikipedia.org/wiki/File:Protein_primary_structure.svg "fig:Protein_primary_structure.svg")是鏈狀的胺基酸\]\]
[Phenylalanin_-_Phenylalanine.svg](https://zh.wikipedia.org/wiki/File:Phenylalanin_-_Phenylalanine.svg "fig:Phenylalanin_-_Phenylalanine.svg")是標準胺基酸的一種\]\]

**胺基酸**是[生物學上重要的](../Page/生物.md "wikilink")[有機化合物](../Page/有機化合物.md "wikilink")，由[胺基](../Page/胺基.md "wikilink")（-NH<sub>2</sub>）和[羧基](../Page/羧基.md "wikilink")（-COOH）的[官能團](../Page/官能團.md "wikilink")，以及连到每一個胺基[酸的](../Page/酸.md "wikilink")[側鏈組成](../Page/取代基.md "wikilink")。胺基酸是構成[蛋白質的基本單位](../Page/蛋白質.md "wikilink")。賦予蛋白質特定的分子結構形態，使其分子具有[生化活性](../Page/生化活性.md "wikilink")。蛋白質是生物体重要的[活性分子](../Page/活性分子.md "wikilink")，包括催化[新陳代謝的](../Page/新陳代謝.md "wikilink")[酶](../Page/酶.md "wikilink")（又称“酵素”）。

不同的胺基酸[脱水缩合形成](../Page/脱水缩合.md "wikilink")[肽](../Page/肽.md "wikilink")（蛋白質的原始片段），是蛋白質生成的[前體](../Page/前體.md "wikilink")。

## 基本結構

胺基酸为分子结构中含有[氨基](../Page/氨基.md "wikilink")（-NH<sub>2</sub>
）和[羧基](../Page/羧基.md "wikilink")（-COOH），并且氨基和羧基都是直接连接在一个-CH-结构上的有机化合物。通式是H<sub>2</sub>NCHRCOOH(R代表某種有機[取代基](../Page/取代基.md "wikilink"))。根据氨基连结在羧酸中碳原子的位置，可分为*α*、*β*、*γ*、*δ*……等等的氨基酸(
C……C-C-C-C-COOH)。

蛋白质经[水解後](../Page/水解.md "wikilink")，即生成20种氨基酸，如[甘氨酸](../Page/甘氨酸.md "wikilink")（Glycine）、[丙氨酸](../Page/丙氨酸.md "wikilink")（Alanine）、[缬氨酸](../Page/缬氨酸.md "wikilink")（Valine）、[亮氨酸](../Page/亮氨酸.md "wikilink")（Leucine）、[异亮氨酸](../Page/异亮氨酸.md "wikilink")（Isoleucine）、[苯丙氨酸](../Page/苯丙氨酸.md "wikilink")（Phenylalanine）、[色氨酸](../Page/色氨酸.md "wikilink")（Tryptophan）、[酪氨酸](../Page/酪氨酸.md "wikilink")（Tyrosine）、[天冬氨酸](../Page/天冬氨酸.md "wikilink")（Aspartate）、[组氨酸](../Page/组氨酸.md "wikilink")（Histidine）、[天冬酰胺](../Page/天冬酰胺.md "wikilink")（Asparagine）、[谷氨酸](../Page/谷氨酸.md "wikilink")（Glutamate）、[赖氨酸](../Page/離胺酸.md "wikilink")（Lysine）、[谷氨酰胺](../Page/谷氨酰胺.md "wikilink")（Glutamine）、[甲硫氨酸](../Page/甲硫氨酸.md "wikilink")（Methionine）、[精氨酸](../Page/精氨酸.md "wikilink")（Arginine）、[丝氨酸](../Page/丝氨酸.md "wikilink")（Serine）、[苏氨酸](../Page/苏氨酸.md "wikilink")（Threonine）、[半胱氨酸](../Page/半胱氨酸.md "wikilink")（Cysteine）、[脯氨酸](../Page/脯氨酸.md "wikilink")（Proline）等。

## 分類

根据其结合[基团不同](../Page/基团.md "wikilink")。可分为[脂肪族胺基酸](../Page/脂肪族.md "wikilink")、[芳香族胺基酸](../Page/芳香族.md "wikilink")、[杂环胺基酸](../Page/杂环.md "wikilink")、無環胺基酸(acyclic)、[鹼性胺基酸含](../Page/鹼.md "wikilink")[硫胺基酸](../Page/硫.md "wikilink")、含[羥基](../Page/羥基.md "wikilink")(醇)胺基酸、含[碘胺基酸等](../Page/碘.md "wikilink")。

## 理化特性

[Peptidformationball.svg](https://zh.wikipedia.org/wiki/File:Peptidformationball.svg "fig:Peptidformationball.svg")

1.  都是无色[结晶](../Page/结晶.md "wikilink")。[熔点约在](../Page/熔点.md "wikilink")230℃以上，大多没有确切的熔点，[熔融时](../Page/熔融.md "wikilink")[分解并放出](../Page/分解反应.md "wikilink")[CO<sub>2</sub>](../Page/二氧化碳.md "wikilink")；都能溶于[强酸和](../Page/强酸.md "wikilink")[强碱](../Page/强碱.md "wikilink")[溶液中](../Page/溶液.md "wikilink")，除[胱胺酸](../Page/胱胺酸.md "wikilink")、[酪胺酸](../Page/酪胺酸.md "wikilink")、[二碘甲状腺素外](../Page/二碘甲状腺素.md "wikilink")，均溶於水；除[脯胺酸和](../Page/脯胺酸.md "wikilink")[羟脯胺酸外](../Page/羟脯胺酸.md "wikilink")，均难溶於[乙醇和](../Page/乙醇.md "wikilink")[乙醚](../Page/乙醚.md "wikilink")。
2.  具有[两性](../Page/兩性_\(化學\).md "wikilink")。有碱性\[二元胺基一元羧酸，例如赖胺酸（lysine）\]；酸性\[一元胺基二元羧酸，例如穀胺酸（Glutamic
    acid）\]；中性\[一元胺基一元羧酸，例如丙胺酸（Alanine）\]三種類型。大多数胺基酸都呈显不同程度的[酸性或](../Page/酸性.md "wikilink")[碱性](../Page/碱性.md "wikilink")，呈显中性的较少。所以既能与酸结合成[盐](../Page/盐.md "wikilink")，也能与碱结合成盐。
3.  由于有不对称的碳原子，呈[旋光性](../Page/旋光性.md "wikilink")（除甘氨酸外）。同时由于空间的排列位置不同，又有两种构型：[D型和L型](../Page/手性.md "wikilink")，组成天然蛋白质的胺基酸，都属L型。
    由于以前胺基酸来源于蛋白质水解（现在大多为人工合成），而天然蛋白质水解所得的胺基酸大多为α-氨基酸，所以在生化研究方面胺基酸通常指α-胺基酸。至于β、γ、δ……ω等的胺基酸在生化研究中用途较小，大都用于[有机合成](../Page/有机合成.md "wikilink")、[石油化工](../Page/石油化工.md "wikilink")、[医疗等方面](../Page/医疗.md "wikilink")。胺基酸及其[衍生物品种很多](../Page/衍生物.md "wikilink")，大多性质稳定，要避光、干燥贮存。

## 构成天然蛋白质的20种常见胺基酸列表

<table>
<thead>
<tr class="header">
<th><p>縮寫</p></th>
<th><p>縮寫</p></th>
<th><p>全名</p></th>
<th><p>中文譯名</p></th>
<th><p>結構</p></th>
<th><p>支鏈</p></th>
<th><p>分子量</p></th>
<th><p><a href="../Page/等電點.md" title="wikilink">等電點</a></p></th>
<th><p><a href="../Page/解離常數.md" title="wikilink">解離常數</a><br />
（羧基）</p></th>
<th><p>解離常數<br />
（胺基）</p></th>
<th><p>pKr(R)</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>G</p></td>
<td><p>Gly</p></td>
<td><p>Glycine</p></td>
<td><p><a href="../Page/甘胺酸.md" title="wikilink">甘胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Glycin_-_Glycine.svg" title="fig:甘胺酸 Glycine">甘胺酸 Glycine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>75.07</p></td>
<td><p>6.06</p></td>
<td><p>2.35</p></td>
<td><p>9.78</p></td>
<td></td>
<td><p>沒有旋光性</p></td>
</tr>
<tr class="even">
<td><p>A</p></td>
<td><p>Ala</p></td>
<td><p>Alanine</p></td>
<td><p><a href="../Page/丙胺酸.md" title="wikilink">丙胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Alanin_-_Alanine.svg" title="fig:丙胺酸 Alanine">丙胺酸 Alanine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>89.09</p></td>
<td><p>6.11</p></td>
<td><p>2.35</p></td>
<td><p>9.87</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>V</p></td>
<td><p>Val</p></td>
<td><p>Valine</p></td>
<td><p><a href="../Page/纈胺酸.md" title="wikilink">纈胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Valin_-_Valine.svg" title="fig:缬胺酸 Valine">缬胺酸 Valine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>117.15</p></td>
<td><p>6.00</p></td>
<td><p>2.39</p></td>
<td><p>9.74</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>L</p></td>
<td><p>Leu</p></td>
<td><p>Leucine</p></td>
<td><p><a href="../Page/亮胺酸.md" title="wikilink">亮胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Leucin_-_Leucine.svg" title="fig:亮胺酸 Leucine">亮胺酸 Leucine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>131.17</p></td>
<td><p>6.01</p></td>
<td><p>2.33</p></td>
<td><p>9.74</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>I</p></td>
<td><p>Ile</p></td>
<td><p>Isoleucine</p></td>
<td><p><a href="../Page/異亮胺酸.md" title="wikilink">異亮胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Isoleucin_-_Isoleucine.svg" title="fig:異亮胺酸 Isoleucine">異亮胺酸 Isoleucine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>131.17</p></td>
<td><p>6.05</p></td>
<td><p>2.32</p></td>
<td><p>9.76</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>F</p></td>
<td><p>Phe</p></td>
<td><p>Phenylalanine</p></td>
<td><p><a href="../Page/苯丙胺酸.md" title="wikilink">苯丙胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Phenylalanin_-_Phenylalanine.svg" title="fig:苯丙胺酸 Phenylalanine">苯丙胺酸 Phenylalanine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>165.19</p></td>
<td><p>5.49</p></td>
<td><p>2.20</p></td>
<td><p>9.31</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>W</p></td>
<td><p>Trp</p></td>
<td><p>Tryptophan</p></td>
<td><p><a href="../Page/色胺酸.md" title="wikilink">色胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tryptophan_-_Tryptophan.svg" title="fig:色胺酸 Tryptophan">色胺酸 Tryptophan</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>204.23</p></td>
<td><p>5.89</p></td>
<td><p>2.46</p></td>
<td><p>9.41</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Y</p></td>
<td><p>Tyr</p></td>
<td><p>Tyrosine</p></td>
<td><p><a href="../Page/酪胺酸.md" title="wikilink">酪胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tyrosin_-_Tyrosine.svg" title="fig:酪胺酸 Tyrosine">酪胺酸 Tyrosine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>181.19</p></td>
<td><p>5.64</p></td>
<td><p>2.20</p></td>
<td><p>9.21</p></td>
<td><p>10.46</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D</p></td>
<td><p>Asp</p>
<td nowrap>
<p>Aspartate</p></td>
<td><p><a href="../Page/天冬胺酸.md" title="wikilink">天冬胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Asparaginsäure_-_Aspartic_acid.svg" title="fig:天冬胺酸 Aspartic acid">天冬胺酸 Aspartic acid</a></p></td>
<td><p><a href="../Page/酸性.md" title="wikilink">酸性</a></p></td>
<td><p>133.10</p></td>
<td><p>2.85</p></td>
<td><p>1.99</p></td>
<td><p>9.90</p></td>
<td><p>3.90</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H</p></td>
<td><p>His</p></td>
<td><p>Histidine</p></td>
<td><p><a href="../Page/組胺酸.md" title="wikilink">組胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Histidin_-_Histidine.svg" title="fig:组胺酸 Histidine">组胺酸 Histidine</a></p></td>
<td><p><a href="../Page/鹼性.md" title="wikilink">鹼性</a></p></td>
<td><p>155.16</p></td>
<td><p>7.60</p></td>
<td><p>1.80</p></td>
<td><p>9.33</p></td>
<td><p>6.04</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>N</p></td>
<td><p>Asn</p></td>
<td><p>Asparagine</p></td>
<td><p><a href="../Page/天冬酰胺.md" title="wikilink">天冬酰胺</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Asparagin_-_Asparagine.svg" title="fig:天冬酰胺 Asparagine">天冬酰胺 Asparagine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>132.12</p></td>
<td><p>5.41</p></td>
<td><p>2.14</p></td>
<td><p>8.72</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>E</p></td>
<td><p>Glu</p>
<td nowrap>
<p>Glutamate</p></td>
<td><p><a href="../Page/穀胺酸.md" title="wikilink">穀胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Glutaminsäure_-_Glutamic_acid.svg" title="fig:谷(麩)胺酸 Glutamic acid">谷(麩)胺酸 Glutamic acid</a></p></td>
<td><p><a href="../Page/酸性.md" title="wikilink">酸性</a></p></td>
<td><p>147.13</p></td>
<td><p>3.15</p></td>
<td><p>2.10</p></td>
<td><p>9.47</p></td>
<td><p>4.07</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K</p></td>
<td><p>Lys</p></td>
<td><p>Lysine</p></td>
<td><p><a href="../Page/赖胺酸.md" title="wikilink">赖胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lysin_-_Lysine.svg" title="fig:離胺酸 Lysine">離胺酸 Lysine</a></p></td>
<td><p><a href="../Page/鹼性.md" title="wikilink">鹼性</a></p></td>
<td><p>146.19</p></td>
<td><p>9.60</p></td>
<td><p>2.16</p></td>
<td><p>9.06</p></td>
<td><p>10.54</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q</p></td>
<td><p>Gln</p></td>
<td><p>Glutamine</p></td>
<td><p><a href="../Page/穀氨酰胺.md" title="wikilink">穀氨酰胺</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Glutamin_-_Glutamine.svg" title="fig:谷(麩)胺酰胺 Glutamine">谷(麩)胺酰胺 Glutamine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>146.15</p></td>
<td><p>5.65</p></td>
<td><p>2.17</p></td>
<td><p>9.13</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>M</p></td>
<td><p>Met</p></td>
<td><p>Methionine</p></td>
<td><p><a href="../Page/甲硫胺酸.md" title="wikilink">甲硫胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Methionin_-_Methionine.svg" title="fig:甲硫胺酸 Methionine">甲硫胺酸 Methionine</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>149.21</p></td>
<td><p>5.74</p></td>
<td><p>2.13</p></td>
<td><p>9.28</p></td>
<td></td>
<td><p>蛋白質合成時第一個胺基酸<br />
可能在<a href="../Page/轉譯.md" title="wikilink">轉譯過程被移除</a></p></td>
</tr>
<tr class="even">
<td><p>R</p></td>
<td><p>Arg</p></td>
<td><p>Arginine</p></td>
<td><p><a href="../Page/精胺酸.md" title="wikilink">精胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Arginin_-_Arginine.svg" title="fig:精胺酸 Arginine">精胺酸 Arginine</a></p></td>
<td><p><a href="../Page/鹼性.md" title="wikilink">鹼性</a></p></td>
<td><p>174.20</p></td>
<td><p>10.76</p></td>
<td><p>1.82</p></td>
<td><p>8.99</p></td>
<td><p>12.48</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>S</p></td>
<td><p>Ser</p></td>
<td><p>Serine</p></td>
<td><p><a href="../Page/絲胺酸.md" title="wikilink">絲胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Serin_-_Serine.svg" title="fig:丝胺酸 Serine">丝胺酸 Serine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>105.09</p></td>
<td><p>5.68</p></td>
<td><p>2.19</p></td>
<td><p>9.21</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>T</p></td>
<td><p>Thr</p></td>
<td><p>Threonine</p></td>
<td><p><a href="../Page/苏胺酸.md" title="wikilink">苏胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Threonin_-_Threonine.svg" title="fig:苏胺酸 Threonine">苏胺酸 Threonine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>119.12</p></td>
<td><p>5.60</p></td>
<td><p>2.09</p></td>
<td><p>9.10</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>C</p></td>
<td><p>Cys</p></td>
<td><p>Cysteine</p></td>
<td><p><a href="../Page/半胱胺酸.md" title="wikilink">半胱胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cystein_-_Cysteine.svg" title="fig:半胱胺酸 Cysteine">半胱胺酸 Cysteine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td><p>121.16</p></td>
<td><p>5.05</p></td>
<td><p>1.92</p></td>
<td><p>10.70</p></td>
<td><p>8.37</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>P</p></td>
<td><p>Pro</p></td>
<td><p>Proline</p></td>
<td><p><a href="../Page/脯胺酸.md" title="wikilink">脯胺酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Prolin_-_Proline.svg" title="fig:脯胺酸 Proline">脯胺酸 Proline</a></p></td>
<td><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></td>
<td><p>115.13</p></td>
<td><p>6.30</p></td>
<td><p>1.95</p></td>
<td><p>10.64</p></td>
<td></td>
<td><p>會破壞蛋白質α螺旋結構</p></td>
</tr>
</tbody>
</table>

## 標準胺基酸

共20种，是组成生命体中的蛋白质的主要单元，第21和第22種胺基酸，[硒半胱胺酸和](../Page/硒半胱胺酸.md "wikilink")[吡咯賴胺酸](../Page/吡咯賴胺酸.md "wikilink")，分別用通常的終止密碼子UGA和UAG編碼，出現在少數蛋白質中。

## 主要化學性質

以下的分子量是根據各元素的[同位素在自然界的](../Page/同位素.md "wikilink")[豐度所做的平均值](../Page/豐度.md "wikilink")。此外由於形成[肽鍵時](../Page/肽鍵.md "wikilink")，會減少一個[水分子](../Page/水分子.md "wikilink")，因此蛋白質中單一氨基酸的分子量，比下表數值少了18.01524
Da。

| 氨基酸                                  | 分子量（[Da](../Page/原子質量單位.md "wikilink")） | [pI](../Page/等電點.md "wikilink") | [pK](../Page/解離常數.md "wikilink")<sub>1</sub>（α-COOH） | pK<sub>2</sub>（α-<sup>+</sup>NH<sub>3</sub>） |
| ------------------------------------ | --------------------------------------- | ------------------------------- | ---------------------------------------------------- | -------------------------------------------- |
| [丙胺酸](../Page/丙胺酸.md "wikilink")     | 89.09404                                | 6.01                            | 2.35                                                 | 9.87                                         |
| [半胱胺酸](../Page/半胱胺酸.md "wikilink")   | 121.15404                               | 5.05                            | 1.92                                                 | 10.70                                        |
| [硒半胱胺酸](../Page/硒半胱胺酸.md "wikilink") | 169.06                                  |                                 |                                                      |                                              |
| [天冬胺酸](../Page/天冬胺酸.md "wikilink")   | 133.10384                               | 2.85                            | 1.99                                                 | 9.90                                         |
| [谷胺酸](../Page/谷胺酸.md "wikilink")     | 147.13074                               | 3.15                            | 2.10                                                 | 9.47                                         |
| [苯丙胺酸](../Page/苯丙胺酸.md "wikilink")   | 165.19184                               | 5.49                            | 2.20                                                 | 9.31                                         |
| [甘胺酸](../Page/甘胺酸.md "wikilink")     | 75.06714                                | 6.06                            | 2.35                                                 | 9.78                                         |
| [組胺酸](../Page/組胺酸.md "wikilink")     | 155.15634                               | 7.60                            | 1.80                                                 | 9.33                                         |
| [異亮胺酸](../Page/異亮胺酸.md "wikilink")   | 131.17464                               | 6.05                            | 2.32                                                 | 9.76                                         |
| [賴胺酸](../Page/賴胺酸.md "wikilink")     | 146.18934                               | 9.60                            | 2.16                                                 | 9.06                                         |
| [吡咯賴胺酸](../Page/吡咯賴胺酸.md "wikilink") | 255.31                                  |                                 |                                                      |                                              |
| [亮胺酸](../Page/亮胺酸.md "wikilink")     | 131.17464                               | 6.01                            | 2.33                                                 | 9.74                                         |
| [甲硫胺酸](../Page/甲硫胺酸.md "wikilink")   | 149.20784                               | 5.74                            | 2.13                                                 | 9.28                                         |
| [天冬醯胺](../Page/天冬醯胺.md "wikilink")   | 132.11904                               | 5.41                            | 2.14                                                 | 8.72                                         |
| [脯胺酸](../Page/脯胺酸.md "wikilink")     | 115.13194                               | 6.30                            | 1.95                                                 | 10.64                                        |
| [谷氨酰胺](../Page/谷氨酰胺.md "wikilink")   | 146.14594                               | 5.65                            | 2.17                                                 | 9.13                                         |
| [精胺酸](../Page/精胺酸.md "wikilink")     | 174.20274                               | 10.76                           | 1.82                                                 | 8.99                                         |
| [絲胺酸](../Page/絲胺酸.md "wikilink")     | 105.09344                               | 5.68                            | 2.19                                                 | 9.21                                         |
| [蘇胺酸](../Page/蘇胺酸.md "wikilink")     | 119.12034                               | 5.60                            | 2.09                                                 | 9.10                                         |
| [纈胺酸](../Page/纈胺酸.md "wikilink")     | 117.14784                               | 6.00                            | 2.39                                                 | 9.74                                         |
| [色胺酸](../Page/色胺酸.md "wikilink")     | 204.22844                               | 5.89                            | 2.46                                                 | 9.41                                         |
| [酪胺酸](../Page/酪胺酸.md "wikilink")     | 181.19124                               | 5.64                            | 2.20                                                 | 9.21                                         |

## 支鏈性質

以下列表中的pKa值，可能與這些氨基酸在蛋白質內部時有所不同。

<table>
<thead>
<tr class="header">
<th><p>氨基酸</p></th>
<th><p>支鏈</p></th>
<th><p><a href="../Page/疏水性.md" title="wikilink">疏水性</a></p></th>
<th><p>pKa</p></th>
<th><p><a href="../Page/極性.md" title="wikilink">極性</a></p></th>
<th><p><a href="../Page/電荷.md" title="wikilink">電荷</a></p></th>
<th><p><a href="../Page/芳香族.md" title="wikilink">芳香族</a><br />
或<a href="../Page/脂肪族.md" title="wikilink">脂肪族</a></p></th>
<th><p><a href="../Page/凡得瓦半徑.md" title="wikilink">范德瓦尔斯半径</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/丙氨酸.md" title="wikilink">丙氨酸</a></p></td>
<td><p>-CH<sub>3</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>67</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半胱氨酸.md" title="wikilink">半胱氨酸</a></p></td>
<td><p>-CH<sub>2</sub>SH</p></td>
<td><p>X</p></td>
<td><p>8.18</p></td>
<td><p>-</p></td>
<td><p>酸性</p></td>
<td><p>-</p></td>
<td><p>86</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/硒半胱胺酸.md" title="wikilink">硒半胱胺酸</a></p></td>
<td><p>-CH<sub>2</sub>SeH</p></td>
<td><p>X</p></td>
<td><p>5.73</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天冬氨酸.md" title="wikilink">天冬氨酸</a></p></td>
<td><p>-CH<sub>2</sub>COOH</p></td>
<td><p>-</p></td>
<td><p>3.90</p></td>
<td><p>X</p></td>
<td><p>酸性</p></td>
<td><p>-</p></td>
<td><p>91</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穀氨酸.md" title="wikilink">穀氨酸</a></p></td>
<td><p>-CH<sub>2</sub>CH<sub>2</sub>COOH</p></td>
<td><p>-</p></td>
<td><p>4.07</p></td>
<td><p>X</p></td>
<td><p>酸性</p></td>
<td><p>-</p></td>
<td><p>109</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苯丙氨酸.md" title="wikilink">苯丙氨酸</a></p></td>
<td><p>-CH<sub>2</sub>C<sub>6</sub>H<sub>5</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>芳香性</p></td>
<td><p>135</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甘氨酸.md" title="wikilink">甘氨酸</a></p></td>
<td><p>-H</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>48</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/組氨酸.md" title="wikilink">組氨酸</a></p></td>
<td><p>-CH<sub>2</sub>-C<sub>3</sub>H<sub>3</sub>N<sub>2</sub></p></td>
<td><p>-</p></td>
<td><p>6.04</p></td>
<td><p>X</p></td>
<td><p>弱鹼性</p></td>
<td><p>芳香性</p></td>
<td><p>118</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/異亮氨酸.md" title="wikilink">異亮氨酸</a></p></td>
<td><p>-CH(CH<sub>3</sub>)CH<sub>2</sub>CH<sub>3</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>脂肪性</p></td>
<td><p>124</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/離氨酸.md" title="wikilink">離氨酸</a></p></td>
<td><p>-(CH<sub>2</sub>)<sub>4</sub>NH<sub>2</sub></p></td>
<td><p>-</p></td>
<td><p>10.54</p></td>
<td><p>X</p></td>
<td><p>鹼性</p></td>
<td><p>-</p></td>
<td><p>135</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吡咯賴氨酸.md" title="wikilink">吡咯賴氨酸</a></p></td>
<td><p>-C<sub>12</sub>H<sub>21</sub>N<sub>3</sub>O<sub>3</sub></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亮氨酸.md" title="wikilink">亮氨酸</a></p></td>
<td><p>-CH<sub>2</sub>CH(CH<sub>3</sub>)<sub>2</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>脂肪性</p></td>
<td><p>124</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甲硫氨酸.md" title="wikilink">甲硫氨酸</a></p></td>
<td><p>-CH<sub>2</sub>CH<sub>2</sub>SCH<sub>3</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>124</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天冬醯胺.md" title="wikilink">天冬醯胺</a></p></td>
<td><p>-CH<sub>2</sub>CONH<sub>2</sub></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>96</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/脯氨酸.md" title="wikilink">脯氨酸</a></p></td>
<td><p>-CH<sub>2</sub>CH<sub>2</sub>CH<sub>2</sub>-</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>90</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/穀氨醯胺.md" title="wikilink">穀氨醯胺</a></p></td>
<td><p>-CH<sub>2</sub>CH<sub>2</sub>CONH<sub>2</sub></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>114</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/精氨酸.md" title="wikilink">精氨酸</a></p></td>
<td><p>-(CH<sub>2</sub>)<sub>3</sub>NH-C(NH)NH<sub>2</sub></p></td>
<td><p>-</p></td>
<td><p>12.48</p></td>
<td><p>X</p></td>
<td><p>鹼性</p></td>
<td><p>-</p></td>
<td><p>148</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/絲氨酸.md" title="wikilink">絲氨酸</a></p></td>
<td><p>-CH<sub>2</sub>OH</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>73</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇氨酸.md" title="wikilink">蘇氨酸</a></p></td>
<td><p>-CH(OH)CH<sub>3</sub></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>X</p></td>
<td><p>弱酸性</p></td>
<td><p>-</p></td>
<td><p>93</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/纈氨酸.md" title="wikilink">纈氨酸</a></p></td>
<td><p>-CH(CH<sub>3</sub>)<sub>2</sub></p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>脂肪性</p></td>
<td><p>105</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/色氨酸.md" title="wikilink">色氨酸</a></p></td>
<td><p>-CH<sub>2</sub>-C<sub>8</sub>H<sub>6</sub>N</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>芳香性</p></td>
<td><p>163</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酪氨酸.md" title="wikilink">酪氨酸</a></p></td>
<td><p>-CH<sub>2</sub>-C<sub>6</sub>H<sub>4</sub>OH</p></td>
<td><p>X</p></td>
<td><p>10.46</p></td>
<td><p>X</p></td>
<td><p>-</p></td>
<td><p>芳香性</p></td>
<td><p>141</p></td>
</tr>
</tbody>
</table>

## 基因表現與生物化學

<table>
<thead>
<tr class="header">
<th><p>氨基酸</p></th>
<th><p>短寫</p></th>
<th><p>縮寫</p></th>
<th><p><a href="../Page/密碼子.md" title="wikilink">密碼子</a></p></th>
<th><p>在蛋白質中<br />
出現頻率（%）</p></th>
<th><p>對人類的必需性</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/丙氨酸.md" title="wikilink">丙氨酸</a></p></td>
<td><p>A</p></td>
<td><p>Ala</p></td>
<td><p>GCU, GCC, GCA, GCG</p></td>
<td><p>7.8</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半胱氨酸.md" title="wikilink">半胱氨酸</a></p></td>
<td><p>C</p></td>
<td><p>Cys</p></td>
<td><p>UGU, UGC</p></td>
<td><p>1.9</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/硒半胱氨酸.md" title="wikilink">硒半胱氨酸</a></p></td>
<td><p>U</p></td>
<td><p>Sec</p></td>
<td><p>UGA</p></td>
<td></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天冬氨酸.md" title="wikilink">天冬氨酸</a></p></td>
<td><p>D</p></td>
<td><p>Asp</p></td>
<td><p>GAU, GAC</p></td>
<td><p>5.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穀氨酸.md" title="wikilink">穀氨酸</a></p></td>
<td><p>E</p></td>
<td><p>Glu</p></td>
<td><p>GAA, GAG</p></td>
<td><p>6.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苯丙氨酸.md" title="wikilink">苯丙氨酸</a></p></td>
<td><p>F</p></td>
<td><p>Phe</p></td>
<td><p>UUU, UUC</p></td>
<td><p>3.9</p></td>
<td><p>X</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甘氨酸.md" title="wikilink">甘氨酸</a></p></td>
<td><p>G</p></td>
<td><p>Gly</p></td>
<td><p>GGU, GGC, GGA, GGG</p></td>
<td><p>7.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/組氨酸.md" title="wikilink">組氨酸</a></p></td>
<td><p>H</p></td>
<td><p>His</p></td>
<td><p>CAU, CAC</p></td>
<td><p>2.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/異亮氨酸.md" title="wikilink">異亮氨酸</a></p></td>
<td><p>I</p></td>
<td><p>Ile</p></td>
<td><p>AUU, AUC, AUA</p></td>
<td><p>5.3</p></td>
<td><p>X</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/離氨酸.md" title="wikilink">離氨酸</a></p></td>
<td><p>K</p></td>
<td><p>Lys</p></td>
<td><p>AAA, AAG</p></td>
<td><p>5.9</p></td>
<td><p>X</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吡咯賴氨酸.md" title="wikilink">吡咯賴氨酸</a></p></td>
<td><p>O</p></td>
<td><p>Pyl</p></td>
<td><p>UAG</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亮氨酸.md" title="wikilink">亮氨酸</a></p></td>
<td><p>L</p></td>
<td><p>Leu</p></td>
<td><p>UUA, UUG, CUU, CUC, CUA, CUG</p></td>
<td><p>9.1</p></td>
<td><p>X</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甲硫氨酸.md" title="wikilink">甲硫氨酸</a></p></td>
<td><p>M</p></td>
<td><p>Met</p></td>
<td><p>AUG</p></td>
<td><p>2.3</p></td>
<td><p>X</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天冬醯胺.md" title="wikilink">天冬醯胺</a></p></td>
<td><p>N</p></td>
<td><p>Asn</p></td>
<td><p>AAU, AAC</p></td>
<td><p>4.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/脯氨酸.md" title="wikilink">脯氨酸</a></p></td>
<td><p>P</p></td>
<td><p>Pro</p></td>
<td><p>CCU, CCC, CCA, CCG</p></td>
<td><p>5.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/穀氨醯胺.md" title="wikilink">穀氨醯胺</a></p></td>
<td><p>Q</p></td>
<td><p>Gln</p></td>
<td><p>CAA, CAG</p></td>
<td><p>4.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/精氨酸.md" title="wikilink">精氨酸</a></p></td>
<td><p>R</p></td>
<td><p>Arg</p></td>
<td><p>CGU, CGC, CGA, CGG, AGA, AGG</p></td>
<td><p>5.1</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/絲氨酸.md" title="wikilink">絲氨酸</a></p></td>
<td><p>S</p></td>
<td><p>Ser</p></td>
<td><p>UCU, UCC, UCA, UCG, AGU, AGC</p></td>
<td><p>6.8</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇氨酸.md" title="wikilink">蘇氨酸</a></p></td>
<td><p>T</p></td>
<td><p>Thr</p></td>
<td><p>ACU, ACC, ACA, ACG</p></td>
<td><p>5.9</p></td>
<td><p>X</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/纈氨酸.md" title="wikilink">纈氨酸</a></p></td>
<td><p>V</p></td>
<td><p>Val</p></td>
<td><p>GUU, GUC, GUA, GUG</p></td>
<td><p>6.6</p></td>
<td><p>X</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/色氨酸.md" title="wikilink">色氨酸</a></p></td>
<td><p>W</p></td>
<td><p>Trp</p></td>
<td><p>UGG</p></td>
<td><p>1.4</p></td>
<td><p>X</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酪氨酸.md" title="wikilink">酪氨酸</a></p></td>
<td><p>Y</p></td>
<td><p>Tyr</p></td>
<td><p>UAU, UAC</p></td>
<td><p>3.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>終止編碼</p></td>
<td><p>-</p></td>
<td><p>Term</p></td>
<td><p>UAA, UAG, UGA</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

## 必需氨基酸

[人體能](../Page/人體.md "wikilink")[消化吸收以及利用的胺基酸只有](../Page/消化.md "wikilink")20種。其中有9種氨基酸（嬰兒為10種）是成人体内不能合成或合成速度不能满足机体的需要，必须从[膳食補充的氨基酸稱為必需氨基酸](../Page/膳食.md "wikilink")（EAA），即[亮氨酸](../Page/亮氨酸.md "wikilink")、[異亮氨酸](../Page/異亮氨酸.md "wikilink")、[纈氨酸](../Page/纈氨酸.md "wikilink")、[甲硫氨酸](../Page/甲硫氨酸.md "wikilink")、[苯丙氨酸](../Page/苯丙氨酸.md "wikilink")、[色氨酸](../Page/色氨酸.md "wikilink")、[苏氨酸](../Page/苏氨酸.md "wikilink")、[賴氨酸](../Page/賴氨酸.md "wikilink")、[組氨酸](../Page/組氨酸.md "wikilink")，精氨酸为小儿生长发育期间的必需氨基酸，胱氨酸、酪氨酸、牛磺酸为早产儿所必需。

肉類中的[蛋白質是完全蛋白質](../Page/蛋白質.md "wikilink")，可以提供人體所需的全部[胺基酸種類](../Page/胺基酸.md "wikilink")，瘦豬肉的蛋白質含量約為10%至17%，肥豬肉則只有2.2%；瘦牛肉為20%左右，肥牛肉為15.1%；瘦羊肉17.3%，肥羊肉9.3%；兔肉21.2%；雞肉23.3%；鴨肉16.5%；鵝肉10.8%。肉類的蛋白質經過烹調，有一部分會散在肉湯中，也有一部分水解成[胺基酸](../Page/胺基酸.md "wikilink")，溶於肉湯裡，故烹調好的肉湯味道鮮美而富於營養。其他13種非必需胺基酸可以用[葡萄糖或是別的](../Page/葡萄糖.md "wikilink")[矿物质來源製造](../Page/矿物质.md "wikilink")。

## 次要编码氨基酸

通常的中止[密码子UGA和UAG编码的氨基酸](../Page/密码子.md "wikilink")，出现在少数[蛋白质中](../Page/蛋白质.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>縮寫</p></th>
<th><p>縮寫</p></th>
<th><p>全名</p></th>
<th><p>中文譯名</p></th>
<th><p>結構</p></th>
<th><p>支鏈</p></th>
<th><p>分子量</p></th>
<th><p><a href="../Page/等電點.md" title="wikilink">等電點</a></p></th>
<th><p><a href="../Page/解離常數.md" title="wikilink">解離常數</a><br />
（羧基）</p></th>
<th><p>解離常數<br />
（胺基）</p></th>
<th><p>pKr(R)</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>U</p></td>
<td><p>Sec</p></td>
<td><p>Selenocysteine</p></td>
<td><p><a href="../Page/硒半胱氨酸.md" title="wikilink">硒半胱氨酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:L-Selenocysteine.svg" title="fig:硒半胱氨酸 Selenocysteine">硒半胱氨酸 Selenocysteine</a></p></td>
<td><p><a href="../Page/親水性.md" title="wikilink">親水性</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>O</p></td>
<td><p>Pyl</p></td>
<td><p>Pyrrolysine</p></td>
<td><p><a href="../Page/吡咯赖氨酸.md" title="wikilink">吡咯赖氨酸</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Pyrrolysine.svg" title="fig:吡咯赖氨酸 Pyrrolysine">吡咯赖氨酸 Pyrrolysine</a></p></td>
<td><p><a href="../Page/鹼性.md" title="wikilink">鹼性</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 其它胺基酸

  - [胱氨酸](../Page/胱氨酸.md "wikilink")
  - [羟脯氨酸](../Page/羟脯氨酸.md "wikilink")
  - [鸟氨酸](../Page/鸟氨酸.md "wikilink")
  - [瓜氨酸](../Page/瓜氨酸.md "wikilink")
  - [鹅膏蕈氨酸](../Page/鹅膏蕈氨酸.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - [人民教育出版社](../Page/人民教育出版社.md "wikilink") 普通高中课程标准实验教科书
      - 生物 必修1 - **分子与细胞**
      - 生物 必修2 - **遗传与进化**

<!-- end list -->

  - 网页

<!-- end list -->

  - [國立臺灣大學](../Page/國立臺灣大學.md "wikilink") 莊榮輝教授網頁系統之**生物化學基礎**
    [胺基酸](http://juang.bst.ntu.edu.tw/BCbasics/Amino1.htm)

## 参见

  - [氨基酸的化学反应](../Page/氨基酸的化学反应.md "wikilink")
  - [肽](../Page/肽.md "wikilink")
  - [蛋白质](../Page/蛋白质.md "wikilink")

{{-}}

[氨基酸](../Category/氨基酸.md "wikilink")
[Category:氮素代谢](../Category/氮素代谢.md "wikilink")
[Category:两性离子](../Category/两性离子.md "wikilink")