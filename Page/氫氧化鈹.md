**氫氧化鈹**是一種白色的固體，[分子式為Be](../Page/分子式.md "wikilink")(OH)<sub>2</sub>，是少數[兩性金屬的](../Page/兩性_\(化學\).md "wikilink")[氫氧化物](../Page/氫氧根.md "wikilink")，既溶於[酸亦溶](../Page/酸.md "wikilink")[鹼](../Page/鹼.md "wikilink")，短期大量接觸可引起急性[鈹病](../Page/鈹病.md "wikilink")，是[致癌物質](../Page/致癌物質.md "wikilink")。

[Category:铍化合物](../Category/铍化合物.md "wikilink")
[Category:氢氧化物](../Category/氢氧化物.md "wikilink")