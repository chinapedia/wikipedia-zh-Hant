[Hydrogen_Deuterium_Tritium_Nuclei_Schematic.svg](https://zh.wikipedia.org/wiki/File:Hydrogen_Deuterium_Tritium_Nuclei_Schematic.svg "fig:Hydrogen_Deuterium_Tritium_Nuclei_Schematic.svg")的三種同位素：氕、氘、氚\]\]

**[氢](../Page/氢.md "wikilink")**（[原子量](../Page/原子量.md "wikilink")：1.00794(7)）共有7個已知同位素，其中有2個同位素是[穩定的](../Page/穩定同位素.md "wikilink")。它有三個天然的[同位素](../Page/同位素.md "wikilink")，分別是[氕](../Page/氕.md "wikilink")、[氘和](../Page/氘.md "wikilink")[氚](../Page/氚.md "wikilink")（<sup>1</sup>H、<sup>2</sup>H、<sup>3</sup>H），另外四個[同位素都非常的不穩定](../Page/同位素.md "wikilink")（<sup>4</sup>H到<sup>7</sup>H），只有在[實驗室製造出來過](../Page/實驗室.md "wikilink")，並沒有在自然界中出現。[氫也是唯一跟其同位素擁有不同名稱的](../Page/氫.md "wikilink")[元素](../Page/元素.md "wikilink")。雖然其他的[元素的同位素在以前也有不同的名稱](../Page/元素.md "wikilink")，但是今天已經不再使用了。

## 氫-1（氕）

[H-1_atom.png](https://zh.wikipedia.org/wiki/File:H-1_atom.png "fig:H-1_atom.png")的原子含有一个质子和一个电子\]\]
**氫原子**又稱**氕**是[氫元素的](../Page/氫.md "wikilink")[原子](../Page/原子.md "wikilink")，[元素豐度達](../Page/元素豐度.md "wikilink")99.98%
，是構造最簡單的的[原子](../Page/原子.md "wikilink")。[電中性的原子含有一個正價的](../Page/電子.md "wikilink")[質子與一個負價的](../Page/質子.md "wikilink")[電子](../Page/電子.md "wikilink")，被[庫侖定律束縛於](../Page/庫侖定律.md "wikilink")[原子核內](../Page/原子核.md "wikilink")。在大自然中，氫原子是[豐度最高的同位素](../Page/豐度.md "wikilink")，稱為**氫**，**氫-1**
，或**氕**，讀音：瞥。氫原子不含任何[中子](../Page/中子.md "wikilink")，別的氫[同位素含有一個或多個中子](../Page/同位素.md "wikilink")。這條目主要描述氫-1
。

## 氫-2（氘）

[H-2_atom.png](https://zh.wikipedia.org/wiki/File:H-2_atom.png "fig:H-2_atom.png")的原子含有一个质子、一个中子和一个电子\]\]**氘**為[氫的一種穩定形態同位素](../Page/氫.md "wikilink")，也被稱為**重氫**，[元素符號為D或](../Page/元素符號.md "wikilink")<sup>2</sup>H。它的[原子核由一顆](../Page/原子核.md "wikilink")[質子和一顆](../Page/質子.md "wikilink")[中子組成](../Page/中子.md "wikilink")。在大自然的含量約為[一般氫的](../Page/氕.md "wikilink")7000分之一，讀音：刀。

## 氫-3（氚）

[H-3_atom.png](https://zh.wikipedia.org/wiki/File:H-3_atom.png "fig:H-3_atom.png")的原子含有一个质子、两个中子和一个电子\]\]
**氚**，亦稱**超重氫**，是[氫的同位素之一](../Page/氫.md "wikilink")，[元素符號為T或](../Page/元素符號.md "wikilink")<sup>3</sup>H。它的[原子核由一顆](../Page/原子核.md "wikilink")[質子和兩顆](../Page/質子.md "wikilink")[中子所組成](../Page/中子.md "wikilink")，並帶有[放射性](../Page/放射性.md "wikilink")，會發生[β衰變成为](../Page/β衰變.md "wikilink")[氦－3](../Page/氦－3.md "wikilink")，其[半衰期為](../Page/半衰期.md "wikilink")12.43[年](../Page/年.md "wikilink")。
讀音：穿

## 氫-4

**氫-4**不穩定的[氫同位素之一](../Page/氫.md "wikilink")，它包含了一個[質子和三個](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")，[半衰期為](../Page/半衰期.md "wikilink")9.93696秒
。

## 氫-4.1

**氫-4.1**結構上類似[氦](../Page/氦.md "wikilink")，它包含了2個[質子和](../Page/質子.md "wikilink")2個[中子](../Page/中子.md "wikilink")，但因其中一個[電子被](../Page/電子.md "wikilink")[緲子取代](../Page/緲子.md "wikilink")，由於緲子的軌道特殊，軌道非常接近[原子核](../Page/原子核.md "wikilink")，而最內側的電子軌道與緲子的軌道相較之下在很外側，因此，該[緲子可視為](../Page/緲子.md "wikilink")[原子核的一部份](../Page/原子核.md "wikilink")，所以整個原子可視為:“[原子核由](../Page/原子核.md "wikilink")1個[緲子](../Page/緲子.md "wikilink")、2個[質子和](../Page/質子.md "wikilink")2個[中子組成](../Page/中子.md "wikilink")、外側只有一個電子”，因此可以視為一種**氫的同位素**，也是一種[奇異原子](../Page/奇異原子.md "wikilink")。一個[緲子重約](../Page/緲子.md "wikilink")0.1U，故名氫-
4.1(<sup>4.1</sup>H)。氫-4.1原子可以與其他元素反應，其行為更像一個氫原子不是像惰性的[氦](../Page/氦.md "wikilink")[原子](../Page/原子.md "wikilink")\[1\]。

## 氫-5

**氫-5**不穩定的[氫同位素之一](../Page/氫.md "wikilink")，它包含了一個[質子和四個](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")，[半衰期為](../Page/半衰期.md "wikilink")8.01930秒
。

## 氫-6

**氫-6**不穩定的[氫同位素之一](../Page/氫.md "wikilink")，它包含了一個[質子和五個](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")，[半衰期為](../Page/半衰期.md "wikilink")3秒
。

## 氫-7

**氫-7**不穩定的[氫同位素之一](../Page/氫.md "wikilink")，它包含了一個[質子和六個](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")。

## 圖表

<table>
<thead>
<tr class="header">
<th><p>符號</p></th>
<th><p>Z（<a href="../Page/質子.md" title="wikilink">p</a>）</p></th>
<th><p>N（<a href="../Page/中子.md" title="wikilink">n</a>）</p></th>
<th><p>同位素質量（<a href="../Page/原子質量單位.md" title="wikilink">u</a>）</p></th>
<th><p><a href="../Page/半衰期.md" title="wikilink">半衰期</a></p></th>
<th><p><a href="../Page/衰變方式.md" title="wikilink">衰變<br />
方式</a>[2]</p></th>
<th><p><a href="../Page/衰变产物.md" title="wikilink">衰變<br />
產物</a>[3]</p></th>
<th><p>原子核<br />
<a href="../Page/自旋.md" title="wikilink">自旋</a></p></th>
<th><p>相對<a href="../Page/豐度.md" title="wikilink">豐度</a><br />
（<a href="../Page/莫耳.md" title="wikilink">莫耳分率</a>）[4]</p></th>
<th><p>相對<a href="../Page/豐度.md" title="wikilink">豐度的變化量</a><br />
（<a href="../Page/莫耳.md" title="wikilink">莫耳分率</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/氕.md" title="wikilink"><sup>1</sup>H</a></p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td></td>
<td><p><strong><a href="../Page/稳定同位素.md" title="wikilink">穩定</a></strong>[5][6]</p></td>
<td><p><sup>+</sup></p></td>
<td></td>
<td><p>–</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>, [7]</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p>1<sup>+</sup></p></td>
<td><p>[8]</p></td>
<td><p>–</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>, [9]</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><sup>+</sup></p></td>
<td><p><a href="../Page/痕量元素.md" title="wikilink">痕量</a><ref group="n">{{tsl|en|Cosmogenic nuclide</p></td>
<td><p>Cosmogenic}}</ref>或<a href="../Page/天然放射性.md" title="wikilink">天然放射性</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td></td>
<td><p><br />
[]</p></td>
<td></td>
<td></td>
<td><p>2<sup>−</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Μ子#μ原子.md" title="wikilink">{{SimpleNuclide</a></p></td>
<td><p>2<br />
<a href="../Page/質子.md" title="wikilink">p</a></p></td>
<td><p>1<br />
<a href="../Page/緲子.md" title="wikilink">μ−</a></p></td>
<td><p>2<br />
<a href="../Page/中子.md" title="wikilink">n</a></p></td>
<td></td>
<td><p>&gt; ?</p></td>
<td><p><a href="../Page/Μ子#衰變.md" title="wikilink">μ</a></p></td>
<td><p><a href="../Page/氦-4.md" title="wikilink">{{SimpleNuclide</a>, <a href="../Page/反電中微子.md" title="wikilink"><font style="text-decoration: overline">v</font><sub>e</sub></a>, <a href="../Page/反μ中微子.md" title="wikilink"><font style="text-decoration: overline">v</font><sub>μ</sub></a></p></td>
<td><p>0<sup>+</sup>#</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td></td>
<td><p>&gt; ?</p></td>
<td><p>2</p></td>
<td></td>
<td><p>(<sup>+</sup>)</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>5</p></td>
<td></td>
<td><p><br />
[]</p></td>
<td><p>3n</p></td>
<td></td>
<td><p>2<sup>−</sup>#</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4n</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>#</p></td>
<td><p>#</p></td>
<td><p>4n</p></td>
<td></td>
<td><p><sup>+</sup>#</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**備註**：畫上\#號的數據代表沒有經過實驗的証明，僅為理論推測，而用括號括起來的代表數據不確定性。

## 註釋

<references group="n"/>

## 參考資料

## 外部链接

  - [新闻：氢-7的发现](http://physicsweb.org/articles/news/7/3/3)
  - [关于氢-4和氢-5的文章](http://content.aip.org/APCPCS/v610/i1/920_1.html)
    （需要登录阅读）

|- style="text-align:center" | width="30%" |← | width="40%"
|[同位素列表](../Page/同位素列表.md "wikilink") | width="30%" |→ |- |
width="30%" align=center| [中子的同位素](../Page/中子的同位素.md "wikilink") |
width="40%" align=center| **氫的同位素** | width="30%" align=center|
[氦的同位素](../Page/氦的同位素.md "wikilink")

[氢的同位素](../Category/氢的同位素.md "wikilink")
[H](../Category/各元素同位素列表.md "wikilink")

1.
2.  <http://www.nucleonica.net/unc.aspx>
3.  穩定的衰變產物以**粗體**表示
4.  指的是在水中。
5.  大於 。 參見[質子衰變](../Page/質子衰變.md "wikilink").
6.  此核素和<sup>3</sup>He是唯一質子數量多於中子且穩定的核素
7.  Produced during [太初核合成](../Page/太初核合成.md "wikilink")
8.  Tank hydrogen has a  abundance as low as  (mole fraction).
9.  Produced during Big Bang nucleosynthesis, but not primordial, as all
    such atoms have since decayed to <sup>3</sup>He