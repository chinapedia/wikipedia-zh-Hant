**氯化氢**（），[分子式为HCl](../Page/分子式.md "wikilink")，[室温下为无色](../Page/室温.md "wikilink")[气体](../Page/气体.md "wikilink")，遇空气中的[水汽形成白色](../Page/水汽.md "wikilink")[盐酸酸雾](../Page/盐酸.md "wikilink")。氯化氢及其水溶液[盐酸在化工中非常重要](../Page/盐酸.md "wikilink")。二者分子式均可写为HCl。

## 分子结构

氯化氢[分子是由一个](../Page/分子.md "wikilink")[氢](../Page/氢.md "wikilink")[原子和一个](../Page/原子.md "wikilink")[氯原子由](../Page/氯.md "wikilink")[共价键形成的](../Page/共价键.md "wikilink")[双原子分子](../Page/双原子分子.md "wikilink")。因为氯原子的[电负性远高于氢原子](../Page/电负性.md "wikilink")，两原子间的共价键[极性很强](../Page/极性.md "wikilink")，所以氯化氢分子[偶极矩很大](../Page/偶极矩.md "wikilink")，氯原子上有部分负[电荷](../Page/电荷.md "wikilink")，氢原子上有部分正电荷，因此氯化氢易溶于水和其他极性[溶剂](../Page/溶剂.md "wikilink")。

### 红外光谱

氯化氢的[红外光谱在](../Page/红外光谱.md "wikilink")2886 cm<sup>-1</sup>（波长约3.47
cm）处有若干尖锐[吸收峰](../Page/吸收谱线.md "wikilink")。室温下，几乎所有分子处于[基态振动](../Page/基态.md "wikilink")（v=0）。v=1的[激发态对应于](../Page/激发态.md "wikilink")2880
cm<sup>-1</sup>的吸收谱线。Q支吸收谱线因对称性禁阻而观察不到，而由分子旋转产生的P支和R支谱线则可观察到。[量子力学原理只允许部分转动模式](../Page/量子力学.md "wikilink")：转动[量子数J只能是整数](../Page/量子数.md "wikilink")，而且其变化量ΔJ只能是±1。根据

  -
    E(J) = h·B·J(J+1)，

因为B远小于振动能级，分子转动所需的能量很小，一般处于[微波波段](../Page/微波.md "wikilink")，而分子的振动能级处于红外波段，这样用红外光谱仪观察普通气室就能观测到分子的转动-振动能级。

自然界中的氯有两种[同位素](../Page/同位素.md "wikilink")：<sup>35</sup>Cl和<sup>37</sup>Cl，丰度比为3:1。虽然两种分子振子的[弹性系数相差无几](../Page/弹性系数.md "wikilink")，但[约化质量不同](../Page/约化质量.md "wikilink")，所以转动能级也不同。因此每条吸收谱线其实是紧靠着的两条，强度比也是3:1。

## 性质

### 气态

氯化氢是无色、挥发性气体，有刺激性气味。它易溶于[水](../Page/水.md "wikilink")，在0℃时，1体积的水大约能[溶解](../Page/溶解.md "wikilink")500体积的氯化氢。氯化氢的水[溶液呈](../Page/溶液.md "wikilink")[酸性](../Page/酸性.md "wikilink")，叫做[氢氯酸](../Page/氢氯酸.md "wikilink")，习惯上叫[盐酸](../Page/盐酸.md "wikilink")。

氯化氫氣體溶於水生成鹽酸，所以打开盛有盐酸的试剂瓶后挥发出来的氯化氢气体常與空氣中的水汽形成鹽酸酸霧。常用[氨水來檢驗氯化氢的存在](../Page/氨水.md "wikilink")，氨水中挥发出来的[氨气會與氯化氫反應生成白色的](../Page/氨气.md "wikilink")[氯化铵微粒](../Page/氯化铵.md "wikilink")。氯化氫有強烈的偶極。

### 溶液

氯化氢因为分子极性很强，所以易溶于水和其他极性溶剂。溶于水后，氯化氢和水分子反应生成[水合氢离子和](../Page/水合氢离子.md "wikilink")[氯离子](../Page/氯离子.md "wikilink")，反应方程式如下：

  -
    HCl + H<sub>2</sub>O → H<sub>3</sub>O<sup>+</sup> + Cl<sup>−</sup>

反应所形成的溶液叫做[盐酸](../Page/盐酸.md "wikilink")，是一种强酸。这是因为氯化氢的[酸度系数](../Page/酸度系数.md "wikilink")*K*<sub>a</sub>很大，在水中完全[电离](../Page/电离.md "wikilink")。即使在非水溶剂中，氯化氢也能呈酸性。比如它可以溶于[甲醇](../Page/甲醇.md "wikilink")，使其[质子化](../Page/质子化.md "wikilink")，并可作为无水环境中的酸性[催化剂](../Page/催化剂.md "wikilink")。

  -
    HCl + CH<sub>3</sub>OH → CH<sub>3</sub>O<sup>+</sup>H<sub>2</sub> +
    Cl<sup>−</sup>

因此，氯化氢具有[腐蚀性](../Page/腐蚀性.md "wikilink")，尤其在潮湿环境中。

| 温度 (°C)                        | 0   | 20  | 30  | 50  |
| ------------------------------ | --- | --- | --- | --- |
| 水                              | 823 | 720 | 673 | 596 |
| 甲醇                             | 513 | 470 | 430 |     |
| [乙醇](../Page/乙醇.md "wikilink") | 454 | 410 | 381 |     |
| [乙醚](../Page/乙醚.md "wikilink") | 356 | 249 | 195 |     |

氯化氢在常见溶剂中的溶解性 (g/L)\[1\]

### 固态

固态氯化氢于98.4
K发生[相变](../Page/相变.md "wikilink")。[X射线粉末衍射结果表明此时该固体从](../Page/X射线粉末衍射.md "wikilink")[正交晶系变为](../Page/正交晶系.md "wikilink")[立方晶系](../Page/立方晶系.md "wikilink")。在两种晶系中氯原子均按[面心立方排列](../Page/面心立方.md "wikilink")，但氢原子无法定位。\[2\]
对其[光谱](../Page/光谱.md "wikilink")、[介电性以及对氯化氘](../Page/介电质.md "wikilink")（DCl）结构的测定显示氯化氢分子在固体中排成之字形，与[氟化氢相同](../Page/氟化氢.md "wikilink")。\[3\]

## 合成方法

工业上合成的氯化氢大多用于生产[盐酸](../Page/盐酸.md "wikilink")。

### 直接合成

在[氯碱工业中](../Page/氯碱工业.md "wikilink")，电解盐水会生成[氯气](../Page/氯气.md "wikilink")（Cl<sub>2</sub>）、[氢氧化钠](../Page/氢氧化钠.md "wikilink")（NaOH）和[氢气](../Page/氢气.md "wikilink")（H<sub>2</sub>）。紫外光照下氯气和氢气化合生成氯化氢。
2NaCl+2H<sub>2</sub>O→Cl<sub>2</sub>+2NaOH+H<sub>2</sub>

  -
    Cl<sub>2</sub>(g) + H<sub>2</sub>(g) → 2 HCl(g)

这是一个[放热反应](../Page/放热反应.md "wikilink")，反应装置称为氯化氢[烘箱或氯化氢燃烧器](../Page/烘箱.md "wikilink")。生成的氯化氢气体经[去离子水吸收](../Page/去离子水.md "wikilink")，成为化学纯的盐酸。反应产物相当纯，可用于[食品工业](../Page/食品工业.md "wikilink")。

### 有机合成

产量最大的合成方式是伴随[有机物](../Page/有机物.md "wikilink")（如[聚四氟乙烯](../Page/聚四氟乙烯.md "wikilink")、[氟利昂和其他](../Page/氟利昂.md "wikilink")[氯氟烃](../Page/氯氟烃.md "wikilink")，还有[一氯乙酸和](../Page/一氯乙酸.md "wikilink")[聚氯乙烯](../Page/聚氯乙烯.md "wikilink")）的[卤化进行的](../Page/卤化.md "wikilink")。通常这种方式生产的氯化氢当场在密闭环境下就能利用。在这个反应中，[烃中的氢原子被氯原子取代](../Page/烃.md "wikilink")，然后和剩下的氯原子结合形成氯化氢分子。接下来氟化反应将氯原子取代，又生成氯化氢。

  -
    R-H + Cl<sub>2</sub> → R-Cl + HCl
    R-Cl + HF → R-F + HCl

生成的氯化氢气体或是直接利用，或是被水吸收形成工业纯的盐酸。

### 实验室合成

要制取少量氯化氢，可在氯化氢发生器中用浓[硫酸或无水](../Page/硫酸.md "wikilink")[氯化钙使盐酸脱水](../Page/氯化钙.md "wikilink")。另外，氯化氢可由浓硫酸和氯化钠反应生成：\[4\]

  -
    NaCl + H<sub>2</sub>SO<sub>4</sub> → NaHSO<sub>4</sub> + HCl↑

该反应于室温下进行。如果氯化钠过量，且温度超过200℃，会有进一步反应：

  -
    NaHSO<sub>4</sub> + NaCl → Na<sub>2</sub>SO<sub>4</sub> + HCl↑

总的化学方程式可以表示如下：

  -
    2NaCl + H<sub>2</sub>SO<sub>4</sub> → Na<sub>2</sub>SO<sub>4</sub> +
    2HCl↑

试剂必须干燥才能使反应进行。

水解某些活跃的氯化物如[三氯化磷](../Page/三氯化磷.md "wikilink")、[五氯化磷](../Page/五氯化磷.md "wikilink")、[氯化亚砜和](../Page/氯化亚砜.md "wikilink")[酰氯也能制得氯化氢](../Page/酰氯.md "wikilink")。例如，向五氯化磷逐滴加入冷水可由以下反应生成氯化氢：

## 参考资料

## 参见

  - [鹽酸](../Page/鹽酸.md "wikilink")
  - [氢氟酸](../Page/氢氟酸.md "wikilink") - [溴化氢](../Page/溴化氢.md "wikilink")
    - [碘化氢](../Page/碘化氢.md "wikilink") -
    [砹化氢](../Page/砹化氢.md "wikilink")

[Category:无机酸](../Category/无机酸.md "wikilink")
[Category:氢化合物](../Category/氢化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:非金属卤化物](../Category/非金属卤化物.md "wikilink")
[Category:空气污染物](../Category/空气污染物.md "wikilink")
[Category:化學武器](../Category/化學武器.md "wikilink")

1.
2.
3.
4.