**氯化鋇**（[化学式](../Page/化学式.md "wikilink")：[Ba](../Page/钡.md "wikilink")[Cl](../Page/氯.md "wikilink")<sub>2</sub>）是[钡的](../Page/钡.md "wikilink")[氯化物](../Page/氯化物.md "wikilink")，有毒，灼烧時產生黃綠色的光。

## 製备

氯化鋇可以由[碳酸鋇](../Page/碳酸鋇.md "wikilink")（自然界中的[毒重石](../Page/毒重石.md "wikilink")）或[氫氧化鋇和](../Page/氫氧化鋇.md "wikilink")[鹽酸的反應得到](../Page/鹽酸.md "wikilink")。工業上可以從[硫酸鋇經過兩個步驟得到](../Page/硫酸鋇.md "wikilink")：

  -
    [BaSO<sub>4</sub>](../Page/硫酸鋇.md "wikilink") + 4
    [C](../Page/碳.md "wikilink") → [BaS](../Page/硫化钡.md "wikilink") +
    4 [CO](../Page/一氧化碳.md "wikilink")（加熱）
    BaS + [CaCl<sub>2</sub>](../Page/氯化鈣.md "wikilink") →
    BaCl<sub>2</sub> + [CaS](../Page/硫化鈣.md "wikilink") （溶掉混合物）

## 化學性質

氯化鋇溶於水解离为Ba<sup>2+</sup>和Cl<sup>-</sup>，解离出的离子可以产生沉淀反应：\[1\]

  -
    BaCl<sub>2</sub> + Na<sub>2</sub>SO<sub>4</sub> → 2 NaCl +
    BaSO<sub>4</sub>↓
    BaCl<sub>2</sub> + K<sub>2</sub>CrO<sub>4</sub> → 2 KCl +
    BaCrO<sub>4</sub>↓
    BaCl<sub>2</sub> + Na<sub>2</sub>C<sub>2</sub>O<sub>4</sub> → 2 NaCl
    + BaC<sub>2</sub>O<sub>4</sub>↓
    BaCl<sub>2</sub> + 2 AgNO<sub>3</sub> → 2 AgCl↓ +
    Ba(NO<sub>3</sub>)<sub>2</sub>

在1000℃，熔化的氯化钡和[钠有如下平衡](../Page/钠.md "wikilink")：\[2\]

  -
    2 Na + BaCl<sub>2</sub> ↔ 2 NaCl + Ba

## 用途

在[實驗室](../Page/實驗室.md "wikilink")，它可用來測試硫酸鹽。

在[煙花中](../Page/煙花.md "wikilink")，。

## 安全

氯化钡与其它的水溶性钡盐一样，因其可在水溶液中[离解出钡离子](../Page/离解.md "wikilink")，所以都是有高毒性的。可溶性钡盐中毒通常使用[硫酸镁作为解毒剂](../Page/硫酸镁.md "wikilink")，它们会反应产生[硫酸钡](../Page/硫酸钡.md "wikilink")，因其不溶性而相对于氯化钡则为无毒的。

## 参考文献

[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:碱土金属卤化物](../Category/碱土金属卤化物.md "wikilink")
[Category:钡化合物](../Category/钡化合物.md "wikilink")
[Category:烟火着色剂](../Category/烟火着色剂.md "wikilink")

1.  陈寿春. 重要无机化学反应（第三版）. 上海科学技术出版社, 1982. 第一章 阳离子. 四-2. 钡(Ba)

2.