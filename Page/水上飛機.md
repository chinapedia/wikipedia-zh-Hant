[CaproniCa.60.jpg](https://zh.wikipedia.org/wiki/File:CaproniCa.60.jpg "fig:CaproniCa.60.jpg")
**水上飛機**（）泛指利用水面（包括[海洋](../Page/海洋.md "wikilink")、[湖泊與](../Page/湖泊.md "wikilink")[河川](../Page/河川.md "wikilink")）起飛、降落與停靠的[飛機](../Page/飛機.md "wikilink")，可以是民用或[军用飞机](../Page/军用飞机.md "wikilink")。

## 型態

水上飛機依照與水面接觸的設計方式，可以區分為四類：

### 浮筒水上飛機

[Wasserflugzeug_01_KMJ.jpg](https://zh.wikipedia.org/wiki/File:Wasserflugzeug_01_KMJ.jpg "fig:Wasserflugzeug_01_KMJ.jpg")
浮筒水上飛機（Floatplane）一般多是小型飛機所採用，在機身下方裝有一或兩個浮筒，將機身與水面分離，有些飛機在機翼兩邊還裝有小型輔助浮筒，以避免飛機因為往兩側傾斜還有翻覆的危險。

只有一個浮筒設計的水上飛機，浮筒位於機腹正下方。兩個浮筒設計的，則是分別位於機腹兩側的位置。

自[一次世界大戰開始](../Page/一次世界大戰.md "wikilink")，[巡洋艦以上的](../Page/巡洋艦.md "wikilink")[軍艦多半會攜帶一架或者是更多的浮筒水上飛機](../Page/軍艦.md "wikilink")，以進行遠距離偵查，目標搜索與火炮射擊時的誤差修正等任務。這些飛機平時停放在軍艦上面，有些是從[炮塔上方的起飛滑軌彈射出去](../Page/炮塔.md "wikilink")，有些噸位較大的艦艇有專屬的滑軌或者是機庫安放這些觀測用的水上飛機。當飛機降落的時候則是先降落在軍艦附近的海面上，然後由軍艦上的起重機將飛機吊回艦上安置妥當，以進行下一次的任務。這一類搭載方式的浮筒水上飛機也可以由水面直接起飛。

### 飛行艇

[Kawanishi_H8K_Flying_Boat_Emily_h8k-1.jpg](https://zh.wikipedia.org/wiki/File:Kawanishi_H8K_Flying_Boat_Emily_h8k-1.jpg "fig:Kawanishi_H8K_Flying_Boat_Emily_h8k-1.jpg")\]\]
**[飛行艇](../Page/飛行艇.md "wikilink")**（英文：flying
boat）又稱[飛船](../Page/飛船.md "wikilink")，是一種有著[船體的定翼](../Page/船體.md "wikilink")**水上飛機**，可以降落在水面上。跟[浮筒飛機不同](../Page/浮筒飛機.md "wikilink")，體型通常更大，而且機身本來便能產生浮力，不過也有不少大型飛船會在機翼加裝浮筒確保停泊時的平衡與浮力，或會以下翼或突出像翼的[副翼產生的浮力來達成穩定](../Page/副翼.md "wikilink")。飛艇或許是二十世紀前半最大型的[大氣載具](../Page/大氣載具.md "wikilink")，這個地位直到二戰時才被[轟炸機所取代](../Page/轟炸機.md "wikilink")。它們的好處是可以用水體代替昂貴的陸上跑道，使它們在戰間期擔任國際航空線的基礎\[1\]。也常被用於海上巡邏與空海救援。在二戰後使用率逐漸縮小，部分原因是因為在戰爭時對於機場的投資。在二十一世紀，飛艇使用在幾項合適的用途，如海上救援、對森林大火投水和造訪未開發或無道路的地區。現在的水上飛機通常是兩棲的，有起落架可以在路面降落。飛艇的另外一種衍生型是利用[地面效應的](../Page/地面效應.md "wikilink")[地效飛行器](../Page/地效飛行器.md "wikilink")，不過一般並不將這種飛行器包含在飛艇的分類當中。

### 兩棲飛機

[Consolidated_PBY_vr.jpg](https://zh.wikipedia.org/wiki/File:Consolidated_PBY_vr.jpg "fig:Consolidated_PBY_vr.jpg")[PBY卡特琳娜水上飛機有內建機輪](../Page/PBY卡特琳娜水上飛機.md "wikilink")，可以直接在陸地上起降\]\]
兩棲飛機（Amphibian）又稱水陸兩用機，是指可以加裝或者是本身就有機輪，能夠直接在陸地上操作與起降的水上飛機，這一類飛機可以視需要在水面或者是陸上機場活動，因此有兩棲飛機的稱呼。

### 水上直升機

在[直升機底部以浮筒替代機輪或雪橇而能在水面降落的便是水上直升機](../Page/直升機.md "wikilink")。由於直升機是垂直起降，故浮筒造型可以不用考慮流體力學，比較需注意的是浮筒產生的浮力能確保直升機在水面安定降落。

## 任務與服務類型

水上飛機在[二次世界大戰結束以前無論在軍用或者是民用兩個環境使用都相當廣泛](../Page/二次世界大戰.md "wikilink")。在軍用環境下單任的任務包含：

  - 偵查
  - 觀測（艦上火炮射擊標定與誤差修正）
  - 搜索
  - 運輸
  - 反潛
  - 水上救難等。

民用環境下，大型水上飛機是早期跨洋飛行的主要機種之一，尤其是需要經過遠距離的海洋而中間有沒有適當的機場能夠停留加油的航線上。水上飛機能夠利用沒有陸上機場的島嶼或者是船隻進行中途加油與整補的工作。

## 歷史

第一架水上飛機是由[法國](../Page/法國.md "wikilink")[工程師](../Page/工程師.md "wikilink")[亨利·法布爾於](../Page/亨利·法布爾.md "wikilink")1910年3月展示的Le
Canard。第一次正式有水上飛機這個稱呼則是1913年[英國](../Page/英國.md "wikilink")[邱吉爾所提出的](../Page/邱吉爾.md "wikilink")。水上飛機與使用陸上機場的飛機發展的時間相當接近，早期發展的著眼點在於：

1.  地球上超過70%的面積是水所覆蓋，包過海洋，湖泊與河川，這些都是水上飛機可以操作的區域。
2.  早期陸上機場數量不多，許多機場的跑道並未經過適當的整理，早期的起落架容易產生意外。
3.  即使有跑道的機場也會因為風向的關係限制飛機起降的方向，水上飛機受到的限制較小。
4.  對於陸地面積較小但是被水環繞的地區，或者是遠距離航線上沒有可以使用的機場時，只有水上飛機能夠到達這些地點。
5.  有些需要跑道長度特別高的飛機，像是大型客機或者是競速機，水面給予他們良好的起降場所。

### 一次世界大戰

[一次世界大戰期間](../Page/一次世界大戰.md "wikilink")，水上飛機已經被搭載於[巡洋艦噸位以上的艦艇上擔任偵查與協助](../Page/巡洋艦.md "wikilink")[艦炮射擊的任務](../Page/艦炮.md "wikilink")，同時也擔任[反潛](../Page/反潛.md "wikilink")、船團護航、沿海巡邏與轟炸等各種任務。並發展出[水上飛機母艦作專用載台](../Page/水上飛機母艦.md "wikilink")。

### 兩次大戰期間

兩次大戰期間，民航業的發展逐漸興起，雖然許多大型飛機的航程可以抵達過去無法到達的地區，可是機場嚴重缺乏或者是跑道狀況不佳的狀況，使得水上飛機成為這些航線的最佳候選者。諸如由[英國前往](../Page/英國.md "wikilink")[印度或是](../Page/印度.md "wikilink")[澳大利亞等國家航線都在這個時間陸續建立](../Page/澳大利亞.md "wikilink")。即使大型飛機的航程足夠飛越某些海洋地區，然而在航線途中欠缺機場的狀況下，萬一陸上飛機發上故障或者是需要緊急降落時，將會產生嚴重的困擾，而水上飛機恰巧得以滿足這些方面的需求。

此外，各種競速機比賽的參加者也多利用水上飛機作為設計的型態，這是著眼於高速飛行下需要很長的跑道降落，為了減少比賽地區的限制，利用水面起降是一種極佳的解決方式。雖然浮筒與相關的結構會產生額外的阻力與重量，這個階段許多競速機都有非常優異的成績，像是英國[超級馬林](../Page/超級馬林.md "wikilink")（Supermarine）的S.6B競速機曾經拿下1931年[史奈德盃競速比賽的冠軍](../Page/史奈德盃.md "wikilink")，而他在該年創下最高的飛行紀錄是651.2[公里](../Page/公里.md "wikilink")/時的高速。

### 二次世界大戰

[M6A1.jpg](https://zh.wikipedia.org/wiki/File:M6A1.jpg "fig:M6A1.jpg")打算轟炸巴拿馬運河的晴嵐\]\]
[H-4_Hercules_2.jpg](https://zh.wikipedia.org/wiki/File:H-4_Hercules_2.jpg "fig:H-4_Hercules_2.jpg")
[二次世界大戰時期水上飛機的使用與發展到達一個巔峰的狀態](../Page/二次世界大戰.md "wikilink")，包括[美國](../Page/美國.md "wikilink")、[英國](../Page/英國.md "wikilink")、[德國](../Page/德國.md "wikilink")、[日本與](../Page/日本.md "wikilink")[義大利都有各種軍用水上飛機](../Page/義大利.md "wikilink")，這些飛機除了繼續他們的前輩所擔負的巡邏、護航、偵查、反潛、轟炸與射擊標定之外，也擔任對其他海上目標的[魚雷攻擊](../Page/魚雷.md "wikilink")，或者是對其他的水上飛機進行空戰等等，

[日本的](../Page/日本.md "wikilink")[愛知飛機公司甚至設計出可以搭載於](../Page/愛知.md "wikilink")[伊-400號潛特型](../Page/伊號第四〇〇潛艇.md "wikilink")[潛艇的特殊](../Page/潛艇.md "wikilink")[轟炸機](../Page/轟炸機.md "wikilink")：M6A1[晴嵐](../Page/晴嵐.md "wikilink")，預備對[巴拿馬運河進行轟炸](../Page/巴拿馬運河.md "wikilink")，阻斷[美國海軍增援](../Page/美國海軍.md "wikilink")[太平洋戰區的速率](../Page/太平洋.md "wikilink")。

戰爭期間美軍曾向[休斯飞机公司採購一架越洋](../Page/休斯飞机公司.md "wikilink")[運輸機](../Page/運輸機.md "wikilink")，[霍华德·休斯因而開發出巨型飛行艇](../Page/霍华德·休斯.md "wikilink")[休斯H-4大力神](../Page/休斯H-4大力神.md "wikilink")，然而該機實際上到戰爭結束才完成，而且噪音、耗油量、舒適與安全性均不佳，結果遭放棄採用。H-4大力神至今仍是最大的飛機之一，翼展仍是世界第一，高度同[A380](../Page/空中客车A380.md "wikilink")，長度僅次[An-225](../Page/安托諾夫An-225運輸機.md "wikilink")。

### 二戰之後

[F2Y_Sea_Dart_2.jpg](https://zh.wikipedia.org/wiki/File:F2Y_Sea_Dart_2.jpg "fig:F2Y_Sea_Dart_2.jpg")：[美國F](../Page/美國.md "wikilink")2Y海標槍\]\]
[ShinMaywa_US-2_at_Atsugi.jpg](https://zh.wikipedia.org/wiki/File:ShinMaywa_US-2_at_Atsugi.jpg "fig:ShinMaywa_US-2_at_Atsugi.jpg")的救難機\]\]
二次世界大戰時期，陸上機場的數量大幅增加，跑道品質較以往進步，飛機的性能與可靠性也顯著提升，同時[空中加油技術逐漸成熟](../Page/空中加油.md "wikilink")，水上飛機的地位開始受到影響，不過在1950年代還是有新的研發計畫，包括[美國海軍的](../Page/美國海軍.md "wikilink")[F2Y海標式噴射水上戰鬥機計畫](../Page/F2Y海標式噴射水上戰鬥機.md "wikilink")。

此外，民間使用水上飛機的用途也從運輸擴展到救護與消防等其他工作上面，尤其是利用水上飛機可以在水面降落的同時，吸取大量的淡水進行森林火災的撲滅或壓制。

然而，各種飛機技術的進展與相關設備的完善，使得水上飛機的優勢慢慢的消失，特別是在遠程跨洋航線上，陸上起降的噴射客機取代水上飛機成為各家[航空公司的主流](../Page/航空公司.md "wikilink")，而噴射動力的特性也必須經過額外處理才能夠使用於水上飛機，同時還得要考慮到避免吸入任何海水的情況。原先水面救護的工作也隨著[直升機的成熟化而拱手讓賢](../Page/直升機.md "wikilink")。

因此水上飛機逐漸退出軍用環境，僅有少數國家繼續採用擔任救護或者是反潛等任務。民用市場則以救護，消防與中小型交通與運輸為水上飛機的主要活動市場。

{{-}}

## 參見

  - [岸基機](../Page/岸基機.md "wikilink")
  - [地效飛行器](../Page/地效飛行器.md "wikilink")
  - [艦載機](../Page/艦載機.md "wikilink")
  - [水上飛機母艦](../Page/水上飛機母艦.md "wikilink")

## 註釋

## 參考資料

  - Philip Jarrett，Biplane to Monoplane: Aircraft Development,
    1919-39，Conway Maritime Press，ISBN 0-85177-874-7

## 外部連結

  - [Convair YF-7A
    'Seadart'](https://web.archive.org/web/20070506201058/http://aeroweb.brooklyn.cuny.edu/specs/convair/yf-7a.htm)
  - [水轰-5水上反潜巡逻机](http://www.airforceworld.com/pla/sh5.htm)
  - [Convair YF2Y-1 Sea
    Dart](http://www.fiddlersgreen.net/AC/aircraft/Convair-SeaDart/Seadart.php)
  - [The Cutaway PBY Display](http://home.earthlink.net/~cutawaypby/)
  - [森林消防飞机](https://web.archive.org/web/20070422074750/http://www.losn.com.cn/hkht/fj/slxffj.htm)
  - [US-2：日本水上飛機要當反潛新軍](http://military.people.com.cn/BIG5/42963/3818531.html)
  - [中国人民解放军装备-水轰5轰炸机](http://news.sohu.com/03/50/subject201195003.shtml)
  - [The Seaplane Pilots Association](http://www.seaplanes.org/)
  - [PBY-5A SUPER CATALINA](http://www.superthree.com/)
  - [The Shin Meiwa PS-1 / US-1 & Harbin SH-5 Flying
    Boats](http://www.vectorsite.net/avps1.html)
  - ["水轟五"水上飛機](https://web.archive.org/web/20050314060951/http://vm.rdb.nthu.edu.tw/cwm/weapon/wp7/wp7-05.html)
  - [The Canadair CL-215
    & 415](https://web.archive.org/web/20061209074332/http://www1.airliners.net/info/stats.main?id=119)
  - [The Beriev
    Be200](https://web.archive.org/web/20061209074054/http://www1.airliners.net/info/stats.main?id=84)
  - [The Republic RC3
    Seabee](https://web.archive.org/web/20061209081949/http://www1.airliners.net/info/stats.main?id=335)
  - [WWW.PBY.COM Home Page](http://www.pby.com/)

[\*](../Category/水上飞机.md "wikilink")
[Category:航空器类型](../Category/航空器类型.md "wikilink")

1.  <http://www.clipperflyingboats.com/>