[Admiralty_Arch_2007_5.jpg](https://zh.wikipedia.org/wiki/File:Admiralty_Arch_2007_5.jpg "fig:Admiralty_Arch_2007_5.jpg")

**水师提督门**（英語：**Admiralty
Arch**）是[英国](../Page/英国.md "wikilink")[伦敦的一座纪念性建筑物](../Page/伦敦.md "wikilink")，建于1912年。水师提督门位于伦敦[西敏市中心](../Page/西敏市.md "wikilink")，连通[特拉法加廣場和](../Page/特拉法加廣場.md "wikilink")[白金汉宫前的](../Page/白金汉宫.md "wikilink")[林荫路](../Page/林荫路.md "wikilink")，因连接旧海军部（水师提督衙门）而得名，又稱作**海軍部拱門**。门楼在特拉法加广场的西南角，林荫路的东北端。
该建筑名为门，实为楼（参见[新华门](../Page/新华门.md "wikilink")）。2012年以前在楼内办公的有[内阁办公厅和首相策略处](../Page/内阁.md "wikilink")。

门楼左右为四层。中间部分下部是五道券门（三道车行，两道人行），上有一层。

水师提督门是按英王[爱德华七世之命建造的](../Page/爱德华七世.md "wikilink")，以纪念王母，[维多利亚女王](../Page/维多利亚女王.md "wikilink")。门洞上方有[拉丁语铭文](../Page/拉丁语.md "wikilink")：

<center>

<small> : ANNO : DECIMO : EDWARDI : SEPTIMI : REGIS :
: VICTORIÆ : REGINÆ : CIVES : GRATISSIMI : MDCCCCX :</small>

</center>

意即“王爱德华七世十年：臣民感维多利亚女王之恩而立：1910”。

水师提督门平时悬挂英国国旗，重要庆典场合（作为海军部建筑）悬挂海军旗——[白船旗](../Page/白船旗.md "wikilink")。水师提督拱门是从[特拉法加广场通往](../Page/特拉法加广场.md "wikilink")[林荫路的仪仗入口](../Page/林荫路.md "wikilink")，而林荫路本身则是[白金汉宫前的仪仗路](../Page/白金汉宫.md "wikilink")。因此，包括皇家婚礼、葬礼、加冕等重要场合中的銮驾队伍和其他重要庆典中的游行队伍（例如2012年奥运会和残奥会后的游行）都会从此门下通过。\[1\]

2012年，作为政府紧缩开支的项目，水师提督门被租予[西班牙地产商塞拉诺](../Page/西班牙.md "wikilink")（），用于改造为豪华旅馆、餐厅及四套公寓。租期为125年。\[2\]楼内公寓2016年发售，旅馆预计在2020年开业。

## 参考资料

[Category:倫敦紀念性建築物](../Category/倫敦紀念性建築物.md "wikilink")
[Category:伦敦的门](../Category/伦敦的门.md "wikilink")
[Category:维多利亚女王](../Category/维多利亚女王.md "wikilink")
[Category:英國皇家海軍](../Category/英國皇家海軍.md "wikilink")

1.  Tom Peck, [Admiralty Arch to become London's next landmark hotel
    after sale to Spanish
    investor](http://www.independent.co.uk/news/uk/home-news/admiralty-arch-to-become-londons-next-landmark-hotel-after-sale-to-spanish-investor-8226016.html),
    *The Independent*, 25 October 2012
2.  David Batty, [London landmark Admiralty Arch sold to become luxury
    hotel](http://www.guardian.co.uk/culture/2012/oct/24/admiralty-arch-sold-luxury-hotel?newsfeed=true),
    *The Guardian*, 24 October 2012