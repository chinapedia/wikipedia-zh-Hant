**水戶蜀葵**（拉丁字：Mito
HollyHock）是日本一支職業足球球隊，目前在[日本職業足球聯賽乙級](../Page/日本職業足球聯賽.md "wikilink")（J2）比賽。

## 簡介

球隊在1990年成立。成立時的球隊名稱是Prima Aseno FC，後來改名為土浦Prima Ham
FC，在1996年地區聯賽決勝戰升上JFL。1997年因其企業Prima退出贊助，因此球隊與當時的FC水戶合併。命名為水戶蜀葵，球隊的名字**Hollyhock**的由來是[江戶時代](../Page/江戶時代.md "wikilink")[水戶德川家的家紋](../Page/水戶德川家.md "wikilink")，在英語可解作葵。

球隊在1999年降級到新的日本足球聯賽（1999年擴展為J2），到2000年升級到J2。但球隊目前仍然沒有什麼大突破，球隊從未奪得比賽冠軍。

球隊的根據地在[茨城縣](../Page/茨城縣.md "wikilink")[水戶市](../Page/水戶市.md "wikilink")，主場是[笠松運動公園陸上競技場](../Page/笠松運動公園陸上競技場.md "wikilink")。

## 球員名單

## 著名前球员

  - [塞爾吉奧·路易斯·多尼采蒂](../Page/塞爾吉奧·路易斯·多尼采蒂.md "wikilink")

  -
  - [阮公凤](../Page/阮公凤.md "wikilink")

## 參考資料

  - [球隊簡介](https://web.archive.org/web/20070826085933/http://www.mito-hollyhock.net/f13pro/13pro01.html)

## 外部連結

  - [官方網站](http://www.mito-hollyhock.net)

[Category:日本足球俱樂部](../Category/日本足球俱樂部.md "wikilink")
[Category:水戶市](../Category/水戶市.md "wikilink")
[Category:1994年建立的足球俱樂部](../Category/1994年建立的足球俱樂部.md "wikilink")
[Category:1994年日本建立](../Category/1994年日本建立.md "wikilink")