[Land_ocean_ice_cloud_1024.jpg](https://zh.wikipedia.org/wiki/File:Land_ocean_ice_cloud_1024.jpg "fig:Land_ocean_ice_cloud_1024.jpg")
**水文学**属于[地理學](../Page/地理學.md "wikilink")，研究的是关于[地球](../Page/地球.md "wikilink")-{表}-面、[土壤中](../Page/土壤.md "wikilink")、[岩石下和](../Page/岩石.md "wikilink")[大气中](../Page/大气.md "wikilink")[水的各種行為](../Page/水.md "wikilink")，包含水循环、含量、分布、物理化学特性、對物質影响以及与所有生物之间关系的[科学](../Page/科学.md "wikilink")。

## 水文的基本概念

### 水循環

水分子藉由吸收[太陽的](../Page/太陽.md "wikilink")[輻射能](../Page/輻射能.md "wikilink")，使其從海水或是陸地蒸發，可因為地面溫度過低而直接在蒸發後不久凝結於地表，甚至凝固稱做[霜](../Page/霜.md "wikilink")，大多數情況蒸發的水分子，或稱做[水蒸氣](../Page/水蒸氣.md "wikilink")，上升至高空，因寒冷而凝結成[雲](../Page/雲.md "wikilink")，再以[雨水](../Page/雨.md "wikilink")、[雪](../Page/雪.md "wikilink")、[冰雹等形式降至海洋](../Page/冰雹.md "wikilink")、陸地，若降落在寒冷高地則可能形成[冰川](../Page/冰川.md "wikilink")，或在較為平緩的地勢匯集形成湖泊或河流，或是滲入[土壤成為](../Page/土壤.md "wikilink")[地下水](../Page/地下水.md "wikilink")，隨後河水、湖水、地下水等液態水分子再流入海洋，這當中可能還伴隨著蒸發。由於這個過程可以不斷的循環發生，因此稱為水循環。在水循環的過程中，還必需顧及生物系統對於水資源的遲延作用，同時地表滯留洼蓄、植物截留與中間流的效應也是水循環的一部份。

### 水平衡

**水平衡**是指地表上某一區域，在一定時間之內，水量收入和支出的均衡狀態。全球海陸總[降水量又與總](../Page/降水.md "wikilink")[蒸發量保持平衡](../Page/蒸發.md "wikilink")，大約57萬立方公里。儘管整體來說全球保持水平衡，但不同地區，因為氣候的差異，使得水平衡會隨著[時間](../Page/時間.md "wikilink")、[空間產生變化](../Page/空間.md "wikilink")，一般而言，降水量與蒸發量的差額稱為**逕流量**，包括地面水體與地下水。逕流量可以反應一地可用水的盈虧，也就是一個地區是否為剩水區或缺水區，更意味著逕流量的差距，在不同地區會影響人們用水的習慣。例如在[以色列](../Page/以色列.md "wikilink")，該國因地處副熱帶高壓區，因大量蒸發與缺少降水，儘管[約旦河流經而該國](../Page/約旦河.md "wikilink")，逕流量仍為負值，而在農業上發展出對水的高度節約使用技術，稱為「[滴灌](../Page/滴灌.md "wikilink")」。

### 水資源

[地球上總](../Page/地球.md "wikilink")[水量約有](../Page/水.md "wikilink")13.8億立方公里。其中以[海水的儲量最大](../Page/海水.md "wikilink")，佔據96.5%，而[人類生活所仰賴的](../Page/人類.md "wikilink")[淡水資源](../Page/淡水.md "wikilink")，則分布在[地下水](../Page/地下水.md "wikilink")、[河川](../Page/河川.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")、[冰川等分布在陸地上的水體](../Page/冰川.md "wikilink")，實際上可以讓人類直接利用者為地下淡水、淡水湖、河水，**不及世界總水量1%**（見下文水體儲量表格）在各[大陸上](../Page/大陸.md "wikilink")，淡水資源分布極為不均，世界上水資源最豐富的地區是赤道區，尤其是[南美洲與](../Page/南美洲.md "wikilink")[非洲具有陸地的](../Page/非洲.md "wikilink")[赤道區](../Page/赤道.md "wikilink")，而相對較為乾燥的地區，則是[副熱帶高壓區](../Page/副熱帶高壓.md "wikilink")，以及[溫帶大陸內部](../Page/溫帶.md "wikilink")。

### 水污染

水體之中含有[化合物](../Page/化合物.md "wikilink")，可能包括[離子化合物或](../Page/離子化合物.md "wikilink")[分子化合物等](../Page/分子化合物.md "wikilink")，一旦這些化合物包含著對人類、動植物或是生態環境帶來危害，便稱為水污染。例如在河川上游的[水庫](../Page/水庫.md "wikilink")、集水區域，因為人類活動而受到污染時，可能會造成[優養化的現象](../Page/優養化.md "wikilink")，則會干擾水資源的提供與分配。而下游則會因為[工業廢水](../Page/工業.md "wikilink")、[都市廢水等](../Page/都市.md "wikilink")，使得水資源失去了被觀賞、遊玩、垂釣、生態棲息的功能。

## 特殊形態的水體

### 河流

當降水量超過地面的滲入量，過剩的雨水會沿著地表的坡度向海拔較低處流動，經年累月會在地表上刻劃出小的紋路或水溝，長期下來會因為持續侵蝕溝道而形成具有固定流道的[河流](../Page/河流.md "wikilink")。

### 湖泊

水體在地表凹陷處長及積聚，終年不乾不涸，且水深在五公尺以上者稱為**湖泊**。分布於[外流河流域的](../Page/外流河.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")，由於湖水外流，鹽類不會堆積，因此多為[淡水湖](../Page/淡水湖.md "wikilink")；分布遠離海岸的[內流河流域之湖泊](../Page/內流河.md "wikilink")，[鹽類積聚容易](../Page/鹽.md "wikilink")，多成為[鹹水湖](../Page/鹹水湖.md "wikilink")。

### 冰河

高山或兩極地區的積雪由本身壓力形成冰塊，又因[重力作用而沿地表坡度滑動](../Page/重力.md "wikilink")，這種移動的大冰塊稱為**冰河**，亦稱[冰川](../Page/冰川.md "wikilink")。

### 地下水

廣義**地下水**指降水滲入地面後，儲存在地面以下的水體，包含不飽和帶土壤水，和飽和帶地下水，後者為狹義[地下水](../Page/地下水.md "wikilink")。

### 海洋

地表上除陸地以外的廣大鹹水域。包括[太平洋](../Page/太平洋.md "wikilink")、[大西洋](../Page/大西洋.md "wikilink")、[印度洋](../Page/印度洋.md "wikilink")、[地中海](../Page/地中海.md "wikilink")、[北冰洋等](../Page/北冰洋.md "wikilink")，約占地表面積71%。

## 各種水體儲量表

<table>
<thead>
<tr class="header">
<th><p>水體種類</p></th>
<th><p>儲量（千立方公里）</p></th>
<th><p>佔總水體千分比</p></th>
<th><p>佔總淡水千分比</p></th>
<th><p>滯留時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>海水</p></td>
<td><div align="right">
<p>1,338,000</p>
</div></td>
<td><div align="right">
<p>965.3</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
<td><div align="right">
<p>2500年</p>
</div></td>
</tr>
<tr class="even">
<td><p>地下鹹水</p></td>
<td><div align="right">
<p>12,870</p>
</div></td>
<td><div align="right">
<p>9.3</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
<td><div align="right">
<p>深層1400年</p>
</div></td>
</tr>
<tr class="odd">
<td><p>地下淡水</p></td>
<td><div align="right">
<p>10,530</p>
</div></td>
<td><div align="right">
<p>7.6</p>
</div></td>
<td><div align="right">
<p>300.6</p>
</div></td>
<td><div align="right">
<p>深層1400年</p>
</div></td>
</tr>
<tr class="even">
<td><p>永凍土底水</p></td>
<td><div align="right">
<p>300</p>
</div></td>
<td><div align="right">
<p>0.22</p>
</div></td>
<td><div align="right">
<p>8.6</p>
</div></td>
<td><div align="right">
<p>10000年</p>
</div></td>
</tr>
<tr class="odd">
<td><p>土壤水</p></td>
<td><div align="right">
<p>17</p>
</div></td>
<td><div align="right">
<p>0.01</p>
</div></td>
<td><div align="right">
<p>0.5</p>
</div></td>
<td><div align="right">
<p>1年</p>
</div></td>
</tr>
<tr class="even">
<td><p>鹹水湖</p></td>
<td><div align="right">
<p>85</p>
</div></td>
<td><div align="right">
<p>0.06</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
<td><div align="right">
<p>17年</p>
</div></td>
</tr>
<tr class="odd">
<td><p>淡水湖</p></td>
<td><div align="right">
<p>91</p>
</div></td>
<td><div align="right">
<p>0.07</p>
</div></td>
<td><div align="right">
<p>2.6</p>
</div></td>
<td><div align="right">
<p>17年</p>
</div></td>
</tr>
<tr class="even">
<td><p>沼澤水</p></td>
<td><div align="right">
<p>12</p>
</div></td>
<td><div align="right">
<p>0.008</p>
</div></td>
<td><div align="right">
<p>0.3</p>
</div></td>
<td><div align="right">
<p>5年</p>
</div></td>
</tr>
<tr class="odd">
<td><p>河水</p></td>
<td><div align="right">
<p>2</p>
</div></td>
<td><div align="right">
<p>0.002</p>
</div></td>
<td><div align="right">
<p>0.06</p>
</div></td>
<td><div align="right">
<p>16天</p>
</div></td>
</tr>
<tr class="even">
<td><p>生物水</p></td>
<td><div align="right">
<p>1</p>
</div></td>
<td><div align="right">
<p>0.001</p>
</div></td>
<td><div align="right">
<p>0.03</p>
</div></td>
<td><div align="right">
<p>數小時</p>
</div></td>
</tr>
<tr class="odd">
<td><p>冰川</p></td>
<td><div align="right">
<p>24,064</p>
</div></td>
<td><div align="right">
<p>17.36</p>
</div></td>
<td><div align="right">
<p>686.97</p>
</div></td>
<td><div align="right">
<p>極地9700年<br />
高山1600年</p>
</div></td>
</tr>
<tr class="even">
<td><p>大氣水</p></td>
<td><div align="right">
<p>12</p>
</div></td>
<td><div align="right">
<p>0.01</p>
</div></td>
<td><div align="right">
<p>0.34</p>
</div></td>
<td><div align="right">
<p>8天</p>
</div></td>
</tr>
<tr class="odd">
<td><p><strong>水體總計</strong></p></td>
<td><div align="right">
<p>1,385,984</p>
</div></td>
<td><div align="right">
<p>1000</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
</tr>
<tr class="even">
<td><p><strong>淡水總計</strong></p></td>
<td><div align="right">
<p>35,029</p>
</div></td>
<td><div align="right">
<p>25.3</p>
</div></td>
<td><div align="right">
<p>1000</p>
</div></td>
<td><div align="right">
<p>——</p>
</div></td>
</tr>
</tbody>
</table>

## 分支学科

  - [河流水文学](../Page/河流水文学.md "wikilink")（也称“河川水文学”）
  - [湖泊水文学](../Page/湖泊水文学.md "wikilink")
  - [沼泽水文学](../Page/沼泽水文学.md "wikilink")
  - [冰川水文学](../Page/冰川水文学.md "wikilink")
  - [雪水文学](../Page/雪水文学.md "wikilink")
  - [水文气象学](../Page/水文气象学.md "wikilink")
  - [地下水水文学](../Page/地下水水文学.md "wikilink")
  - [区域水文学](../Page/区域水文学.md "wikilink")
  - [海洋水文学](../Page/海洋水文学.md "wikilink")
  - [农业水文学](../Page/农业水文学.md "wikilink")
  - [森林水文学](../Page/森林水文学.md "wikilink")
  - [都市水文学](../Page/都市水文学.md "wikilink")

## 参考文献

## 外部链接

  - [美国地质调查局水科学教育网](http://ga.water.usgs.gov/edu/languages/chinese/)
  - [日本水文科学会](http://wwwsoc.nii.ac.jp/jahs/)
  - [水文·水資源学会](https://web.archive.org/web/20080905154922/http://www.jshwr.org/modules/news/)

{{-}}

[Hydrology](../Category/地理学分支.md "wikilink")
[水文学](../Category/水文学.md "wikilink")