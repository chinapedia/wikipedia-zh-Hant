**水晶报表**（英文：**Crystal
Reports**），是一款[商务智能](../Page/商务智能.md "wikilink")（BI）软件，主要用于设计及生成报表。

## 源由

  - 产品的源由

Crystal Services Inc.一開始开发了水晶报表，后来由 Seagate Software 买下，并将Crystal
Services Inc.更名为 Crystal Decisions。

  - 2003-12 Business Objects 买下 Crystal Decisions 公司，当时的产品版本为 10；
  - 2007-10 SAP 买下 Business Objects 公司；
  - 2011-05 发布Crystal Reports 2011 (version 14)。

<!-- end list -->

  - 被Microsoft推广使用

<!-- end list -->

  - 1997年，由于被[Microsoft附加在](../Page/Microsoft.md "wikilink") [Visual
    Basic](../Page/Visual_Basic.md "wikilink") 5 里而被广泛使用；
  - 在[Visual Studio](../Page/Visual_Studio.md "wikilink") 6 更改为需另外安裝；
  - Visual Studio .NET 2002 再次搭配 Crystal Reports；
  - Visual Studio .NET 2003 仍搭配 Crystal Reports；
  - Visual Studio 2005 Pro板以上，提供「CRYSTAL REPORTS FOR MICROSOFT VISUAL
    STUDIO 2005」；
  - Visual Studio 2008 Pro板以上，提供「CRYSTAL REPORTS BASIC FOR MICROSOFT
    VISUAL STUDIO 2008」；
  - Visual Studio 2010 / 2012 / 2013 改为需从 Crystal Reports 官网下载安装。

## 支持的数据源

Crystal Reports可以存取的数据源包括有：

  - 储存在[資料庫系统內的数据](../Page/数据库.md "wikilink")，例如：[PostgreSQL](../Page/PostgreSQL.md "wikilink")、[Sybase](../Page/Adaptive_Server_Enterprise.md "wikilink")、[IBM
    DB2](../Page/IBM_DB2.md "wikilink")、[Ingres](../Page/Ingres.md "wikilink")、[Microsoft
    Access](../Page/Microsoft_Access.md "wikilink")、[Microsoft SQL
    Server](../Page/Microsoft_SQL_Server.md "wikilink")、[MySQL](../Page/MySQL.md "wikilink")、[Interbase及](../Page/Interbase.md "wikilink")[Oracle等](../Page/Oracle_database.md "wikilink")。\[1\]
  - [dBase](../Page/dBase.md "wikilink") dbf
  - [Btrieve](../Page/Btrieve.md "wikilink")
  - 储存在[試算表內的数据](../Page/試算表.md "wikilink")，例如：[Microsoft
    Excel](../Page/Microsoft_Excel.md "wikilink")
  - 文本文档
  - [XML文件](../Page/XML.md "wikilink")

## 参看

  - 某Crystal Reports持有的公司(2003-2007)
  - [SAP公司](../Page/SAP公司.md "wikilink")：现时持有Business Objects的公司

<!-- end list -->

  - 竞争对手：
      - [FineReport](../Page/FineReport.md "wikilink")
      - [Microsoft Access](../Page/Microsoft_Access.md "wikilink")
      - [Smart Query](../Page/Smart_Query.md "wikilink")

## 外部链接

  - [SAP
    SE](https://web.archive.org/web/20141115185108/http://www.sap.com/solution/sme/software/analytics/crystal-bi/index.html)

  - [博奥杰软件(上海)有限公司](https://web.archive.org/web/20080517011458/http://www.china.businessobjects.com/)

  - [Crystal Reports
    website](http://www.china.businessobjects.com/products/reporting/crystalreports/default.asp)

  - [Crystal Reports Forum](http://www.crystalreportsbook.com/forum/)

  - [Crystal
    Reports](http://www.taiwan.businessobjects.com/products/reporting/crystalreports/default.asp)

  - [SAP BusinessObjects
    線上商店台灣](http://store.businessobjects.com/store/bobjasia/zh_TW/DisplayProductDetailsPage/productID.100546100)

  - [SAP
    BusinessObjects网上商店中国](http://store.businessobjects.com/store/bobjasia/zh_CN/DisplayProductDetailsPage/productID.100546100)

## 参考资料

[FineReport和水晶報表的比較](http://www.finereport.com/tw/knowledge/acquire/crystalreport.html)

[Category:報告軟件](../Category/報告軟件.md "wikilink")

1.