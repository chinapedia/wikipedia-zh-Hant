**水楊酸**（，源于拉丁文的“杨柳”
*salix*），又名**柳酸**、**鄰羥基苯甲酸**、**2-羟基苯甲酸**。水楊酸易溶於[乙醇](../Page/乙醇.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")、[苯](../Page/苯.md "wikilink")、[丙酮](../Page/丙酮.md "wikilink")、[松節油](../Page/松節油.md "wikilink")，不易溶于水，20°C时[溶解度为每](../Page/溶解度.md "wikilink")100毫升0.2克\[1\]。存在於自然界的[柳樹皮](../Page/柳樹皮.md "wikilink")、[白珠樹葉及](../Page/白珠樹.md "wikilink")[甜樺中](../Page/甜樺.md "wikilink")。水杨酸是一种[有机酸](../Page/有机酸.md "wikilink")，可由[水杨苷代谢得到](../Page/水杨苷.md "wikilink")。它被广泛应用于有机合成中，也是一种[植物激素](../Page/植物激素.md "wikilink")。水杨酸具有与[阿司匹林](../Page/阿司匹林.md "wikilink")（乙酰水杨酸）相近的结构与药效，也可用于治疗[痤疮](../Page/痤疮.md "wikilink")\[2\]。

## 历史

公元前五世纪左右，[希腊医生](../Page/希腊.md "wikilink")[希波克拉底在记录中提到一种从柳树树皮中提取的苦味粉末可以用于止痛和退烧](../Page/希波克拉底.md "wikilink")。这一办法还见于[苏美尔](../Page/苏美尔.md "wikilink")、[黎巴嫩和](../Page/黎巴嫩.md "wikilink")[亚述古文献的记载](../Page/亚述.md "wikilink")。切诺基人与其他北美原住民有用树皮浸出液治疗发烧的传统\[3\]，他们使用植物的内侧树皮制成药物并用于镇痛。1763年，一名英国牧师爱德华（埃德蒙德）·斯通指出柳树树皮可有效退烧。\[4\]

1828年，法国药剂师亨利·勒鲁克斯与意大利化学家拉斐尔·皮里亚提取出了柳树皮中的有效成分，并以[白柳的拉丁文学名Salix](../Page/白柳.md "wikilink")
alba将其命名为水杨苷。皮里亚通过分解该物质得到了水杨酸。

1839年，德国研究人员还从[旋果蚊子草](../Page/旋果蚊子草.md "wikilink")（*[Filipendula
ulmaria](../Page/Filipendula_ulmaria.md "wikilink")*）中提取了水杨酸。虽然他们的提取物具有相同的疗效，但也对消化系统有副作用，如导致胃发炎、出血、腹泻，高剂量摄入时会导致死亡。

## 植物激素

水杨酸是一种酚类激素，可调节植物的生长发育，对植物的[光合作用](../Page/光合作用.md "wikilink")、[蒸腾作用与](../Page/蒸腾作用.md "wikilink")[离子的吸收与运输也有调节作用](../Page/离子.md "wikilink")。水杨酸同时也可以诱导植物细胞的[分化与](../Page/分化.md "wikilink")[叶绿体的生成](../Page/叶绿体.md "wikilink")。水杨酸还作为内生信号参与植物对[病原体的抵御](../Page/病原体.md "wikilink")\[5\]，通过诱导组织产生病程相关蛋白，当植物的一部分受到病原体感染时在其他部分产生抗性。通过形成挥发性的水杨酸甲酯，这一信号还可在不同植物间传递。\[6\]

## 药用

水杨酸在古代已经被用于缓解疼痛和发热、同时还有消炎作用\[7\]。

现代医学中，水杨酸甲酯也被用于缓解关节和肌肉疼痛；水杨酸胆碱广泛用于治疗口腔溃疡。

[Salicylic_acid_pads.jpg](https://zh.wikipedia.org/wiki/File:Salicylic_acid_pads.jpg "fig:Salicylic_acid_pads.jpg")
pads soaked in salicylic acid can be used to chemically
[exfoliate](../Page/Exfoliation_\(cosmetology\).md "wikilink") skin\]\]

与果酸（β-羟基酸）类似，水杨酸是很多护肤品中的关键去[角質成分](../Page/角質.md "wikilink")，用于藥品時，可治疗[痤疮](../Page/痤疮.md "wikilink")、[脂溢性皮炎](../Page/脂溢性皮炎.md "wikilink")、[银屑病](../Page/银屑病.md "wikilink")、[鸡眼](../Page/鸡眼.md "wikilink")、[毛囊角化症](../Page/毛囊角化症.md "wikilink")。\[8\]
用于脱去肥厚的[茧时](../Page/茧.md "wikilink")，其典型的浓度是6%的阿司匹林-凡士林膏，涂抹在该部位1小时，再洗去即可。

水楊酸外用對[微生物有抗菌性](../Page/微生物.md "wikilink")，其防腐力近于[酚](../Page/酚.md "wikilink")。水楊酸可使角質溶解，其製劑濃度不同而藥理作用各異：

  - 1％-3％有角化促成和止癢作用；常見非處方(OTC)可購的水楊酸洗髮水含水楊酸3%
  - 5％-10％具有角質溶解作用，可使角質層中連接鱗細胞間粘合質溶解，從而使角質鬆開而脫屑，亦可產生抗真菌作用（因去除角質層後並抑制真菌生長，水楊酸能幫助其他抗真菌藥物的穿透，並抑制細菌生長）。
  - 25％[濃度具有腐蝕作用](../Page/濃度.md "wikilink")，可脫除肥厚的胼胝。適用於皮脂溢出，[脂溢性皮炎](../Page/脂溢性皮炎.md "wikilink")，淺部真菌病，疣，雞眼，胼胝及局部[角質增生](../Page/角質增生.md "wikilink")。

## 其他用途

  - 虽然大剂量的水杨酸对人体有害，但水杨酸常用作[食品中的防腐剂和牙膏](../Page/防腐剂.md "wikilink")[抗菌剂](../Page/抗菌剂.md "wikilink")。对某些对水杨酸过敏的人而言，即使小剂量接触可能也是有害的。
  - 水杨酸钠是一种常用的[真空紫外](../Page/真空紫外.md "wikilink")[荧光粉](../Page/荧光粉.md "wikilink")，可发出波长420nm的荧光。\[9\]
  - 水杨酸甲酯搽剂可缓解关节和肌肉疼痛。

[次水杨酸铋是一种常见的胃药](../Page/次水杨酸铋.md "wikilink")，用于治疗[腹泻](../Page/腹泻.md "wikilink")、恶心、[胃灼热和胃气胀](../Page/胃灼热.md "wikilink")。也是一种温和的抗生素。

胆碱水杨酸局部使用，可以治疗口腔溃疡。

## 安全性

水楊酸可引起接觸性皮膚炎。大面積使用吸收後，可出現水楊酸全身[中毒症狀](../Page/中毒.md "wikilink")，如[頭暈](../Page/頭暈.md "wikilink")，神志模糊，精神錯亂，呼吸急促，持續性耳鳴，劇烈或持續[頭痛](../Page/頭痛.md "wikilink")，刺痛。

有[糖尿病](../Page/糖尿病.md "wikilink")，[四肢周圍](../Page/四肢.md "wikilink")[血管疾病者](../Page/血管疾病.md "wikilink")，或嬰幼兒，使用水楊酸25％-60％乳膏或軟膏、水楊酸15％-50％硬膏、水楊酸17％凝膠、水楊酸13.6％-26％溶液等應慎重考慮，有可能引起念珠性炎症或[潰瘍](../Page/潰瘍.md "wikilink")，甚至致死；對[皮炎或皮膚感染使用水楊酸](../Page/皮炎.md "wikilink")25％-60％乳膏或軟膏、水楊酸40％-50％硬膏，亦需注意。

水杨酸可以抑制外毛细胞运动蛋白（Prestin）的活性，因而具有耳毒性。\[10\]对于缺锌的患者可能导致暂时性失聪。这一发现基于对白鼠的临床试验。给缺锌大白鼠注射水杨酸会导致失聪，而同时注射锌溶液可治愈失聪。给缺锌大白鼠注射镁溶液没有治愈水杨酸性失聪。

没有专门的对水杨酸对妊娠造成的影响的研究。妊娠的前三个月口服水杨酸（或阿司匹林）不会增加胎儿畸形的概率，但在妊娠晚期服用可能导致胎儿颅内出血\[11\]。即使在妊娠后期外用水杨酸也不会造成不良影响。许多外用皮肤病药物含有水杨酸，没有报告表明外用水杨酸有致畸风险。\[12\]

过量的水杨酸可导致水杨酸中毒，临床表现为代谢性酸中毒和呼吸性碱中毒。急性患者的发病率为16%，死亡率为1%。\[13\]

有些人对水杨酸及其衍生物过敏。

美国食品和药品管理局建议在使用含有水杨酸（或任何其他抗氧化剂）的护肤品时配合使用防晒护肤产品。\[14\]

虽然没有数据表明使用水杨酸会导致[雷尔氏综合症](../Page/雷尔氏综合症.md "wikilink")，美国国家雷尔氏综合征基金会建议儿童和青少年（年齡16歲以下）尽量减少服用水杨酸与阿司匹林。\[15\]

## 合成

[Thomé_Salix_alba_clean.jpg](https://zh.wikipedia.org/wiki/File:Thomé_Salix_alba_clean.jpg "fig:Thomé_Salix_alba_clean.jpg")

生物体中水杨酸可由[苯丙氨酸合成得到](../Page/苯丙氨酸.md "wikilink")。

水杨酸的工业生产是通过[柯尔伯-施密特反应用](../Page/柯尔伯-施密特反应.md "wikilink")[苯酚与](../Page/苯酚.md "wikilink")[二氧化碳在高温](../Page/二氧化碳.md "wikilink")（390K）高压（100大气压）下合成水杨酸的钠盐，再通过[硫酸酸化得到水杨酸](../Page/硫酸.md "wikilink")。

  -
    [Kolbe-Schmitt.png](https://zh.wikipedia.org/wiki/File:Kolbe-Schmitt.png "fig:Kolbe-Schmitt.png")

水杨酸也可通过水解[阿司匹林](../Page/阿司匹林.md "wikilink")（[乙酰水杨酸](../Page/乙酰水杨酸.md "wikilink")）或[水杨酸甲酯得到](../Page/水杨酸甲酯.md "wikilink")。\[16\]

## 参见

  - [乙酰水杨酸](../Page/乙酰水杨酸.md "wikilink")

## 外部链接

  - [MSDS安全数据](http://msds.chem.ox.ac.uk/SA/salicylic_acid.html)链接失效

## 参考资料

[Category:非甾体抗炎药](../Category/非甾体抗炎药.md "wikilink")
[Category:防腐剂](../Category/防腐剂.md "wikilink")
[水杨酸](../Category/水杨酸.md "wikilink")
[Category:植物激素](../Category/植物激素.md "wikilink")
[Category:单羟基苯甲酸](../Category/单羟基苯甲酸.md "wikilink")
[Category:痤疮治疗](../Category/痤疮治疗.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.
2.
3.  Paul B. Hemel and Mary U. Chiltoskey, *Cherokee Plants and Their
    Uses -- A 400 Year History,* Sylva, NC: Herald Publishing Co.
    (1975); cited in Dan Moerman, A Database of Foods, Drugs, Dyes and
    Fibers of Native American Peoples, Derived from
    Plants.[1](http://herb.umd.umich.edu/) A search of this database for
    "salix AND medicine" finds 63 entries.
4.
5.
6.  p.306 *Plant Physiology Third Edition* Taiz and Zeiger 2002
7.
8.
9.  JAR Samson *Techniques of Vacuum Ultraviolet Spectroscopy*
10.
11. Rumack et al., 1981
12. [Acne and Pregnancy](http://www.fetal-exposure.org/ACNE.html)
13. [Toxicity, Salicylate: eMedicine Emergency
    Medicine](http://emedicine.medscape.com/article/818242-overview)
14.
15.
16.