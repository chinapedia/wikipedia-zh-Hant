**水橋香織**（，）是一名[日本女性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")。隸屬於[ARTSVISION](../Page/ARTSVISION.md "wikilink")。出生於[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")。[身高](../Page/身高.md "wikilink")150cm，[血型是O型](../Page/血型.md "wikilink")，[星座為](../Page/星座.md "wikilink")[處女座](../Page/處女座.md "wikilink")，有一個弟弟。

## 經歷

  - 1974年8月28日出生於[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")
  - 從小開始就很喜歡朗讀書籍，特別是台詞的部分。在國文課上念課文中的台詞時會唸的特別認真\[1\]。在中學時得知有[聲優這個職業](../Page/聲優.md "wikilink")，從此便以成為[聲優為志願](../Page/聲優.md "wikilink")。
  - 1992年8月，在アニメディア・アニメV主辦的以聲優、動畫歌手志願者為主的比賽「DRACON'92」中獲得了準優勝獎。
  - 1993年3月，在出身地的高中畢業後便獨自一人前往東京。一邊做著類似於在遊戲中心中的廣播員的工作，一邊經過了俳協ボイスアクターズスタジオ、日本ナレーション演技研究所等養成所的培訓後，加入了事務所[ARTSVISION](../Page/ARTSVISION.md "wikilink")。
  - 出道作品為1996年3月25日發售的DramaCD「ワルキューレの伝説 外伝
    ローザの冒険」。這時還是[ARTSVISION的新人女性聲優組合](../Page/ARTSVISION.md "wikilink")「フェアリーズ」的其中一人。而在同年PC-FX遊戲「ファイアーウーマン纏組」的公開試鏡中落選。從出道開始有一段時間一直是處於角色時有時無的狀況\[2\]。
  - 在2000年10月的[機巧奇傳ヒヲウ戦記中終於獲得了動畫中首次演出的固有角色](../Page/機巧奇傳ヒヲウ戦記.md "wikilink")「マチ」、在次年NHK動畫的[カスミン中則獲得了第一次的主角](../Page/カスミン.md "wikilink")「春野カスミ」，而後開始獲得知名度。

## 人物

  - 從進入2000年後開始就沒在看電視。無法確切得知是從何時開始的，據猜測大約是在2000～2005年之間。情報源多來自於電車上。因此對於正在流行的話題、發生的事情都不太清楚。在聽到[後藤邑子愚人節時說的](../Page/後藤邑子.md "wikilink")｢2008年的[奧林匹克好像要停止舉行](../Page/奧林匹克.md "wikilink")｣的謊話後，並不是驚訝於｢要中止嗎｣而是驚訝於｢今年已經是[奧林匹克了啊](../Page/奧林匹克.md "wikilink")｣\[3\]。
  - 基本上被叫什麼稱呼都不會有太大的反感，但只有對於被叫「かおりちゃん」有著極大的厭惡感。不過也是有例外。例如在[女友伴身邊的廣播節目中曾被](../Page/女友伴身邊.md "wikilink")[丹下櫻多次的稱呼](../Page/丹下櫻.md "wikilink")「かおりちゃん」卻完全沒表現出厭惡的感覺\[4\]。而其原因則是因為本人是[丹下櫻的粉絲](../Page/丹下櫻.md "wikilink")。在[女友伴身邊的廣播節目中](../Page/女友伴身邊.md "wikilink")，只要有[丹下櫻當來賓就會變的十分沉默](../Page/丹下櫻.md "wikilink")（為了能好好享受[丹下櫻的聲音](../Page/丹下櫻.md "wikilink")）、甚至還會做出發出奇怪的笑聲、要求[丹下櫻做兩次自我介紹等奇特的舉動](../Page/丹下櫻.md "wikilink")，粉絲們通常稱呼這種狀態的水橋為「デレハス」\[5\]
  - 有收集[人偶的興趣](../Page/人偶.md "wikilink")，收集的多為女孩子的[人偶](../Page/人偶.md "wikilink")，就本人所說已經超過100個以上了。
  - 很在意自己的身高。多次在[女友伴身邊的廣播節目中提起身高的話題時說過](../Page/女友伴身邊.md "wikilink")「在我想像中的自己是個180公分的帥哥」而被吐槽「那是完全不同的生物吧」，在問到「所以水橋的身高是多少」的時候則迅速的回答了「我不想說」。
  - 給水橋取「ミズハス」這個暱稱的始祖是[阿澄佳奈](../Page/阿澄佳奈.md "wikilink")\[6\]。
  - 極其容易害羞。在[君吻的廣播節目中被](../Page/君吻.md "wikilink")[廣橋涼說](../Page/廣橋涼.md "wikilink")「我最近發現啊，每次一放這個BGM時，水橋就會跟著搖動身體呢」而害羞的發怒大喊「可惡的廣橋！」並提起過，自己有在玩電視遊戲時被父親指出「你是會跟著遊戲搖的類型啊」而和父親吵起來的事情過。
  - 家中成員有雙親和一個弟弟，且有養一隻灰白色的貓。不過在[江ノ島ベイビィ的廣播節目中被問起貓的名字時則以](../Page/江ノ島ベイビィ.md "wikilink")「可能會被當成捏他叫」為理由而拒絕回答。
  - [阿澄佳奈在](../Page/阿澄佳奈.md "wikilink")[向陽素描的廣播節目中](../Page/向陽素描.md "wikilink")，被[チョー問起](../Page/チョー.md "wikilink")「對水橋的印象是什麼」時回答了「ミズハス在我的心中是個萌系角色」
  - 十分受[日本一ソフトウェア的重用](../Page/日本一ソフトウェア.md "wikilink")。在[魔界戰記系列遊戲中最多演出了](../Page/魔界戰記.md "wikilink")6人的固有角色以及其他路人角色，其他相關作品也幾乎都出演了主要角色，甚至在[超次元戰記
    戰機少女中飾演了](../Page/超次元戰記_戰機少女.md "wikilink")[日本一ソフトウェア的擬人化角色](../Page/日本一ソフトウェア.md "wikilink")[日本一ちゃん](../Page/日本一ちゃん.md "wikilink")。
  - 屬於對演出角色及作品深入考察理解的類型。因其出色的演技及多彩的聲線而被許多人稱為「困ったときの水橋」\[7\]
  - 十分擅長繪畫。因為中學時沒有漫畫研究社類型的社團，為了想畫插畫而參加了美術部\[8\]，也在[君吻的廣播節目中提到](../Page/君吻.md "wikilink")，自己曾在上課時偷畫過[三國志的漫畫](../Page/三國志.md "wikilink")\[9\]。更在[現視研的特裝版第九卷漫畫中挑戰了畫附錄漫畫](../Page/現視研.md "wikilink")。
  - 在[カスミン](../Page/カスミン.md "wikilink")、[向陽素描中有即興作曲的經驗](../Page/向陽素描.md "wikilink")。
  - 喜歡特攝。有教過[阿澄佳奈](../Page/阿澄佳奈.md "wikilink")[太陽戰隊太陽火神的決定姿勢](../Page/太陽戰隊太陽火神.md "wikilink")。
  - 和父親長的很像。在『君のぞラジオPresents アカネマニアックスアニメ化記念ツアー
    次女の奇妙な冒険』中的工作人員曾提到過有「看到父親時還以為是水橋小姐就這麼跑過去直接搭話了」的回憶。\[10\]
  - 有寫過[君吻廣播劇的腳本的經驗](../Page/君吻.md "wikilink")。\[11\]

## 主要參與作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

**1999年**

  - [伊甸少年](../Page/伊甸少年.md "wikilink")（）
  - [魔術師歐菲Revenge](../Page/魔術師歐菲.md "wikilink")（少女）

**2000年**

  - [機巧奇傳](../Page/機巧奇傳.md "wikilink")（**瑪吉**）

**2001年**

  - [妙妙魔法屋](../Page/妙妙魔法屋.md "wikilink")（**春野霞**）
  - [地球防衛家族](../Page/地球防衛家族.md "wikilink")（嬰兒 其他）
  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")（胡椒）
  - [厄夜怪客](../Page/厄夜怪客.md "wikilink")（伊特古拉（少女時代））
  - [魔法少女貓](../Page/魔法少女貓.md "wikilink")（）

**2002年**

  - [青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")（**水無月妙子**）
  - [犬夜叉](../Page/犬夜叉.md "wikilink")（紫織）
  - [銀河戰警](../Page/銀河戰警.md "wikilink")（）
  - [The Big O](../Page/The_Big_O.md "wikilink")（）
  - [G-on少女騎士團](../Page/G-on少女騎士團.md "wikilink")（）
  - [驚爆危機](../Page/驚爆危機.md "wikilink")（黃楊圓）
  - [洛克人exe](../Page/洛克人exe系列.md "wikilink")（**櫻井美露**）

**2003年**

  - [青出於藍 ～緣～](../Page/青出於藍_\(動畫\).md "wikilink")（**水無月妙子**）
  - [萬花筒之星](../Page/萬花筒之星.md "wikilink")（**羅賽塔．帕賽爾**）
  - [廢棄公主](../Page/廢棄公主.md "wikilink")（賽菲莉絲）
  - [哆啦A夢](../Page/哆啦A夢_\(電視動畫\).md "wikilink")（少女）
  - [魔法咪路咪路](../Page/魔法咪路咪路.md "wikilink")（若夢）
  - [洛克人exe AXESS](../Page/洛克人exe系列.md "wikilink")（**櫻井美露**）

**2004年**

  - [微笑的閃士](../Page/微笑的閃士.md "wikilink")（鐵破少年）
  - [雙戀](../Page/雙戀.md "wikilink")（白鐘沙羅）
  - [魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")（**尤諾·斯克萊亞**）
  - [洛克人exe Stream](../Page/洛克人exe系列.md "wikilink")（**櫻井美露**）

**2005年**

  - [ARIA The ANIMATION](../Page/ARIA.md "wikilink")（**小愛**、姬·M·葛蘭基斯塔）
  - [英國戀物語\~艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")（薇薇安·瓊斯）
  - [不可思議星球的雙胞胎公主](../Page/不可思議星球的雙胞胎公主.md "wikilink")（阿爾泰莎）
  - [雙戀Alternative](../Page/雙戀.md "wikilink")（**白鐘沙羅**）
  - [魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")（**尤諾·斯克萊亞**）
  - [洛克人exe BEAST](../Page/洛克人exe系列.md "wikilink")（**櫻井美露**）

**2006年**

  - [ARIA The NATURAL](../Page/ARIA.md "wikilink")（**小愛**、姬·M·葛蘭基斯塔）
  - [傳頌之物](../Page/傳頌之物.md "wikilink")（紗雅）
  - [金色琴弦〜primo passo〜](../Page/金色琴弦#TV動畫.md "wikilink")（**利利**）
  - [我的裘可妹妹](../Page/我的裘可妹妹.md "wikilink")（**芹川千歲**）
  - [不可思議星球的雙胞胎公主Gyu\!](../Page/不可思議星球的雙胞胎公主.md "wikilink")（阿爾泰莎）
  - [魔界戰記](../Page/魔界戰記.md "wikilink")（**拉哈爾**）
  - [洛克人exe BEAST+](../Page/洛克人exe系列.md "wikilink")（**櫻井美露**）

**2007年**

  - [英國戀物語\~艾瑪 第二幕](../Page/艾瑪_\(漫畫\).md "wikilink")（薇薇安·瓊斯）
  - [君吻 pure rouge](../Page/君吻#動畫.md "wikilink")（里仲成美）
  - [現視研2](../Page/現視研#電視動畫.md "wikilink")（**荻上千佳**）
  - [初音島II](../Page/初音島II.md "wikilink")（澤井麻耶、DJ）
  - [向陽素描](../Page/向陽素描.md "wikilink")（**宮子**）
  - [PRISM ARK](../Page/PRISM_ARK.md "wikilink")（**妃露**、）
  - [魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")（**薇薇歐**、薩依、尤諾·斯克萊亞）

**2008年**

  - [ARIA The ORIGINATION](../Page/ARIA.md "wikilink")（**小愛**、姬·M·葛蘭基斯塔）
  - [初音島II第二季](../Page/初音島II第二季.md "wikilink")（澤井麻耶、DJ）
  - [出包王女](../Page/出包王女.md "wikilink")（藤崎綾）
  - [向陽素描×365](../Page/向陽素描.md "wikilink")（**宮子**）
  - [我家有個狐仙大人](../Page/我家有個狐仙大人.md "wikilink")（六瓢、鴫守）

**2009年**

  - [金色琴弦〜secondo passo〜](../Page/金色琴弦#電視動畫.md "wikilink")（**利利**）
  - [女王之刃 流浪的戰士](../Page/女王之刃.md "wikilink")（**艾莉娜**）
  - [遊魂 -Kiss on my
    Deity-](../Page/遊魂_-Kiss_on_my_Deity-.md "wikilink")（**小鳥遊由美奈**）
  - [11eyes](../Page/11eyes.md "wikilink")（奈月香央里）
  - [KIDDY GiRL-AND](../Page/KIDDY_GiRL-AND.md "wikilink")（）

**2010年**

  - [祝福之鐘](../Page/祝福之鐘.md "wikilink")（**阿妮艾斯·布蘭潔**）
  - [真・戀姬†無雙〜少女大亂〜](../Page/戀姬†無雙.md "wikilink")（[呂蒙](../Page/呂蒙.md "wikilink")）
  - [強襲魔女2](../Page/強襲魔女.md "wikilink")（瑪蒂娜·克雷斯玻）
  - [傳說的勇者的傳說](../Page/傳說的勇者的傳說.md "wikilink")（）
  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")（**島田美波**）
  - [向陽素描×☆☆☆](../Page/向陽素描.md "wikilink")（**宮子**）
  - [向陽素描×☆☆☆ 特別編](../Page/向陽素描.md "wikilink")（**宮子**）
  - [變身！公主偶像](../Page/變身！公主偶像.md "wikilink")（夏目真晝）
  - [FORTUNE ARTERIAL
    -紅色約定-](../Page/FORTUNE_ARTERIAL.md "wikilink")（千堂伽耶）
  - [出包王女 第二季](../Page/出包王女.md "wikilink")（藤崎綾）

**2011年**

  - [日常](../Page/日常.md "wikilink")（天使）
  - [眾神中的貓神](../Page/眾神中的貓神.md "wikilink")（賀茂雪那）
  - [笨蛋，測驗，召喚獸 第二季](../Page/笨蛋，測驗，召喚獸.md "wikilink")（**島田美波**）
  - [請認真的和我戀愛！！](../Page/請認真的和我戀愛！！.md "wikilink")（不死川心\[12\]）
  - [魔法少女小圓☆魔力](../Page/魔法少女小圓☆魔力.md "wikilink")（**巴麻美**、鹿目達也、魔女之夜）
  - 47都道府犬（北海道犬\[13\]）

**2012年**

  - [戀愛與選舉與巧克力](../Page/戀愛與選舉與巧克力.md "wikilink")（**木場美冬**）
  - [加速世界](../Page/加速世界.md "wikilink")（Purple Thorn）
  - [武裝神姬](../Page/武裝神姬.md "wikilink")（**愛涅斯**）
  - [向陽素描×蜂窩](../Page/向陽素描.md "wikilink")（**宮子**）
  - [出包王女DARKNESS](../Page/出包王女.md "wikilink")（藤崎綾）

**2013年**

  - [閃亂神樂](../Page/閃亂神樂_-少女們的真影-.md "wikilink")（**柳生**）
  - [戀曲寫真](../Page/戀曲寫真.md "wikilink")（**實原冰里**）
  - [人魚又上鉤](../Page/人魚又上鉤.md "wikilink")（E.C.O. 人魚）
  - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")（忍野扇）
  - [魔鬼戀人](../Page/魔鬼戀人.md "wikilink")（奏人〈幼少期〉）
  - [Walkure Romanze
    少女騎士物語](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（柊木綾子）

**2014年**

  - [大家集合！Falcom 學園](../Page/大家集合！Falcom_學園.md "wikilink")（**緹歐**）
  - [咲-Saki- 全國篇](../Page/咲-Saki-.md "wikilink")（瀧見春）
  - [天雷爭霸：復仇者聯盟](../Page/天雷爭霸：復仇者聯盟.md "wikilink")（**蜂女**）
  - [金色琴弦 Blue♪Sky](../Page/金色琴弦.md "wikilink")（**水嶋悠人**）
  - [花物語](../Page/花物語.md "wikilink")（忍野扇）
  - [灰色的果實](../Page/灰色的果實.md "wikilink")（**松嶋滿**）
  - [女友伴身邊](../Page/女友伴身邊.md "wikilink")（姬島木乃子）
  - [憑物語](../Page/憑物語.md "wikilink")（忍野扇）

**2015年**

  - 大家集合！Falcom 學園SC（**緹歐**）
  - [魔法少女奈葉ViVid](../Page/魔法少女奈葉ViVid.md "wikilink")（**高町薇薇歐**、薩依）
  - [灰色的迷宮](../Page/灰色的迷宮.md "wikilink")（**松嶋滿**）
  - [灰色的樂園](../Page/灰色的樂園.md "wikilink")（**松嶋滿**）
  - [出包王女DARKNESS 2nd](../Page/出包王女.md "wikilink")（藤崎綾）
  - [終物語](../Page/終物語.md "wikilink")（**忍野扇**）

**2016年**

  - [蒼之彼方的四重奏](../Page/蒼之彼方的四重奏.md "wikilink")（伊莉娜·艾瓦隆）
  - [少年女僕](../Page/少年女僕.md "wikilink")（日野美枝）
  - [ViVid Strike\!](../Page/ViVid_Strike!.md "wikilink")（**高町薇薇歐**、薩依）

**2017年**

  - [終物語 (下)](../Page/終物語.md "wikilink")（**忍野扇**）
  - [如果有妹妹就好了。](../Page/如果有妹妹就好了。.md "wikilink")（春斗的妹妹）

**2018年**

  - [搖曳露營△](../Page/搖曳露營△.md "wikilink")（志摩咲）
  - [伊藤潤二驚選集](../Page/伊藤潤二驚選集.md "wikilink")（彩子）
  - [妖怪旅館營業中](../Page/妖怪旅館營業中.md "wikilink")（律子）
  - 閃亂神樂 SHINOVI MASTER -東京妖魔篇-（**柳生**\[14\]）

**2019年**

  - [環戰公主](../Page/環戰公主.md "wikilink")（**相澤步**\[15\]）

### OVA

  - （**涼宮茜**）

  - [星空防衛隊 Advanced](../Page/星空防衛隊.md "wikilink")（青木霧子）

  - （**織倉真奈美**）

  - [厄夜怪客](../Page/厄夜怪客.md "wikilink")（（少女時代））

  -
  - [現視研](../Page/現視研.md "wikilink")（**荻上千佳**）

  - [ARIA The OVA ～ARIETTA～](../Page/水星領航員.md "wikilink")（小愛）

  - [傳頌之物\~逝去者的搖籃曲](../Page/傳頌之物.md "wikilink")（紗雅（））

  - [Carnival Phantasm](../Page/Carnival_Phantasm.md "wikilink")（蓮/白蓮）

  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")（島田美波）

  - [我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")（小宮山琴美）※漫畫第7卷附贈DVD初回限定特裝版

  - [ViVid Strike\! OVA](../Page/ViVid_Strike!.md "wikilink")（**高町薇薇欧**）

### 劇場版動畫

  - 2010年

<!-- end list -->

  - [魔法少女奈叶 The MOVIE
    1st](../Page/魔法少女奈叶_The_MOVIE_1st.md "wikilink")（**[尤诺·斯克莱亚](../Page/尤诺·斯克莱亚.md "wikilink")**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [魔法少女奈叶 The MOVIE 2nd
    A's](../Page/魔法少女奈叶_The_MOVIE_2nd_A's.md "wikilink")（尤诺·斯克莱亚）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [魔法少女小圓 ［新篇］叛逆的物語](../Page/魔法少女小圓_［新篇］叛逆的物語.md "wikilink")（巴麻美）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [加速世界 INFINITE∞BURST](../Page/加速世界.md "wikilink")（Purple Thorn）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [魔法少女奈叶Reflection](../Page/魔法少女奈叶Reflection.md "wikilink")（尤诺·斯克莱亚）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [續終物語](../Page/續終物語.md "wikilink")（忍野扇）

### 遊戲

  - [英雄傳說 軌跡系列](../Page/英雄傳說_軌跡系列.md "wikilink")（**緹歐·普拉托**）

      - \[PSP\][英雄傳說 零之軌跡](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PSP\][英雄傳說 碧之軌跡](../Page/英雄傳說_碧之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 零之軌跡
        Evolution](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 碧之軌跡
        Evolution](../Page/英雄傳說_碧之軌跡.md "wikilink")
      - \[PS4\][英雄傳說 閃之軌跡III](../Page/英雄傳說_閃之軌跡III.md "wikilink")

  - [那由多之軌跡](../Page/那由多之軌跡.md "wikilink")（**那由多·赫歇爾**）

  - [純愛手札4](../Page/純愛手札.md "wikilink")（七河瑠依）

  - [青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")（水無月妙子）

  - [We Are\*](../Page/We_Are*.md "wikilink")（神明菊菜）

  - [問答魔法學院](../Page/問答魔法學院.md "wikilink")（**瑪蓉**）

  - [金色琴弦](../Page/金色琴弦.md "wikilink")（**利利**）

  - [薩爾達傳說 時之笛](../Page/薩爾達傳說_時之笛.md "wikilink") (）

  -
  - [DUEL SAVIOR DESTINY](../Page/DUEL_SAVIOR.md "wikilink")（古妮亞）

  -
  - （**織倉真奈美**）

  - （**織倉真奈美**）

  - [夢幻奇緣2☆☆☆](../Page/夢幻奇緣2.md "wikilink")（**瑪琳·史都華特**）

  -
  -
  - （白鐘沙羅）

  - （**白鐘沙羅**）

  - [受讚頌者 給逝者的搖籃曲](../Page/受讚頌者.md "wikilink")（サクヤ）

  - [Muv-Luv](../Page/Muv-Luv.md "wikilink")（涼宮茜）

  - [Muv-Luv
    Alternative](../Page/Muv-Luv_Alternative.md "wikilink")（涼宮茜）

  - [初音島II Plus
    Situation](../Page/初音島II_Plus_Situation.md "wikilink")（**沢井麻耶**、沢井勇斗、エルニーニョ絵梨奈）

  - [PRISM ARK -AWAKE-](../Page/PRISM_ARK.md "wikilink")（フェル）

  - [11eyes CrossOver](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")（奈月香央里）

  - [桃華月憚 -光風の陵王-](../Page/桃華月憚.md "wikilink")（犬飼真琴）

  - [祝福的鐘聲 Portable](../Page/祝福的鐘聲.md "wikilink")（**アニエス・ブーランジュ**）

  - [少女愛上大姊姊](../Page/處女愛上姊姊.md "wikilink")（アニエス・ブーランジュ）

  - [認真和我談戀愛！R](../Page/認真和我談戀愛！.md "wikilink")（不死川心）

  - [魔界戰記](../Page/魔界戰記.md "wikilink")（**ラハール**）

  - [魔界戰記2](../Page/魔界戰記2.md "wikilink")（）

  - [魔界戰記3](../Page/魔界戰記3.md "wikilink")（ラハール、ハナコ、マローネ、バール、プラム、プレネールさん）

  - [魔界戰記4](../Page/魔界戰記4.md "wikilink")（ラハール、プレネールさん、プラム、日本一ちゃん、ペタ、マローネ）

  - [魔界戰記5](../Page/魔界戰記5.md "wikilink")（ラハール /
    ラハールちゃん、プレネールさん、日本一ちゃん、プラム、ペタ）

  - [Melty Blood](../Page/Melty_Blood.md "wikilink")（蓮/白蓮、瀬尾晶）

  -
  - 小魔女帕妃系列（

  - [君吻](../Page/君吻.md "wikilink")（**里仲成美**、女子A）

  - [真·戀姬†無雙](../Page/真·戀姬†無雙.md "wikilink")（呂蒙（真名：亞紗））

  - （公由夏美）

  - [超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")（日本一）

  - [美夢俱樂部](../Page/美夢俱樂部.md "wikilink")（Dream C Club）（小雪）

  - [戀愛與選舉與巧克力 PORTABLE](../Page/戀愛與選舉與巧克力.md "wikilink")（**木場美冬**）

  - [這間教室被不回家社占領了](../Page/這間教室被不回家社占領了.md "wikilink")（**秋月琴音**）

  - [灰色的果實 -LE FRUIT DE LA
    GRISAIA-](../Page/灰色的果實.md "wikilink")（**松嶋滿**）

  - [魔女與百騎兵](../Page/魔女與百騎兵.md "wikilink")（**涅札莉亞**）

  - [魔法少女奈叶 A's
    携带版：命运齿轮](../Page/魔法少女奈葉A's.md "wikilink")（尤诺·斯克莱亚、高町薇薇欧）

  - [女友伴身邊](../Page/女友伴身邊.md "wikilink")（姬島木乃子）

  - [間接之戀V](../Page/間接之戀.md "wikilink")（**園原碧里**）

  - [蒼之彼方的四重奏](../Page/蒼之彼方的四重奏.md "wikilink")（伊莉娜·艾瓦隆）

  - [Harmonia](../Page/Harmonia_\(游戏\).md "wikilink")（**汐奈**）

  - [聖火降魔錄 英雄](../Page/火焰之纹章_英雄.md "wikilink")（エイリーク）

  - [閃耀幻想曲](../Page/閃耀幻想曲.md "wikilink")（宮子）

  - [遊魂 -Kiss on my
    Deity-](../Page/遊魂_-Kiss_on_my_Deity-.md "wikilink")（**小鳥遊由美奈**）

  - [遊魂2 -you're the only
    one-](../Page/遊魂2_-you're_the_only_one-.md "wikilink")（小鳥遊由美奈）

  - [御靈錄Otogi](../Page/御靈錄Otogi.md "wikilink")（神便鬼毒酒）

  - [Crash Fever台港澳版本](../Page/Crash_Fever.md "wikilink")(文財神 比干)

### Drama CD

  - [ARIA](../Page/ARIA.md "wikilink")（**水無燈里**）

  - [久遠之絆平安編](../Page/久遠之絆.md "wikilink")（賀茂桐子）

  -
  - [夢幻奇緣2系列](../Page/夢幻奇緣2.md "wikilink")（**瑪琳·史都華特**）

      - Marine Style
      - Aoi Romance
      - Aqua Balance

  - [あの娘にキスと白百合を](../Page/あの娘にキスと白百合を.md "wikilink")（**黒沢ゆりね**）

  - [ヤンデレの女の子に死ぬほど愛されて眠れないCDぎゃーーーっ！](../Page/ヤンデレの女の子に死ぬほど愛されて眠れないCDぎゃーーーっ！.md "wikilink")（**綾小路咲夜**）

  - [ヤンデレ惨～ヤンデレの女の子に死ぬほど愛されて眠れないCD3～](../Page/ヤンデレ惨～ヤンデレの女の子に死ぬほど愛されて眠れないCD3～.md "wikilink")（**櫻之宮亞梨主**）

  - [魔法少女奈叶 Sound
    Stage系列](../Page/魔法少女奈叶_Sound_Stage.md "wikilink")（尤诺·斯克莱亚、高町薇薇欧、萨依）

      - 魔法少女奈叶 Sound Stage 1-4
      - 魔法少女奈叶A's Sound Stage 1-4
      - 魔法少女奈叶StrikerS Sound Stage 1-4
      - StrikerS Sound Stage
      - [魔法少女奈叶The MOVIE 1st 广播剧CD
        Side-M\&N](../Page/魔法少女奈叶.md "wikilink")
      - [魔法少女奈叶The MOVIE 2nd 广播剧CD A's
        Side-T](../Page/魔法少女奈叶A's.md "wikilink")
      - [魔法少女奈叶Reflection 广播剧CD](../Page/魔法少女奈叶Reflection.md "wikilink")

## 注解

## 外部連結

  - [經紀公司網站所載簡歷](https://web.archive.org/web/20140715070956/http://www.artsvision.co.jp/data.php?id=713)
  - [水橋香織本人營運的個人網頁](http://miz84.com/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:ARTSVISION所属声优](../Category/ARTSVISION所属声优.md "wikilink")

1.  出自「月刊コンプティーク」上連載的『水橋かおりの「はたらく水橋さん」』最終回（2005年2月刊）
2.  在『[向陽素描](../Page/向陽素描.md "wikilink")』第2回中、和[阿澄佳奈說過自己在這個時期是DaMAX狀態](../Page/阿澄佳奈.md "wikilink")(非常不行的狀態)。
3.  在『[向陽素描×365](../Page/向陽素描×365.md "wikilink")』廣播節目第04回中提到。
4.  『[女友伴身邊](../Page/女友伴身邊.md "wikilink")』廣播節目第57回。
5.  『[女友伴身邊](../Page/女友伴身邊.md "wikilink")』廣播節目第15回、36回、57回、71回等。
6.  首次出現是在『[向陽素描](../Page/向陽素描.md "wikilink")』廣播節目「帰ってきたひだまりラジオ」中。
7.  「只要出問題，C4都能搞定」的日文版捏他。意思就是「只要有問題，水橋都能搞定」
8.  出自『[向陽素描](../Page/向陽素描.md "wikilink")』動畫DVD第1季第4卷的聲優採訪。
9.  『かおりと涼の キミキス チューニングポップ♪』
10. 出自Ageファンクラブ会報「アゲくのはて vol.17」。
11. 『かおりと涼の キミキス チューニングポップ♪』Vol.4中的附錄廣播劇。
12.
13. 『聲優綜藝節目 SAY\!YOU\!SAY\!ME\!』内的原創動畫
14.
15.