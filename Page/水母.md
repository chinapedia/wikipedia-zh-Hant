**水母**（*Jellyfish*，又名**白鮓**、****\[1\]）是[無脊椎動物](../Page/無脊椎動物.md "wikilink")，屬於[刺胞動物門中的一員](../Page/刺胞動物門.md "wikilink")，其中包括水母、[海葵](../Page/海葵.md "wikilink")、[珊瑚和](../Page/珊瑚.md "wikilink")[水螅](../Page/水螅.md "wikilink")。全世界的海洋中有超過兩百種的水母，牠們分布於全球各地的水域裡，無論是熱帶的水域﹑溫帶的水域﹑淺水區﹑約百米深的[海洋](../Page/海洋.md "wikilink")，甚至是淡水區都有牠們的蹤影。水母早於六億五千萬年前就已經存在。水母的形狀大小各不相同，最大的水母其[觸手可以延伸約十米遠](../Page/觸手.md "wikilink")。
在分類上有些屬於[水螅綱](../Page/水螅綱.md "wikilink")，有些屬於[缽水母綱](../Page/缽水母綱.md "wikilink")，其生活史中，幾乎所有種類都有兩型，即**水螅型**和**水母型**，並有兩型在[有性生殖與](../Page/有性生殖.md "wikilink")[無性生殖之間的世代交現象](../Page/無性生殖.md "wikilink")，而人們常見的水母則是有性的水母型。

## 壽命

水母壽命很短，平均只有數個月的生命。但有一種[灯塔水母](../Page/灯塔水母.md "wikilink")（學名：）能通過反覆通常[生殖和轉分化達到](../Page/生殖.md "wikilink")「不老不死」。

## 構造

大部分水母都有三個主要部位：圓傘狀或是鐘狀的身體，觸器和口腕。
水母鐘狀身體下面有一些特殊的肌肉能擴張然後迅速收縮，把身體內的水排出體外，通過噴水推進的方法，水母便能向相反的方向游動。一些水母有一層能夠收縮鐘狀體的皮層，使水母能夠快速移動。水母並不擅長游泳，牠們常常要借助風、浪和水流來移動。

水母體內百分之九十五是[水](../Page/水.md "wikilink")，百分之三是[鹽](../Page/鹽.md "wikilink")，百分之二是[蛋白質](../Page/蛋白質.md "wikilink")。水母的[傘狀體內有一種特別的腺](../Page/傘狀體.md "wikilink")，可以發出[一氧化碳](../Page/一氧化碳.md "wikilink")，使傘狀體膨脹。牠們沒有[心臟](../Page/心臟.md "wikilink")﹑[血液](../Page/血液.md "wikilink")﹑[鰓和](../Page/鰓.md "wikilink")[腦](../Page/腦.md "wikilink")。牠們簡單的感應器官，使牠們能分辨氣味、味道並幫助牠們在水裏保持平衡。鐘狀體的邊緣有一排圓形的小囊，當水母向一方過度傾斜的時候，這些囊就會刺激神經末梢來收縮[肌肉](../Page/肌肉.md "wikilink")，並把水母轉到正確的方向上去。位於鐘狀體邊緣的光感器官能使牠們分辨光亮與黑暗。
通過化學感受器水母可感覺到氣味和味道。通過觸手和口腕上的感受器，水母還可以感到物體的運動，幫助牠們尋找食物。有一種水母，牠們有一個或更多的口溝。裏面有口腕，功能就像嘴巴。牠們的觸手裏面有一個小小的器官，也就是水母們的「耳朵」，能夠預報[海洋風暴的來臨](../Page/海洋風暴.md "wikilink")，當[海浪和](../Page/海浪.md "wikilink")[空氣磨擦而產生的](../Page/空氣.md "wikilink")[次聲波衝擊它](../Page/次聲波.md "wikilink")，便會刺激周圍的[神經感受器](../Page/神經感受器.md "wikilink")。所以，在風暴來臨之前的十幾個小時，牠們就會接到消息從海面一下子全部消失了。水母沒有[肺或鰓](../Page/肺.md "wikilink")，他們通過外層的組織來呼吸，整個外層的組織都可以和海水交換氧氣和二氧化碳。鐘狀體上有閃光的白色線條，稱為[徑向水管](../Page/徑向水管.md "wikilink")，用來輸送養分至水母的身體各部份。水母的[觸手和身體上都佈滿](../Page/觸手.md "wikilink")[刺絲囊](../Page/刺絲囊.md "wikilink")。這些絲囊可以在幾毫秒內迅速螫傷、捕捉或征服獵物。有些水母是無毒。

## 形態

[Jellyfish_in_Sumida_Aquarium2_20180215.jpg](https://zh.wikipedia.org/wiki/File:Jellyfish_in_Sumida_Aquarium2_20180215.jpg "fig:Jellyfish_in_Sumida_Aquarium2_20180215.jpg")
不同的水母有不同的形態，地球上最大的水母是「[獅鬃水母](../Page/獅鬃水母.md "wikilink")」，直徑有2[公尺](../Page/米.md "wikilink")，[觸手長](../Page/觸手.md "wikilink")35米，最小的[伊魯康吉水母全長則只有](../Page/伊魯康吉水母.md "wikilink")12[毫米](../Page/毫米.md "wikilink")。[帶水母的周圍和中間部分](../Page/帶水母.md "wikilink")，分佈著幾條平行的光帶，當牠游動的時候，光帶隨波搖曳，非常優美。水母大多因為獨特的外表被人冠以特別的名字。

有些水母不單顏色多變，而且還會在水中發光，有些閃耀著微弱的淡綠色或藍紫色光芒，有些還帶有彩虹般的光暈，當牠們在海中遊動時，就變成了一個光彩奪目的彩球，光影隨波搖曳，非常優美。水母發光靠的是一種叫[埃奎明的奇妙](../Page/埃奎明.md "wikilink")[蛋白質](../Page/蛋白質.md "wikilink")，這種蛋白質和[鈣](../Page/鈣.md "wikilink")[離子相混合的時候](../Page/離子.md "wikilink")，就會發出強藍光來。[埃奎明的量在水母體內越多](../Page/埃奎明.md "wikilink")，發的光就越強，每隻水母平均只含有五十[微克的這種物質](../Page/微克.md "wikilink")。

## 繁殖

[雌雄異體](../Page/雌雄異體.md "wikilink")，有[生殖腺在近](../Page/生殖腺.md "wikilink")[胃囊處](../Page/胃囊.md "wikilink")。成熟的[精子流入雌彥萁體內受精](../Page/精子.md "wikilink")。[受精卵發育成幼蟲離開母體](../Page/受精卵.md "wikilink")，在水裏游動一會兒後，沉下海底形成[幼體](../Page/幼體.md "wikilink")，後變成[橫裂體](../Page/橫裂體.md "wikilink")，橫裂體分裂成多個碟狀幼體，再發育成成體。

近年，世界各地的海域常常有突如其來的水母群的出現。早前，[日本海域便出現兩米長的巨型](../Page/日本.md "wikilink")「[越前水母](../Page/越前水母.md "wikilink")」群，估計[中國](../Page/中國.md "wikilink")[長江流域是越前水母的來源之一](../Page/長江.md "wikilink")。隨著中國沿海業的過度捕撈，減少了與水母爭食的魚類，增加了[浮游生物](../Page/浮游生物.md "wikilink")，造成水母繁衍過剩，加上[長江流域連場反常的大雨](../Page/長江.md "wikilink")，將巨型水母沖到日本海域。此外，海洋的水温變暖亦加速水母的繁殖和生長。

## 獵物與獵者

[Jellyfish_in_Sumida_Aquarium4_20180215.jpg](https://zh.wikipedia.org/wiki/File:Jellyfish_in_Sumida_Aquarium4_20180215.jpg "fig:Jellyfish_in_Sumida_Aquarium4_20180215.jpg")
所有水母都是肉食性動物，牠們以[魚類和](../Page/魚類.md "wikilink")[浮游生物為食](../Page/浮游生物.md "wikilink")。當獵食的時候，水母很被動，只捕食游到牠們身邊的動物。牠們用觸手上的刺絲囊來螫傷或是殺死獵物，然後把食物送到嘴和消化腔裏。大部份水母幾乎是透明的，令敵人難於發現。有些水母能夠發光，單憑身上發出的幽光，水母不費吹灰之力就能吸引獵物。

儘管水母有刺絲囊的保護，但[海龜和](../Page/海龜.md "wikilink")[翻車魚還是以牠們為食](../Page/翻車魚.md "wikilink")，而水母亦是許多人的美食。若海龜數目減少，水母的[繁殖率便會激增](../Page/繁殖率.md "wikilink")，水母過度繁殖，自然又會影響到其他魚類及浮游生物的繁衍，令生態系統失衡。

在[馬來西亞至](../Page/馬來西亞.md "wikilink")[澳大利亞的海面上](../Page/澳大利亞.md "wikilink")，有兩種分別叫做[澳洲箱形水母和](../Page/澳洲箱形水母.md "wikilink")[曳手水母](../Page/曳手水母.md "wikilink")，其分泌的毒性很強，如果被它們刺到，在幾分鐘之內就會呼吸困難而死亡，因此它們又被稱為殺手水母，被這樣的水母螫傷的人可以在短時間之內喪命。但大多數情況下，螫傷只會引起極端**[疼痛](../Page/疼痛.md "wikilink")**、**[惡心](../Page/惡心.md "wikilink")**、**[紅疹](../Page/紅疹.md "wikilink")**和**[鞭痕](../Page/鞭痕.md "wikilink")**，有時會持續數周。被水母螫後，發生[呼吸困難症狀時](../Page/呼吸困難.md "wikilink")，應立即實施[人工呼吸](../Page/人工呼吸.md "wikilink")，或注射[強心劑](../Page/強心劑.md "wikilink")，千萬不可大意，並應迅速尋求醫護救助。

## 天敵

水母的天敵除了[人類之外](../Page/人類.md "wikilink")，目前已知大自然中會捕食水母的生物有：

  - [鯊魚](../Page/鯊魚.md "wikilink")：[鯊魚偶爾會](../Page/鯊魚.md "wikilink")[捕食水母](../Page/捕食.md "wikilink")，但大部分的時候不會。
  - [玳瑁](../Page/玳瑁.md "wikilink")：目前已知[玳瑁除了](../Page/玳瑁.md "wikilink")[捕食小](../Page/捕食.md "wikilink")[魚為生](../Page/魚.md "wikilink")，另一[食物來源為水母](../Page/食物.md "wikilink")。
  - [海龜](../Page/海龜.md "wikilink")：捕食水母為生，海龜除了[眼睛外](../Page/眼睛.md "wikilink")，身體其他部分都可以抵抗水母的毒性，牠們在捕食水母時會閉上眼睛。不过因為目前[海中](../Page/海.md "wikilink")[垃圾很多](../Page/垃圾.md "wikilink")，經常發生[海龜將漂浮的](../Page/海龜.md "wikilink")[塑膠袋當作水母吸食而](../Page/塑膠袋.md "wikilink")[窒息](../Page/窒息.md "wikilink")[死亡](../Page/死亡.md "wikilink")。
  - [曼波魚](../Page/曼波魚.md "wikilink")：[捕食水母為生](../Page/捕食.md "wikilink")。
  - [紫螺](../Page/紫螺.md "wikilink")：分布在熱帶太平洋溫暖的水域，愛吃漂浮在水面的水母。

## 外部連結

  - [Explore Jellyfish – Smithsonian Ocean
    Portal](https://web.archive.org/web/20120419201900/http://ocean.si.edu/ocean-portal-generated-tags/jellyfish)
  - [Jellyfish and Other Gelatinous
    Zooplankton](http://jellieszone.com/)
  - [Jellyfish Facts – Information on Jellyfish and Jellyfish
    Safety](http://www.jellyfishfacts.net/)
  - [Cotylorhiza
    tuberculata](http://www.malawicichlidhomepage.com/other/cotylorhiza_tuberculata.html)
  - "[There's no such thing as a
    jellyfish](https://www.youtube.com/watch?v=3HzFiQFFQYw)" from The
    [MBARI](../Page/Monterey_Bay_Aquarium_Research_Institute.md "wikilink")
    YouTube channel

<!-- end list -->

  - 圖片：

<!-- end list -->

  - [Jellyfish Exhibition At National Aquarium, Baltimore, Maryland
    (USA) – Photo
    Gallery](https://archive.is/20130102224831/http://picasaweb.google.com/sridhar.saraf.pictures/JellyfishExhibitionAtNationalAquariumBaltimoreMarylandUSA/)

[Category:刺胞動物門](../Category/刺胞動物門.md "wikilink")

1.