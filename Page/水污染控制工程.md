[Wonga_wetlands_sewage_plant.jpg](https://zh.wikipedia.org/wiki/File:Wonga_wetlands_sewage_plant.jpg "fig:Wonga_wetlands_sewage_plant.jpg")
**水污染控制工程**，是目标使其受纳[水体的各项功能能符合有关方面要求所作的各类工作的总称](../Page/水体.md "wikilink")。[水污染控制工程按处理功能的对象](../Page/水污染控制.md "wikilink")，有接合了环境功能的水利设施、大型市政污水处理系统、中小城镇污水处理系统及工商业污水处理系统。

依處理的群組區分，則包括集中污水处理工程和分类污水处理工程。分类污水处理工程一般用于处理工厂排放的特定类型污水，含有工厂产生的具体有毒有害物质，需要用特别设计的工艺处理。集中污水处理工程主要用于集中处理城市生活污水，建造城市污水处理厂，主要处理对象为生活污水中含有的[耗氧物质](../Page/耗氧物质.md "wikilink")（指标为[COD或](../Page/COD.md "wikilink")[BOD](../Page/BOD.md "wikilink")）、[氮素](../Page/氮素.md "wikilink")（指标为[铵盐](../Page/铵盐.md "wikilink")、[硝酸盐和](../Page/硝酸盐.md "wikilink")[亚硝酸盐](../Page/亚硝酸盐.md "wikilink")）、[磷素](../Page/磷素.md "wikilink")（指标为[正磷酸盐和](../Page/磷酸.md "wikilink")[总磷](../Page/总磷.md "wikilink")）。另外还需要控制的指标有[DO](../Page/溶解氧.md "wikilink")（溶解氧）、[色度](../Page/色度.md "wikilink")、恶臭、[pH值](../Page/pH.md "wikilink")、[大肠杆菌数量和](../Page/大肠杆菌.md "wikilink")[SS](../Page/懸浮物.md "wikilink")（悬浮固体物质）。

普通污水处理方法主要分三大类：物理方法（包括过滤、沉淀、紫外消毒等）、化学方法（臭氧氧化、氯气消毒等）、生物化学方法（厌氧生物法、好氧生物法等）。

一个典型的污水处理工艺流程为：进水、物理处理（格栅除渣、沉砂）、生物处理（厌氧法、好氧法）、物理处理（二次沉淀）、物理化学处理（紫外、臭氧或氯气消毒）及出水。生物处理环节去除掉污水中大部分的污染物，并降低色度，最后的消毒程序去除了大部分的致病菌，如大肠杆菌。

## [厌氧法](../Page/厌氧法.md "wikilink")

  - [厌氧消化池](../Page/厌氧消化池.md "wikilink")
      - [化粪池](../Page/化粪池.md "wikilink")
      - [Imhoff Tank](../Page/Imhoff_Tank.md "wikilink")
  - 现代高速厌氧反应器
      - [厌氧接触法](../Page/厌氧接触法.md "wikilink")
      - [厌氧滤池](../Page/厌氧滤池.md "wikilink")
      - [上流式厌氧污泥床](../Page/上流式厌氧污泥床.md "wikilink")([UASB](../Page/UASB.md "wikilink"))

## [好氧法](../Page/好氧法.md "wikilink")

  - [活性污泥法](../Page/活性污泥法.md "wikilink")
      - 传统活性污泥法
      - 完全混合活性污泥法
      - 阶段曝气活性污泥法
      - 吸附-再生活性污泥法
      - 延时曝气活性污泥法
      - 高负荷活性污泥法
      - 纯氧曝气活性污泥法
      - 浅层低压曝气活性污泥法
      - 深水曝气活性污泥法
      - 深井曝气活性污泥法
  - [生物膜法](../Page/生物膜法.md "wikilink")
      - 生物滤池
      - 生物转盘
      - 生物接触氧化法
      - 好氧生物流化床
      - [曝气生物滤池(ABF)](../Page/曝气生物滤池\(ABF\).md "wikilink")
  - 普通[三段法A](../Page/三段法.md "wikilink")-A-O工艺
  - [氧化沟](../Page/氧化沟.md "wikilink")
      - Carrousel氧化沟（卡鲁塞尔法）
      - Orbal氧化沟-同心圆式（奥贝尔法）
      - 交替工作氧化沟
      - 曝气沉淀一体化氧化沟
  - [分批式间歇活性污泥法(SBR)](../Page/分批式间歇活性污泥法\(SBR\).md "wikilink")
      - UNITANK
  - [薄膜生物反应器(MBR)](../Page/薄膜生物反应器\(MBR\).md "wikilink")
  - [A-B吸附降解法](../Page/A-B吸附降解法.md "wikilink")（此种方法其实属于好氧法的一种，没有必要单独分类）

## 废水天然生物处理

  - 稳定塘
      - 好养塘
      - 兼性塘
      - 厌氧塘
      - 曝气塘
      - 深度处理塘
      - 综合生物塘
  - 土地处理
      - 慢速渗滤
      - 快速渗滤
      - 地表漫流
      - 湿地系统
      - 地下渗滤系统

[Category:水污染](../Category/水污染.md "wikilink")
[Category:環境工程](../Category/環境工程.md "wikilink")