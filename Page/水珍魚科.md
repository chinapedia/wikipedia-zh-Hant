**水珍魚科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[水珍魚目的一個科](../Page/水珍魚目.md "wikilink")。

## 分類

**水珍魚科**其下分2個屬，如下：

### 水珍魚屬(*Argentina*)

  - [艾麗西亞水珍魚](../Page/艾麗西亞水珍魚.md "wikilink")(*Argentina aliceae*)
  - [澳洲水珍魚](../Page/澳洲水珍魚.md "wikilink")(*Argentina australiae*)
  - [巴西水珍魚](../Page/巴西水珍魚.md "wikilink")(*Argentina brasiliensis*)
  - [布氏水珍魚](../Page/布氏水珍魚.md "wikilink")(*Argentina brucei*)
  - [長體水珍魚](../Page/長體水珍魚.md "wikilink")(*Argentina elongata*)
  - [肯尼亞水珍魚](../Page/肯尼亞水珍魚.md "wikilink")(*Argentina euchus*)
  - [喬奇水珍魚](../Page/喬奇水珍魚.md "wikilink")(*Argentina georgei*)
  - [鹿兒島水珍魚](../Page/鹿兒島水珍魚.md "wikilink")(*Argentina
    kagoshimae*)：又稱[水珍魚](../Page/水珍魚.md "wikilink")。
  - [太平洋水珍魚](../Page/太平洋水珍魚.md "wikilink")(*Argentina sialis*)
  - [大西洋水珍魚](../Page/大西洋水珍魚.md "wikilink")(*Argentina silus*)
  - [梭水珍魚](../Page/梭水珍魚.md "wikilink")(*Argentina sphyraena*)
  - [施氏水珍魚](../Page/施氏水珍魚.md "wikilink")(*Argentina stewarti*)
  - [條紋水珍魚](../Page/條紋水珍魚.md "wikilink")(*Argentina striata*)

### 舌珍魚屬(*Glossanodon*)

  - [澳大利亞舌珍魚](../Page/澳大利亞舌珍魚.md "wikilink")(*Glossanodon australis*)
  - [丹氏舌珍魚](../Page/丹氏舌珍魚.md "wikilink")(*Glossanodon danieli*)
  - [長身舌珍魚](../Page/長身舌珍魚.md "wikilink")(*Glossanodon elongatus*)
  - [小鷹丸舌珍魚](../Page/小鷹丸舌珍魚.md "wikilink")(*Glossanodon kotakamaru*)
  - [舌珍魚](../Page/舌珍魚.md "wikilink")(*Glossanodon leioglossus*)：又稱光舌水珍魚。
  - [線紋舌珍魚](../Page/線紋舌珍魚.md "wikilink")(*Glossanodon lineatus*)
  - [黑灰舌珍魚](../Page/黑灰舌珍魚.md "wikilink")(*Glossanodon melanomanus*)
  - [前肛舌珍魚](../Page/前肛舌珍魚.md "wikilink")(*Glossanodon mildredae*)
  - [智利舌珍魚](../Page/智利舌珍魚.md "wikilink")(*Glossanodon nazca*)
  - [波氏舌珍魚](../Page/波氏舌珍魚.md "wikilink")(*Glossanodon polli*)
  - [擬線紋舌珍魚](../Page/擬線紋舌珍魚.md "wikilink")(*Glossanodon pseudolineatus*)
  - [矮舌珍魚](../Page/矮舌珍魚.md "wikilink")(*Glossanodon pygmaeus*)
  - [半帶舌珍魚](../Page/半帶舌珍魚.md "wikilink")(*Glossanodon semifasciatus*)
  - [斯氏舌珍魚](../Page/斯氏舌珍魚.md "wikilink")(*Glossanodon struhsakeri*)

[\*](../Category/水珍魚科.md "wikilink")