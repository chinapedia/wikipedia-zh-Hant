**水稻**（[学名](../Page/学名.md "wikilink")：**）是[草本](../Page/草本.md "wikilink")[稻屬的一种](../Page/稻屬.md "wikilink")，也是稻属中作为粮食的最主要最悠久的一种，又称为**亚洲型栽培稻**，簡單來說也可以說是[稻](../Page/稻.md "wikilink")。

## 特征

為一年生，[禾本科植物](../Page/禾本科.md "wikilink")，[单子叶](../Page/單子葉植物.md "wikilink")，性喜温湿，成熟時約有1到1.8[米高](../Page/米.md "wikilink")，葉子細長，約有50到100[公分長](../Page/公分.md "wikilink")，寬約2到2.5公分。稻米的花非常小，開花時，主要花枝會呈現拱形，在枝頭往下30到50公分間都會開小花，大部分自花[授粉並結種子](../Page/授粉.md "wikilink")，稱為稻穗。一般稻穗的大小在5到12[毫米長](../Page/毫米.md "wikilink")，2到3毫米厚度。

## 病害

水稻种植中常见的病害如下\[1\]\[2\]。

### 昆蟲

  - 水稻[水象鼻蟲](../Page/水象鼻蟲.md "wikilink")
  - 稻[象鼻蟲](../Page/象鼻蟲.md "wikilink")
  - 非洲[螻蛄](../Page/螻蛄.md "wikilink")
  - [三化螟](../Page/三化螟.md "wikilink")
  - [二化螟](../Page/二化螟.md "wikilink")
  - [大螟](../Page/大螟.md "wikilink")
  - [褐飛蝨](../Page/褐飛蝨.md "wikilink")
  - [白背飛蝨](../Page/白背飛蝨.md "wikilink")
  - [斑飛蝨](../Page/斑飛蝨.md "wikilink")
  - [偽黑尾葉蟬](../Page/偽黑尾葉蟬.md "wikilink")
  - [電光葉蟬](../Page/電光葉蟬.md "wikilink")
  - [白翅葉蟬](../Page/白翅葉蟬.md "wikilink")
  - 稻粉[介殼蟲](../Page/介殼蟲.md "wikilink")
  - [小稻蝗](../Page/小稻蝗.md "wikilink")
  - 水稻[薊馬](../Page/薊馬.md "wikilink")
  - [瘤野螟](../Page/瘤野螟.md "wikilink")
  - [稻螟蛉](../Page/稻螟蛉.md "wikilink")
  - 臺灣夜盜（[稻行軍蟲](../Page/稻行軍蟲.md "wikilink")）
  - [稻苞蟲](../Page/稻苞蟲.md "wikilink")
  - [樹蔭蝶](../Page/樹蔭蝶.md "wikilink")
  - [負泥蟲](../Page/負泥蟲.md "wikilink")
  - [稻心蠅](../Page/稻心蠅.md "wikilink")
  - 亞洲[潛葉蠅](../Page/潛葉蠅.md "wikilink")
  - 南方[綠椿象](../Page/綠椿象.md "wikilink")

### 動物

  - [稻細蟎](../Page/稻細蟎.md "wikilink")
  - [福壽螺](../Page/福壽螺.md "wikilink")
  - 冬季候鳥（[雁鴨類](../Page/雁.md "wikilink")）
  - [沙蠶](../Page/沙蠶.md "wikilink")（鹽水[蜈蚣](../Page/蜈蚣.md "wikilink")）
  - [螯蝦](../Page/螯蝦.md "wikilink")
  - [麻雀](../Page/麻雀.md "wikilink")
  - [斑文鳥](../Page/斑文鳥.md "wikilink")

### 微生物

  - 育箱秧苗立枯病
  - 稻胡麻[葉枯病](../Page/葉枯病.md "wikilink")
  - 水稻徒長病
  - [稻熱病](../Page/稻熱病.md "wikilink")
  - 稻小粒菌核病
  - 水稻疑似[紋枯病](../Page/紋枯病.md "wikilink")
  - [稻葉鞘腐敗病](../Page/稻葉鞘腐敗病菌.md "wikilink")
  - 稻褐條葉枯病
  - [紋枯病](../Page/紋枯病.md "wikilink")
  - [稻麴病](../Page/稻麴病.md "wikilink")
  - 穀粒病害
  - 稻細菌性[穀枯病](../Page/穀枯病.md "wikilink")
  - 水稻白葉枯病
  - 水稻[黃萎病](../Page/黃萎病.md "wikilink")
  - [水稻黃葉病](../Page/水稻黃葉病.md "wikilink")
  - 水稻縞葉枯病
  - 水稻草狀矮化病及其相關病害
  - 水稻皺縮矮化病
  - 水稻[白尖病](../Page/白尖病.md "wikilink")

## 相关条目

  - [山稻](../Page/山稻.md "wikilink")
  - [育秧移栽](../Page/育秧移栽.md "wikilink")
  - [BT63基因改造水稻事件](../Page/BT63基因改造水稻事件.md "wikilink")

## 参考文献

[Category:稻屬](../Category/稻屬.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [水稻保護](http://kmweb.coa.gov.tw/techcd/%E4%BD%9C%E7%89%A9%E7%97%85%E8%9F%B2%E5%AE%B3%E8%88%87%E8%82%A5%E5%9F%B9%E7%AE%A1%E7%90%86%E6%8A%80%E8%A1%93%E8%B3%87%E6%96%99/EBOOK/%E6%B0%B4%E7%A8%BB%E4%BF%9D%E8%AD%B7/index.htm)，農業知識入口網，[行政院農業委員會動植物防疫檢疫局](../Page/行政院農業委員會動植物防疫檢疫局.md "wikilink")
2.  [主要水稻害蟲檢索表](http://www.baphiq.gov.tw/Publish/plant_protect_pic_8/ricePDF/03-2.pdf)（[pdf](../Page/pdf.md "wikilink")），植物保護圖鑑系列8，行政院農業委員會動植物防疫檢疫局