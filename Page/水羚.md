**水羚**（學名：*Kobus
ellipsiprymnus*），又称**非洲大羚羊**，是一種生活在[西非](../Page/西非.md "wikilink")、[中非](../Page/中非.md "wikilink")、[東非及](../Page/東非.md "wikilink")[南非的](../Page/南非.md "wikilink")[羚羊](../Page/羚羊.md "wikilink")。

水羚肩高190-210厘米及體重160-240[公斤](../Page/公斤.md "wikilink")。牠們的毛皮是褐紅色及隨著年紀而轉暗。在[氣管附近有白色的圍兜](../Page/氣管.md "wikilink")，及在尾巴處有白色的圍圈。只有雄性有角，角呈螺旋型並向後彎。

水羚會在[灌木林及](../Page/灌木林.md "wikilink")[大草原近水的地方吃](../Page/大草原.md "wikilink")[草](../Page/草.md "wikilink")。雖然牠們的名字與水有關，但牠們並不喜歡進入水中。水羚是白天活動的[動物](../Page/動物.md "wikilink")。2-600頭雌性水羚會聚集成群。雄性會在自己年青時保護自己約300英畝的領域。但在十歲前會開始失去牠們的領域。

水羚的身體有著難聞的味度。一般[獅子都不會獵殺牠們](../Page/獅子.md "wikilink")，除非在非常饑餓的情況下才會獵殺牠們。

[Kobus_ellipsiprymnus.JPG](https://zh.wikipedia.org/wiki/File:Kobus_ellipsiprymnus.JPG "fig:Kobus_ellipsiprymnus.JPG")

[Category:水羚屬](../Category/水羚屬.md "wikilink")