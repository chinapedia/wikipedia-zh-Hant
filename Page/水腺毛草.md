**水腺毛草**（學名：*Byblis
aquatica*）為[腺毛草科的](../Page/腺毛草科.md "wikilink")[一年生](../Page/一年生.md "wikilink")[食肉植物](../Page/食肉植物.md "wikilink")。通常稱作「彩虹草」。本種描述是於1998年由[艾倫·勞瑞以及](../Page/艾倫·勞瑞.md "wikilink")[約翰·葛德菲·康蘭發表](../Page/:en:John_Godfrey_Conran.md "wikilink")，歸類到[澳大利亞北部一年生的物種](../Page/澳大利亞.md "wikilink")，曾被認為是「[亞麻花腺毛草的複合體](../Page/亞麻花腺毛草.md "wikilink")」。生長是以半水生的狀態，並會運用長在莖上的黏液腺體。

## 植物學歷史

水腺毛草的是由艾倫·勞瑞於1988年4月首次採得。栽培本種時被視為是亞麻花腺毛草的生態型，並命名成*Byblis* aff.
*liniflora* "Darwin"。這樣的處理直到貝利·梅耶斯-萊斯（Barry
Meyers-Rice）清楚證明本種的[生殖隔離](../Page/生殖隔離.md "wikilink")，1996年揚·弗利兹（Jan
Flisek）提議改成一個新物種的[分類單元來描述](../Page/分類單元.md "wikilink")。以至於1998年勞瑞處理了一部分他的澳大利亞北部種類的修訂。

## 注釋

<div class="references-small">

  - Lowrie, Allen and Conran, John G. 1998. A Taxonomic Revision Of The
    Genus *Byblis* (Byblidaceae) In Northern Australia. In: *Nuytsia*
    **12**(1):59-74.
  - Conran, John G. and Lowrie, Allen. 1993. *Byblis liniflora* subsp.
    *occidentalis* (Byblidaceae), A New Subspecies From North-Western
    Australia. In: *Austral. Syst. Bot.* **6**: 175-179.

</div>

## 延伸閱讀

<div class="references-small">

  - Meyers-Rice, Barry. *Byblis* - Notes On Forms New To Cultivation.
    In: *[Carnivorous Plant
    Newsletter](../Page/Carnivorous_Plant_Newsletter.md "wikilink")*
    **22**: 39-40.
  - Flísek, Jan. *Byblis* aff. *liniflora* "Darwin" - Novy druh rodu
    Byblis? In: Trifid, *Darwiniana* **4**: 27-28, 43.

</div>

[Category:腺毛草科](../Category/腺毛草科.md "wikilink")