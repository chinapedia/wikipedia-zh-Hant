台灣總督府總務長官 | vice_occupation= | vice_president= | predecessor = |
successor = [曾根靜夫](../Page/曾根靜夫.md "wikilink") | birth_date =  |
birth_place =
[日本](../Page/日本.md "wikilink")[尾張](../Page/尾張.md "wikilink")（今[愛知縣](../Page/愛知縣.md "wikilink")）
| death_date =  | death_place = [日本](../Page/日本.md "wikilink") |
religion = | profession = [文官](../Page/文官.md "wikilink") | spouse = |
party = }}
**水野遵**（）[日本](../Page/日本.md "wikilink")[尾張](../Page/尾張.md "wikilink")（今[愛知縣](../Page/愛知縣.md "wikilink")）人，幼名恂造，號大路，1895年5月21日以[公使身分兼](../Page/公使.md "wikilink")[台灣總督府](../Page/台灣總督府.md "wikilink")[民政長官](../Page/民政長官.md "wikilink")，1896年4月1日真除奉派為台灣總督府首任民政長官。

## 生平

[1896日本外務次官原敬致臺灣民政局長水野遵照會（清國支那人渡臺證明書）Document_from_Japanese_Vice-Minister_for_Foreign_Affairs_Hara_Takashi_to_Civilian_Affairs_Chief_under_the_Governor-General_of_Taiwan.jpg](https://zh.wikipedia.org/wiki/File:1896日本外務次官原敬致臺灣民政局長水野遵照會（清國支那人渡臺證明書）Document_from_Japanese_Vice-Minister_for_Foreign_Affairs_Hara_Takashi_to_Civilian_Affairs_Chief_under_the_Governor-General_of_Taiwan.jpg "fig:1896日本外務次官原敬致臺灣民政局長水野遵照會（清國支那人渡臺證明書）Document_from_Japanese_Vice-Minister_for_Foreign_Affairs_Hara_Takashi_to_Civilian_Affairs_Chief_under_the_Governor-General_of_Taiwan.jpg")次官[原敬致](../Page/原敬.md "wikilink")[臺灣民政局長水野遵照會](../Page/民政長官.md "wikilink")\]\]
生於尾張的水野遵，其世族於[幕府時代為](../Page/幕府時代.md "wikilink")[武士](../Page/武士.md "wikilink")。1870年，留學[中國習得](../Page/中國.md "wikilink")[漢文](../Page/漢文.md "wikilink")，得其因緣，於1874年[牡丹社事件擔任日漢文通譯](../Page/牡丹社事件.md "wikilink")（[翻譯](../Page/翻譯.md "wikilink")），之後著《-{征}-蕃私記》鼓吹-{征}-臺。1895年5月21日，受[明治政府任命為](../Page/明治.md "wikilink")[公使](../Page/公使.md "wikilink")，全權負責與[李經方辦理交割台灣後續事宜](../Page/李經方.md "wikilink")，同時也兼任代民政局長。1896年4月1日，真除為民政局長。1897年，水野遵離職，升任日本[拓殖務省次官](../Page/拓殖務省.md "wikilink")（次長）。

[Category:貴族院敕選議員](../Category/貴族院敕選議員.md "wikilink")
[Category:臺灣總督府民政長官](../Category/臺灣總督府民政長官.md "wikilink")
[Category:愛知縣出身人物](../Category/愛知縣出身人物.md "wikilink")
[Category:牡丹社事件人物](../Category/牡丹社事件人物.md "wikilink")