[РЦ-53М.JPG](https://zh.wikipedia.org/wiki/File:РЦ-53М.JPG "fig:РЦ-53М.JPG")
**水銀電池**（），亦可稱為**鋅汞電池**，是一种以[锌为负极](../Page/锌.md "wikilink")、[氧化汞为正极](../Page/氧化汞.md "wikilink")、[氢氧化钾为电解液的](../Page/氢氧化钾.md "wikilink")[原电池](../Page/原电池.md "wikilink")。水銀電池放電平穩、開路電壓也非常穩定、易保存且有相當高的[體積](../Page/體積.md "wikilink")[能量比](../Page/能量.md "wikilink")，常见于各种[鈕扣电池](../Page/鈕扣电池.md "wikilink")。但是因為含汞，價格相對較高，且有[環境汙染的問題](../Page/環境汙染.md "wikilink")，目前已減少使用。甚至在1996年以後，在許多國家禁止銷售汞電池而遭到淘汰。

## 歷史

在100多年前，汞氧化鋅電池系統已经被制造。\[1\]，並沒有成為被廣泛使用，直到1942年開發有用的平衡汞電池给軍事應用，如金屬探測器，彈藥和[无线对讲机](../Page/无线对讲机.md "wikilink")。\[2\]水銀電池是在[二次大戰期間](../Page/二次大戰.md "wikilink")，由Samuel
Ruben與Mallory公司共同開發出來，與許多[科技一樣](../Page/科技.md "wikilink")，初期只供軍用，到了戰後，才漸漸被民間使用。

## 基本資料

  - [電壓](../Page/電壓.md "wikilink")：1.35[伏特](../Page/伏特.md "wikilink")
  - 外型：不一定，以[鈕扣型居多](../Page/鈕扣.md "wikilink")

## 構造

  - [正極材料](../Page/正極.md "wikilink")：80\~95%的[氧化汞](../Page/氧化汞.md "wikilink")
    ＋ 5\~15%的[石墨](../Page/石墨.md "wikilink")
  - [負極材料](../Page/負極.md "wikilink")：90%的[鋅粉](../Page/鋅.md "wikilink") ＋
    10%的[汞](../Page/汞.md "wikilink")
  - [電解液](../Page/電解液.md "wikilink")：35\~40%的[氫氧化鉀](../Page/氫氧化鉀.md "wikilink")[溶液](../Page/溶液.md "wikilink")

## 化學反應

負極：Zn + 2OH<sup>-</sup> → Zn(OH)<sub>2</sub> + 2e<sup>-</sup>

正極：HgO + H<sub>2</sub>O + 2e<sup>-</sup> → Hg + 2OH<sup>-</sup>

## 參見

  - [水銀](../Page/水銀.md "wikilink")
  - [電池](../Page/電池.md "wikilink")

## 参考文献

[Category:電池](../Category/電池.md "wikilink")
[Category:汞](../Category/汞.md "wikilink")

1.  C. L. Clarke, US Patent 298175, 1884.
2.  David Linden, Thomas B. Reddy (ed). Handbook Of Batteries 3rd
    Edition. McGraw-Hill, New York, 2002 ISBN 0-07-135978-8, chapter 11.