**水龍獸屬**（[學名](../Page/學名.md "wikilink")：）意為「鏟子蜥蜴」，是[獸孔目](../Page/獸孔目.md "wikilink")[二齒獸下目的一屬](../Page/二齒獸下目.md "wikilink")，存活在[二疊紀晚期到](../Page/二疊紀.md "wikilink")[三疊紀早期](../Page/三疊紀.md "wikilink")，約2億5000萬年前，化石分佈於[南極洲](../Page/南極洲.md "wikilink")、[印度](../Page/印度.md "wikilink")、[南非](../Page/南非.md "wikilink")。在1930年代到1970年代曾有許多種被歸類於水龍獸屬，但目前僅有4到6個已承認種。

水龍獸是種體型笨重、中等大小的[草食性動物](../Page/草食性.md "wikilink")，有短胖的四肢，不同種類的水龍獸的大小有著及其大的差異，小的接近現代的[豬](../Page/豬.md "wikilink")、大的甚至有6米長足以和當時的獵食者[法索拉鱷抗衡](../Page/法索拉鱷.md "wikilink")。水龍獸的嘴裡只有兩顆長牙，自上頜延伸出來。上下頜前端可能有喙狀嘴，用來切碎[植物](../Page/植物.md "wikilink")。根據肩膀與臀部的關節結構，顯示水龍獸採取往兩側半延展的步態。由於前肢比後肢粗壯，水龍獸被認為是一種擅長挖掘的穴居動物。在三疊紀早期，大氣層的[氧氣含量低](../Page/氧氣.md "wikilink")，[二氧化碳含量高](../Page/二氧化碳.md "wikilink")。

水龍獸是已知[三疊紀早期的最常見陸地](../Page/三疊紀.md "wikilink")[脊椎動物](../Page/脊椎動物.md "wikilink")，全世界各地都有牠的化石。水龍獸在[二疊紀-三疊紀滅絕事件中存活下來的原因](../Page/二疊紀-三疊紀滅絕事件.md "wikilink")，可能是因為牠們的祖先體型嬌小、動作靈活而且居住在地下，比其他大型動物更快適應滅絕事件後的環境。近年的研究顯示，水龍獸雖然存活過二疊紀-三疊紀滅絕事件、且三疊紀的陸地動物化石中至少有30%的化石是由水龍獸構成，但三疊紀早期的水龍獸數量仍然比二疊紀晚期的數量少了很多。

## 敘述

[Lystrosaurus2(Skull)-PaleozoologicalMuseumOfChina-May23-08.jpg](https://zh.wikipedia.org/wiki/File:Lystrosaurus2\(Skull\)-PaleozoologicalMuseumOfChina-May23-08.jpg "fig:Lystrosaurus2(Skull)-PaleozoologicalMuseumOfChina-May23-08.jpg")\]\]
水龍獸是種常見的[合弓動物](../Page/合弓動物.md "wikilink")，屬於[獸孔目](../Page/獸孔目.md "wikilink")[二齒獸下目](../Page/二齒獸下目.md "wikilink")，體型接近[豬](../Page/豬.md "wikilink")，身長約0.9公尺（3呎），重量約90公斤（200磅）\[1\]。與其他獸孔目不同，二齒獸類的口鼻部相當短，只有兩顆上頜的長牙。口鼻部的前端可能有[角質喙狀嘴](../Page/角質.md "wikilink")，用來切碎植物。水龍獸已具有[次生顎](../Page/次生顎.md "wikilink")。頜部關節原始，只能做出往前、往後的動作，而不能做出往上、往下的動作。水龍獸被推測頜部肌肉佔據頭顱骨頂端、後側的許多空間。眼睛的位置高，角度往前\[2\]。另外，水龍獸的前肢比後肢粗壯\[3\]，因此被認為有挖掘洞穴的習性\[4\]。
[Lystrosaurus_skeleton.jpg](https://zh.wikipedia.org/wiki/File:Lystrosaurus_skeleton.jpg "fig:Lystrosaurus_skeleton.jpg")
有數個特徵顯示水龍獸是採取往兩側半延展的步態：

  - [肩胛骨後下端有明顯地骨化](../Page/肩胛骨.md "wikilink")，顯示前肢的前後步伐大，也降低身體的側向動作靈活度。\[5\]
  - [薦椎有](../Page/薦椎.md "wikilink")5節，體積大，但彼此之間、與骨盆之間並未癒合。這使水龍獸的脊椎活動較不易彎曲，當牠們行進時身體不會左右擺動。薦椎少於5節的獸孔目，被認為採取四肢往兩側延展的步態，類似現代[蜥蜴](../Page/蜥蜴.md "wikilink")\[6\]。恐龍與哺乳類的四肢直立於身體下方，而薦椎彼此之間癒合，薦椎與骨頭之間癒合\[7\]。
  - [髖臼的結構可使水龍獸採取半四肢往兩側延展步態行走時](../Page/髖臼.md "wikilink")，[股骨不會關節脫落](../Page/股骨.md "wikilink")\[8\]。

## 發現歷史

[Gondwana_fossil_map_ger.png](https://zh.wikipedia.org/wiki/File:Gondwana_fossil_map_ger.png "fig:Gondwana_fossil_map_ger.png")的分布（咖啡色）\]\]
水龍獸的第一個化石，是在19世紀後期由傳教士兼業餘化石挖掘者Elias Root
Beadle所發現。Beadle將這化石的消息通知古動物學家[奧塞內爾·查利斯·馬什](../Page/奧塞內爾·查利斯·馬什.md "wikilink")（Othniel
Charles
Marsh），但馬什沒有回應。馬什的敵手[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker
Cope）獲得這些化石，並在1870年命名為水龍獸\[9\]。水龍的屬名在[古希臘文意為](../Page/古希臘文.md "wikilink")「鏟子蜥蜴」\[10\]。在1871年，馬什也購得一個頭顱骨，但並沒有詳細地研究\[11\]。

### 大陸漂移學說

在1969到1970年，與他的團隊在[南極縱貫山脈的Coalsack](../Page/南極縱貫山脈.md "wikilink")
Bluff發現水龍獸的化石，有助於確認[大陸漂移學說而說服了懷疑論者](../Page/大陸漂移.md "wikilink")。水龍獸目前已在非洲南部、[印度](../Page/印度.md "wikilink")、[中國的早](../Page/中國.md "wikilink")[侏儸紀地層發現](../Page/侏儸紀.md "wikilink")\[12\]。

## 分佈與種

水龍獸的化石發現於[二疊紀晚期到](../Page/二疊紀.md "wikilink")[三疊紀早期的陸相地層](../Page/三疊紀.md "wikilink")，大部分來自於[非洲](../Page/非洲.md "wikilink")，其他化石則發現於[印度](../Page/印度.md "wikilink")、[中國](../Page/中國.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[俄羅斯的](../Page/俄羅斯.md "wikilink")[歐洲部分](../Page/歐洲.md "wikilink")、以及[南極洲](../Page/南極洲.md "wikilink")。由於水龍獸的分布廣泛，因此被當成[大陸漂移學說的重要證據之一](../Page/大陸漂移學說.md "wikilink")\[13\]。

大多數的水龍獸化石發現於[南非](../Page/南非.md "wikilink")，尤其集中於[卡魯盆地](../Page/卡魯盆地.md "wikilink")。該地區已發現大量水龍獸化石，也命名了許多種，但科學家對於該地區有幾種的水龍獸，目前還沒有定論\[14\]。

在1930年代到1970年代之間，水龍獸屬曾經包含23個種\[15\]。在1980年代到1990年代，則剩下6個已承認種：*L.
curvatus*、*L. platyceps*、*L. oviceps*、*L. maccaigi*、*L. murrayi*、以及*L.
declivis*。在2006年的新研究，則縮減到4個種，原本的*L. platyceps*、*L. oviceps*被併入*L.
curvatus*\[16\]。
[Lystrosaurus_BW.jpg](https://zh.wikipedia.org/wiki/File:Lystrosaurus_BW.jpg "fig:Lystrosaurus_BW.jpg")
[Lystr_georg1DB.jpg](https://zh.wikipedia.org/wiki/File:Lystr_georg1DB.jpg "fig:Lystr_georg1DB.jpg")
[模式種](../Page/模式種.md "wikilink")*L. murrayi*與*L.
declivis*的化石，僅發現於二疊紀晚期的地層\[17\]。*L.
maccaigi*是體型最大、最特化的一種，而*L.
curvatus*是最原始的一種。同樣發現於南非的*Kwazulusaurus*，化石類似水龍獸，尤其是*L.
curvatus*。*Kwazulusaurus*有可能是*L. curvatus*的祖先，或是其祖先的近親\[18\]。

*L.
maccaigi*的化石僅發現於二疊紀晚期地層，可能沒有存活過[二疊紀-三疊紀滅絕事件](../Page/二疊紀-三疊紀滅絕事件.md "wikilink")。由於*L.
maccaigi*是較為衍化的物種，但在[化石紀錄的時間很短](../Page/化石紀錄.md "wikilink")，沒有可追溯的直系物種，有研究推測牠們原本存活於其他二疊紀地層，然後遷徙至卡魯地區\[19\]。*L.
curvatus*的化石則集中在狹窄的帶狀地區，生存於滅絕事件前後的短暫時期，因此可以當成判斷兩個時代交界的化石之一。在[尚比亞的二疊紀晚期地層](../Page/尚比亞.md "wikilink")，曾經發現一個*L.
curvatus*的頭顱骨。由於過去在卡魯盆地的二疊紀地層，沒有發現*L. curvatus*的地層，曾有理論推測*L.
curvatus*從尚比亞遷徙至南非卡魯盆地。但近年在卡魯盆地的二疊紀地層發現*L.
curvatus*的化石，推翻上述理論\[20\]。

*L.
georgi*的化石發現於俄羅斯的[莫斯科地區](../Page/莫斯科.md "wikilink")，地質年代為三疊紀最早期，牠們可能是*L.
curvatus*的近親\[21\]。

## 古生態學

在三疊紀最早期的數百萬年，水龍獸是最常見的陸地[脊椎動物](../Page/脊椎動物.md "wikilink")。在某些該時期的挖掘地點，至少有95%的化石是由水龍獸構成\[22\]\[23\]。至少有一個種存活過[二疊紀-三疊紀滅絕事件](../Page/二疊紀-三疊紀滅絕事件.md "wikilink")。由於三疊紀早期缺少肉食性動物的獵食，以及其他草食性動物的競爭，水龍獸迅速[演化輻射出多個物種](../Page/演化輻射.md "wikilink")，而成為三疊紀早期的最常見陸地脊椎動物\[24\]。這是地球歷史中的唯一時期，陸地生態系統幾乎由某種動物所佔據\[25\]。有少數二疊紀物種也存活過二疊紀-三疊紀滅絕事件，例如[獸頭亞目的](../Page/獸頭亞目.md "wikilink")[四犬齒獸](../Page/四犬齒獸.md "wikilink")（*Tetracynodon*）、[麝喙獸](../Page/麝喙獸.md "wikilink")（*Moschorhinus*）、[似鼬鱷獸](../Page/似鼬鱷獸.md "wikilink")（*Ictidosuchoides*），但數量沒有水龍獸繁盛\[26\]。一位研究人員估計陸地脊椎動物花了3000萬年，直到三疊紀晚期，才恢復之前的繁盛與多樣性\[27\]。

目前有數個理論，試圖解釋水龍獸存活過二疊紀-三疊紀滅絕事件的原因，以及水龍獸迅速成為三疊紀早期優勢物種的原因\[28\]：

  - 二疊紀-三疊紀滅絕事件的原因之一，是大氣層的[氧含量下降](../Page/氧.md "wikilink")，[二氧化碳含量上升](../Page/二氧化碳.md "wikilink")\[29\]。水龍獸被推測可能是穴居動物，可能因此適應較差的空氣品質，得以存活過這次滅絕事件。水龍獸的多項特徵，顯示牠們有良好的呼吸效能，例如桶狀胸腔可以容納較大[肺臟](../Page/肺臟.md "wikilink")，較短的鼻孔通道使呼吸較快，高[神經棘使胸部肌肉在擴張](../Page/神經棘.md "wikilink")、收縮時產生更大應力。但是，與其他二齒獸類的身體比例相比，水龍獸的胸腔並沒有特別大。三疊紀二齒獸類的神經棘更長，神經棘的長度似乎與步態、移動速度、體型較有關係，與呼吸效能較無關係。*L.
    murrayi*、*L.
    declivis*的數量也比三疊紀早期的穴居動物更多，例如：[前稜蜥](../Page/前稜蜥.md "wikilink")、[三尖叉齒獸](../Page/三尖叉齒獸.md "wikilink")\[30\]。
  - 另一理論推測水龍獸是半水生動物，因此受到滅絕事件的波及較小。但是卡魯盆地的三疊紀早期地層，雖然有許多[兩棲類化石](../Page/兩棲類.md "wikilink")，但數量仍少於*L.
    murrayi*、*L. declivis*\[31\]。
  - 體型較大的陸地動物，容易因為滅絕事件而滅絕。此理論可以解釋體型較小、較原始的*L.
    curvatus*存活過滅絕事件，而體型較大、較衍化的*L.
    maccaigi*消失了\[32\]。
  - 水龍獸的存亡，似乎也與所食的植物有關係。二疊紀晚期的[叉蕨](../Page/叉蕨.md "wikilink")（*Dicroidium*，又譯[二舌羊齒](../Page/二舌羊齒.md "wikilink")），在三疊紀早期繁盛；而較大型的[舌羊齒](../Page/舌羊齒.md "wikilink")（*Glossopteris*）則消失了\[33\]。
  - 在三疊紀早期的大型掠食動物，例如獸頭亞目的*[麝喙獸屬](../Page/麝喙獸屬.md "wikilink")*、[主龍形類的](../Page/主龍形類.md "wikilink")[原鱷](../Page/原鱷.md "wikilink")，在化石紀錄的存在時間很短，可能造成水龍獸的繁盛\[34\]。

## 大眾文化

水龍獸曾經出現在[BBC的](../Page/BBC.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")《[與巨獸共舞](../Page/與巨獸共舞.md "wikilink")》（*Walking
with
Monsters*），節目單位宣稱[二疊紀的小型](../Page/二疊紀.md "wikilink")[雙齒獸演化成三疊紀的大型水龍獸](../Page/雙齒獸.md "wikilink")。但實際上，兩者可能曾經存活於相同時期。此外，水龍獸還出現在電視節目《[動物末日](../Page/動物末日.md "wikilink")》（*Animal
Armageddon*），節目單位試圖解釋水龍獸具有多物種的原因。

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [Palaeos.com:
    Dicynodontia](https://web.archive.org/web/20060222040556/http://www.palaeos.com/Vertebrates/Units/400Therapsida/400.725.html#Dicynodontia#Dicynodontia)
  - [Hugh Rance, *The Present is the Key to the Past*: "Mammal-like
    reptiles of
    Pangea"](https://web.archive.org/web/20060427060532/http://geowords.com/histbooknetscape/i07.htm)

[Category:二齒獸下目](../Category/二齒獸下目.md "wikilink")
[Category:二疊紀合弓類](../Category/二疊紀合弓類.md "wikilink")
[Category:三疊紀合弓類](../Category/三疊紀合弓類.md "wikilink")

1.

2.

3.
4.
5.
6.
7.

8.
9.

10.

11.
12. Naomi Lubick, *[Investigating the
    Antarctic](http://www.agiweb.org/geotimes/feb05/feature_Antarcticfossils.html)*,
    Geotimes, 2005.

13.

14.
15.  Full version online at

16.

17.
18.
19.
20.
21.
22.
23. [The Consolations of Extinction: includes section on Lystrosaurus
    and end-Permian
    extinction](http://www.orionmagazine.org/index.php/articles/article/268/)

24.

25. BBC: Life Before Dinosaurs

26.
27. {{ cite journal |
    url=<http://journals.royalsociety.org/content/qq5un1810k7605h5/fulltext.pdf>
    | author=Sahney, S. and Benton, M.J. | year=2008 | title=Recovery
    from the most profound mass extinction of all time |
    journal=Proceedings of the Royal Society: Biological |
    doi=10.1098/rspb.2007.1370 | volume = 275 | pages = 759|format=PDF}}

28.

29.
30.
31.
32.
33.
34.