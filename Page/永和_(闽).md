**永和**（935年-936年二月）是[闽惠宗](../Page/闽.md "wikilink")[王延钧的年号](../Page/王延钧.md "wikilink")，共计2年。

闽康宗[王繼鵬即位沿用](../Page/王繼鵬.md "wikilink")\[1\]。

## 纪年

| 永和                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 935年                           | 936年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[永和年号](../Page/永和.md "wikilink")
  - 同期存在的其他政权年号
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：[吳](../Page/吳_\(十國\).md "wikilink")—楊溥之年號
      - [天祚](../Page/天祚.md "wikilink")（935年九月至937年十月）：吳—楊溥之年號
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：後蜀—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:闽年号](../Category/闽年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")
[Category:935年](../Category/935年.md "wikilink")
[Category:936年](../Category/936年.md "wikilink")

1.  陈光主编，中国历代帝王年号手册，北京燕山出版社，1999年11月,280-281 ISBN 7540210311