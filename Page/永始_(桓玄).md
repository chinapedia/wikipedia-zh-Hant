**永始**（初作**[建始](../Page/建始.md "wikilink")**；403年十二月-404年五月）是[東晋](../Page/東晋.md "wikilink")[晋安帝时](../Page/晋安帝.md "wikilink")[桓玄篡立的](../Page/桓玄.md "wikilink")[年号](../Page/年号.md "wikilink")，共计2年。

[胡三省注](../Page/胡三省.md "wikilink")《[资治通鉴](../Page/资治通鉴.md "wikilink")》：[元兴元年](../Page/元兴.md "wikilink")“[桓玄寻改曰](../Page/桓玄.md "wikilink")[大亨](../Page/大亨_\(年号\).md "wikilink")。玄篡，又改曰永始。”

根据《[晋书](../Page/晋书.md "wikilink")》记载，桓玄最初改元“建始”，因为和[赵王伦同](../Page/司马伦.md "wikilink")，改为永始。

## 纪年

| 永始                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 403年                           | 404年                           |
| [干支](../Page/干支纪年.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他时期使用的[永始年号](../Page/永始.md "wikilink")
  - 同期存在的其他政权年号
      - [元兴](../Page/元兴_\(晋安帝\).md "wikilink")（402年-404年）：[東晉皇帝](../Page/東晉.md "wikilink")[晋安帝司马德宗的年号](../Page/晋安帝.md "wikilink")
      - [弘始](../Page/弘始.md "wikilink")（399年九月-416年正月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [光始](../Page/光始.md "wikilink")（401年八月-406年）：[后燕政权](../Page/后燕.md "wikilink")[慕容熙年号](../Page/慕容熙.md "wikilink")
      - [弘昌](../Page/弘昌.md "wikilink")（402年三月-404年二月）：[南凉政权](../Page/南凉.md "wikilink")[秃发傉檀年号](../Page/秃发傉檀.md "wikilink")
      - [建平](../Page/建平.md "wikilink")（400年-405年十一月）：[南燕政权](../Page/南燕.md "wikilink")[慕容德年号](../Page/慕容德.md "wikilink")
      - [庚子](../Page/庚子.md "wikilink")（400年十一月-404年）：[西凉政权](../Page/西凉.md "wikilink")[李暠年号](../Page/李暠.md "wikilink")
      - [永安](../Page/永安_\(北涼\).md "wikilink")（401年六月-412年十月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [天兴](../Page/天兴.md "wikilink")（398年十二月-404年十月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:桓楚年号](../Category/桓楚年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")