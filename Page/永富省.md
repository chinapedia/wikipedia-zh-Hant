**永富省**（）是[越南從](../Page/越南.md "wikilink")1968年到1996年期間的省份，省莅[越池市](../Page/越池市.md "wikilink")，今属[永福省和](../Page/永福省.md "wikilink")[富壽省](../Page/富壽省.md "wikilink")。

## 地理位置

北邊接壤[黃連山省](../Page/黃連山省.md "wikilink")、[河宣省和](../Page/河宣省.md "wikilink")[北太省](../Page/北太省.md "wikilink")，東邊接壤[河內市](../Page/河內市.md "wikilink")，南邊接壤河內市和[河山平省](../Page/河山平省.md "wikilink")，西邊接壤[山羅省](../Page/山羅省.md "wikilink")。

## 面积人口

1976年，永富省面積為​​5187平方千米，人口159.1萬。1979年，面積為4630平方千米，人口1,429,900人。1991年，面積為4823平方千米，人口2,081,043人。

## 歷史

1968年1月26日，[越南民主共和國國會常務委員會發出](../Page/越南民主共和國.md "wikilink")504-NQ/TVQH號決議，合併[永福省和](../Page/永福省.md "wikilink")[富壽省成為永富省](../Page/富壽省.md "wikilink")，[省蒞為](../Page/省蒞.md "wikilink")[越池市](../Page/越池市.md "wikilink")。

永富省下辖[越池市](../Page/越池市.md "wikilink")、[富寿市社](../Page/富寿市社.md "wikilink")、[福安市社](../Page/福安市社.md "wikilink")、[永安市社和](../Page/永安市社.md "wikilink")[平川县](../Page/平川县.md "wikilink")、[锦溪县](../Page/锦溪县.md "wikilink")、[多福县](../Page/多福县.md "wikilink")、[端雄县](../Page/端雄县.md "wikilink")、[夏和县](../Page/夏和县.md "wikilink")、[金英县](../Page/金英县.md "wikilink")、[临洮县](../Page/临洮县_\(越南\).md "wikilink")、[立石县](../Page/立石县.md "wikilink")、[扶宁县](../Page/扶宁县.md "wikilink")、[三阳县](../Page/三阳县.md "wikilink")、[三农县](../Page/三农县.md "wikilink")、[青波县](../Page/青波县.md "wikilink")、[清山县](../Page/清山县.md "wikilink")、[清水县](../Page/清水县_\(越南\).md "wikilink")、[永祥县](../Page/永祥县.md "wikilink")、[安乐县](../Page/安乐县.md "wikilink")、[安朗县](../Page/安朗县.md "wikilink")、[安立县](../Page/安立县.md "wikilink")18个县。

1976年6月26日，福安市社并入安朗县。

1977年7月5日，越南政府通过178-CP号决议，调整永富省行政区划。三农县和清水县合并为[三清县](../Page/三清县.md "wikilink")，永祥县和安乐县合并为[永乐县](../Page/永乐县.md "wikilink")，立石县和三阳县合并为[三岛县](../Page/三岛县.md "wikilink")。安立县和锦溪县合并为[洮江县](../Page/洮江县_\(越南\).md "wikilink")，临洮县和扶宁县合并为[峰州县](../Page/峰州县.md "wikilink")，平川县和安朗县合并为[麊泠县](../Page/麊泠县.md "wikilink")，多福县和金英县、春和市镇（省直辖）合并为[朔山县](../Page/朔山县.md "wikilink")，端雄县、青波县和夏和县合并成[泸江县](../Page/泸江县_\(永富省\).md "wikilink")\[1\]。

1978年12月29日，麊泠县和朔山县划归[河内市](../Page/河内市.md "wikilink")\[2\]。

1979年2月26日，三岛县析置立石县\[3\]。

1980年，永富省下辖越池市、富寿市社、永安市社3市和三岛县、三清县、立石县、永乐县、洮江县、泸江县、峰州县、清山县8县。

1980年12月22日，洮江县析置安立县，泸江县分设端雄县和[青和县](../Page/青和县.md "wikilink")\[4\]。

1991年8月12日，麊泠县重新划归永富省\[5\]。

1995年10月7日，青和县分设为青波县和夏和县，永乐县分设为永祥县和安乐县\[6\]。

1996年，永富省下辖越池市、富寿市社、永安市社3市和端雄县、夏和县、立石县、麊泠县、峰州县、洮江县、三岛县、三清县、青波县、清山县、永祥县、安乐县、安立县13县。

1996年11月26日，越南國會第九屆國會第十次會議通過調整多省行政邊界的決議，将永富省重新分设为[永福省和](../Page/永福省.md "wikilink")[富寿省](../Page/富寿省.md "wikilink")。永福省下辖[永安市社和](../Page/永安市社.md "wikilink")[立石县](../Page/立石县.md "wikilink")、[麊泠县](../Page/麊泠县.md "wikilink")、[三岛县](../Page/三岛县.md "wikilink")、[永祥县](../Page/永祥县.md "wikilink")、[安乐县](../Page/安乐县.md "wikilink")5县；富寿省下辖[越池市](../Page/越池市.md "wikilink")、[富寿市社和](../Page/富寿市社.md "wikilink")[端雄县](../Page/端雄县.md "wikilink")、[夏和县](../Page/夏和县.md "wikilink")、[峰州县](../Page/峰州县.md "wikilink")、[洮江县](../Page/洮江县_\(越南\).md "wikilink")、[三清县](../Page/三清县.md "wikilink")、[青波县](../Page/青波县.md "wikilink")、[清山县](../Page/清山县.md "wikilink")、[安立县](../Page/安立县.md "wikilink")8县\[7\]。

## 行政区划

1996年，永富省下辖1市2市社13县。

  - [越池市](../Page/越池市.md "wikilink")
  - [富寿市社](../Page/富寿市社.md "wikilink")
  - [永安市社](../Page/永安市社.md "wikilink")
  - [端雄县](../Page/端雄县.md "wikilink")
  - [夏和县](../Page/夏和县.md "wikilink")
  - [立石县](../Page/立石县.md "wikilink")
  - [麊泠县](../Page/麊泠县.md "wikilink")
  - [峰州县](../Page/峰州县.md "wikilink")
  - [洮江县](../Page/洮江县_\(越南\).md "wikilink")
  - [三岛县](../Page/三岛县.md "wikilink")
  - [三清县](../Page/三清县.md "wikilink")
  - [青波县](../Page/青波县.md "wikilink")
  - [清山县](../Page/清山县.md "wikilink")
  - [永祥县](../Page/永祥县.md "wikilink")
  - [安乐县](../Page/安乐县.md "wikilink")
  - [安立县](../Page/安立县.md "wikilink")

## 注释

[Category:越南当代行政区划史](../Category/越南当代行政区划史.md "wikilink")
[Category:永福省](../Category/永福省.md "wikilink")
[Category:富壽省](../Category/富壽省.md "wikilink")
[Category:越南旧省](../Category/越南旧省.md "wikilink")
[Category:越南民主共和国行政区划](../Category/越南民主共和国行政区划.md "wikilink")

1.  [178-CP：永富省调整行政区划](https://thuvienphapluat.vn/archive/Quyet-dinh/Quyet-dinh-178-CP-hop-nhat-dieu-chinh-dia-gioi-huyen-thuoc-tinh-Vinh-Phu-vb57438t17.aspx)
2.  [国会决议：河内市扩大地界](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-phe-chuan-viec-phan-vach-lai-dia-gioi-thanh-pho-Ha-Noi-TPHCM-cac-tinh-Ha-Son-Binh-Vinh-Phu-Cao-Lang-Bac-Thai-Quang-Ninh-va-Dong-Nai/42744/noi-dung.aspx)
3.  [71-CP：三岛县析置立石县](https://thuvienphapluat.vn/archive/Quyet-dinh/Quyet-dinh-71-CP-dieu-chinh-dia-gioi-huyen-Tam-Dao-Vinh-Lac-tinh-Vinh-Phu-vb58183t17.aspx)
4.  [377-CP：洮江县析置安立县，泸江县分设为端雄县和青和县](https://thuvienphapluat.vn/archive/Quyet-dinh/Quyet-dinh-377-CP-sua-doi-don-vi-hanh-chinh-cap-huyen-tinh-Vinh-Phu-chia-huyen-Song-Thao-va-huyen-Song-Lo-dieu-chinh-dia-gioi-huyen-Phong-Chau-vb44207t17.aspx)
5.  [国会决议：麊泠县重归永富省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-dieu-chinh-dia-gioi-hanh-chinh-mot-so-tinh-thanh-pho-truc-thuoc-Trung-uong-42808.aspx)
6.  [63-CP：拆分青和县和永乐县](https://thuvienphapluat.vn/archive/Nghi-dinh/Nghi-dinh-63-CP-chia-huyen-vinh-lac-thanh-hoa-thuoc-tinh-Vinh-Phu-vb39596t11.aspx)
7.  [国会决议：永富省恢复设立永福省和富寿省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-chia-va-dieu-chinh-dia-gioi-hanh-chinh-tinh-40091.aspx)