**永恒沉睡**（Sopor Aeternus and the Ensemble of Shadows），常简称作Sopor
Aeternus、Sopor或SA，是[德国的一支](../Page/德国.md "wikilink")[暗潮与](../Page/暗潮.md "wikilink")[哥特风格的乐队](../Page/哥特摇滚.md "wikilink")。乐队的固定成员仅有Anna-Varney
Cantodea一人。Sopor Æternus为拉丁语，意为永恒沉睡或死亡。成立于1989年。

## 简介

永恒沉睡起源于Anna-Varney与Holger相逢于德国的一个哥特俱乐部。最初两人没有钱购买乐器，只能在头脑中创作，尽管如此，他们还是创作了相当多的作品，并在1992年发行了样品唱片。后来Holger离开了乐队。而后厂牌Apocalyptic
Visions为永恒沉睡发行了首张录音室唱片。Sopor
Aeternus的的作品里面使用大量的[铜管](../Page/铜管.md "wikilink")，[木管乐器](../Page/木管.md "wikilink")，尤其是笛子等带有强烈宗教色彩的配乐，以及各种吉他，合成器之类，内容多涉及死亡，自杀，悲苦，绝望，孤独，变性等题材。

## 作品列表

  - *[Es reiten die Toten so
    schnell](../Page/Es_reiten_die_Toten_so_schnell.md "wikilink")*
    (demo tape) (1989年) - 限量50份
  - *["...Ich töte mich jedesmal aufs Neue, doch ich bin unsterblich,
    und ich erstehe wieder auf; in einer Vision des
    Untergangs..."](../Page/"...Ich_töte_mich_jedesmal_aufs_Neue,_doch_ich_bin_unsterblich,_und_ich_erstehe_wieder_auf;_in_einer_Vision_des_Untergangs...".md "wikilink")*
    (1994年)
  - *["Todeswunsch" - Sous le soleil de
    Saturne](../Page/"Todeswunsch"_-_Sous_le_soleil_de_Saturne.md "wikilink")*
    (1995年)
  - *[Ehjeh Ascher Ehjeh](../Page/Ehjeh_Ascher_Ehjeh.md "wikilink") EP*
    (1995年) - 限量3000份
  - *["The Inexperienced Spiral
    Traveller"](../Page/"The_Inexperienced_Spiral_Traveller".md "wikilink")*
    (1997年)
  - *["Voyager" - The Jugglers of
    Jusa](../Page/"Voyager"_-_The_Jugglers_of_Jusa.md "wikilink")*
    (1997年) - 限量3000份
  - *["Dead Lovers' Sarabande" (Face
    One)](../Page/"Dead_Lovers'_Sarabande"_\(Face_One\).md "wikilink")*
    (1999年)
  - *["Dead Lovers' Sarabande" (Face
    Two)](../Page/"Dead_Lovers'_Sarabande"_\(Face_Two\).md "wikilink")*
    (1999年)
  - *["Songs from the inverted
    Womb"](../Page/"Songs_from_the_inverted_Womb".md "wikilink")*
    (2000年)
  - *[Nenia C'alladhan](../Page/Nenia_C'alladhan.md "wikilink")* (2002年)
  - *["Es reiten die Toten so schnell" (或: The Vampyre sucking at his
    own
    Vein)](../Page/"Es_reiten_die_Toten_so_schnell"_\(或:_The_Vampyre_sucking_at_his_own_Vein\).md "wikilink")*
    (2003年)
  - *["La Chambre d'Echo" - Where the dead Birds
    sing](../Page/"La_Chambre_d'Echo"_-_Where_the_dead_Birds_sing.md "wikilink")*
    (2004年)
  - *["Flowers in
    Formaldehyde"](../Page/"Flowers_in_Formaldehyde".md "wikilink") EP*
    (2004年) - 限量2000份
  - *[The Goat / The Bells have stopped
    ringing](../Page/The_Goat_/_The_Bells_have_stopped_ringing.md "wikilink")
    12" vinyl* (2005年) - 限量1000份
  - *[Like a Corpse standing in
    Desperation](../Page/Like_a_Corpse_standing_in_Desperation.md "wikilink")*
    (2005年) - 限量1000份，包括第一张有关永恒沉睡的DVD
  - *[Les Fleurs du Mal](../Page/Les_Fleurs_du_Mal.md "wikilink")*
    (2007年)
  - *Sanatorium Altrosa* 2008年）
  - *A Strange Thing To Say* （2010年）

## 外部链接

  - [官方网站 (英语/德语)](http://www.soporaeternus.de/)
  - [官方乐迷网站 (德语)](http://www.sopor-aeternus.de/)

[Category:德國樂團](../Category/德國樂團.md "wikilink")