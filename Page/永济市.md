**永济市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省的一个](../Page/山西省.md "wikilink")[县级市](../Page/县级市.md "wikilink")，由[运城市代管](../Page/运城市.md "wikilink")。位于山西西南隅，[同蒲铁路穿越市境](../Page/同蒲铁路.md "wikilink")。

## 历史

古称[蒲坂](../Page/蒲坂.md "wikilink")，相传为[舜帝之都](../Page/舜.md "wikilink")。[战国时属于](../Page/战国.md "wikilink")[魏国](../Page/魏国_\(战国\).md "wikilink")，称[蒲邑](../Page/蒲邑.md "wikilink")。[秦置](../Page/秦朝.md "wikilink")[蒲坂县](../Page/蒲坂县.md "wikilink")，[王莽时改](../Page/王莽.md "wikilink")[蒲城](../Page/蒲城.md "wikilink")。[东汉復名蒲坂县](../Page/东汉.md "wikilink")。[隋朝开皇十六年](../Page/隋朝.md "wikilink")（596年）析蒲坂县置[河东县](../Page/河东县.md "wikilink")，[大业二年](../Page/大业_\(年号\).md "wikilink")（606年）省蒲坂入河东县。至[明朝](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年），省河东县入[蒲州](../Page/蒲州_\(北周\).md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正六年](../Page/雍正.md "wikilink")（1728年）升为[蒲州府](../Page/蒲州府.md "wikilink")，并设[附郭](../Page/附郭.md "wikilink")[永济县](../Page/永济县.md "wikilink")，以境内永济渠命名。

1912年废府存县，1958年与[安邑县](../Page/安邑县.md "wikilink")、[解虞县](../Page/解虞县.md "wikilink")、[临猗县合并为](../Page/临猗县.md "wikilink")[运城县](../Page/运城县.md "wikilink")（现运城市）。1961年析运城县复置永济县。1994年1月撤县设市，目前隶属于地级[运城市](../Page/运城市.md "wikilink")。市中心建有舜都广场，纪念舜帝建都于此的传说。

## 地理

位于山西西南隅，南倚[中条山](../Page/中条山.md "wikilink")，西临[黄河](../Page/黄河.md "wikilink")，与[陕西省隔河相望](../Page/陕西.md "wikilink")。

## 行政区划

永济市辖3个[街道](../Page/街道办事处.md "wikilink")、7个[镇](../Page/镇.md "wikilink")。

  - 街道：[城西街道](../Page/城西街道_\(永济市\).md "wikilink")、[城北街道](../Page/城北街道_\(永济市\).md "wikilink")、[城东街道](../Page/城东街道_\(永济市\).md "wikilink")
  - 镇：[虞乡镇](../Page/虞乡镇.md "wikilink")、[卿头镇](../Page/卿头镇.md "wikilink")、[开张镇](../Page/开张镇.md "wikilink")、[栲栳镇](../Page/栲栳镇.md "wikilink")、[蒲州镇](../Page/蒲州镇.md "wikilink")、[韩阳镇](../Page/韩阳镇.md "wikilink")、[张营镇](../Page/张营镇_\(永济市\).md "wikilink")

## 交通

  - 铁路：[同蒲铁路](../Page/同蒲铁路.md "wikilink")（[大同](../Page/大同市.md "wikilink")－[风陵渡](../Page/风陵渡.md "wikilink")）[永济站](../Page/永济站.md "wikilink")、[大西客运专线](../Page/大西客运专线.md "wikilink")[永济北站](../Page/永济北站.md "wikilink")
  - 公路：[运风高速公路](../Page/运风高速公路.md "wikilink")（S87）、、

## 经济

永济的工业以[轻工业为主](../Page/轻工业.md "wikilink")，农业主产[小麦](../Page/小麦.md "wikilink")、[棉花](../Page/棉花.md "wikilink")、[淡水鱼](../Page/淡水鱼.md "wikilink")、芦笋等。

高技术行业方面，有[中国中车集团旗下的](../Page/中国中车集团.md "wikilink")[永济电机公司](../Page/永济电机.md "wikilink")，负责供应[牵引电动机予全国铁路车辆](../Page/牵引电动机.md "wikilink")。

## 物产

  - 矿产有磷、石英砂、石灰岩、白云石、铁、铅锌、金、铜、红玛瑙等。
  - 野生动物有野猪、野兔、野羊等。
  - 土特产主要有桑落酒、蒲柿、黄河大鲤鱼。

## 人口

2010年第六次全国人口普查44.4724万人。\[1\]

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[蒲津渡与](../Page/蒲津渡.md "wikilink")[蒲州故城遗址](../Page/蒲州故城遗址.md "wikilink")，蒲津渡为古代黄河上的著名渡口，其附近曾发现四尊每个重达60吨的[唐代铁牛](../Page/唐代.md "wikilink")，是中国现存最大的铁铸文物。
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[石庄遗址](../Page/石庄遗址.md "wikilink")、[高市村汉墓群](../Page/高市村汉墓群.md "wikilink")、[小朝村汉墓群](../Page/小朝村汉墓群.md "wikilink")、[杨博墓](../Page/杨博墓.md "wikilink")、[万固寺](../Page/万固寺.md "wikilink")、[普救寺塔](../Page/普救寺塔.md "wikilink")、[韩楫墓](../Page/韩楫墓.md "wikilink")、[栖岩寺塔群](../Page/栖岩寺塔群.md "wikilink")、[赵杏古墓群](../Page/赵杏古墓群.md "wikilink")、[叔夷伯齐墓](../Page/叔夷伯齐墓.md "wikilink")、[扁鹊庙](../Page/扁鹊庙.md "wikilink")、[解梁古城](../Page/解梁古城.md "wikilink")、[孟桐墓](../Page/孟桐墓.md "wikilink")、[杨瞻墓](../Page/杨瞻墓.md "wikilink")、[董村戏台](../Page/董村戏台.md "wikilink")
  - [永济市文物保护单位](../Page/永济市文物保护单位.md "wikilink")
  - [五老峰](../Page/五老峰.md "wikilink")：[道教名山](../Page/道教.md "wikilink")，史称东华山。它和附近的古蒲州共同构成了一处[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")。
  - [鹳雀楼](../Page/鹳雀楼.md "wikilink")：古代名楼，始建于五代，因[王之涣的](../Page/王之涣.md "wikilink")《登鹳雀楼》闻名。最近一次毁于清朝，后于1997年重建。

## 名人

[杨贵妃](../Page/杨贵妃.md "wikilink")、[柳宗元](../Page/柳宗元.md "wikilink")、[马远](../Page/马远.md "wikilink")

## 参考文献

[永济市](../Category/永济市.md "wikilink")
[Category:运城区县市](../Category/运城区县市.md "wikilink")
[Category:山西县级市](../Category/山西县级市.md "wikilink")
[晋](../Category/中国小城市.md "wikilink")

1.  [运城市第六次全国人口普查主要数据公报](http://www.sxycrb.com/html/55/n-33455.html)