[GSI_CKT20092-C58-19_20090427.jpg](https://zh.wikipedia.org/wiki/File:GSI_CKT20092-C58-19_20090427.jpg "fig:GSI_CKT20092-C58-19_20090427.jpg")（東北側）、[赤坂](../Page/赤坂.md "wikilink")（西側）、[虎之門](../Page/虎之門.md "wikilink")（南側）一帶空拍圖\]\]

**永田町**（）是[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[千代田區南端的地名](../Page/千代田區.md "wikilink")。[國會議事堂](../Page/國會議事堂.md "wikilink")、[國立國會圖書館](../Page/國立國會圖書館.md "wikilink")、[總理大臣官邸](../Page/總理大臣官邸.md "wikilink")（日本首相府）、、、[自由民主黨本部](../Page/自由民主黨_\(日本\).md "wikilink")、[民進黨本部](../Page/民進黨_\(日本\).md "wikilink")、[社會民主黨本部](../Page/社會民主黨_\(日本\).md "wikilink")、[日枝神社](../Page/日枝神社_\(千代田區\).md "wikilink")、、、所在地，日本國家政治的中樞地區。

由於[日本首相的辦公室與寓所都位於永田町](../Page/日本首相.md "wikilink")，有時外界會以「永田町之町長」來揶揄日本首相。

## 沿革

鄰近[江戶城](../Page/江戶城.md "wikilink")，因此[大名宅邸林立](../Page/大名_\(稱謂\).md "wikilink")。2003年舊總理大臣官邸重建工程時，發現内藤家的屋敷遺跡。[日枝神社周邊是賞櫻花的著名地點](../Page/日枝神社_\(千代田區\).md "wikilink")。

[明治時代在此設立](../Page/明治.md "wikilink")[陸軍省](../Page/陸軍省.md "wikilink")。當時的「永田町」一般可以指陸軍參謀本部。1936年國會議事堂完成後政治機能集中於此，「永田町」成為政界的代名詞。1982年發生大火災的、因[226事件而有名的](../Page/226事件.md "wikilink")今日已不存在。

### 地名由來

江戶初期，此地有永田姓的屋敷，因此稱作「永田馬場」，後演變為「永田町」\[1\]。

### 町名變遷

| 實施後                               | 實施年月日              | 實施前（未備註表各町部分地區）                   |
| --------------------------------- | ------------------ | --------------------------------- |
| 永田町一丁目                            | 1967年4月1日          | 永田町一丁目（大部分）、三年町、霞關一丁目、霞關二丁目、霞關三丁目 |
| 永田町二丁目                            | 永田町一丁目、永田町二丁目（大部分） |                                   |
| [霞關三丁目](../Page/霞關.md "wikilink") | 霞關三丁目、三年町、永田町二丁目   |                                   |

## 設施、建築

本章節僅列一部分

  - [國會議事堂](../Page/國會議事堂.md "wikilink")－[眾議院](../Page/日本眾議院.md "wikilink")、[參議院](../Page/參議院_\(日本\).md "wikilink")

  - [國立國會圖書館](../Page/國立國會圖書館.md "wikilink")

  - [里索那銀行參議院](../Page/里索那銀行.md "wikilink")、眾議院支店

  -
  - [總理大臣官邸](../Page/總理大臣官邸.md "wikilink")、

  - －官邸前

  - [日枝神社](../Page/日枝神社_\(千代田區\).md "wikilink")

  - 、

  -
  - [民進黨本部](../Page/民進黨_\(日本\).md "wikilink")（三宅坂大樓）

  - [社會民主黨本部](../Page/社會民主黨_\(日本\).md "wikilink")

  - [自由民主黨本部](../Page/自由民主黨_\(日本\).md "wikilink")

  - [內閣府廳舍](../Page/內閣府.md "wikilink")

  -
  -
  - 山王飯店

  -
  -
  -
  - [山王公園塔](../Page/山王公園塔.md "wikilink")

  - [墨西哥大使館](https://web.archive.org/web/20120705220426/http://portal.sre.gob.mx/japonj/)

<File:Diet> of Japan Kokkai
2009.jpg|[國會議事堂](../Page/國會議事堂.md "wikilink")
<File:National> diet library
2009.jpg|[國立國會圖書館](../Page/國立國會圖書館.md "wikilink")
<File:Soridaijinkantei2.jpg>|[總理大臣官邸](../Page/總理大臣官邸.md "wikilink")
<File:Hie-jinja> torii.jpg|[日枝神社](../Page/日枝神社_\(千代田区\).md "wikilink")
<File:Japan> LDP HQ.jpg|[自由民主黨本部](../Page/自由民主黨_\(日本\).md "wikilink")
<File:Democratic> Party of Japan
headquarters.jpg|[民進黨本部](../Page/民進黨_\(日本\).md "wikilink")
<File:Shakai-Minshu-to.jpg>|[社會民主黨本部](../Page/社會民主黨_\(日本\).md "wikilink")
<File:Hibiya> High School.jpg|
<File:Sannnouparkyoko.JPG>|[山王公园塔](../Page/山王公园塔.md "wikilink")

## 交通

  - [Subway_TokyoMarunouchi.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoMarunouchi.png "fig:Subway_TokyoMarunouchi.png")[丸之内線](../Page/東京地下鐵丸之内線.md "wikilink")（[國會議事堂前站](../Page/國會議事堂前站.md "wikilink")、[赤坂見附站](../Page/赤坂見附站.md "wikilink")）
  - [Subway_TokyoChiyoda.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoChiyoda.png "fig:Subway_TokyoChiyoda.png")[千代田線](../Page/東京地下鐵千代田線.md "wikilink")（國會議事堂前站）
  - [Subway_TokyoGinza.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoGinza.png "fig:Subway_TokyoGinza.png")[銀座線](../Page/東京地下鐵銀座線.md "wikilink")（[溜池山王站](../Page/溜池山王站.md "wikilink")、赤坂見附站）
  - [Subway_TokyoYurakucho.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoYurakucho.png "fig:Subway_TokyoYurakucho.png")[有樂町線](../Page/東京地下鐵有樂町線.md "wikilink")（[永田町站](../Page/永田町站.md "wikilink")）
  - [Subway_TokyoHanzomon.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHanzomon.png "fig:Subway_TokyoHanzomon.png")[半藏門線](../Page/東京地下鐵半藏門線.md "wikilink")（永田町站）
  - [Subway_TokyoNamboku.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoNamboku.png "fig:Subway_TokyoNamboku.png")[南北線](../Page/東京地下鐵南北線.md "wikilink")（溜池山王站、永田町站）

## 備註

## 關連項目

  - [永田町站](../Page/永田町站.md "wikilink")

[\*](../Page/category:永田町.md "wikilink")

[Category:東京都區部地區](../Category/東京都區部地區.md "wikilink")
[Category:千代田區町名](../Category/千代田區町名.md "wikilink")

1.  [千代田区
    町名由来板ガイド：永田町一丁目（ながたちょういっちょうめ）](http://www.city.chiyoda.lg.jp/koho/bunka/bunka/chome/yurai/nagata-1.html)