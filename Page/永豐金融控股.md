[SinoPac_Holdings_Co.2017-6.JPG](https://zh.wikipedia.org/wiki/File:SinoPac_Holdings_Co.2017-6.JPG "fig:SinoPac_Holdings_Co.2017-6.JPG")
**永豐金融控股股份有限公司**（英語：SinoPac Financial Holdings Co.,
Ltd.，簡稱：**永豐金控**、**永豐金**）是一家總部位於[臺灣的大型](../Page/臺灣.md "wikilink")[金融機構](../Page/金融机构.md "wikilink")，其淨值逾1,400億元，總市值逾1,200億元，其中國際專業投資機構持股比例約為三成；其[資產總價值超過新臺幣](../Page/資產.md "wikilink")1.6兆元，銀行據點達127處，客戶總數超過250萬戶。

其旗下包括[銀行](../Page/銀行.md "wikilink")、[證券](../Page/證券.md "wikilink")、投資信託、客服科技、保險代理、創業投資、期貨及租賃等金融產業部門；在[南京](../Page/南京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[美國](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")、[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")、[越南設有分支機構或辦事處](../Page/越南.md "wikilink")，並於[美國](../Page/美國.md "wikilink")[加州曾擁有全資銀行](../Page/加州.md "wikilink")「遠東國民銀行」，於2016年7月簽約出售\[1\]，2017年7月獲得台灣金管會核准此案\[2\]。

## 發展歷史

  - 2002年5月9日，由[華信商業銀行](../Page/華信商業銀行.md "wikilink")、[建弘證券](../Page/建弘證券.md "wikilink")、[金華信銀證券共同以股份轉換方式成立](../Page/金華信銀證券.md "wikilink")「**建華金融控股公司**」\[3\]（名稱取字「建」弘證券和「華」信銀行的組合）\[4\]，同日股票掛牌上市。同年6月20日將旗下的華信商業銀行更名為「[建華商業銀行](../Page/建華商業銀行.md "wikilink")」，7月22日將「建弘證券」與「金華信銀證券」合併，以建弘證券為存續公司。\[5\]

<!-- end list -->

  - 2003年，建華金控收購原由華信銀行與[ING集團合資成立的](../Page/ING集團.md "wikilink")「**安泰信信用卡**」的股權後，將其納入金控子公司，並更名為「[安信信用卡](../Page/安信信用卡.md "wikilink")」。

<!-- end list -->

  - 2005年，建華金控與[台北國際商業銀行決定合併](../Page/台北國際商業銀行.md "wikilink")，12月26日，台北國際商業銀行以換股方式成為建華金控百分之百持股的子公司\[6\]。合併後改由台北商銀最大股東[永豐餘集團主導營運](../Page/永豐餘集團.md "wikilink")，最初曾計畫在合併後改名為「**台北華信金融控股公司**」\[7\]，但在2006年決定更名為「**永豐金融控股公司**」，同年11月13日旗下兩家銀行建華商業銀行和台北國際商業銀行合併，合併後以建華商業銀行成為存續公司，並配合金控之新名稱更名為「[永豐商業銀行](../Page/永豐商業銀行.md "wikilink")」，而金控之子公司安信信用卡和建弘證券也更名為「[永豐信用卡](../Page/永豐信用卡.md "wikilink")」和「[永豐金證券](../Page/永豐金證券.md "wikilink")」。\[8\]

<!-- end list -->

  - 2009年6月1日將旗下的永豐信用卡併入永豐銀行。
  - 2015年，原始股東[潤泰企業集團逐步出脫持股淡出永豐金董事會](../Page/潤泰企業集團.md "wikilink")，目前已非前十大股東之一\[9\]。
  - 2016年7月，永豐金控代旗下子公司宣布:永豐銀行出售位於美國加州的遠東國民銀行(簡稱FENB)，交易金額114億元台幣，預計收益達20.62億元台幣\[10\]。
  - 2017年，由於永豐金過去一段期間，家族內鬨，放話賣股，致使集團動用相關資源持續買股護盤，以穩住年度董監改選行情<ref>楊筱筠，\[永豐餘動員
    回補自家金控股

<https://money.udn.com/money/story/5613/2402313>` ``]，經濟日報，2017-04-14`</ref>`，再者，由於內部管控疏失，弊案連連，致使其在`[`臺灣證券交易所所公布的公司治理評鑑排名上大幅退步`](../Page/臺灣證券交易所.md "wikilink")\[11\]`，總經理兼財務長暨永豐銀董事長游國治，與永豐金租賃總經理莊耀、永豐金證券（亞洲）總經理蔡建志等三人為內控不當請辭負責`\[12\]`，永豐金控董事長`[`何壽川也因涉及對三寶建設超貸的弊案`](../Page/何壽川.md "wikilink")`，遭到檢調單位約談，並收押且遭金管會解職`\[13\]\[14\]`，由前財政部長`[`邱正雄擔任董事長`](../Page/邱正雄.md "wikilink")`，後因其角色具有爭議性，故於兩週內請辭，成為臺灣有史以來，任期最短的金控公司董事長`\[15\]`，最後則是以前`[`中華郵政董事長翁文祺臨危受命`](../Page/中華郵政.md "wikilink")`，接任金控董座`\[16\]`。`

## 爭議事件

  - 2009年，永豐餘集團旗下[永豐金證券承銷](../Page/永豐金證券.md "wikilink")[頂新國際集團旗下](../Page/頂新國際集團.md "wikilink")[康師傅控股](../Page/康師傅控股.md "wikilink")[TDR](../Page/TDR.md "wikilink")。\[17\][頂新集團黑心油事件後](../Page/頂新國際集團#劣質油品事件.md "wikilink")，被查出購買帝寶十戶僅付1%自備款，其餘99%皆為銀行貸款\[18\]\[19\]，其中永豐銀貸款給頂新魏家二億七千萬。\[20\]

<!-- end list -->

  - 2016年，爆發鼎興牙材向銀行詐貸案，其中永豐金控旗下的永豐銀行對三等姻親授信（鼎興集團負責人何宗英，為永豐前董事何宗達姨丈），卻沒有提報董事會議決，而被懷疑有違反利害關係人交易及內部授信流程等缺失，被金管會重懲\[21\]，因此並導致整個金控集團人事大搬風\[22\]。

<!-- end list -->

  - 2017年，金管會金檢永豐金於2016年對[三寶建設超貸案](../Page/三寶建設.md "wikilink")，經查結果確有缺失，並予以重懲\[23\]。這主要是有該公司員工扮演[吹哨者的角色向金管會投訴](../Page/吹哨者.md "wikilink")：永豐金旗下的租賃公司涉超貸給三寶建設新台幣四十多億元，三寶建設旗下其中有一家海外紙上公司是與永豐金控集團有關聯的[永豐餘和](../Page/永豐餘.md "wikilink")[元太科技所轉投資](../Page/元太科技.md "wikilink")，被人質疑永豐餘自己先投資三寶建設，把資金部位先建立好，再要永豐金控對三寶建設放款，而被懷疑是圖利永豐金何家，把永豐金控當成私人金庫的行為\[24\]\[25\]\[26\]。

<!-- end list -->

  - 永豐金控旗下的[永豐金證券香港子公司](../Page/永豐金證券.md "wikilink")，借了約[港幣](../Page/港幣.md "wikilink")3.4億元（約新台幣13億元）給客戶，去買輝山股票，其中九成還是用輝山股票當擔保品，永豐金證券總經理也因監管不周而遭停職\[27\]，而這家在香港上市的[輝山乳業](../Page/輝山乳業.md "wikilink")，2017年因被踢爆作假帳及掏空，導致股價從港幣2.81元狂洩至0.42元，最後被停止交易。法人表示，若輝山股票最後變「壁紙」，這筆客戶融資恐收不回來，屆時變成呆帳，對永豐金控獲利將帶來巨大衝擊\[28\]。

<!-- end list -->

  - 2017年4月[永豐金證券香港子公司涉及非法引進中資上海龍峰集團大舉買進](../Page/永豐金證券.md "wikilink")[大同公司](../Page/大同公司.md "wikilink")
    (2371-TW) 股票，並介入經營權之爭，被人舉報而遭金融監督管理委員會裁罰\[29\]。

## 金控成員及其關係

{{ familytree/start }} {{ familytree | level0 |v| level1 |v| level2 |
level0 = **永豐金融控股** | level1 = [永豐商業銀行](../Page/永豐商業銀行.md "wikilink") |
level2 = 永豐銀行（中國）}} {{ familytree | | | |\!| | | |\!| }} {{ familytree
| | | |\!| | | |)| level2 |-| level3 |-| level4 | level2 = 永豐金（香港）財務 |
level3 = SinoPac Capital (BVI) | level4 = 華訊資訊服務 }} {{ familytree | | |
|\!| | | |\!| | | }} {{ familytree | | | |\!| | | |)| level2 | level2 =
永豐人身保險代理人 }} {{ familytree | | | |\!| | | |\!| }} {{ familytree | | |
|\!| | | |)| level2 | level2 = 永豐金財產保險代理人 }} {{ familytree | | | |\!| |
| |\!| }} {{ familytree | | | |\!| | | |\`| level2 | level2 =永豐（香港）保險經紀
}} {{ familytree | | | |\!| }} {{ familytree | | | |)| level1 |v| level2
|,| level3 | level1 = [永豐金證券](../Page/永豐金證券.md "wikilink") | level2 =
永豐期貨 | level3 = 永豐金資產管理（亞洲）}} {{ familytree | | | |\!| | | |\!| |
| |\!| }} {{ familytree | | | |\!| | | |)| level2 |)| level3 |,| level4
| level2 = 永豐證券投資顧問 | level3 = 永豐金證券（歐洲） | level4 = 永豐金金融服務 }} {{
familytree | | | |\!| | | |\!| | | |\!| | | |\!| }} {{ familytree | | |
|\!| | | |)| level2 |+| level3 |+| level4 | level2 = 永豐金證券（開曼）控股 |
level3 = 永豐金證券（亞洲） | level4 = 永豐金(亞洲)代理人 }} {{ familytree | | | |\!| | |
|\!| |||\!|| | |\!| }} {{ familytree | | | |)| level1 |\`| level2 |\`|
level3 |\`| level4 | level1 = [永豐客服科技](../Page/永豐客服科技.md "wikilink") |
level2 = 永豐金財務諮詢（上海）| level3 = 永豐金國際控股 | level4 = 永豐金資本（亞洲） }} {{
familytree | | | |\!| }} {{ familytree | | | |)| level1 |-| level2 |
level1 = [永豐創業投資](../Page/永豐創業投資.md "wikilink") | level2 = 匯川創業投資 }} {{
familytree | | | |\!| }} {{ familytree | | | |)| level1 |,| level2 |
level1 = [永豐證券投資信託](../Page/永豐證券投資信託.md "wikilink") | level2 = SinoPac
Capital International Limited }} {{ familytree | | | |\!| | | |\!| }} {{
familytree | | | |\`| level1 |+| level2 | level1 =
[永豐金租賃](../Page/永豐金租賃.md "wikilink") | level2 =
永豐金國際租貸 }} {{ familytree | | | | | | | |\!| }} {{ familytree |
| | | | | | |\`| level2 | level2 = 永豐金融資租賃（天津） }} {{ familytree/end }}

## 參考資料

## 外部連結

  - [永豐金控](http://www.sinopac.com)
      - [永豐銀行](http://bank.sinopac.com)
      - [永豐金證券](http://securities.sinopac.com/)
      - [永豐投信](http://sitc.sinopac.com/)
      - [永豐金租賃](http://spl.sinopac.com/)

[Y](../Category/臺灣的金融控股公司.md "wikilink")
[2](../Category/臺灣證券交易所上市公司.md "wikilink")
[Y永](../Category/總部位於臺北市中山區的工商業機構.md "wikilink")
[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")
[永豐金控](../Category/永豐金控.md "wikilink")
[Category:2002年台灣建立](../Category/2002年台灣建立.md "wikilink")

1.  [熱門股－永豐金 外資連2砍 走向跌破票面價的貼權息路](http://times.hinet.net/news/19234166)

2.  陳慧菱，[永豐金出售美國子行
    金管會准了](http://news.cnyes.com/news/id/3860725)，鉅亨網，2017-07-07

3.

4.

5.

6.

7.
8.

9.  [永豐金前十大股東大搬風　尹衍樑賣、壽險股撿《ETtoday財經新聞》](http://www.ettoday.net/news/20150504/501924.htm)

10. [永豐銀擬售美國子行，獲准後可認列20.6億元獲利《Money
    DJ理財新聞網》](http://www.moneydj.com/KMDJ/News/NewsViewer.aspx?a=695fdfcc-6aec-46bb-8343-29f2be00afcd)

11. `陳永吉，`[`弊案纏身``
     ``中信、兆豐、永豐三金控公司治理評鑑大退步`](http://news.ltn.com.tw/news/business/breakingnews/2036459)`，自由時報，2017-04-14`

12. `陳瑩欣、廖珮君，`[`永豐金高層請辭負責　游國治等3總經理下台`](http://www.appledaily.com.tw/realtimenews/article/new/20170525/1125805/)`，蘋果日報，2017-05-25`

13. `陳一姍、盧沛樺，`[`永豐金控董事長何壽川遭收押``
     ``百年何家的最大劫難`](http://www.cw.com.tw/article/article.action?id=5083183)`，天下雜誌，2017-06-17`

14. [`永豐金董座何壽川遭金管會解職`](http://www.nownews.com/n/2017/06/19/2569954)

15. `沈婉玉，`[`永豐金董事長``
     ``邱正雄火速下台`](https://udn.com/news/story/7239/2548228)`，聯合新聞網，2017-06-27`

16. `魏喬怡、曾薏蘋，`[`二度救火邱正雄接永豐金董座`](http://www.chinatimes.com/newspapers/20170622000089-260205)`，中時電子報，2017-06-22`

17. 蔣永佑; 呂志明; 張嘉伶. [給頂新8600萬佣金
    永豐金遭查](http://www.appledaily.com.tw/appledaily/article/headline/20120406/34141060).
    蘋果日報. 2012-04-06.

18.

19.

20. 徐嶔煌.[如果不是你，那誰才是頂新的守門神？](http://opinion.udn.com/opinion/story/6807/451258-%E5%A6%82%E6%9E%9C%E4%B8%8D%E6%98%AF%E4%BD%A0%EF%BC%8C%E9%82%A3%E8%AA%B0%E6%89%8D%E6%98%AF%E9%A0%82%E6%96%B0%E7%9A%84%E5%AE%88%E9%96%80%E7%A5%9E%EF%BC%9F).聯合報，
    2014-11-03

21. 廖珮君、陳瑩欣，[鼎興牙材詐貸案
    永豐銀挨罰1000萬](http://www.appledaily.com.tw/appledaily/article/finance/20161109/37445428/)，蘋果日報，2016-11-09

22. 孫彬訓、陳欣文，[詐貸延燒 永豐金＋銀
    高層大搬風](http://www.chinatimes.com/newspapers/20161126000068-260202)，中時電子報，2016-11-26

23. 魏喬怡、彭禎伶，[超貸三寶建設
    永豐金遭重罰1千萬](http://www.chinatimes.com/newspapers/20170412000065-260202)，中時電子報，2017-04-12

24. 王孟倫，[永豐金涉超貸案 1個月內決定是否核處](http://news.ltn.com.tw/news/business/paper/1084237)，自由時報，2017-03-09

25. 韓化宇，[黑函滿天飛
    永豐金又被檢舉掏空](https://udn.com/news/story/6/2178690?from=udn-referralnews_ch2artbottom)，經濟日報，2016-12-19

26. 彭禎伶，[三寶授信案，永豐金下周恐再吃罰單](https://ctee.com.tw/News/ViewCateNews.aspx?newsid=145385&cateid=jrdc)，工商時報，2014-04-05

27. 彭禎玲，[永豐金證總座葉黃杞
    遭停職](http://www.chinatimes.com/newspapers/20170620000052-260202)，2017-06-20，中時電子報

28. 廖珮君、林韋伶、劉煥彥，[踩雷輝山乳業
    永豐金證重傷](http://www.appledaily.com.tw/appledaily/article/finance/20170411/37613314/)，蘋果日報，2017-04-11

29. [永豐金又見吹哨者　香港員工爆大同炒股神秘中資](http://www.nextmag.com.tw/realtimenews/news/321106)，壹週刊，2017-07-12