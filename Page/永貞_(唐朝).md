**永貞**（805年八月－十二月）是[唐順宗的年號](../Page/唐順宗.md "wikilink")。

## 大事記

## 出生

## 逝世

**永貞元年**

  - [唐順宗](../Page/唐順宗.md "wikilink")

## 紀年

| 永貞                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 805年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") |

## 參看

  - [永贞革新](../Page/永贞革新.md "wikilink")
  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他使用[永貞年號的政權](../Page/永貞.md "wikilink")
  - 同期存在的其他政权年号
      - [延曆](../Page/延曆.md "wikilink")（782年八月十九日—806年五月十八日）：[平安時代](../Page/平安時代.md "wikilink")[桓武天皇之年號](../Page/桓武天皇.md "wikilink")
      - [正曆](../Page/正曆_\(大嵩璘\).md "wikilink")（795年—809年）：渤海康王[大嵩璘之年號](../Page/大嵩璘.md "wikilink")
      - [上元](../Page/上元_\(異牟尋\).md "wikilink")（784年起）：[南詔領袖](../Page/南詔.md "wikilink")[異牟尋之年號](../Page/異牟尋.md "wikilink")
      - [元封](../Page/元封_\(異牟尋\).md "wikilink")（至808年）：南詔領袖異牟尋之年號

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:800年代中国政治](../Category/800年代中国政治.md "wikilink")