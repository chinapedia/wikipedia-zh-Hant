[Taipei_art.JPG](https://zh.wikipedia.org/wiki/File:Taipei_art.JPG "fig:Taipei_art.JPG")
**氹仔市政花園**（）是位於[澳門](../Page/澳門.md "wikilink")[氹仔島上的](../Page/氹仔島.md "wikilink")[公園](../Page/公園.md "wikilink")，於1955年落成。氹仔市政花園為[澳門八景之](../Page/澳門八景.md "wikilink")「龍環葡韻」景區的一部分，花園所在地區的歷史價值，已為[澳門政府所評定](../Page/澳門政府.md "wikilink")。

## 地理位置

氹仔市政花園位於澳門氹仔島上的[嘉路士米耶馬路](../Page/嘉路士米耶馬路.md "wikilink")，佔地總面積約3500平方公尺。花園順山勢伸沿，從[旅遊景點](../Page/旅遊.md "wikilink")[嘉模聖母堂伸延至](../Page/嘉模聖母堂.md "wikilink")[龍環葡韻住宅式博物館](../Page/龍環葡韻住宅式博物館.md "wikilink")，公園小路接連[氹仔市區](../Page/氹仔市區.md "wikilink")。另外，旁邊的文娛設施有[氹仔市政圖書館和](../Page/氹仔市政圖書館.md "wikilink")[嘉模泳池](../Page/嘉模泳池.md "wikilink")。

## 特色

氹仔市政花園的主要特色是[賈梅士的](../Page/賈梅士.md "wikilink")[雕像](../Page/雕像.md "wikilink")、小[噴泉與外圍蔭棚](../Page/噴泉.md "wikilink")。花園的最高處豎立了[葡萄牙著名](../Page/葡萄牙.md "wikilink")[詩人賈梅士之雕像](../Page/詩人.md "wikilink")；另小噴泉的水池形狀為獨特的十字型花瓣與圓形外圍蔭棚上種植了多種藤本植物，都是值得欣賞之特色。

## 參考來源

  - [澳門文物知多少：市政花園(氹仔)](https://web.archive.org/web/20070318111903/http://www.macaoheritage.net/info/pointC.asp?pId=13&id=135&k=&i=)

## 外部連結

  - [澳門自然網：氹仔市政花園簡介](https://nature.iacm.gov.mo/c/park/detail.aspx?id=a1215ae2-6c94-4841-80bd-b8028aa35b82)

[Category:澳門公園](../Category/澳門公園.md "wikilink")
[Category:澳門西式庭園](../Category/澳門西式庭園.md "wikilink")