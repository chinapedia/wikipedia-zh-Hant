[Ting_Kok_2018.jpg](https://zh.wikipedia.org/wiki/File:Ting_Kok_2018.jpg "fig:Ting_Kok_2018.jpg")的山腳\]\]
**汀角**（），為[香港的一個地方](../Page/香港.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[大埔區內](../Page/大埔區.md "wikilink")，處[船灣海北岸](../Page/船灣海.md "wikilink")，在[布心排與](../Page/布心排.md "wikilink")[大尾篤之間一帶](../Page/大尾篤.md "wikilink")。

坐落在[八仙嶺的山腳](../Page/八仙嶺.md "wikilink")，附近有很多燒烤場，所以假日，尤其星期六有不少慕名而來遊樂的市民在此站下車，前往就近的燒烤場該地為[汀角村的所在地](../Page/汀角村.md "wikilink")。而汀角的海邊也是[紅樹林棲身之地](../Page/紅樹林.md "wikilink")，[汀角攀樹蟹於該處被發現](../Page/汀角攀樹蟹.md "wikilink")，亦以該地名命名\[1\]。

## 歷史

[漁農自然護理署於](../Page/漁農自然護理署.md "wikilink")1985年3月將汀角選為[具特殊科學價值地點](../Page/具特殊科學價值地點.md "wikilink")、是[香港第四大](../Page/香港.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")。包括[欖李](../Page/欖李.md "wikilink")、[白骨壤](../Page/白骨壤.md "wikilink")、[桐花樹](../Page/桐花樹.md "wikilink")、[秋茄](../Page/秋茄.md "wikilink")、[露兜樹](../Page/露兜樹.md "wikilink")、[黃槿等海邊](../Page/黃槿.md "wikilink")[樹木和](../Page/樹木.md "wikilink")[紅樹皆可在此發現](../Page/紅樹.md "wikilink")。

有發展商計劃在汀角發展水療度假酒店，除擬於地盤東面興建酒店住宿、水療設施、餐飲及零售設施外，更計劃在東面臨近海邊位置，設置一所以舉辦婚禮為主題的特色教堂，以配合已落實興建的[龍尾泳灘](../Page/龍尾泳灘.md "wikilink")。該發展計劃地盤面積約61,000平方米，地積比率將不多於0.4倍，酒店房間不多於230間，建築物高度不多於3層。大埔區議員[任啟邦認同水療酒店發展計劃對區內就業旅遊有好處](../Page/任啟邦.md "wikilink")，但擔心新發展對鄰近的紅樹木有影響，亦憂慮區內交通配套不勝負荷。

## 交通

  - [汀角路](../Page/汀角路.md "wikilink")

## 區議會議席分佈

由於汀角人口稀少，所以往往跟[三門仔](../Page/三門仔.md "wikilink")、[洞梓](../Page/洞梓.md "wikilink")、[船灣](../Page/船灣.md "wikilink")、[大尾篤等鄉村範圍劃為同一個選區](../Page/大尾篤.md "wikilink")。

| 年度/範圍 | 2000-2003                                   | 2004-2007 | 2008-2011 | 2012-2015 | 2016-2019 | 2020-2023 |
| ----- | ------------------------------------------- | --------- | --------- | --------- | --------- | --------- |
| 整個汀角  | {{[船灣選區](../Page/船灣_\(選區\).md "wikilink")}} |           |           |           |           |           |

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參考資料

[Category:大埔區](../Category/大埔區.md "wikilink")
[Category:具特殊科學價值地點](../Category/具特殊科學價值地點.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")

1.  [ScienceDaily](https://www.sciencedaily.com/releases/2017/03/170321122557.htm)
    Pensoft Publishers. 2017, March 21. Retrieved April 11, 2017.