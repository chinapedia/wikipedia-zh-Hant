**汉中市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[陕西省下辖的](../Page/陕西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于陕西省西南部。[中国历史文化名城之一](../Page/中国历史文化名城.md "wikilink")，中国国家生态示范建设试点地区。经济以[农业](../Page/农业.md "wikilink")、[旅游业](../Page/旅游业.md "wikilink")、[采矿业为主](../Page/采矿业.md "wikilink")，工业产值较低。与[甘肃](../Page/甘肃.md "wikilink")、[四川毗邻](../Page/四川.md "wikilink")。北依[秦岭](../Page/秦岭.md "wikilink")，南屏[大巴山](../Page/大巴山.md "wikilink")，中部为[盆地](../Page/盆地.md "wikilink")。全市辖[汉台区](../Page/汉台区.md "wikilink")、[南郑区](../Page/南郑区.md "wikilink")、[城固县](../Page/城固县.md "wikilink")、[洋县](../Page/洋县.md "wikilink")、[勉县](../Page/勉县.md "wikilink")（1964年前旧名“沔縣”）、[西乡县](../Page/西乡县.md "wikilink")、[略阳县](../Page/略阳县.md "wikilink")、[镇巴县](../Page/镇巴县.md "wikilink")、[宁强县](../Page/宁强县.md "wikilink")、[留坝县](../Page/留坝县.md "wikilink")、[佛坪县](../Page/佛坪县.md "wikilink")，总面积2.72万[平方公里](../Page/平方公里.md "wikilink")，户籍人口386万（2016年）\[1\]。地方[方言为](../Page/方言.md "wikilink")[汉中话](../Page/汉中话.md "wikilink")，属[西南官话的](../Page/西南官话.md "wikilink")[成渝片](../Page/成渝片.md "wikilink")。

## 历史

### 先秦

[先秦时期](../Page/先秦时期.md "wikilink")，汉中地区在[传说和](../Page/传说.md "wikilink")[史料中皆有部分涉及](../Page/史料.md "wikilink")，但由于年代久远，均无法查证。《[尚书](../Page/尚书.md "wikilink")·禹贡》中所谓“[梁州](../Page/梁州.md "wikilink")”、《[史记](../Page/史记.md "wikilink")》中“褒国”皆被认为是汉中地区在史料和传说中的体现。南郑之名，可上溯至公元前771年。《[水经注](../Page/水经注.md "wikilink")》载：“南郑之号，始于[郑桓公](../Page/郑桓公.md "wikilink")。桓公死于[犬戎](../Page/犬戎.md "wikilink")，其民南奔，故以南郑称。”\[2\]
但此说史家存疑。\[3\]
[战国中期](../Page/战国.md "wikilink")，南郑是秦蜀争夺的要地。[秦厉公](../Page/秦厲公.md "wikilink")26年（公元前451年），左庶长修筑南郑城\[4\]。

### 秦汉

[虎头桥遗址_碑刻.jpg](https://zh.wikipedia.org/wiki/File:虎头桥遗址_碑刻.jpg "fig:虎头桥遗址_碑刻.jpg")
秦漢時期漢中郡西起沔陽[陽平關](../Page/陽平關.md "wikilink")，東至鄖關和荊山，綿延千里\[5\]。[秦朝时设](../Page/秦朝.md "wikilink")[漢中郡](../Page/漢中郡.md "wikilink")\[6\]，郡治南郑，在今天漢中市南郑县附近。秦朝末年，各股政治势力角逐中国大地，[鸿门宴之后](../Page/鸿门宴.md "wikilink")，[劉邦向](../Page/劉邦.md "wikilink")[项羽称臣](../Page/项羽.md "wikilink")，项羽封其为漢王，《史记》中记载，劉邦颇为失落，谋士[萧何劝慰](../Page/萧何.md "wikilink")：“语曰‘天漢’，其称甚美”\[7\]。劉邦在漢中时期韬光养晦，采用[韩信](../Page/韩信.md "wikilink")“明修[栈道](../Page/栈道.md "wikilink")，暗度[陈仓](../Page/寶雞市.md "wikilink")”的策略，拜韩信为大将，后突袭拿下[三秦地区](../Page/三秦.md "wikilink")，和项羽一争高低，史称“[楚漢之爭](../Page/楚漢之爭.md "wikilink")”。

劉邦最终取得军事胜利，因其原封地在漢中，称漢王，故迁都[长安并建立中央统一](../Page/长安.md "wikilink")[封建王朝后](../Page/封建王朝.md "wikilink")，國號称“[漢朝](../Page/漢朝.md "wikilink")”，劉邦亦被称作「[漢太祖](../Page/漢太祖.md "wikilink")」\[8\]。今日中国“[漢族](../Page/漢族.md "wikilink")”即得名于漢朝，漢中乃中华漢人称号的古发源地，并留下大量漢朝时期[文物](../Page/文物.md "wikilink")[古迹](../Page/古迹.md "wikilink")，如[拜将坛](../Page/拜将坛.md "wikilink")（劉邦拜韩信处）、[漢台](../Page/漢台.md "wikilink")、栈道等。[西漢](../Page/西漢.md "wikilink")[武帝时期](../Page/漢武帝.md "wikilink")，城固人[张骞出使](../Page/张骞.md "wikilink")[西域](../Page/西域.md "wikilink")，成为“[丝绸之路](../Page/丝绸之路.md "wikilink")”兴起的标志事件。

[东漢末年政局动荡](../Page/东漢.md "wikilink")，[巴郡](../Page/巴郡.md "wikilink")[少数民族起事](../Page/少数民族.md "wikilink")，攻入漢中、三蜀。[中平元年](../Page/中平.md "wikilink")，[五斗米道](../Page/五斗米道.md "wikilink")[张修叛亂](../Page/张修.md "wikilink")，响应[黄巾軍](../Page/黃巾軍.md "wikilink")，攻巴郡、入漢中，兵败归附[刘焉](../Page/刘焉.md "wikilink")。[熹平年间](../Page/熹平.md "wikilink")（172－178年）五斗米道在漢中传播。\[9\]\[10\]
后来[张鲁除掉张修](../Page/张鲁.md "wikilink")，在此建立持续达近三十年的[政教合一的割據](../Page/政教合一.md "wikilink")[政权](../Page/政权.md "wikilink")，后依附[曹操](../Page/曹操.md "wikilink")。

由于其入蜀门户的特殊地理位置和南北交通要道，在[劉備入蜀后很快成为曹操和劉備激烈争夺的地区](../Page/劉備.md "wikilink")。后来劉備取胜，称“漢中王”，[蜀漢即獲得此地](../Page/蜀漢.md "wikilink")，现今仍遗留有大量[三国古迹](../Page/三国.md "wikilink")，如[武侯墓](../Page/武侯墓.md "wikilink")（[诸葛亮墓地](../Page/诸葛亮.md "wikilink")）、沔縣[武侯祠](../Page/武侯祠.md "wikilink")、[马超庙](../Page/马超.md "wikilink")、[定军山](../Page/定军山.md "wikilink")、虎头桥（[魏延斩首之处](../Page/魏延.md "wikilink")）等。

诸葛亮去世后，汉中复由[魏夺得](../Page/曹魏.md "wikilink")，设梁州\[11\]。后魏灭，[西晋政权建立](../Page/西晋.md "wikilink")。随着政局变动，汉中的归属也开始在政权更替中反复变更。先后归属西晋、[东晋](../Page/东晋.md "wikilink")、[宋](../Page/刘宋.md "wikilink")、[齐](../Page/南齊.md "wikilink")、[北魏](../Page/北魏.md "wikilink")、[梁](../Page/南梁.md "wikilink")、[西魏](../Page/西魏.md "wikilink")、[北周政权](../Page/北周.md "wikilink")。

### 东汉以降

东汉以降，汉中郡曾先后称为[汉宁郡](../Page/漢寧郡.md "wikilink")、梁州、[汉川郡](../Page/汉川郡.md "wikilink")、[山南西道](../Page/山南西道.md "wikilink")、[兴元府](../Page/興元府.md "wikilink")、[汉中府](../Page/汉中府.md "wikilink")、汉中道，而南郑之称，除在西魏[废帝三年至](../Page/废帝.md "wikilink")[隋](../Page/隋.md "wikilink")[大业年间](../Page/大业_\(年号\).md "wikilink")（554－605年）一度改为光义县外，南郑之名一直为历代朝廷沿用\[12\]。西晋政权动荡后，归于[成汉](../Page/成汉.md "wikilink")\[13\]。[桓温平定蜀地后](../Page/桓温.md "wikilink")，复设汉中郡，属梁州。下辖八县：南郑、蒲池、褒中、沔阳、城固、西乡、黄金、兴道。没过多久，被[前秦征服](../Page/前秦.md "wikilink")。南北朝时为汉中郡，领南郑、汉阳\[14\]、城固三县。[后周改汉中为汉川郡](../Page/后周.md "wikilink")\[15\]。

隋初，郡废州存。大业初年，州废，改为汉（中）川\[16\]
郡，统领八县：南郑、西、褒城、城固、兴势、西乡、黄金和难江\[17\]。隋大业八年，移县理郡西城南，临[汉水](../Page/汉水.md "wikilink")，[宋](../Page/宋.md "wikilink")[嘉定十二年重筑](../Page/嘉定_\(年号\).md "wikilink")。

### 唐宋元

[唐初](../Page/唐.md "wikilink")，改汉川郡曰梁州，[开元中因梁](../Page/开元.md "wikilink")、凉声近，更名曰[褒州](../Page/褒州.md "wikilink")。不久重新改回梁州。[天宝初年](../Page/天宝.md "wikilink")，又改为汉中郡。[唐德宗于](../Page/唐德宗.md "wikilink")[兴元六年](../Page/興元.md "wikilink")（874年）三月避乱汉中，六月，叛乱平定后返长安，并以其年号改汉中为兴元府，领五县：南郑、褒城、城固、西、三泉，余地为金、洋、凤、兴四州\[18\]。[五代时期](../Page/五代.md "wikilink")，先后成为[前蜀](../Page/前蜀.md "wikilink")、[后唐](../Page/后唐.md "wikilink")、[后蜀政权的领地](../Page/后蜀.md "wikilink")。

[北宋平後蜀](../Page/北宋.md "wikilink")，仍为兴元府，领南郑、城固、褒城、西四县\[19\]，在[熙宁五年](../Page/熙宁.md "wikilink")（1072）成为利州路兴元府的治所。北宋时汉中商税收入仅次于[开封](../Page/开封.md "wikilink")、[成都府等地](../Page/成都府.md "wikilink")，居全国前列。汉中的利州路、成都的[益州路](../Page/益州路.md "wikilink")、南充江油的[梓州路和重庆渝中的](../Page/梓州路.md "wikilink")[夔州路共同构成了](../Page/夔州路.md "wikilink")“川陕四路”\[20\]。[南宋时成为边陲重镇](../Page/南宋.md "wikilink")，增设廉水一县。余地为沔、金、凤三州\[21\]。汉中也是宋金战争中的一个战场，将领[吴玠取得了仙人关之战的胜利](../Page/吴玠.md "wikilink")。

[元代为](../Page/元代.md "wikilink")[兴元路](../Page/兴元路.md "wikilink")，隶属于陕西行省\[22\]。

### 明清

[明](../Page/明.md "wikilink")[洪武三年](../Page/洪武.md "wikilink")，改为[汉中府](../Page/汉中府.md "wikilink")，领两州：金、宁羌，统辖十四县：南郑、城固、洋、西乡、褒城、凤、沔、略阳、石泉、汉阴、平利、洵阳、紫阳、白河，其中南郑为附郭\[23\]。知府[费震在宋城基础上对汉中城进行重修](../Page/费震.md "wikilink")，基本奠定了今天汉中城的格局。洪武四年，府治西设汉中御守[千户所](../Page/千戶所.md "wikilink")，十三年改为汉中卫，领左、右、中、前、后五所，隶属陕西[都司](../Page/都司.md "wikilink")\[24\]。
[明代琉璃照壁_汉中.jpg](https://zh.wikipedia.org/wiki/File:明代琉璃照壁_汉中.jpg "fig:明代琉璃照壁_汉中.jpg")
[明神宗第五子](../Page/明神宗.md "wikilink")[朱常浩受封](../Page/朱常浩.md "wikilink")[瑞王藩于此地](../Page/明朝藩王列表_\(世宗、穆宗及神宗系\).md "wikilink")，并在城内建瑞王府（今儿童公园为遗址所在地）\[25\]。经过二十多年修建，瑞王府规模宏大，院落相连，楼台相望，亭阁错列。东连莲花池，南至西大街，西接北教场粮库，北抵城墙基，并将北城墙向北移二十步。面积约占当时汉中城三分之一。今石狮子坝为瑞王府大门，门前原有石狮两蹲而得名。今祥瑞巷为瑞王府前第一巷。太古石巷（莲湖路修建工程中废弃）原有一蹲太古石，为瑞王府花园装饰品。瑞王府前原有[琉璃](../Page/琉璃.md "wikilink")[照壁](../Page/照壁.md "wikilink")（今石狮酒店处），1935年修路时拆除。现存琉璃照壁为瑞王在原府城基重新修葺而成，高6.45米，长13.6米，宽1.23米，位于伞铺街东端\[26\]。

[崇祯十六年](../Page/崇祯.md "wikilink")（1643年），[李自成由](../Page/李自成.md "wikilink")[湖北](../Page/湖北.md "wikilink")、[河南进攻陕西](../Page/河南.md "wikilink")，[张献忠也配合夹击](../Page/张献忠.md "wikilink")。起义军占领[西安后](../Page/西安.md "wikilink")，朱常浩南逃四川避难。留在汉中的豪华王府，被当地人拆除，一抢而空。[清](../Page/清.md "wikilink")[康熙年间](../Page/康熙.md "wikilink")，[王士祯](../Page/王士祯.md "wikilink")《瑞王故宫曲》这样写道：“往日朱门帝子家，柴车一去即天涯。平台宾客今何处，零落小山丛桂花。”昔日瑞王府，今日留下莲花池及琉璃照壁
，还有铸“飞天”、天龙”等图案古铜钟，现存古汉台，花园旁太古石，存于汉中市图书馆\[27\]。

1863年春，[太平天国扶王](../Page/太平天国.md "wikilink")[陈得才](../Page/陈得才.md "wikilink")、遵王[赖文光](../Page/赖文光.md "wikilink")、瑞王[蓝成春](../Page/蓝成春.md "wikilink")、启王[梁成富等率部西进](../Page/梁成富.md "wikilink")，与清军交战，同年8月攻克汉中，杀南郑[知县周蕃寿](../Page/知縣.md "wikilink")，后被清军镇压。

### 中华民国时期

[中华民国成立后](../Page/中华民国.md "wikilink")，漢中地方由前清漢中府改稱[漢中道](../Page/漢中道.md "wikilink")，前清[進士](../Page/進士.md "wikilink")[周沆任職漢中道道尹](../Page/周沆.md "wikilink")。[國民政府時期](../Page/國民政府.md "wikilink")，汉中成为入蜀交通枢纽进行重点建设，中日局势紧张后，成为战略物资的转移站，在战前物资人力以及[故宫博物院文物转移入蜀中发挥了重要作用](../Page/故宫博物院.md "wikilink")。

1937年，“[七七事变](../Page/七七事变.md "wikilink")”后，前[国立北平大学](../Page/國立北平大學.md "wikilink")、[国立北平师范大学](../Page/国立北平师范大学.md "wikilink")、[天津](../Page/天津.md "wikilink")[国立北洋工学院等校在西安合校重组为](../Page/国立北洋工学院.md "wikilink")“[国立西安临时大学](../Page/国立西安临时大学.md "wikilink")”，1938年2月，迁至汉中、城固等地，校本部设城固考院小学，更名为“[国立西北大学](../Page/國立西北大學.md "wikilink")”，办学地就在现在城固二中校园处，8月原[北平师范大学独立设置](../Page/北平师范大学.md "wikilink")，更名为“[国立西北师范学院](../Page/国立西北师范学院.md "wikilink")”（办学地就在依然是现在城固二中校园处），1945年后迁至[兰州](../Page/兰州.md "wikilink")，而国立西北大学则迁回西安复校。抗日战争期间，汉中属于[第五战区辖区](../Page/抗日战争第五战区.md "wikilink")。

### 中华人民共和国成立后

[中华人民共和国成立时](../Page/中华人民共和国.md "wikilink")，汉中尚属国民政府控制中，1949年12月，[解放军](../Page/解放军.md "wikilink")[第二野战军第十八兵团中路部队进入区内](../Page/第二野战军.md "wikilink")，至1950年，新政府全面控制区内管辖权。此后，交通建设、政权巩固措施开始实行（扫清[土匪和地方势力](../Page/土匪.md "wikilink")、驱逐海外[传教士](../Page/传教士.md "wikilink")、[土地改革等](../Page/土地改革.md "wikilink"))。

20世纪50年代末期，“[大饥荒](../Page/三年自然灾害.md "wikilink")”时期汉中受到很大影响，1959年春，农村农民生活困难，缺粮现象严重，至该年4月，中共[汉中地委](../Page/汉中专区.md "wikilink")《关于病饿死人及有关情况的报告》中反映，全区纯属饿死、饿病致死者10人，因缺粮外出谋生者88人，浮肿（或干肿、黄肿）病者2730人，其中死亡228人（官方数据，但在此后民间记录和档案中显示，数据远不止此）\[28\]。
“[三年困难时期](../Page/三年困难时期.md "wikilink")”，汉中确切死亡人数、饿病致死人数，至今未有明确统计数字。1964年的社教运动中，1万余人遭到批斗，[自杀](../Page/自杀.md "wikilink")182起。后来定性为运动扩大打击面，多数为错案，被平反纠正。

“[三线建设](../Page/三线建设.md "wikilink")”开始后，汉中成为重点建设地区。1965年2月20日，国防工办主任[罗瑞卿向](../Page/罗瑞卿.md "wikilink")[中共中央主席](../Page/中共中央主席.md "wikilink")[毛泽东](../Page/毛泽东.md "wikilink")、[中共中央政治局常委汇报了三线建设布局](../Page/中共中央政治局常委.md "wikilink")，提出了《关于国防工业在二、三线地区新建项目的报告》，报告中指出：“[汉中地区处于战略纵深带](../Page/汉中地区.md "wikilink")，山川交错，气候温和，物产丰富，水力资源充足，是[国防工业建设的良好基地](../Page/國防工業.md "wikilink")”。布局方案中明确指出：[汉中地区](../Page/汉中地区.md "wikilink")，以[航空工业为主](../Page/航空.md "wikilink")，辅之国防重要项目和国家工业骨干项目。同年3月21日，报告获得通过。此后，从1965年到1969年，对汉中进行了多次选厂布点和筹建工作，1964年10月28日，[〇一二基地正式成立](../Page/〇一二基地.md "wikilink")。同时[机械工业部下属核工业](../Page/机械工业部.md "wikilink")405厂在区内开始建设，后属[核工业部](../Page/核工业部.md "wikilink")，今属[中核集团即](../Page/中核集团.md "wikilink")[中核陝西鈾濃縮有限公司](../Page/中核陝西鈾濃縮有限公司.md "wikilink")。

1966年开始，机械工业部所属[汉江机床厂](../Page/汉江机床厂.md "wikilink")、[汉川机床厂](../Page/汉川机床厂.md "wikilink")、[汉江工具厂](../Page/汉江工具厂.md "wikilink")、汉江铸锻件厂开始在区内建设。[文革开始后](../Page/文革.md "wikilink")，1967年解放军8318部队和汉中军分区奉命介入，支持群众“革命”。2月，两大造反派组织形成——“汉中统临矿指挥部”（简称“统派”）和“汉中联新革命造反总指挥部”（简称“联新派”）。此后开始不断械斗和武斗，冲击市政设施、[公安系统](../Page/公安.md "wikilink")，正常工作生活陷入全面瘫痪。“[破四旧](../Page/破四旧.md "wikilink")”时期，大量古迹遭到破坏，[城墙遭到拆毁](../Page/城墙.md "wikilink")、[钟楼在武斗中被彻底摧毁](../Page/钟楼.md "wikilink")，汉台等遗迹都有不同程度的损毁和人为破坏，此后遗留的大多数古迹，都是在进入[20世纪90年代后重新修建或在原址基础上翻新而来](../Page/20世纪90年代.md "wikilink")。1968年，军队介入，武斗和“革命狂热”逐渐被遏制\[29\]。

1969年11月，中央决定将汉中作为[运输机生产基地](../Page/运输机.md "wikilink")，并列为“四五计划”的重点项目。[改革开放后](../Page/改革开放.md "wikilink")，[军工企业逐步撤出山区](../Page/军工.md "wikilink")\[30\]。

1986年，中国有记录的第一例“[安乐死](../Page/安乐死.md "wikilink")”出现在汉中市传染病医院（现为第二人民医院），医生蒲连升因为他人实施安乐死手术（复方冬眠灵175[毫克](../Page/毫克.md "wikilink")），被控[谋杀](../Page/谋杀.md "wikilink")，1986年7月3日立案。1991年5月17日，经过了6年的漫长审理后，汉中市[人民法院判决原告无罪释放](../Page/人民法院_\(中国\).md "wikilink")\[31\]，
与此同时引发的争议一直没有停止\[32\]。

[汉中北部夜景_HDR.jpg](https://zh.wikipedia.org/wiki/File:汉中北部夜景_HDR.jpg "fig:汉中北部夜景_HDR.jpg")

2003年5月27日，汉中市[人大副主任](../Page/人大.md "wikilink")[张云亭在汉江边遇刺身亡](../Page/张云亭.md "wikilink")，同行女伴受伤\[33\]。但公开发表的新闻均未说明该主任为何携女伴在江边出行，该女性[公务员身份及两人关系一时在坊间成为茶余饭后笑谈](../Page/公务员.md "wikilink")，该案例也成为该级别公务员大陆遇刺第一案。

2008年5月12日[汶川大地震](../Page/汶川大地震.md "wikilink")，汉中是受灾较为严重的城市之一\[34\]。2009年入春后，境内大规模爆发[狂犬疫情](../Page/狂犬病.md "wikilink")，造成至少12人死亡，数千人次受到狂犬攻击\[35\]。但随后当局对辖区内犬只的大规模扑杀却引发舆论的争议\[36\]。

## 地理概况

### 地形

[Hanzhong_District.jpg](https://zh.wikipedia.org/wiki/File:Hanzhong_District.jpg "fig:Hanzhong_District.jpg")卫星图上的汉中盆地\]\]
北界秦岭山脉，南界大巴山，总面积27,246 km²\[37\]。其间由[汉江冲积形成](../Page/汉江.md "wikilink")[汉中盆地](../Page/汉中盆地.md "wikilink")。属凉[亚热带气候](../Page/亞熱帶氣候.md "wikilink")。汉中市位于[汉水上游](../Page/汉水.md "wikilink")，汉中盆地中部，东经106°51'－107°10'、北纬33°2'－33°22'之间。地形特点南低北高，市内有平坝、[丘陵和](../Page/丘陵.md "wikilink")[山地等三种地貌](../Page/山地.md "wikilink")，平坝为汉江[冲积平原的一二阶梯](../Page/冲积平原.md "wikilink")，[海拔](../Page/海拔.md "wikilink")500－600米之间，地势平坦，土壤肥沃，占全市面积的34.62%；丘陵为山前洪积扇形成的宽谷浅丘地带，海拔601－800米之间，地势起伏较大，约占全市面积的28.1%；山区是秦岭南坡形成的浅山和中山地区，地势较为复杂，土壤贫瘠，海拔在701－2038米之间，约占全市总面积的37.2%。

汉中盆地南北长约37公里，东西宽约23公里，占汉中地区总面积的1.9%，可耕地303200[亩](../Page/亩.md "wikilink")，其中以[水田为主](../Page/水田.md "wikilink")。汉中属于亚热带气候区，北有秦岭屏障，[寒流不易侵入](../Page/寒流.md "wikilink")，气候温和湿润。

### 水文

[Hanzhong_baoheriver.jpg](https://zh.wikipedia.org/wiki/File:Hanzhong_baoheriver.jpg "fig:Hanzhong_baoheriver.jpg")\]\]
本区域的河流均属[长江流域](../Page/长江流域.md "wikilink")，汉江东西横贯，[嘉陵江南北纵穿](../Page/嘉陵江.md "wikilink")，[米仓山南坡有](../Page/米仓山.md "wikilink")[渠江上游河源区的部分河流](../Page/渠江.md "wikilink")。同时河流密布，每平方公里平均河流长度为1.4－2公里。

#### 汉江水系

汉江，又名汉水，境内部分古称[沔水](../Page/沔水.md "wikilink")，为[长江最大一级](../Page/长江.md "wikilink")[支流](../Page/支流.md "wikilink")。汉中位于汉江上游，流域范围北起秦岭，南达米仓山，西接嘉陵江流域，东至[子午河](../Page/子午河.md "wikilink")、[茶镇与](../Page/茶镇.md "wikilink")[褚河一带](../Page/褚河镇.md "wikilink")，汉江也是秦岭与巴山的分界线。汉江干流自西向东流经宁强、勉县、汉中市区、南郑、城固、洋县和西乡县境，横贯汉中盆地，是区内水系网络的骨架。

#### 嘉陵江水系

分布在区内西部和西南部。干流自北向南，纵贯略阳、宁强两县，为过境大河。流域狭长。

### 气候

#### 气温及降水

区内[气温的地理分布](../Page/气温.md "wikilink")，受地形影響較大。西部略低于东部，南北山区低于平坝和丘陵。海拔600米以下的平坝地区年均气温在14.2－14.6℃；一般海拔1000米以上的地区年均气温低于12℃；西嘉陵江河谷年均气温高于13℃。以下图表以汉中市区平坝数据为准：

此外，本区年平均及各月地温与气温分布形势基本一致，差值不大。

漢中屬[副熱帶季風氣候](../Page/副熱帶季風氣候.md "wikilink")。地处[北半球](../Page/北半球.md "wikilink")[中纬度](../Page/中纬度.md "wikilink")，形成全年降水的暖湿空气，主要来自[印度洋](../Page/印度洋.md "wikilink")[孟加拉湾](../Page/孟加拉湾.md "wikilink")，其次是西[太平洋](../Page/太平洋.md "wikilink")。夏季，在[副热带高压影响下](../Page/副熱帶高壓.md "wikilink")，孟加拉湾水汽沿西南低涡下部的西南[季风北上](../Page/季风.md "wikilink")，经[西藏](../Page/西藏.md "wikilink")、[云南及四川西北部到达本区上空](../Page/云南.md "wikilink")；西太平洋水汽随副热带高压边缘的东南气流输入本区上空。冬季，受[极地大陆冷气团](../Page/极地大陆冷气团.md "wikilink")（主要是[蒙古高压](../Page/蒙古高壓.md "wikilink")）控制，多西北季风，形成低溫少雨的天气。春秋为过渡季节，春暖少雨，秋凉多雨，气候湿润。地面[植被](../Page/植被.md "wikilink")、[水库](../Page/水库.md "wikilink")、河流、田园等所蒸发于空间的水汽参与降水甚微。

来自西南、东南的暖湿气流受巴山、秦岭阻隔，使得区内雨量充沛，但由于两山位于西南气流的路径上，由于中低层云系的水汽系统辐合，以及地形抬升和山地垂直影响，使本区南北承接水汽不等，[降水量分布悬殊](../Page/降水量.md "wikilink")，多年平均降水量为700－1700毫米之间。其中南部米仓山最为丰富，成为陕西之冠。

#### 湿度

整个汉中地区年平均[相对湿度分布态势](../Page/相对湿度.md "wikilink")，基本呈南大北小。汉江平坝、巴山山地70－80%；秦岭山地73%。一年中冬春两季较小，夏秋较大。9、10月为全年之冠，均在80－86%；冬季（12、1、2月）三个月汉江平坝、巴山山地为75－80%；秦岭山地58－66%。

#### 风

区内年均平均风速介于1－2.5[米/秒之间](../Page/米/秒.md "wikilink")。鲜见大风天，年均不到2天，周边郊县则有不同。

### 生物

  - [植物](../Page/植物.md "wikilink")：

秦巴山区多[原始森林](../Page/原始森林.md "wikilink")，植被类型多样。截止1995年10月，已发现汉中地区境内有2942种植物，分属[种子](../Page/种子植物.md "wikilink")、[蕨类](../Page/蕨类植物.md "wikilink")、[苔藓](../Page/苔藓植物.md "wikilink")、[地衣](../Page/地衣.md "wikilink")、[真菌和](../Page/真菌.md "wikilink")[藻类](../Page/藻类.md "wikilink")，计7门、282科、1160属植物。其中仅种子植物就有166科、958属、2564种，分别占到全国科（166/291）的57%、属（958/2940）的32.6%、种（2564/24300）的10.6%。\[38\]
[Ibis_KIN.JPG](https://zh.wikipedia.org/wiki/File:Ibis_KIN.JPG "fig:Ibis_KIN.JPG")

  - [动物种群](../Page/动物.md "wikilink")：

汉中地区位于[古北界动物区系和](../Page/古北界.md "wikilink")[东洋界动物区系的交汇处](../Page/东洋界.md "wikilink")，南北动物兼有，动物种群组成丰富。其间生存珍稀动物有[大熊猫](../Page/大熊猫.md "wikilink")、[朱鹮](../Page/朱鹮.md "wikilink")、[金丝猴](../Page/金丝猴.md "wikilink")、[羚牛](../Page/羚牛.md "wikilink")、[中國大鲵](../Page/中國大鯢.md "wikilink")（娃娃鱼）等。

汉中地区野生[脊椎动物计有](../Page/脊椎动物.md "wikilink")：[鸟类](../Page/鸟类.md "wikilink")335种，属17目、51科，其中[留鸟](../Page/留鸟.md "wikilink")155种，夏[候鸟](../Page/候鸟.md "wikilink")91种，冬候鸟28种，旅鸟61种；[哺乳动物](../Page/哺乳动物.md "wikilink")137种，属7目、27科；[两栖类动物](../Page/两栖类动物.md "wikilink")24种，属2目、7科；[爬行类](../Page/爬行类动物.md "wikilink")37种，属3目、9科；[鱼类](../Page/鱼类.md "wikilink")109种，属6目、15科。

## 政治

### 现任领导

<table>
<caption>汉中市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党汉中市委员会.md" title="wikilink">中国共产党<br />
汉中市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/汉中市人民代表大会.md" title="wikilink">汉中市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/汉中市人民政府.md" title="wikilink">汉中市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议汉中市委员会.md" title="wikilink">中国人民政治协商会议<br />
汉中市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/王建军_(1963年).md" title="wikilink">王建军</a>[39]</p></td>
<td><p><a href="../Page/张雁毅.md" title="wikilink">张雁毅</a>[40]</p></td>
<td><p><a href="../Page/方红卫.md" title="wikilink">方红卫</a>[41]</p></td>
<td><p><a href="../Page/王隆庆.md" title="wikilink">王隆庆</a>[42]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/丹凤县.md" title="wikilink">丹凤县</a></p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/寿县.md" title="wikilink">寿县</a></p></td>
<td><p>陕西省<a href="../Page/富平县.md" title="wikilink">富平县</a></p></td>
<td><p>陕西省<a href="../Page/洋县.md" title="wikilink">洋县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年2月</p></td>
<td><p>2017年2月</p></td>
<td><p>2017年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

下辖2个[市辖区](../Page/市辖区.md "wikilink")、9个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[汉台区](../Page/汉台区.md "wikilink")、[南郑区](../Page/南郑区.md "wikilink")
  - 县：[城固县](../Page/城固县.md "wikilink")、[洋县](../Page/洋县.md "wikilink")、[西乡县](../Page/西乡县.md "wikilink")、[勉县](../Page/勉县.md "wikilink")\[43\]、[宁强县](../Page/宁强县.md "wikilink")、[略阳县](../Page/略阳县.md "wikilink")、[镇巴县](../Page/镇巴县.md "wikilink")、[留坝县](../Page/留坝县.md "wikilink")、[佛坪县](../Page/佛坪县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>汉中市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[44]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>610700</p></td>
</tr>
<tr class="odd">
<td><p>610702</p></td>
</tr>
<tr class="even">
<td><p>610703</p></td>
</tr>
<tr class="odd">
<td><p>610722</p></td>
</tr>
<tr class="even">
<td><p>610723</p></td>
</tr>
<tr class="odd">
<td><p>610724</p></td>
</tr>
<tr class="even">
<td><p>610725</p></td>
</tr>
<tr class="odd">
<td><p>610726</p></td>
</tr>
<tr class="even">
<td><p>610727</p></td>
</tr>
<tr class="odd">
<td><p>610728</p></td>
</tr>
<tr class="even">
<td><p>610729</p></td>
</tr>
<tr class="odd">
<td><p>610730</p></td>
</tr>
<tr class="even">
<td><p>注：汉台区数字包含汉中经济开发区所辖鑫源街道。</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>汉中市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[45]（2010年11月）</p></th>
<th><p>户籍人口[46]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>汉中市</p></td>
<td><p>3416196</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>汉台区</p></td>
<td><p>534923</p></td>
<td><p>15.66</p></td>
</tr>
<tr class="even">
<td><p>南郑区</p></td>
<td><p>471634</p></td>
<td><p>13.81</p></td>
</tr>
<tr class="odd">
<td><p>城固县</p></td>
<td><p>464903</p></td>
<td><p>13.61</p></td>
</tr>
<tr class="even">
<td><p>洋　县</p></td>
<td><p>383981</p></td>
<td><p>11.24</p></td>
</tr>
<tr class="odd">
<td><p>西乡县</p></td>
<td><p>341812</p></td>
<td><p>10.01</p></td>
</tr>
<tr class="even">
<td><p>勉　县</p></td>
<td><p>388123</p></td>
<td><p>11.36</p></td>
</tr>
<tr class="odd">
<td><p>宁强县</p></td>
<td><p>308885</p></td>
<td><p>9.04</p></td>
</tr>
<tr class="even">
<td><p>略阳县</p></td>
<td><p>201645</p></td>
<td><p>5.90</p></td>
</tr>
<tr class="odd">
<td><p>镇巴县</p></td>
<td><p>246817</p></td>
<td><p>7.22</p></td>
</tr>
<tr class="even">
<td><p>留坝县</p></td>
<td><p>43398</p></td>
<td><p>1.27</p></td>
</tr>
<tr class="odd">
<td><p>佛坪县</p></td>
<td><p>30075</p></td>
<td><p>0.88</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")3416196人\[47\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共减少62387人，减少1.8%，年平均减少0.2%。其中，男性人口为1756525人，占51.4%；女性人口为1659671人，占48.6%。常住人口性别比（以女性为100）为105.84。0－14岁人口为527919人，占15.5%；15－64岁人口为2522041人，占73.8%；65岁及以上人口为366236人，占10.7%。

## 经济

### 工业

区内大型工业均以国防工业衍化而来。[飞机工业也是汉中市工业经济增长的重要组成部分](../Page/飞机.md "wikilink")。陕西飞机工业（集团）有限公司（简称[陕飞集团](../Page/陕飞集团.md "wikilink")），所属于[中国航空工业集团](../Page/中国航空工业集团.md "wikilink")，是经国家批准于1969年开始建设的中国唯一研制、生产大、中型民用运输机的大型国有军工企业，现有总资产30亿元，职工近万人\[48\]。陕西飞机工业（集团）有限公司（原第三工业机械部所属〇一二基地）位于区内，为大型运输机制造基地\[49\]。2009年，[中国国务院已经正式批准大飞机国家重大专项立项实施](../Page/中国国务院.md "wikilink")\[50\]，汉中陕飞集团与[哈尔滨](../Page/哈尔滨.md "wikilink")[哈飞集团](../Page/哈飞集团.md "wikilink")、西安[西飞集团](../Page/西飞集团.md "wikilink")、[沈阳](../Page/沈阳.md "wikilink")[沈飞集团和成都](../Page/沈飞集团.md "wikilink")[成飞集团並肩成为](../Page/成飞集团.md "wikilink")“大飞机”五大主制造商，主要负责形成“大飞机”零部件的转包生产能力\[51\]\[52\]。

此外，中核工业集团所属405厂也位于区内，从事[铀浓缩生产](../Page/铀.md "wikilink")。

[有色冶金工业是汉中市工业经济增长的重要支撑力量](../Page/有色冶金.md "wikilink")。2005年，汉中的有色冶金工业发展到49家，占重型[规模以上工业企业的](../Page/规模以上工业.md "wikilink")20.4%，其中采矿业31家，冶炼及加工业18家，分别占重型规模以上工业企业的12.9%、7.5%。有色冶金工业实现工业总产值51.78亿元，占重型规模以上工业企业工业总产值的31.2%；实现主营业务收入44.02亿元，占重型规模以上工业企业主营业务收入的30.9%；实现利税1.52亿元，占重型规模以上工业企业利税总额的14.7%\[53\]。2009年，汉中市[钢铁企业正式进行重组](../Page/钢铁.md "wikilink")，由汉中市[国资委](../Page/国资委.md "wikilink")、[汉中钢铁（集团）股份有限公司](../Page/汉中钢铁（集团）股份有限公司.md "wikilink")、[陕西略阳钢铁有限责任公司各出资](../Page/陕西略阳钢铁有限责任公司.md "wikilink")2亿元，注册成立汉中钢铁（集团）股份有限公司，并组建了[汉中钢铁集团管理委员会负责重组工作](../Page/汉中钢铁集团管理委员会.md "wikilink")\[54\]。

### 农业

由于区位限制，区内农业生产机械化程度较低，农作物以中国南方亚热带地域农作物为主，[水稻](../Page/水稻.md "wikilink")、[小麦](../Page/小麦.md "wikilink")、[玉米均有种植](../Page/玉米.md "wikilink")，菌类和[中草药种植为特色](../Page/中草药.md "wikilink")，兼有部分果品种植。汉中市可耕种面积为20.09万[公顷](../Page/公顷.md "wikilink")，非农业人口占全市人口的80%。2006年，农民创造的农、林、牧、渔业总产值达94.79亿元，[粮食总产量](../Page/粮食.md "wikilink")116.89万吨，油料14.50万吨，[蔬菜](../Page/蔬菜.md "wikilink")120.66万吨，[水果](../Page/水果.md "wikilink")19.29万吨，[茶叶](../Page/茶叶.md "wikilink")7450吨，[中药材](../Page/中藥材.md "wikilink")10.06万吨\[55\]。

汉中的[樱桃生产地发展面积为](../Page/樱桃.md "wikilink")3平方公里，是中国三大樱桃产地之一，以[樱桃沟为主产地](../Page/樱桃沟.md "wikilink")。其樱桃品种以中国[酸水樱桃为主](../Page/酸水樱桃.md "wikilink")，樱桃沟连片种植樱桃12300亩，年产樱桃3600多吨，农业年收入960多万元。2008年，樱桃沟吸引游客41万，旅游综合收入达2494.8万元\[56\]。

汉中市也是中国北方茶区中最优良的[茶叶适生地](../Page/茶叶.md "wikilink")，其种植历史悠久，始于战国、兴于秦汉、盛于唐宋、繁荣于明清，自古就是茶马互市的重要供茶地。自2000年起，汉中市茶产业得到显著发展及成效。茶园面积从1987年的13.06万亩发展到2008年的60多万亩，增长4.2倍；产量从2992吨提高到8367吨，增长2.8倍；产值达到4.8亿元，成为汉中市四大主导产业之一\[57\]。2009年，汉中市对全市8个产茶县的20余个茶叶品牌进行整合，统一为“[汉中仙毫](../Page/汉中仙毫.md "wikilink")”\[58\]，并在汉中举办“2009中国·汉中茶叶节樱桃节”\[59\]。

## 旅游业

汉中市坐拥汉水，一江两岸景色秀美，文物名胜众多，旅游业较为发达。

汉中市自2010年以来，每年召开“中国最美油菜花海汉中旅游文化节”。\[60\]

### 商业

[汉中_东南全景图.jpg](https://zh.wikipedia.org/wiki/File:汉中_东南全景图.jpg "fig:汉中_东南全景图.jpg")
[汉中_北部天际线.jpg](https://zh.wikipedia.org/wiki/File:汉中_北部天际线.jpg "fig:汉中_北部天际线.jpg")

  - ；

  - 旅游可下榻宾馆较多，四星级宾馆有邮政大酒店和红叶大酒店，三星级有田园酒店、石狮酒店等；

  - [出租车](../Page/出租车.md "wikilink")、[公交车系统在汉台区较为方便](../Page/公交.md "wikilink")，各县区也有出租车、公交车、三轮[摩托车可供选择](../Page/摩托车.md "wikilink")；

  - 汉台区购物[商场有世纪阳光](../Page/商场.md "wikilink")、星光百货、万邦时代广场等。大型超市有晶众家乐购物广场、[华润万家超市等](../Page/華潤萬家超市.md "wikilink")。主要小商品商业街在北大街，批发业集中于虎桥路运达批发市场。

## 交通

### 古代交通

古栈道位于秦岭巴山之间的汉中盆地，后来人们把越秦岭翻巴山、连接西安、汉中、[广元](../Page/广元.md "wikilink")、成都的古道称“[蜀道](../Page/蜀道.md "wikilink")”\[61\]\[62\]。

距今约3500年，由[商代诸侯国](../Page/商代.md "wikilink")“方”（又称“[巴方](../Page/巴方.md "wikilink")”）的巴人在夏末商初时开通，由古[巴国的今](../Page/巴国.md "wikilink")[巴中出发](../Page/巴中.md "wikilink")，越米仓山北达陕西古梁州，延伸到汉中，称之为“**米仓古道**”，是[石牛道未开通之前唯一的一条川陕通道](../Page/石牛道.md "wikilink")，名曰“[巴岭路](../Page/巴岭路.md "wikilink")”\[63\]，是中国古代最早的[国道](../Page/国道.md "wikilink")\[64\]。

汉代，由汉中至[关中的栈道计有](../Page/关中.md "wikilink")[故道](../Page/故道.md "wikilink")、[褒斜道](../Page/褒斜道.md "wikilink")、[骆道和](../Page/骆道.md "wikilink")[子午道](../Page/子午道.md "wikilink")；由汉中到四川的栈道有[金牛道和](../Page/金牛道.md "wikilink")[米仓道](../Page/米仓道.md "wikilink")；由汉中到甘肃的栈道有[白水道](../Page/白水道.md "wikilink")\[65\]。共有栈道六条，这些栈道中以兴修于西汉中叶的褒斜道最为有名，其由[褒河之谷](../Page/褒河.md "wikilink")（今陕西汉中附近）入[斜水之谷](../Page/斜水.md "wikilink")（今陕西[眉县境内](../Page/眉县.md "wikilink")）\[66\]\[67\]。

### 现代交通

[宁陕境内的西汉高速.jpg](https://zh.wikipedia.org/wiki/File:宁陕境内的西汉高速.jpg "fig:宁陕境内的西汉高速.jpg")
1931年后，[国民政府](../Page/国民政府.md "wikilink")[全国经济建设委员会公路处副处长](../Page/全國經濟委員會_\(1931年－1937年\).md "wikilink")[赵祖康](../Page/赵祖康.md "wikilink")，后逐渐升迁至[交通部公路总局副局长](../Page/交通部公路总局.md "wikilink")。[中国抗日战争爆发后](../Page/中国抗日战争.md "wikilink")，[赵祖康主持抢修了多条军用公路](../Page/赵祖康.md "wikilink")，包括负责修建了西兰公路（[西安至](../Page/西安.md "wikilink")[兰州](../Page/兰州.md "wikilink")）后、立即修建西汉公路（西安至[汉中](../Page/汉中.md "wikilink")）。

  - 铁路：[宝成铁路](../Page/宝成铁路.md "wikilink")（宝鸡－成都）、[阳安铁路](../Page/阳安铁路.md "wikilink")（[阳平关](../Page/阳平关.md "wikilink")－[安康](../Page/安康.md "wikilink")）、[西成客运专线](../Page/西成客运专线.md "wikilink")（西安－成都）
  - 公路：[108国道](../Page/108国道.md "wikilink")、[316国道](../Page/316国道.md "wikilink")、[210国道](../Page/210国道.md "wikilink")、[西汉高速公路](../Page/西汉高速公路.md "wikilink")（西安－汉中，2007年9月30日通车）、[十天高速公路](../Page/十天高速公路.md "wikilink")（[十堰](../Page/十堰.md "wikilink")－天水）、[宝汉高速公路](../Page/宝汉高速公路.md "wikilink")（宝鸡－汉中，部分通车，部分建设中）
  - 航空：[汉中城固机场](../Page/汉中城固机场.md "wikilink")（支线机场），开辟有至西安、北京、重庆、[深圳的航线](../Page/深圳.md "wikilink")。原有航空港<s>[漢中西關機場](../Page/漢中西關機場.md "wikilink")</s>已停用。

## 文化及其他

### 文物古迹

[题匾_汉中武侯祠.jpg](https://zh.wikipedia.org/wiki/File:题匾_汉中武侯祠.jpg "fig:题匾_汉中武侯祠.jpg")
10处[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")\[68\]：

  - [褒斜道石门及其摩崖石刻](../Page/褒斜道石门及其摩崖石刻.md "wikilink")（褒斜道為入蜀[栈道之一](../Page/栈道.md "wikilink")，部分由于[褒河水庫修建而淹沒于水下](../Page/石门水库_\(汉台\).md "wikilink")，今天存留為翻修。摩崖石刻存于[漢中市博物館](../Page/汉中市博物馆.md "wikilink")）
  - 勉县武侯祠
  - 龙岗寺遗址
  - 张骞墓祠
  - 五门堰
  - 蔡伦墓和祠
  - 开明寺塔
  - 李家村遗址
  - 张良庙（與張良并無直接聯繫，今留坝县境内，清代修建\[69\]）
  - 灵崖寺

[汉中拜将台.jpg](https://zh.wikipedia.org/wiki/File:汉中拜将台.jpg "fig:汉中拜将台.jpg")
地方遺址及古跡：

  - 古城墙遗址（翻新而来，民主街与西环路交汇处）
  - 明瑞王府遗址（儿童公园及伞铺街照壁遗存）
  - 拜将坛（相传为刘邦拜韩信大将处）
  - 虎头桥遺址
  - 饮马池
  - 武侯祠（勉县境内）
  - 定军山古战场（勉县境内）

### 历史名人

  - [褒姒](../Page/褒姒.md "wikilink")，西周[周幽王王后](../Page/周幽王.md "wikilink")，有“倾城倾国”“一笑失天下”的[成语典故](../Page/成语.md "wikilink")。
  - [张骞](../Page/张骞.md "wikilink")，西汉城固县人，[旅行家](../Page/旅行家.md "wikilink")、[外交家](../Page/外交家.md "wikilink")，开辟了“丝绸之路”。
  - 杨王孙，西汉城固县人，提倡简葬。
  - [李固](../Page/李固.md "wikilink")，东汉南郑人，曾任三朝[太尉](../Page/太尉.md "wikilink")，为人刚正，有“北斗喉舌”之称。名句“阳春之曲，和者盖寡；盛名之下，其实难副”。
  - [蔡伦](../Page/蔡伦.md "wikilink")，东汉学者，改进[造纸术](../Page/造纸术.md "wikilink")，史称“蔡伦纸”，葬于封地龙亭，即今洋县[龙亭镇](../Page/龙亭镇.md "wikilink")。
  - [张鲁](../Page/张鲁.md "wikilink")，东汉五斗米道教主，道教著名人物，东汉末年曾割据汉中一方。
  - [诸葛亮](../Page/诸葛亮.md "wikilink")，三国时期蜀汉[丞相](../Page/丞相.md "wikilink")，以汉中为北伐魏国的基地。

<!-- end list -->

  - [王元寿](../Page/王元壽.md "wikilink")，南北朝北魏[秦州略阳人](../Page/秦州.md "wikilink")。据《[资治通鉴](../Page/资治通鉴.md "wikilink")》所载，著名[酷吏](../Page/酷吏.md "wikilink")。

<!-- end list -->

  - [文同](../Page/文同.md "wikilink")，北宋[书法家](../Page/书法家.md "wikilink")，曾任兴元府[知府](../Page/知府.md "wikilink")、洋州[知州](../Page/知州.md "wikilink")，有“胸有成竹”的成语典故。
  - [陆游](../Page/陆游.md "wikilink")，南宋[诗人](../Page/诗人.md "wikilink")、[词人](../Page/词人.md "wikilink")，曾在南郑任左丞议郎、四川宣抚使司干办公事、兼检法官，筹划抗金。
  - [方孝孺](../Page/方孝孺.md "wikilink")，明政治人物、著名学者，曾任汉中府学教授。
  - [僧省悟](../Page/僧省悟.md "wikilink")，汉中人。明朝中期四川[民变首领](../Page/民变.md "wikilink")。

<!-- end list -->

  - [张勇](../Page/張勇_\(襄壯\).md "wikilink")，明末清初[西安府](../Page/西安府.md "wikilink")[咸寧縣](../Page/咸寧縣.md "wikilink")（今陕西省西安市，一说为陕西洋县）人，悍将，征西北、定西南，镇三藩叛乱。
  - [王炳](../Page/王炳_\(同治進士\).md "wikilink")，陕西省汉中府南郑县（今陕西省汉中市）人，清朝政治人物、[进士出身](../Page/进士.md "wikilink")。
  - [劉寶濂](../Page/劉寶濂.md "wikilink")，辛亥革命元老，中華民國實業家、政治人物。
  - [刘宝锷](../Page/刘宝锷.md "wikilink")，土木工程师，以监造南京[中山陵最为著名](../Page/中山陵.md "wikilink")。
  - [陆费逵](../Page/陆费逵.md "wikilink")，[复姓](../Page/复姓.md "wikilink")“陆费”，原籍[浙江省](../Page/浙江省.md "wikilink")[桐乡](../Page/桐乡县_\(明朝\).md "wikilink")，生于陕西汉中。中国近代著名[教育家](../Page/教育家.md "wikilink")、出版家，[中華書局創辦人](../Page/中華書局.md "wikilink")。
  - [何挺穎](../Page/何挺穎.md "wikilink")，中國共產黨早期人物。
  - [刘恩荫](../Page/刘恩荫.md "wikilink")，[中华民国国军将领](../Page/中华民国国军.md "wikilink")，1949年随国民政府赴[台湾](../Page/台湾.md "wikilink")，后任[装甲兵副](../Page/装甲兵.md "wikilink")[司令](../Page/司令.md "wikilink")，[陸軍金門防衛指揮部司令](../Page/陸軍金門防衛指揮部.md "wikilink")。
  - [方继信](../Page/方继信.md "wikilink")，1948年，当选[第一屆國民大會陝西省代表](../Page/第一屆國民大會陝西省代表.md "wikilink")。1949年随国民政府赴台湾。
  - [龙文](../Page/龙文_\(美术教育家\).md "wikilink")，中华民国第一屆國民大會陝西省代表，美术教育家。
  - [趙文藝](../Page/趙文藝.md "wikilink")，陝西省城固縣人，曾任中华民国第一届西安市[立法委员](../Page/立法委员.md "wikilink")、歷任[臺灣師範大學](../Page/臺灣師範大學.md "wikilink")、[省立臺北師範專科學校](../Page/國立臺北教育大學.md "wikilink")、[中國文化大學兒童福利研究所兼任教授](../Page/中國文化大學.md "wikilink")，擁有“文藝界老佛爺”稱號之現代[文學家與教育家](../Page/文學家.md "wikilink")。

<!-- end list -->

  - [杨育才](../Page/杨育才.md "wikilink")，勉县人，[中国人民志愿军一级战斗英雄](../Page/中国人民志愿军.md "wikilink")。著名[京剧](../Page/京剧.md "wikilink")《[奇袭白虎团](../Page/奇袭白虎团.md "wikilink")》人物侦察排[排长严伟才原型](../Page/排长.md "wikilink")。

<!-- end list -->

  - [王智量](../Page/王智量.md "wikilink")，出生在陕西省汉中市，籍贯在[江苏省](../Page/江苏省.md "wikilink")[江宁县](../Page/江宁区.md "wikilink")，[澳大利亚华人](../Page/澳大利亚华人.md "wikilink")，[中国作家协会成员](../Page/中国作家协会.md "wikilink")，中国共产党党员，[民盟党员](../Page/民盟.md "wikilink")，已经翻译[俄文](../Page/俄文.md "wikilink")[长篇小说](../Page/长篇小说.md "wikilink")30多部，荣获[俄罗斯联邦政府颁发的奖章](../Page/俄罗斯联邦.md "wikilink")。

### 当代名人

  - [冯新柱](../Page/冯新柱.md "wikilink")，陕西洋县人；[中共中央党校函授学院经济管理](../Page/中共中央党校.md "wikilink")[本科毕业](../Page/本科.md "wikilink")，[西北大学经济管理学院](../Page/西北大学.md "wikilink")[博士研究生班学历](../Page/博士.md "wikilink")。1985年6月加入中国共产党。现任中共[铜川](../Page/铜川.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")、市[人大常委会主任](../Page/人大常委会.md "wikilink")。

<!-- end list -->

  - [赵铱民](../Page/赵铱民.md "wikilink")，[少将](../Page/少将.md "wikilink")[军衔](../Page/军衔.md "wikilink")，专业技术三级，[总后优秀教师](../Page/总后.md "wikilink")，总后科技银星，军队院校育才金奖获得者。[第四军医大学校长兼口腔医院院长](../Page/第四军医大学.md "wikilink")，兼任世界军事齿科学会主席，中华口腔医学会副会长，国际颌面缺损修复学会理事等职务。长期从事口腔修复学研究，承担国家自然科学基金重点项目等课题17项，获国家科技进步二等奖2项、军队科技进步一等奖2项、国家实用新型[专利](../Page/专利.md "wikilink")3项；发表论文182篇，被SCI收录36篇；独著《颌面赝复学》（上、下卷）系列专著；培养博士生24名、硕士生37名。

<!-- end list -->

  - [张虹](../Page/张虹_\(作家\).md "wikilink")，1978年毕业于[汉中师范学院中文系](../Page/汉中师范学院.md "wikilink")，1990年又毕业于[西北大学中文系作家班](../Page/西北大学.md "wikilink")。1980年开始文学创作。1994年加入中国作家协会。
  - [哈辉](../Page/哈辉.md "wikilink")，知名[古典音乐艺人](../Page/古典音乐.md "wikilink")。于[解放军艺术学院获](../Page/解放军艺术学院.md "wikilink")[学士学位](../Page/学士.md "wikilink")，[中国音乐学院艺术](../Page/中国音乐学院.md "wikilink")[硕士](../Page/硕士.md "wikilink")。
  - [何毅亭](../Page/何毅亭.md "wikilink")，中华人民共和国政治人物。曾任[中央办公厅政策研究室主任](../Page/中央办公厅.md "wikilink")。2000年6月，任[中央政策研究室副主任](../Page/中央政策研究室.md "wikilink")。2013年9月，任中央党校常务副校长。
  - [胡蝶](../Page/胡蝶_\(主播\).md "wikilink")，出生于中國陝西省漢中市，現為[中央電視台新聞頻道](../Page/中央電視台新聞頻道.md "wikilink")《[朝聞天下](../Page/朝聞天下.md "wikilink")》女主播。

### 风土人情

[汉中面皮.jpg](https://zh.wikipedia.org/wiki/File:汉中面皮.jpg "fig:汉中面皮.jpg")

  - 汉中地处中国南北交界之处，与陕西中部的关中地区为秦岭山脉相隔，是地理风土人情等各方面更接近于[西南地区](../Page/西南地区.md "wikilink")，尤其受四川影响较大，与秦岭北部的关中地区有很大差异；民族有[汉](../Page/汉.md "wikilink")、[回](../Page/回.md "wikilink")、[羌](../Page/羌.md "wikilink")、[土族](../Page/土族.md "wikilink")、[党项为主](../Page/党项.md "wikilink")。在位于大巴山脚下的镇巴县[青水乡是西北地区最大的](../Page/青水乡.md "wikilink")[苗民聚集地](../Page/苗族.md "wikilink")，史载，清[乾隆五十年](../Page/乾隆.md "wikilink")（1785年）[熊](../Page/熊.md "wikilink")、[陶](../Page/陶.md "wikilink")、[李](../Page/李.md "wikilink")、[吴](../Page/吴.md "wikilink")、[杨](../Page/杨.md "wikilink")、[马](../Page/马.md "wikilink")、[王七姓苗民从](../Page/王.md "wikilink")[贵州](../Page/贵州.md "wikilink")[遵义逃荒到镇巴县青水乡海拔](../Page/遵义.md "wikilink")1600米的大楮、仁和村，过着[刀耕火种的日子](../Page/刀耕火种.md "wikilink")。境内有苗族42户255人，主要居住在该乡大楮、仁和等偏僻山区。
  - 汉中素以“小江南”著称，具有温润的气候，适宜的生活环境。
  - 汉中饮食同时兼有[米食和](../Page/米食.md "wikilink")[面食](../Page/面食.md "wikilink")，以米食为主，地方小吃以麻辣味为主，有[面皮](../Page/凉皮.md "wikilink")、粉皮、[菜豆腐](../Page/菜豆腐.md "wikilink")、麻辣[火锅](../Page/火锅.md "wikilink")、罐罐鸡、核桃馍、油茶、槟豆凉粉、烧烤等。

[P1080724.JPG](https://zh.wikipedia.org/wiki/File:P1080724.JPG "fig:P1080724.JPG")

### 友好城市

  - [出云市](../Page/出云市.md "wikilink")

  - [特恩豪特市](../Page/特恩豪特.md "wikilink")\[70\]

## 教育

### 高等教育

  - [陕西理工学院](../Page/陕西理工学院.md "wikilink")（由原“汉中师范学院”和“陕西工学院”2002年合并而成，现分别为南北校区。）
  - [汉中职业技术学院](../Page/汉中职业技术学院.md "wikilink")（专科职业院校，由原“汉中教育学院”、“汉中农业学校”、“汉中市卫生学校”、“汉中师范学校”和“汉中财经学校”合并而成）
  - [陕西航空职业技术学院](../Page/陕西航空职业技术学院.md "wikilink")（专科职业院校，原名陕南航空职工大学，前身为创建于1982年6月的O一二基地工学院。1989年经[教育部备案](../Page/中华人民共和国教育部.md "wikilink")，原航空工业部批准更名为陕南航空职工大学。2003年4月经[陕西省人民政府批准](../Page/陕西省人民政府.md "wikilink")，教育部备案改制为陕西航空职业技术学院。）

### 中等教育

  - [汉中中学](../Page/汉中中学.md "wikilink")（历史上的[汉南书院始建于乾隆年间](../Page/汉南书院.md "wikilink")，是陕南地区一所书塾性质的官办学校，今天的[汉中中学建立在原址基础上](../Page/汉中中学.md "wikilink")。另外有一所新建的陕南书院，则与前者并无关联）
  - [漢中市龍崗學校](../Page/漢中市龍崗學校.md "wikilink")（是汉中市教育局、南郑县人民政府扶持，陕西艺苑集团投资2.76亿元修建的集小学、初中、高中为一体现代化、[寄宿制](../Page/寄宿.md "wikilink")、自称“优质品牌学校”，位于汉中市“一江两岸”的城市南区。）

## 宗教、祭祀场所

  - [道教与](../Page/道教.md "wikilink")[佛教](../Page/佛教.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>宗教</p></th>
<th><p>政府批准活动场所及地址</p></th>
<th><p>批准开放时间 （始于）</p></th>
<th><p>未开放教点</p></th>
<th><p>信徒人数（约）</p></th>
<th><p>信众人数（约）</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>道教</p></td>
<td><p>天台山（武乡镇石堰村）<br />
文公祠（东关磨子桥）</p></td>
<td><p>1986年<br />
1994年</p></td>
<td><p>老君观 北海观 五郎观 东岳庙 青龙观</p></td>
<td><p>500</p></td>
<td><p>3500</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>佛教</p></td>
<td><p>万寿寺（市西郊三里店村）<br />
宝峰寺（武乡镇共力村）</p></td>
<td><p>1992年<br />
1986年</p></td>
<td><p>塔庙 金山寺 观音寺 观桂寺</p></td>
<td><p>1000</p></td>
<td><p>5000</p></td>
<td></td>
</tr>
</tbody>
</table>

  - [基督教新教](../Page/基督教新教.md "wikilink")、[天主教和](../Page/天主教.md "wikilink")[伊斯兰教](../Page/伊斯兰教.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>宗教</p></th>
<th><p>政府批准活动场所及地址</p></th>
<th><p>批准开放时间 （始于）</p></th>
<th><p>未开放教点</p></th>
<th><p>信教人数</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>天主教（详见：<a href="../Page/天主教漢中教區.md" title="wikilink">天主教漢中教區</a>）</p></td>
<td><p>天主教教堂（友爱路）<br />
周家湾堂（<a href="../Page/七里乡.md" title="wikilink">七里乡周家湾村</a>）<br />
余王村堂（<a href="../Page/望江乡.md" title="wikilink">望江乡）</a></p></td>
<td><p>1980年<br />
1992年<br />
1990年</p></td>
<td><p>刘家营堂 铺镇堂</p></td>
<td><p>2800</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>基督教（新教）</p></td>
<td><p>汉中市礼拜堂（友爱路）<br />
河东店礼拜堂（河东店）<br />
阎营活动点（<a href="../Page/龙江乡.md" title="wikilink">龙江乡</a>）</p></td>
<td><p>1980年<br />
1984年<br />
1984年</p></td>
<td><p><a href="../Page/铺镇.md" title="wikilink">铺镇</a></p></td>
<td><p>3200</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>伊斯兰教</p></td>
<td><p>团结清真寺（东关北四巷）<br />
仙隐清真寺（石马坡）<br />
铺镇清真寺（铺镇联丰村）</p></td>
<td><p>1981年<br />
1990年<br />
1984年</p></td>
<td><p>原寺内清真寺</p></td>
<td><p>3500</p></td>
<td><p>区内伊斯兰教徒主要分为三派：一是格底木，又称老教，源于元明清以来迁入。二是伊合也瓦力，又称新教、尊经派，是清末、民国以来在对老教有所改革的基础上形成。三是卡迪林耶支系，此派是<a href="../Page/康熙.md" title="wikilink">康熙年间由西乡鹿龄寺祁静一创建</a>，教徒主要分布在西乡。</p></td>
</tr>
</tbody>
</table>

\[71\]

## 参考文献

## 深入阅读

  - 《汉中盆地地理考察报告》[王德基等](../Page/王德基.md "wikilink")，1941年。

## 外部链接

  - [汉中市政府门户网站](http://www.hanzhong.gov.cn/)
  - [古汉台](http://www.guhantai.com/)
  - [CCTV:歷史名城——漢中](https://web.archive.org/web/20090412040838/http://city.cctv.com/html/lvcheng/2fbc9fba73b9db40bf4aaad56d85e153.html)

{{-}}

[Category:陕西地级市](../Category/陕西地级市.md "wikilink")
[汉中](../Category/汉中.md "wikilink")
[陕](../Category/国家历史文化名城.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14. 《魏书·地形志》中作汉阴

15.
16. 《隋书·地理志》

17.
18.
19. 宋史地理志

20.

21.
22.
23.
24.
25.

26.

27.
28. 援引《汉中地区志》第四册，2221页，2005年版，**编者按注明，由于数字不全，实际情况远不只此。**

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43. 同[沔縣](../Page/沔縣.md "wikilink")，1964年被中國國務院改為同音字

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.
57.

58.

59.

60.

61. 《地名大辞典》、《汉中地区名胜古迹》载：“濂水源出陕西南郑县南巴岭山之邓家垭，北流，经黄官岭、廉水县、新集、高台、郭滩至石拱桥的中咀汇入汉江”。

62. 《南江县志》：“隋大业三年，因此道交通便利，难江（今南江）划属汉川郡（今汉中）。”

63. 《[辞海](../Page/辞海.md "wikilink")·历史地理》：“米仓古道自陕西汉中县南、循汉水支流濂水谷道和嘉陵江支流巴江谷道到达四川巴中地区”

64.

65.

66. 《鄐君开通褒斜道》摩崖石刻载：“永平六年，汉中郡以诏书受广汉、蜀郡、巴郡徒二千六百九十人，开通褒斜道。”

67. [北魏](../Page/北魏.md "wikilink")《石门铭》：“诏遣左校令贾三德领徒一万人，万师百人共成其事。”

68.

69. 《汉中地区志》，2005年版

70.

71. 1995年数据，