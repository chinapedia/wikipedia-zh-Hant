**汉江**，亦称**汉水**，又名**襄河**，古称**水**，是[長江最长的](../Page/長江.md "wikilink")[支流](../Page/支流.md "wikilink")。汉水位处[长江中游左岸](../Page/长江中游.md "wikilink")，發源於[中国](../Page/中国.md "wikilink")[陝西省](../Page/陝西省.md "wikilink")[秦岭南麓的](../Page/秦岭.md "wikilink")[沮水](../Page/沮水_\(汉水\).md "wikilink")，[干流自西向東流经](../Page/干流.md "wikilink")[陕南和](../Page/陕南.md "wikilink")[鄂西北](../Page/湖北省.md "wikilink")，于[武汉](../Page/武汉.md "wikilink")[汉口注入長江](../Page/汉口.md "wikilink")。长1577公里，流域面积15.9万平方公里。\[1\]

[汉族的族称追根溯源即来自汉水](../Page/汉族.md "wikilink")。[秦惠文王置](../Page/秦惠文王.md "wikilink")[汉中郡](../Page/汉中郡.md "wikilink")，名字取自汉水。\[2\]后[刘邦受](../Page/刘邦.md "wikilink")[项羽封于巴](../Page/项羽.md "wikilink")、蜀、汉中，都于汉中郡[南郑](../Page/南郑.md "wikilink")，因称汉王，统一天下后亦以“汉”为[国号](../Page/国号.md "wikilink")。\[3\]\[4\][汉朝前后](../Page/汉朝.md "wikilink")400余年，[经济](../Page/经济.md "wikilink")、[文化和](../Page/文化.md "wikilink")[疆域都有了大发展](../Page/疆域.md "wikilink")，原称[华夏的](../Page/华夏.md "wikilink")[中原居民称为汉人](../Page/中原.md "wikilink")，汉人从此成为[中国主体民族的通称](../Page/中国.md "wikilink")。\[5\]

## 名称

**汉江**、**汉水**之名的源头可以追溯到[先秦时代](../Page/先秦.md "wikilink")。《[書](../Page/尚书_\(书\).md "wikilink")·[禹貢](../Page/禹貢.md "wikilink")》中就有“嶓冢<sup>（嶓冢山）</sup>導漾<sup>（[漾水河](../Page/漾水河.md "wikilink")）</sup>，東流為**漢**”\[6\]。《[詩](../Page/诗经.md "wikilink")·小雅·四月》中有“滔滔江**漢**，南國之紀”\[7\]，将汉江与长江并称为南方的两条大河。

**沔水**之称，亦可追溯至《書·禹貢》：“浮于潛，逾于**沔**”\[8\]。《[史记](../Page/史记.md "wikilink")》记载：“於是自[殽以東](../Page/殽.md "wikilink")……大川祠二……水曰[濟](../Page/济水.md "wikilink")，曰[淮](../Page/淮河.md "wikilink")。……自[華以西](../Page/华山.md "wikilink")……名川四……水曰[河](../Page/黄河.md "wikilink")……**沔**……湫淵……[江水](../Page/长江.md "wikilink")。”\[9\]《[水经](../Page/水经.md "wikilink")》中只称“沔水”，不称“汉”；[北魏](../Page/北魏.md "wikilink")[郦道元](../Page/郦道元.md "wikilink")《[水经注](../Page/水经注.md "wikilink")》中两种称呼都有使用，“互相通稱”\[10\]。但也有把沔水看成是汉水的上游段的，如[东晋](../Page/东晋.md "wikilink")[梅赜所献尚书伪](../Page/梅赜.md "wikilink")[孔传称](../Page/孔安国.md "wikilink")“漢上曰沔”\[11\]、“東南流為沔水，至[漢中東流為漢水](../Page/漢中.md "wikilink")”\[12\]。

**襄江**、**襄河**，特指汉江[襄阳段或襄阳以下](../Page/襄阳.md "wikilink")。《[明史](../Page/明史.md "wikilink")》记载：“襄陽[倚](../Page/附郭县.md "wikilink")……漢水在城北，亦曰襄江。”\[13\]《[讀史方輿紀要](../Page/讀史方輿紀要.md "wikilink")》记载：“襄河者，漢水自襄陽來也。”\[14\]

## 流域范围

汉江[流域之北为](../Page/流域.md "wikilink")[黄河流域](../Page/黄河.md "wikilink")，以秦岭、[外方山](../Page/外方山.md "wikilink")、[伏牛山为](../Page/伏牛山.md "wikilink")[分水岭](../Page/分水岭.md "wikilink")；东北为[淮河流域](../Page/淮河.md "wikilink")，以伏牛山、[桐柏山为分水岭](../Page/桐柏山.md "wikilink")；西南隔[大巴山](../Page/大巴山.md "wikilink")、[荆山与](../Page/荆山.md "wikilink")[嘉陵江](../Page/嘉陵江.md "wikilink")、[沮漳河为邻](../Page/沮漳河.md "wikilink")；东南为[江汉平原](../Page/江汉平原.md "wikilink")，与长江无明显的天然分水岭。

[流域面积达](../Page/流域面积.md "wikilink")15.9万[平方公里](../Page/平方公里.md "wikilink")，从西北至东南长约820[公里](../Page/公里.md "wikilink")，涉及陕西、湖北、[甘肃](../Page/甘肃.md "wikilink")、[四川](../Page/四川.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[河南等](../Page/河南.md "wikilink")6省的21[地](../Page/地级行政区.md "wikilink")、79[县](../Page/县级行政区.md "wikilink")。流域内总人口达3955万人（2006年），其中湖北、陕西、河南分别占53.76%、21.14%和24.53%。\[15\]

## 水系特征

[Dan_river_g.png](https://zh.wikipedia.org/wiki/File:Dan_river_g.png "fig:Dan_river_g.png")\]\]

干流全长1577公里，总落差1964米。上中游以[丹江口为界](../Page/丹江口.md "wikilink")，中下游以[钟祥为界](../Page/钟祥.md "wikilink")。上游长约925公里，流域面积约9.52万平方公里，[盆地与](../Page/盆地.md "wikilink")[峡谷相间](../Page/峡谷.md "wikilink")，河深水急；中游长约270公里，流域面积约4.68万平方公里，流经[丘陵](../Page/丘陵.md "wikilink")[河谷盆地](../Page/河谷.md "wikilink")，河床不稳定，时冲时淤；下游长约382公里，流域面积约1.7万平方公里，多曲流、[洲滩](../Page/洲滩.md "wikilink")，两岸筑有[堤防](../Page/堤防.md "wikilink")。襄阳以下，河槽上宽下窄，河道逐渐收拢，再加上可能的长江洪水[顶托](../Page/顶托.md "wikilink")，遇大水时水流难以宣泄，深受[洪灾威胁](../Page/洪灾.md "wikilink")。

流域水系呈[叶脉状](../Page/水系#叶脉状.md "wikilink")，[支流大多较短小](../Page/支流.md "wikilink")，左右岸分布不均衡。流域面积在0.5万-1.0万平方公里的支流有[旬河](../Page/旬河.md "wikilink")、[夹河](../Page/夹河.md "wikilink")、唐河（[唐白河支流](../Page/唐白河.md "wikilink")）、[南河](../Page/南河_\(漢水支流\).md "wikilink")、[汉北河](../Page/汉北河.md "wikilink")；流域面积在1.0万平方公里以上的支流有[堵河](../Page/堵河.md "wikilink")、[丹江](../Page/丹江.md "wikilink")、唐白河。其中，唐白河流域面积居首，丹江长度第一，堵河水量最大。\[16\]

## 水文要素

### 降水、蒸发

[降水以降](../Page/降水.md "wikilink")[雨](../Page/雨.md "wikilink")、降[雪为主](../Page/雪.md "wikilink")，鲜有[冰雹](../Page/冰雹.md "wikilink")、积雪。流域内年均[降水量约](../Page/降水量.md "wikilink")873[毫米](../Page/毫米.md "wikilink")，下游地区可达1100毫米以上，上中游地区约700-900毫米。时间分配不均匀，连续最大4个月降水（下游约为4-7月，上游约为7-10月）占年降水的55%-65%。

年均水面[蒸发量约为](../Page/蒸发量.md "wikilink")700-1100毫米，空间上西少东多，时间上1或12月最少。\[17\]

### 径流

流域内1956-1998年平均年[径流量为](../Page/径流量.md "wikilink")566亿[立方米](../Page/立方米.md "wikilink")。年际变化大，如皇庄[水文站](../Page/水文站.md "wikilink")<sup>钟祥</sup>最枯年径流量（182亿立方米，1999年）约为最丰年径流量（1060亿立方米，1964年）的六分之一。年内变化大，主汛期（7-10月）径流量占年径流量的65%，约为枯水期（11月-次年6月）的2倍；最大月径流量约为最小月的10倍。\[18\]

### 泥沙

[Wuhan_114.27328E_30.58114N.jpg](https://zh.wikipedia.org/wiki/File:Wuhan_114.27328E_30.58114N.jpg "fig:Wuhan_114.27328E_30.58114N.jpg")清浊分明。\]\]

汉江本是长江中游的主要产沙区，为湖北省内[输沙模数最大的地区](../Page/输沙模数.md "wikilink")，干流年均含沙量为0.5-2.0[千克每立方米](../Page/千克.md "wikilink")。7、8、9月的来沙量占全年的80%以上。[丹江口水库建成后](../Page/丹江口水库.md "wikilink")，泥沙淤积库内不出，中下游沙情大幅改善，黄家港水文站<sup>丹江口</sup>年均输沙量减至建库前的1.2%，皇庄年均输沙量减至建库前的16.5%。\[19\]

### 水质

汉江干流[水质总体较好](../Page/水质.md "wikilink")：数据表明，水质达[Ⅱ类以上河段占评价河长的](../Page/水质#水质分类.md "wikilink")78%；Ⅲ类河段占13.82%，多在中下游地区；Ⅳ类河段占7.78%，主要在上游的[汉中](../Page/汉中.md "wikilink")、[安康地区](../Page/安康.md "wikilink")。支流中，丹江水质较好，所有测站全年水质均达到Ⅱ类，唐白河水质较差。\[20\]

## 地形地势

汉江流域地势西高东低。西部为[秦](../Page/秦岭.md "wikilink")[巴山地](../Page/大巴山.md "wikilink")，[海拔](../Page/海拔.md "wikilink")1000-3000米；中部为[南襄盆地](../Page/南襄盆地.md "wikilink")，海拔100-300米；东部为江汉平原，平均海拔20-40米。干流总落差1964米，河口处海拔18米。流域内[山地占](../Page/山地.md "wikilink")55%，主要是西部的中低山区；丘陵占21%，多分布于南襄盆地和江汉平原的边缘地带；平原占23%，主要为南襄盆地、江汉平原和汉江河谷；湖泊1%，基本分布于江汉平原。\[21\]

## 自然灾害

### 水灾

汉江流域早在公元前就有关于[洪水的记载](../Page/洪水.md "wikilink")。平均约一百年就可能发生一次特大洪水。1416年以来，特大洪水的记述更多，1583年至今发生了7次特大洪水。1822年-1949年，汉江干流堤防发生溃决的有76年，平均2年不到溃决一次。

1583年（[万历十一年](../Page/万历.md "wikilink")），汉江满溢，覆没[金州城](../Page/金州_\(陕西\).md "wikilink")。

1832年、1852年、1867年、1921年、1935年，汉江均发生了特大洪灾。

1954年，长江流域发生[特大洪水](../Page/1954年长江洪水.md "wikilink")，汉江洪水与长江洪水遭遇，[潜江市汉江右岸堤坝溃决](../Page/潜江市.md "wikilink")，造成45.3万人受灾。

1983年，汉江全流域大暴雨，安康市城区遭受[灭顶之灾](../Page/1983年安康水灾.md "wikilink")，直接损失达8亿元；下游局部地区受灾，受灾人口接近9万。

1990年，汉江上游发生百年一遇特大洪水，汉中平川段100万人受灾。\[22\]

### 旱灾

汉江流域的[旱灾多为轻旱](../Page/旱灾.md "wikilink")。上游多夏旱，[商洛](../Page/商洛.md "wikilink")、安康、汉中等地区平均受旱率在20%上下；中下游多[伏旱](../Page/三伏.md "wikilink")，襄阳为重旱灾害多发区，极端干旱机遇最大的则数[十堰](../Page/十堰.md "wikilink")。\[23\]

### 水土流失

汉江全流域都存在不同程度的[水土流失](../Page/水土流失.md "wikilink")，水土流失面积约为6.26万平方公里，其中以汉江干流沿岸、丹江中上游和汉江源头区最为严重。\[24\]

### 地震

汉江流域大部分处于弱[震区](../Page/地震.md "wikilink")，很少发生较强地震。只有汉中-安康一个中强[地震带](../Page/地震带.md "wikilink")；788年（[贞元四年](../Page/贞元_\(唐朝\).md "wikilink")）安康曾发生[6.5级大地震](../Page/黎克特制地震震級.md "wikilink")。

丹江口水库1967年蓄水后，产生[诱发地震](../Page/诱发地震.md "wikilink")，十几年内大小地震500余次。最大地震发生在1973年，震级4.7级。1980年后，库区地震数量减少，强度降低，趋于平稳。\[25\]

## 水能开发

汉江流域[水力资源全流域理论总蕴藏量](../Page/水力资源.md "wikilink")1083.16万[千瓦](../Page/千瓦.md "wikilink")，其中技术可开发量装机容量约820万千瓦，经济可开发量装机容量约780万千瓦。水力资源集中于上游，占全流域的60%以上；干流占水力资源总量的47.1%，支流52.9%；中小型电站占62.4%，大型电站占37.6%。

汉江全流域已经或在建水电站168座，总装机容量367.49万千瓦，年发电量126.86亿[千瓦时](../Page/千瓦时.md "wikilink")。干流现已建造[石泉](../Page/石泉水电站.md "wikilink")、[喜河](../Page/喜河水电站.md "wikilink")、[安康](../Page/安康水电站.md "wikilink")、[丹江口](../Page/丹江口水电站.md "wikilink")、[王甫洲](../Page/王甫洲水电站.md "wikilink")、[蜀河](../Page/蜀河水电站.md "wikilink")、[崔家营](../Page/崔家营水电站.md "wikilink")、[兴隆等](../Page/兴隆水利枢纽.md "wikilink")8座水电站。\[26\]

## 航运情况

[0275-Wuhan-Hanjiang.jpg](https://zh.wikipedia.org/wiki/File:0275-Wuhan-Hanjiang.jpg "fig:0275-Wuhan-Hanjiang.jpg")航運。\]\]
汉江干流从汉中[洋县开始可以通航](../Page/洋县.md "wikilink")，通航里程1342公里。

各河段[航道等级](../Page/中国内河航道等级.md "wikilink")：[洋县](../Page/洋县.md "wikilink")—黄瓜架，等级以下；黄瓜架—安康水库，Ⅳ级；安康水库-[白河](../Page/白河县.md "wikilink")，Ⅵ级；白河—[神定河](../Page/神定河.md "wikilink")<sup>[郧阳区](../Page/郧阳区.md "wikilink")</sup>，Ⅶ级；神定河—丹江口，Ⅴ级；丹江口—皇庄，Ⅳ级；干流下游段，全年Ⅴ级，中洪水期Ⅳ级；河口段（[蔡甸](../Page/蔡甸.md "wikilink")—河口）全年Ⅳ级，中洪水期Ⅲ级。\[27\]

## 各段分述

### 源头

汉江有北源[沮水](../Page/沮水_\(汉水\).md "wikilink")、中源漾水河、南源[玉带河三个源头](../Page/玉带河.md "wikilink")，均位于陕西省[汉中市内](../Page/汉中市.md "wikilink")。

中源漾水河发源于[宁强县东北部](../Page/宁强县.md "wikilink")，流程较短。南源玉带河发源于宁强县南部，长101公里。北源沮水发源于[留坝县与](../Page/留坝县.md "wikilink")[宝鸡市](../Page/宝鸡市.md "wikilink")[凤县交界的](../Page/凤县.md "wikilink")[紫柏山南麓的黄花坪沟](../Page/紫柏山.md "wikilink")，向西南流，穿过[勉县后](../Page/勉县.md "wikilink")，在[略阳县内的](../Page/略阳县.md "wikilink")[黑河坝乡与白河汇合](../Page/黑河坝乡.md "wikilink")，然后又向东南流回勉县境内；以白河[口为界](../Page/河口.md "wikilink")，上游称黑河，长112公里，下游称沮水，长18公里。

在勉县，漾水河与玉带河先在[新铺镇炭厂寺汇合](../Page/新铺镇.md "wikilink")，随后沮水在[土关铺乡沮水村汇入](../Page/土关铺乡.md "wikilink")，三源汇合后始称汉江，已颇具大河规模，自西向东流至[武侯镇后进入](../Page/武侯镇.md "wikilink")[汉中盆地](../Page/汉中盆地.md "wikilink")。\[28\]

#### 正源

中源漾水河长度、流域面积、流量均较小，却在传统上被视为正源。《書·禹貢》：“嶓冢導漾，東流為漢。”\[29\]学者认为，在《尚書》时代之后，古汉水上游发生了重大的水系变迁，[西汉初](../Page/西汉.md "wikilink")[武都地区的大地震](../Page/武都.md "wikilink")，最终导致原为古汉水上游的[西汉水被](../Page/西汉水.md "wikilink")[袭夺](../Page/袭夺.md "wikilink")，成了嘉陵江上游；这也可以解释现在的漾水河为何会拥有跟河流大小不相符的宽广[河谷和普遍](../Page/河谷.md "wikilink")[沉积现象](../Page/沉积.md "wikilink")。\[30\]\[31\]

今汉江的正源，《中国河湖大典》根据“河源唯远”原则，定为北源沮水。\[32\]也有学者根据河谷的一贯性，排除出自逼仄峡谷的南北两源，定正源为河谷宽广的中源漾水河。\[33\]也有工具书定正源为南源玉带河，如《[中国大百科全书](../Page/中国大百科全书.md "wikilink")》认为汉江源自[米仓山西麓](../Page/米仓山.md "wikilink")。\[34\]

### 上游

汉江流出河源区后，从勉县武侯镇开始，穿越[南郑县](../Page/南郑县.md "wikilink")、[汉中市区](../Page/汉中.md "wikilink")、[城固县](../Page/城固县.md "wikilink")，至洋县[龙亭镇大龙河口才又进入峡谷](../Page/龙亭镇.md "wikilink")。该段河长约105公里，宽300-400米（洪水期1000-2000米），流域面积万余平方公里，中间即为[汉中盆地](../Page/汉中盆地.md "wikilink")。该河段内，穿过盆地进入汉江的支流，北有[褒河](../Page/褒河.md "wikilink")、[湑水河](../Page/湑水河.md "wikilink")、[灙水河](../Page/灙水河.md "wikilink")，南有[漾家河](../Page/漾家河.md "wikilink")、[濂水河](../Page/濂水河.md "wikilink")、[冷水河](../Page/冷水河_\(汉江\).md "wikilink")、[南沙河等](../Page/南沙河.md "wikilink")。

汉江流出汉中盆地，即进入一段长约600公里的峡谷河段，河床狭窄，河谷陡峻，水流湍急，一泻千里，水力资源丰富，丹江口水电站、安康水电站等均坐落于这片峡谷中。

  - 从大龙河口到[黄金峡镇渭门村的一段](../Page/黄金峡镇.md "wikilink")，长50余公里，是汉江上游最为险峻的峡谷段，上段称小峡，北纳[酉水河](../Page/酉水河.md "wikilink")，下段即著名的黄金峡，又称大峡，先向北、再向东、再向东南、再向南兜一个大圈，收纳[金水河](../Page/金水河.md "wikilink")、[子午河](../Page/子午河.md "wikilink")。
  - 从渭门村出，汉江往东南方向流去，纳[牧马河](../Page/牧马河.md "wikilink")，然后流入石泉水库。[石泉县至](../Page/石泉县.md "wikilink")[白河县为峡谷盆地交错段](../Page/白河县.md "wikilink")，包括石泉盆地、[紫阳峡谷](../Page/紫阳县.md "wikilink")、[安康盆地](../Page/安康.md "wikilink")、[旬阳峡谷](../Page/旬阳.md "wikilink")、白河峡谷等，安康城上游建有[安康水库](../Page/安康水库.md "wikilink")。该段左岸有[池河](../Page/池河_\(汉水\).md "wikilink")、[月河](../Page/月河_\(汉水支流\).md "wikilink")、[旬河](../Page/旬河.md "wikilink")、[夹河等汇入](../Page/夹河.md "wikilink")，右岸有[黄洋河](../Page/黄洋河.md "wikilink")、[岚河](../Page/岚河.md "wikilink")、[坝河等汇入](../Page/坝河_\(汉水支流\).md "wikilink")。从陕西省旬阳县[兰滩乡](../Page/兰滩乡.md "wikilink")/湖北省[郧西县兰滩开始](../Page/郧西县.md "wikilink")，汉江成为两省的[界河](../Page/界河.md "wikilink")。
  - 出白河县后，汉江干流全部进入湖北省境内，至郧阳区[柳陂镇堵河口](../Page/柳陂镇.md "wikilink")，纳右岸的[堵河](../Page/堵河.md "wikilink")，然后流至[丹江口市](../Page/丹江口市.md "wikilink")，纳左岸的[丹江](../Page/丹江.md "wikilink")，从白河县至丹江口市河段长223公里。丹江口建有[丹江口水库](../Page/丹江口水库.md "wikilink")，[南水北调中线工程即从丹江口水库引汉江水向北](../Page/南水北调中线工程.md "wikilink")。\[35\]

### 中游

[The_Town_of_Fancheng,_on_the_Bank_of_the_Han_Jiang_(Han_River)._Hubei_Province,_China,_1874_WDL2101.png](https://zh.wikipedia.org/wiki/File:The_Town_of_Fancheng,_on_the_Bank_of_the_Han_Jiang_\(Han_River\)._Hubei_Province,_China,_1874_WDL2101.png "fig:The_Town_of_Fancheng,_on_the_Bank_of_the_Han_Jiang_(Han_River)._Hubei_Province,_China,_1874_WDL2101.png")汉江河岸（1874）\]\]

汉江自丹江口开始，流向由西-东变为西北-东南，穿行于低山丘陵间，然后流入[老河口市](../Page/老河口市.md "wikilink")，市区下游建有王甫洲水库，作为丹江口水库的反调节水库。出老河口市区后，划过一个弧形，流入[谷城县境内](../Page/谷城县.md "wikilink")，谷城下游有[南河从右岸注入](../Page/南河_\(汉水支流\).md "wikilink")。然后继续东南流，流至[襄阳市](../Page/襄阳市.md "wikilink")，[唐白河在左岸入汇](../Page/唐白河.md "wikilink")。丹江口-襄阳段为丘陵平原，河道弯曲，河床开阔，江面低水位宽300-400米，洪水期宽2000-3000米，[心滩](../Page/心滩.md "wikilink")、洲滩发育。

出襄阳后，江面逐渐加宽，流经[宜城](../Page/宜城.md "wikilink")，[蛮河自右岸注入](../Page/蛮河.md "wikilink")。继续东南流，进入[钟祥市境内](../Page/钟祥市.md "wikilink")。襄阳-钟祥段位于汉江[地堑](../Page/地堑.md "wikilink")，河道弯曲，水流散乱，江面低水位宽300-400米，洪水期宽2000-3000米、甚至8000-10000米；心滩、洲滩众多，枯水时河汊密布，洪水时汪洋一片。\[36\]

### 下游

[Wuhan,_Han_river-2.jpg](https://zh.wikipedia.org/wiki/File:Wuhan,_Han_river-2.jpg "fig:Wuhan,_Han_river-2.jpg")\]\]

钟祥以下，汉江进入江汉平原。下游两岸均修筑堤防，汉江在其约束下，从钟祥南流至[沙洋县](../Page/沙洋县.md "wikilink")，然后转向东流。至[潜江市泽口](../Page/潜江市.md "wikilink")，右岸有汉江最大[分流](../Page/分流_\(水文学\).md "wikilink")[东荆河](../Page/东荆河.md "wikilink")。然后经过[天门市](../Page/天门市.md "wikilink")、[仙桃市](../Page/仙桃市.md "wikilink")，流至[汉川市](../Page/汉川市.md "wikilink")，左岸纳[汈汊湖](../Page/汈汊湖.md "wikilink")、[汉北河](../Page/汉北河.md "wikilink")。最后进入[武汉市](../Page/武汉市.md "wikilink")，于[汉口龙王庙汇入长江](../Page/汉口.md "wikilink")。

据1995年统计，汉江干流下游堤防总长约727公里，左岸堤防自钟祥至汉口，长约369公里；右岸堤防自沙洋至[汉阳](../Page/汉阳.md "wikilink")，长约358公里。下游左岸的[遥堤](../Page/遥堤.md "wikilink")，上起钟祥市罗汉寺，下至天门市多宝湾，是汉江最重要的堤防。1935年，汉江特大洪灾，中游左岸堤防溃决达4公里，淹没汉北平原33.33万公顷农田，死亡8万余人，重修时放弃原堤段，留下200余平方公里的自然滞洪区，修建罗汉寺至旧口的遥堤18.2公里，后将旧口至多宝湾的堤段并入，合计56.26公里。

下游堤防从明代开始修筑，历经演变，河道越往下越收拢，遥堤末端约宽3500米，至汉川市仅300-400米，汉口附近只剩100米左右，遇上洪水，排泄不畅，容易酿成洪灾。汉川河段的河道安全泄量只有9000[m³](../Page/立方米.md "wikilink")/[s](../Page/秒.md "wikilink")，长江水位较高时则只有5000m³/s，而1935年特大洪灾中游洪峰流量超过50000m³/s。因此，为预防洪灾，1956年于仙桃市附近汉江右岸修建了[杜家台分洪工程](../Page/杜家台分洪工程.md "wikilink")。\[37\]

## 参考文献

## 外部链接

  -
## 参见

  - [江汉平原](../Page/江汉平原.md "wikilink")
  - [南水北调工程](../Page/南水北调工程.md "wikilink")、[引汉济渭工程](../Page/引汉济渭工程.md "wikilink")
  - [引江济汉工程](../Page/引江济汉工程.md "wikilink")、[引江补汉工程](../Page/引江补汉工程.md "wikilink")

{{-}}

[Category:汉水水系](../Category/汉水水系.md "wikilink")
[Category:湖北河流](../Category/湖北河流.md "wikilink")
[Category:陕西河流](../Category/陕西河流.md "wikilink")

1.

2.  “漢中郡……秦惠文王置郡。因水名也。”

3.  “更立沛公為漢王，王巴、蜀、漢中，都南鄭。”

4.  “漢中郡……縣十二：……南鄭……”

5.

6.

7.

8.
9.

10.

11.

12.

13.

14.

15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.

31.

32.
33.
34.

35.
36.
37.