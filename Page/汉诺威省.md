<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>Provinz Hannover</strong><br />
<strong>汉诺瓦省</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Flagge_Preußen_-_Provinz_Hannover.svg" title="fig:Flagge_Preußen_-_Provinz_Hannover.svg">Flagge_Preußen_-_Provinz_Hannover.svg</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Wappen_Preußische_Provinzen_-_Hannover.png" title="fig:Wappen_Preußische_Provinzen_-_Hannover.png">Wappen_Preußische_Provinzen_-_Hannover.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>[ <a href="../Page/旗帜.md" title="wikilink">旗帜</a> ]</small></p></td>
<td style="text-align: center;"><p><small>[ <a href="../Page/徽章.md" title="wikilink">徽章</a> ]</small></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>存在时期</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/首府.md" title="wikilink">首府</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/人口.md" title="wikilink">人口</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/人口密度.md" title="wikilink">人口密度</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>版图</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:German_Empire_-_Prussia_-_Hanover_(1871).svg" title="fig:German_Empire_-_Prussia_-_Hanover_(1871).svg">German_Empire_-_Prussia_-_Hanover_(1871).svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1871年德意志帝国时期，普鲁士王国内的汉诺瓦省</p></td>
</tr>
</tbody>
</table>

**汉诺瓦省**（）是1868年王1946年[普鲁士和](../Page/普鲁士.md "wikilink")[普鲁士自由邦的一个省分](../Page/普鲁士自由邦.md "wikilink")。

在[普奥战争期间](../Page/普奥战争.md "wikilink")，[汉诺瓦王国曾尝试与其它一些参与](../Page/漢諾瓦_\(地區\).md "wikilink")[德意志邦联的城邦一同维持中立地位](../Page/德意志邦联.md "wikilink")。当1866年6月14日汉诺瓦投票同意动员德意志邦联军队与普鲁士作战的时候，这就被普鲁士视为能向汉诺瓦宣战的借口。之后不久，汉诺瓦王国很快就被打败，并被普鲁士合并。被罢黜的[漢諾瓦王朝的私人财产则为](../Page/漢諾瓦王朝.md "wikilink")[奥托·冯·俾斯麦提供资金](../Page/奥托·冯·俾斯麦.md "wikilink")，以利用对付巴伐利亚的[路德维希二世](../Page/路德维希二世_\(巴伐利亚\).md "wikilink")。

1946年，英国占领区将汉诺瓦省、[奥尔登堡](../Page/奥尔登堡自由邦.md "wikilink")、[不伦瑞克和](../Page/不伦瑞克自由邦.md "wikilink")[绍姆堡-利泊等地组成新的](../Page/绍姆堡-利泊自由邦.md "wikilink")[下萨克森州的主要部分](../Page/下萨克森.md "wikilink")，而[汉诺瓦则成为这个新州分的首府](../Page/汉诺瓦.md "wikilink")。

[Hannover_Oldenburg_Braunschweig_1905.png](https://zh.wikipedia.org/wiki/File:Hannover_Oldenburg_Braunschweig_1905.png "fig:Hannover_Oldenburg_Braunschweig_1905.png")

## 另见

  - [汉诺瓦](../Page/汉诺瓦.md "wikilink")
  - [汉诺瓦王国](../Page/漢諾瓦_\(地區\).md "wikilink")
  - [下萨克森](../Page/下萨克森.md "wikilink")

[Category:普鲁士历史](../Category/普鲁士历史.md "wikilink")
[Category:1868年建立的國家或政權](../Category/1868年建立的國家或政權.md "wikilink")
[Category:1946年終結的國家或政權](../Category/1946年終結的國家或政權.md "wikilink")
[Category:1868年普魯士建立](../Category/1868年普魯士建立.md "wikilink")
[Category:1946年德國廢除](../Category/1946年德國廢除.md "wikilink")