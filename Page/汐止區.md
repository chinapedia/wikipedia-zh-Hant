[Xizhi_District_Administration_Center_20120804.jpg](https://zh.wikipedia.org/wiki/File:Xizhi_District_Administration_Center_20120804.jpg "fig:Xizhi_District_Administration_Center_20120804.jpg")
**汐止區**
（），是[中華民國](../Page/中華民國.md "wikilink")[新北市的](../Page/新北市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")，屬於[台北都會區的](../Page/台北都會區.md "wikilink")[衛星都市](../Page/衛星都市.md "wikilink")；1999年6月28日因人口超過十五萬人，由[鎮改制為](../Page/鎮_\(中華民國\).md "wikilink")[縣轄市](../Page/縣轄市.md "wikilink")。2010年12月25日，因原來[臺北縣升格為](../Page/新北市.md "wikilink")[直轄市並改名為新北市](../Page/直轄市.md "wikilink")，所以便由原來台北縣所屬的縣轄市汐止市改制為新北市所屬的汐止區；現今人口20萬餘人。

汐止區位於新北市東北部，西以[大坑溪](../Page/大坑溪_\(新北市\).md "wikilink")、[基隆河](../Page/基隆河.md "wikilink")、[內溝溪與](../Page/內溝溪.md "wikilink")[台北市相鄰](../Page/台北市.md "wikilink")，東則以[丘陵地與](../Page/丘陵.md "wikilink")[臺灣省](../Page/臺灣省.md "wikilink")[基隆市相鄰](../Page/基隆市.md "wikilink")，介於[台北市和](../Page/台北市.md "wikilink")[基隆市之間](../Page/基隆市.md "wikilink")。全區地形多元，僅在基隆河沿岸有狹長[平原](../Page/平原.md "wikilink")，氣候常年多[雨](../Page/雨.md "wikilink")。

## 歷史

汐止發展，始於明清時期。明朝崇禎十三年（1640年），荷蘭人進兵台北，驅西班牙人而有其地。文獻所記載之「峰仔峙社」，約為今日汐止基隆河南北兩岸，原多為原住民所居。清乾隆初葉，有廣東人從淡水港溯河，轉基隆河抵達今日汐止境內，與凱達格蘭平埔族墾殖，並建「峰仔峙莊」，以基隆河為主要的經濟動脈。至乾隆三十年（1765年）遂發展成為「峰仔峙街」，即為現在的中正路，迄今，汐止人仍習慣以「街仔」來稱呼。清咸豐十年（1860年）台灣開港後，許多外國人來台設立洋行進行貿易。淡水至大稻埕（迪化街附近）間，機動船隻往來頻繁；此時商人溯淡水河而上，沿河收購茶葉。
汐止及其它附近山地因自然環境適合種，因此茶山遍遍。汐止附近山區的茶農，如平溪、石碇、瑞芳等地，先將茶葉運至水返腳街上的茶館，茶館將茶葉精製好後，即利用基隆河運至大稻埕輸出；又由於汐止位於基隆河中游，有許多支流深入附近山區，使汐止成為附近山區土產輸出及雜貨輸入的集散地。汐止於清光緒廿年（1894年），隸屬基隆廳石碇堡轄區。

汐止境內昔為[凱達格蘭族](../Page/凱達格蘭族.md "wikilink")「峰仔峙社」（Kypanas）所在地。據[余文儀](../Page/余文儀.md "wikilink")《[續修台灣府志](../Page/續修台灣府志.md "wikilink")》記載，在1758年（乾隆二十三年）前後，漢人已在此形成街肆，延用平埔族的舊名，稱為「峰仔峙庄」。

汐止舊名，在[清朝](../Page/清朝.md "wikilink")[臺灣](../Page/臺灣府.md "wikilink")[方志上](../Page/方志.md "wikilink")，是稱為**「水返腳」**（《[淡水廳志](../Page/淡水廳志.md "wikilink")》、《[噶瑪蘭廳志](../Page/噶瑪蘭廳志.md "wikilink")》...等是記為「水返腳」），或**「水轉腳」**（《[福建通志](../Page/福建通志.md "wikilink")[臺灣府](../Page/臺灣府.md "wikilink")》、《[噶瑪蘭志略](../Page/噶瑪蘭志略.md "wikilink")》...等是稱為「水轉腳」），但於[臺灣話兩者意思皆通](../Page/臺灣話.md "wikilink")，所以清朝時是通用或混用。而以[臺灣話發音來講](../Page/臺灣話.md "wikilink")，理應是「水轉跤」為正確字，但民間積非成是，所以後來無論官方民間，都改為通用寫成「水返腳」（但仍念成「水轉跤」的閩南語音tsuí-tńg-kha）。

民間流傳「水返腳」或「水轉腳」地名源起，乃因為[基隆河受海水](../Page/基隆河.md "wikilink")[潮汐影響](../Page/潮汐.md "wikilink")，[漲潮至此](../Page/漲潮.md "wikilink")，故稱之。然而，基隆河雖是[感潮河川](../Page/感潮河川.md "wikilink")，海水對於基隆河的影響大約只到圓山一帶，並不會真正的漲至汐止。但是河川受潮汐影響，會有水位及流速的周期性變化，若是大潮又逢河川水量大增，極易造成洪患（如溫妮颱風、賀伯颱風）。

[日治時期取地名原義](../Page/台灣日治時期.md "wikilink")，被[日本人雅化改為和式地名](../Page/日本人.md "wikilink")「汐止」（；*shiodome*），並沿用至今。故[陳培桂的](../Page/陳培桂.md "wikilink")《淡水廳志》載道：「水返腳，謂潮漲至此地。」水返腳位[台北盆地東北隅](../Page/台北盆地.md "wikilink")，基隆河中游，西北方為[大武崙丘陵](../Page/大武崙丘陵.md "wikilink")，東南方為[南港山系](../Page/南港山.md "wikilink")，四周有[大尖山](../Page/大尖山.md "wikilink")、[姜仔寮山](../Page/姜仔寮山.md "wikilink")、[五指山等](../Page/五指山.md "wikilink")。

清[咸豐十年](../Page/咸豐.md "wikilink")（1860年）[台灣開港後](../Page/台灣開港.md "wikilink")，許多外國人來台設立[洋行進行貿易](../Page/洋行.md "wikilink")。[淡水至](../Page/淡水區.md "wikilink")[大稻埕](../Page/大稻埕.md "wikilink")（今日[台北市](../Page/台北市.md "wikilink")[迪化街附近](../Page/迪化街.md "wikilink")）間，機動船隻往來頻繁；此時商人溯淡水河而上，沿河收購[茶葉](../Page/茶葉.md "wikilink")。汐止及其它附近山地因自然環境適合種茶，因此茶山遍遍。汐止附近山區的茶農，如[平溪](../Page/平溪區.md "wikilink")、[石碇](../Page/石碇區.md "wikilink")、[瑞芳等地](../Page/瑞芳區.md "wikilink")，先將茶葉運至水返腳街上的茶館，茶館將茶葉精製好後，即利用基隆河運至大稻埕輸出；又由於汐止位於基隆河中游，有許多支流深入附近山區，使汐止成為附近山區土產輸出及雜貨輸入的集散地。

以前基隆河水量豐沛尚未淤積，從[平溪](../Page/平溪區.md "wikilink")（石底）迤邐至此，由台北注入[淡水河後再流入海中](../Page/淡水河.md "wikilink")。因海水潮汐至水返腳而止，讓此段河道行船便利成為水返腳先民及附近[鄉民出入的主要交通要道](../Page/鄉民.md "wikilink")，民眾要進出[臺北城買賣貨物或辦公洽事](../Page/臺北城.md "wikilink")，都在[濟德宮](../Page/濟德宮.md "wikilink")[媽祖廟的](../Page/媽祖.md "wikilink")[牛稠頭上下渡船](../Page/牛稠頭.md "wikilink")，也因此地是海水潮汐的界點又是基隆河水運的終點造就水返腳早期的繁榮及人文的鼎盛。

日治初期，因火車的運費太貴，故商家仍利用船運。大稻埕為本島貨物之重要集散地，由[中國大陸及日本輸入的貨物由淡水港進口](../Page/中國大陸.md "wikilink")，經淡水河運至大稻埕，再溯基隆河而上至水返腳。由《[淡水廳志](../Page/淡水廳志.md "wikilink")》的記載，與汐止相關的渡口共有十二個，上行可通往基隆，也可往基隆河下游到[台北市](../Page/臺北市.md "wikilink")，汐止也因為交通的便利繁華一時。

汐止區境內多山多水，景緻優美，又因為早期周圍山區擁有豐富的[煤礦](../Page/煤礦.md "wikilink")、[楠木及](../Page/楠木.md "wikilink")[樟樹](../Page/樟樹.md "wikilink")，讓先民得以利用這些天然的資源發跡，曾興過許多大家族。而[縱貫線穿過汐止區](../Page/縱貫線_\(鐵路\).md "wikilink")，為基隆往臺北的交通要道。日治時期，此地為一宗教人文繁盛之地，深具地方特色。

### 行政區沿革

  - 1894年
    為[基隆廳](../Page/基隆廳.md "wikilink")[石碇堡轄區](../Page/石碇堡.md "wikilink")。
  - 1897年 左右，隸屬台北縣水返腳辦務署石碇堡。
  - 1909年 改為[台北廳水返腳支廳水返腳區](../Page/臺北廳.md "wikilink")。
  - 1920年
    改制為[台北州](../Page/臺北州.md "wikilink")[七星郡](../Page/七星郡.md "wikilink")[汐止街](../Page/汐止街.md "wikilink")。
  - 1945年12月 改制為[臺北縣七星區汐止鎮](../Page/新北市.md "wikilink")。
  - 1947年2月4日 裁撤七星區署，改隸為台北縣[淡水區汐止鎮](../Page/淡水郡.md "wikilink")。
  - 1950年9月 撤廢淡水區署，改為台北縣汐止鎮。
  - 1999年6月28日 因人口超過十五萬人，改制為台北縣汐止市。
  - 2010年12月25日 改制為新北市汐止區。

## 歷任首長

| 屆別     | 姓名                               | 任期                     | 備註                                                                                     |
| ------ | -------------------------------- | ---------------------- | -------------------------------------------------------------------------------------- |
| 1      | [李勝德](../Page/李勝德.md "wikilink") | 1951年7月1日\~1953年7月1日   |                                                                                        |
| 2      | 1953年7月1日\~1956年7月1日             |                        |                                                                                        |
| 3      | 1956年7月1日\~1960年1月16日            |                        |                                                                                        |
| 4      | [楊明遠](../Page/楊明遠.md "wikilink") | 1960年1月16日\~1964年3月1日  |                                                                                        |
| 5      | [唐四明](../Page/唐四明.md "wikilink") | 1964年3月1日\~1968年3月1日   |                                                                                        |
| 6      | 1968年3月1日\~1973年4月1日             |                        |                                                                                        |
| 7      | [張清祥](../Page/張清祥.md "wikilink") | 1973年4月1日\~1977年12月30日 |                                                                                        |
| 8      | 1977年12月30日\~1982年3月1日           |                        |                                                                                        |
| 9      | [余達德](../Page/余達德.md "wikilink") | 1982年3月1日\~1986年2月28日  |                                                                                        |
| 10     | 1986年3月1日\~1990年2月28日            |                        |                                                                                        |
| 11     | [廖學廣](../Page/廖學廣.md "wikilink") | 1990年3月1日\~1994年2月28日  |                                                                                        |
| 12     | 1994年3月1日\~1995年11月1日            | 1995年11月2日因案停職         |                                                                                        |
| 代理     | [蕭坤山](../Page/蕭坤山.md "wikilink") | 1995年11月2日\~1996年6月25日 |                                                                                        |
| 12（補選） | [周雅淑](../Page/周雅淑.md "wikilink") | 1996年6月26日\~1998年2月28日 |                                                                                        |
| 13     | [周麗美](../Page/周麗美.md "wikilink") | 1998年3月1日\~1999年6月27日  | 1999年6月28日,任內由[鎮改制為](../Page/鎮_\(中華民國\).md "wikilink")[縣轄市](../Page/縣轄市.md "wikilink") |

台北縣汐止鎮時期\[1\]

| 屆別    | 姓名                               | 任期                          | 備註                                                                                     |
| ----- | -------------------------------- | --------------------------- | -------------------------------------------------------------------------------------- |
| 1     | [周麗美](../Page/周麗美.md "wikilink") | 1999年6月28日\~2002年2月28日      | 1999年6月28日,任內由[鎮改制為](../Page/鎮_\(中華民國\).md "wikilink")[縣轄市](../Page/縣轄市.md "wikilink") |
| 2     | [黃建清](../Page/黃建清.md "wikilink") | 2002年3月1日\~2006年2月28日       |                                                                                        |
| 3     | 2006年3月1日\~2009年11月6日            | 2009年11月7日\~2009年12月29日因案停職 |                                                                                        |
| 代理    | [林煌源](../Page/林煌源.md "wikilink") | 2009年11月7日\~2009年12月29日     |                                                                                        |
| 3（復職） | [黃建清](../Page/黃建清.md "wikilink") | 2009年12月30日\~2010年12月24日    |                                                                                        |

台北縣汐止市時期\[2\]

| 屆別 | 姓名                               | 任期                      | 備註 |
| -- | -------------------------------- | ----------------------- | -- |
| 1  | [吳興邦](../Page/吳興邦.md "wikilink") | 2010年12月25日\~2012年7月31日 |    |
| 2  | [徐開宇](../Page/徐開宇.md "wikilink") | 2012年8月1日\~2019年2月1日    |    |
| 3  | [陳健民](../Page/陳健民.md "wikilink") | 2019年2月1日\~             | 現任 |

新北市汐止區時期\[3\]

## 地理

汐止區面積71.2873平方公里，居[新北市各區之第九位](../Page/新北市.md "wikilink")。

汐止位於[台北盆地外緣東南隅](../Page/台北盆地.md "wikilink")，南邊以[南港山脈與](../Page/南港山.md "wikilink")[平溪區](../Page/平溪區.md "wikilink")、[石碇區為鄰](../Page/石碇區.md "wikilink")：北邊以[五指山山脈](../Page/五指山山脈.md "wikilink")\[4\]與[萬里區](../Page/萬里區.md "wikilink")、台北[士林區](../Page/士林區.md "wikilink")、[內湖區相鄰](../Page/內湖區.md "wikilink")；東鄰[臺灣省](../Page/臺灣省.md "wikilink")[基隆市](../Page/基隆市.md "wikilink")[七堵區](../Page/七堵區.md "wikilink")、[暖暖區](../Page/暖暖區.md "wikilink")；西南以河川與[南港區為界](../Page/南港區.md "wikilink")。四面環山，且交通便利，近年已成為大台北地區熱門的健行登山路線。

### 四極

汐止區為低緯度地區，屬台灣標準時區，由以下地理疆域資料可詳見汐止之經緯度位置：

極東：東山里 東南角，東經121度43分。

極西：八連里西北角，東經121度36分，東西共跨經度7分。

極南：白雲里南界角，北緯25度2分。

極北：烘內里東北角，北緯25度9分，南北共跨緯度7分。

### 地質

汐止境內山系主要由頁岩、砂岩所組成，含跨[中新世](../Page/中新世.md "wikilink")、[上新世](../Page/上新世.md "wikilink")、[更新世等地層](../Page/更新世.md "wikilink")，就《臺灣山岳一覽表》得知，汐止境內有命名、查勘過的山至少有四十六座，其中以廿座山峰較為人們所青睞，計有：大尖山（大針山）、四分尾山、槓尾山、五指山、新山、柯子林山、金面山、烘內山、狗寮窟崙、鵠鵠崙、松柏崎山等，這些山區近年已成為大臺北地區熱門的健行登山路線。名勝山岳行汐止境內二百至五百公尺的名山非常多，各有特色，且附近交通便利，在大臺北地區新郊熱門的登山路線享負盛名，因此吸引不少山友、遊客探尋一番。

### 地形

汐止區地勢南北高，中央低，河岸階地海拔平均僅9公尺，市內除[基隆河兩岸為較大之河谷平原之外](../Page/基隆河.md "wikilink")，其餘多為坡度百分之卅以上之山坡地。[基隆河由汐止區之東蜿蜒西流](../Page/基隆河.md "wikilink")，南北高山夾峙，基隆河貫穿其中。

汐止區四面環山，接鄰[內湖](../Page/內湖區.md "wikilink")、[基隆](../Page/基隆市.md "wikilink")[七堵](../Page/七堵區.md "wikilink")、[南港](../Page/南港區.md "wikilink")、[石碇](../Page/石碇區.md "wikilink")、[平溪](../Page/平溪區.md "wikilink")、[萬里](../Page/萬里區.md "wikilink")、[士林等周圍山區](../Page/士林區.md "wikilink")。其境內山脈皆未到達一千公尺，大部份山高多位於三百至六百公尺之間，東西走向為主；是為小型山系。

西北方為[五指山山脈](../Page/五指山山脈.md "wikilink")，最高峰為[五指山](../Page/五指山（新北市）.md "wikilink")（車坪寮山），海拔高度為六九九公尺，形成[瑪鍊溪與](../Page/瑪鍊溪.md "wikilink")[基隆河的](../Page/基隆河.md "wikilink")[分水嶺](../Page/分水嶺.md "wikilink")。東南方為[南港山脈](../Page/南港山.md "wikilink")，而南面山地屬[大尖山系](../Page/大尖山.md "wikilink")，最高峰為[姜子寮山](../Page/姜子寮山.md "wikilink")，海拔高度為七二九公尺。

### 水文

[基隆河水量豐沛](../Page/基隆河.md "wikilink")，平均全年逕流量為1723.8百萬平方公尺，因此切割山脈厲害。而位於中游的汐止，亦由主幹流基隆河及保長坑溪、茄苳溪、康誥坑溪、大坑溪、內溝溪、叭嗹溪、北港溪、橫科溪等八大支流注入沖積、切割而形成蜿蜒河谷平原及瀑布。基隆河在汐止境內之大小支流共14條。

基隆河右岸支流由西向東排序為[內溝溪](../Page/內溝溪.md "wikilink")、[草濫溪](../Page/草濫溪_\(新北市\).md "wikilink")、[北峰溪](../Page/北峰溪.md "wikilink")、[叭嗹溪](../Page/叭嗹溪.md "wikilink")、[北港溪](../Page/北港溪_\(新北市\).md "wikilink")、[無名溪](../Page/無名溪_\(新北市\).md "wikilink")、[鄉長溪](../Page/鄉長溪.md "wikilink")。基隆河左岸支流由西向東排序為[大坑溪](../Page/大坑溪_\(新北市\).md "wikilink")、[橫科溪](../Page/橫科溪.md "wikilink")、[下寮溪](../Page/下寮溪.md "wikilink")、[康誥坑溪](../Page/康誥坑溪.md "wikilink")、[智慧溪](../Page/智慧溪.md "wikilink")、[禮門溪](../Page/禮門溪.md "wikilink")、[茄苳溪](../Page/茄苳溪_\(新北市\).md "wikilink")、[保長坑溪](../Page/保長坑溪.md "wikilink")。

### 氣候

汐止區位居臺北盆地外緣之東南隅，為臺北盆地與東部盆地交接之處。因此雖屬臺灣北部氣候區，但卻易受基隆地區之東北氣候區的影響。汐止區之有效溫度大部分屬於[副熱帶季風氣候區](../Page/副熱帶季風氣候.md "wikilink")。氣候之特徵為東北區與西南區之過渡性質，夏季雨量稍微多於冬季，本區冬季因當大陸及地氣團南下之衝，故氣溫較低。

臺灣的[颱風主要是發生於七](../Page/颱風.md "wikilink")、八、九月，其中尤以八月份最多，每因狂風暴雨而成巨災，直至科技發達的今日亦不例外。汐止區以前由於缺乏有系統之排水路，只能依賴天然溝渠以及道路側溝排水，由於溝面狹小，經常淤積；而汐止屬於相對低窪地區，豪雨一來容易釀成水災。

汐止二十年來的嚴重水災：1987年[琳恩風災](../Page/颱風琳恩.md "wikilink")（台北市、汐止嚴重水災）、1996年[賀伯風災](../Page/颱風賀伯.md "wikilink")（新北市淹水）、1997年[溫妮風災](../Page/颱風溫妮.md "wikilink")（[林肯大郡倒塌](../Page/林肯大郡.md "wikilink")）、1998年[瑞伯風災](../Page/颱風瑞伯.md "wikilink")（汐止大淹水）、[2000年象神風災](../Page/颱風象神_\(2000年\).md "wikilink")（汐止、基隆大淹水）、2001年[納莉風災](../Page/颱風納莉.md "wikilink")

納莉之後的颱風，汐止各地幾乎沒有大淹水，最嚴重就是路面積水，也不超過50公分。主因是[瑞芳的](../Page/瑞芳區.md "wikilink")[員山子分洪道完工](../Page/員山子分洪道.md "wikilink")，只要[基隆河水位達到六十二點五公尺](../Page/基隆河.md "wikilink")，水流即會流過側流堰進入沉砂池；水位若持續上升至六十三公尺溢過攔河堰，就會進入分洪隧道流入外海，不必再藉由人工開挖的方式分洪，更因此改變了汐止每逢颱風必淹的窘況。

## 人口

為[新北市人口的第九名](../Page/新北市.md "wikilink")，汐止區雖然隸屬於[新北市](../Page/新北市.md "wikilink")，但位於[台北市與](../Page/臺北市.md "wikilink")[基隆市之間](../Page/基隆市.md "wikilink")，離[新北市市中心較遠](../Page/新北市.md "wikilink")，和[新北市政府所在地](../Page/新北市政府.md "wikilink")[板橋區中間隔一個](../Page/板橋區_\(台灣\).md "wikilink")[台北市](../Page/臺北市.md "wikilink")，導致汐止生活型態和[台北市和](../Page/臺北市.md "wikilink")[基隆市反而比較接近](../Page/基隆市.md "wikilink")。統計人口大概19萬人，因鄰近[台北市](../Page/臺北市.md "wikilink")[內湖區](../Page/內湖區.md "wikilink")，部分的汐止居民選擇就讀內湖的學區（例：東湖國小、東湖國中、明湖國小、明湖國中、南湖國小、麗湖國小、大湖國小、三民國中等），但轉移學區須轉移戶籍，所以汐止區真正的人口早已突破20萬人。

汐止多山、多河，平原與丘陵地並不多，因此汐止的人口分布，除了沿基隆河岸區域之外，有不少人居住在山坡地，且大多為人為開發填築，造成擁有許多大型山莊別墅的特殊現象。

例如：伯爵山莊、白馬山莊、[摩天鎮](../Page/摩天鎮.md "wikilink")、水蓮山莊、[林肯大郡](../Page/林肯大郡.md "wikilink")、瓏山林社區、水晶山莊、台北小別墅、東湖山莊.....等。

## 行政分區與次分區

### 行政區

  - 八連里、白雲里、忠山-{里}-、長安里、厚德里、[崇德里](../Page/崇德里_\(新北市汐止區\).md "wikilink")、湖興里、義民里、禮門里、建成里
  - [大同里](../Page/大同里_\(新北市汐止區\).md "wikilink")、[江北里](../Page/江北里\(新北市汐止區\).md "wikilink")、[忠孝里](../Page/忠孝里_\(新北市汐止區\).md "wikilink")、長青里、[茄苳里](../Page/茄苳里_\(新北市\).md "wikilink")、康福里、[福山-{里}-](../Page/福山里_\(新北市汐止區\).md "wikilink")、樟樹里、北山里
  - 山光里、北峰里、金龍里、保安里、拱北里、[復興里](../Page/復興里_\(新北市汐止區\).md "wikilink")、福安里、橋東里
  - [中興里](../Page/中興里_\(新北市汐止區\).md "wikilink")、[自強里](../Page/自強里_\(新北市汐止區\).md "wikilink")、東山-{里}-、保長里、烘內里、智慧里、福德里、興福里
  - [仁德里](../Page/仁德里_\(新北市汐止區\).md "wikilink")、秀山-{里}-、東勢里、[保新里](../Page/保長坑.md "wikilink")、信望里、湖光里、鄉長里、橫科里
  - [文化里](../Page/文化里_\(新北市汐止區\).md "wikilink")、秀峰里、宜興里、城中里、湖蓮里、[新昌里](../Page/新昌里_\(新北市\).md "wikilink")、環河-{里}-

### 地名與次分區

**東汐止**

  - [保長坑](../Page/保長坑.md "wikilink")、[石門坑](../Page/石硿子.md "wikilink")
  - [鄉長厝](../Page/鄉長厝.md "wikilink")、[茄苳腳](../Page/茄苳腳_\(新北市\).md "wikilink")
  - 東山、[鵠鵠崙](../Page/鵠鵠崙.md "wikilink")、[姜子寮](../Page/姜子寮.md "wikilink")

**市中心**

  - 汐止、橋東、建成路(國泰醫院)
  - [汐止國小](../Page/新北市汐止區汐止國民小學.md "wikilink")、汐科(下寮)、[康誥坑(水源路)](../Page/康誥坑.md "wikilink")
  - 區公所(秀峰)、仁愛路、青山

**北汐止**

  - 江北、[北港](../Page/北港_\(新北市\).md "wikilink")、伯爵山莊
  - 烘內、[叭嗹港](../Page/叭嗹港.md "wikilink")

**西汐止**

  - 昊天嶺、[東科](../Page/東方科學園區.md "wikilink")、[遠雄廣場](../Page/IFG遠雄廣場.md "wikilink")
  - [樟樹灣](../Page/樟樹灣.md "wikilink")、[社-{后}-](../Page/社後_\(汐止區\).md "wikilink")
  - [橫科](../Page/橫科.md "wikilink")、白雲、[白匏湖](../Page/白匏湖.md "wikilink")

## 媒體

  - [聯合報總部](../Page/聯合報.md "wikilink")
  - [民視攝影棚](../Page/民視.md "wikilink")
  - [天良電視台總部](../Page/天良電視台.md "wikilink")

## 交通

[缩略图](https://zh.wikipedia.org/wiki/File:TRA_Sike_Station_south_entrance_20080803.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:TRA_Sijhih_Station_East_Entrance_20080803.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:TRA_Wudu_Station_entrance_on_Changan_Road_20080803.jpg "fig:缩略图")

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [縱貫線](../Page/縱貫線_\(北段\).md "wikilink")：[五堵車站](../Page/五堵車站.md "wikilink")
    - [汐止車站](../Page/汐止車站.md "wikilink") -
    [汐科車站](../Page/汐科車站.md "wikilink")

### 國道及公路

#### 國道

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號
    （中山高速公路）](../Page/中山高速公路.md "wikilink")
      - [汐止交流道](../Page/汐止交流道.md "wikilink")

      - [汐止系統交流道](../Page/汐止系統交流道.md "wikilink")
        （連接國道三號[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")）
  - [TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號
    （福爾摩沙高速公路）](../Page/福爾摩沙高速公路.md "wikilink")
      - [汐止系統交流道](../Page/汐止系統交流道.md "wikilink")
        （連接國道一號[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")）

      - [新台五路交流道](../Page/新台五路交流道.md "wikilink")

#### 省道

  - [TW_PHW5.svg](https://zh.wikipedia.org/wiki/File:TW_PHW5.svg "fig:TW_PHW5.svg")[台5線](../Page/台5線.md "wikilink")：[北基公路](../Page/北基公路.md "wikilink")（[台北市](../Page/臺北市.md "wikilink")-[基隆市](../Page/基隆市.md "wikilink")）
      - [TW_PHW5a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW5a.svg "fig:TW_PHW5a.svg")[台5甲](../Page/台5線.md "wikilink")：大同路
      - [TW_PHW5b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW5b.svg "fig:TW_PHW5b.svg")[台5乙](../Page/台5線.md "wikilink")：汐止貨櫃車專用道（已於2013年6月17日全線解編）\[5\]。

#### 鄉道

  - [TW_THWtp28.png](https://zh.wikipedia.org/wiki/File:TW_THWtp28.png "fig:TW_THWtp28.png")[北28線](../Page/北28線.md "wikilink")：[長壽路](../Page/長壽路.md "wikilink")
    （連接萬崁公路）
  - [TW_THWtp29.png](https://zh.wikipedia.org/wiki/File:TW_THWtp29.png "fig:TW_THWtp29.png")[北29線](../Page/北29線.md "wikilink")：汐萬公路（拱北-汐止間）
  - [TW_THWtp30.png](https://zh.wikipedia.org/wiki/File:TW_THWtp30.png "fig:TW_THWtp30.png")[北30線](../Page/北30線.md "wikilink")：茄苳路、茄安路
  - [TW_THWtp31.png](https://zh.wikipedia.org/wiki/File:TW_THWtp31.png "fig:TW_THWtp31.png")[北31線](../Page/北31線.md "wikilink")：[汐平公路](../Page/汐平公路.md "wikilink")

#### 主要道路

  - [大同路](../Page/大同路.md "wikilink")：貫穿汐止區全境，新台五路以西為[台五線](../Page/台5線.md "wikilink")，以東為台五甲線，是[南港區通往](../Page/南港區.md "wikilink")[基隆市必經道路](../Page/基隆市.md "wikilink")；[聯合報系總部所在地](../Page/聯合報系.md "wikilink")。是[保長坑](../Page/保長坑.md "wikilink")、汐科、橫科、五堵和汐止區中心的要道。
  - 中興路：社-{后}-地區主要道路。
  - 汐平路：汐止通往平溪區主要道路。
  - 忠孝東路：汐止後站主要道路。
  - 康寧街：經過北港、社后等地，是連接東湖汐止重要道路，過去曾經為[麥帥公路一部分](../Page/麥帥公路.md "wikilink")。
  - 鄉長路：鄉長厝、五堵地區的重要道路。
  - [中正路](../Page/中正路.md "wikilink")：汐止老街。
  - 民權街：橫科地區主要道路，離南港區近，有交流道可上環東大道。
  - [汐萬路](../Page/汐萬路.md "wikilink")：汐止通往[五指山](../Page/車坪寮山.md "wikilink")、[萬里地區主要道路](../Page/萬里區.md "wikilink")。
  - 建成路：橋東里主要道路。
  - 新台五路：台五線，經過[東方科學園區和](../Page/東方科學園區.md "wikilink")[遠東世界中心等廠辦大樓和](../Page/遠東世界中心.md "wikilink")[福爾摩沙高速公路新台五路交流道](../Page/福爾摩沙高速公路.md "wikilink")。
  - 水源路、秀峰路、勤進路、汐碇路：通往汐止大尖山、[石碇重要道路](../Page/石碇.md "wikilink")。
  - 保長路：連結台五線與五堵地區。

#### 捷運

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - <font color={{台北捷運色彩|民}}>█</font> </small>[民生汐止線](../Page/民生汐止線.md "wikilink")（規劃中）：[<font color="#888888">下社后站</font>](../Page/下社后站.md "wikilink")
    - [<font color="#888888">社后站</font>](../Page/社后站.md "wikilink") -
    [<font color="#888888">樟樹灣站</font>](../Page/樟樹灣站.md "wikilink") -
    [<font color="#888888">汐科站</font>](../Page/汐科站.md "wikilink") -
    [<font color="#888888">汐止區公所站</font>](../Page/汐止區公所站.md "wikilink")
  - <font color={{台北捷運色彩|民支}}>█</font> </small>[東湖支線](../Page/東湖支線.md "wikilink")（規劃中）：[<font color="#888888">社后下站</font>](../Page/社后下站.md "wikilink")
    - [<font color="#888888">五分站</font>](../Page/五分站.md "wikilink") -
    [<font color="#888888">火炭坑站</font>](../Page/火炭坑站.md "wikilink") -
    [<font color="#888888">內溝站</font>](../Page/內溝站.md "wikilink")

[基隆輕軌](../Page/基隆輕軌.md "wikilink")（規劃中）

  - [北五堵站](../Page/北五堵站.md "wikilink") North Wudu Station
  - [五堵站](../Page/五堵站.md "wikilink") Wudu Station
    臺灣鐵路縱貫線：[五堵](../Page/五堵.md "wikilink")
  - [汐止站](../Page/汐止站.md "wikilink") Xizhi Station
    臺灣鐵路縱貫線：[汐止](../Page/汐止.md "wikilink")
  - [汐科站](../Page/汐科站.md "wikilink") Xike Station
    臺灣鐵路縱貫線：[汐科](../Page/汐科.md "wikilink")
  - [樟樹灣站](../Page/樟樹灣站.md "wikilink") Zhangshuwan Station
    [臺北捷運民生汐止線](../Page/臺北捷運民生汐止線.md "wikilink")：[樟樹灣](../Page/樟樹灣.md "wikilink")

## 經濟

### 企業總部

  - [宏碁股份有限公司](../Page/宏碁股份有限公司.md "wikilink")（Acer
    Incorporated）：知名全球五大品牌之一電腦企業，總部位於新台五路一段。
  - [精華光學股份有限公司](../Page/精華光學股份有限公司.md "wikilink")（ST. Shine Optica Co.,
    Ltd.）：知名全球五大品牌之一隱形眼鏡製造廠，總部位於大同路。
  - [研華股份有限公司](../Page/研華股份有限公司.md "wikilink")（Advantech Co.,
    Ltd.）：簡稱研華科技或是研華，[上市公司](../Page/上市公司.md "wikilink")，是一家台灣的工業自動化公司，研華台北系統整合廠位於康寧街。
  - [台灣國際航電股份有限公司](../Page/台灣國際航電股份有限公司.md "wikilink")
    (Garmin)，位於樟樹二路68號。
  - [聯合報](../Page/聯合報.md "wikilink")（United Daily
    News）總部，台灣重要媒體之一，位於大同路。

### 科學園區

[Oriental_Science_Park_20160305.jpg](https://zh.wikipedia.org/wiki/File:Oriental_Science_Park_20160305.jpg "fig:Oriental_Science_Park_20160305.jpg")\]\]

  - [東方科學園區](../Page/東方科學園區.md "wikilink")
  - [台灣科學園區](../Page/台灣科學園區_\(汐止\).md "wikilink")
  - [雍和科學園區](../Page/雍和科學園區.md "wikilink")
  - [遠東科技中心](../Page/遠東科技中心.md "wikilink")
  - [康寧科技園區](../Page/康寧科技園區.md "wikilink")

### 百貨公司與購物中心

[Farglory_U-Town_20160305.jpg](https://zh.wikipedia.org/wiki/File:Farglory_U-Town_20160305.jpg "fig:Farglory_U-Town_20160305.jpg")\]\]
[Farglory_U-Town_20160624.jpg](https://zh.wikipedia.org/wiki/File:Farglory_U-Town_20160624.jpg "fig:Farglory_U-Town_20160624.jpg")大門\]\]

  - [IFG遠雄廣場](../Page/IFG遠雄廣場.md "wikilink")

### 專業賣場與量販店

  - [好市多汐止店](../Page/好市多.md "wikilink")
  - [家樂福汐科店](../Page/家樂福.md "wikilink")

## 警政治安

  - [新北市政府警察局汐止分局](http://www.xizhi.police.ntpc.gov.tw/)
    [110](../Page/110.md "wikilink") / （02）2641-2610、（02）2641-2642
  - 長青派出所（原八連派出所） （02）2645-1281
  - 汐止派出所 （02）2641-2827、（02）2649-0711
  - 東山派出所 （02）2641-5841
  - 社后派出所 （02）2694-2819、（02）2694-6170
  - 長安派出所 （02）8648-2618
  - 烘內派出所 （02）2646-2564
  - 橫科派出所 （02）2640-1237、（02）2640-7043
  - 汐止分局交通分隊 （02）8642-4742

## 教育

### 高級中學

  - [新北市立秀峰高級中學](../Page/新北市立秀峰高級中學.md "wikilink")
  - [新北市立樟樹國際實創高級中等學校](../Page/新北市立樟樹國際實創高級中等學校.md "wikilink")
  - [新北市私立崇義高級中學](../Page/新北市私立崇義高級中學.md "wikilink")

### 國民中學

  - [新北市立秀峰高級中學國中部](../Page/新北市立秀峰高級中學.md "wikilink")
  - [新北市立樟樹國際實創高級中等學校國中部](../Page/新北市立樟樹國際實創高級中等學校.md "wikilink")
  - [新北市立汐止國民中學](../Page/新北市立汐止國民中學.md "wikilink")
  - [新北市立青山國民中小學](http://www.csjhs.ntpc.edu.tw)
  - [新北市私立崇義高級中學國中部](../Page/新北市私立崇義高級中學.md "wikilink")

### 國民小學

  - [新北市汐止區汐止國民小學](http://www.hjes.ntpc.edu.tw)
  - [新北市汐止區北港國民小學](http://www.bkes.ntpc.edu.tw)
  - [新北市汐止區長安國民小學](http://www.chanes.ntpc.edu.tw)
  - [新北市汐止區金龍國民小學](http://www.gdps.ntpc.edu.tw)
  - [新北市汐止區保長國民小學](http://www.bjes.ntpc.edu.tw)
  - [新北市汐止區崇德國民小學](http://www.cdps.ntpc.edu.tw)
  - [新北市汐止區白雲國民小學](http://www.pyps.ntpc.edu.tw)
  - [新北市汐止區北峰國民小學](http://www.pfes.ntpc.edu.tw)
  - [新北市汐止區東山國民小學](http://www.dsps.ntpc.edu.tw)
  - [新北市汐止區秀峰國民小學](http://www.sfps.ntpc.edu.tw)
  - [新北市汐止區樟樹國民小學](http://www.tctes.ntpc.edu.tw)
  - 新北市立青山國民中小學國小部

### 圖書館

  - [新北市立圖書館汐止大同分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止北峰圖書閱覽室](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止江北分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止長安分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止汐止茄苳分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止新昌分館](../Page/新北市立圖書館.md "wikilink")
  - [新北市立圖書館汐止橫科圖書閱覽室](../Page/新北市立圖書館.md "wikilink")

## 體育場館

  - [新北市汐止國民運動中心](../Page/汐止國民運動中心.md "wikilink")
  - [汐止綜合運動場](../Page/汐止綜合運動場.md "wikilink")
  - 北峰國小社-{后}-多目標體育館

## 醫療

[缩略图](https://zh.wikipedia.org/wiki/File:Sijhih_Cathay_General_Hospital_20140527.jpg "fig:缩略图")

  - [國泰綜合醫院](../Page/國泰綜合醫院.md "wikilink")
    汐止分院：於2005年12月7日成立。設置有心臟內科、腸胃科、腎臟內科、呼吸胸腔科、內分泌科、神經內科、感染科、一般外科、神經外科、整形外科、骨科、婦產科、小兒科、耳鼻喉科、泌尿科、眼科、皮膚科、家庭醫學科、精神科、急診醫學科、麻醉科、牙科、復健科，並發展急重症醫療照護及加強臨床醫學研究。一般病床440床，精神科病床40床，另設有護理之家、安寧病房、健康檢查中心、洗腎中心及美容中心等，合計794床。

## 旅遊景點

[Ni_Chiang-huai_1936c_Xizhi_Taipei.jpg](https://zh.wikipedia.org/wiki/File:Ni_Chiang-huai_1936c_Xizhi_Taipei.jpg "fig:Ni_Chiang-huai_1936c_Xizhi_Taipei.jpg")/
1929年/ 水彩/ 台北市立美術館典藏\]\]

  - [汐止添福宮茄苳樹](../Page/汐止添福宮茄苳樹.md "wikilink")
  - [大尖山登山步道系統](../Page/大尖山.md "wikilink")
  - [石門峽](https://www.google.com.tw/maps/place/%E7%9F%B3%E9%96%80%E5%B3%BD%E8%B0%B7/@25.067348,121.688576,17z/data=!3m1!4b1!4m2!3m1!1s0x345d53bba218a013:0xdd8cd16d70576717?hl=zh-TW)
  - [杜月笙墓園](../Page/杜月笙.md "wikilink")
  - 茄苳瀑布
  - [拱北殿](../Page/拱北殿.md "wikilink")
  - [慈航紀念堂](../Page/慈航紀念堂.md "wikilink")
  - [濟德宮](../Page/濟德宮.md "wikilink")
  - [忠順廟](../Page/忠順廟.md "wikilink")
  - [終南洞三聖宮](../Page/終南洞三聖宮.md "wikilink")（仙公廟）
  - 錦峰山觀音寺
  - 大尖山天秀宮
  - 北峰寺
  - [五指山](../Page/五指山（新北市）.md "wikilink")（車坪寮山）
  - 汐止老街（中正路）：清乾隆年間稱為「水返腳街」，即今日之中正路，是汐止最早開發的商業街，素有「汐止第一街」的美譽。
  - [金龍湖](../Page/金龍湖_\(汐止區\).md "wikilink")
  - 柯子林峽谷
  - 秀峰瀑布
  - [夢湖](../Page/夢湖.md "wikilink")
  - [翠湖](../Page/翠湖_\(台北\).md "wikilink")

### 汐止古道

  - 天秀宮古道
  - 白雲古道
  - 茄苳古道
  - 菁桐古道
  - 磐石古道
  - 水源古道
  - 柞埤古道
  - 姜子寮古道
  - 薯榔尖古道
  - 小南港山 （又稱「橫科山」、「橫科口山」） 步道

### 汐止古蹟

  - 基隆河巴洛克老宅
  - 站前蘇家老厝

## 出身人物

### 政治界

  -   - [黃國昌](../Page/黃國昌.md "wikilink")：[時代力量第二屆曾任中央黨部主席](../Page/時代力量.md "wikilink")，第九屆（[現任](../Page/現任.md "wikilink")）立法委員。
      - [黃智姍](../Page/黃智姍.md "wikilink")：時代力量黃國昌立委汐止服務處秘書，曾任蓮新獅子會，喜悅獅子會，新北市里長服務協會的顧問，汐止警友辦事處的顧問，汐止義消中隊的顧問，汐止義警中隊的顧問。
      - [沈發惠](../Page/沈發惠.md "wikilink")：第六屆立法委員、第一、二屆[新北市議員](../Page/新北市.md "wikilink")。
      - [白珮茹](../Page/白珮茹.md "wikilink")：新北市議員。
      - [白添枝](../Page/白添枝.md "wikilink")：第六屆立法委員 現任第一屆中華民國農會常務監事 行政院政務顧問
        立法院最高顧問。
      - [周雅淑](../Page/周雅淑.md "wikilink")：前汐止鎮鎮長。
      - [廖學廣](../Page/廖學廣.md "wikilink")：第十、十一屆前台北縣縣議員、第十一、十二屆前汐止鎮鎮長、第三、四屆前立法委員。
      - [蕭敬嚴](../Page/蕭敬嚴.md "wikilink")：[中華民國史上最年輕](../Page/中華民國.md "wikilink")[立法委員候選人](../Page/立法委員.md "wikilink")、第十屆[中國國民黨青年團總團長](../Page/中國國民黨青年團.md "wikilink")、[中國國民黨中央常務委員](../Page/中國國民黨.md "wikilink")、[新北市立秀峰高級中學第十二屆學生自治會會長](../Page/新北市立秀峰高級中學.md "wikilink")。
      - [孫繼正](../Page/孫繼正.md "wikilink")：[安定力量聯盟主席](../Page/安定力量.md "wikilink")，崇光金品公司代表人。

### 體育界

  -   - [吳念庭](../Page/吳念庭.md "wikilink"): 棒球選手。
      - [蔡仲南](../Page/蔡仲南.md "wikilink")：棒球選手。

### 演藝界

  -   - [鄭如吟](../Page/鄭如吟.md "wikilink")：藝名玉兔（Yu
        Tu），台灣女藝人，曾是[我愛黑澀會節目成員](../Page/我愛黑澀會.md "wikilink")。
      - [蔡玓彤](../Page/蔡玓彤.md "wikilink")：藝名貝童彤，舊藝名彤彤，曾是[我愛黑澀會節目成員](../Page/我愛黑澀會.md "wikilink")，曾經於MOMO親子台擔任兒童節目主持人，有「彤彤姐姐」的稱號，現居於汐止。
      - [顏正國](../Page/顏正國.md "wikilink")：台灣電影男演員、導演、書法老師。昔日童星，以演出《[好小子](../Page/好小子.md "wikilink")》系列電影聞名。

### 科學界

  -   - [闕志鴻](../Page/闕志鴻.md "wikilink")：[臺灣](../Page/臺灣.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")，現任教於[國立臺灣大學物理學系暨天文物理研究所](../Page/國立臺灣大學物理學系.md "wikilink")。

### 教育界

  -   - [陳紹馨](../Page/陳紹馨.md "wikilink")：。日本[東北帝國大學法文學部畢業](../Page/東北帝國大學.md "wikilink")，日本[關西大學社會學博士](../Page/关西大学.md "wikilink")，為臺灣第一位[社會學博士](../Page/社會學.md "wikilink")，致力於臺灣[社會學](../Page/社會學.md "wikilink")、[人類學及](../Page/人類學史.md "wikilink")[人口學研究](../Page/人口学.md "wikilink")，曾赴美國[普林斯頓大學從事人口研究](../Page/普林斯頓大學.md "wikilink")，為國際知名[人類學家及](../Page/人類學史.md "wikilink")[社會學家](../Page/社會學.md "wikilink")。
      - [楊朝祥](../Page/楊朝祥.md "wikilink")：曾任教育部長，現任[佛光大學校長](../Page/佛光大學.md "wikilink")。

### 其他

  -   - [林正緯](../Page/林正緯.md "wikilink")：知名機車達人，就讀[國立臺灣海洋大學](../Page/國立臺灣海洋大學.md "wikilink")。
      - [林庭旭](../Page/林庭旭.md "wikilink")：購物專家。
      - [陳植棋](../Page/陳植棋.md "wikilink")：畫家。

## 姊妹友好城市

[邁阿密](../Page/邁阿密.md "wikilink")[都瑞爾市 （ City of Doral
）](../Page/多拉_\(佛羅里達州\).md "wikilink")

## 参考文献

## 外部連結

  - [汐止區公所](http://www.xizhi.ntpc.gov.tw/)
  - [新北市汐止戶政事務所](http://www.xizhi.ris.ca.ntpc.gov.tw/)

{{-}}

[X](../Category/新北市行政區劃.md "wikilink")
[汐止區](../Category/汐止區.md "wikilink")
[X](../Category/源自日本統治的台灣地名.md "wikilink")

1.

2.
3.
4.  楊貴三、沈淑敏（民99）。台灣全志，卷二，土地志，地形篇。南投市：台灣文獻館。

5.  [行政院 102.06.17.
    院臺交字第1020036318號公告](http://motclaw.motc.gov.tw/s.aspx?soid=4747)