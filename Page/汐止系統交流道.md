**汐止系統交流道**坐落於[台灣](../Page/台灣.md "wikilink")[基隆河上](../Page/基隆河.md "wikilink")、[新北市](../Page/新北市.md "wikilink")[汐止區內](../Page/汐止區.md "wikilink")，[國道一號里程](../Page/中山高速公路.md "wikilink")11.5公里、[國道三號里程](../Page/福爾摩沙高速公路.md "wikilink")10.9公里處，於1996年3月通車。為兩大主線三個交會點中最北者。設計時考量與國道一號前一交流道僅相距850公尺，將交流道型式設計為雙葉型，在國道一號兩旁增設集散道引導車流，國道三號左轉入國道一號則均以環道設計連接。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
<th><p>南下</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a><a href="https://zh.wikipedia.org/wiki/File:TWHW5.svg" title="fig:TWHW5.svg">TWHW5.svg</a><strong>高速公路</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北上</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a><strong>萬里</strong>　<a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a><strong>木柵</strong>　<a href="https://zh.wikipedia.org/wiki/File:TWHW5.svg" title="fig:TWHW5.svg">TWHW5.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a></p></td>
<td></td>
<td><p>南下</p></td>
<td><p><strong>汐止　內湖</strong></p></td>
</tr>
<tr class="odd">
<td><p>北上</p></td>
<td><p><strong>內湖　五堵</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近交流道

|-

## 參考資料

## 外部連結

  - [國道一號汐止交流道、汐止系統交流道動線圖](https://www.freeway.gov.tw/UserFiles/File/Traffic/7-1-2/%E4%BA%A4%E6%B5%81%E9%81%93%E7%A4%BA%E6%84%8F%E5%9C%96_102.10/01-010%E6%B1%90%E6%AD%A2.jpg)
  - [國道三號汐止系統交流道動線圖](https://www.freeway.gov.tw/UserFiles/File/Traffic/7-1-2/%E4%BA%A4%E6%B5%81%E9%81%93%E7%A4%BA%E6%84%8F%E5%9C%96_102.10/03-010%E6%B1%90%E6%AD%A2%E7%B3%BB%E7%B5%B1.jpg)

[Category:新北市交流道](../Category/新北市交流道.md "wikilink")
[Category:汐止區](../Category/汐止區.md "wikilink")