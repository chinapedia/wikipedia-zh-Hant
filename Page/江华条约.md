《**江华条约**》又称《**江华岛条约**》、《**日朝修好條規**》、《**日鮮修好條規**》、《**丙子修好条约**》，是[大日本帝国与](../Page/大日本帝国.md "wikilink")[朝鲜王朝于](../Page/李氏朝鲜.md "wikilink")1876年2月26日在[江华岛签订的](../Page/江华岛.md "wikilink")[不平等条约](../Page/不平等条约.md "wikilink")，从此朝鲜逐步沦落为大日本帝国的半殖民地。

## 背景

1875年9月，日本派出军舰[云扬号航行到朝鲜西海岸测量海图](../Page/云扬号.md "wikilink")，遭到朝鮮[江华岛砲台守軍砲擊](../Page/江华岛.md "wikilink")，雙方發生交火，最後日本方攻陷炮台，史称“[江华岛事件](../Page/江华岛事件.md "wikilink")”（日本稱「」，韓國稱「」）。

## 内容

《江华条约》共计12款，是朝鲜和外国签订的第一个不平等条约。日本取得了自由勘测朝鲜海口、[领事裁判权](../Page/领事裁判权.md "wikilink")、贸易等权利。自此，朝鲜对外开放，外国商品進入朝鲜。条约的主要内容包括承认朝鲜为独立国及日本享有的领事裁判权等。

## 影响

《江华条约》是日本「[大陆政策](../Page/大陆政策.md "wikilink")」实践的开始。此政策以征服中国为中心，但征服中国的第一步是征服朝鲜，簽訂江華條約为以后日本吞并朝鲜做了铺垫。

## 参见

  - [維基文庫中的條約內文](../Page/:s:日朝修好條規.md "wikilink")
  - [济物浦条约](../Page/济物浦条约.md "wikilink")
  - [日韩保护协约](../Page/日韩保护协约.md "wikilink")
  - [日韩合併条约](../Page/日韩合併条约.md "wikilink")
  - [马关条约](../Page/马关条约.md "wikilink")

[Category:不平等条约](../Category/不平等条约.md "wikilink") [Category:日朝關係
(朝鮮王朝)](../Category/日朝關係_\(朝鮮王朝\).md "wikilink")
[Category:日本條約](../Category/日本條約.md "wikilink")
[Category:朝鲜王朝条约](../Category/朝鲜王朝条约.md "wikilink")
[Category:1870年代亚洲政治](../Category/1870年代亚洲政治.md "wikilink")
[Category:1876年條約](../Category/1876年條約.md "wikilink")
[Category:1876年日本](../Category/1876年日本.md "wikilink")
[Category:1876年朝鮮半島](../Category/1876年朝鮮半島.md "wikilink")
[Category:1876年2月](../Category/1876年2月.md "wikilink")