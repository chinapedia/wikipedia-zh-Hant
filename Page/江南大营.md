**江南大营**是[清朝为镇压](../Page/清朝.md "wikilink")[太平天国集结起的最后两支直接听命于](../Page/太平天国.md "wikilink")[咸丰帝的野战军之一](../Page/咸丰帝.md "wikilink")（另一支为[科尔沁部](../Page/科尔沁部.md "wikilink")[僧格林沁野战](../Page/僧格林沁.md "wikilink")[骑兵](../Page/骑兵.md "wikilink")）。全国各地质素较好的[綠營官兵集结](../Page/綠營.md "wikilink")[太平天國](../Page/太平天國.md "wikilink")[天京](../Page/天京.md "wikilink")[孝陵卫驻扎屏蔽苏杭财赋区成为江南大营](../Page/孝陵卫.md "wikilink")，分为两期，皆被太平天國围歼。另有兩次駐軍稱[江北大營](../Page/江北大營.md "wikilink")。江南大营的被歼宣告在[华东地区](../Page/华东地区.md "wikilink")、[中南地区再无直接听命于清朝皇室的野战军](../Page/中南地区.md "wikilink")，清朝皇室不得不起用汉族[湘军与](../Page/湘军.md "wikilink")[淮军而尾大不掉](../Page/淮军.md "wikilink")。

咸豐三年(1853)，欽差大臣[向榮領兵一萬人](../Page/向榮.md "wikilink")，在南京城東孝陵衛駐紮，號稱江南大營。[琦善則領兵一萬人在揚州城外駐紮](../Page/琦善.md "wikilink")，號稱江北大營。1856年江南大營第一次被太平军击破，1858年初[和春再度重建江南大营](../Page/和春.md "wikilink")。

## 第一次江南大营

**駐軍時間**：1853年（咸豐三年）正月廿九日，[太平軍開始攻南京](../Page/太平軍.md "wikilink")，二月九日，攻佔南京，改名[天京](../Page/天京.md "wikilink")，建國號[太平天國](../Page/太平天國.md "wikilink")；十餘天後[向榮率](../Page/向榮.md "wikilink")50000名清兵自[武昌追至](../Page/武昌.md "wikilink")，驻军於[孝陵卫](../Page/孝陵卫.md "wikilink")，名之。

**統帥：**欽差大臣[向榮](../Page/向榮.md "wikilink")，官兵十萬以內

**潰滅時間：** 1856年6月

**太平軍勝戰統帥：**「東王」[楊秀清](../Page/楊秀清.md "wikilink")、[秦日綱](../Page/秦日綱.md "wikilink")（於月餘後[天京事变中](../Page/天京事变.md "wikilink")，秦殺[楊秀清](../Page/楊秀清.md "wikilink")。）

## 第二次江南大营

**駐軍時間：**1858年（咸豐八年）二月

**統帥：**欽差大臣[和春](../Page/和春.md "wikilink")、提督[張國樑](../Page/張國樑.md "wikilink")（出身[綠營](../Page/綠營.md "wikilink")、原為廣西[大湟江水賊](../Page/大湟江.md "wikilink")、與未加入太平軍時的[羅大綱同伙](../Page/羅大綱.md "wikilink")，[江湖人稱外號](../Page/江湖.md "wikilink")「大頭羊」，後與羅分裂）官兵至少二十萬以上，據[美國來華](../Page/美國.md "wikilink")[基督教傳教](../Page/基督教.md "wikilink")[牧師描述](../Page/牧師.md "wikilink")：「……（江南大营）不像軍隊，形同市集，吃喝玩樂，大煙娼賭俱全」。

**潰滅時間：**1860年（咸豐十年）五月

**太平軍勝戰統帥：**「忠王」[李秀成](../Page/李秀成.md "wikilink")

## 参考文献

### 引用

### 来源

  - 《[清史稿](../Page/清史稿.md "wikilink")》
  - [鄧忠](../Page/鄧忠.md "wikilink") 著：《美國人與太平天國》

## 参见

  - [江北大营](../Page/江北大营.md "wikilink")

[Category:清朝政府军队](../Category/清朝政府军队.md "wikilink")
[Category:太平天国军事](../Category/太平天国军事.md "wikilink")
[Category:南京军事史](../Category/南京军事史.md "wikilink")