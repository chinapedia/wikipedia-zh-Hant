**江南機器製造總局**，简称**江南制造总局**、**江南制造局**、**上海机器局**、**滬局**成立於1865年的[上海](../Page/上海.md "wikilink")，是[清朝](../Page/清朝.md "wikilink")[洋务運動中成立的軍事生產機構](../Page/洋务運動.md "wikilink")，為晚清中國最重要的[軍工廠](../Page/軍工廠.md "wikilink")，也是近代最早的新式[工廠之一](../Page/工廠.md "wikilink")。為後來[江南造船廠的前身](../Page/江南造船廠.md "wikilink")。

## 成立及沿革

[1967-04_1967年上海江南制造厂_朝阳号.jpg](https://zh.wikipedia.org/wiki/File:1967-04_1967年上海江南制造厂_朝阳号.jpg "fig:1967-04_1967年上海江南制造厂_朝阳号.jpg")
該機構成立於1865年9月20日的[上海](../Page/上海.md "wikilink")，由[曾國藩规划](../Page/曾國藩.md "wikilink")，後由[李鴻章經始](../Page/李鴻章.md "wikilink")[督辦](../Page/督辦.md "wikilink")，並由[丁日昌](../Page/丁日昌.md "wikilink")[總辦](../Page/總辦.md "wikilink")，實際負責。其構成是由兩個不同項目的計畫合流

曾國藩在1863年與赴美學成歸國的[容閎會面](../Page/容閎.md "wikilink")，容閎向曾提出在國內創辦機械製造廠，為國內未來工業奠基。曾國藩因此湊出了6萬8千兩白銀，委託容閎去[美國購置必要器械](../Page/美國.md "wikilink")。

而江南廠開辦，則是李鴻章的構想，主要經辦者為丁日昌。李鴻章的軍隊在[太平天國戰役期間](../Page/太平天國.md "wikilink")，為了淮軍的裝備補充已經在長江一代建成[安慶製械所](../Page/安慶製械所.md "wikilink")、[松江西洋制炮局](../Page/松江西洋制炮局.md "wikilink")（韩殿甲經辦）、[上海洋砲局等小型軍工廠](../Page/上海洋砲局.md "wikilink")；在戰役大勢底定的背景下，李鴻章上奏提出在上海設兵工廠與近代造船廠的構想。得到上層允諾後，李鴻章先行向上海租界旁虹口的美商公司[旗記鐵廠以](../Page/旗記鐵廠.md "wikilink")6萬兩白銀的代價購買其全廠機械，並將原先自辦的軍械廠全數遷入該區。在容閎採購的機械製造設備完成後，李鴻章將這批機械納入江南廠下，1865年9月，**江南機器製造總局**正式成立，總廠辦丁日昌、會辦韓殿甲、襄辦馮俊光，主要專以製造軍備為主，原先機械廠的定位則在此消滅。

由於李鴻章開辦該廠的目的是軍工使用，租界管理單位不悅該廠會破壞租界本身的中立性，因此要求遷廠。在1866年李鴻章在上海[高昌廟陳家港畔購置土地](../Page/高昌廟.md "wikilink")，並建置第一座泥質船塢（長99公尺），1867年5月將原先在虹口的設備全數遷入，此後江南廠除了製造軍械，也開始製造近代內燃動力船艦。1869年，上海[同文館遷入江南製造總局](../Page/同文館.md "wikilink")。1890年，增購了煉鋼設備，開辦煉鋼廠。\[1\]

由於江南製造總局規模過大，實際開銷入不敷出，尤其是缺少技術基礎的造船部門在李鴻章主要以外購軍艦為優先政策後，船塢長期沒有新造船隻，處於閒置。

光緒三十年（1904年）兩江總督[周馥頗視察江南廠後](../Page/周馥頗.md "wikilink")，提出「局塢分離」構想。光緒三十一年（1905年），製造局造船的部門獨立，稱作江南船塢，由海軍部管轄，海軍則聘請英國人毛根為總工程師，重振廠務。

  - 江南製造局

江南製造局本身也於1917年改稱上海兵工廠，1932年[一二八淞沪抗战時被迫停廠](../Page/一二八淞沪抗战.md "wikilink")，後在停戰協議內寫入上海非軍事化，因此上海兵工廠被迫停辦。在停辦期間，兵工機械陸續撥交給[漢陽兵工廠與](../Page/漢陽兵工廠.md "wikilink")[金陵兵工廠](../Page/金陵兵工廠.md "wikilink")，煉鋼設備則長時間閒置，在1937年抗日戰爭爆發後才陸續內遷。日軍佔領上海後，將其場地和殘餘機械併入江南造船所。

  - 江南船塢

江南船塢在改制後，將原先的泥質船塢擴大改建為木質船塢，船塢擴建後長117.3公尺（375英尺）、寬22.9公尺（75英尺）、深7.47公尺（18英尺），在1911年[辛亥革命後](../Page/辛亥革命.md "wikilink")，江南船塢改名江南造船所，維持海軍部管轄。

1915年，江南造船所船塢再度擴建，延長船塢為169.5公尺（556英尺），此為中國首個可容納萬噸級船舶的乾塢。國民革命軍北伐開入南京後，江南造船所歸南京國民政府管轄，並更名為**海軍江南造船所**。

1931年1月，海軍馬尾船政局飛機製造處併入江南造船所，所有機具與人員自原廠拆遷併入。

民國三十二年（1933年），江南造船所第三座乾塢動工，船塢分兩期工程。第一期工程是開闢出長114.3公尺（375英尺）、寬27.13公尺（89英尺）、深7.92公尺（26英尺）的船塢，工程從民國三十二年至民國三十三年（1934年）10月完工；第二期工程是將乾塢長度擴建300英尺、寬度擴建11英呎，工程在民國二十五年（1936年）4月結束，第三號船塢的總長205.7公尺（675英呎）、寬30.48公尺（100英尺）、深7.92公尺（26英呎），為當時中國最大的船塢。

1937年8月上海松滬戰役爆發後，江南造船所將一部分機器拆遷至上海租界內存放；1938年日軍占領江南造船所，更名**朝日工作部上海工廠**，委託[三菱財閥經營](../Page/三菱財閥.md "wikilink")。1945年日軍戰敗後，該廠由國民政府收回，維持海軍江南造船所之名。

1949年國民政府在[上海戰役後](../Page/上海戰役.md "wikilink")，爆破江南造船所相關機材，1949年5月25日，[中國人民解放軍佔領江南造船所](../Page/中國人民解放軍.md "wikilink")，上海市軍事管制委員會任命[張元培](../Page/張元培.md "wikilink")、王季芬為造船所正、副軍代表。1953年更名為江南造船廠，1996年改為江南造船有限責任公司，屬於[中國船舶工業集團公司](../Page/中國船舶工業集團公司.md "wikilink")。

## 管理和運作

[上海江南制造局所造航海鐘.jpg](https://zh.wikipedia.org/wiki/File:上海江南制造局所造航海鐘.jpg "fig:上海江南制造局所造航海鐘.jpg")內所展示上海江南制造局所造[航海鐘](../Page/航海鐘.md "wikilink")。\]\]
江南機器製造局是自強運動幾個兵工廠中，規模最大、預算最多的一個，除了開設當年投資約25萬兩的費用之外，其後早期主要經費來自於[淮軍的軍費](../Page/淮軍.md "wikilink")，後來1867年時曾國藩獲得許可從上海[海關取得百分之十的關稅作為製造局的經費](../Page/海關.md "wikilink")，1869年又提高到百分之二十，這相當於每年有至少40萬兩以上的經費。

製造局最高領導人是督辦，曾國藩、[左宗棠](../Page/左宗棠.md "wikilink")、[張之洞等人均曾擔任](../Page/張之洞.md "wikilink")，但晚清大多時期是由李鴻章擔任，督辦以下為行政主管，早期由李鴻章選任[馮焌光和](../Page/馮焌光.md "wikilink")[沈保靖為行政主管](../Page/沈保靖.md "wikilink")，並由[上海道台](../Page/上海道台.md "wikilink")（1865年時為[丁日昌](../Page/丁日昌.md "wikilink")）加以兼督，實際的機械管理方面的工作則多由西方人負責，例如早期的首席工程師霍斯（T．F．Falls）為美國人。

江南製造局底下僱用了大量的中國工人，包括[滿人和](../Page/滿人.md "wikilink")[漢人](../Page/漢人.md "wikilink")，在其中操作及學習機器，因其專業技能，當時工人的薪水是一般城市中苦力的4-8倍。這些工人成為中國近代最早形成的一批技術工人。

## 軍工方面的成果

江南機器製造局在[同治年間是全](../Page/同治.md "wikilink")[東亞最大的兵工廠](../Page/東亞.md "wikilink")，對於清朝的軍事力量以及重工業生產都有提昇作用。例如在1867年時，每天平均可以生產十五枝毛瑟槍和各式彈藥，李鴻章認為當時該局生產的槍械彈藥，對於後來[捻亂的平定有所助益](../Page/捻亂.md "wikilink")。除了槍彈之外，該局也在1868年生產出了中國第一艘自造的汽船（木製船身）惠吉號。1891年時，為中國首次炼出[鋼鐵](../Page/鋼鐵.md "wikilink")。

  - 枪械

<!-- end list -->

  - [边针后膛抬枪](../Page/边针后膛抬枪.md "wikilink")：口径15.9毫米，枪全长2445毫米，全重13.2公斤，铅子重23克。
  - [林明登边针枪](../Page/林明登边针枪.md "wikilink")：由美国Remington厂于1865年推出。1867年至1883年，江南制造局共生产步枪2.5万余支、马枪717支。
  - [毛瑟槍](../Page/毛瑟槍.md "wikilink")：由德国人彼得保尔毛瑟于1866年发明。1883年，江南制造局试造12支。
  - [快利步槍](../Page/快利步槍.md "wikilink")：是中国自行设计制造的第一种连发枪。1890年，两江总督李鸿章因见江南制造局所造枪支均是旧式，仿造英国[南夏枪亦不适用](../Page/南夏枪.md "wikilink")，乃“饬令专就[曼利夏](../Page/曼利夏.md "wikilink")、新毛瑟两式讲求仿造。”同年，江南制造局在购得各国样枪后，先后仿造了奥地利[曼利夏和英国](../Page/曼利夏.md "wikilink")[新利洋枪](../Page/新利.md "wikilink")。1891年，又仔细研究[曼利夏](../Page/曼利夏.md "wikilink")、[新利和](../Page/新利.md "wikilink")[南夏三种枪的优缺点](../Page/南夏.md "wikilink")，取各枪之长，设计出[快利步枪](../Page/快利步枪.md "wikilink")，制出样枪六支。1892年，送天津军械局进行试验，考察结果“与德国新毛瑟相等，其速率、线路更驾于曼利夏之上。”当年正式投入生产。

<!-- end list -->

  - 火炮

<!-- end list -->

  - [克虏伯](../Page/克虏伯.md "wikilink")305mm[后膛炮](../Page/后膛炮.md "wikilink")，于1893年开始由江南制造局制造，至甲午战争爆发时为止共生产12门，北洋舰队中的“广字”舰可能装备了此炮。

但是，整體而言，江南製造局在晚清時的軍備生產品質並不好，例如步槍的水準不高，性能不佳（据說因此連李鴻章率領的[淮軍都拒絕使用](../Page/淮軍.md "wikilink")），且生產成本高於直接購買。造船的情形亦類似，1868年首次生產之後，陸續造了數艘汽船，但所造船的速度不快，整體生產成本及耗燃料都高，結果反而不如直接向外國购買（自造一艘船的錢大約可以向英國買兩艘船）。
製造局軍備生產成本的高漲，可歸因於幾個方面：一、所有的生產原料幾乎都靠進口。二、人員薪水偏高。三、申購物資浮濫。四、人員人數過於浮濫：除了外國顧問日漸增加，中國官員、職員有不少人是利用關係進入，坐領乾薪。以官員而言，1870年代初期只有40人，大約不到十年人數就增加了一倍之多。

\[2\] \[3\]

## 文化上的影響

江南機器製造局除了機械的製造之外，另附設有[廣方言館](../Page/廣方言館.md "wikilink")（即語言學校，原設於1863年，1869年併入江南製造局）翻譯館以及工藝學堂，用以介紹西方知識，以及培養語言和科技人才，在1868年-1907年之間，譯書達160種，除以軍事科技為主之外，旁及[地理](../Page/地理.md "wikilink")、[經濟](../Page/經濟.md "wikilink")、[政治](../Page/政治.md "wikilink")、[歷史等方面的書籍](../Page/歷史.md "wikilink")，其所翻譯書籍的水準，被認為超過晚清數十年其他翻譯書籍的質量。對於晚清的知識分子吸收西方知識產生很大的影響。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - [費正清](../Page/費正清.md "wikilink")（John Fairbank）主編，張玉法
    主譯：《[劍橋中國史晚清篇](../Page/劍橋中國史.md "wikilink")》（台北：南天書局，1987年）第十章
  - [芮瑪麗](../Page/芮瑪麗.md "wikilink")（Mary Wright），房德鄰 等
    譯：《[同治中興](../Page/同治中興.md "wikilink")》（北京：中國社會科學出版社，2002年）
    ISBN 7-5004-3258-5

## 研究書目

  - T.L. Kennedy 著，楊天宏 等 譯：《李鴻章與中國軍事工業近代化》（成都：四川大學出版社，1992年）。

## 參見

  - [江南造船厂](../Page/江南造船厂.md "wikilink")
  - [洋务運動](../Page/洋务運動.md "wikilink")
  - [晚清軍事改革](../Page/晚清軍事改革.md "wikilink")

{{-}}

[category:上海经济史](../Page/category:上海经济史.md "wikilink")
[category:上海军事史](../Page/category:上海军事史.md "wikilink")

[Category:洋務運動](../Category/洋務運動.md "wikilink")
[Category:中国军事工业](../Category/中国军事工业.md "wikilink")
[Category:中国工业遗产保护名录](../Category/中国工业遗产保护名录.md "wikilink")

1.  [劉鳳瀚](../Page/劉鳳瀚.md "wikilink")《國民黨軍事制度史》下，[北京](../Page/北京.md "wikilink")：[中國大百科全書出版社](../Page/中國大百科全書出版社.md "wikilink")，頁523。
2.  <http://www.aikanshu.com/books/8691/272536.htm>
    安庆内军械所到江南制造局的过渡——上海、苏州洋炮局
3.  <http://www.aikanshu.com/books/8691/272536.htm>