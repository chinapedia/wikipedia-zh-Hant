**江南运河**，又称**江南河**，是[中国](../Page/中国.md "wikilink")[京杭大运河在](../Page/京杭大运河.md "wikilink")[长江以南的一段](../Page/长江.md "wikilink")，又分为苏南运河和浙北运河两段。沟通长江、[钱塘江及](../Page/钱塘江.md "wikilink")[太湖平原诸水系](../Page/太湖平原.md "wikilink")，是这一地区重要的水运通道。江南人民俗称江南运河为“官塘河”、“官河”或“官塘”。

## 运河河道

运河长约330公里，北起[镇江](../Page/镇江.md "wikilink")，经[丹阳](../Page/丹阳.md "wikilink")、[常州](../Page/常州.md "wikilink")、[无锡](../Page/无锡.md "wikilink")、[苏州](../Page/苏州.md "wikilink")、[吴江](../Page/吴江.md "wikilink")、[嘉兴](../Page/嘉兴.md "wikilink")、[桐乡到](../Page/桐乡.md "wikilink")[杭州](../Page/杭州.md "wikilink")。运河北接长江，南接[钱塘江](../Page/钱塘江.md "wikilink")，和丹金溧漕河、锡溧漕河、锡澄运河、望虞河、[浏河](../Page/浏河.md "wikilink")、[吴淞江](../Page/吴淞江.md "wikilink")、太浦河、吴兴塘、平湖塘、华亭塘、[杭甬运河等运河相连接](../Page/杭甬运河.md "wikilink")，是[江南河运的主干道](../Page/江南.md "wikilink")。运河在吴江[平望镇以南分成三道](../Page/平望.md "wikilink")\[1\]：去往[嘉兴的一道称为古运河](../Page/嘉兴.md "wikilink")，以[嘉兴护城河为主线](../Page/嘉兴护城河.md "wikilink")，北接前往苏州的苏州塘（或称[苏嘉运河](../Page/苏嘉运河.md "wikilink")），西转[杭州塘](../Page/杭州塘.md "wikilink")，又有故道至海宁[盐官古城转](../Page/盐官镇_\(海宁市\).md "wikilink")[上塘河](../Page/上塘河.md "wikilink")，塘河河网交织；直接去往杭州的一道由[元末军阀](../Page/元朝.md "wikilink")[张士诚开凿](../Page/张士诚.md "wikilink")；去往[湖州的一道称为](../Page/湖州.md "wikilink")[吴兴塘](../Page/吴兴塘.md "wikilink")，即今天的湖嘉申航道[頔塘河道](../Page/頔塘.md "wikilink")，向南有多条塘路可达杭州。

## 历史

早在[春秋時的](../Page/春秋.md "wikilink")[吳國](../Page/吳國.md "wikilink")，即以都城吳（今[蘇州](../Page/蘇州.md "wikilink")）為中心，在太湖平原鑿了許多條運河，其中最早的要属泰伯渎，连接了今天的苏州、无锡两地，历代修筑乃通长江，明以后常称常州府河；吴越争霸时期，吴国为运粮通过连接自然水道的方式把运河南展至[錢塘江](../Page/钱塘江.md "wikilink")，越王勾践灭吴后改建为吴陵道\[2\]\[3\]\[4\]。[秦始皇灭六国后](../Page/秦始皇.md "wikilink")，为东巡[会稽](../Page/会稽.md "wikilink")，在春秋古运河的基础上建成北起[丹徒](../Page/丹徒.md "wikilink")（治所在今镇江东南丹徒镇），中经会稽郡治[吴县](../Page/吴县.md "wikilink")（今苏州），南到钱塘（今杭州）的水道，名曰陵水道\[5\]。[三国](../Page/三国.md "wikilink")[孙吴时代](../Page/孙吴.md "wikilink")，孙吴政权为了沟通[建业城和](../Page/建业城.md "wikilink")[太湖平原而开凿江南运河](../Page/太湖平原.md "wikilink")，以后历朝历代又不断加强，至[唐代基本形成现在的格局](../Page/唐代.md "wikilink")。此后[东晋时又多次对此水道加以疏浚整修](../Page/东晋.md "wikilink")。到[隋朝时](../Page/隋朝.md "wikilink")，隋煬帝以东都[洛阳为中心](../Page/洛阳.md "wikilink")，开凿连通南北的大运河。在先后开凿[通济渠](../Page/通济渠.md "wikilink")、疏通[邗沟](../Page/邗沟.md "wikilink")、开凿永济渠之后，[隋煬帝于](../Page/隋煬帝.md "wikilink")[大業六年](../Page/大業.md "wikilink")（610年）下旨疏凿和拓宽长江以南的运河古道，连接[京口](../Page/京口.md "wikilink")（今[镇江](../Page/镇江.md "wikilink")）和[餘杭](../Page/餘杭.md "wikilink")（今[杭州](../Page/杭州.md "wikilink")），将就有运河连接拓宽，截弯取直\[6\]。《[资治通鉴](../Page/资治通鉴.md "wikilink")·隋纪四》記載：“大業六年冬十二月，敕穿江南河，自京口至餘杭，八百餘里，廣十餘丈，使可通龍舟，並置驛宮、草頓，欲東巡會稽。”

## 参考文献

## 参见

  - [大运河](../Page/大运河.md "wikilink")
      - [京杭大运河](../Page/京杭大运河.md "wikilink")
      - [浙东运河](../Page/浙东运河.md "wikilink")

{{-}}

[Category:京杭大运河](../Category/京杭大运河.md "wikilink")
[Category:江苏运河](../Category/江苏运河.md "wikilink")
[Category:浙江运河](../Category/浙江运河.md "wikilink")

1.

2.

3.

4.

5.
6.