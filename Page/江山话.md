**江山话**是指浙江省[衢州](../Page/衢州.md "wikilink")[江山市的方言](../Page/江山市.md "wikilink")，屬於[吴语](../Page/吴语.md "wikilink")[上麗片](../Page/上麗片.md "wikilink")[上山小片](../Page/上山小片.md "wikilink")（舊分區法中屬[處衢片](../Page/處衢片.md "wikilink")[龍衢小片](../Page/龍衢小片.md "wikilink")）。

## 音系

### 声母

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p><a href="../Page/唇音.md" title="wikilink">唇音</a></p></th>
<th><p><a href="../Page/齿音.md" title="wikilink">齿音</a></p></th>
<th><p><a href="../Page/龈腭音.md" title="wikilink">龈腭音</a></p></th>
<th><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a></p></th>
<th><p><a href="../Page/聲門音.md" title="wikilink">喉音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td><p>馬</p></td>
<td><p>腦</p></td>
<td></td>
<td><p>我</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td><p><small>清不送气</small></p></td>
<td><p>巴</p></td>
<td><p>豬</p></td>
<td></td>
<td><p>姜</p></td>
</tr>
<tr class="odd">
<td><p><small>清送气</small></p></td>
<td><p>怕</p></td>
<td><p>聽</p></td>
<td></td>
<td><p>氣</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><small>弛声</small></p></td>
<td><p>爬</p></td>
<td><p>同</p></td>
<td></td>
<td><p>群</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td><p><small>清不送气</small></p></td>
<td></td>
<td><p>再</p></td>
<td><p>朱</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small>清送气</small></p></td>
<td></td>
<td><p>寸</p></td>
<td><p>唱</p></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><small>弛声</small></p></td>
<td></td>
<td><p>茶</p></td>
<td><p>傳</p></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td><p><small>清不送气</small></p></td>
<td><p>夫</p></td>
<td><p>山</p></td>
<td><p>心</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small>弛声</small></p></td>
<td><p>浮</p></td>
<td><p>事</p></td>
<td><p>樹</p></td>
<td></td>
<td><p>雲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/边音.md" title="wikilink">边近音</a></p></td>
<td></td>
<td><p>李</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 韵母

|   |   |   |   |   |
| - | - | - | - | - |
| 耳 | 衣 | 无 | 雨 | 之 |
| 拉 | 家 | 瓦 |   |   |
| 鞋 | 也 | 快 |   |   |
| 天 |   | 全 |   |   |
| 开 |   | 会 |   |   |
| 哥 |   |   |   |   |
| 包 | 表 |   |   |   |
| 楼 | 秋 |   |   |   |
| 三 | 香 | 关 |   |   |
| 干 | 状 |   |   |   |
| 盘 | 官 |   |   |   |
| 门 | 穷 |   |   |   |
| 齿 | 鱼 |   |   |   |
| 金 |   | 森 |   |   |
| 京 |   | 春 |   |   |
| 八 | 学 | 扩 |   |   |
| 笔 |   | 十 |   |   |
| 日 | 习 |   | 越 |   |
| 月 | 肉 |   |   |   |
| 合 |   | 肉 |   |   |

### 声调

|    |     |      |
| -- | --- | ---- |
| 阴平 | 44  | 天空飞山 |
| 阳平 | 231 | 南来田皮 |
| 阴上 | 324 | 纸九火口 |
| 阳上 | 22  | 坐买有被 |
| 阴去 | 51  | 菜四送去 |
| 阳去 | 31  | 备洞路硬 |
| 阴入 | 45  | 七雪踢客 |
| 阳入 | 12  | 六肉白独 |

## 民间[谚语](../Page/谚语.md "wikilink")

  - 春雾晴，夏雾雨，秋雾霜，冬雾雪。　
  - 天红霞，呒水泡茶。
  - 春寒多雨水。　
  - 雨天挑稻草——越挑越重。
  - 一九二九，汗汃雨流；

<!-- end list -->

  -
    三九廿七，曰事结疙；
    四九三十六，门前挂糊搭；
    五九四十五，穷汉街上舞；
    六九五十四，提篮讨野菜；
    七九六十三，搭衣挂肩担；
    八九七十二，黄狗在阴边嬉；
    九九八十一，犁耕耙糙统统出；
    十九足，豁秧谷。

## 相关条目

  - [吴语](../Page/吴语.md "wikilink")
  - [衢州话](../Page/衢州话.md "wikilink")

[Category:吳語](../Category/吳語.md "wikilink")
[Category:衢州](../Category/衢州.md "wikilink")
[Category:浙江語言](../Category/浙江語言.md "wikilink")