[Edo_1844-1848_Map.jpg](https://zh.wikipedia.org/wiki/File:Edo_1844-1848_Map.jpg "fig:Edo_1844-1848_Map.jpg")

**江戶**（）是[日本](../Page/日本.md "wikilink")[東京之舊稱](../Page/東京.md "wikilink")，特別是指[江戶時代的東京](../Page/江戶時代.md "wikilink")，以[江戶城](../Page/江戶城.md "wikilink")（今[皇居](../Page/皇居.md "wikilink")）為城市的中心\[1\]。當時統治日本的[德川氏以江戶城做為居所](../Page/德川氏.md "wikilink")，並將[幕府設置於此](../Page/幕府_\(日本\).md "wikilink")，使江戶成為當時日本實質的政治中心。[江戶幕府與江戶時代便是得名於此](../Page/江戶幕府.md "wikilink")。

江戶的[市區分為兩大部分](../Page/市區.md "wikilink")，分別為江戶城西側的「」、以及東側的「[下町](../Page/下町.md "wikilink")」。作為江戶幕府的[直轄地](../Page/天領.md "wikilink")，江戶同時是德川氏（[德川將軍家](../Page/德川將軍家.md "wikilink")）的[城下町](../Page/城下町.md "wikilink")，別稱「武陽」（）。江戶幕府的[將軍大多居住於江戶城](../Page/江戶幕府將軍列表.md "wikilink")，但日本的政治中心在[幕末時代轉移到](../Page/幕末.md "wikilink")[京都](../Page/京都.md "wikilink")[二条城](../Page/二条城.md "wikilink")，而末代將軍[德川慶喜就任後完全沒在江戶城居住過](../Page/德川慶喜.md "wikilink")。進入[明治時代後](../Page/明治.md "wikilink")，江戶被更名為東京，並取代京都成為[日本首都至今](../Page/日本首都.md "wikilink")。

## 歷史

「江戶」一詞最早記載於[鎌倉時代的書籍](../Page/鎌倉時代.md "wikilink")《[吾妻鏡](../Page/吾妻鏡.md "wikilink")》，是在[平安時代後期產生的地名](../Page/平安時代.md "wikilink")。其由來眾說紛紜，但最可信的解釋是從「江之入口」（）簡化而來，其中「江」是指「」，是日語對海、湖等水體深入陸地的地形稱呼，類似[港灣](../Page/港灣.md "wikilink")，「戶」則是入口之意。當時的江戶是位於做為[武藏國與](../Page/武藏國.md "wikilink")[下總國分界的](../Page/下總國.md "wikilink")[隅田川的](../Page/隅田川.md "wikilink")[河口西側](../Page/河口.md "wikilink")，這個河口被稱為「」（位於現今的東京[日比谷與](../Page/日比谷.md "wikilink")[丸之內一帶](../Page/丸之內.md "wikilink")），與日後築城的江戶城址相當接近。

江戶的開發始於[平安時代末期](../Page/平安時代.md "wikilink")，秩父地區秩父氏（武藏國守代理職河越氏分家，畠山氏的分家）的分家，秩父重綱之子四朗，在江戶櫻田築居館（現在皇居的本丸、二之丸），並以該地為姓氏，改名為江戶重繼，建立了。之後掌握[關東管領職位的](../Page/關東管領.md "wikilink")[扇谷上杉家](../Page/扇谷上杉家.md "wikilink")，由其家臣江戶氏太田資長（即[太田道灌](../Page/太田道灌.md "wikilink")）在江戶氏的住所舊址上興築了江戶城。[德川家康在](../Page/德川家康.md "wikilink")[關原之戰取得關鍵性的勝利後](../Page/關原之戰.md "wikilink")，於1603年以江戶城為據點開設[江戶幕府](../Page/江戶幕府.md "wikilink")，開啟了往後[德川家掌權逾兩百年的江戶時代](../Page/德川家.md "wikilink")。

[天正十八年](../Page/天正_\(日本\).md "wikilink")（1590年）[八月初一](../Page/八月初一.md "wikilink")，德川家康受贈[關八州而入城](../Page/關八州.md "wikilink")，江戶城雖已歷經太田道灌、[北條氏的經營](../Page/北條氏.md "wikilink")，但多經兵禍而荒廢，並不具備大城的樣貌，家康開始大力整建，仍比不上[京都及](../Page/京都.md "wikilink")[大坂](../Page/大坂.md "wikilink")（今[大阪](../Page/大阪.md "wikilink")）的人口及繁榮度。至第三代將軍[德川家光時期因](../Page/德川家光.md "wikilink")[參勤交代制度化](../Page/參勤交代.md "wikilink")，各藩單身赴任的武士及服徭役的農民大量擁入，至此頃日本一國之力歷經三百年繁榮江戶，向江戶內海（今[東京灣](../Page/東京灣.md "wikilink")）[填海建設市街](../Page/填海.md "wikilink")、開鑿江戶城外[護城河](../Page/護城河.md "wikilink")（即）、整治江戶市區河川、建設[玉川上水等供水設施](../Page/玉川上水.md "wikilink")，使得江戶的城市建設得以大力飛昇。

隨著[武家與外來人口的大量移入](../Page/武家.md "wikilink")，江戶成為[東日本的經濟樞紐](../Page/東日本.md "wikilink")，也逐漸與[大坂](../Page/大坂.md "wikilink")、京都所在的「[上方](../Page/上方.md "wikilink")」並列為日本文化的中心之地。另一方面，大量人口從農村移入江戶，也造成不少都市發展上的障礙。1657年的[明曆大火後](../Page/明曆大火.md "wikilink")，江戶展開一連串[都市更新計畫](../Page/都市更新.md "wikilink")，大量開闢[防火道](../Page/防火道.md "wikilink")、等防火設施，並重新配置[大名宅邸](../Page/大名_\(称谓\).md "wikilink")，市區範圍逐漸跨越隅田川向東擴張。根據後世推算，在18世紀初超過100萬人，是當時世界最大的城市之一，江戶時代結束時也有50萬人，仍居世界前列。

[慶應四年](../Page/慶應.md "wikilink")（1868年）[七月十七](../Page/七月十七.md "wikilink")，明治天皇[下詔將江戶改名為東京](../Page/江戶改稱為東京詔書.md "wikilink")，並在同年十至十二月，隔年（[明治二年](../Page/明治.md "wikilink")，1869年）更將[皇室與](../Page/日本皇室.md "wikilink")[政府駐地從](../Page/中央政府所在地.md "wikilink")[京都遷到江戶](../Page/京都.md "wikilink")，史稱[遷都東京](../Page/遷都東京.md "wikilink")。此舉使東京成為實質上的[日本首都](../Page/日本首都.md "wikilink")，也延續江戶時代以來做為日本政經中樞的地位，直至今日。

## 都市發展

江戶時代初期，江戶的範圍僅止於今日的[東京都](../Page/東京都.md "wikilink")[千代田區周邊](../Page/千代田區.md "wikilink")。明曆大火後，江戶市區的範圍向外擴張，因而有「八百八町」的稱號。1818年，江戶幕府劃定「」，正式確立江戶的都市範圍，[四方邊界分別至今之](../Page/四方.md "wikilink")[龜戶](../Page/龜戶.md "wikilink")、[西新宿](../Page/西新宿.md "wikilink")、[南品川與](../Page/南品川.md "wikilink")[千住](../Page/千住.md "wikilink")，這也是後世「大江戶」一詞所指的範圍。

與眾多日本城市相同，江戶發展的過程中刻意分為兩個特色區域，其一是「下町」，指江戶城東側的[隅田川為首之河川](../Page/隅田川.md "wikilink")、護城河地帶的[庶民市街](../Page/庶民.md "wikilink")，另一是武家住宅區所在的「」，從江戶城西南側向北延伸，地理上位於[武藏野台地東端](../Page/武藏野台地.md "wikilink")。

## 人口

## 參考資料

## 參見

  - [江戶城](../Page/江戶城.md "wikilink")
  - [江戶時代](../Page/江戶時代.md "wikilink")
  - [江戶幕府](../Page/江戶幕府.md "wikilink")
  - [東京](../Page/東京.md "wikilink")
  - [東京歷史](../Page/東京歷史.md "wikilink")

[江戶](../Category/江戶.md "wikilink")
[Category:東京歷史](../Category/東京歷史.md "wikilink")

1.  （[江戶城歷史完全圖解](http://www.books.com.tw/exep/prod/magazine/magfile.php?item=M010017249)
    ）