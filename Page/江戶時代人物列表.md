本列表是[日本](../Page/日本.md "wikilink")**[江戶時代](../Page/江戶時代.md "wikilink")**人物列表：

## 皇室

### 天皇、皇族

  - [後陽成天皇](../Page/後陽成天皇.md "wikilink")
  - [後水尾天皇](../Page/後水尾天皇.md "wikilink")
  - [明正天皇](../Page/明正天皇.md "wikilink")
  - [後光明天皇](../Page/後光明天皇.md "wikilink")
  - [後西天皇](../Page/後西天皇.md "wikilink")
  - [靈元天皇](../Page/靈元天皇.md "wikilink")
  - [東山天皇](../Page/東山天皇.md "wikilink")
  - [中御門天皇](../Page/中御門天皇.md "wikilink")
  - [櫻町天皇](../Page/櫻町天皇.md "wikilink")
  - [桃園天皇](../Page/桃園天皇.md "wikilink")
  - [後櫻町天皇](../Page/後櫻町天皇.md "wikilink")
  - [後桃園天皇](../Page/後桃園天皇.md "wikilink")
  - [光格天皇](../Page/光格天皇.md "wikilink")
  - [仁孝天皇](../Page/仁孝天皇.md "wikilink")
  - [孝明天皇](../Page/孝明天皇.md "wikilink")
  - [東福門院](../Page/德川和子.md "wikilink")

### 公家

  - [近衛家熙](../Page/近衛家熙.md "wikilink")
  - [近衛基熙](../Page/近衛基熙.md "wikilink")
  - [烏丸光廣](../Page/烏丸光廣.md "wikilink")
  - [中山愛親](../Page/中山愛親.md "wikilink")
  - [野宮定基](../Page/野宮定基.md "wikilink")

## 幕府

### 將軍

  - [德川家康](../Page/德川家康.md "wikilink")　　(初代)
  - [德川秀忠](../Page/德川秀忠.md "wikilink")　　(二代)
  - [德川家光](../Page/德川家光.md "wikilink")　　(三代)
  - [德川家綱](../Page/德川家綱.md "wikilink")　　(四代)
  - [德川綱吉](../Page/德川綱吉.md "wikilink")　　(五代)
  - [德川家宣](../Page/德川家宣.md "wikilink")　　(六代)
  - [德川家繼](../Page/德川家繼.md "wikilink")　　(七代)
  - [德川吉宗](../Page/德川吉宗.md "wikilink")　　(八代)
  - [德川家重](../Page/德川家重.md "wikilink")　　(九代)
  - [德川家治](../Page/德川家治.md "wikilink")　　(十代)
  - [德川家齊](../Page/德川家齊.md "wikilink")　(十一代)
  - [德川家慶](../Page/德川家慶.md "wikilink")　(十二代)
  - [德川家定](../Page/德川家定.md "wikilink")　(十三代)
  - [德川家茂](../Page/德川家茂.md "wikilink")　(十四代)
  - [德川慶喜](../Page/德川慶喜.md "wikilink")　(十五代)

## 大名

  - [浅野長矩](../Page/浅野長矩.md "wikilink")
  - [上杉鷹山](../Page/上杉鷹山.md "wikilink")
  - [池田光政](../Page/池田光政.md "wikilink")
  - [酒井忠勝](../Page/酒井忠勝_\(出羽國庄內藩主\).md "wikilink")
  - [島津重豪](../Page/島津重豪.md "wikilink")
  - [竹中重義](../Page/竹中重義.md "wikilink")　
  - [伊達政宗](../Page/伊達政宗.md "wikilink")
  - [徳川忠長](../Page/徳川忠長.md "wikilink")
  - [細川重賢](../Page/細川重賢.md "wikilink")
  - [松前公廣](../Page/松前公廣.md "wikilink")
  - [松前慶廣](../Page/松前慶廣.md "wikilink")
  - [松倉勝家](../Page/松倉勝家.md "wikilink")　
  - [毛利重就](../Page/毛利重就.md "wikilink")
  - [島津齊彬](../Page/島津齊彬.md "wikilink")

### 陪臣

  - [坂本龍馬](../Page/坂本龍馬.md "wikilink")

## 商人

  - [茶屋四郎次郎](../Page/茶屋四郎次郎.md "wikilink")

## 學者

### 儒學

### 國學

### 兵法家

### 其他學者

  - [小幡景憲](../Page/小幡景憲.md "wikilink")
  - [北條氏長](../Page/北條氏長.md "wikilink")
  - [楠木正辰](../Page/楠木正辰.md "wikilink")
  - [由井正雪](../Page/由井正雪.md "wikilink")
  - [山鹿素行](../Page/山鹿素行.md "wikilink")
  - [山下幸內](../Page/山下幸內.md "wikilink")

## 文人

### 畫家

### 文藝

### 音樂

### 其他文人

## 劍客

  - [宮本武藏](../Page/宮本武藏.md "wikilink")
  - [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink")
  - [伊東一刀齋](../Page/伊東一刀齋.md "wikilink")
  - [千葉周作](../Page/千葉周作.md "wikilink")
  - [柳生宗冬](../Page/柳生宗冬.md "wikilink")
  - [柳生宗矩](../Page/柳生宗矩.md "wikilink")
  - [松浦清](../Page/松浦清.md "wikilink")
  - [岡田以藏](../Page/岡田以藏.md "wikilink")

## 其他人物

## 參看

  - [藩列表](../Page/藩列表.md "wikilink")
  - [日本幕末人物列表](../Page/日本幕末人物列表.md "wikilink")

[Category:日本人列表](../Category/日本人列表.md "wikilink")
[江戶時代人物](../Category/江戶時代人物.md "wikilink")