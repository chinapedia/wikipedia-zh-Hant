  -
    参见：[中华人民共和国友好城市或姐妹城市列表](../Page/中华人民共和国友好城市或姐妹城市列表.md "wikilink")。

## 省级

|                                     |                                                                                             |
| ----------------------------------: | :------------------------------------------------------------------------------------------ |
| 〖[江蘇省](../Page/江蘇省.md "wikilink")〗- | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[維多利亞州](../Page/維多利亞州.md "wikilink") (1979年11月18日) |
|                                     | <small>[日本](../Page/日本.md "wikilink")[愛知縣](../Page/愛知縣.md "wikilink") (1980年7月28日)          |
|                                     | <small>[朝鮮](../Page/朝鮮.md "wikilink")[江原道](../Page/江原道.md "wikilink") (1984年11月8日)          |
|                                     | <small>[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink") (1985年11月21日)     |
|                                     | <small>[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink") (1989年4月21日)          |
|                                     | <small>[英國](../Page/英國.md "wikilink")[埃塞克斯郡](../Page/埃塞克斯郡.md "wikilink") (1992年7月16日)      |
|                                     | <small>[德國](../Page/德國.md "wikilink")[北威州](../Page/北威州.md "wikilink") (1992年8月1日)           |
|                                     | <small>[義大利](../Page/義大利.md "wikilink")[托斯卡納大區](../Page/托斯卡納大區.md "wikilink") (1992年9月18日)  |
|                                     | <small>[日本](../Page/日本.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink") (1992年11月4日)          |
|                                     | <small>[巴基斯坦](../Page/巴基斯坦.md "wikilink")[旁遮普省](../Page/旁遮普省.md "wikilink") (1993年12月28日)   |
|                                     | <small>[德國](../Page/德國.md "wikilink")[巴登-符腾堡州](../Page/巴登-符腾堡州.md "wikilink") (1994年4月23日)  |
|                                     | <small>[荷蘭](../Page/荷蘭.md "wikilink")[北布拉邦省](../Page/北布拉邦省.md "wikilink") (1994年9月9日)       |
|                                     | <small>[韓國](../Page/韓國.md "wikilink")[全羅北道](../Page/全羅北道.md "wikilink") (1994年10月27日)       |
|                                     | <small>[巴西](../Page/巴西.md "wikilink")[米纳斯吉拉斯州](../Page/米纳斯吉拉斯州.md "wikilink") (1996年3月27日)  |
|                                     | <small>[義大利](../Page/義大利.md "wikilink")[威尼托大區](../Page/威尼托大區.md "wikilink") (1998年6月22日)    |
|                                     | <small>[瑞典](../Page/瑞典.md "wikilink")[東約特蘭省](../Page/東約特蘭省.md "wikilink") (1999年3月22日)      |
|                                     | <small>[俄羅斯](../Page/俄羅斯.md "wikilink")[莫斯科州](../Page/莫斯科州.md "wikilink") (1999年8月20日)      |
|                                     | <small>[比利時](../Page/比利時.md "wikilink")[那慕爾省](../Page/那慕爾省.md "wikilink") (2000年5月7日)       |
|                                     | <small>[南非](../Page/南非.md "wikilink")[自由州省](../Page/自由邦_\(南非\).md "wikilink") (2000年6月7日)   |
|                                     | <small>[波蘭](../Page/波蘭.md "wikilink")[小波蘭省](../Page/小波蘭省.md "wikilink") (2000年11月16日)       |
|                                     | <small>[芬蘭](../Page/芬蘭.md "wikilink")[南芬蘭省](../Page/南芬兰.md "wikilink") (2001年5月11日)         |
|                                     | <small>[哥倫比亞](../Page/哥倫比亞.md "wikilink")[大西洋省](../Page/大西洋省.md "wikilink") (2001年6月4日)     |
|                                     | <small>[馬來西亞](../Page/馬來西亞.md "wikilink")[马六甲州](../Page/马六甲.md "wikilink") (2002年9月18日)     |
|                                     | <small>[法國](../Page/法國.md "wikilink")[阿爾薩斯大區](../Page/阿爾薩斯大區.md "wikilink") (2007年5月24日)    |
|                                     | <small>[墨西哥](../Page/墨西哥.md "wikilink")[下加利福尼亚](../Page/下加利福尼亚.md "wikilink") (2006年8月23日)  |
|                                     | <small>[瑞士](../Page/瑞士.md "wikilink")[盧塞恩州](../Page/盧塞恩州.md "wikilink") (2011年4月26日)        |
|                                     | <small>[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink") (2011年7月18日)    |
|                                     | <small>[西班牙](../Page/西班牙.md "wikilink")[巴斯克自治區](../Page/巴斯克自治區.md "wikilink") (2012年4月27日)  |
|                                     | <small>[土耳其](../Page/土耳其.md "wikilink")[伊兹密爾省](../Page/伊兹密爾省.md "wikilink") (2012年4月30日)    |
|                                     | <small>[丹麥](../Page/丹麥.md "wikilink")[首都大區](../Page/首都大區.md "wikilink") (2015年1月30日)        |
|                                     | <small>[白俄羅斯](../Page/白俄羅斯.md "wikilink")[莫吉廖夫州](../Page/莫吉廖夫州.md "wikilink") (2015年5月10日)  |
|                                     | <small>[納米比亞](../Page/納米比亞.md "wikilink")[霍馬斯省](../Page/霍馬斯區.md "wikilink") (2015年6月19日)    |

## 地级

|                                       |                                                                                                |
| ------------------------------------: | :--------------------------------------------------------------------------------------------- |
|   〖[常熟市](../Page/常熟市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[綾部市](../Page/綾部市.md "wikilink") (1989年5月12日)             |
|                                       | <small>[日本](../Page/日本.md "wikilink")[川內市](../Page/川內市.md "wikilink") (1991年7月26日)             |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[湯斯維爾](../Page/湯斯維爾.md "wikilink") (1995年4月30日)       |
|                                       | <small>[美國](../Page/美國.md "wikilink")[惠蒂爾](../Page/惠蒂爾.md "wikilink") (1995年5月12日)             |
|                                       | <small>[法國](../Page/法國.md "wikilink")[布列斯特](../Page/布列斯特.md "wikilink") (1996年7月16日)           |
|   〖[常州市](../Page/常州市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[普拉托](../Page/普拉托.md "wikilink") (1986年6月6日)            |
|                                       | <small>[日本](../Page/日本.md "wikilink")[高槻市](../Page/高槻市.md "wikilink") (1987年3月18日)             |
|                                       | <small>[日本](../Page/日本.md "wikilink")[所澤市](../Page/所澤市.md "wikilink") (1992年4月19日)             |
|                                       | <small>[荷蘭](../Page/荷蘭.md "wikilink")[蒂爾堡](../Page/蒂爾堡.md "wikilink") (1997年4月8日)              |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[赫斯特維爾](../Page/赫斯特維爾.md "wikilink") (1998年5月6日)      |
|                                       | <small>[毛里求斯](../Page/毛里求斯.md "wikilink")[博巴森](../Page/博巴森.md "wikilink") (1998年10月9日)         |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[南揚州](../Page/南揚州.md "wikilink") (1999年9月21日)             |
|                                       | <small>[美國](../Page/美國.md "wikilink")[羅克福德](../Page/羅克福德.md "wikilink") (1999年10月25日)          |
|                                       | <small>[巴西](../Page/巴西.md "wikilink")[南卡希亞斯](../Page/南卡希亞斯.md "wikilink") (2003年9月28日)         |
|   〖[武進區](../Page/武進區.md "wikilink")〗- | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[阿什菲爾德](../Page/阿什菲爾德.md "wikilink") (1998年11月26日)    |
|   〖[大豐市](../Page/大豐市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[阿斯科利皮切諾省](../Page/阿斯科利皮切諾省.md "wikilink") (2001年9月27日) |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[九裏](../Page/九裏.md "wikilink") (2003年11月26日)              |
|   〖[丹陽市](../Page/丹陽市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[柴田町](../Page/柴田町.md "wikilink") (1994年2月23日)             |
|   〖[高郵市](../Page/高郵市.md "wikilink")〗- | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[南格蘭扁郡](../Page/南格蘭扁郡.md "wikilink") (1999年9月25日)     |
|   〖[淮安市](../Page/淮安市.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[韋尼雪](../Page/韋尼雪.md "wikilink") (1988年7月2日)              |
|                                       | <small>[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")[昆卡](../Page/昆卡.md "wikilink") (1993年4月20日)           |
|                                       | <small>[日本](../Page/日本.md "wikilink")[西山町](../Page/西山町.md "wikilink") (1995年10月29日)            |
|                                       | <small>[白俄羅斯](../Page/白俄羅斯.md "wikilink")[戈梅利](../Page/戈梅利.md "wikilink") (1997年6月19日)         |
|                                       | <small>[美國](../Page/美國.md "wikilink")[亞伯林達](../Page/亞伯林達.md "wikilink") (1998年3月5日)            |
|                                       | <small>[日本](../Page/日本.md "wikilink")[賀陽町](../Page/賀陽町.md "wikilink") (1999年1月26日)             |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[完州](../Page/完州.md "wikilink") (1999年4月22日)               |
|                                       | <small>[義大利](../Page/義大利.md "wikilink")[盧卡省](../Page/盧卡省.md "wikilink") (2000年9月27日)           |
|   〖[江都市](../Page/江都市.md "wikilink")〗- | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[古爾本](../Page/古爾本.md "wikilink") (2000年11月22日)        |
|                                       | <small>[美國](../Page/美國.md "wikilink")[斯坦福](../Page/斯坦福.md "wikilink") (2003年9月25日)             |
|   〖[江陰市](../Page/江陰市.md "wikilink")〗- | <small>[巴西](../Page/巴西.md "wikilink")[貝洛奧裏藏特](../Page/貝洛奧裏藏特.md "wikilink") (1996年11月22日)      |
|                                       | <small>[日本](../Page/日本.md "wikilink")[藤岡市](../Page/藤岡市.md "wikilink") (2000年4月28日)             |
|   〖[昆山市](../Page/昆山市.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[南艾爾蒙地](../Page/南艾爾蒙地.md "wikilink") (1993年6月7日)          |
|                                       | <small>[納米比亞](../Page/納米比亞.md "wikilink")[赫魯特方丹](../Page/赫魯特方丹.md "wikilink") (2003年7月21日)     |
|   〖[溧陽市](../Page/溧陽市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[松任](../Page/松任.md "wikilink") (1996年10月23日)              |
| 〖[連雲港市](../Page/連雲港市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[堺市](../Page/堺市.md "wikilink") (1983年12月3日)               |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[木浦](../Page/木浦.md "wikilink") (1993年7月1日)                |
|                                       | <small>[新西蘭](../Page/新西蘭.md "wikilink")[內皮爾](../Page/內皮爾.md "wikilink") (1994年6月3日)            |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[吉朗](../Page/吉朗.md "wikilink") (1994年7月21日)           |
|                                       | <small>[俄羅斯](../Page/俄羅斯.md "wikilink")[伏爾加斯基](../Page/伏爾加斯基.md "wikilink") (1997年12月18日)      |
|                                       | <small>[日本](../Page/日本.md "wikilink")[佐賀](../Page/佐賀.md "wikilink") (1998年11月27日)              |
|   〖[南京市](../Page/南京市.md "wikilink")〗- | <small>~~[日本](../Page/日本.md "wikilink")[名古屋](../Page/名古屋.md "wikilink") (1978年12月21日)~~\[1\]   |
|                                       | <small>[美國](../Page/美國.md "wikilink")[聖路易斯](../Page/圣路易斯_\(密苏里州\).md "wikilink") (1979年11月2日)  |
|                                       | <small>[義大利](../Page/義大利.md "wikilink")[佛羅倫斯](../Page/佛羅倫斯.md "wikilink") (1980年2月22日)         |
|                                       | <small>[荷蘭](../Page/荷蘭.md "wikilink")[埃因霍温](../Page/埃因霍温.md "wikilink") (1985年10月9日)           |
|                                       | <small>[德國](../Page/德國.md "wikilink")[萊比錫](../Page/萊比錫.md "wikilink") (1988年5月21日)             |
|                                       | <small>[墨西哥](../Page/墨西哥.md "wikilink")[墨西卡利](../Page/墨西卡利.md "wikilink") (1991年10月14日)        |
|                                       | <small>[賽普勒斯](../Page/賽普勒斯.md "wikilink")[利馬索爾](../Page/利馬索爾.md "wikilink") (1992年9月23日)       |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[大田](../Page/大田.md "wikilink") (1994年11月14日)              |
|                                       | <small>[加拿大](../Page/加拿大.md "wikilink")[倫敦](../Page/倫敦.md "wikilink") (1997年5月7日)              |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[珀斯](../Page/珀斯.md "wikilink") (1998年5月18日)           |
|                                       | <small>[南非](../Page/南非.md "wikilink")[布隆方丹](../Page/布隆方丹.md "wikilink") (2000年3月22日)           |
|                                       | <small>[哥倫比亞](../Page/哥倫比亞.md "wikilink")[巴蘭基亞](../Page/巴蘭基亞.md "wikilink") (2001年6月4日)        |
|   〖[南通市](../Page/南通市.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[斯旺西](../Page/斯旺西.md "wikilink") (1987年4月10日)             |
|                                       | <small>[日本](../Page/日本.md "wikilink")[豐橋市](../Page/豐橋市.md "wikilink") (1987年5月26日)             |
|                                       | <small>[日本](../Page/日本.md "wikilink")[和泉](../Page/和泉.md "wikilink") (1993年4月24日)               |
|                                       | <small>[美國](../Page/美國.md "wikilink")[澤西](../Page/澤西.md "wikilink") (1996年4月2日)                |
|                                       | <small>[德國](../Page/德國.md "wikilink")[特羅斯多夫](../Page/特羅斯多夫.md "wikilink") (1997年4月8日)          |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[金堤](../Page/金堤.md "wikilink") (1997年10月22日)              |
|                                       | <small>[義大利](../Page/義大利.md "wikilink")[奇維塔韋基亞](../Page/奇維塔韋基亞.md "wikilink") (1999年12月1日)     |
|                                       | <small>[加拿大](../Page/加拿大.md "wikilink")[裏穆斯基](../Page/裏穆斯基.md "wikilink") (2003年9月8日)          |
|   〖[蘇州市](../Page/蘇州市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink") (1980年3月24日)           |
|                                       | <small>[加拿大](../Page/加拿大.md "wikilink")[維多利亞](../Page/維多利亞.md "wikilink") (1980年10月20日)        |
|                                       | <small>[日本](../Page/日本.md "wikilink")[池田市](../Page/池田市.md "wikilink") (1981年6月6日)              |
|                                       | <small>[日本](../Page/日本.md "wikilink")[金澤市](../Page/金澤市.md "wikilink") (1981年6月13日)             |
|                                       | <small>[美國](../Page/美國.md "wikilink")[波特蘭](../Page/波特蘭.md "wikilink") (1988年6月7日)              |
|                                       | <small>[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[圖爾恰縣](../Page/圖爾恰縣.md "wikilink") (1995年9月20日)       |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[全州](../Page/全州.md "wikilink") (1996年3月21日)               |
|                                       | <small>[拉脫維亞](../Page/拉脫維亞.md "wikilink")[里加](../Page/里加.md "wikilink") (1997年9月22日)           |
|                                       | <small>[埃及](../Page/埃及.md "wikilink")[伊斯梅利亞](../Page/伊斯梅利亞.md "wikilink") (1998年3月3日)          |
|                                       | <small>[法國](../Page/法國.md "wikilink")[格勒諾布](../Page/格勒諾布.md "wikilink") (1998年9月20日)           |
|                                       | <small>[荷蘭](../Page/荷蘭.md "wikilink")[奈梅亨](../Page/奈梅亨.md "wikilink") (1999年9月23日)             |
|                                       | <small>[丹麥](../Page/丹麥.md "wikilink")[埃斯比約](../Page/埃斯比約.md "wikilink") (2002年8月20日)           |
|                                       | <small>[德国](../Page/德国.md "wikilink")[康斯坦茨](../Page/康斯坦茨.md "wikilink") (2002年10月)             |
|                                       | <small>[新西兰](../Page/新西兰.md "wikilink")[陶波](../Page/陶波.md "wikilink") (2002年10月)               |
|                                       | <small>[巴西](../Page/巴西.md "wikilink")[阿雷格里港](../Page/阿雷格里港.md "wikilink") (2004年6月22日)         |
|                                       | <small>[馬達加斯加](../Page/馬達加斯加.md "wikilink")[塔那那利佛](../Page/塔那那利佛.md "wikilink") (2005年11月28日)  |
|   〖[金閶區](../Page/金閶區.md "wikilink")〗- | <small>[馬爾他](../Page/馬爾他.md "wikilink")[桑塔露西亞](../Page/桑塔露西亞.md "wikilink") (2001年11月9日)       |
|   〖[吳中區](../Page/吳中區.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[里薩](../Page/里薩.md "wikilink") (1999年8月16日)               |
|                                       | <small>[新西蘭](../Page/新西蘭.md "wikilink")[羅托魯瓦](../Page/羅托魯瓦.md "wikilink") (2000年2月18日)         |
|   〖[相城區](../Page/相城區.md "wikilink")〗- | <small>[韓國](../Page/韓國.md "wikilink")[榮州](../Page/榮州.md "wikilink") (1998年4月23日)               |
|   〖[宿遷市](../Page/宿遷市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[比林蒂西](../Page/比林蒂西.md "wikilink") (2001年10月25日)        |
|                                       | <small>[日本](../Page/日本.md "wikilink")[加世田](../Page/加世田.md "wikilink") (2002年8月20日)             |
|   〖[太倉市](../Page/太倉市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[羅索裏納](../Page/羅索裏納.md "wikilink") (2000年2月23日)         |
|   〖[泰興市](../Page/泰興市.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[阿卡迪亞](../Page/阿卡迪亞.md "wikilink") (1997年11月4日)           |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[布洛肯希爾](../Page/布洛肯希爾.md "wikilink") (1998年8月14日)     |
|   〖[泰州市](../Page/泰州市.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[紐波特紐斯](../Page/紐波特紐斯.md "wikilink") (1998年11月4日)         |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[拉托羅布](../Page/拉托羅布.md "wikilink") (2000年4月7日)        |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[陰城郡](../Page/陰城郡.md "wikilink") (2000年9月27日)             |
|                                       | <small>[芬蘭](../Page/芬蘭.md "wikilink")[科特卡](../Page/科特卡.md "wikilink") (2001年9月19日)             |
|   〖[通州市](../Page/通州市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[羽咋市](../Page/羽咋市.md "wikilink") (2001年5月22日)             |
|   〖[無錫市](../Page/無錫市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[明石市](../Page/明石市.md "wikilink") (1981年8月29日)             |
|                                       | <small>[美國](../Page/美國.md "wikilink")[查塔努加](../Page/查塔努加.md "wikilink") (1982年10月12日)          |
|                                       | <small>[日本](../Page/日本.md "wikilink")[相模原市](../Page/相模原市.md "wikilink") (1985年10月6日)           |
|                                       | <small>[新西蘭](../Page/新西蘭.md "wikilink")[哈密爾頓](../Page/哈密爾頓.md "wikilink") (1986年7月5日)          |
|                                       | <small>[葡萄牙](../Page/葡萄牙.md "wikilink")[卡斯凱什](../Page/卡斯凱什.md "wikilink") (1993年9月14日)         |
|                                       | <small>[加拿大](../Page/加拿大.md "wikilink")[斯卡伯勒](../Page/斯卡伯勒.md "wikilink") (1996年4月10日)         |
|   〖[錫山區](../Page/錫山區.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[奧爾德姆](../Page/奧爾德姆.md "wikilink") (1999年9月7日)            |
|   〖[吳江市](../Page/吳江市.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[布林昆.雅里昂](../Page/布林昆.雅里昂.md "wikilink") (1993年10月7日)     |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[達博](../Page/達博.md "wikilink") (1995年6月7日)            |
|                                       | <small>[日本](../Page/日本.md "wikilink")[千葉](../Page/千葉.md "wikilink") (1996年10月10日)              |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[華城郡](../Page/華城郡.md "wikilink") (2000年9月27日)             |
|   〖[新沂市](../Page/新沂市.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[特拉伯爾](../Page/特拉伯爾.md "wikilink") (1996年5月9日)            |
|                                       | <small>[烏克蘭](../Page/烏克蘭.md "wikilink")[亞歷山德里亞](../Page/亞歷山德里亞.md "wikilink") (1998年12月22日)    |
|   〖[興化市](../Page/興化市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[浪江町](../Page/浪江町.md "wikilink") (1996年4月17日)             |
|   〖[徐州市](../Page/徐州市.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[聖艾蒂安](../Page/聖艾蒂安.md "wikilink") (1984年3月27日)           |
|                                       | <small>[美國](../Page/美國.md "wikilink")[紐華克](../Page/紐華克.md "wikilink") (1993年4月21日)             |
|                                       | <small>[日本](../Page/日本.md "wikilink")[半田](../Page/半田.md "wikilink") (1993年5月27日)               |
|                                       | <small>[奧地利](../Page/奧地利.md "wikilink")[萊奧本](../Page/萊奧本.md "wikilink") (1994年8月29日)           |
|                                       | <small>[德國](../Page/德國.md "wikilink")[波鴻](../Page/波鴻.md "wikilink") (1994年9月16日)               |
|                                       | <small>[烏克蘭](../Page/烏克蘭.md "wikilink")[基洛沃格勒](../Page/基洛沃格勒州.md "wikilink") (1996年8月31日)      |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[丹德農](../Page/丹德農.md "wikilink") (1996年9月12日)         |
|                                       | <small>[俄羅斯](../Page/俄羅斯.md "wikilink")[梁贊](../Page/梁贊.md "wikilink") (1998年5月13日)             |
|                                       | <small>[巴西](../Page/巴西.md "wikilink")[奧薩斯庫](../Page/奧薩斯庫.md "wikilink") (1999年5月11日)           |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[井邑市](../Page/井邑市.md "wikilink") (2000年9月27日)             |
|                                       | <small>[德國](../Page/德國.md "wikilink")[埃爾福特](../Page/埃爾福特.md "wikilink") (2005年11月26日)          |
|   〖[鹽城市](../Page/鹽城市.md "wikilink")〗- | <small>[義大利](../Page/義大利.md "wikilink")[基耶蒂](../Page/基耶蒂.md "wikilink") (1992年10月1日)           |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[南原](../Page/南原.md "wikilink") (1998年8月31日)               |
|                                       | <small>[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[德瓦](../Page/德瓦.md "wikilink") (1999年1月18日)           |
|                                       | <small>[日本](../Page/日本.md "wikilink")[鹿島市](../Page/鹿島市.md "wikilink") (2002年11月8日)             |
|                                       | <small>[美國](../Page/美國.md "wikilink")[聖達戈郡](../Page/聖達戈郡.md "wikilink") (2003年10月20日)          |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[博沃德](../Page/博沃德.md "wikilink") (2005年5月13日)         |
|   〖[揚州市](../Page/揚州市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[唐津市](../Page/唐津市_\(日本\).md "wikilink") (1982年2月22日)      |
|                                       | <small>[日本](../Page/日本.md "wikilink")[厚木市](../Page/厚木市.md "wikilink") (1984年10月23日)            |
|                                       | <small>[美國](../Page/美國.md "wikilink")[肯特](../Page/肯特.md "wikilink") (1994年4月9日)                |
|                                       | <small>[美國](../Page/美國.md "wikilink") (1995年6月5日)                                              |
|                                       | <small>[德國](../Page/德國.md "wikilink")[奧芬巴赫](../Page/美因河畔奥芬巴赫.md "wikilink") (1997年5月6日)        |
|                                       | <small>[緬甸](../Page/緬甸.md "wikilink")[仰光](../Page/仰光.md "wikilink") (1997年7月8日)                |
|                                       | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[大綠三角州地區](../Page/大綠三角州地區.md "wikilink") (1997年10月8日) |
|                                       | <small>[義大利](../Page/義大利.md "wikilink")[裏米尼](../Page/裏米尼.md "wikilink") (1999年3月16日)           |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[龍仁](../Page/龍仁.md "wikilink") (2000年5月10日)               |
|   〖[宜興市](../Page/宜興市.md "wikilink")〗- | <small>[斯洛文尼亞](../Page/斯洛文尼亞.md "wikilink")[新梅斯托](../Page/新梅斯托.md "wikilink") (1995年11月9日)     |
| 〖[張家港市](../Page/張家港市.md "wikilink")〗- | <small>[澳大利亞](../Page/澳大利亞.md "wikilink")[波特蘭](../Page/波特蘭.md "wikilink") (1995年8月8日)          |
|                                       | <small>[日本](../Page/日本.md "wikilink")[丸龜市](../Page/丸龜市.md "wikilink") (1999年5月28日)             |
|                                       | <small>[俄羅斯](../Page/俄羅斯.md "wikilink")[維亞基馬](../Page/維亞基馬.md "wikilink") (2004年10月11日)        |
|   〖[鎮江市](../Page/鎮江市.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[津](../Page/津.md "wikilink") (1984年6月11日)                 |
|                                       | <small>[美國](../Page/美國.md "wikilink")[坦佩](../Page/坦佩.md "wikilink") (1989年3月6日)                |
|                                       | <small>[加拿大](../Page/加拿大.md "wikilink")[拉克梅岡蒂克](../Page/拉克梅岡蒂克.md "wikilink") (1995年10月8日)     |
|                                       | <small>[土耳其](../Page/土耳其.md "wikilink")[伊茲米特](../Page/伊茲米特.md "wikilink") (1996年11月14日)        |
|                                       | <small>[巴西](../Page/巴西.md "wikilink")[隆德里納](../Page/隆德里納.md "wikilink") (1997年6月4日)            |
|                                       | <small>[日本](../Page/日本.md "wikilink")[倉敷](../Page/倉敷.md "wikilink") (1997年11月18日)              |
|                                       | <small>[韓國](../Page/韓國.md "wikilink")[益山市](../Page/益山市.md "wikilink") (1998年5月17日)             |
|                                       | <small>[德國](../Page/德國.md "wikilink")[曼海姆](../Page/曼海姆.md "wikilink") (2004年3月1日)              |

## 参考资料

<references/>

[Category:江苏列表](../Category/江苏列表.md "wikilink")
[Category:中国姐妹城市列表](../Category/中国姐妹城市列表.md "wikilink")
[Category:江蘇外事](../Category/江蘇外事.md "wikilink")

1.  <http://news.sina.com.cn/z/njnagoya/> 南京暂停与名古屋官方交往-新浪新闻中心