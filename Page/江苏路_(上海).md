**江苏路**是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[长宁区东部的一条南北向干道](../Page/长宁区.md "wikilink")，在1943年以前名为[忆定盘路](../Page/忆定盘路.md "wikilink")（Edinburgh
Road），属于[上海公共租界在沪西的](../Page/上海公共租界.md "wikilink")[越界筑路](../Page/上海公共租界越界筑路.md "wikilink")。

## 开辟

1906年，工部局越界填浜筑成这条南北向马路，北起[白利南路](../Page/白利南路.md "wikilink")，南至[海格路](../Page/海格路.md "wikilink")。全长1649米。路名得名于苏格兰首府、历史文化名城[爱丁堡](../Page/爱丁堡.md "wikilink")。\[1\]

## 沿路近代建筑

此路开辟后，沿路陆续建成许多[西班牙式庭院住宅或花园式别墅](../Page/西班牙.md "wikilink")，成为沪西住宅区的组成部分之一。其中列入[上海市优秀历史建筑名单的有](../Page/上海市优秀历史建筑.md "wikilink")：

  - 江苏路46-78弄：[中一村](../Page/中一村.md "wikilink")，第四批上海市优秀历史建筑
  - 江苏路91号：[中西女中](../Page/中西女中.md "wikilink")，第二批上海市优秀历史建筑
  - 江苏路162弄3号：[上海海关税务司住宅](../Page/上海海关税务司住宅.md "wikilink")，第四批上海市优秀历史建筑
  - 江苏路284弄3号、5号、7号、9号、10号、11号、12号、14号、15号、16号、17号、18号、19号、27号：[安定坊](../Page/安定坊.md "wikilink")，第四批上海市优秀历史建筑
  - 江苏路480弄：[月村](../Page/月村.md "wikilink")，第四批上海市优秀历史建筑
  - 江苏路495弄：[忆定村](../Page/忆定村.md "wikilink")，第四批上海市优秀历史建筑
  - 江苏路796号：[麦加利银行高级职员住宅](../Page/麦加利银行高级职员住宅.md "wikilink")，第四批上海市优秀历史建筑

## 交汇道路(由北向南)

  - [长宁路](../Page/长宁路.md "wikilink")
  - [武定西路](../Page/武定西路.md "wikilink")
  - [愚园路](../Page/愚园路.md "wikilink")
  - [宣化路](../Page/宣化路.md "wikilink")、[东诸安浜路](../Page/东诸安浜路.md "wikilink")
  - [安化路](../Page/安化路.md "wikilink")
  - [利西路](../Page/利西路.md "wikilink")
  - [延安西路](../Page/延安西路_\(上海\).md "wikilink")
  - [昭化东路](../Page/昭化东路.md "wikilink")
  - [华山路](../Page/华山路.md "wikilink")

## 参考资料

<references/>

[Category:长宁区](../Category/长宁区.md "wikilink")
[J](../Category/上海道路.md "wikilink")

1.  [专业志 \>\> 上海市政工程志 \>\> 第一篇市区道路 \>\> 第二章主要干道 \>\>
    第二节　南北向主要干道](http://shtong.gov.cn/newsite/node2/node2245/node68289/node68294/node68312/node68325/userobject1ai65726.html)