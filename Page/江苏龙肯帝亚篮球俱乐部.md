**江苏龙肯帝亚篮球俱乐部**（简称**江苏龙**，或称赞助商名字**江苏肯帝亚**；由于冠名的关系，这支队伍曾被称作**江苏大业**，**江苏大华**，**江苏中天钢铁**，**江苏德玛斯特**，**江苏南钢**）是[中国篮球职业联赛的一支球队](../Page/中国篮球职业联赛.md "wikilink")。球队主场设于[江苏省](../Page/江苏省.md "wikilink")[镇江市和](../Page/镇江市.md "wikilink")[南京市](../Page/南京市.md "wikilink")，自1995年建队起在中国篮球职业联赛中排名起起落落，发挥不稳，有媒体称之为最容易爆冷门的球队。

球队拥有多名中国国家队球员，已退役的[胡卫东和](../Page/胡卫东.md "wikilink")[张成都是出自江苏队](../Page/张成.md "wikilink")，现在的江苏龙队的前锋[易立是球队的当家球员](../Page/易立.md "wikilink")，其在联赛中有很强的统治力。另外球队中的后卫[胡雪峰也是非常出色的球员拥有较强的个人能力和组织意识](../Page/胡雪峰.md "wikilink")，多次获得联赛的助攻王和抢断王，也是联赛历史上抢断总数的纪录保持者，易立则是2005-2006赛季联赛的的最佳新秀。

江苏龙曾于2004-2005赛季闯入CBA总决赛，但在最后一场负于[广东宏远](../Page/广东宏远.md "wikilink")，获得亚军。

## 球队阵容

### 管理层

  - 总经理：[史琳杰](../Page/史琳杰.md "wikilink")

<!-- end list -->

  - 副总经理、领队：[王继华](../Page/王继华.md "wikilink")

<!-- end list -->

  - 主教练：[胡雪峰](../Page/胡雪峰.md "wikilink")

<!-- end list -->

  - 执行主教练：[梅米·贝西诺维奇](../Page/梅米·贝西诺维奇.md "wikilink")

<!-- end list -->

  - 助理教练：[李长山](../Page/李长山.md "wikilink")、[胡卫东](../Page/胡卫东.md "wikilink")

### 现役球员

更新于2016年1月27日

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 曾效力球员

  - [胡卫东](../Page/胡卫东.md "wikilink")（1985-2005）

  - [胡雪峰](../Page/胡雪峰.md "wikilink")（1999-）

  - [克里斯·安德森](../Page/克里斯·安德森.md "wikilink")（1999-2000）

  - [克里斯·赫伦](../Page/克里斯·赫伦.md "wikilink")（2003-2004）

  - [易立](../Page/易立.md "wikilink")（2004-）

  - [托尼·道格拉斯](../Page/托尼·道格拉斯.md "wikilink")（2014-2015）

  - [羅意庭](../Page/羅意庭.md "wikilink") (2014-2015)

  - [潘宁](../Page/潘宁.md "wikilink")（2015-）

  - [马尚·布鲁克斯](../Page/马尚·布鲁克斯.md "wikilink")（2015-）

  - [格雷格·奥登](../Page/格雷格·奥登.md "wikilink")（2015-）

  - [陈磊](../Page/陈磊_\(篮球运动员\).md "wikilink")（2015-2018）

## 历年教练

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 历年成绩

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p>2018/19赛季</p></td>
<td><center>
<p>第9名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2017/18赛季</p></td>
<td><center>
<p>第5名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2016/17赛季</p></td>
<td><center>
<p>第10名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2015/16赛季</p></td>
<td><center>
<p>第11名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2014/15赛季</p></td>
<td><center>
<p>第14名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2013/14赛季</p></td>
<td><center>
<p>第11名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2012/13赛季</p></td>
<td><center>
<p>第12名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2011/12赛季</p></td>
<td><center>
<p>第17名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2010/11赛季</p></td>
<td><center>
<p>第4名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2009/10赛季</p></td>
<td><center>
<p>第6名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2008/09赛季</p></td>
<td><center>
<p>季军</p></td>
</tr>
<tr class="even">
<td><center>
<p>2007/08赛季</p></td>
<td><center>
<p>季军</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2006/07赛季</p></td>
<td><center>
<p>季军</p></td>
</tr>
<tr class="even">
<td><center>
<p>2005/06赛季</p></td>
<td><center>
<p>第4名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2004/05赛季</p></td>
<td><center>
<p>亚军</p></td>
</tr>
<tr class="even">
<td><center>
<p>2003/04赛季</p></td>
<td><center>
<p>季军</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2002/03赛季</p></td>
<td><center>
<p>第9名</p></td>
</tr>
<tr class="even">
<td><center>
<p>2001/02赛季</p></td>
<td><center>
<p>第9名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1999/00赛季</p></td>
<td><center>
<p>第8名</p></td>
</tr>
<tr class="even">
<td><center>
<p>1998/99赛季</p></td>
<td><center>
<p>第10名</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1997/98赛季</p></td>
<td><center>
<p>第7名</p></td>
</tr>
<tr class="even">
<td><center>
<p>1996/97赛季</p></td>
<td><center>
<p>第7名</p></td>
</tr>
</tbody>
</table>

## 外部链接

  -
[Category:中国篮球职业联赛球队](../Category/中国篮球职业联赛球队.md "wikilink")
[Category:江苏篮球俱乐部](../Category/江苏篮球俱乐部.md "wikilink")
[Category:南京体育组织](../Category/南京体育组织.md "wikilink")
[Category:镇江组织](../Category/镇江组织.md "wikilink")
[Category:1995年建立的體育俱樂部](../Category/1995年建立的體育俱樂部.md "wikilink")