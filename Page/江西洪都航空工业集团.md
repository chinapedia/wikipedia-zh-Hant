[缩略图](https://zh.wikipedia.org/wiki/File:Nanchang_CJ-6A_Airplane_over_California_Coastline_N4183E_20110219.jpg "fig:缩略图")\]\]
**江西洪都航空工业集团有限责任公司**（Jiangxi Hongdu Aviation Industry
Group，简称**中航工业洪都**）创建于1951年，是[中国航空工业集团所属的特大型企业](../Page/中国航空工业集团.md "wikilink")，原名为南昌飞机制造公司，前身为国营洪都机械厂。\[1\]

1954年至1958年，制造了379架[初教五](../Page/初教五.md "wikilink")。此后生产[初教六](../Page/初教六.md "wikilink")。1957年至1968年，生产了727架[运-5](../Page/运-5.md "wikilink")，此后运五由[石家庄飞机制造厂生产](../Page/石家庄飞机制造厂.md "wikilink")。1968年至2012年，生产[强五](../Page/强五.md "wikilink")。1990年代与[巴基斯坦合作研制](../Page/巴基斯坦.md "wikilink")[K8型教练机](../Page/K8型教练机.md "wikilink")，出口到多个国家。

南昌飞机制造厂还生产[农-5农林飞机](../Page/农-5.md "wikilink")、[L-15猎鹰高级](../Page/L-15.md "wikilink")[教练机等飞机](../Page/教练机.md "wikilink")。

2005年12月27日，[美国国务院指责该公司向](../Page/美国国务院.md "wikilink")[伊朗提供武器和技术](../Page/伊朗.md "wikilink")，并对该公司进行[经济制裁](../Page/经济制裁.md "wikilink")。

## 参考资料

<references />

[Category:中国航空工业集团](../Category/中国航空工业集团.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:南昌公司](../Category/南昌公司.md "wikilink")
[Category:江西工业](../Category/江西工业.md "wikilink")
[Category:1951年成立的公司](../Category/1951年成立的公司.md "wikilink")
[Category:1934年中國建立](../Category/1934年中國建立.md "wikilink")

1.