**江部乙町**（）是過去位於[北海道](../Page/北海道.md "wikilink")[空知支廳的一個](../Page/空知支廳.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。已於1971年4月1日與[瀧川市](../Page/瀧川市.md "wikilink")[合併為新設置的](../Page/市町村合併.md "wikilink")[瀧川市](../Page/瀧川市.md "wikilink")，過去的轄區位於現在瀧川市的北半部區域。

## 歷史

  - 1909年4月1日：江部乙村從瀧川村（後來的瀧川市）分割獨立設置為北海道二級村。\[1\]
  - 1915年4月1日：改為北海道一級村。
  - 1952年5月5日：改制為江部乙町。
  - 1971年4月1日：與瀧川市合併。

## 參考資料

<div class="references-small">

<references />

[Category:瀧川市](../Category/瀧川市.md "wikilink")
[Category:空知管內](../Category/空知管內.md "wikilink")
[Category:日本已廢除的町](../Category/日本已廢除的町.md "wikilink")

1.