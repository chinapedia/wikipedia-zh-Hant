**江铸久**（），[中国著名](../Page/中国.md "wikilink")[围棋棋手](../Page/围棋.md "wikilink")，祖籍[山东](../Page/山东.md "wikilink")[济宁](../Page/济宁.md "wikilink")，出生于[山西](../Page/山西.md "wikilink")[太原](../Page/太原.md "wikilink")，现与妻子[芮迺伟同为](../Page/芮迺伟.md "wikilink")[韩国棋院客座九段](../Page/韩国棋院.md "wikilink")。

## 生平

江出生于一个围棋世家，6岁即开始学习围棋，其大哥[江鸣久为围棋职业七段高手](../Page/江鸣久.md "wikilink")，姐姐江声久也有业余五段水平，在山西大学物理系教书，同时也开办围棋课。弟弟江玉久棋艺也不错。1982年，江被[中国围棋协会定为五段](../Page/中国围棋协会.md "wikilink")。1984年，江在第一届[中日围棋擂台赛上连胜](../Page/中日围棋擂台赛.md "wikilink")[日本五位棋手](../Page/日本.md "wikilink")，被破格晋升为八段，1987年晋升为九段。江还曾经取得过1989年[全国围棋个人赛亚军等成绩](../Page/全国围棋个人赛.md "wikilink")。

1990年，江的恋人[芮迺伟因为三峽事件被迫离开中国赴日本拜入](../Page/芮迺伟.md "wikilink")[吴清源门下](../Page/吴清源.md "wikilink")，8月，江铸久因为赴日本的请求被中国围棋协会拒绝，转而以私人身份赴[美国留学](../Page/美国.md "wikilink")，其后与芮迺伟团聚于美国，两人于1992年结婚。

1998年，夫妻協助[埃爾溫·伯利坎普研究量化官子](../Page/埃爾溫·伯利坎普.md "wikilink")，參與[數學卡片棋比賽](../Page/數學卡片棋.md "wikilink")\[1\]。

1999年，江芮二人被韩国棋院接纳为客座棋手至今。

## 註解

## 参考资料

  - [新浪体育·江铸久](http://sports.sina.com.cn/star/jiang_zhujiu/)

[J江](../Category/中国围棋棋手.md "wikilink")
[J江](../Category/韩国围棋棋手.md "wikilink")
[J江](../Category/太原人.md "wikilink")
[Z铸](../Category/江姓.md "wikilink")

1.  [江铸久：数学天才Elwyn博士他的数学卡片棋](http://qipai.people.com.cn/GB/47625/13033967.html)