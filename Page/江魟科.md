**江魟科**是[燕魟目的一](../Page/燕魟目.md "wikilink")[科](../Page/科.md "wikilink")，和[魟科鱼非常相似](../Page/魟科.md "wikilink")，但本科[鱼只生活在淡水中](../Page/鱼.md "wikilink")，没有在海中的种类，主要分布在[南美洲](../Page/南美洲.md "wikilink")，尤其是[亚马逊河流域](../Page/亚马逊河.md "wikilink")，在体盘中部有一个长、薄、尖的突起，尾部一般也有毒刺，人被蛰后感到剧痛，非常容易感染。本科鱼最大的可达1米宽。

## 分類

由於江魟科物種的分類比較複雜，到現在還有[未被命名的物種](../Page/二名法.md "wikilink")。現時本科之下分為4個屬，各物種如下：

  - (*Heliotrygon*)
      - (''Heliotrygon gomesi '')
      - (''Heliotrygon rosai '')
  - [副江魟屬](../Page/副江魟屬.md "wikilink")(*Paratrygon*)
      - [巴西副江魟](../Page/巴西副江魟.md "wikilink")(*Paratrygon ajereba*)
  - [近江魟屬](../Page/近江魟屬.md "wikilink")(*Plesiotrygon*)
      - [近江魟](../Page/近江魟.md "wikilink")(*Plesiotrygon iwamae*)
      - (*Plesiotrygon nana*)
  - [江魟屬](../Page/江魟屬.md "wikilink")(*Potamotrygon*)
      - (*Potamotrygon amandae*)
      - (*Potamotrygon boesemani*)
      - (*Potamotrygon brachyura*)
      - (*Potamotrygon constellata*)
      - [卡氏江魟](../Page/卡氏江魟.md "wikilink")(*Potamotrygon falkneri*)
      - (*Potamotrygon henlei*)
      - (*Potamotrygon humerosa*)
      - [暗纹江魟](../Page/暗纹江魟.md "wikilink")(*Potamotrygon hystrix*)
      - (*Potamotrygon leopoldi*)
      - [利馬江魟](../Page/利馬江魟.md "wikilink")(*Potamotrygon limai*)
      - [馬氏江魟](../Page/馬氏江魟.md "wikilink")(*Potamotrygon magdalenae*)
      - (*Potamotrygon marinae*)
      - [南美江魟](../Page/南美江魟.md "wikilink")(*Potamotrygon motoro*)
      - (*Potamotrygon ocellata*)
      - [奧氏江魟](../Page/奧氏江魟.md "wikilink")(*Potamotrygon orbignyi*)
      - (*Potamotrygon pantanensis*)
      - (*Potamotrygon schroederi*)
      - (*Potamotrygon schuhmacheri*)
      - (*Potamotrygon scobina*)
      - (*Potamotrygon signata*)
      - (*Potamotrygon tatianae*)
      - (*Potamotrygon tigrina*)
      - [耶氏江魟](../Page/耶氏江魟.md "wikilink")(*Potamotrygon yepezi*)

## 外部链接

  - [江魟科介绍和照片](http://www.peterah.demon.co.uk/potam.htm)

[\*](../Category/江魟科.md "wikilink")