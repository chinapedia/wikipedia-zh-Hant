**池上飯包**，又稱**池上便當**，是一款源自臺灣[臺東縣](../Page/臺東縣.md "wikilink")[池上鄉的](../Page/池上鄉.md "wikilink")[鐵路便當](../Page/鐵路便當.md "wikilink")，並以使用產自該地區的[稻米及以](../Page/稻米.md "wikilink")[木](../Page/木板.md "wikilink")[盒裝盛而聞名](../Page/盒.md "wikilink")\[1\]。
[31274819654_65e18d3408_yuann.tw.jpg](https://zh.wikipedia.org/wiki/File:31274819654_65e18d3408_yuann.tw.jpg "fig:31274819654_65e18d3408_yuann.tw.jpg")

## 歷史

1930年代初期，各地[移民進入池上地區](../Page/移民.md "wikilink")[開墾](../Page/開墾.md "wikilink")；池上飯包創始者李約典、林來富[夫婦也自臺北](../Page/夫婦.md "wikilink")[三重埔遷徙至](../Page/三重區.md "wikilink")[池上火車站前定居](../Page/池上車站_\(臺灣\).md "wikilink")。\[2\]

1940年，李約典、林來富開始在池上車站[月台販賣](../Page/月台.md "wikilink")[番薯餅](../Page/番薯餅.md "wikilink")；[戰後](../Page/臺灣戰後時期.md "wikilink")，夫婦二人改賣以[月桃葉包裹之](../Page/月桃.md "wikilink")[飯糰](../Page/飯糰.md "wikilink")，這是池上飯包的第一代版本。\[3\]

後來，由於李氏夫婦之[子李丁保在車站任職](../Page/兒子.md "wikilink")，夫婦二人便將飯糰攜至[列車上販賣](../Page/鐵路列車.md "wikilink")。在李氏夫婦把事業交與[兒媳李陳雲之後](../Page/兒媳.md "wikilink")，李陳雲考慮到當時[列車行駛時間甚長等因素](../Page/列車.md "wikilink")，而決定將飯糰改為[飯包](../Page/飯包.md "wikilink")。\[4\]

當時，池上飯包外以月桃葉包裝，內有[白米飯](../Page/米飯.md "wikilink")、[滷肉](../Page/滷肉.md "wikilink")、黃[菜頭](../Page/菜頭.md "wikilink")（染色[醃漬白蘿蔔](../Page/醃漬.md "wikilink")）、烤[肉乾](../Page/肉乾.md "wikilink")、[豬肝](../Page/豬肝.md "wikilink")、瘦肉片，再加上一小塊[蛋餅以及小](../Page/蛋餅.md "wikilink")[蝦和](../Page/蝦.md "wikilink")[麵粉油炸製成的](../Page/麵粉.md "wikilink")[炸蝦餅與](../Page/炸蝦餅.md "wikilink")[酸梅](../Page/酸梅.md "wikilink")，售價則為[新臺幣](../Page/新臺幣.md "wikilink")1.5元。\[5\]

1962年起，池上飯包再度改以[木片製成的盒子裝盛](../Page/木.md "wikilink")，成為今日所見之池上飯包。\[6\]

此後，當地紛紛出現多家業者競相製作販賣飯包，後來更逐漸擴散至其他地區。但是，在大眾鐵路運輸搭乘人次逐漸減少之下，飯包事業規模隨之減縮。\[7\]

1999年，池上飯包老店導入經營團隊，並重新命名為「悟饕池上飯包」，更推動全台連鎖[加盟](../Page/加盟.md "wikilink")，又將其拓展至世界各地，以[家鄉口味作其號招](../Page/家鄉.md "wikilink")。\[8\]

2014年，郭怡文、吳侄璉及張育齊在[紐約華埠開設](../Page/纽约华埠.md "wikilink")「台灣熊屋」販售池上飯包\[9\]；他們聘請臺灣[便當](../Page/便當.md "wikilink")[連鎖企業傳授經營](../Page/連鎖店.md "wikilink")[技術](../Page/技術.md "wikilink")，再依當地[口味與](../Page/口味.md "wikilink")[法規調整](../Page/法規.md "wikilink")。\[10\]\[11\]

## 特色

傳統池上飯包有三大特點：使用池上米、用木盒裝盛、乾式飯包。\[12\]

## 食材

### 米飯

許多店家使用之米飯產地皆不一定是池上地區；部分來自[花東地區](../Page/花東縱谷.md "wikilink")，其他則取自各[稻米產地](../Page/稻米.md "wikilink")。但是，池上地區所生產之稻米因池上飯包而更加廣為人知，池上鄉也因此打響[觀光名氣](../Page/觀光.md "wikilink")。\[13\]

## 代表商號

  - 全美行\[14\]
  - 悟饕池上飯包：一家由總部位在宜蘭[五結的](../Page/五結鄉.md "wikilink")[連鎖餐飲企業](../Page/連鎖店.md "wikilink")\[15\]

## 参考文献

<div class="references-small">

<references />

</div>

## 延伸閱讀

  - [月台與池上便當──家鄉的第一道與最後一道風景](http://opinion.udn.com/opinion/story/6785/2263041)
  - [微笑台灣：翻轉池上便當，方寸裡挖掘新可能](https://smiletaiwan.cw.com.tw/article/627)

[Category:台灣米飯類食品](../Category/台灣米飯類食品.md "wikilink")
[Category:台灣傳統食品](../Category/台灣傳統食品.md "wikilink")
[Category:池上鄉](../Category/池上鄉.md "wikilink")
[Category:台灣鐵路便當](../Category/台灣鐵路便當.md "wikilink")
[Category:臺東縣文化](../Category/臺東縣文化.md "wikilink")

1.

2.
3.
4.
5.
6.
7.

8.
9.

10.

11.

12.
13.
14.

15.