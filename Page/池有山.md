**池有山**為[台灣知名山峰](../Page/台灣.md "wikilink")，也是[台灣百岳之一](../Page/台灣百岳.md "wikilink")，排名第52。池有山高3,303公尺，山頂有三等[三角點](../Page/三角點.md "wikilink")，屬於[雪山山脈](../Page/雪山山脈.md "wikilink")，[武陵四秀之一](../Page/武陵四秀.md "wikilink")，是[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉與](../Page/尖石鄉.md "wikilink")[台中市](../Page/台中市.md "wikilink")[和平區的分界嶺](../Page/和平區_\(台灣\).md "wikilink")；因山嶺西部草原有新達池、亞美池與品田池等數座高山池塘，故名。其泰雅語稱呼為「**Tamarappu**」，曾音譯為｢塔馬拉普｣山；日文漢字轉譯為**玉羅府山**\[1\]\[2\]。池有山西方為[品田山](../Page/品田山.md "wikilink")，東側則接[桃山](../Page/桃山_\(臺灣\).md "wikilink")、[詩崙山與](../Page/詩崙山.md "wikilink")[喀拉業山](../Page/喀拉業山.md "wikilink")。

## 攀登路線

要攀登池有山，可由[武陵農場往桃山瀑布的步道上的岔路可通往三岔營地](../Page/武陵農場.md "wikilink")，由此處登池有山最近，也是攀登武陵四秀的方法之一

## 參考資料

<div class="references-small">

<references />

</div>

  - 楊建夫，《台灣的山脈》，2001年，臺北，遠足文化公司

## 相關條目

  - [雪霸國家公園](../Page/雪霸國家公園.md "wikilink")
  - [台灣百岳](../Page/台灣百岳.md "wikilink")
  - [聖稜線](../Page/聖稜線.md "wikilink")
  - [登山](../Page/登山.md "wikilink")

## 外部連結

  - [大霸/聖稜線段](http://trail.forest.gov.tw/NationalTrailSystem/TR_G_01.htm)
    - 國家步道系統
  - [中華民國山岳協會](https://web.archive.org/web/20070414235108/http://www.mountaineering.org.tw/index.asp)
    - 最初為「台灣山岳會」
  - [雪霸聖稜線](https://web.archive.org/web/20070927193452/http://www.beautymountain88.com.tw/snownmountainj/syuebanationparkj1-2.htm)
  - [聖稜線](http://www.ntut.edu.tw/~s8370037/%B3%B7-%B8t%B3%AE.htm) -
    [聖稜線步道稜脈圖](http://www.ntut.edu.tw/~s8370037/images/%B8t%B3%AEmap-hl.gif)
  - [聖稜線](https://web.archive.org/web/20070926212836/http://csm01.csu.edu.tw/0150/49002134/index-12.htm)
    - 連峰照片
  - [雪山山脈的聖稜、南稜與西南稜線](https://web.archive.org/web/20070926212842/http://csm01.csu.edu.tw/0150/49002134/index-11-1.htm)
    - 連峰照片
  - [山情點滴─山與人的故事](http://www.tces.chc.edu.tw/alan/taiwan/g/g1/g16.htm)

[Category:新竹縣山峰](../Category/新竹縣山峰.md "wikilink")
[Category:台中市山峰](../Category/台中市山峰.md "wikilink")
[Category:台灣百岳](../Category/台灣百岳.md "wikilink")
[Category:梨山風景區](../Category/梨山風景區.md "wikilink")
[Category:雪霸國家公園](../Category/雪霸國家公園.md "wikilink")
[Category:尖石鄉](../Category/尖石鄉.md "wikilink") [Category:和平區
(臺灣)](../Category/和平區_\(臺灣\).md "wikilink")

1.  第二世代台灣百岳全集上冊，戶外生活圖書股份有限公司，陳遠見主編，2007年8月31日初版
2.  台灣百岳全集，連鋒宗編著，上河文化發行，2007年6月初版