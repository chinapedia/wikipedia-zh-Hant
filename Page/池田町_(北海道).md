**池田町**（）位於[北海道](../Page/北海道.md "wikilink")[十勝綜合振興局中部](../Page/十勝綜合振興局.md "wikilink")，以葡萄酒之町聞名，並設有町營釀酒事業。名稱則來自此地最早成立的「池田農場」。

## 地理

位於十勝平原的東緣，[十勝川位於轄區的西南側](../Page/十勝川.md "wikilink")，為與[幕別町的分界](../Page/幕別町.md "wikilink")，十勝川的支流利別川南北貫穿轄區中央，在與十勝川的會合處為主要市區，明治時代也是個繁榮的河港。

## 歷史

  - 1879年：來自[山梨縣的](../Page/山梨縣.md "wikilink")[武田菊平前來開墾](../Page/武田菊平.md "wikilink")。\[1\]
  - 1896年：成立池田農場和高島農場。
  - 1899年：設置凋寒外13村戶長役場。
  - 1906年4月1日：凋寒村、蝶多村、十弗村、樣舞村、誓牛村、信取村、蓋派村、居邊村[合併成為北海道二級村凋寒村](../Page/市町村合併.md "wikilink")。\[2\]
  - 1913年4月1日：改名為川合村。
  - 1925年4月1日：部份區域被併入士幌村（現在的[士幌町](../Page/士幌町.md "wikilink")）。
  - 1926年7月1日：實施町制，同時改名為池田町。

## 產業

農作以[菜豆](../Page/菜豆.md "wikilink")、[紅豆](../Page/紅豆.md "wikilink")、[甜菜和](../Page/甜菜.md "wikilink")[馬鈴薯為主](../Page/馬鈴薯.md "wikilink")，畜牧以肉牛和奶酪畜牧業為主。此外也栽種[葡萄](../Page/葡萄.md "wikilink")，並進行[釀酒製造](../Page/釀酒.md "wikilink")；町營的釀酒工廠所生產的十勝[葡萄酒為主要名產](../Page/葡萄酒.md "wikilink")。

## 交通

### 機場

  - [帶廣機場](../Page/帶廣機場.md "wikilink")（位於[帶廣市](../Page/帶廣市.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [根室本線](../Page/根室本線.md "wikilink")
        ：[利別車站](../Page/利別車站.md "wikilink") -
        [池田車站](../Page/池田車站_\(北海道\).md "wikilink")
  - 北海道高原鐵路銀河線，現已廢除。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/道東自動車道.md" title="wikilink">道東自動車道</a>：<a href="../Page/池田IC_(北海道).md" title="wikilink">池田交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道242號.md" title="wikilink">國道242號</a></li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道31號音更池田線</li>
<li>北海道道73號帶廣浦幌線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道236號勇足池田線</li>
<li>北海道道237號池田停車場高島線</li>
<li>北海道道496號下居邊高島停車場線</li>
<li>北海道道882號利別牛首別線</li>
<li>北海道道974號東台留真線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

  - [清見丘公園](../Page/清見丘公園.md "wikilink")
  - [千代田堰堤](../Page/千代田堰堤.md "wikilink")
  - [池田葡萄酒城](../Page/池田葡萄酒城.md "wikilink")（）
  - [淺原六朗文學記念館](../Page/淺原六朗文學記念館.md "wikilink")
  - 池田[美夢成真博物館](../Page/美夢成真.md "wikilink")（DCT garden IKEDA）

## 教育

### 高等學校

  - 道立北海道池田高等學校

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>池田町立池田中學校</li>
</ul></td>
<td><ul>
<li>池田町立高島中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>池田町立池田小學校</li>
<li>池田町立高島小學校</li>
</ul></td>
<td><ul>
<li>池田町立利別小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [池田市](../Page/池田市.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")）
  - [池田町](../Page/池田町_\(長野縣\).md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")[北安曇郡](../Page/北安曇郡.md "wikilink")）
  - [池田町](../Page/池田町_\(岐阜縣\).md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")[揖斐郡](../Page/揖斐郡.md "wikilink")）
  - [池田町](../Page/池田町_\(福井縣\).md "wikilink")（[福井縣](../Page/福井縣.md "wikilink")[今立郡](../Page/今立郡.md "wikilink")）
  - [三好市](../Page/三好市_\(德島縣\).md "wikilink")（[德島縣](../Page/德島縣.md "wikilink")）：原[三好郡](../Page/三好郡.md "wikilink")[池田町合併加入](../Page/池田町_\(德島縣\).md "wikilink")
  - [池田町](../Page/池田町_\(香川縣\).md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")[小豆郡](../Page/小豆郡.md "wikilink")）：現已合併為[小豆島町](../Page/小豆島町.md "wikilink")

### 海外

  - [Penticton](http://www.penticton.ca/)（[加拿大](../Page/加拿大.md "wikilink")
    [英屬哥倫比亞](../Page/英屬哥倫比亞.md "wikilink")）

## 本地出身的名人

  - [美濃政市](../Page/美濃政市.md "wikilink")：[政治家](../Page/政治家.md "wikilink")
  - [丸谷金保](../Page/丸谷金保.md "wikilink")：政治家、前池田町長、[參議院](../Page/參議院.md "wikilink")[議員](../Page/議員.md "wikilink")
  - [吉田美和](../Page/吉田美和.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [大和田夏希](../Page/大和田夏希.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [島本和彥](../Page/島本和彥.md "wikilink")：漫畫家
  - [佐藤秀峰](../Page/佐藤秀峰.md "wikilink")：漫畫家
  - [及川佑](../Page/及川佑.md "wikilink")：[競速滑冰選手](../Page/競速滑冰.md "wikilink")
  - [長島圭一郎](../Page/長島圭一郎.md "wikilink")：競速滑冰選手
  - [田中康平](../Page/田中康平.md "wikilink")：[足球選手](../Page/足球.md "wikilink")，日本[鹿島鹿角隊](../Page/鹿島鹿角隊.md "wikilink")

## 參考資料

## 外部連結

[Category:以人名命名的行政区](../Category/以人名命名的行政区.md "wikilink")

1.
2.