**池谷薫**（）是[日本知名的業餘](../Page/日本.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")\[1\]以及[彗星獵者](../Page/彗星.md "wikilink")，生於[靜岡縣](../Page/靜岡縣.md "wikilink")。

## 生平

他於1962年開始尋找彗星，翌年發現首顆新彗星，名為[池谷彗星](../Page/池谷彗星.md "wikilink")（C/1963
A1）\[2\]，其後五年間共發現了五顆，其中以1965年與[關勉共同發現的](../Page/關勉.md "wikilink")[池谷·關彗星](../Page/池谷·關彗星.md "wikilink")(C/1965
S1)最為知名。在1980年代他也發現了兩顆[超新星](../Page/超新星.md "wikilink")。

2002年2月1日，他與來自[中國的](../Page/中國.md "wikilink")[張大慶共同發現了](../Page/張大慶.md "wikilink")[池谷－張彗星](../Page/池谷－張彗星.md "wikilink")\[3\]\[4\]，距對上一顆發現的[池谷-關彗星](../Page/池谷-關彗星.md "wikilink")（C/1967
Y1）的日子相隔了35年。

2005年至今，池谷從事[反射望遠鏡的反射鏡的研磨工作](../Page/反射望遠鏡.md "wikilink")，以及繼續在夜空中尋找彗星。2010年11月3日，池谷薰與[新潟縣](../Page/新潟縣.md "wikilink")[村上茂樹同時發現新彗星](../Page/村上茂樹.md "wikilink")[池谷–村上彗星](../Page/池谷–村上彗星.md "wikilink")，這是他第七次發現新彗星\[5\]。

## 榮譽

[小行星4037是以池谷薫來命名的](../Page/小行星4037.md "wikilink")。

## 參考資料

[分類:彗星發現者](../Page/分類:彗星發現者.md "wikilink")

[Category:日本天文学家](../Category/日本天文学家.md "wikilink")
[Category:業餘天文學家](../Category/業餘天文學家.md "wikilink")
[Category:1943年出生](../Category/1943年出生.md "wikilink")
[Category:靜岡縣出身人物](../Category/靜岡縣出身人物.md "wikilink")

1.
2.
3.
4.
5.  [日本のアマ天文家2名が発見，池谷・村上彗星と命名](http://www.asahi.com/science/update/1105/TKY201011050581.html)，朝日新聞HP
    2010年11月5日より