**汤原县**是[黑龙江省](../Page/黑龙江省.md "wikilink")[佳木斯市下辖的一个县](../Page/佳木斯市.md "wikilink")。1905年设县，汤原县得名于[汤旺河冲积平原](../Page/汤旺河.md "wikilink")\[1\]。县人民政府驻汤原镇。该县目前为国家级贫困县。

## 行政区划

下辖4个镇6个乡。\[2\] 。

## 参考资料

[\*](../Category/汤原县.md "wikilink")
[佳木斯](../Category/黑龙江省县份.md "wikilink")
[黑](../Category/国家级贫困县.md "wikilink")

1.
2.