**汤姓**是[漢族姓氏之一](../Page/漢族姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排第72位，按人口计算排名第101位（2007年数据）\[1\]。

## 起源

汤姓主要出自[子姓](../Page/子姓.md "wikilink")，皆為[商汤的後人](../Page/商汤.md "wikilink")，有几个起源：

1.  [商朝宗室之中某支系以先祖成汤的谥号为氏](../Page/商朝.md "wikilink")，是为河南汤氏。
2.  商亡以后，[周公将](../Page/周公.md "wikilink")[商纣王的庶兄](../Page/商纣王.md "wikilink")[微子启封于](../Page/微子启.md "wikilink")[宋国](../Page/宋国.md "wikilink")，《万姓统谱》记载，[春秋时期宋国有](../Page/春秋时期.md "wikilink")[荡氏](../Page/荡氏.md "wikilink")，是[宋桓公之子](../Page/宋桓公.md "wikilink")[公子荡的后裔](../Page/公子荡.md "wikilink")，后来将“荡”字去掉草字头而成为汤氏；
3.  子姓[宋国再传至](../Page/宋国.md "wikilink")[战国末年](../Page/战国.md "wikilink")[宋康王偃而亡国](../Page/宋康王.md "wikilink")，宋康王有一弟名昌，昌生子隆，逢[秦始皇](../Page/秦始皇.md "wikilink")[焚书坑儒](../Page/焚书坑儒.md "wikilink")，为避祸而改姓汤。
4.  [宋朝时有](../Page/宋朝.md "wikilink")[汤悦](../Page/汤悦.md "wikilink")，本姓[殷氏](../Page/殷氏.md "wikilink")（也是商汤后裔），为避宋宣祖[趙弘殷的名讳而改姓汤氏](../Page/趙弘殷.md "wikilink")。
5.  其它来源有外姓、少数民族改姓，包括[满族](../Page/满族.md "wikilink")、[侗族](../Page/侗族.md "wikilink")、[土族](../Page/土族.md "wikilink")、[蒙古族都有汤姓者](../Page/蒙古族.md "wikilink")。

## 郡望

  - [中山郡](../Page/中山郡.md "wikilink")
  - [范陽郡](../Page/范陽郡.md "wikilink")

## 堂號

  - [中山堂](../Page/中山堂.md "wikilink")
  - [范陽堂](../Page/范陽堂.md "wikilink")
  - [掬星堂](../Page/掬星堂.md "wikilink")
  - [吞星堂](../Page/吞星堂.md "wikilink")
  - [玉茗堂](../Page/玉茗堂.md "wikilink")

## 名人

  - [湯顯祖](../Page/湯顯祖.md "wikilink")：明朝末期戲曲劇作家及文學家
  - [湯和](../Page/湯和.md "wikilink")：明朝開國功臣，軍事家，和朱元璋是好友。1395年卒，獲追封東甌王，諡襄武
  - [湯思退](../Page/湯思退.md "wikilink")：南宋主和派宰相，阻挠[隆兴北伐](../Page/隆兴北伐.md "wikilink")。
  - [湯斌](../Page/湯斌.md "wikilink")：清代[理学名臣](../Page/理学.md "wikilink")，康熙时被皇帝斥责为假[道学](../Page/道学.md "wikilink")，雍正时作为文人的正面典型树立弘扬，从祀[曲阜](../Page/曲阜.md "wikilink")[文庙](../Page/文庙.md "wikilink")。
  - [湯球](../Page/湯球.md "wikilink")：晚清著名学者，[辑佚学家](../Page/辑佚.md "wikilink")。
  - [湯用彤](../Page/湯用彤.md "wikilink")：民国哲学家、教育家、历史学家，擅长宗教史研究，曾任北大副校长。
  - [湯一介](../Page/湯一介.md "wikilink")：汤用彤之子，曾任《[儒藏](../Page/儒藏.md "wikilink")》编纂中心主任、首席专家。
  - [湯元普](../Page/湯元普.md "wikilink")：第十七任黃埔軍校校長，籍貫江蘇邳縣，為該校第29期畢業
  - [湯恩伯](../Page/湯恩伯.md "wikilink")：原名湯克勤，中國國民黨高級將領，蔣中正的四大心腹之一
  - [湯寿潜](../Page/湯寿潜.md "wikilink")：晚清[立宪派的领袖人物](../Page/立宪派.md "wikilink")，因争路权、修铁路而名重一时。
  - [湯化龙](../Page/湯化龙.md "wikilink")：清末民初著名立宪派人物，与[梁启超关系密切](../Page/梁启超.md "wikilink")。后在[加拿大](../Page/加拿大.md "wikilink")[维多利亚市被国民党员王昌刺杀](../Page/维多利亚_\(不列颠哥伦比亚\).md "wikilink")。
  - [湯德章](../Page/湯德章.md "wikilink")：台湾台南人，知名律师。1947年被国民党军杀害。
  - [湯仙虎](../Page/湯仙虎.md "wikilink")：中國羽毛球運動員、教練
  - [湯英伸](../Page/湯英伸.md "wikilink")：台灣嘉義縣阿里山鄉鄒族原住民。就讀嘉義師專期間，於暑期打工因被僱主虐待殺害雇主一家，造成台灣社會震撼。
  - [湯寶如](../Page/湯寶如.md "wikilink")：香港歌手
  - [湯加丽](../Page/湯加丽.md "wikilink")：著名中國女演員、人体艺术模特。
  - [湯唯](../Page/湯唯.md "wikilink")：著名中國女演員、2006年因出演李安導演的電影《色，戒》的女主角王佳芝一舉成名
  - [湯家驊](../Page/湯家驊.md "wikilink")：香港資深大律師、立法會議員
  - [湯曜明](../Page/湯曜明.md "wikilink")：[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[一級上將](../Page/一級上將.md "wikilink")，曾任[陸軍總司令](../Page/中華民國陸軍司令.md "wikilink")、[參謀總長與](../Page/國防部參謀本部.md "wikilink")[國防部部長](../Page/中華民國國防部.md "wikilink")
  - [湯鎮業](../Page/湯鎮業.md "wikilink")：香港前演員、廣東省政協委員
  - [湯志偉](../Page/湯志偉.md "wikilink")：台灣演員
  - [湯毓綺](../Page/湯毓綺.md "wikilink")：藝名**9m88**，台灣女歌手\[2\]。
  - [湯盈盈](../Page/湯盈盈.md "wikilink")：香港女藝員、獲無綫電視頒發「最佳女配角」獎
  - [湯顯明](../Page/湯顯明.md "wikilink")：香港[廉政專員](../Page/廉政專員.md "wikilink")
  - [湯漢](../Page/湯漢.md "wikilink")：天主教香港教區主教
  - [湯于瀚](../Page/汤于翰.md "wikilink")：香港醫學家
  - [湯洛雯](../Page/湯洛雯.md "wikilink")：香港[TVB演員](../Page/TVB.md "wikilink")

## 参考文献

[T湯](../Category/漢字姓氏.md "wikilink") [\*](../Category/湯姓.md "wikilink")

1.  [汤姓](http://baijiaxing.xpcha.com/68047602ce7/) 旧百家姓排名：72 新百家姓排名：101
2.  [實踐大學服裝設計系歌唱比賽 小芭](https://www.youtube.com/watch?v=mNYfkknh8og)