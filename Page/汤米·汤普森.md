**汤米·乔治·汤普森**（**Tommy George
Thompson**，1941年11月19日[威斯康星州](../Page/威斯康星州.md "wikilink")[埃尔罗伊](../Page/埃尔罗伊_\(威斯康星州\).md "wikilink")），[美国政治家](../Page/美国.md "wikilink")，[美国共和党成员](../Page/美国共和党.md "wikilink")，曾任[威斯康星州州长](../Page/威斯康星州州长.md "wikilink")（1987年-2001年）、[美国卫生与公众服务部长](../Page/美国卫生与公众服务部长.md "wikilink")（2001年-2005年）。

[T](../Category/1941年出生.md "wikilink")
[T](../Category/在世人物.md "wikilink")
[T](../Category/美国卫生与公众服务部长.md "wikilink")
[T](../Category/威斯康星州州长.md "wikilink")
[T](../Category/威斯康辛大學麥迪遜分校校友.md "wikilink")