**汪道涵**（），原名**汪导淮**，[安徽省](../Page/安徽省.md "wikilink")[盱眙县](../Page/盱眙县.md "wikilink")[嘉山人](../Page/嘉山县.md "wikilink")，[中华人民共和国政治人物](../Page/中华人民共和国.md "wikilink")。\[1\]

## 生平

1915年，汪道涵生于[安徽省](../Page/安徽省.md "wikilink")[盱眙县](../Page/盱眙县.md "wikilink")(1932年分設[嘉山县](../Page/嘉山县.md "wikilink")，今[明光市](../Page/明光市.md "wikilink")）。1932年考入[上海的](../Page/上海.md "wikilink")[交通大学机械系学习](../Page/交通大学.md "wikilink")。1933年1月参加[中国共产党领导的革命工作](../Page/中国共产党.md "wikilink")。1933年3月加入[中国共产党](../Page/中国共产党.md "wikilink")，同年11月底遭[国民政府军警逮捕](../Page/国民政府.md "wikilink")，三个月后被家人保释出狱，随后回到家乡继续从事革命工作。\[2\]\[3\]1937年春，复考入[光华大学](../Page/光华大学.md "wikilink")（今[华东师范大学](../Page/华东师范大学.md "wikilink")）理学院数理系，插入二年级继续读书。\[4\]

[七七事变后](../Page/七七事变.md "wikilink")，汪道涵赴[延安](../Page/延安.md "wikilink")。1938年1月，他来到[新四军工作](../Page/新四军.md "wikilink")，历任新四军第四支队战地服务团副团长、团长\[5\]；后参加新四军第五支队\[6\]。其间参与了[皖东抗日根据地的开辟](../Page/皖东抗日根据地.md "wikilink")。1940年3月起，历任[嘉山县抗日民主政府县长](../Page/嘉山县.md "wikilink")、中共嘉山县委书记兼县长。1942年1月，任淮南行署（[津浦路东](../Page/津浦路.md "wikilink")）副主任。1943年2月，任淮南行署副主任兼津浦路东专员。\[7\]后来出任中共淮南地委财经部部长兼淮南行署副主任。\[8\]1945年后，历任[苏皖边区政府财政厅副厅长](../Page/苏皖边区.md "wikilink")、建设厅副厅长，[华中军区](../Page/华中军区.md "wikilink")、[山东军区军工部部长兼政委](../Page/山东军区.md "wikilink")，胶东区行署代主任，安徽省财办主任。\[9\]\[10\]

1949年后，汪道涵历任[杭州市军管会副主任兼财经部部长](../Page/中国人民解放军杭州市军事管制委员会.md "wikilink")，浙江省财办副主任、[浙江省财政厅厅长兼](../Page/浙江省财政厅.md "wikilink")[浙江省商业厅厅长](../Page/浙江省商业厅.md "wikilink")，[华东军政委员会工业部部长](../Page/华东军政委员会.md "wikilink")。1952年后，历任[第一机械工业部副部长](../Page/第一机械工业部.md "wikilink")、[对外经济联络委员会常务副主任](../Page/对外经济联络委员会.md "wikilink")。[文化大革命期间](../Page/文化大革命.md "wikilink")，汪道涵遭受迫害。1978年后，历任[对外经济联络部副部长](../Page/对外经济联络部.md "wikilink")，[国家进出口管理委员会](../Page/国家进出口管理委员会.md "wikilink")、[外国投资管理委员会副主任](../Page/外国投资管理委员会.md "wikilink")。1980年后，历任[中共上海市委书记](../Page/中共上海市委.md "wikilink")，[上海市人民政府副市长](../Page/上海市人民政府.md "wikilink")、代市长、市长。\[11\]\[12\]\[13\]

1985年，退出领导岗位，改任上海市人民政府顾问、国务院上海经济区规划办公室主任。1985年12月28日，上海市台湾研究会成立，汪道涵被推举为上海市台湾研究会名誉会长。1985年，美国[塔夫茨大学授予其公共管理学名誉博士称号](../Page/塔夫茨大学.md "wikilink")，以赞扬其在上海市市长任内的政绩。[美国](../Page/美国.md "wikilink")[芝加哥市也授予他荣誉市民称号](../Page/芝加哥市.md "wikilink")。\[14\]\[15\]1985年，汪道涵增选为[中顾委委员](../Page/中顾委.md "wikilink")；1987年，再度当选为[中顾委委员](../Page/中顾委.md "wikilink")。\[16\]\[17\]\[18\]\[19\]1990年，当选为[宋庆龄基金会副主席](../Page/宋庆龄基金会.md "wikilink")。1993年11月，被推选为第九届上海市工商联名誉会长。1994年，被[上海交通大学董事会聘为名誉董事长](../Page/上海交通大学.md "wikilink")。他还是[北京大学](../Page/北京大学.md "wikilink")、[复旦大学](../Page/复旦大学.md "wikilink")、[同济大学等高校的客座教授](../Page/同济大学.md "wikilink")。\[20\]\[21\]\[22\]

1991年12月16日，[海峡两岸关系协会在](../Page/海峡两岸关系协会.md "wikilink")[北京成立](../Page/北京.md "wikilink")，汪道涵被推举为会长。1992年，任[中共中央对台工作领导小组成员](../Page/中共中央对台工作领导小组.md "wikilink")。1993年4月，汪道涵与[海基会董事长](../Page/海基会.md "wikilink")[辜振甫在](../Page/辜振甫.md "wikilink")[新加坡举行第一次](../Page/新加坡.md "wikilink")“[汪辜会谈](../Page/汪辜会谈.md "wikilink")”，签署了《汪辜会谈共同协议》等四项协议。\[23\]1998年10月14日，两人又在上海[和平饭店和平厅进行了](../Page/和平饭店.md "wikilink")“自然对话”。2005年5月3日，[中国国民党主席](../Page/中国国民党.md "wikilink")[连战在首次访问上海时](../Page/连战.md "wikilink")，也专程与汪道涵在[锦江饭店锦江小礼堂会面](../Page/锦江饭店.md "wikilink")。2005年12月8日，汪道涵獲[香港中文大學頒授榮譽](../Page/香港中文大學.md "wikilink")[法學](../Page/法學.md "wikilink")[博士學位](../Page/博士.md "wikilink")\[24\]。

汪道涵是中共八大、十二大、十三大、十四大、十五大代表，中共第十二届中央候补委员。他是第五、六届全国人大代表。\[25\]

2005年12月24日，汪道涵在上海[瑞金醫院病逝](../Page/瑞金醫院.md "wikilink")，享年90岁。\[26\]

## 家庭

  - 父：[汪雨相](../Page/汪雨相.md "wikilink")，[同盟会元老](../Page/同盟会.md "wikilink")\[27\]。
  - 元配：戴锡可，生有二子三女后病逝。
      - 长子：[汪致远](../Page/汪致远.md "wikilink")，[中国人民解放军总装备部科技委员会副主任](../Page/中国人民解放军总装备部.md "wikilink")、[中将](../Page/中将.md "wikilink")。
      - 次子：汪致重，上海海湾投资董事长。
      - 长女：汪东宁，上海国际经济文化交流公司总经理。
      - 三女：汪凝，吉大正元董事长。
  - 继室：孙维聪
      - 幼子：[汪雨](../Page/汪雨.md "wikilink")（孙维聪生）
  - 汪导洋（1930年出生）、汪导海（1924年出生），兄弟关系，两人均任中央办公厅机要局局长。

## 参考文献

## 相关条目

  - [汪辜会谈](../Page/汪辜会谈.md "wikilink")
  - [九二共识](../Page/九二共识.md "wikilink")

[Category:中共八大代表](../Category/中共八大代表.md "wikilink")
[Category:中共十二大代表](../Category/中共十二大代表.md "wikilink")
[Category:中共十三大代表](../Category/中共十三大代表.md "wikilink")
[Category:中共十四大代表](../Category/中共十四大代表.md "wikilink")
[Category:中共十五大代表](../Category/中共十五大代表.md "wikilink")
[Category:第五届全国人大代表](../Category/第五届全国人大代表.md "wikilink")
[Category:第六届全国人大代表](../Category/第六届全国人大代表.md "wikilink")
[Category:中国共产党第十二届中央委员会候补委员](../Category/中国共产党第十二届中央委员会候补委员.md "wikilink")
[3](../Category/上海市人民政府市长.md "wikilink")
[Category:海峽兩岸關係协会会长](../Category/海峽兩岸關係协会会长.md "wikilink")
[Category:南師附中校友](../Category/南師附中校友.md "wikilink")
[Category:中央大学附属中学校友](../Category/中央大学附属中学校友.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[D](../Category/汪姓.md "wikilink")
[Category:明光人](../Category/明光人.md "wikilink")
[Category:光華大學校友](../Category/光華大學校友.md "wikilink")

1.  [汪道涵生平简介，中国台湾网，2010-12-29](http://www.taiwan.cn/xwzx/jdxg/201012/t20101229_1667851.htm)


2.  [滁州革命珍典，中国共产党新闻网，于2013-04-29查阅](http://dangshi.people.com.cn/GB/18027604.html)

3.  [汪道涵生平简历，凤凰网，2008年04月28日](http://news.ifeng.com/history/1/renwu/200804/0428_2665_512269.shtml)

4.  [学生时代的汪道涵，新浪，2005-12-28](http://news.sina.com.cn/o/2005-12-28/01517833254s.shtml)

5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19. [汪道涵同志遗体在沪火化，光明网，2005-12-31](http://www.gmw.cn/01gmrb/2005-12/31/content_354400.htm)

20.
21.
22.
23.
24.

25.
26.
27.