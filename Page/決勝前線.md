，是由[Best
way開發的](../Page/Best_way.md "wikilink")[即時戰术遊戲](../Page/即時戰术遊戲.md "wikilink")，[育碧软件发行](../Page/育碧软件.md "wikilink")。這遊戲是2004年的《[士兵：二战英雄](../Page/士兵：二战英雄.md "wikilink")》的續集。有鑑於《士兵：二战英雄》可以控制一些小隊隊員單獨奮戰於敵境，《決勝前線》決心帶領玩家體驗高度且自主的[人工智慧控制隊伍於大規模戰役中](../Page/人工智慧.md "wikilink")。此遊戲擁有全3D引擎可使玩家掌握戰況視角更甚於前代。開發者更加強了多人戰役於此遊戲，賦予更多的表現方式和選擇。遊戲發售於[俄羅斯](../Page/俄羅斯.md "wikilink")2006年9月8日（由[1C
Company代理](../Page/1C_Company.md "wikilink")），[北美](../Page/北美.md "wikilink")2006年9月12日。

## 單人遊戲模式

難度可分為遊戲模式（Arcade Mode）及戰術模式（Tactic
Mode），換句話說，也就是簡單及困難。而[德軍](../Page/德意志國防軍.md "wikilink")
、[英](../Page/英.md "wikilink")[美](../Page/美.md "wikilink")[盟軍](../Page/盟軍.md "wikilink")、[蘇軍將是玩家在此能使用的軍隊](../Page/蘇軍.md "wikilink")。

| [德軍](../Page/德意志國防軍.md "wikilink")      | [英](../Page/英.md "wikilink")[美](../Page/美.md "wikilink")[盟軍](../Page/盟軍.md "wikilink") | [蘇軍](../Page/蘇軍.md "wikilink")                    |
| --------------------------------------- | -------------------------------------------------------------------------------------- | ------------------------------------------------- |
| 基本訓練（Basic Training）                    |                                                                                        |                                                   |
| 坦克訓練（Tank Training）                     |                                                                                        |                                                   |
| 奈美根橋（Nijmegen bridge）                   | 雷達站（Radar）                                                                             | [盧布林](../Page/盧布林.md "wikilink")（Lublin）          |
| 維爾茨（Wiltz）                              | 法萊斯（Falaise）                                                                           | 奪回文件（Return The Documents）                        |
| 斯圖蒙（Stoumont）                           | [瓦爾赫倫島](../Page/瓦爾赫倫島.md "wikilink")（Walcheren Isle）                                   | 要塞（Stronghold）                                    |
| Clerf                                   | [馬斯垂克](../Page/馬斯垂克.md "wikilink")（Maastricht）                                         | Seelow Station                                    |
| Marvie                                  | 阿夫朗什（Avranches）                                                                        | 側襲（Flank Assault）                                 |
| [迪南](../Page/迪南.md "wikilink")（Dinant）  | Saint-Lambert                                                                          | Neuenhagen                                        |
| 羅什福爾（Rochefort）                         | [巴斯通](../Page/巴斯通.md "wikilink")（Bastogne）                                             | 萊特車站（Lehrter Bahnhof）                             |
| 必須完成以上所有關卡，才會自動解開以下隱藏關卡                 |                                                                                        |                                                   |
| [魯汶](../Page/魯汶.md "wikilink")（Louvain） | 奧馬哈海灘（Omaha Beach）                                                                     | [德國國會大樓](../Page/德國國會大樓.md "wikilink")（Reichstag） |

## 多人遊戲模式

過去Soldiers：Heroes of World War II只能連IP，現在Faces of
War可以連線別人開的遊戲房間（1-8名玩家）。遊戲支援德軍、英美聯軍、蘇聯三種國家。

決勝前線在1.04.1版本，支援以下6種多人模式：

  - Battle Zone（戰略點）：搶點、守點，累計分數。

<!-- end list -->

  - Chicken
    Hunt（捉雞）：你的同伴們肚子餓了，除了偷走兇惡農場主人的雞隻來補充能量外，沒有其餘選擇。抓農場的雞，累計分數。會有血超厚、攻擊力超強鬍子大叔（農場主人）把關，殺死大叔和敵方單位都不會得分。地圖有三個農場。當農場的主人陣亡，自動轉為其餘兩個農場，如此類推。

<!-- end list -->

  - Combat（團體對抗）：可以選自己國互打（Ex:德國打德國），或是對立國互打（Ex:德國打蘇聯）。一開始分兩隊進行，建立關卡者可以決定兩隊的國家，後來加入的玩家自己選擇要加入哪一隊之中。最多支援4X4共8隊同時連線。

<!-- end list -->

  - Combat FFA（誅死對抗）：沒有分隊，除了自己以外都是敵人，最多8人同時進行遊戲，國家自選。

<!-- end list -->

  - Cooperative（合作破關）：可以多人一起破單人劇情關卡，最多支援四個玩家

<!-- end list -->

  - Front
    Line（前線）：一方當進攻方，一方當守備方，一場結束後，攻守互換。進攻方要努力進攻，佔領守方根據地；守方要努力防守，防止對方佔領。其間進攻方資源採時間制，不同單位能製造的時間不同，要等時間；進攻方可以製造所有載具。守方資源採預設制，每個據點算一輪，每輪給予一固定點數。每個單位製造點數不同，點數花費後不補充，用完就沒了。防禦方只能製造防禦性武器，中、重型戰車與火箭車都不能製造。共三次進攻機會，每次成功佔領對方據點後。守方會退往下一個據點，進攻方也會被強制移動到之前佔領的據點。每移動一次據點，地形越狹窄，對守方越有利。

## 載具武器

遊戲中包含以下陸、海、空載具武器

### [蘇聯](../Page/蘇聯.md "wikilink")

#### 機車，車，卡車

  - 67B
  - M-72
  - ZIS-5（運兵車）
  - ZIS-42
  - GAZ-67, GAZ-67B

#### 裝甲車

  - M3半履帶車
  - BA-64
  - BA-11

#### 坦克

  - [T-70輕型坦克](../Page/T-70輕型坦克.md "wikilink")
  - [BT-7中型坦克](../Page/BT-7中型坦克.md "wikilink")
  - [T-34/57中型坦克](../Page/T-34坦克.md "wikilink")
  - [T-34/76中型坦克](../Page/T-34坦克.md "wikilink") （掃雷型）
  - [T-34/85中型坦克](../Page/T-34坦克.md "wikilink")
  - [IS-2重型坦克](../Page/IS-2重型坦克.md "wikilink")
  - [KV-85重型坦克](../Page/KV-85重型坦克.md "wikilink")
  - [KV-1重型坦克](../Page/KV-1重型坦克.md "wikilink")

#### 防空炮

  - ZIS-42 with Quad

#### 自走炮

  - [SU-85自走砲](../Page/SU-85自走砲.md "wikilink")
  - SU-76M自走反坦克炮
  - ISU-152自走榴彈炮
  - BM-13 "卡秋沙"

#### 戰機

  - IL-2攻擊機 （M3/IL-2M3）

#### 海上

  - PG-117
  - YA-5
  - BK-1124

#### 火炮

  - ZIS-3式中型反坦克炮

### [德國](../Page/納粹德國.md "wikilink")

#### 機車，車，卡車

  - VW 82水桶吉普車
  - Horch Car
  - R12
  - Blitz 3.6-36（with modifications）

#### 裝甲車

  - Leichter_Panzerspahwagen|SdKfz-223
  - Sdkfz.251/22反坦克半履帶車（Equipped with a 37 mm Pak 36 anti-tank gun
    mount）

#### 坦克

  - [一號坦克](../Page/一號坦克.md "wikilink")（Pz I （B））
  - [二號坦克](../Page/二號坦克.md "wikilink")（Pz Luchs）
  - [三號坦克](../Page/三號坦克.md "wikilink")（Pz III （N））
  - 三號除雷坦克（Pz Sapper）
  - [四號坦克](../Page/四號坦克.md "wikilink")（Pz IV Ausf.G）
  - [豹式坦克](../Page/豹式坦克.md "wikilink")（Pz V Panther）
  - [虎I坦克](../Page/虎I坦克.md "wikilink")（Pz VI Tiger）
  - [虎II坦克](../Page/虎II坦克.md "wikilink")（Pz VI Konigstiger）

#### 防空炮

  - Panzer I|Flakpanzer-I
  - SdKfz 7|SdKfz-7/1
  - 旋風自走防空炮
  - FlaK 38四聯裝20毫米防空炮

#### 自走炮

  - [追獵者式戰車](../Page/追獵者式戰車.md "wikilink")
  - 胡蜂自走炮
  - Panzerwerfer|SdKfz-4/1 "Panzerwerfer"

#### 火炮

  - PaK 40反坦克炮
  - sFH 18 150mm
  - FlaK 38 20毫米防空炮
  - Flak 41 88毫米火炮
  - Granatenwerfer sGrW 42
  - Raketenwerfer 56

#### 戰機

  - [Bf 109戰鬥機](../Page/Bf_109戰鬥機.md "wikilink")
  - [Ju 87俯衝轟炸機](../Page/Ju_87俯衝轟炸機.md "wikilink")
  - Fi-156 Storch

### 英美同盟

#### 機車，車，卡車

  - Willys MB , Willys AT（吉普車）
  - GMC Refueller（加油車）
  - GMC CCKW-353（運輸兵車）
  - Dodge WC-56

#### 裝甲車，兩棲載具

  - M8灰狗裝甲車
  - M20通用裝甲
  - LVT（A）鱷魚兩棲登陸火力支援車（兩棲載具）
  - M3A1（半履帶裝甲運輸兵車）
  - M16 半履帶反空裝甲車

#### 坦克

  - [M4雪曼](../Page/M4雪曼.md "wikilink")
  - M4雪曼中型坦克（掃雷型）
  - M4A3雪曼Calliope
  - 雙D雪曼水陸兩棲坦克
  - M4螢火蟲坦克
  - [M26潘興](../Page/M26潘興.md "wikilink")
  - Mk.VII傘兵戰車
  - M36 Jackson

#### 防空炮

  - Crusader AAII

#### 自走炮

  - [M7牧師](../Page/M7牧師.md "wikilink")
  - M8自走輕型榴彈炮
  - MK.I Bishop
  - A39

#### 火炮

  - M1A1 75毫米輕型榴彈炮
  - QF25磅輕型榴彈炮

#### 戰機

  - [P-51野馬戰鬥機](../Page/P-51野馬戰鬥機.md "wikilink")
  - CG-4A Hardrian
  - G.A.L.49 Hamilc

## 遊戲評價

  - GameSpot 6.0/10
  - IGN 7.0/10
  - Gamers Europe 7.5/10
  - 2404 - PC Gaming 8.9/10
  - PC Gamer 46/100
  - PC Zone UK 75/100
  - PC Format UK 79/100
  - Computer Games Mag 2.5/5
  - Jolt Online Gaming UK 5.4 /10
  - Fragland 75.5/100
  - Gamers Temple 60 / 100
  - Pro-G 6 / 10
  - Pelit （Finland） 90 / 100
  - Gameplanet 3 / 5
  - Games Radar 6 / 10
  - Gamer Within 5.6 / 10
  - TotalVideoGames 5 / 10

## 遊戲資料片

由Digitalmindsoft一手製作的Faces Of
War資料片，資料片將會新增玩家戰績記錄系統功能、新增武器載具、新建築物、任務以及新塗裝等等，資料片更加會把遊戲優化。

## 外部連結

  - [決勝前線官方網站](http://facesofwargame.uk.ubi.com/)

  - [決勝前線官方討論區](http://forums.ubi.com/groupee/forums?a=frm&s=400102&f=3001055373/)

[F](../Category/2006年电子游戏.md "wikilink")
[Category:即时战术游戏](../Category/即时战术游戏.md "wikilink")
[Category:二战题材电子游戏](../Category/二战题材电子游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:后传电子游戏](../Category/后传电子游戏.md "wikilink")
[Category:乌克兰开发电子游戏](../Category/乌克兰开发电子游戏.md "wikilink")