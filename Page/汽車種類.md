[Portail_automobile_bandeau_centre.gif](https://zh.wikipedia.org/wiki/File:Portail_automobile_bandeau_centre.gif "fig:Portail_automobile_bandeau_centre.gif")
**[汽車種類](../Page/汽車.md "wikilink")**（**Car
classification**）即自從進入多種類汽車的時代後，依據車輛底盤、大小、功用、性能、售價、特性、排氣量，所給予汽車相當主觀的區分名稱。但是，並非代表所有的汽車種類皆有在各個國家出售或使用相同的名稱。因為，即使是相同的汽車也不一定被分類至該區域內。

例如在日本的「乘用車」字面意思相當於中國所說的「載客車」，但日本的乘用車其實只是[轎車](../Page/轎車.md "wikilink")，中國的載客車是同時指轎車和[客車的](../Page/客車.md "wikilink")。

## 種類名稱

依據各個國家廣泛使用的汽車種類名稱為對象：

### 普通[私家車的分類](../Page/私家車.md "wikilink")

**說明：各個國家常用的名稱，皆以廣泛使用的通用名稱為對象。主要的中譯名稱與原文名稱皆以「粗體字」表示；次要的中譯名稱至原文名稱上「加註」並以「粗體字」表示；其他的中譯名稱至原文名稱上方「加註」不以「粗體字」表示。如果，特定英文名稱未有廣泛使用的中譯名稱，則用「—」記號代替。**

<table>
<thead>
<tr class="header">
<th><p>colspan = "7"| 汽車種類<br />
（Car classification）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>中文譯名<br />
<font size="-1">（Chinese）</font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1"><a href="../Page/極小型車.md" title="wikilink">極小型車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1"><a href="../Page/次紧凑型车.md" title="wikilink">次紧凑型车</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1">Supermini</font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1"><a href="../Page/紧凑型轿车.md" title="wikilink">紧凑型轿车</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1"><a href="../Page/中型車.md" title="wikilink">中型車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">入门级<br />
<a href="../Page/高級車.md" title="wikilink">高級車</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1"><a href="../Page/中大型車.md" title="wikilink">中大型車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">中型<br />
<a href="../Page/高級車.md" title="wikilink">高級車</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1"><a href="../Page/大型車.md" title="wikilink">大型車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1"><a href="../Page/跑車.md" title="wikilink">跑車</a></font></p></td>
</tr>
<tr class="even">
<td><p>————</p></td>
</tr>
<tr class="odd">
<td><p><font size="-1"><a href="../Page/超級跑車.md" title="wikilink">超級跑車</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1"><a href="../Page/敞篷車.md" title="wikilink">敞篷車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">雙人座敞篷車<br />
Roadster</font></p></td>
</tr>
<tr class="even">
<td><p>————</p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">微型<br />
<a href="../Page/多功能休旅車.md" title="wikilink">多功能休旅車</a></font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1">小型<br />
多功能休旅車</font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">大型<br />
多功能休旅車</font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1">微型<br />
<a href="../Page/運動休旅車.md" title="wikilink">運動休旅車</a></font></p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">小型<br />
運動休旅車</font></p></td>
</tr>
<tr class="even">
<td><p>————</p></td>
</tr>
<tr class="odd">
<td><p><font size="-1">中型<br />
運動休旅車</font></p></td>
</tr>
<tr class="even">
<td><p><font size="-1">大型<br />
運動休旅車</font></p></td>
</tr>
</tbody>
</table>

### [電單車](../Page/電單車.md "wikilink")

而歷史上[卡尔·本茨所改裝的車子](../Page/卡尔·本茨.md "wikilink")，實際上是一輛[人力三輪車](../Page/人力三輪車.md "wikilink")，所以以現代定義平治發明汽車的同時發明了電單車，電單車是否汽車一直有爭論，但常一併被談論的。包括了

  - 傳統[電單車](../Page/電單車.md "wikilink")

<!-- end list -->

  - [電動機車如](../Page/電動機車.md "wikilink")[電動自行車](../Page/電動自行車.md "wikilink")

<!-- end list -->

  - [摩托化自行車](../Page/摩托化自行車.md "wikilink")

<!-- end list -->

  - 電動[獨輪車](../Page/獨輪車.md "wikilink")

<!-- end list -->

  - 雙輪並列而非縱列的構造如[賽格威和](../Page/賽格威.md "wikilink")[本田UNI-CUB](../Page/本田UNI-CUB.md "wikilink")

<!-- end list -->

  - 動力的[滑板車如](../Page/滑板車.md "wikilink")[綿羊仔](../Page/綿羊仔.md "wikilink")

<!-- end list -->

  - 機動[三輪車如](../Page/三輪車.md "wikilink")[嘟嘟車和](../Page/嘟嘟車.md "wikilink")[電動三輪車](../Page/電動三輪車.md "wikilink")

<!-- end list -->

  - [履帶式機車](../Page/履帶.md "wikilink")，如全履帶的[雪地電單車和](../Page/雪地電單車.md "wikilink")[半履帶車如](../Page/半履帶車.md "wikilink")[Sdkfz
    2](../Page/Sdkfz_2.md "wikilink")

<!-- end list -->

  - [全地形車](../Page/全地形車.md "wikilink")

<!-- end list -->

  - [雙輪驅電單車](../Page/雙輪驅電單車.md "wikilink")

<!-- end list -->

  - 電單車的[特種車](../Page/特種車.md "wikilink")，特制或改裝於特殊指定用途。

### 另類構造的汽車

整個二十世紀汽車主流就是由人駕駛，以[內燃機燃燒](../Page/內燃機.md "wikilink")[汽油或](../Page/汽油.md "wikilink")[柴油的四輪或以上的機動交通工具](../Page/柴油.md "wikilink")，但實際上因為科技發展出現過不同類型的汽車，只要車輛本身裝有動力機構和擁有輪子，不需要司機用力量就可以保持平衡，就算是廣義汽車了，而在一些常規結構的汽車難以使用的場合，則由另類的設計代替。

#### 新能源汽車

在二十世紀初經常使用非[汽油或](../Page/汽油.md "wikilink")[柴油能源的車輛](../Page/柴油.md "wikilink")，因為其不便被淘汰，在[石油危機與](../Page/石油危機.md "wikilink")[全球暖化後卻得到復興](../Page/全球暖化.md "wikilink")。

  - [電動車輛](../Page/電動車輛.md "wikilink")
    電動車不會排放廢氣又起動容易，十九世紀出現了預先用化學的方式產生的[原電池車](../Page/原電池.md "wikilink")，在1908年發明大王[愛迪生發明了使用](../Page/愛迪生.md "wikilink")[蓄電池的車輛](../Page/蓄電池.md "wikilink")，但兩者都不像內燃機汽車方便而被淘汰。現時流行的方案是[混合動力車輛](../Page/混合動力車輛.md "wikilink")，可以免卻了充電的功夫，但仍然需要其他燃料，但因為其發電機是內燃機最佳效率的狀態發電充電，所以綜合其粍油量仍然遠低於純內燃機汽車。

<!-- end list -->

  - [氣體燃料車輛](../Page/氣體燃料.md "wikilink")
    可使用各種低污染和廉價的氣體燃料。而因為節省燃料上的方便在[二戰的日本](../Page/二戰.md "wikilink")，經常使用像[木煤氣等氣體燃料驅動汽車](../Page/木煤氣.md "wikilink")，但二十一世紀的形式多是使[天然氣或](../Page/天然氣.md "wikilink")[氫動力汽車](../Page/氫動力汽車.md "wikilink")。缺點是氣體燃料體積大和低燃點的安全問題。

<!-- end list -->

  - [核能車](../Page/核能.md "wikilink")，理論上全壽命不需要加添燃料，但放射性的安全問題和廢料循環較難解決，可能需要[第四代反應堆才成事](../Page/第四代反應堆.md "wikilink")。

<!-- end list -->

  - [蒸汽動力](../Page/蒸汽動力.md "wikilink"),
    因為工業革命是從蒸氣機開始的，所以最早實現的汽車型式，但因為需要預先煮沸水來產生蒸氣故使用不便，從未被推廣並在二十世紀中被淘汰，可是因為[石油危機其超高能量轉化效率復興](../Page/石油危機.md "wikilink")，又可以使用任何化學燃料，但新形式多是[斯特林发动机](../Page/斯特林发动机.md "wikilink")。

<!-- end list -->

  - [太陽能車](../Page/太陽能.md "wikilink")，通常實際是指有[太陽能電池和蓄電池的車輛](../Page/太陽能電池.md "wikilink")。

<!-- end list -->

  - [機械能車](../Page/機械能.md "wikilink")，如採用[壓縮空氣和](../Page/壓縮空氣.md "wikilink")[飛輪或](../Page/飛輪.md "wikilink")[發條等驅動車輛](../Page/發條.md "wikilink")，而十七世紀德國制造過一輛發條車做實驗，而在二十世紀末法國也做出一種用壓縮空氣驅動的車輛。

<!-- end list -->

  - [生物能源車](../Page/生物能源.md "wikilink")，主要指燒[酒精等生物制品的車輛](../Page/酒精.md "wikilink")，缺點是現時的材料為[糧食](../Page/糧食.md "wikilink")，把人吃的餵汽車變成本末倒置，故正在發展用其他材料如[草或](../Page/草.md "wikilink")[藻類做](../Page/藻類.md "wikilink")[甲醇材料代替](../Page/甲醇.md "wikilink")。

<!-- end list -->

  - 六[衝程發動機汽車](../Page/衝程.md "wikilink")，[內燃機仍然大有潛力](../Page/內燃機.md "wikilink")，有人開發出一種六衝程發動機，效率可以大幅超出現代[四衝程發動機](../Page/四衝程.md "wikilink")。

#### [自動駕駛汽車與](../Page/自動駕駛汽車.md "wikilink")[遙控車](../Page/遙控.md "wikilink")

[雷達和](../Page/雷達.md "wikilink")[電腦的發展後](../Page/電腦.md "wikilink")，不需要人手控制的新世代的汽車，早年為了軍事和科研活動中，一些高危或明知有去無回的任務中應用。最近發展成熟到可以在一般交通上使用，並在某些國家或地區的路上行走了。

  - [探測車](../Page/探測車.md "wikilink")，在[航天事業中使用來在地外天體上探索的汽車](../Page/航天.md "wikilink")。

<!-- end list -->

  - [遙控車](../Page/遙控.md "wikilink")，雖然不是全自動但是無人駕駛如[哥利亞遙控炸彈和](../Page/哥利亞遙控炸彈.md "wikilink")[遙控戰車](../Page/遙控戰車.md "wikilink")。

#### [履帶車](../Page/履帶.md "wikilink")

適用於泥地上負載很重的車輛，不使用普通車輪如[坦克車](../Page/坦克車.md "wikilink")，有人認為技術上汽車只可以用輪子直接驅動，所以履帶車是否汽車也是一個值得討論的問題。

  - [坦克車](../Page/坦克車.md "wikilink")
  - [半履帶車](../Page/半履帶車.md "wikilink")
  - [雪地車](../Page/雪地車.md "wikilink")

#### [燃氣輪機汽車](../Page/燃氣輪機.md "wikilink")

#### [兩棲車輛](../Page/兩棲車輛.md "wikilink")

#### [噴氣發動機車輛](../Page/噴氣發動機.md "wikilink")

作為創世界記錄和研究以輪子行駛的車輛在極限條件下狀態的車輛，速度遠超任何[超跑或](../Page/超跑.md "wikilink")[賽車](../Page/賽車.md "wikilink")，甚至可以超過[音速的](../Page/音速.md "wikilink")。

  - [火箭發動機](../Page/火箭發動機.md "wikilink")

<!-- end list -->

  - [渦輪噴氣發動機](../Page/渦輪噴氣發動機.md "wikilink") 代表
      - [超音速推進號](../Page/超音速推進號.md "wikilink")

#### [科幻文化汽車的代替品](../Page/科幻文化.md "wikilink")

科幻故事中常出現可以可以停泊在街道或公路旁的交通工具，駕駛方式如同汽車的[交通工具](../Page/交通工具.md "wikilink")，在人物的對話中可能稱為「車」或在故事中起了現實世界汽車的作用，一部分真的仍然屬於汽車的範圍，如上文的新能源汽車和自動駕駛汽車等並且已經實現了。

但另有兩種其實和其他的技術產物重疊，即[飛行車和](../Page/飛行.md "wikilink")[步行車](../Page/步行.md "wikilink")，其實只算是[飛行器和](../Page/飛行器.md "wikilink")[機器人](../Page/機器人.md "wikilink")。而現實中也有人真的做出過飛得起的汽車，但就是簡單地把汽車裝在[飛機上](../Page/飛機.md "wikilink")，結果在飛行時嫌過重而從未被接受。

## 外觀與功能

### 貨車（Trucks）

#### 軍用卡車

#### 貨櫃車

### 公車（Bus）

#### 小巴

  -
#### 雙層巴士

  -
#### 客車

  -
#### 旅遊車

  -
#### 計程車

  -
### 軍車

  -
軍事需要開發制造的汽車，但不論戰時和平時軍隊都會使用普通的汽車。

#### 坦克車

  -
#### 裝甲車

  -
#### 軍用卡車

  -
#### 自走炮

  -
#### 越野車

  -
#### 兩棲車輛

  -
### 賽車

  -   - [賽車車輛和賽事分組](../Page/:EN:List_of_motorsport_championships.md "wikilink")

#### 方程式賽車

  -   - [小型賽車](../Page/小型賽車.md "wikilink")
      - [一級方程式](../Page/一級方程式.md "wikilink")

#### 改裝賽車

  -   - [R組賽車](../Page/:en:Group_R.md "wikilink")
      - [N組賽車](../Page/N組賽車.md "wikilink")
      - [S組賽車](../Page/:en:Group_S.md "wikilink")
      - [Silhouette (賽車)](../Page/Silhouette_\(賽車\).md "wikilink")

#### 原型賽車

  -
  -
### 特種車

  -
特種汽車及設備(Special Purpose Vehicle)，簡稱SPV, 為了一些較特別的用途開發和改裝的汽車。

#### 概念車

  -
#### 消防車

  -
#### 運鈔車

  -
#### 救護車

  -
#### [警車](../Page/警車.md "wikilink")

  -
#### 花車

  -
#### 運油車

  -
#### 洗街車

  -
#### 實驗車

  -
#### 垃圾車

  -
### 工程車輛

#### 拖拉機

#### 拖車（Trailer）

#### 推土機

#### 壓路機

#### 裝載機

#### 礦運裝載車

#### 施工起重台

#### 鑽孔機

#### 挖掘机

#### 全斷面隧道鑽掘機

#### 叉車

#### 抓鈎機

#### 自動傾卸卡車

### 露營車（Recreation Vehicle）

設置具有居住設備的車輛，如同可自由活動的住家，專門使用在戶外露營或旅遊活動。最大的特色為完全解決置身戶外時，所有食宿與民生問題。依據車輛行駛和使用方式，大致可分為「自走式」（Motorhome）、「承載式」（Motorcoach）及「拖曳式」（Caravan）三種型式。

### 私家車

供市場一般人購買和代步的交通工具，分類可見於車體風格。

#### 轎車（Sedan）

轎車實際上是所有私家車的原型，作為個人或小群人的代步工具，但有些私家車因為主要目的是遊樂和營業或炫耀等，實際上會被分開視為其他車型，可仍然被歸屬廣義的轎車之中。
  [極小型車](../Page/極小型車.md "wikilink")（Mini Sedan）

  - Hyundai Accent
  - Proton saga
  - Toyota Yaris/VItz

[微型車](../Page/微型車.md "wikilink")（Subcompact car）

  - [本田飞度](../Page/本田飞度.md "wikilink")（Honda Fit）
  - [福特Festiva](../Page/福特Festiva.md "wikilink")
  - [豐田Yaris](../Page/豐田Yaris.md "wikilink")
  - [馬自達2](../Page/馬自達2.md "wikilink")

[緊湊型車](../Page/緊湊型車.md "wikilink")（Compact Sedan）

  - Ford Focus
  - Proton Gen-2
  - Honda Civic
  - Mitsubishi Lancer
  - Toyota Corolla Altis
  - [Nissan Tiida 4-Door](../Page/日產Tiida.md "wikilink")
  - [Nissan Sentra](../Page/日產Sentra.md "wikilink")

[中型車](../Page/中型車.md "wikilink")（Mid-size Sedan）

  - Ford Mondeo
  - Honda Accord
  - Peugeot 407
  - Nissan Altima
  - Toyota Camry
  - [Nissan Teana](../Page/日產天籟.md "wikilink")

大型（Full-size Sedan）

  - Honda Legend
  - Peugeot 607
  - Lexus GS 300
  - Nissan Maxima

[豪華車](../Page/豪華車.md "wikilink")(Luxury vehicle)

  - [奥迪A8](../Page/奥迪A8.md "wikilink")
  - [勞斯萊斯幻影](../Page/勞斯萊斯.md "wikilink")

[加長轎車](../Page/加長轎車.md "wikilink")(limousine)

  - [林肯领航员](../Page/林肯领航员.md "wikilink")
  - [凱迪拉克加長型](../Page/凱迪拉克.md "wikilink")

<File:Proton> Saga (re-release; third facelift) (front),
Serdang.jpg|Proton Saga <File:2012> Hyundai Accent GLS sedan --
06-29-2011.jpg|Hyundai Accent <File:Peugeot> 607 2.7 HDi Facelift
20090529 front.JPG|Peugeot 607 <File:2008> Nissan Teana 01.JPG|Nissan
Teana <File:4D> Nissan Tiida.jpg|Nissan Tiida 4-Door <File:2013> Nissan
Sentra SR front left.jpg|Nissan Sentra

#### 掀背車（Hatchback）

通常，小型車大多是掀背車，可以最大程度地利用有限的空間。雖然，內部空間的較正常轎車短，其尾門幾乎是垂直，座位所占用的空間也減少很多，卻使得增強許多操縱的靈活度，這對一些道路狹窄且交通擁擠的國家特別重要。大多數後座都具有摺疊平放的功能，可在後排座不坐人的情況下，增加行李廂的空間。
  雙門（2D Hatchback）

  - Bmw Mini
  - Citroën C2
  - Proton Satria Neo
  - Peugeot 308
  - Peugeot 208
  - Mini Cooper
  - Volkswagen Golf
  - Renault Mégane
  - Volvo C30
  - Opel Astra

五門（5D Hatchback）

  - Citroën C4
  - Perodua Myvi
  - [Mazda2](../Page/馬自達2.md "wikilink")
  - Nissan March
  - Mitsubishi Mirage
  - Toyota Yaris
  - Suzuki Swift
  - Mitsubishi Colt Plus
  - Ford Focus
  - Subaru Impreza
  - [Nissan Livina](../Page/日產Livina.md "wikilink")
  - [Nissan Tiida 5-Door](../Page/日產Tiida.md "wikilink")

<File:2012-03-07> Motorshow Geneva 4263.JPG|Opel Astra <File:Suzuki>
Swift Sport.JPG|Suzuki Swift
[File:ProtonSatriaR3-MelbMotorshow.JPG|Proton](File:ProtonSatriaR3-MelbMotorshow.JPG%7CProton)
Satria Neo <File:2011> Perodua Myvi SE in Kota Kinabalu,
Sabah.jpg|Perodua Myvi <File:Renault> Mégane III RS
Kyalamigelb.JPG|Renault Mégane <File:Nissan> Tiida C12 01 China
2012-05-12.jpg|Nissan Tiida 5-Door <File:Nissan> Livina
20160412.jpg|Nissan Livina

#### 凹背車（Notchback）和快背車（fastback）

#### 旅行車（Sedan Wagon）

車輛用途十分廣泛，上班時可做為商業用途，下班後可做為休閒旅行使用的車型。雖然，其尾門幾乎是垂直，但內部空間並沒有因此減少，卻獲得增強許多操縱的靈活度。大多數後座都具有摺疊平放的功能，增加物品擺放空間。因此，具有多變空間與實用機能，使得受到不少人青睞。本意的旅行車是從三廂式轎車發展而來，現在則和[休旅車重疊了](../Page/休旅車.md "wikilink")，分別是傳統旅行車卻較像普通的轎車，休旅車則兼有越野車風格，其堅固和可靠性較高，但車速慢兼單價和粍油量也較高。
  小型（Compact Wagon）

  - Cadillac BLS
  - Ford Activa
  - [Mazda Isamu Genki](../Page/馬自達323.md "wikilink")

中型（Mid-size Wagon）

  - Dodge Magnum
  - Volkswagen Passat Variant
  - Mercedes-Benz E-Class Wagon

大型（Full-size Wagon）

  - Chrysler 300 Wagon
  - Opel Signum

<File:Opel> Signum Facelift 20090717 front.JPG|Opel Signum
<File:Mercedes-Benz> W123 T-Modell front 20090430.jpg|Mercedes-Benz
E-Class Wagon <File:2008> Dodge Magnum SRT-8 DC.JPG|Dodge Magnum
<File:Mercedes-Benz> E63 AMG stationwagon front Tx-re.jpg|Mercedes-Benz
E63AMG

#### 豪華車（Luxury car）

專門針對少數人士階層製造的意思，販賣價格高昂且車輛裝備豪華的轎車稱呼，雖然開發的本意仍然是代步，但還有人用來營業或烗耀的意味。豪車定義爭議較大的，因為對於品牌的崇拜或見識問題，有些人把一些由歷史上生產豪車普及度高的品牌所做當代[下價車也當作廣義的豪車](../Page/下價車.md "wikilink")，而未聞一些產量很少的頂尖豪車品牌和以下價車聞名的廠家的旗艦車，但[加長轎車卻是最符合經典意味上豪車的標準](../Page/加長轎車.md "wikilink")。

中型（Mid-size Luxury）

  - [Audi A6](../Page/Audi_A6.md "wikilink")
  - [BMW 5系列](../Page/BMW_5系列.md "wikilink")
  - Cadillac STS
  - Infiniti M37
  - Lexus ES
  - [Mercedes-Benz E-Class](../Page/梅賽德斯-奔馳E級.md "wikilink")
  - Saab 9-5

大型（Full-size Luxury）

  - Audi A8
  - [BMW 7系列](../Page/BMW_7系列.md "wikilink")
  - Cadillac DTS
  - Infiniti Q45
  - [勞斯萊斯幻影](../Page/勞斯萊斯幻影.md "wikilink")
  - Lexus LS
  - [Mercedes-Benz S-Class](../Page/梅賽德斯-賓士S-Class.md "wikilink")

[File:InfQ45.jpg|Infiniti](File:InfQ45.jpg%7CInfiniti) Q45 <File:2008>
Cadillac DTS·L.jpg|Cadillac DTS <File:2008> Infiniti M35X.jpg|Infiniti
M35 <File:2008-2009> Cadillac STS.jpg|Cadillac STS

#### 經濟型車（Economy car）

專門針對多數民眾階層製造的意思，販賣價格低廉且車輛裝備平凡的轎車稱呼。一般而言，亦表示這輛轎車的普及率比較高，所以單價一定要低到被發展中國家的[中產至發達國家的](../Page/中產.md "wikilink")[草根都能負擔的](../Page/草根.md "wikilink")，又要可以同時坐進一個[小家庭](../Page/小家庭.md "wikilink")，所以[電單車和](../Page/電單車.md "wikilink")[迷你車又不夠大了](../Page/迷你車.md "wikilink")，所以車型多為微型車、小型車或中型車。除了單價要平和全車較小外，粍油量和[牌照費等售後費用也很重要的嚴格地輕](../Page/牌照.md "wikilink")，所以不是以品牌來定義國民車的標準，正確是以售價低配備不奢侈和售後費用低來定義，所以上世紀末的小型化高級車品牌也打入了這個市場。
  微型（Mini Economy）

  - [SMART](../Page/司麥特_\(汽車\).md "wikilink")
  - Formosa Matiz
  - [Honda Fit](../Page/Honda_Fit.md "wikilink")
  - Toyota Vios
  - [Nissan March](../Page/日產March.md "wikilink")

小型（Compact Economy）

  - Chevrolet Aveo
  - Ford Focus
  - Toyota Altis
  - Mitsubishi Colt PLUS
  - [Nissan Livina](../Page/日產Livina.md "wikilink")
  - [Nissan Tiida 4-Door](../Page/日產Tiida.md "wikilink")
  - [Nissan Tiida 5-Door](../Page/日產Tiida.md "wikilink")
  - [Nissan Sentra](../Page/日產Sentra.md "wikilink")
  - [AUDI 1](../Page/奥迪A1.md "wikilink")
  - [BENZ A Class](../Page/梅赛德斯-奔驰A级.md "wikilink")
  - [寶馬迷你](../Page/寶馬迷你.md "wikilink")
  - Honda City

中型（Mid-size Economy）

  - Formosa Magnus
  - [Honda Civic](../Page/Honda_Civic.md "wikilink")
  - [Toyota Camry](../Page/豐田Camry.md "wikilink")
  - [Nissan Teana](../Page/日產天籟.md "wikilink")
  - [BMW 1](../Page/BMW_1系列.md "wikilink")

<File:2009> Chevrolet Aveo5 LS.jpg|Chevrolet Aveo <File:Third>
generation Toyota Vios-1-.jpg|Toyota Vios <File:Mitsubishi> Colt Plus
(front).JPG|Mitsubishi Colt PLUS <File:Nissan> Livina
20160412.jpg|Nissan Livina
[File:Nissan-MarchK13.jpg|Nissan](File:Nissan-MarchK13.jpg%7CNissan)
March <File:Nissan> Tiida C12 01 China 2012-05-12.jpg|Nissan Tiida
5-Door <File:2008> Nissan Teana 01.JPG|Nissan Teana <File:2013> Nissan
Sentra SR front left.jpg|Nissan Sentra <File:4D> Nissan Tiida.jpg|Nissan
Tiida 4-Door

#### 多功能休旅車（MPV）

微型（Mini MPV）

  - Daihatsu Materia
  - Peogeot 1007
  - Fiat Idea
  - [Honda Fit](../Page/Honda_Fit.md "wikilink")
  - Hyundai Matrix
  - Opel Meriva
  - Renault Modus
  - Mercedes-Benz A-Class
  - Nissan Cube
  - Perodua Alza
  - Toyota bB
  - Toyota Verso S

小型（Compact MPV／Minivan）

  - Chevrolet HHR
  - Citroën Picasso
  - Fiat Multipla
  - Honda FR-V
  - Toyota Verso
  - Proton Exora
  - [Mazda 5](../Page/馬自達5.md "wikilink")
  - Mercedes-Benz B-Class
  - Mitsubishi Freeca
  - Mitsubishi Zinger
  - Toyota WISH
  - Volkswagen Touran

大型（Full-size MPV／Minivan）

  - Peugeot 807
  - Citroën C8
  - Chrysler Town and Country
  - Ford Freestar
  - Honda Odyssey
  - Kia Carnival
  - [Mazda MPV](../Page/馬自達MPV.md "wikilink")
  - Nissan Quest
  - SsangYong Rodius
  - Toyota Sienna
  - Toyota Previa
  - Toyota Innova

<File:Opel> Meriva 1.4 Design Edition (B) – Frontansicht, 11. März 2012,
Heiligenhaus.jpg|Opel Meriva <File:2009> Kia Sedona LX --
11-25-2009.jpg|Kia Carnival <File:Proton> Exora.jpg|Proton Exora
<File:Renault> Modus Facelift 20090808 front.JPG|Renault Modus

### 客貨車

本意等同[廂型車](../Page/廂型車.md "wikilink")，但當代出現了其他多用途的車型，通常出產者目的是為了營業，但實際很多人用來代步或遊樂。

#### 廂型車（Van）

小型（Compact Van）

  - Ford PRZ
  - Ford Pronto
  - Mitsubishi Varica
  - [Subaru Estratto](../Page/速霸陸Domingo.md "wikilink")
  - [Suzuki Every](../Page/鈴木Every.md "wikilink")
  - [Suzuki Landy](../Page/鈴木Landy.md "wikilink")

中型（Mid-size Van）

  - Ford Econovan
  - Mitsubishi Delica
  - Mitsubishi
  - Toyota Hiace
  - Volkswagen T4

大型（Full-size Van）

  - Chevrolet Express
  - Nissan Vanette
  - Toyota Hiace

<File:Ford> Pronto of Duskin Taiwan 7471-QQ 20100914.jpg|Ford Pronto
<File:SUZUKI> LANDY SC26 F.jpg|Suzuki Landy
[File:Chevrolet-Express-Van.jpg|Chevrolet](File:Chevrolet-Express-Van.jpg%7CChevrolet)
Express <File:NISSAN> Vanette Van S21.jpg|Nissan Vanette

#### 貨卡車（Pickup Trucks）

19世紀初，原本是針對美國民眾在日常生活載物、運送務農機具，所需孕育而生的特殊車型，兼有可當作私家車的後座位，又有真正貨車的貨斗。因此，其便利的特性與實用的特質，仍為美國當地的主流車型之一。至今，貨卡車依舊是不少美系車廠的生產主力。近年來，歐系汽車與日系汽車，逐漸也開始往此領域持續發展當中，期望能扭轉此一情勢。
  微型（Mini Pickup Trucks）

  - Austin Mini Pick-up
  - Fiat Strada
  - Proton Arena

小型（Compact Pickup Trucks）

  - Ford Ranger
  - [Isuzu D-Max](../Page/Isuzu_D-Max.md "wikilink")
  - Holden Ute

大型（Full-size Pickup Trucks）

  - Nissan Titan
  - Toyota Tacoma
  - Toyota Hilux

<File:Proton> Arena (solid bed cover) (front), Kuala Lumpur.jpg|Proton
Arena <File:Mini> pickup truck.jpg|Austin Mini Pick-up <File:ISUZU>
D-MAX, 2nd Gen, Front Perspective View.jpg|Isuzu D-Max

#### 越野車或运动商务车（SUV）

專為征服各式崎嶇不平的路面狀況，因而特別設計的車型。通常，本身皆有[四輪驅動的特性](../Page/四輪驅動.md "wikilink")、引擎輸出扭力較大、車身底盤較高，車體較為堅固的優勢，使其具有高度機動性，足以適應各種嚴苛的路況，給人有種粗獷豪邁的感覺。另一方面，燃料耗損較快、車身重心較高，車輛機械結構較複雜的缺陷，仍是普遍為人所詬病的地方。
  微型（Mini 4x4／SUV）

  - Daihatsu Terios
  - Honda HR-V
  - Mitsubishi Pajero Mini
  - Jeep Wrangler
  - Suzuki Jimny
  - Suzuki Escudo/Sidekick 2D
  - [Nissan Juke](../Page/日產Juke.md "wikilink")

小型（Compact 4x4／SUV）

  - BMW X3
  - Ford Escape
  - Jeep Compass
  - Suzuki Vitara/Sidekick 4D
  - Suzuki Grand Vitara

中型（Mid-size 4x4／SUV）

  - BMW X5
  - Honda CR-V
  - Hyundai iX35
  - [Nissan X-Trail](../Page/日产奇骏.md "wikilink")
  - Toyota RAV4
  - [Nissan Murano](../Page/日產Murano.md "wikilink")

大型（Full-size 4x4／SUV）

  - Audi Q7
  - Hummer H2
  - Hyundai Veracruz
  - Land Rover LR3
  - [Mazda CX-9](../Page/馬自達CX-9.md "wikilink")

豪華 (Luxury 4x4 / SUV)

  - Lexus LX570
  - Porsche Cayenne
  - Range Rover

<File:Daihatsu> Terios Top front.jpg|Daihatsu Terios <File:Land> Rover
LR3 .jpg|Land Rover LR3 <File:2010> Hyundai Veracruz Limited --
08-26-2010.jpg|Hyundai Veracruz <File:Ford> Escape P4220633.jpg|Ford
Escape <File:2014> Nissan X-Trail (T32) ST 2WD wagon (2015-08-07)
01.jpg|Nissan X-Trail <File:Nissan> Murano P4220650.jpg|Nissan Murano
<File:Nissan> JUKE 15RS (YF15) front.JPG|Nissan Juke

#### 運動休旅車（SUV）

多用途[越野車的](../Page/#越野車（4x4）.md "wikilink")[美語名稱](../Page/美國英語.md "wikilink")。是由貨卡車的拖曳能力，旅行車的載客空間以及卡車的越野能力組合成的一種車型。

  - [高機動車](../Page/高機動車.md "wikilink")
  - [Nissan Rogue](../Page/Nissan_Rogue.md "wikilink")
  - [BMW X3](../Page/BMW_X3.md "wikilink")
  - [奇瑞瑞虎](../Page/奇瑞瑞虎.md "wikilink")
  - [本田CR-V](../Page/本田CR-V.md "wikilink")
  - [荒原路華](../Page/荒原路華.md "wikilink")

#### 運動貨卡車（SUT）

既不屬於轎車、貨卡車和越野車，但卻整合上述車輛優勢的車型。具有舒適的乘坐空間，便利的實用特性及高度機動性能，迎合年輕世代族群的喜好。
中型（Mid-size SUT）

  - Ford Explorer Sport Trac
  - [Honda Ridgeline](../Page/Honda_Ridgeline.md "wikilink")
  - Mitsubishi Triton
  - SsangYong Musso Sports
  - Toyota Tacoma

大型（Full-size SUT）

  - Chevrolet Avalanche
  - Ford F-150
  - [HUMMER](../Page/HUMMER.md "wikilink") H1 SUT
  - HUMMER H2 SUT
  - Toyota Hilux

<File:Chevrolet> Avalanche Z71 Black Diamond Last Edition
2013.jpg|Chevrolet Avalanche <File:2012> Toyota Tacoma --
10-19-2011.jpg|Toyota Tacoma <File:2009> Ford Sport Trac XLT.jpg|Ford
Explorer Sport Trac <File:SsangYong> Musso Sports 290S 1.jpg|Ssangyong
Musso Sports

#### 廂式休旅車（Minivan）

[多功能休旅車的](../Page/#多功能休旅車（MVP）.md "wikilink")[美語名稱](../Page/美國英語.md "wikilink")。

### 跨界休旅車 (CUV)

### 跑車（Sports Car）

私家車發展出特化於行駛性能和安全性，可算是[賽車運動的大眾化產物](../Page/賽車.md "wikilink")，開發的本意是遊樂或運動甚至炫耀，但事實很多人用來代步的。

強烈追求速度感、駕駛操控性樂趣、車身造型設計流線、車體製作輕巧穩定。如同行駛在『一般道路上的競速使用車輛』。高速行駛時，相當注重操控性和運動性能。雖然，普遍未必都有超高的速度，但卻擁有極佳的加速能力。

跑車可細分做多種類型而一部分與[轎車重疊](../Page/轎車.md "wikilink")，因為跑車中的一些很易被分類專屬條文，所以這裡集中在難歸類的跑車。


  - Chevrolet Corvette
  - SRT Viper
  - Ferrari 458 Italia

<!-- end list -->

  - Lamborghini Gallardo
  - Lotus Europa S
  - [Marcos TSO](../Page/Marcos_TSO.md "wikilink")

<!-- end list -->

  - [Mazda RX-7](../Page/馬自達RX-7.md "wikilink")
  - Mitsubishi 3000GT
  - [Nissan FairladyZ](../Page/日產Z系列.md "wikilink")
  - Noble M400

<!-- end list -->

  - [Porsche
    911](../Page/Porsche_911.md "wikilink")(車系內變化很多，從小型跑車到超跑一應俱全)
  - Porsche Cayman
  - TVR Sagaris
  - TVR Tuscan
  - Venturi Fétish

<File:TVR> blue gray.jpg|TVR Tuscan
[File:TVRSagaris.jpg|TVR](File:TVRSagaris.jpg%7CTVR) Sagaris
[File:NobleM400.jpg|Noble](File:NobleM400.jpg%7CNoble) M400
<File:Nissan> 370Z front-1 20100718.jpg|Nissan 370Z

#### 小型跑車

跑車中的較小和輕巧的產品，標榜的是控制的感覺和靈活性，而不像中檔或超級跑車般強調速度和安全性，雖然一般較平價和大眾化，屬跑車中低端產品，但仍然有少數少量生產的高價貨。

  - [奧迪TT](../Page/奧迪TT.md "wikilink")
  - [豐田86](../Page/豐田86.md "wikilink")
  - [保時捷912](../Page/保時捷912.md "wikilink")
  - [馬自達MX-5](../Page/馬自達MX-5.md "wikilink")
  - [三菱日蝕](../Page/三菱日蝕.md "wikilink")
  - [奧迪A1SPORT](../Page/奧迪A1.md "wikilink")
  - [大眾尚酷](../Page/大眾尚酷.md "wikilink")
  - [梅赛德斯-奔驰SLK级](../Page/梅赛德斯-奔驰SLK级.md "wikilink")
  - [蓮花汽車Elise](../Page/蓮花汽車.md "wikilink")

#### 敞篷車（Convertible）

車頂可自由開起或閉合的車型，有著更高的實用性與遊樂性，享受微風拂面的樂趣。早期，原是獨立開發出來的車型。目前，則是遴選受歡迎的轎車，同時開發出來的車型。另外，敞篷開閉方式也分為手動與電動；車頂類型也分為軟頂與硬頂；乘座人數也分為雙人與四人。
  雙人座（Roadster）

  - Daihatsu Copen
  - [Mazda MX-5](../Page/馬自達MX-5.md "wikilink")
  - [Mercedes-Benz SLK](../Page/SLK.md "wikilink")
  - Mercedes-Benz SL
  - [BMW Z4](../Page/BMW_Z4.md "wikilink")

四人座（Convertible）

  - Fiat Barchetta
  - [Peugeot 206](../Page/Peugeot_206.md "wikilink") CC
  - Volkswagen Eos

<File:1961> Mercedes Benz 190 SL - silver - fvl.jpg|Mercedes-Benz SL
<File:Mercedes> SL 63 AMG Edition IWC (3998335541).jpg|Mercedes-Benz SL
AMG <File:Fiat> Barchetta – Frontansicht, 3. Juni 2011,
Wülfrath.jpg|Fiat Barchetta <File:VW> Eos 1.4 TSI BlueMotion Technology
(Facelift) – Frontansicht, 26. Mai 2011, Velbert.jpg|Volkswagen Eos

#### 超級跑車（Supercar）

跑車中的高端產品但仍然屬於量產型私家車的範圍，趨近極端的引擎性能、超高加速性能、制動能力、車身造型設計特殊、車體製作輕量但仍然穩定的特性。可算是有近乎[賽車性能的跑車](../Page/賽車.md "wikilink")，所以設計上高速行駛時特別注重駕駛安全和操控性能，不能像小型跑車般徹底地輕量化，而且擁有急速剎車的能力，以保障人車的安全。


  - [Audi R8](../Page/Audi_R8.md "wikilink")
  - Ascari KZ1
  - [Bugatti Veyron 16/4](../Page/布佳迪Veyron_16/4.md "wikilink")
  - Dauer 962 LeMans
  - Ferrari Enzo
  - Ferrari F50 GT

<!-- end list -->

  - Ferrari FXX
  - Ford GT
  - Gumpert Apollo
  - [Honda NSX](../Page/本田NSX.md "wikilink")
  - Koenigsegg CCR
  - Lamborghini Aventador

<!-- end list -->

  - Maserati MC12
  - McLaren P1
  - Mega Monte Carlo
  - Mercedes-Benz SLS AMG
  - [Marussia B2](../Page/Marussia_B2.md "wikilink")

<!-- end list -->

  - [Mitsuoka Orochi](../Page/光岡Orochi.md "wikilink")
  - Mosler MT900
  - [Nissan GT-R](../Page/Nissan_GT-R.md "wikilink")
  - Pagani Huayra
  - Porsche Carrera GT
  - Schuppan 962CR

<!-- end list -->

  - Stealth B6
  - Spyker Cars

<File:Dauer> 1994.JPG|Dauer 962 LeMans <File:Mosler> MT900 GTR Gen1 000
2006-2011 frontleft 2012-10-07 A.JPG|Mosler MT900 <File:Ascari> KZ1
II.jpg|Ascari KZ1 <File:Schuppan> 962CR.jpg|Schuppan 962CR <File:Sport>
cars.jpg|Ferrari FXX <File:Stealth> B6 - Flickr - robad0b.jpg|Stealth B6
<File:Chelsea> Auto Legends 2012 (7948646218).jpg|Maserati MC12
<File:Nissan> GT-R 02.JPG|Nissan GT-R

#### 轎跑車

有較大後座空間的跑車，外觀如同雙門的轎車，在追求速度的同時保持舒適性，例如所謂的[肌肉車或](../Page/肌肉車.md "wikilink")[小馬車等](../Page/小馬車.md "wikilink")。

  - [Ford Mustang](../Page/Ford_Mustang.md "wikilink") GT

#### 高性能轎車

類似轎跑車，但多指跑車般性能和風格的四門房車。

  - [BMW M](../Page/BMW_M.md "wikilink")

#### 豪華旅行車

簡稱GT較舒適的跑車通常有後座位。但現時很多廠家都把自己的產品稱為GT，所以其實不過是指不過分像賽車的跑車，所以分類和其他跑車重疊了。

  - [日產GT-R](../Page/日產GT-R.md "wikilink")

#### 運動休旅車（SUV）

把跑車的高性能用在越野車和旅行車上的產物，和傳統跑車最明顯不同是車底較高，所以在轉彎時不像跑車穩定，還多半是[四輪驅動的](../Page/四輪驅動.md "wikilink")，但在崎嶇的路上較安全而通行能力優秀。

  - [保時捷卡宴](../Page/保時捷卡宴.md "wikilink")

## 参考文献

## 外部链接

## 参见

  - [汽車](../Page/汽車.md "wikilink")
  - [交通安全](../Page/交通安全.md "wikilink")
  - [車體風格](../Page/車體風格.md "wikilink")
  - [歐盟新車安全評鑑協會](../Page/歐盟新車安全評鑑協會.md "wikilink")

[汽車種類](../Category/汽車種類.md "wikilink")