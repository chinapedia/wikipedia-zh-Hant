\-{zh-hans:《**赛车总动员**》（英语：Ca-{}-rs）;zh-hk:《**反斗車王**》（英語：Ca-{}-rs）;zh-tw:《**Ca-{}-rs**》，又譯《**車**》、《**汽車總動員**》;}-，由[皮克斯動畫工作室製作](../Page/皮克斯動畫工作室.md "wikilink")，並由[華特迪士尼於](../Page/華特迪士尼影業.md "wikilink")2006年發行的[電腦動畫](../Page/電腦動畫.md "wikilink")[電影](../Page/電影.md "wikilink")。該片在2006年6月9日於[美國](../Page/美國.md "wikilink")、[台灣上映](../Page/台灣.md "wikilink")，隨後在同年7月13日於[香港上映](../Page/香港.md "wikilink")，[中国大陆上映日則为同年](../Page/中国大陆.md "wikilink")8月22日。在台灣上映時，以英文片名「Ca-{}-rs」上檔而無[中文名稱](../Page/中文.md "wikilink")，但[行政院新聞局相關法規指出片名註冊時須為中文](../Page/行政院新聞局.md "wikilink")，故登記時之中文片名為《車》\[1\]，後來當地發行[DVD再另取譯名為](../Page/DVD.md "wikilink")《汽車-{}-總動員》。這是迪士尼／皮克斯第七部的動畫長片，也是在迪士尼買下皮克斯之前，兩家公司舊有合約下的最後一部電影。

電影由之前三部電影《[玩具總動員](../Page/玩具總動員.md "wikilink")》、《[-{zh-hans:虫虫特工队;zh-hk:蟲蟲特工隊;zh-tw:蟲蟲危機;}-](../Page/蟲蟲危機.md "wikilink")》、《[玩具總動員2](../Page/玩具總動員2.md "wikilink")》的導演[约翰·拉萨特](../Page/約翰·雷斯特.md "wikilink")（John
Lasseter）執導，背景設定在一個由[擬人化的汽車和其它車輛所組成的世界](../Page/擬人化.md "wikilink")，並且由[欧文·威尔森](../Page/歐文·威爾森.md "wikilink")（Owen
Wilson）、[保羅·紐曼](../Page/保羅·紐曼.md "wikilink")（Paul Newman）、（Bonnie
Hunt）配音演繹。

## 劇情

[由左至右：-{zh-hans:路霸;zh-hk:磁力霸;zh-sg:路霸;zh-cn:路霸;zh-tw:路霸;}-
、-{zh-hans:赛车马昆;zh-hk:閃電王麥坤;zh-sg:闪电麦坤;zh-cn:闪电麦昆;zh-tw:閃電麥坤;}-、-{zh-hans:冠军;zh-hk:車王;zh-sg:冠军;zh-cn:车王;zh-tw:冠軍;}-。](https://zh.wikipedia.org/wiki/File:Cars_image_2.JPG "fig:由左至右：-{zh-hans:路霸;zh-hk:磁力霸;zh-sg:路霸;zh-cn:路霸;zh-tw:路霸;}- 、-{zh-hans:赛车马昆;zh-hk:閃電王麥坤;zh-sg:闪电麦坤;zh-cn:闪电麦昆;zh-tw:閃電麥坤;}-、-{zh-hans:冠军;zh-hk:車王;zh-sg:冠军;zh-cn:车王;zh-tw:冠軍;}-。")

主角閃電王麥坤（Lightning McQueen）是一輛渴望贏得「活塞杯汽车大赛」（Piston Cup
Championship）的新晉賽車，在賽事的最後一圈與路霸（Chick
Hicks，麥可·基頓飾）與前冠軍-{zh-hant:史崔威勒;zh-cn:斯特里普;}-（"The
King", Strip
Weathers，理察·佩帝飾）同時衝線，三車將於七天後在[加州舉行的附加赛中分出勝負](../Page/加州.md "wikilink")。

麥坤急盼勝出比賽以擺脫舊贊助商和取代前冠軍的地位，為搶先抵達賽車場開始練習而要求接載他的運輸卡車夥伴阿麥通霄行駛。阿麥在路上因倦打盹並被四輛街頭賽車嚇倒，導致麥坤在睡夢中溜出公路。麥坤驚醒後在黑夜中迷路，闖進了冷清的-{zh-tw:油車水鎮;
zh-sg:油车水镇; zh-cn:水箱温泉镇; zh-hk:打冷鎮;}-（Radiator Springs）並破壞了鎮上的主要道路。

麥坤在拖吊車「-{zh-hk:哨牙嘜; zh-tw:拖線; zh-sg:马特;
zh-cn:板牙;}-」（Mater）看守下度過了晚上，第二天被帶到法庭。鎮上的法官兼醫師-{zh-hk:藍天博士;
zh-tw:韓大夫; zh-sg:哈德逊医生; zh-sg:哈德逊博士; zh-cn:哈德逊医生;}-（Doc
Hudson，保羅·紐曼飾）宣判麥坤立即離開本鎮，但鎮上律師-{zh-tw:莎莉; zh-cn:莎莉;
zh-hk:莎莎;}-（Sally Carrera，邦妮·亨特飾）說服-{zh-hk:藍天博士; zh-tw:韓大夫; zh-sg:哈德逊医生;
zh-sg:哈德逊医生; zh-cn:哈德逊医生;}-讓麥坤留在鎮上把被破壞的道路修好。

[mcqueenandhudson.jpg](https://zh.wikipedia.org/wiki/File:mcqueenandhudson.jpg "fig:mcqueenandhudson.jpg")

麥坤不欲花數天時間修好路面，其粗劣的修路過程把路面弄得更糟，但在與-{zh-hk:藍天博士; zh-tw:韓大夫; zh-sg:哈德逊医生;
zh-sg:哈德逊医生;
zh-cn:哈德逊医生;}-進行的打賭比賽中落敗後被迫重新鋪設路面，過程中除與部分鎮上居民成了朋友，也獲悉小鎮因在[66號公路旁而一度繁榮](../Page/66号美国国道.md "wikilink")，但[40號州際公路的啟用導致小鎮的訪客減少而衰落](../Page/美國40號州際公路.md "wikilink")。麥坤也發現-{zh-hk:藍天博士;
zh-tw:韓大夫; zh-sg:哈德逊医生; zh-sg:哈德逊博士;
zh-cn:哈德逊医生;}-乃活塞杯汽车大赛的三屆冠軍得主，但1954年一場意外使他被迫退出並被車迷遺忘。麥坤修好道路後啟發各位居民開始修繕小鎮，但-{zh-hk:藍天博士;
zh-tw:韓大夫; zh-sg:哈德逊医生; zh-sg:哈德逊医生;
zh-cn:哈德逊医生;}-因較早前暗地向媒体透露了麥坤正在鎮上，大批媒體和阿麥湧至小鎮，麥坤匆促下離開小鎮參加比賽。

在比賽中，麥坤因不時分神而落至最後一名，但在看見-{zh-tw:油車水鎮; zh-hk:打冷鎮; zh-sg:油车水镇;
zh-cn:水箱温泉镇;}-部分居民到了賽車場上擔任他的維修團隊而重新振作，逐漸追上並取得領先優勢。在最後一圈時，路霸為免成為最後一名而把-{zh-hant:史崔威勒;zh-cn:斯特里普;}-撞出賽道，導致-{zh-hant:史崔威勒;zh-cn:斯特里普;}-嚴重受損。即將贏得勝利的麥坤憶起-{zh-tw:韓大夫;
zh-sg:哈德逊医生; zh-sg:哈德逊医生;
zh-cn:哈德逊医生;}-的遭遇，在終點線前停下讓路霸取得冠軍，再折回把-{zh-hant:史崔威勒;zh-cn:斯特里普;}-推向衝點。麥坤在大家讚賞其[體育精神時決定拒絕大贊助商的邀約](../Page/體育精神.md "wikilink")，繼續與舊贊助商合作。事後麥坤決定搬到-{zh-tw:油車水鎮;
zh-hk:打冷鎮; zh-sg:油车水镇;
zh-cn:水箱温泉镇;}-建立賽車事業總部以協助小鎮[重新繁榮起來](../Page/Cars_2：世界大賽.md "wikilink")。

## 配音表

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>美國</p></td>
<td><p>台灣</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐文·威爾森.md" title="wikilink">歐文·威爾森</a></p></td>
<td><p><a href="../Page/吳建豪.md" title="wikilink">吳建豪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/陆明君.md" title="wikilink">陆明君</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹尼爾·勞倫斯·懷特尼.md" title="wikilink">丹尼爾·勞倫斯·懷特尼</a></p></td>
<td><p><a href="../Page/康康.md" title="wikilink">康康</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/保羅·紐曼.md" title="wikilink">保羅·紐曼</a></p></td>
<td><p><a href="../Page/佟紹宗.md" title="wikilink">佟紹宗</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/托尼·夏爾赫布.md" title="wikilink">托尼·夏爾赫布</a></p></td>
<td><p><a href="../Page/陳宗岳.md" title="wikilink">陳宗岳</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/切奇·马林.md" title="wikilink">切奇·马林</a></p></td>
<td><p><a href="../Page/胡立成.md" title="wikilink">胡立成</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/姜瑰瑾.md" title="wikilink">姜瑰瑾</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/黎景全.md" title="wikilink">黎景全</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/崔幗夫.md" title="wikilink">崔幗夫</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/约翰·拉岑贝格尔.md" title="wikilink">约翰·拉岑贝格尔</a></p></td>
<td><p><a href="../Page/陳旭昇.md" title="wikilink">陳旭昇</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥可·基頓.md" title="wikilink">麥可·基頓</a></p></td>
<td><p><a href="../Page/孫德成.md" title="wikilink">孫德成</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喬治·卡林.md" title="wikilink">喬治·卡林</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 反響

### 專業評價

好评居多，烂番茄网站新鲜度达74%，平均分为6.9。Metacritic网站的打分为73。

### 票房

美国票房为$244,082,982，全球则为$461,981,604。

## 家庭媒體

### DVD

《汽車總動員》的DVD於2006年10月25日在澳洲和紐西蘭推出，並於11月7日在美國發行。台灣隨後於11月10日上市，英國則於11月27日發行，並同時推出寬螢幕和全螢幕的版本。DVD中收錄的兩隻短片《[-{zh-hans:拖线遇上鬼火;zh-hk:哨牙嘜鬼火驚魂;zh-tw:拖線遇上鬼火;}-](../Page/拖线和鬼火.md "wikilink")》（Mater
and the
Ghostlight）和《[-{zh-hans:单人乐队;zh-hk:壹蚊Show;zh-tw:許願池;}-](../Page/许愿池_\(电影\).md "wikilink")》（One
Man
Band，另一部皮克斯的短片，在戲院於汽車總動員開始前播出），以及故事的靈感起源（一部16分鐘長的紀錄片，關於電影的導演[約翰·拉薩特](../Page/約翰·拉薩特.md "wikilink")）。

與以往皮克斯的DVD不同，這次的DVD僅有單碟版，目前為止（2006年11月）尚未有雙碟版的推出計畫。根據皮克斯DVD製作部門的莎拉·馬赫（Sara
Maher）表示，這是因為約翰·拉薩特和皮克斯正忙於製作下一部電影《[-{zh-hans:料理鼠王;zh-hk:五星級大鼠;zh-tw:料理鼠王;}-](../Page/料理鼠王.md "wikilink")》（*Ratatouille*）\[2\]
，而在DVD的網站上也發表了未曾公開過的片段。\[3\]。

## 後續

### 反斗車王天地

2012年6月15日，美國加州的[迪士尼加州冒險樂園開啟了](../Page/迪士尼加州冒險樂園.md "wikilink")[反斗車王天地](../Page/反斗車王天地.md "wikilink")。內有三個遊戲：

  - 打冷鎮大賽車
  - 路易茲的飛天輪胎
  - 哨牙嘜的垃圾場大露營

## 注釋

<div style="font-size: small">

<references />

</div>

## 外部連結

  - [中国大陆官方网站](http://www.disney.com.cn/minisites/cars/)

  - [迪士尼香港官方網站-Disney.com.hk
    反斗車王官方網站](https://web.archive.org/web/20070513062337/http://www.disney.com.hk/cars/)

  -
  -
  -
  -
  -
  - {{@movies|fcen00317219}}

  -
  -
  -
[Category:汽車總動員](../Category/汽車總動員.md "wikilink")
[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:2000年代動畫電影](../Category/2000年代動畫電影.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:美國喜劇片](../Category/美國喜劇片.md "wikilink")
[Category:美國動畫電影](../Category/美國動畫電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:約翰·拉薩特電影](../Category/約翰·拉薩特電影.md "wikilink")
[Category:擬人化角色電影](../Category/擬人化角色電影.md "wikilink")
[Category:皮克斯動畫](../Category/皮克斯動畫.md "wikilink")
[Category:公路電影](../Category/公路電影.md "wikilink")
[Category:安妮奖最佳动画片](../Category/安妮奖最佳动画片.md "wikilink")
[Category:金球獎最佳動畫長片](../Category/金球獎最佳動畫長片.md "wikilink")
[Category:亞利桑那州背景電影](../Category/亞利桑那州背景電影.md "wikilink")
[Category:華特迪士尼影業電影](../Category/華特迪士尼影業電影.md "wikilink")

1.  [迪士尼 /
    皮克斯【ＣＡＲＳ】](https://disney.lovesakura.com/Special/2006/060528/index.htm)
2.  [Video Business Online report about Cars
    DVD](http://www.videobusiness.com/index.asp?layout=article&articleid=CA6388059)
    Jennifer Netherby / videobusiness.com
3.  [Official Cars DVD
    Website](http://disney.go.com/disneyvideos/animatedfilms/cars/)