[autobots.jpg](https://zh.wikipedia.org/wiki/File:autobots.jpg "fig:autobots.jpg")
**汽车人**（**Autobot**）是《[变形金刚](../Page/变形金刚.md "wikilink")》里的正派角色，和[霸天虎](../Page/霸天虎.md "wikilink")（Decepticon）一样，都是[塞伯坦](../Page/塞伯坦.md "wikilink")（Cybertron）星球上的超级机械生命体。

## 汽车人成员

### [变形金刚](../Page/变形金刚.md "wikilink")（The Transformers）

[变形金刚](../Page/变形金刚.md "wikilink")（The
Transformers）、[变形金刚：大都市发动篇](../Page/变形金刚：大都市发动篇.md "wikilink")（Transformers:
Scramble City）、[变形金刚：大电影](../Page/变形金刚：大电影.md "wikilink")（The
Transformers: The Movie）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus Prime）（集装箱卡车）、辛特龍(sentinel
    prime)
    (消防車)、[救护车](../Page/救护车.md "wikilink")（Ratchet）（救护车）、[刹车](../Page/刹车.md "wikilink")（Skids）（面包车）、[铁皮](../Page/铁皮.md "wikilink")（Ironhide）（面包车）、[吊车](../Page/吊车.md "wikilink")（Grapple）（吊车）、[滑车](../Page/滑车.md "wikilink")（Hoist）（清障拖车）、[红色警报](../Page/红色警报.md "wikilink")（Red
    Alert）（轿车）、[消防车](../Page/消防车.md "wikilink")（Inferno）（消防车）、[幻影](../Page/幻影.md "wikilink")（Mirage）（F1赛车）、[巨浪](../Page/巨浪.md "wikilink")（Beachcomber）（沙漠越野车）、[开路先锋](../Page/开路先锋.md "wikilink")（Trailbreaker）（房车）、[探长](../Page/探长.md "wikilink")（Hound）（吉普越野车）、[战戟](../Page/战戟.md "wikilink")（Warpath）（M551坦克车）、[变速箱](../Page/变速箱.md "wikilink")（Gears）（皮卡）、[充电器](../Page/充电器.md "wikilink")（Windcharger）（轿车）、[大汉](../Page/大汉.md "wikilink")（Brawn）（越野车）、[鲁莽](../Page/鲁莽.md "wikilink")（Huffer）（卡车头）、[大黄蜂](../Page/大黄蜂.md "wikilink")/[金飞虫](../Page/金飞虫.md "wikilink")（Bumblebee/Goldbug）（甲壳虫轿车）、[飞过山](../Page/飞过山.md "wikilink")（Cliffjumper）（轿车）、[爵士](../Page/爵士.md "wikilink")（Jazz）（轿车）、[千斤顶](../Page/千斤顶.md "wikilink")（Wheeljack）（轿车）、[飞毛腿](../Page/飞毛腿.md "wikilink")（Sunstreaker）（轿车）、[横炮](../Page/横炮.md "wikilink")（Sideswipe）（轿车）、[轮胎](../Page/轮胎.md "wikilink")（Tracks）（轿车）、[警车](../Page/警车.md "wikilink")（Prowl）（轿车）、[蓝霹雳](../Page/蓝霹雳.md "wikilink")（Bluestreak）（轿车）、[烟幕](../Page/烟幕.md "wikilink")（Smokescreen）（轿车）

<!-- end list -->

  - [背离](../Page/背离.md "wikilink")（Swerve）（皮卡）、[挡板](../Page/挡板.md "wikilink")（Tailgate）（轿车）、[腹地](../Page/腹地.md "wikilink")（Outback）（越野车）、[管子](../Page/管子.md "wikilink")（Pipes）（卡车头）

<!-- end list -->

  - [热破](../Page/热破.md "wikilink")/[补天士](../Page/补天士.md "wikilink")（Rod
    Hot/Rodimus Prime）（轿车/卡车）、[通天晓](../Page/通天晓.md "wikilink")（Ultra
    Magnus）（开放式货柜卡车）、[阿尔茜](../Page/阿尔茜.md "wikilink")（Arcee）（轿车）、[杯子](../Page/杯子.md "wikilink")（Kup）（皮卡）、[罗嗦](../Page/罗嗦.md "wikilink")（Blurr）（轿车）、[转轮](../Page/转轮.md "wikilink")（Wheelie）（轿车）、[沙漠风暴](../Page/沙漠风暴.md "wikilink")（Sandstorm）（沙漠越野车/直升机）、[弹簧](../Page/弹簧.md "wikilink")（Springer）（装甲车/直升机）

<!-- end list -->

  - [女汽车人](../Page/女汽车人.md "wikilink")（Female
    Autobot）：[艾丽塔](../Page/艾丽塔.md "wikilink")（Elita
    One）（轿车）、[克劳莉亚](../Page/克劳莉亚.md "wikilink")（Chromia）（轿车）、[火翼星](../Page/火翼星.md "wikilink")（Firestar）（皮卡）、[月娇](../Page/月娇.md "wikilink")（Moonracer）（轿车）、[绿光](../Page/绿光.md "wikilink")（Greenlight）（轿车）、[枪兵](../Page/枪兵.md "wikilink")（Lancer）（气垫船）

<!-- end list -->

  - [机器恐龙](../Page/机器恐龙.md "wikilink")（Dinobot）：[钢索](../Page/钢索.md "wikilink")（Grimlock）（霸王龙）、[铁渣](../Page/铁渣.md "wikilink")（Slag）（角龙）、[淤泥](../Page/淤泥.md "wikilink")（Sludge）（长颈龙）、[嚎叫](../Page/嚎叫.md "wikilink")（Snarl）（剑龙）、[飞镖](../Page/飞镖.md "wikilink")（Swoop）（翼龙）

<!-- end list -->

  - [调节车](../Page/调节车.md "wikilink")（Throttlebot）：[高速路](../Page/高速路.md "wikilink")（Freeway）（轿车）、[宽载](../Page/宽载.md "wikilink")（Wideload）（翻斗车）、[路障](../Page/路障.md "wikilink")（Rollbar）（吉普越野车）、[探照灯](../Page/探照灯.md "wikilink")（Searchlight）（轿车）、[追捕](../Page/追捕.md "wikilink")（Chase）（轿车）

<!-- end list -->

  - [飞行太保](../Page/飞行太保.md "wikilink")（Aerialbot）：[大无畏](../Page/大无畏.md "wikilink")（Superion）、[银剑](../Page/银剑.md "wikilink")（Sliverbolt）（协和客机）、[弹弓](../Page/弹弓.md "wikilink")（Slingshot）（鹞式战斗机）、[飞火](../Page/飞火.md "wikilink")（Fireflight）（F4战斗机）、[俯冲](../Page/俯冲.md "wikilink")（Skydive）（F16战斗机）、[空袭](../Page/空袭.md "wikilink")（Air
    Raid）（F15战斗机）

<!-- end list -->

  - [机器卫兵](../Page/机器卫兵.md "wikilink")（Protectobot）：[守护神](../Page/守护神.md "wikilink")（Defensor）、[热点](../Page/热点.md "wikilink")（Hot
    Sopt）（消防车）、[车辙](../Page/车辙.md "wikilink")（Groove）（摩托车）、[大街](../Page/大街.md "wikilink")（Streetwise）（轿车）、[刀刃](../Page/刀刃.md "wikilink")（Blades）（直升机）、[急救员](../Page/急救员.md "wikilink")（First
    Aid）（救护车）

<!-- end list -->

  - [神风队](../Page/神风队.md "wikilink")（Technobot）：[计算王](../Page/计算王.md "wikilink")（Computron）、[激光炮](../Page/激光炮.md "wikilink")（Scattershot）（战斗机/炮）、[钢鞭](../Page/钢鞭.md "wikilink")（Strafe）（战斗机）、[光速](../Page/光速.md "wikilink")（Lightspeed）（战斗机）、[烙铁](../Page/烙铁.md "wikilink")（Afterburner）（摩托车）、[钻探机](../Page/钻探机.md "wikilink")（Nosecone）（钻探机）

<!-- end list -->

  - [大都市](../Page/大都市.md "wikilink")（Metroplex）（基地）、[蹦蹦跳](../Page/蹦蹦跳.md "wikilink")（Scamper）（轿车）、[猛攻](../Page/猛攻.md "wikilink")（Six
    Gun）（武器）、[班房](../Page/班房.md "wikilink")（Slammer）（坦克）、[钛师傅](../Page/钛师傅.md "wikilink")（Alpha
    Trion）、[贝塔](../Page/贝塔.md "wikilink")（Beta）、[排炮](../Page/排炮.md "wikilink")（Broadside）（航空母舰/战斗机）、[浪花](../Page/浪花.md "wikilink")（Seaspray）（气垫船）、[大力金刚](../Page/大力金刚.md "wikilink")（Omega
    Supreme）（火箭）、[天猫号](../Page/天猫号.md "wikilink")（Sky
    Lynx）（始祖鸟/美洲豹/航天飞机）、[天火](../Page/天火.md "wikilink")（Jetfire）（航天飞机）、[滑翔机](../Page/滑翔机.md "wikilink")（Powerglide）（A10攻击机）、[敌无双](../Page/敌无双.md "wikilink")（Devcon）（宇宙穿梭机）、[宇宙飞碟](../Page/宇宙飞碟.md "wikilink")（Cosmos）（宇宙飞碟）、[感知器](../Page/感知器.md "wikilink")（Perceptor）（显微镜）、[录音机](../Page/录音机.md "wikilink")（Blaster）（录音机）、[发条](../Page/发条.md "wikilink")（Rewind）（磁带）、[喷射](../Page/喷射.md "wikilink")（Eject）（磁带）、[钢钳](../Page/钢钳.md "wikilink")（Steeljaw）（磁带）、[犀牛](../Page/犀牛.md "wikilink")（Ramhorn）（磁带）

### [变形金刚：头领战士](../Page/变形金刚：头领战士.md "wikilink")（Transforemrs: The Headmasters）

  - [出击](../Page/出击.md "wikilink")/[反击](../Page/反击.md "wikilink")
    （Punch/Counterpunch）（轿车）

<!-- end list -->

  - [飞车](../Page/飞车.md "wikilink")（Trainbot）：[风行者](../Page/风行者.md "wikilink")（Rail
    Racer）、[雪原号](../Page/雪原号.md "wikilink")（Yukikaze）（火车）、[山岳号](../Page/山岳号.md "wikilink")（Seizan）（火车）、[神力号](../Page/神力号.md "wikilink")（Kaen）（火车）、[极光号](../Page/极光号.md "wikilink")（Shouki）（火车）、[夜行号](../Page/夜行号.md "wikilink")（Getsuei）（火车）、[原野号](../Page/原野号.md "wikilink")（Suiken）（火车）

<!-- end list -->

  - [怪兽](../Page/怪兽.md "wikilink")（Monsterbot）：[双头龙](../Page/双头龙.md "wikilink")（Doublecross）（双头龙）、[剑齿虎](../Page/剑齿虎.md "wikilink")（Grotusque）（剑齿虎）、[暴乱兽](../Page/暴乱兽.md "wikilink")（Repugnus）（怪兽）

<!-- end list -->

  - [头领战士](../Page/头领战士.md "wikilink")（Headmaster）：[巨无霸](../Page/巨无霸.md "wikilink")（Fortress
    Maximus）（战舰）、[老顽固](../Page/老顽固.md "wikilink")（Hardhead）（坦克）、[电脑怪杰](../Page/电脑怪杰.md "wikilink")（Chromedome）（轿车）、[海龙](../Page/海龙.md "wikilink")（Highbrow）（直升机）、[小诸葛](../Page/小诸葛.md "wikilink")（Brainstorm）（战斗机）

<!-- end list -->

  - [目标战士](../Page/目标战士.md "wikilink")（Targetmaster）：[布兰卡](../Page/布兰卡.md "wikilink")（Pointblank）（轿车）、[克罗斯](../Page/克罗斯.md "wikilink")（Crosshairs）（沙漠越野车）、[煞特](../Page/煞特.md "wikilink")（Sureshot）（沙漠越野车）

<!-- end list -->

  - [克隆](../Page/克隆.md "wikilink")（Clone）：[浪子](../Page/浪子.md "wikilink")（Fastlane）（轿车）、[腾云](../Page/腾云.md "wikilink")（Cloudraker）（战斗机）

<!-- end list -->

  - [双履带](../Page/双履带.md "wikilink")（Twincast）（录音机）

### [变形金刚：超神战士战队](../Page/变形金刚：超神战士战队.md "wikilink")（Transfoermsers: Super God Masterforce）

  - [神力战士](../Page/神力战士.md "wikilink")（Godmaster）：[仁莱](../Page/仁莱.md "wikilink")/[超级仁莱](../Page/超级仁莱.md "wikilink")/[超神仁莱](../Page/超神仁莱.md "wikilink")（Ginrai/Super
    Ginrai/God
    Ginrai）（卡车头/集装箱卡车）、[格雷](../Page/格雷.md "wikilink")（Lightfoot）（轿车）、[乔伊](../Page/乔伊.md "wikilink")（Ranger）（沙漠越野车）、[史迪](../Page/史迪.md "wikilink")（Road
    King）（F1赛车）、[快枪手](../Page/快枪手.md "wikilink")（Clouder）（导弹车）

<!-- end list -->

  - [头领战士](../Page/头领战士.md "wikilink")（Headmaster）：巨无霸[格兰](../Page/格兰.md "wikilink")（Grand
    Maximus）（战舰）

:\* [少年头领战士](../Page/少年头领战士.md "wikilink")（Headmaster
Junior）：[特警号](../Page/特警号.md "wikilink")（Shuta
Go）（轿车）、[烈火](../Page/烈火.md "wikilink")（Minerva）（救护车）、[水龙头](../Page/水龙头.md "wikilink")（Cab）（消防车）

  - [隐者](../Page/隐者.md "wikilink")（Pretender）：[汉克](../Page/汉克.md "wikilink")（Metahawk）（战斗机）、[地灵](../Page/地灵.md "wikilink")（Lander）（车）、[浪子](../Page/浪子.md "wikilink")（Diver）（潜水艇）、[神风](../Page/神风.md "wikilink")（Phoenix）（战斗机）

### [变形金刚：胜利之斗争](../Page/变形金刚：胜利之斗争.md "wikilink")（Transfoermsers: Victory）

  - [智者战士](../Page/智者战士.md "wikilink")（Brainmaster）：[史达](../Page/史达.md "wikilink")/[狮王史达](../Page/狮王史达.md "wikilink")（Star
    Saber/Victory Saber）（战斗机/航天飞机）

:\* [路上王](../Page/路上王.md "wikilink")（Road
Caesar）、[布里加](../Page/布里加.md "wikilink")（Blacker）（沙漠越野车）、[布里巴](../Page/布里巴.md "wikilink")（Braver）（轿车）、[拉斯塔](../Page/拉斯塔.md "wikilink")（Laster）（轿车）

  - [微型战士](../Page/微型战士.md "wikilink")（Micromaster）：[瑞克](../Page/瑞克.md "wikilink")（Riker）、[璐琦](../Page/璐琦.md "wikilink")（Clipper）（轿车）

:\* [救援队](../Page/救援队.md "wikilink")（Rescue Patrol
Team）：[路路通](../Page/路路通.md "wikilink")（Holi）（轿车）、[冲锋号](../Page/冲锋号.md "wikilink")（Fire）（消防车）、[黑面神](../Page/黑面神.md "wikilink")（Pipo）（救护车）、[探子号](../Page/探子号.md "wikilink")（Boater）（水翼船）

  - [变体战队](../Page/变体战队.md "wikilink")（Multiforce）：[飞遥翼](../Page/飞遥翼.md "wikilink")（Landcross）

:\*
[飞翼](../Page/飞翼.md "wikilink")（Wingwaver）、[翅膀](../Page/翅膀.md "wikilink")（Wing）（战斗机）、[摇摆](../Page/摇摆.md "wikilink")（Waver）（水翼船）

:\*
[疾风](../Page/疾风.md "wikilink")（Dashtacker）、[冲撞](../Page/冲撞.md "wikilink")（Dash）（轿车）、[钉子](../Page/钉子.md "wikilink")（Tacker）（卡车）

:\*
[向风](../Page/向风.md "wikilink")（Machtackle）、[马赫](../Page/马赫.md "wikilink")（Mach）（航天飞机）、[滑轮](../Page/滑轮.md "wikilink")（Tackle）（半履带车）

  - [六面兽](../Page/六面兽.md "wikilink")（Greatshot）（犀牛/坦克/越野车/航天飞机/枪）、[狮王](../Page/狮王.md "wikilink")（Victory
    Leo）（狮子/航天飞机）、[冲天炮](../Page/冲天炮.md "wikilink")（Galaxy Shuttle）（航天飞机）

### [变形金刚：地带](../Page/变形金刚：地带.md "wikilink")（Transfoermsers: Zone）

  - [超能战士](../Page/超能战士.md "wikilink")（Powered
    Master）：[顶天者](../Page/顶天者.md "wikilink")（Dai
    Atlas）（钻探机/航天飞机/基地）、[音速轰炸](../Page/音速轰炸.md "wikilink")（Sonic
    Bomber）（航天飞机/基地）

<!-- end list -->

  - [微型战士](../Page/微型战士.md "wikilink")（Micromaster）：[月球雷达](../Page/月球雷达.md "wikilink")（Moonradar）（月球车）、[拉比环山](../Page/拉比环山.md "wikilink")（Rabbicrater）（轿车）

:\* [超级车巡逻队](../Page/超级车巡逻队.md "wikilink")：格子（Gingham）（轿车）、黑热（Black
Heat）（轿车）、路抱（Road Hugger）（轿车）、死时（Deadhour）（轿车）

:\* [喷气机巡逻队](../Page/喷气机巡逻队.md "wikilink")：星云（Star
Cloud）（阵风战斗机）、耳语（Whisper）（F22战斗机）、风缘（Windrin）（A10攻击机）、夜航（Nightflight）（F14战斗机）

:\*
[赛车巡逻队](../Page/赛车巡逻队.md "wikilink")：[路行](../Page/路行.md "wikilink")（Roadhandler）（轿车）、[老千](../Page/老千.md "wikilink")（Swindler）（轿车）、[尾旋](../Page/尾旋.md "wikilink")（Tailspin）（轿车）

:\*
[战斗巡逻队](../Page/战斗巡逻队.md "wikilink")：[枪手](../Page/枪手.md "wikilink")（Gunlift）（装甲车）、[爆弹](../Page/爆弹.md "wikilink")（Powerbomb）（装甲车）、[侧轨](../Page/侧轨.md "wikilink")（Sidetrack）（装甲车）、[追日](../Page/追日.md "wikilink")（Sunrunner）（E2预警机）

### [变形金刚：汽车人战记](../Page/变形金刚：汽车人战记.md "wikilink")（Transfoermsers: Car Robots/Transformers: Robots In Disguise）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（消防车）、[通天晓](../Page/通天晓.md "wikilink")（Ultra
    Magnus）（卡车）、[大汉](../Page/大汉.md "wikilink")（X-Brawn）（SUV）、[警车](../Page/警车.md "wikilink")（Prowl）（轿车）、[火线](../Page/火线.md "wikilink")（Side
    Burn）（轿车）、[牵引绳](../Page/牵引绳.md "wikilink")（Tow
    Line）（清障拖车）、[刹车](../Page/刹车.md "wikilink")（Skid-Z）（F1赛车）

<!-- end list -->

  - [飞车队](../Page/飞车队.md "wikilink")（Team Bullet
    Train）：[风行者](../Page/风行者.md "wikilink")（Rail
    Racer）、[轨道钉](../Page/轨道钉.md "wikilink")（Railspike）（火车）、[快车](../Page/快车.md "wikilink")（Rapid
    Run）（火车）、[夜行](../Page/夜行.md "wikilink")（Midnight Express）（火车）

<!-- end list -->

  - [微星](../Page/微星.md "wikilink")（Spy
    Changer）：[激射](../Page/激射.md "wikilink")（Hot
    Shot）（轿车）、[改装](../Page/改装.md "wikilink")（REV）（轿车）、[交叉路](../Page/交叉路.md "wikilink")（Crosswise）（轿车）、[战火](../Page/战火.md "wikilink")（WARS）（轿车）、[幻影](../Page/幻影.md "wikilink")（Mirage）（F1赛车）、[铁皮](../Page/铁皮.md "wikilink")（Ironhide）（皮卡）

<!-- end list -->

  - [工程队](../Page/工程队.md "wikilink")（Built
    Team）：[大力神](../Page/大力神.md "wikilink")（Landfill）、[推土机](../Page/推土机.md "wikilink")（Wedge）（推土机）、[吊钩](../Page/吊钩.md "wikilink")（Hightower）（起重机）、[挖土机](../Page/挖土机.md "wikilink")（Grimlock）（挖掘机）、[翻斗车](../Page/翻斗车.md "wikilink")（Heavy
    Load）（翻斗车）

<!-- end list -->

  - [头领战士](../Page/头领战士.md "wikilink")（Headmaster）：[巨无霸](../Page/巨无霸.md "wikilink")（Fortress
    Maximus）（战舰）、[塞伯罗斯](../Page/塞伯罗斯.md "wikilink")（Emissary）（头）、[斯派克](../Page/斯派克.md "wikilink")（Cerebros）（头）

### [变形金刚：微型传说](../Page/变形金刚：微型传说.md "wikilink")/[变形金刚：雷霆舰队](../Page/变形金刚：雷霆舰队.md "wikilink")（Transfoermsers: Micro Legend/Transformers: Armada）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（集装箱卡车）、[红色警报](../Page/红色警报.md "wikilink")（Red
    Alert）（SUV）、[激射](../Page/激射.md "wikilink")（Hot
    Shot）（轿车）、[烟幕](../Page/烟幕.md "wikilink")/[滑车](../Page/滑车.md "wikilink")（Smokescreen/Hoist）（起重机/挖掘机）、[清道夫](../Page/清道夫.md "wikilink")（Scavenger）（推土机）、[急先锋](../Page/急先锋.md "wikilink")（Blurr）（轿车）、[横炮](../Page/横炮.md "wikilink")（Side
    Swipe）（轿车）

<!-- end list -->

  - [天火](../Page/天火.md "wikilink")（Jetfire）（航天飞机）

### [变形金刚：超能连接](../Page/变形金刚：超能连接.md "wikilink")/[变形金刚：能量晶体](../Page/变形金刚：能量晶体.md "wikilink")（Transfoermsers: Super Link/Transformers: Energon）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（集装箱卡车）、[大力金刚](../Page/大力金刚.md "wikilink")（Omega
    Supreme）（火车/太空船）、[激射](../Page/激射.md "wikilink")（Hot
    Shot）（轿车）、[铁皮](../Page/铁皮.md "wikilink")（Ironhide）（SUV）、[消防车](../Page/消防车.md "wikilink")/[路障](../Page/路障.md "wikilink")（Inferno/Roadblock）（消防车/吊车）

<!-- end list -->

  - [补天士队](../Page/补天士队.md "wikilink")（Team
    Rodimus）：[补天士](../Page/补天士.md "wikilink")（Rodimus）（卡车）、[地雷](../Page/地雷.md "wikilink")（Landmine）（半履带车）、[警车](../Page/警车.md "wikilink")（Browl）（F1赛车）

<!-- end list -->

  - [隔板队](../Page/隔板队.md "wikilink")（Team
    Bulkhead）：[隔板](../Page/隔板.md "wikilink")（Bulkhead）（直升机）、[飞过山](../Page/飞过山.md "wikilink")（Cliffjumper）（沙漠越野车）、[下档](../Page/下档.md "wikilink")（Downshift）（轿车）

<!-- end list -->

  - [航空队](../Page/航空队.md "wikilink")（Air
    Team）：[大无畏](../Page/大无畏.md "wikilink")（Superion
    Maximus）、[暴风雨](../Page/暴风雨.md "wikilink")（Storm
    Jet）（SR71侦察机）、[风刀](../Page/风刀.md "wikilink")（Windrazor）（F22战斗机）、[扫射](../Page/扫射.md "wikilink")（Treadshot）（F22战斗机）、[天影](../Page/天影.md "wikilink")（Sky
    Shadow）（A10攻击机）、[骤降](../Page/骤降.md "wikilink")（Terradive）（A10攻击机）

<!-- end list -->

  - [全向金刚](../Page/全向金刚.md "wikilink")（Omnicon）：[阿尔茜](../Page/阿尔茜.md "wikilink")（Arcee）（摩托车）、[天爆](../Page/天爆.md "wikilink")（Skyblast）（战斗机）、[铁腕](../Page/铁腕.md "wikilink")（Strongarm）（吉普越野车）、[信号弹](../Page/信号弹.md "wikilink")（Signal
    Flare）（半履带车）

<!-- end list -->

  - [锁扣](../Page/锁扣.md "wikilink")（Padlock）、[天火](../Page/天火.md "wikilink")（Jetfire）（航天飞机）、[翼刃](../Page/翼刃.md "wikilink")/[翼剑](../Page/翼剑.md "wikilink")（Wing
    Dagger/Wing Saber）（B2轰炸机）

### [变形金刚：银河之力](../Page/变形金刚：银河之力.md "wikilink")/[变形金刚：塞伯坦传奇](../Page/变形金刚：塞伯坦传奇.md "wikilink")（Transfoermsers: Galaxy Force/Transformers: Cybertron）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（消防车）、[检修/捷狮](../Page/检修/捷狮.md "wikilink")（Overhaul/Leobreaker）（悍马越野车/狮子）、[挡泥板](../Page/挡泥板.md "wikilink")（Mudflap）（起重机）、[地雷](../Page/地雷.md "wikilink")（Landmine）（装载机）、[红色警报](../Page/红色警报.md "wikilink")（Red
    Alert）（SUV/装甲车）、[机关炮](../Page/机关炮.md "wikilink")（Scattorshot）（半履带车/装甲车）、[激射](../Page/激射.md "wikilink")（Hot
    Shot）（轿车/装甲车）、[烟幕](../Page/烟幕.md "wikilink")（Smokescreen）（轿车）

<!-- end list -->

  - [天火](../Page/天火.md "wikilink")（Jetfire）（A224运输机）、[翼剑](../Page/翼剑.md "wikilink")（Wing
    Saber）（A10攻击机）、[真空](../Page/真空.md "wikilink")（Evac）（海豚救援直升机）、[信号灯](../Page/信号灯.md "wikilink")（Signal
    Lancer）（信号灯）

### [变形金刚：活力无限](../Page/变形金刚：活力无限.md "wikilink")（Transformers: Animated）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（消防车）、（航天飞机）、[大黄蜂](../Page/大黄蜂.md "wikilink")（Bumblebee）（轿车）、[隔板](../Page/隔板.md "wikilink")（Bulkhead）（装甲运兵车）、[警车](../Page/警车.md "wikilink")（Prowl）（摩托车）、[救护车](../Page/救护车.md "wikilink")（Rachet）（救护车）

<!-- end list -->

  - [雅典娜队](../Page/雅典娜队.md "wikilink")（Team
    Athenia）：[补天士](../Page/补天士.md "wikilink")（Rodimus
    Prime）（轿车）、[大汉](../Page/大汉.md "wikilink")（Brawn）（半履带车）、[激射](../Page/激射.md "wikilink")（Hot
    Shot）（轿车）、[铁皮](../Page/铁皮.md "wikilink")（Ironhide）（面包车）、[红色警报](../Page/红色警报.md "wikilink")（Red
    Alert）（救护车）

<!-- end list -->

  - [塞伯坦精英卫队](../Page/塞伯坦精英卫队.md "wikilink")（Cybertron Elite
    Guard）：[通天晓](../Page/通天晓.md "wikilink")（Ultra
    Magnus）（战术卡车）、[御天敌](../Page/御天敌.md "wikilink")（Sentinel
    Prime）（皮卡）、[爵士](../Page/爵士.md "wikilink")（Jazz）（轿车）、[罗嗦](../Page/罗嗦.md "wikilink")（Burr）（轿车）、[战戟](../Page/战戟.md "wikilink")（Warpath）（坦克车）、[护卫](../Page/护卫.md "wikilink")（Safeguard）（战斗机）、[天火](../Page/天火.md "wikilink")（Jetfire）（战斗机）、[天雷](../Page/天雷.md "wikilink")（Jetstorm）（战斗机）

<!-- end list -->

  - [塞伯坦科技部](../Page/塞伯坦科技部.md "wikilink")（Cybertron Ministry Of
    Science）：[海隆](../Page/海隆.md "wikilink")（Highbrow）（履带车）、[感知器](../Page/感知器.md "wikilink")（Perceptor）（显微镜）、[千斤顶](../Page/千斤顶.md "wikilink")（Wheeljack）（轿车）

<!-- end list -->

  - [塞伯坦情报局](../Page/塞伯坦情报局.md "wikilink")（Cybertron
    Intelligence）：[主机](../Page/主机.md "wikilink")（Mainframe）（计算机）、[阿尔茜](../Page/阿尔茜.md "wikilink")（Arcee）（轿车）、[飞过山](../Page/飞过山.md "wikilink")（Cliffjumper）（轿车）

<!-- end list -->

  - [高大帅](../Page/高大帅.md "wikilink")（Grandus）、[昙花](../Page/昙花.md "wikilink")（Flareup）、[武玄天](../Page/武玄天.md "wikilink")（Yuketron）、[大力金刚](../Page/大力金刚.md "wikilink")（Omega
    Supreme）

### [变形金刚：领袖之证](../Page/变形金刚：领袖之证.md "wikilink")（Transfoermser: Prime）

  - [领袖卫队](../Page/领袖卫队.md "wikilink")（Team
    Prime）：[擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（卡车头）、[阿尔茜](../Page/阿尔茜.md "wikilink")（Arcee）（摩托车）、[大黄蜂](../Page/大黄蜂.md "wikilink")（Bumblebee）（轿车）、[隔板](../Page/隔板.md "wikilink")（Bulkhead）（皮卡）、[救护车](../Page/救护车.md "wikilink")（Ratchet）（救护车）、[飞过山](../Page/飞过山.md "wikilink")（Cliffjumper）（轿车）、[通天晓](../Page/通天晓.md "wikilink")（Ultra
    Magnus）（卡车头）、[千斤顶](../Page/千斤顶.md "wikilink")（Wheeljack）（轿车）、[烟幕](../Page/烟幕.md "wikilink")（Smokescreen）（轿车）、[击倒](../Page/击倒.md "wikilink")（Knock
    Out）（轿车）

<!-- end list -->

  - [钛师傅](../Page/钛师傅.md "wikilink")（Alpha Trion）

### [变形金刚：救援汽车人](../Page/变形金刚：救援汽车人.md "wikilink")（Transformers: Rescue Bots）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（卡车头）、[大黄蜂](../Page/大黄蜂.md "wikilink")（Bumblebee）（轿车）

<!-- end list -->

  - [救援汽车人](../Page/救援汽车人.md "wikilink")（Rescue
    Bots）：[海潮](../Page/海潮.md "wikilink")（High
    Tide）（起重船/潜水艇）、[打捞](../Page/打捞.md "wikilink")（Salvage）（垃圾车）、[罗嗦](../Page/罗嗦.md "wikilink")（Blurr）（轿车）、[闪影](../Page/闪影.md "wikilink")（Quickshadow）（轿车）

:\* [第17救援队](../Page/第17救援队.md "wikilink")（Rescue Force Sigma
17）：[热浪](../Page/热浪.md "wikilink")（Heatwave）（霸王龙/消防车/消防船）、[刀锋](../Page/刀锋.md "wikilink")（Blades）（翼龙/救援直升机）、[巨石](../Page/巨石.md "wikilink")（Boulder）（角龙/推土机）、[追踪](../Page/追踪.md "wikilink")（Chase）（剑龙/轿车）

### [变形金刚：参乘合体](../Page/变形金刚：参乘合体.md "wikilink")（Transformers Go: Triple Combination）

  - [猎人擎天柱](../Page/猎人擎天柱.md "wikilink")（Hunter Optimus
    Prime）（卡车头）/[Ex擎天柱](../Page/Ex擎天柱.md "wikilink")（Optimus
    Exprime）（龙/火车）

<!-- end list -->

  - [刀剑太保](../Page/刀剑太保.md "wikilink")（Swordbot）：

:\* [忍者队](../Page/忍者队.md "wikilink")（Shinobi
Team）：[豪疾驰](../Page/豪疾驰.md "wikilink")（GoGekisou）/[豪潜水](../Page/豪潜水.md "wikilink")（GoSensui）/[豪飞翔](../Page/豪飞翔.md "wikilink")（GoHishou）、[疾驰丸](../Page/疾驰丸.md "wikilink")（Gekisoumaru）（狮子）、[潜水丸](../Page/潜水丸.md "wikilink")（Sensuimaru）（鲨鱼）、[飞翔丸](../Page/飞翔丸.md "wikilink")（Hishoumaru）（凤凰鸡）

:\* [武士队](../Page/武士队.md "wikilink")（Hishoumaru
Team）：[豪剑斩](../Page/豪剑斩.md "wikilink")（GoKenzan）/[豪岩王](../Page/豪岩王.md "wikilink")（GoGanoh）/[豪迅武](../Page/豪迅武.md "wikilink")（GoJinbu）、[剑斩](../Page/剑斩.md "wikilink")（Kenzan）（轿车）、[岩王](../Page/岩王.md "wikilink")（Ganoh）（消防车）、[迅武](../Page/迅武.md "wikilink")（Jinbu）（前掠翼战斗机）

### [变形金刚：领袖的挑战](../Page/变形金刚：领袖的挑战.md "wikilink")（Transformers: Robots In Disguise）

  - [擎天柱](../Page/擎天柱.md "wikilink")（Optimus
    Prime）（集装箱卡车）、[救护车](../Page/救护车.md "wikilink")（Ratchet）（面包车）、[爵士](../Page/爵士.md "wikilink")（Jazz）（轿车）

<!-- end list -->

  - [蜜蜂队](../Page/蜜蜂队.md "wikilink")（Bee Team）：Ultra
    Bee、[大黄蜂](../Page/大黄蜂.md "wikilink")（Bumblebee）（轿车）、[横炮](../Page/横炮.md "wikilink")（Sideswipe）（轿车）、[漂移](../Page/漂移.md "wikilink")（Drift）（轿车）、[铁腕](../Page/铁腕.md "wikilink")（Strongarm）（SUV）、[钢索](../Page/钢索.md "wikilink")（Grimlock）（霸王龙）

<!-- end list -->

  - [风刃](../Page/风刃.md "wikilink")（Windblade）（垂直起降战斗机）

## 汽车人基地

### [方舟号](../Page/方舟号.md "wikilink")（Ark）

[方舟号是](../Page/方舟号.md "wikilink")[变形金刚](../Page/变形金刚.md "wikilink")（The
Transformers）里位于美国一座火山的汽车人司令部。[方舟号原本为汽车人的航天飞机](../Page/方舟号.md "wikilink")，400万年前坠落于地球。1984年，汽车人在地球苏醒后，成为汽车人的司令部。基地拥有汽车人的超级计算机[显像1号](../Page/显像1号.md "wikilink")（Teletraan
I）。

### [汽车人城](../Page/汽车人城.md "wikilink")（Autobot City）

[汽车人城是](../Page/汽车人城.md "wikilink")[变形金刚：大电影](../Page/变形金刚：大电影.md "wikilink")（The
Transformers: The Movie）、[变形金刚](../Page/变形金刚.md "wikilink")（The
Transformers）和[变形金刚：头领战士](../Page/变形金刚：头领战士.md "wikilink")（Transformers:
The
Headmasters）里位于地球的汽车人司令部。[汽车人城竣工于](../Page/汽车人城.md "wikilink")2005年，拥有汽车人的超级计算机[显像2号](../Page/显像2号.md "wikilink")（Teletraan
II）和用于连接[塞伯坦](../Page/塞伯坦.md "wikilink")（Cybertron）的[太空桥](../Page/太空桥.md "wikilink")（Space
Bridge）。[通天晓](../Page/通天晓.md "wikilink")（Ultra
Magnus）、[录音机](../Page/录音机.md "wikilink")（Blaster）担任[汽车人城的指挥官](../Page/汽车人城.md "wikilink")。[大都市](../Page/大都市.md "wikilink")（Metroplex）可以与[汽车人城连接](../Page/汽车人城.md "wikilink")，并成为[汽车人城的一部分](../Page/汽车人城.md "wikilink")。

### [汽车人基地](../Page/汽车人基地.md "wikilink")（Autobot Base）

[汽车人基地是](../Page/汽车人基地.md "wikilink")[变形金刚：超神战士战队](../Page/变形金刚：超神战士战队.md "wikilink")（Transformers:
Super God
Masterforce）里位于日本的汽车人秘密基地。基地由[汉克](../Page/汉克.md "wikilink")（Metahawk）的好友，[剛秀太的父亲](../Page/剛秀太.md "wikilink")，Haruma
Gō教授打造。为了保密，基地被伪装成一个大山丘。

### [宇宙防卫军本部](../Page/宇宙防卫军本部.md "wikilink")（Galactic Defense Force Headquarters）

[宇宙防卫军本部是](../Page/宇宙防卫军本部.md "wikilink")[变形金刚：胜利之斗争](../Page/变形金刚：胜利之斗争.md "wikilink")（Transformers:
Victory）里位于Victory星（planet Victory）的汽车人指挥中心。

### [塞伯坦基地](../Page/塞伯坦基地.md "wikilink")（Cybertron Base）

[塞伯坦基地是](../Page/塞伯坦基地.md "wikilink")[变形金刚：汽车人战记](../Page/变形金刚：汽车人战记.md "wikilink")（Transformers:
Car Robots/Transformers: Robots In Disguise）里位于地球Metro
City的汽车人秘密基地。基地位于一栋建筑物停车场下面，包括一个大型计算机连接控制中心。基地通过停车场的秘密入口或者[环球太空桥](../Page/环球太空桥.md "wikilink")（global
space bridge）进入。全息影像人工智能系统[爱](../Page/爱.md "wikilink")（T-Ai）常驻于基地。

### [塞伯坦城](../Page/塞伯坦城.md "wikilink")（Cybertron City）

[塞伯坦城是](../Page/塞伯坦城.md "wikilink")[变形金刚：超能连接](../Page/变形金刚：超能连接.md "wikilink")（Transformers:
Super Link）/[变形金刚：能量晶体](../Page/变形金刚：能量晶体.md "wikilink")（Transformers:
Energon）里汽车人位于地球的基地。[塞伯坦城一共有](../Page/塞伯坦城.md "wikilink")5座，分别为：[海洋城](../Page/海洋城.md "wikilink")（Ocean
City）、[丛林城](../Page/丛林城.md "wikilink")（Jungle
City）、[风雪城](../Page/风雪城.md "wikilink")（Blizzard
City）、[平原城](../Page/平原城.md "wikilink")（Plains
Ciry）、[沙漠城](../Page/沙漠城.md "wikilink")（Desert
City）。5座城市都具备开采[能量晶体](../Page/能量晶体.md "wikilink")（energon）的能力。[海洋城是位于海洋的汽车人指挥中心](../Page/海洋城.md "wikilink")，拥有[太空桥](../Page/太空桥.md "wikilink")（space
bridge），能降到海平面以下，当升出海面后，可以通过一系列桥梁和周边陆地连接。

### [汽车人地球基地](../Page/汽车人地球基地.md "wikilink")（Autobot Earth Base）

[汽车人地球基地是](../Page/汽车人地球基地.md "wikilink")[变形金刚：活力无限](../Page/变形金刚：活力无限.md "wikilink")（Transformers:
Animated）里位于地球的汽车人基地。基地由一个旧的废弃工厂改造而成。

### [欧米茄1号](../Page/欧米茄1号.md "wikilink")（Omega One）

[欧米茄1号是](../Page/欧米茄1号.md "wikilink")[变形金刚：领袖之证](../Page/变形金刚：领袖之证.md "wikilink")（Transformers:
Prime）里位于美国内华达州山脉的汽车人秘密基地。基地由[救护车](../Page/救护车.md "wikilink")（Ratchet）驻守，拥有[陆地桥](../Page/陆地桥.md "wikilink")（groundbridge）和[太空桥](../Page/太空桥.md "wikilink")（space
bridge）技术。

### [狮鹫岛消防局指挥中心](../Page/狮鹫岛消防局指挥中心.md "wikilink")（Griffin Rock Firehouse Headquarters）/[救援汽车人训练中心](../Page/救援汽车人训练中心.md "wikilink")（Rescue Bot Training Center）

[狮鹫岛消防局指挥中心是](../Page/狮鹫岛消防局指挥中心.md "wikilink")[变形金刚：救援汽车人](../Page/变形金刚：救援汽车人.md "wikilink")（Transformers:
Rescue Bots）里位于美国缅因州[狮鹫岛](../Page/狮鹫岛.md "wikilink")（Griffin
Rock）的救援汽车人指挥中心。[救援汽车人训练中心是](../Page/救援汽车人训练中心.md "wikilink")[变形金刚：救援汽车人](../Page/变形金刚：救援汽车人.md "wikilink")（Transformers:
Rescue
Bots）里位于英国苏格兰[梅恩兰岛](../Page/梅恩兰岛.md "wikilink")（Mainland）的救援汽车人训练中心。[救援汽车人训练中心大部分建筑位于地下](../Page/救援汽车人训练中心.md "wikilink")，并具备伪装功能，能伪装成普通建筑。[狮鹫岛消防局指挥中心和](../Page/狮鹫岛消防局指挥中心.md "wikilink")[救援汽车人训练中心可以通过](../Page/救援汽车人训练中心.md "wikilink")[陆地桥](../Page/陆地桥.md "wikilink")（groundbridge）进行连接。

### [废料场](../Page/废料场.md "wikilink")（Vintage Salvage Depot for the Discriminating Nostalgist/Scrapyard）

[废料场是](../Page/废料场.md "wikilink")[变形金刚：领袖的挑战](../Page/变形金刚：领袖的挑战.md "wikilink")（Transformers:
Robots In
Disguise）里位于美国皇冠城外的一大片空地。汽车人的监狱舰[艾之墓](../Page/艾之墓.md "wikilink")（
Alchemor）坠落于此。汽车人在[废料厂拥有由](../Page/废料厂.md "wikilink")[迷你金刚](../Page/迷你金刚.md "wikilink")（Minicon）[黑面神](../Page/黑面神.md "wikilink")（Fixit）操控的[陆地桥](../Page/陆地桥.md "wikilink")（groundbridge）。

## 汽车人飞船

### [方舟号](../Page/方舟号.md "wikilink")（Ark）

[方舟号](../Page/方舟号.md "wikilink")（Ark）是[变形金刚](../Page/变形金刚.md "wikilink")（The
Transformers）里汽车人的母舰。在[擎天柱的率领下](../Page/擎天柱.md "wikilink")，[铁皮](../Page/铁皮.md "wikilink")（Ironhide）、[救护车](../Page/救护车.md "wikilink")（Ratchet）、[幻影](../Page/幻影.md "wikilink")（Mirage）、[开路先锋](../Page/开路先锋.md "wikilink")（Trailbreaker）、[探长](../Page/探长.md "wikilink")（Hound）、[变速箱](../Page/变速箱.md "wikilink")（Gears）、[充电器](../Page/充电器.md "wikilink")（Windcharger）、[大汉](../Page/大汉.md "wikilink")（Brawn）、[鲁莽](../Page/鲁莽.md "wikilink")（Huffer）、[大黄蜂](../Page/大黄蜂.md "wikilink")（Bumblebee）、[飞过山](../Page/飞过山.md "wikilink")（Cliffjump）、[爵士](../Page/爵士.md "wikilink")（Jazz）、[千斤顶](../Page/千斤顶.md "wikilink")（Wheeljack）、[飞毛腿](../Page/飞毛腿.md "wikilink")（Sunstreaker）、[横炮](../Page/横炮.md "wikilink")（Sideswipe）、[警车](../Page/警车.md "wikilink")（Prowl）、[蓝霹雳](../Page/蓝霹雳.md "wikilink")（Bluestreak），18位汽车人乘坐[方舟号离开](../Page/方舟号.md "wikilink")[塞伯坦寻找能源](../Page/塞伯坦.md "wikilink")。经过太空激战，飞船坠毁于地球。1984年，汽车人在地球苏醒后，成为汽车人位于地球的基地。

### [艾萨龙号](../Page/艾萨龙号.md "wikilink")（Axalon）

[艾萨龙号](../Page/艾萨龙号.md "wikilink")（Axalon）是[变形金刚：微信传说](../Page/变形金刚：微信传说.md "wikilink")（The
Transformers: Micro
Legend）/[变形金刚：雷霆舰队](../Page/变形金刚：雷霆舰队.md "wikilink")（Transformers:
Armada）里汽车人的飞船。

### [米兰达2号](../Page/米兰达2号.md "wikilink")（Miranda）

[米兰达2号](../Page/米兰达2号.md "wikilink")（Miranda）是[变形金刚：超能连接](../Page/变形金刚：超能连接.md "wikilink")（The
Transformers: Super
Link）/[变形金刚：能量晶体](../Page/变形金刚：能量晶体.md "wikilink")（Transformers:
Energon）里汽车人的飞船。

### [千金锤号](../Page/千金锤号.md "wikilink")（Jackhammer）

[千金锤号](../Page/千金锤号.md "wikilink")（Jackhammer）是[变形金刚：领袖之证](../Page/变形金刚：领袖之证.md "wikilink")（The
Transformers: Prime）里[千斤顶](../Page/千斤顶.md "wikilink")（Wheeljack）的飞船。

### [西格玛号](../Page/西格玛号.md "wikilink")（Sigma）

[西格玛号](../Page/西格玛号.md "wikilink")（Sigma）是[变形金刚：救援汽车人](../Page/变形金刚：救援汽车人.md "wikilink")（The
Transformers: Rescue Bots）里[第17救援队](../Page/第17救援队.md "wikilink")（Rescue
Force Sigma 17）的飞船。