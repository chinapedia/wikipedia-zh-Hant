**汽车灯**為裝設在[汽車上的](../Page/汽車.md "wikilink")[燈具](../Page/燈.md "wikilink")，包括[大燈](../Page/大燈.md "wikilink")（又稱[頭燈](../Page/头灯\(汽车\).md "wikilink")，包括远光灯、近光灯）、位置燈、[刹车灯](../Page/刹车灯.md "wikilink")、[倒車燈](../Page/倒車燈.md "wikilink")、[尾燈](../Page/尾燈.md "wikilink")、[霧燈](../Page/霧燈.md "wikilink")、[牌照燈](../Page/牌照燈.md "wikilink")、[日间行车灯与](../Page/日间行车灯.md "wikilink")[轉向燈](../Page/轉向燈.md "wikilink")。

## 远光灯

夜间在無照明設備的路段(如郊外)使用。当对面来车时应切换为近光灯，以免强光晃眼，影响对方司机视线。

远光灯的照明有效距离应在150米。

## 近光灯

夜間在市區等地方駕駛或雨天等天氣型態駕駛使用。

近光燈的照明有效距離應在30公尺。

## 位置燈

不同的車廠設計的位置也不同。通常此燈包含在頭燈組裡(或用於方向燈)。

此燈亮度不高，適用在天氣良好的晨曦或傍晚時段。

將頭燈旋鈕撥到此段時，尾燈、牌照燈會跟位置燈一起亮。

## 刹车灯

刹车灯为红色。

## 日间行车灯

白天天气晴好时是否需要开车灯，不同的国家有不同的规定。正方认为此时开车灯不是为了自己要看清道路，而是要别人能看清自己，及时避让，减少事故发生率；反方认为没有足够数据证明它提高了安全性，反而更耗能源。当前鼓励甚至强制白天开车灯的国家有：瑞典、德国、法国、加拿大、美国等。

## 转向灯

[Turnsignals_On.jpg](https://zh.wikipedia.org/wiki/File:Turnsignals_On.jpg "fig:Turnsignals_On.jpg")

轉向燈，亦稱指揮燈、方向指示燈或方向燈，通常為[橘色](../Page/橘色.md "wikilink")，在部份國家允許[紅色](../Page/紅色.md "wikilink")。通常安裝於車輛的四個邊角，部份新款車輛在[後照鏡的鏡殼也安裝方向燈](../Page/後照鏡.md "wikilink")。

轉向燈的[作用](../Page/作用.md "wikilink")，在於在[轉彎時讓一側的](../Page/轉彎.md "wikilink")[燈](../Page/燈.md "wikilink")[點滅](../Page/點滅.md "wikilink")[閃爍以提示自己將要轉彎的](../Page/閃爍.md "wikilink")[方向的](../Page/方向.md "wikilink")[裝置](../Page/裝置.md "wikilink")，亦稱方向指示燈。[臺灣則稱作方向燈](../Page/臺灣.md "wikilink")。除轉彎外，轉向燈還用在[起步](../Page/起步.md "wikilink")、[超車和變更](../Page/超車.md "wikilink")[行車線的時候](../Page/行車線.md "wikilink")。於停着車上下[乘客以及突然](../Page/乘客.md "wikilink")[拋錨之情形下](../Page/拋錨.md "wikilink")，還常使左右方向的轉向燈同時閃爍，以引起其他行駛[車輛](../Page/車輛.md "wikilink")[注意](../Page/注意.md "wikilink")。

為避免[駕駛於轉向後忘記關閉方向燈](../Page/駕駛.md "wikilink")，以致誤導其他車輛，多数汽车装有方向灯开关随方向盘自动回位的功能。方向燈閃爍時車內會發出[同步](../Page/同步.md "wikilink")[聲響](../Page/聲響.md "wikilink")，以提醒駕駛。大型車的方向燈會在車外同一邊發出聲響以提醒車旁邊的人注意。

[Category:汽车部件](../Category/汽车部件.md "wikilink")