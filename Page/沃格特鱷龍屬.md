**沃格特鱷龍屬**（[學名](../Page/學名.md "wikilink")：*Walgettosuchus*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，目前狀態是[疑名](../Page/疑名.md "wikilink")。

在1905年，[澳洲](../Page/澳洲.md "wikilink")[南威爾士的沃格特鎮](../Page/南威爾士.md "wikilink")（Walgett）附近的[閃電山脊](../Page/閃電山脊.md "wikilink")，發現一些成為[蛋白石的獸腳類脊椎化石](../Page/蛋白石.md "wikilink")。化石被送到[大英自然史博物館](../Page/自然史博物館_\(倫敦\).md "wikilink")，由[亞瑟·史密斯·伍德沃德](../Page/亞瑟·史密斯·伍德沃德.md "wikilink")（Arthur
Smith Woodward）進行研究\[1\]

在1932年，[休尼](../Page/休尼.md "wikilink")（Friedrich von
Huene）將這些不完整的尾巴[脊椎](../Page/脊椎.md "wikilink")（編號BMNH
R3717）命名為沃格特鱷龍，其屬名的意思是「沃格特的[鱷魚](../Page/鱷魚.md "wikilink")」，種名則是以伍德沃德為名。在當時，休尼命名恐龍時常使用*suchus*字尾，而非*saurus*字尾，因為休尼認為恐龍是鱷魚的近親，而離[蜥蜴是遠親](../Page/蜥蜴.md "wikilink")。

[正模標本](../Page/正模標本.md "wikilink")（編號**BMNH R3717**）發現於的Griman
Creek組地層，地質年代屬於[下白堊紀的](../Page/下白堊紀.md "wikilink")[阿爾布階](../Page/阿爾布階.md "wikilink")。這些[尾椎椎體長](../Page/尾椎.md "wikilink")6.3公分，椎體前後端有凹處\[2\]。不知為何\[3\]，休尼認為牠有[前關節突](../Page/前關節突.md "wikilink")\[4\]。休尼亦建議若發現更多的遺骸，他有把握證實沃格特鱷龍是[盜龍的](../Page/盜龍.md "wikilink")[異名](../Page/異名.md "wikilink")，另一種發現於閃電山脊的[虛骨龍類](../Page/虛骨龍類.md "wikilink")\[5\]。

在1990年，Ralph
Molnar指沃格特鱷龍[正模標本尾椎](../Page/正模標本.md "wikilink")，與[似鳥龍科或](../Page/似鳥龍科.md "wikilink")[異特龍科的尾椎沒有分別](../Page/異特龍科.md "wikilink")，因此認為牠是獸腳亞目的分類未定屬，狀態是個疑名\[6\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")
[Category:堅尾龍類](../Category/堅尾龍類.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:大洋洲恐龍](../Category/大洋洲恐龍.md "wikilink")

1.  A.S. Woodward, 1910, "On remains of a megalosaurian dinosaur from
    New South Wales", *Report of the British Association for the
    Advancement of Science* **79**: 482-483

2.

3.

4.
5.
6.