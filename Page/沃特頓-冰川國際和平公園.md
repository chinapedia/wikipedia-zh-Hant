**沃特頓-冰川國際和平公園**（**Waterton-Glacier International Peace
Park**）是[北美兩個大型](../Page/北美.md "wikilink")[國家公園](../Page/國家公園.md "wikilink")——[加拿大的](../Page/加拿大.md "wikilink")**[沃特頓湖群國家公園](../Page/沃特頓湖群國家公園.md "wikilink")**（Waterton
Lakes National
Park）和[美國的](../Page/美國.md "wikilink")**[冰川国家公园](../Page/冰川国家公园_\(美国\).md "wikilink")**（Glacier
National
Park）的并稱。兩個國家公園都被[聯合國教科文組織宣布為](../Page/聯合國教科文組織.md "wikilink")[生物圈保護區](../Page/生物圈保護區.md "wikilink")，并聯合成為一處[世界自然遺產](../Page/世界自然遺產.md "wikilink")。

該國際和平公園經過來自[艾伯塔省和](../Page/艾伯塔省.md "wikilink")[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[扶轮社成員的不懈努力](../Page/扶轮社.md "wikilink")，終于在1932年得以成立。是世界上第一個國際和平公園，象征著美加兩國的和平與友誼。成立時由查爾斯·亞瑟·孟德爵士（[Sir
Charles Arthur
Mander](../Page/:en:Charles_Arthur_Mander.md "wikilink")）致辭\[1\]。

该公园由于夜空资源保护完好，2017年被[国际暗天协会指定为国际暗天公园](../Page/国际暗天协会.md "wikilink")\[2\]。

## 參見

  - [沃特頓湖群國家公園](../Page/沃特頓湖群國家公園.md "wikilink")
  - [冰川国家公园](../Page/冰川国家公园_\(美国\).md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [世界遺產組織上的介紹](http://whc.unesco.org/pg.cfm?cid=31&id_site=354)
  - [冰川国家公园圖集](https://web.archive.org/web/20070930060130/http://glacier-national-park.psoai.com/glacier-national-park-photograph.html)

[Category:加拿大世界遺產](../Category/加拿大世界遺產.md "wikilink")
[Category:美國世界遺產](../Category/美國世界遺產.md "wikilink")
[Category:落基山脉历史](../Category/落基山脉历史.md "wikilink")

1.  [和平公園](http://www.iloveparks.com/peaceparks/)
2.