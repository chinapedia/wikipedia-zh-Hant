**沃罗涅日州**（），是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[中央聯邦管區](../Page/中央聯邦管區.md "wikilink")。面積52,400平方公里，人口2,378,803
(2002年)。首府[沃罗涅日](../Page/沃罗涅日.md "wikilink")，距離首都[莫斯科](../Page/莫斯科.md "wikilink")582公里。该州与乌克兰接壤。

沃罗涅日州是1958年[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")[帕维尔·阿列克谢耶维奇·切连科夫的出生地](../Page/帕维尔·阿列克谢耶维奇·切连科夫.md "wikilink")。

[Don_(Voronezh_Oblast).jpg](https://zh.wikipedia.org/wiki/File:Don_\(Voronezh_Oblast\).jpg "fig:Don_(Voronezh_Oblast).jpg")

## 行政区划

### 区域

| 中文名                                          | 俄文名                   | 行政中心                                             |
| -------------------------------------------- | --------------------- | ------------------------------------------------ |
| [安纳区](../Page/安纳区.md "wikilink")             | Аннинский район       | [安纳](../Page/安纳.md "wikilink")                   |
| [博布罗夫区](../Page/博布罗夫区.md "wikilink")         | Бобровский район      | [博布罗夫](../Page/博布罗夫.md "wikilink")               |
| [博古恰尔区](../Page/博古恰尔区.md "wikilink")         | Богучарский район     | [博古恰尔](../Page/博古恰尔.md "wikilink")               |
| [布图尔利诺夫卡区](../Page/布图尔利诺夫卡区.md "wikilink")   | Бутурлиновский район  | [布图尔利诺夫卡](../Page/布图尔利诺夫卡.md "wikilink")         |
| [上马蒙区](../Page/上马蒙区.md "wikilink")           | Верхнемамонский район | [上马蒙](../Page/上马蒙.md "wikilink")                 |
| [上哈瓦区](../Page/上哈瓦区.md "wikilink")           | Верхнехавский район   | [上哈瓦](../Page/上哈瓦.md "wikilink")                 |
| [沃罗比约夫卡区](../Page/沃罗比约夫卡区.md "wikilink")     | Воробьёвский район    | [沃罗比约夫卡](../Page/沃罗比约夫卡.md "wikilink")           |
| [格里巴诺夫斯基区](../Page/格里巴诺夫斯基区.md "wikilink")   | Грибановский район    | [格里巴诺夫斯基](../Page/格里巴诺夫斯基.md "wikilink")         |
| [卡拉切耶夫斯基区](../Page/卡拉切耶夫斯基区.md "wikilink")   | Калачеевский район    | [卡拉奇](../Page/卡拉奇_\(沃羅涅日州\).md "wikilink")       |
| [卡缅卡区](../Page/卡缅卡区_\(沃罗涅日州\).md "wikilink") | Каменский район       | [卡缅卡](../Page/卡缅卡.md "wikilink")                 |
| [坎捷米罗夫卡区](../Page/坎捷米罗夫卡区.md "wikilink")     | Кантемировский район  | [坎捷米罗夫卡](../Page/坎捷米罗夫卡.md "wikilink")           |
| [卡希尔斯科耶区](../Page/卡希尔斯科耶区.md "wikilink")     | Каширский район       | [卡希尔斯科耶](../Page/卡希尔斯科耶.md "wikilink")           |
| [利斯基区](../Page/利斯基区.md "wikilink")           | Лискинский район      | [利斯基](../Page/利斯基.md "wikilink")                 |
| [下杰维茨克区](../Page/下杰维茨克区.md "wikilink")       | Нижнедевицкий район   | [下杰维茨克](../Page/下杰维茨克.md "wikilink")             |
| [新乌斯曼区](../Page/新乌斯曼区.md "wikilink")         | Новоусманский район   | [新乌斯曼](../Page/新乌斯曼.md "wikilink")               |
| [新霍皮奥尔斯克区](../Page/新霍皮奥尔斯克区.md "wikilink")   | Новохопёрский район   | [新霍皮奥尔斯克](../Page/新霍皮奥尔斯克.md "wikilink")         |
| [奥利霍瓦特卡区](../Page/奥利霍瓦特卡区.md "wikilink")     | Ольховатский район    | [奥利霍瓦特卡](../Page/奥利霍瓦特卡.md "wikilink")           |
| [奥斯特罗戈日斯克区](../Page/奥斯特罗戈日斯克区.md "wikilink") | Острогожский район    | [奥斯特罗戈日斯克](../Page/奥斯特罗戈日斯克.md "wikilink")       |
| [巴甫洛夫斯克区](../Page/巴甫洛夫斯克区.md "wikilink")     | Павловский район      | [巴甫洛夫斯克](../Page/巴甫洛夫斯克_\(沃罗涅日州\).md "wikilink") |
| [帕尼诺区](../Page/帕尼诺区.md "wikilink")           | Панинский район       | [帕尼诺](../Page/帕尼诺.md "wikilink")                 |
| [彼得罗巴甫洛夫卡区](../Page/彼得罗巴甫洛夫卡区.md "wikilink") | Петропавловский район | [彼得罗巴甫洛夫卡](../Page/彼得罗巴甫洛夫卡.md "wikilink")       |
| [波沃里诺区](../Page/波沃里诺区.md "wikilink")         | Поворинский район     | [波沃里诺](../Page/波沃里诺.md "wikilink")               |
| [波德戈连斯基区](../Page/波德戈连斯基区.md "wikilink")     | Подгоренский район    | [波德戈连斯基](../Page/波德戈连斯基.md "wikilink")           |
| [拉蒙区](../Page/拉蒙区.md "wikilink")             | Рамонский район       | [拉蒙](../Page/拉蒙.md "wikilink")                   |
| [列皮约夫卡区](../Page/列皮约夫卡区.md "wikilink")       | Репьёвский район      | [列皮约夫卡](../Page/列皮约夫卡.md "wikilink")             |
| [罗索希区](../Page/罗索希区.md "wikilink")           | Россошанский район    | [罗索希](../Page/罗索希_\(罗索希区\).md "wikilink")        |
| [谢米卢基区](../Page/谢米卢基区.md "wikilink")         | Семилукский район     | [谢米卢基](../Page/谢米卢基.md "wikilink")               |
| [塔洛瓦亚区](../Page/塔洛瓦亚区.md "wikilink")         | Таловский район       | [塔洛瓦亚](../Page/塔洛瓦亚.md "wikilink")               |
| [捷尔诺夫卡区](../Page/捷尔诺夫卡区.md "wikilink")       | Терновский район      | [捷尔诺夫卡](../Page/捷尔诺夫卡.md "wikilink")             |
| [霍霍利斯基区](../Page/霍霍利斯基区.md "wikilink")       | Хохольский район      | [霍霍利斯基](../Page/霍霍利斯基.md "wikilink")             |
| [埃尔季利区](../Page/埃尔季利区.md "wikilink")         | Эртильский район      | [埃尔季利](../Page/埃尔季利.md "wikilink")               |

## 注释

## 参考文献

[\*](../Category/沃羅涅日州.md "wikilink")
[Category:中央聯邦管區](../Category/中央聯邦管區.md "wikilink")