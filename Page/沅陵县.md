**沅陵县**位于[湖南省西部](../Page/湖南.md "wikilink")，[怀化市北部](../Page/怀化.md "wikilink")。[汉](../Page/西汉.md "wikilink")[高祖五年](../Page/汉高祖.md "wikilink")（前202年）始置沅陵县至今，为湖南省辖域面积最大的[县](../Page/县.md "wikilink")。南北宽106.6千米，东西99.5千米。东与[桃源](../Page/桃源縣.md "wikilink")、[安化相连](../Page/安化.md "wikilink")，南与[溆浦](../Page/溆浦.md "wikilink")、[辰溪接壤](../Page/辰溪.md "wikilink")，西与[泸溪](../Page/泸溪.md "wikilink")、[古丈](../Page/古丈.md "wikilink")、[永顺毗邻](../Page/永顺.md "wikilink")，北与[张家界市交界](../Page/张家界市.md "wikilink")。
辖域总面积5,825平方公里，[国内生产总值](../Page/国内生产总值.md "wikilink")155.78亿元（2013年）\[1\]，总人口62.2万人（2004年），其中农业人口55.1万人。[县治](../Page/县治.md "wikilink")[沅陵镇](../Page/沅陵镇.md "wikilink")。

[沅水由西南入境](../Page/沅水.md "wikilink")，向东注入[洞庭湖](../Page/洞庭湖.md "wikilink")，横贯县境中央，将全县分成南、北两部分，沅水以北属[武陵山系](../Page/武陵山.md "wikilink")，沅水以南属[雪峰山系](../Page/雪峰山.md "wikilink")。

## 历史沿革

春秋时期属楚巫中地，战国属楚黔中地，秦属[黔中郡](../Page/黔中郡.md "wikilink")。[汉高祖五年](../Page/汉高祖.md "wikilink")(公元前202年)始置沅陵县，属[武陵郡](../Page/武陵郡.md "wikilink")。汉[高后元年](../Page/高后.md "wikilink")(前187年)封[长沙王子吴阳为沅陵顷侯](../Page/长沙王.md "wikilink")。新莽时改沅陵县为沅陆县。[东汉时](../Page/东汉.md "wikilink")，县属荆州[武陵郡](../Page/武陵郡.md "wikilink")。三国时先属蜀，后属吴。齐置武陵都尉府于沅陵。陈[天嘉元年](../Page/天嘉.md "wikilink")(公元560年)，县改属沅州通宁郡。[太建七年](../Page/太建.md "wikilink")(575年)，改通宁郡为沅陵郡，治沅陵．隋[开皇九年](../Page/开皇.md "wikilink")(589年)，改置辰州，治沅陵。[大业二年](../Page/大业.md "wikilink")(606年)，复改辰州为沅陵郡，隶[荆州](../Page/荆州.md "wikilink")。唐[武德二年](../Page/武德.md "wikilink")(619年)，沅陵郡改为[辰州](../Page/辰州.md "wikilink")，县属辰州。唐[贞观元年](../Page/贞观.md "wikilink")(627年)，分天下为10道，县属辰州，隶[江南道](../Page/江南道.md "wikilink")。[开元二十一年](../Page/开元.md "wikilink")(733年)分天下为15道，县属辰州，隶黔中道。宋隶[荆湖北路](../Page/荆湖北路.md "wikilink")。元改辰州为辰州路，治沅陵，隶湖广行中书省。明县属辰州府，隶[湖广布政使司](../Page/湖广布政使司.md "wikilink")。清县仍属辰州府，隶[辰沅永靖道](../Page/辰沅永靖道.md "wikilink")。民国元年(1912年)，废沅陵县存辰州府，府仅辖沅陵县地。3年裁府恢复沅陵县，属辰沅道。
民国24年7月，设湘西绥靖处于沅陵。辖19县。1950年1月1日，湖南省临时人民政府在沅陵设置湘西行政公署，辖沅陵、[会同](../Page/会同.md "wikilink")、[永顺](../Page/永顺.md "wikilink")3个专区，县属沅陵专区。1952年8月，撤销湘西行政公署及其所辖
3个专区，沅陵县改属[芷江专区](../Page/芷江专区.md "wikilink")。12月，芷江专区迁驻黔阳县[安江镇](../Page/安江.md "wikilink")，改名黔阳专区，县属黔阳专区。1968年，黔阳专区改称黔阳地区，1981年，黔阳地区改称[怀化地区](../Page/怀化地区.md "wikilink")，县属怀化地区，1997年改属[怀化市至今](../Page/怀化市.md "wikilink")。

由[中科院长沙土地构造研究所和湖南考古研究所人员组成的专家组对湖南沅陵窑头村一带的地质地貌进行了调查和测试](../Page/中科院.md "wikilink")，初步确认位于沅陵县城南窑头村的古遗址，就是秦代古黔中郡故城遗址。

## 行政区划

沅陵2005年在区划调整以前辖8个[区公所分管](../Page/区公所.md "wikilink")52个乡镇，经过[撤区并乡](../Page/撤区并乡.md "wikilink")，到2005年调整为23个乡镇。调整前区划共计52个乡镇。

  - 8[镇](../Page/镇.md "wikilink")：[沅陵镇](../Page/沅陵镇.md "wikilink")、[麻溪铺镇](../Page/麻溪铺镇.md "wikilink")、[五强溪镇](../Page/五强溪镇.md "wikilink")、[明溪口镇](../Page/明溪口镇.md "wikilink")、[凉水井镇](../Page/涼水井鎮_\(沅陵縣\).md "wikilink")、[七甲坪镇](../Page/七甲坪镇.md "wikilink")、[筲箕湾镇](../Page/筲箕湾镇.md "wikilink")、[官庄镇](../Page/官莊鎮_\(沅陵縣\).md "wikilink")；
  - 15[乡](../Page/乡.md "wikilink")：[杜家坪乡](../Page/杜家坪乡.md "wikilink")、[楠木铺乡](../Page/楠木铺乡.md "wikilink")、[深溪口乡](../Page/深溪口乡.md "wikilink")、[肖家桥乡](../Page/肖家桥乡.md "wikilink")、[陈家滩乡](../Page/陈家滩乡.md "wikilink")、[清浪乡](../Page/清浪乡.md "wikilink")、[借母溪鄉](../Page/借母溪鄉.md "wikilink")、[荔溪乡](../Page/荔溪乡.md "wikilink")、[大合坪乡](../Page/大合坪乡.md "wikilink")、[太常乡](../Page/太常乡.md "wikilink")、[马底驿乡](../Page/马底驿乡.md "wikilink")、[北溶乡](../Page/北溶乡.md "wikilink")、[盤古鄉](../Page/盤古鄉_\(沅陵縣\).md "wikilink")；
  - 2[民族乡](../Page/民族乡.md "wikilink")：[二酉苗族鄉和](../Page/二酉苗族鄉.md "wikilink")[火场土家族乡](../Page/火场土家族乡.md "wikilink")

## 饮食文化

沅陵粉当属湖南小吃一绝，而[米粉中当以猪脚粉为主打](../Page/米粉.md "wikilink")，其由米粉和特制猪脚而成，是当地人过早的主选食物。现今已有以“三鲜”“牛肉”“猪肚子”“饼饼肉”等带有地方特色的佐料的其它米粉。沅陵小吃诸如“猪脚粉”之类的早点，只有在上午11点左右之前才有供应，商家并不会因为生意好而在其它时间段进行供应，所以因此而形成了沅陵特有的早餐文化。因为沅陵交通不便原因，许多古老，优秀的文化没有得到发扬光大，所以也只有当地人才能享受如此的人间美味。

## 参考资料

[湘](../Page/分类:国家级贫困县.md "wikilink")

[沅陵县](../Category/沅陵县.md "wikilink")
[县](../Category/怀化区县市.md "wikilink")
[怀化](../Category/湖南省县份.md "wikilink")

1.  [1](http://www.hhtj.gov.cn/article/2014/0121/articles_8311.html)