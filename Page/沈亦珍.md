**沈亦珍**（），本名**禕**，字**亦珍**，[江蘇](../Page/江蘇.md "wikilink")[高郵人](../Page/高郵.md "wikilink")，教育家。他是[中國现代超常教育的先驱者](../Page/中國.md "wikilink")。由於本名「禕」經常被人誤讀「偉」，所以常常自稱「亦珍」，以致不少人以為他是女性。

## 生平

1917年考入[南京高等師範學校](../Page/南京高等師範學校.md "wikilink")，就讀于[工藝專修科](../Page/东南大学机械工程学院.md "wikilink")；次年轉入[香港大學](../Page/香港大學.md "wikilink")，主修心理學、教育學，1922年畢業。畢業后在[集美師範學校任教](../Page/集美師範學校.md "wikilink")，繼而到[暨南中學主持校務](../Page/暨南中學.md "wikilink")，到[上海大學英文系](../Page/上海大學.md "wikilink")、上海立達學校\[1\]任教，之後擔任江蘇省立[上海中學師範科主任](../Page/上海中學.md "wikilink")、教務主任。1933年赴[美國入](../Page/美國.md "wikilink")[密歇根大學](../Page/密歇根大學.md "wikilink")，獲[教育學](../Page/教育學.md "wikilink")[碩士學位後入](../Page/碩士.md "wikilink")[哥倫比亞大學教育學院](../Page/哥倫比亞大學.md "wikilink")。1936年獲博士學位，同年歸國，後執教于[國立中山大學](../Page/國立中山大學.md "wikilink")，不久接掌江蘇省立[鎮江中學](../Page/鎮江中學.md "wikilink")。

1937年，[中國抗日戰爭爆發後](../Page/中國抗日戰爭.md "wikilink")，先任[重慶](../Page/重慶.md "wikilink")[復旦大學文學院教授](../Page/復旦大學.md "wikilink")，後赴[甘肅省襄助教育廳長辦理全省教育](../Page/甘肅省.md "wikilink")，三年期滿后轉任[西北師範學院外文系任教授兼系主任](../Page/西北師範學院.md "wikilink")。之後，回母校[国立中央大学任師範學院教授](../Page/国立中央大学_\(南京\).md "wikilink")，主講教育及哲學、教育原理等課程。

1945年抗戰勝利後，返任[江蘇省立上海中學校長](../Page/江蘇省立上海中學.md "wikilink")。1949年後，先後任[國立台灣大學](../Page/國立台灣大學.md "wikilink")、[台灣師範大學教授](../Page/台灣師範大學.md "wikilink")。嗣後任[中華民國教育部普通教育司司長](../Page/中華民國教育部.md "wikilink")，四年間完成修訂了小學、中學及職校的課程標準；編印海外[華僑教科書](../Page/華僑.md "wikilink")；指定中、小學進行實驗研究工作等，還曾主持設在臺灣的美國在華教育基金會。　

## 晚年

1962年，赴[香港擔任](../Page/香港.md "wikilink")[蘇浙公學校長](../Page/蘇浙公學.md "wikilink")，長達18年。在此期間，參與創辦[香港中文大學之](../Page/香港中文大學.md "wikilink")[新亞書院](../Page/新亞書院.md "wikilink")、[新亞中學](../Page/新亞中學.md "wikilink")、[新亞研究所](../Page/新亞研究所.md "wikilink")，出任校董、校長等職。

## 著作

  - 《教育心理學》
  - 《中國聰穎兒童教育》
  - 《美國教育》
  - 《社會中心教育》
  - 《現代歐美中等教育》
  - 《教育論叢》

## 參考資料

[分類:國立中央大學教授](../Page/分類:國立中央大學教授.md "wikilink")
[分類:南京大學教授](../Page/分類:南京大學教授.md "wikilink")
[工](../Page/分類:中央大学校友.md "wikilink")
[分類:南京大學校友](../Page/分類:南京大學校友.md "wikilink")

[Category:中華民國教育家](../Category/中華民國教育家.md "wikilink")
[Category:中華民國政府官員](../Category/中華民國政府官員.md "wikilink")
[Category:香港教育家](../Category/香港教育家.md "wikilink")
[Category:新亞書院院長](../Category/新亞書院院長.md "wikilink")
[Category:香港中文大學教授](../Category/香港中文大學教授.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:國立臺灣師範大學教授](../Category/國立臺灣師範大學教授.md "wikilink")
[Category:復旦大學教授](../Category/復旦大學教授.md "wikilink")
[Category:國立中山大學教授](../Category/國立中山大學教授.md "wikilink")
[Category:上海大學教授](../Category/上海大學教授.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:密西根大學校友](../Category/密西根大學校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[2](../Category/东南大学校友.md "wikilink")
[Category:江蘇裔台灣人](../Category/江蘇裔台灣人.md "wikilink")
[Category:高郵人](../Category/高郵人.md "wikilink")
[Yi亦](../Category/沈姓.md "wikilink")

1.