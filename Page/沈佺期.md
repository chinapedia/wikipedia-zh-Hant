**沈佺期**，字**云卿**，[相州](../Page/相州.md "wikilink")[内黄](../Page/内黄.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[安陽市](../Page/安陽市.md "wikilink")[內黃縣](../Page/內黃縣.md "wikilink")）人。[唐朝著名](../Page/唐朝.md "wikilink")[诗人](../Page/诗人.md "wikilink")。

## 世系

曾祖[沈纂](../Page/沈纂.md "wikilink")，隋[秘书正字](../Page/秘书正字.md "wikilink")。祖[沈德](../Page/沈德.md "wikilink")，唐[潞州](../Page/潞州.md "wikilink")[长子县令](../Page/长子县.md "wikilink")。父[沈贞松](../Page/沈贞松.md "wikilink")，唐[泗州](../Page/泗州.md "wikilink")[下邳县令](../Page/下邳县.md "wikilink")。贞松生沈佺期、[沈全交](../Page/沈全交.md "wikilink")、[沈全宇](../Page/沈全宇.md "wikilink")。沈全交（664年－724年），有墓志出土。

沈佺期生[沈子昌](../Page/沈子昌.md "wikilink")、[沈之象](../Page/沈之象.md "wikilink")、[沈东美](../Page/沈东美.md "wikilink")、[沈惟清](../Page/沈惟清.md "wikilink")。沈东美，[给事中](../Page/给事中.md "wikilink")、[夏州都督](../Page/夏州.md "wikilink")。

沈佺期长子《大唐故上津县令沈子昌墓志》1997年在洛阳东边毗邻的偃师市首阳山出土。

## 生平

上元二年（675年）[进士](../Page/进士.md "wikilink")。[武后时](../Page/武后.md "wikilink")，官[协律郎累迁](../Page/协律郎.md "wikilink")[考功员外郎](../Page/考功员外郎.md "wikilink")。曾因受贿入狱。出狱后复职，迁给事中。[唐中宗时](../Page/唐中宗.md "wikilink")，因勾结[张昌宗兄弟](../Page/张昌宗.md "wikilink")，被流放到[驩州](../Page/驩州.md "wikilink")（今属[越南](../Page/越南.md "wikilink")）。神龙三年（707年），召拜[起居郎兼](../Page/起居郎.md "wikilink")[修文馆直学士](../Page/修文馆直学士.md "wikilink")，常侍宫中。后历[中书舍人](../Page/中书舍人.md "wikilink")、[太子少詹事](../Page/太子少詹事.md "wikilink")。

沈佺期工于[五言律诗](../Page/五言律诗.md "wikilink")，与[宋之问同为当时著名的宫廷诗人](../Page/宋之问.md "wikilink")，[文学史上并称](../Page/文学史.md "wikilink")“[沈宋](../Page/沈宋.md "wikilink")”。他们所作多为歌舞升平的[应制诗](../Page/应制诗.md "wikilink")，风格绮靡，不脱梁，陈[宫体诗风](../Page/宫体诗.md "wikilink")。可是沈、宋俩人总结了六朝以来[新体诗创作的经验](../Page/新体诗.md "wikilink")，对[律诗的成熟与定型](../Page/律诗.md "wikilink")，贡献颇大，是唐代五言律诗的奠基人。

  - 查洪德编有《**沈佺期年谱**》

## 著作

原有文集10卷，已佚。明人辑有《**沈佺期集**》。　

## 生卒

关于沈佺期的生卒年，历来有几种看法：

  - 吴海林、李延沛《**中国历史人物生卒年表**》，生于高宗显庆元年（656年），卒于开元二年（714年）
  - [闻一多](../Page/闻一多.md "wikilink")《**唐诗大系**》，约生于显庆元年（656年），卒于开元四年（716年）
  - 陆侃如、冯沅君《**中国诗史**》，约生于公元650年（高宗永徽元年），卒于公元714年（开元二年）
  - 刘开扬《**谈沈佺期、宋之问、李峤、杜审言等人的诗**》，约生于公元656年（高宗显庆元年），卒于713年（玄宗开元元年）

## 外部链接

  - [沈佺期、宋之问研究](https://web.archive.org/web/20030609102807/http://libweb.zju.edu.cn/renwen/site/guoxue/newbook/book22/duxiaoqin/20centure-03-6.htm)
  - [古诗库：沈佺期诗全集](http://www.lingshidao.com/gushi/shenquanqi.htm)

[S沈](../Category/656年出生.md "wikilink")
[S沈](../Category/714年逝世.md "wikilink")
[Category:唐朝詩人](../Category/唐朝詩人.md "wikilink")
[Category:唐朝進士](../Category/唐朝進士.md "wikilink")
[Category:唐朝协律郎](../Category/唐朝协律郎.md "wikilink")
[Category:唐朝吏部员外郎](../Category/唐朝吏部员外郎.md "wikilink")
[Category:唐朝给事中](../Category/唐朝给事中.md "wikilink")
[Category:唐朝起居郎](../Category/唐朝起居郎.md "wikilink")
[Category:唐朝学士](../Category/唐朝学士.md "wikilink")
[Category:唐朝中书舍人](../Category/唐朝中书舍人.md "wikilink")
[Category:唐朝太子少詹事](../Category/唐朝太子少詹事.md "wikilink")
[Category:河南诗人](../Category/河南诗人.md "wikilink")
[S沈](../Category/内黄人.md "wikilink")
[Category:沈姓](../Category/沈姓.md "wikilink")