**沈傲君**（），本名**赵燕**，[满族](../Page/满族.md "wikilink")，爸爸是辽宁[昌图人](../Page/昌图.md "wikilink")，妈妈是辽宁[丹东人](../Page/丹东.md "wikilink"),生长于[哈尔滨](../Page/哈尔滨.md "wikilink")。因[百事可乐广告而出道](../Page/百事可乐.md "wikilink")。1995年考入[上海电视台小荧星艺术学校学习影视专业](../Page/上海电视台.md "wikilink")，1997年8月以优异成绩毕业选送到上海电视台小荧星艺术团为演员。2013年凭电影《胡巧英告状》获第16届上海国际电影节之第10届电影频道传媒大奖最佳女主角奖。
2014年4月30日再次凭借影片《胡巧英告状》 荣获第14届电影频道电影百合奖优秀女演员奖。这是她演了15年的电视剧后获得的首个影后头衔。

## 作品

### 电视剧

  - 2000年 《[天地传说之宝莲灯](../Page/天地传说之宝莲灯.md "wikilink")》饰演：踏雪
  - 2000年 《[真情告白](../Page/真情告白.md "wikilink")》饰演：小燕
  - 2000年 《[难得有情人](../Page/难得有情人.md "wikilink")》饰演：包菁菁
  - 2001年
    《[大唐情史](../Page/大唐情史.md "wikilink")》饰演：[李世民十七公主](../Page/李世民.md "wikilink")[高阳公主](../Page/高阳公主.md "wikilink")
  - 2002年 《[少年四大名捕](../Page/少年四大名捕.md "wikilink")》饰演：诸葛青青
  - 2002年 《[关西无极刀](../Page/关西无极刀.md "wikilink")》饰演：梅娘
  - 2002年 《[壮志雄心](../Page/壮志雄心.md "wikilink")》饰演：刘可可
  - 2002年
    《[神医喜来乐](../Page/神医喜来乐.md "wikilink")》饰演：“食为天”饭馆老板“赛[西施](../Page/西施.md "wikilink")”
  - 2003年 《[首任台湾巡抚刘铭传](../Page/首任台湾巡抚刘铭传.md "wikilink")》饰演：刘之义女淑婷
  - 2003年 《不可触摸的真情》（又名：真情无价）饰演：美惠子
  - 2004年 《[大宋提刑官](../Page/大宋提刑官.md "wikilink")》饰演：柳絮儿
  - 2004年 《[天下第一媒婆](../Page/天下第一媒婆.md "wikilink")》饰演：德宁公主（化名花喜儿）
  - 2004年 《[早春二月](../Page/早春二月.md "wikilink")》饰演：文嫂
  - 2004年 《[青天衙门](../Page/青天衙门.md "wikilink")》饰演：石傲霜
  - 2004年 《[凌云壮志包青天](../Page/凌云壮志包青天.md "wikilink")》饰演：丽贵妃
  - 2004年 《[高考时光](../Page/高考时光.md "wikilink")》（又名：男生女生）饰演：郑瑾
  - 2005年 《[玉碎](../Page/玉碎_\(电视剧\).md "wikilink")》饰演：赵怀玉
  - 2005年
    《[伏魔录](../Page/伏魔录.md "wikilink")》（又名《[青天衙门](../Page/青天衙门.md "wikilink")Ⅱ》）饰演：李慧娘
  - 2005年 《[盐亨](../Page/盐亨.md "wikilink")》饰演：苏柳青
  - 2006年
    《[北魏冯太后](../Page/北魏冯太后_\(电视剧\).md "wikilink")》（又名：大漠朝阳）饰演：[花木兰](../Page/花木兰.md "wikilink")
  - 2006年 《[楚汉风云](../Page/楚汉风云.md "wikilink")》饰演：曹女
  - 2006年 《[旗袍](../Page/旗袍.md "wikilink")》饰演：沈诗语（客串）
  - 2006年 《[暴雨梨花](../Page/暴雨梨花.md "wikilink")》饰演：温柔（客串）
  - 2007年 《[子夜](../Page/子夜.md "wikilink")》饰演：林佩瑶
  - 2007年 《[我们俩的婚姻](../Page/我们俩的婚姻.md "wikilink")》饰演：秦芳
  - 2007年 《[金婚](../Page/金婚_\(电视剧\).md "wikilink")》饰演：李天骄
  - 2007年 《[爱情烘焙屋](../Page/爱情烘焙屋.md "wikilink")》饰演：安如
  - 2007年 《[霓虹灯下的哨兵](../Page/霓虹灯下的哨兵.md "wikilink")》饰演：夏梦瑶
  - 2008年 《[父亲的战争](../Page/父亲的战争.md "wikilink")》饰演：冉幺姑
  - 2008年 《[都叫我三妹](../Page/都叫我三妹.md "wikilink")》饰演：姚三美
  - 2008年 《[潜伏](../Page/潜伏_\(电视剧\).md "wikilink")》饰演：左蓝
  - 2009年 《[网球王子2](../Page/网球王子2.md "wikilink")》饰演：华村
  - 2014年 《温暖的日子》(用一生去爱你) 饰演：秦珍
  - 2014年 《[产科医生](../Page/产科医生.md "wikilink")》饰演：
  - 2014年
    《[历史转折中的邓小平](../Page/历史转折中的邓小平.md "wikilink")》饰演：[邓楠](../Page/邓楠.md "wikilink")
  - 2015年
    《[大舜](../Page/大舜_\(电视剧\).md "wikilink")》饰演：[握登](../Page/握登.md "wikilink")

### 电影

  - 《[胡巧英告状](../Page/胡巧英告状.md "wikilink")》饰演：胡巧英
  - 《[月夜歌声](../Page/月夜歌声.md "wikilink")》饰演：光慈
  - 《[风雨上海滩](../Page/风雨上海滩.md "wikilink")》饰演：毛岸青
  - 《[鲁迅](../Page/鲁迅.md "wikilink")》饰演：杨小佛
  - 1999年 《流星雨》
  - 2000年 《[千年等一天](../Page/千年等一天.md "wikilink")》饰演：林晓天
  - 2000年 《[夏日嬷嬷茶](../Page/夏日嬷嬷茶.md "wikilink")》饰演：yoyo
  - 2001年 《[我心不死](../Page/我心不死.md "wikilink")》 饰演：Maggie
  - 2004年
    《[大汉风](../Page/大汉风.md "wikilink")》饰演：[曹女](../Page/曹氏_\(劉邦\).md "wikilink")
  - 电视电影：《[爱情星期天](../Page/爱情星期天.md "wikilink")》饰演：贝贝

### 舞台剧

  - 《常回家看看》饰斌斌
  - 《烛光灿烂》饰宾宾

### 主持

  - 2002年 第35届亚行年会开幕式节目主持人

### 广告

  - 麦当劳
  - 六神沐浴露
  - 龙凤包子
  - 阿里斯顿热水器
  - 陆陆牌兄弟泥螺
  - 佑康汤圆
  - 炮天红酒
  - 蒙牛随变冰淇淋

### 代言

  - 菲克旅游鞋形象人。

巴黎奥美姿天然植物化妆品代言人

### MV

  - 卢秀梅独唱《团聚》得银奖。

## 外部链接

  - [相关专题](https://web.archive.org/web/20091107072845/http://space.tv.cctv.com/podcast/shenaojun)

  -
  -
  -
  -
  -
  -
[N傲](../Category/赵姓.md "wikilink")
[Category:滿族人](../Category/滿族人.md "wikilink")
[Category:黑龙江人](../Category/黑龙江人.md "wikilink")
[Category:哈尔滨人](../Category/哈尔滨人.md "wikilink")
[Category:辽宁人](../Category/辽宁人.md "wikilink")
[Category:丹東人](../Category/丹東人.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:滿族演员](../Category/滿族演员.md "wikilink")
[Category:黑龙江演员](../Category/黑龙江演员.md "wikilink")
[Category:辽宁演员](../Category/辽宁演员.md "wikilink")
[Category:上海戏剧学院校友](../Category/上海戏剧学院校友.md "wikilink")