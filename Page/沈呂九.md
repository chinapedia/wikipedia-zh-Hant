**沈呂九**（），[美籍华裔](../Page/美籍华裔.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")，[中央研究院院士](../Page/中央研究院院士.md "wikilink")。

## 学术研究

沈呂九在1965年與[沃爾特·科恩發表了一篇名為](../Page/沃爾特·科恩.md "wikilink")「包涵交換及關聯效應之自洽方程」的文章，文中提出了[科恩－沈吕九方程方程](../Page/科恩－沈吕九方程.md "wikilink")，奠定了[密度泛函理論的基礎](../Page/密度泛函理論.md "wikilink")。科恩也因此拿到了1998年的[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")。

## 重要獎項、榮譽、與經歷

  - [美國](../Page/美國.md "wikilink")[加州大學聖地牙哥分校物理系傑出教授](../Page/加州大學聖地牙哥分校.md "wikilink")
  - [中央研究院院士](../Page/中央研究院院士.md "wikilink")
  - 美國國家科學院院士
  - 美國物理學會會士
  - W.E. Lamb Medal
  - [國立交通大學榮譽博士](../Page/國立交通大學.md "wikilink")

[Category:香港科學家](../Category/香港科學家.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:聖地牙哥加州大學教師](../Category/聖地牙哥加州大學教師.md "wikilink")
[Category:國立交通大學榮譽博士](../Category/國立交通大學榮譽博士.md "wikilink")
[Category:劍橋大學校友](../Category/劍橋大學校友.md "wikilink")
[Category:香港培正中學校友](../Category/香港培正中學校友.md "wikilink")
[Category:香港浸會大學榮譽博士](../Category/香港浸會大學榮譽博士.md "wikilink")
[Category:沈姓](../Category/沈姓.md "wikilink")