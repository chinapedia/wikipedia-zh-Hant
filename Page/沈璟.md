**沈璟**（）\[1\]，字**伯英**，[直隸](../Page/南直隸.md "wikilink")[蘇州府](../Page/蘇州府.md "wikilink")[吳江縣人](../Page/吳江縣.md "wikilink")，軍籍，[明朝政治人物](../Page/明朝.md "wikilink")、散曲作家。

## 生平

萬曆二年（1574年）甲戌科會試第三名，殿試登二甲第五名進士\[2\]\[3\]\[4\]。

历任[兵部](../Page/兵部.md "wikilink")、[礼部](../Page/礼部.md "wikilink")、[吏部诸司](../Page/吏部.md "wikilink")[主事](../Page/主事.md "wikilink")、[员外郎](../Page/员外郎.md "wikilink")。因請求給王恭妃封號，忤旨，降行人司正\[5\]，中年即告病歸里，專注于戏曲创作。

## 著作

沈璟持論甚高，守律甚嚴，但才氣不足以達到要求，作品少有佳作\[6\]。其《南九宮十三調曲譜》（即《南曲全谱》）及《南詞韻選》兩書問世之後，[南曲開始走上格律之途](../Page/南曲.md "wikilink")，一時研究曲律之風大盛，如[王驥德](../Page/王驥德.md "wikilink")《曲律》、[呂天成](../Page/呂天成.md "wikilink")《曲品》，均為[曲學要點](../Page/曲學.md "wikilink")。

## 家族

曾祖父[沈漢](../Page/沈漢_\(明朝\).md "wikilink")，曾任戶科左給事中
贈太常寺少卿；祖父[沈嘉謀](../Page/沈嘉謀.md "wikilink")，曾任上林苑監署丞；父親[沈侃](../Page/沈侃.md "wikilink")，曾任監生。母卜氏\[7\]。有弟[沈瓚](../Page/沈瓚_\(萬曆進士\).md "wikilink")。

## 影響

[嘉靖年間](../Page/嘉靖.md "wikilink")，[魏良輔改良](../Page/魏良輔.md "wikilink")[崑腔](../Page/崑腔.md "wikilink")，[梁辰魚](../Page/梁辰魚.md "wikilink")、沈璟為之推波助瀾，於是南曲大盛，[北曲衰亡](../Page/北曲.md "wikilink")。而梁辰魚、沈璟南曲又多以[參詞法為之](../Page/參詞法.md "wikilink")，對明代後期曲壇，影響極大。

后来有些戏曲作家在音律上拥护他的主张，模仿他的风格，被称为“吴江派”。

沈璟是明清时期吴江沈氏文学家族的核心人物，后继者有从子[沈自晋](../Page/沈自晋.md "wikilink")、[沈自徵](../Page/沈自徵.md "wikilink")。

## 参考文献

[Category:明朝吏部主事](../Category/明朝吏部主事.md "wikilink")
[Category:明朝吏部員外郎](../Category/明朝吏部員外郎.md "wikilink")
[Category:明朝行人司正](../Category/明朝行人司正.md "wikilink")
[Category:明朝劇作家](../Category/明朝劇作家.md "wikilink")
[Category:吳江人](../Category/吳江人.md "wikilink")
[J](../Category/沈姓.md "wikilink")

1.
2.
3.
4.
5.  [清](../Page/清.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷206）：“曾孫璟，萬曆中為吏部員外郎。請王恭妃封號，忤旨，降行人司正。天啟初，贈少卿。”
6.  [葉慶炳](../Page/葉慶炳.md "wikilink")《[中國文學史](../Page/中國文學史.md "wikilink")》下冊
    頁284，1997
7.