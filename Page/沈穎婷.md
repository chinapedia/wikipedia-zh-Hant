**沈穎婷**（****，前名**沈頴婷**，），籍貫[江西](../Page/江西.md "wikilink")，曾是[無綫電視及](../Page/無綫電視.md "wikilink")[英皇電影旗下藝人](../Page/英皇電影.md "wikilink")。\[1\]\[2\]\[3\]\[4\]

## 簡介

沈穎婷是[香港息影藝人](../Page/香港.md "wikilink")[李影的女兒](../Page/李影.md "wikilink")\[5\]，父親是香港製衣商人[-{沈}-海鍵](../Page/沈海鍵.md "wikilink")，自幼在加拿大長大，她在[2004年參加](../Page/2004年.md "wikilink")[香港小姐競選而入行](../Page/香港小姐競選.md "wikilink")，成为“翡翠新力量”\[6\]的成員之一。但是，直到2007年，於電視劇集《[學警出更](../Page/學警出更.md "wikilink")》中飾演學警藍詠詩後，才漸為人所認識。她亦曾經憑《白雪天使》一曲獲得2006年度十大兒歌金曲獎。

由於沈穎婷於[無綫電視常安排飾演跑龍套的角色](../Page/無綫電視.md "wikilink")，她認為發揮不大，故於[2007年](../Page/2007年.md "wikilink")12月轉投[新視線](../Page/新視線.md "wikilink")，間接簽約[亞洲電視](../Page/亞洲電視.md "wikilink")，第一個工作為拍攝[電視劇](../Page/電視劇.md "wikilink")《[火蝴蝶](../Page/火蝴蝶.md "wikilink")》。

2009年，沈穎婷約滿后，不再選擇續約，恢復自由身，後簽約[英皇電影](../Page/英皇電影.md "wikilink")。2010年9月，入職國泰航空公司，現為空中小姐。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

#### [2005年](../Page/2005年.md "wikilink")

  - 《[識法代言人](../Page/識法代言人.md "wikilink")》 飾 Elsa（2005-2006）

#### [2006年](../Page/2006年.md "wikilink")

  - 《[人生馬戲團](../Page/人生馬戲團.md "wikilink")》 飾 趙蘭蘭
  - 《[法證先鋒](../Page/法證先鋒.md "wikilink")》 飾 Fanny
  - 《[愛情全保](../Page/愛情全保.md "wikilink")》 飾 唐仁佳
  - 《[鳳凰四重奏](../Page/鳳凰四重奏.md "wikilink")》飾 魯杏芬

#### [2007年](../Page/2007年.md "wikilink")

  - 《[師奶兵團](../Page/師奶兵團.md "wikilink")》 飾 Ada
  - 《[學警出更](../Page/學警出更.md "wikilink")》 飾 藍詠詩
  - 《[爸爸閉翳](../Page/爸爸閉翳.md "wikilink")》 飾 Candy
  - 《[歲月風雲](../Page/歲月風雲.md "wikilink")》 飾 私房菜老闆女
  - 《[奸人堅](../Page/奸人堅.md "wikilink")》飾 娟妹
  - 《[同事三分親](../Page/同事三分親.md "wikilink")》飾 娟

#### [2008年](../Page/2008年.md "wikilink")

  - 《[野蠻奶奶大戰戈師奶](../Page/野蠻奶奶大戰戈師奶.md "wikilink")》飾 Flora
  - 《[秀才愛上兵](../Page/秀才愛上兵.md "wikilink")》飾 香水
  - 《[最美麗的第七天](../Page/最美麗的第七天.md "wikilink")》 飾 Sales
  - 《[銀樓金粉](../Page/銀樓金粉.md "wikilink")》飾 護士
  - 《[古靈精探](../Page/古靈精探.md "wikilink")》 飾 方翠芝（Annie）
  - 《[當狗愛上貓](../Page/當狗愛上貓.md "wikilink")》 飾 Pat
  - 《[珠光寶氣](../Page/珠光寶氣_\(電視劇\).md "wikilink")》

#### [2009年](../Page/2009年.md "wikilink")

  - 《[大冬瓜](../Page/大冬瓜.md "wikilink")》飾 小甜

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### [2008年](../Page/2008年.md "wikilink")

  - 《[火蝴蝶](../Page/火蝴蝶.md "wikilink")》 飾 趙凱琪

#### [2010年](../Page/2010年.md "wikilink")

  - 《[法網群英](../Page/法網群英.md "wikilink")》

### 其他演出（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 2006-2008年：《[一擲千金](../Page/一擲千金_\(香港\).md "wikilink")》 - 千金小姐

### 其他演出（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 2008年：《[樂壇風雲50年](../Page/樂壇風雲50年.md "wikilink")》--Joyce Lee Wedding
    Party

### 節目主持（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### [2008年](../Page/2008年.md "wikilink")

  - 《[今日法庭](../Page/今日法庭.md "wikilink")》
  - 《[高清打機王](../Page/高清打機王.md "wikilink")》
  - 《[識飲識食識香味](../Page/識飲識食識香味.md "wikilink")》

### 音樂作品（歌曲）

  - [2006年](../Page/2006年.md "wikilink")：白雪天使
  - [2008年](../Page/2008年.md "wikilink")：恭喜發財迎新春（[aTV賀年歌](../Page/aTV.md "wikilink")）

### 音樂影帶

  - [李嘉強](../Page/李嘉強.md "wikilink")《唯你獨尊》（[TVB版](../Page/TVB.md "wikilink")）
  - [張繼聰](../Page/張繼聰.md "wikilink")《彈弓手》（[TVB版](../Page/TVB.md "wikilink")）
  - [周國賢](../Page/周國賢.md "wikilink")《漢城沉沒了》（[TVB版](../Page/TVB.md "wikilink")）
  - [Bliss](../Page/Bliss.md "wikilink")《跳飛機》（[TVB版](../Page/TVB.md "wikilink")）

## 曾獲獎項

  - 2006年度兒歌金曲頒獎典禮：十大兒歌金曲獎（《白雪天使》）

## 參見

  - 2006及2007[無綫月曆](../Page/無綫月曆.md "wikilink")
  - [2006年度兒歌金曲頒獎典禮得獎名單](../Page/2006年度兒歌金曲頒獎典禮得獎名單.md "wikilink")

## 參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [Wini da Slow](http://www.wini-shum.com/)
  - [沈穎婷網誌](http://hk.myblog.yahoo.com/slow1128/)

[Category:2004年度香港小姐競選參賽者](../Category/2004年度香港小姐競選參賽者.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[Category:加拿大华人](../Category/加拿大华人.md "wikilink")
[Category:溫哥華人](../Category/溫哥華人.md "wikilink")
[Category:江西人](../Category/江西人.md "wikilink")
[wing](../Category/沈姓.md "wikilink")

1.  [1](http://blog.xuite.net/ohblogger/ohblogger/13350090-%E6%B2%88%E7%A9%8E%E5%A9%B7%E3%80%8C%E6%B7%98%E6%B0%A3%E5%B0%8F%E9%AC%BC%E5%81%B7%E5%85%A5%E5%B1%8B%E3%80%8D)
2.  [2](https://hk.lifestyle.appledaily.com/nextplus/magazine/article/20170110/2_468119_0/%E6%92%88%E9%81%8E%E9%85%92%E5%90%A7%E5%81%9A%E9%81%8E%E9%A6%AC%E6%9C%83-32%E6%AD%B2%E6%B2%88%E7%A9%8E%E5%A9%B7%E9%9D%A0%E6%89%93%E6%8B%B3%E9%9F%BF%E5%90%8D%E5%A0%82)
3.  [3](https://hk.entertainment.appledaily.com/entertainment/daily/article/20150805/19243124)
4.  [4](http://eastweek.my-magazine.me/main/60458)[5](http://hk.on.cc/hk/bkn/cnt/entertainment/20160918/bkn-20160918163156958-0918_00862_001.html)
5.
6.