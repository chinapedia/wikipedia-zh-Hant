**沈芝華**是1960年代[香港粵語片演員](../Page/香港.md "wikilink")，是时人称为影壇[七公主的七人](../Page/七公主.md "wikilink")（包括[陈宝珠](../Page/陈宝珠.md "wikilink")、[萧芳芳](../Page/萧芳芳.md "wikilink")、[薛家燕等](../Page/薛家燕.md "wikilink")）之一\[1\]\[2\]，也是最早退出演藝事業的一位。\[3\]其胞姐為粵語片演員[沈月華](../Page/沈月華.md "wikilink")，姨甥為香港歌手及演員[蔡興麟](../Page/蔡興麟.md "wikilink")。

她的作品有《迷人小鳥》、《金色聖誕夜》、《旺財嫂》、《青春玫瑰》等。沈芝華首次[婚姻是嫁給國語片小生](../Page/婚姻.md "wikilink")[金川](../Page/金川\(香港電影\).md "wikilink")，婚後育有一子。離異後再婚，長於[美國居住](../Page/美國.md "wikilink")。\[4\]

2017年客串TVB劇集《[燦爛的外母](../Page/燦爛的外母.md "wikilink")》，相隔五十多年再次跟薛家燕、馮素波等粵語片年代花旦合作演出。

## 演出電影 (31 部)

| 影片名稱             | 導演      | 角色        | 合演                                                                                                                                     |
| ---------------- | ------- | --------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| **1960年**        |         |           |                                                                                                                                        |
| 《雙孝子月宮救母》        | 陳焯生     | 村童        | [鄧碧雲](../Page/鄧碧雲.md "wikilink"), [麥炳榮](../Page/麥炳榮.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink")                                   |
| **1961年**        |         |           |                                                                                                                                        |
| 《無敵楊家將》          | 黃鶴聲     | 金麟太子      | [于素秋](../Page/于素秋.md "wikilink"), [林家聲](../Page/林家聲.md "wikilink"), [靚次伯](../Page/靚次伯.md "wikilink"), [半日安](../Page/半日安.md "wikilink") |
| **1962年**        |         |           |                                                                                                                                        |
| 《大戰泗洲城》          | 黃鶴聲     | 分飾 悟空, 蝦精 | [林家聲](../Page/林家聲.md "wikilink"), [文蘭](../Page/文蘭.md "wikilink"), [梁醒波](../Page/梁醒波.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink")   |
|                  |         |           |                                                                                                                                        |
| 《火海勝字旗》          | 黃鶴聲     | 少芳        | [麥炳榮](../Page/麥炳榮.md "wikilink"), [鳳凰女](../Page/鳳凰女.md "wikilink")                                                                     |
|                  |         |           |                                                                                                                                        |
| 《孫悟空鬧龍宮》         | 陳焯生     | 三太子       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [李香琴](../Page/李香琴.md "wikilink")                                                                     |
| **1963年**        |         |           |                                                                                                                                        |
| 《龍虎下江南(上集)》      | 康毅      |           | [曹達華](../Page/曹達華.md "wikilink"), [于素秋](../Page/于素秋.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink"), [蕭芳芳](../Page/蕭芳芳.md "wikilink") |
|                  |         |           |                                                                                                                                        |
| 《雷電天仙劍》          | 凌雲      | 馬錦雲       | [曹達華](../Page/曹達華.md "wikilink"), [于素秋](../Page/于素秋.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink"), [石堅](../Page/石堅.md "wikilink")   |
| **1964年**        |         |           |                                                                                                                                        |
| 《柳葉刀》            | 胡鵬      | 柳一鶯       | [夏娃](../Page/夏娃.md "wikilink"), [張活游](../Page/張活游.md "wikilink"), [胡楓](../Page/胡楓.md "wikilink")                                       |
|                  |         |           |                                                                                                                                        |
| 《梁山伯與祝英台》 (國語電影) | 嚴俊      | 銀心        | [尤敏](../Page/尤敏.md "wikilink"), [李麗華](../Page/李麗華.md "wikilink")                                                                       |
|                  |         |           |                                                                                                                                        |
| 《萬變飛狐》           | 蔡昌, 蕭笙  | 上官琦       | [于素秋](../Page/于素秋.md "wikilink"), [張英才](../Page/張英才.md "wikilink"), [李紅](../Page/李紅.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink")   |
|                  |         |           |                                                                                                                                        |
| 《龍虎震江南(上集)》      | 康毅      | 梅心美       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [于素秋](../Page/于素秋.md "wikilink"), [曹達華](../Page/曹達華.md "wikilink")                                   |
|                  |         |           |                                                                                                                                        |
| 《龍虎震江南(下集)》      | 康毅      | 梅心美       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [于素秋](../Page/于素秋.md "wikilink"), [曹達華](../Page/曹達華.md "wikilink")                                   |
| **1965年**        |         |           |                                                                                                                                        |
| 《一滴俠義血(上集)》      | 凌雲      | 朱碧        | [曹達華](../Page/曹達華.md "wikilink"), [李湄](../Page/李湄.md "wikilink"), [鳳凰女](../Page/鳳凰女.md "wikilink"), [關海山](../Page/關海山.md "wikilink")   |
| **1966年**        |         |           |                                                                                                                                        |
| 《播音王子》           | 龍剛      | 謝淑儀       | [謝賢](../Page/謝賢.md "wikilink"), [陳齊頌](../Page/陳齊頌.md "wikilink"), [王偉](../Page/王偉.md "wikilink")                                       |
|                  |         |           |                                                                                                                                        |
| 《中原奇俠》           | 蔡昌, 蕭笙  | 上官琦       | [于素秋](../Page/于素秋.md "wikilink"), [李紅](../Page/李紅.md "wikilink"), [張英才](../Page/張英才.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink")   |
| **1967年**        |         |           |                                                                                                                                        |
| 《飛賊金絲貓》          | 蔣偉光     | 史珍妮       | [蕭芳芳](../Page/蕭芳芳.md "wikilink"), [胡楓](../Page/胡楓.md "wikilink"), [石堅](../Page/石堅.md "wikilink")                                       |
|                  |         |           |                                                                                                                                        |
| 《七公主(上集)》        | 凌雲      | 呂世豪       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink"), [蕭芳芳](../Page/蕭芳芳.md "wikilink")                                   |
|                  |         |           |                                                                                                                                        |
| 《七公主(下集)》        | 凌雲      | 呂世豪       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [陳寶珠](../Page/陳寶珠.md "wikilink"), [蕭芳芳](../Page/蕭芳芳.md "wikilink")                                   |
| **1968年**        |         |           |                                                                                                                                        |
| 《藍鷹》             | 王風      | 赫玫瑰       | [蕭芳芳](../Page/蕭芳芳.md "wikilink"), [鄭少秋](../Page/鄭少秋.md "wikilink"), [夏萍](../Page/夏萍.md "wikilink"), [石堅](../Page/石堅.md "wikilink")     |
|                  |         |           |                                                                                                                                        |
| 《青春玫瑰》           | 蔣偉光     | 林金佩       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [胡楓](../Page/胡楓.md "wikilink")                                                                       |
|                  |         |           |                                                                                                                                        |
| 《方世玉三打木人巷》       | 馮志剛     | 李小環       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [鄭少秋](../Page/鄭少秋.md "wikilink"), [容玉意](../Page/容玉意.md "wikilink"), [石堅](../Page/石堅.md "wikilink")   |
|                  |         |           |                                                                                                                                        |
| 《樊梨花》            | 馮志剛     | 薛應龍       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [羽佳](../Page/羽佳.md "wikilink"), [譚蘭卿](../Page/譚蘭卿.md "wikilink")                                     |
|                  |         |           |                                                                                                                                        |
| 《血影紅燈》           | 王風      | 夏侯婉       | [陳寶珠](../Page/陳寶珠.md "wikilink"), [薛家燕](../Page/薛家燕.md "wikilink"), [張英才](../Page/張英才.md "wikilink"), [鄭少秋](../Page/鄭少秋.md "wikilink") |
| **1969年**        |         |           |                                                                                                                                        |
| 《銀刀血劍》           | 王風      | 金燕子       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [鄭少秋](../Page/鄭少秋.md "wikilink"), [石堅](../Page/石堅.md "wikilink")                                     |
|                  |         |           |                                                                                                                                        |
| 《名劍天驕》           | 蕭笙, 羅基石 | 蘭花        | [李琳琳](../Page/李琳琳.md "wikilink"), [曾江](../Page/曾江.md "wikilink"), [陳齊頌](../Page/陳齊頌.md "wikilink"), [秦沛](../Page/秦沛.md "wikilink")     |
|                  |         |           |                                                                                                                                        |
| 《峨嵋霸刀》           | 陳烈品     | 村姑--小蘭    | [蕭芳芳](../Page/蕭芳芳.md "wikilink"), [曾江](../Page/曾江.md "wikilink"), [石堅](../Page/石堅.md "wikilink")                                       |
|                  |         |           |                                                                                                                                        |
| 《小魔俠》            | 陳烈品     | 梅鳳姬       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [江文聲](../Page/江文聲.md "wikilink")                                                                     |
| **1970年**        |         |           |                                                                                                                                        |
| 《小金剛》            | 馮峯      | 于文芳       | [馮寶寶](../Page/馮寶寶.md "wikilink"), [曾江](../Page/曾江.md "wikilink"), [薛家燕](../Page/薛家燕.md "wikilink"), [石堅](../Page/石堅.md "wikilink")     |
|                  |         |           |                                                                                                                                        |
| 《冰谷魔女》           | 羅熾      |           | [陳思思](../Page/陳思思.md "wikilink"), [張冲](../Page/張冲.md "wikilink")                                                                       |
| **1972年**        |         |           |                                                                                                                                        |
| 《天涯客》            | 吳天池     |           | [金童](../Page/金童.md "wikilink"), [唐菁](../Page/唐菁.md "wikilink"), [白鷹](../Page/白鷹.md "wikilink"), [石堅](../Page/石堅.md "wikilink")         |
| **1973年**        |         |           |                                                                                                                                        |
| 《頂天立地》           | 朱牧      |           | [王青](../Page/王青.md "wikilink"), [元秋](../Page/元秋.md "wikilink"), [成龍](../Page/成龍.md "wikilink")                                         |
|                  |         |           |                                                                                                                                        |

**電視劇**

2017年：《[燦爛的外母](../Page/燦爛的外母.md "wikilink")》飾 表姨媽（客串）

## 参考

<references/>

## 外部鏈結

  -
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港粵語片演員](../Category/香港粵語片演員.md "wikilink")
[C芝](../Category/沈姓.md "wikilink")

1.  [附图：香港电影金像奖影后列传之萧芳芳](http://movie.ifensi.com/article-53565.html)

2.  [薛家燕：与陈宝珠萧芳芳三分天下
    一手挖掘哥哥梅姑](http://lady.163.com/13/1015/14/9B81E45J00264M4F_all.html)

3.  [圖揭香港七公主
    蕭芳芳曾搶林青霞愛人](http://news.hexun.com.tw/2012-03-29/139849455_36.html)

4.