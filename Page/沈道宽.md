**沈道宽**（），[清朝](../Page/清朝.md "wikilink")[书法家](../Page/书法家.md "wikilink")、[画家](../Page/画家.md "wikilink")，[字](../Page/表字.md "wikilink")**栗仲**，[浙江](../Page/浙江.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")。

## 家世

出自[栎社沈氏](../Page/栎社沈氏.md "wikilink")，先世居于浙江鄞县，后来入籍[大兴](../Page/大兴.md "wikilink")（今[北京市](../Page/北京市.md "wikilink")）。先祖有[明朝诗人](../Page/明朝.md "wikilink")[沈明臣](../Page/沈明臣.md "wikilink")，首辅[沈一贯](../Page/沈一贯.md "wikilink")\[1\]。父[沈谦](../Page/沈谦.md "wikilink")，子[沈敦兰](../Page/沈敦兰.md "wikilink")\[2\]。后人有美食家[沈京似](../Page/沈京似.md "wikilink")。

## 生平

[嘉庆二十五年](../Page/嘉庆.md "wikilink")（1820年），[进士出身](../Page/进士.md "wikilink")，曾在[湖南省的](../Page/湖南省.md "wikilink")[酃县](../Page/酃县.md "wikilink")、[桃源做](../Page/桃源縣.md "wikilink")[知县](../Page/知县.md "wikilink")。工于[书法](../Page/书法.md "wikilink")，擅长画[山水画](../Page/山水画.md "wikilink")。享年八十二岁。

## 遗迹

  - [清朝](../Page/清朝.md "wikilink")[道光七年](../Page/道光.md "wikilink")（1827年），沈道宽作为[知县](../Page/知县.md "wikilink")，曾主持重修[炎帝陵](../Page/炎帝陵.md "wikilink")，并主持修编了《炎陵志》八卷。[炎帝陵墓冢碑文为](../Page/炎帝陵.md "wikilink")[清朝](../Page/清朝.md "wikilink")[道光七年](../Page/道光.md "wikilink")（1827年）沈道宽所书。
  - 沈道宽曾主持修建“咏丰台”，并书写“咏丰台”三字刻于石碑。
  - 沈氏[遂园](../Page/遂园.md "wikilink")：原沈家的私家花园，今位于[江苏省](../Page/江苏省.md "wikilink")[淮安市](../Page/淮安市.md "wikilink")，是淮安市级文物保护单位\[3\]。

## 著作

  - 《话山草堂诗钞》。

## 参考

  - 《清画家诗史》

[Category:清朝酃縣知縣](../Category/清朝酃縣知縣.md "wikilink")
[Category:清朝湖南桃源縣知縣](../Category/清朝湖南桃源縣知縣.md "wikilink")
[Category:清朝书法家](../Category/清朝书法家.md "wikilink")
[Category:清朝画家](../Category/清朝画家.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:鄞县人](../Category/鄞县人.md "wikilink")
[D沈道宽](../Category/栎社沈氏.md "wikilink")
[D道](../Category/沈姓.md "wikilink")

1.

2.
3.