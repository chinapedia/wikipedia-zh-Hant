**沉重龍屬**（[學名](../Page/學名.md "wikilink")：*Epachthosaurus*）是[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，[化石發現於](../Page/化石.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")[巴塔哥尼亞中部及北部](../Page/巴塔哥尼亞.md "wikilink")，地質年代屬於[上白堊紀](../Page/上白堊紀.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")*E.
sciuttoi*，是在1990年被描述、命名，另一個接近完整的骨骼則於2004年被研究、描述。

## 參考資料

  -
## 外部連結

  - [恐龍博物館——沉重龍](https://web.archive.org/web/20071109012559/http://www.dinosaur.net.cn/museum/Epachthosaurus.htm)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:泰坦巨龍類](../Category/泰坦巨龍類.md "wikilink")