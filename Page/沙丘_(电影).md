\-{H|zh-hans:沙丘;zh-hk:星際奇兵;zh-tw:沙丘魔堡;}-
是一部1984年美國[史詩](../Page/史詩片.md "wikilink")[科幻電影](../Page/科幻電影.md "wikilink")，由[大卫·林奇執導和編劇](../Page/大卫·林奇.md "wikilink")，基于1965年[弗兰克·赫伯特的](../Page/弗兰克·赫伯特.md "wikilink")[同名小说改编](../Page/沙丘_\(小说\).md "wikilink")。影星[凯尔·麦克拉克兰出演主要角色](../Page/凯尔·麦克拉克兰.md "wikilink")，其他配角演员由一些著名的[美国和](../Page/美国.md "wikilink")[欧洲演员担当](../Page/欧洲.md "wikilink")，包括[史田](../Page/史田.md "wikilink")
、、[维吉尼亚·马德森](../Page/维吉尼亚·马德森.md "wikilink")、[琳达·亨特](../Page/琳达·亨特.md "wikilink")、[柏克·史釗域](../Page/柏克·史釗域.md "wikilink")、[麥士·馮·西度和](../Page/麥士·馮·西度.md "wikilink")\[1\]\[2\]\[3\]等。该片主要在[墨西哥的](../Page/墨西哥.md "wikilink")拍摄。

本片被評論家負面批評，也在[票房上失敗](../Page/票房毒藥.md "wikilink")，其成本达4000万美元但国内票房仅为3090万美元。它也受到赫伯特的崇拜者的批评，因为他们反对导演在电影中擅自偏离小说的故事情节。在近來，本片被公認為一部[邪典电影](../Page/邪典电影.md "wikilink")。

## 主要演员阵容

  - 飾演

  - 飾演 男爵的医生（the Baron's Doctor）

  - [布拉德·道里夫](../Page/布拉德·道里夫.md "wikilink") 飾演

  - [何塞·費勒](../Page/何塞·費勒.md "wikilink") 飾演

  - [琳达·亨特](../Page/琳达·亨特.md "wikilink") 飾演 夏杜特·梅帕斯（Shadout Mapes）

  - 飾演

  - 飾演

  - [凯尔·麦克拉克兰](../Page/凯尔·麦克拉克兰.md "wikilink") 飾演

  - [维吉尼亚·马德森](../Page/维吉尼亚·马德森.md "wikilink") 飾演

  - [施雲娜·曼簡奴](../Page/施雲娜·曼簡奴.md "wikilink") 飾演 Reverend Mother Ramallo

  - 飾演 史帝加（Stilgar）

  - 飾演 男爵

  - 飾演 Captain Iakin Nefud

  - 飾演 教母

  - 飾演 公爵 莱特·亚崔迪（Duke Leto Atreides）

  - 飾演 野兽拉宾（the Beast Rabban）

  - [柏克·史釗域](../Page/柏克·史釗域.md "wikilink") 飾演

  - [史田](../Page/史田.md "wikilink") 飾演

  - [狄恩·史達威爾](../Page/狄恩·史達威爾.md "wikilink") 飾演 惠灵顿·岳大夫（Dr. Wellington
    Yueh）

  - [麥士·馮·西度](../Page/麥士·馮·西度.md "wikilink") 飾演 凯恩斯博士（Dr. Kynes）

  - 飾演

  - [西恩·楊](../Page/西恩·楊.md "wikilink") 飾演

  - （刪除畫面） 飾演 Orlop

  - Honorato Magalone 飾演 Otheym

  - Judd Omen 飾演 Jamis

  - Molly Wryn 飾演 Harah，Jamis的妻子

  - [大卫·林奇](../Page/大卫·林奇.md "wikilink")（未掛名） 飾演 香料工人

## 改编

该片几乎全部在[墨西哥摄制](../Page/墨西哥.md "wikilink")。它是一个系列小说的第一部分的改编（参看小說《[沙丘](../Page/沙丘_\(小说\).md "wikilink")》），原著[弗兰克·赫伯特](../Page/弗兰克·赫伯特.md "wikilink")（Frank
Herbert），也包含后来部分的一些元素。主要情节关于一个年轻人，他在[预言中被描述为将一个](../Page/预言.md "wikilink")[沙漠行星从邪恶的贵族哈肯尼手中拯救出来的](../Page/沙漠.md "wikilink")[Kwisatz
Haderach](../Page/Kwisatz_Haderach.md "wikilink")。

[大卫·林奇](../Page/大卫·林奇.md "wikilink")（David
Lynch）最初制作一个长得多的电影；他的135页的剧本最后产生了一个4－5小时的电影。但在后期制作中，制片人Dino
De Laurentiis 不想冒险发行3个小时之久的四千万制作的电影，所以他迫使大卫·林奇将其剪为137分钟。

弗兰克·赫伯特观看了两个版本。他非常喜欢长版本而不喜欢短版本。

[雷利·史考特](../Page/雷利·史考特.md "wikilink")（Ridley
Scott）最初曾被选为导演，但制作问题产生了，而史考特转去执导《[银翼杀手](../Page/银翼杀手.md "wikilink")》（*Blade
Runner*），史考特研究了一些脚本草稿，最终倾向于同时导演两部电影。他回忆前期制作进程很慢，如果完成计划要花更多时间。

前期制作可以追溯到1975年，[亚历杭德罗·霍多罗斯基](../Page/亚历杭德罗·霍多罗斯基.md "wikilink")（Alejandro
Jodorowsky）试图将该故事拍摄成10小时的电影，并想和[奥逊·威尔斯](../Page/奥逊·威尔斯.md "wikilink")（Orson
Welles），[萨尔瓦多·达利](../Page/萨尔瓦多·达利.md "wikilink")（Salvador
Dalí），[格洛丽亚·斯旺森](../Page/格洛丽亚·斯旺森.md "wikilink")（Gloria
Swanson），[H·R·吉格尔](../Page/H·R·吉格尔.md "wikilink")（H. R.
Giger）等人合作。音乐本来想由[平克·弗洛伊德](../Page/平克·弗洛伊德.md "wikilink")（Pink
Floyd）乐队完成，但该计划一直没有实现。有些准备后来被用于影片《[异形](../Page/异形_\(电影\).md "wikilink")》（*Alien*）。

## 票房和观感

该片没有成为制片人所期待的轰动大片，国内销售毛额仅为$30.9百万，而成本估算则为$40百万。这可能是因为故事的复杂度，其单薄，松散和梦境般的线索。在影评中，[Roger
Ebert写道](../Page/Roger_Ebert.md "wikilink")：「该电影是一团混乱，一个无法理解的，丑陋的，无结构的，无意义的，在史上最令人困惑的剧本之一的黑暗场景中的漫游。」

原著的书迷大部分也较为失望，科幻迷也认为特技有很多待改进的地方。而且，作为票房和影评的双重失败，大卫·林奇不喜欢在采访中谈论《沙丘》，总是有礼貌的称他自己“屏蔽”了那段时间的记忆。评论家通常视其为最差的大卫·林奇电影（但是反过来，它也是最流行的林奇电影之一）。

友善一点的评论褒扬了林奇在影片中采用的黑色－巴洛克式手法，并称观赏者必须先了解沙丘的背景才便于观看。

## 和小说的分歧

电影和原著有多处偏离，包括一下几点。

  - 小说中，“玄秘之法”（"Weirding
    Way"，正式名称为“prana-bindu训练”），是一种超级武术形式，它使得保罗·亚崔迪这样的熟练使用者以闪电般的速度移动。而林奇的电影将其替换为“玄秘模块”（本质上是一种声音手枪）它能够放大使用者的喊叫，成为一种破坏力（类似“狮子吼”）。它重现了小说中的一幕保罗说他的名字成了死亡祈祷因为弗瑞曼人杀死对手前大喊“Muad'dib\!”。在电影中，该弗瑞曼人实际上通过喊他的名字直接杀死了他们的敌人，使得保罗评论到“我的名字成了带杀伤力的词”。
  - 菲得·罗萨·哈肯尼的角色大量缩减，所以对故事不作很大影响，即使在高潮的一幕。在小说中，保罗和菲得进行了一场戏剧性的匕首战。而电影中，在高潮时没有太多打斗，菲得被极快制服。
  - 电影以保罗“命令”在阿拉吉斯（Arrakis）上下雨终结。在小说中，这是通过多年的[地形改造达成的](../Page/地形改造.md "wikilink")，在保罗登上王位数十年后都没有下雨。这是因为保罗的力量没有神奇的成分；他是基因配种和训练的产物，不可能命令天空在沙丘行星上下雨。
  - 在小说中，最后的台词，由杰西卡说给加妮的是“承担妾的名分的我们，历史会将我们记为妻”（这指的是保罗娶了伊如兰，但她只是名义上的妻子）。电影中，最后的台词是（由阿丽亚说出）「他**就是**[科维扎基·哈得那奇](../Page/科维扎基·哈得那奇.md "wikilink")！」（尽管事实上，在书中保罗声称最后他**不是**科维扎基·哈得那奇，而是完全没有预料到的产物，因为他比计划早了一代）。

## 邪典式的成功以及改编

尽管有不满的赫伯特迷的抱怨，尖刻的批评和空前的票房败绩，该片获得了值得敬仰的邪典式的地位，至少发行了三个不同版本：

  - **原始影院版**(137分钟)--该版式唯一导演认可和授权的版本。可以在大量录影带和[DVD中找到](../Page/DVD.md "wikilink")。
  - **Alan Smithee版本** (约190分钟)--少见一些的3小时『[Alan
    Smithee](../Page/Alan_Smithee.md "wikilink")』版本身是邪典经典。最初为垄断的电视所预备（后来出现在有线电视中），它在某些市场中以DVD发行（包括[加拿大和](../Page/加拿大.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")）。剪掉的片段包括序幕中的蒙太奇，而一些场景被重新加回，博阿扩香料精华的“小制作器”场景。该电视版由几乎是随意编辑的（例如，有些镜头重复出现给人以片段被加入的印象）。林奇反对这些编辑，并将他的名字从电视版的谢辞中消去了（他的名字在剧场版中有因为那是唯一导演认可的版本）。该版本还缺少更多剪掉的场景的证据还是可以找到的：Thufir
    Hawat在王位最终章可以在后面看到，但是其后他消失了。该场景在2006年超长版DVD版本中的“剪掉的沙丘”的特色中可以找到。
  - **二频道版**
    (约180分钟)—[KTVU](../Page/KTVU.md "wikilink")，加州[旧金山](../Page/旧金山.md "wikilink")[福克斯电视网](../Page/福克斯电视网.md "wikilink")（Fox
    network）旗下的电台，将上面两个版本重新拼接用于在旧金山湾区于1992年播放。它实质上是电视版加上剧场版的暴力镜头的重新加回。

**超长版**
由环球家庭娱乐公司在美国于2006年1月31日在[DVD上发行](../Page/DVD.md "wikilink")。它包含林奇的137分钟剧场剪贴和一个177分钟编辑的Alan
Smithee电视版（后者首次以它原始的[Todd-AO长宽比发行](../Page/Todd-AO.md "wikilink")）。它也包含设计和特技的记录片，和一个以前所有版本中没有出现的场景组成的补充章节，包括一个不同的可能结局。

还有，一个[DVD](../Page/DVD.md "wikilink")
**超长版**在2005年11月在欧洲发行。视频和音频未经处理，有较差的电视质量。尽管封面说它是单声道的，但它事实上是立体声的。

[英国观察家报于](../Page/英国观察家报.md "wikilink")2006年1月22日送出了沙丘DVD的免费拷贝。它不包含任何特色内容。

## 影响

  - 该片给了[一系列电子游戏以灵感](../Page/沙丘_（电脑游戏）.md "wikilink")，由[Cryo
    Interactive和](../Page/Cryo_Interactive.md "wikilink")[Westwood制作](../Page/Westwood_Studios.md "wikilink")，包括[沙丘
    2000和](../Page/沙丘_2000.md "wikilink")[皇帝：沙丘之战](../Page/皇帝：沙丘之战.md "wikilink")，其特色有隐含剧情中的真实演员（包括John
    Rhys-Davies
    出演[亚崔迪的](../Page/House_亚崔迪.md "wikilink")[门塔特](../Page/门塔特.md "wikilink")，而在后续中，Michael
    Dorn 出演[亚崔迪的Achillus公爵](../Page/House_亚崔迪.md "wikilink")）。

<!-- end list -->

  - 它最近由[科幻频道改制成为三部分的](../Page/科幻频道.md "wikilink")[沙丘（电视短片）系列并制成录像和DVD发售](../Page/沙丘（电视短片）.md "wikilink")。

## 花絮

  - [Michael
    Bolton在超长DVD版中出现](../Page/Michael_Bolton.md "wikilink")，在菲得和保罗匕首单挑中击鼓。

## 參考文獻

## 外部链接

  - [《沙丘》](http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:14962~C)
    [全电影导引](../Page/全电影导引.md "wikilink")

  -
  -
  -
  -
  -
  - [《沙丘》](http://www.digitalmonkeybox.com/dune.htm) DVD评论 （[Digital
    Monkey Box](../Page/Digital_Monkey_Box.md "wikilink")）

[Category:1984年電影](../Category/1984年電影.md "wikilink")
[Category:美國科幻動作片](../Category/美國科幻動作片.md "wikilink")
[Category:邪典电影](../Category/邪典电影.md "wikilink") [Category:沙丘
(小說)](../Category/沙丘_\(小說\).md "wikilink")
[Category:科幻小說改編電影](../Category/科幻小說改編電影.md "wikilink")
[Category:大卫·林奇电影](../Category/大卫·林奇电影.md "wikilink")

1.  [Jurgen Prochnow
    約根柏契納](http://app.atmovies.com.tw/star/star.cfm?action=stardata&starid=sJP5003513)
     - @movie[開眼電影網](../Page/開眼電影網.md "wikilink")
2.  [Jurgen
    Prochnow](http://www.moviex.com.hk/movieSearch.php?ai%5B%5D=5920)  -
    MoviEx
3.  [尤尔根·普洛斯诺 Jürgen Prochnow](http://people.mtime.com/943439) -
    Mtime[时光网](../Page/时光网.md "wikilink")