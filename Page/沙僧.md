[Sha_Wujing_in_Beijing_opera.JPG](https://zh.wikipedia.org/wiki/File:Sha_Wujing_in_Beijing_opera.JPG "fig:Sha_Wujing_in_Beijing_opera.JPG")

[Xyj-shaseng.jpg](https://zh.wikipedia.org/wiki/File:Xyj-shaseng.jpg "fig:Xyj-shaseng.jpg")
**沙僧**，又叫**沙和尚**，法号**悟净**，其前身为**捲簾大將軍**，是[中国古典小说](../Page/中国.md "wikilink")《[西遊記](../Page/西遊記.md "wikilink")》中的人物，他是[唐僧在](../Page/唐僧.md "wikilink")[流沙河收的徒弟](../Page/流沙河_\(西游记\).md "wikilink")。

## 名稱

  - 沙悟淨、沙僧、沙和尚
  - 捲簾大將：沙僧被貶下凡之前的職務，為該小說自創，實際上道教中原本並沒有捲簾大將。
  - 金身羅漢：沙僧取經完成之後受封職務。
  - 土、黃婆、刀圭：《西遊記》中以五行對應唐僧師徒五人（含白龍馬），沙僧對應“土”，書中又稱“黃婆”、“刀圭”。如：第40回“猿马刀圭木母空”；第53回“黄婆运水解邪胎”；第88回“心猿木土授门人”；第89回“金木土计闹豹头山”。

## 簡介

原是[天庭武将](../Page/天庭.md "wikilink")，受封**卷帘大将**，失手打碎琉璃盏被贬[下凡](../Page/下凡.md "wikilink")，盘踞在流沙河成為[妖怪](../Page/妖怪.md "wikilink")，以人为食，项下挂着九個[骷髅](../Page/骷髅.md "wikilink")，串為[項鍊](../Page/項鍊.md "wikilink")\[1\]。其後被觀世音菩薩指點成为唐僧徒弟之后与师傅、师兄[孙悟空](../Page/孙悟空.md "wikilink")、[猪八戒以及](../Page/猪八戒.md "wikilink")[白龙马一起赴西天取经](../Page/白龙马.md "wikilink")，取经途中，沙僧勤勤恳恳任劳任怨，历经九九八十一难后，功德圆满，獲封为金身[罗汉](../Page/罗汉.md "wikilink")。

## 原型

[鲁迅认为沙僧的原型来自](../Page/鲁迅.md "wikilink")《大唐三藏取经诗话》里面的**深沙神**。[陳寅恪認為沙和尚这一形象](../Page/陳寅恪.md "wikilink")，当来自《慈恩法师传》（《大慈恩寺三藏法師傳》），该传记实际又受到《波罗密心经》的影响，“沙和尚是师徒四个人中性格刻画最不成功的”。\[2\]

## 演化

在日本，沙僧的形象，進一步與當地傳說動物[河童結合](../Page/河童.md "wikilink")。

## 武器

**降妖寶杖**\[3\]為沙僧使用武器，由[魯班以](../Page/魯班.md "wikilink")[吳剛砍下的桂樹枝所製](../Page/吳剛_\(道教\).md "wikilink")，外型兩端細中間粗，曾被妖怪形容像[擀麵棍](../Page/擀麵杖.md "wikilink")\[4\]。
但在各類文藝作品中，武器形象則多為[月牙铲](../Page/月牙铲.md "wikilink")。据原著描述，该杖同[九齿钉耙一样重](../Page/九齿钉耙.md "wikilink")。

## 演出

  - [盤絲洞](../Page/盤絲洞_\(1927年電影\).md "wikilink")
    （1927年），[但二春](../Page/但二春.md "wikilink") 飾演
  - [孫悟空](../Page/孫悟空_\(1940年日本電影\).md "wikilink") 前篇 （1940年）
  - [孫悟空](../Page/孫悟空_\(1940年日本電影\).md "wikilink") 後篇 （1940年）
  - [西遊記 (1986年電視劇)](../Page/西遊記_\(1986年電視劇\).md "wikilink")
    [閆懷禮飾演](../Page/閆懷禮.md "wikilink")
  - [西遊記 (1999年電視劇)](../Page/西遊記_\(1999年電視劇\).md "wikilink")
    [刘大刚飾演](../Page/刘大刚.md "wikilink")
  - [西遊記後傳](../Page/西遊記後傳.md "wikilink")
    [李京飾演](../Page/李京.md "wikilink")
  - [吴承恩与西遊記](../Page/吴承恩与西遊記.md "wikilink")
    [刘大刚飾演](../Page/刘大刚.md "wikilink")
  - [西遊記 (2010年電視劇)](../Page/西遊記_\(2010年電視劇\).md "wikilink")
    [牟凤彬飾演](../Page/牟凤彬.md "wikilink")
  - [西遊記 (2011年電視劇)](../Page/西遊記_\(2011年電視劇\).md "wikilink")
    [徐锦江飾演](../Page/徐锦江.md "wikilink")
  - [西遊記 (1996年電視劇)](../Page/西遊記_\(1996年電視劇\).md "wikilink")
    [麥長青飾演](../Page/麥長青.md "wikilink")
  - [西遊記（貳）](../Page/西遊記（貳）.md "wikilink")
    [麥長青飾演](../Page/麥長青.md "wikilink")
  - [齊天大聖孫悟空](../Page/齊天大聖孫悟空.md "wikilink")
    [李璨琛飾演](../Page/李璨琛.md "wikilink")
  - [西行平妖](../Page/西行平妖.md "wikilink")
    [杜玉明飾演](../Page/杜玉明.md "wikilink")
  - [大話西遊](../Page/大話西遊_\(電影\).md "wikilink")
    [江約誠飾演](../Page/江約誠.md "wikilink")
  - [情癫大圣](../Page/情癫大圣.md "wikilink")
    [张致恒飾演](../Page/张致恒.md "wikilink")
  - [西遊·降魔篇](../Page/西游·降魔篇.md "wikilink")，[李尚正飾演](../Page/李尚正.md "wikilink")
  - [西游·伏妖篇](../Page/西游伏妖篇.md "wikilink")，[巴特尔饰演](../Page/孟克·巴特尔.md "wikilink")
  - [萬萬沒想到：西遊篇](../Page/萬萬沒想到：西遊篇.md "wikilink")，[易振興飾演](../Page/易振興.md "wikilink")
  - [西游记系列](../Page/西游记系列.md "wikilink")，[岸部四郎飾演](../Page/岸部四郎.md "wikilink")
  - [西遊記 (1993年電視劇)](../Page/西游记系列.md "wikilink")，飾演
  - [新西遊記](../Page/新西遊記.md "wikilink")　[柄本明飾演](../Page/柄本明.md "wikilink")
  - [西遊記
    (2006年電視劇)](../Page/西遊記_\(2006年電視劇\).md "wikilink")　[内村光良飾演](../Page/内村光良.md "wikilink")
  - [和遊記](../Page/和遊記.md "wikilink")
    [張光](../Page/張光_\(演員\).md "wikilink") 飾演

## 注釋

[Category:西遊記角色](../Category/西遊記角色.md "wikilink")
[Category:虚构妖怪](../Category/虚构妖怪.md "wikilink")

1.  《大唐三藏取经诗话》说沙僧的脖子上那串骷髅是三藏法师的前身
2.  陈寅恪：《金明馆丛稿二编》第166-170页。
3.  《西遊記》原著第22回：「寶杖原來名譽大，本是月裡梭羅派。吳剛伐下一枝來，魯班制造工夫蓋。裡邊一條金趁心，外邊萬道珠絲玠。名稱寶杖善降妖，永鎮靈霄能伏怪。
    只因官拜大將軍，玉皇賜我隨身帶。或長或短任吾心，要細要粗憑意態。也曾護駕宴蟠桃，也曾隨朝居上界。值殿曾經眾聖參，卷簾曾見諸仙拜。
    養成靈性一神兵，不是人間凡器械。自從遭貶下天門，任意縱橫游海外。不當大膽自稱誇，天下槍刀難比賽。看你那個鏽釘鈀，只好鋤田與築菜。」
4.  《西遊記》原著第49回：「妖邪道：『你这个模样，象一个磨博士出身。』沙僧道：『如何认得我象个磨博士？』
    妖邪道：『你不是磨博士，怎么会使擀麵杖？』沙僧骂道：『你这孽障，是也不曾见！这般兵器人间少，故此难知宝杖名。出自月宫无影处，梭罗仙木琢磨成。外边嵌宝霞光耀，内里钻金瑞气凝。先日也曾陪御宴，今朝秉正保唐僧。西方路上无知识，上界宫中有大名。唤做降妖真宝杖，管教一下碎天灵！』」