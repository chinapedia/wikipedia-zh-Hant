**沙查·諾姆·巴隆·科恩**（，）是一名[英国](../Page/英国.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[喜剧演员](../Page/喜剧演员.md "wikilink")、[編劇和](../Page/編劇.md "wikilink")[監製](../Page/監製.md "wikilink")。因在他的[英國第四台電視劇](../Page/英國第四台.md "wikilink")《》上出演一系列成功而充满争议的角色而出名，其中的Ali
G、[波拉特及](../Page/波拉特.md "wikilink")[布魯諾更被搬上了大銀幕](../Page/波兒出城之妖壇教祖3點「畢露」搞硬美國佬.md "wikilink")。科恩更因在《[波拉特：为建设伟大祖国哈萨克斯坦而学习美国文化](../Page/波拉特_\(电影\).md "wikilink")》中演活波拉特一角而獲得多項獎項，包括[金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")，並提名了[奧斯卡最佳改編劇本獎](../Page/奧斯卡最佳改編劇本獎.md "wikilink")。

這些富争议性的角色最初只出現於《Da Ali G
Show》中，但自2002年的《》開始，到《[波叔出城](../Page/波叔出城.md "wikilink")》及《[布魯諾出城](../Page/波兒出城之妖壇教祖3點「畢露」搞硬美國佬.md "wikilink")》，高漢很有可能會把電視劇中的所有角色都改編成電影。2011年後，他又出演了《[雨果的冒險](../Page/雨果的冒險.md "wikilink")》（2011年）、《[大鈍裁者](../Page/大鈍裁者.md "wikilink")》（2012年）及《[悲慘世界](../Page/悲慘世界_\(2012年電影\).md "wikilink")》（2012年）。

## 早年生平

柯恩出生于英国伦敦[漢默史密斯](../Page/漢默史密斯.md "wikilink")，毕业于[剑桥大学基督学院历史系](../Page/剑桥大学基督学院.md "wikilink")。他的母親Daniella
Naomi（娘家姓Weiser）是猶太人，出生於[以色列](../Page/以色列.md "wikilink")，是運動教練\[1\]\[2\]。他的父親Gerald
Baron
Cohen是[猶太人](../Page/猶太人.md "wikilink")，出生於[倫敦並住在](../Page/倫敦.md "wikilink")[威爾士](../Page/威爾士.md "wikilink")，經營著一間服飾店\[3\]\[4\]\[5\]。他的哥哥Erran
Baron Cohen是作曲家\[6\]。他的外祖母曾經是芭蕾舞者，現在居住在以色列\[7\]。

柯恩是犹太人\[8\]\[9\]\[10\]。他在一個正統派猶太教（Orthodox
Jewish）家庭長大\[11\]\[12\]，而且能說一口流利的[希伯來語](../Page/希伯來語.md "wikilink")\[13\]。是三兄弟中年齡最小的\[14\]。

## 作品列表

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>標題</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1996</p></td>
<td><p>Punch</p></td>
<td><p>無名角色</p></td>
<td><p>短片</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p>The Jolly Boys' Last Stand</p></td>
<td><p>Vinnie</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td></td>
<td><p><a href="../Page/阿里G.md" title="wikilink">阿里G</a><br />
<a href="../Page/芭樂特·薩格季耶夫.md" title="wikilink">芭樂特·薩格季耶夫</a></p></td>
<td><p>兼編劇和執行製片人</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>Spyz</p></td>
<td><p><a href="../Page/詹姆士·龐德.md" title="wikilink">詹姆士·龐德</a>（<a href="../Page/阿里G.md" title="wikilink">阿里G</a>）</p></td>
<td><p>短片；兼編劇和監製</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/馬達加斯加_(2005年電影).md" title="wikilink">馬達加斯加</a></p></td>
<td><p>King Julien XIII</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/王牌飆風.md" title="wikilink">王牌飆風</a></p></td>
<td><p>Jean Girard</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/芭樂特：哈薩克青年必修（理）美國文化.md" title="wikilink">芭樂特</a></p></td>
<td><p><a href="../Page/芭樂特·薩格季耶夫.md" title="wikilink">芭樂特·薩格季耶夫</a></p></td>
<td><p>兼編劇和監製</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/瘋狂理髮師：倫敦首席惡魔剃刀手.md" title="wikilink">瘋狂理髮師：倫敦首席惡魔剃刀手</a></p></td>
<td><p><a href="../Page/阿道夫·皮雷利.md" title="wikilink">阿道夫·皮雷利紳士</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/馬達加斯加2.md" title="wikilink">馬達加斯加2</a></p></td>
<td><p>King Julien XIII</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/G型教主.md" title="wikilink">G型教主</a></p></td>
<td><p><a href="../Page/布魯諾·格哈德.md" title="wikilink">布魯諾·格哈德</a></p></td>
<td><p>兼編劇和監製</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/雨果的冒險.md" title="wikilink">雨果的冒險</a></p></td>
<td><p>Inspector Gustav</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/大獨裁者落難記.md" title="wikilink">大獨裁者落難記</a></p></td>
<td><p>Admiral General Haffaz Aladeen</p></td>
<td><p>兼編劇和監製</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/馬達加斯加3：歐洲大圍捕.md" title="wikilink">馬達加斯加3：歐洲大圍捕</a></p></td>
<td><p>King Julien XIII</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/悲慘世界_(2012年電影).md" title="wikilink">悲慘世界</a></p></td>
<td><p><a href="../Page/泰納第.md" title="wikilink">泰納第</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/銀幕大角頭2：傳奇再續.md" title="wikilink">銀幕大角頭2：傳奇再續</a></p></td>
<td><p>BBC新聞記者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/特務大臨演.md" title="wikilink">特務大臨演</a></p></td>
<td><p>Norman "Nobby" Grimsby</p></td>
<td><p>兼編劇和監製</p></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/魔境夢遊：時光怪客.md" title="wikilink">魔境夢遊：時光怪客</a></p></td>
<td><p>Time</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>標題</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1995</p></td>
<td><p>Jack and Jeremy's Police 4</p></td>
<td><p>Execution victim</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p>Pump TV</p></td>
<td><p>主持人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p>Live from the Lighthouse</p></td>
<td><p><a href="../Page/阿里G.md" title="wikilink">阿里G</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/The_11_O&#39;Clock_Show.md" title="wikilink">The 11 O'Clock Show</a></p></td>
<td><p><a href="../Page/阿里G.md" title="wikilink">阿里G</a></p></td>
<td><p>編劇</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td></td>
<td><p><a href="../Page/阿里G.md" title="wikilink">阿里G</a><br />
<a href="../Page/芭樂特·薩格季耶夫.md" title="wikilink">芭樂特·薩格季耶夫</a><br />
<a href="../Page/布魯諾·格哈德.md" title="wikilink">布魯諾·格哈德</a></p></td>
<td><p>6集<br />
編劇</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>（美國）</p></td>
<td><p><a href="../Page/阿里G.md" title="wikilink">阿里G</a><br />
<a href="../Page/芭樂特·薩格季耶夫.md" title="wikilink">芭樂特·薩格季耶夫</a><br />
<a href="../Page/布魯諾·格哈德.md" title="wikilink">布魯諾·格哈德</a></p></td>
<td><p>12集<br />
兼編劇和執行製片人</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/人生如戲.md" title="wikilink">人生如戲</a></p></td>
<td><p>Larry's guide</p></td>
<td><p>第5季，第10集：「The End」</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>Night of Too Many Stars</p></td>
<td><p><a href="../Page/芭樂特·薩格季耶夫.md" title="wikilink">芭樂特·薩格季耶夫</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/辛普森家庭.md" title="wikilink">辛普森家庭</a></p></td>
<td><p>Jakob[15]</p></td>
<td><p>配音<br />
單集: 「The Greatest Story Ever D'ohed」</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/職棒鮮師.md" title="wikilink">職棒鮮師</a></p></td>
<td><p>Ronnie Thelman</p></td>
<td><p>第4季，第8集：「大結局」</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>Ali G: Rezurection</p></td>
<td><p>阿里G</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>Highston</p></td>
<td><p>-</p></td>
<td><p>執行製片人</p></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/盖瑞·山德林的禅意日记.md" title="wikilink">盖瑞·山德林的禅意日记</a></p></td>
<td><p>他自己</p></td>
<td><p>迷你紀錄劇</p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p><a href="../Page/Who_Is_America?.md" title="wikilink">Who Is America?</a></p></td>
<td><p>Billy Wayne Ruddick Jr., PhD<br />
Dr. Nira Cain-N'Degeocello<br />
Rick Sherman<br />
Erran Morad</p></td>
<td><p>導演、創作者、編劇和執行製片人</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 爭議

### 種族歧視

科恩在88屆[奧斯卡頒獎典禮上台擔任引言人時曾說](../Page/奧斯卡頒獎典禮.md "wikilink")：「為什麼奧斯卡不給那些認真工作的小屌（tiny
dongs）黃種人？你知道的，『小小兵』。」一口氣踩到歧視亞裔和性暗示兩條紅線\[16\]。

## 參考文獻

## 外部链接

  -
  - – biography and credits

  - [沙查·巴隆·科恩](http://www.discogs.com/artist/Sacha+Baron+Cohen)
    discography at [Discogs](../Page/Discogs.md "wikilink")

  - [Sacha Baron Cohen audio
    interview](http://www.npr.org/templates/story/story.php?storyId=3613548)
    with [NPR](../Page/NPR.md "wikilink")'s [Robert
    Siegel](../Page/Robert_Siegel.md "wikilink")

  - [*And Now for the World According to Borat and Sacha Baron
    Cohen*](http://www.authorsden.com/visit/viewarticle.asp?AuthorID=25279&id=28405),
    *Authors Den*, 2007-03-24

  - [*Sacha Baron Cohen: Killing off
    Borat*](http://www.telegraph.co.uk/arts/main.jhtml?xml=/arts/2007/12/21/bfborat121.xml),
    article at *[The Daily
    Telegraph](../Page/The_Daily_Telegraph.md "wikilink")*, 2007-12-21

[Category:1971年出生](../Category/1971年出生.md "wikilink")
[Category:英国电影演员](../Category/英国电影演员.md "wikilink")
[Category:英國電視演員](../Category/英國電視演員.md "wikilink")
[Category:英國喜劇演員](../Category/英國喜劇演員.md "wikilink")
[Category:英國犹太人](../Category/英國犹太人.md "wikilink")
[Category:劍橋大學校友](../Category/劍橋大學校友.md "wikilink")
[Category:金球獎獲得者](../Category/金球獎獲得者.md "wikilink")
[Category:英國電影學院獎得主](../Category/英國電影學院獎得主.md "wikilink")
[Category:金球奖最佳音乐或喜剧男主角奖得主](../Category/金球奖最佳音乐或喜剧男主角奖得主.md "wikilink")
[Category:英格蘭電視編劇](../Category/英格蘭電視編劇.md "wikilink")
[Category:时代百大人物](../Category/时代百大人物.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15. [The Greatest Story Ever D'ohed
    (\#21.16)](http://www.imdb.com/title/tt1614895/). *IMDB*.
16. [1](http://udn.com/news/story/9597/1567216-童工、會計師、小小兵…那一夜，奧斯卡玩亞裔)