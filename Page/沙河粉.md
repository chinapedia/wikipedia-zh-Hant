[HofanUpClose.jpg](https://zh.wikipedia.org/wiki/File:HofanUpClose.jpg "fig:HofanUpClose.jpg")
[都匀小吃_粉.jpg](https://zh.wikipedia.org/wiki/File:都匀小吃_粉.jpg "fig:都匀小吃_粉.jpg")小吃粉\]\]

**沙河粉**，簡稱**河粉**、**河**或**粉**，是一種通過蒸煮米漿而成的粉條食品，是[中国南方和](../Page/中国南方.md "wikilink")[港澳](../Page/港澳.md "wikilink")、[东南亚一带常见的一种](../Page/东南亚.md "wikilink")[食品](../Page/食品.md "wikilink")，常見的煮法為炒或水煮後配湯，例如炒牛河、上湯河粉。正宗山水沙河粉的原产[广州](../Page/广州.md "wikilink")[沙河镇](../Page/沙河街道_\(广州市\).md "wikilink")。

## 制作方法

[Chow_Kueh_Teow.JPG](https://zh.wikipedia.org/wiki/File:Chow_Kueh_Teow.JPG "fig:Chow_Kueh_Teow.JPG")

沙河粉原料是[大米](../Page/大米.md "wikilink")，将米洗净後磨成粉加水制成米浆，用米浆蒸成薄粉皮、再切成带状而成。，所产的粉薄白透明，爽软韧筋兼备，炒、泡、拌食皆宜。

## 烹调方法

[Wok_Cooking.jpg](https://zh.wikipedia.org/wiki/File:Wok_Cooking.jpg "fig:Wok_Cooking.jpg")
沙河粉可以用[蒸](../Page/蒸.md "wikilink")、[炒](../Page/炒.md "wikilink")（分干炒和湿炒两种）、[煮等多种方法烹调](../Page/煮.md "wikilink")。　

  - 乾炒：加油配[韭黃以猛火炒熟](../Page/韭黃.md "wikilink")，加上[酱油](../Page/酱油.md "wikilink")，可配[牛肉](../Page/牛肉.md "wikilink")、[牛腩](../Page/牛腩.md "wikilink")、猪什等，其中以[乾炒牛河最為著名](../Page/乾炒牛河.md "wikilink")。
  - 湿炒：在干炒后，打上[芡汁](../Page/芡汁.md "wikilink")。
  - 煮：與一般湯麵麵食類似，煮熟后配上[大地魚](../Page/大地魚.md "wikilink")[上湯作湯底](../Page/上湯.md "wikilink")，再將河粉佐以[魚丸](../Page/魚丸.md "wikilink")、[牛丸](../Page/牛丸.md "wikilink")、[雲吞或其他肉類等配料](../Page/雲吞.md "wikilink")。在廣東各地的粉麵食店中相當普遍，店家一般會供客人作為麵食之中的一種選擇（[粗麵](../Page/粗麵.md "wikilink")/[幼麵](../Page/幼麵.md "wikilink")/[米粉](../Page/米粉.md "wikilink")/河粉）。
  - 蒸：蒸熟后，自行醮酱。

## 现状

[Sha_Ho_Fun_in_Ipoh.jpg](https://zh.wikipedia.org/wiki/File:Sha_Ho_Fun_in_Ipoh.jpg "fig:Sha_Ho_Fun_in_Ipoh.jpg")
手工制成的沙河粉色白，近来也有店家在其中加入各种[蔬菜汁或者](../Page/蔬菜.md "wikilink")[水果汁](../Page/水果.md "wikilink")，制成五颜六色的沙河粉。沙河粉已实现机械化生产，在广州随之出现了大量自行生产的米粉，部分厂家甚至在陈化米中加入[硼砂制作](../Page/硼砂.md "wikilink")，影响了饮食安全。

2004年，广州市政府考虑将沙河粉以“广州沙河粉”之名申报“原产地域产品”保护，从而加强对沙河粉制造业的管理。\[1\]
2009年，炒河粉被[洛杉矶时报评为](../Page/洛杉矶时报.md "wikilink")“年度十大食谱”冠军。

沙河粉也隨著20世紀初，大量的[客家與](../Page/客家.md "wikilink")[廣東移民至](../Page/廣東.md "wikilink")[馬來半島](../Page/馬來半島.md "wikilink")，在當地也發展出許多麵食，其中最有名的是[馬來西亞](../Page/馬來西亞.md "wikilink")[霹靂州的](../Page/霹靂州.md "wikilink")[雞絲河粉](../Page/雞絲河粉.md "wikilink")、[芽菜雞等](../Page/芽菜雞.md "wikilink")。

## 其他称谓

河粉和其他地方的同类食品，如[客家人的](../Page/客家人.md "wikilink")[粄条](../Page/粄条.md "wikilink")、[闽南人的](../Page/闽南人.md "wikilink")[粿条极为相似](../Page/粿条.md "wikilink")，甚至无法分辨，以致很多时候，在多种方言交叠的地区，[粄条](../Page/粄条.md "wikilink")、[粿条和河粉都被视为同一种食物](../Page/粿条.md "wikilink")，例如在马、新一帶，在福建人、广府人、潮州人和客家人生活交融的环境，河粉和粿条可说是同义词。

## 参见

  - [雞絲河粉](../Page/雞絲河粉.md "wikilink")
  - [芽菜雞](../Page/芽菜雞.md "wikilink")
  - [乾炒牛河](../Page/乾炒牛河.md "wikilink")
  - [米線](../Page/米線.md "wikilink")
  - [米粉](../Page/米粉.md "wikilink")
  - [稻米\#麵條類米製品](../Page/稻米#麵條類米製品.md "wikilink")
  - [粄條](../Page/粄條.md "wikilink")
  - [粿条](../Page/粿条.md "wikilink")

## 参考资料

## 外部链接

[Category:沙河粉](../Category/沙河粉.md "wikilink")
[Category:廣東小吃](../Category/廣東小吃.md "wikilink")
[Category:馬來西亞食品](../Category/馬來西亞食品.md "wikilink")

1.  [广东新闻：产权保护沙河粉将申请地域保护不让随意标称](http://www.southcn.com/news/gdnews/nanyuedadi/200304020647.htm)