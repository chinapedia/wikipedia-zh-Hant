[Shenzhen_Konka_Building.JPG](https://zh.wikipedia.org/wiki/File:Shenzhen_Konka_Building.JPG "fig:Shenzhen_Konka_Building.JPG")旧總部（已拆除）\]\]
[Shenzhen_Happy_Line_Monorail_Intamin_1.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Happy_Line_Monorail_Intamin_1.jpg "fig:Shenzhen_Happy_Line_Monorail_Intamin_1.jpg")
**沙河街道**是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[南山區下辖的一个](../Page/南山区_\(深圳市\).md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")，位于南山区东部，1984年3月成立，範圍包括了由僑城東至沙河一帶區域。

## 地理

辖区面积27平方公里，其中填海区约4.40平方公里，山地约5.08平方公里。辖区设有社区工作站13个，社区居委会17个；辖区总人口约19.37万人，户籍居民62,387人。　

## 經濟

区内有[世界之窗](../Page/世界之窗.md "wikilink")、[欢乐谷](../Page/欢乐谷.md "wikilink")、[锦绣中华和](../Page/锦绣中华.md "wikilink")[中国民俗文化村等旅游景点](../Page/中国民俗文化村.md "wikilink")。另外還有[华侨城集团](../Page/华侨城集团.md "wikilink")、[康佳集团和](../Page/康佳集团.md "wikilink")[沙河集团等著名企業](../Page/沙河集团.md "wikilink")。

## 交通

### 道路

  - [深南路](../Page/深南路.md "wikilink")

### 地鐵

  - [深圳地鐵1號線](../Page/深圳地鐵1號線.md "wikilink")
      - [華僑城站](../Page/華僑城站.md "wikilink")
      - [世界之窗站](../Page/世界之窗站.md "wikilink")
      - [白石洲站](../Page/白石洲站.md "wikilink")
  - [深圳地鐵2號線](../Page/深圳地鐵2號線.md "wikilink")
      - [世界之窗站](../Page/世界之窗站.md "wikilink")
      - [紅樹灣站](../Page/紅樹灣站.md "wikilink")
  - [深圳地鐵11號線](../Page/深圳地鐵11號線.md "wikilink")
      - [紅樹灣南站](../Page/紅樹灣南站.md "wikilink")

## 参考资料

1.  [街道简介](https://web.archive.org/web/20140221225000/http://www.szns.gov.cn/publish/shc/3001/3002/3003/index.html)，沙河街道办事处网站，2014年2月11日造访。

## 外部链接

  - [沙河街道办事处网站](https://web.archive.org/web/20080421235310/http://www.szns.gov.cn/shjdb/)

[Category:深圳市南山区行政区划](../Category/深圳市南山区行政区划.md "wikilink")
[南山区](../Category/深圳街道_\(行政区划\).md "wikilink")