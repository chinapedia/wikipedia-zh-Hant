**沙瀝公路**（英文：**Sha Lek
Highway**）是連接[沙田路](../Page/沙田路.md "wikilink")[沙田圍及](../Page/沙田圍.md "wikilink")[小瀝源](../Page/小瀝源.md "wikilink")[大老山公路及](../Page/大老山公路.md "wikilink")[大老山隧道交界](../Page/大老山隧道.md "wikilink")，為[沙田路的分支公路](../Page/沙田路.md "wikilink")。全長1.5公里，全線均為二或三線雙程分隔[公路](../Page/公路.md "wikilink")。限制為每小時80公里。

公路由[1號幹線](../Page/香港1號幹線.md "wikilink")[沙田路](../Page/沙田路.md "wikilink")[沙田圍入口起](../Page/沙田圍.md "wikilink")，以高架橋形式在[沙田圍路上面](../Page/沙田圍路.md "wikilink")，然後前往位於[小瀝源的](../Page/小瀝源.md "wikilink")[2號幹線的](../Page/香港2號幹線.md "wikilink")[大老山公路及](../Page/大老山公路.md "wikilink")[大老山隧道交界](../Page/大老山隧道.md "wikilink")。

沙瀝公路是香港首條未被納入任何[幹線系統的](../Page/香港幹線編號系統.md "wikilink")[快速公路](../Page/快速公路.md "wikilink")\[1\]，另一條則為[竹篙灣公路](../Page/竹篙灣公路.md "wikilink")。

## 途經的公共交通服務

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參考資料

[Category:香港高速公路](../Category/香港高速公路.md "wikilink")
[Category:沙田區街道](../Category/沙田區街道.md "wikilink")

1.