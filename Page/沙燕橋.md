**沙燕橋**（），是[香港橋樑之一](../Page/香港橋樑.md "wikilink")，[沙田鄉事會路的一部分](../Page/沙田鄉事會路.md "wikilink")，以[1980年代初戰績彪炳的首支華人少年棒球隊](../Page/1980年代香港.md "wikilink")「[沙燕隊](../Page/沙田體育會#屬會.md "wikilink")」命名。沙燕橋位於[沙田](../Page/沙田.md "wikilink")[城門河中游之上](../Page/城門河.md "wikilink")，貫通往來[沙田市中心與](../Page/沙田市中心.md "wikilink")[沙田圍等地的交通](../Page/沙田圍.md "wikilink")。

## 建築

沙燕橋實際上是[沙田鄉事會路的其中一段](../Page/沙田鄉事會路.md "wikilink")，並非獨立街道。沙燕橋西起[源禾路](../Page/源禾路.md "wikilink")，連接沙田市中心及[瀝源邨附近](../Page/瀝源邨.md "wikilink")；東至[大涌橋路](../Page/大涌橋路.md "wikilink")，接駁沙田圍一帶道路。橋上除雙向各三條[行車線外](../Page/行車線.md "wikilink")，亦設有[行人路和](../Page/行人路.md "wikilink")[單車徑](../Page/單車徑.md "wikilink")，是一道由[鋼筋](../Page/鋼筋.md "wikilink")[混凝土架構的現代](../Page/混凝土.md "wikilink")[行車天橋](../Page/行車天橋.md "wikilink")。[行人](../Page/行人.md "wikilink")、[單車及](../Page/單車.md "wikilink")[車輛使用此橋樑都](../Page/車輛.md "wikilink")[不另收費](../Page/免費.md "wikilink")。

## 命名

沙燕橋的命名是源於[沙田區的一支少年棒球隊](../Page/沙田區.md "wikilink")「沙燕隊」。該隊是香港首支華人少年棒球隊，於1982年成立，由當時沙田基覺小學的[校長](../Page/校長.md "wikilink")[盧光輝提議](../Page/盧光輝.md "wikilink")，並獲時任沙田區[政務專員的前](../Page/政務專員.md "wikilink")[行政長官](../Page/行政長官.md "wikilink")[曾蔭權撥款支持](../Page/曾蔭權.md "wikilink")。而由基層兒童組成的沙燕隊，於翌年出戰香港少棒聯盟公開賽即奪得冠軍，成績斐然。及後，[沙田區議會及](../Page/沙田區議會.md "wikilink")[民政署決定將此橋命名為](../Page/民政署.md "wikilink")「沙燕橋」，以茲紀念。\[1\]

改編自沙燕隊事跡的2016年[香港電影](../Page/香港電影.md "wikilink")[點五步](../Page/點五步.md "wikilink")，因此命名背景，特地於此橋取景。

## 鄰近主要建築物

  - [沙田公園](../Page/沙田公園.md "wikilink")
  - [源禾路體育館](../Page/源禾路體育館.md "wikilink")
  - [瀝源邨](../Page/瀝源邨.md "wikilink")
  - [河畔花園](../Page/河畔花園.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[沙田站和](../Page/沙田站.md "wikilink")[沙田圍站](../Page/沙田圍站.md "wikilink")
  - [香港資優教育學苑](../Page/香港資優教育學苑.md "wikilink")
  - [沙角邨](../Page/沙角邨.md "wikilink")

## 相關條目

  - [點五步](../Page/點五步.md "wikilink")
  - [獅子橋](../Page/獅子橋.md "wikilink")
  - [翠榕橋](../Page/翠榕橋.md "wikilink")
  - [錦龍橋](../Page/錦龍橋.md "wikilink")
  - [瀝源橋](../Page/瀝源橋.md "wikilink")

## 外部連結

  - [香港地方: 沙燕橋 (Sand Martin
    Bridge)的名字由來](http://www.hk-place.com/view.php?id=321)

## 參考資料

<div class="references-small">

<references />

</div>

[Category:香港行車天橋](../Category/香港行車天橋.md "wikilink")
[Category:香港體育](../Category/香港體育.md "wikilink")
[Category:沙田](../Category/沙田.md "wikilink")
[Category:香港跨河橋梁](../Category/香港跨河橋梁.md "wikilink")

1.