|                                                                                           |
| :---------------------------------------------------------------------------------------: |
| [Chartres_1.jpg](https://zh.wikipedia.org/wiki/File:Chartres_1.jpg "fig:Chartres_1.jpg") |
|                             [国家](../Page/国家列表.md "wikilink")                              |
|                           [大区](../Page/大区_\(法国\).md "wikilink")                           |
|                            [省](../Page/省_\(法国\).md "wikilink")                            |
|                            [邮政编码](../Page/邮政编码.md "wikilink")                             |
|                              [海拔](../Page/海拔.md "wikilink")                               |

**沙特尔**（）是[法国中北部的一座城市](../Page/法国.md "wikilink")，也是[厄尔-卢瓦省的首府](../Page/厄尔-卢瓦省.md "wikilink")。城市位于[奥尔良内地区](../Page/奥尔良内.md "wikilink")，坐落于[厄尔河附近](../Page/厄尔河.md "wikilink")。总面积16.85平方公里，2009年时的人口为39122人。\[1\]

## 历史

沙特尔是[高卢部落的](../Page/高卢人.md "wikilink")的主要城市，罗马人称之为，以[厄尔河](../Page/厄尔河.md "wikilink")（当时称）而得名。后来该城被称为（卡尔尼特人的城市）。现在的名字“沙特尔”（）就是来自于“”（卡尔尼特人）。
在[高卢为](../Page/高卢.md "wikilink")[法兰克人征服之后](../Page/法兰克人.md "wikilink")，它仍然是一个人口聚居区。858年，[诺曼人在洗劫法国沿海地区时几乎摧毁了沙特尔](../Page/诺曼人.md "wikilink")。从1286年开始，沙特尔成为法国王室的直辖领地；在[法国宗教战争中](../Page/法国宗教战争.md "wikilink")，[亨利三世被](../Page/亨利三世_\(法兰西\).md "wikilink")[吉斯公爵亨利赶出](../Page/吉斯公爵_\(第三\).md "wikilink")[巴黎后曾来此避难](../Page/巴黎.md "wikilink")。1594年，[亨利四世在沙特尔加冕](../Page/亨利四世_\(法兰西\).md "wikilink")，开创了法国历史上辉煌的[波旁王朝](../Page/波旁王朝.md "wikilink")。

## 人口

沙特尔人口变化图示
[Population_-_Municipality_code_28085.svg](https://zh.wikipedia.org/wiki/File:Population_-_Municipality_code_28085.svg "fig:Population_-_Municipality_code_28085.svg")

## 交通

[沙特尔站开行前往](../Page/沙特尔站.md "wikilink")[勒芒](../Page/勒芒站.md "wikilink")、[巴黎和](../Page/巴黎蒙帕纳斯站.md "wikilink")[蒙杜布洛三个方向的列车](../Page/蒙杜布洛站.md "wikilink")。前往[奥尔良方向的铁路预计](../Page/奥尔良.md "wikilink")2020年通车。

## 经济

以[农](../Page/农业.md "wikilink")[贸业和](../Page/商业.md "wikilink")[制造业为主](../Page/制造业.md "wikilink")。制造业的主要产品包括：机械、电子仪器、化肥。

## 市区景观

城市本身有一定坡度，在市区最高点是举世闻名的[沙特尔大教堂](../Page/沙特尔大教堂.md "wikilink")。这座教堂通常被认为是法国[哥特式教堂建筑的最经典之作](../Page/哥特式建筑.md "wikilink")。

自1976，[普伊格公司在这座城市建立了自己的生产基地](../Page/普伊格公司.md "wikilink")。\[2\]

## 行政机构

现任市长为让-皮埃尔·若尔日。

## 著名人士

  - [沙特尔的富尔谢](../Page/沙特尔的富尔谢.md "wikilink")，编年史作者，记载了[第一次十字军东征的事迹](../Page/第一次十字军东征.md "wikilink")
  - [雅克·皮埃尔·布里索](../Page/雅克·皮埃尔·布里索.md "wikilink")，[法国大革命时期的](../Page/法国大革命.md "wikilink")[吉伦特派活动家](../Page/吉伦特派.md "wikilink")

## 相关条目

  - [沙特尔大教堂](../Page/沙特尔大教堂.md "wikilink")
  - [厄尔-卢瓦省市镇列表](../Page/厄尔-卢瓦省市镇列表.md "wikilink")

## 参考文献

[C](../Category/厄尔-卢瓦省市镇.md "wikilink")

1.  [法国INSEE人口数据-2009年](http://www.insee.fr/fr/ppp/bases-de-donnees/recensement/populations-legales/france-departements.asp?annee=2009)
2.