**沙田T4主幹道**為一條計劃中的道路，連接[1號幹線的](../Page/香港1號幹線.md "wikilink")[沙田路及](../Page/沙田路.md "wikilink")[9號幹線的](../Page/香港9號幹線.md "wikilink")[城門隧道公路](../Page/城門隧道公路.md "wikilink")、[8號幹線的](../Page/香港8號幹線.md "wikilink")[青沙公路](../Page/青沙公路.md "wikilink")，以高架形式建造，全長1.7公里。[土木工程拓展署](../Page/土木工程拓展署.md "wikilink")2004年提出的6億建造計劃一直建遭沙田及大圍多個屋苑居民強烈反對，擔心高架路影響景觀及造成污染等，計劃曾於2006年刊憲，但造價升至11億元。\[1\]

到2018年1月，[土木工程拓展署決定重新研究計劃](../Page/土木工程拓展署.md "wikilink")，並維持原來走線，為減低對附近民居的影響，新方案會降低幹道的行車天橋高度，部分路段亦改以隧道形式興建，預計研究為期3年。\[2\]

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [土木工程拓展署
    道路規劃圖](http://www.cedd.gov.hk/eng/projects/major/doc/7705th.pdf)

[Category:香港已擱置建設](../Category/香港已擱置建設.md "wikilink")
[Category:沙田](../Category/沙田.md "wikilink")

1.  [11億高架路玩謝60萬人](http://www2.news.sina.com.hk/cgi-bin/nw/show.cgi/4/1/1/1329083/1.html)東方日報2009-11-16
2.