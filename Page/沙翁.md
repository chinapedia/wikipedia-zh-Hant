[HK_Food_San_Yung_Sai_Ying_Pun_Star_Seafood_Rest.jpg](https://zh.wikipedia.org/wiki/File:HK_Food_San_Yung_Sai_Ying_Pun_Star_Seafood_Rest.jpg "fig:HK_Food_San_Yung_Sai_Ying_Pun_Star_Seafood_Rest.jpg")
**沙翁**又稱**炸蛋球**\[1\]、**沙壅**、**冰花蛋球**\[2\]、**琉璃蛋球**，台灣多稱之為**開口笑**，是源自[中國的油炸](../Page/中國.md "wikilink")[甜食之一](../Page/甜食.md "wikilink")，流行於中國[廣東](../Page/廣東.md "wikilink")、[河南等地](../Page/河南.md "wikilink")，又傳至[琉球群島](../Page/琉球群島.md "wikilink")。現時各地沙翁或略有不同，但都是使用[雞蛋](../Page/雞蛋.md "wikilink")、[油及](../Page/油.md "wikilink")[麵粉混合而成的](../Page/麵粉.md "wikilink")[麵團炸起後在外加](../Page/麵團.md "wikilink")[糖而成](../Page/糖.md "wikilink")。

## 不同地區的沙翁

### 廣東

炸蛋球在廣東最遲在[明朝末年已經出現](../Page/明朝.md "wikilink")，據[屈大均](../Page/屈大均.md "wikilink")《[廣東新語](../Page/廣東新語.md "wikilink")》載，當時廣東人以糯米粉加上白砂糖、豬油同煮，稱為「沙壅」\[3\]，當時沙壅也是一種賀年食品\[4\]，后来改为用面粉，而不用糯米粉，並加入雞蛋。因｢壅｣字生僻，加上炸熟后滚粘上一层白糖，犹如满头白发的老翁，故又作沙翁\[5\]\[6\]。

### 河南

在[河南](../Page/河南.md "wikilink")，炸蛋球稱為**琉璃蛋球**，與廣東沙翁的差異在於它並非在表面沾上砂糖，而是把炸好的蛋球放進煮熔的糖漿裡，讓糖漿包裹蛋球\[7\]。

### 琉球

[サーターアンダギー3.jpg](https://zh.wikipedia.org/wiki/File:サーターアンダギー3.jpg "fig:サーターアンダギー3.jpg")
沙翁又傳到[琉球](../Page/琉球群島.md "wikilink")\[8\]，[琉球語](../Page/琉球語.md "wikilink")[首里方言稱為](../Page/首里方言.md "wikilink")「****」，[宮古群島稱為](../Page/宮古群島.md "wikilink")「****」。其中「サーター」、「さた」是糖的意思\[9\]，「アンダーギー」即油炸之意\[10\]，「ぱんびん」即油炸麵食\[11\]，在日本本土又稱為「砂糖天婦羅」（）\[12\]。在[琉球國時代](../Page/琉球國.md "wikilink")，當地宮廷御廚到中國學習廚藝，其中包括沙翁的製法，回國後就以當地的食材製作沙翁\[13\]。有些琉球沙翁會用[黑糖代替白糖](../Page/黑糖.md "wikilink")\[14\]\[15\]，在店家售賣時為了區別，會把用白糖的稱為白沙翁（），用黑糖的稱為黑沙翁（。而隨着東亞人口在19世紀末期開始移居北美洲，沙翁也流傳到夏威夷，而當地人亦保留琉球人的稱呼，叫這種食物作「andagi」。

### 帛琉

[日治時代](../Page/南洋群島.md "wikilink")，來自沖繩的移民將沙翁帶到了帛琉，帛琉人稱為**Tama**，源自[日語](../Page/日語.md "wikilink")「球」的意思。

## 現況

沙翁表面脆口，內裏鬆化，而且蛋味香濃，從前在香港很多[酒樓和](../Page/酒樓.md "wikilink")[茶餐廳供應](../Page/茶餐廳.md "wikilink")\[16\]，現在已較為少見。河南琉璃蛋球和琉球的沙翁則是當地家庭小吃，在琉球也被視為[節日和](../Page/節日.md "wikilink")[祝祭的食品](../Page/祝祭.md "wikilink")，也是[納徵時代表女方的食品](../Page/納徵.md "wikilink")\[17\]。

## 類似食品

  - [泡芙](../Page/泡芙.md "wikilink")
  - [甜甜圈](../Page/甜甜圈.md "wikilink")
  - [贝奈特饼](../Page/贝奈特饼.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:油炸麵食](../Category/油炸麵食.md "wikilink")
[Category:中國甜食](../Category/中國甜食.md "wikilink")
[Category:琉球食品](../Category/琉球食品.md "wikilink")
[Category:中國賀年食品](../Category/中國賀年食品.md "wikilink")
[Category:甜甜圈](../Category/甜甜圈.md "wikilink")
[Category:夏威夷飲食](../Category/夏威夷飲食.md "wikilink")

1.
2.  [冰花蛋球
    慢火出細貨](https://ol.mingpao.com/cfm/style5.cfm?File=20071130/sta01/waa2.txt)

3.  屈大均《广东新语》：“以糯粉相杂白砂糖，入猪脂煮之，名沙壅。”

4.  屈大均《廣東新語》：「元日拜年，燒爆竹，啖煎堆白餅沙壅，飲柏酒。」

5.  [广东小吃沙翁的做法详细介绍](http://www.ttmeishi.com/xiaochi/57b8a3790933c04b.htm)

6.  [歐陽應霽《香港味道:
    不脫絲襪的奶茶》，萬里機構，2007年，ISBN9621435129, 9789621435125](http://books.google.com.hk/books?id=yNEFLt23vfwC&pg=PA131&lpg=PA131&dq=%22%E5%BB%A3%E6%9D%B1%E6%96%B0%E8%AA%9E%22+%22%E6%B2%99%E7%BF%81%22&source=bl&ots=FaUYXUCiWC&sig=aoQlwJm312pEQ0PHOCPACUXNRtQ&hl=zh-TW&ei=e2mdTLTnB4SivgPI1Zy3DQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBYQ6AEwAA#v=onepage&q=%22%E5%BB%A3%E6%9D%B1%E6%96%B0%E8%AA%9E%22%20%22%E6%B2%99%E7%BF%81%22&f=false)

7.  [琉璃蛋球](http://www.canyinshijie.com/caipu/txt/1206/13768.html)

8.  [サーターアンダギー - 沖縄うまいもの王国](http://tokusanhanbai.shop-pro.jp/?mode=f24)

9.  [沖縄名物　サーターアンダギー](http://okinawa0meibutu.ti-da.net/)

10.
11.
12. [サーターアンダギー](http://chanpurucom.gozaru.jp/01itadaki/recipe/sataangagi.html)

13.
14. [黒糖を使ったお菓子](http://kokuto.kenko-life.org/sata.html)

15. [サーターアンダーギー特集](http://www.rupinon.com/andagi.html)

16.
17.