**沙马·顺达卫**（
；），華文名**李沙馬**\[1\]，生于[曼谷](../Page/曼谷.md "wikilink")，為[泰國華裔](../Page/泰國華人.md "wikilink")。他是泰國[人民力量黨的前任領袖](../Page/人民力量黨.md "wikilink")，也是[-{A上第](../Page/泰國歷史.md "wikilink")25位（33任）首相。

## 早年

沙馬的家中有兄弟姊妹五人。他曾經就讀於和。

## 家庭

  - 父：[沙勉·順達叻威](../Page/沙勉·順達叻威.md "wikilink")（*Samien Sundaravej*）
  - 母：[翁盼·順達叻威](../Page/翁盼·順達叻威.md "wikilink")（*Umphan Sundaravej*），
  - 妻：**素叻尊女士**（***Khunying Surat
    Sundaravej***）；於泰國[正大集團任職財務顧問](../Page/正大集團.md "wikilink")，夫婦育有二子女。

## 從政簡歷

  - [泰國民主黨黨員](../Page/泰國民主黨.md "wikilink")（1968年－1976年）
  - [泰國國會眾議員](../Page/泰國國會.md "wikilink")（1973年－1975年、1976年、1979年－1983年、1986年－1990年、1992年－2000年）
  - 成立泰國人民黨並出任黨主席（1979年－2000年）
  - 擔任[泰國農業與合作部副部長](../Page/泰國農業與合作部.md "wikilink")（1975年－1976年）
  - 任[泰國內務部副部長](../Page/泰國內務部.md "wikilink")（1976年）
  - 任泰國內務部部長（1976年－1977年）
  - 任[泰國運輸部部長](../Page/泰國運輸部.md "wikilink")（1983年－1986年、1990年－1991年）
  - 任[曼谷市市长](../Page/曼谷市市长.md "wikilink")（2000年－2003年）
  - 任[泰國參議院議員](../Page/泰國參議院.md "wikilink")（2006年9月19日議會在[軍事政變之後被解散](../Page/2006年泰國軍事政變.md "wikilink")）
  - 被選為[人民力量黨主席](../Page/人民力量黨.md "wikilink")（2007年7月29日－2008年9月9日）
  - [泰國首相](../Page/泰國首相.md "wikilink")（2008年1月28日－2008年9月9日）

## 政治經歷

2006年9月19日[曼谷發生](../Page/曼谷.md "wikilink")[軍事政變後](../Page/2006年泰國軍事政變.md "wikilink")，原憲法被廢除；[泰愛泰黨後來受到](../Page/泰愛泰黨.md "wikilink")[憲章法庭的勒令解散](../Page/泰國憲章法庭.md "wikilink")，原首相[塔克辛和多數黨的領導人被禁止參與政治五年](../Page/塔克辛.md "wikilink")，而人民力量黨由於其政治影響一向微弱而得以保存。2007年7月29日，一些原泰愛泰黨國會議員宣佈加入人民力量黨角逐[國會大選](../Page/2007年泰国国会选举.md "wikilink")。

在2007年8月24日的黨內選舉大會上，沙麥被推舉為新任黨主席，而前泰愛泰黨內閣部長素拉蓬（*Surapong
Suebwonglee*）則成為新任黨秘書長與發言人。普遍認為他是[前泰相塔克辛的政治同盟](../Page/泰國首相.md "wikilink")；他本人也曾多次公開聲言他是[塔克辛的代言人](../Page/塔克辛.md "wikilink")\[2\]，該黨將繼承原泰愛泰黨的一切政治綱領，承諾一旦當選將允許[塔克辛回國並解除對他的一切指控](../Page/塔克辛.md "wikilink")。

2007年12月23日，人民力量黨在泰國國會大選中贏得228個席位，雖然少于絕對優勢241席，但高于所有對手，得以入朝組閣。 \[3\]

2008年1月29日在[下議院中獲選為](../Page/下議院.md "wikilink")[泰國首相](../Page/泰國首相.md "wikilink")。8月尾爆發[政治危機](../Page/2008年泰國政治危機.md "wikilink")，沙馬稱自己是民主選出，堅拒下台。同年9月初，沙馬被舉報曾任民營[電視台受薪](../Page/電視台.md "wikilink")[烹飪節目](../Page/烹飪節目.md "wikilink")[主持人](../Page/主持人.md "wikilink")，涉嫌違反泰國[憲法](../Page/憲法.md "wikilink")，受[法院審查](../Page/法院.md "wikilink")。法庭在9月9日判決沙馬違憲，即時解除相職。執政黨其後推舉[塔克辛妹夫](../Page/塔克辛.md "wikilink")[宋才·旺沙瓦接任](../Page/宋才·旺沙瓦.md "wikilink")。\[4\]

2009年11月24日，因[肝癌去世](../Page/肝癌.md "wikilink")\[5\]。

## 参考文献

## 参见

  - [人民力量党](../Page/人民力量党.md "wikilink")
  - [2006年泰國軍事政變](../Page/2006年泰國軍事政變.md "wikilink")
  - [2008年泰國政治危機](../Page/2008年泰國政治危機.md "wikilink")

{{-}}

[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:泰國法政大學校友](../Category/泰國法政大學校友.md "wikilink")
[Category:朱拉隆功大學校友](../Category/朱拉隆功大學校友.md "wikilink")
[Category:泰國佛教徒](../Category/泰國佛教徒.md "wikilink")
[Category:曼谷人](../Category/曼谷人.md "wikilink")
[李沙马](../Category/泰國潮汕人.md "wikilink")
[沙馬](../Category/李姓.md "wikilink")
[Category:罹患肝癌逝世者](../Category/罹患肝癌逝世者.md "wikilink")

1.
2.  Darren Schuettler,
    [「塔克辛的政治同盟」準備在泰國軍事政變後首次民主大選中有所作為](http://www.swissinfo.ch/eng/news/international/Thaksin_looms_over_Thailand_s_post_coup_vote.html?siteSect=143&sid=8563032&cKey=1198392761000&ty=ti)，路透社消息。
3.  [塔克辛的政治同盟贏得了泰國的國會大選](http://news.bbc.co.uk/2/hi/asia-pacific/7158354.stm)，英國廣播公司2007年12月23日。
4.
5.