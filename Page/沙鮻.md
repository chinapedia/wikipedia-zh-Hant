**沙鮻**(*Sillaginidae*)，俗稱**沙腸仔**，是一種[鱸目的](../Page/鱸目.md "wikilink")[海鱼](../Page/海鱼.md "wikilink")，亦泛指所有屬於**沙鮻科**的[魚類](../Page/魚類.md "wikilink")。沙鮻科又名**鱚科**。

沙鮻魚的身体呈细长的[圆柱形](../Page/圆柱.md "wikilink")，頭部作錐體狀，魚鱗和口部都較小。沙鮻魚的特性是极容易受惊吓，而且受惊吓時會躲進海底的沙泥裡\[1\]。沙鮻魚杂食，攝食泥地的小[蝦](../Page/蝦.md "wikilink")、[甲殼類](../Page/甲殼類.md "wikilink")、[多毛類等底棲動物](../Page/多毛類.md "wikilink")\[2\]。

沙鮻常見於[西太平洋及](../Page/太平洋.md "wikilink")[印度洋的淺海海床](../Page/印度洋.md "wikilink")。由於沙鮻魚通常背部都較深色，但魚肚則是白色，所以在[澳大利亞及](../Page/澳大利亞.md "wikilink")[印度的人](../Page/印度.md "wikilink")，會把當地常見的一種沙鮻魚稱為Whiting。

## 種類

鱚科共有5屬35種，當中絕大部份的31種都屬於鱚属。其餘的4個種，分別屬於各自的屬。 \[3\] ：

  - 鱚科
      - [鱚属](../Page/鱚属.md "wikilink")
          - [雜色鱚](../Page/雜色鱚.md "wikilink")/[星沙鮻](../Page/星沙鮻.md "wikilink")
            (*Sillago aeolus*)
          - [金線鱚](../Page/金線鱚.md "wikilink") (*Sillago analis*)
          - [阿拉伯鱚](../Page/阿拉伯鱚.md "wikilink") (*Sillago arabica*)
          - [銀帶鱚](../Page/銀帶鱚.md "wikilink") (*Sillago argentifasciata*)
          - [亞洲鱚](../Page/亞洲鱚.md "wikilink")/[亞洲沙鮻](../Page/亞洲沙鮻.md "wikilink")
            (*Sillago asiatica*)
          - [波斯灣鱚](../Page/波斯灣鱚.md "wikilink") (*Sillago attenuata*)
          - [銀鱚](../Page/銀鱚.md "wikilink") (*Sillago bassensis*)
          - [北部灣鱚](../Page/北部灣鱚.md "wikilink") (*Sillago boutani*)
          - [紅鱚](../Page/紅鱚.md "wikilink") (*Sillago burrus*)
          - [砂鱚](../Page/砂鱚.md "wikilink")/[大指沙鮻](../Page/大指沙鮻.md "wikilink")
            (*Sillago chondropus*)
          - [纖鱚](../Page/纖鱚.md "wikilink") (*Sillago ciliata*)
          - [弗氏鱚](../Page/弗氏鱚.md "wikilink") (*Sillago flindersi*)
          - [印度鱚](../Page/印度鱚.md "wikilink") (*Sillago indica*)
          - [海灣鱚](../Page/海灣鱚.md "wikilink")/[灣沙鮻](../Page/灣沙鮻.md "wikilink")
            (*Sillago ingenuua*)
          - [間鱚](../Page/間鱚.md "wikilink") (*Sillago intermadius*)
          - [少鱗鱚](../Page/少鱗鱚.md "wikilink")/[青沙鮻](../Page/青沙鮻.md "wikilink")
            (*Sillago japonica*)
          - [泥鱚](../Page/泥鱚.md "wikilink") (*Sillago lutea*)
          - [大鱗鱚](../Page/大鱗鱚.md "wikilink") (*Sillago macrolepis*)
          - [星鱚](../Page/星鱚.md "wikilink") (*Sillago maculata aeolus*)
          - [澳洲鱚](../Page/澳洲鱚.md "wikilink") (*Sillago maculata burrus*)
          - [斑鱚](../Page/斑鱚.md "wikilink") (*Sillago maculata maculata*)
          - [大頭鱚](../Page/大頭鱚.md "wikilink") (*Sillago megacephalus*)
          - [小眼鱚](../Page/小眼鱚.md "wikilink") (*Sillago microps*)
          - [粗鱚](../Page/粗鱚.md "wikilink") (*Sillago nierstraszi*)
          - [細鱗鱚](../Page/細鱗鱚.md "wikilink")/[小鱗沙鮻](../Page/小鱗沙鮻.md "wikilink")
            (*Sillago parvisquamis*)
          - [鈍頭鱚](../Page/鈍頭鱚.md "wikilink") (*Sillago robusta*)
          - [黃鰭鱚](../Page/黃鰭鱚.md "wikilink") (*Sillago schomburgkii*)
          - [多鱗鱚](../Page/多鱗鱚.md "wikilink")/**沙鮻** (*Sillago sihama*)
          - [沙林鱚](../Page/沙林鱚.md "wikilink") (*Sillago soringa*)
          - [文氏鱚](../Page/文氏鱚.md "wikilink") (*Sillago vincenti*)
          - [斜紋鱚](../Page/斜紋鱚.md "wikilink") (*Sillago vittata*)
      - [长背鱚属](../Page/长背鱚属.md "wikilink")/[似鱚屬](../Page/似鱚屬.md "wikilink")
        (*Sillaginodes*)
          - [斑似鱚](../Page/斑似鱚.md "wikilink") (*Sillaginodes punctata*)
      - [九棘鱚属](../Page/九棘鱚属.md "wikilink")/[擬鱚屬](../Page/擬鱚屬.md "wikilink")
        (*Sillaginopsis*)
          - [平頭擬鱚](../Page/平頭擬鱚.md "wikilink") (*Sillaginopsis panijus*)
      - [大指鱚屬](../Page/大指鱚屬.md "wikilink") (*Sillaginopodys*)
          - [砂大指鱚](../Page/砂大指鱚.md "wikilink") (*Sillaginopodys
            chondropus*)
      - [副鱚屬](../Page/副鱚屬.md "wikilink") (*Parasillago*)
          - [副鱚](../Page/副鱚.md "wikilink") (*Parasillago ciliata*)

### 常見的品種

  - [沙钻](../Page/沙钻.md "wikilink")，有兩個品種，可能是
      - *sillago japonica* (Temminck et Schlegel,
        1844)，一種[少鱗鱚](../Page/少鱗鱚.md "wikilink")
      - *sillago maculata* (Quoy et Gaimard,
        1824)，一種[斑鱚](../Page/斑鱚.md "wikilink")
  - [青沙鮻](../Page/青沙鮻.md "wikilink") (Japanese Whiting，一種少鱗鱚)
  - [白沙鮻](../Page/白沙鮻.md "wikilink") (Silver Whiting，一種少鱗鱚)
  - [砂肠仔](../Page/砂肠仔.md "wikilink") *sillago sihama* (Forsk?l,
    1775)，一種[多鳞鱚](../Page/多鳞鱚.md "wikilink") (Common whiting)
  - [六带拟鲈](../Page/六带拟鲈.md "wikilink")
  - [多带拟鲈](../Page/多带拟鲈.md "wikilink")

## 參考

[\*](../Category/鱚科.md "wikilink")

1.
2.
3.