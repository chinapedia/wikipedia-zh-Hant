**沙鸡科**（學名*Pteroclididae*）在[鸟类传统分类系统中是](../Page/鸟类传统分类系统.md "wikilink")[鸟纲中的](../Page/鸟纲.md "wikilink")[鸽形目中的一个](../Page/鸽形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")。有時也專門列爲一目，即**沙鸡目**（*Pteroclidiformes*）。

## 描述

[Sandgrouse_flock.jpg](https://zh.wikipedia.org/wiki/File:Sandgrouse_flock.jpg "fig:Sandgrouse_flock.jpg")

沙雞體型小，有[鴿子般的頭部和頸部以及壯碩的身體](../Page/鴿子.md "wikilink")。體長24至40公分，重量150至500克。[兩性異形成年雄性沙雞體型略比雌性沙雞大](../Page/兩性異形.md "wikilink")，顏色也比較鮮豔。快速直接飛行特點歸因於十一個具有長尖翼的羽毛。強壯的翅膀肌肉讓他們能夠快速起飛與持續飛行。成年沙雞，特別是雄性的腹部羽毛特別適合吸收水分並保留它，使沙雞能將水攜帶到離水坑數英里外小沙雞飲用\[1\]\[2\]
，這種利用羽毛攜帶水的水量為15至20毫升。\[3\]

## 外部連結

  - [Sandgrouse
    videos](http://ibc.lynxeds.com/family/sandgrouse-pteroclidae) on the
    Internet Bird Collection

  -
## 參考文獻

[Category:鸽形目](../Category/鸽形目.md "wikilink")
[\*](../Category/沙鸡科.md "wikilink")

1.
2.
3.