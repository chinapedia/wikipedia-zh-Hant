[Wei_River,Ning_Xiang_county_28.JPG](https://zh.wikipedia.org/wiki/File:Wei_River,Ning_Xiang_county_28.JPG "fig:Wei_River,Ning_Xiang_county_28.JPG")
**沩水河**（“**沩水**”），即**沩江**、**沩河**，古名“玉潭江”，[湘江下游西侧一级支流](../Page/湘江.md "wikilink")，位于[湖南省](../Page/湖南省.md "wikilink")[长沙市境内](../Page/长沙市.md "wikilink")，发源于[沩山](../Page/沩山.md "wikilink")。分南北两源，南源出扶王山西麓大托里，北原出灯窝塞北麓大沙坪；两源于[黄材水库汇合](../Page/黄材水库.md "wikilink")\[1\]。河流自西从[宁乡县向东流经](../Page/宁乡县.md "wikilink")[望城区](../Page/望城区.md "wikilink")，于[新康乡与](../Page/新康乡.md "wikilink")[高塘岭镇交界处团山湖与](../Page/高塘岭镇.md "wikilink")[八曲河汇合后入湘江](../Page/八曲河.md "wikilink")。沩水全长144[公里](../Page/公里.md "wikilink")，流域面积2,750平方公里。沩水河流域蕴藏发达的古代文明，上游有[炭河里](../Page/炭河里遗址.md "wikilink")、[寨子山](../Page/寨子山遗址.md "wikilink")、[月亮山等](../Page/月亮山遗址.md "wikilink")[遗址](../Page/遗址.md "wikilink")。沩水河为宁乡主要河流，境内总长度117.2公里，流域面积2,125平方公里\[2\]。

## 干流

[Wei_River,Ning_Xiang_county_06.JPG](https://zh.wikipedia.org/wiki/File:Wei_River,Ning_Xiang_county_06.JPG "fig:Wei_River,Ning_Xiang_county_06.JPG")
沩水的干流自西向东有90余条河溪，主要有

  - [黄绢水](../Page/黄绢水.md "wikilink")
  - [塅溪](../Page/塅溪.md "wikilink")
  - [梅溪](../Page/梅溪.md "wikilink")
  - [铁冲河](../Page/铁冲河.md "wikilink")
  - [玉堂水](../Page/玉堂水.md "wikilink")
  - [楚江](../Page/楚江.md "wikilink")
  - [乌江](../Page/乌江_\(宁乡县\).md "wikilink")

## 流域乡镇

  - [黄材镇](../Page/黄材镇.md "wikilink")
  - [横市镇](../Page/横市镇.md "wikilink")
  - [双凫铺镇](../Page/双凫铺镇.md "wikilink")
  - [大成桥镇](../Page/大成桥镇.md "wikilink")
  - [坝塘镇](../Page/坝塘镇.md "wikilink")
  - [回龙铺镇](../Page/回龙铺镇.md "wikilink")
  - [白马桥乡](../Page/白马桥乡_\(宁乡县\).md "wikilink")
  - [玉潭镇](../Page/玉潭镇.md "wikilink")
  - [历经铺乡](../Page/历经铺乡.md "wikilink")
  - [双江口镇](../Page/双江口镇.md "wikilink")
  - [金洲镇](../Page/金洲镇.md "wikilink")
  - [新康乡](../Page/新康乡.md "wikilink")（[望城区](../Page/望城区.md "wikilink")）
  - [乌山镇](../Page/乌山镇.md "wikilink")（望城区）
  - [高塘岭镇](../Page/高塘岭镇.md "wikilink")（望城区）

## 参考

## 外部链接

[Category:宁乡县](../Category/宁乡县.md "wikilink")
[Category:望城区](../Category/望城区.md "wikilink")
[Category:长沙地理](../Category/长沙地理.md "wikilink")
[Category:湘江水系](../Category/湘江水系.md "wikilink")
[Category:湖南河流](../Category/湖南河流.md "wikilink")

1.
2.