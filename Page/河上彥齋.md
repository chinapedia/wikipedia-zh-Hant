[Kawakami_Gensai.jpg](https://zh.wikipedia.org/wiki/File:Kawakami_Gensai.jpg "fig:Kawakami_Gensai.jpg")
**河上彥齋**（，[天保五年](../Page/天保.md "wikilink")[十一月廿五](../Page/十一月廿五.md "wikilink")
-
[明治四年](../Page/明治.md "wikilink")[十二月初四](../Page/十二月初四.md "wikilink")）。[尊王攘夷派的日本](../Page/尊王攘夷.md "wikilink")[武士和](../Page/武士.md "wikilink")[熊本藩士](../Page/熊本藩.md "wikilink")。[諱](../Page/諱.md "wikilink")**玄明**。[幕末四大人斬之一](../Page/幕末四大人斬.md "wikilink")。

## 生涯

[文久三年](../Page/文久.md "wikilink")（1863年）30歳時，被同格的幹部[宮部鼎藏選拔為](../Page/宮部鼎藏.md "wikilink")[熊本藩親衛兵](../Page/熊本藩.md "wikilink")。一般被人稱為「人斬彥齋」，彥齋所斬殺過的人物確實有[佐久間象山](../Page/佐久間象山.md "wikilink")，但以後斬殺了幾個人卻沒有明確的流傳下來。其身高五[丈多](../Page/丈.md "wikilink")（150厘米）身材短小但皮膚白皙，據說初次見面的人會誤會他是[女性](../Page/女性.md "wikilink")。而彥齋的[劍術為](../Page/劍術.md "wikilink")[我流](../Page/我流.md "wikilink")，被傳為是單手拔刀的高手（單膝跪在地面上以那樣的低姿勢使出[逆袈裟斬來攻擊](../Page/逆袈裟斬.md "wikilink")）。

不過也有說法認為他修行的[劍術是](../Page/劍術.md "wikilink")[伯耆流](../Page/伯耆流.md "wikilink")。而這個說法的根據，為當時熊本藩最盛行的拔刀術就是伯耆流，並且有人提出彥齋所使用的劍術雖然和伯耆流有些不同但大多有著一樣的招式，所以用這個為理由來證明。但除了在白天公然的暗殺了[佐久間象山以外](../Page/佐久間象山.md "wikilink")，就完全沒有殘留其他關於他使用什麼劍術的跡象。因此他可能是[幕末四大人斬裡面最令人害怕的人](../Page/幕末四大人斬.md "wikilink")。

在「[八月十八日的政變](../Page/八月十八日的政變.md "wikilink")」之後，彥齋前往[長州](../Page/長州.md "wikilink")。在長州擔任[三條實美的護衛](../Page/三條實美.md "wikilink")。[元治元年](../Page/元治.md "wikilink")（1864年）6月的[池田屋騷動後](../Page/池田屋事件.md "wikilink")，彥齋為了要幫在事件中死去的[宮部鼎藏復仇於是再次前往](../Page/宮部鼎藏.md "wikilink")[京都企圖討伐](../Page/京都.md "wikilink")[新選組](../Page/新選組.md "wikilink")。7月11日，彥齋暗殺了[公武合體派的開国論者](../Page/公武合體.md "wikilink")[佐久間象山](../Page/佐久間象山.md "wikilink")。從[暗殺象山以後](../Page/暗殺.md "wikilink")，彥齋就沒再進行暗殺的行動了。

[第二次長州征伐時](../Page/幕長戰爭.md "wikilink")，參加長州軍，為長州軍增加勝利的機會。[慶應三年](../Page/慶應.md "wikilink")（1867年）歸藩，但由於那時熊本藩為[佐幕派所掌權](../Page/佐幕.md "wikilink")，彥齋因此而下獄。所以當[大政奉還](../Page/大政奉還.md "wikilink")、[王政復古](../Page/王政復古_\(日本\).md "wikilink")、[鳥羽伏見之戰等重大事件發生時](../Page/鳥羽伏見之戰.md "wikilink")，彥齋都是在獄中度過。慶應四年（1868年）2月彥齋出獄，而佐幕派的熊本藩想利用彥齋踏上[維新的浪潮](../Page/明治維新.md "wikilink")，但遭彥齋的拒絕。

[明治維新後](../Page/明治維新.md "wikilink")，走向[開国政策的新政府相當害怕彥齋會提出](../Page/開国.md "wikilink")[攘夷的建議](../Page/攘夷.md "wikilink")。而且彥齋也被質疑和[二卿事件](../Page/二卿事件.md "wikilink")（意圖推翻[明治政府的事件](../Page/明治政府.md "wikilink")）有所關係，然後又被懷疑是暗殺参議員[廣澤真臣的兇手](../Page/廣澤真臣.md "wikilink")，於是在[明治四年](../Page/明治.md "wikilink")（1871年）12月遭斬首。但是這個暗殺事件和彥齋並無關係，所以一般認為是因為彥齋不遵守明治政府的政策方針，而因此被殺。

彥齋的[辞世之歌為](../Page/辞世之歌.md "wikilink")

`君が為め　死ぬる骸に　草むさば　赤き心の　花や咲くらん `
`君を思い君の御法に死ぬる身を　ゆめ見こりなそつくせ世の人 `
`かねてよりなき身と知れど君が世を　思う心ぞ世に残りける `

[Genzai_kawakami.jpg](https://zh.wikipedia.org/wiki/File:Genzai_kawakami.jpg "fig:Genzai_kawakami.jpg")

## 逸話

在志士集會的時候，酒席上大家批評了一位幕臣。並且紛紛說出「絕不原諒這傢伙」、「砍了他」等話，而這些喝了酒的志士們因為這樣講著講著而怒氣上身，河上彥齋同時也在那個場合。這時聽到這些話的彥齋暫時離開了座位，然後據說當大家還在講這個話題時，彥齋已經提了這個幕臣的頭回來了。

## 小知識

  - 漫畫《[神劍闖江湖](../Page/神劍闖江湖.md "wikilink")》（浪客劍心）的主角[緋村劍心](../Page/緋村劍心.md "wikilink")，即是以河上彥齋為原型所創作出來的人物。
  - 在漫畫《[銀魂](../Page/銀魂.md "wikilink")》中，有位幫倒幕派工作的劍客河上萬齊，而他的名字由來就是河上彥齋。

[K](../Category/日本劍客.md "wikilink")
[Category:熊本藩](../Category/熊本藩.md "wikilink")
[Category:明治時代人物](../Category/明治時代人物.md "wikilink")
[Category:肥後國出身人物](../Category/肥後國出身人物.md "wikilink")
[Category:日本刺客](../Category/日本刺客.md "wikilink")
[Category:被處決的日本人](../Category/被處決的日本人.md "wikilink")