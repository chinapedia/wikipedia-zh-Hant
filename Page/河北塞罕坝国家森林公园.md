[Saihanba_park.jpg](https://zh.wikipedia.org/wiki/File:Saihanba_park.jpg "fig:Saihanba_park.jpg")

**河北塞罕坝国家森林公园**（\[1\]），位于[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[承德市](../Page/承德市.md "wikilink")[围场满族蒙古族自治县的最北端](../Page/围场满族蒙古族自治县.md "wikilink")，与[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[克什克腾旗临界](../Page/克什克腾旗.md "wikilink")，主要由塞罕坝机械林场组成。

## 历史

该国家级森林公园原名“河北木兰围场国家森林公园”，1993年5月由原[林业部批准设立](../Page/林业部.md "wikilink")。2007年12月5日，原“**河北木兰围场国家森林公园**”更名为“**河北塞罕坝国家森林公园**”\[2\]。2008年1月7日，[国家林业局准予](../Page/国家林业局.md "wikilink")[河北省木兰围场国有林场管理局设立](../Page/河北省.md "wikilink")“河北木兰围场国家森林公园”。原“木兰围场国家级森林公园”与今“木兰围场国家级森林公园”虽同处围场满族蒙古族自治县，唯范围有异，所辖林班不重合。

## 概况

公园总面积141万[亩](../Page/亩.md "wikilink")，融[森林](../Page/森林.md "wikilink")、[草原和沼泽](../Page/草原.md "wikilink")[湿地为一身](../Page/湿地.md "wikilink")，有山地、高原、丘陵、河流、湖泊、溪流、松树林、桦树林、羊群、军马，是消暑[旅游和](../Page/旅游.md "wikilink")[摄影的好地方](../Page/摄影.md "wikilink")，也是影视创作的基地。

[木兰围场](../Page/木兰围场.md "wikilink")（又被称为围场坝上），与[丰宁坝上和](../Page/丰宁坝上.md "wikilink")[张北坝上被称为](../Page/张北坝上.md "wikilink")[坝上](../Page/坝上.md "wikilink")。坝上是一个俗称，主要指在[华北平原和](../Page/华北平原.md "wikilink")[蒙古高原交界处](../Page/蒙古高原.md "wikilink")，海拔陡然升高呈台阶状。木兰围场的平均海拔为1487米。在[清朝](../Page/清朝.md "wikilink")，木兰围场是三代[清帝](../Page/清帝.md "wikilink")[秋猎打围的猎苑](../Page/木兰秋狝.md "wikilink")，木兰是[满语](../Page/满语.md "wikilink")“哨鹿”的意思\[3\]，也是蒙古部族王公朝見清帝的地方，平時有人駐守防止[盜獵](../Page/盜獵.md "wikilink")。范围包含今[围场满族蒙古族自治县全境](../Page/围场满族蒙古族自治县.md "wikilink")，在承德市与内蒙古自治区交界处。现在成为旅游景点。

[Inner_Mongolia_grassland_(2005).jpg](https://zh.wikipedia.org/wiki/File:Inner_Mongolia_grassland_\(2005\).jpg "fig:Inner_Mongolia_grassland_(2005).jpg")
[Saihanba3.jpg](https://zh.wikipedia.org/wiki/File:Saihanba3.jpg "fig:Saihanba3.jpg")

## 旅游资源

  - 将军泡子
  - 西大泡子（公主湖）
  - 皮夹沟
  - 桦木沟
  - 五彩山
  - 小红山

## 动植物资源

塞罕坝国家森林公园是[黑琴鸡](../Page/黑琴鸡.md "wikilink")、[大鸨等珍稀鸟类的主要](../Page/大鸨.md "wikilink")[栖息地](../Page/栖息地.md "wikilink")，除此之外还有[豹](../Page/豹.md "wikilink")、[鹿](../Page/鹿.md "wikilink")、[黄羊](../Page/黄羊.md "wikilink")、[狍子](../Page/狍子.md "wikilink")、[天鹅等动物](../Page/天鹅.md "wikilink")。

主要树种有[白桦](../Page/白桦.md "wikilink")、[山杨](../Page/山杨.md "wikilink")、[樟子松](../Page/樟子松.md "wikilink")、[落叶松](../Page/落叶松.md "wikilink")、[云杉](../Page/云杉.md "wikilink")，还有[柞树](../Page/柞树.md "wikilink")、[五角枫等](../Page/五角枫.md "wikilink")。

## 参考文献

## 外部链接

  - [塞罕坝国家森林公园简介](http://shbfp.forestry.gov.cn/8880/71941.html)

## 参见

  - [丰宁坝上](../Page/丰宁坝上.md "wikilink")（[丰宁县](../Page/丰宁县.md "wikilink")）
  - [张北坝上](../Page/张北坝上.md "wikilink")（[张北县](../Page/张北县.md "wikilink")）

[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:河北国家森林公园](../Category/河北国家森林公园.md "wikilink")
[Category:承德公园](../Category/承德公园.md "wikilink")
[Category:围场自治县](../Category/围场自治县.md "wikilink")

1.  參見《滿漢大辭典》，安雙成 編，遼寧民族出版社1993年出版，789頁

2.

3.