**河南中路**旧名**河南路**，是当年[上海公共租界中区和今天](../Page/上海公共租界.md "wikilink")[上海市](../Page/上海市.md "wikilink")[黄浦区的一条南北向马路](../Page/黄浦区.md "wikilink")，南起[延安东路](../Page/延安东路.md "wikilink")（原[爱多亚路](../Page/爱多亚路.md "wikilink")），北至苏州路。全长1233米。与[广东路](../Page/广东路.md "wikilink")、[福州路](../Page/福州路.md "wikilink")、[汉口路](../Page/汉口路.md "wikilink")、[九江路](../Page/九江路.md "wikilink")、[南京路](../Page/南京路.md "wikilink")、[北京路等多条道路交汇](../Page/北京东路_\(上海\).md "wikilink")。

1846年，租界修筑该路洋泾浜至南京路的南段，作为英租界的西界，名为界路。但是2年后，英租界就向西拓展至今西藏路，于是界路名不符实。1856年向北延伸至苏州路。沿路有五洲大楼、南京大楼、国华大楼、丽华公司、亨达利钟表店等。向北过苏州河就是北河南路，通往北火车站。[福州路至](../Page/福州路.md "wikilink")[广东路段](../Page/广东路.md "wikilink")，俗称为**棋盘街**，多文具店及呢绒店、[商务印书馆](../Page/商务印书馆.md "wikilink")、[中华书局等集中两侧](../Page/中华书局.md "wikilink")。

今日，河南中路上保留至今的近代建筑有恒利银行(495、503号，天津路口)、美丰银行(521-529号，宁波路口)和吉祥里(531号、541号)等。

## 轨道交通

[上海轨道交通二号线在河南中路南京东路口设](../Page/上海轨道交通二号线.md "wikilink")[南京东路站](../Page/南京东路站.md "wikilink")。

## 交会道路（南向北）

  - [延安东路](../Page/延安东路.md "wikilink")
  - [广东路](../Page/广东路.md "wikilink")
  - [泗泾路](../Page/泗泾路.md "wikilink")
  - [昭通路](../Page/昭通路.md "wikilink")
  - [福州路](../Page/福州路.md "wikilink")
  - [汉口路](../Page/汉口路.md "wikilink")
  - [九江路](../Page/九江路.md "wikilink")
  - [南京东路](../Page/南京东路.md "wikilink")
  - [天津路](../Page/天津路.md "wikilink")
  - [宁波路](../Page/宁波路.md "wikilink")
  - [北京东路](../Page/北京东路.md "wikilink")
  - [南苏州路](../Page/南苏州路.md "wikilink")

## 参见

  - [河南南路 (上海)](../Page/河南南路_\(上海\).md "wikilink")
  - [河南北路 (上海)](../Page/河南北路_\(上海\).md "wikilink")

[nl:East Nanjing Lu](../Page/nl:East_Nanjing_Lu.md "wikilink")

[Category:上海租界](../Category/上海租界.md "wikilink")
[H](../Category/上海道路.md "wikilink")