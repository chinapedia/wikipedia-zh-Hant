**河南博物院**是位于[河南省](../Page/河南省.md "wikilink")[郑州市的一所国家级的大型](../Page/郑州市.md "wikilink")[现代化综合](../Page/现代化.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")，[国家级重点博物院之一](../Page/中央地方共建国家级重点博物馆.md "wikilink")，前身是**河南省博物馆**。河南省博物院是[中国](../Page/中国.md "wikilink")[近代较早创立的博物馆](../Page/近代.md "wikilink")，自[民國十六年在](../Page/民國十六年.md "wikilink")[冯玉祥的主导下建立](../Page/冯玉祥.md "wikilink")，距今已有八十多年历史，[抗日战争期间](../Page/抗日战争.md "wikilink")，河南博物院的部分珍贵文物几经辗转最终被珍藏在[台湾](../Page/台湾.md "wikilink")[国立历史博物馆](../Page/国立历史博物馆.md "wikilink")。大陸的河南博物院馆址几经变更，旧址位于[开封市](../Page/开封市.md "wikilink")[龙亭区三胜街](../Page/龙亭区.md "wikilink")31号，新馆于1997年开放，馆址位于[河南省](../Page/河南省.md "wikilink")[郑州市农业路](../Page/郑州市.md "wikilink")。

目前馆藏文物多来自于[二十世纪初](../Page/二十世纪.md "wikilink")[淅川](../Page/淅川.md "wikilink")、[洛阳](../Page/洛阳.md "wikilink")、[三门峡](../Page/三门峡.md "wikilink")、[辉县](../Page/辉县.md "wikilink")、[新郑](../Page/新郑.md "wikilink")、[安阳等地的考古发掘](../Page/安阳.md "wikilink")，数量达13万多件，[史前文物](../Page/史前.md "wikilink")、[商](../Page/商.md "wikilink")[周](../Page/周.md "wikilink")[青铜器](../Page/青铜器.md "wikilink")、历代[陶瓷器](../Page/陶瓷.md "wikilink")、[玉器最具特色](../Page/玉器.md "wikilink")。其中[国家一级文物与国家二级文物](../Page/国家一级文物.md "wikilink")5000余件，历史文化艺术价值极高。，当世中国之博物馆，以藏品而言，除[故宫博物院外](../Page/故宫博物院.md "wikilink")，[河南博物馆当属第二](../Page/河南博物馆.md "wikilink")。

## 历史沿革

### 创立

[GREG3838.jpg](https://zh.wikipedia.org/wiki/File:GREG3838.jpg "fig:GREG3838.jpg")位于[开封市](../Page/开封市.md "wikilink")[三胜街](../Page/三胜街.md "wikilink")，是第五批[河南省文物保护单位](../Page/河南省文物保护单位.md "wikilink")\]\]
1923年夏，河南新郑一绅士李锐打井，无意间打出一座古墓，发掘出100多件青铜器。北洋陆军14师师长闻讯后向[吴佩孚驰报](../Page/吴佩孚.md "wikilink")。吴佩孚命令该师长将出土文物运至开封妥善保管，以垂久远。为了更好的保存这批文物，1927年7月在[冯玉祥极力主张下于创立了](../Page/冯玉祥.md "wikilink")[河南博物馆](../Page/河南博物馆.md "wikilink")，筹建于[开封](../Page/开封.md "wikilink")[三圣庙街](../Page/三圣庙街.md "wikilink")（今[开封三胜街](../Page/开封.md "wikilink")）[河南法政学堂和](../Page/河南法政学堂.md "wikilink")[河道总督衙门旧址](../Page/河道总督衙门.md "wikilink")。1928年5月，河南省政府为展现民族历史，宣传民族共和和世界大同的理想，将河南博物馆改名为“民族博物院”，“民族博物院”以“启发民众知识文明、激增革命思想、促进社会文明”为宗旨，在各地广征历史、民族服饰、自然、艺术等方面的实物资料于10月10日，大量运用模型等手段，成功举办了第一次陈列展览。1930年12月1日，“民族博物院”被定为社会教育机构，复名为“河南博物馆”，隶属于河南省教育厅。1931年1月20日，河南省教育厅专门颁布了《河南博物馆组织条例》，条列明确了河南博物馆以发扬固有文化、提倡学术研究、增长民众知识、促进社会文明为宗旨，同时设立了保管部、搜集研究部，成立了由河南民政厅厅长、河南教育厅厅长、河南大学校长、博物馆馆长等组成的七人理事会，将原来的19个陈列室缩减为7个，撤出大量民族服饰和模型，充实历史文物。1930年至1937年这8年是河南博物馆的辉煌时期，馆藏文物最为丰富，且涌现出以[关百益为代表的一批高水平的文物研究员](../Page/关百益.md "wikilink")。

### 抗日战争爆發至新中国成立

1937年[卢沟桥事变爆发后](../Page/卢沟桥事变.md "wikilink")，[国民政府为确保文物安全](../Page/国民政府.md "wikilink")，于11月24日将原河南博物馆所藏文物精选珍品5678件，[拓片](../Page/拓片.md "wikilink")1162张，图书1472套（册）装68箱，用了三天时间运往[汉口法租界租房保存](../Page/汉口.md "wikilink")。1938年9月，汉口沦陷之前，这批文物又被迫转移至[重庆](../Page/重庆.md "wikilink")，保存在中央大学磁器口校舍中。

1949年11月，[中華民國政府將存放于](../Page/国民政府.md "wikilink")[重庆的原](../Page/重庆.md "wikilink")[河南博物馆文物中的珍貴文物](../Page/河南博物馆.md "wikilink")5119件，图书1450套（册）装38箱与[国立故宫博物院部分文物藏品一起运往](../Page/国立故宫博物院.md "wikilink")[台湾](../Page/台湾.md "wikilink")，后成为[台北](../Page/台北.md "wikilink")[国立历史博物馆的主要藏品](../Page/国立历史博物馆.md "wikilink")，其余30箱由[中国人民解放军军管会接管](../Page/中国人民解放军.md "wikilink")。

### 中華人民共和國成立以後

1950年8月21日，存放于重庆的原[河南博物馆剩余部分文物陆续运回](../Page/河南博物馆.md "wikilink")[河南](../Page/河南.md "wikilink")。1958年，随着河南省会由开封迁至郑州，[河南省博物馆在郑州新建](../Page/河南省博物馆.md "wikilink")。由于在建筑规模，内部设施上难以难适应当代博物馆的发展需要，河南省政府领导决定重新建设一座具有当今先进水平的现代化博物馆，1991年开始建设新馆，由中央和河南省累计投资近3亿人民币，1996年建成。1997年7月，原中原石刻艺术馆和[河南省博物馆合并](../Page/河南省博物馆.md "wikilink")，在新址成立**河南博物院**。同年10月，应河南博物院邀请，时任台湾国立历史博物馆副馆长[黄永川率团来豫访问](../Page/黄永川.md "wikilink")，两馆自此拉开合作帷幕。

自2015年7月14日起，河南博物院将闭馆对主展馆进行维修，工程主要涉及主展馆外部石材及玻璃幕墙更换、防水层和保温层更新，并加强主展馆建筑的抗震力，预计工期18个月。同时自当天起，西配楼一楼临时展厅举办“大象中原——河南古代文明瑰宝展”免费对外开放。\[1\]

## 院徽

[Henan_musem_logo.png](https://zh.wikipedia.org/wiki/File:Henan_musem_logo.png "fig:Henan_musem_logo.png")
河南博物院的馆徽于2007年8月中旬公开征集。最终胜出的院徽的涵义如下：

标志模仿河南博物院主题馆的建筑外形，上部为仰斗以承“甘露”，下部为覆斗以纳“地气”。

上面的三角形与下面的三角形重叠区域的圆形“○”寓意中国古语的“[天圆地方](../Page/天圆地方.md "wikilink")”。象征中国5000年的历史沉淀。

上下相交的三角形象征博物院的合作、交流、沟通和融合的精神。同时，该造型似一个“[沙漏](../Page/沙漏.md "wikilink")”，而沙漏在古代是一种计时工具，沙落下，时间流逝，历史是流逝时间的积累，博物院正是展现历史的场所，标志简练醒目、刚柔并济、稳重大方。

色彩方面，运用了沉稳、内敛、厚重的暗红色和代表辉煌历史和灿烂文明的的金色。

## 馆舍

河南博物院占地面积10余万平方米，建筑面积7.8万平方米。主展馆位于院区中央，外形取位于[河南](../Page/河南.md "wikilink")[登封的](../Page/登封.md "wikilink")[元代古](../Page/元代.md "wikilink")[观星台为雏形](../Page/观星台.md "wikilink")，故呈[金字塔形](../Page/金字塔.md "wikilink")，冠部为方斗形，上扬下覆，取上承“[甘露](../Page/甘露.md "wikilink")”、下纳“[地气](../Page/地气.md "wikilink")”之意，寓意中原为华夏之源，融汇四方。外部墙面为土黄褐色，取[中原](../Page/中原.md "wikilink")“[黄土](../Page/黄土.md "wikilink")”“[黄河](../Page/黄河.md "wikilink")”
孕育了[华夏文明之意](../Page/华夏文明.md "wikilink")，主馆正面从上至下有浅蓝色的透明窗与自上而下的透明采光带，具有“[黄河之水天上来](../Page/黄河之水天上来.md "wikilink")”的磅礴气势。主馆后为文物库房。

## 陈列

### 基本陈列

[七层连阁绘彩陶楼1394.jpg](https://zh.wikipedia.org/wiki/File:七层连阁绘彩陶楼1394.jpg "fig:七层连阁绘彩陶楼1394.jpg")
基本陈列为“河南古代文明之光”系列展览，展示了上至[史前时代](../Page/史前.md "wikilink")，下至[明](../Page/明.md "wikilink")[清的河南历史文物](../Page/清.md "wikilink")。

  - 文明曙光——[原始社会时期](../Page/原始社会.md "wikilink")
  - [三代辉煌](../Page/三代.md "wikilink")——[夏](../Page/夏.md "wikilink")[商](../Page/商.md "wikilink")[周时期](../Page/周.md "wikilink")
  - 兼容并蓄——[两汉](../Page/两汉.md "wikilink")[魏晋南北朝时期](../Page/魏晋南北朝.md "wikilink")
  - 盛世荣华——[隋](../Page/隋.md "wikilink")[唐时期](../Page/唐.md "wikilink")
  - 余光明媚——[宋](../Page/宋.md "wikilink")[金](../Page/金.md "wikilink")[元时期](../Page/元.md "wikilink")

### 专题陈列

  - “中原百年风云”
  - “河南古代石刻艺术”
  - “楚国青铜艺术”
  - “河南古代玉器”
  - “明清工艺珍品”

### 临时展览

主要用来陈列各种文化交流与合作的展览，新馆开放以来，先后与国内外各个机构单位合作交流，有“金色王朝－－[故宫御用金银器特展](../Page/故宫.md "wikilink")”、“[意大利](../Page/意大利.md "wikilink")[文艺复兴时期绘画巨匠原作展](../Page/文艺复兴.md "wikilink")
”、“[国家宝藏](../Page/国家宝藏.md "wikilink")”、“三千大千世界：[台北](../Page/台北.md "wikilink")[历史博物馆藏](../Page/历史博物馆.md "wikilink")[张大千](../Page/张大千.md "wikilink")[书画展](../Page/书画.md "wikilink")”、“华夏文明之源”等众多展览。

## 镇院之宝

[妇好鸮尊](../Page/妇好鸮尊.md "wikilink")：1976年出土于[河南](../Page/河南.md "wikilink")[安阳](../Page/安阳.md "wikilink")[殷墟](../Page/殷墟.md "wikilink")[妇好墓](../Page/妇好墓.md "wikilink")，系[商代晚期器物](../Page/商代.md "wikilink")。通高45.9厘米，口径16.4厘米。盛酒器，形体呈猫头鹰状，昂首、圆目、宽喙、小
耳、高冠，双翅并拢，双足与垂尾共为三点支撑，后颈有口，上有盖。内壁铸“[妇好](../Page/妇好.md "wikilink")”二字铭文。背有兽首弓形鋬。器身满布缛丽的纹饰，造型典雅凝重，为[商器之精品](../Page/商.md "wikilink")。

[莲鹤方壶](../Page/莲鹤方壶.md "wikilink")：是河南博物院最早的藏品之一，为保护及研究随其一起于1923年出土的大批[新郑城关李家楼村](../Page/新郑.md "wikilink")[郑公大墓珍贵文物亦是河南博物院创立的初衷之一](../Page/郑公大墓.md "wikilink")。，高120厘米，口径31厘米。[春秋时期器物](../Page/春秋.md "wikilink")。
壶上有冠盖，壶冠呈双层盛开的莲瓣形；中间平盖上立一展翅欲飞之[鹤](../Page/鹤.md "wikilink")；器身长劲，壶颈两侧用附壁回首之龙形怪兽为耳；垂腹，腹部四角各攀附一立体小兽、圈足，圈足下有两个侧首吐舌的卷尾兽，倾其全力承托重器；器身满饰[蟠螭纹](../Page/蟠螭纹.md "wikilink")。该壶造型宏伟气派，装饰典雅华美。

[武曌金简](../Page/武曌.md "wikilink")：1982年出土于[中岳](../Page/中岳.md "wikilink")[嵩山](../Page/嵩山.md "wikilink")[峻极峰](../Page/峻极峰.md "wikilink")，唐代早期遗物，这是中国目前发现唯一的一枚[皇帝金简](../Page/皇帝.md "wikilink")。金简呈长方形，长36.2厘米，宽8厘米，厚0.1厘米，重223.5克。正面镌刻双钩[楷书](../Page/楷书.md "wikilink")[铭文](../Page/铭文.md "wikilink")3行63字，"大周国主武曌好乐真道长生神仙，谨诣[中岳嵩高山门](../Page/中岳.md "wikilink")，投金简一通，迄[三官九府除武曌罪名](../Page/三官九府.md "wikilink")，太发庚子七（）月甲申朔七（）日甲寅小使臣（）胡超稽首再拜谨奏。"，是[武则天在](../Page/武则天.md "wikilink")[久视元年](../Page/久视.md "wikilink")（公元700年）七月七日来嵩山祈福，谴宫廷太监胡超向诸神投简以求除罪消灾。

[汝窑天蓝釉刻花鹅颈瓶](../Page/汝窑天蓝釉刻花鹅颈瓶.md "wikilink")：1987年出土于[河南](../Page/河南.md "wikilink")[宝丰](../Page/宝丰.md "wikilink")[清凉寺汝官窑遗址](../Page/清凉寺汝官窑遗址.md "wikilink")，是[北宋晚期器物](../Page/北宋.md "wikilink")，中国唯一考古出土的[汝窑珍品](../Page/汝窑.md "wikilink")。高19.6厘米，口径5.8厘米，足径8.4厘米。敞口细颈，下有圈足，颈部长腹分别刻以折枝莲花，器表满施天蓝釉，光亮滋润，布满[开片](../Page/开片.md "wikilink")。瓷瓶为传世[御用](../Page/御用.md "wikilink")[汝瓷中绝无仅有](../Page/汝瓷.md "wikilink")，也为刻花[汝官瓷的鉴定提供了实物依据](../Page/汝官瓷.md "wikilink")。刻花鹅颈瓶不仅造型讲究，而且用刻花装饰，更饰以天蓝[釉](../Page/釉.md "wikilink")，保存完整无缺，实在弥足珍贵。

[杜岭方鼎](../Page/杜岭方鼎.md "wikilink")：1974年出土于[河南](../Page/河南.md "wikilink")[郑州商城遗址](../Page/郑州商城遗址.md "wikilink")，是[商代早期器物](../Page/商代.md "wikilink")，高87厘米，口边61厘米×61厘米。是目前人类所能认知的年代最早、体量最大、铸造最为完美、保存最为完整的[青铜重器](../Page/青铜重器.md "wikilink")。
更是中国乃至世界在[青铜时代所创造的第一座青铜文明](../Page/青铜时代.md "wikilink")[纪念碑](../Page/纪念碑.md "wikilink")。该鼎为[多范分铸而成](../Page/多范分铸.md "wikilink")。口呈长方形，上立二次铸成的拱形立耳一对，深腹，腹壁微内敛，平底，下有四个上粗下细的空柱形足，器身四面和四隅各铸单线兽面纹一组。每面两侧与下部饰[乳钉纹](../Page/乳钉纹.md "wikilink")，足上部各饰[兽面纹](../Page/兽面纹.md "wikilink")。这是中国目前发现时代最早的大型铜[方鼎](../Page/方鼎.md "wikilink")，是研究商代前期青铜冶铸的宝贵资料。

[贾湖骨笛](../Page/贾湖骨笛.md "wikilink")：1987年出土于[河南省](../Page/河南省.md "wikilink")[舞阳县](../Page/舞阳县.md "wikilink")[贾湖遗址](../Page/贾湖遗址.md "wikilink")，贾湖骨笛是中国目前出土的年代最早的乐器实物，距今已有约8000年历史，被称为“[中华第一笛](../Page/中华第一笛.md "wikilink")”。骨笛长23.1厘米，七孔，为[鹤类](../Page/鹤.md "wikilink")[胫骨所制](../Page/胫骨.md "wikilink")。演奏试验和测音结果表明，[舞阳骨笛已经具备七声音阶结构](../Page/舞阳.md "wikilink")，发音准确，音质也较好，至今仍可吹奏旋律。

[云纹铜禁](../Page/云纹铜禁.md "wikilink")：1978年出土于[河南省](../Page/河南省.md "wikilink")[淅川](../Page/淅川.md "wikilink")[淅川下寺](../Page/淅川香严寺.md "wikilink")[2号楚墓（子庚墓）](../Page/淅川下寺春秋楚墓群.md "wikilink")。出土时的云纹铜禁是数百块碎片，整整装了两个麻袋，后来由中国国内为数不多的知名[青铜器修复专家](../Page/青铜器.md "wikilink")[王长青花费了两年多的时间成功修复](../Page/王长青.md "wikilink")。云纹铜禁是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国家一级文物](../Page/国家一级文物.md "wikilink")，也是64件禁止出国（境）展览的文物之一。其工艺精湛复杂，令人叹为观止。此禁整体用[失蜡法](../Page/失蜡法.md "wikilink")（[熔模工艺](../Page/熔模工艺.md "wikilink")）铸就。通高28.8厘米，长103厘米，宽46厘米。[春秋时期器物](../Page/春秋.md "wikilink")。禁为承置[酒器的](../Page/酒器.md "wikilink")[案](../Page/案.md "wikilink")，其器身以粗细不同的铜梗支撑多层[镂空](../Page/镂空.md "wikilink")[云纹](../Page/云纹.md "wikilink")，十二只[龙形异兽攀缘于禁的四周](../Page/龙.md "wikilink")，另十二只蹲于禁下为足。

[四神云气图](../Page/四神云气图.md "wikilink")：1987年出土于[河南](../Page/河南.md "wikilink")[永城](../Page/永城.md "wikilink")[芒砀山](../Page/芒砀山.md "wikilink")[梁](../Page/梁.md "wikilink")[国](../Page/国.md "wikilink")[王陵区柿园墓](../Page/王陵.md "wikilink")，是[西汉早期壁画](../Page/西汉.md "wikilink")，壁画南北长5.14米，东西宽3.27，面积16.8平方米。柿园墓的主人是[西汉](../Page/西汉.md "wikilink")[梁国第二代王](../Page/梁国.md "wikilink")--[梁](../Page/梁.md "wikilink")[共王](../Page/共王.md "wikilink")[刘买](../Page/刘买.md "wikilink")，是大名鼎鼎的梁国开国[藩王](../Page/藩王.md "wikilink")[梁孝王](../Page/梁孝王.md "wikilink")[刘武](../Page/刘武.md "wikilink")（[汉文帝的次子](../Page/汉文帝.md "wikilink")）的儿子。这幅四神云气图出现的时间要比闻名于世的[敦煌](../Page/敦煌.md "wikilink")[壁画还早](../Page/壁画.md "wikilink")600年左右，是中国目前所见时代最早、画面最大、级别最高、保存最为完整的[壁画](../Page/壁画.md "wikilink")。

[玉柄铁剑](../Page/玉柄铁剑.md "wikilink")：1990年出土于[河南省](../Page/河南省.md "wikilink")[三门峡市](../Page/三门峡市.md "wikilink")[虢国墓](../Page/虢国.md "wikilink")（虢季墓），身长20厘米，茎长13厘米，[西周晚期器物](../Page/西周.md "wikilink")。
剑身为铁质，铁质剑身与铜芯相接，铜芯部嵌入玉茎内。剑首及茎身接合部均镶以[绿松石片](../Page/绿松石.md "wikilink")。出土时已折为两段，剑身外包一层[丝织品](../Page/丝织品.md "wikilink")，并被装在用皮革精心制作的[剑鞘内](../Page/剑鞘.md "wikilink")。这把玉柄铁剑，将[中国](../Page/中国.md "wikilink")[冶铁史向前推进了](../Page/冶铁.md "wikilink")200年，被誉为“[中华第一剑](../Page/中华第一剑.md "wikilink")”。

## 相关条目

  - [河南省博物馆旧址](../Page/河南省博物馆旧址.md "wikilink")
  - [国立历史博物馆](../Page/国立历史博物馆.md "wikilink")

## 參考资料与書目

  - Lee Yuan-Yuan and Shen, Sinyan. *Chinese Musical Instruments
    (Chinese Music Monograph Series)*. 1999. Chinese Music Society of
    North America Press. ISBN 1-880464039

[Category:郑州博物馆](../Category/郑州博物馆.md "wikilink")
[Category:中国历史博物馆](../Category/中国历史博物馆.md "wikilink")
[Category:国家一级博物馆](../Category/国家一级博物馆.md "wikilink")
[Category:1927年建立](../Category/1927年建立.md "wikilink")
[Category:金水区](../Category/金水区.md "wikilink")
[Category:1997年完工建築物](../Category/1997年完工建築物.md "wikilink")

1.