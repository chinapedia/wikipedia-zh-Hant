**河口瑶族自治县**，簡稱**河口縣**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[红河哈尼族彝族自治州下属的一个少數民族](../Page/红河哈尼族彝族自治州.md "wikilink")[自治县](../Page/自治县.md "wikilink")。面积1313平方公里，2012年人口9万人。县政府驻[河口镇](../Page/河口镇_\(河口县\).md "wikilink")。

河口县北与[屏边县](../Page/屏边苗族自治县.md "wikilink")、[马关县及](../Page/马关县.md "wikilink")[个旧市接壤](../Page/个旧市.md "wikilink")，西与[金平县毗连](../Page/金平苗族瑶族傣族自治县.md "wikilink")，东与南则以[红河](../Page/红河.md "wikilink")、[南溪河为界与](../Page/南溪河.md "wikilink")[越南](../Page/越南.md "wikilink")[老街省接壤](../Page/老街省.md "wikilink")。

县内地势北高南低，渐渐向东南倾斜。县内海拔最高点2363米，而南溪河、红河交汇处为最低海拔76.4米，为西南云南、[贵州](../Page/贵州.md "wikilink")、[四川三省的最低点](../Page/四川.md "wikilink")。\[1\]

目前下辖：河口镇和南溪镇；老范寨乡、桥头苗族壮族乡、瑶山乡和莲花滩乡。\[2\]

## 历史沿革

西汉属[牂柯郡](../Page/牂柯郡.md "wikilink")[进桑县地](../Page/进桑县.md "wikilink")，东汉为[进乘县](../Page/进乘县.md "wikilink")\[3\]。[三国](../Page/三国.md "wikilink")[蜀](../Page/蜀汉.md "wikilink")[建兴三年](../Page/建兴_（刘禅）.md "wikilink")（225年），分牂柯郡南部置[兴古郡](../Page/兴古郡.md "wikilink")\[4\]。进乘县属之。西晋因之。西晋末[宁州刺史](../Page/宁州.md "wikilink")[王逊分兴古郡置梁水郡](../Page/王逊.md "wikilink")，进乘县属之\[5\]。[西魏](../Page/西魏.md "wikilink")-[北周夺取](../Page/北周.md "wikilink")[梁的宁州之后](../Page/南梁.md "wikilink")，置[南宁州](../Page/南宁州_\(云南\).md "wikilink")（因西魏之前已将[豳州改名为](../Page/豳州_\(北魏\).md "wikilink")[宁州古加一](../Page/宁州_\(西魏\).md "wikilink")“南”字），该州所领郡县不可考\[6\]，今河口县地当属该州。隋改南宁州为[南宁州总管府](../Page/南宁州总管府.md "wikilink")\[7\]，[唐](../Page/唐朝.md "wikilink")[武德七年](../Page/武德.md "wikilink")（624年）改为南宁州都督府，贞观元年废（627年）；[调露元年](../Page/调露.md "wikilink")（679年）设[安南都护府](../Page/安南都护府.md "wikilink")，今河口县地属之。[大中八年](../Page/大中_\(唐朝\).md "wikilink")（854年）后，为[南诏](../Page/南诏.md "wikilink")[拓东节度所占领](../Page/拓东节度.md "wikilink")，今河口、马关一代为[七绾洞蛮地](../Page/七绾洞.md "wikilink")，亦属拓东节度。唐[咸通年间](../Page/咸通.md "wikilink")，南诏分拓东节度置[通海都督](../Page/通海都督.md "wikilink")，今河口县地属之。[大理国后期](../Page/大理国.md "wikilink")，改通海都督为[秀山郡](../Page/秀山郡.md "wikilink")，后又分东部为[最宁府](../Page/最宁府.md "wikilink")，河口地区为其下辖的[阿马部](../Page/阿马部.md "wikilink")、[阿月部属地](../Page/阿月部.md "wikilink")。

[元](../Page/元朝.md "wikilink")[至元十一年](../Page/至元.md "wikilink")（1274年），建立[云南行省](../Page/云南等处行中书省.md "wikilink")，河口地区属[临安广西元江等处宣慰司](../Page/临安广西元江等处宣慰司.md "wikilink")[临安路](../Page/临安路.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武十五年](../Page/洪武.md "wikilink")（1382年），改路为府，河口地区为[临安府](../Page/臨安府_\(雲南\).md "wikilink")[王弄山长官司地](../Page/王弄山长官司.md "wikilink")。清康熙五年（1666年），裁王弄山长官司，属[开化府](../Page/开化府.md "wikilink")[安平厅](../Page/安平厅.md "wikilink")。

道光中期，今河口县地辟为汛地，取名**河口汛**，是为河口之始。[光绪二十三年](../Page/光绪.md "wikilink")（1897年），设**河口对汛副督办公署**，由云南省直辖，河口辟为商埠，隶[临安开广道](../Page/临安开广道.md "wikilink")。1915年，置**河口对汛督办公署**。1926年河口一度划为特别行政区，由省府直辖。\[8\]

1950年1月设河口县，5月撤销河口对汛督办公署，设**河口市**，属[蒙自专区](../Page/蒙自专区.md "wikilink")。1955年改为**河口县**。1957年[红河哈尼族彝族自治州成立](../Page/红河哈尼族彝族自治州.md "wikilink")，河口县属红河州。1958年撤河口县及[屏边县的](../Page/屏边县.md "wikilink")[瑶山瑶族自治区](../Page/瑶山瑶族自治区.md "wikilink")，设河口瑶族自治县。1960年撤河口瑶族自治县及屏边苗族自治县，合设**河口瑶族苗族自治县**。1962年撤河口瑶族苗族自治县，恢复河口县、屏边县。1963年复设河口瑶族自治县。\[9\]

1992年，设立面积4.02平方千米的**河口边境经济合作区**，享省级经济管理权，后改设**河口边境经济贸易区**。\[10\]

2014年，[南溪镇列为](../Page/南溪镇_\(河口县\).md "wikilink")[全国重点镇](../Page/全国重点镇.md "wikilink")\[11\]。

## 人口

根据2010年第六次人口普查，该自治县常住总人口为104,609人。

其中，河口镇37,074人，南溪镇21,217人，老范寨乡4,806人，桥头苗族壮族乡16,616人，瑶山乡13,231人，莲花滩乡11,665人。\[12\]。

该县哈尼族人口为1,575人，占全县总人口的1.51%。\[13\]

## 交通

[Hekou-LaoCai_Bridge_01.jpg](https://zh.wikipedia.org/wiki/File:Hekou-LaoCai_Bridge_01.jpg "fig:Hekou-LaoCai_Bridge_01.jpg")河口老街铁路大桥\]\]
[Border_crossing_at_Lao_Cai.png](https://zh.wikipedia.org/wiki/File:Border_crossing_at_Lao_Cai.png "fig:Border_crossing_at_Lao_Cai.png")
是[昆河铁路的终点站](../Page/昆河铁路.md "wikilink")。连接至越南的[河老铁路](../Page/河老铁路.md "wikilink")（），并最终可达[海防](../Page/海防市.md "wikilink")。

2008年开通了一条与越南[老街联通的高速公路](../Page/老街.md "wikilink")，是云南境内第一条与[东盟国家联通的高速公路](../Page/东盟.md "wikilink")。

## 自然资源

矿藏主产[锑矿和](../Page/锑.md "wikilink")[汉白玉大理石](../Page/汉白玉大理石.md "wikilink")。\[14\]

由于地处山地[热带雨林](../Page/热带雨林.md "wikilink")，有着丰富的动、植物资源。其中鳖、黄鱼为红河特产\[15\]。

主要的土特产品为香蕉、菠萝等\[16\]。

## 友好城市

  - [老街市](../Page/老街市.md "wikilink")\[17\]

## 参考资料

[河口瑶族自治县](../Category/河口瑶族自治县.md "wikilink")
[县/自治县](../Category/红河州县市.md "wikilink")
[红河州](../Category/云南省民族自治县.md "wikilink")
[云](../Category/中国瑶族自治县.md "wikilink")

1.  [河口瑶族自治县概况](http://www.xzqh.org/html/show/yn/19904.html)，行政区划网。

2.
3.  《后汉书·郡国志》作“进乘”，[王先谦](../Page/王先谦.md "wikilink")《汉书补注》曰：“《续志》作进乘，盖误。”；《后汉书集解》：“《前汉》县作进桑，三国蜀因改属兴古郡”；然《晋书·地理志》、《宋书·州郡志》等并作“进乘”。

4.  《[水经注](../Page/水经注.md "wikilink")·温水》：“[刘禅建兴三年](../Page/刘禅.md "wikilink")，分牂柯置兴古郡，治[宛温县](../Page/宛温县.md "wikilink")。”

5.  《华阳国志校补图注卷四》

6.

7.
8.  [河口县历史沿革](http://www.xzqh.org/html/show/yn/19903.html)，行政区划网。

9.
10.
11.
12.
13.

14.

15.
16.
17.