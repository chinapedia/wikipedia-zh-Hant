[Stream_capture.png](https://zh.wikipedia.org/wiki/File:Stream_capture.png "fig:Stream_capture.png")
**河川襲奪**是一個[地理學上的名詞](../Page/地理學.md "wikilink")，指在[河流](../Page/河流.md "wikilink")[發育的過程中](../Page/侵蝕輪迴.md "wikilink")，相鄰的兩條河由於[側蝕或](../Page/侵蝕.md "wikilink")[向源侵蝕的關係](../Page/向源侵蝕.md "wikilink")，「[低位河](../Page/低位河.md "wikilink")」越過[分水嶺搶奪](../Page/分水嶺.md "wikilink")「[高位河](../Page/高位河.md "wikilink")」[上游](../Page/上游.md "wikilink")[集水區的現象](../Page/集水區.md "wikilink")，簡稱**襲奪**。襲奪高位河的低位河稱為「襲奪河」，被襲奪的高位河上游稱為「改向河」，剩餘失去源水的高位河河段則稱為「斷頭河」。

## 發生條件

兩條河間要發生河川襲奪現象，必須要有三個初始條件：

1.  兩條河川間的距離不能太遠。
2.  其中一條河川的側蝕或向源侵蝕強烈。
3.  必須一條為**高位河**、另一條為**低位河**。

## 過程

1.  低位河主流或支流向源侵蝕或側蝕至與高位河分界的分水嶺。
2.  侵蝕位置的分水嶺產生斷崖並造成岩塊崩塌。
3.  侵蝕越過原本的[山稜線使得山稜線下降並改變](../Page/山稜線.md "wikilink")。
4.  侵蝕位置的分水嶺消失，高位河上游河水改流入低位河形成河川襲奪。

## 影響

襲奪河因流量增加，改向河因侵蝕基準下移，侵蝕力皆增強而產生[峽谷或](../Page/峽谷.md "wikilink")[河階](../Page/河階.md "wikilink")，並出現回春作用。斷頭河則因失去集水區，呈現谷大水小的「無能河」情形，容易發生堆積，部份斷頭河因河道平緩產生[埤塘](../Page/池塘.md "wikilink")，例如[臺灣](../Page/臺灣.md "wikilink")[宜蘭的](../Page/宜蘭縣.md "wikilink")[雙連埤](../Page/雙連埤.md "wikilink")\[1\]，而其支流匯入處則容易形成[沖積扇](../Page/沖積扇.md "wikilink")。改向河與斷頭河的舊[河床形成新的分水嶺](../Page/河床.md "wikilink")，稱之為「風口」。由於集水區[河道改變使得下游](../Page/河道.md "wikilink")[水源分配發生變化](../Page/水源.md "wikilink")，對於用水量較大產業如[農業](../Page/農業.md "wikilink")、居民飲用水以及河道[運輸造成衝擊](../Page/運輸.md "wikilink")，搶水河下游水量增加有利發展，斷頭河下游則減少供水。

## 實例

  - 古[金沙江本經由](../Page/金沙江.md "wikilink")[紅河流入南海](../Page/紅河.md "wikilink")，古[长江本发源于](../Page/长江.md "wikilink")[巫山东麓](../Page/巫山.md "wikilink")。距今十几万年前，古长江不断进行[向源侵蚀](../Page/向源侵蚀.md "wikilink")，切穿[巫山](../Page/巫山.md "wikilink")，形成[三峡后](../Page/三峡.md "wikilink")，将[四川盆地古湖放空](../Page/四川盆地.md "wikilink")，进而向西南，在今[云南](../Page/云南.md "wikilink")[石鼓处](../Page/石鼓.md "wikilink")，切夺金沙江河道，使金沙江成为长江正源。
  - 約在六萬年前，[台北](../Page/臺北都會區.md "wikilink")[丘陵陷落形成](../Page/丘陵.md "wikilink")[台北盆地](../Page/台北盆地.md "wikilink")，原由東向西流在[林口入海的古](../Page/林口區.md "wikilink")[新店溪轉向北由](../Page/新店溪.md "wikilink")[淡水入海形成古](../Page/淡水區.md "wikilink")[淡水河](../Page/淡水河.md "wikilink")，同時沿著[林口台地邊緣向南侵蝕在三萬年前於](../Page/林口台地.md "wikilink")[石門襲奪原由東向西流在](../Page/石門區.md "wikilink")[桃園入海的古](../Page/桃園市.md "wikilink")[大漢溪](../Page/大漢溪.md "wikilink")。另一支流古[基隆河則向東侵蝕分別在二萬年前於](../Page/基隆河.md "wikilink")[八堵](../Page/暖暖區.md "wikilink")、[瑞芳連奪兩條原由南向北在](../Page/瑞芳區.md "wikilink")[基隆](../Page/基隆市.md "wikilink")、[瑞濱入海的河流](../Page/瑞芳區.md "wikilink")，匯集成今日的淡水河流域\[2\]\[3\]。
  - 現在，在[臺灣中部](../Page/臺灣.md "wikilink")[玉山東北側的](../Page/玉山.md "wikilink")[陳有蘭溪正向南侵蝕](../Page/陳有蘭溪.md "wikilink")，在[八通關大山及](../Page/八通關山.md "wikilink")[玉山北峰的陵線以北形成](../Page/玉山北峰.md "wikilink")[金門峒斷崖](../Page/金門峒斷崖.md "wikilink")，目前已開始越過山稜線，將來有可能到達[荖濃溪源流河道](../Page/荖濃溪.md "wikilink")，屆時高位的荖濃溪現有源流將會被低位的陳有蘭溪襲奪。不過由於被襲奪點以上的流域面積不大，對於荖濃溪流量影響甚微。\[4\]\[5\]。
  - [美國](../Page/美國.md "wikilink")[俄亥俄州與](../Page/俄亥俄州.md "wikilink")[西維吉尼亞州的](../Page/西維吉尼亞州.md "wikilink")[Teays河上游與](../Page/Teays河.md "wikilink")[俄亥俄河襲奪](../Page/俄亥俄河.md "wikilink")。
  - [日本](../Page/日本.md "wikilink")[廣島縣](../Page/廣島縣.md "wikilink")[太田川流域的](../Page/太田川流域.md "wikilink")[根之谷川與](../Page/根之谷川.md "wikilink")[可愛川襲奪](../Page/可愛川.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

## 相關條目

  - [向源侵蝕](../Page/向源侵蝕.md "wikilink")
  - [河川改道](../Page/河川改道.md "wikilink")

## 外部連結

  - [地理學課程——河流地形](http://mail.hlgs.hlc.edu.tw/~ouhw/geog11/river.htm) -
    [老歐的Home Page](http://mail.hlgs.hlc.edu.tw/~ouhw/)
  - [河流掠奪 -
    地理入門](http://ihouse.hkedcity.net/~hm1203/hydrosphere/river-capture.htm)

[Category:河流](../Category/河流.md "wikilink")
[Category:地形](../Category/地形.md "wikilink")

1.  網頁：鄧國雄，《[雙連埤的河川襲奪](http://sow.tw/conservation/action/swan-lien-pi/river.htm)》，[荒野保護協會](../Page/荒野保護協會.md "wikilink")[1](http://www.sow.org.tw/)，2006.8.7
2.  圖書：莊展鵬、黃盛璘，《台北地質形成史》，[遠流出版公司](../Page/遠流出版公司.md "wikilink")[2](http://www.ylib.com/)，《台北地質之旅》，22-25頁，1996.11.15再版
    ISBN 957-32-1280-3
3.  網頁：賴進貴、余俊青、高傳棋、謝品華、郭俊麟、王韋力，《[淡水河的形成過程](http://mars.csie.ntu.edu.tw/tamsui/subject/subject-1/index.html)
    》，[臺灣大學](../Page/臺灣大學.md "wikilink")，《[淡水河溯源數位博物館](http://mars.csie.ntu.edu.tw/tamsui/index.html)
    》，1998-1999
4.  新聞：謝介裕，《[河川襲奪
    荖濃溪可能斷頭](http://www.libertytimes.com.tw/2007/new/feb/13/today-life5.htm)
    》，[自由時報](../Page/自由時報.md "wikilink")，生活版，2007.2.13
5.  經濟部水利署網站歷史新聞[水利署今（13）日鄭重表示「河川襲奪係屬水文自然現象影響南部用水甚微」](http://www.wra.gov.tw/ct.asp?xItem=31395&ctNode=5281&comefrom=lp)