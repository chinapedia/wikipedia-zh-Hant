**河森正治**是[日本的](../Page/日本.md "wikilink")[機械設計師](../Page/機械設計師.md "wikilink")、[動畫監督](../Page/動畫.md "wikilink")、演出家。出生於[富山縣東礪波郡](../Page/富山縣.md "wikilink")（現[南砺市](../Page/南砺市.md "wikilink")）。

在遊戲、玩具等各領域裡也有參與。隸屬於[Studio
Nue](../Page/Studio_Nue.md "wikilink")。兼任[SATELIGHT取締役](../Page/SATELIGHT.md "wikilink")。

## 略歴

就讀於[慶應義塾高等學校時](../Page/慶應義塾高等學校.md "wikilink")，和同學的[美樹本晴彥](../Page/美樹本晴彥.md "wikilink")、[細野不二彥](../Page/細野不二彥.md "wikilink")、[大野木寛等人一同拜訪了以科幻美術知名的](../Page/大野木寛.md "wikilink")[Studio
Nue](../Page/Studio_Nue.md "wikilink")。考上[慶應義塾大学工学部之後](../Page/慶應義塾大学.md "wikilink")，做幫忙設計的打工甚至參與過[戴亞克隆系列作品](../Page/戴亞克隆.md "wikilink")，就這樣於1978年進入該公司。受到前輩[宮武一貴的薫陶](../Page/宮武一貴.md "wikilink")，在《[黑豹傳奇](../Page/黑豹傳奇.md "wikilink")（闘士ゴーディアン）》等作品以新銳機械設計師開始受到注意。而後請退了慶應義塾大学的學業。

1982年，在Studio
Nue原作的[科幻動畫](../Page/科幻動畫.md "wikilink")《[超時空要塞](../Page/超時空要塞.md "wikilink")》首次設計了主角機械的可變戰鬥機[VF-1女武神](../Page/VF-1女武神.md "wikilink")，以嶄新的創意一舉成名。另外在作品企畫階段參與的構成、演出方面也發揮了他的個性。1984年的劇場作品《[超時空要塞
愛還記得嗎](../Page/超時空要塞_愛還記得嗎.md "wikilink")》裡被提拔為共同監督，24歳初次監督作品其實力受到高度肯定。雖然許多人期待他的下一個作品，但沒能拿到企畫案1980年代後半在低迷中度過。短片的動畫電影《舞夢》也由於演出上的瓶頸而中止製作。

進入1990年代後，以機械設計師身份復出。經歷《[閃電霹靂車](../Page/閃電霹靂車.md "wikilink")》等作品，在1994年《[Macross
Plus](../Page/Macross_Plus.md "wikilink")》正式坐回監督的位子。此後在各方面持續活動，在《[機戰傭兵](../Page/機戰傭兵.md "wikilink")》系列等作品裡也有參與遊戲的製作。[-{zh-hans:索尼;zh-hk:索尼;zh-tw:新力;}-的](../Page/Sony.md "wikilink")[寵物狗](../Page/娛樂機器人.md "wikilink")[AIBO的改造設計也蔚為話題](../Page/AIBO.md "wikilink")。身為影像作家，在《[聖天空戰記](../Page/聖天空戰記.md "wikilink")》（原作）以後，能夠看到傾向神話傳承世界的作風，在描寫[宮澤賢治半個人生的](../Page/宮澤賢治.md "wikilink")《[イートハーブ幻想
Kenjiの春](../Page/イートハーブ幻想_Kenjiの春.md "wikilink")》中開拓了機器人動畫以外的新境界。

1995年參與[SATELIGHT公司的成立](../Page/SATELIGHT.md "wikilink")。2003年起擔任該公司的取締役一職，運用[3DCG技術向更複雜的機械動作表現挑戰](../Page/3DCG.md "wikilink")。破天荒的怪作《[創聖的亞庫艾里翁](../Page/創聖的亞庫艾里翁.md "wikilink")》（創聖機械天使）更引起動畫迷的熱烈討論。

## 人物

  - 被稱為「**變形的河森**」，以設計從機器人轉換到其他型態（飛行機、汽車）的變形機構為生計。不只在紙面上構圖，有時也會自己製作試作模型，提出立體化的企畫案。試作品使用[樂高積木](../Page/樂高.md "wikilink")，但因為仔細檢討可動部分的關係，可能花費數年後才設計完成。而在小時候父母親所買的積木[Fischer
    Technik](https://web.archive.org/web/20061116132622/http://www.ici2000.co.jp/fischertechnik.htm)，在性質上用來試作可變機械會比樂高輕鬆數倍，不過弄不到手所以使用樂高。此段在《機戰傭兵
    Mechanical Guidance（）》的訪談中所提到。
  - 身為創作者，擁有「不模仿別人」「不重複相同的模式」等對於獨創性要求的堅持。在每個作品都有令人驚奇的點子，不過對於人物多為傳統式的描寫。
  - 在虛構的機械設計中，會將現實中存在的飛機和汽車等投射到作品裡為其特徵。除了真實機械以外，也有受到《[雷鳥神機隊](../Page/雷鳥神機隊.md "wikilink")》的影響。喜愛飛機的個性為人所知，有從超音速爆撃機[XB-70取用](../Page/XB-70.md "wikilink")[女武神的名字](../Page/VF-1女武神.md "wikilink")，而《Macross
    Plus》更是向西片《[太空先鋒](../Page/太空先鋒.md "wikilink")》（THE RIGHT
    STUFF）表示敬意的作品。
  - 在製作《[Macross
    Plus](../Page/Macross_Plus.md "wikilink")》時，為了體驗駕駛員的空間感覺，曾與機械作畫監督[板野一郎到美國](../Page/板野一郎.md "wikilink")，實際進行過空中模擬戰。
  - 在私交深篤的電影導演[押井守的作品裡](../Page/押井守.md "wikilink")，提供了有活躍表現的空艇兵器等。2006年上映的押井導演的真人電影《[立喰師列伝](../Page/立喰師列伝.md "wikilink")》中，以偽印度人立喰師角色參與演出。
  - 因為長得黝黑有時也會使用「黒河影次」為筆名。此人物在馬克羅斯作品中以[隱藏角色身份登場](../Page/隱藏角色.md "wikilink")，遭遇到各式各樣的災難。

## 主要作品

### 電視動畫

  - 1978年 - [鬥將大武士](../Page/鬥將大武士.md "wikilink")（機械設計）
  - 1979年 - [黑豹傳奇](../Page/黑豹傳奇.md "wikilink")（闘士ゴーディアン）（機械設計）
  - 1979年 -
    [太空神話](../Page/太空神話.md "wikilink")（宇宙伝説ユリシーズ31）（日法合作動畫的企畫、機械設計）
  - 1979年 - [宇宙超人](../Page/宇宙超人.md "wikilink")（ザ☆ウルトラマン）（機械設計）
  - 1981年 - [無敵小戰士](../Page/無敵小戰士.md "wikilink")（ゴールドライタン）（機械設計）
  - 1982年 - [超時空要塞](../Page/超時空要塞.md "wikilink")（劇本、演出、監修、機械設計）
  - 1991年 - [閃電霹靂車](../Page/閃電霹靂車.md "wikilink")（車體設計）
  - 1994年 - [超时空要塞7](../Page/超时空要塞7.md "wikilink")（原作）
  - 1996年 - [聖天空戰記](../Page/聖天空戰記.md "wikilink")（原作、系列構成）
  - 1996年 - [イーハトーブ幻想
    Kenjiの春](../Page/イーハトーブ幻想_Kenjiの春.md "wikilink")（監督、脚本）
  - 1998年 - [星際牛仔](../Page/星際牛仔.md "wikilink")（原案協力、腳本）
  - 1998年 - [星方武俠OUTLAWSTAR](../Page/星方武俠OUTLAWSTAR.md "wikilink")（機械設計）
  - 2001年 - [地球少女Arjuna](../Page/地球少女Arjuna.md "wikilink")（原作、系列構成、監督）
  - 2001年 - [地球防衛家族](../Page/地球防衛家族.md "wikilink")（原作、系列構成）
  - 2002年 - [頑皮小白貂](../Page/頑皮小白貂.md "wikilink")（分鏡 白河明治名義）
  - 2005年 - [創聖機械天使](../Page/創聖的亞庫艾里翁.md "wikilink")（原作、系列構成、監督、機械設計）
  - 2005年 - [交響詩篇](../Page/交響詩篇.md "wikilink")（機械設計）
  - 2006年 - [玻璃艦隊](../Page/玻璃艦隊.md "wikilink")（機械設計）
  - 2007年 - [機神大戰](../Page/機神大戰.md "wikilink")（ギガンティック設計）
  - 2007年 - [星界死者之書](../Page/星界死者之書.md "wikilink")（機械設計）
  - 2008年 -
    [超时空要塞Frontier](../Page/超时空要塞Frontier.md "wikilink")（原作、總監督、系列構成、Valkyrie機體設計）
  - 2009年 - [BASQUASH\!](../Page/BASQUASH!.md "wikilink")（原作、项目主管）
  - 2009年 - [動物偵探奇魯米](../Page/動物偵探奇魯米.md "wikilink")（原作、編劇）
  - 2012年 - [創聖機械天使EVOL](../Page/創聖機械天使EVOL.md "wikilink")（原作、總監督）
  - 2012年 - [AKB0048](../Page/AKB0048.md "wikilink")（原作、總監督）
  - 2012年 - [交響詩篇AO](../Page/交響詩篇AO.md "wikilink")（機械設計）
  - 2013年 - [AKB0048 Next Stage](../Page/AKB0048.md "wikilink")（原作、總監督）
  - 2014年 - [愚者信長](../Page/愚者信長.md "wikilink")（原作、系列構成、機械設計）
  - 2014年 - [M3〜其為黑鋼〜](../Page/M3〜其為黑鋼〜.md "wikilink")（機械設計）
  - 2015年 - [創聖機械天使LOGOS](../Page/Aquarion_Logos.md "wikilink")（機械設計）
  - 2015年 - [新雷鳥神機隊](../Page/新雷鳥神機隊.md "wikilink")（雷鳥S號設計）
  - 2016年 - [超時空要塞Δ](../Page/超時空要塞Δ.md "wikilink")（原作、總監督）
  - 2018年 - [重神机潘多拉](../Page/重神机潘多拉.md "wikilink")（原作、總監督）

### 動畫電影

  - 1982年 - [Technopolis
    21C](../Page/Technopolis_21C.md "wikilink")（機械設計）
  - 1983年 - [Crusher Joe](../Page/Crusher_Joe.md "wikilink")（作画、機械設計）
  - 1989年 - [機動警察 the Movie](../Page/機動警察.md "wikilink")（機械設計協力）
  - 1993年 - [機動警察2 the Movie](../Page/機動警察.md "wikilink")（機械設計）
  - 1995年 - [GHOST IN THE SHELL /
    攻殻機動隊](../Page/攻殻機動隊.md "wikilink")（機械設計）
  - 2003年 - [WXIII 機動警察](../Page/機動警察.md "wikilink")（機械設計）
  - 2019年 - [劇場版 為了誰的鍊金術士](../Page/劇場版_為了誰的鍊金術士.md "wikilink")（總監督）

### OVA動畫

  - 1987年 - [破邪大星 彈劾凰](../Page/破邪大星_彈劾凰.md "wikilink")（主要機械設計）
  - 1991年 - [機動戰士Gundam 0083：Stardust
    Memory](../Page/機動戰士Gundam_0083：Stardust_Memory.md "wikilink")（[鋼彈試作1號機](../Page/GUNDAM開發計劃#RX-78GP01_"Zephyranthes".md "wikilink")、[2號機](../Page/GUNDAM開發計劃#RX-78GP02A_"Physalis"）.md "wikilink")、ALBION戰艦的造型）
  - 1992年 - [閃電霹靂車](../Page/閃電霹靂車.md "wikilink")11（車體設計）
  - 1993年 - [裝甲兵天使](../Page/裝甲兵天使.md "wikilink")（概念設計、機械設計）
  - 1994年 - [閃電霹靂車ZERO](../Page/閃電霹靂車.md "wikilink")（車體設計）
  - 1994年 - [Macross Plus](../Page/Macross_Plus.md "wikilink")（機械設定）
  - 1996年 - [閃電霹靂車SAGA](../Page/閃電霹靂車.md "wikilink")（車體設計）
  - 1998年 - [閃電霹靂車SIN](../Page/閃電霹靂車.md "wikilink")（車體設計）
  - 1998年 - [青之6號](../Page/青之6號.md "wikilink")（機械設計）
  - 2007年 - [創聖的亞庫艾里翁 －背叛的翅膀－](../Page/創聖的亞庫艾里翁.md "wikilink")
    (原作、系列構成、監督、機械設計）

### 特攝實寫

  - 1989年 - [GUNHED](../Page/Gunhed.md "wikilink")（機械設計）
  - 1996年 - [宇宙船レムナント6](../Page/宇宙船レムナント6.md "wikilink")（機械設計）
  - 2006年 - [立喰師列伝](../Page/立喰師列伝.md "wikilink")（「中辣的Sub」角色）

### 遊戲

  - 1989年 - [Out Live](../Page/Out_Live.md "wikilink")（機械設計）
  - 1997年 - [機戰傭兵系列](../Page/機戰傭兵.md "wikilink")（概念設計、機械設計）
  - 1997年 - [馬克羅斯 DIGITAL MISSION
    VF-X系列](../Page/超時空要塞_\(遊戲\).md "wikilink")（総監修、機械設計）
  - 1998年 - [超鋼戰紀](../Page/超鋼戰紀.md "wikilink")（企画、機械設計）
  - 1999年 - [Omega Boost](../Page/Omega_Boost.md "wikilink")（CG監修、機械設計）
  - 2006年 - PS2 - 新世紀GPX[閃電霹靂車](../Page/閃電霹靂車.md "wikilink") Road to the
    Infinity 3
  - 2011年 - [空戰奇兵：突擊地平線](../Page/空戰奇兵：突擊地平線.md "wikilink") （原創機體設計）
  - 2016年 - [為了誰的鍊金術士](../Page/為了誰的鍊金術士.md "wikilink")（OP影片監督）

### 工業製品、CM

  - 1980年 -
    [DIACLONE](../Page/Diaclone.md "wikilink")（[TAKARA的玩具機器人設計](../Page/TOMY.md "wikilink")）
  - 1992年 -
    [Battletech](../Page/:en:Battletech.md "wikilink")（桌上遊戲的日語版監修）
  - 2001年 - [AIBO](../Page/AIBO.md "wikilink")（ERS-220的外裝設計）
  - 2006年 - [變形金剛系列](../Page/變形金剛.md "wikilink") （DIACLONE以來的設計監修）

## 参考文献

  - 《》　Movic刊　2001年　160頁
  - 《》　MDN Corporation刊　2006年　208頁

## 外部連結

  - [SATELIGHT 官方網站](http://www.satelight.co.jp/)
  - [創聖的大天使 官方網站](http://www.aquarion.info/)
  - [个人官网](http://www.satelight.co.jp/kawamori/)

[Category:日本動畫導演](../Category/日本動畫導演.md "wikilink")
[Category:機甲設計師](../Category/機甲設計師.md "wikilink")
[Category:富山縣出身人物](../Category/富山縣出身人物.md "wikilink")