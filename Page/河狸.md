**河狸属**（[學名](../Page/學名.md "wikilink")：**）是[囓齒目](../Page/囓齒目.md "wikilink")[河狸科的一属](../Page/河狸科.md "wikilink")，也是河狸科存活至今的唯一一属，是世界第二大的囓齒動物（僅次於[水豚](../Page/水豚.md "wikilink")），本屬现存兩種河狸，分別為[美洲河狸](../Page/美洲河狸.md "wikilink")（*Castor
canadensis*）與[歐亞河狸](../Page/歐亞河狸.md "wikilink")（*Castor fiber*）。

河狸有「自然界[水壩工程師](../Page/水壩.md "wikilink")」之稱。主要分布在[美洲北部](../Page/美洲.md "wikilink")，至於[歐洲和](../Page/歐洲.md "wikilink")[亞洲的分布則非常少](../Page/亞洲.md "wikilink")。

在北美洲，河狸的數量曾經超過 6 千萬頭，但在 1988 年僅剩下 6 百萬至 1 千 2
百萬頭。數量銳減的主因是[狩獵](../Page/狩獵.md "wikilink")，河狸的毛皮具有經濟價值，而牠們的腺體則能用來當作藥或是香水；另外，牠們會咬斷樹木以及建築水壩造成淹水，這些行為都會干擾人類的土地利用，因此該地區的河狸也會因此被人類獵殺\[1\]。

在歐洲，河狸受到[聯合國的保護](../Page/聯合國.md "wikilink")\[2\]，在[中國河狸被列為一級](../Page/中國.md "wikilink")[保護動物](../Page/保護動物.md "wikilink")，並設有[新疆布爾根河狸自然保護區](../Page/新疆.md "wikilink")\[3\]。

## 描述

河狸與[囊鼠科](../Page/囊鼠科.md "wikilink")、[異鼠科均為](../Page/異鼠科.md "wikilink")的齧齒類，幾乎僅分布於北美洲。雖然現存僅有
2
種，不過自[始新世開始北半球就有河狸的化石紀錄了](../Page/始新世.md "wikilink")，並且曾有許多巨型物種存在，例如歐洲的（*Trogontherium*）以及北美洲的[巨河狸](../Page/巨河狸.md "wikilink")（*Castoroides*）。

河狸會在河流或溪流中利用樹枝、草和泥巴構築水壩，形成池塘，並在池塘中建立巢穴（又稱為小屋（））；同時也會建造運河來運輸巢穴與水壩的建材\[4\]\[5\]。牠們會用鋒利的[門牙咬斷樹木與其他植物作為建材以及食物](../Page/門牙.md "wikilink")。

[Beaverbones.jpg](https://zh.wikipedia.org/wiki/File:Beaverbones.jpg "fig:Beaverbones.jpg")

[Beaver_skeleton.jpg](https://zh.wikipedia.org/wiki/File:Beaver_skeleton.jpg "fig:Beaver_skeleton.jpg")[奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")）\]\]

當發現潛在的威脅時，在水中的河狸會用力拍擊水面，警告範圍內的所有河狸。在聽到警告聲後，所有河狸都會躲入水中，牠們在陸地上時雖然行動緩慢，卻很擅長游泳，而且可以憋氣長達
15 分鐘。

河狸並不[冬眠](../Page/冬眠.md "wikilink")，而是會將樹枝和樹幹堆放在池塘中，做為儲糧。

河狸的後腳有蹼、並且具有寬扁的尾巴。視力不好，但聽覺、嗅覺和觸覺敏銳。牠們必須透過不斷啃咬木頭來防止門牙變得過長\[6\]。牠們的四顆門牙由前側堅硬的琺瑯質以及背側較柔軟的[牙本質所組成](../Page/牙本質.md "wikilink")，透過啃咬木頭的方式將門牙磨利成[鑿狀](../Page/鑿.md "wikilink")。河狸門牙中上的琺瑯質含有鐵，比起其他哺乳動物的門牙更能抵抗酸蝕\[7\]。

河狸終其一生不斷成長，成年體重可以超過 ，雌性一般較雄性大。在野外壽命可長達 24 年。

## 栖息地

### 河狸水坝

河狸建造水坝的主要目的是为了防范天敌，如土狼、狼、熊等，冬天时更容易获取食物。他们总是在夜里开工，而且是高产的建筑师，用前爪搬运泥和石块，用牙搬运木料。不移除河狸的话，很难摧毁水坝，因为他们隔夜就能重建一座水坝。河狸可能沿河建造一系列的水坝。

[Blaeu_-_Nova_Belgica_et_Anglia_Nova_(Detail_Hudson_Area).png](https://zh.wikipedia.org/wiki/File:Blaeu_-_Nova_Belgica_et_Anglia_Nova_\(Detail_Hudson_Area\).png "fig:Blaeu_-_Nova_Belgica_et_Anglia_Nova_(Detail_Hudson_Area).png")谷地图上的河狸
c. 1635\]\]

## 参考文献

## 延伸阅读

  -
  - Article on the history of beavers in Britain.

  -
  -
  -
  -
  - *The Tent Dwellers* – [Beavers' habits, habitat and conservation
    status (as of 1908) are recurring
    themes](http://www.treesearch.fs.fed.us/pubs/23250) by Albert
    Bigelow Paine.

## 外部链接

  - [Beaver Facts &
    Pictures](http://animal.discovery.com/mammals/beaver/)

  - [Beaver
    Tracks](http://northernbushcraft.com/animalTracks/beaver/notes.htm):
    How to identify beaver tracks in the wild

  - *[The Romance of the Beaver: Being the History of the Beaver in the
    Western
    Hemisphere](https://archive.org/details/romanceofbeaverb00dugmiala)*
    by A. Radclyffe Dugmore

  - [Aigas Field Centre Beaver
    Project](https://web.archive.org/web/20070921033600/http://www.aigas.co.uk/2007-beaver-diary-g.asp)
    – history of a pair of European beavers released into a large
    enclosure in the Highlands of Scotland.

  - [The Canadian Encyclopedia, The
    Beaver](http://thecanadianencyclopedia.com/articles/beaver-rodent)

  -
  - [Video of a beaver building a
    canal](https://archive.is/20130628033353/http://blog.scottishbeavers.org.uk/tag/beaver-canal/)

[河狸科](../Category/河狸科.md "wikilink")
[Category:中国国家一级保护动物](../Category/中国国家一级保护动物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  Nowak, Ronald M. 1991. pp. 364–367. *Walker's Mammals of the World*
    Fifth Edition, vol. I. Johns Hopkins University Press, Baltimore.
2.  [野生世界中的建筑师](http://www.un.org/chinese/unworks/environment/animalplanet/beaver.html)
3.
4.
5.
6.
7.