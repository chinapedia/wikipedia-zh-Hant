**河西节度使**，[唐朝在](../Page/唐朝.md "wikilink")[凉州设置的](../Page/凉州.md "wikilink")[节度使](../Page/节度使.md "wikilink")。[唐玄宗时](../Page/唐玄宗.md "wikilink")，作为十大节度使之一。[晚唐以后复置的河西军](../Page/晚唐.md "wikilink")，是作为唐末[五代时凉州一带的官军残余势力](../Page/五代.md "wikilink")。

## 盛唐时历史

[唐睿宗](../Page/唐睿宗.md "wikilink")[景云二年](../Page/景云.md "wikilink")（711年）四月以贺拔延嗣为凉州[都督充河西节度使](../Page/都督.md "wikilink")，河西节度使负责断隔[吐蕃](../Page/吐蕃.md "wikilink")、[突厥](../Page/突厥.md "wikilink")，治凉州（武威郡，今[甘肃](../Page/甘肃.md "wikilink")[武威](../Page/武威.md "wikilink")），统辖凉州、[甘州](../Page/甘州.md "wikilink")、[肃州](../Page/肃州.md "wikilink")、[瓜州](../Page/瓜州.md "wikilink")、[沙州](../Page/沙州.md "wikilink")、[伊州](../Page/伊州.md "wikilink")、[西州等](../Page/西州.md "wikilink")7州。

### 历任节度使

  - [贺拔延嗣](../Page/贺拔延嗣.md "wikilink") 711年——714年
  - [郭虔瓘](../Page/郭虔瓘.md "wikilink") 714年——720年
  - [杨敬述](../Page/杨敬述.md "wikilink") 720年——721年
  - [王君㚟](../Page/王君㚟.md "wikilink")（音chuò，廣韻丑略切） 721年——723年
  - [张敬忠](../Page/张敬忠.md "wikilink") 723年——727年
  - [李嗣直](../Page/李琮_\(奉天皇帝\).md "wikilink") 727年
  - [萧嵩](../Page/萧嵩.md "wikilink") 727年——729年
  - [牛仙客](../Page/牛仙客.md "wikilink") 732年——737年
  - [崔希逸](../Page/崔希逸.md "wikilink") 737年——738年
  - [萧炅](../Page/萧炅.md "wikilink") 738年——740年
  - [盖嘉运](../Page/盖嘉运.md "wikilink") 740年——742年
  - [王倕](../Page/王倕.md "wikilink") 742年——744年
  - [夫蒙灵詧](../Page/夫蒙灵詧.md "wikilink") 744年——746年
  - [皇甫惟明](../Page/皇甫惟明.md "wikilink") 746年
  - [王忠嗣](../Page/王忠嗣.md "wikilink") 746年——747年
  - [安思顺](../Page/安思顺.md "wikilink") 747年——752年
  - [高仙芝](../Page/高仙芝.md "wikilink") 751年（未任）
  - [哥舒翰](../Page/哥舒翰.md "wikilink") 753年——756年
  - [王思礼](../Page/王思礼.md "wikilink") 756年
  - [周佖](../Page/周佖.md "wikilink") 756年——757年
  - 丰王[李珙](../Page/李珙.md "wikilink") 756年
  - [郭子仪](../Page/郭子仪.md "wikilink")
    757年[九月](../Page/九月.md "wikilink")——759年[三月](../Page/三月.md "wikilink")
  - [来瑱](../Page/来瑱.md "wikilink") 759年
  - [杨志烈](../Page/杨志烈_\(唐朝\).md "wikilink")
    759年——765年[十月](../Page/十月.md "wikilink")
  - [杨休明](../Page/杨休明.md "wikilink") 约765年下半年——766年
  - [周鼎](../Page/周鼎.md "wikilink") 约766年——771年
    [唐](../Page/唐.md "wikilink")[大历六年](../Page/大历.md "wikilink")（公元771年），周鼎为阎朝所杀，朝廷不再任命河西节度使，河西节度使遂废。唐建中二年（公元781年），阎朝被迫投降吐蕃，沙州遂陷落。

## 复置

自[安史之乱后](../Page/安史之乱.md "wikilink")，吐蕃乘机东进，曾兵临[长安城下](../Page/长安.md "wikilink")，后遂以[六盘山为界](../Page/六盘山.md "wikilink")，以西为[吐蕃所有](../Page/吐蕃.md "wikilink")，原河西节度使撤销。公元842年，吐蕃[赞普](../Page/赞普.md "wikilink")[朗达玛被杀](../Page/朗达玛.md "wikilink")，吐蕃陷入内乱之中，敦煌一带的汉族和其他民族在[张议潮带领下驱逐吐蕃驻军](../Page/张议潮.md "wikilink")，并向东向西进军，一度收复了河西一带的唐朝故土，在入朝后被授予河西等十一州观察处置使。朝廷派遣兵卒郓人二千五百员戌守凉州（西凉府），因[黄巢之乱而隔绝](../Page/黄巢.md "wikilink")，而西面则因为[甘州为回鹘所夺取与瓜沙](../Page/甘州.md "wikilink")[归义军阻断](../Page/归义军.md "wikilink")，遂自立。

  - 朱氏 中和年间
  - 胡敬璋 895年
  - 翁郜 唐昭宗时

## 五代时历史

[五代时](../Page/五代.md "wikilink")，河西地为[回鹘](../Page/回鹘.md "wikilink")、[党项分占](../Page/党项.md "wikilink")，唯有凉州、甘州、瓜州、沙州常遣使进贡。瓜沙二州是归义军辖地，中间有甘州隔开，只有凉州较靠近中原。[後梁太祖时曾以](../Page/後梁太祖.md "wikilink")[朔方节度使兼领河西节度](../Page/朔方节度使.md "wikilink")，而观察甘、肃、威等州。然而徒有其名，凉州人民自立守将。

[后唐明帝](../Page/李嗣源.md "wikilink")[长兴四年](../Page/长兴.md "wikilink")（933年），凉州留后[孙超遣大将](../Page/孙超.md "wikilink")[拓跋承谦入朝进贡](../Page/拓跋承谦.md "wikilink")，受明帝召见，奏称：“[凉州东距](../Page/凉州.md "wikilink")[灵武千里](../Page/灵武.md "wikilink")，西北至甘州五百里。旧有[郓州人二千五百为戍兵](../Page/郓州.md "wikilink")，及黄巢之乱，遂为阻绝。超及城中汉户百余，皆戍兵之子孙也。其城今方幅数里，中有[县令](../Page/县令.md "wikilink")、[判官](../Page/判官.md "wikilink")、[都押衙](../Page/都押衙.md "wikilink")、[都知兵马使](../Page/都知兵马使.md "wikilink")，衣服言语略如汉人。”明帝遂即任命孙超为凉州刺史，兼河西军节度留后。

[清泰元年](../Page/清泰.md "wikilink")（934年），留后[李文谦来请命](../Page/李文谦.md "wikilink")。几年后，凉州人逐出李文谦，灵武节度使[冯晖遣部下](../Page/冯晖.md "wikilink")[吴继勋代李文谦为留后](../Page/吴继勋.md "wikilink")，时为[天福七年](../Page/天福.md "wikilink")（942年）。次年（943年），[后晋高祖遣](../Page/后晋高祖.md "wikilink")[泾州押牙](../Page/泾州.md "wikilink")[陈延晖携诏书安抚凉州](../Page/陈延晖.md "wikilink")，凉州人挽留陈延晖，立为刺史。[后汉隐帝](../Page/后汉隐帝.md "wikilink")[乾祐年间](../Page/乾祐.md "wikilink")（948年－950年之间），权知凉州留后[折逋嘉施遣使者入朝廷请命](../Page/折逋嘉施.md "wikilink")，受封为节度使。嘉施是当地土豪。

[后周](../Page/后周.md "wikilink")[广顺二年](../Page/广顺.md "wikilink")（952年），折逋嘉施到开封卖马，并且请求朝廷派遣将帅官吏，时任[枢密使的](../Page/枢密使.md "wikilink")[王峻奏请起用故人](../Page/王峻.md "wikilink")[申师厚以左卫将军拜为](../Page/申师厚.md "wikilink")**河西节度使**。申师厚至凉州，推荐押衙副使[崔虎心](../Page/崔虎心.md "wikilink")、[阳妃谷首领](../Page/阳妃谷.md "wikilink")[沈念般等以及当地汉人](../Page/沈念般.md "wikilink")[王廷翰](../Page/王廷翰.md "wikilink")、[温崇乐](../Page/温崇乐.md "wikilink")、[刘少英为将吏](../Page/刘少英.md "wikilink")，又自[安国镇至凉州](../Page/安国镇.md "wikilink")，设立三州以控扼诸羌，用当地酋豪[折逋支等人为刺史](../Page/折逋葛支.md "wikilink")，得到朝廷允诺。但是当地汉人和少数民族杂居，申师厚又不能安抚，[后周世宗](../Page/后周世宗.md "wikilink")[显德元年](../Page/显德.md "wikilink")（954年），申师厚为其所迫，命儿子为留后而独自逃归，被贬官。凉州遂与中原断了联系。

### 历任留后及节度

#### 朔方军兼领河西节度

  - [韩逊](../Page/韩逊.md "wikilink") 开平元年（907年）——乾化四年（914年）
  - [韩洙](../Page/韩洙.md "wikilink") 乾化四年（914年）——天成三年（928年）
  - [韩璞](../Page/韩璞.md "wikilink") 天成三年（928年）
  - [韩澄](../Page/韩澄.md "wikilink") 天成四年（929年）
  - [康福](../Page/康福.md "wikilink") 天成四年（929年）——长兴元年（930年）

#### 凉州留后及河西节度

  - [马全节](../Page/马全节.md "wikilink") 长兴元年（930年）
  - [孙超](../Page/孙超_\(节度使\).md "wikilink") 长兴二年（931年）——长兴四年（933年）
  - [李文谦](../Page/李文谦.md "wikilink") 清泰元年（934年）——天福六年（941年）
  - [吴继兴](../Page/吴继兴.md "wikilink") 天福六年（941年）——天福七年（942年）
  - [陈延晖](../Page/陈延晖.md "wikilink") 天福七年（942年）——开运元年（944年）
  - [张遵古](../Page/张遵古.md "wikilink") 开运二年（945年）——天福十二年（947年）
  - [折逋嘉施](../Page/折逋嘉施.md "wikilink") 乾祐元年（948年）——广顺元年（951年）
  - [申师厚](../Page/申师厚.md "wikilink") 广顺元年（951年）——显德元年（954年）
  - 申师厚子 954年--？？

## 后续

之后其地为号称的[六谷部](../Page/六谷部.md "wikilink")[吐蕃所占](../Page/吐蕃.md "wikilink")，宋朝时曾来请帅，当时[宋惟清在凉州买马](../Page/宋惟清.md "wikilink")，命以领州事。

## 参考文献

  - 《[新唐书](../Page/新唐书.md "wikilink")》
  - 《[新五代史](../Page/新五代史.md "wikilink")》四夷附录
  - 《[宋史](../Page/宋史.md "wikilink")》外国传八

## 参见

  - [归义军](../Page/归义军.md "wikilink")
  - [六谷部吐蕃](../Page/六谷部.md "wikilink")

{{-}}

[河西節度使](../Category/河西節度使.md "wikilink")
[Category:唐朝官制](../Category/唐朝官制.md "wikilink")