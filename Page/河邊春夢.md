《**河邊春夢**》（）為知名[臺語歌曲](../Page/臺語.md "wikilink")，作曲作詞者為[周添旺](../Page/周添旺.md "wikilink")。\[1\]因為歌曲作於1934年的[台灣日治時期](../Page/台灣日治時期.md "wikilink")，演唱者為[日本人](../Page/日本人.md "wikilink")[松原靜韻](../Page/松原靜韻.md "wikilink")，此間的「河」係指[淡水河](../Page/淡水河.md "wikilink")。河邊春夢隨後由周添旺所屬[古倫美亞唱片](../Page/古倫美亞唱片.md "wikilink")（現稱『[哥倫比亞唱片](../Page/哥倫比亞唱片.md "wikilink")』）製播成台語唱片，不過因日籍歌手不熟台語，咬字不清，該歌曲並未受到矚目。

## 緣起

1933年，因《[月夜愁](../Page/月夜愁.md "wikilink")》流行成名的周添旺，以24歲年紀，進入哥倫比亞唱片會社，擔任企劃製作，但是仍持續詞曲創作。因為時值日治時期，為符合社會需求，《河邊春夢》除了製作台語版外，也以日文版面世。而名為《新夜花》的日語版與台語版的河邊春夢皆由日籍歌手-{松}-原·靜韻演唱。\[2\]　

## 十三號水門命案

[Taipei_Bridge_(1925)_3.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Bridge_\(1925\)_3.jpg "fig:Taipei_Bridge_(1925)_3.jpg")。圖為1925年的台北大橋。\]\]
《河邊春夢》雖早在1934年出品問世，但是真正流行卻是在於日治時期結束後的1950年代，而讓河邊春夢真正廣為人知的，是一部描述真實社會案件的同名電影。

1950年1月13日，[台灣廣播電台](../Page/台灣廣播電台.md "wikilink")\[3\]\[4\]播音員[陳素卿於淡水河十三號水門偕已婚男友同事](../Page/陳素卿.md "wikilink")[張白帆殉情自殺](../Page/張白帆.md "wikilink")，後僅陳女自殺身亡。因為《[中央日報](../Page/中央日報.md "wikilink")》刊載死者留下「文情並茂」遺書\[5\]，因此引起包含[台大校長](../Page/台大.md "wikilink")[傅斯年](../Page/傅斯年.md "wikilink")\[6\]等社會各階層人士注目\[7\]。後該案被《[台灣新生報](../Page/台灣新生報.md "wikilink")》記者[沈嫄璋揭穿是他殺](../Page/沈嫄璋.md "wikilink")\[8\]。

約略同時期，敘述上述「淡水河十三號水門」社會案件；並以台語歌曲《河邊春夢》為主題曲的同名電影上映，於台灣頗為賣座。\[9\]至此，《河邊春夢》一曲才藉由社會案件與電影媒體於[台灣盛為流行](../Page/台灣.md "wikilink")。

另外，頗為流行的《河邊春夢》，除了原唱者靜韻之外，數十年來翻唱者眾。較知名者有[紀露霞](../Page/紀露霞.md "wikilink")、[文夏](../Page/文夏.md "wikilink")、[郭金發](../Page/郭金發.md "wikilink")、[王識賢](../Page/王識賢.md "wikilink")、[秀蘭瑪雅等](../Page/秀蘭瑪雅.md "wikilink")。而與《河邊春夢》歌曲相關的翻拍電影與電視劇集也常見。例如，1964年\[10\]及1968年\[11\]就翻拍過兩回[台語電影](../Page/台語電影.md "wikilink")。

## 背景

以台灣北部最大河川淡水河為創作標的的《河邊春夢》，詞曲內容主要敘述民風保守下，男女愛意無法表達的鬱悶心情。該曲除特有的[台灣小調曲式外](../Page/台灣小調.md "wikilink")，也以四段歌詞與四種完全不同韻腳，闡述於淡水河邊，因為河水湍急引發的哀怨。其中歌詞中「目睭看橋頂」\[12\]的橋頂，就是[台北大橋](../Page/台北大橋.md "wikilink")，而第四段歌詞的「淡水無崁蓋」\[13\]一句，則暗指當時台北地區，頗多為情所困年輕人跳淡水河自殺的情景。

雖然鼓勵自殺的第四段歌詞，後來於1950年代重唱階段，遭原作者周添旺刪除，但至今與[自殺相關的](../Page/自殺.md "wikilink")「淡水河無崁蓋」一詞，仍為台灣常見的俗語。

## 備註

## 參考資料

  - [自由時報副刊，河邊春夢─台灣歌謠界泰斗周添旺](https://web.archive.org/web/20070929133626/http://www.libertytimes.com.tw/2001/new/aug/30/life/article-1.htm)
  - [由河邊春夢到滿面春風](https://web.archive.org/web/20070927011222/http://www.taiwan123.com.tw/main_digi.asp?id=54)
  - 《歷史月刊》第33期
  - [河邊春夢歌詞](http://www.hyes.tyc.edu.tw/~sk7/s43.htm)
  - [河邊春夢](https://web.archive.org/web/20070927011319/http://www.taiwan123.com.tw/musicdata/search_sd.asp?id=26)
  - [新竹市文化局，王台新收藏展2-台語片明星照片展](http://www.hcccb.gov.tw/chinese/02activity/act_a02.asp?bull_id=989)
  - [台灣俗語](https://web.archive.org/web/20071107101543/http://web.pu.edu.tw/~folktw/proverb/proverb_a10.htm)
  - {{@movies|fCcmb4041380}}

[分類:1934年歌曲](../Page/分類:1934年歌曲.md "wikilink")
[分類:大稻埕背景作品](../Page/分類:大稻埕背景作品.md "wikilink")

[Category:闽南语歌曲](../Category/闽南语歌曲.md "wikilink")
[Category:台灣歌曲](../Category/台灣歌曲.md "wikilink")
[Category:台语歌曲](../Category/台语歌曲.md "wikilink")
[Category:台灣電影主題曲](../Category/台灣電影主題曲.md "wikilink")
[Category:1960年代台灣電影作品](../Category/1960年代台灣電影作品.md "wikilink")
[Category:愛情題材歌曲](../Category/愛情題材歌曲.md "wikilink")
[Category:河川題材歌曲](../Category/河川題材歌曲.md "wikilink")

1.  不過亦有學者認為河邊春夢的作曲部分為[黎明](../Page/黎明_\(作曲家\).md "wikilink")（本名[楊樹木](../Page/楊樹木.md "wikilink")）作品。
2.  另有，台語與日語兩版本分由松原及靜韻兩日籍歌手演唱的說法。
3.  同年，改名為中國廣播電台，即[中廣](../Page/中廣.md "wikilink")。
4.  館址則位於今[二二八和平公園內的](../Page/二二八和平公園.md "wikilink")[台北市二二八紀念館](../Page/台北市二二八紀念館.md "wikilink")
5.  該兩封遺書後經證實為張男偽造，張男後因謀殺陳素卿罪嫌遭逮捕，之後遭法院以「幫助自殺罪」（或謀殺罪）重判7年徒刑。
6.  傅斯年連同數位台大教授，亦與中央日報舉辦「補葬陳素卿活動」。
7.  據報載，留給男主角的其中一封遺書，「內容文情並茂，字裡行間流露出高貴純潔的愛戀及無怨無悔的癡心，令讀者為之鼻酸落淚。」
8.
9.  因台語電影尚未出現，因此應為國語電影、默片、廈語片或其他片種。
10. 由女星游娟主演
11. [高幸枝](../Page/高幸枝.md "wikilink")、[石軍](../Page/石軍.md "wikilink")、[江青霞等演員參演](../Page/江青霞.md "wikilink")
12. 即「眼睛看橋上」
13. 即「淡水河沒有加蓋」，見