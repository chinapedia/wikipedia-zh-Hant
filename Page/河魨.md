**河魨**（英語:**Puffer
fish**），常作**河豚**，古名**肺鱼**，俗称**气鼓鱼**、**气泡鱼**、**吹肚鱼**、**雞泡魚**、**青郎君**、**刺䲅**等\[1\]\[2\]，一般泛指[魨形目中](../Page/魨形目.md "wikilink")[二齒魨科](../Page/二齒魨科.md "wikilink")、[三齒魨科](../Page/三齒魨科.md "wikilink")、[四齒魨科以及](../Page/四齒魨科.md "wikilink")[箱魨科所屬的](../Page/箱魨科.md "wikilink")[魚類](../Page/魚.md "wikilink")。河魨普遍分佈在世界各地北緯45度至南緯45度之間的[海水](../Page/海水.md "wikilink")、[淡水等水域](../Page/淡水.md "wikilink")。河魨普遍具有膨脹身體的能力，能夠將大量的水或空氣吸入極具彈性的胃中，使身體大小膨脹數倍，以嚇阻掠食者。同時，大多數四齒魨科以及箱魨科的河魨，分別具有劇毒[河魨毒素及](../Page/河魨毒素.md "wikilink")[箱魨毒素](../Page/箱魨毒素.md "wikilink")，依品種分佈於內臟、肌肉、血液、皮膚等等不同部位，毒性並隨季節有所變化。河魨肝最毒，但富含[ω-3脂肪酸](../Page/ω-3脂肪酸.md "wikilink")，而且味道可口，1975年日本傳奇歌舞伎演員八代目[坂東三津五郎吃了四份河魨肝](../Page/坂東三津五郎.md "wikilink")，中毒身亡，日本政府之後便下令禁吃河魨肝。中国大陆也一度禁止市场上的河魨销售，直到2016年年底方有限度解禁。

## 命名

在中文里，河魨自古以来有多种叫法。因河魨在受到威胁时会鼓起身体，还能发出“咕咕”的声音，因此中国古代称其为**䲅**、**鱼**、**黄驹**、**魺**、**嗔鱼**、**鲑**。广东海丰、潮州、汕头一带的闽语仍然有称河魨为“**鲑鱼**”的用法\[3\]
。

因河魨体表的斑纹，中文里又称其为**鲐鱼**，以及**鮧**、**鯸鲐**、**鯸鮧**、**鰗鮧**、**鹕夷**、**鯸䱌**等音近的称呼。此外，还有**䰽**的称呼\[4\]。

## 食用

中國很早就對其毒性有所見解。晉人[左思](../Page/左思.md "wikilink")《三都賦·吳都賦》便有“王鮪鯸鮐”之句，其注曰：“鯸鮐魚，狀如蝌蚪，大者尺-{zh-hans:余;zh-hant:餘}-，腹下白，背上青黑，有黃紋，性有毒。”[沈括在](../Page/沈括.md "wikilink")《[夢溪筆談](../Page/夢溪筆談.md "wikilink")》中說：“吳人嗜河豚魚，有遇毒者，往往殺人，可為深戒。”《[太平廣記](../Page/太平廣記.md "wikilink")》亦云：“鯸鮐魚文斑如虎，俗-{云}-煮之不熟，食者必死。”《嘉靖江陰縣志》在“魚之屬”中提到：“河豚，……凡腹、子、目、精、脊血有毒。”《丹徒縣志》稱：“子與眼人知去之。血藏脂內，脂至肥美，有西施乳之稱，食者必不肯棄。苟治不法，則危矣。”清代名醫[王士雄稱](../Page/王士雄.md "wikilink")：“其肝、子與血尤毒。或-{云}-去此三物，洗之極凈，食之無害。”

河魨肉極度美味，是[中国](../Page/中国.md "wikilink")“[长江三鲜](../Page/长江三鲜.md "wikilink")”（河魨、[刀鱼](../Page/刀鱼.md "wikilink")、[鲥鱼](../Page/鲥鱼.md "wikilink")）之首，长江镇江下游产的河魨则是[长江中最好的](../Page/长江.md "wikilink")。北宋詩人[梅堯臣的](../Page/梅堯臣.md "wikilink")《范饒州坐中客語食河豚魚》描寫：“春洲生荻芽，春岸飛楊花。河豚當是時，貴不數魚蝦。”[歐陽修在](../Page/歐陽修.md "wikilink")《六一詩話》裡說：“河豚嘗生於春暮，群游水上，食絮而肥。南人多與荻芽為羹，-{云}-最美。故知詩者謂只破題兩句，已道盡河魨好處。聖俞平生苦於吟詠，以閒遠古淡為意，故其構思極艱。此詩作於樽俎之間，筆力雄贍，頃刻而成，遂為絕唱。”另外[蘇軾也寫下](../Page/蘇軾.md "wikilink")“蒌蒿满地芦芽短，正是河豚欲上时”，\[5\]並曾說河魨味道“值那一死”\[6\]。宋朝人用[蒌蒿](../Page/蒌蒿.md "wikilink")、[芦苇的幼芽与河豚合烹](../Page/芦苇.md "wikilink")，據說是可以解毒。\[7\]常有美食家因河豚料理不當或品種不分，造成意外中毒死亡，因而古有「拼死吃河豚——勉強從事」的說法。現在河魨的研究以[日本研究最深](../Page/日本.md "wikilink")，在日本，河魨的料理均需嚴格訓練領有執照的廚師才能夠進行。日本已養殖出無毒河魨。養殖則以中國大陸為大宗，每年皆飼養輸出大量河魨至日本等地。

## 观赏

河魨同時也是一種觀賞魚，在水族館中的觀賞用河魨，一般根據體型大小分別稱為[狗頭或是](../Page/狗頭.md "wikilink")[娃娃](../Page/娃娃_\(河豚\).md "wikilink")。常見的觀賞用河魨有[金娃娃](../Page/金娃娃.md "wikilink")、[八字娃娃](../Page/八字娃娃.md "wikilink")、[黃金娃娃](../Page/黃金娃娃.md "wikilink")、[巧克力娃娃](../Page/巧克力娃娃.md "wikilink")、[皇冠狗頭](../Page/皇冠狗頭.md "wikilink")、[斑馬狗頭](../Page/斑馬狗頭.md "wikilink")，[紅木瓜狗頭等等](../Page/紅木瓜狗頭.md "wikilink")。吃小魚，小蝦，螺(水生蝸牛)。
[Arothron_nigropunctatus_East_Timor.jpg](https://zh.wikipedia.org/wiki/File:Arothron_nigropunctatus_East_Timor.jpg "fig:Arothron_nigropunctatus_East_Timor.jpg")

## 自衛機制

河魨的體型渾圓，主要依靠胸鰭推進。這樣的體型雖然可以靈活旋轉，速度卻不快，是個容易獵取的目標。因此，河魨演化出了迥異於一般魚類的自衛機制。河魨受到威脅時，能夠快速地將水或空氣吸入極具彈性的胃中，在短時間內膨脹成數倍大小，嚇退掠食者。棘魨科的[刺河魨身上甚至帶有刺](../Page/刺河魨.md "wikilink")，膨脹時全身的刺便會豎起，令掠食者難以吞食。

四齒魨科的河魨更含有[河魨毒素](../Page/河魨毒素.md "wikilink")，為一種劇毒，僅需極少量便能致人於死。箱魨科河魨亦含有毒性比[氰化物強烈](../Page/氰化物.md "wikilink")275倍的[箱魨毒素](../Page/箱魨毒素.md "wikilink")（Ostracitoxin）。[陳藏器](../Page/陳藏器.md "wikilink")《[本草拾遺](../Page/本草拾遺.md "wikilink")》稱：“入口爛舌，入腹爛腸，無藥可解。”河魨的劇毒大多分佈在內臟，然而隨著品種不同，毒性分佈的位置也不同，有些品種的河魨甚至連肌肉以及皮膚都有毒，完全無法食用。而河魨的毒性也會隨著季節而有強弱的變化，例如河魨到了繁殖期，毒性往往會變強。

也有些河魨不具毒性，例如棘魨科的河魨便為無毒。而少數的四齒魨，如黑鯖河魨（克氏兔頭魨）、白鯖河魨（懷氏兔頭魨）等，通常、或僅偶爾在內臟有微弱毒性。

## 河魨毒

[河魨毒素是一種劇毒](../Page/河魨毒素.md "wikilink")，毒性大約為[氰化物的](../Page/氰化物.md "wikilink")1200倍。对于老鼠实验，其[半數致死量濃度低至](../Page/半數致死量.md "wikilink")8µg每公斤體重。一隻河魨體內所含的毒素，估計足以殺死三十個成人。河魨毒並非獨見於河魨體內，[藍圈章魚](../Page/藍圈章魚.md "wikilink")、[芋螺](../Page/芋螺.md "wikilink")、以及某些種類的[蠑螈都含有河魨毒](../Page/蠑螈.md "wikilink")。河魨毒是由河魨體內共生的細菌所產生的，河魨於攝食的過程中獲得產毒所需的細菌。河魨本身由於[細胞膜上](../Page/細胞膜.md "wikilink")[鈉離子通道的結構與一般生物不同](../Page/鈉離子通道.md "wikilink")，因而對河魨毒免疫。少數魚類對河魨毒免疫，因而成為河魨的天敵，例如[虎鯊](../Page/虎鯊.md "wikilink")、[狗母等](../Page/狗母.md "wikilink")。

## 分類

[Canthigaster_valentini_1.jpg](https://zh.wikipedia.org/wiki/File:Canthigaster_valentini_1.jpg "fig:Canthigaster_valentini_1.jpg")

  - [二齒魨科](../Page/二齒魨科.md "wikilink")（棘魨科）*Diodontidae*
  - [三齒魨科](../Page/三齒魨科.md "wikilink") *Triodontidae*
  - [四齒魨科](../Page/四齒魨科.md "wikilink")（真河魨科）*Tetraodontidae*
      - [扁背魨屬](../Page/扁背魨屬.md "wikilink") *Canthigaster*
      - [寬吻魨屬](../Page/寬吻魨屬.md "wikilink") *Amblyrhynchotes*
      - [叉鼻魨屬](../Page/叉鼻魨屬.md "wikilink") *Arothron*
      - [凹鼻魨屬](../Page/凹鼻魨屬.md "wikilink") *Chelonodon*
      - [兔頭魨屬](../Page/兔頭魨屬.md "wikilink") *Lagocephalus*
      - [圓魨屬](../Page/圓魨屬.md "wikilink") *Sphoeroides*
      - [多紀魨屬](../Page/多紀魨屬.md "wikilink") *Takifugu*
          - [鉛點多紀魨](../Page/鉛點多紀魨.md "wikilink") *Takifugu alboplumbeus*
            (Richardson, 1845)
          - *Takifugu basilevskianus* (Basilewsky, 1855)
          - [雙斑東方純](../Page/雙斑東方純.md "wikilink") *Takifugu bimaculatus*
            (Richardson, 1845)
          - [中華多紀魨](../Page/中華多紀魨.md "wikilink") *Takifugu chinensis*
            (Abe, 1949)
          - *Takifugu chrysops* (Hilgendorf, 1879)
          - *Takifugu coronoidus* (Ni and Li, 1992)
          - *Takifugu exascurus* (Jordan and Snyder, 1901)
          - [菊黃多紀魨](../Page/菊黃多紀魨.md "wikilink") *Takifugu flavidus*
            (Li, Wang and Wang in Cheng et al., 1975)
          - [星點多紀魨](../Page/星點多紀魨.md "wikilink") *Takifugu niphobles*
            (Jordan and Snyder, 1901)
          - [橫紋多紀魨](../Page/橫紋多紀魨.md "wikilink") *Takifugu oblongus*
            (Bloch, 1786)
          - [暗紋多紀魨](../Page/暗紋多紀魨.md "wikilink") *Takifugu obscurus*
            (Abe, 1949)
          - *Takifugu ocellatus* (Linnaeus, 1758)
          - *Takifugu orbimaculatus* (Kuang, Li and Liang, 1984)
          - [豹紋多紀魨](../Page/豹紋多紀魨.md "wikilink") *Takifugu pardalis*
            (Temminck and Schlegel, 1850)
          - [網斑多紀魨](../Page/網斑多紀魨.md "wikilink") *Takifugu poecilonotus*
            (Temminck and Schlegel, 1850)
          - [紫色多紀魨](../Page/紫色多紀魨.md "wikilink") *Takifugu porphyreus*
            (Temminck and Schlegel, 1850)
          - [假睛多紀魨](../Page/假睛多紀魨.md "wikilink") *Takifugu pseudommus*
            (Chu, 1935)
          - *Takifugu radiatus* (Abe, 1947)
          - *Takifugu reticularis* (Tien, Cheng and Wang in Cheng et
            al., 1975)
          - [紅鰭多紀魨](../Page/紅鰭多紀魨.md "wikilink") *Takifugu rubripes*
            (Temminck and Schlegel, 1850)
          - *Takifugu snyderi* (Abe, 1988)
          - [密斑多紀魨](../Page/密斑多紀魨.md "wikilink") *Takifugu stictonotus*
            (Temminck and Schlegel, 1850)
          - [蟲紋多紀魨](../Page/蟲紋多紀魨.md "wikilink") *Takifugu vermicularis*
            (Temminck and Schlegel, 1850)
          - [黃鰭多紀魨](../Page/黃鰭多紀魨.md "wikilink") *Takifugu xanthopterus*
            (Temminck and Schlegel, 1850)

<!-- end list -->

  - [箱魨科](../Page/箱魨科.md "wikilink") *Ostraciontidae*

[File:Fugu.Tsukiji.CR.jpg|紅鰭多紀魨](File:Fugu.Tsukiji.CR.jpg%7C紅鰭多紀魨)
*Takifugu rubripes*

## 文化

  - 日本電影《[送行者：禮儀師的樂章](../Page/送行者：禮儀師的樂章.md "wikilink")》电影，有吃烤河魨“鱼白”的鏡頭。鱼白是指雄性河魨的精巢，亦名西施乳。

## 注釋

<div class="references-small">

<references/>

</div>

## 参考

  - 朱振藩：〈一種魚引發文人瘋狂——蘇軾拼死吃河豚〉，台灣《歷史月刊》259期

[T](../Category/魨形目.md "wikilink")
[Category:有毒魚類](../Category/有毒魚類.md "wikilink")

1.  [臺灣閩南語常用辭典：刺䲅](http://twblg.dict.edu.tw/holodict_new/index.html)，中華民國教育部

2.  康熙字典 䲅：《本草》䲅魚，一名鶘夷。以物觸之，卽塡腹如氣毬。亦曰嗔魚。白背有赤道，如印，魚目得合，與諸魚不同，卽今河魨也。《註》音規。

3.

4.
5.  王士祯在《渔阳诗话》卷中说：“坡诗‘蒌蒿满地芦芽短，正是河豚欲上时’，非但风韵之妙，盖河豚食蒿芦则肥，亦如梅圣俞之‘春洲生荻芽，春岸飞杨花’，无一字泛设也。”

6.  《河南邵氏聞見後錄》：經筵官會食資善堂，東坡盛稱河豚之美。呂元明問其味，曰：「直那一死！」再會，又稱豬肉之美。范淳甫曰：「奈發風何？」東坡笑呼曰：「淳甫誣告豬肉！」

7.  [张耒](../Page/张耒.md "wikilink")《明道杂志》云：“河豚，水族之奇味，世传以为有毒，能杀人。余守丹阳及宣城，见土人户食之，其烹煮亦无法，但用蒌蒿、荻芽、菘菜三物，而未尝见死者。”