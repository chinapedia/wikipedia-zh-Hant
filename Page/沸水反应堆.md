[Hamaoka_npp_2_mlit1975.jpg](https://zh.wikipedia.org/wiki/File:Hamaoka_npp_2_mlit1975.jpg "fig:Hamaoka_npp_2_mlit1975.jpg")沸水反應堆於1975年興建時的空照圖（[日本國土交通省國土航空攝影](../Page/國土交通省.md "wikilink")）\]\]
**沸水反應爐**（）是一種用來發電的[輕水反應爐](../Page/輕水反應爐.md "wikilink")。沸水反應爐是第二常見的核能發電反應爐型式，在五十年代中期由[愛達荷國家實驗室](../Page/愛達荷國家實驗室.md "wikilink")（）與[通用電氣公司共同研發成功](../Page/通用電氣公司.md "wikilink")。現在主要製造廠商是專門設計與建造這類反應爐的[GE日立核能](../Page/GE日立核能.md "wikilink")（）。

## 概述

\[\[<File:Boiling> water reactor english.svg|thumb|450px|沸水反應爐工作原理示意圖：

<div style="column-count:2">

1.  反應爐壓力槽
2.  核燃料棒
3.  控制棒
4.  循環泵
5.  控制棒電動機
6.  蒸汽
7.  飼水
8.  高壓渦輪機
9.  低壓渦輪機
10. 發電機
11. 激磁機
12. 冷凝器
13. 冷卻劑
14. 預熱器
15. 給水泵
16. 冷水泵
17. 混凝土圍阻體
18. 連接至電網

</div>

\]\]

沸水反應爐以[去离子水作为](../Page/去离子水.md "wikilink")[冷却劑](../Page/冷却劑.md "wikilink")（）和[中子減速劑](../Page/中子減速劑.md "wikilink")。反應爐爐心進行的[核分裂會產生熱能](../Page/核分裂.md "wikilink")，使得已冷卻的水沸騰，變為高壓蒸汽，從而驅動[渦輪機](../Page/渦輪機.md "wikilink")，然後通過[發電機轉換為](../Page/發電機.md "wikilink")[電能](../Page/電能.md "wikilink")。離開渦輪機的蒸汽，經過[冷凝器凝結為液態水](../Page/冷凝器.md "wikilink")（给水）後，回流至反應爐爐心，完成一個循環。在爐心裏，已冷卻的水保持在75个[大气壓](../Page/大气壓.md "wikilink")，這會促使它在285℃左右[沸腾](../Page/沸腾.md "wikilink")。

稍加比較，在壓水反應爐爐心內，由於維持高壓強（大約158個大氣壓），不會出現大量的沸騰。但沸水反应堆构造简单，且大大降低了反应堆的工作压力和堆芯温度，因此显著提高了反应堆的安全性，降低了造价。但由于沸水堆的循环系统直接连接了堆芯和渦轮机，因此可能造成渦轮机受到[放射性污染](../Page/放射性.md "wikilink")，给设计和维修带来麻烦。

## BWR構成要素

  - 燃料 : [低濃縮鈾](../Page/低濃縮鈾燃料.md "wikilink")
  - [冷却材](../Page/冷却材.md "wikilink")・[減速材](../Page/減速材.md "wikilink") :
    [輕水](../Page/輕水.md "wikilink")

## 沸水式反應爐的沿革

### 量產第一系列(BWR/1–BWR/6)

  - 第一代 BWR: BWR/1 搭配 Mark I 圍阻體
  - 第二代 BWR: BWR/2, BWR/3, 與部份 BWR/4，搭配 Mark I 圍阻體，其他 BWR/4, BWR/5，搭配
    Mark II 圍阻體
  - 第三代 BWR: BWR/6 搭配 Mark III 圍阻體

### 先進沸水反應爐 (ABWR)

*進步型沸水式反應爐*（ABWR），是一款符合[第三代反應器規範的](../Page/第三代反應器.md "wikilink")[沸水反應爐](../Page/沸水反應爐.md "wikilink")。目前由[奇異日立核能](../Page/奇異日立核能.md "wikilink")（GEH）和[東芝合作生產](../Page/東芝.md "wikilink")。

### 簡化沸水反應爐 (SBWR)

### 經濟簡化沸水反應爐 (ESBWR)

經濟簡化沸水反應爐 (Economic Simplified Boiling Water Reactor, ESBWR)
是第3+代的核能反應爐設計，始於90年代後期，GE工程師提出把簡化沸水反應爐特點的被動安全設計，與先進沸水反應爐設計結合，另加大功率到1600[MWe](../Page/MWe.md "wikilink")
(4500
[MWth](../Page/MWth.md "wikilink"))的方案。這個設計已送交[美國核能管理委員會審核](../Page/美國核能管理委員會.md "wikilink")，並已到最後設計複審階段。

## 参见

  - [压水反应堆](../Page/压水反应堆.md "wikilink")

## 參考文獻

  - 多田順一郎 『わかりやすい放射線物理学』オーム社 1997.12.20 ISBN 4-274-13123-8
  - 安斎育郎 『放射線と放射能』ナツメ社 2007.2.14 ISBN 978-4-8163-4255-4

## 外部链接

  - [香港天文台網站《現時用作商業運行的反應堆》](http://www.weather.gov.hk/education/dbcp/pow_stat/chi/r3_2.htm)
  - [中国科普博览——核能博物馆](http://www.kepu.net.cn/gb/technology/nuclear/station/200207310086.html)

[Category:核電反應堆類型](../Category/核電反應堆類型.md "wikilink")
[Category:輕水反應堆](../Category/輕水反應堆.md "wikilink")