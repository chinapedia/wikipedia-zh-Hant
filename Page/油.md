[Italian_olive_oil_2007.jpg](https://zh.wikipedia.org/wiki/File:Italian_olive_oil_2007.jpg "fig:Italian_olive_oil_2007.jpg")\]\]
**油**，是由一种或多种液态的[碳氢化合物组成的物质](../Page/碳氢化合物.md "wikilink")。由于油具有[疏水性的特性](../Page/疏水性.md "wikilink")，“油”亦是许多与[水不溶之](../Page/水.md "wikilink")[液体的总称](../Page/液体.md "wikilink")。而可以在油中溶解的物质都具有[亲油性](../Page/亲油性.md "wikilink")，一般不溶於水。

油和水可以在[乳液](../Page/乳液.md "wikilink")，比如[奶中短時間內比较均匀地混合](../Page/奶.md "wikilink")。但是乳液是亚稳定的状态，一段时间后又会重新分为油和水两个[相态](../Page/相态.md "wikilink")。
油的过量摄入会导致[脂肪增生](../Page/脂肪.md "wikilink")，可能導致心血管疾病的發生。

## 概述

油与[醇](../Page/醇.md "wikilink")、[酮和](../Page/酮.md "wikilink")[醚等碳氢化合物的区别在于](../Page/醚.md "wikilink")，油的组成部分不极化；与[脂肪的区别在于](../Page/脂肪.md "wikilink")，组成油的化合物的分子长度和[分子之间的连接比较小](../Page/分子.md "wikilink")。

## 油的种类

油亦是多种不同的物质的日常统称，因此它的分类并不系统，一般按其来源、用途等分类。

### [食用油](../Page/食用油.md "wikilink")

  - 植物油：

许多[植物油被用来食用](../Page/常見植物油一覽.md "wikilink")（比如[菜油](../Page/菜油.md "wikilink")、[向日葵油](../Page/向日葵油.md "wikilink")、[橄榄油等](../Page/橄榄油.md "wikilink")）。

  - 动物油：

[动物油中用作食用油的相对来说比较少](../Page/动物油.md "wikilink")（比如[鱼肝油](../Page/鱼肝油.md "wikilink")），动物油与脂肪的区分也不十分明确。此外动物油还被用在工业中（[润滑油](../Page/润滑油.md "wikilink")、[肥皂等](../Page/肥皂.md "wikilink")）。

### [精油](../Page/精油.md "wikilink")

精油是易挥发的、有芳香气味的植物油（比如[玫瑰油](../Page/玫瑰油.md "wikilink")），常用在[化妆品中](../Page/化妆品.md "wikilink")。

### [矿物油](../Page/石油.md "wikilink")

[Biodiesel.JPG](https://zh.wikipedia.org/wiki/File:Biodiesel.JPG "fig:Biodiesel.JPG")
矿物油是有机化学工业非常重要的原材料。矿物油提炼后也是目前世界上最重要的能量原材料。作为取暖用油、[汽油](../Page/汽油.md "wikilink")、[柴油和](../Page/柴油.md "wikilink")[重油的它是今日](../Page/重油.md "wikilink")[文明的保障](../Page/文明.md "wikilink")。此外矿物油產物还被用来製造[润滑油](../Page/润滑油.md "wikilink")。

矿物油主要是从地下开采出来的[石油](../Page/石油.md "wikilink")，其主要组成部分是[烷烃](../Page/烷烃.md "wikilink")。大自然中的[原油大多数含有杂物](../Page/原油.md "wikilink")，除少数例外外（比如[利比亚的原油](../Page/利比亚.md "wikilink")）都含有比较多的[硫和其它物质](../Page/硫.md "wikilink")。

### [硅油](../Page/矽氧樹脂.md "wikilink")

硅油是由硅和氧组成的半有机的高分子和非均相高分子化合物。它的优点是不易[氧化](../Page/氧化.md "wikilink")，不易受温度和其它因素影响。硅油除被用作润滑剂外，还在[化妆工业被大量使用](../Page/化妆.md "wikilink")。

## 用途

  - [润滑剂](../Page/润滑剂.md "wikilink")
  - [燃料](../Page/燃料.md "wikilink")
  - [液压油](../Page/液压油.md "wikilink")
  - [化工业原料](../Page/化工业原料.md "wikilink")
  - [热容](../Page/热容.md "wikilink")
  - [肥皂](../Page/肥皂.md "wikilink")、[生物柴油](../Page/生物柴油.md "wikilink")（[回鍋油回收再利用](../Page/回鍋油.md "wikilink")）

[\*](../Category/油.md "wikilink")