[Yau_Tong_Overview_201407.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Overview_201407.jpg "fig:Yau_Tong_Overview_201407.jpg")
[Yau_Tong_View_201301.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_View_201301.jpg "fig:Yau_Tong_View_201301.jpg")
[Yau_Tong_Old_Buildings_201106.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Old_Buildings_201106.jpg "fig:Yau_Tong_Old_Buildings_201106.jpg")一帶的船廠於2011年至2012年陸續清拆\]\]
[Yau_Tong_Dusk_View_201106.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Dusk_View_201106.jpg "fig:Yau_Tong_Dusk_View_201106.jpg")望向油塘\]\]
[Sam_Ka_Tsuen_Typhoon_Shelter_201106.jpg](https://zh.wikipedia.org/wiki/File:Sam_Ka_Tsuen_Typhoon_Shelter_201106.jpg "fig:Sam_Ka_Tsuen_Typhoon_Shelter_201106.jpg")\]\]
[Yau_Tong_Road_Playground_201608.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Road_Playground_201608.jpg "fig:Yau_Tong_Road_Playground_201608.jpg")
[Yau_Tong_Station_2014_04_part1.JPG](https://zh.wikipedia.org/wiki/File:Yau_Tong_Station_2014_04_part1.JPG "fig:Yau_Tong_Station_2014_04_part1.JPG")
**油塘**（）位於[香港](../Page/香港.md "wikilink")[九龍東南的地區](../Page/九龍.md "wikilink")，[鯉魚門以西北](../Page/鯉魚門.md "wikilink")，行政上屬於[觀塘區](../Page/觀塘區.md "wikilink")。現在油塘北部以[公屋和](../Page/公屋.md "wikilink")[居屋住宅為主](../Page/居屋.md "wikilink")，南部臨海位置現主要用作[工業發展的地區已經被政府規劃成私人住宅區](../Page/工業.md "wikilink")，但仍有零星的工業大廈存在。

油塘接鄰的[三家村](../Page/三家村_\(九龍\).md "wikilink")，已有150年歷史，油塘區內也設有[觀塘魚類批發市場](../Page/觀塘魚類批發市場.md "wikilink")。

## 歷史

1940年之前油塘未有發展，仍屬市郊地帶。50年代油塘以北的[茶果嶺興建了](../Page/茶果嶺.md "wikilink")[石油儲存庫](../Page/石油.md "wikilink")。60年代，油塘沿[鯉魚門道一帶被香港政府劃作公屋區](../Page/鯉魚門道.md "wikilink")，1964年興建了油塘邨與及[高超道邨等多個公共屋邨](../Page/高超道邨.md "wikilink")，沿海一帶設有一個[工業區](../Page/工業.md "wikilink")。

由於油塘較偏離[觀塘以至整個](../Page/觀塘.md "wikilink")[九龍的市區發展重心](../Page/九龍.md "wikilink")，油塘被不少人認為是一個偏遠地區。[地鐵](../Page/香港地鐵.md "wikilink")（現稱[港鐵](../Page/港鐵.md "wikilink")）[觀塘綫通車時](../Page/觀塘綫.md "wikilink")，擬於油塘設站的計劃一直未有落實，加深外人對油塘偏遠的印象，直到[東區海底隧道和](../Page/東區海底隧道.md "wikilink")[將軍澳綫通車](../Page/將軍澳綫.md "wikilink")，後者設有[油塘站](../Page/油塘站.md "wikilink")，大型[屋邨也陸續落成](../Page/屋邨.md "wikilink")，這個觀念才逐漸消失。

油塘公屋於1990年代陸續清拆重建，油塘人口一度下跌，加上[香港製造業息微](../Page/香港製造業.md "wikilink")，而政府1998年底將油塘工業區改劃為住宅用途及綜合發展區後，發展商積極改建工廈成住宅。\[1\]現時油塘主要是一個公屋和[居屋區](../Page/居屋.md "wikilink")，很多都是重建過來的，[油塘邨和](../Page/油塘邨.md "wikilink")[鯉魚門邨便是例子](../Page/鯉魚門邨.md "wikilink")。2012年9月，由房屋署擁有的最大型的購物商場[大本型開業](../Page/大本型.md "wikilink")，為區內增添大型消費場所。

## 地名起源

油塘有說舊稱**游塘**。[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")，[醉酒灣防綫就是起於](../Page/醉酒灣防綫.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[葵涌](../Page/葵涌.md "wikilink")，迄於游塘。\[2\]

油塘的英文地名Yau
Tong，最早見於1924年香港政府軍部的[魔鬼山地圖上](../Page/魔鬼山.md "wikilink")，有一字，見於海灣之上，其北有[茶果嶺村](../Page/茶果嶺.md "wikilink")。可見油塘地名出於更早之時。當時當未有任何發展。1935年，軍部全港地圖上並無改變。

根據《1963年[香港年報](../Page/香港年報.md "wikilink")》，同一書內兩幅地圖中，一者書，一者書。寫在填海圖海灣上，而則用於分區界上。不過書中內文只有提及，說當地只作修船造船用途，因其近岸兼有地儲水，政府賣地予木廠之用。這些都是當時政規劃特例。

至於出處，仍未有定論。

有一說即「[馬游塘](../Page/馬游塘.md "wikilink")」，得名於藍田山上的馬游塘村，不過兩者相距甚遠。馬游塘於1963年時，劃分為觀塘區，而非油塘區。

另有一說，1947年[亞細亞石油公司購入近茶果嶺二十多萬方呎地興建油庫](../Page/亞細亞石油公司.md "wikilink")\[3\]；1954年香港政府更批出現時[麗港城的位置給](../Page/麗港城.md "wikilink")[亞細亞石油公司擴建油庫](../Page/亞細亞石油公司.md "wikilink")，因此改名為「油塘」，而油庫以北的地區便是今日的[觀塘商貿區](../Page/觀塘商貿區.md "wikilink")。

1960年代末，政府擬建[地下鐵路](../Page/香港地鐵.md "wikilink")，曾建議於現時[油塘邨一帶設](../Page/油塘邨.md "wikilink")「」，但最後該站因擬建系統規模縮減而除去。可見寫該計劃的顧問公司，跟隨政府部份資料而命名為。根據如資深[巴士迷](../Page/巴士迷.md "wikilink")巴士書籍的舊照片及電影片段顯示，九巴公司於1960年代前往油塘地區的巴士路線，起訖點一律稱為「游塘」。然而2002年地鐵（現稱[港鐵](../Page/港鐵.md "wikilink")）[將軍澳綫啟用](../Page/將軍澳綫.md "wikilink")，於油塘設站，當中站名則使用「[油塘站](../Page/油塘站.md "wikilink")」。

然而1968年政府九龍半島全圖，近[三家村的海灣已命名為](../Page/三家村_\(九龍\).md "wikilink")[觀塘仔灣](../Page/觀塘仔灣.md "wikilink")，而該區更見發展，建有。由此可知，1920年代，政府使用油塘一名並無間斷，並曾以其名作區名。

## 未來發展

### 油塘灣綜合發展區

「綜合發展區」的長遠規劃目標，是逐步取締現有工業作業，解決環境問題及優化海濱供公眾享用。\[4\]油塘灣現有的工廠大廈已於2011年起陸續清拆，騰出空間後由[恒地興建遊艇中心](../Page/恒地.md "wikilink")、海濱長廊、多棟中密度住宅和酒店，當中住宅高度為18至29層，非住宅高度為6及20層，提供6,556個中小型住宅單位。建築面積約9.94公頃，總地積比率最高定於4.5倍，發展商須採用獨特的漸進式建築物高度輪廓，建築物高度向海濱遞降，屆時該區將成為東九龍的高尚住宅區。\[5\]\[6\]部份住宅單位景觀遠眺[中環及](../Page/中環.md "wikilink")[尖沙咀一帶海域](../Page/尖沙咀.md "wikilink")，每晚皆可看到[幻彩詠香江的雷射表演](../Page/幻彩詠香江.md "wikilink")，每逢[農曆新年和](../Page/農曆新年.md "wikilink")[回歸紀念更可觀看](../Page/香港特別行政區成立紀念日.md "wikilink")[煙花](../Page/煙花.md "wikilink")。

## 住宅

### 私人屋苑

  - [鯉灣天下](../Page/鯉灣天下.md "wikilink")
  - [嘉賢居](../Page/嘉賢居.md "wikilink")
  - [Ocean One](../Page/Ocean_One.md "wikilink")
  - [Peninsula East](../Page/Peninsula_East.md "wikilink")-2幢（1座及2座）
  - [海傲灣](../Page/海傲灣.md "wikilink")-3幢（1A,1B及2座）
  - 曦臺(Maya)-2幢（1座及2座）【[宏安地產及](../Page/宏安地產.md "wikilink")[旭輝四山街](../Page/旭輝.md "wikilink")13至15號項目。兩幢共326伙，面積由470至1,800平方呎不等，於2019年3月預售樓花】

賣地/已完成補地價：

  - [五礦地產旗下崇信街與仁宇圍交界](../Page/五礦地產.md "wikilink")(2016年中標政府土地)，4幢大廈共提供792伙，當中7成為中型單位，逾3,000平方呎留作零售樓面，最快2019年底推樓花。

<!-- end list -->

  - [信和置業及](../Page/信和置業.md "wikilink")[資本策略旗下](../Page/資本策略.md "wikilink")，港鐵油塘高超道及鯉魚門道交界油塘通風樓，佔地約4.3萬平方呎，可建住宅樓面約32.5萬平方呎，提供約500伙

<!-- end list -->

  - [保利置業及](../Page/保利置業.md "wikilink")[尚嘉控股](../Page/梁安琪_\(澳門\).md "wikilink")，地皮面積約38,611方呎，指定作非工業（不包括倉庫、酒店及加油站）用途，最高可建樓面356,504方呎。

有待補地價：

  - [長實旗下東源街](../Page/長實.md "wikilink")5號及8號地盤項目(2016年已完成改劃)，共建5幢大廈提供536伙（2019年3月完成8號地盤補地價）
  - [越秀地產旗下油塘東源街越秀冷藏倉庫及鄰近用地](../Page/越秀地產.md "wikilink")(2016年底向城規會申請)，共建5幢大廈提供1056伙，單位平均面積料約600方呎
  - 青建國際（新加坡財團）旗下東源街18號，興建2幢21層高住宅，提供224伙住宅單位，平均每戶約1000呎(2019年向城規會申請)
  - [恒基地產等的油塘灣大型綜合發展項目第](../Page/恒基地產.md "wikilink")1、2期，獲屋宇署批建樓面超過408.8萬方呎，將興建30幢分層物業，約6200伙。

### 居者有其屋

  - [油塘中心](../Page/油塘中心.md "wikilink")（私人參建居屋）
  - [高俊苑](../Page/高俊苑.md "wikilink")
  - [油翠苑](../Page/油翠苑.md "wikilink")

### 公共屋邨

  - [油塘邨](../Page/油塘邨.md "wikilink")
  - [高怡邨](../Page/高怡邨.md "wikilink")
  - [高翔苑](../Page/高翔苑.md "wikilink")（僅[新十字型大廈](../Page/新十字型大廈.md "wikilink")）
  - [油麗邨](../Page/油麗邨.md "wikilink")
  - [鯉魚門邨](../Page/鯉魚門邨.md "wikilink")

### 紀律部隊宿舍

  - [油美苑](../Page/油美苑.md "wikilink")
  - [高翔苑](../Page/高翔苑.md "wikilink")（僅[康和型大廈](../Page/康和型大廈.md "wikilink")）

## 商場

  - [鯉魚門廣場](../Page/鯉魚門廣場.md "wikilink")
  - [大本型](../Page/大本型.md "wikilink")
  - [油麗商場](../Page/油麗商場.md "wikilink")
  - 嘉榮商場 (設於[油塘中心第](../Page/油塘中心.md "wikilink")1座)
  - 嘉華商場 (設於[油塘中心第](../Page/油塘中心.md "wikilink")2、3座)
  - 嘉富商場 (設於[油塘中心第](../Page/油塘中心.md "wikilink")4座)
  - 嘉貴商場 (設於[油塘中心第](../Page/油塘中心.md "wikilink")5、6座)
  - 嘉發商場 (設於[油塘中心第](../Page/油塘中心.md "wikilink")7、8、9座)

## 中、小學、特殊學校

  - [天主教普照中學](../Page/天主教普照中學.md "wikilink") : 普照路
  - [聖安當女書院](../Page/聖安當女書院.md "wikilink") : 高超道
  - [佛教何南金中學](../Page/佛教何南金中學.md "wikilink") : 高超徑
  - [香港道教聯合會圓玄學院陳呂重德紀念學校](../Page/香港道教聯合會圓玄學院陳呂重德紀念學校.md "wikilink") :
    高超徑
  - [聖安堂小學](../Page/聖安堂小學.md "wikilink") : 油塘道
  - [福建中學附屬學校](../Page/福建中學附屬學校.md "wikilink") :
    [油塘邨第二期](../Page/油塘邨.md "wikilink")
  - [聖公會油塘基顯小學](../Page/聖公會油塘基顯小學.md "wikilink") : 油塘道
  - [中華基督教會基法小學(油塘)](../Page/中華基督教會基法小學\(油塘\).md "wikilink") : 油塘道
  - [基督教中國佈道會聖道學校](../Page/基督教中國佈道會聖道學校.md "wikilink") : 鯉魚門道

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>、<font color="{{將軍澳綫色彩}}">█</font><a href="../Page/將軍澳綫.md" title="wikilink">將軍澳綫</a>：<a href="../Page/油塘站.md" title="wikilink">油塘站</a></li>
</ul>
<dl>
<dt><a href="../Page/油塘公共運輸交匯處.md" title="wikilink">油塘公共運輸交匯處</a></dt>

</dl>
<dl>
<dt>跨境巴士</dt>

</dl>
<ul>
<li>東九龍快綫：油塘至<a href="../Page/深圳灣口岸.md" title="wikilink">深圳灣口岸</a></li>
</ul>
<dl>
<dt><a href="../Page/鯉魚門邨公共運輸交匯處.md" title="wikilink">鯉魚門邨公共運輸交匯處</a></dt>

</dl>
<dl>
<dt><a href="../Page/鯉魚門道.md" title="wikilink">鯉魚門道</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小型巴士.md" title="wikilink">公共小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至油塘</a>/鯉魚門線[7]</li>
<li><a href="../Page/佐敦道.md" title="wikilink">佐敦道</a>/<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣至油塘線</a>[8]</li>
<li><a href="../Page/青山道.md" title="wikilink">青山道至油塘線</a>[9]</li>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至油塘線</a> (上下午繁時服務)[10]</li>
<li><a href="../Page/旺角.md" title="wikilink">旺角至油塘線</a>[11]</li>
<li><a href="../Page/灣仔.md" title="wikilink">灣仔至油塘線</a> (通宵線)[12]</li>
</ul>
<dl>
<dt><a href="../Page/欣榮街.md" title="wikilink">欣榮街</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小型巴士.md" title="wikilink">公共小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至油塘</a>/鯉魚門線</li>
<li><a href="../Page/佐敦道.md" title="wikilink">佐敦道</a>/<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣至油塘線</a></li>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至油塘線</a> (上下午繁時服務)</li>
<li><a href="../Page/旺角.md" title="wikilink">旺角至油塘線</a></li>
</ul>
<dl>
<dt><a href="../Page/高超道.md" title="wikilink">高超道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至油塘</a>/鯉魚門線</li>
<li><a href="../Page/佐敦道.md" title="wikilink">佐敦道</a>/<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣至油塘線</a></li>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至油塘線</a> (上下午繁時服務)</li>
<li><a href="../Page/青山道.md" title="wikilink">青山道至油塘線</a></li>
<li><a href="../Page/旺角.md" title="wikilink">旺角至油塘線</a></li>
<li><a href="../Page/灣仔.md" title="wikilink">灣仔至油塘線</a> (通宵線)</li>
</ul>
<dl>
<dt><a href="../Page/茶果嶺道.md" title="wikilink">茶果嶺道</a></dt>

</dl>
<dl>
<dt><a href="../Page/油塘道.md" title="wikilink">油塘道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>油麗邨至觀塘線[13]</li>
</ul>
<dl>
<dt>鄰近<a href="../Page/鯉魚門.md" title="wikilink">鯉魚門</a></dt>

</dl>
<ul>
<li>三家村碼頭</li>
</ul>
<dl>
<dt>步行</dt>

</dl>
<ul>
<li>油麗邨至東區海底隧道升降機塔</li>
</ul>
<h3 id="主要交通幹道">主要交通幹道</h3>
<ul>
<li><a href="../Page/鯉魚門道.md" title="wikilink">鯉魚門道</a></li>
<li><a href="../Page/茶果嶺道.md" title="wikilink">茶果嶺道</a></li>
<li><a href="../Page/東區海底隧道.md" title="wikilink">東區海底隧道</a></li>
<li><a href="../Page/高超道.md" title="wikilink">高超道</a></li>
<li>經<a href="../Page/東南九龍T2主幹道.md" title="wikilink">東南九龍T2主幹道</a>（計劃中）連接西九龍</li>
</ul></td>
</tr>
</tbody>
</table>

## 區議會議席分佈

為方便比較，以下列表以[茶果嶺村](../Page/茶果嶺.md "wikilink")、[鯉魚門道](../Page/鯉魚門道.md "wikilink")、[啟田道交匯處以南](../Page/啟田道.md "wikilink")、[五桂山](../Page/五桂山.md "wikilink")、[碧雲道至](../Page/碧雲道.md "wikilink")[澳景路交界以南為範圍](../Page/澳景路.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/茶果嶺.md" title="wikilink">茶果嶺村以南</a>、<a href="../Page/鯉魚門道.md" title="wikilink">鯉魚門道以西</a></p></td>
<td><p><a href="../Page/油塘東.md" title="wikilink">油塘四山選區</a></p></td>
<td><p><a href="../Page/油塘四山西.md" title="wikilink">油塘四山西選區</a></p></td>
<td><p><a href="../Page/油塘中.md" title="wikilink">油塘中選區及</a><a href="../Page/油塘西.md" title="wikilink">油塘西選區</a></p></td>
<td><p><a href="../Page/油麗.md" title="wikilink">油麗選區</a>、<a href="../Page/翠翔.md" title="wikilink">翠翔選區及</a><a href="../Page/油塘西.md" title="wikilink">油塘西選區</a></p></td>
<td><p><a href="../Page/翠翔.md" title="wikilink">油翠選區</a>、<a href="../Page/油麗.md" title="wikilink">油麗選區及</a><a href="../Page/油塘西.md" title="wikilink">油塘西選區</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鯉魚門道.md" title="wikilink">鯉魚門道以東</a></p></td>
<td><p><a href="../Page/油塘四山東.md" title="wikilink">油塘四山東選區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 鄰近地區

  - [鯉魚門](../Page/鯉魚門.md "wikilink")
  - [調景嶺](../Page/調景嶺.md "wikilink")

## 註釋

## 外部連結

  - [中原地圖的油塘](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=842348&cy=817743&zm=6&mx=842348&my=817743&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

{{-}}

{{-}}

[油塘](../Category/油塘.md "wikilink")
[Category:觀塘區](../Category/觀塘區.md "wikilink")
[Category:新九龍](../Category/新九龍.md "wikilink")

1.  [油塘臨海地 可打造精品豪宅
    《香港經濟日報》 2015年3月6日](https://hk.finance.yahoo.com/news/%E6%B2%B9%E5%A1%98%E8%87%A8%E6%B5%B7%E5%9C%B0-%E5%8F%AF%E6%89%93%E9%80%A0%E7%B2%BE%E5%93%81%E8%B1%AA%E5%AE%85-225529895--sector.html)
2.
3.  〈[香港掌故\[造地編](http://hk.epochtimes.com/7/9/14/51650.htm){三之三}\]〉，《大紀元時報》，2007年9月14日
4.  [城市規劃委員會
    都會規劃小組委員會會議記錄](http://www.info.gov.hk/tpb/sc/meetings/MPC/Minutes/m369mpc_c.pdf)
5.  [油塘灣擬減建住宅](http://orientaldaily.on.cc/cnt/finance/20110216/00204_004.html)《東方日報》2011年02月16日
6.  [油塘綜合項目規劃十度延期](http://www.singtao.com/yesterday/pro/0807ho08.html)《星島日報》2012-08-07
7.  [觀塘協和街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k14.html)
8.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
9.  [青山道香港紗廠　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
10. [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
11. [旺角先達廣場　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k40.html)
12. [灣仔　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_kh63.html)
13. [油麗邨　—　觀塘物華街](http://www.16seats.net/chi/rmb/r_k21.html)