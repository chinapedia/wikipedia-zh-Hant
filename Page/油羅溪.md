**油羅溪**，位於[台灣北部](../Page/台灣.md "wikilink")，為[頭前溪的支流](../Page/頭前溪.md "wikilink")，河長26公里，流域面積178平方公里\[1\]，分佈於[新竹縣](../Page/新竹縣.md "wikilink")[芎林鄉南端](../Page/芎林鄉.md "wikilink")、[橫山鄉中部及](../Page/橫山鄉_\(台灣\).md "wikilink")[尖石鄉北部](../Page/尖石鄉.md "wikilink")。主流發源於標高1,914公尺之[李棟山](../Page/李棟山.md "wikilink")\[2\]西側，先向西北流經八五山、樹林後，轉向西南流經嘉樂、麥樹仁、[尖石](../Page/尖石鄉.md "wikilink")，再轉向西北西流經[內灣](../Page/內灣村.md "wikilink")、油羅，於下公館附近與[上坪溪會合](../Page/上坪溪.md "wikilink")，改稱為頭前溪。

## 水系主要河川

  - **油羅溪**：[新竹縣](../Page/新竹縣.md "wikilink")[芎林鄉](../Page/芎林鄉.md "wikilink")、[橫山鄉](../Page/橫山鄉_\(台灣\).md "wikilink")、[尖石鄉](../Page/尖石鄉.md "wikilink")
      - [粗坑溪](../Page/粗坑溪.md "wikilink")：新竹縣橫山鄉
      - [馬胎溪](../Page/馬胎溪.md "wikilink")：新竹縣尖石鄉
      - [那羅溪](../Page/那羅溪.md "wikilink")：新竹縣尖石鄉
          - [錦屏溪](../Page/錦屏溪.md "wikilink")：新竹縣尖石鄉
      - [水田溪](../Page/水田溪.md "wikilink")：新竹縣尖石鄉

## 主要橋樑

以下由[河口至](../Page/河口.md "wikilink")[源頭列出](../Page/源頭.md "wikilink")[主流上之主要](../Page/主流.md "wikilink")[橋樑](../Page/橋樑.md "wikilink")：

  - [內灣吊橋](../Page/內灣吊橋.md "wikilink")：新竹縣橫山鄉內灣村
  - [內灣大橋](../Page/內灣大橋.md "wikilink")：[新竹縣](../Page/新竹縣.md "wikilink")[橫山鄉](../Page/橫山鄉_\(台灣\).md "wikilink")[內灣村](../Page/內灣村.md "wikilink")

## 相關條目

  - [頭前溪](../Page/頭前溪.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")

## 參考資料及註釋

<div class="references-small">

<references />

</div>

[Category:新竹縣河川](../Category/新竹縣河川.md "wikilink")
[Category:頭前溪水系](../Category/頭前溪水系.md "wikilink")

1.  [經濟部水利署水利櫥窗：治理計畫--
    頭前溪](http://www.wra.gov.tw/ct.asp?xItem=12800&ctNode=2384&comefrom=lp)


2.