**治外法权**是免除本地[法律](../Page/法律.md "wikilink")[司法权的情形](../Page/司法.md "wikilink")，通常是[外交谈判的结果](../Page/外交.md "wikilink")。例如，若一个甲国[公民当在乙国访问时享受治外法权](../Page/公民.md "wikilink")，那麼这个人在涉嫌犯罪时，乙国的法院不能进行审判。

## 传统惯例的治外法权

治外法权通常是互相给与的，主要包括：

  - [外交豁免权](../Page/外交豁免权.md "wikilink")
  - [国家元首的官式访问](../Page/国家元首.md "wikilink")
  - 停靠在本国港口的外国籍公共船只
  - 停在本国机场的外国籍飞机

## 强压的治外法权

治外法权过去常授予[外交人员以外的外国](../Page/外交人员.md "wikilink")[侨民](../Page/侨民.md "wikilink")。19世纪，[西方](../Page/西方世界.md "wikilink")[列强曾经在](../Page/列强.md "wikilink")[中国](../Page/中国.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[日本](../Page/日本.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[泰国](../Page/泰国.md "wikilink")，以这些司法制度不健全，或者存在酷刑（凌迟，五马分尸）的国家没有能力作出人道判决或者判决过于主观为理由，采取方法保护侨民单方面的治外法权。以[清朝為例](../Page/清朝.md "wikilink")，西方人很難接受中國舊式的監獄管理（[衛生與](../Page/衛生.md "wikilink")[拷打都是一大問題](../Page/拷打.md "wikilink")）以及[凌遲等野蛮残酷的](../Page/凌遲.md "wikilink")[酷刑](../Page/酷刑.md "wikilink")，除此之外，没有独立法庭与辩护人的存在使得判决不够客观。因此西方的[领事被授予处理所有与本国公民有关的](../Page/领事.md "wikilink")[民事和](../Page/民事.md "wikilink")[刑事案件的权力](../Page/刑事.md "wikilink")，即[领事裁判权](../Page/领事裁判权.md "wikilink")。

### 在中国的治外法权

在中国，1917年，[德国和](../Page/德国.md "wikilink")[奥匈帝国因](../Page/奥匈帝国.md "wikilink")[第一次世界大战中成为中国的敌对国](../Page/第一次世界大战.md "wikilink")，而被废除不平等条约。中華民國在這幾十年間致力於國際交涉，最終造成1924年[苏联放弃其在中国的特权](../Page/苏联.md "wikilink")。1943年1月到1944年4月,先后有美国、英国、比利时、卢森堡、挪威、加拿大等6个国家与中国签订条约，取消在华治外法权及有关特权。1945年4—5月瑞典、荷兰与中国签订放弃在华治外法权及解决有关事件的条约。[意大利和](../Page/意大利.md "wikilink")[日本因](../Page/日本.md "wikilink")[第二次世界大战中成为中国的敌对国失去他们的治外法权地位](../Page/第二次世界大战.md "wikilink")。1946年2月28日签署互换了《关于法国放弃在华治外法权及其有关特权条约》（中法平等新约），[法国也放弃其在中国的特权](../Page/法国.md "wikilink")，至此列强在中国的治外法权完全終結。

## 军事基地相关的治外法权

[美軍在一些海外国家设立军事基地](../Page/美軍.md "wikilink")，军队的成员和文职人员及其家属在军事基地外享受治外法权，如[冲绳的](../Page/沖繩縣.md "wikilink")[駐日美軍](../Page/駐日美軍.md "wikilink")。

## 現存的治外法權

  - [外交豁免權](../Page/外交豁免權.md "wikilink")
  - [國家元首的](../Page/國家元首.md "wikilink")[國事訪問](../Page/國事訪問.md "wikilink")
  - [聖座在梵蒂岡以外的](../Page/聖座.md "wikilink")[房地產](../Page/聖座房地產.md "wikilink")，例如位於[義大利](../Page/義大利.md "wikilink")[岡多菲堡的](../Page/岡多菲堡.md "wikilink")[岡道爾夫堡教宗別墅](../Page/岡道爾夫堡教宗別墅.md "wikilink")
  - 義大利[罗马市內的](../Page/罗马.md "wikilink")[馬爾他騎士團總部](../Page/馬爾他騎士團.md "wikilink")
  - [聯合國部分辦公處所](../Page/聯合國.md "wikilink")，例如位於美國[纽约州](../Page/纽约州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[曼哈頓區的](../Page/曼哈頓區.md "wikilink")[總部大樓](../Page/联合国总部大楼.md "wikilink")
  - 法國[塞夫尔的](../Page/塞夫尔.md "wikilink")[國際度量衡局](../Page/國際度量衡局.md "wikilink")
  - 比利時[布鲁塞尔的](../Page/布鲁塞尔.md "wikilink")[北約總部和](../Page/北大西洋公约组织.md "wikilink")[蒙斯的](../Page/蒙斯.md "wikilink")[歐洲盟軍最高司令部](../Page/歐洲盟軍最高司令部.md "wikilink")[盟軍作戰指揮部總部](../Page/盟軍作戰指揮部.md "wikilink")
  - [瑞士](../Page/瑞士.md "wikilink")[日內瓦州的](../Page/日內瓦州.md "wikilink")[歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")
  - 法國[上普羅旺斯阿爾卑斯省](../Page/上普羅旺斯阿爾卑斯省.md "wikilink")[魯穆萊的](../Page/魯穆萊_\(上普羅旺斯阿爾卑斯省\).md "wikilink")[Roumoules無線電發射器](../Page/Roumoules無線電發射器.md "wikilink")
  - 義大利[聖瑪利亞·加勒里亞的](../Page/聖瑪利亞·加勒里亞.md "wikilink")[梵蒂岡廣播電台廣播中心](../Page/梵蒂岡廣播電台.md "wikilink")
  - 阿根廷内乌肯省巴塔哥尼亚的中国航天测控站深空站

## 参考文献

## 参见

  - [领事裁判权](../Page/领事裁判权.md "wikilink")
  - [不平等条约](../Page/不平等条约.md "wikilink")
  - [租界](../Page/租界.md "wikilink")
  - [外國人](../Page/外國人.md "wikilink")

{{-}}

[Category:外交](../Category/外交.md "wikilink")
[Category:法律术语](../Category/法律术语.md "wikilink")
[Category:国际法](../Category/国际法.md "wikilink")