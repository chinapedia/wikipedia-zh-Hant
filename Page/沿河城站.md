**沿河城站**（**Yanhecheng Railway
Station**）是[丰沙铁路上的一个小火车站](../Page/丰沙铁路.md "wikilink")。该站位于[北京市](../Page/北京市.md "wikilink")[门头沟区](../Page/门头沟区.md "wikilink")[斋堂镇沿河城村境内](../Page/斋堂镇.md "wikilink")，距离[北京站](../Page/北京站.md "wikilink")79[公里](../Page/公里.md "wikilink")，[沙城站](../Page/沙城站.md "wikilink")42公里。建设于1952年，为上行车站。

## 参见

  - [丰沙铁路车站列表](../Page/丰沙铁路车站列表.md "wikilink")

## 邻近车站

## 参考资料

[Category:门头沟区铁路车站](../Category/门头沟区铁路车站.md "wikilink")
[Category:1952年启用的铁路车站](../Category/1952年启用的铁路车站.md "wikilink")