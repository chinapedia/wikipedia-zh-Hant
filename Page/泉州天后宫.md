**泉州天后宫**位于[福建](../Page/福建.md "wikilink")[泉州市](../Page/泉州市.md "wikilink")[鲤城区南门天后路](../Page/鲤城区.md "wikilink")，主祀[妈祖女神](../Page/妈祖.md "wikilink")。

## 历史

泉州天后宫始建于[宋](../Page/宋朝.md "wikilink")[慶元二年](../Page/慶元_\(宋寧宗\).md "wikilink")（1196年），初名“**顺济庙**”。[明](../Page/明.md "wikilink")[永乐五年](../Page/永乐_\(明成祖\).md "wikilink")（1407年），[郑和奏请重修顺济庙](../Page/郑和.md "wikilink")，永乐十三年（1415年）再次重修，并更名“**天妃宫**”。[清](../Page/清.md "wikilink")[康熙二十四年](../Page/康熙.md "wikilink")（1685年），感念妈祖有助清兵攻取[台湾](../Page/台湾.md "wikilink")，更庙名为“**天后宫**”。

## 建筑

泉州天后宫坐北朝南，占地1.8亩，是典型的[闽南建筑风格](../Page/闽南建筑.md "wikilink")。中轴线上为照壁、山门、正殿、寝殿、梳妆楼等主体建筑，中轴两侧分布着东西阙（钟鼓楼）、东西廊、东西轩、凉亭、斋馆等建筑。其中寝殿、东廊和凉亭为明代建筑，正殿、石刻为清代物件，山门于1990年重修。

正殿面阔五间24.6米，进深五间23米，为重檐歇山顶，屋脊装饰华丽。寝殿面阔七间35.1米，进深五间19.8米，单檐悬山顶。

## 地位与价值

泉州天后宫是海内外建筑规格最高，年代最早的[妈祖庙之一](../Page/妈祖庙.md "wikilink")，[台湾和](../Page/台湾.md "wikilink")[东南亚的许多妈祖庙都是从这里分灵的](../Page/东南亚.md "wikilink")，有**温陵天后祖庙**之称。它也是中国大陆妈祖庙中第一座[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

## 接驳交通

公交站

  - 大隘门站
    [27路](../Page/泉州公交27路.md "wikilink")、[31路](../Page/泉州公交31路.md "wikilink")

## 相關條目

  - [媽祖](../Page/媽祖.md "wikilink")
  - [台灣媽祖信仰](../Page/台灣媽祖信仰.md "wikilink")
  - [台灣民間信仰](../Page/台灣民間信仰.md "wikilink")

## 参考资料

  -
[Category:泉州庙宇](../Category/泉州庙宇.md "wikilink")
[Category:泉州文物保护单位](../Category/泉州文物保护单位.md "wikilink")
[Category:福建妈祖庙](../Category/福建妈祖庙.md "wikilink")
[Category:宋朝建筑](../Category/宋朝建筑.md "wikilink")
[Category:福建全国重点文物保护单位](../Category/福建全国重点文物保护单位.md "wikilink")