**淵男产**（），[唐](../Page/唐朝.md "wikilink")[高句丽人](../Page/高句丽.md "wikilink")，[淵蓋蘇文子](../Page/淵蓋蘇文.md "wikilink")。[乾封元年](../Page/乾封.md "wikilink")，与其二兄[淵男建逐长兄](../Page/淵男建.md "wikilink")[淵男生](../Page/淵男生.md "wikilink")，拥男建为[莫离支](../Page/莫离支.md "wikilink")。

## 生平

淵男产生于[唐太宗](../Page/唐太宗.md "wikilink")[贞观十三年](../Page/貞觀_\(唐朝\).md "wikilink")（639年），家族世为[高句丽](../Page/高句丽.md "wikilink")[莫离支](../Page/莫离支.md "wikilink")，早年任[中里小兄](../Page/中里小兄.md "wikilink")，18岁授[中里大兄](../Page/中里大兄.md "wikilink")，21岁升中里大活，23岁迁[中里位头大兄](../Page/中里位头大兄.md "wikilink")，累迁为中军主活。协助二兄[淵男建逐走长兄](../Page/淵男建.md "wikilink")[淵男生](../Page/淵男生.md "wikilink")，拥男建为[莫离支](../Page/莫离支.md "wikilink")。30岁时为太大莫离支（[太大兄](../Page/太大兄.md "wikilink")）。

唐[乾封元年](../Page/乾封.md "wikilink")（666年）11月，[唐高宗任命司空](../Page/唐高宗.md "wikilink")、英国公[李世勣为辽东道行军大总管](../Page/李世勣.md "wikilink")，率裨将[郭待封等征讨高句丽](../Page/郭待封.md "wikilink")。乾封二年(667年)2月，李世勣渡过[辽河进军至新城下](../Page/辽河.md "wikilink")，随后攻下新城，淵男產持白幡率首领九十八人出降。

[总章元年](../Page/总章.md "wikilink")（668年）9月，李世勣移师于[平壤城南](../Page/平壤.md "wikilink")，在高句丽下捉兵总管僧信诚的内应下，11月破城而入，[淵男建自杀](../Page/淵男建.md "wikilink")，但未死，与[宝藏王一起被俘](../Page/宝藏王.md "wikilink")。12月，高句丽君臣被押回京师献俘于[含元宫](../Page/含元宫.md "wikilink")，宝藏王被授予司平太常伯；[淵男產因先降](../Page/淵男產.md "wikilink")，授司宰少卿（从四品上），仍加[金紫光禄大夫](../Page/金紫光禄大夫.md "wikilink")[员外置同正员](../Page/员外置同正员.md "wikilink")（正三品）；[淵男建顽抗唐军](../Page/淵男建.md "wikilink")，被处死刑。但在淵男生求情下，免死配流[黔州](../Page/黔州.md "wikilink")（在今[重庆](../Page/重庆.md "wikilink")[彭水县](../Page/彭水县.md "wikilink")）；淵男生归附唐朝则授右卫大将军，封为卞国公。

696年或697年，泉男產封[辽阳郡](../Page/辽阳郡.md "wikilink")[开国公](../Page/开国公.md "wikilink")，又迁[营缮监大匠员外置同正员](../Page/营缮监大匠.md "wikilink")。[圣历二年](../Page/圣历.md "wikilink")（699年），授[上护军](../Page/上护军.md "wikilink")（勋官，视正三品）。

[大足元年三月廿七日](../Page/大足_\(年号\).md "wikilink")（701年5月9日），卒于府第，年六十三，以其年四月廿三日，葬于[洛阳县](../Page/洛阳县.md "wikilink")[平阴](../Page/平阴.md "wikilink")[乡某所](../Page/乡.md "wikilink")。\[1\]

## 家族

  - 曾祖父：渊子游，[莫离支](../Page/莫离支.md "wikilink")
  - 祖父：渊太祚，[莫离支](../Page/莫离支.md "wikilink")
  - 父亲：[淵蓋蘇文](../Page/淵蓋蘇文.md "wikilink")(渊盖金)，[莫离支](../Page/莫离支.md "wikilink")、太[大对卢](../Page/大对卢.md "wikilink")
  - 叔父：渊净土
  - 长兄：[淵男生](../Page/淵男生.md "wikilink")
  - 次兄：[淵男建](../Page/淵男建.md "wikilink")
      - 侄子：[泉献诚](../Page/泉献诚.md "wikilink")，袭卞国公，谥曰庄。
      - 侄子：[泉献忠](../Page/泉献忠.md "wikilink")，被淵男建所杀。

## 参见

  - [唐与高句丽的战争](../Page/唐与高句丽的战争.md "wikilink")

## 参考

<references/>

[Y](../Category/唐朝高句丽人.md "wikilink")
[Y](../Category/唐朝官员.md "wikilink")
[Y](../Category/高句丽将领.md "wikilink")
[Category:朝鮮半島入仕中國者](../Category/朝鮮半島入仕中國者.md "wikilink")
[Category:唐朝郡公](../Category/唐朝郡公.md "wikilink")

1.  《大周故金紫光禄大夫行营缮大匠上护军辽阳郡开国公泉君墓志铭并序》，通直郎、襄城县开国子泉光富撰。