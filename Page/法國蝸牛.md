[Helix_pomatia_89a.jpg](https://zh.wikipedia.org/wiki/File:Helix_pomatia_89a.jpg "fig:Helix_pomatia_89a.jpg")——[罗曼蜗牛](../Page/罗曼蜗牛.md "wikilink")（*Helix
pomatia*）\]\]
[Escargot_p1150449.jpg](https://zh.wikipedia.org/wiki/File:Escargot_p1150449.jpg "fig:Escargot_p1150449.jpg")和[香草黄油烹調的带壳法國蝸牛](../Page/香草.md "wikilink")（与€0.02[欧元硬币作对比](../Page/欧元.md "wikilink")）\]\]
[Escargot_p1150450.jpg](https://zh.wikipedia.org/wiki/File:Escargot_p1150450.jpg "fig:Escargot_p1150450.jpg")
**法国蝸牛**（，），是一種可食用的蝸牛。一般會用來作[法国菜的](../Page/法国菜.md "wikilink")[頭盤菜](../Page/頭盤.md "wikilink")。

## 可食用的品種

在世界上，並非所有的蝸牛都可以食用，不过其中还是有很多是可食的；即使在可食用的品種裡，不同品種的味道及口感各異，而其中有兩種品種有法國原生，成為了法國菜所用的蝸牛品種。这两种蜗牛中有一种[散大蜗牛](../Page/散大蜗牛.md "wikilink")（*Helix
aspersa*，），在全球的[温带气候地区都普遍繁殖](../Page/温带.md "wikilink")。

## 烹調與食用

由於蝸牛日常以泥土、[腐殖質及各種不同品種的樹葉作食糧](../Page/腐殖質.md "wikilink")，蝸牛胃部裡的食物殘餘可能對人體有害。因此，在烹調之前，一定要先讓蝸牛禁食，讓蝸牛排出所有殘餘的食物，才可以把它們的消化系統切除，以準備食用。由於這項處理過程需時多天，一般急凍的蝸牛都已清理過。在美國及歐洲，亦有蝸牛農場專門培養烹調用的蝸牛。這些蝸牛的食糧都是研磨後的穀物，使食客可以放心食用。

## 營養價值

法国蜗牛是高蛋白低脂肪的食物，大约含有15%蛋白质，2.4%脂肪以及大约80%的水分。\[1\]

## 做法

[Escargotbordeaux.jpg](https://zh.wikipedia.org/wiki/File:Escargotbordeaux.jpg "fig:Escargotbordeaux.jpg")
一般來說，蝸牛在烹調前都已把它的殼和內臟移除。最常見的烹調方式是與[大蒜和](../Page/大蒜.md "wikilink")[黄油一起烹煮](../Page/黄油.md "wikilink")，然後把煮好的蝸牛肉和汁液放進殼裡。蝸牛一般都會用有小凹坑的碟來上菜，以防止蝸牛在碟上滾動。法国人也發明了一种专门吃蜗牛的[叉子和](../Page/叉子.md "wikilink")[钳子](../Page/钳子.md "wikilink")。鉗子的外貌像女性的[睫毛鉗](../Page/睫毛鉗.md "wikilink")，而叉子則是一條很細的兩指叉。

除了用大蒜和黄油一起煮，亦有模仿海鮮的煮法，把蝸牛肉與[薯蓉及](../Page/马铃薯泥.md "wikilink")[烟肉一起烤焗](../Page/烟肉.md "wikilink")。

## 參看

  -
  - [蜗牛食品](../Page/蜗牛#蜗牛的生活.md "wikilink")

  - [非洲大蝸牛](../Page/非洲大蝸牛.md "wikilink")

  - [炒田螺](../Page/炒田螺.md "wikilink")

## 外部链接

  - [法國蝸牛的營養價值](http://www.dietaryfiberfood.com/snail-escargot.php)

## 參考資料

<div class="references-small">

<references />

</div>

[Category:法国食品](../Category/法国食品.md "wikilink")

1.  [dietaryfiberfood.com](http://www.dietaryfiberfood.com/snail-escargot.php)