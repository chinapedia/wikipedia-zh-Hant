**-{zh-hans:法布里齐奥·拉瓦内利; zh-hk:拉雲拿利;}-**（**Fabrizio
Ravanelli**，1968年12月11日 -
），[意大利人](../Page/意大利.md "wikilink")，前职业足球運動員，司职[前鋒](../Page/前鋒_\(足球\).md "wikilink")，有「白头佬」和「銀狐」之稱。

## 生平

出身於意大利中部城鎮[佩魯賈的拉雲拿利早年是在家鄉球會打起](../Page/佩魯賈.md "wikilink")，曾轉會到多家低組別球會。至1992年轉會至意大利班霸[祖雲達斯](../Page/祖雲達斯.md "wikilink")，拉雲拿利才嶄露頭角。由於他總是以白色頭髮示人，故「銀狐」之名不脛而走。在祖雲達斯期間他贏得了一屆[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")(1992-93)、一屆[意甲聯賽冠軍](../Page/意甲.md "wikilink")(1994-95)，並於1996年贏得[歐洲聯賽冠軍杯](../Page/歐洲聯賽冠軍杯.md "wikilink")。

其後拉雲拿利轉到[英國發展](../Page/英國.md "wikilink")，起先他效力[英超下游球會](../Page/英超.md "wikilink")[米德尔斯堡](../Page/米德尔斯堡足球俱乐部.md "wikilink")，乃當時球會薪金數一數二的球星。不過不幸的是雖然拉雲拿利貴為英超入球量高的球員，但米杜士堡卻在他加盟後第二年降級收場。事後拉雲拿利公開批評米杜士堡根本沒良好的訓練設施，沒資格成為英超一分子。其後他轉到[法甲效力](../Page/法甲.md "wikilink")[馬賽](../Page/马赛足球俱乐部.md "wikilink")，打了兩年；之後再回到意大利效力首都球會[拉素](../Page/拉素體育會.md "wikilink")，不過只能擔任後備。

2001年他回到英超加盟另一支下游球會[德比郡](../Page/德比郡足球俱乐部.md "wikilink")，力求爭取上陣機會，可是打比郡隨後降級收場。之後拉雲拿利不但未能即時離開，還因球會財政困難而被欠薪，直到現在依然未能全數付足。2003年轉投[蘇超的登地](../Page/蘇格蘭足球超級聯賽.md "wikilink")，但半年後被會方終止合同。結果拉雲拿利重返母會直至退役。

現時他是意大利一電視台的評述員。曾執教祖雲達斯青年隊及法甲[阿雅克肖](../Page/阿雅克肖足球俱乐部.md "wikilink")。

## 外部連結

  - [拉雲拿利個人網站](http://www.ravanelli.com/)

[Category:1968年出生](../Category/1968年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:意大利足球運動員](../Category/意大利足球運動員.md "wikilink")
[Category:佩魯賈球員](../Category/佩魯賈球員.md "wikilink")
[Category:阿韋利諾球員](../Category/阿韋利諾球員.md "wikilink")
[Category:祖雲達斯球員](../Category/祖雲達斯球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:打比郡球員](../Category/打比郡球員.md "wikilink")
[Category:登地球員](../Category/登地球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:蘇超球員](../Category/蘇超球員.md "wikilink")
[Category:義大利旅外足球運動員](../Category/義大利旅外足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:法國外籍足球運動員](../Category/法國外籍足球運動員.md "wikilink")
[Category:蘇格蘭外籍足球運動員](../Category/蘇格蘭外籍足球運動員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")