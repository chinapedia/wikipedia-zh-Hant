[Faxian_zhuan.JPG](https://zh.wikipedia.org/wiki/File:Faxian_zhuan.JPG "fig:Faxian_zhuan.JPG")

《**法显传**》是[东晋高僧](../Page/东晋.md "wikilink")[法显所著的游记](../Page/法显.md "wikilink")。法显在[弘始二年](../Page/弘始.md "wikilink")（400年），与慧景、道整、慧应、慧嵬等僧人，从[长安出发](../Page/长安.md "wikilink")，西渡流沙，到[天竺寻求戒律](../Page/天竺.md "wikilink")。法显于[义熙八年](../Page/义熙.md "wikilink")（412年）归国，前后14年。法显归国后著《佛国记》一书，记录西行见闻。《法显传》又名《佛国记》、《历游天竺记传》\[1\]，《佛游天竺记传》、《释法明游天竺记》\[2\]，是[佛教史和](../Page/佛教史.md "wikilink")[中外交通史的重要文献](../Page/中外交通史.md "wikilink")。

## 行程

[隆安三年](../Page/隆安_\(东晋\).md "wikilink")(399年)，法显与慧景、道整、慧应、慧嵬，一行五位僧人、从长安出发，过[陕西与](../Page/陕西.md "wikilink")[甘肃之间的](../Page/甘肃.md "wikilink")[陇山到](../Page/陇山.md "wikilink")[乾归国](../Page/乾归.md "wikilink")，逢夏日雨季，休整约三个月后，前往[褥檀国](../Page/褥檀.md "wikilink")。过养楼山到[张掖镇](../Page/张掖镇.md "wikilink")，受到张掖王接待，会合智严、慧简、僧绍、宝云、僧景，结为同志，一同在张掖夏坐。夏坐完毕后，前往[敦煌停留一个月](../Page/敦煌.md "wikilink")。法显等五人先行，别过智严等五人，度大沙漠，行17日抵达鄯善国，国王信奉小乘学。在此住一个月，前往[乌夷国](../Page/乌夷.md "wikilink")，国中有僧四千余，信奉小乘。住二月余，向西南进发，35日后，抵达[于阗](../Page/于阗.md "wikilink")，国中有僧数万，信奉大乘教。再25日抵达[子合国](../Page/子合国.md "wikilink")，有[大乘教僧人千余](../Page/大乘教.md "wikilink")，法显等在此住15日，入[葱岭到](../Page/葱岭.md "wikilink")[于麾国](../Page/于麾国.md "wikilink")，山行25日到[竭叉国](../Page/竭叉国.md "wikilink")，由此西北，度葱岭抵达[北天竺小国](../Page/北天竺.md "wikilink")[陀历](../Page/陀历.md "wikilink")，有[小乘僧人](../Page/小乘.md "wikilink")。西南行15日过新头河，道路艰难险阻，汉之[张骞](../Page/张骞.md "wikilink")，[甘英未曾到此](../Page/甘英.md "wikilink")。度河，抵达[乌苌国](../Page/乌苌国.md "wikilink")，小乘佛法盛行。慧景，道整，慧达三人先行，前往[那竭国](../Page/那竭国.md "wikilink")，法显留此坐夏。坐夏毕，南下至[宿呵多国](../Page/宿呵多国.md "wikilink")。东行五日，抵达[犍陀卫国](../Page/犍陀罗.md "wikilink")，国人多奉小乘学。

## 評價

  - [季羡林曾比較過](../Page/季羡林.md "wikilink")《佛国记》與《[大唐西域記](../Page/大唐西域記.md "wikilink")》，他認為玄奘前往印度時，佛教在印度已經逐漸沒落了\[3\]。

## 翻译本

  - 英 [理雅各](../Page/理雅各.md "wikilink")
    [《沙门法显自记游天竺事》](https://web.archive.org/web/20090124223922/http://etext.library.adelaide.edu.au/f/fa-hien/f15l/)
  - 德 Max Deeg, Das Gaoseng Faxian zhuan, Wiesbaden, Harrasowwitz, 2005
  - 日 长泽和俊 法显传校注

## 注釋

## 參考書目

  - [东晋](../Page/东晋.md "wikilink")[法显著](../Page/法显.md "wikilink") 章巽校注
    《法显传校注》 中华书局 ISBN 978-7-101-05758-4

## 外部連結

  - 王邦維：[〈法顯與《法顯傳》〉](http://big.hi138.com/wenhua/lishixue/200605/52104.asp)（2006）

[Category:史部地理類](../Category/史部地理類.md "wikilink")
[Category:史傳部](../Category/史傳部.md "wikilink")
[Category:中外交通史文献](../Category/中外交通史文献.md "wikilink")
[Category:东晋典籍](../Category/东晋典籍.md "wikilink")
[Category:东晋佛教](../Category/东晋佛教.md "wikilink")
[Category:南亚佛教史](../Category/南亚佛教史.md "wikilink")
[Category:中亚佛教史](../Category/中亚佛教史.md "wikilink")
[Category:5世纪中国书籍](../Category/5世纪中国书籍.md "wikilink")

1.  《大唐内典录》 卷三
2.  杜佑《通典》 卷一百七十四
3.  《玄奘与〈大唐西域〉——校注〈大唐西域记〉前言》