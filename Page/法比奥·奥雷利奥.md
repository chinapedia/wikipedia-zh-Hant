**法比奧·奧利里奧·洛迪古斯**（**Fábio Aurélio
Rodrigues**，），[巴西足球運動員](../Page/巴西.md "wikilink")，曾經效力[英超球會](../Page/英超.md "wikilink")[利物浦](../Page/利物浦足球會.md "wikilink")。場上位置是左閘，亦可推前作左中場。由於他擁有[意大利護照](../Page/意大利.md "wikilink")，因此在[英國工作並不需要特別許可](../Page/英國.md "wikilink")。

出身於巴西球會[聖保羅](../Page/聖保羅足球會.md "wikilink")，1997年首次為一隊上陣，曾代表[巴西](../Page/巴西國家足球隊.md "wikilink")17歲以下、20歲以下及21歲以下國家隊，亦曾參加[2000年悉尼奧運足球項目](../Page/2000年夏季奧林匹克運動會.md "wikilink")。

在2000年加盟[西班牙球會](../Page/西班牙.md "wikilink")[華倫西亞](../Page/瓦倫西亞足球俱樂部.md "wikilink")，簽訂6年合約。數年間已上陣96場，主要以後備居多。由於[華倫西亞乃現任利物浦教練](../Page/瓦倫西亞足球俱樂部.md "wikilink")[賓尼迪斯執教過的球會](../Page/賓尼迪斯.md "wikilink")，在[賓尼迪斯上任利物浦教練後](../Page/賓尼迪斯.md "wikilink")，奧利里奧遂在合同到期後於2006年8月13日自由轉投利物浦，並於2006年[英格蘭慈善盾中首次上陣](../Page/英格蘭慈善盾.md "wikilink")。奧利里奧的傳中球質素甚高，因而在場上往往被[賓尼迪斯指示處理定位球](../Page/賓尼迪斯.md "wikilink")。他的加盟被認為是取代左後衛[懷斯的地位](../Page/懷斯.md "wikilink")，然而加盟利物浦以來受傷不斷，令他只能偶爾為利物浦上陣。

2010年5月25日，奧利里奧被球會拒絕簽署一份依出場次數決定主要薪金的新合約，一度成為自由身球員\[1\]；最終同年7月31日與球會達成協議，簽約兩年。\[2\]

## 榮譽

**冠軍**

  - [聖保羅](../Page/聖保羅.md "wikilink")

<!-- end list -->

  - [聖保羅州聯賽](../Page/巴西聖保羅州聯賽.md "wikilink")：1998年、2000年

<!-- end list -->

  - 華倫西亞

<!-- end list -->

  - [西甲](../Page/西甲.md "wikilink")：2001-02、2003-04
  - [歐洲足協盃](../Page/歐洲足協盃.md "wikilink")：2004年
  - [歐洲超級盃](../Page/歐洲超級盃.md "wikilink")：2004年

<!-- end list -->

  - [利物浦](../Page/利物浦足球會.md "wikilink")

<!-- end list -->

  - [英格蘭慈善盾](../Page/英格蘭慈善盾.md "wikilink")：2006年
  - [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink"): 2011/12年

## 參考資料

## 外部連結

  - [soccerbase奧雷利奧資料](http://www.soccerbase.com/players_details.sd?playerid=24168)
  - [利物浦官方網站
    奧雷利奧資料](https://web.archive.org/web/20070714172017/http://www.liverpoolfc.tv/team/squad/aurelio/)
  - [LFChistory.net
    奧雷利奧資料](http://www.lfchistory.net/player_profile.asp?player_id=1157)

[Category:巴西足球運動員](../Category/巴西足球運動員.md "wikilink")
[Category:聖保羅球員](../Category/聖保羅球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:聖保羅州人](../Category/聖保羅州人.md "wikilink")
[Category:巴西奧運足球運動員](../Category/巴西奧運足球運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會足球運動員](../Category/2000年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:義大利裔巴西人](../Category/義大利裔巴西人.md "wikilink")
[Category:歸化義大利公民](../Category/歸化義大利公民.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")

1.
2.