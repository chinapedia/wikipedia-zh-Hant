**法羅群島足球代表隊**由[法羅群島足球協會管理](../Page/法羅群島足球協會.md "wikilink")，現時屬於[國際足協及](../Page/國際足協.md "wikilink")[歐洲足協成員之一](../Page/歐洲足協.md "wikilink")。法羅群島至今從來沒有進入過國際大賽的決賽週。

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>成績</p></th>
<th><p>外圍賽紀錄</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>賽</p></td>
<td><p>勝</p></td>
<td><p>和</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
<td><p>未參與</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
<td><p>外圍賽出局</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>總結</p></td>
<td><p>50</p></td>
<td><p>5</p></td>
</tr>
</tbody>
</table>

## 歐洲國家盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>成績</p></th>
<th><p>外圍賽紀錄</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>賽</p></td>
<td><p>勝</p></td>
<td><p>和</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年歐洲國家盃.md" title="wikilink">1960年</a></p></td>
<td><p>未參與</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年歐洲國家盃.md" title="wikilink">1964年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年歐洲國家盃.md" title="wikilink">1968年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年歐洲國家盃.md" title="wikilink">1972年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年歐洲國家盃.md" title="wikilink">1976年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年歐洲國家盃.md" title="wikilink">1980年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984年歐洲國家盃.md" title="wikilink">1984年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年歐洲國家盃.md" title="wikilink">1988年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年歐洲國家盃.md" title="wikilink">1992年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年歐洲國家盃.md" title="wikilink">1996年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p> <a href="../Page/2000年歐洲國家盃.md" title="wikilink">2000年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年歐洲國家盃.md" title="wikilink">2004年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p> <a href="../Page/2008年歐洲國家盃.md" title="wikilink">2008年</a></p></td>
<td><p>外圍賽出局</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p> <a href="../Page/2012年歐洲國家盃.md" title="wikilink">2012年</a></p></td>
<td><p><a href="../Page/2012年歐洲國家盃外圍賽C組.md" title="wikilink">外圍賽出局</a></p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p>總結</p></td>
<td><p>58</p></td>
<td><p>4</p></td>
</tr>
</tbody>
</table>

## [國際島國運動會足球比賽成績](../Page/國際島國運動會.md "wikilink")

  - 1989年 - *冠軍*
  - 1991年 - *冠軍*

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽C組](../Page/2014年世界盃外圍賽歐洲區C組.md "wikilink")

### [2016年歐洲國家盃](../Page/2016年歐洲國家盃外圍賽.md "wikilink")

## 外部連結

  - [官方网站](http://www.football.fo/)
  - [RSSSF archive of
    results 1930–](http://www.rsssf.com/tablesf/far-intres.html)
  - [UEFA.com](http://www.uefa.com/Competitions/EURO/Teams/Team=51218/index.html)
  - [Footballsupporters.fo](https://web.archive.org/web/20070328201910/http://footballsupporters.fo/)
    (12. Maður – "12th Man", the supporters of the Faroe Islands
    national football team)

[Category:欧洲国家足球队](../Category/欧洲国家足球队.md "wikilink")
[Category:法羅群島足球](../Category/法羅群島足球.md "wikilink")