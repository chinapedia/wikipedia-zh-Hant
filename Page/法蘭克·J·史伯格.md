**法蘭克·朱利安·史伯格**（，），美國發明家兼海軍軍官，對[電動機](../Page/電動機.md "wikilink")、[電氣化鐵路和](../Page/電氣化鐵路.md "wikilink")[升降機發展有貢獻](../Page/升降機.md "wikilink")。他促進了[都市化](../Page/都市化.md "wikilink")：更好的運輸系統使城市的大小合理增加，改良[升降機技術幫助了](../Page/升降機.md "wikilink")[摩天大樓發展](../Page/摩天大樓.md "wikilink")，繼而令商業發展更密集。他有「電力牽引之父」之稱。

[Theatrical_District,_Richmond,_Virginia,_1923.jpg](https://zh.wikipedia.org/wiki/File:Theatrical_District,_Richmond,_Virginia,_1923.jpg "fig:Theatrical_District,_Richmond,_Virginia,_1923.jpg")的[有轨电车系統便是史伯格的作品](../Page/有轨电车.md "wikilink")\]\]

[S](../Category/1857年出生.md "wikilink")
[S](../Category/1934年逝世.md "wikilink")
[S](../Category/美国发明家.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")