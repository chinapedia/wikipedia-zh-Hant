**法蘭西共和國臨時政府**（[法语](../Page/法语.md "wikilink")：，简称****）是指自1944年至1946年管理[法國](../Page/法國.md "wikilink")（[国号为](../Page/国号.md "wikilink")[法兰西共和国](../Page/法兰西共和国.md "wikilink")）的[临时政府](../Page/临时政府.md "wikilink")。

1944年6月3日，（，简称****）宣佈成立臨時政府。在1944年夏[解放巴黎後](../Page/解放巴黎.md "wikilink")，這個政府接管了法國並由[戴高樂統領](../Page/戴高樂.md "wikilink")。

臨時政府由[共產主義者](../Page/共產主義.md "wikilink")、[社會主義者及](../Page/社會主義.md "wikilink")[戴高樂主義者組成](../Page/戴高樂主義.md "wikilink")，均曾參與對抗[納粹德國的侵略](../Page/納粹德國.md "wikilink")。臨時政府將許多在[維琪政權時期所制定的法律變成無效](../Page/維希法國.md "wikilink")，並宣佈維琪政權為非法。1946年1月20日，由於與[共產黨人不合](../Page/法國共產黨.md "wikilink")，戴高樂辭去總統一職。

法蘭西共和國臨時政府是1944年至1946年之間的[過渡政府](../Page/過渡政府.md "wikilink")。1940年，法國由[貝當所建立的维希政權所管治](../Page/貝當.md "wikilink")，1944年[諾曼第登陸後](../Page/諾曼第登陸.md "wikilink")，巴黎被光復，维希政權瓦解。政權交由戴高樂所領導的臨時政府。臨時政府最終由1946年的**[法蘭西第四共和國](../Page/法蘭西第四共和國.md "wikilink")**繼承。

## 臨時政府主席（總統）名單

  - [夏尔·戴高樂](../Page/夏尔·戴高樂.md "wikilink")，1944-1946

  - ，1946

  - ，1946

  - [莱昂·布鲁姆](../Page/莱昂·布鲁姆.md "wikilink")，1946-1947

[Category:法国历史](../Category/法国历史.md "wikilink")
[Category:短命國家](../Category/短命國家.md "wikilink")