**法蘭西斯·福山**（，），[美國](../Page/美國.md "wikilink")[作家](../Page/作家.md "wikilink")、[政治經濟學者](../Page/政治經濟學.md "wikilink")。福山本人大學就讀[康乃爾大學](../Page/康乃爾大學.md "wikilink")，並獲得文學士（主修古典文獻與[政治](../Page/政治學.md "wikilink")），並於[哈佛大學獲得政治學博士](../Page/哈佛大學.md "wikilink")，師從[塞缪尔·P·亨廷顿](../Page/塞缪尔·P·亨廷顿.md "wikilink")。

## 早年生活

福山在[芝加哥](../Page/芝加哥.md "wikilink")[海德公園區出生](../Page/海德公园_\(芝加哥\).md "wikilink")。他的祖父在1905年因[日俄戰爭而逃難至美國](../Page/日俄战争.md "wikilink")，其後在西岸經營店舖，於[第二次世界大戰中被囚禁](../Page/第二次世界大战.md "wikilink")。\[1\]福山的父親出生於[芝加哥](../Page/芝加哥.md "wikilink")，為第二代[日本裔](../Page/日裔美國人.md "wikilink")，並於[芝加哥大學修得](../Page/芝加哥大學.md "wikilink")[社會學](../Page/社會學.md "wikilink")[博士學位](../Page/博士學位.md "wikilink")；其母為[京都大學](../Page/京都大學.md "wikilink")[經濟學系創始人](../Page/經濟學.md "wikilink")[河田嗣郎之女儿](../Page/河田嗣郎.md "wikilink")。\[2\]
福山在[曼哈頓長大](../Page/曼哈頓.md "wikilink")，雖身為日裔，但沒有學習[日語](../Page/日语.md "wikilink")，也很少接觸日本文化。1967年，福山和家人移居至[賓夕法尼亞州的](../Page/宾夕法尼亚州.md "wikilink")[州學院](../Page/州学院.md "wikilink")。\[3\]

## 論點

福山是《[歷史的終結及最後之人](../Page/歷史的終結及最後之人.md "wikilink")》一書的作者，他在該書中認為，人類歷史的前進與[意識形態之間的鬥爭正走向](../Page/意識形態.md "wikilink")“終結”，隨著[冷戰的結束](../Page/冷戰.md "wikilink")，「[自由民主](../Page/自由民主.md "wikilink")」和[資本主義被定於一尊](../Page/資本主義.md "wikilink")，是謂「資本陣營」的勝利。在政治上，福山為一[新保守主義者](../Page/新保守主義.md "wikilink")。他自1997年起活躍於「美國新世紀專案」[智囊團](../Page/智囊團.md "wikilink")，並曾簽署文件建議當時[美國總統](../Page/美國總統.md "wikilink")[柯林頓推翻](../Page/柯林頓.md "wikilink")[伊拉克總統](../Page/伊拉克總統.md "wikilink")[海珊](../Page/薩達姆·侯賽因.md "wikilink")；但他反對2003年的[美伊戰爭](../Page/美伊戰爭.md "wikilink")，並認為：新保守主義一味以武力推行美國價值，恐將重蹈[列寧主義的覆轍](../Page/列寧主義.md "wikilink")\[4\]。

2014年福山的著作《政治秩序及其衰落》（*Political Order and Political
Decay*），思考國家政府的穩定性問題。福山認為其結論要做出修正，將「[法治](../Page/法治.md "wikilink")」（rule
of
law）、「民主問責」（democracy）之外另加第三變量「國家治理能力」（state）；認為很多國家在這三項中前兩項得分高、但是第三項得分很低，\[5\]造成發展不如預期。多數觀點認為是[中國崛起和中東混亂的效應讓福山必須修正觀點](../Page/中國崛起.md "wikilink")，福山則堅稱自己核心理念不變、只是小修改。

2015年1月，由《[環球時報](../Page/環球時報.md "wikilink")》記者全球採訪西方[政治人物和](../Page/政治人物.md "wikilink")[智庫成員共](../Page/智庫.md "wikilink")41人的報導文集《我們誤判了中國：西方政要智囊重構對華認知》出版\[6\]，福山在本書中认为，最近20年中国经验刷新了他的部分看法，他没有料到中国经济增长“带来的冲击那么广泛”\[7\]\[8\]。2016年11月11日，針對[2016年美國總統選舉](../Page/2016年美國總統選舉.md "wikilink")[唐納·川普勝選](../Page/唐納·川普.md "wikilink")，福山在《[金融時報](../Page/金融時報.md "wikilink")》發表專文〈對抗世界的美國：川普治下的美國與新全球秩序〉（US
against the world? Trump's America and the new global
order），明確提出：整個人類世界已經進入「民粹式[民族主義](../Page/民族主義.md "wikilink")」（populist
nationalism）主導的動盪時代\[9\]。

2017年4月14日，福山在《[聯合報](../Page/聯合報.md "wikilink")》專訪中表明新觀點的三大主軸\[10\]：

  - 上街頭或網路投票之類的「直接民主」是不可行的，因為：多數民眾並沒知識水準和時間去了解極度複雜的政策和未來影響，最後只能被簡單口號操縱投票，等於被媒體老闆操縱。「直接民主，作為政府形式之一，是完全無法運作的。」還是代議政治優於直接民主，但代議制的眼前諸多巨大問題他並未提出方案，只就單純這兩者比較而言。
  - 科技和[全球化的影響比他當年預判的還強大很多](../Page/全球化.md "wikilink")，全球化確實只讓極少數人變巨富而多數人生活下降，科技會搶走更多工作，政府必須介入重分配、不能放任經濟自由。重分配的失敗是導致西方人民開始對西方政治模式產生質疑的關鍵點，但價值觀和富人政治讓這些重分配無法執行，陷入死循環。
  - 隨著對重分配的失望繼續蔓延，民粹主義將主導許多國家的選舉，強人政治會讓很多欧美或日本民眾羨慕，中俄模式——尤其是[中國模式](../Page/中國模式.md "wikilink")——會有吸引力；但中國模式能否做到西方無法達成的財富重分配，現在還遠不是看出定論的時候，必須再等待很長時期下去看最後的結果。

## 註記

## 著作

  - *The End of History and the Last Man*(1992)，中文譯本：
      - 《歷史之終結-{}-與最後一人》，[李永熾譯](../Page/李永熾.md "wikilink")，[時報文化](../Page/時報文化.md "wikilink")（1993）ISBN
        9789571306513。
      - 《历史的终结-{}-及最后之人》，黄胜强、许铭原译，[中国社会科学出版社](../Page/中国社会科学出版社.md "wikilink")（2003）ISBN
        9787500437086。
  - *Trust: The Social Virtues and the Creation of
    Prosperity*(1995)，中文譯本：
      - 《信任：社会美德与创造经济繁荣》，彭志华译，[海南出版社](../Page/海南出版社.md "wikilink")（2001）ISBN
        9787544301732。
      - 《信任：社會德性與繁榮的創造》，李宛蓉譯，立緒文化（2004）ISBN 9570411945。
      - 《信任：社會德性與經濟繁榮》，李宛蓉譯，立緒文化（2014）ISBN 9789863600220。

<!-- end list -->

  - *The Great Disruption: Human Nature and the Reconstitution of Social
    Order*(1999)，中文譯本：
      - 《跨越斷層：人性與社會秩序重建》，張美惠譯，時報文化（2000）ISBN 9571331120
      - 《大分裂：人类本性与社会秩序的重建》，刘榜离等译，中国社会科学出版社（2002）ISBN 7500432925
  - *Our Posthuman Future: Consequences of the Biotechnology
    Revolution*(2002)，中文譯本：
      - 《後人類未來：基因工程的人性浩劫》，杜默譯，時報文化（2002）ISBN 9571336610。
  - *State-Building:Governance and World Order in the 21th
    Century*(2004)，中文譯本：
      - 《強國論》，閻紀宇譯，時報文化（2005）ISBN 9571343900。
      - 《国家构建：21世纪的国家治理与世界秩序》，中国社会科学出版社（2007）ISBN 9787500460183
  - *America at the Crossroads: Democracy, Power, and the
    Neoconservative Legac*(2006)，中文譯本：
      - 《美国处在十字路口：民主、权力与新保守主义的遗产》，周琪译，中国社会科学出版社（2008） ISBN 9787500473749
  - *The Origins of Political Order: From Prehuman Times to the French
    Revolution*(2011)，中文譯本：
      - 《政治秩序的起源 上卷：從史前到法國大革命》，黃中憲、林錦慧譯，時報文化（2014）ISBN 9789571360652。
      - 《政治秩序的起源：从前人类时代到法国大革命》，毛俊杰译，[广西师范大学出版社](../Page/广西师范大学出版社.md "wikilink")（2012）
        ISBN 9787549512805
  - *Political Order and Political Decay: From the Industrial Revolution
    to the Globalization of Democracy*(2014)，中文譯本：
      - 《政治秩序的起源 下卷：從工業革命到民主全球化的政治秩序與政治衰敗》，林麗雪譯，時報文化（2015）ISBN
        9789571364032。

[F](../Category/美國政治學家.md "wikilink")
[F](../Category/美國經濟學家.md "wikilink")
[F](../Category/美國作家.md "wikilink")
[F](../Category/關西大學教師.md "wikilink")
[F](../Category/哈佛大學校友.md "wikilink")
[F](../Category/康乃爾大學校友.md "wikilink")
[F](../Category/日本裔美国人.md "wikilink")
[Category:兰德公司人物](../Category/兰德公司人物.md "wikilink")

1.

2.

3.
4.

5.  [福山論國家治理能力
    大陸比美國強](http://www.chinatimes.com/newspapers/20160214000287-260108)

6.  谷棣、謝戎彬主編，《我們誤判了中國：西方政要智囊重構對華認知》，[華文出版社](../Page/華文出版社.md "wikilink")2015年1月出版，ISBN
    978-7507542837。

7.

8.  [金灿荣为《我们误判了中国》作序：中国纠错意志和能力非常强](http://www.thepaper.cn/newsDetail_forward_1296517)

9.

10. [法蘭西斯·福山憂心中國越強大 會對台灣不利](https://udn.com/news/story/7314/2405582)