[Francis_Galton.jpg](https://zh.wikipedia.org/wiki/File:Francis_Galton.jpg "fig:Francis_Galton.jpg")

**法蘭西斯·高爾頓**爵士，[FRS](../Page/皇家學會院士.md "wikilink")（，），[英格蘭](../Page/英格蘭.md "wikilink")[維多利亞時代的](../Page/維多利亞時代.md "wikilink")[博学家](../Page/博学家.md "wikilink")、[人類學家](../Page/人类学.md "wikilink")、[優生學家](../Page/優生學.md "wikilink")、熱帶探險家、[地理學家](../Page/地理學家.md "wikilink")、[發明家](../Page/發明家.md "wikilink")、[氣象學家](../Page/氣象學.md "wikilink")、[統計學家](../Page/統計學.md "wikilink")、[心理學家和](../Page/心理學.md "wikilink")[遺傳學家](../Page/遺傳學家.md "wikilink")，[查爾斯·達爾文的表弟](../Page/查爾斯·達爾文.md "wikilink")。

## 生平

高爾頓一生中發表了超過340篇的報告和書籍，他在1909年获封爵士。他在1883年率先使用「[優生學](../Page/優生學.md "wikilink")」（eugenics）一詞。在他於1869年的著作《遺傳的天才》（*Hereditary
Genius*）中，高爾頓主張人類的才能是能夠透過[遺傳延續的](../Page/遺傳.md "wikilink")。

他在[統計學方面也有貢獻](../Page/統計學.md "wikilink")，高爾頓在1877年發表關於種子的研究結果，指出[迴歸到平均值](../Page/迴歸到平均值.md "wikilink")（regression
toward the
mean）現象的存在，這個概念與現代統計學中的“回歸”並不相同，但是卻是回歸一詞的起源。在此後的研究中，高爾頓第一次使用了[相關係數](../Page/相關係數.md "wikilink")（correlation
coefficient）的概念。他使用字母“r”來表示相關係數，這個傳統一直延續至今。同時他也發表了關於[指紋的論文和書籍](../Page/指紋.md "wikilink")，被認為對於現代利用指紋進行犯罪搜查方面有很大的貢獻。

他在[心理學方面也有貢獻](../Page/心理學.md "wikilink")，他在1884年出版《品格的測量》，是差异心理学和心理测量学的科学方法的創立者。

## 參考文獻

<div class="references-small">

1.  Gillham, Nicholas Wright (2001). A Life of Sir Francis Galton: From
    African Exploration to the Birth of Eugenics. Oxford University
    Press: New York. ISBN 0-19-514365-5
2.  Bulmer, Michael (2003). Francis Galton: Pioneer of Heredity and
    Biometry. Johns Hopkins University Press. ISBN 0-8018-7403-3
3.  《心理測量與測驗》（第2版）鄭日昌主編 中國人民大學出版社p239-243

</div>

## 外部連結

  - [高頓完整作品列表](http://galton.org)（Galton.org），包含所有出版書籍、科學報告、在大眾媒體上發表的文章，以及其他未公開發表的作品和傳記資料。

  - [歷史與數學（History and
    Mathematics）](http://urss.ru/cgi-bin/db.pl?cp=&page=Book&id=53184&lang=en&blang=en&list=Found)

  - [人類記憶 - 阿姆斯特丹大學](http://memory.uva.nl/testpanel/gc/en/)，根據高頓的成就進行的實驗

[Galton, Francis](../Category/英国人类学家.md "wikilink") [Galton,
Francis](../Category/英国探险家.md "wikilink") [Galton,
Francis](../Category/英國地理学家.md "wikilink") [Galton,
Francis](../Category/英國气象学家.md "wikilink") [Galton,
Francis](../Category/英国统计学家.md "wikilink") [Galton,
Francis](../Category/下級勳位爵士.md "wikilink") [Galton,
Francis](../Category/英國皇家學會院士.md "wikilink")
[G](../Category/達爾文-威治伍德家族.md "wikilink")
[G](../Category/劍橋大學三一學院校友.md "wikilink")
[G](../Category/倫敦國王學院校友.md "wikilink")
[Category:英国优生学家](../Category/英国优生学家.md "wikilink")
[Category:皇家奖章获得者](../Category/皇家奖章获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")