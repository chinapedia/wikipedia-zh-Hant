**法語**（）也稱**法文**，属于[印欧语系](../Page/印欧语系.md "wikilink")[罗曼语族](../Page/罗曼语族.md "wikilink")，法語是除[英語之外最多國家的](../Page/英語.md "wikilink")[官方語言也是](../Page/官方語言.md "wikilink")[聯合國](../Page/聯合國.md "wikilink")[工作語言之一](../Page/工作語言.md "wikilink")，法語也是聯合國、歐盟、北約、奧運會、世貿和國際紅十字會等眾多國際組織的官方語言及正式行政語言。法語在11世纪曾是除了[中古漢語以外](../Page/中古漢語.md "wikilink")，當時世界上使用人口最多的语言。現時全世界有約一億人将法语作为母语，另有2.8億人使用法语（包括把它作为[第二语言的人](../Page/第二语言.md "wikilink")）；这些数字目前仍在增長中，尤其是在非洲大陸。法語被广泛使用，其程度位居世界第二，僅次於[英語](../Page/英語.md "wikilink")。1635年[法蘭西學術院設立](../Page/法蘭西學術院.md "wikilink")，規範法語的語法與拼寫。現今[法国法语](../Page/法国法语.md "wikilink")（français
métropolitain）和[魁北克法语](../Page/魁北克法语.md "wikilink")（québécois）是世界上最主要的两大法语分支，兩者在發音以及少数詞彙上有所区别，但書面形式則一致。

## 历史

4世纪，[罗马帝国统治](../Page/罗马帝国.md "wikilink")[法国](../Page/法国.md "wikilink")，[拉丁語开始在法国流行](../Page/拉丁語.md "wikilink")。至5世纪，拉丁語已经广泛取代了原先通行于法国的[凯尔特语](../Page/凯尔特语.md "wikilink")。在[高卢境内](../Page/高卢.md "wikilink")，随着罗马移民的增加，高卢人与之使用的[通用拉丁語融合形成](../Page/通用拉丁語.md "wikilink")[通俗拉丁语](../Page/通俗拉丁语.md "wikilink")，与此同时，作为上层文人使用的书面拉丁語开始衰退。5世纪，高卢境内的说拉丁语的早先居民，与随着[民族大迁徙进入高卢的讲](../Page/民族大迁徙.md "wikilink")[日耳曼语的](../Page/日耳曼语.md "wikilink")[法兰克人的语言开始融合](../Page/法兰克人.md "wikilink")。法语开始失去非重音音节。6－7世纪，大众语变为一种混合性语言（）。8世纪，[查理曼帝国的建立开始使得法語开始规范化](../Page/查理曼帝国.md "wikilink")。到9世纪，拉丁语和[日耳曼语最终融合成](../Page/日耳曼语.md "wikilink")[罗曼语](../Page/罗曼语.md "wikilink")。

1539年，法国国王[弗朗索瓦一世颁布的](../Page/弗朗索瓦一世_\(法兰西\).md "wikilink")[维莱科特雷法令正式规定法语取代拉丁语](../Page/维莱科特雷法令.md "wikilink")、方言和其它地方性语言，成为法律和行政上的官方语言。

1066年[征服者威廉把法語帶到了](../Page/征服者威廉.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")，讓英語產生了劇烈的改變（例如英語的[牛肉](../Page/牛肉.md "wikilink")是由法語演變而來的）。法語在當時是英格蘭王室使用的語言，所以英語裡的法語借詞往往聽起來比較高級。法國在路易十四之後發展成歐洲的強權，18世紀歐洲各國貴族也欣賞法國貴族的生活方式，法語變成歐洲各國的宮廷語言。直至當代的歐洲，法語是外交場合與上流社會的[通用語](../Page/通用語.md "wikilink")（lingua
franca）。

## 國際地位

[法國人一直對自己的語言非常自豪和驕傲](../Page/法國人.md "wikilink")。[法語很早就有規範的形式](../Page/法語.md "wikilink")，16世紀開始便成為國際外交及學術語言（例如辛丑合約就規定以法文版為準）。直到[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，歐洲衰弱[美國崛起](../Page/美國.md "wikilink")，英語逐漸取代了法語的國際地位。法國人對此十分不快，在力所能及的地方全力抗擊英語文化勢力。儘管如此，法語也在各個方面不同程度受到了英語的影响。

法語現在還維持第二大強勢語言的地位，它是聯合國六大官方語言之一，是主要的兩大工作語言之一。法語也是國際奧林匹克委員會的官方語言之一，在奧會體系的運動賽事的典禮上會先說法語，其次英語，再來是主辦國語言。國際民航組織的護照標準也規定護照的資料頁必須至少是雙語，英國、澳洲護照內印有英語及法語，美國護照則使用英語、法語西班牙語，此外歐盟的護照也都可以找到法語。

## 法语方言

[法語在法國和世界其他地區有多種變體](../Page/法語.md "wikilink")（[方言](../Page/方言.md "wikilink")）。法國人一般使用以巴黎的法語為標準的“本土法語”（français
métropolitain），但法國南部人亦使用受[奧克語影響的所謂的](../Page/奧克語.md "wikilink")“南部法語”（）。歐洲的法語變體有[比利時法語](../Page/比利時法語.md "wikilink")、[瑞士法語和義大利](../Page/瑞士法語.md "wikilink")[瓦萊達奧斯塔地區的奧斯塔法語](../Page/瓦萊達奧斯塔.md "wikilink")（）。在加拿大，法語與英語同為官方語言，法語方言主要有[魁北克法語和](../Page/魁北克法語.md "wikilink")[阿卡迪亞法語](../Page/阿卡迪亞法語.md "wikilink")。在[黎巴嫩](../Page/黎巴嫩.md "wikilink")，法語直到1941年為止是官方語言之一，當地的法語方言為[黎巴嫩法語](../Page/黎巴嫩法語.md "wikilink")。其他地區亦有法語的方言。

法国其它地区之语言则不属於法语方言，如西方布列塔尼半岛之[布列塔尼语属](../Page/布列塔尼语.md "wikilink")[凯尔特语族](../Page/凯尔特语族.md "wikilink")，北方[弗拉芒语则属](../Page/弗拉芒语.md "wikilink")[日尔曼语族](../Page/日尔曼语族.md "wikilink")，两者與法语所属之[罗曼语族语言皆有根本结构上之差异](../Page/罗曼语族.md "wikilink")。法語衍生的克里奧爾語也被認為是不同的語言。

## 地理分布

[Langues_de_la_France1.gif](https://zh.wikipedia.org/wiki/File:Langues_de_la_France1.gif "fig:Langues_de_la_France1.gif")
[Francophone_Africa.svg](https://zh.wikipedia.org/wiki/File:Francophone_Africa.svg "fig:Francophone_Africa.svg")地位。\]\]

  - 法語是下列国家或地区的第一语言，也是唯一的官方語言：
      - '''
        '''（6000萬人使用，包括[新喀里多尼亚](../Page/新喀里多尼亚.md "wikilink")、[留尼汪](../Page/留尼汪.md "wikilink")、[瓜德洛普](../Page/瓜德洛普.md "wikilink")、[马提尼克](../Page/马提尼克.md "wikilink")、[法属圭亚那和](../Page/法属圭亚那.md "wikilink")[圣皮埃尔和密克隆](../Page/圣皮埃尔和密克隆.md "wikilink")）
      - '''  '''
      - '''  '''
  - 法語是下列地区的第一语言，該國還有其他官方語言
      - *'
        [加拿大](../Page/加拿大.md "wikilink")[魁北克省](../Page/魁北克省.md "wikilink")*'（670萬人使用者，主要在[魁北克省](../Page/魁北克省.md "wikilink")）：[加拿大法语](../Page/加拿大法语.md "wikilink")
      - *'
        [比利時法語社群](../Page/比利時法語社群.md "wikilink")*'（400萬人使用者，[瓦龙语是](../Page/瓦龙语.md "wikilink")
         语的一种方言，与比利时法语不同）：[比利时法语](../Page/比利时法语.md "wikilink")
      - '''  [瑞士法語區](../Page/瑞士法語區.md "wikilink")
        '''（和[德語](../Page/德語.md "wikilink")、[意大利語和](../Page/意大利語.md "wikilink")[羅曼什語四語共行](../Page/羅曼什語.md "wikilink")）：[瑞士法語](../Page/瑞士法語.md "wikilink")
      - '''  [卢森堡](../Page/卢森堡.md "wikilink")
        '''（和[盧森堡語](../Page/盧森堡語.md "wikilink")、[德語並列](../Page/德語.md "wikilink")）
  - 在[安道尔是一种普遍的语言](../Page/安道尔.md "wikilink")。
  - 它在下列国家是官方语言，也是学校唯一使用的语言：[科摩罗](../Page/科摩罗.md "wikilink")、[刚果共和国](../Page/刚果共和国.md "wikilink")、[法属波利尼西亚](../Page/法属波利尼西亚.md "wikilink")、[加蓬和](../Page/加蓬.md "wikilink")[马里](../Page/马里.md "wikilink")。
  - 它在下列国家是官方语言或官方语言之一，但是没有当地语那么常用：[贝宁](../Page/贝宁.md "wikilink")、[科特迪瓦](../Page/科特迪瓦.md "wikilink")、[布基纳法索](../Page/布基纳法索.md "wikilink")、[布隆迪](../Page/布隆迪.md "wikilink")、[喀麦隆](../Page/喀麦隆.md "wikilink")、[中非共和国](../Page/中非共和国.md "wikilink")、[乍得](../Page/乍得.md "wikilink")、[赤道几内亚](../Page/赤道几内亚.md "wikilink")、[象牙海岸](../Page/象牙海岸.md "wikilink")、[几内亚](../Page/几内亚.md "wikilink")、[马达加斯加](../Page/马达加斯加.md "wikilink")、[尼日尔](../Page/尼日尔.md "wikilink")、[卢旺达](../Page/卢旺达.md "wikilink")、[塞内加尔](../Page/塞内加尔.md "wikilink")、[塞舌尔](../Page/塞舌尔.md "wikilink")、[多哥](../Page/多哥.md "wikilink")、[瓦努阿图和](../Page/瓦努阿图.md "wikilink")[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")。
  - 法語也是下列国家的主要第二语言，但沒有官方地位：[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[毛里求斯](../Page/毛里求斯.md "wikilink")、[摩洛哥和](../Page/摩洛哥.md "wikilink")[突尼西亞](../Page/突尼西亞.md "wikilink")。
  - 另外，在[埃及](../Page/埃及.md "wikilink")、[印度](../Page/印度.md "wikilink")[本地治里](../Page/本地治里.md "wikilink")、[意大利](../Page/意大利.md "wikilink")（）、[老挝](../Page/老挝.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[毛利塔尼亚](../Page/毛利塔尼亚.md "wikilink")、[英国](../Page/英国.md "wikilink")（[汊河岛](../Page/汊河岛.md "wikilink")，[海峽群島](../Page/海峽群島.md "wikilink")）、[美国](../Page/美国.md "wikilink")（[阿卡迪亚](../Page/阿卡迪亚.md "wikilink")、[卡真](../Page/卡真.md "wikilink")，參見[美國的法語](../Page/美國的法語.md "wikilink")）和[越南也有一些法语使用者](../Page/越南.md "wikilink")。

根据有关部门调查，有一千万[美国人有法国血统](../Page/美国人.md "wikilink")，而大概有一百万法国裔美国人在家庭使用法语。

<table>
<caption>全球以法語為母語人口大致分佈情形</caption>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>以法語為母語人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>60,000,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1,600,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>4,180,000（大部分在<a href="../Page/瓦隆區.md" title="wikilink">瓦隆區</a>）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>30,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>300,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>100,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>36,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>780,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>18,240,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>110,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>200,000</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1,000,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6,700,000（大部分在<a href="../Page/魁北克省.md" title="wikilink">魁北克省</a>）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>30,000</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 法语发音

法文祖先的[拉丁语很有规律性](../Page/拉丁语.md "wikilink")。而歷史上的因素及語音的演變，使得[拼写和实际](../Page/拼写.md "wikilink")[读音不一定相同](../Page/读音.md "wikilink")，但比語源多元的英語更具规则性。拉丁语由于不再作为母语，教材中仍然需要以[长音符号标注长音](../Page/长音符号.md "wikilink")。

### 字母

法文字母表是由[拉丁字母组成](../Page/拉丁字母.md "wikilink")。

  - 元音（母音）字母：
  - 辅音字母：
  - 连字：（）

法語和英語一样用26个[拉丁字母](../Page/拉丁字母.md "wikilink")，在学习法语发音的时候要分清楚元音字母和[元音](../Page/元音.md "wikilink")，辅音字母和[辅音亦然](../Page/辅音.md "wikilink")。法語和[英语](../Page/英语.md "wikilink")、[汉语的不同之处在于法语没有](../Page/汉语.md "wikilink")[双元音](../Page/双元音.md "wikilink")，发每个元音时口型都不滑动，尤其要注意发[鼻化元音时不能像汉语](../Page/鼻化元音.md "wikilink")[韵母似的有延续动作](../Page/韵母.md "wikilink")。

### 變音符號

法语主要用五個[變音符號](../Page/變音符號.md "wikilink")，有时候用来表示不同的发音，有时候只是区别不同的[语义](../Page/语义.md "wikilink")：

  - “”[长音符通常用于區分詞形相同的詞](../Page/长音符.md "wikilink")，或者表示某個元音字母後面曾經有一個/s/音，但是這個/s/音已經隨著語音流變而消失了，如源于[拉丁语单词](../Page/拉丁语.md "wikilink")（古法語為)，中间的s已經隨著語音流變而消失了；
  - “”[分音符可以和多个元音字母组合](../Page/分音符.md "wikilink")，表示这个元音字母不跟前面的元音字母构成一个字母组合，而分别发音，类似于双元音；
  - “”[闭音符只用在字母](../Page/闭音符.md "wikilink")“e”之上，表示这个字母发音为闭口音。也可以是某一個音消失的痕跡，如古法語系詞的過去分詞為，現代法語為；
  - “”[开音符用在字母](../Page/开音符.md "wikilink")“e”上表示这个字母发开口音，而用在其他字母上则用以区分不同的语义，如ou（「或者」）和où（「哪裡」）两个单词发音拼写完全一样，但是不同的词；
  - “”[软音符只用于](../Page/软音符.md "wikilink")“c”字母下面，因为法语中和英语中一样，“c”在“a、o、u”前发\[k\]音，在“e、i”前发\[s\]音，如果在“a、o”想让它发\[s\]音，需加软音符，如在français（「法国人」）中。

不同的地區對使用變音符號的指引略有不同：[法蘭西學術院主張大寫字母上方的變音符號不應該省略](../Page/法蘭西學術院.md "wikilink")\[1\]，但是則允許在某些情況下省略大寫字母上方的變音符號\[2\]。

### 元音

[French_oral_vowel_chart.svg](https://zh.wikipedia.org/wiki/File:French_oral_vowel_chart.svg "fig:French_oral_vowel_chart.svg")

法语的元音多数圆唇，因此法国人说话的时候嘴唇好像总是圆着的。要注意的是，/[ɑ](../Page/ɑ.md "wikilink")/对很多法语使用者来说不再是音位。但是对于/ə/是否是法语中的[音素还存在争论](../Page/音素.md "wikilink")。有人认为它與/[œ](../Page/œ.md "wikilink")/同音。

### 辅音

  - [爆破音](../Page/爆破音.md "wikilink")：/p\~b/、/k\~ɡ/、/t\~d/

<!-- end list -->

  -
    说[汉语的人](../Page/汉语.md "wikilink")（某些[方言区除外](../Page/方言.md "wikilink")）对这三组音比较难以掌握。难点在于区分每一组裡面左右两个音左边为[清辅音](../Page/清音.md "wikilink")，右边为[浊辅音](../Page/浊音.md "wikilink")。原因在于[普通话不存在不送氣清](../Page/普通话.md "wikilink")/浊辅音（不送氣全清/不送氣全濁）的区别，而只有送气與否的区别。即使学过英语，也可能没有注意，因为英语裡的清塞音一般情况下需要送气，s后面的清塞音不送气，如s**p**y，s**t**y，s**k**y等等，但並不是濁化）。发清辅音的时候，声带不震动；发浊辅音时，声带必须震动。

<!-- end list -->

  - [摩擦音](../Page/摩擦音.md "wikilink")：/s\~z/、/f\~v/、/ʃ\~ʒ/、/ʁ/

<!-- end list -->

  -
    \[v\]音在[汉语拼音所采用的字母裡面](../Page/汉语拼音.md "wikilink")，只用来拼写一些[少数民族译音](../Page/少数民族.md "wikilink")。/ʒ/听起来有点像汉语拼音的r，/z/像是[四川话](../Page/四川话.md "wikilink")“人”字的声母，汉语普通话裡没有/v/和/z/两个音素，仅在部分方言中出现。

<!-- end list -->

  - [腭音](../Page/腭音.md "wikilink")：//

<!-- end list -->

  -
    腭音，发音的时候舌面贴上腭，气流同时从口腔和鼻腔送出。為[上海话中](../Page/上海话.md "wikilink")“人”的尾音，同时在[陕西方言里的](../Page/陕西方言.md "wikilink")“做啥呢？”这句话的最后一个音与这个[音素其相似](../Page/音素.md "wikilink")。

<!-- end list -->

  - [边音](../Page/边音.md "wikilink")：/l/
  - [颤音](../Page/颤音.md "wikilink")：/ʀ/

<!-- end list -->

  -
    现代法语里面通常发小舌擦音，各地的发音方式略有不同；少数地区和少数情况下也用小舌颤音。有少数法语区如[加拿大的](../Page/加拿大.md "wikilink")[魁北克](../Page/魁北克.md "wikilink")，部分[非洲国家和法国南部少数地区也有仅使用](../Page/非洲.md "wikilink")[大舌音](../Page/大舌音.md "wikilink")。小舌即医学上的[悬雍垂](../Page/悬雍垂.md "wikilink")，大舌即[舌](../Page/舌.md "wikilink")。

<!-- end list -->

  - [半元音](../Page/半元音.md "wikilink")：/j/、/w/、/ɥ/

### 读音规则

世界上的拼音文字可分为不需要音标拼写的直接拼法、需要音标辅助的间接拼法。世界上上绝大多数表音文字都是属于直接拼法，即是拼写都非常规则不使用音标就可以直接正确地拼读出单词，尽管法语的读音规则非常简单，但法语跟英语一样需要音标辅助拼写单词，法语属于间接拼法，当掌握规律后可以不用音标正确拼出单词，拼写比英语规则，通常在普通的法語字典裡佔一页的篇幅，但是法语单词中不发音的字母特别多，同一个字母或字母组合可以发不同的音，不同的字母或字母组合可以发相同的音，看单词一般可以读出正确的发音，但不一定能根据单词的发音正确拼写出单词，人们举例拼写复杂的言语时通常用法语和英语为例。

但是对于使用中文的學習者來說，需要了解一下这些规则：

  - 单词末尾的[辅音字母通常是不发音的](../Page/辅音字母.md "wikilink")，除非其后跟的有[元音字母或同一个辅音字母](../Page/元音字母.md "wikilink")（例如“paquet”、“pas”、“las”结尾的“t”和“s”不发音）。但是，这些辅音字母在[联诵或者](../Page/联诵.md "wikilink")[连音中可能发音](../Page/连音.md "wikilink")。
  - “n”和“m”在元音字母前面发字母音，而在某些元音字母後面并且后面没有元音字母或者“m”或“n”相连的时候与前面的元音构成[鼻化元音](../Page/鼻化元音.md "wikilink")。
  - 另外，以不发音的的辅音字母结尾的法语单词后面紧跟一个元音开头的单词并且与之位于同一个[节奏组中的时候会发生](../Page/节奏组_\(语言\).md "wikilink")[联诵](../Page/联诵.md "wikilink")，同样情况下如果前面的单词以辅音结尾，则会发生[连音](../Page/连音.md "wikilink")。
      - （在上面文字中，请注意辅音、元音与辅音字母、元音字母的区别。）
  - [辅音字母](../Page/辅音字母.md "wikilink")“h”在任何时候都不发音，但在作为单词开头时区分为“哑音”和“嘘音”，词典上一般在嘘音单词前加上“\*”。哑音和嘘音主要分别为哑音开头的词其读音和写法变化和元音开头的单词一样，而嘘音开头的单词的变化则和辅音开头的单词一样，即不能连读，不能省音等。

## 常用词组

  -
    <small>參見：[各地常用會話列表](../Page/各地常用會話列表.md "wikilink")</small>

<div>

|                                                                                                                  |                                                                                              |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| [Gnome-speakernotes.png](https://zh.wikipedia.org/wiki/File:Gnome-speakernotes.png "fig:Gnome-speakernotes.png") | 以下段落含有-{zh-tw:音訊檔案; zh-cn:音频文件}-，若您有收聽方面的問題，請參見[媒體幫助](../Page/Wikipedia:媒體幫助.md "wikilink")。 |

</div>

<table>
<thead>
<tr class="header">
<th><p>中文</p></th>
<th><p>法語</p></th>
<th><p>發音（魁北克口音）</p></th>
<th><p>發音（法國口音）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>法國人/法語</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中國人</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>台灣人</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>新加坡人</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>漢語官話/現代標準漢語/普通話/國語</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>英格蘭人/英語</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>是</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>否</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>你好！</p></td>
<td></td>
<td><p>[bõʒuːʁ]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>晚上好！</p></td>
<td></td>
<td><p>[bõswɑːʁ]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>晚安！</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>再見！</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>祝你有美好的一天！</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>請</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>謝謝</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>不客氣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>抱歉/對不起</p></td>
<td></td>
<td><p>/ </p></td>
<td><p>/ </p></td>
</tr>
<tr class="even">
<td><p>誰？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>什麼？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>何時？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>哪裡？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>為什麼？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>你叫什麼名字？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>因為</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>也许</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>如何？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>多少？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>我不明白。</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>是，我明白。</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>救命！</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>廁所在哪裡？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>您會說中文嗎？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>您會說英语嗎？</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>我很好。</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 语法

关于法語的动词形态变化， 以下举动词 （）“是”为例：

<table>
<tbody>
<tr class="odd">
<td><p>时态<br />
<small>temps</small></p></td>
<td><p>现在时<br />
<small>présent</small></p></td>
<td><p>简单未来时<br />
<small>futur simple</small></p></td>
<td><p>未完成过去时<br />
<small>imparfait</small></p></td>
<td><p>简单过去时<br />
<small>passé simple</small></p></td>
</tr>
<tr class="even">
<td><p>语式<br />
<small>modes</small></p></td>
<td><p>直陈式<br />
<small>indicatif</small></p></td>
<td><p>虚拟式<br />
<small>subjonctif</small></p></td>
<td><p>命令式<br />
<small>impératif</small></p></td>
<td><p>条件式<br />
<small>conditionnel</small></p></td>
</tr>
<tr class="odd">
<td><p>1人称单数<br />
<small>je 我</small></p></td>
<td><p><strong>suis</strong> [sɥi]</p></td>
<td><p><strong>sois</strong></p></td>
<td><p><strong>-</strong></p></td>
<td><p><strong>serais</strong></p></td>
</tr>
<tr class="even">
<td><p>2人称单数<br />
<small>tu 你</small></p></td>
<td><p><strong>es</strong> [ɛ]</p></td>
<td><p><strong>sois</strong></p></td>
<td><p><strong>sois</strong></p></td>
<td><p><strong>serais</strong></p></td>
</tr>
<tr class="odd">
<td><p>3人称单数<br />
<small>il/elle 他/她</small></p></td>
<td><p><strong>est</strong> [ɛ]</p></td>
<td><p><strong>soit</strong></p></td>
<td><p><strong>-</strong></p></td>
<td><p><strong>serait</strong></p></td>
</tr>
<tr class="even">
<td><p>1人称复数<br />
<small>nous 我们</small></p></td>
<td><p><strong>sommes</strong> [sɔm]</p></td>
<td><p><strong>soyons</strong></p></td>
<td><p><strong>soyons</strong></p></td>
<td><p><strong>serions</strong></p></td>
</tr>
<tr class="odd">
<td><p>2人称复数<br />
<small>vous 你们</small></p></td>
<td><p><strong>êtes</strong> [ɛt]</p></td>
<td><p><strong>soyez</strong></p></td>
<td><p><strong>soyez</strong></p></td>
<td><p><strong>seriez</strong></p></td>
</tr>
<tr class="even">
<td><p>3人称复数<br />
<small>ils/elles 他们/她们</small></p></td>
<td><p><strong>sont</strong> [sɔ̃]</p></td>
<td><p><strong>soient</strong></p></td>
<td><p><strong>-</strong></p></td>
<td><p><strong>seraient</strong></p></td>
</tr>
</tbody>
</table>

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《外国习俗丛书·法国》，[世界知识出版社](../Page/世界知识出版社.md "wikilink")，1993年.
    ISBN 7-5012-0542-6.
  - [北京外国语大学](../Page/北京外国语大学.md "wikilink")：《现代汉法，法汉辞典》.

## 外部链接

  - [汉法词典](http://www.hanzidico.com/dictionnaire-francais-anglais-chinois/)
  - [使用Google自动将法语翻译成简体中文](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-CN)
  - [使用Google自动将法语翻译成繁体中文](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-TW)
  - [Académie Française](http://www.academie-francaise.fr/)
  - [法语同义词词典](http://dico.isc.cnrs.fr/dico/fr/chercher)
  - [汉法词典dictionaric](http://www.dictionaric.com/dicochinois/dicochinois.php)
  - [法语十日入门（2014修订版）](http://www.italian.org.cn/app/french.html)
  - [法-英-汉词典（免费软件，有Android和Babylon两个版本）](http://www.italian.org.cn/app/fecd.html)

## 參見

  - [古法語](../Page/古法語.md "wikilink")
  - [中古法語](../Page/中古法語.md "wikilink")
  - [法語圈](../Page/法語圈.md "wikilink")

{{-}}

[法语](../Category/法语.md "wikilink")
[Category:罗曼语族](../Category/罗曼语族.md "wikilink")
[Category:法國語言](../Category/法國語言.md "wikilink")
[Category:比利時語言](../Category/比利時語言.md "wikilink")
[Category:加拿大語言](../Category/加拿大語言.md "wikilink")
[Category:摩纳哥语言](../Category/摩纳哥语言.md "wikilink")
[Category:瑞士语言](../Category/瑞士语言.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:毛里裘斯諸語言](../Category/毛里裘斯諸語言.md "wikilink")
[Category:屈折語](../Category/屈折語.md "wikilink")

1.
2.