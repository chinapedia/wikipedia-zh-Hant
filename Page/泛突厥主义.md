[Turkic_language_map-present_range.png](https://zh.wikipedia.org/wiki/File:Turkic_language_map-present_range.png "fig:Turkic_language_map-present_range.png")
**泛突厥主義**是由十九世紀的[韃靼知識分子萌發的文化和政治統一](../Page/韃靼人.md "wikilink")[民族主義運動](../Page/民族主義.md "wikilink")，與**[圖蘭主義](../Page/圖蘭主義.md "wikilink")**相似。[图蘭低地是在](../Page/图蘭低地.md "wikilink")[伊朗的東北](../Page/伊朗.md "wikilink")，[咸海以东](../Page/咸海.md "wikilink")、以南的一块平原。傳說這裡是一切突厥人的發源地。他們指圖蘭是[突厥人的後代](../Page/突厥人.md "wikilink")。曾有一陣子，土耳其帝國為縮小與歐洲臣民的差距，提出**圖蘭[雅利安人](../Page/雅利安人種.md "wikilink")**的說法，就是指突厥人是圖蘭人後人。圖蘭是[菲尔多西的](../Page/菲尔多西.md "wikilink")[列王纪中提到的与](../Page/列王纪.md "wikilink")[伊朗相对立的另一方](../Page/伊朗.md "wikilink")，大致相当于[伊朗东北的广大区域](../Page/伊朗.md "wikilink")。[匈牙利人提出的圖蘭的範圍则更大](../Page/匈牙利人.md "wikilink")，甚至包括[蒙古](../Page/蒙古.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")。泛突厥主義是用來抵抗[泛斯拉夫主義](../Page/泛斯拉夫主義.md "wikilink")，主張通過教育和文化自治，團结使用[突厥语的民族](../Page/突厥语.md "wikilink")。当时在欧洲也兴起了[民族主义浪潮](../Page/民族主义.md "wikilink")，这极大地刺激了鞑靼知识分子。泛突厥主义的主要目標是在政治、文化、语言上统一突厥民族，建立一个西起[亚得里亚海](../Page/亚得里亚海.md "wikilink")、東至[中國新疆](../Page/中國.md "wikilink")、甘肅和青海的「大突厥国」\[1\]。

最早的泛突厥主義在1804年由一個韃靼神學家[库萨维提出](../Page/库萨维.md "wikilink")，呼籲伊斯蘭教的現代化建設，這是[扎吉德運動](../Page/扎吉德運動.md "wikilink")。1843年開始了世俗教育改革。1880年代，在[伏爾加河](../Page/伏爾加河.md "wikilink")，發展了新伊斯蘭運動。泛突厥主義者認為由土耳其到阿爾泰山有一突厥帶，生活很多突厥人。

俄国境内的鞑靼民族知识分子利用文化认同意识，激发民族主义的團聚力，通过教育和语言改革，試圖将操突厥语的各民族团结成为一个统一的“突厥民族”，以抵制沙俄政府。沙俄鞑靼人[易司马仪·哈斯皮拉里于](../Page/易司马仪·哈斯皮拉里.md "wikilink")1883年明确提出：俄罗斯的突厥人应该“在語言上，在思想上和在行动上联合起来”，他是泛突厥主義之父。其继承人[優素福·阿克楚拉提出把所有](../Page/優素福·阿克楚拉.md "wikilink")[突厥民族合并成为一个统一的民族](../Page/突厥.md "wikilink")。而在土耳其，有一重要人物[茲亞·戈卡爾普](../Page/茲亞·戈卡爾普.md "wikilink")，寫了一本書，名為《突厥主義原理》，提出突厥主義的三階段，第一階段統一操[烏古斯語支的](../Page/烏古斯語支.md "wikilink")[土耳其人](../Page/土耳其人.md "wikilink")、[土庫曼人與](../Page/土庫曼人.md "wikilink")[阿塞拜疆人](../Page/阿塞拜疆人.md "wikilink")，第二階段是操[欽察語支的突厥民族](../Page/欽察語.md "wikilink")，远期理想是[圖兰](../Page/圖兰.md "wikilink")。1908年，[青年土耳其黨人嘗試建立一個突厥人帝國](../Page/青年土耳其黨人.md "wikilink")，取代在歐洲的失地。1917年[十月革命后](../Page/十月革命.md "wikilink")，哈斯皮拉里等流亡[土耳其](../Page/土耳其.md "wikilink")。于是，“泛突厥主义”被一些[鄂圖曼帝国的知识分子接手](../Page/鄂圖曼帝国.md "wikilink")，改变成恢复鄂圖曼帝国昔日强大辉煌的民族复兴运动的精神支柱，并向世界传播。其中一個特別提倡泛突厥主義的人物是[恩维尔·帕夏](../Page/恩维尔·帕夏.md "wikilink")，鄂圖曼的战争部长和署理总司令。在第一次世界大战之后，他成为在中亞發生的[巴斯馬奇起义的领导者之一](../Page/巴斯馬奇起义.md "wikilink")，反抗俄罗斯帝国和苏联的统治。
[Map-TurkicLanguages.png](https://zh.wikipedia.org/wiki/File:Map-TurkicLanguages.png "fig:Map-TurkicLanguages.png")
在蘇聯時代與[凱末爾時代](../Page/凱末爾.md "wikilink")，泛突厥主義受壓抑，在第二次世界大戰時期又被提出，納粹黨人[阿尔弗雷德·羅森堡曾經想在](../Page/阿尔弗雷德·羅森堡.md "wikilink")[中亞設一](../Page/中亞.md "wikilink")[突厥斯坦总督辖區](../Page/突厥斯坦总督辖區.md "wikilink")，分割蘇聯。

泛突厥主义在中國[新疆有一定影響](../Page/新疆.md "wikilink")，泛突厥主義者一直稱新疆為[東突厥斯坦](../Page/東突厥斯坦.md "wikilink")。

現時許多新的泛突厥主義者把注意力集中于建立一個經濟一體化的突厥聯盟，由各個突厥裔人國家組成，希望形成一個類似[歐洲聯盟的經濟及政治聯盟](../Page/歐洲聯盟.md "wikilink")。2000年，土耳其主管中亞經貿事務的部長提出仿效[阿拉伯聯盟體制建立一個突厥國家聯盟](../Page/阿拉伯聯盟.md "wikilink")，受到[俄羅斯的嚴重警告和反對](../Page/俄羅斯.md "wikilink")。

說：泛突厥主義有文化與政治二種，[政治上失敗了](../Page/政治.md "wikilink")，但[文化](../Page/文化.md "wikilink")[沙文主義上仍在進行](../Page/沙文主義.md "wikilink")。

## 參考文獻

### 引用

### 来源

  - [伯納德·劉易斯](../Page/伯納德·劉易斯.md "wikilink")《現代土耳其的興起》〈中亞的民族關係－歷史、現狀與前景〉

{{-}}

[Category:极右派政治](../Category/极右派政治.md "wikilink")
[泛突厥主义](../Category/泛突厥主义.md "wikilink")
[Category:民族統一主義](../Category/民族統一主義.md "wikilink")
[Category:泛運動](../Category/泛運動.md "wikilink")
[Category:亞洲政治運動](../Category/亞洲政治運動.md "wikilink")

1.