**波兰犹太人的历史**长达一个[千禧年](../Page/千禧年.md "wikilink")，既经历过漫长的[宗教宽容时期](../Page/宗教宽容.md "wikilink")，该国的[犹太人群体繁荣昌盛](../Page/犹太人.md "wikilink")；也在20世纪[纳粹德国占领](../Page/纳粹德国.md "wikilink")[波兰期间](../Page/波兰.md "wikilink")，经历了[犹太人大屠杀](../Page/犹太人大屠杀.md "wikilink")，整个群体几乎遭受了彻底的[种族灭绝](../Page/种族灭绝.md "wikilink")。

自从10世纪[波兰王国创建](../Page/波兰王国_\(1025–1138\).md "wikilink")，以及1569年成立[波兰立陶宛联邦以来](../Page/波兰立陶宛联邦.md "wikilink")，[波兰是](../Page/波兰.md "wikilink")[欧洲最宽容的国家之一](../Page/欧洲.md "wikilink")。它拥有世界最大、最活跃的犹太人群体之一。[波兰立陶宛联邦由于外部侵略和内部文化变迁而逐漸衰落下来](../Page/波兰立陶宛联邦.md "wikilink")，[新教的](../Page/新教.md "wikilink")[宗教改革和](../Page/宗教改革.md "wikilink")[天主教的](../Page/天主教.md "wikilink")[反宗教改革](../Page/反宗教改革.md "wikilink")，使波兰宽容的传统开始。1795年[瓜分波兰以后](../Page/瓜分波兰.md "wikilink")，波兰被列强瓜分。

波兰在20世纪赢得独立，[第二次世界大战前夕](../Page/第二次世界大战.md "wikilink")，波兰拥有活跃的犹太人群体，總人數超过300万。在犹太人大屠杀期间，超过90%的波兰犹太人被纳粹德国杀害。

纳粹在第二次世界大战中对犹太人犯下了滔天罪行，波兰犹太人只有1/12得以逃生。战火熄灭后，波蘭政府将猶太人的土地、房屋等财产分给了波兰人。为避免让已属于自己的财物“物还其主”，一些波兰人便对刚刚逃离战火、满怀希望返回家园的犹太人举起了屠刀。从纳粹淫威下“十二死一生”侥幸存活下来的犹太人早已失去了反抗能力，他们或远走他乡，或逃到深山野外隐居起来。

从40年代后期到50年代中期，“排外”的阴霾一直盘旋在战火乍熄的波兰大地上。“一部分别有用心的波兰人，将第三帝国对犹太人的暴行裹上民主的斗篷承接过来。”

1945-1946年，克拉科夫等地曾发生了至少50次谋杀犹太人的案件，被谋杀者都是大屠杀中的幸存者，谋杀者施暴的目的是阻止那些犹太人重返家园。这一切促成了1945-1947年和1956-1957年两次波兰犹太人向海外移民的浪潮。有人认为，波兰犹太人历史研究所，实际上是在波兰几乎没有犹太人的情况下建立起来的。

1968年波兰又爆发了新一轮反犹浪潮，波兰犹太人口剧减，大部分犹太人都逃离了波兰。1989年，[共产党政权在波兰](../Page/共产党.md "wikilink")，波兰犹太人的处境已经正常化了。

今天在波兰，反犹太主义的水平可与其它[欧洲国家诸如](../Page/欧洲.md "wikilink")[意大利](../Page/意大利.md "wikilink")、[西班牙和](../Page/西班牙.md "wikilink")[德国相比](../Page/德国.md "wikilink")。当代的波兰犹太人社區估计有大约8千到1萬2千名成員。

## 参考资料

  - [Marek Jan
    Chodakiewicz](../Page/Marek_Jan_Chodakiewicz.md "wikilink"), *After
    the Holocaust*, East European Monographs, 2003, ISBN 0880335114.
  - [Marek Jan
    Chodakiewicz](../Page/Marek_Jan_Chodakiewicz.md "wikilink"),
    *Between Nazis and Soviets: Occupation Politics in Poland,
    1939–1947*, Lexington Books, 2004, ISBN 0739104845.
  - [William W. Hagen](../Page/William_W._Hagen.md "wikilink"), Before
    the "Final Solution": Toward a Comparative Analysis of Political
    Anti-Semitism in Interwar Germany and Poland, [The Journal of Modern
    History](../Page/The_Journal_of_Modern_History.md "wikilink"), Vol.
    68, No. 2 (Jun., 1996), 351–381.
  - [Gershon David
    Hundert](../Page/Gershon_David_Hundert.md "wikilink"), *Jews in
    Poland-Lithuania in the Eighteenth Century: A Genealogy of
    Modernity*, University of California Press, 2004, ISBN 0520238443
    [Google
    Print](http://archive.wikiwix.com/cache/20060628091628/http://print.google.com/print%3Fq%3DLiberty%27s%2Bfolly:%2Bthe%2BPolish-Lithuanian%2Bcommonwealth%2Bin%2Bthe%2Beighteenth%2Bcentury%26lr%3D%26start%3D10)
  - [Antony Polonsky](../Page/Antony_Polonsky.md "wikilink") and [Joanna
    B. Michlic](../Page/Joanna_B._Michlic.md "wikilink"). *The Neighbors
    Respond: The Controversy over the Jedwabne Massacre in Poland*,
    Princeton University Press, 2003 ISBN 0691113068. ([The introduction
    is
    online](https://web.archive.org/web/20041228113559/http://www.pupress.princeton.edu/chapters/i7632.html))
  - [Iwo Cyprian
    Pogonowski](../Page/Iwo_Cyprian_Pogonowski.md "wikilink"), *Jews in
    Poland. A Documentary History*, Hippocrene Books, Inc., 1998,
    ISBN 0781806046.
  - [David Vital](../Page/David_Vital.md "wikilink"), *A People Apart: A
    Political History of the Jews in Europe 1789-1939*, Oxford
    University Press, 2001.
  - [M. J. Rosman](../Page/M._J._Rosman.md "wikilink"), *The Lord's
    Jews: Magnate-Jewish Relations in the Polish-Lithuanian Commonwealth
    During the Eighteenth Century*, Harvard University Press, 1990,
    ISBN 0916458180

## 延伸阅读

  - [Alvydas Nikzentaitis](../Page/Alvydas_Nikzentaitis.md "wikilink"),
    [Stefan Schreiner](../Page/Stefan_Schreiner.md "wikilink"), [Darius
    Staliunas](../Page/Darius_Staliunas.md "wikilink") (editors). *The
    Vanished World of Lithuanian Jews*. Rodopi, 2004, ISBN 9042008504
    [Google
    print](http://archive.wikiwix.com/cache/20060628165954/http://print.google.com/print%3Flr%3D%26q%3DPolish-Lithuanian%2Bcommonwealth%26start%3D20)

## 外部链接

  - [Extermination of Jews in Poland during the
    Holocaust](http://isurvived.org/TOC-VI.html#Poles-Jews) from
    Holocaust Survivors' Network—iSurvived.org

[Category:欧洲犹太史](../Category/欧洲犹太史.md "wikilink")
[Category:波兰宗教史](../Category/波兰宗教史.md "wikilink")
[Category:波兰民族史](../Category/波兰民族史.md "wikilink")
[Category:波蘭猶太人](../Category/波蘭猶太人.md "wikilink")