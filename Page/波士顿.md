**波士頓**（）是[美国](../Page/美国.md "wikilink")[马萨诸塞州的首府和最大城市](../Page/马萨诸塞州.md "wikilink")，也是[新英格兰地区的最大城市](../Page/新英格兰.md "wikilink")，其人口规模全美大都市排名第21\[1\]。该市位于美国东北部大西洋沿岸，创建于1630年，是美国最古老、最具有历史文化价值的城市之一。波士顿是欧洲清教徒移民最早登陆美洲所建立的城市，在[美国革命期间是许多重要事件的发源地](../Page/美国革命.md "wikilink")，曾经是一个重要的航运港口和制造业中心。今天，该市是美国高等教育、医疗保健及投资基金的中心，是全美人口受教育程度最高的城市之一，它的经济基础是教育、金融、医疗及科技，是全美人均收入最高的少数大城市之一，并被认为是一个全球性城市或世界性城市。\[2\]

该市位于[大波士顿都会区的中心](../Page/大波士顿.md "wikilink")，这个都会区包括[萨福克县的全部和](../Page/萨福克县.md "wikilink")[剑桥](../Page/剑桥_\(马萨诸塞州\).md "wikilink")、[昆西](../Page/昆西_\(马萨诸塞州\).md "wikilink")、[牛顿](../Page/牛顿_\(马萨诸塞州\).md "wikilink")、[萨默维尔](../Page/萨默维尔_\(马萨诸塞州\).md "wikilink")、[里维尔和](../Page/里维尔_\(马萨诸塞州\).md "wikilink")[切尔西等城市](../Page/切尔西_\(马萨诸塞州\).md "wikilink")，以及一些小镇和远离波士顿的郊区，还包括了[新罕布什尔州的一部分](../Page/新罕布什尔州.md "wikilink")。\[3\]

## 历史

[Boston_1630_1675.jpg](https://zh.wikipedia.org/wiki/File:Boston_1630_1675.jpg "fig:Boston_1630_1675.jpg")
[North_End,_Boston.jpg](https://zh.wikipedia.org/wiki/File:North_End,_Boston.jpg "fig:North_End,_Boston.jpg")
[Situationsplan_von_Boston_(Massachusetts).jpg](https://zh.wikipedia.org/wiki/File:Situationsplan_von_Boston_\(Massachusetts\).jpg "fig:Situationsplan_von_Boston_(Massachusetts).jpg")
[Old_Mass_State_House.JPG](https://zh.wikipedia.org/wiki/File:Old_Mass_State_House.JPG "fig:Old_Mass_State_House.JPG")

1630年9月7日，来自英国的[清教徒移民创建了波士顿](../Page/清教徒.md "wikilink")。\[4\]波士顿所属的[马萨诸塞湾殖民地和附近](../Page/马萨诸塞湾殖民地.md "wikilink")60公里外在10年前（1620年11月21日）建立的[普利茅斯殖民地](../Page/普利茅斯殖民地.md "wikilink")（位于今天的[布里斯托尔县](../Page/布里斯托縣_\(麻薩諸塞州\).md "wikilink")、[普利茅斯县和](../Page/普利茅斯縣_\(麻薩諸塞州\).md "wikilink")[巴恩斯特布尔县](../Page/巴恩斯特布爾縣.md "wikilink")）这2个清教徒团体在宗教实行方面截然不同，互相不太有来往。这2个分开的殖民地直到1691年才联合起来，形成[马萨诸塞湾省](../Page/马萨诸塞湾省.md "wikilink")。

波士顿建造在一个[半岛上](../Page/半岛.md "wikilink")，
通过一个狭窄的[地峡与大陆相连](../Page/地峡.md "wikilink")，并被[马萨诸塞湾和后湾](../Page/马萨诸塞湾.md "wikilink")--[查尔斯河的](../Page/查尔斯河.md "wikilink")[河口所环绕](../Page/河口.md "wikilink")。波士顿的早期欧洲移民最初根据这里的三座小山丘命名为三山城（Trimountaine）；后来根据移民中一些杰出人物的家乡--[英格兰](../Page/英格兰.md "wikilink")[波士顿
(林肯郡)的名称重新命名](../Page/波士顿_\(林肯郡\).md "wikilink")。当时，波士顿的居民绝大多数是来自英国的清教徒。[马萨诸塞湾殖民地最初的统治者](../Page/马萨诸塞湾殖民地.md "wikilink")[约翰·温斯罗普发表过一篇著名的布道词](../Page/约翰·温斯罗普.md "wikilink")，题为“[基督徒慈善的典范](../Page/山上的城.md "wikilink")”，认为波士顿与上帝之间存在着特别的契约。温斯罗普还带动了[剑桥协定的签署](../Page/剑桥协定.md "wikilink")，这份协定被认为是创建该市的关键文献。清教徒的道德规范在波士顿塑造了一个极端稳定、结构良好的社会。例如，在波士顿建立不久，清教徒就创立了美国第一所公立学校[波士顿拉丁学校](../Page/波士顿拉丁学校.md "wikilink")（1635年），和美国第一所大学[哈佛大学](../Page/哈佛大学.md "wikilink")（1636年）。直到今天，努力工作、道德正直、重视教育仍然是波士顿文化的一部分。

1770年代初，英国人尝试全面控制[北美十三州](../Page/北美十三州.md "wikilink")，主要通过征税，促使波士顿人发动了[美国革命](../Page/美國革命.md "wikilink")。[波士顿屠杀](../Page/波士顿屠杀.md "wikilink")、[波士顿茶叶事件](../Page/波士頓茶葉事件.md "wikilink")，以及美国革命几个早期的战役——[列克星敦和康科德战役](../Page/列克星敦和康科德战役.md "wikilink")、[邦克山战役和](../Page/邦克山战役.md "wikilink")[波士顿围城战](../Page/波士顿围城战.md "wikilink")，都发生在该市或附近的地方。[保羅·列维尔著名的午夜骑行也发生在这一时期](../Page/保羅·列维尔.md "wikilink")。

美国革命以后一段时期内，由于波士顿是美国距离欧洲最近的一个主要港口，因而它迅速发展了海外贸易，向欧洲出口[朗姆酒](../Page/兰姆酒.md "wikilink")、[鱼](../Page/鱼.md "wikilink")、[食盐和](../Page/食盐.md "wikilink")[烟草](../Page/烟草.md "wikilink")，成为当时世界上最富裕的国际商港之一。这一时期，一些波士顿名门世家的后代成为了美国社会文化的精英；他们后来被称为*[波士顿婆罗门](../Page/波士顿婆罗门.md "wikilink")*。1822年，波士顿得到特许状，正式取得了城市身份。

19世纪中叶，波士顿的制造业在国际贸易上占有一定的重要性。此后直到20世纪初，波士顿仍然是美国最大的制造业中心之一，其中特别以[服装](../Page/服装.md "wikilink")、[皮革制品和机械工业著称](../Page/皮革.md "wikilink")。该市通过附近的小型河流连接了附近地区，方便的货运使工厂的增设成为可能。后来，密集的铁路网更加促进了该地区的工商业繁荣。从19世纪中后期起，波士顿的文化开始繁荣起来，以慷慨的艺术捐助者著称。这一时期的波士顿还成了[废奴主义运动的一个中心](../Page/废奴主义.md "wikilink")。

1820年代，由于第一波欧洲移民潮的到来，波士顿的人口构成开始发生戏剧性的变化。大批[爱尔兰人和](../Page/爱尔兰人.md "wikilink")[意大利人移居该市](../Page/意大利人.md "wikilink")，将他们的[天主教也带到这座城市](../Page/天主教.md "wikilink")。目前，天主教是波士顿最大的宗教团体，从20世纪初起，爱尔兰人开始在波士顿政治中扮演重要角色，[肯尼迪家族就是其中的杰出代表之一](../Page/肯尼迪家族.md "wikilink")。

从1630年到1890年，通过填平沼泽、海滨泥滩和码头之间的缝隙，波士顿的城市规模扩大了三倍。\[5\]在19世纪进行了巨大的“削山填海”的改造工程。1807年开始，灯塔山的山顶被用于填平一个面积50[英亩](../Page/英亩.md "wikilink")（20[公顷](../Page/公顷.md "wikilink")）的池塘，后来那里形成了[干草市场广场](../Page/干草市场广场.md "wikilink")。今天，[马萨诸塞州政府就坐落在变矮后的灯塔山山顶](../Page/马萨诸塞州政府.md "wikilink")。19世纪中叶的工程形成了[波士顿南区](../Page/波士顿南区.md "wikilink")、[波士顿西区](../Page/波士顿西区.md "wikilink")、金融区、和[波士顿唐人街](../Page/波士顿唐人街.md "wikilink")。[1872年波士顿大火之后](../Page/1872年波士顿大火.md "wikilink")，工人用碎石重建了濒水的市中心地区。在19世纪中后期，工人们用铁轨从尼达姆高地取来泥土，填平了波士顿西面大约600英亩（2.4平方公里）的查尔斯河沼泽地。另外，该市陆续合并了附近的市镇——洛克斯伯里（1868年）、多尔切斯特（1870年）、布莱顿、西洛克斯伯里、查尔斯镇和[海德公园](../Page/海德公园_\(马萨诸塞\).md "wikilink")。1874年，波士顿最后一次合并了3个镇，从此市区界限固定了下来，未再变动。\[6\]

20世纪早期和中期，波士顿由于工厂陈旧老化开始衰落，工厂纷纷迁往劳动力更低廉的地区。波士顿做出了反应，开始建造各种城市更新项目，包括拆除业已陈旧的西区，和建造[波士顿政府中心](../Page/波士顿政府中心.md "wikilink")。1970年代，波士顿在长达30年的经济低迷时期之后恢复了繁荣，并带动了[共同基金的发展](../Page/共同基金.md "wikilink")。波士顿因其卓越的医疗服务享有盛誉，[麻省总医院](../Page/麻省总医院.md "wikilink")、[贝斯以色列女执事医疗中心和](../Page/贝斯以色列女执事医疗中心.md "wikilink")[布莱根妇女医院等医院在疾病治疗和医疗创新方面在全美国领先](../Page/布莱根妇女医院.md "wikilink")。[哈佛大学](../Page/哈佛大学.md "wikilink")、[麻省理工学院](../Page/麻省理工学院.md "wikilink")、[波士顿学院](../Page/波士顿学院.md "wikilink")、[东北大学和](../Page/东北大学_\(美国\).md "wikilink")[波士顿大学等高等学府吸引了许多学生来到波士顿都会区求学深造](../Page/波士顿大学.md "wikilink")。

21世纪初，波士顿已经成为一个智力、技术与政治思想的中心。不过，该市还必须解决1990年代以来因房价急剧上涨造成生活费用过高的问题。2004年，波士顿大都会区已经成为全美国生活开支最高的地方，而马萨诸塞州是全国唯一人口下降的州。\[7\]

## 地理

[Boston_2013-6-2.jpg](https://zh.wikipedia.org/wiki/File:Boston_2013-6-2.jpg "fig:Boston_2013-6-2.jpg")
由于開發较早的缘故，波士顿人口密度非常高。根据[美国人口调查局资料](../Page/美国人口调查局.md "wikilink")，该市的总面积为89.6[平方英里](../Page/平方英里.md "wikilink")（232.1平方公里）；其中陆地面积为48.4平方英里（125.4平方公里），水域面积为41.2平方英里（106.7平方公里，占46.0%）。而其它许多人口相仿的城市，如[丹佛有](../Page/丹佛.md "wikilink")154.9平方英里，而[夏洛特有](../Page/夏洛特_\(北卡羅萊那州\).md "wikilink")280.5平方英里。在50万人口以上的美国城市中，只有[旧金山和](../Page/旧金山.md "wikilink")[华盛顿的面积小于波士顿](../Page/华盛顿.md "wikilink")。

波士顿的海拔高度以[洛根国际机场为标准是](../Page/洛根国际机场.md "wikilink")19英尺（5.8米）。\[8\]波士顿的最高点是贝勒维尔山，海拔330英尺（101米）。\[9\]与波士顿毗邻的城镇有[溫索普](../Page/溫索普_\(马萨诸塞州\).md "wikilink")、[里维尔](../Page/里维尔_\(马萨诸塞州\).md "wikilink")、[切尔西](../Page/切尔西_\(马萨诸塞州\).md "wikilink")、[艾弗瑞](../Page/艾弗瑞_\(马萨诸塞州\).md "wikilink")、[萨默维尔](../Page/萨默维尔_\(马萨诸塞州\).md "wikilink")、[剑桥](../Page/剑桥_\(马萨诸塞州\).md "wikilink")、[水城](../Page/水城_\(马萨诸塞州\).md "wikilink")、[牛顿](../Page/牛顿_\(马萨诸塞州\).md "wikilink")、[布鲁克兰](../Page/布鲁克兰_\(马萨诸塞州\).md "wikilink")、[尼达姆](../Page/尼达姆_\(马萨诸塞州\).md "wikilink")、[戴得汉姆](../Page/戴得汉姆_\(马萨诸塞州\).md "wikilink")、[坎顿](../Page/坎顿_\(马萨诸塞州\).md "wikilink")、[米尔顿和](../Page/米尔顿_\(马萨诸塞州\).md "wikilink")[昆西](../Page/昆西_\(马萨诸塞州\).md "wikilink")，通常都被认为是[大波士顿的一部分](../Page/大波士顿.md "wikilink")。

[后湾和](../Page/后湾.md "wikilink")[波士顿南区的大部分是用垃圾碎石填海形成的新生地](../Page/波士顿南区.md "wikilink")，面积是波士顿最初的3座小山区域的2倍半。最初的3座小山中只有最小的[灯塔山被削低以后被保留了部分](../Page/灯塔山.md "wikilink")。市中心区域及近邻地区主要由低层砖砌或石砌建筑组成，有许多联邦建筑风格的古老建筑，其中一些与现代高层建筑混杂在一起，特别是在金融区、政府中心、南波士顿滨水地区和后湾，有[波士顿公共图书馆](../Page/波士顿公共图书馆.md "wikilink")、[基督教科學會教堂](../Page/基督教科學會.md "wikilink")、[科普来广场](../Page/科普来广场.md "wikilink")、[纽伯里街等许多杰出的地标建筑](../Page/纽伯里街.md "wikilink")，还有新英格兰最高的2座建筑：[约翰·汉考克塔楼和](../Page/约翰·汉考克塔楼.md "wikilink")[慎重塔楼](../Page/慎重塔楼.md "wikilink")\[10\]。约翰·汉考克塔楼的近旁有伯克莱大厦（老约翰·汉考克大厦）及其醒目的[气象塔](../Page/气象塔.md "wikilink")。在由单户居住的别墅和多户居住的砖木结构的联排式房屋构成的住宅区中，则散布着一些较小的商业区。目前，南区历史地区仍保留着美国规模最大的维多利亚时期街区。\[11\]

[波士顿公园](../Page/波士顿公园.md "wikilink")（Boston
Common）位于金融区和灯塔山附近，是美国最古老的公园\[12\]。连同附近的波士顿公共花园，都属于“翡翠项链”（一系列环绕城市的公园）的一部分，并且都是由[弗雷德里克·劳·奥姆斯泰德所设计](../Page/弗雷德里克·劳·奥姆斯泰德.md "wikilink")。主要公园还有沿着[查尔斯河的河岸休闲公园](../Page/查尔斯河.md "wikilink")。其他公园则散布全市。主要的公园与海滩都靠近查尔斯镇、[城堡岛](../Page/城堡岛.md "wikilink")，或沿着多尔切斯特、南波士顿和东波士顿的海岸线。该市最大的公园是弗兰克林公园，包括动物园、[阿诺德植物园](../Page/阿诺德植物园.md "wikilink")，和石溪国家保留地。

查尔斯河将波士顿市区和剑桥、水城以及邻近的查尔斯镇分隔开来，向东面是[波士顿港和](../Page/波士顿港.md "wikilink")[波士顿港岛国家休闲区](../Page/波士顿港岛国家休闲区.md "wikilink")。尼本赛特河构成波士顿南区与昆西、米尔顿2市的界河。神秘河则将查尔斯镇与切尔西和艾弗瑞分开，而切尔西溪与波士顿港将波士顿和东波士顿分开。

## 气候

波士顿和整个[新英格兰地区一样](../Page/新英格蘭.md "wikilink")，属于[大陆性气候](../Page/大陆性气候.md "wikilink")。但是由于濒临[大西洋的地理位置](../Page/大西洋.md "wikilink")，气候也明显受到海洋的影响。冬季较冷，微潮，时而偏向寒冷，日最高气温低于或等于的平均日数为25天，日最低气温低于或等于的平均日数为14天，低于或等于的平均日数为2.8天；夏季相对炎热潮湿，日最高气温超过的日数年均有28天，超过的有2.9天。\[13\]最冷月（1月）均温，极端最低气温（1934年2月9日）。最热月（7月）均温，极端最高气温（1911年7月4日）。无霜期平均为217天（4月6日至11月8日）；可测量降雪平均期为11月29日至3月27日。\[14\]年均降水量约，年极端最少降水量为（1965年），最多为（1878年）。\[15\]年均降雪量为（西郊平均降雪量可达150厘米以上），但降雪年际变化较大；1936–37年的降雪量最少，积累降雪量只有，1995–96年的降雪量最多，积累降雪量为。\[16\]

雾为常见，特别是在春季与初夏。热带风暴或[飓风偶尔也会袭击该地区](../Page/飓风.md "wikilink")，特别是在初秋。马萨诸塞伸入[北大西洋的地理位置也使它极易受东北风影响](../Page/大西洋.md "wikilink")，一次暴风雪就可能降雪超过25厘米。由于该市的海岸位置，海风为常见，导致市区气温有时远低于西郊气温，甚至导致气温中午之前降下10℃。\[17\]\[18\]

## 人口

根据2012年美国人口统计\[19\]，该市拥有636,479人口，272,481个住户，和248,704个家庭。[人口密度为](../Page/人口密度.md "wikilink")12,792.7人/平方英里（4,697人/平方千米）。住户平均密度为5,203/平方英里（2,009/km²）。不过，波士顿白天的人口将增加到大约120万人。人口的起伏是由于郊区居民到市区上班、上学、就医，以及从事其它事情。\[20\]

根据人口统计，该市的种族构成为[白人占](../Page/白色人种.md "wikilink")53.9%，黑人占24.4%，印第安人占0.4%，[亚裔美国人占](../Page/亞裔美國人.md "wikilink")8.9%，0.06%为太平洋群岛人，7.83%为其它，3.9%为2个或2个以上种族的混血。17.5%的人口为[西班牙裔美国人或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人](../Page/拉丁美洲人.md "wikilink")，可能是任何一个种族。（一些研究估计在马萨诸塞州有25万名巴西人，因此对这些数据还需要进行更深入的研究，以精确地进行各族裔的人口统计）。

[Boston_area_income.png](https://zh.wikipedia.org/wiki/File:Boston_area_income.png "fig:Boston_area_income.png")
[爱尔兰裔是该市最大的族裔群体](../Page/爱尔兰裔美国人.md "wikilink")，占人口的15.8%。[意大利人也构成该市人口的重要部分](../Page/意大利人.md "wikilink")，占人口的8.3%。[西印度群岛人后裔也有一定的规模](../Page/西印度群岛.md "wikilink")（6.4%）；其中一半是[海地裔](../Page/海地.md "wikilink")。在多尔切斯特等一些地方，在过去几年中接受了流入的[越南人居民](../Page/京族.md "wikilink")。

在2272,481个住户中，有22.7%的家庭里有18岁以下的孩子同住，有27.4%的家庭只有一对夫妇住在一起，有16.4%的家庭户主为女性，没有丈夫同住，51.9%为非家庭。37.1%的住户为一个人独居，9.1%的住户为65岁以上的人独自居住。平均住户规模为2.31人，而平均家庭规模为3.17人。

该市人口中，低于18岁者占19.8%，18-24岁占16.2%，25-44岁占35.8%，45-64岁占17.8%，65岁以上者占10.4%。年龄中值为31岁。男女性别比为92.8：100，在18岁以上人群中，男女性别比为90.2：100。

该市住户收入[中值为](../Page/中位數.md "wikilink")33,589美元，而家庭收入中值为53.136美元。男性收入中值为37,435美元，而女性收入中值为32,421美元。该市的[个人平均所得为](../Page/个人平均所得.md "wikilink")23,353美元。21.2%的人口和15.3%的家庭生活低于[贫困线](../Page/貧窮門檻.md "wikilink")。其中25.6%为18岁以下人口，18.2%为65岁以上人口。

## 政治

[MassStateHouse.jpg](https://zh.wikipedia.org/wiki/File:MassStateHouse.jpg "fig:MassStateHouse.jpg")
波士顿实行实权市长制度，市长被授予广泛的行政权力，每四年选举一次。市议会每2年选举一次，有9个席位按照每个选区进行多数投票，另有4个席位进行普选，每个投票者直接对普选候选人进行投票。每个候选人只能投票一次。得票最高的4名候选人当选。议长由议员们选举产生。学校委员会由市长指定。自1930年代以來市長一直由民主黨人出任。

作为马萨诸塞州首府，波士顿在马萨诸塞州政治生活中扮演重要角色。波士顿还是[美国联邦政府在新英格兰的中心](../Page/美国联邦政府.md "wikilink")，建有约翰·肯尼迪联邦办公大楼和托马斯·奥尼尔联邦大厦。该市还设有[美国联邦第一巡回上诉法院](../Page/美国联邦第一巡回上诉法院.md "wikilink")、马萨诸塞[地区法院](../Page/地区法院.md "wikilink")，以及波士顿[联邦储备银行](../Page/联邦储备银行.md "wikilink")（联储第一区）总部。该市属于[美国众议院马萨诸塞州第](../Page/美国众议院.md "wikilink")8选区和第9选区。

## 经济

2008年，大波士顿都市区GDP达到3630亿美元，是美国第6大，世界第12大城市经济体（["Global city GDP
rankings 2008–2025". Pricewaterhouse
Coopers](https://web.archive.org/web/20110513194342/https://www.ukmediacentre.pwc.com/Content/Detail.asp?ReleaseID=3421&NewsAreaID=2)）。

波士顿的大学是影响该市和整个区域经济的主要因素。他们不仅是主要的雇主，而且将高技术产业吸引到该市及附近地区，包括计算机硬件与软件公司，以及生物工程公司（如[千禧年医药](../Page/千禧年医药.md "wikilink")）。波士顿每年从国家健康协会得到的资金是所有美国城市中最多的。\[21\]

其他重要产业有[金融业](../Page/金融.md "wikilink")（特别是[共同基金](../Page/共同基金.md "wikilink")）和[保险业](../Page/保险业.md "wikilink")。以波士顿为基地的[富达投资](../Page/富达投资.md "wikilink")（Fidelity）在1980年代帮助普及共同基金，使得波士顿成为美国的顶级金融城市之一。该市还拥有主要银行的地区总部如[美洲银行和](../Page/美國銀行.md "wikilink")[美國桑坦德銀行](../Page/桑坦德銀行_\(美國\).md "wikilink")（Santander）。波士顿还是一个印刷与出版业中心。教科书出版商[霍顿·米夫林出版社的总部设在市内](../Page/霍顿·米夫林出版社.md "wikilink")。该市拥有4个主要的[会议中心](../Page/会议中心.md "wikilink")：[海恩斯会议中心在后湾](../Page/海恩斯会议中心.md "wikilink")，贝赛德博览中心在多尔切斯特，波士顿世界贸易中心和[波士顿会议展览中心位于南波士顿的濒水地区](../Page/波士顿会议展览中心.md "wikilink")。由于它拥有州首府和联邦政府地区中心的地位，法律与政府是该市经济的另一个主要成分。

总部位于该市的大公司有[吉列公司](../Page/吉列公司.md "wikilink")（由[宝洁公司拥有](../Page/宝洁公司.md "wikilink")）和[泰瑞达公司](../Page/泰瑞达公司.md "wikilink")（Teradyne），后者是世界顶级的半导体和其他电子试验设备制造商之一。[新百伦](../Page/新百伦.md "wikilink")（New
Balance）将总部设在波士顿。其他大公司则将总部设在市区以外，主要是沿着[马萨诸塞128号公路](../Page/马萨诸塞128号公路.md "wikilink")。[波士顿港是美国东海岸的主要海港之一](../Page/波士顿港.md "wikilink")，也是[西半球最古老的仍然活跃的商港和渔港之一](../Page/西半球.md "wikilink")。

## 社会

### 媒体

[ISH_WC_Boston2.jpg](https://zh.wikipedia.org/wiki/File:ISH_WC_Boston2.jpg "fig:ISH_WC_Boston2.jpg")
《[波士顿环球报](../Page/波士顿环球报.md "wikilink")》和《[波士顿先驱报](../Page/波士顿先驱报.md "wikilink")》是波士顿主要的2份[日报](../Page/日报.md "wikilink")。该市的其他出版物还有《[波士顿凤凰报](../Page/波士顿凤凰报.md "wikilink")》、《[波士顿杂志](../Page/波士顿杂志.md "wikilink")》、《[每周深入报道](../Page/每周深入报道.md "wikilink")》、《[不合时宜的波士顿人](../Page/不合时宜的波士顿人.md "wikilink")》和《Metro国际》的波士顿版。《波士顿环球报》还专为该市的公立中学发行了一种学生版季度刊物。报纸《Teens
in Print》（T.i.P.）由该市十几岁的孩子写作，每季度发行一期。

波士顿拥有新英格兰最大的广播市场，在全美国排名第11位。\[22\]主要的[调幅电台有谈话台WRKO](../Page/振幅調變.md "wikilink")
680 AM、体育台/谈话台WEEI 850 AM，以及新闻台WBZ 1030
AM。多种[调频电台服务该地区](../Page/频率调制.md "wikilink")，例如国家公共广播（NPR）。大学广播电台有WZBC（波士顿学院）、WERS（爱默森大学）、WUMB（UMass
Boston）和WMFO（塔夫脱大学）。

划给波士顿电视台的指定市场区域（DMA）包括[曼彻斯特
(新罕布什尔州)](../Page/曼彻斯特_\(新罕布什尔州\).md "wikilink")，是美国第七大市场。\[23\]在波士顿经营电视业务的有[哥伦比亚广播公司](../Page/哥倫比亞廣播公司.md "wikilink")（CBS）的WBZ
4、[美国广播公司](../Page/美国广播公司.md "wikilink")（ABC）的WCVB
5、[国家广播公司](../Page/国家广播公司.md "wikilink")（NBC）的WHDH
7、[福克斯广播公司的WFXT](../Page/福克斯广播公司.md "wikilink")
25等。波士顿还是[美国公共广播公司](../Page/美国公共广播公司.md "wikilink")（PBS）的WGBH
2节目的主要制作地点。大部分波士顿电视台都将发射台设在尼达姆和牛顿附近。

### 医疗

[长木医学区是波士顿地区医疗与研究设施的集中地](../Page/长木医学区.md "wikilink")，包括[贝斯以色列女执事医疗中心](../Page/贝斯以色列女执事医疗中心.md "wikilink")、[布莱根妇女医院](../Page/布莱根妇女医院.md "wikilink")、[波士顿儿童医院](../Page/波士顿儿童医院.md "wikilink")、达纳-法伯癌症研究所、[哈佛医学院](../Page/哈佛医学院.md "wikilink")。\[24\]贝斯以色列医疗中心和布莱根妇女医院都经历过合并：前者由贝斯以色列医院和新英格兰女执事医院合并而成，后者由彼得·本特·布莱根医院和波士顿妇女医院合并而成。[麻省总医院](../Page/麻省总医院.md "wikilink")（MGH）靠近灯塔山，附近还有麻省眼耳医院和斯保定康复医院。波士顿还有[VA医学中心](../Page/美国退伍军人事务部.md "wikilink")。
波士顿的许多医疗设施都与大学有关。[长木医学区和](../Page/长木医学区.md "wikilink")[麻省总医院是世界知名的医学研究中心](../Page/麻省总医院.md "wikilink")，与[哈佛医学院有关](../Page/哈佛医学院.md "wikilink")。新英格兰医学中心位于唐人街南部，与[塔夫茨大学有关](../Page/塔夫茨大學.md "wikilink")，现在已经更名为[塔夫茨医学中心](https://en.wikipedia.org/wiki/Tufts_Medical_Center)。波士顿医学中心位于南区，主要是波士顿大学医学院的教学医院，也是波士顿地区最大的外伤中心；\[25\]它是由波士顿大学医院和波士顿市立医院合并而成。

### 治安

在20世纪末、21世纪初，波士顿的低犯罪率要归功于波士顿警察局与社区、教会的合作，以阻止青少年加入犯罪团伙。因而被誉为“波士顿奇迹”。该市的谋杀案从1990年的152起（谋杀率为十万分之26.5）下降到了1999年的31起（谋杀率为十万分之5.26，其中没有一起是青少年犯罪）。此后几年中，谋杀案件以50%的幅度上下波动：2002年为60起，2003年为39起，2004年为64起，2005年为75起。尽管数字还远不及1990年那样高，但是谋杀率的再度提高已经使许多波士顿人深感不安，开始讨论波士顿警察局是否应该要调整其与犯罪作战的方法。\[26\]\[27\]\[28\]

## 文化

[ISH_WC_Boston1.jpg.jpg](https://zh.wikipedia.org/wiki/File:ISH_WC_Boston1.jpg.jpg "fig:ISH_WC_Boston1.jpg.jpg")
波士顿和整个新英格兰地区一样，拥有独特的不发**r**音的口音，称为（波士顿英语）；而[当地烹调的食材取用海鲜和乳制品较多](../Page/新英格兰烹调.md "wikilink")。

[爱尔兰裔美国人在波士顿政治和宗教机构中影响很大](../Page/爱尔兰裔美国人.md "wikilink")。

许多人认为波士顿是一个很有教养的城市，主要得益于众多高级知识分子的名声；波士顿许多文化起源于它的大学。\[29\]该市拥有一些华丽的剧院，有[卡特拉庄严剧院](../Page/卡特拉庄严剧院.md "wikilink")（Cutler
Majestic
Theatre）、[波士顿歌剧院](../Page/波士顿歌剧院.md "wikilink")、[王安表演艺术中心](../Page/王安表演艺术中心.md "wikilink")、舒伯特剧院和Orpheum剧院。著名的表演艺术团体有[波士顿芭蕾舞团](../Page/波士顿芭蕾舞团.md "wikilink")、[波士顿交响乐团](../Page/波士顿交响乐团.md "wikilink")、[波士顿抒情歌剧团](../Page/波士顿抒情歌剧团.md "wikilink")、[波士顿巴洛克和](../Page/波士顿巴洛克.md "wikilink")[亨德尔与海顿协会](../Page/亨德尔与海顿协会.md "wikilink")（美国最古老的合唱队）。这里每年还有许多重要事件，如新年前夕的[首夜演出](../Page/首夜演出.md "wikilink")，和在[美国独立日期间持续一周的海港节](../Page/美国独立日.md "wikilink")（Harborfest），举行波士顿流行音乐会，并在[查尔斯河岸燃放焰火](../Page/查尔斯河.md "wikilink")。

由于该市在[美国革命中扮演的重要角色](../Page/美國革命.md "wikilink")，一些与该时期有关的历史遗迹被作为[波士顿国家历史公园的一部分得到保护](../Page/波士顿国家历史公园.md "wikilink")。其中许多是沿着[自由之路](../Page/自由之路.md "wikilink")（Freedom
Trail），这条路线是用一条红色线或植入地面的砖块标出来。该市还拥有几个优秀的美术馆，包括[波士顿美术博物馆和](../Page/波士顿美术博物馆.md "wikilink")[加德纳博物馆](../Page/加德纳博物馆.md "wikilink")。[马萨诸塞大学在哥伦比亚角的校园内设有](../Page/马萨诸塞大学.md "wikilink")[约翰·肯尼迪图书馆](../Page/约翰·肯尼迪图书馆.md "wikilink")。市内还有[波士顿图书馆](../Page/波士顿图书馆.md "wikilink")（Boston
Athenaeum，美国最古老的独立图书馆之一）、[波士顿儿童博物馆](../Page/波士顿儿童博物馆.md "wikilink")、[公牛与小莺酒馆](../Page/公牛与小莺酒馆.md "wikilink")（Bull
& Finch Pub，该建筑因拍摄电视剧《[干杯](../Page/干杯.md "wikilink")》而著称）、[科学博物馆
(波士顿)和](../Page/科学博物馆_\(波士顿\).md "wikilink")[新英格兰水族馆](../Page/新英格兰水族馆.md "wikilink")。

波士顿还是音乐流派[硬核朋克](../Page/硬核朋克.md "wikilink")（hardcore
punk）的诞生地之一。波士顿的音乐家对这种音乐形式贡献甚大。1990年代，波士顿拥有顶级的[第三波斯卡](../Page/第三波斯卡.md "wikilink")（third
wave ska）和[斯卡朋克](../Page/斯卡朋克.md "wikilink")（ska
punk）现场表演，著名乐队有麦迪麦迪波斯顿（*The Mighty
Mighty Bosstones*）、*The Allstonians*、*Skavoovie and the
Epitones*、*Dropkick
Murphys*等。1980年代，硬核朋克摇滚组织了一次这种音乐流派的乐队荟萃演出“[这是波士顿，不是洛杉矶](../Page/这是波士顿，不是洛杉矶.md "wikilink")”。当时一些夜总会都以这种流派的乐队表演著称，不过所有那些夜总会均已在城市改造时被夷为平地。

### 教育

[BCburnslawnsunset.jpg](https://zh.wikipedia.org/wiki/File:BCburnslawnsunset.jpg "fig:BCburnslawnsunset.jpg")
波士顿被誉为“美国雅典”，是因为在波士顿大都会区拥有超过100所大学，超过25万名大学生在此接受教育。\[30\]在市区内，[东北大学是一所大型的私立大学](../Page/东北大学_\(美国\).md "wikilink")，在[芬威区有一座校园](../Page/芬威-Kenmore.md "wikilink")。[波士顿大学是世界上最大的私立大学之一](../Page/波士顿大学.md "wikilink")，位于查尔斯河畔的[联邦大道](../Page/联邦大道_\(波士顿\).md "wikilink")。[惠洛克学院](../Page/惠洛克学院.md "wikilink")、[西蒙斯学院](../Page/西蒙斯学院.md "wikilink")、[马萨诸塞药学院和](../Page/马萨诸塞药学院.md "wikilink")[温沃斯理工学院组成了](../Page/温沃斯理工学院.md "wikilink")[芬威大学群](../Page/芬威大学群.md "wikilink")，毗邻东北大学。[萨福克大学是一所小型私立大学](../Page/萨福克大学.md "wikilink")，以法学院著称，校园坐落在灯塔山。[新英格兰法学院是一所小型的私立法学院](../Page/新英格兰法学院.md "wikilink")，位于剧院区，创建时是美国唯一的一所女子法学院。\[31\][愛默生學院是一所小型的私立学院](../Page/愛默生學院.md "wikilink")，在表演艺术领域、新闻、写作和电影方面有很高的声望，位置靠近波士顿公园。该市还拥有几所音乐学校和美术学校，包括[马萨诸塞美术学院](../Page/马萨诸塞美术学院.md "wikilink")、[新英格兰音乐学校](../Page/新英格兰音乐学校.md "wikilink")（美国最古老的独立音乐学校）、[波士顿音乐学校](../Page/波士顿音乐学校.md "wikilink")、[波士顿美术博物馆学校和](../Page/波士顿美术博物馆学校.md "wikilink")[伯克利音乐学院](../Page/伯克利音乐学院.md "wikilink")。波士顿主要的公立大学是[马萨诸塞大学波士顿分校](../Page/马萨诸塞大学波士顿分校.md "wikilink")，位于多尔切斯特的哥伦比亚角，该市的2所社区学院是洛克斯布里社区学院和[邦克山社区学院](../Page/邦克山社区学院.md "wikilink")。

几所美国主要的大学位于波士顿外围，在该市有重要影响。[布兰戴斯大学被誉为](../Page/布兰戴斯大学.md "wikilink")“犹太哈佛”，成立于1948年，是全美最年轻的主要研究院大学。[哈佛大学是美国最古老的高等教育机构](../Page/哈佛大学.md "wikilink")，位于查尔斯河对岸的[剑桥](../Page/剑桥_\(马萨诸塞州\).md "wikilink")。[哈佛商学院和](../Page/哈佛商学院.md "wikilink")[哈佛医学院位于波士顿](../Page/哈佛医学院.md "wikilink")，并正在计划在波士顿的阿尔斯顿附近进行重大扩展。[麻-{}-省理工學院](../Page/麻省理工學院.md "wikilink")（MIT）最初位于波士顿市内，很长时期称为“波士顿理工学院”（1865年－1916年），在1916年才跨过查尔斯河迁往剑桥。[塔夫茨大学管理着它的医学院和牙科学院](../Page/塔夫茨大学.md "wikilink")，[塔夫茨-新英格兰医疗中心是一个拥有](../Page/塔夫茨-新英格兰医疗中心.md "wikilink")451个床位的医学学术机构，拥有一个科室齐全的成人医院和流动的儿童医院。[波士顿学院是市内最早的高等教育机构](../Page/波士顿学院.md "wikilink")，也是美国最古老的[耶稣会大学之一](../Page/耶稣会.md "wikilink")，位于[栗树山](../Page/栗树山_\(马萨诸塞州\).md "wikilink")。近来该校向布莱顿（波士顿繁荣兴旺的地区）扩展。位於[巴布森公園的](../Page/巴布森公園.md "wikilink")[巴布森學院](../Page/巴布森學院.md "wikilink")，其在創業管理領域的排名已經連續十八年為全美第一。\[32\]

[波士顿公立学校是美国最古老的公立学校系统](../Page/波士顿公立学校.md "wikilink")，从[幼儿园到](../Page/幼儿园.md "wikilink")12年级，共有58,600名学生。该系统包括145所学校，其中包括[波士顿拉丁学校](../Page/波士顿拉丁学校.md "wikilink")（最古老的公立学校，成立于1635年；有7-12年级，在7-9年级通过公开考试招收学生)，[英文中学](../Page/英文中学.md "wikilink")（最古老的公立[中学](../Page/中学.md "wikilink")，成立于1821年）和马特学校（最古老的公立小学，成立于1639年。\[33\]该市还拥有私立学校、教会学校和[特许学校](../Page/特许学校.md "wikilink")（charter
school）。3000名少数族裔学生通过大都会教育机会委员会（METCO）得到在郊区学校就学的机会。2002年，《[福布斯杂志](../Page/福布斯.md "wikilink")》将波士顿公立学校系统列为美国最好的大城市学校系统，毕业率达到82%。\[34\]

### 体育

[Fenway_park.jpg](https://zh.wikipedia.org/wiki/File:Fenway_park.jpg "fig:Fenway_park.jpg")

| 俱乐部                                      | 联盟                                                | 运动                                 | 主场                                   | 成立时间  | 冠軍賽                                       |
| ---------------------------------------- | ------------------------------------------------- | ---------------------------------- | ------------------------------------ | ----- | ----------------------------------------- |
| [波士顿红袜](../Page/波士顿红袜.md "wikilink")     | [美国职棒大联盟](../Page/美國職棒大聯盟.md "wikilink")（MLB）     | [棒球](../Page/棒球.md "wikilink")     | [芬威球場](../Page/芬威球場.md "wikilink")   | 1901年 | 9次[世界冠军](../Page/世界大賽.md "wikilink")      |
| [新英格兰爱国者](../Page/新英格兰爱国者.md "wikilink") | [美式橄榄球大联盟](../Page/國家美式足球聯盟.md "wikilink")（NFL）   | [美式足球](../Page/美式足球.md "wikilink") | [吉列体育场](../Page/吉列体育场.md "wikilink") | 1960年 | 6次[超級盃](../Page/超級盃.md "wikilink")        |
| [波士顿塞爾提克](../Page/波士顿塞爾提克.md "wikilink") | [国家篮球协会](../Page/nBA.md "wikilink")（NBA）          | [篮球](../Page/篮球.md "wikilink")     | [TD花园](../Page/TD花园.md "wikilink")   | 1946年 | 17次[NBA总冠军](../Page/NBA总冠军.md "wikilink") |
| [波士顿棕熊](../Page/波士顿棕熊.md "wikilink")     | [国家冰球联盟](../Page/国家冰球联盟.md "wikilink")（NHL）       | [冰球](../Page/冰球.md "wikilink")     | [TD花园](../Page/TD花园.md "wikilink")   | 1924年 | 6次[史丹利杯](../Page/史丹利杯.md "wikilink")      |
| [新英格兰革命](../Page/新英格兰革命.md "wikilink")   | [美国职业足球大联盟](../Page/美国职业足球大联盟.md "wikilink")（MLS） | [英式足球](../Page/英式足球.md "wikilink") | [吉列体育场](../Page/吉列体育场.md "wikilink") | 1995年 | \--                                       |
| [波士顿加农炮](../Page/波士顿加农炮.md "wikilink")   | [职业长曲棍球联盟](../Page/职业长曲棍球联盟.md "wikilink")（MLL）   | [长曲棍球](../Page/长曲棍球.md "wikilink") | [哈佛体育场](../Page/哈佛体育场.md "wikilink") | 2001年 | \--                                       |

[波士顿红袜](../Page/波士顿红袜.md "wikilink")（俗称“袜子”）是[美国职棒大联盟的创始成员之一](../Page/美國職棒大聯盟.md "wikilink")，该队的主场[芬威球場](../Page/芬威球場.md "wikilink")（Fenway
Park）兴建于1912年，是美国四大职业运动项目仍在使用的最古老的运动场地。1903年，在波士顿举行了第一场[世界职业棒球大赛](../Page/世界大赛_\(棒球\).md "wikilink")，波士顿红袜（当时称为“朝圣者”）对阵[匹兹堡海盗](../Page/匹兹堡海盗.md "wikilink")。\[35\]

[Mass_State_House_with_C's_and_B's_banners.JPG](https://zh.wikipedia.org/wiki/File:Mass_State_House_with_C's_and_B's_banners.JPG "fig:Mass_State_House_with_C's_and_B's_banners.JPG")
尽管[新英格兰爱国者自从](../Page/新英格兰爱国者.md "wikilink")1971年就搬到郊区的[福克斯堡](../Page/福克斯堡.md "wikilink")，它仍然是波士顿的美式足球队。该队成立于1960年，是[美式橄榄球大联盟的发起成员之一](../Page/國家美式足球聯盟.md "wikilink")。该队自2001年赛季以来已经6次赢得[超级-{zh-hans:碗;zh-hk:碗;zh-tw:盃;}-](../Page/超级碗.md "wikilink")（2002年、2004年、2005年、2015年、2017年、2019年）。该队与属于[美国职业足球大联盟的](../Page/美国职业足球大联盟.md "wikilink")[新英格兰革命队](../Page/新英格兰革命.md "wikilink")（英式足球）共用[吉列体育场](../Page/吉列体育场.md "wikilink")。

[TD花园](../Page/TD花园.md "wikilink")（旧名舰队街）靠近[北站](../Page/北站_\(波士顿\).md "wikilink")，是[波士顿棕熊队和](../Page/波士顿棕熊队.md "wikilink")[波士顿塞爾提克队](../Page/波士顿塞爾提克队.md "wikilink")2队的主场。波士顿棕熊队是[国家冰球联盟的第一个成员](../Page/国家冰球联盟.md "wikilink")，拥有“创始6人”的特权。波士顿凯尔特人队则是[国家篮球协会的创始会员](../Page/nBA.md "wikilink")。波士顿凯尔特人队因在1959年到1966年連續8年奪得NBA總冠軍，2008年共17次夺得NBA总冠军，超过美国其他任何城市而声名显赫。\[36\]

波士顿许多大学在体育方面都很活跃。波士顿地区有4个[全国大学体育协会](../Page/全国大学体育协会.md "wikilink")（NCAA）成员：[波士顿学院](../Page/波士顿学院.md "wikilink")、[波士顿大学](../Page/波士顿大学.md "wikilink")、[东北大学和](../Page/东北大学_\(美国\).md "wikilink")[哈佛大学](../Page/哈佛大学.md "wikilink")。4所大学的冰球队每年二月的第二个星期一晚上，都在TD北方银行花园球馆进行一场“豆锅锦标赛”（Beanpot
Tournament）。

[波士顿马拉松是该市最著名的一项体育赛事之一](../Page/波士顿马拉松.md "wikilink")，赛程长42公里，从霍普金顿到后湾的科普来广场。这是世界上最古老的马拉松比赛，在每年四月第3个星期一的[爱国者日举行](../Page/爱国者日.md "wikilink")，参加比赛者人数甚多。

## 交通

[Tunnel-large.jpg](https://zh.wikipedia.org/wiki/File:Tunnel-large.jpg "fig:Tunnel-large.jpg")
[洛根国际机场位于邻近的](../Page/洛根国际机场.md "wikilink")[东波士顿](../Page/东波士顿.md "wikilink")，运营波士顿的大部分定期客运业务。在城市30英里（48公里）半径范围内还有许多小型机场。

波士顿市中心的街道没有规则可循，因为它们是在几个世纪以前根据需要而形成，并未经过规划，逐渐填满了小小的半岛。\[37\]一些迂回曲折的街道，每一段都有不同的名称，忽而消失，忽而又随意的分成数条小巷。2006年3月的《自行车》杂志将波士顿列为美国最不适合骑自行车的城市之一。\[38\]波士顿被描绘成一个“广场之城”，传统上将主要大道的交叉点用该市的历史名人命名。另一方面，[后湾](../Page/后湾.md "wikilink")、[东波士顿](../Page/东波士顿.md "wikilink")、[波士顿南区和](../Page/波士顿南区.md "wikilink")[南波士顿的街道则用方格系统命名](../Page/南波士顿.md "wikilink")。这些方格路网建造在该市早期形成的杂乱街道外围。

波士顿是[90号州际公路](../Page/90号州际公路.md "wikilink")（又称为[马萨诸塞收费公路](../Page/马萨诸塞收费公路.md "wikilink")）的东部起点，[95号州际公路环绕该市](../Page/95号州际公路.md "wikilink")，当地称为[马萨诸塞128号公路](../Page/马萨诸塞128号公路.md "wikilink")；[93号州际公路从北到南纵贯该市](../Page/93号州际公路.md "wikilink")。穿过波士顿市中心的[中央动脉车流量极大](../Page/中央动脉.md "wikilink")，于是改为通过一条地下（Big
Dig）通过市中心。

[波士顿地铁是美国第一个地下快速运输系统](../Page/波士顿地铁.md "wikilink")\[39\]，现已扩展到65.5英里长\[40\]，北到马尔登，南到Braintree，西到牛顿。波士顿的郊区通勤铁路长度超过200英里\[41\]，北到梅里迈克河谷，西到[伍斯特](../Page/伍斯特.md "wikilink")，南到[普罗维登斯](../Page/普罗维登斯.md "wikilink")。波士顿有31.5%的通勤者使用公共交通。\[42\]由于城市布局紧凑，学生数量众多，在波士顿步行者的数量达到13%，远比美国同规模城市多\[43\]。

## 旅游

### 三一堂

[三一堂](../Page/三一堂_\(波士顿\).md "wikilink")（Trinity
Church）的位置在[波士顿公共图书馆美威大楼的前方](../Page/波士顿公共图书馆.md "wikilink")，奠基於1733年，是一座典雅的[哥德复兴式的教堂](../Page/哥德复兴.md "wikilink")，曾於一八七二年十一月八、九日两天的波士顿大火中付之一炬，1877年予以重建，是仿十一世纪法国中部岛奥凡尼（Auvergne）的「法国罗马式」建筑而成，塔楼取材西班牙[萨拉曼卡旧主教座堂](../Page/萨拉曼卡旧主教座堂.md "wikilink")，西簷廊则採法国南部[阿尔斯的圣多芬教堂的形式](../Page/阿尔斯.md "wikilink")，设计非常精致，是美国教堂建筑中的代表作。

### 波士顿公园

[波士顿公园位于波士顿市中心](../Page/波士顿公园.md "wikilink")，面积59英亩。这个公园与其邻近的公共花园（Public
Garden）都是波士顿城最为核心的地方，而波士顿公园更是自由行迹（Freedom
Trail）的起始点，同时也是整个美国年代最久的公园。最初这里用于放牧，也曾作过军事训练场。自1830年开始，这片区域開始禁止放牧。从那时直到现在，波士顿公园主要被用作一个公共區域供人们在此进行休闲娱乐活动。这个公园是典型的英国式花园，是城市中难得的绿洲，是波士顿人理想的休闲场所，這裡还能看到音乐家、表演家、演说者的表演。地址：公园街站和博伊尔斯顿站。

### 哈佛大学

[哈佛大学是美国最早的私立大学](../Page/哈佛大学.md "wikilink")，是以培养研究生和从事科学研究为主的综合性大学，哈佛大学的正式注册名称为：The
President and Fellows of Harvard College，总地址是Byerly Hall, 8 Garden
Street,
Cambridge。哈佛大学是全美第一所大学。美国独立战争以来大量的革命先驱都出自于哈佛的门下。哈佛大学獲誉为美国政府的思想库。这里先后诞生了八位[美国总统](../Page/美国总统.md "wikilink")，40位[诺贝尔奖得主和](../Page/诺贝尔奖.md "wikilink")30位[普利策奖得主](../Page/普利策奖.md "wikilink")。其商学院案例教学盛名远播。这里也培养了缔造了[微软](../Page/微软.md "wikilink")、[IBM](../Page/IBM.md "wikilink")、[Facebook等一个个商业奇迹的人物](../Page/Facebook.md "wikilink")。

## 友好城市

波士顿有八个官方承认的[友好城市](../Page/友好城市.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>国家或地区</p></th>
<th><p>时间</p></th>
<th><p>参考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/京都.md" title="wikilink">京都</a></p></td>
<td></td>
<td><p>1959</p></td>
<td><p>[44]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯特拉斯堡.md" title="wikilink">-{zh-hans:斯特拉斯堡;zh-hant:史特拉斯堡}-</a></p></td>
<td></td>
<td><p>1960</p></td>
<td><p>[45][46]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴塞罗那.md" title="wikilink">巴塞罗那</a></p></td>
<td></td>
<td><p>1980</p></td>
<td><p>[47][48]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杭州.md" title="wikilink">杭州</a></p></td>
<td></td>
<td><p>1982</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帕多瓦.md" title="wikilink">帕多瓦</a></p></td>
<td></td>
<td><p>1983</p></td>
<td><p>[49]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/墨尔本.md" title="wikilink">墨尔本</a></p></td>
<td></td>
<td><p>1985</p></td>
<td><p>[50]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台北市.md" title="wikilink">台北</a></p></td>
<td></td>
<td><p>1996</p></td>
<td><p>[51]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞康第─塔科拉迪.md" title="wikilink">塞康第─塔科拉迪</a></p></td>
<td></td>
<td><p>2001</p></td>
<td></td>
</tr>
</tbody>
</table>

## 注释

## 延伸阅读

  -
  -
  -
  -
  -
  -
## 外部链接

  - [市政府網站](http://www.cityofboston.gov)
  - [Greater Boston Chamber of Commerce](http://www.bostonchamber.com/)
  - [Greater Boston Convention & Visitors
    Bureau](http://www.bostonusa.com/)
  - [The Boston Indicators
    Project](https://web.archive.org/web/20051025065943/http://www.tbf.org/indicatorsProject/)
  - [Open Space
    Plan 2002-2006](http://www.cityofboston.gov/parks/openspace_doc.asp),
    City of Boston, maps and analyses
  - [Historical Maps of Boston from the Norman B. Leventhal Map Center
    at the Boston Public
    Library](http://maps.bpl.org/tag/location:Boston+%28Mass.%29/)
  - [Commercial portal](http://www.boston.com)
  - [City guide](http://yourtown.boston.com)
  - [City
    guide](https://web.archive.org/web/20040519162354/http://boston-online.com/)
  - [City guide](http://www.december.com/places/bos/blue.html)
  - [Go Boston Card: Sightseeing at 31 Boston Attractions and
    Tours](http://www.gobostoncard.com)
  - [Guide to the local
    language](https://web.archive.org/web/20040401204758/http://www.boston-online.com/glossary.html)
  - [波士顿环球报](http://www.boston.com/news/globe/) 英文日報
  - [波士顿凤凰报](https://web.archive.org/web/20051011111353/http://www.bostonphoenix.com/)
    英文報纸
  - [Boston Magazine](http://www.bostonmagazine.com/) 英文雜誌
  - [波士顿生活网](http://www.shenghuonet.com/)
  - [波士頓区中华文化協会](http://www.gbcca.org/)
  - [波士頓华人佈道会](http://www.bcec.net/)
  - [华美福利会](http://www.aaca-boston.org/)
  - [星島日報
    波士頓新聞](https://web.archive.org/web/20140711051400/http://ny.stgloballink.com/boston/)
  - [世界日報──波士頓新聞](https://web.archive.org/web/20080506082704/http://www.worldjournal.com/wj-bonews.php?mc_seq_id=16)
  - [爰国者隊 (美式足球)
    新英格兰爱国者队](https://web.archive.org/web/20051201150734/http://chinese.patriots.com/)
  - [波士頓中文網](http://www.sinoboston.com/index.php?c_lang=big5/)
  - [波士顿旅游景点](http://www.lulutrip.com/scene/view/id-5)

{{-}}

[Category:马萨诸塞州城市](../Category/马萨诸塞州城市.md "wikilink")
[Category:大西洋沿海城市](../Category/大西洋沿海城市.md "wikilink")
[波士顿](../Category/波士顿.md "wikilink")
[Category:大波士顿](../Category/大波士顿.md "wikilink")
[B](../Category/美国各州首府.md "wikilink")

1.

2.

3.

4.

5.   [Boston: History of the
    Landfills](http://www.bc.edu/bc_org/avp/cas/fnart/fa267/bos_fill2.html)

6.

7.

8.

9.

10.

11.

12.

13.
14.
15.
16.
17.

18.

19. [U.S. QuickFacts
    Finder](http://quickfacts.census.gov/qfd/states/25/2507000.html)

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.
42.

43.

44.

45.

46.

47.

48.

49.

50.

51.