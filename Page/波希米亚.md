**波希米亚**（；；；）是[古中欧地名](../Page/古中欧地名.md "wikilink")，占据了古捷克地区西部三分之二的区域。现在位于包括[布拉格在内的](../Page/布拉格.md "wikilink")[捷克共和国中西部地区](../Page/捷克共和国.md "wikilink")。广义上，尤其是有关[波希米亞王國的历史文献中](../Page/波希米亞王國.md "wikilink")，也常指代包括[捷克](../Page/捷克.md "wikilink")[摩拉维亚和](../Page/摩拉维亚.md "wikilink")[西里西亚在内的整个捷克地区](../Page/西里西亚.md "wikilink")。波希米亚是古中欧国家，曾为[神圣罗马帝国中的一个王国](../Page/神圣罗马帝国.md "wikilink")，随后成为[奧地利哈布斯堡王朝的一个省](../Page/奧地利哈布斯堡王朝.md "wikilink")。波希米亚曾经南临[奥地利](../Page/奥地利.md "wikilink")，西抵[巴伐利亚](../Page/巴伐利亚.md "wikilink")，北接[萨克森和](../Page/萨克森.md "wikilink")[卢萨蒂亚](../Page/卢萨蒂亚.md "wikilink")，东北与[西里西亚为邻](../Page/西里西亚.md "wikilink")，并與东部的[摩拉维亚接壤](../Page/摩拉维亚.md "wikilink")。1918年至1939年以及1945年至1992年，波希米亚属于[捷克斯洛伐克的一部分](../Page/捷克斯洛伐克.md "wikilink")，1993年之后成为组成[捷克共和国的主要部分之一](../Page/捷克共和国.md "wikilink")。

如今的波希米亚拥有52,065平方公里的疆域，[捷克共和国](../Page/捷克共和国.md "wikilink")1030万居民中有约600万人生活在波西米亚地区。现今的波西米亚地区西与[德国接壤](../Page/德国.md "wikilink")，北邻[波兰](../Page/波兰.md "wikilink")，东部为古[摩拉维亚地区](../Page/摩拉维亚.md "wikilink")，南部则与[奥地利为邻](../Page/奥地利.md "wikilink")。波希米亚地区山峦环绕，分别通过[波希米亚森林](../Page/波希米亚森林.md "wikilink")，[厄尔士山脉](../Page/厄尔士山脉.md "wikilink")，[克尔科诺谢山和](../Page/克尔科诺谢山.md "wikilink")[蘇台德山脈等与其他地区接壤](../Page/蘇台德山脈.md "wikilink")。其中最高峰位于[蘇台德山脈内](../Page/蘇台德山脈.md "wikilink")。

## 语源

公元前2世纪，[古罗马人和多个民族在争取意大利北部的统治权](../Page/古罗马.md "wikilink")，其中一个民族是[波伊人](../Page/波伊人.md "wikilink")。古罗马人在[皮亚琴察战役](../Page/皮亚琴察战役.md "wikilink")（西元前194年）和[摩德納战役](../Page/摩德納战役.md "wikilink")（西元前193年）中打败波伊人。此后，大部分波伊人撤退并越过[阿尔卑斯山北迁](../Page/阿尔卑斯山.md "wikilink")。

古罗马文学家将占领的这片地区称为**Boihaemum**，最早出现在[塔西佗的](../Page/塔西佗.md "wikilink")[日耳曼尼亚志一书中](../Page/日耳曼尼亚志.md "wikilink")（著于公元1世纪末）。该名称包含波伊人的部落名*Boi-*加上[日耳曼语中](../Page/日耳曼语.md "wikilink")“家园”的词根*xaim-*。这片地区包含了部分南波希米亚地区，部分[巴伐利亚地区](../Page/巴伐利亚.md "wikilink")（其名称也是从波伊人部落名演化而来）和[奥地利](../Page/奥地利.md "wikilink")。波希米亚的[捷克语名称](../Page/捷克语.md "wikilink")"Čechy"则是从6或7世纪迁入这片地区的捷克[斯拉夫部落的名称演化而来](../Page/斯拉夫.md "wikilink")。

## 歷史

波希米亞地區在[羅馬帝國時期為一支叫波希人的](../Page/羅馬帝國.md "wikilink")[凱爾特人的聚居地](../Page/凱爾特人.md "wikilink")。公元前1世紀，[日耳曼人遷入](../Page/日耳曼人.md "wikilink")，[斯拉夫人亦於公元](../Page/斯拉夫人.md "wikilink")6世紀遷入波希米亞。由於日耳曼人為主的[神聖羅馬帝國的強大](../Page/神聖羅馬帝國.md "wikilink")，波希米亞的日耳曼裔[貴族成立一獨立王國](../Page/貴族.md "wikilink")，君主由[推舉制產生](../Page/推舉制.md "wikilink")。

1306年波希米亞王族絕後，[哈布斯堡家族的](../Page/哈布斯堡王朝.md "wikilink")[鲁道夫一世以](../Page/鲁道夫一世_\(波希米亚\).md "wikilink")[外戚身分繼承波希米亞王位](../Page/外戚.md "wikilink")。但在15世紀，由於直言極諫的[布拉格大學](../Page/布拉格大學.md "wikilink")[教授](../Page/教授.md "wikilink")[揚·胡斯](../Page/揚·胡斯.md "wikilink")，被[羅馬教廷以](../Page/羅馬教廷.md "wikilink")[火刑](../Page/火刑.md "wikilink")[處死](../Page/處死.md "wikilink")，波希米亞[鄉民憤而](../Page/鄉民.md "wikilink")[皈依](../Page/皈依.md "wikilink")[胡斯教派](../Page/胡斯.md "wikilink")，而哈布斯堡家族信仰的是[天主教](../Page/天主教.md "wikilink")。因此，哈布斯堡家族的王位繼承權備受挑戰。在1555年，哈布斯堡[君主簽署](../Page/君主.md "wikilink")[奥格斯堡宗教和约](../Page/奥格斯堡宗教和约.md "wikilink")，授與波希米亞人[宗教自由](../Page/宗教自由.md "wikilink")。這些[宗教寬容政策](../Page/宗教寬容.md "wikilink")，令哈布斯堡君主一直被選為[波希米亞國王](../Page/捷克君主列表.md "wikilink")。

1517年[日耳曼的](../Page/日耳曼.md "wikilink")[馬丁路德推行](../Page/馬丁路德.md "wikilink")[宗教改革以來](../Page/宗教改革.md "wikilink")，[新教就廣傳於](../Page/新教.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。1617年，狂熱的天主教徒[斐迪南二世](../Page/斐迪南二世_\(神聖羅馬帝國\).md "wikilink")，繼承他的表哥，成為[神聖羅馬皇帝](../Page/神聖羅馬皇帝.md "wikilink")，一改諸先王作風，不再對新教採取容忍政策，導致他落選波希米亞國王。為奪回王位，斐迪南決定征服波希米亞，導致[三十年戰爭的爆發](../Page/三十年戰爭.md "wikilink")。

三十年戰爭過後，波希米亞一直被哈布斯堡王朝統治，但仍然保持為一獨立的[王國](../Page/王國.md "wikilink")，擁有自己的[政府](../Page/政府.md "wikilink")。直至1743年，波希米亞成為[哈布斯堡-洛林皇朝統治下的一個](../Page/哈布斯堡-洛林皇朝.md "wikilink")[行省](../Page/行省.md "wikilink")，[德語成為唯一的官方語言](../Page/德語.md "wikilink")，[捷克語則是](../Page/捷克語.md "wikilink")[方言](../Page/方言.md "wikilink")。

[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，[奧匈帝國被肢解](../Page/奧匈帝國.md "wikilink")，波希米亞成為[東歐新國家](../Page/東歐.md "wikilink")[捷克斯洛伐克的一個](../Page/捷克斯洛伐克.md "wikilink")[行省](../Page/行省.md "wikilink")。1993年，成為[捷克共和國一重要組成部分](../Page/捷克共和國.md "wikilink")。

## 地理

**波希米亞高地**位於今捷克的波希米亞地區，多[森林](../Page/森林.md "wikilink")，面積約158,000平方公里，海拔1,602公尺。西南部為波希米亞森林，由[奧赫熱河上游河谷至](../Page/奧赫熱河.md "wikilink")[奧地利境內的](../Page/奧地利.md "wikilink")[多瑙河河谷](../Page/多瑙河.md "wikilink")，呈西北—東南分布，海拔3,500英尺，主要為針葉林和落葉林。

## 參見

  - [捷克共和國](../Page/捷克共和國.md "wikilink")
  - [摩拉維亞](../Page/摩拉維亞.md "wikilink")
  - [捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")
  - [波希米亞主義](../Page/波希米亞主義.md "wikilink")
  - [布波族](../Page/布波族.md "wikilink")

[Category:波希米亚](../Category/波希米亚.md "wikilink")