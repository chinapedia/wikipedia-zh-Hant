**波德申**（，简称）是[马来西亚](../Page/马来西亚.md "wikilink")[森美兰州第二大城市](../Page/森美兰.md "wikilink")，位于森州首府[芙蓉外](../Page/芙蓉.md "wikilink")32公里的海岸，为当地旅游度假胜地。

波德申是一個馬來人、華人、印度人雜處的小鎮；華人大多居住在市區、馬來人住在周邊農村，印度人則住在較偏遠的橡膠或油棕園裡的社區。

由於波德申有一座火力發電廠及兩座煉油廠，所以該鎮有大量的外來人口遷入到這幾個大工廠工作。

## 簡史

波德申舊名Arang，在馬來語為「木炭」之意；這說明了它原本是一個以製造木炭而名的濱海聚落，一直到约10年前此處仍有大片供燒製木炭用的紅樹林。在英國殖民地時代，此間開闢小港，乃以開港英國人Sir
John Frederick Dickson之名，命名為Port
Dickson。在英殖期間，波德申興建了連往森美蘭州首府芙蓉的鐵路支線，與馬來半島鐵路主幹線連網，這使波德申逐漸發展為一個能源工業市鎮。在1980年代，波德申的市中心仍是一漁港，但在填海計畫實施後，漁業沒落。能源工業在興建本身的專用碼頭後，市區貨港也沒落至僅剩往來蘇門答臘的小量載客。

## 旅館

波德申雖然只是一個小鎮，但由於長達18公里的沙灘海岸渡假資源，共有20間三星級以上旅館，頗有特色。

  - [Ancasa Resort Apartment](http://www.ancasaresort.com/)(☆☆☆)
  - Avillion Village Resort(☆☆☆☆☆)
  - Bayu Beach Resort(☆☆☆)
  - Casa Rachado Resort(☆☆☆)
  - Corus Paradise Resort(☆☆☆☆)
  - Desa Lagoon Resort(☆☆☆☆)
  - Duta Villa Gulf Resort(☆☆☆)
  - Duta Hacienda Riviera Resort(☆☆☆)
  - [Eagle Ranch Resort](http://www.eagleranch.com.my/)(☆☆☆)
  - Glory Beach Resort(☆☆☆)
  - [Grand Lexis Port Dickon](http://www.grandlexispd.com/)(☆☆☆☆☆)
  - Guoman Resort(☆☆☆☆☆)
  - Ilham Resort(☆☆☆)
  - LE Paris Resort(☆☆☆)
  - [Lexis Hibiscus Port Dickson](http://lexishibiscuspd.com/)(☆☆☆☆☆)
  - [Lexis Port Dickson](http://www.lexispd.com/)(☆☆☆☆)
  - Ris Beach Hotel(☆☆☆)
  - Selesa Beach Resort(☆☆☆☆)
  - Seri Malaysia Hotel(☆☆☆)
  - Sunshine Bay Resort(☆☆☆)
  - The Regency Tanjung Tuan Beach Resort(☆☆☆)
  - Primaland Resort(☆☆☆)

## 產業及新興燕窩業

波德申百年前是燒製木炭聞名的小鎮，也有不少居民從事[近海漁業](../Page/近海漁業.md "wikilink")。[二次世界大戰後](../Page/二次世界大戰.md "wikilink")，煉油、發電等[能源產業在此間發展](../Page/能源產業.md "wikilink")，當然地帶動了相關的工程業，使波德申成為一個外來人口眾多的工業、工程業市鎮。大約在2000年，當地廢棄的長江戲院被[印尼人租來當](../Page/印尼.md "wikilink")"倉庫"──後來發現其實是從事誘燕定居、採燕窩的生意，自此波德申人開始對[金絲燕](../Page/金絲燕.md "wikilink")(Aerodramus
spp.)有了認識，在[馬來西亞](../Page/馬來西亞.md "wikilink")21世紀興起的燕屋風潮中，成為中部燕屋重鎮。由於波德申是[馬六甲海峽距離](../Page/馬六甲海峽.md "wikilink")[蘇門答臘最近之處](../Page/蘇門答臘.md "wikilink")、只有19海浬，與印尼最頂級燕窩產地[廖內群島很近](../Page/廖內群島.md "wikilink")、並且紅樹林生態相似，所以因印尼森林大火逃難至此的金絲燕大量聚居波德申，所以此間燕窩加工技術號雖差，原料品質卻備受行家肯定，被譽為繼[越南](../Page/越南.md "wikilink")[會安燕](../Page/會安.md "wikilink")、印尼廖內燕以後，難得的燕窩品種。

图像:Portdickson 005.jpg|波德申海滩 图像:Bodeshen.png|波德申地图 图像:Portdickson
011.jpg|海边涼亭

## 参考

[春泉镇](../Page/春泉镇.md "wikilink")－Bandar Springhill

[Category:马来西亚城市](../Category/马来西亚城市.md "wikilink")