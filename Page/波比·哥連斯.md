**羅拔 "波比"·楊·哥連斯**（[英文](../Page/英文.md "wikilink")：**Robert "Bobby" Young
Collins**，）生於[蘇格蘭](../Page/蘇格蘭.md "wikilink")[格拉斯哥Govanhill](../Page/格拉斯哥.md "wikilink")，以其在[些路迪](../Page/些路迪.md "wikilink")、[愛華頓](../Page/愛華頓.md "wikilink")、[列斯聯的魅力在](../Page/列斯聯.md "wikilink")[足球界見稱](../Page/足球.md "wikilink")。

司職[中場的他身高雖然只有](../Page/中場.md "wikilink")
160厘米（5[呎](../Page/呎.md "wikilink")3[吋](../Page/吋.md "wikilink")），但他體格強健且勤力走動。他在17歲時已加盟[些路迪](../Page/些路迪.md "wikilink")，效力至27歲。

## 外部連結

  - [蘇格蘭足球總會對他的介紹](http://www.scottishfa.co.uk/player_details.cfm?playerid=112797)

[Category:1958年世界盃足球賽球員](../Category/1958年世界盃足球賽球員.md "wikilink")
[Category:蘇格蘭足球代表隊球員](../Category/蘇格蘭足球代表隊球員.md "wikilink")
[Category:蘇格蘭足球運動員](../Category/蘇格蘭足球運動員.md "wikilink")
[Category:些路迪球員](../Category/些路迪球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:貝利球員](../Category/貝利球員.md "wikilink")
[Category:摩頓球員](../Category/摩頓球員.md "wikilink")
[Category:蘇格蘭足球領隊](../Category/蘇格蘭足球領隊.md "wikilink")
[Category:哈德斯菲爾德領隊](../Category/哈德斯菲爾德領隊.md "wikilink")
[Category:侯城領隊](../Category/侯城領隊.md "wikilink")
[Category:班士利領隊](../Category/班士利領隊.md "wikilink")
[Category:愛爾蘭外籍足球運動員](../Category/愛爾蘭外籍足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:格拉斯哥人](../Category/格拉斯哥人.md "wikilink")