**波尔图**（），是[葡萄牙北部一個面向](../Page/葡萄牙.md "wikilink")[大西洋的港口城市](../Page/大西洋.md "wikilink")，2011年人口为23万\[1\]，城市面积为389平方千米\[2\]，大都会区人口约180万。是葡萄牙第一大港和第二大城市（次于[里斯本](../Page/里斯本.md "wikilink")），兼[波尔图区首府及](../Page/波尔图区.md "wikilink")[北部大區的行政](../Page/北部大區_\(葡萄牙\).md "wikilink")、經濟與文化中心。市內擁有葡萄牙國內著名的足球會[-{zh-cn:波尔图;zh-hk:波圖;zh-mo:波爾圖;zh-tw:波爾圖;}-與知名建築](../Page/波圖足球會.md "wikilink")[波尔图音乐厅](../Page/波尔图音乐厅.md "wikilink")。

波爾圖市由十五個子分區組成，北部接壤馬杜辛紐什、和麥雅衛星都市，東部與貢多馬爾市接壤；市區南部流有源自西班牙的北部第一大河[杜羅河](../Page/杜羅河.md "wikilink")，對岸遙望[加亞新城](../Page/加亞新城.md "wikilink")（Vila
Nova de Gaia），西面則向大西洋。

葡萄牙这一國名與[波酒都源於這座城市](../Page/波酒.md "wikilink")。其舊城區與周圍產酒區是[世界文化遺產](../Page/世界文化遺產.md "wikilink")。

## 歷史

波爾圖早在公元四世紀就已有人居住。其時波爾圖是一個商業[港口](../Page/港口.md "wikilink")，其名字就有「港口」之意。城始建于公元五世纪。但在公元700年因外來入侵引發戰爭，而趨向沒落。

12世纪，城市逐渐形成了现在的模式，分为上面的文化区和下面的商业区两部分。在12世纪[葡萄牙王國自](../Page/葡萄牙王國.md "wikilink")[莱昂王国獨立以後](../Page/莱昂王国.md "wikilink")，這裡便是葡萄牙的中心地區。公元14至15世紀，[大航海發現使波爾圖開始成為造船港口](../Page/大航海时代_\(地理\).md "wikilink")。這時在[杜羅河谷所生產的](../Page/杜羅河.md "wikilink")[葡萄酒](../Page/葡萄酒.md "wikilink")，也於波爾圖這個地方售賣。1703年葡萄牙與[英國簽訂了條約](../Page/英國.md "wikilink")，開始建立了雙邊貿易關係。

20世纪以来经济日益发展，为[葡萄牙西北部重要经济中心](../Page/葡萄牙.md "wikilink")。

## 地理

波尔图地处滨海平原，冬温夏凉，年降水量900毫米。土壤肥沃，附近为重要农业区，盛产[葡萄](../Page/葡萄.md "wikilink")、[橄榄](../Page/橄榄.md "wikilink")、[柑橘](../Page/柑橘.md "wikilink")。波尔图是[杜罗河上游](../Page/杜罗河.md "wikilink")[葡萄酒的集散地](../Page/葡萄酒.md "wikilink")，品质甘醇的波尔图葡萄酒，被誉为葡萄牙的「第一大使」。

## 經濟

[Porto_nightscape.jpg](https://zh.wikipedia.org/wiki/File:Porto_nightscape.jpg "fig:Porto_nightscape.jpg")
波尔图是葡萄牙北部经济中心，亦是以出產[缽酒及](../Page/缽酒.md "wikilink")[波爾圖葡萄酒出名](../Page/波爾圖葡萄酒.md "wikilink")。

除了有名的[葡萄酒之外](../Page/葡萄酒.md "wikilink")，其他工业如纺织（棉、毛、丝）、漁業（製鹽）、软木加工、机械、化工和炼油也非常發達。

波尔图的外贸出口额占全国的50％，国民生产总值占全国的三分之一。

## 景點

  - [波多音樂廳](../Page/波多音樂廳.md "wikilink")：波爾圖市內的重要代表建築。由荷蘭建築師[雷姆·庫哈斯設計](../Page/雷姆·庫哈斯.md "wikilink")，是2001年[欧洲文化之都計劃的一部份](../Page/欧洲文化之都.md "wikilink")。
  - [波尔图主教座堂](../Page/波尔图主教座堂.md "wikilink")
  - 克莱瑞科塔（Torre dos Clerigos）
  - 水晶宫
  - [杜罗河口](../Page/杜罗河.md "wikilink")：波爾圖是葡萄牙河流[杜羅河的出口](../Page/杜羅河.md "wikilink")。
  - 自由广场
  - [火龍球場](../Page/火龍球場.md "wikilink")：葡萄牙足球會[-{zh-cn:波尔图;zh-hk:波圖;zh-mo:波爾圖;zh-tw:波爾圖;}-主場舘](../Page/波圖足球會.md "wikilink")。-{zh-cn:波尔图;zh-hk:波圖;zh-mo:波爾圖;zh-tw:波爾圖;}-曾於2004年贏得[歐冠盃](../Page/歐冠盃.md "wikilink")。

## 姐妹城市

  - [法国](../Page/法国.md "wikilink")[波尔多](../Page/波尔多.md "wikilink")（1978）
  - [中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")（1995）
  - [澳門](../Page/澳門.md "wikilink")（1997）

## 參考資料

<references />

[\*](../Category/波爾圖.md "wikilink")
[Category:大西洋沿海城市](../Category/大西洋沿海城市.md "wikilink")
[Category:葡萄牙城市](../Category/葡萄牙城市.md "wikilink")
[Category:波尔图区](../Category/波尔图区.md "wikilink")
[Category:葡萄牙世界遗产](../Category/葡萄牙世界遗产.md "wikilink")

1.
2.  [Demographia: World Urban
    Areas](http://www.demographia.com/db-worldua.pdf), March 2010