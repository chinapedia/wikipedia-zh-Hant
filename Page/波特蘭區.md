<table>
<caption><big><big><strong>波特蘭區</strong><br />
<strong></strong></big></big></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Portland_in_Jamaica.svg" title="fig:Portland_in_Jamaica.svg">Portland_in_Jamaica.svg</a></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>位置</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/首府.md" title="wikilink">首府</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>主要城鎮</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>郡</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>地區</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>排行</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/人口.md" title="wikilink">人口</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/商業.md" title="wikilink">商業</a></strong></p></td>
</tr>
</tbody>
</table>

**波特蘭**（[英語](../Page/英語.md "wikilink")：****）是[牙買加東北岸的一個](../Page/牙買加.md "wikilink")[區](../Page/牙买加行政区划.md "wikilink")，位於[聖托馬斯區](../Page/聖托馬斯區_\(牙買加\).md "wikilink")（St.
Thomas
Parish）以[北](../Page/北.md "wikilink")，[薩里郡的](../Page/薩里郡.md "wikilink")[聖瑪麗區](../Page/聖瑪麗區_\(牙買加\).md "wikilink")（St.
Mary
Parish）以東。波特蘭是牙買加的[郊區之一](../Page/郊區.md "wikilink")，以[海灘聞名](../Page/海灘.md "wikilink")。[首府](../Page/首府.md "wikilink")[安東尼奧港](../Page/安東尼奧港.md "wikilink")（Port
Antonio）。

## 歷史

[安東尼奧港在](../Page/安東尼奧港.md "wikilink")16世紀由[西班牙人發現](../Page/西班牙.md "wikilink")，「波特蘭」的得名源於兩大區──聖喬治及聖托馬斯部分地區的合併。18世紀，波特蘭是[大英帝國的](../Page/大英帝國.md "wikilink")[海軍據點](../Page/海軍.md "wikilink")，時至今日，不少[英國](../Page/英國.md "wikilink")[建築仍可見於該區的](../Page/建築.md "wikilink")[廣場](../Page/廣場.md "wikilink")。[聖公宗石](../Page/聖公宗.md "wikilink")[教堂建於](../Page/教堂.md "wikilink")1840年。DeMontevin旅舍建於1881年，現為Titchfield半島一間著名賓館。建於[1729的喬治堡](../Page/1729.md "wikilink")（Fort
George），其[大砲俯瞰著](../Page/大砲.md "wikilink")[港灣](../Page/港.md "wikilink")。Titchfield半島亦是Titchfield學校的根據地，於1785年創校，是加勒比海地區歷史最悠久、最有地立的私立[中學之一](../Page/中學.md "wikilink")。該校位於喬治堡。

[非洲](../Page/非洲.md "wikilink")[奴隸工人種植](../Page/奴隸.md "wikilink")[甘蔗和](../Page/甘蔗.md "wikilink")[咖啡的時期](../Page/咖啡.md "wikilink")，很多奴隸通過策劃叛亂成功逃到多山的內陸。他們在數量上遠超過[白人僱主](../Page/白人.md "wikilink")（比例是10比1）。波特蘭區原來是人口稀少的，及至1723年，[總督向白人](../Page/總督.md "wikilink")[新教徒和黑白](../Page/新教徒.md "wikilink")[混血兒提供免費土地](../Page/混血兒.md "wikilink")，[印第安人和](../Page/美洲原住民.md "wikilink")[黑人開始到這裏定居](../Page/黑人.md "wikilink")。此舉似乎用以吸引更多[英國人](../Page/英國人.md "wikilink")，但是逃亡到藍山和John
Crow山居住的[黑奴決定不讓英國人接管該區](../Page/黑奴.md "wikilink")。

1730年代，一連串[戰役發生](../Page/戰役.md "wikilink")。隨着「逃亡黑人保姆」（Nanny of the
Maroon）對英國人和逃亡黑奴拓居地的統治，雙方在1739年簽訂[條約停戰](../Page/條約.md "wikilink")。「逃亡黑人保姆」後來成為牙買加首位兼唯一的國家英雄。

[旅遊業亦是波特蘭區發展不可或缺的一環](../Page/旅遊業.md "wikilink")。隨着[香蕉出口](../Page/香蕉.md "wikilink")，商業在19世紀中期開展起來。首府安東尼奧港，以「世界香蕉之都」聞名。實際上，牙買加的[旅遊業發源於此](../Page/旅遊業.md "wikilink")，從[美國](../Page/美國.md "wikilink")[波士頓回航的香蕉船為牙買加帶來首批遊客](../Page/波士頓.md "wikilink")。影星[埃尔罗·弗林](../Page/埃尔罗·弗林.md "wikilink")（Errol
Flynn）亦進動了本地經濟。一次旅行中，他的[遊艇被惡劣天氣沖上岸](../Page/遊艇.md "wikilink")，然後愛上了這個地區。他在離岸不遠的[海軍島](../Page/海軍島_\(牙買加\).md "wikilink")（Navy
Island）購買了房產，用來招待他的[荷里活朋友](../Page/荷里活.md "wikilink")。

## 地理及人口

波特蘭區位於[北緯](../Page/北緯.md "wikilink")18°10'，[西經](../Page/西經.md "wikilink")75°27'。該區由[藍山的最高峰](../Page/藍山_\(牙買加\).md "wikilink")（海拔2,256米）延伸至北岸。而且，當地土地肥沃，景色怡人，海灘優美。該區位於東北[信風的直達路徑](../Page/信風.md "wikilink")，其以南的藍山[山脊阻隔了濕氣](../Page/山脊.md "wikilink")，引致該區的降雨量是全島最多的。安東尼奧港是波特蘭的首府兼主要城鎮，有兩個[海港](../Page/港口.md "wikilink")，西面那個受一個名為海軍島的小島保護。波特蘭佔地814平方公里，是牙買加第七大區。

波特蘭區有各種複雜地形。洞穴、海灣、河流、瀑布和小山分佈在整條海岸線。十四個洞穴散見於黃皮灣、橘子灣、希望灣、安東尼奧港、波士頓灣、長灣、Innis灣和Nonsuch。另外，十七條河流組成了貫穿區的網狀系統。最大的河流是Rio
Grande、黃皮灣和Hectors河。

波特蘭[人口約有](../Page/人口.md "wikilink")82,000人，當中15,000人住在首府。[黑人佔](../Page/黑人.md "wikilink")89.8%，白人佔1.2%，[亞裔佔](../Page/亞裔.md "wikilink")5.3%，[混血種族佔](../Page/混血兒.md "wikilink")2.6%，其他佔1.1%。

## 商業

### 農業

波特蘭區是主要生產[香蕉](../Page/香蕉.md "wikilink")、[椰子](../Page/椰子.md "wikilink")、[麵包果](../Page/麵包果.md "wikilink")、[咖啡](../Page/咖啡.md "wikilink")、[芒果和](../Page/芒果.md "wikilink")[阿開木果](../Page/阿開木果.md "wikilink")，用作內銷和出口。波特蘭沿岸一帶有最肥沃的土地，適合各種[耕作](../Page/耕種.md "wikilink")，很多國內作物在此出產。[製造業是當地經濟的一小部分](../Page/製造業.md "wikilink")，約有18間工廠。

### 旅遊業

波特蘭仍然有錢人的是度假勝地，不少人在這裏擁有房產。旅遊設施有Trident別墅及酒店、牙買加王宮、龍灣別墅、Goblin山酒店、牙買加山頂、Fern山會所、仿聲鳥山等，豐儉由人。現在，波特蘭加強推廣[生態旅遊](../Page/生態旅遊.md "wikilink")，因為有關方面最有利當地的發展。

#### 景點

[Rafting.jpg](https://zh.wikipedia.org/wiki/File:Rafting.jpg "fig:Rafting.jpg")
由於自然景觀得天獨厚，波特蘭區的[旅遊業頗興盛](../Page/旅遊業.md "wikilink")。這裏有優美的海灘，如法國人灣、[波士頓](../Page/波士頓海灘_\(牙買加\).md "wikilink")、Winifred及龍灣。有名的藍山及世界知名的[藍潟湖亦位於波特蘭區](../Page/藍潟湖.md "wikilink")。藍潟湖據信是[死火山的](../Page/死火山.md "wikilink")[火山口](../Page/火山口.md "wikilink")，幾乎是一個[內陸](../Page/內陸.md "wikilink")[小海灣](../Page/灣.md "wikilink")，水深約55米（180英尺）。Rio
Grande河上的[飄筏運動亦是一項旅遊特色](../Page/飄筏運動.md "wikilink")。

Boston Jerk
Centre是波特蘭一個地區，其肉乾食品遠近馳名，如雞乾、魚乾和豬肉乾。乾肉食品由[牙買加乾肉香料配製而成](../Page/牙買加乾肉香料.md "wikilink")。

### 其他活動

  - 製片業可追溯至1950年代初，超過782部[電影在波特蘭製作](../Page/電影.md "wikilink")。兩部最著名的電影分別是1954年的《[海底兩萬里](../Page/海底兩萬里.md "wikilink")》（[美國](../Page/美國.md "wikilink")[華特迪士尼出品](../Page/華特迪士尼.md "wikilink")）及1972年的《[不速之客](../Page/不速之客.md "wikilink")（*The
    Harder They Come*，Vista Productions
    Ja.出品）。《[雞尾酒](../Page/雞尾酒.md "wikilink")》（*Cocktail*）中，[湯姆·克魯斯在龍灣海灘的一間](../Page/湯姆·克魯斯.md "wikilink")[酒吧拍攝](../Page/酒吧.md "wikilink")。《[蒼蠅王](../Page/蒼蠅王.md "wikilink")》早期的電影版本大部分在法國人灣取景。

## 地方

### 城鎮

  - [Cattawood Springs](../Page/:en:Cattawood_Springs.md "wikilink")
  - [Nanny鎮](../Page/:en:Nanny_Town.md "wikilink")
  - [安東尼奧港](../Page/安東尼奧港.md "wikilink")，[首府](../Page/首府.md "wikilink")

### 河流

  - [Rio Grande](../Page/:en:Rio_Grande_\(Jamaica\).md "wikilink")

### 島嶼

  - [海軍島](../Page/海軍島_\(牙買加\).md "wikilink")

### 海灘

  - [波士頓海灘](../Page/波士頓海灘_\(牙買加\).md "wikilink")
  - [長灣海灘](../Page/長灣海灘_\(牙買加\).md "wikilink")
  - [法國人灣](../Page/法國人灣_\(牙買加\).md "wikilink")

## 參考資料

  - [波特蘭區資訊](http://www.jamlib.org.jm/portland_history.htm)
  - [牙買加統計學會](http://www.statinja.com/)

## 外部連結

  - [波特蘭區概況](http://www.mfaft.gov.jm/Country_Profile/portland.htm)
  - [牙買加政治地理](http://www.discoverjamaica.com/gleaner/discover/geography/polgol.htm)
  - [歷史](http://www.tripz.com/travelguide/013220702-port-antonio-history.html)

[P](../Category/牙買加行政區劃.md "wikilink")