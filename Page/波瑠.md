**波瑠**，[日本女](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")、[Fashion
Model](../Page/模特兒.md "wikilink")，出身於[東京都](../Page/東京都.md "wikilink")[足立區](../Page/足立區.md "wikilink")。隸屬事務所。早期曾以**南波瑠**的名字擔任雜誌《[Seventeen](../Page/SEVENTEEN_\(雜誌\).md "wikilink")》的專屬模特兒，亦曾是[non-no雜誌的專屬模特兒](../Page/non-no.md "wikilink")。2015年主演[NHK下半年度](../Page/NHK.md "wikilink")[晨間劇](../Page/晨間小說連續劇.md "wikilink")《[阿淺來了](../Page/阿淺來了.md "wikilink")》。

## 經歷

初中1年級時，波瑠報名參加一個音樂錄影帶的面試，被現在所屬的事務所相中，從此進入藝能界。首部戲劇作品為2006年的電視劇《》。首張單曲[CD作品則](../Page/CD.md "wikilink")2008年4月16日推出。出道以來除了模特工作外也持續演出許多影視作品，但多是配角或客串，並未得到太大注目，2012年將出道起維持的長髮剪去改成短髮造型，在影視方面得到更多發展機會。2014年春季演出電視劇《[BORDER](../Page/BORDER.md "wikilink")》裡的女性鑑識醫，獲得不錯的評價\[1\]，8月初主演電視劇《》\[2\]及9月初次單獨主演電影《[榕樹食堂之戀](../Page/榕樹食堂之戀.md "wikilink")》\[3\]。同年10月，在《[對不起青春！](../Page/對不起青春！.md "wikilink")》劇中飾演男主角高中時代的暗戀對象，亦引起觀眾注目。

2015年3月，[NHK公布由波瑠主演](../Page/NHK.md "wikilink")2015年9月開播的[晨間劇](../Page/晨間小說連續劇.md "wikilink")《[阿淺來了](../Page/阿淺來了.md "wikilink")》，飾演女主角「白岡淺」。因憧憬演出晨間劇，此為波瑠第四次參加試鏡，最後在2590位競爭者之中脫穎而出。\[4\]\[5\]同月宣布結束自2012年開始的《[non-no](../Page/non-no.md "wikilink")》雜誌專屬平面模特兒身分，專心於演員工作。\[6\]

2016年夏季主演[富士電視台電視劇](../Page/富士電視台.md "wikilink")**ON
異常犯罪搜查官·藤堂比奈子**，是她首次擔任[民放電視劇主演](../Page/民放.md "wikilink")。\[7\]

## 人物

  - 波瑠或是早期使用的南波瑠皆是藝名。\[8\]
  - 2014年9月的專訪提到，高中畢業後曾有一段時間欲專心於演員工作，但沒有工作上門，也有過焦慮想去考取其他證照的時期，最後選擇了大量觀賞電影來學習演技，當時一個月可看30部電影。\[9\]
  - 有許多媒體或網路評價認為波瑠與日本已故知名女星[夏目雅子頗為神似](../Page/夏目雅子.md "wikilink")\[10\]\[11\]\[12\]，而她也曾在改編自伊集院静自傳的電視劇《》中飾演過夏目雅子本人的角色\[13\]。
  - 興趣：[繪畫](../Page/繪畫.md "wikilink")、看[電影](../Page/電影.md "wikilink")、寫[電郵](../Page/電郵.md "wikilink")、[烹飪](../Page/烹飪.md "wikilink")
  - 特長：[跳高](../Page/跳高.md "wikilink")、彈奏[結他](../Page/結他.md "wikilink")

## 演出作品

### 電視劇

  - （[WOWOW](../Page/WOWOW.md "wikilink")，2006年）

  - [14歲的母親](../Page/14歲的母親.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")，2006年）飾演
    上田春

  - [我們的教科書](../Page/我們的教科書.md "wikilink")（第1、9集）（[富士電視台](../Page/富士電視台.md "wikilink")，2007年）飾演
    熊澤櫻

  - [人形少女16歲\!\!](../Page/人形少女16歲!!.md "wikilink")（[BS富士](../Page/BS富士.md "wikilink")，2008年）飾演
    桐生明美

  - [少年刑警〜童顏刑警·柴田竹虎〜](../Page/少年刑警#電視劇.md "wikilink")（第1集）（富士電視台，2008年）飾演留美

  - [戀空](../Page/戀空_\(電視劇\).md "wikilink")（[東京廣播公司](../Page/TBS電視台.md "wikilink")，2008年）飾演咲

  - [33分偵探](../Page/33分偵探.md "wikilink")（第3集）（富士電視台，2008年）飾演赤梅女學院學生

  - （第3集）（富士電視台，2008年）飾演

  - （[朝日電視台](../Page/朝日電視台.md "wikilink")，2009年3月20日）飾演秋山莉奈

  - 被塗了血的挑戰狀I（富士電視台，2009年3月27日）飾演朝岡真里菜

  - [雙頭犬](../Page/雙頭犬.md "wikilink")（TBS，2009年）飾演白川加奈

  - [新參者](../Page/新參者.md "wikilink")（第4集）（TBS，2010年）飾演澤村香苗

  - [GOLD](../Page/GOLD.md "wikilink")（第1、2集）（富士電視台，2010年）飾演椎名涼子

  - （第6集）（[關西電視台](../Page/關西電視台.md "wikilink")，2010年）飾演橋元望

  - [黑豹
    人中之龍新章](../Page/黑豹_人中之龍新章.md "wikilink")（[每日放送](../Page/每日放送.md "wikilink")，2010年）飾演工藤沙紀

  - （第5回「」）（[NHK數碼衛星高清頻道](../Page/NHK數碼衛星高清頻道.md "wikilink")，2010年10月29日）飾演

  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")（2011年真人（[岡田將生](../Page/岡田將生.md "wikilink")）版）（日本電視台，2011年4月23日）飾演八坂渚

  - （特備劇版）（NHK，2011年4月-5月）飾演榎木光子

  - [Switch
    Girl\!\!～變身指令～](../Page/Switch_Girl!!～變身指令～.md "wikilink")（，2011年12月）飾演城崎麗香

  - （[BS-TBS](../Page/BS-TBS.md "wikilink")，2012年3月28日）飾演

  - [Legal High](../Page/Legal_High.md "wikilink")（第1集）（富士電視台，2012年）飾演
    島村智子

  - （富士電視台TWO，2012年）飾演 夏目蓮花

  - [東野圭吾懸疑故事](../Page/東野圭吾懸疑故事.md "wikilink")（第8集「」）（富士電視台，2012年8月30日）飾演
    佐伯洋子

  - [MONSTERS](../Page/破案怪獸.md "wikilink")（第4集）（[TBS電視台](../Page/TBS電視台.md "wikilink")，2012年）飾演鈴木佳代

  - （第8集）（關西電視台，2012年）飾演 葵

  - [相棒](../Page/相棒.md "wikilink")（第11季，第11集元旦日Special「」）（朝日電視台，2013年）飾演
    二百鄉茜

  - [書店員美智留的遭遇](../Page/書店員美智留的遭遇.md "wikilink")（[NHK](../Page/日本放送協會.md "wikilink")，2013年1月-3月）飾演
    古川千秋

  - 食物语·她的菜单（）（第9、10集）（[NHK BS
    Premium](../Page/NHK_BS_Premium.md "wikilink")，2013年）飾演 山根麻衣

  - [幽靈女友](../Page/幽靈女友.md "wikilink")（第7集）（關西電視台，2013年）飾演

  - [救命病棟24時](../Page/救命病棟24時.md "wikilink")（第5季）（富士電視台，2013年7月-9月）飾演
    國友花音

  - （朝日電視台，2013年9月15日）飾演

  - [一幣通關小子：我們的電玩史](../Page/一幣通關小子：我們的電玩史.md "wikilink")（[東京電視台](../Page/東京電視台.md "wikilink")，2013年10月-12月）飾演
    高野文美

  - [萬事占卜處
    歡迎來到陰陽屋](../Page/萬事占卜處_歡迎來到陰陽屋.md "wikilink")（第3集）（關西電視台，2013年）飾演
    宮内夏央

  - （TBS電視台，2013年11月25日）飾演 安田涼子

  - （TBS電視台，2013年12月24日）飾演

  - [金田一少年之事件簿
    獄門塾殺人事件](../Page/金田一少年之事件簿_\(電視劇\)#第六季單發特別版（2014年）.md "wikilink")（日本電視台，2014年1月4日）飾演
    濱秋子

  - （WOWOW，2014年3月8日）飾演 平澤瞳

  - （NHK BS Premium，2014年）飾演 花店店員

  - [BORDER
    警視廳搜查一課殺人犯搜查第4係](../Page/BORDER.md "wikilink")（朝日電視台，2014年4月-6月）飾演
    比嘉美香

  - [怪談 三島屋奇異百物語](../Page/怪談_三島屋奇異百物語.md "wikilink")（NHK BS
    Premium，2014年8月-9月）飾演 ****（**主演**）

  - [對不起青春！](../Page/對不起青春！.md "wikilink")（TBS電視台，2014年10月-12月）飾演 蜂矢祐子

  - （東京電視台，2015年1月2日）飾演 田沼早苗

  - [草裙舞女孩與愛犬可可](../Page/草裙舞女孩與愛犬可可.md "wikilink")（東京電視台，2015年3月11日）飾演
    竹澤霞

  - [NHK晨間劇](../Page/連續電視小說.md "wikilink")
    [阿淺來了](../Page/阿淺來了.md "wikilink")（NHK，2015年9月-2016年4月）飾演
    **今井淺→白岡淺**（**主演**）

  - [世界上最艱難的戀愛](../Page/世界上最艱難的戀愛.md "wikilink")（日本電視台，2016年4月-6月 ）飾演
    柴山美咲

  - 阿淺來了番外 破鍋配鍋蓋（NHK BS Premium，2016年4月23日）飾演 白岡淺

  - [ON
    異常犯罪搜查官·藤堂比奈子](../Page/ON_異常犯罪搜查官·藤堂比奈子.md "wikilink")（富士電視台，2016年7月-9月）飾演
    **藤堂比奈子**（**主演**）

  - [媽媽，不當您女兒可以嗎？](../Page/媽媽，不當您女兒可以嗎？.md "wikilink")（NHK，2017年1月-3月）飾演
    **早瀨美月**（**主演**）

  - [北風與太陽的法庭](../Page/北風與太陽的法庭.md "wikilink")（）（日本電視台，3月17日）飾演
    **櫻川風香**（**主演**）

  - [其實並不在乎你](../Page/其實並不在乎你.md "wikilink")（TBS電視台，2017年4月-6月）飾演
    **渡邊美都**（**主演**）

  - BORDER 衝動～檢察官·比嘉美香～（朝日電視台，2017年10月6日、13日）飾演 **比嘉美香**（**主演**）

  - BORDER 贖罪（朝日電視台，2017年10月29日）飾演 比嘉美香

  - [有錢人家鳥事多](../Page/有錢人家鳥事多.md "wikilink")（日本電視台，2018年1月-3月）飾演北澤知晶

  - （東京電視台，2018年1月8日）飾演國枝實希

  - [Sniffer 嗅覺搜查官](../Page/嗅覺神探#日本版.md "wikilink") 特備劇（NHK，2018年3月21日）

  - （）（朝日電視台，2018年4月-6月）飾演**矢代朋**（**主演**）

  - （第三季，第2、9集）（朝日電視台，2018年）飾演矢代朋

  - （日本電視台，2018年7月-9月）飾演****（**主演**）

  - 未解決之女 警視廳文書搜查官〜緋色的信號〜（朝日電視台，預定於2019年4月28日播映）\[14\]

### 電影

  - [在通勤電車上找到座位的技術！](../Page/在通勤電車上找到座位的技術！.md "wikilink")（[sazanami](../Page/sazanami.md "wikilink")，2006年10月7日公映）飾演愛

  - [檸檬時代](../Page/檸檬時代.md "wikilink")（[Theres
    Enterprise](../Page/Theres_Enterprise.md "wikilink")，2007年3月31日公映）飾演吉井薰（負責Side
    Guitar）

  - [戀空](../Page/戀空.md "wikilink")（[東寶](../Page/東寶.md "wikilink")，2007年11月3日公映）飾演亞矢

  - （2007年）飾演果步（**主演**）

  - [在遙遠彼方的小千](../Page/在遙遠彼方的小千.md "wikilink")（[Synergy
    Relations](../Page/Synergy_Relations.md "wikilink")，2008年1月19日公映）飾演林田遊子

  - （「」[製作委員會](../Page/製作委員會方式.md "wikilink")，2008年1月26日公映）飾演阿染

  - [奪命捉迷藏](../Page/奪命捉迷藏.md "wikilink")（[PHANTOM
    FILM](../Page/PHANTOM_FILM.md "wikilink")，2008年2月2日公映）飾演佐藤春

  - [Rock'n Roll ★
    Diet\!](../Page/Rock'n_Roll_★_Diet!.md "wikilink")（[Bio-Tide and
    Associates](../Page/Bio-Tide_and_Associates.md "wikilink")，2008年9月13日公映）飾演

  - （[Astaire](../Page/Astaire.md "wikilink")，2008年11月8日公映）飾演川代瑞樹

  - [Ikemen Bank](../Page/Ikemen_Bank.md "wikilink")（[Jolly
    Roger](../Page/Jolly_Roger_\(公司\).md "wikilink")，2009年3月14日公映）飾演山村百合

  - [守護天使](../Page/守護天使_\(小說\).md "wikilink")（[Avex
    Entertainment](../Page/Avex_Entertainment.md "wikilink")，2009年6月20日公映）飾演渡邊麻美

  - [山形SCREAM](../Page/山形SCREAM.md "wikilink")（Jolly
    Roger，2009年8月1日公映）飾演波來前胸惠

  - （Avex Entertainment、IMJ Entertainment，2009年8月29日公映）飾演（高中生時代）

  - [武士道SIXTEEN](../Page/武士道SIXTEEN.md "wikilink")（[GO
    CINEMA](../Page/GO_CINEMA.md "wikilink")，2010年4月24日公映）飾演西荻綠子

  - [SOFTBOY](../Page/SOFTBOY.md "wikilink")（[東映](../Page/東映.md "wikilink")，2010年6月19日公映）飾演草薙結衣

  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（真人電影版）（Jolly
    Roger、[Liverpool](../Page/Liverpool_\(公司\).md "wikilink")、[名古屋電視台](../Page/名古屋電視台.md "wikilink")，2010年11月6日公映）飾演小笠原祥子（主演）

  - [高校痞子田中](../Page/高校痞子田中.md "wikilink")（[SHOWGATE](../Page/SHOWGATE.md "wikilink")，2012年2月18日公映）飾演

  - GIRL（[東寶](../Page/東寶株式會社.md "wikilink")，2012年5月26日公映）飾演北村裕子

  - [我和少女、還有貓](../Page/我和少女、還有貓.md "wikilink")（Good
    Coming，2012年6月4日公映）飾演美由紀

  - BUNGO～适度的欲望～（）（[角川映畫](../Page/角川映畫.md "wikilink")，2012年9月29日公映）飾演絹子。

  - 派对从澡堂开始（日本出版販賣，2012年12月1日公映）飾演望

  - 大家、再見（）（PHANTOM FILM，2013年1月26日公映）飾演松島有里

  - [絕叫學級](../Page/絕叫學級.md "wikilink")（真人電影版）（東寶映像事業部，2013年6月14日公映）飾演

  - [100次哭泣](../Page/100次哭泣.md "wikilink")（SHOWGATE，2013年6月22日公映）飾演小川惠子

  - [不完美情人](../Page/不完美情人.md "wikilink")（東寶，2013年10月26日公映）飾演川口朝美

  - [新大久保物語](../Page/新大久保物語.md "wikilink")（[United
    Entertainment](../Page/United_Entertainment.md "wikilink")，2013年11月16日公映）

  - [哥斯拉](../Page/哥斯拉_\(2014年電影\).md "wikilink")（東寶，2014年7月25日公映）為艾麗·布羅迪（[伊麗莎白·奧爾森的角色](../Page/伊麗莎白·奧爾森.md "wikilink")）配音

  - [榕樹食堂之戀](../Page/榕樹食堂之戀.md "wikilink")（）（[BS-TBS](../Page/BS-TBS.md "wikilink")，2014年9月20日公映）飾演（**主演**）

  - [Again
    第28年的甲子園](../Page/Again_第28年的甲子園.md "wikilink")（東映，2015年1月17日公映）飾演
    戶澤美枝

  - （）（[株式會社KADOKAWA](../Page/株式會社KADOKAWA.md "wikilink")／[松竹](../Page/松竹.md "wikilink")，2015年11月7日公映）飾演百合子

  - （[ARK
    Entertainment](../Page/ARK_Entertainment.md "wikilink")，2015年11月21日公映）飾演
    **本山奈緒子**（**主演**）

  - [在咖啡冷掉之前](../Page/在咖啡冷掉之前.md "wikilink")（東寶，2018年9月21公映）飾演清川二美子

  - （[HIGH BROW CINEMA](../Page/HIGH_BROW_CINEMA.md "wikilink")、PHANTOM
    FILM，2018年10月26日公映）飾演**波平久-{瑠}-美**（**主演**）

### 電視動畫

  - [元氣媽媽](../Page/元氣媽媽.md "wikilink")（第23集）（[東京電視台](../Page/東京電視台.md "wikilink")，2009年9月16日）聲演君子。

### 電影動畫

  - （，2016年7月2日公映）聲演 倫妲（）。

<!-- end list -->

  - （[東寶](../Page/東寶.md "wikilink")，預定於2019年8月2日公映）聲演 芙羅拉（）。

### 其他電視節目

  - （[富士電視台](../Page/富士電視台.md "wikilink")，2008年4月 - 2009年3月）。

  - （[名古屋電視台](../Page/名古屋電視台.md "wikilink")，2008年7月29日 - ）常規演出。

  - （[朝日放送](../Page/朝日放送.md "wikilink")，2009年8月14日）。

### 電台節目

  - [All Night Japan
    R](../Page/All_Night_Japan_R.md "wikilink")（[日本放送](../Page/日本放送.md "wikilink")，2008年6月21日）
    -
    與[有末麻祐子](../Page/有末麻祐子.md "wikilink")、[水澤惠麗奈](../Page/水澤惠麗奈.md "wikilink")3人的一天節目主持。

### 舞台劇

  - （2010年11月 - 12月）飾演小須田美香（主演）。

  - （2016年5月7日 - 22日）飾演里子。

### 廣告

#### 電視廣告

  - [SONY](../Page/SONY.md "wikilink")
    [Walkman](../Page/Walkman.md "wikilink")×[NET
    JUKE](../Page/NET_JUKE.md "wikilink")「ROCK'IN少女」篇（2006年）

  - [NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink")

      - 春之CAMPAIGN 音樂手機「櫻之承諾」篇（2007年）

    <!-- end list -->

      -

          -
            與[岡田將生共同演出](../Page/岡田將生.md "wikilink")。

    <!-- end list -->

      - d Market BOOKStore（2012年5月 - ）

  - [環球音樂](../Page/環球音樂_\(日本\).md "wikilink") 『DRIVIN' J-POP for
    LOVE\&JOY』（雜錦CD）（2008年）

  - [三得利](../Page/三得利.md "wikilink") Lucky Cider「」篇（2008年7月 - 2009年1月）

  - [倍樂生](../Page/倍樂生.md "wikilink")
    [進研Seminar高校講座](../Page/進研Seminar高校講座.md "wikilink")

      - 「攻擊大學考試 合格實績」篇（2008年11月 - ）
      - 「攻擊大學考試 宣誓」篇（2008年12月 - ）
      - 「攻擊大學考試 認真」篇（2009年1月）
      - 「攻擊大學考試 學生宣誓」篇（2009年2月 - ）
      - 「'09夏篇」篇（2009年6月 - ）

  - [花王](../Page/花王.md "wikilink")

      - 「在過山車的實證」篇（2009年10月 - ）

      - Liese（2014年3月 - ）

      - F （2016年10月 - ）

      - Primavista（2017年2月 - ）

  - [日本環球影城](../Page/日本環球影城.md "wikilink")「」篇（2010年8月 - 11月）

  - [Autobacs Seven](../Page/Autobacs_Seven.md "wikilink")
    Autobacs（2011年9月 - ）

  - [強生](../Page/強生.md "wikilink") 1·Day Acuvue Moist 散光用（2012年5月 - ）

  - [大塚食品](../Page/大塚食品.md "wikilink") MATCH（2013年5月 - ）

  - [NHK-BS](../Page/NHK-BS.md "wikilink") 訂費（2013年6月 - ）

  - [森永製菓](../Page/森永製菓.md "wikilink") DARS（2013年10月 - ）

  - [POINT](../Page/POINT.md "wikilink") [LOWRYS
    FARM](../Page/LOWRYS_FARM.md "wikilink")（2013年10月 - ）

  -   - Mynavi護士（2014年1月 - ）
      - Mynavi藥劑師（2016年9月 - ）

  - [日本郵政](../Page/日本郵政.md "wikilink") （2014年10月 - ）

  - （2015年3月 - ）

  - [GU](../Page/GU.md "wikilink")（2015年3月 - 2016年8月）

  - [三井SHOPPING PARK](../Page/三井不動產商業管理.md "wikilink") （2015年4月 - ）

  -   - 「波瑠 味」篇（2016年4月 - ）

      - [麒麟啤酒](../Page/麒麟啤酒.md "wikilink") （2017年4月 - ）

      - RED & 冰結GREEN（2019年2月 - ）

  - [Mister Donut](../Page/Mister_Donut.md "wikilink") Croissant
    Muffin（2016年4月 - ）

  - [久光製藥](../Page/久光製藥.md "wikilink") 「」篇（2016年5月 - ）

  - 「」 （2016年10月 - ）

  - 「札幌一番 」 （2016年10月 - ）

  -  「身分證明書」篇 （2017年）

  - （2018年9月 - ）

#### 平面廣告

  - [強生](../Page/強生.md "wikilink") [ACUVUE
    ADVANCE硬照](../Page/Acuvue.md "wikilink")（2007年）
  - [消防廳](../Page/消防廳.md "wikilink")
    2008年度秋天[火災預防運動海報](../Page/火災預防運動.md "wikilink")（2008年）
  - [建設業勞動災害防止協會](../Page/建設業勞動災害防止協會.md "wikilink") 年度末海報（2014年）
  - [法務省](../Page/法務省.md "wikilink")
    第64回[照亮社會運動](../Page/照亮社會運動.md "wikilink")「歡迎回來」海報（2014年）
  - [消防試驗研究中心](../Page/消防試驗研究中心.md "wikilink") 資格試驗廣報用海報（2014年）
  - [警視廳](../Page/警視廳.md "wikilink") 2017年度春天海報（2017年）

#### 其他

  - [日本損害保險協會](../Page/日本損害保險協會.md "wikilink") 2012年度廣報人物（2013年）

  - [亞瑟士](../Page/亞瑟士.md "wikilink") ASICS33宣傳大使（2013年）

  - 花卉大使（2017年）

  - （2016年 -）

### 電子遊戲

  - （[世嘉](../Page/世嘉.md "wikilink")，2010年）

### 音樂錄影帶

  - [FUNKY MONKEY BABYS](../Page/FUNKY_MONKEY_BABYS.md "wikilink")
    「告白」（2008年）

  - 「雪道」（2009年）

  - [張根碩](../Page/張根碩.md "wikilink") 「Stay」（2012年）

  - 「」（2014年）

  - [DREAMS COME TRUE](../Page/DREAMS_COME_TRUE.md "wikilink") 「」（2017年）

  - 「」（2017年）

### 雜誌

  - [SEVENTEEN專屬模特兒](../Page/SEVENTEEN_\(雜誌\).md "wikilink")（2007年 -
    2012年，[集英社](../Page/集英社.md "wikilink")）
  - [non-no專屬模特兒](../Page/non-no.md "wikilink")（2012年 - 2015年3月 ，集英社）

### 其他

  - 手機音樂劇  [20 -CRY-](../Page/20_-CRY-.md "wikilink")（Episode
    1）（[DOR@MO](../Page/DOR@MO.md "wikilink")，2009年1月7日 - ）

  - 手機戀愛劇
    100Scene之戀（Vol.3，第1集）（[VOLTAGE](../Page/VOLTAGE.md "wikilink")，2008年12月1日
    - ）飾演彩音。

  - （[NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink")
    [BeeTV](../Page/BeeTV_\(日本\).md "wikilink")，2011年10月1日 - ）飾演齊藤麻耶。

  - [秘密的關係〜老師是同居人〜](../Page/秘密的關係〜老師是同居人〜.md "wikilink")（BeeTV，2010年7月1日
    - ）飾演清瀨美和子。

  - （BeeTV，2013年5月20日 - ）飾演前園遙。

  - （[三陽商會官方網站](../Page/三陽商會.md "wikilink")，2013年5月13日 - ）飾演早坂千鳥（主演）。

  - （[森永製菓朱古力](../Page/森永製菓.md "wikilink")「DARS」官方網站，2013年10月1日 -
    ）飾演（主演）。

  - （第2集）（，2013年11月8日） - 飾演（主演）。

  - （[YouTube](../Page/YouTube.md "wikilink")，2015年6月15日 - 17日）。

## CD

### 單曲

  - [I miss you /
    MESSAGE～給明天的我～](../Page/I_miss_you_/_MESSAGE～給明天的我～.md "wikilink")（；2008年4月16日推出）（[東京廣播公司電視節目](../Page/東京廣播公司.md "wikilink")「」的片尾曲）

## 獎項

  - [第88回日劇學院賞主演女優賞](../Page/第88回日劇學院賞.md "wikilink")（阿淺來了）\[15\]
  - 2016年[東京國際戲劇節](../Page/東京國際戲劇節.md "wikilink") 主演女優獎（阿淺來了）
  - 第5回[CONFiDENCE日劇大獎最佳女主角獎](../Page/CONFiDENCE日劇大獎.md "wikilink")（ON
    異常犯罪搜查官·藤堂比奈子）\[16\]
  - [第93回日劇學院賞主演女優賞](../Page/第93回日劇學院賞.md "wikilink")（我可能沒那麼愛你）

## 参考文献

## 外部連結

  - [事務所官方介紹頁](http://www.horiagency.co.jp/talent/haru/index.html)

  - [唱片公司官方介紹頁](http://www.universal-music.co.jp/haru/)

  - [官方Blog「Haru's official blog」](http://ameblo.jp/horiagency-haru/)

  -
  - [中的前官方Blog](https://web.archive.org/web/20090621053654/http://www.mbga.jp/.pc/_u?u=12715926)<small>（[Internet
    Archive存檔](../Page/Internet_Archive.md "wikilink")）</small>

  - [ST MO的Special
    Blog](https://web.archive.org/web/20080822000834/http://www.blog.s-woman.net/st_haru/index.html)<small>（Internet
    Archive存檔）</small>

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:NHK晨間連續劇主演演員](../Category/NHK晨間連續劇主演演員.md "wikilink")
[Category:Seventeen模特兒](../Category/Seventeen模特兒.md "wikilink")
[Category:Non-no模特兒](../Category/Non-no模特兒.md "wikilink")
[Category:日劇學院賞最佳女主角得主](../Category/日劇學院賞最佳女主角得主.md "wikilink")
[Category:東京國際戲劇節最佳女主角得主](../Category/東京國際戲劇節最佳女主角得主.md "wikilink")
[Category:CONFiDENCE日劇大獎最佳女主角得主](../Category/CONFiDENCE日劇大獎最佳女主角得主.md "wikilink")

1.  [「収穫」の若手女優は門脇麦、波瑠＝民放連続ドラマで好演光る](http://www.jiji.com/jc/c?g=etm_30&k=2014060400730)
2.  \[ 波瑠が時代劇で初主演！宮部みゆき原作ドラマ『おそろし～三島屋変調百物語』がスタート\]\]
3.  [波瑠主演×大谷健太郎監督、沖縄県名護市舞台の「がじまる食堂の恋」9月公開](http://eiga.com/news/20140327/17/)
4.  [NHK朝ドラヒロインに波瑠さん　９月スタート「あさが来た」](http://www.sankei.com/entertainments/news/150312/ent1503120010-n1.html)
5.  [ヒロインは波瑠さん！平成27年度後期　連続テレビ小説「あさが来た」](http://www.nhk.or.jp/dramatopics-blog/1000/211312.html)
6.  [「non-no」卒業の波瑠「大変だし、つらい」女優業への本音
    朝ドラヒロインが目指す先](http://mdpr.jp/review/detail/1475187)

7.
8.  [20130620 笑っていいとも 本人談](https://www.youtube.com/watch?v=2A51z2YSelM)
9.  [【波瑠】月３０本映画鑑賞で刺激「毒を持ってる人を演じたい」](http://www.zakzak.co.jp/people/news/20140919/peo1409190830001-n2.htm)
10. [“平成の夏目雅子”と話題
    ＮＨＫ朝ドラ主演波瑠が持つオーラ](http://www.nikkan-gendai.com/articles/view/geino/158077)
11. [波瑠:夏目雅子似？朝ドラ新ヒロイン　起用理由は「品位」](http://mainichi.jp/mantan/news/20150312dyo00m200080000c.html)
12. [波瑠、まるで夏目雅子の生まれ変わり　オジサンの妄想、完璧に演じる](http://withnews.jp/article/f0150111001qq000000000000000G0010601qq000011371A)
13. [波瑠、故・夏目雅子さんを熱演\!
    藤原竜也の相手役に抜てき](http://www.oricon.co.jp/news/2026544/full/)
14.
15.
16. [<span lang="ja">「家売るオンナ」がコンフィデンス賞、主演賞は波瑠<span>](http://www.oricon.co.jp/article/31345/)