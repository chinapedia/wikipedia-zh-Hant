[Meyerbaltikum.jpg](https://zh.wikipedia.org/wiki/File:Meyerbaltikum.jpg "fig:Meyerbaltikum.jpg")
[Bundesarchiv_R_49_Bild-0705,_Polen,_Herkunft_der_Umsiedler,_Karte.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_R_49_Bild-0705,_Polen,_Herkunft_der_Umsiedler,_Karte.jpg "fig:Bundesarchiv_R_49_Bild-0705,_Polen,_Herkunft_der_Umsiedler,_Karte.jpg")

**波罗的海德意志人**（）指[波罗的海东岸](../Page/波罗的海.md "wikilink")[爱沙尼亚和](../Page/爱沙尼亚.md "wikilink")[拉脱维亚的](../Page/拉脱维亚.md "wikilink")[德意志人居民](../Page/德意志人.md "wikilink")。尽管波罗的海的德意志人占人口少数
，但从[12世纪到](../Page/12世纪.md "wikilink")[20世纪初](../Page/20世纪.md "wikilink")，波罗的海的德意志人控制着这两地的政治、经济、教育和文化。自从12世纪初[拉丁语在波罗的海东岸地区衰落](../Page/拉丁语.md "wikilink")，[德语迅速取而代之](../Page/德语.md "wikilink")，成为一切正式文件的官方语言，爱沙尼亚和拉脱维亚的所有城镇村庄，一律采用德文名字，为时达数百年之久直到19世纪90年代。

数百年来波罗的海的德意志人是爱沙尼亚和拉脱维亚的地主，他们是这地区的实际统治阶层。政治上他们在1710年之前只听命于[瑞典帝国](../Page/瑞典帝国.md "wikilink")，
在1917年之前听命于[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")。爱沙尼亚和拉脱维亚的本土原居民，大多数是居住在乡村的[农奴](../Page/农奴.md "wikilink")，不享有同等的权利。这种情况延续到1917年俄罗斯覆灭，1918年爱沙尼亚和拉脱维亚脱离俄国独立为止。爱沙尼亚和拉脱维亚独立后推行土地改革，剥夺波罗的海德意志人地主的土地。此后多数波罗的海的德国人成为爱沙尼亚国或拉脱维亚国的公民。

波罗的海德国人的历史在1939年骤然告终，[莫洛托夫—里賓特洛甫條約的秘密附加協議書劃分出德蘇勢力範圍](../Page/莫洛托夫—里賓特洛甫條約.md "wikilink")－－愛沙尼亞與拉脫維亞被納入蘇聯勢力範圍，[希特勒将大批波罗的海德国人迁移到](../Page/希特勒.md "wikilink")[纳粹德国在二战中占领的](../Page/纳粹德国.md "wikilink")[波兰西部](../Page/波兰.md "wikilink")。1944年[苏联占领爱沙尼亚和拉脱维亚国](../Page/苏联.md "wikilink")，波罗的海德国人再也没有返回爱沙尼亚和拉脱维亚的家园。

时至今日，波罗的海德国人的后裔，分布世界各地方，以[德国和](../Page/德国.md "wikilink")[加拿大人数最多](../Page/加拿大.md "wikilink")。

## 知名人物

  - [阿列克謝二世](../Page/阿列克謝二世.md "wikilink")
  - [卡尔·安斯特·冯·贝尔](../Page/卡尔·安斯特·冯·贝尔.md "wikilink")
  - [米哈伊尔·博格达诺维奇·巴克莱·德托利](../Page/米哈伊尔·博格达诺维奇·巴克莱·德托利.md "wikilink")
  - [法比安·戈特利布·冯·别林斯高晋](../Page/法比安·戈特利布·冯·别林斯高晋.md "wikilink")
  - [沃納·貝根格林](../Page/沃納·貝根格林.md "wikilink")
  - [贝勒 (汉学家)](../Page/贝勒_\(汉学家\).md "wikilink")
  - [格奥尔格·达豪](../Page/格奥尔格·达豪.md "wikilink")
  - [海因茨·艾哈特](../Page/海因茨·艾哈特.md "wikilink")
  - [約翰·弗里德里希·馮·埃施朔爾茨](../Page/約翰·弗里德里希·馮·埃施朔爾茨.md "wikilink")
  - [乔治·汉宁金-胡恩](../Page/乔治·汉宁金-胡恩.md "wikilink")
  - [盖沙令](../Page/盖沙令.md "wikilink")
  - [萊昂內爾·基席里茨基](../Page/萊昂內爾·基席里茨基.md "wikilink")
  - [奧托·馮·蓋茲比](../Page/奧托·馮·蓋茲比.md "wikilink")
  - [亚当·约翰·冯·克鲁森施滕](../Page/亚当·约翰·冯·克鲁森施滕.md "wikilink")
  - [恩斯特·吉迪恩·冯·劳东](../Page/恩斯特·吉迪恩·冯·劳东.md "wikilink")
  - [海因里希·楞次](../Page/海因里希·楞次.md "wikilink")
  - [沃爾夫岡·呂特](../Page/沃爾夫岡·呂特.md "wikilink")
  - [保羅·馮·利林費爾德](../Page/保羅·馮·利林費爾德.md "wikilink")
  - [亞歷山大·馮·米登多夫](../Page/亞歷山大·馮·米登多夫.md "wikilink")
  - [葉夫根尼·米勒](../Page/葉夫根尼·米勒.md "wikilink")
  - [弗拉基米爾·德米特里耶維奇·納博科夫](../Page/弗拉基米爾·德米特里耶維奇·納博科夫.md "wikilink")
  - [亚历山大·冯·厄廷根](../Page/亚历山大·冯·厄廷根.md "wikilink")
  - [威廉·奥斯特瓦尔德](../Page/威廉·奥斯特瓦尔德.md "wikilink")
  - [保羅·馮·倫寧坎普](../Page/保羅·馮·倫寧坎普.md "wikilink")
  - [格奧爾格·威廉·里奇曼](../Page/格奧爾格·威廉·里奇曼.md "wikilink")
  - [阿爾弗雷德·羅森堡](../Page/阿爾弗雷德·羅森堡.md "wikilink")
  - [路德維希·馬克西米利安·埃爾溫·馮·施勃納－里希特](../Page/路德維希·馬克西米利安·埃爾溫·馮·施勃納－里希特.md "wikilink")
  - [艾哈特·施密特](../Page/艾哈特·施密特.md "wikilink")
  - [格奥尔格·奥古斯特·施维因富特](../Page/格奥尔格·奥古斯特·施维因富特.md "wikilink")
  - [托馬斯·約翰·塞貝克](../Page/托馬斯·約翰·塞貝克.md "wikilink")
  - [瓦西里·雅可夫列维奇·斯特鲁维](../Page/瓦西里·雅可夫列维奇·斯特鲁维.md "wikilink")（[斯特魯維家族](../Page/斯特魯維家族.md "wikilink")）
  - [弗兰克·蒂斯](../Page/弗兰克·蒂斯.md "wikilink")
  - [爱德华·托尔](../Page/爱德华·托尔.md "wikilink")
  - [雅各布·馮·烏也斯庫爾](../Page/雅各布·馮·烏也斯庫爾.md "wikilink")
  - [罗曼·冯·恩琴](../Page/罗曼·冯·恩琴.md "wikilink")
  - [费迪南德·冯·弗兰格尔](../Page/费迪南德·冯·弗兰格尔.md "wikilink")（[法蘭格爾家族](../Page/法蘭格爾家族.md "wikilink")）
  - [彼得·尼古拉耶维奇·弗兰格尔](../Page/彼得·尼古拉耶维奇·弗兰格尔.md "wikilink")
  - [弗里德里希·詹德](../Page/弗里德里希·灿德尔.md "wikilink")
  - [瓦尔特·察普](../Page/瓦尔特·察普.md "wikilink")
  - [鋼和泰](../Page/鋼和泰.md "wikilink")

## 延伸閱讀

  - Helmreich E.C. (1942) [The return of the Baltic
    Germans](http://www.jstor.org/view/00030554/di960830/96p01493/0).
    *[The American Political Science
    Review](../Page/The_American_Political_Science_Review.md "wikilink")*
    36.4, 711–716.
  - Whelan, Heide W. (1999). *Adapting to Modernity: Family, Caste and
    Capitalism among the Baltic German Nobility*. Ostmitteleuropa in
    Vergangenheit und Gegenwart, vol. 22. Cologne: Böhlau Verlag, 1999.
    ISBN 3-412-10198-2
  - Hiden, John W. (1970). [The Baltic Germans and German policy towards
    Latvia
    after 1918](http://www.jstor.org/view/0018246x/di013388/01p02712/0).
    *[The Historical
    Journal](../Page/The_Historical_Journal.md "wikilink")* 13.2,
    295–317.
  - Hiden, John (1987). *The Baltic States and Weimar Ostpolitik*.
    Cambridge: Cambridge University Press. ISBN 0-521-89325-9
  - Anders Henriksson (1983). *The Tsar's Loyal Germans. The Riga
    Community: Social Change and the Nationality Question, 1855–1905*.
    Boulder, CO: East European Monographs. ISBN 0-88033-020-1
  - Mikko Ketola (2000). *The Nationality Question in the Estonian
    Evangelical Lutheran Church, 1918–1939*. Helsinki: Publications of
    the Finnish Society of Church History. ISBN 952-5031-17-9

## 外部連結

  - [A Baltic German
    site.](https://web.archive.org/web/20120204141016/http://www.deutschbalten.de/daten1.htm)
  - [The association of German Baltic
    Nobility](http://www.baltische-ritterschaften.de/) (rulers of
    Estland, Livland and Kurland between 1252 and 1918) – also see
    [English language
    version](https://web.archive.org/web/20080304100811/http://www.baltische-ritterschaften.de/Englische%20Version/index-engl.htm)
  - [Estonian Manors Portal](http://www.mois.ee/english) the English
    version introduces 438 well-preserved manors historically owned by
    the Baltic Germans (Baltic nobility)

[波罗的海德意志人](../Category/波罗的海德意志人.md "wikilink")
[Category:波罗的海](../Category/波罗的海.md "wikilink")
[Category:德国歷史](../Category/德国歷史.md "wikilink")
[Category:爱沙尼亚裔德国人](../Category/爱沙尼亚裔德国人.md "wikilink")
[Category:拉脱维亚裔德国人](../Category/拉脱维亚裔德国人.md "wikilink")
[Category:立陶宛裔德国人](../Category/立陶宛裔德国人.md "wikilink")
[Category:德国裔爱沙尼亚人](../Category/德国裔爱沙尼亚人.md "wikilink")
[Category:德国裔拉脱维亚人](../Category/德国裔拉脱维亚人.md "wikilink")
[Category:德国裔立陶宛人](../Category/德国裔立陶宛人.md "wikilink")
[Category:愛沙尼亞歷史](../Category/愛沙尼亞歷史.md "wikilink")
[Category:拉脫維亞歷史](../Category/拉脫維亞歷史.md "wikilink")
[Category:立陶宛歷史](../Category/立陶宛歷史.md "wikilink")