**雜食豆齒鰻**，又稱**波路荳齒蛇鰻**、**硬骨篡**或**[土龍](../Page/土龍.md "wikilink")**，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目之下的一個](../Page/鰻鱺目.md "wikilink")[種](../Page/種.md "wikilink")。成鰻身長超過2尺。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[孟加拉](../Page/孟加拉.md "wikilink")、[印度](../Page/印度.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")。

## 特徵

本魚體延長。肛門在身體中央或之後。齒顆粒狀，大小不等，由數列排成齒帶。前上頜骨齒，齒顆粒狀且呈帶狀排列，與鋤骨齒相互緊接。鋤骨齒3至4列。上唇無肉褶。背鰭起點在胸鰭之後。脊椎骨數160。體長為體高之48.7倍；尾長為頭與軀幹之2.1倍；頭長為吻長之5.4倍；吻長為眼徑之2.8倍。上唇腹面有一突起。後鼻孔位於眼前。前鼻孔成管狀。口裂超過眼之後緣。背鰭、臀鰭在近尾端處結束，二者非常低。脊椎骨171至173枚。體長可達100公分。

## 生態

本魚棲息在沿岸及河口區之泥質海底，常將身體埋於泥沙中，伺機捕食小魚及[甲殼類等](../Page/甲殼類.md "wikilink")，屬肉食性。

## 經濟利用

有輕微經濟價值，民間利用網袋等捕獲作為生鮮出售\[1\]，部分地區當作[食補的食材之一](../Page/食補.md "wikilink")，或以藥酒的形態處理。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[boro](../Category/豆齒鰻屬.md "wikilink")

1.  [Pisodonophis boro
    (Hamilton, 1822)](http://www.fishbase.org/summary/Pisodonophis-boro.html)