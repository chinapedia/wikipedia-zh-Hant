**波道帕耶**（，Bodawpaya，），《[清史稿](../Page/清史稿.md "wikilink")》稱之為**孟雲**，[雍笈牙之子](../Page/雍笈牙.md "wikilink")，[緬甸](../Page/緬甸.md "wikilink")[貢榜王朝的君主之一](../Page/貢榜王朝.md "wikilink")。1782年，波道帕耶正式接任貢榜王朝君主，統治上緬甸地區，並將該王朝國都移至為於中緬甸的[阿馬拉布拉城市](../Page/阿馬拉布拉.md "wikilink")。

任內，他進行了兩次社會普查，根據普查結果加強行政管理和改革稅收，並發展水利與農業生產，又命人編纂《[新緬甸史](../Page/新緬甸史.md "wikilink")》。同時他也將阿拉干併入緬甸。

除此，他一生擁有200位后妃，且有120位子女。长子[扎多敏梭](../Page/扎多敏梭.md "wikilink")，是[实皆王](../Page/巴吉道.md "wikilink")、[沙耶瓦底王之父](../Page/沙耶瓦底王.md "wikilink")。

[Category:緬甸君主](../Category/緬甸君主.md "wikilink")