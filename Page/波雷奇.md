**波雷奇**（[克罗地亚语](../Page/克罗地亚语.md "wikilink")：****）位于[克罗地亚西北部](../Page/克罗地亚.md "wikilink")[伊斯特拉半岛](../Page/伊斯特拉半岛.md "wikilink")[伊斯特拉县的一座城市](../Page/伊斯特拉县.md "wikilink")，面积53.7平方公里，人口17,460人（2001年）。

## 外部链接

  - [Poreč - Croatian National Tourist
    Board](https://web.archive.org/web/20070929110741/http://www.croatia.hr/English/Destinacije/Opcenito.aspx?idDestination=336)
  - [Porec Tourist Office Official Website](http://www.istria-porec.com)
  - [Poreč Istria Tourist Board
    page](http://www.istra.hr/en/where/cities_and_towns/13-ch-0?&l_over=1)
  - [Istria and Its People](http://www.istrianet.org/istria/index.html)
  - [Poreč 3D virtual exploration](http://www.porec360.com/)
  - [Poreč old
    postcards](https://web.archive.org/web/20060216095832/http://www.hister.org/arhivsr/porec/porec_1.html)

[Category:克罗地亚城市](../Category/克罗地亚城市.md "wikilink")