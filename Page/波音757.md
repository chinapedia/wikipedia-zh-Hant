**波音757**為[美國](../Page/美國.md "wikilink")[波音公司開發的中型单通道窄体民航客机](../Page/波音公司.md "wikilink")，原設計為[美国東方航空及](../Page/美国東方航空.md "wikilink")[英國航空取代旗下的](../Page/英國航空.md "wikilink")[波音727](../Page/波音727.md "wikilink")，本客機於1983年投入服務，並於2005年11月18日停產，共生產了1,050架，最後一架已交付[上海航空](../Page/上海航空.md "wikilink")\[1\]。

波音757可被視為[波音最成功的計劃之一](../Page/波音公司.md "wikilink")，可是隨著銷售量於90年代末開始下跌，最終導致波音757于2005年停產，停產後產品空缺由[737](../Page/波音737.md "wikilink")-900ER代替。

波音757系列载客量在186-279人之间，最大[航程为](../Page/航程.md "wikilink")3100-3900海里（5900-7200公里），757-300的需求主要來自[美國](../Page/美國.md "wikilink")[紐約至](../Page/紐約.md "wikilink")[西歐的航線](../Page/西歐.md "wikilink")。截至2017年7月，全球共有666架波音757在服役中。\[2\]

## 簡介

[Boeing_757-200_(American_Airlines)_092.jpg](https://zh.wikipedia.org/wiki/File:Boeing_757-200_\(American_Airlines\)_092.jpg "fig:Boeing_757-200_(American_Airlines)_092.jpg")波音757-200客機\]\]
[newdeltalivery.jpg](https://zh.wikipedia.org/wiki/File:newdeltalivery.jpg "fig:newdeltalivery.jpg")波音757-232客機\]\]
波音757（於起初發展階段名為7N7）由[波音公司設計](../Page/波音公司.md "wikilink")，用于替换[波音727](../Page/波音727.md "wikilink")，并在客源较少的航线上补充[波音767](../Page/波音767.md "wikilink")。相比起原構思的波音727-300（727-200的加長版），757擁有較新的設計，包括採用雙引擎、雙人操作的駕駛室。最初設計的757亦世襲至727，具有「T型垂直尾翼尾」（T-tail），雖然T型尾翼擁有風阻小的優點，但因為容易使飛機[失速](../Page/失速.md "wikilink")，最終設計仍使用傳統的垂直尾翼。此外757与[707](../Page/波音707.md "wikilink")、[727和](../Page/波音727.md "wikilink")[737采用相同的上机身直径](../Page/波音737.md "wikilink")。这保证了757在高温、高原条件下的表现并增加了[最大起飞重量](../Page/最大起飞重量.md "wikilink")（MTOW）。\[3\]

757為波音第一款航機使用非美國生產的[發動機](../Page/發動機.md "wikilink")—
[勞斯萊斯RB](../Page/勞斯萊斯.md "wikilink")211系列发动机。後來[普惠](../Page/普惠.md "wikilink")
（Pratt & Whitney）
另提供PW2000系列可供選擇。本來[通用電氣亦打算提供CF](../Page/通用電氣.md "wikilink")6-32型號，但最後因得不到航空公司垂青而取消計劃。

在[空中客车A321LR投产前](../Page/空中客车A320neo.md "wikilink")，波音757擁有窄體客機中最大的航程，在滿載200名乘客的情況下可飛行超過7,200公里，這航程甚至比超音速[協和飛機更大](../Page/協和飛機.md "wikilink")（協和飛機的航程有6,500公里），使它足以橫越大西洋的續航距離，亦是一款最早獲得[雙發延程飛行（ETOPS）評級之一的民航客機](../Page/雙發延程飛行.md "wikilink")。757的載客量比[波音727多出](../Page/波音727.md "wikilink")50人，更符合經濟效益。

[波音於](../Page/波音.md "wikilink")757上大量地使用與[767相同的部件](../Page/波音767.md "wikilink")，而兩款飛機均獲得相同的[美國聯邦航空局評級](../Page/美國聯邦航空局.md "wikilink")，即飛行員只須受其中一款型號的訓練及測試，就能同時獲准飛行另一型號。

757的性能非常優異，亦因其高的爬升速度而不時被稱為「火箭飛機」（Rocket
Plane），在最大起飛重量的情況下，757能比其他商業客機在較短的時間內爬升至41,000尺。另有一些航空公司都選用757來往氣候較熱和地勢較高的目的地，例如[墨西哥城](../Page/墨西哥城.md "wikilink")，因為它在以上地方的性能亦比其他機型出色。基於以上情況，[墨西哥總統亦選擇](../Page/墨西哥總統.md "wikilink")757作為其[行政專機](../Page/行政專機.md "wikilink")。雖然757是被設計為取代727的客機，但某些航空公司卻以757的窄體客機中最大航程的特點，用來取代載客量相若、四引擎、及耗油量高的[707客機](../Page/波音707.md "wikilink")。

可是，757必須要有75%或以上的載客率，才可以使航班有盈利，令757只能使用於高密度航線。另外，1990年代隨著[空中巴士的](../Page/空中巴士.md "wikilink")[A321投入競爭](../Page/空中巴士A320.md "wikilink")，757的銷量大減。波音終於在2004年銷售量達到1,000架時便宣佈把757停產。

757停產後，美國開始放寬[雙發延程飛行的限制](../Page/雙發延程飛行.md "wikilink")，越來越多航空公司開始使用757執飛跨大西洋航線\[4\]。757的轉售價值於停產後有所提升。事實上，[美國大陸航空於](../Page/美國大陸航空.md "wikilink")2004年12月29日訂單中除了訂購新的[787-8和](../Page/波音787.md "wikilink")[737-800客機外](../Page/波音737.md "wikilink")，還包括10架二手757-300
（購自聯美航空）。

另外，757雖然是一款窄體客機，但基於其[尾流紊流度](../Page/尾流紊流度.md "wikilink")（Wake
Turbulence）較其他窄體客機大，因此，在航空交通管制上被列為需要額外間隔空間和時間的「重型飛機」級別。

## 型號

基本上只生產了兩種型號。其中**757-200**的機身比**757-300**短，但有較長的巡航距離。757-100型原本為其中一種型號，但從來沒有投產

### 757-100

原設計為直接代替[727的](../Page/波音727.md "wikilink")150-座位客機。但因不引起業界的興趣，故沒有投產。

[Finnair_757_EFHK.jpg](https://zh.wikipedia.org/wiki/File:Finnair_757_EFHK.jpg "fig:Finnair_757_EFHK.jpg")波音757-200客機\]\]

### 757-200

757-200為最後定案的版本並佔了757系列的大多數。757-200亦有貨運型（757-200F）及客貨混合型（757-200M）。90年代後期，一些客運型757-200被改裝為貨機。

客運型757-200有兩種不同的艙門佈置。一款為每邊設3扇標準艙門，於機翼後方再加一扇較細的緊急逃生門，全部8扇門均設有充氣逃生滑梯。另一款則為每邊設3扇標準艙門（2扇於機翼前方，另一扇則於機翼後方），再於每邊機翼位置加設2扇“嵌入式”
（plug-type） 逃生門，取代設於機翼後方的標準艙門。

  - 757-200ER：加大航程型
    （Extended-Range），從來沒有完成設計階段。一些航空公司稱旗下總重量較高的-200型號為-200ER，特別是洲際航機。
  - 757-200PF：貨運型，1985年[美國](../Page/美國.md "wikilink")[聯合包裹服務公司](../Page/聯合包裹服務公司.md "wikilink")（UPS）訂購後開始生產。
  - 757-200SF：為DHL製造的特別貨運版本。
  - 757-200PCF：由Precision
    Conversions公司以757-200客运型改造的货运版本。相比于标准的757-200系列，靠近驾驶舱一侧的舱门改小且更加靠近机头。

[Condor_B753_D-ABOF_MUC.jpg](https://zh.wikipedia.org/wiki/File:Condor_B753_D-ABOF_MUC.jpg "fig:Condor_B753_D-ABOF_MUC.jpg")波音757-300客機\]\]

### 757-300

\-300 為-200
的加長型，雖提升了載客量但卻換來航程的減少，1998年8月首次飛行。757-300的載客量為252人，飛行距離為3,500海里。此型號飛機共有8扇標準艙門，另外4扇置於機翼位置（每邊兩扇）。757-300還擁有與[波音737新世代相似的客艙](../Page/波音737新世代.md "wikilink")，與[波音777客艙協調](../Page/波音777.md "wikilink")。

[C-32.jpg](https://zh.wikipedia.org/wiki/File:C-32.jpg "fig:C-32.jpg")C-32，757的子類型，美國副總統的專機(空軍二號)\]\]

### 私人及軍用型號

[美國空軍使用波音](../Page/美國空軍.md "wikilink")757作為運載貴賓之用，定名為C-32。這些C-32被經常用作接載美國副總統之用，其呼號為“[空軍二號](../Page/空軍二號.md "wikilink")”。紐西蘭皇家空軍亦有使用2架波音757來運送軍隊及貴賓。

[阿根廷亦使用](../Page/阿根廷.md "wikilink")1架波音757作為總統專機，稱為"Tango
1"。[墨西哥同樣地使用波音](../Page/墨西哥.md "wikilink")757作為總統及貴賓專機。[沙地阿拉伯皇室則利用一架波音](../Page/沙地阿拉伯.md "wikilink")757作為“飛行醫院”。

[2004年美國總統選舉中](../Page/2004年美國總統選舉.md "wikilink")，候選人美國[參議員](../Page/美國參議院.md "wikilink")[約翰·克里曾包租了](../Page/約翰·克里.md "wikilink")1架波音757-200作為競選用專機，該機別號"Freedom
Bird"。

另最少有4架波音757為私人擁有的。一架為超級市場巨人 Ronald
Burkie，另外兩架為[微軟創辦人之一](../Page/微軟.md "wikilink")[保羅·艾倫所有](../Page/保羅·艾倫.md "wikilink")。[保羅·艾倫使用其中一架作私人用途](../Page/保羅·艾倫.md "wikilink")（編號：N757AF），後來這架飛機被賣給[唐納德·川普作私人用途](../Page/唐納德·川普.md "wikilink")。還有一架是特朗普的。另一架（編號
N756AF）則作為團隊專機。

## 服役期

[B-2876_Boeing_B.752_Shanghai_Airlines_(12205120454).jpg](https://zh.wikipedia.org/wiki/File:B-2876_Boeing_B.752_Shanghai_Airlines_\(12205120454\).jpg "fig:B-2876_Boeing_B.752_Shanghai_Airlines_(12205120454).jpg")

大部份的757都是服役於美國的航空公司的本土航線上，其中[達美航空擁有最大的](../Page/達美航空.md "wikilink")757機隊，共184架（包括原[西北航空的](../Page/西北航空.md "wikilink")61架），[聯合航空有](../Page/聯合航空.md "wikilink")155架（包括原[美國大陸航空的](../Page/美國大陸航空.md "wikilink")62架）、[美國航空有](../Page/美國航空.md "wikilink")107架（包括原[全美航空的](../Page/全美航空.md "wikilink")24架）、[美西航空亦有使用](../Page/美西航空.md "wikilink")757客機。一些航空公司投放757客機於越洋航線上，如[冰島航空](../Page/冰島航空.md "wikilink")、美國大陸航空、美國航空以及[ATA航空](../Page/ATA航空.md "wikilink")。[文萊皇家航空公司及](../Page/文萊皇家航空公司.md "wikilink")[尼泊爾航空利用](../Page/尼泊爾航空.md "wikilink")757高經濟效益的特性，使航空公司能開辦前往歐洲的洲際航線。很多[第三世界以](../Page/第三世界.md "wikilink")757取替同廠的[707客機](../Page/波音707.md "wikilink")。

757客機成為一些專營假日或包機服務航空公司的熱門機種，例如英國的Thomas Cook Airlines、（First Choice
Airways）、[君主航空](../Page/君主航空.md "wikilink")（Monarch
Airlines）、[泰坦航空](../Page/泰坦航空.md "wikilink")（Titan
Airways）、Excel
Airways、Astraeus及Thomsonfly。757的航程能從英國直達[南非](../Page/南非.md "wikilink")，亦可到鄰近的[阿姆斯特丹和](../Page/阿姆斯特丹.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。

經過高銷量時期，757客機的銷量從1990年代中期開始急劇下降。航空公司傾向將757投放於長程但客量小，以及新航線上，當航線客量上升，航空公司不得不以有更高成本效益的廣體客機接棒。

短程市場上，航空公司認為757太大，757需要75%客量才能有純利，短程航線都使用[波音737](../Page/波音737.md "wikilink")，以及[A320家族](../Page/空中客车A320.md "wikilink")。雖然A321和737-900的航程比757短，但仍足以勝任90%的757航線，757客機成為市場轉型的犧牲品。不過，757在美國的航空公司中有新的任務，它的航程能有效地服務於越大西洋的長程少客量航線，例如來往美國東岸樞紐機場以及[格拉斯哥](../Page/格拉斯哥.md "wikilink")、[香農等地](../Page/香農.md "wikilink")。

第1,050架，亦是最後一架757客機，是屬於[上海航空公司](../Page/上海航空公司.md "wikilink")，於2004年10月28日從Renton的生產線中下線。757-200將會被737-900ER暫時取代，長遠會被[波音737
MAX
9接替](../Page/波音737MAX.md "wikilink")，而757-300則由[787-8廣體客機接替](../Page/波音787.md "wikilink")。

近年，一些757客機出現有毒引擎燃油氣體進入駕駛艙的問題，影響飛行員工作，但問題並沒有如[BAe
146的那麼廣泛](../Page/BAe_146.md "wikilink")。

1992年9月10日，[中国西南航空开始使用配备](../Page/中国西南航空.md "wikilink")[RB211发动机的](../Page/勞斯萊斯RB211.md "wikilink")**波音757**飞机服务于[成都](../Page/成都.md "wikilink")—拉萨航线，从而拉开了波音757飞机高原运行的序幕。其后开通了成都—昌都，重庆—拉萨，昌都—拉萨，成都—拉萨—加德满都，成都-迪庆香格里拉等高原航线。因其卓越的高原飞行性能，波音757飞机被人们习惯的称为高原之鹰。

2006年7月12日，装备RNP导航设备的波音757飞机首次试飞世界上飞行难度最大的西藏[林芝米林机场](../Page/林芝米林机场.md "wikilink")，并在9月22日完成首航。

至2010年，以[RB211发动机为动力的](../Page/勞斯萊斯RB211.md "wikilink")**波音757**飞机已经在[青藏高原安全飞行](../Page/青藏高原.md "wikilink")18周年。

[continental.airlines.b757-200.takeoff.arp.jpg](https://zh.wikipedia.org/wiki/File:continental.airlines.b757-200.takeoff.arp.jpg "fig:continental.airlines.b757-200.takeoff.arp.jpg")的757-200裝上了翼尖小翼\]\]
雖然757现在已經停產，但Aviation Partners
Inc.推出新的適用於757上的[翼尖小翼](../Page/翼尖小翼.md "wikilink")（winglet），令續航距離和耗油效率有所提升。[美國航空](../Page/美國航空.md "wikilink")、[美國大陸航空](../Page/美國大陸航空.md "wikilink")、西北航空和[冰島航空等公司已著手為旗下的](../Page/冰島航空.md "wikilink")757加裝此小翼，目標為[波音737-400或](../Page/波音737.md "wikilink")[麥道](../Page/麥道.md "wikilink")[MD-80均不能完成](../Page/MD-80.md "wikilink")，或需求不足以使用[波音767或](../Page/波音767.md "wikilink")[空中巴士A300](../Page/空中巴士A300.md "wikilink")/[A310的航線](../Page/空中客车A310.md "wikilink")。

2018年，波音与罗克伟尔柯林斯联合推出新的波音757航电系统，以三块液晶显示屏为主的全电子化玻璃座舱替代原有的半电子化玻璃座舱，可以使其航电系统与现在的[波音787和](../Page/波音787.md "wikilink")[波音737MAX保持一致以减少从](../Page/波音737MAX.md "wikilink")767以外机型转驾到757及训练成本，同时新的航电系统比原有系统减轻了150榜以提升燃油效率。

## 規格

<table>
<thead>
<tr class="header">
<th></th>
<th style="text-align: center;"><p>757-200</p></th>
<th style="text-align: center;"><p>757-200F</p></th>
<th style="text-align: center;"><p>757-300</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>首次飛行日期</strong></p></td>
<td style="text-align: center;"><p>1982年2月19日</p></td>
<td style="text-align: center;"><p>1987年8月13日</p></td>
<td style="text-align: center;"><p>1998年8月2日</p></td>
</tr>
<tr class="even">
<td><p><strong>飛機尺寸</strong></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>總長</p></td>
<td style="text-align: center;"><p>47.32 公尺</p></td>
<td style="text-align: center;"><p>54.41 公尺</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>高（垂直尾翼頂端）</p></td>
<td style="text-align: center;"><p>13.56 公尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>機身直徑</p></td>
<td style="text-align: center;"><p>3.76 公尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>最大機艙寬度</p></td>
<td style="text-align: center;"><p>3.54 公尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>翼展</p></td>
<td style="text-align: center;"><p>38.05 公尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>機翼面積（參考）</p></td>
<td style="text-align: center;"><p>185.25 平方公尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>輪距</p></td>
<td style="text-align: center;"><p>18.29 公尺</p></td>
<td style="text-align: center;"><p>22.35 公尺</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p><strong>基本運行數據</strong></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>發動機</p></td>
<td style="text-align: center;"><p>兩具 <a href="../Page/勞斯萊斯.md" title="wikilink">勞斯萊斯</a><a href="../Page/勞斯萊斯RB211.md" title="wikilink">RB211-535</a> 或 <a href="../Page/普惠.md" title="wikilink">普惠</a> PW2037 或<br />
<a href="../Page/普惠.md" title="wikilink">普惠</a> PW2040 或 <a href="../Page/普惠.md" title="wikilink">普惠</a> PW2043</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>發動機推力</p></td>
<td style="text-align: center;"><p>163-193 kN</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>典型乘客人</p></td>
<td style="text-align: center;"><p>200（2級）- 228（1級）</p></td>
<td style="text-align: center;"><p>0</p></td>
<td style="text-align: center;"><p>243（2級）- 280（1級）</p></td>
</tr>
<tr class="even">
<td><p>續航距離 （w/最大乘客數）</p></td>
<td style="text-align: center;"><p>7,275 公里</p></td>
<td style="text-align: center;"><p>5,834 公里</p></td>
<td style="text-align: center;"><p>6,421 公里</p></td>
</tr>
<tr class="odd">
<td><p>最大巡航速度</p></td>
<td style="text-align: center;"><p>0.8 馬赫（每小時870公里）</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>最高巡航高度</p></td>
<td style="text-align: center;"><p>42,000 尺</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p><strong>設計重量</strong></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td><p>最大滑行重量<br />
（Max taxi weight/kg）</p></td>
<td style="text-align: center;"><p>116,100 公斤<br />
256,000 磅</p></td>
<td style="text-align: center;"><p>122,930 公斤<br />
271,000 磅</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>最大起飛重量<br />
（Max takeoff weight/kg）</p></td>
<td style="text-align: center;"><p>115,650 公斤<br />
255,000 磅</p></td>
<td style="text-align: center;"><p>116,650 公斤<br />
255,000 磅</p></td>
<td style="text-align: center;"><p>122,470 公斤<br />
270,000 鎊</p></td>
</tr>
<tr class="even">
<td><p>最大著陸重量<br />
（Max landing weight/kg）</p></td>
<td style="text-align: center;"><p>95,250 公斤<br />
210,000 磅</p></td>
<td style="text-align: center;"><p>92,250 公斤<br />
210,000 磅</p></td>
<td style="text-align: center;"><p>101,600 公斤<br />
224,050 磅</p></td>
</tr>
<tr class="odd">
<td><p>無燃油最大重量<br />
（Max zero fuel weight/kg）</p></td>
<td style="text-align: center;"><p>85,300 公斤<br />
188,000 磅</p></td>
<td style="text-align: center;"><p>90,700 公斤<br />
200,000 磅</p></td>
<td style="text-align: center;"><p>95,260 公斤<br />
210,000 磅</p></td>
</tr>
<tr class="even">
<td><p>最大燃油容量（Max takeoff fuel payload/l）</p></td>
<td style="text-align: center;"><p>42,680 公升</p></td>
<td style="text-align: center;"><p>43,495 公升</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>Typical operating weight empty / kg</p></td>
<td style="text-align: center;"><p>59,350 公斤<br />
130,875 磅</p></td>
<td style="text-align: center;"><p>51,700 公斤<br />
114,000 磅</p></td>
<td style="text-align: center;"><p>64,580 公斤<br />
142,350 磅</p></td>
</tr>
<tr class="even">
<td><p>最大貨運容量（Bulk hold volume/m<small>3</small>）</p></td>
<td style="text-align: center;"><p>86.9 立方公尺</p></td>
<td style="text-align: center;"><p>86.9 立方公尺</p></td>
<td style="text-align: center;"><p>114.1 立方公尺</p></td>
</tr>
</tbody>
</table>

## 意外

  - 1990年10月2日，[厦门航空一架編號](../Page/厦门航空.md "wikilink") B-2510
    的[波音737型客機被](../Page/波音737.md "wikilink")[劫機](../Page/1990年廣州白雲機場劫機空難.md "wikilink")，因燃料不足，以及劫機者對機長施暴，飛機於是失控，於[廣州](../Page/廣州.md "wikilink")[舊白雲機場墜毀](../Page/舊白雲機場.md "wikilink")。該機上75名乘客和7名機組人員遇難，另有18名乘客受傷。此外，另有[中国南方航空編號B](../Page/中国南方航空.md "wikilink")-2812的[波音](../Page/波音.md "wikilink")757-200型客機（当时使用的是原中国民航，即后来中国国际航空的涂装）和[中国西南航空編號B](../Page/中国西南航空.md "wikilink")-2402的[波音707型客機](../Page/波音707.md "wikilink")（当时使用的是原中国民航，即后来中国国际航空的涂装）被撞击，分別有46名乘客遇難和有1名機組人員受傷，事后三架飞机全部报废[(1990年廣州白雲機場劫機相撞事件)](../Page/:1990年广州白云机场劫机相撞事件.md "wikilink")。
  - 1995年12月20日，[美國航空965號班機為波音](../Page/美國航空965號班機空難.md "wikilink")757-223型客機，於[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[邁亞密國際機場前往](../Page/邁亞密國際機場.md "wikilink")[哥倫比亞](../Page/哥倫比亞.md "wikilink")時，偏離航道並於[哥倫比亞撞山](../Page/哥倫比亞.md "wikilink")。155名乘客和8名機組人員中僅有4人生還。
  - 1996年2月6日，[伯根航空301號班機為波音](../Page/伯根航空301號班機.md "wikilink")757-225型客機，於[多明尼加起飛後](../Page/多明尼加.md "wikilink")9分鐘，因儀器故障導致機員接收錯誤訊息。其後飛機失控在空中旋轉，最後失速墜海，全機189人死亡。
  - 1996年10月2日，[秘魯航空603號班機為波音](../Page/秘魯航空603號班機.md "wikilink")757-23A型客機，於[秘魯](../Page/秘魯.md "wikilink")[Jorge
    Chávez
    國際機場起飛後不久後墜毀](../Page/Jorge_Chávez_國際機場.md "wikilink")，61位乘客和9位機組人員全部遇難。調查後發現飛機凊洗後未有把[皮托管的套除掉](../Page/皮托管.md "wikilink")，以致未得到正確的飛行資料而坠海。
  - 1997年3月10日，[遠東航空128号班機](../Page/遠東航空128号班機.md "wikilink")（波音757-200型客機，機身編號B-27001）原訂由高雄國際機場飛往台北松山機場，卻於下午2時許，被台灣旅客劉善忠以全身淋汽油方式威脅，要將客機劫往中國大陸，而於3時36分降落廈門高崎機場。機上計有旅客150人，機組人員8人。中國政府採「人機分離」模式，使機上旅客、機組員繼續其航程，並於當日晚上8時許，返抵台北，但留下劫機犯劉善忠偵辦。
  - 2001年9月11日，[美国航空77号班机](../Page/美国航空77号班机.md "wikilink")（757-223）和[联合航空93号班机](../Page/联合航空93号班机.md "wikilink")（757-222）为[911事件两架被劫持的飞机](../Page/911事件.md "wikilink")，其中77号航班撞入了美国国防部[五角大楼](../Page/五角大楼.md "wikilink")，而93号航班则在宾夕法尼亚的尚克斯维尔坠毁。两机上均无人生还。
  - 2002年7月1日，[DHL快遞公司](../Page/DHL.md "wikilink")611號航班為波音757-200F型貨機，於[瑞士和](../Page/瑞士.md "wikilink")[德國邊界的](../Page/德國.md "wikilink")[康士坦茨湖上空與](../Page/康士坦茨湖.md "wikilink")[巴什克利安航空2937號班機相撞](../Page/巴什克利安航空2937號班機.md "wikilink")。DHL貨機上兩名機師全部遇難，而巴什克利安航空2937號班機上60名乘客和9名機組人員亦全部遇難。
  - 2006年11月16日，[遠東航空306號班機](../Page/遠東航空306號班機.md "wikilink")（EF306）是一班從桃園國際機場起飛前往濟州國際機場的航班，載有129人和8名機組人員的航機在韓國濟州島上空準備下降著陸的時候，與另一班從仁川國際機場起飛前往新曼谷國際機場的泰國航空659號班機空中接近，導致兩機需緊急轉向迴避。意外導致遠東航空306號班機上有14名乘客和6名機組人員，共20人受傷；而泰國航空659號班機上則無人受傷，繼續航程。
  - 2018年11月9日，[牙买加航空256号班机事故一架从圭亚那首都喬治敦飞往多伦多的波音](../Page/牙买加航空256号班机事故.md "wikilink")757-200在起飞不久后发生机械故障，随后在迫降时坠毁，6人受伤。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [757官方资料](http://www.boeing.com/commercial/757family/)

## 相關條目

### 波音产品线

  - [B707](../Page/波音707.md "wikilink") -
    [B717](../Page/波音717.md "wikilink") -
    [B727](../Page/波音727.md "wikilink") -
    [B737](../Page/波音737.md "wikilink") -
    [B747](../Page/波音747.md "wikilink") -
    [B757](../Page/波音757.md "wikilink") -
    [B767](../Page/波音767.md "wikilink") -
    [B777](../Page/波音777.md "wikilink") -
    [B787](../Page/波音787.md "wikilink")

### 類似機型

  - [空中客車A321](../Page/空中客車A320#A321.md "wikilink")
  - [波音737-900](../Page/波音737#B737-900.md "wikilink")

<!-- end list -->

  - [圖-204](../Page/圖-204.md "wikilink")

{{-}}

[波音757](../Category/波音757.md "wikilink")
[Category:波音飞机](../Category/波音飞机.md "wikilink")
[Category:干线客机](../Category/干线客机.md "wikilink")
[Category:雙發噴射機](../Category/雙發噴射機.md "wikilink")

1.  [波音757官方网页](http://www.boeing.com/commercial/757family/index.html)
2.
3.  Birtles 2001, pp. 16-7.
4.