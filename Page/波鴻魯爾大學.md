[Audimax-Aussen.jpg](https://zh.wikipedia.org/wiki/File:Audimax-Aussen.jpg "fig:Audimax-Aussen.jpg")
[570_Im_Chinesischen_Garten.JPG](https://zh.wikipedia.org/wiki/File:570_Im_Chinesischen_Garten.JPG "fig:570_Im_Chinesischen_Garten.JPG")

**波鴻鲁尔大学**（；RUB）位于德國[波鸿](../Page/波鸿.md "wikilink")，建立于1962年，也是在新成立的[联邦德国时期成立的第一所大学](../Page/联邦德国.md "wikilink")。大学选址在[波鸿市区以外的绿色草原地区](../Page/波鸿.md "wikilink")[Querenburg](../Page/Querenburg.md "wikilink")，形成了一个独立校区。整个校园由几个靠近的相对独立的建筑构成。

在过去几年校园进行了大面积的校园维护改造工程。兴建了在各种教学楼中间的公用绿地。而以前这里几乎是寸草不生。原本波鴻魯爾大學计划修建的更大。不过现在很多建筑都已经不会再修建了，例如计划中的大学诊所等。为[医学专业所修建的两所建筑虽然竣工](../Page/医学.md "wikilink")，不过始终没有投入使用。他们长期以来都处于闲置状态，现在已经确定改建为私人企业。长年以来，波鴻魯爾大學一直处于改革中，也是德国高校改革试点大学之一。

## 建筑风格

波鴻魯爾大學的建筑风格时至今日依然是颇有争议的一个话题。因为在大学内，大学图书馆和[Audimax坐镇中央](../Page/Audimax.md "wikilink")，而在四周都是由四座完全对称的高大建筑辅以不同颜色而构成，这一设计体现了各个学科的内涵，也来自于某个不闻名的设计理念。它体现了大学就如同知识海洋中的一座港口，而每座建筑则是启航的船只。而Audimax更是如同一个牡蛎一样处于校区中。在90年代兴建的城市有轨电车的车站顶棚也相应的以海浪为外形。

## 學院

  - [基督教神学](../Page/基督教神学.md "wikilink")
  - [天主教神学](../Page/天主教神学.md "wikilink")
  - [哲学](../Page/哲学.md "wikilink")，[教育学和](../Page/教育学.md "wikilink")[大众传播学](../Page/大众传播学.md "wikilink")
  - [历史学](../Page/历史学.md "wikilink")
  - [语文学](../Page/语文学.md "wikilink")
  - [法学](../Page/法学.md "wikilink")
  - [经济学](../Page/经济学.md "wikilink")
  - [社会学](../Page/社会学.md "wikilink")
  - [东亚学](../Page/东亚学.md "wikilink")
  - [体育学](../Page/体育学.md "wikilink")
  - [心理学](../Page/心理学.md "wikilink")
  - [劳动学](../Page/劳动学.md "wikilink")
  - [土木工程](../Page/土木工程.md "wikilink")
  - [机械制造](../Page/机械制造.md "wikilink")
  - [电子学](../Page/电子学.md "wikilink")，[信息技术](../Page/信息技术.md "wikilink")
  - [数学](../Page/数学.md "wikilink")
  - [物理学](../Page/物理学.md "wikilink")，[天文学](../Page/天文学.md "wikilink")
  - [生物学](../Page/生物学.md "wikilink")
  - [化学](../Page/化学.md "wikilink")，[生物化学](../Page/生物化学.md "wikilink")
  - [地理学](../Page/地理学.md "wikilink")
  - [医学](../Page/医学.md "wikilink")

## 校园文化

大学内有诸多的组织和团体。每年学校举办一届波鸿国际影视节。从1967 大学学生总会AStA出版《波鸿学生报》。这是德国历史最悠久的学生报纸。

## 学费

[2006年](../Page/2006年.md "wikilink")[9月18日](../Page/9月18日.md "wikilink")，校方正式确定，对于波鴻魯爾大學所有在校学生征收按照学费法案的每学期500欧元的学费，该决定自2007年夏季学期开始实行。

自[2010年冬季學期開始](../Page/2010年.md "wikilink")，北威邦議會通過免除學費議案，因此校方今日只酌收社會捐費用，一共三百零三歐元。

## 参考文献

## 外部链接

  - [鲁尔大学官方网站](http://www.ruhr-uni-bochum.de/)

[B](../Category/德国大学.md "wikilink")
[Category:1962年創建的教育機構](../Category/1962年創建的教育機構.md "wikilink")
[波鸿鲁尔大学](../Category/波鸿鲁尔大学.md "wikilink")