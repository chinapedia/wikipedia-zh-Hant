**泰利·史高斯**（**Tales
Schutz**，），簡稱**T·史高斯**，生於[巴西](../Page/巴西.md "wikilink")[阿雷格里港](../Page/阿雷格里港.md "wikilink")，[巴西足球運動員](../Page/巴西.md "wikilink")，擁有[德國血統](../Page/德國.md "wikilink")，司職[前鋒](../Page/前鋒.md "wikilink")，曾奪得[香港甲組足球聯賽神射手](../Page/香港甲組足球聯賽.md "wikilink")，現時效力[巴西](../Page/巴西.md "wikilink")[南里奧格蘭德州球會](../Page/南里奧格蘭德州.md "wikilink")。

## 球員生涯

史高斯出身於[巴西甲組足球聯賽老牌球會](../Page/巴西甲組足球聯賽.md "wikilink")[保地花高](../Page/保地花高.md "wikilink")，曾被先後外借至[帕拉尼恩斯](../Page/帕拉尼恩斯.md "wikilink")、[阿斯杜德](../Page/阿什杜德足球俱乐部.md "wikilink")、[尼坦耶馬卡比](../Page/内坦亚马加比足球俱乐部.md "wikilink")、。其後先後效力過、[喬治羅尼亞](../Page/喬治羅尼亞比亞韋斯托克足球俱樂部.md "wikilink")、、等多支球隊。2006年，史高斯被[巴西甲組足球聯賽球隊](../Page/巴西甲組足球聯賽.md "wikilink")借用給[香港甲組足球聯賽球隊](../Page/香港甲組足球聯賽.md "wikilink")[南華](../Page/南華足球隊.md "wikilink")，表現出色以
17
個聯賽入球，成為[2006–07年度](../Page/2006年至2007年香港甲組足球聯賽.md "wikilink")[香港甲組足球聯賽神射手](../Page/香港甲組足球聯賽.md "wikilink")。2007年夏天，因與[南華未能就借用費達成協議](../Page/南華足球隊.md "wikilink")，史高斯離隊轉投[葡萄牙超級聯賽升班馬](../Page/葡萄牙超級聯賽.md "wikilink")[歷索斯](../Page/歷索斯體育俱樂部.md "wikilink")，不過上陣機會不多。到了2008年1月，冬季轉會市場期間，史高斯以超過一百萬港元破[香港轉會費紀錄的價格](../Page/香港.md "wikilink")，\[1\]正式轉會[南華](../Page/南華足球隊.md "wikilink")，並簽下四年合約，惟入球數字大不如前。[2008–09年球季結束後](../Page/2008年至2009年香港甲組足球聯賽.md "wikilink")，大部分[南華外援球員離隊](../Page/南華足球隊.md "wikilink")，史高斯是唯一留隊的球員。2011年初，史高斯返回[巴西治療傷勢](../Page/巴西.md "wikilink")。2011年夏天，史高斯與[南華約滿後未獲續約](../Page/南華足球隊.md "wikilink")，轉投[阿塞拜疆球會](../Page/阿塞拜疆.md "wikilink")。2012年，史高斯再轉投另一支[阿塞拜疆球會](../Page/阿塞拜疆.md "wikilink")。2013年9月，[香港甲組足球聯賽球會](../Page/香港甲組足球聯賽.md "wikilink")[流浪宣布羅致T史高斯](../Page/香港流浪足球會.md "wikilink")。\[2\]2014年4月20日，[流浪憑史高斯在](../Page/香港流浪足球會.md "wikilink")82分鐘近門窩利射入致勝一球以2–1擊敗[晨曦](../Page/晨曦.md "wikilink")。\[3\]2014年5月9日，[流浪與史高斯解約](../Page/香港流浪足球會.md "wikilink")。\[4\]

## 個人榮譽

  - [香港足球明星選舉最佳](../Page/香港足球明星選舉.md "wikilink")11人
    兩次（2008-09、2009-10季度）
  - [香港甲組足球聯賽神射手](../Page/香港甲組足球聯賽.md "wikilink") 一次（2006-07季度）
  - [香港高級組銀牌神射手](../Page/香港高級組銀牌.md "wikilink") 一次（2006-07季度）
  - [香港足總盃神射手](../Page/香港足總盃.md "wikilink") 一次（2006-07季度）

## 參考資料

## 外部連結

  - [泰利·史高斯](http://www.90minut.pl/kariera.php?id=6135) (90minut.pl)

[Category:南華球員](../Category/南華球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:保地花高球員](../Category/保地花高球員.md "wikilink")
[Category:帕拉尼恩斯球員](../Category/帕拉尼恩斯球員.md "wikilink")
[Category:喬治羅尼亞球員](../Category/喬治羅尼亞球員.md "wikilink")
[Category:歷索斯球員](../Category/歷索斯球員.md "wikilink")
[Category:德國裔巴西人](../Category/德國裔巴西人.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:阿雷格里港人](../Category/阿雷格里港人.md "wikilink")
[Category:香港巴西籍足球運動員](../Category/香港巴西籍足球運動員.md "wikilink")
[Category:葡萄牙外籍足球運動員](../Category/葡萄牙外籍足球運動員.md "wikilink")
[Category:香港外籍足球運動員](../Category/香港外籍足球運動員.md "wikilink")

1.  [南華逾百萬元簽
    T史高斯](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20080111&sec_id=25391&subsec_id=25392&art_id=10631571)
2.  [流浪馬其仕轉投元朗](http://orientaldaily.on.cc/cnt/sport/20130919/00286_008.html)《東方日報》
3.  [泰利史高斯英雄變罪人 10人流浪2：1挫晨曦](http://sports.now.com/home/news/details?id=9683344475)
    【now.com體育】2014/04/20
4.  [流浪已與泰利史高斯解約](http://news.takungpao.com.hk/paper/q/2014/0509/2466586.html)
    [大公網](../Page/大公網.md "wikilink") 2014年5月9日