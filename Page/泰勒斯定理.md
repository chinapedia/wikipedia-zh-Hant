[Thales'_Theorem_Simple.svg](https://zh.wikipedia.org/wiki/File:Thales'_Theorem_Simple.svg "fig:Thales'_Theorem_Simple.svg")

**泰勒斯定理**（）以[古希腊思想家](../Page/古希腊.md "wikilink")、科学家、哲学家[泰勒斯的名字命名](../Page/泰勒斯.md "wikilink")，其内容为：若*A*,
*B*,
*C*是[圆周上的三](../Page/圆.md "wikilink")[點](../Page/點.md "wikilink")，且*AC*是该圆的[直徑](../Page/直徑.md "wikilink")，那么∠*ABC*必然為[直角](../Page/直角.md "wikilink")。或者说，直径所对的[圆周角是直角](../Page/圆周角.md "wikilink")。该定理在[欧几里得](../Page/欧几里得.md "wikilink")《[几何原本](../Page/几何原本.md "wikilink")》第三卷中被提到并证明。

泰勒斯定理的[逆定理同样成立](../Page/定理#逆定理.md "wikilink")，即：[直角三角形中](../Page/直角三角形.md "wikilink")，直角的顶点在以[斜边为直径的圆上](../Page/斜边.md "wikilink")。

## 證明

### 证法一

以下證明主要使用兩個定理：

  - [三角形的](../Page/三角形.md "wikilink")[內角和等於](../Page/內角和.md "wikilink")180°
  - [等腰三角形的兩個底角相等](../Page/等腰三角形.md "wikilink")

設*O*為[圓心](../Page/圓心.md "wikilink")，因為*OA* = *OB* =
*OC*，所以△*OAB*和△*OBC*都是[等腰三角形](../Page/等腰三角形.md "wikilink")。因為等腰三角形底角相等，故有∠*OBC*
= ∠*OCB*，且∠*BAO* = ∠*ABO*。設*α* = ∠*BAO*，*β* =
∠*OBC*。在△*ABC*中，因为三角形的内角和等于180°，所以有

\[\alpha+\left( \alpha + \beta \right) + \beta = 180^\circ\]

\[{2}\alpha + {2}\beta =180^\circ\]

\[{2}( \alpha + \beta ) =180^\circ\]

\[\therefore \angle ABC = \alpha + \beta =90^\circ.\]

### 证法二

泰勒斯定理也可以用[三角学方法证明](../Page/三角学.md "wikilink")，证明如下：

令*O* =（0, 0）, *A* =（-1, 0）, *C* =（1,
0）。此时，*B*就是[单位圆](../Page/单位圆.md "wikilink")\((\cos \theta, \sin \theta)\)上的一点。我们将通过证明*AB*与*BC*
[垂直](../Page/垂直.md "wikilink")，即它们的[斜率之积等于](../Page/斜率.md "wikilink")–1，来证明这个定理。计算*AB*和*BC*的斜率：

\[m_{AB} = \frac{y_B - y_A}{x_B - x_A} = \frac{\sin \theta}{\cos \theta + 1}\]

\[m_{BC} = \frac{y_B - y_C}{x_B - x_C} = \frac{\sin \theta}{\cos \theta - 1}\]

并证明它们的积等于–1:

\[\begin{align}
&m_{AB} \cdot m_{BC}\\
=&\frac{\sin \theta}{\cos \theta + 1} \cdot \frac{\sin \theta}{\cos \theta - 1}\\
=&\frac{\sin ^2 \theta}{\cos ^2 \theta -1}\\
=&\frac{\sin ^2 \theta}{-\sin ^2 \theta}\\
=&-1
\end{align}\]

注意以上证明过程中运用了[毕达哥拉斯三角恒等式](../Page/三角恒等式#基本關係.md "wikilink")\(\sin ^2 \theta + \cos ^2 \theta = 1\)。

### 逆定理的證明

此證明使用兩線的[向量形成](../Page/向量.md "wikilink")[直角三角形](../Page/直角三角形.md "wikilink")，[若且唯若其](../Page/若且唯若.md "wikilink")[內積為零](../Page/內積.md "wikilink")。設有直角三角形*ABC*，和以*AC*為直徑的圓*O*。設*O*在原點，以方便計算。则*AB*和*BC*的內積為：

\[(A-B)\times (B-C)=(A-B)\times(B+A)=|A|^2-|B|^2=0\]

\[|A|=|B|\]

故*A*和*B*與圓心等距，即*B*在圆上。

## 一般化以及有关定理

泰勒斯定理是「同弧所对的[圓周角是](../Page/圓周角.md "wikilink")[圓心角的一半](../Page/圓心角.md "wikilink")」的一個特殊情況。

以下是泰勒斯定理的一个相关定理：

  -
    如果*AC*是一个圆的直径，则：
      - 若*B*在圆内，则∠*ABC* \> 90°
      - 若*B*在圆上，则∠*ABC* = 90°
      - 若*B*在圆外，则∠*ABC* \< 90°

## 歷史

[泰勒斯並非此定理的首名發現者](../Page/泰勒斯.md "wikilink")，[古埃及人和](../Page/古埃及.md "wikilink")[巴比倫人一定已知這特性](../Page/巴比倫.md "wikilink")，可是他們沒有給出證明。

[T](../Category/几何定理.md "wikilink")
[Category:圆](../Category/圆.md "wikilink")