**泰王國**（，*Ratcha-anachak Thai*），通稱**泰国**（，*Prathet
Thai*），舊名**[暹罗](../Page/暹罗.md "wikilink")**、**暹邏**（，*Sayam*），是[东南亚的](../Page/东南亚.md "wikilink")[君主立宪制](../Page/君主立宪制.md "wikilink")[国家](../Page/国家.md "wikilink")，首都及最大城市为[曼谷](../Page/曼谷.md "wikilink")。泰国国土东临[老挝和](../Page/老挝.md "wikilink")[柬埔寨](../Page/柬埔寨.md "wikilink")，南接[暹罗湾和](../Page/暹罗湾.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")，西靠[缅甸和](../Page/缅甸.md "wikilink")[安达曼海](../Page/安达曼海.md "wikilink")，[东南亚国家联盟創始國之一](../Page/东南亚国家联盟.md "wikilink")。

## 國名

泰國的正式全稱為「泰王國」（
），其中「」表示「國王」、「」意為「領土」、而「」則代表「自由」之意。在過去，泰國一直以「[暹羅](../Page/暹羅.md "wikilink")」、「暹邏」（）作為國名。在擺脱[高棉人統治之後](../Page/高棉人.md "wikilink")，暹羅人以「[自由](../Page/自由.md "wikilink")」一詞（讀音即為「泰」）作為主體民族[泰族的名稱](../Page/泰族.md "wikilink")。[銮披汶·颂堪元帅在](../Page/銮披汶·颂堪.md "wikilink")1939年6月23日至1945年9月8日期間，将暹羅改稱「泰國」（），取其「自由領土」之意，皆因在當時的[東南亞地區](../Page/東南亞.md "wikilink")，周邊的各個大小王國均相繼淪為[葡萄牙](../Page/葡萄牙.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[英国](../Page/英国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[日本等](../Page/日本.md "wikilink")[歐](../Page/歐洲.md "wikilink")[亞](../Page/亞洲.md "wikilink")[列強的殖民地](../Page/列強.md "wikilink")，使得泰國成為僅存的獨立國家。1945年9月8日，泰國復稱為「暹羅王國」。1949年5月11日，暹羅正式更名為「泰王國」。

泰国的旧称“暹罗”根据《[明史](../Page/明史.md "wikilink")》所载，係為“暹羅斛”之简称，“暹”（）或指古国[素万那普](../Page/素万那普.md "wikilink")，“羅斛”指另一古国[罗涡](../Page/罗涡.md "wikilink")。两国于十四世纪并合为[大城王国](../Page/大城王国.md "wikilink")，因而被中国古籍以组合词“暹羅斛”或“暹羅”代指之。“暹”一词的来源众说纷纭，一说來自於[梵語詞彙](../Page/梵語.md "wikilink")“”（Śyāma），意為“暗色”\[1\]；或来自于[巴利文的](../Page/巴利文.md "wikilink")“suvaṇṇabhūmi”，意为“黄金之地”（此名後來作為[曼谷新國際機場名](../Page/曼谷國際機場.md "wikilink")）；也有说法称来自于[孟语词汇](../Page/孟语.md "wikilink")“”（rhmañña），意为“外地人”。中文古籍的称呼“暹”被葡萄牙傳教士轉寫為Siam，成为西方通用的称呼。“暹”在[蒙固國王統治時期定為正式國號](../Page/蒙固.md "wikilink")\[2\]，也被近代中文以“暹罗”对应之。

## 歷史

泰國至少從舊石器時期，大約4萬年前，即有人類居住。泰國古代文明甚為發達。大約五千年前，泰國北部的班清（有時譯作滿慶）已經進入青銅器時代。\[3\]公元1世紀，開始出现[王國](../Page/王國.md "wikilink")；1238年[素可泰王国建立](../Page/素可泰王国.md "wikilink")；14世纪中叶[阿瑜陀耶王国](../Page/阿瑜陀耶王国.md "wikilink")（[大城王国](../Page/大城王国.md "wikilink")）取而代之；18世纪华裔[郑昭建立](../Page/郑昭.md "wikilink")[吞武里王国](../Page/吞武里王国.md "wikilink")，后来[拉玛一世夺取政权](../Page/拉玛一世.md "wikilink")，建都曼谷，史称[扎克里王朝](../Page/扎克里王朝.md "wikilink")（[曼谷王朝](../Page/曼谷王朝.md "wikilink")）。16世纪[歐洲列强到達暹羅](../Page/歐洲.md "wikilink")，直至19世紀末英法兩國已分別在暹羅東西兩邊建立英屬[印度和法屬](../Page/印度.md "wikilink")[印度支那](../Page/印度支那.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。1896年英法簽訂《關於暹羅等地的宣言》，將暹羅列為兩個殖民地之間的「緩衝國」；其後英法於1904年劃定勢力範圍：[昭披耶河以東為法國勢力範圍](../Page/昭披耶河.md "wikilink")，以西為英國勢力範圍。雖然暹羅沒有成為列強殖民地，但仍受英法諸多壓制。

[曼谷王朝的](../Page/曼谷王朝.md "wikilink")[拉瑪四世](../Page/拉瑪四世.md "wikilink")（1851年至1868年在位）開始國家開放政策，至五世王[朱拉隆功](../Page/朱拉隆功.md "wikilink")（1868年至1910年在位）借鑒西方國家經驗，進行一系列改革以因應殖民國家施加的壓力。1932年6月，在一次不流血革命後，暹羅成為[君主立憲制國家](../Page/君主立憲制.md "wikilink")。[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[鑾披汶‧頌堪政府傾向](../Page/鑾披汶‧頌堪.md "wikilink")[日本](../Page/日本.md "wikilink")，1941年12月7日，日本發動[太平洋戰爭](../Page/太平洋戰爭.md "wikilink")，日本和暹羅簽訂《日泰攻守同盟条約》。1942年1月25日泰國宣佈向[英](../Page/英國.md "wikilink")[美宣戰](../Page/美國.md "wikilink")，日本曾將部份在[緬甸和](../Page/緬甸.md "wikilink")[馬來亞半島北部佔領地割讓給暹羅](../Page/馬來亞半島.md "wikilink")。1945年8月15日[日本投降](../Page/日本投降.md "wikilink")，暹羅隨即在翌日宣佈「暹羅1942年1月25日對英美宣戰宣言無效」，暹羅的「宣戰無效」宣言被[同盟國承認](../Page/同盟國_\(第二次世界大戰\).md "wikilink")。1949年改名**泰國**。第二次世界大戰後泰國成為[美國在東南亞的主要軍事盟國](../Page/美國.md "wikilink")。在東南亞地區，泰國亦是一個舉足輕重的國家；首都[曼谷是該區域中國際化程度很高的](../Page/曼谷.md "wikilink")[大都會區](../Page/大都會區.md "wikilink")。另外，泰國是[東協始創國之一](../Page/東南亞國家聯盟.md "wikilink")，亦在東南亞區內事務有積極的參與。

二戰後的泰國長期實行軍政府獨裁統治，[鑾披汶·頌堪](../Page/鑾披汶·頌堪.md "wikilink")、[沙立·他那叻](../Page/沙立·他那叻.md "wikilink")、[他儂·吉滴卡宗](../Page/他儂·吉滴卡宗.md "wikilink")、[江萨·差瑪南及](../Page/江萨·差瑪南.md "wikilink")[炳·廷素拉暖等先後掌權](../Page/炳·廷素拉暖.md "wikilink")。[1991年泰國軍事政變](../Page/1991年泰國軍事政變.md "wikilink")，軍方推翻[差猜·春哈旺](../Page/差猜·春哈旺.md "wikilink")、憲法和國会。其後泰國人民上街遊行，抗議軍方統治。軍方血腥鎮壓，迫使泰王介入。總理蘇欽達和示威人士領袖雙雙跪在泰王面前，承諾平息風波，示威始告结束，選舉和憲法也得到恢复，泰國開始民主化。[泰國民主黨的](../Page/泰國民主黨.md "wikilink")[川·立派成為總理](../Page/川·立派.md "wikilink")。1998年7月14日，[他信·西那瓦創建](../Page/他信·西那瓦.md "wikilink")[泰愛泰黨](../Page/泰愛泰黨.md "wikilink")，並任黨主席。不到五年期間，泰愛泰黨合併幾個小黨，形成[一黨獨大的局面](../Page/一黨獨大.md "wikilink")。

2001年2月的大選，他信·西那瓦以壓倒性優勢當選總理，泰愛泰黨在國會下議院500席中取得248席，擊敗民主黨，成為單一大黨，他成為泰國第23任總理。2005年2月的大選，泰愛泰黨贏得500個議會席位中的375席，取得壓倒性勝利，他信成為泰國歷史上第一位任滿4年，並成功連任的民選總理。

2006年4月2日的議會選舉，遭到三大反對黨的聯合抵制，造成嚴重的政治危機。4月5日，[看守內閣總理他信在內閣特別會議上指派副總理奇猜](../Page/看守內閣.md "wikilink")·萬那沙提暫時行使看守內閣總理職責。\[4\]5月8日，[泰國憲法法院裁定](../Page/泰國憲法法院.md "wikilink")，泰國選舉委員會在組織4月2日國會下議院選舉中「有違法和違憲行為」，因此選舉結果無效，應重新舉行[國會](../Page/國會.md "wikilink")[下議院選舉](../Page/下議院.md "wikilink")。\[5\]而他信繼續出任看守總理。9月19日，泰國軍隊乘他信總理赴[紐約參加](../Page/紐約.md "wikilink")[聯合國大會之機發動](../Page/聯合國大會.md "wikilink")[政變](../Page/2006年泰國軍事政變.md "wikilink")，宣佈暫時廢除憲法。\[6\]9月22日，陸軍司令[頌提·汶雅叻格林出任管理改革委員會主席](../Page/頌提·汶雅叻格林.md "wikilink")。\[7\]10月1日，管理改革委員會頒布臨時憲法並立即生效，管理改革委員會更名為泰國國家安全委員會。\[8\]10月9日，以[素拉育·朱拉暖為臨時總理的臨時內閣宣誓就職](../Page/素拉育·朱拉暖.md "wikilink")。\[9\]10月20日，臨時立法機構泰國國民立法大會成立。\[10\]2007年8月19日，就新憲法草案舉行[全民公決](../Page/全民公決.md "wikilink")\[11\]，獲得通過。\[12\]其後獲他信支持的[人民力量黨在](../Page/人民力量黨.md "wikilink")[2007年泰國國會選舉中勝出](../Page/2007年泰國國會選舉.md "wikilink")。

2011年7月3日，獲他信支持的[為泰黨在大選中勝出](../Page/為泰黨.md "wikilink")，再次上臺執政。2014年5月7日，泰國總理[英叻·欽那瓦因違法濫權調動國家安全委員會秘書長](../Page/英叻·欽那瓦.md "wikilink")，泰國憲法法庭宣判，看守總理盈拉需即時下臺。

## 政治

[Government_House_of_Thailand.JPG](https://zh.wikipedia.org/wiki/File:Government_House_of_Thailand.JPG "fig:Government_House_of_Thailand.JPG")\]\]

按泰国1997年的宪法，[泰国国王和王室实际](../Page/泰国国王.md "wikilink")[权力極少](../Page/权力.md "wikilink")，国王本人主要是国家及其国家精神的象徵。已故泰國九世王[普密蓬·阿杜德很受到国民的敬爱](../Page/普密蓬·阿杜德.md "wikilink")，因此他也時常出面调解政治危机。但是泰國法律依舊對這名義上國家元首極為尊敬，如對君主或王室成員有冒犯的言行，或以不當詞句形容、或疑似揶揄、調侃，將觸犯[大不敬罪](../Page/大不敬.md "wikilink")，可判[有期徒刑三年至十五年](../Page/有期徒刑.md "wikilink")，這條罪名也不容質疑，凡是質疑「[大不敬罪](../Page/大不敬.md "wikilink")」是否應該存在，或者[大不敬罪是否過於嚴厲](../Page/大不敬.md "wikilink")，也觸犯[大不敬罪](../Page/大不敬.md "wikilink")。美國駐泰國大使戴維斯（Glyn
T.
Davies）曾與外國記者提到，[美國政府憂慮判處違反](../Page/美國政府.md "wikilink")「大不敬罪」者，處以「空前且漫長」的監禁。結果泰國警方發表聲明，表示戴維斯憂慮「大不敬罪」的言論，也涉嫌「大不敬罪」，已經展開調查。\[13\]

[政府首脑是](../Page/政府首脑.md "wikilink")[總理](../Page/泰国總理.md "wikilink")，一般由[众议院联合政府](../Page/众议院.md "wikilink")（多数党派领袖）出任，再請国王指定。

[Thai_Parliament_House.JPG](https://zh.wikipedia.org/wiki/File:Thai_Parliament_House.JPG "fig:Thai_Parliament_House.JPG")\]\]

[泰国国会由两院组成](../Page/泰国国会.md "wikilink")：根据2011年宪法修正案，众议院有500席，其中375席通过普选产生，另外125席按比例代表制从各党派中选出。[参议院有](../Page/参议院.md "wikilink")150席。众议员任期四年，参议员任期六年。最高司法机关是大理院，其成员先由政府機關內部選任，後由国王指定。

2006年9月19日，由[頌提·汶雅叻格林将军领导的](../Page/頌提·汶雅叻格林.md "wikilink")[軍隊趁](../Page/泰国皇家軍隊.md "wikilink")[塔克辛總理赴紐約參加](../Page/塔克辛.md "wikilink")[聯合國大會之機發動](../Page/聯合國大會.md "wikilink")[政變](../Page/2006年泰国军事政變.md "wikilink")，宣佈暫時停止[憲法](../Page/憲法.md "wikilink")，解散[内阁及禁止一切](../Page/内阁.md "wikilink")[政党活动](../Page/政党.md "wikilink")。荷枪实弹的军人及坦克进驻首都[曼谷](../Page/曼谷.md "wikilink")，军方控制电视台等战略地点，全国进入军事[戒严状态](../Page/戒严.md "wikilink")，頌提任代總理。

總理[沙麥·順達衛違反憲法](../Page/沙麥·順達衛.md "wikilink")，2008年9月9日遭解除職位，9月17日，下議院選出前法官[頌猜為總理](../Page/頌猜.md "wikilink")，但頌猜於12月2日被裁定選舉舞弊而被迫下台。

據報導，泰國的政變有時或與王室及支持王室的勢力有關；國王為保持平衡，指示政爭須有限度。[2008年的政治危機中](../Page/2008年－2009年泰國政治危機.md "wikilink")，穿著黃衫、包圍機場的人被指為王室支持者；運動起源於國王懼怕他信進一步擴張影響力。總理頌猜代表民選政府命令警察進入機場驅逐黃衫人，但軍警都只忠於國王而不願行動。\[14\]

政变后，泰国通过2007年宪法，规定由军队控制的选举委员会指定参议院160席中的84席，其余议席中的绝大部分也由各地的选举委员会指定；并且通过法院限制原[泰爱泰党议员参选而控制住众议院](../Page/泰爱泰党.md "wikilink")，进而控制政府官员的选择。由于军队是忠于泰国王室，因此，通过政变，泰国王室重新控制及主導泰国的军政大权。

[2013年泰國反政府抗議活動泰國在野黨民主黨](../Page/2013年泰國反政府抗議活動.md "wikilink")（Democrat
Party）10月30日宣布，31日將在曼谷的訕甚（Sam
Sen）鐵路一帶發動反特赦案集會。抗議人士認為，特赦案目的是要替因貪汙遭定罪的前總理[塔克辛·欽那瓦漂白](../Page/塔克辛·欽那瓦.md "wikilink")，讓他能夠結束自我流亡，返回泰國。11月11日參議院以壓倒性票數駁回特赦法案，但示威人潮並未減退。同年12月9日，總理[英叻·钦那瓦宣布解散國會](../Page/英叻·钦那瓦.md "wikilink")，提前於2014年2月2日舉行大選，但反對派不買帳，繼續示威抗議，要求[英叻·钦那瓦下台](../Page/英叻·钦那瓦.md "wikilink")，組成人民議會。

2014年5月20日，泰國軍方宣布實施戒嚴法。[5月22日泰国爆发军事政变](../Page/2014年泰国军事政变.md "wikilink")，陸軍總司令[帕拉育宣布接掌政權](../Page/帕拉育·詹歐查.md "wikilink")，帕拉育與資深軍方官員一同在全國轉播的節目上，宣布接掌政權，並稱此舉是為了恢復和平，這是泰國自1932年以來，軍方第12次發動政變。

## 地理

[Thailand_Topography.png](https://zh.wikipedia.org/wiki/File:Thailand_Topography.png "fig:Thailand_Topography.png")
國境大部份為低緩的山地和高原。地形多变，可分為西、中、東、南四個部份。

泰國西部為山區，是[喜瑪拉雅山脈的延伸](../Page/喜瑪拉雅山脈.md "wikilink")[他念他翁山脈為主的山地](../Page/他念他翁山脈.md "wikilink")，一直由北向南走向。位於[清邁府的](../Page/清邁府.md "wikilink")[因他暖山](../Page/因他暖山.md "wikilink")（海拔2,576公尺）是泰国的最高峰。

东北部是[呵叻高原](../Page/呵叻高原.md "wikilink")，这里夏季极干旱，雨季非常泥泞，不宜耕作。

中部是[昭披耶河平原](../Page/昭披耶河.md "wikilink")。由曼谷向北，地勢逐步緩升，昭披耶河沿岸土地豐饒，是泰國主要農產地。曼谷以南為暹羅灣紅樹林地域，漲潮時沒入水中，退潮後成為[紅樹林沼澤地](../Page/紅樹林.md "wikilink")。

泰國南部是西部山脈的延續，山脈再向南形成[馬來半島](../Page/馬來半島.md "wikilink")，最狹處稱為[克拉地峽](../Page/克拉地峽.md "wikilink")。

另外，泰國的一般大眾習慣將國家的疆域比作[大象的頭部](../Page/象.md "wikilink")，將北部視為「象冠」，東北地方代表「象耳」，暹羅灣代表「象口」，而南方的狹長地帶則代表了「象鼻」。

### 氣候

泰国大部分地区属于[热带季风气候](../Page/热带季风气候.md "wikilink")。常年温度不下攝氏18℃，平均年降水量約1000毫米。11月至2月受較涼的東北季候風影響比較乾燥，3月到5月气温最高，可達攝氏40-42℃，7月至9月受西南季候風影響，是雨季。10月至12月偶有[熱帶氣旋從](../Page/熱帶氣旋.md "wikilink")[南海經過中南半島吹襲泰國東部](../Page/南海.md "wikilink")，但在[暹羅灣形成的熱帶氣旋為數甚少且弱](../Page/暹羅灣.md "wikilink")。

## 人口

[Wat_arun_bangkok.jpg](https://zh.wikipedia.org/wiki/File:Wat_arun_bangkok.jpg "fig:Wat_arun_bangkok.jpg")西畔的[黎明寺是泰国规模最大的大乘舍利式塔](../Page/黎明寺.md "wikilink")，享有“泰國埃菲爾鐵塔”的美稱。\]\]
全國共有20多個民族，總計6000多萬人口。[泰族為主要民族](../Page/泰族.md "wikilink")，佔人口總數的75％，[漢族佔](../Page/泰國華人.md "wikilink")14%，[馬來族佔](../Page/馬來族.md "wikilink")2.3%，其餘是[缅族](../Page/缅族.md "wikilink")、[高棉族](../Page/高棉族.md "wikilink")、[苗族](../Page/苗族.md "wikilink")、[瑤族](../Page/瑤族.md "wikilink")、[桂族](../Page/桂族.md "wikilink")、[汶族](../Page/汶族.md "wikilink")、[克倫族](../Page/克倫族.md "wikilink")、[塞芒族](../Page/塞芒人.md "wikilink")、[沙蓋族](../Page/沙蓋族.md "wikilink")、[孟族等民族](../Page/孟族.md "wikilink")。

大约有14%的泰国人口是[华裔](../Page/泰國華人.md "wikilink")，其中相当一部份来自[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[潮汕地区](../Page/潮汕地区.md "wikilink")。

另外，[马来族是另一个重要的少数民族](../Page/马来族.md "wikilink")（2.3%），其余的少数民族包括高棉、孟族等民族。除此之外尚有京族，以及一些住在山上的部落，例如[巴通族](../Page/巴通族.md "wikilink")、[嘎良族](../Page/嘎良族.md "wikilink")（著名的[長頸族](../Page/長頸族.md "wikilink")）、[拉祜族](../Page/拉祜族.md "wikilink")、[苗族](../Page/苗族.md "wikilink")、[瑤族等](../Page/瑤族.md "wikilink")，接近緬甸的山區有少數[撣族人](../Page/撣族.md "wikilink")，这些人口大约为788,024人。

除此之外，泰国还有大量的来自其他亚洲地區、欧洲、北美等长期居住在泰国的人口，还有大量非法移民。不过由于泰国人口基数大，所以这些人口只占总人口的很小一部分。

以產業結構來分析，泰国的人口主要为[农业人口](../Page/农业.md "wikilink")，集中在稻米產地，即泰国的中部和东北、北方。随着[全球化进程](../Page/全球化.md "wikilink")，泰国也在工业化过程中，有大约31.1%的泰国人口集中在[曼谷等大城市](../Page/曼谷.md "wikilink")，而且在持续增长中。

### 語言

泰国的官方语言是[泰语](../Page/泰语.md "wikilink")，用[泰语字母](../Page/泰语字母.md "wikilink")，當中約5000萬人視為母語。

少數民族有他們各自的语言。最多的为在东北地区的[老挝语方言](../Page/老挝语.md "wikilink")。在泰国南部，有着[马来语的方言](../Page/马来语.md "wikilink")，这个作为南部马来穆斯林使用的语言。泰国华人也会使用[漢语方言](../Page/漢语.md "wikilink")，其中以屬於[閩南語系的](../Page/閩南語系.md "wikilink")[潮汕话最多](../Page/潮汕话.md "wikilink")。

### 宗教

[Phutthamonthon_Buddha.JPG](https://zh.wikipedia.org/wiki/File:Phutthamonthon_Buddha.JPG "fig:Phutthamonthon_Buddha.JPG")
[Thai_traditional_costumes_Chiang_Mai_2005_033.jpg](https://zh.wikipedia.org/wiki/File:Thai_traditional_costumes_Chiang_Mai_2005_033.jpg "fig:Thai_traditional_costumes_Chiang_Mai_2005_033.jpg")
[Large_group_dancing_females_Thailand.jpg](https://zh.wikipedia.org/wiki/File:Large_group_dancing_females_Thailand.jpg "fig:Large_group_dancing_females_Thailand.jpg")

泰国是世界上知名的[佛教国家之一](../Page/佛教.md "wikilink")，大多数泰国人信奉[佛教](../Page/佛教.md "wikilink")，佛教徒佔全國人口95%以上。不过[泰国宪法中并未规定](../Page/泰国宪法.md "wikilink")[国教](../Page/国教.md "wikilink")，而是保证公民的信仰自由。但宪法规定[泰國君主必须是](../Page/泰國君主.md "wikilink")[佛教徒](../Page/佛教徒.md "wikilink")。

佛教对日常生活产生强烈的影响。[比丘](../Page/比丘.md "wikilink")[长老非常受人们尊敬](../Page/长老.md "wikilink")。因此，无论在城市还是乡村，寺庙（wat）都是社会生活和宗教生活的中心。禅是佛教最普及的方面之一，有无数泰国人定期[坐禅以提升内心的平静和愉快](../Page/坐禅.md "wikilink")。游客也可以在曼谷的几个中心或者国内的其他地方学习坐禅的基本原则。

泰國南部的[陶公府](../Page/陶公府.md "wikilink")、[北大年府和](../Page/北大年府.md "wikilink")[惹拉府以信奉](../Page/惹拉府.md "wikilink")[伊斯兰教的](../Page/伊斯兰教.md "wikilink")[穆斯林为主](../Page/穆斯林.md "wikilink")，佔全國人口的4%。另外亦有信奉[基督教](../Page/基督教.md "wikilink")（西北[克倫族](../Page/克倫族.md "wikilink")）和[印度教的信徒](../Page/印度教.md "wikilink")，但較佛教徒的人口有很大的相差，其僅佔總人口的1%左右。这些非佛教信仰者有完全的信仰自由，得到保障。

### 泰國華商和華人

早在[清朝時](../Page/清朝.md "wikilink")，華人就已經來到泰國謀生，並漸漸融入當地。泰國歷史上建立[吞武里王國的就是具有](../Page/吞武里王國.md "wikilink")[潮汕血統](../Page/潮汕人.md "wikilink")[鄭昭](../Page/鄭昭.md "wikilink")。如今，鄭王像仍被泰國民眾虔誠地供奉。泰國華商主要來自於廣東潮汕地區。泰國的重要經濟支柱都由華人把持，不少大型企業都是由華商創辦。華人的經濟地位相比當地泰族原住民較為優越。但因為泰國是個佛教國家，住民淳樸，對華人沒有太多的種族對立。

上世紀五六十年代，中國大陸政權更替後，中國開始向東南亞各國[輸出革命](../Page/輸出革命.md "wikilink")\[15\]，支持所在國革命組織顛覆政府。因為東南亞共產革命的參加者幾乎都是華人，導致當地各國政府為防共而排華。泰國是東南亞國家中排華最輕微的，沒有像印尼那樣大屠殺。但為切斷華人與母國的血脈聯繫，泰國政府禁止教授華文和使用華文。華人為求自保，都放棄原來的中國華語姓氏，而改用有相同意思的泰語文字來當作他們的新姓氏甚或採用當地姓氏。這運動後來稱之為「[改姓名運動](../Page/改姓名運動.md "wikilink")」。

[鄧小平時代](../Page/鄧小平時代.md "wikilink")，中國改善與東南亞國家的關係，在1989年，中國徹底結束了對東南亞的革命輸出，[緬共與](../Page/緬共.md "wikilink")[泰共失去了物質支持而解散](../Page/泰共.md "wikilink")。中國與泰國的經濟合作日益增多，泰國允許恢復華文教學。泰國經濟的起飛，也吸引了大批華人到泰國發展。華人在今天的泰國經濟和政治中，更扮演着重要的角色。泰國前總理[他信](../Page/他信.md "wikilink")、[阿披實](../Page/阿披實.md "wikilink")、[英拉](../Page/英拉.md "wikilink")、泰國最大的商業集團[正大集團的董事長等等都是華裔](../Page/正大集團.md "wikilink")，華商在泰國的經濟發展中擁有舉足輕重的地位。

## 行政區劃

泰国全國共有76個一級行政區，其中包括75個「府」（จังหวัด，changwat）與直轄市的首都——曼谷。這76個行政區一般被劃分為5個主要地區，包括[北部](../Page/北部地區_\(泰國\).md "wikilink")、[東北部](../Page/東北地區_\(泰國\).md "wikilink")、[東部](../Page/東部地區_\(泰國\).md "wikilink")、[中部與](../Page/中部地區_\(泰國\).md "wikilink")[南部地區](../Page/南部地區_\(泰國\).md "wikilink")，每個府都是以其首府（เมือง，Mueang）作為該府的命名。在府底下，又有更小的次級行政區劃，稱為「縣或郡」（อำเภอ，Amphoe）與「次區」（กิ่งอำเภอ，King
Amphoe），根據2000年時的統計，泰國全國共有795個縣與81個次區。至於首都曼谷的次級行政區則與各府的次級行政區在命名上有點出入，稱為「เขต」（Khet），總數達50個。

### 主要城市

## 經濟

一般認為泰國經濟體是[新興市場及](../Page/新興市場.md "wikilink")[新興工業化國家](../Page/新興工業化國家.md "wikilink")，依照購買力平價標準(PPP)泰國的2013年[國內生產毛額達到](../Page/國內生產毛額.md "wikilink")6,730億美金\[16\]，泰國在[東南亞國家是僅次於](../Page/東南亞國家.md "wikilink")[印尼的第二大經濟體](../Page/印尼.md "wikilink")。依照[人均國內生產總值泰國在](../Page/人均國內生產總值.md "wikilink")[東南亞國家排名在](../Page/東南亞國家.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")、[汶萊與](../Page/汶萊.md "wikilink")[馬來西亞之後位居中間為第四名](../Page/馬來西亞.md "wikilink")。

對於[寮國](../Page/寮國.md "wikilink")、[緬甸與](../Page/緬甸.md "wikilink")[柬埔寨周邊鄰國來說泰國是該區域的](../Page/柬埔寨.md "wikilink")[經濟體系中心](../Page/經濟體系.md "wikilink")，依據泰國在2014年第三季[失業率約](../Page/失業率.md "wikilink")0.84%。\[17\]

### 近期經濟歷史

1985年至1995年间泰国的经济发展是世界上最快的。1997年泰国的货币危机引发了[亚洲经济危机](../Page/亚洲经济危机.md "wikilink")，[泰銖对](../Page/泰銖.md "wikilink")[美元的比值从](../Page/美元.md "wikilink")26：1降到56：1。1998年国民经济下降了10%。

1999年开始泰国经济开始得到恢复。其主要动力是[出口及](../Page/出口.md "wikilink")[旅遊業](../Page/旅遊業.md "wikilink")。
[Rice_fields_Chiang_Mai.jpg](https://zh.wikipedia.org/wiki/File:Rice_fields_Chiang_Mai.jpg "fig:Rice_fields_Chiang_Mai.jpg")的水稻田，泰国是世界上最大的稻米出口国\]\]
[ไอคอนสยาม_Grand_Openning_day_Iconsiam_of_Thailand_3_Copyrights_of_Trisorn_Triboon.jpg](https://zh.wikipedia.org/wiki/File:ไอคอนสยาม_Grand_Openning_day_Iconsiam_of_Thailand_3_Copyrights_of_Trisorn_Triboon.jpg "fig:ไอคอนสยาม_Grand_Openning_day_Iconsiam_of_Thailand_3_Copyrights_of_Trisorn_Triboon.jpg")\]\]
[Bangkok_skytrain_sunset.jpg](https://zh.wikipedia.org/wiki/File:Bangkok_skytrain_sunset.jpg "fig:Bangkok_skytrain_sunset.jpg")
[Thailandabteilung_in_Ethnological_Museum_Berlin_02.JPG](https://zh.wikipedia.org/wiki/File:Thailandabteilung_in_Ethnological_Museum_Berlin_02.JPG "fig:Thailandabteilung_in_Ethnological_Museum_Berlin_02.JPG")考古地點出土的文物\]\]
據[世界銀行最新的經濟體排名](../Page/世界銀行.md "wikilink")（2016年）顯示，其人均國民收入總值約為5894美元，屬中收入國家。貧富差距問題嚴重，而且泰國近年來常常[政變](../Page/政變.md "wikilink")，現今泰國在發展經濟同時，亦要面臨如何遏止政變發生的問題。

### 農業

作為世界稻米市場的其中一個主要出口國，[水稻是泰國最重要的農作物](../Page/水稻.md "wikilink")，其他主要農產品有魚類、[木薯](../Page/木薯.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")、穀物和[蔗糖](../Page/蔗糖.md "wikilink")。而加工食品如罐裝[金槍魚](../Page/金槍魚.md "wikilink")、[菠蘿和冷凍](../Page/菠蘿.md "wikilink")[蝦的出口量也在上升](../Page/蝦.md "wikilink")。該國北部是[黑象牙咖啡](../Page/黑象牙咖啡.md "wikilink")（Black
Ivory coffee）出產地。

### 旅遊业

旅遊業在泰國觀光旅遊局的大力推動下，成為泰國主要的經濟收入來源，遊客可以在境內不同的地區享受不同的旅遊形式。泰國有現代化的城市曼谷，南部面臨[暹羅灣和印度洋](../Page/暹羅灣.md "wikilink")，有很多天然的沙灘度假區發展起來，北部山區氣候宜人，亦適宜旅行。

著名的旅遊景點：

  - [曼谷](../Page/曼谷.md "wikilink")
      - [丹嫩莎朵水上市場](https://weismile.tw/kkdaydammnoen1/)
      - [美功鐵道市場](https://weismile.tw/maeklong/)
  - 南部沙灘度假區
      - [烏打拋](../Page/烏打拋.md "wikilink")
      - [芭達雅](../Page/芭達雅.md "wikilink")
      - [華欣](../Page/華欣.md "wikilink")
      - [普吉島](../Page/普吉島.md "wikilink")
      - [蘇梅島](../Page/蘇梅島.md "wikilink")
      - [沙美島](../Page/沙美島.md "wikilink")
      - [帕岸島](../Page/帕岸島.md "wikilink")
  - 中北部地區
      - [清邁](../Page/清邁.md "wikilink")
      - [清萊](../Page/清萊.md "wikilink")——[金三角地區](../Page/金三角.md "wikilink")

#### 世界遺產

泰國有5處[世界遺產](../Page/世界遺產.md "wikilink")，當中包括3項文化遺產，2項自然遺產。

  - [素可泰歷史城鎮和相關歷史城鎮群](../Page/素可泰歷史城鎮和相關歷史城鎮群.md "wikilink")（文化遺產，1991年）
  - [阿瑜陀耶歷史城市](../Page/阿瑜陀耶.md "wikilink")（文化遺產，1991年）
  - [通艾—會卡肯野生生物禁獵區](../Page/通艾—會卡肯野生生物禁獵區.md "wikilink")（自然遺產，1991年）
  - [班清考古地點](../Page/班清.md "wikilink")（文化遺產，1992年）
  - [棟巴耶延—考愛森林綜合體](../Page/棟巴耶延—考愛森林綜合體.md "wikilink")（自然遺產，2005年）

另外泰國和[柬埔寨的邊界的](../Page/柬埔寨.md "wikilink")[柏威夏寺亦是世界遺產](../Page/柏威夏寺.md "wikilink")，其所有权处于雙方爭議中。

## 通讯行业

### 邮政

目前，全泰国有超过1万所邮局，超过3,400家代办点并已经实现联网。邮政信箱超过3.8万个。

### 固定电话

泰国有四家大的电信运营公司，即：国有的（TOT）、私营企业公司、国电话电信公司（TT\&T）、国际固话业务由（CAT）负责。

### 移动电话

泰国由三家營運移动电话服务：[AIS](../Page/AIS通信.md "wikilink")、和。泰国移动电话通话费用可以预付费，也可以后付费。

### 互联网络

泰国主要的固网骨干由CAT负责，主要营运商有TOT、TRUE与AIS。企业网络则有UIH、Kirz等。

### 国际长途

国际长途方面，在泰国可以通过CAT的系统播拨打，也可以购买TollD、i-talk、ThookJung和Easy
Card等较便宜的电话卡轉撥，只是訊號品質较差。

## 文化

### 电影

泰国电影以[恐怖片](../Page/恐怖片.md "wikilink")、鬼片为主，如《[鬼影](../Page/鬼影_\(電影\).md "wikilink")》等，而以《[拳霸](../Page/拳霸.md "wikilink")》为代表的[泰拳动作片则在国际上比较流行](../Page/泰拳.md "wikilink")。近年来，泰国以青春为题材的电影逐渐流行，形成一股浓郁的泰国风，如《[小情人](../Page/小情人.md "wikilink")》、《[初恋这件小事](../Page/初恋这件小事.md "wikilink")》、《[暹罗之恋](../Page/暹罗之恋.md "wikilink")》、《[愛我一下夏](../Page/愛我一下夏.md "wikilink")》、《[下一站，說愛你](../Page/下一站，說愛你.md "wikilink")》、《[音為愛](../Page/音為愛.md "wikilink")》、《[想愛就愛](../Page/想愛就愛.md "wikilink")》、《[出貓特攻隊](../Page/出貓特攻隊.md "wikilink")》等。泰式恐怖片也是全世界知名，其恐怖程度可與[日本恐怖片相提並論](../Page/日本.md "wikilink")。

### 少數民族傳統

泰國其實有很多不同的少數民族，也分佈在泰國不同的地區，各民族都有其各自的部落。泰國北區有著長頸族部落的居住，其民族在泰北有自由的生活圈，泰國政府不會干預並且遵重其傳統。長頸族服裝的特徵為脖子會繞著一圈圈的環，環的圈數愈多，表示愈漂亮。頸環只限於女生，男的則不用，女孩到了年齡約莫四、五歲左右就開始了環頸的傳統，且直到老去都將一直戴著它。

### 飲食

泰國菜講究「酸、甜、苦、辣、鹹」五味的互相平衡，通常以鹹、酸、辣為主，而帶着一點甜，而苦味則隱隱約約在背後。在不同地區之間，飲食口味及偏好稍有差異。主要分成四大菜系，各種菜系都有不同特色。

## 教育

泰國教育主要是由泰國教育部管理。學齡兒童必須就學，直到完成中學前三年的課程，這些學生可以就讀公立或私立學校，這些學校的學制分為小學六年的初等教育、以及中學六年的中等教育。少數偏遠地區學校的學制只包含六年小學和三年中學。

根據2014年的統計，泰國的整體識字率大約為93.5%。

## 體育

[Muay_Thai_match_in_Bangkok,_Thailand.jpg](https://zh.wikipedia.org/wiki/File:Muay_Thai_match_in_Bangkok,_Thailand.jpg "fig:Muay_Thai_match_in_Bangkok,_Thailand.jpg")
[Lisu_Elder_(Jurvetson).jpg](https://zh.wikipedia.org/wiki/File:Lisu_Elder_\(Jurvetson\).jpg "fig:Lisu_Elder_(Jurvetson).jpg")

[泰拳不仅是泰国的传统](../Page/泰拳.md "wikilink")[国技](../Page/国技.md "wikilink")，也是泰国最为流行的体育运动项目之一\[18\]。

泰国在各大型国际综合运动会中所获得的成绩如下：

  - [夏季奥运会](../Page/夏季奥运会.md "wikilink")：7金6银11铜24枚（亚洲第8，世界前50）
  - [亚洲运动会](../Page/亚洲运动会.md "wikilink")：109金148银209铜463枚（第7）
  - [夏季大运会](../Page/世界大學生運動會.md "wikilink")：21金11银30铜62枚（亚洲第6，世界第26）
  - [世界運動會](../Page/世界運動會.md "wikilink")：1金2银2铜5枚（亚洲第8，世界前60）
  - [亞洲室內運動會](../Page/亞洲室內運動會.md "wikilink")：58金66银89铜213枚（第2）
  - [夏季青奥会](../Page/青年奧林匹克運動會.md "wikilink")：4金3银7枚（亚洲第4，世界第14）
  - [世界武搏运动会](../Page/世界武搏运动会.md "wikilink")：4金3银2铜9枚（亚洲第3，世界第7）
  - [亞洲沙灘運動會](../Page/亞洲沙灘運動會.md "wikilink")：37金36银28铜103枚（第1）
  - [亞洲武藝運動會](../Page/亞洲武藝運動會.md "wikilink")：21金17银16铜 54枚（第1）
  - [亞洲青年運動會](../Page/亞洲青年運動會.md "wikilink")：11金7银2铜20枚（第3）

## 主要節慶

<table>
<thead>
<tr class="header">
<th><p>日期</p></th>
<th><p>中文名</p></th>
<th><p>当地名</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1月1日</p></td>
<td><p><a href="../Page/元旦.md" title="wikilink">新年</a></p></td>
<td><p>Wan Khun Pee Mai</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1月至2月间</p></td>
<td><p><a href="../Page/春节.md" title="wikilink">春节</a></p></td>
<td></td>
<td><p>農曆正月初一至初三（非法定假日）</p></td>
</tr>
<tr class="odd">
<td><p>2月</p></td>
<td><p><a href="../Page/萬佛節.md" title="wikilink">萬佛節</a></p></td>
<td><p>Mahka Bucha</p></td>
<td><p><a href="../Page/泰曆.md" title="wikilink">泰曆</a>3月15日</p></td>
</tr>
<tr class="even">
<td><p>4月6日</p></td>
<td></td>
<td><p>Chakri</p></td>
<td><p>纪念<a href="../Page/扎克里王朝.md" title="wikilink">扎克里王朝的奠定</a></p></td>
</tr>
<tr class="odd">
<td><p>4月13日至4月15日</p></td>
<td><p><a href="../Page/宋干节.md" title="wikilink">宋干节</a>/<a href="../Page/泼水节.md" title="wikilink">泼水节</a>/<a href="../Page/泰曆.md" title="wikilink">泰曆</a><a href="../Page/新年.md" title="wikilink">新年</a></p></td>
<td><p>Songkran</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5月</p></td>
<td><p><a href="../Page/卫塞节.md" title="wikilink">佛诞</a></p></td>
<td><p>Vaisakh Bucha</p></td>
<td><p><a href="../Page/泰曆.md" title="wikilink">泰曆</a>6月15日，紀念佛祖<a href="../Page/釋迦牟尼.md" title="wikilink">釋迦牟尼出生</a>、成道和出滅</p></td>
</tr>
<tr class="odd">
<td><p>5月1日</p></td>
<td><p><a href="../Page/国际劳动节.md" title="wikilink">劳动节</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>5月5日</p></td>
<td></td>
<td></td>
<td><p>纪念<a href="../Page/普密蓬·阿杜德.md" title="wikilink">普密蓬·阿杜德加冕</a></p></td>
</tr>
<tr class="odd">
<td><p>5月10日</p></td>
<td><p><a href="../Page/春耕节.md" title="wikilink">春耕节</a></p></td>
<td></td>
<td><p>祈求农业丰收的节日</p></td>
</tr>
<tr class="even">
<td><p>7月</p></td>
<td><p>守夏節</p></td>
<td><p>Ansanha Bucha</p></td>
<td><p><a href="../Page/泰曆.md" title="wikilink">泰曆</a>8月15日，紀念佛祖成道後向信徒講說之日</p></td>
</tr>
<tr class="odd">
<td><p>7月</p></td>
<td><p>佛祖開示紀念日</p></td>
<td><p>Khao Phansa</p></td>
<td><p><a href="../Page/泰曆.md" title="wikilink">泰曆</a>8月16日，是為期3個月的佛教齋戒期的首天</p></td>
</tr>
<tr class="even">
<td><p>8月12日</p></td>
<td><p><a href="../Page/母親節.md" title="wikilink">母親節</a></p></td>
<td><p>Mother's Day</p></td>
<td><p>纪念<a href="../Page/詩麗吉王后.md" title="wikilink">现王后生日</a></p></td>
</tr>
<tr class="odd">
<td><p>10月13日</p></td>
<td><p>九世王纪念日</p></td>
<td></td>
<td><p>纪念<a href="../Page/普密蓬·阿杜德.md" title="wikilink">普密蓬·阿杜德逝世日</a></p></td>
</tr>
<tr class="even">
<td><p>10月23日</p></td>
<td></td>
<td><p>Wan Piyamaharat</p></td>
<td><p>纪念<a href="../Page/朱拉隆功.md" title="wikilink">朱拉隆功大帝逝世日</a></p></td>
</tr>
<tr class="odd">
<td><p>11月</p></td>
<td><p><a href="../Page/水灯节.md" title="wikilink">水灯节</a></p></td>
<td><p>Loy Krathong</p></td>
<td><p><a href="../Page/泰曆.md" title="wikilink">泰曆</a>12月的第一个月圆之日</p></td>
</tr>
<tr class="even">
<td><p>12月5日</p></td>
<td><p><a href="../Page/父親節.md" title="wikilink">父親節</a></p></td>
<td><p>Father's Day</p></td>
<td><p>纪念<a href="../Page/普密蓬·阿杜德.md" title="wikilink">普密蓬·阿杜德生日</a></p></td>
</tr>
<tr class="odd">
<td><p>12月10日</p></td>
<td></td>
<td><p>Constitution Day</p></td>
<td><p>纪念1932年立宪</p></td>
</tr>
<tr class="even">
<td><p>12月28日</p></td>
<td><p>鄭皇節</p></td>
<td><p>Taksin Day</p></td>
<td><p>抗緬復國的民族英雄<a href="../Page/郑昭.md" title="wikilink">達信大帝登基纪念日</a></p></td>
</tr>
</tbody>
</table>

## 參看

  - [泰国君主列表](../Page/泰国君主列表.md "wikilink")
  - [2006年泰国军事政变](../Page/2006年泰国军事政变.md "wikilink")
  - [2008年－2009年泰國政治危機](../Page/2008年－2009年泰國政治危機.md "wikilink")
  - [2010年泰国反政府示威](../Page/2010年泰国反政府示威.md "wikilink")
  - [2011年泰國水災](../Page/2011年泰國水災.md "wikilink")
  - [2013年泰国反政府示威](../Page/2013年泰国反政府示威.md "wikilink")

## 参考文献

## 外部連結

  - [泰王国政府官方网站](http://www.thaigov.go.th/)

  - [泰國觀光局](http://www.tattpe.org.tw/)

  - [泰国中央银行](http://www.bot.or.th/)

  - [泰国文化部](http://www.m-culture.go.th/)

  - [更多来自泰国](http://www.sweetv.co.kr/)

  - [泰国旅游图片](http://www.pinterest.com/southeastasia/thailand-travel/)

  - [泰国旅游指南(英文)](http://www.southeastasiatrip.com/thailand/)

  -
  -
  - [維客旅行上的](../Page/維客旅行.md "wikilink")[泰国](http://wikitravel.org/zh/泰国)

  -
  -
  - \[//maps.google.com/maps?q=泰国 谷歌地圖\]

[\*](../Category/泰國.md "wikilink") [T](../Category/亚洲国家.md "wikilink")
[T](../Category/王國.md "wikilink")
[Thailand](../Category/君主立憲國.md "wikilink")
[Category:佛教国家](../Category/佛教国家.md "wikilink")

1.

2.  [Thailand (Siam)
    History](http://www.csmngt.com/thailand_history.htm), CSMngt-Thai.

3.

4.  [他信指派副總理奇猜行使泰國看守內閣總理職責](http://news.xinhuanet.com/newscenter/2006-04/05/content_4387858.htm)

5.

6.

7.  [1](http://news.xinhuanet.com/world/2006-09/22/content_5124752.htm)

8.  [2](http://news.xinhuanet.com/world/2006-10/01/content_5161358.htm)

9.  [3](http://news.xinhuanet.com/world/2006-10/09/content_5182050.htm)

10.
11. 張秋來，凌朔，[泰國舉行新憲法草案全民公決](http://news.xinhuanet.com/newscenter/2007-08/19/content_6562761.htm)，新華網

12. 沈敏，凌朔，[泰國新憲法草案通過公決](http://news.xinhuanet.com/newscenter/2007-08/21/content_6574384.htm)，新華網

13. [主持法官就職儀式 泰王蒲美蓬罕見露面](https://www.thenewslens.com/article/32630)

14.

15.

16.

17.

18.