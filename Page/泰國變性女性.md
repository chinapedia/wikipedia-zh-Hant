[Nongthoomfairtex.jpg](https://zh.wikipedia.org/wiki/File:Nongthoomfairtex.jpg "fig:Nongthoomfairtex.jpg")是泰國有名[變性女](../Page/泰國變性女人.md "wikilink")[泰拳冠軍](../Page/泰拳.md "wikilink")。她的故事曾被拍成電影《**[美麗拳王](../Page/美麗拳王.md "wikilink")**》。\]\]
[kathoey.jpg](https://zh.wikipedia.org/wiki/File:kathoey.jpg "fig:kathoey.jpg")[娜娜廣場跳](../Page/娜娜廣場.md "wikilink")[艷舞的跨性別女性](../Page/艷舞.md "wikilink")\]\]
**泰國變性人**（，[皇家轉寫](../Page/皇家泰語轉寫通用系統.md "wikilink")：Kathoey，[IPA](../Page/國際音標.md "wikilink")：\[kaʔtʰɤːj\]）是指由男性變为女性的[泰國](../Page/泰國.md "wikilink")[變性人](../Page/變性.md "wikilink")，又名**ladyboy**。許多華人常將其稱為「泰國人妖」。

在古代，กะเทย是指[雌雄同體](../Page/雌雄同體.md "wikilink")，與現代用來指[異裝者](../Page/異裝者.md "wikilink")、[變性者或](../Page/變性者.md "wikilink")[男同志不同](../Page/男同志.md "wikilink")。

## 簡介

現代幾乎所有的泰國變性人都有接受[荷爾蒙補充療法](../Page/荷爾蒙補充療法.md "wikilink")，大部分有作過[隆胸手術](../Page/隆胸.md "wikilink")，有些已接受[變性手術以及其他](../Page/變性手術.md "wikilink")[整形手術](../Page/整形.md "wikilink")。通常泰國變性人很年輕就開始[異裝](../Page/異裝.md "wikilink")、注射[荷爾蒙](../Page/荷爾蒙.md "wikilink")、整形等等。泰國變性人的異裝形象與身材通常非常女性化。

在泰國，還有專門培養變性人的學校。一般是從小孩兩三歲時開始培養。培養的方式是以女性化為標準，女式衣著、打扮、女性行為方式、女性的愛好。同時，更重要的一點是吃女性荷爾蒙藥。

這種藥的作用在於抑制男性生殖器官的發育，促進體內新陳代謝，並向女性發展。一般有十多年的服藥期。十多年後，男性生理特徵便逐漸萎縮，如男性陽具，就會變得又短又小，而皮膚就會變得細潤，有光澤，臀部、胸部會越發達，像女性一樣，肌肉減少，皮下脂肪增多，皮膚富於彈性，胸乳增長快的，比普通女性還高聳、渾圓、挺拔。

很多泰國變性人在娛樂業與旅遊業作舞者，歌舞秀，以及[性工作](../Page/性工作.md "wikilink")。在娛樂業與旅遊業之外的並不多，有些則是知名[美容](../Page/美容.md "wikilink")[理髮師](../Page/理髮.md "wikilink")。在泰國，如果一个家庭有個成為變性人的兒子，父母通常都會感到很失望。法律則完全不承認變性人。即使在變性手術之後，變性人也不能變更其法定性別。

泰國變性人通常來自社會低層，自殺率顯著高出一般人\[1\]。其寿命也比自然发育的人要更短。

[Kathoy1649.jpg](https://zh.wikipedia.org/wiki/File:Kathoy1649.jpg "fig:Kathoy1649.jpg")作歌舞秀的kathoey\]\]
1996年，一個大半由[男同志與泰國變性人組成的排球隊](../Page/男同志.md "wikilink")（外號「鐵娘子」）贏得全國[排球大賽冠軍](../Page/排球.md "wikilink")。但是泰國政府以國家形象為由禁止其中兩個變性人以國手隊的名义参加國際比賽。泰國變性人排球隊的事件在2000年改編成賣座電影《[人妖打排球](../Page/人妖打排球.md "wikilink")》，並於2003年推出續集。

根據[東森新聞](../Page/東森新聞.md "wikilink")2007年6月與7月的報導，已常常見到娛樂業與[旅遊業之外的變性人](../Page/旅遊業.md "wikilink")，一所技術學校也設立了變性人專用廁所，同志和變性人並且於此時成立「泰國同性戀政治群」，目的是为了同志權利向政府进行施壓。

在2015年，泰國將制定的新憲法中將承認第三性的存在，並保障他們的權益，此將使泰國成為亞洲承認第三性國家包含[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[尼泊爾的一員](../Page/尼泊爾.md "wikilink")，然而在[泰國護照只能填男女](../Page/泰國護照.md "wikilink")，而[同性婚姻也還不合法](../Page/同性婚姻.md "wikilink")。\[2\]

## 相關條目

  - 泰國電影[美麗拳王中](../Page/美麗拳王.md "wikilink")，主角的現實身份 -
    [帕莉亞](../Page/帕莉亞.md "wikilink")，是一位參加[泰拳比賽的知名變性人](../Page/泰拳.md "wikilink")。

## 相關

  - [人妖](../Page/人妖.md "wikilink")

## 外部連結

  - Peter Jackson, [Performative Genders, Perverse Desires: A
    Bio-History of Thailand's Same-Sex and Transgender
    Cultures](http://wwwsshe.murdoch.edu.au/intersections/issue9/jackson.html).
    （"The Homosexualisation of Cross-Dressing"那段）

  - Andrew Matzner: [In Legal Limbo: Thailand, Transgendered Men, and
    the
    Law](https://web.archive.org/web/20041120094937/http://home.att.net/~leela2/inlegallimbo.htm),
    1999.描述1999年時kathoey的一般社會與法律狀況。

[Category:跨性別](../Category/跨性別.md "wikilink")
[Category:泰国LGBT](../Category/泰国LGBT.md "wikilink")
[Category:變性人](../Category/變性人.md "wikilink")

1.  [1](http://kwankaiman.blogspot.tw/2014/04/transsexualmarriage4.html)，
    (Heyer 2011, p. 91) 在2010年10月的一個調查訪問了七千個變性人，發現自殺率是41%。(Heyer 2011,
    p. 2) 此外，Laura’s Playground是一個支援變性人的網站，它指出每年有數以千計的變性人喪失生命。(Heyer
    2011, p. 23)。
2.  [2](http://world.yam.com/post.php?id=3212)，泰國憲法將承認第三性。