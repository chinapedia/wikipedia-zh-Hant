[Thailand_film_clapperboard.svg](https://zh.wikipedia.org/wiki/File:Thailand_film_clapperboard.svg "fig:Thailand_film_clapperboard.svg")

泰國電影列表是關於[泰國出品的](../Page/泰國.md "wikilink")[電影列表](../Page/泰國電影.md "wikilink")。

## 0-9

  - [13 駭人遊戲](../Page/13_駭人遊戲.md "wikilink") *13 beloved*
  - *14 Beyond*
  - *2022 Tsunami*
  - [鬼4虐](../Page/鬼4虐.md "wikilink") *4Bia*
  - [愛4狂潮](../Page/愛4狂潮.md "wikilink") *4 Romance*
  - [69兩頭勾](../Page/69兩頭勾.md "wikilink") *6ixtynin9*
  - [逃妻七日情](../Page/逃妻七日情.md "wikilink") *7 Days to Leave My Wife*
  - [九寺贖魂](../Page/九寺贖魂.md "wikilink") *9 Temples*
    （[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［幻想無限-[泰國](../Page/泰國.md "wikilink")］單元）
  - *9.9.81 阴缘*

## A

  - [鐵雞諜網種情花](../Page/鐵雞諜網種情花.md "wikilink") *The Adventure of Iron
    Pussy*
  - [連體陰](../Page/連體陰.md "wikilink") *Alone*
  - *A Moment in June*
  - *Anueng*
  - [邪降：惡魔的藝術](../Page/邪降：惡魔的藝術.md "wikilink") *Art of the Devil 2*
  - [邪降3：鬼影隨行](../Page/邪降3：鬼影隨行.md "wikilink") *Art of the Devil 3*

## B

  - [無聲殺手](../Page/無聲殺手.md "wikilink") *Bangkok Dangerous*
  - [鬼債](../Page/鬼債.md "wikilink") *Bangkok Haunted*
  - [曼鼓瘋](../Page/曼鼓瘋.md "wikilink") *Bangkok Loco*
  - [曼谷愛情故事](../Page/曼谷愛情故事.md "wikilink") *Bangkok Love Story*
  - [美麗拳王](../Page/美麗拳王.md "wikilink") *Beautiful Boxer*
  - *Beautiful Wonderful Perfect*
  - *Before Valentine*
  - [愛久彌新](../Page/愛久彌新.md "wikilink") '' Best of Times''
  - [再見！流浪犬](../Page/再見！流浪犬.md "wikilink") *Bite of Love*
  - [極樂森林](../Page/極樂森林.md "wikilink") *Blissfully Yours*
  - *Blue Sky of Love*
  - [鬼肢解](../Page/鬼肢解.md "wikilink") *Body\#19*
  - [曼谷保鑣之危險英雄](../Page/曼谷保鑣之危險英雄.md "wikilink") *The Bodyguard*
  - [曼谷保鑣](../Page/曼谷保鑣.md "wikilink") *The Bodyguard 2*
  - [鐵血拳霸](../Page/鐵血拳霸.md "wikilink") *Brave*
  - [下一站，说爱你／曼谷轻轨恋曲](../Page/下一站說愛你.md "wikilink") *Bangkok Traffic Love
    Story*
  - *Bullet Teen*

## C

  - [人鬼鬧墳場](../Page/人鬼鬧墳場.md "wikilink") *Chaos At The Grveyard*
  - *Cheerleader Queens*
  - [致命巧克力](../Page/致命巧克力.md "wikilink") *Chocolate*
  - [大狗民](../Page/大狗民.md "wikilink") *Citizen Dog*
  - [棺材](../Page/棺材\(泰國電影\).md "wikilink") *The Coffin*
  - [卡到陰](../Page/卡到陰.md "wikilink") *Colic*
  - [鬼片](../Page/鬼片.md "wikilink") *Coming Soon*
  - *Crazy*
  - [鬼胡琴](../Page/鬼胡琴.md "wikilink") *Crying Fiddle*
  - [鬼髮2](../Page/鬼髮2.md "wikilink") *Cursed Hair*

## D

  - [特警霸王花](../Page/特警霸王花.md "wikilink") *Dangerous Flowers（Chai Lai）*
  - [親愛的伽利略](../Page/親愛的伽利略.md "wikilink") *Dear
    Galileo*(高雄電影節第十屆［焦點導演］單元)
  - [蛇博士](../Page/蛇博士.md "wikilink") *Devil Species*
  - [隔世冤靈](../Page/隔世冤靈.md "wikilink") *Diecovery*
  - [鬼宿舍](../Page/鬼宿舍.md "wikilink") *Dorm*
  - [功夫拳霸](../Page/功夫拳霸.md "wikilink") *Dynamite Warrior*
  - [鬼機NO.8](../Page/鬼機NO.8.md "wikilink") *Dark Flight 3D*

## G

  - [死人錢😴😴](../Page/死人錢😴😴.md "wikilink") '😴'😴 coins''

## E

  - [見鬼](../Page/見鬼.md "wikilink") *The Eye*
  - [見鬼2](../Page/見鬼2.md "wikilink") *The Eye 2*
  - [見鬼10](../Page/見鬼10.md "wikilink") *The Eye 10*

## F

  - [絕魂印](../Page/絕魂印.md "wikilink") *The Fatality*
  - [拳速反擊](../Page/拳速反擊.md "wikilink") *Fighting Beat*
  - *Final Score*
  - [籃球火](../Page/籃球火\(泰國電影\).md "wikilink") *Fireball*
  - [初戀那件小事](../Page/初戀那件小事.md "wikilink") *First Love (A Little Thing
    Called Love)*
  - [純友誼](../Page/純友誼\(泰國電影\).md "wikilink") *Friendship*
  - [第一拳](../Page/第一拳\(泰國電影\).md "wikilink") *First fight*
  - [离间](../Page/离间\(泰國電影\).md "wikilink") *Friendship break down*

## G

  - [鬧鬼](../Page/鬧鬼\(泰國電影\).md "wikilink") *Ghost Game*
  - [守護靈](../Page/守護靈.md "wikilink") *Ghost Mother*
  - [幽魂娜娜2：鬼剎](../Page/幽魂娜娜2：鬼剎.md "wikilink") *Ghost of Mae Nak*
  - [賞我一個妹](../Page/賞我一個妹.md "wikilink") *The Gig*
  - *Go-Six*
  - [โปรแกรมหน้า วิญญาณอาฆาต](../Page/鬼片.md "wikilink") *โปรแกรมหน้า
    วิญญาณอาฆาต*

## H

  - [雙鬼計](../Page/雙鬼計.md "wikilink") *Hala*
  - [一路上有手](../Page/一路上有手.md "wikilink") *Handle Me With Care*
  - [卡娣的幸福](../Page/卡娣的幸福.md "wikilink") *The Happiness of Kati*
  - [來不及說生日快樂](../Page/來不及說生日快樂.md "wikilink") *Happy Birthday*
  - [人頭蠱](../Page/人頭蠱.md "wikilink") *The Haunted Drum*
  - [頑皮鬼](../Page/頑皮鬼.md "wikilink") *Haunting Me*
  - [斷頭家族](../Page/斷頭家族.md "wikilink") *Headless Family*
  - [荷爾蒙/愛我一下．夏](../Page/荷爾蒙/愛我一下．夏.md "wikilink") *Hormones*
  - [鬼店](../Page/鬼店\(泰國電影\).md "wikilink") *The Hotel*
  - [香頌鬼屋](../Page/香頌鬼屋.md "wikilink") *The House*
  - [水底冤靈](../Page/水底冤靈.md "wikilink") *Hunch*

## I

  - [罪孽成佛](../Page/罪孽成佛.md "wikilink") *In the Shadow of the Naga*
    （[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［幻想無限-[泰國](../Page/泰國.md "wikilink")］單元）
  - *The Intruder*
  - [人妖打排球](../Page/人妖打排球.md "wikilink") *The Iron Ladies*
  - [人妖打排球2：鐵娘子](../Page/人妖打排球2：鐵娘子.md "wikilink") *The Iron Ladies 2*

## J

  - [晚孃](../Page/晚孃_\(2001年電影\).md "wikilink") *Jan Dara*（2001年）
  - [晚孃](../Page/晚孃_\(2012年電影\).md "wikilink") *Jan Dara*（2012年）

## K

  - [國王密令](../Page/國王密令.md "wikilink") *The King Maker*
  - [鱷妻](../Page/鱷妻.md "wikilink") *Krai Thong*

## L

  - [宇宙的最後生命](../Page/宇宙的最後生命.md "wikilink") *Last Life in the Universe*
  - [悲戀三人行](../Page/悲戀三人行.md "wikilink") *The Last Moment*
  - [暹羅女王](../Page/暹羅女王.md "wikilink") *The Legend of Suriyothai*
  - *The Legend of the Queen*
  - [吊死詭](../Page/吊死詭.md "wikilink") *Letters of Death*
  - [愛在暹邏](../Page/愛在暹邏.md "wikilink") *The Love of Siam*
  - [戀愛超男女](../Page/戀愛超男女.md "wikilink") *Loser Love*
  - [我死在去年夏天](../Page/我死在去年夏天.md "wikilink") *Last Summer*

## M

  - [我的兄弟情人](../Page/我的兄弟情人.md "wikilink") *My Bromance*
  - [蛇妻](../Page/蛇妻.md "wikilink") *Mae bia*
  - [人肉麵線](../Page/人肉麵線.md "wikilink") *Meat Grinder*
  - [湄公河滿月盛宴](../Page/湄公河滿月盛宴.md "wikilink") *Mekhong Full Moon Party*
  - [靈虐](../Page/靈虐.md "wikilink") *Memory*
  - *Me...Myself*
  - [水銀俠](../Page/水銀俠.md "wikilink") *Mercury Man*
  - [小麻煩上電視](../Page/小麻煩上電視.md "wikilink") *Mheejou*
  - [午夜，我的愛](../Page/午夜，我的愛.md "wikilink") *Midnight My Love*
  - [真情收音機](../Page/真情收音機.md "wikilink") *Mon-Rak Transistor*
  - *Mor3pee4*
  - [泰南拳](../Page/泰南拳.md "wikilink") *Muay Thai Chaiya*
  - *My Best Bodyguard*
  - *My EX*
  - [小情人](../Page/小情人.md "wikilink") *My Girl（Fan
    Chan）*（[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［焦點導演］單元）
  - [正午顯影](../Page/正午顯影.md "wikilink") *Mysterious Object at Noon*

## N

  - [小鬼也瘋狂](../Page/小鬼也瘋狂.md "wikilink") *Nak*
    （[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［幻想無限-[泰國](../Page/泰國.md "wikilink")］單元）
  - [幽魂娜娜](../Page/幽魂娜娜.md "wikilink") *Nang nak*
  - [通靈巫師](../Page/通靈巫師.md "wikilink") *Necromancer*
  - [鬼鄰](../Page/鬼鄰之亡記.md "wikilink") *Next Door*
  - *Nymph*

## O

  - [蝴蝶魚](../Page/蝴蝶魚.md "wikilink") *Ocean Butterfly*
  - [拳霸](../Page/拳霸.md "wikilink") *Ong Bak*
  - [拳霸3](../Page/拳霸3.md "wikilink") *Ong Bak 2*
  - [神探‧人獸‧機關槍](../Page/神探‧人獸‧機關槍.md "wikilink") *Opapatika*
  - [最後的木琴師](../Page/最後的木琴師.md "wikilink") *The Overture*

## P

  - [淒厲人妻／嚇鬼阿嫂](../Page/淒厲人妻／嚇鬼阿嫂.md "wikilink") *Pee Mak*
  - *Phobia*
  - [人魚傳說](../Page/人魚傳說.md "wikilink") *Phra-Apai Mani*
  - [快樂工廠](../Page/快樂工廠.md "wikilink") *Pleasure Factory*
  - [愛情保鮮期](../Page/愛情保鮮期.md "wikilink") *Ploy*
  - [蛇靈](../Page/蛇靈.md "wikilink") *The Poison*
  - *Power Kids*

## Q

  - [海上的女皇](../Page/海上的女皇.md "wikilink") ''The Queen of LangKasuka ''

## R

  - [609猛鬼套房](../Page/609猛鬼套房.md "wikilink") *Rahtree: Flower of The
    Night*
  - [609猛鬼附身](../Page/609猛鬼附身.md "wikilink") *Rahtree Reborn*
  - [609猛鬼終結者](../Page/609猛鬼終結者.md "wikilink") *Rahtree Revenge*
  - [24小時愛情便利店](../Page/24小時愛情便利店.md "wikilink") *Rakna 24 Hours*
  - [飛俠紅鷹](../Page/飛俠紅鷹.md "wikilink") *The Red
    Eagle*（[高雄電影節第十屆閉幕片](../Page/高雄電影節.md "wikilink")）
  - *Roommate*
  - [湄江情浪](../Page/湄江情浪.md "wikilink")*Romance in Mekong
    River*（1933年泰國華僑本地制作的粵語片）

## S

  - [早安！琅勃拉邦](../Page/早安！琅勃拉邦.md "wikilink") *Sabaidee Luang Prabang*
  - [足球娘子軍](../Page/足球娘子軍.md "wikilink") *Sassy Player*
  - [奪命開學禮](../Page/奪命開學禮.md "wikilink") *Scared*
  - [泰難忘男人味](../Page/泰難忘男人味.md "wikilink") *Scent of Thai Male*
  - *The Scout*
  - [季节变换](../Page/季节变换.md "wikilink") *Seasons Change*
    （[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［焦點導演］單元）
  - [頭七](../Page/頭七\(泰國電影\).md "wikilink") *Seven Days in Coffin*
  - [鬼影](../Page/鬼影_\(電影\).md "wikilink") *Shutter*
  - [恐怖護理站](../Page/恐怖護理站.md "wikilink") *Sick Nurses*
  - [鬼怨](../Page/鬼怨.md "wikilink") *The Sisters*
  - [鬼詛咒](../Page/鬼詛咒.md "wikilink") *Six*
  - [致命切割](../Page/致命切割.md "wikilink") *Slice*
    （[高雄電影節第十屆](../Page/高雄電影節.md "wikilink")［幻想無限-[泰國](../Page/泰國.md "wikilink")］單元）
  - [蛇女](../Page/蛇女.md "wikilink") *The Snake King's Child*
  - *Spicy Beauty Queen in Bangkok*
  - [屍魂落魄](../Page/屍魂落魄.md "wikilink") *The Spiritual World*
  - [聖麒麟傳說](../Page/聖麒麟傳說.md "wikilink") *Sudsakorn*
  - [戀愛症候群](../Page/戀愛症候群.md "wikilink") *Syndromes and a Century*
  - [音為愛](../Page/音為愛.md "wikilink") *Suckseed*
  - [暹羅浴血戰](../Page/暹羅浴血戰.md "wikilink") ''Siam Yuth: The Dawn of the
    Kingdom ''
  - [暹羅復興錄](../Page/暹羅復興錄.md "wikilink")

## T

  - [黑虎的眼淚](../Page/黑虎的眼淚.md "wikilink") *Tears of the Black Tiger*
  - [三更](../Page/三更之輪迴.md "wikilink") *Three*
  - *Three Cripples*（*Yern Peh Lay semakute*）
  - [神探虎刀](../Page/神探虎刀.md "wikilink") *The Tiger Blade*
  - [拳霸2](../Page/拳霸2.md "wikilink") *Tom yum goong*
  - [猛鬼列車](../Page/猛鬼列車.md "wikilink") *Train of the Dead*
  - [熱帶幻夢](../Page/熱帶幻夢.md "wikilink") *Tropical Malady*

## U

  - [著鬼](../Page/著鬼.md "wikilink") *The Unborn*
  - [複製人](../Page/複製人\(泰國電影\).md "wikilink") *Unhuman*
  - *Unlimited Love*
  - [庭怨森森](../Page/庭怨森森.md "wikilink") *The Unseeable*
  - [鬼嬰廟](../Page/鬼嬰廟.md "wikilink") *The Unborn child*

## V

  - [邪靈復仇](../Page/邪靈復仇.md "wikilink") *Vengeance*
  - [嚇死鬼](../Page/嚇死鬼.md "wikilink") *The Victim*
  - [駭人手機](../Page/駭人手機.md "wikilink") *Video Clip*
  - [求神問鬼](../Page/求神問鬼.md "wikilink") *Vow of Death*

## W

  - [傷心蔚藍海](../Page/傷心蔚藍海.md "wikilink") *Wonderful Town*

## X

## Y

  - [想愛就愛](../Page/想愛就愛.md "wikilink")*Yes or No*
  - [想愛就愛2](../Page/想愛就愛2.md "wikilink")''Yes or No２

## Z

[Category:泰國電影](../Category/泰國電影.md "wikilink")
[Category:各國電影列表](../Category/各國電影列表.md "wikilink")
[Category:泰国相关列表](../Category/泰国相关列表.md "wikilink")