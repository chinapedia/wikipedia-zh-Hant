**泰坦二號運載火箭**原為洲際飛彈，後由[格伦·L·马丁公司改良](../Page/格伦·L·马丁公司.md "wikilink")，[泰坦一號運載火箭也是如此](../Page/泰坦一號運載火箭.md "wikilink")。

## 任務

大力神II型洲际导弹為獨創性的[長程洲際彈道飛彈](../Page/長程洲際彈道飛彈.md "wikilink")（[ICBM](../Page/ICBM.md "wikilink")），以前為[美國空軍](../Page/美國空軍.md "wikilink")（[美國太空總署及](../Page/美國太空總署.md "wikilink")[美國國家海洋和大氣管理局所屬](../Page/美國國家海洋和大氣管理局.md "wikilink")）的中型[火箭](../Page/火箭.md "wikilink")，所發射[衛星包含USAF防禦飛](../Page/衛星.md "wikilink")[氣象衛星](../Page/氣象衛星.md "wikilink")（DMSP）RU6[美國國家海洋和大氣管理局所屬的](../Page/美國國家海洋和大氣管理局.md "wikilink")[大氣衛星](../Page/大氣衛星.md "wikilink")，改良後的泰坦二號火箭由[加利福尼亞洲的](../Page/加利福尼亞洲.md "wikilink")[范登堡](../Page/范登堡.md "wikilink")[空軍基地發射至](../Page/空軍.md "wikilink")2003年止。

## 特徵

泰坦二號火箭為兩節式[液態火箭](../Page/液態火箭.md "wikilink")，設計用來發射中小型[衛星](../Page/衛星.md "wikilink")，他發射4200磅（1900公斤）的[衛星至](../Page/衛星.md "wikilink")[低地球軌道](../Page/低地球軌道.md "wikilink")，第一節由兩種液態燃料組成[發動機為飛行噴射](../Page/發動機.md "wikilink")（Aerojet）LR87型；第二節則是飛行噴射（Aerojet）LR91型。

## 演變

[Gemini-Titan_11_Launch_-_GPN-2000-001020.jpg](https://zh.wikipedia.org/wiki/File:Gemini-Titan_11_Launch_-_GPN-2000-001020.jpg "fig:Gemini-Titan_11_Launch_-_GPN-2000-001020.jpg")，在1966年9月12日\]\]
泰坦系列在1955年10月就成立了，[公司名稱為](../Page/公司.md "wikilink")[馬丁公司](../Page/馬丁公司.md "wikilink")，並贏得[美國太空總署的合約](../Page/美國太空總署.md "wikilink")，所以[馬丁公司開始建造](../Page/馬丁公司.md "wikilink")[長程洲際彈道飛彈](../Page/長程洲際彈道飛彈.md "wikilink")（[ICBM](../Page/ICBM.md "wikilink")）那便是[大力神I型洲际导弹](../Page/泰坦一號火箭.md "wikilink")，它是第一台有兩節式的[長程洲際彈道飛彈](../Page/長程洲際彈道飛彈.md "wikilink")（[ICBM](../Page/ICBM.md "wikilink")）及地下隱藏的[發射台](../Page/發射台.md "wikilink")。[大力神I型洲际导弹存在一些不足](../Page/泰坦一號火箭.md "wikilink")：因使用液氧，液氧必须低温保存，并会在存储期间蒸发，所以[大力神I型洲际导弹以](../Page/泰坦一號火箭.md "wikilink")3座发射井共用同一地下液氧制造厂和指挥所部署；[大力神I型洲际导弹不能直接从发射井发射](../Page/泰坦一號火箭.md "wikilink")，须先以升降平台整个推出发射井口，于露天才可加注燃料及点火发射。

[美國太空總署便多給了一個改良的機會](../Page/美國太空總署.md "wikilink")，可以酬載更大的[彈頭也可以提高命中率並減少發射所需的時間](../Page/彈頭.md "wikilink")。[馬丁公司收到一份契約書](../Page/馬丁公司.md "wikilink")，因此設計了SM-68B引擎，為泰坦二號運載火箭之引擎，在1960年6月，泰坦二號運載火箭比[泰坦一號運載火箭重了百分之五十](../Page/泰坦一號運載火箭.md "wikilink")，而第一節比[泰坦一號運載火箭長且第二節比](../Page/泰坦一號運載火箭.md "wikilink")[泰坦一號運載火箭的](../Page/泰坦一號運載火箭.md "wikilink")[直徑寬](../Page/直徑.md "wikilink")，泰坦二號運載火箭也用穩定性較高的[四氧化二氮](../Page/四氧化二氮.md "wikilink")/[聯氨之推進劑](../Page/聯氨.md "wikilink")，可以在常温下长期保存，免去了[泰坦一號運載火箭所需的液氧制造厂](../Page/泰坦一號運載火箭.md "wikilink")，但其缺點為具强腐蚀性和自燃現象，所以只要有任何一地破洞漏氣，即可能發生爆炸。而[泰坦二號運載火箭的发射井也做了改变](../Page/泰坦二號運載火箭.md "wikilink")，增设了耐火壁和溢流槽，可在发射井中加注燃料并发射，也免去了用升降机推出井口的时间。但与同样使用[四氧化二氮](../Page/四氧化二氮.md "wikilink")/[聯氨推進劑的苏联](../Page/聯氨.md "wikilink")[R-36撒旦不同](../Page/R-36.md "wikilink")，[泰坦二號運載火箭的燃料罐防腐蚀工艺一般](../Page/泰坦二號運載火箭.md "wikilink")，不能长期存贮燃料，所以实用中燃料仍需在發射前加注，与[泰坦一號運載火箭相同](../Page/泰坦一號運載火箭.md "wikilink")。

第一次發射泰坦二號運載火箭的時候是1960年12月,而與泰坦二號運載火箭較相似的是[LGM-25](../Page/LGM-25.md "wikilink"),且較具發展潛力，泰坦二號運載火箭部署核子武器則要等到1963年10月,其所搭載的為W-53核子彈頭，可飛行達15000[公里](../Page/公里.md "wikilink")(9325[哩](../Page/哩.md "wikilink"))，爆炸威力約等於900萬[噸](../Page/噸.md "wikilink")[黃色炸藥](../Page/黃色炸藥.md "wikilink")，而且具有導行之功能，精確瞄準目標。泰坦二號運載火箭之後由[美國太空總署接手](../Page/美國太空總署.md "wikilink")，載1960年代發射十次[雙子星座號宇宙飞船](../Page/双子星座计划.md "wikilink")，同一時期也有運載更強的核子武器，約等同3500萬噸黃色炸藥。

## 退役

泰坦二號運載火箭發射系統從1982年7月逐漸減量，至1987年6月完成，全面停止使用，儲存於亞利桑那洲的戴維斯----馬森(Davis
Monthan)AFB倉庫，在此之前，泰坦二號運載火箭是最強大的後備部署核子火箭。

## 歷史

**泰坦二號運載火箭**從1963年發射至1987年，原先63枚部署在[范登堡空軍基地](../Page/范登堡空軍基地.md "wikilink"),1978年8月24日，[氧化劑因漏出導致兩名工人喪身](../Page/氧化劑.md "wikilink")，另一次發生在1980年9月19日,燃料滲出導致一名太空人死亡，而且發射臺也損毀。

## 參見

  - [泰坦23G運載火箭](../Page/泰坦23G運載火箭.md "wikilink")

[Category:泰坦運載火箭](../Category/泰坦運載火箭.md "wikilink")