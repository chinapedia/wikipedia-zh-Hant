**泰语人名**的次序是先名后姓，這個傳統來源於[印度](../Page/印度.md "wikilink")。泰语的姓氏数量庞大，几乎每个家族的姓氏都是独一无二的，有泰人亦会相对频繁地更改姓氏。1913年泰国（时称[暹罗](../Page/暹罗.md "wikilink")）法律规定每一名公民都必须拥有姓氏\[1\]，而在此之前许多泰人只有名字。

泰人姓氏常含有积极的意义，而且较长，尤其是泰国[华人的姓氏](../Page/华人.md "wikilink")。例如泰国前总理[他信·西那瓦](../Page/他信·西那瓦.md "wikilink")（汉名丘达新），其姓氏“西那瓦”取用于1938年，意为“多多行善”。根据泰国现行人名法BE
2505
(1962)\[2\]，泰人姓氏不能多于10个[泰语字母](../Page/泰语字母.md "wikilink")，不包括元音符号和变音符号。该法律还规定新创制的姓氏不能与任何其它姓氏相仿，但这个规定在过去是无法完全落实的，因为当时并不存在计算机数据库，因此也有极少数互相毫无关系的泰人拥有相同姓氏\[3\]。有些地名会以的形式出现于姓氏之中。

## 常见人名列表

## 泰语译音表

## 参考文献

## 参见

  - [改姓名運動](../Page/改名#東南亞排華的自保措施.md "wikilink")

{{-}}

[Category:泰语](../Category/泰语.md "wikilink")
[Thai](../Category/各文化人名.md "wikilink")
[Category:泰國文化](../Category/泰國文化.md "wikilink")

1.
2.
3.