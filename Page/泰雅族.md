[Atayal_Culture,TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:Atayal_Culture,TAIWAN.jpg "fig:Atayal_Culture,TAIWAN.jpg")的[泰雅族織布文物館](../Page/泰雅族織布文物館.md "wikilink")\]\]

**泰雅族**（**Atayal**），舊稱**泰耶尔**，屬於[南島語族的一支](../Page/南島語族.md "wikilink")，是在台灣的第三大原住民族群，為典型的高山民族，古有出草獵人頭習俗。現世居台灣中央山脈旁的聚落。

泰雅人分佈於台灣島上的山地高山，為[南島民族之一](../Page/南島民族.md "wikilink")。生活幾千年的[臺灣原住民族群](../Page/臺灣原住民.md "wikilink")，分佈於北部[中央山脈兩側](../Page/中央山脈.md "wikilink")，東至[花蓮](../Page/花蓮縣.md "wikilink")[太魯閣](../Page/太魯閣.md "wikilink")，西至[東勢](../Page/東勢區.md "wikilink")，北到[烏來](../Page/烏來區.md "wikilink")，南迄[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")，曾是台灣原住民中分佈面積最廣的一族。

泰雅族的族名原意為「真人」或「勇敢的人」。考古學家認為在距今五千年前，泰雅族就開始在台灣活動，在濁水溪上游曾發現他們的遺址，年代約為西元前2000年至3000年間。

由於泰雅族的方言群眾多，一般將泰雅族分為兩大語系亞族，分別是泰雅亞族與賽德克族，共有二十五個方言群體。根據不同方言群泰雅族人的傳說，其祖先起源包括三個地方，首先是於雪山山脈（大霸尖山），其次是今南投縣仁愛鄉的發祥村瑞岩部落，再來是位於南投縣仁愛鄉中央山脈的白石山。後來因人口增長開始往西北方向、東部及西南方向遷移。
在日治時期，泰雅族被稱為北蕃，為台灣北部最大原住民族群，台灣南部最大族群則是被稱為南蕃的布農族。現在，泰雅族分佈在台灣北部中央山脈兩側，以及花蓮、宜蘭等山區，共分佈台灣北部八縣十三個鄉內。總人口數約九萬餘人，就人數而言，僅次於阿美族及排灣族，為台灣原住民族中的第三大族。
居住在新北市烏來區的泰雅族聚落，是全世界南島語系居住地中，最北方的聚落。

## 族系分支

依其語言及風俗習慣的不同，傳統上，[人類學家通常將泰雅族分為](../Page/人類學.md "wikilink")「泰雅亞族」和「[賽德克族](../Page/賽德克族.md "wikilink")」兩個亞族，但在亞族之內還可以再繼續做分類：

  - 泰雅亞族（Tayal）
      - 賽考列克群（Sekoleq）
          - [馬卡納奇亞群](../Page/馬卡納奇亞群.md "wikilink")
            （Makanaji）：福骨群（Xalut/白狗群）、石加路群(Shyakalo)、金那基群(Mknazi)、大嵙崁群、南澳群
          - [馬列巴亞群](../Page/馬列巴亞群.md "wikilink")
            （Malepa）：[屈尺群](../Page/屈尺群.md "wikilink")、大嵙崁群、卡奧灣群(Gaogan)、溪頭群、司加耶武群(Sqoyaw)
          - [馬里闊丸亞群](../Page/馬里闊丸亞群.md "wikilink")
            （Malikoan）：馬里闊丸群(Mrqwan)、馬武督群(M'utu)
      - 澤敖列群（Tseole）
          - [馬巴阿拉亞群](../Page/馬巴阿拉亞群.md "wikilink")
            （Mabaala）：南澳群、馬巴阿拉群、萬大群
          - [馬巴諾亞群](../Page/馬巴諾亞群.md "wikilink") （Mapanox）：汶水群、北勢群、南勢群
          - [莫拿玻亞群](../Page/莫拿玻亞群.md "wikilink") （Menebo）：南澳群
          - [莫里拉亞群](../Page/莫里拉亞群.md "wikilink")
            （Mererax）：鹿場群、大湖群、加拉排群(Kalabai)
  - [賽德克族](../Page/賽德克族.md "wikilink")（Seediq）
      - [德魯固群](../Page/德魯固群.md "wikilink")（Seediq
        Truku/[太魯閣群](../Page/太魯閣族.md "wikilink")）
      - [道澤群](../Page/道澤群.md "wikilink")（Seediq Toda）
      - [德克達雅群](../Page/德克達雅群.md "wikilink")（Seediq Tgdaya）

值得注意的是，泰雅族內的不同方言群體雖然存有差距，但在文化與學術的分類上，自日本領台以來，一直被分類為同一個人群體，並以「泰雅族」做為族稱。

隨著台灣原住民民族自治的政治目標目益受到渴求與重視，花蓮部份賽德克亞族太魯閣群的人，不顧南投賽德克亞族太魯閣群以及花蓮其他賽德克亞族太魯閣群、道澤群、德克達雅群的反對，主張以太魯閣族做為新的族稱，而從暨有的泰雅族中脫離。這個政治目標在2004年1月14日獲得[中華民國](../Page/中華民國.md "wikilink")[行政院原住民族委員會的承認](../Page/行政院原住民族委員會.md "wikilink")，讓太魯閣族正式成為第十二個台灣原住民族\[1\]。

隨後，反對以太魯閣族做為新的族名的人，也發起正名運動，以「[賽德克族](../Page/賽德克族.md "wikilink")」為族名\[2\]，並於2008年4月23日正式成為第十四個台灣原住民族。

目前關於的泰雅族的各種論述，因為泰雅族與太魯閣族、賽德克族分立的情況，出現了文化分類與政治分類相悖的困境。

## 文化特質

在台灣原住民中，在臉上刺青（人類學家稱之為「[紋面](../Page/紋面.md "wikilink")」或「[文面](../Page/文面.md "wikilink")」）的民族有泰雅族與賽夏族，而賽夏族的文面主要與泰雅族互動生存相關，賽夏族人本身不會刺文，多由泰雅族人執行，也因此，台灣真正擁有長久文面文化基礎的，僅有泰雅族。
由於黥面一詞在漢文原為古代的刑罰，多有負面的意思（馬騰嶽
1998），目前一般學術界與民間均改以「文面」稱呼泰雅族人的此一文化，以視對於泰雅族文化的尊重。泰雅族語稱文面為ptasan，有四種意義：

1.  驅除邪魔的作用；
2.  美麗雅觀的效果；
3.  族系的識別；
4.  榮耀的象徵。

紋面對於男子而言，是成年的標誌也是勇武的象徵。對於女子，則是善於織布 ---
泰雅族正以其精緻的[織布聞名於世](../Page/紡織.md "wikilink")
--- 的標記。事實上，除了美觀、避邪，代表了女子的善織、男子的勇武以外，文面也是泰雅族死後認祖歸宗的標誌。

傳統上，泰雅族的男嬰出生一星期後，族中長老便會贈送一把刀，家人會將他的臍帶放在打獵用的藤盒，由母親將其抱至屋外，有向狩獵的路上，祝禱男嬰成為勇猛的獵人，可以得知泰雅族男性是天生的獵人，男子到十二、三歲後，外出時必配刀，片刻不離身，不論外出或戰鬥都隨時配刀，刀具可以說是泰雅族勇士的生命。泰雅族的刀具是原住民族群中，長度最長，彎度最彎，刀背最厚，文獻記載中，泰雅族之勇士刀背厚達1cm以上，刀長甚至有超過90cm以上者。

泰雅族女子在十三、四歲的時候，就開始跟著母親學習織布的技巧，也開始為自己準備出嫁時的衣裳。當少女的織藝精進，也就是准許在臉上刺青的時候了。然而，紋面的風俗在台灣的日治時期已經逐漸被禁止，目前仍保有紋面的泰雅人，都是八.九十歲以上的族人了。

歷史上，由於泰雅族剽悍善戰、堅韌耿直，因此在台灣的[清治時期](../Page/台灣清治時期.md "wikilink")[劉銘傳擔任](../Page/劉銘傳.md "wikilink")[台灣巡撫的六](../Page/台灣巡撫.md "wikilink")、七年間，厲行[開山撫番的政策下](../Page/開山撫番.md "wikilink")，幾乎年年出兵攻打泰雅族，如[大嵙崁之役等](../Page/大嵙崁之役.md "wikilink")，造成泰雅族人死傷者眾。[日治時期](../Page/台灣日治時期.md "wikilink")，也不時爆發激烈的抗日事件，其中又以霧社事件（註：賽德克族）最為猛烈。也因為這樣，泰雅族人的傳統[儀式和](../Page/儀式.md "wikilink")[文化](../Page/文化.md "wikilink")，被日本人破壞和禁止的項目特別多，也強力受到日本『[皇民化運動](../Page/皇民化運動.md "wikilink")』的改造。隨後[國民政府接收後](../Page/國民政府.md "wikilink")，變本加厲大力推行[同化政策](../Page/同化.md "wikilink")，強制族人改為漢人姓名，並禁止學校教授[族語](../Page/泰雅語.md "wikilink")，導致文化語言嚴重的斷層。

目前的學術研究中，關於泰雅族文面的研究，可以參考衛惠林〈台灣土著諸族文身習俗之研究〉，在台大考古學刊十五十六集合刊（1950）。馬騰嶽《泰雅族文面圖譜》（1998）

對獵場劃分與部落和解時會舉行[埋石立約儀式](../Page/埋石立約.md "wikilink")。

  -   - 泰雅族的特色飲食\~竹筒飯\*\*

泰雅族以《米飯》為主要食糧，但因為經常要外出工作，發明了獨特的《竹筒飯》。

外出的泰雅族會把米帶在身上，到肚子餓的時候，便砍下較幼小的《竹》《莖》，上方留孔，下方密封，把米從孔隙中倒進去，然後用蒸煮的方法把米飯弄熟，吃時只須把竹子掀開，便可嗅到香氣四溢的竹筒飯香。

泰雅族的竹筒飯以傳統的竹筒香原味為主，後來為了符合客人的口味才加入香菇、雞肉等食材。

## 社會組織

[Atayal_(1).jpg](https://zh.wikipedia.org/wiki/File:Atayal_\(1\).jpg "fig:Atayal_(1).jpg")的男女，攝於台灣日治時期。坐姿吹竹笛者戴用白色貝殼製成的圓形耳飾，也佩同樣的胸飾，這些只限馘首成功者佩戴。放在腳邊的是頭顱，只有在馘首祭，或面對頭顱時才吹竹笛，平時不吹。\]\]
傳統的泰雅族社會以[狩獵及](../Page/狩獵.md "wikilink")[山田燒墾為生](../Page/山田燒墾.md "wikilink")，[聚落以集居式的村落為主](../Page/聚落.md "wikilink")。大體而言，泰雅族的社會組織可以分成下列幾個團體：（1）部落組織；（2）[祭祀團體](../Page/祭祀團體.md "wikilink")；（3）共負罪責團體（gaga）；以及（4）狩獵團體。現簡單分述如下：

1.  部落組織：其部落的形成以血緣基礎，以[父系中心](../Page/父系.md "wikilink")，由諸兄弟形成聯合家族，財產與房舍共同擁有。由具聰明英勇有才智，有領導能力的人擔任部落領袖，遇到部落有重大事情的，則由頭目召集長老會議以決策之（泰雅社會中原來沒有頭目，是日治時期日本人為其治理之便而產生的。
2.  祭祀團體：有共同祭祀對象的家庭祖合，如為了[祖靈祭之進行而組成的團體](../Page/祖靈祭.md "wikilink")。
3.  共負罪責團體（gaga）：是共同遵守誡律和規範的團體。泰雅人稱之為gaga（一般稱為祭團），
    gaga是一種社會規範，是泰雅人日常生活、風俗習慣的誡律，也是最具有約束力與公權力的團體，可以算是泰雅社會中行為道德與社會律法的最高維護與審判者。
4.  狩獵團體：同一個獵團，集體狩獵時大家分工合作共勞共食共享。（改寫自鄉土文化教學與鄉土文化研究 ）

這四個團體的成員有互相重疊的特質，在不同部落，祭祀團體可能大於狩獵團體，其他的部落可能狩獵團體大於任何一個團體。但基本上各地的差異性頗大，無法一概而論。在這四個團體當中，由於gaga的性質最特別，也在學界當中獲得最多的關注，成為不少學者的研究議題。

泰雅族為父系社會，但質為平權。

## 統計

以下統計包含尚未正名之[太魯閣族與](../Page/太魯閣族.md "wikilink")[賽德克族人口](../Page/賽德克族.md "wikilink")

| 縣市  | 泰雅族人口  | 總人口        | 比率    |
| --- | ------ | ---------- | ----- |
| 花蓮縣 | 2,673  | 331,372    | 0.81% |
| 桃園市 | 19,701 | 2,136,702  | 0.92% |
| 新竹縣 | 16,009 | 544,624    | 2.94% |
| 宜蘭縣 | 12,575 | 457,808    | 2.75% |
| 南投縣 | 5,959  | 506,966    | 1.18% |
| 新北市 | 7,262  | 3,972,204  | 0.18% |
| 臺中市 | 8,787  | 2,754,191  | 0.32% |
| 苗栗縣 | 4,564  | 534,366    | 0.85% |
| 臺北市 | 2,692  | 2,702,925  | 0.10% |
| 雲林縣 | 1,452  | 705,440    | 0.21% |
| 高雄市 | 719    | 1,493,806  | 0.05% |
| 新竹市 | 677    | 395,746    | 0.17% |
| 基隆市 | 544    | 387,504    | 0.14% |
| 屏東縣 | 520    | 872,902    | 0.06% |
| 臺東縣 | 363    | 204,919    | 0.18% |
| 臺南市 | 315    | 1,120,394  | 0.03% |
| 彰化縣 | 275    | 1,255,332  | 0.02% |
| 嘉義縣 | 192    | 552,749    | 0.03% |
| 金門縣 | 108    | 56,275     | 0.19% |
| 澎湖縣 | 81     | 83,214     | 0.10% |
| 連江縣 | 75     | 17,775     | 0.42% |
| 嘉義市 | 74     | 266,126    | 0.03% |
| 總計  | 91,883 | 22,300,929 | 0.41% |
|     |        |            |       |

縣市別泰雅族人口 (2000年) \[3\]

| 鄉鎮市區   | 泰雅族人口 | 總人口     | 比率     |
| ------ | ----- | ------- | ------ |
| 南投縣仁愛鄉 | 7,620 | 12,891  | 59.11% |
| 花蓮縣秀林鄉 | 7,516 | 13,674  | 54.97% |
| 桃園市復興區 | 7,484 | 11,120  | 67.30% |
| 新竹縣尖石鄉 | 6,080 | 7,245   | 83.92% |
| 花蓮縣萬榮鄉 | 4,334 | 6,083   | 71.25% |
| 宜蘭縣南澳鄉 | 4,125 | 5,199   | 79.34% |
| 宜蘭縣大同鄉 | 3,902 | 4,995   | 78.12% |
| 苗栗縣泰安鄉 | 2,983 | 4,356   | 68.48% |
| 臺中市和平區 | 2,970 | 10,210  | 29.09% |
| 新竹縣五峰鄉 | 2,867 | 3,985   | 71.94% |
| 新竹縣竹東鎮 | 2,603 | 88,355  | 2.95%  |
| 桃園市大溪區 | 2,982 | 93,871  | 3.18%  |
| 南投縣埔里鎮 | 1,623 | 78,226  | 2.07%  |
| 花蓮縣吉安鄉 | 1,607 | 75,913  | 2.12%  |
| 新北市烏來區 | 1,546 | 3,463   | 44.64% |
| 雲林縣斗六市 | 1,305 | 102,818 | 1.27%  |
| 花蓮縣卓溪鄉 | 1,292 | 5,529   | 23.37% |
| 花蓮縣新城鄉 | 1,223 | 26,754  | 4.57%  |
| 桃園市平鎮區 | 1,504 | 220,665 | 0.68%  |
| 桃園市龍潭區 | 1,222 | 119,626 | 1.02%  |
| 桃園市中壢區 | 1,791 | 394,421 | 0.45%  |
| 桃園市龜山區 | 531   | 151,453 | 0.35%  |
| 花蓮縣花蓮市 | 902   | 95,888  | 0.94%  |
| 桃園市八德區 | 1,199 | 191,536 | 0.63%  |
| 桃園市桃園區 | 1,170 | 432,316 | 0.27%  |
| 新北市新店區 | 660   | 288,701 | 0.23%  |
| 宜蘭縣宜蘭市 | 577   | 90,293  | 0.64%  |
| 臺中市北屯區 | 569   | 214,117 | 0.27%  |
| 桃園市楊梅區 | 811   | 163,107 | 0.50%  |
| 桃園市蘆竹區 | 398   | 157,891 | 0.25%  |
| 桃園市大園區 | 215   | 86,609  | 0.25%  |
| 桃園市新屋區 | 70    | 48,697  | 0.14%  |
| 桃園市觀音區 | 324   | 65,390  | 0.50%  |

鄉鎮市區別泰雅族人口 (2000年)\[4\]

## 泰雅族名人

參見「[台灣原住民人物列表](../Page/台灣原住民人物列表.md "wikilink")」

### 歌手、演藝人員

[Vivian4comp.jpg](https://zh.wikipedia.org/wiki/File:Vivian4comp.jpg "fig:Vivian4comp.jpg")\]\]

  - [贊薇](../Page/贊薇.md "wikilink")：歌手，[新北市](../Page/新北市.md "wikilink")[烏來區泰雅族](../Page/烏來區.md "wikilink")1/2混血(母親[菲律賓人](../Page/菲律賓.md "wikilink"))
  - [林慶台](../Page/林慶台.md "wikilink")：本名：**Nolay Piho**。傳道師、獵人、木雕藝術家、演員
  - [游大慶](../Page/游大慶.md "wikilink")：演員，[賽德克·巴萊男主角](../Page/賽德克·巴萊.md "wikilink")，飾演青年[莫那魯道](../Page/莫那魯道.md "wikilink")，[宜蘭縣](../Page/宜蘭縣.md "wikilink")[南澳鄉澳花村](../Page/南澳鄉.md "wikilink")，貨櫃車司機、曾任[憲兵](../Page/憲兵.md "wikilink")
  - [丁小芹](../Page/丁小芹.md "wikilink"):歌手[宜蘭縣](../Page/宜蘭縣.md "wikilink")[南澳鄉澳花村](../Page/南澳鄉.md "wikilink")，泰雅族四分之一，美國混血
  - [伊凡諾幹](../Page/伊凡諾幹.md "wikilink")：第10屆[考試委員](../Page/考試院.md "wikilink")
  - [千百惠](../Page/千百惠.md "wikilink")：[歌手](../Page/歌手.md "wikilink")：[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉人](../Page/尖石鄉.md "wikilink")
  - [黃路梓茵](../Page/黃路梓茵.md "wikilink")：[臺中市](../Page/臺中市.md "wikilink")[和平區的泰雅族](../Page/和平區.md "wikilink")
  - [TITAN劉漢之](../Page/TITAN劉漢之.md "wikilink")：[新北市](../Page/新北市.md "wikilink")[烏來區的泰雅族](../Page/烏來區.md "wikilink")
  - [馬紹·阿紀](../Page/馬紹·阿紀.md "wikilink")：[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉](../Page/尖石鄉.md "wikilink")，[原民台台長](../Page/原住民族電視台.md "wikilink")、[公共電視主播](../Page/公共電視.md "wikilink")，[紀錄片](../Page/紀錄片.md "wikilink")《老師》導演
  - [陳盈潔](../Page/陳盈潔.md "wikilink")：台語女歌手：四分之一的泰雅族[新竹縣](../Page/新竹縣.md "wikilink")[關西鎮馬武督](../Page/關西鎮_\(台灣\).md "wikilink")
  - [楊林](../Page/楊林.md "wikilink")：女歌手，[宜蘭縣二分之一的泰雅族](../Page/宜蘭縣.md "wikilink")
  - [黃建福](../Page/黃建福.md "wikilink")（不浪‧尤幹）：歌手，[新北市](../Page/新北市.md "wikilink")[烏來區](../Page/烏來區.md "wikilink")
  - [羅美玲](../Page/羅美玲.md "wikilink")：歌手，本名：**Yokuy
    Utaw／尤桂·伍道**，[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉新樂村](../Page/尖石鄉.md "wikilink")
  - [張心傑](../Page/張心傑.md "wikilink")：歌手，[桃園市](../Page/桃園市.md "wikilink")[復興區](../Page/復興區_\(桃園市\).md "wikilink")
  - [雲力思](../Page/雲力思.md "wikilink")：歌手，本名**Inka
    Mbing／因卡·美明**，[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉新樂村](../Page/尖石鄉.md "wikilink")。
  - [張雨生](../Page/張雨生.md "wikilink")：歌手，泰雅族1/2混血，母親為台中市和平區泰雅族人，父親是來自浙江[嘉興的榮民](../Page/嘉興.md "wikilink")。
  - [徐若瑄](../Page/徐若瑄.md "wikilink")：女藝人、歌手，母親為[苗栗縣](../Page/苗栗縣.md "wikilink")[泰安鄉泰雅族人](../Page/泰安鄉.md "wikilink")
  - [周渝民](../Page/周渝民.md "wikilink")：男藝人、歌手、演員，四分之一的泰雅族血统；一說，是二分之一泰雅族血統
  - [言承旭](../Page/言承旭.md "wikilink")：歌手，泰雅族二分之一血統
  - [陳璽恩](../Page/陳璽恩.md "wikilink")：歌手，泰雅族1/2混血(母親[閩南](../Page/福建省.md "wikilink")[彰化人](../Page/彰化縣.md "wikilink"))
  - [吳亦帆](../Page/吳亦帆.md "wikilink")：歌手，本名：**吉娃斯·杜嵐**，苗栗泰安鄉泰雅族[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉家樂部落](../Page/尖石鄉.md "wikilink")
  - [曾宇辰](../Page/曾宇辰.md "wikilink")：歌手，[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉嘉樂部落](../Page/尖石鄉.md "wikilink")
  - [溫嵐](../Page/溫嵐.md "wikilink")：歌手，本名**Yungai
    Hayung／擁愛·哈永**，[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉馬里光](../Page/尖石鄉.md "wikilink")
  - [梁文音](../Page/梁文音.md "wikilink")：[宜蘭縣泰雅族與](../Page/宜蘭縣.md "wikilink")[魯凱族混血](../Page/魯凱族.md "wikilink")。
  - [曾之喬](../Page/曾之喬.md "wikilink")（喬喬）：歌手[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉嘉樂村](../Page/尖石鄉.md "wikilink")，泰雅族1/2混血，[Sweety成員之一](../Page/Sweety.md "wikilink")
  - [黃瀞怡](../Page/黃瀞怡.md "wikilink")（小薰）：演員、歌手，[桃園市](../Page/桃園市.md "wikilink")[復興區溪口](../Page/復興區_\(台灣\).md "wikilink")，[黑Girl成員之一](../Page/黑Girl.md "wikilink")
  - [依拜維吉](../Page/依拜維吉.md "wikilink")：歌手。本名：**Ipay
    Buyici**。[宜蘭縣](../Page/宜蘭縣.md "wikilink")[大同鄉](../Page/大同鄉_\(台灣\).md "wikilink")[四季村](../Page/四季村.md "wikilink")，第19屆[金曲獎最佳](../Page/金曲獎.md "wikilink")[原住民語歌手獎](../Page/原住民語.md "wikilink")
  - [甘友妹](../Page/甘友妹.md "wikilink")：[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉](../Page/尖石鄉.md "wikilink")[田埔部落](../Page/田埔部落.md "wikilink")[紋面國寶](../Page/紋面.md "wikilink")。98年1月14日過世。
  - [高金素梅](../Page/高金素梅.md "wikilink")：第五、六屆（山地原住民）立法委員，一半的泰雅族血统。
  - [吳与濱](../Page/吳与濱.md "wikilink")：專輯唱片製作人、作詞曲人、歌手、演員，宜蘭縣南澳鄉金岳，澳花人
  - [宋黃雙妹](../Page/宋黃雙妹.md "wikilink")：[新竹縣](../Page/新竹縣.md "wikilink")[五峰鄉](../Page/五峰鄉_\(台灣\).md "wikilink")[桃山村](../Page/桃山村.md "wikilink")，是新竹縣唯一碩果僅存的泰雅[紋面國寶](../Page/紋面.md "wikilink")。現年103歲。
  - [陳浩偉](../Page/陳浩偉.md "wikilink")：前[style成員](../Page/style.md "wikilink")，[超級星光大道選手](../Page/超級星光大道.md "wikilink")，母親為[新北市](../Page/新北市.md "wikilink")[烏來區人](../Page/烏來區.md "wikilink")。
  - [呂薔](../Page/呂薔.md "wikilink")：歌手，[新竹縣](../Page/新竹縣.md "wikilink")[五峰鄉泰雅族](../Page/五峰鄉.md "wikilink")。
  - [夏宇童](../Page/夏宇童.md "wikilink")：演員、歌手，[新竹縣](../Page/新竹縣.md "wikilink")[五峰鄉泰雅族](../Page/五峰鄉.md "wikilink")。
  - [傅珮慈](../Page/傅珮慈.md "wikilink")：演員，泰雅族。
  - [傅顯皓](../Page/傅顯皓.md "wikilink")：演員，泰雅族。
  - [傅顯濬](../Page/傅顯濬.md "wikilink")：演員，泰雅族。
  - [Iris](../Page/Iris.md "wikilink")：[flow成員](../Page/flow.md "wikilink")，舞者，歌手，[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")。
  - [Sam](../Page/Sam.md "wikilink")：[Dance
    flow成員](../Page/Dance_flow.md "wikilink")，舞者，歌手，[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")。
  - [武勇‧達印](../Page/武勇‧達印.md "wikilink")：[五峰鄉](../Page/五峰鄉_\(台灣\).md "wikilink")，賽夏族與泰雅族，歌手。
  - [胡靖杰](../Page/胡靖杰.md "wikilink")：原名胡鴻福，[桃園市](../Page/桃園市.md "wikilink")[復興區](../Page/復興區_\(台灣\).md "wikilink")，歌手。
  - [胡炫人](../Page/胡炫人.md "wikilink")：前\[X\]團體成員，母親為[新北市](../Page/新北市.md "wikilink")[烏來區人](../Page/烏來區.md "wikilink")。
  - [宋妍甄](../Page/宋妍甄.md "wikilink")：歌手，《[超級偶像](../Page/超級偶像.md "wikilink")》[第一屆校際爭霸戰踢館選手](../Page/超級偶像：第一屆校際爭霸戰.md "wikilink")，[新北市](../Page/新北市.md "wikilink")[烏來區](../Page/烏來區.md "wikilink")
  - [拉娃谷幸](../Page/拉娃谷幸.md "wikilink"): TVBS-NEWS假日新聞主播
  - KENJI03:
    歌手，日本[饒舌金屬樂團](../Page/饒舌金屬.md "wikilink")的主唱。父親是日本人，母親是[宜蘭縣](../Page/宜蘭縣.md "wikilink")[南澳鄉南澳村泰雅族人](../Page/南澳鄉.md "wikilink")。日本歌手[倖田來未的老公](../Page/倖田來未.md "wikilink")
  - [賴銘偉](../Page/賴銘偉.md "wikilink")（**Yuming**）：歌手，泰雅族1/2混血
  - [蘇婭](../Page/蘇婭.md "wikilink")：歌手。
  - [夏子傑](../Page/夏子傑.md "wikilink")：歌手、演員、監製、製片、編劇與導演。本名：**尤幹·阿路**，泰雅族與賽夏族，擁有日本、中國、台灣閩南血統，[新竹縣](../Page/新竹縣.md "wikilink")[五峰鄉](../Page/五峰鄉.md "wikilink")[大隘村](../Page/大隘村.md "wikilink")。
  - [潘君侖](../Page/潘君侖.md "wikilink")：歌手。[宜蘭縣](../Page/宜蘭縣.md "wikilink")[大同鄉泰雅族](../Page/大同鄉.md "wikilink")，[GTM成員之一](../Page/GTM.md "wikilink")

### 藝文界

  - [雲力思](../Page/雲力思.md "wikilink")：原名：因卡美明。作家、歌手、教師。生於新竹尖石鄉部落。
  - [游霸士‧撓給赫](../Page/游霸士‧撓給赫.md "wikilink")：漢名：田敏忠。作家。生於澤敖利北勢八社薩衣亞天狗部落(苗栗縣泰安鄉梅園村)
  - [瓦歷斯·諾幹](../Page/瓦歷斯·諾幹.md "wikilink")：作家。本名：**Walis
    Nokan**，台中市和平區雙崎部落泰雅族人
  - [里慕伊‧阿紀](../Page/里慕伊‧阿紀.md "wikilink")：漢名：曾修媚。作家。新竹縣尖石鄉嘉樂村嘉樂排部落
  - [啟明‧拉瓦](../Page/啟明‧拉瓦.md "wikilink")：漢名：趙啟明。作家，任職於國立自然科學博物館，文學創作之外，還從事人類學考古研究
  - [娃利斯‧羅干](../Page/娃利斯‧羅干.md "wikilink")：漢名：王捷茹。作家，擔任牧師
  - [得木‧阿漾](../Page/得木‧阿漾.md "wikilink")：漢名：李永松。作家。國文教師
  - [麗依京‧尤瑪](../Page/麗依京‧尤瑪.md "wikilink")：作家。
  - [馬紹‧阿紀](../Page/馬紹‧阿紀.md "wikilink")：作家。
  - [多馬斯](../Page/多馬斯.md "wikilink")：作家。
  - [悠蘭‧多又](../Page/悠蘭‧多又.md "wikilink")：作家。
  - [尤幹‧阿路](../Page/尤幹‧阿路.md "wikilink")：漢名：[夏子傑](../Page/夏子傑.md "wikilink")。設計師、策展人、藝術家、藝術經紀人。新竹縣五峰鄉大隘村。
  - [安力‧給怒](../Page/安力‧給怒.md "wikilink")：漢名：[賴安淋](../Page/賴安淋.md "wikilink")。藝術家、牧師。生於新竹尖石鄉部落。

### 政治

  - [樂信·瓦旦](../Page/樂信·瓦旦.md "wikilink")：白色恐怖受難者
  - [葉榮光](../Page/葉榮光.md "wikilink")（**Male Badu／馬賴·巴都**）: 白色恐怖受難者
  - [田富達](../Page/田富達.md "wikilink")（**Yumi
    Badu／尤明·巴都**）：前[中華人民共和國](../Page/中華人民共和國.md "wikilink")[全國人大代表](../Page/台灣省全國人民大會代表.md "wikilink")，為中國政治輩分最高的原住民
  - [黃榮泉](../Page/黃榮泉.md "wikilink")（**Masa
    Tohui**）：文史工作學家，2007年薪傳獎原住民得主；泰雅族議會議長
  - [高金素梅](../Page/高金素梅.md "wikilink")（**吉娃斯·阿麗**）：立法委員，泰雅族1/2混血（父親為[外省族群](../Page/外省族群.md "wikilink")）

### 軍事

  - [徐衍璞](../Page/徐衍璞.md "wikilink")：陸軍中將，為原住民中首位晉升至國軍中將階級者，歷任[陸軍第六軍團指揮官](../Page/陸軍第六軍團指揮部.md "wikilink")、[國防部參謀本部人事參謀次長等職](../Page/國防部參謀本部.md "wikilink")。泰雅族1/2混血（母親為[宜蘭](../Page/宜蘭.md "wikilink")[南澳鄉出身的泰雅族人](../Page/南澳鄉.md "wikilink")，父親為[山東移民](../Page/山東人.md "wikilink")）\[5\]。

### 學術界

  - 笠征（[日语](../Page/日语.md "wikilink")：Ryu
    Masao，[中文](../Page/中文.md "wikilink")：劉三富，1943-
    、台中[梨山](../Page/梨山.md "wikilink")[薩拉矛社](../Page/薩拉矛社.md "wikilink")、[泰雅語名字](../Page/臺灣原住民族命名文化.md "wikilink")：Masao
    Umao，笠征其父親名字：Umao
    Sadan）：淡江文理學院中文系、[東勢中學擔任一年教員](../Page/臺中市立東勢國民中學.md "wikilink")、後1986年獲得九州大學文學博士。日本[福岡大學人文學部東亞細亞地域言語學科](../Page/福岡大學.md "wikilink")
    教授；專長中國文學。

### 體育界

  - [林克謙](../Page/林克謙.md "wikilink")：台灣職業棒球選手。

## 相關條目

[泰雅生活館.jpg](https://zh.wikipedia.org/wiki/File:泰雅生活館.jpg "fig:泰雅生活館.jpg")[大同鄉泰雅生活館](../Page/大同鄉_\(臺灣\).md "wikilink")\]\]

  - 泰雅文化：泰雅族、[泰雅語群](../Page/泰雅語群.md "wikilink")、[泰雅語](../Page/泰雅語.md "wikilink")、[泰雅語語法](../Page/泰雅語語法.md "wikilink")、[泰雅語音系](../Page/泰雅語音系.md "wikilink")、[泰雅族口簧琴](../Page/泰雅族口簧琴.md "wikilink")。
  - 歷史事件：[薩拉矛事件](../Page/薩拉矛事件.md "wikilink")、[霧社事件](../Page/霧社事件.md "wikilink")、[大豹社事件](../Page/大豹社事件.md "wikilink")\[6\]\[7\]、[莫那魯道](../Page/莫那魯道.md "wikilink")、[高砂義勇隊](../Page/高砂義勇隊.md "wikilink")

<!-- end list -->

  - 其他：[亞越絲伯列](../Page/亞越絲伯列.md "wikilink")(Yajut
    Blyah/中野Yajut/1885.12.10-1932)

## 注釋

1.  關於對「泰雅族」這個族群之內部分類比較詳細的討論，可以參考楊盛涂。
2.  這裡對「賽德克亞族」的分類係根據王田明。比較通俗的分法似乎是將這個亞族簡單分成「西賽德克群」和「東賽德克群」（見順益台灣原住民博物館；行政院原住民族委員會文化園區管理局。

## 参考文獻

## 參考書目

  - 大英百科全書，2003，泰雅族，見大英百科全書線上繁體中文版 \[online\]。np：Encyclopadia
    Britannica,
    Inc.。\[引用於2004年11月1日\]。全球資訊網網址：[3](http://wordpedia.eb.com/tbol/article?i=004906)。
  - Ethnologue: Languages of the World. 2005. Atayal: A Language of
    Taiwan \[online\]. Dallas, Tex.: Ethnologue: Languages of the World,
    \[cited 12 February 2005\]. Available from World Wide Web:
    [4](http://www.ethnologue.com/show_language.asp?code=TAY).
  - 順益台灣原住民博物館，nd，泰雅族，見原住民博覽
    \[online\]。台北：順益台灣原住民博物館。\[引用於2004年10月14日\]。全球資訊網網址：[5](https://web.archive.org/web/20041010052544/http://www.museum.org.tw/view02.html)。
  - 王田明，nd，東賽德克族群：馬里巴西社會組織探討
    \[online\]。台東：萬榮鄉代表會。\[引用於2004年10月27日\]。全球資訊網網址：[6](http://www.hlwr119.com.tw/%B8U%BAa/%AB%A5%AA%BA%ACG%B6m%B3%CC%AC%FC/page_2.htm)。
  - 楊碧川，1997，台灣歷史詞典。台北：前衛。
  - 楊盛涂，nd，賽德克亞族（Sejiq）族群的淵源
    \[online\]。花蓮：太魯閣國家公園管理處。\[引用於2004年10月27日\]。全球資訊網網址：[7](http://www.taroko.gov.tw/naturestudy/action/truku/data/01.doc)。
  - 鄉土文化教學與鄉土文化研究，nd，泰雅族
    \[online\]。台南：國立台南大學鄉土文化教學與鄉土文化研究。\[引用於2004年10月27日\]。全球資訊網網址：[8](http://ici.ntntc.edu.tw/%A4E%B1%DA%BA%F4%AD%B6/%AE%F5%B6%AE%B1%DA/%AE%F5%B6%AE%B1%DA.htm)。
  - 行政院，2004，游院長宣佈太魯閣族成為原住民族第十二族 強調以族群平等觀念取代族群融合 \[online\]。台北：行政院。1月14日
    \[引用於2004年10月27日\]。全球資訊網網址：[9](http://www.ey.gov.tw/web92/news/Wc1e86d14d65fa.htm)。
  - 行政院原住民族委員會文化園區管理局，nd， 泰雅族 \[online\]。台北：行政院原住民族委員會文化園區管理局。\[引用於
    2004年10月27日\]。全球資訊網網址：[10](https://web.archive.org/web/20041023191909/http://www.tacp.gov.tw/intro/NINE/atayal/atayal1.htm)。
  - 陳茂泰，1973，泰雅族經濟變遷與調適的研究 平靜與望洋的例子。國立臺灣大學考古人類學研究所碩士論文。
  - 陳茂泰，2001，台北縣烏來鄉泰雅族耆老口述歷史。台北縣板橋市：台北縣政府文化局。
  - 達西烏拉彎·畢馬（田哲益），2002，臺灣的原住民 泰雅族。台北：台原。
  - 李亦園、徐人仁、宋龍生、吳燕和，1963，南澳的泰雅人。中央研究院民族學研究所專刊之五。
  - 廖守臣，1984，泰雅族的文化 部落遷徙與拓展。台北：世界新專觀光宣導科。
  - 廖守臣，1998，泰雅族社會組織。花蓮：私立慈濟醫學暨人文社會學院。
  - 馬騰嶽著作攝影，1998，泰雅族文面圖譜。台北：攝影天地雜誌社。
  - 馬騰嶽，2003，分裂的民族與破碎的臉：「泰雅族」民族認同的建構與分裂。國立清華大學人類學研究所碩士論文。
  - 吳佰祿、馬騰嶽、阮昌銳、李子寧，1999，文面、馘首、泰雅文化:泰雅族文面文化展專輯。台北：國立台灣博物館。
  - 孫大川總策劃，2016，台灣原住民的神話與傳說10泰雅族：彩虹橋的審判。台北：新自然主義。

## 外部連結

  - [祖靈之邦-泰雅族](http://www.abohome.org.tw/index.php?option=com_content&view=article&id=3275:tinycontent-100&catid=77:11)
  - [泰雅專輯-走出殖民地](http://www.abohome.org.tw/index.php?option=com_content&view=article&id=3401:2009-02-13-07-59-30&catid=87:music)
  - [行政院原住民族委員會文化園區管理局：泰雅族](https://web.archive.org/web/20150922201835/http://www.tacp.gov.tw/home02_3.aspx?ID=$3041&IDK=2&EXEC=L)
  - [賽德克文史工作室](http://www.i-hualien.com.tw/zoneHualien/h2_content.asp?p=716)
  - [Atayal（英文網站）](http://www.atayal.org/)
  - [中央研究院南島語數位典藏](https://web.archive.org/web/20070115071850/http://formosan.sinica.edu.tw/)
  - [不老部落](http://www.bulaubulau.com)
  - [臺灣原住民歷史語言文化大辭典網路版](http://citing.hohayan.net.tw/default.asp)
  - 導演魏德聖新作-[《賽德克·巴萊》電影官方部落格-台灣
    繁體版](http://www.wretch.cc/blog/seediq1930)
  - 導演魏德聖新作-[《賽德克·巴萊》電影官方部落格-大陸 簡體版](http://blog.sina.com.cn/weibale)
  - [泰雅語老師
    日人碑懷](http://www.libertytimes.com.tw/2004/new/mar/4/today-so11.htm)
  - \[<http://blog.xuite.net/min351205/twblog/132958331-中野.亞越絲.伯列（Yajut+Blyah）是泰雅爾女性先驅教育家>。
    中野.亞越絲.伯列（Yajut Blyah）是泰雅爾女性先驅教育家。\]

<!-- end list -->

  - [國立臺灣歷史博物館
    斯土斯民-臺灣的故事：紋面工具](http://the.nmth.gov.tw/nmth/zh-TW/Item/Detail/e87830af-84b8-4957-bfab-9942c5095d5d)

[Ata](../Category/臺灣原住民族.md "wikilink")
[泰雅族](../Category/泰雅族.md "wikilink")

1.  〈[游院長宣佈太魯閣族成為原住民族第十二族
    強調以族群平等觀念取代族群融合](http://publish.gio.gov.tw/newsc/newsc/930114/93011405.html)〉，2004年1月14日

2.  〈[「還我族名」
    賽德克族連署正名](http://www.libertytimes.com.tw/2007/new/jan/13/today-center4.htm)〉，2007年1月13日

3.

4.
5.

6.  賽德克．巴萊效應,"屠村殺26婦孺「莫那魯道不是英雄」-泰雅耆老控賽德克族勾結日本"[1](http://tw.nextmedia.com/applenews/article/art_id/33662442/IssueID/20110912),[蘋果日報](../Page/蘋果日報.md "wikilink"),2011年
    09月12日.

7.  影劇中心／綜合報導,"屠殺26婦孺　泰雅耆老控「莫那魯道不是英雄」"[2](http://www.nownews.com/2011/09/12/11464-2741849.htm),[NOWnews](../Page/NOWnews.md "wikilink"),2011/09/12
    10:36.