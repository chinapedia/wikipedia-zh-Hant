[thumb遷台之前的台灣南島語言分布圖](../Page/文件:Formosan_languages.png.md "wikilink")(按
Blust,
1999)\[1\].東台灣"[蘭嶼島](../Page/蘭嶼.md "wikilink")(深紅色)表示為使用[馬來-玻里尼西亞語族](../Page/馬來-玻里尼西亞語族.md "wikilink")[巴丹語群](../Page/巴丹語群.md "wikilink")[達悟語的區域](../Page/達悟語.md "wikilink").\]\]

**泰雅語**（[泰雅文](../Page/原住民族語羅馬字.md "wikilink")：、），亦稱**泰雅爾語**，是[南島語系的一種](../Page/南島語系.md "wikilink")，用[拉丁文字書寫](../Page/拉丁文字.md "wikilink")，為[台灣](../Page/台灣.md "wikilink")[泰雅族的民族語言](../Page/泰雅族.md "wikilink")。分布地域極廣，一般學者將其與[賽德克語一併歸於](../Page/賽德克語.md "wikilink")[泰雅語群](../Page/泰雅語群.md "wikilink")（）。目前臺灣原住民泰雅語認證考試分為6類方言語群：[賽考利克泰雅語](../Page/賽考利克泰雅語.md "wikilink")、[澤敖利泰雅語](../Page/澤敖利泰雅語.md "wikilink")、[汶水泰雅語](../Page/汶水泰雅語.md "wikilink")、[萬大泰雅語](../Page/萬大泰雅語.md "wikilink")、[四季泰雅語](../Page/四季泰雅語.md "wikilink")、[宜蘭澤敖利泰雅語](../Page/宜蘭澤敖利泰雅語.md "wikilink")。\[2\]\[3\]\[4\]

## 語音分區

泰雅語分布極廣，狹義泰雅語的使用人口從[新北市](../Page/新北市.md "wikilink")[烏來區到中部](../Page/烏來區.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")、台中和平區皆有。大致分為"賽考利克泰雅語群()"，及"澤敖利泰雅語群()"兩大語群。

  - 賽考利克泰雅語群()

<!-- end list -->

  - [賽考利克泰雅語](../Page/賽考利克泰雅語.md "wikilink")（Squliq
    Atayal）：分布最廣，區域廣達新北市烏來區，以[桃園市](../Page/桃園市.md "wikilink")[復興區為主的](../Page/復興區_\(桃園市\).md "wikilink")[北橫山區](../Page/北部橫貫公路.md "wikilink")，[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉山區](../Page/尖石鄉.md "wikilink")，部分南投縣仁愛鄉等。\[5\]
  - [四季泰雅語](../Page/四季泰雅語.md "wikilink")（Skikun
    Atayal）：宜蘭縣[四季村](../Page/四季村.md "wikilink")。

<!-- end list -->

  - 澤敖利泰雅語群()

<!-- end list -->

  - [澤敖利泰雅語](../Page/澤敖利泰雅語.md "wikilink")（Ci'uli
    Atayal）：新竹縣尖石鄉[梅花村](../Page/梅花村.md "wikilink")。
  - [宜蘭澤敖利泰雅語](../Page/寒溪語.md "wikilink")（ts'ole'[寒溪泰雅語](../Page/寒溪語.md "wikilink")）：宜蘭[大同鄉與](../Page/大同鄉_\(臺灣\).md "wikilink")[南澳鄉](../Page/南澳鄉.md "wikilink")。
  - [汶水泰雅語](../Page/汶水泰雅語.md "wikilink")（Mayrinax Atayal,
    itaal）：新竹縣[五峰鄉](../Page/五峰鄉_\(台灣\).md "wikilink")，[苗栗縣](../Page/苗栗縣.md "wikilink")[泰安](../Page/泰安鄉.md "wikilink")，[宜蘭縣山區](../Page/宜蘭縣.md "wikilink")。
  - [萬大泰雅語](../Page/萬大泰雅語.md "wikilink")（Plngawan Atayal/Bandai Atayal,
    itaral）：宜蘭[大同](../Page/大同鄉_\(台灣\).md "wikilink")，南投縣仁愛鄉[親愛部落](../Page/親愛部落.md "wikilink")。

因為"泰雅語言"分布極廣，且加入[日語](../Page/日語.md "wikilink")、[賽德克語以及](../Page/賽德克語.md "wikilink")[太魯閣語等語種之混入影響](../Page/德路固語.md "wikilink")，伴隨時間演變，故逐漸形成不同地區泰雅族[部落所使用之泰雅語歧異程度極高的現象](../Page/部落.md "wikilink")。就泰雅語群的使用人口上而言，在2012年6月該語群使用人口約為83,088人。\[6\]

## 語音系統

泰雅語的音系之[音位大都使用適當的](../Page/音位.md "wikilink")[Unicode符號來標示](../Page/Unicode.md "wikilink")。在[台灣南島語的](../Page/台灣南島語.md "wikilink")[書寫系統的訂定上](../Page/書寫系統.md "wikilink")，元輔音之音系表原則上都是先「[發音部位](../Page/發音部位.md "wikilink")」(橫列)、後「[發音方法](../Page/發音方法.md "wikilink")」(縱列)、再考量「[清濁音](../Page/清濁音.md "wikilink")」，來訂定其音系之音位架構。\[7\]

## 泰雅語語法

在語法的分類上[台灣南島語並不同於一般的](../Page/台灣南島語.md "wikilink")[分析語或其它](../Page/分析語.md "wikilink")[综合语裡的動詞](../Page/综合语.md "wikilink")、名詞、形容詞、介詞和副詞等之基本[詞類分類](../Page/詞類.md "wikilink")。比如台灣南島語裡普遍沒有副詞，而副詞的概念一般以動詞方式呈現、可稱之為「副動詞」，類之於[俄语裡的副動詞](../Page/俄语.md "wikilink")。\[8\]
泰雅語語法分類是將基礎的語法之詞類、詞綴、字詞結構及分類法，對比[分析語等之詞類分類法加以條析判別](../Page/分析語.md "wikilink")。\[9\]
而在語詞的表達上以"澤敖利泰雅語群"作為參考基準；以[賽考利克泰雅語群作為一般通行參考](../Page/賽考利克泰雅語.md "wikilink")。

## 例句

  - Ima lalu-su? 你叫什麼名字？
  - Lokah-su ga? 你好嗎？
  - Mhway-su balay. 非常感謝你。
  - Ptngi-ta klama, lokah hi tala. 早餐吃飽了，才會有氣力。
  - K'nhriq kmmu qu ryax. 要愛惜光陰。

## 註釋

## 參考文獻

  - 葉郁婷(Maya Yuting Yeh)、黃宣範(Shuanfan Huang),"泰雅語之 Hya’
    與立場標記"[7](http://www.ling.sinica.edu.tw/files/publication/j2013_1_04_2362.pdf),《語言暨語言學》期刊,第十四卷第一期
    2013(1).
  - Sing 'Olan, Syat Yupas,"Sinqunan Biru Ke na
    Tayal（泰雅爾族語詞彙集）"，台北：使徒出版有限公司，2006年11月。<small>ISBN
    978-986-7134-08-01</small>
  - 李壬癸，「泰雅語羣不同年齡在語言形式上的差異」[8](http://www.hss.nthu.edu.tw/~thj/catalogue_detail.php?id=455)，清華大學學報/第14卷第2期：167-191，1982-12。

## 參見

  - [原住民族語能力認證](../Page/原住民族語能力認證.md "wikilink")
  - [原住民族語言書寫系統 (中華民國)](../Page/原住民族語言書寫系統_\(中華民國\).md "wikilink")
  - [族語新聞](../Page/族語新聞.md "wikilink")
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")
  - [原始人類語言](../Page/原始人類語言.md "wikilink")
  - [巴義慈神父](../Page/巴義慈.md "wikilink")

## 外部連結

  - [南投縣本土教育資訊網](https://web.archive.org/web/20120817023533/http://local.raych.net/)
  - [易家樂（台灣原住民歷史語言文化大辭典）](http://134.208.27.115/citing_content.asp?id=2435&keyword=易家樂)
  - [台灣南島語言的奧秘(影片)](http://www.youmaker.com/video/sv?id=cc0c9909af5e4c6d8758ef0def64ead0001)
  - [公共電視 知識的饗宴-第十九集
    台灣南島語言的奧秘](http://web.pts.org.tw/~web01/knowledge/19.htm)
  - [ABVD: Atayal (Mayrinax)/Paul Jen-kuei
    Li（李壬癸）](http://language.psy.auckland.ac.nz/austronesian/language.php?id=742)

[category:泰雅語](../Page/category:泰雅語.md "wikilink")

[Category:謂主賓語序語言](../Category/謂主賓語序語言.md "wikilink")
[Category:謂賓主語序語言](../Category/謂賓主語序語言.md "wikilink")

1.  Blust, R. (1999). "Subgrouping, circularity and extinction: some
    issues in Austronesian comparative linguistics" in E. Zeitoun &
    P.J.K Li (Ed.) Selected papers from the Eighth International
    Conference on Austronesian Linguistics (pp. 31-94). Taipei: Academia
    Sinica.

2.  國立臺灣師範大學進修推廣學院,"98年度原住民族語言能力認證考試",臺北市,2009.

3.  李壬癸 院士,"珍惜台灣南島語",台灣本鋪：前衛出版社,臺北市,2010年1月. ISBN 978-957-801-635-4

4.  鄭光博,"第一章誰是泰雅族"[1](http://nccur.lib.nccu.edu.tw/bitstream/140.119/34564/6/25900306.pdf),2005.

5.  楊明峰/桃園報導,"國小母語鐘聲
    傳承泰雅古調"[2](http://news.chinatimes.com/society/11050301/112012090800165.html),中國時報,2012-09-08/01:38.

6.
7.  行政院原住民族委員會,"原住民族語言書寫系統"[3](http://www.edu.tw/files/list/M0001/aboriginal.pdf),台語字第0940163297號,原民教字第09400355912號公告,中華民國94年12月15日.

8.  張永利,"台灣南島語言語法：語言類型與理論的啟示(Kavalan)"[4](http://vietnam.nsc.gov.tw/public/Data/012714292271.pdf),語言學門熱門前瞻研究,2010年12月/12卷1期,pp.112-127.

9.  Barbara B.H. Partee, A.G. ter Meulen, R. Wall,"Mathematical Methods
    in Linguistics (Studies in Linguistics and
    Philosophy)(語言研究的數學方法)"[5](http://acl.ldc.upenn.edu/J/J92/J92-1009.pdf)[6](http://www.amazon.com/Mathematical-Methods-Linguistics-Studies-Philosophy/dp/9027722455),Springer,1/e
    1993 edition(April 30, 1990).