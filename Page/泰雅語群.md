**泰雅語群**為[南島語系中的語言](../Page/南島語系.md "wikilink")，由[台灣原住民使用的語言](../Page/台灣原住民.md "wikilink")，分布極廣。泰雅語群目前被歸類於[北台灣南島語族項目之下](../Page/北台灣南島語族.md "wikilink")，學者將此語群又分為[泰雅語](../Page/泰雅語.md "wikilink")、[賽德克語](../Page/賽德克語.md "wikilink")。\[1\]\[2\]

## 起源與分類

泰雅語群首先分為泰雅語與賽德克語兩個分支，兩個語言間有六條音變的差異，[賽德克語大致保存了古泰雅語的濁塞音](../Page/賽德克語.md "wikilink")\[b,
d, g\]，而泰雅語則已變成擦音。\[3\]

在泰雅語系中，又分支出[賽考利克泰雅語與](../Page/賽考利克泰雅語.md "wikilink")[寒溪語等](../Page/寒溪語.md "wikilink")。此種分類與官方分法或原住民語調均有出入，其原因在於因為分布廣，諸如[日本語混入的變種因素過多](../Page/日本語.md "wikilink")，因語種內語言語法之混雜現象，故目前將[寒溪語歸類在泰雅語群之下](../Page/寒溪語.md "wikilink")。

賽德克語分成「德固達雅語」(Seediq Tgdaya/tg)、「都達語」(Sediq Toda/to)與「德路固語」(Seejiq
Truku/tr)三種次語群。[太魯閣語屬於德路固語分支](../Page/太魯閣語.md "wikilink")。

泰雅語族最紛歧的區域集中在南投縣仁愛鄉，語言學家[李壬癸推測](../Page/李壬癸.md "wikilink")，這個語群最早就是由仁愛鄉開始分化與擴散。這與泰雅族傳說，部落起源於仁愛鄉發祥村（古稱瑞岩）相符。

## 分佈

狹義泰雅語的使用人口從[新北市](../Page/新北市.md "wikilink")[烏來區到中部](../Page/烏來區.md "wikilink")[南投縣皆有](../Page/南投縣.md "wikilink")，也大致分成四種型態方言，而賽德克語的主要使用區域則位於台灣中部[南投縣](../Page/南投縣.md "wikilink")[霧社](../Page/霧社.md "wikilink")、東部[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[花蓮縣一帶](../Page/花蓮縣.md "wikilink")。就泰雅語群的使用人口上而言，1993年該語群使用人口約為63,000人。\[4\]

## 註釋

## 參考文獻

  - Li, Paul Jen-kuei. 1981. "Reconstruction of Proto-Atayalic
    Phonology." In Li, Paul Jen-kuei. 2004. Selected Papers on Formosan
    Languages. Taipei, Taiwan: Institute of Linguistics, Academia
    Sinica.

[Category:泰雅語言](../Category/泰雅語言.md "wikilink")

1.  國立台灣師範大學進修推廣學院,"98年度原住民族語言能力認證考試",臺北市,2009.
2.  李壬癸 院士,"珍惜台灣南島語",台灣本鋪：前衛出版社,臺北市,2010年1月. ISBN 978-957-801-635-4
3.  李壬癸(Paul Jen-kuei Li),"臺灣南島語言的語音符號(Orthographic Systems for Formosan
    Languages)",教育部教育研究委員會(Ministry of Education
    ROC),臺北市(Taipei),中華民國八十年五月(May
    1991),pp.10-11/14-16.
4.  (1993 Johnstone) *Ethnologue: Languages of the World 2005* -
    *[Atayal: A Language of
    Taiwan](http://www.ethnologue.com/14/show_language.asp?code=TAY)*，引用於2005年2月12日