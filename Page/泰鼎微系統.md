**泰鼎微系統**（Trident Microsystems,
Inc.），簡稱「泰鼎」或「Trident」，是一家供應平面螢幕顯示器（包括[液晶顯示器](../Page/液晶顯示器.md "wikilink")、[電漿顯示器](../Page/電漿顯示器.md "wikilink")、[HDTV等等](../Page/HDTV.md "wikilink")..）之影像處理器（Video
Processor）晶片的公司，過去也曾是提供[個人電腦](../Page/個人電腦.md "wikilink")[顯示卡晶片與](../Page/顯示卡.md "wikilink")[音效卡晶片的廠商](../Page/音效卡.md "wikilink")。

## 歷史

[Trident_TVGA9000.jpg](https://zh.wikipedia.org/wiki/File:Trident_TVGA9000.jpg "fig:Trident_TVGA9000.jpg")
[替代=](https://zh.wikipedia.org/wiki/File:Lifetec_LT9303_-_Motherboard_-_Trident_Cyber9525DVD-1137.jpg "fig:替代=")
泰鼎創立於1987年，當時在製造、銷售廉價但效能普通的[SVGA顯示晶片上頗有聲譽](../Page/SVGA.md "wikilink")，許多[OEM顯示卡製造廠商都採用泰鼎的晶片](../Page/OEM.md "wikilink")。在[個人電腦顯示卡市場從單純頁框緩衝顯示](../Page/個人電腦.md "wikilink")（基本VGA彩色顯示輸出）移轉至更高等的硬體加速顯示（多重解析度，SVGA輸出，勿與[3D硬體加速混淆](../Page/3D.md "wikilink")）時，泰鼎繼續延襲他們銷售價格低廉與但性能普通顯示卡晶片的商業策略。在1990年代中期，泰鼎在晶片效能短暫追上他的主要對手─[S3
Graphics](../Page/S3_Graphics.md "wikilink")，推出了效能可與S3
Trio64V+媲擬的TGUI-9680晶片，但Trio64V+還是在[True
Color模式輸出效能上勝過TGUI](../Page/真彩色.md "wikilink")-9680。

在顯示卡進入3D硬體加速時代，許多顯示晶片廠商包括泰鼎，都疏忽沒有跟上時機。直到1990年代晚期，終於推出了一顆具競爭性的3D繪圖加速晶片－TGUI9880
（Blade 3D）。在此之前，泰鼎已從有[ATI](../Page/ATI.md "wikilink")、[S3
Graphics](../Page/S3_Graphics.md "wikilink")、[SiS等廠商壅塞其中競爭的低階OEM市場上撤退](../Page/SiS.md "wikilink")。

於同時的[膝上型電腦市場](../Page/膝上型電腦.md "wikilink")，泰鼎是最早使用嵌入型記憶體與[半導體製程技術](../Page/半導體器件製造.md "wikilink")，來封裝結合繪圖控制器與顯示記憶體於[SoC單晶片上的先驅](../Page/系統單晶片.md "wikilink")，這個複合單晶片方案解決了顯示記憶體顆粒佔用膝上型電腦主機板珍貴有限空間的問題。

雖然泰鼎享受了3DImage與Blade
3D產品線的一些成功，但自從[英特爾大舉侵入個人電腦底端繪圖晶片市場後](../Page/英特爾.md "wikilink")，泰鼎轉而與主機板系統晶片廠商[威盛](../Page/威盛.md "wikilink")、[揚智等合作](../Page/揚智.md "wikilink")，將繪圖晶片整合於個人電腦系統晶片上，如ALi
Cyber-ALADDiN、VIA
PLE133等產品，但市場成功不大。面對萎縮的市場佔有率及攀升的研發經費（主要由於3D填圖技術複雜性的增加），泰鼎在2003年6月宣布公司將於年底進行重大改組，將從前的顯示繪圖晶片部門完全售與[圖誠科技](../Page/圖誠科技.md "wikilink")，從此轉往平面顯示影像處理器發展。

泰鼎於 2012年1月4日 宣佈，泰鼎微系統公司已申請破產保護，並表示，已任命視頻應用晶片製造商Entropic Communications
Inc(ENTR)為非公開拍賣的買家 (stalking horse
bid)，意味着其他公司若想參與競拍Trident資産，其出價必須高於Entropic。Entropic將以5500萬美元收購Trident的機頂盒業務、專利及其他智慧財產權，並將承擔該公司的部分債務。

## 泰鼎舊產品線

### 繪圖顯示晶片

以下列表尚未完整，泰鼎已不再從事個人電腦繪圖顯示晶片的研發與銷售。

桌上型電腦繪圖顯示晶片

  - 8800 （1988年） - first S/VGA compatible chipset （ISA）, 512KB
    framebuffer
  - 8900 - high-color （65,536） display-mode support, 1MB framebuffer
  - 9000 - first integrated （VGA+[RAMDAC](../Page/RAMDAC.md "wikilink")）
    VGA chipset
      - 9000B （1992年）
      - 9000i-1 （1994年） - appeared on Trident's VC512TM
        [ISA](../Page/Industry_Standard_Architecture.md "wikilink")
        video cards
  - 92xx, 94xx - first Windows accelerators
  - 9440 （1994年） - first performance competitive Windows 2D-accelerator
    （2MB PCI/VLB）
  - 9660 - similar to 9440, 64-bit datapath
  - 9680, 9682, 9685 - motion video accelerator （zoom + YUV-\>RGB,
    Directdraw overlay）
  - 3DImage9750, 3DImage9850 - first Windows 3D-accelerators （4MB
    PCI/AGP）
  - Blade3D （1999年） - first performance competitive Windows
    3D-accelerators （8MB PCI/AGP）
  - Blade XP
  - XP4 - DirectX 8 chip.
  - XP4E - AGP8x support.
  - XP8 （cancelled） - DirectX 9 chip, marketed for under $100US.
  - XP10 （cancelled） - PCI Express controller.

膝上型電腦繪圖顯示晶片

  - Cyber9320
  - Cyber9382
  - Cyber9385
  - Cyber9397 and Cyber9397DVD
  - 9525DVD
  - CyberBlade
      - CyberBlade Ai1
      - CyberBlade e4-128
      - CyberBlade i1
      - CyberBlade i7
  - Blade XP
  - XP4
      - XP4m16/XP4m32 - embedded memory.
  - XP8 （cancelled） - DirectX 9 chip.

系統內建整合型繪圖顯示晶片

  - ALi CyberALADDiN-T （）
  - ALi CyberALADDiN-P4 （CyberBLADE XP2）
  - ? （codename Napa2T）
  - ? （codename Napa2-P4）
  - ? （codename Napa2-Banias）

### 音效晶片

泰鼎已不再從事個人電腦音效晶片的研發與銷售。

  - Trident 4DWAVE-DX/NX, based on the T2 platform which is also used by
    [SIS](../Page/Silicon_Integrated_Systems.md "wikilink") and
    [ALi](../Page/Acer_Laboratories_Incorporated.md "wikilink") for
    their own audio interfaces. Supports
    [Q3D](../Page/QSound.md "wikilink") 2.0.

## 泰鼎現今產品

現今泰鼎產品以平面螢幕顯示器（包括[液晶顯示器](../Page/液晶顯示器.md "wikilink")、[電漿顯示器](../Page/電漿顯示器.md "wikilink")、[HDTV等等](../Page/HDTV.md "wikilink")..）之影像處理器（Video
Processor）晶片為主，主要客戶有[新力](../Page/新力.md "wikilink")、[東芝](../Page/東芝.md "wikilink")、[夏普](../Page/夏普.md "wikilink")、[三星](../Page/三星電子.md "wikilink")、[飛利浦](../Page/飛利浦.md "wikilink")、[JVC等知名廠商](../Page/JVC.md "wikilink")，其中泰鼎與新力有較密切的合作關係。

SVP<sup>TM</sup> 影像處理器系列

  - SVP<sup>TM</sup>－PX
  - SVP<sup>TM</sup>－CX
  - SVP<sup>TM</sup>－LX

HiDTV<sup>TM</sup> 數位電視單晶片影像處理器系列

  - HiDTV<sup>TM</sup>－EX
  - HiDTV<sup>TM</sup>－LX
  - HiDTV<sup>TM</sup>Pro－PX
  - HiDTV<sup>TM</sup>Pro－EX

類比終端訊號處理器（Trident Analog Front End Signal Processor）

  - TAFE II

TDM<sup>TM</sup>數位電視訊號解調器（Demodulator）系列

  - TDM5510
  - TDM5530
  - TDM5550/2
  - TDM5560/70
  - TDM5562/72

## 外部連結

  - [泰鼎微系統](http://www.tridentmicro.com/)

[Category:顯示設備硬件公司](../Category/顯示設備硬件公司.md "wikilink")
[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[T](../Category/聖塔克拉拉公司.md "wikilink")
[T](../Category/1987年成立的公司.md "wikilink")