[Sai_Wan_Swimming_Shed_201508.jpg](https://zh.wikipedia.org/wiki/File:Sai_Wan_Swimming_Shed_201508.jpg "fig:Sai_Wan_Swimming_Shed_201508.jpg")，建於1960至70年代，為攝影發燒友必到之地。\]\]
**泳棚**是為方便[游泳人士而設的設施](../Page/游泳.md "wikilink")，通常建於[海邊](../Page/海.md "wikilink")。泳棚一般為以[竹搭成的棚屋](../Page/竹.md "wikilink")，提供男女[更衣室](../Page/更衣室.md "wikilink")、[浴室及](../Page/浴室.md "wikilink")[儲物櫃](../Page/儲物櫃.md "wikilink")。

泳棚在[20世紀早期的香港甚為流行](../Page/香港20世紀初歷史.md "wikilink")，當時的泳棚收費廉宜，每逢[假日及公餘時間都吸引不少](../Page/假日.md "wikilink")[香港市民使用](../Page/香港市民.md "wikilink")。海面上建有小[橋](../Page/橋.md "wikilink")，方便泳客下水。部份泳棚附設[食肆及](../Page/食肆.md "wikilink")[溜冰等康樂設施以吸引其他類型的顧客](../Page/溜冰.md "wikilink")。由於當年[香港社會的經濟不富裕](../Page/香港社會.md "wikilink")，泳棚提供[泳衣租用服務](../Page/泳衣.md "wikilink")，女裝泳衣租金3毫[港幣](../Page/港幣.md "wikilink")，男裝泳褲的租金則較為便宜。

## 歷史

1911年，香港中華遊樂會於[北角](../Page/北角.md "wikilink")[七姊妹海邊設置泳棚](../Page/七姊妹_\(香港\).md "wikilink")，成為當時香港游泳勝地，每年吸引的泳客達10萬人次。

1950年代，港九新界合共有8至10個泳棚，其中較著名的有位於[香港島](../Page/香港島.md "wikilink")[西環](../Page/西環.md "wikilink")（[堅尼地城及](../Page/堅尼地城.md "wikilink")[摩星嶺一帶](../Page/摩星嶺.md "wikilink")）的鐘聲泳棚和金銀泳棚，[南華體育會於](../Page/南華體育會.md "wikilink")1950年至1972年期間於[阿公岩設立泳棚](../Page/阿公岩.md "wikilink")。[九龍](../Page/九龍.md "wikilink")[荔枝角灣也設有很多泳棚](../Page/荔枝角灣.md "wikilink")，亦提供小[艇出租服務](../Page/艇.md "wikilink")。其他設有泳棚的地區有[南區的](../Page/南區_\(香港\).md "wikilink")[中灣等](../Page/中灣.md "wikilink")。

1960年代，[香港政府收回部份地區的泳棚土地](../Page/香港政府.md "wikilink")，重新發展，而香港政府亦在香港各區興建了多座公眾[泳池](../Page/泳池.md "wikilink")，加上海港污染漸趨嚴重，令到泳棚逐漸被拆卸而式微。1988年，泳廬團長尹洪輝及其友人向香港政府申請，並且獲得批准在西環[域多利道重開泳棚](../Page/域多利道.md "wikilink")，令此成為香港市區現存唯一泳棚。泳廬不受到[康樂及文化事務署監管](../Page/康樂及文化事務署.md "wikilink")，但是需要定期與香港政府續約，並且向香港政府繳交[差餉及建築費用](../Page/差餉.md "wikilink")，泳廬設有[辦公室](../Page/辦公室.md "wikilink")、男女浴室及儲物櫃等設施，每日清晨5時至下午1時開放，月費為120港元。

2012年10月22日，[行政長官](../Page/香港特別行政區行政長官.md "wikilink")[梁振英於](../Page/梁振英.md "wikilink")[網誌上撰文](../Page/網誌.md "wikilink")，指出隨着大型污水排放工程完成，[維多利亞港的水質逐年好轉](../Page/維多利亞港.md "wikilink")，他認為可以考慮於維多利亞港兩岸興建泳棚，方便大家，特別是於[中環和](../Page/中環.md "wikilink")[尖沙嘴的工作人士在維多利亞港暢泳或者在岸邊](../Page/尖沙嘴.md "wikilink")[垂釣](../Page/垂釣.md "wikilink")，紓緩工作壓力。但是[香港公開大學科技學院院長](../Page/香港公開大學.md "wikilink")[何建宗指出](../Page/何建宗.md "wikilink")，維多利亞港的水質至今不適宜長時間游泳，至少有待《[淨化海港計劃](../Page/淨化海港計劃.md "wikilink")》第二期於2014年完成，維多利亞港的水質才有希望可以改善至適合[游泳的水平](../Page/游泳.md "wikilink")。他指現時每日仍然有不少[香港市民在](../Page/香港市民.md "wikilink")[北角及](../Page/北角.md "wikilink")[紅磡等岸邊游泳](../Page/紅磡.md "wikilink")，而且該些地方都沒有任何配套設施，他認為可以考慮於維多利亞港兩岸的[海濱長廊重新闢設小型泳棚](../Page/海濱長廊.md "wikilink")\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

  - [50年代興盛 平民消暑熱點](http://hk.news.yahoo.com/070926/12/2gelz.html) 明報
  - [兩七旬婦早泳溺斃 一泳棚對開遇急流
    一深水灣出事](http://hk.news.yahoo.com/070926/12/2gelx.html)
    明報
  - [一小時內兩婦晨泳溺斃](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070927&sec_id=4104&subsec_id=11867&art_id=10208328)
    蘋果日報

[Category:香港歷史](../Category/香港歷史.md "wikilink")
[Category:香港運動場地](../Category/香港運動場地.md "wikilink")
[Category:游泳](../Category/游泳.md "wikilink")

1.  [特首倡維港建泳棚
    專家﹕水質至少要等兩年後](https://archive.is/20130105193729/hk.news.yahoo.com/%E7%89%B9%E9%A6%96%E5%80%A1%E7%B6%AD%E6%B8%AF%E5%BB%BA%E6%B3%B3%E6%A3%9A-%E5%B0%88%E5%AE%B6-%E6%B0%B4%E8%B3%AA%E8%87%B3%E5%B0%91%E8%A6%81%E7%AD%89%E5%85%A9%E5%B9%B4%E5%BE%8C-211157993.html)
    《明報》 2012年10月23日