**泽泻科**是一类[水生植物](../Page/水生植物.md "wikilink")，包括有12个[属大约](../Page/属.md "wikilink")85-95[种](../Page/种.md "wikilink")，广泛分布在全世界各地，在北半球[温带地区的种类最多](../Page/温带.md "wikilink")，绝大部分为[草本](../Page/草本.md "wikilink")[植物](../Page/植物.md "wikilink")，一般生长在[沼泽或水塘中](../Page/沼泽.md "wikilink")。[中国有](../Page/中国.md "wikilink")5属约13种，全国均有分布。

## 形态

泽泻科植物大部分为多年生植物，也有部分种类为一年生植物，如果在永久有[水的环境中就可能是多年生](../Page/水.md "wikilink")，但在只有季节性有水的环境就可能是一年生，当然也有例外情况。这科植物的[茎一般为匍匐状](../Page/茎.md "wikilink")，没入水下的[叶细线状](../Page/叶.md "wikilink")，但伸出水面的叶变宽，有的成为箭头状，基生，叶柄具鞘。

[花序总状或圆锥状](../Page/花.md "wikilink")，[花被二轮](../Page/花被.md "wikilink")，外轮3片[花萼状](../Page/花萼.md "wikilink")，内轮3片[花瓣状](../Page/花瓣.md "wikilink")。雄蕊6至多数，少数为3，[瘦果](../Page/瘦果.md "wikilink")，[种子没有](../Page/种子.md "wikilink")[胚乳](../Page/胚乳.md "wikilink")，胚为马蹄形弯曲。

## 分类

根據[被子植物APG
III分类法](../Page/被子植物APG_III分类法.md "wikilink")，現在的泽泻科包含17個現存的屬和2個化石屬：\[1\]\[2\]\[3\]

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Albidella.md" title="wikilink">Albidella</a></em> <small>Pichon</small></li>
<li><em><a href="../Page/Alisma.md" title="wikilink">Alisma</a></em> <small><a href="../Page/Carl_Linnaeus.md" title="wikilink">L.</a> </small></li>
<li><em><a href="../Page/Alismaticarpum.md" title="wikilink">Alismaticarpum</a></em> † <small>Collinson</small></li>
<li><em><a href="../Page/Astonia.md" title="wikilink">Astonia</a></em> <small>S.W.L.Jacobs </small></li>
<li><em><a href="../Page/Baldellia.md" title="wikilink">Baldellia</a></em> <small><a href="../Page/Filippo_Parlatore.md" title="wikilink">Parl.</a> </small></li>
<li><em><a href="../Page/Burnatia.md" title="wikilink">Burnatia</a></em> <small><a href="../Page/Marc_Micheli.md" title="wikilink">Micheli</a> </small></li>
<li><em><a href="../Page/Butomopsis.md" title="wikilink">Butomopsis</a></em> <small><a href="../Page/Kunth.md" title="wikilink">Kunth</a></small></li>
<li><em><a href="../Page/Caldesia.md" title="wikilink">Caldesia</a></em> <small>Parl. </small></li>
<li><em><a href="../Page/Damasonium.md" title="wikilink">Damasonium</a></em> <small><a href="../Page/Philip_Miller.md" title="wikilink">Mill.</a> </small></li>
<li><em><a href="../Page/Echinodorus.md" title="wikilink">Echinodorus</a></em> <small><a href="../Page/Louis_Claude_Richard.md" title="wikilink">Rich.</a> ex <a href="../Page/George_Engelmann.md" title="wikilink">Engelm.</a> </small></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Helanthium.md" title="wikilink">Helanthium</a></em> <small>(Benth. &amp; Hook. f.) Engelm. ex J.G. Sm.</small></li>
<li><em><a href="../Page/Hydrocleys.md" title="wikilink">Hydrocleys</a></em> <small><a href="../Page/Rich..md" title="wikilink">Rich.</a></small></li>
<li><em><a href="../Page/Limnocharis.md" title="wikilink">Limnocharis</a></em> <small><a href="../Page/Bonpl..md" title="wikilink">Bonpl.</a></small></li>
<li><em><a href="../Page/Limnophyton.md" title="wikilink">Limnophyton</a></em> <small><a href="../Page/Friedrich_Anton_Wilhelm_Miquel.md" title="wikilink">Miq.</a> </small></li>
<li><em><a href="../Page/Luronium.md" title="wikilink">Luronium</a></em> <small><a href="../Page/Constantine_Samuel_Rafinesque-Schmaltz.md" title="wikilink">Raf.</a> </small></li>
<li><em><a href="../Page/Ranalisma.md" title="wikilink">Ranalisma</a></em> <small><a href="../Page/Otto_Stapf.md" title="wikilink">Stapf</a> </small></li>
<li><em><a href="../Page/Sagisma.md" title="wikilink">Sagisma</a></em> † <small>Nikitin</small></li>
<li><em><a href="../Page/Sagittaria.md" title="wikilink">Sagittaria</a></em> <small>L. </small></li>
<li><em><a href="../Page/Wiesneria.md" title="wikilink">Wiesneria</a></em> <small>Micheli </small></li>
</ul></td>
</tr>
</tbody>
</table>

### 种植和利用

有的种类例如[慈姑属](../Page/慈姑属.md "wikilink")，其[根状茎可以食用](../Page/根.md "wikilink")，[美洲](../Page/美洲.md "wikilink")[印第安人也将它们作为食物](../Page/印第安人.md "wikilink")。有的种可以作为观赏植物和[水族箱中的](../Page/水族箱.md "wikilink")[水草](../Page/水草.md "wikilink")。

## 参考文献

## 外部链接

  - [泽泻科](http://delta-intkey.com/angio/www/alismata.htm)

[\*](../Category/泽泻科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:水生植物](../Category/水生植物.md "wikilink")

1.  [Alismataceae](http://delta-intkey.com/angio/www/alismata.htm) in
    [L. Watson and M.J. Dallwitz (1992 onwards). The families of
    flowering plants.](http://delta-intkey.com/angio/)
2.
3.  [Kew World Checklist of Selected Plant
    Families](http://apps.kew.org/wcsp/qsearch.do;jsessionid=16C963FB3B760714D96041A5493D2712)