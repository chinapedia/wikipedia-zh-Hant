**洋金花**（[學名](../Page/學名.md "wikilink")：**）屬[茄科植物](../Page/茄科.md "wikilink")，是[中醫學常用的草藥](../Page/中醫.md "wikilink")，但具有毒性，不能過量服用。洋金花多數生長於村邊、路旁、荒地，在濕潤向陽的土地較為常見。此花有可能原產自東[印度](../Page/印度.md "wikilink")\[1\]或[美洲](../Page/美洲.md "wikilink")，在[北美洲視為](../Page/北美洲.md "wikilink")[觀賞植物](../Page/觀賞植物.md "wikilink")，而香港則稱之為「[香港四大毒草](../Page/香港.md "wikilink")」之一。\[2\]

## 形態特徵

洋金花為[一年生直立](../Page/一年生植物.md "wikilink")[草本](../Page/草本.md "wikilink")，約高0.5-1.5米，全株近乎無毛，莖呈深紫色。

葉互生，在莖上部呈假對生，葉柄長2-6厘米，葉身成卵形或寬卵形，長5-20厘米，寬4-l5厘米，葉身基部不對稱，頂端漸尖，側[脈每邊約](../Page/葉脈.md "wikilink")4-6條；葉邊全緣、微波狀、或具不規則短齒。

花直立而單生，一般生長於葉腋或枝叉間，花梗長約1厘米，花萼圓筒形，長4-9厘米，直徑2厘米；花冠呈漏斗形，有白色、淡黃色、淡紫色之分，長14-20厘米，檐部直徑6-10厘米，野生類種為單[瓣](../Page/花瓣.md "wikilink")，培植的種類則有二重瓣或三重瓣，裂片有短尖，短尖下有3條縱脈紋。

雄蕊有5枚（培植的種類可達15枚），花絲貼生於花冠筒內，長1-1.2厘米，而雌蕊則有1枚，子房疏生短剌毛，花柱11-16厘米，頭呈棒狀。蒴果近球形，直徑約3cm，外殼刺疏而短，不規則4瓣裂。種子近腎形，顏色淡褐，直徑3毫米。花期及果期為3月至12月。\[3\]

### 與曼陀羅花的分別

洋金花（Datura metel L）與[曼陀羅花](../Page/曼陀羅花.md "wikilink")（Datura
stramonium）是[生物分類法中的同一](../Page/生物分類法.md "wikilink")[屬](../Page/属_\(生物\).md "wikilink")，外形亦十分相似。不過，曼陀羅花的蒴果是規則4瓣裂，花萼有5棱，洋金花蒴果則為不規則4瓣裂，花萼沒有棱。另外曼陀羅花的花冠亦比較細小。\[4\]

## 醫藥用途及其毒性

洋金花全株皆含莨菪烷類[生物鹼](../Page/生物鹼.md "wikilink")，包括[東莨菪鹼](../Page/東莨菪鹼.md "wikilink")（hyoscine）、[莨菪鹼](../Page/莨菪鹼.md "wikilink")（hyoscyamine）、[阿托品](../Page/阿托品.md "wikilink")（Atropine）這些物質有麻醉、止痛、鬆弛肌肉等作用。經製煉的洋金花的花可用作麻醉，只需口服3-5[克](../Page/克.md "wikilink")，5分鐘就會發作，效力可長達5-6小時。如果服用過多，就會出現幻覺、中毒等病徵，嚴重者會致命。由於使用稍微過量便可致毒，故必須慎用。
從[中醫角度](../Page/中醫.md "wikilink")，多數以花入藥，果、葉、根亦可，常於夏秋採集，洗淨曬乾備用。洋金花味苦、辛，性溫，有大毒，主治麻醉鎮痛、平喘止咳。而在古時[印度](../Page/印度.md "wikilink")，因為洋金花含有麻醉作用，能減輕症狀，所以會廣泛用作治療[癲癇症](../Page/癲癇症.md "wikilink")、精神病、[心臟病](../Page/心臟病.md "wikilink")，甚至[腹瀉等病症](../Page/腹瀉.md "wikilink")。而在[越南有人將洋金花的干花和葉子卷成](../Page/越南.md "wikilink")[香煙吸食](../Page/香煙.md "wikilink")。\[5\]

## 分佈

野生的洋金花常見於[熱帶和](../Page/熱帶.md "wikilink")[亞熱帶地區](../Page/亞熱帶.md "wikilink")，而[溫帶地區則以人工栽種為主](../Page/溫帶.md "wikilink")。在中華地區，[台灣](../Page/台灣.md "wikilink")、[香港](../Page/香港.md "wikilink")、[福建](../Page/福建.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[貴州等地都常見野生洋金花](../Page/貴州.md "wikilink")，而江蘇、浙江，甚至北方城市都有人工培植。

## 別名

  - 中文：白花曼陀羅、曼陀羅花、廣東鬧洋花、風茄花、大喇叭花
  - 英文：Hindu Datura、Devil's Trumpet、Metel、Downy Thorn-Apple

## 图片展示

<File:A> berry of Datura metel.JPG <File:DaturaMetel-plant.jpg>
<File:Devil's> Trumpet, Horn of Plenty, Downy Thorn Apple (Datura
metel).jpg <File:Datura> metel Blanco1.35.jpg <File:Gc12_Datura.jpg>|
*Datura Metel L.* 或 *Matura fastuosa L.* <File:Datura> metel Prague 2017
2.jpg <File:Datura> metel Prague 2017 3.jpg

## 參考

主要來源：

  -
## 外部連結

  - [洋金花
    Yangjinhua](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01133)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [洋金花
    Yangjinhua](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00315)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [洋金花 Yang Jin
    Hua](http://libproject.hkbu.edu.hk/was40/outline?page=1&channelid=44273&searchword=%E6%B4%8B%E9%87%91%E8%8A%B1&sortfield=+name_chi_sort)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [L-天仙子胺
    L-hyoscyamine](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00325)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[ml:കറുത്തുമ്മം](../Page/ml:കറുത്തുമ്മം.md "wikilink")

[Category:曼陀羅屬](../Category/曼陀羅屬.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:一年生植物](../Category/一年生植物.md "wikilink")
[Category:草本植物](../Category/草本植物.md "wikilink")
[Category:觀賞植物](../Category/觀賞植物.md "wikilink")
[Category:美洲植物](../Category/美洲植物.md "wikilink")
[Category:印度植物](../Category/印度植物.md "wikilink")
[Category:台灣植物](../Category/台灣植物.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:中国植物](../Category/中国植物.md "wikilink")

1.  來源：
2.  來源：
3.  來源：
4.  來源：
5.  來源：