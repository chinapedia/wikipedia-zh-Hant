[Maazel_08.jpg](https://zh.wikipedia.org/wiki/File:Maazel_08.jpg "fig:Maazel_08.jpg")

**洛林·马泽尔**（；\[1\]），[指挥家](../Page/指挥家.md "wikilink")，小提琴家，[作曲家](../Page/作曲家.md "wikilink")。以其敏锐，有时有些极端的演奏而闻名\[2\]。

## 生平

马泽尔出生在[法国](../Page/法国.md "wikilink")，父母是美国人，他在四岁时随父母移居[美国](../Page/美国.md "wikilink")，五岁时开始学习[小提琴和](../Page/小提琴.md "wikilink")[钢琴](../Page/钢琴.md "wikilink")\[3\]，成长在美国。八岁时第一次公开指挥一支大学交响乐团\[4\]，并于15岁时第一次小提琴演出。后来他在[匹兹堡大学学习](../Page/匹兹堡大学.md "wikilink")。

## 指挥生涯

1965年-1971年，他是[柏林德意志歌劇院的指挥](../Page/柏林德意志歌劇院.md "wikilink")，1965年-1975年，柏林广播交响乐团（Berlin
Radio Symphony
Orchestra）；1972年-1982年，[克利夫兰管弦乐团](../Page/克利夫兰管弦乐团.md "wikilink")（Cleveland
Orchestra）；1982年-1984年，[维也纳国家歌剧院](../Page/维也纳国家歌剧院.md "wikilink")（Vienna
State Opera）；1988年-1996年，匹兹堡交响乐团（Pittsburgh Symphony
Orchestra）；1993年-2002年，巴伐利亚广播交响乐团（Bavarian Radio Symphony
Orchestra）；2002年，他接任了Kurt
Masur作为[纽约爱乐乐团](../Page/纽约爱乐乐团.md "wikilink")（New
York Philharmonic Orchestra）音乐总监的职务。

马泽尔曾经在1980至1986年、1994年、1996年、1999年和2005年指挥过[维也纳新年音乐会](../Page/维也纳新年音乐会.md "wikilink")。他的指挥风格清新、潇洒，稳重之中不乏幽默。有时还会担任乐队的小提琴独奏部分。他使用的是于1723年制作的斯特拉底瓦里琴\[5\]。

2008年2月24日，率領紐約愛樂在[北韓的](../Page/北韓.md "wikilink")[東平壤大劇院舉行音樂會](../Page/東平壤大劇院.md "wikilink")，為北韓建國後首次有美國樂團在北韓演奏，並第一次在北韓演奏[美國國歌](../Page/美國國歌.md "wikilink")。

2014年7月13日於弗吉尼亞州卡斯爾頓自己的農場莊園因[肺炎併發症逝世](../Page/肺炎.md "wikilink")，享年84歲\[6\]。

## 外部链接

  - Maestro Maazel's Website: [Maestro
    Maazel](http://www.maestromaazel.com)

## 引文参考

<references/>

[Category:20世紀指揮家](../Category/20世紀指揮家.md "wikilink")
[Category:21世紀指揮家](../Category/21世紀指揮家.md "wikilink")
[Category:美国指挥家](../Category/美国指挥家.md "wikilink")
[Category:美國作曲家](../Category/美國作曲家.md "wikilink")
[Category:美國小提琴家](../Category/美國小提琴家.md "wikilink")
[Category:儿童古典音乐家](../Category/儿童古典音乐家.md "wikilink")

1.

2.

3.  [央视国际对洛林·马泽尔的介绍](http://www.cctv.com/world/special/C13146/20041122/101367.shtml)

4.
5.
6.