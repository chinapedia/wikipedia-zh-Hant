**洛桑·尼玛**（；），汉名（原名）**杨虹**，[四川省](../Page/四川省.md "wikilink")[康定县人](../Page/康定县.md "wikilink")，父親是[藏族](../Page/藏族.md "wikilink")，母親是[漢族](../Page/漢族.md "wikilink")，[相声演员](../Page/相声.md "wikilink")。

## 生平

洛桑13岁考入[中央民族学院音乐舞蹈系](../Page/中央民族学院.md "wikilink")，1986年7月毕业並考入[中华全国总工会文工团歌舞团任舞蹈演员](../Page/中华全国总工会文工团.md "wikilink")。后来相声演员（尹）[博林发现洛桑有极强的模仿力和](../Page/博林.md "wikilink")[口技功夫](../Page/口技.md "wikilink")，经[中央电视台](../Page/中央电视台.md "wikilink")《[曲苑杂坛](../Page/曲苑杂坛.md "wikilink")》编导人员包装于1993年合作表演《洛桑学艺》一炮走红，逐渐成为相声界的特色演员。1995年10月2日演出后，由于酒后驾车（[神龙富康](../Page/神龙富康.md "wikilink")）出车祸罹难，年仅27岁。2004年，洛桑的父亲杨仁清和母亲张瑞宇将洛桑的骨灰从康定迁葬于[青城山味江陵园名人园](../Page/青城山.md "wikilink")\[1\]\[2\]。

## 注释

<references/>

## 参考文献

  - [博林访谈](https://web.archive.org/web/20070126175837/http://ent.tom.com/1661/200493-95637.html)
  - [央视国际 相关报道](http://www.cctv.com/program/qyzt/20050530/100498.shtml)

[Category:康定县人](../Category/康定县人.md "wikilink")
[Category:中央民族大学校友](../Category/中央民族大学校友.md "wikilink")
[Category:中国藏族演员](../Category/中国藏族演员.md "wikilink")
[Category:相声演员](../Category/相声演员.md "wikilink")
[Category:中華人民共和國車禍身亡者](../Category/中華人民共和國車禍身亡者.md "wikilink")
[Category:葬于四川](../Category/葬于四川.md "wikilink")
[H](../Category/杨姓.md "wikilink")

1.
2.