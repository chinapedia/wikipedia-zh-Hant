**洛迪高·安達利斯·加華奧**（**Rodrigo Andreis
Galvão**，），生於[巴西](../Page/巴西.md "wikilink")[聖利奧波爾杜](../Page/聖利奧波爾杜.md "wikilink")，[巴西足球運動員](../Page/巴西.md "wikilink")，司職[前鋒](../Page/前鋒_\(足球\).md "wikilink")，2007年來港效力[東方並協助球隊贏得](../Page/東方足球隊.md "wikilink")[香港高級銀牌冠軍](../Page/香港高級組銀牌.md "wikilink")。

## 球員生涯

洛迪高生於[巴西](../Page/巴西.md "wikilink")[聖利奧波爾杜](../Page/聖利奧波爾杜.md "wikilink")，出身於[巴西著名球會](../Page/巴西.md "wikilink")[高士路](../Page/高士路.md "wikilink")，其後效力過[聖荷塞](../Page/:en:São_José.md "wikilink")、[FC馬高](../Page/:en:F.C._Marco.md "wikilink")、[卡歷斯馬](../Page/:en:Criciúma_Esporte_Clube.md "wikilink")、[烏爾巴](../Page/:en:Sport_Club_Ulbra.md "wikilink")、[瓜拉迪古塔等球會](../Page/:en:Guaratinguetá_Futebol.md "wikilink")。2007年夏天，加盟獲[香港足球總會邀請重返](../Page/香港足球總會.md "wikilink")[甲組聯賽角逐的老牌球會](../Page/香港甲組足球聯賽.md "wikilink")[東方](../Page/東方足球隊.md "wikilink")，迅速成為球隊攻擊重心。2007年12月23日在[香港大球場舉行的](../Page/香港大球場.md "wikilink")[高級組銀牌決賽](../Page/香港足球高級銀牌賽.md "wikilink")，個人梅開二度，協助球隊以3－1擊敗[傑志](../Page/傑志體育會.md "wikilink")\[1\]，奪得重返甲組後首項錦標，洛迪高亦以6個入球成為賽事最佳射手\[2\]。2008年11月，因右大腿筋嚴重拉傷，影響甚至包括腹部肌肉，經專家診斷認為難以繼續球員生涯，與[東方解約](../Page/東方足球隊.md "wikilink")，返回巴西\[3\]。

## 個人榮譽

  - [香港足球明星選舉](../Page/香港足球明星選舉.md "wikilink") 最佳十一人（2007–08季度）
  - [香港足球高級銀牌賽](../Page/香港足球高級銀牌賽.md "wikilink") 最佳射手（2007–08季度）

## 參考資料

## 外部連結

  - [香港足球總會球員註冊資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=7279)

[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:高士路球員](../Category/高士路球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:香港巴西籍足球運動員](../Category/香港巴西籍足球運動員.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")
[Category:巴西足球運動員](../Category/巴西足球運動員.md "wikilink")
[Category:香港外籍足球運動員](../Category/香港外籍足球運動員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")

1.  [2007-08銀牌決賽球賽報告](http://www.hkfa.com/zh-hk/match_result.php?match_date=2007-12-23&match_id=3268)
2.  [2007/08年度彩豐行高級銀牌賽](http://www.hkfa.com/zh-hk/match_cups.php?league_id=9&ly=2007-2008)
3.  [洛迪高右腿傷重返巴西就醫](http://www.takungpao.com.hk/news/08/11/02/GW-984241.htm)[大公報](../Page/大公報.md "wikilink")