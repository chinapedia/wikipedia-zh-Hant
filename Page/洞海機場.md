**洞海機場**
（）是位于[越南](../Page/越南.md "wikilink")[廣平省](../Page/廣平省.md "wikilink")[洞海市的一座機場](../Page/洞海市.md "wikilink")。机场从[2004年9月开始建造](../Page/2004年9月.md "wikilink")，[2008年5月完工](../Page/2008年5月.md "wikilink")。机场跑道長2400米，宽45米，投資约130萬[美元](../Page/美元.md "wikilink")，由[越南机场总公司出资兴建](../Page/越南机场总公司.md "wikilink")。完成後，[越南航空將開通](../Page/越南航空.md "wikilink")[新山一國際機場和](../Page/新山一國際機場.md "wikilink")[內排國際機場到此機場航線](../Page/內排國際機場.md "wikilink")。洞海機場完成後，可服務參觀[世界遺產](../Page/世界遺產.md "wikilink")[風牙者榜國家公園的旅游者](../Page/風牙者榜國家公園.md "wikilink")。

## 本機場歷史

法屬殖民時期，法軍建築军用洞海機場（土跑道）。他們使用本機場來攻擊[越南獨立同盟會](../Page/越南獨立同盟會.md "wikilink")
和[老撾共产力量](../Page/老挝人民军.md "wikilink")。[越南戰爭時期](../Page/越南戰爭.md "wikilink")，越南共產黨使用此機場來進攻[越南共和國](../Page/越南共和國.md "wikilink")。1975年後，此機場被荒廢。

机场最大能力；300乘客每高峰小時，相当
500,000乘客每年。機場于2008年5月18日正式启用。越南總理已經批准機場開展規劃，該機場將扩建以服務远程飞机。
[DHAirport12.jpg](https://zh.wikipedia.org/wiki/File:DHAirport12.jpg "fig:DHAirport12.jpg")

### 扩大项目

[Dong_Hoi_Airport_Expansion_Project.jpg.png](https://zh.wikipedia.org/wiki/File:Dong_Hoi_Airport_Expansion_Project.jpg.png "fig:Dong_Hoi_Airport_Expansion_Project.jpg.png")
越南交通部长和越南总理[阮春福与](../Page/阮春福.md "wikilink")2018年四月批准对此机场的扩大项目：

  - 实现时间：2018年底到2020年底。
  - 投资项目：延长跑道3600米、建筑两座航站楼（国内和国际大楼），最大能力十百万乘客每年。各辅助工程。
  - 投资者：越南不动产暨旅行和航空FLC私人集团。这家集团也是[竹航空公司](../Page/竹航空公司.md "wikilink")(Bamboo
    Airways)的母公司。

## 航運和路線

[DHAirport10.jpg](https://zh.wikipedia.org/wiki/File:DHAirport10.jpg "fig:DHAirport10.jpg")

## 参考

<references/>

[Category:廣平省](../Category/廣平省.md "wikilink")
[Category:越南機場](../Category/越南機場.md "wikilink")
[Category:2008年完工建築物](../Category/2008年完工建築物.md "wikilink")