**津巴布韦國家足球隊**是由[津巴布韋足球協會組織而成](../Page/津巴布韋足球協會.md "wikilink")，綽號''武士(The
Warriors) ''，津巴布韋國家足球隊於1980年之前稱為
[羅德西亞國家足球隊](../Page/羅德西亞國家足球隊.md "wikilink")，津巴布韋從沒有晉身過[世界杯決賽周](../Page/世界杯.md "wikilink")，直至2006年，津巴布韋只參與過2004年和[2006年的](../Page/2006年非洲國家盃.md "wikilink")[非洲國家盃](../Page/非洲國家盃.md "wikilink")。2015年3月，因積欠巴西籍前總教練Jose
Claudinei
Georgini薪資，遭到[國際足球總會禁賽](../Page/國際足球總會.md "wikilink")，因而無法參加[2018年世界盃足球賽會外賽](../Page/2018年世界盃足球賽.md "wikilink")\[1\]。

津巴布韋的球衣贊助商是[LEGEA](../Page/LEGEA.md "wikilink")。

## 世界杯成績

  - 至 *沒有參與*（仍然名為[羅德西亞](../Page/羅德西亞.md "wikilink")）

  - *外圍賽*（仍然名為[羅德西亞](../Page/羅德西亞.md "wikilink")）

  - 至 *沒有參與*（仍然名為[羅德西亞](../Page/羅德西亞.md "wikilink")）

  - 至 *外圍賽*

## 非洲國家杯成績

  - 1957年至1980年 *沒有參與*（仍然名為[羅德西亞](../Page/羅德西亞.md "wikilink")）
  - 1982年至2002年 *外圍賽*
  - 2004年 **分組賽**
  - 2006年 **分組賽出局**
  - 2008年至2010年 *外圍賽*

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽第二輪G組](../Page/2014年世界盃外圍賽非洲區第二輪#G組.md "wikilink")

## 註釋

[Category:非洲足球代表隊](../Category/非洲足球代表隊.md "wikilink")
[Category:津巴布韋足球](../Category/津巴布韋足球.md "wikilink")

1.  [Zimbabwe expelled from the preliminary competition of the 2018 FIFA
    World Cup Russia -
    FIFA.com](http://www.fifa.com/worldcup/news/y=2015/m=3/news=zimbabwe-expelled-from-the-preliminary-competition-of-the-2018-fifa-wo-2557911.html)