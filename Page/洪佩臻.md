**洪佩臻**（）為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[味全龍隊](../Page/味全龍.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。

## 經歷

  - [嘉義縣朴子國小少棒隊](../Page/嘉義縣.md "wikilink")
  - [屏東縣美和中學青少棒隊](../Page/屏東縣.md "wikilink")
  - [屏東縣美和中學青棒隊](../Page/屏東縣.md "wikilink")
  - 陸光棒球隊
  - [俊國建設棒球隊](../Page/俊國建設.md "wikilink")（業餘甲組）
  - [味全龍隊練習生](../Page/味全龍.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[味全龍隊](../Page/味全龍.md "wikilink")
  - 台灣之星棒球隊（業餘乙組）
  - 養工處棒球隊
  - 新北市淡水區淡水慢速壘球隊(淡水聯盟)

## 職棒生涯成績

| 年度    | 球隊                               | 出賽 | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振 | 責失 | 投球局數 | 防禦率  |
| ----- | -------------------------------- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | ---- | ---- |
| 1996年 | [味全龍](../Page/味全龍.md "wikilink") | 23 | 2  | 0  | 0  | 0  | 0  | 0  | 28 | 21 | 33 | 47.1 | 6.28 |
| 1997年 | [味全龍](../Page/味全龍.md "wikilink") | 15 | 0  | 0  | 0  | 0  | 0  | 0  | 9  | 18 | 26 | 30.2 | 7.63 |
| 合計    | 2年                               | 38 | 2  | 0  | 0  | 0  | 0  | 0  | 37 | 39 | 56 | 78.0 | 6.46 |

## 特殊事蹟

## 外部連結

  -
  -
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:味全龍隊球員](../Category/味全龍隊球員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:俊國建設棒球隊](../Category/俊國建設棒球隊.md "wikilink")
[Category:美和中學青少棒隊](../Category/美和中學青少棒隊.md "wikilink")
[Category:朴子人](../Category/朴子人.md "wikilink")
[Pei佩](../Category/洪姓.md "wikilink")