**洪吉童**（），又作**洪吉同**，是[朝鮮王朝](../Page/朝鮮王朝.md "wikilink")[燕山君在位期間的一個盜賊](../Page/燕山君.md "wikilink")。[本貫](../Page/本貫.md "wikilink")[南陽洪氏](../Page/南陽洪氏.md "wikilink")，是[洪尚植的](../Page/洪尚植.md "wikilink")[孽子](../Page/庶出子女.md "wikilink")（即「庶子」），有兩名嫡出兄弟[洪逸童](../Page/洪逸童.md "wikilink")、[洪貴童](../Page/洪貴童.md "wikilink")，[母親是](../Page/母親.md "wikilink")[妓生出身的文氏](../Page/妓生.md "wikilink")，妓名[玉英香](../Page/玉英香.md "wikilink")。洪吉童在《[萬姓大同譜](../Page/萬姓大同譜.md "wikilink")》中被稱為「精於道術者」。哥哥洪逸童的女兒是[朝鮮成宗的後宮](../Page/朝鮮成宗.md "wikilink")[淑儀洪氏](../Page/淑儀洪氏_\(朝鮮成宗\).md "wikilink")。

在[李瀷的](../Page/李瀷.md "wikilink")《[星湖僿說](../Page/星湖僿說.md "wikilink")》中，將洪吉童與[張吉山](../Page/張吉山.md "wikilink")、[林巪正並稱為朝鮮三大義賊](../Page/林巪正.md "wikilink")。

## 生平

依照[朝鮮王朝的](../Page/朝鮮王朝.md "wikilink")[從母法](../Page/從母法.md "wikilink")，子女需繼承母親的社會階層，洪吉童為屬於[賤民的妓生所生之子](../Page/賤民.md "wikilink")，亦是賤民階級，因此不能參加大科[科舉考試](../Page/科舉考試.md "wikilink")（「大科」即考取功名授予官職參與政治的考試，只有[兩班階層可以參加](../Page/兩班.md "wikilink")，其他百姓與賤民只能參加「雜科」，即技能方面的考試）。朝廷廢除了庶民管理同等制度的《[經國大典](../Page/經國大典.md "wikilink")》頒佈後，洪吉童來到了當時隸屬于羅州目的[長城縣](../Page/長城縣.md "wikilink")，以葦嶺爲中心開展活動。又以光州的無登山和靈岩的月出山爲根據地，劫走貪官污吏及土豪的不義之財並分之與民衆，與官軍對抗。後又在金泉黃嶽山跟隨著學祖大師學習兵法和武功。

[睿宗元年](../Page/朝鮮睿宗.md "wikilink")[正月](../Page/正月.md "wikilink")[十月被政府軍討伐](../Page/十月.md "wikilink")，於同年十一月中旬把根據地移至[新安郡](../Page/新安郡_\(韓國\).md "wikilink")[押海岛](../Page/押海岛.md "wikilink")。洪吉童親自參與了[漁業和](../Page/漁業.md "wikilink")[農業活動](../Page/農業.md "wikilink")，[成宗元年](../Page/朝鮮成宗.md "wikilink")，再把根據地遷到西南海岸的海岛上。

成宗二十年，[全南道](../Page/全南道.md "wikilink")[道使](../Page/道使.md "wikilink")[韓建将洪吉童集团定为暴徒](../Page/韓建.md "wikilink")，把被捕的犯人严刑拷打。洪吉童集團暫停農業活動。成宗二十四年，朝廷為控制反抗活动，對从事海上活动的人发行海上通行证，洪吉童集團受到更嚴密的監控。

[燕山君元年](../Page/燕山君.md "wikilink")，洪吉童集团以势力范围扩展到了整个[忠清道](../Page/忠清道.md "wikilink")，參與者包括朝廷官吏、地方首領如[嚴貴孫等](../Page/嚴貴孫.md "wikilink")。燕山君六年發生[壬午士禍](../Page/壬午士禍.md "wikilink")，不少[士林派被殺或是被流放](../Page/士林派.md "wikilink")，加上連年乾旱，民怨載道。朝廷頒布大赦令，洪吉童集团[自首](../Page/自首.md "wikilink")，被判欺上罪，同年十一月被流放至南海三千里。

1505年，洪吉童集团由[琉球國](../Page/琉球國.md "wikilink")[久米岛](../Page/久米岛.md "wikilink")（今屬[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")）登陆，驱逐了实行残酷统治的酋长，开展了以日本、琉球国、中国为对象的中介贸易活动，並建造了防禦敌人的城，統治當地。據《[洪吉童傳](../Page/洪吉童傳.md "wikilink")》所載，洪吉童享年七十歲。

洪吉童與妻子[高乙露育有三男兩女](../Page/高乙露.md "wikilink")，高乙露把優質[稻種帶到琉球](../Page/稻.md "wikilink")，改善當地農民的生活，現時沖繩一帶和八重山地區都把她推崇爲豐裕之女神。

有說法指洪吉童與[尚真王時代的](../Page/尚真王.md "wikilink")[豪族](../Page/豪族.md "wikilink")[遠彌計赤蜂是同一人](../Page/遠彌計赤蜂.md "wikilink")，但現時仍未有足夠史料證實\[1\]。

## 有記載洪吉童的文獻

### 朝鮮

  - 《[朝鲜王朝实录](../Page/朝鲜王朝实录.md "wikilink")》
  - 《[泽堂集](../Page/泽堂集.md "wikilink")》
  - 《[溪西野谈](../Page/溪西野谈.md "wikilink")》
  - 《[青丘野谈](../Page/青丘野谈.md "wikilink")》
  - 《[补海东异绩](../Page/补海东异绩.md "wikilink")》
  - 《[星湖僿说](../Page/星湖僿说.md "wikilink")》
  - 《[韦岛王传](../Page/韦岛王传.md "wikilink")》
  - 《[万姓大同谱](../Page/万姓大同谱.md "wikilink")》

### 中國

  - 《[明史](../Page/明史.md "wikilink")》
  - 《[明史宝鉴](../Page/明史宝鉴.md "wikilink")》

### 琉球

  - 《[球阳](../Page/球阳.md "wikilink")》
  - 《[八重山由来记](../Page/八重山由来记.md "wikilink")》
  - 《[八重山岛缘来记](../Page/八重山岛缘来记.md "wikilink")》
  - 《[琉球国由来记](../Page/琉球国由来记.md "wikilink")》
  - 《[宫多史典](../Page/宫多史典.md "wikilink")》
  - 《[宫多岛庶民史](../Page/宫多岛庶民史.md "wikilink")》

## 相關作品

洪吉童的事蹟被朝鮮王朝時期的作家[許筠寫成](../Page/許筠.md "wikilink")[小說](../Page/小說.md "wikilink")《[洪吉童傳](../Page/洪吉童傳.md "wikilink")》，小說把洪吉童描繪成一個武藝高超、智慧非凡、道術高強的傳奇人物，這個藝術形象比歷史上真實的洪吉童更為人所熟悉。洪吉童的事蹟也被改編成[電影和](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，如《[快刀洪吉童](../Page/快刀洪吉童.md "wikilink")》、《[逆賊：偷百姓的盜賊](../Page/逆賊：偷百姓的盜賊.md "wikilink")》。

## 相關條目

  - [洪吉童作戰](../Page/洪吉童作戰.md "wikilink")，[大韓民國國軍在](../Page/大韓民國國軍.md "wikilink")[越南戰爭中的一次大規模軍事行動](../Page/越南戰爭.md "wikilink")

## 參考資料

  - [洪吉童年代记](https://web.archive.org/web/20071009004208/http://chi.jangseong.go.kr/sub03/sub_3_01.html)

<div class="references-small">

<references>

</references>

</div>

[Category:朝鮮王朝人物](../Category/朝鮮王朝人物.md "wikilink")
[Category:第二尚氏王朝人物](../Category/第二尚氏王朝人物.md "wikilink")
[Category:盜賊](../Category/盜賊.md "wikilink")
[Category:南陽洪氏](../Category/南陽洪氏.md "wikilink")

1.  [日韓友好の懸け橋に/洪吉童国際学術シンポ](http://ryukyushimpo.jp/news/storyid-111918-storytopic-86.html)