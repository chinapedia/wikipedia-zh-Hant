**洪堡縣**（**Humboldt
County**）是[美國](../Page/美國.md "wikilink")[加利福尼亞州西北部的一個縣](../Page/加利福尼亞州.md "wikilink")，西臨[太平洋](../Page/太平洋.md "wikilink")。面積10,495平方公里。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年估計，共有人口126,518人。縣治[尤里卡](../Page/尤里卡_\(加利福尼亞州\).md "wikilink")
（Eureka）。

此縣成立於1853年。縣名紀念[德國著名的博物學家](../Page/德國.md "wikilink")[亞歷山大·馮·洪堡](../Page/亞歷山大·馮·洪堡.md "wikilink")。縣內80%的人口都聚集在[洪堡灣附近](../Page/洪堡灣.md "wikilink")。

## 參考文獻

[H](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:亞歷山大·洪堡](../Category/亞歷山大·洪堡.md "wikilink")