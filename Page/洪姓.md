**洪姓**为[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第184位。2006年中国大陆洪姓人口排名第99名。此姓氏（）在[韓國亦有分佈](../Page/韓國.md "wikilink")，約佔當地人口1.13%。

## 起源

传说洪姓为[炎帝神农氏之后](../Page/炎帝.md "wikilink")--[共工的后代](../Page/共工.md "wikilink")。共工本姓共氏，[黄帝时担任了治理天下水利的官职](../Page/黄帝.md "wikilink")，被人们尊为水神。[颛顼帝时](../Page/颛顼.md "wikilink")，共工起兵争天下失败。传说他失败后一怒之下撞倒了西北方支撑天地的[不周山](../Page/不周山.md "wikilink")。共工氏为了讓子孫記住祖先是水神，因此在姓的旁边加上三点水，因此称洪姓。又一說共世後代(共普)為躲避仇人因此改姓洪等有共字邊之姓氏。

[山东省](../Page/山东.md "wikilink")[汶上县](../Page/汶上.md "wikilink")、[河南省](../Page/河南.md "wikilink")[偃师市](../Page/偃师.md "wikilink")《姬氏志》都介绍：“洪姓，系出安乐郡，姬姓，卫大夫弘演之后。到唐始避[高宗子李弘之讳](../Page/唐高宗.md "wikilink")，改为洪姓。”此支洪姓是[弘姓人弘演后裔](../Page/弘姓.md "wikilink")。

## 郡望

  - [敦煌郡](../Page/敦煌郡.md "wikilink")：汉武帝元鼎六年置。在今甘肃省河西走廊西端。
  - [豫章郡](../Page/豫章郡.md "wikilink")：汉代将秦代的九江郡改为豫章郡。
  - [宣城郡](../Page/宣城郡.md "wikilink")：晋时置郡。治所在宛陵（今安徽省定城）。

## 堂号

  - [燉煌堂](../Page/燉煌堂.md "wikilink")：因洪姓曾是[唐朝時](../Page/唐朝.md "wikilink")[敦煌郡的郡望](../Page/敦煌.md "wikilink")，故亦有此堂號。此堂號是目前在[台灣地區的洪姓及部分華僑所用](../Page/台灣.md "wikilink")。如[南投縣](../Page/南投縣.md "wikilink")[草屯鎮洪姓](../Page/草屯鎮.md "wikilink")[族譜](../Page/族譜.md "wikilink")、[臺南市](../Page/臺南市.md "wikilink")[北門區](../Page/北門區.md "wikilink")[洪氏宗親會堂便是便用此堂號](../Page/洪氏宗親會.md "wikilink")。
  - [嶝山堂](../Page/嶝山堂.md "wikilink")：洪姓先祖洪皎是北宋政和五年進士，南宋建炎年間年間擔任福州府丞，奉諫議大夫。

` 洪皎之次子洪道因避亂於紹興十年（1140）隱居同安縣小嶝嶼後頭保社，創立嶝山分堂號。`

  - [双忠堂](../Page/双忠堂.md "wikilink")：宋代[洪皓](../Page/洪皓.md "wikilink")，以礼部侍郎的身份出使金国，被扣15年不屈服，被比作苏武。儿子[洪迈](../Page/洪迈.md "wikilink")，又以翰林学士的身份出使金国，金人强迫他称"陪臣"他坚决拒绝，被金人拘留。他父子都为了祖国恪尽忠诚，人称"父子双忠"。
  - [六桂堂](../Page/六桂堂.md "wikilink")：[宋朝時候](../Page/宋朝.md "wikilink")[福建地方的](../Page/福建.md "wikilink")[翁乾度生有六子](../Page/翁乾度.md "wikilink")，皆中[進士](../Page/進士.md "wikilink")。後為避亂分姓為洪、江、翁、方、龔、汪六姓，時人美曰：「六桂聯芳」。於南方省分、台灣、東南亞分佈最多。見[翁姓](../Page/翁姓.md "wikilink")。
  - [益春洪](../Page/益春洪.md "wikilink")：[宋朝時候婢女洪益春跟隨](../Page/宋朝.md "wikilink")[陳三](../Page/陳三.md "wikilink")、[五娘一起來到](../Page/五娘.md "wikilink")[泉州](../Page/泉州.md "wikilink")，後來元兵進犯泉州，陳三夫婦投井自殺，身懷六甲的[洪益春逃到](../Page/洪益春.md "wikilink")[南安](../Page/南安.md "wikilink")，改姓[洪](../Page/洪.md "wikilink")。於南方省分、台灣、東南亞分佈最多。見[陳三五娘](../Page/陳三五娘.md "wikilink")、[荔鏡記](../Page/荔鏡記.md "wikilink")。

## 歷史名人

  - [洪皓](../Page/洪皓.md "wikilink")：宋朝忠良，民族英雄。
  - [洪迈](../Page/洪迈.md "wikilink")：宋朝作家，忠良，民族英雄。
  - [洪适](../Page/洪适.md "wikilink")：宋代名醫。
  - [洪遵](../Page/洪遵.md "wikilink")：“[三洪](../Page/三洪.md "wikilink")”之一，宋朝钱币學家。
  - [洪咨夔](../Page/洪咨夔.md "wikilink")：南宋诗人。嘉定进士，官至刑部尚书，翰林学士。
  - [洪應明](../Page/洪應明.md "wikilink")：明朝作家。
  - [洪旭](../Page/洪旭.md "wikilink")：[明鄭大將](../Page/明鄭.md "wikilink")。
  - [洪升](../Page/洪升.md "wikilink")：清朝作家。
  - [洪國榮](../Page/洪國榮.md "wikilink")：洪萬衡之五代孫，朝鮮正祖時期人物。
  - [洪景舟](../Page/洪景舟.md "wikilink")：朝鲜政治人物。
  - [熙嬪洪氏](../Page/熙嬪洪氏.md "wikilink"):洪景舟的女兒,朝鮮中宗後宮嬪御
  - [洪亮吉](../Page/洪亮吉.md "wikilink")：清代學者兼文學家，著有《洪北江全集》。
  - [洪秀全](../Page/洪秀全.md "wikilink")：[太平天國君主](../Page/太平天國.md "wikilink")。
  - [洪学智](../Page/洪学智.md "wikilink")：中國將軍。
  - [洪虎](../Page/洪虎.md "wikilink")：中國政治人物。
  - [洪天贵福](../Page/洪天贵福.md "wikilink")：太平天國君主。
  - [洪腾云](../Page/洪腾云.md "wikilink")：清朝商人。
  - [洪纪](../Page/洪纪.md "wikilink")：清朝政治人物。
  - [洪仁玕](../Page/洪仁玕.md "wikilink")：太平天國政治人物。
  - [洪承畴](../Page/洪承畴.md "wikilink")：明末清初軍事人物。
  - [洪毓琛](../Page/洪毓琛.md "wikilink")：清朝政治人物。
  - [洪吉童](../Page/洪吉童.md "wikilink")：朝鮮歷史人物。
  - [洪光迪](../Page/洪光迪.md "wikilink")：越南歷史人物，[明鄉人](../Page/明鄉人.md "wikilink")。

## 現代名人

  - [洪清田](../Page/洪清田.md "wikilink")：中國作家。
  - [洪磊](../Page/洪磊.md "wikilink")：中國外交官，現任中國外交部發言人。
  - [洪建威](../Page/洪建威.md "wikilink")：中國藝人。
  - [洪立熙](../Page/洪立熙.md "wikilink")：中國運動員。
  - [洪長庚](../Page/洪長庚.md "wikilink")：台灣第一位眼科博士。
  - [洪山川](../Page/洪山川.md "wikilink")：[台灣天主教](../Page/台灣天主教.md "wikilink")[主教](../Page/主教.md "wikilink")。
  - [洪元雄](../Page/洪元雄.md "wikilink")：[台灣中華郵政](../Page/台灣中華郵政.md "wikilink")[嘉義區退休局長](../Page/嘉義區退休局長.md "wikilink")。
  - [洪协](../Page/洪协.md "wikilink")：台湾政治人物。
  - [洪哲胜](../Page/洪哲胜.md "wikilink")：台湾政治人物。
  - [洪慈庸](../Page/洪慈庸.md "wikilink")：台灣政治人物。現任[立法委員](../Page/立法委員.md "wikilink")。第九屆
  - [洪仲丘](../Page/洪仲丘.md "wikilink")：[洪慈庸無血緣關係之弟](../Page/洪慈庸.md "wikilink")，[洪仲丘事件死難者](../Page/洪仲丘事件.md "wikilink")。
  - [洪宗熠](../Page/洪宗熠.md "wikilink")：台湾政治人物。現任[立法委員](../Page/立法委員.md "wikilink")。第九屆
  - [洪奇昌](../Page/洪奇昌.md "wikilink")：台湾政治人物。曾任[立法委員](../Page/立法委員.md "wikilink")。
  - [洪昭男](../Page/洪昭男.md "wikilink")：台湾政治人物。曾任[立法委員](../Page/立法委員.md "wikilink")。現任[監察委員](../Page/監察委員.md "wikilink")。
  - [洪健益](../Page/洪健益.md "wikilink")：台湾政治人物。曾任[立法委員](../Page/立法委員.md "wikilink")。
  - [洪秀柱](../Page/洪秀柱.md "wikilink")：台湾政治人物。曾任[中華民國第](../Page/中華民國.md "wikilink")14任第8屆[立法院副院長](../Page/立法院.md "wikilink")。
  - [洪維和](../Page/洪維和.md "wikilink")：台灣政治受難者。
  - [洪惟仁](../Page/洪惟仁.md "wikilink")：台湾語言學家。
  - [洪醒夫](../Page/洪醒夫.md "wikilink")：台湾作家。
  - [洪炎秋](../Page/洪炎秋.md "wikilink")：台湾作家。
  - [洪明奇](../Page/洪明奇.md "wikilink")：台湾分子生物學家。[中央研究院院士](../Page/中央研究院院士.md "wikilink")。
  - [洪致文](../Page/洪致文.md "wikilink")：台灣鐵道文章作家，同時是鐵道文化協會創始會員。
  - [洪胜德](../Page/洪胜德.md "wikilink")：台湾藝人。
  - [洪小鈴](../Page/洪小鈴.md "wikilink")：台湾藝人。
  - [洪清鴻](../Page/洪清鴻.md "wikilink")：台灣才子。
  - [洪偉權](../Page/洪偉權.md "wikilink")：香港著名補習天皇。
  - [洪金寶](../Page/洪金寶.md "wikilink")：香港演員。
  - [洪詩涵](../Page/洪詩涵_\(藝人\).md "wikilink")：藝名洪詩，[台灣女](../Page/台灣.md "wikilink")[藝人](../Page/藝人.md "wikilink")，前[黑澀會美眉成員](../Page/黑澀會美眉.md "wikilink")，現為[Popu
    Lady團員](../Page/Popu_Lady.md "wikilink")。
  - [洪一中](../Page/洪一中.md "wikilink")：台湾運動員。
  - [洪志傑](../Page/洪志傑.md "wikilink")：[祖籍](../Page/祖籍.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州石狮洪窟](../Page/泉州.md "wikilink")，曾效力[香港政府](../Page/香港政府.md "wikilink")，现职教师。香港中文大學碩士，[閩南語與](../Page/閩南語.md "wikilink")[普通話兼擅](../Page/普通話.md "wikilink")，乃香港少數研究[閩語的學者之一](../Page/閩語.md "wikilink")。第四屆香港特別行政區選舉委員會候選人。
  - [洪德麟](../Page/洪德麟.md "wikilink")：台湾漫畫家。
  - [洪晓蕾](../Page/洪晓蕾.md "wikilink")：台湾模特兒。
  - [洪一峰](../Page/洪一峰.md "wikilink")：台灣知名閩南語歌手、作曲家，有「寶島歌王」之稱。
  - [洪榮宏](../Page/洪榮宏.md "wikilink")：洪一峰之子，台灣知名閩南語歌手、作曲家，有「臺語歌王」之稱。
  - [洪敬堯](../Page/洪敬堯.md "wikilink")：洪一峰之子、洪榮宏之弟，台灣知名歌手、作曲家、編曲家。
  - [洪明甫](../Page/洪明甫.md "wikilink"): 韓國退役足球運動員。台灣知名教授級講師
  - [洪成南](../Page/洪成南.md "wikilink")：朝鲜總理。
  - [洪莉娜](../Page/洪莉娜.md "wikilink")：韓國演員。
  - [洪恩希](../Page/洪恩希.md "wikilink")：韓國演員。
  - [洪正好](../Page/洪正好.md "wikilink")：韩国足球运动员
  - [洪蘭坡](../Page/洪蘭坡.md "wikilink")：韓國作曲家。
  - [洪知秀](../Page/洪知秀.md "wikilink")：韓國男子團體[SEVENTEEN成員](../Page/Seventeen_\(組合\).md "wikilink")，分隊Vocal
    Team成員，藝名：Joshua。
  - [洪丕正](../Page/洪丕正.md "wikilink")：香港著名財經界人士
  - [洪真英](../Page/洪真英.md "wikilink")：韓國演員、歌手
  - [洪全瑞](../Page/洪全瑞.md "wikilink")：台灣著名木造船匠師，現為為文化部文化資產局列冊「木造船製作修復技術」保存者，文化部「傳統建築大木作」匠師，美和科技大學文化創意系榮譽教授。
  - [洪宗玄](../Page/洪宗玄.md "wikilink")：韓國演員。
  - [洪祥陞](../Page/洪祥陞.md "wikilink")：Lewis Hung， 福建晉江人，

香港出生現居香港，環保建材專家，隔熱玻璃發明家，擁有一個實用新型專利科技，世界知名的BNI商務引薦組織香港區大使。

  - [洪崇晏](../Page/洪崇晏.md "wikilink")：台灣街頭社會運動參與者。
  - [洪若崴](http://www.epochtimes.com/b5/16/10/21/n8420219.htm)：台灣出生南投縣人六桂堂宗親，台灣當代新銳藝術家，指畫家，鶯歌傳統琺華彩瓷器藝師，佛像修復彩繪藝師，傳統獅頭彩繪師。

## 外部連結

  - [洪氏宗亲网](http://www.hongjia.org/)

[洪姓](../Category/洪姓.md "wikilink") [S單](../Category/漢字姓氏.md "wikilink")
[Category:朝鮮語姓氏](../Category/朝鮮語姓氏.md "wikilink")