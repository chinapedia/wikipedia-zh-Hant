**洪性榮**（），[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[政治人物](../Page/政治人物.md "wikilink")，曾代表[中國國民黨當選為](../Page/中國國民黨.md "wikilink")[彰化縣議會議員](../Page/彰化縣議會.md "wikilink")、[臺灣省議會第七至八屆議員及第二至四屆](../Page/臺灣省議會.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")\[1\]，但於2001年底競選連任時失利\[2\]。因觸犯貪汙治罪條例起訴，經多次上訴，最後[台灣高等法院因洪性榮死亡](../Page/臺灣高等法院.md "wikilink")，判決公訴不受理\[3\]。

## 參考資料

## 外部連結

[Category:第2屆中華民國立法委員](../Category/第2屆中華民國立法委員.md "wikilink")
[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第8屆臺灣省議員](../Category/第8屆臺灣省議員.md "wikilink")
[Category:第7屆臺灣省議員](../Category/第7屆臺灣省議員.md "wikilink")
[Category:彰化縣議員](../Category/彰化縣議員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:國立彰化師範大學校友](../Category/國立彰化師範大學校友.md "wikilink")
[Category:國立臺中教育大學校友](../Category/國立臺中教育大學校友.md "wikilink")
[Category:彰化人](../Category/彰化人.md "wikilink")
[Xing性](../Category/洪姓.md "wikilink")

1.  [台灣省諮議會
    洪性榮議員簡介](http://www.tpa.gov.tw/big5/Councilor/Councilor_view.asp?id=464&cid=2&urlID=20)
2.  [《第 05 屆 立法委員選舉
    區域候選人得票概況》，選舉資料庫網站](http://210.69.23.140/vote3.asp?pass1=B2001A0000000000aaa)
3.