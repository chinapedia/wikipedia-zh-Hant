[HunsrueckLage.png](https://zh.wikipedia.org/wiki/File:HunsrueckLage.png "fig:HunsrueckLage.png")
[Hunsrück_-_Deutsche_Mittelgebirge,_Serie_A-de.png](https://zh.wikipedia.org/wiki/File:Hunsrück_-_Deutsche_Mittelgebirge,_Serie_A-de.png "fig:Hunsrück_-_Deutsche_Mittelgebirge,_Serie_A-de.png")和[萨尔州的位置](../Page/萨尔州.md "wikilink")\]\]

**洪斯吕克山脉**（德语：Hunsrück）是[德国](../Page/德国.md "wikilink")[莱茵兰-普法尔茨州的一座低矮的山脉](../Page/莱茵兰-普法尔茨州.md "wikilink")。[摩泽尔河](../Page/摩泽尔河.md "wikilink")（北面）、[纳厄河](../Page/纳厄河.md "wikilink")（南面）和[莱茵河](../Page/莱茵河.md "wikilink")（东面）三条河的河谷构成了它的边界。洪斯吕克山脉东面延伸到[莱茵河东侧的](../Page/莱茵河.md "wikilink")[陶努斯山](../Page/陶努斯山.md "wikilink")，北面隔[摩泽尔河与](../Page/摩泽尔河.md "wikilink")[艾菲尔山相望](../Page/艾菲尔山.md "wikilink")，南面越过[纳厄河就是](../Page/纳厄河.md "wikilink")[普法尔茨地区](../Page/普法尔茨地区.md "wikilink")。

洪斯吕克山属于多雨气候。地质构成为[页岩](../Page/页岩.md "wikilink")。

洪斯吕克山脉的大部分山丘海拔高度都不超过400米。最高峰是埃尔博斯科普夫山（Erbeskopf）海拔816米。

山区比较知名的城镇包括（Simmern），（Kirchberg），[伊达-奥博施坦](../Page/伊达-奥博施坦.md "wikilink")（），[卡斯特劳恩](../Page/卡斯特劳恩.md "wikilink")（）和[莫巴赫](../Page/莫巴赫.md "wikilink")（）。[法兰克福-哈恩机场](../Page/法兰克福-哈恩机场.md "wikilink")，这座发展中的廉价航空机场也位于这个地区。

## 参考资料

<div class="references-small">

  - Uwe Anhäuser: *Sagenhafter Hunsrück.* Rhein-Mosel-Verlag, Alf/ Mosel
    1995, ISBN 3-929745-23-2.
  - Uwe Anhäuser: *Kultur-Erlebnis Hunsrück.* Literaturverlag Dr.
    Gebhardt und Hilden, Idar-Oberstein 2000, ISBN 3-932515-29-3.
  - Uwe Anhäuser: *Schinderhannes und seine Bande.* Rhein-Mosel-Verlag,
    Alf/ Mosel 2003, ISBN 3-89801-014-7.
  - Uwe Anhäuser: *Die Ausoniusstraße von Bingen über den Hunsrück nach
    Trier. Ein archäologischer Reise- und Wanderführer.*
    Rhein-Mosel-Verlag, Alf/ Mosel 2006, ISBN 3-89801-032-5.

</div>

## 外部链接

  - [洪斯吕克山博物馆](http://www.hunsrueck-museum.de)德语
  - [洪斯吕克山地理](http://www.mgas.de/fundstellen/hunsrueck/uebersicht.htm)德语
  - [概念上的洪斯吕克](https://web.archive.org/web/20080120031427/http://www.hunsnet.de/fh/region/name.htm)德语
  - [来自荷兰的关于洪斯吕克山与徒步旅行的网站](http://www.hunsrueck.nl/)荷兰语
  - [户外网站关于洪斯吕克山区Ausoniusweg的介绍](https://web.archive.org/web/20070911113430/http://wiki.outdoorseiten.net/index.php/Ausoniusweg)德语

[H](../Category/德国山峰.md "wikilink")