**洪明甫**（，），出生于[韩国](../Page/韩国.md "wikilink")[汉城](../Page/汉城.md "wikilink")(今首尔)，是一名[韩国退役足球运动员](../Page/韩国.md "wikilink")，场上司职后卫，曾被认为是亚洲最好的后卫。他也是唯一一名入选[FIFA
100名单的韩国球员](../Page/FIFA_100.md "wikilink")。现时担任足球教练。

## 球员生涯

洪明甫在退役前一直是[韩国国家队的主力球员](../Page/韩国国家足球队.md "wikilink")，也是首名韩国球员连续出席了四屆[世界杯決賽周](../Page/世界杯.md "wikilink")(1990-2002年)。他在[高丽大学毕业后加入了](../Page/高丽大学.md "wikilink")[K联赛球队](../Page/K联赛.md "wikilink")[浦项制铁](../Page/浦项制铁.md "wikilink")，1997年加盟[湘南比马转踢](../Page/湘南比马.md "wikilink")[日本职业足球联赛](../Page/日本职业足球联赛.md "wikilink")，2002年世界杯前回到[浦项制铁效力](../Page/浦项制铁.md "wikilink")。

1990年世界杯是洪明甫第一次代表韩国队参赛世界杯，从那以后，他就成为了韩国队后防线上的中坚，并担任队长。洪明甫作为队长曾在2002年世界杯上率领韩国队历史性地打入半决赛,使韩国成为第一支闯入世界杯半决赛的亚洲球队。他于2003年赛季加盟[美国职业足球大联盟球队](../Page/美国职业足球大联盟.md "wikilink")[洛杉矶银河](../Page/洛杉矶银河.md "wikilink")，成为美国职业大联盟历史上第一位韩国球员，并於2004年末在[洛杉矶银河結束其球员生涯](../Page/洛杉矶银河.md "wikilink")。

## 教练生涯

退役以後洪明甫於2005年9月26日獲委任為韓國國家足球隊助理教練，作为国家队及国奥队的助理教练，参加了2006年世界杯、2007年亚洲杯以及[2008年北京奥运会](../Page/2008年北京奥运会.md "wikilink")。2009年洪明甫作为国青队主教练，率韩国队在世青赛上闯入了八强。

2013年6月24日起任韩国国家队主教练。2014年7月10日，洪明甫突然宣布辞去韩国队主帅一职。有媒体在近日将他在世界杯集训期间在城南市购买土地的消息披露了出来，这使得他的家人都受到了舆论批评的影响。此外，韩国媒体还曝光了一段视频，在世界杯出局后，洪明甫率领国家队队员在巴西当地一家餐厅聚餐，手舞足蹈，与当地女性饮酒唱歌跳舞的画面，也是引发了民众的口水，谴责之声越发响亮。最终洪明甫宣布辞职，韩国足球协会也进入了新的选帅期。

2016年，洪明甫执教[中超球队](../Page/中国足球协会超级联赛.md "wikilink")[杭州绿城](../Page/杭州绿城.md "wikilink")，但当赛季结束后降级到[中甲](../Page/中国足球协会甲级联赛.md "wikilink")。2017年5月22日，战绩不佳的球队与其解约，但他爆料称遭球队管理层干涉排兵布阵。

[Category:2014年世界盃足球賽主教練](../Category/2014年世界盃足球賽主教練.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:1996年亞洲盃足球賽球員](../Category/1996年亞洲盃足球賽球員.md "wikilink")
[Category:1994年世界盃足球賽球員](../Category/1994年世界盃足球賽球員.md "wikilink")
[Category:1990年世界盃足球賽球員](../Category/1990年世界盃足球賽球員.md "wikilink")
[Category:韓國國家足球隊主教練](../Category/韓國國家足球隊主教練.md "wikilink")
[Category:韓國國家足球隊球員](../Category/韓國國家足球隊球員.md "wikilink")
[Category:韓國足球運動員](../Category/韓國足球運動員.md "wikilink")
[Category:浦项制铁球员](../Category/浦项制铁球员.md "wikilink")
[Category:湘南麗海球員](../Category/湘南麗海球員.md "wikilink")
[Category:柏雷素尔球员](../Category/柏雷素尔球员.md "wikilink")
[Category:洛杉矶银河球员](../Category/洛杉矶银河球员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink") [Category:FIFA
100](../Category/FIFA_100.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")
[Category:日本外籍足球運動員](../Category/日本外籍足球運動員.md "wikilink")
[Category:美國外籍足球運動員](../Category/美國外籍足球運動員.md "wikilink")
[Category:韓國旅外足球運動員](../Category/韓國旅外足球運動員.md "wikilink")
[Category:高麗大學校友](../Category/高麗大學校友.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")