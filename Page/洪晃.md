**洪晃**（，），生于[北京](../Page/北京.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[中国](../Page/中国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[宁波市](../Page/宁波市.md "wikilink")[慈溪县](../Page/慈溪县.md "wikilink")，[美籍华人](../Page/美籍华人.md "wikilink")，[北京媒体业名人](../Page/北京.md "wikilink")，中国互动媒体集团的CEO，《世界都市iLOOK》杂志主编兼出版人,[中国名媛](../Page/中国.md "wikilink")[章含之与原配前夫](../Page/章含之.md "wikilink")[洪君彦之女](../Page/洪君彦.md "wikilink")，其继父为[中华人民共和国著名外交官](../Page/中华人民共和国.md "wikilink")[乔冠华](../Page/乔冠华.md "wikilink")，其前夫之一为著名导演[陈凯歌](../Page/陈凯歌.md "wikilink")。

## 早年

洪晃的生母[章含之](../Page/章含之.md "wikilink")，曾是[毛泽东主席亲自钦点的英文教师](../Page/毛泽东.md "wikilink")。在其父母于[文革期间离婚之后](../Page/文革.md "wikilink")，洪晃成为外交官[乔冠华的继女](../Page/乔冠华.md "wikilink")。\[1\]。

  - 1974年，作为[中国外交部的](../Page/中国外交部.md "wikilink")「后备人才」培養對象之一，12歲的洪晃被以「[高干子弟小](../Page/高干子弟.md "wikilink")[留学生](../Page/留学生.md "wikilink")」的身份派往[美国學習](../Page/美国.md "wikilink")，並在期間[寄宿於當地家庭](../Page/寄宿.md "wikilink")，得以深入了解[美國文化](../Page/美國文化.md "wikilink")\[2\]。
  - 1978至1980年，洪晃返回[中國大陸](../Page/中國大陸.md "wikilink")，並任職於[中国国际广播电台](../Page/中国国际广播电台.md "wikilink")。
  - 1980年代，洪晃获得全额奖学金，得以進入[美国](../Page/美国.md "wikilink")[私立大学](../Page/私立大学.md "wikilink")[瓦萨学院学习](../Page/瓦萨学院.md "wikilink")，主修国际政治，成為最早接受[美國教育的](../Page/美國教育.md "wikilink")[太子黨成員之一](../Page/太子黨.md "wikilink")\[3\]，後於1984年畢業。
  - 1985年，洪晃再度返回[中國大陸](../Page/中國大陸.md "wikilink")，任外国咨询公司咨询员。
  - 1986年，洪晃担任（[北京办事处](../Page/北京.md "wikilink")）驻[华首席代表](../Page/中國大陸.md "wikilink")。
  - 1996年，洪晃担任[北京](../Page/北京.md "wikilink")[标准国际投资管理有限公司](../Page/北京标准国际投资管理有限公司.md "wikilink")（[民企](../Page/民营企业.md "wikilink")）执行董事。

## 中国時尚教母

1996年起，洪晃的大部分時間都專注在監控她所擁有的投資公司的各項投資項目上。在1998年，一項公司的投資－－一本開办兩年的婦女時尚雜誌《LOOK》開始表現不佳，洪晃欣然表示願意改變跑道投入，先任主編，然後任出版人。《LOOK》雜誌現在是《iLOOK世界都市》時尚雜誌\[4\]。洪晃表示“我們的讀者不只是想讀購物指南，他們想知道國際上尖端的潮流\[5\]。”《iLook世界都市》雜誌專注名人，該雜誌最受歡迎的定期專欄報导名人在公共场合的穿着打扮，另一個讀者最喜愛的專欄介绍名牌，另一個專欄介绍中国還没有的名牌\[6\]。

2000年3月起，洪晃担任[中国](../Page/中國大陸.md "wikilink")[互动媒体集团](../Page/中国互动媒体集团.md "wikilink")（）的[首席执行官](../Page/首席执行官.md "wikilink")，旗下的[時尚](../Page/時尚.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")[品牌有](../Page/品牌.md "wikilink")：

  - 《iLOOK世界都市》
  - 《名牌世界·乐》月刊\[7\]（即[英国杂志](../Page/英国.md "wikilink")《[Time
    Out](../Page/Time_Out.md "wikilink")》的[中国版](../Page/中國大陸.md "wikilink")，创刊于2002年8月，现已转变为专属面向[大陆](../Page/中國大陸.md "wikilink")[一线城市的地区性消费导刊](../Page/全球城市.md "wikilink")\[8\]。）
      - 《Time Out
        北京》双周刊（含[中](../Page/中文.md "wikilink")/[英文版](../Page/英文.md "wikilink")，改版自2008年8月。）
      - 《Time Out
        上海》双周刊（含[中](../Page/中文.md "wikilink")/[英文版](../Page/英文.md "wikilink")，改版自2008年8月。）
  - 《SEVENTEEN青春一族》（即[美国杂志](../Page/美国.md "wikilink")《[SEVENTEEN](../Page/17_\(雜誌\).md "wikilink")》的[中国版](../Page/中國大陸.md "wikilink")）

另外，洪晃还擁有一間地處[北京](../Page/北京.md "wikilink")[三里屯](../Page/三里屯.md "wikilink")、以[中國](../Page/中國.md "wikilink")[設計師為主打](../Page/設計師.md "wikilink")[品牌的](../Page/品牌.md "wikilink")[原创设计](../Page/原创设计.md "wikilink")[概念店](../Page/概念店.md "wikilink")（或[精品店](../Page/精品店.md "wikilink")）“薄荷·糯米·葱”\[9\]，以幫助[中國大陸的](../Page/中國大陸.md "wikilink")[服裝設計師來銷售他們自己的作品](../Page/服裝設計師.md "wikilink")\[10\]。

在[美國](../Page/美國.md "wikilink")，[紐約時報報導常有人称洪晃為](../Page/紐約時報.md "wikilink")「中國的[奧花·雲費](../Page/奧花·雲費.md "wikilink")」\[11\]。[華爾街日報称洪晃為](../Page/華爾街日報.md "wikilink")「中国的设计[教母](../Page/教母.md "wikilink")」\[12\]。《[時代](../Page/時代\(雜誌\).md "wikilink")》[雜誌將她選入了](../Page/雜誌.md "wikilink")2011年最具影響力[时代百大人物](../Page/时代百大人物.md "wikilink")\[13\]。

2011年4月，洪晃被[台灣媒體](../Page/台灣媒體.md "wikilink")《[旺報](../Page/旺報.md "wikilink")》採訪和標榜為「中國第一間設計博物館負責人」\[14\]，並在同年7月與[網易時尚專訪過程中](../Page/网易.md "wikilink")，提議[中國政府需要](../Page/中华人民共和国政府.md "wikilink")「建立一个[设计](../Page/设计.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")，建立一个[对外开放的设计](../Page/对外开放.md "wikilink")[图书馆](../Page/图书馆.md "wikilink")，建立一个非常好的中国[手工艺博物馆](../Page/手工艺.md "wikilink")，建立一个手工艺[档案库](../Page/档案.md "wikilink")。\[15\]」才能為更多[中國內地設計師創造良好的發展舞台](../Page/中國內地.md "wikilink")。

## 私人生活

洪晃经历过三次婚姻，育有一女（养女）；1983年大学三年级時嫁给一位任職[IBM的美国工程师](../Page/IBM.md "wikilink")。1985年與夫婿回國，後來兩人吵翻。1989年和中国电影导演[陈凯歌在纽约结婚](../Page/陈凯歌.md "wikilink")，1993年洪晃提出离婚。同年洪晃第三次结婚，嫁給法国驻上海领事馆文化官员彭赛。2005年，她与彭赛分居。後來新男友杨小平是一位室内装修设计师\[16\]\[17\]。

## 著作

  - 《我的非正常生活》 － 自傳
  - 《无目的美好生活》 － 散文
  - 《廉价哲学》 － 散文

## 身世

## 参考文献

## 參見

  - [太子党](../Page/太子党.md "wikilink")
  - [高干子弟](../Page/高干子弟_\(中华人民共和国\).md "wikilink")

## 外部連結

  - 《[我的非正常生活](http://book.sina.com.cn/liter/myabnormallife/)》 新浪公司 新浪主页
    \> 读书频道 \> 长篇连载 \> 我的非正常生活

  -
[Category:中华人民共和国高级干部的女儿](../Category/中华人民共和国高级干部的女儿.md "wikilink")
[Category:在中华人民共和国的美国人](../Category/在中华人民共和国的美国人.md "wikilink")
[Category:归化美国公民的中华人民共和国人](../Category/归化美国公民的中华人民共和国人.md "wikilink")
[H洪](../Category/北京人.md "wikilink")
[H洪](../Category/慈溪人.md "wikilink")
[H洪](../Category/宁波裔美国人.md "wikilink")
[Category:时代百大人物](../Category/时代百大人物.md "wikilink")
[H](../Category/洪姓.md "wikilink") [H](../Category/瓦薩學院校友.md "wikilink")

1.

2.  [纽约空降红小兵](http://www.people.com.cn/GB/wenhua/1088/2490597.html)
    2004-05-11 人民网

3.

4.  [Hung Huang • Interpreter of
    Style](http://www.time.com/time/2005/style/030105/who/2.html)
    2005-02-28 《時代》雜誌

5.
6.  [Magazine licensing red-hot in
    China](http://www.atimes.com/atimes/China/FL16Ad01.html) 2004-12-16
    [亞洲時報在線](../Page/亞洲時報在線.md "wikilink")

7.  [关于城市生活类杂志的对话](http://www.china.com.cn/chinese/feature/203400.htm)
    2002-09-11 [中华读书报](../Page/中华读书报.md "wikilink")

8.  [半程盘点之封面女星PK：周迅封后
    章子怡冲劲足](http://yule.sohu.com/20080815/n258764204.shtml)
    2008-08-15 [搜狐娱乐](../Page/搜狐.md "wikilink")

9.  根據其官方網站所解釋之經營理念，店名當中每個[詞組的](../Page/詞組.md "wikilink")[拼音首](../Page/拼音.md "wikilink")[字母可被視為](../Page/字母.md "wikilink")[縮寫](../Page/縮寫.md "wikilink")「」，引申代表「」之意。

10. [BNC薄荷糯米葱
    不按常理出牌的幽默](http://www.ilook.com.cn/ilook_bnc/20101208_4105)
    2010-09-10 iLOOK 世界都市

11. [Hung Huang](http://www.nytimes.com/ref/business/economy/huang.html)
    2008-09-18 [紐約時報](../Page/紐約時報.md "wikilink")

12. [The Godmother of Chinese
    Designers](http://online.wsj.com/article/SB10001424052748703960004575426852614702026.html?mod=googlenews_wsj)
    2010-08-15 [華爾街日報](../Page/華爾街日報.md "wikilink")

13. [The 2011 TIME 100 - Hung
    Huang](http://www.time.com/time/specials/packages/article/0,28804,2066367_2066369_2066139,00.html)
    2011-04-21
    《[時代](../Page/時代\(雜誌\).md "wikilink")》[雜誌](../Page/雜誌.md "wikilink")

14. [出身名門
    洪晃引領中國時尚](http://ssl.want-daily.com/News/Content.aspx?id=0&yyyymmdd=20110423&k=17915aed7bb9a81196139f84ceafb832&h=c6f057b86584942e415435ffb1fa93d4&nid=K@20110423@N0033.001)
     2011-04-23 [旺報](../Page/旺報.md "wikilink")

15. [洪晃访谈:奢侈品牌，我们在萌芽，西方在衰落（六）](http://fashion.163.com/11/0729/13/7A4R178F00264J1O_6.html)
    2011-07-29 [網易](../Page/網易.md "wikilink")

16. [凤凰卫视
    《名人面对面》：洪晃的爱情史](http://news.xinhuanet.com/book/2004-05/13/content_1466915.htm)
    2004-05-13 [中华读书报](../Page/中华读书报.md "wikilink")

17.