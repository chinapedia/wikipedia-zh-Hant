**洪泽湖大堤**，古称**高家堰**，是位于中国[江苏省](../Page/江苏省.md "wikilink")[淮安市的古代水利工程](../Page/淮安市.md "wikilink")。

高家堰（洪泽湖大堤）始建于[东汉](../Page/东汉.md "wikilink")[建安五年](../Page/建安_\(东汉\).md "wikilink")（200年）
，由[广陵](../Page/廣陵郡.md "wikilink")[太守](../Page/太守.md "wikilink")[陈登主持建筑](../Page/陳登.md "wikilink")，初为30里。当时还没有[洪泽湖](../Page/洪泽湖.md "wikilink")，大堤是在淮河边的富陵湖、白水塘等诸湖荡的东北隅。明[永乐年间](../Page/永乐_\(明朝\).md "wikilink")，[漕运总督](../Page/漕运总督.md "wikilink")[陈瑄在武墩至周桥之间兴工修堤](../Page/陳瑄.md "wikilink")。明万历年间，漕运总督[潘季驯又将大堤延筑至蒋坝](../Page/潘季驯.md "wikilink")，大堤基本定型。清代經過歷任[河道總督](../Page/河道總督.md "wikilink")、[江南河道總督如](../Page/江南河道總督.md "wikilink")[靳輔](../Page/靳輔.md "wikilink")、[張鵬翮](../Page/張鵬翮.md "wikilink")、[蘭第錫](../Page/蘭第錫.md "wikilink")、[吳璥](../Page/吳璥.md "wikilink")、[徐端](../Page/徐端.md "wikilink")、[黎世序](../Page/黎世序.md "wikilink")、[張井等持續修建以成今貌](../Page/張井.md "wikilink")。

从明[万历八年](../Page/万历.md "wikilink")（1580年）起，洪泽湖大堤在迎水面开始增筑直立条石墙护面，到清[乾隆十六年](../Page/乾隆.md "wikilink")（1751年）的171年内，筑成长60.1公里，高7至8米的石工墙，蜿蜒曲折共108弯，甚为壮观。且规格统一，是用长0.8至1.2米、宽厚各0.4米的6万多块条石砌成，筑工精细。

大堤沿线有减水坝如仁、义、礼、智、信五座遗址（其中信坝保存较为完好）、林坝、新信坝、蒋家坝，[蒋坝镇以南的仁](../Page/蒋坝镇.md "wikilink")、义、礼坝。还有众多的名胜古迹如周桥越堤遗址、镇水铁牛、乾隆御碑、三河闸等。

2006年，洪泽湖大堤被列为第六批全国重点文物保护单位。此前洪泽湖大堤已被江苏省文物局作为申报世界文化遗产的推荐名单，上报国家文物局。

## 参考文献

[Category:淮安建筑物](../Category/淮安建筑物.md "wikilink")
[Category:中国古代水利工程](../Category/中国古代水利工程.md "wikilink")
[Category:江苏水利](../Category/江苏水利.md "wikilink")
[Category:京杭大运河世界遗产点](../Category/京杭大运河世界遗产点.md "wikilink")