**洪湖**位于中国[湖北省](../Page/湖北省.md "wikilink")[洪湖市和](../Page/洪湖市.md "wikilink")[监利县之间](../Page/监利县.md "wikilink")，[长江中游北岸](../Page/长江.md "wikilink")，[东荆河南侧](../Page/东荆河.md "wikilink")，是中国第七大[淡水湖](../Page/淡水湖.md "wikilink")，湖北省第一大湖。东西长23.4千米，南北宽20.8千米，岸线长104.5千米，湖底高程22.5～22.8米，湖面面积348.2平方千米（水位24.4米时），流域面积3314平方千米。2002年10月测得平均水深1.34米，最大水深2.30米。2000年最高水位26.5米，最低水位23.0米。1969年长江决堤时最高水位27.46米。\[1\]\[2\]

洪湖形成于约2500年前\[3\]，是古[云梦泽的残迹](../Page/云梦泽.md "wikilink")。

2000年被列为湖北省首家[湿地类型的省级](../Page/湿地.md "wikilink")[自然保护区](../Page/自然保护区.md "wikilink")。洪湖湿地已被列入[中国湿地保护行动计划和](../Page/中国湿地保护行动计划.md "wikilink")[中国重要湿地名录](../Page/中国重要湿地名录.md "wikilink")。\[4\]

## 参考文献

[H](../Category/长江中游水系.md "wikilink")
[H](../Category/湖北地理之最.md "wikilink")
[H](../Category/荆州地理.md "wikilink")
[H](../Category/洪湖市.md "wikilink")
[H](../Category/中国湿地.md "wikilink")

1.

2.

3.
4.