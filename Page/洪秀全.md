**洪秀全**（），又名**洪日**，[客家人](../Page/客家人.md "wikilink")\[1\]，是[清末宗教组织](../Page/清.md "wikilink")[拜上帝会创始人](../Page/拜上帝会.md "wikilink")、[太平天国运动的民變领袖以及](../Page/太平天国运动.md "wikilink")[太平天国的建立者](../Page/太平天国.md "wikilink")。原名**洪仁坤**，小名**火秀**，[广东](../Page/广东.md "wikilink")[花县](../Page/花县.md "wikilink")（今[廣州市](../Page/廣州市.md "wikilink")[花都區](../Page/花都區.md "wikilink")）人。\[2\]

洪秀全早年曾接触[基督教思想](../Page/基督教.md "wikilink")，创立拜上帝会，主张建立远古“天下为公”盛世。1851年拜上帝会在[广西](../Page/广西.md "wikilink")[桂平县](../Page/桂平县.md "wikilink")[金田村起兵反抗清朝统治](../Page/金田村.md "wikilink")，建立太平天国，洪秀全自称**天王**。1853年攻取[江寧府](../Page/江寧府.md "wikilink")（今[南京](../Page/南京.md "wikilink")），改名[天京](../Page/天京.md "wikilink")，以其作为首都。太平天国占据了[江南富庶一帶](../Page/江南.md "wikilink")，撼動清廷稅收來源。1864年洪秀全在天京病逝，太平天國於其死後旋即滅亡。

## 姓名

原名**洪仁坤**，小名**火秀**，又名**洪日**。在1837年大病痊癒後，聲稱夢見[天父著白袍於夢境賜](../Page/耶和華.md "wikilink")[寶劍](../Page/寶劍.md "wikilink")「雲中雪」、[印璽給他](../Page/璽.md "wikilink")\[3\]。上帝在中文名為耶火華，天父指他名須[避諱](../Page/避諱.md "wikilink")，必須用全（全[拆字為人王](../Page/拆字.md "wikilink")，有自稱[君王之意](../Page/君王.md "wikilink")）代替火字（火字乃犯「爺火華」，即今譯之天父耶和華名諱），又說他有三種改名方法。一是隱匿，自稱洪秀；二是新創，自稱洪全；三也可以使用不犯諱的名字，叫**洪秀全**，因而得名。

## 生平

### 早年

[Writings_by_Hong_Xiuquan_2011-12.JPG](https://zh.wikipedia.org/wiki/File:Writings_by_Hong_Xiuquan_2011-12.JPG "fig:Writings_by_Hong_Xiuquan_2011-12.JPG")藏。\]\]
1814年1月1日([清](../Page/清朝.md "wikilink")[嘉庆十八年十二月初十](../Page/嘉庆.md "wikilink"))，洪秀全於生於[广东](../Page/广东.md "wikilink")[花县](../Page/花县.md "wikilink")（今[廣州市](../Page/廣州市.md "wikilink")[花都區](../Page/花都區.md "wikilink")）福源水村。父親[洪鏡揚](../Page/洪鏡揚.md "wikilink")，是耕读世家，母親王氏。洪秀全還有長兄洪仁發，二兄洪仁達、堂弟[洪仁玕](../Page/洪仁玕.md "wikilink")。

洪秀全祖先居於今日[江苏](../Page/江苏.md "wikilink")，其祖先[洪皓是](../Page/洪皓.md "wikilink")[南宋的名臣](../Page/南宋.md "wikilink")，与当时的[岳飞都是同期的爱国](../Page/岳飞.md "wikilink")[民族英雄](../Page/民族英雄.md "wikilink")，而後南遷[閩南](../Page/閩南.md "wikilink")，又遷居[廣東](../Page/廣東.md "wikilink")，在[廣州府](../Page/廣州府.md "wikilink")[花縣落腳](../Page/花縣.md "wikilink")。

洪秀全7歲起，在村中[私塾上學](../Page/私塾.md "wikilink")，學習[四書五經及其它一些古籍](../Page/四書五經.md "wikilink")。由於他是兄弟中唯一識字者，村中父老看好洪秀全可考取功名光宗耀祖，可是三次[府试都失败落选](../Page/府试.md "wikilink")，第三次在[广州落选后已经是](../Page/广州.md "wikilink")25岁（1837年，[虚岁](../Page/虚岁.md "wikilink")）了，受此打击回家以后重病一场，一度昏迷，他本人说病中幻觉有一老人对他说：奉[上天的旨意](../Page/上天.md "wikilink")，命他到人间来斩妖除魔。从此洪秀全言语沉默，举止怪异。

此时洪秀全并不甘心于考试的失败，在6年后的1843年春天再一次参加了广州的府试，结果还是以落选告终。這时洪秀全翻閱以前在[廣州應試時收到的基督徒](../Page/廣州.md "wikilink")[梁发的](../Page/梁发.md "wikilink")《[劝世良言](../Page/劝世良言.md "wikilink")》一書，把書中內容與自己以前大病時的幻覺對比，自認為受上帝之命下凡誅妖，自認上帝的幼子，耶穌的幼弟，並稱上帝耶和華為「天父」，稱耶穌為「天兄」，一气抛开了[孔孟之书](../Page/儒學.md "wikilink")，不再做一名儒生而改信了[基督教的教义](../Page/基督教.md "wikilink")，索性把家里的[孔子牌位换成了](../Page/孔子.md "wikilink")[上帝的牌位](../Page/耶和華.md "wikilink")。雖然未曾讀過《[聖經](../Page/聖經.md "wikilink")》，洪秀全卻开始逢人便宣传他所理解的基督教教义，称之为“拜上帝會”。洪秀全说：「人心太坏，政治腐败，天下将有大灾大难，唯信仰上帝入教者可以免难。入教之人，无论男女尊贵一律平等，男曰兄弟，女曰姊妹。」洪秀全的「[拜上帝會](../Page/拜上帝會.md "wikilink")」在教義上模仿[基督宗教](../Page/基督宗教.md "wikilink")，太平天國的十誡叫[十款天條](../Page/十款天條.md "wikilink")。\[4\]

洪秀全最初在廣州附近傳教，但未取得很大成功。1844年洪秀全和[馮雲山轉至](../Page/馮雲山.md "wikilink")[廣西一帶傳教](../Page/廣西.md "wikilink")，洪不久便返回廣東，馮留下發展，在當地的信徒日增。1845年至1846年間，在家鄉的洪秀全寫下《[原道醒世訓](../Page/原道醒世訓.md "wikilink")》、《[原道覺世訓](../Page/原道覺世訓.md "wikilink")》、《百正歌》等作品。1847年初，洪秀全來到[傳教士](../Page/傳教士.md "wikilink")[羅孝全在廣州的禮拜堂學習了幾個月](../Page/羅孝全.md "wikilink")，曾要求[受洗](../Page/受洗.md "wikilink")，但羅孝全不同意洪秀全對以前大病時所見「異象」的見解，並認為洪秀全對[教義的認識不足夠](../Page/教義.md "wikilink")，拒絕為他施洗。洪秀全4個月後離開。洪其後再到廣西會合馮雲山，並陸續制訂拜上帝會的規條及儀式。

洪秀全的[拜上帝會與地方](../Page/拜上帝會.md "wikilink")[衙門的矛盾日漸加深](../Page/衙門.md "wikilink")，洪秀全等人在1850年決定反清，加緊準備，會眾在下半年間陸續前來金田團營。

### 建立太平天国

[TaiPingRevolutionSeal.png](https://zh.wikipedia.org/wiki/File:TaiPingRevolutionSeal.png "fig:TaiPingRevolutionSeal.png")

1851年1月11日在[广西](../Page/广西.md "wikilink")[桂平的金田村举行](../Page/桂平.md "wikilink")[起义](../Page/金田起义.md "wikilink")，建号太平天国，洪秀全稱天王；9月攻克永安后，分封东、西、南、北、翼诸王，各王均受东王[楊秀清节制](../Page/楊秀清.md "wikilink")；1852年太平軍離開廣西，1853年攻佔南京，改名天京並定都在此，正式建立了與清廷對峙的農民政權。洪秀全一面派出軍隊西征北伐，一面頒佈《[天朝田畝制度](../Page/天朝田畝制度.md "wikilink")》等政策法規，試圖達到“有田同耕，有飯同食，有衣同穿，有錢同使，無處不均勻，無人不飽暖”的理想社會。

太平天国举行第一次[科舉考试时](../Page/科舉考试.md "wikilink")，有史以来特为妇女参加考试设立女科，拔取了女[状元](../Page/状元.md "wikilink")、女[进士等](../Page/进士.md "wikilink")。

定都天京後，洪秀全主張把[四書五經列為禁書](../Page/四書五經.md "wikilink")，東王楊秀清不同意，借「天父下凡」迫洪秀全讓步，洪-{只}-好同意四書五經在修改後可以刊印流傳，然而直至太平天國滅亡仍未曾刊行。後來洪秀全又修改《[聖經](../Page/聖經.md "wikilink")》，按照政治上的需要及個人喜惡改動內容，在太平天國內頒行。

天王洪秀全與東王楊秀清的矛盾日漸加深。洪秀全知道北王[韋昌輝](../Page/韋昌輝.md "wikilink")、翼王[石達開及燕王](../Page/石達開.md "wikilink")[秦日綱對東王不滿](../Page/秦日綱.md "wikilink")，在1856年詔三人誅東王，9月發生[天京事變](../Page/天京事變.md "wikilink")，東王、北王與燕王先後被誅。翼王石達開在天京主政一段時間，為洪秀全所忌，洪秀全封自己的親兄弟[洪仁發](../Page/洪仁發.md "wikilink")、[洪仁達為王](../Page/洪仁達.md "wikilink")，以牽制石達開，引起石達開不滿。1857年石帶領大軍出走，脫離天王指揮。自天京事變及翼王出走後，洪秀全雖然掌握了朝政大權，太平天國卻開始走下坡。

### 晚年

清軍開始進迫天京，在[陳玉成和](../Page/陳玉成.md "wikilink")[李秀成等人支撐下](../Page/李秀成.md "wikilink")，太平軍在數年間擋住了清軍的多次攻勢。

1859年族弟[洪仁玕抵達天京](../Page/洪仁玕.md "wikilink")，洪秀全大喜，封仁玕為軍師、-{干}-王，名義上總理天國朝政，卻不肯把軍事權力交給他。由於洪仁玕未有立功而封王，洪秀全怕其他人不服，再次封異姓為王。

1862年英王[陈玉成被殺后](../Page/陈玉成.md "wikilink")，形勢急轉直下，天京附近據點逐一陷落。李秀成知道天京難以久守，向洪秀全建議放棄天京，轉戰中原，被洪秀全斥責。

1864年3月，天京遭到包圍后，城內糧食不足，洪秀全帶頭吃“甜露”（草團）充飢，因而致病。1864年6月1日（同治三年四月二十七日），洪秀全病逝天京。[曾國藩卻向清廷報稱洪秀全服毒自盡](../Page/曾國藩.md "wikilink")。\[5\]洪秀全死后尸体被秘密葬在[天王府的後花園内](../Page/天王府.md "wikilink")。

1864年7月30日，天京陷落之後，[湘军总兵](../Page/湘军.md "wikilink")[熊登武得到一个](../Page/熊登武.md "wikilink")[天王府黄姓](../Page/天王府.md "wikilink")[宫女告密](../Page/宫女.md "wikilink")，在她的指引下，[曾国荃派人从](../Page/曾国荃.md "wikilink")[天王府的大殿内挖出了洪秀全的尸体](../Page/天王府.md "wikilink")。8月1日，曾国藩断然下达了最严厉的惩处方式：“[戮尸](../Page/戮尸.md "wikilink")，烈火而焚之！”于是洪秀全的尸体再次被拖了出来，被刀戮、火焚。[焚尸後](../Page/焚尸.md "wikilink")，曾国藩根据湘军将领[李臣典的建议](../Page/李臣典.md "wikilink")，命人把[骨灰和以](../Page/骨灰.md "wikilink")[火药](../Page/火药.md "wikilink")，装入炮弹，然后接连发射出去，目的是要让洪秀全徹底灰飞烟灭，屍骨無存，阴魂无归，更可以使太平軍的舊部失去了精神象徵\[6\]。

## 家庭

[Dragon_robe_-_Taiping_Kingdom_History_Museum.jpg](https://zh.wikipedia.org/wiki/File:Dragon_robe_-_Taiping_Kingdom_History_Museum.jpg "fig:Dragon_robe_-_Taiping_Kingdom_History_Museum.jpg")

### 洪秀全家族的历史资料

根据古老的洪氏世系资料编修的家谱，名为《万派朝宗》，记载了从金田起义前后，一直追溯远祖自[江浙南迁到](../Page/江浙.md "wikilink")[广东的过程](../Page/广东.md "wikilink")。洪氏于[漢代](../Page/漢代.md "wikilink")、[三國时世居](../Page/三國.md "wikilink")[彭城郡](../Page/彭城郡.md "wikilink")[下邳](../Page/下邳.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[邳县](../Page/邳县.md "wikilink")），[西晋](../Page/西晋.md "wikilink")[永嘉之乱中南迁](../Page/永嘉之乱.md "wikilink")[京口](../Page/京口.md "wikilink")（今[镇江](../Page/镇江.md "wikilink")），[晋安帝时续迁](../Page/晋安帝.md "wikilink")[新安郡](../Page/新安郡.md "wikilink")[遂安县](../Page/遂安县.md "wikilink")（今[杭州市](../Page/杭州市.md "wikilink")[淳安县](../Page/淳安县.md "wikilink")），此后在各种战乱中，辗转经由江西、福建而入广东。洪秀全其祖先[洪皓是南宋时期的名臣](../Page/洪皓.md "wikilink")，洪皓曾孙[洪璞出任](../Page/洪璞.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[晋江县](../Page/晋江县.md "wikilink")[县尉](../Page/县尉.md "wikilink")，举家迁到[泉州](../Page/泉州.md "wikilink")。[元朝末年社会动荡](../Page/元朝.md "wikilink")，唯[岭南较安定](../Page/岭南.md "wikilink")，洪璞的第十世孙[洪贵生从福建迁往](../Page/洪贵生.md "wikilink")[广东](../Page/广东.md "wikilink")[潮州府](../Page/潮州府.md "wikilink")[海阳县](../Page/海阳县.md "wikilink")（今日[潮安縣](../Page/潮安縣.md "wikilink")）的[布心](../Page/布心.md "wikilink")。所以洪贵生是洪氏迁入广东的始祖。洪秀全便是洪氏迁入广东后的第十六世孙，也即是迁到[花县官禄布后的第六代](../Page/花县.md "wikilink")。

### 妻妾

共有妻妾87人，皆稱王娘。以其夢見的「天妻」為元配妻子，稱[正月宮](../Page/正月宮.md "wikilink")，正室賴蓮英稱[又正月宮](../Page/又正月宮.md "wikilink")，[妃嬪依次有](../Page/妃嬪.md "wikilink")[副月宮](../Page/副月宮.md "wikilink")、[又副月宮兩等](../Page/又副月宮.md "wikilink")。妻妾中僅五人有記載身份。

正室：

1.  又正月宫[赖莲英](../Page/赖莲英.md "wikilink")，生幼天王

妾：

1.  副月宮第四妻余氏
2.  副月宮第十二妻陈氏，生洪天光
3.  副月宮第十九妻吴氏，生洪天明
4.  黃氏

### 子女

五个儿子，一说六个

1.  [洪天貴福](../Page/洪天貴福.md "wikilink")，尊稱幼主万岁，封[王世子](../Page/王世子.md "wikilink")\[7\]，即位為幼天王
2.  洪天曾，生于1852-1853年，夭折
3.  [洪天明](../Page/洪天明_\(太平天國\).md "wikilink")，尊稱王三殿下永歲，封明王，生于1854年
4.  [洪天光](../Page/洪天光.md "wikilink")，尊稱王四殿下永歲，封光王，生于1854年
5.  [洪天佑](../Page/洪天佑.md "wikilink")，封幼东王，[过继给](../Page/过继.md "wikilink")[杨秀清](../Page/杨秀清.md "wikilink")

女儿，史料不详，至少有八人

1.  長女[洪天姣](../Page/洪天姣.md "wikilink")，尊稱天長金，夫婿天二駙馬、金王[钟万信](../Page/钟万信.md "wikilink")
2.  次女洪氏，尊稱天二金，夫婿列王[徐朗](../Page/徐朗.md "wikilink")
3.  三女洪氏
4.  四女洪氏，尊稱天四金，夫婿天四驸马[黄栋梁](../Page/黄栋梁.md "wikilink")
5.  某女洪氏，夫婿天西驸马[黄文胜](../Page/黄文胜.md "wikilink")
6.  某女洪氏，夫婿天东驸马
7.  某女洪氏
8.  八女洪氏，尊稱天八金，夫婿天八驸马

## 著作

  - 《斬邪留正詩》
  - 《太平詔書》
  - 《原道救世歌》
  - 《原道醒世訓》
  - 《原道覺世訓》

## 纪念

1961年，为纪念洪秀全，当地政府在其故居原址上复原的建筑。现在的故居建有洪秀全故居纪念馆，洪氏宗祠辟为纪念馆辅助陈列室。1988年列为[第三批全国重点文物保护单位](../Page/第三批全国重点文物保护单位.md "wikilink")，亦是省、市首批[爱国主义教育基地](../Page/爱国主义教育基地.md "wikilink")，2005年成为国家AAA级旅游景区。1991年正式对外开放。

## 參考資料

  - 羅爾綱，《太平天國史》-{卷}-四十二。

## 研究書目

  - Jonathan D. Spence（史景遷）著，朱慶葆等譯：《天國之子和他的世俗王朝》（上海：上海遠東出版社，2001）。

## 相关条目

  - [太平天国](../Page/太平天国.md "wikilink")
  - [拜上帝教](../Page/拜上帝教.md "wikilink")
  - [天父诗](../Page/天父诗.md "wikilink")

## 外部連結

  - 中国广州网：[花都洪秀全故居与洪秀全纪念馆](https://web.archive.org/web/20070926221227/http://www.guangzhou.gov.cn/node_464/node_468/node_635/node_637/2005-07/112227343661816.shtml)

{{-}}

[H洪](../Category/太平天國天王.md "wikilink")
[H洪](../Category/清朝宗教人物.md "wikilink")
[H洪](../Category/花縣人.md "wikilink")
[X秀](../Category/洪姓.md "wikilink")
[H洪](../Category/广东客家人.md "wikilink")
[Category:新興宗教創始人](../Category/新興宗教創始人.md "wikilink")

1.  韓山文，《太平天國起義記.洪氏之世系》：“洪秀全之祖先由嘉應州遷此，故均用嘉應方言。本地人稱此等客籍民為客家”
2.  羅爾綱，《太平天國史》-{卷}-四十二
3.  《洪秀全政治人格之研究》楊碧玉 秀威資訊出版 ISBN 9789862211410.
4.  [洪秀全與馮雲山共同制定《十款天條》](http://www.epochtimes.com/b5/2/8/27/c8685.htm)
5.
6.
7.  《定营规条十要》