洪-{}-都拉斯 (藝人)}}
**洪都拉斯共和國**（），通稱**洪都拉斯**，中國大陸和港澳稱**洪都-{}-拉斯**，台灣稱**宏都-{}-拉斯**，[中美洲](../Page/中美洲.md "wikilink")[共和制](../Page/共和制.md "wikilink")[國家](../Page/國家.md "wikilink")，西鄰[危地馬拉](../Page/危地馬拉.md "wikilink")，西南接[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")，東南毗[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")，東、北方濱[加勒比海](../Page/加勒比海.md "wikilink")，南臨[太平洋的洪塞加灣](../Page/太平洋.md "wikilink")。大陸以外的加勒比海上還有[天鵝群島](../Page/天鵝群島.md "wikilink")、[海灣群島等領土](../Page/海灣群島.md "wikilink")。[首都](../Page/首都.md "wikilink")[特古西加尔巴](../Page/特古西加尔巴.md "wikilink")。

## 國名

[西班牙语中](../Page/西班牙语.md "wikilink")，正式名稱为，通稱（发音：[\[onˈduɾas](../Page/Help:西班牙语国际音标.md "wikilink")\]）。中文表記為**洪都拉斯共和國**，通稱**洪都拉斯**。

「洪都拉斯」國名來歷最普遍的一說是由於航海家[克里斯託弗·哥倫布於](../Page/克里斯託弗·哥倫布.md "wikilink")1502年在該地登陸時，發現船隻無法擱淺，海底深不可測，故將其地命名「hondura」（西班牙語中「深邃」之意）。\[1\]
[明代](../Page/明代.md "wikilink")《[坤輿萬國全圖](../Page/坤輿萬國全圖.md "wikilink")》稱之為“酆度蠟”。

## 歷史

4～7世纪，洪都拉斯西部為[馬雅文明中心之一](../Page/馬雅文明.md "wikilink")。1502年[哥伦布到达洪都拉斯的](../Page/哥伦布.md "wikilink")[海灣群島](../Page/海灣群島省.md "wikilink")，為宏都拉斯與歐洲接觸之始，1524年[西班牙军队开始征服洪都拉斯](../Page/西班牙.md "wikilink")，同年沦为西班牙殖民地。1537～1539年[莱姆皮拉领导](../Page/莱姆皮拉.md "wikilink")3万印第安人举行起义，反抗西班牙殖民统治。1539年，洪都拉斯划归[瓜地馬拉都督府管辖](../Page/瓜地馬拉.md "wikilink")。

洪都拉斯于1821年9月15日宣布独立，但1822年被并入[墨西哥第一帝國](../Page/墨西哥第一帝國.md "wikilink")。1823年加入[中美洲联邦](../Page/中美洲联邦.md "wikilink")，1838年10月退出中美洲联邦，成立共和国。1840年宏都拉斯在危地马拉独裁者[拉斐尔·卡雷拉支持下](../Page/拉斐尔·卡雷拉.md "wikilink")，保守派建立独裁政府。1853年以后，洪都拉斯國內的自由派和保守派经常发生政变和内战，政权更迭频繁。金、银矿業遭到破坏、影響經濟發展甚鉅。從1840年代起，[英国侵占洪都拉斯东部地区和巴亚群岛](../Page/英国.md "wikilink")，修建铁路，并取得大片土地的租让权。

19世纪末，[美国攫取了](../Page/美国.md "wikilink")地区的银矿开采权。20世纪初，美国联合果品公司和标准果品公司霸占北部沿海平原大片土地，发展[香蕉种植园](../Page/香蕉.md "wikilink")，并垄断大部分铁路、航运、电力和香蕉出口。到1913年洪都拉斯90%以上的对外贸易为美国所控制。而在[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，美国更加紧对洪都拉斯的政治控制和经济掠夺。多次出兵干涉洪都拉斯内政，扶植傀儡政权。在联合果品公司的控制下，洪都拉斯成为以香蕉種植為主的单一经济国家，香蕉生产发展很快，到1930年香蕉出口占世界第一位。1929年世界资本主义经济危机使洪都拉斯经济受到沉重打击,群众生活下降，人民起义不断发生。[宏都拉斯国民党领袖](../Page/宏都拉斯国民党.md "wikilink")在美国的支持和策动下，于1933年攫取政权，建立独裁统治。1954年該國工人为提高工资、改善劳动条件和争取参加工会的权利举行大罢工，使美国對於垄断资本作出某些让步。

宏都拉斯自1821年獨立以來至1978年，共发生139次政变，是拉丁美洲政变最频繁的国家之一。1957年大选中由[自由党的](../Page/洪都拉斯自由党.md "wikilink")勝選擔任总统。1963年武装部队司令在美国策动下发动政变，推翻了莫拉莱斯政權，並于1965年当选总统。1971年国民党竞选获胜，但执政不久，阿雷利亚诺又一次发动政变上台。1975年武装部队司令发动政变，取代阿雷利亚诺。而1978年武装部队司令发动政变，组成以他为首的军人委员会。

2013年，宏都拉斯首都近郊一座監獄發生騷亂，造成3死、12人受傷之後，總統[洛沃下令這座主要監獄實施軍事化管理](../Page/波爾菲里奧·洛沃.md "wikilink")。\[2\]

### 足球戰爭

1969年，宏都拉斯與鄰國[薩爾瓦多因](../Page/薩爾瓦多.md "wikilink")[1970年世界盃足球賽北美外圍賽作為導火線](../Page/1970年世界盃足球賽.md "wikilink")，發生了為時僅6日的戰爭，原因則為兩國的國營傳播媒體均煽動仇視對方的國民與政治，使洪都拉斯政府下令將成千上萬的薩爾瓦多勞工驅逐出境。最後導致軍事衝突。

## 地理

[Honduras.PNG](https://zh.wikipedia.org/wiki/File:Honduras.PNG "fig:Honduras.PNG")
[Tegucigalpa_from_La_Leona.jpg](https://zh.wikipedia.org/wiki/File:Tegucigalpa_from_La_Leona.jpg "fig:Tegucigalpa_from_La_Leona.jpg")\]\]
洪都拉斯位于太平洋和加勒比海之间，主要是山地，内陆为熔岩平原，在海岸有狭窄的平原地带。气候包括从低地的[热带气候到山地的](../Page/热带.md "wikilink")[溫帶气候](../Page/溫帶.md "wikilink")。滨海地带和山地向风坡年降水量可高达3000毫米。

## 经济

洪都拉斯經濟狀態在中美洲算中下，2012年估計洪都拉斯的[人均GDP為](../Page/人均GDP.md "wikilink")2,323美元，產值最高前五種產業為咖啡栽種業、香蕉栽種業、養蝦及吳郭魚養殖業、棕櫚油業及成衣加工業\[3\]。

當地出产木料，金，银，铜，铅，锌，铁矿，锑，煤，鱼，有丰富的水力與農業資源，農產出口以咖啡、香蕉為大宗，其中美國即占宏都拉斯農產品出口值約6成，係該國重要創匯來源之一。2012年農業產值占GDP比重12.6%，製造業及服務業則分別占26.7%及60.5%\[4\]
。相較之下，宏都拉斯的[工業基礎較為缺乏](../Page/工業.md "wikilink")，宏都拉斯長期面臨高額貿易赤字、財政赤字及[外債之問題](../Page/外債.md "wikilink")，致[國際貨幣基金將之列為高負債貧窮國](../Page/國際貨幣基金.md "wikilink")。2010年元月新政府上任，採行一連串經濟成長措施，使2010年宏國經濟成長較2009年受政變影響略微恢復，達2.6%，但因宏國因原物料及民生用品進口依賴大、基礎建設不甚完善、科技發展落後，其整個經濟發展之動力來源仍須仰賴外人投資。\[5\]

2015年，宏都拉斯在僑匯持續成長與咖啡出口成長樂觀的情況下，可望走出2014年賦稅改革衝擊，預估經濟成長率約在2.5％到3.5％間\[6\]。

<File:Catacamas> center.JPG|宏都拉斯街頭 <File:San> Juancito
Honduras.jpg|首都貧民區 HSBC Bank Tegucigalpa.jpg|匯豐銀行分行 <File:CPN>
STR 22 01.jpg|馬雅遺址 <File:Comayaguela> market
Honduras.jpg|[德古斯加巴](../Page/德古斯加巴.md "wikilink")
<File:Ojojona> Honduras street 4.jpg|Ojojona市
[File:Sanpedro1.JPG|圣佩德罗苏拉](File:Sanpedro1.JPG%7C圣佩德罗苏拉)
<File:West> Bay Beach -Roatan
-Honduras-23May2009-g.jpg|宏都拉斯的[羅阿坦島](../Page/羅阿坦島.md "wikilink")

## 觀光

宏都拉斯有兩項[世界遺產](../Page/世界遺產.md "wikilink")，為[科潘的](../Page/科潘.md "wikilink")[馬雅遺址與](../Page/馬雅.md "wikilink")[普拉塔諾河生物圈保護區](../Page/普拉塔諾河生物圈保護區.md "wikilink")。\[7\]

## 人口

至止，洪都拉斯的人口總數為人。 2010年15歲以下人口的比例為36.8％，15至65歲為58.9％，65歲或以上為4.3％。\[8\]

洪都拉斯社會的種族比例為90％的[麥士蒂索人](../Page/麥士蒂索人.md "wikilink")，7％的美洲印第安人，2％的黑人和1％的白人（2017年）。\[9\]

洪都拉斯人口的男女比例為1.01。
出生比率為1.05，15-24歲比率為1.04，25-54歲比率為1.02，55-64歲比率為0.88，65歲或65歲以上比率為0.77。\[10\]2015年洪都拉斯的性別發展指數為.942，女性HDI為.600，男性為.637。\[11\]男性和女性的出生預期壽命分別為70.9和75.9。\[12\]洪都拉斯的預計受教育年限為男性10.9歲（平均6.1歲）和女性11.6歲（平均6.2歲）。\[13\]以上數字並未顯示男性和女性發展水平之間存在巨大差異，但由於性別差異，洪都拉斯的人均GNI很大。\[14\]男性的人均GNI為6,254美元，而女性僅為2,680美元。\[15\]洪都拉斯的總體GDI高於其他中等人類發展指數（HDI）國家（.871），但低於拉丁美洲和加勒比地區的GHDI（.981）。\[16\]

自1975年以來，隨著經濟移民和政治難民在其他地方尋求發展機遇，洪都拉斯的移民活動開始加速。大部分的洪都拉斯僑民居住在美國。2012年美國國務院的一項估計顯示，當時共有80萬至100萬洪都拉斯人居住在美國，佔洪都拉斯人口的近15％。\[17\]這一數字非常不確定，因為許多洪都拉斯人在沒有簽證的情況下居住在美國。在2010年的美國人口普查中，有617,392名居民被確定為洪都拉斯人，而2000年為217,569人。\[18\]

## 行政区

[HondurasDivisions.png](https://zh.wikipedia.org/wiki/File:HondurasDivisions.png "fig:HondurasDivisions.png")
[Honduras_Topography.png](https://zh.wikipedia.org/wiki/File:Honduras_Topography.png "fig:Honduras_Topography.png")
洪都拉斯一共有18省，如下：

| \!省       | 首府                                               | 2001年人口                                     | 2010年人口\[19\] | 人口變化 (%)  | 面積 (km<sup>2</sup>)\[20\] |
| --------- | ------------------------------------------------ | ------------------------------------------- | ------------- | --------- | ------------------------- |
| **1.**    | [阿特兰蒂达省](../Page/阿特兰蒂达省.md "wikilink")           | [拉塞瓦](../Page/拉塞瓦.md "wikilink")            | 344,099       | 407,551   | 18.44%                    |
| **2.**    | [喬盧特卡省](../Page/喬盧特卡省.md "wikilink")             | [喬盧特卡](../Page/喬盧特卡.md "wikilink")          | 390,805       | 459,124   | 17.48%                    |
| **3.**    | [科隆省](../Page/科隆省_\(宏都拉斯\).md "wikilink")        | [特魯希略](../Page/特魯希略_\(洪都拉斯\).md "wikilink") | 246,708       | 293,540   | 18.98%                    |
| **4.**    | [科马亚瓜省](../Page/科马亚瓜省.md "wikilink")             | [科马亚瓜](../Page/科马亚瓜.md "wikilink")          | 352,881       | 442,251   | 25.33%                    |
| **5.**    | [科潘省](../Page/科潘省.md "wikilink")                 | [聖羅莎](../Page/聖羅莎.md "wikilink")            | 288,766       | 362,226   | 25.44%                    |
| **6.**    | [科爾特斯省](../Page/科爾特斯省.md "wikilink")             | [聖佩德羅蘇拉](../Page/聖佩德羅蘇拉.md "wikilink")      | 1,202,510     | 1,570,291 | 30.58%                    |
| **7.**    | [埃爾帕拉伊索省](../Page/埃爾帕拉伊索省.md "wikilink")         | [尤斯卡蘭](../Page/尤斯卡蘭.md "wikilink")          | 350,054       | 427,232   | 22.05%                    |
| **8.**    | [弗朗西斯科-莫拉桑省](../Page/弗朗西斯科-莫拉桑省.md "wikilink")   | [特古西加爾巴](../Page/特古西加爾巴.md "wikilink")      | 1,180,676     | 1,433,810 | 21.44%                    |
| **9.**    | [格拉西亚斯-阿迪奥斯省](../Page/格拉西亚斯-阿迪奥斯省.md "wikilink") | [倫皮拉港](../Page/倫皮拉港.md "wikilink")          | 67,384        | 88,314    | 31.06%                    |
| **10.**   | [因蒂布卡省](../Page/因蒂布卡省.md "wikilink")             | [拉埃斯佩兰萨](../Page/拉埃斯佩兰萨.md "wikilink")      | 179,862       | 232,509   | 29.27%                    |
| **11.**   | [海灣群島省](../Page/海灣群島省.md "wikilink")             | [羅阿坦](../Page/罗阿坦岛.md "wikilink")           | 38,073        | 49,158    | 29.12%                    |
| **12.**   | [拉巴斯省](../Page/拉巴斯省_\(宏都拉斯\).md "wikilink")      | [拉巴斯市](../Page/拉巴斯_\(洪都拉斯\).md "wikilink")  | 156,560       | 196,322   | 25.40%                    |
| **13.**   | [倫皮拉省](../Page/倫皮拉省.md "wikilink")               | [格拉西亞斯](../Page/格拉西亞斯.md "wikilink")        | 250,067       | 315,565   | 26.19%                    |
| **14.**   | [奧科特佩克省](../Page/奧科特佩克省.md "wikilink")           | [新奧科特佩克](../Page/新奧科特佩克.md "wikilink")      | 108,029       | 132,453   | 22.61%                    |
| **15.**   | [奧蘭喬省](../Page/奧蘭喬省.md "wikilink")               | [奧蘭喬省](../Page/奧蘭喬省.md "wikilink")          | 419,561       | 509,564   | 21.45%                    |
| **16.**   | [圣巴巴拉省](../Page/圣巴巴拉省.md "wikilink")             | [圣巴巴拉省](../Page/圣巴巴拉省.md "wikilink")        | 342,054       | 402,367   | 17.63%                    |
| **17.**   | [山谷省](../Page/山谷省_\(宏都拉斯\).md "wikilink")        | [納考梅](../Page/納考梅.md "wikilink")            | 151,841       | 171,613   | 13.02%                    |
| **18.**   | [約羅省](../Page/約羅省.md "wikilink")                 | [約羅市](../Page/約羅.md "wikilink")             | 465,414       | 552,100   | 18.63%                    |
| **Total** | \-                                               | \-                                          | 6,535,344     | 8,045,990 | 23.12%                    |

\[21\]

## 政治

宏都拉斯憲法規定，洪都拉斯實行共和、民主和代議制。[國民議會](../Page/洪都拉斯國民議會.md "wikilink")(一院制)為最高立法機構。[總統是國家元首和政府首腦和武裝力量最高統帥](../Page/洪都拉斯總統.md "wikilink")。

## 軍事

[洪都拉斯軍隊現有兵員約](../Page/洪都拉斯軍隊.md "wikilink")12,000人。過去實行[義務兵役制](../Page/義務兵役制.md "wikilink")，於1995年轉為[志願兵役制](../Page/志願兵役制.md "wikilink")。1999年，洪都拉斯軍隊加入國內的治安恢復中。政府於2010年4月在東部加勒比海岸的加拉達加斯興建美國支援的新海軍基地。同時總理加瑙阿提於7月14日在加勒比海上的海灣群島發佈將在此建設新基地。

## 教育

至2004年爲止，洪都拉斯的受教育人口大約佔83.6%，淨小學入學率為94%，\[22\]2014年小學完成率為90.7%。

洪都拉斯擁有多所雙語（西班牙語和英語）和三語（西班牙語附英語、阿拉伯語和德語）學校。\[23\]洪都拉斯的高等教育由洪都拉斯國立自治大學管理，該中心位於洪都拉斯多個重要城市。洪都拉斯著名的高等學院除國立自治大學外還有[宏都拉斯天主教大學](../Page/宏都拉斯天主教大學.md "wikilink")、圣佩德罗苏拉大学、中美洲科技大学、洪都拉斯科技大学等。

## 宗教

雖然大多數洪都拉斯人名義上信奉羅馬[天主教](../Page/天主教.md "wikilink")（被認為是主要宗教），但洪都拉斯[天主教會的成員數量正在下降](../Page/天主教會.md "wikilink")，而新教教會的成員數量正在增加。2008年國際宗教自由報告指出，根據CID蓋洛普民意調查顯示，51.4％的人口認為自己信奉天主教，36.2％認為自己信奉[福音派](../Page/福音主義.md "wikilink")[新教](../Page/新教.md "wikilink")，1.3％聲稱來自其他宗教，包括[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")、[佛教](../Page/佛教.md "wikilink")、[猶太教](../Page/猶太教.md "wikilink")，[拉斯塔法里教等](../Page/拉斯塔法里运动.md "wikilink")，11.1％不屬於任何宗教或不予回應。8％的人報告是[無神論者或](../Page/無神論者.md "wikilink")[不可知論者](../Page/不可知論.md "wikilink")。\[24\]\[25\]

## 體育

[足球是宏都拉斯最受歡迎的運動](../Page/足球.md "wikilink")。[宏都拉斯國家足球隊由](../Page/宏都拉斯國家足球隊.md "wikilink")[宏都拉斯國家足球協會管理](../Page/宏都拉斯國家足球協會.md "wikilink")，曾3度打入世界盃決賽周，分別是[1982年](../Page/1982年世界盃足球賽.md "wikilink")、[2010年以及](../Page/2010年世界盃足球賽.md "wikilink")[2014年](../Page/2014年世界盃足球賽.md "wikilink")，並曾經於1993年、1995年及2011年三奪[中美洲國家盃冠軍](../Page/中美洲國家盃.md "wikilink")。

## 公共安全

由於執法資源不足，洪都拉斯内的犯罪活動猖獗，犯罪分子逍遙法外。
因此洪都拉斯是世界上謀殺率最高的國家之一。洪都拉斯國家暴力觀察站（Honduran
Observatory on National
Violence）的官方統計數據顯示，2015年洪都拉斯的殺人率為每10萬人中有60人，其中大部分殺人案件未被起訴。\[26\]

在洪都拉斯，公路襲擊和路障或檢查站劫車事件經常發生，這些事件常常由身穿警察制服的犯罪分子所爲。雖然關於外國人綁架事件的報導並不常見，但綁架受害者的家人因害怕報復經常寜願支付贖金而不會為犯罪行為報案，因此綁架人數可能會少報。\[27\]

由於政府和企業在2014年採取措施改善旅遊安全，羅丹和海灣群島的犯罪率低於洪都拉斯大陸。\[28\]

在人口較少的格拉西亞斯-阿迪奧斯地區，販毒活動猖獗，警察數量較少。[毒販和其他犯罪組織對美國公民的威脅導致美國大使館限制美國官員在該地區的活動](../Page/毒販.md "wikilink")。\[29\]

## 參考資料

<references/>

## 外部連結

  - [宏都拉斯政府官方網站](https://web.archive.org/web/20080911172930/http://www.gob.hn/)


  -

  -

  -

[Category:中美洲國家](../Category/中美洲國家.md "wikilink")
[Category:宏都拉斯](../Category/宏都拉斯.md "wikilink")
[Category:1821年建立的國家或政權](../Category/1821年建立的國家或政權.md "wikilink")

1.  [拉丁美洲各國國名的由來](http://gdfao.southcn.com/wszs/cgcs/200607260042.htm)

2.  [宏都拉斯監獄進行軍事化管理](http://tw.news.yahoo.com/%E7%99%BC%E7%94%9F%E6%9A%B4%E5%8B%95-%E5%AE%8F%E9%83%BD%E6%8B%89%E6%96%AF%E7%9B%A3%E7%8D%84%E9%80%B2%E8%A1%8C%E8%BB%8D%E4%BA%8B%E5%8C%96%E7%AE%A1%E7%90%86-014937753.html%7C發生暴動)

3.  [宏都拉斯投資環境簡介 Investment Guide to Honduras 經濟部投資業務處
    編印](http://fta.trade.gov.tw/pimage/20140320154457023.pdf)

4.  [宏都拉斯國情摘要](http://www.eximclub.com.tw/countries/info_print02.asp?idno=2183&continen=2&country=%A7%BB%B3%A3%A9%D4%B4%B5)

5.  [台商網- 宏都拉斯-
    經濟環境](http://twbusiness.nat.gov.tw/countryPage.do?id=10&country=HN)

6.  [宏都拉斯經濟看好
    今年GDP約3%](http://www.ksnews.com.tw/newsdetail_ex.php?n_id=0000713436&level2_id=114)

7.  [Honduras - UNESCO World Heritage
    Centre](http://whc.unesco.org/en/statesparties/HN/)

8.

9.

10.
11.

12.
13.
14.
15.
16.
17.

18.

19. [Honduran National Institute of
    Statistics](http://www.ine.gob.hn/drupal/node/205)

20. [Statoids.com entry for Honduras](http://www.statoids.com/uhn.html).

21.

22.

23.

24.

25.

26.

27.
28.
29.