**洪都拉斯自由党**（[西班牙语](../Page/西班牙语.md "wikilink")：Partido Liberal de
Honduras）是[洪都拉斯的一个自由主义](../Page/洪都拉斯.md "wikilink")[政党](../Page/政党.md "wikilink")，1891年成立。该党是[国际自由联盟的一个成员](../Page/国际自由联盟.md "wikilink")。洪都拉斯自由党用红色来识别，其对手[洪都拉斯国民党是蓝色](../Page/洪都拉斯国民党.md "wikilink")。洪都拉斯的所有[民主选举都是在这两个党之间进行的](../Page/民主.md "wikilink")。

2001年11月25日举行的[立法选举中](../Page/立法.md "wikilink")，该党赢得40.8%的选票，在[洪都拉斯国会的](../Page/洪都拉斯国会.md "wikilink")128个席位中赢得55席。在[洪都拉斯总统选举中](../Page/洪都拉斯总统.md "wikilink")，它的候选人[拉斐尔·皮内达·庞塞赢得](../Page/拉斐尔·皮内达·庞塞.md "wikilink")44.3%的选票，但被洪都拉斯国民党的[里卡多·马杜罗击败](../Page/里卡多·马杜罗.md "wikilink")。

在[2013年大选](../Page/2013年洪都拉斯大选.md "wikilink")，該黨的總統候選人[毛里西奧·比列達在總統選舉得票排第](../Page/毛里西奧·比列達.md "wikilink")3落敗，該黨在國會選舉則取得27席。

## 參考來源

[Category:洪都拉斯政党](../Category/洪都拉斯政党.md "wikilink")
[Category:自由主義政黨](../Category/自由主義政黨.md "wikilink")