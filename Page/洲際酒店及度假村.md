[HK_InterContinentalGrandStanfordHongKong.JPG](https://zh.wikipedia.org/wiki/File:HK_InterContinentalGrandStanfordHongKong.JPG "fig:HK_InterContinentalGrandStanfordHongKong.JPG")海景嘉福酒店\]\]
**洲際酒店及度假村**（）是一個豪華[酒店](../Page/酒店.md "wikilink")[連鎖品牌](../Page/連鎖店.md "wikilink")，為[洲際酒店集團所擁有](../Page/洲際酒店集團.md "wikilink")，在世界超過60個國家經營183家酒店。

## 歷史

洲際酒店創立於1946年，同年第一家酒店於[巴西](../Page/巴西.md "wikilink")[貝倫開幕](../Page/貝倫_\(巴西\).md "wikilink")。1981年[洲際酒店集團控股公司賣給](../Page/洲際酒店集團.md "wikilink")[Grand
Metropolitan](../Page/Grand_Metropolitan.md "wikilink")。[Grand
Metropolitan為了專注於核心產業於](../Page/Grand_Metropolitan.md "wikilink")1988年將[洲際酒店集團控股公司賣給](../Page/洲際酒店集團.md "wikilink")[日本西武流通集團](../Page/日本.md "wikilink")。1998年被[Bass
Hotels &
Resorts收購](../Page/Bass_Hotels_&_Resorts.md "wikilink")，成為今日的[洲際酒店集團](../Page/洲際酒店集團.md "wikilink")。

## 全球分佈

### 亞洲

<File:Zifeng> Tower.jpg|[南京](../Page/南京.md "wikilink")
<File:InterContinental> Suzhou.jpg|[苏州](../Page/苏州.md "wikilink")
<File:Hangzhou> International Conference Center
1.JPG|[杭州](../Page/杭州.md "wikilink") <File:Baili> Center.jpg|
[合肥](../Page/合肥.md "wikilink") <File:InterContinental> Kunming Hotel
(20180212190002).jpg| [昆明](../Page/昆明.md "wikilink") <File:HK>
InterContinental FrontView.JPG|[香港洲際酒店](../Page/香港洲際酒店.md "wikilink")
<File:InterContinentalNewDelhi.jpg>|[新德里](../Page/新德里.md "wikilink")
<File:View> from Intercontinental hotel in
Hanoi.jpg|[河內市](../Page/河內市.md "wikilink")
<File:InterContinental> Hotel,
Singapore.JPG|[新加坡](../Page/新加坡.md "wikilink")
<File:InterContinental> Hotel
Yokohama.jpg|[橫濱市](../Page/橫濱市.md "wikilink")

### 北美洲

<File:Willard> Intercontinental
Hotel.JPG|[華盛頓哥倫比亞特區](../Page/華盛頓哥倫比亞特區.md "wikilink")
<File:Construction> of the InterContinental Hotel San Francisco
2007-08-12.jpg|[舊金山](../Page/舊金山.md "wikilink") <File:Hotel>
Intercontinental Miami 20100707.jpg|[邁阿密](../Page/邁阿密.md "wikilink")
<File:20070509> Intercontinental.JPG|[芝加哥](../Page/芝加哥.md "wikilink")
<File:Inter-Continental> The Barclay E43 NYC
jeh.jpg|[紐約](../Page/紐約.md "wikilink")

### 歐洲及大洋洲

<File:AmstelHotelAmsterdam.jpg>|[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")
<File:Continental> Hotel Belgrade.jpg|[貝爾格勒](../Page/貝爾格勒.md "wikilink")
<File:InterContinental> Warszawa.JPG|[華沙](../Page/華沙.md "wikilink")
<File:Hotel> Intercontinental Frankfurt DSC
5287.jpg|[美因河畔法蘭克福](../Page/美因河畔法蘭克福.md "wikilink")
<File:Cannes> - hotel CARLTON.jpg|[坎城](../Page/坎城.md "wikilink")
<File:Hotel> Intercontinental Vienna August
2006.jpg|[維也納](../Page/維也納.md "wikilink")

## 參考資料

## 外部連結

  - [洲際酒店](http://www.intercontinental.com/)

[Category:旅馆品牌](../Category/旅馆品牌.md "wikilink")
[Category:洲際酒店集團](../Category/洲際酒店集團.md "wikilink")