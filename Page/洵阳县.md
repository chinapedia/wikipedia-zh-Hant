**洵阳县**，[中国旧](../Page/中国.md "wikilink")[县名](../Page/县.md "wikilink")。

秦朝时于旬水入汉水处设旬关（在今[陕西省](../Page/陕西省.md "wikilink")[旬阳县老城](../Page/旬阳县.md "wikilink")），置旬阳县。东汉时废入[西城县](../Page/西城县_\(西汉\).md "wikilink")。[西晋](../Page/西晋.md "wikilink")[太康四年](../Page/太康_\(西晋\).md "wikilink")（283年）复置\[1\]，[治所在今旬阳县北汉水北岸](../Page/治所.md "wikilink")。属[魏兴郡](../Page/魏兴郡.md "wikilink")。[北魏为](../Page/北魏.md "wikilink")[洵阳郡治](../Page/洵阳郡.md "wikilink")。[南朝宋](../Page/南朝宋.md "wikilink")、[齐为](../Page/南朝齐.md "wikilink")**旬**阳县，[西魏为](../Page/西魏.md "wikilink")“洵阳县”，为[洵州](../Page/洵州.md "wikilink")、[洵阳郡治](../Page/洵阳郡.md "wikilink")。[北周为洵阳郡治](../Page/北周.md "wikilink")。[隋朝时](../Page/隋朝.md "wikilink")，先属[金州](../Page/金州.md "wikilink")，后属[西城郡](../Page/西城郡.md "wikilink")。[唐朝](../Page/唐朝.md "wikilink")、[宋朝属金州](../Page/宋朝.md "wikilink")。[元朝时废](../Page/元朝.md "wikilink")。[明朝](../Page/明朝.md "wikilink")[洪武三年](../Page/洪武.md "wikilink")（1370年）复置，治今旬阳县。先后属[四川省](../Page/四川省.md "wikilink")[夔州府](../Page/夔州府.md "wikilink")[大宁州](../Page/大宁州.md "wikilink")、金州、[兴安州](../Page/兴安州_\(明朝\).md "wikilink")。[清朝属](../Page/清朝.md "wikilink")[兴安府](../Page/興安府.md "wikilink")。

[辛亥革命爆发后](../Page/辛亥革命.md "wikilink")，洵阳县[知事](../Page/知事.md "wikilink")[白玉昆被驱逐](../Page/白玉昆.md "wikilink")。11月，陕西军政府南路安抚招讨使[张宝麟率部由](../Page/张宝麟.md "wikilink")[白河县西上](../Page/白河县.md "wikilink")，以方斌为洵阳县知事。民国初年，府废，属[汉中道](../Page/汉中道.md "wikilink")。1933年，道废，直属[陕西省](../Page/陝西省_\(中華民國\).md "wikilink")。1935年，属第五行政督察专员公署\[2\]。

1948年4月，[解放军第一次攻占洵阳县](../Page/中国人民解放军.md "wikilink")。至8月，县境分属中华民国洵阳县政府和[中共政权领导的上关县人民民主政府](../Page/中共政权.md "wikilink")。当年，双方武装在洵阳县东区蜀河、双河争夺激烈。1949年5月，中共政权在湖北省[郧西县境成立洵阳县民主政府](../Page/郧西县.md "wikilink")。7月13日，解放军由湖北西进，攻占洵阳县东区。8月，洵阳县民主政府迁移至东区蜀河。10月，中华人民共和国成立。11月26日，[解放军第19军](../Page/中国人民解放军第19军.md "wikilink")57师攻占洵阳县城。至此，[中华民国政府对此地管辖结束](../Page/中華民國政府.md "wikilink")。洵阳县属[陕南行署区安康分区](../Page/陕南行署区.md "wikilink")。1950年，[陕西省人民政府成立](../Page/陕西省人民政府.md "wikilink")，洵阳县属[安康专区](../Page/安康专区.md "wikilink")。1959年，属[安康地区](../Page/安康地区.md "wikilink")。1964年，改名[旬阳县](../Page/旬阳县.md "wikilink")\[3\]。

## 注释

[Category:西魏县份](../Category/西魏县份.md "wikilink")
[Category:北周县份](../Category/北周县份.md "wikilink")
[Category:隋朝县份](../Category/隋朝县份.md "wikilink")
[Category:唐朝县份](../Category/唐朝县份.md "wikilink")
[Category:五代十国县份](../Category/五代十国县份.md "wikilink")
[Category:宋朝县份](../Category/宋朝县份.md "wikilink")
[Category:元朝县份](../Category/元朝县份.md "wikilink")
[Category:明朝县份](../Category/明朝县份.md "wikilink")
[Category:清朝县份](../Category/清朝县份.md "wikilink")
[Category:中华民国陕西省县份](../Category/中华民国陕西省县份.md "wikilink")
[Category:已撤消的中华人民共和国陕西省县份](../Category/已撤消的中华人民共和国陕西省县份.md "wikilink")
[Category:安康行政区划史](../Category/安康行政区划史.md "wikilink")
[Category:旬阳县](../Category/旬阳县.md "wikilink")
[Category:280年代建立的行政区划](../Category/280年代建立的行政区划.md "wikilink")
[Category:1964年废除的行政区划](../Category/1964年废除的行政区划.md "wikilink")

1.
2.

3.