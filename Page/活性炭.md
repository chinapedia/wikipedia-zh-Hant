[Activated_Carbon.jpg](https://zh.wikipedia.org/wiki/File:Activated_Carbon.jpg "fig:Activated_Carbon.jpg")
**活性炭**（），亦稱**活性碳**（）、**活化炭**（；）或**活化碳**（），是黑色粉末状或颗粒状的[碳物質](../Page/无定形碳#礦物學.md "wikilink")。活性炭在结构上由于微晶碳是不规则排列，在交叉连接之间有细孔，在[活化时会产生碳组织缺陷](../Page/活化.md "wikilink")，因此它是一种多孔碳，堆积密度低，比表面积大，也是做一個過濾器的主要物料。

## 历史

在20世纪初活性炭作为[专利被发明之前](../Page/专利.md "wikilink")，历史上有[文献记载与许多提法的更多的是关于](../Page/文献.md "wikilink")[木炭应用的历史](../Page/木炭.md "wikilink")。

公元前3750年，已知最早使用木炭的是[埃及人和](../Page/埃及.md "wikilink")[苏美尔人](../Page/苏美尔.md "wikilink")。

公元前1550年，[古埃及有](../Page/古埃及.md "wikilink")的记载。[古希腊名医](../Page/古希腊.md "wikilink")[希波克拉底](../Page/希波克拉底.md "wikilink")（公元前460-359）及[古羅馬科學家](../Page/羅馬帝國.md "wikilink")[老普林尼均有記載用木炭治疗](../Page/老普林尼.md "wikilink")[癲癇和](../Page/癲癇.md "wikilink")[炭疽病的療法](../Page/炭疽病.md "wikilink")。\[1\]

公元前450年的[腓尼基商船](../Page/腓尼基.md "wikilink")，淡水被储存在烧焦的木桶里以抑制細菌滋生及變質，此法一直被沿用至18世紀。於同一时期的[孔雀帝國](../Page/孔雀帝國.md "wikilink")，[印度教的宗教文件中提到利用沙子和木炭过滤和净化](../Page/印度教.md "wikilink")[恒河河水以作飲用](../Page/恒河.md "wikilink")。

157年，克劳迪乌斯医疗论文中提到了[蔬菜和](../Page/蔬菜.md "wikilink")[动物来源制备的木炭](../Page/动物.md "wikilink")，用于治疗多种疾病。

[中国](../Page/中国.md "wikilink")[明代](../Page/明朝.md "wikilink")[李时珍](../Page/李时珍.md "wikilink")（公元1518-1593年）所编著的[本草纲目中提及木炭用于治疗疾病](../Page/本草纲目.md "wikilink")。

1773年，[卡尔·威廉·舍勒通过大量实验发现木炭的](../Page/卡尔·威廉·舍勒.md "wikilink")[吸附能力并且可以吸附各种](../Page/吸附.md "wikilink")[气体](../Page/气体.md "wikilink")。

1777年，报道了木炭热效应与吸附气体的能力，导致后来的“[冷凝吸附理论](../Page/冷凝吸附理论.md "wikilink")”
的提出。

1785年，舍勒研究了木炭吸附气体，其吸附能力从蒸气到一系列的[有机化学物质以及各种水溶液中使用木炭](../Page/有机化学.md "wikilink")[脱色](../Page/脱色.md "wikilink")，特别是生产[酒石酸的商业应用](../Page/酒石酸.md "wikilink")。这似乎是第一次系统地考虑到在[液相上木炭的吸附](../Page/液相.md "wikilink")。在这个时候，制糖行业一直在寻找一种有效的[糖浆脱色的方法](../Page/糖浆.md "wikilink")。但是，木材木炭在这个时候并没有特别有效的发挥这一作用，大概是因为孔隙度开发的程度尚未达到糖浆脱色所用木炭的程度的要求。

1794年，[英国一家糖厂成功的生产出使用木炭脱色的糖浆](../Page/英国.md "wikilink")。1805年，[法国利用木炭脱色第一次大规模生产使用](../Page/法国.md "wikilink")[甜菜制备的糖浆](../Page/甜菜.md "wikilink")。1805年至1808年，Delessert在甜菜[酿酒中成功的使用木炭脱色](../Page/酿酒.md "wikilink")。1815年，大部分制糖行业已转用颗粒状骨炭作为[脱色剂](../Page/脱色剂.md "wikilink")。

1822年，Bussy表明，影响活性炭脱色性能的除了固有的原始材料，还取决于热加工和颗粒大小的成品。他表明，[炭化过高温度或过长](../Page/炭化.md "wikilink")，降低了吸附性能和孔隙度，虽然他没有办法衡量这一[因素](../Page/因素.md "wikilink")。
这是第一次记录活性炭生产的热和化学过程。

1841年，斯加登在加热再生的骨碳之前系统化的使用[盐酸](../Page/盐酸.md "wikilink")[酸洗](../Page/酸洗.md "wikilink")。这有效地消除了[矿物盐吸附的碳](../Page/矿物盐.md "wikilink")。他还介绍了在德国的第一个连续立窑生产以及再生骨碳的过程。

1854年，豪斯介绍了成功应用于[伦敦](../Page/伦敦.md "wikilink")[下水道系统过滤器中去除](../Page/下水道.md "wikilink")[蒸气和气体中的杂质的碳](../Page/蒸气.md "wikilink")。1862年，Lipscombe制备出了使用碳净化的[饮用水](../Page/饮用水.md "wikilink")。

1865年，猎人发现了使用椰子壳为原料的炭具有很好的气体吸附性能。1881年，凯泽尔首次使用'吸附'这个词来形容吸收气体的碳。

1901年，Raphael von
Ostrejko发明以[金屬氯化物](../Page/金屬鹵化物.md "wikilink")[炭化](../Page/炭化.md "wikilink")[植物源原料或用](../Page/植物.md "wikilink")[二氧化碳或](../Page/二氧化碳.md "wikilink")[水蒸气与](../Page/水蒸气.md "wikilink")[炭化材料反应制造活性炭](../Page/炭化材料.md "wikilink")，并先后取得英国和德国专利。

1911年，[奥地利的一家工厂生产出活性炭](../Page/奥地利.md "wikilink")，[商标名称为Eponit](../Page/商标.md "wikilink")。

1914年至1918年，[第一次世界大战有](../Page/第一次世界大战.md "wikilink")[毒气体进入战场](../Page/毒气.md "wikilink")，颗粒活性炭作为吸附剂得到规模化大量生产用于[军事用途的](../Page/军事.md "wikilink")[防毒面具](../Page/防毒面具.md "wikilink")。

一戰以後，战时发展大规模的活性炭生產导致战后活性炭商业化生产及应用。在[欧洲制造活性炭的新原料取得了很大进展](../Page/欧洲.md "wikilink")。加入椰子、杏仁壳及[氯化锌生产出的活性炭具有较高的](../Page/氯化锌.md "wikilink")[机械性和吸附气体和蒸气的能力](../Page/机械性.md "wikilink")。

1935-1940年，在[捷克斯洛伐克通过木屑及氯化锌作](../Page/捷克斯洛伐克.md "wikilink")[活化劑生产活性炭](../Page/催化劑.md "wikilink")，用于清除、回收意外洩漏的[挥发性](../Page/挥发性有机物.md "wikilink")[溶剂和](../Page/溶剂.md "wikilink")[煤气等有害氣體](../Page/煤气.md "wikilink")。

## 种类

[Aktivkohlerp.jpg](https://zh.wikipedia.org/wiki/File:Aktivkohlerp.jpg "fig:Aktivkohlerp.jpg")
由于原料来源、制造方法、外观形状和应用场合不同，活性炭的种类很多，到目前为止尚无精确的统计材料，大约有上千个品种。

活性炭的孔隙半径大小可分为：

  - 大孔：半径 \> 20,000nm
  - 过渡孔：半径150 ～ 20,000nm
  - 微孔：半径 \< 150nm

### 按原料来源分

  - 木质活性炭
  - [兽骨](../Page/骨炭.md "wikilink")、血炭
  - 矿物质原料活性炭
  - 其它原料的活性炭
  - 再生活性炭

### 按活化方法分

  - 化学法活性炭（化学炭）
  - 物理法活性炭
  - 化学－物理法活性炭
  - 物理－化学法活性炭

### 按外观形状分

  - 粉状活性炭（Powdered activated carbon, PAC）
  - 颗粒活性炭（Granular activated carbon, GAC）
  - 成形活性碳（Extruded activated carbon, EAC ）
      - 圓柱形活性炭
      - 球形活性炭（Bead activated carbon, BAC）
  - 活性炭纤维
  - 蜂窝活性炭
  - 其它形状的活性炭

## 活化方法

主要活化方法：

1.  木炭、果壳炭、[煤等原料经造粒后](../Page/煤.md "wikilink")，在1000℃下用水蒸气、[二氧化碳](../Page/二氧化碳.md "wikilink")、进行活化的气体活化法。
2.  干燥后的原料用[氯化锌](../Page/氯化锌.md "wikilink")[溶液浸渍](../Page/溶液.md "wikilink")，混合，在500\~700℃下加热，进行碳化或活化，称为药剂活化法。

活性炭的吸附性减弱后，可以再生。把活性炭置于容器里，通入一定壓力的水蒸气，然后在一定量[氧气存在下](../Page/氧气.md "wikilink")，加热到400
℃，以除掉表面上的吸附物质。

## 用途

[Activated_carbon.JPG](https://zh.wikipedia.org/wiki/File:Activated_carbon.JPG "fig:Activated_carbon.JPG")
活性炭是疏水性的吸附剂，具有对非极性物质有选择性吸附的特性，还具有由碳表面的[官能团产生的](../Page/官能团.md "wikilink")[催化作用和碳本身作为反应物质的性质](../Page/催化作用.md "wikilink")。关于它的反应机理现在还有许多不清楚的地方。

活性炭的用途很多。广泛应用于几乎所有的国民经济部门和日常生活。**粉末炭**可用于液相脱色，脱臭精制，上下水净化。**粒状炭**应用于气相吸附，[溶剂回收](../Page/溶剂.md "wikilink")，空气净化，[香烟滤嘴](../Page/香烟.md "wikilink")，此外还可用于[氯乙烯](../Page/氯乙烯.md "wikilink")、[醋酸乙烯合成](../Page/醋酸乙烯.md "wikilink")[催化剂](../Page/催化剂.md "wikilink")，[贵金属催化剂的](../Page/贵金属催化剂.md "wikilink")[载体](../Page/催化剂.md "wikilink")。

  - 氣體淨化

<!-- end list -->

1.  [空气净化](../Page/空氣淨化器.md "wikilink")

2.  [口罩](../Page/口罩.md "wikilink")

3.
4.  化工品储存排气净化

5.  汽车[尾气净化](../Page/尾气.md "wikilink")

6.  PTA氧化装置净化气体

7.  污水處理场排气吸附

<!-- end list -->

  - 水處理

<!-- end list -->

1.  饮料水处理
2.  电厂水预处理
3.  废水回收前处理
4.  生物法[污水處理](../Page/污水處理.md "wikilink")
5.  有毒废水处理

<!-- end list -->

  - 工業製程

<!-- end list -->

1.  化學[催化剂](../Page/催化剂.md "wikilink")[载体](../Page/催化剂.md "wikilink")

2.
3.  石化无碱脱硫醇

4.  溶剂回收

5.  制糖、酒类、味精医药、食品精制、脱色

6.  乙烯脱盐水填料

<!-- end list -->

  - 医学

<!-- end list -->

1.  可用来吸附口服毒物和

## 參見

  - [生物炭](../Page/生物炭.md "wikilink")
  - [木炭](../Page/木炭.md "wikilink")

## 參考資料

## 外部链接

  - [History of Carbon, University of
    Kentucky](http://www.caer.uky.edu/carbon/history/carbonhistory.shtml)

[Category:炭](../Category/炭.md "wikilink")
[Category:过滤器](../Category/过滤器.md "wikilink")
[Category:毒理学治疗](../Category/毒理学治疗.md "wikilink")
[Category:赋形剂](../Category/赋形剂.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.  [1](http://www.caer.uky.edu/carbon/history/carbonhistory.shtml)