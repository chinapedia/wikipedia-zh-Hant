**活機器**（****）又称**活的机器**、**生活机器**或**生命机器**，它是一種[廢水處理的設計形式](../Page/廢水處理.md "wikilink")，以模仿[濕地的清潔功能](../Page/濕地.md "wikilink")。他們是密集的[生物修復系統](../Page/生物修復.md "wikilink")，也可以產生有益的副產物，如[甲烷煤氣](../Page/甲烷.md "wikilink")，食用和觀賞植物，以及[魚](../Page/魚.md "wikilink")。水產品和[濕地植物](../Page/濕地.md "wikilink")，[細菌](../Page/細菌.md "wikilink")，[藻類](../Page/藻類.md "wikilink")，[原生動物](../Page/原生動物.md "wikilink")，[浮游生物](../Page/浮游生物.md "wikilink")，[蝸牛](../Page/蝸牛.md "wikilink")，[蛤](../Page/蛤.md "wikilink")，[魚和其他生物體是用來在系統中](../Page/魚.md "wikilink")，提供具體的清洗或[營養功能](../Page/營養.md "wikilink")。在[溫帶氣候下](../Page/溫帶氣候.md "wikilink")，該系統的水箱，水管和過濾器，置於一個溫室，以提高溫度，以及生物活動的頻率。生活機器初步的發展，一般是歸功於[約翰托德](../Page/約翰托德.md "wikilink")，後來在現已不運作的[新煉金術中心研究出](../Page/新煉金術中心.md "wikilink")[Bioshelter的概念](../Page/:en:Bioshelter.md "wikilink")。

**生活機器**，是[新墨西哥](../Page/新墨西哥.md "wikilink")[生活設計集團有限責任公司](http://www.livingdesignsgroup.com/green-engineering-and-living-m/)
，的註冊商標。生活機器屬於新興學科的[生態工程](../Page/生態工程.md "wikilink")，並有許多相似的系統在[歐洲建立](../Page/歐洲.md "wikilink")，在當地被稱為“活機器”。

## 設計理論

居住機器的規格甚廣，由後院實驗到大型的[公共工程](../Page/公共工程.md "wikilink")。一些生活機器在幾個生態自覺的村莊運作並處理[廢水](../Page/廢水.md "wikilink")，如[芬德霍恩社區](../Page/芬德霍恩基金會.md "wikilink")，[蘇格蘭](../Page/蘇格蘭.md "wikilink")。
\[1\]，部分生活機器處理半都市區的混和污水，像[南伯靈頓·佛蒙特](../Page/南伯靈頓·佛蒙特.md "wikilink")\[2\]
每個系統的設計是專門處理一定數量的水，每天運作，而該系統也能接受一些特殊的水體。舉例來說，如果進水含有高水平的[重金屬](../Page/重金屬.md "wikilink")
，居住機器的設計必須包括適當[生物來積累重金屬](../Page/生物.md "wikilink")。 \[3\]
在"春季時，有相當高濃度的[毒素會累積在水裡](../Page/毒素.md "wikilink")。這一突如其來的[毒素提升是一個快速累積的例子](../Page/毒素.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:水处理](../Category/水处理.md "wikilink")

1.
2.  [1](http://www.epa.gov/owmitnet/mtb/living_machine.pdf)
3.  Todd, Nancy J. 2005, A Safe and Sustainable World: The promise of
    Ecological Design. Island Press, Washington D.C.