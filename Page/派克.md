[Sonnet_parker_3n06.jpg](https://zh.wikipedia.org/wiki/File:Sonnet_parker_3n06.jpg "fig:Sonnet_parker_3n06.jpg")

**派克筆公司**（）於1888年由在[美国](../Page/美国.md "wikilink")[威斯康辛州](../Page/威斯康辛州.md "wikilink")成立，因製造世界眾多著名或具收集性的筆(如多福、51等)而著称。

## 歷史

創始人喬冶‧派克曾是的銷售代理。他於1889年獲得首個[鋼筆相關的](../Page/鋼筆.md "wikilink")[專利](../Page/專利.md "wikilink")。1894年派克的"Lucky
Curve"專利获准，该专利用於在鋼筆不使用時將多餘的墨水送回筆中。Lucky Curve被用於不同的形狀直至1928年。

從20年代到60年代直到[原子筆普及前](../Page/原子筆.md "wikilink")，派克的書寫工具銷量居世界前列。1931年派克发明了[Quink](../Page/:en:Quink.md "wikilink")"快速變乾墨水"，用來防止墨渍污染，並使得[Parker
51成為鋼筆發展歷史中使用最廣泛的型號](../Page/:en:Parker_51.md "wikilink")(在其30年歷史中銷售額總共超過40億)。製造設備設立在[加拿大](../Page/加拿大.md "wikilink")、[英格蘭](../Page/英格蘭.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[法國](../Page/法國.md "wikilink")、[墨西哥與](../Page/墨西哥.md "wikilink")[阿根廷多年](../Page/阿根廷.md "wikilink")。派克筆經常用來簽署重要文件例如[第二次世界大戰停戰協議](../Page/第二次世界大戰.md "wikilink")，而有時會有紀念版本發行。

1976年，派克在臨時職工市場发达的時候擁有[万宝盛华公司](../Page/万宝盛华.md "wikilink")。當時万宝盛华獲得的收入超过其文具业。

1987年一次管理層融資收購將公司總部移到[英格蘭](../Page/英格蘭.md "wikilink")[东萨塞克斯郡](../Page/东萨塞克斯郡.md "wikilink")[纽黑文
(东萨塞克斯郡)](../Page/纽黑文_\(东萨塞克斯郡\).md "wikilink")。1993年派克由[吉列公司所有](../Page/吉列公司.md "wikilink")，吉列公司當時已擁有[PaperMate品牌](../Page/:en:PaperMate.md "wikilink")，其中一種最暢銷的可替換原子筆。吉列公司於2000年將書寫工具部門出售給[諾威屈伯德股份](../Page/諾威屈伯德股份.md "wikilink")，一家擁有書寫工具分部Sanford的公司，擁有世上最多品牌如Rotring、Sharpie、Reynolds以及派克、PaperMate、Waterman(華特曼)與Liquid
Paper。

## 型號

公司歷史中關鍵的型號包括：

  - Jointless (1899年)
  - Jack Knife Safety (1909年)
  - Duofold世纪系列 (1921年)
  - Parker Vacumatic (1932年)
  - Parker 51派克51 (1941年)
  - Jotter(1954年)
  - Parker 61派克61 (1956年)
  - Parker 75(1964年)
  - Duofold International(1987年)
  - Sonnet 卓尔(商籟)系列(1993年)
  - Parker Premier 派克首席
  - Parker 100 派克100(2004\~2006)
  - Parker Frontier 派克云峰
  - Parker Urban 派克都市
  - Parker IM 派克IM
  - Parker Vector 派克威雅
  - Parker Premier 首席(尊爵)
  - Parker Duofold Prestige (2016)

## 外部連結

  - [Parker](http://cn.parker5thchina.com/) 官方網站
  - [ContraSistema Parker
    Pens](https://web.archive.org/web/20140517064452/http://contrasistema.com/)
    全部收藏品
  - [Parker Incentives](http://www.parkerincentives.co.uk) 派克筆的最大供應商
  - [parkerpens.net](http://www.parkerpens.net) 非官方的愛好者網站，擁有大量關於古老派克筆的資訊
  - [Parker 51 Special Edition](http://www.parker-51.com/)
    最初為重新發行的[Parker
    51官方網站](../Page/Parker_51.md "wikilink")，但似乎亦有其他關於派克筆的新聞
  - [www.parker75.com](http://www.parker75.com/) 大量關於從Parker 75至今資料的嗜好網站
  - [PenHero.com: Pengallery:
    Parker](http://www.penhero.com/PenGallery/Parker/Parker.htm)
  - [Pen Lovers: Parker Flighters
    Collection](http://www.penlovers.com/index.cfm?t=collections&s=flighter)
    來自大量派克系列中所有種類的不銹鋼鋼筆

[Category:書寫工具](../Category/書寫工具.md "wikilink")
[Category:美国公司](../Category/美国公司.md "wikilink")
[Category:1888年成立的公司](../Category/1888年成立的公司.md "wikilink")
[Category:钢笔及墨水制造商](../Category/钢笔及墨水制造商.md "wikilink")