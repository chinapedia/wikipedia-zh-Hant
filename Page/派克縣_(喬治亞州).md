**派克縣**（**Pike County,
Georgia**）是[美國](../Page/美國.md "wikilink")[喬治亞州西部的一個縣](../Page/喬治亞州.md "wikilink")。面積568平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,688人。縣治[紀伯倫](../Page/紀伯倫_\(喬治亞州\).md "wikilink")（Zebulon）。

成立於1822年12月9日。縣名紀念美國探險家[紀伯倫·蒙哥馬里·派克](../Page/紀伯倫·蒙哥馬里·派克.md "wikilink")。

[P](../Category/喬治亞州行政區劃.md "wikilink")