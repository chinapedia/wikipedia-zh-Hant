[PresqueIsleStatePark.JPG](https://zh.wikipedia.org/wiki/File:PresqueIsleStatePark.JPG "fig:PresqueIsleStatePark.JPG")
[PresqueIsleLighthouse.jpg](https://zh.wikipedia.org/wiki/File:PresqueIsleLighthouse.jpg "fig:PresqueIsleLighthouse.jpg")

**派斯克小島州立公園**（Presque Isle State
Park），[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[州立公園](../Page/州立公園.md "wikilink")，佔地3200[英畝](../Page/英畝.md "wikilink")，位於[伊利湖的一個含沙半島](../Page/伊利湖.md "wikilink")，鄰近[伊利](../Page/伊利_\(賓夕法尼亞州\).md "wikilink")。每年有4百萬個訪客。訪客常常在公園內到沙灘遊玩、[游泳](../Page/游泳.md "wikilink")、[遠足](../Page/遠足.md "wikilink")、踏[單車](../Page/單車.md "wikilink")、[線型滑冰和](../Page/線型滑冰.md "wikilink")[觀鳥](../Page/觀鳥.md "wikilink")。公園包含13英哩馬路和21英哩的郊遊徑。[奧利弗·哈澤德·佩裡](../Page/奧利弗·哈澤德·佩裡.md "wikilink")（在1812年戰爭期間的一位[海軍](../Page/海軍.md "wikilink")[少校](../Page/少校.md "wikilink")）的一座紀念碑矗立在半島遠遠的邊陲。

## 參見

  -
  - 、[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")

  -
  -
## 註釋

## 參考文獻

  -
  -
  -
  -
## 外部链接

  - [派斯克小島州立公园](https://web.archive.org/web/20080517070511/http://www.visitpaparks.com/parks/presqueisle.aspx)
  - [派斯克小島的沙灘工程](http://www.post-gazette.com/pg/06142/692199-100.stm)
  - [Presque Isle State
    Park](http://www.dcnr.state.pa.us/stateparks/findapark/presqueisle/index.htm)
    (PA DCNR)
      -  
  - [Presque Isle, Erie, PA](http://www.presqueisle.org) (GoErie.com)
  - [Presque Isle Partnership](http://www.discoverpi.com/)



[Category:宾夕法尼亚州州立公园](../Category/宾夕法尼亚州州立公园.md "wikilink")
[Category:1921年設立的保護區](../Category/1921年設立的保護區.md "wikilink")
[Category:1921年賓夕法尼亞州建立](../Category/1921年賓夕法尼亞州建立.md "wikilink")