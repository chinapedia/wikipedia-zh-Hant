[Shahab-3_Range.jpg](https://zh.wikipedia.org/wiki/File:Shahab-3_Range.jpg "fig:Shahab-3_Range.jpg")
**流星3型**(en: Shehabs fa:شهاب
۳)是[伊朗研發中的地對地](../Page/伊朗.md "wikilink")[彈道飛彈](../Page/彈道飛彈.md "wikilink")，射程至少可達1280公里/810[英里](../Page/英里.md "wikilink")，能夠攻擊[以色列](../Page/以色列.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[印度與](../Page/印度.md "wikilink")[沙烏地阿拉伯等國家](../Page/沙烏地阿拉伯.md "wikilink")。

导弹由伊朗国防军工组织Defense Industries Organization旗下的萨娜姆工业集团Sanam Industrial
Group（第140部） 和沙希德 ·哈马特工业集团Shahid Hemmat Industrial Group (SHIG)共同完成。

## 發展

流星3型飛彈是[伊朗於](../Page/伊朗.md "wikilink")1980年代後期開始自行研制的中程戰略飛彈，利用主要採用[俄羅斯飛彈技術的朝鮮](../Page/俄羅斯.md "wikilink")[勞動1彈道飛彈為基礎上研發的](../Page/勞動1.md "wikilink")。流星3型飛彈長度為16米，採用單級火箭推進，能夠攜帶重約1噸的彈頭，射程約為1680公里，足以對[以色列](../Page/以色列.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[南亞次大陸境內的目標](../Page/南亞.md "wikilink")，以及在海灣地區駐紥的所有[美軍目標造成威脅](../Page/美軍.md "wikilink")。

自[兩伊戰爭開始](../Page/兩伊戰爭.md "wikilink")，伊朗從盟友蘇聯引進了[飛毛腿飛彈](../Page/飛毛腿飛彈.md "wikilink")，並與[伊拉克進行一連串的飛彈攻城戰](../Page/伊拉克.md "wikilink")，雙方以各自的首都[德黑蘭和](../Page/德黑蘭.md "wikilink")[巴格達為目標相互攻擊](../Page/巴格達.md "wikilink")；後來由於中、俄、北韓等伊朗有力盟邦源源不斷的技術支援，伊朗的飛彈戰力也大幅強化。

另外，[伊朗還計劃研制射程更遠的兩種飛彈](../Page/伊朗.md "wikilink")：射程為2000[公里的](../Page/公里.md "wikilink")[流星4型飛彈](../Page/流星4型.md "wikilink")，以及射程為5500公里的[流星5型飛彈](../Page/流星5型.md "wikilink")。

## 參看

  - [流星4型](../Page/流星4型.md "wikilink")
  - [流星5型](../Page/流星5型.md "wikilink")

[Category:彈道飛彈](../Category/彈道飛彈.md "wikilink")