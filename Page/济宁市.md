**济宁市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山东省下辖的](../Page/山东省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于山东省西南部。市境西界[菏泽市](../Page/菏泽市.md "wikilink")，西北接[河南省](../Page/河南省.md "wikilink")[濮阳市](../Page/濮阳市.md "wikilink")，北达[泰安市](../Page/泰安市.md "wikilink")，东邻[临沂市](../Page/临沂市.md "wikilink")，东南临[枣庄市](../Page/枣庄市.md "wikilink")，南毗[江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")。地处鲁中南山地与[黄淮平原交接地带](../Page/黄淮平原.md "wikilink")，地势呈东高西低，东部为低山丘陵，中部和南部为低洼湖区。[京杭运河纵贯市境并流经市区](../Page/京杭运河.md "wikilink")，主要河流还有[泗河](../Page/泗水_\(河流\).md "wikilink")、[东鱼河](../Page/东鱼河.md "wikilink")、[洙赵新河等](../Page/洙赵新河.md "wikilink")。市境南部有[南阳湖](../Page/南阳湖.md "wikilink")、[昭阳湖](../Page/昭阳湖.md "wikilink")、[独山湖](../Page/独山湖.md "wikilink")、[微山湖](../Page/微山湖.md "wikilink")，合称[南四湖](../Page/南四湖.md "wikilink")，为中国北方最大[淡水湖群](../Page/淡水湖.md "wikilink")。全市总面积1.12万平方公里，人口829.92万。

## 历史

### 史前

济宁，古称任。《左传》:“任、宿、须句、颛臾,风姓也,实司太昊与有济之祀,以服事诸夏。”因任是太皞之墟，靠近济水，太皞后代负责祭祀济水和祭祀太皞，手工业发达，逐渐形成了繁荣的河滨城市。历经少康中兴，汤武革命，武王伐纣，历代分封，到了周代，世袭任国。秦灭任国，置任城县，即今济宁。

### 先秦

《禹贡》有“[济](../Page/济水.md "wikilink")[河惟兖州](../Page/黄河.md "wikilink")。”又有“[海](../Page/黄海.md "wikilink")、[岱及](../Page/泰山.md "wikilink")[淮惟徐州](../Page/淮河.md "wikilink")。”\[1\]《尔雅》有“[济](../Page/济水.md "wikilink")[河间曰兖州](../Page/黄河.md "wikilink")，[济东曰徐州](../Page/济水.md "wikilink")。”\[2\]可知上古时代济宁地区除西北小部属兖州外，其余地区皆属徐州。\[3\]\[4\]而今[兖州区得名兖州源于](../Page/兖州区.md "wikilink")[南北朝](../Page/南北朝.md "wikilink")，将于之后详述。
至[周成王时](../Page/周成王.md "wikilink")，徐州不再列于九州之中，\[5\]其地分入青、兖二州。\[6\]济宁地区于此时归入兖州。\[7\]\[8\]

### 秦汉

### 三国、两晋、南北朝

### 隋唐

### 五代、宋、金

[后周](../Page/后周.md "wikilink")[广顺二年](../Page/广顺.md "wikilink")（952年），割郓州之[鉅野](../Page/鉅野.md "wikilink")、[郓城](../Page/郓城.md "wikilink")，兖州之[任城](../Page/任城.md "wikilink")，单州之[金乡等](../Page/金乡.md "wikilink")4县置[济州](../Page/济州_\(后周\).md "wikilink")，治[巨野](../Page/巨野.md "wikilink")。\[9\]\[10\][济州因濒临](../Page/济州_\(后周\).md "wikilink")[济水而得名](../Page/济水.md "wikilink")，\[11\]此为济宁得名之源头。\[12\]\[13\]
[宋朝](../Page/北宋.md "wikilink")[济州建制未变](../Page/济州_\(后周\).md "wikilink")。\[14\]兖州则升为袭庆府，辖[瑕县](../Page/滋阳.md "wikilink")、[仙源](../Page/曲阜.md "wikilink")、[泗水](../Page/泗水县.md "wikilink")、[邹县及](../Page/邹县.md "wikilink")[奉符](../Page/泰安.md "wikilink")、[莱芜等县](../Page/莱芜.md "wikilink")。\[15\]
[建炎元年](../Page/建炎.md "wikilink")（[金太宗](../Page/金太宗.md "wikilink")[天会五年](../Page/天会_\(金朝\).md "wikilink")，1127年）十一月，金军多路攻宋。\[16\]至[建炎三年](../Page/建炎.md "wikilink")（[天会七年](../Page/天会_\(金朝\).md "wikilink")，1129年）[京东两路皆已沦陷](../Page/京东路.md "wikilink")。\[17\]
[天会八年](../Page/天会_\(金朝\).md "wikilink")（[建炎四年](../Page/建炎.md "wikilink")，1130年），[金割](../Page/金朝.md "wikilink")[黄河以南为](../Page/黄河.md "wikilink")[齐国](../Page/刘齐.md "wikilink")，立[刘豫为](../Page/刘豫.md "wikilink")[齐国皇帝](../Page/刘齐.md "wikilink")。\[18\]\[19\][天会十五年](../Page/天会_\(金朝\).md "wikilink")（[绍兴七年](../Page/绍兴.md "wikilink")，1137年）废除[齐国](../Page/刘齐.md "wikilink")，济宁地区正式纳入[金朝版图](../Page/金朝.md "wikilink")。\[20\]\[21\]
[金](../Page/金朝.md "wikilink")[皇统年间](../Page/皇统.md "wikilink")（1141年 -
1149年），分[任城](../Page/任城.md "wikilink")、[巨野二县部分土地置](../Page/巨野.md "wikilink")[嘉祥县](../Page/嘉祥县.md "wikilink")，隶属于济州。\[22\][天德二年](../Page/天德_\(金朝\).md "wikilink")（[绍兴二十年](../Page/绍兴_\(南宋\).md "wikilink")，1150年），[巨野因河水淹没而废县](../Page/巨野县.md "wikilink")，州治迁至[任城](../Page/任城.md "wikilink")。\[23\]袭庆府于金朝时复降为兖州，辖[滋阳](../Page/滋阳.md "wikilink")、[曲阜](../Page/曲阜.md "wikilink")、[泗水](../Page/泗水县.md "wikilink")、[宁阳](../Page/宁阳.md "wikilink")，而[邹县割与滕州](../Page/邹县.md "wikilink")。\[24\]

### 元

[元太宗七年](../Page/元太宗.md "wikilink")（[宋理宗](../Page/宋理宗.md "wikilink")[端平二年](../Page/端平.md "wikilink")，1235年）[济州割属](../Page/济州_\(后周\).md "wikilink")[东平府](../Page/东平府.md "wikilink")。[至元六年](../Page/至元_\(忽必烈\).md "wikilink")（1269年），复立[巨野县](../Page/巨野县.md "wikilink")，并迁州治到此。\[25\]
[至元八年](../Page/至元_\(忽必烈\).md "wikilink")（1271年），升济州为[济宁府](../Page/济宁府.md "wikilink")，治[任城](../Page/任城县.md "wikilink")，不久迁回[巨野](../Page/巨野县.md "wikilink")。十二年（1275年）以[任城当江淮水陆要冲](../Page/任城县.md "wikilink")，废县复立济州，属[济宁府](../Page/济宁府.md "wikilink")。二十三年（1286年）复立[任城县隶属济州](../Page/任城县.md "wikilink")。\[26\]
[至元十六年](../Page/至元_\(忽必烈\).md "wikilink")（1279年）升[济宁府为](../Page/济宁府.md "wikilink")[济宁路](../Page/济宁路.md "wikilink")，辖[巨野](../Page/巨野县.md "wikilink")、[金乡等](../Page/金乡县.md "wikilink")7县及济州、兖州、单州3州（共领[任城](../Page/任城县.md "wikilink")、[嵫阳](../Page/兖州区.md "wikilink")、[嘉祥等](../Page/嘉祥县.md "wikilink")9县），治[巨野](../Page/巨野县.md "wikilink")。自此[济宁路基本稳定](../Page/济宁路.md "wikilink")，辖境涵盖今济宁市大部分县市区（[汶上隶属东平路](../Page/汶上县.md "wikilink")），可视为济宁市之雏形。\[27\]
[至正八年](../Page/至正.md "wikilink")（1348年），撤济州，移[济宁路治于](../Page/济宁路.md "wikilink")[任城](../Page/任城.md "wikilink")。\[28\]此后，济宁或为州，或为府，或为县，治所皆在[任城](../Page/任城.md "wikilink")，[任城遂称济宁以至于今](../Page/任城.md "wikilink")。\[29\]

[蒙古灭金后与](../Page/蒙古帝国.md "wikilink")[南宋在东部对峙于](../Page/南宋.md "wikilink")[淮河一线](../Page/淮河.md "wikilink")。为军事需要，[元宪宗七年](../Page/元宪宗.md "wikilink")（[宋理宗](../Page/宋理宗.md "wikilink")[宝祐五年](../Page/宝祐.md "wikilink")，1257年）济州掾吏毕辅国在[堽城筑坝](../Page/堽城镇.md "wikilink")，引[汶水入](../Page/汶水.md "wikilink")[洸河](../Page/洸河.md "wikilink")，流经[任城](../Page/任城县.md "wikilink")，于城南入[泗河](../Page/泗水_\(河流\).md "wikilink")，以济漕运。\[30\]\[31\]

### 明

[至正二十七年](../Page/至正.md "wikilink")（明太祖吴元年，1367年），[徐达](../Page/徐达.md "wikilink")、[常遇春北上平定山东](../Page/常遇春.md "wikilink")，张兴祖取东平、济宁。\[32\]\[33\]\[34\]济宁改路为府。\[35\]
[洪武十八年](../Page/洪武.md "wikilink")（1385年）降济宁为州，省州治任城县，其地由州直辖，另辖[嘉祥](../Page/嘉祥县.md "wikilink")、[巨野](../Page/巨野.md "wikilink")、[郓城三县](../Page/郓城.md "wikilink")。升[兖州为府](../Page/兖州府.md "wikilink")，而济宁州隶属之。自此，明清两代不复置任城县，只称济宁州。明时[兖州府辖鲁南四州二十三县](../Page/兖州府.md "wikilink")，西至[曹州](../Page/曹州.md "wikilink")，东至[沂州](../Page/沂州.md "wikilink")，治[滋阳](../Page/滋阳.md "wikilink")。\[36\]


### 清

[崇祯十七年](../Page/崇祯.md "wikilink")（[顺治元年](../Page/顺治.md "wikilink")，1644年）清军入关，[巴哈纳](../Page/巴哈纳.md "wikilink")、[石廷柱攻取山东](../Page/石廷柱.md "wikilink")。\[37\]
[清初山东府县沿用](../Page/清朝.md "wikilink")[明朝建制](../Page/明朝.md "wikilink")。[雍正二年](../Page/雍正.md "wikilink")（1724年）[济宁升为直隶州](../Page/济宁州.md "wikilink")，八年（1730年）复降属[兖州府](../Page/兖州府.md "wikilink")，[巨野](../Page/巨野.md "wikilink")、嘉祥割属[曹州](../Page/曹州.md "wikilink")，[郓城割属](../Page/郓城.md "wikilink")[兖州府](../Page/兖州府.md "wikilink")。[巨野](../Page/巨野.md "wikilink")、[郓城二县自五代以来与济宁相互从属的关系至此结束](../Page/郓城.md "wikilink")。[乾隆四十一年](../Page/乾隆.md "wikilink")（1776年）复升为直隶州，以嘉祥、[鱼台割属](../Page/鱼台.md "wikilink")，四十五年（1780年），又割[金乡来属](../Page/金乡.md "wikilink")，自此建制稳定至清末。\[38\]\[39\][兖州府自](../Page/兖州府.md "wikilink")[雍正二年](../Page/雍正.md "wikilink")（1724年）起所属州县渐次割出，辖境缩小至十县，治[滋阳](../Page/滋阳.md "wikilink")。\[40\]

### 现当代

1948年7月14日设济宁市；1967年改称地区；1983年10月升级为省辖地级市。

### 建制沿革

## 地理

### 位置

济宁市位于[山东省西南部与本省](../Page/山东省.md "wikilink")[泰安市](../Page/泰安市.md "wikilink")、[临沂市](../Page/临沂市.md "wikilink")、[枣庄市](../Page/枣庄市.md "wikilink")、[菏泽市](../Page/菏泽市.md "wikilink")、[江苏省](../Page/江苏省.md "wikilink")[徐州市接壤](../Page/徐州市.md "wikilink")，与[河南省](../Page/河南省.md "wikilink")[濮阳市隔](../Page/濮阳市.md "wikilink")[黄河相望](../Page/黄河.md "wikilink")。

### 地形

济宁市位于[山东省西南部](../Page/山东省.md "wikilink")，地处[黄淮海平原与鲁中南山丘区交接地带](../Page/黄淮海平原.md "wikilink")，东部多山，丘陵连绵。[京沪铁路以东海拔在](../Page/京沪铁路.md "wikilink")50～100米以上，比较有名的山有[曲阜](../Page/曲阜市.md "wikilink")[尼山](../Page/尼山.md "wikilink")（海拔344米），[邹城](../Page/邹城市.md "wikilink")[峄山](../Page/峄山.md "wikilink")（海拔545米），凤凰山（海拔648.4米，全市最高），[泗水尧山](../Page/泗水县.md "wikilink")（海拔582米）。[南四湖以东](../Page/南四湖.md "wikilink")—东部山麓为[泰沂山前冲积平原](../Page/泰沂山脉.md "wikilink")，自西向东倾斜，地面海拔30～50米，地面起伏较大。[南四湖以西为较平坦的](../Page/南四湖.md "wikilink")[黄泛平原](../Page/黄泛平原.md "wikilink")，自西向东倾斜，地面海拔39～34米，起伏较小。中部有[南四湖贯穿南北](../Page/南四湖.md "wikilink")。湖北为[泰沂山前冲积扇下缘](../Page/泰沂山脉.md "wikilink")，自东北向西南倾斜，地面海拔60～35米。\[41\]

### 气候

济宁市属[暖温带大陆性季风气候](../Page/暖温带大陆性季风气候.md "wikilink")，四季分明，光照充足、暖湿交替。年平均降雨量667.9毫米，年平均气温14.4℃。\[42\]\[43\]

### 水文

#### 黄河

[黄河是济宁](../Page/黄河.md "wikilink")[梁山县与](../Page/梁山县.md "wikilink")[濮阳](../Page/濮阳.md "wikilink")[台前县的界河](../Page/台前县.md "wikilink")，过境河段全部为[悬河](../Page/悬河.md "wikilink")，大堤即分水岭，故济宁境内并无流域分布。

然而黄河对于济宁水系的影响却不止于此。自古[汶水自东而来](../Page/汶水.md "wikilink")，至梁山下转向东北注入渤海，是为[大清河](../Page/大清河.md "wikilink")，也就是济水。明朝治理运河，自南旺而北至[梁山](../Page/梁山.md "wikilink")、[张秋](../Page/张秋镇.md "wikilink")，与济水相交。清[咸丰五年](../Page/咸丰_\(年号\).md "wikilink")（1855年）黄河于兰阳（今兰考）铜瓦厢决口，其中一溜一路向东北于[张秋冲决运河大堤注入](../Page/张秋镇.md "wikilink")[大清河](../Page/大清河.md "wikilink")，这条水道后来成为黄河入海主要通道，南流入淮的故道逐渐废弃。

#### 大汶河

[大汶河即](../Page/大汶河.md "wikilink")[汶水](../Page/汶水.md "wikilink")，东起[鲁山](../Page/鲁山.md "wikilink")，西至[东平湖](../Page/东平湖.md "wikilink")，最终注入[黄河](../Page/黄河.md "wikilink")。[汶水于](../Page/汶水.md "wikilink")[左传中多有记载](../Page/左传.md "wikilink")。[元](../Page/元朝.md "wikilink")[明两代开掘运河](../Page/明朝.md "wikilink")，[汶水即为](../Page/汶水.md "wikilink")[济州河主要水源](../Page/济州河.md "wikilink")。元时筑[堽城坝引汶水入](../Page/堽城坝.md "wikilink")[洸河](../Page/洸河.md "wikilink")，于[济宁](../Page/济宁.md "wikilink")[天井闸](../Page/天井闸.md "wikilink")（[会源闸](../Page/会源闸.md "wikilink")、[大闸口](../Page/大闸口（济宁）.md "wikilink")）汇入运河，明[永乐年间筑](../Page/永乐_\(明朝\).md "wikilink")[戴村坝引汶水入](../Page/戴村坝.md "wikilink")[小汶河](../Page/小汶河.md "wikilink")，于[南旺枢纽汇入运河](../Page/南旺枢纽.md "wikilink")，通过分水工程以时启闭调节南北水量，有“七分朝天子，三分下江南”之说，至此解决了[济州河的无水之患](../Page/济州河.md "wikilink")，[运河得以贯通](../Page/京杭大运河.md "wikilink")。

[大汶河济宁段实为](../Page/大汶河.md "wikilink")[济宁](../Page/济宁.md "wikilink")[汶上县与](../Page/汶上县.md "wikilink")[泰安](../Page/泰安.md "wikilink")[肥城市](../Page/肥城市.md "wikilink")、[东平县的界河](../Page/东平县.md "wikilink")，东起[琵琶山溢流坝](../Page/琵琶山溢流坝.md "wikilink")，西至代村坝（[戴村坝](../Page/戴村坝.md "wikilink")），水流湍急，历年被列为重点防汛河道，几乎年年护岸复堤。\[44\]

#### 泗水

[泗河即](../Page/泗河.md "wikilink")[泗水](../Page/泗水_\(河流\).md "wikilink")，发源于[新泰](../Page/新泰市.md "wikilink")[太平顶山西侧](../Page/太平顶山.md "wikilink")，自东向西经[泗水](../Page/泗水县.md "wikilink")、[曲阜流至](../Page/曲阜.md "wikilink")[兖州城下折向南去](../Page/兖州区.md "wikilink")，原本于[淮安](../Page/淮安.md "wikilink")[清口注入](../Page/清口.md "wikilink")[淮河](../Page/淮河.md "wikilink")，作为[淮河的重要支流](../Page/淮河.md "wikilink")，并称“淮泗”。[泗水济宁以南河段向来为中原联通江淮的交通要道](../Page/泗水_\(河流\).md "wikilink")。[宋](../Page/北宋.md "wikilink")[建炎二年](../Page/建炎.md "wikilink")（1128年）[黄河改道](../Page/黄河.md "wikilink")，[徐州以下河道为](../Page/徐州.md "wikilink")[黄河所夺](../Page/黄河.md "wikilink")，自[元](../Page/元朝.md "wikilink")[明以后](../Page/明朝.md "wikilink")，[济宁以下河道又成为](../Page/济宁.md "wikilink")[运河的重要部分](../Page/京杭大运河.md "wikilink")，数百年间经由黄河与人力的改造，济宁以下河段或潴为湖泊，或完全渠化为运河，原始河道已不复存在。现代泗河的终点是[济宁](../Page/济宁.md "wikilink")[任城区辛闸村](../Page/任城区.md "wikilink")，泗河于此注入[南四湖](../Page/南四湖.md "wikilink")，成为运河水系的一部分，只有[江苏的](../Page/江苏.md "wikilink")[泗阳](../Page/泗阳.md "wikilink")、[泗洪等地名还标示着古](../Page/泗洪.md "wikilink")[泗水曾经流经的地方](../Page/泗水_\(河流\).md "wikilink")。

[泗河是](../Page/泗水_\(河流\).md "wikilink")[济州河的另一水源](../Page/济州河.md "wikilink")，为济运四水之一。元[至元年间](../Page/至元_\(元惠宗\).md "wikilink")，于兖州城东泗河上筑[金口坝](../Page/金口坝.md "wikilink")，引水入府河，流至济宁[会源闸](../Page/会源闸.md "wikilink")（[大闸口](../Page/大闸口（济宁）.md "wikilink")）汇入运河。\[45\]

[泗河是季节性山洪河道](../Page/泗水_\(河流\).md "wikilink")，每逢大汛，洪水溢涨，泥沙沉积严重，致使河床抬高，河道险工险段增加，加之原有堤防矮小单薄，河道弯曲，宽窄不一，形成历史上洪涝灾害频繁的局面。建国后至2002年，经多次整治，泗河防汛能力大幅提高。\[46\]

#### 洸府河

[宁阳](../Page/宁阳.md "wikilink")、[兖州](../Page/兖州.md "wikilink")、[济宁间原有](../Page/济宁.md "wikilink")[洸河](../Page/洸河.md "wikilink")、[府河](../Page/府河.md "wikilink")、杨家河等河流，1965年开始[洸府河治理工程](../Page/洸府河.md "wikilink")，对各河道裁弯取直合并，按三年一遇除涝50%，十年一遇防洪标准新开挖了[洸府河干流](../Page/洸府河.md "wikilink")，注入[南四湖](../Page/南四湖.md "wikilink")。后经多年整治，除涝能力接近三年一遇，防洪标准接近二十年一遇
。\[47\]

#### 运河

#### 其他河流

  - [白马河](../Page/白马河_\(独山湖\).md "wikilink")
  - [洙水河](../Page/洙水河.md "wikilink")
  - [洙赵新河](../Page/洙赵新河.md "wikilink")
  - [新万福河](../Page/新万福河.md "wikilink")
  - [老万福河](../Page/老万福河.md "wikilink")
  - [红卫河](../Page/红卫河.md "wikilink")
  - [复兴河](../Page/复兴河\(鱼台\).md "wikilink")

#### 湖泊、水库

##### 现存湖泊

  - [南四湖](../Page/南四湖.md "wikilink")
  - [东平湖](../Page/东平湖.md "wikilink")

##### 已消失湖泊

  - [梁山泊](../Page/梁山泊.md "wikilink")
  - 南旺诸湖：[南旺湖](../Page/南旺湖.md "wikilink")、[马踏湖](../Page/马踏湖.md "wikilink")、[蜀山湖](../Page/蜀山湖.md "wikilink")
  - [马场湖](../Page/马场湖.md "wikilink")

##### 水库

  - [尼山水库](../Page/尼山水库.md "wikilink")
  - [西苇水库](../Page/西苇水库.md "wikilink")
  - [梁山泊平原水库](../Page/梁山泊平原水库.md "wikilink")

#### 黄河、运河与水系变迁

#### 南水北调

## 政治

### 现任领导

<table>
<caption>济宁市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党济宁市委员会.md" title="wikilink">中国共产党<br />
济宁市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/济宁市人民代表大会.md" title="wikilink">济宁市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/济宁市人民政府.md" title="wikilink">济宁市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议济宁市委员会.md" title="wikilink">中国人民政治协商会议<br />
济宁市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/王艺华.md" title="wikilink">王艺华</a>[48]</p></td>
<td><p><a href="../Page/孙博_(1961年).md" title="wikilink">孙博</a>[49]</p></td>
<td><p><a href="../Page/傅明先.md" title="wikilink">傅明先</a>[50]</p></td>
<td><p><a href="../Page/张继民.md" title="wikilink">张继民</a>[51]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/阳谷县.md" title="wikilink">阳谷县</a></p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/孟村县.md" title="wikilink">孟村县</a></p></td>
<td><p>山东省<a href="../Page/青岛市.md" title="wikilink">青岛市</a></p></td>
<td><p>山东省<a href="../Page/梁山县.md" title="wikilink">梁山县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年2月</p></td>
<td><p>2016年2月</p></td>
<td><p>2017年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

济宁市下辖2个[市辖区](../Page/市辖区.md "wikilink")、7个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管2个[县级市](../Page/县级市.md "wikilink")：

  - 市辖区：[任城区](../Page/任城区.md "wikilink")、[兖州区](../Page/兖州区.md "wikilink")；
  - 县级市：[曲阜市](../Page/曲阜市.md "wikilink")、[邹城市](../Page/邹城市.md "wikilink")；
  - 县：[微山县](../Page/微山县.md "wikilink")、[鱼台县](../Page/鱼台县.md "wikilink")、[金乡县](../Page/金乡县.md "wikilink")、[嘉祥县](../Page/嘉祥县.md "wikilink")、[汶上县](../Page/汶上县.md "wikilink")、[泗水县](../Page/泗水县.md "wikilink")、[梁山县](../Page/梁山县.md "wikilink")。

除正式行区划外，济宁市还设立以下经济功能区：[济宁高新技术产业开发区](../Page/济宁高新技术产业开发区.md "wikilink")（[国家级](../Page/国家级高新技术产业开发区.md "wikilink")）、[济宁经济技术开发区](../Page/济宁经济技术开发区.md "wikilink")、[太白湖新区](../Page/太白湖新区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>济宁市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[52]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>370800</p></td>
</tr>
<tr class="odd">
<td><p>370811</p></td>
</tr>
<tr class="even">
<td><p>370812</p></td>
</tr>
<tr class="odd">
<td><p>370826</p></td>
</tr>
<tr class="even">
<td><p>370827</p></td>
</tr>
<tr class="odd">
<td><p>370828</p></td>
</tr>
<tr class="even">
<td><p>370829</p></td>
</tr>
<tr class="odd">
<td><p>370830</p></td>
</tr>
<tr class="even">
<td><p>370831</p></td>
</tr>
<tr class="odd">
<td><p>370832</p></td>
</tr>
<tr class="even">
<td><p>370881</p></td>
</tr>
<tr class="odd">
<td><p>370883</p></td>
</tr>
<tr class="even">
<td><p>注：任城区数字包含济宁高新技术产业开发区所辖柳行、洸河2街道及北湖旅游度假区所辖许庄街道；兖州区数字包含济宁高新技术产业开发区所辖王因、黄屯2街道。</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>济宁市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[53]（2010年11月）</p></th>
<th><p>户籍人口[54]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>济宁市</p></td>
<td><p>8081905</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>市中区</p></td>
<td><p>588861</p></td>
<td><p>7.29</p></td>
</tr>
<tr class="even">
<td><p>任城区</p></td>
<td><p>652151</p></td>
<td><p>8.07</p></td>
</tr>
<tr class="odd">
<td><p>微山县</p></td>
<td><p>633357</p></td>
<td><p>7.84</p></td>
</tr>
<tr class="even">
<td><p>鱼台县</p></td>
<td><p>437146</p></td>
<td><p>5.41</p></td>
</tr>
<tr class="odd">
<td><p>金乡县</p></td>
<td><p>625262</p></td>
<td><p>7.74</p></td>
</tr>
<tr class="even">
<td><p>嘉祥县</p></td>
<td><p>818188</p></td>
<td><p>10.12</p></td>
</tr>
<tr class="odd">
<td><p>汶上县</p></td>
<td><p>684617</p></td>
<td><p>8.47</p></td>
</tr>
<tr class="even">
<td><p>泗水县</p></td>
<td><p>536087</p></td>
<td><p>6.63</p></td>
</tr>
<tr class="odd">
<td><p>梁山县</p></td>
<td><p>730652</p></td>
<td><p>9.04</p></td>
</tr>
<tr class="even">
<td><p>曲阜市</p></td>
<td><p>640498</p></td>
<td><p>7.93</p></td>
</tr>
<tr class="odd">
<td><p>兖州市</p></td>
<td><p>618394</p></td>
<td><p>7.65</p></td>
</tr>
<tr class="even">
<td><p>邹城市</p></td>
<td><p>1116692</p></td>
<td><p>13.82</p></td>
</tr>
<tr class="odd">
<td><p>注：全市常住人口数据中包含济宁高新技术产业开发区151462人及北湖旅游度假区58363人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")808.19万人\[55\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加了34.16万人，增长4.41%，年平均增长0.43%。其中，男性为411.84万人，占总人口的50.96%；女性为396.35万人，占总人口的49.04%，总人口性别比（以女性为100）为103.91。0－14岁的人口为139.00万人，占17.20
%，15－64岁的人口为592.87万人，占73.36 %；65岁及以上的人口为76.32万人，占9.44%。

## 经济

### 概况

济宁位于山东西南采煤区，属于[兖州煤田](../Page/兖州煤田.md "wikilink")，有丰富煤炭资源。
并有多处大型火力发电厂，如[唐村电厂](../Page/唐村电厂.md "wikilink")、济宁[里彦电厂等](../Page/里彦电厂.md "wikilink")。

济宁是鲁西南地区经济、文化和商品的流通中心，山东省工业中心城市，建设中的组群结构特大城市。济宁物产资源丰富，是山东省重要的煤炭能源基地和农副产品生产基地，经济实力雄厚。

济宁玉堂酱园有限责任公司，始建于1714年，至今已有三百年历史，是鲁西南地区唯一的中华老字号企业。

山东鲁抗医药股份有限公司是国家大型生物制药企业，国家重要的抗生素研发、生产基地，国家重点高新技术企业，拥有国家级企业技术中心。鲁抗医药A股（600789）股票于1997年2月在上海证券交易所挂牌上市，现有资产38亿元，占地近4000亩，拥有5家全资子公司、7家控股子公司、1家参股公司，员工6500余人，其中各类专业技术人员3100余人。

### 资源型城市

### 转型

## 交通

### 公路

#### 东西向

[G1511](../Page/G1511.md "wikilink"),日兰高速，境内出口有：

  - 泗水，S611圣源大道、泗水县城
  - 曲阜南，G104、曲阜城区、小雪镇
  - 兖州，G327、兖州城区
  - 济宁，S614、济宁城区东、颜店镇
  - 济宁北，G105、济宁城区西、康驿镇、廿里铺镇
  - 济宁西，S337、长沟镇
  - 嘉祥，S252、嘉祥县城

[G327](../Page/中国公路命名与编号.md "wikilink"),327国道，起自[江苏](../Page/江苏.md "wikilink")[连云港](../Page/连云港.md "wikilink"),由[泗水入境](../Page/泗水.md "wikilink")，经泗水县城、曲阜市中、兖州城北-西、济宁城北-西、嘉祥城南，向西出境至巨野县城，终点到[宁夏](../Page/宁夏.md "wikilink")[固原](../Page/固原.md "wikilink")。

#### 南北向

### 铁路

济宁市铁路主要有南北向的[京沪铁路](../Page/京沪铁路.md "wikilink")、东西向的[新兖](../Page/新兖铁路.md "wikilink")-[兖石铁路](../Page/兖石铁路.md "wikilink")，以及与[京沪铁路大致平行的](../Page/京沪铁路.md "wikilink")[京沪高铁](../Page/京沪高铁.md "wikilink")。[京九铁路则于西北一隅过境](../Page/京九铁路.md "wikilink")。
[京沪铁路自](../Page/京沪铁路.md "wikilink")[济南](../Page/济南站.md "wikilink")、[泰安南下](../Page/泰山站_\(泰安\).md "wikilink")，经[磁窑由](../Page/磁窑站.md "wikilink")[曲阜入境](../Page/曲阜市.md "wikilink")，转西南至[兖州](../Page/兖州站.md "wikilink")，后转东南经[邹城出境](../Page/邹城站.md "wikilink")，再经[滕州](../Page/滕州站.md "wikilink")、[枣庄出省](../Page/枣庄西站.md "wikilink")。
[新兖铁路起](../Page/新兖铁路.md "wikilink")[河南](../Page/河南省.md "wikilink")[新乡](../Page/新乡站.md "wikilink")，经[巨野由](../Page/巨野站.md "wikilink")[嘉祥入境](../Page/嘉祥县.md "wikilink")，东行经[济宁城南转东北](../Page/济宁站.md "wikilink")，至[兖州汇入](../Page/兖州区.md "wikilink")[京沪铁路](../Page/京沪铁路.md "wikilink")，是为[兖州站](../Page/兖州站.md "wikilink")。
[兖石铁路起](../Page/兖石铁路.md "wikilink")[兖州站](../Page/兖州站.md "wikilink")，经[京沪铁路南行至](../Page/京沪铁路.md "wikilink")[程家庄分轨](../Page/程家庄站.md "wikilink")，转东北至[曲阜](../Page/曲阜站.md "wikilink")，东行经[泗水出境](../Page/泗水县.md "wikilink")，再东南经[平邑](../Page/平邑站.md "wikilink")、[临沂](../Page/临沂站.md "wikilink")，至[日照](../Page/日照市.md "wikilink")[石臼港出海](../Page/石臼港.md "wikilink")。
[京沪高铁自](../Page/京沪高速铁路.md "wikilink")[济南](../Page/济南西站.md "wikilink")、[泰安南下](../Page/泰安站.md "wikilink")，由[曲阜入境](../Page/曲阜市.md "wikilink")，南行经[曲阜东站](../Page/曲阜东站.md "wikilink")，由[邹城出境](../Page/邹城市.md "wikilink")，再经[滕州](../Page/滕州东站.md "wikilink")、[枣庄出省](../Page/枣庄站.md "wikilink")。
[京九铁路自](../Page/京九铁路.md "wikilink")[台前](../Page/台前县.md "wikilink")[孙口镇经](../Page/孙口镇.md "wikilink")[孙口黄河大桥跨越](../Page/孙口黄河大桥.md "wikilink")[黄河到](../Page/黄河.md "wikilink")[梁山](../Page/梁山县.md "wikilink")[赵堌堆乡入境](../Page/赵堌堆乡.md "wikilink")，未经县城直接南下由本县出境，至[郓城转西南至](../Page/郓城站.md "wikilink")[菏泽](../Page/菏泽站.md "wikilink")，复转正南经[定陶](../Page/定陶站.md "wikilink")、[曹县出省](../Page/曹县站.md "wikilink")。济宁境内段仅15公里，[梁山站在](../Page/梁山站.md "wikilink")[杨营镇](../Page/杨营镇_\(梁山县\).md "wikilink")。
境内主要车站有[兖州站](../Page/兖州站.md "wikilink")、[济宁站](../Page/济宁站.md "wikilink")、[曲阜东站等](../Page/曲阜东站.md "wikilink")。
目前建设中铁路有[鲁南高速铁路](../Page/鲁南高速铁路.md "wikilink")(国铁一类干线，设计时速350km)，[湖西铁路等](../Page/湖西铁路.md "wikilink")。

### 航空

### 水运

### 公共交通

## 教育

### 中小学

<small>仅列举济宁市境内的部分知名中小学</small>

  - [济宁市实验中学](../Page/济宁市实验中学.md "wikilink")
  - [济宁学院附属中学](../Page/济宁学院附属中学.md "wikilink")
  - [济宁市第一中学](../Page/济宁市第一中学.md "wikilink")
  - [济宁市第十五中学](../Page/济宁市第十五中学.md "wikilink")
  - [济宁市第十三中学](../Page/济宁市第十三中学.md "wikilink")
  - [济宁市育才中学](../Page/济宁市育才中学.md "wikilink")
  - [济宁学院附属高级中学](../Page/济宁学院附属高级中学.md "wikilink")

### 大学

  - [曲阜师范大学](../Page/曲阜师范大学.md "wikilink")
  - [济宁医学院](../Page/济宁医学院.md "wikilink")
  - [济宁学院](../Page/济宁学院.md "wikilink")
  - [山东理工职业学院](../Page/山东理工职业学院.md "wikilink")
  - [济宁职业技术学院](../Page/济宁职业技术学院.md "wikilink")
  - [济宁技师学院](../Page/济宁技师学院.md "wikilink")
  - [曲阜远东职业技术学院](../Page/曲阜远东职业技术学院.md "wikilink")

## 旅游

## 人物

  - [孔子](../Page/孔子.md "wikilink")（前551年-前479年），中国思想家，[儒家创始人](../Page/儒家.md "wikilink")。
  - [孟子](../Page/孟子.md "wikilink")（前372年-前289年），中国思想家，儒家。
  - [颜子](../Page/颜回.md "wikilink")（前521年-前490年），字子渊，是孔子最得意的弟子之一。[唐太宗](../Page/唐太宗.md "wikilink")[贞观二年](../Page/贞观.md "wikilink")（628年）尊颜回为“先师”。[唐玄宗时追封为](../Page/唐玄宗.md "wikilink")“充国公”。[元代时又加封为](../Page/元代.md "wikilink")“充国复圣公”。至此颜回便被尊称为“复圣”。
  - [曾子](../Page/曾子.md "wikilink")（前505年-前432年），[春秋末年](../Page/春秋时期.md "wikilink")[鲁国人](../Page/鲁国.md "wikilink")。名参，字子舆，被尊称为曾子，又被后世儒家尊为“宗圣”。
  - [鲁班](../Page/鲁班.md "wikilink")，中国建筑之祖。
  - [左丘明](../Page/左丘明.md "wikilink")，春秋末期著名史学家，著有《[左传](../Page/左传.md "wikilink")》和《[国语](../Page/国语.md "wikilink")》。
  - [王弼](../Page/王弼_\(三國\).md "wikilink")，[魏晋玄学理论的奠基人](../Page/魏晋.md "wikilink")。字辅嗣，山阳高平（今山东邹城、金乡一带）人。
  - [郗鉴](../Page/郗鉴.md "wikilink")，金乡人。东晋重臣，历[元帝](../Page/晋元帝.md "wikilink")、[明帝](../Page/晋明帝.md "wikilink")、[成帝三朝](../Page/晋成帝.md "wikilink")，官至太尉。为[王羲之岳父](../Page/王羲之.md "wikilink")。
  - [孔尚任](../Page/孔尚任.md "wikilink")，字聘之，又字季重，号东塘，别号岸堂，自称云亭山人。山东曲阜人，孔子六十四代孙，清初诗人、戏曲作家。时人将他与《[长生殿](../Page/长生殿.md "wikilink")》作者[洪升并论](../Page/洪升.md "wikilink")，称“南洪北孔”。
  - [路中一](../Page/路中一.md "wikilink")，[一贯道第十七代祖师](../Page/一贯道.md "wikilink")，被一贯道官方认为是[弥勒祖师转世](../Page/弥勒.md "wikilink")。
  - [张天然](../Page/张天然.md "wikilink")，[一贯道第十八代祖师](../Page/一贯道.md "wikilink")，被一贯道官方认为是[济公活佛转世](../Page/济公活佛.md "wikilink")。
  - [乔羽](../Page/乔羽.md "wikilink")，中国著名歌词作家。
  - [潘晓婷](../Page/潘晓婷.md "wikilink")，中国[台球著名](../Page/台球.md "wikilink")“九球小天后”。
  - [曹興誠](../Page/曹興誠.md "wikilink")，台灣知名企業家，現任[聯電董事](../Page/聯電.md "wikilink")。
  - [李丁](../Page/李丁.md "wikilink")，表演艺术家，成功塑造了“六王爷”等角色。

## 姊妹城市

  - [栃木縣](../Page/栃木縣.md "wikilink")[足利市](../Page/足利市.md "wikilink")
    1984年9月21日

  - [俄克拉何马州](../Page/俄克拉何马州.md "wikilink")[劳顿](../Page/劳顿_\(俄克拉何马州\).md "wikilink")
    1995年10月18日

  - [上莱茵省](../Page/上莱茵省.md "wikilink")[米卢斯](../Page/米卢斯.md "wikilink")
    1996年9月11日

  - [石川縣](../Page/石川縣.md "wikilink")[小松市](../Page/小松市.md "wikilink")
    2008年9月5日\[56\]

  - [塔甘羅格](../Page/塔甘羅格.md "wikilink") 2009年3月6日

  - [江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")
    2009年9月9日

## 參考

## 外部链接

  - [济宁政府信息网](http://www.jining.gov.cn)

{{-}}

[\*](../Category/济宁.md "wikilink") [J](../Category/山东地级市.md "wikilink")
[J](../Category/运河城市.md "wikilink") [鲁](../Category/中国大城市.md "wikilink")

1.  《尚书》禹贡。上古时河在北，济在南。济约同现黄河山东段，河则由河北北上至天津、沧州附近出海。

2.  《尔雅》释地。

3.  （唐）杜佑《通典》州郡十·古徐州·鲁郡。“始，禹导兖水而为济，截河南渡，东流与菏泽、汶水会，又东北入于海。兖州在济河之间，因济水发源为名，今郡理乃非其地。”“（鲁郡）《禹贡》徐、兖二州之域。任城、龚丘（今宁阳）县即兖州界，余并徐州城。”禹贡时代济水在菏泽、济宁境内的走向未详，但若以《通典》列述鲁郡各县所属推断，济水自菏泽东来至任城而北，于宁阳合汶水，但此流向过于迂回，至宁阳已不可能继续东北流。且与《水经注》所载不同，《水经注》记济水经定陶、巨野、寿张、须昌，皆在任城、龚丘以西。故任城、龚丘为兖州界存疑。

4.  （唐）李吉甫《元和郡县图志》河南道·兖州。“《禹贡》导沇水东流为济，截河南渡，东与菏泽、汶水会，又东北入于海。兖州在济河之间，因济水发源为名，今郡理乃非其境。”沇、兖同音，兖州即沇州。唐朝兖州辖十县，大约相当于今济宁、泰安、莱芜三市。

5.  （唐）杜佑《通典》州郡一·序目上。“至成王时，亦曰九州，属职方氏。扬、荆、荆河（豫）、青、兖、雍、幽、冀、并。”

6.  《周礼》夏官司马·职方。“正东曰青州，其山镇曰[沂山](../Page/沂山.md "wikilink")，其泽薮曰望诸，其川[淮](../Page/淮河.md "wikilink")、[泗](../Page/泗河.md "wikilink")，其浸[沂](../Page/沂河.md "wikilink")、[沭](../Page/沭河.md "wikilink")，其利蒲鱼，其民二男二女，其畜宜鸡、狗，其谷宜稻、麦。河东曰兖州，其山镇曰[岱山](../Page/泰山.md "wikilink")，其泽薮曰[大野](../Page/巨野泽.md "wikilink")，其川[河](../Page/黄河.md "wikilink")、[泲](../Page/济水.md "wikilink")，其浸卢、[维](../Page/潍河.md "wikilink")，其利蒲鱼，其民二男三女，其畜宜六扰，其谷宜四种。”《职方考》中徐州不复存在。

7.  《周礼》夏官司马·职方。从《职方考》所记山川湖泊大体可以看出相较《禹贡》，兖、青、徐三州范围发生了很大变化，青州北部分入兖州，徐州东部和南部分入青州。而徐州西北部，也就是济宁地区所处之地的归属，周礼中描述并不明确。若以[河东为兖州](../Page/黄河.md "wikilink")，[沂山为青州](../Page/沂山.md "wikilink")，则今济宁全境当属兖州。

8.  （唐）杜佑《通典》州郡十·古徐州·鲁郡。“至周置兖州，始县兼得今郡之地。”

9.  （宋）乐史《太平寰宇记》河南道十四·济州。“周高祖广顺二年九月平兖州，回至巨野，因诏于此复置济州，仍割兖州之任城金乡、郓州之中都等县隶之，其年十二月又割郓州郓城县隶之，中都却入郓州。”

10. 《新五代史》职方考第三。《旧五代史》郡县志载，广顺二年九月，割郓州巨野，兖州任城、中都，单州金乡4县置济州。是年十二月割郓州郓城隶[济州](../Page/济州_\(后周\).md "wikilink")，中都改隶郓州。变化仅数月间，故《新五代史》直记巨野、郓城、任城、金乡4县，不记中都。

11. 《元史》地理志一·济州。“周濒济水立济州。宋因之。”

12. （清）叶圭绶《续山东考古录》济宁直隶州沿革。“任城县属济州，乃任城县变为济宁州所由也。”

13. 《济宁县志（民国十六年）》经界篇。“至济宁之得名，自元世祖至元八年改济宁府始，以其先五代时属济州耳，其实南北二济故道皆不在境内也。然自元以来相沿未改，于今亦六七百年矣。”

14.
15. 《宋史》地理一·京东西路。

16. 《金史》世纪补·睿宗。“天会五年，宗望薨，帝为右副元帅，驻兵燕京。十一月，分遣诸将伐宋，帝发自河间，徇地淄、青。”

17. 《宋史》高宗本纪二。自建炎二年十一月至三年九月，京东两路州府相继沦陷。济州失陷月日未列，但自濮州始，至于单州，济州周边诸州府皆已失陷，济州似不当独存。

18. 《金史》列传第十五·刘豫。“天会八年九月戊申，备礼册命，立豫为大齐皇帝，都大名，仍号北京，置丞相以下官，赦境内。”

19. 《宋史》列传第二百三十四·刘豫。“界旧河以南，俾豫统之。”建炎二年黄河改道由泗入淮，因此说“旧河”。

20. 《金史》列传第十五·刘豫。“天会十五年，诏废齐国，降封豫为蜀王。”

21. 《金史》河渠志。“金始克宋，两河悉畀刘豫。豫亡，河遂尽入金境。”

22. （清）叶圭绶《续山东考古录》济宁直隶州沿革·嘉祥。

23. 《元史》地理志一·济州。“金迁州治任城，以河水湮没故也。”

24. 《金史》地理中·山东西路。

25. 《元史》地理志一·济宁路。

26.
27.
28. 《明史》地理志二·济宁州。移济宁路治于任城一事，《明史》载而《元史》阙，此处从《续山东考古录》列以存异。

29.
30. （元）李惟明《改作东大闸记》：“国初岁丁巳，济倅奉符毕辅国请于严东平，始于汶水之阴、堽城之左作一斗门，堨汶水入洸，益泗漕，以饷宿蕲戍边之众，且以溉济兖间田。汶由是有南入泗淮之派。”

31. （清）顾炎武《天下郡国利病书》山东备录上·漕河。“宋理宗宝祐五年，济倅奉符毕辅国于堽城作斗门，以遏汶水入洸，至任城，益泗漕，以饷宿蕲戍边之众，由是汶有南入泗淮之派。”

32. 《明史》太祖本纪一。

33. 《明史》列传第十三·徐达。

34. 《明史》列传第二十一·张兴祖。

35. 《明史》地理志二·济宁州。

36. 《明史》地理志二·兖州府。明朝府极大，山东只有济南、东昌、兖州、青州、莱州、登州等六府。

37. 《清史稿》世祖本纪一。

38. 《清史稿》地理志八·济宁直隶州。

39. 《清史稿》地理志八·曹州府。

40. 《清史稿》地理志八·兖州府。

41. [济宁水利概况](http://www.jnsl.gov.cn/Html/?1951.html)

42. [济宁气象局网站](http://www.jnqxj.gov.cn/index2.aspx#jnqh)

43.
44. [济宁水利—大汶河](http://www.jnsl.gov.cn/sxt/dwh.htm)

45. （清）顾祖禹《读史方舆纪要》卷三十·山东一·汶水。“为一闸于兖州，以导泗沂会洸合而至会源闸南北分流。”

46. [济宁水利—泗河](http://www.jnsl.gov.cn/sxt/sh.htm)

47. [济宁水利—洸府河](http://www.jnsl.gov.cn/sxt/gfh.htm)

48.

49.

50.

51.

52.

53.

54.

55.

56.