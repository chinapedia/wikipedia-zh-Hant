**浏阳县**，[中国旧](../Page/中国.md "wikilink")[县名](../Page/县.md "wikilink")。

[东汉](../Page/东汉.md "wikilink")[建安中析](../Page/建安_\(東漢\).md "wikilink")[临湘县置](../Page/临湘县_\(西汉\).md "wikilink")，[治所在今](../Page/治所.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[浏阳市东北](../Page/浏阳市.md "wikilink")。属[长沙郡](../Page/长沙郡.md "wikilink")。“因县南浏阳水为名”（《元和郡县志》）。[隋朝](../Page/隋朝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年）废。[唐朝](../Page/唐朝.md "wikilink")[景龙二年](../Page/景龙.md "wikilink")（708年）于故城复置，寻移今浏阳市。属[潭州](../Page/潭州.md "wikilink")。[元朝时属](../Page/元朝.md "wikilink")[潭州路](../Page/潭州路.md "wikilink")。[元贞元年](../Page/元贞.md "wikilink")（1295年）升为[浏阳州](../Page/浏阳州.md "wikilink")，移治今浏阳市东北官渡。[明朝](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年）复降为县，还治今市。属[长沙府](../Page/长沙府.md "wikilink")。[清代因袭不改](../Page/清代.md "wikilink")。1934-1936年期间，在县境，一度析置[平浏长县苏维埃政权](../Page/平浏长县.md "wikilink")。1993年改设**浏阳市**。

## 著名人物

  - [李鴻樾](../Page/李鸿樾.md "wikilink")（1938年任瀏陽一中校長，伯祖父「[李興銳](../Page/李興銳.md "wikilink")」1904年任[兩江總督](../Page/兩江總督.md "wikilink")）\[1\]
  - [宋任穷](../Page/宋任穷.md "wikilink"),「[中共八大元老](../Page/中共八大元老.md "wikilink")」之一,曾担任[中国共产党中央顾问委员会副主任](../Page/中国共产党中央顾问委员会.md "wikilink")、[中共中央组织部部长](../Page/中共中央组织部.md "wikilink")、[中共中央政治局委员](../Page/中共中央政治局委员.md "wikilink")、[全国政协副主席](../Page/全国政协副主席.md "wikilink")。
  - [王震](../Page/王震.md "wikilink"),「[中共八大元老](../Page/中共八大元老.md "wikilink")」之一,曾担任[中华人民共和国副主席](../Page/中华人民共和国副主席.md "wikilink")、[中華人民共和國國務院副總理](../Page/中華人民共和國國務院副總理.md "wikilink")。
  - [胡耀邦](../Page/胡耀邦.md "wikilink"),[中国共产党和](../Page/中国共产党.md "wikilink")[中华人民共和国的第二代主要领导人之一](../Page/中华人民共和国.md "wikilink"),曾任[中共中央总书记](../Page/中共中央总书记.md "wikilink")。
  - [杨勇](../Page/杨勇.md "wikilink")
    ,[中国人民解放军](../Page/中国人民解放军.md "wikilink")[开国上将](../Page/开国上将.md "wikilink"),曾担任[中国人民解放军第一副总参谋长](../Page/中国人民解放军副总参谋长.md "wikilink")、[全国人大常委](../Page/全国人大常委.md "wikilink")、第十至十一届[中央委员](../Page/中央委员.md "wikilink")。

## 参考文献

## 參見

  - [瀏陽一中](../Page/瀏陽一中.md "wikilink")

[Category:东汉县份](../Category/东汉县份.md "wikilink")
[Category:晋朝县份](../Category/晋朝县份.md "wikilink")
[Category:南北朝县份](../Category/南北朝县份.md "wikilink")
[Category:隋朝县份](../Category/隋朝县份.md "wikilink")
[Category:唐朝县份](../Category/唐朝县份.md "wikilink")
[Category:五代十国县份](../Category/五代十国县份.md "wikilink")
[Category:宋朝县份](../Category/宋朝县份.md "wikilink")
[Category:元朝县份](../Category/元朝县份.md "wikilink")
[Category:明朝县份](../Category/明朝县份.md "wikilink")
[Category:清朝县份](../Category/清朝县份.md "wikilink")
[Category:中华民国湖南省县份](../Category/中华民国湖南省县份.md "wikilink")
[Category:已撤消的中华人民共和国湖南省县份](../Category/已撤消的中华人民共和国湖南省县份.md "wikilink")
[Category:长沙行政区划史](../Category/长沙行政区划史.md "wikilink")
[Category:浏阳市](../Category/浏阳市.md "wikilink")
[Category:200年代建立的行政区划](../Category/200年代建立的行政区划.md "wikilink")
[Category:1993年废除的行政区划](../Category/1993年废除的行政区划.md "wikilink")

1.  大陸新聞中心／綜合報導"史上最牛畢業證書！梁啟超、王國維當導師"[1](http://www.nownews.com/2011/02/28/91-2692511.htm),[NOWnews](../Page/NOWnews.md "wikilink")/今日新聞網,2011/02/28
    10:13.