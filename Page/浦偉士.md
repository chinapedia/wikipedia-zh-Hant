**浦偉士**[爵士](../Page/爵士.md "wikilink")<span style="font-size:smaller;">，[DSO](../Page/傑出服務勳章.md "wikilink")</span>（Sir
**William
Purves**，），[英國人](../Page/英國.md "wikilink")，生於[蘇格蘭](../Page/蘇格蘭.md "wikilink")，[滙豐集團前](../Page/滙豐集團.md "wikilink")[主席](../Page/主席.md "wikilink")。

浦偉士在1948年畢業於凱爾素中學，同年加入[蘇格蘭皇家銀行](../Page/蘇格蘭皇家銀行.md "wikilink")。1951年，他服兵役並来到[香港](../Page/香港.md "wikilink")，來港後不久，他就參加[韓戰](../Page/韓戰.md "wikilink")，戰後隨即返港。

1954年，浦偉士加入[香港上海滙豐銀行](../Page/香港上海滙豐銀行.md "wikilink")，之後他曾經在[德國](../Page/德國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[日本以及英國分行工作](../Page/日本.md "wikilink")。1979年，他升任為國際業務部的總經理，主管[中國業務](../Page/中國.md "wikilink")，之後於1982年成為執行董事。1986年，他接替退休的[沈弼爵士](../Page/沈弼.md "wikilink")，成為香港上海滙豐銀行主席，並於1991年出任滙豐控股有限公司首任主席。

滙豐控股於1993年遷往[倫敦後](../Page/倫敦.md "wikilink")，浦偉士亦重返英國，退出香港上海滙豐銀行主席一職，之後他出任[米特蘭銀行](../Page/米特蘭銀行.md "wikilink")（現稱[英國滙豐銀行](../Page/英國滙豐銀行.md "wikilink")）主席，不過一年中他每兩个月定期來港，也經常前往中国内地。直到1998年，浦偉士退休，由[龐約翰出任滙豐集團主席之後](../Page/龐約翰.md "wikilink")，他一直都在倫敦生活。

他也曾獲委任為[行政局議員](../Page/行政局.md "wikilink")、前[英皇御准香港賽馬會主席](../Page/英皇御准香港賽馬會.md "wikilink")、康體發展局首任主席和英國銀行學會副總裁，積極參與社會事務。此外，他在1983年被委为[太平绅士](../Page/太平绅士.md "wikilink")，1990年獲得[CBE勳銜](../Page/CBE.md "wikilink")，1993年冊封為爵士，1998年獲得[香港公開大學榮譽工商管理博士學位](../Page/香港公開大學.md "wikilink")，1999年獲授予[大紫荊勳章](../Page/大紫荊勳章.md "wikilink")。\[1\]

[category:汇丰人物](../Page/category:汇丰人物.md "wikilink")

[Category:英國銀行家](../Category/英國銀行家.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:香港大紫荊勳賢](../Category/香港大紫荊勳賢.md "wikilink")
[Category:前香港行政局議員](../Category/前香港行政局議員.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:軍功章受領者](../Category/軍功章受領者.md "wikilink")
[Category:杰出服务勋章得主](../Category/杰出服务勋章得主.md "wikilink")
[Category:在香港的英國人](../Category/在香港的英國人.md "wikilink")
[Category:苏格兰人](../Category/苏格兰人.md "wikilink")

1.  劉美儀\[[https://hk.finance.appledaily.com/finance/daily/article/20150302/19060351"撬開浦偉士金口](https://hk.finance.appledaily.com/finance/daily/article/20150302/19060351%22撬開浦偉士金口)　需絕佳提問"\],*蘋果日報*,2015年3月2日