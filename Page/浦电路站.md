[Pudian_Road_Station_Line_6.jpg](https://zh.wikipedia.org/wiki/File:Pudian_Road_Station_Line_6.jpg "fig:Pudian_Road_Station_Line_6.jpg")
[Pudian_Road_Station_Line_6_Concourse.jpg](https://zh.wikipedia.org/wiki/File:Pudian_Road_Station_Line_6_Concourse.jpg "fig:Pudian_Road_Station_Line_6_Concourse.jpg")
**浦电路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[福山路](../Page/福山路.md "wikilink")[浦电路及](../Page/浦电路.md "wikilink")[东方路](../Page/东方路.md "wikilink")、[浦电路口](../Page/浦电路.md "wikilink")，为[上海轨道交通4号线及](../Page/上海轨道交通4号线.md "wikilink")[6号线的地下](../Page/上海轨道交通6號線.md "wikilink")[岛式站台](../Page/岛式站台.md "wikilink")。其中4号线车站位于福山路浦电路地底，為地下[岛式站台設計](../Page/岛式站台.md "wikilink")，以白色作為佈置的主要色彩。6号线车站位于[东方路](../Page/东方路.md "wikilink")、[浦电路口地底](../Page/浦电路.md "wikilink")，亦为地下[岛式站台](../Page/岛式站台.md "wikilink")，距4号线站台数百米，以淺啡色作為佈置的主要色彩。

两线在此站既无法站内换乘，也无法使用交通卡出站换乘，但在4号线站厅层和6号线站厅层间设有地下通道联通。由于无法享受换乘优惠，且相邻两站皆可相互换乘，本站换乘客流较低。

## 公交换乘

### 4号线浦电路站

169、736

### 6号线浦电路站

169、170、219、522、583、639、736、746、779、785、819、871、970、978、989、隧道九线、东川专线、338、451、792、795、798、隧道夜宵线

## 车站出口

### 4号线

  - 1号口：福山路西侧，竹林路北
  - 2号口：浦电路北侧，福山路东
  - 3号口：浦电路北侧，福山路西
  - 4号口：福山路东侧，竹林路北

### 6号线

  - 1号口：东方路东侧，浦电路南
  - 3号口：东方路东侧，1号口南
  - 4号口：东方路西侧，浦电路南

注：4号线2号口及6号线1、3号口之间设有无障碍电梯| 4号线2号口设有洗手间

## 邻近车站

## 备注

## 参考资料

  - [浦电路站建设用地规划许可证](http://www.shghj.gov.cn/Ct_3.aspx?ct_id=00070801E12164)

</div>

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2005年启用的铁路车站](../Category/2005年启用的铁路车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")