**浦臼町**（）為一個位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[空知綜合振興局中部的](../Page/空知綜合振興局.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。

日本[明治維新時代的維新志士](../Page/明治維新.md "wikilink")[坂本龍馬之外甥為此地早期的開墾者之一](../Page/坂本龍馬.md "wikilink")，而坂本龍馬養子之妻及其子女也在之後移居至此。\[1\]
\[2\]

町名「浦臼」源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「uray-us-nay」，意思是有[魚梁的河川](../Page/魚梁.md "wikilink")\[3\]。

## 歷史

  - 1887年： 由樺戶集治監內之犯人開闢了月形至晩生內之間的道路，開始有移民進入此地進行開墾。\[4\]
  - 1899年： 浦臼村從月形村（現在的[月形町](../Page/月形町.md "wikilink")）分村。
  - 1909年4月1日： 成為北海道二級村。
  - 1960年9月1日： 改制為浦臼町。

## 交通

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [札沼線](../Page/札沼線.md "wikilink")：[於札內車站](../Page/於札內車站.md "wikilink")
        - [鶴沼車站](../Page/鶴沼車站.md "wikilink") -
        [浦臼車站](../Page/浦臼車站.md "wikilink") -
        [札的車站](../Page/札的車站.md "wikilink")-
        [晩生內車站](../Page/晩生內車站.md "wikilink")

|                                                                                                                                                          |                                                                                                                                                                |
| -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Urausu_Station_in_Sasshou_Line.jpg](https://zh.wikipedia.org/wiki/File:Urausu_Station_in_Sasshou_Line.jpg "fig:Urausu_Station_in_Sasshou_Line.jpg") | [Osokinai_Station_in_Sasshou_Line.jpg](https://zh.wikipedia.org/wiki/File:Osokinai_Station_in_Sasshou_Line.jpg "fig:Osokinai_Station_in_Sasshou_Line.jpg") |

### 渡船

  - 美浦渡船（航行於石狩川、往來於浦臼町與[美唄市之間](../Page/美唄市.md "wikilink")）

### 道路

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道275號（空知國道）</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道278號奈井江浦臼線</li>
<li>北海道道603號浦臼停車場線</li>
<li>北海道道1159號美唄浦臼線</li>
</ul></td>
</tr>
</tbody>
</table>

### 巴士

  - 北海道中央巴士：
      - 浦臼車站 - 瀧川
  - 浦臼町營巴士
      - 浦臼車站 - 奈井江車站
      - 浦臼車站 - 石狩新宮

## 觀光

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光景點">觀光景點</h3>
<ul>
<li>鶴沼公園</li>
<li>美浦渡船</li>
<li>浦臼町自然休養村中心</li>
</ul></td>
<td><h3 id="祭典活動">祭典活動</h3>
<ul>
<li>浦臼夏祭</li>
<li>酒鄉慶典in 浦臼</li>
<li>牡丹蕎麦收穫祭 in 浦臼</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="中學校">中學校</h3>
<ul>
<li>浦臼町立浦臼中學校</li>
</ul></td>
<td><h3 id="小學校">小學校</h3>
<ul>
<li>浦臼町立浦臼小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [本山町](../Page/本山町.md "wikilink")（[高知縣](../Page/高知縣.md "wikilink")[長岡郡](../Page/長岡郡.md "wikilink")）

## 參考資料

## 外部連結

  - [浦臼町的教育](https://web.archive.org/web/20060714060801/http://www15.ocn.ne.jp/~urachu1/index.html)

<!-- end list -->

1.
2.
3.
4.