**浦项制鐵足球俱乐部**（；[英语](../Page/英语.md "wikilink")：**Pohang
Steelers**），又被譯作**浦項鐵人**。原來名叫[浦項鋼鐵](../Page/浦項鋼鐵.md "wikilink")，後來是浦項鋼鐵買下球隊經營權，於是才改名浦項鋼鐵人（球隊曾用過POSCO原子和浦項原子為名）。浦項制鐵是1983年[K聯賽初成立時的創始俱樂部之一當時名為](../Page/K聯賽.md "wikilink")（**POSCO**）。浦項制鐵是韓國聯賽勁旅之一，球會除了五次獲得[K聯賽冠軍外](../Page/K聯賽.md "wikilink")，球隊更三次於[亞足聯冠軍聯賽稱王](../Page/亞足聯冠軍聯賽.md "wikilink")，至今尚未有其他亞洲球隊打破此榮譽記錄。

## 歷史

球隊在1984年正式成為全職業足球隊，同一年他們改名叫**POSCO海豚**，隔年賽季又改名叫**POSCO原子**。1986年賽季球隊贏得隊史上第一個聯賽冠軍，他們在賽季中完全稱霸K聯賽。在1985年到1998年賽季之間球隊一直都保持在聯賽排名前四位。

浦項制铁隊在1990年啟用了可以容納25,000人的[浦项鋼園球場](../Page/浦项鋼園球場.md "wikilink")。這是[韩国足球史上第一次有足球球隊自己蓋了一個足球專用的體育場](../Page/韩国.md "wikilink")。

在1995年浦項隊改名為**浦項原子**（Pohang
Atoms），球隊希望藉此強化與地方的關係。在1997年浦項隊更名為今日的名字**Pohang
Steelers**。浦項制鐵在1997年定名之後橫掃亞洲足壇；浦項制鐵在1997年以及1998年連續兩年歷史性奪得[亞足聯冠軍聯賽冠軍](../Page/亞足聯冠軍聯賽.md "wikilink")，成為當時亞洲公認的最佳球隊。

浦項制鐵雖然在90年代尾聲取得佳績，但在2000年亦曾經歷過歷經慘澹的一年，球隊的排名幾乎接近榜底。不過在2004年球隊成績漸漸回並升奪下了常規賽的冠軍，只是最後於季後賽面對[水原三星卻以](../Page/水原三星.md "wikilink")4-3於[點球大戰落敗無緣總冠軍](../Page/點球.md "wikilink")。

2009年是浦項制鐵近年最成功的一年,它先於[亞足聯冠軍聯賽小組賽](../Page/亞足聯冠軍聯賽.md "wikilink")3勝3平的成績位H組榜首晉級八分之一决赛，及後在淘汰赛不斷過關斬將击败[纽卡斯尔联喷气机](../Page/纽卡斯尔联喷气机.md "wikilink")、[塔什干缔造者](../Page/本尤德科足球俱乐部.md "wikilink")、[乌姆萨拉尔第三次晉身决赛](../Page/乌姆沙拉尔体育俱乐部.md "wikilink")，更于[日本](../Page/日本.md "wikilink")[东京国立霞丘陆上竞技场中以](../Page/東京國立競技場.md "wikilink")2比1的比分絕殺[沙特阿拉伯豪门](../Page/沙特阿拉伯.md "wikilink")[吉达团结再次登上亚洲足球之巅](../Page/吉达团结.md "wikilink")。浦项制铁在奪冠後沒有停止走向更高的步伐，於同年12月舉行的[国际足联世界俱乐部杯先後擊敗](../Page/国际足联世界俱乐部杯.md "wikilink")[非洲冠軍聯賽冠軍](../Page/非洲冠军联赛.md "wikilink")[马赞姆贝全能及](../Page/TP马赞姆贝.md "wikilink")[中北美及加勒比海冠軍聯賽冠軍](../Page/中北美冠军联赛.md "wikilink")[亚特兰特](../Page/亚特兰特足球俱乐部.md "wikilink")，結果球隊獲得了第一次參賽便得到季軍的榮譽，更平了亞洲球隊參加該項賽事的最佳成績而成為韓國足球界的一時佳話。

經過數年變遷，2013年[浦項制鐵以全韓班姿態參加賽季比賽](../Page/浦項制鐵.md "wikilink")。雖然在[亞足聯冠軍聯賽小組賽以一勝四平一負僅得](../Page/亞足聯冠軍聯賽.md "wikilink")7分未能晉級十六強，但球隊於出局後卻在聯賽高歌猛進，以零外援的陣容下先於8月份舉行的[韓國足協盃在](../Page/韓國足協盃.md "wikilink")120分鐘內以1-1打成平手，最後以十二碼決勝負4-3的總比分擊倒[全北現代獲得冠軍](../Page/全北現代.md "wikilink"),後於同年12月1日[K聯賽最後一輪以最后一分鐘絕殺](../Page/K聯賽.md "wikilink")，以1-0的比分客场击败[蔚山現代的比賽](../Page/蔚山現代.md "wikilink"),僅以積分榜一分之差力壓[蔚山現代奪得](../Page/蔚山現代.md "wikilink")[經典K聯賽冠軍](../Page/經典K聯賽.md "wikilink")，歷史性成為[韓國職業聯賽首支於同年內同時奪得](../Page/韓國職業足球聯賽.md "wikilink")[K聯賽冠軍和](../Page/K聯賽.md "wikilink")[韓國足協盃冠軍的兩冠球隊](../Page/韓國足協盃.md "wikilink")。

## 俱乐部历年成绩

| 赛季                                                  | 联赛等级                                | 俱乐部数量 | 联赛最终排名 | [韩国足协杯](../Page/韩国足协杯.md "wikilink") | [亚足联冠军联赛](../Page/亚足联冠军联赛.md "wikilink") |
| --------------------------------------------------- | ----------------------------------- | ----- | ------ | ------------------------------------ | ---------------------------------------- |
| [1983](../Page/1983_K-League.md "wikilink")         | [1](../Page/K-League.md "wikilink") | 5     | **4**  | \-                                   | \-                                       |
| [1984](../Page/1984_K-League.md "wikilink")         | 1                                   | 8     | **5**  | \-                                   | \-                                       |
| [1985](../Page/1985_K-League.md "wikilink")         | 1                                   | 8     | **2**  | \-                                   | \-                                       |
| [1986](../Page/1986_K-League.md "wikilink")         | 1                                   | 6     | **1**  | \-                                   | \-                                       |
| [1987](../Page/1987_K-League.md "wikilink")         | 1                                   | 5     | **2**  | \-                                   | \-                                       |
| [1988](../Page/1988_K-League.md "wikilink")         | 1                                   | 5     | **1**  | \-                                   | \-                                       |
| [1989](../Page/1989_K-League.md "wikilink")         | 1                                   | 6     | **4**  | \-                                   | \-                                       |
| [1990](../Page/1990_K-League.md "wikilink")         | 1                                   | 6     | **3**  | \-                                   | \-                                       |
| [1991](../Page/1991_K-League.md "wikilink")         | 1                                   | 6     | **3**  | \-                                   | \-                                       |
| [1992](../Page/1992_K-League.md "wikilink")         | 1                                   | 6     | **1**  | \-                                   | \-                                       |
| [1993](../Page/1993_K-League.md "wikilink")         | 1                                   | 6     | **4**  | \-                                   | \-                                       |
| [1994](../Page/1994_K-League.md "wikilink")         | 1                                   | 7     | **3**  | \-                                   | \-                                       |
| [1995](../Page/1995_K-League.md "wikilink")         | 1                                   | 8     | **2**  | \-                                   | \-                                       |
| [1996](../Page/1996_K-League.md "wikilink")         | 1                                   | 9     | **3**  | 冠军                                   | \-                                       |
| [1997](../Page/1997_K-League.md "wikilink")         | 1                                   | 10    | **4**  | 半决赛                                  | 冠军                                       |
| [1998](../Page/1998_K-League.md "wikilink")         | 1                                   | 10    | **3**  | 半决赛                                  | 冠军                                       |
| [1999](../Page/1999_K-League.md "wikilink")         | 1                                   | 10    | **5**  | 16强                                  | 1/4决赛                                    |
| [2000](../Page/2000_K-League.md "wikilink")         | 1                                   | 10    | **9**  | 1/4决赛                                | \-                                       |
| [2001](../Page/2001_K-League.md "wikilink")         | 1                                   | 10    | **5**  | 亚军                                   | \-                                       |
| [2002](../Page/2002_K-League.md "wikilink")         | 1                                   | 10    | **6**  | 亚军                                   | \-                                       |
| [2003](../Page/2003_K-League.md "wikilink")         | 1                                   | 12    | **7**  | 1/4决赛                                | \-                                       |
| [2004](../Page/2004_K-League.md "wikilink")         | 1                                   | 13    | **2**  | 32强                                  | \-                                       |
| [2005](../Page/2005_K-League.md "wikilink")         | 1                                   | 13    | **5**  | 1/4决赛                                | \-                                       |
| [2006](../Page/2006_K-League.md "wikilink")         | 1                                   | 14    | **3**  | 16强                                  | \-                                       |
| [2007](../Page/2007_K-League.md "wikilink")         | 1                                   | 14    | **1**  | 亚军                                   | \-                                       |
| [2008](../Page/2008_K-League.md "wikilink")         | 1                                   | 14    | **5**  | 冠军                                   | 小组赛                                      |
| [2009](../Page/2009_K-League.md "wikilink")         | 1                                   | 15    | **3**  | 1/4决赛                                | 冠军                                       |
| [2010](../Page/2010_K-League.md "wikilink")         | 1                                   | 15    | **9**  | 16强                                  | 1/4决赛                                    |
| [2011](../Page/2011_K-League.md "wikilink")         | 1                                   | 16    | **3**  | 半决赛                                  | \-                                       |
| [2012](../Page/2012_K-League.md "wikilink")         | 1                                   | 16    | **3**  | 冠军                                   | 小组赛                                      |
| [2013](../Page/2013_K_League_Classic.md "wikilink") | 1                                   | 14    | **1**  | 冠军                                   | 小组赛                                      |
| [2014](../Page/2014_K_League_Classic.md "wikilink") | 1                                   | 12    | 4      | 16强                                  | 1/4决赛                                    |
| [2015](../Page/2015_K_League_Classic.md "wikilink") | 1                                   | 12    | **3**  | 8强                                   | \-                                       |
| [2016](../Page/2016_K_League_Classic.md "wikilink") | 1                                   | 12    |        | 32强                                  | 小组赛                                      |

## 殊榮

### 國内聯賽

  - **[經典K聯賽](../Page/經典K聯賽.md "wikilink")**
    [无框](https://zh.wikipedia.org/wiki/File:K_League_Classic_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:K_League_Classic_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:K_League_Classic_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:K_League_Classic_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:K_League_Classic_Trophy.png "fig:无框")

<!-- end list -->

  -
    ** 冠軍（5）**： 1986, 1988, 1992, 2007, 2013
    亞軍（4）： 1985, 1987, 1995, 2004

### 國内盃賽

  - **[韓國足總盃](../Page/韓國足總盃.md "wikilink")**
    [无框](https://zh.wikipedia.org/wiki/File:Korean_FA_Cup_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:Korean_FA_Cup_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:Korean_FA_Cup_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:Korean_FA_Cup_Trophy.png "fig:无框")

<!-- end list -->

  -
    ** 冠軍（4）**： 1996, 2008, 2012, 2013
    亞軍（3）： 2001, 2002, 2007

<!-- end list -->

  - **[韓國聯賽盃](../Page/韩国联赛杯.md "wikilink")**

<!-- end list -->

  -
    ** 冠軍（2）**： 1993, 2009
    亞軍（2）： 1996, 1997

<!-- end list -->

  - **韓國全國足球錦標賽**

<!-- end list -->

  -

    亞軍（2）： 1977, 1985

<!-- end list -->

  - **韓國總統盃**

<!-- end list -->

  -
    ** 冠軍（1）**： 1974
    亞軍（1）： 1989

### 亞洲賽事

  - **[亞足聯冠軍聯賽](../Page/亞足聯冠軍聯賽.md "wikilink")**
    [无框](https://zh.wikipedia.org/wiki/File:AFC_Champions_League_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:AFC_Champions_League_Trophy.png "fig:无框")[无框](https://zh.wikipedia.org/wiki/File:AFC_Champions_League_Trophy.png "fig:无框")

<!-- end list -->

  -
    ** 冠軍（3）**： [1996-97](../Page/1996–97年亞洲球會錦標賽.md "wikilink"),
    [1997-98](../Page/1997–98年亞洲球會錦標賽.md "wikilink"),
    [2009](../Page/2009年亞足聯冠軍聯賽.md "wikilink")

<!-- end list -->

  - **[亞洲超級盃](../Page/亞洲超級盃.md "wikilink")**

<!-- end list -->

  -
    ** 冠軍（2）**： 1997，1998

<!-- end list -->

  - **[A3冠軍盃](../Page/A3冠军杯.md "wikilink")**

<!-- end list -->

  -

    亞軍（1）： [2005](../Page/2005年A3冠军杯.md "wikilink")

### 世界賽事

  - **[世界冠軍球會盃](../Page/世界冠軍球會盃.md "wikilink")**

<!-- end list -->

  -

    季軍（1）： [2009](../Page/2009年世界冠軍球會盃.md "wikilink")

<!-- end list -->

  - **亞非俱樂部錦標賽**

<!-- end list -->

  -

    亞軍（2）： 1997，1998

### 友好比賽

  - **[賀歲盃](../Page/賀歲盃足球賽.md "wikilink")**

<!-- end list -->

  -
    ** 冠軍（1）**：[2010](../Page/2010年賀歲盃足球賽.md "wikilink")

## 球員名單

## 俱乐部主要职员

**教练团队**

  - 主教练 ：  [金基東](../Page/金基東.md "wikilink")
  - 助理教练 :  [金基東](../Page/金基東.md "wikilink")(兼職)
  - 体能教练 :  Luis Flavio Ribeiro Buongermino
  - 守门员教练 :  李大喜
  - 领队 :  Kim Tae-Soo,  Ahn Seung-Hoon
  - 翻译 :  孔完倍

**附属团队**

  - U-18 主教练 :  Lee Chang-Won
  - U-18 助理教练 :  Baek Ki-Tae
  - U-15 主教练 :  Kim Dong-Young
  - U-15 助理教练 :
  - U-12 主教练 :  Kim Sung-Jin
  - U-12 助理教练 :  Oh Jin-Kwang
  - 守门员教练 :  Gjorgji Jovanovski
  - 青年队教练 (北区) :  Lee Young-Hwan
  - 青年队教练 (南区) :  Na Yeong-Chae

<!-- end list -->

  - 青年俱乐部官员 :  Shin Joo-Hyun

## 历任教练

只统计[K聯賽比赛](../Page/K聯賽.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>姓名</p></th>
<th><p>任职开始时间</p></th>
<th><p>任职结束时间</p></th>
<th><p>赛季</p></th>
<th><p>胜</p></th>
<th><p>平</p></th>
<th><p>负</p></th>
<th><p>附注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/Han_Hong-Ki.md" title="wikilink">Han Hong-Ki</a></p></td>
<td><p>1973/05/02</p></td>
<td><p>1984/11/29</p></td>
<td><p>1983–1984</p></td>
<td><p>16</p></td>
<td><p>11</p></td>
<td><p>17</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p></p></td>
<td><p>1984/11/29</p></td>
<td><p>1986/12/16</p></td>
<td><p>1985–1986</p></td>
<td><p>20</p></td>
<td><p>16</p></td>
<td><p>21</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p></p></td>
<td><p>1986/12/16</p></td>
<td><p>1992/12/31</p></td>
<td><p>1987–1992</p></td>
<td><p>75</p></td>
<td><p>70</p></td>
<td><p>61</p></td>
<td><p>包括 <a href="../Page/Kim_Soon-Ki.md" title="wikilink">Kim Soon-Ki</a>, <a href="../Page/Kim_Chul-Soo_(footballer).md" title="wikilink">Kim Chul-Soo</a><br />
和 的执教成绩</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Caretaker_manager.md" title="wikilink">代</a></p></td>
<td><p><a href="../Page/Kim_Soon-Ki.md" title="wikilink">Kim Soon-Ki</a><br />
 <a href="../Page/Kim_Chul-Soo_(footballer).md" title="wikilink">Kim Chul-Soo</a></p></td>
<td><p>1989/04/29</p></td>
<td><p>1989/??/??</p></td>
<td><p>1989</p></td>
<td></td>
<td></td>
<td></td>
<td><p>李會擇出任<a href="../Page/韩国国家足球队.md" title="wikilink">韩国国家足球队主教练率队参加</a><a href="../Page/1990年世界杯足球赛.md" title="wikilink">1990年世界杯足球赛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Caretaker_manager.md" title="wikilink">代</a></p></td>
<td><p><a href="../Page/Cho_Yoon-Ok.md" title="wikilink">Cho Yoon-Ok</a></p></td>
<td><p>1989/09/??</p></td>
<td><p>1989/??/??</p></td>
<td><p>1989</p></td>
<td></td>
<td></td>
<td></td>
<td><p>李會擇出任<a href="../Page/韩国国家足球队.md" title="wikilink">韩国国家足球队主教练率队参加</a><a href="../Page/1990年世界杯足球赛.md" title="wikilink">1990年世界杯足球赛</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/許丁茂.md" title="wikilink">許丁茂</a></p></td>
<td><p>1993/01/??</p></td>
<td><p>1995/11/25</p></td>
<td><p>1993–1995</p></td>
<td><p>42</p></td>
<td><p>40</p></td>
<td><p>24</p></td>
<td><p>包括<a href="../Page/Kim_Soon-Ki.md" title="wikilink">Kim Soon-Ki的执教成绩</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Caretaker_manager.md" title="wikilink">代</a></p></td>
<td><p><a href="../Page/Kim_Soon-Ki.md" title="wikilink">Kim Soon-Ki</a></p></td>
<td><p>1994/??/??</p></td>
<td><p>1994/07/??</p></td>
<td><p>1994</p></td>
<td></td>
<td></td>
<td></td>
<td><p>許丁茂出任<a href="../Page/韩国国家足球队.md" title="wikilink">韩国国家足球队助理教练参加</a><a href="../Page/1994年世界杯足球赛.md" title="wikilink">1994年世界杯足球赛</a></p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/朴成華.md" title="wikilink">朴成華</a></p></td>
<td><p>1995/12/12</p></td>
<td><p>2000/07/31</p></td>
<td><p>1996–2000</p></td>
<td><p>76</p></td>
<td><p>47</p></td>
<td><p>59</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Caretaker_manager.md" title="wikilink">代</a></p></td>
<td><p></p></td>
<td><p>2000/08/01</p></td>
<td><p>2003/12/31</p></td>
<td><p>2000</p></td>
<td><p>57</p></td>
<td><p>47</p></td>
<td><p>59</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2001/01/01</p></td>
<td><p>2004/12/05</p></td>
<td><p>2001–2004</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/塞尔吉奥·法里亚斯.md" title="wikilink">塞尔吉奥·法里亚斯</a></p></td>
<td><p>2005/01/06</p></td>
<td><p>2009/12/20</p></td>
<td><p>2005–2009</p></td>
<td><p>83</p></td>
<td><p>55</p></td>
<td><p>43</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p></p></td>
<td><p>2010/01/08</p></td>
<td><p>2010/05/10</p></td>
<td><p>2010</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Caretaker_manager.md" title="wikilink">代</a></p></td>
<td><p></p></td>
<td><p>2010/05/11</p></td>
<td><p>2010/11/08</p></td>
<td><p>2010</p></td>
<td><p>7</p></td>
<td><p>8</p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/黄善洪.md" title="wikilink">黄善洪</a></p></td>
<td><p>2010/12/13</p></td>
<td><p>现在</p></td>
<td><p>2011–现在</p></td>
<td><p>21</p></td>
<td><p>8</p></td>
<td><p>8</p></td>
<td></td>
</tr>
</tbody>
</table>

## 球衣赞助商

  - 1982–1987 : [阿迪达斯](../Page/阿迪达斯.md "wikilink")
  - 1987–1989 :  (1987年10月开始使用)
  - 1990–1992 : [阿迪达斯](../Page/阿迪达斯.md "wikilink")
  - 1993–1995 :
  - 1996–2001 : [阿迪达斯](../Page/阿迪达斯.md "wikilink")
  - 2002 : [迪亚多纳](../Page/迪亚多纳.md "wikilink")
  - 2003–2005 : [彪马](../Page/彪马.md "wikilink")
  - 2006–2012 : [卡帕](../Page/卡帕_\(公司\).md "wikilink")
  - 2013–2014: 阿特米
  - 2015–现在:[Hummel](../Page/Hummel.md "wikilink")

## 相關條目

  - [浦項鋼鐵](../Page/浦項鋼鐵.md "wikilink")

## 外部連結

  - [浦項鐵人官網](http://www.steelers.co.kr/)
  - [浦項球迷會"marines"](https://web.archive.org/web/20091027063219/http://www.marines.or.kr/)

[Category:韓國足球俱樂部](../Category/韓國足球俱樂部.md "wikilink")
[Category:1973年建立的足球俱樂部](../Category/1973年建立的足球俱樂部.md "wikilink")
[Category:浦項市](../Category/浦項市.md "wikilink")