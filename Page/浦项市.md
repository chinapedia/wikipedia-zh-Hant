**浦项市**（）是[韩国](../Page/韩国.md "wikilink")[庆尚北道的一个市](../Page/庆尚北道.md "wikilink")，东临[日本海](../Page/日本海.md "wikilink")，人口51萬9907人（2011年）。面积1127.24平方公里。

浦项是世界最大的钢铁公司之一—[浦项钢铁的总部所在](../Page/浦项钢铁.md "wikilink")。1970年代初浦項製铁所誕生以来是韓國有数的工業都市。

## 行政

  - [南区](../Page/南区_\(浦項市\).md "wikilink")
  - [北区](../Page/北区_\(浦項市\).md "wikilink")

## 略史

  - 1914年 - 迎日郡成立。
  - 1931年 - 浦項面升为邑。
  - 1949年 - 浦項邑昇格为市。
  - 1950年 -
    [浦項戰役爆發](../Page/浦項戰役.md "wikilink")，[大韓民國國軍](../Page/大韓民國國軍.md "wikilink")、[美軍與](../Page/美軍.md "wikilink")[朝鮮人民軍在浦項交戰](../Page/朝鮮人民軍.md "wikilink")
  - 1962年 - 指定国際開港場。
  - 1973年 - 浦項製铁所第一期工事完成、操業開始。
  - 1995年 - 浦項市・迎日郡合并市域扩大、設置北区和南区。

## 气候

  - 最高气温極値38.6℃（1994年7月14日）
  - 最低气温極値-15.0℃（1945年1月5日）

|                                     | 1月    | 2月    | 3月   | 4月   | 5月   | 6月    | 7月    | 8月    | 9月    | 10月  | 11月  | 12月   | 年      |
| ----------------------------------- | ----- | ----- | ---- | ---- | ---- | ----- | ----- | ----- | ----- | ---- | ---- | ----- | ------ |
| 平均气温（[C](../Page/摄氏.md "wikilink")） | 1.6   | 3.1   | 7.4  | 13.4 | 17.9 | 21.0  | 24.8  | 25.4  | 21.3  | 16.1 | 9.0  | 4.0   | 13.8   |
| 最高气温（[C](../Page/摄氏.md "wikilink")） | 6.5   | 8.1   | 12.3 | 18.5 | 23.1 | 25.3  | 28.8  | 29.3  | 25.4  | 21.2 | 15.0 | 9.2   | 18.6   |
| 最低气温（[C](../Page/摄氏.md "wikilink")） | \-2.4 | \-1.1 | 3.0  | 8.6  | 13.1 | 17.5  | 21.7  | 22.4  | 17.7  | 11.8 | 5.6  | \-0.1 | 9.8    |
| 降水量（mm）                             | 40.5  | 43.4  | 67.1 | 79.4 | 74.6 | 138.9 | 182.4 | 207.9 | 159.7 | 52.3 | 47.9 | 26.2  | 1120.3 |

**浦項平均气温与降水量**\[1\]

## 軍事

浦項是[韓国海军陆战隊司令部及](../Page/韓国海军陆战隊.md "wikilink")師部所在地，[韓國海軍在此亦設有軍港及航空基地](../Page/大韓民國海軍.md "wikilink")\[2\]。[美國海軍陸戰隊在韓國唯一的設施](../Page/美國海軍陸戰隊.md "wikilink")－小規模基地「」（）位於浦項市區以南不遠處。

## 交通

  - 与[鬱陵島通旅客船](../Page/鬱陵島.md "wikilink")。
  - [韓国铁道公社](../Page/韓国铁道公社.md "wikilink")・[東海南部線](../Page/東海南部線.md "wikilink")[浦項站](../Page/浦項站.md "wikilink")
  - [浦項機場](../Page/浦項機場.md "wikilink")（与[首尔特別市](../Page/首尔特別市.md "wikilink")[金浦国際机场](../Page/金浦国際机场.md "wikilink")、[济州市通飞机](../Page/济州市.md "wikilink")）
  - [益山浦項高速公路](../Page/益山浦項高速公路.md "wikilink")
      - 杞渓交流道 - 浦項收費站 - 浦項交流道

## 教育

### 大学

  - [浦項工科大學](../Page/浦項工科大學.md "wikilink")（POSTECH）
  - [韓東國際大學](../Page/韓東國際大學.md "wikilink")

### 専門大学

  - [浦項大學](../Page/浦項大學.md "wikilink")
  - [善隣大学](../Page/善隣大学_\(韓国\).md "wikilink")
  - [韓国理工6大学](../Page/韓国理工6大学.md "wikilink")

## 体育

浦項製铁所的ー部分设立职业足球俱乐部[浦項制铁足球俱乐部](../Page/浦項制铁足球俱乐部.md "wikilink")、韓国第ー个俱乐部専用球场[浦項鋼園球場](../Page/浦項鋼園球場.md "wikilink")。截止2009年夺得[K联赛](../Page/K联赛.md "wikilink")4次冠军，[亚足联冠军联赛](../Page/亚足联冠军联赛.md "wikilink")3次冠军。

## 出身名人

  - [李相得](../Page/李相得.md "wikilink")
  - [李同國](../Page/李同國.md "wikilink") - 足球選手
  - [宋智孝](../Page/宋智孝.md "wikilink") - 演員、主持人
  - [申惠真](../Page/申惠真.md "wikilink") - [Oh My
    Girl成員](../Page/Oh_My_Girl.md "wikilink")
  - [李彩煐](../Page/李彩煐.md "wikilink") -
    [Fromis_9成員](../Page/Fromis_9.md "wikilink")

## 姊妹都市・友好都市

  - 　中華人民共和國[吉林省](../Page/吉林省.md "wikilink")[琿春市](../Page/琿春市.md "wikilink")

  - 　中華人民共和國[廣西壯族自治區](../Page/廣西壯族自治區.md "wikilink")[北海市](../Page/北海市.md "wikilink")

  - 　日本[广島县](../Page/广島县.md "wikilink")[福山市](../Page/福山市.md "wikilink")

  - 　日本[新潟县](../Page/新潟县.md "wikilink")[上越市](../Page/上越市.md "wikilink")

  - 　美国[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[長灘
    (加利福尼亞州)](../Page/長灘_\(加利福尼亞州\).md "wikilink")

  - 　美国加利福尼亚州匹兹堡

  - （国内）[全羅南道光陽市](../Page/全羅南道.md "wikilink")

  - （国内）[全羅北道扶安郡](../Page/全羅北道.md "wikilink")

## 圖片

<File:Korea-Pohang-Guryongpo> Beach-Visitors-01.jpg|浦項海灘 <File:Pohang>
Yangdeok-dong City.jpg|城市外景 <File:Pohang> Yunghan Crossroad.jpg|浦項道路
<File:Pohang> Jungsangga Shilgaecheon Night view 2012 11 26.jpg|街頭夜景
<File:Pohang> Bus Terminal.jpg|缩略图|浦項長途客運站

## 相關條目

  - [慶尚北道](../Page/慶尚北道.md "wikilink")
  - [北区 (浦項市)](../Page/北区_\(浦項市\).md "wikilink")
  - [南区 (浦項市)](../Page/南区_\(浦項市\).md "wikilink")
  - [浦項戰役](../Page/浦項戰役.md "wikilink")（1950年）

## 參考

[浦項市](../Category/浦項市.md "wikilink")
[Category:韓國城市](../Category/韓國城市.md "wikilink")
[Category:慶尚北道](../Category/慶尚北道.md "wikilink")
[Category:日本海/朝鮮東海沿海城市](../Category/日本海/朝鮮東海沿海城市.md "wikilink")

1.  [大韓民国气象厅](http://www.kma.go.kr/)
2.  [沧浪客军事园－\>军队建设－\>韩国－\>第1海军陆战师](http://www.clk-mil.com/jdjs/kr/krlz1.htm)