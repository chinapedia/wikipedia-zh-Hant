[Awatdevatasupperlevel01.JPG](https://zh.wikipedia.org/wiki/File:Awatdevatasupperlevel01.JPG "fig:Awatdevatasupperlevel01.JPG")的Devatas女神浮雕\]\]

**浮雕**是[雕刻的一种](../Page/雕刻.md "wikilink")，艺术家在一块平板上将他要塑造的形象雕刻出来，使它脱离原来材料的平面。浮雕的材料有石头、木头、象牙和金属等，一般分为[浅浮雕](../Page/浅浮雕.md "wikilink")、[高浮雕和](../Page/高浮雕.md "wikilink")[凹雕](../Page/凹雕.md "wikilink")3种。

浮雕艺术在世界上到处都有，从[古埃及到](../Page/古埃及.md "wikilink")[古希腊和](../Page/古希腊.md "wikilink")[古罗马的神庙和墓碑的雕塑](../Page/古罗马.md "wikilink")，在[中国的庙宇](../Page/中国.md "wikilink")、洞窟和君王的陵墓也有许多浮雕艺术，著名的比如有[昭陵六骏](../Page/昭陵六骏.md "wikilink")。

埃及的“负”浮雕，浮雕的形象沉入材料的平面裡，是世界上绝无仅有的。

## 圖像

<center>

<File:Qajari>
relief.jpg|[卡扎尔时代的波斯浮雕](../Page/卡扎尔王朝.md "wikilink")，[波斯波利斯风格](../Page/波斯波利斯.md "wikilink")，位于[Tangeh
Savashi](../Page/:en:Tangeh_Savashi.md "wikilink") <File:Clonfert>
angels-north (ajusted)
20006-06-21.jpg|[克朗弗特大教堂的天使](../Page/克朗弗特大教堂.md "wikilink")，位于[爱尔兰](../Page/爱尔兰.md "wikilink")[高维郡](../Page/高维郡.md "wikilink")
<File:Mayapanel.JPG>|[墨西哥古城](../Page/墨西哥.md "wikilink")[雅克其兰的](../Page/雅克其兰.md "wikilink")[石楣](../Page/石楣.md "wikilink")24，描述[血祭的](../Page/血祭.md "wikilink")[玛雅雕刻](../Page/玛雅.md "wikilink")

</center>

## 參考文獻

  - Avery, Charles, in [Grove Art
    Online](../Page/:en:Oxford_Art_Online.md "wikilink"), "Relief
    sculpture",Retrieved April 7, 2011.

## 外部链接

  - Heilbrunn Timeline of Art History, ["American Relief
    Sculpture"](http://www.metmuseum.org/toah/hd/amrs/hd_amrs.htm),
    Metropolitan Museum of Art, [New
    York](../Page/New_York.md "wikilink").
  - Melissa Hardiman, ["Bas-Relief
    Pathfinder"](https://web.archive.org/web/20080919065201/http://edweb.tusd.k12.az.us/Sabino/library/pathfindermelissa2.htm)

[人物浮雕](http://www.quyangdiaoke.net)

[Category:雕塑](../Category/雕塑.md "wikilink")