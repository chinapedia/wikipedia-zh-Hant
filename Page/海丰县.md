**海丰县**（传统外文：）取义“臨海物丰”\[1\]，是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[汕尾市下辖的一个县](../Page/汕尾.md "wikilink")，位于[广东省东南海滨](../Page/广东省.md "wikilink")，处在[惠州市](../Page/惠州市.md "wikilink")[惠东县和](../Page/惠东县.md "wikilink")[汕尾市](../Page/汕尾市.md "wikilink")[陆丰市之间](../Page/陆丰市.md "wikilink")。

## 歷史

### 古代

海丰何时置县，史存二说。一说汉，依据[唐代](../Page/唐代.md "wikilink")[杜佑](../Page/杜佑.md "wikilink")《[通典](../Page/通典.md "wikilink")》和[宋代乐史](../Page/宋代.md "wikilink")《太平寰宇记》皆记录海丰为“汉旧县”。又有一说[東晉](../Page/東晉.md "wikilink")[咸和六年](../Page/咸和_\(东晋\).md "wikilink")（331年）始建海豐縣。

西汉元鼎六年（前111年）汉闽越王郢出兵攻南越，南越请求内附。南越平定后，建为汉十三州之一的交州，又增设了两个县揭阳县（今潮汕梅榕一带）、中宿县（今清远一带）。在汉代，海丰全境属交州南海郡傅罗县。东晋咸和元年（326年）析南海郡东部置东官郡。咸和六年（331）年，从博罗县析出宝安县、海丰县、安怀县（今东莞市）；同年，原揭阳县拆为海阳县、潮阳县、绥安县（今福建省云霄县）、海宁县等四县。海丰县，隶属东官郡，辖地包括现海丰全境及惠来、普宁、揭西之部分地区。

[隋文帝开皇十一年](../Page/隋文帝.md "wikilink")（591），并梁化郡、东官郡和南海郡一部分置循州（郡治今惠州），海丰改属循州。隋炀帝大业三年（607）改州为郡，循州改为龙川郡，海丰属龙川郡。

[唐高祖武德五年](../Page/唐高祖.md "wikilink")（622），龙川郡复为循州，海丰划东部置安陆县（今陆丰县）。太宗贞观元年（627），安陆县复归海丰县，隶属岭南道循州。
[武则天天授元年](../Page/武则天.md "wikilink")（690）改循州为雷乡州。玄宗天宝元年（742）改雷乡州为海丰郡（郡治在归善）。肃宗乾元元年（758）复改为循州，属循州。

[五代十国时期](../Page/五代十国.md "wikilink")，岭南一带为南汉所辖。大宝元年（958），改循州为祯州，属祯州。

[北宋真宗天禧五年](../Page/北宋.md "wikilink")（1021）因避太子趟祯之讳改祯州为惠州，海丰县属广南东路惠州。

[元代海丰县属江西中书省广东道惠州路](../Page/元代.md "wikilink")。
[明代海丰属广东布政司惠州府](../Page/明代.md "wikilink")。[嘉靖三年](../Page/嘉靖.md "wikilink")（1524）海丰划出龙溪都合置[惠来县](../Page/惠来县.md "wikilink")。

[清代属](../Page/清代.md "wikilink")[广东省](../Page/广东省.md "wikilink")[惠州府](../Page/惠州府.md "wikilink")。[雍正九年](../Page/雍正.md "wikilink")（1731）海丰划出石帆、吉康、坊廓3都置[陆丰县](../Page/陆丰县.md "wikilink")。海丰存兴贤、石塘、杨安、金锡4都。旧志称：纵横不过270里，即东40里至白沙，东北50里至伯公凹，东南70里至大德港，皆接陆丰；西130里至鹅埠分水塘，西北80里至赤石大安峒，西南90里至小漠旺官圩，南50里至汕尾海，北40里至栅仔岭，皆抵归善（今惠阳）。

### 近代

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = 1963-05 1963年 海丰县.jpg | caption1 = 1963年的海丰县 }}
[中华民国二年](../Page/中华民国.md "wikilink")（1913年）属广东[潮循道](../Page/潮循道.md "wikilink")；民国9年撤潮循道，改属东江绥靖委员公署；民国19年（1930年）属第十区行政视察专员公署（包括6县），后改属第四区行政督察专员公署。
1927年11月中国第一个[苏维埃政权成立](../Page/苏维埃.md "wikilink")，称「海陆丰苏维埃」。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，县区域废都约，实行乡镇建制。1952年春，原属惠阳县的小漠、元澳2个建制乡，划为海丰县属。1958年冬原惠阳县的高潭区又划为海丰县属，至1962年7月高潭区复归惠阳县（今惠东县）。1988年1月，经国务院批准，在原海丰、陆丰两县的行政区域上设置地级[汕尾市](../Page/汕尾市.md "wikilink")，并析海丰县南部沿海的汕尾、[红草](../Page/红草镇.md "wikilink")、[马宫](../Page/马宫街道.md "wikilink")、[东涌](../Page/东涌镇_\(汕尾市\).md "wikilink")、[田墘](../Page/田墘街道.md "wikilink")、[捷胜](../Page/捷胜镇.md "wikilink")、[遮浪](../Page/遮浪街道.md "wikilink")7镇建置城区；设陆丰县北部山区的[河田](../Page/河田镇_\(陆河县\).md "wikilink")、[河口](../Page/河口镇_\(陆河县\).md "wikilink")、[新田](../Page/新田镇_\(陆河县\).md "wikilink")、[螺溪](../Page/螺溪镇_\(陆河县\).md "wikilink")、[水唇](../Page/水唇镇.md "wikilink")、[上护](../Page/上护镇.md "wikilink")、[南万](../Page/南万镇.md "wikilink")、[东坑](../Page/东坑镇_\(陆河县\).md "wikilink")8个镇设置陆河县。汕尾市管辖[城区](../Page/城区_\(汕尾市\).md "wikilink")、海丰县、陆丰县、陆河县。

1992年底，市城区设出田墘镇、遮浪镇，新建红海湾经济开发试验区。1995年，设原陆丰所辖华侨农场为华侨管理区。以上两区属市政府派出机构。1995年，经国务院批准，陆丰撤县建市（县级市），由省政府直辖，委托汕尾市人民政府代管。2011年5月21日，深汕特别合作区授牌仪式在广州举行。海丰县划出[鮜门](../Page/鲘门镇.md "wikilink")、[赤石](../Page/赤石镇.md "wikilink")、[鹅埠](../Page/鹅埠镇.md "wikilink")、[小漠四镇总面积](../Page/小漠镇.md "wikilink")463平方公里，规划控制面积约200平方公里的“深汕特别合作区”的功能区。2013年底，汕尾市城区、[红海湾经济开发区](../Page/红海湾.md "wikilink")、[梅陇农场](../Page/梅陇.md "wikilink")、鲘门镇四地组合成汕尾红海湾新区。

## 文化

### 語言

海豐縣大部分地區通行[海丰话](../Page/海丰话.md "wikilink")，属于[闽南语](../Page/闽南语.md "wikilink")[泉漳片](../Page/泉漳片.md "wikilink")，接近[漳州话](../Page/漳州话.md "wikilink")，在當地稱之為學佬話、福佬話。由於海豐話與[陸豐話存在很高地相似性](../Page/陸豐話.md "wikilink")，被合稱為[海陸豐話](../Page/海陸豐話.md "wikilink")。

北部部分地區通行[客家语](../Page/客家语.md "wikilink")，屬[海陆片](../Page/海陆片.md "wikilink")，与臺灣[海陸腔音系非常接近](../Page/海陸腔.md "wikilink")。

### 戏曲

盛行戲曲劇種為海陸豐[白字戲](../Page/白字戲.md "wikilink")（序號217編號IV-74）和[西秦戲](../Page/西秦戲.md "wikilink")（序號191編號IV-47），亦有[正字戲](../Page/正字戲.md "wikilink")（正音戲，序號159編號IV-15，主要分布在[陸豐市](../Page/陸豐.md "wikilink")），都已列進[中國](../Page/中國.md "wikilink")[國務院](../Page/國務院.md "wikilink")2006年發表的第1批[國家級非物質文化遺產名錄](../Page/國家級非物質文化遺產.md "wikilink")。

## 地理

海豐縣南臨[南中國海](../Page/南中國海.md "wikilink")，沿海多丘陵，西北部群峰起伏，中部則為平原。

## 行政區域

海豐縣下辖16个镇，分别为：[海城](../Page/海城镇_\(海丰县\).md "wikilink")、[附城](../Page/附城镇_\(海丰县\).md "wikilink")、[城東](../Page/城东镇_\(海丰县\).md "wikilink")、[黄羌](../Page/黄羌镇.md "wikilink")、[公平](../Page/公平镇_\(海丰县\).md "wikilink")、[平東](../Page/平东镇_\(海丰县\).md "wikilink")、[可塘](../Page/可塘镇.md "wikilink")、[大湖](../Page/大湖镇_\(海丰县\).md "wikilink")、[赤坑](../Page/赤坑镇_\(海丰县\).md "wikilink")、[陶河](../Page/陶河镇.md "wikilink")、[聯安](../Page/联安镇.md "wikilink")、[梅隴](../Page/梅陇镇_\(海丰县\).md "wikilink")、[小漠](../Page/小漠镇.md "wikilink")、[鵝埠](../Page/鹅埠镇.md "wikilink")、[赤石及](../Page/赤石镇.md "wikilink")[鮜門](../Page/鲘门镇.md "wikilink")。其中[深汕特別合作区由鮜门](../Page/深汕特別合作区.md "wikilink")、赤石、鹅埠、小漠四镇于2011年5月21日成立，为享有地级市一级管理权限的功能区\[2\]。

## 氣候

一月份均溫為14℃，七月均溫28℃，年均降水量2,382毫米，[夏季常有](../Page/夏季.md "wikilink")[颱風](../Page/颱風.md "wikilink")，為[廣東省暴雨中心之一](../Page/廣東省.md "wikilink")。

## 自然資源

  - [礦產](../Page/礦產.md "wikilink")：[錫](../Page/錫.md "wikilink")、[硫鐵](../Page/硫鐵.md "wikilink")、[鈦鐵](../Page/鈦鐵.md "wikilink")、[鎢](../Page/鎢.md "wikilink")、[鋯英石](../Page/鋯英石.md "wikilink")、[水晶](../Page/水晶.md "wikilink")、[鋁](../Page/鋁.md "wikilink")、[鈮](../Page/鈮.md "wikilink")、[綠柱石](../Page/綠柱石.md "wikilink")、[錳](../Page/錳.md "wikilink")、[銻](../Page/銻.md "wikilink")、[砷](../Page/砷.md "wikilink")、[雲母](../Page/雲母.md "wikilink")、[鉬](../Page/鉬.md "wikilink")、[銅](../Page/銅.md "wikilink")、[高嶺石](../Page/高嶺石.md "wikilink")、[耐火粘土等](../Page/耐火粘土.md "wikilink")。
  - [中藥材](../Page/中藥.md "wikilink")：[巴戟](../Page/巴戟.md "wikilink")、[板藍根](../Page/板藍根.md "wikilink")、[土茯苓](../Page/土茯苓.md "wikilink")、[天香爐](../Page/天香爐.md "wikilink")、[蛇舌草等](../Page/蛇舌草.md "wikilink")。
  - [野生動物](../Page/野生動物.md "wikilink")：
    [穿山甲](../Page/穿山甲.md "wikilink")、、[猴](../Page/猴.md "wikilink")、[鷹](../Page/鷹.md "wikilink")、[蠎蛇等](../Page/蠎蛇.md "wikilink")。

## 經濟

  - [農產品](../Page/農產品.md "wikilink")：[水稻](../Page/水稻.md "wikilink")、[甘薯](../Page/甘薯.md "wikilink")、[花生](../Page/花生.md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[大豆](../Page/大豆.md "wikilink")、[黃紅麻](../Page/黃紅麻.md "wikilink")、[煙葉等](../Page/煙葉.md "wikilink")。
  - [海產](../Page/海產.md "wikilink")：沿海有[鮜門](../Page/鮜門.md "wikilink")、[小漠等漁港](../Page/小漠.md "wikilink")。盛產[對蝦](../Page/對蝦.md "wikilink")、[龍蝦](../Page/龍蝦.md "wikilink")、[鮑魚](../Page/鮑魚.md "wikilink")、[魷魚海馬等](../Page/魷魚海馬.md "wikilink")。
  - [工業](../Page/工業.md "wikilink")：[紡織](../Page/紡織.md "wikilink")、[化工](../Page/化工.md "wikilink")、[電力](../Page/電力.md "wikilink")、[建材](../Page/建材.md "wikilink")、[製糖](../Page/製糖.md "wikilink")、[服裝](../Page/服裝.md "wikilink")、[手工藝等](../Page/手工藝.md "wikilink")。

2002年全縣國內生產總值81億3千萬元人民幣。

## 交通

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Haifeng County 2018 11 part1.jpg | caption1 = 翻新後的海麗大道及兩旁建築物 }}
[海麗大道](../Page/海麗大道.md "wikilink")、[沈海高速公路](../Page/沈海高速公路.md "wikilink")、[深汕公路](../Page/深汕公路.md "wikilink")、[324國道橫貫本县](../Page/324國道.md "wikilink")，[甬莞高速公路横跨该县北部](../Page/甬莞高速公路.md "wikilink")。
高铁[厦深铁路在鲘门镇设](../Page/厦深铁路.md "wikilink")[鲘门站](../Page/鲘门站.md "wikilink")。

## 旅遊

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Peng Pai Statue in Haifeng.jpg | caption1 = 彭湃铜像 }}

景點：海豐紅宮、紅場舊址、彭湃故居、陈炯明故居、陈炯明都督府、方饭亭等。

特產小吃：[草雞](../Page/草雞.md "wikilink")、[西坑茶](../Page/西坑茶.md "wikilink")、[高螺蠔豉](../Page/高螺蠔豉.md "wikilink")、[蠔油](../Page/蠔油.md "wikilink")、[梅隴蓮藕](../Page/梅隴蓮藕.md "wikilink")、[慈菰](../Page/慈菰.md "wikilink")、[荷蘭豆](../Page/荷蘭豆.md "wikilink")、[東平蕌頭](../Page/東平蕌頭.md "wikilink")、[赤坑乳豬](../Page/赤坑乳豬.md "wikilink")、[公平菜脯](../Page/公平菜脯.md "wikilink")、[梅隴菜粿](../Page/梅隴菜粿.md "wikilink")、[牛肉脯](../Page/牛肉脯.md "wikilink")、[海丰小米](../Page/海丰小米.md "wikilink")、[冬节鸽](../Page/冬节鸽.md "wikilink")、[菜茶](../Page/菜茶.md "wikilink")。

## 名人

### 政治

  - [彭湃](../Page/彭湃.md "wikilink")：[中國共產黨早期領導人之一](../Page/中國共產黨.md "wikilink")，中国现代农民运动领袖和先驱。
  - [陈炯明](../Page/陈炯明.md "wikilink")：[辛亥革命元勋](../Page/辛亥革命.md "wikilink")，曾任广东省总督、[中国致公党首任](../Page/中国致公党.md "wikilink")[总理](../Page/总理.md "wikilink")。
  - [陳演生](../Page/陳演生.md "wikilink")：中國致公黨創黨祕書長。
  - [陳其尤](../Page/陳其尤.md "wikilink")：中国致公党第3屆副主席，第4、第5、第6届中央主席。
  - [黃鼎臣](../Page/黃鼎臣.md "wikilink")：1927年參加中國共產黨、1946年加入中國致公黨，中國致公黨第7、第8屆中央主席。
  - [林劍虹](../Page/林劍虹.md "wikilink")：早期中國同盟會會員，1925年9月29日就任海豐縣縣長
  - [曾广顺](../Page/曾广顺.md "wikilink")：1984年5月至1993年任[中華民國僑務委員會委员长](../Page/中華民國僑務委員會.md "wikilink")、[行政院大陸委員會委员](../Page/行政院大陸委員會.md "wikilink")。1988年7月当选[中國國民黨第十三届中央委员](../Page/中國國民黨.md "wikilink")，并任中央常委委员。
  - [黃洋達](../Page/黃洋達.md "wikilink")：香港獨立主義政黨[熱血公民領導](../Page/熱血公民.md "wikilink")。

### 科技专业

  - [彭士祿](../Page/彭士祿.md "wikilink")：[中国工程院首批院士](../Page/中国工程院.md "wikilink")、核动力科学家、“中国核潜艇之父”。
  - [彭實戈](../Page/彭實戈.md "wikilink")：[中国科学院院士](../Page/中国科学院院士.md "wikilink")、著名数学家、中国金融数学的奠基人。
  - [黄旭华](../Page/黄旭华.md "wikilink")：中国第一代核潜艇研发人员、总工程师。

### 文学

  - [钟敬文](../Page/钟敬文.md "wikilink")：中国民间文学和[民俗学的开拓者之一](../Page/民俗学.md "wikilink")，“中国民俗学之父”。

### 音乐及演艺界

  - [马思聪](../Page/马思聪.md "wikilink")：中国作曲家、小提琴家与音乐教育家，“中国小提琴第一人”。
  - [吳大江](../Page/吳大江.md "wikilink")：民族音樂家、作曲家、指揮家。
  - [敖嘉年](../Page/敖嘉年.md "wikilink")：香港男艺人，电视演員。
  - [林保怡](../Page/林保怡.md "wikilink")：香港演員。
  - [蔡一智](../Page/蔡一智.md "wikilink")：香港歌手。
  - [蔡一傑](../Page/蔡一傑.md "wikilink")：香港歌手。
  - [徐克](../Page/徐克.md "wikilink")：香港導演。
  - [黎耀祥](../Page/黎耀祥.md "wikilink")：香港演員。

### 商界

  - [林世榮](../Page/林世榮_\(商人\).md "wikilink")：[香港上市公司恆豐金業科技集團](../Page/香港上市公司.md "wikilink")[金至尊珠寶的創辦人](../Page/金至尊珠寶.md "wikilink")。
  - [彭錦俊](../Page/彭錦俊.md "wikilink")：[香港上市公司](../Page/香港上市公司.md "wikilink")[俊和發展集團](../Page/俊和發展集團.md "wikilink")（）的创始人及前主席。

### 其他

  - [葉繼歡](../Page/葉繼歡.md "wikilink")：一代賊王。
  - [呂樂](../Page/呂樂.md "wikilink")：曾任香港[总华探长](../Page/总华探长.md "wikilink")，卸任之后被指控贪污，被称“五亿探长”。

## 圖片集

<File:Hoi> Fung 2018 10 part11.jpg|烏石村 <File:Hoi> Fung 2018 10
part12.jpg|海麗大道 <File:Hoi> Fung 2018 10 part13.jpg|私人住宅第一城一期 <File:Hoi>
Fung 2018 10 part14.jpg|私人住宅第一城二期 <File:Hoi> Fung 2018 10
part15.jpg|彭湃中學 <File:Hoi> Fung 2018 10 part16.jpg|附城鎮派出所
<File:Hoi> Fung 2018 10 part17.jpg|南湖客運車站 <File:Hoi> Fung 2018 10
part18.jpg|紅色文化街 <File:Hoi> Fung 2018 10 part19.jpg|紅場 <File:Hoi> Fung
2018 10 part20.jpg|購物城 <File:Hoi> Fung 2018 10 part21.jpg|藍天廣場
<File:Hoi> Fung 2018 10 part22.jpg|紅城大道

## 参考文献

### 引用

### 来源

  -
## 外部链接

  - [海丰商务](https://web.archive.org/web/20050923190552/http://www.sw163.com.cn/index0.htm)

{{-}}

[汕尾](../Category/广东省县份.md "wikilink")
[海丰县](../Category/海丰县.md "wikilink")
[县](../Category/汕尾区县市.md "wikilink")

1.
2.