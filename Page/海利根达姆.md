[Bad_Doberan_in_Germany.png](https://zh.wikipedia.org/wiki/File:Bad_Doberan_in_Germany.png "fig:Bad_Doberan_in_Germany.png")位置圖\]\]

**海利根达姆**（Heiligendamm）是位于[德国东北部](../Page/德国.md "wikilink")[梅克伦堡-前波美拉尼亚州](../Page/梅克伦堡-前波美拉尼亚州.md "wikilink")[波罗的海沿岸旅游城市](../Page/波罗的海.md "wikilink")[巴特多伯兰的一部分](../Page/巴特多伯兰.md "wikilink")。該市於1793年成立。

由於海岸線林立著古典白色建築，該城又被稱為「海邊白城」（[德語](../Page/德語.md "wikilink"):Die weiße
Stadt am
Meer）。今日海邊已是一座五星級飯店。一條[窄軌蒸氣鐵路](../Page/窄軌.md "wikilink")（即是著名的「[Molli](../Page/:en:Molli.md "wikilink")」），連結了海利根達姆與[Bad
Doberan城和](../Page/:en:Bad_Doberan.md "wikilink")[Kühlungsborn城之交通](../Page/:en:Kühlungsborn.md "wikilink")。

2006年7月13日，[美國](../Page/美國.md "wikilink")[總統](../Page/美國總統.md "wikilink")[喬治·沃克·布希在前往](../Page/喬治·沃克·布希.md "wikilink")[施特拉爾松與](../Page/施特拉爾松.md "wikilink")[德國](../Page/德國.md "wikilink")[總理](../Page/德國總理.md "wikilink")[安格拉·默克爾進行國事訪問的期間在海利根達姆停留](../Page/安格拉·默克爾.md "wikilink")。

2007年6月6日到8日，第33屆[八国集团首脑会议在海利根达姆举行](../Page/八国集团首脑会议.md "wikilink")。數以百計的[無政府主義與](../Page/無政府主義.md "wikilink")[反全球化示威者也因此於海利根達姆抗議](../Page/反全球化.md "wikilink")。

[Kurhaus,_Heiligendamm.jpg](https://zh.wikipedia.org/wiki/File:Kurhaus,_Heiligendamm.jpg "fig:Kurhaus,_Heiligendamm.jpg")

## 歷史

[Panoramaansicht_von_Heiligendamm_1887,_Godewind_Verlag.jpg](https://zh.wikipedia.org/wiki/File:Panoramaansicht_von_Heiligendamm_1887,_Godewind_Verlag.jpg "fig:Panoramaansicht_von_Heiligendamm_1887,_Godewind_Verlag.jpg")
[Heiligendamm_um_1841_Salon_und_Badehaus,_Godewind_Verlag.jpg](https://zh.wikipedia.org/wiki/File:Heiligendamm_um_1841_Salon_und_Badehaus,_Godewind_Verlag.jpg "fig:Heiligendamm_um_1841_Salon_und_Badehaus,_Godewind_Verlag.jpg")

海利根達姆已經被發展成適合貴族與上層社會的雅緻會議場地。而它最著名的的客人則是來自於[德意志帝國](../Page/德意志帝國.md "wikilink")。

1793年，它的第一個客人是[Grand Duke of Mecklenburg Frederick Francis
I](../Page/:en:Friedrich_Franz_I,_Grand_Duke_of_Mecklenburg-Schwerin.md "wikilink")，他讓這個勝地成為風尚。在1793年到1870年之間，
[Johann Christoph](../Page/Johann_Christoph.md "wikilink")、[Heinrich von
Seydwitz](../Page/Heinrich_von_Seydwitz.md "wikilink")、[Carl Theodor
Severin與](../Page/:de:Carl_Theodor_Severin.md "wikilink")[Gustav Adolph
Demmler為游泳與住宿創造了真實的](../Page/:de:Georg_Adolf_Demmler.md "wikilink")「[總體藝術](../Page/總體藝術.md "wikilink")」（[Gesamtkunstwerk](../Page/:en:Gesamtkunstwerk.md "wikilink")）。海利根達姆永遠是[德國最雅致的海濱勝地](../Page/德國.md "wikilink")。直到20世紀，來自全[歐洲的貴族依然將此地當作良好的避暑勝地](../Page/歐洲.md "wikilink")。

在[第二次世界大戰之後](../Page/第二次世界大戰.md "wikilink")，海利根達姆的建築被用來當作療養院。當[梅克倫堡](../Page/梅克倫堡.md "wikilink")（[Mecklenburg](../Page/:en:Mecklenburg.md "wikilink")）被[東德](../Page/德意志民主共和國.md "wikilink")[共產黨控制時](../Page/共產黨.md "wikilink")，一些海利根達姆著名的建築物被摧毀或是改建成實用的建築。在1989/1990年[兩德統一之後](../Page/兩德統一.md "wikilink")，一群投資者買下了當地大多數的建築物並且進行一個大規模的新生計畫。一座全新的建築，所謂的「[凱賓斯基飯店](../Page/凱賓斯基飯店.md "wikilink")」([Kempinski
Grand
Hotel](../Page/:en:Kempinski_Grand_Hotel_Heiligendamm.md "wikilink"))，於2003年春天開幕。而由於主要的街道與自行車道皆被移除或是變更，這樣的發展也導致一些當地居民的衝突。

## 照片

<File:Heiligendamm> May 2007
020.jpg|[凱賓斯基飯店](../Page/凱賓斯基飯店.md "wikilink")([Kempinski
Grand
Hotel](../Page/:en:Kempinski_Grand_Hotel_Heiligendamm.md "wikilink"))
<File:Heiligendamm> May 2007
015.jpg|[霍亨索倫王朝的城堡](../Page/霍亨索倫王朝.md "wikilink")，現在被[凱賓斯基飯店](../Page/凱賓斯基飯店.md "wikilink")
使用 <File:Heiligendamm> May 2007 001.jpg|過去的療養院
[File:Heiligendamm_Gedenkstein.jpg|創設紀念碑](File:Heiligendamm_Gedenkstein.jpg%7C創設紀念碑)
<File:Heiligendamm> May 2007 011.jpg|療養院的餐廳 <File:Heiligendamm> May 2007
016.jpg|[霍亨索倫王朝的城堡](../Page/霍亨索倫王朝.md "wikilink") <File:Heiligendamm>
May 2007 013.jpg <File:Heiligendamm> May 2007
002.jpg|[Kühlungsborn](../Page/:en:Kühlungsborn.md "wikilink")
<File:Heiligendamm_Molli.jpg>|[Molli](../Page/:en:Molli.md "wikilink")
<File:Heiligendamm.jpg>
<File:KempiHeilig3.jpg>|[霍亨索倫王朝的城堡](../Page/霍亨索倫王朝.md "wikilink")
[File:KempiHeilig1.jpg|療養院](File:KempiHeilig1.jpg%7C療養院)
<File:Heiligendamm> Strand.jpg

## 文學

**英文作品**

  - Charles James Apperley: *"Nimrods German Tour"* – a travel through
    northern Germany, especially to Heiligendamm in 1828. Publishing
    company: [Godewind
    Verlag](http://de.wikipedia.org/wiki/Godewind_Verlag), Germany 2006.
    ISBN 978-3-939198-70-3

**德文作品**

  - Hans Thielcke: *Die Bauten des Seebades Doberan - Heiligendamm um
    1800 und Ihr Baumeister Severin*. Godewind Verlag, 2004 ISBN
    978-3-938347-90-4. (Reprint d. Originalausgabe von 1917)
  - Friedrich Compart: *Geschichte des Klosters Doberan*. Godewind
    Verlag, 2004. ISBN 978-3-938347-07-2. (Reprint der Originalausgabe
    von 1872)
  - Heinrich Hesse: *Die Geschichte von Doberan-Heiligendamm*. Godewind
    Verlag, Wismar 2004, ISBN 978-3-938347-09-6. (Bearbeitete Neuauflage
    der Originalausgabe von 1838)
  - Adolf Nizze: *Doberan-Heiligendamm: Geschichte des ersten deutschen
    Seebades*. Godewind Verlag, Wismar 2004, ISBN 978-3-938347-23-2.
    (Bearbeitete Neuauflage der Originalausgabe von 1823)
  - *Die Reise eines Gesunden in die Seebäder Swinemünde, Putbus und
    Doberan*. Godewind Verlag, Wismar 2005, ISBN 978-3-938347-73-7.
    (Bearbeitete Neuauflage der Originalausgabe von 1823)
  - Hans-Jürgen Herbst: *Kalender 2007, Doberan & Heiligendamm, erstes
    deutsches Seebad*. Godewind Verlag, 2007, ISBN 978-3-938347-57-7.
  - Dr. Samuel G. Vogel: *Allgemeine Baderegeln zum Gebrauche für
    Badelustige überhaupt und diejenigen insbesondere, welche sich des
    Seebades in Doberan bedienen*. Godewind Verlag, 2004, ISBN
    978-3-938347-88-1. (Bearbeitete Neuauflage der Originalausgabe von
    1817)

## 外部链接

  - [官方网站](http://www.heiligendamm.de/index-eng.html)
  - [G8官方網站](http://www.g-8.de/Webs/G8/EN/Homepage/home.html)

[H](../Category/梅克伦堡-前波美拉尼亚州.md "wikilink")