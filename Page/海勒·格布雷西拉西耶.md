**海力·格布列塞拉希**（，haylē gebre
silassē；，），亦有部份香港媒體譯作**基比拉沙**\[1\]、**基比斯拉斯**\[2\]，[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")[长跑运动员](../Page/长跑.md "wikilink")，他曾是[马拉松](../Page/马拉松.md "wikilink")、[10公里馬拉松](../Page/10公里馬拉松.md "wikilink")、[20000公尺賽跑](../Page/20000公尺賽跑.md "wikilink")、[半程馬拉松和](../Page/半程馬拉松.md "wikilink")[1小時限時跑](../Page/1小時限時跑.md "wikilink")[世界纪录保持者](../Page/世界纪录.md "wikilink")。

## 宣佈放棄參加2008年北京奧運馬拉松賽事

2008年3月10日，基比沙拉錫宣布，由於[北京市](../Page/北京市.md "wikilink")[空氣污染問題嚴重](../Page/空氣污染.md "wikilink")，故不參加[2008年夏季奧林匹克運動會的](../Page/2008年夏季奧林匹克運動會.md "wikilink")[馬拉松賽事](../Page/2008年夏季奧林匹克運動會田徑比賽.md "wikilink")，避免其[哮喘病發](../Page/哮喘.md "wikilink")，但他仍會參加10,000米賽事。\[3\]\[4\]\[5\]\[6\]\[7\]\[8\]

## 參考資料

## 外部連結

  -
  - [Marathon World Record –
    Berlin 2008](http://www.ethiotube.net/video/905/Haile-breaks-the-Marathon-WR-Again)
    – EthioTube Video

  - [Spikes Hero profile on
    www.spikesmag.com](https://web.archive.org/web/20080715081501/http://www.spikesmag.com/athletes/Heroes/hailegebrselassie.aspx)

  - [Gebrselassie approaching Record for Running
    Records](http://www.iaaf.org/news/news/gebrselassie-approaching-record-for-running-r)

  - [Gebrselassie's World Records, rankings, race history, recent news,
    and more.](https://netfiles.uiuc.edu/bpence2/www/Geb_Index.html)

  - [Pacing splits and analysis of Gebrselassie marathon world
    record](http://scienceofsport.blogspot.com/2007/10/haile-gebrselassie-world-record-splits.html)

  - [Berlin 2008: Gebrselassie runs 2:03:59 – race report and analysis
    of
    pacing](https://web.archive.org/web/20081002000255/http://www.sportsscientists.com/2008/09/haile-gebrselassie.html)

[Category:衣索比亞奧運田徑運動員](../Category/衣索比亞奧運田徑運動員.md "wikilink")
[Category:埃塞俄比亚马拉松运动员](../Category/埃塞俄比亚马拉松运动员.md "wikilink")
[Category:埃塞俄比亚中跑运动员](../Category/埃塞俄比亚中跑运动员.md "wikilink")
[Category:埃塞俄比亚长跑运动员](../Category/埃塞俄比亚长跑运动员.md "wikilink")
[Category:衣索比亞奧林匹克運動會金牌得主](../Category/衣索比亞奧林匹克運動會金牌得主.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會田徑運動員](../Category/1996年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會田徑運動員](../Category/2000年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會田徑運動員](../Category/2008年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")
[Category:田徑項目世界記錄保持者](../Category/田徑項目世界記錄保持者.md "wikilink")
[Category:柏林马拉松冠军](../Category/柏林马拉松冠军.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.

2.

3.
4.
5.

6.

7.

8.