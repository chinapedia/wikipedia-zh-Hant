**海南藏族自治州**（），簡稱**海南州**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[青海省下辖的](../Page/青海省.md "wikilink")[自治州](../Page/自治州.md "wikilink")。自治州政府驻[共和县](../Page/共和县.md "wikilink")。

## 历史

[汉平帝元始五年冬](../Page/汉平帝.md "wikilink")（1年）置西海郡，今共和、兴海两县为其辖地。唐至德元年，地入吐蕃。清[顺治六年](../Page/顺治.md "wikilink")（1649年），在今贵德县设归德所。[乾隆三年](../Page/乾隆.md "wikilink")（1738年），归德所改隶西宁府。

1953年12月31日建立自治区，1955年改自治州。

## 地理

海南州位于青海省的东部、[青海湖以南](../Page/青海湖.md "wikilink")。地处[青藏高原东北部](../Page/青藏高原.md "wikilink")，全州除青海南山外，呈西南向东北降低的趋势，由于[黄河流经中部](../Page/黄河.md "wikilink")，整个地势又以黄河为中轴轴向倾斜。

境内地形以山地为主，四周环山，西部與西南部有[鄂拉山](../Page/鄂拉山.md "wikilink")。盆地居中，高原丘陵和河谷台地相间其中，地势起伏较大，复杂多样。全州平均海拔在3000米以上，最高海拔5305米，最低海拔2168米。黄河横贯中南部5县20个乡镇，长411.3公里，有支流101条；以[青海湖为主的](../Page/青海湖.md "wikilink")6个内陆水系有支流40余条。

## 政治

### 现任领导

<table>
<caption>海南藏族自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党海南藏族自治州委员会.md" title="wikilink">中国共产党<br />
海南藏族自治州委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/海南藏族自治州人民代表大会.md" title="wikilink">海南藏族自治州人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/海南藏族自治州人民政府.md" title="wikilink">海南藏族自治州人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议海南藏族自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
海南藏族自治州委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/张文魁.md" title="wikilink">张文魁</a>[1]</p></td>
<td><p><a href="../Page/王建民_(1965年).md" title="wikilink">王建民</a>[2]</p></td>
<td><p><a href="../Page/索南东智.md" title="wikilink">索南东智</a>[3]</p></td>
<td><p><a href="../Page/李加曲.md" title="wikilink">李加曲</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/藏族.md" title="wikilink">藏族</a></p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>藏族</p></td>
<td><p>藏族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/青海省.md" title="wikilink">青海省</a><a href="../Page/祁连县.md" title="wikilink">祁连县</a></p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/砀山县.md" title="wikilink">砀山县</a></p></td>
<td><p>青海省<a href="../Page/化隆县.md" title="wikilink">化隆县</a></p></td>
<td><p>青海省<a href="../Page/海东市.md" title="wikilink">海东市</a><a href="../Page/乐都区.md" title="wikilink">乐都区</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2013年5月</p></td>
<td><p>2016年11月</p></td>
<td><p>2013年6月</p></td>
<td><p>2015年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

[Hainan_mcp.png](https://zh.wikipedia.org/wiki/File:Hainan_mcp.png "fig:Hainan_mcp.png")
下辖5个[县](../Page/县_\(中华人民共和国\).md "wikilink")：[共和县](../Page/共和县.md "wikilink")、[同德县](../Page/同德县.md "wikilink")、[贵德县](../Page/贵德县.md "wikilink")、[兴海县](../Page/兴海县.md "wikilink")、[贵南县](../Page/贵南县.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>区划代码[5]</p></th>
<th><p>区划名称<br />
藏文</p></th>
<th><p>汉语拼音<br />
威利转写</p></th>
<th><p>面积[6]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[7]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>镇</p></td>
<td><p>乡</p></td>
<td><p>民族乡</p></td>
<td><p>社区</p></td>
<td><p>行政村</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>632500</p></td>
<td><p>海南藏族自治州<br />
</p></td>
<td></td>
<td><p>43,453.24</p></td>
<td><p><a href="../Page/共和县.md" title="wikilink">共和县</a></p></td>
<td><p>813000</p></td>
<td><p>16</p></td>
</tr>
<tr class="odd">
<td><p>632521</p></td>
<td><p>共和县<br />
</p></td>
<td></td>
<td><p>16,626.73</p></td>
<td><p><a href="../Page/恰卜恰镇.md" title="wikilink">恰卜恰镇</a></p></td>
<td><p>813000</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>632522</p></td>
<td><p>同德县<br />
</p></td>
<td></td>
<td><p>4,652.80</p></td>
<td><p><a href="../Page/尕巴松多镇.md" title="wikilink">尕巴松多镇</a></p></td>
<td><p>813200</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>632523</p></td>
<td><p>贵德县<br />
</p></td>
<td></td>
<td><p>3,510.37</p></td>
<td><p><a href="../Page/河阴镇.md" title="wikilink">河阴镇</a></p></td>
<td><p>811700</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>632524</p></td>
<td><p>兴海县<br />
</p></td>
<td></td>
<td><p>12,177.63</p></td>
<td><p><a href="../Page/子科滩镇.md" title="wikilink">子科滩镇</a></p></td>
<td><p>813300</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>632525</p></td>
<td><p>贵南县<br />
</p></td>
<td></td>
<td><p>6,485.71</p></td>
<td><p><a href="../Page/茫曲镇.md" title="wikilink">茫曲镇</a></p></td>
<td><p>813100</p></td>
<td><p>3</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>海南藏族自治州各县人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[8]（2010年11月）</p></th>
<th><p>户籍人口[9]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>海南藏族自治州</p></td>
<td><p>441691</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>共和县</p></td>
<td><p>122966</p></td>
<td><p>27.84</p></td>
</tr>
<tr class="even">
<td><p>同德县</p></td>
<td><p>64369</p></td>
<td><p>14.57</p></td>
</tr>
<tr class="odd">
<td><p>贵德县</p></td>
<td><p>101771</p></td>
<td><p>23.04</p></td>
</tr>
<tr class="even">
<td><p>兴海县</p></td>
<td><p>76025</p></td>
<td><p>17.21</p></td>
</tr>
<tr class="odd">
<td><p>贵南县</p></td>
<td><p>76560</p></td>
<td><p>17.33</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")441689人\[10\]。同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加了39946人，增长9.94%。年平均增长率为0.95%。其中，男性人口为225963人，占51.16%；女性人口为215726人，占48.84%。总人口性别比（以女性为100）为104.75。0－14岁的人口为109262人，占24.74%；15－64岁的人口为309217人，占70.01%；65岁及以上的人口为23210人，占5.25%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")109694人，占24.84%；各[少数民族人口为](../Page/少数民族.md "wikilink")331995人，占75.16%。其中：[藏族](../Page/藏族.md "wikilink")292888人，占66.31%；[回族](../Page/回族.md "wikilink")30203人，占6.84%；[土族](../Page/土族.md "wikilink")3991人，占0.90%；[撒拉族](../Page/撒拉族.md "wikilink")1040人，占0.24%；[蒙古族](../Page/中国蒙古族.md "wikilink")3096人，占0.70%；其它少数民族777人，占0.17%。

{{-}}

| 民族名称         | [藏族](../Page/藏族.md "wikilink") | [汉族](../Page/汉族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [土族](../Page/土族.md "wikilink") | [蒙古族](../Page/中国蒙古族.md "wikilink") | [撒拉族](../Page/撒拉族.md "wikilink") | [东乡族](../Page/东乡族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ---------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | ---- |
| 人口数          | 292885                         | 109699                         | 30203                          | 3991                           | 3096                               | 1040                             | 349                              | 150                            | 63                               | 37                             | 178  |
| 占总人口比例（%）    | 66.31                          | 24.84                          | 6.84                           | 0.90                           | 0.70                               | 0.24                             | 0.08                             | 0.03                           | 0.01                             | 0.01                           | 0.04 |
| 占少数民族人口比例（%） | 88.22                          | \---                           | 9.10                           | 1.20                           | 0.93                               | 0.31                             | 0.11                             | 0.05                           | 0.02                             | 0.01                           | 0.05 |

**海南藏族自治州民族构成（2010年11月）**\[11\]

## 旅游

  - 日月山
  - 玉皇阁古建筑群

## 参考资料

[海南藏族自治州](../Category/海南藏族自治州.md "wikilink")
[青](../Category/藏族自治州.md "wikilink")
[Category:青海自治州](../Category/青海自治州.md "wikilink")

1.
2.
3.
4.
5.
6.  （[第二次全国土地调查数据](../Page/第二次全国土地调查.md "wikilink")）
7.
8.
9.
10.
11.