**海厄利亞**（）是[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[邁阿密-戴德縣的一個城市](../Page/邁阿密-戴德縣.md "wikilink")。面積51.1平方公里，2006年人口217,141人，是該州第六大城市。\[1\]

1925年建市。

## 相關條目

  - （），一座以海厄利亞市命名、廢止於2006年的[駐韓美軍基地](../Page/駐韓美軍.md "wikilink")，位於[韓國](../Page/韓國.md "wikilink")[釜山市](../Page/釜山市.md "wikilink")[釜山鎮区](../Page/釜山鎮区.md "wikilink")\[2\]。

## 参考文献

[Category:佛罗里达州城市](../Category/佛罗里达州城市.md "wikilink")
[Category:邁阿密-戴德縣城市](../Category/邁阿密-戴德縣城市.md "wikilink")
[Category:邁阿密都會區城市](../Category/邁阿密都會區城市.md "wikilink")
[Category:1925年建立的聚居地](../Category/1925年建立的聚居地.md "wikilink")
[Category:1925年佛羅里達州建立](../Category/1925年佛羅里達州建立.md "wikilink")

1.  [Hialeah, Florida - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=ChangeGeoContext&geo_id=16000US1230000&_geoContext=01000US%7C04000US37%7C16000US3710740&_street=&_county=Hialeah+city&_cityTown=Hialeah+city&_state=04000US12&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=010&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)
2.  [釜山廣域市 - 釜山市政 - 核心項目 -
    釜山市民公園](http://tchinese.busan.go.kr/02government/04_01.jsp)