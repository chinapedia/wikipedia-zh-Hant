[Lammermuirparty.jpg](https://zh.wikipedia.org/wiki/File:Lammermuirparty.jpg "fig:Lammermuirparty.jpg")

**海外基督使团**（**The Overseas Missionary Fellowship**或**OMF
International**）是一个[基督教](../Page/基督教.md "wikilink")[新教](../Page/新教.md "wikilink")[公理宗的](../Page/公理宗.md "wikilink")[差会](../Page/差会.md "wikilink")；它的前身是1865年由[英國](../Page/英國.md "wikilink")[傳教士](../Page/傳教士.md "wikilink")[戴德生创办的](../Page/戴德生.md "wikilink")**中國內地會**（**China
Inland Mission**，**CIM**），1964年改稱現名。

1854年，戴德生作为[郭实腊的](../Page/郭实腊.md "wikilink")[中国传教会传道人](../Page/中国传教会.md "wikilink")，首次从英国来到[中国](../Page/中国.md "wikilink")，先在[上海](../Page/上海市.md "wikilink")、[汕头等地传教](../Page/汕头市.md "wikilink")，1857年定居[宁波](../Page/宁波市.md "wikilink")，并成立“宁波差会”。1860年他回到英国，兩年後（1862年）在[布萊頓招募到宁波差会第一位志愿者](../Page/布萊頓.md "wikilink")[宓道生](../Page/宓道生.md "wikilink")，并派往宁波。1865年6月25日，戴德生将宁波差会改名为「中國內地會」（本文簡稱「內地會」），并确立其宣教方針，就是要召募一批可以遷往中國內陸地區長期工作的傳教士，將基督教[信仰傳入中國内地](../Page/福音.md "wikilink")；內地會的总部最初在[杭州](../Page/杭州市.md "wikilink")，1890年迁至上海，慢慢把宣教工作深入中國內陸，最遠抵达[新疆迪化](../Page/新疆维吾尔自治区.md "wikilink")（今[乌鲁木齐](../Page/乌鲁木齐市.md "wikilink")），直到1950年代才在東西方[冷戰的政治背景下撤出中国](../Page/冷戰.md "wikilink")。

1964年內地會更名為海外基督使团，持續向[亞洲](../Page/亞洲.md "wikilink")17[亿的](../Page/亿.md "wikilink")[異教徒人口传扬福音](../Page/異教徒.md "wikilink")；現時海外基督使团有大約一千名宣教士，來自[日本](../Page/日本.md "wikilink")、[韩国](../Page/韩国.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[香港等地](../Page/香港.md "wikilink")。

## 原则

### 拓荒植会

仿效[使徒](../Page/使徒.md "wikilink")[保罗](../Page/保罗.md "wikilink")，不在有[基督的名被传过的地方传揚福音](../Page/基督.md "wikilink")。戴德生和早期内地会宣教士看见[中國内地的需要](../Page/中华人民共和国.md "wikilink")，決定将福音传给那些被遗忘或被忽略的群体，并在他们中间建立[教会](../Page/教会.md "wikilink")。目標是在中国各省都建立教会。

顧名思義，中國內地會是特別為中國設立的，目標是希望中國每一個省份都至少有一對宣教士在那個地方。當時西方宣教士主要是在沿海的通商口岸傳教。虽然到了《[北京條約](../Page/北京條約.md "wikilink")》以後可以自由到內地傳教，但大部分的宣教士都還是只在中國沿海地區，遍布于從東北地區到南邊的[廣東](../Page/广东省.md "wikilink")，很少有宣教士能深入到內地裡面去。

### 信心

戴德生設立中國內地會以後，就帶領一批宣教士來到了中國。內地會後來成為西方的所謂「[信心差會](../Page/信心差會.md "wikilink")」和世界內地宣教的榜樣，那麼西方宣教史就以他作為分界線，所以就看到戴德生在宣教運動上的重要性跟地位，從那時候就稱為「內地宣教時期」。內地會的「信心原則」（Faith
Principle）就是憑著信心按照[上帝的呼召去進行工作](../Page/上帝.md "wikilink")。不借贷，不募捐。遇上經費的困難時，他們不會公開向人徵款或籌款，只讓上帝感動人來幫助他們。
　　 以下是戴德生的一些语录
“亲爱的妈妈，请继续为我祷告。虽然我的衣食充足，充满快乐和感恩，但是我需要你为我代祷。。。妈妈，我怎样才能让你明白我是多么渴望成为宣教士，渴想把喜讯带给那些贫困又正步向灭亡的罪人呢。求主使用我，让我为那代我受死的主鞠躬尽瘁！。。。妈妈，试想想，有一千二百万人——一个多么庞大的数字，多么难以想象——是的，在中国每年就有一千二百多万灵魂，在没有神，没有盼望底下步向灭亡。。。啊，让我们有怜悯的心看待这庞大人民的需要！神既怜恤我们，我们也应该体恤祂的心意。。。。。。
我要搁笔了。你愿不愿意因那为你而死的主耶稣献上一切？妈妈，我知道你愿意。愿神与你同在和安慰你。当我储备了足够的路费时，我可以立刻启程吗？我深感假若我不为中国做点什么，我就不能再活下去了。”

`                                            载自《属灵的秘诀：戴德生信心之旅》`
`                                            这封戴德生写给他妈妈的信大概写于1851年，他于1865年创办中国内地会，毕生竭力把福音传到中国各省。`

### 跨越文化

内地會的宣教士們打破語言、文化和生活方式的隔閡，來到中國内地傳福音，並建立教會，克服重重困難。初期，內地會的宣教士來到中國後，必須改穿中國服，並梳中國髮式
（[去髮留辮](../Page/剃发令.md "wikilink")）。當時，戴德生更因而被英國國內的一些人士指控叛國。但戴德生认为只要能领人信主，穿中国服不是什么大麻烦，反而可以让传福音的工作更加顺利。

## 历史年表

### 1860年代

  - 1860年7月18日，戴德生从上海启程回英国，11月20日抵达[泰晤士河口的贵利夫逊](../Page/泰晤士河.md "wikilink")（Gravesend）。
  - 1862年1月8日，戴德生以“宁波差会”名义招募的传教士[宓道生](../Page/宓道生.md "wikilink")，启程前往中国创立宁波传教站
  - 1862年7月，戴德生获得外科医生证书
  - 1865年6月25日，戴德生将宁波差会改名“中国内地会”
  - 1865年10月，戴德生在伦敦出版《China's Spiritual Need and Claims》
  - 1866年3月12日，内地会在伦敦出版第一期会刊
  - 1866年5月26日，以内地会名义的第一批传教士在戴德生的率领下，乘坐“兰茂密尔”（[Lammermuir](../Page/:en:Lammermuir_\(1864_clipper\).md "wikilink")）号船，启程前往中国，9月30日抵达上海。
  - 1866年12月，“兰茂密尔号船传教团”（the Lammermuir
    Party）抵达[杭州清泰门新开弄](../Page/杭州市.md "wikilink")（新巷）1号
  - 1866年 [浙江](../Page/浙江省.md "wikilink") :
    [杭州](../Page/杭州市.md "wikilink") (戴德生建立传教总部)
  - 1866年 [浙江](../Page/浙江省.md "wikilink") :
    [奉化](../Page/奉化.md "wikilink")
    [甘比治](../Page/甘比治.md "wikilink")（George Crombie） 建立传教站。
  - 1866年 [浙江](../Page/浙江省.md "wikilink") :
    [绍兴](../Page/绍兴市.md "wikilink")
    [范明德](../Page/范明德.md "wikilink")（John Stevenson
    (missionary)）和妻子Ann 建立传教站。
  - 1867年 [浙江](../Page/浙江省.md "wikilink") :
    [萧山](../Page/萧山.md "wikilink")
    [倪义来夫妇](../Page/倪义来.md "wikilink")（Lewis & Eliza
    Nicol）, [卫养生](../Page/卫养生.md "wikilink")（James Williamson
    (missionary)）建立传教站。
  - 1867年 [浙江](../Page/浙江省.md "wikilink") : John Sell
    死于[天花](../Page/天花.md "wikilink")
  - 1867年 [浙江](../Page/浙江省.md "wikilink") :
    [杭州](../Page/杭州市.md "wikilink"): 戴德生的长女Grace Dyer Taylor
    死于[脑膜炎](../Page/脑膜炎.md "wikilink")
  - 1867年 [浙江](../Page/浙江省.md "wikilink") :
    [台州](../Page/台州市.md "wikilink")
    [宓道生](../Page/宓道生.md "wikilink")（James Joseph Meadows）,
    Josiah Jackson 建立传教站。
  - 1867年 [江苏](../Page/江苏省.md "wikilink") :
    [南京](../Page/南京市.md "wikilink")
    [童跟福](../Page/童跟福.md "wikilink")（George Duncan） 建立传教站。
  - 1868年 [江苏](../Page/江苏省.md "wikilink") :
    [扬州](../Page/扬州市.md "wikilink")
    戴德生建立传教站。同年8月22日，[扬州教案](../Page/扬州教案.md "wikilink")
  - 1868年 [浙江](../Page/浙江省.md "wikilink") :
    [湖州](../Page/湖州市.md "wikilink")
    [麦克悌](../Page/麦克悌.md "wikilink")（John McCarthy ）建立传教站。
  - 1869年 [安徽](../Page/安徽省.md "wikilink") :
    [安庆](../Page/安庆市.md "wikilink") 宓道生,
    卫养生建立传教站。同年11月3日发生[安庆教案](../Page/安庆教案.md "wikilink")。
  - 1869年 [江西](../Page/江西省.md "wikilink") :
    [九江](../Page/九江市.md "wikilink")
    [高学海](../Page/高学海.md "wikilink")（John Edwin
    Cardwell）夫妇建立传教站。

### 1870年代

  - 1870年7月23日，"差会的母亲"[Maria Jane
    Dyer在江苏](../Page/Maria_Jane_Dyer.md "wikilink")[镇江去世](../Page/镇江市.md "wikilink")。
  - 1871年11月，福珍妮嫁给戴德生。
  - 1871年-1875年 [江西](../Page/江西省.md "wikilink") :
    [大姑塘镇](../Page/大姑塘镇.md "wikilink")（今九江庐山区姑塘镇）
    高学海夫妇建立传教站。
  - 1874年 [湖北](../Page/湖北省.md "wikilink") :
    [武昌](../Page/武昌区.md "wikilink")
    戴德生和[祝名扬](../Page/祝名扬.md "wikilink")（Charles
    Henry Judd）建立传教站。
  - 1875年1月 《招募18个同工》在伦敦出版
  - 1875年7月，《[China's
    Millions](../Page/China's_Millions.md "wikilink")》第一期在伦敦出版
  - 1875年 [河南](../Page/河南省.md "wikilink") :
    [戴亨利](../Page/戴亨利.md "wikilink")（Henry Taylor
    (missionary)）是该省最早的新教传教士，该省是中国未听见过福音的9个省份之一。
  - 1875年 [湖南](../Page/湖南省.md "wikilink") :
    祝名扬和[都夭德](../Page/都夭德.md "wikilink")（Adam C.
    Dorward）是该省最早的新教传教士，后来在1880-1882年，二人旅行1500英里穿越中国
  - 1876年 [山西](../Page/山西省.md "wikilink") :
    [秀耀春](../Page/秀耀春.md "wikilink")（Francis James
    (missionary)）和[德治安](../Page/德治安.md "wikilink")（Joshua J.
    Turner）是该省最早的新教传教士，开始救济“[丁戊年华北大饥荒](../Page/丁戊奇荒.md "wikilink")”的难民
  - 1876年 [陕西](../Page/陕西省.md "wikilink") :
    [鮑康寧](../Page/鮑康寧.md "wikilink")（Frederick W.
    Baller）、[金辅仁](../Page/金辅仁.md "wikilink")（George King
    (missionary)）是该省最早的新教传教士
  - 1876年 [甘肃](../Page/甘肃省.md "wikilink") :
    [义世敦](../Page/义世敦.md "wikilink")（George F.
    Easton）和[巴格道](../Page/巴格道.md "wikilink")（George Parker
    (missionary)）是该省最早的新教传教士
  - 1876年 [四川](../Page/四川省.md "wikilink") :
    [贾美仁](../Page/贾美仁.md "wikilink")（James Cameron
    (missionary)）是该省最早的新教传教士. [George
    Nicoll在巡回布道之后定居此处](../Page/George_Nicoll.md "wikilink")。
  - 1877年 [贵州](../Page/贵州省.md "wikilink") :
    祝名扬和[包兰顿](../Page/包兰顿.md "wikilink")（James F.
    Broumton）是该省最早的新教巡回传教士。包兰顿 later pioneered
    在少数民族[苗族和](../Page/苗族.md "wikilink")[彝族中工作](../Page/彝族.md "wikilink")。
  - 1877年 [广西](../Page/广西壮族自治区.md "wikilink") : [Edward
    Fishe是该省最早的新教传教士](../Page/Edward_Fishe.md "wikilink")，同年去世。
  - 1877年 [云南](../Page/云南省.md "wikilink") :
    [麦克悌从](../Page/麦克悌.md "wikilink")[镇江徒步旅行](../Page/镇江市.md "wikilink")，经过[汉口](../Page/汉口.md "wikilink")、四川、贵州和云南到[缅甸](../Page/缅甸.md "wikilink")[八莫](../Page/八莫.md "wikilink")；履行持续7个月，沿途传道。他是最早进入云南的新教传教士。
  - 1877年 [西藏](../Page/西藏自治区.md "wikilink") :
    贾美仁从重庆步行到[巴塘](../Page/巴塘.md "wikilink")，第一个传福音给藏人。然后他去了大理和八莫，又经过[广东回到重庆](../Page/广东省.md "wikilink")，行程遍及当时中国18个省份中的17个。
  - 1878年山西 : [福珍尼](../Page/福珍尼.md "wikilink") Jennie Taylor ([Jane
    Elizabeth
    Faulding](../Page/:en:Jennie_Faulding_Taylor.md "wikilink"))是最早在中国内地旅行的女性传教士。
  - 1879年陕西: 金辅仁和 Emily Snow King
    是第一对迁入[汉中的传教士夫妇](../Page/汉中市.md "wikilink")
  - 1879年 四川 : [M. A. Howland](../Page/M._A._Howland.md "wikilink")
    Nicoll是第一名住进[重庆的女性传教士](../Page/重庆市.md "wikilink")

### 1880年代

  - 1880年山西: [Harold A.
    Schofield在](../Page/Harold_A._Schofield.md "wikilink")[太原开办内地会第一所医院](../Page/太原市.md "wikilink")。
  - 1880年 甘肃: [Elizabeth Wilson
    (missionary)是该省最早的女传教士](../Page/Elizabeth_Wilson_\(missionary\).md "wikilink")
  - 1880年 贵州 : George Clarke (missionary) 和妻子Fanny 定居该省
  - 1881年 云南 : [John Stevenson
    (missionary)和](../Page/John_Stevenson_\(missionary\).md "wikilink")[Henry
    Soltau在](../Page/Henry_Soltau.md "wikilink")86天中徒步旅行了1900英里，从八莫、[昆明](../Page/昆明市.md "wikilink")、重庆、[武昌到上海](../Page/武昌区.md "wikilink")，从西向东穿越中国。
  - 1881年 云南 : George and Fanny Clarke settle to work in
    [大理](../Page/大理.md "wikilink").
  - 1881年 [山东](../Page/山东省.md "wikilink") :
    [芝罘学校在](../Page/芝罘学校.md "wikilink")[烟台开办](../Page/烟台市.md "wikilink")(最初称为
    "The Protestant Collegiate School")
  - 1885年3月3日，“[剑桥七杰](../Page/剑桥七杰.md "wikilink")”抵达中国
  - 1887年，派出“[一百名同工](../Page/一百名同工.md "wikilink")”来华
  - 1888年，[海班明](../Page/海班明.md "wikilink")（Benjamin
    Broomhall）创办《[National
    Righteousness](../Page/National_Righteousness.md "wikilink")》:
    一份宣传[戒毒的期刊](../Page/戒毒.md "wikilink")
  - 1888年10月30日，第一批内地会[美国传教士抵达中国](../Page/美国.md "wikilink")[上海](../Page/上海市.md "wikilink")

### 1890年代

  - 1890年，戴德生在虹口区[吴淞路](../Page/吴淞路.md "wikilink")9号正式建立内地会第一个总部大楼，可供300多位传教士办公和居住。购屋经费由[榮晃熙](../Page/榮晃熙.md "wikilink")(Archibald
    Orr Ewing,1857\~1930)捐献。
  - 1890年，第一批[澳大利亚传教士抵达](../Page/澳大利亚.md "wikilink")
  - 1894年[甲午战争](../Page/甲午战争.md "wikilink")，[稻惟德在烟台参加红十字救助伤员的工作](../Page/稻惟德.md "wikilink")。
  - 1895年，Newington Green office Headquarters 伦敦

### 1900年代

  - 在1900年的[义和团事变中](../Page/义和团事变.md "wikilink")，有58名内地会传教士和21名他们的孩子被杀害（见[义和团教案中内地会殉道者名单](../Page/:en:List_of_China_Inland_Mission_missionaries_killed_during_the_Boxer_Rebellion.md "wikilink")）。
  - 1901年，戴德生拒绝接受清政府对财产和生命损失的赔偿，展示‘基督的温柔和良善’
  - 1901年，[何斯德](../Page/何斯德.md "wikilink")（Dixon Edward Hoste）被指定为代理总监督
  - 1902年，杭州[崇一堂落成](../Page/崇一堂.md "wikilink")。
  - 1902年11月，**戴德生辞去内地会总监督**
  - **1905年6月3日，戴德生在[湖南](../Page/湖南省.md "wikilink")[长沙去世](../Page/长沙市.md "wikilink")**
  - 1906年，[胡进洁](../Page/胡进洁.md "wikilink")（Sir George W.
    Hunter）进入[新疆](../Page/新疆维吾尔自治区.md "wikilink")[迪化传教](../Page/乌鲁木齐市.md "wikilink")
  - 1908年，慈禧太后去世，内地会向 [江苏](../Page/江苏省.md "wikilink"),
    [安徽和](../Page/安徽省.md "wikilink")[河南派遣洪水和饥荒慰问团队](../Page/河南省.md "wikilink")

### 1910年代

  - 1910年，[J. O. Fraser抵达中国](../Page/富能仁.md "wikilink")
  - [云南西部部落地区有了](../Page/云南省.md "wikilink")60,000名基督徒
  - 1911年，[海班明](../Page/海班明.md "wikilink")（[Benjamin
    Broomhall](../Page/Benjamin_Broomhall.md "wikilink")）在[戒毒运动成功后去世](../Page/戒毒运动.md "wikilink")。（第一届世界戒毒大会于1909年在上海汇中饭店召开，海班明是大会主席。）
  - 1912年1月1日，[中华民国成立](../Page/中華民國.md "wikilink")。
  - 1912年，内地会传道员超过1000人, 成为在华最大的传教机构

### 1920年代

  - 中国内战（Chinese Civil
    War）和[非基督教运动迫使几乎所有传教士都暂时撤退](../Page/非基督教运动.md "wikilink")
  - 1927年-1932年，何斯德发起[200人专案活动](../Page/200人专案.md "wikilink")，援助内地宣教运动，从1200名申请人中挑选了共计203名传教士分三批抵达中国（1929年39人，1930年47人，1931年117人）

### 1930年代

  - 1930年，总部迁往上海[新闸路](../Page/新闸路.md "wikilink")1531号，有行政管理大楼、传教士公寓、同工宿舍、礼拜堂、招待所。
  - 1934年10月1日，[薄复礼牧师在贵州被迷路的](../Page/薄复礼.md "wikilink")[红军抓到](../Page/红军.md "wikilink")，随[肖克和](../Page/肖克.md "wikilink")[贺龙的部队长征达](../Page/贺龙.md "wikilink")560天，经过贵州、四川、湖南、湖北、云南5省，行程达一万公里，1936年4月在云南被释放。11月，他将在红军中的经历出书——《神灵之手》（The
    Restraining
    Hand，中文名《红军长征秘闻录》），是西方最早介绍中国红军[长征的专著](../Page/长征.md "wikilink")。
  - 1934年12月8日，200人专案中的[师达能牧师夫妇](../Page/师达能夫妇.md "wikilink")（John and
    Betty Stam）在皖南[旌德县被红军绑票杀害](../Page/旌德县.md "wikilink")
  - [第二次世界大战迫使许多传教士到更偏僻的内陆](../Page/第二次世界大战.md "wikilink") -
    或被日本人投入狱中，直到战争结束

### 1940年代

  - 1942年11月，位于[烟台的传教士子弟学校](../Page/烟台市.md "wikilink")[芝罘学校被关闭](../Page/芝罘学校.md "wikilink")，所有学生和职员被日军关进集中营。
  - 1945年8月，芝罘学校被美国伞兵解救。
  - 1949年10月1日，[毛泽东在](../Page/毛泽东.md "wikilink")[北京宣布](../Page/北京市.md "wikilink")[中华人民共和国成立](../Page/中华人民共和国.md "wikilink")

### 1950年代

  - 1951年，
    [三自爱国运动](../Page/三自爱国运动.md "wikilink")，政府控制教会。"三自宣言"发表以后，内地会开始撤退传教士，到1953年撤退完毕。所有传教士重新部署在东亚其他地区。
  - 1951年11月，在新加坡建立新总部，机构名称也改为The China Inland Mission Overseas
    Missionary Fellowship

### 1960年代

  - 1964年，中国内地会更名为海外基督使团。
  - [文化大革命](../Page/文化大革命.md "wikilink") 1966年-1972年:
    取缔包括[三自爱国运动在内的所有基督教活动](../Page/三自爱国运动.md "wikilink")
  - 在泰国农村的医疗工作开始

### 1970年代

### 1980年代

  - 中国教会受洗信徒人数达到2150万，包括慕道者的基督徒超过5200万

### 1990年代

## 在华传教站列表

其中安徽、四川、贵州、甘肃、陕西、云南、河南等省都由内地会首先开辟。1927年1月內地會在中國共有937個宣教站，到了同年7月只剩296個。1928年1月開始有宣教士回來重開宣教站，便增為
305個，同年7月465個。再過半年，北伐內戰漸平靜，1929年1月，宣教站達840個。\[1\]

### 浙江

[杭州](../Page/杭州市.md "wikilink")（1866），绍兴（1866），奉化（1866），台州（1867），[温州](../Page/温州市.md "wikilink")（1867），宁海（1868），平阳（1874），衢州，常山，[金华](../Page/金华市.md "wikilink")，兰溪，[永康](../Page/永康市.md "wikilink")，天台，仙居，新昌，嵊县，黄岩，莫干山，

  - 教堂：杭州[崇一堂](../Page/崇一堂.md "wikilink")、敬一堂，温州[花园巷堂](../Page/花园巷堂.md "wikilink")

### 江苏

[扬州](../Page/扬州市.md "wikilink")（1868），[镇江](../Page/镇江市.md "wikilink")，[淮安](../Page/淮安市.md "wikilink")，[涟水](../Page/涟水.md "wikilink")（1893）

  - 教堂：扬州[皮市街教堂](../Page/皮市街教堂.md "wikilink")（[扬州教案发生地](../Page/扬州教案.md "wikilink")），
  - [镇江和](../Page/镇江市.md "wikilink")[扬州分别设有面向男性传教士和女性传教士的语言训练所](../Page/扬州市.md "wikilink")。

### 上海

1890年总部迁至[上海虹口区](../Page/上海市.md "wikilink")[吴淞路](../Page/吴淞路.md "wikilink")，1930年迁至新闸路1531号，1950年代总部建筑被改为上海第六医院\[2\]（目前为上海交通大学附属儿童医院使用）。上海没有面向中国人的教堂，只有供传教士使用的地丰路礼拜堂（今[新恩堂](../Page/新恩堂.md "wikilink")）。

### 安徽

[安庆](../Page/安庆市.md "wikilink")（1869），[阜阳](../Page/阜阳市.md "wikilink")，太和，六安，正阳关，舒城，广德，宣城，泾县，郎溪，芜湖，

### 江西

[九江](../Page/九江市.md "wikilink")（1869），[大姑塘镇](../Page/大姑塘.md "wikilink")(位于九江[庐山区](../Page/庐山区.md "wikilink")），[吉安](../Page/吉安市.md "wikilink")（1891），[南昌](../Page/南昌市.md "wikilink")，[赣州](../Page/赣州市.md "wikilink")，[上饶](../Page/上饶市.md "wikilink")，[宜春](../Page/宜春市.md "wikilink")，[鄱阳](../Page/鄱阳.md "wikilink")，[信丰](../Page/信丰.md "wikilink")，[弋阳](../Page/弋阳.md "wikilink")，[河口镇](../Page/河口镇.md "wikilink")（铅山），[樟树](../Page/樟树.md "wikilink")，[贵溪](../Page/贵溪.md "wikilink")，[东乡](../Page/东乡.md "wikilink")，[遂川](../Page/遂川.md "wikilink")，【永丰】，【永新】

### 湖北

宜昌，老河口

### 河南

[周口](../Page/周口市.md "wikilink")，社旗，襄城，[开封](../Page/开封市.md "wikilink")，

  - 教堂：开封[大纸坊街礼拜堂](../Page/大纸坊街礼拜堂.md "wikilink")、南关福音医院礼拜堂

### 四川

[重庆](../Page/重庆市.md "wikilink")（1877），成都（1881），[阆中](../Page/阆中.md "wikilink")，[巴中](../Page/巴中市.md "wikilink")，万县，[广元](../Page/广元市.md "wikilink")，[渠县](../Page/渠县.md "wikilink")，[南充](../Page/南充市.md "wikilink")，梁平，垫江，达县，奉节，灌县，[江津](../Page/江津.md "wikilink")，

### 贵州

[贵阳](../Page/贵阳市.md "wikilink")（1877），[安顺](../Page/安顺市.md "wikilink")，[赫章](../Page/赫章.md "wikilink")[葛布](../Page/葛布.md "wikilink")，结构，旁海，[遵义](../Page/遵义市.md "wikilink")，[镇远](../Page/镇远.md "wikilink")，平坝，

### 湖南

[常德](../Page/常德市.md "wikilink")（1879）、[华容](../Page/华容.md "wikilink")、[湘潭](../Page/湘潭市.md "wikilink")、[湘乡](../Page/湘乡市.md "wikilink")、[长沙](../Page/长沙市.md "wikilink")

### 山西

平阳府（[临汾](../Page/临汾市.md "wikilink")）（1878），[洪洞](../Page/洪洞.md "wikilink")（1886，山西省总部），[霍州](../Page/霍州.md "wikilink")，[平遥](../Page/平遥.md "wikilink")，[祁县](../Page/祁县.md "wikilink")，潞安府（长治），隰县，大宁，孝义，河津，赵城，介休，潞城。

  - 学校：[洪洞县普潤學校](../Page/洪洞.md "wikilink")（Hoste Schools），內設中、小學。

### 陕西

汉中，凤翔，城固，洋县，

### 甘肃、宁夏、青海

甘肃天水，甘肃[兰州](../Page/兰州市.md "wikilink")，甘肃武威，宁夏银川，宁夏平罗（1932年），青海西宁。

  - 教堂：兰州[山字石教堂](../Page/山字石教堂.md "wikilink")

### 云南

大理（1881），昆明（1882），武定洒普山，腾冲，丽江，楚雄

  - 教堂：昆明[三一圣堂](../Page/三一圣堂.md "wikilink")（1903年）

### 山东

烟台，[牟平](../Page/牟平.md "wikilink")（1886）

  - 烟台设有传教士子弟学校[芝罘学校和疗养院](../Page/芝罘学校.md "wikilink")。

### 河北

获鹿（1887），邢台（1888）

### 新疆

迪化（1906）

## 著名人物

  - [戴德生](../Page/戴德生.md "wikilink")（James Hudson Taylor，－1905）
  - [何斯德](../Page/何斯德.md "wikilink")(Dix E.Hoste，1861-1946)
  - [宓道生](../Page/宓道生.md "wikilink")(James Meadows，1835-1914)
  - [海班明](../Page/海班明.md "wikilink")
  - [剑桥七杰](../Page/剑桥七杰.md "wikilink")
  - [施达德](../Page/施达德.md "wikilink")（Charles Studd），
  - [章必成](../Page/章必成.md "wikilink")(Montague BeauchamP)，
  - [司安仁](../Page/司安仁.md "wikilink")(Stanley Smith)，
  - [亚瑟.端纳](../Page/亚瑟.端纳.md "wikilink")（A.T. Polhill-Turner），
  - [宝耀庭](../Page/宝耀庭.md "wikilink")(Cecil Polhill-Turner)，
  - [盖士利](../Page/盖士利.md "wikilink")(William Cassels)，
  - [童跟福](../Page/童跟福.md "wikilink")（George Duncan）
  - [戴存仁](../Page/戴存仁.md "wikilink")
  - [海思波](../Page/海思波.md "wikilink")
  - [盖群英](../Page/盖群英.md "wikilink")
  - [祝名扬](../Page/祝名扬.md "wikilink")
  - [高学海](../Page/高学海.md "wikilink")
  - [王志明](../Page/王志明.md "wikilink")
  - [富能仁](../Page/富能仁.md "wikilink")
  - [内地会在华传教士列表](../Page/内地会在华传教士列表.md "wikilink")
  - [义和团教案中内地会殉道者名单](../Page/义和团教案中内地会殉道者名单.md "wikilink")

## 图集

<File:lammermuirparty.jpg>|

<center>

**4. *The [Lammermuir Party](../Page/兰茂密尔团队.md "wikilink") sailed on 26
May 1866***

</center>

<File:china_inland_mission.jpg>|

<center>

**10. *The China Inland Mission, one of two Grade 2 listed buildings on
Mildmay's Newington Green. (October 2005)***

</center>

<File:CIM1902.jpg>|

<center>

**11. *Map of the Stations of the China Inland Mission in 1902 near
Taylor's death***

</center>

## 参考文献

### 出处

<div class="references-small">

<references />

</div>

### 书目

  -
  -
## 外部链接

  - [Overseas Missionary Fellowship](http://www.omf.org/)
  - [Christian Biography
    Resources](http://www.wholesomewords.org/biography/biorptaylor.html)
  - <http://www.missionaryetexts.org/>
  - <http://www.worldinvisible.com/library/hudsontaylor/hudsontaylorv1/hudsontaylorv1tc.htm>
  - [1](http://www.weihsien-paintings.org/)
  - <https://web.archive.org/web/20070926212919/http://www.genealogy.com/users/y/o/r/Brian-York-Burnsville/?Welcome=1091209026>
  - [China Through the Eyes of CIM
    Missionaries](http://lib-sca.hkbu.edu.hk:8080/was40/search?channelid=47511)
    Hong Kong Baptist University Library. Special Collections and
    Archives 香港浸會大學圖書館特藏組

## 参见

  - [:en:19th Century Protestant Missions in
    China](../Page/:en:19th_Century_Protestant_Missions_in_China.md "wikilink")
  - [:en:List of China Inland Mission missionaries in
    China](../Page/:en:List_of_China_Inland_Mission_missionaries_in_China.md "wikilink")
  - [:en:List of Protestant missionaries in
    China](../Page/:en:List_of_Protestant_missionaries_in_China.md "wikilink")
  - [中国基督教史](../Page/中国基督教史.md "wikilink")
  - [席胜魔](../Page/席胜魔.md "wikilink")
  - [William Thomas Berger](../Page/William_Thomas_Berger.md "wikilink")
  - [Benjamin Broomhall](../Page/Benjamin_Broomhall.md "wikilink")

{{-}}

[内地会](../Category/内地会.md "wikilink")
[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")

1.  [黃錫培：傷寒離世之陶茂芝牧師](http://www.ccmlit.org/Proclaim/Proclaim_107/Proclaim_107_05.html)
2.  [静安区志 \>\> 第二十五编民族·宗教 \>\> 第二章宗教 \>\>
    第三节　基督教](http://www.shtong.gov.cn/newsite/node2/node4/node2249/node4412/node17453/node19363/node62337/userobject1ai7728.html)