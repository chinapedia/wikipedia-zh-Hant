'''小行星11海妖星 ''' ([IPA](../Page/IPA.md "wikilink"):
)是一顆較大且明亮的[小行星帶天體](../Page/小行星帶.md "wikilink")。

海妖星由[安尼巴莱·德·加斯帕里斯於](../Page/安尼巴莱·德·加斯帕里斯.md "wikilink")1850年5月11日所發現，這也是他發現的許多小行星中的第二顆。該天體後來以[希臘神話中的](../Page/希臘神話.md "wikilink")[海妖賽蓮中的一人](../Page/海妖賽蓮.md "wikilink")[帕耳忒诺佩來命名](../Page/帕耳忒诺佩.md "wikilink")，這個名稱起因於[約翰·弗里德里希·威廉·赫歇爾](../Page/約翰·弗里德里希·威廉·赫歇爾.md "wikilink")，他在德·加斯帕里斯於1849年發現[健神星後所建議的](../Page/健神星.md "wikilink")。\[1\]

在1987年2月13日有一次海妖星[掩星被觀測到](../Page/掩星.md "wikilink")。

## 參見

<references/>

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1850年发现的小行星](../Category/1850年发现的小行星.md "wikilink")

1.