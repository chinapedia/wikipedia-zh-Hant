**海山汽車客運股份有限公司**，簡稱**海山客運**，曾是[大台北地區](../Page/大台北地區.md "wikilink")[公路客運經營者之一](../Page/公路.md "wikilink")，原總部位於[臺北縣](../Page/新北市.md "wikilink")[三峽鎮](../Page/三峽區.md "wikilink")。

海山客運的前身為**[三峽汽車運輸合作社](../Page/三峽汽車運輸合作社.md "wikilink")**，也是今日[中興巴士集團旗下之](../Page/中興巴士集團.md "wikilink")[淡水客運前身](../Page/淡水客運.md "wikilink")。1974年3月，三峽汽車運輸合作社由於經營困難，讓售於[台北客運](../Page/台北客運.md "wikilink")，並更名為**三峽汽車客運股份有限公司**（簡稱**三峽客運**）。1977年，[基隆顏家買入台北客運與三峽客運](../Page/基隆顏家.md "wikilink")，因三峽客運所經營之路線大部分以1950年縣轄區建制廢除前的[臺北縣](../Page/新北市.md "wikilink")[海山區為主](../Page/海山郡.md "wikilink")，故把三峽客運更名為海山客運。

海山客運被基隆顏家購入後，將位於三峽鎮的辦公地點併入當時位於[板橋市文化路的台北客運總公司](../Page/板橋區_\(新北市\).md "wikilink")（現在已經搬遷至[三重區](../Page/三重區.md "wikilink")）。1980年代末期，海山客運的路線已經大部份委交台北客運代駛，難以見到海山客運車輛。1991年，海山客運正式併入台北客運，相關路線的營運權亦轉移之。

## 相關路線

<table>
<thead>
<tr class="header">
<th><p>路線營運區間</p></th>
<th><p>整併前經營業者</p></th>
<th><p>現況</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>三峽－橫溪－安新</p></td>
<td><p>海山客運</p></td>
<td><p>今<a href="../Page/台北聯營公車.md" title="wikilink">台北聯營公車</a><strong>779</strong>線</p></td>
</tr>
<tr class="even">
<td><p>三峽－吊橋－白雞</p></td>
<td><p>海山客運</p></td>
<td><p>今<a href="../Page/台北聯營公車.md" title="wikilink">台北聯營公車</a><strong>778</strong>線</p></td>
</tr>
<tr class="odd">
<td><p>三峽－湊合－插角</p></td>
<td><p>台北客運、海山客運</p></td>
<td><p>今<a href="../Page/台北聯營公車.md" title="wikilink">台北聯營公車</a><strong>807</strong>線</p></td>
</tr>
<tr class="even">
<td><p>三峽－鶯歌－樹林</p></td>
<td><p>海山客運</p></td>
<td><p>（不詳）</p></td>
</tr>
<tr class="odd">
<td><p>三峽－大同橋－添福里</p></td>
<td><p>海山客運</p></td>
<td><p>今台北客運「三峽－添福里」線，停駛日期待查，變更為通勤專車</p></td>
</tr>
<tr class="even">
<td><p>三峽－橫溪－茶場</p></td>
<td><p>海山客運</p></td>
<td><p>今台北客運「三峽－茶場」線，停駛日期待查，變更為通勤專車</p></td>
</tr>
<tr class="odd">
<td><p>三峽－樂樂谷</p></td>
<td><p>海山客運</p></td>
<td><p>今台北聯營公車<strong>807</strong>線</p></td>
</tr>
<tr class="even">
<td><p>三峽－樹林－板橋</p></td>
<td><p>海山客運</p></td>
<td><p>（不詳）</p></td>
</tr>
<tr class="odd">
<td><p>三峽－橫溪－安坑</p></td>
<td><p>海山客運</p></td>
<td><p>今台北客運「三峽－鹿母潭」線，停駛日期待查，變更為通勤專車</p></td>
</tr>
<tr class="even">
<td><p>三峽－八張－弘道里</p></td>
<td><p>海山客運</p></td>
<td><p>（不詳）</p></td>
</tr>
<tr class="odd">
<td><p>三峽－鶯歌－樹林－台北</p></td>
<td><p>台北客運、海山客運、<br />
<a href="../Page/首都客運.md" title="wikilink">首都客運</a>、<a href="../Page/三重客運.md" title="wikilink">三重客運</a></p></td>
<td><p>今台北聯營公車<strong>702</strong>線</p></td>
</tr>
<tr class="even">
<td><p>三峽－樹林柑園－土城－台北</p></td>
<td><p>台北客運、海山客運、<br />
首都客運、三重客運</p></td>
<td><p>今台北聯營公車<strong>703</strong>線（后与副线一起缩线至捷运府中站并改号889）</p></td>
</tr>
<tr class="odd">
<td><p>三峽－文化路－中山路－台北</p></td>
<td><p>海山客運</p></td>
<td><p>今台北客運「三峽－土城－台北」線</p></td>
</tr>
<tr class="even">
<td><p>三峽－樹林－板橋</p></td>
<td><p>海山客運</p></td>
<td><p>（不詳）</p></td>
</tr>
</tbody>
</table>

## 相關條目

  - [台北客運](../Page/台北客運.md "wikilink")
  - [淡水客運](../Page/淡水客運.md "wikilink")
  - [三峽汽車運輸合作社](../Page/三峽汽車運輸合作社.md "wikilink")

[H海](../Category/臺灣已結業交通運輸公司.md "wikilink")
[H海](../Category/台灣客運公司.md "wikilink")
[H海](../Category/1991年結業公司.md "wikilink")