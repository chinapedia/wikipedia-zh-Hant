《**海峽時報**》（）是[新加坡歷史十分悠久的](../Page/新加坡.md "wikilink")[英文](../Page/英語.md "wikilink")[報紙](../Page/報紙.md "wikilink")。它是[新加坡报业控股有限公司的](../Page/新加坡报业控股.md "wikilink")[旗艦](../Page/旗艦.md "wikilink")[刊物](../Page/刊物.md "wikilink")。《海峽時報》於1845年7月15日首次發行，目前發行量為每日40萬份。它是新加坡唯一寬版的英文報紙報導一般[社會新聞](../Page/社會新聞.md "wikilink")。另一份大張的英文報紙－《[商業時報](../Page/商業時報.md "wikilink")》則只關注[商業新聞](../Page/商業新聞.md "wikilink")。

## 歷史

海峽時報是由[亞美尼亞人吉席](../Page/亞美尼亞.md "wikilink")·摩西(Catchick
Moses)創辦。摩西的朋友Martyrose
Apcar本來打算辦一份本地報紙，卻遇到有經濟困難。為了實現朋友的夢想，摩西接手，任命羅伯特·卡爾·伍茲(Robert
Carr Woods)擔任主編。1845年7月15日，海峽時報出版了八頁的周報，使用手動印刷機在商業廣場7號(7 Commercial
Square)印刷。每月訂費1.75叻幣。1846年9月，摩西把海峽時報賣給羅伯特．卡爾．伍茲，因為證實報紙無利可圖。

1942年2月20日，英國向日軍投降5日後，海峽時報改稱為昭南時報(The Shonan Times)和昭南新聞(The Syonan
Shimbun)，1945年9月新加坡恢復英國統治後復名。

## 相關事件

自1957年8月31日[馬來西亞獨立後](../Page/馬來西亞.md "wikilink")，海峽時報親華人的政治立場敏感，不得在鄰國馬來西亞銷售，後來馬來西亞的[新海峽時報也不得在新加坡銷售](../Page/新海峽時報.md "wikilink")。

2005年1月1日，兩國政府討論解禁。新加坡前總理[李光耀說贊成這樣的舉動](../Page/李光耀.md "wikilink")，雖然馬來西亞政客們更加謹慎。在供水問題發生爭執過程中，報紙被禁止在馬來西亞銷售。

## 參考文獻

  - Thio, *HR and the Media in Singapore* in HR and the Media, Robert
    Haas ed, Malaysia: AIDCOM 1996 69 at 72-5.

## 外部連結

  - [海峡时报官网](https://www.straitstimes.com/)

[Category:新加坡报纸](../Category/新加坡报纸.md "wikilink")
[Category:英文報紙](../Category/英文報紙.md "wikilink")
[Category:1845年建立](../Category/1845年建立.md "wikilink")