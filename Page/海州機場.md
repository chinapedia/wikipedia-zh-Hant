**海州機場**（）是[朝鮮](../Page/朝鮮.md "wikilink")[黃海南道首府](../Page/黃海南道.md "wikilink")[海州市的地區機場](../Page/海州市.md "wikilink")。屬[朝鮮人民軍管理](../Page/朝鮮人民軍.md "wikilink")。一般用作軍事基地，然而亦有限度開放予民航客機及貨運使用。

## 設備

  - 一條混凝土跑道走向為12/30（120/300°）
  - 海拔高度：12公尺
  - 跑道長度：2,000公呎。

## 使用航空公司

  - [朝鮮的](../Page/朝鮮.md "wikilink")[高麗航空商社](../Page/高麗航空.md "wikilink")（Air
    Koryo）是目前唯一服務海州機場的航空公司，它主要使用：
      - 安東諾夫-24 螺旋槳客機（載客：52人）
      - 伊尔-18 螺旋槳貨機（載貨量：64,000公斤）

## 航點

## 參考

  - 朝鮮[平壤順安國際機場](../Page/平壤順安國際機場.md "wikilink")(Pyongyang Sunan
    International Airport, FNJ)
  - 朝鮮咸鏡北道漁郎郡[清津機場](../Page/清津機場.md "wikilink")(ChongjinAirport)
  - 朝鮮[高麗航空商社](../Page/高麗航空.md "wikilink")(Air Koryo)

## 外部連結

  - [平壤順安國際機場的非官方網站](https://web.archive.org/web/20040415023157/http://hk.geocities.com/hkgalbert/fnj.htm)(英文、繁體中文)
  - [朝鮮高麗航空商社的官方網站](http://www.air-koryo.com/)(英文、韩文、简体中文)
  - [朝鮮高麗航空商社的非官方網站](https://web.archive.org/web/20040415123351/http://hk.geocities.com/hkgalbert/index.htm)(英文、繁體中文)

[category:朝鮮機場](../Page/category:朝鮮機場.md "wikilink")