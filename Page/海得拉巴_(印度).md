**海得拉巴**（[泰卢固语](../Page/泰卢固语.md "wikilink")：హైదరాబాదు，[乌尔都语](../Page/乌尔都语.md "wikilink")：）是[印度第四大城市](../Page/印度.md "wikilink")(人口)，位于印度中部，[安得拉邦和](../Page/安得拉邦.md "wikilink")[泰倫加納邦兩邦的首府](../Page/泰倫加納邦.md "wikilink")，距離第五大城[清奈](../Page/清奈.md "wikilink")(位於半島東南岸)約五百多公里。海城在1591年為[庫特布沙希王朝所建立](../Page/庫特布沙希王朝.md "wikilink")，信仰[伊斯蘭教的邦王](../Page/伊斯蘭教.md "wikilink")[穆罕默德·库里·库特布·沙阿所統治](../Page/穆罕默德·库里·库特布·沙阿.md "wikilink")。18世纪蒙古人后裔阿西夫·扎一世来此建立了王国：海德拉巴尼扎姆。自17世紀初期英國[東印度公司多年於](../Page/英國東印度公司.md "wikilink")[孟買](../Page/孟買.md "wikilink")(位於半島西岸)、清奈、到[西孟加拉等沿海地區軍事與貿易活動影響](../Page/西孟加拉.md "wikilink")，1798年尼扎姆宣布效忠英王，成为[英属印度的一个](../Page/英属印度.md "wikilink")[土邦](../Page/海得拉巴_\(土邦\).md "wikilink")。1948年，[海德拉巴邦加入](../Page/海德拉巴邦.md "wikilink")[印度自治领](../Page/印度自治领.md "wikilink")，海德拉巴为其首府。1956年后，海德拉巴成为[安得拉邦首府](../Page/安得拉邦.md "wikilink")，及[印度共和国的冬都](../Page/印度共和国.md "wikilink")。2014年[特伦甘纳邦从安得拉析置](../Page/特伦甘纳邦.md "wikilink")，海得拉巴成为其首府。随后[安得拉邦则将政府机构移置](../Page/安得拉邦.md "wikilink")[阿马拉瓦蒂](../Page/阿马拉瓦蒂.md "wikilink")，但仍然视海得拉巴为法定首府。

海德拉巴以富饶的历史，清真寺、庙宇等建筑而著名，也拥有丰富的艺术、手工艺和舞蹈的文化遗產。八所高等院校，200多所技术院校設立於此。

## 历史

海得拉巴位于[德干高原边缘](../Page/德干高原.md "wikilink")，建城於1591年；16世纪中葉迅速发展，吸引许多[高尔康达人民來此定居](../Page/高尔康达.md "wikilink")，总面积约260平方公里。

海得拉巴的统治者注重教育，也興建不少建筑，为海得拉巴的印度—伊斯兰文化贡献良多。同时海得拉巴也和高尔康达成为世界首要的[钻石](../Page/钻石.md "wikilink")、[珍珠](../Page/珍珠.md "wikilink")、[钢铁和武器市场](../Page/钢铁.md "wikilink")。

当[英国和](../Page/英国.md "wikilink")[法国将势力推進印度时](../Page/法国.md "wikilink")，海得拉巴的统治者不与他们交惡，从而保留邦国主权。而海得拉巴的经济和文化則在此期間，發展昌隆。宫殿、建筑物、房屋、公寓、公园和街道，因受多种文化影响而各具特色，是當時印度最大的邦国，面積一度比[英格兰和](../Page/英格兰.md "wikilink")[苏格兰加在一起更為廣大](../Page/苏格兰.md "wikilink")。尚且拥有自己的货币、造币厂、铁路和邮政局，邦境之內不收[所得税](../Page/所得税.md "wikilink")。

印度独立后不久，海得拉巴邦即加入印度联邦。1956年11月1日，印度联邦重新規劃行政区後，海得拉巴成为[安得拉邦的首府](../Page/安得拉邦.md "wikilink")。2014年，新的[泰倫加納邦因著財源比例](../Page/泰倫加納邦.md "wikilink")\[1\]，於安得拉邦析置，海得拉巴成為兩邦首府。惟十年之內，海得拉巴將成為衹是一邦的首府。\[2\]

## 今天的海得拉巴

海得拉巴约有700万居民。[穆斯林人口所佔比例與其他印度地區相較為高](../Page/穆斯林.md "wikilink")。此地常用[泰卢固语](../Page/泰卢固语.md "wikilink")、[印地语和](../Page/印地语.md "wikilink")[乌尔都语](../Page/乌尔都语.md "wikilink")，[英语則為贸易用語](../Page/英语.md "wikilink")。

海得拉巴是印度較早发展[通訊技术的城市](../Page/通訊技术.md "wikilink")。完善的基础建设，使海得拉巴成为软體发展、[企業流程委外和](../Page/企業流程委外.md "wikilink")[生物技术的基地](../Page/生物技术.md "wikilink")，许多印度和外商公司在这里设有总部或研究中心，也是珍珠和珍珠加工、银手工业和其他手工业的製造中心。

由海得拉巴赴[中东](../Page/中东.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")（尤其[新加坡和](../Page/新加坡.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")）以及印度其他城市的交通非常方便。国际机场為[拉吉夫·甘地國際機場](../Page/拉吉夫·甘地國際機場.md "wikilink")。機場之旁有一座[F1赛车场](../Page/F1赛车.md "wikilink")，现在正积极爭取未来几年可举办[一级方程式](../Page/一级方程式.md "wikilink")[印度大奖赛](../Page/印度大奖赛.md "wikilink")。

2010年度[国际数学家大会在此举办](../Page/国际数学家大会.md "wikilink")。

## 參見

  - [海德拉巴科技城](../Page/海德拉巴科技城.md "wikilink")
  - [維客旅行上的](../Page/維客旅行.md "wikilink")[海得拉巴](http://wikitravel.org/en/Hyderabad)
  - \[//maps.google.com/maps?q=Hyderabad 谷歌地圖\]

## 參考來源

[Category:印度人口过百万的城市](../Category/印度人口过百万的城市.md "wikilink")
[Category:印度都会区](../Category/印度都会区.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")
[Category:1590年代建立](../Category/1590年代建立.md "wikilink")

1.
2.  [Amid chaos and slogans, Rajya Sabha clears Telangana
    bill](http://www.ndtv.com/article/cheat-sheet/amid-chaos-and-slogans-rajya-sabha-clears-telangana-bill-485953)
    – NDTV, 20 Feb 2014