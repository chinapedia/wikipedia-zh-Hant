**海怡半島站**（）是[港鐵的](../Page/港鐵.md "wikilink")[南港島綫上行的南端終點站](../Page/南港島綫.md "wikilink")，主要服務[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[鴨脷洲西部地區](../Page/鴨脷洲.md "wikilink")，即[海怡半島及](../Page/海怡半島.md "wikilink")[鴨脷洲邨一帶](../Page/鴨脷洲邨.md "wikilink")，於2016年12月28日啟用。

## 車站設計

一般車站是以[混凝土興建](../Page/混凝土.md "wikilink")，設[防水層防止水由頂部或牆壁滲入](../Page/防水層.md "wikilink")。不過本站受原有結構保護，加上受空間限制，因此B出入口頂部由[防火板建造](../Page/防火板.md "wikilink")，有別於一般車站以混凝土建造來配合防水層。\[1\]

### 樓層

海怡半島站設有一層車站大堂及一層月台，而連接大堂的扶手電梯位於月台（路軌盡頭）的西面，因此車站的大堂及出口均是在車站西面。

<table>
<tbody>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>地面</p></td>
<td><p>出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>L1</strong></p></td>
<td><p>大堂</p></td>
<td><p>資訊台（只設於非付費區）、車站商店、自助客務機、免費Wi-Fi熱點、洗手間</p></td>
</tr>
<tr class="odd">
<td><p><strong>L2</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>島式月台，開左/右車門</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 大堂

海怡半島站大堂設於怡南路及海怡路交界、海怡東商場牌樓前之地底，垂直處於月台與路面間。如同利東及黃竹坑站，此站是首批全站不設客務中心之港鐵車站，大堂僅設資訊台及三部自助客務機，乘客如遇票務疑難，須透過自助客務機之視像通話功能向站外職員求助。閘區內設客用洗手間。
{{ multiple image | align = center | direction = vertical | width = 750
| image1 = Panorama of South Horizons Station Concourse.jpg | caption1 =

<center>

大堂全景圖（2016年12月）

</center>

}}

### 商店

大堂設三間商店，包括[山崎麵包](../Page/山崎麵包.md "wikilink")、7-11便利店及[唐包點](../Page/唐記包點.md "wikilink")。

### 月台

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = South Horizons Station 2018 01 part2.jpg | caption1 = 1號月台 |
image2 = South Horizons Station 2018 01 part1.jpg | caption2 = 2號月台 }}
海怡半島站月台位處海怡半島公共運輸交匯處外一段怡南路地底，採用島式月台設計，兩個月台處於同一月台島上，設有月台幕門。此站位於南港島綫路軌盡頭，往海怡半島之列車駛入月台開門後，便會改為相反方向駛離車站。月台與大堂間以三條電梯及一部升降機連接。
{{ multiple image | align = center | direction = vertical | width = 750
| image1 = South Horizons Station 2016 12 part2.jpg | caption1 =

<center>

1號月台全景圖（2016年12月）

</center>

}} {{ multiple image | align = center | direction = vertical | width =
750 | image1 = South Horizons Station 2016 12 part3.jpg | caption1 =

<center>

2號月台全景圖（2016年12月）

</center>

}}

### 出入口

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = South Horizons Station exit A.jpg | caption1 = A出入口 | image2 =
Entrance and exit B of South Horizons Station.jpg | caption2 = B出入口 |
image3 = MTR SOH (33).JPG | caption3 = C出入口 }}

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>指示</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/鴨脷洲邨.md" title="wikilink">鴨脷洲邨</a>／<br />
<a href="../Page/海怡半島.md" title="wikilink">海怡半島</a>1-6座</p></td>
<td><p>鴨脷洲邨、海怡半島1-6座、鴨脷洲社區會堂、鴨脷洲邨街市、<a href="../Page/香港小童群益會.md" title="wikilink">香港小童群益會海怡中心</a>、<a href="../Page/香港南區官立小學.md" title="wikilink">香港南區官立小學</a>、海怡東商場、海怡東街市、海怡寶血小學、海怡社區中心</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海怡半島7-23A座</p></td>
<td><p>海怡半島7-23A座、蒙特梭利國際學校、<a href="../Page/海怡西商場.md" title="wikilink">海怡西商場</a>、海怡路</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海怡半島25-33A座</p></td>
<td><p>海怡半島25-33A座、海怡半島17－23A座、公共運輸交匯處、怡南路</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 車站藝術

海怡半島站設兩組車站藝術品。

#### 藝術概覽

| 海怡半島站車站藝術概覽  |
| ------------ |
| 藝術品          |
| 「日」與「夜」\[2\] |
| 翱遊半島         |

#### 相片

{{ multiple image | align = left | width = 250 | image2 = South Horizons
Station Soaring Horizon 201612.jpg | caption2 = 車站藝術品《翱遊半島》 | image3 =
Southeast of Soaring Horizon.jpg | caption3 = 壁畫東南面 | image4 = SOH
Station day and moon art day.jpg| caption4 = 車站藝術品《「日」與「夜」》1 | image5 =
SOH Station day and moon art moon.jpg| caption5 =車站藝術品《「日」與「夜」》2 }}

{{ multiple image | align = center | direction = vertical | width = 750
| image1 = Panorama of Soaring Horizon.jpg | caption1 =

<center>

車站藝術品《翱遊半島》全景圖

</center>

}}

## 歷史

港鐵公司曾就車站選址提出兩個方案，原本的選址在鴨脷洲邨旁的海怡路，屬一個半沉降式車站\[3\]，特色是車站大堂和出口均處於同一層，預計2015年落成。後來因原本選址有[港燈公司的電纜阻礙而預計需要延遲](../Page/港燈公司.md "wikilink")3年至2018年啟用。故港鐵公司提出另一個選址，則在怡南路與利南道交界，屬地底車站。2009年2月，有南區區議員引述港鐵將會採用後者，即車站會如期落成，有關選址在刊憲方案得以確認。

2009年7月版本\[4\]及2010年6月版本\[5\]的刊憲方案圖則之切面圖均顯示車站大堂與兩個月台均處於同一層，而大堂位於月台（路軌盡頭）的西面，與[新加坡地鐵](../Page/新加坡地鐵.md "wikilink")[樟宜機場站樓層佈局相近](../Page/樟宜機場站.md "wikilink")。然而，最終落實的設計已改為將大堂與月台分開兩個樓層。

2016年12月5日，港鐵公司正式公佈於同月28日通車\[6\]，比預期延遲了一年。

South Horizons Station 2016 12 part8.jpg|車站大堂往A出入口通道 South Horizons
Station 2016 12 part7.jpg|車站大堂往B出入口 South Horizons Station Exit C view
201612.jpg|車站大堂往C出入口 South Horizons Station shops.jpg|車站商店 Tong Bao Dim
201612.jpg|B出入口前設唐包點 MTR SOH (37).JPG|行人天橋連接鴨脷洲邨及A出入口

## 特色

  - 該站的月台只有約6米的闊度，是港鐵系統的島式月台中最窄的。\[7\]
  - 該站是港鐵系統中，首個以「非港鐵屬下私人屋苑」全名作為站名的車站。（[美孚站毗鄰美孚新邨](../Page/美孚站.md "wikilink")、[第一城站毗鄰沙田第一城](../Page/第一城站.md "wikilink")、太古站毗鄰太古城，[黃埔站毗鄰黃埔花園](../Page/黃埔站.md "wikilink")，雖然也以「非港鐵屬下私人屋苑」命名，但並非全名）
  - 該站常被坊間人士及傳媒簡稱為「**海怡站**」。
  - 該站是港鐵網路中（除輕鐵外）第一個英文站名有「Z」的中重型鐵路車站。(連輕鐵，共2個)
  - 該站A出口的指示名稱於通車初期為「
    [鴨脷洲邨](../Page/鴨脷洲邨.md "wikilink")／[海怡半島一及四期](../Page/海怡半島.md "wikilink")」，長達13個中文字（連標點），於2017年3月初縮短為「鴨脷洲邨／海怡半島1-6座」。

## 事故

2017年2月14日晚上9時57分，海怡路地下一條淡水管爆裂，引致大量積水沿海怡半島站樓梯及假天花流向站內大堂，列車服務受阻超過3小時。到早上，站內所有升降機和扶手電梯仍然暫停使用。大堂部分店舖受水浸波及，有商戶將貨架最低層的貨品取走。是次水浸的損失難以估算。\[8\]

## 鄰接車站

海怡半島站是南港島綫的上行終點站，乘客可在此乘坐往[金鐘列車](../Page/金鐘站.md "wikilink")，往返[南區及](../Page/南區_\(香港\).md "wikilink")[中西區等地](../Page/中西區_\(香港\).md "wikilink")。

## 利用狀況

車站附近設有[海怡半島及](../Page/海怡半島.md "wikilink")[鴨脷洲邨兩個大型屋苑](../Page/鴨脷洲邨.md "wikilink")，加上鴨脷洲南部（[鴨脷洲工業區](../Page/鴨脷洲工業區.md "wikilink")）的員工在平日星期一至星期五會利用本站轉乘[新巴95線](../Page/新巴95線.md "wikilink")，因此該站使用量頗高。

## 接駁交通

<table>
<thead>
<tr class="header">
<th><p>接駁交通列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 工程期間相片

| A出入口工程期間相片                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Entrance and exit A of South Horizons Station.jpg|海怡半島站A出入口工地（2016年5月） Entrance and exit A of South Horizons Station under construction in March 2016.JPG|海怡半島站A出入口工地（2016年3月） South Horizons Station Exit A in Nov 2015.JPG|海怡半島站A出入口工地（2015年11月） South Horizons Station entrance and exit A under construction in August 2015.JPG|海怡半島站A出入口工地正面（2015年8月） Entrance and exit A of South Horizons Station under construction in August 2015.JPG|海怡半島站A出入口工地背面（2015年8月） Entrance and exit A of South Horizons Station under construction in July 2015.JPG|海怡半島站A出入口工地（背面，2015年7月） Entrance and exit A of South Horizons Station under construction in March 2015.JPG|海怡半島站A出入口工地（2015年3月） |

| B出入口工程期間相片                                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <File:Entrance> and exit B of South Horizons Station under final works in June 2016.jpg|海怡半島站B出入口工地（2016年6月） South Horizons Station entrance and exit B.jpg|海怡半島站B出入口工地（2016年5月） Entrance and exit B of South Horizons Station under construction in March 2016.jpg|海怡半島站B出入口工地（2016年3月） South Horizons Station entrance and exit B under construction in August 2015.JPG|海怡半島站B出入口工地（2015年8月） |

| 西面及C出入口工程期間相片                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Entrance and exit C of South Horizons Station before opening.jpg|海怡半島站C出入口（2016年9月） West construction site of South Horizons Station in September 2016.jpg|車站西面工地路面重舖工程，右下方為C出入口（2016年9月） West construction site of South Horizons Station in May 2016.jpg|海怡半島站西面及C出入口工地（2016年5月） West construction site of South Horizons Station in March 2016.jpg|海怡半島站西面及C出入口工地（2016年3月） South Horizons Station entrance and exit C.jpg|海怡半島站C出入口（2016年5月） South Horizons Station Exit C in Nov 2015 (1).JPG|海怡半島站西面及C出入口工地（2015年11月） Entrance and exit C, west construction site of South Horizons Station in August 2015.jpg|海怡半島站西面及C出入口工地（2015年8月） West construction site of South Horizons Station in December 2014 (brighter version).jpg|海怡半島站西面及C出入口工地（2014年12月） West construction site of South Horizons Station in April 2014.JPG|海怡半島站西面及C出入口工地（2014年4月） West construction site of South Horizons Station.jpg|海怡半島站西面及C出入口工地（2013年4月） |

| 東面工地工程期間相片                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| East construction site of South Horizons Station in September 2016.jpg|車站東面工地路面重舖工程（2016年9月） East construction site of South Horizons Station in May 2016.jpg|海怡半島站東面工地（2016年5月） East construction site of South Horizons Station in March 2016.jpg|海怡半島站東面工地（2016年3月） South Horizons Station Construction near Lee Nam Road in Nov 2015.JPG|海怡半島站東面工地（2015年11月） East construction site of South Horizons Station in August 2015.JPG|海怡半島站東面工地（2015年8月） East construction site of South Horizons Station in December 2014.JPG|海怡半島站東面工地（2014年12月） East construction site of South Horizons Station in April 2014.JPG|海怡半島站東面工地（2014年4月） East construction site of South Horizons Station.jpg|海怡半島站東面工地（2013年4月） |

## 參考資料

## 參看

  - [南港島綫](../Page/南港島綫.md "wikilink")

## 外部連結

  - \[//www.mtr.com.hk/archive/ch/services/maps/soh.pdf 港鐵公司－海怡半島站街道圖\]
  - \[//www.mtr.com.hk/archive/ch/services/layouts/soh.pdf
    港鐵公司－海怡半島站位置圖\]
  - \[//www.mtr.com.hk/archive/ch/services/timetables/soh.pdf
    港鐵公司－海怡半島站列車服務時間\]

[Category:鴨脷洲](../Category/鴨脷洲.md "wikilink")
[Category:以屋苑命名的港鐵車站](../Category/以屋苑命名的港鐵車站.md "wikilink")
[Category:2016年啟用的鐵路車站](../Category/2016年啟用的鐵路車站.md "wikilink")
[Category:南港島綫車站](../Category/南港島綫車站.md "wikilink")
[Category:建在填海/填塘地的港鐵車站](../Category/建在填海/填塘地的港鐵車站.md "wikilink")
[Category:以綠色為主題的港鐵站](../Category/以綠色為主題的港鐵站.md "wikilink")
[Category:南區鐵路車站 (香港)](../Category/南區鐵路車站_\(香港\).md "wikilink")

1.
2.  前稱格線上的樹影
3.  港鐵公司網站：[南港島綫（東段）](http://www.mtr.com.hk/chi/projects/images/sil_brochure_chi.pdf)，於2008年3月2日存取
4.  香港特別行政區路政署：[南港島線（東段）總體規劃圖則SILE-G13](http://www.hyd.gov.hk/tc/road_and_railway/railway_projects/sil_e/gazettal/scheme/doc/gn4569/gl/SILE-G13.pdf)
    切面圖AI （2009年7月版本）
5.  香港特別行政區路政署：[南港島線（東段）總體規劃圖則SILE-G13](http://www.hyd.gov.hk/tc/road_and_railway/railway_projects/sil_e/gazettal/scheme/doc/gn3204/gl/SILE-G13_a1.pdf)
    切面圖AI （2010年6月版本）
6.
7.  [海怡半島站月台僅闊6米、11步之距　黃之鋒憂逼爆月台引起混亂](http://www.hk01.com/sns/article/62054?utm_content=buffer1f564&utm_medium=Social&utm_source=facebook+hk01&utm_campaign=buffer)，香港01，2016-12-25
8.