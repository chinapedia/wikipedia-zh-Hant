[Map_of_Texas_highlighting_Hays_County.svg](https://zh.wikipedia.org/wiki/File:Map_of_Texas_highlighting_Hays_County.svg "fig:Map_of_Texas_highlighting_Hays_County.svg")
**海斯縣**（**Hays County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州中南部的一個縣](../Page/德克薩斯州.md "wikilink")。面積1,761平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口97,589人。縣治[聖馬科斯](../Page/聖馬科斯_\(德克薩斯州\).md "wikilink")（San
Marcos）。

成立於1848年3月1日，縣政府成立於8月7日。縣名是[美墨戰爭時期軍人](../Page/美墨戰爭.md "wikilink")、[加利福尼亞州總測量師約翰](../Page/加利福尼亞州.md "wikilink")·C·海斯。\[1\]

## 教育

截至2009年，全縣有3所高中，5所中學和11所小學。

## 人口

截至2015年德克薩斯州人口估計計劃，該縣人口為193,963人，非西班牙裔白人106,919人（55.1％），非西班牙裔黑人5,860人（3.0％），其他非西班牙裔人，6,624（3.4％），和西班牙裔人和拉美裔人74,560（38.4％）。\[2\]

## 歷史

  - 公元前6000年古印第安人是第一批居民。\[3\]

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/得克萨斯州行政区划.md "wikilink")
[Category:1848年建立的聚居地](../Category/1848年建立的聚居地.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.
2.
3.