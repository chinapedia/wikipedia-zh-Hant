[THE_PRAYA,_HONG-KONG.jpg](https://zh.wikipedia.org/wiki/File:THE_PRAYA,_HONG-KONG.jpg "fig:THE_PRAYA,_HONG-KONG.jpg")
**海旁填海計劃**（**Praya Reclamation
Scheme**），又稱**西環至中環海旁填海計劃**，是[香港](../Page/香港.md "wikilink")[19世紀末的一項主要](../Page/香港開埠初期歷史.md "wikilink")[填海工程](../Page/香港填海工程.md "wikilink")。該工程於1890年開始，並於1903年全面完成，使[香港島的](../Page/香港島.md "wikilink")[西環至](../Page/西環.md "wikilink")[中環得到大片填海土地](../Page/中環.md "wikilink")。

## 沿革

海旁填海計劃的前身是1855年由當時[香港總督](../Page/香港總督.md "wikilink")[寶靈倡議的由](../Page/寶靈.md "wikilink")[中環填至](../Page/中環.md "wikilink")[東角的](../Page/東角.md "wikilink")[寶靈填海計劃](../Page/寶靈填海計劃.md "wikilink")，惟當時在擁有海旁業權的英國商人的反對聲音下，未能得以落實。\[1\]最終政府決定先在[上環和](../Page/上環.md "wikilink")[西環填海](../Page/西環.md "wikilink")，工程於1868年開始，1873年完成，並完成了[寶靈海旁西](../Page/德輔道西.md "wikilink")。\[2\]

到了1875年，政府建議在[西環至](../Page/西環.md "wikilink")[中環海旁填海](../Page/中環.md "wikilink")，但因經費不足及反對聲音下同樣沒有落實。1887年7月，政府再一次重提計劃，而今次由於得到[立法局非官守議員暨](../Page/香港立法局.md "wikilink")[九龍倉公司大班](../Page/九龍倉公司.md "wikilink")[保羅遮打的支持下](../Page/保羅遮打.md "wikilink")\[3\]，計劃成功在1888年落實。經部分修訂後，工程終於在1890年2月起分階段進行。\[4\]

## 內容

海旁填海計劃將[中環的海岸線由當時的](../Page/中環.md "wikilink")[寶靈海旁中北移至今](../Page/德輔道中.md "wikilink")[干諾道中一帶](../Page/干諾道中.md "wikilink")，面積約59[英畝](../Page/英畝.md "wikilink")\[5\]至64英畝\[6\]。為紀念[港督](../Page/港督.md "wikilink")[德輔任內推動填海計劃](../Page/德輔.md "wikilink")，寶靈海旁西和寶靈海旁中改稱為[德輔道西及](../Page/德輔道西.md "wikilink")[德輔道中](../Page/德輔道中.md "wikilink")。然而，由於填海時期的不同，兩條道路並非直接連貫。

## 參考資料

<div class="references-small">

<references />

</div>

## 參見

  - [中環填海計劃 (1950年代)](../Page/中環填海計劃_\(1950年代\).md "wikilink")
  - [海旁東填海計劃](../Page/海旁東填海計劃.md "wikilink")
  - [中環及灣仔填海計劃](../Page/中環及灣仔填海計劃.md "wikilink")

[Category:香港填海工程](../Category/香港填海工程.md "wikilink")
[Category:香港殖民地時期](../Category/香港殖民地時期.md "wikilink")

1.  Bard, Solomon. \[2002\] (2002). Voices from the Past: Hong Kong
    1842-1918. HK University press. ISBN 9622095747

2.
3.  [Amo.gov.hk](http://www.amo.gov.hk/form/AAB_Paper129_queen_annexb_e.pdf)
    : Praya Reclamation Scheme

4.
5.  Wordie, Jason. \[2002\] (2002) Streets: Exploring Hong Kong Island.
    Hong Kong: Hong Kong University Press. ISBN 962-2095631

6.