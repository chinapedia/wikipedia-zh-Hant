**海沧区**是[厦门市所辖的一个区](../Page/厦门市.md "wikilink")，与[厦门岛隔海相望](../Page/厦门岛.md "wikilink")。1989年，国务院批准在原[杏林区成立海沧台商投资区](../Page/杏林区.md "wikilink")\[1\]。

## 历史

海沧区大部分地区（除[东孚外](../Page/东孚.md "wikilink")）原属[漳州府](../Page/漳州府.md "wikilink")[海澄县](../Page/海澄县.md "wikilink")，1958年10月改隶厦门市。原为杏林区一部分，2003年5月经[国务院批准](../Page/国务院.md "wikilink")，杏林区的[杏林街道办事处和](../Page/杏林街道.md "wikilink")[杏林镇划归](../Page/杏林镇.md "wikilink")[集美区管辖](../Page/集美区.md "wikilink")，杏林区其余部分更名为海沧区。

## 行政区划

下辖4个街道办事处：[海沧街道](../Page/海沧街道.md "wikilink")、嵩屿街道、[新阳街道](../Page/新阳街道.md "wikilink")、[东孚街道](../Page/东孚街道.md "wikilink")，及第一农场、海沧农场、天竺山林场、新阳工业区和出口加工区\[2\]。嵩屿街道于2015年设立。\[3\]

## 文化

海沧区的[保生大帝信俗](../Page/保生大帝信俗.md "wikilink")、[海沧蜈蚣阁被列入](../Page/海沧蜈蚣阁.md "wikilink")[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

## 交通

### 公路

[沈海高速公路出口](../Page/沈海高速公路.md "wikilink")，319、324国道途经地点，[海沧大桥](../Page/海沧大桥.md "wikilink")、[海沧海底隧道与厦门岛连接](../Page/海沧海底隧道.md "wikilink")，新阳大桥跨马銮湾连通[集美区](../Page/集美区.md "wikilink")，[厦漳跨海大桥连接](../Page/厦漳跨海大桥.md "wikilink")[龙海市招商局漳州开发区](../Page/龙海市.md "wikilink")。

### 铁路

[15px](../Page/file:China_Railways.svg.md "wikilink")[鹰厦电气化铁路站点](../Page/鹰厦铁路.md "wikilink")，[15px](../Page/file:China_Railways.svg.md "wikilink")[厦深铁路](../Page/厦深铁路.md "wikilink")、[15px](../Page/file:China_Railways.svg.md "wikilink")[龙厦铁路途经地点](../Page/龙厦铁路.md "wikilink")。

### 港口

海沧港、嵩屿港。

## 经济

[Xiamen_Area_of_Fujian_Free_Trade_Zone.jpg](https://zh.wikipedia.org/wiki/File:Xiamen_Area_of_Fujian_Free_Trade_Zone.jpg "fig:Xiamen_Area_of_Fujian_Free_Trade_Zone.jpg")厦门片区。\]\]
海沧区是厦门工业总产值最大的一个区，以电子、机械、生物制药为主导产业，又以台资为主。拥有东孚、新阳、港区三个工业集中区。海沧保税港区是全国第七个保税港区，面积为9.5平方公里。

## 著名人物

  - [邱韻香](../Page/邱韻香.md "wikilink")（1881—1977）（[詩人](../Page/詩人.md "wikilink")、女醫），嘉義名士邱緝臣女，乙未割臺之際隨父遷居漳州海澄，後遊歷香港、江蘇等地。著《繡英閣詩鈔》。

## 参考文献

## 外部链接

  - [厦门市海沧区人民政府  (海沧台商投资区管委会)](http://www.haicang.gov.cn/)
  - [海沧发布（福建省厦门市海沧区委宣传部官方微博）](http://weibo.com/u/5112332878?refer_flag=1001030201_&is_all=1)
  - [厦门市海沧区人民政府官方微博](http://weibo.com/govhaicang?refer_flag=1001030201_)
  - [中国人民政治协商会议厦门市海沧区委员会](https://web.archive.org/web/20161116101232/http://www.xmhczx.gov.cn/)

[海沧区](../Category/海沧区.md "wikilink")
[H](../Category/厦门市辖区.md "wikilink")

1.  [1](http://www.xm.gov.cn/zjxm/xmgk/200708/t20070830_173886.htm)，行政区划--中国厦门。
2.
3.