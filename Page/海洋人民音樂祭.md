**海洋人民音樂祭**是一個原定於2006年7月21日—7月23日在[台灣](../Page/台灣.md "wikilink")[臺北縣](../Page/新北市.md "wikilink")[貢寮鄉境內的](../Page/貢寮區.md "wikilink")[福隆海水浴場舉辦](../Page/福隆海水浴場.md "wikilink")，但受颱風等因素影響未能舉行的[摇滚音樂節](../Category/摇滚音樂節.md "wikilink")。也是[貢寮-{}-國際海洋音樂祭第一屆至第六屆的主辦單位](../Page/貢寮國際海洋音樂祭.md "wikilink")—[角頭音樂自行舉辦的民間版海洋音樂祭](../Page/角頭音樂.md "wikilink")。

## 活動起源

從2000年開始每年舉辦的[貢寮國際海洋音樂祭](../Page/貢寮國際海洋音樂祭.md "wikilink")，直到2005年的第六屆為止都由獨立音樂製作公司「[角頭音樂](../Page/角頭音樂.md "wikilink")」承辦規劃。實際上，貢寮國際海洋音樂祭幕後是由[台北縣政府出資主辦](../Page/台北縣政府.md "wikilink")，因此在2006年時進行的[招標活動由](../Page/招標.md "wikilink")[民間全民電視公司](../Page/民間全民電視公司.md "wikilink")（民視）得標取得主辦權，讓不少多年參與海洋音樂祭的民眾和獨立樂團等音樂工作者感到相當驚訝。雖然錯失官方版本的主辦權，但角頭音樂決定以民間自辦的方式，延續連續五年舉辦海洋音樂祭的精神和傳統。由於貢寮國際海洋音樂祭的名稱以及相關的標誌、設計等有許多版權都屬於台北縣政府所有，角頭無法繼續使用原本的中文名稱，因此活動定名為「海洋人民音樂祭」。

## 取消原因

首屆的海洋人民音樂祭原定於2006年7月21日—7月23日在[台灣](../Page/台灣.md "wikilink")[臺北縣](../Page/新北市.md "wikilink")[貢寮鄉](../Page/貢寮區.md "wikilink")[福隆海水浴場舉辦](../Page/福隆海水浴場.md "wikilink")。但7月13日[碧利斯颱風開始影響台灣](../Page/強烈熱帶風暴碧利斯_\(2006年\).md "wikilink")，使得原定7月14日開始的貢寮國際海洋音樂祭延期一週，改至7月21日—7月23日舉辦，剛好與海洋人民音樂祭的時間重疊。由於海洋人民音樂祭邀請的數組國外樂團和藝人（[Dragon
Ash](../Page/Dragon_Ash.md "wikilink")、[崔健等](../Page/崔健.md "wikilink")）檔期無法配合延期，因此活動無法跟著順延，主辦單位只好在7月12日正式宣佈取消。

## 演出活動

<small>由於海洋人民音樂祭已經取消舉行，因此以下的樂團名單並未實際演出。</small>

  - 7月21日

<!-- end list -->

  -
    城市之光（2005海洋音樂祭評審團大獎得獎樂團）、[王宏恩](../Page/王宏恩.md "wikilink")、[張懸](../Page/張懸.md "wikilink")、雅Miyavi（日本）、[Dragon
    Ash](../Page/Dragon_Ash.md "wikilink")

<!-- end list -->

  - 7月22日

<!-- end list -->

  -
    [圖騰樂團](../Page/圖騰樂團.md "wikilink")（2005海洋大賞首獎）

<!-- end list -->

  - 7月23日

<!-- end list -->

  -
    So
    What（2005海洋音樂祭票選最受歡迎獎）、[1976](../Page/1976_\(樂團\).md "wikilink")、[陳建年](../Page/陳建年_\(歌手\).md "wikilink")、[MC
    HOTDOG和](../Page/MC_HOTDOG.md "wikilink")[張震嶽](../Page/張震嶽.md "wikilink")+Free
    9樂團、[崔健](../Page/崔健.md "wikilink")（中國）

## 海洋人民音樂大賞

<small>由於海洋人民音樂祭已經取消舉行，因此沒有最後的名次，僅有入圍名單。</small>

  - 總決賽入圍樂團：

<!-- end list -->

  -
    教練樂團 、胖虎 、bellytree the distortion 、Vista 、毒殺芬 、LuckyPie 、OpenEye
    、[表兒](../Page/表兒.md "wikilink") 、DIGIHAI 、巴西瓦里

<!-- end list -->

  - **海洋大賞評審團**

:\* 小柯（[濁水溪公社樂團](../Page/濁水溪公社.md "wikilink")）

:\* 小寶（前瓢蟲樂團）

:\* [MC HOTDOG](../Page/MC_HOTDOG.md "wikilink")

:\* 亂彈阿翔

:\* 段書珮（ELVIS音樂雜誌主編）

:\* 翁嘉銘（樂評家）

:\* 葉雲平（樂評家）

## 外部連結

  - [官方網站](https://web.archive.org/web/20130605123722/http://hohaiyan.com/)
  - [海洋人民音樂祭 初賽樂團演出片段](http://www.kkbox.com.tw/event/hohaiyan/index.php)
  - [海洋人民音樂祭部落格](http://www.streetvoice.com.tw/profile/home.asp?sd=1793)

[Category:台灣音樂節](../Category/台灣音樂節.md "wikilink")
[Category:搖滾音樂節](../Category/搖滾音樂節.md "wikilink")
[Category:新北市文化](../Category/新北市文化.md "wikilink")
[Category:貢寮區](../Category/貢寮區.md "wikilink")