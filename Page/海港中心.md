[Harbour_Centre.jpg](https://zh.wikipedia.org/wiki/File:Harbour_Centre.jpg "fig:Harbour_Centre.jpg")
[Harbour_Centre_Entrance_2015.jpg](https://zh.wikipedia.org/wiki/File:Harbour_Centre_Entrance_2015.jpg "fig:Harbour_Centre_Entrance_2015.jpg")
**海港中心**（**Harbour
Centre**）是[香港一座寫字樓](../Page/香港.md "wikilink")，位處[灣仔](../Page/灣仔.md "wikilink")[港灣道](../Page/港灣道.md "wikilink")25號，樓高33層，於1983年落成，鄰近[香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")、[灣仔碼頭等地點](../Page/灣仔碼頭.md "wikilink")。

大樓內集有多個國家的領事館，包括[澳洲駐港總領事館](../Page/澳洲駐港總領事館.md "wikilink")、[津巴布韋駐港總領事館](../Page/津巴布韋.md "wikilink")、[摩納哥駐港名譽領事館等等](../Page/摩納哥.md "wikilink")，其他租戶包括[東方海外貨櫃航運公司](../Page/東方海外貨櫃航運公司.md "wikilink")，IDP教育有限公司（28樓2807-09室），以及[選舉事務處](../Page/選舉事務處.md "wikilink")、[選舉管理委員會的辦事處等](../Page/香港選舉管理委員會.md "wikilink")。

海港中心下層設有商場，名為[海港中心商場](../Page/海港中心商場.md "wikilink")，面積有40,865平方呎。

<File:HK> Wan Chai Platform Great Eagle Centre n Central
Plaza.JPG|海港中心及[中環廣場](../Page/中環廣場.md "wikilink")（左） <File:HK>
Great Eagle Centre 2007.jpg|海港中心（右二），旁為鷹君中心（最右方大廈） <File:Harbour> Centre
Access 2014.jpg|海港中心平台通道

## 鄰近

  - [鷹君中心](../Page/鷹君中心.md "wikilink")
  - [香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")
  - [灣仔碼頭](../Page/灣仔碼頭.md "wikilink")
  - [新鴻基中心](../Page/新鴻基中心.md "wikilink")
  - [中環廣場](../Page/中環廣場.md "wikilink")
  - [入境事務大樓](../Page/入境事務大樓.md "wikilink")
  - [稅務大樓](../Page/稅務大樓.md "wikilink")
  - [灣仔政府大樓](../Page/灣仔政府大樓.md "wikilink")

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[灣仔站A](../Page/灣仔站.md "wikilink")5出口
  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/南北綫_\(香港\).md "wikilink")：[會展站B出入口](../Page/會展站.md "wikilink")（2021年通車\[1\]）

<!-- end list -->

  - [渡輪](../Page/香港渡輪.md "wikilink")

<!-- end list -->

  - [灣仔碼頭](../Page/灣仔碼頭.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 外部連結

  - [海港中心](http://www.shkp.com/html/harbourcentre/index.html)

[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:灣仔區商場](../Category/灣仔區商場.md "wikilink")
[Category:灣仔區寫字樓](../Category/灣仔區寫字樓.md "wikilink")
[Category:灣仔北](../Category/灣仔北.md "wikilink")
[Category:1983年完工建築物](../Category/1983年完工建築物.md "wikilink")

1.