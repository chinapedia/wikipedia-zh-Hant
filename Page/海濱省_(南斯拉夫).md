**海滨省**
([克罗地亚语和](../Page/克罗地亚语.md "wikilink")[波斯尼亚语](../Page/波斯尼亚语.md "wikilink")：**)
在1929至39年是[南斯拉夫王国的一个省](../Page/南斯拉夫王国.md "wikilink")，领土包括古[达尔马提亚大部分](../Page/达尔马提亚.md "wikilink")，亦即今[克罗地亚沿海领土](../Page/克罗地亚.md "wikilink")，以及[波斯尼亚和黑塞哥维纳部分地区](../Page/波斯尼亚和黑塞哥维纳.md "wikilink")，因滨临[亚得里亚海而得名](../Page/亚得里亚海.md "wikilink")，首府是[斯普利特](../Page/斯普利特.md "wikilink")。

1939年，该省与[萨瓦河省和邻近省份部分地区合并](../Page/萨瓦河省.md "wikilink")，组成[克罗地亚省](../Page/克罗地亚省.md "wikilink")。

1941年，该省被[轴心国占领](../Page/轴心国.md "wikilink")。从[斯普利特到](../Page/斯普利特.md "wikilink")[扎达尔的沿海地区被](../Page/扎達爾.md "wikilink")[意大利占领](../Page/意大利.md "wikilink")，而其余地区变成[克罗地亚独立国一部分](../Page/克罗地亚独立国.md "wikilink")。战后，该地大部分成为[南斯拉夫社会主义联邦共和国下辖的](../Page/南斯拉夫社会主义联邦共和国.md "wikilink")[波斯尼亚和黑塞哥维纳和](../Page/波斯尼亚和黑塞哥维纳.md "wikilink")[克罗地亚部分](../Page/克罗地亚.md "wikilink")。

[Category:南斯拉夫王國行政區劃](../Category/南斯拉夫王國行政區劃.md "wikilink")
[Category:克罗地亚行政区划史](../Category/克罗地亚行政区划史.md "wikilink")
[Category:波斯尼亚和黑塞哥维那行政区划史](../Category/波斯尼亚和黑塞哥维那行政区划史.md "wikilink")