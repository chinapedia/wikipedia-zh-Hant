**海濱車站**位於[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[瑞芳區](../Page/瑞芳區.md "wikilink")，為[台鐵](../Page/台鐵.md "wikilink")[深澳線之車站](../Page/深澳線.md "wikilink")，原名焿子寮，於1989年深澳線裁撤客運服務時廢止。

## 車站構造

[海濱車站-20170705.jpg](https://zh.wikipedia.org/wiki/File:海濱車站-20170705.jpg "fig:海濱車站-20170705.jpg")

  - [島式月台一座](../Page/島式月台.md "wikilink")，水災後因地基流失停用。
  - [岸式月台一座](../Page/岸式月台.md "wikilink")。(已拆除，水災後用枕木臨時搭建)

## 利用狀況

  - 廢除前為招呼站。

## 歷史

  - 本站在[日治時期舊名為](../Page/日治時期.md "wikilink")「焿子寮」。
  - 1967年8月25日：設站，站名為「焿寮」。
  - 1967年10月31日：站名改為「海濱」，為三等站。
  - 1971年3月1日：降為簡易站，由濂洞站管理。
  - 1977年12月1日：為配合[北部濱海公路興建](../Page/北部濱海公路.md "wikilink")，停止營運。
  - 1978年1月11日：恢復營運。
  - 1986年7月12日：配合濱海公路工程，海濱－濂洞間鐵道路線拆除。
  - 1987年7月28日：降為招呼站，由深澳站管理。
  - 1989年8月21日：隨著深澳線停駛而廢止。

## 鄰近車站

  - **廢止營運模式**

## 外部連結

  - [我的赤腳旅行](http://blog.udn.com/barefoot/1118278)
  - [海濱車站的月台遺跡（照片）](http://photo.xuite.net/starr0228/2743998/62.jpg)

[H](../Category/深澳線車站.md "wikilink")
[深](../Category/台灣鐵路廢站.md "wikilink")
[H](../Category/1989年關閉的鐵路車站.md "wikilink")
[H](../Category/1967年啟用的鐵路車站.md "wikilink")