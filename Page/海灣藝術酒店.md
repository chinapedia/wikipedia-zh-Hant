**海灣藝術酒店**是位於[台灣](../Page/台灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[北區](../Page/北區_\(台中市\).md "wikilink")[中清路](../Page/中清路.md "wikilink")、[漢口路口的觀光飯店](../Page/漢口路_\(台中市\).md "wikilink")，創設於1989年6月時為「通豪大飯店」，為一幢24層樓建築物。擁有二百餘間客房，並具有中、西餐廳、宴會大廳、空中露天游泳池、[三溫暖](../Page/三溫暖.md "wikilink")、健身房等設施。

## 歷史

  - 1989年6月29日，「**通豪大飯店**」創立於[臺中市](../Page/臺中市.md "wikilink")[北區](../Page/北區_\(台中市\).md "wikilink")。
  - 2003年，設在[台東縣](../Page/台東縣.md "wikilink")[卑南鄉](../Page/卑南鄉.md "wikilink")[知本溫泉區的](../Page/知本溫泉.md "wikilink")「知本通豪大飯店」開始獨立經營。
  - 2018年[清明節連假期間爆發疑似超賣事件](../Page/清明節.md "wikilink")。\[1\]6月，由海灣酒店集團收購並改名為「**海灣藝術酒店**」。

## 交通

  - [台中市公車](../Page/台中市公車.md "wikilink")

<!-- end list -->

  - [台中客運](../Page/台中客運.md "wikilink")：[6](../Page/台中市公車6路.md "wikilink")、[9](../Page/台中市公車9路.md "wikilink")、[29](../Page/台中市公車29路.md "wikilink")、[33](../Page/台中市公車33路.md "wikilink")、[101](../Page/台中市公車101路.md "wikilink")、[108](../Page/台中市公車108路.md "wikilink")、[115](../Page/台中市公車115路.md "wikilink")、[154](../Page/台中市公車154路.md "wikilink")
  - [統聯客運](../Page/統聯客運.md "wikilink")：[61](../Page/台中市公車61路.md "wikilink")
  - [東南客運](../Page/東南客運.md "wikilink")：[67](../Page/台中市公車67路.md "wikilink")
  - [豐榮客運](../Page/豐榮客運.md "wikilink")：[127](../Page/台中市公車127路.md "wikilink")

## 週邊設施

  - [臺中市私立曉明女子高級中學](../Page/臺中市私立曉明女子高級中學.md "wikilink")
  - [臺中市立立人國民中學](../Page/臺中市立立人國民中學.md "wikilink")
  - [臺中市北區立人國民小學](../Page/臺中市北區立人國民小學.md "wikilink")
  - [臺中市北區賴厝國民小學](../Page/臺中市北區賴厝國民小學.md "wikilink")
  - [臺中市北區中華國民小學](../Page/臺中市北區中華國民小學.md "wikilink")
  - 天津路商圈

| 戶外證婚                                                         | 戶外證婚                                                             |
| ------------------------------------------------------------ | ---------------------------------------------------------------- |
| [缩略图](https://zh.wikipedia.org/wiki/File:戶外婚禮.jpg "fig:缩略图") | [缩略图](https://zh.wikipedia.org/wiki/File:6U1A4481.jpg "fig:缩略图") |

## 資料來源

<references/>

## 外部連結

  - [海灣藝術酒店](https://rsv.ec-hotel.net/webhotel/0807?_rand=lh3stjgoms)

[P](../Page/category:台中市旅館.md "wikilink")

[P](../Category/1989年完工建築物.md "wikilink") [Category:北區
(臺中市)](../Category/北區_\(臺中市\).md "wikilink")

1.  [聯合報- 通豪飯店超賣房間還趕人](https://udn.com/news/story/7266/3072063)