{{ Otheruses|subject=海生的[開花植物](../Page/開花植物.md "wikilink")|海藻}}
**海草**指分屬四個[植物](../Page/植物.md "wikilink")[科](../Page/科.md "wikilink")（[波喜盪草科](../Page/波喜盪草科.md "wikilink")、[大葉藻科](../Page/大葉藻科.md "wikilink")、[水鱉科以及](../Page/水鱉科.md "wikilink")[絲粉藻科](../Page/絲粉藻科.md "wikilink")），生長在[海洋和完全](../Page/海洋.md "wikilink")[鹽水環境的一類](../Page/鹽水.md "wikilink")[開花植物](../Page/開花植物.md "wikilink")。

## 生態學

[Posidonia_2_Alberto_Romeo.jpg](https://zh.wikipedia.org/wiki/File:Posidonia_2_Alberto_Romeo.jpg "fig:Posidonia_2_Alberto_Romeo.jpg")\]\]
這些特殊的海中開花植物之所以被稱為是「海草」，是由於它們的葉片又長又細，絕大部分是綠色的，而且植株群時常生長在廣大的「[草地](../Page/草地.md "wikilink")」上，看起來就像是一大片的草原：在其它的辭彙中，多數的水草種類從表面上看來類似陸生的[禾本科草類](../Page/禾本科.md "wikilink")。

由於這類植物必須行[光合作用](../Page/光合作用.md "wikilink")，它們的生長受限於浸沒的[透光帶](../Page/透光帶.md "wikilink")，且絕大多數存在於淺攤與隱匿海岸，固定生長在沙灘或是泥沙底部。當它們完成全部的水下生命週期時才會接受[授粉](../Page/授粉.md "wikilink")。全世界的這類植物約有60種（儘管在[分類學上仍是有爭議的](../Page/分類學.md "wikilink")），目前已知[南中國海約有](../Page/南中國海.md "wikilink")20種，[香港約有](../Page/香港.md "wikilink")3種，[台灣](../Page/台灣.md "wikilink")（含[澎湖](../Page/澎湖.md "wikilink")、[金門](../Page/金門.md "wikilink")、[馬祖](../Page/馬祖.md "wikilink")、[東沙](../Page/東沙.md "wikilink")）約有10種已紀錄到，實際種類可能會有所增加。

海草組成大規模的海床或是草地，可以是由單屬種（僅由一種構成）或多屬種（多於一種共存）任一組成。在[溫帶](../Page/溫帶.md "wikilink")，通常僅有1種或少數幾種具有優勢（像是在[北大西洋的](../Page/北大西洋.md "wikilink")[大葉藻](../Page/大葉藻.md "wikilink")），然而在[熱帶的海床通常有更多的不同種類](../Page/熱帶.md "wikilink")，[菲律賓有超過](../Page/菲律賓.md "wikilink")13種紀錄。

[White-spotted_puffer.jpg](https://zh.wikipedia.org/wiki/File:White-spotted_puffer.jpg "fig:White-spotted_puffer.jpg")通常可發現於海草區。\]\]
[Posidonia1.jpg](https://zh.wikipedia.org/wiki/File:Posidonia1.jpg "fig:Posidonia1.jpg")\]\]
海草床具有高度多樣性，而且是富有生產力的[生態系統](../Page/生態系統.md "wikilink")，並能提供躲藏處給來自所有[分類門的數以百計的物種組合](../Page/門_\(生物\).md "wikilink")，比如年幼和成熟的[魚](../Page/魚.md "wikilink")、附著並自由營生的[海藻與](../Page/海藻.md "wikilink")[微細植物](../Page/微細植物.md "wikilink")、[軟體動物](../Page/軟體動物.md "wikilink")、[剛毛蟲以及](../Page/剛毛蟲.md "wikilink")[線蟲等](../Page/線蟲.md "wikilink")。起初被認為是少數種類以海草[葉為食](../Page/葉.md "wikilink")（部分由於它們的營養含量不足），然而學術評論與實際使用的改進分類法顯示，在食物鏈中海草[草食性是高度重要的一環鏈結](../Page/草食性.md "wikilink")，全世界有上百個物種皆以攝取海草為食，包括[儒艮](../Page/儒艮.md "wikilink")、[海牛](../Page/海牛.md "wikilink")、[魚](../Page/魚.md "wikilink")、[鵝](../Page/鵝.md "wikilink")、海龜、[天鵝](../Page/天鵝.md "wikilink")、[海膽以及](../Page/海膽.md "wikilink")[螃蟹](../Page/螃蟹.md "wikilink")。

海草有時候也被稱為「[生態系統環境建設者](../Page/生態系統環境建設者.md "wikilink")」，因為它們某種程度上營造出特有的[棲息地](../Page/棲息地.md "wikilink")：長葉能使水流減速而增進[沉澱](../Page/沉澱.md "wikilink")，並且海草的[根與](../Page/根.md "wikilink")[地下莖可安定海床](../Page/地下莖.md "wikilink")。它們對物種組合真正的重要性大概就是提供躲藏處了（透過它們在水體中三度空間的結構），而且[初級生產有格外高度的比率仰賴它們](../Page/初級生產.md "wikilink")。因此，海草供給[沿岸帶一些](../Page/沿岸帶.md "wikilink")「[生態系統貨物](../Page/生態系統貨物.md "wikilink")」與「[生態系統服務](../Page/生態系統服務.md "wikilink")」，例如[漁場](../Page/漁場.md "wikilink")、[防浪湧](../Page/防浪湧.md "wikilink")、產生[氧氣以及保護海岸來抵抗](../Page/氧氣.md "wikilink")[侵蝕作用](../Page/侵蝕.md "wikilink")。

## 用途

海草有被人為採集製成用於沙土的肥料，這在[葡萄牙的Ria](../Page/葡萄牙.md "wikilink") de
Aveiro是重要的活動，植株的收集成品被稱為「moliço」。20世紀早期，海草曾為法國所用，並在海峽群島小規模的做成床墊（草褥）的形狀充填物，且在[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，由於法國部隊的緣故而有很高的供需。近年海草則是用於傢俱，以及如同藤莖一樣應用在紡織。

## 對海草的擾亂與威脅

自然的擾亂諸如[放牧](../Page/放牧.md "wikilink")、[風暴](../Page/風暴.md "wikilink")、鋸冰以及[乾旱是海草生態系統動態中本身就有的一部分](../Page/乾旱.md "wikilink")。海草顯示出格外高的[表型可塑性層級](../Page/表型可塑性.md "wikilink")，迅速適應環境改變的條件。然而，全球的海草數量卻正在下降，前次十年期間消逝了3萬平方公里。對此次下降的緣由便是人為的擾亂，最明顯的是[優氧化](../Page/優氧化.md "wikilink")、棲息地遭受機械毀壞以及[過漁](../Page/過漁.md "wikilink")。過度輸入的營養物（[氮與](../Page/氮.md "wikilink")[磷](../Page/磷.md "wikilink")）對海草而言是直接的毒性，但是最重要的，這會刺激附著與自由漂浮的巨大和微小的[海藻生長](../Page/海藻.md "wikilink")。這種結果造成減少[陽光到達海草的葉片上](../Page/陽光.md "wikilink")，減少[光合作用與](../Page/光合作用.md "wikilink")[初級生產](../Page/初級生產.md "wikilink")。腐壞的海草葉與海藻激起[藻華增殖](../Page/藻華.md "wikilink")，導致正向[反饋](../Page/反饋.md "wikilink")。這種情形可能引發徹底的從海草過渡到海藻的[相互消長](../Page/相互消長.md "wikilink")。累積的證據也支持過度捕撈頂端的掠食者（廣泛的掠食魚類）可能間接增加海藻生長，藉由實行減低放牧控制[中級草食者](../Page/中級草食者.md "wikilink")，諸如[甲殼類與](../Page/甲殼亞門.md "wikilink")[腹足類通過](../Page/腹足綱.md "wikilink")「[營養瀑布](../Page/營養瀑布.md "wikilink")」。以最多使用的方式進行保護與回復海草地，包括減低營養層級與污染，用[海洋保護區達成保育](../Page/海洋保護區.md "wikilink")，以及運用[移植來復原族群](../Page/移植_\(植物\).md "wikilink")。

## 海草的屬

  - [波喜盪草科](../Page/波喜盪草科.md "wikilink") Posidoniaceae
      - [波喜盪草屬](../Page/波喜盪草屬.md "wikilink") *Posidonia*

<!-- end list -->

  - [大葉藻科](../Page/大葉藻科.md "wikilink") Zosteraceae （甘藻科）
      - [大葉藻屬](../Page/大葉藻屬.md "wikilink") *Zostera*
        （甘藻屬），[異葉藻屬](../Page/異葉藻屬.md "wikilink")
        已併入此屬中。
      - [蝦海藻屬](../Page/蝦海藻屬.md "wikilink") *Phyllospadix*

<!-- end list -->

  - [水鱉科](../Page/水鱉科.md "wikilink") Hydrocharitaceae
      - [海菖蒲屬](../Page/海菖蒲屬.md "wikilink") *Enhalus*
      - [喜鹽草屬](../Page/喜鹽草屬.md "wikilink") *Halophila*
      - [泰來藻屬](../Page/泰來藻屬.md "wikilink") *Thalassia*

<!-- end list -->

  - [絲粉藻科](../Page/絲粉藻科.md "wikilink") Cymodoceaceae
      - [根枝草屬](../Page/根枝草屬.md "wikilink") *Amphibolis*
      - [絲粉藻屬](../Page/絲粉藻屬.md "wikilink") *Cymodocea*
      - [二藥藻屬](../Page/二藥藻屬.md "wikilink") *Halodule*
      - [針葉藻屬](../Page/針葉藻屬.md "wikilink") *Syringodium*
      - [全楔草屬](../Page/全楔草屬.md "wikilink") *Thalassodendron*

## 參考文獻

<div class="references-small">

  - 第34章353節至356節

  - 第377頁至391頁

  - 第6頁、第8頁、第14頁至第15頁、第16頁

  - 第38頁至第41頁

  - 第163頁至第168頁

  - 第50頁至第55頁

  - 第56頁至第59頁；第342頁、第344頁、第348頁、第370頁、第372頁

  -
  - den Hartog, C. 1970. *The Sea-grasses of the World*. *Verhandl. der
    Koninklijke Nederlandse Akademie van Wetenschappen, Afd.
    Natuurkunde*, No. 59(1).

  - Hemminga, M.A. & Duarte, C. 2000. *Seagrass Ecology*. Cambridge
    University Press, Cambridge. 298 pp.

  - Short, F.T. & Coles, R.G.(eds). 2001. *Global Seagrass Research
    Methods*. Elsevier Science, Amsterdam. 473 pp.

  - Green, E.P. & Short, F.T.(eds). 2003. World Seagrass Atlas. UNEP
    World Conservation Monitoring Centre, UCP, Berkely. 286 pp.

  - A.W.D. Larkum, R.J. Orth, and C.M. Duarte (eds). Seagrass Biology: A
    Treatise. CRC Press, Boca Raton, FL, in press.

</div>

[澤瀉目](../Category/澤瀉目.md "wikilink")