**海螯蝦科**（學名：），又名**螯龙虾科**、**澳洲龍蝦**、**美國大龍蝦**，有一對非常巨大的鉗子，是一種大型海生[甲殼類的總稱](../Page/甲殼類.md "wikilink")，生物学上属于[十足目](../Page/十足目.md "wikilink")。

## 分類

  - Neophoberinae
      - 密刺螯蝦屬 *Acanthacaris*
  - Thymopinae
      - 擬海螯蝦屬 *Nephropsis*
      - *Nephropides*
      - *Thymops*
      - *Thymopsis*
  - 海螯蝦亞科 Nephropinae
      - [螯龍蝦屬](../Page/螯龍蝦屬.md "wikilink") *Homarus*
          - [美洲螯龍蝦](../Page/美洲螯龍蝦.md "wikilink")'' H. americanus''種
          - [歐洲螯龍蝦](../Page/歐洲螯龍蝦.md "wikilink")'' H. gammarus''種
      - [海螯蝦屬](../Page/海螯蝦屬.md "wikilink") *Nephrops*
      - *Homarinus*
      - [後海螯蝦屬](../Page/後海螯蝦屬.md "wikilink") *Metanephrops*
      - *Eunephrops*
      - *Thymopides*

## 生態

海螯蝦分布於世界各地的海域，大中華地區主要是各種[後海螯蝦](../Page/後海螯蝦屬.md "wikilink")，由於憩息在至少200米深的海底（大多種類更深），不易俘獲也較爲罕見；日本、印尼則还可找到[鋸指螯蝦](../Page/鋸指螯蝦屬.md "wikilink")，亦憩息于深海。

海螯蝦通常棲息於從[海岸到](../Page/海岸.md "wikilink")[大陸棚邊緣的石質](../Page/大陸棚.md "wikilink")、沙質或泥質[海床](../Page/海床.md "wikilink")。海螯蝦通常以活的魚類、貝類、其他甲殼類及海草為食，有時也會吃動物屍體；在圈養環境下，還有可能[同類相食](../Page/同類相食.md "wikilink")。

海螯蝦終其一生都會不斷進行[脫殼](../Page/脫殼.md "wikilink")。成熟的海螯蝦可能有幾乎不再[老化的能力](../Page/老化.md "wikilink")，在沒有受傷、疾病及被捕捉的情況下有著很長的壽命\[1\]，有可能超過100年\[2\]。

海螯蝦的年紀可根據重量來推算，每一磅（0.454公斤）可換算為7到10歲。金氏記錄中捕獲的最大海螯蝦重達20.14公斤，全長從尾部到大螯尖端達1.06公尺，1977年捕獲於[加拿大](../Page/加拿大.md "wikilink")[新斯科舍外海](../Page/新斯科舍.md "wikilink")，是世界上最重的海生甲殼類\[3\]。

2009年1月，一隻重達20磅（約9公斤）、名為「喬治」的海螯蝦於美國東北部[緬因州坎尼邦克港附近放生](../Page/緬因州.md "wikilink")，該隻蝦是在約兩週前於加拿大[紐芬蘭外海被捕](../Page/紐芬蘭.md "wikilink")。因此推估「喬治」已140歲了\[4\]。

<center>

<File:Astice.jpg>|[歐洲螯龍蝦](../Page/歐洲螯龍蝦.md "wikilink") *Homarus
gammarus*
[File:KreeftbijDenOsse.jpg|歐洲螯龍蝦](File:KreeftbijDenOsse.jpg%7C歐洲螯龍蝦)
*H. gammarus* <File:Обыкновенный> омар.jpg|歐洲螯龍蝦 *H. gammarus*
<File:Bubba>, the 22-lb. lobster (5476104).jpg|美洲螯龍蝦 *Homarus
americanus*

</center>

## 與[龍蝦的不同](../Page/龍蝦.md "wikilink")

## 烹飪

海螯蝦也是一种著名的西方美食。海螯蝦通常被活生生的煮熟；大部份廚師會將活的海螯蝦放進滾水中煮熟或蒸熟，部份先用[水产品电击机電死減少其痛苦](../Page/水产品电击机.md "wikilink")。2017年在澳洲悉尼，一間海鮮市場餐廳未讓龍蝦首先失去知覺再宰殺，被悉尼[Downing
Centre法庭指控虐待动物](../Page/:en:Downing_Centre.md "wikilink")，宣布判处餐厅罚款1500[澳元](../Page/澳元.md "wikilink")。这是澳州皇家防止虐待动物协会（[RSPCA
Australia](../Page/:en:RSPCA_Australia.md "wikilink")）处理的第一起甲壳类动物虐待案件。\[5\]海螯蝦也可以用炸、烤、烘等其他方式料理，當做主餐、[沙拉或](../Page/沙拉.md "wikilink")[濃湯](../Page/濃湯.md "wikilink")，或是與[蛋黄酱混合做成龍蝦卷](../Page/蛋黄酱.md "wikilink")。大部份蝦肉位於尾部和兩支大螯，腳部和軀幹也有較少部份的肉可供食用。龍蝦肉通常會沾[酥油](../Page/酥油.md "wikilink")，可以提升甜味。

<center>

<File:Lobster> at Fisherman's
Wharf.JPG|[舊金山漁人碼頭販賣的](../Page/舊金山漁人碼頭.md "wikilink")“龍蝦”（海螯蝦）
<File:Hummer> gekocht.jpg|“龍蝦”（海螯蝦）大餐 <File:Boiled> Maine
Lobster.jpg|煮熟的海螯蝦 <File:Homar> 3.jpg|Lobster in restaurant,
Dubrovnik

</center>

## 註解

[\*](../Category/螯蝦.md "wikilink")
[海螯蝦科](../Category/海螯蝦科.md "wikilink")

1.  *Emerging Area of Aging Research: Long-Lived Animals with
    "Negligible Senescence"*, John C. Guerin. Annals of the New York
    Academy of Sciences 1019 (1) , 518–520.
    ([abstract](http://www.blackwell-synergy.com/doi/abs/10.1196/annals.1297.096))
2.
3.
4.  <http://tw.news.yahoo.com/article/url/d/a/090111/78/1cs1p.html>
5.