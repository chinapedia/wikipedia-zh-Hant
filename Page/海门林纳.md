**海门林纳**（，）是原[南芬兰省的首府](../Page/南芬兰.md "wikilink")，它隶属于[坎塔海梅区](../Page/坎塔海梅区.md "wikilink")，地处[赫尔辛基](../Page/赫尔辛基.md "wikilink")（98公里）和[坦佩雷](../Page/坦佩雷.md "wikilink")（75公里）中间的位置。

城市的名字源自坐落在该市的中世纪建筑[海梅城堡](../Page/海梅城堡.md "wikilink")（）。

## 历史

[Tavastehus.jpg](https://zh.wikipedia.org/wiki/File:Tavastehus.jpg "fig:Tavastehus.jpg")
海门林纳是位于瓦纳亚湖末端的古老居住地。市中心现存有很多著名而带争论性的考古遗址，比如铁器时代和中世纪初期的聚居，这些被解读成当时的名称“瓦纳亚城”(Vanain
kaupunki)。

海门林纳始建于1200年代现在的Linnanniemi，不久后城堡北边开始人口开始增多。这个微不足道的居住中心

从維京时代开始已经有一个名叫[瓦纳亚的定居点存在于该市现在的位置上了](../Page/瓦纳亚.md "wikilink")。古堡始建于13世纪后半叶，当时作为芬兰中部对瑞典势力的防御。在[海梅城堡旁边的一个村庄成为居民的主要供给来源](../Page/海梅城堡.md "wikilink")。

1639年，这个村庄获得了城市权利，不久后瑞典国王将它向南迁移了1公里到了它现在的位置。

该市的知名度来源于很多著名芬兰人士都求学于该市的学校、学府。这里的学校、政府和军队全面地描绘出了海门林纳的历史生命。

1862年，在海门林纳和赫尔辛基之间，芬兰开通了第一条铁路线。现在使用的海门林纳火车站（*Hämeenlinna's
Rautatieasema*）是后来在1921年建造的。

1948年1月1日，海门林纳第一次大规模扩建完成，城市北部的大部分土地由原来的海门林纳下区和瓦纳亚区合并到该市。1967年初，海门林纳迄今最大一次扩张完成，瓦纳亚周末三大城市归入该市。2007年11月，海门林纳确定了区成员：豪霍，伽勒沃拉，拉米，伦果和杜洛斯，有效期至2009年1月1日。

## 著名人物

作曲家[让·西贝柳斯出生](../Page/让·西贝柳斯.md "wikilink")、成长于海门林纳。1885年他从海门林纳高中毕业。

[一级方程式赛车手](../Page/一级方程式.md "wikilink")[基米·莱科宁和](../Page/基米·莱科宁.md "wikilink")[燕妮·达尔曼于](../Page/燕妮·达尔曼.md "wikilink")2004年在海门林纳结婚。

## 经济

[Hameenlinna_lake_vanajavesi.jpg](https://zh.wikipedia.org/wiki/File:Hameenlinna_lake_vanajavesi.jpg "fig:Hameenlinna_lake_vanajavesi.jpg")

### 最大的雇主 (雇员人数) \[1\]

  - 海门林纳市：2490
  - 芬兰中央政府: 2480
  - Kanta-Häme Hospital District: 1460
  - [Ruukki](../Page/Rautaruukki.md "wikilink") (Rautaruukki Oyj): 1030
  - Huhtamäki Oyj: 700
  - [Hämeen AMK](../Page/Hämeen_ammattikorkeakoulu.md "wikilink"): 510
  - Aina Group Oyj: 500
  - Kansanterveystyön ky: 490
  - [Patria Vehicles](../Page/Patria_Vehicles.md "wikilink") Oy: 430
  - Koulutuskeskus Tavastia: 270
  - Konecranes Standard Lifting Oy: 250
  - Lindström Oy: 175

## 友好城市\[2\]

  - [挪威](../Page/挪威.md "wikilink")[贝鲁姆](../Page/贝鲁姆.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[策勒](../Page/策勒_\(德国\).md "wikilink")

  - [格陵兰](../Page/格陵兰.md "wikilink")

  - [丹麦](../Page/丹麦.md "wikilink")[腓特烈斯贝](../Page/腓特烈斯贝自治市.md "wikilink")

  - [冰岛](../Page/冰岛.md "wikilink")[哈夫纳夫约杜尔](../Page/哈夫纳夫约杜尔.md "wikilink")

  - [匈牙利](../Page/匈牙利.md "wikilink")[皮什珀克洛达尼](../Page/皮什珀克洛达尼.md "wikilink")

  - [爱沙尼亚](../Page/爱沙尼亚.md "wikilink")[塔尔图](../Page/塔尔图.md "wikilink")

  - [波兰](../Page/波兰.md "wikilink")[托伦](../Page/托伦.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[特维尔](../Page/特维尔.md "wikilink")

  - [瑞典](../Page/瑞典.md "wikilink")[乌普萨拉](../Page/乌普萨拉.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[魏玛](../Page/魏玛.md "wikilink")

## 体育

  - 职业冰球俱乐部：[HPK](../Page/HPK.md "wikilink")
  - 足球俱乐部：[海门林纳FC](../Page/海门林纳FC.md "wikilink")（FC Hämeenlinna）

## 参考文献

## 外部链接

  - [海门林纳](http://www.hameenlinna.fi) — 官方网站
  - [海梅城堡](http://www.nba.fi/en/hame_castle)

[Category:芬兰城市](../Category/芬兰城市.md "wikilink")
[Category:坎塔海梅区市镇](../Category/坎塔海梅区市镇.md "wikilink")

1.  [海门林纳市信息](http://www.hameenlinna.fi/keskushallinto/index.php?id=2308)
     *Taskutietoa Hämeenlinnasta*, 海门林纳市公布的数据（芬兰语）
2.  [海门林纳官方网站：友好城市](http://www.hameenlinna.fi/english/index.php?id=2747)