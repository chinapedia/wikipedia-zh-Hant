**海飞丝**（[英語](../Page/英語.md "wikilink")：**Head &
Shoulders**）是[宝洁公司](../Page/宝洁公司.md "wikilink")（Procter &
Gamble）的一款[洗髮精产品](../Page/洗髮精.md "wikilink")，属于中档洗髮水品牌。1985年進入[台灣](../Page/台灣.md "wikilink")，1988年进入[中国大陸](../Page/中国大陸.md "wikilink")，以“去[头皮屑](../Page/头皮屑.md "wikilink")”为其特点。

在-{zh-hans:台湾和港澳;zh-hant:中國大陸}-，-{zh-hans:海飞丝;zh-hant:海倫仙度絲}-被稱為-{zh-hans:海伦仙度丝;zh-hant:海飛絲}-；但台灣早年由[葡萄王企業推出過同名的洗髮精](../Page/葡萄王企業.md "wikilink")，亦以去頭皮屑為號召，兩者並無任何關係。

在中國大陸市場，海飞丝还有[护发素](../Page/护发素.md "wikilink")，2006年换了全新的[包装](../Page/包装.md "wikilink")，[护发素也全新](../Page/护发素.md "wikilink")[升级](../Page/升级.md "wikilink")。

## 主要成分

1.  [ZPT](../Page/吡硫鎓锌.md "wikilink")，是一种除[真菌的](../Page/真菌.md "wikilink")[化学成分](../Page/化学.md "wikilink")，所以海飞丝能够起到防止头皮屑的作用。
2.  MCI，比MI多了氯而已。化學結構與甲醛相似，故被當成防腐劑使用。
3.  MI，---------------。化學結構與甲醛相似，故被當成防腐劑使用。

## 代言人

  - [范冰冰](../Page/范冰冰.md "wikilink")
  - [宣萱](../Page/宣萱.md "wikilink")
  - [舒淇](../Page/舒淇.md "wikilink")
  - [倪妮](../Page/倪妮.md "wikilink")
  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [蔡依林](../Page/蔡依林.md "wikilink")
  - [姚晨](../Page/姚晨.md "wikilink")
  - [王菲](../Page/王菲.md "wikilink")
  - [徐若瑄](../Page/徐若瑄.md "wikilink")
  - [梁朝偉](../Page/梁朝偉.md "wikilink")
  - [官恩娜](../Page/官恩娜.md "wikilink")
  - [賈靜雯](../Page/賈靜雯.md "wikilink")
  - [歐鎧淳](../Page/歐鎧淳.md "wikilink")
  - [甄子丹](../Page/甄子丹.md "wikilink")
  - [汪詩詩](../Page/汪詩詩.md "wikilink")
  - [張艾嘉](../Page/張艾嘉.md "wikilink")
  - [張子蕾](../Page/張子蕾.md "wikilink")
  - [彭于晏](../Page/彭于晏.md "wikilink")
  - [王力宏](../Page/王力宏.md "wikilink")
  - [周迅](../Page/周迅.md "wikilink")
  - [鄭裕玲](../Page/鄭裕玲.md "wikilink")

## 外部链接

  - [台灣海倫仙度絲](https://www.livingartist.com.tw/tag/headandshoulders)
      -
  - [香港經典廣告 1997年 - Head & Shoulders
    (葉蒨文)](https://www.youtube.com/watch?v=v56Jb_PeqFo)

## 副作用

  - 可能導致皮膚[過敏](../Page/過敏.md "wikilink")。

## 各界反應

  - [英國皮膚科醫生協會調查指出](../Page/英國皮膚科醫生協會.md "wikilink")，有逾60%的洗髮產品含致敏防腐劑MI及MCI。
  - [香港的免疫及過敏病科專科醫生指](../Page/香港.md "wikilink")，近兩年在香港求診的病人中，有六成對MI或MCI敏感，「好多都係女士，塊面敏感，呢個情況五、六年前好少見」，患者應停用有關產品。
  - [香港醫院藥劑師學會呼籲](../Page/香港.md "wikilink")，濕疹或皮膚過敏患者不要使用含MI或MCI的產品；市民使用後如出現脫皮或紅腫等徵狀，也應即時停用。他指，濕疹及過敏是免疫系統疾病，若人體經常接觸致敏物質，有可能激發免疫系統反應，「原本沒有濕疹都會變成有濕疹」
  - 大眾熟悉的品牌如[草本精華](../Page/草本精華.md "wikilink")、海倫仙度絲、[潘婷](../Page/潘婷.md "wikilink")、[飄柔](../Page/飄柔.md "wikilink")、[沙宣等均含有遭世界權威醫學組織呼籲禁用的兩種防腐劑MI和MCI](../Page/沙宣.md "wikilink")/MI。

[Category:清洁用品品牌](../Category/清洁用品品牌.md "wikilink")
[-{zh-cn:宝洁;zh-tw:寶鹼;zh-hk:寶潔;}-公司品牌](../Category/寶潔公司品牌.md "wikilink")
[Category:1965年成立的公司](../Category/1965年成立的公司.md "wikilink")
[Category:1961年面世的產品](../Category/1961年面世的產品.md "wikilink")