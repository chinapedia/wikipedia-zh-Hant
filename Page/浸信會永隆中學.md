**浸信會永隆中學**（）位於[香港](../Page/香港.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")[大興花園第](../Page/大興花園.md "wikilink")2期內，1996年成立，校訓為「智仁忠信」，校監
王寧添先生，校長
鄭繼霖先生。學校類別屬於津貼男女中文中學，辦學團體為[香港浸信會聯會](../Page/香港浸信會聯會.md "wikilink")，宗教是[基督教](../Page/基督教.md "wikilink")。

## 歷史

1991年，香港浸信會聯會中等教育部開始向教育署申請開辦新中學。

1996年1月，[校董會獲教育署通知可以在同年秋天在](../Page/校董會.md "wikilink")[屯門](../Page/屯門.md "wikilink")[大興花園內開辦中學](../Page/大興花園.md "wikilink")，學校暫名「香港浸信會聯會中學」，同年秋天獲得[思源基金](../Page/思源基金.md "wikilink")（[恆隆集團轄下基金會](../Page/恆隆集團.md "wikilink")）及[伍宜孫慈善基金會](../Page/伍宜孫.md "wikilink")（伍為[永隆銀行創辦人](../Page/永隆銀行.md "wikilink")）慷慨捐助，合捐開辦費500萬港元正。

1997年1月，校董會經多方面徵詢意見後，決定以伍宜孫創立之永隆銀行之名，命名為「浸信會永隆中學」，並於同年3月6日舉行命名禮及開放日。

2000年10月26日，浸信會永隆中學在第一屆香港綠色學校獎比賽，中學組中勇奪冠軍，並獲得十五萬元獎金

2008年獲教育局、香港基督教服務處及香港輔導教師協會舉辦關愛校園獎勵計劃之「最關愛教職員團隊」主題大獎及「關愛校園」榮譽。

2010年榮獲香港城市大學「拉濶天空」全接觸獎勵計和諧校園比賽「最和諧校園」榮譽冠軍。

2012-2013年度本校榮獲行政長官卓越教學獎（訓育及輔導）。

2013-2014年度本校青年加FUN站「家點愛義工發展計劃」榮獲社會福利署頒發義務工作嘉許（小組）金狀。

2014-2015年度浸信會永隆中學義工小組再下一城，榮獲社會福利署頒發：

  - 『屯』聚印記嘉許狀
  - 最佳學生及青年義工計劃比賽——中學組冠軍
  - 義務工作嘉許狀（小組）金狀
  - 義務工作嘉許狀（個人）銀狀——共5人
  - 義務工作嘉許狀（個人）銅狀——共17人

## 學校的使命

  - 宣揚聖經真理（）
  - 提供全人教育（）
  - 鼓勵終身學習（）
  - 追求學術成就（）
  - 實踐可持續發展（）

## 校訓

**智 Wisdom**

  - 聰明、有見識、知識淵博
  - 「敬畏耶和華，是智慧的開端，認識至聖者，便是聰明。」（《箴言》9:10）
  - “The fear of the Lord is the beginning of wisdom, and knowledge of
    the Holy One is understanding.”（Proverbs 9:10）

**仁 Kindness**

  - 對人關心、同情、愛護、寬厚
  - 「當記念主耶穌的話，說：『施比受更為有福。』」（《使徒行傳》20:35）
  - “Remembering the words the Lord Jesus himself said: ‘It is more
    blessed to give than to receive.’ ”（Acts 20:35）

**忠 Loyalty**

  - 赤誠無私、盡心竭力
  - 「所求於管家的，是要他有忠心。」（《哥林多前書》4:2）
  - “Now it is required that those who have been given a trust must
    prove faithful.”（I Corinthians 4:2）

**信 Faith**

  - 誠實、守諾言、不懷疑
  - 「人非有信，就不能得神的喜悅。」（《希伯來書》11:6）
  - “Without faith it is impossible to please God.”（Hebrews 11:6）

## 新高中學制

共開五班，除4個必修科目外，學生可修讀3個或2個選修科目。

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>必修科目</dt>

</dl>
<ul>
<li>中國語文</li>
<li>英國語文</li>
<li>數學</li>
<li>通識教育</li>
</ul>
<dl>
<dt>選修科目（其他）</dt>

</dl>
<ul>
<li>數學延伸單元二</li>
<li>資訊與通訊科技</li>
<li>視覺藝術</li>
<li>體育</li>
<li>日文</li>
<li>法文</li>
<li>應用學習</li>
<li>宗教</li>
</ul></td>
<td><dl>
<dt>選修科目（文組）</dt>

</dl>
<ul>
<li>歷史</li>
<li>中國歷史</li>
<li>地理</li>
</ul>
<dl>
<dt>選修科目（理組）</dt>

</dl>
<ul>
<li>物理</li>
<li>化學</li>
<li>生物</li>
</ul>
<dl>
<dt>選修科目（商組）</dt>

</dl>
<ul>
<li>經濟</li>
<li>企業、會計與財務概論</li>
</ul></td>
</tr>
</tbody>
</table>

## 學校特色

## 交通

  - [輕鐵](../Page/輕鐵_\(香港\).md "wikilink")[蔡意橋站](../Page/蔡意橋站.md "wikilink"):
      - [輕鐵](../Page/輕鐵_\(香港\).md "wikilink")507 - 田景至屯門碼頭
      - [輕鐵](../Page/輕鐵_\(香港\).md "wikilink")751 - 友愛至天逸
  - [小巴](../Page/小巴.md "wikilink")
      - 45 - 大興花園至[屯門市中心](../Page/屯門市中心.md "wikilink")
      - 40 - 小坑村至友愛村（經建生站和屯門站）
      - 42 - 青磚圍至屯門市中心
      - 44A -
        [屯門站至](../Page/屯門站_\(西鐵綫\).md "wikilink")[上水站](../Page/上水站_\(香港\).md "wikilink")
      - 44B -
        [屯門碼頭至](../Page/屯門碼頭.md "wikilink")[落馬洲](../Page/落馬洲.md "wikilink")
  - [西鐵綫](../Page/西鐵綫.md "wikilink")
      - [屯門站](../Page/屯門站_\(西鐵綫\).md "wikilink")

## 著名校友

麥卓

## 資料來源

## 外部連結

1.  [香港浸信會聯會](http://www.hkbaptist.org.hk/)
2.  [浸信會永隆中學](http://www.bwlss.edu.hk/)
3.  [永興浸信會](http://www.ebc.org.hk/)
4.  [浸信會永隆中學校規音樂劇](https://www.youtube.com/watch?v=Vosx2quwL1E&feature=youtu.be/)

[B](../Category/屯門區中學.md "wikilink")
[Category:優質教育](../Category/優質教育.md "wikilink")
[Category:綠色中學](../Category/綠色中學.md "wikilink")
[Category:關愛校園](../Category/關愛校園.md "wikilink") [Category:大興
(香港)](../Category/大興_\(香港\).md "wikilink")
[Category:香港浸信會聯會學校](../Category/香港浸信會聯會學校.md "wikilink")