**消化作用**是指將[食物](../Page/食物.md "wikilink")(大分子)分解成足夠小的水溶性分子(小分子)，可以溶解在[血漿](../Page/血漿.md "wikilink")，讓身體能夠吸收利用的過程。有些生物體會透過[小腸吸收小分子](../Page/小腸.md "wikilink")，帶到血液系統中。消化作用是生物[异化作用](../Page/异化作用.md "wikilink")（分解代謝）的一環，可以分為兩個階段，首先藉由機械性的作用（機械消化，mechanical
digestion）將食物碎裂成小裂片，其次是化學性的作用（化學消化，chemical
digestion），經由[酶的催化](../Page/酶.md "wikilink")，將大分子水解成小分子單體。而無法消化的殘渣則會再排出體外。

大多數食物中所含的有機物包括[蛋白質](../Page/蛋白質.md "wikilink")、[脂肪和](../Page/脂肪.md "wikilink")[碳水化合物](../Page/碳水化合物.md "wikilink")。由於這些大分子聚合物無法穿過[細胞膜進入](../Page/細胞膜.md "wikilink")[細胞內](../Page/細胞.md "wikilink")，而且[動物需要用單體來合成自身身體所需的聚合物](../Page/動物.md "wikilink")，因此動物需要藉由消化作用將食物中的大分子分解成單體。例如將蛋白質分解為[胺基酸](../Page/胺基酸.md "wikilink")，[多醣及](../Page/多醣.md "wikilink")[雙醣分解為](../Page/雙醣.md "wikilink")[單醣](../Page/單醣.md "wikilink")，脂肪分解為[甘油及](../Page/甘油.md "wikilink")[脂肪酸等](../Page/脂肪酸.md "wikilink")。

## 消化系統

最簡單的生命體，例如[原生動物](../Page/原生動物.md "wikilink")，會利用[擴散](../Page/擴散.md "wikilink")、[主動運輸或](../Page/主動運輸.md "wikilink")[胞吞作用而將食物顆粒直接從周圍環境中送入細胞內](../Page/胞吞作用.md "wikilink")，再以酵素分解而獲取營養物，這樣的方式稱為。胞內消化不需要機械性裂解食物的過程，也不需要消化道或腔室，因此限制了此類動物的體型及複雜度，只能利用小的食物顆粒來獲取營養素。

較大型的動物又演化出的構造與機制。在[消化道中](../Page/消化道.md "wikilink")，利用機械性及化學性的作用，可將大塊食物分解成小分子的營養素，這些營養素被吸收後，參與代謝及合成作用。

原始的多細胞動物，例如[水螅](../Page/水螅纲.md "wikilink")，其腸道是封閉的囊狀物，只有一個開口作為入口及出口，稱為，是一種不完全消化道（incomplete
gut）。而自囊蠕蟲類動物起，例如[蠕蟲](../Page/医学寄生虫学#醫學蠕蟲.md "wikilink")、[蛔蟲](../Page/蛔蟲.md "wikilink")，開始發育出[肛門](../Page/肛門.md "wikilink")，具有[口](../Page/口.md "wikilink")、[咽](../Page/咽.md "wikilink")、肛門及完整腸道，是完全消化道（complete
gut）。完全消化道可使食物往單方向移動，不會與先前攝入的食物或廢物混合，並且能循序漸進的處理食物，使食物在不同的步驟中被有效率的消化。

### 分泌系統

[right機制的示意圖](../Page/image:Bacterial_Conjugation_en.png.md "wikilink")**1-**供體細胞產生[性菌毛](../Page/性菌毛.md "wikilink")**2-**性菌毛連上受體細胞，使兩細胞連在一起**3-**流動的質體被剪切後，一小段DNA被轉移到受體細胞**4-**兩個細胞重新將質體繞成圈，合成第二條鏈條，性菌毛再生。這時，兩個細胞都能提供質體了。\]\]

[細菌利用幾種不同的系統來獲得外界其他有機體的養份](../Page/細菌.md "wikilink")。

#### 通道传输系统

在通道传输系统中，由幾種蛋白質形成細菌細胞膜內部和外部之間的通道。通道传输系统包括三種蛋白質：、（MFP）及（OMP）。此分泌系統可以輸送各種不同的分子：從離子、藥物、到不同大小的蛋白質（20-900kDa）。所分泌的分子可以從[大腸桿菌的肽大肠杆菌素](../Page/大腸桿菌.md "wikilink")（10
kDa）到[螢光假單胞菌的细胞粘附蛋白](../Page/螢光假單胞菌.md "wikilink")（900 kDa）\[1\]。

### 分子注射器

有些細菌（像[沙門氏菌屬](../Page/沙門氏菌屬.md "wikilink")、[志賀氏菌屬等](../Page/志賀氏菌屬.md "wikilink")）可以透過分子注射器（molecular
syringe）注射養份或毒素到其他單細胞生物的細胞中。最早是在[鼠疫桿菌中發現此機制](../Page/鼠疫桿菌.md "wikilink")，而且證實可以直接將毒素注射到宿主的細胞質內，而不只是分泌到细胞间质中\[2\]。

#### 接合機制

有些細菌有[接合機制](../Page/接合.md "wikilink")，可以交換DNA及蛋白質。此機制是在[農桿菌中發現](../Page/農桿菌.md "wikilink")，利用此一機制引入Ti質粒到宿主的蛋白質中，因而引發冠纓\[3\]。

#### 外膜囊泡释放

除了利用上述的多蛋白質複合物外，革蘭氏陰性菌還有另一種釋放物質的方法：形成外膜囊泡\[4\]。

### 消化腔

[Venus_Flytrap_showing_trigger_hairs.jpg](https://zh.wikipedia.org/wiki/File:Venus_Flytrap_showing_trigger_hairs.jpg "fig:Venus_Flytrap_showing_trigger_hairs.jpg")

的作用類似胃，一方面進行消化作用，另一方面也將營養分佈到身體的各部份。細胞外消化就是發生在中央消化腔外，其內裡是消化內皮層（gastrodermis），是[上皮組織的內層](../Page/上皮組織.md "wikilink")，消化腔對外只有一個開口，具有攝取食物及排泄的功能，消化後的殘餘物及未消化物質從這個出口排到體外，這可以稱為是不完全的。

像[捕蠅草之類可以用光合作用來產生食物的植物](../Page/捕蠅草.md "wikilink")。其捕捉獵物並且消化的原因不像一般動物為了採集能量及碳元素，而是為了攝取必要的營養素（特別是氮和磷），這些營養素在其酸性沼澤棲息地是很不容易取得的\[5\]。

[Trophozoites_of_Entamoeba_histolytica_with_ingested_erythrocytes.JPG](https://zh.wikipedia.org/wiki/File:Trophozoites_of_Entamoeba_histolytica_with_ingested_erythrocytes.JPG "fig:Trophozoites_of_Entamoeba_histolytica_with_ingested_erythrocytes.JPG")

### 吞噬体

[吞噬体是因為](../Page/吞噬体.md "wikilink")[吞噬作用吸收的物質](../Page/吞噬作用.md "wikilink")，其周圍形成的[液胞](../Page/液胞.md "wikilink")。吞噬体是因為物質附近的[细胞膜融合後所產生的](../Page/细胞膜.md "wikilink")。吞噬体也是[细胞区室](../Page/细胞区室.md "wikilink")，[致病微生物會在吞噬体內被殺死及消化](../Page/致病.md "wikilink")。吞噬体在其成長過程會和[溶酶體融合](../Page/溶酶體.md "wikilink")，最後形成。人類體內的[溶组织内阿米巴會吞噬](../Page/溶组织内阿米巴.md "wikilink")[红血球](../Page/红血球.md "wikilink")\[6\]。

## 脊椎动物消化作用的簡介

對大部份的[脊椎动物而言](../Page/脊椎动物.md "wikilink")，消化是在消化系統中多步驟的作用，從攝取食物（多半是其他的動植物）開始。消化作用也會包括一些物理程序及化學程序，可以分為以下四個步驟：

1.  ：將食物放入口中（食物進入消化系統的起點）。

2.  物理性及化學性的分解：口中咀嚼是物理性的分解，之後在胃腸中和水、[胃酸](../Page/胃酸.md "wikilink")、[胆汁和](../Page/胆汁.md "wikilink")[酵素混合](../Page/酵素.md "wikilink")，將大型的分子分解為較簡單的結構。

3.  吸收：營養素從消化系統藉由[渗透](../Page/渗透.md "wikilink")、[主動運輸及](../Page/主動運輸.md "wikilink")[扩散作用吸收到循环和淋巴毛细血管中](../Page/扩散作用.md "wikilink")。

4.  排泄：最後無法消化的殘餘物質會透過[排便離開消化道](../Page/排便.md "wikilink")。

讓消化作用正常作用的關鍵是消化系統中肌肉的動作，包括吞咽和運動。消化作用中的每一個步驟都需要能量。因此在從吸收的物質中取得能量之前，需要先消耗一些能量。消化作用需要能量的差異對於動物的生活方式、行為，甚至其外形都有很大的影響。像人類都和其他的[人科動物有很大的不同](../Page/人科.md "wikilink")（例如缺乏體毛、较小的颌骨和肌肉组织、齒列不同、腸子長度不同，是否會烹煮食物等）。

消化作用主要是發生在[小腸中](../Page/小腸.md "wikilink")，[大腸主要的作用是利用](../Page/大腸.md "wikilink")讓無法消化的物質發酵，並且在排泄之前吸收殘餘物質中的水份。

### 哺乳類的消化作用

[哺乳類的消化準備工作是從](../Page/哺乳類.md "wikilink")開始，[口腔會分泌](../Page/口腔.md "wikilink")[唾液](../Page/唾液.md "wikilink")，[胃部也會分泌](../Page/胃部.md "wikilink")[消化酶](../Page/消化酶.md "wikilink")。食物入口後，哺乳類會食物，並且食物和唾液混合，此時開始了物理性及化學性的分解，之後食物進入胃部，繼續其他的酵素作用。胃部也會繼續進行物理性及化學性的分解，靠的是胃部的搅拌食物，以及讓食物与胃酸和酵素混合。養份吸收是在胃部([胃酸](../Page/胃酸.md "wikilink"))以及腸道([腸液](../Page/腸液.md "wikilink")、[胰液](../Page/胰液.md "wikilink")、[膽汁](../Page/膽汁.md "wikilink"))，最後殘餘物質會透過[排便離開消化道](../Page/排便.md "wikilink")(排遺作用)\[7\]。

## 不同物質的消化

### 蛋白質的消化

蛋白質是在胃部及十二指腸進行消化，有三種主要的酵素：由胃部分泌的[胃蛋白酶](../Page/胃蛋白酶.md "wikilink")，以及胰臟分泌的[胰蛋白酶及](../Page/胰蛋白酶.md "wikilink")[胰凝乳蛋白酶](../Page/胰凝乳蛋白酶.md "wikilink")，可以將食物中的蛋白質分解為[多肽](../Page/多肽.md "wikilink")，之後再由[外肽酶及](../Page/外肽酶.md "wikilink")分成胺基酸。不過胃和胰臟多半不會直接分泌消化酵素，而是分泌無活性的前体[酶原](../Page/酶原.md "wikilink")。例如胰蛋白酶是以的形式，由胰腺所分泌，再經由十二指腸的活化成為胰蛋白酶。胰蛋白酶可以將[蛋白质分解為較小的多肽](../Page/蛋白质.md "wikilink")。

### 脂質的消化

有些脂質是從進入口腔起就開始消化，會將一些短鏈的脂質轉換為[甘油二酯](../Page/甘油二酯.md "wikilink")。不過大部份的脂質是在小腸消化的\[8\]。小腸中脂肪的出現會產生激素，會讓胰腺釋放[胰脂肪酶](../Page/胰脂肪酶.md "wikilink")，讓肝臟釋放[胆汁酸](../Page/胆汁酸.md "wikilink")，有助於脂肪的乳化，可以以[脂肪酸的形式吸收](../Page/脂肪酸.md "wikilink")\[9\]。一莫耳脂肪（[三酸甘油酯](../Page/三酸甘油酯.md "wikilink")）在完全消化後會變成脂肪酸、甘油一酯、甘油二酯以及一些未分解三酸甘油酯的混合物，但其中不會有游離的[甘油分子](../Page/甘油.md "wikilink")\[10\]。

### 醣類的消化

人類可以消化的膳食澱粉是由[葡萄糖單位組成的長鏈](../Page/葡萄糖.md "wikilink")，稱為[直鏈澱粉](../Page/直鏈澱粉.md "wikilink")\]，屬於[多糖](../Page/多糖.md "wikilink")。在消化時，唾液中和胰臟分泌的[淀粉酶會破壞葡萄糖分子之間的鏈結](../Page/淀粉酶.md "wikilink")，因此葡萄糖的長鏈會變短，產物會是結構較簡單，可以被小腸吸收的葡萄糖及[麥芽糖](../Page/麥芽糖.md "wikilink")（二個葡萄糖組成的[雙醣](../Page/雙醣.md "wikilink")）。

[乳糖酶可以將](../Page/乳糖酶.md "wikilink")[乳糖分解為葡萄糖及](../Page/乳糖.md "wikilink")[半乳糖](../Page/半乳糖.md "wikilink")。小腸可以吸收葡萄糖及半乳糖。大約有65%的成年人分泌的乳糖酶不足，因此無法消化未[发酵的乳製品](../Page/发酵.md "wikilink")，這稱為[乳糖不耐症](../Page/乳糖不耐症.md "wikilink")。乳糖不耐症隨著族群而不同，東亞血統人群超過90%有乳糖不耐症，而北歐後裔只有約5%有乳糖不耐症\[11\]。

[蔗糖酶是分解](../Page/蔗糖酶.md "wikilink")[蔗糖的酶](../Page/蔗糖.md "wikilink")，消化後會得到[果糖及葡萄糖](../Page/果糖.md "wikilink")，可以由小腸持續的吸收。

### DNA和RNA的消化

[DNA和](../Page/脱氧核糖核酸.md "wikilink")[RNA會由胰腺分泌的](../Page/核糖核酸.md "wikilink")[去氧核糖核酸酶](../Page/去氧核糖核酸酶.md "wikilink")（DNase）及[核糖核酸酶](../Page/核糖核酸酶.md "wikilink")（RNase）等[核酸酶分解為](../Page/核酸酶.md "wikilink")[核苷酸](../Page/核苷酸.md "wikilink")。

## 非破壞性消化

有些營養素是複雜結構的分子（例如[维生素B<sub>12</sub>](../Page/维生素B12.md "wikilink")），若分解其分子就會破壞營養素的功能。為了可以以非破壞性的方式消化维生素B<sub>12</sub>，[唾液中的](../Page/唾液.md "wikilink")會和维生素B<sub>12</sub>形成強力的鍵結，當维生素B<sub>12</sub>進入胃部，haptocorrin可以保護B<sub>12</sub>不會被胃酸分解\[12\]。

维生素B<sub>12</sub>-haptocorrin複合物會從胃部通過幽門，進入十二指腸，胰腺蛋白酶會將haptocorrin和维生素B<sub>12</sub>分離，而维生素B<sub>12</sub>會和[內在因子](../Page/內在因子.md "wikilink")（IF）結合。维生素B<sub>12</sub>-內在因子複合物會再行進到小腸[迴腸段](../Page/迴腸.md "wikilink")，而接受器會進行[同化作用](../Page/同化_\(生物學\).md "wikilink")，使维生素B<sub>12</sub>-內在因子複合物進入血液系統\[13\]。

## 參見

  - [消化系統](../Page/消化系統.md "wikilink")

  - [消化道](../Page/消化道.md "wikilink")

  - [营养学](../Page/营养学.md "wikilink")

  -
  - [胃食管反流病](../Page/胃食管反流病.md "wikilink")

  - [氫離子幫浦阻斷劑](../Page/氫離子幫浦阻斷劑.md "wikilink")

## 參考資料

[Category:消化系统](../Category/消化系统.md "wikilink")

1.

2.  Salyers, A. A. & Whitt, D. D. (2002). *Bacterial Pathogenesis: A
    Molecular Approach*, 2nd ed., Washington, D.C.: ASM Press. ISBN
    978-1-55581-171-6

3.

4.  Chatterjee, SN and J Das. "Electron microscopic observations on the
    excretion of cell wall material by *Vibrio cholerae*."
    "J.Gen.Microbiol." "49" : 1-11 (1967) ; Kuehn, MJ and NC Kesty.
    "Bacterial outer membrane vesicles and the host-pathogen
    interaction." *Genes Dev*.and then the **19**(22):2645-55 (2005)

5.

6.

7.

8.  [Digestion of fats
    (triacylglycerols)](http://pharmaxchange.info/press/2013/10/digestion-of-fats-triacylglycerols/)

9.
10.
11.

12.

13.