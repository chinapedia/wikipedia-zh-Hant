**消化系统**（）是[多細胞生物用以](../Page/多細胞生物.md "wikilink")[進食](../Page/進食.md "wikilink")、[消化食物](../Page/消化.md "wikilink")、獲取能量和[營養](../Page/營養.md "wikilink")、排遺剩餘废物的一组[器官](../Page/器官.md "wikilink")，其主要功能為[攝食](../Page/攝食.md "wikilink")、[消化](../Page/消化.md "wikilink")、[吸收](../Page/吸收.md "wikilink")、[同化和](../Page/同化_\(生物学\).md "wikilink")[排遺](../Page/排遺.md "wikilink")。其中有關[排遺的部分](../Page/排遺.md "wikilink")，也可歸類到的一部分\[1\]。

## 構造

消化系統的構造差異可以大略分為[囊狀消化系統與](../Page/囊狀消化系統.md "wikilink")[管狀消化系統](../Page/管狀消化系統.md "wikilink")。囊狀消化系統具有囊狀的消化腔，通常如[水螅](../Page/水螅.md "wikilink")、[水母](../Page/水母.md "wikilink")、[海葵等](../Page/海葵.md "wikilink")[水生動物](../Page/水生動物.md "wikilink")，它們大都使用[消化腔開口的觸手補食獵物](../Page/消化腔.md "wikilink")，而消化完之殘渣亦由同一開口排出。管狀消化系統則具備[消化道](../Page/消化道.md "wikilink")，有口腔及[肛門兩個口呈現一管狀構造](../Page/肛门.md "wikilink")。[脊椎動物為此類](../Page/脊椎動物.md "wikilink")。

所有的[哺乳動物都有一個由口腔經](../Page/哺乳動物.md "wikilink")[食道](../Page/食道.md "wikilink")、[胃](../Page/胃.md "wikilink")、[腸等的消化系統](../Page/腸.md "wikilink")，可將食物分解為身體可利用的簡單物質。食物進入嘴巴之後，就會開始進行消化過程。食物從胃被送入小腸後，各種各樣的[酶就開始消化](../Page/酶.md "wikilink")[碳水化合物也就是](../Page/碳水化合物.md "wikilink")[醣類](../Page/醣類.md "wikilink")、[蛋白質和](../Page/蛋白質.md "wikilink")[脂肪](../Page/脂肪.md "wikilink")。消化的食物養分透過腸壁被吸收，未被消化的物質則直接被排出體外。消化過程是動物從食物中獲取能量的第一步。\[2\]

某些動物只能在含有適當營養素的環境中生存，像[蚯蚓就是食用沉積物的動物](../Page/蚯蚓.md "wikilink")，他們在土中打洞並順便吃掉前面土壤。它們的消化系統會分解掉土壤中的有機物，並將廢物排出。
很多昆蟲完全靠吸食液體生存，通常會生有特殊的管狀口器以便於進食，牠們的口器通常有尖銳的尖端，像是[蝴蝶就有長長的虹吸式口器](../Page/蝴蝶.md "wikilink")，用以刺入植物的莖或動物的皮膚，如吸血的[蚊子](../Page/蚊子.md "wikilink")、吸取植物汁液的[蚜蟲和](../Page/蚜蟲.md "wikilink")[蟬](../Page/蟬.md "wikilink")。\[3\]

## 消化過程

動物必須藉由攝食食物透過[消化過程吸取需要的養分](../Page/消化.md "wikilink")，提供給動物的生理與生長發育過程提供能量。

哺乳動物吞下食物到胃裡後，胃裡的一種強酸性物質會將食物部分分解，然後小腸和大腸吸收所有已消化的食物和水分。[胰腺製造中和胃酸的物質](../Page/胰腺.md "wikilink")。但是植物性食物會在[盲腸中消化](../Page/盲腸.md "wikilink")。\[4\]

草食性動物的消化系統較為特別，尤其是[反芻動物通常消化系統會分為好幾部分](../Page/反芻動物.md "wikilink")，有助於其消化草、樹葉等不易消化的植物，所以食物嚥下後會先進[瘤胃的消化室內](../Page/瘤胃.md "wikilink")，讓其中的微生物幫助分解。植物慢慢分解後，再將食物吐出到口中細細咀嚼，這就叫[反芻](../Page/反芻.md "wikilink")。反芻後的食物再依次通過其他各胃室，如[網胃](../Page/網胃.md "wikilink")、[瓣胃和](../Page/瓣胃.md "wikilink")[皺胃](../Page/皺胃.md "wikilink")。\[5\]

草食性動物只能吃植物，肉食動物只能吃肉類，而雜食性動物可以吃多樣的食物。

某些動物的消化是在體外進行的，其中[蜘蛛是典型的進行](../Page/蜘蛛.md "wikilink")[體外消化的動物](../Page/體外消化.md "wikilink")。它們抓到一隻昆蟲之後，會向昆蟲注入一種含[酶的液體](../Page/酶.md "wikilink")，等酶分解了昆蟲身體，蜘蛛再去吸取昆蟲的體內營養汁液，這就是體外消化。\[6\]

## 动物的消化系统

[Idisslarmage.png](https://zh.wikipedia.org/wiki/File:Idisslarmage.png "fig:Idisslarmage.png")
動物的消化系统由構造的差異可粗分為[胞內消化和](../Page/胞內消化.md "wikilink")[囊狀消化系統還有](../Page/囊狀消化系統.md "wikilink")[管狀消化系統](../Page/管狀消化系統.md "wikilink")。

胞內消化在細胞內進行，且用偽足或細胞膜凹陷形成食泡，在與溶小體結合，利用水解酶進行分解，如[海綿等單細胞生物](../Page/海綿.md "wikilink")。

囊狀消化系統具有囊狀的消化腔，通常如[水螅](../Page/水螅.md "wikilink")、[水母](../Page/水母.md "wikilink")、[海葵等水生生物](../Page/海葵.md "wikilink")，它們大都使用消化腔開口的觸手捕食獵物，而消化完之殘渣亦由同一開口排出。

管狀消化系統則擁有消化道，有口腔及肛門兩個口呈現一管狀構造。通常如[蚯蚓](../Page/蚯蚓.md "wikilink")、[螳螂](../Page/螳螂.md "wikilink")、[鳥類和](../Page/鳥類.md "wikilink")[哺乳類皆為此類](../Page/哺乳類.md "wikilink")。

## 人类的消化系统

[人体的消化系统主要由](../Page/人体.md "wikilink")[消化道和](../Page/消化道.md "wikilink")[消化腺组成](../Page/消化腺.md "wikilink")。一個正常男性成人的消化道大約長6.5米，由上消化道和下消化道組成。消化道是一條連接口腔和肛門的管道，由許多負責處理食物的構造組成。消化腺能分泌消化液以消化食物。

人類的上消化道由[口](../Page/口.md "wikilink")，[咽](../Page/咽.md "wikilink")，[食道和](../Page/食道.md "wikilink")[胃組成](../Page/胃.md "wikilink")。\[7\]口包含[口腔黏膜](../Page/口腔黏膜.md "wikilink")，[唾液腺](../Page/唾液腺.md "wikilink")，舌頭和牙齒。在口後面是[咽](../Page/咽.md "wikilink")，[咽連接着一條由肌肉組成的中空管道](../Page/咽.md "wikilink")，即[食道](../Page/食道.md "wikilink")。食道通过肌肉的收縮和放鬆，把食物向下推，穿過橫膈膜到達胃。\[8\]

下消化道包括腸和肛門。腸是[消化系統中](../Page/消化系統.md "wikilink")，由[胃至](../Page/胃.md "wikilink")[肛門之間的消化管道](../Page/肛門.md "wikilink")，為大部份[化學](../Page/化學.md "wikilink")[消化過程的所在地](../Page/消化.md "wikilink")，將[食物的](../Page/食物.md "wikilink")[營養吸收](../Page/營養.md "wikilink")。
[小腸有](../Page/小腸.md "wikilink")及绒毛，與微絨毛。可以增加腸道的表面積。絨毛內包含乳糜管及微血管，乳糜管吸收脂溶性養分，如：甘油、脂肪酸、維生素ADEK等。微血管吸收水溶性養分，如：單醣、胺基酸、維生素BC等。[空腸可吸收像](../Page/空腸.md "wikilink")[醣](../Page/醣.md "wikilink")、[胺基酸及](../Page/胺基酸.md "wikilink")[脂肪酸等的養分](../Page/脂肪酸.md "wikilink")。[迴腸有](../Page/迴腸.md "wikilink")[肠绒毛可以吸收](../Page/肠绒毛.md "wikilink")[维生素B12及](../Page/维生素B12.md "wikilink")[膽汁酸](../Page/膽汁酸.md "wikilink")，也可以吸收其他養分。
[大腸有](../Page/大腸.md "wikilink")[盲腸](../Page/盲腸.md "wikilink")，連接着[闌尾](../Page/闌尾.md "wikilink")。
[結腸](../Page/結腸.md "wikilink")，包括[升結腸](../Page/升結腸.md "wikilink")、[橫結腸](../Page/橫結腸.md "wikilink")、[降結腸和](../Page/降結腸.md "wikilink")[乙狀結腸](../Page/乙狀結腸.md "wikilink")，結腸的作用是吸收水分，但其中也有一些可以生成[維生素K的細菌](../Page/維生素K.md "wikilink")。[直肠](../Page/直肠.md "wikilink")，是人的消化系统的一部分，它是[肠的最后一部分](../Page/肠.md "wikilink")，位于[肛门的前面](../Page/肛门.md "wikilink")，其作用是积累[粪便](../Page/粪便.md "wikilink")。当直肠中的粪便积累到一定程度后就会向[大脑通知这个状态](../Page/大脑.md "wikilink")，以便[排便](../Page/排便.md "wikilink")。最後由肛門排出糞便。

人類[消化腺又分为小消化腺和大消化腺两种](../Page/消化腺.md "wikilink")。
小消化腺是散在于消化管各部的管壁内的小腺体。这类腺体数量甚多，如[胃腺](../Page/胃腺.md "wikilink")、[肠腺等](../Page/肠腺.md "wikilink")：
大消化腺位于[消化道外](../Page/消化道.md "wikilink")，它们主要通过导管将分泌物排入消化道内。大消化腺主要有：三对[唾液腺](../Page/唾液腺.md "wikilink")（[腮腺](../Page/腮腺.md "wikilink")、[下颌下腺](../Page/下颌下腺.md "wikilink")、[舌下腺](../Page/舌下腺.md "wikilink")）、[肝脏和](../Page/肝脏.md "wikilink")[胰脏](../Page/胰脏.md "wikilink")

食物通過消化道需要的時間會隨許多因素而不同，吃飯後約需要一個小時的時間，胃部才會有一半排空，要二個小時胃部才會全部排空，讓食物進入小腸。小腸一半排空也需要一至二個小時，食物到達結腸約需12至50小時，會隨個別情形有很大的差異\[9\]\[10\]。

### 組織

[腸壁正常組織
gland：腺
Muscularis：肌層
submucosa:黏膜下層
circular muscle：環形肌
longitudinal muscle：縱肌
serosa：漿膜
peritoneum：腹膜
mucosa：黏膜
villi：絨毛
tubular gland：管狀腺
mesentery：腸系膜](https://zh.wikipedia.org/wiki/File:Gut_wall.svg "fig:腸壁正常組織 gland：腺 Muscularis：肌層 submucosa:黏膜下層 circular muscle：環形肌 longitudinal muscle：縱肌 serosa：漿膜 peritoneum：腹膜 mucosa：黏膜 villi：絨毛 tubular gland：管狀腺 mesentery：腸系膜")
消化道可以分為四個部份：

  - [黏膜](../Page/黏膜.md "wikilink")

  - [黏膜下層](../Page/黏膜下層.md "wikilink")

  -
  - 或[漿膜](../Page/漿膜.md "wikilink")

<!-- end list -->

  - 黏膜

[黏膜是消化道的最內層](../Page/黏膜.md "wikilink")，包圍管腔內，或在管內的開放空間。此層直接接觸消化的食物（[食糜](../Page/食糜.md "wikilink")）。

黏膜分為三層：

  - 上皮 - 最內層。
  - 固有層 - 一層結締組織。
  - 粘膜肌層 - 一層薄的平滑肌。

<!-- end list -->

  - 黏膜下層

黏膜下層包括邁斯納神經叢（一種[腸神經系統](../Page/腸神經系統.md "wikilink")），位於肌層的內表面上。

  - 肌層

肌層包括一個內側環形層和一個縱向外肌層。內側環形層防止食物向後移動，而縱向外肌層縮短消化道。

  - 外膜或漿膜

在胃腸道的最外層，包括數層的[結締組織](../Page/結締組織.md "wikilink")。

胃腸道的腹腔部分覆蓋漿膜層。漿膜層包含大部分的[胃](../Page/胃.md "wikilink")，[十二指腸的第一部分](../Page/十二指腸.md "wikilink")，以及所有的[小腸](../Page/小腸.md "wikilink")、[盲腸和](../Page/盲腸.md "wikilink")[闌尾](../Page/闌尾.md "wikilink")、[橫結腸](../Page/橫結腸.md "wikilink")、[乙狀結腸](../Page/乙狀結腸.md "wikilink")，和[直腸](../Page/直腸.md "wikilink")。

## 臨床意義

### 相關疾病

有些疾病和症狀會影響消化系统，包括：

  - [感染](../Page/感染.md "wikilink")，[腸胃炎是胃或小腸的發炎](../Page/腸胃炎.md "wikilink")，是發生率最高的小腸疾病。
  - [癌症可能出現在消化道或的任何一部份](../Page/癌症.md "wikilink")，例如[口腔癌](../Page/口腔癌.md "wikilink")、[食道癌](../Page/食道癌.md "wikilink")、[胃癌及](../Page/胃癌.md "wikilink")[大腸癌](../Page/大腸癌.md "wikilink")。
  - 發炎，例如及[結腸炎等](../Page/結腸炎.md "wikilink")。
  - [闌尾炎若沒有治療](../Page/闌尾炎.md "wikilink")，是潛在的致命疾病，大部份的闌尾炎都需要手術治療。

是工業化國家中老人常見的症狀，一般會發生在大腸，但也可能發生在小腸。

[炎症性腸病是腸壁發炎的症狀](../Page/炎症性腸病.md "wikilink")，也包括[克隆氏症及](../Page/克隆氏症.md "wikilink")[潰瘍性結腸炎](../Page/潰瘍性結腸炎.md "wikilink")。克隆氏症會影響整個消化道，潰瘍性結腸炎則只在大腸。克隆氏症一般認為是[自體免疫性疾病](../Page/自體免疫性疾病.md "wikilink")，雖然潰瘍性結腸炎的處理也類似自體免疫性疾病，但目前還不清楚潰瘍性結腸炎是否是自體免疫性疾病。

### 影像

以下是一些有關消化系统的影像檢查方式。

  - 可以吞嚥的染料（例如[鋇餐](../Page/鋇餐.md "wikilink")）阻擋X射線穿過，制造影像。

  - 一些部位可以用配合攝影機或照像機觀察內部情形，例如[內視鏡可以觀察上消化道](../Page/內視鏡.md "wikilink")，或可以觀察下消化道，在檢查時也可以進行[活組織檢查需要的切片](../Page/活組織檢查.md "wikilink")。是將照像機裝在胶囊中。

  - 也可以用來檢查下消化道。

### 症狀

有些症狀和消化系统的疾病有關：

  - [嘔吐](../Page/嘔吐.md "wikilink")，包括吐出食物或是[嘔血](../Page/嘔血.md "wikilink")。
  - [腹瀉](../Page/腹瀉.md "wikilink")，[糞便性狀改變](../Page/糞便.md "wikilink")，糞質稀薄，或帶有液體。
  - [便秘](../Page/便秘.md "wikilink")，糞便量變少且變硬，不易排出。
  - [便血](../Page/便血.md "wikilink")，糞便中帶有鮮紅色或黑色的血液。

### 其他

  - [乳糜瀉](../Page/乳糜瀉.md "wikilink")

  - [霍亂](../Page/霍亂.md "wikilink")

  - [腹瀉](../Page/腹瀉.md "wikilink")

  -
  - [賈第蟲病](../Page/賈第蟲病.md "wikilink")

  - [大腸激躁症](../Page/大腸激躁症.md "wikilink")

  - [胰腺炎](../Page/胰腺炎.md "wikilink")

  - [胃及十二指腸潰瘍](../Page/胃及十二指腸潰瘍.md "wikilink")

  - [黃熱病](../Page/黃熱病.md "wikilink")

  - [幽門螺桿菌](../Page/幽門螺桿菌.md "wikilink")

  -
  -
  - [腸道病毒](../Page/腸道病毒.md "wikilink")

  - [子宮內膜異位症](../Page/子宮內膜異位症.md "wikilink")

  - [肠扭转](../Page/肠扭转.md "wikilink")

  - [腸道血管發育不良](../Page/腸道血管發育不良.md "wikilink")

  -
  - [便秘](../Page/便秘.md "wikilink")

  -
  - [腸套疊](../Page/腸套疊.md "wikilink")

  - [息肉](../Page/息肉.md "wikilink")

  - [偽膜性結腸炎](../Page/偽膜性結腸炎.md "wikilink")

  - [潰瘍性結腸炎](../Page/潰瘍性結腸炎.md "wikilink")

  -
## 免疫功能

胃腸道是免疫系統重要的部份。\[11\]消化道的表面積，大約是一個足球場的表面面積\[12\]，由於表面積大，因此需要免疫系統的工作才能防止病原體進入血液及淋巴\[13\]<sup>\[WP:V\]</sup>。

胃內部低[pH](../Page/pH.md "wikilink")（約為1到4之間）的環境可以殺死許多[微生物](../Page/微生物.md "wikilink")。而包括[免疫球蛋白A在內的](../Page/免疫球蛋白A.md "wikilink")[黏液也可以消滅許多微生物](../Page/黏液.md "wikilink")。消化道中也有其他成份對免疫系統有幫助，例如[唾液或是](../Page/唾液.md "wikilink")[膽汁中的](../Page/膽汁.md "wikilink")[酵素](../Page/酵素.md "wikilink")。像Cyp3A4之類的酵素有逆向轉運蛋白活性，可以對[抗原或](../Page/抗原.md "wikilink")有解毒作用。

對健康有益的也可以避免可能有害的細菌在腸道中生長。種細菌在腸道中為有限的空間及食物而競爭，腸道一般會有有80-85%的益菌，其餘15-20%是可能有害的細菌。也可以保持微生物的平衡。

### 免疫系統穩態

益菌也可以建立消化系统的穩態，例如腸道和之間的關係\[14\]。食用高纖的食物可以誘導[调节T细胞的分化](../Page/调节T细胞.md "wikilink")，這是因為[丁酸鹽和](../Page/丁酸鹽.md "wikilink")[丙酸鹽在發酵過程中產生的短鏈脂肪酸](../Page/丙酸鹽.md "wikilink")。丁酸鹽可以誘導T细胞的分化，方式是透過增強啟動子中組蛋白H3的乙酰化，以及保持Foxp3基因的非編碼序列區域，因此可以調節T細胞，少其發炎及過敏反應。

### 大腸菌群

大腸中有許多細菌處理人類無法分解的分子\[15\]，例如[纖維素](../Page/纖維素.md "wikilink")，這是一種[共生的例子](../Page/共生.md "wikilink")，這些細菌也會產生氣體，在放屁時排出。

## 參考文獻

## 外部連結

  - [生理健康网](https://web.archive.org/web/20050204124647/http://www.zgxl.net/sljk/imgbody/xhxt.htm)

  - [消化系統的保健](https://web.archive.org/web/20130530174808/http://www.zionpark.com.tw/health/B4.htm)

  - [Pediatric Overview of Human Digestive
    System](http://www.pediatricfeeding.org/gi_anatomy.html)

  - [Anatomy atlas of the Digestive
    System](http://www.innerbody.com/image/digeov.html)

  - [Overview](http://www.vivo.colostate.edu/hbooks/pathphys/digestion/)
    at [Colorado State
    University](../Page/Colorado_State_University.md "wikilink")

  - [Your Digestive System and How It Works at National Institutes of
    Health](http://digestive.niddk.nih.gov/ddiseases/pubs/yrdd/)

{{-}}

[Digestive](../Category/器官系统.md "wikilink")
[消化系统](../Category/消化系统.md "wikilink")
[Category:腹部](../Category/腹部.md "wikilink")
[Category:给药途径](../Category/给药途径.md "wikilink")

1.  [在養生之道裡，晚飯不吃，餓治百病](http://blog.ltn.com.tw/kang1021/2013/11/20/151647)

2.
3.
4.
5.
6.

7.

8.

9.  Kim SK. Small intestine transit time in the normal small bowel
    study. American Journal of Roentgenology 1968; 104(3):522-524.

10. Uday C Ghoshal, Vikas Sengar, and Deepakshi Srivastava. [Colonic
    Transit Study Technique and Interpretation: Can These Be Uniform
    Globally in Different Populations With Non-uniform Colon Transit
    Time?](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3325313) J
    Neurogastroenterol Motil. 2012 April; 18(2): 227–228.

11.

12.

13. Animal Physiology textbook

14.

15. Judson Knight. *[Science of everyday things: Real-life earth
    science. Vol. 4](http://books.google.com/books?id=G0sCAAAACAAJ)*.
    Gale Group; 2002. ISBN 978-0-7876-5634-8.