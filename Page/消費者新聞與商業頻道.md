**消费者新闻与商业频道**（，[縮寫](../Page/縮寫.md "wikilink")：），是[美国一家通过](../Page/美国.md "wikilink")[有线电视](../Page/有线电视.md "wikilink")、[卫星电视和](../Page/卫星电视.md "wikilink")[互联网覆盖](../Page/互联网.md "wikilink")[美加地区的](../Page/美加地区.md "wikilink")，隶属于[NBC环球集团下的](../Page/NBC环球集团.md "wikilink")，同时也是[康卡斯特集团的](../Page/康卡斯特.md "wikilink")[孙公司](../Page/子公司.md "wikilink")。CNBC的总部位于美国[新泽西州的](../Page/新泽西州.md "wikilink")森林大道（）900号。\[1\]
CNBC在交易日主要播出美国及国际市场的交易资讯及财经新闻；而在交易日交易结束后或非交易日时段，CNBC主要播出以财经和商业为主题的[纪录片或](../Page/纪录片.md "wikilink")[真人秀](../Page/真人秀.md "wikilink")。CNBC前身是由[全国广播公司和](../Page/全国广播公司.md "wikilink")合资组建的“消费者新闻和财经频道（）”。\[2\]
随后在1991年，该频道收购了其主要竞争对手“财经新闻电视网（）”，这一举措扩大了CNBC的分销渠道和员工队伍。后来，美国有线电视公司将其持有的股份出售给全国广播公司，使CNBC成为全国广播公司的全资子公司。截止到2015年2月，CNBC在美国拥有93,623,000个收费家庭用户，占美国收视家庭总数的80.4%。\[3\]
在2007年，CNBC获评美国最有价值有线电视频道的第19名，并被估价40亿美元。\[4\]

CNBC除了美国版之外，还拥有其他许多，以服务全球各个国家和地区。NBC环球集团是这些频道的所有者或少数股东。\[5\]\[6\]

## 频道历史

[111807f.jpg](https://zh.wikipedia.org/wiki/File:111807f.jpg "fig:111807f.jpg")

CNBC最初是旗下一个播出老电影、教学节目和娱乐节目的低成本综合频道。该频道后来一度改名叫“节拍电视频道（）”。在签署收购节拍电视频道的意向书之后，\[7\]
[全国广播公司在](../Page/全国广播公司.md "wikilink")1988年6月签订了租用电视转发器的协议。\[8\]
在这个平台上，根据的指导，这个频道在1989年4月17日重新启动，并改名为“消费者新闻与商业频道”。全国广播公司和最初是按照50:50的比例出资成立该频道，\[9\]
频道总部位于[新泽西州](../Page/新泽西州.md "wikilink")的河渠边。

CNBC最初很难获得有线电视运营商的转播渠道，因为有线电视运营上怀疑其很难与建立许久的竞争。截止1990年冬季，尽管拥有全国广播公司的支持，但是CNBC只拥有1,700万个家庭订阅用户，这个数据还不到财经新闻电视网的一半。\[10\]

但是就在这个时候，财经新闻电视网遭遇到了财政困难。在与[道琼斯](../Page/道琼斯公司.md "wikilink")-公司的竞标战之后，\[11\]
CNBC于1991年5月21日以1.453亿[美元的价格收购了财经新闻电视网](../Page/美元.md "wikilink")。\[12\]
随后CNBC合并两个公司的业务部门，并雇佣了原财经新闻电视网300名员工中的60人。\[13\]
这笔交易扩大了CNBC的家庭订阅用户数，使之上升到4,000万家庭用户。\[14\]
在交易完成之后，美国有线电视公司将自己持有的50%股份出售给全国广播公司。\[15\]
随着“消费者新闻与商业频道”全称被弃用，该频道的财经节目首先启用了“CNBC/FNN”这个呼号，但是这个呼号沿用到1990年代中期也被废止。

在罗杰斯的领导下，CNBC在1990年代飞速发展，并在1995年和1996年分别推出了[CNBC亚洲台和](../Page/CNBC亚洲台.md "wikilink")[CNBC欧洲台](../Page/CNBC欧洲台.md "wikilink")。\[16\]
1997年，CNBC与[道琼斯公司达成战略合作协议](../Page/道琼斯公司.md "wikilink")，其中包括与[道琼斯通讯社和](../Page/道琼斯通讯社.md "wikilink")《[华尔街日报](../Page/华尔街日报.md "wikilink")》共享内容，并将渠道命名为“NBC与道琼斯旗下頻道”。\[17\]
随后，CNBC的国际频道在1998年与道琼斯公司的旗下业务部门——位于[伦敦的欧洲商业财经公司以及位于](../Page/伦敦.md "wikilink")[新加坡的亚洲财经新闻公司合并组建了新公司](../Page/新加坡.md "wikilink")。\[18\]
CNBC美国频道的增长一直持续到[千禧年](../Page/千禧年.md "wikilink")[互联网泡沫破灭为止](../Page/互联网泡沫.md "wikilink")。\[19\]

[CNBC_entry_jeh.jpg](https://zh.wikipedia.org/wiki/File:CNBC_entry_jeh.jpg "fig:CNBC_entry_jeh.jpg")

新千年也给CNBC带来了新变化。2003年，CNBC的全球总部从新泽西州李堡的弗莱彻大街2200号迁往的森林大道900号。新总部拥有位于[英国](../Page/英国.md "wikilink")[诺丁汉郡](../Page/诺丁汉郡.md "wikilink")的PDG公司和位于美国[奥科伊的FX集团所制造的完全数字化视频制作设备和演播室](../Page/奥科伊_\(佛罗里达州\).md "wikilink")。

2005年年底，[NBC环球集团重新从道琼斯公司手里获得了CNBC亚洲台和CNBC欧洲台的完整控制权](../Page/NBC环球集团.md "wikilink")。但是，道琼斯公司对CNBC美国频道的股权却没有变化。\[20\]

现在，CNBC每天[美国东部时间早上](../Page/美国东部时间.md "wikilink")4点到晚上7点提供不间断的财经新闻节目服务；而在晚间和凌晨时段则提供[脱口秀](../Page/脱口秀.md "wikilink")、调查报告、[纪录片](../Page/纪录片.md "wikilink")、[资讯型广告和其他节目](../Page/资讯型广告.md "wikilink")。在CNBC[电视新闻镜面的底部](../Page/电视新闻镜面.md "wikilink")[字幕跑马灯上会实时推送](../Page/字幕跑马灯.md "wikilink")[纽约证交所](../Page/纽约证交所.md "wikilink")、[纳斯达克以及](../Page/纳斯达克.md "wikilink")[美国证交所的实时股票数据](../Page/美国证券交易所.md "wikilink")；或市场指数、新闻摘要以及CNBC气象专家提供的气象播报。\[21\]
而镜面顶部的跑马灯则主要提供来自国际市场的实时指数和商品价格更新。

2014年10月13日，CNBC美国台也采用了16:9的[信箱模式播出](../Page/黑边.md "wikilink")，以与它的欧洲台、亚洲台保持一致。从2016年1月4日起，由于应用了10号代码方案，将原本是[480i的标清画质实现在](../Page/480i.md "wikilink")4:3画面向16:9画面转换。

## 合作伙伴

### 道琼斯公司

自1997年12月以来，CNBC与出版与金融信息公司——[道琼斯公司达成战略合作协议](../Page/道琼斯公司.md "wikilink")。\[22\]
根据协议，CNBC与、《[华尔街日报](../Page/华尔街日报.md "wikilink")》、[道琼斯通讯社和](../Page/道琼斯通讯社.md "wikilink")《》进行了广泛的合作，他们的记者与编辑会经常在CNBC各档节目里出镜。一些CNBC的前项目中还包括道琼斯的品牌，\[23\]
目前CNBC与制作的联播周播节目就是《》。\[24\]

CNBC目前与道琼斯公司的协议在2012年到期，但是其合作的不确定性来自道琼斯公司在2007年被其竞争对手[福斯财经网的母公司](../Page/福斯财经网.md "wikilink")[新闻集团所收购](../Page/新闻集团_\(1979－2013年\).md "wikilink")。直到2013年新闻集团宣布分拆，道琼斯公司被用作组建新的[新闻集团](../Page/新闻集团.md "wikilink")，并提供给[21世纪福斯旗下的各频道](../Page/21世纪福斯.md "wikilink")。\[25\]
根据新闻集团[CEO](../Page/CEO.md "wikilink")[鲁伯特·默多克的表示](../Page/鲁伯特·默多克.md "wikilink")，目前的协议“只包括财经资讯方面的新闻与人员合作”，而在其他方面，道琼斯公司可以自由与21世纪福斯公司继续合作。\[26\]

### 其他合作伙伴

CNBC也与《[纽约时报](../Page/纽约时报.md "wikilink")》达成协议，自2008年1月开始进行内容合作。这被视为是两大媒体联合起来以对抗[新闻集团的扩张](../Page/新闻集团_\(1979－2013年\).md "wikilink")。\[27\]\[28\]
根据协议，CNBC可以从《纽约时报》获取新闻报道内容，而CNBC的视频内容也会出现在《纽约时报》的网站上。\[29\]
CNBC的视频剪辑内容和其他报道也会刊登在\[30\]
和[美国在线金融频道](../Page/美国在线.md "wikilink")。\[31\]

CNBC的实时市场价格、经济数据与其他统计资料主要由[汤森路透提供](../Page/汤森路透.md "wikilink")。\[32\]
自2006年9月以来，\[33\]
CNBC开始与[伦敦的](../Page/伦敦.md "wikilink")[富时集团合作运营自己的](../Page/富时集团.md "wikilink")[股市指数](../Page/股市指数.md "wikilink")。富时CNBC全球300指数来自上各行业最大的15家公司与[新兴市场的](../Page/新兴市场.md "wikilink")30家最大公司的相关数据。\[34\]
该指数在每个交易日的[美东时间晚上](../Page/美东时间.md "wikilink")9点到次日下午4点30分通过CNBC各档节目实时播出，特别是被《》节目所采用。

CNBC还维护着一个名为“CNBC投资者网络”的项目，该项目是将一系列美国独立金融机构交易室内的[网络摄像头连接在一起](../Page/网络摄像头.md "wikilink")。该项目于2007年10月22日启动，它将允许交易参与者和投资策略师通过摄像头在交易日出现在电视网上。\[35\]

2010年12月14日，CNBC宣布与CarryQuote进行软件开发及营销方面的合作，这促使了针对个人投资者的桌面及移动应用“CNBC
PRO”的诞生。\[36\] CNBC
PRO为用户提供来自100家[证券交易机构的实时财务数据](../Page/证券交易所.md "wikilink")，已经CNBC的新闻及视频[流媒体](../Page/流媒体.md "wikilink")。\[37\]

## 国际服务

[Fleet_Place.jpg](https://zh.wikipedia.org/wiki/File:Fleet_Place.jpg "fig:Fleet_Place.jpg")总部，位于[伦敦的](../Page/伦敦.md "wikilink")。\]\]
[International_Plaza,_Jan_06.JPG](https://zh.wikipedia.org/wiki/File:International_Plaza,_Jan_06.JPG "fig:International_Plaza,_Jan_06.JPG")总部，位于[新加坡的](../Page/新加坡.md "wikilink")。\]\]

CNBC从1995年开始经营自己的全球服务，同年[CNBC亚洲台宣布开播](../Page/CNBC亚洲台.md "wikilink")。紧接着在1996年，[CNBC欧洲台也宣布开播](../Page/CNBC欧洲台.md "wikilink")。1997年12月9日，[全国广播公司和](../Page/全国广播公司.md "wikilink")[道琼斯公司宣布将合并他们的国际财经新闻业务](../Page/道琼斯公司.md "wikilink")。这导致了CNBC欧洲台与[欧洲财经新闻以及CNBC亚洲台与](../Page/CNBC欧洲台.md "wikilink")[亚洲财经新闻的合并](../Page/CNBC亚洲台.md "wikilink")。从那时候起直至2006年1月，CNBC国际服务旗下频道的标语都是“NBC环球与道琼斯公司旗下服务”。\[38\]
在CNBC国际服务的发展过程中，[道琼斯通讯社也做出了贡献](../Page/道琼斯通讯社.md "wikilink")。CNBC欧洲台的总部位于[伦敦](../Page/伦敦.md "wikilink")，而CNBC亚洲台的总部则位于[新加坡](../Page/新加坡.md "wikilink")。2005年12月31日，道琼斯公司在CNBC国际服务上的交易生效。2006年1月1日，CNBC将“NBC环球与道琼斯公司旗下服务”字样从CNBC国际服务的频道中全部移走。与此相配的是，CNBC亚洲台将台标中的“亚洲”字样移走，以突破渠道局限；2008年9月30日，CNBC欧洲台也从台标中移走了“欧洲”字样。目前，CNBC旗下三大主要频道CNBC、CNBC亚洲台和CNBC欧洲台都是采用一样的台标。

除了[CNBC欧洲台和](../Page/CNBC欧洲台.md "wikilink")[CNBC亚洲台之外](../Page/CNBC亚洲台.md "wikilink")，CNBC还与其他公司在当地建立本地财经新闻频道。这些频道包括[意大利的](../Page/意大利.md "wikilink")；[土耳其的](../Page/土耳其.md "wikilink")；[阿拉伯联合酋长国的](../Page/阿拉伯联合酋长国.md "wikilink")；[日本的](../Page/日本.md "wikilink")[日经CNBC](../Page/日经CNBC.md "wikilink")；[印度的](../Page/印度.md "wikilink")、（[印地语](../Page/印地语.md "wikilink")）、CNBC
Baazar（[古吉拉特语](../Page/古吉拉特语.md "wikilink")）；[巴基斯坦的](../Page/巴基斯坦.md "wikilink")和[波兰的](../Page/波兰.md "wikilink")。

[CNBC欧洲台和](../Page/CNBC欧洲台.md "wikilink")[CNBC亚洲台会在某些地区更改台标和台呼](../Page/CNBC亚洲台.md "wikilink")，并在[新闻跑马灯上侧重于当地的财经资讯](../Page/新闻跑马灯.md "wikilink")。例如：CNBC北欧频道、CNBC新加坡频道、CNBC香港频道和CNBC澳洲频道。其中CNBC香港频道和CNBC澳洲频道会在免费电视网的12:55、17:55和21:55三个时间段提供5分钟的财经资讯。

在[北美地区](../Page/北美地区.md "wikilink")，会播出[CNBC欧洲台和](../Page/CNBC欧洲台.md "wikilink")[CNBC亚洲台的节目](../Page/CNBC亚洲台.md "wikilink")，以及和[联合国提供的周刊节目](../Page/联合国.md "wikilink")。

而在[中美洲和](../Page/中美洲.md "wikilink")[加勒比海地区](../Page/加勒比海地区.md "wikilink")，则会播出CNBC和的节目。

在[加拿大](../Page/加拿大.md "wikilink")，CNBC播出的节目基本和美国版一样。但是由于部分节目在加拿大的版权归属问题，CNBC晚上九点播出的综艺节目，例如：《[一掷千金](../Page/一掷千金_\(电视节目\).md "wikilink")》、《[飞黄腾达](../Page/飞黄腾达.md "wikilink")》、《[以一敌百](../Page/以一敌百.md "wikilink")》和《单挑扑克（）》，以及任何与[奥运会相关的节目都会被](../Page/奥运会.md "wikilink")节目所取代。不过，[纪录片可以正常在加拿大播出](../Page/纪录片.md "wikilink")。这就造成了一个重大问题，曾于2006年4月23日播出的《飞黄腾达
第四季》的某一集并没有在加拿大地区被遮蔽掉。原本在当天晚上9点的首播被遮蔽掉了，但是在次日凌晨的重播却意外没有被遮蔽。但是在加拿大播出版权归属[全国广播公司和](../Page/全国广播公司.md "wikilink")[环球电视的节目会依旧被屏蔽下去](../Page/環球電視_\(加拿大\).md "wikilink")。然而有时播出的时候如果没有被屏蔽的话，那应该是和节目信号出现错误有关。

CNBC最近推出的国际频道服务就是。仅仅是在[南非](../Page/南非.md "wikilink")，CNBC就投入了6亿[美元的广告预算](../Page/美元.md "wikilink")，因为CNBC看好CNBC非洲台在该地区的收视潜力。CNBC非洲台在南非、[尼日利亚和](../Page/尼日利亚.md "wikilink")[肯尼亚设立办事处](../Page/肯尼亚.md "wikilink")，每个交易日制作大约9小时的当地节目内容。CNBC非洲台已于2007年6月1日开播。\[39\]

CNBC的[朝鲜语频道](../Page/朝鲜语.md "wikilink")[SBS-CNBC已于](../Page/SBS-CNBC.md "wikilink")2010年1月在[韩国推出](../Page/韩国.md "wikilink")，这是CNBC品牌在全球的第15个频道。\[40\]

2016年1月10日，CNBC与达成战略合作伙伴协议，即将为CNBC推出[印度尼西亚语频道的CNBC印尼台](../Page/印度尼西亚语.md "wikilink")。\[41\]

<File:CNBC> Pakistan HQ at night.jpg|右侧为总部。 <File:CNBC> Awaaz News
Van.jpg|的新闻厢货车。 <File:TVN> CNBC Biznes.jpg|的采访。 <File:CNBC> in
Dubai.jpg|的总部。

## 机场服务

CNBC旗下的专营店（内部称之为“局”）一般对外标示为“”，在美国国内诸多机场都有开设。自2001年以来，CNBC与签署了许可协议以开设这些专营店。\[42\]
这些专营店出售CNBC的限定版周边商品，以及报纸、杂志和少量的饮料与小吃。

### 商店分布

  - [路易斯安那州](../Page/路易斯安那州.md "wikilink")[巴吞鲁日机场](../Page/巴吞鲁日_\(路易斯安那州\).md "wikilink")
  - [马里兰州](../Page/马里兰州.md "wikilink")[巴尔的摩](../Page/巴尔的摩.md "wikilink")[巴华机场](../Page/巴尔的摩/华盛顿国际机场.md "wikilink")
  - [俄亥俄州](../Page/俄亥俄州.md "wikilink")[哥伦布](../Page/哥伦布_\(俄亥俄州\).md "wikilink")[哥伦布机场](../Page/哥伦布国际机场.md "wikilink")
  - [密歇根州](../Page/密歇根州.md "wikilink")[底特律机场](../Page/底特律.md "wikilink")
  - [佛罗里达州](../Page/佛罗里达州.md "wikilink")[麦尔兹堡机场](../Page/麦尔兹堡.md "wikilink")
  - [德克萨斯州](../Page/德克萨斯州.md "wikilink")[休斯顿](../Page/休斯顿.md "wikilink")[布什机场](../Page/喬治·布什洲際機場.md "wikilink")

<!-- end list -->

  - [密苏里州](../Page/密苏里州.md "wikilink")[堪萨斯城机场](../Page/堪薩斯城_\(密蘇里州\).md "wikilink")
  - 德克萨斯州[麦卡伦机场](../Page/麦卡伦_\(得克萨斯州\).md "wikilink")\[43\]\[44\]
  - [威斯康辛州](../Page/威斯康辛州.md "wikilink")[密尔沃基机场](../Page/密尔沃基.md "wikilink")
  - [伊利诺伊州](../Page/伊利诺伊州.md "wikilink")[莫林机场](../Page/莫林_\(伊利诺伊州\).md "wikilink")
  - [俄克拉荷马州](../Page/俄克拉荷马州.md "wikilink")[俄克拉何马城机场](../Page/俄克拉何马城.md "wikilink")
  - [宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[费城机场](../Page/费城.md "wikilink")

<!-- end list -->

  - [亚利桑那州](../Page/亚利桑那州.md "wikilink")[凤凰城机场](../Page/鳳凰城_\(亞利桑那州\).md "wikilink")
  - [罗德岛州](../Page/罗德岛州.md "wikilink")[普罗维登斯机场](../Page/普罗维登斯_\(罗德岛州\).md "wikilink")
  - [犹他州](../Page/犹他州.md "wikilink")[盐湖城机场](../Page/盐湖城.md "wikilink")
  - [加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[旧金山机场](../Page/旧金山.md "wikilink")
  - [北卡罗来纳州](../Page/北卡罗来纳州.md "wikilink")[罗利-达拉姆机场](../Page/羅利達拉姆國際機場.md "wikilink")
  - 俄克拉荷马州[塔尔萨机场](../Page/塔尔萨.md "wikilink")

## 批评反响

## 同类频道

  - [彭博电视](../Page/彭博电视.md "wikilink")
  - [福斯财经网](../Page/福斯财经网.md "wikilink")
  - [中国中央电视台财经频道](../Page/中国中央电视台财经频道.md "wikilink")
  - [第一财经电视](../Page/第一财经电视.md "wikilink")
  - [東森財經新聞台](../Page/東森財經新聞台.md "wikilink")
  - [台視財經台](../Page/台視財經台.md "wikilink")
  - [Now財經台](../Page/Now財經台.md "wikilink")
  - [非凡商業台](../Page/非凡商業台.md "wikilink")

## 参考资料

### 脚注

### 出典

## 外部链接

  - [CNBC官方网站](https://www.cnbc.com/)

[\*](../Category/CNBC.md "wikilink")
[Category:NBC環球](../Category/NBC環球.md "wikilink")
[Category:美國電視台](../Category/美國電視台.md "wikilink")
[Category:國家廣播公司](../Category/國家廣播公司.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:卫星电视频道](../Category/卫星电视频道.md "wikilink")
[Category:網路電視頻道](../Category/網路電視頻道.md "wikilink")
[Category:新泽西州文化](../Category/新泽西州文化.md "wikilink")
[Category:美國24小時電視新聞頻道](../Category/美國24小時電視新聞頻道.md "wikilink")
[Category:1989年成立的电视台或电视频道](../Category/1989年成立的电视台或电视频道.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.
12. 在这次收购失败之后，[道琼斯公司在](../Page/道琼斯公司.md "wikilink")20年后组建了CNBC的主要竞争对手[福斯财经网](../Page/福斯财经网.md "wikilink")。

13.

14.
15.

16.

17.

18.

19.

20.

21. 在2006年3月27日之前，CNBC的所有天气报告均由[准确天气提供](../Page/AccuWeather.md "wikilink")。

22.
23.
24.

25.

26.
27.

28.

29.
30.

31.

32. Acknowledged at bottom of [cnbc.com homepage](https://www.cnbc.com)

33.

34.
35.

36.

37.

38. 部分国际频道会根据所在地合作伙伴的不同而修改，会用当地的合作伙伴替代“道琼斯公司”字样。

39.

40.

41.

42. [Paradies - Brand Detail -
    CNBC](http://www.theparadiesshops.com/branddetail.asp?ID=19) ,
    theparadiesshops.com, undated. 2011-02-05查阅.

43. [Airport Opens Gift Shop; Construction To Begin On CNBC
    Store](http://www.mcallen.net/news/default/2010-07-22/airport_opens_gift_shop_construction_to_begin_on_cnbc_store.aspx),
    McAllen.net (City of McAllen website), 2010-07-22. 2011-02-05查阅.

44. [McAllen Airport Announces Opening Of CNBC
    Store](http://www.mcallen.net/news/default/2010-10-18/mcallen_airport_announces_opening_of_cnbc_store.aspx),
    McAllen.net (City of McAllen website), 2010-10-08. 2011-02-05查阅.