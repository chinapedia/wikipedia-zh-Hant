[Firefighters_in_Iraq.JPG](https://zh.wikipedia.org/wiki/File:Firefighters_in_Iraq.JPG "fig:Firefighters_in_Iraq.JPG")
[Firefighting_exercise.jpg](https://zh.wikipedia.org/wiki/File:Firefighting_exercise.jpg "fig:Firefighting_exercise.jpg")第20[工兵中队消防兵在](../Page/工兵.md "wikilink")[南卡罗来纳州的](../Page/南卡罗来纳州.md "wikilink")[肖空军基地](../Page/肖空军基地.md "wikilink")（）进行灭火[演习](../Page/演习.md "wikilink")\]\]

**消防員**為[政府或民間團體所成立的滅](../Page/政府.md "wikilink")[火及救援團體的成員](../Page/火.md "wikilink")，通常是一個[國家所成立專門司職於滅火及救援等事項的專門](../Page/國家.md "wikilink")[部門最底層的分枝](../Page/部門.md "wikilink")，為站在第一線的[政府機關](../Page/政府機關.md "wikilink")；消防員即為其中成員。一些[熱心的民間人士也會在政府的許可下成立民間的](../Page/熱心.md "wikilink")[消防隊](../Page/消防隊.md "wikilink")，台湾簡稱「義消」，一般皆為服務性質，熱心服務大眾，屬志工類型。消防是高[危险的](../Page/危险.md "wikilink")[工作](../Page/工作.md "wikilink")，存在比較高的[殉職](../Page/殉職.md "wikilink")[機率](../Page/機率.md "wikilink")。

## 编制

在世界范围内，大部分[国家和地区](../Page/国家.md "wikilink")，例如[美国](../Page/美国.md "wikilink")、[德国等](../Page/德国.md "wikilink")，消防员实行的是[职业制](../Page/职业制.md "wikilink")，甚至是[终身制](../Page/终身制.md "wikilink")，[退休后享受政府](../Page/退休.md "wikilink")[养老金](../Page/养老金.md "wikilink")。而对于新招收的消防员，其要求教育程度需在[高中以上](../Page/高中.md "wikilink")，之后需要進行一段時間的培训，经考核合格后才能升格成为正式的消防员。\[1\]

在[中国](../Page/中国.md "wikilink")，消防员一般实行职业公务员制、[企业或政府专职以及社会志愿参与这三种编制](../Page/企业.md "wikilink")，即[中国消防救援队](../Page/中国消防救援队.md "wikilink")、政府及依法应当建立专职消防队的专职消防人员以及志愿消防组织。其中，[中国消防救援队实行的职业公务员制](../Page/中国消防救援队.md "wikilink")，是目前消防工作的主要力量，占全国的60%以上，约有23万人。但近年来现役消防编制得不到增加的情况日益突出，原[公安消防部队开始大量依托地方政府招收合同制消防员使用](../Page/公安消防部队.md "wikilink")，目前合同制消防员的数量已达到现役消防员的数量。公安消防员的招收实行的是征兵制，两年服役期，但是近半数可专为专业警士继续服役。公安消防部队和专职消防人员一般依法享受社会保险和福利待遇\[2\]。合同制消防员和专职消防员是需要通过[面试](../Page/面试.md "wikilink")、[体检](../Page/体检.md "wikilink")、[考核以及](../Page/考核.md "wikilink")[政审四关才能成为正式消防员](../Page/政治审查.md "wikilink")，一般实行的合同制\[3\]\[4\]。中国曾实行的消防现役制与西方职业制消防员两种编制不同之处在于，由于现役制消防员受到服役期的限制，可能刚刚熟悉消防工作或还没有熟悉，就已经面临服役期到来而离开消防工作，因此更新换代比较快。而西方职业制消防员一般都经验丰富，但更新换代比较慢\[5\]。

## 職務

消防员工作一般有两类，一种是行政工作，主要是负责[文书](../Page/文书.md "wikilink")、联络、消防安全設備稽查與消防教育宣導等工作；另一种是实战工作，主要负责灭火与拯救工作。但在实际分配中，不同的编制会有不同的工作重点。专职消防队一般主要负责重大[灾害事故和其他以抢救人员生命的应急救援](../Page/灾害.md "wikilink")，对志愿消防队等消防组织进行[业务指导](../Page/业务.md "wikilink")；有时还会根据扑救火灾的需要，调动指挥专职消防队参加一些火灾扑救。而[志愿消防组织一般是制定防火安全](../Page/志愿.md "wikilink")[公约](../Page/公约.md "wikilink")，普及消防知识、协助进行防火安全检查，有时还需协助专职消防队的火灾扑救工作。\[6\]

<File:Polish_fire_brigade_car.jpg>|[波兰消防车](../Page/波兰.md "wikilink")
[File:Strahlenschutz_uebung.jpg|救護中的消防隊員](File:Strahlenschutz_uebung.jpg%7C救護中的消防隊員)
<File:Pompiers> ste-anne-de-beaupré
2012.jpg|[加拿大消防员在一次行动中](../Page/加拿大.md "wikilink")

## 消防工具

消防员在参与消防工作时，需要用到一些工具，这些工具确保消防员完成灭火与拯救工作。这些工具有[消防车](../Page/消防车.md "wikilink")、[曲臂云梯车](../Page/曲臂云梯车.md "wikilink")、[直臂云梯车](../Page/直臂云梯车.md "wikilink")、[消防船](../Page/消防船.md "wikilink")、艇，有时还会用到[直升飞机](../Page/直升飞机.md "wikilink")。而另一方面，也需要确保消防员的人身安全，这些个人防护装备有滤毒空气[呼吸器](../Page/呼吸器.md "wikilink")、防化学腐蚀和高温的[防护衣](../Page/防护衣.md "wikilink")、[有害气体](../Page/有害气体.md "wikilink")[报警器](../Page/报警器.md "wikilink")；以及一些检测仪，如气体[检测仪](../Page/检测仪.md "wikilink")、有害液体及固体检测仪、非接触型[热点](../Page/热点.md "wikilink")[探测器](../Page/探测器.md "wikilink")；另外还会需要一些辅助工具，如防爆[手电筒](../Page/手电筒.md "wikilink")、[围堵用堤索](../Page/围堵用堤索.md "wikilink")、除污冲洗设备和[护目镜等](../Page/护目镜.md "wikilink")。\[7\]

## 社會認同

消防員由於其職業的特殊性質，經常參與搶救公私財產及民眾生命，其地位被社會廣泛地認同。在西方國家，消防員這一職業由於其危險性以及不畏危險的專業精神，被認為是廣受尊重的一個職業。在2001年[美國](../Page/美國.md "wikilink")[紐約市發生的](../Page/紐約市.md "wikilink")[911事件當中](../Page/911事件.md "wikilink")，紐約市的消防員於搶救大樓傷員及控制大火上付出了巨大的犧牲，他們的敬業精神令美國甚至於其他國家的民眾為之動容。而攝影師在當時更是拍下大量消防員救人的照片，更有疲憊不堪的消防員於搶救大火及傷員后獨自坐在廢區當中哭泣的照片被畫家創作成油畫於世界各地廣為流傳。而2009年[澳洲](../Page/澳洲.md "wikilink")[維州山火中](../Page/2009年維多利亞州山火.md "wikilink")，一隻被燒傷的\[樹熊\]主動的向一名消防員靠攏，並將燒傷的手臂放在消防員的手中，而消防員對它進行餵水的照片更是令全球為之感動。\[8\]

## 参考文献

## 相關網站

  - [中国消防在线](http://119.china.com.cn/)
  - [香港特別行政區政府消防處](http://www.hkfsd.gov.hk)
  - [澳門特別行政區政府消防局](https://web.archive.org/web/20060701120516/http://www.fsm.gov.mo/cb/)
  - [中華民國內政部消防署](http://www.nfa.gov.tw/)

[消防員](../Category/消防員.md "wikilink")
[Category:服务性职业](../Category/服务性职业.md "wikilink")

1.

2.

3.

4.

5.
6.

7.
8.  <http://hk.crntt.com/doc/1008/8/4/7/100884721.html?coluid=7&kindid=0&docid=100884721>