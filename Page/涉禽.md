**涉禽**（wader），在北美也称为水鸟（shorebirds，在这些地方的涉禽往往指的是长脚的涉水[鸟类](../Page/鸟类.md "wikilink")，如[鹳和](../Page/鹳.md "wikilink")[鹭](../Page/鹭.md "wikilink")），属[鸻形目](../Page/鸻形目.md "wikilink")（*Charadriiformes*），但不包括那些海边有蹼的[海鸟](../Page/海鸟.md "wikilink")。后者包括[贼鸥](../Page/贼鸥.md "wikilink")*skua*（[贼鸥科](../Page/贼鸥科.md "wikilink")，*Stercorariidae*）、[海鸥](../Page/海鸥.md "wikilink")*gull*（[鸥科](../Page/鸥科.md "wikilink")，*Laridae*）、[燕鸥](../Page/燕鸥.md "wikilink")*tern*（[燕鸥科](../Page/燕鸥科.md "wikilink")，*Sternidae*）、[剪嘴鸥](../Page/剪嘴鸥.md "wikilink")*skimmer*（[剪嘴鸥科](../Page/剪嘴鸥科.md "wikilink")，*Rhynchopidae*）及[海雀](../Page/海雀.md "wikilink")*auk*（[海雀科](../Page/海雀科.md "wikilink")，*Alcidae*）。另外，[林鹬](../Page/林鹬.md "wikilink")*pratincole*（[燕鸻科](../Page/燕鸻科.md "wikilink")，*Glareolidae*）和[蟹鸻](../Page/蟹鸻.md "wikilink")*Crab
Plover*（[蟹鸻科](../Page/蟹鸻科.md "wikilink")，*Dromadidae*）外表和涉禽更像，也和[海鸟有很密切的关系](../Page/海鸟.md "wikilink")。

涉禽至少有210
个[物种](../Page/物种.md "wikilink")，大多数[物种都分布在](../Page/物种.md "wikilink")[湿地或](../Page/湿地.md "wikilink")[沿海](../Page/沿海.md "wikilink")。[北极和](../Page/北极.md "wikilink")[温带的一些](../Page/温带.md "wikilink")[物种比较多会](../Page/物种.md "wikilink")[迁徙](../Page/迁徙.md "wikilink")，而[热带的](../Page/热带.md "wikilink")[物种则常为](../Page/物种.md "wikilink")[留鸟](../Page/留鸟.md "wikilink")（无[迁徙习性](../Page/迁徙.md "wikilink")）或只在不同降雨带[迁徙](../Page/迁徙.md "wikilink")。一些[北极物种](../Page/北极.md "wikilink")，如长途[迁徙动物中的](../Page/迁徙.md "wikilink")[小滨鹬](../Page/小滨鹬.md "wikilink")，非[繁殖季节会在](../Page/繁殖季节.md "wikilink")[南半球](../Page/南半球.md "wikilink")。

大多数涉禽会吃[土壤中翻出来或暴露在外的小](../Page/土壤.md "wikilink")[昆虫](../Page/昆虫.md "wikilink")。不同[喙的长度使得有着同一个习惯的涉禽会吃不同的食物](../Page/喙.md "wikilink")，特别是在海边。这也使得它们在食物方面并没有直接竞争。一些涉禽在[喙的尾端有着敏感的](../Page/喙.md "wikilink")[神经末梢](../Page/神经末梢.md "wikilink")，这样就能很好地吃到泥土中的[猎物](../Page/猎物.md "wikilink")。一些更大的物种，特别是栖息在更干燥地区的会吃更大的[猎物](../Page/猎物.md "wikilink")，包括[昆虫和小的](../Page/昆虫.md "wikilink")[爬行动物](../Page/爬行动物.md "wikilink")。

一种栖息在海边更小的物种，特别是但不仅仅是[滨鹬](../Page/滨鹬.md "wikilink")*calidrids*，也常称为“[鹬](../Page/鹬.md "wikilink")*sandpipers*”，但这个称呼并不严格，因为[矶鹞](../Page/矶鹞.md "wikilink")*Upland
Sandpiper* 是草地品种。

## 体重及大小

这一类动物最小的成员是[小滨鹬](../Page/小滨鹬.md "wikilink")*Least
Sandpiper*，小的成年动物重量会达到15.5克且大小只有13厘米（5英寸）。一般认为最大的是[大杓鹬](../Page/大杓鹬.md "wikilink")*Far
Eastern
Curlew*，约63厘米（25英寸）、860克（1.9磅）。虽然[反嘴鴴](../Page/反嘴鴴.md "wikilink")*Beach
Thick-knee*重量有1千克（2.2磅），是最重的。

## 分类学

在[鳥類DNA分類系統](../Page/鳥類DNA分類系統.md "wikilink")（Sibley-Ahlquist
taxonomy），涉禽和其它的一些动物被一起归到更大的[鸻形目](../Page/鸻形目.md "wikilink")*Charadriiformes*。但是，由于被证实并不适用解释[鸻形目的内部关系](../Page/鸻形目.md "wikilink")，将涉禽归到[鸻形目成了](../Page/鸻形目.md "wikilink")[鳥類DNA分類系統的一大缺陷](../Page/鳥類DNA分類系統.md "wikilink")。早期，涉禽曾被归类到一个单一的[亚目](../Page/亚目.md "wikilink")**[鸻亚目Charadrii](../Page/鸻亚目.md "wikilink")**，但最终证实这是个""，含有不低于4个血缘的[并系群](../Page/并系群.md "wikilink")。但是，[领鹑](../Page/领鹑.md "wikilink")*Plains
Wanderer*实际上也是属于其中的一种。根据最新的研究 (Ericson *et al.*, 2003; Paton *et al.*,
2003; Thomas *et al.*, 2004a, b; van Tuinen *et al.*, 2004; Paton &
Baker, 2006)，涉禽可以细分为以下几种：

  - **[Scolopaci亚目](../Page/Scolopaci亚目.md "wikilink")** *Scolopaci*
      - [鷸科](../Page/鷸科.md "wikilink")（*Scolopacidae*）：[沙锥](../Page/沙锥.md "wikilink")*snipe*，[鹬](../Page/鹬.md "wikilink")*sandpipers*，[瓣蹼鹬](../Page/瓣蹼鹬.md "wikilink")*phalaropes*等

<!-- end list -->

  - **[Thinocori亚目](../Page/Thinocori.md "wikilink")** *Thinocori*
      - [彩鷸科](../Page/彩鷸科.md "wikilink")（*Rostratulidae*）：[彩鹬](../Page/彩鹬.md "wikilink")*painted
        snipe*
      - [雉鴴科](../Page/雉鴴科.md "wikilink")（*Jacanidae*）：[雉鸻](../Page/雉鸻.md "wikilink")*jacanas*
      - [籽鷸科](../Page/籽鷸科.md "wikilink")（*Thinocoridae*）：[籽鷸](../Page/籽鷸.md "wikilink")*seedsnipe*
      - [領鶉科](../Page/領鶉科.md "wikilink")（*Pedionomidae*）：[领鹑](../Page/领鹑.md "wikilink")*Plains
        Wanderer*

<!-- end list -->

  - **[Chionidi亚目](../Page/Chionidi亚目.md "wikilink")** *Chionidi*
      - [石鴴科](../Page/石鴴科.md "wikilink")（*Burhinidae*）：[石鸻](../Page/石鸻.md "wikilink")*thick-knees*
      - [鞘嘴鷗科](../Page/鞘嘴鷗科.md "wikilink")（*Chionididae*）：[鞘嘴鸥](../Page/鞘嘴鸥.md "wikilink")*sheathbills*
      - [Pluvianellidae科](../Page/Pluvianellidae科.md "wikilink")（*Pluvianellidae*）：[麦哲伦鸻](../Page/麦哲伦鸻.md "wikilink")*Magellanic
        Plover*

<!-- end list -->

  - **[鸻亚目](../Page/鸻亚目.md "wikilink")** *Charadrii*
      - [鹮嘴鷸科](../Page/鹮嘴鷸科.md "wikilink")（*Ibidorhynchidae*）：[鹮嘴鹬](../Page/鹮嘴鹬.md "wikilink")*Ibisbill*
      - [反嘴鷸科](../Page/反嘴鷸科.md "wikilink")（*Recurvirostridae*）：[反嘴鹬](../Page/反嘴鹬.md "wikilink")*avocets*和[长脚鹬](../Page/长脚鹬.md "wikilink")*stilts*
      - [蠣鷸科](../Page/蠣鷸科.md "wikilink")（*Haematopodidae*）：[蛎鹬](../Page/蛎鹬.md "wikilink")*oystercatchers*
      - [鸻科](../Page/鸻科.md "wikilink")（*Charadriidae*）：[鸻](../Page/鸻.md "wikilink")*plovers*和[麦鸡](../Page/麦鸡.md "wikilink")*lapwings*

在传统的[分类中](../Page/分类.md "wikilink")，[Thinocori亚目被归类到](../Page/Thinocori亚目.md "wikilink")[Scolopaci亚目](../Page/Scolopaci亚目.md "wikilink")，而[Chionidi亚目被归类到](../Page/Chionidi亚目.md "wikilink")**[鸻亚目](../Page/鸻亚目.md "wikilink")**。但是，随着对现代[鸟类早期](../Page/鸟类.md "wikilink")[进化史知识的增多](../Page/进化史.md "wikilink")，Paton
*et al.* (2003)和Thomas *et al.*
(2004b)的4个“涉禽”[血缘](../Page/血缘.md "wikilink")（＝[亚目](../Page/亚目.md "wikilink")）在[白垩纪-第三纪灭绝事件前后已经存在的假设被认为是正确的](../Page/白垩纪-第三纪灭绝事件.md "wikilink")。

## 参见

  - [鸻形目](../Page/鸻形目.md "wikilink")

## 参考文献

  - **Ericson**, P. G. P.; Envall, I.; Irestedt, M. & Norman, J. A.
    (2003): Inter-familial relationships of the shorebirds (Aves:
    Charadriiformes) based on nuclear DNA sequence data. *[BMC Evol.
    Biol.](../Page/BMC_journals.md "wikilink")* **3**: 16.  [PDF
    fulltext](http://www.biomedcentral.com/content/pdf/1471-2148-3-16.pdf)

<!-- end list -->

  - **Paton**, Tara A. & **Baker**, Allan J. (2006): Sequences from 14
    mitochondrial genes provide a well-supported phylogeny of the
    Charadriiform birds congruent with the nuclear RAG-1 tree.
    *Molecular Phylogenetics and Evolution* **39**(3): 657–667.  (HTML
    abstract)

<!-- end list -->

  - **Paton**, T. A.; Baker, A. J.; Groth, J. G. & Barrowclough, G. F.
    (2003): RAG-1 sequences resolve phylogenetic relationships within
    charadriiform birds. *Molecular Phylogenetics and Evolution* **29**:
    268-278.  (HTML abstract)

<!-- end list -->

  - **Thomas**, Gavin H.; Wills, Matthew A. & Székely, Tamás (2004a):
    Phylogeny of shorebirds, gulls, and alcids (Aves: Charadrii) from
    the cytochrome-*b* gene: parsimony, Bayesian inference, minimum
    evolution, and quartet puzzling. *Molecular Phylogenetics and
    Evolution* **30**(3): 516-526.  (HTML abstract)

<!-- end list -->

  - **Thomas**, Gavin H.; Wills, Matthew A. & Székely, Tamás (2004b): A
    supertree approach to shorebird phylogeny. *[BMC Evol.
    Biol.](../Page/BMC_journals.md "wikilink")* **4**: 28.  [PDF
    fulltext](http://www.pubmedcentral.org/picrender.fcgi?artid=515296&blobtype=pdf)
    [Supplementary
    Material](http://www.pubmedcentral.org/articlerender.fcgi?artid=515296#supplementary-material-sec)

<!-- end list -->

  - **van Tuinen**, Marcel; Waterhouse, David & Dyke, Gareth J. (2004):
    Avian molecular systematics on the rebound: a fresh look at modern
    shorebird phylogenetic relationships. *Journal of Avian Biology*
    **35**(3): 191-194. [PDF
    fulltext](http://www.stanford.edu/group/hadlylab/images/Lab%20Members/Marcel/JAB2004.PDF_1.pdf)

[Category:禽类](../Category/禽类.md "wikilink")
[Category:涉禽](../Category/涉禽.md "wikilink")