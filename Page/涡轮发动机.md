[Dampfturbine_Laeufer01.jpg](https://zh.wikipedia.org/wiki/File:Dampfturbine_Laeufer01.jpg "fig:Dampfturbine_Laeufer01.jpg")機組所使用的大型蒸汽渦輪。\]\]
[Water_turbine_grandcoulee.jpg](https://zh.wikipedia.org/wiki/File:Water_turbine_grandcoulee.jpg "fig:Water_turbine_grandcoulee.jpg")[大古力水壩](../Page/大古力水壩.md "wikilink")750MW[水輪機](../Page/水輪機.md "wikilink")\]\]
[Turbines_impulse_v_Chinese.png](https://zh.wikipedia.org/wiki/File:Turbines_impulse_v_Chinese.png "fig:Turbines_impulse_v_Chinese.png")，右为[反击式水轮机](../Page/反击式水轮机.md "wikilink")\]\]

**涡轮发动机**（**Turbine
engine**，或常簡稱為**涡轮**，**Turbine**）是一種利用旋轉的機件自穿過它的[流體中汲取](../Page/流體.md "wikilink")[動能的](../Page/動能.md "wikilink")[發動機形式](../Page/發動機.md "wikilink")。經常在[飞机與大型的](../Page/飞机.md "wikilink")[船舶或](../Page/船舶.md "wikilink")[車輛上看到其應用](../Page/車.md "wikilink")。

雖然渦輪發動機可能有許多不同的運作原理，但最簡單的渦輪型式可以只包含一個「轉子」（Rotor），例如一個帶有中心軸的扇葉，將此扇葉放置在[流體中](../Page/流體.md "wikilink")（例如[空氣或](../Page/空氣.md "wikilink")[水](../Page/水.md "wikilink")），流體通過時對扇葉施加的力量會帶動整個轉子開始轉動，進而得以從中心軸輸出軸向的[扭力](../Page/扭力.md "wikilink")。[風車與](../Page/風車.md "wikilink")[水車這類的裝置](../Page/水車.md "wikilink")，可以說是人類最早發明的渦輪發動機原型。

依照不同的分類方式，渦輪發動機也可以分類成很多不同的型式。例如以燃燒室與轉子的位置是否在一起來區別，就存在有屬於[外燃機一類的](../Page/外燃機.md "wikilink")[蒸汽渦輪發動機](../Page/蒸汽渦輪發動機.md "wikilink")，與屬於[內燃機的](../Page/內燃機.md "wikilink")[燃氣渦輪發動機](../Page/燃氣渦輪發動機.md "wikilink")。

如果將渦輪發動機反過來運作，則會變成一種輸入力量之後可以將流體帶動的設備，例如[壓縮機](../Page/壓縮機.md "wikilink")（compressor）與[泵](../Page/泵.md "wikilink")（pump）。

有些渦輪發動機本身具有多組扇葉，其中部分是用於自流體汲取動力，部分是用於推動流體，二者不能混為一談。舉例來說在大部分的[渦輪扇發動機與](../Page/渦輪扇發動機.md "wikilink")[渦輪螺旋槳發動機中](../Page/渦輪螺旋槳發動機.md "wikilink")，位於燃燒室之前的扇葉實際的作用是用於加壓進氣，因此應被視為是一種壓縮機。真正的渦輪機部分是位於燃燒室後方的扇葉，被燃燒後的排氣推動產生動力，再透過傳動軸將力量輸送至主扇葉（渦輪扇發動機）或[螺旋槳](../Page/螺旋槳.md "wikilink")（渦輪螺旋槳發動機）處，推動其運轉。

## 渦輪增壓引擎

渦輪增壓引擎是利用渦輪將空氣壓縮後強制送入汽缸內，因此汽缸中的壓力必然是屬於正壓，也就是高於一個大氣壓力之上。以汽車渦輪為例，渦輪的作用雖然是將空氣壓縮後送入汽缸內，但驅動渦輪的力量卻是來自於引擎排出的廢氣，當引擎轉速逐漸提升後，廢氣排出的力量便會增大，渦輪轉速也會相對的提高，這時候送入引擎的空氣也就會更加處於高壓縮狀態。不過當引擎處於中低轉速時，由於汽缸排出的廢氣還不足以使渦輪達到最大的增壓狀態，因此這時候儘管踩下油門踏板，引擎也無法發揮應有的增壓效果，這樣的現象也就是一般所謂的「渦輪遲滯」，也就是turbo
lag。

關於渦輪增壓引擎的運轉過程，進氣溫度也是攸關增壓反應與動力輸出的重要環節。由於空氣在壓縮後會導致溫度提高，進氣溫度一旦過高，除了會影響到引擎的燃燒效率，也有可能會導致爆震的現象產生。為了解決空氣在壓縮後溫度提高的問題，大多數渦輪增壓引擎都會在渦輪與引擎之間裝設一個用來冷卻空氣的裝置。由於渦輪的動力是來自於引擎排出的廢氣，所以只要引擎持續排出廢氣，渦輪便會一直處於增壓的狀態，但是引擎並非隨時都需要渦輪送入高壓空氣，而且渦輪在增壓時也必須要有一定的上限，否則送入引擎的空氣如果壓力過高，便很可能會導致內部機件損毀，嚴重甚至會有引擎爆炸的危險。至於維持渦輪增壓的裝置，原廠引擎通常是在渦輪上裝設一個「洩壓閥」，一旦壓力超過了預設值之後，洩壓閥便會自動開啟，一來可避免渦輪持續增壓，二來則是使渦輪能夠維持在預設的增壓值。

## 參看

  - [阿基米德式螺旋抽水机](../Page/阿基米德式螺旋抽水机.md "wikilink")
  - [渦輪扇發動機](../Page/渦輪扇發動機.md "wikilink")
  - [涡轮喷气发动机](../Page/涡轮喷气发动机.md "wikilink")
  - [渦輪軸發動機](../Page/渦輪軸發動機.md "wikilink")
  - [渦輪螺旋槳發動機](../Page/渦輪螺旋槳發動機.md "wikilink")
  - [槳扇發動機](../Page/槳扇發動機.md "wikilink")
  - [蒸汽渦輪發動機](../Page/蒸汽渦輪發動機.md "wikilink")
  - [燃氣渦輪發動機](../Page/燃氣渦輪發動機.md "wikilink")

[Category:涡轮](../Category/涡轮.md "wikilink")
[渦](../Category/機械工程.md "wikilink")
[Category:发动机](../Category/发动机.md "wikilink")