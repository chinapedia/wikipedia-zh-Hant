{{ Expand |time=2010-09-23}}
[Hot_pot_in_Taiwanese_restaurant.jpg](https://zh.wikipedia.org/wiki/File:Hot_pot_in_Taiwanese_restaurant.jpg "fig:Hot_pot_in_Taiwanese_restaurant.jpg")
**涮涮鍋**或者**呷哺呷哺**（日語：しゃぶしゃぶ），是一種起源於日本的、以[昆布](../Page/昆布.md "wikilink")[柴魚湯等為](../Page/柴魚湯.md "wikilink")[湯底的涮牛肉或者涮豬肉火鍋](../Page/湯底.md "wikilink")，基本以神戶[和牛為主食](../Page/和牛.md "wikilink")、偶爾也有松阪豬。涮涮鍋有時候所使用的湯底是用[柴魚或](../Page/柴魚.md "wikilink")[雞骨](../Page/雞.md "wikilink")、[大骨去熬的](../Page/大骨.md "wikilink")，再加以鹽或者味增簡單[調味](../Page/調味.md "wikilink")。依個人喜好[川燙各種](../Page/川燙.md "wikilink")[肉類食用](../Page/肉類.md "wikilink")，並搭配其它像是[蔬菜](../Page/蔬菜.md "wikilink")、[菌菇](../Page/菌菇.md "wikilink")、[豆腐等食材](../Page/豆腐.md "wikilink")。在台灣，涮涮鍋也有著超級高的人氣，幾乎每家美食街、繁華商業區都有的地步。口味較一般中國式或者蒙古式的[火鍋為簡單](../Page/火鍋.md "wikilink")、口味也較為清淡，但是卻能選擇多種多樣的[調味料和無限吃到飽的](../Page/調味料.md "wikilink")[冰淇淋](../Page/冰淇淋.md "wikilink")。

## 源起

20世紀初期佔領[中國](../Page/中國.md "wikilink")[东北地方的](../Page/中国东北地区.md "wikilink")[日本人將當地](../Page/日本人.md "wikilink")[涮羊肉的烹飪方式帶回日本](../Page/涮羊肉.md "wikilink")，早期是涮牛肉薄片，現在已發展成涮各種肉類薄片或者海鮮。至於這個名詞乃是20世紀在[大阪一家名為](../Page/大阪.md "wikilink")的餐廳為自己所賣的這種料理命名的，並在1955年註冊為商標。

## 台式一人鍋吃到飽類型的涮涮鍋

台灣因為經過[日治時代](../Page/日治時代.md "wikilink")，並且台灣民眾及其喜愛日本食物，所以像涮涮鍋一樣的很多日本家常料理已經融入了台灣人的生活中。雖然台灣的涮涮鍋有很多號稱日本原版，但實際上已經經過了台灣人的徹底改造，擁有了許多日本涮涮鍋沒有的特點，例如：

1.  鍋既有多人份的、也有是一人份的迷你鍋，最多不超過四人，超過四人會在同一張桌子上加入兩個鍋；但日本原版的是一桌一大鍋、多人共享一鍋，無論桌子多大都還是一鍋。
2.  有[吃到飽的服務](../Page/吃到飽.md "wikilink")，吃到飽在台灣的涮涮鍋業界是絕對的主流，只有少數日本人開的涮涮鍋因為要堅持採用日本原始模式、才變得使用日本做法；這在價格昂貴的日本本土無法想像的，日本的涮涮鍋只能點一鍋、甚至連裡面的配菜的總數都已經被決定好，不能更改、有時在一些昂貴的店家甚至不能加點，要加點只能加點另一整套鍋。
3.  調味料不僅僅有日式的醬油和白蘿蔔泥，還有生雞蛋、[芝麻](../Page/芝麻.md "wikilink")、韓式辣醬、泰式辣醬、[台式醬油](../Page/台式醬油.md "wikilink")/[醬油膏](../Page/醬油膏.md "wikilink")/[九層塔](../Page/九層塔.md "wikilink")、中式[花生醬](../Page/花生醬.md "wikilink")/四川[麻辣醬](../Page/麻辣醬.md "wikilink")、美式[蕃茄醬放在公用的吧台上](../Page/蕃茄醬.md "wikilink")，可以讓顧客自由選擇、變化添加，創造出自己喜歡的口味；而真正的日式涮涮鍋只有醬油和白蘿蔔泥作為醬料。
4.  吧台上還有免費的[魚丸](../Page/魚丸.md "wikilink")、[貢丸](../Page/貢丸.md "wikilink")、[海鮮](../Page/海鮮.md "wikilink")（[墨魚](../Page/墨魚.md "wikilink")、[蝦子](../Page/蝦子.md "wikilink")、蟹腳等）、蔬菜（[金針菇](../Page/金針菇.md "wikilink")、[香菇](../Page/香菇.md "wikilink")、[高麗菜](../Page/高麗菜.md "wikilink")、[九層塔](../Page/九層塔.md "wikilink")、[地瓜葉](../Page/地瓜葉.md "wikilink")、[海帶等](../Page/海帶.md "wikilink")）、以及[豬血糕](../Page/豬血糕.md "wikilink")、台式小香腸、火鍋用臭豆腐，免費的飲料區（提供[咖啡](../Page/咖啡.md "wikilink")、[紐帶](../Page/紐帶.md "wikilink")、[奶茶](../Page/奶茶.md "wikilink")、[紅茶](../Page/紅茶.md "wikilink")、[綠茶](../Page/綠茶.md "wikilink")、[可樂](../Page/可樂.md "wikilink")、[雪碧](../Page/雪碧.md "wikilink")）和免費的冰淇淋去（一般至少有2種以上口味的冰淇淋，有些店家甚至以採用[哈根達斯](../Page/哈根達斯.md "wikilink")、[酷聖石](../Page/酷聖石.md "wikilink")、[明治](../Page/明治.md "wikilink")、[卡比索冰淇淋來吸引顧客](../Page/卡比索.md "wikilink")）以供選擇。
5.  通常不是涮涮鍋的專賣店，會和四川麻辣火鍋、泰式冬陰湯鍋、韓式辣醬鍋、日式[壽喜燒鍋都可以選擇](../Page/壽喜燒.md "wikilink")，涮涮鍋只不過是其中一個湯底的種類，顧客甚至能自我搭配出陰陽形的雙重口味火鍋、真實[賓士標誌形的三種口味火鍋](../Page/賓士.md "wikilink")。

## 備註

## 相關條目

  - [火鍋](../Page/火鍋.md "wikilink")
  - [羊肉爐](../Page/羊肉爐.md "wikilink")
  - [涮羊肉](../Page/涮羊肉.md "wikilink")
  - [臭臭鍋](../Page/臭臭鍋.md "wikilink")

[Category:火鍋](../Category/火鍋.md "wikilink")
[Category:日本食品](../Category/日本食品.md "wikilink")
[Category:牛肉料理](../Category/牛肉料理.md "wikilink")