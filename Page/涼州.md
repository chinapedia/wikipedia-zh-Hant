**涼州**，是中國古代的地名之一，為[漢代十三刺史部之一](../Page/漢朝行政區劃.md "wikilink")。其轄域在今[甘肅省及](../Page/甘肅省.md "wikilink")[寧夏全境](../Page/寧夏.md "wikilink")、[青海東北部](../Page/青海.md "wikilink")、[新疆東南部及](../Page/新疆.md "wikilink")[內蒙古](../Page/內蒙古.md "wikilink")[阿拉善盟一帶](../Page/阿拉善盟.md "wikilink")。

## 歷代沿革

### 漢代

[戰國時期](../Page/戰國時期.md "wikilink")，由於[秦國中央集權的加強](../Page/秦國.md "wikilink")，勢力迅速擴展，秦國隨即出兵滅[義渠](../Page/義渠.md "wikilink")，將義渠逐出黃河以南地區，在其地設置了隴西、北地、上郡三郡，並在邊境開始修建長城。

[秦始皇一統天下時](../Page/秦始皇.md "wikilink")，河西尚屬化外之地，自秦至漢初，河西先後雜居著[月氏](../Page/月氏.md "wikilink")、[烏孫與日後的](../Page/烏孫.md "wikilink")[匈奴等遊牧民族](../Page/匈奴.md "wikilink")。到[漢武帝時](../Page/漢武帝.md "wikilink")，河西尚為北方匈奴所領，也阻隔了[中國與](../Page/中國.md "wikilink")[西域兩大文明區之間的接觸機會](../Page/西域.md "wikilink")。為此，漢武帝尚曾派遣[張騫偷渡河西](../Page/張騫.md "wikilink")，以聯合西域各國制衡匈奴，然而張騫西行探險卻為匈奴發覺並將之囚禁長達十餘年。[西漢](../Page/西漢.md "wikilink")[元狩二年](../Page/元狩.md "wikilink")（前121年），漢武帝備妥軍力，派驃騎大將軍[霍去病遠征](../Page/霍去病.md "wikilink")[河西](../Page/河西走廊.md "wikilink")，歷經數次戰役後，擊敗[匈奴](../Page/匈奴.md "wikilink")，開辟了[河西四郡](../Page/河西四郡.md "wikilink")，即[武威](../Page/武威郡.md "wikilink")、[酒泉](../Page/酒泉郡.md "wikilink")、[張掖](../Page/張掖郡.md "wikilink")、[敦煌四郡](../Page/敦煌郡.md "wikilink")，正式納入中國版圖。[元封五年](../Page/元封.md "wikilink")（前106年），分天下為十三州，各置一刺史，史稱「[十三部刺史](../Page/西漢行政區劃.md "wikilink")」。在今甘肅省置涼州刺史部，涼州之名自此始。意為「地處西方，常寒涼也」，下轄隴西、天水、安定、北地、酒泉、張掖、敦煌、武威、金城、西海10郡，治在隴縣（今[陝西省](../Page/陝西省.md "wikilink")[隴縣](../Page/隴縣.md "wikilink")），轄域約為今[甘肅](../Page/甘肅.md "wikilink")、[寧夏](../Page/寧夏.md "wikilink")、[青海三省區](../Page/青海.md "wikilink")[湟水流域](../Page/湟水.md "wikilink")，[陝西省](../Page/陝西省.md "wikilink")[定邊](../Page/定邊.md "wikilink")、[吳旗](../Page/吳旗.md "wikilink")、[鳳縣](../Page/鳳縣.md "wikilink")、[略陽等縣和](../Page/略陽.md "wikilink")[內蒙古自治區](../Page/內蒙古自治區.md "wikilink")[額濟納旗等地](../Page/額濟納旗.md "wikilink")。

漢朝實行大量移民實邊政策，將大量的漢人移居此處，充分利用當地的肥沃綠洲，河西農業生產快速發展。歷史上有名的武威、張掖、酒泉、敦煌，也就是所謂的「河西四郡」，也在此時建立起來。經過漢朝不斷開墾，到[漢平帝時](../Page/漢平帝.md "wikilink")，據《漢書·地理志》記載，河西四郡人口總計，戶數已達7萬1270戶，人口也達28萬餘。東漢時代，河西的敦煌更是漢朝控制西域的前哨，為當時中西交通孔道的咽喉。

[更始元年](../Page/更始.md "wikilink")（23年），涼州為[隗囂所據](../Page/隗囂.md "wikilink")。更始二年，[更始帝召隗囂入朝後](../Page/更始帝.md "wikilink")，金城郡及河西四郡轉入張掖屬國都尉[竇融之手](../Page/竇融.md "wikilink")。[漢光武帝](../Page/漢光武帝.md "wikilink")[建武五年](../Page/建武.md "wikilink")（27年），竇融降漢。東漢世，增置武都、永陽、南安、安定屬國、張掖屬國(以上3郡後廢)、張掖居延屬國(即西海郡)。[漢獻帝](../Page/漢獻帝.md "wikilink")[建安初年](../Page/建安.md "wikilink")，分涼州河西的酒泉、張掖、敦煌、武威、張掖居延屬國5郡置[雍州刺史](../Page/雍州_\(古代\).md "wikilink")。建安十八年（213年）省涼州入雍州。

### 魏晉南北朝

[魏文帝](../Page/魏文帝.md "wikilink")[黃初元年](../Page/黃初.md "wikilink")（220年），分雍州河西地區的金城、武威、張掖、酒泉、敦煌、西海、西平、西郡8郡復置涼州，一直到[西晉](../Page/西晉.md "wikilink")，[姑臧均為涼州治所](../Page/姑臧.md "wikilink")，轄域僅及今甘肅省[黃河以西地區](../Page/黃河.md "wikilink")。[東晉時期的](../Page/東晉.md "wikilink")[前涼](../Page/前涼.md "wikilink")、[後涼](../Page/後涼.md "wikilink")、[南涼](../Page/南涼.md "wikilink")、[北涼及唐初的](../Page/北涼.md "wikilink")[大涼](../Page/大涼.md "wikilink")，都曾建都與此。

東漢末年[黃巾之亂以來](../Page/黃巾之亂.md "wikilink")，中原紛亂，群雄割據，百姓流離。[東晉](../Page/東晉.md "wikilink")[十六國時代](../Page/十六國.md "wikilink")，中原戰事更加頻繁，干戈不息，[劉聰](../Page/劉聰.md "wikilink")、[石勒等五胡政權相繼在中原征戰](../Page/石勒.md "wikilink")，“于时天下丧乱，秦雍之民死者十八九，唯凉州独全”，此時的涼州便成為政治上相對穩定的區域。[张轨效法](../Page/张轨.md "wikilink")[窦融](../Page/窦融.md "wikilink")，统治河西，政治稳定，成了西晋安定的后方，史稱前凉。时有民谣：“秦川中，血没腕，唯有凉州倚柱观。”中原士人便認為當時涼州刺史[張軌治下的涼州](../Page/張軌.md "wikilink")，是天下亂世中的「避難之國」，所以由中原來涼州避難的人民之多，號稱「日月相繼」。張軌更在此時順勢大興教育，建立學校。在張氏政權數代經營下，涼州總共享受了長達約六十年的和平。由於政治的穩定，加上河西在地緣上也為中西交通樞紐，外地胡商也樂於來往河西進行貿易，這些有利的內外發展條件，自然促使涼州經濟更加繁榮，文教更興。[张重华執政時期前凉版图達到頂峰](../Page/张重华.md "wikilink")。张重华死，其子[张天锡](../Page/张天锡.md "wikilink")（張軌曾孫）继位時年僅十歲，大权旁落，朝政日趋衰落。[張天錫在位時期自居為文人雅士](../Page/張天錫.md "wikilink")，四方文人向他拜師求學的頗多，張天錫同時也鼓勵[佛教宣揚](../Page/佛教.md "wikilink")，涼州亦是高僧輩出，成為佛教興盛處之一。文士如[宋纖](../Page/宋纖.md "wikilink")、[郭荷等都是當時名聲極著的學術家](../Page/郭荷.md "wikilink")。

前秦[苻坚滅前涼](../Page/苻坚.md "wikilink")，以中书令[梁熙為凉州刺史](../Page/梁熙.md "wikilink")。公元385年，[吕光至姑臧](../Page/吕光.md "wikilink")，杀梁熙，自领凉州刺史，呂光後來得知苻坚已被后秦[姚苌所杀](../Page/姚苌.md "wikilink")，遂于公元386年自立为王，史称后凉。吕光是武人出身，對於治國沒有經緯，在位期间，四处征战，穷兵黩武，大将[杜进竟为其所杀](../Page/杜进.md "wikilink")。隆安元年（397）正月，[秃发乌孤建立南凉](../Page/秃发乌孤.md "wikilink")，立即與吕光在街亭（今甘肃永登县西）展开激战，后凉以慘败收場。隆安元年（397年），吕光之弟[吕延进攻西秦](../Page/吕延.md "wikilink")，兵败被杀。吕光以败军之罪杀[沮渠罗仇](../Page/沮渠罗仇.md "wikilink")、[沮渠麹粥二人](../Page/沮渠麹粥.md "wikilink")，其侄子[沮渠蒙逊为报仇](../Page/沮渠蒙逊.md "wikilink")，起兵反抗後涼。后凉時期的河西政權动荡不安，“三寇窥窬，迭伺国隙”\[1\]，吕光死后，“诸子兢寻干戈”，[后秦](../Page/后秦.md "wikilink")、[南凉](../Page/南凉.md "wikilink")、[北凉等政權交相攻逼](../Page/北凉.md "wikilink")，国势日衰。神鼎三年（403年）北凉、南凉围后凉都城姑臧，[吕邈兵敗被杀](../Page/吕邈.md "wikilink")，吕隆不得已降于后秦。後來[鲜卑](../Page/鲜卑.md "wikilink")、[匈奴](../Page/匈奴.md "wikilink")、[羯](../Page/羯.md "wikilink")、[氐](../Page/氐.md "wikilink")、[羌等民族不断起兵割据於此](../Page/羌.md "wikilink")，有“一成、一夏、二赵、三秦、四燕、五凉”等国，史称“[十六国](../Page/十六国.md "wikilink")”。其中的“五涼政權”即為前凉、后凉、南凉、北凉和西凉。[岑参有](../Page/岑参.md "wikilink")《题金城临河驿楼》诗：“左戍依重险，高楼见五凉。”

[北魏時因地理因素](../Page/北魏.md "wikilink")，改涼州為[敦煌鎮](../Page/敦煌鎮.md "wikilink")。[太和十四年](../Page/太和.md "wikilink")（490年）復置涼州。[西魏時分涼州西部置](../Page/西魏.md "wikilink")[西涼州](../Page/西涼州.md "wikilink")。[北周時因處於西域的軍事要地中](../Page/北周.md "wikilink")，設置[涼州總管府於此](../Page/涼州總管府.md "wikilink")。北周時管有[武威郡](../Page/武威郡.md "wikilink")、[番和郡](../Page/番和郡.md "wikilink")、[廣武郡](../Page/廣武郡.md "wikilink")、[泉城郡共](../Page/泉城郡.md "wikilink")4郡。

### 隋朝以後

[隋文帝廢郡改置涼州](../Page/隋文帝.md "wikilink")，[隋煬帝時改稱武威郡](../Page/隋煬帝.md "wikilink")。轄域僅有今甘肅省永昌以東、天祝以西地區。唐代[天寶元年](../Page/天宝_\(唐朝\).md "wikilink")（742年）改[武威郡](../Page/武威郡.md "wikilink")，[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年）復為涼州。[北宋](../Page/北宋.md "wikilink")[天聖六年](../Page/天聖.md "wikilink")（1028年）改為[西涼府](../Page/西涼府.md "wikilink")。

[元代時設](../Page/元代.md "wikilink")[永昌路](../Page/永昌路.md "wikilink")。[明](../Page/明.md "wikilink")[清稱](../Page/清.md "wikilink")[涼州府](../Page/涼州府.md "wikilink")。[民國初](../Page/民國.md "wikilink")，置[河西道](../Page/河西道.md "wikilink")（後改稱[甘涼道](../Page/甘涼道.md "wikilink")）。

## 歷代刺史

### 東漢

  - [耿鄙](../Page/耿鄙.md "wikilink")，[中平元年](../Page/中平.md "wikilink")（184年）十一月，[北宮伯玉與](../Page/北宮伯玉.md "wikilink")[先零羌起兵反](../Page/先零羌.md "wikilink")[漢時任涼州刺史](../Page/漢朝.md "wikilink")。
  - [种劭](../Page/种劭.md "wikilink")，[董卓既擅權](../Page/董卓.md "wikilink")，而惡種劭彊力，遂左轉[議郎](../Page/議郎.md "wikilink")，出為[益](../Page/益州.md "wikilink")、涼二州刺史。（《後漢書·張王种陳列傳》）
  - [韋端](../Page/韋端.md "wikilink")，[馬騰](../Page/馬騰.md "wikilink")、[韓遂互鬥](../Page/韓遂.md "wikilink")。[建安初年](../Page/建安.md "wikilink")，[曹操使](../Page/曹操.md "wikilink")[司隸校尉](../Page/司隸校尉.md "wikilink")[鍾繇](../Page/鍾繇.md "wikilink")、涼州牧韋端和解。
  - [韋康](../Page/韋康.md "wikilink")，韋端從涼州牧徵為太僕，韋康代為涼州刺史，當時人榮之。

### 三國

  - 曹魏

<!-- end list -->

  - [鄒岐](../Page/鄒岐.md "wikilink")，[黃初元年](../Page/黃初.md "wikilink")（220年）以安定太守遷涼州刺史。（《魏志·張既傳》）
  - [張既](../Page/張既.md "wikilink")，黃初元年（220年）七月繼鄒岐後任，四年（223年）卒。（《魏志·張既傳》）
  - [溫恢](../Page/溫恢.md "wikilink")，黃初四年（223年）繼張既後任，死於道中。（《魏志·溫恢傳》）
  - [孟建](../Page/孟建.md "wikilink")，黃初四年（223年）繼溫恢後任。（《魏志·溫恢傳》）
  - [徐邈](../Page/徐邈.md "wikilink")，約[太和元年](../Page/太和_\(曹魏\).md "wikilink")（227年）至二年（228年）間繼孟建後任，在任10餘年。[正始元年](../Page/正始_\(曹魏\).md "wikilink")（240年）遷大司農。（《魏志·徐邈傳》）
  - [王渾](../Page/王渾_\(王戎之父\).md "wikilink")，約正始年間。（《魏志·崔林傳》、《晉書·王戎傳》）
  - [李憙](../Page/李憙.md "wikilink")，約[甘露年間](../Page/甘露_\(曹魏\).md "wikilink")（256年－260年）。《晉書·李憙傳》

<!-- end list -->

  - 蜀漢

<!-- end list -->

  - [馬超](../Page/馬超.md "wikilink")，[章武元年](../Page/章武.md "wikilink")（221年）至二年（222年），卒於任內。（《蜀志·馬超傳》）
  - [魏延](../Page/魏延.md "wikilink")，[建興五年](../Page/建兴_\(蜀汉\).md "wikilink")（227年）至十二年（234年），卒於任內。（《蜀志·魏延傳》）
  - [姜維](../Page/姜維.md "wikilink")，[延熙六年](../Page/延熙_\(蜀汉\).md "wikilink")（243年）領。（《蜀志·姜維傳》）

### 西晉

  - [牽弘](../Page/牽弘.md "wikilink")，[泰始六年](../Page/泰始_\(西晋\).md "wikilink")（270年）任，七年（271年）死於邊事。（《魏志·牽招傳》、《晉書·陳騫傳》，《資治通鑑》卷78、卷79）
  - [胡喜](../Page/胡喜.md "wikilink")（《魏志·鍾會傳》注引《晉諸公贊》）
  - [蘇愉](../Page/蘇愉.md "wikilink")，泰始年間見在任，為禿髪樹機能所敗。（《晉書·禿髮烏孤載記》）
  - [張軌](../Page/張軌.md "wikilink")，[永寧元年](../Page/永宁_\(西晋\).md "wikilink")（301年）至[建興二年](../Page/建兴_\(西晋\).md "wikilink")（314年）在任，死於任內。（《晉書·張軌傳》）
  - [張寔](../Page/張寔.md "wikilink")，建興二年（314年）任，終西晉世。（《晉書·張軌傳》）

## 参考文献

### 引用

### 来源

  - 嚴耕望：《兩漢太守刺史表》，台北：台灣商務印書館
  - 信弦：〈一個歷史上的分合勢力－魏晉十六國時期的河西與中原〉，《古代史事》

## 参见

  - [涼州區](../Page/涼州區.md "wikilink")
  - [敦煌](../Page/敦煌.md "wikilink")
  - [玉門關](../Page/玉門關.md "wikilink")
  - [西域](../Page/西域.md "wikilink")
  - [絲路](../Page/絲路.md "wikilink")
  - [河西走廊](../Page/河西走廊.md "wikilink")
  - [祁連山脈](../Page/祁連山脈.md "wikilink")

{{-}}

[Category:五胡十六国的州](../Category/五胡十六国的州.md "wikilink")
[Category:北朝的州](../Category/北朝的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:甘肃的州](../Category/甘肃的州.md "wikilink")
[Category:宁夏的州](../Category/宁夏的州.md "wikilink")
[Category:青海的州](../Category/青海的州.md "wikilink")
[Category:新疆的州](../Category/新疆的州.md "wikilink")
[Category:内蒙古的州](../Category/内蒙古的州.md "wikilink")
[Category:1020年代废除的行政区划](../Category/1020年代废除的行政区划.md "wikilink")

1.  《晋书·吕光载记》