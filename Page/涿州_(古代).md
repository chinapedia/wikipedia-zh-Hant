**涿州**，[唐朝时设置的](../Page/唐朝.md "wikilink")[州](../Page/州.md "wikilink")。

[大历四年](../Page/大曆.md "wikilink")（769年）析[幽州的](../Page/幽州_\(十六国\).md "wikilink")[范阳县](../Page/范阳县.md "wikilink")、[归义县](../Page/归义县_\(唐朝\).md "wikilink")、[固安县置](../Page/固安县.md "wikilink")，[治所在范阳县](../Page/治所.md "wikilink")（即今[河北省](../Page/河北省.md "wikilink")[涿州市](../Page/涿州市.md "wikilink")）。下领五县：范阳县、归义县、固安县、[新昌县](../Page/新昌县_\(唐朝\).md "wikilink")、[新城县](../Page/新城县_\(唐朝\).md "wikilink")\[1\]。辖境相当今河北省涿州市、[雄县及](../Page/雄县.md "wikilink")[固安等地](../Page/固安县.md "wikilink")。

[五代时](../Page/五代.md "wikilink")，为[后晋割让予](../Page/后晋.md "wikilink")[契丹的](../Page/辽朝.md "wikilink")[燕云十六州之一](../Page/燕雲十六州.md "wikilink")。[北宋末年](../Page/北宋.md "wikilink")，[北宋政府曾短暂收回](../Page/北宋政府.md "wikilink")，复又被[金国所占](../Page/金朝.md "wikilink")。[元太宗八年](../Page/元太宗.md "wikilink")（1236年）升为[涿州路](../Page/涿州路.md "wikilink")，[中统四年](../Page/中統_\(年號\).md "wikilink")（1263年）复降为涿州。[明朝](../Page/明朝.md "wikilink")[洪武初年](../Page/洪武.md "wikilink")，省范阳县入州，仅领一县：[房山县](../Page/房山县.md "wikilink")，属[顺天府](../Page/顺天府.md "wikilink")\[2\]。民国初年，全国[废府州厅改县](../Page/废府州厅改县.md "wikilink")，于1913年降为[涿县](../Page/涿县.md "wikilink")。

## 注释

## 相关

  - [燕雲十六州](../Page/燕雲十六州.md "wikilink")

[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:后梁的州](../Category/后梁的州.md "wikilink")
[Category:后唐的州](../Category/后唐的州.md "wikilink")
[Category:后晋的州](../Category/后晋的州.md "wikilink")
[Category:辽朝的州](../Category/辽朝的州.md "wikilink")
[Category:北宋的州](../Category/北宋的州.md "wikilink")
[Category:金朝的州](../Category/金朝的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:明朝的州](../Category/明朝的州.md "wikilink")
[Category:清朝的州](../Category/清朝的州.md "wikilink")
[Category:河北的州](../Category/河北的州.md "wikilink")
[Category:保定行政区划史](../Category/保定行政区划史.md "wikilink")
[Category:廊坊行政区划史](../Category/廊坊行政区划史.md "wikilink")
[Category:760年代建立的行政区划](../Category/760年代建立的行政区划.md "wikilink")
[Category:1913年废除的行政区划](../Category/1913年废除的行政区划.md "wikilink")

1.  《[新唐书](../Page/新唐书.md "wikilink")·志第二十九·地理三》幽州范阳郡......涿州，上。大历四年，节度使[朱希彩表析幽州之范阳](../Page/朱希彩.md "wikilink")、归义、固安置。县五：范阳，望。本涿，武德七年更名。归义，上。武德五年置，贞观元年省，八年复置。景云二年隶鄚州，是年，还隶幽州。固安，上。新昌，上。大历四年析固安置。新城。上。大和六年以故督亢地置。......瀛州河间郡......
2.  《[明史](../Page/明史.md "wikilink")·志第十六·地理一》顺天府......涿州
    洪武初以州治范阳县省入。西有独鹿山。北有涿水，西北有挟河，合焉。南有范水。东北距府百四十里。领县一。房山......