**淘宝网**是[中国](../Page/中国.md "wikilink")[阿里巴巴集团旗下](../Page/阿里巴巴集团.md "wikilink")[网络购物网站](../Page/网络购物.md "wikilink")，由[马云创立于](../Page/马云.md "wikilink")2003年5月10日，是面向[中国大陆](../Page/中国大陆.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳门](../Page/澳门.md "wikilink")、[台湾的大中華地區消费者與](../Page/台湾.md "wikilink")[马来西亚之](../Page/马来西亚.md "wikilink")[C2C购物网站](../Page/C2C.md "wikilink")（[B2C模式网站](../Page/B2C.md "wikilink")——天猫已拆出），个人或企业均可在淘宝网开设自己的网络[店铺](../Page/店铺.md "wikilink")，此外淘宝网上还拥有[拍卖平台](../Page/拍卖.md "wikilink")。2011年6月16日，网站分拆为淘宝网、[天猫](../Page/天猫.md "wikilink")、[一淘网](../Page/一淘网.md "wikilink")。

## 网站历史

早在2003年4月份，[马云就与](../Page/马云.md "wikilink")10名员工在[杭州筹划淘宝网的建设](../Page/杭州.md "wikilink")，2003年5月10日，淘宝网正式成立，淘宝刚上线初到2003年7月4日，网站快速发展，日新增商品达2000多件\[1\]，2003年8月，淘宝宣布免费3年，吸引了大量的中小型卖家入驻\[2\]。2年时间内，淘宝网成为中国国内网络购物市场的第一名，占据了中国网络购物70％左右的市场份额\[3\]。2005年10月，淘宝宣布“继续免费3年”，这使得已经稳定收费的[EBay退出中国市场](../Page/EBay.md "wikilink")\[4\]。2006年淘宝成交额达169亿，超过2005年中国网购整体市场总量，注册会员超3000万人\[5\]。2007年淘宝拥有800万件在线商品和700余万注册用户，网站日浏览量超过9000万\[6\]。2010年9月10日，淘宝推出了全新的虚拟形象“淘公仔”，替换了使用了7年的“淘蚂蚁”\[7\]。2010年12月31日，淘宝网注册会员超过3.7亿人，占中国网购市场80%的份额\[8\]。2012年淘宝网和[天猫网的交易额超过了](../Page/天猫网.md "wikilink")1万亿[人民币](../Page/人民币.md "wikilink")\[9\]，超过[亞馬遜公司和](../Page/亞馬遜公司.md "wikilink")[eBay之和](../Page/eBay.md "wikilink")。阿里巴巴中国零售交易市场2016财年商品交易实时总额(GMV)突破3万亿元人民币（在2012年时达到平台GMV
1万亿元）\[10\]。英国《[经济学人](../Page/经济学人.md "wikilink")》杂志曾称其为“中国最大的线上集市”\[11\]。

2011年6月16日，[阿里巴巴集团宣布旗下淘宝公司将分拆为三个独立的公司](../Page/阿里巴巴集团.md "wikilink")——沿袭原C2C业务的淘宝网（taobao.com）、平台型B2C电子商务服务商淘宝商城（tmall.com，後來的[天貓](../Page/天貓.md "wikilink")）和一站式购物搜索引擎[一淘网](../Page/一淘网.md "wikilink")（etao.com）\[12\]。[马云说](../Page/马云.md "wikilink")：“我们相信淘宝分拆能创造更大的产业价值、公司价值和[股东利益](../Page/股东.md "wikilink")。”2014年，[阿里巴巴集团在](../Page/阿里巴巴集团.md "wikilink")[美国上市](../Page/美国.md "wikilink")\[13\]。

## 子项目和姐妹项目

  - **购物杂志：**2009年9月10日，淘宝网创建了购物杂志《**淘宝天下**》\[14\]，两个月内发行量达每期28.3万册\[15\]。
  - **电视购物：**2009年12月，淘宝与3家公司合作推出植入淘宝购物系统应用的手机并发展[电视淘宝购物](../Page/电视购物.md "wikilink")\[16\]，并和湖南卫视合作组建“**快乐淘宝**”公司，2010年4月，淘宝在[湖南卫视推出](../Page/湖南卫视.md "wikilink")“[快乐淘宝](../Page/快乐淘宝.md "wikilink")”节目\[17\]。
  - **团购：**2011年10月21日，淘宝网旗下的团购平台**[聚划算](../Page/聚划算.md "wikilink")**也独立出来\[18\]。
  - **购物搜索：一淘网**2011年6月16日分拆而出，是一个购物搜索网站，但其搜索结果会偏向淘宝网和天猫。

### 天猫

[Tmall-logo_2.png](https://zh.wikipedia.org/wiki/File:Tmall-logo_2.png "fig:Tmall-logo_2.png")
2010年11月3日，淘宝旗下B2C平台淘宝商城使用独立域名“www.tmall.com”从淘宝分离出来，进入B2C市场\[19\]。

2011年10月11日，因阿里巴巴集团宣布从2012年1月1日起，将淘宝商城入驻卖家的技术服务年费由6000元提高至3万元和6万元两个档次、保证金从1万元提高到5万元、10万元和15万元三个档次\[20\]，数千中小卖家对大卖家进行攻击，手段主要包括恶意拍货、给差评、恶意选择货到付款和申请退款。淘宝斥其为“网络[恐怖主义](../Page/恐怖主义.md "wikilink")”。参与人数最高达5万，有人称数万淘宝“[蚂蚁](../Page/蚂蚁.md "wikilink")”秒杀“[大象](../Page/大象.md "wikilink")”\[21\]。淘宝商城方面称：“攻击者里不乏大量炒作信用集团、职业差评师等存在，他们通过各种手段欺骗用户入群。\[22\]”随后淘宝网针对新规则发表了声明对规则一一回应。10月14日攻击停止\[23\]。[马云在](../Page/马云.md "wikilink")[微博称](../Page/微博.md "wikilink")“心累”\[24\]。\[25\]\[26\]\[27\]\[28\]\[29\][网易评论作家郭建龙分析认为这是淘宝过度竞争引起的](../Page/网易.md "wikilink")\[30\]。10月17日下午，淘宝作出让步，并宣布将投入18亿元扶持中小卖家，但是拒绝谈判\[31\]。10月21日，攻击者们还发起了[挤兑](../Page/挤兑.md "wikilink")[支付宝事件](../Page/支付宝.md "wikilink")\[32\]。

2012年1月11日上午，淘宝商城正式宣布更名为“天猫”，3月29日发布全新Logo形象\[33\]。

### 闲鱼

域名是2.taobao.com。司法拍賣也屬於闲魚\[34\]。

## 购物与售卖

使用淘寶網購物需要註冊用戶，在賬戶安全的狀態下，用戶可以聯繫客服自主[註銷](../Page/登出.md "wikilink")。如果有過購物，那麼註銷會導致失去保障。

### 查找和推送商品

2006年淘宝准备推出搜索竞价排名的“招财进宝”计划，但遭商家反对，且[腾讯旗下](../Page/腾讯.md "wikilink")[拍拍网宣布至少三年免费](../Page/拍拍网.md "wikilink")，计划被迫流产\[35\]。2010年7月8日，淘宝使用新的商品排名方式重新调整商品排名，因素包括成交量、收藏人数、卖家信誉、好评率、浏览量、宝贝下架时间\[36\]。淘宝网上会展示商品的图片，展示图和实物差距最大的是衣服\[37\]。只有[天猫卖家才能用](../Page/天猫.md "wikilink")“旗舰”、“专卖”，个人店铺名称不能使用会让用户混淆的词汇，例如特许经营、总经销、总代理、加盟\[38\]。2012年4月20日，《[IT时代周刊](../Page/IT时代周刊.md "wikilink")》发表的文章称：“中国知名电商淘宝内部员工存在集体性腐败行为。\[39\]”阿里巴巴网络安全部总监称：“淘宝是个社会，不可能只有好人。\[40\]”

### 开设网店

淘宝卖家主要为中小型企业\[41\]，淘宝不卖任何东西\[42\]，在淘宝上的卖家主要分为两类：卖能够在众多商品中脱颖而出的特色商品或特卖特价商品\[43\]。在淘宝开设网店不需要[实名制](../Page/实名制.md "wikilink")，但需要绑定[银行卡](../Page/银行卡.md "wikilink")，一名会员只能开设一个店铺，店铺无法转让\[44\]\[45\]，但可司法继承\[46\]。除了实体商品外，淘宝上还可以售卖虚拟物品和一些特别的服务，例如[微博粉丝](../Page/微博.md "wikilink")\[47\]，几乎什么都能卖\[48\]，但对部分危险物品仍有限制，2014年3月，淘宝删除了部分危险[刀具](../Page/刀具.md "wikilink")，西瓜刀、长刀、[斩马刀等不能正常搜索展示](../Page/斩马刀.md "wikilink")\[49\]，4月1日，在淘宝购买危险性刀具必须实名认证\[50\]。然而自2016年4月起，具有强烈刺激性气味且内部气压较大的[瑞典鹽醃鯡魚也在淘宝售卖](../Page/瑞典鹽醃鯡魚.md "wikilink")。\[51\]

淘宝允许卖家使用淘宝提供的计算机和网络技术设计个性化店铺“旺铺”\[52\]，也可以和企业自己的[网站进行对接](../Page/网站.md "wikilink")，如果企业没有能力自己建立网站，淘宝可以为其创建，但在网站首页上会有“Power
by
Taobao”这一标语\[53\]。淘宝网发布的《网购市场发展报告》显示，淘宝卖家44%每月收入1000-2000元，16%的卖家月收入在1000元左右\[54\]。在淘宝平台就业者中，28.1%在淘宝上获得的收入高于当地[最低工资水平](../Page/最低工资.md "wikilink")，47.8%将淘宝作为唯一收入来源\[55\]。据调查，5年以上的网店平均雇佣员工为3.4人\[56\]。2011年5月24日，淘宝开放“我的淘宝（i.taobao.com）”为淘宝的第三方插件平台，淘江湖融入“我的淘宝”之中，不再作为独立的应用，[网址跳转至](../Page/网址.md "wikilink")“我的淘宝”，同时新增了管理客户功能\[57\]。

### 商家与顾客交流

淘宝网的即时聊天工具是阿里旺旺，该[软件可以实时的保存双方的聊天记录](../Page/软件.md "wikilink")，淘宝网支持阿里旺旺的聊天记录可以作为交易出现纠纷时的证据使用\[58\]。2013年1月29日，淘宝网推出[千牛用以替代阿里旺旺卖家版](../Page/千牛.md "wikilink")，千牛是专为淘宝网卖家提供的集各种辅助交易插件聊天应用\[59\]。为了对付腾讯[微信](../Page/微信.md "wikilink")，淘宝还推出了[微淘](../Page/微淘.md "wikilink")，通过微淘建立商家与用户之间的粉丝关系\[60\]。

### 交易付款

淘宝网上线之初的大部分的交易都是同城进行，即“一手交钱，一手交货”，也有一些交易采取银行间转账的方式。而为了降低用户上当受骗的风险，淘宝网鼓励“线上下单，线下成交”的方式，但这一交易方式有很大的局限性\[61\]。为了解决用户间的资金安全，并在买家和卖家之间建立互相信任的关系，淘宝团队开发出支付工具[支付宝](../Page/支付宝.md "wikilink")，目前为淘宝交易中使用最普遍的支付应用\[62\]，买家需要先将钱转账给支付宝来给支付宝充值，在淘宝购买商品时付款后钱款会由支付宝冻结扣留，不会立刻支付给卖家，等待卖家货物运达后买家验货并确认收货后才会将钱款打入卖家账户\[63\]。在淘宝上转账不需要手续费，因此有父母将生活费通过淘宝交易转给孩子\[64\]。由于是[网络购物](../Page/网络购物.md "wikilink")，所以不会开具[发票](../Page/发票.md "wikilink")\[65\]。

### 售后评价

淘宝网倡导“以买卖双方互评为基础的诚信机制”\[66\]，信用评级分为“好评”、“中评”、“差评”三种类型。另外买家还可以对物流公司、卖家发货速度、商品与卖家描述相符程度三项指标进行1—5星级的不记名评价，买家可以选择不做评价、做出全部评价或部分评价，卖家获得的评分将在网上店铺中显示，此评分会影响卖家出售的商品在搜索列表中的排序。但这一机制会被反向利用，并形成了炒信黑色产业链\[67\]。[中国政法大学知识产权研究中心研究员张樊表示](../Page/中国政法大学.md "wikilink")：“如果卖家在信用上作弊，就有违反《消费者权益保护法》，欺诈消费者的嫌疑。\[68\]”

虚拟、成人和茶叶这三个类目商品的评价是强制显示匿名评价。\[69\]

### 消费者保障

运送中最容易受损的商品为图书和牛奶\[70\]。据报道，只要是实名的举报和投诉，淘宝都会受理\[71\]。卖家会作出一些保障承诺\[72\]。如果发生交易纠纷，双方可提交快递单复印件和商品照片等交易证明，由淘宝网仲裁是否将钱款打入卖家账户或者退还给买家\[73\]。由于是快递送货，所以需要填写收货地址，如果发生纠纷，买家可能会收到对方寄来的[刀片](../Page/刀片.md "wikilink")、[灵位](../Page/灵位.md "wikilink")\[74\]或者[冥币](../Page/冥币.md "wikilink")\[75\]。

### 假冒伪劣商品

2010年2月，淘宝投入1亿并承诺如果买家买到假货淘宝网会先行赔付，3月8日，淘宝网发起建立的消费者维权平台上线\[76\]。2010年淘宝共处理侵权商品1400万件，处罚不同程度侵权行为的会员59万次\[77\]。2011年2月28日，因假货和盗版盛行，[美国贸易代表办公室](../Page/美国贸易代表办公室.md "wikilink")（USTR）将淘宝和[百度列入](../Page/百度.md "wikilink")“[恶名市场](../Page/恶名市场.md "wikilink")”\[78\]。2011年4月21日，[中国官方下发了](../Page/中国.md "wikilink")《关于进一步推进网络购物领域打击侵犯知识产权和制售假冒伪劣商品行动的通知》，明确了淘宝等网购平台的责任：要承担市场主办方责任，要“采取技术手段屏蔽侵犯知识产权和制售假冒伪劣商品信息，建立24小时网上巡查制度，及时处理违法违规行为”\[79\]。[焦点访谈称其知假售假](../Page/焦点访谈.md "wikilink")\[80\]。2011年上半年，淘宝网建立了商品抽检制度，并处理侵犯知识产权商品信息4700万条，处罚卖家24万余人次，接受投诉处理的商品信息为157万余件\[81\]。淘宝有2000多名员工处理消费者维权，占公司员工总数的一半\[82\]，淘宝还曾在一个星期关闭了18000多家售假店\[83\]。为从“恶名市场”脱身，淘宝网删除了8200万件商品，货值1800亿元\[84\]。2012年12月，淘宝网从“恶名市场”名单中除名\[85\]。

2014年8月到10月，中国[国家工商行政管理总局对网上所销售的商品进行了抽查并于](../Page/国家工商行政管理总局.md "wikilink")2015年1月23日公布了结果，结果显示淘宝网的合格率仅为37.25%\[86\]。2015年1月27日，淘宝网官方微博发布《一个80后淘宝网运营小二心声——[刘红亮司长](../Page/刘红亮.md "wikilink")：您违规了，别吹黑哨！》一文回应工商总局的监测结果，称“抽检程序违规”\[87\]。1月27日晚，国家工商总局新闻发言人表示“抽检合法合规”\[88\]，1月28日，国家工商总局公开了2014年《关于对阿里巴巴集团进行行政指导工作情况的[白皮书](../Page/白皮书.md "wikilink")》，指出阿里系网络交易平台存在的五大突出问题，包括主体准入把关不严、对商品信息审查不力、销售行为管理混乱、信用评价存有缺陷、内部工作人员管控不严\[89\]。随后淘宝官方微博发表官方声明认为刘红亮司长在监管过程中存在程序失当、情绪[执法的行为](../Page/执法.md "wikilink")，用错误的方式得到一个不客观的结论，对淘宝以及中国电子商务从业者造成了非常严重的负面影响，并决定向[国家工商总局投诉](../Page/中华人民共和国国家工商行政管理总局.md "wikilink")\[90\]。

2016年10月，对淘宝假货问题不满的申请将淘宝网重新列入美国贸易代表办公室的“恶名市场”名单\[91\]。同年12月，美国贸易代表办公室发布2016年恶名市场名单，淘宝网时隔4年再度上榜。为此，阿里巴巴集团表示“非常失望”，称美国贸易代表办公室的决定忽略了阿里巴巴为打击假货所做的切实工作\[92\]。[中华人民共和国外交部发言人](../Page/中华人民共和国外交部发言人.md "wikilink")[华春莹也就该事件表示](../Page/华春莹.md "wikilink")：希望双方通过友好协商妥善处理双方经贸关系中出现的问题，为彼此企业开展正常互利合作提供公平公正的环境\[93\]。

2018年1月，淘宝网再度登上美国贸易代表办公室年度“恶名市场”黑名单。对此阿里巴巴集团发表声明表示“基于[贸易保护主义抬头](../Page/贸易保护主义.md "wikilink")，阿里巴巴再次成为美国贸易代表办公室高度政治化环境下的牺牲品。美国贸易代表办公室的恶名市场名单仅仅针对非美国国家，它已不是关乎知识产权保护，而是实现美国政府贸易政策目标的工具，其真实意图终将为人所知\[94\]。”

### 拍卖平台

2012年6月26日，浙江省[高级人民法院和淘宝网联合推出的司法拍卖平台正式上线](../Page/高级人民法院.md "wikilink")\[95\]。部分[法院将淘宝作为司法拍卖平台](../Page/法院.md "wikilink")\[96\]，在淘宝上进行司法拍卖不需要收取佣金，透明度高\[97\]。网络拍卖可以有效降低串标和暴力制止其他买家出价的行为\[98\]。[南通市](../Page/南通市.md "wikilink")[中级人民法院公布自](../Page/中级人民法院.md "wikilink")2014年年初出台《关于司法公开建设的实施方案》规定拍卖应当通过淘宝到2016年1月1日以来，全市法院共上传拍品1175宗，成交476宗，成交额达11.1余亿元，节约佣金约3900余万\[99\]。另外淘宝网还上线了“不良资产处置平台”拍卖专场\[100\]。截至2014年8月13日，淘宝上超过500家法院，10余家产权交易所，200家国内外拍卖机构、画廊、艺术商店入驻淘宝拍卖平台\[101\]。

## 影响

### 经济发展

[淘宝大学_20150122_102134.jpg](https://zh.wikipedia.org/wiki/File:淘宝大学_20150122_102134.jpg "fig:淘宝大学_20150122_102134.jpg")淘宝网提供了大量创业的机会，为大量中小型企业和个人减少了创业成本\[102\]，截止2011年年底，淘宝创造了270万就业机会\[103\]。在[山东省](../Page/山东省.md "wikilink")[滨州市](../Page/滨州市.md "wikilink")[博兴县](../Page/博兴县.md "wikilink")[湾头村](../Page/湾头村.md "wikilink")，有1617多户\[104\]，500多户居民在淘宝开设网店，根据阿里巴巴集团的调查，在中国有14个类似于湾头村的“淘宝村”，其网店达到1万家，拉动就业人数4万人\[105\]。

### 购物节

2005年成交金额破80亿元，超越沃尔玛。 2007年，淘宝的交易额实现了433亿元，比2006年增长156%。
2008年上半年，淘宝成交额就已达到413亿元。 2009年全年交易额达到2083亿人民币。
2009年双11，淘宝+天猫销售额0.5亿元。 2010年双11，淘宝+天猫销售额9.36亿元。
2011年双11，天猫双11销售额33.6亿元，淘宝和天猫共52亿。 \[10\]
2012年双11，截至上午8点16分，开始8个小时的天猫双11购物节，支付宝交易额已经达到50亿元，接近2011年双11全天淘宝和天猫的总交易额。双11全天，淘宝天猫平台交易金额已经达到191亿元。
2013年双11，开场仅1分钟成交的订单数量达到33.9万笔，总成交金额达到1.17亿元。第二分钟，成交数字突破3.7亿元。到了零时6分7秒，成交额直接冲上10亿元。截至11日24时，“双11”天猫及淘宝的总成交额破300亿元，达350.19亿元。
2014年双11，淘宝+天猫成交额再次刷新记录，达到571亿元。 2015年双11，淘宝+天猫成交额再次刷新记录，达到912亿元 \[11\]
。 2016年双11，淘宝+天猫成交额再次刷新记录，达到1207亿元，15个小时天猫销售额达到去年912亿元的销售总额，线上占比为82%。
2017年双11，淘宝+天猫成交额再次刷新记录，达到1682亿元，无线成交额占比90%。全球消费者通过支付宝完成的支付总笔数达14.8亿笔，比2016年增长41%。截至24点，全球有225个国家和地区加入2017天猫双11全球狂欢节。
\[10\]

### 公益事业

有超过3万名[残疾人在淘宝平台创业就业](../Page/残疾人.md "wikilink")\[106\]。2010年10月，淘宝开通残疾人创业公益通道，审核通过的店铺可以享受免费标准版旺铺、网上创业培训、淘宝卖家工具等优惠\[107\]。2011年5月23日，淘宝网与杭州市残联合共对175位报名的残疾人进行[打字速度和对淘宝网的了解程度的测试](../Page/打字.md "wikilink")，通过测试可以免费参加“云客服”培训课程。通过考试的人可以成为淘宝的“云客服”\[108\]。

## 来源

## 参见

  - [支付宝](../Page/支付宝.md "wikilink")、[菜鸟网络](../Page/菜鸟网络.md "wikilink")、[一淘网](../Page/一淘网.md "wikilink")
  - [中華人民共和國電子商務法](../Page/中華人民共和國電子商務法.md "wikilink")

## 外部链接

  - [淘宝网](https://www.taobao.com)
  - [天猫](https://www.tmall.com)（淘宝旗下网站）

[Category:阿里巴巴集团](../Category/阿里巴巴集团.md "wikilink")
[Category:中国民營企業](../Category/中国民營企業.md "wikilink")
[Category:购物网站](../Category/购物网站.md "wikilink")
[Category:中国网站](../Category/中国网站.md "wikilink")
[Category:香港網絡購物平台](../Category/香港網絡購物平台.md "wikilink")
[Category:2003年建立的网站](../Category/2003年建立的网站.md "wikilink")

1.

2.

3.

4.
5.

6.
7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.
26.
27.
28.
29.

30.

31.

32.

33.

34.

35.

36.

37.
38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.
53.

54.
55.
56.
57.

58.
59.

60.

61.

62.

63.

64.

65.

66.

67.

68.
69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.
81.

82.

83.

84.

85.
86.

87.

88.
89.
90.

91.

92.

93.

94.

95.

96.

97.

98.

99.

100.

101.

102.

103.
104.

105.

106.

107.
108.