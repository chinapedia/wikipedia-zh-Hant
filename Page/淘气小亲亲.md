《**淘氣小親親**》（），是[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")[多田薰的一部](../Page/多田薰.md "wikilink")[愛情](../Page/愛情.md "wikilink")[校園](../Page/校園.md "wikilink")[漫畫](../Page/日本漫畫.md "wikilink")，是作者的巅峰之作，被誉为“恋爱圣经”。1990年於別冊《Margaret》開始連載，漫画共23卷，累计发售量达2700万\[1\]。台灣中文版由[東立出版社代理](../Page/東立出版社.md "wikilink")，單行本全23卷已發售。作品靈感來自作者與丈夫相識、相戀及相處的點點滴滴，但由於作者在1999年3月11日的執筆途中辭世，因此作品沒有完結。

## 劇情內容

在高中的入學典禮中，一年F班的相原琴子第一眼看到一年A班的入江直樹，便喜歡上他。當鼓起勇氣，寫情書告白卻換回「我討厭笨女人」的無情答案。打算從此忘記直樹的琴子，有一天新建的自家卻因為地震震垮了，於是只好寄住在父親重雄學生時代的友人家。沒想到，父親友人的家即是直樹的家……

## 主要角色

聲優標示為廣播劇CD／動畫版／香港配音員／台灣配音員。

### 入江、相原家

  - 入江直樹（，聲優：[浪川大輔](../Page/浪川大輔.md "wikilink")／[平川大輔](../Page/平川大輔.md "wikilink")／[黃榮璋](../Page/黃榮璋.md "wikilink")（香港））
    1972年11月12日出生，血型A型，身高178cm。IQ200的超級天才，長得很帥而且運動萬能。個性既冷酷又壞心，在漫畫中直樹的媽媽都叫他「哥哥」。心裡其實都一直都很在意琴子，卻一直沒發現，不過最後發現自己其實會吃靠近琴子男人的醋。
  - 相原琴子（，聲優：[水樹奈奈](../Page/水樹奈奈.md "wikilink")（動畫）／[張頌欣](../Page/張頌欣.md "wikilink")／[楊凱凱](../Page/楊凱凱.md "wikilink")）
    1972年9月28日出生，血型O型，身高158cm。被分在偏差値最低的一年F班（放牛班）。雖然直樹冷漠地對待她，但她仍然不氣餒地繼續使出攻勢，多虧直樹的教導，總算能夠進入[斗南大學](../Page/斗南大學.md "wikilink")。非常喜歡直樹，但總是得不到自己所希望的結果，笨手笨腳，什麼事情都需要直樹的幫忙。單行本第10集、動畫第14集和入江直樹結婚，改姓入江，動畫中最後產下一個女孩。
  - 相原重雄（，聲優：[相澤正輝](../Page/相澤正輝.md "wikilink")／[島田敏](../Page/島田敏.md "wikilink")／[古明華](../Page/古明華.md "wikilink")／[林谷珍](../Page/林谷珍.md "wikilink")）
    琴子父親，料理屋「福吉」的主廚兼店長，江戶子（指在[東京土生土長的在地人](../Page/東京.md "wikilink")），通稱「阿相」。
  - 相原悅子（
    琴子母親，已逝。長相與琴子十分相似。
  - 入江裕樹（，聲優：[田中真弓](../Page/田中真弓.md "wikilink")／[朴璐美](../Page/朴璐美.md "wikilink")／[伍秀霞](../Page/伍秀霞.md "wikilink")（香港））
    血型A型。直樹弟弟，對直樹相當尊敬，對欺負琴子感到有趣的小孩。
  - 入江紀子（，聲優：[堀越真己](../Page/堀越真己.md "wikilink")／[松井菜櫻子](../Page/松井菜櫻子.md "wikilink")／[蔡惠萍](../Page/蔡惠萍.md "wikilink")／[李明幸](../Page/李明幸.md "wikilink")）
    佐賀縣太良町出身，血型A型。直樹母親，琴子的愛情後援隊。
  - 入江重樹（，聲優：[龍田直樹](../Page/龍田直樹.md "wikilink")／[長嶝高士](../Page/長嶝高士.md "wikilink")／[陳永信](../Page/陳永信.md "wikilink")（香港））
    佐賀縣出身，血型O型。直樹父親，公司「潘達」社長，通稱「阿入」。
  - 入江琴美（，聲優：[金田朋子](../Page/金田朋子.md "wikilink")）
    直樹和琴子的女兒。
  - 入江理加（）
    直樹的堂妹/裕樹的堂姊，漂亮又聰明，從小就欣賞直樹，在美國生活五年後歸國。
  - 小小（）
    入江家的愛犬。

### 其他角色

  - 池澤金之助（，聲優：[阪口候一](../Page/阪口候一.md "wikilink")／[阪口周平](../Page/阪口周平.md "wikilink")／[何承駿](../Page/何承駿.md "wikilink")／[孫誠](../Page/孫誠.md "wikilink")）
    12月9日生。琴子好友兼同班同學，衝動型，單戀琴子，視直樹為情敵。在動畫版中因為向琴子求婚被拒絕而失落一陣子，但後來在動畫23集和克里斯結婚。
  - 小森純子（，聲優：[熊井統子](../Page/熊井統子.md "wikilink")／[山田茸](../Page/山田茸.md "wikilink")／[陳琴雲](../Page/陳琴雲.md "wikilink")／[李明幸](../Page/李明幸.md "wikilink")）
    琴子好友兼同班同學。
  - 石川理美（，聲優：[鈴木菜穗子](../Page/鈴木菜穗子.md "wikilink")／[早水理沙](../Page/早水理沙.md "wikilink")／[曾秀清](../Page/曾秀清.md "wikilink")（香港））
    琴子好友兼同班同學。
  - ナラサキ淳平（）
    高宮良（）
    理美的男友，富有人家的兒子。
  - 渡邊純一（，聲優：[楠田敏之](../Page/楠田敏之.md "wikilink")／[李凱傑](../Page/李凱傑.md "wikilink")（香港））
    直樹的同班同學，常跟隨直樹。
  - 矢田園子（，聲優：[仲間由紀惠](../Page/仲間由紀惠.md "wikilink")）
    直樹的同班同學。
  - 松本裕子（，聲優：[木村亞希子](../Page/木村亞希子.md "wikilink")／[陳凱婷](../Page/陳凱婷.md "wikilink")／[李明幸](../Page/李明幸.md "wikilink")）
    崇拜、愛慕直樹的天才少女，琴子的情敵，但卻是拿得起放得下的人。
  - 松本綾子（，聲優：[淺川悠](../Page/淺川悠.md "wikilink")／[鄭麗麗](../Page/鄭麗麗.md "wikilink")（香港））
    裕子的妹妹。
  - 須藤前輩（，聲優：[岩田光央](../Page/岩田光央.md "wikilink")／[李錦綸](../Page/李錦綸.md "wikilink")／[林谷珍](../Page/林谷珍.md "wikilink")）
    直樹的學長，喜歡裕子，畢業後仍死纏大學網球部，因為他想每天都能看到裕子。
  - ノンちゃん（，聲優：[真田麻美](../Page/真田麻美.md "wikilink")）
    中川武人（，聲優：[泰勇氣](../Page/泰勇氣.md "wikilink")）
    原先追求琴子的大一生，曾是阿金的情敵，後來求愛失敗轉而跟綾子戀愛。
  - 菅原（，聲優：[泉尚摯](../Page/泉尚摯.md "wikilink")）
    琴子高中F班的老師。

#### 看護科

  - 小倉智子（，聲優：[片貝まこ](../Page/片貝まこ.md "wikilink")／[陳皓宜](../Page/陳皓宜.md "wikilink")（香港））
  - 品川真理奈（，聲優：[井上麻里奈](../Page/井上麻里奈.md "wikilink")／[余欣沛](../Page/余欣沛.md "wikilink")（香港））
  - 鴨狩啟太（，聲優：[神奈延年](../Page/神奈延年.md "wikilink")／[黃啟昌](../Page/黃啟昌.md "wikilink")（香港））
  - 桔梗幹（，聲優：[齋賀觀月](../Page/齋賀觀月.md "wikilink")／[麥皓豐](../Page/麥皓豐.md "wikilink")（香港））

#### 醫學部

  - 船津誠一（）

## 出版書籍

  - Margaret Comics《淘氣小親親》全23卷

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>1991年1月30日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>1991年4月1日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>1991年8月28日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>1991年12月21日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>1992年4月29日</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>1992年8月30日</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>1992年2月22日</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>1993年4月27日</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>1993年8月30日</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>1993年12月22日</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>1994年7月30日</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>1994年11月30日</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>1995年4月30日</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>1995年10月30日</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>1996年2月28日</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>1996年5月29日</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>1996年9月30日</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>1997年3月2日</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>1997年7月30日</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>1998年3月30日</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>1998年7月29日</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>1998年12月2日</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>1999年5月30日</p></td>
</tr>
</tbody>
</table>

  - 集英社文庫Comic版《淘氣小親親》全14卷
  - 集英社Girls Remix《淘氣小親親》全6卷
  - 淘氣小親親─多田薰插畫集 SG Comics Special
  - Fairbell Comics《淘氣小親親》全12卷
  - Cobalt文庫《淘氣小親親》全2冊（著 - 原田紀）

## 廣播劇CD

  - イタズラなKiss 1 〜フラれても好きな人〜（2005年1月25日發行）
  - イタズラなKiss 2 〜イタズラなKiss\!?〜（2005年2月25日發行）
  - イタズラなKiss 3 〜入江くんのプロポーズ\!?〜（2005年3月25日發行）
  - イタズラなKiss 4 〜戀のライバル出現注意報\!\!〜（2005年5月25日發行）
  - イタズラなKiss 5 （2006年發行）
  - イタズラなKiss 6 （2006年發行）

### 音樂

  - 「Dist」[M＋K](../Page/M＋K.md "wikilink")（第1卷～第3卷 OP）

<!-- end list -->

  -
    作詞、作曲：[Mine](../Page/Mine.md "wikilink")

<!-- end list -->

  - 「モンマルトルの丘」M＋K（第1卷～第3卷 ED）

<!-- end list -->

  -
    作詞：Mine，作曲：[Kohta Igarashi](../Page/五十嵐公太.md "wikilink")

<!-- end list -->

  - 「76th STAR」[水樹奈々](../Page/水樹奈々.md "wikilink")（第4卷～第6卷 OP）

<!-- end list -->

  -
    作詞：[Nokko](../Page/Nokko.md "wikilink")、沢ちひろ，作曲：[土橋安騎夫](../Page/土橋安騎夫.md "wikilink")，編曲：五十嵐公太

<!-- end list -->

  - 「好き\!」水樹奈々（第4卷～第6卷 ED）

<!-- end list -->

  -
    作詞：水樹奈々，作曲、編曲：五十嵐公太

### 製作人員

  - 企畫、製作 - 港製作公司
  - 音響監製、導演 - [難波圭一](../Page/難波圭一.md "wikilink")
  - 腳本 - [松田環](../Page/松田環.md "wikilink")
  - 錄音、調整 - [石阪徹](../Page/石阪徹.md "wikilink")
  - 音響效果 - [村田裕子](../Page/村田裕子.md "wikilink")
  - 角色設定協助 - kekke corporation
  - 製作進行 - [原田茂](../Page/原田茂.md "wikilink")
  - 監製 - [西川茂](../Page/西川茂.md "wikilink")

## 電視動畫

《惡作劇之吻 Kiss》全25話

  - [日本於](../Page/日本.md "wikilink")2008年4月起在[CBC](../Page/中部日本放送.md "wikilink")、[TBS播放](../Page/東京廣播公司.md "wikilink")。
  - [台灣於](../Page/台灣.md "wikilink")2009年1月5日起在[緯來日本台週一至五晚上七點半至八點播放](../Page/緯來日本台.md "wikilink")。
  - [香港於](../Page/香港.md "wikilink")2010年12月17日起在[J2台週五晚上十時至十時半播放](../Page/J2台.md "wikilink")。

### 每集標題

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>日文標題</p></th>
<th><p>中文標題</p></th>
<th><p>腳本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>命運的惡作劇</p></td>
<td><p>清水友佳子</p></td>
<td><p><a href="../Page/山崎理.md" title="wikilink">山崎理</a></p></td>
<td><p>大久保修</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>危險的同居生活</p></td>
<td><p>広田光毅</p></td>
<td><p><a href="../Page/金崎貴臣.md" title="wikilink">金崎貴臣</a></p></td>
<td><p>山口武志</p></td>
<td><p>小林一三</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>戀愛接力棒</p></td>
<td><p>松田恵里子</p></td>
<td><p>小林一三</p></td>
<td><p>小酒井悠</p></td>
<td><p>Chol young hee<br />
Jung gee hee</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>心跳的暑假</p></td>
<td><p>小林成朗</p></td>
<td><p>大庭秀昭</p></td>
<td><p><a href="../Page/新田義方.md" title="wikilink">新田義方</a></p></td>
<td><p>森友廣樹<br />
鵜池一馬</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>走上絕路！F班冬季之陣</p></td>
<td><p>広田光毅</p></td>
<td><p>星野真</p></td>
<td><p>佐々木敏子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>巧克力、應考和疫病</p></td>
<td><p>清水友佳子</p></td>
<td><p>名村英敏</p></td>
<td><p>吉田理紗子</p></td>
<td><p>池下博紀</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>惡作劇之吻</p></td>
<td><p>松田恵里子</p></td>
<td><p>福田貴之</p></td>
<td><p>伊藤伸太郎</p></td>
<td><p>加藤洋人<br />
Kim boo young</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>憧憬的校園生活!?</p></td>
<td><p>小林成朗</p></td>
<td><p>小林一三</p></td>
<td><p>康村諒</p></td>
<td><p>末廣直貴</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>瞄準約會!</p></td>
<td><p>広田光毅</p></td>
<td><p>五月女有作</p></td>
<td><p><a href="../Page/渡辺伸弘.md" title="wikilink">渡辺伸弘</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>再見．雨天</p></td>
<td><p>小林成朗</p></td>
<td><p>山口武志</p></td>
<td><p>阿部達也</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>在夢中Kiss．Kiss．Kiss</p></td>
<td><p>松田恵里子</p></td>
<td><p>辻伸一</p></td>
<td><p>井上義弘</p></td>
<td><p>Lee jong kyung<br />
Kim boo young</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>交錯的心</p></td>
<td><p>小林成朗</p></td>
<td><p>大庭秀昭</p></td>
<td><p>守田芸成</p></td>
<td><p>鵜池一馬</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>戀愛的句號</p></td>
<td><p>清水友佳子</p></td>
<td><p>高梨光</p></td>
<td><p>山崎理<br />
清水聰</p></td>
<td><p>大久保修</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>最強的Kiss</p></td>
<td><p>山崎光惠</p></td>
<td><p>藤原未来夫</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>蜜月旅行．恐慌!</p></td>
<td><p>広田光毅</p></td>
<td><p>名村英敏</p></td>
<td><p>津田尚克</p></td>
<td><p>Lee jong kyung<br />
Kim boo young</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>緊隨而至的幸福</p></td>
<td><p>小林成朗</p></td>
<td><p>小林一三</p></td>
<td><p>水本葉月</p></td>
<td><p>明珍宇作</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>令人妒忌的新面孔</p></td>
<td><p>廣田光毅</p></td>
<td><p>五月女有作</p></td>
<td><p><a href="../Page/三浦貴弘.md" title="wikilink">三浦貴弘</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>不愉快的三角關係</p></td>
<td><p>清水友佳子</p></td>
<td><p>高梨光</p></td>
<td><p>山口武志</p></td>
<td><p>音尾琢磨</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>CRAZY FOR YOU</p></td>
<td><p>大庭秀昭</p></td>
<td><p>守田芸成</p></td>
<td><p>鵜池一馬</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>南丁格爾的誓言</p></td>
<td><p>松田恵里子</p></td>
<td><p><a href="../Page/山本泰一郎.md" title="wikilink">山本泰一郎</a></p></td>
<td><p>辻泰永</p></td>
<td><p>高橋由伸</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p>玻璃般的少年</p></td>
<td><p>広田光毅</p></td>
<td><p>名村英敏</p></td>
<td><p>所俊克</p></td>
<td><p>八代希美子<br />
石田啓一</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p>最好的禮物</p></td>
<td><p>小林成朗</p></td>
<td><p>青柳宏宣</p></td>
<td><p>吉田里紗子</p></td>
<td><p>池下博紀</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>你的世界因我而轉</p></td>
<td><p>清水友佳子</p></td>
<td><p>高梨光</p></td>
<td><p>五月女有作</p></td>
<td><p>渡辺伸弘</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>高興．戀愛．狂歡</p></td>
<td><p>名村英敏</p></td>
<td><p>清水聡</p></td>
<td><p>小坂知<br />
<a href="../Page/佐佐木惠子.md" title="wikilink">佐佐木惠子</a><br />
鵜池一馬</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td></td>
<td><p>Hello Again</p></td>
<td><p>松田恵里子</p></td>
<td><p>金崎貴臣</p></td>
<td><p>山崎理</p></td>
<td></td>
</tr>
</tbody>
</table>

### 音樂

  - 片頭曲

<!-- end list -->

  - 「」（第1話～第25話）

<!-- end list -->

  -
    作詞、作曲：秦基博，編曲：[松浦晃久](../Page/松浦晃久.md "wikilink")，演唱：[秦基博](../Page/秦基博.md "wikilink")

<!-- end list -->

  - 片尾曲

<!-- end list -->

  - 「」（第1話～第12話）

<!-- end list -->

  -
    作詞：，作曲：[中島優美](../Page/中島優美.md "wikilink")，編曲、演唱：[GO\!GO\!7188](../Page/GO!GO!7188.md "wikilink")

<!-- end list -->

  - 「」（第10話插曲）（第13話～第23話、第25話）

<!-- end list -->

  -
    作詞：AZU & [Naoki
    Takada](../Page/Naoki_Takada.md "wikilink")，作曲：[Kazunori
    Fujimoto](../Page/Kazunori_Fujimoto.md "wikilink") & Naoki
    Takada，編曲：Kazunori
    Fujimoto，演唱：[AZU](../Page/AZU.md "wikilink")

### 製作人員

  - 導演 - [山崎理](../Page/山崎理.md "wikilink")
  - 系列構成 - [清水友佳子](../Page/清水友佳子.md "wikilink")
  - 人物設定 - [藤岡真紀](../Page/藤岡真紀.md "wikilink")、[想田
    和弘](../Page/想田_和弘.md "wikilink")
  - 美術監製 - [松本浩樹](../Page/松本浩樹.md "wikilink")
  - 色彩設計 -
    [西香代子](../Page/西香代子.md "wikilink")、[佐藤直子](../Page/佐藤直子.md "wikilink")
  - 攝影監製 - [桑良人](../Page/桑良人.md "wikilink")
  - 編輯 - [柳圭介](../Page/柳圭介.md "wikilink")
  - 音樂 - [高梨康治](../Page/高梨康治.md "wikilink")
  - 音響監製 - [長崎行男](../Page/長崎行男.md "wikilink")
  - 音響製作 - [Trinity Sound](../Page/Trinity_Sound.md "wikilink")
  - 動畫製作 - [TMS Entertainment](../Page/TMS_Entertainment.md "wikilink")

## 舞台劇

  - [日本](../Page/日本.md "wikilink")

<!-- end list -->

  -
    2008年《惡作劇之吻〜戀愛夥伴的學園傳說〜》（）及2009年《惡作劇之吻〜畢業篇〜》（）。由[八神蓮飾演入江直樹](../Page/八神蓮.md "wikilink")；[安倍麻美飾演相原琴子](../Page/安倍麻美.md "wikilink")。

<!-- end list -->

  - [台灣](../Page/台灣.md "wikilink")

<!-- end list -->

  -
    2012年《淘氣小親親》。由[黃騰浩飾演江植樹](../Page/黃騰浩.md "wikilink")；[魏蔓飾演袁湘琴](../Page/魏蔓.md "wikilink")。

## 電視劇

  - [日本](../Page/日本.md "wikilink")

<!-- end list -->

  -
    1996年拍攝「[《惡作劇之吻》](../Page/惡作劇之吻_\(日本電視劇\).md "wikilink")」（）。由[柏原崇飾演入江直樹](../Page/柏原崇.md "wikilink")；[佐藤藍子飾演相原琴子](../Page/佐藤藍子.md "wikilink")，於1996年10月14日起於[日本](../Page/日本.md "wikilink")[朝日電視台週一晚上播出](../Page/朝日電視台.md "wikilink")。
    2013年拍攝「[《一吻定情～Love in
    TOKYO》](../Page/惡作劇之吻～Love_in_TOKYO.md "wikilink")」（）及2014年拍攝續集「[《一吻定情2～Love
    in TOKYO
    》](../Page/惡作劇之吻～Love_in_TOKYO.md "wikilink")」（）。由[古川雄輝飾演入江直樹](../Page/古川雄輝.md "wikilink")；[未來穂香飾演相原琴子](../Page/未來穂香.md "wikilink")，於2013年3月29日起在[日本](../Page/日本.md "wikilink")[富士電視台週五深夜收費台播出](../Page/富士電視台.md "wikilink")。

<!-- end list -->

  - [台灣](../Page/台灣.md "wikilink")

<!-- end list -->

  -
    2005年拍攝「[《惡作劇之吻》台灣版](../Page/惡作劇之吻_\(2005年電視劇\).md "wikilink")」。由[鄭元暢飾演江直樹](../Page/鄭元暢.md "wikilink")；[林依晨飾演袁湘琴](../Page/林依晨.md "wikilink")，於2005年9月25日起於台灣[中視週日晚上播出](../Page/中視.md "wikilink")。
    2007年拍攝续集「[《惡作劇2吻》](../Page/惡作劇2吻.md "wikilink")」。由[鄭元暢飾演江直樹](../Page/鄭元暢.md "wikilink")；[林依晨飾演袁湘琴](../Page/林依晨.md "wikilink")，於2007年12月16日起於台灣[中視週日晚上播出](../Page/中視.md "wikilink")。
    2016年再度拍攝「[《惡作劇之吻》](../Page/惡作劇之吻_\(2016年電視劇\).md "wikilink")」。由[李玉璽飾演江植樹](../Page/李玉璽.md "wikilink")；[吳心緹飾演向月琴](../Page/吳心緹.md "wikilink")，於2016年12月起於台灣[超視週日晚上播出](../Page/超視.md "wikilink")。

<!-- end list -->

  - [韓國](../Page/韓國.md "wikilink")

<!-- end list -->

  -
    2010年拍攝「[《惡作劇之吻》韓國版](../Page/惡作劇之吻_\(韓國電視劇\).md "wikilink")」（）。由[SS501的](../Page/SS501.md "wikilink")[金賢重飾演白勝祖](../Page/金賢重.md "wikilink")；[鄭素敏飾演吳哈妮](../Page/鄭素敏.md "wikilink")，於2010年9月1日起於韓國[MBC電視台播出](../Page/文化廣播_\(韓國\).md "wikilink")。

<!-- end list -->

  - [泰國](../Page/泰國.md "wikilink")

<!-- end list -->

  -
    2015年拍攝「[Kiss
    Me](../Page/Kiss_Me.md "wikilink")」（รักล้นใจนายแกล้งจุ๊บ
    Kiss me）。由[Golf & Mike的](../Page/Golf_&_Mike.md "wikilink")[Mike D.
    Angelo飾演Ten](../Page/Mike_D._Angelo.md "wikilink")
    Ten；[李海娜飾演Taliw](../Page/李海娜.md "wikilink")，於2015年起於泰國[True4U電視台播出](../Page/True4U.md "wikilink")。

## 電影

  - [日本](../Page/日本.md "wikilink")

<!-- end list -->

  -
    2016年及2017年拍攝「[《惡作劇之吻 THE
    MOVIE》](../Page/惡作劇之吻_THE_MOVIE.md "wikilink")」（）。由[佐藤寬太飾演入江直樹](../Page/佐藤寬太.md "wikilink")；[美沙玲奈飾演相原琴子](../Page/美沙玲奈.md "wikilink")。共三部。

<!-- end list -->

  - [臺灣](../Page/臺灣.md "wikilink")

<!-- end list -->

  -
    2019年拍攝「[一吻定情](../Page/一吻定情_\(電影\).md "wikilink")」。由[王大陸飾演江直樹](../Page/王大陸.md "wikilink")；[林允飾演原湘琴](../Page/林允.md "wikilink")。

## 各改編作品之主要演員列表

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th><p>朝日電視台<br />
《<a href="../Page/惡作劇之吻_(日本電視劇).md" title="wikilink">惡作劇之吻</a>》<br />
(1996)</p></th>
<th><p>中視、八大電視台<br />
《<a href="../Page/惡作劇之吻.md" title="wikilink">惡作劇之吻</a>、<a href="../Page/惡作劇2吻.md" title="wikilink">惡作劇2吻</a>》<br />
(2005、2007)</p></th>
<th><p>MBC<br />
《<a href="../Page/惡作劇之吻_(韓國電視劇).md" title="wikilink">惡作劇之吻</a>》<br />
(2010)</p></th>
<th><p>富士TWO<br />
《<a href="../Page/惡作劇之吻～Love_in_TOKYO.md" title="wikilink">惡作劇之吻<small>～Love in TOKYO</small></a>》(2013)<br />
《<a href="../Page/惡作劇之吻～Love_in_TOKYO.md" title="wikilink">惡作劇之吻2<small>～Love in TOKYO</small></a>》(2014)</p></th>
<th><p>True4U<br />
《<a href="../Page/Kiss_Me.md" title="wikilink">รักล้นใจนายแกล้งจุ๊บ Kiss me</a>》<br />
(2015)</p></th>
<th><p><br />
《<a href="../Page/惡作劇之吻_(2016年電視劇).md" title="wikilink">惡作劇之吻</a>》<br />
(2016)</p></th>
<th><p>電影<br />
《惡作劇之吻<small> THE MOVIE Part1 〜高中篇〜</small>》(2016)<br />
《惡作劇之吻<small> THE MOVIE Part2 大學篇～</small>》(2017)<br />
《惡作劇之吻<small> THE MOVIE Part3 求婚篇～</small>》(2017)</p></th>
<th><p>舞台劇<br />
《惡作劇之吻<small>〜戀愛夥伴的學園傳說〜</small>》(2008)<br />
《惡作劇之吻<small>〜畢業篇〜</small>》(2009)</p></th>
<th><p>舞台劇<br />
《淘氣小親親》(2012)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>入江直樹</p></td>
<td><p><a href="../Page/柏原崇.md" title="wikilink">柏原崇</a></p></td>
<td><p><a href="../Page/鄭元暢.md" title="wikilink">鄭元暢</a><br />
（角色：江直樹）</p></td>
<td><p><a href="../Page/金賢重.md" title="wikilink">金賢重</a><br />
（角色：白勝祖）</p></td>
<td><p><a href="../Page/古川雄輝.md" title="wikilink">古川雄輝</a></p></td>
<td><p><a href="../Page/Mike_D._Angelo.md" title="wikilink">Mike D. Angelo</a><br />
（角色：Ten Ten）</p></td>
<td><p><a href="../Page/李玉璽.md" title="wikilink">李玉璽</a><br />
（角色：江植樹）</p></td>
<td><p><a href="../Page/佐藤寬太.md" title="wikilink">佐藤寬太</a></p></td>
<td><p><a href="../Page/八神蓮.md" title="wikilink">八神蓮</a></p></td>
<td><p><a href="../Page/黃騰浩.md" title="wikilink">黃騰浩</a><br />
（角色：江植樹）</p></td>
</tr>
<tr class="even">
<td><p>相原琴子</p></td>
<td><p><a href="../Page/佐藤藍子.md" title="wikilink">佐藤藍子</a></p></td>
<td><p><a href="../Page/林依晨.md" title="wikilink">林依晨</a><br />
（角色：袁湘琴）</p></td>
<td><p><a href="../Page/庭沼珉.md" title="wikilink">庭沼珉</a><br />
(角色：吳哈妮)</p></td>
<td><p><a href="../Page/未来穂香.md" title="wikilink">未来穂香</a></p></td>
<td><p><a href="../Page/李海娜.md" title="wikilink">李海娜</a><br />
（角色：Taliw）</p></td>
<td><p><a href="../Page/吳心緹.md" title="wikilink">吳心緹</a><br />
（角色：向月琴）</p></td>
<td><p><a href="../Page/美沙玲奈.md" title="wikilink">美沙玲奈</a></p></td>
<td><p><a href="../Page/安倍麻美.md" title="wikilink">安倍麻美</a></p></td>
<td><p><a href="../Page/魏蔓.md" title="wikilink">魏蔓</a><br />
（角色：袁湘琴）</p></td>
</tr>
<tr class="odd">
<td><p>相原重雄</p></td>
<td><p><a href="../Page/內藤剛志.md" title="wikilink">內藤剛志</a></p></td>
<td><p><a href="../Page/唐從聖.md" title="wikilink">唐從聖</a><br />
（角色：袁有才）</p></td>
<td><p><a href="../Page/姜南佶.md" title="wikilink">姜南佶</a><br />
（角色：吳基同）</p></td>
<td><p><a href="../Page/田中要次.md" title="wikilink">田中要次</a></p></td>
<td><p>Jirawat Wachirasarunpat<br />
（角色：Han）</p></td>
<td><p><a href="../Page/洪都拉斯_(藝人).md" title="wikilink">-{洪}-都拉斯</a><br />
（角色：向東流）</p></td>
<td><p><a href="../Page/陣內孝則.md" title="wikilink">陣內孝則</a></p></td>
<td></td>
<td><p><a href="../Page/朱陸豪.md" title="wikilink">朱陸豪</a><br />
（角色：阿財）</p></td>
</tr>
<tr class="even">
<td><p>入江重樹</p></td>
<td><p><a href="../Page/德井優.md" title="wikilink">德井優</a></p></td>
<td><p><a href="../Page/張永正.md" title="wikilink">張永正</a><br />
（角色：江萬利）</p></td>
<td><p><a href="../Page/吳京秀.md" title="wikilink">吳京秀</a><br />
（角色：白秀昌）</p></td>
<td><p><a href="../Page/芋洗坂係長.md" title="wikilink">芋洗坂係長</a></p></td>
<td><p>Puttipong Pormsaka<br />
（角色：Bom）</p></td>
<td><p><a href="../Page/趙自強.md" title="wikilink">趙自強</a><br />
（角色：江春水）</p></td>
<td><p><a href="../Page/石塚英彥.md" title="wikilink">石塚英彥</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>入江紀子</p></td>
<td><p><a href="../Page/淺田美代子.md" title="wikilink">淺田美代子</a></p></td>
<td><p><a href="../Page/趙詠華.md" title="wikilink">趙詠華</a><br />
（角色：阿利嫂）</p></td>
<td><p><a href="../Page/鄭慧英.md" title="wikilink">鄭惠英</a><br />
（角色：黃晶熙）</p></td>
<td><p><a href="../Page/西村知美.md" title="wikilink">西村知美</a></p></td>
<td><p><a href="../Page/Inthira_Yeunyong.md" title="wikilink">Inthira Yeunyong</a>（角色：Kaew）</p></td>
<td><p><a href="../Page/蔡燦得.md" title="wikilink">蔡燦得</a><br />
（角色：盼盼姨）</p></td>
<td><p><a href="../Page/石田光.md" title="wikilink">石田光</a></p></td>
<td><p><a href="../Page/田中里枝.md" title="wikilink">田中里枝</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>入江裕樹</p></td>
<td><p><a href="../Page/明石亮太郎.md" title="wikilink">明石亮太郎</a></p></td>
<td><p><a href="../Page/章柏翰.md" title="wikilink">章柏翰</a>、<a href="../Page/瞿嵩洋.md" title="wikilink">瞿嵩洋</a><br />
（角色：江裕樹）</p></td>
<td><p><a href="../Page/崔元洪.md" title="wikilink">崔元洪</a><br />
（角色：白恩祖）</p></td>
<td><p><a href="../Page/相澤侑我.md" title="wikilink">相澤侑我</a><br />
（2013）<br />
<a href="../Page/宮城孔明.md" title="wikilink">宮城孔明</a><br />
（2014）</p></td>
<td><p>Yukung Watanabe<br />
（角色：Turbo）</p></td>
<td><p><a href="../Page/沈昶宏.md" title="wikilink">沈昶宏</a><br />
（角色：江玉樹）</p></td>
<td></td>
<td><p><a href="../Page/星野亞門.md" title="wikilink">星野亞門</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>池澤金之助</p></td>
<td><p><a href="../Page/青木伸輔.md" title="wikilink">青木伸輔</a></p></td>
<td><p><a href="../Page/汪東城.md" title="wikilink">汪東城</a><br />
（角色：金元豐）</p></td>
<td><p><a href="../Page/李太成.md" title="wikilink">李太成</a><br />
（角色：奉俊具）</p></td>
<td><p><a href="../Page/山田裕貴.md" title="wikilink">山田裕貴</a></p></td>
<td><p><a href="../Page/Tao_Phiangphor.md" title="wikilink">Tao Phiangphor</a><br />
（角色：King）</p></td>
<td><p><a href="../Page/宮以騰.md" title="wikilink">宮以騰</a><br />
（角色：金支柱）</p></td>
<td><p><a href="../Page/大倉士門.md" title="wikilink">大倉士門</a></p></td>
<td><p><a href="../Page/鷲尾昇.md" title="wikilink">鷲尾昇</a></p></td>
<td><p><a href="../Page/郭彥均.md" title="wikilink">郭彥均</a><br />
（角色：阿金）</p></td>
</tr>
<tr class="even">
<td><p>渡邊純一</p></td>
<td><p><a href="../Page/井澤健.md" title="wikilink">井澤健</a></p></td>
<td><p><a href="../Page/賴智偉.md" title="wikilink">賴智偉</a><br />
（角色：杜建中）</p></td>
<td></td>
<td><p><a href="../Page/河相沙羅.md" title="wikilink">河相沙羅</a></p></td>
<td></td>
<td><p><a href="../Page/吳承璟.md" title="wikilink">吳承璟</a><br />
（角色：杜亞鈞）</p></td>
<td><p><a href="../Page/井出卓也.md" title="wikilink">井出卓也</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小森仁子</p></td>
<td><p><a href="../Page/上原櫻.md" title="wikilink">上原櫻</a></p></td>
<td><p><a href="../Page/劉容嘉.md" title="wikilink">劉容嘉</a><br />
（角色：劉雅農）</p></td>
<td><p><a href="../Page/洪允華.md" title="wikilink">洪允華</a><br />
（角色：鄭茱莉）</p></td>
<td><p><a href="../Page/藤本七海.md" title="wikilink">藤本七海</a></p></td>
<td><p>Paveena Rojjindangamr<br />
（角色：PAEW）</p></td>
<td><p><a href="../Page/文聞.md" title="wikilink">文聞</a><br />
（角色：小森）</p></td>
<td><p><a href="../Page/灯敦生.md" title="wikilink">灯敦生</a></p></td>
<td><p><a href="../Page/仲村瑠璃亞.md" title="wikilink">仲村瑠璃亞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>石川理美</p></td>
<td><p><a href="../Page/宮內知美.md" title="wikilink">宮內知美</a></p></td>
<td><p><a href="../Page/楊佩婷.md" title="wikilink">楊佩婷</a><br />
（角色：林純美）</p></td>
<td><p><a href="../Page/尹承雅.md" title="wikilink">尹承雅</a><br />
（角色：獨孤敏兒）</p></td>
<td><p><a href="../Page/山谷花純.md" title="wikilink">山谷花純</a></p></td>
<td><p>Suthada Jongjaiphar<br />
（角色：YUYI）</p></td>
<td><p><a href="../Page/喬雅琳.md" title="wikilink">喬雅琳</a><br />
（角色：石理美）</p></td>
<td><p><a href="../Page/山口乃乃華.md" title="wikilink">山口乃乃華</a></p></td>
<td><p><a href="../Page/黑木馬里那.md" title="wikilink">黑木馬里那</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>松本裕子</p></td>
<td><p><a href="../Page/小澤真珠.md" title="wikilink">小澤真珠</a></p></td>
<td><p><a href="../Page/許瑋甯.md" title="wikilink">許瑋甯</a><br />
（角色：裴子瑜）</p></td>
<td><p><a href="../Page/李詩英.md" title="wikilink">李詩英</a><br />
（角色：尹赫拉）</p></td>
<td><p><a href="../Page/森寬和.md" title="wikilink">森寬和</a></p></td>
<td><p>Pim Bubear<br />
（角色：Namkang）</p></td>
<td><p><a href="../Page/席惟倫.md" title="wikilink">席惟倫</a><br />
（角色：吳子裕）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>松本綾子</p></td>
<td></td>
<td><p><a href="../Page/謝欣穎.md" title="wikilink">謝欣穎</a><br />
（角色：裴子琪）</p></td>
<td></td>
<td><p><a href="../Page/彩夢.md" title="wikilink">彩夢</a></p></td>
<td></td>
<td><p><a href="../Page/吳季璇.md" title="wikilink">吳季璇</a><br />
（角色：吳子綾）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>須藤前輩</p></td>
<td></td>
<td><p><a href="../Page/王威登.md" title="wikilink">王威登</a><br />
（角色：王皓謙）</p></td>
<td><p><a href="../Page/崔成國.md" title="wikilink">崔成國</a><br />
（角色：王慶秀）</p></td>
<td><p><a href="../Page/加治將樹.md" title="wikilink">加治將樹</a></p></td>
<td><p>Techin<br />
（角色：P dan）</p></td>
<td><p><a href="../Page/邱俊儒.md" title="wikilink">邱俊儒</a><br />
（角色：胡須藤）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>阿俊</p></td>
<td></td>
<td><p><br />
（角色：阿俊）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>一也</p></td>
<td></td>
<td><p><br />
（角色：小亞）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>阿諾</p></td>
<td></td>
<td><p><a href="../Page/邱勝翊.md" title="wikilink">邱勝翊</a><br />
（角色：丁海諾）</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/駱炫銘.md" title="wikilink">駱炫銘</a><br />
（角色：秦一諾）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中川武人</p></td>
<td></td>
<td><p><a href="../Page/李紹祥.md" title="wikilink">李紹祥</a><br />
（角色：張武人）</p></td>
<td><p><br />
（角色：金啟泰）</p></td>
<td><p><a href="../Page/內海啓貴.md" title="wikilink">內海啓貴</a></p></td>
<td></td>
<td><p><a href="../Page/WISH.md" title="wikilink">WISH</a><br />
（角色：武中川）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高宮良</p></td>
<td></td>
<td><p><a href="../Page/炎亞綸.md" title="wikilink">炎亞綸</a><br />
（角色：阿布）</p></td>
<td></td>
<td><p><a href="../Page/白又敦.md" title="wikilink">白又敦</a></p></td>
<td></td>
<td><p>郭子豪</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大泉沙穗子</p></td>
<td></td>
<td><p><a href="../Page/白歆惠.md" title="wikilink">白歆惠</a><br />
（角色：白惠蘭）</p></td>
<td></td>
<td><p>-{<a href="../Page/高田里穗.md" title="wikilink">高田里穗</a>}-</p></td>
<td><p>Pimpatchara Vajrasevee<br />
（角色：Nana）</p></td>
<td><p><a href="../Page/簡廷芮.md" title="wikilink">簡廷芮</a><br />
（角色：白莎穗）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大泉會長</p></td>
<td></td>
<td><p><a href="../Page/乾德門.md" title="wikilink">乾德門</a><br />
（角色：大泉社長）</p></td>
<td><p><br />
（角色：尹會長）</p></td>
<td><p><a href="../Page/堀內正美.md" title="wikilink">堀內正美</a></p></td>
<td></td>
<td><p><a href="../Page/高振鵬.md" title="wikilink">高振鵬</a><br />
（角色：白泉會長）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>堀內麻理</p></td>
<td></td>
<td><p><a href="../Page/千田愛紗.md" title="wikilink">千田愛紗</a><br />
（角色：瑪麗）</p></td>
<td><p><br />
（角色：賢雅）</p></td>
<td><p><a href="../Page/增田有華.md" title="wikilink">增田有華</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>堀內巧</p></td>
<td></td>
<td><p><a href="../Page/谷炫淳.md" title="wikilink">谷炫淳</a><br />
（角色：阿巧）</p></td>
<td></td>
<td><p><a href="../Page/岡部尚.md" title="wikilink">岡部尚</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>船津誠一</p></td>
<td></td>
<td><p><a href="../Page/修杰楷.md" title="wikilink">修-{杰}-楷</a><br />
（角色：周傳津）</p></td>
<td></td>
<td><p><a href="../Page/竹內壽.md" title="wikilink">竹內壽</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐川好美</p></td>
<td></td>
<td><p><a href="../Page/林珈妤.md" title="wikilink">林珈妤</a>、<a href="../Page/孟耿如.md" title="wikilink">孟耿如</a><br />
（角色：林好美）</p></td>
<td><p><br />
（角色：莎拉）</p></td>
<td><p><a href="../Page/松浦愛弓.md" title="wikilink">松浦愛弓</a>、<a href="../Page/小野花梨.md" title="wikilink">小野花梨</a></p></td>
<td><p>Shinaradee Anupongpichart<br />
（角色：June）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>直樹外公</p></td>
<td></td>
<td><p><a href="../Page/關聰.md" title="wikilink">關聰</a><br />
（角色：直樹外公）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>克莉絲汀·羅賓斯</p></td>
<td></td>
<td><p><a href="../Page/瑞莎.md" title="wikilink">瑞莎</a><br />
（角色：克莉絲汀·羅賓斯）</p></td>
<td><p><br />
（角色：克莉絲）</p></td>
<td><p><a href="../Page/中井諾愛美.md" title="wikilink">中井諾愛美</a></p></td>
<td></td>
<td><p>安妮<br />
（角色：麗娜）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>阿魯邦</p></td>
<td></td>
<td><p><a href="../Page/鄭元暢.md" title="wikilink">鄭元暢</a><br />
（角色：阿魯邦）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>小倉智子</p></td>
<td></td>
<td><p><a href="../Page/五熊.md" title="wikilink">五熊</a><br />
（角色：羅智儀）</p></td>
<td></td>
<td><p><a href="../Page/伊藤梨沙子.md" title="wikilink">伊藤梨沙子</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>品川真裡奈</p></td>
<td></td>
<td><p><a href="../Page/江佩珍.md" title="wikilink">江佩珍</a><br />
（角色：章妮娜）</p></td>
<td></td>
<td><p><a href="../Page/川上朱莉杏.md" title="wikilink">川上朱莉杏</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>鴨狩啟太</p></td>
<td></td>
<td><p><a href="../Page/曾少宗.md" title="wikilink">曾少宗</a><br />
（角色：楊啟太）</p></td>
<td></td>
<td><p><a href="../Page/堀井新太.md" title="wikilink">堀井新太</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>桔梗幹</p></td>
<td></td>
<td><p><a href="../Page/唐禹哲.md" title="wikilink">唐禹哲</a><br />
（角色：歐陽幹）</p></td>
<td></td>
<td><p><a href="../Page/鈴木身來.md" title="wikilink">鈴木身來</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>良母</p></td>
<td></td>
<td><p><a href="../Page/王雪娥.md" title="wikilink">王雪娥</a><br />
（角色：阿布母）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>細井小百合</p></td>
<td></td>
<td><p><a href="../Page/胡珮瑩.md" title="wikilink">胡珮瑩</a><br />
（角色：趙清水）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>永沢秋子</p></td>
<td></td>
<td><p><a href="../Page/林舒語.md" title="wikilink">林舒語</a><br />
（角色：許秋賢）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉澤豐</p></td>
<td></td>
<td><p><a href="../Page/邱秀敏.md" title="wikilink">邱秀敏</a><br />
（角色：邱罔腰）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>相原悅子</p></td>
<td></td>
<td><p><a href="../Page/林依晨.md" title="wikilink">林依晨</a><br />
（角色：黃秋菊《湘琴母》）</p></td>
<td></td>
<td><p><a href="../Page/未来穗香.md" title="wikilink">未来穗香</a></p></td>
<td><p>Mookmada Rojanatanaboon</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鶴三</p></td>
<td></td>
<td><p><a href="../Page/林裕豐.md" title="wikilink">林裕豐</a><br />
（角色：小可《湘琴三舅》）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>椎名奈美</p></td>
<td></td>
<td><p><a href="../Page/簡嘉芸.md" title="wikilink">簡嘉芸</a><br />
（角色：張君雅）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>奈美母</p></td>
<td></td>
<td><p><a href="../Page/陸明君.md" title="wikilink">陸明君</a><br />
（角色：君雅母）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西垣東馬</p></td>
<td></td>
<td><p><a href="../Page/朱德剛.md" title="wikilink">朱德剛</a><br />
（角色：孫熙恆）</p></td>
<td><p><br />
（角色：郭泰山）</p></td>
<td><p><a href="../Page/君澤由紀.md" title="wikilink">君澤由紀</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大蛇森醫生</p></td>
<td></td>
<td><p><a href="../Page/馬念先.md" title="wikilink">馬念先</a><br />
（角色：杜澤森）</p></td>
<td></td>
<td><p><a href="../Page/中谷龍.md" title="wikilink">中谷龍</a></p></td>
<td><p>Suchao Pongwilai</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>菅原</p></td>
<td><p><a href="../Page/遠山俊也.md" title="wikilink">遠山俊也</a></p></td>
<td><p><a href="../Page/郭宏法.md" title="wikilink">紅膠囊</a><br />
（角色：楊大信）</p></td>
<td><p><a href="../Page/黃孝恩.md" title="wikilink">黃孝恩</a><br />
（角色：宋江伊）</p></td>
<td></td>
<td><p>Bandit Thongdee<br />
（角色：Teacher Hang）</p></td>
<td><p><a href="../Page/民雄.md" title="wikilink">民雄</a><br />
（角色：管原野）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>A班班導</p></td>
<td></td>
<td><p><a href="../Page/姚采穎.md" title="wikilink">姚采穎</a><br />
（角色：文貞觀）</p></td>
<td><p><a href="../Page/宋勇植.md" title="wikilink">宋勇植</a><br />
（角色：宋志奧）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/羅介碩.md" title="wikilink">羅介碩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 排序參考：[イタズラなKiss特集](http://comic-bunko.shueisha.co.jp/plan/kiss/kiss_chara_f.html)

## 相關連結

  - [惡作劇之吻 (日本電視劇)](../Page/惡作劇之吻_\(日本電視劇\).md "wikilink")
  - [惡作劇之吻](../Page/惡作劇之吻.md "wikilink")（2005年台灣電視劇）
  - [惡作劇2吻](../Page/惡作劇2吻.md "wikilink")（台灣電視劇）
  - [惡作劇之吻 (韓國電視劇)](../Page/惡作劇之吻_\(韓國電視劇\).md "wikilink")
  - [惡作劇之吻～Love in
    TOKYO](../Page/惡作劇之吻～Love_in_TOKYO.md "wikilink")（日本電視劇）
  - [Kiss Me](../Page/Kiss_Me.md "wikilink") （泰國電視劇）
  - [惡作劇之吻](../Page/惡作劇之吻_\(2016年電視劇\).md "wikilink")（2016年台灣電視劇）

## 参考文献

## 外部連結

  - [多田薰 官方網站](http://tadakaoru.com/)

  - [電視動畫《惡作劇之吻
    Kiss》官方網頁](http://japan.videoland.com.tw/channel/itakiss/introduction.htm)（緯來日本台）

  - [《一吻定情》（2013版）愛奇藝獨播專區](http://www.iqiyi.com/dianshiju/ywdq.html)

[Category:淘氣小親親](../Category/淘氣小親親.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:少女漫畫](../Category/少女漫畫.md "wikilink")
[Category:高中背景漫畫](../Category/高中背景漫畫.md "wikilink")
[Category:戀愛漫畫](../Category/戀愛漫畫.md "wikilink")
[Category:未完成書籍](../Category/未完成書籍.md "wikilink")
[Category:護士主角題材作品](../Category/護士主角題材作品.md "wikilink")
[Category:校園電視喜劇](../Category/校園電視喜劇.md "wikilink")
[Category:高中背景電視劇](../Category/高中背景電視劇.md "wikilink")
[Category:日本愛情劇](../Category/日本愛情劇.md "wikilink")
[Category:校園喜劇](../Category/校園喜劇.md "wikilink")
[Category:2008年TBS電視網動畫](../Category/2008年TBS電視網動畫.md "wikilink")
[Category:緯來電視外購動畫](../Category/緯來電視外購動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:廣播劇CD](../Category/廣播劇CD.md "wikilink")
[Category:東森電視外購動畫](../Category/東森電視外購動畫.md "wikilink")
[Category:中學背景作品](../Category/中學背景作品.md "wikilink")
[Category:大學背景作品](../Category/大學背景作品.md "wikilink")

1.  [イタズラなKiss 漫画系列](http://bangumi.tv/subject/60488)