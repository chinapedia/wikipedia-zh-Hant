**淡路市**（）是位於[日本](../Page/日本.md "wikilink")[兵庫縣](../Page/兵庫縣.md "wikilink")[淡路島北部的城市](../Page/淡路島.md "wikilink")。往北可通過[明石海峽大橋與位於](../Page/明石海峽大橋.md "wikilink")[本州的](../Page/本州.md "wikilink")[神戶市相接](../Page/神戶市.md "wikilink")，南側則是同位於淡路島的[洲本市](../Page/洲本市.md "wikilink")，西側為[播磨灘](../Page/播磨灘.md "wikilink")，東為[大阪灣](../Page/大阪灣.md "wikilink")\[1\]；轄區中央為丘陵地，海岸則擁有多個漁港和海水浴場。

1995年1月17日發生的[阪神大地震震央即位於轄內](../Page/阪神大地震.md "wikilink")。

## 歷史

現在的轄區範圍在過去全屬[淡路國](../Page/淡路國.md "wikilink")[津名郡](../Page/津名郡.md "wikilink")，於[江戶時期屬於](../Page/江戶時期.md "wikilink")[德島藩的領地](../Page/德島藩.md "wikilink")。

1889年日本實施[町村制後](../Page/町村制.md "wikilink")，在本市範圍內設置了、、、、、、、、、、、、、、、、、、、，共2町18村。

經歷1950年代[昭和大合併後](../Page/市町村合併.md "wikilink")，前述町村被整合為、、、、共5町。

2005年4月1日，這5町再次被整合，合併後成為現在的**淡路市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1926年</p></th>
<th><p>1926年-1944年</p></th>
<th><p>1944年-1954年</p></th>
<th><p>1954年-1989年</p></th>
<th><p>1989年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>鹽田村</p></td>
<td><p>1955年4月1日<br />
津名町</p></td>
<td><p>2005年4月1日<br />
淡路市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>志築町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>生穗村</p></td>
<td><p>1928年3月15日<br />
生穗町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佐野村</p></td>
<td><p>1928年11月1日<br />
佐野町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大町村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>釜口村</p></td>
<td><p>1956年4月1日<br />
淡路町</p></td>
<td><p>1961年6月19日<br />
東浦町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>來馬村</p></td>
<td><p>1912年3月1日<br />
仮屋町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>岩屋町岩屋浦</p></td>
<td><p>岩屋町</p></td>
<td><p>淡路町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岩屋町箙村</p></td>
<td><p>1892年1月22日<br />
野島村</p></td>
<td><p>1955年3月22日<br />
北淡町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>仁井村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>仁井村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>富島村</p></td>
<td><p>1924年4月1日<br />
富島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>淺野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>育波村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>室津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>尾崎村</p></td>
<td><p>1955年3月31日<br />
一宮町</p></td>
<td><p>一宮町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>郡家村</p></td>
<td><p>1923年4月1日<br />
郡家町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>多賀村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>江井村</p></td>
<td><p>1923年4月1日<br />
江井町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>山田村</p></td>
<td><p>1956年4月1日<br />
併入一宮町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

對外交通主要依賴高速公路[神戶淡路鳴門自動車道](../Page/神戶淡路鳴門自動車道.md "wikilink")，往北可經由[明石海峡大桥通往](../Page/明石海峡大桥.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")，往南可經由[洲本市](../Page/洲本市.md "wikilink")、[南淡路市](../Page/南淡路市.md "wikilink")、[大鳴門橋通往](../Page/大鳴門橋.md "wikilink")[四國](../Page/四國.md "wikilink")。此段高速公路在轄內設有[淡路交流道](../Page/淡路IC.md "wikilink")、[淡路服務區](../Page/淡路SA.md "wikilink")、[東浦交流道](../Page/東浦IC.md "wikilink")、[北淡交流道](../Page/北淡IC.md "wikilink")、[室津休息區](../Page/室津停車區.md "wikilink")、[津名一宮交流道](../Page/津名一宮IC.md "wikilink")。

除了高速公路，主要道路則為。

在明石海峡大桥完工通車前，島上對外交通仰賴多條渡輪航線，但現因難敵公路交通的便利性，多已停駛，現僅存定期往返轄內及。

## 觀光資源

[AKNGP02s3200.jpg](https://zh.wikipedia.org/wiki/File:AKNGP02s3200.jpg "fig:AKNGP02s3200.jpg")
[Nojima_Fault.JPG](https://zh.wikipedia.org/wiki/File:Nojima_Fault.JPG "fig:Nojima_Fault.JPG")
[Esakitoudai_01.jpg](https://zh.wikipedia.org/wiki/File:Esakitoudai_01.jpg "fig:Esakitoudai_01.jpg")
[MPMOP01s3200.jpg](https://zh.wikipedia.org/wiki/File:MPMOP01s3200.jpg "fig:MPMOP01s3200.jpg")
[Ohiso_art-yama_museum02s3200.jpg](https://zh.wikipedia.org/wiki/File:Ohiso_art-yama_museum02s3200.jpg "fig:Ohiso_art-yama_museum02s3200.jpg")

  - ：[彌生時代後期最大的鐵器製造群落遺跡](../Page/彌生時代.md "wikilink")。

  - ：幕府末期的砲台遺跡。

  -
  - [伊弉諾神宮](../Page/伊弉諾神宮.md "wikilink")：淡路國[一宮](../Page/一宮.md "wikilink")、[名神大社](../Page/名神大社.md "wikilink")、[官幣大社](../Page/官幣大社.md "wikilink")。

  - [本福寺](../Page/本福寺_\(淡路市\).md "wikilink")：水御堂由[安藤忠雄設計](../Page/安藤忠雄.md "wikilink")。

  -
  -
  -
  - [淡路夢舞台](../Page/淡路夢舞台.md "wikilink")

      - [奇跡之星植物館](../Page/奇跡之星植物館.md "wikilink")

  - [瀨戶內海國立公園](../Page/瀨戶內海國立公園.md "wikilink")

  - （淡路地區）

  -
  - ：保留[阪神大地震震央附近](../Page/阪神大地震.md "wikilink")。

  -
  -
<!-- end list -->

  - 海水浴場

<!-- end list -->

  - 岩屋海水浴場
  - 浦縣民陽光海灘
  - 北淡縣民陽光海灘
  - 北淡室津海灘
  - 尾崎海水浴場
  - 多賀之濱海水浴場
  - 江井海水浴場

## 教育

### 大學

  -
  - 淡路島臨海研討中心

  - [神戶大學內海域環境教育研究中心](../Page/神戶大學.md "wikilink")

  - [兵庫縣立大學大學院](../Page/兵庫縣立大學.md "wikilink") 綠環境景観管理研究科（）

## 姊妹、友好城市

### 日本

  - [奥尻町](../Page/奥尻町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[奧尻郡](../Page/奧尻郡.md "wikilink")）

  - [宍粟市](../Page/宍粟市.md "wikilink")（[兵庫縣](../Page/兵庫縣.md "wikilink")）

  - （[岡山縣](../Page/岡山縣.md "wikilink")[岡山市](../Page/岡山市.md "wikilink")[北區](../Page/北區_\(岡山市\).md "wikilink")）

### 海外

  - （[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[奥格莱塞县](../Page/奥格莱塞县.md "wikilink")）

## 本地出身之名人

  - [志田重男](../Page/志田重男.md "wikilink")：社會運動人士

## 參考資料

## 相關條目

  - [淡路島](../Page/淡路島.md "wikilink")

## 外部連結

  - [淡路島觀光導航](http://www.awajishima-kanko.jp/)

  -

  -

<!-- end list -->

1.