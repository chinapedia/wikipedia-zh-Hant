[Flooding,_Shenzhen,_China.jpg](https://zh.wikipedia.org/wiki/File:Flooding,_Shenzhen,_China.jpg "fig:Flooding,_Shenzhen,_China.jpg")
**深圳6·13特大暴雨**是指2008年6月12日-6月13日[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[深圳市遭遇罕見特大降雨襲擊](../Page/深圳市.md "wikilink")\[1\]。其降雨强度超過了50年一遇，接近百年一遇。

此次暴雨導致深圳多處嚴重[水浸](../Page/內澇.md "wikilink")，[交通受不同程度的影響](../Page/交通.md "wikilink")，6人死亡，十多萬人安全轉移。中小學、幼兒園亦於13日下午停課半日。

## 暴雨

本次暴雨的最大降雨量出現在[寶安區](../Page/寶安區.md "wikilink")[光明唐家村](../Page/光明街道_\(深圳市\).md "wikilink")，6月12日-6月13日累計降雨量超過了502毫米。

[寶安和](../Page/寶安.md "wikilink")[龍崗共有](../Page/龍崗.md "wikilink")33个测站出现特大暴雨。寶安累計降雨量已超過500毫米，其中[石岩](../Page/石岩街道.md "wikilink")、[石岩水庫](../Page/石岩水庫.md "wikilink")、[黃麻布水庫累計降雨量超過了](../Page/黃麻布水庫.md "wikilink")400毫米。

[羅湖](../Page/羅湖.md "wikilink")、[福田降雨量平均在](../Page/福田.md "wikilink")200毫米以上；[龍崗北部降雨量普遍在](../Page/龍崗.md "wikilink")250毫米以上；[南山](../Page/南山區_\(深圳市\).md "wikilink")、[鹽田降雨量在](../Page/鹽田.md "wikilink")100毫米以上。

## 氣象預警

  - 6月13日
      - 00时10分，[深圳市气象台在全市陆地](../Page/深圳市气象台.md "wikilink")、东部海区和西部海区发布<span style="color:yellow; background-color:silver;">**暴雨黄色预警信号**</span>。预计受雷雨云团的影响，深圳市将先后出现暴雨降水。
      - 00时30分，深圳市气象台在全市陆地、东部海区和西部海区发布<span style="color:yellow; background-color:silver;">**雷电预警信号**</span>。预计雷暴云团将影响深圳市。
      - 08时40分，深圳市气象台在全市陆地、东部海区和西部海区发布<span style="color:orange;">**暴雨橙色预警信号**</span>。预计降雨将进一步加大。
      - 09时05分，深圳市气象台在[宝安区发布](../Page/宝安区.md "wikilink")<span style="color: red;">**暴雨红色预警信号**</span>。预计宝安区强降雨将继续加大。
      - 13时00分，深圳市气象台在全市陆地、东部海区和西部海区发布<span style="color:red;">**暴雨红色预警信号**</span>和**地質災害4級預警**。预计强降水还将持续。

<!-- end list -->

  - 6月14日
      - 00时10分，深圳市气象台在全市陆地、东部海区和西部海区将<span style="color:red;">**暴雨红色预警信号**</span>降级为<span style="color:orange;">**暴雨橙色预警信号**</span>。强降雨云系已先后减弱。
      - 05时05分，深圳市气象台在全市陆地、东部海区和西部海区将<span style="color:orange;">**暴雨橙色预警信号**</span>降级为<span style="color:yellow; background-color:silver;">**暴雨黄色预警信号**</span>。强降水云团进一步减弱。
      - 12时40分，深圳市气象台在全市陆地、东部海区和西部海区取消<span style="color:yellow; background-color:silver;">**暴雨黄色预警信号**</span>和<span style="color:yellow; background-color:silver;">**雷电预警信号**</span>。强降雨云系已经先后移出深圳市。

本次發佈的暴雨紅色預警信號為2008年首個，更持續生效了11小時10分鐘。若計入在寶安區在6月13日09时05分發佈的暴雨紅色預警信號，則為15小時05分鐘。

13日凌晨到18時，深圳市气象台共發送決策服務短信8次，服務人群6千餘人次；公眾服務短信11次，共計六百萬條；12121天氣預報查詢電話撥打量達到了2萬人次。

## 市面影響

深圳全市共出现多达1000处以上不同程度[内涝](../Page/内涝.md "wikilink")。[宝安区](../Page/宝安区.md "wikilink")、[光明新区](../Page/光明新区.md "wikilink")、[南山前海片区](../Page/南山区_\(深圳市\).md "wikilink")、[蛇口片区出现大面积水浸](../Page/蛇口街道.md "wikilink")。宝安有23间房屋倒塌，65处山体滑坡，大范围出现小区、工厂水浸，近万家企业停业。

由于[铁岗水库泄洪致使](../Page/铁岗水库.md "wikilink")[宝安](../Page/宝安.md "wikilink")[西乡街道不同程度的水浸](../Page/西乡街道.md "wikilink")，现已转移5万余人；[光明新区](../Page/光明新区.md "wikilink")[公明街道](../Page/公明街道.md "wikilink")10余个工厂被淹，水深达1.5米，受灾的1000多人已安全转移；[宝安区](../Page/宝安区.md "wikilink")[西乡街道](../Page/西乡街道.md "wikilink")1厂房水淹3米，被困约300人已全被转移；[光明新区受灾社区达](../Page/光明新区.md "wikilink")28个，受灾人口达32万，主动转移人数32100人，受水浸面积达70[平方公里](../Page/平方公里.md "wikilink")，房屋倒塌41间，各避险中心安置人员4750人。

[罗湖区金湖山庄边坡出现塌方](../Page/罗湖区.md "wikilink")，留医部[东门北路地铁施工工地出现地基下陷](../Page/东门北路.md "wikilink")，均已得到处理；[南山](../Page/南山区_\(深圳市\).md "wikilink")[南头街道](../Page/南头街道.md "wikilink")、[南山街道](../Page/南山街道_\(深圳市\).md "wikilink")、[粤海街道](../Page/粤海街道.md "wikilink")、[招商街道](../Page/招商街道.md "wikilink")、[桃源街道等出现](../Page/桃源街道_\(深圳市\).md "wikilink")30－100厘米不等的积水，倒塌瓦房7间，山体滑坡3处，[南山区出动抢险人员](../Page/南山区_\(深圳市\).md "wikilink")2500多人（武警官兵120多人），转移群众2680人；13日21时，[长岭陂水库至](../Page/长岭陂水库.md "wikilink")[西丽水库的输水明渠约](../Page/西丽水库.md "wikilink")15米发生坍塌，100名武警官兵和100多名抢险人员及时开展了现场抢险，目前坍塌的输水明渠已修复。\[2\]

## 交通影响

### 鐵路

自13日14時，[深圳站發往](../Page/深圳站.md "wikilink")[廣州站](../Page/廣州站.md "wikilink")、[東莞站](../Page/東莞站.md "wikilink")、[石龍站](../Page/石龍站.md "wikilink")、[樟木頭站等地的火車已無法輸送旅客](../Page/樟木頭站.md "wikilink")，因此在各車站的候車室內滯留了不少旅客。15時18分，[廣深線售票廳內全部窗口改為](../Page/廣深線.md "wikilink")“臨時退票窗”。16時，10個售票窗口恢復廣深線正常售票。

### 民航

自13日17時，[深圳宝安国际机场已有近](../Page/深圳宝安国际机场.md "wikilink")130個離港航班延誤，13個離港航班被迫取消，18個抵深航班則改降在[廣州](../Page/广州白云国际机场.md "wikilink")、[福州](../Page/福州機場.md "wikilink")、[汕頭](../Page/汕頭機場.md "wikilink")、[桂林等地](../Page/桂林機場.md "wikilink")。

為了應付降雨對機場造成的影響，深圳機場在13日8時30分緊急啟動了“三防應急預案”和“不正常航班保障預案”。13日下午，深圳機場（[寶安區](../Page/寶安區.md "wikilink")）降雨強度有所減弱，延誤航班陸續起飛。

### 公路

13日，暴雨導致[廣深高速和](../Page/廣深高速.md "wikilink")[莞深高速等高速公路一度被迫關閉](../Page/莞深高速.md "wikilink")，積水最深處更達到0.7[米](../Page/米.md "wikilink")。其中廣深高速在電子屏幕上打出：一些路段積水。其中新橋鶴州段因積水，正常時行駛幾分鐘的路程卻需大概半小時。

13日下午開始，[107國道](../Page/107國道.md "wikilink")[寶安段](../Page/寶安區.md "wikilink")、[松白路段等地方積水嚴重](../Page/松白路.md "wikilink")，車輛寸步難行。部分路段積水達半[米](../Page/米.md "wikilink")。
[205國道方面](../Page/205國道.md "wikilink")，[龍崗段積水最深處達](../Page/龍崗.md "wikilink")1[米](../Page/米.md "wikilink")。

[核龍線沙灣路段](../Page/核龍線.md "wikilink")、[石葵線](../Page/石葵線.md "wikilink")、[沙徑線盤山公路](../Page/沙徑線.md "wikilink")、[布樓線](../Page/布樓線.md "wikilink")、[雪龍線和](../Page/雪龍線.md "wikilink")[西寶線等道路出現不同程度塌方](../Page/西寶線.md "wikilink")。

關內外交通要道[沙湾路在](../Page/沙湾路.md "wikilink")13日发生山体滑坡，雙向行車完全中斷。

## 停課

深圳市大部分中小學、幼兒園在深圳市氣象台發佈暴雨紅色預警信號之後，陸續於13日13時之後停課。但仍有部分學校不理會預警內容，繼續上課。學生指責學校的做法。然而，大部分学校在发布预警后匆忙让学生回家，此举与预警信号设计中“学校和托幼机构应指派专人负责保护到校的学生和入园（托）的儿童”相违背，并可能使学生陷入危险。

## 泄洪

自12日20时至14日12时，深圳市主要供水水庫水庫共增加蓄水量約5500萬[立方米](../Page/立方米.md "wikilink")。其中[西麗水庫](../Page/西麗水庫.md "wikilink")、[石岩水庫](../Page/石岩水庫.md "wikilink")、[鐵崗水庫和](../Page/鐵崗水庫.md "wikilink")[三洲田水庫最先超過防限水位](../Page/三洲田水庫.md "wikilink")，開閘泄洪。而該4水庫是至目前為止仍在泄洪的水庫。\[3\]

[深圳水庫於](../Page/深圳水庫.md "wikilink")13日19時30分開始泄洪，泄洪量達30－60立方米／秒。自14日午，深圳水库总泄洪量506万立方米；11时20分，水位已降到防限水位以下，已关闸停止泄洪。

進行了泄洪的水庫：

  - [深圳水库](../Page/深圳水库.md "wikilink")
  - [三洲田水库](../Page/三洲田水库.md "wikilink")
  - [铁岗水库](../Page/铁岗水库.md "wikilink")
  - [石岩水库](../Page/石岩水库.md "wikilink")
  - [西丽水库](../Page/西丽水库.md "wikilink")
  - [公明莲塘水库](../Page/公明莲塘水库.md "wikilink")
  - [横岗水库](../Page/横岗水库.md "wikilink")
  - [大凼水库](../Page/大凼水库.md "wikilink")
  - [石头湖水库](../Page/石头湖水库.md "wikilink")
  - [罗村水库](../Page/罗村水库.md "wikilink")
  - [畔坑水库](../Page/畔坑水库.md "wikilink")
  - [长流陂水库](../Page/长流陂水库.md "wikilink")
  - [民治水库](../Page/民治水库.md "wikilink")
  - [屋山水库](../Page/屋山水库.md "wikilink")
  - [大水坑水库](../Page/大水坑水库.md "wikilink")
  - [正坑水库](../Page/正坑水库.md "wikilink")
  - [骆马岭水库](../Page/骆马岭水库.md "wikilink")
  - [叠翠湖水库](../Page/叠翠湖水库.md "wikilink")
  - [田祖上水库](../Page/田祖上水库.md "wikilink")
  - [和尚径水库](../Page/和尚径水库.md "wikilink")
  - [石豹水库](../Page/石豹水库.md "wikilink")
  - [企炉坑水库](../Page/企炉坑水库.md "wikilink")
  - [雅宝水库](../Page/雅宝水库.md "wikilink")
  - [南坑水库](../Page/南坑水库.md "wikilink")

## 抗澇

## 伤亡

此次暴雨造成6人死亡、1人失蹤，其中4人因擋土牆坍塌死亡，因觸電死亡2人，1人受傷。\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [深圳市氣象台](http://www.szmb.gov.cn)
  - [深圳新聞網](http://www.sznews.com)

[Category:深圳灾难](../Category/深圳灾难.md "wikilink")
[Category:2008年水灾](../Category/2008年水灾.md "wikilink")
[Category:2000年代中国水灾](../Category/2000年代中国水灾.md "wikilink")
[Category:2008年中国灾难](../Category/2008年中国灾难.md "wikilink")
[Category:2008年广东](../Category/2008年广东.md "wikilink")
[Category:2008年6月](../Category/2008年6月.md "wikilink")

1.
2.
3.
4.