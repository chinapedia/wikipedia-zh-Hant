[China-SAR-HongKong_border_view-from-Shenzhen1.jpg](https://zh.wikipedia.org/wiki/File:China-SAR-HongKong_border_view-from-Shenzhen1.jpg "fig:China-SAR-HongKong_border_view-from-Shenzhen1.jpg")
[Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg](https://zh.wikipedia.org/wiki/File:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg "fig:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg")》附圖中的**深圳河**，一度成為中英的邊境界線。\]\]

**深圳河**（），[珠江三角洲水系](../Page/珠江三角洲.md "wikilink")，發源於[梧桐山](../Page/梧桐山.md "wikilink")，流经[深圳市区和](../Page/深圳.md "wikilink")[香港](../Page/香港.md "wikilink")，自东北向西南流入[深圳湾](../Page/深圳湾.md "wikilink")，出[伶仃洋](../Page/伶仃洋.md "wikilink")，是[香港與](../Page/香港.md "wikilink")[中國内地的](../Page/中國内地.md "wikilink")[界河](../Page/界河.md "wikilink")（中下游），位於香港特别行政区和[廣東省](../Page/廣東省.md "wikilink")[深圳市之間](../Page/深圳市.md "wikilink")，原屬於[香港管轄範圍](../Page/香港.md "wikilink")\[1\]，[香港回归后中心线以北划归](../Page/香港回归.md "wikilink")[深圳市管辖](../Page/深圳市.md "wikilink")。[羅湖橋和](../Page/羅湖橋.md "wikilink")[福田口岸邊境大樓等均橫跨於深圳河上](../Page/福田口岸.md "wikilink")。港深邊界除了深圳河外，還有與深圳河相連的[沙頭角河和](../Page/沙頭角河.md "wikilink")[中英街](../Page/中英街.md "wikilink")。

深圳河於歷史上稱為**明溪**，自1898年《[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")》起，更改稱為深圳河，作為[英國租借香港](../Page/英國.md "wikilink")[新界部份的](../Page/新界.md "wikilink")99年界限時期的界河。深圳河是[香港最長的](../Page/香港.md "wikilink")[河流](../Page/河流.md "wikilink")。[冷戰时期](../Page/冷戰.md "wikilink")，[大批中国内地居民逃往香港](../Page/逃港.md "wikilink")，经由深圳河，因此深圳河被國際社會稱為中國的[柏林圍牆](../Page/柏林圍牆.md "wikilink")\[2\]\[3\]\[4\]，是[共产主义地區人民逃往](../Page/共产主义.md "wikilink")[資本主義地區](../Page/資本主義.md "wikilink")「投奔自由」的世界史中之一部份。\[5\]

## 地理

深圳河通常是指[沙湾河与](../Page/沙湾河.md "wikilink")[莲塘河汇合处](../Page/莲塘河.md "wikilink")——“三岔河”以下河段，深圳河干流上游一说为沙湾河，发源於[牛尾岭](../Page/牛尾岭.md "wikilink")（與[沙頭角河幾乎是同一發源點](../Page/沙頭角河.md "wikilink")），一说为莲塘河，发源于[梧桐山](../Page/梧桐山.md "wikilink")（梧桐山屬於中國大陸深圳市境內）；由東北向西南流入[深圳灣](../Page/深圳灣.md "wikilink")，出[伶仃洋](../Page/伶仃洋.md "wikilink")，全長37公里，流域面積為312.5平方公里，河道平均比降1.1%，水系分布呈扇形；其中深圳一側占六成，主要包括深圳市[龙岗区的](../Page/龙岗区.md "wikilink")[布吉镇](../Page/布吉镇.md "wikilink")、[横岗镇](../Page/横岗镇.md "wikilink")、[平湖镇和](../Page/平湖镇_\(深圳\).md "wikilink")[罗湖区](../Page/罗湖区.md "wikilink")，香港一侧占四成，主要是香港的渔农区。主要支流有深圳一側的[布吉河](../Page/布吉河.md "wikilink")、[福田河](../Page/福田河.md "wikilink")、、[新洲河及](../Page/新洲河.md "wikilink")[香港一側的](../Page/香港.md "wikilink")[梧桐河和](../Page/梧桐河.md "wikilink")[平原河](../Page/平原河.md "wikilink")。\[6\]\[7\]\[8\]

## 氣候

深圳河流域屬南亞熱帶季風海洋性氣候，雨量豐沛而集中。4至10月份為[汛期](../Page/汛期.md "wikilink")，其中4至6月份為前汛期，後汛期為7至9月份。主要受災害性[颱風及熱帶](../Page/颱風.md "wikilink")[低壓槽影響](../Page/低壓槽.md "wikilink")，常伴有暴雨和暴風潮，加之城市化面積逐渐增加，使其具有產流快、滙流时間短的特點。風暴潮也會使深圳灣潮位抬高，導致深圳河宣泄不暢，兩岸發生洪水災害。

## 治理

「**深圳河治理工程**」由香港[渠務署和深圳市](../Page/渠務署.md "wikilink")[水务局共同承担](../Page/水务局.md "wikilink")。基本分為三期，主要目標是改善香港[北區每逢雨季的水浸情況](../Page/北區_\(香港\).md "wikilink")，其次是改善水質。第一、二及三期工程分別於1997年、2000年及2006年完成，前三期將原有長約18公里的深圳河拉直、擴闊和挖深為13.5公里的新河道，大幅度地提高了從平原河口至后海灣一段深圳河的防洪能力，從而紓緩深圳河兩岸，包括[元朗區和北區的水浸問題](../Page/元朗區.md "wikilink")\[9\]。因為河道拉直，兩地交換了部份土地，當中包括在[皇崗口岸東面原來的](../Page/皇崗口岸.md "wikilink")[曲流](../Page/曲流.md "wikilink")（[落馬洲河套區](../Page/落馬洲河套區.md "wikilink")）劃入香港境內\[10\]。

為配合[蓮塘/香園圍新口岸發展](../Page/蓮塘/香園圍口岸.md "wikilink")，[香港政府已开始规划治理深圳河第四期工程](../Page/香港政府.md "wikilink")。工程將會改善深圳河上游及蓮塘/香園圍口岸一帶的排洪能力使其能滿足新口岸的防洪標準，另外，河道治理工程需要遷移現有相關聯的邊界路及邊界保安設施。\[11\]香港政府已經撥款約4億[港元於](../Page/港元.md "wikilink")2012年3月展開前期工程，重置4.3公里邊境巡邏路，以配合往後的河道改善計劃；工程預計於2017年完成，屆時河段的防洪能力將會提升至可以抵禦50年一遇的暴雨。第4期工程治理約4.5公里長的相關河段，採用生態河道的設計概念，以修復和保護現有生態環境作為主要考慮，盡量保留天然河床和河道的自然走向，並且於河岸進行綠化，從而提高深圳河的生態價值\[12\]。

2013年8月30日，香港特别行政区[發展局與](../Page/發展局.md "wikilink")[深圳市人民政府簽訂協議書](../Page/深圳市人民政府.md "wikilink")，委託[深圳市治理深圳河辦公室管理和監督治理深圳河第](../Page/深圳市治理深圳河辦公室.md "wikilink")4期工程的首份河道工程合約。第四期餘下的工程合約預計於同年年底批出。

## 参考资料

<references />

## 注释

</span>

[S](../Category/广东河流.md "wikilink") [S](../Category/深圳地理.md "wikilink")
[Category:香港河流](../Category/香港河流.md "wikilink")
[Category:香港之最](../Category/香港之最.md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")
[S](../Category/廣東-香港邊界.md "wikilink")

1.  [《展拓香港界址專條》](https://zh.wikisource.org/wiki/展拓香港界址專條)
2.
3.
4.
5.
6.
7.
8.
9.  [深圳河治理完工130萬人免水患](http://paper.wenweipo.com/2007/04/25/HK0704250031.htm)文汇报.
    2007-04-25
10. <http://www.pland.gov.hk/misc/FCA/plan/06.jpg>
11. [渠務署 治理深圳河第IV期工程
    工程項目簡介](http://www.epd.gov.hk/eia/chi/register/profile/latest/cesb200/cesb200.pdf)渠務署
12. [深圳河四期工程預計2017年完成](http://hk.news.yahoo.com/深圳河四期工程預計2017年完成-103900517.html)《星島日報》
    2012年10月9日