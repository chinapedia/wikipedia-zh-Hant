[Shenzhen_Bay_Port_Passenger_Terminal_View1_201108.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Bay_Port_Passenger_Terminal_View1_201108.jpg "fig:Shenzhen_Bay_Port_Passenger_Terminal_View1_201108.jpg")
**深圳灣口岸**，是[中國第一个按照](../Page/中國.md "wikilink")“[一地两检](../Page/一地两检.md "wikilink")”查验模式运作、[亚洲最大的客货综合性公路](../Page/亚洲.md "wikilink")[口岸](../Page/口岸.md "wikilink")，中國国家“[十五](../Page/第十個五年計劃_\(中國\).md "wikilink")”重点建设项目。口岸位於[廣東省](../Page/廣東省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[南山區](../Page/南山區_\(深圳市\).md "wikilink")[蛇口東角頭](../Page/蛇口.md "wikilink")，佔地110公顷，藉[深圳灣公路大橋與](../Page/深圳灣公路大橋.md "wikilink")[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區的鰲磡石連接](../Page/元朗區.md "wikilink")。[中華人民共和國全國人民代表大會常務委員會為了緩解內地與香港特別行政區交往日益增多帶來的陸路通關壓力](../Page/中華人民共和國全國人民代表大會常務委員會.md "wikilink")，適應深圳市與香港特別行政區之間交通運輸和便利通關的客觀要求，促進內地和香港特別行政區之間的人員交流和經貿往來，推動兩地經濟共同發展，在深圳灣口岸內設立港方口岸區，專用於人員、交通工具、貨物的通關查驗。\[1\]

## 歷史

工程進行期間，由於深、港雙方未能突破複雜的[法制和](../Page/法律.md "wikilink")[行政問題](../Page/行政.md "wikilink")，使「一地兩檢」遲遲未能落實通過，而深圳方面的工程進度也受到延誤。2006年10月31日，第十屆[全國人大常委會二十四次會議通過](../Page/全國人大常委會.md "wikilink")《全國人大常委會關於授權香港特別行政區對深圳灣口岸港方口岸區實施管轄的決定》，授權[香港特別行政區政府在位於深圳境內的港方口岸區範圍內實行禁區式管理](../Page/香港特別行政區政府.md "wikilink")（根據《公安條例》中在2007年增加的特別禁區令），港方口岸區的範圍及使用期限都由[中华人民共和国国务院決定](../Page/中华人民共和国国务院.md "wikilink")。此等決定容許香港政府在港方口岸區內實施[香港法律](../Page/香港法律.md "wikilink")，並由香港特別行政區的執法人員管理。由於當時口岸尚未開幕，「一地兩檢」得以在開通時開始實行。雖然港方口岸區由香港管理，並為元朗警區管轄區域，但不屬於任何區議會分區，而非像1997年前劃歸元朗區；而在香港地圖中通常不視為香港一部分，只特別標示「深圳灣管制站」。

[香港特別行政區立法會於](../Page/香港特別行政區立法會.md "wikilink")2007年4月25日也通過《深圳灣口岸港方口岸區條例》，作為一地兩檢所需的本地立法。隨後政府將有關深圳灣口岸港方口岸區的附屬法例呈交立法會省覽。

然後深圳灣口岸於2007年7月1日傍晚6時連同深港西部通道一同啟用，以慶祝香港回歸十周年。由於其通車屬於慶祝[香港回歸十周年的活動之一](../Page/香港回歸十周年.md "wikilink")，所以由當時[中共中央總書記](../Page/中共中央總書記.md "wikilink")、[國家主席](../Page/中華人民共和國主席.md "wikilink")[胡錦濤與](../Page/胡錦濤.md "wikilink")[行政長官](../Page/香港特別行政區行政長官.md "wikilink")[曾蔭權等主持啟用典禮](../Page/曾蔭權.md "wikilink")。

香港與深圳為深圳灣口岸港方口岸區簽訂租約，土地使用期限至2047年6月30日，自啟用起港方每年需付逾623萬元人民幣（810萬港幣）租金予深圳市政府。租金每5年調整一次，調整幅度上限為30%。\[2\]
而此等土地租借模式亦於之後的[澳門大學新校區中應用](../Page/澳門大學新校區.md "wikilink")。

此外，[香港納稅人還需要自費](../Page/香港納稅人.md "wikilink")25億港元建築費，加向內地支付17.2億港元「土地開發費」，因此有泛民議員要求港府向中国大陆收取[高鐵](../Page/廣深港高速鐵路香港段.md "wikilink")[西九龍站](../Page/西九龍站.md "wikilink")「土地開發費」。\[3\]\[4\]因應2018年9月4日[西九龙内地口岸区租予中国大陆](../Page/西九龙内地口岸区.md "wikilink")，深圳國土局將於2019年7月1日起將深圳灣口岸下調至每年1000元人民幣的象徵式租金。\[5\]

## 港方口岸區

[201611_Sino-HK_border_at_Shenzhen_Bay_Port.jpg](https://zh.wikipedia.org/wiki/File:201611_Sino-HK_border_at_Shenzhen_Bay_Port.jpg "fig:201611_Sino-HK_border_at_Shenzhen_Bay_Port.jpg")
[全國人大常委會授權香港特別行政區自深圳灣口岸啟用之日起](../Page/全國人民代表大會常務委員會.md "wikilink")，對該口岸所設港方口岸區依照香港特別行政區法律實施管轄。香港特別行政區對深圳灣口岸港方口岸區實行禁區式管理。深圳灣口岸港方口岸區的範圍面積達415,654平方米，由[中華人民共和國國務院規定](../Page/中華人民共和國國務院.md "wikilink")。深圳灣口岸港方口岸區土地使用期限，由國務院依照有關法律的規定確定。\[6\]

### 範圍

深圳灣口岸港方口岸區範圍包括港方查驗區和與之相連接的深圳灣公路大橋部分橋面。港方查驗區總用地面積為41.565公頃（具體以界址點座標控制）。深圳灣公路大橋部分橋面是指從港方查驗區東南部邊界至粵港分界綫的部分（具體以界址點座標控制）。深圳灣口岸港方口岸區的土地使用權，由[香港特別行政區政府同廣東省](../Page/香港特別行政區政府.md "wikilink")[深圳市人民政府簽訂國有土地租賃合同](../Page/深圳市人民政府.md "wikilink")，以租賃的方式取得，土地使用期限自口岸啓用之日起至2047年6月30日止。經雙方協商並按程序報經國務院批准，可提前終止土地使用權或者在租賃期滿後續期。\[7\]由於[西九龍站內地口岸區港方每年只收取內地港幣](../Page/西九龍站內地口岸區.md "wikilink")1000元的「象徵式費用」，深圳灣口岸的香港口岸租金惹起爭議，2018年8月，深圳市政府同意由2019年7月1日起下調每年租金至1000元。\[8\]\[9\]

### 港方口岸區屬禁區

就《[公安條例](../Page/公安條例_\(香港\).md "wikilink")》（[香港法例第](../Page/香港法例.md "wikilink")245章）及任何其他適用於[禁區](../Page/香港邊境禁區.md "wikilink")（該條例第2(1)條所界定者）的成文法則的施行而言，港方口岸區屬如此界定的禁區。

### 香港特別行政區法律適用於港方口岸區

除在任何於2007年7月1日或之後制定或（如屬附屬法例）訂立的成文法則另有訂定的範圍內，[香港法律適用於港方口岸區](../Page/香港法律.md "wikilink")。為香港法律適用於港方口岸區的目的，港方口岸區視為位於香港以內的地域。如某成文法則只適用於香港某特定地域或藉提述香港不同地域而適用，則為決定該地域是否包括港方口岸區的目的，港方口岸區視為以其名稱為名而位於[新界以內](../Page/新界.md "wikilink")、[新九龍以外的地域](../Page/新九龍.md "wikilink")。\[10\]

### 香港特別行政區政府土地

就香港法律適用於港方口岸區而言，儘管港方口岸區的土地使用權是以租賃的方式取得的，在港方口岸區以內的土地視為位於香港以內的政府土地的一部分。任何港方口岸區以內的土地的權利或權益，如已憑藉在2007年7月1日或之後進行的交易而處置，則視為直接或間接（視屬何情況而定）得自香港特別行政區政府的權利或權益。

### 香港法院的司法管轄權

香港特別行政區法院具有[司法管轄權聆訊和裁定因本條例的施行而產生的任何民事或刑事訟案或事宜](../Page/司法管轄權.md "wikilink")，一如它就香港法律在香港的施行而具有司法管轄權一樣。在不損害上句的一般性的原則下，法院具有權力作出裁定、賦予或委予某項地域界限局限於港方口岸區或包括港方口岸區的權利或義務的法院命令。

### 條例期滿失效

《深圳灣口岸港方口岸區條例》（[香港法例第](../Page/香港法例.md "wikilink")591章）於2047年6月30日午夜12時期滿失效，該日期即前述的以租賃的方式取得的港方口岸區土地使用權的土地使用期限期滿之日。如土地使用權提前終止或租賃在期滿後續期，香港特別行政區政府[保安局局長須藉](../Page/保安局.md "wikilink")[香港特別行政區政府憲報公告公布土地使用權或租賃](../Page/香港特別行政區政府憲報.md "wikilink")（經如此提前終止或續期者）期滿的日期，而本條例於該日期午夜12時期滿失效。
\[11\]

### 設施管理

目前香港口岸日常營運（包括保安、洗手間、清潔、維修）已外判予[中國海外旗下物業管理公司](../Page/中國海外.md "wikilink")。

## 設計

[Shenzhen_Bay_Bridge_To_HK_on_Shenzhen_Side.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Bay_Bridge_To_HK_on_Shenzhen_Side.jpg "fig:Shenzhen_Bay_Bridge_To_HK_on_Shenzhen_Side.jpg")
深圳灣口岸位於深圳市[南山區](../Page/南山区_\(深圳市\).md "wikilink")[蛇口東角頭的一塊填海地](../Page/蛇口.md "wikilink")，佔地117.9[公頃](../Page/公頃.md "wikilink")。在口岸南侧、[深圳灣公路大橋北引桥处設有車輛轉線設施](../Page/深圳灣公路大橋.md "wikilink")，以配合香港和中國大陸相反的[行車方向](../Page/行車方向.md "wikilink")，然後通過[深港西部通道](../Page/深港西部通道.md "wikilink")（即[深圳灣公路大橋](../Page/深圳灣公路大橋.md "wikilink")）連接[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區的](../Page/元朗區.md "wikilink")[鰲磡石](../Page/鰲磡石.md "wikilink")。但极为特殊的，港方口岸区从[深圳湾大桥北引桥开始的](../Page/深圳湾大桥.md "wikilink")[道路通行方向不与香港法律所规定的靠左行驶一致](../Page/道路通行方向.md "wikilink")，而是与[中国大陆行车方向一致的靠右行驶](../Page/中国大陆.md "wikilink")\[12\]。

而口岸建築物則由關善明建築師事務所（香港建築署負責項目管理）及深圳市建築設計研究院第三設計院\[13\]合作設計（其中聯檢大樓及其他港方建築物按香港《建築物條例》標準建造）。

港方口岸區由通電圍網（不附鐵絲網）及綠化緩衝區包圍，與[深圳灣公園及](../Page/深圳灣公園.md "wikilink")[深圳地鐵](../Page/深圳地鐵.md "wikilink")[2號綫](../Page/深圳地铁2号线.md "wikilink")[后海停車場分隔](../Page/后海停車場.md "wikilink")。

此口岸現時的通關時間約為每日06:30至00:00\[14\]。此關口特別之處在於實行「一地兩檢」，即香港及中國大陸邊防人員在同一所大樓內進行檢查工作；雙方人員分別在不同但相近的地點，替旅客辦理檢查工作，但不會只由任何一方負責。這樣的安排可以節省通關時間，過境旅客只需要下車一次，進行兩次檢查然後上車離開，省下一次上車及下車手續。

深圳灣口岸聯檢大樓深港兩方整體的[食水及](../Page/食水.md "wikilink")[燃氣供應分別由深圳市自來水](../Page/燃氣.md "wikilink")（集團）有限公司和深圳市燃氣集團提供（而港方燃氣則採用中央石油氣）；渠務方面由深圳市水務局負責，但港方部份之渠蓋及渠道由香港政府提供及管理；[電力方面聯檢大樓深方部份由廣東電網公司深圳供電局提供](../Page/電力.md "wikilink")，港方部份則由香港[中華電力提供](../Page/中華電力.md "wikilink")（然而地底電纜[混凝土渠蓋則採用中國格式並以簡體字標註](../Page/混凝土.md "wikilink")）；[電話和](../Page/電話.md "wikilink")[通訊方面聯檢大樓深方部分由](../Page/通訊.md "wikilink")[中國電信深圳分公司提供](../Page/中國電信.md "wikilink")，港方部分則由香港[和記環球電訊提供](../Page/和記環球電訊.md "wikilink")。

[深圳灣公路大橋於深圳灣口岸香港口岸區界線至深圳灣公園岸邊位置](../Page/深圳灣公路大橋.md "wikilink")，橋上起6米高適用[香港法律](../Page/香港法律.md "wikilink")，橋下[望海路及其領空則適用](../Page/望海路.md "wikilink")[中華人民共和國法律](../Page/中華人民共和國法律.md "wikilink")，此[埠間與](../Page/埠間.md "wikilink")[司法管轄區立體交叉分區之措施為全球同類少數特例](../Page/司法管轄區.md "wikilink")。

## 利用狀況

| 年份          | 全年過境旅客人次   | 日均過境旅客人次 |
| ----------- | ---------- | -------- |
| 2009年       | 17,905,127 |          |
| 2011年       | 28,695,578 | 78,618   |
| 2012年       | 32,110,842 | 87,735   |
| 2013年       | 36,258,108 | 99,337   |
| 2014年       | 40,970,705 | 112,249  |
| 2015年\[15\] | 41,524,479 | 113,766  |
|             |            |          |

口岸開通初期使用旅客不多。隨南山中心區及口岸周邊建築陸續落成，吸引更多香港人前往南山區消費，而由於從深圳灣口岸過關只需上落一次，節省等候乘客過關時間，加上[廣深沿江高速開通](../Page/廣深沿江高速.md "wikilink")，由於口岸連接[東濱隧道直達](../Page/東濱隧道.md "wikilink")[廣深沿江高速的入口](../Page/廣深沿江高速.md "wikilink")，令來往珠三角西岸及廣東省西部、廣州與香港更便捷，比使用[皇崗口岸和](../Page/皇崗口岸.md "wikilink")[廣深高速車程節省一小時以上](../Page/廣深高速.md "wikilink")，吸引不少過境巴士轉用。自2014年起，深圳灣管制站的使用人次首次超越落馬洲管制站，成為目前香港使用人次最多的公路管制站\[16\]。在車流量上，雖然遠遠不達到設計車流量日均逾60,000架次，但在2016年仍有日均11,000架次使用，為主要港深跨境通道之一。此外，在2018年12月28日凌晨，[輕鐵第五期列車首兩卡車輛經深圳灣口岸及](../Page/輕鐵第四、五期列車.md "wikilink")[深圳灣公路大橋運抵香港](../Page/深圳灣公路大橋.md "wikilink")，是本港首次使用跨海大橋作列車乙種運送，亦是目前唯一曾有乙種運送車隊使用的口岸。

## 事故

深圳灣口岸深圳口岸區曾經發生壞電腦事故，引致[深圳灣公路大橋大塞車](../Page/深圳灣公路大橋.md "wikilink")\[17\]
。

## 惡劣天氣下之影響

因連接深圳灣口岸的[深圳灣公路大橋是跨海大橋](../Page/深圳灣公路大橋.md "wikilink")，在惡劣天氣（如[八號烈風或暴風信號生效時](../Page/八號烈風或暴風信號.md "wikilink")）下[深圳灣公路大橋將暫停開放](../Page/深圳灣公路大橋.md "wikilink")，深圳灣口岸因而暫停服務\[18\]。如[皇崗口岸仍然開放](../Page/皇崗口岸.md "wikilink")，持有過境車輛封閉道路通行許可證的車輛可改用[皇崗口岸](../Page/皇崗口岸.md "wikilink")\[19\]。

## 接駁交通

深圳灣口岸啟用當日，港方共有三條專利巴士路線（**[B2](../Page/新大嶼山巴士B2線.md "wikilink")**、**[B3](../Page/城巴B3線.md "wikilink")**及**B3X**）及一條專線小巴路線（**[新界專線小巴618線](../Page/新界區專線小巴618線.md "wikilink")**）於傍晚6時起投入服務，\[20\]\[21\]，而深圳方面亦有兩條巴士路線投入服務（**90**及**機場8線**），並將一條現有巴士路線**245**線遷往深圳灣口岸。另外，由[永東直巴管理有限公司營辦](../Page/永東直通巴士.md "wikilink")，往來[香港國際機場至深圳灣口岸的](../Page/香港國際機場.md "wikilink")「-{航天}-新幹線」，亦於2007年7月13日起投入服務。从深圳方向前往香港机场的旅客不需步行过关，只需从深圳侧搭乘去香港机场的巴士过关，入关手续可以乘车时办理。

由2007年12月26日起，[深圳巴士集團增闢一條新線](../Page/深圳巴士集团公司.md "wikilink")**98**，往來深圳灣口岸至[新城聯檢站](../Page/新城聯檢站.md "wikilink")（即寶安大道與[深南大道交界的檢查站](../Page/深南大道.md "wikilink")），方便[南山西部及](../Page/南山区.md "wikilink")[寶安區民眾往來口岸過境](../Page/寶安區.md "wikilink")。2008年1月18日起，路線**31**及**217**亦改以深圳灣口岸為起站，**31**路線提供了往[海上世界](../Page/深圳海上世界.md "wikilink")、南海酒店一帶的直接巴士服務，而**217**則為前往南油一帶乘客提供**245**外多一個選擇，**217**亦是往來口岸與市區最便宜的巴士服務，非空調巴士收費為￥1，空調巴士￥2。

2008年7月26日起，[嶼巴新線](../Page/嶼巴.md "wikilink")**[B2P](../Page/新大嶼山巴士B2P線.md "wikilink")**投入服務，初時往來深圳灣口岸至[天水圍市中心](../Page/天水圍市中心.md "wikilink")，途經[藍地](../Page/藍地.md "wikilink")、[洪水橋](../Page/洪水橋.md "wikilink")、[天耀邨及](../Page/天耀邨.md "wikilink")[天瑞邨](../Page/天瑞邨.md "wikilink")，只於星期六及假日提供服務，為天水圍居民提供**[B2](../Page/新大嶼山巴士B2線.md "wikilink")**線以外的另一個選擇，2009年1月10日起總站遷往[天慈邨](../Page/天慈邨.md "wikilink")，同年7月13日起正式提供每天全日服務。\[22\]2008年8月16日開始，城巴路線**[B3](../Page/城巴B3線.md "wikilink")**被分拆為**[B3](../Page/城巴B3線.md "wikilink")**及**B3A**兩條路線，後者取代了**B3**在屯門西北區的服務。

2010年代期間，[中旅汽車更增加了由該地到](../Page/中旅汽車.md "wikilink")[香港各大旅遊景點之路邊](../Page/香港旅遊景點.md "wikilink")，例如來往[香港海洋公園](../Page/香港海洋公園.md "wikilink")、[香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")、[亞洲國際博覽館等](../Page/亞洲國際博覽館.md "wikilink")。

###  香港方面

[Shenzhen_Bay_Port_Passenger_Terminal_HK_Bus_Stop_2011.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Bay_Port_Passenger_Terminal_HK_Bus_Stop_2011.jpg "fig:Shenzhen_Bay_Port_Passenger_Terminal_HK_Bus_Stop_2011.jpg")\]\]

#### [專營巴士服務](../Page/香港巴士.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>起訖點</p></th>
<th><p>途經地</p></th>
<th><p>車費</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/嶼巴B2線.md" title="wikilink">B2</a></p></td>
<td></td>
<td><p><a href="../Page/藍地.md" title="wikilink">藍地</a>、<a href="../Page/洪水橋.md" title="wikilink">洪水橋</a>、<a href="../Page/屏山_(香港).md" title="wikilink">屏山</a>、<a href="../Page/元朗市中心.md" title="wikilink">元朗市中心</a></p></td>
<td><p>HK$8.5－12</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/嶼巴B2P線.md" title="wikilink">B2P</a></p></td>
<td></td>
<td><p><a href="../Page/天耀邨.md" title="wikilink">天耀邨</a>、<a href="../Page/天瑞邨.md" title="wikilink">天瑞邨</a>、<a href="../Page/天水圍市中心.md" title="wikilink">天水圍市中心</a></p></td>
<td><p>HK$8.5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新大嶼山巴士B2X線.md" title="wikilink">B2X</a></p></td>
<td></td>
<td><p>天耀邨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/城巴B3線.md" title="wikilink">B3</a></p></td>
<td></td>
<td><p><a href="../Page/新屯門中心.md" title="wikilink">新屯門中心</a>、<a href="../Page/豐景園.md" title="wikilink">豐景園</a>、<a href="../Page/置樂花園.md" title="wikilink">置樂花園</a>、<a href="../Page/屯門市中心.md" title="wikilink">屯門市中心</a></p></td>
<td><p>HK$11.8</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/城巴B3A線.md" title="wikilink">B3A</a></p></td>
<td></td>
<td><p><a href="../Page/大興邨.md" title="wikilink">大興</a>、<a href="../Page/良景邨.md" title="wikilink">良景</a>、<a href="../Page/田景邨.md" title="wikilink">田景</a>、<a href="../Page/寶田邨.md" title="wikilink">寶田</a>、<a href="../Page/兆康苑.md" title="wikilink">兆康苑</a>、<a href="../Page/富泰邨.md" title="wikilink">富泰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/城巴B3M線.md" title="wikilink">B3M</a></p></td>
<td></td>
<td><p><a href="../Page/屯門公路.md" title="wikilink">屯門公路</a>（<a href="../Page/紅橋_(香港).md" title="wikilink">紅橋</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/城巴B3X線.md" title="wikilink">B3X</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 跨境巴士

[Shenzhen_Bay_Port_Passenger_Terminal_Bus_stop_2011.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Bay_Port_Passenger_Terminal_Bus_stop_2011.jpg "fig:Shenzhen_Bay_Port_Passenger_Terminal_Bus_stop_2011.jpg")
現時不少來往香港及[廣東省的跨境巴士均利用深圳灣口岸往返兩地](../Page/廣東省.md "wikilink")，亦利用深圳灣口岸作轉乘之用。部分路線容許乘客購票來往深圳灣口岸及香港市區，並在深圳灣口岸深圳入口設置售票處；這些路線包括：

  - **[環島旅運](../Page/環島旅運.md "wikilink")[深圳灣專線](../Page/環島深圳灣專線.md "wikilink")**

<!-- end list -->

  -

      -
        [尖沙咀專線](../Page/環島尖沙咀-深圳灣專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        ↔ 深圳灣口岸 ↔
        [尖沙咀](../Page/尖沙咀.md "wikilink")（[海港城](../Page/海港城.md "wikilink")）
        [旺角(太子)專線](../Page/環島太子-深圳灣專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        ↔ 深圳灣口岸 ↔
        [太子](../Page/太子.md "wikilink")（[砵蘭街](../Page/砵蘭街.md "wikilink")）
        [上環專線](../Page/環島上環-深圳灣專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        ↔ 深圳灣口岸 ↔
        [上環](../Page/上環.md "wikilink")（[信德中心](../Page/信德中心.md "wikilink")）
        [銅鑼灣專線](../Page/環島銅鑼灣-深圳灣專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        ↔ 深圳灣口岸 ↔
        [銅鑼灣](../Page/銅鑼灣.md "wikilink")（[時代廣場](../Page/時代廣場.md "wikilink")）
        [海洋公園專線](../Page/環島消遙遊_海洋公園專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        ↔ 深圳灣口岸 ↔ [海洋公園](../Page/海洋公園.md "wikilink")
        [迪士尼樂園專線](../Page/環島消遙遊_迪士尼樂園專線.md "wikilink")：[深圳機場](../Page/深圳寶安國際機場.md "wikilink")
        → 深圳灣口岸 → [迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")

經深圳灣口岸前往廣東省其他地方：

  - [廣州](../Page/廣州.md "wikilink")
      - [番禺](../Page/番禺.md "wikilink")
  - [中山](../Page/中山.md "wikilink")
  - [江門](../Page/江門.md "wikilink")
      - [新會](../Page/新會.md "wikilink")
      - [台山](../Page/台山.md "wikilink")
      - [開平](../Page/開平.md "wikilink")
      - [恩平](../Page/恩平.md "wikilink")
  - [陽春](../Page/陽春.md "wikilink")（經[新興](../Page/新興.md "wikilink")）

<!-- end list -->

  - **[深西特快](../Page/深西特快.md "wikilink")**

<!-- end list -->

  -

      -
        [世界之窗公交總站](../Page/世界之窗.md "wikilink") ↔ 深圳灣口岸 ↔
        [九龍塘港鐵站](../Page/九龍塘港鐵站.md "wikilink")（往香港方向經[太子](../Page/太子_\(香港\).md "wikilink")）

<!-- end list -->

  - **[西部通道快線](../Page/西部通道快線.md "wikilink")**\[23\]

<!-- end list -->

  -

      -
        [深圳機場](../Page/深圳寶安國際機場.md "wikilink") ↔ 深圳灣口岸↔
        [九龍站](../Page/九龍站.md "wikilink")[圓方](../Page/圓方.md "wikilink")

<!-- end list -->

  - **[深圳灣跨境快線](../Page/深圳灣跨境快線.md "wikilink")**

<!-- end list -->

  -

      -
        [京基百纳广场](../Page/京基百纳广场.md "wikilink") ↔ 深圳灣口岸 ↔
        [葵芳](../Page/葵芳.md "wikilink") ↔
        [油麻地](../Page/油麻地.md "wikilink") ↔
        [西灣河](../Page/西灣河.md "wikilink")[嘉亨灣](../Page/嘉亨灣.md "wikilink")

<!-- end list -->

  - [**-{航天}-新幹線**](../Page/深圳灣過境巴士機場線.md "wikilink")

<!-- end list -->

  -

      -
        深圳灣口岸 ↔ [香港國際機場](../Page/香港國際機場.md "wikilink")

<!-- end list -->

  - **[深圳灣跨境巴士東涌東薈城線](../Page/深圳灣跨境巴士東涌東薈城線.md "wikilink")**

<!-- end list -->

  -

      -
        深圳灣口岸 ↔
        [東涌](../Page/東涌.md "wikilink")[東薈城](../Page/東薈城.md "wikilink")

<!-- end list -->

  - **[東九龍快線](../Page/東九龍快線.md "wikilink")**

<!-- end list -->

  -

      -
        深圳灣口岸 ↔
        [觀塘](../Page/觀塘.md "wikilink")[馬蹄徑](../Page/馬蹄徑.md "wikilink")
        ↔ [坑口](../Page/坑口.md "wikilink")[寶順路](../Page/寶順路.md "wikilink")
        ↔
        [將軍澳](../Page/將軍澳.md "wikilink")[健明邨](../Page/健明邨.md "wikilink")
        亦有支線來往[油塘](../Page/油塘.md "wikilink")[大本型](../Page/大本型.md "wikilink")

<!-- end list -->

  - **[永東](../Page/永東.md "wikilink")[屯門穿梭巴士](../Page/屯門區.md "wikilink")**

<!-- end list -->

  -

      -
        深圳灣口岸 ↔
        [屯門站](../Page/屯門站_\(西鐵綫\).md "wikilink")[瓏門](../Page/瓏門.md "wikilink")

#### [專線小巴](../Page/香港小巴.md "wikilink")

  - **[618](../Page/新界區專線小巴618線.md "wikilink")** - 深圳灣口岸 ↔
    [天恩邨](../Page/天恩邨.md "wikilink")\[24\]（途經天恆邨、香港濕地公園、麗湖居、天頌苑、廈村），車費HK$12

#### [的士](../Page/香港的士.md "wikilink")

**<span style="color: red;">市區</span>**及**<span style="color: #00B14F;">新界的士</span>**可以駛入深圳灣口岸港方口岸區接載乘客。

###  深圳方面

由於深圳灣口岸為填海地塊，遠離[南山區的傳統民居區](../Page/南山区_\(深圳市\).md "wikilink")，所以現階段只有巴士或的士可以到達。2010年12月[深圳地鐵2號綫通車](../Page/深圳地铁2号线.md "wikilink")，现有的公交巴士可轉乘距離口岸最近的地鐵站[登良站及](../Page/登良站.md "wikilink")[海月站](../Page/海月站.md "wikilink")，即使乘坐的士前往南山著名的購物娛樂中心“海岸城”地鐵站[-{后}-海站](../Page/后海站.md "wikilink")，車資12元亦已足夠（10元起跳，另需缴纳燃油附加费）。

#### 巴士

[Shenzhen_Bay_Port_Bus_Terminus(Shenzhen).jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Bay_Port_Bus_Terminus\(Shenzhen\).jpg "fig:Shenzhen_Bay_Port_Bus_Terminus(Shenzhen).jpg")
深圳灣口岸北方為公交總站，亦設有充電站，方便電力驅動的公交車充電。

| 线路                                           | 起讫站                                                                              | 全程票价 | 备注                                                                                                                                                         |
| -------------------------------------------- | -------------------------------------------------------------------------------- | ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [深圳巴士集团](../Page/深圳巴士集团.md "wikilink")       |                                                                                  |      |                                                                                                                                                            |
| [90](../Page/深圳公交90路.md "wikilink")          | [深圳灣口岸](../Page/深圳灣口岸公共運輸交匯處.md "wikilink") ↔ [世界之窗](../Page/世界之窗.md "wikilink") | ￥2   |                                                                                                                                                            |
| [B737](../Page/深圳公交B737路.md "wikilink")      | 深圳灣口岸 ↔ [東角頭](../Page/東角頭.md "wikilink")                                         | ￥1   | 可於海月站轉乘[SZMetro_flatlogo.svg](https://zh.wikipedia.org/wiki/File:SZMetro_flatlogo.svg "fig:SZMetro_flatlogo.svg")[深圳地鐵二號線](../Page/深圳地鐵二號線.md "wikilink") |
| [B817](../Page/深圳公交B817路.md "wikilink")      | 深圳灣口岸 ↔ [桃花園](../Page/桃花園.md "wikilink")                                         | ￥1   |                                                                                                                                                            |
| [M123](../Page/深圳公交M123路.md "wikilink")      | 深圳灣口岸 ↔ [秀峰工業園](../Page/秀峰工業園.md "wikilink")                                     | ￥2.5 | 原123綫                                                                                                                                                      |
| [M299](../Page/深圳公交M299路.md "wikilink")      | 深圳灣口岸 ↔ [深圳北站](../Page/深圳北站.md "wikilink")                                       | ￥2.5 |                                                                                                                                                            |
| [M398](../Page/深圳公交M398路.md "wikilink")      | 深圳灣口岸 ↔ [坂田互聯E時代](../Page/坂田互聯E時代.md "wikilink")                                 | ￥2.5 |                                                                                                                                                            |
| [M409](../Page/深圳公交M409路.md "wikilink")      | 深圳灣口岸 ↔ [月亮灣](../Page/月亮灣.md "wikilink")[填海區](../Page/填海區.md "wikilink")         | ￥2   |                                                                                                                                                            |
| [M474](../Page/深圳公交M474路.md "wikilink")      | 深圳灣口岸 ↔ [桃園村](../Page/桃園村.md "wikilink")                                         | ￥2   |                                                                                                                                                            |
| [M506](../Page/深圳公交M506路.md "wikilink")      | 深圳灣口岸 ↔ [中山園](../Page/中山園.md "wikilink")                                         | ￥2   | 原B813綫                                                                                                                                                     |
| [M507](../Page/深圳公交M507路.md "wikilink")      | 深圳灣口岸 ↔ [西鄉大鏟灣碼頭](../Page/西鄉大鏟灣碼頭.md "wikilink")                                 | ￥2   | 原B688綫                                                                                                                                                     |
| [M528](../Page/深圳公交M528路.md "wikilink")      | 深圳灣口岸 ↔ [寶安機場](../Page/寶安機場.md "wikilink")                                       | ￥3   | 原机场8线，分段收费                                                                                                                                                 |
| [N26](../Page/深圳公交N26路.md "wikilink")        | 深圳灣口岸 ↔ [蛇口](../Page/蛇口.md "wikilink")[沃爾瑪](../Page/沃爾瑪.md "wikilink")           | ￥2   | 夜班线路                                                                                                                                                       |
| [N27](../Page/深圳公交N27路.md "wikilink")        | 深圳灣口岸 ↔ [西鄉](../Page/西鄉.md "wikilink")[大鏟灣碼頭](../Page/大鏟灣碼頭.md "wikilink")       | ￥3   | 夜班线路                                                                                                                                                       |
| [高峰120](../Page/深圳公交高峰專線120.md "wikilink")   | 深圳灣口岸 ↔ [深圳北站](../Page/深圳北站.md "wikilink")                                       | ￥2.5 | 高峰路線，終點與m299線相同                                                                                                                                            |
| [深圳西部公汽](../Page/深圳西部公共汽车有限公司.md "wikilink") |                                                                                  |      |                                                                                                                                                            |
| [E19](../Page/深圳公交E19路.md "wikilink")        | 深圳灣口岸 ↔ [公明](../Page/公明.md "wikilink")[馬山頭](../Page/馬山頭.md "wikilink")           | ￥10  |                                                                                                                                                            |
| [M206](../Page/深圳公交M206路.md "wikilink")      | 深圳灣口岸 ↔ [光明](../Page/光明.md "wikilink")[東周](../Page/東周.md "wikilink")             | ￥8   | 分段收费                                                                                                                                                       |
| [高快24](../Page/深圳公交高快巴士24路.md "wikilink")    | 深圳灣口岸 ↔ [光明](../Page/光明.md "wikilink")[光晟廠](../Page/光晟廠.md "wikilink")           | ￥10  | 高峰专线                                                                                                                                                       |
| [深圳市新通寶運輸](../Page/深圳市新通寶運輸.md "wikilink")   |                                                                                  |      |                                                                                                                                                            |
| [旅游9](../Page/旅游9.md "wikilink")             | 深圳灣口岸 ↔ [大運城邦](../Page/大运城邦.md "wikilink")                                       | ￥2.6 | 旅游专线，分段收费                                                                                                                                                  |
|                                              |                                                                                  |      |                                                                                                                                                            |

服務時間，以**90**最長，末班车時間為23:30；支線巴士（**B線**）最短，末班车時間一般為20:00。

#### [地铁](../Page/深圳地铁.md "wikilink")

  - [SZMetro_flatlogo.svg](https://zh.wikipedia.org/wiki/File:SZMetro_flatlogo.svg "fig:SZMetro_flatlogo.svg")
    [深圳地鐵13号線](../Page/深圳地鐵13號線.md "wikilink")[深圳灣口岸站](../Page/深圳灣口岸站.md "wikilink")（興建中）
  - [SZMetro_flatlogo.svg](https://zh.wikipedia.org/wiki/File:SZMetro_flatlogo.svg "fig:SZMetro_flatlogo.svg")
    [深圳地鐵2号線](../Page/深圳地铁2号线.md "wikilink")[海月站](../Page/海月站.md "wikilink")（步行15分鐘）

#### [的士](../Page/深圳出租车.md "wikilink")

深圳市**<span style="color: red;">红色出租车</span>（含蓝色车身电动出租车）**可以驶入深圳湾口岸深方口岸区的士站上落乘客。

#### 長途巴士

香港過境至內地各城市，如[東莞](../Page/東莞.md "wikilink")、[佛山](../Page/佛山.md "wikilink")、[廣州等地方](../Page/廣州.md "wikilink")，有空位可以即時購票上車。

往[中山](../Page/中山.md "wikilink")[石岐有三間公司提供](../Page/石岐.md "wikilink")：
\***中港通**（票價80元）：09:00-21:30，30-60分鐘一班

  - **香港中旅**（票價60元）：09:00、10:00、11:00、14:30、17:00、20:00
  - **粤港榮利**（票價80元）：08:45-21:30，30-60分鐘一班

深圳灣口岸也設有長途巴士站，惟大巴需停靠深圳其他汽車站，車程較過境巴士長。

另外，也有跨境巴士或其接駁客車從深圳灣口岸來往深圳市內地點，如寶安中心（停宏發領域）、京基百納廣場、世界之窗、[深圳機場等](../Page/深圳機場.md "wikilink")，惟這些路綫或不允許在深圳灣口岸購票上車。

## 參考文獻

## 相關條目

  - [深港西部通道](../Page/深港西部通道.md "wikilink")
  - [港珠澳大橋香港口岸](../Page/港珠澳大橋香港口岸.md "wikilink")
  - [皇崗口岸](../Page/皇崗口岸.md "wikilink")/[落馬洲管制站](../Page/落馬洲管制站.md "wikilink")
  - [福田口岸](../Page/福田口岸.md "wikilink")
  - [羅湖口岸](../Page/羅湖口岸.md "wikilink")
  - [文錦渡口岸](../Page/文錦渡口岸.md "wikilink")
  - [蓮塘/香園圍口岸](../Page/蓮塘/香園圍口岸.md "wikilink") (預計2019年啟用)
  - [沙頭角口岸](../Page/沙頭角口岸.md "wikilink")
  - [香港西九龍站](../Page/香港西九龍站.md "wikilink")
  - [紅磡站](../Page/紅磡站.md "wikilink")

## 外部連結

  - 《[全國人大常委會關於授權香港特別行政區對深圳灣口岸港方口岸區實施管轄的決定](http://npc.people.com.cn/GB/15017/4982121.html)》
  - 《[香港法例第591章 -
    深圳灣口岸港方口岸區條例](https://www.elegislation.gov.hk/hk/cap591!zh-Hant-HK)》
  - [前往深圳灣口岸注意事項](http://www.td.gov.hk/tc/transport_in_hong_kong/access_to_shenzhen_bay_port/index.html)，[香港運輸署](../Page/香港運輸署.md "wikilink")

{{-}}

[Category:香港出入境管制站](../Category/香港出入境管制站.md "wikilink")
[Category:港深口岸](../Category/港深口岸.md "wikilink")
[Category:深圳口岸](../Category/深圳口岸.md "wikilink")
[Category:深圳市南山区](../Category/深圳市南山区.md "wikilink")
[Category:一國兩制](../Category/一國兩制.md "wikilink")
[Category:中华人民共和国口岸](../Category/中华人民共和国口岸.md "wikilink")

1.

2.  [深圳灣口岸兩檢
    港付42億](http://hk.apple.nextmedia.com/news/art/20150516/19149604)
    蘋果日報，2015年5月16日

3.

4.

5.

6.
7.

8.

9.

10.
11.
12.

13.

14. [出入境管制站介紹](http://www.gov.hk/tc/residents/immigration/control/location.htm)，香港入境事務處

15.

16.
17.

18. 文匯報 - [受颱風影響
    深圳灣口岸暫停通關](http://news.wenweipo.com/2016/08/02/IN1608020010.htm)

19.

20. 《[立法會交通事務委員會深港西部通道啟用後的交通及運輸安排](http://www.legco.gov.hk/yr06-07/chinese/panels/tp/papers/tp0302cb1-1004-5-c.pdf)》2007年3月2日

21. 《[西部通道3巴士線單程11元 屯門元朗往返
    城巴嶼巴經營](http://hk.news.yahoo.com/070622/12/2a2pm.html)》
    - Yahoo\! 新聞，2007年6月23日

22.

23. [中港通 深圳機場\<\>九龍站](http://www.chinalink.hk/new5.html)

24. 《[第1951號公告—道路交通 (公共服務車輛) 規例 (第374D章)--新界的公共小型巴士 (專線)
    路線](http://www.gld.gov.hk/cgi-bin/gld/egazette/gazettefiles.cgi?lang=c&year=2007&month=3&day=23&vol=11&no=12&gn=1951&header=1&part=0&df=1&nt=gn&newfile=1&acurrentpage=12&agree=1&gaz_type=mg)》，2007年3月23日