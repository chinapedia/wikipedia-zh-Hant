**深川市**（）是位於[北海道](../Page/北海道.md "wikilink")[空知綜合振興局北空知地區的主要](../Page/空知綜合振興局.md "wikilink")[城市](../Page/城市.md "wikilink")，是空知北部的交通中心，有許多[鐵路和](../Page/鐵路.md "wikilink")[公路在此交會](../Page/公路.md "wikilink")。該市[蕎麥的種植面積是全日本第二](../Page/蕎麥.md "wikilink")。\[1\]\[2\]

市名源自轄區內[大鳳川的](../Page/大鳳川.md "wikilink")[阿伊努語名稱](../Page/阿伊努語.md "wikilink")「oho-nay」，意思是「深的河川」。\[3\]

## 歷史

連結[札幌至](../Page/札幌.md "wikilink")[旭川間的上川道路通車後](../Page/旭川.md "wikilink")，由於大量的開拓者經由此道路前往[上川地區](../Page/上川支廳.md "wikilink")，於是在沿路上連續設置了許多驛站，1889年便設置了音江法華驛站，也開始帶動附近地區的發展。\[4\]

同樣在1889年，三條公爵、蜂須賀侯爵、菊亭侯爵等六名[華族](../Page/華族_\(日本\).md "wikilink")（日本當時的[貴族](../Page/貴族.md "wikilink")）決定向政府借用雨龍原野的土地成立「華族聯合雨龍農場」（），開始徵募大批移民及屯田兵進入開墾。

### 年表

  - 1889年：連結[札幌至](../Page/札幌.md "wikilink")[旭川間的上川道路開通](../Page/旭川.md "wikilink")。\[5\]
  - 1890年：設置華族組合農場。
  - 1892年：[北海道廳將原屬於](../Page/北海道廳.md "wikilink")[幌加內町的](../Page/幌加內町.md "wikilink")[雨龍川左岸一帶設置深川村](../Page/雨龍川.md "wikilink")，並以三條公爵、蜂須賀侯爵、菊亭侯爵所設置的農場及[屯田兵為主要開拓中心](../Page/屯田兵.md "wikilink")。
  - 1901年10月16日：秩父別村（現在的[秩父別町](../Page/秩父別町.md "wikilink")）分村。
  - 1902年4月1日：成為北海道二級村。
  - 1907年4月1日：成為北海道一級村。
  - 1918年1月1日：改制為深川町。
  - 1923年1月1日：妹背牛村（現在的[妹背牛町](../Page/妹背牛町.md "wikilink")）從深川町分村。
  - 1963年5月1日：[深川町](../Page/深川町.md "wikilink")、[一已村](../Page/一已村.md "wikilink")、[納內村](../Page/納內村.md "wikilink")、[音江村合併為深川市](../Page/音江村.md "wikilink")。
  - 1970年4月1日：[多度志町併入深川市](../Page/多度志町.md "wikilink")。

## 交通

### 機場

  - [旭川機場](../Page/旭川機場.md "wikilink")（位於[東神樂町](../Page/東神樂町.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [函館本線](../Page/函館本線.md "wikilink")：[深川車站](../Page/深川車站.md "wikilink")
        - [納內車站](../Page/納內車站.md "wikilink")
      - [留萌本線](../Page/留萌本線.md "wikilink")：深川車站 -
        [北一已車站](../Page/北一已車站.md "wikilink")

|                                                                                                                                                                            |                                                                                                                                                                                                        |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Osamunai_Station,_Hakodate_Main_Line.jpg](https://zh.wikipedia.org/wiki/File:Osamunai_Station,_Hakodate_Main_Line.jpg "fig:Osamunai_Station,_Hakodate_Main_Line.jpg") | [Kitaichiyan_Station,_Rumoi_Main_Line_20040630.jpg](https://zh.wikipedia.org/wiki/File:Kitaichiyan_Station,_Rumoi_Main_Line_20040630.jpg "fig:Kitaichiyan_Station,_Rumoi_Main_Line_20040630.jpg") |

### 道路

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/道央自動車道.md" title="wikilink">道央自動車道</a>：<a href="../Page/深川系統交流道.md" title="wikilink">深川系統交流道</a> - <a href="../Page/深川交流道.md" title="wikilink">深川交流道</a> - <a href="../Page/音江休息區.md" title="wikilink">音江休息區</a></li>
<li><a href="../Page/深川留萌自動車道.md" title="wikilink">深川留萌自動車道</a>：深川系統交流道 - <a href="../Page/深川西交流道.md" title="wikilink">深川西交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道12號.md" title="wikilink">國道12號</a></li>
<li><a href="../Page/國道233號.md" title="wikilink">國道233號</a></li>
<li><a href="../Page/國道275號.md" title="wikilink">國道275號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道4號旭川蘆別線</li>
<li>北海道道47號深川雨龍線</li>
<li>北海道道57號旭川深川線</li>
<li>北海道道79號深川豐里線</li>
<li>北海道道94號增毛稻田線</li>
<li>北海道道98號旭川多度志線</li>
</ul>
<dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道281號深川多度志線</li>
<li>北海道道284號深川停車場線</li>
<li>北海道道429號多度志停車場線</li>
<li>北海道道693號鷹泊鷹泊停車場線</li>
<li>北海道道875號多度志一已線</li>
<li>北海道道916號湯內園線</li>
<li>北海道道920號幌內湯內線</li>
</ul></td>
</tr>
</tbody>
</table>

### 巴士

[Fukagawa_Terminal.jpg](https://zh.wikipedia.org/wiki/File:Fukagawa_Terminal.jpg "fig:Fukagawa_Terminal.jpg")

  - [北海道中央巴士](../Page/北海道中央巴士.md "wikilink")
      - 高速留萌號
      - 高速旭川號
  - [JR北海道巴士](../Page/JR北海道巴士.md "wikilink")
      - 深名線
  - [道北巴士](../Page/道北巴士.md "wikilink")、[沿岸巴士](../Page/沿岸巴士.md "wikilink")
      - 留萌旭川線、快速留萌旭川線

## 觀光景點

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光">觀光</h3>
<ul>
<li>國見峰</li>
<li>東洲館</li>
<li>櫻山遊園地</li>
</ul></td>
<td><h3 id="文化遺產">文化遺產</h3>
<ul>
<li>音江環狀石（國指定史跡）</li>
<li>多度志獅子舞（深川市指定無形民俗文化財）</li>
<li>納內町猩猩獅子舞（深川市指定無形民俗文化財）</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 短期大學

  - [拓殖大學北海道短期大學](../Page/拓殖大學北海道短期大學.md "wikilink")

### 高等學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.hukagawanishi.hokkaido-c.ed.jp/">道立深川西高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.fukatoko.hokkaido-c.ed.jp/">道立深川東高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.clark.ed.jp/">Clark記念國際高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 專修學校

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>深川市立高等看護學院</li>
</ul></td>
<td><ul>
<li>深川醫師會准看護學院</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>深川市立深川中學校</li>
<li>深川市立一已中學校</li>
</ul></td>
<td><ul>
<li>深川市立納內中學校</li>
<li>深川市立音江中學校</li>
</ul></td>
<td><ul>
<li>深川市立多度志中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>深川市立深川小學校</li>
<li>深川市立一已小學校</li>
</ul></td>
<td><ul>
<li>深川市立北新小學校</li>
<li>深川市立納內小學校</li>
</ul></td>
<td><ul>
<li>深川市立音江小學校</li>
<li>深川市立多度志小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 海外

  - [亞博斯福](../Page/亞博斯福.md "wikilink")（[加拿大](../Page/加拿大.md "wikilink")
    [不列顛哥倫比亞](../Page/不列顛哥倫比亞.md "wikilink")）

## 本地出身之名人

  - [石黑達昌](../Page/石黑達昌.md "wikilink")：[作家](../Page/作家.md "wikilink")、[醫生](../Page/醫生.md "wikilink")
  - [小川東洲](../Page/小川東洲.md "wikilink")：[書法家](../Page/書法家.md "wikilink")
  - [荻原ゆかり](../Page/荻原ゆかり.md "wikilink")：[女演員](../Page/女演員.md "wikilink")
  - [木下博勝](../Page/木下博勝.md "wikilink")：[醫生](../Page/醫生.md "wikilink")
  - [兒島仁](../Page/兒島仁.md "wikilink")：前[日本電信電話](../Page/日本電信電話.md "wikilink")[社長](../Page/社長.md "wikilink")
  - [谷內友美](../Page/谷內友美.md "wikilink")：[配音員](../Page/配音員.md "wikilink")、女演員
  - [二所之關](../Page/二所之關.md "wikilink")[親方](../Page/親方.md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[年寄](../Page/年寄.md "wikilink")、前[關脇](../Page/關脇.md "wikilink")
  - [松島正幸](../Page/松島正幸.md "wikilink")：[畫家](../Page/畫家.md "wikilink")
  - [山田吾一](../Page/山田吾一.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [唯是震一](../Page/唯是震一.md "wikilink")：[作曲家](../Page/作曲家.md "wikilink")

## 參考資料

## 外部連結

  - [深川地區生活情報](http://www.fukanavi.com/)

  - [深川商店街振興組合聯合會](https://web.archive.org/web/20061009112342/http://www.k2.dion.ne.jp/~sinren/)

  - [深川青年會議所](http://fukagawajc.tuzikaze.com/)

  - [深川市果樹協會](https://web.archive.org/web/20081119142311/http://fukajyu.net/)

  - [北空知農會](https://web.archive.org/web/20080201005716/http://www3.ocn.ne.jp/~kitasora/)

  - [深川國際交流協會](http://www.fukakoku.net/)

  - [東京深川會](http://www.yyclub.net/~tfk/)

<!-- end list -->

1.
2.
3.
4.
5.