[Sham_Shui_Po_Police_Station_2015.jpg](https://zh.wikipedia.org/wiki/File:Sham_Shui_Po_Police_Station_2015.jpg "fig:Sham_Shui_Po_Police_Station_2015.jpg")
[Sham_Shui_Po_Police_Station_view_2015.jpg](https://zh.wikipedia.org/wiki/File:Sham_Shui_Po_Police_Station_view_2015.jpg "fig:Sham_Shui_Po_Police_Station_view_2015.jpg")

**深水埗警署**（官方名稱：**深水埗分區警署**）是[香港警務處](../Page/香港警務處.md "wikilink")[西九龍總區](../Page/西九龍總區.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")[警區的](../Page/警區.md "wikilink")[分區](../Page/分區.md "wikilink")[警署](../Page/警署.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[深水埗區](../Page/深水埗區.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")[欽州街](../Page/欽州街.md "wikilink")37號A，屬於[香港二級歷史建築](../Page/香港二級歷史建築.md "wikilink")。

## 歷史

為了處理大量[中國大陸人湧入香港後](../Page/中國大陸人.md "wikilink")，在[深水埗區所引起的](../Page/深水埗區.md "wikilink")[治安問題](../Page/香港治安.md "wikilink")，深水埗警署於1924年動工，於1925年竣工，當時鄰近海旁及[深水埗軍營](../Page/深水埗軍營.md "wikilink")；深水埗警署僅比九龍現存最古舊的警署──[油麻地警署](../Page/油麻地警署.md "wikilink")，遲兩年落成。

[香港保衛戰時](../Page/香港保衛戰.md "wikilink")，深水埗警署被[駐港英軍充作](../Page/駐港英軍.md "wikilink")[軍營用途](../Page/軍營.md "wikilink")，至[香港日治時期時](../Page/香港日治時期.md "wikilink")，被[日本皇軍改為深水埗集中營的指揮部](../Page/日本皇軍.md "wikilink")。

1978年，[長沙灣警署落成前](../Page/長沙灣警署.md "wikilink")，深水埗警署為深水埗分區總部\[1\]，管轄範圍於[香港保衛戰前遠及](../Page/香港保衛戰.md "wikilink")[荃灣一帶](../Page/荃灣.md "wikilink")。

## 建築

深水埗警署由[公和洋行設計](../Page/公和洋行.md "wikilink")，是[英國殖民地典型的](../Page/英國殖民地.md "wikilink")[新古典主義建築](../Page/新古典主義.md "wikilink")，分為A至E座，其中以面對[欽州街及](../Page/欽州街.md "wikilink")[荔枝角道的C座歷史最悠久](../Page/荔枝角道.md "wikilink")。C座樓高3層，建有傳統圓柱門廊，並且設有裝飾欄杆。傾斜屋頂鋪上中式瓦片，數支煙囪在屋頂突出。面對內庭一面設計則比較現代化，展露[包浩斯風格](../Page/包豪斯.md "wikilink")。深水埗警署樓高3層，為了應付需要，大樓旁於後來分別增建了多座現代化[建築物](../Page/建築物.md "wikilink")。

## 公共交通

<div class="NavFrame collapsed" style="color: BLACK; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[深水埗站](../Page/深水埗站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 鄰近地點

  - [西九龍中心](../Page/西九龍中心.md "wikilink")
  - [麗安邨](../Page/麗安邨.md "wikilink")
  - [欽州街小販市場](../Page/欽州街小販市場.md "wikilink")
  - [-{丰}-滙](../Page/丰滙.md "wikilink")

深水埗公園

## 軼聞

深水埗警署[羈留室內其中一間羈留倉多年來儘管於疑犯人滿為患時](../Page/羈留室.md "wikilink")，均不被使用。傳聞早年每逢有疑犯被羈押在該倉內，於睡覺時均被[鬼魂騷擾](../Page/鬼魂.md "wikilink")，例必於翌日要求調換倉\[2\]。

2008年[盂蘭節後次日](../Page/盂蘭節.md "wikilink")[凌晨](../Page/凌晨.md "wikilink")，[香港傳媒報道有人員從](../Page/香港傳媒.md "wikilink")[閉路電視中觀察到拘留所內出現著白衫](../Page/閉路電視.md "wikilink")、白褲及披長髮的[女鬼飄移](../Page/女鬼.md "wikilink")，巡遊倉間。當時有約7名[疑犯被拘留](../Page/疑犯.md "wikilink")，當人員巡察時，當中一名疑犯突然大叫，人員因此查問，疑犯回應表示目睹鬼魂飄遊，另外一名疑犯異口同聲認同。[警察公共關係科被傳媒問及](../Page/警察公共關係科.md "wikilink")，表示將會作出跟進。

## 參考注釋

[Category:香港警務建築](../Category/香港警務建築.md "wikilink")
[Category:香港二級歷史建築](../Category/香港二級歷史建築.md "wikilink")
[Category:深水埗](../Category/深水埗.md "wikilink")
[Category:西九龍總區](../Category/西九龍總區.md "wikilink")

1.  [深水埗警署](http://www.lcsd.gov.hk/specials/deltaphoto2013/b5/hk7.php)
2.  [一夜槍殺兩警　同袍拒登鬼船](http://hk.apple.nextmedia.com/news/art/20120113/15979191)
    《蘋果日報》 2012年1月13日