**深江橋車站**（）是一位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[東成區](../Page/東成區.md "wikilink")、由[大阪市高速電氣軌道所經營的](../Page/大阪市高速電氣軌道.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。深江橋車站是該公司所屬的[大阪地下鐵](../Page/大阪地下鐵.md "wikilink")[中央線沿線的車站之一](../Page/中央線_\(大阪市高速電氣軌道\).md "wikilink")，車站編號為**C21**，在1985年中央線的延伸路段開始啟用之前，曾一度是中央線的東端終點站。

## 車站構造

深江橋車站是一個[地下車站](../Page/地下車站.md "wikilink")，配置有兩座[側式月台兩條乘車線](../Page/側式月台.md "wikilink")。用來連結對向兩座月台的[中央走道是設置於站內月台靠長田車站的方向](../Page/中央走道.md "wikilink")，而非一般常見的在月台中段處。

### 月台配置

| 月台 | 路線                                                                                                                                                                          | 目的地                                                                        |
| -- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| 1  | [4C.png](https://zh.wikipedia.org/wiki/File:4C.png "fig:4C.png") 中央線                                                                                                        | [長田](../Page/長田站_\(大阪府\).md "wikilink")、[生駒](../Page/生駒站.md "wikilink")、方向 |
| 2  | [森之宮](../Page/森之宮站.md "wikilink")、[本町](../Page/本町站.md "wikilink")、[阿波座](../Page/阿波座站.md "wikilink")、[弁天町](../Page/弁天町站.md "wikilink")、[宇宙廣場方向](../Page/宇宙廣場站.md "wikilink") |                                                                            |

深江橋站月台

## 歷史

  - 1968年（[昭和](../Page/昭和.md "wikilink")43年）7月29日 -
    伴隨地下鐵4號線（也就是今日的中央線）[森之宮](../Page/森之宮車站.md "wikilink")（）～深江橋間路段通車，本站也同時啟用。
  - 1985年（昭和60年）4月5日 - 中央線自深江橋延長至長田，因此本站的地位也從原本的終點站轉為一般的沿線車站。

## 相鄰車站

  - 大阪市高速電氣軌道（大阪地下鐵）
    [4C.png](https://zh.wikipedia.org/wiki/File:4C.png "fig:4C.png") 中央線
      -
        [綠橋](../Page/綠橋站.md "wikilink")（C-20）－**深江橋（C-21）**－[高井田](../Page/高井田站_\(大阪府東大阪市\).md "wikilink")（C-22）

## 外部連結

  - [車站導覽：深江橋站](http://www.osakametro.co.jp/station-guide/C/c21.html) -
    Osaka Metro

[Kaebashi](../Category/日本鐵路車站_Fu.md "wikilink")
[Category:東成區鐵路車站](../Category/東成區鐵路車站.md "wikilink")
[Category:中央線車站
(大阪市高速電氣軌道)](../Category/中央線車站_\(大阪市高速電氣軌道\).md "wikilink")
[Category:1968年启用的铁路车站](../Category/1968年启用的铁路车站.md "wikilink")