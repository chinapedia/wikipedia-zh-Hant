**淵杜父魚科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目的其中一科](../Page/鮋形目.md "wikilink")。

## 分類

**深杜父魚科**下分5個屬，如下：

### 淵杜父魚屬(*Abyssocottus*)

  - [埃氏淵杜父魚](../Page/埃氏淵杜父魚.md "wikilink")(*Abyssocottus elochini*)
  - [隆背淵杜父魚](../Page/隆背淵杜父魚.md "wikilink")(*Abyssocottus gibbosus*)
  - [柯氏淵杜父魚](../Page/柯氏淵杜父魚.md "wikilink")(*Abyssocottus korotneffi*)

### 粗杜父魚屬(*Asprocottus*)

  - [喜冷粗杜父魚](../Page/喜冷粗杜父魚.md "wikilink")(*Asprocottus abyssalis*)
  - [赫氏粗杜父魚](../Page/赫氏粗杜父魚.md "wikilink")(*Asprocottus herzensteini*)
  - [中間粗杜父魚](../Page/中間粗杜父魚.md "wikilink")(*Asprocottus intermedius*)
  - [科氏粗杜父魚](../Page/科氏粗杜父魚.md "wikilink")(*Asprocottus korjakovi*)
  - [小粗杜父魚](../Page/小粗杜父魚.md "wikilink")(*Asprocottus minor*)
  - [貝加爾湖粗杜父魚](../Page/貝加爾湖粗杜父魚.md "wikilink")(*Asprocottus parmiferus*)
  - [平頭粗杜父魚](../Page/平頭粗杜父魚.md "wikilink")(*Asprocottus platycephalus*)
  - [美艷粗杜父魚](../Page/美艷粗杜父魚.md "wikilink")(*Asprocottus pulcher*)

### 仔杜父魚屬(*Cottinella*)

  - [布氏仔杜父魚](../Page/布氏仔杜父魚.md "wikilink")(*Cottinella boulengeri*)

### 駝背杜父魚屬(*Cyphocottus*)

  - [寬嘴駝背杜父魚](../Page/寬嘴駝背杜父魚.md "wikilink")(*Cyphocottus eurystomus*)
  - [黑身駝背杜父魚](../Page/黑身駝背杜父魚.md "wikilink")(*Cyphocottus megalops*)

### 湖杜父魚屬(*Limnocottus*)

  - [貝加爾湖杜父魚](../Page/貝加爾湖杜父魚.md "wikilink")(*Limnocottus bergianus*)
  - [戈氏湖杜父魚](../Page/戈氏湖杜父魚.md "wikilink")(*Limnocottus godlewskii*)
  - [灰湖杜父魚](../Page/灰湖杜父魚.md "wikilink")(*Limnocottus griseus*)
  - [蒼色湖杜父魚](../Page/蒼色湖杜父魚.md "wikilink")(*Limnocottus pallidus*)

### 新淵杜父魚屬(*Neocottus*)

  - [溫泉新淵杜父魚](../Page/溫泉新淵杜父魚.md "wikilink")(*Neocottus thermalis*)
  - [沃氏新淵杜父魚](../Page/沃氏新淵杜父魚.md "wikilink")(*Neocottus werestschagini*)

### 原杜父魚屬(*Procottus*)

  - [戈氏原杜父魚](../Page/戈氏原杜父魚.md "wikilink")(*Procottus gotoi*)
  - [古氏原杜父魚](../Page/古氏原杜父魚.md "wikilink")(*Procottus gurwicii*)
  - [傑氏原杜父魚](../Page/傑氏原杜父魚.md "wikilink")(*Procottus jeittelesii*)
  - [原杜父魚](../Page/原杜父魚.md "wikilink")(*Procottus jeittelesii*)

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/淵杜父魚科.md "wikilink")