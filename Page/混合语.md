**混合语**（）是指多种语言融合产生的语言。目前已知的混合语都是两种语言的混合。虽然语言间的相互借用和影响非常普遍，但是真正的混合语并不多。混合语的产生有可能标志着新的[民族或](../Page/民族.md "wikilink")[文化](../Page/文化.md "wikilink")[群体的产生](../Page/群体.md "wikilink")。

## 混合語之區別

混合语、[克里奧爾語和](../Page/克里奧爾語.md "wikilink")[皮欽語的区别是](../Page/皮欽語.md "wikilink")：

  - 混合语来自确定的多种语言。保留更多的源语言之特点。
  - 克里奥尔语往往来自一种确定的语言和若干其他不确定的来源。倾向于简化源语言。

## 範例

[汉语和其他语言的混合语有](../Page/汉语.md "wikilink")[誒話](../Page/誒話.md "wikilink")、[五屯话](../Page/五屯话.md "wikilink")、[倒話](../Page/倒話.md "wikilink")、[瓦鄉話](../Page/瓦鄉話.md "wikilink")、[貓家話等](../Page/貓家話.md "wikilink")。

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
  - Bakker, Peter (1997) A Language of Our Own: The Genesis of Michif,
    the Mixed Cree-French Language of the Canadian Metis, Oxford: Oxford
    University Press. ISBN 0-19-509712-2

  - Bakker, P., and M. Mous, eds. (1994) Mixed languages: 15 case
    studies in language intertwining, Amsterdam: IFOTT. ISBN
    0-12-345678-9

  - Matras, Yaron and Peter Bakker, eds. (2003) The Mixed Language
    Debate: Theoretical and Empirical Advances, Berlin: Walter de
    Gruyter. ISBN 3-11-017776-5

## 外部連結

  - [](http://www.russiansociety.org)
  - [](http://slovari.yandex.ru/dict/rges/article/rg2/rg2-0923.htm/)

## 参见

  - [协和语](../Page/协和语.md "wikilink")
  - [皮钦语](../Page/皮钦语.md "wikilink")
  - [克里奧爾語](../Page/克里奧爾語.md "wikilink")
  - [澳門土生葡語](../Page/澳門土生葡語.md "wikilink")（瀕危）

{{-}}    [category:語言接觸](../Page/category:語言接觸.md "wikilink")

[混合语言](../Category/混合语言.md "wikilink")