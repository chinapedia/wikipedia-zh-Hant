**混種短毛貓**是來自不同地方混種的貓，每個品種的貓會有不同的外貌特徵和性格，這也是經由各種不同品系雜交後的結果。中國大陸俗稱為**中華田園貓**或**土貓**，港澳地區俗稱為**唐貓**或**街貓**、臺灣將混種貓俗稱為**米克斯貓**（音譯自[英語的](../Page/英語.md "wikilink")「mix」）、因此總類有很多的變化，[三色貓](../Page/三色貓.md "wikilink")、[橘子貓](../Page/橘子貓.md "wikilink")、[虎斑貓都極為常見](../Page/虎斑貓.md "wikilink")。美國人道協會將牠稱爲**短毛家貓**（Domestic
Shorthair，\[1\] 簡稱「DSH」），育貓協會將牠稱爲**短毛家庭貓**（House Cat,
Shorthair，\[2\]\[3\] 簡稱「HCS」）或**短毛家庭寵物**（Shorthair Household
Pet\[4\]）。

## 種類

[Cat_in_Taiwan.JPG](https://zh.wikipedia.org/wiki/File:Cat_in_Taiwan.JPG "fig:Cat_in_Taiwan.JPG")居民飼養的典型米克斯貓，毛色為虎斑與純白混合，可能遺傳自[愛琴海貓的基因](../Page/愛琴海貓.md "wikilink")。\]\]

1.  [三色貓](../Page/三色貓.md "wikilink")：「玳瑁白色貓」就是三色貓的正式名稱，身上可以見到[黑色](../Page/黑色.md "wikilink")、[橘黃色](../Page/橘黃色.md "wikilink")、[白色三種顏色漂亮分布著](../Page/白色.md "wikilink")，在[英國被稱為](../Page/英國.md "wikilink")「印花白貓」在[美國被稱為](../Page/美國.md "wikilink")「花班貓」。
2.  [橘子貓](../Page/橘子貓.md "wikilink")：「紅色虎斑短毛家貓」就是橘子貓的正式名稱，紅色虎斑、毛短而密。
3.  [虎斑貓](../Page/虎斑貓.md "wikilink")：[臺灣常見的虎斑貓咪大多是由細平行條紋所構成的](../Page/臺灣.md "wikilink")[魚骨紋虎斑](../Page/魚骨紋.md "wikilink")。虎斑紋[基因是顯性的](../Page/基因.md "wikilink")，幾乎都會出現在所有[家貓身上](../Page/家貓.md "wikilink")。
4.  [白貓](../Page/白貓.md "wikilink")：全白、毛短而厚。藍眼的白貓是所謂「[白子](../Page/白化症.md "wikilink")」，白子貓咪天生[內耳缺陷](../Page/內耳.md "wikilink")，所以會造成[耳聾](../Page/耳聾.md "wikilink")、[眼睛缺乏色素](../Page/眼睛.md "wikilink")、易受光害、對光反應敏感陰、而常聽到陰陽眼的貓咪則是「半白子」的貓咪，沒有白子的缺陷，卻有不同顏色漂亮的眼睛。
5.  [黑貓](../Page/黑貓.md "wikilink")：全黑、毛厚、短而有光澤、被毛上常有虎斑紋，但容易因全身漆黑而被忽略，且[肚子上易長出些許白毛](../Page/肚子.md "wikilink")。
6.  [玳瑁貓](../Page/玳瑁貓.md "wikilink")：黑色及深淺紅色的斑紋。貓咪身上如果有三種顏色存在，這樣的毛色屬於[性聯](../Page/性聯.md "wikilink")[基因](../Page/基因.md "wikilink")，通常會發生在[母貓身上](../Page/母貓.md "wikilink")，若公貓出現三色現象，一般都是[染色體異常](../Page/染色體.md "wikilink")，缺乏[生殖能力](../Page/生殖.md "wikilink")。

## 參考來源

### 参考文献

  - 陳正茂，《第一次養貓就上手》。臺北：易博士文化出版，2003年：38—43頁。

## 相關條目

  - [混種狗](../Page/混種狗.md "wikilink")
  - [楼楼](../Page/楼楼.md "wikilink")

[M](../Page/CATEGORY:貓品種.md "wikilink")

1.   Gives a figure of 95% short-haired.
2.
3.
4.   [PDF](../Page/Portable_Document_Format.md "wikilink") version:
    <http://www.wcf-online.de/WCF-EN/library/HHP_en_2010-01-01.pdf>