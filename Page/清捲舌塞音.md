**清捲舌塞音**（英文：**Voiceless retroflex
plosive**）是[輔音的一種](../Page/輔音.md "wikilink")，屬[塞音](../Page/塞音.md "wikilink")，通常用於一些[口語中](../Page/口語.md "wikilink")。用來表示這個[輔音的標誌是](../Page/輔音.md "wikilink")，而[X-SAMPA的標誌是](../Page/X-SAMPA.md "wikilink")。

## 特點

清捲舌塞音特點：

## 該音見於漢語

[普通話中此音並不單獨存在](../Page/普通話.md "wikilink")，而是和[清捲舌擦音組成](../Page/清捲舌擦音.md "wikilink")[破擦音](../Page/清捲舌塞擦音.md "wikilink")/ʈʂ/和/ʈʂʰ/，拼音時是*zh*和*ch*。[臺灣口音的捲舌程度比北京口音的低得多](../Page/台灣國語.md "wikilink")，比較接近[清齒齦破擦音](../Page/清齒齦破擦音.md "wikilink")/ʦ/。

部分音韵学家认为清捲舌塞音独立存在于[中古汉语中](../Page/中古汉语.md "wikilink")，如[知母](../Page/知母_\(音韻\).md "wikilink")/ʈ/和[彻母](../Page/彻母.md "wikilink")/ʈʰ/。

## 該音見於英語

大部分主要的[英語口音都沒有此音](../Page/英語.md "wikilink")。但帶強烈非英語口音的人，如[印度英語有可能會出現此音](../Page/印度.md "wikilink")。在美國裡的一些口音中，此音和[清齒齦塞音](../Page/清齒齦塞音.md "wikilink")/t/成[同位音](../Page/同位音.md "wikilink")，出現在當t置於r的後面。但捲舌的程度沒有像上述的印度口音那麼明顯。

## 該音見於其他語言

### [卫藏語](../Page/卫藏語.md "wikilink")

在西藏中被視為正統的[拉薩口音裡有](../Page/拉薩.md "wikilink")/ʈʂ/和/ʈʂʰ/兩個[音素](../Page/音素.md "wikilink")，出現在兩個輔音並列，而後面的輔音是/r/的地方。英語翻譯這些音時，會用dr、tr或者t來代表。例如[哲蚌寺](../Page/哲蚌寺.md "wikilink")、[扎什倫布寺](../Page/扎什倫布寺.md "wikilink")、[創巴祖古](../Page/創巴祖古.md "wikilink")。中國官方[藏语拼音系統中則以](../Page/藏语拼音.md "wikilink")*zh*和*ch*表示。

### [瑞典語](../Page/瑞典語.md "wikilink")

許多瑞典語和[挪威語的口音中](../Page/挪威語.md "wikilink")，在長[元音後的](../Page/元音.md "wikilink")*rt*會讀成/ʈ/，如()

### [印度的語言](../Page/印度.md "wikilink")

此音於所有[印度-雅利安語支和](../Page/印度-雅利安語支.md "wikilink")[達羅毗荼語系的語言中存在](../Page/達羅毗荼語系.md "wikilink")，例如[印地語](../Page/印地語.md "wikilink")、[梵語](../Page/梵語.md "wikilink")、[泰米爾語等](../Page/泰米爾語.md "wikilink")，並且和[清齒齦塞音](../Page/清齒齦塞音.md "wikilink")/t/的音素分開成另一個獨立音素。除了泰米爾語外，其他語言都有區分此音的原位音和[送氣音兩個版本](../Page/送氣音.md "wikilink")。

## 參看

  - <span lang="en" style="font-size:120%;">[ʈ](../Page/ʈ.md "wikilink")</span>（擴展[拉丁字母](../Page/拉丁字母.md "wikilink")）

[Category:捲舌音](../Category/捲舌音.md "wikilink")
[Category:塞音](../Category/塞音.md "wikilink")