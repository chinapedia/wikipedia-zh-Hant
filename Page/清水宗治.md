**清水宗治**（）是[日本戰國时代的武將](../Page/日本戰國时代.md "wikilink")。[備中国](../Page/備中国.md "wikilink")[高松城城主](../Page/高松城_\(備中國\).md "wikilink")。通稱**長左衛門**。曾仕於[三村氏](../Page/三村氏.md "wikilink")，後來成為[毛利氏家臣](../Page/毛利氏.md "wikilink")。父親是[清水宗則](../Page/清水宗則.md "wikilink")。三村氏的有力部下[石川久智的女婿](../Page/石川久智.md "wikilink")，兒子有[宗之](../Page/清水宗之.md "wikilink")、[景治](../Page/清水景治.md "wikilink")，兄長是[宗知](../Page/清水宗知.md "wikilink")（月清入道）。顽强抵抗了[織田信長的](../Page/織田信長.md "wikilink")[中國征伐而扬名天下](../Page/中國征伐.md "wikilink")。
[Shimizu_Muneharu.jpg](https://zh.wikipedia.org/wiki/File:Shimizu_Muneharu.jpg "fig:Shimizu_Muneharu.jpg")畫\]\]

## 生平

[天文](../Page/天文_\(後奈良天皇\).md "wikilink")6年（1537年）於[備中國](../Page/備中國.md "wikilink")[賀陽郡](../Page/賀陽郡.md "wikilink")[清水村](../Page/清水村.md "wikilink")（現在的[岡山縣](../Page/岡山縣.md "wikilink")[總社市](../Page/總社市.md "wikilink")[井手](../Page/井手.md "wikilink")）出生（誕生日期不詳）。幼名是**才太郎**。

以備中國豪族的家臣身分擔任備中[清水城的城主](../Page/清水城.md "wikilink")，後來成為[高松城城主](../Page/高松城.md "wikilink")。這段經歷有諸種説法，但是一般都是在天正的[備中兵亂之際](../Page/備中兵亂.md "wikilink")，以三村氏[譜代](../Page/譜代.md "wikilink")[石川氏的女婿和重臣的立場支持毛利氏](../Page/石川氏.md "wikilink")，因而得到高松城城主的地位。這個備中兵亂是三村氏和毛利氏以備中一帶為舞台的一場大戰，雖然立場是三村氏的家臣，但是投向毛利方的人亦存在，因此狀況的判斷成為了首要問題（即使是[三村親成等三村氏一門亦投向毛利方](../Page/三村親成.md "wikilink")）。還有另一個說法指在[永祿](../Page/永祿.md "wikilink")8年（1565年），宗治背叛三村氏譜代石川氏並奪取高松城，直接歸順毛利氏而成為城主，但是因為當時毛利氏對備中的三村氏採用間接支配的體制，因此這個説法很難令人相信（有備中一方的資料但是証據不足）。無論如何，成為毛利氏家臣以後於[小早川隆景的配下參與毛利氏平定](../Page/小早川隆景.md "wikilink")[中國地方](../Page/中國地方.md "wikilink")，忠誠地工作，作為隆景和毛利氏的最前陣而受著深厚的信賴。

[天正](../Page/天正.md "wikilink")10年（1582年），向著統一日本進發的[織田信長派遣家臣](../Page/織田信長.md "wikilink")[羽柴秀吉進行](../Page/羽柴秀吉.md "wikilink")[中國征伐](../Page/中國征伐.md "wikilink")，宗治在高松城開始籠城並展開抗戰。秀吉開出如果降伏的話就給予備中國一國的條件，但是宗治沒有回應。因此[黑田孝高發動水攻](../Page/黑田孝高.md "wikilink")，高松城馬上陷入困境（[備中高松城之戰](../Page/備中高松城之戰.md "wikilink")）。水攻期間的6月因為[京都爆發](../Page/京都.md "wikilink")[本能寺之變而令到信長死去](../Page/本能寺之變.md "wikilink")，收到這個情報的秀吉提議以宗治的性命為條件，雙方講和，不殺城中軍民。宗治一直不知道信長已經死去，數日後與兄長[月清](../Page/清水月清.md "wikilink")、弟弟難波傳兵衛及援將末近信賀一同在船上[切腹](../Page/切腹.md "wikilink")。享年46歲。

辭世句為「」 漢譯：浮世唯有今日渡，武士名存高松苔。(來自《新史太閣記》中譯本)

墓所在[山口縣](../Page/山口縣.md "wikilink")[光市的](../Page/光市.md "wikilink")[清鏡寺](../Page/清鏡寺.md "wikilink")。

## 關於切腹

[日本戰國時代以前的](../Page/日本戰國時代.md "wikilink")[武士道並沒有確立過](../Page/武士道.md "wikilink")[切腹的作法](../Page/切腹.md "wikilink")。切腹的例子有很多，但是只是作為自殺的其中一種手段。其中一種是「無念腹」（），把[內臓拉出來等](../Page/內臓.md "wikilink")，是相當凄惨的場面。

而在戰後被捕又有高級身分的武士，大部份都是把首級割下，並沒有切腹的習慣。亦可能根據情況而被處以[磔刑](../Page/磔刑.md "wikilink")。

把切腹的作法改變的轉機就是清水宗治的切腹。於水上乘著小舟，切腹前把一輪舞跳完後，莊重地切開腹部，再由[介錯人斬下首級](../Page/介錯人.md "wikilink")，清水宗治的作法相當俐落，受到當時目擊武士們的讚嘆。[羽柴秀吉為了討伐殺死信長的](../Page/羽柴秀吉.md "wikilink")[明智光秀而欲盡早返回](../Page/明智光秀.md "wikilink")[京](../Page/京.md "wikilink")，但是說「要直到見證到名將清水宗治的最後」而在陣中一步都沒有移動。還有後來會見[小早川隆景的秀吉讚賞](../Page/小早川隆景.md "wikilink")「宗治是武士之鑑」。

經過此事之後，武士切腹是名譽的死法就廣為人所認識，而且作為刑罰的切腹亦廣泛流傳而成為習慣。後來秀吉對[豐臣秀次](../Page/豐臣秀次.md "wikilink")、[千利休等處罰亦是切腹](../Page/千利休.md "wikilink")。

## 登場作品

### 小說

  - 『』（收錄在短編集「」，[野中信二著](../Page/野中信二.md "wikilink")）

### 電影

  - 『』第一話～第四話（1941年，演員：[大國一公](../Page/大國一公.md "wikilink")）
  - 『』第一編 第二編 完結編（1955年，演員：[岡譲司](../Page/岡譲司.md "wikilink")）

### 電視劇

  - 『』（1959年，[關西電視台](../Page/關西電視台.md "wikilink")，演員：[北村和夫](../Page/北村和夫.md "wikilink")）
  - [NHK大河劇](../Page/NHK大河劇.md "wikilink")『』（1965年，[日本放送協會](../Page/日本放送協會.md "wikilink")，演員：[田崎潤](../Page/田崎潤.md "wikilink")）
  - NHK大河劇『』（1973年，日本放送協會，演員：[伊達正三郎](../Page/伊達正三郎.md "wikilink")）
  - 『』（1973年，[朝日電視台](../Page/朝日電視台.md "wikilink")，演員：[大友柳太朗](../Page/大友柳太朗.md "wikilink")）
  - NHK大河劇『』（1978年，日本放送協會，演員：[寶生閑](../Page/寶生閑.md "wikilink")）
  - 『』（1987年，[TBS電視台](../Page/TBS電視台.md "wikilink")，演員：[竹脇無我](../Page/竹脇無我.md "wikilink")）
  - 『』（1993年，TBS電視台，演員：[橋爪淳](../Page/橋爪淳.md "wikilink")）
  - 『』（1995年，東京電視台，演員：[及川以造](../Page/及川以造.md "wikilink")）
  - NHK大河劇『』（1996年，日本放送協會，演員：[真田五郎](../Page/真田五郎.md "wikilink")）
  - NHK大河劇『』（2006年，日本放送協會，演員：[木下浩之](../Page/木下浩之.md "wikilink")）
  - 『』（2011年，東京電視台，演員：[中村雅俊](../Page/中村雅俊.md "wikilink")）
  - NHK大河劇『[軍師官兵衛](../Page/軍師官兵衛.md "wikilink")』（2014年，NHK，演員：）

## 外部連結

  - [清水宗治Web](http://www.bitchu.jp/muneharu/top/index.html)

## 相關條目

  - [清水氏](../Page/清水氏.md "wikilink")
  - [切腹](../Page/切腹.md "wikilink")

[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:備中國出身人物](../Category/備中國出身人物.md "wikilink")
[Category:清水氏](../Category/清水氏.md "wikilink")