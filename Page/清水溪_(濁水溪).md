[Undercut_Slope_of_Cingshuei_River(_Nantou_County_).JPG](https://zh.wikipedia.org/wiki/File:Undercut_Slope_of_Cingshuei_River\(_Nantou_County_\).JPG "fig:Undercut_Slope_of_Cingshuei_River(_Nantou_County_).JPG")
**清水溪**，原名**阿拔泉溪**\[1\]\[2\]，位於[台灣中部](../Page/台灣.md "wikilink")，為[濁水溪的支流](../Page/濁水溪.md "wikilink")，河長51公里，流域分佈於[雲林縣](../Page/雲林縣.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")、[嘉義縣](../Page/嘉義縣.md "wikilink")。由於清水溪在觸口山與濁水溪匯合時，清水、濁水同流，因為濁水的水量比較大，顏色又濃，清水混合濁水，一清一濁，故得名\[3\]。

清水溪的上游為[阿里山溪](../Page/阿里山溪_\(嘉義縣\).md "wikilink")，發源於[阿里山脈標高](../Page/阿里山脈.md "wikilink")2,663公尺的[大塔山](../Page/大塔山.md "wikilink")\[4\]，其主流至[豐山附近與](../Page/豐山.md "wikilink")[石鼓盤溪會合後](../Page/石鼓盤溪.md "wikilink")，才始稱「清水溪」。清水溪的主流轉向西，在流經[草嶺](../Page/草嶺.md "wikilink")、倒交山後，再轉向北，流經[樟湖](../Page/樟湖.md "wikilink")、檳榔宅後，至[桶頭開始](../Page/桶頭.md "wikilink")，河道兩度方向改變，因此出現[河階與](../Page/河階.md "wikilink")[攻擊坡](../Page/攻擊坡.md "wikilink")，繼續流經[林頂](../Page/林頂.md "wikilink")、[瑞竹](../Page/瑞竹.md "wikilink")、[過溪](../Page/過溪.md "wikilink")，最後於[觸口注入濁水溪主流](../Page/觸口.md "wikilink")。

## 地理

清水溪流域的地形東南側較高，向西北側降低，地表岩層分布以[中新世桂竹林層的](../Page/中新世.md "wikilink")[砂岩為主](../Page/砂岩.md "wikilink")\[5\]。土壤為沖積土，由砂礫及粘土混合而成，土壤為暗灰色至橄欖灰色、褐黑色，黏性高，無污染，含豐富有機物質氮、磷、鉀、矽酸鎂，適宜農耕\[6\]。

## 歷史

位處清水溪上游的[雲林縣](../Page/雲林縣.md "wikilink")[古坑鄉](../Page/古坑鄉.md "wikilink")[草嶺](../Page/草嶺.md "wikilink")，當地的堀坔山由於地形、岩性、地質等因素，加上[地震](../Page/地震.md "wikilink")、豪雨誘發，自1862年至今已有四次[山崩造成](../Page/山崩.md "wikilink")[堰塞湖的紀錄](../Page/堰塞湖.md "wikilink")\[7\]。1898年（明治31年）發生[戊戌大水災](../Page/戊戌大水災.md "wikilink")，[草嶺潭第一次潰決](../Page/草嶺潭.md "wikilink")，造成下游的濁水溪流路北移，洪水回歸[舊濁水溪故道](../Page/舊濁水溪.md "wikilink")，使舊濁水溪成為濁水溪下游的主流，洪水亦造成[二水](../Page/二水.md "wikilink")、[溪州](../Page/溪州.md "wikilink")、[北斗](../Page/北斗鎮_\(台灣\).md "wikilink")、[田尾](../Page/田尾.md "wikilink")、[埤頭到](../Page/埤頭.md "wikilink")[二林都有嚴重水患](../Page/二林.md "wikilink")，洪水沖破河岸，北斗街全市浸水，四塊厝、沙仔崙、曾厝崙、北勢寮土地大量流失，埤頭庄、田尾鄉也災情慘重\[8\]。

1941年至1942年第二次形成堰塞湖，1951年（民國40年）[中華民國國軍第](../Page/中華民國國軍.md "wikilink")75軍16師工兵營奉命開鑿草嶺間的公路，以利草嶺潭開發，並協助整修草嶺潭天然土壩的坍方，於該年5月18日清晨，由於連日豪雨造成山洪暴發，土壩潰決，奪走了74名官兵的性命\[9\]。1979年（民國68年）8月15日，草嶺潭又因豪雨而形成，但很快在8月24日就因豪雨而潰決，並未造成災害\[10\]。

1999年（民國88年）發生[921大地震後](../Page/921大地震.md "wikilink")，草嶺再度形成一個蓄水量4,600萬立方公尺的草嶺潭，稱為「[新草嶺潭](../Page/新草嶺潭.md "wikilink")」，崩坍土方量高達1億2千萬立方公尺，土壩高約有40～50公尺，阻塞河道長達約4,815公尺，2004年（民國93年）[敏督利颱風帶來大量砂石](../Page/敏督利颱風.md "wikilink")，再度把新草嶺潭填滿\[11\]。

## 交通與產業

清水溪流域交通路線以[縣道149號](../Page/縣道149號.md "wikilink")（鯉南路）南北貫穿其中，北起[南投縣](../Page/南投縣.md "wikilink")[竹山鎮](../Page/竹山鎮.md "wikilink")，往西南至[雲林縣](../Page/雲林縣.md "wikilink")[古坑鄉](../Page/古坑鄉.md "wikilink")，自竹山市街以南大致沿著清水溪河岸往上游方向前進\[12\]。此外縣道149線向東轉接投49線可鄰近[鹿谷鄉](../Page/鹿谷鄉.md "wikilink")\[13\]。

本區產業以農業為主，多種植[茶葉](../Page/茶葉.md "wikilink")、[竹筍](../Page/竹筍.md "wikilink")、[柳丁](../Page/柳丁.md "wikilink")、[咖啡等為當地特產](../Page/咖啡.md "wikilink")，沿途景象多以茶園、果園或竹林為主\[14\]。

## 清水溪水系主要河川

[Shimankeng_River_flowing_through_Wannian_Canyon.jpg](https://zh.wikipedia.org/wiki/File:Shimankeng_River_flowing_through_Wannian_Canyon.jpg "fig:Shimankeng_River_flowing_through_Wannian_Canyon.jpg")的侵蝕而造就出萬年峽谷，其溪水流向清水溪。\]\]

  - **清水溪**：雲林縣[林內鄉](../Page/林內鄉.md "wikilink")、南投縣[竹山鎮](../Page/竹山鎮_\(台灣\).md "wikilink")、雲林縣[古坑鄉](../Page/古坑鄉.md "wikilink")、嘉義縣[梅山鄉](../Page/梅山鄉_\(台灣\).md "wikilink")、[竹崎鄉](../Page/竹崎鄉.md "wikilink")、[阿里山鄉](../Page/阿里山鄉.md "wikilink")
      - [鯉魚尾坑](../Page/鯉魚尾坑.md "wikilink")：南投縣竹山鎮
      - [過溪坑](../Page/過溪坑.md "wikilink")：南投縣竹山鎮
      - [大灣坑](../Page/大灣坑.md "wikilink")：南投縣竹山鎮
      - [黃圮林坑](../Page/黃圮林坑.md "wikilink")：南投縣竹山鎮
      - [山坪頂乾坑](../Page/山坪頂乾坑.md "wikilink")：南投縣竹山鎮
      - [加走寮溪](../Page/加走寮溪.md "wikilink")：南投縣竹山鎮
          - [芊蓁崙溪](../Page/芊蓁崙溪.md "wikilink")
          - [吊倫溪](../Page/吊倫溪.md "wikilink")
      - [烏巢坑](../Page/烏巢坑.md "wikilink")：南投縣竹山鎮
      - [番婆夾坑](../Page/番婆夾坑.md "wikilink")：南投縣竹山鎮
      - [雷公坑](../Page/雷公坑.md "wikilink")：南投縣竹山鎮
          - [倒踏坑](../Page/倒踏坑.md "wikilink")：南投縣竹山鎮
      - [內湖坑溪](../Page/內湖坑溪.md "wikilink")：雲林縣古坑鄉、南投縣竹山鎮
      - [生毛樹溪](../Page/生毛樹溪.md "wikilink")：嘉義縣梅山鄉、竹崎鄉
          - [竹仔坑溪](../Page/竹仔坑溪.md "wikilink")：嘉義縣梅山鄉
          - [粗紙坑溪](../Page/粗紙坑溪.md "wikilink")：嘉義縣梅山鄉、竹崎鄉
              - [出水溪](../Page/出水溪.md "wikilink")：嘉義縣梅山鄉
      - [竹篙水溪](../Page/竹篙水溪.md "wikilink")：雲林縣古坑鄉
      - [石鼓盤溪](../Page/石鼓盤溪.md "wikilink")：亦名[豐山溪](../Page/豐山溪.md "wikilink")，雲林縣古坑鄉、嘉義縣梅山鄉、阿里山鄉
      - **[阿里山溪](../Page/阿里山溪_\(嘉義縣\).md "wikilink")**：嘉義縣梅山鄉、阿里山鄉

## 周邊景點

  - [萬年峽谷](../Page/萬年峽谷.md "wikilink")
  - [太極峽谷](../Page/太極峽谷.md "wikilink")
  - [瑞龍瀑布](../Page/瑞龍瀑布.md "wikilink")
  - [一光山風景區](../Page/一光山風景區.md "wikilink")
  - [來吉風景區](../Page/來吉風景區.md "wikilink")
  - [太和風景區](../Page/太和風景區.md "wikilink")
  - [瑞里風景區](../Page/瑞里風景區.md "wikilink")
  - [豐山風景區](../Page/豐山風景區.md "wikilink")
  - [草嶺風景區](../Page/草嶺風景區.md "wikilink")
  - [樟湖風景區](../Page/樟湖風景區.md "wikilink")
  - [石壁風景區](../Page/石壁風景區.md "wikilink")

## 相關條目

  - [濁水溪](../Page/濁水溪.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [清水溪](../Page/清水溪.md "wikilink")

## 參考資料

[Category:濁水溪水系](../Category/濁水溪水系.md "wikilink")
[Category:雲林縣河川](../Category/雲林縣河川.md "wikilink")
[Category:南投縣河川](../Category/南投縣河川.md "wikilink")
[Category:嘉義縣河川](../Category/嘉義縣河川.md "wikilink")

1.
2.
3.
4.  [阿里山鄉公所：地理與環境](http://www.alishan.gov.tw/content/index.asp?m=1&m1=4&m2=86)


5.
6.
7.
8.
9.
10.
11.
12.
13.
14.