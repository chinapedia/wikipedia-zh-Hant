**清水爱**（），[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[声优兼](../Page/声优.md "wikilink")[職業摔角選手](../Page/職業摔角.md "wikilink")，生于[东京都](../Page/东京都.md "wikilink")，兴趣有[中文和器械](../Page/中文.md "wikilink")[体操](../Page/体操.md "wikilink")。於2016年7月3日在女子摔角團體Ice
Ribbon獲得Triangle Ribbon第22屆冠軍。

## 個人

  - [血型](../Page/血型.md "wikilink")：AB型
  - 入行年份：2002年
  - [身高](../Page/身高.md "wikilink")：154cm
  - [高中的所屬是外語專業的中文路線](../Page/高中.md "wikilink")，學習中文及各種中國方言，當中以[北京語尤為擅長](../Page/北京官话.md "wikilink")。
  - 就讀日本工學院時，與[新谷良子和](../Page/新谷良子.md "wikilink")[坂本美里一起組成](../Page/坂本美里.md "wikilink")「めえめえもお」。此後與新谷在各處也共同演出，結成「テディミルク」。
  - 在[魔力女管家](../Page/魔力女管家.md "wikilink")、[這醜陋又美麗的世界中與](../Page/這醜陋又美麗的世界.md "wikilink")[川澄綾子共演姊妹角色](../Page/川澄綾子.md "wikilink")，因此她間中會稱川澄綾子為姊姊。
  - 自[妄想科學美少女和](../Page/妄想科學美少女.md "wikilink")[拜託了雙子星起常與](../Page/拜託了雙子星.md "wikilink")[聲優](../Page/聲優.md "wikilink")[中原麻衣共演](../Page/中原麻衣.md "wikilink")，2人的關係亦非常良好，在清水的日記中也常看到提及中原的事。
  - 2009年7月25至26日，受邀前往台灣擔任第14屆[開拓動漫祭嘉賓](../Page/開拓動漫祭.md "wikilink")（同場嘉賓：聲優[小清水亞美及畫師](../Page/小清水亞美.md "wikilink")[七瀨葵](../Page/七瀨葵.md "wikilink")）。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 2001年

<!-- end list -->

  - [銀河冒險戰記](../Page/銀河冒險戰記.md "wikilink")（ブリッジクルー）
  - [魔力女管家](../Page/魔力女管家.md "wikilink")（新聞報導之聲）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [朝霧的巫女](../Page/朝霧的巫女.md "wikilink")（**稗田柚子**）
  - [ヒートガイジェイ](../Page/ヒートガイジェイ.md "wikilink")（モニカ・ガブリエル）
  - [魔力女管家 更美麗的事物](../Page/魔力女管家.md "wikilink")（**安藤美奈和**）
  - [爆彈小新娘](../Page/爆彈小新娘.md "wikilink")（胡桃）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [拜託了雙子星](../Page/拜託了雙子星.md "wikilink")（**小野寺樺戀**）
  - [貯金大冒險](../Page/貯金大冒險.md "wikilink")（**ドロップ**）
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（ヒトミちゃん（二代目））
  - [妄想科學美少女](../Page/妄想科學美少女.md "wikilink")（**菊8號**）
  - [闇與帽子與書的旅人](../Page/闇與帽子與書的旅人.md "wikilink")（**初美**）
  - [萊姆色戰奇譚](../Page/萊姆色戰奇譚.md "wikilink")（**真田木綿**）
  - [洛克人EXE](../Page/洛克人exe系列.md "wikilink")（女僕C）
  - [魔法咪路咪路](../Page/魔法咪路咪路.md "wikilink")（菲菲）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [女孩萬歲](../Page/女孩萬歲.md "wikilink")（はかな）
  - [這醜陋又美麗的世界](../Page/這醜陋又美麗的世界.md "wikilink")（**明**）
  - [W ～Wish～](../Page/W_～Wish～.md "wikilink")（**遠野泉奈**）
  - [DearS](../Page/DearS.md "wikilink")（**莲**）
  - [舞-HiME](../Page/舞-HiME.md "wikilink")（**美袋命**）
  - [魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")（月村鈴香）
  - [洛克人EXE AXESS](../Page/洛克人exe系列.md "wikilink")（女僕）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [あかほり外道アワー らぶげ](../Page/あかほり外道アワー_らぶげ.md "wikilink")（**良澄愛美**）
  - [我的太太是魔法少女](../Page/我的太太是魔法少女.md "wikilink")（紅さやか / クルージェ・ギャップ）
  - [我的主人爱作怪](../Page/我的主人爱作怪.md "wikilink")（**澤渡美月**）
  - [地獄少女](../Page/地獄少女.md "wikilink")（鷹村涼子）
  - [絕對少年](../Page/絕對少年.md "wikilink")（海野潮音）
  - [TSUBASA翼](../Page/TSUBASA翼.md "wikilink")（李子）
  - [不可思議星球的雙胞胎公主](../Page/不可思議星球的雙胞胎公主.md "wikilink")（プペット）
  - [神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink")（トラン）
  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")（命（猫 / 人間體） / 咪咪）
  - [魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")（月村鈴香）
  - [MÄR 魔兵傳奇](../Page/MÄR_魔兵傳奇.md "wikilink")（**白雪** / **小雪**）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [鍵姬物語 永久愛麗絲迴旋曲](../Page/鍵姬物語_永久愛麗絲迴旋曲.md "wikilink")（**有栖川ありす**）
  - [陰守忍者](../Page/陰守忍者.md "wikilink")（服部山芽）
  - [Gift ～ギフト～ eternal
    rainbow～](../Page/Gift_〜ギフト〜.md "wikilink")（**深峰莉子**）
  - [死神的歌謠](../Page/死神的歌謠.md "wikilink")（**丹尼爾**）
  - [女子高生 GIRL'S-HIGH](../Page/女子高生.md "wikilink")（鈴木桃香）
  - [草莓狂熱](../Page/草莓狂熱.md "wikilink")（**涼水玉青**、日向絆奈）
  - [驅魔少年](../Page/驅魔少年.md "wikilink")（羅多‧卡米洛）
  - [心跳回憶 Only Love](../Page/心跳回憶_Only_Love.md "wikilink")（吉野椿）
  - [護くんに女神の祝福を！](../Page/護くんに女神の祝福を！.md "wikilink")（吉村逸美）
  - [落語天女](../Page/落語天女.md "wikilink")（**小石川铃**）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [AYAKASHI](../Page/AYAKASHI.md "wikilink")（帕姆·威盧娜·阿莎庫拉）
  - [魔女獵人](../Page/魔女獵人.md "wikilink")（**艾莉斯**）
  - [怪物王女](../Page/怪物王女.md "wikilink")（夏伍德）
  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（阿國）
  - [GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")（**生田蒔輝**）
  - [sola](../Page/sola.md "wikilink")（石月小依）
  - [桃華月憚](../Page/桃華月憚.md "wikilink")（コゲちび）
  - [はぴはぴクローバー](../Page/はぴはぴクローバー.md "wikilink")（クルリさん）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [アキバちゃん](../Page/アキバちゃん.md "wikilink")（**アキバちゃん**）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（龍骨寺椿姬）
  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")（犬神初音）
  - [超能少女](../Page/超能少女.md "wikilink")（藤本月）
  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink")（ララウェル）
  - [旋風管家](../Page/旋風管家.md "wikilink")（女子靈）
  - [ぷるるんっ\!しずくちゃん あはっ☆](../Page/ぷるるんっ!しずくちゃん_あはっ☆.md "wikilink")（ハイビー）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [咲-Saki-](../Page/咲-Saki-.md "wikilink")（國廣一）
  - [守護甜心\!](../Page/守護甜心!.md "wikilink")（木暮由紀奈）
  - [花冠之淚](../Page/花冠之淚.md "wikilink")（**艾爾敏**）
  - [魔力充電娘](../Page/魔力充電娘.md "wikilink")（クラン）
  - [碧陽學園學生會議事錄](../Page/碧陽學園學生會議事錄.md "wikilink")（藤堂愛麗絲）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [聖痕鍊金士](../Page/聖痕鍊金士.md "wikilink")（利茲）
  - [寶石寵物 Twinkle☆](../Page/寶石寵物_Twinkle☆.md "wikilink")（**珊瑚**）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（大老師）
  - [架向星空之橋](../Page/架向星空之橋.md "wikilink")（**神本圓佳**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [咲-Saki- 阿知賀編 episode of side-A](../Page/咲-Saki-.md "wikilink")（國廣一）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [學生會的一存 Lv.2](../Page/碧陽學園學生會議事錄.md "wikilink")（藤堂愛麗絲）
  - [寶石寵物
    Happiness](../Page/寶石寵物_Happiness.md "wikilink")（**花園麻理惠**、食堂大嬸、珊瑚）
  - [寶石寵物 Twinkle☆特別篇](../Page/寶石寵物_Twinkle☆.md "wikilink")（**珊瑚**）
  - [惡魔高校D×D NEW](../Page/惡魔高校D×D.md "wikilink")（賽拉芙露·利維坦）
  - [只有神知道的世界 女神篇](../Page/只有神知道的世界.md "wikilink")（倉川燈）
  - [Walkure Romanze
    少女騎士物語](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（**希咲美櫻**）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [咲-Saki-全國篇](../Page/咲-Saki-.md "wikilink")（國廣一）
  - [我們大家的河合莊](../Page/我們大家的河合莊.md "wikilink")（千夏）
  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（女學生）
  - Re:␣ 超能偵探社（琴音）
  - [灰色的果實](../Page/灰色的果實.md "wikilink")（**小嶺幸**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [靈感少女](../Page/靈感少女.md "wikilink")（小孩）
  - [惡魔高校D×D BorN](../Page/惡魔高校D×D.md "wikilink")（賽拉芙露·利維坦）
  - [灰色的迷宮](../Page/灰色的迷宮.md "wikilink")（**小嶺幸**）
  - [灰色的樂園](../Page/灰色的樂園.md "wikilink")（**小嶺幸**）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [蒼之彼方的四重奏](../Page/蒼之彼方的四重奏.md "wikilink")（白瀨水面）

<!-- end list -->

  - 2018年

<!-- end list -->

  - 惡魔高校D×D HERO（賽拉芙露·利維坦）

### OVA

  - [草莓100%](../Page/草莓100%.md "wikilink")（端本千奈美）
  - [こすぷれCOMPLEX](../Page/こすぷれCOMPLEX.md "wikilink")（今井あてな）
  - [機械女僕](../Page/機械女僕_\(漫畫\).md "wikilink")（咲夜）
  - [サイキックアカデミー煌羅万象](../Page/サイキックアカデミー煌羅万象.md "wikilink")（キャール）
  - [ジェネレーションオブカオス3
    〜時の封印〜](../Page/ジェネレーションオブカオス3_〜時の封印〜.md "wikilink")（ティーファ）
  - [ナースウィッチ小麦ちゃんマジカルて](../Page/ナースウィッチ小麦ちゃんマジカルて.md "wikilink")（ポソ吉）
  - [舞-乙HiME Zwei](../Page/舞-乙HiME_Zwei.md "wikilink")（命）
  - [Memories Off 3.5 朝向回憶的彼方](../Page/想君：秋之回憶.md "wikilink")（荷島音緒）

### 遊戲

  - [想君:秋之回憶](../Page/想君:秋之回憶.md "wikilink")（荷岛音绪）
  - [W ～Wish～](../Page/W_～Wish～.md "wikilink")（**遠野泉奈**）
  - [萊姆色戰奇譚☆純](../Page/萊姆色戰奇譚.md "wikilink")（**真田木綿**）
  - [White Princess the
    second](../Page/White_Princess.md "wikilink")（岡・アレクサンド・柚那）
  - [We Are\*](../Page/We_Are*.md "wikilink")（ 稻敷楓）
  - [舞-HiME～命運的系統樹](../Page/舞-HiME～命運的系統樹.md "wikilink")（美袋命）
  - [Gift -Prism-](../Page/Gift.md "wikilink")（**深峰莉子**）
  - [舞-HiME～命運的系統樹 修羅](../Page/舞-HiME～命運的系統樹_修羅.md "wikilink")（美袋命）
  - [真·幸運星 啟程](../Page/真·幸運星_啟程.md "wikilink")（小早川 優）
  - [PS2-アイドル雀士スーチーパイ４](../Page/PS2-アイドル雀士スーチーパイ４.md "wikilink")（
    **御崎翔子**、**スーチーパイＳ**）
  - [花冠之泪](../Page/花冠之泪.md "wikilink")（艾爾敏）
  - [天津御空！雲之盡頭](../Page/天津御空！.md "wikilink")（**観崎美唯**）
  - [1/2 summer+](../Page/1/2_summer.md "wikilink")（**神木汐**）
  - [灰色的果實 -LE FRUIT DE LA
    GRISAIA-](../Page/灰色的果實.md "wikilink")（**小岭幸**）
  - [灰色的迷宮 -LE LABYRINTHE DE LA
    GRISAIA](../Page/灰色的迷宮.md "wikilink")（**小嶺幸**）
  - [LOVELY QUEST -Unlimited-](../Page/LOVELY_QUEST.md "wikilink") -
    **八乙女彩華**
  - [戀愛0公里 Portable](../Page/戀愛0公里.md "wikilink")、[戀愛0公里
    V](../Page/戀愛0公里.md "wikilink")（**木ノ本乃來亜**）
  - [秋之回憶 -無垢少女-](../Page/秋之回憶_-無垢少女-.md "wikilink")（荷嶋音緒）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [喜歡與喜歡的三角戀](../Page/喜歡與喜歡的三角戀.md "wikilink")（**小森江七瑠**\[1\]）

### 廣播劇CD

  - [漂亮怪獸](../Page/漂亮怪獸.md "wikilink") - 露露
  - [魔法少女奈叶INNOCENT Sound
    Stage](../Page/魔法少女奈葉_INNOCENT.md "wikilink")（**月村铃香**）
  - [魔法少女奈叶Reflection
    廣播劇CD](../Page/魔法少女奈叶Reflection.md "wikilink")（月村铃香）

## 相關條目

  - [81 Produce](../Page/81_Produce.md "wikilink")

## 外部連結

  - [事務所公開簡歷](http://haikyo.co.jp/profile/profile.php?ActorID=12348)

  - [MellowHead Official Site｜清水愛
    箱入り柘榴姫](https://web.archive.org/web/20070108003335/http://www.mellow-head.jp/ai/index.html)

  -
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:Lantis旗下歌手](../Category/Lantis旗下歌手.md "wikilink")

1.