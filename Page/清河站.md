**清河站**是[京包铁路](../Page/京包铁路.md "wikilink")（原[京张铁路](../Page/京张铁路.md "wikilink")）上的一个火车站，位于[北京市](../Page/北京市.md "wikilink")[海淀区安宁庄一带](../Page/海淀区.md "wikilink")。2016年11月开始进行站改工程，2018年10月17日封顶\[1\]，预计将在2019年年底投入使用。车站改建方案由法国[AREP设计公司设计](../Page/AREP设计公司.md "wikilink")，建筑面积达10.9万平方米，以配合[京张城际铁路接入](../Page/京张城际铁路.md "wikilink")。\[2\]

京张城际铁路建成后，清河站承担京张城际铁路部分列车的始发任务。[北京地铁13号线](../Page/北京地铁13号线.md "wikilink")、[昌平线和](../Page/北京地铁昌平线.md "wikilink")[19号线将设置车站与国铁接驳](../Page/北京地铁19号线.md "wikilink")。\[3\]

## 车站结构

改造后的清河站以“海纳百川”作为设计灵感，建筑面积13.75万平方米。车站结构西高东低，形成U型过渡的弧面屋顶结构，同时也避免对西侧高速公路造成视觉负担；车站入口广场采用下沉9米的设计，使换乘层直接与室外联通。车站共有地下两层、地上二层，局部三层。地上二层是高架候车室，三层为商业空间。一层为进站层和站台层，共设置5个站台面合计10个站台；其中，西侧站台为[北京地铁](../Page/北京地铁.md "wikilink")[13号线使用站台](../Page/北京地铁13号线.md "wikilink")；另外4个站台为550米长1.25米高站台，用于[京张城际铁路的列车停靠](../Page/京张城际铁路.md "wikilink")。地下一层为到达层、换乘大厅与地下停车场。地下二层为[北京地铁](../Page/北京地铁.md "wikilink")[19号线支线](../Page/北京地铁19号线.md "wikilink")、[昌平线站台](../Page/北京地铁昌平线.md "wikilink")。\[4\]\[5\]

<table>
<tbody>
<tr class="odd">
<td><p><strong>3</strong></p></td>
<td><p>高铁站房</p></td>
<td><p>商业空间</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2</strong></p></td>
<td><p>高铁站房</p></td>
<td><p>候车厅，检票口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>西入口大厅</p></td>
<td><p>进站口，售票厅，安检口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>南</p></td>
<td><p><a href="../Page/北京地铁.md" title="wikilink">地铁</a>往<a href="../Page/西直门站.md" title="wikilink">西直门方向</a> <small>（<a href="../Page/上地站.md" title="wikilink">上地站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a></small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/北京地铁.md" title="wikilink">地铁</a>往<a href="../Page/东直门站.md" title="wikilink">东直门方向</a> <small>（<a href="../Page/西二旗站.md" title="wikilink">西二旗站</a>）</small></p></td>
<td><p>北 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/张家口南站.md" title="wikilink">张家口南方向</a><small>（<a href="../Page/沙河站_(京包铁路).md" title="wikilink">沙河站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a></small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/张家口南站.md" title="wikilink">张家口南方向</a><small>（<a href="../Page/沙河站_(京包铁路).md" title="wikilink">沙河站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>3</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/张家口南站.md" title="wikilink">张家口南方向</a><small>（<a href="../Page/沙河站_(京包铁路).md" title="wikilink">沙河站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a></small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>4</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/张家口南站.md" title="wikilink">张家口南方向</a><small>（<a href="../Page/沙河站_(京包铁路).md" title="wikilink">沙河站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>5</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/北京北站.md" title="wikilink">北京北方向</a><small>（<a href="../Page/北京北站.md" title="wikilink">北京北站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a></small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>6</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/北京北站.md" title="wikilink">北京北方向</a><small>（<a href="../Page/北京北站.md" title="wikilink">北京北站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>7</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/北京北站.md" title="wikilink">北京北方向</a><small>（<a href="../Page/北京北站.md" title="wikilink">北京北站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a></small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>8</strong>站台</p></td>
<td><p><strong><font color=#000001>京张城际铁路</font></strong> 往<a href="../Page/北京北站.md" title="wikilink">北京北方向</a><small>（<a href="../Page/北京北站.md" title="wikilink">北京北站</a>）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>-1</strong></p></td>
<td><p>西下沉广场</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>城市通廊</p></td>
<td><p>出站口，高铁地铁换乘空间，地下车库</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>东下沉广场</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>-2</strong></p></td>
<td><p>地铁站台</p></td>
<td><p><a href="../Page/北京地铁.md" title="wikilink">地铁</a>站台</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北京地铁.md" title="wikilink">地铁</a>支线站台</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 历史

清河站始建于1905年，1906年9月30日建成\[6\]，当时的站房为6柱5间式站房，中间3间为候车室，站名由京张铁路总办[陈昭常题写](../Page/陈昭常.md "wikilink")\[7\]\[8\]\[9\]。

## 参见

  - [京张铁路车站列表](../Page/京张铁路车站列表.md "wikilink")
  - [京包铁路车站列表](../Page/京包铁路车站列表.md "wikilink")

## 参考资料

{{-}}     |-

[Category:京张铁路车站](../Category/京张铁路车站.md "wikilink")
[Category:京包铁路车站](../Category/京包铁路车站.md "wikilink")
[Category:海淀區鐵路車站](../Category/海淀區鐵路車站.md "wikilink")
[Category:中国铁路三等站‎](../Category/中国铁路三等站‎.md "wikilink")
[Category:北京鐵路局](../Category/北京鐵路局.md "wikilink")
[Category:1909年启用的铁路车站](../Category/1909年启用的铁路车站.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.