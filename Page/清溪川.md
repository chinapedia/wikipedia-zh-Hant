[Korea-Seoul-Cheonggyecheon-2008-01.jpg](https://zh.wikipedia.org/wiki/File:Korea-Seoul-Cheonggyecheon-2008-01.jpg "fig:Korea-Seoul-Cheonggyecheon-2008-01.jpg")
**清溪川**（**청계천**），為[韓國一條總長約](../Page/韓國.md "wikilink")13.7[公里的人工](../Page/公里.md "wikilink")[河流](../Page/河流.md "wikilink")（在首爾市區部份約5.8公里），自[仁王山為起點向東穿過](../Page/仁王山.md "wikilink")[首爾市中心](../Page/首爾.md "wikilink")。在[朝鮮時代為解決雨季積水無法排出市區而挖掘](../Page/朝鮮時代.md "wikilink")，過去曾作為[下水道使用](../Page/下水道.md "wikilink")，2005年改造轉型為國際知名觀光景點。清溪川為[漢江第二大支流](../Page/漢江_\(韓國\).md "wikilink")，在城東區的[沙斤洞](../Page/沙斤洞.md "wikilink")、[松亭洞](../Page/松亭洞.md "wikilink")、[聖水洞與第一大支流](../Page/聖水洞.md "wikilink")[中浪川匯流後進入漢江](../Page/中浪川.md "wikilink")。

## 概要

[Cheonggyecheon_1904.jpg](https://zh.wikipedia.org/wiki/File:Cheonggyecheon_1904.jpg "fig:Cheonggyecheon_1904.jpg")
1406\~1407年[朝鮮太宗開始進行](../Page/朝鮮太宗.md "wikilink")**主流**河床拓寛、兩岸築壩等工程；1411年設立臨時整治機構「[開渠都監](../Page/開渠都監.md "wikilink")」；1412年1月15日\~2月15日投入5萬2800人進行大規模整治。太宗為了要把[梅雨季時在](../Page/梅雨.md "wikilink")[漢陽](../Page/首爾.md "wikilink")（首爾）市區及[首爾盆地雨水集中東流排出而挖掘清溪川自然形態](../Page/首爾盆地.md "wikilink")，並以[土木工程進行河川整治](../Page/土木工程.md "wikilink")，稱為**開川**。故清溪川又有「開川」之稱。

到了[朝鮮世宗時期](../Page/朝鮮世宗.md "wikilink")，整治重點在**支流**的治理，主要防止過量的水流入上游導致市區淹水，1441年在[馬前橋西側水中設立](../Page/馬前橋.md "wikilink")[標石監測水位變化](../Page/標石.md "wikilink")。由於朝鮮世宗同意「由於都城居住著大量人口，將會產生大量垃圾，需要有排污的河川」的主張，清溪川往後就此定位成為排放雨水及排放污水的生活河川，居住在漢陽市區10萬居民的生活污水全由此排放。

居住在首爾盆地人口不斷的成長，周遭自然環境大量被開發，長年下來堆積了許多垃圾和泥沙，1725年[朝鮮英祖即位時](../Page/朝鮮英祖.md "wikilink")，河床幾乎和地面平行。1759年10月[朝鮮英祖設管理河川治理事務的](../Page/朝鮮英祖.md "wikilink")[濬川司](../Page/濬川司.md "wikilink")，1760年2月18日\~4月15日為止共耗時57天動用20餘萬人進行疏濬整治，此後每隔2～3年進行疏通工程。1773年6月，在河岸兩側用石頭造出固定河堤，使彎曲的河道變得筆直。

1914年日本殖民時期改稱「清溪川」，因是朝鮮人和日本人居住地的界線，當時被日本人嗤笑為「濁溪川」。\[1\]\[2\]

韓國在1960年代[朴正熙時期](../Page/朴正熙.md "wikilink")，由於經濟增長及都市發展，清溪川曾被覆蓋成為暗渠，清溪川的水質亦因廢水排放而變得惡劣。1968年更在清溪川上興建[高架道路](../Page/清溪高架道路.md "wikilink")。

2003年7月起，在時任[首爾市長](../Page/首爾.md "wikilink")[李明博推動下進行重新修復工程](../Page/李明博.md "wikilink")，不僅將[清溪高架道路拆除](../Page/清溪高架道路.md "wikilink")，並重新挖掘河道，並為河流重新美化、灌水，及種植各種[植物](../Page/植物.md "wikilink")，又征集興建多條各种特色[橋樑橫跨河道](../Page/橋樑.md "wikilink")。復原廣通橋，將舊廣通橋的橋墩混合到現代橋樑中重建。修築河床以使清溪川水不易流失，在旱季時引漢江水灌清溪川，以使清溪川長年不斷流，分清水及污水兩條管道分流，以使水質保持清潔。工程總耗資9000億[韓圓](../Page/韓圓.md "wikilink")（約62億[港元](../Page/港元.md "wikilink")），在2005年9月完成，現已成為[首爾市中心一個休憩地點](../Page/首爾市.md "wikilink")。

## 環境效益

整建前清溪高架道路四周溫度高於首爾全市平均氣溫5[℃以上](../Page/℃.md "wikilink")；現在則低於全市平均氣溫3.6℃，有效調節都市高溫；平均風速比往年同期快了50％左右，空氣潔淨度明顯提升；清溪川與中浪川交匯處的[楊柳濕地現被指定為](../Page/楊柳濕地.md "wikilink")[白鷺](../Page/白鷺.md "wikilink")、[野鴨](../Page/鴨.md "wikilink")、[翠鳥等鳥類保護區](../Page/翠鳥.md "wikilink")，水中生物有[魚類](../Page/魚類.md "wikilink")、[兩棲類多樣性](../Page/兩棲類.md "wikilink")[生物](../Page/生物.md "wikilink")，[水生植物有](../Page/水生植物.md "wikilink")[溪柳](../Page/柳.md "wikilink")、[荊三棱](../Page/荊三棱.md "wikilink")、[菖蒲等](../Page/菖蒲.md "wikilink")，沿岸可見[狼尾草](../Page/狼尾草.md "wikilink")、[紫芒等植物](../Page/紫芒.md "wikilink")。

### 爭議

當年參與清溪川整治工程規畫的[首爾大學景觀系教授金晟均批評](../Page/首爾大學.md "wikilink")：“韓國人引以為傲的首爾市清溪川其實是一條沒有生命的人工排水道”并称“這是一件相當政治導向的景觀工程”。\[3\]清溪川80%的水都依靠[馬達從](../Page/馬達.md "wikilink")[漢江抽取](../Page/漢江.md "wikilink")，由上游往下游放水；清溪川河床採取「三面光」工程
\[4\]，過度的水泥化及人工化，並且鋪上不透水層防止滲漏至[首爾地鐵](../Page/首爾地鐵.md "wikilink")；清溪川每年需要耗資至少700万美元維持，一旦韓國政府預算不足，整治後的清溪川可能再度死亡。\[5\]\[6\]

<File:Seoul-Restoration> site 02.jpg|清溪川整治工程 |清溪川已成为人们休闲之地 <File:Seoul>
Cheonggyecheon night.jpg|夜間的清溪川 <File:Seoul> Cheonggyecheon at
night.jpg|夜間的清溪川

## 相關參見

  - [啟德河](../Page/啟德河.md "wikilink")
  - [韩国旅游](../Page/韩国旅游.md "wikilink")

## 参考资料

## 外部連結

  - [東大門的景點 / 清溪川 /
    中浪川](http://chinese.ddm.go.kr/chn/sub05/02/sub0502_0104.jsp)

  - [Visit Seoul - 清溪川 | 首爾觀光公社 |
    首爾市官方旅遊網站](https://web.archive.org/web/20140808064300/http://www.visitseoul.net/cb/article/article.do?_method=view&m=0004003002003&p=03&art_id=540&lang=cb)

  - [清溪川網站](http://english.sisul.or.kr/grobal/cheonggye/tchinese/WebContent/index.html)

  - [韓國公園綠地及景觀植栽等公共設施管理維護業務考察](https://web.archive.org/web/20140809212536/http://163.29.253.35/OpenFront/report/report_detail.jsp?sysId=C101AD005)

[Category:首爾地理](../Category/首爾地理.md "wikilink")
[Category:韓國河流](../Category/韓國河流.md "wikilink")
[Category:首爾旅遊景點](../Category/首爾旅遊景點.md "wikilink")

1.  [清溪川歷史](http://english.sisul.or.kr/grobal/cheonggye/tchinese/WebContent/sub_history.jsp?page=ceu050101)
2.  [全世界的首都都在突幅猛進，為什麼台北除外？韓國首爾清溪川](http://www.yufulin.net/2007/08/cheonggyecheon.html)
3.  \[<http://e-info.org.tw/node/4956> 南韓學者踢爆：慮河川生態和永續經營等問題
4.  [常常聽到「三面光」或「二面光」的河道，那是什麼樣的河道？](http://eem.pcc.gov.tw/node/35)
5.  [從「溼地與工程的對話」看見城市的未來](http://webarchive.ncl.edu.tw/archive/disk22/22/79/08/20/78/200711263220/20110625/web/eem.pcc_/node/29435.html)
6.  [清溪川的死亡與再生：我們學到了什麼？](http://e-info.org.tw/node/29770)