**清迈**市（[泰语](../Page/泰语.md "wikilink")：**เชียงใหม่**），是[清迈府的首府](../Page/清迈府.md "wikilink")，也是泰国北部政治、经济、文化的中心。人口約25万人。

清邁是泰國北部最大的[歷史文化城市](../Page/歷史.md "wikilink")，也是[泰北](../Page/泰北.md "wikilink")[政治](../Page/政治.md "wikilink")[經濟中心](../Page/經濟.md "wikilink")。為[清邁府首府](../Page/清邁府.md "wikilink")，在1296年至1768年間為[蘭納王國首都](../Page/蘭納.md "wikilink")，蘭納王國其後於1774-1939發展為[清邁王國的](../Page/清邁王國.md "wikilink")[朝貢國](../Page/朝貢國.md "wikilink")。清邁距離[曼谷](../Page/曼谷.md "wikilink")700公里，倚靠著泰國最高的山。清邁在[平河的流域中](../Page/平河.md "wikilink")，平河是[昭拍耶河的主要支流](../Page/昭拍耶河.md "wikilink")。清邁在[泰語的意思是新城市](../Page/泰語.md "wikilink")，在蘭納王國1296年訂定首都時所取的。清邁取代了[清萊成為](../Page/清萊.md "wikilink")[蘭納的首都](../Page/蘭納.md "wikilink")。

清邁在2006年[東協加三於清邁簽訂](../Page/東協.md "wikilink")[清邁協定時得到顯著的政治地位](../Page/清邁協定.md "wikilink")。清邁同時是2020年泰國[大城府世界博覽會展場之一](../Page/大城府世界博覽會.md "wikilink")。清邁被定位成一個[文創城市](../Page/文創.md "wikilink")，目前正在申請[聯合國教科文組織的世界創意城市中](../Page/聯合國教科文組織.md "wikilink")。清邁也是旅遊勝地，曾被旅遊雜誌[TripAdvisor票選為](../Page/TripAdvisor.md "wikilink")2014年世界25大最佳旅遊地，並獲選為第24名。

清邁位於平河流域以及主要[貿易樞紐的位置使他具有相當歷史重要性](../Page/貿易.md "wikilink")。

雖然清邁市區內的人口只有25萬，但是在整個泰北與鄰近衛星城鎮，形成了一個清邁都會生活圈，該都會區有近百萬人，佔了清邁府一半人口以上。

清邁被下設四個行政區、[洛平](../Page/洛平.md "wikilink")、[室利佛逝](../Page/室利佛逝.md "wikilink")、[曼格莱和](../Page/曼格莱.md "wikilink")[卡威拉](../Page/卡威拉.md "wikilink")。前三個[行政區位於平河西岸](../Page/行政區.md "wikilink")，卡威拉則位於東岸。而城市的中心位於室利佛逝。

## 歷史

  - 1296年由**孟萊王**（[孟萊王的母親是](../Page/孟萊王.md "wikilink")[西雙版納](../Page/西雙版納.md "wikilink")[景洪統治者的女兒](../Page/景洪.md "wikilink")，父親是清盛一帶的統治者。孟萊建立了[清萊和清邁兩座城市](../Page/清萊.md "wikilink")，領導[蘭納和](../Page/蘭納.md "wikilink")[西雙版納](../Page/西雙版納.md "wikilink")[傣族人民抗擊](../Page/傣族.md "wikilink")[蒙古侵略者](../Page/蒙古.md "wikilink")，使[蘭納歷史上出現了第一次高峰](../Page/蘭納.md "wikilink")。[傣族後人爲了紀念他](../Page/傣族.md "wikilink")，把一部法典命名為“孟萊法典”，流傳于[泰國北部和](../Page/泰國.md "wikilink")[西雙版納](../Page/西雙版納.md "wikilink")。）從[清萊遷至該地](../Page/清萊.md "wikilink")，起了城牆與護城河，將該地建設成[蘭納泰王國](../Page/蘭納.md "wikilink")（中文史料稱蘭納泰王國為「八百媳婦國」）的[首都](../Page/首都.md "wikilink")**納空清邁**。
  - 14世紀以來，蘭納泰王國受[佛教影響](../Page/佛教.md "wikilink")，在清邁建立很多佛寺，逐漸成為佛教聖地。
  - 1477年蘭納泰的**提洛卡拉王**在柴迪隆寺舉行第8次世界[佛教會議](../Page/佛教.md "wikilink")，是為清邁市的黃金時代。
  - 1564年起被[缅甸控制](../Page/缅甸.md "wikilink")。
  - 1774年[吞武里王朝國王](../Page/吞武里王朝.md "wikilink")[鄭昭从缅甸人手中再度夺回清迈](../Page/鄭昭.md "wikilink")。
  - 19世紀中葉後期，[拉瑪五世就任](../Page/拉瑪五世.md "wikilink")[暹羅國王](../Page/暹羅.md "wikilink")，撤銷藩王制度，將清邁地區置府（省），清邁市成為清邁府的首府。
  - 1980年代起，清邁漸漸發展成泰國北部重要城市和旅遊中心。

[泰國清邁城](../Page/泰國.md "wikilink")、[老撾](../Page/老撾.md "wikilink")[瑯勃拉邦城](../Page/瑯勃拉邦.md "wikilink")、[中國](../Page/中國.md "wikilink")[景洪城和](../Page/景洪.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")[景棟城並稱](../Page/景棟.md "wikilink")[蘭納王國四大城市](../Page/蘭納.md "wikilink")，後在來歷史流變中，分屬於四個國家。但至今這四座城市的[方言差異很小](../Page/方言.md "wikilink")，可以互通。

## 地理和氣候

清邁市（東經98°58′北緯18°46′）距離[曼谷北面約](../Page/曼谷.md "wikilink")700[公里](../Page/公里.md "wikilink")，海拔高約310[公尺的中部盆地上](../Page/公尺.md "wikilink")。

清邁市氣候宜人，冬暖夏涼，是著名的避暑勝地。

## 教育

  - [清邁大學](../Page/清邁大學.md "wikilink")
  - 密柱农学院
  - 西北工学院
  - [私立西北大学](../Page/西北大学_\(泰国\).md "wikilink")
  - 清迈皇家大学

## 旅遊與文化

[缩略图](https://zh.wikipedia.org/wiki/File:Yi_peng_sky_lantern_festival_San_Sai_Thailand.jpg "fig:缩略图")

### 節慶

清邁舉辦許多泰國節慶，包含：
[缩略图](https://zh.wikipedia.org/wiki/File:Chiang-Mai_Thailand_Songkran-Festival-2017-02.jpg "fig:缩略图")

  - [水燈節在](../Page/水燈節_\(東南亞\).md "wikilink")[泰國曆](../Page/泰國曆.md "wikilink")12月滿月時舉辦（西曆通常為11月）。每年有上萬名民眾以香蕉葉、花朵和蠟燭製作[水燈](../Page/水燈.md "wikilink")，再放入河流中，來祭拜水神。同時，民眾也會施放紙紮天燈，據說可消災解厄。
  - [泼水节在每年四月中旬舉辦](../Page/泼水节.md "wikilink")，慶祝傳統泰國新年。有許多遊客在潑水節期間造訪清邁，體驗不同的宗教和文化活動，其中著名的活動就是潑水大戰、遊行和潑水節選美賽。
  - 清邁花卉節在每年二月第一週舉辦，為期3天，剛好是清邁地區熱帶花卉盛開之時。

### 語言

當地居民操[北部泰语](../Page/北部泰语.md "wikilink")。用來撰寫北部泰語的文字系統稱為[老傣文](../Page/老傣文.md "wikilink")，但僅為學術研究和宗教之用，一般民眾改以標準[泰文字撰寫](../Page/泰文字.md "wikilink")。由於清邁觀光業發達，旅館和旅遊業也會使用英文、中文和日文。

### 宗教

[Chiangmai_wchluang04.jpg](https://zh.wikipedia.org/wiki/File:Chiangmai_wchluang04.jpg "fig:Chiangmai_wchluang04.jpg")

  - **[柴迪隆寺](../Page/柴迪隆寺.md "wikilink")**（**Wat Chedi
    Luang**）：位於清邁古城的中央，柴迪隆寺在[泰文的意思是](../Page/泰文.md "wikilink")「大塔寺」，又譯作隆聖骨寺\[1\]。這座[納蘭泰王朝建築形式的大佛塔建於公元](../Page/納蘭泰王朝.md "wikilink")1441年原高90公尺，於1447年於此寺舉行著名的第8屆[世界佛教會議](../Page/世界佛教會議.md "wikilink")。但經歷1545年大地震和16世紀泰緬戰爭，現時寺廟略見傾頗，高度僅剩60公尺高，原存於此塔東面神龕的玉佛，後移至曼谷玉佛寺\[2\]。近年由[聯合國教科文組織和](../Page/聯合國教科文組織.md "wikilink")[日本政府出資修繕](../Page/日本.md "wikilink")，以保存原貌。
  - **[松達寺](../Page/松達寺.md "wikilink")**（**Wat Suan
    Duk**）：泰文意思是「花園寺」。此地位於清邁古城外西邊，建於公元1373年，原本是為佛學大師素瑪那泰拉（Phra
    Sumana
    Thera）雨季時的居所，後曾成為納蘭泰王朝的「御花園」，傳說在松達寺的最大佛塔下埋了佛舍利子，而每個超過一人高的白色塔林亦是納蘭泰王室過世後埋葬處。此寺有一座建於16世紀的青銅佛像，每年4月清邁[潑水節的主要儀式都會在該寺舉行](../Page/潑水節.md "wikilink")\[3\]。
  - **[帕邢寺](../Page/帕邢寺.md "wikilink")**（**Wat Phra
    Singh**）：位於清邁古城內Rajdamnoen Road和Samlam
    Road交界。被視為清邁歷史最悠久的佛寺之一，由[孟萊王朝的坎福王建於](../Page/孟萊王朝.md "wikilink")1345年，目的是用來安奉其父王的靈骨。屹立該寺的大佛像，據說是由斯里蘭卡請來的佛陀叫“帕辛”，不過外貌則是泰北風格。每年潑水節時帕邢寺會為佛像淨身。

### 景點

[Chiangmai_Nightbazaar-1.JPG](https://zh.wikipedia.org/wiki/File:Chiangmai_Nightbazaar-1.JPG "fig:Chiangmai_Nightbazaar-1.JPG")

  - **[清邁古城](../Page/清邁古城.md "wikilink")**：建於1296年的清邁古城，呈四方形，每邊城界長約1.5公里，四邊均由城牆和護城河包圍著，現時城牆和護城河均保存良好。為防止古城景觀受到破壞，清邁市政府於1990年禁止市區建築高樓，並為護城河加建濾水設施。
  - **清邁素贴山**：素贴寺的平台（海拔1053公尺）\[4\]，本可以把清迈市全景尽收眼底（但是由于近几年到了春天的耕种季节，农民们买不起肥料就烧沥青来肥沃土地，结果整个泰国北部都是烟雾缭绕，能见度极差）。清迈皇后的旧宫，虽然不大但是里面鸟语花香，蝴蝶飞舞，异常漂亮。

<!-- end list -->

  - **[清邁藝術文化中心](../Page/清邁藝術文化中心.md "wikilink")**：這座博物館的建築物建於1924年，有展示清邁的歷史資料、清邁人古今生活、清邁佛教文化、農業及山地民族資料等。位於文化中心的正前方，是著名的**三王雕像**，三位對清邁有重大貢獻的人物：[兰甘亨大帝](../Page/兰甘亨大帝.md "wikilink")、孟萊王和南蒙王並立，常有當地人在雕像前燒香獻花，以示尊敬。

<!-- end list -->

  - **夜市**：清邁最熱鬧的地方，原只是昌康路（Chang Klan
    Road）的一群攤販，因附近旅館林立，得地利之便，現已成為固定上有棚架、路邊攤販雲集的夜市，販售各式各樣的廉價品，每天晚上都有，從日落到晚間11點。大部分的當地特產都可在這兒買到，如木雕、漆器、銀器、古董、香腸、各式服飾、水果和點心，一應俱全。

## 交通

### 航空

[清邁國際機場位於清邁市中心西南面約](../Page/清邁國際機場.md "wikilink")4公里處，是泰國第四大國際機場，每日有數十個航班往來首都曼谷，為進出泰北地區的門戶。此外也有国际航班直接飞往[新加坡](../Page/新加坡.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台北等鄰近](../Page/台北.md "wikilink")[亚洲城市](../Page/亚洲.md "wikilink")。

### 鐵路

有[火車鐵路貫通清邁和南面的曼谷](../Page/火車.md "wikilink")、及北部的市鎮。[中國計劃將](../Page/中國.md "wikilink")[滇南鐵路自](../Page/滇南鐵路.md "wikilink")[雲南](../Page/雲南.md "wikilink")[昆明南下出境](../Page/昆明.md "wikilink")，其中一個方案是接連清邁市。

### 市內交通

[Chiangmai_Tuktuk.JPG](https://zh.wikipedia.org/wiki/File:Chiangmai_Tuktuk.JPG "fig:Chiangmai_Tuktuk.JPG")
[缩略图](https://zh.wikipedia.org/wiki/File:Rot_daeng_Chiang_Mai_5.jpg "fig:缩略图")
清邁市主要靠以下幾種交通工具：

  - 計程車：泰北地區只有清邁市內看到有跳表的計程車，該類計程車可以按距離而收費，或可以在上車前和司機議好車資。但此类出租车数量较少，且主要在机场和长途汽车站运营，古城内一般没有出租车。从机场搭車到古城约120-150铢。
  - [雙排車或稱雙條車](../Page/雙排車.md "wikilink")：亦称作宋条，是泰北最常見的交通工具，車身以棗紅色為主色，由於車後面有一載客車廂，左右各有兩排長條形的座椅，故稱為雙排車，因其發音越來越多人稱其為雙條車，運作形式介乎巴士與計程車之間：雙排車无運行固定路線，按照距離長短收費，乘客只要揚手示意便可隨時上車，亦可向司機議價或包車。古城附近一般20铢/人，从汽车站到古城约40泰铢/人。
  - [嘟嘟車](../Page/嘟嘟車.md "wikilink")：亦称突突，屬改裝機車，一般是三輪機車，通常只在市內作短程行駛，价格比出租车和双条车便宜，需議價。最多只能坐4个人（2人最舒适）。
  - 摩托計程車：由一位摩托車司機載一位乘客，需要議價。
  - 租摩托车：自助游清迈最佳的出行方式就是借摩托车，大多为125cc排氣量，24小时200铢，自己去加油。驾车时一定要戴安全帽，最好随身携带驾照。
  - 租自行车：租一辆自行车在清迈古城内漫游不失为一个轻松自由的好办法，非常惬意，租赁自行车约50铢/天，城内到处可见租车的地方，有些旅馆还会有出租自行车的服务。

## 著名人物

  - [鄧麗君](../Page/鄧麗君.md "wikilink")：[台灣籍著名的華語及日語](../Page/台灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")，1995年5月8日逝世於清邁市。
  - [維特維斯特·海倫亞沃恩酷](../Page/維特維斯特·海倫亞沃恩酷.md "wikilink")：暱稱Pich或者Pchy，[泰國](../Page/泰國.md "wikilink")[演員和](../Page/演員.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [英祿·西那瓦](../Page/英祿.md "wikilink")：泰國前總理，1966年出生於清邁。

## 友好城市

  - [昆明](../Page/昆明.md "wikilink")（1999年6月7日）

  - [哈爾濱](../Page/哈爾濱.md "wikilink")（2008年4月29日）

  - [富山縣](../Page/富山縣.md "wikilink")[魚津市](../Page/魚津市.md "wikilink")（1989年8月8日）

  - [埼玉縣](../Page/埼玉縣.md "wikilink")（1992年11月9日）

  - [平壤](../Page/平壤.md "wikilink")

  -
## 參考資料

<references />

[Category:泰國城市](../Category/泰國城市.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")
[清邁](../Category/清邁.md "wikilink")

1.  [清邁](http://www.tattpe.org.tw/view/index.php?page=ChiangMai_003.htm)
    - 泰國觀光局

2.
3.  [清邁松達寺](http://www.tattpe.org.tw/view/index.php?page=ChiangMai_002.htm)
    - 泰國觀光局

4.  [珍愛暹邏](http://www.comebesttour.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CNX-01-02&iSUB_CD=GO&SITE_CD=1&MP_ID=MP002&STYLE_TP=1&JOIN_TP=1)