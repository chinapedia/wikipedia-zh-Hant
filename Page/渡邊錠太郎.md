[Jotaro_Watababe_posing.jpg](https://zh.wikipedia.org/wiki/File:Jotaro_Watababe_posing.jpg "fig:Jotaro_Watababe_posing.jpg")

**渡邊錠太郎**（），[日本軍官](../Page/日本.md "wikilink")，1930年6月2日以[日軍](../Page/日軍.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[航空本部](../Page/航空本部.md "wikilink")[中將](../Page/中將.md "wikilink")[本部長身分調往](../Page/本部長.md "wikilink")[台灣擔任](../Page/台灣.md "wikilink")[台灣軍司令官](../Page/台灣軍司令官.md "wikilink")，1931年8月1日免。1935年7月，就任陸軍[教育總監](../Page/教育總監.md "wikilink")。由於擁護[天皇機關說](../Page/天皇機關說.md "wikilink")，因此激怒不少青年將校，成為日後遇襲身亡的主因之一。[二二六兵变期間](../Page/二二六兵变.md "wikilink")，在[東京都](../Page/東京都.md "wikilink")[杉並区](../Page/杉並区.md "wikilink")[上荻窪私宅受袭身亡](../Page/荻窪_\(杉並區\).md "wikilink")。

## 年譜

|            |                                                                                                                    |
| ---------- | ------------------------------------------------------------------------------------------------------------------ |
| 1894年12月   | 陸軍[士官候補生](../Page/士官候補生.md "wikilink")                                                                             |
| 1895年7月    | [陸士入校](../Page/陆军士官学校_\(日本\).md "wikilink")（8期）                                                                    |
| 1896年11月   | 陸士畢                                                                                                                |
| 1897年6月    | [步兵第19連隊付](../Page/步兵第19連隊.md "wikilink")、任官為[少尉](../Page/少尉.md "wikilink")                                        |
| 1899年11月   | 升為[中尉](../Page/中尉.md "wikilink")                                                                                   |
| 1900年12月   | [陸軍大學校入校](../Page/日本陸軍大學校.md "wikilink")（17期）                                                                      |
| 1903年12月   | 陸大畢業、[步兵第36連隊](../Page/步兵第36連隊.md "wikilink")[中隊長及昇任](../Page/中隊長.md "wikilink")[大尉](../Page/大尉.md "wikilink")     |
| 1904年7月至9月 | [日俄戰爭隨軍參戰](../Page/日俄戰爭.md "wikilink")、負傷                                                                          |
| 1904年10月   | [大本營](../Page/大本營.md "wikilink")[參謀](../Page/參謀.md "wikilink")                                                     |
| 1905年9月    | 元老[山縣有朋之副官](../Page/山縣有朋.md "wikilink")                                                                            |
| 1906年      | 出差清朝                                                                                                               |
| 1907年      | 調駐德國                                                                                                               |
| 1908年12月   | 任官[少佐](../Page/少佐.md "wikilink")                                                                                   |
| 1909年5月    | 擔任[日本駐德大使館武官補佐官](../Page/日本駐德大使館.md "wikilink")                                                                    |
| 1910年6月    | 擔任[參謀本部勤務](../Page/参谋本部_\(大日本帝国\).md "wikilink")                                                                   |
| 1910年11月   | [山縣元帥副官](../Page/山縣有朋.md "wikilink")                                                                               |
| 1913年1月    | 升為[中佐](../Page/中佐.md "wikilink")                                                                                   |
| 1915年2月    | [步兵第3連隊](../Page/步兵第3連隊.md "wikilink")                                                                             |
| 1916年5月    | 參謀本部課長                                                                                                             |
| 1916年7月    | 任官[大佐](../Page/大佐.md "wikilink")                                                                                   |
| 1917年10月   | 擔任荷蘭公使館付武官                                                                                                         |
| 1920年8月    | 擔任步兵第29[旅團長及任官](../Page/旅.md "wikilink")[少將](../Page/少將.md "wikilink")                                             |
| 1922年9月    | [參謀本部第](../Page/參謀本部.md "wikilink")4部長                                                                             |
| 1925年5月    | [陸軍大學校校長及任官](../Page/日本陸軍大學校.md "wikilink")[中將](../Page/中將.md "wikilink")                                          |
| 1926年3月    | [第7師團長](../Page/第7師團長.md "wikilink")                                                                               |
| 1929年3月    | [航空本部長](../Page/陸軍航空本部.md "wikilink")                                                                              |
| 1930年6月    | [台湾軍](../Page/台湾軍.md "wikilink")[司令官](../Page/司令官.md "wikilink")                                                   |
| 1931年8月    | [軍事參議官兼航空本部長](../Page/軍事參議官.md "wikilink")・任官[陸軍](../Page/陸軍.md "wikilink")[大將](../Page/大將.md "wikilink")          |
| 1935年7月    | 就任陸軍[教育總監](../Page/教育總監.md "wikilink")                                                                             |
| 1936年2月26日 | [二二六事件時於](../Page/二二六事件.md "wikilink")[杉並區](../Page/杉並區.md "wikilink")[上荻窪自邸被殺](../Page/荻窪_\(杉並区\).md "wikilink")。 |

## 相關關目

  -
## 參考文獻

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")『日本陸海軍総合事典』第2版、[東京大学出版会](../Page/東京大学出版会.md "wikilink")、2005年。
  - 福川秀樹『日本陸軍将官辞典』芙蓉書房出版、2001年。
  - 外山操編『陸海軍将官人事総覧 陸軍篇』芙蓉書房出版、1981年。

[Category:日本陸軍軍人](../Category/日本陸軍軍人.md "wikilink")
[Category:日本駐外武官](../Category/日本駐外武官.md "wikilink")
[Category:台灣軍司令官](../Category/台灣軍司令官.md "wikilink")
[Category:二二六事件人物](../Category/二二六事件人物.md "wikilink")
[Category:日本遇刺身亡者](../Category/日本遇刺身亡者.md "wikilink")
[Category:愛知縣出身人物](../Category/愛知縣出身人物.md "wikilink")