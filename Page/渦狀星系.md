**渦狀星系**（Whirlpool Galaxy），又叫做**M51**或是**NGC
5194**，位在天空北方的[獵犬座](../Page/獵犬座.md "wikilink")（Canes
Venaciti），長度約有65000光年，距離地球2300萬光年。

## 發現

渦狀星系是由[查爾斯·梅西耶於](../Page/查爾斯·梅西耶.md "wikilink")1773年10月13日發現的。其伴星系NGC
5195則由[皮埃爾·梅香於](../Page/皮埃爾·梅香.md "wikilink")1781年發現。直至1845年，它才第一次被看出是[旋涡星系](../Page/旋涡星系.md "wikilink")，而發現它是漩渦狀的是[威廉·帕森思](../Page/威廉·帕森思.md "wikilink")，他透過一座建於[愛爾蘭](../Page/愛爾蘭.md "wikilink")[比爾城堡的](../Page/比爾城堡.md "wikilink")72吋[反射望遠鏡而得出此觀察結果](../Page/反射望遠鏡.md "wikilink")。在2005年，人們觀察到渦狀星系內的一顆[超新星](../Page/超新星.md "wikilink")[SN
2005cs](../Page/SN_2005cs.md "wikilink")，其最高亮度達14[等](../Page/星等.md "wikilink")。2011年5月31日觀察到另一顆屬於[II型超新星的超新星](../Page/II型超新星.md "wikilink")[SN
2011dh](../Page/SN_2011dh.md "wikilink")，視星等達到13.5，座標是13:30:05.08
+47:10:11.2\[1\]。有時候，M51是指渦狀星系及其伴星系，如果要作出區分的話，則會將渦M51分為M51A（NGC
5194）及M51B（NGC 5195）。

## 相關條目

  - [觸鬚星系](../Page/觸鬚星系.md "wikilink")
  - [雙鼠星系](../Page/雙鼠星系.md "wikilink")
  - [阿普 299](../Page/阿普_299.md "wikilink")

## 注釋

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[051](../Category/梅西耶天體.md "wikilink")
[47404](../Category/PGC天體.md "wikilink")
[Category:西佛星系](../Category/西佛星系.md "wikilink")
[085](../Category/阿普天體.md "wikilink")
[08493](../Category/UGC天體.md "wikilink")
[5194](../Category/獵犬座NGC天體.md "wikilink")
[Category:1773年發現的天體](../Category/1773年發現的天體.md "wikilink")

1.