## 分布

位于[歐亚大陆的温带东部](../Page/歐亚大陆.md "wikilink")，具体在[秦岭](../Page/秦岭.md "wikilink")[淮河以北](../Page/淮河.md "wikilink")、[大兴安岭](../Page/大兴安岭.md "wikilink")——[阴山](../Page/阴山.md "wikilink")——[贺兰山以东以南](../Page/贺兰山.md "wikilink")、[日本关东以北](../Page/日本.md "wikilink")，包括[华北平原](../Page/华北平原.md "wikilink")、[黄土高原](../Page/黄土高原.md "wikilink")、[东北平原](../Page/东北平原.md "wikilink")、[北海道岛](../Page/北海道.md "wikilink")、[朝鲜半岛大部及](../Page/朝鲜半岛.md "wikilink")[俄罗斯的](../Page/俄罗斯.md "wikilink")[鄂霍次克海沿岸](../Page/鄂霍次克海.md "wikilink")。

## 成因

位于[最大的大陆与](../Page/亚欧大陆.md "wikilink")[最大的大洋之间](../Page/太平洋.md "wikilink")，[海陆热力性质差异显著](../Page/海陆热力性质差异.md "wikilink")。夏季亚欧大陆[低压连成一片](../Page/低压.md "wikilink")，海洋上副熱帶高氣壓西伸北进，从北太平洋[副熱帶高氣壓散发出来的东南季风带来丰沛的降水](../Page/副熱帶高氣壓.md "wikilink")；冬季强大的[蒙古高压散发出来的西北季风影响本地](../Page/蒙古高压.md "wikilink")。因风向切变符合[季风要求](../Page/季风.md "wikilink")，故为季风气候。

## 特徵

1.  夏季高溫多雨：夏季[太阳高度角增大](../Page/太阳高度角.md "wikilink")，晝長夜短，氣溫較高，從熱帶海洋吹來的東南季風帶來豐沛的降水。
2.  冬季寒冷干燥：[最冷月均温在摄氏](../Page/最冷月.md "wikilink")0度以下，冬季寒冷，成因有：本地纬度较高，离冬季风源地近，地形较低平坦地势西高东低使冬季季风得以加强。
3.  季风显著。
4.  夏秋常受[热带气旋影响](../Page/热带气旋.md "wikilink")。

## 自然带

为[温带落叶阔叶林带](../Page/温带落叶阔叶林带.md "wikilink")。

## 区内农业景观

[小麦](../Page/小麦.md "wikilink")、[甜菜](../Page/甜菜.md "wikilink")、[花生](../Page/花生.md "wikilink")、[棉花等](../Page/棉花.md "wikilink")

## 代表城市

  - [北京](../Page/北京.md "wikilink")

  - [西安](../Page/西安.md "wikilink")

  - [哈尔滨](../Page/哈尔滨.md "wikilink")

  - [沈阳](../Page/沈阳.md "wikilink")

  - [長春](../Page/長春.md "wikilink")

  - [石家莊](../Page/石家莊.md "wikilink")

  - [天津](../Page/天津.md "wikilink")

  - [太原](../Page/太原.md "wikilink")

  - [济南](../Page/济南.md "wikilink")

  - [鄭州](../Page/鄭州.md "wikilink")

  - [札幌](../Page/札幌.md "wikilink")

  - [函館](../Page/函館.md "wikilink")

  - [青森](../Page/青森.md "wikilink")

  - [秋田](../Page/秋田.md "wikilink")

  - [仙台](../Page/仙台.md "wikilink")

  - [山形](../Page/山形.md "wikilink")

  - [平壤](../Page/平壤.md "wikilink")

  - [首尔](../Page/首尔.md "wikilink")

  - [海参崴](../Page/海参崴.md "wikilink")

## 气候图表

## 参考文献

## 参见

  - [大气环流](../Page/大气环流.md "wikilink")
  - [副热带季风气候](../Page/副热带季风气候.md "wikilink")
  - [热带季风气候](../Page/热带季风气候.md "wikilink")
  - [东亚季风区](../Page/东亚季风区.md "wikilink")

{{-}}

[Category:气候类型](../Category/气候类型.md "wikilink")
[Category:季風](../Category/季風.md "wikilink")