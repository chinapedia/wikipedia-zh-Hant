**港南台車站**（）是一個位於[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")[港南區](../Page/港南區.md "wikilink")三丁目，屬於[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）[根岸線的](../Page/根岸線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

開業時起就是[島式月台](../Page/島式月台.md "wikilink")1面2線的[跨站式站房](../Page/跨站式站房.md "wikilink")。

### 月台配置

| 月台                                                                                                                                                | 路線                                                                                                                   | 方向                                                                                                                                          | 目的地                                                                                                                                                     | 備註 |
| ------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | -- |
| 1                                                                                                                                                 | [JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg") 根岸線 | 下行                                                                                                                                          | [本鄉台](../Page/本鄉台站.md "wikilink")、[大船方向](../Page/大船站.md "wikilink")                                                                                     |    |
| 2                                                                                                                                                 | 上行                                                                                                                   | [洋光台](../Page/洋光台站.md "wikilink")、[橫濱](../Page/橫濱站.md "wikilink")、[東京](../Page/東京站.md "wikilink")、[大宮方向](../Page/大宮站_\(埼玉縣\).md "wikilink") | 直通[JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg") [京濱東北線](../Page/京濱東北線.md "wikilink") |    |
| [JR_JH_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JH_line_symbol.svg "fig:JR_JH_line_symbol.svg") [橫濱線](../Page/橫濱線.md "wikilink") | \-                                                                                                                   | [新橫濱](../Page/新橫濱站.md "wikilink")、[町田](../Page/町田站.md "wikilink")、[八王子方向](../Page/八王子站.md "wikilink")                                       | 只限早晩運行                                                                                                                                                  |    |

JR港南台站月台

## 使用情況

2016年度1日平均上車人次為**32,279人**。使用人數持續減少，近年大概是橫向狀態。

近年的1日平均上車人次如下表。

<table>
<caption>各年度1日平均上車人次[1][2]</caption>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>1日平均<br />
上車人次</p></th>
<th><p>來源</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991年（平成3年）</p></td>
<td><p>38,925</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年（平成4年）</p></td>
<td><p>39,816</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年（平成5年）</p></td>
<td><p>40,496</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994年（平成6年）</p></td>
<td><p>40,547</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995年（平成7年）</p></td>
<td><p>40,495</p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td><p>1996年（平成8年）</p></td>
<td><p>40,908</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年（平成9年）</p></td>
<td><p>40,063</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年（平成10年）</p></td>
<td><p>39,184</p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td><p>1999年（平成11年）</p></td>
<td><p>38,289</p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td><p>2000年（平成12年）</p></td>
<td><p>[6]37,403</p></td>
<td><p>[7]</p></td>
</tr>
<tr class="odd">
<td><p>2001年（平成13年）</p></td>
<td><p>[8]36,722</p></td>
<td><p>[9]</p></td>
</tr>
<tr class="even">
<td><p>2002年（平成14年）</p></td>
<td><p>[10]36,203</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="odd">
<td><p>2003年（平成15年）</p></td>
<td><p>[12]35,847</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="even">
<td><p>2004年（平成16年）</p></td>
<td><p>[14]35,069</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="odd">
<td><p>2005年（平成17年）</p></td>
<td><p>[16]34,983</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td><p>2006年（平成18年）</p></td>
<td><p>[18]34,878</p></td>
<td><p>[19]</p></td>
</tr>
<tr class="odd">
<td><p>2007年（平成19年）</p></td>
<td><p>[20]34,705</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="even">
<td><p>2008年（平成20年）</p></td>
<td><p>[22]34,648</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="odd">
<td><p>2009年（平成21年）</p></td>
<td><p>[24]33,752</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="even">
<td><p>2010年（平成22年）</p></td>
<td><p>[26]33,248</p></td>
<td><p>[27]</p></td>
</tr>
<tr class="odd">
<td><p>2011年（平成23年）</p></td>
<td><p>[28]33,307</p></td>
<td><p>[29]</p></td>
</tr>
<tr class="even">
<td><p>2012年（平成24年）</p></td>
<td><p>[30]33,103</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="odd">
<td><p>2013年（平成25年）</p></td>
<td><p>[32]33,377</p></td>
<td><p>[33]</p></td>
</tr>
<tr class="even">
<td><p>2014年（平成26年）</p></td>
<td><p>[34]31,807</p></td>
<td><p>[35]</p></td>
</tr>
<tr class="odd">
<td><p>2015年（平成27年）</p></td>
<td><p>[36]32,136</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年（平成28年）</p></td>
<td><p>[37]32,279</p></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 東日本旅客鐵道
    [JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg")
    根岸線
      -

        快速、各站停車（全部列車於根岸線內均為各站停車）

          -
            [洋光台](../Page/洋光台站.md "wikilink")（JK 04）－**港南台（JK
            03）**－[本鄉台](../Page/本鄉台站.md "wikilink")（JK 02）

## 參考資料

  - JR東日本2000年度以後的上車人次

<!-- end list -->

  - 神奈川縣縣勢要覽

## 相關條目

  - [日本鐵路車站列表 Ko](../Page/日本鐵路車站列表_Ko.md "wikilink")

## 外部連結

  -
[Unandai](../Category/日本鐵路車站_Ko.md "wikilink")
[Category:根岸線車站](../Category/根岸線車站.md "wikilink")
[Category:港南區鐵路車站 (日本)](../Category/港南區鐵路車站_\(日本\).md "wikilink")
[Category:1973年啟用的鐵路車站](../Category/1973年啟用的鐵路車站.md "wikilink")

1.  [神奈川県県勢要覧](http://www.pref.kanagawa.jp/cnt/f160349/)

2.  [横浜市統計ポータル](http://www.city.yokohama.lg.jp/ex/stat/index2.html#3) -
    横浜市

3.   - 18ページ

4.  神奈川県県勢要覧（平成12年度） - 220ページ

5.   - 222ページ

6.  [各駅の乗車人員（2000年度）](http://www.jreast.co.jp/passenger/2000_01.html) -
    JR東日本

7.
8.  [各駅の乗車人員（2001年度）](http://www.jreast.co.jp/passenger/2001_01.html) -
    JR東日本

9.   - 220ページ

10. [各駅の乗車人員（2002年度）](http://www.jreast.co.jp/passenger/2002_01.html) -
    JR東日本

11.  - 220ページ

12. [各駅の乗車人員（2003年度）](http://www.jreast.co.jp/passenger/2003_01.html) -
    JR東日本

13.  - 220ページ

14. [各駅の乗車人員（2004年度）](http://www.jreast.co.jp/passenger/2004_01.html) -
    JR東日本

15.  - 222ページ

16. [各駅の乗車人員（2005年度）](http://www.jreast.co.jp/passenger/2005_01.html) -
    JR東日本

17.  - 222ページ

18. [各駅の乗車人員（2006年度）](http://www.jreast.co.jp/passenger/2006_01.html) -
    JR東日本

19.  - 224ページ

20. [各駅の乗車人員（2007年度）](http://www.jreast.co.jp/passenger/2007_01.html) -
    JR東日本

21.  - 228ページ

22. [各駅の乗車人員（2008年度）](http://www.jreast.co.jp/passenger/2008_01.html) -
    JR東日本

23.  - 238ページ

24. [各駅の乗車人員（2009年度）](http://www.jreast.co.jp/passenger/2009_01.html) -
    JR東日本

25.  - 236ページ

26. [各駅の乗車人員（2010年度）](http://www.jreast.co.jp/passenger/2010_01.html) -
    JR東日本

27.  - 236ページ

28. [各駅の乗車人員（2011年度）](http://www.jreast.co.jp/passenger/2011_01.html) -
    JR東日本

29.  - 232ページ

30. [各駅の乗車人員（2012年度）](http://www.jreast.co.jp/passenger/2012_01.html) -
    JR東日本

31.  - 234ページ

32. [各駅の乗車人員（2013年度）](http://www.jreast.co.jp/passenger/2013_01.html) -
    JR東日本

33.  - 236ページ

34. [各駅の乗車人員（2014年度）](http://www.jreast.co.jp/passenger/2014_01.html) -
    JR東日本

35.  - 236ページ

36. [各駅の乗車人員（2015年度）](http://www.jreast.co.jp/passenger/2015_01.html) -
    JR東日本

37. [各駅の乗車人員（2016年度）](http://www.jreast.co.jp/passenger/2016_01.html) -
    JR東日本