**港女**一詞原泛指[香港的](../Page/香港.md "wikilink")[女性](../Page/女性.md "wikilink")，語調中立，常用於[報章標題](../Page/報章.md "wikilink")，但於香港[網絡用語上則作貶義用](../Page/網絡用語.md "wikilink")。近年網上[討論區每當批評香港女性時](../Page/討論區.md "wikilink")，文章不時採用「港女」一詞。

## 讀音

「港女」的「女」字應讀作二聲，即「契女」、「舞女」的「女」（[粵拼](../Page/粵拼.md "wikilink")：neoi2）音，而非「女性」的「餒」（粵拼：neoi5），此為[粵語變調的一個例子](../Page/粵語.md "wikilink")。

## 定義

港女是過份自我中心、[拜金主義者](../Page/拜金主義.md "wikilink")，有[公主病的香港](../Page/公主病.md "wikilink")[女性](../Page/女性.md "wikilink")。她們比較喜歡壞的男生，例如[MK仔](../Page/MK文化.md "wikilink")、[古惑仔等](../Page/古惑仔.md "wikilink")；對照港女現象，於香港男性即稱港男。

在一個網上論壇曾經發表「港女81宗罪」\[1\]，及後作者葉一知在其著作《港女聖經》\[2\]中，將該81宗罪歸納為12宗罪，分別為：

## 新詞意的起源

約2005年底開始，網上的不同維基網站出現「港女」詞新用法。[共筆型網站](../Page/共筆.md "wikilink")[香港網絡大典開始出現批評港女的文章](../Page/香港網絡大典.md "wikilink")，並把「港女」定義為貪錢及愛名牌的人，這字義逐漸在大眾傳媒採用\[3\]。

由於港女傳統特點太易被確認，近年部分港女（特別是家庭環境一般的OL）進化至懂得假裝乖巧，從未拍拖，體弱多病，而又有很多朋友（或長輩）愛憐的模樣，去吸引戀愛經驗不多，而又富同情心的港男做兵，當中以三十多歲，卻未有對象的白領男性最易上當。　

[無綫電視節目](../Page/無綫電視.md "wikilink")《[星期日檔案](../Page/星期日檔案.md "wikilink")》曾經以「港女」及「港男」作為主題探討。\[4\]

## 流行文化

歌曲：

  - [莫文蔚](../Page/莫文蔚.md "wikilink")︰婦女新姿
  - [謝安琪](../Page/謝安琪.md "wikilink")︰港女的幸福星期日

電視劇：

  - [香港](../Page/香港.md "wikilink")[TVB](../Page/TVB.md "wikilink")︰[飛女正傳](../Page/飛女正傳.md "wikilink")

訪問：

  - [杜汶澤最鍾意香港女仔](http://at.she.com/chapman_to_wiki_hkgirl)

## 參見

  - [港女文](../Page/港女文.md "wikilink")
  - [母豬教](../Page/母豬教.md "wikilink")
  - 日語維基百科：

## 參考來源

## 外部連結

  - [《港女殺她死》](https://web.archive.org/web/20100107164655/http://www.speechlessness.com/2006/08/14/563/)，原載《明報》，2006年8月13日

  - 香港電台電視節目 香港故事 [港男港女](http://www.rthk.org.hk/rthk/tv/hkstories/)

  - [「日日打機睇漫畫」
    中產港女踩港男質素低](http://forum.cyberctm.com/forum/viewthread.php?tid=38262)

  -
[分類:香港特定人群稱謂](../Page/分類:香港特定人群稱謂.md "wikilink")

[Category:香港網絡文化](../Category/香港網絡文化.md "wikilink")
[Category:香港次文化](../Category/香港次文化.md "wikilink")
[Category:香港女性](../Category/香港女性.md "wikilink")

1.  [「港女」81宗罪 She.com
    論壇](http://community.she.com/messageboard/relationship/?action=view.topic&id=1816894)，2007年1月5日。
2.  葉一知，《港女聖經》，非傳媒有限公司，2007年。ISBN 978-988-99924-1-5
3.  信報財經新聞副刊：維基百科再添對手 2007年4月2日
4.  港男．講女 星期日檔案 2009-03-08