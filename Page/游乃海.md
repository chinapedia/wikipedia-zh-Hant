**游乃海**（**Yau Nai
Hoi**，），香港電影編劇、導演，其堂兄為資深網上電台製作人兼[社會民主連線黨員](../Page/社會民主連線.md "wikilink")[游乃權](../Page/游乃權.md "wikilink")。

1989年加入[無線電視](../Page/無線電視.md "wikilink")，1992年獲[杜琪峰邀請加入](../Page/杜琪峰.md "wikilink")[大都會電影製作公司](../Page/大都會電影製作公司.md "wikilink")。自[銀河映像於](../Page/銀河映像.md "wikilink")1996年成立以來，一直都是公司的中堅份子，也是杜琪峰及[韋家輝的得力助手](../Page/韋家輝.md "wikilink")，與其他編劇如[司徒錦源](../Page/司徒錦源.md "wikilink")、[欧健兒](../Page/欧健兒.md "wikilink")、[葉天成等人合作無間](../Page/葉天成.md "wikilink")，創作不少受歡迎的電影作品。

凭借2003年的《[大隻佬](../Page/大隻佬_\(電影\).md "wikilink")》和2005年的《[黑社會](../Page/黑社會_\(電影\).md "wikilink")》两次獲得[香港電影金像獎最佳編劇](../Page/香港電影金像獎.md "wikilink")。凭借联合编剧的《[PTU](../Page/PTU.md "wikilink")》、《[柔道龙虎榜](../Page/柔道龙虎榜.md "wikilink")》、《黑社会》和《[夺命金](../Page/夺命金.md "wikilink")》四度获得[金馬獎最佳原著劇本獎](../Page/金馬獎.md "wikilink")。2007年作為導演拍出處女作《[跟蹤](../Page/跟蹤_\(電影\).md "wikilink")》，获得[第27届香港电影金像奖新晋导演奖](../Page/第27届香港电影金像奖.md "wikilink")，以及最佳影片、最佳导演和最佳编剧提名。

## 電影作品

### 導演作品

  - 2007年：[跟蹤](../Page/跟蹤_\(電影\).md "wikilink")

### 編劇作品

  - 1993年：[赤腳小子](../Page/赤腳小子.md "wikilink")
  - 1995年：[無味神探](../Page/無味神探.md "wikilink")
  - 1996年：[天若有情Ⅲ烽火佳人](../Page/天若有情Ⅲ烽火佳人.md "wikilink")
  - 1997年：[十萬火急](../Page/十萬火急.md "wikilink")
  - 1998年：[真心英雄](../Page/真心英雄.md "wikilink")
  - 1998年：[重案组之非常突然](../Page/重案组之非常突然.md "wikilink")
  - 1998年：[暗花](../Page/暗花.md "wikilink")
  - 1999年：[暗戰](../Page/暗戰.md "wikilink")
  - 1999年：[再見阿郎](../Page/再見阿郎.md "wikilink")
  - 1999年：[槍火](../Page/槍火.md "wikilink")
  - 2000年：[孤男寡女](../Page/孤男寡女.md "wikilink")
  - 2000年：[辣手回春](../Page/辣手回春.md "wikilink")
  - 2001年：[瘦身男女](../Page/瘦身男女.md "wikilink")
  - 2001年：[鍾無艷](../Page/鍾無艷_\(電影\).md "wikilink")
  - 2001年：[暗戰2](../Page/暗戰2.md "wikilink")
  - 2002年：[嚦咕嚦咕新年财](../Page/嚦咕嚦咕新年财.md "wikilink")
  - 2002年：[我左眼見到鬼](../Page/我左眼見到鬼.md "wikilink")
  - 2003年：[向左走向右走](../Page/向左走向右走.md "wikilink")
  - 2003年：[大隻佬](../Page/大隻佬.md "wikilink")
  - 2003年：[百年好合](../Page/百年好合.md "wikilink")
  - 2003年：[PTU](../Page/PTU_\(電影\).md "wikilink")
  - 2004年：[柔道龍虎榜](../Page/柔道龍虎榜.md "wikilink")
  - 2005年：[黑社會](../Page/黑社會_\(電影\).md "wikilink")
  - 2006年：[黑社會：以和為貴](../Page/黑社會：以和為貴.md "wikilink")
  - 2007年：[跟蹤](../Page/跟蹤_\(電影\).md "wikilink")
  - 2007年：[鐵三角](../Page/鐵三角_\(2007年電影\).md "wikilink")
  - 2011年：[單身男女](../Page/單身男女.md "wikilink")
  - 2011年：[奪命金](../Page/奪命金.md "wikilink")
  - 2012年：[高海拔之戀II](../Page/高海拔之戀II.md "wikilink")
  - 2013年：[毒战](../Page/毒战.md "wikilink")
  - 2013年：[盲探](../Page/盲探.md "wikilink")
  - 2016年：[三人行](../Page/三人行_\(電影\).md "wikilink")

### 監製作品

  - 2016年：[樹大招風](../Page/樹大招風.md "wikilink")
    ，[三人行](../Page/三人行.md "wikilink")

## 電視劇作品

  - 1990年：[人在邊緣](../Page/人在邊緣.md "wikilink") 編劇
  - 1991年：[大家族](../Page/大家族.md "wikilink") 編劇

## 参考资料

## 外部链接

  -
  -
  -
  -
  -
[Category:香港电影导演](../Category/香港电影导演.md "wikilink")
[Category:香港编剧](../Category/香港编剧.md "wikilink")
[Category:前無綫電視編劇](../Category/前無綫電視編劇.md "wikilink")
[Category:香港電影金像獎最佳编剧得主](../Category/香港電影金像獎最佳编剧得主.md "wikilink")
[Category:金馬獎最佳原著劇本獎獲得者](../Category/金馬獎最佳原著劇本獎獲得者.md "wikilink")
[Category:香港電影金像獎新晉導演得主](../Category/香港電影金像獎新晉導演得主.md "wikilink")
[N乃](../Category/游姓.md "wikilink")
[银河映像电影](../Category/银河映像电影.md "wikilink")
[Category:觀塘瑪利諾書院校友](../Category/觀塘瑪利諾書院校友.md "wikilink")