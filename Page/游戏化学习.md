**游戏化学习**(Learning through
play)，又称为**学习游戏化**，就是采用[遊戲化的方式进行学习](../Page/遊戲化.md "wikilink")。它是目前比较流行的[教学理论和](../Page/教学理论.md "wikilink")[教育实践](../Page/教育实践.md "wikilink")。

## 概述

爱好[游戏是人类的天性](../Page/游戏.md "wikilink")。但是沉溺于游戏之中，则使人[玩物丧志](../Page/玩物丧志.md "wikilink")，影响学习、工作和生活，因此电子游戏曾经被教育界公认为“电子[海洛因](../Page/海洛因.md "wikilink")”。处理得不善，[网瘾也会成为社会问题](../Page/网瘾.md "wikilink")。

不過，如果将学生对[游戏的喜好转化为對学习的动機](../Page/游戏.md "wikilink")，則可以使教師的努力事半功倍。這正是游戏化学习[研究的核心](../Page/研究.md "wikilink")。

目前这种观点，存在两种倾向：

1.  第一種观点引用了[評估中學習的理論](../Page/評估中學習.md "wikilink")，透過利用编訂[教学游戏软件或](../Page/教学游戏软件.md "wikilink")[電子課本](../Page/電子課本.md "wikilink")，将教学的内容串聯游戏或網上競技的方法引導学生進入學習狀態，而避免了传统授課模式的死板和枯燥。這種方式目前在香港有小學正在試行，試點學校計有[油蔴地天主教小學
    (海泓道)及](../Page/油蔴地天主教小學_\(海泓道\).md "wikilink")[將軍澳的](../Page/將軍澳.md "wikilink")[佛教黃藻森小學](../Page/佛教黃藻森小學.md "wikilink")。
2.  另一种观点认为，游戏本身即具学习性。因此，通过研究游戏的特性，用这些特性来改造学习机制，使得学习本身成为一项游戏——而不是将其中的内容通过游戏表达出来。這種觀點主要存在於歐美國家，特別是[紐西蘭](../Page/紐西蘭.md "wikilink")。

## 實行現況

目前游戏化学习的研究，在歐美國家已初見成效。不少學校都在教學中加入遊戲元素，特別是需要體力消耗的活動。例如：在拋[壘球的練習裡](../Page/壘球.md "wikilink")，突然向學生提出數學問題，使學生在接球的現場可以因著體驗和觀察而提出答案。這樣做，比單單背[乘數表的效果更為理想](../Page/乘數表.md "wikilink")。

相關的應用在中国仅处于初步阶段。对游戏的认识以及在教学中的应用，都含糊不清。在香港，由於業內的校長和老師都比較保守，再加上一般家長對“從遊戲中學習”這個新的教學方法並沒有深刻的認識，經常都是單方面向學校或[教育統籌局投訴學校花太多時間在遊戲之上](../Page/教育統籌局.md "wikilink")，使遊戲化學習的發展與推廣在香港停滯不前。

## 教育游戏分类

  - [幼儿游戏](../Page/幼儿游戏.md "wikilink")
  - [数学游戏](../Page/数学游戏.md "wikilink")
  - [语文游戏](../Page/语文游戏.md "wikilink")
  - [英语游戏](../Page/英语游戏.md "wikilink")
  - [科学游戏](../Page/科学游戏.md "wikilink")
  - [综合游戏](../Page/综合游戏.md "wikilink")

## 外部链接**（全部鏈接已失效）**

### 相关论文

  - 《[给孩子创造一个玩学习的快乐天空](http://xd.nsjy.com/eduplay/phd01.htm)》
  - 《[研究游戏也是教育工作者的神圣职责](http://xd.nsjy.com/eduplay/phdplay2.htm)》
  - [大型网络竞技游戏系统《学习技能奥运会》](http://xd.nsjy.com/eduplay/phdplay3.htm)

### 相关企业

  - [游戏学堂](http://www.uc520.com.cn)
  - [奥卓尔公司](https://web.archive.org/web/20130715063157/http://www.aojoy.com/)
  - [网游学堂](http://www.sunun.net)

### 其他

  - [首都师大游戏化学习社区](https://web.archive.org/web/20040902120745/http://cmet.cnu.edu.cn/vr/resources.asp)
  - [遊戲式學習，學習做遊戲](https://www.gameislearning.url.tw/)

[Category:教育學](../Category/教育學.md "wikilink")