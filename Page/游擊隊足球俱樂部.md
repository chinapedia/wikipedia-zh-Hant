</ref> | chairman = Milorad Vučelić | manager =  Miroslav Đukić | league
= [塞爾維亞足球超級聯賽](../Page/塞爾維亞足球超級聯賽.md "wikilink") | season = 2017–18 |
position = 塞爾超，第 2 位 |
pattern_la1=_partizan1617h|pattern_b1=_partizan1617h|pattern_ra1=_partizan1617h|pattern_sh1=
_white_stripes|pattern_so1=_partizan1617h |
leftarm1=000000|body1=000000|rightarm1=000000|shorts1=000000|socks1=000000
|pattern_b2=_partizan1617a |body2=ffffff |pattern_la2=_partizan1617a
|leftarm2= ffffff |pattern_ra2=_partizan1617a |rightarm2= ffffff
|pattern_sh2=_partizan1617a |pattern_so2=_partizan1617ha |shorts2=
ffffff |socks2= ffffff }} **游擊隊足球俱樂部**（Fudbalski Klub
Partizan，[塞爾維亞語](../Page/塞爾維亞語.md "wikilink"): ФК
Партизан，）是[塞爾維亞](../Page/塞爾維亞.md "wikilink")[貝爾格萊德的](../Page/貝爾格萊德.md "wikilink")[足球俱樂部](../Page/足球.md "wikilink")，因此慣稱**-{zh-hans:贝尔格莱德游击队;
zh-hant:貝爾格萊德游擊隊}-**\[1\]，而[港澳地區則把球會名稱音譯作](../Page/港澳地區.md "wikilink")**-{zh-hans:柏迪遜;
zh-hant:柏迪遜}-**。游擊隊足球俱樂部是貝爾格萊德游擊隊體育協會的主要組成部分，[塞爾維亞足球甲級聯賽也是由游擊隊所擁有的](../Page/塞爾維亞足球甲級聯賽.md "wikilink")。根據[歐洲足聯的統計](../Page/歐洲足聯.md "wikilink")，游擊隊有著僅次於荷蘭[阿賈克斯足球俱樂部的青訓學校](../Page/阿賈克斯足球俱樂部.md "wikilink")。\[2\]
在近期的投票調查中，游擊隊是塞爾維亞第二受歡迎的足球俱樂部，有著32.2%的支持度。\[3\]游擊隊曾在1965-66賽季的[歐冠中擊敗](../Page/歐洲足球聯合會冠軍聯賽.md "wikilink")[曼聯挺進決賽](../Page/曼聯.md "wikilink")，最終敗給了[皇家馬德里](../Page/皇家馬德里.md "wikilink")。

## 隊史

貝爾格萊德游擊隊創建於1945年10月4日，是南斯拉夫體育協會的一部分，俱樂部現任依然是更名後的塞爾維亞體育協會成員。游擊隊包括了25個不同運動的俱樂部，但是都有著各自獨立的組織，管理，財政和運動設施。

俱樂部最初是在南斯拉夫人民軍隊中創建的，體育場很長一段時間稱作南斯拉夫人民軍隊體育場。在1950年代早期從軍隊中獨立出來，1945年11月6日和[莫斯科中央陸軍進行了球隊歷史上第一場的國家比賽](../Page/莫斯科中央陸軍體育俱樂部.md "wikilink")。

游擊隊的商業系統中存在著許多企業，他們和俱樂部一同運營。他們也擁有一些大眾設施，比如MIP (音樂和游擊隊)廣播電視台和Samo
Partizan (只有游擊隊)雜誌。

游擊隊現在的球衣色為黑色和白色，在俱樂部的最初13年中球隊的隊衣色為紅色和藍色。

## 球隊榮譽

貝爾格萊德游擊隊有著 24 個聯賽冠軍頭銜:

  - 8 次在**[塞爾維亞](../Page/塞爾維亞.md "wikilink")**時期:

2007–08, 2008–09, 2009–10, 2010–11, 2011–12, 2012–13, 2014–15, 2016–17

  - 8 次在**[塞爾維亞和黑山](../Page/塞爾維亞和黑山.md "wikilink") /
    [南斯拉夫聯盟共和國](../Page/南斯拉夫.md "wikilink")**時期:

1992–93, 1993–94, 1995–96, 1996–97, 1998–99, 2001–02, 2002–03, 2004–05

  - 11 次在**[南斯拉夫社會主義聯邦共和國](../Page/南斯拉夫.md "wikilink")**時期:

1946–47, 1948–49, 1960–61, 1961–62, 1962–63, 1964–65, 1975–76, 1977–78,
1982–83, 1985–86, 1986–87

貝爾格萊德游擊隊贏得過12座全國杯賽冠軍頭銜:

  - 3 次在**[塞爾維亞](../Page/塞爾維亞.md "wikilink")**時期:

2008, 2009, 2011

  - 4 次在**[南斯拉夫社會主義聯邦共和國](../Page/南斯拉夫.md "wikilink")**:

1992, 1994, 1998, 2001

  - 5 次在**[南斯拉夫社會主義聯邦共和國](../Page/南斯拉夫.md "wikilink")**:

1947, 1952, 1954, 1957, 1989

在國際賽場上，游擊隊取得過1965–66賽季[歐冠聯賽的亞軍](../Page/歐洲足球聯合會冠軍聯賽.md "wikilink")，1978年中歐盃（Mitropa
Cup）冠軍，1989–90賽季[歐洲盃賽冠軍盃八強以及四次](../Page/歐洲盃賽冠軍盃.md "wikilink")[歐洲足協盃的](../Page/歐洲足協盃.md "wikilink")
16 強。

## 現役球員

\[4\]

*([vice-captain](../Page/Captain_\(association_football\).md "wikilink"))*

*([captain](../Page/Captain_\(association_football\).md "wikilink"))*


## 同城死敵貝爾格萊德紅星

游擊隊的同城死敵堪稱[貝爾格萊德紅星](../Page/貝爾格萊德紅星.md "wikilink")，這兩個俱樂部之間的比賽被稱之為「永恆的德比」
([塞爾維亞拉丁字母](../Page/塞爾維亞語.md "wikilink"): *večiti derbi*, 塞爾維亞西裡爾字母:
*вечити
дерби*)。兩隊之間德比大戰觀看人數最多的比賽觀眾高達108,000人。尽管在第一次德比中紅星隊取得了勝利，但是游擊隊使用保持著7-1的兩隊交戰球差紀錄。

## 參考資料

## 外部連結

### 官方網站

  - [FK Partizan](http://www.partizan.rs)
  - [UEFA
    Profile](http://www.uefa.com/footballeurope/club=50162/domestic.html)

### 非官方網站

  - [Unofficial club site](http://www.partizan.net)
  - [Grobari1970 fans
    site](https://web.archive.org/web/20160305214647/http://grobari1970.info/)
  - [Juzni front fans site](http://www.juznifront.com)

[Partizan](../Category/塞爾維亞足球俱樂部.md "wikilink")
[Partizan](../Category/南斯拉夫足球俱樂部.md "wikilink")
[\*](../Category/柏迪遜足球會.md "wikilink")

1.  例如歐洲足協的網頁：[cn.uefa.com](http://cn.uefa.com/footballeurope/club=50162/competition=14/index.html)

2.
3.
4.