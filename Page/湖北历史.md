在[湖北](../Page/湖北.md "wikilink")[京山县的屈家岭文化遗址](../Page/京山县.md "wikilink")，出土了蛋壳彩陶，距今已有4000多年的历史。
 据文献记载，商代在湖北境内，有楚（今南漳荆山）、卢（今襄阳）、彭（今房县）、庸（今竹山）等封国。

## 春秋战国

[Wuhan_Chu_town.jpg](https://zh.wikipedia.org/wiki/File:Wuhan_Chu_town.jpg "fig:Wuhan_Chu_town.jpg")\]\]
[YellowCraneTower.jpg](https://zh.wikipedia.org/wiki/File:YellowCraneTower.jpg "fig:YellowCraneTower.jpg")\]\]

春秋战国时期，**湖北地区属于[楚国](../Page/楚国.md "wikilink")**，西部为楚文化的发源，东部大部分被[沼泽覆盖](../Page/沼泽.md "wikilink")，是历史上著名的“[云梦泽](../Page/云梦泽.md "wikilink")”。楚国是当时南方的大国，**[楚庄王](../Page/楚庄王.md "wikilink")**曾经问鼎中原，同齐桓公、晋文公、秦穆公、宋襄公并称**[春秋五霸](../Page/春秋五霸.md "wikilink")**。到战国时，楚国力更强，同齐、燕、韩、赵、魏、秦并称为**[战国七雄](../Page/战国七雄.md "wikilink")**。楚国的京城长期位于今荆州——纪南故城。

## 秦汉

[秦代统一中国后](../Page/秦朝.md "wikilink")，要经湖北南下，因此成为交通要道，人口开始增加，沼泽经过排水逐渐成为良田。秦在今湖北地区设立郡县。为避秦始皇父亲的名讳，将楚改称荆山（今南漳）之"荆"。因此后来湖北就多称荆州，有时合称荆楚。

[汉代在湖北设置江夏郡和南郡](../Page/汉朝.md "wikilink")，均属于天下13州中的荆州。

[三国时代](../Page/三国时代.md "wikilink")，湖北地区是[蜀汉和](../Page/蜀汉.md "wikilink")[吴国必争之地](../Page/吴国.md "wikilink")。今天仍有众多的古三国遗迹，如赤壁古战场、襄阳隆中。

[晋代在湖北设置荆州](../Page/晋朝.md "wikilink")；

## 隋唐

湖北在[唐代分属](../Page/唐朝.md "wikilink")4个道：山南东道、淮南道、江南西道和黔中道，山南东道辖襄州、荆州、随州、郢州、复州、峡州、归州、房州、均州，治所在襄州；淮南道辖沔州（汉阳）、安州（安陆）、黄州、蕲州，治所在今江苏扬州；江南西道辖鄂州（武昌），治所在洪州（今江西南昌）；黔中道辖施州（恩施），治所在黔州（今重庆彭水）。

## 宋元

[北宋时期](../Page/北宋.md "wikilink")，湖北主要归属[荆湖北路和](../Page/荆湖北路.md "wikilink")[京西南路](../Page/京西南路.md "wikilink")。荆湖北路是以洞庭湖以北至荆山，西包沅澧二水之地，简称湖北路，湖北之名即由此始。荆湖北路辖江陵府（荆州）、鄂州（武昌）、复州、峡州、归州、安州（安陆）、汉阳军、荆门军，治所在江陵府。京西南路辖襄州、随州、郢州、房州、均州、光化军，治所在襄州。东南角设置了兴国军（阳新），属于江南西路（治所在洪州，今江西南昌）；西南角设置了施州，属于夔州路（治所在夔州，今重庆奉节）。

今天湖北东南部的[黄州](../Page/黄州.md "wikilink")、[蕲州在北宋初期处于](../Page/蕲州.md "wikilink")[淮南路](../Page/淮南路.md "wikilink")，治所在今江苏[扬州](../Page/扬州.md "wikilink")，后淮南路分为东西两路，[黄州](../Page/黄州.md "wikilink")、[蕲州属于](../Page/蕲州.md "wikilink")[淮南西路](../Page/淮南西路.md "wikilink")；[江南西路辖治所在洪州](../Page/江南西路.md "wikilink")（今江西南昌）管辖今天[黄梅部分土地](../Page/黄梅.md "wikilink")；西南角[恩施](../Page/恩施.md "wikilink")，[利川等地设置了施州](../Page/利川.md "wikilink")，属于[夔州路](../Page/夔州路.md "wikilink")。

北宋末年，由于北方金兵入侵，战乱不止，大批人口南迁，湖北逐渐繁荣，带来北方先进的[文化和](../Page/文化.md "wikilink")[农业技术](../Page/农业.md "wikilink")，湖北成为重要的粮食产地。
[YUAN(South_China).jpg](https://zh.wikipedia.org/wiki/File:YUAN\(South_China\).jpg "fig:YUAN(South_China).jpg")
[元代的湖北大体以长江为界](../Page/元朝.md "wikilink")，分属[湖广行中书省和](../Page/湖广行中书省.md "wikilink")[河南江北行中书省](../Page/河南江北行中书省.md "wikilink")。湖广行省包括今日湖北的东南部、湖南、广西、海南和广东的西南角，在湖北东南部设有武昌路、兴国路（阳新）和汉阳府。[武昌是湖广行省的省会](../Page/武昌.md "wikilink")，也是两湖流域农产品的最大集散地。湖北的大部分地区属于河南江北行省，辖襄阳路、黄州路、蕲州路、峡州路（宜昌）、中兴路（荆州）和德安、沔阳、安陆（钟祥）等府。

## 明朝

[明代设置湖广布政使司](../Page/明朝.md "wikilink")，驻扎武昌。湖广布政使司在今湖北辖武昌府、汉阳府、荆州府、襄阳府、黄州府、承天府（钟祥）、德安府（安陆）、郧阳府。

明末，汉水改道以后，新河道的北侧凹岸形成新兴的商业城镇——**[汉口](../Page/汉口.md "wikilink")**，吸引了大批徽州、山西、江西各帮商人前来，经营盐、茶、木、药等业，街区沿汉水北岸绵延二十里，列为当时**[四大名镇](../Page/四大名镇.md "wikilink")**之一。

明代，在湖北西北部的**[武当山](../Page/武当山.md "wikilink")**，兴建了规模庞大的道教建筑群。

|                                  |                                  |                                                                   |                                                                                                                                                                                                                                                                                                                                           |
| :------------------------------: | :------------------------------: | :---------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                府                 |               附郭县                |                               散州、厅                                |                                                                                                                                                                     县                                                                                                                                                                     |
| [武昌府](../Page/武昌府.md "wikilink") | [江夏县](../Page/江夏县.md "wikilink") |                 [兴国州](../Page/兴国州.md "wikilink")                  |                                  [武昌县](../Page/武昌县.md "wikilink") [大冶县](../Page/大冶县.md "wikilink") [嘉鱼县](../Page/嘉鱼县.md "wikilink") [咸宁县](../Page/咸宁县.md "wikilink") [蒲圻县](../Page/蒲圻县.md "wikilink") [通山县](../Page/通山县.md "wikilink") [崇阳县](../Page/崇阳县.md "wikilink") [通城县](../Page/通城县.md "wikilink")                                  |
| [汉阳府](../Page/汉阳府.md "wikilink") | [汉阳县](../Page/汉阳县.md "wikilink") |                                 /                                 |                                                                                                                                                     [汉川县](../Page/汉川县.md "wikilink")                                                                                                                                                      |
| [荆州府](../Page/荆州府.md "wikilink") | [江陵县](../Page/江陵县.md "wikilink") |  [夷陵州](../Page/夷陵州.md "wikilink") [归州](../Page/归州.md "wikilink")  | [公安县](../Page/公安县.md "wikilink") [石首县](../Page/石首县.md "wikilink") [监利县](../Page/监利县.md "wikilink") [松滋县](../Page/松滋县.md "wikilink") [枝江县](../Page/枝江县.md "wikilink") [宜都县](../Page/宜都县.md "wikilink") [巴东县](../Page/巴东县.md "wikilink") [长阳县](../Page/长阳县.md "wikilink") [兴山县](../Page/兴山县.md "wikilink") [远安县](../Page/远安县.md "wikilink") |
| [襄阳府](../Page/襄阳府.md "wikilink") | [襄阳县](../Page/襄阳县.md "wikilink") |                  [均州](../Page/均州.md "wikilink")                   |                                                                                   [光化县](../Page/光化县.md "wikilink") [谷城县](../Page/谷城县.md "wikilink") [枣阳县](../Page/枣阳县.md "wikilink") [宜城县](../Page/宜城县.md "wikilink") [南漳县](../Page/南漳县.md "wikilink")                                                                                    |
| [黄州府](../Page/黄州府.md "wikilink") | [黄冈县](../Page/黄冈县.md "wikilink") |                  [蕲州](../Page/蕲州.md "wikilink")                   |                                                  [黄陂县](../Page/黄陂县.md "wikilink") [蕲水县](../Page/蕲水县.md "wikilink") [广济县](../Page/广济县.md "wikilink") [黄梅县](../Page/黄梅县.md "wikilink") [麻城县](../Page/麻城县.md "wikilink") [黄安县](../Page/黄安县.md "wikilink")　[罗田县](../Page/罗田县.md "wikilink")                                                   |
| [承天府](../Page/承天府.md "wikilink") | [钟祥县](../Page/钟祥县.md "wikilink") | [荆门州](../Page/荆门州.md "wikilink") [沔阳州](../Page/沔阳州.md "wikilink") |                                                                                                    [潜江县](../Page/潜江县.md "wikilink") [景陵县](../Page/天门县.md "wikilink") [京山县](../Page/京山县.md "wikilink") [当阳县](../Page/当阳县.md "wikilink")                                                                                                    |
| [德安府](../Page/德安府.md "wikilink") | [安陆县](../Page/安陆县.md "wikilink") |                  [随州](../Page/随州.md "wikilink")                   |                                                                                                    [云梦县](../Page/云梦县.md "wikilink") [应城县](../Page/应城县.md "wikilink") [应山县](../Page/应山县.md "wikilink") [孝感县](../Page/孝感县.md "wikilink")                                                                                                    |
| [郧阳府](../Page/郧阳府.md "wikilink") |  [郧县](../Page/郧县.md "wikilink")  |                                 /                                 |                                                                    [郧西县](../Page/郧西县.md "wikilink") [竹山县](../Page/竹山县.md "wikilink") [竹溪县](../Page/竹溪县.md "wikilink") [房县](../Page/房县.md "wikilink") [保康县](../Page/保康县.md "wikilink") [上津县](../Page/上津县.md "wikilink")                                                                    |
|                                  |                                  |                                                                   |                                                                                                                                                                                                                                                                                                                                           |

## 清朝

[清代康熙](../Page/清朝.md "wikilink")3年（1664年）分湖广布政使司为左右2个布政使司，康熙6年
（1667年），湖广左右布政使司分别改名为湖北布政使司和湖南布政使司，湖北、湖南两省从此定名，沿袭至今。湖北省会仍在武昌。清代的湖北省下辖武昌府、汉阳府、荆州府、襄阳府、黄州府、安陆府（钟祥）、德安府（安陆）、郧阳府、宜昌府、施南府（恩施）和荆门直隶州。

1860年[第二次鸦片战争后](../Page/第二次鸦片战争.md "wikilink")，外国资本沿[长江上溯](../Page/长江.md "wikilink")，汉口辟为商埠，并在旧市镇的下游设立了[汉口英租界](../Page/汉口英租界.md "wikilink")。1895年到1898年，又陆续设立了[汉口德租界](../Page/汉口德租界.md "wikilink")、[汉口俄租界](../Page/汉口俄租界.md "wikilink")、[汉口法租界和](../Page/汉口法租界.md "wikilink")[汉口日租界](../Page/汉口日租界.md "wikilink")。另外，在1877年和1896年，[宜昌和](../Page/宜昌.md "wikilink")[沙市也相继开辟为商埠](../Page/沙市.md "wikilink")。

同一时期，[洋务运动兴起](../Page/洋务运动.md "wikilink")，湖广总督[张之洞在湖北兴办大量洋务企业](../Page/张之洞.md "wikilink")，包括汉阳铁厂、汉阳兵工厂等，使得湖北成为中国主要的工业省份之一。

|                                      |                                  |                                                                 |                                                                                                                                                                                                                                                                         |
| :----------------------------------: | :------------------------------: | :-------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                  府                   |               附郭县                |                              散州、厅                               |                                                                                                                                    县                                                                                                                                    |
|   [武昌府](../Page/武昌府.md "wikilink")   | [江夏县](../Page/江夏县.md "wikilink") |                [兴国州](../Page/兴国州.md "wikilink")                 | [武昌县](../Page/武昌县.md "wikilink") [大冶县](../Page/大冶县.md "wikilink") [嘉鱼县](../Page/嘉鱼县.md "wikilink") [咸宁县](../Page/咸宁县.md "wikilink") [蒲圻县](../Page/蒲圻县.md "wikilink") [通山县](../Page/通山县.md "wikilink") [崇阳县](../Page/崇阳县.md "wikilink") [通城县](../Page/通城县.md "wikilink") |
|   [汉阳府](../Page/汉阳府.md "wikilink")   | [汉阳县](../Page/汉阳县.md "wikilink") |                [沔阳州](../Page/沔阳州.md "wikilink")                 |                                                                                   [黄陂县](../Page/黄陂县.md "wikilink")　[孝感县](../Page/孝感县.md "wikilink")　[汉川县](../Page/汉川县.md "wikilink")                                                                                    |
|   [荆州府](../Page/荆州府.md "wikilink")   | [江陵县](../Page/江陵县.md "wikilink") |                                /                                |                                  [公安县](../Page/公安县.md "wikilink") [石首县](../Page/石首县.md "wikilink") [监利县](../Page/监利县.md "wikilink") [松滋县](../Page/松滋县.md "wikilink") [枝江县](../Page/枝江县.md "wikilink") [宜都县](../Page/宜都县.md "wikilink")                                  |
|   [襄阳府](../Page/襄阳府.md "wikilink")   | [襄阳县](../Page/襄阳县.md "wikilink") |                 [均州](../Page/均州.md "wikilink")                  |                                                  [光化县](../Page/光化县.md "wikilink") [谷城县](../Page/谷城县.md "wikilink") [枣阳县](../Page/枣阳县.md "wikilink") [宜城县](../Page/宜城县.md "wikilink") [南漳县](../Page/南漳县.md "wikilink")                                                   |
|   [黄州府](../Page/黄州府.md "wikilink")   | [黄冈县](../Page/黄冈县.md "wikilink") |                 [蕲州](../Page/蕲州.md "wikilink")                  |                                  [蕲水县](../Page/蕲水县.md "wikilink") [广济县](../Page/广济县.md "wikilink") [黄梅县](../Page/黄梅县.md "wikilink") [麻城县](../Page/麻城县.md "wikilink") [黄安县](../Page/黄安县.md "wikilink")　[罗田县](../Page/罗田县.md "wikilink")                                  |
|   [安陆府](../Page/安陆府.md "wikilink")   | [钟祥县](../Page/钟祥县.md "wikilink") |                                /                                |                                                                                   [潜江县](../Page/潜江县.md "wikilink") [天门县](../Page/天门县.md "wikilink") [京山县](../Page/京山县.md "wikilink")                                                                                    |
|   [德安府](../Page/德安府.md "wikilink")   | [安陆县](../Page/安陆县.md "wikilink") |                 [随州](../Page/随州.md "wikilink")                  |                                                                                   [云梦县](../Page/云梦县.md "wikilink") [应城县](../Page/应城县.md "wikilink") [应山县](../Page/应山县.md "wikilink")                                                                                    |
|   [郧阳府](../Page/郧阳府.md "wikilink")   |  [郧县](../Page/郧县.md "wikilink")  |                                /                                |                                                   [郧西县](../Page/郧西县.md "wikilink") [竹山县](../Page/竹山县.md "wikilink") [竹溪县](../Page/竹溪县.md "wikilink") [房县](../Page/房县.md "wikilink") [保康县](../Page/保康县.md "wikilink")                                                    |
|   [宜昌府](../Page/宜昌府.md "wikilink")   | [东湖县](../Page/东湖县.md "wikilink") | [归州](../Page/归州.md "wikilink")　[鹤峰州](../Page/鹤峰州.md "wikilink") |                                                                   [巴东县](../Page/巴东县.md "wikilink") [长阳县](../Page/长阳县.md "wikilink") [兴山县](../Page/兴山县.md "wikilink") [长乐县](../Page/五峰县.md "wikilink")                                                                   |
|   [施南府](../Page/施南府.md "wikilink")   | [恩施县](../Page/恩施县.md "wikilink") |                                /                                |                                                  [利川县](../Page/利川县.md "wikilink") [建始县](../Page/建始县.md "wikilink") [宣恩县](../Page/宣恩县.md "wikilink") [咸丰县](../Page/咸丰县.md "wikilink") [来凤县](../Page/来凤县.md "wikilink")                                                   |
| [荆门直隶州](../Page/荆门直隶州.md "wikilink") |                /                 |                                /                                |                                                                                                    [当阳县](../Page/当阳县.md "wikilink") [远安县](../Page/远安县.md "wikilink")                                                                                                    |

## 中华民国

[Wuhan_13.jpg](https://zh.wikipedia.org/wiki/File:Wuhan_13.jpg "fig:Wuhan_13.jpg")
1911年10月10日，[辛亥革命在湖北打响了第一枪](../Page/辛亥革命.md "wikilink")（[武昌起义](../Page/武昌起义.md "wikilink")），在湖北谘议局成立了武汉起义军政府。[中华民国成立](../Page/中华民国.md "wikilink")。

[中华民国成立后](../Page/中华民国.md "wikilink")，以[汉口为中心](../Page/汉口.md "wikilink")，湖北的[经济一度迅速发展](../Page/经济.md "wikilink")。但在[抗日战争中](../Page/抗日战争.md "wikilink")，湖北经济又受到严重破坏。

1920年代，[武汉三镇是历次革命风潮的中心之一](../Page/武汉三镇.md "wikilink")。包括京汉铁路二七大罢工、等。

## 中华人民共和国

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，原中南局设置在武汉，管辖湖北、[湖南](../Page/湖南省.md "wikilink")、[河南](../Page/河南省.md "wikilink")、[江西](../Page/江西省.md "wikilink")、[广东](../Page/广东省.md "wikilink")、[广西等六个省区](../Page/广西壮族自治区.md "wikilink")，湖北又有较大的发展，武汉在20世纪60年代以前是中南地区的中心。

[\*](../Category/湖北歷史.md "wikilink")