<table>
<caption>湖南地级行政区划</caption>
<thead>
<tr class="header">
<th></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

[湖南属于内陆省份](../Page/湖南.md "wikilink")，位于[中国中南部](../Page/中国.md "wikilink")[长江中游以南](../Page/长江.md "wikilink")，因大部分位于[洞庭湖以南而得名湖南](../Page/洞庭湖.md "wikilink")，又因[湘江贯穿于全境而简称](../Page/湘江.md "wikilink")“湘”，全省面积过半为湘江流域和洞庭湖流域。地图坐标为[东经](../Page/东经.md "wikilink")109°—114°、[北纬](../Page/北纬.md "wikilink")20°—30°之间，相邻有六个省市即[广东](../Page/广东省.md "wikilink")、[江西](../Page/江西省.md "wikilink")、[湖北](../Page/湖北省.md "wikilink")、[贵州](../Page/贵州省.md "wikilink")、[广西壮族自治区和](../Page/广西壮族自治区.md "wikilink")[重庆直辖市](../Page/重庆市.md "wikilink")。

[缩略图](https://zh.wikipedia.org/wiki/File:Chinese_landscape_screen.jpg "fig:缩略图")

[缩略图](https://zh.wikipedia.org/wiki/File:Mount_Heng_003.jpg "fig:缩略图")

## 地形

### 地形概况

全省以中、低山与[丘陵为主](../Page/丘陵.md "wikilink")，[面积约为](../Page/面积.md "wikilink")14.9万[平方公里](../Page/平方公里.md "wikilink")，占70.2％；[岗地与](../Page/岗地.md "wikilink")[平原约为](../Page/平原.md "wikilink")5.2万平方公里，占24.5％；[河流](../Page/河流.md "wikilink")[湖泊水域面积约为](../Page/湖泊.md "wikilink")1.1万平方公里，占5.3％。本省东、南、西三面山地环绕，中部和北部地势低平，呈马蹄形的[丘陵型盆地](../Page/丘陵.md "wikilink")。西北有[武陵山脉](../Page/武陵山脉.md "wikilink")，西南有[雪峰山脉](../Page/雪峰山脉.md "wikilink")，南部为[五岭山脉](../Page/五岭山脉.md "wikilink")（即[南岭山脉](../Page/南岭.md "wikilink")），东面为湘赣交界诸山，湘中地区大多为[丘陵](../Page/丘陵.md "wikilink")、[盆地和河谷冲击平原](../Page/盆地.md "wikilink")，除[衡山高达千米以外其他均为](../Page/衡山风景名胜区.md "wikilink")[海拔](../Page/海拔.md "wikilink")500米以下，湘北为[洞庭湖](../Page/洞庭湖.md "wikilink")、与[湘](../Page/湘江.md "wikilink")、[资](../Page/资江.md "wikilink")、[沅](../Page/沅水.md "wikilink")、[澧四水尾闾的河湖](../Page/澧水.md "wikilink")[冲积平原](../Page/冲积平原.md "wikilink")，地势很低，一般海拔50米以下，因此，湖南的[水系呈扇形状汇入洞庭湖](../Page/水系.md "wikilink")。

最高点位于[石门境内的](../Page/石门县.md "wikilink")[壶瓶山](../Page/壶瓶山.md "wikilink")，[海拔](../Page/海拔.md "wikilink")2099米。湖南海拔超过2000米的山峰有五座，另外四座为[炎陵](../Page/炎陵.md "wikilink")（原“酃县”）的[斗笠顶海拔](../Page/斗笠顶.md "wikilink")2052米，[桂东的](../Page/桂东.md "wikilink")[八面山](../Page/八面山.md "wikilink")2042米，[城步的](../Page/城步.md "wikilink")[二宝顶](../Page/二宝顶.md "wikilink")2021米和[道县的](../Page/道县.md "wikilink")[韭菜岭](../Page/韭菜岭.md "wikilink")2009米。

### 地形分布格局

武陵山脉位于湘西北角，属湘鄂山原的一部分，海拔1000米左右。山地地势大致从西北向东南渐降。澧水的源流与支流多发源于西北流向东南，成为平行状水系注入干流，最后转向东北方向流入洞庭湖。

湘西北的雪峰山脉地势较高，南端高达1500米左右，主峰[苏宝顶海拔](../Page/苏宝顶.md "wikilink")1934米。雪峰山脉为资江与沅水的[分水岭](../Page/分水岭.md "wikilink")，贯穿于湖南西部，成为湖南西部地区交通的主要屏障。

湘南边缘山地属五岭的一部分。一般海拔在1000—1500米左右，为长江与[珠江水系的分水岭](../Page/珠江.md "wikilink")。山间[盆地较多](../Page/盆地.md "wikilink")，谷地为交通要道。南岭山地，山体大，延伸长，山势高。

湘中大部属[湘中丘陵](../Page/湘中丘陵.md "wikilink")，[台地广布](../Page/台地.md "wikilink")，一般海拔在200米—500米之间，[红岩盆地众多](../Page/红岩.md "wikilink")，主要盆地有[衡阳盆地](../Page/衡阳盆地.md "wikilink")、[株洲盆地](../Page/株洲盆地.md "wikilink")、[长沙盆地](../Page/长沙盆地.md "wikilink")、[永兴](../Page/永兴.md "wikilink")[茶陵盆地和](../Page/茶陵.md "wikilink")[攸县盆地等](../Page/攸县.md "wikilink")，均为红岩盆地，盆地沿河区域常有河流冲积平原，为[人口密度较大的地区与](../Page/人口密度.md "wikilink")[农业区](../Page/农业.md "wikilink")。从形态上看，湘中丘陵的起伏与高度不完全一致，丘陵的顶部比较平坦。[衡山位于丘陵台地之上](../Page/衡山风景名胜区.md "wikilink")，主峰[祝融峰海拔](../Page/祝融峰.md "wikilink")1290米，为中国名山[五岳之一的](../Page/五岳.md "wikilink")[南岳](../Page/衡山风景名胜区.md "wikilink")。

湘北为洞庭湖湖积冲积平原，海拔大多在50米以下。河流、湖泊分布广，[堤垸众多](../Page/堤垸.md "wikilink")。一般的堤垸边缘的位置较垸心高，土壤以粉沙[壤土或轻](../Page/壤土.md "wikilink")[粘土为主](../Page/粘土.md "wikilink")，土壤肥沃，为重要产粮区。

### 主要山峰

|                                                                                                     |
| :-------------------------------------------------------------------------------------------------: |
| [湖南境内](../Page/湖南.md "wikilink")[海拔](../Page/海拔.md "wikilink")1900米以上[山峰](../Page/山峰.md "wikilink") |
|                                  [山峰名称](../Page/山峰.md "wikilink")                                   |
|                                  [壶瓶山](../Page/壶瓶山.md "wikilink")                                   |
|                                  [斗笠顶](../Page/斗笠顶.md "wikilink")                                   |
|                                  [八面山](../Page/八面山.md "wikilink")                                   |
|                                  [二宝顶](../Page/二宝顶.md "wikilink")                                   |
|                                  [韭菜岭](../Page/韭菜岭.md "wikilink")                                   |
|                                  [畚箕窝](../Page/畚箕窝.md "wikilink")                                   |
|                                  [南山顶](../Page/南山顶.md "wikilink")                                   |
|                                  [苏宝顶](../Page/苏宝顶.md "wikilink")                                   |
|                                  [狮子口](../Page/狮子口.md "wikilink")                                   |
|                                  [牛坡头](../Page/牛坡头.md "wikilink")                                   |
|                                  [猛坑石](../Page/猛坑石.md "wikilink")                                   |
|                                  [柴家山](../Page/柴家山.md "wikilink")                                   |

## 水文

### 湖泊

### 河流

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/湘江.md" title="wikilink">湘江水系</a></p></td>
<td style="text-align: center;"><p><a href="../Page/资江.md" title="wikilink">资江水系</a></p></td>
<td style="text-align: center;"><p><a href="../Page/沅水.md" title="wikilink">沅江水系</a></p></td>
<td style="text-align: center;"><p><a href="../Page/澧水.md" title="wikilink">澧水水系</a></p></td>
<td style="text-align: center;"><p>其他水系</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 三湘四水

**“三湘四水”**是“三湘”和“四水”两个概念的合称，大致包括了当今中国湖南省的全部范围，所以它常被湖南当地人作为对該省的代称。

其中： “三湘”是对中国湖南省主要土地区域的总称，通常也用“三湘大地”来代指湖南省。关于“三湘”的具体所指现众说不一，主要有以下三种：

  - 是指：漓湘、潇湘和蒸湘。（分别指湘江的三段流域。湘江上游流域称“漓湘”，潇水汇入至蒸水汇入一段流域称“潇湘”，蒸水汇入后至洞庭湖一段流域称“蒸湘”）
  - 是指：[湘南](../Page/湘南.md "wikilink")、湘西和湘北。
  - 是指：[湘潭](../Page/湘潭.md "wikilink")、[湘乡和](../Page/湘乡.md "wikilink")[湘阴](../Page/湘阴.md "wikilink")。

但一般第一种说法被认为最可靠，赞同的人也最多。

“四水”则是对流经湖南境内的四条大的[长江支流的总称](../Page/长江.md "wikilink")，包括：

  - [湘江](../Page/湘江.md "wikilink")
  - [资江](../Page/资江.md "wikilink")
  - [沅江](../Page/沅水.md "wikilink")
  - [澧水](../Page/澧水.md "wikilink")

它们最终都于洞庭湖汇入长江，湖南省大部分地区都在“四水”的流域内。

## 资料来源

  - 《湖南地理》、《湖南水利》

[\*](../Category/湖南地理.md "wikilink")