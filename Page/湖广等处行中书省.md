[YUAN(South_China).jpg](https://zh.wikipedia.org/wiki/File:YUAN\(South_China\).jpg "fig:YUAN(South_China).jpg")

**湖广等处行中书省**（湖广行中书省），为直属[元朝的](../Page/元朝.md "wikilink")[一级行政区](../Page/一级行政区.md "wikilink")，简称“[湖广](../Page/湖广.md "wikilink")”或“湖广省”，在当时民间多简称为**鄂州行省**、**潭州行省**、**湖广行省**。

[元世祖](../Page/元世祖.md "wikilink")[至元十一年](../Page/至元_\(忽必烈\).md "wikilink")（1274年）置**荆湖等路行中书省**，因拟取[鄂州而别称鄂州行省](../Page/鄂州.md "wikilink")。[至元十四年](../Page/至元.md "wikilink")（1277年），并鄂州行省入[潭州行省](../Page/潭州.md "wikilink")，治所[长沙](../Page/长沙.md "wikilink")。至元十八年（1281年），迁省治到鄂州，治所[武昌](../Page/武昌.md "wikilink")（今[武汉市武昌](../Page/武汉市.md "wikilink")）。

湖广行中书省为全国的10个[行中书省之一](../Page/行中书省.md "wikilink")，辖境包括今[长江以南](../Page/长江.md "wikilink")、[湖北南部](../Page/湖北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[贵州大部及](../Page/贵州.md "wikilink")[广西](../Page/广西.md "wikilink")、[海南全省](../Page/海南省.md "wikilink")、[广东](../Page/广东.md "wikilink")[雷州半岛](../Page/雷州半岛.md "wikilink")、[重庆东南部](../Page/重庆.md "wikilink")，下辖武昌、岳州、常德、澧州、辰州、沅州、兴国等30[路](../Page/路_\(行政区划\).md "wikilink")。今湖北、湖南西部、陕西南部地区属于[四川等处行中书省管辖](../Page/四川等处行中书省.md "wikilink")。

## [岭北湖南道](../Page/岭北湖南道.md "wikilink")（湖南道宣慰司）

1278年，改[宋](../Page/宋朝.md "wikilink")[荆湖南路为](../Page/荆湖南路.md "wikilink")[岭北湖南道或简称](../Page/岭北湖南道.md "wikilink")“湖南道”，“湖南道”下辖8个[路](../Page/路_\(行政区划\).md "wikilink")、2个[直隶州](../Page/直隶州.md "wikilink")。

  - [潭州路](../Page/潭州路.md "wikilink")
  - [衡州路](../Page/衡州路.md "wikilink")
  - [永州路](../Page/永州路.md "wikilink")
  - [郴州路](../Page/郴州路.md "wikilink")
  - [道州路](../Page/道州路.md "wikilink")
  - [全州路](../Page/全州路.md "wikilink")
  - [宝庆路](../Page/宝庆路.md "wikilink")
  - [武冈路](../Page/武冈路.md "wikilink")
  - [直隶州为](../Page/直隶州.md "wikilink")[茶陵州和](../Page/茶陵州.md "wikilink")[常宁州](../Page/常宁州.md "wikilink")

## [江南湖北道](../Page/江南湖北道.md "wikilink")

或简称“湖北道”，下辖的区域包括包括6个路

  - [岳阳路](../Page/岳阳路.md "wikilink")
  - [常德路](../Page/常德路.md "wikilink")
  - [澧州路](../Page/澧州路.md "wikilink")
  - [辰州路](../Page/辰州路.md "wikilink")
  - [沅州路](../Page/沅州路.md "wikilink")
  - [靖州路](../Page/靖州路.md "wikilink")

## [海北海南道](../Page/海北海南道.md "wikilink")

  - [雷州路](../Page/雷州路.md "wikilink")
  - [化州路](../Page/化州路.md "wikilink")
  - [高州路](../Page/高州路.md "wikilink")
  - [钦州路](../Page/钦州路.md "wikilink")
  - [廉州路](../Page/廉州路.md "wikilink")
  - [乾宁安抚司](../Page/乾宁安抚司.md "wikilink")
  - [南宁军](../Page/南宁军.md "wikilink")、[万安军和](../Page/万安军.md "wikilink")[吉阳军](../Page/吉阳军.md "wikilink")

## [广西两江道](../Page/广西两江道.md "wikilink")

  - [静江路](../Page/静江路.md "wikilink")
  - [南宁路](../Page/南宁路.md "wikilink")
  - [梧州路](../Page/梧州路.md "wikilink")
  - [浔州路](../Page/浔州路.md "wikilink")
  - [柳州路](../Page/柳州路.md "wikilink")
  - [庆远路](../Page/庆远路.md "wikilink")
  - [庆远南丹溪洞等处军民安抚司](../Page/庆远南丹溪洞等处军民安抚司.md "wikilink")（[庆远路](../Page/庆远路.md "wikilink")）
  - [容州](../Page/容州.md "wikilink")（[容州路](../Page/容州路.md "wikilink")）
  - [象州](../Page/象州_\(古代\).md "wikilink")（[象州路](../Page/象州路.md "wikilink")）
  - [宾州](../Page/宾州_\(广西\).md "wikilink")（[宾州路](../Page/宾州路.md "wikilink")）
  - [横州](../Page/横州.md "wikilink")（[横州路](../Page/横州路.md "wikilink")）
  - [融州](../Page/融州.md "wikilink")（[融州路](../Page/融州路.md "wikilink")）
  - [平乐府](../Page/平乐府.md "wikilink")
  - [郁林州](../Page/郁林州.md "wikilink")
  - [藤州](../Page/藤州.md "wikilink")
  - [贺州](../Page/贺州_\(隋朝\).md "wikilink")
  - [贵州](../Page/貴州_\(廣西\).md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[元史](../Page/元史.md "wikilink")·志第十五·地理六》

<!-- end list -->

  - 网页

<!-- end list -->

  - [行政区划·历史](https://web.archive.org/web/20090601215800/http://www.xzqh.org/lishi/index.htm)

## 参见

  - [元朝行政区划](../Page/元朝行政区划.md "wikilink")
  - [湖广](../Page/湖广.md "wikilink")
      - [湖廣等處承宣布政使司](../Page/湖廣等處承宣布政使司.md "wikilink")
      - [湖广总督](../Page/湖广总督.md "wikilink")

{{-}}

[Category:行中书省](../Category/行中书省.md "wikilink")
[Category:湖北古代行政区划](../Category/湖北古代行政区划.md "wikilink")
[Category:湖南古代行政区划](../Category/湖南古代行政区划.md "wikilink")
[Category:广西古代行政区划](../Category/广西古代行政区划.md "wikilink")
[Category:海南古代行政区划](../Category/海南古代行政区划.md "wikilink")
[Category:广东古代行政区划](../Category/广东古代行政区划.md "wikilink")
[Category:贵州古代行政区划](../Category/贵州古代行政区划.md "wikilink")