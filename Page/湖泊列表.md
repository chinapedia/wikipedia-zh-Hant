[Baku_Bulvar.jpg](https://zh.wikipedia.org/wiki/File:Baku_Bulvar.jpg "fig:Baku_Bulvar.jpg")湖岸\]\]
**湖泊列表**將[湖泊分到各大洲表列展示](../Page/湖泊.md "wikilink")。

## [非洲湖泊列表](../Page/非洲湖泊列表.md "wikilink")

  - [维多利亚湖](../Page/维多利亚湖.md "wikilink")，非洲第一大湖，世界第二大湖。
  - [坦噶尼喀湖](../Page/坦噶尼喀湖.md "wikilink")，世界第二深湖。
  - 塔纳湖，非洲最高湖。
  - [乍得湖](../Page/乍得湖.md "wikilink")，内陆盆地最大湖泊。
  - [马拉维湖](../Page/马拉维湖.md "wikilink")，非洲有涨潮和落潮的湖泊。

## [南極洲湖泊列表](../Page/南極洲湖泊列表.md "wikilink")

  - [維達湖](../Page/維達湖.md "wikilink")
  - [沃斯托克湖](../Page/沃斯托克湖.md "wikilink")，是世界最大的冰下湖。
  - [弗里克塞尔湖](../Page/弗里克塞尔湖.md "wikilink")，長4.5公里，在維多利亞地的泰勒谷下端，加拿大冰川和聯邦冰川之間。

## [亚洲湖泊列表](../Page/亚洲湖泊列表.md "wikilink")

  - [贝加尔湖](../Page/贝加尔湖.md "wikilink")，世界最深的湖，水量最多的淡水湖，位于[俄罗斯的](../Page/俄罗斯.md "wikilink")[西伯利亚中南部](../Page/西伯利亚.md "wikilink")。
  - [里海](../Page/里海.md "wikilink")，咸水湖，世界面积最大的湖，位于亚欧分界处。
  - [巴尔喀什湖](../Page/巴尔喀什湖.md "wikilink")，位于[中亚的](../Page/中亚.md "wikilink")[哈萨克斯坦境内](../Page/哈萨克斯坦.md "wikilink")，湖水一半淡一半咸。
  - [咸海](../Page/咸海.md "wikilink")，咸水湖，原湖面大部已经干涸，位于中亚。
  - [乌布苏湖](../Page/乌布苏湖.md "wikilink")，咸水湖，位于[蒙古西北靠近俄蒙交界处](../Page/蒙古.md "wikilink")。
  - [兴凯湖](../Page/兴凯湖.md "wikilink")，淡水湖，位于[中国](../Page/中国.md "wikilink")[东北与俄罗斯交界处](../Page/东北.md "wikilink")。
  - [青海湖](../Page/青海湖.md "wikilink")，咸水湖，位于[青藏高原](../Page/青藏高原.md "wikilink")。
  - [纳木错](../Page/纳木错.md "wikilink")，咸水湖，位于[青藏高原](../Page/青藏高原.md "wikilink")。
  - [太湖](../Page/太湖.md "wikilink")，淡水湖，位于[长江下游的平原](../Page/长江.md "wikilink")。
  - [洞里萨湖](../Page/洞里萨湖.md "wikilink")，淡水湖，位于[中南半岛上](../Page/中南半岛.md "wikilink")[柬埔寨西部](../Page/柬埔寨.md "wikilink")。
  - [船灣淡水湖](../Page/船灣淡水湖.md "wikilink")，淡水湖，位于[香港東北部](../Page/香港.md "wikilink")。

## [欧洲湖泊列表](../Page/欧洲湖泊列表.md "wikilink")

  - [拉多加湖](../Page/拉多加湖.md "wikilink")，位于俄罗斯靠近[芬兰的西北部](../Page/芬兰.md "wikilink")。
  - [奥涅加湖](../Page/奥涅加湖.md "wikilink")，拉多加湖附近，东北方向上。

## [北美洲湖泊列表](../Page/北美洲湖泊列表.md "wikilink")

  - [大熊湖](../Page/大熊湖.md "wikilink")、[大奴湖](../Page/大奴湖.md "wikilink")，淡水湖，位于加拿大境内[马更些河流域](../Page/马更些河.md "wikilink")。

### [五大湖](../Page/五大湖.md "wikilink")

这几个湖位于北美东部美国和加拿大交界处，水流注入[圣劳伦斯河](../Page/圣劳伦斯河.md "wikilink")，由西向东为：

  - [蘇必略湖](../Page/蘇必略湖.md "wikilink")
  - [密歇根湖](../Page/密歇根湖.md "wikilink")，完全位于美国境内。
  - [休伦湖](../Page/休伦湖.md "wikilink")
  - [伊利湖](../Page/伊利湖.md "wikilink")
  - [安大略湖](../Page/安大略湖.md "wikilink")

## [南美洲湖泊列表](../Page/南美洲湖泊列表.md "wikilink")

  - [马拉开波湖](../Page/马拉开波湖.md "wikilink")，南部水体为淡水，北部为咸水，位于委内瑞拉西北沿海地区。
  - [的的喀喀湖](../Page/的的喀喀湖.md "wikilink")，淡水湖，位于秘鲁与玻利维亚交界的安第斯山脉中。

## [大洋洲湖泊列表](../Page/大洋洲湖泊列表.md "wikilink")

  - 北[艾尔湖](../Page/艾尔湖.md "wikilink")，位于澳大利亚中部盆地，[时令湖](../Page/时令湖.md "wikilink")。

## 参见

  - [湖泊](../Page/湖泊.md "wikilink")
      - [湖泊面积列表](../Page/湖泊面积列表.md "wikilink")

[湖泊列表](../Category/湖泊列表.md "wikilink")