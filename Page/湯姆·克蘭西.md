**湯姆·克蘭西**（，全名，）\[1\]\[2\]是[美國一位暢銷小說](../Page/美國.md "wikilink")[作家](../Page/作家.md "wikilink")，擅長寫作以[冷戰時期為背景的](../Page/冷戰.md "wikilink")[政治](../Page/政治.md "wikilink")、[軍事科技以及](../Page/軍事科技.md "wikilink")[間諜故事等等](../Page/間諜.md "wikilink")，其名字已經成為了[軍事小說的代表](../Page/軍事.md "wikilink")。

當批評湯姆·克蘭西的作品一成不變時，相反地，其作品在市場上大為被推薦，而且熱賣，書迷們喜愛他作品中糾結的陰謀、對於軍事科技認知之準確以及諜報活動的生動描寫。

湯姆·克蘭西是於1990年代中期熱賣了20萬本作品的兩位作家的其中之一（[約翰·葛里遜為另外一位](../Page/約翰·葛里遜.md "wikilink")）\[3\]。他在《[紐約時報](../Page/紐約時報.md "wikilink")》暢銷書排行榜創下單一作品蟬聯榜首數週、上榜數十週之久的佳績。克蘭西於1989年出版的《迫切的危機》賣出了1,625,544本精裝本，使他成為1980年代暢銷小說作家的第一名。\[4\]多部克蘭西的作品被改編成同名電影或者[電子遊戲](../Page/湯姆·克蘭西電子遊戲列表.md "wikilink")。

湯姆·克蘭西於美國當地時間2013年10月1日馬里蘭州住家附近的醫院辭世，享壽66歲。\[5\]

## 早期生涯

湯姆生於[美國](../Page/美國.md "wikilink")[馬利蘭州](../Page/馬利蘭州.md "wikilink")[巴爾的摩醫療之星富蘭克林廣場醫療中心](../Page/巴爾的摩.md "wikilink")，並在巴爾的摩[諾斯伍德成長](../Page/諾斯伍德_\(巴爾的摩\).md "wikilink")。\[6\]\[7\]\[8\]

湯姆是家中的次子，其父親托馬斯在[美國郵政署工作](../Page/美國郵政署.md "wikilink")，母親凱瑟琳則於一間商店工作。\[9\]\[10\]湯姆於[陶森洛約拉布雷克菲爾德的私立天主教中學讀書](../Page/陶森.md "wikilink")，於1965年畢業。\[11\]\[12\]\[13\]之後就讀[洛約拉書院](../Page/馬里蘭洛約拉大學.md "wikilink")，並於1969年獲得[英語文學學位畢業](../Page/英語文學.md "wikilink")。\[14\]\[15\]在大學期間亦是棋藝學會的主席。\[16\]

畢業後，他加入了[陸軍後備軍官訓練團](../Page/陸軍後備軍官訓練團.md "wikilink")，但因其深度[近視問題而被拒絕](../Page/近視.md "wikilink")。\[17\]\[18\]之後他在[康涅狄格州](../Page/康涅狄格州.md "wikilink")[哈特福一家保險公司任職](../Page/哈特福.md "wikilink")。\[19\]1973年，他轉往馬利蘭州[奧因斯另一家保險公司工作](../Page/奧因斯.md "wikilink")。\[20\]\[21\]\[22\]\[23\]

1980年，他收購了位於奧因斯的保險公司，並開始在空餘時間創作小說。\[24\]\[25\]在奧因斯的工作期間，他完成了第一作《[獵殺紅色十月號](../Page/獵殺紅色十月號.md "wikilink")》。\[26\]

## 作家生涯

[Tom_Clancy_at_Burns_Library,_Boston_College.jpg](https://zh.wikipedia.org/wiki/File:Tom_Clancy_at_Burns_Library,_Boston_College.jpg "fig:Tom_Clancy_at_Burns_Library,_Boston_College.jpg")的簽書會，攝於1989年\]\]
克蘭西的作家生涯始於1982年，即他開始寫《獵殺紅色十月號》之時。他本身並沒有從軍的經驗，因此是透過精緻的取材才能得到情報。於1985年，海軍學會出版社以5000美金的稿費購買了該書的版權。\[27\]\[28\]出版商非常欣賞該書，而出版社編輯德博拉·格羅夫納更指出「我認為我們這裡有一本潛在的暢銷書，如果我們不抓緊這次機會，別人就會。」。\[29\]雖然如此，出版商仍然要求克蘭西削減大量的技術細節，並將頁數刪減了至少100頁。\[30\]克蘭西原本認為該書能售出5,000本，但結果卻售出了45,000本。\[31\]\[32\]出版後，該書獲得[美國總統](../Page/美國總統.md "wikilink")[-{zh-hans:罗纳德·里根;zh-hant:隆納·雷根}-的讚賞](../Page/罗纳德·里根.md "wikilink")。當雷根讚賞此書後，其銷量很快就上升，售出了逾300,000本[精裝書和](../Page/精裝書.md "wikilink")200萬本[平裝書](../Page/平裝書.md "wikilink")。\[33\]\[34\]\[35\]評論家大多稱讚這本書極度精確的技術用語，並讓克蘭西能夠和美軍一些高級軍官會晤。\[36\]

隨著處女作成為暢銷書，克蘭西又寫了好幾本軍事小說，其中包括1986年的《[赤色風暴](../Page/赤色風暴.md "wikilink")》、1987年的《[愛國者遊戲](../Page/愛國者遊戲.md "wikilink")》等。這些書都成為了暢銷書，而部份更被改編成[電影和](../Page/電影.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")。克蘭西本人亦被評價為擁有「綿密的取材能力、精密的分析方式、大膽的軍事預言及政治風向」。之後，他創作的以原證券交易員的軍事歷史學家，爾後擔任[中央情報局分析師](../Page/中央情報局.md "wikilink")、[美國副總統及](../Page/美國副總統.md "wikilink")[美國總統的](../Page/美國總統.md "wikilink")[傑克·萊恩為主角的系列作品成為其代表作](../Page/傑克·萊恩.md "wikilink")。

到了1988年，《獵殺紅色十月號》已為克蘭西帶來130萬美元的收入，而之後的三部作品亦帶來300萬美元收入。\[37\]於1997，企鵝普特南出版社表示願意以5000萬美元購買他兩部作品的版權，而[育碧软件旗下的紅色風暴娛樂亦表示願意與克蘭西簽下為期四年的](../Page/育碧软件.md "wikilink")2500萬美元合約。\[38\]後來，企鵝普特南出版社又表示若克蘭西願意與傑夫·羅賓合作，酬金就能增加2200萬。結果，克蘭西在該年獲得了9700萬美元的酬金。\[39\]

於2008年，育碧软件購買了克蘭西的作品的冠名權，這些作品包括電影、書籍和電子遊戲。\[40\]

## 政治觀點

克蘭西也是[共和黨的忠實支持者](../Page/共和黨_\(美國\).md "wikilink")，並且曾大量捐款。他最欣賞的美國總統是[-{zh-hans:罗纳德·里根;zh-hant:隆納·雷根}-](../Page/罗纳德·里根.md "wikilink")。在[911事件發生後](../Page/911事件.md "wikilink")，克蘭西表示美國左翼政治家需要負上一定的責任，因為他們清空了[中央情报局的內部](../Page/中央情报局.md "wikilink")。\[41\]

近年來，克蘭西與將軍[安東尼·津尼合作](../Page/安東尼·津尼.md "wikilink")。安東尼·津尼本身除了是總統[喬治·W·布什的反對者](../Page/喬治·W·布什.md "wikilink")，還是反對前國防部長[唐纳德·拉姆斯菲尔德的人](../Page/唐纳德·拉姆斯菲尔德.md "wikilink")。\[42\]

於2001年9月11日，克蘭西被[CNN的朱迪](../Page/CNN.md "wikilink")·伍德拉夫訪問。\[43\]在採訪過程中，克蘭西指出「伊斯蘭教不允許自殺」，且911事件中恐怖份子缺乏規劃，因此認為911事件可能不是恐怖襲擊。他亦認為[美國情報體系向傳媒施壓](../Page/美國情報體系.md "wikilink")，令他們向公眾發佈911事件一定是恐怖襲擊的消息。\[44\]

克蘭西自1978年起便是[美国全国步枪协会的一個終身會員](../Page/美国全国步枪协会.md "wikilink")。\[45\]

## 逝世

於2013年10月1日，克蘭西於美國馬里蘭州住家附近的[约翰·霍普金斯医院辭世](../Page/约翰·霍普金斯医院.md "wikilink")，享年66歲。\[46\]\[47\]他的死因沒有公開。克蘭西死時，其妻子亞歷山德拉·克蘭西、女兒亞歷克西斯、以及前妻萬達·金的四個孩子米歇爾·班迪、克莉絲汀·布諾斯、凱瑟琳·克蘭西和托馬斯·克蘭西三世都在其身邊。\[48\]曾獲[普利策奖的](../Page/普利策奖.md "wikilink")[斯蒂芬·亨特在](../Page/斯蒂芬·亨特.md "wikilink")《[芝加哥論壇報](../Page/芝加哥論壇報.md "wikilink")》指出「當《獵殺紅色十月號》出版後，克蘭西重新定義，並擴大了軍事小說派，使得很多作家能夠創作軍事小說。」\[49\]。

育碧软件於湯姆·克蘭西辭世的消息被媒體披露後，透過Facebook官方粉絲團頁面，表達了對這位一代軍事作家離去的悲痛之意，並向其家屬致意。育碧软件在聲明中讚揚湯姆‧克蘭西是名非凡的作家，細膩文字與引人入勝的故事對那些被他所征服的全球讀者而言是一份禮物；育碧软件旗下開發團隊，尤其是紅色風暴娛樂工作室相當感恩能有機會與他合作，向他學習。育碧软件未來仍會持續使用「湯姆·克蘭西」的名字創作。\[50\]

## 主要的作品

  - 《[獵殺紅色十月號](../Page/獵殺紅色十月號.md "wikilink")》（，1984年）

<!-- end list -->

  -
    最新銳的蘇聯核子動力潛艇「紅色十月號」計畫往美國投誠。得知此情報的美國計畫秘密接收紅色十月號。本作於1990年改編為同名電影。

<!-- end list -->

  - 《[红色風暴](../Page/红色風暴.md "wikilink")》（，1986年）

<!-- end list -->

  -
    因國內的油田受到破壞而發生能源危機的蘇聯決定要侵略中東。為了牽制[北約而攻擊了](../Page/北約.md "wikilink")[冰島及](../Page/冰島.md "wikilink")[西德](../Page/西德.md "wikilink")。本作並非傑克·萊恩系列的作品。

<!-- end list -->

  - 《[愛國者遊戲](../Page/愛國者遊戲.md "wikilink")》（，1987年）

<!-- end list -->

  -
    在英國旅行中的傑克·萊恩偶然遭遇受到恐怖份子襲擊的英國威尔士亲王夫妇並阻止悲劇的發生。因此恐怖份子計畫於美國襲擊萊恩一家人。

<!-- end list -->

  - 《[克里姆林宮的樞機主教](../Page/克里姆林宮的樞機主教.md "wikilink")》（，1988年）

<!-- end list -->

  -
    潛伏在蘇聯高官身邊，代號[樞機主教的間諜陷入危機](../Page/樞機主教.md "wikilink")。為了救出樞機主教，萊恩前往蘇聯。

<!-- end list -->

  - 《[迫切的危機](../Page/迫切的危機.md "wikilink")》（，1989年）

<!-- end list -->

  -
    為了消滅哥倫比亞毒品組織而秘密潛入的美國特種部隊，因為政治的因素而被捨棄。為此決心將他們救出的傑克·萊恩與[約翰·克拉克四處奔走著](../Page/約翰·克拉克.md "wikilink")。同名電影原著。

<!-- end list -->

  - 《[恐懼的總和](../Page/恐懼的總和.md "wikilink")》（，1991年）

<!-- end list -->

  -
    歷史上使中東和平動盪不安的恐怖份子在美國引爆核彈恐怖攻擊。在恐怖與疑惑中面臨與蘇聯全面宣戰的危機。同名電影原著。

<!-- end list -->

  - 《[冷血悍將](../Page/冷血悍將_\(小說\).md "wikilink")》（，1993年）

<!-- end list -->

  -
    為了深愛的少女而單身與毒品組織挑戰的約翰·凱利，因此事件改名為約翰·克拉克。

<!-- end list -->

  - 《[美日開戰](../Page/美日開戰.md "wikilink")》（，1994年）

<!-- end list -->

  -
    美國訂立貿易改革法案，針對日本與日本關稅同步。因次陷入危機的日本經濟的支配者對美國有著戰爭的陰謀。

<!-- end list -->

  - 《[總統命令](../Page/總統命令.md "wikilink")》（，1996年）

<!-- end list -->

  -
    美國政治核心崩壞，由萊恩繼任總統。此時發生伊朗為了併吞伊拉克所發動的戰爭。

<!-- end list -->

  - 《[彩虹六號](../Page/彩虹六號.md "wikilink")》（，1996年）

<!-- end list -->

  -
    冷戰後針對國際恐怖活動，北約組織成立機密的多國籍精英反恐怖部隊
    隊名"彩虹"。在一連串偶然且不相關，頻頻發生的恐怖事件，在背後似乎有著什麼陰謀。同名射击游戏原著。

<!-- end list -->

  - [Net Force系列](../Page/Net_Force.md "wikilink")（，1999－2013年）
  - 《[熊與龍](../Page/The_Bear_and_the_Dragon.md "wikilink")》（，2000年）

<!-- end list -->

  -
    美国总统[杰克·萊恩公然承认](../Page/杰克·萊恩.md "wikilink")[台灣](../Page/台灣.md "wikilink")。同时[梵蒂岡大使被中國警官杀害](../Page/梵蒂岡.md "wikilink")，因此各地發動的拒買運動使中國經濟不景气。迫于资源危机中國进攻[西伯利亞以获取資源](../Page/西伯利亞.md "wikilink")。
    美俄最终联合打败并颠覆中华人民共和国。

<!-- end list -->

  - 《[紅兔子](../Page/紅兔子.md "wikilink")》（，2002年）

<!-- end list -->

  -
    在蘇聯的影響下，波蘭的反政府活動日益活躍，波蘭出身的羅馬教宗感到同感，考慮著辭掉教宗的職位並回到波蘭支援，得知此消息的蘇聯計畫要暗殺教宗。

<!-- end list -->

  - 《[老虎牙](../Page/老虎牙.md "wikilink")》（，2005年）

<!-- end list -->

  -
    這本書介紹了傑克·萊恩的兒子和兩個侄子作為他遺產的繼承人。

<!-- end list -->

  - 《》（，2010年）

<!-- end list -->

  -
    與格蘭特·布萊克伍德合著。

<!-- end list -->

  - 《[全面围攻](../Page/全面围攻.md "wikilink")》(抵抗全敌)（，2011年）

<!-- end list -->

  -
    與彼得·特萊茨合著。本作並非傑克·萊恩系列的作品。

<!-- end list -->

  - 《[锁定](../Page/锁定.md "wikilink")》（，2011年）

<!-- end list -->

  -
    與馬克·格里尼合著。

<!-- end list -->

  - 《[入侵载体](../Page/入侵载体.md "wikilink")》（，2012年）

<!-- end list -->

  -
    與馬克·格里尼合著。

## 參見

  - [傑克·萊恩](../Page/傑克·萊恩.md "wikilink")
  - [史蒂芬·庫恩斯](../Page/史蒂芬·庫恩斯.md "wikilink")
  - [戴爾·布朗](../Page/戴爾·布朗.md "wikilink")

## 參考文獻

## 外部連結

  -
  -
  - [湯姆·克蘭西官方網站](http://www.tomclancy.com/)

  - Library resources in [your
    library](http://tools.wmflabs.org/ftl/cgi-bin/ftl?at=wp&au=Tom+Clancy)
    and in [other
    libraries](http://tools.wmflabs.org/ftl/cgi-bin/ftl?at=wp&au=Tom+Clancy&library=0CHOOSE0)
    by Tom Clancy

  - [Transcript of interview with Deborah Norville on the War in Iraq —
    April 2004](http://www.msnbc.msn.com/id/5137382/)

  - [Tom Clancy](http://www.ibdof.com/author_books.php?author=205) at
    the Internet Book Database of Fiction

  -
  - [The Page of Tom Clancy's Primary
    Publisher](http://us.penguingroup.com/static/html/author/tomclancy.html)

  - [*Booknotes* interview with Clancy and Gen. Fred Franks on *Into the
    Storm: A Study in Command*, July 13,
    1997.](https://web.archive.org/web/20131005155654/http://www.booknotes.org/Watch/86425-1/Tom+Clancy.aspx)

  - [*In Depth* interview with Clancy, February 3,
    2002](http://www.c-spanvideo.org/program/TomC)

  - [傑克·雷恩百科事典（日文）](https://web.archive.org/web/20060224120825/http://www.jttk.zaq.ne.jp/shig/ryan/)

[C](../Category/美国小说家.md "wikilink")
[C](../Category/愛爾蘭裔美國人.md "wikilink")
[C](../Category/巴尔的摩人.md "wikilink")
[Category:20世纪作家](../Category/20世纪作家.md "wikilink")
[\*](../Category/汤姆·克兰西.md "wikilink")
[Category:21世紀美國小說家](../Category/21世紀美國小說家.md "wikilink")
[C](../Category/馬利蘭洛約拉大學校友.md "wikilink")

1.

2.  [Tom Clancy, Best-Selling Novelist of Military Thrillers, Dies
    at 66](http://www.nytimes.com/2013/10/03/books/tom-clancy-best-selling-novelist-of-military-thrillers-dies-at-66.html?_r=0)，The
    New York Times，2013年10月2日

3.

4.

5.
6.
7.

8.

9.

10.

11.
12.
13.
14.
15.
16.
17.
18.
19.

20.
21.
22.
23.
24.
25.

26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.

38.

39.
40.

41.

42. Paperback Writer, *The New Republic*, May 25, 2004.

43.

44.

45.

46.
47.

48.

49. [Tom Clancy, author, dead
    at 66](http://www.chicagotribune.com/entertainment/chi-tom-clancy-dead-20131002,0,2346736.story),
    Tribune staff, 2:42 p.m. CDT, October 2, 2013, *Chicago Tribune*

50. [Ubisoft saddened by Tom Clancy's
    death](http://www.videogamer.com/ps4/tom_clancys_the_division/news/ubisoft_saddened_by_tom_clancys_death_2.html)