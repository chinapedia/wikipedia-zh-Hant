[Yapasuyongʉ_Yulunana.jpg](https://zh.wikipedia.org/wiki/File:Yapasuyongʉ_Yulunana.jpg "fig:Yapasuyongʉ_Yulunana.jpg")
[臺灣原住民鄒族領袖高一生(吾雍．雅達烏猶卡那)與湯守仁(湯川一丸)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg](https://zh.wikipedia.org/wiki/File:臺灣原住民鄒族領袖高一生\(吾雍．雅達烏猶卡那\)與湯守仁\(湯川一丸\)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg "fig:臺灣原住民鄒族領袖高一生(吾雍．雅達烏猶卡那)與湯守仁(湯川一丸)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg")（右）與湯守仁（左），兩人均於1954年慘遭[中國國民黨政府殺害](../Page/中國國民黨政府.md "wikilink")\]\]
[1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_in_Alishan_mountains_of_TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_in_Alishan_mountains_of_TAIWAN.jpg "fig:1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_in_Alishan_mountains_of_TAIWAN.jpg")、[樂信·瓦旦與湯守仁於阿里山合影](../Page/樂信·瓦旦.md "wikilink")\]\]
[1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山賓館_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_at_guest_house_in_Alishan_mountains_of_TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山賓館_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_at_guest_house_in_Alishan_mountains_of_TAIWAN.jpg "fig:1951臺灣原住民領袖樂信．瓦旦、吾雍．雅達烏猶卡那與湯守仁於阿里山賓館_Indigenous_Taiwanese_Leaders_Loshin_Wadan,_Uyongu_Yatauyungana,_and_Tang_Shou-jen_at_guest_house_in_Alishan_mountains_of_TAIWAN.jpg")、[樂信·瓦旦與湯守仁於阿里山賓館合影](../Page/樂信·瓦旦.md "wikilink")\]\]

**湯守仁**（[鄒語](../Page/鄒語.md "wikilink")：，[音譯](../Page/音譯.md "wikilink")：**雅巴斯勇‧優路拿納**，[日語](../Page/日語.md "wikilink")：，）[臺灣](../Page/臺灣.md "wikilink")[鄒族人](../Page/鄒族.md "wikilink")，生於[日治臺灣](../Page/日治臺灣.md "wikilink")[臺南州](../Page/臺南州.md "wikilink")[嘉義郡](../Page/嘉義郡.md "wikilink")[蕃地](../Page/蕃地_\(嘉義郡\).md "wikilink")，[嘉義縣縣參議員](../Page/嘉義縣議會.md "wikilink")、教師，[二二八事件嘉義地區原住民反抗軍領袖](../Page/二二八事件.md "wikilink")，[臺灣白色恐怖時期受難者](../Page/臺灣白色恐怖時期.md "wikilink")。

## 生平

### 戰前

湯守仁於[日治時期生於今](../Page/台灣日治時期.md "wikilink")[阿里山鄉境內](../Page/阿里山鄉.md "wikilink")，曾取日本名**湯川一丸**，是[二戰期間](../Page/二戰.md "wikilink")[日本關東軍基層軍官](../Page/日本關東軍.md "wikilink")。

戰爭初期，湯守仁被徵調至中國華南地區戰俘收容所當警衛，因表現特殊，破例保送至[日本陸軍士官學校受訓](../Page/日本陸軍士官學校.md "wikilink")
，由見習士官轉升少尉；1945年8月在東北被[蘇聯軍隊所俘虜](../Page/蘇聯軍事史.md "wikilink")，送至[西伯利亞戰俘營數月](../Page/西伯利亞.md "wikilink")，後經證實非日本人，始被釋放並遣送回台；二戰結束後，日本政府為酬勞各級官兵，軍階無條件各升一級，故最後階級為中尉，亦稱「波茨坦中尉」（因日本接受[波茨坦宣言](../Page/波茨坦宣言.md "wikilink")，無條件投降），為該族當時位階最高的軍官。之後，湯守仁於1946年回到家鄉，並被國民政府受聘為當地體育老師。

### 二二八事件

1947年爆發[二二八事件](../Page/二二八事件.md "wikilink")，他向台南縣吳鳳鄉（今嘉義縣[阿里山鄉](../Page/阿里山鄉.md "wikilink")）鄉長[高一生要求](../Page/高一生.md "wikilink")，於3月7日率領六十多名曾赴服役於日本陸海軍之鄒族青年協助[嘉義市市民維持治安](../Page/嘉義市.md "wikilink")，除了攻下[紅毛埤地區軍械庫外](../Page/蘭潭水庫.md "wikilink")，並連同[嘉義民兵與部分](../Page/嘉義民兵.md "wikilink")[二七部隊圍攻](../Page/二七部隊.md "wikilink")[嘉義水上機場](../Page/嘉義水上機場.md "wikilink")。之後，因主張繼續對抗，與當時嘉義民兵意見分歧，加上高一生希望以阿里山為最後保衛防線，故湯守仁乃於3月10日率領鄒族部隊回山，並於重要路口架設輕重機槍，以阿里山作為守護鄒族族人之最後防線。二二八事件後，高一生、湯守仁被捕，因另一位未捲入事件的原住民領袖、第一屆台灣省議員[樂信·瓦旦](../Page/樂信·瓦旦.md "wikilink")（[泰雅族](../Page/泰雅族.md "wikilink")）極力力保，國民政府將湯守仁與高一生兩位開釋。

### 高砂族自治會

1949年
，湯守仁與[樂信·瓦旦組織受](../Page/樂信·瓦旦.md "wikilink")[簡吉](../Page/簡吉.md "wikilink")、[陳顯富之命](../Page/陳顯富.md "wikilink")，組「[高砂族自治會](../Page/高砂族自治會.md "wikilink")」（即「蓬萊族解放委員會」）\[1\]，該組織主張原住民高度自治，他並擔任該自治會軍事主委。1950年，他當選阿里山鄉代表，隨後並參選縣參議員獲得當選。高一生與湯守仁辦理自新之後，受當局運用，總統府機要室資料組主任蔣經國提出山地招撫政策，湯守仁被任命為保安司令部少校參謀\[2\]，同年10月該高砂族自治會被政府所破獲，雖他交出武器並隨即獲釋，不過卻仍在1952年再度被捕，並隨著情治單位對省工委案後續收網，將湯守仁之自白書有關參與組織與武器繳械等問題作為叛亂證據，1954年2月與[高一生](../Page/高一生.md "wikilink")、[樂信·瓦旦及](../Page/樂信·瓦旦.md "wikilink")[汪清山等原住民族領袖同遭槍決](../Page/汪清山.md "wikilink")\[3\]。

## 親屬

他的姪女[湯蘭花是台灣的女演員](../Page/湯蘭花.md "wikilink")，而他亦是[湯英伸的叔公](../Page/湯英伸.md "wikilink")。

## 參考來源

## 外部連結

  - [4/17《吾雍殉難紀念日》《樂信殉難紀念日》](http://kongtaigi.pts.org.tw/2013/04/417.html)，
    [公視](../Page/公視.md "wikilink")《台語新聞》， 2013年4月16日

[Category:嘉義縣議員](../Category/嘉義縣議員.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:二二八事件相關人物](../Category/二二八事件相關人物.md "wikilink")
[Category:關東軍人物](../Category/關東軍人物.md "wikilink")
[Category:台籍日本兵](../Category/台籍日本兵.md "wikilink")
[Category:日本陸軍士官學校校友](../Category/日本陸軍士官學校校友.md "wikilink")
[Category:台灣原住民軍事人物](../Category/台灣原住民軍事人物.md "wikilink")
[Category:阿里山人](../Category/阿里山人.md "wikilink")
[Category:鄒族人](../Category/鄒族人.md "wikilink")
[Shou守](../Category/湯姓.md "wikilink")
[Category:被槍決的台灣人](../Category/被槍決的台灣人.md "wikilink")
[Category:臺灣原住民族運動者](../Category/臺灣原住民族運動者.md "wikilink")
[Category:湯守仁等叛亂及貪污案](../Category/湯守仁等叛亂及貪污案.md "wikilink")

1.

2.

3.