<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}}學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/玉井區.md" title="wikilink">噍吧哖</a><a href="../Page/公學校.md" title="wikilink">公學校</a>（今玉井國小）畢業</li>
<li><a href="../Page/臺南大學.md" title="wikilink">臺南師範學校</a>（今<a href="../Page/國立臺南大學.md" title="wikilink">國立臺南大學前身</a>）</li>
<li><a href="../Page/台灣總督府.md" title="wikilink">台灣總督府警察官練習所甲種班</a></li>
<li><a href="../Page/中央大學_(日本).md" title="wikilink">日本中央大學法律系就讀</a></span></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}}經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/玉井糖廠.md" title="wikilink">玉井糖廠技工</a>(1922年)</li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州乙種巡查考試及格</a>(1926年)</li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州甲種巡查考試及格</a></li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州警察教習生</a></li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州</a><a href="../Page/東石郡.md" title="wikilink">東石郡</a><a href="../Page/巡查.md" title="wikilink">巡查</a>(1927年-1929年)</li>
<li><p>及格(1929年)</p></li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州開山</a><a href="../Page/派出所.md" title="wikilink">派出所次席巡查</a>(1930年-1934年)</li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州</a><a href="../Page/警部補.md" title="wikilink">警部補</a>(1934年-1935年)</li>
<li><a href="../Page/臺南州.md" title="wikilink">臺南州</a><a href="../Page/新豐郡.md" title="wikilink">新豐郡保安衛生系</a>(-1939年)</li>
<li><p>司法科及格(1942年)</p></li>
<li><p>行政科及格(1943年)</p></li>
<li><a href="../Page/台灣總督府.md" title="wikilink">台灣總督府登錄律師</a>(1943年)</li>
<li><a href="../Page/執業律師.md" title="wikilink">執業律師</a>(1943年9月)</li>
<li><a href="../Page/臺南市.md" title="wikilink">臺南市南區</a><a href="../Page/區長.md" title="wikilink">區長</a>(1945年11月)</li>
<li><a href="../Page/臺灣省參議會.md" title="wikilink">臺灣省參議會</a><a href="../Page/參議員.md" title="wikilink">參議員選舉</a>，被列為候補參議員(1946年4月)</li>
<li>台南律師公會人民自由保障委員會主任委員(1946年)</li>
<li><a href="../Page/二二八事件處理委員會.md" title="wikilink">二二八事件處理委員會台南分會治安組長</a>(1947年)</li>
</ul>
<p></span></p>
</div></td>
</tr>
</tbody>
</table>

**湯德章**（），[臺灣](../Page/臺灣.md "wikilink")[臺南人](../Page/臺南市.md "wikilink")。父親為[日本東京人](../Page/東京.md "wikilink")，母親為[臺南南化人](../Page/南化區.md "wikilink")。湯德章[過繼母系](../Page/過繼母系.md "wikilink")，故[籍貫為臺灣臺南](../Page/籍貫.md "wikilink")。[柔道五段](../Page/柔道.md "wikilink")，任職[警官](../Page/警官.md "wikilink")、[執業律師](../Page/執業律師.md "wikilink")，於[二二八事件中遭](../Page/二二八事件.md "wikilink")[國軍槍決於](../Page/國民政府軍.md "wikilink")[民生綠園](../Page/民生綠園.md "wikilink")（即現今的湯德章紀念公園）。

## 家世

湯德章之父親是[日本東京人](../Page/東京.md "wikilink")，本名「坂井德藏」，因[過繼到](../Page/過繼.md "wikilink")「新居」家，因而改為「新居德藏」。新居德藏為奉派來臺的[警察](../Page/警察.md "wikilink")，任職噍吧哖支廳南庄派出所巡查，1915年[噍吧哖事件](../Page/噍吧哖事件.md "wikilink")（[西來庵事件](../Page/西來庵事件.md "wikilink")）中於[南化殉職](../Page/南化區.md "wikilink")。事變當時，湯德章與姐姐湯柳亦在派出所內，得工友黃木貴背負救出，倖免於難。

湯德章之母親「湯玉」為[臺南南化農家女子](../Page/南化區.md "wikilink")，替人[縫紉為生](../Page/針黹.md "wikilink")。湯德章[過繼母系](../Page/抽豬母稅.md "wikilink")，故從母姓「湯」，湯德章有一姐一弟，都從母姓「湯」。

## 生平

### 童年

湯德章幼年非常[貧困](../Page/貧困.md "wikilink")，得到善心的[漢方醫師楊泉的資助](../Page/漢方.md "wikilink")，勉強度日。後在[噍吧哖](../Page/玉井區.md "wikilink")[公學校](../Page/公學校.md "wikilink")（今[玉井國小](../Page/玉井區#教育.md "wikilink")）畢業，考進[臺南師範學校](../Page/臺南大學.md "wikilink")（今[國立臺南大學前身](../Page/國立臺南大學.md "wikilink")），因父親離世，家境貧寒，輟學返家務農，又在糖廠打工。一面在鄉下[私塾學習](../Page/私塾.md "wikilink")[漢文](../Page/漢文.md "wikilink")、[四書五經](../Page/四書五經.md "wikilink")，一面學習[中國武術](../Page/中國武術.md "wikilink")[少林拳與日本武術](../Page/少林拳.md "wikilink")，造就其文武皆備的能力。當時湯德章曾因替被強取[保護費的理髮店老闆](../Page/保護費.md "wikilink")「老健仔」出面與流氓談判，以寡擊眾取得勝利，一舉成為捍衛善良鄉民的[俠士](../Page/俠士.md "wikilink")。之後湯考入[臺北警察練習所](../Page/臺北.md "wikilink")，但被發覺年齡不足，因成績優異破格錄取。根據湯德章就讀之噍吧哖公學校（今玉井國小）六年級級任老師林心在其著作《六五回憶錄》的回憶：「湯德章為人勤勉誠實，做事有魄力、負責，有點不修邊幅，但有抑強扶弱之氣概。」楊泉醫師的孫子楊顯堂描述，湯德章「體格魁梧，好打抱不平，看似吊兒郎當，未認真讀書，卻是絕頂聰明，一舉考進台南師範。」

### 擔任警官

1927年任[臺南州](../Page/臺南州.md "wikilink")[巡查](../Page/巡查.md "wikilink")，分發至[東石郡](../Page/東石郡.md "wikilink")，因查戶口而認識當地女子陳爁，於1928年結婚。1929年7月30日，《[臺灣日日新報](../Page/臺灣日日新報.md "wikilink")》報導湯德章逮捕重大逃犯，湯一舉成為地方名人。因1929年普通文官考試及格，於1930年升任臺南開山[派出所次席巡查](../Page/派出所.md "wikilink")。1934年任[臺南州](../Page/臺南州.md "wikilink")[警部補](../Page/警部補.md "wikilink")。而後轉任為[新豐郡保安衛生系](../Page/新豐郡.md "wikilink")。1938年[收養其姊的五男湯聰模為子](../Page/收養.md "wikilink")。

由於湯德章並不是純日本人，他的日本同學此時已經升至課長職位，而湯又特別照顧臺灣人，因此時常受到上級的歧視與壓力，使湯非常不滿。1939年當時發生一名日本[醫師鹿沼正雄](../Page/醫師.md "wikilink")（臺南一家醫院院長，與[臺南州知事是同學](../Page/臺南州知事.md "wikilink")，父親是全臺南知名的タグシー[株式會社](../Page/株式會社.md "wikilink")[社長兼](../Page/社長.md "wikilink")[臺南州](../Page/臺南州.md "wikilink")[州協議會員](../Page/州協議會.md "wikilink")），駕自用轎車撞死臺灣人莊正興，日本警察無人敢辦此案，身為警官的湯德章力主追究，引發上級不滿，加上日本人將湯德章視為臺灣[本籍人士](../Page/本島人.md "wikilink")，並且對他頗有[歧視](../Page/種族歧視.md "wikilink")，湯憤而離職，脫離警界。

### 赴日深造與返臺開業

[湯德章故居.jpg](https://zh.wikipedia.org/wiki/File:湯德章故居.jpg "fig:湯德章故居.jpg")
湯德章於1939年辭去警官職務後，回到父親的故鄉，投靠東京的叔父「坂井又藏」，成為其[螟蛉子](../Page/收養.md "wikilink")，並改名「坂井德章」，並赴東京[中央大學深造](../Page/中央大學_\(日本\).md "wikilink")。1942年10月28日日本高等文官考試司法科及格，《[臺灣日日新報](../Page/臺灣日日新報.md "wikilink")》專文報導湯德章於日本當地奮鬥有成通過考試的消息，轟動臺南。1943年日本高等文官考試行政科及格，臺灣的報紙又登出湯德章金榜題名的消息。此時湯德章沒有接受日本親友的建議留在日本發展，反而決定返回臺灣。返臺之後，認為自己不應意氣用事，應認同自己為臺灣人，於是又改回[湯姓](../Page/湯姓.md "wikilink")\[1\]\[2\]。1943年9月[臺灣總督府登錄為辯護士](../Page/臺灣總督府.md "wikilink")（律師），在臺南[南門町定居](../Page/南門町_\(臺南市\).md "wikilink")，並於[末廣町開業](../Page/末廣町_\(臺南市\).md "wikilink")，成為臺南地區知名律師，常酌收廉價的訴訟費用，或義務辯護，幫助法律知識不足的臺灣人伸張正義，抵抗劣質[日本人的欺凌](../Page/日本人.md "wikilink")。

### 國民政府時代

1945年8月，[二次大戰結束](../Page/二次大戰.md "wikilink")，[日本投降](../Page/日本投降.md "wikilink")，中國[國民政府接管臺灣](../Page/國民政府.md "wikilink")，[臺灣省行政長官](../Page/臺灣省行政長官.md "wikilink")[陳儀親自致函力邀湯德章擔任](../Page/陳儀.md "wikilink")「臺灣省公務員訓練所」所長，湯德章深知官場[貪污成習不願就任](../Page/貪污.md "wikilink")，曾說：「當中國官，在心理上要做貪污的準備，我不願埋沒自己的良心。」1945年11月選任[臺南市南區](../Page/臺南市.md "wikilink")[區長](../Page/區長.md "wikilink")。1946年5月起，臺南[霍亂大流行](../Page/霍亂.md "wikilink")（短短三個月臺南963人染病、死亡338人），湯德章要求衛生當局立刻施行消毒、隔離、注射預防針，但中國來臺官員不為所動，湯德章表示抗議憤而辭去區長職務。1946年臺灣省參議會參議員選舉中，被列為候補省參議員。此時的湯德章僅擔任執業律師，以及「台南律師公會人民自由保障委員會」主任委員。湯德章並積極參與民間人民自由保障的推廣活動，以「人權律師」的身分主張[社會公義](../Page/社會公義.md "wikilink")、宣揚[民主思想](../Page/民主.md "wikilink")，為[社會改革奔走](../Page/社會改革.md "wikilink")，得到許多民眾的愛戴。

## 二二八事件

[1947-3-9臺南市選出過渡時期民選市長候選人_Newspaper_concerning_aftermath_(Residents_of_Tainan_elected_3_Mayoral_Candidates)_of_the_228_Incident_of_TAIWAN.jpeg](https://zh.wikipedia.org/wiki/File:1947-3-9臺南市選出過渡時期民選市長候選人_Newspaper_concerning_aftermath_\(Residents_of_Tainan_elected_3_Mayoral_Candidates\)_of_the_228_Incident_of_TAIWAN.jpeg "fig:1947-3-9臺南市選出過渡時期民選市長候選人_Newspaper_concerning_aftermath_(Residents_of_Tainan_elected_3_Mayoral_Candidates)_of_the_228_Incident_of_TAIWAN.jpeg")
[1947年3月13日臺灣臺南市長候選人湯德章律師遭蔣中正與陳儀所屬非法殺害_Lawyer_and_People-elected_Mayoral-candidate_of_Tainan_murdered_by_Chinese_military.jpeg](https://zh.wikipedia.org/wiki/File:1947年3月13日臺灣臺南市長候選人湯德章律師遭蔣中正與陳儀所屬非法殺害_Lawyer_and_People-elected_Mayoral-candidate_of_Tainan_murdered_by_Chinese_military.jpeg "fig:1947年3月13日臺灣臺南市長候選人湯德章律師遭蔣中正與陳儀所屬非法殺害_Lawyer_and_People-elected_Mayoral-candidate_of_Tainan_murdered_by_Chinese_military.jpeg")所屬[中華日報指控湯德章為暴徒](../Page/中華日報.md "wikilink")，並執行槍決。\]\]

### 擔任處委會台南分會治安組長

1947年[二二八事件發生](../Page/二二八事件.md "wikilink")，此時湯德章正身染[瘧疾臥病在床](../Page/瘧疾.md "wikilink")，仍被推任[二二八事件處理委員會臺南市分會治安組組長](../Page/二二八事件處理委員會列表.md "wikilink")，維持該市治安。3月9日下午，台南市全體參議員、區里長、人民團體、及學生代表等集會，依[陳儀所宣稱之諾言](../Page/陳儀.md "wikilink")，推舉7月1日舉行縣市長民選前過渡期間之市長，結果[黃百祿](../Page/黃百祿.md "wikilink")、[侯全成](../Page/侯全成.md "wikilink")、湯德章當選為「市長候選人」。

### 遭國民政府軍隊捕殺

以平亂名義進入臺灣的國軍21師殺入臺南，隨即以[叛亂罪名](../Page/叛國.md "wikilink")，於3月11日派出二、三十名憲警特務闖進住所逮捕湯德章。湯德章憑藉壯碩的身材與高超的[武藝對抗](../Page/武藝.md "wikilink")，一方面徒手以[柔道拒捕](../Page/柔道.md "wikilink")，另一方面爭取時間將參與臺南市治安工作的臺籍菁英及學生名單焚毀，挽救數千名知識分子免遭橫禍。

3月12日，由於國軍嚴刑逼供要求名單，湯德章被懸吊[刑求一夜](../Page/刑求.md "wikilink")，[肋骨被槍托打斷](../Page/肋骨.md "wikilink")\[3\]
，並夾手指造成手指腫脹（當時在隔壁牢房[楊熾昌的口述](../Page/楊熾昌.md "wikilink")）\[4\]，遭受酷刑後反綁雙腕，背插書寫姓名之木牌，綁在[卡車上遊行臺南市區](../Page/卡車.md "wikilink")\[5\]。3月13日，湯德章被控[叛亂罪名](../Page/叛亂.md "wikilink")，押赴[民生綠園公開](../Page/民生綠園.md "wikilink")[槍決](../Page/槍決.md "wikilink")，湯仍神情自若，並向四周市民微笑示意\[6\]。臨刑時不斷遭受幾名士兵踹踢，並強逼下跪，湯德章向士兵破口大罵，並堅拒下跪，士兵踹湯德章的腳，隨即開槍\[7\]\[8\]，子彈貫穿湯的鼻梁及前額，鮮血腦漿噴灑一地，湯之雙眼怒目圓睜，傲立不動良久，過些時才倒下\[9\]，雙眼依然圓睜未闔，死狀悽慘\[10\]。由於湯德章久為臺南市民所尊崇景仰\[11\]\[12\]，現場圍觀群眾見湯遭此不公平待遇內心悲憤交加，紛紛落淚並當場傳出哭泣聲\[13\]\[14\]。湯德章遭到槍決之後，國軍不讓湯德章[親人收屍](../Page/親人.md "wikilink")，逼其曝屍公園，經一再哀求，才准許以毛毯覆屍，但仍不准移動，置於地面曝屍三日\[15\]，而湯的妻子守候在屍體旁邊哭泣。\[16\]

在湯德章行刑現場親眼目睹者包含[奇美創辦人](../Page/奇美.md "wikilink")[許文龍](../Page/許文龍.md "wikilink")\[17\]。根據在場臺南市民陳開元證言：「3月11日，由[高雄開來的國民黨軍一到](../Page/高雄.md "wikilink")[台南](../Page/台南.md "wikilink")，就下令[戒嚴](../Page/戒嚴.md "wikilink")，展開瘋狂的捕殺。首先被槍殺的，是經由各界人士選舉出來的市長候選人之一湯德章。3月13日中午，湯德章被五花大綁押上卡車遊街示眾。從[本町向](../Page/本町.md "wikilink")[大正公園開來](../Page/大正公園.md "wikilink")...湯德章雖然被五花大綁，背上插著牌子，但他面不改色，昂首怒視著劊子手們，下車之後[劊子手要他跪下](../Page/劊子手.md "wikilink")，但他不但不屈服，還破口大罵蔣賊軍。我是有生以來第一次看見這種公開殺人的，印象特別深刻。」\[18\]

台灣流行音樂家[郭一男表示](../Page/郭一男.md "wikilink")，當時他當小販在[民權路賣](../Page/民權路_\(臺南市\).md "wikilink")[茯苓糕維生](../Page/茯苓#藥用.md "wikilink")。聽到街邊有人說湯德章被押往民生綠園，跟過去後看到湯德章的屍體，胸口滿是血與沙土，被放在[擔架上](../Page/擔架.md "wikilink")。國民黨士兵對湯德章的臉吐口水，罵：「[他媽的](../Page/髒話.md "wikilink")！」並用腳踢，褻瀆屍體。郭一男表示由於台灣人最重視死者，這種做法不但前所未聞，也給他很大的衝擊。\[19\]

## 評價

[行政院](../Page/行政院.md "wikilink")《二二八事件研究報告》指出：「湯德章之死，臺南市民同聲喊冤」、「他背負起全臺南動亂的責任，是替臺南市民[贖罪的羔羊](../Page/替罪羊.md "wikilink")」\[20\]；臺南市文獻委員謝碧連律師表示：「湯律師是其前輩，傳奇一生令人景仰，實為後輩楷模。」

## 紀念

為了彰顯湯德章於[二二八事件中維持治安的功績](../Page/二二八事件.md "wikilink")，與紀念他無辜受害的慘況，1998年，[臺南市政府特將湯殉難之處](../Page/臺南市政府.md "wikilink")「民生綠園」，改名為[湯德章紀念公園](../Page/湯德章紀念公園.md "wikilink")，簡稱「湯德章公園」，以資紀念。湯德章的半身銅像由雕塑名師邱火松製作，銅像基座鐫刻紀念碑文。現今湯德章銅像所在位置，正是湯被[槍決之地](../Page/槍決.md "wikilink")。

2013年2月28日[二二八紀念日中](../Page/二二八紀念日.md "wikilink")，針對各界要求訂定「湯德章紀念日」及移除湯德章公園內[孫文銅像的訴求](../Page/孫文.md "wikilink")，[臺南市長](../Page/臺南市長.md "wikilink")[賴清德表示市府將研議訂定湯德章紀念日](../Page/賴清德.md "wikilink")。
\[21\]2014年賴清德宣布，每年3月13日為「台南市正義與勇氣紀念日」，以紀念湯律師英勇堅韌的一生所表彰的台灣精神典範。

2015年3月13日，臺南市政府在湯德章故居舉行「歷史名人故居」的掛牌儀式\[22\]。該房子在1968年被湯德章之子湯聰模賣給王榮彬\[23\]。

## 参考文献

## 研究書目

  - 著，林琪禎、張弈伶、李雨青譯：《湯德章：不該被遺忘的正義與勇氣》（台北：玉山社，2016）

## 外部連結

  - [《二二八消失的台灣菁英》—湯德章（1907-1947）](http://taiwantt.org.tw/taiwanspirit/frame/frame28.htm)
  - [一封給外省太太朋友的公開信—讓我們從一個英雄故事來談二二八，陳嘉君，上報，2019年03月13日](https://www.upmedia.mg/news_info.php?SerialNo=59123)（包含湯德章照片集）

[Category:二二八事件受難者](../Category/二二八事件受難者.md "wikilink")
[Category:被台灣槍決者](../Category/被台灣槍決者.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[Category:國立臺南大學校友](../Category/國立臺南大學校友.md "wikilink")
[Category:日本裔台灣人](../Category/日本裔台灣人.md "wikilink")
[Category:日本中央大學校友](../Category/日本中央大學校友.md "wikilink")
[Category:臺灣日治時期警務人員](../Category/臺灣日治時期警務人員.md "wikilink")
[Category:台灣省參議員](../Category/台灣省參議員.md "wikilink")
[Category:台灣律師](../Category/台灣律師.md "wikilink")
[Category:湯姓](../Category/湯姓.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:台裔混血兒](../Category/台裔混血兒.md "wikilink")

1.  [臺灣百大人物
    湯德章](http://www.taiwancon.com/190747/%E5%8F%B0%E7%81%A3%E7%99%BE%E5%A4%A7%E4%BA%BA%E7%89%A9-%E6%B9%AF%E5%BE%B7%E7%AB%A0%EF%BC%88%E9%98%AA%E4%BA%95%E5%BE%B7%E7%AB%A0%EF%BC%89.htm)

2.  [台南民眾的憤怒是有原因的](http://www.appledaily.com.tw/realtimenews/article/new/20140223/349499/)，鄧鴻源，蘋果日報，2014-02-23

3.  [《二二八消失的台灣菁英》—
    湯德章（1907-1947）](http://taiwantt.org.tw/taiwanspirit/frame/frame28.htm)，李筱峰，自立晚報社文化出版部，1990-01-01

4.  [口述歷史(三)，第3卷](https://books.google.com.tw/books?id=DLa4CgAAQBAJ&pg=PA136&lpg=PA136)，第136頁，張玉法，中央研究院近代史研究所，2015-08-31

5.
6.
7.  [第三章創傷的紀念性](http://mocfile.moc.gov.tw/bochhistory/91e95645-4c6d-4d96-bc35-882c8c26fce3.pdf)，具變動力量的集體性文化資產：論標注性事件紀念物之紀念性意涵，第97頁，林蕙玟，文化部網站，2008

8.  [【犧牲小我 完成大我】228 死難者 ：湯德章
    (二)](http://www.taiwancon.com/25649/%E8%A8%98%E5%8F%96%E6%AD%B7%E5%8F%B2%E7%9A%84%E6%95%99%E8%A8%93%EF%BC%8C%E7%82%BA%E5%8F%B0%E7%81%A3%EF%BC%8C%E4%B8%96%E7%95%8C%E5%92%8C%E5%B9%B3%E4%BE%86%E7%A5%88%E7%A6%8F.htm)，李谷，TAIWANCON台灣控

9.
10. \[<http://www.pct.org.tw/article_soc.aspx?strBlockID=B00007&strContentID=C2016022200028&strDesc=&strSiteID=&strCTID=CT0002&strASP=article_soc不滅的烙記>:
    張大邦的故事\]，林家鴻，<台灣教會公報>第3287期

11.
12. [緬懷湯德章賴市長：湯德章律師樹立台灣精神典範](http://www.shinying.gov.tw/tainan/pdapage2.asp?id=%7B3E929D29-4901-4738-B48A-78444B2E56DA%7D)，臺南市政府，2014-03-13

13. [《湯德章—不該被遺忘的正義與勇氣》新書發表會](https://www.taiwanus.net/news/press/2017/201701070642521483.htm)，李筱峰教授，主辦：玉山社出版公司，協辦：台南市政府文化局，2017-01-06

14.
15.
16. [說古論今，談情說愛](http://journal.nmtl.gov.tw/opencms/nmtl_search/download.html?atype=B&ctype=F&volume=39&id=2013060039070075)，王美珍，國立台灣文學館，2013-03-24

17. [許文龍：歷史能原諒但不能忘](http://www.chinatimes.com/newspapers/20150314000490-260107)，曹婷婷╱台南報導，中時電子報，2015-03-14

18. 《證言2.28》，葉芸芸主編，台北，人間出版社出版，1993，頁162-165

19. [走過228
    還活著的老警備隊長：「機關槍掃射無辜！足可惡ㄟ！」](https://www.nextmag.com.tw/realtimenews/news/250994)，撰文:傅紀鋼、攝影:李宗明
    張文玠，壹週刊，2017-02-28

20. 《二二八事件研究報告》，行政院研究二二八事件小組，賴澤涵總主筆，臺灣臺北:時報文化出版企業股份有限公司，1994-02-20，頁326

21. [《孫文銅像移置》台南市府將研訂
    湯德章紀念日](http://www.libertytimes.com.tw/2013/new/mar/1/today-south9.htm?Slots=TPhoto)
    ，記者蔡文居、黃文鍠／台南報導，自由時報，2013-03-01

22.

23.