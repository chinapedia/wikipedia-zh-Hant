**溫哥華國際機場**（；，）是一座位於[加拿大](../Page/加拿大.md "wikilink")[卑詩省](../Page/卑詩.md "wikilink")[列治文](../Page/列治文.md "wikilink")[海島的國際機場](../Page/海島_\(卑詩省\).md "wikilink")，距離[溫哥華](../Page/溫哥華.md "wikilink")[市中心約](../Page/溫哥華市中心.md "wikilink")15公里。以2011年的起降架次（338,073架次\[1\]）和旅客量（2593萬人次\[2\]）計算，溫哥華國際機場是全加拿大第二繁忙的機場，僅次於[多倫多皮爾遜國際機場](../Page/多倫多皮爾遜國際機場.md "wikilink")。機場提供不停站航班往返[亞洲](../Page/亞洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[大洋洲](../Page/大洋洲.md "wikilink")、[美國](../Page/美國.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[加勒比地區和加拿大國內其它地點](../Page/加勒比地區.md "wikilink")。機場曾獲[Skytrax評為](../Page/Skytrax.md "wikilink")2007年和2010年全北美洲最佳機場\[3\]\[4\]\[5\]。[加拿大航空](../Page/加拿大航空.md "wikilink")、[爵士航空和](../Page/爵士航空.md "wikilink")[加拿大越洋航空以此為](../Page/加拿大越洋航空.md "wikilink")[樞紐機場](../Page/樞紐機場.md "wikilink")，而[西捷航空則以此為](../Page/西捷航空.md "wikilink")[重點機場](../Page/重點機場.md "wikilink")。這機場也是全加拿大八個設有[美國境外入境審查設施的機場之一](../Page/美國境外入境審查.md "wikilink")。

溫哥華國際機場是由[加拿大交通部擁有](../Page/加拿大交通部.md "wikilink")\[6\]，營運單位則是溫哥華國際機場管理局\[7\]。

## 歷史

溫哥華地區的首個機場為設於列治文[魯魯島明納努公園的草地升降場](../Page/魯魯島.md "wikilink")，於1912年處理了其首班載客航班。然而，[查爾斯·林白於](../Page/查爾斯·林白.md "wikilink")1927年進行北美巡迴訪問時認為此機場的設備有欠妥善，因此拒絕到訪。兩年後，溫哥華市政府在海島購地約480英畝來興建新機場，以取代位於魯魯島的機場\[8\]。跑道、行政大樓和機庫工程於1930年4月展開，而溫哥華市立機場（）則於1931年7月22日正式開幕\[9\]。

[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，機場和航站樓（即現時的南航站樓）被聯邦政府租借，並由[國防部和](../Page/加拿大國防部.md "wikilink")[交通部營運](../Page/加拿大交通部.md "wikilink")\[10\]。機場遂成為[皇家加拿大空軍的訓練基地](../Page/皇家加拿大空軍.md "wikilink")，而海島的東南角則被開發成一個名為柏加衛（）的住宅區，以便安置機場人員及其親屬\[11\]。戰後，機場的操控權於1947年歸還溫哥華市政府，並正式改稱溫哥華國際機場\[12\]。一條長達3公里的東西向跑道於1953年落成，而機場則於1962年再度轉交聯邦政府\[13\]。現在的主航站樓於1968年完工\[14\]，其後逐步擴展成現在國內和國際航班各設獨立航站樓的局面，而北跑道則於1996年完工。

## 門戶

因為這機場在全加拿大機場中最近亞洲，所以機場便成為了來往加拿大和亞洲的門戶。在航班次數中，穿越太平洋的數目多於往來其他加拿大機場的數目。相當數量的[亞裔加拿大人都住在大溫哥華地區](../Page/亞裔加拿大人.md "wikilink")，他们推动了温哥华机场越太平洋航班的上升。

幾個主要外國的國家航空公司現在協商開設航線來往溫哥華國際機場，成事的話將進一步鞏固溫哥華國際機場的北美洲門戶地位。

## 航站樓

溫哥華國際機場設有三個[航站樓](../Page/航站樓.md "wikilink")：

:\***國內航站樓**（1968年完工）

:\***國際航站樓**（包括美國境外入境審查設施）

:\***南航站樓**。

國內航站樓和國際航站樓基本上是一座相連但被分成兩截的大型建築物，但南航站樓則是一座獨立建築，並位於機場較偏遠的位置，主要服務卑詩省以內的地區航線。大部分前往美國的航班都會使用國際航站樓內的美國境外入境審查設施。

## 航空公司和目的地

[Vancouver-yvr-terminal.id.jpg](https://zh.wikipedia.org/wiki/File:Vancouver-yvr-terminal.id.jpg "fig:Vancouver-yvr-terminal.id.jpg")
[Trilingual_signage_at_YVR.jpg](https://zh.wikipedia.org/wiki/File:Trilingual_signage_at_YVR.jpg "fig:Trilingual_signage_at_YVR.jpg")
[YVR_International_arrivals_Hall_2018.jpg](https://zh.wikipedia.org/wiki/File:YVR_International_arrivals_Hall_2018.jpg "fig:YVR_International_arrivals_Hall_2018.jpg")
[YVR_International_Terminal_2018.jpg](https://zh.wikipedia.org/wiki/File:YVR_International_Terminal_2018.jpg "fig:YVR_International_Terminal_2018.jpg")
[Yvr-intl-term.jpg](https://zh.wikipedia.org/wiki/File:Yvr-intl-term.jpg "fig:Yvr-intl-term.jpg")
[YVRsalmon-statue.jpg](https://zh.wikipedia.org/wiki/File:YVRsalmon-statue.jpg "fig:YVRsalmon-statue.jpg")
[Vancouver_Art.jpg](https://zh.wikipedia.org/wiki/File:Vancouver_Art.jpg "fig:Vancouver_Art.jpg")
[Vancouver_Airport_Skytrain_Station_2008-04-22.JPG](https://zh.wikipedia.org/wiki/File:Vancouver_Airport_Skytrain_Station_2008-04-22.JPG "fig:Vancouver_Airport_Skytrain_Station_2008-04-22.JPG")
[N606AA-2008-09-13-YVR.jpg](https://zh.wikipedia.org/wiki/File:N606AA-2008-09-13-YVR.jpg "fig:N606AA-2008-09-13-YVR.jpg")[波音757降落中](../Page/波音757.md "wikilink")\]\]
[YVR_winter_take_off.jpg](https://zh.wikipedia.org/wiki/File:YVR_winter_take_off.jpg "fig:YVR_winter_take_off.jpg")的一架貨機在離陸中\]\]
[Vancouver_International_Airport_2006.jpg](https://zh.wikipedia.org/wiki/File:Vancouver_International_Airport_2006.jpg "fig:Vancouver_International_Airport_2006.jpg")

### 貨運航空公司

  - [加拿大航空貨運](../Page/加拿大航空.md "wikilink")
  - [AirPac航空](../Page/AirPac航空.md "wikilink")\[15\]
  - [Ameriflight](../Page/Ameriflight.md "wikilink")
  - [ABX航空](../Page/ABX航空.md "wikilink")
  - [全球國際貨運](../Page/全球國際貨運.md "wikilink")
  - [貨捷航空](../Page/貨捷航空.md "wikilink")
  - [國泰航空貨運](../Page/國泰航空.md "wikilink")
  - [中國南方航空貨運](../Page/中國南方航空.md "wikilink")
  - [DHL](../Page/DHL.md "wikilink")
  - [帝國航空](../Page/帝國航空.md "wikilink")
  - [聯邦快遞](../Page/聯邦快遞.md "wikilink")
  - [基隆拿空中包機](../Page/基隆拿空中包機.md "wikilink")
  - [晨星空中快運](../Page/晨星空中快運.md "wikilink")
  - [Nolinor航空](../Page/Nolinor航空.md "wikilink")
  - [Purolator快遞](../Page/Purolator快遞.md "wikilink")
  - [UPS航空](../Page/UPS航空.md "wikilink")
  - [伏爾加-第聶伯航空](../Page/伏爾加-第聶伯航空.md "wikilink")（季節性）

[Air_routes_from_YVR.PNG](https://zh.wikipedia.org/wiki/File:Air_routes_from_YVR.PNG "fig:Air_routes_from_YVR.PNG")

## 交通配套

[Canada_Line_Skytrain_Cars-2008-04-22.JPG](https://zh.wikipedia.org/wiki/File:Canada_Line_Skytrain_Cars-2008-04-22.JPG "fig:Canada_Line_Skytrain_Cars-2008-04-22.JPG")

溫哥華國際機場是全加拿大第一個提供[對外捷運服務的機場](../Page/機場聯絡軌道系統.md "wikilink")
。[運輸聯線旗下的](../Page/運輸聯線.md "wikilink")[溫哥華架空列車](../Page/溫哥華架空列車.md "wikilink")[加拿大線於](../Page/加拿大線.md "wikilink")2009年8月17日通車，來往列治文和[溫哥華市中心](../Page/溫哥華市中心.md "wikilink")，其中一條支線從[魯魯島上的](../Page/魯魯島.md "wikilink")[橋港站起](../Page/橋港站.md "wikilink")，往西經[天普頓站和](../Page/天普頓站.md "wikilink")[海島中心站延至](../Page/海島中心站.md "wikilink")[溫哥華國際機場站](../Page/溫哥華國際機場站.md "wikilink")。連接機場和溫哥華市中心[濱海站的列車日間](../Page/濱海站_\(溫哥華\).md "wikilink")[班距為每](../Page/班距.md "wikilink")7.5分鐘一班，全程需時約25分鐘。加拿大線的機場支線由溫哥華國際機場管理局斥資3億[加元興建](../Page/加拿大元.md "wikilink")\[16\]。

此外，運輸聯線亦有營運N10號深宵巴士線，在加拿大線深宵休車期間行駛與該線相若的路線。

## 參考文獻

## 外部链接

  - [溫哥華國際機場](http://www.yvr.ca/) /

[V](../Category/加拿大机场.md "wikilink")
[溫哥華國際機場](../Category/溫哥華國際機場.md "wikilink")
[Category:美國境外入境審查機場](../Category/美國境外入境審查機場.md "wikilink")
[Category:1931年启用的机场](../Category/1931年启用的机场.md "wikilink")

1.
2.
3.

4.

5.

6.

7.

8.

9.
10.
11.
12.
13.
14.
15.

16.