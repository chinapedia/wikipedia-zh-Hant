**溴苯那敏**()是一種用於過敏症狀的[藥物](../Page/藥物.md "wikilink")，屬於第一代[组胺H1受体拮抗剂](../Page/组胺H1受体拮抗剂.md "wikilink")（又稱[抗組織胺藥](../Page/抗組織胺藥.md "wikilink")），[化学式C](../Page/化学式.md "wikilink")<sub>16</sub>H<sub>19</sub>BrN<sub>2</sub>。其鹽類**馬來酸溴苯那敏**(Brompheniramine
maleate)常用於藥品中。溴苯那敏的用途及不良反應與同類藥物[氯苯那敏相似](../Page/氯苯那敏.md "wikilink")。

## 適應症及用量

減輕過敏症狀，例如[過敏性鼻炎](../Page/過敏性鼻炎.md "wikilink")（包括花粉症）、[荨麻疹等](../Page/荨麻疹.md "wikilink")。溴苯那敏可減輕流鼻涕及噴嚏。用於口服時，成人劑量通常是每4至6小時服4毫克。

## 不良反應

主要是容易令人產生睡意，其它比較常見的不良反應有頭痛、[小便滯留](../Page/小便.md "wikilink")、口乾、腸胃不適等。

## 外部連結

  - [MedlinePlus Drug Information:
    Brompheniramine](http://www.nlm.nih.gov/medlineplus/druginfo/medmaster/a682545.html)（英文）
  - [Rx-List.com -
    Brompheniramine](http://www.rxlist.com/drugs/drug-55309-Brompheniramine+Oral.aspx?drugid=55309&drugname=Brompheniramine+Oral)
  - [Drugs.com -
    Brompheniramine](http://www.drugs.com/MTM/brompheniramine.html)

[Category:吡啶](../Category/吡啶.md "wikilink")
[Category:有机溴化合物](../Category/有机溴化合物.md "wikilink")
[Category:H1受体拮抗剂](../Category/H1受体拮抗剂.md "wikilink")
[Category:阿斯利康制药](../Category/阿斯利康制药.md "wikilink")