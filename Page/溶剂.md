[GBLjugs.jpg](https://zh.wikipedia.org/wiki/File:GBLjugs.jpg "fig:GBLjugs.jpg")
**溶剂**是一种可以[溶解固体](../Page/溶解.md "wikilink")，液体或气体[溶质的液体](../Page/溶质.md "wikilink")，继而成为[溶液](../Page/溶液.md "wikilink")。在日常生活中最普遍的溶剂是[水](../Page/水.md "wikilink")。而所谓**[有机溶剂](../Page/有机溶剂.md "wikilink")**即是包含[碳原子的](../Page/碳.md "wikilink")[有机化合物溶剂](../Page/有机化合物.md "wikilink")。溶剂通常拥有比较低的[沸点和容易挥发](../Page/沸点.md "wikilink")。或是可以由[蒸馏来去除](../Page/蒸馏.md "wikilink")，从而留下被溶物。因此，溶剂不可以对溶质产生[化学反应](../Page/化学反应.md "wikilink")。它们必须为低[活性的](../Page/活性.md "wikilink")。溶剂可从混合物[萃取](../Page/萃取.md "wikilink")[可溶化合物](../Page/可溶.md "wikilink")，最普遍的例子是以热水冲泡[咖啡或](../Page/咖啡.md "wikilink")[茶](../Page/茶.md "wikilink")。溶剂通常是透明，无色的液体，他们大多都有独特的气味。

溶液的[浓度取决于溶解在溶剂内的物质的多少](../Page/浓度.md "wikilink")。**[溶解度](../Page/溶解度.md "wikilink")**则是溶剂在特定[温度下](../Page/温度.md "wikilink")，可以溶解最多多少物质。
有机溶剂主要用于[干洗](../Page/干洗.md "wikilink")（例如[四氯乙烯](../Page/四氯乙烯.md "wikilink")），作[涂料稀释剂](../Page/涂料稀释剂.md "wikilink")（例如[甲苯](../Page/甲苯.md "wikilink")、[香蕉水](../Page/香蕉水.md "wikilink")、[松香水](../Page/松香.md "wikilink")、[松节油](../Page/松节油.md "wikilink")），作洗甲水或去除胶水（例如[丙酮](../Page/丙酮.md "wikilink")，[醋酸甲酯](../Page/醋酸甲酯.md "wikilink")，[醋酸乙酯](../Page/醋酸乙酯.md "wikilink")），除锈（例如[己烷](../Page/己烷.md "wikilink")），作洗洁精（[柠檬精](../Page/柠檬精.md "wikilink")），用于[香水](../Page/香水.md "wikilink")（[酒精](../Page/酒精.md "wikilink")）跟用于[化学合成](../Page/化学合成.md "wikilink")。

## 極性，可溶性與可溶混性

溶劑和溶質大致上可分為*極性*（[親水的](../Page/親水的.md "wikilink")）和*非極性*（[疏水的](../Page/疏水的.md "wikilink")）。極性可以透過量度物質的[介電常數或](../Page/介電常數.md "wikilink")[電偶極矩得知](../Page/電偶極矩.md "wikilink")。溶劑的極性决定了它所能溶解的物質以及可以相互混合的其他溶劑或液態物質。根據這個基本原則，極性物質在極性溶劑溶解的最好；非極性物質在非極性的溶劑中溶解的最好：即“相似相溶原理”。像是無機鹽類（如食鹽）或是醣類（如蔗糖）等極性強的物質，僅溶解於極性強的溶劑中，例如水。而像是油或者是臘等強非極性物質僅溶解於十分非極性的有機溶劑中，像是[己烷](../Page/己烷.md "wikilink")。相同地，水與己烷（或是醋與沙拉油）是無法相互混合溶解，儘管經過充分的攪拌後仍然會迅速地分成兩層。

## 質子性與非質子性溶劑

極性溶劑可再細分為極性[質子性溶劑與極性](../Page/質子性溶劑.md "wikilink")[非質子性溶劑](../Page/质子溶剂.md "wikilink")。水（H-O-H）、乙醇（CH<sub>3</sub>-CH<sub>2</sub>-OH）及[醋酸](../Page/醋酸.md "wikilink")（CH<sub>3</sub>-C（=O）OH）是幾種具有代表性的極性質子性溶劑。丙酮（CH<sub>3</sub>-C（=O）-CH<sub>3</sub>）則為一種極性非質子性溶劑。在[化學反應中](../Page/化學反應.md "wikilink")，使用極性質子性溶劑對於[S<sub>N</sub>1](../Page/SN1反應.md "wikilink")[反應機制比較有利](../Page/反應機制.md "wikilink")，而極性非質子性溶劑則對[S<sub>N</sub>2反應機構較有利](../Page/SN2反應.md "wikilink")。

## 沸點

另一個溶劑重要的性質就是沸點，沸點關係到了蒸發的速度。少量的低沸點溶劑，像是乙醚，二氯甲烷或丙酮，在室溫之下幾秒鐘之內就會蒸發掉了。然而高沸點溶劑，像是水或[二甲基亞碸](../Page/二甲基亞碸.md "wikilink")，則需要較高的溫度、氣體的吹拂或真空（或低壓）的環境下，才能快速揮發。

## 密度

多数[有机溶剂的](../Page/有机溶剂.md "wikilink")[密度比水小](../Page/密度.md "wikilink")，因此它们比水更轻，在[分层时处于水的上面](../Page/分层.md "wikilink")。但一些含卤素元素的有机溶剂（如[二氯甲烷](../Page/二氯甲烷.md "wikilink")（CH<sub>2</sub>Cl<sub>2</sub>）、[氯仿](../Page/氯仿.md "wikilink")（CHCl<sub>3</sub>））通常比水更沉，会沉到水底。这在使用[分液漏斗进行水和溶剂的分液时要特别注意](../Page/分液漏斗.md "wikilink")。

## 相互作用

溶劑會和溶質造成許多微弱的分子間作用力而使其溶解。最常見的此種交互作用有相對較弱的[范德瓦耳斯力](../Page/范德瓦耳斯力.md "wikilink")（誘導偶極作用力）、較強的偶極－偶極作用力與更強的[氫鍵](../Page/氫鍵.md "wikilink")（氧－氫或氮－氫鍵的氫與氧或氮原子的交互作用）。

## 安全性

大部分有機溶劑可燃或極易燃燒，視其揮發性而定。一些含氯溶劑如[二氯甲烷及](../Page/二氯甲烷.md "wikilink")[氯仿則為例外](../Page/氯仿.md "wikilink")。有機溶劑的蒸汽和空氣的混合物會爆炸。溶劑蒸汽比空氣重，因此會沈到底部並擴散很長的距離而幾乎不被稀釋。溶劑蒸汽也會在接近空的桶或罐子等容器中形成。

[醚類如](../Page/醚.md "wikilink")[乙醚與](../Page/乙醚.md "wikilink")[四氫呋喃](../Page/四氫呋喃.md "wikilink")（THF）在接觸氧氣及光線時，會形成高爆炸性的[過氧化物](../Page/過氧化物.md "wikilink")。這些過氧化物因為沸點高，會在蒸餾時濃縮。醚類必須在穩定劑如[BHT或](../Page/BHT.md "wikilink")[氫氧化鈉的存在下儲存在封閉容器內](../Page/氫氧化鈉.md "wikilink")，並置於陰暗處。

許多溶劑假如[吸入過多](../Page/吸入.md "wikilink")，會造成突然失去意識。一些溶劑如乙醚和氯仿，曾長期在醫藥上用作[麻醉劑和](../Page/麻醉劑.md "wikilink")[麻醉藥品](../Page/麻醉藥品.md "wikilink")。乙醇則是被廣為使用及濫用的[精神藥物](../Page/精神藥物.md "wikilink")。乙醚、氯仿以及許多其他溶劑（例如汽油或強力膠中的）以[吸食揮發性物質的方式被用作娛樂用途](../Page/吸食揮發性物質.md "wikilink")，然而通常這種吸食行為對健康具有慢性的影響，如[神經毒性或](../Page/神經毒性.md "wikilink")[癌症](../Page/癌症.md "wikilink")。

一些溶劑如氯仿和[苯](../Page/苯.md "wikilink")（[汽油的成分之一](../Page/汽油.md "wikilink")）是[致癌物](../Page/致癌物.md "wikilink")。許多其他溶劑可對內臟如[肝](../Page/肝.md "wikilink")、[腎或](../Page/腎.md "wikilink")[腦造成傷害](../Page/腦.md "wikilink")。[甲醇會造成眼睛的傷害](../Page/甲醇.md "wikilink")，包括永久性失明。

### 一般注意事項

  - 避免在通風不良或沒有[通風櫃的地方產生溶劑蒸汽](../Page/通風櫃.md "wikilink")。
  - 將儲存溶劑的容器蓋緊。
  - 絕不在接近可燃溶劑處使用火焰，應使用電熱來代替。
  - 絕不將可燃溶劑沖入下水道，以免造成爆炸或火災。
  - 避免吸入溶劑蒸汽。
  - 避免以皮膚接觸溶劑--許多溶劑容易經由皮膚吸收。

## 常用溶剂性质表

溶劑由其極性的大小被歸類為非極性、極性非質子溶劑與極性質子溶劑。而其極性是經由[介電常數所得到來](../Page/介電常數.md "wikilink")。下表中[密度大於水的非極性溶劑利用粗體表示](../Page/密度.md "wikilink")。

| 溶剂                                                                        | [化学式](../Page/化学式.md "wikilink")                                                          | [沸点](../Page/沸点.md "wikilink") | [极性](../Page/介电常数.md "wikilink")\* | [密度](../Page/密度.md "wikilink") |
| ------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------ | ---------------------------------- | ------------------------------ |
| **非极性溶剂**                                                                 |                                                                                           |                                |                                    |                                |
| [己烷](../Page/己烷.md "wikilink")                                            | CH<sub>3</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>3</sub> | 69 °C                          | 2.0                                | 0.655 g/ml                     |
| [苯](../Page/苯.md "wikilink")                                              | C<sub>6</sub>H<sub>6</sub>                                                                | 80 °C                          | 2.3                                | 0.879 g/ml                     |
| [甲苯](../Page/甲苯.md "wikilink")                                            | C<sub>6</sub>H<sub>5</sub>-CH<sub>3</sub>                                                 | 111 °C                         | 2.4                                | 0.867 g/ml                     |
| [二乙醚](../Page/二乙醚.md "wikilink")                                          | CH<sub>3</sub>CH<sub>2</sub>-O-CH<sub>2</sub>-CH<sub>3</sub>                              | 35 °C                          | 4.3                                | 0.713 g/ml                     |
| [氯仿](../Page/氯仿.md "wikilink")                                            | CHCl<sub>3</sub>                                                                          | 61 °C                          | 4.8                                | **1.498 g/ml**                 |
| [乙酸乙酯](../Page/乙酸乙酯.md "wikilink")                                        | CH<sub>3</sub>-C(=O)-O-CH<sub>2</sub>-CH<sub>3</sub>                                      | 77 °C                          | 6.0                                | 0.894 g/ml                     |
| [四氢呋喃](../Page/四氢呋喃.md "wikilink")（THF）                                   | <u>/CH<sub>2</sub>-CH<sub>2</sub>-O-CH<sub>2</sub>-CH<sub>2</sub>\\</u>                   | 66 °C                          | 7.5                                | 0.886 g/ml                     |
| [二氯甲烷](../Page/二氯甲烷.md "wikilink")                                        | CH<sub>2</sub>Cl<sub>2</sub>                                                              | 40 °C                          | 9.1                                | **1.326 g/ml**                 |
| **極性非質子性溶劑**                                                              |                                                                                           |                                |                                    |                                |
| [丙酮](../Page/丙酮.md "wikilink")                                            | CH<sub>3</sub>-C(=O)-CH<sub>3</sub>                                                       | 56 °C                          | 21                                 | 0.786 g/ml                     |
| [乙腈](../Page/乙腈.md "wikilink")（MeCN）                                      | CH<sub>3</sub>-C≡N                                                                        | 82 °C                          | 37                                 | 0.786 g/ml                     |
| [二甲基甲醯胺](../Page/二甲基甲醯胺.md "wikilink")（DMF）                               | H-C(=O)N(CH<sub>3</sub>)<sub>2</sub>                                                      | 153 °C                         | 38                                 | 0.944 g/ml                     |
| [二甲基亞碸](../Page/二甲基亞碸.md "wikilink")（DMSO）                                | CH<sub>3</sub>-S(=O)-CH<sub>3</sub>                                                       | 189 °C                         | 47                                 | 1.092 g/ml                     |
| **極性質子性溶劑**                                                               |                                                                                           |                                |                                    |                                |
| [乙酸](../Page/乙酸.md "wikilink")                                            | CH<sub>3</sub>-C(=O)OH                                                                    | 118 °C                         | 6.2                                | 1.049 g/ml                     |
| [*n*-丁醇](../Page/丁醇.md "wikilink")                                        | CH<sub>3</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2</sub>-OH</sub>                      | 118 °C                         | 18                                 | 0.810 g/ml                     |
| [异丙醇](../Page/异丙醇.md "wikilink")                                          | CH<sub>3</sub>-CH(-OH)-CH<sub>3</sub>                                                     | 82 °C                          | 18                                 | 0.785 g/ml                     |
| [*n*-丙醇](../Page/丙醇.md "wikilink")                                        | CH<sub>3</sub>-CH<sub>2</sub>-CH<sub>2</sub>-OH</sub>                                     | 97 °C                          | 20                                 | 0.803 g/ml                     |
| [乙醇](../Page/乙醇.md "wikilink")                                            | CH<sub>3</sub>-CH<sub>2</sub>-OH</sub>                                                    | 79 °C                          | 24                                 | 0.789 g/ml                     |
| [甲醇](../Page/甲醇.md "wikilink")                                            | CH<sub>3</sub>-OH                                                                         | 65 °C                          | 33                                 | 0.791 g/ml                     |
| [甲酸](../Page/甲酸.md "wikilink")                                            | H-C(=O)OH                                                                                 | 100 °C                         | 58                                 | 1.21 g/ml                      |
| [水](../Page/水.md "wikilink")                                              | H-O-H                                                                                     | 100 °C                         | 80                                 | 0.998 g/ml                     |
| \* 查看相对极性，请参看[1](http://www.speckanalytical.co.uk/products/Tips/bps.html) |                                                                                           |                                |                                    |                                |

## 參見

  - [溶液](../Page/溶液.md "wikilink")
  - [濃度](../Page/濃度.md "wikilink")
  - [LogP分配係數](../Page/LogP.md "wikilink")
  - [溶剂列表](../Page/溶剂列表.md "wikilink")

## 外部連結

  - 常用77种溶剂的理化性质简表[2](http://bbs.chem8.org/viewthread.php?tid=28795)
  - [Table and
    text](https://web.archive.org/web/20041113054037/http://www.usm.maine.edu/~newton/Chy251_253/Lectures/Solvents/Solvents.html)
    O-Chem Lecture
  - 有機溶劑性質與毒性[表格](https://web.archive.org/web/20041207190740/http://virtual.yosemite.cc.ca.us/smurov/orgsoltab.htm)

[\*](../Category/溶劑.md "wikilink")