[Rongdong.jpg](https://zh.wikipedia.org/wiki/File:Rongdong.jpg "fig:Rongdong.jpg")
**溶洞**指的是由雨水或地下水溶解侵蚀石灰岩层所形成的空洞。又称**钟乳洞**、**石灰岩洞**。

## 溶洞的形成

[中国湖北境内的一个溶洞入口.jpg](https://zh.wikipedia.org/wiki/File:中国湖北境内的一个溶洞入口.jpg "fig:中国湖北境内的一个溶洞入口.jpg")
溶洞的形成是[石灰岩地區地下水長期](../Page/石灰岩.md "wikilink")[溶蝕的結果](../Page/溶蝕.md "wikilink")。石灰[岩層是先決條件](../Page/岩層.md "wikilink")，石灰岩的主要成分是[碳酸鈣](../Page/碳酸鈣.md "wikilink")，在有[水和](../Page/水.md "wikilink")[二氧化碳時發生化學反應生成碳酸氫鈣](../Page/二氧化碳.md "wikilink")\[CaCO<sub>3</sub>+H<sub>2</sub>O+CO<sub>2</sub>--\>Ca(HCO<sub>3</sub>)<sub>2</sub>\]，後者可溶于水，當這種水在地下深處有一定壓力時，溶解更甚。石灰岩中的鈣被水溶解帶走，經過幾十萬、百萬年甚至上千萬年的沉積鈣化，石灰岩地表就會形成溶溝、溶槽，地下就會形成空洞。當這種含鈣的水，在流動中失去壓力，或成份發生變化，鈣有一部分會以石灰岩的[堆積物形態](../Page/堆積物.md "wikilink")[沉澱下來](../Page/沉澱.md "wikilink")，由於免受自然外力的破壞，便形成了[石鐘乳](../Page/石鐘乳.md "wikilink")、[石筍](../Page/石筍.md "wikilink")、[石柱等](../Page/石柱.md "wikilink")[自然景觀](../Page/自然景觀.md "wikilink")。
由於這種[地理現象在](../Page/地理現象.md "wikilink")[南歐](../Page/南歐.md "wikilink")[亞德利亞海岸的](../Page/亞德利亞海岸.md "wikilink")[喀斯特高原上最為典型](../Page/喀斯特高原.md "wikilink")，所以常把石灰岩地區的這種地形籠統地稱之[喀斯特地形](../Page/喀斯特地形.md "wikilink")。

岩溶，顧名思義，就是岩石受到了水的溶解和侵蝕。自然界中的各種岩石，絕大多數是不可溶的，但也有少數能被水所溶解，如[岩鹽](../Page/岩鹽.md "wikilink")、[石膏和](../Page/石膏.md "wikilink")[黃土](../Page/黃土.md "wikilink")。石灰岩等[碳酸鹽岩](../Page/碳酸鹽岩.md "wikilink")，對於普通水來說幾乎是不可溶的，但是當水中溶解有一定量的二氧化碳以後，就使其對石灰岩的溶解能力提高了幾十倍。不過即使這樣，石灰岩的溶解仍是十分微弱的，是肉眼所難於察覺的。但經歷漫長的地質歲月以後，就可以水滴石穿，在石灰岩地區形成規模極其宏大的各種岩溶地形。如[溶溝](../Page/溶溝.md "wikilink")、[溶槽](../Page/溶槽.md "wikilink")、[溶蝕漏斗](../Page/溶蝕漏斗.md "wikilink")、[溶蝕湖](../Page/溶蝕湖.md "wikilink")、[暗河](../Page/暗河.md "wikilink")、溶洞等等。

溶洞的形成不僅有[溶蝕作用](../Page/溶蝕作用.md "wikilink")，還有[沉析作用](../Page/沉析作用.md "wikilink")。在石灰崖區從地上面流入地下的水，大多已溶解有一定量的碳酸鈣，但當其到達溶洞時由於環境中溫度、壓力的變化，會使水中含有的二氧化碳被釋放出來。於是水對碳酸鈣的溶解力降低，使本來溶解在水中的碳酸鈣結晶析出。此外，滴落到溶洞中的水有時也會因蒸發而使在洞頂的碳酸鈣晶體向下生長，便成為[鐘乳石](../Page/鐘乳石.md "wikilink")；若滴在洞底再[凝結出來](../Page/凝結.md "wikilink")，向上生長便形成石筍；鐘乳石和石筍在生長中逐漸銜接成為一體，就是石柱。當然實際情況要複雜得多。比如，由於滴水的石縫被析出的石鐘乳所堵塞，或者由於[地殼運動](../Page/地殼運動.md "wikilink")，使得地形、水流以及滲水的通道發生了變化，致使水的滴落方向、速度、水量也隨之發生變化，結果，有些才生長到一半的石鐘乳和石筍不再繼續生長了，這樣又在邊上長新的鐘乳石和石筍……這些變化後形成的鐘乳石、石筍和石柱相互交錯、疊接，便構成了令人歎為觀止的各種瑰異的景觀。

鐘乳石、石筍的形成常常需要幾千或幾萬年的時間。有人曾對桂林龍隱岩的鐘乳石作過測定，發現每年平均僅長兩毫米。

## 最大的溶洞

世界上最大的溶洞是北美阿帕拉契山脈的[馬默斯洞穴國家公園](../Page/馬默斯洞穴國家公園.md "wikilink")，位於[肯塔基州境內](../Page/肯塔基州.md "wikilink")，洞深64[公里](../Page/公里.md "wikilink")，所有的岔洞連起來的總長度達250公里。洞裏寬的似[廣場](../Page/廣場.md "wikilink")，窄的似[長廊](../Page/長廊.md "wikilink")，高的有30[米](../Page/米.md "wikilink")，整個洞平面上迂迴曲折，垂直向上可分出三層。[雨季](../Page/雨季.md "wikilink")，整個洞內都有流水，成為地下河流在坡折處河水跌落，形成[瀑布](../Page/瀑布.md "wikilink")；[旱季](../Page/旱季.md "wikilink")，局部地區有水，成地下[湖泊](../Page/湖泊.md "wikilink")，可能還有積水很深的[潭](../Page/潭.md "wikilink")。

## 中国的溶洞

  - [本溪水洞](../Page/本溪水洞.md "wikilink")
    [辽宁](../Page/辽宁.md "wikilink")[本溪](../Page/本溪.md "wikilink")
  - [龙骨洞](../Page/龙骨洞.md "wikilink")
    [北京](../Page/北京.md "wikilink")[周口店](../Page/周口店.md "wikilink")
  - [义龙洞](../Page/义龙洞.md "wikilink")
    [江西](../Page/江西.md "wikilink")[萍乡](../Page/萍乡.md "wikilink")
  - [中峰洞](../Page/中峰洞.md "wikilink")
    [四川](../Page/四川.md "wikilink")[巴中](../Page/巴中.md "wikilink")
  - [鸡冠洞](../Page/鸡冠洞.md "wikilink")
    [河南](../Page/河南.md "wikilink")[栾川](../Page/栾川.md "wikilink")
  - [黄龙洞](../Page/黄龙洞.md "wikilink")
    [湖南](../Page/湖南.md "wikilink")[张家界](../Page/张家界.md "wikilink")
  - [芙蓉洞](../Page/芙蓉洞.md "wikilink")
    [重庆](../Page/重庆.md "wikilink")[武隆](../Page/武隆.md "wikilink")
  - [雪玉洞](../Page/雪玉洞.md "wikilink")
    [重庆](../Page/重庆.md "wikilink")[酆都](../Page/酆都.md "wikilink")
  - [织金洞](../Page/织金洞.md "wikilink")
    [贵州](../Page/贵州.md "wikilink")[织金](../Page/织金.md "wikilink")
  - [双龙洞](../Page/双龙洞.md "wikilink")
    [浙江](../Page/浙江.md "wikilink")[金华](../Page/金华.md "wikilink")
  - [腾龙洞](../Page/腾龙洞.md "wikilink")
    [湖北](../Page/湖北.md "wikilink")[利川](../Page/利川.md "wikilink")
  - [百魔洞](../Page/百魔洞.md "wikilink")
    [广西](../Page/广西.md "wikilink")[巴马](../Page/巴马.md "wikilink")
  - [双河洞](../Page/双河洞.md "wikilink")
    [贵州](../Page/贵州.md "wikilink")[绥阳](../Page/绥阳.md "wikilink")
  - 太行龙洞 山西长治

## 澳洲的溶洞

  - 塔斯鐘乳石 (Gunns Plains Cave)

[溶洞](../Category/地形.md "wikilink")
[Category:喀斯特地形](../Category/喀斯特地形.md "wikilink")