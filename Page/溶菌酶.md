**溶菌酶**（英文名称：**Lysozyme**，又譯**溶解酶**）是一个[分子量为](../Page/分子量.md "wikilink")14.4[kDa的](../Page/kDa.md "wikilink")[酶](../Page/酶.md "wikilink")，它經由催化[肽聚糖中](../Page/肽聚糖.md "wikilink")[N-乙酰胞壁酸和](../Page/N-乙酰胞壁酸.md "wikilink")[N-乙酰氨基葡萄糖残基间和](../Page/N-乙酰氨基葡萄糖.md "wikilink")[壳糊精中](../Page/壳糊精.md "wikilink")[N-乙酰葡糖胺残基间的](../Page/N-乙酰葡糖胺.md "wikilink")1,4-β链的水解，而破坏细菌的细胞壁。一些人体细胞[分泌液中含有溶菌酶在](../Page/分泌液.md "wikilink")，如[唾液](../Page/唾液.md "wikilink")、[眼泪](../Page/眼泪.md "wikilink")、[鼻涕](../Page/鼻涕.md "wikilink")；溶菌酶也存在于[粒線體中的细胞质颗粒体和](../Page/粒線體.md "wikilink")[蛋清中](../Page/蛋清.md "wikilink")。

## 历史

1909年Laschtschenko发现蛋清中有杀菌物质。1922年，[亚历山大·弗莱明在研究鼻腔粘液的抗菌作用时发现并命名了溶菌酶](../Page/亚历山大·弗莱明.md "wikilink")。\[1\]

1965年，[大卫·菲利浦用](../Page/大卫·菲利浦.md "wikilink")[X射线衍射技术研究溶菌酶晶体](../Page/X射线.md "wikilink")，解析出了2[埃分辨率的晶体结构](../Page/埃.md "wikilink")。\[2\]\[3\]这是第二个使用[X射线衍射技术得到的蛋白质结构](../Page/X射线.md "wikilink")，也是第一个解析出的酶结构。大卫·菲利浦根据次结构提出了溶菌酶催化的机理，该催化机理最近得到了修正。\[4\]

## 生理学功能

溶菌酶是体内免疫系统的一部分，可杀灭革兰氏阳性菌。溶菌酶结合到细菌表面，减少负电荷并协助对细菌的吞噬作用。新生儿缺乏溶菌酶会导致肺支气管发育不良（bronchopulmonary
dysplasia）。食用缺乏溶菌酶的配方奶粉会使婴儿腹泻的概率提高三倍。眼泪中缺乏溶菌酶会导致结膜炎。

某些癌细胞会分泌过量的溶菌酶，导致血液中溶菌酶含量过高，造成肾衰竭和低血钾。

## 催化机制

溶菌酶进攻[肽聚糖](../Page/肽聚糖.md "wikilink")（细菌细胞壁的组分，特别在[革兰氏阳性菌的细胞壁中含量丰富](../Page/革兰氏阳性菌.md "wikilink")）[水解连接N](../Page/水解.md "wikilink")-乙酰胞壁酸和N-乙酰葡糖胺第四位碳原子的糖苷键。整个过程是，首先溶菌酶通过其两个结构域之间的“沟”结合到肽聚糖分子上；随后其底物在酶中形成[过渡态的构象](../Page/过渡态.md "wikilink")。根据Phillips机制，溶菌酶与[葡聚六糖结合](../Page/葡聚六糖.md "wikilink")。然后溶菌酶将葡聚六糖上的第四个糖扭曲为半椅形构象。在这种扭曲状态（能量较高）中，糖苷键很容易就发生断裂。

位于溶菌酶蛋白序列35位的[谷氨酸](../Page/谷氨酸.md "wikilink")（Glu35）和52位的[天冬氨酸](../Page/天冬氨酸.md "wikilink")（Asp52）的侧链被发现对于溶菌酶的活性非常关键。Glu35作为糖苷键的[质子供体](../Page/质子.md "wikilink")，剪切底物的C-O键；而Asp52作为亲核试剂参与生成糖基酶中间体。随后，糖基酶中间体与水分子发生反应，水解生成产物，而酶保持不变。

## 相关疾病

在一些遗传性[淀粉变性症中发现](../Page/淀粉变性症.md "wikilink")，病人编码的溶菌酶[基因中含有一个突变](../Page/基因.md "wikilink")，从而导致溶菌酶在体内多个[组织中聚集](../Page/组织_\(生物学\).md "wikilink")。\[5\]

## 应用

溶菌酶被广泛用于实验室中对细菌所进行的[细胞破碎](../Page/细胞破碎.md "wikilink")。

由于溶菌酶易于结晶，常被用于各种[晶体学相关的研究](../Page/晶体学.md "wikilink")。

## 参见

  - [肽聚糖](../Page/肽聚糖.md "wikilink")
  - [糖苷水解酶](../Page/糖苷水解酶.md "wikilink")
  - [淀粉变性症](../Page/淀粉变性症.md "wikilink")
  - [细胞破碎](../Page/细胞破碎.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [溶菌酶](http://macromoleculeinsights.com/lysozyme.php)
  - [溶菌酶结构和相关文章](http://lysozyme.co.uk/)

[Category:位於12號人類染色體的基因](../Category/位於12號人類染色體的基因.md "wikilink")
[Category:EC 3.2.1](../Category/EC_3.2.1.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Fleming A. *On a remarkable bacteriolytic element found in tissues
    and secretions.* Proc Roy Soc Ser B 1922;93:306-17
2.  Blake CC, Koenig DF, Mair GA, North AC, Phillips DC, Sarma VR.
    Structure of hen egg-white lysozyme. A three-dimensional Fourier
    synthesis at 2 Ångstrom resolution. *Nature*, **206**, 757-61
3.  Johnson LN, Phillips DC. Structure of some crystalline
    lysozyme-inhibitor complexes determined by X-ray analysis at 6
    Ångstrom resolution. *Nature*, **206**, 761-3.
4.  Vocadlo, D. J.; Davies, G. J.; Laine, R.; Withers, S. G. *Nature*
    2001, **412**, 835.
5.