**滇中龍屬**（[學名](../Page/學名.md "wikilink")：*Dianchungosaurus*）是[鱷形超目](../Page/鱷形超目.md "wikilink")[中真鱷類中已](../Page/中真鱷類.md "wikilink")[滅絕的一](../Page/滅絕.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[中國](../Page/中國.md "wikilink")。牠原先被認為是屬於[恐龍](../Page/恐龍.md "wikilink")\[1\]，但於2005年被[保羅·巴雷特](../Page/保羅·巴雷特.md "wikilink")（Paul
Barrett）與[徐星重新分類為](../Page/徐星.md "wikilink")[中真鱷類](../Page/中真鱷類.md "wikilink")。\[2\]
牠們可能與*Tianchungosaurus*是同一種動物。[模式種是祿豐滇中龍](../Page/模式種.md "wikilink")。

## 化石材料

滇中龍目前已被發現兩個標本：

  - IVPP
    V4735a：[正模式標本](../Page/正模式標本.md "wikilink")，一個[前上頜骨](../Page/前上頜骨.md "wikilink")。
  - IVPP
    V4735b：[副模式標本](../Page/副模式標本.md "wikilink")，[齒骨的左右部分](../Page/齒骨.md "wikilink")，附有牙齒。

兩個標本都是在[中國](../Page/中國.md "wikilink")[雲南省](../Page/雲南省.md "wikilink")[下祿豐組所發現](../Page/下祿豐組.md "wikilink")，年代為早[侏儸紀](../Page/侏儸紀.md "wikilink")[錫內穆階](../Page/錫內穆階.md "wikilink")。

## 分類系統

滇中龍最初在1982年被[楊鍾健分類於](../Page/楊鍾健.md "wikilink")[畸齒龍科](../Page/畸齒龍科.md "wikilink")，但有些人懷疑牠們的分類傾向，並認為滇中龍是個[疑名](../Page/疑名.md "wikilink")；但滇中龍還是通常被認為是畸齒龍科的一個有效屬，直到最近。在2005年，巴雷特與徐星發現滇中龍是不同動物的[嵌合體](../Page/嵌合體.md "wikilink")。正模式標本被重新分類於[中真鱷類](../Page/中真鱷類.md "wikilink")，而副模式標本則被分類於[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，但不確定為何屬。

為了維持科學文獻的一致性，模式標本依舊名為祿豐滇中龍，而副模式標本仍等待者正式的敘述。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐龍博物館——滇中龍](https://web.archive.org/web/20070612194517/http://www.dinosaur.net.cn/Museum/Diachongosaurus.htm)
  - [*Dianchungosaurus*](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=38728&real_user=1)
    in the Paleobiology Database

[Category:侏羅紀鱷形類](../Category/侏羅紀鱷形類.md "wikilink")
[Category:鱷形超目](../Category/鱷形超目.md "wikilink")

1.
2.