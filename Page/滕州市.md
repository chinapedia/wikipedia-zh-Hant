**滕州市**在[中国](../Page/中国.md "wikilink")[山东省南部](../Page/山东省.md "wikilink")，是[枣庄市代管的](../Page/枣庄市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，为山东省人口最多的县级市。

滕州人文[历史悠久](../Page/历史.md "wikilink")，据传为[墨家学派创始人](../Page/墨家.md "wikilink")[墨子和工匠祖师爷](../Page/墨子.md "wikilink")[鲁班的故里](../Page/鲁班.md "wikilink")。\[1\]

## 历史

上古及[周这里是](../Page/周.md "wikilink")[滕](../Page/滕国.md "wikilink")、[薛](../Page/薛国.md "wikilink")、[邾国之地](../Page/邾国.md "wikilink")，有“滕小国”之称，至今仍存有[东周时遗迹](../Page/东周.md "wikilink")[薛国遗址](../Page/薛国遗址.md "wikilink")。这里也是[春秋](../Page/春秋.md "wikilink")[战国时期的](../Page/战国.md "wikilink")[思想家](../Page/思想家.md "wikilink")、[军事家和](../Page/军事家.md "wikilink")[墨家学派创始人](../Page/墨家.md "wikilink")[墨子的故里](../Page/墨子.md "wikilink")。也是[战国四公子之一](../Page/战国四公子.md "wikilink")[孟尝君的故里](../Page/孟尝君.md "wikilink")，其墓现在滕州境内。[毛遂墓也在滕州境内](../Page/毛遂墓.md "wikilink")。[秦在此置滕县](../Page/秦.md "wikilink")，[汉改蕃县](../Page/汉.md "wikilink")，[隋复名滕县](../Page/隋.md "wikilink")。1988年撤县设市。

### 上古及先秦

[Painted_Bo-Shaped_Pottery_Ding.jpg](https://zh.wikipedia.org/wiki/File:Painted_Bo-Shaped_Pottery_Ding.jpg "fig:Painted_Bo-Shaped_Pottery_Ding.jpg")彩陶缽形鼎，[东沙河镇](../Page/东沙河镇.md "wikilink")[岗上遗址出土](../Page/岗上遗址.md "wikilink")\]\]
上古时，[炎帝和](../Page/炎帝.md "wikilink")[黄帝之战后](../Page/黄帝.md "wikilink")，炎帝战败，黄帝东进，封其第十子于滕。黄帝所封滕国，于[商末灭亡](../Page/商.md "wikilink")，被周代替。约公元前1046年，[周武王封其弟](../Page/周武王.md "wikilink")[错叔绣于滕](../Page/错叔绣.md "wikilink")，传31世，于公元前414年为[越国所灭](../Page/越国.md "wikilink")，后复国。[战国时期](../Page/战国时期.md "wikilink")，在[滕文公当政时期](../Page/滕文公.md "wikilink")，按照[孟子的主张治理国家](../Page/孟子.md "wikilink")，行善政，施善教，政绩卓著，被誉为“善国”。

### 秦汉

[秦始皇统一六国后](../Page/秦始皇.md "wikilink")，废[封建置](../Page/封建.md "wikilink")[郡县](../Page/郡县制.md "wikilink")，滕县属薛郡。

[汉初](../Page/汉朝.md "wikilink")，[刘邦撤小邾置](../Page/刘邦.md "wikilink")[蕃县](../Page/蕃县.md "wikilink")，[汉武帝时](../Page/汉武帝.md "wikilink")，改滕县为[公丘县](../Page/公丘县.md "wikilink")。[北魏撤销公丘县](../Page/北魏.md "wikilink")，设立阳平县。北齐把阳平、薛、合乡、昌虑、永兴、永福六县并入蕃县，并将[沛](../Page/沛.md "wikilink")、高平、南武阳等县的部分地区划归蕃县。

### 隋唐

[隋](../Page/隋朝.md "wikilink")[开皇六年](../Page/开皇.md "wikilink")（586年）蕃县改名为滕县。开皇十六年（596年），改为滕郡。不久，又将滕郡改为滕县。唐太宗[李世民](../Page/李世民.md "wikilink")，封其弟[李元婴为](../Page/李元婴.md "wikilink")[滕王于此](../Page/滕王.md "wikilink")。其在滕期间，无心政务，奢于游玩，沉于歌乐，被当地[人民弹告](../Page/人民.md "wikilink")，被贬于洪州（即今日[南昌](../Page/南昌.md "wikilink")），筑[滕王阁并继续游乐](../Page/滕王阁.md "wikilink")。唐[元和年间](../Page/元和_\(唐朝\).md "wikilink")，县城东移二里修筑新城，并在新城东建有龙泉禅寺（根据明代地图所示，应为“慈泉寺”），并建有龙泉塔用以镇压奔腾的泉水（仅存龙泉塔于龙泉广场）。

### 宋元

[北宋基本沿袭](../Page/北宋.md "wikilink")[唐朝的建制](../Page/唐朝.md "wikilink")。宋南迁之后，金于滕县兼置滕阳军。金[大定二十二年](../Page/大定.md "wikilink")（1182年），改滕阳军为滕阳州。大定二十四年，改滕阳州为滕州（治滕县），辖滕、沛、邹三县和陶阳镇。

### 明清

[明洪武二年](../Page/明朝.md "wikilink")（1369年），撤销滕州。清[乾隆年间](../Page/乾隆.md "wikilink")，将湖陵县部分地区划入滕县。

### 现代

[民国时期](../Page/中华民国.md "wikilink")，滕县政区基本未变。[徐州会战时期](../Page/徐州会战.md "wikilink")，这里首先进行了[滕縣保卫战](../Page/滕縣戰鬥_\(抗日戰爭\).md "wikilink")，保卫战中，从[四川调出的川军](../Page/四川.md "wikilink")（二十二集团军一二二师）受到重创。是役共击毙日军2000余人，守军自师长[王铭章以下伤亡近万人](../Page/王铭章.md "wikilink")。一二二师师长[王铭章于此牺牲](../Page/王铭章.md "wikilink")。

[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，设置滕县专区，后改济宁专区。1979年，滕县改属[枣庄市](../Page/枣庄市.md "wikilink")。1988年3月7日，撤销滕县，设立县级滕州市，由地级枣庄市管理。

## 地理

地处[黄淮平原上](../Page/黄淮平原.md "wikilink")，东接[泰沂山脉](../Page/泰沂山脉.md "wikilink")、西临[微山湖](../Page/微山湖.md "wikilink")。全市地势比较平缓，年平均气温13℃，降水量773毫米。
滕州是山东重要的粮食产区，主产[小麦](../Page/小麦.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、土豆等。
滕州境内[煤炭资源丰富](../Page/煤炭.md "wikilink")，储量高達52.3億[公噸](../Page/公噸.md "wikilink")，居山东省之首。\[2\]

## 行政区划

辖4个[街道办事处](../Page/街道办事处.md "wikilink")、15个[镇](../Page/镇.md "wikilink")，179个[居委会](../Page/居委会.md "wikilink")、1047个[村委会](../Page/村委会.md "wikilink")。

## 交通

  - [京沪铁路纵贯市境](../Page/京沪铁路.md "wikilink")，有[河道连通](../Page/河.md "wikilink")[京杭大运河](../Page/京杭大运河.md "wikilink")。
  - [京福高速公路和](../Page/京福高速公路.md "wikilink")[104国道穿越境内](../Page/104国道.md "wikilink")，104国道横穿市区，京福高速在城市东部。[京沪高速铁路在东部穿过](../Page/京沪高速铁路.md "wikilink")，设有[滕州东站](../Page/滕州东站.md "wikilink")。

## 经济

滕州[经济由于人口众多](../Page/经济.md "wikilink")，经济总量较高在全国[百强县排名中](../Page/百强县.md "wikilink")，位于第23名。
同时，也应看到，滕州的经济仍然以轻工业、生产加工业、煤炭相关产业为主，服务业发展较为落后，在经济构成中占比不足4成，属于典型的工业化中后期产业结构。
从经济关联度上来看，滕州市与枣庄其它区县的关联并不大，因此造成了枣庄全市没有“统一城区”的现象：滕州作为县级市自立一城，薛城区凭借发达的交通和新城建设自立一城，市中区则由历史缘故保留一城。从城市聚集效应的角度来说，枣庄一市之内不存在人口流动壁垒，滕州的经济发展没有对周围的人口形成吸引，也并没有产生区域性辐射，导致枣庄市难以形成统一城区，而是由于历史原因在多处围绕产业、行政区位因素形成小城镇。

从财政上来说，目前滕州市财政被纳入省直辖管理，相对枣庄其它区县较为独立。
[Tengzhou_Govenment_Inside_2011-05.JPG](https://zh.wikipedia.org/wiki/File:Tengzhou_Govenment_Inside_2011-05.JPG "fig:Tengzhou_Govenment_Inside_2011-05.JPG")

## 教育

滕州身为墨鲁故里，又紧邻[孟子故里](../Page/孟子.md "wikilink")[邹城市](../Page/邹城市.md "wikilink")，离[曲阜亦很近](../Page/曲阜.md "wikilink")，深受儒家，墨家文化影响，重视教育。这里私人投资教育很常见，比较出名的有以初中教育为主的[滕东中学](../Page/滕东中学.md "wikilink")、[育才中学](../Page/育才中学.md "wikilink")、[尚贤中学](../Page/尚贤中学.md "wikilink")、[善国中学](../Page/善国中学.md "wikilink")。身为县级市，积极争取在当地办大学教育，2004年成立的[科圣大学](../Page/科圣大学.md "wikilink")（[枣庄科技职业学院](../Page/枣庄科技职业学院.md "wikilink")）是滕州历史上的第一所高等专科院校。滕州一中为该市最好中学，原[中国人民解放军](../Page/中国人民解放军.md "wikilink")[政治部主任](../Page/政治部.md "wikilink")[李继耐即为该校毕业](../Page/李继耐.md "wikilink")。

## 文化旅游

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[薛城遗址](../Page/薛城遗址.md "wikilink")、[北辛遗址](../Page/北辛文化.md "wikilink")
  - [山东省文物保护单位](../Page/山东省文物保护单位.md "wikilink")：[岗上遗址](../Page/岗上遗址.md "wikilink")、[滕国故城](../Page/滕国故城.md "wikilink")、[薛国故城](../Page/薛国故城.md "wikilink")、[北辛遗址](../Page/北辛遗址.md "wikilink")、[龙泉塔](../Page/龙泉塔.md "wikilink")、[滕州县衙](../Page/滕州县衙.md "wikilink")、[皇陵旧址](../Page/皇陵旧址.md "wikilink")、[百寿坊](../Page/百壽坊_\(滕州市\).md "wikilink")、[王家祠堂](../Page/王家祠堂.md "wikilink")、[刘氏家祠](../Page/刘氏家祠.md "wikilink")、[张氏祠堂](../Page/张氏祠堂.md "wikilink")、[唐代石雕群](../Page/唐代石雕群.md "wikilink")、[庄里西遗址](../Page/庄里西遗址.md "wikilink")、[西康留遗址](../Page/西康留遗址.md "wikilink")、[前掌大遗址](../Page/前掌大遗址.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/毛遂墓.md" title="wikilink">毛遂墓</a></li>
<li><a href="../Page/荊河公園.md" title="wikilink">荊河公園</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/善国文公台.md" title="wikilink">善国文公台</a></li>
<li><a href="../Page/孟尝君陵园.md" title="wikilink">孟尝君陵园</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/汉画像石馆.md" title="wikilink">汉画像石馆</a></li>
<li><a href="../Page/滕州博物馆.md" title="wikilink">滕州博物馆</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/王学仲艺术馆.md" title="wikilink">王学仲艺术馆</a></li>
<li><a href="../Page/中外酒瓶博物馆.md" title="wikilink">中外酒瓶博物馆</a></li>
<li><a href="../Page/滨湖万亩红荷湿地.md" title="wikilink">滨湖万亩红荷湿地</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 特色饮食

滕州的特色食品有[羊肉汤](../Page/羊肉汤.md "wikilink")、[煎饼](../Page/煎饼.md "wikilink")、[烧饼](../Page/烧饼.md "wikilink")（当地所讲烧饼特指吊炉烧饼，即一种蒲扇大小白面薄饼，单面烘烤，初较香脆，久置则变软。另有一种类似黄桥烧饼，当地称麻饼或麻火烧），[菜煎饼](../Page/菜煎饼.md "wikilink")、[辣汤等](../Page/辣汤.md "wikilink")。还有张汪板鸭，王开猪头肉。

## 著名人物

### 古代

  - [滕文公](../Page/滕文公.md "wikilink")
  - [墨子](../Page/墨子.md "wikilink")
  - [鲁班](../Page/鲁班.md "wikilink")
  - [奚仲](../Page/奚仲.md "wikilink")
  - [毛遂](../Page/毛遂.md "wikilink")
  - [孟尝君](../Page/孟尝君.md "wikilink")
  - [叔孙通](../Page/叔孙通.md "wikilink")
  - [公孙弘](../Page/公孙弘.md "wikilink")
  - [王嘉宾](../Page/王嘉宾.md "wikilink")

### 近代

  - [高熙喆](../Page/高熙喆.md "wikilink")
  - [刘子衡](../Page/刘子衡.md "wikilink")
  - [洪振海](../Page/洪振海.md "wikilink")

### 现代

  - [王学仲](../Page/王学仲.md "wikilink")
  - [马世晓](../Page/马世晓.md "wikilink")
  - [杨广立](../Page/杨广立.md "wikilink")
  - [李景](../Page/李景_\(解放军\).md "wikilink")
  - [杨斯德](../Page/杨斯德.md "wikilink")
  - [李继耐](../Page/李继耐.md "wikilink")
  - [刘书田](../Page/刘书田.md "wikilink")
  - [张兆垠](../Page/张兆垠.md "wikilink")
  - [张知寒](../Page/张知寒.md "wikilink")
  - [刘大印](../Page/刘大印.md "wikilink")

## 參考

<div class="references-small">

<references />

</div>

武洪洋

## 外部链接

  - [滕州市政府](http://www.tengzhou.gov.cn)
  - [滕州在线](http://www.tzzx.net)
  - [滕州日报](http://www.tzdaily.com.cn)
  - [滕州123](http://www.tengzhou123.com)
  - [掌上滕州](https://web.archive.org/web/20120816024438/http://www.0632tz.cn/)

[滕州市](../Category/滕州市.md "wikilink") [市](../Category/枣庄区市.md "wikilink")
[枣庄](../Category/山东县级市.md "wikilink")

1.
2.