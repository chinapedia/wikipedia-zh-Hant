**滕海濱**（），生於[北京](../Page/北京.md "wikilink")，世界級[體操運動員](../Page/體操.md "wikilink")，被譽為「有望成为继[李小鵬之后的新领军人物](../Page/李小鵬_\(體操運動員\).md "wikilink")」。

滕海濱最初在入讀[北京](../Page/北京.md "wikilink")[什刹海体校學習体操](../Page/什刹海.md "wikilink")，之後进入北京市队。兩年後入選国家队。

滕海濱於2000年首先奪得全国青少年体操锦标赛[自由体操冠军](../Page/自由体操.md "wikilink")、[单杠冠军](../Page/单杠.md "wikilink")、[双杠冠军以及全能冠军](../Page/双杠.md "wikilink")，同年再奪[法国国际体操邀请赛双杠](../Page/法国.md "wikilink")、[鞍马以及自由操冠軍](../Page/鞍马.md "wikilink")。2002年分別贏得「全国锦标赛」单杠冠军、[釜山亚运会单杠](../Page/釜山亚运会.md "wikilink")、鞍马及团体賽金牌、世锦赛鞍马殿軍。1年後再奪得世锦赛鞍马與团体冠军。

2004年，滕海濱於男子鞍马項目中為中國摘下了[2004年雅典奧運唯一的一面體操金牌](../Page/2004年夏季奥林匹克运动会.md "wikilink")。可惜的是，滕海滨在2004年夏季奥林匹克运动会的男子体操团体比赛失利，加上狀態每況愈下，令他無緣[2008年夏季奥林匹克运动会](../Page/2008年夏季奥林匹克运动会.md "wikilink")。

2009年，在[第十一屆全運會奪得男子個人全能金牌](../Page/中華人民共和國第十一屆全運會體操比賽.md "wikilink")。

2012年，滕海滨原本將要代表[中国出戰](../Page/2012年夏季奥林匹克运动会中国代表团.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦舉行的](../Page/倫敦.md "wikilink")[奥林匹克运动会](../Page/2012年夏季奥林匹克运动会.md "wikilink")[體操比賽](../Page/2012年夏季奧林匹克運動會體操比賽.md "wikilink")；但在奧運開幕前數天，他在[北爱尔兰训练时不慎弄伤小臂](../Page/北爱尔兰.md "wikilink")；數天後，总教练[黄玉斌認為他不能在短时间内恢复](../Page/黄玉斌.md "wikilink")，因而被迫宣布由[郭伟阳顶替他](../Page/郭伟阳.md "wikilink")\[1\]。

2014年3月14日，滕海滨與[張楠領証完婚](../Page/張楠.md "wikilink")。\[2\]

## 參考資料

## 外部連結

  - [滕海滨(双杠)](https://g-flash.net/en/pb-tenhaibin)

[Category:中国体操运动员](../Category/中国体操运动员.md "wikilink")
[Category:中国奥运体操运动员](../Category/中国奥运体操运动员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:北京籍运动员](../Category/北京籍运动员.md "wikilink")
[Haibin](../Category/滕姓.md "wikilink")
[Category:奧林匹克運動會體操獎牌得主](../Category/奧林匹克運動會體操獎牌得主.md "wikilink")
[Category:世界体操锦标赛奖牌得主](../Category/世界体操锦标赛奖牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:2010年亚洲运动会金牌得主](../Category/2010年亚洲运动会金牌得主.md "wikilink")
[Category:2010年亚洲运动会铜牌得主](../Category/2010年亚洲运动会铜牌得主.md "wikilink")

1.  [滕海滨下郭伟阳上
    体操领队详解幕后原因](http://news.sports.cn/gymnastics/2012-07-27/11402.html)
     《中国体育报》2012-07-27
2.  <http://www.chinanews.com/ty/2014/03-14/5952024.shtml>