[tengwangge_song.jpg](https://zh.wikipedia.org/wiki/File:tengwangge_song.jpg "fig:tengwangge_song.jpg")人笔下的[滕王阁](../Page/滕王阁.md "wikilink")\]\]
**滕王閣序**，原题作《**滕王阁诗序**》，全名《**秋日登洪府滕王阁饯别序**》，[初唐四傑之一](../Page/初唐四傑.md "wikilink")[王勃作品](../Page/王勃.md "wikilink")，是古今傳誦的[駢文名篇](../Page/駢文.md "wikilink")。

## 背景

[上元二年](../Page/上元_\(唐高宗\).md "wikilink")（675年）九月九日王勃探親路過[南昌](../Page/南昌.md "wikilink")，正值洪州都督（洪州牧）[阎伯屿重修](../Page/阎伯屿.md "wikilink")[滕王閣畢](../Page/滕王閣.md "wikilink")，於閣上大宴賓客，餞別新任新州[刺史宇文氏一行](../Page/刺史.md "wikilink")；閻席上假意邀請在座賓客為滕王閣寫作序文，不料王勃竟提筆大作。

關於《滕王閣序》的由來，唐末[王定保的](../Page/王定保.md "wikilink")《[唐摭言](../Page/唐摭言.md "wikilink")》有一段生動的記載，原來閻伯嶼本意是讓其婿[孟學士作序以彰其名](../Page/孟學士.md "wikilink")，不料在假謙讓時，王勃卻提筆就作。閻公初憤然離席，至配室更衣，並派人伺其下筆。初聞「豫章故郡，洪都新府」，閻公覺得「亦是老生常談」；接著又報：「星分翼軫，地接衡廬。」閻又說：「無非是些舊事罷了。」接下來的「臺隍枕夷夏之交，賓主盡東南之美」，公聞之，沉吟不言；及至「落霞與孤鶩齊飛，秋水共長天一色」一句，乃大驚「此真天才，當垂不朽矣！」，出立於勃側而觀，遂亟請宴所，極歡而罷。

|                                                                                                                                                                                                                                                                                                                |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Wengzhengming4.jpg](https://zh.wikipedia.org/wiki/File:Wengzhengming4.jpg "fig:Wengzhengming4.jpg")……[Wengzhengming2.jpg](https://zh.wikipedia.org/wiki/File:Wengzhengming2.jpg "fig:Wengzhengming2.jpg")[Wengzhengming1.jpg](https://zh.wikipedia.org/wiki/File:Wengzhengming1.jpg "fig:Wengzhengming1.jpg") |
| [文徵明書法](../Page/文徵明.md "wikilink")《滕王閣序》                                                                                                                                                                                                                                                                       |

## 参见

  - [王勃](../Page/王勃.md "wikilink")
  - [滕王阁](../Page/滕王阁.md "wikilink")
  - [骈文](../Page/骈文.md "wikilink")
  - [初唐四傑](../Page/初唐四傑.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 參考書目

  - [唐邦治](../Page/唐邦治.md "wikilink")：《王子安秋日登洪府滕王閣宴餞序詳註》
  - 道坂昭廣：〈[關於王勃〈滕王閣序〉的幾個問題](http://www.cl.nthu.edu.tw/ezfiles/278/1278/attach/58/pta_17053_1250834_98726.pdf)〉。

[Category:唐朝文学作品](../Category/唐朝文学作品.md "wikilink")