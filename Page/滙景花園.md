<span id="coordinates" class="plainlinksneverexpand">

<div style="position:absolute; right: 3px; top:28px; width: 420px; font-size: small;">

[座標](../Page/地理坐标.md "wikilink")：

</div>

</span> {{ Otheruses|other=滙景花園之商場|匯景廣場}}

[Sceneway_Garden_swimming_pool.jpg](https://zh.wikipedia.org/wiki/File:Sceneway_Garden_swimming_pool.jpg "fig:Sceneway_Garden_swimming_pool.jpg")
[Sceneway_Garden_Enterance_Plaza.jpg](https://zh.wikipedia.org/wiki/File:Sceneway_Garden_Enterance_Plaza.jpg "fig:Sceneway_Garden_Enterance_Plaza.jpg")
**滙景花園**（****）為[香港](../Page/香港.md "wikilink")[九龍區大型私人屋苑之一](../Page/九龍區.md "wikilink")，興建在[港鐵](../Page/港鐵.md "wikilink")[藍田站上蓋](../Page/藍田站.md "wikilink")，為其鐵路沿線物業。屋苑於1991年12月至1992年4月落成入伙。共設17座，合共提供4,112個住宅單位。

此屋苑由[劉榮廣伍振民建築師事務所設計](../Page/劉榮廣.md "wikilink")，是第三個不屬於[港鐵公司的港鐵上蓋項目](../Page/港鐵公司.md "wikilink")。滙景花園的發展商是[長江實業旗下銀宇有限公司及原](../Page/長江實業.md "wikilink")[東區海底隧道財團](../Page/東區海底隧道.md "wikilink")（另外兩個是[銀禧花園和](../Page/銀禧花園.md "wikilink")[文禮閣](../Page/文禮閣.md "wikilink")，亦由長實發展）。由於建在小山丘上面，向南前排單位可以享有[香港島以至維港煙花海景](../Page/香港島.md "wikilink")。

## 位置

滙景花園位處[2號幹綫的一個平台之上](../Page/香港二號幹綫.md "wikilink")，下面是[匯景廣場](../Page/匯景廣場.md "wikilink")，且有[天橋直通](../Page/天橋.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[藍田站](../Page/藍田站.md "wikilink")。區內的另外數個大型購物商場包括[啓田商場和](../Page/啓田商場.md "wikilink")[麗港城商場亦在滙景花園](../Page/麗港城商場.md "wikilink")10分鐘腳程之內。滙景花園一共有17座，9至17座屬於較早啟用的B期，每座28層高；1至8座則屬於A期，每座31至34層高。

## 間隔

滙景花園提供面積由674呎兩房戶至最大的913方呎三房連套房，全部為鑽石廳間隔。兩房戶由674呎至最大的680方呎，數呎之差純粹因高低層的牆身有別，間隔一律有小走廊、方形房間兼有窗台。兩房戶僅得第一至第八座的ABCD室供應，佔全苑的25%。屋苑三房單位，面積由783至785呎，佔總供應量的一半，選擇最多。至於910至913方呎則為三房連套房，除了夠闊落，更雄踞9至17座的外圍景觀，大部份單位擁綠油油晒草灣景觀，高層更可遠眺海景。

## 設施

滙景花園的設施包括住客會所、兩個[游泳池](../Page/游泳池.md "wikilink")、[網球場和多個](../Page/網球場.md "wikilink")[兒童遊樂場](../Page/兒童遊樂場.md "wikilink")。平台花園佔地43萬平方呎，中央設有大型廣場，廣場兩則設有[緩跑徑及有蓋行人通道連接各座住宅](../Page/緩跑徑.md "wikilink")。平台花園外圍則設有[幼稚園](../Page/幼稚園.md "wikilink")、老人中心及通道連接[匯景廣場](../Page/匯景廣場.md "wikilink")。

屋苑附近範圍亦設有1995年落成的[茜發道網球場及全港首個興建在已修復堆填區的](../Page/茜發道網球場.md "wikilink")[晒草灣遊樂場](../Page/晒草灣遊樂場.md "wikilink")，以推動環保為主題，是居民戶外活動的好去處。場內有多用途草地球場（可供足球或棒球使用）、棒球練習場、兒童遊樂場及緩跑徑等，更設有「環保廊」讓公眾了解香港的廢物管理。

屋苑附近的另一個大型私人屋苑[麗港城設有廣達](../Page/麗港城.md "wikilink")30萬平方呎之[麗港公園亦在步程十分鐘之內](../Page/麗港公園.md "wikilink")。

<File:Sceneway> Garden Open Space overview.jpg|平台花園（1） <File:Sceneway>
Garden Open Space View1.jpg|平台花園（2） <File:Sceneway> Garden Open Space
View2.jpg|平台花園內的緩跑徑 <File:Sceneway> Garden Children Playground
2010.JPG|平台花園內的兒童遊樂場 <File:Club> Sceneway view 2011.jpg|平台花園內會所入口

## 交通

滙景花園座落[港鐵](../Page/港鐵.md "wikilink")[藍田站上蓋](../Page/藍田站.md "wikilink")，亦位處[東區海底隧道出口](../Page/東區海底隧道.md "wikilink")，因此交通非常方便。另外，[匯景廣場基座設有](../Page/匯景廣場.md "wikilink")[公共運輸交匯處](../Page/藍田公共運輸交匯處.md "wikilink")。有的士站、小巴站、巴士站、往返[皇崗口岸的](../Page/皇崗口岸.md "wikilink")[觀塘24小時跨境快線和機場巴士](../Page/觀塘24小時跨境快線.md "wikilink")[城巴A22線可供選擇](../Page/城巴A22線.md "wikilink")。

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[藍田站](../Page/藍田站.md "wikilink")

<!-- end list -->

  - [藍田站巴士總站](../Page/藍田站巴士總站.md "wikilink")

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 觀塘24小時跨境快線：藍田站至[皇崗口岸](../Page/皇崗口岸.md "wikilink")

<!-- end list -->

  - [鯉魚門道](../Page/鯉魚門道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 藍田至[觀塘線](../Page/觀塘.md "wikilink")\[1\]
  - [旺角至藍田線](../Page/旺角.md "wikilink") (24小時服務)\[2\]
  - [佐敦道](../Page/佐敦道.md "wikilink")/[土瓜灣至藍田線](../Page/土瓜灣.md "wikilink")\[3\]
  - [青山道至藍田](../Page/青山道.md "wikilink")/[油塘線](../Page/油塘.md "wikilink")\[4\]
  - [灣仔至藍田](../Page/灣仔.md "wikilink")/油塘線 (通宵服務)\[5\]
  - 觀塘至油塘/[鯉魚門線](../Page/鯉魚門.md "wikilink")\[6\]
  - 旺角至油塘/鯉魚門線 (24小時服務)\[7\]
  - 佐敦道至油塘/鯉魚門線\[8\]
  - [荃灣至油塘](../Page/荃灣.md "wikilink")/鯉魚門線 (上下午繁時服務)\[9\]
  - 觀塘至[柴灣線](../Page/柴灣.md "wikilink")\[10\]

<!-- end list -->

  - [將軍澳道](../Page/將軍澳道.md "wikilink")

<!-- end list -->

  - [啟田道](../Page/啟田道.md "wikilink")/[德田街](../Page/德田街.md "wikilink")

<!-- end list -->

  - [茜發道](../Page/茜發道.md "wikilink")

</div>

</div>

  - 道路交通

<!-- end list -->

  - [啟田道近](../Page/啟田道.md "wikilink")[鯉魚門道交匯處有天橋進入滙景花園](../Page/鯉魚門道.md "wikilink")，即[匯景道](../Page/匯景道#.E6.BB.99.E6.99.AF.E9.81.93.md "wikilink")。

### 匯景道

[scenewayroad.jpg](https://zh.wikipedia.org/wiki/File:scenewayroad.jpg "fig:scenewayroad.jpg")
**匯景道**（****）為滙景花園的一條[私家路](../Page/私家路.md "wikilink")，位於九龍東[晒草灣一帶](../Page/晒草灣.md "wikilink")，以途經的滙景花園命名。道路西面連接滙景花園，東面則連接[聖公會基孝中學旁的](../Page/聖公會基孝中學.md "wikilink")[啟田道](../Page/啟田道.md "wikilink")。此道路為駕車人士前往滙景花園及[匯景廣場必經之路](../Page/匯景廣場.md "wikilink")，上放學時間均有非滙景花園住戶行走此道路。

## 購物

基座設有街坊式商場[匯景廣場外](../Page/匯景廣場.md "wikilink")，附近亦設有[領展的](../Page/領展.md "wikilink")[啓田商場以購買日常用品](../Page/啓田商場.md "wikilink")。住客可以步行約20分鐘或乘港鐵前往[觀塘](../Page/觀塘.md "wikilink")[創紀之城五期的](../Page/創紀之城.md "wikilink")[apm商場](../Page/apm.md "wikilink")。

## 附近規劃

政府為了增加土地供應，擬把毗鄰滙景花園及麗港城的[茜發道山坡地皮放入勾地表](../Page/茜發道.md "wikilink")，並把該帶地段分批推出。該地皮位處麗港城35座後面，已經平整多時，由於地勢稍高，屬東九龍罕有海景靚地；此外，地皮對落已是佈滿殘破舊樓的茶果嶺道，形成一片超大型發展空間，價值非比尋常。該地皮距港鐵站約5分鐘步程，社區設施已經完善，加上海景優越，市場視之為「絕品」靚地，市區難尋。市場人士估計東九海景地價每呎值1萬港元。

## 事故

  - 滙景花園於1991年5月10日建築期間發生[四級大火](../Page/香港火警分級制度#四級火警.md "wikilink")，第9座地庫停車場起火，濃煙瀰漫整個[藍田](../Page/藍田.md "wikilink")，焚燒達36小時後至翌日才被撲熄，造成3名消防員受傷，地鐵[藍田站一度局部封閉](../Page/藍田站.md "wikilink")。\[11\]\[12\]

## 參考資料

<div class="references-small">

<references/>

</div>

  - [中原數據-滙景花園](http://www.centadata.com/ptest.aspx?type=2&code=EWGYWPWXPD)

[Category:觀塘區私人屋苑](../Category/觀塘區私人屋苑.md "wikilink") [Category:藍田
(香港)](../Category/藍田_\(香港\).md "wikilink")
[Category:晒草灣](../Category/晒草灣.md "wikilink")
[Category:長江實業物業](../Category/長江實業物業.md "wikilink")

1.  [觀塘市中心　—　藍田](http://www.16seats.net/chi/rmb/r_k60.html)
2.  [旺角花園街　—　藍田](http://www.16seats.net/chi/rmb/r_k16.html)
3.  [佐敦道寧波街　—　藍田](http://www.16seats.net/chi/rmb/r_k55.html)
4.  [青山道香港紗廠　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
5.  [灣仔　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_kh63.html)
6.  [觀塘協和街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k14.html)
7.  [旺角先達廣場　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k40.html)
8.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
9.  [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
10. [觀塘宜安街　—　柴灣站](http://www.16seats.net/chi/rmb/r_kh08.html)
11. 「建築地盤首期第九座地庫停車場 滙景花園四級大火 濃煙瀰漫整個藍田
    灌救困難迄今日凌晨仍未熄滅」，[華僑日報第一頁](../Page/華僑日報.md "wikilink")，1991年5月11日
12. 「焚燒三十六小時 三消防員受傷 滙景長命火昨晚撲熄 樓層穿洞結構待檢驗 火場灌滿鹹水
    地鐵站要局部封閉」，[大公報第八版](../Page/大公報.md "wikilink")，1991年5月12日