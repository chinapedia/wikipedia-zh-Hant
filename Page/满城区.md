**满城区**，史稱**北平**，在[河北省中部偏西](../Page/河北省.md "wikilink")、[太行山东麓](../Page/太行山.md "wikilink")，是[保定市下辖的一个市辖区](../Page/保定市.md "wikilink")。区政府驻[满城镇中山路](../Page/满城镇.md "wikilink")。

## 历史沿革

[汉朝置](../Page/汉朝.md "wikilink")**北平县**，属[中山国](../Page/中山郡.md "wikilink")，[魏](../Page/曹魏.md "wikilink")[晋皆因袭之](../Page/西晋.md "wikilink")；[东魏侨置之](../Page/东魏.md "wikilink")[南营州](../Page/南营州.md "wikilink")**永乐县**治县内乐城（今县城西北五里）；[北齐合并北平](../Page/北齐.md "wikilink")、蒲阴、望都为北平县，治原蒲阴县城；[隋朝仍置永乐县](../Page/隋朝.md "wikilink")，属[上谷郡](../Page/上谷郡.md "wikilink")；[唐朝改](../Page/唐朝.md "wikilink")**满城**，属[易州](../Page/易州.md "wikilink")；宋辽之际为边境，北部属[辽](../Page/辽朝.md "wikilink")，废置，南部属[宋](../Page/宋朝.md "wikilink")，并入[保塞县](../Page/保塞县.md "wikilink")；[金复置](../Page/金朝.md "wikilink")，属[保州](../Page/保州.md "wikilink")；[元属](../Page/元朝.md "wikilink")[保定路](../Page/保定路.md "wikilink")，[明](../Page/明朝.md "wikilink")[清属](../Page/清朝.md "wikilink")[保定府](../Page/保定府.md "wikilink")。

### 建国后

1949年8月，改属保定专区。

1958年6月，[完县](../Page/完县.md "wikilink")、满城县合并，称完满县，同年11月又分立，满城县改为保定市满城区。

1960年3月，[清苑](../Page/清苑.md "wikilink")、完县、满城三区合并，称清苑县，县治满城县，仍属保定市。

1961年，恢复满城县建制，仍属保定专区。

1983年11月，划归保定市管辖。

2015年5月8日，国务院批准撤销满城县，设立保定市满城区。

## 行政区划

下辖1个[街道办事处](../Page/街道办事处.md "wikilink")、5个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 旅游资源

  - 西汉[中山靖王墓](../Page/中山靖王墓.md "wikilink")（[刘胜及其妻](../Page/刘胜.md "wikilink")[窦绾墓](../Page/窦绾.md "wikilink")），俗称[满城汉墓](../Page/满城汉墓.md "wikilink")

## 官方网站

  - [满城县政府门户网](http://www.mancheng.gov.cn/)

## 参考文献

[满城区](../Page/category:满城区.md "wikilink")
[区](../Page/category:保定区县市.md "wikilink")
[保定](../Page/category:河北市辖区.md "wikilink")