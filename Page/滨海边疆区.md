**滨海边疆区**（）是[俄罗斯](../Page/俄罗斯.md "wikilink")[远东联邦管区的一个边疆区](../Page/远东联邦管区.md "wikilink")，[面积](../Page/面积.md "wikilink")164,673平方公里，2010年統計[人口](../Page/人口.md "wikilink")1,956,497人。首府是[海参崴](../Page/海参崴.md "wikilink")（符拉迪沃斯托克）。该区与[中国及](../Page/中国.md "wikilink")[朝鲜接壤](../Page/朝鲜.md "wikilink")。

清代中国于1689年与[俄罗斯帝国订立](../Page/俄罗斯帝国.md "wikilink")《[尼布楚条约](../Page/尼布楚条约.md "wikilink")》，是中国首次与西方国家签订具现代国际法標准的条约，确立了中国历史上的第一条具有主权性质边界，条约确定包括滨海边疆区等[外東北地區劃入中国主权范围](../Page/外東北.md "wikilink")。1860年11月14日俄罗斯逼迫中国清朝政府簽署了《[中俄北京條約](../Page/中俄北京條約.md "wikilink")》将原先规定为中俄共同管理的[烏蘇里江以東至海之地](../Page/烏蘇里江.md "wikilink")（包括[库页岛以及](../Page/库页岛.md "wikilink")[海参崴在内](../Page/海参崴.md "wikilink")）约40万平方公里歸俄國所屬，从此中国失去了這片东北地区对[日本海的出海口](../Page/日本海.md "wikilink")。

## 历史

今滨海边疆区（以下简称**滨海**）所辖地区在历史上与[中国有密切的关系](../Page/中国.md "wikilink")。清代属于[吉林省辖地](../Page/吉林省.md "wikilink")。

### 先秦時期

上古[五帝时](../Page/五帝.md "wikilink")[肃慎](../Page/肃慎.md "wikilink")（活动范围就在黑龙江流域及海）臣服于[舜帝](../Page/舜帝.md "wikilink")，向舜帝朝贡武器\[1\]。

[周武王克](../Page/周武王.md "wikilink")[殷商后](../Page/商朝.md "wikilink")，肃慎再次进贡楛（音户）矢石砮\[2\]\[3\]。
周成王攻伐东夷取得胜利时，肃慎族的使者前来祝贺，周成王指令荣伯作“锡肃慎氏命”。“命”是古代中国帝王以信物爵位赐给臣下的诏书，周初被称为肃慎的部落联盟就臣属西周并接受周王的册命。\[4\]

在[伯力博物馆里存放着自](../Page/伯力博物馆.md "wikilink")[战国至两汉间的古钱币多件](../Page/战国.md "wikilink")，这证明当时滨海与中国有者频繁的贸易往来。

### 汉代

[汉朝](../Page/汉朝.md "wikilink")、[三国时这里聚居着附属于](../Page/三国.md "wikilink")[玄菟郡](../Page/玄菟郡.md "wikilink")、[辽东郡下的](../Page/辽东郡.md "wikilink")[夫余部族](../Page/夫余.md "wikilink")\[5\]，向[汉朝进贡楛](../Page/汉朝.md "wikilink")（音户）矢石砮以及[貂皮等贡品](../Page/貂皮.md "wikilink")。

### 北朝時期

[挹娄](../Page/挹娄.md "wikilink")（臣服于[夫余](../Page/夫余.md "wikilink")）向[曹魏](../Page/曹魏.md "wikilink")、[西晋进贡楛](../Page/西晋.md "wikilink")（音户）矢石砮\[6\]。[北朝时聚居在今天滨海边疆区的这里被称为勿吉的部落](../Page/北朝_\(中国朝代\).md "wikilink")，向[北魏](../Page/北魏.md "wikilink")、[东魏朝贡不断](../Page/东魏.md "wikilink")。

### 隋唐时期

[隋唐时聚居在这里的被称为](../Page/隋唐.md "wikilink")[号室靺鞨的部落](../Page/号室靺鞨.md "wikilink")（音末合），其中生活居住在辽河的巨大弯曲处的地方的粟末靺鞨（农耕为生）在[隋文帝](../Page/隋文帝.md "wikilink")[开皇年间向隋朝朝贡](../Page/开皇.md "wikilink")，[隋炀帝大业八年](../Page/隋炀帝.md "wikilink")（612年）时，曾参加隋军征伐高句丽（[隋与高句丽的战争](../Page/隋与高句丽的战争.md "wikilink")）。\[7\]

[唐太宗](../Page/唐太宗.md "wikilink")[贞观四年](../Page/貞觀_\(唐朝\).md "wikilink")（630年），[东突厥汗国被唐军打击下解体后](../Page/东突厥汗国.md "wikilink")，聚居在今滨海边疆区的被唐人称为[号室靺鞨部落的首领在贞观五年](../Page/号室靺鞨.md "wikilink")（631年）向[唐太宗朝贡从而建立了藩属性的外交关系](../Page/唐太宗.md "wikilink")。[唐玄宗](../Page/唐玄宗.md "wikilink")[先天二年](../Page/先天_\(唐朝\).md "wikilink")（713年）唐朝廷在今滨海边疆区南部及其正南设羁縻性质的忽汗州都督府（[渤海都督府](../Page/渤海都督府.md "wikilink")）。其中[率宾府驻地就是](../Page/率宾府.md "wikilink")[双城子](../Page/双城子.md "wikilink")。唐玄宗[开元十年](../Page/开元.md "wikilink")（722年），[唐玄宗任命黑水靺鞨部落首领](../Page/唐玄宗.md "wikilink")[倪属利稽为](../Page/倪属利稽.md "wikilink")[勃利](../Page/勃利.md "wikilink")（伯力）刺史，同年设黑水府。开元十一年（723年），设[黑水都督府隶属](../Page/黑水都督府.md "wikilink")[安东都护府](../Page/安东都护府.md "wikilink")，唐朝廷派官员到黑水都督府任职，官职为长史，监督都督府行政。黑水都督府、渤海都督府均隶属于安东都护府。

### 辽朝時期

[辽朝时属](../Page/辽朝.md "wikilink")[东京道五国部节度使](../Page/东京道.md "wikilink")。

### 金朝時期

[金朝时属](../Page/金朝.md "wikilink")[速频路](../Page/速频路.md "wikilink")。

### 元代

[元朝入主中原](../Page/元朝.md "wikilink")，首次划入[中原王朝版图](../Page/中原王朝.md "wikilink")，时属[辽阳等处行中书省](../Page/辽阳等处行中书省.md "wikilink")[开元路](../Page/开元路_\(元朝\).md "wikilink")、[水达达路](../Page/水达达路.md "wikilink")（由开元路分出）。

### 明代

[明朝](../Page/明朝.md "wikilink")[永乐七年](../Page/永乐_\(明朝\).md "wikilink")（1409年）5月21日设立[奴儿干都指挥使司羁縻此地](../Page/奴儿干都指挥使司.md "wikilink")。后在[明宣宗](../Page/明宣宗.md "wikilink")[宣德九年](../Page/宣德.md "wikilink")（1434年），废弃之。

### 清代

[Ussuriysk-Stone-Tortoise-S-3542.jpg](https://zh.wikipedia.org/wiki/File:Ussuriysk-Stone-Tortoise-S-3542.jpg "fig:Ussuriysk-Stone-Tortoise-S-3542.jpg")中央公園中的[贔屭](../Page/贔屭.md "wikilink")，于1868年被發現，可能是[金代墓葬遺物](../Page/金代.md "wikilink")\]\]

[奴儿干都指挥使司](../Page/奴儿干都指挥使司.md "wikilink")1434年废弃后，该地区為女真人統治，[明朝的勢力已退居](../Page/明朝.md "wikilink")[长城](../Page/长城.md "wikilink")（辽东边墙）以里，无法鞭及此地。

清代中国于1689年与[俄罗斯帝国订立](../Page/俄罗斯帝国.md "wikilink")《[尼布楚条约](../Page/尼布楚条约.md "wikilink")》，也是中国政府首次与西方国家签订的具有现代国际法水准的正式条约，确立了[中国历史上的第一条具有](../Page/中国历史.md "wikilink")[主权性质边界](../Page/主权.md "wikilink")。[外满州主权进入中国主权范围](../Page/外满州.md "wikilink")。

清代时属[吉林将军](../Page/吉林将军.md "wikilink")[三姓和](../Page/三姓副都统.md "wikilink")[宁古塔副都统辖区](../Page/宁古塔副都统.md "wikilink")。

[咸丰十年](../Page/咸丰_\(年号\).md "wikilink")（1860年），清政府与[俄罗斯帝国签订](../Page/俄罗斯帝国.md "wikilink")《[中俄北京条约](../Page/中俄北京条约.md "wikilink")》，条约将[乌苏里江以东包括](../Page/乌苏里江.md "wikilink")[库页岛在内的约](../Page/库页岛.md "wikilink")40万平方公里的领土割让给[俄国](../Page/俄国.md "wikilink")，其中包括今天的滨海边疆区。\[8\]
\[9\] 至此，[中国失去对该地区的主权](../Page/中国.md "wikilink")。

### 俄罗斯帝国时期

  - 1856年成立[滨海边疆州](../Page/滨海边疆州.md "wikilink")，1860年的管理范围扩至《中俄北京条约》割让地。

### 蘇聯時期

  - 1920年归属于[远东共和国](../Page/远东共和国.md "wikilink")（4月6日成立）
  - 1926年归属[远东边疆区](../Page/远东边疆区.md "wikilink")（1月4日成立）
  - 1938年10月20日成立滨海边疆区至今。

## 地理

滨海边疆区是俄罗斯最東南的地区，处于[北纬](../Page/北纬.md "wikilink") 42°-
48°和[东经](../Page/东经.md "wikilink")
130°-139°之间。西与[中国](../Page/中国.md "wikilink")[黑龙江](../Page/黑龙江.md "wikilink")、[吉林省接壤](../Page/吉林省.md "wikilink")，南与[朝鲜](../Page/朝鲜.md "wikilink")[罗先直辖市接壤](../Page/罗先直辖市.md "wikilink")，北与[哈巴罗夫斯克边疆区相连](../Page/哈巴罗夫斯克边疆区.md "wikilink")、东隔着[日本海与](../Page/日本海.md "wikilink")[日本远远相望](../Page/日本.md "wikilink")。滨海边疆区面积为164，673[平方公里](../Page/平方公里.md "wikilink")，占俄罗斯远东地区的5.3％\[10\]。

滨海边疆区的东部是[锡霍特山脉](../Page/锡霍特山脉.md "wikilink")，且几乎分布全境\[11\]，最高山峰是[阿尼克山](../Page/阿尼克山.md "wikilink")，高达1933米，处于滨海边疆区的北部，接近哈巴罗夫斯克边疆区。西南部是乌苏里斯克平原和布利哈卡伊斯克平原。主要河流为[乌苏里江](../Page/乌苏里江.md "wikilink")，最大湖泊为[兴凯湖](../Page/兴凯湖.md "wikilink")。在滨海边疆区一侧的乌苏里江上游的狭长平原，宽30、40公里到150公里，平原向东延伸成为1500到1700米高的高地\[12\]。这一地区森林覆盖面积为60％，木材种类多样\[13\]。

滨海边疆区的山地为褐色森林土壤，平原为褐色灰化土壤、褐色草原土壤和冲积土壤。边疆区内百分之七十的土地面积被针叶林和混合的满洲类型树木所覆盖。

## 气候

滨海边疆区的气候是[温带季风气候](../Page/温带季风气候.md "wikilink")，一月份平均温度在零下27℃～零下12℃之间，七月份平均温度在14℃～21℃之间，年平均降水量为600毫米～900毫米。

## 行政区划

**滨海边疆区**是俄罗斯联邦主体之一，下辖12个**城市區**（）和22个**市政區**（）；市政區下設**镇**（）和**乡**（）；城市區、鎮和鄉之下設**市**（）、**社區**（）和**村**（）。
[Prim-Kray-All.png](https://zh.wikipedia.org/wiki/File:Prim-Kray-All.png "fig:Prim-Kray-All.png")

轄下行政區如下：

<table>
<thead>
<tr class="header">
<th><p><strong>地圖代號</strong></p></th>
<th><p><strong>旗幟</strong></p></th>
<th><p><strong>徽章</strong></p></th>
<th><p><strong>俄語名稱</strong></p></th>
<th><p><strong>中文譯名</strong></p></th>
<th><p>'''人口 '''（2014年）[14]</p></th>
<th><p>'''面積 '''（平方公里）</p></th>
<th><p><strong>行政中心俄語名稱</strong></p></th>
<th><p><strong>行政中心中文譯名</strong></p></th>
<th><p><strong>行政中心傳統名稱</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>I</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Arseniev_(Primorsky_kray).png" title="fig:Flag_of_Arseniev_(Primorsky_kray).png">Flag_of_Arseniev_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Arseniev_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Arseniev_(Primorsky_kray).png">Coat_of_Arms_of_Arseniev_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/阿爾謝尼耶夫城市區.md" title="wikilink">阿爾謝尼耶夫城市區</a></p></td>
<td><p>54,085</p></td>
<td><p>90.74</p></td>
<td></td>
<td><p><a href="../Page/阿爾謝尼耶夫.md" title="wikilink">阿爾謝尼耶夫市</a></p></td>
<td><p>黃土坎子</p></td>
</tr>
<tr class="even">
<td><p>II</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Artyom_(Primorsky_kray).svg" title="fig:Flag_of_Artyom_(Primorsky_kray).svg">Flag_of_Artyom_(Primorsky_kray).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Artyom_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Artyom_(Primorsky_kray).png">Coat_of_Arms_of_Artyom_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/阿爾喬姆都城市區.md" title="wikilink">阿爾喬姆都城市區</a></p></td>
<td><p>111,840</p></td>
<td><p>506.4</p></td>
<td></td>
<td><p><a href="../Page/阿爾喬姆.md" title="wikilink">阿爾喬姆市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>III</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Vladivostok_(Primorsky_kray).png" title="fig:Flag_of_Vladivostok_(Primorsky_kray).png">Flag_of_Vladivostok_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Vladivostok_2014.jpg" title="fig:Coat_of_arms_of_Vladivostok_2014.jpg">Coat_of_arms_of_Vladivostok_2014.jpg</a></p></td>
<td></td>
<td><p><a href="../Page/符拉迪沃斯托克城市區.md" title="wikilink">符拉迪沃斯托克城市區</a></p></td>
<td><p>630,027</p></td>
<td><p>560</p></td>
<td></td>
<td><p><a href="../Page/海参崴.md" title="wikilink">符拉迪沃斯托克市</a></p></td>
<td><p><a href="../Page/海参崴.md" title="wikilink">海参崴</a></p></td>
</tr>
<tr class="even">
<td><p>IV</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png">Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/達利涅戈爾斯克城市區.md" title="wikilink">達利涅戈爾斯克城市區</a></p></td>
<td><p>44,446</p></td>
<td><p>5,300</p></td>
<td></td>
<td><p><a href="../Page/達利涅戈爾斯克.md" title="wikilink">達利涅戈爾斯克市</a></p></td>
<td><p>Те́тюхе 野豬河</p></td>
</tr>
<tr class="odd">
<td><p>V</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Dalnerechensk_(Primorsky_kray).png" title="fig:Flag_of_Dalnerechensk_(Primorsky_kray).png">Flag_of_Dalnerechensk_(Primorsky_kray).png</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/達利涅列琴斯克城市區.md" title="wikilink">達利涅列琴斯克城市區</a></p></td>
<td><p>29,516</p></td>
<td><p>100</p></td>
<td></td>
<td><p><a href="../Page/達利涅列琴斯克.md" title="wikilink">達利涅列琴斯克市</a></p></td>
<td><p>Иман 伊曼</p></td>
</tr>
<tr class="even">
<td><p>VI</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Lesozavodsk_(Primorsky_kray)_(2006).png" title="fig:Coat_of_Arms_of_Lesozavodsk_(Primorsky_kray)_(2006).png">Coat_of_Arms_of_Lesozavodsk_(Primorsky_kray)_(2006).png</a></p></td>
<td></td>
<td><p><a href="../Page/列索扎沃茨克城市區.md" title="wikilink">列索扎沃茨克城市區</a></p></td>
<td><p>44,643</p></td>
<td><p>3,100</p></td>
<td></td>
<td><p><a href="../Page/列索扎沃茨克.md" title="wikilink">列索扎沃茨克市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>VII</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Nakhodka_(Primorsky_krai).png{{!}}border" title="fig:Flag_of_Nakhodka_(Primorsky_krai).png{{!}}border">Flag_of_Nakhodka_(Primorsky_krai).png{{!}}border</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Nakhodka_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Nakhodka_(Primorsky_kray).png">Coat_of_Arms_of_Nakhodka_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/納霍德卡城市區.md" title="wikilink">納霍德卡城市區</a></p></td>
<td><p>157,397</p></td>
<td><p>300</p></td>
<td></td>
<td><p><a href="../Page/納霍德卡.md" title="wikilink">納霍德卡市</a></p></td>
<td><p>灠溝崴</p></td>
</tr>
<tr class="even">
<td><p>VIII</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Partizansk_(Primorsky_kray).png" title="fig:Flag_of_Partizansk_(Primorsky_kray).png">Flag_of_Partizansk_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png">Coat_of_Arms_of_Dalnegorsk_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/游擊隊城城市區.md" title="wikilink">游擊隊城城市區</a></p></td>
<td><p>45,646</p></td>
<td><p>1,300</p></td>
<td></td>
<td><p><a href="../Page/游擊隊城.md" title="wikilink">游擊隊城市</a></p></td>
<td><p>Сучан 蘇城 / 蘇昌</p></td>
</tr>
<tr class="odd">
<td><p>IX</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/斯帕斯克達利尼城市區.md" title="wikilink">斯帕斯克達利尼城市區</a></p></td>
<td><p>42,491</p></td>
<td><p>50</p></td>
<td></td>
<td><p><a href="../Page/斯帕斯克達利尼.md" title="wikilink">斯帕斯克達利尼市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>X</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Ussuriysk_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Ussuriysk_(Primorsky_kray).png">Coat_of_Arms_of_Ussuriysk_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/烏蘇里斯克城市區.md" title="wikilink">烏蘇里斯克城市區</a></p></td>
<td><p>192,844</p></td>
<td><p>3,600</p></td>
<td></td>
<td><p><a href="../Page/雙城子.md" title="wikilink">烏蘇里斯克市</a></p></td>
<td><p><a href="../Page/雙城子.md" title="wikilink">雙城子</a></p></td>
</tr>
<tr class="odd">
<td><p>XI</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Bolshoy_Kamen_(Primorsky_kray).png" title="fig:Flag_of_Bolshoy_Kamen_(Primorsky_kray).png">Flag_of_Bolshoy_Kamen_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Bolshoy_Kamen_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Bolshoy_Kamen_(Primorsky_kray).png">Coat_of_Arms_of_Bolshoy_Kamen_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/大卡緬城市區.md" title="wikilink">大卡緬城市區</a></p></td>
<td><p>40,393</p></td>
<td><p>120</p></td>
<td></td>
<td><p><a href="../Page/大卡緬.md" title="wikilink">大卡緬市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>XII</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Fokino_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Fokino_(Primorsky_kray).png">Coat_of_Arms_of_Fokino_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/福基諾封閉城市區.md" title="wikilink">福基諾封閉城市區</a></p></td>
<td><p>31,309</p></td>
<td><p>260</p></td>
<td></td>
<td><p><a href="../Page/福基諾_(濱海邊疆區).md" title="wikilink">福基諾市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>01</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Anuchinsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Anuchinsky_rayon_(Primorsky_kray).png">Flag_of_Anuchinsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Anuchinsky_rayon_(Primorye_krai).png" title="fig:Coat_of_Arms_of_Anuchinsky_rayon_(Primorye_krai).png">Coat_of_Arms_of_Anuchinsky_rayon_(Primorye_krai).png</a></p></td>
<td></td>
<td><p><a href="../Page/亞努琴斯基區.md" title="wikilink">亞努琴斯基區</a></p></td>
<td><p>13,680</p></td>
<td><p>3,800</p></td>
<td></td>
<td><p><a href="../Page/亞努欽諾村.md" title="wikilink">亞努欽諾村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>02</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Dalnerechensky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Dalnerechensky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Dalnerechensky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/達利涅列琴斯基區.md" title="wikilink">達利涅列琴斯基區</a></p></td>
<td><p>10,405</p></td>
<td><p>7,300</p></td>
<td><p>г. Дальнереченск</p></td>
<td><p>達利涅列琴斯克市[15]</p></td>
<td><p>Иман 伊曼</p></td>
</tr>
<tr class="odd">
<td><p>03</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kavalerovsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Kavalerovsky_rayon_(Primorsky_kray).png">Flag_of_Kavalerovsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gerb-Kavalerovsky-region.gif" title="fig:Gerb-Kavalerovsky-region.gif">Gerb-Kavalerovsky-region.gif</a></p></td>
<td></td>
<td><p><a href="../Page/卡瓦列洛夫斯基區.md" title="wikilink">卡瓦列洛夫斯基區</a></p></td>
<td><p>24,986</p></td>
<td><p>4,200</p></td>
<td></td>
<td><p><a href="../Page/卡瓦列羅沃社區.md" title="wikilink">卡瓦列羅沃社區</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>04</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Kirovsky_rayon_(Primorye_krai).PNG" title="fig:Coat_of_Arms_of_Kirovsky_rayon_(Primorye_krai).PNG">Coat_of_Arms_of_Kirovsky_rayon_(Primorye_krai).PNG</a></p></td>
<td></td>
<td><p><a href="../Page/基洛夫斯基區（濱海邊疆區）.md" title="wikilink">基洛夫斯基區</a></p></td>
<td><p>19,926</p></td>
<td><p>3,400</p></td>
<td></td>
<td><p><a href="../Page/基洛夫斯基社區.md" title="wikilink">基洛夫斯基社區</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>05</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Krasnoarmeysky_rayon_(Primorye_krai).png" title="fig:Flag_of_Krasnoarmeysky_rayon_(Primorye_krai).png">Flag_of_Krasnoarmeysky_rayon_(Primorye_krai).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Krasnoarmeysky_rayon_(Primorye_krai).png" title="fig:Coat_of_Arms_of_Krasnoarmeysky_rayon_(Primorye_krai).png">Coat_of_Arms_of_Krasnoarmeysky_rayon_(Primorye_krai).png</a></p></td>
<td></td>
<td><p><a href="../Page/克拉斯諾雅爾梅斯基區.md" title="wikilink">克拉斯諾雅爾梅斯基區</a></p></td>
<td><p>17,452</p></td>
<td><p>20,700</p></td>
<td></td>
<td><p><a href="../Page/Новопокровка村.md" title="wikilink">Новопокровка村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>06</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Lazovsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Lazovsky_rayon_(Primorsky_kray).png">Flag_of_Lazovsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/拉佐夫斯基區.md" title="wikilink">拉佐夫斯基區</a></p></td>
<td><p>13,424</p></td>
<td><p>4,700</p></td>
<td></td>
<td><p><a href="../Page/拉佐村.md" title="wikilink">拉佐村</a></p></td>
<td><p>Вангоу 灣溝</p></td>
</tr>
<tr class="odd">
<td><p>07</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Mihailovsky_rayon_(Primorsky_Krai).PNG" title="fig:Flag_of_Mihailovsky_rayon_(Primorsky_Krai).PNG">Flag_of_Mihailovsky_rayon_(Primorsky_Krai).PNG</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Mikhaylovsky_rayon_(Primorsky_kray)_(2006).png" title="fig:Coat_of_Arms_of_Mikhaylovsky_rayon_(Primorsky_kray)_(2006).png">Coat_of_Arms_of_Mikhaylovsky_rayon_(Primorsky_kray)_(2006).png</a></p></td>
<td></td>
<td><p><a href="../Page/米哈伊洛夫斯基區.md" title="wikilink">米哈伊洛夫斯基區</a></p></td>
<td><p>32,216</p></td>
<td><p>2,700</p></td>
<td></td>
<td><p><a href="../Page/米哈伊洛夫卡村.md" title="wikilink">米哈伊洛夫卡村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>08</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Nadezhdinsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Nadezhdinsky_rayon_(Primorsky_kray).png">Flag_of_Nadezhdinsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Nadezhdinsky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Nadezhdinsky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Nadezhdinsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/納德日丁斯基區.md" title="wikilink">納德日丁斯基區</a></p></td>
<td><p>38,232</p></td>
<td><p>1,600</p></td>
<td></td>
<td><p><a href="../Page/Вольно-Надеждинское村.md" title="wikilink">Вольно-Надеждинское村</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>09</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Oktyabrsky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Oktyabrsky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Oktyabrsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/奧克提亞布爾斯基區.md" title="wikilink">奧克提亞布爾斯基區</a></p></td>
<td><p>28,425</p></td>
<td><p>1,700</p></td>
<td></td>
<td><p><a href="../Page/Покровка_村.md" title="wikilink">Покровка 村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Olginsky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Olginsky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Olginsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/奧爾金斯基區.md" title="wikilink">奧爾金斯基區</a></p></td>
<td><p>10,106</p></td>
<td><p>6,400</p></td>
<td></td>
<td><p><a href="../Page/奧加_(俄國).md" title="wikilink">奧莉加社區</a></p></td>
<td><p>安州</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Partizanskii_rayon_coa.gif" title="fig:Partizanskii_rayon_coa.gif">Partizanskii_rayon_coa.gif</a></p></td>
<td></td>
<td><p><a href="../Page/帕爾提扎斯基區.md" title="wikilink">帕爾提扎斯基區</a></p></td>
<td><p>29,683</p></td>
<td><p>4,300</p></td>
<td></td>
<td><p><a href="../Page/Владимиро-Александровское村.md" title="wikilink">Владимиро-Александровское村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Pogranichny_rayon_(Primorsky_kray).png" title="fig:Flag_of_Pogranichny_rayon_(Primorsky_kray).png">Flag_of_Pogranichny_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Pogranichny_rayon_(Primorye_krai).png" title="fig:Coat_of_Arms_of_Pogranichny_rayon_(Primorye_krai).png">Coat_of_Arms_of_Pogranichny_rayon_(Primorye_krai).png</a></p></td>
<td></td>
<td><p><a href="../Page/伯格拉尼奇斯基區.md" title="wikilink">伯格拉尼奇斯基區</a></p></td>
<td><p>22,885</p></td>
<td><p>3,700</p></td>
<td></td>
<td><p><a href="../Page/波格拉尼奇內社區.md" title="wikilink">波格拉尼奇內社區</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Pozharsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Pozharsky_rayon_(Primorsky_kray).png">Flag_of_Pozharsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Pozharsky_District_(new,_2009).png" title="fig:Coat_of_arms_of_Pozharsky_District_(new,_2009).png">Coat_of_arms_of_Pozharsky_District_(new,_2009).png</a></p></td>
<td></td>
<td><p><a href="../Page/波扎爾斯基區.md" title="wikilink">波扎爾斯基區</a></p></td>
<td><p>29,547</p></td>
<td><p>22,700</p></td>
<td></td>
<td><p><a href="../Page/盧切戈爾斯克.md" title="wikilink">盧切戈爾斯克社區</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Spassky_rayon_(Primorsky_krai).svg" title="fig:Flag_of_Spassky_rayon_(Primorsky_krai).svg">Flag_of_Spassky_rayon_(Primorsky_krai).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Spassk_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Spassk_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Spassk_rayon_(Primorsky_kray).png</a></p></td>
<td><p>[:ru:Спасский район (Приморский край)|Спасский муниципальный район]]</p></td>
<td><p><a href="../Page/斯帕斯斯基區.md" title="wikilink">斯帕斯斯基區</a></p></td>
<td><p>28,943</p></td>
<td><p>4,100</p></td>
<td><p>г. Спасск-Дальний</p></td>
<td><p>斯帕斯克達利尼市[16]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/特爾涅伊斯基區.md" title="wikilink">特爾涅伊斯基區</a></p></td>
<td><p>11,796</p></td>
<td><p>27,700</p></td>
<td><p>пгт. </p></td>
<td><p><a href="../Page/捷爾涅伊社區.md" title="wikilink">捷爾涅伊社區</a></p></td>
<td><p>大圍子</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Hankaisky_rayon_(Primorsky_kray)_proposal.png" title="fig:Coat_of_Arms_of_Hankaisky_rayon_(Primorsky_kray)_proposal.png">Coat_of_Arms_of_Hankaisky_rayon_(Primorsky_kray)_proposal.png</a></p></td>
<td></td>
<td><p><a href="../Page/哈卡伊斯基區.md" title="wikilink">哈卡伊斯基區</a></p></td>
<td><p>23,287</p></td>
<td><p>2,700</p></td>
<td></td>
<td><p><a href="../Page/卡緬雷博洛夫村.md" title="wikilink">卡緬雷博洛夫村</a></p></td>
<td><p>紅土岩</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Khasansky_rayon_(Primorsky_krai).png" title="fig:Flag_of_Khasansky_rayon_(Primorsky_krai).png">Flag_of_Khasansky_rayon_(Primorsky_krai).png</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/哈桑斯基縣.md" title="wikilink">哈桑斯基區</a></p></td>
<td><p>33,167</p></td>
<td><p>4,100</p></td>
<td></td>
<td><p><a href="../Page/斯拉维扬卡.md" title="wikilink">斯拉維揚卡社區</a></p></td>
<td><p>土拉子</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Khorolsky_rayon_(Primorsky_krai).png" title="fig:Flag_of_Khorolsky_rayon_(Primorsky_krai).png">Flag_of_Khorolsky_rayon_(Primorsky_krai).png</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/霍洛爾斯基區.md" title="wikilink">霍洛爾斯基區</a></p></td>
<td><p>28,610</p></td>
<td><p>1,970</p></td>
<td><p>с. </p></td>
<td><p><a href="../Page/霍洛爾村.md" title="wikilink">霍洛爾村</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Chernigovsky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Chernigovsky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Chernigovsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/切爾尼果夫斯基區.md" title="wikilink">切爾尼果夫斯基區</a></p></td>
<td><p>34,418</p></td>
<td><p>1,800</p></td>
<td><p>с. </p></td>
<td><p><a href="../Page/切爾尼果夫卡村.md" title="wikilink">切爾尼果夫卡村</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Chuguyevsky_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Chuguyevsky_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Chuguyevsky_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/楚古耶夫斯基區.md" title="wikilink">楚古耶夫斯基區</a></p></td>
<td><p>23,335</p></td>
<td><p>12,300</p></td>
<td><p>с. </p></td>
<td><p><a href="../Page/楚古耶夫卡村.md" title="wikilink">楚古耶夫卡村</a></p></td>
<td><p>頭道溝</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Shkotovsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Shkotovsky_rayon_(Primorsky_kray).png">Flag_of_Shkotovsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Shkotovsky_rayon_(Primorye_krai).png" title="fig:Coat_of_Arms_of_Shkotovsky_rayon_(Primorye_krai).png">Coat_of_Arms_of_Shkotovsky_rayon_(Primorye_krai).png</a></p></td>
<td></td>
<td><p><a href="../Page/斯科托夫斯基區.md" title="wikilink">斯科托夫斯基區</a></p></td>
<td><p>24,233</p></td>
<td><p>3,300</p></td>
<td><p>пгт. </p></td>
<td><p><a href="../Page/斯莫科亞尼諾沃社區.md" title="wikilink">斯莫科亞尼諾沃社區</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Yakovlevsky_rayon_(Primorsky_kray).png" title="fig:Flag_of_Yakovlevsky_rayon_(Primorsky_kray).png">Flag_of_Yakovlevsky_rayon_(Primorsky_kray).png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Yakovlevo_rayon_(Primorsky_kray).png" title="fig:Coat_of_Arms_of_Yakovlevo_rayon_(Primorsky_kray).png">Coat_of_Arms_of_Yakovlevo_rayon_(Primorsky_kray).png</a></p></td>
<td></td>
<td><p><a href="../Page/雅科夫列夫斯基區.md" title="wikilink">雅科夫列夫斯基區</a></p></td>
<td><p>15,123</p></td>
<td><p>2,400</p></td>
<td><p>с. </p></td>
<td><p><a href="../Page/雅科夫列夫卡村.md" title="wikilink">雅科夫列夫卡村</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 人口民族

滨海边疆区在2015年人口为1,933,308\[17\]。

滨海边疆区的民族构成分外来民族和[原住民](../Page/原住民.md "wikilink")，2010年時分别占99%和1%。

外来民族的人口及比例如下：

  - [俄罗斯族](../Page/俄羅斯人.md "wikilink")——1,675,992（85.66％）
  - 無指明種族——144,927（7.41％）
  - [乌克兰族](../Page/乌克兰族.md "wikilink")——49,953（2.55％）
  - [朝鮮族](../Page/朝鲜族.md "wikilink")——18,824（0.96％）
  - [鞑靼族](../Page/鞑靼人.md "wikilink")——10,640（0.54％）
  - [烏茲別克族](../Page/烏茲別克族.md "wikilink")——8,993（0.46％）
  - [白俄罗斯族](../Page/白俄罗斯族.md "wikilink")——5,930（0.30％）
  - [亞美尼亞族](../Page/亚美尼亚人.md "wikilink")——5,924（0.30％）
  - [阿塞拜疆族](../Page/阿塞拜疆族.md "wikilink")——3,937（0.20％）
  - [華人](../Page/华人.md "wikilink")——2,857（0.15％）
  - [莫爾多瓦人](../Page/莫尔多瓦人.md "wikilink")——2,223（0.11％）
  - [德意志人](../Page/德意志人.md "wikilink")——2087（0.10％）
  - [楚瓦什人](../Page/楚瓦什人.md "wikilink")——1960 (0,10 %)

原住民的人口及比例如下：

  - [烏德蓋人](../Page/乌德盖人.md "wikilink")——793（0.04％）
  - [赫哲族](../Page/赫哲族.md "wikilink")——383 (0,01 %)
  - [塔茲族](../Page/塔兹族.md "wikilink")——253 (0,01 %)

## 注释

## 参考资料

## 参考书籍

  - 斯图尔特·柯尔比《苏联的远东地区》、上海师范大学历史系、地理系翻译、上海人民出版社、内部发行、1976年、统一书号 12171.12

## 外部链接

  - [官方网站](http://www.primorsky.ru/)

{{-}}{{-}}  {{-}}

[Category:遠東聯邦管區](../Category/遠東聯邦管區.md "wikilink")
[\*](../Category/濱海邊疆區.md "wikilink")
[Primorsky](../Category/1938年設立的行政區劃.md "wikilink")
[Category:1938年蘇聯建立](../Category/1938年蘇聯建立.md "wikilink")

1.  《今本竹书纪年》载舜帝２５年时“息慎来朝，贡弓矢”。

2.  《国语.鲁语下》载“昔武王克商，通道于九夷百蛮，使各以其方贿来贡，使无忘职业。于是肃慎氏贡矢石砮”。

3.  《逸周书·王会篇》记载，周成王大会诸侯时，肃慎人曾献上“大鹿”。

4.  《左传.昭公九年》载:“肃慎、燕、毫,吾北土也。”

5.  详见《三国志30卷》

6.  详见《晋书》卷3

7.  《[隋书](../Page/隋书.md "wikilink")》卷八十一

8.  [关于中俄边界问题的联合声明](http://www.cctv.com/special/903/-1/70031.html)

9.  [中俄交换边界东段补充协定批准书
    边界走向确定](http://news.sina.com.cn/c/2005-06-02/13166060882s.shtml)

10. 柯尔比，上海师范大学译 pp.150

11.
12. 柯尔比，上海师范大学译 pp.153

13. 柯尔比，上海师范大学译 pp.154

14. ЧИСЛЕННОСТЬ НАСЕЛЕНИЯ РОССИЙСКОЙ ФЕДЕРАЦИИ по муниципальным
    образованиям на 1 января 2014
    года[1](http://www.gks.ru/wps/wcm/connect/rosstat_main/rosstat/ru/statistics/publications/catalog/afc8ea004d56a39ab251f2bafc3a6fce)

15. 行政中心不在轄區內。

16.
17.