**滴道区**是[黑龙江省](../Page/黑龙江省.md "wikilink")[鸡西市下辖的一个区](../Page/鸡西.md "wikilink")。

## 地理

滴道区位于鸡西市的西北角，与[七台河相邻](../Page/七台河.md "wikilink")，离鸡西市中心约10千米。

## 行政区划

下辖4个街道、2个乡。\[1\] 。

## 经济

在矿产中尤其以[煤矿为主](../Page/煤矿.md "wikilink")，农业以畜牧业为主。

## 交通

[哈密铁路和](../Page/哈密铁路.md "wikilink")[201国道通过滴道区](../Page/201国道.md "wikilink")。

## 参考资料

## 外部链接

  - [鸡西市滴道区人民政府网](http://www.didaoqu.gov.cn/)

[Category:鸡西区县市](../Category/鸡西区县市.md "wikilink")
[鸡西](../Category/黑龙江市辖区.md "wikilink")

1.