[500greatestalbumsofalltime.JPG](https://zh.wikipedia.org/wiki/File:500greatestalbumsofalltime.JPG "fig:500greatestalbumsofalltime.JPG")
**滚石杂志五百大专辑**或**史上最伟大的500张专辑**（）是美国《[滚石](../Page/滚石_\(杂志\).md "wikilink")》杂志的一期特刊，发行于2003年11月，相关书籍发行于2005年<ref name="RS500">
**Related news articles:**

  -
  - </ref>。该榜单由273位摇滚音乐人、乐评人和业内人士投票评选，每人提交一份加权的50张专辑列表。《滚石》还在一年后评选了类似榜单“[史上最伟大的500首歌曲](../Page/史上最伟大的500首歌曲.md "wikilink")”。2012年，该榜单得以更新，加入了2000年代发行的专辑，收录了38张此前未入选的专辑\[1\]。

## 统计

  - [披头士的专辑](../Page/披头士.md "wikilink")《[Sgt. Pepper's Lonely Hearts
    Club
    Band](../Page/Sgt._Pepper's_Lonely_Hearts_Club_Band.md "wikilink")》被选为第一位，该专辑发行于1967年。
  - 拥有最多入榜专辑的艺人为[鲍勃·迪伦](../Page/鲍勃·迪伦.md "wikilink")（11张，2张进入前十位），紧随其后的为披头士（10张，4张进入前十位，不包括解散后成员的个人专辑），[滾石合唱團](../Page/滾石合唱團.md "wikilink")（10张，1张进入前十位），[布魯斯·斯普林斯汀](../Page/布魯斯·斯普林斯汀.md "wikilink")（8张），[艾瑞克·克萊普頓](../Page/艾瑞克·克萊普頓.md "wikilink")（8张，包括2张个人专辑，3张[奶油乐队专辑](../Page/奶油乐队.md "wikilink")，1张专辑，1张[雏鸟乐队专辑](../Page/雏鸟乐队.md "wikilink")），[尼尔·杨](../Page/尼尔·杨.md "wikilink")（7张，包括5张个人专辑，1张[水牛春田合唱团专辑和](../Page/水牛春田合唱团.md "wikilink")1张[克罗斯比、史提尔斯、纳许与尼尔·杨专辑](../Page/克罗斯比、史提尔斯、纳许与尼尔·杨.md "wikilink")），[誰人樂團](../Page/誰人樂團.md "wikilink")（7张），[卢·里德](../Page/卢·里德.md "wikilink")（6张，包括2张个人专辑，4张[地下丝绒专辑](../Page/地下丝绒.md "wikilink")），[大衛·鮑伊](../Page/大衛·鮑伊.md "wikilink")（5张），[艾爾頓·约翰](../Page/艾爾頓·约翰.md "wikilink")（5张），[保羅·西蒙](../Page/保羅·西蒙.md "wikilink")（5张，包括2张个人专辑，3张[賽門與葛芬柯专辑](../Page/賽門與葛芬柯.md "wikilink")），[U2樂團](../Page/U2樂團.md "wikilink")（5张），[鲍勃·馬利與哭泣者乐队](../Page/鲍勃·馬利與哭泣者乐队.md "wikilink")（5张），[齊柏林飛艇乐队](../Page/齊柏林飛艇乐队.md "wikilink")（5张），[電台司令](../Page/電台司令.md "wikilink")（5张）\[2\]。

## 争议

报纸《[今日美國](../Page/今日美國.md "wikilink")》上刊登的一文称该榜单老套无惊喜，偏向充满睾酮的老派摇滚\[3\]。它因过时，过于男性主导，英美两国主导而遭到批评\[4\]\[5\]。

## 参考资料

## 外部链接

  - [《滚石》杂志官网上的完整榜单](http://www.rollingstone.com/music/lists/500-greatest-albums-of-all-time-20120531)

[Category:音樂專輯列表](../Category/音樂專輯列表.md "wikilink")
[Category:滾石雜誌](../Category/滾石雜誌.md "wikilink")
[Category:排名表](../Category/排名表.md "wikilink")

1.
2.
3.  ["It's Certainly a Thrill: Sgt. Pepper Is Best
    Album"](http://www.usatoday.com/life/music/news/2003-11-16-rolling-stone-list_x.htm),
    *USA Today*, November 17, 2003.
4.  Biron, Dean. 2011. Towards a Popular Music Criticism of
    Replenishment. *Popular Music & Society*, 34/5: 661-682.
5.  Schmutz, Vaughan. 2005. Retrospective Critical Consecration in
    Popular Music: Rolling Stone's Greatest Albums of All Time.
    *American Behavioral Scientist*, 48/11: 1510-1523.