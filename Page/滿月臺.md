**滿月臺**（**만월대**），位於[朝鮮民主主義人民共和國的](../Page/朝鮮民主主義人民共和國.md "wikilink")[開城工業地區之市中心區的西北之](../Page/開城工業地區.md "wikilink")[松岳山南麓](../Page/松岳山.md "wikilink")，它是[高麗時代的](../Page/高麗時代.md "wikilink")[王宮遺跡](../Page/王宮.md "wikilink")，佔地約1,250,000平方公呎，現時僅存宮址及礎石。

## 建築

建造[王宮時](../Page/王宮.md "wikilink")，先築起高臺，高臺上再蓋了建築物，許多類似的建築物組成一個統一的建築群落，建築物的屋頂部分層層疊疊，對稱的效果，顯得這王宮更宏偉壯麗。

王宮城牆的正門在東邊，稱為[光華門](../Page/光華門.md "wikilink")。從光華門往西走一段路，向北拐，就看見有兩層城樓的[昇平門](../Page/昇平門.md "wikilink")，它的左右兩旁各有[亭樓](../Page/亭樓.md "wikilink")。

昇平門和其北方的[神鳳門之間](../Page/神鳳門.md "wikilink")，是[球庭](../Page/球庭.md "wikilink")。[高麗王曾在這裡閱兵及觀看擊球比賽](../Page/高麗君主列表.md "wikilink")。
從[球庭徑直走](../Page/球庭.md "wikilink")，就依次有[第二大門及](../Page/第二大門.md "wikilink")[第三大門](../Page/第三大門.md "wikilink")，走過這兩扇大門，就看見一片寬廣的廣場。這個廣場上曾有過砌有4條寬大岩石階梯的高臺，滿月臺的主建築就是坐落在這個高臺上。
高臺上聳立的主建築群之西方，是高麗王日常處理政事和起居的房間。

## 現況

在1950年代的[韓戰](../Page/韓戰.md "wikilink")（北韓稱：祖國解放戰爭），攻入[北韓的](../Page/北韓.md "wikilink")[美國軍隊炸毀了這個歷史遺跡](../Page/美國.md "wikilink")，現時只剩滿月臺的礎石。然而，位於首都[平壤市的](../Page/平壤.md "wikilink")[朝鮮中央歷史博物館及位於](../Page/朝鮮中央歷史博物館.md "wikilink")[開城工業地區](../Page/開城工業地區.md "wikilink")
的[成均館](../Page/成均館.md "wikilink")，都擁有是[高麗時代的](../Page/高麗時代.md "wikilink")[王宮之模型](../Page/王宮.md "wikilink")，供[觀光客欣賞](../Page/觀光客.md "wikilink")。

2013年6月23日聯合國教科文組織在柬埔寨舉行的第37屆世界遺產大會，公佈開城歷史古蹟包括開城區、滿月台、開城南大門、成均館、嵩陽書院、王建皇陵、恭愍皇陵列入世界文化遺產。

## 鄰近的觀光熱點

  - [瞻星臺](../Page/瞻星臺.md "wikilink")：[高麗時代的](../Page/高麗時代.md "wikilink")[天文臺](../Page/天文臺.md "wikilink")

[Category:朝鲜民主主义人民共和国国宝](../Category/朝鲜民主主义人民共和国国宝.md "wikilink")
[Category:朝鮮民主主義人民共和國建築](../Category/朝鮮民主主義人民共和國建築.md "wikilink")
[Category:朝鮮民主主義人民共和國旅遊景點](../Category/朝鮮民主主義人民共和國旅遊景點.md "wikilink")
[Category:高麗王室建築](../Category/高麗王室建築.md "wikilink")
[Category:黃海北道建築](../Category/黃海北道建築.md "wikilink")
[Category:朝鲜民主主义人民共和国世界遗产](../Category/朝鲜民主主义人民共和国世界遗产.md "wikilink")
[Category:開城特級市建築物](../Category/開城特級市建築物.md "wikilink")