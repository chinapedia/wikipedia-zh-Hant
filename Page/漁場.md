**漁場**（）\[1\]是鱼类、贝类等水生动物丰富的[水域](../Page/水域.md "wikilink")。

## 世界大型漁場

[AYool_SEAWIFS_annual.png](https://zh.wikipedia.org/wiki/File:AYool_SEAWIFS_annual.png "fig:AYool_SEAWIFS_annual.png")
世界大型渔场通常分布在[大陆架宽阔且有](../Page/大陆架.md "wikilink")[寒流和](../Page/寒流.md "wikilink")[暖流汇合的地方](../Page/暖流.md "wikilink")，[日本渔场](../Page/日本渔场.md "wikilink")、[纽芬兰渔场和](../Page/纽芬兰渔场.md "wikilink")[北海渔场是世界三大渔场](../Page/北海渔场.md "wikilink")。[秘鲁渔场也是世界大型渔场](../Page/秘鲁渔场.md "wikilink")，不过这里只有[秘鲁寒流经过](../Page/秘鲁寒流.md "wikilink")。中国最大的渔场是[舟山渔场](../Page/舟山渔场.md "wikilink")，处于[黑潮和东海](../Page/黑潮.md "wikilink")[沿岸流](../Page/沿岸流.md "wikilink")（寒流）的交界處。
海洋渔场的分布与海洋中[浮游植物的光合作用生产的有机碳的量](../Page/浮游植物.md "wikilink")（称为“[初级生产量](../Page/初级生产量.md "wikilink")”）直接相关。根据[海洋生态学的研究](../Page/海洋生态学.md "wikilink")，海洋浮游植物的光合作用受日光强度、海水中营养物质包括氮、磷、硅酸以及铁的浓度的制约。因此热带亚热带大洋往往由于营养素的贫乏，所以初级生产量有限，不能成为渔场。而在[寒流流经的海域](../Page/寒流.md "wikilink")，由于寒流从南北极区海域带来丰富的营养物质，所以所流经之处的光合作用的初级生产量显著高于大洋。此外，近岸特别是河流入海口也有丰富的营养物质，因此海域初级生产量很高，成为渔场。这也能解释全世界最著名的渔场如北大西洋特别是北海渔场、纽芬兰渔场，西北太平洋包括白令海渔场、鄂霍茨克海渔场，以及[南大洋的](../Page/南大洋.md "wikilink")[磷虾为何占了海洋渔业资源的主要部分](../Page/磷虾.md "wikilink")。

## 參考來源

## 相關

  -
  - 新生魚（），指漁場補充魚群。

[Category:漁業](../Category/漁業.md "wikilink")
[\*](../Category/漁場.md "wikilink")

1.  [fishing ground - 漁場](http://terms.naer.edu.tw/detail/968901/) -
    [國家教育研究院](../Page/國家教育研究院.md "wikilink")