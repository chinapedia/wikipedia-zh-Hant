[Fisherman's_friend_logo_2019.jpg.png](https://zh.wikipedia.org/wiki/File:Fisherman's_friend_logo_2019.jpg.png "fig:Fisherman's_friend_logo_2019.jpg.png")
[Fisherman's_Friend_diverse_Sorten-6341.jpg](https://zh.wikipedia.org/wiki/File:Fisherman's_Friend_diverse_Sorten-6341.jpg "fig:Fisherman's_Friend_diverse_Sorten-6341.jpg")
**漁夫之寶**（）是[英國](../Page/英國.md "wikilink")[蘭開夏郡](../Page/蘭開夏.md "wikilink")[弗利特伍德市生產的強勁薄荷](../Page/弗利特伍德.md "wikilink")[喉糖](../Page/喉片.md "wikilink")。

## 歷史

漁夫之寶最初由名叫James
Lofthouse的[藥劑師於](../Page/藥劑師.md "wikilink")1865年硏製以減輕漁夫在[冰島捕魚地點的惡劣環境所引起的各種呼吸和暈船問題](../Page/冰島.md "wikilink")。原為包含[薄荷與](../Page/薄荷.md "wikilink")[桉樹油的葯水](../Page/桉樹.md "wikilink")，Lofthouse將這種液體製成喉糖令其更易運送與管理。很快地漁夫開始將這種喉糖比喻為他們的「朋友（friends）」，這亦是其名字的由來。該喉糖由創立至今沒有任何改變，包裝亦為當時使用的紙包裝，不過現已在包裝紙內加入銀箔。

## 口味選擇

漁夫之寶現於超過100個國家推出，並擁有多種口味，其中一些只在特定地區發售。包裝上有條紋的為無糖口味：

  - 特強原味
  - [八角](../Page/八角.md "wikilink")
  - 特強薄荷
  - 無糖(使用[山梨糖醇](../Page/山梨糖醇.md "wikilink"))
      - 原味
      - [柑橘](../Page/柑橘.md "wikilink")
      - [青檸](../Page/青檸.md "wikilink")
      - [檸檬](../Page/檸檬.md "wikilink")
      - [薄荷](../Page/薄荷.md "wikilink")
      - [蘋果與](../Page/蘋果.md "wikilink")[肉桂](../Page/肉桂.md "wikilink")
      - 強勁[Salmiak](../Page/Salmiakki.md "wikilink")
      - [櫻桃](../Page/櫻桃.md "wikilink")
      - 辣[橘](../Page/橘.md "wikilink")

該製造商共獲得三次[英女王出口產業成就獎](../Page/英女王企業獎.md "wikilink")。

由中国上海市大昌洋行总经销的25克庄渔夫之宝，以铁盒包装，上有轮船图和渔夫之宝字样。

## 參考文獻

## 外部連結

  - [官方網站](http://www.fishermansfriend.com)

[Category:薄荷糖](../Category/薄荷糖.md "wikilink")
[Category:止咳药片](../Category/止咳药片.md "wikilink")
[Category:1865年面世](../Category/1865年面世.md "wikilink")
[Category:英國品牌](../Category/英國品牌.md "wikilink")