**漂泊甲龍屬**（[學名](../Page/學名.md "wikilink")：*Aletopelta*）是種[鳥臀目](../Page/鳥臀目.md "wikilink")[甲龍下目](../Page/甲龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，化石發現於[美國](../Page/美國.md "wikilink")[加州南部](../Page/加州.md "wikilink")。

屬名在[古希臘文意為](../Page/古希臘文.md "wikilink")「漂泊的盾甲」，因為[本·卡斯勒](../Page/本·卡斯勒.md "wikilink")（Ben
Creisler）提出這些化石因為[半島山脈地體北上移動](../Page/半島山脈地體.md "wikilink")，而被帶離原本的地點，原始位置相當於[墨西哥中部的西側](../Page/墨西哥.md "wikilink")。種名則是以[古生物學家Walter](../Page/古生物學家.md "wikilink")
P. Coombs, Jr.為名，以紀念他多年來對於甲龍類的研究貢獻。

## 化石

漂泊甲龍是種中型[甲龍科恐龍](../Page/甲龍科.md "wikilink")，身長估計約為6公尺。化石是一個部份骨骼（編號**SDNHM
33909**），包含：[股骨](../Page/股骨.md "wikilink")、[脛骨](../Page/脛骨.md "wikilink")、[腓骨](../Page/腓骨.md "wikilink")、不完整的[肩胛骨](../Page/肩胛骨.md "wikilink")、[肱骨](../Page/肱骨.md "wikilink")、[尺骨](../Page/尺骨.md "wikilink")、左右[坐骨](../Page/坐骨.md "wikilink")、脊椎、[肋骨](../Page/肋骨.md "wikilink")、至少60個不相連的骨盆部位裝甲、8顆牙齒。這些化石發現於加州[卡爾斯巴德附近的海相](../Page/卡爾斯巴德_\(加利福尼亞州\).md "wikilink")[諾馬角組](../Page/諾馬角組.md "wikilink")，年代為[白堊紀晚期的上](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")。這隻恐龍的屍體可能被沖刷入海洋中，並在海底成為小型沙礁。漂泊甲龍因為牠們的甲板形狀與排列方式，而被歸類於甲龍科。

## 外部連結

  - [*Aletopelta*](http://www.dinodata.org/index.php?option=com_content&task=view&id=6033&Itemid=67)
    at DinoData

[Category:甲龍科](../Category/甲龍科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")