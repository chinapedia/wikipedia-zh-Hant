**漆咸樓**（），正式名稱為**皇家國際事務研究所**（），又譯為**查塔姆研究所**或**英国皇家战略研究所**，是一位於[倫敦的](../Page/倫敦.md "wikilink")[非營利](../Page/非營利組織.md "wikilink")[非政府的](../Page/非政府組織.md "wikilink")[智庫組織](../Page/智庫.md "wikilink")；其使命為分析國際事務與[時事](../Page/新聞.md "wikilink")，以及推廣其認識，被同業認為是世界國際事務領域的領導機構\[1\]，亦為知名（承諾不會引述出席者言論以利自由討論）的發源地。

截至2013年，漆咸樓被美國[賓州大學的](../Page/賓州大學.md "wikilink")[全球關鍵智庫指標評比報告排名為世界第二](../Page/全球關鍵智庫指標.md "wikilink")，美國境外第一\[2\]。由於其研究者常引領世界議題及新提案，美國《[外交政策](../Page/外交政策_\(雜誌\).md "wikilink")》雜誌亦稱之為美國境外排名第一的[智庫](../Page/智庫.md "wikilink")\[3\]。

## 組織與成員

來自英國不同政黨，漆咸樓現有三位領導：前英國首相[約翰·梅傑](../Page/約翰·梅傑.md "wikilink")、前[自由民主黨黨魁及](../Page/自由民主黨_\(英國\).md "wikilink")[駐波斯尼亞及黑山歐盟特別代表](../Page/国际社会驻波黑高级代表.md "wikilink")、前英國檢察總長\[4\]。

為了要鼓勵國際事務及政策的討論，漆咸樓主要以從事政策研究及會談為主，開放任何人及組織加入其成員，包括[企業](../Page/企業.md "wikilink")、[學術單位](../Page/學術單位.md "wikilink")、[非政府組織及](../Page/非政府組織.md "wikilink")[個人成為會員](../Page/個人.md "wikilink")，儘管漆咸樓仍被批評反映建制派的世界觀\[5\]。

## 歷史

1920年，在[1919年巴黎和會之後皇家國際事務研究所正式成立](../Page/1919年巴黎和會.md "wikilink")，時其名曰「國際事務研究所」；1923年，因獲贈該所取得其會址擁有權。2004年9月，研究所正式以該會址建築物－「漆咸樓」為機關名稱，但「皇家國際事務研究所」一名仍有人用\[6\]\[7\]。

## 漆咸樓獎

漆咸樓獎於2005年設立，以水晶獎杯及英國女皇簽發的卷軸奬勵對該年國際關係改善做出最重要貢獻的人士\[8\]。
[Aung_San_Suu_Kyi_at_Chatham_House.jpg](https://zh.wikipedia.org/wiki/File:Aung_San_Suu_Kyi_at_Chatham_House.jpg "fig:Aung_San_Suu_Kyi_at_Chatham_House.jpg")在獲得[漆咸樓奬與發表負責任的投資](../Page/漆咸樓奬.md "wikilink")[緬甸演說後正要離開相片](../Page/緬甸.md "wikilink")\]\]
[Hillary_Rodham_Clinton_Chatham_House_Prize_2013_Award_Ceremony_(10224254843).jpg](https://zh.wikipedia.org/wiki/File:Hillary_Rodham_Clinton_Chatham_House_Prize_2013_Award_Ceremony_\(10224254843\).jpg "fig:Hillary_Rodham_Clinton_Chatham_House_Prize_2013_Award_Ceremony_(10224254843).jpg")獲獎者[希拉蕊·羅登·柯林頓](../Page/希拉蕊·羅登·柯林頓.md "wikilink")\]\]

  - 獲獎名單

<table>
<thead>
<tr class="header">
<th><p>年</p></th>
<th><p>名稱</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/烏克蘭.md" title="wikilink">烏克蘭</a><a href="../Page/總統.md" title="wikilink">總統</a> <a href="../Page/維克多·安德烈耶維奇·尤先科.md" title="wikilink">維克多·安德烈耶維奇·尤先科</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/莫桑比克.md" title="wikilink">莫桑比克總統</a> <a href="../Page/若阿金·希薩諾.md" title="wikilink">若阿金·希薩諾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/卡達.md" title="wikilink">卡達</a> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/加納.md" title="wikilink">加納總統</a> <a href="../Page/約翰·庫福爾.md" title="wikilink">約翰·庫福爾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西總統</a> <a href="../Page/路易斯·伊纳西奥·卢拉·达席尔瓦.md" title="wikilink">路易斯·伊纳西奥·卢拉·达席尔瓦</a>[9]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其總統</a> <a href="../Page/阿卜杜拉·居爾.md" title="wikilink">阿卜杜拉·居爾</a>[10]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/緬甸.md" title="wikilink">緬甸反對黨領袖</a><a href="../Page/翁山蘇姬.md" title="wikilink">翁山蘇姬</a>[11]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/突尼西亞.md" title="wikilink">突尼西亞總統</a> <a href="../Page/蒙瑟夫·馬佐基.md" title="wikilink">蒙瑟夫·馬佐基與</a><a href="../Page/拉希德·加努希.md" title="wikilink">拉希德·加努希</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/美國國務卿.md" title="wikilink">國務卿</a> <a href="../Page/希拉蕊·羅登·柯林頓.md" title="wikilink">希拉蕊·羅登·柯林頓</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/比爾與美琳達·蓋茨基金會.md" title="wikilink">比爾與梅琳達·蓋茨基金會</a> 共同創辦人 <a href="../Page/梅琳達·蓋茲.md" title="wikilink">梅琳達·蓋茲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/無國界醫生.md" title="wikilink">無國界醫生</a> [12]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>伊朗外長 <a href="../Page/穆罕默德·賈瓦德·扎里夫.md" title="wikilink">穆罕默德·賈瓦德·扎里夫</a>[13]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美國國務卿.md" title="wikilink">美國國務卿</a> <a href="../Page/约翰·克里.md" title="wikilink">约翰·克里</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Chattam.jpg](https://zh.wikipedia.org/wiki/File:Chattam.jpg "fig:Chattam.jpg")

## 參考來源

## 參見

  - [智庫](../Page/智庫.md "wikilink")

  -
  - [恐怖主义](../Page/恐怖主义.md "wikilink")

## 外部連結

  - [漆咸樓官網](http://www.chathamhouse.org.uk/)
  - [英國歷史建築](http://www.british-history.ac.uk/report.asp?compid=40553)

[Category:1920年建立的组织](../Category/1920年建立的组织.md "wikilink")
[Category:英國智庫](../Category/英國智庫.md "wikilink")
[Category:英國歷史建築](../Category/英國歷史建築.md "wikilink")

1.  [Think-tanks going global, survey
    finds](http://www.euractiv.com/pa/think-tanks-going-global-survey-news-257212)
    EurActiv.com
2.  University of Pennsylvania [2012 Global Go To Think Tanks Ranking
    Released](http://www.gotothinktank.com/2012-global-goto-tank-index-report)
     21 January 2013
3.
4.  [Patrons, Presidents, Council, and
    Directors](http://www.chathamhouse.org/about-us/governance) at
    Chatham House
5.
6.
7.
8.  Adrian Croft, 2011 [Myanmar's Suu Kyi wins top UK Chatham House
    prize](http://www.reuters.com/article/2011/09/22/us-myanmar-suukyi-prize-idUSTRE78L7F120110922)
    LONDON | Thu Sep 22, 2011 7:25pm EDT
9.
10.
11.
12.
13.