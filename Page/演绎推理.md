[Noun_project_3067.svg](https://zh.wikipedia.org/wiki/File:Noun_project_3067.svg "fig:Noun_project_3067.svg")

**演绎推理**（）在传统的[亚里士多德逻辑中是](../Page/亚里士多德逻辑.md "wikilink")「结论，可从叫做‘前提’的已知事实，‘必然地’得出的推理」。如果前提为真，则结论必然为真。这区别于[溯因推理和](../Page/溯因推理.md "wikilink")[归纳推理](../Page/归纳推理.md "wikilink")：它们的前提可以预测出高概率的结论，但是不确保结论为真。

“演绎推理”还可以定义为结论在普遍性上不大于前提的[推理](../Page/推理.md "wikilink")，或「结论在确定性上，同前提一样」的推理。

## 例子

任何三角形只可能是锐角三角形、直角三角形和钝角三角形。——大前提

这个三角形既不是锐角三角形，也不是钝角三角形。——小前提

**所以，它是一个直角三角形。**——结论

## 常用的基本论证形式

|                                          |
| :--------------------------------------: |
|                演算的基本论证形式                 |
|                    名字                    |
|   [肯定前件论式](../Page/肯定前件.md "wikilink")   |
|   [否定后件论式](../Page/否定后件.md "wikilink")   |
|  [假言三段论式](../Page/假言三段论.md "wikilink")   |
|  [选言三段论式](../Page/选言三段论.md "wikilink")   |
|                 创造性二难论式                  |
|                 破坏性二难论式                  |
|                   简化论式                   |
|                   合取式                    |
|                   增加论式                   |
|                   合成论式                   |
|                德·摩根定律(1)                 |
|                德·摩根定律(2)                 |
|                  交换律(1)                  |
|                  交换律(2)                  |
|                  结合律(1)                  |
|                  结合律(2)                  |
|                  分配律(1)                  |
|                  分配律(2)                  |
|                  双重否定律                   |
|                   换位律                    |
|                实质蕴涵律（蕴析律）                |
|                 实质等价律(1)                 |
|                 实质等价律(2)                 |
|                   输出律                    |
|                   输入律                    |
|     [重言式](../Page/重言式.md "wikilink")     |
|     [排中律](../Page/排中律.md "wikilink")     |
|      indiscernibility of identicals      |
| [吸收律](../Page/吸收律_\(逻辑学\).md "wikilink") |

## 公理化

更加形式化的说，演绎是陈述的序列，每个陈述都可以从它前面的陈述推导出来。本质上，这导致-{了}-如何证明第一个句子的公开问题(因为它不能从任何事物得到)。公理化命题逻辑通过要求证明满足下列条件来解决这个问题:

来自 [wff](../Page/合式公式.md "wikilink") 的全体 Σ 的证明 α 是一个 wff 的有限序列:

  -
    β1,...,βi,...,βn

这里的

  -
    βn = α一

并且对于每个 βi (1 ≤ i ≤ n)，

  -
    要么 βi ∈ Σ
    要么 βi 是一个公理。
    要么 βi 是两个前面的 wff βi-g 和 βi-h 的[肯定前件的输出](../Page/肯定前件.md "wikilink")。

不同版本的公理化命题逻辑都包含一些[公理](../Page/公理.md "wikilink")，通常是三个或多于三个，除了一个或更多的推理规则之外。例如[弗雷格公理化的命题逻辑](../Page/弗雷格.md "wikilink")，它也是这种尝试的第一个实例，有六个命题公理和两个规则。[伯特兰·罗素和](../Page/伯特兰·罗素.md "wikilink")[阿尔弗雷德·诺思·怀特黑德也提议了有五个公理的一个系统](../Page/阿尔弗雷德·诺思·怀特黑德.md "wikilink")。

例如[扬·武卡谢维奇](../Page/扬·武卡谢维奇.md "wikilink")(，1878年-1956年)版本的公理化命题逻辑有接受如下公理的公理集合
A:

:\* \[PL1\] p → (q → p)

:\* \[PL2\] (p → (q → r)) → ((p → q) → (p → r))

:\* \[PL3\] (¬p → ¬q) → (q → p) 并且它有有一个规则的推理规则的集合 R，这个规则就是下面的肯定前件:

:\* \[MP\] 从 α 和 α → β, 推出 β。

推理规则允许我们从公理或给定的全体 Σ 的 wff 推导出陈述。

## 自然演绎逻辑

在 E.J. Lemmon 提出的我们称为系统 L 的一个版本的自然演绎逻辑中，我们首先没有任何公理。我们只有支配证明的语法的九个基本规则。

系统 L 的九个基本规则是:

1.  [假定规则](../Page/假定.md "wikilink") (A)
2.  [肯定前件规则](../Page/肯定前件.md "wikilink") (MPP)
3.  双重否定规则 (DN)
4.  [条件证明规则](../Page/条件证明.md "wikilink") (CP)
5.  ∧-介入规则 (∧I)
6.  ∧-除去规则 (∧E)
7.  ∨-介入规则 (∨I)
8.  ∨-除去规则 (∨E)
9.  [反证法规则](../Page/反证法.md "wikilink") (RAA)

在系统 L 中，证明的定义有下列条件:

1.  有一个 wff([合式公式](../Page/合式公式.md "wikilink"))的有限序列
2.  它的每行都被系统 L 的一个规则所证明
3.  证明的最后一行是想要的([Q.E.D.](../Page/Q.E.D..md "wikilink"), quod erat
    demonstrandum, 是拉丁语: 这就是要证明的)，并且证明的最后一行只使用给出的前提；或者没有前提（如果什么都没有给出的话）。

如果没有前提给出，则相继式叫做定理。所以在系统 L 中定理的定义是:

  - 定理是在系统 L 中使用空的假定集合能证明的相继式。

或者换句话说:

  - 定理是在系统 L 中从假定的空集可以证明的相继式。

相继式的证明的一个例子(这里是否定后件):

|                              |
| :--------------------------: |
| p → q, ¬q ├ ¬p \[否定后件(MTT)\] |
|             假定号              |
|              1               |
|              2               |
|              3               |
|             1,3              |
|            1,2,3             |
|             1,2              |
|            Q.E.D.            |
|                              |

相继式证明的一个例子(这里是一个定理):

|         |
| :-----: |
| ├p ∨ ¬p |
|   假定号   |
|    1    |
|    2    |
|    2    |
|  1, 2   |
|    1    |
|    1    |
|    1    |
|    1    |
|         |
|         |
| Q.E.D.  |
|         |

系统 L 的每行都有自己对输入或进入的类型的要求，它可以接受并且拥有它自己的处理和计算于是它的输入使用的假定的方式。

## 引用

  - Jennings, R. E., Continuing Logic, the course book of **Axiomatic
    Logic** in Simon Fraser University, Vancouver, Canada
  - Zarefsky, David, Argumentation: The Study of Effective Reasoning
    Parts I and II, The Teaching Company 2002

## 参见

  - [真理的符合理论](../Page/真理的符合理论.md "wikilink")
  - [可废止推理](../Page/可废止推理.md "wikilink")
  - [归纳推理](../Page/归纳推理.md "wikilink")
  - [假设演绎方法](../Page/假设演绎方法.md "wikilink")
  - [命题演算](../Page/命题演算.md "wikilink")
  - [可靠性](../Page/可靠性定理.md "wikilink")
  - [逆推推理](../Page/逆推推理.md "wikilink")
  - [有效性](../Page/有效性.md "wikilink")

[Category:邏輯](../Category/邏輯.md "wikilink")
[Y](../Category/數理邏輯.md "wikilink")