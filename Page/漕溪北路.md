[ShanghaiHighWayCXNR.jpg](https://zh.wikipedia.org/wiki/File:ShanghaiHighWayCXNR.jpg "fig:ShanghaiHighWayCXNR.jpg")漕溪北路立交\]\]

**漕溪北路**是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[徐汇区中部的一条街道](../Page/徐汇区.md "wikilink")，南北走向，北起[徐家汇](../Page/徐家汇.md "wikilink")[肇嘉浜路](../Page/肇嘉浜路.md "wikilink")、[虹桥路](../Page/虹桥路.md "wikilink")[徐家汇接](../Page/徐家汇.md "wikilink")[衡山路](../Page/衡山路_\(上海\).md "wikilink")、[华山路](../Page/华山路_\(上海\).md "wikilink")，南至[中山南二路](../Page/中山南二路.md "wikilink")、[中山西路交会处接](../Page/中山西路_\(上海\).md "wikilink")[漕溪路](../Page/漕溪路.md "wikilink")。长1488米，宽35米到56米。漕溪北路是上海市区三横三纵主干道西纵的一部分，也是[沪闵高架路的起点](../Page/沪闵高架路.md "wikilink")，是从上海南部进入市中心以及徐家汇的重要途径。[上海轨道交通1号线沿漕溪北路地下连接徐家汇和上海体育馆](../Page/上海轨道交通1号线.md "wikilink")。[徐家汇站一号口旁有](../Page/徐家汇站.md "wikilink")[徐光启像](../Page/徐光启.md "wikilink")。

## 历史

此处原为河道，干涸后于1945年填河筑路。1949年以后拓宽形成交通干道。因位于漕溪路之北并与之相接而命名。1950年漕溪北路翻建成弹街路面，即用小石块铺砌的路面。1973年则再次拓宽并建成钢筋水泥混凝土路面，也就是如今的样子。\[1\]

## 沿路建筑

漕溪北路两侧原是[天主教机构集中的区域](../Page/天主教.md "wikilink")，西侧为男性机构[徐家汇圣依纳爵主教座堂](../Page/徐家汇圣依纳爵主教座堂.md "wikilink")、[耶稣会会院](../Page/耶稣会.md "wikilink")、大修道院（今[徐汇区人民政府](../Page/徐汇区.md "wikilink")）、[徐家汇藏书楼](../Page/徐家汇藏书楼.md "wikilink")（今[上海图书馆分部](../Page/上海图书馆.md "wikilink")）、[徐汇中学](../Page/徐汇中学.md "wikilink")，东侧为女性机构[徐家汇圣母院](../Page/徐家汇圣母院.md "wikilink")（拯亡会、献堂会和孤儿院）、圣衣院（今[上海电影制片厂](../Page/上海电影制片厂.md "wikilink")）。1970年代在马路西侧建起了[徐汇新村](../Page/徐汇新村.md "wikilink")。1980年代以后新建[华亭宾馆](../Page/华亭宾馆.md "wikilink")、[东方商厦](../Page/东方商厦.md "wikilink")、[上海体育馆等建築物](../Page/上海体育馆.md "wikilink")。

## 交汇道路（由南向北）

  - 零陵路
  - 裕德路
  - 影业街
  - 蒲汇塘路
  - 慈云街
  - 南丹路/南丹东路
  - 蒲西路

## 参考资料

[Category:徐汇区](../Category/徐汇区.md "wikilink")
[Category:上海道路](../Category/上海道路.md "wikilink")

1.