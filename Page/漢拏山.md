**漢拏山**（），又作**漢拿山**，是一座位於[韓國](../Page/大韓民國.md "wikilink")[濟州島中部](../Page/濟州島.md "wikilink")[漢拏山國立公園的](../Page/漢拏山國立公園.md "wikilink")[休火山](../Page/休火山.md "wikilink")，[海拔](../Page/海拔.md "wikilink")1947.06[公尺](../Page/公尺.md "wikilink")（6,398英尺），超過韓國本土最高峰[智異山](../Page/智異山.md "wikilink")（**지리산**，海拔1915公尺）與第二高峰[雪嶽山](../Page/雪嶽山.md "wikilink")（**설악산**，海拔1708公尺），為濟州島、韓國第一高峰，據記載，漢拏山最後一次[火山爆發為](../Page/火山爆發.md "wikilink")1007年。

漢拏山是韓國為數不多的火山，周圍有360座寄生火山。並形成長達14公里的熔岩隧道。2007年被列為[世界自然遺產](../Page/世界自然遺產.md "wikilink")。

## 图片

[File:Hallasan.jpg|漢拏山冬景](File:Hallasan.jpg%7C漢拏山冬景)
[File:Halasan.jpg|漢拏山山頂火山口](File:Halasan.jpg%7C漢拏山山頂火山口)
<File:Hallasan-2005> 07 22 - 1.jpg|漢拏山山腳登山步道
[File:Hallasan,Jeju.jpg|漢拏山远观](File:Hallasan,Jeju.jpg%7C漢拏山远观)

## 参见

  - [韩国山峰列表](../Page/韩国山峰列表.md "wikilink")
  - [韩国旅游](../Page/韩国旅游.md "wikilink")
  - [白鹿潭](../Page/白鹿潭.md "wikilink")

[Category:韓國火山](../Category/韓國火山.md "wikilink")
[Category:休火山](../Category/休火山.md "wikilink")
[Category:盾狀火山](../Category/盾狀火山.md "wikilink")
[Category:火山湖](../Category/火山湖.md "wikilink")
[Category:濟州特別自治道地理](../Category/濟州特別自治道地理.md "wikilink")
[Category:韓國旅遊景點](../Category/韓國旅遊景點.md "wikilink")
[Category:韩国地理之最](../Category/韩国地理之最.md "wikilink")