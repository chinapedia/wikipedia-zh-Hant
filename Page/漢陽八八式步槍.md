**漢陽八八式步槍**，又俗称「**漢陽造**」或「**老套筒**」，由[清末建立的](../Page/清朝.md "wikilink")[漢陽兵工廠獲得](../Page/漢陽兵工廠.md "wikilink")[德國授權生產的](../Page/德國.md "wikilink")[Gewehr
88步枪](../Page/Gewehr_88步枪.md "wikilink")，是[中國生產的第一款](../Page/中國.md "wikilink")[旋轉後拉式槍機](../Page/旋轉後拉式槍機.md "wikilink")[步槍](../Page/步槍.md "wikilink")。在清末的新军一直到[抗日战争](../Page/抗日战争.md "wikilink")、[国共内战以及](../Page/国共内战.md "wikilink")[朝鲜战争都是中國軍隊的主要步槍枪型之一](../Page/朝鲜战争.md "wikilink")，作为制式步枪在軍隊服役超過60年，可以說是20世紀初中國軍隊的代表性[轻武器装备之一](../Page/轻武器.md "wikilink")。套筒一詞來自包覆於槍管外層的護罩，這項引人注目的設計原意是為了防止槍托與和護木因膨脹或彎曲等外力促使槍管變形從而喪失準確性，同時也能避免士兵在上刺刀時被槍管灼傷，但實際上在惡劣的環境中，砂石與和泥水反而更容易滲入縫隙且不易清除，進而增加了生鏽與磨損的風險，而後有部分的套筒直接被焊接在了槍管上。再加上生產廠方對新式無煙火藥的了解並不充分，不知其產生的膛壓比以往的所有彈藥都高，導致所用鋼材的碳含量過低，從而導致膛室無法承受新式彈藥所帶來的壓力，最終膛炸事件四起，此一風波還導致作為火藥生產商的猶太家族因德國國會議員指責而蒙受污名。
[Infanteriegewehr_m-1888_-_Tyskland_-_kaliber_7,92mm_-_Armémuseum.jpg](https://zh.wikipedia.org/wiki/File:Infanteriegewehr_m-1888_-_Tyskland_-_kaliber_7,92mm_-_Armémuseum.jpg "fig:Infanteriegewehr_m-1888_-_Tyskland_-_kaliber_7,92mm_-_Armémuseum.jpg")

## 歷史

### 清末

[汉阳造1.JPG](https://zh.wikipedia.org/wiki/File:汉阳造1.JPG "fig:汉阳造1.JPG")
该枪原型为德国1888式“委员会”步枪（Gewehr 88或简称Gew
88），採用无烟发射药小口径（较当时[弹药口径而言](../Page/弹药.md "wikilink")）步枪弹，配用子弹为七九口径圆头弹，枪身下有外露弹仓，用5发漏弹夹装填，由于该枪存在着装弹退弹困难、抽壳可靠性不佳、容易炸膛等问题，1898年德国军方采用[毛瑟Gew98步枪将其取代](../Page/Gew_98步槍.md "wikilink")。该型步枪遂转售给[土耳其](../Page/土耳其.md "wikilink")、非洲一些国家和中国的清政府。\[1\]

基于清政府的[洋务官员对](../Page/洋务.md "wikilink")“[毛瑟](../Page/毛瑟.md "wikilink")”品牌的迷信，德国商人谎称1888式步枪为“[毛瑟步枪](../Page/毛瑟步枪.md "wikilink")”，成功地将设计资料和生产机械卖给了清政府。故从1896年开始，最初为[湖广总督](../Page/湖广总督.md "wikilink")[张之洞所推动在](../Page/张之洞.md "wikilink")[湖北建立的湖北枪炮厂](../Page/湖北.md "wikilink")（[汉阳兵工厂的前身](../Page/汉阳兵工厂.md "wikilink")）开始生产此型步枪，定名为**八八式**。第一年生产了1千3百支步枪。八八式步枪枪管外部有一套筒，因此俗称“**老套筒**”；因为汉阳兵工厂是八八式步枪最主要的生产厂，所以也俗称为“**汉阳造**”。1899年[江南制造局也开始生产此型步枪](../Page/江南制造局.md "wikilink")。一直到1944年，此型步枪在中国前后生产了将近50年，为当时中国生产时间最长的一种轻武器。

汉阳兵工厂在1904年对八八式步枪进行了改进，外观上最显著的特征是去掉枪管外部套筒；1910年又进一步改进，增加枪管上护木，将直立式表尺改为固定式弧形表尺。在清朝末年雖然清廷一度希望將其停產改制[元年式步槍](../Page/元年式步槍.md "wikilink")（毛瑟1907式步槍的授權生產版），但是因經費不足此計畫並未執行。而漢陽造步槍也就逐漸生產並配發至兩湖新軍並持續生產到民国時代。1911年引发[辛亥革命导致清政府垮台的](../Page/辛亥革命.md "wikilink")[武昌起义中](../Page/武昌起义.md "wikilink")，[湖北新军就装备漢陽八八式步槍](../Page/湖北新军.md "wikilink")。

根据汉阳兵工厂生产记录：*1910年『湖北兵工钢药厂历年及宣统元年造成拨解实存各项军火表册』记载由1895年至1909年年底的生产总数为七密里九口径所谓的“毛瑟”步枪121974支；马枪（全长为955mm）8062支。*
[NRA_anti-Japanese_war_poster.jpg](https://zh.wikipedia.org/wiki/File:NRA_anti-Japanese_war_poster.jpg "fig:NRA_anti-Japanese_war_poster.jpg")

### 民國時期

[19th_Root_Army,_being_in_engagement_with_the_Japanese_in_Chapei_front.jpg](https://zh.wikipedia.org/wiki/File:19th_Root_Army,_being_in_engagement_with_the_Japanese_in_Chapei_front.jpg "fig:19th_Root_Army,_being_in_engagement_with_the_Japanese_in_Chapei_front.jpg")
[Hangyangzao_WUM.jpg](https://zh.wikipedia.org/wiki/File:Hangyangzao_WUM.jpg "fig:Hangyangzao_WUM.jpg")民國初期，由於[北洋政府並未進行造槍機具更新](../Page/北洋政府.md "wikilink")，因此漢陽兵工廠仍持續製造八八式步槍，南京[金陵兵工廠在此時更改設備製造八八式步槍](../Page/金陵兵工廠.md "wikilink")。之後由於[軍閥割據一方](../Page/軍閥.md "wikilink")，機具更新不但不可能，連生產槍枝的經費也無著落，有时因无力添购材料直至停工，又加上连年战乱，因此兩間兵工廠的步槍生產处于時開時停的階段；直到1928年[國民革命軍佔領](../Page/國民革命軍.md "wikilink")[武漢與](../Page/武漢.md "wikilink")[南京之後兩廠生產才逐漸穩定](../Page/南京.md "wikilink")，仍作为国民革命军的制式步枪。据枪上序号推算，1910至1932年初产量为46万支。

**漢陽八八式步槍**原本為在抗戰前夕，[中正式步槍還未出現以前的過渡型槍枝](../Page/中正式步槍.md "wikilink")，不過後來國民政府雖一度希望將生產漢陽造的步槍機具改造為製造[中正式步槍](../Page/中正式步槍.md "wikilink")，但是隨後1937年7月[抗日戰爭爆發使這項計畫延後](../Page/抗日戰爭.md "wikilink")，金陵廠與漢陽廠也因抗戰內遷后方，在內遷的过程中漢陽兵工廠（改名为第一兵工厂）之枪厂迁往[重庆](../Page/重庆.md "wikilink")，后来生產步槍的機具移交給遷到重慶的第二一廠（金陵兵工廠改組後代號），使得漢陽八八式步槍的生產集中在單一廠區。1939年复产，改称为**汉式七九步枪**，在日軍支那武器識別一書中更談到，汉式七九步枪，可以使用[7.92×57毫米尖頭彈](../Page/7.92_x_57毫米毛瑟.md "wikilink")，以及橋夾進行進彈，不同於原本的漏夾。[鞏縣兵工廠步槍部門被炸毀後中正式步枪的產能大幅下降](../Page/鞏縣兵工廠.md "wikilink")，因此在抗戰時期漢陽八八式步槍仍在軍中佔有重要地位，二一廠的漢陽八八式步槍持續生產到1944年，廠內機具才完全修改為製造中正式步枪後才告停產。战时在重庆的第二一厂生产了207164支。七九口径圓頭彈到抗戰結束仍有大量生產。由於數量龐大且耐用因此仍持續在[国共内战中使用](../Page/国共内战.md "wikilink")。

關於漢陽造步槍的改膛技術，漢陽造步槍源自於德國的M1888步槍，在第一次世界大戰的時候，土耳其也接手了許多德國的M1888步槍，當時的德國當局也幫助了土耳其軍隊所擁有的M1888步槍，進行了改造的工作，便使原本的M1888步槍擁有如同毛瑟系統一樣可以用橋夾進彈，而原本M1888步槍彈匣下面的開口，也用了一塊鐵片封起來了，並且也可以使用[7.92×57毫米尖頭彈供彈射擊](../Page/7.92_x_57毫米毛瑟.md "wikilink")，這樣改造工作其實相當的簡單，在工業技術上的要求其實也不高，在對日抗戰結束後的國共內戰也可發現到，許多從日軍繳獲而來的[三八式步槍與](../Page/三八式步槍.md "wikilink")[九九式步槍](../Page/九九式步槍.md "wikilink")，都可以被國府軍改膛為以[7.92×57毫米尖頭彈進行射擊](../Page/7.92_x_57毫米毛瑟.md "wikilink")。所以相同的道理，將漢陽造步槍改造為如同毛瑟步槍的橋夾進彈系統，並且改用[7.92×57毫米尖頭彈供彈射擊](../Page/7.92_x_57毫米毛瑟.md "wikilink")，其實也並不是什麼困難的事情，因為方式都跟原本德國與土耳其的M1888步槍的改造方法都是差不多的。

因为统计资料匮乏，确定漢陽八八式步槍的总产量是比较困难的事。根据后来的枪上序号推算在1938年汉阳兵工厂迁厂之前漢陽八八式步槍制造数量大约是超过87万支。由此推算漢陽八八式步槍的总产量超过了100万支。

### 退役

至[朝鲜战争時期依然可見漢陽八八式步槍的蹤影](../Page/朝鲜战争.md "wikilink")。直到朝鲜战争結束後[中華人民共和國經由軍援以及自產獲得大量蘇聯制式單兵武器後](../Page/中華人民共和國.md "wikilink")，漢陽八八式步槍才退出現役轉為[民兵使用到](../Page/民兵.md "wikilink")80年代才完全淘汰。

在中国近代史上，汉阳造颇为著名。在湖北武汉“辛亥武昌起義工程營發難處”紀念碑上雕刻有汉阳造步枪的形象。在江西[南昌](../Page/南昌.md "wikilink")“八一南昌起义纪念塔”上雕刻着漢陽造步枪与军旗的形象。

## 注释

## 参考文献

## 相關條目

  - [中正式步枪](../Page/中正式步枪.md "wikilink")
  - [三八式步槍](../Page/三八式步槍.md "wikilink")
  - [莫辛-納甘步槍](../Page/莫辛-納甘步槍.md "wikilink")
  - [漢陽造 (2012年電視劇)](../Page/漢陽造_\(2012年電視劇\).md "wikilink")

## 外部链接

  - [漢陽造–火器堂](http://www.chinesefirearms.com/110108/articles/hanyang.htm)
  - [Gew88與漢陽八八式的分別](http://www.thedonovan.com/archives/000578.html)
  - [槍砲世界\> 1888式委員會步槍](http://firearmsworld.net/german/r/1888/1888.htm)（Commission
    Rifle或ReichsGewehr）。

[Category:栓動式步槍](../Category/栓動式步槍.md "wikilink")
[Category:中華人民共和國槍械](../Category/中華人民共和國槍械.md "wikilink")
[Category:中華民國槍械](../Category/中華民國槍械.md "wikilink")
[Category:7.92×57毫米槍械](../Category/7.92×57毫米槍械.md "wikilink")
[Category:抗戰時期中國武器](../Category/抗戰時期中國武器.md "wikilink")
[Category:韓戰武器](../Category/韓戰武器.md "wikilink")

1.  [1888式委员会步枪](http://firearmsworld.net/german/r/1888/1888.htm)，枪炮世界网站。