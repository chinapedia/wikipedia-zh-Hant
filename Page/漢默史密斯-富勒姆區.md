**漢默史密斯-富咸區**（）是[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[大倫敦的](../Page/大倫敦.md "wikilink")[自治市之一](../Page/倫敦自治市.md "wikilink")，屬[內倫敦的一部份](../Page/內倫敦.md "wikilink")，[人口](../Page/人口.md "wikilink")
171,400，[面積](../Page/面積.md "wikilink")16.40km²。雖然著名的-{zh-tw:切爾西;zh-cn:切尔西;zh-hk:車路士;}-足球俱乐部是按照倫敦的切爾西區命名的，但事實上他们的球場並不在切爾西區境內，而在富咸區的區劃以內。

## 经济

[维珍集团总部](../Page/维珍集团.md "wikilink")\[1\]、[索尼移动通信本部](../Page/索尼移动通信.md "wikilink")\[2\]、[西班牙国家航空总部](../Page/西班牙国家航空.md "wikilink")\[3\]在此。

## 注釋

## 外部連結

  - [哈默史密斯-富勒姆區政府網](http://www.lbhf.gov.uk)

[Category:大倫敦](../Category/大倫敦.md "wikilink")

1.  "[Our company
    information](http://www.virgin.com/Legal/CompanyInformation.aspx) ."
    *Virgin Group*. Retrieved on 14 January 2009. "The School House 50
    Brook Green London, W6 7RR England"
2.  "[Sony Ericsson at a glance.(Telefonaktiebolaget LM Ericsson)(Sony
    Corp.)(Brief
    Article)](http://www.accessmylibrary.com/coms2/summary_0286-18966576_ITM)."
    Employee Benefits. March 2, 2005. Retrieved on November 18, 2009.
3.  "[Iberia
    Airlines](http://www.lata.org/member.php?mid=353&c=sr&lang=es)."
    Latin American Travel Association. Retrieved on 6 September 2011.
    "Contacto Iberia House 10 Hammersmith Broadway London W6 7AL Reino
    Unido"