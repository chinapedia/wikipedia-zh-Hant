《**漫畫改編的舞台劇列表**》是所有受歡迎或特別題材的漫畫改編成舞台劇的作品。

## 日本舞台劇

  - 《[巨人之星](../Page/巨人之星.md "wikilink")》（1969年/舞台劇）
  - 《[小拳王](../Page/小拳王.md "wikilink")》（1971年/舞台劇） -
    [千葉徹彌](../Page/千葉徹彌.md "wikilink")
  - 《[凡爾賽玫瑰](../Page/凡爾賽玫瑰.md "wikilink")》（1974年/寶塚歌舞劇） -
    [池田理代子](../Page/池田理代子.md "wikilink")
  - 《[銀河鐵道999](../Page/銀河鐵道999.md "wikilink")》（1980年、1982年、1986年、1997年/松竹歌舞劇）
    - [松本零士](../Page/松本零士.md "wikilink")
  - 《[千年女王](../Page/千年女王.md "wikilink")》（1981年/歌舞劇） -
    [松本零士](../Page/松本零士.md "wikilink")
  - 《[半神](../Page/半神.md "wikilink")》（1986年/歌舞劇） -
    [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《[千面女郎](../Page/千面女郎.md "wikilink")》（1988年/舞台劇） -
    [美內鈴惠](../Page/美內鈴惠.md "wikilink")
  - 《[火鳥](../Page/火鳥_\(漫畫\).md "wikilink")》（1989年/歌舞劇/舞台劇） -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink")》（1990年/歌舞劇） -
    [車田正美](../Page/車田正美.md "wikilink")
  - 《[緞帶魔法姬](../Page/緞帶魔法姬.md "wikilink")》（1993年/歌舞劇） -
    [水澤惠](../Page/水澤惠.md "wikilink")
  - 《[小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")》（1994年/歌舞劇） -
    [彩花珉](../Page/彩花珉.md "wikilink")
  - 《[櫻之園](../Page/櫻桃園.md "wikilink")》 - （1994年～2011年/歌舞劇） -
    [吉田秋生](../Page/吉田秋生.md "wikilink")
  - 《》（1996年～2010年/舞台劇） - [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《[水色時代](../Page/水色時代.md "wikilink")》（1996年/歌舞劇） -
    [藪內優](../Page/藪內優.md "wikilink")
  - 《》（1996年～1997年/寶塚歌舞劇） - [大和和紀](../Page/大和和紀.md "wikilink")
  - 《[少女革命Utena](../Page/少女革命.md "wikilink")》（1997年/歌舞劇） -
    [齊藤千穗](../Page/齊藤千穗.md "wikilink")
  - 《[魯邦三世](../Page/魯邦三世.md "wikilink")》（1998年/歌舞劇） -
    [加藤一彥](../Page/加藤一彥.md "wikilink")
  - 《[訪問者](../Page/訪問者.md "wikilink")》（1998年/舞台劇） -
    [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《[寶馬騎士](../Page/缎带骑士.md "wikilink")》（1983年～2006年/歌舞劇） -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[烏龍派出所](../Page/烏龍派出所.md "wikilink")》（1999年/舞台劇） -
    [秋本治](../Page/秋本治.md "wikilink")
  - 《[Hunter X Hunter](../Page/HUNTER×HUNTER.md "wikilink")》（2000年/歌舞劇）
    - [富樫義博](../Page/富樫義博.md "wikilink")
  - 《[源氏物語](../Page/源氏物語_\(漫畫\).md "wikilink")》（2000年/寶塚歌舞劇） -
    [大和和紀](../Page/大和和紀.md "wikilink")
  - 《》（2001年/舞台劇） - [本宮宏志](../Page/本宮宏志.md "wikilink")
  - 《》（2001年/舞台劇） - [三原順](../Page/三原順.md "wikilink")
  - 《[網球王子](../Page/網球王子.md "wikilink")》（2003年～現在/歌舞劇） -
    [許斐剛](../Page/許斐剛.md "wikilink")
  - 《[愛與誠](../Page/愛與誠.md "wikilink")》（2003年/舞台劇） -
    [永安巧](../Page/永安巧.md "wikilink")
  - 《[OZ](../Page/Oz.md "wikilink")》（2003年/舞台劇） -
    [樹夏實](../Page/樹夏實.md "wikilink")
  - 《[百鬼夜行抄](../Page/百鬼夜行抄.md "wikilink")》（2003年/舞台劇） -
    [今市子](../Page/今市子.md "wikilink")
  - 《[アメリカン・パイ](../Page/American_Pie.md "wikilink")》（2003年/寶塚歌舞劇） -
    [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《》（2004年、2011年/舞台劇） - [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《》（2005年/舞台劇） - [萩尾望都](../Page/萩尾望都.md "wikilink")
  - 《[BLEACH](../Page/BLEACH.md "wikilink")》（2005年/歌舞劇） -
    [久保帯人](../Page/久保帯人.md "wikilink")
  - 《[Naruto](../Page/Naruto.md "wikilink")》（2005年/歌舞劇） -
    [岸本斉史](../Page/岸本斉史.md "wikilink")
  - 《[Air Gear](../Page/Air_Gear.md "wikilink")》（2007年/歌舞劇） -
    [大暮維人](../Page/大暮維人.md "wikilink")
  - 《[七海星空](../Page/七海星空.md "wikilink")》（2007年～2008年/寶塚歌舞劇） -
    [青池保子](../Page/青池保子.md "wikilink")
  - 《[GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")》（2007年/歌舞劇） -
    [服部充](../Page/服部充.md "wikilink")
  - 《[Switch](../Page/Switch.md "wikilink")》（2007年/歌舞劇） - [Naked
    ape](../Page/Naked_ape.md "wikilink")
  - 《[PEACE
    MAKER鐵](../Page/PEACE_MAKER鐵.md "wikilink")》（2007年～2012年/歌舞劇）
    - [黑乃奈奈繪](../Page/黑乃奈奈繪.md "wikilink")
  - 《》（2008年/舞台劇） - [星野之宣](../Page/星野之宣.md "wikilink")
  - 《[魔法老師](../Page/魔法老師.md "wikilink")》 -
    [赤松健](../Page/赤松健.md "wikilink")
  - 《[恐怖寵物店](../Page/恐怖寵物店.md "wikilink")》 -
    [秋乃茉莉](../Page/秋乃茉莉.md "wikilink")
  - 《[守護天使莉莉佳](../Page/守護天使莉莉佳.md "wikilink")》 -
    [秋元康](../Page/秋元康.md "wikilink")
  - 《[飛輪少年](../Page/飛輪少年.md "wikilink")》 -
    [大暮維人](../Page/大暮維人.md "wikilink")
  - 《》 - [池田理代子](../Page/池田理代子.md "wikilink")
  - 《》 - [小池一夫](../Page/小池一夫.md "wikilink")
  - 《[改造新人類](../Page/改造新人類.md "wikilink")》 -
    [久米田康治](../Page/久米田康治.md "wikilink")
  - 《[火影忍者](../Page/火影忍者.md "wikilink")》 -
    [岸本齊史](../Page/岸本齊史.md "wikilink")
  - 《》 - [木原敏江](../Page/木原敏江.md "wikilink")
  - 《曖昧さ回避》 - [木原敏江](../Page/木原敏江.md "wikilink")
  - 《[校園迷糊大王](../Page/校園迷糊大王.md "wikilink")》 -
    [小林盡](../Page/小林盡.md "wikilink")
  - 《》 - [清水玲子](../Page/清水玲子.md "wikilink")
  - 《[銀牙](../Page/銀牙.md "wikilink")》 -
    [高橋義廣](../Page/高橋義廣.md "wikilink")
  - 《[犬夜叉](../Page/犬夜叉.md "wikilink")》 -
    [高橋留美子](../Page/高橋留美子.md "wikilink")
  - 《[美少女戰士](../Page/美少女戰士.md "wikilink")》 -
    [武内直子](../Page/武内直子.md "wikilink")
  - 《[多重人格偵探～雨宮一彥的歸還～](../Page/多重人格偵探～雨宮一彥的歸還～.md "wikilink")》 -
    [田島昭宇](../Page/田島昭宇.md "wikilink")
  - 《[淘氣小親親](../Page/淘氣小親親.md "wikilink")》 -
    [多田薰](../Page/多田薰.md "wikilink")
  - 《[怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")》 -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《》 - [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[三個阿道夫](../Page/三個阿道夫.md "wikilink")》 -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[七色いんこ](../Page/七色鸚哥.md "wikilink")》 -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[佛陀](../Page/佛陀_\(漫畫\).md "wikilink")》 -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《[多羅羅](../Page/多羅羅.md "wikilink")》 -
    [手塚治虫](../Page/手塚治虫.md "wikilink")
  - 《》 - [福山庸治](../Page/福山庸治.md "wikilink")
  - 《》 - [吉田秋生](../Page/吉田秋生.md "wikilink")
  - 《[戰慄殺機](../Page/Banana_Fish.md "wikilink")》 -
    [吉田秋生](../Page/吉田秋生.md "wikilink")
  - 《[はいからさんが通る](../Page/窈窕淑女_\(漫畫\).md "wikilink")》 -
    [大和和紀](../Page/大和和紀.md "wikilink")
  - 《[灌籃少年](../Page/灌籃少年.md "wikilink")》 -
    [八神浩樹](../Page/八神浩樹.md "wikilink")
  - 《》 - [峰倉和也](../Page/峰倉和也.md "wikilink")
  - 《[最遊記](../Page/最遊記.md "wikilink")》 -
    [峰倉和也](../Page/峰倉和也.md "wikilink")
  - 《[玻璃假面](../Page/玻璃假面.md "wikilink")》 -
    [美內鈴惠](../Page/美內鈴惠.md "wikilink")
  - 《[飆速宅男](../Page/飆速宅男.md "wikilink")》（2012年/舞台劇） -
    [渡邊航](../Page/渡邊航.md "wikilink")
  - 《[孩子們的遊戲](../Page/孩子們的遊戲.md "wikilink")》（2015年/舞台劇） -
    [小花美穗](../Page/小花美穗.md "wikilink")
  - 《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》（2016年/舞台劇） -
    [藤卷忠俊](../Page/藤卷忠俊.md "wikilink")
  - 《》（2016年/舞台劇） - [楳圖一雄](../Page/楳圖一雄.md "wikilink")
  - 《[足球小將](../Page/足球小將.md "wikilink")》（2017年/舞台劇） -
    [高橋陽一](../Page/高橋陽一.md "wikilink")
  - 《[路人超能100](../Page/路人超能100.md "wikilink")》（2018年/舞台劇） -
    [ONE](../Page/ONE_\(漫畫家\).md "wikilink")

## 中國舞台劇

  - 《[大家都有病](../Page/大家都有病.md "wikilink")》 -
    [朱德庸](../Page/朱德庸.md "wikilink")
  - 《[尸兄](../Page/尸兄.md "wikilink")》 - [七度魚](../Page/七度魚.md "wikilink")

## 台灣舞台劇

  - 《[五斗米靠腰](../Page/五斗米靠腰.md "wikilink")》 -
    [馬克](../Page/馬克.md "wikilink")

[\*](../Category/漫畫相關列表.md "wikilink")
[\*](../Category/漫畫改編舞台劇.md "wikilink")
[\*](../Category/戲劇題材.md "wikilink")