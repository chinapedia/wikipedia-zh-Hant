[201901_The_Waterfront_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:201901_The_Waterfront_Hong_Kong.jpg "fig:201901_The_Waterfront_Hong_Kong.jpg")眺望漾日居\]\]
[The_Waterfront.jpg](https://zh.wikipedia.org/wiki/File:The_Waterfront.jpg "fig:The_Waterfront.jpg")仰望漾日居\]\]
[The_Waterfront_Public_access_201206.jpg](https://zh.wikipedia.org/wiki/File:The_Waterfront_Public_access_201206.jpg "fig:The_Waterfront_Public_access_201206.jpg")
**漾日居**（）是[香港](../Page/香港.md "wikilink")[西九龍](../Page/西九龍.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[九龍站第一期的私人屋苑](../Page/九龍站_\(港鐵\).md "wikilink")，由[永泰亞洲](../Page/永泰亞洲.md "wikilink")、[淡馬錫控股有限公司](../Page/淡馬錫控股.md "wikilink")、[新加坡置地有限公司](../Page/新加坡置地.md "wikilink")、[吉寶置業有限公司](../Page/吉寶置業.md "wikilink")、[麗新集團有限公司及](../Page/麗新集團.md "wikilink")[環球投資](../Page/環球投資.md "wikilink")（百慕達）有限公司合組的財團投得發展權，由關永康建築師事務所設計，[協興建築承建的屋苑](../Page/協興建築.md "wikilink")，於2000年與曾經位於九龍站內的[迪生數碼世界同步落成](../Page/迪生數碼世界.md "wikilink")，屬首個於九龍站落成的住宅。

## 屋苑簡介

漾日居共有7座樓宇，1,288個住宅單位，單位面積由77至232[平方米](../Page/平方米.md "wikilink")（833至2590平方呎）。設計上可經[空中花園前往](../Page/空中花園.md "wikilink")[鐵路站](../Page/鐵路.md "wikilink")，為居民帶來方便和悠閒的感覺。

## 歷史

1993年政府批准前[地鐵公司興建](../Page/港鐵公司.md "wikilink")[機場鐵路](../Page/香港機場核心計劃#機場鐵路.md "wikilink")。1996年2月進行招標，[永泰亞洲率領以新加坡資本為主的財團](../Page/永泰亞洲.md "wikilink")，奪得項目的發展權。漾日居落成後，[港鐵因興建基座](../Page/港鐵.md "wikilink")（當時還未命名），而把C出口關閉了8年。

## 設施

屋苑設有會所，設施包括健身室、泳池、模擬[高爾夫球練習場](../Page/高爾夫球練習場.md "wikilink")、桌球室、兒童遊樂場及桑拿房，第5座業主更設獨立會所。

## 社區設施

漾日居基座1樓設社區設施，包括賽馬會漾日居中心、仁愛堂陳鄭玉而幼稚園及香港中華基督教青年會佐敦會所。

## 知名住客

  - [麥明詩](../Page/麥明詩.md "wikilink")

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[九龍站](../Page/九龍站_\(港鐵\).md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[柯士甸站](../Page/柯士甸站.md "wikilink")
  - [廣深港高速鐵路](../Page/廣深港高速鐵路.md "wikilink")：[香港西九龍站](../Page/香港西九龍站.md "wikilink")

<!-- end list -->

  - [西區海底隧道收費廣場](../Page/西區海底隧道收費廣場.md "wikilink")

<!-- end list -->

  - [九龍站公共運輸交匯處](../Page/九龍站公共運輸交匯處.md "wikilink")

<!-- end list -->

  - 鄰近西九龍站

<!-- end list -->

  - 除上述路線外，鄰近西九龍站亦有以下公共交通途經:

<!-- end list -->

  - 佐敦道公共小巴

<!-- end list -->

  - 步行设施

<!-- end list -->

  - 西九龍站至九龍站及柯士甸站行人天橋及行人隧道

</div>

</div>

## 外部連結

  - [skyscraper介紹](http://skyscraperpage.com/cities/?buildingID=7588)
  - [港鐵官方網站](http://www.mtr.com.hk)

[en:Union Square (Hong Kong)\#The
Waterfront](../Page/en:Union_Square_\(Hong_Kong\)#The_Waterfront.md "wikilink")

[Category:油尖旺區私人屋苑](../Category/油尖旺區私人屋苑.md "wikilink")
[Category:麗新發展物業](../Category/麗新發展物業.md "wikilink")
[Category:永泰亞洲物業](../Category/永泰亞洲物業.md "wikilink") [Category:Union
Square](../Category/Union_Square.md "wikilink")
[Category:2000年完工建築物](../Category/2000年完工建築物.md "wikilink")