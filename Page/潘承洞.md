**潘承洞**（），[江苏](../Page/江苏.md "wikilink")[苏州人](../Page/苏州.md "wikilink")，[中国著名](../Page/中国.md "wikilink")[数学家](../Page/数学家.md "wikilink")，[中科院院士](../Page/中科院院士.md "wikilink")，曾任[山东大学校长](../Page/山东大学.md "wikilink")。胞弟[潘承彪亦为著名](../Page/潘承彪.md "wikilink")[数学家](../Page/数学家.md "wikilink")，两人曾合著出版数学专著《[哥德巴赫猜想](../Page/哥德巴赫猜想.md "wikilink")》\[1\]。

## 生平

  - 1952年毕业于苏州[桃坞中学](../Page/桃坞中学.md "wikilink")（美国基督教圣公会创办的教会学堂，后成为[上海圣约翰大学附属中学](../Page/上海圣约翰大学.md "wikilink")，[严家淦](../Page/严家淦.md "wikilink")、[张青莲](../Page/张青莲.md "wikilink")、[刘元方](../Page/刘元方.md "wikilink")、[钱锺书](../Page/钱锺书.md "wikilink")、[钱锺韩等先后就读于此](../Page/钱锺韩.md "wikilink")）。
  - 1956年毕业于[北京大学数学力学系](../Page/北京大学.md "wikilink")。1961年该系研究生毕业，师从[闵嗣鹤](../Page/闵嗣鹤.md "wikilink")。
  - 1961年起，历任山东大学教授、校长兼数学研究所所长。
  - 1991年，当选为中国科学院院士（数学物理学部）。
  - 1997年12月27日在[济南病逝](../Page/济南.md "wikilink")。
  - 2007年12月27日，为纪念潘承洞先生逝世十周年，潘承洞先生的铜像在[山东大学威海分校落成](../Page/山东大学威海分校.md "wikilink")。

## 学术评价

  - 其专长于[解析数论的研究](../Page/解析数论.md "wikilink")，尤以[哥德巴赫猜想研究著名](../Page/哥德巴赫猜想.md "wikilink")，与当代著名数学家[华罗庚](../Page/华罗庚.md "wikilink")、[王元](../Page/王元_\(院士\).md "wikilink")、[陈景润一起成为中国](../Page/陈景润.md "wikilink")[数论派的代表](../Page/数论.md "wikilink").
  - 其在从事哥德巴赫猜想的研究中，首先确定命题{1,C}中C的具体数值并证明命题{1,5}和{1,4}成立，此为证明命题{1,3}和{[1,2](../Page/陈氏定理.md "wikilink")}打下了基础。

## 参考资料

## 外部链接

  -
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:中華人民共和國數學家](../Category/中華人民共和國數學家.md "wikilink")
[Category:苏州人](../Category/苏州人.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:山东大学教授](../Category/山东大学教授.md "wikilink")
[Category:山东大学校长](../Category/山东大学校长.md "wikilink")
[Category:青岛大学校长](../Category/青岛大学校长.md "wikilink")
[C](../Category/潘姓.md "wikilink")

1.