**潘耀焯**（**Poon Yiu
Cheuk**，），生於[香港](../Page/香港.md "wikilink")，車路士足球學校（香港）教練，[香港職業足球員](../Page/香港.md "wikilink")，當時司職[左後衛](../Page/左後衛.md "wikilink")，曾經是[香港足球代表隊主力](../Page/香港足球代表隊.md "wikilink")[左後衛](../Page/左後衛.md "wikilink")，持有[亞洲足協B級足球教練牌照](../Page/亞洲足球協會.md "wikilink")。現效力[南華](../Page/南華足球隊.md "wikilink")，擔任球員兼主教練。

## 球員生涯

潘耀焯生於[香港](../Page/香港.md "wikilink")，出身於[流浪青年軍](../Page/香港流浪足球會.md "wikilink")。早年就讀[保良局姚連生中學](../Page/保良局姚連生中學.md "wikilink")。1996年開始於甲組上陣。一季之後，於1997年，潘耀焯轉投[愉園](../Page/愉園體育會.md "wikilink")，效力了11年之後。於2008年夏天，潘耀焯「上山」加盟[南華](../Page/南華足球隊.md "wikilink")。雖然2009年初開始，外界一直盛傳潘耀焯獲得前班主[林大輝力邀](../Page/林大輝.md "wikilink")，會在2009年夏天加盟來屆升上甲組的[沙田](../Page/沙田體育會足球隊.md "wikilink")，但由於[南華不肯放人](../Page/南華足球隊.md "wikilink")，有約在身的他最終亦決定繼續留隊。2011年夏天，潘耀焯加盟來屆升上甲組的[深水埗](../Page/深水埗體育會足球隊.md "wikilink")。後來因為與[深水埗教練李志堅不和](../Page/深水埗體育會足球隊.md "wikilink")，於2012年1月解約，轉投另一支地區球隊[天水圍飛馬](../Page/天水圍飛馬.md "wikilink")，兼任助理教練。\[1\]

## 國際賽生涯

1995年底，18歲的潘耀焯入選香港青年軍A隊參加日箭盃青年足球賽，並且奪得冠軍。1997年1月，潘耀焯代表[香港青年軍參加新春盃青年足球賽](../Page/香港.md "wikilink")。1997年10月，潘耀焯代表[香港奧運隊參加在](../Page/香港奧運足球代表隊.md "wikilink")[上海舉行的全運會足球賽](../Page/上海.md "wikilink")。1998年11月，潘耀焯首次入選[香港足球代表隊](../Page/香港足球代表隊.md "wikilink")，便以正選身份出戰[滬港盃](../Page/滬港盃.md "wikilink")，以及對[越南的友賽](../Page/越南國家足球隊.md "wikilink")，其後1998年12月更入選[釜山亞運](../Page/釜山亞運.md "wikilink")[香港足球代表隊大軍名單](../Page/香港足球代表隊.md "wikilink")，從此奠定[香港隊正選](../Page/香港足球代表隊.md "wikilink")[左後衛席位](../Page/左後衛.md "wikilink")。2011年1月，潘耀焯以隊長身份踢完[第33屆省港盃足球賽後](../Page/第33屆省港盃.md "wikilink")，宣布退出[香港足球代表隊](../Page/香港足球代表隊.md "wikilink")\[2\]。

## 教練生涯

2012年8月26日，潘耀焯宣佈退役，將智於[車路士足球學校(香港)展開教練生涯](../Page/車路士足球學校\(香港\).md "wikilink")。

## 個人榮譽

  - [香港足球明星選舉最佳年青球員](../Page/香港足球明星選舉.md "wikilink") 一次(1998-99季度)
  - [香港足球明星選舉最佳十一人](../Page/香港足球明星選舉.md "wikilink")
    四次(1999-00、2001-02、2003-04、2005-06季度)

## 職業生涯數據

| 球會表現                                                 | 聯賽                                          | [高級銀牌](../Page/香港足球高級組銀牌.md "wikilink") | [聯賽盃](../Page/香港聯賽盃.md "wikilink") | [足總盃](../Page/香港足總盃.md "wikilink") | [亞協盃](../Page/亞洲足協盃.md "wikilink") | 總計 |
| ---------------------------------------------------- | ------------------------------------------- | --------------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | -- |
| 球季                                                   | 球會                                          | 聯賽                                      | 上陣                                 | 入球                                 | 上陣                                 | 入球 |
| [1996 - 97](../Page/1996年－97年香港甲組足球聯賽.md "wikilink") | [UHLSPORT流浪](../Page/香港流浪足球會.md "wikilink") | [港甲](../Page/香港甲組足球聯賽.md "wikilink")    | ?                                  | ?                                  | ?                                  | ?  |
| 總計（流浪）                                               | ?                                           | ?                                       | ?                                  | ?                                  | \-                                 | ?  |
| [1997 - 98](../Page/1997年－98年香港甲組足球聯賽.md "wikilink") | [愉園](../Page/愉園體育會.md "wikilink")           | [港甲](../Page/香港甲組足球聯賽.md "wikilink")    | ?                                  | ?                                  | ?                                  | ?  |
| [1998 - 99](../Page/1998年－99年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | \-                                 | ?  |
| [1999 - 00](../Page/1999年－00年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | \-                                 | ?  |
| [2000 - 01](../Page/2000年－01年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | ?                                  | ?  |
| [2001 - 02](../Page/2001年－02年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | ?                                  | ?  |
| [2002 - 03](../Page/2002年－03年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | ?                                  | ?  |
| [2003 - 04](../Page/2003年－04年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | ?                                  | ?  |
| [2004 - 05](../Page/2004年－05年香港甲組足球聯賽.md "wikilink") | ?                                           | ?                                       | ?                                  | ?                                  | ?                                  | ?  |
| [2005 - 06](../Page/2005年－06年香港甲組足球聯賽.md "wikilink") | 10(1)                                       | 2                                       | 2                                  | 0                                  | 5                                  | 1  |
| [2006 - 07](../Page/2006年－07年香港甲組足球聯賽.md "wikilink") | 12(2)                                       | 2                                       | 1                                  | 0                                  | 5                                  | 2  |
| [2007 - 08](../Page/2007年－08年香港甲組足球聯賽.md "wikilink") | 17                                          | 3                                       | 3                                  | 0                                  | 4                                  | 2  |
| 總計（愉園）                                               | ≧39(3)                                      | ≧7                                      | ≧6                                 | ≧0                                 | ≧14                                | ≧5 |
| [2008 - 09](../Page/2008年－09年香港甲組足球聯賽.md "wikilink") | [南華](../Page/南華足球隊.md "wikilink")           | [港甲](../Page/香港甲組足球聯賽.md "wikilink")    | 18(1)                              | 0                                  | 2                                  | 0  |
| [2009 - 10](../Page/2009年－10年香港甲組足球聯賽.md "wikilink") | 8(2)                                        | 0                                       | 1                                  | 0                                  | \-                                 | 1  |
| 總計（南華）                                               | 26(3)                                       | 0                                       | 3                                  | 0                                  | 0                                  | 0  |
| 總計（職業生涯）                                             | ≧65(6)                                      | ≧7                                      | ≧9                                 | ≧0                                 | ≧14                                | ≧5 |

  - <small>更新至2010年5月25日</small>

## 參考資料

## 外部連結

  - [香港足球總會網站球員資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=109)
  - [南華足球隊官方網頁球員資料](http://www.southchinafc.com/template?series=3&article=3335)

[Y](../Category/潘姓.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:香港足球代表隊隊長](../Category/香港足球代表隊隊長.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:愉園球員](../Category/愉園球員.md "wikilink")
[Category:南華球員](../Category/南華球員.md "wikilink")
[Category:深水埗球員](../Category/深水埗球員.md "wikilink")
[Category:飛馬球員](../Category/飛馬球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:香港足球主教練](../Category/香港足球主教練.md "wikilink")
[Category:保良局姚連生中學校友](../Category/保良局姚連生中學校友.md "wikilink")

1.  [飛馬新援亮相
    潘耀焯加盟即倒戈](http://football.on.cc/football/new/20120131/fbnewc0102x0.html)
     *東方互動 - 波經*. 2012年1月31日.
2.  [潘耀焯宣布退出代表隊](http://www.hkfa.com/includes_files/board_details.php?news_id=8260)