**潘興縣**（**Pershing County,
Nevada**）是[美國](../Page/美國.md "wikilink")[內華達州西北部的一個縣](../Page/內華達州.md "wikilink")，面積15,715平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口6,753人。\[1\]潘興縣的縣治[拉夫洛克](../Page/拉夫洛克.md "wikilink")（Lovelock）。

潘興縣成立於1919年3月18日，是自[洪堡縣獨立的](../Page/洪堡縣_\(內華達州\).md "wikilink")，且是該州最晚成立的縣。縣名紀念[美國陸軍](../Page/美國陸軍.md "wikilink")[特級上將](../Page/特級上將.md "wikilink")[約翰·約瑟夫·潘興](../Page/約翰·約瑟夫·潘興.md "wikilink")。潘興縣被列為[內華達州歷史標記](../Page/內華達州歷史標記.md "wikilink")17號，而這個標記被置於[拉夫洛克的潘興縣法院內](../Page/拉夫洛克.md "wikilink")。\[2\]

## 地理

[Pershingcourt.JPG](https://zh.wikipedia.org/wiki/File:Pershingcourt.JPG "fig:Pershingcourt.JPG")的潘興縣法院\]\]
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，潘興縣的總面積為，其中有，即99.49為陸地；，即0.51%為水域。\[3\]潘興縣既是內華達州面積第八大的縣份，亦是[美國面積第42大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。\[4\]儘管潘興縣是內華達州面積第八大的縣份，潘興縣卻是內華達州人口第七少的縣份。\[5\]

### 毗鄰縣

因為潘興縣位於內華達州中部，因此所有毗鄰縣皆為內華達州的縣份。

  - [瓦肖縣](../Page/瓦肖縣.md "wikilink")：西方
  - [洪堡縣](../Page/洪堡縣_\(內華達州\).md "wikilink")：北方
  - [蘭德縣](../Page/蘭德縣.md "wikilink")：東方
  - [邱吉爾縣](../Page/邱吉爾縣.md "wikilink")：南方

### 國家保護區

  - [黑岩沙漠 -
    高岩峽谷步道移境國家保護區](../Page/黑岩沙漠_-_高岩峽谷步道移境國家保護區.md "wikilink")（部分）

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，潘興縣擁有6,693居民、1,962住戶和1,383家庭。\[6\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")1居民（每平方公里0.4居民）。潘興縣擁有2,389間房屋单位，其密度為每平方英里0.4間（每平方公里0.15間）。\[7\]潘興縣的人口是由77.69%[白人](../Page/歐裔美國人.md "wikilink")、5.35%[黑人](../Page/非裔美國人.md "wikilink")、3.42%[土著](../Page/美國土著.md "wikilink")、0.63%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.22%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、9.38%其他[種族和](../Page/種族.md "wikilink")3.3%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")19.33%。\[8\]

在1,962住户中，有38.4%擁有一個或以上的兒童（18歲以下）、57.2%為夫妻、7.3%為單親家庭、29.5%為非家庭、24.3%為獨居、8.6%住戶有同居長者。平均每戶有2.69人，而平均每個家庭則有3.22人。在6,693居民中，有25.7%為18歲以下、8.5%為18至24歲、36%為25至44歲、22.1%為45至64歲以及7.8%為65歲以上。人口的年齡中位數為34歲。此縣的男女比例非常懸殊\[9\]，每100個女性就有158.8個男性。而每100個成年女性（18歲以上）就有182.1個成年男性。\[10\]

潘興縣的住戶收入中位數為$40,670，而家庭收入中位數則為$46,268。男性的收入中位數為$34,417，而女性的收入中位數則為$24,301。潘興縣的[人均收入為](../Page/人均收入.md "wikilink")$16,589。約10.2%家庭和11.4%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括14.2%兒童（18歲以下）及5.6%長者（65歲以上）。\[11\]

## 參考文獻

[P](../Category/内华达州行政区划.md "wikilink")

1.  [quickfacts
    census](http://quickfacts.census.gov/qfd/states/32/32027.html)

2.

3.

4.

5.   (2000 Census)

6.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

7.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

8.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

9.  [Gender in the United
    States](http://nationalatlas.gov/articles/people/a_gender.html)

10. [American FactFinder](http://factfinder.census.gov)

11.