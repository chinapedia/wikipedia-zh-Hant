[Zac_Purton.JPG](https://zh.wikipedia.org/wiki/File:Zac_Purton.JPG "fig:Zac_Purton.JPG")
**潘頓**（**Zac Purton**，原名**Zachary
Purton**\[1\]、），人稱「潘狗」，是[澳洲騎師](../Page/澳洲.md "wikilink")，自2007－2008年馬季開始以[香港作為策騎基地](../Page/香港.md "wikilink")，至今因多次策騎熱門馬匹卻三甲不入而令馬會收入大增。

最近一次「最強戰績」是2019年4月17日星期三，於谷草賽事策騎大熱馬匹「肥仔威威」，卻以最後一名完成比賽。事後馬匹經獸醫檢查後並無發現問題，為潘頓證明其實力騎功及第N場拉馬輝煌戰績。

## 簡歷

潘頓是[澳洲](../Page/澳洲.md "wikilink")[騎師](../Page/騎師.md "wikilink")，以騎功硬朗及純熟的車輪鞭技巧策騎見稱。2003年，潘頓以見習騎師身份奪得[澳洲](../Page/澳洲.md "wikilink")[布里斯本城市冠軍騎師殊榮](../Page/布里斯本賽馬會.md "wikilink")，其後他轉到[澳洲](../Page/澳洲.md "wikilink")[悉尼策騎](../Page/悉尼.md "wikilink")，排名只僅次於冠軍[白德民](../Page/白德民.md "wikilink")。

## 在港策騎主要事跡

潘頓於2007-2008年度馬季起來港發展，並於同年9月12日憑策騎「聚寶駒」贏得在港的首場頭馬。潘頓於首季取得29場頭馬勝仗，其後每季成績均穩步上揚。

2008年6月5日，潘頓於[跑馬地馬場先後憑策騎](../Page/跑馬地馬場.md "wikilink")「彪形大漢」、「奪寶群英」及「鑽飾年代」取勝，令他首度封[騎師王](../Page/騎師王.md "wikilink")。

2008年6月15日，潘頓憑策騎「[步步穩](../Page/步步穩.md "wikilink")」，攻下本地三級賽[精英盃冠軍](../Page/精英盃.md "wikilink")，取得在港首項級制賽錦標。

潘頓於2008-2009、2009-2010及2010-2011年度的三個馬季分別取得43場，48場及53場的頭馬勝仗，但仍然落後於[韋達及](../Page/韋達_\(騎師\).md "wikilink")[柏寶之後](../Page/柏寶.md "wikilink")，只能夠於騎師榜取得第三名。

2011-2012年馬季，潘頓的策騎表現繼續進步，並在該季取得了64場頭馬勝仗，取代[柏寶成為該季亞軍騎師](../Page/柏寶.md "wikilink")。

2012年11月26日，潘頓於[日本首次角逐](../Page/日本.md "wikilink")中，並在當天取得兩場頭馬勝仗，擊敗了[濱中俊及](../Page/濱中俊.md "wikilink")[莫雅成為該項賽事冠軍](../Page/莫雅.md "wikilink")。

2012年12月5日，潘頓首次代表[香港角逐在](../Page/香港.md "wikilink")[跑馬地馬場舉行的](../Page/跑馬地馬場.md "wikilink")[國際騎師錦標賽](../Page/國際騎師錦標賽.md "wikilink")。

2012-2013年度馬季，潘頓大部分時間均在騎師榜上取得領先優勢，並取得了88場頭馬勝仗的佳績，只是他在季尾逐漸失去領先優勢，並被[韋達逐漸追趕](../Page/韋達_\(騎師\).md "wikilink")。最終潘頓以13場頭馬勝仗之差不敵[韋達](../Page/韋達_\(騎師\).md "wikilink")，再次屈居於騎師榜次席，未能成為當屆[香港冠軍騎師](../Page/香港冠軍騎師.md "wikilink")。

2014年10月26日，潘頓於[沙田馬場先後憑策騎](../Page/沙田馬場.md "wikilink")「威風猛將」、「靚猴王」、「[友瑩格](../Page/友瑩格.md "wikilink")」、「[軍事出擊](../Page/軍事出擊.md "wikilink")」及「包裝鬥士」取勝，個人首次在港單日勝出五場頭馬，當中他更憑「[友瑩格](../Page/友瑩格.md "wikilink")」及「[軍事出擊](../Page/軍事出擊.md "wikilink")」先後攻下[精英碗及](../Page/精英碗.md "wikilink")[沙田錦標兩項級制賽盃賽](../Page/沙田錦標.md "wikilink")。

2015年3月15日，潘頓於[沙田馬場憑策騎](../Page/沙田馬場.md "wikilink")[蔡約翰馬房的賽駒](../Page/蔡約翰.md "wikilink")「[戰利品](../Page/戰利品_\(馬\).md "wikilink")」，攻下個人自來港發展以來首次的[香港打吡大賽冠軍](../Page/香港打吡大賽.md "wikilink")。\[2\]

2015年3月25日，潘頓於[沙田馬場第三場賽事憑策騎](../Page/沙田馬場.md "wikilink")[賀賢馬房的](../Page/賀賢.md "wikilink")「怡昌之星」，取得個人自來港發展以來的第500場頭馬勝仗。

2015年3月29日，潘頓於[中京競馬場策騎](../Page/中京競馬場.md "wikilink")「[友瑩格](../Page/友瑩格.md "wikilink")」，成功克服不利場地，為[香港賽駒首次奪得](../Page/香港.md "wikilink")[高松宮紀念賽冠軍](../Page/高松宮紀念賽.md "wikilink")，揚威[日本](../Page/日本.md "wikilink")。\[3\]

2015年5月17日，潘頓於[新加坡](../Page/新加坡.md "wikilink")[克蘭芝馬場憑策騎](../Page/克蘭芝馬場.md "wikilink")「[友瑩格](../Page/友瑩格.md "wikilink")」，首度攻下[KrisFlyer國際短途錦標](../Page/KrisFlyer國際短途錦標.md "wikilink")。季內人馬合作下分別在[香港](../Page/香港.md "wikilink")、[日本及](../Page/日本.md "wikilink")[新加坡三度勝出一級賽](../Page/新加坡.md "wikilink")，成為[香港賽馬史上首匹於一季內於三地勝出國際一級賽的賽駒](../Page/香港.md "wikilink")。\[4\]

2015年7月8日至8月9日，潘頓趁香港馬季休季期間到[日本作短期客串](../Page/日本.md "wikilink")，並在其間取得了7場頭馬勝仗。

2016年6月1日，潘頓於[跑馬地馬場尾場賽事憑策騎](../Page/跑馬地馬場.md "wikilink")[賀賢馬房的](../Page/賀賢.md "wikilink")「勇駒」，取得個人來港發展以來的第600場頭馬勝仗。\[5\]

2016年12月11日，潘頓分別策騎「[友瑩格](../Page/友瑩格.md "wikilink")」和「[美麗大師](../Page/美麗大師.md "wikilink")」攻下[香港短途錦標及](../Page/香港短途錦標.md "wikilink")[香港一哩錦標](../Page/香港一哩錦標.md "wikilink")。

2017年5月21日，潘頓於[沙田馬場第五場賽事憑策騎](../Page/沙田馬場.md "wikilink")[葉楚航馬房的](../Page/葉楚航.md "wikilink")「中華之光」，取得個人自來港發展以來的第700場頭馬勝仗。\[6\]

2017年10月14日，潘頓於[沙田馬場第五場賽事憑策騎](../Page/沙田馬場.md "wikilink")[賀賢馬房的](../Page/賀賢.md "wikilink")「小巨人」，替練者取得來港發展以來的第400場頭馬勝仗。\[7\]

2017年12月6日，潘頓在[跑馬地馬場上演的](../Page/跑馬地馬場.md "wikilink")[國際騎師錦標賽中](../Page/國際騎師錦標賽.md "wikilink")，於頭關賽事先憑策騎[沈集成馬房的](../Page/沈集成.md "wikilink")「大英雄」取勝，其後在第三關賽事再憑策騎[何良馬房的](../Page/何良.md "wikilink")「放眼量」取勝，最終他取得24分，個人首度奪得[國際騎師錦標賽總冠軍](../Page/國際騎師錦標賽.md "wikilink")。

2017年12月10日，潘頓於[沙田馬場憑策騎](../Page/沙田馬場.md "wikilink")[告東尼馬房的](../Page/告東尼.md "wikilink")「[馬克羅斯](../Page/馬克羅斯_\(馬\).md "wikilink")」攻下[香港盃](../Page/香港盃.md "wikilink")，個人贏盡[香港國際賽事全部四項大賽冠軍](../Page/香港.md "wikilink")（[香港短途錦標](../Page/香港短途錦標.md "wikilink")、[香港一哩錦標](../Page/香港一哩錦標.md "wikilink")、[香港盃及](../Page/香港盃.md "wikilink")[香港瓶](../Page/香港瓶.md "wikilink")），繼[巫斯義及](../Page/巫斯義.md "wikilink")[莫雷拉後成為另一位取得](../Page/莫雷拉.md "wikilink")[香港國際賽大滿貫的騎師](../Page/香港.md "wikilink")。\[8\]

2018年4月18日，潘頓於[跑馬地馬場頭場賽事憑策騎](../Page/跑馬地馬場.md "wikilink")[霍利時馬房的](../Page/霍利時.md "wikilink")「雷超」，取得個人自來港發展以來的第800場頭馬勝仗。
\[9\]

2018年12月9日，潘頓於[沙田馬場策騎](../Page/沙田馬場.md "wikilink")[告東尼馬房的](../Page/告東尼.md "wikilink")「[時時精綵](../Page/時時精綵.md "wikilink")」，座騎在他胯下於初段一直佔取較前位置，至最後直路一百五十米處，「[時時精綵](../Page/時時精綵.md "wikilink")」一度被[莫雷拉策騎的](../Page/莫雷拉.md "wikilink")[日本雌馬](../Page/日本.md "wikilink")「雍容白荷」超越，但此時「[時時精綵](../Page/時時精綵.md "wikilink")」回氣再上，最終以馬頸位之先擊退「雍容白荷」。潘頓繼2013年憑策騎「[多名利](../Page/多名利.md "wikilink")」為[香港摘下](../Page/香港.md "wikilink")[香港瓶後](../Page/香港瓶.md "wikilink")，事隔五年後再為[香港攻下此項國際賽盃賽](../Page/香港.md "wikilink")，亦替練者[告東尼完成囊括香港國際賽事全部四項大賽冠軍](../Page/告東尼.md "wikilink")（[香港短途錦標](../Page/香港短途錦標.md "wikilink")、[香港一哩錦標](../Page/香港一哩錦標.md "wikilink")、[香港盃及](../Page/香港盃.md "wikilink")[香港瓶](../Page/香港瓶.md "wikilink")）的壯舉。\[10\]
同日，潘頓憑策騎[約翰摩亞馬房的馬王](../Page/約翰摩亞.md "wikilink")「[美麗傳承](../Page/美麗傳承.md "wikilink")」，以三個馬位之先擊敗[日本雌馬](../Page/日本.md "wikilink")「強擊」，輕鬆勝出[香港一哩錦標](../Page/香港一哩錦標.md "wikilink")。\[11\]

2018年12月16日，潘頓於[沙田馬場尾場賽事憑策騎](../Page/沙田馬場.md "wikilink")[約翰摩亞馬房的](../Page/約翰摩亞.md "wikilink")「標準猴王」，取得個人自來港發展以來的第900場頭馬勝仗。

2019年1月27日，潘頓於[沙田馬場憑策騎](../Page/沙田馬場.md "wikilink")[告東尼馬房的](../Page/告東尼.md "wikilink")「[時時精綵](../Page/時時精綵.md "wikilink")」勝出，個人首度攻下三級賽[百週年紀念銀瓶外](../Page/百週年紀念銀瓶.md "wikilink")，亦為練者[告東尼帶來從練第](../Page/告東尼.md "wikilink")1,200場頭馬。\[12\]
\[13\]

2019年2月24日，潘頓於[沙田馬場大發神威](../Page/沙田馬場.md "wikilink")，先後憑策騎「勁飛寶寶」、「志趣相投」、「中華之光」、「我𢥧意」、「天池怪俠」及「美麗滿載」奏凱而回，個人在港首度單日勝出六場頭馬。他亦成為繼[柏寶](../Page/柏寶.md "wikilink")、[韋達及](../Page/韋達_\(騎師\).md "wikilink")[莫雷拉後](../Page/莫雷拉.md "wikilink")，第四位在港取得單日六捷佳績的騎師。\[14\]

-----

-----

### 首度登上冠軍寶座

2013-2014年度馬季，隨著[莫雷拉於](../Page/莫雷拉.md "wikilink")10月份來投，令馬圈競爭局勢出現變化，不再由[韋達壟斷冠軍](../Page/韋達_\(騎師\).md "wikilink")。[莫雷拉在港上陣以來](../Page/莫雷拉.md "wikilink")，連翻交出好成績，短短數月間已躋身騎師榜前列位置，給予潘頓不少壓力。不過，潘頓最終勝出112場頭馬，以15場頭馬之差擊敗[莫雷拉](../Page/莫雷拉.md "wikilink")，首度登上[香港冠軍騎師殊榮](../Page/香港冠軍騎師.md "wikilink")，打破了[韋達過去十三個馬季以來的壟斷局面](../Page/韋達_\(騎師\).md "wikilink")。潘頓該季錄得112場頭馬勝仗，是繼[韋達後第二位能夠在一季內勝出超過](../Page/韋達_\(騎師\).md "wikilink")100場頭馬的[騎師](../Page/騎師.md "wikilink")。此外，潘頓在這季僅花了4個月左右，便取得了50場頭馬勝仗，令他成為歷來首位在港策騎的騎師中，以最快時間贏得50場頭馬的[騎師](../Page/騎師.md "wikilink")。

-----

-----

### 事隔三季，再度登上冠軍寶座

2017-2018年度馬季，潘頓與[莫雷拉的冠軍爭奪戰異常激烈](../Page/莫雷拉.md "wikilink")，尤其是在季尾階段雙方爭奪戰更見精彩。原本潘頓在冠軍[騎師爭奪戰中一直處於下風](../Page/騎師.md "wikilink")，與季初至季中階段一直被[莫雷拉拋離十場以上頭馬](../Page/莫雷拉.md "wikilink")。不過，潘頓在季尾階段逐漸回勇，加上[莫雷拉在此時表現較為遜色](../Page/莫雷拉.md "wikilink")，令他收復了不少失地。2018年5月20日，潘頓於[沙田馬場先後憑策騎](../Page/沙田馬場.md "wikilink")「玩得喜」、「錶之太陽」、「滿地可」及「跳出香港」取勝，全日累積四捷，反觀[莫雷拉於同日未能增添任何頭馬進帳](../Page/莫雷拉.md "wikilink")，令雙方的頭馬差距進一步收窄至只得4場。\[15\]
2018年5月27日，潘頓於[沙田馬場再度大發神威](../Page/沙田馬場.md "wikilink")，先後憑策騎「霸勝」、「陽光」、「[勝得威](../Page/勝得威.md "wikilink")」、「勝利專家」及「白鷺飛翔」獲勝，全日豪取五捷，當中在第四場策騎「[勝得威](../Page/勝得威.md "wikilink")」更攻下三級賽[沙田銀瓶](../Page/沙田銀瓶.md "wikilink")，為座騎首度勝出級制賽賽事。至於第六場賽事，潘頓策騎[約翰摩亞馬房](../Page/約翰摩亞.md "wikilink")「勝利專家」勝出的一仗，亦為練者取得在港從練以來的第1,600場頭馬勝仗。\[16\]
至於[莫雷拉在同日僅憑策騎](../Page/莫雷拉.md "wikilink")「精算風暴」贏得一場頭馬，令潘頓與他的頭馬差距進一步收窄至只得2場。2018年6月10日，潘頓於[沙田馬場趁](../Page/沙田馬場.md "wikilink")[莫雷拉停賽期間](../Page/莫雷拉.md "wikilink")，先後憑策騎「小鳥敖翔」及「友誼之星」取勝起孖，季內累積頭馬增至117場，以一場之差反壓[莫雷拉](../Page/莫雷拉.md "wikilink")，季內首度登上騎師榜榜首位置。\[17\]潘頓其後表現穩定，保持優勢且從未給予[莫雷拉任何機會反超前](../Page/莫雷拉.md "wikilink")，最終他贏得創個人單季頭馬紀錄的136場頭馬，以兩場之差擊敗[莫雷拉](../Page/莫雷拉.md "wikilink")，事隔三季後再度登上[香港冠軍騎師寶座](../Page/香港冠軍騎師.md "wikilink")。\[18\]

## 主要勝出賽事

**[英國](../Page/英國.md "wikilink")**

  - [皇席錦標](../Page/皇席錦標.md "wikilink")- (1) -
    [小橋流水](../Page/小橋流水.md "wikilink") (2012)

-----

-----

**[澳洲](../Page/澳洲.md "wikilink")**

  - [高明錦標](../Page/高明錦標.md "wikilink")- (1) - Tsuimai (2003)

  - [山倫讓賽](../Page/山倫讓賽.md "wikilink")- (1) - Sportsman (2003)

  - (1) - Sportsman (2003)

  - [李智讓賽](../Page/李智讓賽.md "wikilink") - (1) - Zabarra (2003)

  - [舒拉高錦標](../Page/舒拉高錦標.md "wikilink")- (1) - Oomph (2003)

  - \- (1) - 德誠爵士 (2005)

  - \- (1) - Collate (2006)

  - [育馬者經典賽](../Page/育馬者經典賽.md "wikilink")- (1) - Steflara (2006)

  - [王子錦標](../Page/王子錦標.md "wikilink") - (1) - Strada (2006)

  - [育馬錦標](../Page/育馬錦標.md "wikilink")- (2) - Excites (2006)，北美薔薇 (2016)

  - (1) - Coolroom Candidate (2006)

  - [卡賓會錦標](../Page/卡賓會錦標.md "wikilink")- (1) - Belmonte (2006)

  - (1) - Belmonte (2006)

  - \- (1) - He's No Pie Eater (2007)

  - \- (1) - 生命火花 (2007)

  - \- (1) - High Cee (2007)

  - [首演錦標](../Page/首演錦標.md "wikilink")- (1) - German Chocolate (2007)

  - [福克斯錦標](../Page/福克斯錦標.md "wikilink") - (1) - A Country Girl (2007)

  - [萬利安盃](../Page/萬利安盃.md "wikilink")- (1) - The Chieftain (2007)

  - (1) - Theseo (2008)

  - \- (2) -  (2014)，有點兒 (2017)

  - \- (1) - 聖靈瀑布 (2014)

  - \- (1) -  (2014)

  - [阿聯酋碟](../Page/龍舌蘭錦標.md "wikilink")- (1) - 隱惡揚善 (2015)

  - [阿堅斯錦標](../Page/阿堅斯錦標.md "wikilink")- (1) - 聖靈妙丹 (2016)

  - [彈力澎湃錦標](../Page/彈力澎湃錦標.md "wikilink")- (1) - 玉燕投懷 (2016)

  - [星河大賽](../Page/星河大賽.md "wikilink")- (1) - 芳華正茂 (2018)

-----

-----

**[新加坡](../Page/新加坡.md "wikilink")**

  - [新航國際盃](../Page/新航國際盃.md "wikilink")- (1) -
    [軍事出擊](../Page/軍事出擊.md "wikilink") (2013)

  - [KrisFlyer國際短途錦標](../Page/KrisFlyer國際短途錦標.md "wikilink")- (1) -
    [友瑩格](../Page/友瑩格.md "wikilink") (2015)

  - [新加坡堅尼大賽](../Page/新加坡堅尼大賽.md "wikilink")- (1) - 克林 (2018)

  - \- (1) - 川河尊駒 (2018)

-----

-----

**[日本](../Page/日本.md "wikilink")**

  - [高松宮紀念賽](../Page/高松宮紀念賽.md "wikilink")- (1) -
    [友瑩格](../Page/友瑩格.md "wikilink") (2015)

-----

-----

**[香港](../Page/香港.md "wikilink")**

  - [精英盃](../Page/精英盃.md "wikilink")- (1) -
    [步步穩](../Page/步步穩.md "wikilink") (2008)
  - [莎莎婦女銀袋賽](../Page/莎莎婦女銀袋賽.md "wikilink")- (2) - 爆炸
    (2008)，[馬克羅斯](../Page/馬克羅斯_\(馬\).md "wikilink") (2018)
  - [馬會一哩錦標](../Page/馬會一哩錦標.md "wikilink")- (3) -
    [友誼至上](../Page/友誼至上.md "wikilink")
    (2009)，[美麗大師](../Page/美麗大師.md "wikilink")
    (2016)，[美麗傳承](../Page/美麗傳承.md "wikilink") (2018)
  - [新馬短途錦標](../Page/新馬短途錦標.md "wikilink")- (1) - 虎虎生輝 (2009)
  - [沙田銀瓶](../Page/沙田銀瓶.md "wikilink")- (3) - 威武雄獅
    (2009)，[友瑩格](../Page/友瑩格.md "wikilink")
    (2014)，[勝得威](../Page/勝得威.md "wikilink") (2018)
  - [跑馬地銀瓶](../Page/跑馬地銀瓶.md "wikilink")- (4) - 玉峽生輝 (2009)，金碧亮馬
    (2010)，勝哥兒 (2014，2015)
  - [董事盃](../Page/董事盃.md "wikilink")- (2) -
    [友誼至上](../Page/友誼至上.md "wikilink")
    (2010)，[美麗傳承](../Page/美麗傳承.md "wikilink") (2019)
  - [華商會挑戰盃](../Page/華商會挑戰盃.md "wikilink")- (2) -
    [小橋流水](../Page/小橋流水.md "wikilink") (2011)，喜蓮標緻
    (2013)
  - [跑馬地錦標](../Page/跑馬地錦標.md "wikilink")- (3) - 此時此刻 (2011)，控制者
    (2014，2016)
  - [洋紫荊短途錦標](../Page/洋紫荊短途錦標.md "wikilink")- (2) -
    [小橋流水](../Page/小橋流水.md "wikilink")
    (2012)，[新力風](../Page/新力風.md "wikilink") (2016)
  - [短途錦標](../Page/短途錦標.md "wikilink")- (1) -
    [小橋流水](../Page/小橋流水.md "wikilink") (2012)
  - [香港一哩錦標](../Page/香港一哩錦標.md "wikilink")- (3) -
    [雄心威龍](../Page/雄心威龍.md "wikilink")
    (2012)，[美麗大師](../Page/美麗大師.md "wikilink")
    (2016)，[美麗傳承](../Page/美麗傳承.md "wikilink") (2018)
  - [沙田錦標](../Page/沙田錦標.md "wikilink")- (3) -
    [雄心威龍](../Page/雄心威龍.md "wikilink")
    (2012)，[軍事出擊](../Page/軍事出擊.md "wikilink")
    (2014)，[美麗傳承](../Page/美麗傳承.md "wikilink") (2018)
  - [香港特區行政長官盃](../Page/香港特區行政長官盃.md "wikilink")- (3) - 富高勝 (2012)，愛跑得
    (2013)，[勝得威](../Page/勝得威.md "wikilink") (2018)
  - [廣東讓賽盃](../Page/廣東讓賽盃.md "wikilink")- (1) - 薑餅人 (2012)
  - [皇太后紀念盃](../Page/皇太后紀念盃.md "wikilink")- (2) -
    [多名利](../Page/多名利.md "wikilink")
    (2013)，[時時精綵](../Page/時時精綵.md "wikilink") (2018)
  - [香港瓶](../Page/香港瓶.md "wikilink")- (2) -
    [多名利](../Page/多名利.md "wikilink")
    (2013)，[時時精綵](../Page/時時精綵.md "wikilink") (2018)
  - [香港金盃](../Page/香港金盃.md "wikilink")- (3) -
    [軍事出擊](../Page/軍事出擊.md "wikilink")
    (2013)，[馬克羅斯](../Page/馬克羅斯_\(馬\).md "wikilink")
    (2018)，[時時精綵](../Page/時時精綵.md "wikilink") (2019)
  - [精英碟](../Page/精英碟.md "wikilink")- (2) -
    [軍事出擊](../Page/軍事出擊.md "wikilink")
    (2013)，[時時精綵](../Page/時時精綵.md "wikilink") (2018)
  - [女皇銀禧紀念盃](../Page/女皇銀禧紀念盃.md "wikilink")- (3) -
    [雄心威龍](../Page/雄心威龍.md "wikilink")
    (2013)，[美麗傳承](../Page/美麗傳承.md "wikilink") (2018、2019)
  - [香港賽馬會社群盃](../Page/香港賽馬會社群盃.md "wikilink")- (1) - 好靚仔 (2013)
  - [精英碗](../Page/精英碗.md "wikilink")- (1) -
    [友瑩格](../Page/友瑩格.md "wikilink") (2014)
  - [香港短途錦標](../Page/香港短途錦標.md "wikilink")- (2) -
    [友瑩格](../Page/友瑩格.md "wikilink") (2014，2016)
  - [新馬錦標](../Page/新馬錦標.md "wikilink")- (2) - 活力小駒 (2014)，美麗寶貝 (2017)
  - [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")- (1) -
    [戰利品](../Page/戰利品_\(馬\).md "wikilink") (2015)
  - [馬會盃](../Page/馬會盃.md "wikilink")- (1) -
    [軍事出擊](../Page/軍事出擊.md "wikilink") (2015)
  - [香港回歸盃](../Page/香港回歸盃_\(香港賽馬\).md "wikilink")- (1) -
    [準備就緒](../Page/準備就緒.md "wikilink") (2015)
  - [港澳盃](../Page/港澳盃.md "wikilink")- (1) -
    [飛來猛](../Page/飛來猛.md "wikilink") (2016)
  - [百週年紀念短途盃](../Page/百週年紀念短途盃.md "wikilink")- (1) -
    [友瑩格](../Page/友瑩格.md "wikilink") (2016)
  - [麥蘊利盃](../Page/麥蘊利盃.md "wikilink")- (1) -
    [勝得威](../Page/勝得威.md "wikilink") (2017)
  - [一月盃](../Page/一月盃.md "wikilink")- (2) - 活力歡騰
    (2017)，[鷹雄](../Page/鷹雄.md "wikilink") (2018)
  - [香港盃](../Page/香港盃.md "wikilink")- (1) -
    [馬克羅斯](../Page/馬克羅斯_\(馬\).md "wikilink") (2017)
  - [冠軍一哩賽](../Page/冠軍一哩賽.md "wikilink")- (1) -
    [美麗傳承](../Page/美麗傳承.md "wikilink") (2018)
  - [主席短途獎](../Page/主席短途獎.md "wikilink")- (1) -
    [天下為攻](../Page/天下為攻.md "wikilink") (2018)
  - [沙田一哩錦標](../Page/沙田一哩錦標.md "wikilink")- (1) - 跳出香港 (2018)
  - [慶典盃](../Page/慶典盃.md "wikilink")- (1) -
    [美麗傳承](../Page/美麗傳承.md "wikilink") (2018)
  - [百週年紀念銀瓶](../Page/百週年紀念銀瓶.md "wikilink")- (1) -
    [時時精綵](../Page/時時精綵.md "wikilink") (2019)
  - [主席錦標](../Page/主席錦標.md "wikilink")- (1) -
    [美麗傳承](../Page/美麗傳承.md "wikilink") (2019)

-----

-----

**[澳門](../Page/澳門.md "wikilink")**

  - [新葡京盃](../Page/新葡京盃.md "wikilink")- (1) - 太容易 (2017)
  - [澳港盃](../Page/澳港盃.md "wikilink")- (1) - 醒目名駒 (2017)
  - [澳門打吡大賽](../Page/澳門打吡大賽.md "wikilink")- (1) - 非凡夫 (2017)

-----

-----

## 成就

  - [澳洲](../Page/澳洲.md "wikilink")[布里斯本冠軍騎師](../Page/昆士蘭賽馬會.md "wikilink")
    (2002/2003年度馬季)
  - [世界超級騎師大賽冠軍](../Page/世界超級騎師大賽.md "wikilink")- (1) - (2012年)
  - [香港冠軍騎師](../Page/香港冠軍騎師.md "wikilink")- (2) -
    (2013/2014年度馬季、2017/2018年度馬季)
  - [國際騎師錦標賽冠軍](../Page/國際騎師錦標賽.md "wikilink")- (1) - (2017年)

-----

-----

## 在港策騎成績

| 年度        | 冠   | 亞   | 季  | 殿  | 第五 | 出賽次數 | 所贏獎金（港元）     | 備註            |
| --------- | --- | --- | -- | -- | -- | ---- | ------------ | ------------- |
| 2007-2008 | 29  | 23  | 30 | 26 | 43 | 420  | $22,437,475  | 策騎首季          |
| 2008-2009 | 43  | 40  | 37 | 37 | 53 | 489  | $37,035,812  | 第2季 (季軍)      |
| 2009-2010 | 48  | 56  | 38 | 50 | 37 | 518  | $48,676,250  | 第3季 (季軍)      |
| 2010-2011 | 53  | 40  | 45 | 57 | 45 | 527  | $41,472,500  | 第4季           |
| 2011-2012 | 64  | 53  | 58 | 42 | 37 | 482  | $50,993,250  | 第5季 (亞軍)      |
| 2012-2013 | 88  | 86  | 51 | 52 | 31 | 550  | $94,153,400  | 第6季 (亞軍)      |
| 2013-2014 | 112 | 86  | 68 | 56 | 53 | 639  | $101,465,937 | 第7季 **(冠軍)**  |
| 2014-2015 | 95  | 66  | 41 | 40 | 34 | 481  | $107,683,325 | 第8季 (亞軍)      |
| 2015-2016 | 80  | 74  | 49 | 51 | 43 | 531  | $97,453,000  | 第9季 (亞軍)      |
| 2016-2017 | 107 | 92  | 60 | 48 | 50 | 604  | $140,821,315 | 第10季 (亞軍)     |
| 2017-2018 | 136 | 107 | 64 | 60 | 47 | 635  | $181,824,240 | 第11季 **(冠軍)** |
|           |     |     |    |    |    |      |              |               |

-----

-----

## 軼事

2009年及2010年，潘頓兩度與[香港賽馬會不受歡迎人士馮鶴翔接觸利誘售賣貼士](../Page/香港賽馬會.md "wikilink")，雖然潘頓拒絕要求，但因未有向[香港賽馬會申報](../Page/香港賽馬會.md "wikilink")，故此他在2011年6月被[香港賽馬會罰款](../Page/香港賽馬會.md "wikilink")30萬港元。\[19\]

2017年6月21日跑馬地夜賽，潘頓於第五場盃賽場合賽事策騎「晚風」勝出後捧盃，他在覆磅後走到頒獎台期間被[香港賽馬會禮儀小姐催促盡快到達頒獎台完成領獎](../Page/香港賽馬會.md "wikilink")，但潘頓不滿並向禮儀小姐爆出[粗口](../Page/粗口.md "wikilink")，結果他被[香港賽馬會罰款](../Page/香港賽馬會.md "wikilink")10萬港元。\[20\]

2018年10月28日，潘頓於[社交網站發表一則](../Page/社交網站.md "wikilink")[推特訊息](../Page/推特.md "wikilink")，內容批評[騎師](../Page/騎師.md "wikilink")[利敬國於同日在](../Page/利敬國.md "wikilink")[跑馬地馬場策騎](../Page/跑馬地馬場.md "wikilink")「千金一諾」時的騎法，並揶揄[利敬國不懂控制步速](../Page/利敬國.md "wikilink")。雖然潘頓在翌戰策騎「千金一諾」時順利勝出，但他仍被馬會競賽董事小組裁定違反賽事規例第155（5）條，有關不適當行為的指控，結果他被罰款5,000港元。
\[21\]

潘頓雖然騎功出色，但其陣上走位卻不時令馬迷大惑不解，即使在比賽進入直路衝刺期間其座騎附近出現空位，但他仍喜愛將一些較為熱門的馬匹移向內欄位置，令馬匹在沒有足夠空間和位置衝刺的情況下而失去爭勝機會。這種情況在2019年1月1日的賽馬日尾場賽事，潘頓策騎「鑽飾明駝」時出現。2019年1月6日的賽馬日，潘頓策騎大熱門「暴風俠」及「狀元才」時亦出現。

-----

-----

## 其他

自2015-16年馬季起於潘頓養傷及停賽期間，他會客串英語評述\[22\]。

## 參考資料

## 外部連結

  - [騎師資料
    潘頓](http://www.hkjc.com/chinese/racing/JockeyWinStat.asp?JockeyCode=PZ)

[Category:香港騎師](../Category/香港騎師.md "wikilink")
[Category:香港冠軍騎師](../Category/香港冠軍騎師.md "wikilink")
[Category:澳洲騎師](../Category/澳洲騎師.md "wikilink")
[Category:澳大利亞裔香港人](../Category/澳大利亞裔香港人.md "wikilink")
[Category:在香港的澳大利亞人](../Category/在香港的澳大利亞人.md "wikilink")

1.  [Tony Cruz backs Beauty Only after solid effort in Yasuda Kinen as
    HK again falls short in
    Japan](https://www.scmp.com/sport/racing/article/2096845/tony-cruz-backs-beauty-only-after-creditable-effort-yasuda-kinen-hong)
    南華早報
2.   潘頓夥拍「戰利品」首次勝出寶馬香港打吡大賽
3.  <http://www.hkjc.com/chinese/corporate/racing_news_item.asp?in_file=/chinese/news/2015-03/news_2015032901802.html>
    「友瑩格」勇奪高松宮紀念賽締造歷史
4.  <http://www.hkjc.com/chinese/corporate/racing_news_item.asp?in_file=/chinese/news/2015-05/news_2015051602129.html>
    「友瑩格」勇奪KrisFlyer國際短途錦標
5.  <http://racing.on.cc/news.html?id=20160601225147> 【戰況R8】潘頓7檔變1檔
    慳晒位闖600W
6.  <http://racing.on.cc/news.html?id=20170521152650> 【戰況R5】澳門威到香港
    潘頓贏在港700W
7.  【戰況R9】有片！賀賢400W都唔理 趕去錫愛妻！
    <http://racing.on.cc/news.html?id=20171014174134>
8.
9.  <http://racing.on.cc/news.html?id=20180418193739> 【戰況R1】復出即創里程碑
    潘頓勝在港800W
10. 「時時精綵」力壓「雍容白荷」勇奪浪琴表香港瓶
    <https://racingnews.hkjc.com/chinese/2018/12/09/%e3%80%8c%e6%99%82%e6%99%82%e7%b2%be%e7%b6%b5%e3%80%8d%e5%8a%9b%e5%a3%93%e3%80%8c%e9%9b%8d%e5%ae%b9%e7%99%bd%e8%8d%b7%e3%80%8d%e5%8b%87%e5%a5%aa%e6%b5%aa%e7%90%b4%e8%a1%a8%e9%a6%99%e6%b8%af%e7%93%b6/>
11. 「美麗傳承」輕鬆蟬聯浪琴表香港一哩錦標
    <https://racingnews.hkjc.com/chinese/2018/12/09/%e3%80%8c%e7%be%8e%e9%ba%97%e5%82%b3%e6%89%bf%e3%80%8d%e8%bc%95%e9%ac%86%e8%9f%ac%e8%81%af%e6%b5%aa%e7%90%b4%e8%a1%a8%e9%a6%99%e6%b8%af%e4%b8%80%e5%93%a9%e9%8c%a6%e6%a8%99/>
12. <http://racing.on.cc/news.html?articleId=bknrac-20190127163908513-0127_01002_001>
    【有片】馬壇天之驕子 從練1200W達成！
13. 「時時精綵」輕取百週年紀念銀瓶
    <https://racingnews.hkjc.com/chinese/2019/01/27/%e3%80%8c%e6%99%82%e6%99%82%e7%b2%be%e7%b6%b5%e3%80%8d%e8%bc%95%e5%8f%96%e7%99%be%e9%80%b1%e5%b9%b4%e7%b4%80%e5%bf%b5%e9%8a%80%e7%93%b6/>
14. 潘頓一日六勝頭馬　「天池怪俠」冀能入圍打吡
    <https://racingnews.hkjc.com/chinese/2019/02/24/%E6%BD%98%E9%A0%93%E4%B8%80%E6%97%A5%E5%85%AD%E5%8B%9D%E9%A0%AD%E9%A6%AC%e3%80%80%E3%80%8C%E5%A4%A9%E6%B1%A0%E6%80%AA%E4%BF%A0%E3%80%8D%E5%86%80%E8%83%BD%E5%85%A5%E5%9C%8D%E6%89%93%E5%90%A1/>
15. 【真係畀到壓力雷神】潘頓追到只差4W咋！
    <http://racing.on.cc/news.html?id=20180520220801>
16. 【戰況R6】http://racing.on.cc/news.html?id=20180527153005有片！勝利專家大摩1600W
    蕭家啡格贈興
17. 【戰況R10】有片！畀你恨到喇 潘頓終於獨霸榜首！
    <http://racing.on.cc/news.html?id=20180610173520>
18. 馬季圓滿結束 潘頓二度封王
    <https://campaign.hkjc.com/ch/season-end/news.aspx?in_file=2018071502113.html&b_cid=EWRHHLC_1718SEFIN_CONGJ>
19. [遭利誘賣貼士
    潘頓不申報罰30萬](http://the-sun.on.cc/cnt/news/20110622/00407_025.html)
    太陽報
20. [潘頓向禮儀小姐講粗口　贏馬七萬罰十皮](https://hk.on.cc/hk/bkn/cnt/sport/20170622/bkn-20170622162222607-0622_00882_001.html)
    東網香港
21. 【咎由自取】網上諷刺行家利敬國！冠軍騎師潘頓惹禍上身
    <https://hk.sports.appledaily.com/racing/realtime/article/20181108/58887176>
22. [Tune in, I'm about to call race 2 at Happy Valley
    hkjc](https://twitter.com/zpurton/status/644113038199685120)