**潘鼎新**（），[字](../Page/表字.md "wikilink")**琴轩**，[安徽](../Page/安徽.md "wikilink")[庐江广寒乡人](../Page/庐江.md "wikilink")，后迁居[肥西](../Page/肥西.md "wikilink")[三河](../Page/三河.md "wikilink")。清朝政治、軍事人物。

## 仕途

[清](../Page/清.md "wikilink")[道光二十九年](../Page/道光.md "wikilink")（1849）中举，次年[春闱](../Page/春闱.md "wikilink")[会试至国史馆承修臣传](../Page/会试.md "wikilink")。后在家乡办团练，参与镇压[太平军](../Page/太平军.md "wikilink")，受[曾国藩赏识](../Page/曾国藩.md "wikilink")，令募勇立“鼎”字营。[同治元年](../Page/同治.md "wikilink")(1862)署[江苏常镇通海道](../Page/江苏.md "wikilink")，后加按察使、布政使衔。

## 剿捻軍

  - 同治四年
    (1865)率部北上，赴山东镇压[捻军](../Page/捻军.md "wikilink")，后任山东布政使。同治十三年（1874）授雲南布政使。光绪二年（1876）升任巡抚，与总督[刘长佑不合](../Page/刘长佑.md "wikilink")，次年调至京城另候任用。

## 中法戰爭

  - 19世纪80年代，[法国政府推行](../Page/法国.md "wikilink")[帝國擴張](../Page/帝國.md "wikilink")[殖民主义](../Page/殖民主义.md "wikilink")，派军占领[越南](../Page/越南.md "wikilink")，继而准备占领中国的西南腹地。光绪九年（1883）九月，法军向清守军[刘永福部进攻](../Page/刘永福.md "wikilink")。在民族矛盾已上升为主要矛盾的形势下，清政府除加强两广防务外，并命潘鼎新为湖南巡抚，在二线布防。
  - 光绪十年二月二十九，由[湖南巡抚调任](../Page/湖南巡抚.md "wikilink")[廣西巡撫](../Page/廣西巡撫.md "wikilink")。潘鼎新驰抵南宁接印后，即奏请调兵遣将，驻守前钱。六月二十五日，清军大创法军于[观音桥](../Page/观音桥.md "wikilink")。潘鼎新随即驻军[谅山](../Page/谅山.md "wikilink")，扎兵于屯梅、谷松、坚牢等处要隘，与法军初战船头、祗社，获胜。但是清政府面对法军武装侵略，不主张抗击到底，而乞求议和，[李鸿章密令潘鼎新](../Page/李鸿章.md "wikilink")“战胜不道，战败则退”，使清军坐失战机，处于防守挨打的被动地位。
  - 光绪十一年（1885）一月，法军大举来犯，[谅山陷](../Page/谅山.md "wikilink")，师退，潘自请罪，清廷命其带罪立功。二月，潘令杨玉科援军守镇南关，自驻海村为后援。海村为龙州要隘，背靠大河，命撤舟桥，示死决战。二十三日，法军攻[镇南关](../Page/镇南关.md "wikilink")，[杨玉科阵亡](../Page/杨玉科.md "wikilink")，镇南关失守。**潘鼎新**率骑夺关，伤肘坠马，经营救，继续指挥战斗，以[苏元春为先锋](../Page/苏元春.md "wikilink")，经苦战复拔镇南关，把法军赶到[文渊](../Page/文渊.md "wikilink")。随即法军又由[艽封绕道攻](../Page/艽封.md "wikilink")[龙州](../Page/龙州.md "wikilink")，潘令淮、鄂两军迎击，法军退，又令[冯子材](../Page/冯子材.md "wikilink")、苏元春二军驻防[扣坡](../Page/扣坡.md "wikilink")。
  - 三月，法军从北宁调兵三千至谅山，派越夫万人运子弹粮食，扬言初八攻龙州。**潘鼎新**即会商诸将，采取先发制人，令冯部初四日出关攻文渊，[王孝祺军副之](../Page/王孝祺.md "wikilink")，[蒋宗汉](../Page/蒋宗汉.md "wikilink")、[陈嘉二军分起设伏](../Page/陈嘉.md "wikilink")，[苏元春军在关外往来援应](../Page/苏元春.md "wikilink")，并诱敌深入，淮、鄂各军居中截击。冯、王二军进战，自山后攀崖越险，破敌两垒，尽毙守敌。初七日，法军分三路攻镇南关，激战两昼夜，法军大败溃逃，沿阵尸横遍野，清军夺其辎重，奋起追击，初十日克文渊，次日进驻[巴平](../Page/巴平.md "wikilink")。追蹑至谅山城下，潘鼎新骑马指挥攻城，十三日克[谅山](../Page/谅山.md "wikilink")，急率淮、鄂二军追击，连克[观音桥](../Page/观音桥.md "wikilink")、[屯梅二要隘](../Page/屯梅.md "wikilink")，十七日克[谷松](../Page/谷松.md "wikilink")。不断这日夜[龙州送到潘鼎新被革职谕旨](../Page/龙州.md "wikilink")，十八日他回京山交卸，曾作悲歌“兄弟一军归故里，河山百战送蛮夷”。鼎新俟和议定后，乃由桂林归无为侨寓。抚职由按察使[李秉衡暂行护理](../Page/李秉衡.md "wikilink")。\[1\]。

## 返鄉[庐江](../Page/庐江.md "wikilink")

  - 于同治七年（1868）无后捐资重修家鄉安徽庐江城内[文昌宫](../Page/文昌宫.md "wikilink")、奎星楼，以后又资助建南京庐江试馆。光绪十四年
    （1888），清廷赏还鼎新原衔，是年5月12日感暑触发旧伤逝世，终年61岁。

## 参考

{{-}}

[Category:清朝山東布政使](../Category/清朝山東布政使.md "wikilink")
[Category:清朝雲南布政使](../Category/清朝雲南布政使.md "wikilink")
[Category:清朝雲南巡撫](../Category/清朝雲南巡撫.md "wikilink")
[Category:清朝湖南巡撫](../Category/清朝湖南巡撫.md "wikilink")
[Category:清朝廣西巡撫](../Category/清朝廣西巡撫.md "wikilink")
[Category:淮軍人物](../Category/淮軍人物.md "wikilink")
[Category:廬江人](../Category/廬江人.md "wikilink")
[D鼎](../Category/潘姓.md "wikilink")
[Category:清朝常鎮通海道](../Category/清朝常鎮通海道.md "wikilink")
[Category:道光二十九年己酉科舉人](../Category/道光二十九年己酉科舉人.md "wikilink")

1.