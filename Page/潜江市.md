**潜江市**为[湖北省中南部的一个](../Page/湖北省.md "wikilink")[省直辖县级市](../Page/省直辖县级行政区.md "wikilink")([副地级市](../Page/副地级市.md "wikilink"))，位于[江汉平原腹地](../Page/江汉平原.md "wikilink")，北枕[汉水](../Page/汉水.md "wikilink")，南接[岳阳至](../Page/岳阳.md "wikilink")[长沙](../Page/长沙.md "wikilink")，东邻[武汉通](../Page/武汉.md "wikilink")[黄石](../Page/黄石.md "wikilink")，西接[荆州达](../Page/荆州.md "wikilink")[三峡](../Page/三峡.md "wikilink")。
318国道和沪蓉高速、沪渝高铁横穿东西，随岳高速、枣石高速和潜监、襄岳两条一级公路以及江汉平原货运铁路纵贯南北。北有汉江泽口、红旗、兴隆码头，西有中国第一条人工运河连接长江与汉江，中国平原第一坝的兴隆水利枢纽工程就在潜江高石碑镇境内。
潜江境内有全国十大油田之一的[江汉油田和由](../Page/江汉油田.md "wikilink")16
个国有农场组成的农场群。1993年11月被评为全国明星市。境内地势平坦，地面海拔在26米至31米之间，全境都是平原，属亚热带季风性湿润气候，雨量充沛，气候宜人，素来以“水乡园林”著称。潜江是武汉城市圈成员，地上盛产粮棉，地下富藏油气。中国石油化工股份有限公司—江汉油田分公司总部就设在湖北省潜江市的广华。
潜江是著名的“水乡园林”，是水杉的故乡，江汉平原生态文明交通示范市，著名作家碧野曾盛赞潜江是“一座绿色的城”。潜江是世界最大的牛磺酸生产基地、亚洲最大的石油钻头生产基地、全国最大的眼科用药生产基地；被国家授予“中国小龙虾之乡”、“中国小龙虾美食之乡”、“中国民间文化艺术之乡”及“中国戏剧之都”的称号。

## 历史沿革

**潜江**古为[云梦泽一角](../Page/云梦泽.md "wikilink")，历经江水复合冲积和湖水缓慢沉积而逐渐形成。潜江地域[夏](../Page/夏.md "wikilink")、[商](../Page/商.md "wikilink")、[周三代属](../Page/周.md "wikilink")[荆州域](../Page/荆州_\(古代\).md "wikilink")。后分属[竟陵](../Page/竟陵县.md "wikilink")、[江陵等县](../Page/江陵县.md "wikilink")。\[1\]

[北宋](../Page/北宋.md "wikilink")[乾德三年](../Page/乾德_\(北宋\).md "wikilink")（965年）始建县，因境内有河道分流汉水入长江，取“汉出为潜”意，命名潜江，县治设在安远镇（在今下蚌湖附近），隶属于[荆湖北路](../Page/荆湖北路.md "wikilink")[江陵府](../Page/江陵府.md "wikilink")，后先后隶[中兴路](../Page/中興路.md "wikilink")、[荆州府](../Page/荆州府.md "wikilink")、[承天府](../Page/承天府_\(湖北\).md "wikilink")、[安陆府](../Page/安陆府.md "wikilink")。解放后隶属[荆州专区](../Page/荆州专区.md "wikilink")、[荆州地区](../Page/荆州地区.md "wikilink")，1988年5月撤县建市，1994年10月被列为湖北省直管市。\[2\]

潜江是[楚文化的发祥地之一](../Page/楚文化.md "wikilink")，号称
“[天下第一台](../Page/天下第一台.md "wikilink")”的东周楚王行宫就在境内龙湾镇。“[龙湾遗址](../Page/龙湾遗址.md "wikilink")”，是中国迄今发现的保存最为完整、时代最早的楚国离宫别院遗址群落，被定为2000年“[全国十大考古发现](../Page/全国十大考古发现.md "wikilink")”。

## 地理

属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，雨量充沛，气候宜人，素来以“水乡园林”著称。

## 政治

### 现任领导

<table>
<caption>潜江市五大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党潜江市委员会.md" title="wikilink">中国共产党<br />
潜江市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/潜江市人民代表大会.md" title="wikilink">潜江市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/潜江市人民政府.md" title="wikilink">潜江市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议潜江市委员会.md" title="wikilink">中国人民政治协商会议<br />
潜江市委员会</a><br />
主席</p></th>
<th><p><br />
<a href="../Page/潜江市监察委员会.md" title="wikilink">潜江市监察委员会</a><br />
<br />
主任</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p>吴祖云[3]</p></td>
<td><p><a href="../Page/李金萍.md" title="wikilink">李金萍</a>（女）[4]</p></td>
<td><p><a href="../Page/龚定荣.md" title="wikilink">龚定荣</a>[5]</p></td>
<td><p><a href="../Page/申东辉.md" title="wikilink">申东辉</a>[6]</p></td>
<td><p><a href="../Page/张志鹏.md" title="wikilink">张志鹏</a>[7]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/回族.md" title="wikilink">回族</a></p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/南阳市.md" title="wikilink">南阳市</a></p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省</a><a href="../Page/沙洋县.md" title="wikilink">沙洋县</a></p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/曹县.md" title="wikilink">曹县</a></p></td>
<td><p>湖北省<a href="../Page/蕲春县.md" title="wikilink">蕲春县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年1月</p></td>
<td><p>2017年6月</p></td>
<td><p>2018年1月</p></td>
<td><p>2018年1月</p></td>
<td></td>
</tr>
</tbody>
</table>

### 行政区划

潜江市下辖6个[街道办事处](../Page/街道办事处.md "wikilink")、10个[镇](../Page/建制镇.md "wikilink")、6个[管理区](../Page/行政管理区.md "wikilink")（农场）、1个省级经济开发区\[8\]：

  - 街道：[园林街道](../Page/园林街道_\(潜江市\).md "wikilink")、[广华街道](../Page/广华街道.md "wikilink")、[杨市街道](../Page/杨市街道.md "wikilink")、[周矶街道](../Page/周矶街道.md "wikilink")、[泰丰街道](../Page/泰丰街道.md "wikilink")、[高场街道](../Page/高场街道.md "wikilink")（与高场原种场[一个机构两块牌子](../Page/一个机构两块牌子.md "wikilink")）

<!-- end list -->

  - 镇：[熊口镇](../Page/熊口镇.md "wikilink")、[竹根滩镇](../Page/竹根滩镇.md "wikilink")、[高石碑镇](../Page/高石碑镇.md "wikilink")、[老新镇](../Page/老新镇.md "wikilink")、[王场镇](../Page/王场镇_\(潜江市\).md "wikilink")、[渔洋镇](../Page/渔洋镇.md "wikilink")、[龙湾镇](../Page/龙湾镇_\(潜江市\).md "wikilink")、[浩口镇](../Page/浩口镇.md "wikilink")、[积玉口镇](../Page/积玉口镇.md "wikilink")、[张金镇](../Page/张金镇.md "wikilink")
  - 管理区：[白鹭湖管理区](../Page/白鹭湖管理区.md "wikilink")、[总口管理区](../Page/总口管理区.md "wikilink")、[熊口农场管理区](../Page/熊口农场管理区.md "wikilink")、[运粮湖管理区](../Page/运粮湖管理区.md "wikilink")、[后湖管理区](../Page/后湖管理区.md "wikilink")、[周矶管理区](../Page/周矶管理区.md "wikilink")
  - 省级经济开发区：[潜江开发区](../Page/潜江开发区.md "wikilink")

## 人口

| 区划名称 | 面积（公里²） | 常住人口      | 户籍人口      |
| ---- | ------- | --------- | --------- |
| 潜江市  | 2，004   | 1，100,000 | 1,118,265 |

**潜江市面积与人口数据（2014年末）**

## 人物

  - 政治人物：[李汉俊](../Page/李汉俊.md "wikilink")、[刘静庵](../Page/刘静庵.md "wikilink")、[李书城](../Page/李书城.md "wikilink")、[钱瑛](../Page/钱瑛.md "wikilink")、[姚立法](../Page/姚立法.md "wikilink")
  - 文学人物：[曹禺](../Page/曹禺.md "wikilink")、[杜鸣心](../Page/杜鸣心.md "wikilink")
  - 商界人物：[张振高](../Page/张振高.md "wikilink")、[易贤中](../Page/易贤中.md "wikilink")、[王傲华](../Page/王傲华.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [潜江市人民政府网](http://www.hbqj.gov.cn)

[潜江市](../Category/潜江市.md "wikilink")
[Category:湖北直辖县级行政区](../Category/湖北直辖县级行政区.md "wikilink")
[Category:湖北县级市](../Category/湖北县级市.md "wikilink")
[鄂](../Category/中国小城市.md "wikilink")

1.

2.
3.
4.

5.

6.

7.

8.