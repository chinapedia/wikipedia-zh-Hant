**潛規則**是指一些行業及人群中約定俗成，被廣泛行使且普遍遵守的隱含性規則。甚至在某些特定的時空能具體的替代檯面上的正式規則。\[1\]。潜规则是指没有正式规定，而在某些人群中被行为各方普遍遵守的隱含规则。但在中国历史上，往往是潜规则而非种种明文规定，在支配着现实生活的运行。

潜规则在不同时间、不同领域往往有不同称呼，如在明清官场被称为“[官场陋规](../Page/官场陋规.md "wikilink")”，在现在一些行业被称为“行规”、“规矩”，在某些场合又被称为“不成文规定”。

，但也有一些规则是因为没有道理而且琐碎而成为潜规则。前一种情况比如给[官员送](../Page/官员.md "wikilink")[红包](../Page/红包.md "wikilink")，或者在一些国家[少数民族](../Page/少数民族.md "wikilink")、[女性职员不容易被提升到重要岗位](../Page/女性.md "wikilink")；后一种情况比如在欧美不要穿着一个演唱团体的[T恤衫去看该团体的演出](../Page/T恤衫.md "wikilink")。但通常前一种情况的用法更多。

## 概念的发明

[中国作家](../Page/中国.md "wikilink")[吴思在其](../Page/吴思.md "wikilink")2001年出版的《[潜规则：中国历史中的真实游戏](../Page/潜规则：中国历史中的真实游戏.md "wikilink")》\[2\]一书中提出：

吴思在2003年出版的《[血酬定律：中国历史中的生存游戏](../Page/血酬定律：中国历史中的生存游戏.md "wikilink")》\[3\]，对
“潜规则”一词作了更多定义。

自此“潜规则”一词诞生，并逐渐被广泛运用。但这种概念的实行则是自古已有。

## 概念的发展

2006年中国女演员[张钰向媒体透露](../Page/張鈺_\(演員\).md "wikilink")：“很多女演员被迫和[导演](../Page/导演.md "wikilink")、制片人等[陪睡](../Page/陪睡.md "wikilink")，以得到演出机会，这已经是演艺圈的潜规则”\[4\]，此案例類似於“職場心理霸凌”
較不受法律及輿論重視。随着媒体对“张钰事件”的大量报道和讨论，“潜规则”一词迅速普及，并进而衍生为动词，比如“某某曾被某人潜规则”这样的报道层出不穷\[5\]。

## 相关词汇

  - [不成文憲法](../Page/不成文憲法.md "wikilink")

  - [君子協定](../Page/君子協定.md "wikilink")

  - [元规则](../Page/元规则.md "wikilink")

  - [期數列外](../Page/期數列外.md "wikilink")

  - [紅色條規](../Page/紅色條規.md "wikilink")（Code Red，詳見「[-{zh-hk:義海雄風;
    zh-tw:軍官與魔鬼; zh-hans:好人寥寥;}-](../Page/軍官與魔鬼.md "wikilink")」）

  -
## 注释

[\*](../Page/category:潛規則.md "wikilink")

[ja:不文律](../Page/ja:不文律.md "wikilink") [sv:Social norm\#Oskriven
regel](../Page/sv:Social_norm#Oskriven_regel.md "wikilink")

[Category:2000年代创造的新词语](../Category/2000年代创造的新词语.md "wikilink")

1.  [潜规则：中国历史中的真实游戏，吴思，云南人民出版社，2001年1月](http://www.cngdsz.net/book/bookcontent_show.asp?typeid=2&bookid=173&contentid=3172)

2.
3.  [血酬定律：中国历史中的生存游戏，吴思，中国工人出版社，2003年](http://book.sina.com.cn/nzt/1076059710_xuechoudinglv/)

4.  [张钰将公布“潜规则”证据
    有太多女孩上当了](http://entertainment.anhuinews.com/system/2006/11/16/001606085.shtml)

5.  [细数娱乐圈的“潜规则”事件(上)](http://ent.ifeng.com/mala/bead/200712/1206_427_322752.shtml)