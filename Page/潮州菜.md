[缩略图](https://zh.wikipedia.org/wiki/File:2014.11.16.120213_Restaurant_Yuyuan_Tourist_Mart_Shanghai.jpg "fig:缩略图")
**潮州菜**，简称**潮菜**，是[粤菜](../Page/粤菜.md "wikilink")[菜系之一](../Page/中国菜系.md "wikilink")，起源于[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[潮汕地區](../Page/潮汕地區.md "wikilink")。低档的潮州菜馆大多树立**潮菜**自己的名称，高档的潮菜酒楼则一般用**潮粤菜**。潮州菜的特点有善烹海鲜、重汤轻油、崇尚清淡、注重养生等。

## 历史

最早记述潮州菜的是819年被贬到潮州的[韩愈](../Page/韩愈.md "wikilink")，从其诗文可见当时潮州菜有[鲎](../Page/鲎.md "wikilink")、[蚝](../Page/蚝.md "wikilink")、[蒲鱼](../Page/蒲鱼.md "wikilink")、[蛤](../Page/蛤.md "wikilink")、[章鱼](../Page/章鱼.md "wikilink")、[江瑶柱等数十种海鲜并且使用了](../Page/江瑶柱.md "wikilink")[盐和](../Page/食盐.md "wikilink")[醋](../Page/醋.md "wikilink")，[花椒与](../Page/花椒.md "wikilink")[酸橙等助存及调和食物的佐料](../Page/酸橙.md "wikilink")。[段公路曾记述当时潮州有长两尺的红虾的食物](../Page/段公路.md "wikilink")。可见潮州菜中海鲜由来已久。
[缩略图](https://zh.wikipedia.org/wiki/File:HK_九龍城廣場_Kln_City_Plaza_佳寧娜潮州菜_Carrianna_Chiu_Chow_Restaurant.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_Queen's_Road_East_Hop_Hing_Chiu_Chow_Fish_Ball_Noodle_Restaurant_sign_June-2013.JPG "fig:缩略图")

潮州菜尤其擅用食鹽、[鹵水等佐料助存及調和食物](../Page/鹵水.md "wikilink")，也因此潮州菜在海外尤其[南洋發展與交融尤甚](../Page/南洋.md "wikilink")。

## 特点

  - 无海鲜不成宴
  - 正宗的潮州菜馆在饭前饭后都会提供[工夫茶](../Page/工夫茶.md "wikilink")。
  - 咸與甜、荤与素配套，汤与菜交错，先上冷的或热的拼盘，然后隔几道菜穿插一道汤。
  - 素菜荤做，见菜不见肉

## 著名菜式

  - [滷豬頭肉](../Page/滷豬頭肉.md "wikilink")
  - [燒白皮乳豬](../Page/燒白皮乳豬.md "wikilink")
  - [鹹菜炆豬肉](../Page/鹹菜炆豬肉.md "wikilink")
  - [蟹棗](../Page/蟹棗.md "wikilink")
  - [滷鵝](../Page/滷鵝.md "wikilink")
  - [豬頭花](../Page/豬頭花.md "wikilink")
  - [炒甜麵](../Page/炒甜麵.md "wikilink")
  - [魚飯](../Page/魚飯.md "wikilink")
  - [护国菜](../Page/护国菜.md "wikilink")
  - [八宝素菜](../Page/八宝素菜.md "wikilink")
  - [太极芋泥](../Page/太极芋泥.md "wikilink")
  - [明炉烧大海螺](../Page/明炉烧大海螺.md "wikilink")
  - [鸳鸯膏蟹](../Page/鸳鸯膏蟹.md "wikilink")
  - [清炖白鳝](../Page/清炖白鳝.md "wikilink")
  - [干焗蟹塔](../Page/干焗蟹塔.md "wikilink")
  - [白汁鲳鱼](../Page/白汁鲳鱼.md "wikilink")
  - [红焖鲍鱼](../Page/红焖鲍鱼.md "wikilink")
  - [红焖海参](../Page/红焖海参.md "wikilink")
  - [甜芙蓉官燕](../Page/甜芙蓉官燕.md "wikilink")
  - [清金钱鳔](../Page/清金钱鳔.md "wikilink")
  - [油泡鱿鱼](../Page/油泡鱿鱼.md "wikilink")
  - [清汤螺把](../Page/清汤螺把.md "wikilink")
  - [清金鲤虾](../Page/清金鲤虾.md "wikilink")
  - [生菜龙虾](../Page/生菜龙虾.md "wikilink")
  - [炊麒麟鱼](../Page/炊麒麟鱼.md "wikilink")
  - [红炖鱼翅](../Page/红炖鱼翅.md "wikilink")
  - [清炖凤翅](../Page/清炖凤翅.md "wikilink")
  - [文昌雞](../Page/文昌雞.md "wikilink")
  - [卤水鹅](../Page/卤水鹅.md "wikilink")
  - [潮州冻蟹](../Page/潮州冻蟹.md "wikilink")
  - [潮州血蚌](../Page/潮州血蚌.md "wikilink")（即生腌血蚌）
  - [炒石螺](../Page/炒石螺.md "wikilink")
  - [炒鲜薄壳](../Page/炒鲜薄壳.md "wikilink")
  - [東山羊](../Page/東山羊.md "wikilink")（產自[東山縣](../Page/東山縣.md "wikilink")）
  - [腌虾姑](../Page/腌虾姑.md "wikilink")
  - [厚菇大芥菜](../Page/厚菇大芥菜.md "wikilink")
  - [川椒鸡](../Page/川椒鸡.md "wikilink")
  - [烳醃魚](../Page/烳醃魚.md "wikilink")

## 宴席

一般潮州宴席，普遍有10个主菜，海鲜占7成左右，其中2、3三个汤菜。

常見主菜

  - [生炊龙蟹](../Page/生炊龙蟹.md "wikilink")
  - [生炊膏蟹](../Page/生炊膏蟹.md "wikilink")
  - [生炊全鱼](../Page/生炊全鱼.md "wikilink")
  - [炒买穗鱿](../Page/炒买穗鱿.md "wikilink")
  - [红纹海参](../Page/红纹海参.md "wikilink")
  - [红炖鱼翅](../Page/红炖鱼翅.md "wikilink")

常見汤菜：

  - [清汤蟹丸](../Page/清汤蟹丸.md "wikilink")
  - [清汤螺丸](../Page/清汤螺丸.md "wikilink")
  - [潮州鱼丸](../Page/潮州鱼丸.md "wikilink")
  - [清汤鳗把](../Page/清汤鳗把.md "wikilink")
  - [清炖白鳝](../Page/清炖白鳝.md "wikilink")
  - [虫草鲍鱼](../Page/虫草鲍鱼.md "wikilink")
  - [薏米甲鱼](../Page/薏米甲鱼.md "wikilink")
  - [咸菜螺片](../Page/咸菜螺片.md "wikilink")
  - [菜脯明虾](../Page/菜脯明虾.md "wikilink")
  - [干貝蘿蔔](../Page/干貝蘿蔔.md "wikilink")
  - [车白清汤](../Page/车白清汤.md "wikilink")

## 配酱调味

潮菜中不同菜色，配以不同酱碟，一菜一碟。

  - [普宁豆酱](../Page/普宁豆酱.md "wikilink")
  - [南姜](../Page/南姜.md "wikilink")
  - [梅膏](../Page/梅膏.md "wikilink")
  - [鱼露](../Page/鱼露.md "wikilink")
  - [红豉油](../Page/红豉油.md "wikilink")
  - [沙茶酱](../Page/沙茶酱.md "wikilink")

### 潮州打冷

[潮州打冷通常以](../Page/潮州打冷.md "wikilink")[大排檔式經營](../Page/大排檔.md "wikilink")，分佈於香港、深圳、广州，主要菜式包括：

  - [蚝烙](../Page/蚝烙.md "wikilink")
  - [九层塔炒薄壳](../Page/九层塔炒薄壳.md "wikilink")
  - [芥蓝炒牛肉](../Page/芥蓝炒牛肉.md "wikilink")
  - [普宁豆干](../Page/普宁豆干.md "wikilink")
  - [生腌血蚌](../Page/生腌血蚌.md "wikilink")
  - [生腌虾菇](../Page/生腌虾菇.md "wikilink")
  - [生腌蟹](../Page/生腌蟹.md "wikilink")
  - [生腌蚬](../Page/生腌蚬.md "wikilink")
  - [卤水鹅](../Page/卤水鹅.md "wikilink")
  - [卤水拼盘](../Page/卤水拼盘.md "wikilink")
  - [卤水豆腐](../Page/卤水豆腐.md "wikilink")
  - [卤水鸡蛋](../Page/卤水鸡蛋.md "wikilink")
  - [真珠花菜猪血汤](../Page/真珠花菜猪血汤.md "wikilink")
  - [咸菜猪肚汤](../Page/咸菜猪肚汤.md "wikilink")
  - [苦瓜黄豆排骨汤](../Page/苦瓜黄豆排骨汤.md "wikilink")

## 潮州小食

：

  - [卷煎](../Page/卷煎.md "wikilink")
  - [糯米猪肠](../Page/糯米猪肠.md "wikilink")
  - [粿汁](../Page/粿汁.md "wikilink")
  - [粿条汤](../Page/粿条汤.md "wikilink")
  - [潮州春饼](../Page/潮州春饼.md "wikilink")
  - [炒糕粿](../Page/炒糕粿.md "wikilink")
  - [肖米](../Page/肖米.md "wikilink")
  - 麦包
  - [南瓜芋泥](../Page/南瓜芋泥.md "wikilink")
  - [反沙芋](../Page/反沙芋.md "wikilink")
  - [糕烧番薯](../Page/糕烧番薯.md "wikilink")
  - [猪脚圈](../Page/猪脚圈.md "wikilink")
  - [浮豆干](../Page/浮豆干.md "wikilink")
  - [芋丸](../Page/芋丸.md "wikilink")
  - [水晶包](../Page/水晶包.md "wikilink")
  - [白饭桃](../Page/白饭桃.md "wikilink")
  - [清心丸](../Page/清心丸.md "wikilink")
  - [绿豆爽](../Page/绿豆爽.md "wikilink")
  - [鸭母捻](../Page/鸭母捻.md "wikilink")
  - [粽球](../Page/粽球.md "wikilink")
  - [牛肉丸](../Page/牛肉丸.md "wikilink")
  - [束砂](../Page/束砂.md "wikilink")
  - [糖葱薄饼](../Page/糖葱薄饼.md "wikilink")
  - [朥饼](../Page/朥饼.md "wikilink")
  - [姜薯酥](../Page/姜薯酥.md "wikilink")
  - [潮州腐乳饼](../Page/潮州腐乳饼.md "wikilink")
  - [肉粽](../Page/肉粽.md "wikilink")

## 潮州粿

  - [无米粿](../Page/无米粿.md "wikilink")
  - [乒乓粿](../Page/乒乓粿.md "wikilink")
  - [红桃粿](../Page/红桃粿.md "wikilink")
  - [沙茶粿](../Page/沙茶粿.md "wikilink")
  - [韭菜粿](../Page/韭菜粿.md "wikilink")
  - [笋粿](../Page/笋粿.md "wikilink")
  - [甘筒粿](../Page/甘筒粿.md "wikilink")
  - [酵粿](../Page/酵粿.md "wikilink")
  - [朴枳粿](../Page/朴枳粿.md "wikilink")
  - [咸水粿](../Page/咸水粿.md "wikilink")
  - [糍粬粿](../Page/糍粬粿.md "wikilink")
  - [菜头粿](../Page/菜头粿.md "wikilink")
  - [栀粿](../Page/栀粿.md "wikilink")
  - [鲎粿](../Page/鲎粿.md "wikilink")
  - [甜粿](../Page/甜粿.md "wikilink")
  - [碗糕粿](../Page/碗糕粿.md "wikilink")
  - [油粿](../Page/油粿.md "wikilink")
  - [鼠壳粿](../Page/鼠壳粿.md "wikilink")
  - [豆粿](../Page/豆粿.md "wikilink")

## 潮州杂鹹

主要是配潮州白[粥](../Page/粥.md "wikilink")（潮州糜）用的，：

  - [咸菜](../Page/咸菜.md "wikilink")
  - [贡菜](../Page/贡菜.md "wikilink")
  - [菜脯](../Page/菜脯.md "wikilink")
  - [橄榄菜](../Page/橄榄菜.md "wikilink")
  - [冬菜](../Page/冬菜.md "wikilink")
  - [乌橄榄](../Page/乌橄榄.md "wikilink")
  - [豆酱姜](../Page/豆酱姜.md "wikilink")
  - [咸水梅](../Page/咸水梅.md "wikilink")
  - [咸蛋](../Page/咸蛋.md "wikilink")
  - [荞头](../Page/荞头.md "wikilink")
  - [盐水蒜肉](../Page/盐水蒜肉.md "wikilink")
  - [腌制虾菇](../Page/腌制虾菇.md "wikilink")
  - [腌制蚬](../Page/腌制蚬.md "wikilink")
  - [腌制蟹](../Page/腌制蟹.md "wikilink")
  - [蟛蜞](../Page/蟛蜞.md "wikilink")
  - [钱螺鲑](../Page/钱螺鲑.md "wikilink")
  - [饶子脯](../Page/饶子脯.md "wikilink")
  - [芥蓝茎](../Page/芥蓝茎.md "wikilink")
  - [腌杨桃](../Page/腌杨桃.md "wikilink")
  - [咸巴浪鱼](../Page/咸巴浪鱼.md "wikilink")
  - [花仙](../Page/花仙.md "wikilink")
  - [腌黄瓜](../Page/腌黄瓜.md "wikilink")

## 各地潮州菜館

### 香港

  - [尚興潮菜馆](../Page/尚興潮菜馆.md "wikilink")
  - [兩興潮菜馆](../Page/兩興潮菜馆.md "wikilink")
  - [樂口福酒家](../Page/樂口福酒家.md "wikilink")

### 马来西亚

  - [古晋](../Page/古晋.md "wikilink")，位於[砂拉越](../Page/砂拉越.md "wikilink")

## 图片集

<File:Shui> jing bao zz.JPG|水晶包 <File:Teochew> pomfret.jpg|炊鱼
<File:Oyster> omelette.jpg|蚝烙 <File:Khanom> kuichai.jpg|韭菜馃 <File:Song>
dynasty's 'Patriotic soup' -Protect the Country Dish (護國菜) .jpg|护国菜
<File:Fried> Tofu (炸豆腐).jpg|炸豆腐 <File:Teochew> rice noodle soup
(潮州粿條).jpg|潮州粿條 [File:Fansawoo.jpg|反沙芋](File:Fansawoo.jpg%7C反沙芋)
[File:Loumeidish.jpg|滷水鵝](File:Loumeidish.jpg%7C滷水鵝)
[File:Chaozhoufenguo.jpg|潮州粉粿](File:Chaozhoufenguo.jpg%7C潮州粉粿)

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
  -
[潮州菜](../Category/潮州菜.md "wikilink")