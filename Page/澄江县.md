**-{澄江}-县**\[1\]，又名**澂江县**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[玉溪市下属的一个县](../Page/玉溪市.md "wikilink")。位于[昆明市区的东南面](../Page/昆明市.md "wikilink")。面积773平方公里，2006年人口16万人。县政府驻[凤麓镇](../Page/凤麓镇.md "wikilink")。

## 历史

[西汉元封五年](../Page/西汉.md "wikilink")（前106年）置俞元县，属益州郡。[唐贞观八年](../Page/唐.md "wikilink")（634年），俞元县改属黎州，置络县（今江川、澄江）；肃宗上元元年（760年），改称河阳郡。后晋天福二年（937年），段思平讨灭南诏后，自立大理国，分治河阳郡为三部，今澄江为罗伽部。

[元至元十六年](../Page/元.md "wikilink")（1279年），升为澂江路，辖新兴、路南二州及河阳、江川、强宗三县。[明洪武十五年](../Page/明.md "wikilink")（1382年），改称澂江府，所辖州县不变。[民国](../Page/民国.md "wikilink")2年（1913年），撤销澂江府，改称河阳县。后因与河南省河阳县重名，故改称澄江县。

## 行政区划

目前下辖：\[2\] 。

## 名胜古迹

  - [抚仙湖](../Page/抚仙湖.md "wikilink")：是中国第三深的内陆淡水湖泊，蓄水量居国内淡水湖泊第三位，湖水澄碧清澈，生息繁衍着[金线鱼](../Page/金线鱼.md "wikilink")、[𩾌𩷕白鱼等](../Page/𩾌𩷕白鱼.md "wikilink")24种土著鱼种。
  - [澄江化石地](../Page/澄江化石地.md "wikilink")：澄江生物群位于我国[云南澄江帽天山附近](../Page/云南.md "wikilink")，是保存完整的[寒武纪早期古生物化石群](../Page/寒武纪.md "wikilink")。她生动地再现了5.3亿年前海洋生命壮丽景观和现生动物的原始特征，为研究地球早期延续时间为5370万年的生命起源、演化、生态等理论提供了珍贵证据，澄江生物群的研究和发现，不仅为[寒武纪生命大爆发这一非线性突发性演化提供了科学事实](../Page/寒武纪生命大爆发.md "wikilink")，同时对达尔文渐变式进化理论产生了重大的挑战。澄江生物群共涵盖16个门类、200余个物种化石（截止2012年）。
    2012年7月1日，澄江化石地正式被列入《世界遗产名录》

## 化石图片

<File:Misszhouia> longicaudata 2.jpg|*[周小姐虫](../Page/周小姐虫.md "wikilink")
longicaudata* <File:Heliomedusa>
orienta.jpg|*[Heliomedusa](../Page/Heliomedusa.md "wikilink") orienta*
<File:Leanchoilia>
illecebrosa.jpg|*[Leanchoilia](../Page/Leanchoilia.md "wikilink")
illecebrosa* <File:Haikouella> lanceolata
China.jpg|*[Haikouella](../Page/Haikouella.md "wikilink") lanceolata*

## 参考资料

## 外部链接

  - [澄江信息网](https://web.archive.org/web/20080905184937/http://www.yncj.gov.cn/)
  - [澄江生活网](https://web.archive.org/web/20130110015227/http://www.fuxianhu.cn/)

[澄江县](../Category/澄江县.md "wikilink") [县](../Category/玉溪区县.md "wikilink")
[玉溪](../Category/云南省县份.md "wikilink")

1.
2.