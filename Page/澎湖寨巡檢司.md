[Dutch_and_Spanish_Taiwan.png](https://zh.wikipedia.org/wiki/File:Dutch_and_Spanish_Taiwan.png "fig:Dutch_and_Spanish_Taiwan.png")時[大肚王國與明朝澎湖巡檢司](../Page/大肚王國.md "wikilink")\]\]

**澎湖寨巡檢司**，或稱**澎湖巡檢司**，設置於[澎湖群島](../Page/澎湖群島.md "wikilink")。設治時間以1281年（[元世祖至元十八年](../Page/元世祖.md "wikilink")）的考證最為早，也就是根據一般史書及《[元史](../Page/元史.md "wikilink")》、《[新元史](../Page/新元史.md "wikilink")》，考定：「蒙元世祖遠征日本因風失敗，迂迴台灣，道經澎湖設治澎湖，企圖進取台灣，作為征日本之準備。」

該地方區劃隸屬於[元朝](../Page/元朝.md "wikilink")[福建行省](../Page/福建行省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")，主官為[澎湖寨巡檢](../Page/澎湖寨巡檢.md "wikilink")。隨後擊敗元朝取得中原政權的[明朝仍依循前例於澎湖設置該官署](../Page/明朝.md "wikilink")，直至1384年因為實施[封海政策](../Page/海禁.md "wikilink")，予以廢除。1563年，考量沿海治安等因素，明朝復設澎湖寨巡檢司。此官署直至1622年，荷蘭占領澎湖為止。

## 元朝澎湖巡檢司

**澎湖寨巡檢司**，或稱**澎湖巡檢司**，設置於[澎湖群島](../Page/澎湖群島.md "wikilink")。設治時間以1281年（[元世祖至元十八年](../Page/元世祖.md "wikilink")）的考證最為早，也就是根據一般史書及《[元史](../Page/元史.md "wikilink")》、《[新元史](../Page/新元史.md "wikilink")》，考定：「蒙元世祖遠征日本因風失敗，迂迴台灣，道經澎湖設治澎湖，企圖進取台灣，作為征日本之準備。」

該地原於南宋隸屬於晉江縣\[1\]\[2\]，曾遣兵戍守澎湖\[3\]\[4\]。到元朝時該地方區劃隸屬於[元朝](../Page/元朝.md "wikilink")[福建行省](../Page/福建行省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")，主官為[澎湖寨巡檢](../Page/澎湖寨巡檢.md "wikilink")。而**[澎湖寨巡檢](../Page/澎湖寨巡檢.md "wikilink")**是元朝於[澎湖群島設置的官職](../Page/澎湖群島.md "wikilink")，也是[澎湖群島首度設立正式的地方統治官署](../Page/澎湖群島.md "wikilink")（前[澎湖列島歸屬於](../Page/澎湖列島.md "wikilink")[晉江縣](../Page/晉江縣.md "wikilink")）。

駐於[澎湖](../Page/澎湖縣.md "wikilink")[馬公的巡檢官職](../Page/馬公市.md "wikilink")，是元朝最基層的官員，隸屬於[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")。而1366年尚在任的澎湖寨巡檢[陳信惠是元朝首位駐澎官員](../Page/陳信惠.md "wikilink")。

## 明朝澎湖巡檢司

1370年代，[明朝統治中國後](../Page/明朝.md "wikilink")，仍循例於該地設置巡檢官職，也繼續設置[澎湖巡檢司這個行政官署](../Page/澎湖巡檢司.md "wikilink")，直至1384年實施封海政策而廢除。1563年，考量沿海治安等因素，明朝復設澎湖寨巡檢。此官職直至1622年，[荷蘭佔領](../Page/荷蘭.md "wikilink")[澎湖為止](../Page/澎湖.md "wikilink")，才免予實授。

1604年（萬曆三十二年）時，荷蘭東印度公司司令官[韋麻郎](../Page/韋麻郎.md "wikilink")（Wijbrant Van
Waerwijck）率兩艘船抵達澎湖，並派員往[福建請求貿易](../Page/福建.md "wikilink")。福建當局立即嚴禁人民出海接濟，並派都司[沈有容率兵船五十艘前往澎湖](../Page/沈有容.md "wikilink")，要求荷蘭人撤退。韋麻郎見求通商無望，又缺乏補給，於是在當年底離開澎湖。明朝政府於島上立有[沈有容諭退紅毛番碑](../Page/沈有容諭退紅毛番碑.md "wikilink")，現存於[澎湖天后宮](../Page/澎湖天后宮.md "wikilink")。

此官署直至1622年，荷蘭侵犯澎湖為止。1622年時，由於與葡萄牙人爭奪[澳門的軍事攻擊行動失敗](../Page/澳門.md "wikilink")，[荷蘭艦隊最後抵達了澎湖並且在](../Page/荷蘭.md "wikilink")[馬公風櫃尾蛇頭山頂設立了基地](../Page/馬公市.md "wikilink")。明朝政府退出澎湖。荷蘭軍隊強迫當地的居民建設碉堡，並使得奴役工作的1500個工人的其中1300人死亡\[5\]\[6\]。[明朝政府警告駐紮的荷蘭艦隊澎湖為其領土](../Page/明朝.md "wikilink")，之後荷蘭人於[澎湖海戰](../Page/澎湖之戰.md "wikilink")
(1624年)被明軍擊退，轉往台灣。

然而此戰之後，明朝雖再次取得澎湖，卻未復設澎湖巡檢司之職。

## 管轄區域

澎湖群島：90個大小島嶼所組成，以下列出主要有人居住的島嶼

  - [澎湖本島](../Page/澎湖本島.md "wikilink")
  - [白沙島](../Page/白沙島.md "wikilink")
  - [西嶼](../Page/西嶼.md "wikilink")
  - [望安島](../Page/望安島.md "wikilink")
  - [七美嶼](../Page/七美嶼.md "wikilink")
  - [吉貝嶼](../Page/吉貝嶼.md "wikilink")
  - [鳥嶼](../Page/鳥嶼.md "wikilink")
  - [中屯嶼](../Page/中屯嶼.md "wikilink")
  - [大倉嶼](../Page/大倉嶼.md "wikilink")
  - [員貝嶼](../Page/員貝嶼.md "wikilink")
  - [目斗嶼](../Page/目斗嶼.md "wikilink")
  - [小門嶼](../Page/小門嶼.md "wikilink")
  - [虎井嶼](../Page/虎井嶼.md "wikilink")
  - [桶盤嶼](../Page/桶盤嶼.md "wikilink")
  - [花嶼](../Page/花嶼.md "wikilink")
  - [將軍澳嶼](../Page/將軍澳嶼.md "wikilink")
  - [東吉嶼](../Page/東吉嶼.md "wikilink")
  - [東嶼坪嶼](../Page/東嶼坪嶼.md "wikilink")
  - [西嶼坪嶼](../Page/西嶼坪嶼.md "wikilink")

## 參考文獻

### 引用

### 来源

  - 劉寧顏
    編：《重修台灣省通志》，臺北市：[台灣省文獻委員會](../Page/台灣省文獻委員會.md "wikilink")，1994年。
  - 莊永明，1989年，《台灣紀事》，台北，時報出版社。
  - 南宋理宗宝庆元年（西元1225年）宗室[赵汝适著](../Page/赵汝适.md "wikilink")《[诸蕃志](../Page/诸蕃志.md "wikilink")》记载：“泉有海岛曰彭湖，隶晋江县”
  - 南宋马端临的《[文献通考](../Page/文献通考.md "wikilink")》曾记载：“
    琉求国在泉州之东，有岛曰澎湖。烟火相望，水行五日而至”
  - 《[元史](../Page/元史.md "wikilink")·琉求》的记载： “在南海之东。漳泉兴福四界内彭湖诸岛，与琉求相对”
  - 《元史·琉求》中记载元世祖在位时曾遣使自澎湖巡检司出发宣抚流求，渡海遇到一个“山长而低者”的地方，使臣认定到达琉求，但跟当地人语言不通，登陆的两百余人中有三人遭到杀害，无功而返，元成宗元贞三年，福建省平章政事高兴言：“今立省泉州，距琉求为近，可伺其消息，或宜招宜伐，不必它调兵力，兴请就近试之。”九月，高兴遣省都镇抚张浩、福州新军万户张进赴琉求国，生擒一百三十余人而返。

{{-}}

[澎湖寨巡檢](../Category/澎湖寨巡檢.md "wikilink")
[Category:台灣政治史](../Category/台灣政治史.md "wikilink")
[Category:元朝地方政府](../Category/元朝地方政府.md "wikilink")
[Category:明朝地方政府](../Category/明朝地方政府.md "wikilink")

1.  「泉有海島曰澎湖，隸晉江縣，取其國密邇，煙火相望，時至寇掠。其來不測，多罹生噉之害，居民苦之。」 趙汝适《諸蕃志》
2.  「澎湖隸泉州晉江縣，至元年間設巡檢司」汪大淵《島夷志略》
3.  「（乾道7年）四月起知泉州，到郡遇事風生，不勞而辦，郡實瀕海，中有沙洲數萬畝，號平湖，忽為島夷號毗舍邪者奄至，盡刈所種。他日又登海岸殺略，禽四百餘人，殲其渠魁，餘分配諸郡。初則每遇南風，遣戍為備，更迭勞擾，公即其地，造屋二百間，遣將分屯，軍民皆以為便，不敢犯境。」樓鑰《攻媿集》敷文閣學士宣奉大夫致仕贈特進汪公行狀〉
4.  「海中大洲號平湖，邦人就植粟、麥、麻。有毗舍耶蠻，揚颿奄至，肌體漆黑，語言不通，種植皆為所穫。調兵逐捕，則入水持其舟而已。俘民為鄉導，劫掠近城赤洲。於是春夏遣戍，秋暮始歸，勞費不貲。公即其地，造屋二百區，留屯水軍，蠻不復來。周必大《文忠集》〈神道碑〉
5.  Blussé, Leonard (1994). "Retribution and Remorse: The Interaction
    between the Administration and the Protestant Mission in Early
    Colonial Formosa". In Prakash, Gyan. After Colonialism: Imperial
    Histories and Postcolonial Displacement. Princeton University Press.
    ISBN 978-0691037424.
6.  村上直次郎著，許賢瑤譯，2001，〈澎湖島上的荷蘭人〉，《荷蘭時代台灣史論文集》，頁14。宜蘭縣宜蘭市：佛光人文社會學院。