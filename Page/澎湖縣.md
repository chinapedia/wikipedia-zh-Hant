**澎湖縣**（）是[中華民國](../Page/中華民國.md "wikilink")[臺灣省唯一的](../Page/臺灣省.md "wikilink")[離島](../Page/離島.md "wikilink")[縣](../Page/縣_\(中華民國\).md "wikilink")，位於[臺灣海峽上](../Page/臺灣海峽.md "wikilink")，並以[澎湖水道與](../Page/澎湖水道.md "wikilink")[臺灣本島相互遙望](../Page/臺灣.md "wikilink")。全縣由[澎湖群島所組成](../Page/澎湖群島.md "wikilink")。由於[地理](../Page/地理.md "wikilink")、[國防](../Page/國防.md "wikilink")[軍事](../Page/軍事.md "wikilink")[戰略位置極佳](../Page/戰略.md "wikilink")，自古以來便是往來臺灣海峽的船隻中繼站、與各方[經濟和](../Page/经济.md "wikilink")[軍事爭奪的要地](../Page/軍事.md "wikilink")。全縣為四面環海，居民主要是以[漁業維生](../Page/漁業.md "wikilink")；[礦產則以](../Page/礦產.md "wikilink")[文石為主](../Page/文石.md "wikilink")\[1\]，但近年來大力推動[觀光產業](../Page/觀光.md "wikilink")。

## 歷史

[Penghu_Mazu_Temple.jpg](https://zh.wikipedia.org/wiki/File:Penghu_Mazu_Temple.jpg "fig:Penghu_Mazu_Temple.jpg")[澎湖天后宮老照片](../Page/澎湖天后宮.md "wikilink")\]\]
[史前時期](../Page/史前時期.md "wikilink")，澎湖有[粗繩文陶為代表的](../Page/粗繩文陶.md "wikilink")[新石器文化存在](../Page/新石器文化.md "wikilink")，證明该地至少在五千年前已有先住民生活。[連雅堂在](../Page/連雅堂.md "wikilink")1920年出版的《[臺灣通史](../Page/臺灣通史.md "wikilink")》稱：「澎湖之有居人，尤遠在[秦和](../Page/秦.md "wikilink")[漢之際](../Page/漢.md "wikilink")，或曰[楚滅](../Page/楚.md "wikilink")[越](../Page/越.md "wikilink")，越之子孫遷於[閩](../Page/閩.md "wikilink")，流落海上，或居於澎湖」。[宋代時](../Page/宋.md "wikilink")，「澎湖」一名正式見於[史書及](../Page/史書.md "wikilink")[地方志](../Page/地方志.md "wikilink")，最早有[漢人居住澎湖的確實記載始自](../Page/漢人.md "wikilink")[南宋](../Page/南宋.md "wikilink")，在南[宋理宗寶慶元年](../Page/宋理宗.md "wikilink")（西元1225年）宗室[趙汝适著](../Page/趙汝适.md "wikilink")「[諸蕃志](../Page/諸蕃志.md "wikilink")」中明確的指出「泉有海島曰**彭湖**，隸[晉江縣](../Page/晉江縣.md "wikilink")」。

西元1281年，元代在澎湖設置巡檢司，延續至明代。[鄭成功攻臺之役中](../Page/鄭成功攻臺之役.md "wikilink")，鄭成功軍隊曾攻下澎湖群島。至清代西元1727年設立澎湖廳，隸屬福建省。1895年4月清國於[甲午戰爭中戰敗](../Page/甲午戰爭.md "wikilink")，與日本簽署[馬關條約](../Page/馬關條約.md "wikilink")，將臺灣、澎湖，和遼東半島割讓予日本，唯澎湖早已於同年3月日軍發起的[澎湖之役中落入日軍之手](../Page/澎湖之役_\(1895年\).md "wikilink")。1920年日本政府改澎湖郡併入高雄州，後改回澎湖廳。1945年[第二次世界大戰日本戰敗撤出澎湖列島](../Page/第二次世界大戰.md "wikilink")，由中華民國治理，設澎湖縣，撥入新設立的台灣省。

1995年，成立[澎湖國家風景區](../Page/澎湖國家風景區.md "wikilink")，範圍為澎湖群島。2009年9月，澎湖縣進行[博弈條款公投](../Page/2009年澎湖縣博弈公民投票.md "wikilink")，贊成票數13,397票，反對票數17,359票，澎湖[博弈產業確定破局](../Page/博弈.md "wikilink")，將配合中央政策推動，朝國家能源低碳島方向前進。

2009年12月，澎湖縣[望安鄉](../Page/望安鄉.md "wikilink")[東吉嶼至](../Page/東吉嶼.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")[安南區](../Page/安南區.md "wikilink")[鹿耳門週邊海陸域](../Page/鹿耳門.md "wikilink")，正式成立[臺江國家公園](../Page/臺江國家公園.md "wikilink")。2014年6月：[澎湖南方四島國家公園公告實施](../Page/澎湖南方四島國家公園.md "wikilink")。

## 地理

[Penghu_201506.jpg](https://zh.wikipedia.org/wiki/File:Penghu_201506.jpg "fig:Penghu_201506.jpg")
[澎湖群島位於](../Page/澎湖群島.md "wikilink")[中國大陸與](../Page/中國大陸.md "wikilink")[台湾岛之間的](../Page/台湾岛.md "wikilink")[臺灣海峽上](../Page/臺灣海峽.md "wikilink")，由90座大小不等之島嶼組成。近年，因澎湖群島特殊的玄武岩地質景觀，國際地質專家建議爭取列為世界遺產或設置為國家地質公園，引發國內地質學者及文史工作者對澎湖群島64座島嶼、總面積126.864平方公里資料的正確性提出質疑。上述資料係[日治時期由日本政府於](../Page/臺灣日治時期.md "wikilink")[大正](../Page/大正.md "wikilink")5年（1916年）所測得。官方文獻紀錄及資料之引述，長久以來均以此為依據。澎湖群島地勢為起伏平緩、山勢低矮的丘陵地，澎湖本島山勢最高的拱北山也只有海拔52公尺高，澎湖外島最高的望安鄉大貓嶼則有海拔79公尺。著名的有湖西鄉的拱北山、[奎壁山以及西嶼鄉的牛心山](../Page/奎壁山.md "wikilink")。

在[中華民國](../Page/中華民國.md "wikilink")[教科書上](../Page/教科書.md "wikilink")，一直沿用[日治時期以來的資料](../Page/台灣日治時期.md "wikilink")，稱澎湖縣共有64座[島嶼](../Page/島嶼.md "wikilink")；但在2005年12月，[澎湖縣政府乃擬具](../Page/澎湖縣政府.md "wikilink")『澎湖群島島嶼數量委託清查計畫』，委託[國立高雄應用科技大學調查](../Page/國立高雄應用科技大學.md "wikilink")。調查結果顯示，澎湖縣的島嶼共有90座，面積亦從126.8641平方公里增至141.0520平方公里，增加了14.1879平方公里。位置位於北緯23°12至23°47，東經119°19至119°43，島嶼數為90座。極東：[查母嶼](../Page/查母嶼.md "wikilink")；
極西：[花嶼](../Page/花嶼.md "wikilink")； 極南：[七美嶼](../Page/七美嶼.md "wikilink")；
極北：[大蹺嶼](../Page/大蹺嶼.md "wikilink")，[北回歸線](../Page/北回歸線.md "wikilink")23°27穿過群島之中的[虎井嶼之南](../Page/虎井嶼.md "wikilink")。不過至今[內政部卻仍未審核通過](../Page/中華民國內政部.md "wikilink")，因此其隸屬的[主計處仍舊維持原資料](../Page/行政院主計總處.md "wikilink")\[2\]
\[3\]\[4\]\[5\]。

## 區劃人口

[Penghuadm.png](https://zh.wikipedia.org/wiki/File:Penghuadm.png "fig:Penghuadm.png")
民國三十四年（1945年）臺灣脫離日本統治，[澎湖廳改制為澎湖縣](../Page/澎湖廳.md "wikilink")，縣治設於[馬公鎮](../Page/馬公市.md "wikilink")，共轄有2[縣轄區](../Page/縣轄區_\(中華民國\).md "wikilink")6鄉鎮，民國三十九年（1950年）廢縣轄區由縣直轄鄉鎮。民國七十年（1981年）馬公鎮升格為縣轄市，全縣轄有1[市](../Page/縣轄市.md "wikilink")（馬公市）、5[鄉](../Page/鄉_\(中華民國\).md "wikilink")（湖西鄉、白沙鄉、西嶼鄉、望安鄉、七美鄉），共有6個鄉市。

  - 以下資料為2019年3月[資料](http://www.ris.gov.tw/zh_TW/346)

<table>
<thead>
<tr class="header">
<th><p>鄉鎮市名</p></th>
<th><p>面積<br />
<small>（km²）</small></p></th>
<th><p>下轄村<br />
-{里}-數</p></th>
<th><p>下轄鄰數</p></th>
<th><p>人口數</p></th>
<th><p>人口<br />
消長</p></th>
<th><p>人口密度<br />
<small>（人/km²）</small></p></th>
<th><p>郵遞區號</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/馬公市.md" title="wikilink">馬公市</a></p></td>
<td><p>33.9918</p></td>
<td><p>33</p></td>
<td><p>628</p></td>
<td><p>62,653</p></td>
<td><p><font color=blue>+47</font></p></td>
<td><p>1,843</p></td>
<td><p>880</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湖西鄉.md" title="wikilink">湖西鄉</a></p></td>
<td><p>33.3008</p></td>
<td><p>22</p></td>
<td><p>238</p></td>
<td><p>14,673</p></td>
<td><p><font color=red>-4</font></p></td>
<td><p>441</p></td>
<td><p>885</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白沙鄉_(台灣).md" title="wikilink">白沙鄉</a></p></td>
<td><p>20.0875</p></td>
<td><p>15</p></td>
<td><p>179</p></td>
<td><p>9,803</p></td>
<td><p><font color=red>-10</font></p></td>
<td><p>488</p></td>
<td><p>884</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西嶼鄉.md" title="wikilink">西嶼鄉</a></p></td>
<td><p>18.7148</p></td>
<td><p>11</p></td>
<td><p>167</p></td>
<td><p>8,339</p></td>
<td><p><font color=red>-5</font></p></td>
<td><p>446</p></td>
<td><p>881</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/望安鄉.md" title="wikilink">望安鄉</a></p></td>
<td><p>13.7824</p></td>
<td><p>9</p></td>
<td><p>128</p></td>
<td><p>5,280</p></td>
<td><p><font color=blue>+11</font></p></td>
<td><p>383</p></td>
<td><p>882</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/七美鄉_(台灣).md" title="wikilink">七美鄉</a></p></td>
<td><p>6.9868</p></td>
<td><p>6</p></td>
<td><p>61</p></td>
<td><p>3,826</p></td>
<td><p><font color=blue>+3</font></p></td>
<td><p>548</p></td>
<td><p>883</p></td>
</tr>
<tr class="odd">
<td><p><strong>澎湖縣</strong></p></td>
<td><p>126.8641</p></td>
<td><p>96</p></td>
<td><p>1,400</p></td>
<td><p>104,574</p></td>
<td><p><font color=blue><strong>+41</strong></font></p></td>
<td><p>824</p></td>
<td></td>
</tr>
</tbody>
</table>

## 政治

### 縣政組織

[澎湖廳舍_peiiunwu.jpg](https://zh.wikipedia.org/wiki/File:澎湖廳舍_peiiunwu.jpg "fig:澎湖廳舍_peiiunwu.jpg")大樓\]\]
[澎湖縣政府是澎湖縣的最高](../Page/澎湖縣政府.md "wikilink")[行政機關](../Page/行政機關.md "wikilink")，在中華民國政府架構中為縣自治的行政機關，同時負責執行中央機關委辦事項，澎湖縣的自治監督機關為行政院各部會（主要為[內政部](../Page/中華民國內政部.md "wikilink")）。縣長由全體縣民直接選舉產生，任期為四年，連選可連任一次。澎湖縣政府並置縣政會議，為縣政最高決策機構，在縣長及副縣長之下，設有11個內部單位、7個所屬一級機關、9個所屬二級機關、54所各級學校。

澎湖縣縣長是[澎湖縣政府之行政首長](../Page/澎湖縣政府.md "wikilink")，負責綜理縣政，並指揮、監督所屬職員及機構。現任澎湖縣縣長為[中國國民黨籍的](../Page/中國國民黨.md "wikilink")[賴峰偉](../Page/賴峰偉.md "wikilink")。

[澎湖縣議會是澎湖縣的最高](../Page/澎湖縣議會.md "wikilink")[民意機關](../Page/民意機關.md "wikilink")，代表澎湖縣全體縣民立法和監察縣政府之施政。縣議員由公民直選選出，任期為四年，可連選連任。共有19位縣議員，第一選區11席縣議員、第二選區3席縣議員、第三選區2席縣議員、第四選區1席縣議員、第五選區1席縣議員、第六選區1席縣議員，議長、副議長由19位縣議員互選產生。

### 司法暨檢察機關

[臺灣澎湖地方法院是澎湖縣的](../Page/臺灣澎湖地方法院.md "wikilink")[司法機關](../Page/司法機關.md "wikilink")，屬於普通法院，[臺灣澎湖地方檢察署是澎湖縣的](../Page/臺灣澎湖地方檢察署.md "wikilink")[檢察機關](../Page/檢察機關.md "wikilink")，當地居民之民、刑訴訟和審理由該機關辦理。

### 選民結構及政治勢力

澎湖縣是[中華民國](../Page/中華民國.md "wikilink")[臺灣省下轄唯一的離島縣](../Page/臺灣省.md "wikilink")，位於[臺灣海峽上](../Page/臺灣海峽.md "wikilink")，由於縣內遍布[天人菊](../Page/天人菊.md "wikilink")，有「[菊島](../Page/菊島.md "wikilink")」的美稱。由於[地理](../Page/地理.md "wikilink")、[軍事位置極佳](../Page/軍事.md "wikilink")，自古以來便是各方爭奪的要地。然而，由於交通、及經濟因素，該縣長期面臨人口外移、高齡化等問題\[6\]。該縣原為藍略大於綠的地區，近期卻呈現泛綠領先泛藍五個百分點左右。

該縣早期地方派系，主要依縣治[馬公市地理劃分為南](../Page/馬公市.md "wikilink")、北兩派，以及其日後衍生的西蘭派、東許派、及[許素葉系統](../Page/許素葉.md "wikilink")。在[戒嚴時期](../Page/戒嚴時期.md "wikilink")，由於澎湖特殊的政治及軍事環境，縣府長期由[中華民國國軍背景的國民黨掌握](../Page/中華民國國軍.md "wikilink")，地方派系的影響力也因此受限。隨著1990年代末期[台灣民主化](../Page/台灣民主化.md "wikilink")，派系力量更進一步衰退，[黨外力量也隨著崛起](../Page/黨外.md "wikilink")。而民進黨亦同時在澎湖迅速成長，票源逐年上揚，近期泛綠已能與泛藍分庭抗禮。自解除[黨禁至今](../Page/黨禁.md "wikilink")，該縣已歷經三次政黨輪替。

### 各項選舉

按中華民國現行立法委員選制，澎湖縣全境同屬一個立法委員選區，得選出一位立法委員。現任澎湖選區所選出的立法委員，是[2016年立法委員選舉選出的](../Page/2016年中華民國立法委員選舉.md "wikilink")[楊曜](../Page/楊曜.md "wikilink")（民主進步黨提名）。

## 經濟

## 文化

### 文化資產

#### 有形文化資產

至2012年2月10日為止，澎湖縣文化局已經調查研究並且出版成果的有形文化資產有：

  - 古蹟、歷史建築、聚落：
      - [天后宮](../Page/澎湖天后宮.md "wikilink")
      - 許家聚落
      - [馬公三官殿](../Page/澎湖三官殿.md "wikilink")
      - 舊網垵社聚落
      - 望安鄉水垵村聚落
      - [二崁聚落](../Page/二崁聚落.md "wikilink")

<!-- end list -->

  - 文化景觀
      - 澎湖縣境內的漁滬景觀及文化
      - 白沙鄉赤崁和吉貝兩村的漁撈活動及生活

#### 無形文化資產

至2012年2月10日為止，澎湖文化局已經調查研究並且出版成果的無形文化資產有：

  - 傳統藝術：
      - 澎湖境內的諺語
      - 傳統歌謠：[褒歌](../Page/褒歌.md "wikilink")
      - 傳統音樂：小法儀式音樂、[八音](../Page/八音.md "wikilink")、[南管](../Page/南管.md "wikilink")
      - 澎湖傳統民宅之裝飾藝術

<!-- end list -->

  - 民俗及有關文物：
      - 澎湖的糕餅業
      - 澎湖廟宇、[民間祭典之應用文書](../Page/民間祭典.md "wikilink")
      - 澎湖的[五營信仰](../Page/五營.md "wikilink")
      - 澎湖群島的[犒軍儀式](../Page/犒軍.md "wikilink")、[法師系統與村廟組織](../Page/法師.md "wikilink")

<!-- end list -->

  - 古物
      - 澎湖地區的姓氏族譜

<!-- end list -->

  - 自然地景：
      - [玄武岩](../Page/玄武岩.md "wikilink")
      - 澎湖群島的地質
      - 澎湖群島的動物化石

### 文學

在文學史及相關論述方面，至2012年2月已出版的專書有《澎湖研究第一屆學術研討會論文集》、葉連鵬的《澎湖文學發展之研究》、文訊雜誌社出版的「高屏澎地區文學會議」論文集；碩博士論文有葉連鵬〈澎湖文學發展之研究〉、姜佩君〈澎湖民間故事研究〉等。

傳統文人有[蔡廷蘭](../Page/蔡廷蘭.md "wikilink")（清朝[康熙年間](../Page/康熙.md "wikilink")）。在澎湖縣出生的作家有[王福東](../Page/王福東.md "wikilink")、[岑澎維](../Page/岑澎維.md "wikilink")、[鹿憶鹿](../Page/鹿憶鹿.md "wikilink")、[葉言都](../Page/葉言都.md "wikilink")\[7\]。曾被縣文化局（改制前的文化中心）出版過作品的作家有[歐銀釧](../Page/歐銀釧.md "wikilink")、[謝聰明](../Page/謝聰明.md "wikilink")、[張怡媚](../Page/張怡媚.md "wikilink")、[許勝文](../Page/許勝文.md "wikilink")、[陳秋環](../Page/陳秋環.md "wikilink")、[沈瑞華](../Page/沈瑞華.md "wikilink")、[黃東永](../Page/黃東永.md "wikilink")、[林麗卿](../Page/林麗卿.md "wikilink")、[初惠誠](../Page/初惠誠.md "wikilink")、[張慧玲](../Page/張慧玲.md "wikilink")、[黃春鶯](../Page/黃春鶯.md "wikilink")、[陳鼎盛](../Page/陳鼎盛.md "wikilink")、[李錫文](../Page/李錫文.md "wikilink")、[薛東埠](../Page/薛東埠.md "wikilink")、[蔡愛清](../Page/蔡愛清.md "wikilink")、[顏玉露](../Page/顏玉露.md "wikilink")、[黃中堅](../Page/黃中堅.md "wikilink")、[余建中](../Page/余建中.md "wikilink")、[洪東碧](../Page/洪東碧.md "wikilink")、[張子樟](../Page/張子樟.md "wikilink")、[黃仁元](../Page/黃仁元.md "wikilink")、[方英福](../Page/方英福.md "wikilink")、[陳雪美](../Page/陳雪美.md "wikilink")、[蕭瓊瑞](../Page/蕭瓊瑞.md "wikilink")、[蔡宛璇](../Page/蔡宛璇.md "wikilink")、[張慶麟](../Page/張慶麟.md "wikilink")。縣內的文化工作者有[郭金龍](../Page/郭金龍\(作家\).md "wikilink")、[洪國雄](../Page/洪國雄.md "wikilink")、[呂文雄](../Page/呂文雄.md "wikilink")、[陳石筆](../Page/陳石筆.md "wikilink")、[王文良](../Page/王文良.md "wikilink")、[鄭美珠等](../Page/鄭美珠.md "wikilink")。

基金會及文學團體有[澎湖采風文化協會](../Page/澎湖采風文化協會.md "wikilink")、[澎湖鼎灣監獄寫作班](../Page/澎湖鼎灣監獄寫作班.md "wikilink")。推廣及贊助文學活動的基金會有「澎湖縣文化基金會」\[8\]；縣內沒有全國性文學社團\[9\]。

該縣文化局舉辦的文學獎為「[菊島文學獎](../Page/菊島文學獎.md "wikilink")」。除了文學獎，文化局也曾舉辦文藝營隊—[菊島文藝營](../Page/菊島文藝營.md "wikilink")。

### 大眾媒體

#### 廣播電台

澎湖縣可以接收眾多家合法廣播電台，如警廣治安交通網高雄台、教育廣播電台等，也可接收嘉義地區的中功率電台。

#### 調頻（FM）

| 頻率             | 電台名稱                                                                          |
| -------------- | ----------------------------------------------------------------------------- |
| 89.7 兆赫        | 澎湖社區廣播電台                                                                      |
| 91.3 兆赫        | 風聲廣播電台（[快樂聯播網](../Page/快樂聯播網.md "wikilink")）                                  |
| 91.9 兆赫        | 嘉南廣播電台                                                                        |
| 93.3 兆赫        | [雲嘉廣播電台](../Page/雲嘉調頻廣播電台.md "wikilink")                                      |
| 93.9 兆赫        | 大地之聲廣播電台（青春線上）                                                                |
| 96.3 兆赫        | 原民廣播電台                                                                        |
| 96.7 兆赫        | 澎湖廣播電台（[快樂聯播網](../Page/快樂聯播網.md "wikilink")）                                  |
| 99.1 兆赫        | [國立教育廣播電臺澎湖一網](../Page/國立教育廣播電臺.md "wikilink")                                |
| 100.3 兆赫       | 寶島廣播電台                                                                        |
| 103.1/103.3 兆赫 | [中廣流行網](../Page/中廣流行網.md "wikilink")                                          |
| 104.3 兆赫       | 講客廣播電台 澎湖轉播站（開播時間未定）                                                          |
| 105.3 兆赫       | [國立教育廣播電臺澎湖二網](../Page/國立教育廣播電臺.md "wikilink")                                |
| 105.7 兆赫       | 姊妹電台（雲林地區電台）                                                                  |
| 106.1 兆赫       | [全國廣播](../Page/全國廣播.md "wikilink")（澎湖本島、[虎井嶼可收聽到](../Page/虎井嶼.md "wikilink")） |
| 107.1 兆赫       | 環球廣播電台                                                                        |

#### 調幅（AM）

| 頻率      | 電台名稱                                         |
| ------- | -------------------------------------------- |
| 756 千赫  | 勝利之聲廣播電台 馬公轉播台                               |
| 1143 千赫 | [漁業廣播電台](../Page/行政院農業委員會漁業署.md "wikilink")  |
| 1269 千赫 | [漢聲廣播電台澎湖台](../Page/漢聲廣播電台.md "wikilink")    |
| 1296 千赫 | [中廣新聞網](../Page/中廣新聞網.md "wikilink")（台南地區電台） |

## 教育

  - 大專院校
      - [國立澎湖科技大學](../Page/國立澎湖科技大學.md "wikilink")
      - [國立空中大學](../Page/國立空中大學.md "wikilink")[澎湖中心](http://nouphc.nou.edu.tw/)
  - 高級中學
      - [國立馬公高級中學](../Page/國立馬公高級中學.md "wikilink")
  - 高級職業學校
      - [國立澎湖高級海事水產職業學校](../Page/國立澎湖高級海事水產職業學校.md "wikilink")

## 醫療

  - [行政院衛生福利部澎湖醫院](http://www.pngh.mohw.gov.tw/)
  - [三軍總醫院澎湖分院](http://afph.tsgh.ndmctsgh.edu.tw/)

## 交通

[澎湖縣觀光人數統計表.JPG](https://zh.wikipedia.org/wiki/File:澎湖縣觀光人數統計表.JPG "fig:澎湖縣觀光人數統計表.JPG")

### 航空

  - [澎湖機場有航線往返臺灣本島的](../Page/澎湖機場.md "wikilink")[臺北松山機場](../Page/台北松山機場.md "wikilink")、[台中清泉崗機場](../Page/台中清泉崗機場.md "wikilink")、[嘉義機場](../Page/嘉義機場.md "wikilink")、[台南機場](../Page/台南機場.md "wikilink")、[高雄國際機場](../Page/高雄國際機場.md "wikilink")、[七美機場以及](../Page/七美機場.md "wikilink")[金門機場](../Page/金門機場.md "wikilink")．
  - 兩岸直航後，於每週二、五，提供客運包機申請，得以往返[武漢機場](../Page/武汉天河国际机场.md "wikilink")。

### 海運

  - [馬公港有航線往返臺灣本島的](../Page/馬公港.md "wikilink")[臺中港](../Page/台中港.md "wikilink")、[布袋港](../Page/布袋港.md "wikilink")、[高雄港以及澎湖其他島嶼](../Page/高雄港.md "wikilink")；[七美港亦有航線往返高雄港](../Page/七美港.md "wikilink")。

### 公路

  - 縣道

<!-- end list -->

  -
  -
  -
  -
  -
<!-- end list -->

  - 鄉道

### 鐵路

  - 1924年，軍用鐵道[馬公港支線開通](../Page/馬公港支線.md "wikilink")，為澎湖群島當時境內唯一一條[鐵路](../Page/鐵路.md "wikilink")；該路線於1947年廢止，1951年全線拆除。\[10\]

## 旅遊

澎湖群島獨特的景色與人文，使其榮獲[寂寞星球的](../Page/寂寞星球.md "wikilink")2011年全球十大最佳世外桃源島嶼\[11\]\[12\]與獲得成為[世界最美麗海灣俱樂部](../Page/世界最美麗海灣俱樂部.md "wikilink")（The
Most Beautiful Bays of the World）的會員之一\[13\]。

### 自然、人文景點

[Penghu_CrossSeeBridge.jpg](https://zh.wikipedia.org/wiki/File:Penghu_CrossSeeBridge.jpg "fig:Penghu_CrossSeeBridge.jpg")\]\]
[中央老街2.JPG](https://zh.wikipedia.org/wiki/File:中央老街2.JPG "fig:中央老街2.JPG")
[澎湖西瀛虹橋.jpg](https://zh.wikipedia.org/wiki/File:澎湖西瀛虹橋.jpg "fig:澎湖西瀛虹橋.jpg")
[Penghu-coral-wall.jpg](https://zh.wikipedia.org/wiki/File:Penghu-coral-wall.jpg "fig:Penghu-coral-wall.jpg")

  - 澎湖花火節
  - [澎湖國家風景區](../Page/澎湖國家風景區.md "wikilink")（1995年成立）
  - [咾咕石屋](../Page/咾咕石.md "wikilink")
  - [澎湖跨海大橋](../Page/澎湖跨海大橋.md "wikilink")
  - 西瀛虹橋
  - [吉貝嶼](../Page/吉貝嶼.md "wikilink")
  - [七美島雙心](../Page/七美島.md "wikilink")[石滬](../Page/石滬.md "wikilink")
  - [貓嶼海鳥保護區](../Page/貓嶼海鳥保護區.md "wikilink")
  - [黑水溝](../Page/黑水溝.md "wikilink")（海底[玄武岩壁](../Page/玄武岩.md "wikilink")）
  - [綠蠵龜產卵棲地保護區](../Page/望安島綠蠵龜產卵棲地保護區.md "wikilink")（望安島）
  - [澎湖玄武岩自然保留區](../Page/澎湖玄武岩自然保留區.md "wikilink")（[雞善嶼](../Page/雞善嶼.md "wikilink")、[碇鉤嶼](../Page/碇鉤嶼.md "wikilink")、[小白沙嶼](../Page/小白沙嶼.md "wikilink")）、[澎湖南海玄武岩自然保留區與](../Page/澎湖南海玄武岩自然保留區.md "wikilink")[桶盤玄武岩石柱](../Page/桶盤玄武岩石柱.md "wikilink")
  - [馬公觀音亭](../Page/馬公觀音亭.md "wikilink")（西瀛虹橋）

請參見：

  - [澎湖旅遊景點列表](../Page/澎湖旅遊景點列表.md "wikilink")

### 博物館

位於澎湖縣境內，被[中華民國博物館學會列入](../Page/中華民國博物館學會.md "wikilink")2004年9月版《台灣博物館名錄》的博物館有：

  - [馬公市](../Page/馬公市.md "wikilink")：
      - [二呆藝術館](../Page/二呆藝術館.md "wikilink")、[朱盛文物紀念館](../Page/朱盛文物紀念館.md "wikilink")、[雅輪文石紀念館](../Page/雅輪文石紀念館.md "wikilink")、[澎湖希望天地](../Page/澎湖希望天地.md "wikilink")、[澎湖海洋資源館](../Page/澎湖海洋資源館.md "wikilink")、[澎湖縣科學館](../Page/澎湖縣科學館.md "wikilink")、[澎湖縣鄉土資源教育中心](../Page/澎湖縣鄉土資源教育中心.md "wikilink")。

<!-- end list -->

  - 其他鄉鎮：
      - [小門地質館](../Page/小門地質館.md "wikilink")、[澎湖望安綠蠵龜觀光保育中心](../Page/澎湖望安綠蠵龜觀光保育中心.md "wikilink")、[水產試驗所澎湖海洋生物研究中心附設水族館](../Page/水產試驗所澎湖海洋生物研究中心附設水族館.md "wikilink")、[吉貝文物館](../Page/吉貝文物館.md "wikilink")、[澎湖莊家莊民俗館](../Page/澎湖莊家莊民俗館.md "wikilink")。

### 特產

  - [黑糖糕](../Page/黑糖糕.md "wikilink")
  - [仙人掌冰](../Page/仙人掌冰.md "wikilink")
  - [風茹茶](../Page/風茹茶.md "wikilink")
  - [冬瓜糕](../Page/糕點.md "wikilink")
  - 鹹餅
  - 稜角絲瓜
  - [嘉寶瓜](../Page/嘉寶瓜.md "wikilink")[](https://www.kdais.gov.tw/view.php?catid=3130)
  - [楊梅](../Page/楊梅.md "wikilink")（[香瓜茄](../Page/香瓜茄.md "wikilink")）
  - [花生](../Page/花生.md "wikilink")
  - [丁香魚乾](../Page/魚乾.md "wikilink")
  - [干貝醬](../Page/醬料.md "wikilink")
  - [珊瑚](../Page/珊瑚.md "wikilink")
  - [文石](../Page/文石.md "wikilink")
  - [玳瑁石斑](../Page/玳瑁石斑鱼.md "wikilink")（有縣魚之稱）
  - [海膽](../Page/:zh-tw:海膽.md "wikilink")

## 澎湖出身知名人物

[Army_(ROCA)_General_Wu_Ta-peng_陸軍上將吳達澎_(20110506_臺北市政府100年度全民防衛動員（萬安34號）暨複合型災害防救演習_15618222620).jpg](https://zh.wikipedia.org/wiki/File:Army_\(ROCA\)_General_Wu_Ta-peng_陸軍上將吳達澎_\(20110506_臺北市政府100年度全民防衛動員（萬安34號）暨複合型災害防救演習_15618222620\).jpg "fig:Army_(ROCA)_General_Wu_Ta-peng_陸軍上將吳達澎_(20110506_臺北市政府100年度全民防衛動員（萬安34號）暨複合型災害防救演習_15618222620).jpg")，首位澎湖出身的[國軍](../Page/國軍.md "wikilink")[上將](../Page/上將.md "wikilink")\]\]

### 軍事將領

  - [吳達澎](../Page/吳達澎.md "wikilink")：[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[二級上將](../Page/二級上將.md "wikilink")
  - [蔡得勝](../Page/蔡得勝.md "wikilink")：[中華民國國家安全局](../Page/中華民國國家安全局.md "wikilink")[中將](../Page/中將.md "wikilink")
  - [沈慶昌](../Page/沈慶昌.md "wikilink")：[中華民國海岸巡防署](../Page/中華民國海岸巡防署.md "wikilink")[少將](../Page/少將.md "wikilink")
  - [王瑞麟](../Page/王瑞麟.md "wikilink")：[中華民國海軍陸戰隊](../Page/中華民國海軍陸戰隊.md "wikilink")[中將](../Page/中將.md "wikilink")

### 政治人物

  - [黃清埕](../Page/黃清埕.md "wikilink")
  - [蔡廷蘭](../Page/蔡廷蘭.md "wikilink")：澎湖首位進士，曾因颱風困頓[越南月餘](../Page/越南.md "wikilink")，著有《海南雜著》。

### 作家

  - 陳淑瑤，著有《流水帳》、《塗雲記》。

### 藝人

  - [張雨生](../Page/張雨生.md "wikilink")
  - [潘安邦](../Page/潘安邦.md "wikilink")
  - [彭于晏](../Page/彭于晏.md "wikilink")
  - [柯震東](../Page/柯震東.md "wikilink")
  - [陳亞蘭](../Page/陳亞蘭.md "wikilink")
  - [林昕陽](../Page/林昕陽.md "wikilink")
  - [趙學煌](../Page/趙學煌.md "wikilink")
  - [蔡閨](../Page/蔡閨.md "wikilink")

### 企業家

  - [張百萬](../Page/張百萬.md "wikilink")
  - [白文正](../Page/白文正.md "wikilink")
  - [許明仁](../Page/許明仁.md "wikilink")
  - [藍俊昇](../Page/藍俊昇.md "wikilink")
  - [王建民](../Page/王建民.md "wikilink")

### 學者

  - [許雪姬](../Page/許雪姬.md "wikilink")：臺灣史學家，[中央研究院臺灣史研究所研究員](../Page/中央研究院臺灣史研究所.md "wikilink")，於2005年出版《續修澎湖縣志》中擔任總編。
  - [宋佩瑄](../Page/宋佩瑄.md "wikilink")：[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[上校](../Page/上校.md "wikilink")，現任[花蓮](../Page/花蓮.md "wikilink")[大漢技術學院博士校長](../Page/大漢技術學院.md "wikilink")
  - [鄭同僚](../Page/鄭同僚.md "wikilink")：國立[政治大學教育學系副教授](../Page/國立政治大學.md "wikilink")，曾任公共電視文化事業基金會董事長。長年關心偏鄉教育和教育改革，並實際參與實驗教育，著有《案山里100號》、《花嶼村2號：澎湖小島踏查筆記》

### 慈善家

  - [莊朱玉女](../Page/莊朱玉女.md "wikilink")

### 運動員

  - [李振昌](../Page/李振昌.md "wikilink")：職棒選手（出身[湖西鄉龍門村](../Page/湖西鄉.md "wikilink")）
  - [沈慶昌](../Page/沈慶昌.md "wikilink")
  - [呂奇旻](../Page/呂奇旻.md "wikilink")：職籃選手
  - [薛弘偉](../Page/薛弘偉.md "wikilink")：(HKES)電競選手
  - [郭駿傑](../Page/郭駿傑.md "wikilink")：職棒選手
  - [黃偉晟](../Page/黃偉晟.md "wikilink")：職棒選手
  - [姚百川](../Page/姚百川.md "wikilink")：職棒選手
  - [吳英偉](../Page/吳英偉.md "wikilink")：職棒選手
  - [林正豐](../Page/林正豐.md "wikilink")：職棒選手
  - [洪正欽](../Page/洪正欽.md "wikilink")：職棒選手
  - [顏志中](../Page/顏志中.md "wikilink")：職棒選手
  - [林紹凱](../Page/林紹凱.md "wikilink")：職棒選手
  - [盧劭禹](../Page/盧劭禹.md "wikilink")：職棒選手
  - [許文錚](../Page/許文錚.md "wikilink")：職棒選手
  - [鄭佳彥](../Page/鄭佳彥.md "wikilink")：職棒選手

## 在澎湖拍攝的戲劇及MV

### 電影

  - [任賢齊](../Page/任賢齊.md "wikilink")《落跑吧愛情》
  - [連奕琦](../Page/連奕琦.md "wikilink")《癡情男子漢》
  - [侯孝賢](../Page/侯孝賢.md "wikilink")《風櫃來的人》
  - [陸小芬](../Page/陸小芬.md "wikilink")《桂花巷》

### 電視劇

  - [梁修身](../Page/梁修身.md "wikilink")《陽光正藍》

### MV

  - [江美琪](../Page/江美琪.md "wikilink")《親愛的你怎麼不在身邊》
  - [信樂團](../Page/信樂團.md "wikilink")《癡癡的等》
  - [蕭亞軒](../Page/蕭亞軒.md "wikilink")《愛上愛》
  - [劉若英](../Page/劉若英.md "wikilink")《為愛癡狂》
  - [劉若英](../Page/劉若英.md "wikilink")《人之初》
  - [辛曉琪](../Page/辛曉琪.md "wikilink")《遺忘》
  - [邱澤](../Page/邱澤.md "wikilink")《原味的夏天》
  - [張雨生](../Page/張雨生.md "wikilink")《一天到晚游泳的魚》
  - [許茹芸](../Page/許茹芸.md "wikilink")《如果雲知道》

<!-- end list -->

  - [許茹芸](../Page/許茹芸.md "wikilink")《芸且留住》
  - [伍思凱](../Page/伍思凱.md "wikilink")《分享》
  - [伍思凱](../Page/伍思凱.md "wikilink")《一半》
  - [伍思凱](../Page/伍思凱.md "wikilink")《最愛是你》
  - [郭富城](../Page/郭富城.md "wikilink")《別說》
  - [郭富城](../Page/郭富城.md "wikilink")《真的怕了》
  - [陳昇](../Page/陳昇.md "wikilink")《鏡子》
  - [陳昇](../Page/陳昇.md "wikilink")《風箏》
  - [張韶涵](../Page/張韶涵.md "wikilink")《Joureny》

<!-- end list -->

  - [孟庭葦](../Page/孟庭葦.md "wikilink")《風中有朵雨做的雲》
  - [林曉培](../Page/林曉培.md "wikilink")《手太小》
  - [潘安邦](../Page/潘安邦.md "wikilink")《外婆的澎湖灣》
  - [梁詠琪](../Page/梁詠琪.md "wikilink")《短髮》
  - [優客李林](../Page/優客李林.md "wikilink")《了解》
  - [黃義達](../Page/黃義達.md "wikilink")《藍天》
  - [許慧欣](../Page/許慧欣.md "wikilink")《放愛情一個假》
  - [李心潔](../Page/李心潔.md "wikilink")《愛像大海》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《澎湖戀歌》

<!-- end list -->

  - [詹雅雯](../Page/詹雅雯.md "wikilink")《深情海岸》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《我無想要哮》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《船頂的歐里桑》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《騙人彼多》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《感恩的花蕊》
  - [詹雅雯](../Page/詹雅雯.md "wikilink")《續緣》
  - [玖壹壹](../Page/玖壹壹.md "wikilink")《9453》
  - [SKE48](../Page/SKE48.md "wikilink")《意外的芒果》

## 相關條目

  - [澎湖群島](../Page/澎湖群島.md "wikilink")
  - [澎湖縣政府](../Page/澎湖縣政府.md "wikilink")
  - [澎湖縣議會](../Page/澎湖縣議會.md "wikilink")
  - [澎湖娘家宴](../Page/澎湖娘家宴.md "wikilink")

## 注釋

<div class="references-2column">

</div>

## 参考文献

## 外部連結

  - [澎湖縣政府](http://www.penghu.gov.tw/)
  - [澎湖縣議會](http://www.phcouncil.gov.tw/)
  - [澎湖逍遙遊](http://tour.penghu.gov.tw/)
  - [沿著菊島旅行－澎湖資訊網](http://www.phsea.net/)

## 社區旅行

  - [湖西鄉北寮社區](http://chyfun.com/beiliao-fishing-penghu/)
  - [湖西鄉南寮社區](http://chyfun.com/nanliao-farming-penghu/)
  - [馬公市菜園社區](http://chyfun.com/caiyuan-oyster-penghu/)
  - [西嶼鄉二崁社區](http://chyfun.com/erkan-baoge-penghu/)

{{-}}

[澎湖縣](../Category/澎湖縣.md "wikilink")
[Penghu](../Category/1945年建立的行政區劃.md "wikilink")

1.  方建能，《臺灣北部出產的美麗文石》，臺灣鑛業，第 65 卷第 3 期，第 I-IV 頁，2013年9月
2.  [最新統計！　美麗菊島　現在有90座島嶼喔～](http://www.ettoday.com/2005/12/16/1005-1882900.htm)
3.  [增15平方公里　64島嶼變90座　歷史地理將改寫](http://www.ettoday.com/2005/12/17/124-1883033.htm)
4.  [散在台灣海峽的珍珠～菊島美景　90座島嶼相伴](http://www.ettoday.com/2005/12/16/10844-1882903.htm)
5.  [澎湖群島
    變大又變多](http://www.libertytimes.com.tw/2005/new/dec/17/today-life4.htm)
6.  [人口高齡化，社會福利大挑戰](http://www.taiwanngo.tw/files/13-1000-9962-1.php?Lang=zh-tw)
    ，外交部 NGO 國際事務會
7.  台灣文學館，「2007台灣作家作品目錄」搜尋系統。
8.  台灣文學館出版《2007 台灣文學年鑑》的「文學活動基金會」名錄。
9.  台灣文學館出版《2007 台灣文學年鑑》的「文學團體」名錄。
10.
11. [全球10大秘密島嶼
    澎湖入選第7](http://www.libertytimes.com.tw/2010/new/dec/30/today-t3.htm)
12. [The world’s best secret
    islands](http://www.lonelyplanet.com/australia/travel-tips-and-articles/76176),
    寂寞星球新聞稿
13.