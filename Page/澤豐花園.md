[Affluence_Garden_Shopping_Centre_and_Market.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Shopping_Centre_and_Market.jpg "fig:Affluence_Garden_Shopping_Centre_and_Market.jpg")
[Affluence_Garden_Kindergarten,_Badminton_and_Volleyball_Court.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Kindergarten,_Badminton_and_Volleyball_Court.jpg "fig:Affluence_Garden_Kindergarten,_Badminton_and_Volleyball_Court.jpg")
[Affluence_Garden_Roller_Skating_Rink.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Roller_Skating_Rink.jpg "fig:Affluence_Garden_Roller_Skating_Rink.jpg")
[Affluence_Garden_Slide.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Slide.jpg "fig:Affluence_Garden_Slide.jpg")
[Affluence_Garden_Covered_Walkway_and_Seesaw+Spring_Rider.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Covered_Walkway_and_Seesaw+Spring_Rider.jpg "fig:Affluence_Garden_Covered_Walkway_and_Seesaw+Spring_Rider.jpg")
[Affluence_Garden_Fitness_Area.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Fitness_Area.jpg "fig:Affluence_Garden_Fitness_Area.jpg")
[Affluence_Garden_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Affluence_Garden_Pebble_Walking_Trail.jpg "fig:Affluence_Garden_Pebble_Walking_Trail.jpg")
**澤豐花園**（）是[香港](../Page/香港.md "wikilink")[新界一個](../Page/新界.md "wikilink")[居屋屋苑](../Page/居屋.md "wikilink")，座落[屯門中北部](../Page/屯門.md "wikilink")[屯門河畔](../Page/屯門河.md "wikilink")，地址為[大興青松觀路](../Page/大興_\(香港\).md "wikilink")33號，1989年落成，屬[香港房屋委員會](../Page/香港房屋委員會.md "wikilink")[居者有其屋第](../Page/居者有其屋.md "wikilink")10期甲及第10期乙的私人參建居屋計劃屋苑，由上潤有限公司的附屬公司澤安發展有限公司（）發展，由鍾華楠建築師事務所有限公司設計，管理公司為佳定物業管理有限公司。

屋苑建有5座住宅樓宇，每層提供12個單位，共2208個單位，單位[建築面積由](../Page/建築面積.md "wikilink")44至61平方米不等，[實用面積則為](../Page/實用面積.md "wikilink")42至55平方米不等，每座約高37層。苑內有樓高4層的獨立商場，提供熟食[街市](../Page/街市.md "wikilink")、[超市](../Page/超市.md "wikilink")、進修中心，亦有獨立[幼稚園及](../Page/幼稚園.md "wikilink")[托兒所等設施](../Page/托兒所.md "wikilink")。此外，屋苑亦設有多個休閒地區及康樂設施，例如園藝觀賞水池、[滾軸溜冰場](../Page/滾軸溜冰.md "wikilink")、[排球場](../Page/排球.md "wikilink")、[羽毛球場及數個兒童](../Page/羽毛球.md "wikilink")[遊樂場等](../Page/遊樂場.md "wikilink")。

## 樓宇

<table>
<thead>
<tr class="header">
<th><p>樓宇名稱[1]</p></th>
<th><p>層數</p></th>
<th><p>樓宇類型</p></th>
<th><p>落成年份[2]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一座<br />
澤國樓（Prosperland House）</p></td>
<td><p>37層</p></td>
<td><p>私人機構參建居屋計劃</p></td>
<td><p>1989</p></td>
</tr>
<tr class="even">
<td><p>第二座<br />
澤泰樓（Felicitous House）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三座<br />
澤民樓（Civic House）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四座<br />
澤安樓（Pacific House）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第五座<br />
澤富樓（Opulent House）</p></td>
<td><p>36層</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 商舖及設施

  - 街市
  - 商場
  - [仁愛堂專業培訓中心](../Page/仁愛堂.md "wikilink")（澤豐）
  - [華潤萬家超級市場](../Page/華潤萬家超級市場.md "wikilink")
  - 博智補習社
  - [基督教屯門靈光堂](../Page/基督教.md "wikilink")

Affluence Garden Car Park.jpg|停車場

## 鄰近公共設施

  - [屯門鄧肇堅運動場](../Page/屯門鄧肇堅運動場.md "wikilink")
  - [大興體育館](../Page/大興體育館.md "wikilink")
  - [賽馬會仁愛堂游泳池](../Page/賽馬會仁愛堂游泳池.md "wikilink")
  - [屯門醫院](../Page/屯門醫院.md "wikilink")
  - [青山醫院](../Page/青山醫院.md "wikilink")
  - 銀圍休憩公園
  - [屯門河畔公園](../Page/屯門河畔公園.md "wikilink")
  - [青松觀](../Page/青松觀.md "wikilink")

## 鄰近屋苑

  - [卓爾居](../Page/卓爾居.md "wikilink")
  - [大興邨](../Page/大興邨.md "wikilink")
  - 青松觀道政府宿舍

## 鄰近學校

  - [保良局蔡冠深幼稚園](http://www.kids-club.net/edu/plkckskg)（2001年創辦）（位於澤豐花園地下及2樓）
  - [中華基督教會譚李麗芬紀念中學](../Page/中華基督教會譚李麗芬紀念中學.md "wikilink")
  - [馬錦明慈善基金馬可賓紀念中學](../Page/馬錦明慈善基金馬可賓紀念中學.md "wikilink")

## 鄰近街道／橋樑

  - 澤豐街
  - 青松觀路
  - 澤豐橋
  - 新和里
  - 震寰路

## 途經之公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><a href="../Page/輕鐵.md" title="wikilink">輕鐵</a><a href="../Page/澤豐站.md" title="wikilink">澤豐站</a></li>
<li>輕鐵<a href="../Page/銀圍站.md" title="wikilink">銀圍站</a></li>
</ul>
<dl>
<dt><a href="../Page/大興巴士總站.md" title="wikilink">大興巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/震寰路.md" title="wikilink">震寰路</a></dt>

</dl>
<dl>
<dt><a href="../Page/青松觀路.md" title="wikilink">青松觀路</a></dt>

</dl>
<dl>
<dt><a href="../Page/屯門公路.md" title="wikilink">屯門公路</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參考

## 外部連結

  - [房委會澤豐花園資料](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-347-4_5493,00.html)
  - [佳定物業澤豐花園資料](http://www.savillsguardian.com.hk/html/customer/index.asp?id=1380)

[en:Public housing estates in Tuen Mun\#Affluence
Garden](../Page/en:Public_housing_estates_in_Tuen_Mun#Affluence_Garden.md "wikilink")

[Category:私人參建居屋](../Category/私人參建居屋.md "wikilink") [Category:大興
(香港)](../Category/大興_\(香港\).md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.  [澤豐花園](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-4_5493,00.html)

2.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>