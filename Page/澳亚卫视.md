**澳亞衛視有限公司**（[英語](../Page/英語.md "wikilink")：**M**acau **A**sia
**S**atellite **T**ele**v**ision
**Co**mpany.,**L**imi**t**e**d**，**MASTV
Co.,Ltd**)，簡稱：澳亞衛視（[英語](../Page/英語.md "wikilink")：MASTV）
，於2001年6月1日開播，是[澳門特别行政区批准以經營衛星電視為主要業務的持牌公司](../Page/澳門特别行政区.md "wikilink")。目前信号覆盖全球60多个国家和地区，澳亚卫视的卫星频道**澳亚卫视中文台**在2004年3月15日正式开播，但台徽只显示“澳亚卫视”四字。

## 節目定位

澳亞衛視主要定位的的觀眾的是[中國內地](../Page/中國內地.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[臺灣以及](../Page/臺灣.md "wikilink")[東南亞地區](../Page/東南亞.md "wikilink")。所以，澳亞衛視的整體節目基本上都是以[澳門為中心](../Page/澳門.md "wikilink")，辐射全世界。

## 現播新聞時段

自2018年12月24日起：（註：**粗體**為該節目首播時間）

<table>
<thead>
<tr class="header">
<th><p>節目</p></th>
<th><p>播放時間</p></th>
<th><p>播放日期</p></th>
<th><p>現任主播</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/清清早茶.md" title="wikilink">清清早茶</a><br />
Morning News</p></td>
<td><p><strong>07:00 - 07:42</strong></p></td>
<td><p>星期一至日</p></td>
<td><p>-{<a href="../Page/李冀釗(澳亞主播).md" title="wikilink">李冀釗</a>}- / <a href="../Page/華逸凡.md" title="wikilink">華逸凡</a></p></td>
</tr>
<tr class="even">
<td><p>08:00 - 08:42</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/澳亞金錢報.md" title="wikilink">澳亞金錢報</a></p></td>
<td><p><strong>07:45 - 07:57</strong></p></td>
<td><p>星期一至日</p></td>
<td><p>該日播報《清清早茶》主播</p></td>
</tr>
<tr class="even">
<td><p>08:45 - 08:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/午間快報.md" title="wikilink">午間快報</a><br />
Midday Report</p></td>
<td><p><strong>12:00 - 12:29</strong></p></td>
<td><p>星期一至日</p></td>
<td><p><a href="../Page/張曼鈺(澳亞主播).md" title="wikilink">張曼鈺</a> / -{<a href="../Page/李冀釗(澳亞主播).md" title="wikilink">李冀釗</a>}- /<br />
<a href="../Page/華逸凡.md" title="wikilink">華逸凡</a> / <a href="../Page/鍾鼎浩.md" title="wikilink">鍾鼎浩</a></p></td>
</tr>
<tr class="even">
<td><p>14:00 - 14:29</p></td>
<td><p>星期一至<br />
六 / 日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/澳亞快報.md" title="wikilink">澳亞快報</a></p></td>
<td><p><strong>18:00 - 18:03</strong></p></td>
<td><p>星期一至日</p></td>
<td><p>該日播報《澳亞新聞》主播</p></td>
</tr>
<tr class="even">
<td><p><strong>23:00 - 23:03</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>19:00 - 19:03</strong></p></td>
<td><p>星期一、二、四、<br />
五、六、日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>22:00 - 22:03</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>21:57 - 22:00</strong></p></td>
<td><p>星期三<br />
（香港賽馬日）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澳門萬象.md" title="wikilink">澳門萬象</a><br />
Macau News</p></td>
<td><p><strong>18:30 - 18:57</strong></p></td>
<td><p>星期一至日</p></td>
<td><p><a href="../Page/劉中志.md" title="wikilink">劉中志</a>／<a href="../Page/曾竟凡.md" title="wikilink">曾竟凡</a> /<br />
<a href="../Page/林美婷.md" title="wikilink">林美婷</a> / <a href="../Page/崔格僖.md" title="wikilink">崔格僖</a></p></td>
</tr>
<tr class="odd">
<td><p>23:03 - 23:27</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>02:00 - 02:27</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13:30 - 13:57</p></td>
<td><p>星期一至<br />
六 / 日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澳亞新聞.md" title="wikilink">澳亞新聞</a><br />
MASTV News</p></td>
<td><p><strong>20:00 - 20:57</strong></p></td>
<td><p>星期一至日</p></td>
<td><p><a href="../Page/林毓芝.md" title="wikilink">林毓芝</a> / <a href="../Page/鍾鼎浩.md" title="wikilink">鍾鼎浩</a> /<br />
<a href="../Page/劉中志.md" title="wikilink">劉中志</a> / <a href="../Page/陳佳惠.md" title="wikilink">陳珈婕</a></p></td>
</tr>
<tr class="odd">
<td><p>00:00 - 00:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>06:00 - 06:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 現播節目

自2018年12月1日起：（註：**粗體**為該節目首播時間）

<table>
<thead>
<tr class="header">
<th><p>節目</p></th>
<th><p>播放時間</p></th>
<th><p>播放日期</p></th>
<th><p>現任主持人</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/走進台灣.md" title="wikilink">走進台灣</a><br />
Taiwan Today</p></td>
<td><p><strong>21:00 - 21:57</strong></p></td>
<td><p>星期一至日</p></td>
<td><p><a href="../Page/張瑞玲.md" title="wikilink">張瑞玲</a>／<br />
<a href="../Page/林秀慧.md" title="wikilink">林秀慧</a>／<br />
<a href="../Page/賴岳謙.md" title="wikilink">賴岳謙</a>／<br />
<a href="../Page/唐湘龍.md" title="wikilink">唐湘龍</a></p></td>
</tr>
<tr class="even">
<td><p>01:00 - 01:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>05:00 - 05:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12:30 - 13:27</p></td>
<td><p>星期一至五</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11:00 - 11:57</p></td>
<td><p>星期六、日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三生有幸.md" title="wikilink">三生有幸</a></p></td>
<td><p><strong>19:30 - 19:57</strong></p></td>
<td><p>星期六、日</p></td>
<td><p><a href="../Page/高珊.md" title="wikilink">高珊</a>（兼製片人）</p></td>
</tr>
<tr class="odd">
<td><p>04:30 - 04:57</p></td>
<td><p>星期一至日（精選）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14:45 - 15:12</p></td>
<td><p>星期一至日（精選）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/順風順水.md" title="wikilink">順風順水</a><br />
Feng Shui Talk</p></td>
<td><p><strong>18:03 - 18:29</strong></p></td>
<td><p>星期二、四／<br />
星期一、三、五（精選）</p></td>
<td><p><a href="../Page/黃瀞儀.md" title="wikilink">黃瀞儀</a><br />
<a href="../Page/謝沅瑾.md" title="wikilink">謝沅瑾</a>（命理師）</p></td>
</tr>
<tr class="even">
<td><p>23:30 - 23:57</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>09:15 - 09:42</p></td>
<td><p>星期二至六</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澳門，共享繁榮.md" title="wikilink">澳門，共享繁榮</a></p></td>
<td><p><strong>19:03 - 19:28</strong></p></td>
<td><p>星期六</p></td>
<td><p><a href="../Page/林美婷.md" title="wikilink">林美婷</a><br />
（旁白）</p></td>
</tr>
<tr class="odd">
<td><p>17:30 - 17:27</p></td>
<td><p>星期一</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/感動澳門.md" title="wikilink">感動澳門</a></p></td>
<td><p><strong>19:03 - 19:28</strong></p></td>
<td><p>星期日</p></td>
<td><p><a href="../Page/林美婷.md" title="wikilink">林美婷</a><br />
（旁白）</p></td>
</tr>
<tr class="odd">
<td><p>17:30 - 17:27</p></td>
<td><p>星期二</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賽馬直擊.md" title="wikilink">賽馬直擊</a></p></td>
<td><p><strong>19:00 - 20:00</strong></p></td>
<td><p>星期三</p></td>
<td><p>請看《<strong>賽馬直擊</strong>》條目</p></td>
</tr>
<tr class="odd">
<td><p><strong>22:00 - 23:00</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>12:35 - 18:00</strong></p></td>
<td><p>星期六／日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/粵港澳大灣區進行式.md" title="wikilink">粵港澳大灣區進行式</a></p></td>
<td><p><strong>22:03 - 22:57</strong></p></td>
<td><p>星期六</p></td>
<td><p><a href="../Page/徐詩穎.md" title="wikilink">徐詩穎</a></p></td>
</tr>
<tr class="even">
<td><p>12:30 - 13:27</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/前進一帶一路.md" title="wikilink">前進一帶一路</a></p></td>
<td><p><strong>22:03 - 22:57</strong></p></td>
<td><p>星期日</p></td>
<td><p><a href="../Page/林毓芝.md" title="wikilink">林毓芝</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 已下檔節目

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>澳門，預約幸福</li>
<li>傲世財經線</li>
<li>十分科幻時代</li>
<li>一美一輩子</li>
<li><a href="../Page/康熙來了.md" title="wikilink">康熙來了</a></li>
<li>網羅新觀點</li>
<li>看點。觀點</li>
<li>華人世界讓你最美</li>
<li>坊間話題</li>
<li><a href="../Page/關鍵時刻.md" title="wikilink">關鍵時刻</a></li>
<li>至尊百家樂</li>
<li>壹周奇聞</li>
<li>名人博客</li>
<li>不能忘卻的偉大勝利</li>
<li>探真</li>
<li>午間一刻</li>
<li>午間報導</li>
<li>微博江湖</li>
<li>快樂玉米</li>
<li>美人聖經</li>
<li>財經觀察室</li>
<li>娛樂星聞</li>
<li>貨幣戰爭</li>
<li>亞洲劇場</li>
<li>澳亞IGF摔角</li>
<li>文創時空</li>
</ul></td>
</tr>
</tbody>
</table>

## 主播及主持人

|               **主播**                |
| :---------------------------------: |
|  [林毓芝](../Page/林毓芝.md "wikilink")   |
|  [李冀釗](../Page/李冀釗.md "wikilink")   |
| \-{[于越](../Page/于越.md "wikilink")}- |

|         **主持人**（**粗體**者為兼任）          |
| :----------------------------------: |
| **[林毓芝](../Page/林毓芝.md "wikilink")** |
|   [張瑞玲](../Page/張瑞玲.md "wikilink")   |
|                                      |

### 前主播與主持人

  - [璩美鳳](../Page/璩美鳳.md "wikilink")
  - 李虹依
  - 李應青
  - 吳屏珊
  - 卞其樂
  - 王怡翔
  - 王雅惠
  - 郭子玄
  - [吳采妮](../Page/吳采妮.md "wikilink")
  - 劉揚
  - 許復
  - 王松青
  - 姜玉鳳
  - 田森
  - 徐莉婷
  - [黃怡文](../Page/黃怡文.md "wikilink")
  - 牟丹
  - 林濤
  - 車玲玲
  - 莊舒棠
  - 王友信
  - 孫倩
  - 封禎
  - 曹世杰
  - 李雅貞
  - 蔣夏露
  - 謝安安
  - 洪媛
  - 周延
  - 于越
  - 曾懷瑩
  - 陳靜蘭
  - 黃佩珊
  - 蕭裔芬
  - 陳湘芸
  - 趙明頡
  - 李丹
  - 孟真
  - 趙珂雨
  - 馬永斐
  - 侯沛吟
  - 金姿妍
  - 凌希環
  - 曾竟凡
  - 崔格僖

## 卫星信号

### 使用衛星和頻段

澳亞衛視中文台節目通過澳門宇宙衛星發射基地向亞洲5號衛星發射C波段信號。

中国国际电视总公司境外卫星代理部接收澳亚卫视中文台信号，通过亚太6号卫星发射ku波段信号。

## 落地情況

### 中國大陸

澳亞衛視中文台於2002年通过了[國家廣電總局的批准](../Page/中華人民共和國國家廣播電影電視總局.md "wikilink")，與[BBC](../Page/BBC.md "wikilink")、[HBO](../Page/HBO.md "wikilink")、[CNN](../Page/CNN.md "wikilink")、[NHK](../Page/NHK.md "wikilink")、[鳳凰衛視等](../Page/鳳凰衛視.md "wikilink")28家國際媒體在中國大陸一同落地。并于2003年順利地通過年检。

除广东珠三角地区之外只有三星级以上的宾馆或酒店、科研机构、新闻、金融、经贸单位、国家标准二级以上的涉外宾馆、涉外公寓才能收看。

#### 广东

2006年8月3日，澳亚卫视在澳门举行了澳亚卫视在[广州及](../Page/广州.md "wikilink")[深圳有线电视网落地的签约仪式](../Page/深圳.md "wikilink")，标誌着澳亚卫视正式进入两地的有线电视网播出。\[1\]

[广东其他大城市的有线电视网从上述签约仪式后都开始转播澳亚卫视中文台](../Page/广东.md "wikilink")，但亦有城市不提供转播服务。

由于广电总局政策只针对有线电视，故在2016年7月15日，广东IPTV下架了凤凰卫视中文台、澳亚卫视等境外频道（由于澳亚卫视含有广州电视台节目宣传的插播，故为广州珠江数码信号盗播所为，之后被广州有线方告发后而实行），中国广电总局规定，未经允许禁止在IPTV平台播出境外频道。之后，广东IPTV上线互动点播专区“澳亚专区”。

## 参考资料

  - [澳门卫视](../Page/澳门卫视.md "wikilink")
  - [功夫卫视](../Page/功夫卫视.md "wikilink")

## 外部链接

  - [澳亚卫视官方網站](http://www.imastv.com)

  -
  -
  -
[Category:澳門電視台](../Category/澳門電視台.md "wikilink")
[Category:广东电视频道](../Category/广东电视频道.md "wikilink")

1.