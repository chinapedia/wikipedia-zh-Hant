**澳大利亚国家篮球联赛**（[英语](../Page/英语.md "wikilink")：**National Basketball
League**）是[澳大拉西亚最高级别的职业](../Page/澳大拉西亞.md "wikilink")[篮球赛事](../Page/篮球.md "wikilink")。

联赛始于1979年，赛事通常在夏季举行（4月 -
9月），这一模式沿袭了20年直到1998年。在1998/99赛季联赛在上个赛季结束一个月后的10月开始，赛制也变成了现在的冬季模式（10月
- 4月）。这一改变是为了试图避免和澳大利亚广受关注的澳式足球比赛时间的冲突。

联赛目前拥有8支参赛球队，基本包含了澳大利亚的首府城市（除[堪培拉](../Page/堪培拉.md "wikilink")、[霍巴特和](../Page/霍巴特.md "wikilink")[达尔文外](../Page/达尔文市.md "wikilink")）以及多个地区中心，如[凯恩斯](../Page/凯恩斯_\(昆士兰州\).md "wikilink")、[卧龙岗和一支来自](../Page/卧龙岗.md "wikilink")[新西兰](../Page/新西兰.md "wikilink")[奥克兰的球队](../Page/奧克蘭_\(紐西蘭\).md "wikilink")。[墨尔本地区的第二支俱乐部](../Page/墨尔本.md "wikilink")，[南方猛龙在](../Page/南方猛龙.md "wikilink")2006/07赛季加入了联赛。澳大利亚国家篮球联赛在2006/07赛季吸收了[新加坡腾飞之狮俱乐部的加盟](../Page/新加坡腾飞之狮.md "wikilink")，也成为第一个将球队扩充到亚洲地区的澳大利亚运动联盟。在2006年11月21日，联盟宣布设在[黄金海岸的新球队](../Page/黄金海岸_\(澳大利亚\).md "wikilink")[黄金海岸烈焰将在](../Page/黄金海岸烈焰.md "wikilink")2007/08赛季进入联赛成为联赛第13支球队[1](https://web.archive.org/web/20070915102616/http://www.nbl.com.au/default.aspx?s=newsdisplay&aid=4240)。

联盟发展最好的年景是在1980年代末至1990年代初，但是这些年联盟许多球队缩小球场规模去解决财政问题。一些俱乐部解散或者退出了联盟。面对这些问题，2004/05赛季开始阶段和[福克斯体育签订了一份新的电视转播合同](../Page/福克斯体育.md "wikilink")，并且和电子产品生产商[飞利浦集团签订了一份多年的赞助协议](../Page/飞利浦集团.md "wikilink")。联盟之前的最大赞助商是[三菱汽车](../Page/三菱汽车.md "wikilink")，由于赛制转至夏天，缺少资金，联盟也曾接近倒闭。

## 当前队伍

| 俱乐部                                          | 创建于         | 城市                                                                          | 主体育场                                             |
| -------------------------------------------- | ----------- | --------------------------------------------------------------------------- | ------------------------------------------------ |
| **[阿得雷德36人](../Page/阿得雷德36人.md "wikilink")** | 1983年       | [南澳大利亚州](../Page/南澳大利亚州.md "wikilink")[阿德莱德](../Page/阿德莱德.md "wikilink")    | [阿德莱德圆顶体育馆](../Page/阿德莱德圆顶体育馆.md "wikilink")     |
| **[布里斯班子弹](../Page/布里斯班子弹.md "wikilink")**   | 1979年       | [昆士兰州](../Page/昆士兰州.md "wikilink")[布里斯班](../Page/布里斯班.md "wikilink")        | [布里斯班会展中心](../Page/布里斯班会展中心.md "wikilink")       |
| **[凯恩斯大班蛇](../Page/凯恩斯大班蛇.md "wikilink")**   | 1999年/2000年 | [昆士兰州](../Page/昆士兰州.md "wikilink")[凯恩斯](../Page/凯恩斯_\(昆士兰州\).md "wikilink") | [凯恩斯会议中心](../Page/凯恩斯会议中心.md "wikilink")         |
| **[伊拉瓦拉老鹰](../Page/伊拉瓦拉老鹰.md "wikilink")**   | 1979年       | [新南威尔士州](../Page/新南威尔士州.md "wikilink")[卧龙岗](../Page/卧龙岗.md "wikilink")      | [WIN娱乐中心](../Page/WIN娱乐中心.md "wikilink")         |
| **[墨尔本联队](../Page/墨尔本联队.md "wikilink")**     | 1984年       | [维多利亚州](../Page/维多利亚州.md "wikilink")[墨尔本](../Page/墨尔本.md "wikilink")        | [联邦篮网球及曲棍球中心](../Page/联邦篮网球及曲棍球中心.md "wikilink") |
| **[新西兰打击者](../Page/新西兰打击者.md "wikilink")**   | 2003年       | [新西兰](../Page/新西兰.md "wikilink")[奥克兰](../Page/奧克蘭_\(紐西蘭\).md "wikilink")    | [北岸运动中心](../Page/北岸运动中心.md "wikilink")           |
| **[珀斯野猫](../Page/珀斯野猫.md "wikilink")**       | 1982年       | [西澳大利亚州](../Page/西澳大利亚州.md "wikilink")[珀斯](../Page/珀斯.md "wikilink")        | [挑战体育馆](../Page/挑战体育馆.md "wikilink")             |
| **[悉尼国王](../Page/悉尼国王.md "wikilink")**       | 1988年       | [新南威尔士州](../Page/新南威尔士州.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")        | [悉尼娱乐中心](../Page/悉尼娱乐中心.md "wikilink")           |

## 不存在的NBL球队

参见：[不存在的NBL球队](../Page/澳大利亚国家篮球联赛不存在的球队.md "wikilink")

一些球队进入联赛后解散，更名，与其他球队合并或迁徙出球队原先的地方。

| 俱乐部                                                         | 创建于         | 城市                                                                            | 主体育场                                           |
| ----------------------------------------------------------- | ----------- | ----------------------------------------------------------------------------- | ---------------------------------------------- |
| **[黄金海岸烈焰](../Page/黄金海岸烈焰.md "wikilink")**                  | 2007年       | [昆士兰州](../Page/昆士兰州.md "wikilink")[黄金海岸](../Page/黄金海岸_\(澳大利亚\).md "wikilink") | [黄金海岸会议娱乐中心](../Page/黄金海岸会议娱乐中心.md "wikilink") |
| **[新加坡腾飞之狮](../Page/新加坡腾飞之狮.md "wikilink")** <sup>(a)</sup> | 2006年/2007年 | [新加坡加冷](../Page/新加坡.md "wikilink")                                            | [新加坡室内体育馆](../Page/新加坡室内体育馆.md "wikilink")     |
| **[南方猛龙](../Page/南方猛龙.md "wikilink")** <sup>(b)</sup>       | 2006年/2007年 | [维多利亚州](../Page/维多利亚州.md "wikilink")[墨尔本](../Page/墨尔本.md "wikilink")          | [沃达丰球馆](../Page/沃达丰球馆.md "wikilink")           |
| **[悉尼圣灵](../Page/悉尼圣灵.md "wikilink")**                      | 1998年/1999年 | [新南威尔士州](../Page/新南威尔士州.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")          | [悉尼奥林匹克公园运动中心](../Page/悉尼奥林匹克公园.md "wikilink") |
| **[汤斯维尔鳄鱼](../Page/汤斯维尔鳄鱼.md "wikilink")**                  | 1993年       | [昆士兰州](../Page/昆士兰州.md "wikilink")[汤斯维尔](../Page/汤斯维尔.md "wikilink")          | [汤斯维尔娱乐中心](../Page/汤斯维尔娱乐中心.md "wikilink")     |

<sup>(a)</sup> *新加坡腾飞之狮取代了解散了的亨特海盗参加2006/07赛季。*

<sup>(b)</sup> *2006/07赛季南方猛龙成为第12支进入联赛的球队，官方在2005年12月15日宣布。*

## 澳大利亚国家篮球联赛名人

  - [雷·博内尔](../Page/雷·博内尔.md "wikilink")（Ray Borner）
  - [卡尔·布鲁顿](../Page/卡尔·布鲁顿.md "wikilink")（Cal Bruton）
  - [肖恩·赫尔](../Page/肖恩·赫尔.md "wikilink")（Shane Heal）
  - [史蒂夫·卡菲诺](../Page/史蒂夫·卡菲诺.md "wikilink")（Steve Carfino）
  - [詹姆斯·克劳福德](../Page/詹姆斯·克劳福德\(篮球运动员\).md "wikilink")（James Crawford）
  - [马克·戴维斯](../Page/马克·戴维斯\(澳大利亚篮球运动员\).md "wikilink")（Mark Davis）
  - [安德鲁·盖茨](../Page/安德鲁·盖茨.md "wikilink")（Andrew Gaze）
  - [林德塞·盖茨](../Page/林德塞·盖茨.md "wikilink")（Lindsay Gaze）
  - [布莱恩·戈尔让](../Page/布莱恩·戈尔让.md "wikilink")（Brian Goorjian）
  - [里基格·雷斯](../Page/里基格·雷斯.md "wikilink")（Ricky Grace）
  - [莱罗伊·罗根斯](../Page/莱罗伊·罗根斯.md "wikilink")（Leroy Loggins）
  - [拉里·森斯托克](../Page/拉里·森斯托克.md "wikilink")（Larry Sengstock）
  - [菲尔·史麦斯](../Page/菲尔·史麦斯.md "wikilink")（Phil Smyth）
  - [安德鲁·瓦霍夫](../Page/安德鲁·瓦霍夫.md "wikilink")（Andrew Vlahov）
  - [布莱特·马赫尔](../Page/布莱特·马赫尔.md "wikilink")（Brett Maher）

## 转播详细信息

目前澳大利亚国家篮球联赛第一手的转播是由付费电视频道[福克斯体育运作](../Page/福克斯体育.md "wikilink")，周三，周四，周五和周六转播比赛。每周集锦节目*NBL
Wrap*每周三晚间播送一次。

## 名人堂

澳大利亚国家篮球联赛在1998年联赛第20个年头的时候举行了庆祝活动，其中一部分的活动就是首创了NBL的篮球名人堂，用来纪念那些杰出的运动员，教练员，裁判员以及对联赛有着贡献的人。

另外适合进入名人堂的候选人必须符合下列的条件:

  - **运动员** 必须对澳大利亚国家篮球联赛有着杰出的贡献，至少退役4个赛季并且拥有100场以上的出场纪录。
  - **教练** 必须对澳大利亚国家篮球联赛有着杰出的贡献， 至少退役4个赛季并且拥有100场以上的执教纪录
  - **裁判** 必须对澳大利亚国家篮球联赛有着杰出的贡献并且退役至少4个赛季。
  - **贡献者** 必须对澳大利亚国家篮球联赛有着杰出的贡献，可能在任何时候被推选上。

进入名人堂成员，按英语字母顺序排列。

| 姓名                                                                | 奖项  | 进入时间 |
| ----------------------------------------------------------------- | --- | ---- |
| [Barry Barnes](../Page/Barry_Barnes_\(basketball\).md "wikilink") | 教练  | 1998 |
| [雷·博内尔](../Page/雷·博内尔.md "wikilink")（Ray Borner）                  | 运动员 | 2006 |
| [卡尔·布鲁顿](../Page/卡尔·布鲁顿.md "wikilink")（Cal Bruton）                | 运动员 | 1998 |
| [史蒂夫·卡菲诺](../Page/史蒂夫·卡菲诺.md "wikilink")（Steve Carfino）           | 运动员 | 2004 |
| [维尼·卡洛](../Page/维尼·卡洛.md "wikilink")（Wayne Carroll）               | 运动员 | 1999 |
| [依安·戴维斯](../Page/依安·戴维斯\(澳大利亚篮球运动员\).md "wikilink")（Ian David）    | 运动员 | 2001 |
| [马克·戴维斯](../Page/马克·戴维斯\(澳大利亚篮球运动员\).md "wikilink")（Mark Davis)   | 运动员 | 2006 |
| [阿尔·格林](../Page/阿尔·格林\(篮球运动员\).md "wikilink")（Al Green）           | 运动员 | 1999 |
| [迈克尔·约翰逊](../Page/迈克尔·约翰逊\(篮球运动员\).md "wikilink")                 | 运动员 | 2004 |
| [达米安·基奥](../Page/达米安·基奥.md "wikilink")（Damian Keogh）              | 运动员 | 2000 |
| [布赖恩·凯勒](../Page/布赖恩·凯勒.md "wikilink")（Brian Kerle）               | 教练  | 2006 |
| [莱罗伊·罗根斯](../Page/莱罗伊·罗根斯.md "wikilink")（Leroy Loggins）           | 运动员 | 2006 |
| [Herb McEachin](../Page/Herb_McEachin.md "wikilink")              | 运动员 | 1998 |
| [Danny Morseu](../Page/Danny_Morseu.md "wikilink")                | 运动员 | 2002 |
| [Bill Palmer](../Page/Bill_Palmer.md "wikilink")                  | 贡献者 | 1998 |
| [Darryl Pearce](../Page/Darryl_Pearce.md "wikilink")              | 运动员 | 2002 |
| [John Raschke](../Page/John_Raschke.md "wikilink")                | 贡献者 | 1998 |
| [拉里·森斯托克](../Page/拉里·森斯托克.md "wikilink")（Larry Sengstock）         | 运动员 | 2001 |
| [菲尔·史麦斯](../Page/菲尔·史麦斯.md "wikilink")（Phil Smyth）                | 运动员 | 2000 |
| [Mal Speed](../Page/Mal_Speed.md "wikilink")                      | 贡献者 | 2000 |
| [鲍伯·特纳](../Page/鲍伯·特纳.md "wikilink")（Bob Turner）                  | 教练  | 2000 |

## 历届冠军列表

<table>
<thead>
<tr class="header">
<th><p>球队</p></th>
<th><p>夺冠次数</p></th>
<th><p>夺冠赛季</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>珀斯野猫</strong></p></td>
<td><p>8</p></td>
<td><p>1990, 1991, 1995, 2000, 2010, 2014, 2016, 2017</p></td>
<td><p>从1987年起连续32个赛季打进NBL季后赛，在澳大利亚各大职业联赛排名第一，在世界各国篮球职业联赛排名第二，仅次于连续36个赛季打进<a href="../Page/以色列篮球超级联赛.md" title="wikilink">以色列篮球超级联赛季后赛的</a><a href="../Page/特拉维夫马卡比篮球俱乐部.md" title="wikilink">特拉维夫马卡比</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong>阿德莱德36人</strong></p></td>
<td><p>4</p></td>
<td><p>1986, 1998, 1999, 2002</p></td>
<td><p>1986年赢得了有史以来第一次系列赛赛制的NBL总决赛。</p></td>
</tr>
<tr class="odd">
<td><p>墨尔本老虎</p></td>
<td><p>4</p></td>
<td><p>1993, 1997, 2006, 2008</p></td>
<td><p>2014年被<a href="../Page/墨尔本联队.md" title="wikilink">墨尔本联队所取代</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong>新西兰打击者</strong></p></td>
<td><p>4</p></td>
<td><p>2011, 2012, 2013, 2015</p></td>
<td><p>2003年成为第一支来自<a href="../Page/新西兰.md" title="wikilink">新西兰的球队</a>，2011年成为第一支夺得NBL总冠军的新西兰球队。<br />
从2011年到2015年的5个赛季中获得了4个NBL总冠军。</p></td>
</tr>
<tr class="odd">
<td><p>堪培拉加农炮</p></td>
<td><p>3</p></td>
<td><p>1983, 1984, 1988</p></td>
<td><p>2003年改名为<a href="../Page/亨特海盗.md" title="wikilink">亨特海盗</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong>悉尼国王</strong></p></td>
<td><p>3</p></td>
<td><p>2003, 2004, 2005</p></td>
<td><p>1988年由<a href="../Page/悉尼超音速.md" title="wikilink">悉尼超音速和</a><a href="../Page/西悉尼西部之星.md" title="wikilink">西悉尼西部之星合并后成立</a>，2005年成为第一支完成3连冠壮举的NBL球队。</p></td>
</tr>
<tr class="odd">
<td><p><strong>布里斯班子弹</strong></p></td>
<td><p>3</p></td>
<td><p>1985, 1987, 2007</p></td>
<td><p>1985年赢得了最后一次一场定胜负的NBL总决赛。</p></td>
</tr>
<tr class="even">
<td><p>圣基尔达圣徒</p></td>
<td><p>2</p></td>
<td><p>1979, 1980</p></td>
<td><p>1987年改名为<a href="../Page/西墨尔本圣徒.md" title="wikilink">西墨尔本圣徒</a>，1991年又改名为<a href="../Page/南墨尔本圣徒.md" title="wikilink">南墨尔本圣徒</a>。</p></td>
</tr>
<tr class="odd">
<td><p>北墨尔本巨人</p></td>
<td><p>2</p></td>
<td><p>1989, 1994</p></td>
<td><p>1980年到1986年之间被称为<a href="../Page/科堡巨人.md" title="wikilink">科堡巨人</a>，1998年与<a href="../Page/东南墨尔本魔术.md" title="wikilink">东南墨尔本魔术合并后成立</a><a href="../Page/维多利亚泰坦.md" title="wikilink">维多利亚泰坦</a>。</p></td>
</tr>
<tr class="even">
<td><p>东南墨尔本魔术</p></td>
<td><p>2</p></td>
<td><p>1992, 1996</p></td>
<td><p>1992年由<a href="../Page/南墨尔本圣徒.md" title="wikilink">南墨尔本圣徒和</a><a href="../Page/纽纳瓦丁幽灵.md" title="wikilink">纽纳瓦丁幽灵合并后成立</a>，1998年与<a href="../Page/北墨尔本巨人.md" title="wikilink">北墨尔本巨人合并后成立</a><a href="../Page/维多利亚泰坦.md" title="wikilink">维多利亚泰坦</a>。</p></td>
</tr>
<tr class="odd">
<td><p>朗塞斯顿赌城老虎</p></td>
<td><p>1</p></td>
<td><p>1981</p></td>
<td><p>球队于1983年解散。</p></td>
</tr>
<tr class="even">
<td><p>西阿德莱德小熊猫</p></td>
<td><p>1</p></td>
<td><p>1982</p></td>
<td><p>球队于1984年离开NBL联赛。</p></td>
</tr>
<tr class="odd">
<td><p><strong>伊拉瓦拉老鹰</strong></p></td>
<td><p>1</p></td>
<td><p>2001</p></td>
<td><p>1998年到2015年之间被称为<a href="../Page/卧龙岗老鹰.md" title="wikilink">卧龙岗老鹰</a>。</p></td>
</tr>
<tr class="even">
<td><p>南方猛龙</p></td>
<td><p>1</p></td>
<td><p>2009</p></td>
<td><p>球队于2009年离开NBL联赛。</p></td>
</tr>
<tr class="odd">
<td><p><strong>墨尔本联队</strong></p></td>
<td><p>1</p></td>
<td><p>2018</p></td>
<td><p>球队于2014年以<a href="../Page/墨尔本老虎.md" title="wikilink">墨尔本老虎为基础成立</a>，并宣称自己为唯一一支代表整个墨尔本的球队。</p></td>
</tr>
</tbody>
</table>

球队**深色**表示当前仍然存在于NBL联赛之中。

## 奖项获得者

  - [25周年最佳阵容](../Page/澳大利亚国家篮球联赛25周年最佳阵容.md "wikilink")（2003）
  - [20周年最佳阵容](../Page/澳大利亚国家篮球联赛20周年最佳阵容.md "wikilink")（1998）
  - [最有价值球员](../Page/澳大利亚国家篮球联赛最有价值球员.md "wikilink")
  - [总决赛最有价值球员](../Page/澳大利亚国家篮球联赛总决赛最有价值球员.md "wikilink")
  - [年度最佳教练](../Page/澳大利亚国家篮球联赛最佳教练.md "wikilink")
  - [年度最佳新秀](../Page/澳大利亚国家篮球联赛最佳新秀.md "wikilink")
  - [最佳进步球员](../Page/澳大利亚国家篮球联赛最佳进步球员.md "wikilink")
  - [最佳防守球员](../Page/澳大利亚国家篮球联赛最佳防守球员.md "wikilink")
  - [最佳第六人](../Page/澳大利亚国家篮球联赛最佳第六人.md "wikilink")
  - [金手奖](../Page/澳大利亚国家篮球联赛金手奖.md "wikilink")（奖项已取消）
  - [最有效率球员](../Page/澳大利亚国家篮球联赛最有效率球员.md "wikilink")（奖项已取消）
  - [全明星阵容](../Page/澳大利亚国家篮球联赛全明星阵容.md "wikilink")

## 全明星赛

澳大利亚国家篮球联赛全明星赛每年举办一次。以前是以南部全明星对垒北部全明星或是东部全明星对垒西部全明星的比赛形式，2004/05起赛季被澳大利亚本土全明星对垒联盟外籍球员全明星的形式所取代，并且沿用至今。

## 外部链接

  - [澳大利亚国家篮球联赛官方网站](http://www.nbl.com.au/)
  - [澳大利亚篮球协会官方网站](http://www.basketball.net.au/)
  - [新西兰篮球协会官方网站](http://www.basketball.org.nz/)
  - [OZ Hoops](http://www.ozhoops.com.au/)
  - [福克斯体育篮球版块](http://www.foxsports.com.au/basketball)

[Category:澳洲籃球](../Category/澳洲籃球.md "wikilink")
[Category:篮球联赛](../Category/篮球联赛.md "wikilink")