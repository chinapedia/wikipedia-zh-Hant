**澳洲國立大學**（，缩写作
****；常簡稱**澳国立**，又譯作**澳洲國家大學**），於1946年創立，是一所位於[澳洲首都特區](../Page/澳洲首都特區.md "wikilink")[坎培拉的研究型](../Page/坎培拉.md "wikilink")[國立大學](../Page/國立大學.md "wikilink")。它同時也是[國際研究型大學聯盟](../Page/國際研究型大學聯盟.md "wikilink")（IARU）、[澳洲八大名校](../Page/澳洲八大名校.md "wikilink")（Go8）、[大學天文研究協會](../Page/大學天文研究協會.md "wikilink")（AURA）以及[環太平洋大學聯盟](../Page/環太平洋大學聯盟.md "wikilink")（APRU）的盟校之一。是澳大利亞由[澳洲國會立法創建的](../Page/澳洲國會.md "wikilink")[大學](../Page/大學.md "wikilink")，也是澳洲唯一國立的公立大學（其它澳洲公立大學均為州立）。

澳洲國家級學術研究單位有四座位於ANU校園內：[澳大利亞科學院](../Page/澳大利亞科學院.md "wikilink")（AAS）、[澳洲人文科學院](../Page/澳洲人文科學院.md "wikilink")（AAH）、[澳洲社會科學院](../Page/澳洲社會科學院.md "wikilink")（ASSA）和澳洲法律科學院（AAL）。校園四周還坐落著[澳大利亞國立博物館](../Page/澳大利亞國立博物館.md "wikilink")、澳洲最大的國家級科研機構[CSIRO](../Page/CSIRO.md "wikilink")。校內學者獲選為[澳洲聯邦](../Page/澳洲聯邦.md "wikilink")[院士人數更超過](../Page/院士.md "wikilink")270名\[1\]，居所有澳洲大學之冠；而[英國](../Page/英國.md "wikilink")[皇家學會的成員人數亦是澳洲第一](../Page/皇家學會.md "wikilink")。傑出校友包括了六名[諾貝爾獎得主](../Page/諾貝爾獎.md "wikilink")、四十九名[羅德獎學金得主](../Page/羅德獎學金.md "wikilink")\[2\]、兩名[澳洲總理](../Page/澳洲總理.md "wikilink")、十二名現任[聯邦部長與三十名現任](../Page/部長.md "wikilink")[大使等](../Page/大使.md "wikilink")。\[3\]

澳洲國立大學總體學術排名位列[大洋洲第一](../Page/大洋洲.md "wikilink")，在2018年的[QS世界大學排名為全球前](../Page/QS世界大學排名.md "wikilink")20名，澳洲第1\[4\]，[泰晤士高等教育排名為全球第](../Page/泰晤士高等教育排名.md "wikilink")47名（澳洲第2）\[5\]，並被評選為全球前25大最國際化的頂尖學府的第7名（澳洲第1）\[6\]。而在世界大學畢業生就業力排行榜中，ANU名列世界第22（澳洲第1）\[7\]。

## 校史

### 20世紀

[Homopolar_anu-MJC.jpg](https://zh.wikipedia.org/wiki/File:Homopolar_anu-MJC.jpg "fig:Homopolar_anu-MJC.jpg")
澳洲國立大學的創立是依據[澳洲國會通過的專案法](../Page/澳洲國會.md "wikilink")。\[8\]該法案由當時的[澳洲總理](../Page/澳洲總理.md "wikilink")[班·奇夫利以及](../Page/班·奇夫利.md "wikilink")[二次世界大戰戰後重建委員會主任](../Page/二次世界大戰.md "wikilink")所共同提出，並得到了反對黨領袖[羅伯特·孟席斯的全力支持](../Page/羅伯特·孟席斯.md "wikilink")，順利於1946年8月1日獲得通過。創校宗旨，在於建立一所頂尖的研究型國立大學，以提升澳洲的整體學術研究實力。\[9\]ANU也成了[澳洲唯一由](../Page/澳洲.md "wikilink")[聯邦](../Page/聯邦.md "wikilink")[國會立法設立的](../Page/國會.md "wikilink")[國立大學](../Page/國立大學.md "wikilink")，不同於其它澳洲[公立大學皆是由各地](../Page/公立大學.md "wikilink")[州議會立法設立](../Page/州.md "wikilink")。

ANU成立後，大批傑出的澳洲學者們開始陸續加入ANU的創建行列，其中包括了在[雷達開發與](../Page/雷達.md "wikilink")[核物理領域中赫赫有名的](../Page/核.md "wikilink")爵士、[青黴素的發現者](../Page/青黴素.md "wikilink")[霍華德·弗洛里，弗洛里男爵](../Page/霍華德·弗洛里，弗洛里男爵.md "wikilink")、[歷史學界的大師](../Page/歷史學.md "wikilink")爵士和著名的[經濟學者](../Page/經濟學.md "wikilink")爵士等等。\[10\]

創校初期，ANU致力於尖端科技的研發，並且只招收[研究生](../Page/研究生.md "wikilink")。直到1960年將當時隸屬於[墨爾本大學的](../Page/墨爾本大學.md "wikilink")併入，才開始招收大學部學生。1979年創立[ANU
Enterprise](http://www.anuenterprise.com.au/)，負責事業單位的營運，例如：科研專案承接、精密科學儀器生產等等。

### 21世紀

[Peter_Baume_Building_Oct_2012.JPG](https://zh.wikipedia.org/wiki/File:Peter_Baume_Building_Oct_2012.JPG "fig:Peter_Baume_Building_Oct_2012.JPG")
在[澳洲首都特區](../Page/澳洲首都特區.md "wikilink")[坎培拉的](../Page/坎培拉.md "wikilink")[都市計畫藍圖中](../Page/都市計畫.md "wikilink")，ANU主校區不僅僅是一般大學校區，同時也應規劃設立ANU[醫學院和ANU附設](../Page/醫學院.md "wikilink")[醫院](../Page/醫院.md "wikilink")\[11\]。2001年4月，經過長達八個月的公共辯論和政府會議討論，澳洲政府宣布挹注ANU成立澳洲第12所（世界第896所）醫學院。\[12\]\[13\]經過幾年發展，ANU醫學院現已在澳洲的醫學院中名列前茅。

#### 2020年發展策略

在2011年9月，ANU[校長宣布為期十年的新校務發展計畫](../Page/校長.md "wikilink")。\[14\]依據此發展計畫，未來十年ANU將致力於提升學術研究（Research）與教學（Education）品質，並積極強化其在[澳洲與](../Page/澳洲.md "wikilink")[亞太地區公共政策智庫](../Page/亞太地區.md "wikilink")（Public
Policy
Resource）的角色。進而於2020年前將ANU定位並塑造成不僅是澳洲國家的（National）大學，同時也是最好的（Finest）大學。\[15\]經過長達七個月的規劃與討論，細部計畫與短、中、長期執行策略皆於2011年9月22日公佈於ANU官方網站。

## 校徽、校訓與權杖

ANU校徽以[藍色盾為底](../Page/藍色.md "wikilink")；[紋章象徵](../Page/紋章.md "wikilink")[南十字星座光輝下的](../Page/南十字星座.md "wikilink")[澳大利亞聯邦](../Page/澳大利亞聯邦.md "wikilink")；中間的[迴力鏢](../Page/迴力鏢.md "wikilink")（Boomerangs）圖騰，代表著國家權力的核心：[澳洲國會大廈](../Page/澳洲國會大廈.md "wikilink")；水波紋則帶有[太平洋與](../Page/太平洋.md "wikilink")[大洋洲的含意](../Page/大洋洲.md "wikilink")。校徽下的[拉丁文格言則是校訓](../Page/拉丁文.md "wikilink")：*Naturam
Primum Cognoscere Rerum*（拉丁文，意思為：First to learn the nature of
things；中文譯：從認清事物的本質開始）。此校訓是源自於[羅馬共和國](../Page/羅馬共和國.md "wikilink")[哲學家暨](../Page/哲學家.md "wikilink")[詩人](../Page/詩人.md "wikilink")[盧克萊修的詩句](../Page/盧克萊修.md "wikilink")。

  - 2011年初開始，ANU推行採用新的[白底](../Page/白.md "wikilink")[標識](../Page/標識.md "wikilink")（Logo），逐步取代使用多年的舊款方格狀大字標識。新標識的設計概念為簡化風格，強化標示的魄力和清晰度，並改變字體與用色，主要目的在於讓它於運用上能夠有更高的靈活性。\[16\]

### 權杖

澳洲國立大學校內有一柄傳承自英語世界最古老[大學的銀白色](../Page/大學.md "wikilink")[權杖](../Page/權杖.md "wikilink")，象徵其歷任[校長與](../Page/校長.md "wikilink")[校監之學術權威](../Page/校監.md "wikilink")。此權杖是在1950年由[英國](../Page/英國.md "wikilink")[牛津大學獻贈給ANU](../Page/牛津大學.md "wikilink")，與該校的牛津權杖（*Oxford
mace*）完全相同，是由[十八世紀金匠Benjamin](../Page/十八世紀.md "wikilink")
Pyne以[黃金打造而成](../Page/黃金.md "wikilink")。其所散發出的潔白光輝，乃是由於外層鍍上的一層[白銀](../Page/白銀.md "wikilink")\[17\]。

在[畢業典禮等重要儀式上](../Page/畢業.md "wikilink")，可見到這柄權杖。由執杖吏（）負責持杖，引導學校行政高層與校務委員會的成員進場和離席。在學位授予的儀式過程中，此杖會被安置在[校長座前鑲有ANU校徽的杖架上](../Page/校長.md "wikilink")。權杖頂端，則必須對向台前的學位頒布者：[校監](../Page/校監.md "wikilink")。

## 校園治理

根據1991年由[澳洲聯邦政府所修訂的](../Page/澳洲聯邦.md "wikilink")《澳洲國立大學法案》，ANU採用[委員會制的方式治理校園](../Page/委員會制.md "wikilink")。校務委員會由十五名委員組成，負責任命[校長和](../Page/校長.md "wikilink")[院長](../Page/院長.md "wikilink")、審核治校方針和年度預算、樹立營運政策和程序，以及監督辦學成果、產學合作和風險控管等。校務委員會的[主席由ANU](../Page/主席.md "wikilink")[校監](../Page/校監.md "wikilink")（Chancellor）擔任；副主席由副校監（Pro-Chancellor）擔任。[校長](../Page/校長.md "wikilink")（Vice-Chancellor
& President）則是學校的行政首長，在校務委員會的授權與監督下，負責學校的實際營運和行政團隊管理。

### 校監

澳洲國立大學歷任[校監](../Page/校監.md "wikilink")（Chancellor）皆由德高望重的政要名流或學者出任。過去也曾由[諾貝爾獎得主和卸任](../Page/諾貝爾獎.md "wikilink")[澳洲總理擔任](../Page/澳洲總理.md "wikilink")。除了主持校務委員會外，校監亦在畢業典禮上負責主持[學位頒授](../Page/學位.md "wikilink")[儀式](../Page/儀式.md "wikilink")。

現任校監為（2010-），曾任[澳洲工黨](../Page/澳洲工黨.md "wikilink")[黨魁](../Page/黨魁.md "wikilink")；澳洲聯邦[參議院議長](../Page/參議院.md "wikilink")、[司法部部長](../Page/司法部.md "wikilink")、[外交部部長等](../Page/外交部.md "wikilink")；[聯合國秘書長顧問](../Page/聯合國.md "wikilink")；[國際危機組織](../Page/國際危機組織.md "wikilink")[執行長](../Page/執行長.md "wikilink")；[亞洲太平洋經濟合作會議](../Page/亞洲太平洋經濟合作會議.md "wikilink")（APEC）發起人與第一屆APEC部長級會議主席；[東南亞國協](../Page/東南亞國協.md "wikilink")（ASEAN）區域論壇發起人；[御用大律師](../Page/御用大律師.md "wikilink")；曾獲澳洲聯邦政府[勳章和](../Page/勳章.md "wikilink")[聯合國](../Page/聯合國.md "wikilink")[獎章等](../Page/獎章.md "wikilink")；[英國](../Page/英國.md "wikilink")[牛津大學榮譽院士](../Page/牛津大學.md "wikilink")、[劍橋大學期刊編輯顧問](../Page/劍橋大學.md "wikilink")、[美國](../Page/美國.md "wikilink")[耶魯大學與](../Page/耶魯大學.md "wikilink")[史丹佛大學顧問等等](../Page/史丹佛大學.md "wikilink")。

### 校務基金

在2011年，ANU獲得約1.2371億[澳幣的募款資金](../Page/澳幣.md "wikilink")，超越[墨爾本大學和](../Page/墨爾本大學.md "wikilink")[雪梨大學成為人均獲款第一的大學](../Page/雪梨大學.md "wikilink")。\[18\]

## 學院架構

澳洲國立大學現階段包含了七大學院（Colleges）。學院底下設有研究院（Research
Schools），研究院除了進行學術研究外，也肩負研究生和大學生的教學。學院另下轄學群／教學學院（Schools）則以提供教學為主。有些學院還設有研究所／學會（Institute）和智庫中心（University
Centres）擔任[聯邦政府](../Page/聯邦政府.md "wikilink")[智庫角色](../Page/智庫.md "wikilink")，僅專注於學術研究。

ANU高階研究中心與學院附設智庫中心等校內研究組織，會依照[聯邦政府需求或研究需要而進行不定期的整併或新設](../Page/聯邦政府.md "wikilink")。最新的ANU學術架構請參閱澳洲國立大學官方網站：[ANU詳細學院學術架構](http://unistats.anu.edu.au/resources/structure/college_aos.php)或[ANU學院架構](http://info.anu.edu.au/ovc/Executive/040PP_University_Structure/academic_structure)。

### ANU人文與社會科學學院

  - ANU人文與社會科學學院（[ANU College of Arts and Social
    Sciences](http://cass.anu.edu.au/)）
      - 澳洲人口與社會研究所（Australian Demographic and Social Research Institute）
          - 澳洲社會科學資料檔案庫（Australian Social Science Data Archive）
      - 人文與藝術研究院（Research School of Humanities and the Arts）
          - 音樂學院（School of Music）
          - 藝術學院（School of Art）
          - 考古與人類學學群（School of Archaeology and Anthropology）
          - 文化研究學群（School of Cultural Inquiry）
          - 語言學學群（School of Language Studies）
              - 數位人文科學樞紐（Digital Humanities Hub）
              - Freilich基金會（Freilich Foundation）
              - 人文科學研究中心（Humanities Research Centre）
              - 文化遺產與藝術專業實踐研究所（Institute for Professional Practice in
                Heritage and the Arts）
      - 社會科學研究院（Research School of Social Sciences）
          - 歷史學群（School of History）
          - 哲學學群（School of Philosophy）
          - 政治與國際關係學群（School of Politics and International Relations）
          - 社會學學群（School of Sociology）
              - 澳洲國家拉丁美洲研究中心（Australian National Centre for Latin
                American Studies）
              - 澳洲國家實習學程（Australian National Internships Program）
              - 原住民經濟政策研究中心（Centre for Aboriginal Economic Policy
                Research）
              - 阿拉伯與伊斯蘭研究中心（Centre for Arab and Islamic Studies）
              - 教育發展與學術方法中心（Centre for Educational Development and
                Academic Methods）
              - 博弈理論研究中心（Centre for Gambling Research）
              - 政策創新中心（Centre for Policy Innovation）
              - 國家傳記文學中心（National Centre of Biography）
              - 澳洲鄉村與區域國家研究所（National Institute for Rural and Regional
                Australia）
      - 歐洲研究中心（Centre for European Studies）
      - 澳洲國家辭典中心（Australian National Dictionary Centre）

#### 歐洲研究中心

[澳洲國立大學歐洲研究中心](http://ces.anu.edu.au/)（ANU Centre for European
Studies；ANUCES），隸屬ANU人文與社會科學學院。由[歐盟委員會](../Page/歐盟委員會.md "wikilink")（European
Commission）和ANU共同成立。2011年9月[歐盟委員會](../Page/歐盟委員會.md "wikilink")[主席](../Page/主席.md "wikilink")[若澤·曼努埃爾·巴羅佐](../Page/若澤·曼努埃爾·巴羅佐.md "wikilink")（José
Manuel Durão Barroso）到澳洲訪問期間，特地安排前往ANUCES訪問並舉行公開演說。

#### 澳洲國家辭典中心

澳洲國家辭典中心（Australian National Dictionary
Centre；ANDC），是由[澳洲國立大學與](../Page/澳洲國立大學.md "wikilink")[牛津大學於](../Page/牛津大學.md "wikilink")1988年共同成立，合作研究[澳洲英語](../Page/澳洲英語.md "wikilink")，成果委由[牛津大學出版社出版澳洲英語字典](../Page/牛津大學出版社.md "wikilink")。

  - ANU是[亞太地區的](../Page/亞太地區.md "wikilink")[語言學研究與教學之重鎮](../Page/語言學.md "wikilink")，2012年在世界大學語言學領域排行第9名（參考條目：[語言學領域大學排行列表](../Page/語言學領域大學排行列表.md "wikilink")），包括[標準漢語](../Page/標準漢語.md "wikilink")、[廣東話](../Page/廣東話.md "wikilink")、[蒙古語](../Page/蒙古語.md "wikilink")、[臺灣南島語](../Page/臺灣南島語.md "wikilink")、[日語](../Page/日語.md "wikilink")、[韓語](../Page/韓語.md "wikilink")、[泰語](../Page/泰語.md "wikilink")、[馬來語等等](../Page/馬來語.md "wikilink")；ANU也是[自然語義理論學派的大本營](../Page/自然後設語義.md "wikilink")。

### ANU亞太學院

  - ANU亞洲與太平洋學院（[ANU College of Asia and the
    Pacific](http://asiapacific.anu.edu.au/)）
      - 文化、歷史與語言學群（School of Culture, History and Language）
      - 國際、政治與戰略研究學群（School of International, Political and Strategic
        Studies）
      - 法規、司法和外交學群（School of Regulation, Justice and Diplomacy）
  - 克勞福德公共政策學院（[Crawford School of Public
    Policy](http://www.crawford.anu.edu.au/)）
      - 公共政策學會（Institute of Public Policy）
      - 澳洲與紐西蘭政府學院（[Australia and New Zealand School of
        Government；ANZSOG](http://www.anzsog.edu.au/)）
      - 澳洲中華全球研究中心（[Australian Centre on China in the
        World](http://ciw.anu.edu.au/)）
      - 澳洲國家公共政策研究所（Australian National Institute of Public
        Policy；ANIPP）
          - 聯邦政府智庫：慧聰庫姆斯政策論壇（HC Coombs Policy Forum）
          - 羅蘭‧威爾遜爵士基金會（Sir Roland Wilson Foundation）

<!-- end list -->

  - 國家安全學院（[National Security College](http://nsc.anu.edu.au/)）

#### 克勞福德公共政策學院

2010年5月，[澳洲總理](../Page/澳洲總理.md "wikilink")[陸克文宣佈](../Page/陸克文.md "wikilink")[澳洲聯邦政府投入](../Page/澳洲聯邦.md "wikilink")1.11億[澳幣](../Page/澳幣.md "wikilink")，在ANU內成立澳洲國家公共政策研究所（Australian
National Institute for Public
Policy；ANIPP）\[19\]；此學院匯集了澳洲最頂尖的公共政策（Public
Policy）學者。ANU在2012年4月宣布改組成立克勞福德公共政策學院（Crawford School of Public
Policy）\[20\]，以[美國](../Page/美國.md "wikilink")[哈佛大學的](../Page/哈佛大學.md "wikilink")[甘迺迪政府學院為藍本](../Page/約翰·F·甘迺迪政府學院.md "wikilink")\[21\]，整合原先的克勞福德經濟與政府學院和澳洲國家公共政策研究所（ANIPP）。此國家文官學院肩負了澳洲高階公務員培訓、進修和聯邦智庫等任務。

##### 澳洲國立大學公共政策院士

澳洲國立大學公共政策院士（ANU Public Policy
Fellows），是由ANU克勞福德公共政策學院附屬之公共政策學會（Institute
of Public
Policy）進行審核並授予任何在公共政策相關領域有傑出貢獻或影響力之學者此榮銜，院士來自ANU各學院，橫跨各種學術領域。除了學者外，傑出的澳洲公職人員亦有機會成為院士。2012年中旬，公佈了首屆院士名單。公共政策院士們會有定期的聚會交流與公眾論壇活動。

##### 亞太政策學會

亞洲與太平洋政策學會（Asia and the Pacific Policy
Society），成立於2012年七月份，是全球第一個以亞太地區為主體、跨國界與跨學科領域的公共政策學會。會長由ANU克勞福德公共政策學院院長兼任，旨在建立一個讓國際學者與政策制定者們可以共同參與的學會，並成為重要的溝通和交流管道，以進一步促進[亞太地區之整體公共政策研究領域的發展](../Page/亞太地區.md "wikilink")。此學會同時規劃成為重要的國際發表平台，將編審學術期刊《亞太政策研究（Asia
and the Pacific Policy
Studies）》，預計2014年上半年創刊；建構亞太地區的公共政策學術主流，並與最新的國際思潮接軌。學會亦將主辦亞太政策學會年會，首屆年會訂於2012年九月初於[澳洲首都特區首府](../Page/澳洲首都特區.md "wikilink")[坎培拉舉行](../Page/坎培拉.md "wikilink")。

##### 中華全球研究中心

[中華全球研究中心](http://ciw.anu.edu.au/)（Australian Centre on China in the
World；CIW），是當代研究[中國與全球戰略的重鎮](../Page/中國.md "wikilink")，隸屬ANU亞太學院。2010年4月由當時的[澳洲總理](../Page/澳洲總理.md "wikilink")[陸克文宣布](../Page/陸克文.md "wikilink")[聯邦政府出資](../Page/聯邦.md "wikilink")5,300萬元[澳幣成立](../Page/澳幣.md "wikilink")，並在ANU校內建造專屬研究大樓；同年6月，時任[中華人民共和國副主席](../Page/中華人民共和國副主席.md "wikilink")、[中共中央政治局常委](../Page/中共中央政治局.md "wikilink")、[中央書記處書記和](../Page/中央書記處.md "wikilink")[中共中央黨校校長的](../Page/中共中央黨校.md "wikilink")[習近平安排前往CIW訪問並舉辦贈書儀式](../Page/習近平.md "wikilink")，以表示支持和重視。共贈送881種、1723冊相關書籍，外加60多萬種中華研究相關電子書。

### ANU商學與經濟學院

  - ANU商學與經濟學院（[ANU College of Business and
    Economics](http://cbe.anu.edu.au/)）
      - 管理學研究院（Research School of Management；RSM）
      - 會計與電子商務研究院（Research School of Accounting and Business
        Information Systems；RSABIS）
          - 國家資訊系統研究中心（National Centre for Information Systems
            Research；NCISR）
          - 澳洲國家審計與鑑證研究中心（Australian National Centre for Audit and
            Assurance Research；ANCAAR）
      - 經濟學研究院（Research School of Economics；RSE）
          - 經濟史中心（Centre for Economic History；CEH）
          - 應用宏觀經濟分析中心（Centre for Applied Macroeconomic Analysis；CAMA）
      - 金融、精算與應用統計學研究院（Research School of Finance, Actuarial Studies and
        Applied Statistics；RSFAS）

#### 商學院簡史

國立管理學研究生院（National Graduate School of
Management；NGSM），曾經是ANU最主要的[商學院](../Page/商學院.md "wikilink")（Business
School），1994年創立後一直以[企管碩士](../Page/企管碩士.md "wikilink")（MBA）聞名於海內外。2006年ANU進行全校學院架構改組，NGSM與原本的經濟學與商務學院（Faculty
of Economics and Commerce）一起併入新成立的ANU商學與經濟學院（ANU College of Business and
Economics；CBE）。2011年底，CBE提案經校務委員會審核同意，進行組織再造，將其下轄的四個學群（Schools），全部升格並改名為研究院（Research
Schools）。

##### 教學與學術評價

  - 2000年，在《亞洲週刊》中，商學院獲得[亞太地區排名前](../Page/亞太地區.md "wikilink")10大。
  - 2000年，在《澳洲優秀大學指南》中，商學院獲得最高等級之五星級評價。往後歷年評鑑，ANU大多五星。
  - 2003年，國際[經濟學百大榜](../Page/經濟學.md "wikilink")，澳洲第1名。共兩所澳洲大學入榜。
  - 2004年，全球[統計學排名第](../Page/統計學.md "wikilink")33名，澳洲第1名。[經濟學全球第](../Page/經濟學.md "wikilink")56名，澳洲第2名。
  - 2005年，亞太地區[會計學排名](../Page/會計學.md "wikilink")，前10名。
  - 2006年，墨爾本大學高等教育品質排名，ANU商學院排行全澳洲前3名。
  - 2007年，獲得澳洲聯邦政府教學卓越（*Excellence of Teaching*）最高等級之A1排名。

##### 清華大學－澳洲國立大學管理碩士學位教育項目

自2004年開始招生，由[清華大學與澳洲國立大學](../Page/清華大學.md "wikilink")（ANU）兩大世界名校聯手，經中國大陸[教育部與](../Page/教育部.md "wikilink")[國務院學位委員會批准](../Page/國務院.md "wikilink")，始合辦[管理碩士課程](../Page/管理碩士.md "wikilink")（Master
of Management
Program），為[中國各行各業的中壯年](../Page/中國.md "wikilink")[高階管理人員提供國際化的](../Page/高階管理人員.md "wikilink")[在職專班進修機會](../Page/在職專班.md "wikilink")。授課地點在[北京](../Page/北京.md "wikilink")，由清大負責基礎課程教學；ANU則支援核心課程，定期派遣[商學院的講師和學者至中國進行短期教學](../Page/商學院.md "wikilink")。

學員依規定完成課程與學位論文、論文答辯考核後，可獲ANU「管理碩士學位證書」，主修為：科技與創新（*Technology and
Innovation*）。此主修是ANU為這項中澳合作課程專設與專用。

#### 精算學

澳洲國立大學（ANU）是澳洲四所能提供完整[精算專業課程的其中一所](../Page/精算.md "wikilink")，也是唯一被澳洲[精算師協會](../Page/精算師.md "wikilink")（[Institute
of Actuaries of
Australia](http://www.actuaries.asn.au)）認定有資格授予精算學學士（Bachelor
of Actuarial Studies）和精算學碩士（Master of Actuarial
Studies）[學位的大學](../Page/學位.md "wikilink")。

### ANU工程與電腦科學學院

  - ANU工程與電腦科學學院（[ANU College of Engineering and Computer
    Science](http://cecs.anu.edu.au/)）
      - 電腦科學研究院（Research School of Computer Science）
      - 工程研究院（Research School of Engineering）

#### 國家計算機基地

國家計算機基地（[National Computational
Infrastructure](http://nci.org.au/)；NCI），由[澳洲聯邦](../Page/澳洲聯邦.md "wikilink")[政府](../Page/政府.md "wikilink")、[聯邦科學與工業研究組織](../Page/聯邦科學與工業研究組織.md "wikilink")（CSIRO）和[澳洲國立大學](../Page/澳洲國立大學.md "wikilink")（ANU）共同投資。

  - 超級電腦Raijin：2013年啟用，現役[南半球和](../Page/南半球.md "wikilink")[大洋洲最先進的超級電腦](../Page/大洋洲.md "wikilink")，命名為[日文中的](../Page/日文.md "wikilink")[雷神](../Page/雷神.md "wikilink")（Raijin），由ANU負責運作管理。
  - 超級電腦NCI：運算能力曾名列全球前500大[超級電腦中排行第](../Page/超級電腦.md "wikilink")24名，由ANU負責運作管理。
  - [超級電腦Vayu](../Page/超級電腦.md "wikilink")\[22\]：前[大洋洲最先進的超級電腦](../Page/大洋洲.md "wikilink")，由ANU負責運作管理。

### ANU法學院

  - ANU法學院（[ANU College of Law](http://law.anu.edu.au/)）
      - 法律學院（Faculty of Law）
      - 澳洲環境法中心（Australian Centre for Environmental Law）
      - 澳洲農業智慧財產中心（Australian Centre for Intellectual Property in
        Agriculture）
      - 商業法中心（Centre for Commercial Law）
      - 國際公法中心（Centre for International and Public Law）
      - 法學與經濟學中心（Centre for Law and Economics）
      - 氣候法與政策中心（Centre for Climate Law and Policy）
      - 澳洲軍法與司法中心（Australian Centre for Military Law and Justice）
      - 澳洲日本法網絡（Australian Network for Japanese Law）
      - 澳洲勞動法協會（Australian Labour Law Association）
  - 國家原住民研究中心（[National Centre for Indigenous
    Studies](https://web.archive.org/web/20111126072834/http://law.anu.edu.au/ncis/)）

### ANU醫學、生物暨環境學院

  - ANU醫學、生物暨環境學院（[ANU College of Medicine, Biology and
    Environment](http://cmbe.anu.edu.au/)）
      - 澳洲國立大學醫學院（ANU Medical School）
      - 約翰科廷醫學研究院（John Curtin School of Medical Research）
      - 健康與心理學學群（School of Health and Psychological Sciences）
          - 心理學系（Department of Psychology）
      - 生物學研究院（Research School of Biology）
      - 芬納環境與社會學院（Fenner School of Environment and Society）
          - 生物科學研究基金（BioScience Research Facility）
  - 澳洲衛生經濟研究中心（Australian Centre for Economic Research on Health）
  - 澳洲基層衛生保健研究所（Australian Primary Health Care Research Institute）
  - 心理健康研究中心（Centre for Mental Health Research）
  - 孟席斯衛生政策中心（Menzies Centre for Health Policy）
  - 國家生物安全中心（National Centre for Biosecurity）
  - 國家流行病學與人口健康中心（National Centre for Epidemiology and Population
    Health）

### ANU自然科學暨數學學院

  - ANU自然科學暨數學學院（[ANU College of Physical and Mathematical
    Sciences](http://cpms.anu.edu.au/)）
      - 數學研究所（Mathematical Sciences Institute）
      - 物理學和工程研究院（Research School of Physics and Engineering）
      - 天文學與天體物理學研究院（Research School of Astronomy and Astrophysics）
      - 化學研究院（Research School of Chemistry）
      - 地球科學研究院（Research School of Earth Sciences）
          - 先進顯微鏡中心（Centre for Advanced Microscopy）
  - 澳洲國家公眾意識科學中心（Australian National Centre for the Public Awareness of
    Science）

#### 澳洲電漿核融合研究設施

[Australian_National_University_Solar_Concentrator.jpg](https://zh.wikipedia.org/wiki/File:Australian_National_University_Solar_Concentrator.jpg "fig:Australian_National_University_Solar_Concentrator.jpg")
澳洲國立大學被視為是[核融合科技的發源地](../Page/核融合.md "wikilink")。\[23\]核融合程序於1932年由[澳洲科學家馬克](../Page/澳洲.md "wikilink")‧歐力峰[爵士](../Page/爵士.md "wikilink")（）發現；隨後於1950年代早期，他在[澳洲國立大學](../Page/澳洲國立大學.md "wikilink")（ANU）成立了全球知名的電漿核融合研究機構（Fusion
Plasma Research）。ANU校內的電漿核融合研究設施（Plasma Fusion Research
Facility）是全球[核融合與](../Page/核融合.md "wikilink")[電漿研究的重鎮](../Page/電漿.md "wikilink")，目前也是[南半球唯一的核融合研究設施](../Page/南半球.md "wikilink")。

#### 澳洲國家公眾意識科學中心

澳洲國家公眾意識科學中心（Australian National Centre for the Public Awareness of
Science；CPAS），是澳洲國立大學的眾多研究中心之一，同時也是[聯合國教科文組織](../Page/聯合國教科文組織.md "wikilink")（UNESCO）在[亞太地區的重要合作夥伴與據點](../Page/亞太地區.md "wikilink")。以科學傳播（Science
Communication）聞名。

## 校園環境

### 阿克頓主校區

[JCMSR.jpg](https://zh.wikipedia.org/wiki/File:JCMSR.jpg "fig:JCMSR.jpg")
澳洲國立大學的主校區位在澳洲[首都](../Page/首都.md "wikilink")[坎培拉的阿克頓](../Page/坎培拉.md "wikilink")（Acton）社區，校園總面積約150公頃，超過200棟建築與10,000棵樹木，鄰近[黑山](../Page/黑山_\(澳洲首都特區\).md "wikilink")（Black
Mountain）、[伯利·格里芬湖](../Page/伯利·格里芬湖.md "wikilink")（Lake Burley
Griffin）與坎培拉市中心。

ANU校園內設有銀行、郵局、書店、藥妝店、販賣部、咖啡屋、小酒吧、餐廳、旅行社，坐擁[南半球規模最大的圖書館](../Page/南半球.md "wikilink")；另備有完善的體育設施、學生醫療中心、畫廊、博物館、音樂廳等。

此外，坎培拉座落眾多的國家級文化和學術機構，如[澳大利亞聯邦科學與工業研究組織](../Page/澳大利亞聯邦科學與工業研究組織.md "wikilink")（CSIRO）、[澳洲科學院](../Page/澳洲科學院.md "wikilink")（AAS）、[澳洲人文科學院](../Page/澳洲人文科學院.md "wikilink")、[澳洲社會科學院](../Page/澳洲社會科學院.md "wikilink")（ASSA）、[澳洲國家植物園](../Page/澳洲國家植物園.md "wikilink")、[澳洲國家圖書館](../Page/澳洲國家圖書館.md "wikilink")、[澳洲國立美術館](../Page/澳洲國立美術館.md "wikilink")、[澳洲國立博物館](../Page/澳洲國立博物館.md "wikilink")、[澳洲國家檔案館](../Page/澳洲國家檔案館.md "wikilink")、[澳洲國立影片與聲音檔案館](../Page/澳洲國立影片與聲音檔案館.md "wikilink")、澳洲國家生物標準實驗室、[澳洲地質調查組織](http://www.ga.gov.au/)（AGSO）、澳洲犯罪學研究所（AIC）、[澳洲戰爭紀念館](../Page/澳洲戰爭紀念館.md "wikilink")、[國家肖像美術館](../Page/國家肖像美術館.md "wikilink")、[坎培拉博物館暨美術館和](../Page/坎培拉博物館暨美術館.md "wikilink")[國家首都展覽館等等](../Page/國家首都展覽館.md "wikilink")。ANU與這些機構有著緊密的聯繫和合作。
[ANU_School_of_Art.jpg](https://zh.wikipedia.org/wiki/File:ANU_School_of_Art.jpg "fig:ANU_School_of_Art.jpg")

### 分支校區

  - [賽丁泉天文台](../Page/賽丁泉天文台.md "wikilink")（Siding Spring
    Observatory），座落在標高海拔1165米，位於瓦倫本哥國家公園內。

  - [斯壯羅山天文台](../Page/斯壯羅山天文台.md "wikilink")（Mount Stromlo Observatory）。

  - （Kioloa Coastal
    Campus），位於[新南威爾斯州](../Page/新南威爾斯州.md "wikilink")[南海岸的小型分校區](../Page/南海岸.md "wikilink")。

  - [北澳研究單位](http://naru.anu.edu.au/)（North Australia Research
    Unit），位於[北領地](../Page/北領地.md "wikilink")[達爾文市的小型分校區](../Page/達爾文市.md "wikilink")。

  - [偏遠臨床醫學院](http://medicalschool.anu.edu.au/)（ANU Rural Clinical
    School），位於[新南威爾斯州東南方](../Page/新南威爾斯州.md "wikilink")。

  - [臨床醫學院](http://medicalschool.anu.edu.au/)（School of Clinical
    Medicine），與[澳洲首都特區的兩大](../Page/澳洲首都特區.md "wikilink")[醫院合作設立](../Page/醫院.md "wikilink")（[坎培拉醫院](http://health.act.gov.au/health-services/canberra-hospital/)和[Calvary醫院](http://www.calvary-act.com.au/)）。

### 博物館

  - ANU Drill Hall 美術館（[University Drill Hall
    Gallery](http://www.anu.edu.au/mac/content/dhg)）
  - ANU 藝術學院美術館（[ANU School of Art
    Gallery](http://soa.anu.edu.au/school-of-art-gallery)）
  - ANU 古典博物館（[ANU Classics
    Museum](http://culturalinquiry.anu.edu.au/classics-museum)）

<!-- end list -->

  - [澳洲國立博物館](../Page/澳洲國立博物館.md "wikilink")（[The National Museum of
    Australia](http://www.nma.gov.au/)）；非ANU附設博物館，但座落於ANU校園南端[伯利·格里芬湖畔](../Page/伯利·格里芬湖.md "wikilink")。

### 學校宿舍

澳洲國立大學目前一共有九座附屬學生宿舍（Residential Halls and
Colleges），其中八座位於主校區，一座位於挪斯本大道（Northbourne
Avenue）。此外，當中的兩座住宿學院（Residential
Colleges）擁有獨立的自治權暨申請者篩選程序。有些宿舍附食堂並供餐，住宿費用較高；有些宿舍則不供餐，但提供自炊的廚房設施。UniLodge則是採[民間興建營運後轉移模式](../Page/民間興建營運後轉移模式.md "wikilink")（BOT）的新建學生宿舍，2026年將由ANU收回管理權。

ANU校內宿舍（On-campus）：

  - Burgmann College - PG Village（獨立住宿學院）
  - John XXIII College（獨立住宿學院）
  - Bruce Hall - Packard Wing
  - Ursula Hall - Laurus Wing
  - Burton & Garran Hall
  - Toad Hall（限年滿24歲之研究生或榮譽生）
  - Graduate House（僅限研究生）
  - University House（僅限博士生）

[University_House_ANU_Canberra_(2948649223).jpg](https://zh.wikipedia.org/wiki/File:University_House_ANU_Canberra_\(2948649223\).jpg "fig:University_House_ANU_Canberra_(2948649223).jpg")
ANU校外宿舍（Off-campus）：

  - Fenner Hall
  - Griffin Hall（虛擬社群）
  - University Gardens（临近Belconnen， 澳洲首都，属于UniGardens Pty管理 \[24\]）

ANU委外學生宿舍（UniLodge）：

  - UniLodge Davey
  - UniLodge Kinloch
  - UniLodge Warrumbul
  - UniLodge Lena Karmel
  - Academie House

### 圖書館

澳洲國立大學圖書館隸屬於ANU資訊部門（Division of
Information），創建於1948年，至今總藏書量和規模在[亞太地區處於領先地位](../Page/亞太地區.md "wikilink")。由於眾多豐富的館藏無法容納於單一建築，所以ANU校方將圖書分散於校內的四座主圖書館與其它分館中，但皆由ANU總圖書資訊系統進行整合管理，可在任一分館借、還所有館藏。ANU圖書館與世界各國頂尖名校、國家圖書館皆簽有合作協議，可為教職員和學生以限時快遞的方式，向這些圖書館調閱研究資料或拷貝文獻副本，供學術用途。

  - 奇夫利圖書館（J. B. Chifley
    Building）：主要負責人文和社會科學領域的圖書收藏；館名為紀念前[澳洲總理](../Page/澳洲總理.md "wikilink")[約瑟夫·班納迪克特·奇夫利](../Page/約瑟夫·班納迪克特·奇夫利.md "wikilink")。
  - 孟席斯圖書館（R. G. Menzies
    Building）：主要負責國際關係研究，以及特殊經典的珍藏；館名為紀念前[澳洲總理](../Page/澳洲總理.md "wikilink")[羅伯特·孟席斯](../Page/羅伯特·孟席斯.md "wikilink")。
  - 漢考克圖書館（W. K. Hancock
    Building）：主要負責基礎科學、數學、歷史學和科技領域的圖書館藏；館名為紀念澳洲歷史學大師。
  - 法律圖書館（Law Library）：主要負責法學領域的圖書收藏。
  - 埃克爾斯醫學圖書館（Eccles Medical Sciences
    Library）：主要負責醫學領域的圖書收藏；館名為紀念澳洲[諾貝爾生理學或醫學獎得主](../Page/諾貝爾生理學或醫學獎.md "wikilink")[約翰·卡魯·埃克爾斯](../Page/約翰·卡魯·埃克爾斯.md "wikilink")。
  - 藝術圖書館（Art Library）：各式有關藝術領域的館藏，包含書籍、光碟、膠卷、展覽分類、影像帶、期刊與雜誌等等。
  - 化學圖書分館（Chemistry Branch Library）：主要負責化學領域的圖書收藏。
  - 地球科學圖書分館（Earth Sciences Branch Library）：主要負責地球科學、地理學等相關領域的圖書收藏。
  - 音樂圖書館（Music Library）：主要負責音樂藝術領域的影音檔案和文件收藏。

#### ANU電子出版社

[Chifley_library_at_anu.JPG](https://zh.wikipedia.org/wiki/File:Chifley_library_at_anu.JPG "fig:Chifley_library_at_anu.JPG")
澳洲國立大學電子出版社（[ANU E
Press](https://web.archive.org/web/20111203092955/http://epress.anu.edu.au/about.html)）隸屬於ANU資訊部門，創立於2003年。主要任務為出版電子學術書籍與電子學術期刊，亦提供免費的線上電子書資源。同時也可以透過ANU校內的列印需求服務（PoD），將文本以紙本形式呈現。

## 學術評鑑

澳洲國立大學被認為是[澳洲和](../Page/澳洲.md "wikilink")[南半球的學術殿堂](../Page/南半球.md "wikilink")；在全球約兩萬所大學之中，ANU於各主流大學排行榜皆穩居世界前一百大。

### 全球綜合排名

該大學的世界排名在前100之內，國內常排在第一位。

#### THE全球前五十大分科排行

[Ante_Dabro_Standing_figure_1981-82.JPG](https://zh.wikipedia.org/wiki/File:Ante_Dabro_Standing_figure_1981-82.JPG "fig:Ante_Dabro_Standing_figure_1981-82.JPG")

| 領域                                  | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 |
| ----------------------------------- | ---- | ---- | ---- | ---- | ---- | ---- |
| [人文學科](../Page/人文學科.md "wikilink")  | 14   | 4    | 13   | 15   | 16   |      |
| [社會科學](../Page/社會科學.md "wikilink")  | 22   | 18   | 20   | 26   | 24   |      |
| [物理學群](../Page/物理學.md "wikilink")   | N/A  | 26   | 28   | 29   | 28   |      |
| [生命科學](../Page/生命科學.md "wikilink")  | 39   | 33   | 36   | 36   | 33   |      |
| [電機工程](../Page/電機工程學.md "wikilink") | 44   | N/A  | N/A  | N/A  | N/A  |      |
| [臨床醫學](../Page/臨床醫學.md "wikilink")  | N/A  | N/A  | N/A  | N/A  | N/A  |      |
|                                     |      |      |      |      |      |      |

  - 2011年，ANU人文學科與[美國](../Page/美國.md "wikilink")[常春藤盟校](../Page/常春藤盟校.md "wikilink")[普林斯頓大學並列全球第四](../Page/普林斯頓大學.md "wikilink")，超越[英國](../Page/英國.md "wikilink")[牛津大學和](../Page/牛津大學.md "wikilink")[劍橋大學](../Page/劍橋大學.md "wikilink")。\[25\]

#### QS全球分科排行

| 學科                                                                  | 2007 | 2008 | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 |  |
| ------------------------------------------------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |  |
| [藝術與](../Page/藝術.md "wikilink")[人文學科](../Page/人文.md "wikilink")     | 10   | 12   | 12   | 17   | 13   | 13   | 16   | 10   | 12   |  |
| [社會科學與](../Page/社會科學.md "wikilink")[管理學科](../Page/管理.md "wikilink") | 16   | 14   | 11   | 13   | 16   | 16   | 21   | 13   | 18   |  |
| [自然科學學科](../Page/自然科學.md "wikilink")                                | 19   | 21   | 21   | 20   | 19   | 20   | 36   | 29   | 20   |  |
| [電機與](../Page/電機.md "wikilink")[科技學科](../Page/科技.md "wikilink")     | 39   | 36   | 42   | 45   | 44   | 50   | 64   | 49   | 41   |  |
| [生命科學與](../Page/生命科學.md "wikilink")[醫學學科](../Page/醫學.md "wikilink") | 28   | 37   | 21   | 36   | 39   | 44   | 116  | 93   | 81   |  |
|                                                                     |      |      |      |      |      |      |      |      |      |  |

  - ANU各學術領域皆屬世界一流，達全球前五十名；其中[人文學科](../Page/人文學科.md "wikilink")、[社會科學和](../Page/社會科學.md "wikilink")[自然科學領域更是堪稱世界頂尖](../Page/自然科學.md "wikilink")。

#### 澳洲聯邦官方科研實力排名

<table>
<thead>
<tr class="header">
<th><p>評鑑機構</p></th>
<th><p>2011</p></th>
<th><p>2013</p></th>
<th><p>2015</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 澳洲傑出研究計畫（[Excellence in Research for
    Australia](https://web.archive.org/web/20090328205039/http://www.arc.gov.au/era/)；ERA），是澳洲第一個由聯邦政府主導的官方大學排行榜。下次評鑑為2014年，預計2015年發布評鑑結果。

#### 全球大學畢業生就業能力排行

美國《紐約時報》國際版，在2013年10月份報導了一項由[法國和](../Page/法國.md "wikilink")[德國調查機構合作發表的](../Page/德國.md "wikilink")「全球大學畢業生就業能力調查」排行榜。澳洲國立大學（ANU）名列澳洲第
1 的寶座，同時排行世界第 20 名\[26\]。

#### 世界大學辯論排行

[ANU_College_of_Law.jpg](https://zh.wikipedia.org/wiki/File:ANU_College_of_Law.jpg "fig:ANU_College_of_Law.jpg")
近年來，ANU學生參加[世界大學辯論賽](../Page/世界大学辩论赛.md "wikilink")，屢創佳績，目前於世界名校辯論賽的總積分排名高居全球第九，名次高於[普林斯頓大學](../Page/普林斯頓大學.md "wikilink")（No.17）、[哈佛大學](../Page/哈佛大學.md "wikilink")（No.26）、[倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink")（No.27）、[史丹佛大學](../Page/史丹佛大學.md "wikilink")（No.30）和[康乃爾大學](../Page/康乃爾大學.md "wikilink")（No.46）等世界一流名校。\[27\]

## 國際合作和聯盟參與

澳洲國立大學與全球上百所大學簽有學術合作協議，例如：[哈佛大學](../Page/哈佛大學.md "wikilink")、[史丹佛大學](../Page/史丹佛大學.md "wikilink")、[清華大學](../Page/清華大學.md "wikilink")、[香港大學等](../Page/香港大學.md "wikilink")；並與各地名校合作，提供學生海外學習與觀摩機會。\[28\]

ANU是以下聯盟成員之一，與世界各地區的著名學府皆有著相當密切的學術交流與聯盟夥伴合作關係。

  - [國際研究型大學聯盟](../Page/國際研究型大學聯盟.md "wikilink")（IARU）

  - [澳洲八校聯盟](../Page/澳洲八校聯盟.md "wikilink")（Go8）

  - [大學天文研究協會](../Page/大學天文研究協會.md "wikilink")（AURA）

  - [環太平洋大學聯盟](../Page/環太平洋大學聯盟.md "wikilink")（APRU）

  - （ASAIHL）

## 知名校友

[BobHawke(cropped).jpg](https://zh.wikipedia.org/wiki/File:BobHawke\(cropped\).jpg "fig:BobHawke(cropped).jpg")：第23任澳洲總理\]\]
[Kevin_Rudd_DOS_cropped.jpg](https://zh.wikipedia.org/wiki/File:Kevin_Rudd_DOS_cropped.jpg "fig:Kevin_Rudd_DOS_cropped.jpg")：第26任澳洲總理\]\]

澳洲國立大學建校至今，已累積人數龐大的傑出校友與知名教職員\[29\]。除了各國政要和商業鉅子外，還包括了六位[諾貝爾獎得主](../Page/諾貝爾獎.md "wikilink")、兩位[澳洲總理](../Page/澳洲總理.md "wikilink")、一位[日本](../Page/日本.md "wikilink")[親王](../Page/親王.md "wikilink")、多位[澳洲科學院](../Page/澳洲科學院.md "wikilink")（AAS）院長、各國大學[校長和澳洲首席科學家](../Page/校長.md "wikilink")；數十位[澳洲國會議員](../Page/澳洲國會.md "wikilink")；澳洲聯邦法院法官、政黨黨魁、各州[州長](../Page/州長.md "wikilink")、各州[總督](../Page/總督.md "wikilink")；多個領域的傑出學者和科學家等等。

## 學生組織

  - ANU學生會（[Australian National University Students'
    Association](http://sa.anu.edu.au/)；ANUSA），負責代表ANU大學部學生出席校務和政府會議。
  - ANU研究生協會（[Postgraduate and Research Students'
    Association](http://parsa.anu.edu.au/)；PARSA），負責代表ANU研究所學生出席校務和政府會議。
  - ANU學生聯盟（[Australian National University
    Union](http://www.anuunion.com.au/)），非營利性質組織、委員會制，負責校內餐廳和商店的招商與管理；協助各類學生社團運作。
  - ANU發言人報（[Woroni](http://www.woroni.com.au/)），Woroni本意為[澳洲原住民語](../Page/澳洲原住民.md "wikilink")：發言人（Mouthpiece）。

## 相關逸聞

  - 根據2010年最新統計，ANU約有八成三的教職員擁有[博士學位](../Page/博士.md "wikilink")\[30\]，遠高於澳洲全體大學的平均（約五成）。
  - ANU草創初期（1946年），是以[美國大學協會創始校之一的](../Page/美國大學協會.md "wikilink")[約翰·霍普金斯大學為參考藍本](../Page/約翰·霍普金斯大學.md "wikilink")。特點：沒有大學部的研究型大學。
  - ANU的[學位服是採用傳統](../Page/學位服.md "wikilink")[牛劍設計準則](../Page/牛劍.md "wikilink")（*subfusc*），從流蘇、帽、袍到垂布，皆有詳細規範\[31\]。依照學位等級的不同，加上各學術領域的特定配色，多達數十種組合；畢業大典亦沿用[中世紀流傳下來的學位授予儀式](../Page/中世紀.md "wikilink")。

## 争议事件

2017年8月7日，澳大利亚国立大学信息科技专业的计算机课程授课教师Steve
Blackburn，在当日课程结束时展示的PPT中，以中英双语表示“我无法容忍学生作弊”。此举被认为是对中国留学生的侮辱，因而引起了中国留学生对其的强烈不满。\[32\]愤怒的中国学生在该校[Facebook上留言](../Page/Facebook.md "wikilink")，谴责Steve的这一言行，并将其投诉到了该校的管理处。虽然Facebook上的留言很快被学校删除\[33\]，不过Steven还是在这堂课的内部论坛中对事件表示歉意，并辩解道“这堂课上的中国留学生比重较大，所以才加了一个中文翻译版本”\[34\]。

一些评论认为，虽然在前几年曾经在这门课，以及这门课的前置课中发现中国留学生严重的舞弊行为，但这些已经是过去的事情；现在这门课尚未上交任何的作业和课堂检测，Steven不可能拿出证明中国留学生在这门课上作弊过的证据\[35\]；澳大利亚国立大学除澳洲本地学生外，还有很多来自美国、英国、印度等国的学生，Steve用中文对作弊的谴责是对中国来的留学生的偏见\[36\]，“具有很强的指向性”\[37\]。

不过，也有评论认为，把事件定义为“辱华”的中国留学生反应过激了\[38\]。

## 参考文献

## 外部連結

  - [The Australian National University（官方網站）](http://www.anu.edu.au/)

{{-}}

[Category:澳大利亚首都特区大学](../Category/澳大利亚首都特区大学.md "wikilink")
[澳洲國立大學](../Category/澳洲國立大學.md "wikilink")
[Australia](../Category/国立大学.md "wikilink")
[Category:坎培拉](../Category/坎培拉.md "wikilink")
[Category:1946年創建的教育機構](../Category/1946年創建的教育機構.md "wikilink")

1.  [271 ANU staff are current members of national academies
    (2010)](http://learn.anu.edu.au/uni.php)

2.  <https://reporter.anu.edu.au/anu-numbers-global-alumni>

3.

4.

5.

6.

7.

8.  [ANU History](http://www.anu.edu.au/about/history.php)

9.

10.

11. Paul A Gatenby and Nicholas J Glasgow. A medical school for the
    Australian National University. MJA 2002 177 (11): 8–9

12. Paul A Gatenby and Nicholas J Glasgow. A medical school for the
    Australian National University. MJA 2002 177 (11): 8–9

13. John-Paul Moloney. ANU Medical School 'frozen out' of talks on
    hospice. Canberra Times 09 Jul, 2002
    <http://www.canberratimes.com.au/news/local/news/general/anu-medical-school-frozen-out-of-talks-on-hospice/334938.aspx>
    accessed 15 January 2010.

14. [New plan for ANU future](http://news.anu.edu.au/?p=11351)

15. [ANU by 2020](http://www.anu.edu.au/about/strategy/)

16. [Using the ANU brand](http://www.anu.edu.au/mo/content/anu_brand)

17. [Coat of arms, motto &
    mace](http://about.anu.edu.au/profile/logo-motto)

18. [1](http://www.anu.edu.au/mac/images/uploads/Investment_flyer.pdf),
    ANU, December 2010

19. [ANU to establish $111.7m public policy
    precinct](http://news.anu.edu.au/?p=2137)

20. [New ANU ‘Kennedy School’ to play leading national public policy
    role](http://news.anu.edu.au/?p=14441)

21. [澳洲國立大學成立國家級政府學院](http://epaper.edu.tw/e9617_epaper/windows.aspx?windows_sn=5827)

22. [澳大利亞近日啟用超級電腦
    Vayu](http://aus.nsc.gov.tw/ct.asp?xItem=0981214003&ctNode=1076&lang=C)

23. [國科會國際合作簡訊](http://stn.nsc.gov.tw/files/stn_files/09407/c_9407.pdf)
    , Taiwan, 2005 NO. 7

24.

25. [THE Top 50 Arts and Humanities
    universities 2011-2012](http://www.timeshighereducation.co.uk/world-university-rankings/2011-2012/arts-and-humanities.html)

26.

27.

28. [ANU Exchange
    Partners](http://info.anu.edu.au/studyat/International_Office/exchange/anu-exchange-partners)

29. [ANU ALUMNI OFFICAL PAGE](http://www.anu.edu.au/alumni)

30. [83 per cent of ANU academic staff hold a PhD degree
    (2010)](http://learn.anu.edu.au/uni.php)

31. [A description of ANU Academic
    Dress](http://www.anu.edu.au/cabs/orders/acadcerdressorder.pdf)

32.

33.

34.

35.
36.
37.

38.