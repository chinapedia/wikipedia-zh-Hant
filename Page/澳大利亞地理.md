|                                                                                                                                                                         |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **澳大利亞地理**                                                                                                                                                              |
| [Australia_(orthographic_projection).svg](https://zh.wikipedia.org/wiki/File:Australia_\(orthographic_projection\).svg "fig:Australia_(orthographic_projection).svg") |
| **大陸**                                                                                                                                                                  |
| **區域**                                                                                                                                                                  |
| **座標**                                                                                                                                                                  |
| **面積**                                                                                                                                                                  |
| **海岸線**                                                                                                                                                                 |
| **最高點**                                                                                                                                                                 |
| **最低點**                                                                                                                                                                 |
| **最長河流**                                                                                                                                                                |
| **最大湖泊**                                                                                                                                                                |

<imagemap>
[File:Map_of_Australia.png|290px|thumb|澳大利亞地圖（可點擊了解詳情](File:Map_of_Australia.png%7C290px%7Cthumb%7C澳大利亞地圖（可點擊了解詳情)）
rect 30 223 63 238 [伯斯](../Page/伯斯.md "wikilink") rect 149 244 201 258
[阿得雷德](../Page/阿得雷德.md "wikilink") rect 183 280 247 294
[墨爾本](../Page/墨爾本.md "wikilink") rect 273 262 325 273
[坎培拉](../Page/坎培拉.md "wikilink") rect 285 239 326 255
[悉尼](../Page/悉尼.md "wikilink") rect 260 188 315 204
[布里斯本](../Page/布里斯本.md "wikilink") rect 136 58 178 74 [達爾文
(澳大利亞)](../Page/達爾文_\(澳大利亞\).md "wikilink") rect 251 327 294 342
[荷巴特](../Page/荷巴特.md "wikilink") rect 258 317 311 327
[塔斯馬尼亞省](../Page/塔斯馬尼亞省.md "wikilink") rect 241 256 269 264
[澳大利亞首都特區](../Page/澳大利亞首都特區.md "wikilink") poly 271 268 268 266 270 261
275 263 [澳大利亞首都特區](../Page/澳大利亞首都特區.md "wikilink")

poly 123 82 129 226 114 232 107 232 95 239 90 247 70 248 61 252 48 261
36 261 27 254 33 242 22 213 7 191 14 189 6 178 7 165 4 157 9 153 11 154
27 139 34 139 46 131 61 128 71 112 68 105 73 101 79 106 82 102 80 97 88
97 87 89 96 81 108 74 [西澳大利亞州](../Page/西澳大利亞州.md "wikilink") poly 123 82
127 176 196 177 200 96 178 80 183 73 182 68 187 67 191 61 187 58 163 56
155 49 149 51 154 56 139 59 134 68 129 75 130 82
[北領地](../Page/北領地.md "wikilink") poly 130 226 128 176 219
177 214 282 207 279 207 267 204 258 197 261 197 252 191 249 195 238 194
235 188 244 179 249 171 239 169 237 165 230 153 228 147 223
[南澳大利亞州](../Page/南澳大利亞州.md "wikilink") poly 199 96 196 177
219 177 218 202 275 208 283 205 287 205 295 210 300 209 302 205 307 205
313 206 314 185 299 164 299 155 294 153 291 154 285 133 276 128 276 126
270 126 266 115 265 104 263 102 261 86 256 81 249 80 247 66 240 48 233
62 233 65 230 78 229 91 224 104 215 107
[昆士蘭省](../Page/昆士蘭省.md "wikilink") poly 218 203 273 208
284 204 296 210 300 210 302 206 313 204 312 213 306 231 301 241 291 248
285 263 277 281 265 275 264 266 252 267 242 264 241 265 233 258 232 254
227 253 224 248 215 246 [新南威爾斯州](../Page/新南威爾斯州.md "wikilink") poly 215
246 213 281 219 287 229 290 239 286 242 282 245 289 249 293 260 287 276
284 265 275 264 267 255 267 245 264 242 266 233 257 228 253 224 247
[維多利亞省](../Page/維多利亞省.md "wikilink") poly 238 307 241 307 249
313 261 309 262 312 262 321 257 329 250 335 241 325
[塔斯馬尼亞省](../Page/塔斯馬尼亞省.md "wikilink")

rect 115 264 182 285 [大澳洲灣](../Page/大澳洲灣.md "wikilink") rect 277 297 312
317 [塔斯曼海](../Page/塔斯曼海.md "wikilink") rect 13 74 57 99
[印度洋](../Page/印度洋.md "wikilink") rect 290 73 320 96
[珊瑚海](../Page/珊瑚海.md "wikilink") rect 51 4 136 18
[印度尼西亚](../Page/印度尼西亚.md "wikilink") rect 241 1 309 27
[巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink") rect 187 69 229 88
[卡奔塔利亞灣](../Page/卡奔塔利亞灣.md "wikilink") rect 148 30 202 44
[阿拉弗拉海](../Page/阿拉弗拉海.md "wikilink") rect 104 26 134 45
[東帝汶](../Page/東帝汶.md "wikilink") rect 104 45 130 66
[帝汶海](../Page/帝汶海.md "wikilink")

poly 248 56 265 106 295 139 302 120 261 54
[大堡礁](../Page/大堡礁.md "wikilink")

</imagemap>
**[澳大利亞](../Page/澳大利亞.md "wikilink")**位於[南半球](../Page/南半球.md "wikilink")，土地面积7,686,850平方公里，是世界[最小的大陸](../Page/澳大利亞洲.md "wikilink")，也是[世界面積第六大的國家](../Page/世界各國和地區面積列表.md "wikilink")。澳大利亞四面環海，擁有長達25,760公里的海岸線和面積達8,148,250平方公里的[專屬經濟區](../Page/專屬經濟區.md "wikilink")。澳大利亞的海上鄰國包括了[印度尼西亞](../Page/印度尼西亞.md "wikilink")、[東帝汶](../Page/東帝汶.md "wikilink")、[巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink")、[所羅門群島](../Page/所羅門群島.md "wikilink")、[萬那杜](../Page/萬那杜.md "wikilink")、[新喀里多尼亞和](../Page/新喀里多尼亞.md "wikilink")[新西蘭](../Page/新西蘭.md "wikilink")。

澳大利亞全國可分為東部山地、中部平原和西部高原三個地區。中部是遼闊的沙漠，乾旱少雨；而[大分水嶺以東則降水較為充沛](../Page/大分水嶺.md "wikilink")。[澳大利亞人口密度低](../Page/澳大利亞人口.md "wikilink")（3人/平方公里）\[1\]，人口主要集中在東部與東南部海岸。澳大利亞為全球17個[超級生物多樣性國家之一](../Page/超級生物多樣性國家.md "wikilink")，東北部海岸有世界聞名的自然遺產[大堡礁](../Page/大堡礁.md "wikilink")，最大的離島[塔斯馬尼亞島則位于南部海域](../Page/塔斯馬尼亞島.md "wikilink")。

## 疆域

[Australia-states-timezones.png](https://zh.wikipedia.org/wiki/File:Australia-states-timezones.png "fig:Australia-states-timezones.png")
[澳大利亚位於](../Page/澳大利亚.md "wikilink")[南半球](../Page/南半球.md "wikilink")、[东半球](../Page/东半球.md "wikilink")，介於[南太平洋和](../Page/南太平洋.md "wikilink")[印度洋以及](../Page/印度洋.md "wikilink")[亚洲和](../Page/亚洲.md "wikilink")[南极洲之间](../Page/南极洲.md "wikilink")，擁有[澳洲大陆和](../Page/澳洲大陆.md "wikilink")[塔斯曼尼亞等岛屿](../Page/塔斯曼尼亞.md "wikilink")。澳洲四面環海，东南隔[塔斯曼海与](../Page/塔斯曼海.md "wikilink")[紐西蘭为邻](../Page/紐西蘭.md "wikilink")，北部隔[帝汶海和](../Page/帝汶海.md "wikilink")[托雷斯海峡与](../Page/托雷斯海峡.md "wikilink")[东帝汶](../Page/东帝汶.md "wikilink")、[印度尼西亞和](../Page/印度尼西亞.md "wikilink")[巴布亞新几内亞相望](../Page/巴布亞新几内亞.md "wikilink")。\[2\]

[大洋洲地处](../Page/大洋洲.md "wikilink")[亚洲与](../Page/亚洲.md "wikilink")[美洲之间空中](../Page/美洲.md "wikilink")、海上航线以及海底电缆所经之地，同时还是舰船的淡水和燃料供应地，不仅在国际交通上和战略上居重要地位，而且也为其旅游业的发展提供了较为优越的条件。澳大利亚佔有大洋洲绝大部分陸地，總面積769.2萬平方公里。海岸线较为平直，长達36,735公里\[3\]。澳大利亚东西最大距离4,007千米，南北最大距离3,680千米。最北的[约克角位于南纬](../Page/約克角半島.md "wikilink")10°，最南的威尔逊角位于南纬39°，最西的斯提普角位于东经113°，最东的[拜伦角位于东经](../Page/拜伦角.md "wikilink")153°\[4\]。

横跨三个基础时区，西部时区（[西澳洲](../Page/西澳洲.md "wikilink")）为+8区；中部时区（[北领地和](../Page/北领地.md "wikilink")[南澳洲](../Page/南澳洲.md "wikilink")）为+9.5区，其中[南澳洲实行](../Page/南澳洲.md "wikilink")[夏令时为](../Page/夏令时.md "wikilink")+10.5区；东部时区（[昆士兰省等](../Page/昆士兰省.md "wikilink")）为+10区，其中[新南威爾斯](../Page/新南威爾斯.md "wikilink")（NSW）、[塔斯馬尼亞](../Page/塔斯馬尼亞.md "wikilink")（TAS）、[維多利亞](../Page/維多利亞州.md "wikilink")（VIC）和[澳洲首都領地](../Page/澳洲首都領地.md "wikilink")（ACT）实行夏令时为+11区。\[5\]

## 地质

澳大利亚是地球上最古老的大陆，是[冈瓦纳古陆的一部分](../Page/冈瓦纳古陆.md "wikilink")\[6\]。於澳洲西南部Mount
Barren群地層南緣的所做的[鋯石定年分析顯示](../Page/鋯石.md "wikilink")，大約在17亿年前，皮爾巴拉-尤岡（Pilbara-Yilgarn）與尤岡-高勒（Yilgarn-Gawler）兩個[克拉通發生碰撞](../Page/克拉通.md "wikilink")，聚集成為原始的[澳洲大陸](../Page/澳洲大陸.md "wikilink")\[7\]。

由于澳洲位于板块中间，因此没有[活火山](../Page/活火山.md "wikilink")，平均每5年会有一次-{zh-cn:超过6级;zh-tw:規模6以上;zh-hk:超過6級}-的地震。有史以来最大的一次地震是发生在1906年，估计[-{zh-cn:地震震级;zh-tw:地震規模;zh-hk:地震震級}-达](../Page/地震規模.md "wikilink")-{zh-cn:7.3级;zh-tw:7.3;zh-hk:7.3級}-\[8\]。

资源丰富，拥有世界上最多的[黄金](../Page/黄金.md "wikilink")、铁矿石、铅、镍、金红石、银、铀、锌和锆资源，第二多的铝土矿、褐煤、钴、钛铁矿和钽资源，第三多的铜和锂资源，第四多的钍资源以及第五多的黑煤和锰矿资源。其中，黄金储量逾1000吨、铜储量逾400万吨。\[9\]

## 地形

澳大利亚全境平均[海拔](../Page/海拔.md "wikilink")300米，超過1,000米的山地面積低於1％，低於500公尺者達87％，乃地表起伏最和緩的大陸。澳大利亚可以分为五个地形区：一、西部是海拔200\~500米的低[高原](../Page/高原.md "wikilink")，多分布[沙漠和半沙漠](../Page/沙漠.md "wikilink")；也有一些海拔1,000\~1,200米的横断山脉。二、中部是[平原](../Page/平原.md "wikilink")，海拔在200米以下，盛长草本植物，其中[埃尔湖](../Page/埃尔湖.md "wikilink")（Lake
Eyre）是最低点，湖面低于海平面12米，以此为中心的大平原为[大自流盆地](../Page/大自流盆地.md "wikilink")。三、东部是[古老山脉所形成的](../Page/大分水岭.md "wikilink")[高地](../Page/高地.md "wikilink")。四、[大陆型岛屿](../Page/大陆岛.md "wikilink")，如：[塔斯馬尼亞岛](../Page/塔斯馬尼亞岛.md "wikilink")。五、[海洋型岛屿](../Page/珊瑚岛.md "wikilink")，如：东北部沿海的[大堡礁是全球最大的珊瑚礁](../Page/大堡礁.md "wikilink")。\[10\]

澳大利亚西部高原，面积约占该大陆的60％。高原南部、[大澳大利亞灣以北是](../Page/大澳大利亞灣.md "wikilink")[納拉伯平原](../Page/納拉伯平原.md "wikilink")，正好位於[南澳大利亞州和](../Page/南澳大利亞州.md "wikilink")[西澳大利亞州之間](../Page/西澳大利亞州.md "wikilink")。[大分水岭位于](../Page/大分水岭.md "wikilink")[澳洲東岸](../Page/澳洲東岸.md "wikilink")，自[新南威尔士州以北与海岸线大致平行](../Page/新南威尔士州.md "wikilink")，北起[约克角半岛](../Page/约克角半岛.md "wikilink")，南至[维多利亚州](../Page/维多利亚州.md "wikilink")，长3,500公里，宽約200至300公里，海拔一般约800至1,000米，东坡较陡，西部缓斜，东南部的[科修斯科山海拔](../Page/科修斯科山.md "wikilink")2,230米，是全澳最高峰，由于地势较高，冬季会有积雪\[11\]\[12\]\[13\]。

澳大利亚拥有[大自流盆地](../Page/大自流盆地.md "wikilink")、墨累河盆地、巴克利盆地、[尤克拉盆地](../Page/尤克拉盆地.md "wikilink")、荒漠盆地、[坎宁盆地等自流泉盆地](../Page/坎宁盆地.md "wikilink")20多处，总共面积达270万平方公里，大部分位于干旱、半干旱地区。其中，[大自流盆地从](../Page/大自流盆地.md "wikilink")[卡奔塔利亚湾向南延伸至](../Page/卡奔塔利亚湾.md "wikilink")[达令河北支流的上游](../Page/达令河.md "wikilink")，平均海拔200米以下，面积达173.5万平方公里，为世界最大的自流盆地。大自流盆地拥有自流井1.8万眼，丰富的地下水不仅为当地的畜牧业生产提供水源，而且也为发展旅游业在水源方面提供了较好的条件。\[14\]

[塔斯马尼亚岛地势较高](../Page/塔斯马尼亚岛.md "wikilink")，因第四纪冰川三次侵蚀而形成U形谷、悬谷和瀑布等自然景观资源。现在在较高处仍然有冰川分布。岛西南部的维德港，因冰川侵蚀而形成了峡湾海岸。[东部海岸地区有大片的洁净沙滩分布](../Page/黃金海岸_\(澳洲\).md "wikilink")，东北部沿海形成了长达2,000余公里的[大堡礁](../Page/大堡礁.md "wikilink")。\[15\]\[16\]

## 地貌

### 沙漠

<table>
<caption>澳大利亚沙漠</caption>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>名称</p></th>
<th><p>面积<br />
（平方公里）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/大維多利亞沙漠.md" title="wikilink">大維多利亞沙漠</a></p></td>
<td><p>424,400</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/大沙沙漠.md" title="wikilink">大沙沙漠</a></p></td>
<td><p>284,993</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/塔纳米沙漠.md" title="wikilink">塔纳米沙漠</a></p></td>
<td><p>184,500</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/辛普森沙漠.md" title="wikilink">辛普森沙漠</a></p></td>
<td><p>176,500</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/吉布森沙漠.md" title="wikilink">吉布森沙漠</a></p></td>
<td><p>155,000</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/小沙沙漠.md" title="wikilink">小沙沙漠</a></p></td>
<td><p>111,500</p></td>
</tr>
</tbody>
</table>

澳大利亚虽然四面环海，却非常干燥，[沙漠和半沙漠面积达](../Page/沙漠.md "wikilink")340万平方公里，约占总面积的44%\[17\]。[西澳大利亞沙漠是](../Page/西澳大利亞沙漠.md "wikilink")[吉布森沙漠](../Page/吉布森沙漠.md "wikilink")、[大沙沙漠](../Page/大沙沙漠.md "wikilink")、[小沙沙漠等](../Page/小沙沙漠.md "wikilink")[澳大利亞西部](../Page/澳大利亞.md "wikilink")[沙漠的統稱](../Page/沙漠.md "wikilink")\[18\]。
其中，[大维多利亚沙漠位于](../Page/大维多利亚沙漠.md "wikilink")[澳洲内陆西南部](../Page/澳洲.md "wikilink")，跨[南澳大利亚州和](../Page/南澳大利亚州.md "wikilink")[西澳大利亚州](../Page/西澳大利亚州.md "wikilink")，北接[吉布森沙漠](../Page/吉布森沙漠.md "wikilink")，南邻[纳拉伯平原](../Page/纳拉伯平原.md "wikilink")。東西长1,000多公里，最宽约550公里，面積達424,400平方公里；平均海拔150－300米，多[沙丘](../Page/沙丘.md "wikilink")、沙原和盐沼。[塔纳米沙漠位于](../Page/塔纳米沙漠.md "wikilink")[澳洲北部](../Page/澳洲.md "wikilink")，大部份橫亙於[北領地中部](../Page/北領地.md "wikilink")，小部份伸入[西澳大利亞州](../Page/西澳大利亞州.md "wikilink")。[辛普森沙漠位于](../Page/辛普森沙漠.md "wikilink")[澳大利亚中部](../Page/澳大利亚.md "wikilink")，面积约176,500平方公里，包含澳洲[北领地](../Page/北领地.md "wikilink")、[南澳州和](../Page/南澳州.md "wikilink")[昆士兰州的一些地区](../Page/昆士兰州.md "wikilink")\[19\]\[20\]\[21\]。

### 雨林

澳大利亚的冈瓦纳雨林包括散布于[澳大利亚](../Page/澳大利亚.md "wikilink")[新南威尔士州和](../Page/新南威尔士州.md "wikilink")[昆士兰州边界上的多片雨林保护区](../Page/昆士兰州.md "wikilink")，也是世界上最大面积的亚热带雨林\[22\]。盾状火山口群的地质特点和大量珍稀濒危雨林物种使得该保护区在国际上具有很高的科学价值和保护价值。1986年正式列入世界遗产名录，1994年扩展为今日的规模。冈瓦纳雨林共包括50块保护区，总面积达3,665平方公里，相当于澳大利亚总面积的千分之三\[23\]。原本以所在的地域为名称作“中东部雨林”，后来以冈瓦纳为名是因为化石证明，在[冈瓦纳大陆时](../Page/冈瓦纳大陆.md "wikilink")，这片雨林的植物组成就已经今日一样\[24\]。在澳大利亚别的地方，也有和冈瓦纳大陆时一样植被的雨林，最大的一块处于[塔斯马尼亚岛上的塔尔金荒野](../Page/塔斯马尼亚岛.md "wikilink")。冈瓦纳雨林每年的游客大约有二百万\[25\]。

### 丹霞

[Uluru_Panorama.jpg](https://zh.wikipedia.org/wiki/File:Uluru_Panorama.jpg "fig:Uluru_Panorama.jpg")
澳大利亚[丹霞地貌的岩性绝大部分为砂岩](../Page/丹霞地貌.md "wikilink")\[26\]。[烏盧魯是一个大型](../Page/烏盧魯.md "wikilink")[砂岩岩层](../Page/砂岩.md "wikilink")，位於澳洲的近乎地理中央處，[北领地的南部](../Page/北领地.md "wikilink")，[烏魯汝－卡塔楚塔國家公園之內](../Page/烏魯汝－卡塔楚塔國家公園.md "wikilink")，是世界上僅次於[奧古斯特山的第二大單體岩石](../Page/奧古斯特山.md "wikilink")，是（该地区的[澳大利亚原住民](../Page/澳大利亚原住民.md "wikilink")）的圣地，而列为[世界遗产](../Page/世界遗产.md "wikilink")。由於形成烏盧魯的[砂岩含有較多鐵粉](../Page/砂岩.md "wikilink")，氧化之後的鐵份呈現紅色，使得烏盧魯呈現紅色外觀。原住民在距今1萬年之前已經開始在烏盧魯一帶居住。這裡發現的壁畫遺跡中歷史最久的距今已有超過1,000年的歷史\[27\]\[28\]\[29\]。烏盧魯現在由原住民組織所有，在法律上該組織將烏盧魯一帶的土地借給澳洲政府至2084年。現在進入烏盧魯一帶地區需要支付25澳元的入場費。\[30\]

## 水文

### 陆地水文

[墨累河是](../Page/墨累河.md "wikilink")[澳大利亞最長的河流](../Page/澳大利亞.md "wikilink")\[31\]，發源於澳大利亞最高的[山脈](../Page/山脈.md "wikilink")[澳大利亞阿爾卑斯山脈](../Page/澳大利亞阿爾卑斯山脈.md "wikilink")，往西北方流，構成[維多利亞州與](../Page/維多利亞州.md "wikilink")[新南威爾斯州的邊界](../Page/新南威爾斯州.md "wikilink")\[32\]。墨累河在最後500公里往南移動，進入[南澳大利亞州](../Page/南澳大利亞州.md "wikilink")，經由，於[阿德萊德附近的](../Page/阿德萊德.md "wikilink")注入[印度洋](../Page/印度洋.md "wikilink")\[33\]。[墨累河水量季节性强](../Page/墨累河.md "wikilink")，干季易断流。

澳大利亚湖泊大致分为五种类型：沿海湖泊、泻湖（淡水湖）、冰川湖、盐湖和火山湖。澳大利亚地图册中标注的湖泊共计有101个，加上比较小的湖泊，全澳约有700多个湖泊，但全年有水的并不多，大部分是盐沼。澳大利亚建有近400座水库，总库容达800多亿立方米。全澳主要有8个比较大的淡水湖和12个咸水湖，其中[艾爾湖是是澳洲最大的湖泊](../Page/艾爾湖.md "wikilink")，面积超过9,000平方公里，长期呈干涸状态；其次是[托伦斯湖](../Page/托伦斯湖.md "wikilink")。\[34\]

<table>
<caption>澳大利亚主要河流[35]</caption>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>英文名称</p></th>
<th><p>流经州</p></th>
<th><p>注入</p></th>
<th><p>长度（公里）</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Reliefmap_of_Australia.png" title="fig:Reliefmap_of_Australia.png">Reliefmap_of_Australia.png</a><br />
澳大利亚水系图</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/墨累河.md" title="wikilink">墨累河</a></p></td>
<td><p>River Murray</p></td>
<td><p><a href="../Page/新南威爾斯州.md" title="wikilink">新南威爾斯州</a>、南澳大利亞州</p></td>
<td><p><a href="../Page/印度洋.md" title="wikilink">印度洋</a></p></td>
<td><p>2,508</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>马兰比吉河</p></td>
<td><p>Murrumbidgee River</p></td>
<td><p>新南威爾斯州、<a href="../Page/澳大利亞首都特區.md" title="wikilink">澳大利亞首都特區</a></p></td>
<td></td>
<td><p>1,485</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/达令河.md" title="wikilink">达令河</a></p></td>
<td><p>Darling River</p></td>
<td><p>新南威爾斯州</p></td>
<td><p>墨累河</p></td>
<td><p>1,545</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>拉克兰河</p></td>
<td><p>Lachlan River</p></td>
<td><p>新南威爾斯州</p></td>
<td></td>
<td><p>1,339</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>库珀河</p></td>
<td><p>Cooper Creek</p></td>
<td><p><a href="../Page/昆士蘭省.md" title="wikilink">昆士蘭省</a>、<a href="../Page/南澳大利亞州.md" title="wikilink">南澳大利亞州</a></p></td>
<td><p>艾爾湖</p></td>
<td><p>1,113</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>弗林德斯河</p></td>
<td><p>Flinders River</p></td>
<td><p>昆士蘭省</p></td>
<td><p><a href="../Page/卡奔塔利亞灣.md" title="wikilink">卡奔塔利亞灣</a></p></td>
<td><p>1,004</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>迪亚曼蒂纳河</p></td>
<td><p>Diamantina River</p></td>
<td><p>昆士蘭省、南澳大利亞州</p></td>
<td><p><a href="../Page/艾爾湖.md" title="wikilink">艾爾湖</a></p></td>
<td><p>941</p></td>
<td></td>
</tr>
</tbody>
</table>

### 海洋水文

[Corrientes-oceanicas.png](https://zh.wikipedia.org/wiki/File:Corrientes-oceanicas.png "fig:Corrientes-oceanicas.png")\]\]
[東澳洋流是澳洲沿岸最大](../Page/東澳洋流.md "wikilink")[洋流](../Page/洋流.md "wikilink")，沿著[澳洲東岸北邊流向南邊](../Page/澳洲.md "wikilink")，夾帶溫暖的海水。在某些[大陸棚](../Page/大陸棚.md "wikilink")，海流時速最高可達7[節](../Page/節.md "wikilink")，均值為2至3節。東澳洋流會在澳洲和[紐西蘭之間的](../Page/紐西蘭.md "wikilink")[塔斯曼海形成漩渦](../Page/塔斯曼海.md "wikilink")，並將熱帶海洋生物帶往澳洲東南岸亞熱帶的區域。\[36\]\[37\]

西澳洋流为[印度洋環流的一部分](../Page/印度洋環流.md "wikilink")。印度洋的[南赤道洋流西流至](../Page/南赤道洋流.md "wikilink")[非洲沿岸轉向南流](../Page/非洲.md "wikilink")，經[莫三比克海峽](../Page/莫三比克海峽.md "wikilink")，稱為，屬[暖流性質](../Page/暖流.md "wikilink")，然後在非洲南端併入[西風漂流](../Page/西風漂流.md "wikilink")，東流至[澳洲西南](../Page/澳洲.md "wikilink")，一部分水流沿澳洲西岸轉向北流，稱為西澳洋流。\[38\]\[39\]

## 气候

[Australia_Köppen.svg](https://zh.wikipedia.org/wiki/File:Australia_Köppen.svg "fig:Australia_Köppen.svg")圖（[柯本氣候分類法](../Page/柯本氣候分類法.md "wikilink")）\]\]

澳洲地处南半球，12月\~2月为夏季，3月\~5月为秋季，6月\~8月为冬季，9月\~11月为春季\[40\]。澳大利亚北部年均气温為27℃，南部14℃。\[41\]澳大利亚大部分地区受[副热带高气压控制](../Page/副热带高气压.md "wikilink")，气候以炎热干燥为主要特征，但同时也具有多样性。西部高原和内陆沙漠还有墨尔本地区属[热带沙漠气候](../Page/热带沙漠气候.md "wikilink")（BWh）和[熱帶乾旱半乾旱氣候](../Page/熱帶乾旱半乾旱氣候.md "wikilink")（BSh），乾旱少雨，年降水量仅100\~300毫米；北部属[热帶草原气候](../Page/热帶草原气候.md "wikilink")（Aw，冬季干旱型），年降水量1,000\~2,300毫米，为全国多雨区，少部份属[副熱帶濕潤氣候](../Page/副熱帶濕潤氣候.md "wikilink")（CWa、Cfa）；东部新英格兰山地以南属[海洋性气候](../Page/海洋性气候.md "wikilink")（Cfb、Cfc）和[温带阔叶林气候](../Page/溫帶落葉林.md "wikilink")，年降水量500\~1,200毫米。\[42\]

澳洲干旱面积比例达44%，是全球最乾燥的大陆\[43\]，[饮用水主要是自然降水](../Page/饮用水.md "wikilink")，並依賴大壩蓄水供水。政府严禁使用[地下水](../Page/地下水.md "wikilink")，因為地下水資源一旦開採，很難恢復。2006年起，[聖嬰現象影响擴大](../Page/厄尔尼诺现象.md "wikilink")，導致降雨大幅减少，各大城市普遍缺水，纷纷颁布多項限制用水的法令，以节水渡过干旱\[44\]。這一現象直到2008年后開始好轉，[東海岸每年的降雨量亦開始恢復正常](../Page/澳洲東岸.md "wikilink")\[45\]。澳大利亚在夏季风全盛时期存在三个季风槽：西南太平洋夏季风槽、东南印度洋夏季风槽和北澳大利亚季风槽\[46\]。澳大利亚北部地区在季风爆发之前会有降水\[47\]。

## 生态

[Koala_climbing_tree.jpg](https://zh.wikipedia.org/wiki/File:Koala_climbing_tree.jpg "fig:Koala_climbing_tree.jpg")上的[無尾熊](../Page/無尾熊.md "wikilink")\]\]
澳洲大部分是半干旱或[荒漠地带](../Page/荒漠.md "wikilink")，但生态环境極為丰富，包括从高地的石南荒原到热带的雨林，為全球17个[超级生物多样性国家之一](../Page/超级生物多样性国家.md "wikilink")。澳洲许多生物是當地独有，原因是澳洲大陆较古老、地理上长期孤立，和氣候极端多变。其[被子植物约](../Page/被子植物.md "wikilink")85%、[哺乳类动物約](../Page/哺乳类动物.md "wikilink")84%、鸟类超过45%，和近岸温带鱼类約89%是[特有种](../Page/特有种.md "wikilink")。\[48\]澳洲许多生态区，和区内的生物已面臨人类活动和外来物种的威胁。联邦《1999年环境保护和生物多样性保育法案》是保护濒危物种的根本法律。基於“[生物多样性行动计划](../Page/生物多样性行动计划.md "wikilink")”，设立了許多受保护地区以保育独特的生态系統；64个湿地根据《[湿地公约](../Page/湿地公约.md "wikilink")》而登记；另有16个[世界遗产](../Page/世界遗产.md "wikilink")。\[49\]\[50\]

澳大利亚约有27,700种植物，澳洲多數木本植物是常绿的，且很能适应火灾和干旱，例如[桉树和](../Page/桉树.md "wikilink")[金合欢](../Page/金合欢.md "wikilink")。澳洲的[豆科植物种类繁多且本地特有](../Page/豆科.md "wikilink")，它們能与[根瘤菌和菌根](../Page/根瘤菌.md "wikilink")[真菌共生](../Page/真菌.md "wikilink")，因此較适应较贫瘠的土壤。澳大利亚的雨林横跨整个国家，覆盖各种气候类型，其中[昆士兰州北部的戴恩树热带雨林](../Page/昆士兰州.md "wikilink")（Daintree
Rainforest）是地球上最古老的[热带雨林](../Page/热带雨林.md "wikilink")。\[51\]

居住在這片[大陸上的動物中](../Page/澳洲大陸.md "wikilink")，有83%的[哺乳類](../Page/哺乳類.md "wikilink")、89%的[爬蟲類](../Page/爬蟲類.md "wikilink")、90%的[魚與](../Page/魚.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")，以及93%的[兩棲類是澳洲的](../Page/兩棲類.md "wikilink")[特有種](../Page/特有種.md "wikilink")\[52\]。澳洲出名的[动物有](../Page/澳洲動物相.md "wikilink")[单孔目](../Page/单孔目.md "wikilink")（如[鸭嘴兽和](../Page/鸭嘴兽.md "wikilink")[针鼹](../Page/针鼹.md "wikilink")）、[有袋类](../Page/有袋类.md "wikilink")（如[袋鼠](../Page/袋鼠.md "wikilink")、[無尾熊和](../Page/無尾熊.md "wikilink")[袋熊](../Page/袋熊.md "wikilink")，其中[拳击袋鼠还是澳大利亚的国家象征](../Page/拳击袋鼠.md "wikilink")）、[湾鳄和](../Page/湾鳄.md "wikilink")[淡水鳄](../Page/澳洲淡水鱷.md "wikilink")、[鸸鹋和](../Page/鸸鹋.md "wikilink")[笑翠鸟等鸟类](../Page/笑翠鸟.md "wikilink")。澳洲的毒蛇数量在世界之最。\[53\]

[澳大利亚野犬约引进於公元前](../Page/澳大利亚野犬.md "wikilink")3,000年，當時澳洲原住民與南岛人贸易。\[54\]人类最初定居后，许多动植物品种很快绝种，包括一些大型动物，欧洲殖民开始后，更多物种步向灭绝，包括[袋狼](../Page/袋狼.md "wikilink")。\[55\]

## 灾害

### 火灾

但近十年來，由於乾旱惡化，東岸地區屢次傳出森林大火；2009年，由於縱火案，東岸地區的森林和部分民宅受到有史以來最嚴重的火災侵襲。[澳大利亞叢林大火是](../Page/澳大利亞叢林大火.md "wikilink")[澳大利亞炎熱乾燥季節頻繁發生的野外火災](../Page/澳大利亞.md "wikilink")，每年都會破壞大面積的土地，並且造成財產損失和人員傷亡。特定的澳大利亞本地植物群依賴著叢林大火繁殖，火災時常交替發生，成爲了澳大利亞生態的重要組成部份。對一些[桉樹和](../Page/桉樹.md "wikilink")[斑克木來說](../Page/斑克木.md "wikilink")，大火可以使植物種子開裂，從而得以生根發芽\[56\]。大火也促進了新的植被生成，一些別的物種也能夠很快從大火造成的損失中恢復過來。\[57\]\[58\]\[59\]

### 風暴

由于澳大利亚四面环海，会受到大洋上形成的风暴、气旋影响。影响比较大的有[气旋雷瓦](../Page/气旋雷瓦.md "wikilink")、[氣旋塔莎](../Page/氣旋塔莎.md "wikilink")、[強烈熱帶氣旋崔西](../Page/強烈熱帶氣旋崔西.md "wikilink")、[气旋奥森](../Page/气旋奥森.md "wikilink")、[气旋莫妮卡](../Page/气旋莫妮卡.md "wikilink")、[气旋雅思](../Page/气旋雅思.md "wikilink")、[氣旋阿萊西亞等](../Page/氣旋阿萊西亞.md "wikilink")。\[60\]\[61\]\[62\]

## 注释

## 参见

  - [澳大利亞行政區劃](../Page/澳大利亞行政區劃.md "wikilink")

  - [澳大利亞洲](../Page/澳大利亞洲.md "wikilink")

  - [澳大利亚城市人口列表](../Page/澳大利亚城市人口列表.md "wikilink")

  -
## 参考文献

[\*](../Category/澳大利亞地理.md "wikilink")
[Category:大洋洲各国地理](../Category/大洋洲各国地理.md "wikilink")

1.

2.

3.  "State and Territory Government". Government of Australia. Retrieved
    2010-04-23.

4.

5.

6.  Pain, C.F., Villans, B.J., Roach, I.C., Worrall, L. & Wilford, J.R.
    (2012): Old, flat and red - Australia's distinctive landscape. In:
    *Shaping a Nation: A Geology of Australia.* Blewitt, R.S. (Ed.)
    Geoscience Australia and ANU E Press, Canberra. Pp. 227-275 ISBN
    978-1-922103-43-7

7.

8.

9.

10. "The World Factbook 2009". Washington, D.C.: Central Intelligence
    Agency. 2009. Archived from the original on 2010-03-24. Retrieved 29
    March 2010.

11.

12. Shaw, John H., *Collins Australian Encyclopedia*, William Collins
    Pty Ltd., Sydney, 1984, ISBN 0-00-217315-8

13.

14.

15. The Great Barrier Reef World Heritage Area, which is 348,000 km
    squared, has 2900 reefs. However, this does not include the reefs
    found in the Torres Strait, which has an estimated area of 37,000 km
    squared and with a possible 750 reefs and shoals. (Hopley, p. 1)

16. {{ cite web|author=Fodor's|title=Great Barrier Reef Travel
    Guide|url=<http://www.fodors.com/miniguides/mgresults.cfm?destination=great_barrier@230&cur_section=ove>}}

17.

18. Berndt, Ronald M. (1959). *The concept of 'The Tribe' in the Western
    Desert of Australia*, Oceania, 30(2): 81-107.

19.

20.

21.

22.

23.

24. [Introduction to Gondwana Rainforests of
    Australia](http://www.bigvolcano.com.au/natural/whin.htm)

25.
26.

27. Layton, Robert (August 2001). Uluru: An Aboriginal History of Ayers
    Rock (2001 revised ed.). Canberra: Aboriginal Studies Press. ISBN
    0-85575-202-5.

28. [Ayers Rock, Uluru -
    Crystalinks](http://www.crystalinks.com/ayersrock.html)

29. [Amazing facts | Uluru-Kata Tjuta National Park | Parks
    Australia](http://www.parksaustralia.gov.au/uluru/people-place/amazing-facts.html)

30.

31.

32.

33.

34.

35.
36.

37.

38. Tomczak, Matthias & J. Stuart Godfrey: Regional Oceanography: and
    Introduction 2nd Edition. (2003). ISBN 8170353068

39.

40. [全澳天气](http://www.aubest.com/auinfo/ti-1.htm)

41. [Australian Government: Bureau of Meteorology (2005) Infoblatt El
    Niño and La Niña and Australia's
    Climate](http://www.bom.gov.au/info/leaflets/nino-nina.pdf) (PDF;
    1,8 MB)

42.

43.
44.

45. "Administrator of Norfolk Island". Australian Government
    Attorney-General's Department. Archived from the original on 6
    August 2008.

46. Miller, Gifford et al. *[Sensitivity of the Australian Monsoon to
    insolation and vegetation: Implications for human impact on
    continental moisture
    balance](http://geology.gsapubs.org/content/33/1/65.abstract)*.
    Geology Vol. 33, No. 1, pp. 65–67

47.

48.

49.

50.

51.

52. Williams, J. et al. 2001. *Biodiversity, Australia State of the
    Environment Report 2001* (Theme Report), CSIRO Publishing on behalf
    of the Department of the Environment and Heritage, Canberra. ISBN
    0-643-06749-3
    [.pdf](http://www.deh.gov.au/soe/2001/biodiversity/pubs/biodiversity.pdf)

53.

54. Savolainen, P. et al. 2004. A detailed picture of the origin of the
    Australian dingo, obtained from the study of mitochondrial DNA.
    *Proceedings of the National Academy of Sciences of the United
    States of America*. 101:12387–12390 PMID

55.

56. [Effects of fire on plants and animals of a Florida
    Wetland](http://people.eku.edu/sumithrans/Migrat/READINGS/Vogl.PDF)

57. Flannery, T. (1994) "The future eaters" Reed Books Melbourne.

58. Wilson, B., S. Boulter, et al. (2000). Queensland's resources.
    Native Vegetation Management in Queensland. S. L. Boulter, B. A.
    Wilson, J. Westrupet eds. Brisbane, Department of Natural Resources.

59. White, M. E. 1986. The Greening of Gondwana. Reed Books, Frenchs
    Forest, Australia.

60.

61.

62.