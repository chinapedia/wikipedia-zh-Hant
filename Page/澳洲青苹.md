**澳洲青苹**(Granny
Smith)是一种原产于[澳大利亚的](../Page/澳大利亚.md "wikilink")[苹果](../Page/苹果.md "wikilink")。它是在1868年在澳大利亚一次无意中由一位“老奶奶”玛丽亚·安·舍伍德·史密斯（Maria
Ann Sherwood
Smith）[繁殖的](../Page/繁殖.md "wikilink")。因此其英文名成为纪念这位**史密斯老奶奶**（Granny
Smith）。它被认为是由[欧洲野苹果](../Page/欧洲野苹果.md "wikilink")（）和一般的[苹果](../Page/苹果.md "wikilink")（）授粉而产生的。如果该说法属实，那么这个品种是个杂交品种。

澳洲青苹广泛的在[新西兰栽种](../Page/新西兰.md "wikilink")。在1935年，它被引入到[英国](../Page/英国.md "wikilink")；在1972年，被[格拉迪·奥维尔引入到](../Page/格拉迪·奥维尔.md "wikilink")[美国](../Page/美国.md "wikilink")。

澳洲青苹具有浅绿色并有斑点的外观，虽然某些也会有粉红色。它们是脆的，多汁的和酸的苹果，因此很适合于烹调和直接食用。它们还成为制作[沙律的优先选择](../Page/沙律.md "wikilink")，因为切下来的果肉不会像其他品种那样很快变成褐色。

该栽培品种不需要长的低温环境，但是比较长的生长季节来保证果实成熟。因此，它比较适合在气候温和的苹果栽种地区引种。

[澳洲青苹节每年都会在澳洲青苹的家乡](../Page/澳洲青苹节.md "wikilink")，[新南威尔士的](../Page/新南威尔士.md "wikilink")[伊斯特伍德举行](../Page/伊斯特伍德.md "wikilink")。该地位于[悉尼市的郊区](../Page/悉尼.md "wikilink")，

澳洲青苹的果肉比其他绿色苹果要硬，因此对于戴假牙的人来说造成一定困扰。

[苹果唱片的商标上画的苹果就是澳洲青苹](../Page/苹果唱片公司.md "wikilink")。
很多配音演员也会通过咀嚼和食用澳洲青苹来减少他们语音中的咔嗒声。

[File:GreenApple.png|澳洲青苹](File:GreenApple.png%7C澳洲青苹)
[File:Granny_Smith.jpg|玛丽亚·安·舍伍德·史密斯（左](File:Granny_Smith.jpg%7C玛丽亚·安·舍伍德·史密斯（左)）(1799-1870)

## 参考文献

  - ["史密斯老奶奶和她的苹果"
    莱德·康松](https://web.archive.org/web/20070811001112/http://www.ryde.nsw.gov.au/ryde/msherwood.htm)

[Category:苹果栽培品种](../Category/苹果栽培品种.md "wikilink")