**澳門特別行政區2005年度勳章、獎章和獎狀名單**於2005年12月19日由行政長官簽署行政命令，在《[澳門特別行政區政府公報](../Page/澳門特別行政區政府公報.md "wikilink")》公布，共有32名社會人士和8個團體和機構，表揚他們在個人成就、社會貢獻或服務澳門特別行政區方面的傑出表現。而頒授典禮於2006年1月20日下午在[澳門文化中心大劇院舉行](../Page/澳門文化中心.md "wikilink")。

## 榮譽勳章

### 金蓮花榮譽勳章

  - [唐志堅](../Page/唐志堅.md "wikilink")

### 銀蓮花榮譽勳章

  - [張祖奇](../Page/張祖奇.md "wikilink")
  - [羅榮](../Page/羅榮.md "wikilink")

## 功績勳章

### 專業功績勳章

  - [白彼德](../Page/白彼德.md "wikilink")
  - [陳曉筠](../Page/陳曉筠.md "wikilink")

### 工商功績勳章

  - [何桂鈴](../Page/何桂鈴.md "wikilink")
  - [冼志耀](../Page/冼志耀.md "wikilink")
  - [崔煜林](../Page/崔煜林.md "wikilink")
  - [蕭婉儀](../Page/蕭婉儀.md "wikilink")

### 旅遊功績勳章

  - [Andrew Wakter
    Stow](../Page/Andrew_Wakter_Stow.md "wikilink")（[安德魯餅店始創人](../Page/安德魯餅店.md "wikilink")）

### 教育功績勳章

  - [李碧沂修女](../Page/李碧沂.md "wikilink")
  - [區天香](../Page/區天香.md "wikilink")
  - [澳門中華教育會](../Page/澳門中華教育會.md "wikilink")

### 文化功績勳章

  - [呂碩基神父](../Page/呂碩基.md "wikilink")
  - [湯梅笑](../Page/湯梅笑.md "wikilink")

### 仁愛功績勳章

  - [澳門清安醫所慈善會](../Page/澳門清安醫所慈善會.md "wikilink")
  - [澳門童軍總會](../Page/澳門童軍總會.md "wikilink")

### 體育功績勳章

  - [曾鐵明](../Page/曾鐵明.md "wikilink")
  - [黃燕慧](../Page/黃燕慧.md "wikilink")
  - [鄭穎乾](../Page/鄭穎乾.md "wikilink")

## 傑出服務獎章

### 英勇獎章

  - 南亞地震[中國國際救援隊澳門志願醫療分隊](../Page/中國國際救援隊.md "wikilink")
  - [澳門海關知識產權廳](../Page/澳門海關.md "wikilink")

### 勞績獎章

  - [鄭國強](../Page/鄭國強.md "wikilink")

## 獎狀

### 榮譽獎狀

  - [Alan John Hackett](../Page/Alan_John_Hackett.md "wikilink")
  - [Saba Payman](../Page/Saba_Payman.md "wikilink")

### 功績獎狀

  - [王穎璋](../Page/王穎璋.md "wikilink")
  - [秦志堅](../Page/秦志堅.md "wikilink")
  - [區偉雄](../Page/區偉雄.md "wikilink")
  - [張志遠](../Page/張志遠.md "wikilink")，[2005年東亞運動會](../Page/2005年東亞運動會.md "wikilink")[空手道金牌得主](../Page/空手道.md "wikilink")
  - [陳梓俊](../Page/陳梓俊.md "wikilink")，第三十八屆世界職業技能競賽網頁設計項目(WEB Design)銅獎得主
  - [麥沛然](../Page/麥沛然.md "wikilink")
  - [黃響麟](../Page/黃響麟.md "wikilink")
  - [楊厚芹](../Page/楊厚芹.md "wikilink")，[2005年東亞運動會舉重金牌得主](../Page/2005年東亞運動會.md "wikilink")
  - [賈瑞](../Page/賈瑞_\(武術運動員\).md "wikilink")，[2005年東亞運動會武術金牌得主](../Page/2005年東亞運動會.md "wikilink")
  - [賈嘉慧](../Page/賈嘉慧.md "wikilink")，[2005年東亞運動會空手道金牌得主](../Page/2005年東亞運動會.md "wikilink")
  - [鍾家立](../Page/鍾家立.md "wikilink")，[2005年亞洲室內運動會金牌得主](../Page/2005年亞洲室內運動會.md "wikilink")
  - [關淑梅](../Page/關淑梅.md "wikilink")，[2005年亞洲室內運動會金牌得主](../Page/2005年亞洲室內運動會.md "wikilink")
  - 第四屆東亞運動會澳門龍舟女子小龍代表隊
  - 第四屆東亞運動會澳門龍舟男子小龍代表隊
  - 第十一屆[亞洲雪屐曲棍球錦標賽澳門雪屐曲棍球代表隊](../Page/亞洲雪屐曲棍球錦標賽.md "wikilink")

[Category:澳門頒授勳章、獎章和獎狀名單](../Category/澳門頒授勳章、獎章和獎狀名單.md "wikilink")