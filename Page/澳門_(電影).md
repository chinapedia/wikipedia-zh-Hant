**澳門
(Macao)**是一套1952年拍成的[黑白歷奇電影](../Page/黑白.md "wikilink")。這部電影的拍攝地點包括[香港和](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")。在拍攝期間，監製[霍華德·休斯辭去原本的導演](../Page/霍華德·休斯.md "wikilink")[約瑟夫·馮·斯登堡而請了](../Page/約瑟夫·馮·斯登堡.md "wikilink")[尼古拉斯·雷來完成之](../Page/尼古拉斯·雷.md "wikilink")。

## 故事簡介

三個互不認識的人一同來到了港口城市[澳門](../Page/澳門.md "wikilink")。Nick是一個憤世嫉俗的但對人很真誠的[旅行家](../Page/旅行家.md "wikilink");
Julie是一個淫蕩的[夜總會女星](../Page/夜總會.md "wikilink");
Lawrence是一個絲綢和[走私貨品](../Page/走私.md "wikilink")[推銷員](../Page/推銷員.md "wikilink")。而他們三人就是乘坐同一艘船來澳。

## 演員陣容

  - [罗伯特·米彻姆](../Page/罗伯特·米彻姆.md "wikilink") 飾演 Nick Cochran
  - [Jane Russell](../Page/Jane_Russell.md "wikilink") 飾演 Julie Benson
  - [William Bendix](../Page/William_Bendix.md "wikilink") 飾演 Lawrence
    C. Trumble
  - [Thomas Gomez](../Page/Thomas_Gomez.md "wikilink") 飾演 Lt. Sebastian
  - [Gloria Grahame](../Page/Gloria_Grahame.md "wikilink") 飾演 Margie
  - [Brad Dexter](../Page/Brad_Dexter.md "wikilink") 飾演 Vincent Halloran

## 參看

  - [香港電影](../Page/香港電影.md "wikilink")

## 外部連結

  -
[Category:1952年电影](../Category/1952年电影.md "wikilink")
[Category:澳門電影](../Category/澳門電影.md "wikilink")
[M](../Category/澳門背景電影.md "wikilink")