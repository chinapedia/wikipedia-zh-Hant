澳門的交通工具主要由陸路、水路和航空交通所組成。澳門佔地雖小，但其陸路交通的道路網絡和其他大城市一樣建有[公路](../Page/公路.md "wikilink")、[橋樑和](../Page/橋.md "wikilink")[隧道等](../Page/隧道.md "wikilink")。由於道路設計問題，交通擠塞和汽車流量過大等都是存在已久的交通問題。

澳門截至目前依舊沒有[鐵路](../Page/鐵路.md "wikilink")，惟[澳門政府目前正在興建](../Page/澳門政府.md "wikilink")[澳門輕軌](../Page/澳門輕軌.md "wikilink")[路氹段](../Page/路氹.md "wikilink")，預計最快2019年首期路綫通車。澳門的陸路交通以[公車](../Page/公車.md "wikilink")、[計程車](../Page/計程車.md "wikilink")、[私家車和](../Page/私家車.md "wikilink")[電單車為主](../Page/電單車.md "wikilink")，另外富有特色的人力三輪車仍在路上行駛。

## 歷史

澳門早期的代步工具是中國傳統的[轎和葡式軟轎](../Page/轎.md "wikilink")()，但對當時一般人來說這只是社會名流的象徵。[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，收費相宜的[人力車出現](../Page/人力車.md "wikilink")，成爲了主流交通工具。當[單車流行後](../Page/單車.md "wikilink")，三輪車應運而生。三輪車大約在1948年引入澳門，不久即取代人力車及單車成為澳門的主要交通工具。三輪車的高峰時期，全澳有超過七百輛，三輪車站亦近百。

直到1925年，澳門出現街坊車（即早期的[公共汽車](../Page/公共汽車.md "wikilink")）。昔日有兩條主要的行車路線，一條是從火船頭街至[媽閣廟](../Page/媽閣廟.md "wikilink")，另一條則由[十月初五街至](../Page/十月初五街.md "wikilink")[關閘](../Page/關閘.md "wikilink")。1927年，[岐關公司在澳門設立並包辦全澳門的公車服務](../Page/岐關車路有限公司.md "wikilink")，1948年由福利巴士（[新福利巴士前身](../Page/澳門新福利公共汽車有限公司.md "wikilink")）接辦，1974年10月，[嘉樂庇總督大橋正式通車](../Page/嘉樂庇總督大橋.md "wikilink")，[澳門半島](../Page/澳門半島.md "wikilink")、[氹仔島](../Page/氹仔島.md "wikilink")、[路環島交通連成一片](../Page/路環島.md "wikilink")，1975年1月，福利巴士公司更從[英國購入的](../Page/英國.md "wikilink")[雙層巴士正式行走於](../Page/雙層巴士.md "wikilink")[澳門半島與離島之間](../Page/澳門半島.md "wikilink")。

## 陸路運輸

澳门行驶道路总长度为287.31km，道路设计分：大马路（主干道）、一般马路和服务性马路。

## 道路

### 巴士

[Macau_bus_50814.jpg](https://zh.wikipedia.org/wiki/File:Macau_bus_50814.jpg "fig:Macau_bus_50814.jpg")

[澳門現在共有兩家公共巴士業者](../Page/澳門.md "wikilink")，分別為[澳門新福利公共汽車有限公司](../Page/澳門新福利公共汽車有限公司.md "wikilink")（簡稱新福利）及[澳門公共汽車有限公司](../Page/澳門公共汽車有限公司.md "wikilink")（簡稱澳巴），大部份路線服務時間由上午6時至凌晨0時，亦有七條夜間公車路線，於凌晨0時至6時期間行駛。

兩家巴士業者車費統一，由原本2008年12月起，每程收費由[澳門幣](../Page/澳門幣.md "wikilink")2.8圓至6.4圓不等，澳門市區線為3.2圓，澳門氹仔線為4.2圓，澳門路環線為5.0圓，澳門黑沙線為6.4圓，氹仔或路環區內為2.8圓。乘客使用澳門通卡付費有折扣優惠以至路線轉乘，澳門市區、氹仔或路環區內線一律為2.0圓，澳門氹仔線為2.5圓，至於澳門路環及黑沙線為3.0圓，亦有數條免費接駁路線，主要在澳門賽車期間運行。在2018年5月，全澳巴士車費統一至6.0圓，因有澳門政府資助公交出行，使用澳門通收費為3.0/4.0(X字尾)圓。

澳门的娱乐场及部分酒店亦提供从[关闸边检大楼往返娱乐场或酒店的免费穿梭巴士](../Page/关闸边检大楼.md "wikilink")。该种巴士不在中途另外设置站点。

### 計程車

[Macau-Taxi.jpg](https://zh.wikipedia.org/wiki/File:Macau-Taxi.jpg "fig:Macau-Taxi.jpg")
[澳門的士顏色為黑色](../Page/澳門.md "wikilink")，載客量由4人至6人不等，但大多數為4人。收費首1600米19圓，以後每260米一跳收費2圓，停車候客每分鐘收費2圓，如有大件行李，每件加收3圓，由澳門半島或氹仔前往路環會收取附加費，由[澳門氹仔客運碼頭](../Page/澳門氹仔客運碼頭.md "wikilink")、[路氹邊檢大樓](../Page/路氹邊檢大樓.md "wikilink")、[澳門國際機場](../Page/澳門國際機場.md "wikilink")、[澳門大學橫琴校區和](../Page/澳門大學橫琴校區.md "wikilink")[港珠澳大橋澳門邊檢大樓計程車站上車加收](../Page/珠澳口岸人工島.md "wikilink")5圓附加費。

### 人力三輪車

[3wheelscar.jpg](https://zh.wikipedia.org/wiki/File:3wheelscar.jpg "fig:3wheelscar.jpg")
人力三輪車是澳門富特色的交通工具，在[外港碼頭和](../Page/外港碼頭.md "wikilink")[葡京酒店處設有人力車站](../Page/葡京酒店.md "wikilink")。通常每小時包車費為[澳門幣](../Page/澳門幣.md "wikilink")150圓，由[澳門](../Page/澳門.md "wikilink")[外港客運碼頭到](../Page/外港客運碼頭.md "wikilink")[觀音像約](../Page/觀音像.md "wikilink")40圓、[澳門旅遊塔](../Page/澳門旅遊塔.md "wikilink")70圓、[議事亭前地](../Page/議事亭前地.md "wikilink")80圓、到[觀音堂或](../Page/觀音堂.md "wikilink")[媽閣廟約](../Page/媽閣廟.md "wikilink")100圓。

### 私人車輛

## 鐵路運輸

[Macau_Light_Rapid_Transit_Map.png](https://zh.wikipedia.org/wiki/File:Macau_Light_Rapid_Transit_Map.png "fig:Macau_Light_Rapid_Transit_Map.png")

### 重型鐵路

直至2018年，[澳門仍未有任何](../Page/澳門.md "wikilink")[鐵路運輸及](../Page/鐵路運輸.md "wikilink")[軌道運輸系統服務](../Page/軌道運輸系統.md "wikilink")，是全[中國唯一沒有](../Page/中國.md "wikilink")[鐵路的](../Page/鐵路.md "wikilink")[一級行政區](../Page/中國一級行政區.md "wikilink")。但[廣珠城軌](../Page/廣珠城軌.md "wikilink")[珠海站與澳門僅一關之隔](../Page/珠海站.md "wikilink")。目前可直通[廣州南站](../Page/廣州南站.md "wikilink")、[北京和](../Page/北京西站.md "wikilink")[桂林](../Page/桂林北站.md "wikilink")。

### 捷運系統

[澳門特區政府目前正在興建](../Page/澳門特區政府.md "wikilink")[澳門輕軌系統第一期路線](../Page/澳門輕軌系統.md "wikilink")，由[關閘](../Page/關閘.md "wikilink")，經[黑沙環](../Page/黑沙環.md "wikilink")、[外港客運碼頭](../Page/外港客運碼頭.md "wikilink")、[新口岸](../Page/新口岸.md "wikilink")、[南灣](../Page/南灣_\(澳門\).md "wikilink")、[西灣](../Page/西灣_\(澳門\).md "wikilink")、[氹仔](../Page/氹仔.md "wikilink")、[路氹城至](../Page/路氹城.md "wikilink")[澳門國際機場及](../Page/澳門國際機場.md "wikilink")[氹仔客運碼頭](../Page/氹仔客運碼頭.md "wikilink")，其中目前正在施工的為氹仔段的[海洋站至](../Page/海洋站.md "wikilink")[氹仔碼頭站](../Page/氹仔碼頭站.md "wikilink")，預計在2019年通車。

### 松山纜車

[Stationmacau.JPG](https://zh.wikipedia.org/wiki/File:Stationmacau.JPG "fig:Stationmacau.JPG")
[松山纜車](../Page/松山纜車.md "wikilink")，全長僅186公尺，是全球最短的[纜車](../Page/索道.md "wikilink")。[松山纜車由](../Page/松山纜車.md "wikilink")[二龍喉公園往返](../Page/二龍喉公園.md "wikilink")[東望洋山](../Page/東望洋山.md "wikilink")(松山)，由民政總署管理和營運，作為觀光性質之用，遊客可在[纜車內飽覽](../Page/纜車.md "wikilink")[東望洋山一帶的景色](../Page/東望洋山.md "wikilink")。[纜車票價](../Page/索道.md "wikilink")：單程[澳門幣](../Page/澳門幣.md "wikilink")2圓，來回3圓。開放時間為07:30至18:30(每周一停駛)。

**永利皇宮觀光纜車**

**[永利皇宮觀光纜車](../Page/永利皇宮.md "wikilink")**是一個環繞八英畝大表演湖的纜車，纜車全程只有約8至10分鐘，纜車全長679米，離地28米高，設34個車廂，可載6人，乘客可欣賞表演湖的表演。[纜車票價](../Page/索道.md "wikilink")：免費。

## 水路運輸

[从东望洋山顶望向友谊大桥.JPG](https://zh.wikipedia.org/wiki/File:从东望洋山顶望向友谊大桥.JPG "fig:从东望洋山顶望向友谊大桥.JPG")
[20091105-TurboJET_Urzela.jpg](https://zh.wikipedia.org/wiki/File:20091105-TurboJET_Urzela.jpg "fig:20091105-TurboJET_Urzela.jpg")

### 客運

現有澳門有兩間船公司提供來往[港](../Page/香港.md "wikilink")[澳兩地之航班](../Page/澳門.md "wikilink")，分別由[噴射飛航和](../Page/噴射飛航.md "wikilink")[金光飛航提供](../Page/金光飛航.md "wikilink")，從[新口岸](../Page/新口岸.md "wikilink")[外港客運碼頭或](../Page/外港客運碼頭.md "wikilink")[氹仔客運碼頭往來](../Page/氹仔客運碼頭.md "wikilink")[香港](../Page/香港.md "wikilink")[上環](../Page/上環.md "wikilink")[港澳客輪碼頭](../Page/港澳客輪碼頭.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")[中國客運碼頭](../Page/中國客運碼頭.md "wikilink")、[香港國際機場](../Page/香港國際機場.md "wikilink")[海天客運碼頭及](../Page/海天客運碼頭.md "wikilink")[屯門碼頭](../Page/屯門碼頭.md "wikilink")。

另外，[噴射飛航和粵通船務於](../Page/噴射飛航.md "wikilink")[外港客運碼頭](../Page/外港客運碼頭.md "wikilink")、[氹仔客運碼頭和](../Page/氹仔客運碼頭.md "wikilink")[內港客運碼頭](../Page/內港客運碼頭.md "wikilink")，有航班往來[中國](../Page/中國.md "wikilink")[內地](../Page/內地.md "wikilink")，分別前往[珠海市](../Page/珠海市.md "wikilink")[灣仔](../Page/灣仔街道.md "wikilink")、[廣州市](../Page/廣州市.md "wikilink")[南沙](../Page/南沙.md "wikilink")、[深圳市蛇口及福永和](../Page/深圳市.md "wikilink")[江門市](../Page/江門市.md "wikilink")。

[澳門的三個主要港口](../Page/澳門.md "wikilink")：

  - [內港](../Page/澳門內港碼頭.md "wikilink")：位於[澳門半島的西面](../Page/澳門.md "wikilink")，一水道之隔面對[珠海市](../Page/珠海市.md "wikilink")[灣仔](../Page/灣仔街道.md "wikilink")，為主要的內河航運的碼頭。
  - 外港：即[外港客運碼頭](../Page/外港客運碼頭.md "wikilink")，位於[澳門半島東面](../Page/澳門半島.md "wikilink")，水塘對出。
  - [北安](../Page/北安.md "wikilink")：即[氹仔客運碼頭](../Page/氹仔客運碼頭.md "wikilink")，位於北安，[澳門國際機場旁邊](../Page/澳門國際機場.md "wikilink")。

### 貨運

  - [內港](../Page/內港.md "wikilink")：澳港貨櫃碼頭。
  - 九澳貨櫃碼頭(亦稱深水港)：位於[路環島的東北面](../Page/路環.md "wikilink")，為一個深水港，現為澳門貨櫃碼頭的所在地。

## 航空運輸

澳門航空交通主要為飛機及直升機兩種。

### 飛機

[Macao_airport_terminal.jpg](https://zh.wikipedia.org/wiki/File:Macao_airport_terminal.jpg "fig:Macao_airport_terminal.jpg")

[澳門國際機場位於](../Page/澳門國際機場.md "wikilink")[氹仔島東面](../Page/氹仔島.md "wikilink")，1995年投入使用，跑道總長3360米。

  - 參見
    [澳門國際機場網頁](https://web.archive.org/web/20100129035126/http://www.macau-airport.gov.mo/)

### 直昇機

[HK_Sheung_Wan_Shun_Tak_Centre_HK-Macau_Ferry_Terminal_Heliport.JPG](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_Shun_Tak_Centre_HK-Macau_Ferry_Terminal_Heliport.JPG "fig:HK_Sheung_Wan_Shun_Tak_Centre_HK-Macau_Ferry_Terminal_Heliport.JPG")

由[空中快線直昇機有限公司提供服務從](../Page/空中快線直昇機有限公司.md "wikilink")[澳門](../Page/澳門.md "wikilink")[外港客運碼頭來往](../Page/外港客運碼頭.md "wikilink")[香港](../Page/香港.md "wikilink")[上環](../Page/上環.md "wikilink")[港澳客輪碼頭和](../Page/港澳客輪碼頭.md "wikilink")[深圳寶安國際機場](../Page/深圳寶安國際機場.md "wikilink")，航程大約為15至20分鐘。

## 参考文献

## 外部連結

### 政府部門

  - [澳門特別行政區政府交通事務局網頁](http://www.dsat.gov.mo)
  - [澳門特別行政區政府民航局網頁](http://www.aacm.gov.mo)
  - [澳門特別行政區政府海事及水務局網頁](http://www.marine.gov.mo)

### 航空公司

  - [澳門航空網頁](http://www.airmacau.com.mo)
  - [Hell Express 空中快線網頁](http://www.heliexpress.com/)

### 船公司

  - [噴射飛航網頁](http://www.turbojet.com.hk)
  - [金光飛航網頁](https://web.archive.org/web/20140315014443/http://www.cotaijet.com.mo/)

### 巴士公司

  - [澳門新福利公共汽車有限公司網頁](http://www.transmac.com.mo)
  - [澳門公共汽車有限公司網頁](http://www.tcm.com.mo)
  - [澳門新時代公共汽車股份有限公司網頁](http://www.newera.com.mo)

{{-}}

[澳門交通](../Category/澳門交通.md "wikilink")