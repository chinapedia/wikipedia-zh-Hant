[Broadway_Macau_The_Broadway_Food_Street_Night_view_201606.jpg](https://zh.wikipedia.org/wiki/File:Broadway_Macau_The_Broadway_Food_Street_Night_view_201606.jpg "fig:Broadway_Macau_The_Broadway_Food_Street_Night_view_201606.jpg")

**澳門百老匯**（**Broadway
Macau**）是位於[澳門](../Page/澳門.md "wikilink")[路氹城的一個附設](../Page/路氹城.md "wikilink")[賭場及](../Page/賭場.md "wikilink")[舞台的娛樂項目](../Page/舞台.md "wikilink")，由[銀河娛樂場股份有限公司持有及營運](../Page/銀河娛樂場股份有限公司.md "wikilink")，己於2015年5月27日開幕。

## 歷史

銀河娛樂場股份有限公司是澳門開放[博彩業牌照的最初三個牌照持有人之一](../Page/澳門博彩業.md "wikilink")（其他兩個為[澳門博彩股份有限公司及](../Page/澳門博彩股份有限公司.md "wikilink")[永利渡假村(澳門)股份有限公司](../Page/永利渡假村\(澳門\)股份有限公司.md "wikilink")），其後將牌照轉批給[威尼斯人(澳門)股份有限公司](../Page/威尼斯人\(澳門\)股份有限公司.md "wikilink")。威尼斯人集團決定開發新填海區[路氹城成為新一代澳門博彩旅遊集中地後](../Page/路氹城.md "wikilink")，多間集團紛紛到該處地段發展豪華渡假村及賭場。銀河亦於該地段發展其大型渡假村項目，並於毗鄰之地段（即[皇庭海景酒店對面](../Page/澳門皇庭海景酒店.md "wikilink")）發展另一賭場酒店項目，亦即澳門百老匯的前身－澳門金都酒店、金都娛樂城。

2015年1月，管理項目的[銀河娛樂宣布將原有的](../Page/銀河娛樂.md "wikilink")**金都娛樂城**（**Grand
Waldo**）項目重新改造成全新娛樂項目，並名為**澳門百老匯**，同時宣布該項目於同年5月27日開幕。項目以[百老匯劇院作為主題](../Page/百老匯劇院.md "wikilink")，並設有40間澳門本地及亞洲地道食肆、裝置3,000個層疊式座位的場館、飽覽180度全景的會所式娛樂場及一間改造後的酒店。項目有空調系統的行人天橋連接[澳門銀河](../Page/澳門銀河.md "wikilink")。

[File:grandwaldo.jpg|金都酒店時期的酒店大門](File:grandwaldo.jpg%7C金都酒店時期的酒店大門)
<File:Macau> Ferry Pier Grand Waldo Hotel Shuttle Bus.JPG|金都酒店穿梭小巴
<File:Broadway> Macau Broadway Theatre Lobby 2016.jpg|百老匯劇院大堂
<File:Broadway> Macau Access to Galaxy Macau
2016.jpg|連接[澳門銀河的行人天橋](../Page/澳門銀河.md "wikilink")

## 特色項目

前身的金都酒店主要由三幢建築物組成，於2006年5月22日正式開幕，分別為設有317間客房的酒店大樓，娛樂場大樓以及休閒設施大樓。酒店大樓與娛樂場以天橋相接，娛樂場樓高3層，面積約600,000平方呎，設有168張中西式賭檯，而娛樂場大樓頂層則設有空中花園供客人休憩之用。

休閒設施大樓則樓高6層，設有水療坊、桑拿、夜總會、[哥爾夫球練習場以及小型電影院等設施](../Page/哥爾夫球.md "wikilink")。到2018年7月開設國際知名虛擬實境遊戲體驗館Zero
Latency，佔地2,000呎。

金都酒店共設有9間中西式食肆，為客人提供餐飲服務。

## 鄰近建築／景點

## 相關條目

  - [銀河總統娛樂場](../Page/銀河總統娛樂場.md "wikilink")
  - [銀河娛樂場股份有限公司](../Page/銀河娛樂場股份有限公司.md "wikilink")
  - [結好控股](../Page/結好控股.md "wikilink")

## 外部連結

  - [澳門百老匯官方網站](https://web.archive.org/web/20150127044304/http://broadwaymacau.com.mo/zh-hant/opening/)

[澳](../Category/澳門酒店.md "wikilink") [銀](../Category/澳門賭場.md "wikilink")
[Category:嘉華國際](../Category/嘉華國際.md "wikilink")