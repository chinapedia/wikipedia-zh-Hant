[Hotel_Taipa.JPG](https://zh.wikipedia.org/wiki/File:Hotel_Taipa.JPG "fig:Hotel_Taipa.JPG")
**澳門盛世酒店**（）是位於[氹仔市區的具有南歐](../Page/氹仔.md "wikilink")[葡萄牙風情的主題酒店](../Page/葡萄牙.md "wikilink")，17層高，落成時有261間客房、48個車位及4層商場，總樓面約25萬方呎。

## 介紹

澳門盛世酒店前身為**格蘭酒店**（），酒店當時由[招商局置業興建](../Page/招商局置業.md "wikilink")，並於2005年1月以2.42億元售予[華大地產](../Page/華大地產.md "wikilink")，平均呎價968元，其後於2005年2月18日開業，由華美達酒店負責經營。
華大地產於2013年12月將酒店以9億港元售予[英皇娛樂酒店有限公司](../Page/英皇娛樂酒店有限公司.md "wikilink")，其後於2014年交予[新昌管理集團作翻新](../Page/新昌管理集團.md "wikilink")、加建及改建並易名為澳門盛世酒店。
酒店原設有兩個提供私人宴會及會議服務的功能廳“花蘆”及“波爾圖”，並設有[摩卡角子機娛樂場提供](../Page/摩卡角子機娛樂場.md "wikilink")136台角子老虎機，現已關閉及改建為客房。
酒店設有免費穿梭巴士往返英皇娛樂酒店，並於英皇娛樂酒店轉乘至其他地點。 此外，酒店亦設有一個室外游泳池，計劃於2016年重新開放。

## 附近

澳門盛世酒店毗鄰[氹仔舊城區](../Page/氹仔.md "wikilink")，鄰近[澳門八景之一](../Page/澳門八景.md "wikilink")
-
[龍環葡韻住宅式博物館](../Page/龍環葡韻住宅式博物館.md "wikilink")、[氹仔](../Page/氹仔.md "wikilink")[官也街](../Page/官也街.md "wikilink")、[地堡街](../Page/地堡街.md "wikilink")、[嘉模聖母堂](../Page/嘉模聖母堂.md "wikilink")、[澳門賽馬會氹仔馬場以及](../Page/澳門賽馬會.md "wikilink")[金光大道度假中心](../Page/金光大道.md "wikilink")。

## 外部連結

[澳門盛世酒店](http://macau.innhotel.com)

## 參考

  - [華大2.42億買澳門酒店](http://hk.apple.nextmedia.com/financeestate/art/20050106/4566480/)
  - [英皇娛樂酒店9億買澳門格蘭酒店](http://www.the-sun.on.cc/cnt/finance/20131221/00434_030.html/)
  - [新昌管理集團公司概況](http://www.quamnet.com/companySearchDetail.action?coId=962/)

[Category:澳門酒店](../Category/澳門酒店.md "wikilink")
[Category:英皇集團](../Category/英皇集團.md "wikilink")