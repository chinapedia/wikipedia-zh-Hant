[MUST_Stadium.jpg](https://zh.wikipedia.org/wiki/File:MUST_Stadium.jpg "fig:MUST_Stadium.jpg")
[Estádio_de_Universidade_de_Ciência_e_Tecnologia_de_Macau.JPG](https://zh.wikipedia.org/wiki/File:Estádio_de_Universidade_de_Ciência_e_Tecnologia_de_Macau.JPG "fig:Estádio_de_Universidade_de_Ciência_e_Tecnologia_de_Macau.JPG")
**澳門科技大學體育館**（，）位於[澳門科技大學校園內](../Page/澳門科技大學.md "wikilink")、[澳門科技大學運動場之側](../Page/澳門科技大學運動場.md "wikilink")，是澳門科技大學第二個具國際水平的體育設施。

## 簡介

澳門科技大學體育館佔地11400[平方米](../Page/平方米.md "wikilink")，由[第二屆亞洲室內運動會澳門組織委員會所設計](../Page/第二屆亞洲室內運動會澳門組織委員會.md "wikilink")，可容納約1500人。此外，運動場可舉辦各種各樣的室內球賽，且設有[極限運動及](../Page/極限運動.md "wikilink")20米高的攀石牆。而戶外的廣場也可供公眾休憩\[1\]。

## 興建過程

體育館於2004年年中開始動工，主要為了[2007年亞洲室內運動會的舉辦而興建](../Page/2007年亞洲室內運動會.md "wikilink")，並進行趕工，務求在[2007年亞洲室內運動會的舉辦前一個月完工](../Page/2007年亞洲室內運動會.md "wikilink")，可惜未能如願。

澳門科技大學體育館最終於2007年10月20日啟用\[2\]，並於10月20及21日為2007年亞洲室內運動會進行試運轉（即測試賽）\[3\]，成為2007年亞洲室內運動會的其中一個體育場館，但至今仍未舉行任何啟用儀式或開幕禮。

## 管理權

澳門科技大學體育館現屬[第二屆亞洲室內運動會組織委員會股份有限公司](../Page/第二屆亞洲室內運動會組織委員會股份有限公司.md "wikilink")[固定資產](../Page/固定資產.md "wikilink")，第二屆亞洲室內運動會組織委員會股份有限公司於2008年1月1日清算及結算後\[4\]
，體育館將會可能按[澳門科技大學運動場作為先例](../Page/澳門科技大學運動場.md "wikilink")，將可能移交至[澳門科技大學或](../Page/澳門科技大學.md "wikilink")[體育發展局管理](../Page/體育發展局.md "wikilink")。

## 先進技術

澳門科技大學體育館有三項高新科技產品，實現了在頒奬和升旗的同時全自動化、計分系統化，更設有LED大屛幕顯示系統作現場直播賽事，令澳門科技大學體育館以及澳門的運動場館的設備更為科技化。

[北京華體聯合科技有限公司是為隸屬於](../Page/北京華體聯合科技有限公司.md "wikilink")[中華人民共和國](../Page/中華人民共和國.md "wikilink")[國家體育總局的科研開發公司](../Page/國家體育總局.md "wikilink")，為澳門科技大學體育館開發及採用最新資訊技術的LED大屛幕顯示系統（LED大屛幕有兩個，因電子項目可以令賽會及與會人士作判斷有否違背規則。）。LED大屛幕顯示系統整合了和[奧米加](../Page/奧米加.md "wikilink")（Omega）記分計時系統為一體。LED大屛幕顯示系統除比賽文字資訊外，聲音、畫面等綜合資訊同時顯現

第三代頒奬升旗系統是中國首個採用的頒奬升旗系統，令[國旗上升與](../Page/國旗.md "wikilink")[國歌奏播時配合得宜](../Page/國歌.md "wikilink")，避免及消除了“國旗升到頂、國歌還在奏”的尷尬情況出現。

記分計時系統由原先的電子化系統向網絡化系統作出進步，即由過往單純由人手計時，進而至今計時結果與大屛幕即時顯示。\[5\]。

## 曾辦盛事

  - [2007年亞洲室內運動會](../Page/2007年亞洲室內運動會.md "wikilink")（[電子競技項目](../Page/電子競技.md "wikilink")）

## 參考資料

<div class="references-small">

<references />

</div>

[Category:澳門運動場地](../Category/澳門運動場地.md "wikilink")

1.  [工程四大難題逐個擊破](http://space.qoos.com/?action-viewnews-itemid-148787) -
    《[澳門日報](../Page/澳門日報.md "wikilink")》 2007年10月12日
2.  [科大體育館啟用](http://space.qoos.com/?action-viewnews-itemid-151045) 澳門日報
    2007年10月20日 於2007年11月10日造訪
3.  [科大館下周試運轉](http://space.qoos.com/?action-viewnews-itemid-147925)
    澳門日報 2007年10月9日 於2007年11月10日造訪
4.  [第五章-第十九條
    解散和清算](http://www.safp.gov.mo/legismac-orgtxt/2004/S1/2004_20/JASCMC.htm)
    第二屆亞洲室內運動會澳門組織委員會股份有限公司章程 於2007年11月10日造訪
5.  [亞室運高新科技產品推介](http://space.qoos.com/?action-viewnews-itemid-154835)，《[澳門日報](../Page/澳門日報.md "wikilink")》，2007年11月3日