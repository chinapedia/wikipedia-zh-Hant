[Macau_oldmap.jpg](https://zh.wikipedia.org/wiki/File:Macau_oldmap.jpg "fig:Macau_oldmap.jpg")

**澳門舊城**，又稱**澳門城**、**澳門街**、**澳城**、**天主聖名之城**（），是指[澳門發展早期在](../Page/澳門.md "wikilink")[葡萄牙人治理之下](../Page/葡萄牙人.md "wikilink")，逐漸由海澳發展為城市的地區，為澳門[市區最早的組成部分](../Page/市區.md "wikilink")。2005年，澳門舊城建築遺跡以「[澳門歷史城區](../Page/澳門歷史城區.md "wikilink")」之名列入[世界文化遺產](../Page/世界文化遺產.md "wikilink")。

## 沿革

[Macaulighthousecastle.JPG](https://zh.wikipedia.org/wiki/File:Macaulighthousecastle.JPG "fig:Macaulighthousecastle.JPG")的部分城牆與暸望台\]\]
最早期的澳門只有[村落](../Page/村落.md "wikilink")，沒有[城市](../Page/城市.md "wikilink")。而澳門舊城的建設可追溯至[明朝](../Page/明朝.md "wikilink")，葡萄牙人在澳門正式定居，並修築永久性的建築物開始（1557年—1605年）。當時，葡萄牙人在內港北灣至下灣一帶（即現今的[營地大街一帶](../Page/營地大街.md "wikilink")），築成了俗稱「澳門街」的一條狹長[商業兼](../Page/商業.md "wikilink")[住宅區](../Page/住宅.md "wikilink")。

[澳葡當局開始在澳門建築城牆的時間](../Page/澳門議事會.md "wikilink")，最早可追溯至明朝[隆慶三年](../Page/隆慶.md "wikilink")(即1569年)之前，不過其後被明朝政府勒令拆毀。直至1617年，澳葡當局的官員利用[賄賂中國官吏的方式](../Page/賄賂.md "wikilink")，成功再次築起城墻。1622年[荷蘭人入侵澳門](../Page/荷蘭共和國.md "wikilink")，葡萄牙人便以抵禦為理由築起一系列城牆。明[天啟三年](../Page/天啟.md "wikilink")（即1623年），葡萄牙人命荷蘭俘虜聯同[西班牙士兵和](../Page/西班牙.md "wikilink")[華人共同進行防禦工事](../Page/華人.md "wikilink")，初築城牆以炮台為中心向兩旁伸展。同年經明政府官員強力干涉，北部城墻曾被拆毀。及至1632年，澳門北部城牆及炮台又復建完成。實際上，葡萄牙人在明朝傾覆與[清朝初立](../Page/清朝.md "wikilink")（1644年）至[南明滅亡](../Page/南明.md "wikilink")（1661年）的內亂期間，仍繼續建築澳門城墻，最後在此築成了頗具規模的城鎮。據當時的澳門城市圖可見，整個澳門城的北部、東部及南部均築有城墻，而要塞更建置了大小不一的炮台。此時的澳門城，已成為軍事嚴密的[城堡](../Page/城堡.md "wikilink")，成為葡萄牙統治澳門的象徵。

清[道光二十九年](../Page/道光.md "wikilink")（1849年），葡萄牙人在[澳門總督](../Page/澳門總督.md "wikilink")[亞馬留的指揮下驅逐城內的清政府官員](../Page/亞馬留.md "wikilink")，並拆毀[望廈村的](../Page/望廈村.md "wikilink")[香山縣縣丞署](../Page/香山縣.md "wikilink")。自此以後，澳門城完全由葡萄牙控制。及後，葡萄牙為擴張澳門的管理範圍，將城墻陸續拆毀。隨時代變遷，澳門城墻的作用亦隨社會的發展已逐漸消失和拆卸，現存的[舊城牆遺址便是昔日澳門城之一部分](../Page/舊城牆遺址.md "wikilink")。

## 舊城牆與城門

[Na_Tcha_Temple.JPG](https://zh.wikipedia.org/wiki/File:Na_Tcha_Temple.JPG "fig:Na_Tcha_Temple.JPG")

據《澳門地理》描述昔日的城牆：“其牆所經，自今[東望洋山頂天文臺西側起](../Page/東望洋山.md "wikilink")，下山，經[水坑尾細開巷](../Page/水坑尾.md "wikilink")，上[大炮臺](../Page/大炮臺.md "wikilink")，連接大炮臺城，複下山，再接慈幼院，北繞白鴿花園，經[大三巴圍營地之西](../Page/大三巴.md "wikilink")，南通天街經[窗門街過](../Page/紅窗門街.md "wikilink")[萬里長城](../Page/媽閣斜巷.md "wikilink")，[媽閣廟之東北背](../Page/媽閣廟.md "wikilink")、繞[西望洋山城至海濱](../Page/西望洋山.md "wikilink")，足見葡人當時所占只限于三巴門城南之地，至今牆界，尚斷續可尋。”至今，城牆只餘下小部分遺址。

《[香山縣誌](../Page/香山縣誌.md "wikilink")》記載了城牆與城門：「夷所居地，西北枕山，高建圍牆，東南倚水為界。小門三：曰小三巴門，曰沙梨頭門，曰花王廟門，今俱塞；大門三：曰三巴門，曰水坑尾門，曰新開門；炮臺六，最大為三巴炮臺，臺冠山椒，列炮四十七，銅具十六、餘鐵，上宿蕃兵，下為窟室，貯焰硝……」可見早期城牆設三道主要[城門](../Page/城門.md "wikilink")，分別是三巴門、水坑門和新開門。三巴門在澳門以北，其上的[大炮臺有葡萄牙軍隊駐守](../Page/大炮臺.md "wikilink")。到清[乾隆年間](../Page/乾隆.md "wikilink")，葡萄牙人已將澳門城分四道城門，分別是[大三巴門](../Page/大三巴.md "wikilink")、[小三巴門](../Page/三巴仔.md "wikilink")、[沙梨頭門和](../Page/沙梨頭.md "wikilink")[花王廟門](../Page/花王堂區.md "wikilink")。現在，昔日的城門已不復存在了。

## 相關連結

  - [澳門炮台](../Page/澳門炮台.md "wikilink")
  - [澳門歷史](../Page/澳門歷史.md "wikilink")

## 參見

  - [九龍寨城](../Page/九龍寨城.md "wikilink")

## 參考來源

  - [南方網：澳門城的三次變遷](http://www.southcn.com/news/hktwma/twmil/200212170744.htm)
  - [澳門地理暨教育研究會：澳門地理概況](https://web.archive.org/web/20070305141953/http://www.macauphp.net/aiegm/macau.htm)
  - 《澳門掌故》，澳門教育出版社，1999年12月。ISBN 972-97840-1-9

## 外部連結

  - [澳門文物網：舊城牆遺址](https://web.archive.org/web/20070927003937/http://www.macauheritage.net/info/HTextC.asp?id=63)
  - [预告网：澳门回归纪念节日](http://www.foreshow.com/37/2006421144226.htm)

[Category:澳門歷史城區](../Category/澳門歷史城區.md "wikilink")
[Category:澳門歷史](../Category/澳門歷史.md "wikilink")
[Category:澳門地理](../Category/澳門地理.md "wikilink")