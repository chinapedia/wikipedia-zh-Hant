**澳門車輛登記號碼**，是[澳門的](../Page/澳門.md "wikilink")[汽車牌號](../Page/汽車牌號.md "wikilink")（俗稱：汽車牌號「[車牌](../Page/車牌.md "wikilink")」或「車牌號碼」）。牌照在回歸前是由[澳門市政廳負責發出](../Page/澳門市政廳.md "wikilink")，及後由[臨時澳門市政局發出](../Page/臨時澳門市政局.md "wikilink")；2001年[元旦起](../Page/元旦.md "wikilink")，由新組成的[民政總署負責發出](../Page/民政總署.md "wikilink")，及至2008年5月13日，澳門政府成立了[交通事務局](../Page/交通事務局.md "wikilink")，因而現時由[交通事務局負責](../Page/交通事務局.md "wikilink")。

## 簡述

在澳門行走的車輛需要在車頭及車尾各展示一張汽車牌號。牌面由塑料製成，使用黑底白字（此外，免稅車輛採用黃字），首尾車牌同樣，長闊介乎8至11[公分之間](../Page/公分.md "wikilink")。現在的標準於1960年代起使用。而澳門車輛註冊號牌格式標準則類似[葡萄牙車輛註冊號牌](../Page/:en:Portuguese_car_number_plates.md "wikilink")（1937年至1992年款式）。

## 登記號碼編碼系統

### 新車

新車的試車車牌分為兩種，一種是**ES**車牌，而另一種是**EX**車牌；**ES**車牌一般只用於車行未出售的新車，其車牌為白底紅字，並不提供予用家；而**EX**車牌則是正式試車牌，其車牌為紅底白字，意思是「本車尚未註冊」。

### 民用及商業車輛

[MCNP-MK1931.png](https://zh.wikipedia.org/wiki/File:MCNP-MK1931.png "fig:MCNP-MK1931.png")
[MCNP-M1000.png](https://zh.wikipedia.org/wiki/File:MCNP-M1000.png "fig:MCNP-M1000.png")
[China_cross-border_Guangdong-Macau_registration_plate_粤Z_D100澳.jpg](https://zh.wikipedia.org/wiki/File:China_cross-border_Guangdong-Macau_registration_plate_粤Z_D100澳.jpg "fig:China_cross-border_Guangdong-Macau_registration_plate_粤Z_D100澳.jpg")[澳兩地車輛所使用的車牌](../Page/澳門.md "wikilink")。下方：澳門典型車牌。圖中所示的M-10-00澳門車輛登記號碼是由拍賣而得的車輛登記號碼。\]\]

民用及商業車輛的登記號碼是澳門的[葡萄牙文Macau的首個字元](../Page/葡萄牙文.md "wikilink")「**M**」字加以一個英文字母配以一組4位數字(1000\~9999)，英文字母之後，及每2個數字就會有一個「**-**」的符號。民用車輛的使用黑底白字，免稅車輛則使用黑底黃字，包括巴士、旅遊車或其他具備免稅條件之企業或團體的車輛。公共巴士服務自新公交經營模式後,所有巴士首兩年需繳稅,兩年後免稅，並由政府退回首兩年稅予營運公司。

最早發出的登記號碼只得「**M**」字母，由1000發出至9999，然後是「**MA**」字頭(MA-10-00\~MA-99-99)，接著是「**MB**」字頭，如此類推。舊式的澳門車牌是不反光的突字車牌，1998年開始(大約出到「MH」字頭)新發出的澳門車牌採用平面的反光車牌。

一些特別號碼例如「**M\*-88-88**」、「**M-10-00**」也被抽起，供交通事務局作拍賣使用。

2015年7月29日，澳門的汽車牌號不使用「**MV**」字頭，在完成發給「**MU**」組別的汽車註冊編號後，將直接使用「**MW**」組別的汽車註冊編號。
另外，電單車版方面，交通事務局在完成發給「**MP**」車牌後，將直接使用「**MR**」車牌，而「**MQ**」字頭不使用，原因不明。
\[1\]

截至2018年11月，澳門的汽車牌號已發出至「**MY**」字頭。

#### 電單車

[Macaumopedlicence.png](https://zh.wikipedia.org/wiki/File:Macaumopedlicence.png "fig:Macaumopedlicence.png")的登記號碼\]\]
澳門[電單車的登記號碼與一般車輛分開發出](../Page/電單車.md "wikilink")，並分為2種包括重型電單車(50cc以上)以及輕型電單車(50cc或以下)。

重型電單車的登記號碼編碼系統與[民用及商業車輛的方法相同](../Page/澳門車輛登記號碼#民用及商業車輛.md "wikilink")。而輕型電單車的登記號碼編碼系統是以「**CM**」作開頭（即[葡文Ciclomotor的簡寫](../Page/葡文.md "wikilink")），然後配以一組1\~5位數字(1\~99999)(1\~9999比較少會見到)，與前者不同，輕型電單車的車牌為白底黑字的。

而至2017年11月，澳門的輕型電單車車牌號已發出至「**CM86**」字頭。

### 特別車輛

[China_cross-border_Guangdong-Macau_license_plate_粤Z-A123澳.png](https://zh.wikipedia.org/wiki/File:China_cross-border_Guangdong-Macau_license_plate_粤Z-A123澳.png "fig:China_cross-border_Guangdong-Macau_license_plate_粤Z-A123澳.png")

#### 政府車輛

[MCNP-MC0319.png](https://zh.wikipedia.org/wiki/File:MCNP-MC0319.png "fig:MCNP-MC0319.png")

澳門政府各部門所用的車輛與[民用及商業車輛的方法一樣](../Page/澳門車輛登記號碼#民用及商業車輛.md "wikilink")，只是澳門政府的車輛所配以的一組4位數字是由0001\~0999的，因此在澳門街道上見到車牌號碼中的首個數字為「0」的必定是為政府車輛。此外，各政府車輛也會在車牌的側面掛上一個[鵝蛋型狀的塑膠牌寫上所屬的](../Page/鵝蛋.md "wikilink")[政府部門](../Page/政府部門.md "wikilink")(但[中央人民政府駐澳機構](../Page/中央人民政府.md "wikilink")，如[中央人民政府駐澳門特別行政區聯絡辦公室](../Page/中央人民政府駐澳門特別行政區聯絡辦公室.md "wikilink")、外交部駐澳門特派專員公署等，配有「MH-0x-xx」或「MW-0x-XX」的車牌號碼，但沒有「鵝蛋牌」，同時有關機構的車輛亦配有來往中澳兩地的「粵Z」車牌。有趣的是，上述有關車輛的澳門車牌和「粵Z」車牌的號碼是完全一致的)。另外，亦有部分政府車輛採用MW-0字頭車牌，但暫時未知相關部門名稱，而該等車牌已發出10個；常見的是[民政總署的MC字頭車牌](../Page/民政總署.md "wikilink")。而民政總署的車牌字頭都是M？-01字頭的。至於政府電單車的號碼分配方法也是同上(輕型電單車除外,因為輕型電單車的分配方法是用數字分配，不會另外配給，但必定有[鵝蛋型狀的塑膠牌](../Page/鵝蛋.md "wikilink")。

#### 內地車

使用CN字頭車牌，格式為「CN-xx-xx」。

#### 香港車

與普通號碼一樣。

透过往來港澳（市內）常規配額入境澳門的香港私家車無須掛上澳門車牌，只需在車上張貼澳門發出的電子標籤、識別標誌及車輛邊境通行證。

  - 新一批常規配額入境澳門的香港私家車須掛上HK字頭車牌，格式為「HK-XX-XX」,取得澳门特区政府豁免权的香港车辆可以仅悬挂[香港車輛號牌进入澳门](../Page/香港車輛號牌.md "wikilink")。

#### 駐澳部隊車輛

[Macau_license_plate_ZA0001.png](https://zh.wikipedia.org/wiki/File:Macau_license_plate_ZA0001.png "fig:Macau_license_plate_ZA0001.png")

由1999年12月20日中午開始，在
[中國人民解放軍駐澳門部隊進駐的車輛車輛登記號碼開首字母為](../Page/中國人民解放軍駐澳門部隊.md "wikilink")「**ZA**」，為普通話**Z**hu**A**o
(駐澳)
的[漢語拼音縮寫](../Page/漢語拼音.md "wikilink")，然後配以「0」字以及3個數字，可以算是民用及商業車輛與政府車輛的特別版本。

[駐澳解放軍所以使用的ZA車牌](../Page/駐澳解放軍.md "wikilink")，字體及車牌大小參照[中國大陸汽車牌號格式](../Page/中國大陸汽車牌號.md "wikilink")，採用西德字體，與澳門一般車牌的字體和車牌大小不同；但顏色及字元格式則按照澳門車牌規格，顏色黑底白字，字元格式「ZA-nn-nn」（n可為[阿拉伯數字](../Page/阿拉伯數字.md "wikilink")0\~9）。而該等車牌不受[交通事務局管理](../Page/交通事務局.md "wikilink")。

#### 中澳車牌

領有澳門車牌的車輛可申領「粵Z××××澳」號牌，俗稱「中澳車牌」，掛有這類號牌之車輛可經各個口岸來往澳門和廣東省。

#### 港澳車牌

隨者[港珠澳大橋於](../Page/港珠澳大橋.md "wikilink")2018年10月24日啟用，港澳車牌概念也因而出現，但是不是像中澳車牌那樣出現，而是同時懸掛香港及澳門車牌，且配額遠小於中澳車牌。例如[穿梭巴士](../Page/穿梭巴士.md "wikilink")（[港珠澳大橋線](../Page/港珠澳大橋穿梭巴士.md "wikilink")）就會掛上這類車牌。

港珠澳大橋澳門口岸泊車轉乘計劃：

  - 擁有「過境車輛封閉道路通行許可證」，但是不能進入澳門境內，必須要停在口岸停車場（東停車場），再轉乘公共交通入市區。
  - 而包括駕駛員座位在內，座位超過九個；總重量超過3.5公噸；高度超過2公尺；少於四輪的機動或非機動車輛等未經過允許不可停東停車場。
  - 停車時間，是以時段為單位（每時段180澳門元，超過時段者270元），每日分為2個時段，每時段為12小時。每次停車最少為1個時段，最多為16個時段(即192小時)，但每次停泊只可進出澳門停車場一次，且只限指定司機所使用車輛，其他人不得駕駛。

## 特別編碼

### 澳督座駕

[GM_vehicle.jpg](https://zh.wikipedia.org/wiki/File:GM_vehicle.jpg "fig:GM_vehicle.jpg")

[澳葡政府時](../Page/澳葡政府.md "wikilink")，[澳門總督座駕不會掛上一般車牌](../Page/澳門總督.md "wikilink")，而是掛上一個鑲嵌有GM（葡萄牙文的簡寫）字樣，兩字之間是[澳門盾徽的一個車牌](../Page/澳門盾徽.md "wikilink")。

### 主教公署座駕

天主教主教公署座駕除掛上一般車牌外，還會掛上鑲嵌有PE字樣的專屬識別徽號。PE代表Paço Episcopal，即主教公署。

### 現已知特別車牌

  - **MA-00-01,MB-00-01**：[行政長官專用座駕](../Page/澳門行政長官.md "wikilink")
  - **M-00-01**
    ：[全國政協副主席](../Page/全國政協副主席.md "wikilink")[何厚鏵專用座駕](../Page/何厚鏵.md "wikilink")
  - **MB-08-88**：行政法務司司長專用座駕
  - **MC-03-88**：[社會文化司司長座駕](../Page/社會文化司司長.md "wikilink")
  - **MC-03-79**：[保安司司長座駕](../Page/保安司司長.md "wikilink")
  - **MA-99-99**：[何鴻燊私人座駕](../Page/何鴻燊.md "wikilink")

## 短期性特別編碼

### [澳門主權移交](../Page/澳門主權移交.md "wikilink")

[Vehicle1999.jpg](https://zh.wikipedia.org/wiki/File:Vehicle1999.jpg "fig:Vehicle1999.jpg")
在[澳門主權移交前後期間](../Page/澳門主權移交.md "wikilink")，葡、中雙方官員在各個儀式場館間的交通安排，由澳葡政府發出的特別車牌。牌面是綠底黃字。

### [澳門東亞運動會](../Page/澳門東亞運動會.md "wikilink")

[MCNP-MEAGOC128.png](https://zh.wikipedia.org/wiki/File:MCNP-MEAGOC128.png "fig:MCNP-MEAGOC128.png")
[PB010046.JPG](https://zh.wikipedia.org/wiki/File:PB010046.JPG "fig:PB010046.JPG")

為了配合澳門東亞運動會的成功舉辦以及方便各貴賓在澳門在任何地方上落的安排，[治安警察局交通廳聯同](../Page/治安警察局.md "wikilink")2005年東亞運動會澳門組織委員會達成協議，在2005年10月25日至11月9日期間特設有250部[寶馬房車](../Page/寶馬.md "wikilink")，分別以1至250為「車輛登記號碼」。牌面是白底紅字，並由橘色的幼邊包圍著。在「車輛登記號碼」的左面印有澳門東亞運的會徽，而右下角則寫著「MEAGOC」的字樣，即2005年東亞運動會澳門組織委員會的英文簡寫。值得一提的是這種車輛登記號碼在澳門是歷史上的首次之外，也是**首次採用**[香港車輛登記號碼的反光的物料製成的](../Page/香港車輛登記號碼.md "wikilink")。

### [澳門葡語系運動會](../Page/2006年葡語系運動會.md "wikilink")

[Macau_license_plate_Jogos_da_Lusofonia_2006_168.png](https://zh.wikipedia.org/wiki/File:Macau_license_plate_Jogos_da_Lusofonia_2006_168.png "fig:Macau_license_plate_Jogos_da_Lusofonia_2006_168.png")

為了配合首屆葡語系運動會的成功舉辦以及方便各貴賓在澳門在任何地方上落的安排，[治安警察局交通廳聯同](../Page/治安警察局.md "wikilink")2006年葡語系運動會澳門籌備委員會達成協議，在2006年10月2日至10月20日期間特設有[康達](../Page/康達.md "wikilink")、[平治房車共](../Page/梅塞德斯-朋馳.md "wikilink")350部，分別以1至350為「車輛登記號碼」。牌面是白底紅字，並由綠色的幼邊包圍著。在「車輛登記號碼」的左面印有[葡語系奧林匹克委員會總會的會徽](../Page/葡語系奧林匹克委員會總會.md "wikilink")，而右面則印有[2006年葡語系運動會的會徽](../Page/2006年葡語系運動會.md "wikilink")。車輛登記號碼的物料也是採用反光的物料製成的。

### [澳門亞洲室內運動會](../Page/2007年亞洲室內運動會.md "wikilink")

[Macao_license_plate_MAIGOC_326.png](https://zh.wikipedia.org/wiki/File:Macao_license_plate_MAIGOC_326.png "fig:Macao_license_plate_MAIGOC_326.png")
[MACAU_AIG_cars.jpg](https://zh.wikipedia.org/wiki/File:MACAU_AIG_cars.jpg "fig:MACAU_AIG_cars.jpg")

為了配合第二屆亞洲室內運動會的成功舉辦以及方便各貴賓在澳門在任何地方上落的安排，[治安警察局交通廳聯同](../Page/治安警察局.md "wikilink")2007年亞洲室內運動會澳門組織委員會達成協議，在2007年10月20日至11月6日期間特設有多種品牌房車超過500部，分別以由1起為數目為「車輛登記號碼」。牌面是白底紅字，在「車輛登記號碼」的左面印有[亞奧理事會會徽](../Page/亞奧理事會.md "wikilink")。車輛登記號碼的物料也是採用反光的物料製成的。

## 其他

  - 由於內地與澳門的距離近，因此有車輛會時常到內地行走，那些車輛則需要掛上[中國大陸汽車牌號](../Page/中國大陸汽車牌號.md "wikilink")（粵Z牌）
  - “T車牌”：暫時制度
  - 三輪車車牌：在過去以澳門市政廳的簡稱“LS”為起首。隨着市政機構被裁撤，如今改以“澳門”和“MACAU”標示，大概有宣傳澳門旅遊形象的用意。
  - 工業機器車號牌：綠底黃字，以“MI”為起首
  - 古董車號牌：藍底白字的H字牌，在市面並不常見。

## 參見

  - [中華人民共和國車輛號牌](../Page/中華人民共和國車輛號牌.md "wikilink")
      - [香港車輛號牌](../Page/香港車輛號牌.md "wikilink")
  - [交通事務局](../Page/交通事務局.md "wikilink")

## 参考文献

## 外部連結

  - [交通事務局](http://www.dsat.gov.mo)
  - [车牌号查询](http://www.dsat.gov.mo/car/status.aspx?language=c)

{{-}}

[Category:中华人民共和国车辆号牌](../Category/中华人民共和国车辆号牌.md "wikilink")
[Category:澳門交通](../Category/澳門交通.md "wikilink")

1.