**激活素**（，亦成为**激活蛋白**或**活化素**）与**抑制素**（）是一种用于抑制[卵泡刺激素分泌的与组合的](../Page/卵泡刺激素.md "wikilink")[梭氨酸](../Page/梭氨酸.md "wikilink")\[1\]
。参与在[月经周期的调节中](../Page/月经周期.md "wikilink")。

## 结构

抑制素包括一个由[二硫键链接起来的α和β亚单位](../Page/二硫键.md "wikilink")。两种抑制素不同于他们的β亚单位上（A或者B），虽然他们的α亚单位相同。

抑制素属于[转化生长因子-β超家族之内](../Page/转化生长因子-β超家族.md "wikilink")。

## 行为

### 女性中

在女性中，在[卵巢里](../Page/卵巢.md "wikilink")，卵泡刺激素刺激卵泡中[粒层细胞的抑制素分泌](../Page/粒层细胞.md "wikilink")，随之，抑制素抑制了卵泡刺激素。

[促性腺激素释放激素](../Page/促性腺激素释放激素.md "wikilink")（GnRH）控制抑制素的减，[胰岛素样生长因子-1](../Page/胰岛素样生长因子-1.md "wikilink")（IGF-1）控制抑制素的增。

  - *抑制素B*在[卵泡早期到卵泡中期间达到了第一个波峰](../Page/卵泡期.md "wikilink")，于[排卵时达到了第二个波峰](../Page/排卵.md "wikilink")。
  - *抑制素A*在[黄体中期达到波峰](../Page/黄体期.md "wikilink")

抑制素由[性腺](../Page/性腺.md "wikilink")，[脑垂体](../Page/脑垂体.md "wikilink")，[胎盘和其他器官分泌](../Page/胎盘.md "wikilink")。

### 男性中

在男性中，这是一种用于抑制卵泡刺激素的激素。

他由位于[睾丸的](../Page/睾丸.md "wikilink")[细精管中的](../Page/细精管.md "wikilink")[塞尔托利氏细胞分泌](../Page/塞尔托利氏细胞.md "wikilink")\[2\]。

## 活化素

[活化素是与抑制素完全相反的一种缩氨酸](../Page/活化素.md "wikilink")。

## 临床重要性

在通过婴儿出生前的[四屏测试](../Page/四屏.md "wikilink")，抑制素A的量在孕妇怀孕16-18周的时候可以被发现。高升的抑制素（连同增加的[β-人体绒膜促性腺激素](../Page/β-人体绒膜促性腺激素.md "wikilink")，降低的[甲胎蛋白](../Page/甲胎蛋白.md "wikilink")，和降低的[雌三醇](../Page/雌三醇.md "wikilink")）
\[3\]
启动了胎儿的[唐氏综合征](../Page/唐氏综合征.md "wikilink")。就像筛选试验一样，反常的四屏测试得出的结果也需要进一步的验证。

它也是一种患得了[卵巢癌的标志](../Page/卵巢癌.md "wikilink")\[4\]\[5\]。

## 参考资料

[Category:生殖系统](../Category/生殖系统.md "wikilink")
[Category:卵巢激素](../Category/卵巢激素.md "wikilink")
[Category:肽类激素](../Category/肽类激素.md "wikilink")
[Category:TGFβ域](../Category/TGFβ域.md "wikilink")

1.
2.
3.
4.
5.