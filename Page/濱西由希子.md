**濱西由希子**(Yukiko Hamanishi ，)，[A型](../Page/A型.md "wikilink")
[金牛座](../Page/金牛座.md "wikilink")

[日本](../Page/日本.md "wikilink")[花式撞球職業選手](../Page/花式撞球.md "wikilink")，[日本](../Page/日本.md "wikilink")[職業](../Page/職業.md "wikilink")[撞球](../Page/撞球.md "wikilink")[協會](../Page/協會.md "wikilink")(JPBA)32期生
封號「氣質美女」。

2008年8月嫁同為[日本](../Page/日本.md "wikilink")[職業](../Page/職業.md "wikilink")[撞球](../Page/撞球.md "wikilink")[選手大井直幸](../Page/選手.md "wikilink")。

2009年以冠夫姓出賽，是為**大井由希子**（Yukiko Ohi）

## 年度總冠軍賽

### 2007女子職業撞球大賽

|        |                                    |                                  |                                                                   |
| ------ | ---------------------------------- | -------------------------------- | ----------------------------------------------------------------- |
| **站別** | **冠軍**                             | **亞軍**                           | **季軍**                                                            |
| 一      | [柳信美](../Page/柳信美.md "wikilink")   | [賴慧珊](../Page/賴慧珊.md "wikilink") | [林沅君](../Page/林沅君.md "wikilink")、濱西由希子（）                          |
| 二      | [金佳映](../Page/金佳映.md "wikilink")（） | [柳信美](../Page/柳信美.md "wikilink") | 周婕妤、Amit Rubilen（）                                                |
| 三      | [林沅君](../Page/林沅君.md "wikilink")   | 林倍君                              | [柳信美](../Page/柳信美.md "wikilink")、[賴慧珊](../Page/賴慧珊.md "wikilink") |
| 總冠軍賽   | [金佳映](../Page/金佳映.md "wikilink")（） | [蔡佩真](../Page/蔡佩真.md "wikilink") | [張舒涵](../Page/張舒涵.md "wikilink")、[柳信美](../Page/柳信美.md "wikilink") |

### 2008女子職業撞球大賽

|        |                                  |                                    |                                                                     |
| ------ | -------------------------------- | ---------------------------------- | ------------------------------------------------------------------- |
| **站別** | **冠軍**                           | **亞軍**                             | **季軍**                                                              |
| 一      | [蔡佩真](../Page/蔡佩真.md "wikilink") | [柳信美](../Page/柳信美.md "wikilink")   | [范瑞芳](../Page/范瑞芳.md "wikilink")、[周婕妤](../Page/周婕妤.md "wikilink")   |
| 二      | [周婕妤](../Page/周婕妤.md "wikilink") | [金佳映](../Page/金佳映.md "wikilink")（） | [柳信美](../Page/柳信美.md "wikilink")、濱西由希子（）                            |
| 三      | [譚湘玲](../Page/譚湘玲.md "wikilink") | [高淑品](../Page/高淑品.md "wikilink")   | [林沅君](../Page/林沅君.md "wikilink")、[林苗儀](../Page/林苗儀.md "wikilink")   |
| 總冠軍賽   | [周婕妤](../Page/周婕妤.md "wikilink") | [林沅君](../Page/林沅君.md "wikilink")   | [蔡佩真](../Page/蔡佩真.md "wikilink")、[金佳映](../Page/金佳映.md "wikilink")（） |

## 成績

2003年
[亞洲花式撞球錦標賽女子](../Page/亞洲.md "wikilink")[亞軍](../Page/亞軍.md "wikilink")

2005年
第7屆[德國](../Page/德國.md "wikilink")[杜伊斯堡](../Page/杜伊斯堡.md "wikilink")[世界運動會女子](../Page/世界運動會.md "wikilink")9號球第5名

2006年 [安麗盃世界女子花式撞球錦標賽第](../Page/安麗盃.md "wikilink")5名

2007年
[安麗盃世界女子花式撞球錦標賽](../Page/安麗盃.md "wikilink")[季軍](../Page/季軍.md "wikilink")

2007年
[緯來](../Page/緯來.md "wikilink")[2007女子職業撞球大賽第一站](../Page/2007女子職業撞球大賽.md "wikilink")[季軍](../Page/季軍.md "wikilink")

2007年
[澳門](../Page/澳門.md "wikilink")[亞洲室內運動會女子](../Page/亞洲室內運動會.md "wikilink")8號球第4名

2008年 [安麗盃世界女子花式撞球錦標賽第](../Page/安麗盃.md "wikilink")9名

2008年
[日本](../Page/日本.md "wikilink")[九州女子公開賽](../Page/九州.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")

2008年
全[日本女子職業巡迴賽第](../Page/日本.md "wikilink")3站[季軍](../Page/季軍.md "wikilink")

2008年
全[日本女子職業巡迴賽第](../Page/日本.md "wikilink")4站[冠軍](../Page/冠軍.md "wikilink")

2008年
[緯來](../Page/緯來.md "wikilink")[2008女子職業撞球大賽第](../Page/2008女子職業撞球大賽.md "wikilink")2站[季軍](../Page/季軍.md "wikilink")

2009年
全[日本女子職業巡迴賽第](../Page/日本.md "wikilink")2站[季軍](../Page/季軍.md "wikilink")

2009年
[安麗盃世界女子](../Page/安麗盃.md "wikilink")[花式撞球](../Page/花式撞球.md "wikilink")[公開賽第](../Page/公開賽.md "wikilink")9名

2009年
全[日本女子職業巡迴賽第](../Page/日本.md "wikilink")3站[冠軍](../Page/冠軍.md "wikilink")

2010年
全[日本女子職業巡迴賽第一站](../Page/日本.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")

2010年
[東海女子](../Page/東海.md "wikilink")[9號球](../Page/9號球.md "wikilink")[公開賽](../Page/公開賽.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")

[Category:日本花式撞球運動員](../Category/日本花式撞球運動員.md "wikilink")