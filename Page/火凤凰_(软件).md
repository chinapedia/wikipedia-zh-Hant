**火鳳凰**，英文名**FirePhoenix**，原為[美國HeartyIT](../Page/美國.md "wikilink")（漢帝）公司開發的一套[破網軟體](../Page/破網軟體.md "wikilink")，已停产并失效。現在被轉移到[World
Gate名下](../Page/World_Gate.md "wikilink")。官方已停止此软件的更新维护，相关技术已转移至[自由门和](../Page/自由门.md "wikilink")[无界浏览](../Page/无界浏览.md "wikilink")。

## 軟體介紹

  - 使用[虚拟私人网络](../Page/虚拟私人网络.md "wikilink")(Virtual Private
    Network，简称VPN）技术。
  - 隱藏用戶[IP地址](../Page/IP地址.md "wikilink")，使服務提供者不能通過用戶的IP地址，得到用戶的地理位置或身份。他們自己在網站上羅列了其技術特性，例如IP透明包裹技術，自主版權的動態信息加密技術和自主版權的IP指紋驗證技術。
  - 採用業界最高[加密等級處理訊息](../Page/加密.md "wikilink")，核心數據加密算法被各政府、軍事、金融等行業廣泛採用。

和以往其他幾個突破封鎖的軟件採用代理技術不同，火鳳凰採用增加一塊虛擬網卡的方法實現加密，用戶感覺增加了一塊海外的網卡，其技術很類似前段時間日本的[SoftEther](../Page/SoftEther.md "wikilink")。安裝過程，會安裝一塊網卡。因為所有的網絡訊息，都是在通過這塊虛擬網卡加密後傳輸。

  - 支持所有的網絡協議，所有需要上網的軟件，都可以被安全的保護起來，例如[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")、[Mozilla
    Firefox和](../Page/Mozilla_Firefox.md "wikilink")[MSN
    Messenger等等](../Page/MSN_Messenger.md "wikilink")。

在接受[自由亞洲電台的採訪的時候](../Page/自由亞洲電台.md "wikilink")，他們的開發人員火鳥(FireBird)宣佈他們的產品會一直免費向中國大陸用戶開放使用。有人建议謹慎使用，因为其推出時間尚短，技術還在磨合期，可能在使用過程中出現問題。

## 外部連結

  - [火鳳凰官方網站](http://hfh.edoors.com/)
      - [簡體中文版首頁](http://hfh.edoors.com/index-cn.html)
  - [火凤凰交流专版](https://web.archive.org/web/20070627112153/http://www.qxbbs.org/viewforum.php?f=32&sid=8919e76c9617249f87af815b9efcff66)：官方技术支持讨论版，测试版也在此发布。

[Category:破网软件](../Category/破网软件.md "wikilink")
[Category:软件小作品](../Category/软件小作品.md "wikilink")