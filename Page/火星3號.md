**火星3號**（Марс-3）是[蘇聯在](../Page/蘇聯.md "wikilink")1971年發射的[火星探測計畫的探測船](../Page/火星探測.md "wikilink")。於5月28日[協調世界時](../Page/協調世界時.md "wikilink")15時26分20秒由[質子-K/D組級運載火箭發射](../Page/質子-K/D組級運載火箭.md "wikilink")，於在1971年12月成功登陸在火星地面。火星3號與[火星2號是同一系列的探測船](../Page/火星2號.md "wikilink")，都擁有1組軌道船與登陸艇。火星2號於12月27日到達火星後，登陆艇于火星表面撞毁，轨道船继续工作了8个月。而火星3號与火星2號的軌道器工作到次年8月22日宣布退役，但是火星3號的著陸器卻成為了有史以來第一個成功在火星表面著陸的探測器，雖然它僅僅火星上工作了大約14秒，甚至沒能發回一張完整的照片就永遠與地球失去了通信聯繫。

2013年4月11日，[NASA宣布在](../Page/NASA.md "wikilink")[火星侦察轨道器上搭載的](../Page/火星侦察轨道器.md "wikilink")[高解析度成像科學設備拍攝的影像中可能發現了火星](../Page/高解析度成像科學設備.md "wikilink")3號的登陸地點，影像中可能拍攝到了火星3號的[降落傘](../Page/降落傘.md "wikilink")、[減速火箭](../Page/減速火箭.md "wikilink")、[防熱盾和登陸艇](../Page/防熱盾.md "wikilink")\[1\]
。

## 概述

## 相關條目

  - [火星1A號](../Page/火星1A號.md "wikilink")
  - [火星探测](../Page/火星探测.md "wikilink")
  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")

## 參考資料

## 外部連結

  - [NASA's mars probe
    website](http://nssdc.gsfc.nasa.gov/planetary/planets/marspage.html)
  - [Ted Stryk's page on the Mars 3
    Probe](https://web.archive.org/web/20071005010633/http://www.strykfoto.org/mars3.htm)
  - [TASS notice on the Mars-3 landing (in
    Russian)](../Page/s:ru:Сообщение_ТАСС_о_посадке_автоматической_станции_Марс-3.md "wikilink")
    ([Wikisource](../Page/Wikisource.md "wikilink"))

[Category:火星探測器](../Category/火星探測器.md "wikilink")
[Category:蘇聯太空探測器](../Category/蘇聯太空探測器.md "wikilink")

1.