**火流星**是與[流星和](../Page/流星.md "wikilink")[隕石有關的一個術語](../Page/隕石.md "wikilink")。因此，使用這個術語的團體和單位，對於這個名詞的定義尚未完全達成共識。

對於火流星的描述，一個依據[視星等的定義是亮度達到](../Page/視星等.md "wikilink")-14等或更亮的火球\[1\]。另一個定義是任何可以撞擊形成大火山口的天體，而無須知道它的成分（例如，是否是一顆岩石或金屬的小行星，還是冰彗星）\[2\]。
[Bolide.jpg](https://zh.wikipedia.org/wiki/File:Bolide.jpg "fig:Bolide.jpg")

*bolide*這個字源自[希臘文的βολίς](../Page/希臘語.md "wikilink")
*bolis*，他的意思是*飛彈*\[3\]。

## 天文學

[IAU尚未對](../Page/國際天文學聯合會.md "wikilink")*火流星*做正式的定義，一般認為這個字與"火球"是同義詞。然而，通常適用於視星等達到-14等或更亮的流星\[4\]。天文學家傾向於使用**火流星**來標示特別明亮的火球，特別是有爆炸的（有時稱為爆炸火球）。它也可以用在發出聲音的火球。

### 超級火流星

如果一顆火流星的[視星等達到](../Page/視星等.md "wikilink")-17等或更亮，它就被稱為*超級火流星*\[5\]\[6\]。

## 地質學

[地質學家比](../Page/地質學家.md "wikilink")[天文學家更常使用](../Page/天文學家.md "wikilink")*火流星*這個名詞；在地質上它標示出巨大的[撞擊坑](../Page/撞擊事件.md "wikilink")。例如，[美國地質調查局](../Page/美國地質調查局.md "wikilink")[伍茲霍爾海岸和海事科學中心通常使用](../Page/Woods_Hole,_Massachusetts.md "wikilink")*火流星*這個詞來描述形成巨大撞擊坑的天體，而無須知道其組成（例如，是否是一顆岩石或金屬的小行星，還是冰彗星）\[7\]。

## 參考資料

## 相關條目

  - [Tollmann's hypothetical
    bolide](../Page/Tollmann's_hypothetical_bolide.md "wikilink")
  - [車里雅賓斯克隕石](../Page/車里雅賓斯克隕石.md "wikilink")
  - [流星](../Page/流星.md "wikilink")
  - [流星體](../Page/流星體.md "wikilink")
  - [太陽系天體列表](../Page/太陽系天體列表.md "wikilink")

## 外部連結

  - [historic record of bolides that have been witnessed entering the
    Earth’s atmosphere around the world from 861
    through 2012](https://archive.is/20130703201541/http://b612foundation.org/data-visualization-bolides/)
    ([B612 Foundation](../Page/B612_Foundation.md "wikilink"))

[Category:流星體](../Category/流星體.md "wikilink")
[Category:行星地質學](../Category/行星地質學.md "wikilink")

1.  :156

2.

3.

4.
5.
6.  :133

7.