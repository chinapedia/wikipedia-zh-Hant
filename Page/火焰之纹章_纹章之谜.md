是由[Intelligent
Systems制作](../Page/Intelligent_Systems.md "wikilink")，由[任天堂于](../Page/任天堂.md "wikilink")1994年1月21日发行的[SFC平台](../Page/超級任天堂.md "wikilink")[战略角色扮演游戏](../Page/戰略角色扮演遊戲.md "wikilink")。\[1\]游戏分成前后两篇，前篇《第1部：暗黑战争编\~暗黑龙与光之剑》是系列首作《[暗黑龙与光之剑](../Page/火焰之纹章_暗黑龙与光之剑.md "wikilink")》的复刻版，后篇《第2部：英雄战争编\~纹章之谜》则是接续前篇故事情节的新作，在当时的新老玩家之间都大受好评。\[2\]本作中首次出现了支援效果这一设定，该设定也被[火焰之纹章系列沿用至今](../Page/火焰之纹章系列.md "wikilink")。

## 玩法

游戏中新增舞娘职业，她可以让一个单位在一回合内行动两次。这个职业也成为了后来的火焰之纹章系列游戏中不可缺少的一个职业。

## 反响

《[FAMI通](../Page/Fami通.md "wikilink")》给本游戏的评分是36分（满分40分）。\[3\]本作的销量约为77.6万份。\[4\]

## 参考文献

[Category:1994年电子游戏](../Category/1994年电子游戏.md "wikilink") [Category:Wii
Virtual Console游戏](../Category/Wii_Virtual_Console游戏.md "wikilink")
[Category:任天堂3DS Virtual
Console游戏](../Category/任天堂3DS_Virtual_Console游戏.md "wikilink")
[Category:日本開發電子遊戲](../Category/日本開發電子遊戲.md "wikilink")
[Category:王子主角題材作品](../Category/王子主角題材作品.md "wikilink")
[Category:超级任天堂游戏](../Category/超级任天堂游戏.md "wikilink")
[F](../Category/后传电子游戏.md "wikilink") [Monshou no
Nazo](../Category/火焰之纹章系列.md "wikilink")
[Category:战略角色扮演游戏](../Category/战略角色扮演游戏.md "wikilink")

1.

2.
3.

4.