**火牛阵**，[战国](../Page/战国.md "wikilink")[齐将](../Page/田齐.md "wikilink")[田单发明的](../Page/田單.md "wikilink")[战术](../Page/战术.md "wikilink")。

[燕昭王时](../Page/燕昭王.md "wikilink")，[燕将](../Page/燕国.md "wikilink")[乐毅](../Page/乐毅.md "wikilink")[破齐](../Page/濟西之戰.md "wikilink")，田单[坚守即墨](../Page/即墨之戰.md "wikilink")（今[山东](../Page/山东.md "wikilink")[平度](../Page/平度.md "wikilink")）。前279年，[燕惠王即位](../Page/燕惠王.md "wikilink")。田单以反間計促使燕王以騎劫取代樂毅，進而向燕军诈降，使之麻痹，又於夜間用牛千余头，牛角上缚上兵刃，尾上缚苇灌油，以火点燃，猛衝燕军，并以五千勇士随后冲杀，大败燕军，杀死骑劫。田单乘胜连克七十余城\[1\]。

## 後世使用

南宋绍興元年，平秀州水賊[邵青起事](../Page/邵青.md "wikilink")，朝廷派[王德率领大军进剿](../Page/王德.md "wikilink")。邵青采用田单火牛阵出擊。王德知邵青徵集耕牛，笑說：“是古法也，可一不可再，今不知變，此成擒耳。”\[2\]，他以靜制動，萬箭齊發，火牛受傷驚恐，反而掉頭衝入邵青陣中，踩死士兵不計其數。王德一舉敉平叛亂。淳祐元年（1241）[汪世显率蒙古军攻蜀](../Page/汪世显.md "wikilink")，制置使[陈隆之固守](../Page/陈隆之.md "wikilink")，誓与成都存亡。部将[田世显投降](../Page/田世显.md "wikilink")，汉州（今四川广汉）守将[王夔驱赶火牛突围而走](../Page/王夔.md "wikilink")。

1930年，[第一次国共内战時](../Page/第一次国共内战.md "wikilink")，[红军在长沙曾試過火牛陣](../Page/中国工农红军.md "wikilink")，成功击破[国军防线](../Page/国民革命军.md "wikilink")。[日军在](../Page/日军.md "wikilink")[侵华战争中也用过火牛阵](../Page/侵华战争.md "wikilink")，给国军造成極大傷亡。除了火牛阵之外，還有所謂的火象阵、火鸡阵、火猴阵等\[3\]。

## 相關條目

  - [戰豬](../Page/戰豬.md "wikilink")

## 注釋

## 外部链接

  - [蔣震宇：日軍用火牛陣毒瓦斯強攻我陣地](http://www.southcn.com/news/gdnews/sz/kangzhan/laobing/200508120338.htm)
  - [林彪的这一生](http://book.sina.com.cn/2003-07-28/3/12987.shtml)
  - [红军巧布火牛阵](http://zx.my.gov.cn/MYGOV/150607842892578816/20060905/118687.html)

[Category:战国军事](../Category/战国军事.md "wikilink")
[Category:军事战术](../Category/军事战术.md "wikilink")
[Category:中国古代军事学](../Category/中国古代军事学.md "wikilink")
[Category:军用动物](../Category/军用动物.md "wikilink")

1.  《[史记](../Page/史记.md "wikilink")·田单列传》：“（田单）乃收城中得千余牛……束兵刃于其角，而灌脂束苇于尾，烧其端;凿城数十穴，夜纵牛，壮士五千人随其后，牛尾热，怒而奔燕军，燕军夜大惊。”
2.  《[宋史纪事本末](../Page/宋史纪事本末.md "wikilink")》卷六十六
3.  《绿色大世界》1995年01期