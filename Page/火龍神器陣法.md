[火龙经_(18).jpg](https://zh.wikipedia.org/wiki/File:火龙经_\(18\).jpg "fig:火龙经_(18).jpg")
[Yuan_chinese_gun.jpg](https://zh.wikipedia.org/wiki/File:Yuan_chinese_gun.jpg "fig:Yuan_chinese_gun.jpg")
[Ming_cannon.JPG](https://zh.wikipedia.org/wiki/File:Ming_cannon.JPG "fig:Ming_cannon.JPG")

《**火龍神器陣法**》，又名《**火龙经**》，是一部[中国古代](../Page/中国.md "wikilink")[火器大全](../Page/火器.md "wikilink")，署名为[刘基](../Page/刘基.md "wikilink")、[焦玉等](../Page/焦玉.md "wikilink")，自称作于[明朝](../Page/明朝.md "wikilink")[永乐十年](../Page/永乐.md "wikilink")（1403年），首见于[崇祯年间](../Page/崇祯.md "wikilink")[焦勒的](../Page/焦勒.md "wikilink")《[火攻罕要](../Page/火攻罕要.md "wikilink")》，[清朝](../Page/清朝.md "wikilink")[咸豐年间方始有刻本传世](../Page/咸豐_\(年號\).md "wikilink")，其中有最早的[地雷](../Page/地雷.md "wikilink")、[水雷](../Page/水雷.md "wikilink")、[火绳枪等等的描述](../Page/火绳枪.md "wikilink")。

据后人考证，发现书中一些细节有误，如称[朱棣为](../Page/朱棣.md "wikilink")“成祖”（[庙号](../Page/庙号.md "wikilink")）等，但因[元朝](../Page/元朝.md "wikilink")[忽必烈攻打](../Page/忽必烈.md "wikilink")[日本时已经使用铁火炮](../Page/日本.md "wikilink")、[朱元璋大战](../Page/朱元璋.md "wikilink")[陈友諒于](../Page/陈友諒.md "wikilink")[鄱阳湖时也早已使用火箭](../Page/鄱阳湖.md "wikilink")、火銃、[火蒺藜](../Page/火蒺藜.md "wikilink")、大小火枪等多种[火铳](../Page/火铳.md "wikilink")\[1\]，尚能断定今本《火龙经》可能经过[嘉靖年间修订而成](../Page/嘉靖.md "wikilink")，但其余书中原文應不该置疑。

## 图集

<File:'Flying> Crow With Magic Fire', a winged rocket bomb.jpg|神火飛鴉
<File:Chinese> Multistage Rocket.JPG|火龍出水 <File:火龙经> (109).jpg|火龙经地雷
<File:Chinese> Cannon.JPG|火龙经飞云霹雳炮 <File:Chinese> Naval Mine.JPG|火龙经水雷

## 参考文献

### 引用

### 来源

  - 网页

<!-- end list -->

  - [火龍神器陣法（繁體：天策府繁體兵書）](http://www.cos.url.tw/book/6/O-1-053.htm)

## 外部链接

  -
## 参见

  - [突火枪](../Page/突火枪.md "wikilink")
  - [火铳](../Page/火铳.md "wikilink")
  - [碗口銃](../Page/碗口銃.md "wikilink")

{{-}}

[+](../Category/明朝火器.md "wikilink")
[Category:中国军事书籍](../Category/中国军事书籍.md "wikilink")
[Category:明朝科技文献](../Category/明朝科技文献.md "wikilink")
[Category:明朝典籍](../Category/明朝典籍.md "wikilink")

1.  [钱谦益](../Page/钱谦益.md "wikilink") 著《国初群雄事略》卷四