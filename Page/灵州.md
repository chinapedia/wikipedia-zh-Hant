**灵州**，[中国古代的一個](../Page/中国.md "wikilink")[州](../Page/州.md "wikilink")。[治所在今](../Page/治所.md "wikilink")[宁夏回族自治区](../Page/宁夏回族自治区.md "wikilink")[灵武市](../Page/灵武市.md "wikilink")，[灵州城为西北著名古城](../Page/灵州城.md "wikilink")。

## 历史

[西汉](../Page/西汉.md "wikilink")[惠帝四年](../Page/汉惠帝.md "wikilink")（前191年）置[靈州縣](../Page/靈州縣.md "wikilink")，初曰[靈洲](../Page/靈洲.md "wikilink")，为[北地郡](../Page/北地郡.md "wikilink")19[县之一](../Page/县.md "wikilink")，[颜师古注](../Page/颜师古.md "wikilink")：“水中可居曰洲，此地在何志洲，随水上下，未尝沦没，故号灵洲。”。[东汉时](../Page/东汉.md "wikilink")，为[灵州县](../Page/灵州县.md "wikilink")。

[孝昌二年](../Page/孝昌_\(年号\).md "wikilink")（526年），[北魏升薄骨律镇为灵州](../Page/北魏.md "wikilink")。[隋朝成立之初](../Page/隋朝.md "wikilink")，設置靈州，並管轄4郡4縣。[大業](../Page/大业_\(年号\).md "wikilink")3年（607年）行政區重新劃分，靈州改為[靈武郡](../Page/靈武郡.md "wikilink")，改隸[環州管轄](../Page/環州.md "wikilink")，同時編入鳴沙縣等六個下轄縣。

[隋唐时治所在今](../Page/隋唐.md "wikilink")[宁夏回族自治区](../Page/宁夏回族自治区.md "wikilink")[银川市](../Page/银川市.md "wikilink")[永宁县西南](../Page/永宁县.md "wikilink")。唐后期设[朔方节度使于灵州](../Page/朔方节度使.md "wikilink")，故址在今宁夏回族自治区[吴忠市境内以西古城](../Page/吴忠市.md "wikilink")。2003年5月8日，宁夏回族自治区[吴忠市出土唐灵州墓志铭载](../Page/吴忠市.md "wikilink")：墓主人吕氏夫人“终于灵州私第”，“殡于[回乐县东原](../Page/回乐县.md "wikilink")”，据此，宁夏考古专家确认：古灵州城故址在今吴忠市境内。[西夏改为](../Page/西夏.md "wikilink")[西平府](../Page/西平府.md "wikilink")。[元朝时](../Page/元朝.md "wikilink")，灵州属宁夏府路。

[明朝为灵州所](../Page/明朝.md "wikilink")，[洪武三年](../Page/洪武.md "wikilink")（1370年）罢灵州。洪武十七年（1384年），[黄河发大水](../Page/黄河.md "wikilink")，淹没古灵州城，“城凡三徙”，最后于[宣德三年](../Page/宣德.md "wikilink")（1428年）迁至今[灵武市](../Page/灵武市.md "wikilink")，明[朱栴称为](../Page/朱栴.md "wikilink")“今之新城”，即[新灵州城](../Page/新灵州城.md "wikilink")。[弘治十三年](../Page/弘治_\(明朝\).md "wikilink")（1500年）九月，复置灵州，[直隶](../Page/直隶.md "wikilink")[陕西布政司](../Page/陕西布政司.md "wikilink")，是为[灵州直隶州](../Page/灵州直隶州.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正三年属](../Page/雍正.md "wikilink")[宁夏府](../Page/宁夏府.md "wikilink")，为不辖县的[散州](../Page/散州.md "wikilink")。1913年，灵州改为[灵武县](../Page/灵武县.md "wikilink")。

## 行政区划

### 隋朝

| **隋朝时行政区划变迁** |
| ------------- |
| 区分            |
| 州             |
| 郡             |
| 縣             |

## 参考资料

  - 《[明史](../Page/明史.md "wikilink")·志第十八·地理三》
  - 《[清史稿](../Page/清史稿.md "wikilink")·志三十九·地理十一》

[Category:北魏的州](../Category/北魏的州.md "wikilink")
[Category:西魏的州](../Category/西魏的州.md "wikilink")
[Category:北周的州](../Category/北周的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:五代的州](../Category/五代的州.md "wikilink")
[Category:西夏的州](../Category/西夏的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:明朝的州](../Category/明朝的州.md "wikilink")
[Category:清朝的州](../Category/清朝的州.md "wikilink")
[Category:宁夏的州](../Category/宁夏的州.md "wikilink")
[Category:吴忠行政区划史](../Category/吴忠行政区划史.md "wikilink")
[Category:银川行政区划史](../Category/银川行政区划史.md "wikilink")
[Category:526年建立的行政区划](../Category/526年建立的行政区划.md "wikilink")
[Category:1913年废除的行政区划](../Category/1913年废除的行政区划.md "wikilink")