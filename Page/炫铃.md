**炫铃**™，是[中国联通推出的个性化](../Page/中国联通.md "wikilink")[回铃音业务](../Page/回铃音.md "wikilink")\[1\]。

## 概述

2003年，[中国移动通信集团公司率先在上海](../Page/中国移动.md "wikilink")、广东、北京和浙江四省市试推彩铃业务。中国联通也随即推出了“炫铃”业务，以进行市场竞争\[2\]（类似的还有[中国电信](../Page/中国电信.md "wikilink")“七彩铃音”、[中国网通](../Page/中国网通.md "wikilink")“悦铃”\[3\]）\[4\]。2006年，[中国联通公司推广无线公话同时](../Page/中国联通.md "wikilink")，大力推出的个性化[回铃音业务](../Page/回铃音.md "wikilink")\[5\]，可通过[下载](../Page/下载.md "wikilink")、包月等方式定制\[6\]\[7\]。

2008年5月，[中华人民共和国工业和信息化部](../Page/中华人民共和国工业和信息化部.md "wikilink")、[国家发展和改革委员会](../Page/国家发展和改革委员会.md "wikilink")、[中华人民共和国财政部发布的](../Page/中华人民共和国财政部.md "wikilink")《关于深化电信体制改革的通告》。之后[中国电信股份有限公司向](../Page/中国电信.md "wikilink")[中国联通有限公司收购CDMA业务](../Page/中国联通.md "wikilink")。2008年10月1日，CDMA业务的经营主体将由中国联通变更为中国电信。12月1日，原中国联通C网炫铃业务将由中国电信“七彩铃音”业务承接。同月，中国电信爱音乐部门承接全部中国联通的炫铃业务\[8\]。

## 参见

  - [彩铃](../Page/彩铃.md "wikilink")

## 参考

## 外部链接

  - [炫铃官方主页](https://web.archive.org/web/20060810182321/http://ring.myuni.com.cn/)

[Category:行動電話](../Category/行動電話.md "wikilink")
[Category:中国联通](../Category/中国联通.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.