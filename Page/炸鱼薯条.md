[Fish_n_Chips_in_Edinburgh_20171126.jpg](https://zh.wikipedia.org/wiki/File:Fish_n_Chips_in_Edinburgh_20171126.jpg "fig:Fish_n_Chips_in_Edinburgh_20171126.jpg")和[番茄酱的英式炸鱼薯条](../Page/番茄酱.md "wikilink")。\]\]
**炸魚薯條**（）是一道源自[英國的熱食](../Page/英國.md "wikilink")，魚裹上麵糊油炸，搭配炸[薯條，吃的時候還會配上不同口味的調味醬](../Page/薯條.md "wikilink")，属于街邊[小吃](../Page/小吃.md "wikilink")。炸鱼薯条是一道很常見的外帶食物，也是早期融合料理的例子\[1\]\[2\]。[猶太男孩喬瑟夫](../Page/猶太.md "wikilink")·麥林（Joseph
Malin）在1860年的倫敦東區街頭，發明了炸魚配薯條\[3\]\[4\]。[奧爾德姆的湯米菲爾德市場設有一個](../Page/奧爾德姆.md "wikilink")[藍色牌匾](../Page/藍色牌匾.md "wikilink")，標示該地是1860年代炸魚薯條店及速食業起源\[5\]。

1910年的英國，已經有超過
25,000家炸魚薯條店，到了1920年代更超過35,000家\[6\]。首相[邱吉爾稱炸魚薯條是](../Page/温斯顿·丘吉尔.md "wikilink")「好夥伴」，[約翰·藍儂會在炸魚薯條上加大量番茄醬](../Page/约翰·列侬.md "wikilink")，[喬治·歐威爾則稱炸魚薯條是勞動階級](../Page/乔治·奥威尔.md "wikilink")「重要的家常菜」\[7\]。

炸魚薯條在[紐西蘭和](../Page/紐西蘭.md "wikilink")[澳大利亚也很受歡迎](../Page/澳大利亚.md "wikilink")。在[美國也漸漸地普遍起來](../Page/美國.md "wikilink")。

## 歷史

[Fish_and_chips.jpg](https://zh.wikipedia.org/wiki/File:Fish_and_chips.jpg "fig:Fish_and_chips.jpg")
拜北海快速發展的拖網漁業所賜，炸魚薯條成為英國勞動階級餐桌上的常見餐點\[8\]，到了19世紀後半，接連工業大城和港口間的鐵路蓬勃發展，讓新鮮漁獲可以快速送達人口稠密的地區\[9\]。西班牙猶太人將炸魚引進英國\[10\] ，後來變成公認的經典魚肉料理\[11\]。從17世紀初開始定居在英國的西班牙猶太人會製作一種魚肉裹上一層麵粉，類似西班牙炸魚（Pescado
frito）的食物\[12\]。麵糊炸魚會先沾麵粉，再裹上用水混和麵粉做成的麵糊，有時也會用啤酒代水。一些比較新式的食譜會改用玉米粉，並用蘇打水取代啤酒
\[13\]。1860年第一家炸魚薯條店在倫敦開幕，店主喬瑟夫·麥林\[14\]販售「猶太式炸魚」\[15\]。 

雖然歐洲各地都有販售油炸食物的店鋪，但英國才是現代炸魚薯條店（現代英文俚語稱「chippy」或「chipper」\[16\]）的發源地。早期的炸魚薯條店陳設簡單，主要都是一個大鍋，鍋裡裝著用炭火加熱的食用油。後來炸魚薯條店鋪型態變得大同小異：食物用紙包裹遞給在櫃台前排隊的顧客，櫃台後方就是炸爐。到了1910年，已經有超過
25,000家炸魚薯條店，1920年代時，更超過35,000家\[17\]。1928年，哈利·藍斯登（Harry
Ramsden）的連鎖速食餐廳在英國開幕。1952年，哈利位於西約克郡古斯利（Guiseley）的店鋪，一天內就賣出了10,000份炸魚薯條，因此獲得金氏世界紀錄\[18\]。\[19\][喬治·歐威爾的](../Page/乔治·奥威尔.md "wikilink")《*通往威根碼頭之路*》，記錄了他在英格蘭北部的勞動階級生活，作者認為炸魚薯條是家常菜之首，就像勞動階級的萬靈丹\[20\]。[第二次世界大戰之際](../Page/第二次世界大战.md "wikilink")，炸魚薯條是英國少數沒有受到配給限制的食物。\[21\]

英國的炸魚薯條原本是用舊報紙包著販售，但是現在已經幾乎不用舊報紙，改用白紙、厚紙板或塑膠包裝代替。在英國\[22\]和愛爾蘭\[23\]，都各自有魚肉販售標示的規範，魚肉必需標示商品名稱或種類；因此現在「炸鱈魚和薯條」比「炸魚薯條」更常見。

## 做法

[BCLM_fish+chips.jpg](https://zh.wikipedia.org/wiki/File:BCLM_fish+chips.jpg "fig:BCLM_fish+chips.jpg")

1人份炸鱼薯条的炸鱼做法，将100克普通[面粉与](../Page/面粉.md "wikilink")100克自发[面粉混合](../Page/面粉.md "wikilink")，放入半茶匙[盐](../Page/食盐.md "wikilink")（5毫升），挖个小坑，放入[蛋黄一个](../Page/鸡蛋.md "wikilink")；加入半[品脱](../Page/品脱.md "wikilink")[牛奶或](../Page/牛奶.md "wikilink")[黑啤酒](../Page/黑啤酒.md "wikilink")、一汤匙[油](../Page/油.md "wikilink")（15毫升）、适量[水](../Page/水.md "wikilink")，
打成[奶油面糊](../Page/奶油面糊.md "wikilink")。静置30分钟。将两个[蛋白打至起泡](../Page/鸡蛋.md "wikilink")，混入[奶油面糊](../Page/奶油面糊.md "wikilink")。

鱼洗淨，斬頭、去尾、剔骨，两面拍粉，提鱼的一端让整条鱼滑入奶油面糊容器中，取出时注意用容器边沿去除过量面糊。

油温为180摄氏度。将鱼轻轻地放入油箱，注意不要近身，以防溅油。3-4分钟后，鱼会浮至表面，用漏勺翻身，续炸3-4分钟，呈两面金黄即可。

### 烹調

傳統的炸油使用[牛脂或](../Page/牛脂.md "wikilink")[豬油](../Page/豬油.md "wikilink")；現在比較多用[花生油](../Page/花生油.md "wikilink")（因為發煙點相對較高）這類的[植物油](../Page/植物油.md "wikilink")。北英格蘭或蘇格蘭少數攤販，以及大多數南愛爾蘭的攤販仍然會用傳統的豬或牛油，因為可以賦予這道菜獨特的風味，但這樣一來就不適合海鮮類素食者或某些宗教信仰的人食用。

### 厚度

英式薯條通常比美式薯條厚。

### 麵糊

英格蘭及愛爾蘭，傳統的炸魚薯條會用水和麵粉製作麵糊，加上一點[碳酸氫鈉](../Page/碳酸氢钠.md "wikilink")（烘培用蘇打粉）、少許醋，在麵糊會起泡，產生蓬鬆口感。有些食譜會用啤酒或牛奶取代麵糊中的水。啤酒中的[二氧化碳可以讓麵糊的質地更酥脆](../Page/二氧化碳.md "wikilink")，並使成品外觀更偏棕橘色。簡單的麵糊中，麵粉與啤酒體積比例為二比三。不同的啤酒也會影響麵糊的風味：有些人比較愛用[拉格啤酒](../Page/拉格啤酒.md "wikilink")\[24\]\[25\]，有些人則偏好[司陶特啤酒或](../Page/司陶特啤酒.md "wikilink")[苦啤酒](../Page/苦啤酒.md "wikilink")。

## 社会习俗

[罗马天主教紀念](../Page/罗马天主教.md "wikilink")[耶穌受難](../Page/耶穌受難.md "wikilink")，在星期五守[小齋](../Page/小齋.md "wikilink")，不許食[恆溫動物的](../Page/恆溫動物.md "wikilink")[肉](../Page/肉.md "wikilink")，而以[海鮮代之](../Page/海鮮.md "wikilink")，尤其喫魚，大多[英國聖公會信眾也保有这一](../Page/英國聖公會.md "wikilink")[传统](../Page/传统.md "wikilink")，直到今天依然在影响半世俗的与世俗的社会。周五晚上光顾炸鱼薯条店依旧是英国社会传统的一部分，许多餐饮场所是日亦有特别菜单，提供炸鱼薯条的选择。

通常，英国食鱼不去皮，但是[北英格兰和](../Page/北英格兰.md "wikilink")[苏格兰某些地区会把鱼皮完整去掉](../Page/苏格兰.md "wikilink")。

英国北部常用[牛脂来炸鱼](../Page/牛脂.md "wikilink")，而南方则多用[植物油](../Page/植物油.md "wikilink")，讲究的店家用[花生油](../Page/花生油.md "wikilink")。

英國一位98歲的婆婆自1928年起，與丈夫在彭布羅克經營炸魚炸薯條店，每天9時起工作，在午膳時間招呼食客。近80年來天天為街坊開鑊炸魚，深受鄰里歡迎，在2006年獲頒[英帝國員佐勳章](../Page/大英帝国勋章.md "wikilink")（MBE）。布朗婆婆說：「我很驚訝，亦以此為榮。」

## 鱼的种类

在英國，由於鄰近[北海](../Page/北海_\(大西洋\).md "wikilink")，[鳕鱼是最通常的选择](../Page/鳕.md "wikilink")，但是其他鱼种，尤其是白鱼也是常见之选，例如[黑线鳕](../Page/黑线鳕.md "wikilink")、欧鲽、[鳐](../Page/鰩科.md "wikilink")、角鲛等。在英格兰北部和[苏格兰地区](../Page/苏格兰.md "wikilink")，黑线鳕最为常见。
澳洲則常用[潯鱈](../Page/潯鱈.md "wikilink")、[尖吻鱸](../Page/尖吻鱸.md "wikilink")、[牛尾魚](../Page/牛尾魚.md "wikilink")、[南極星鯊](../Page/南極星鯊.md "wikilink")（當地稱為flake）、[銀金鯛等野生魚類](../Page/銀金鯛.md "wikilink")，飼養的[博氏𩷶](../Page/博氏𩷶.md "wikilink")、[藍尖尾無鬚鱈也很常用](../Page/藍尖尾無鬚鱈.md "wikilink")。

根据2006年2月3日英国《卫报》的报道，鳕鱼由于过度捕捞，种群数量过少，因此捕捞鳕鱼有各种限制。过去一条鳕鱼的体重可达7磅，不过现在一条鳕鱼往往很难超过1磅半。

## 配料

炸鱼薯条於各地配料选择不同：

  - 在[英国](../Page/英国.md "wikilink")，炸鱼薯条一般配盐和[麦醋](../Page/醋.md "wikilink")；
  - [苏格兰人则用白醋](../Page/苏格兰.md "wikilink")；
  - [加拿大人喜欢用白醋浇薯条](../Page/加拿大.md "wikilink")，柠檬汁挤在鱼上；
  - [美国人通常配麦醋](../Page/美国人.md "wikilink")，但是也经常看到柠檬瓣或一种白色酱（，有時譯作“[塔塔醬](../Page/塔塔醬.md "wikilink")”）。
  - [澳大利亚人則通常喜歡在薯條上加](../Page/澳大利亚人.md "wikilink")[番茄醬](../Page/番茄醬.md "wikilink")，亦會將檸檬汁擠在魚片上。

愛爾蘭和英國傳統的炸魚薯條店在上菜前會灑上[鹽和](../Page/盐.md "wikilink")[醋](../Page/醋.md "wikilink")\[26\]。麥芽醋、洋蔥醋（醃洋蔥的醋）或更便宜的化學醋都有人使用。在英格蘭，豌豆泥是很受歡迎的配菜\[27\]，佐以各種醃製配菜：小黃瓜、洋蔥和蛋\[28\]。

愛爾蘭、威爾斯和英格蘭等地，大部分外帶餐廳都會提供熱的醬汁如：咖哩、肉汁或豌豆泥。醬汁會澆在薯條上。另外還有一種炸麵糊，原本是炸魚的副產品，麵糊放進油鍋裡炸到金黃酥脆就完成了。也有把焗烤馬鈴薯或薯餅沾上麵糊油炸的配菜。這些都是常搭配上述醬汁的配菜\[29\]。

## 文化影响

2015年10月，[习近平访问英国时](../Page/习近平.md "wikilink")，当时的[英国首相](../Page/英国首相.md "wikilink")[卡梅伦曾带他到一间乡村酒吧](../Page/卡梅伦.md "wikilink")，品尝炸鱼薯条和当地啤酒。根据新华社的报道，习近平称赞食物“very
good”（非常好！）。\[30\]

当地旅游宣传组织Make It York工作人员威尔（Will
Zhuang）指出，2015年，中国国家主席习近平访问英国时，品尝了一份炸鱼薯条，很多中国游客都受其影响。因此，许多中国旅游团把“吃炸鱼薯条”加入他们的行程中。\[31\]

一名中国女游客对BBC表示，给炸鱼薯条这道菜打满分。还有中国游客说，炸鱼薯条是中国最有名的英国美食，英文教科书上有提到，“我一直很想尝尝炸鱼薯条的味道”。\[32\]

## 其他相关食品

炸鱼薯条店通常还会提供其他速食，例如：[康沃尔馅饼](../Page/康沃尔馅饼.md "wikilink")、炸[香肠](../Page/香肠.md "wikilink")、[鱼饼](../Page/鱼饼.md "wikilink")、、炸[鸡](../Page/鸡.md "wikilink")。甜食方面，以[奶油面糊包裹水果](../Page/奶油面糊.md "wikilink")（苹果、香蕉、菠萝等）或冰淇淋油炸，则为。吃炸鱼薯条时，煮[豌豆是最常见的配菜](../Page/豌豆.md "wikilink")。

## 跨文化呼应

炸鱼薯条中的炸鱼用面糊包裹的做法，和源自[葡萄牙](../Page/葡萄牙.md "wikilink")、流行於[日本的](../Page/日本.md "wikilink")[天妇罗](../Page/天妇罗.md "wikilink")，及[中国](../Page/中国.md "wikilink")[宁波的](../Page/宁波.md "wikilink")[面拖黄鱼有近似之处](../Page/面拖黄鱼.md "wikilink")。不过，天妇罗傳至日本後不使用鱼，而多以面糊包裹[虾](../Page/虾.md "wikilink")、[蔬菜等](../Page/蔬菜.md "wikilink")，蘸清淡的天妇罗[酱油](../Page/酱油.md "wikilink")，份额常以一口为限。[黄鱼为东海特产](../Page/黄鱼.md "wikilink")，英国的炸鱼薯条用材则多取自邻近海域。面拖黄鱼另有苔拖黄鱼的变体，更添[地方风味](../Page/地方.md "wikilink")。

## 外部链接

  - [BBC新闻：炸鱼薯条是在法国发明的吗？](http://news.bbc.co.uk/2/hi/uk_news/3380151.stm)

  - [海鱼工业组织论炸鱼薯条](https://web.archive.org/web/20081011050658/http://www.seafish.org/plate/fishandchips.asp)

## 资料来源

  - Olivier Kugler, "Kugler's People", 载英国《卫报》，2006年2月3日，g2版p.16-17

[Category:英國飲食](../Category/英國飲食.md "wikilink")
[Category:鱼肉料理](../Category/鱼肉料理.md "wikilink")
[Category:馬鈴薯食品](../Category/馬鈴薯食品.md "wikilink")
[Category:油炸食品](../Category/油炸食品.md "wikilink")
[Category:薯條](../Category/薯條.md "wikilink")

1.  Black, Les (1996). New Ethnicites And Urban Cult. Oxford: Routledge.
    p. 15. ISBN 1-85728-251-5.

2.  Alexander, James (18 December 2009). "The unlikely origin of fish
    and chips". BBC News. Retrieved 16 July 2013.

3.  "The Portuguese gave us fried fish, the Belgians invented chips but
    150 years ago an East End boy united them to create The World's
    Greatest Double Act". Daily Mail. Retrieved September 21, 2011.

4.  "Bacon butties, roast dinners and a cuppa: 50 things we love best
    about Britain show we're a nation of food lovers". Daily Mail.
    Retrieved 3 November 2016

5.
6.
7.
8.  <http://www.bbc.co.uk/programmes/p039pr7c?intc_type=promo&intc_location=sport&intc_campaign=fishandchips&intc_linkname=radio4_fac_audioclip1>

9.  "Fish and chips - A great English tradition". Archived from the
    original on 16 January 2008. Retrieved 22 June 2009.

10.
11. Hosking, Richard (2007). Eggs in Cookery:Proceedings of the Oxford
    Symposium of Food and Cookery 2006. United Kingdom: Prospect Books.
    p. 183. ISBN 978-1-903018-54-5.

12. Marks, Gil (1999). The world of Jewish cooking: more than 500
    traditional recipes from Alsace to Yemen. Simon & Schuster. ISBN
    0-684-83559-2.

13. <http://www.manchestereveningnews.co.uk/news/greater-manchester-news/chips-in-north-are-the-best-909994>

14. Rayner, Jay (3 November 2005). "Enduring Love". The Guardian.
    London. Retrieved 19 January 2003. In 1860 a Jewish immigrant from
    Eastern Europe called Joseph Malin opened the first business in
    London's East End selling fried fish alongside chipped potatoes
    which, until then, had been found only in the Irish potato shops.

15. "Chip-Shop Fried Fish". The Foods of England Project. Retrieved 23
    June 2016.

16. "Chippy smells of chips complaint". BBC News. 7 November 2006.
    Retrieved 22 June 2009.

17.
18.
19. Hegarty, Shane (3 November 2009). "How fish and chips enriched a
    nation". The Irish Times. Dublin, Ireland. p. 17.

20.
21. "Resources for Learning, Scotland: Rationing". Rls.org.uk. 5 January
    1998. Retrieved 22 June 2009.

22. "Fish Labelling Regulations (England) 2003". The Stationery Office.
    2003. Retrieved 4 April 2009 (equivalent similarly-named legislation
    applies in other countries of the UK)

23. "European Communities (Fish Labelling) Regulations, 2003" (PDF).
    Retrieved 16 October 2012.

24. "Deep fried fish in beer". Retrieved 23 March 2009.

25. Hix, Mark (26 January 2008). "Gurnard in beer batter". The
    Independent. London. Retrieved 23 March 2009.

26. Alan Masterson, tictoc design. ""Seafish. On Plate. Fish & chips"
    (UK Sea Fish Industry Authority website)". Seafish.org. Archived
    from the original on 11 October 2008. Retrieved 22 June 2009.

27. "Crispy fish & chips with mushy peas recipe". BBC. Retrieved 7 March
    2010.

28. "British Food: A History". Britishfoodhistory.wordpress.com. 23
    September 2012. Retrieved 16 July 2013.

29. "Do you know what scraps are? And why they should be free". The
    Guardian. London. 13 July 2007. Retrieved 24 November 2010.

30.

31.
32.