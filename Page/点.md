在[几何学](../Page/几何学.md "wikilink")、[拓扑学以及](../Page/拓扑学.md "wikilink")[数学的相关分支中](../Page/数学.md "wikilink")，一个空间中的**点**用于描述给定空间中一种特别的对象，在空间中有类似于体积、面积、长度或其他高维类似物。一个点是一个零[维度对象](../Page/维.md "wikilink")。点作为最简单的几何概念，通常作为[几何](../Page/几何.md "wikilink")、[物理](../Page/物理.md "wikilink")、[矢量图形和其他领域中的最基本的组成部分](../Page/矢量图形.md "wikilink")。

## 數學的點－歷史

在[亞里斯多德的著作](../Page/亞里斯多德.md "wikilink")《論天體》第三冊中，已經提到數學中的點是沒有大小的\[1\]\[2\]，他依此來駁斥[柏拉圖將數學的幾何形視為物理實體的構成要素](../Page/柏拉圖.md "wikilink")\[3\]（參見[正多面體](../Page/正多面體.md "wikilink")），並強調這與數學思想相違背\[4\]：「數學的平面沒有厚度，所以不能構造物理實體。」他論述說，如果數學平面有厚度，那麼數學的線就要有寬度才能夠構成平面，而數學的點必須有大小才能構成線，但是在數學中已經明確定義數學的點是沒有大小的，因此柏拉圖的理論與數學相牴觸。從這裡，亞里斯多德陳述說，一個幾何物件只能分割成相同型態的幾何物件（而不會變成其它的東西）：平面只能分割成平面，而不能分割成線；線只能分割成線，不能分割成點；這樣的分割可以無限的進行，而不是像[原子論者所說的](../Page/原子論.md "wikilink")，最後分割到原子（或是基本構成要素）就停止了。

因此，早在[歐幾里得的](../Page/歐幾里得.md "wikilink")《[幾何原本](../Page/幾何原本.md "wikilink")》之前，數學中的點只用來標示位置的用法已經是共識。亞里斯多德提到點的時候，用的字是
στιγμὰς，是可見的點（spot），而歐幾里得則（小心翼翼的）採用另一個字 σημεῖόν，原意是「標示」（sign）：
這句話的意思是：「點是沒有部分（μέρος）的東西」。點沒有部分，所以也就沒有大小\[5\]。這個論點來源自亞里斯多德的「部分－整體」理論（part–whole
theory）：

《[幾何原本](../Page/幾何原本.md "wikilink")》的阿拉伯文版，將 σημεῖόν 翻譯為
نقطة\[6\]，意思回到亞里斯多德的可見點\[7\]；拉丁文版則將 σημεῖόν 翻譯為
punctum\[8\]，意思是被尖物刺成的小洞。

## 歐幾里得幾何中的点

[ACP_3.svg](https://zh.wikipedia.org/wiki/File:ACP_3.svg "fig:ACP_3.svg")中的有限点集(蓝色).\]\]
在[歐幾里得幾何中](../Page/歐幾里得幾何.md "wikilink")，**點**是空間中只有位置，沒有大小的圖形。點是整個歐幾里得幾何學的基礎，後者是研究點，[線](../Page/線.md "wikilink")，[面](../Page/面.md "wikilink")，[體的一種科學](../Page/體.md "wikilink")。[欧几里得最初含糊的定义点作为](../Page/欧几里得.md "wikilink")"没有部分的东西".
在二维[欧式空间](../Page/欧式空间.md "wikilink"),
一个点被表示为一个[有序对](../Page/有序数对.md "wikilink")\(\, (x,y)\),
其中第一个数字习惯上表示水平位置,通常记为 \(\, x\), 第二个数字习惯上表示竖直位置, 通常记为 \(\, y\).
这一思想很容易广到三维情况, 此时一个点被表示为一个有序三元组, \(\, (x,y,z)\),
第三个数字表示高度, 通常记为 z。更加一般的情况下，点被表示为一个有序 n 元组：\((a_1,a_2,...,a_n)\)
其中 n 为点所在的空间的维度.

在[現代數學語言中](../Page/現代數學.md "wikilink")，任何集合的元素都叫作「點」，但與[三維空間中的點可以没有任何關係](../Page/三維空間.md "wikilink")。

## 其他数学分支中的点

在[点集拓扑中的点](../Page/点集拓扑.md "wikilink"),
定义为一个[拓扑空间中的集合的元素](../Page/拓扑空间.md "wikilink").

尽管点被看做是主要的几何学和拓扑学中的基本概念, 但是有些几何和拓扑理论并不需要点的概念.
例如[非交换几何和](../Page/非交换几何.md "wikilink")[非点集拓扑](../Page/非点集拓扑.md "wikilink").
一个"非点空间"不是作为一个集合来定义的, 而是通过某种类似于几何上的函数空间的结构(代数上的或者逻辑上的): 连续函数代数或者集合代数.

## 算術中的点

1點（Basis
Point）的定義為“百分之零點零一”（0.01%）或“一個百分點的一百分之一”，可用算術符號‱表示。它在計算利率、匯率、股票價格等範疇被廣泛應用，因為這些範疇須要牽涉極微小百分數的計算。簡單來說：
一百點=百分之一（100‱ = 1%） 一萬點=百分之一百=一（10000‱ = 100% = 1）
在比較百分數時，除了可以用百分點之外，兩個百分數之間細微的差距也可用點子來表達。例如4.02%與4.05%相差0.03個百分點。

## 參考資料

## 互動

<http://www.khanacademy.org/cs/program/1342753278>

[Category:初等几何](../Category/初等几何.md "wikilink")

1.  [論天體，第三冊](http://dhspriory.org/thomas/DeCoelo.htm#3-3)，Thomas
    Aquinas 翻譯與註解
2.  [論天體，第三冊](http://classics.mit.edu/Aristotle/heavens.3.iii.html#57)，The
    Internet Classics Archive
3.  [Ancient Atomism, 3. Plato and
    Platonists](http://plato.stanford.edu/entries/atomism-ancient/#3),
    史丹佛哲學百科
4.  [WHY DOES PLATO'S ELEMENT THEORY CONFLICT WITH
    MATHEMATICS](http://www.rhm.uni-koeln.de/146/Kouremenos.pdf)
5.  [Euclid's Elements of Geometry: From the Latin Translation of
    Commandine](https://archive.org/stream/euclidselements02keilgoog#page/n23/mode/2up),
    by John Keill
6.  [Euclid,
    Elements](http://folk.uio.no/amundbjo/nat/elementa/arab.php#I_def)，阿拉伯文版
7.  [Wiktionary:
    نقطة](https://en.wiktionary.org/wiki/%D9%86%D9%82%D8%B7%D8%A9)
8.  [Euclid's Elements in the middle ages, Boethius
    tradition](http://www.math.ubc.ca/~cass/Euclid/folkerts/folkerts.html)