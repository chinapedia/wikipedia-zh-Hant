《**為什麼要民主？**》（）是一部由[Steps
International主導的](../Page/Steps_International.md "wikilink")[紀錄片系列](../Page/紀錄片.md "wikilink")，包含十部不同的紀錄片，由不同的導演將各國的[民主經驗](../Page/民主.md "wikilink")，透過寫實的方式匯聚成紀錄片。本紀錄片系列包含全球各國獨有的民主歷程經驗，預計於2007年10月8日起，在全球200家[電視台同步首映](../Page/電視台.md "wikilink")。

本紀錄片系列聯播計畫的發起人是[英國廣播公司的](../Page/英國廣播公司.md "wikilink")[尼克·弗拉泽與](../Page/尼克·弗拉泽.md "wikilink")[丹麥廣播公司的](../Page/丹麥廣播公司.md "wikilink")[马特·哈夫曼](../Page/马特·哈夫曼.md "wikilink")。他們希望，這十部紀錄片不只是一個大型國際合製案，更是民主[教育的教材](../Page/教育.md "wikilink")。

## 各紀錄片介紹

各紀錄片的[中文譯名除](../Page/中文.md "wikilink")[中國大陸另有譯名者外](../Page/中國大陸.md "wikilink")，皆是採用[台灣公共電視的譯名](../Page/台灣公共電視.md "wikilink")。各個中文譯名的後方均有附註英文原名。

### 请投我一票

《[请投我一票](../Page/请投我一票.md "wikilink")》（）是中國大陸紀錄片男[導演兼](../Page/導演.md "wikilink")[武漢電視台](../Page/武漢電視台.md "wikilink")[製作人](../Page/製作人.md "wikilink")[陳為軍執導的紀錄片](../Page/陳為軍.md "wikilink")，主題是[武漢市常青第一小學三年一班](../Page/武漢市常青第一小學.md "wikilink")[班级委员长](../Page/班長#学校的班长.md "wikilink")[選舉過程](../Page/選舉.md "wikilink")，共有三位候選人（成成、許曉菲、羅雷），競選期間為一星期，影射了現實社會的競選過程中常有的[賄選](../Page/賄選.md "wikilink")、[人身攻擊等](../Page/人身攻擊.md "wikilink")[負面選舉現象](../Page/負面選舉.md "wikilink")。該片在2006年獲得[日舞影展資助金](../Page/日舞影展.md "wikilink")，並在2007年度獲得最佳紀錄片獎。2008年4月17日[台灣時間](../Page/台灣時間.md "wikilink")22:00～22:56，[公視主頻再度播映該片](../Page/公視主頻.md "wikilink")，作為《[少年地球村](../Page/少年地球村.md "wikilink")》紀錄片系列的第四部（共五部）紀錄片。

該片的核心問題是：「沒有秩序的選舉，是民主的表現嗎？」

### 山內先生的選舉戰

《山內先生的選舉戰》（），是將[Laboratory X
Inc.製作](../Page/Laboratory_X_Inc..md "wikilink")、[日本男導演](../Page/日本.md "wikilink")[想田和弘執導的第一部紀錄片](../Page/想田和弘.md "wikilink")《選舉》（；）[剪輯而成的一](../Page/剪輯.md "wikilink")[小時版](../Page/小時.md "wikilink")（全長兩小時），記錄日本「[政治](../Page/政治.md "wikilink")[菜鳥](../Page/菜鳥.md "wikilink")」[山內和彥在](../Page/山內和彥.md "wikilink")2005年接受[自由民主黨徵召參加](../Page/自由民主党_\(日本\).md "wikilink")[川崎市](../Page/川崎市.md "wikilink")[議員補選的過程](../Page/議員.md "wikilink")，從山內和彥接受自民黨徵召參選開始，直到山內和彥當選為止。想田和弘是山內和彥的老同學。2007年10月26日，2007[高雄電影節曾經播映](../Page/高雄電影節.md "wikilink")《選舉》，當時中文片名為《完全選舉手冊》。

該片的核心問題是：「在[政黨政治中](../Page/政黨政治.md "wikilink")，『單以[政黨或某種](../Page/政黨.md "wikilink")[象徵為選擇標的](../Page/象徵.md "wikilink")』的選舉，是民主的表現嗎？」

### 丹麥漫畫風波

《丹麥漫畫風波》（）是[丹麥資深駐外男](../Page/丹麥.md "wikilink")[記者](../Page/記者.md "wikilink")[卡斯頓·凱爾執導的紀錄片](../Page/卡斯頓·凱爾.md "wikilink")，描寫[日德蘭郵報穆罕默德漫畫事件所引發的東西](../Page/日德蘭郵報穆罕默德漫畫事件.md "wikilink")[文化衝突](../Page/文化衝突.md "wikilink")、以及[宗教自由與](../Page/宗教自由.md "wikilink")[言論自由之間的衝突](../Page/言論自由.md "wikilink")，引起觀眾對言論自由的多方思考。

該片的核心問題是：「只強調自己的[自由](../Page/自由.md "wikilink")，不尊重他人，是民主的表現嗎？」

### 總統的晚餐

《總統的晚餐》（）是[巴基斯坦女導演](../Page/巴基斯坦.md "wikilink")[莎碧哈·蘇瑪執導的紀錄片](../Page/莎碧哈·蘇瑪.md "wikilink")，以該片拍攝者與[巴基斯坦總統](../Page/巴基斯坦總統.md "wikilink")[佩爾韋茲·穆沙拉夫共進](../Page/佩爾韋茲·穆沙拉夫.md "wikilink")[晚餐時的對談作為主線](../Page/晚餐.md "wikilink")，探討巴基斯坦民主政治的現況，兼有穆沙拉夫的抒發己志。

### 埃及民主觀察站

《埃及民主觀察站》（）是男導演[雪瑞夫·愛爾卡沙執導的紀錄片](../Page/雪瑞夫·愛爾卡沙.md "wikilink")，意圖以[埃及](../Page/埃及.md "wikilink")[非營利組織](../Page/非營利組織.md "wikilink")「[民主觀察站](../Page/民主觀察站.md "wikilink")」及其三位創辦人（三位埃及[婦女](../Page/婦女.md "wikilink")）的角度揭穿埃及在[民族民主黨](../Page/民族民主党_\(埃及\).md "wikilink")（或譯為「國家民主黨」）長期執政之下「假民主，真[威權](../Page/威權.md "wikilink")」的事實。

### 尋找甘地

《尋找甘地》（）是[拉利特·瓦切尼](../Page/拉利特·瓦切尼.md "wikilink")（Lalit
Vachani）執導的紀錄片，以[公路電影形式拍攝](../Page/公路電影.md "wikilink")，重新檢驗[印度](../Page/印度.md "wikilink")[聖雄甘地領導的](../Page/聖雄甘地.md "wikilink")[不合作運動的策略](../Page/不合作運動.md "wikilink")，並揭露[印度獨立之後依舊存在於印度社會之中的](../Page/印度獨立.md "wikilink")[社會階級不平等](../Page/社會階級.md "wikilink")、[偏執](../Page/偏執.md "wikilink")、[暴力等問題](../Page/暴力.md "wikilink")。拍攝時的主要行進路線是[食鹽長征的路線](../Page/食鹽長征.md "wikilink")。

### 印加革命

《印加革命》（）是[阿根廷男導演](../Page/阿根廷.md "wikilink")[羅地哥·范斯蓋茲執導的紀錄片](../Page/羅地哥·范斯蓋茲.md "wikilink")，企圖尋找[古巴革命領導人](../Page/古巴革命.md "wikilink")[切·格瓦拉留下的](../Page/切·格瓦拉.md "wikilink")「革命之火」。

### 黑暗紀事

《黑暗紀事》（）是[美國男導演](../Page/美國.md "wikilink")[艾利斯·吉布尼執導的紀錄片](../Page/艾利斯·吉布尼.md "wikilink")，以一名無辜的[阿富汗](../Page/阿富汗.md "wikilink")[計程車駕駛員遭當地](../Page/計程車.md "wikilink")[美軍](../Page/美軍.md "wikilink")[虐待及](../Page/虐待.md "wikilink")[殺害一事作為序幕](../Page/殺害.md "wikilink")，揭發美軍在阿富汗與[伊拉克的駐軍的虐囚行為](../Page/伊拉克.md "wikilink")。

該片的核心問題是：「如果[人權是民主價值的核心](../Page/人權.md "wikilink")，那麼『忽視人權的民主』是民主嗎？」

### 非洲女總統

《非洲女總統》（）是[賴比瑞亞女導演](../Page/賴比瑞亞.md "wikilink")[西雅塔·史考特·強森與男導演](../Page/西雅塔·史考特·強森.md "wikilink")[丹尼爾·瓊格共同執導的紀錄片](../Page/丹尼爾·瓊格.md "wikilink")，貼身拍攝[非洲第一位女](../Page/非洲.md "wikilink")[總統](../Page/總統.md "wikilink")──[賴比瑞亞總統](../Page/賴比瑞亞總統.md "wikilink")[愛倫·約翰森·希爾麗夫](../Page/愛倫·約翰森·希爾麗夫.md "wikilink")。

### 俄國式民主

《俄國式民主》（）是[俄羅斯女導演](../Page/俄羅斯.md "wikilink")[妮諾·科塔茲執導的紀錄片](../Page/妮諾·科塔茲.md "wikilink")，探討[蘇聯解體之後的俄羅斯民主政治會走到什麼方向](../Page/蘇聯解體.md "wikilink")。該片指出，俄羅斯人民信奉的[東正教主張](../Page/東正教.md "wikilink")[上帝](../Page/上帝.md "wikilink")、[耶穌](../Page/耶穌.md "wikilink")、[沙皇](../Page/沙皇.md "wikilink")「[三位一體](../Page/三位一體.md "wikilink")」，傳統[俄羅斯文化推崇](../Page/俄羅斯文化.md "wikilink")[強人政治](../Page/強人政治.md "wikilink")，俄羅斯人民對[冷戰時期的歷史記憶](../Page/冷戰.md "wikilink")，以及蘇聯解體之後的[俄羅斯經濟衰敗](../Page/俄羅斯經濟.md "wikilink")，導致許多俄羅斯人民深信「俄羅斯不需要民主」或「俄羅斯不需要現代[西方國家式的民主](../Page/西方國家.md "wikilink")，尤其美國式民主」。

該片的核心問題是：「民主是[普世價值嗎](../Page/普世價值.md "wikilink")？民主只有一種型態嗎？人民有權利決定他們自己『要不要民主』、『要哪一種民主』嗎？」

2008年6月7日，[龍應台文化基金會](../Page/龍應台文化基金會.md "wikilink")「思沙龍」舉辦「你所不知道的俄羅斯」[演講會](../Page/演講會.md "wikilink")，邀請[淡江大學俄羅斯研究所](../Page/淡江大學.md "wikilink")[教授](../Page/教授.md "wikilink")[亞歷山大·彼薩列夫](../Page/亞歷山大·彼薩列夫.md "wikilink")（Alexander
Pisarev）主講〈你所不知道的俄羅斯〉，並在台灣時間該日14:00～15:00播映該片。\[1\]

## 聯播網成員

<table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/公共電視文化事業基金會.md" title="wikilink">公共電視文化事業基金會</a>（PTS）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本放送協會.md" title="wikilink">日本放送協會</a>（NHK）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓國教育放送公社.md" title="wikilink">韓國教育放送公社</a>（EBS）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/以色列廣播局.md" title="wikilink">以色列廣播局</a>（IBA）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公共廣播電視公司.md" title="wikilink">公共廣播電視公司</a>（PBS）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拿大廣播公司.md" title="wikilink">加拿大廣播公司</a>（CBC）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Knowledge Network</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英國廣播公司.md" title="wikilink">英國廣播公司</a>（BBC）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德法公共电视台.md" title="wikilink">德法公共电视台</a>（arte）</p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德國電視二台.md" title="wikilink">德國電視二台</a>（ZDF）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波蘭電視台.md" title="wikilink">波蘭電視台</a>（TVP）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瑞典電視台.md" title="wikilink">瑞典電視台</a>（SVT）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芬兰广播公司.md" title="wikilink">芬兰广播公司</a>（YLE）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/希臘廣播公司.md" title="wikilink">希臘廣播公司</a>（ERT）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/比利時法語區電視台及電台.md" title="wikilink">比利時法語區電視台及電台</a>（RTBF）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹麥廣播公司.md" title="wikilink">丹麥廣播公司</a>（DR）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/意大利广播电视公司.md" title="wikilink">意大利广播电视公司</a>（RAI）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉比亞電視台.md" title="wikilink">阿拉比亞電視台</a>（Al Arabiya）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南非廣播公司（SABC）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特殊廣播服務.md" title="wikilink">特殊廣播服務</a>（SBS）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>… … … … … … … …</p></td>
<td><p>（總計約200家電視台）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 电视节目的变迁

## 注释

## 参考文献

## 外部連結

  - [《為什麼要民主？》國際官方網站](https://web.archive.org/web/20100602193003/http://www.whydemocracy.net/)
  - [台灣公共電視《為什麼要民主？》官方網頁](http://www.pts.org.tw/~web02/democracy/)
  - [Laboratory X Inc.（《山內先生的選舉戰》製作單位）官方網頁](http://www.laboratoryx.us/)
  - [想田和弘（《山內先生的選舉戰》導演）的部落格](http://documentary-campaign.blogspot.com/)

[W](../Category/2007年电影.md "wikilink") [W](../Category/民主.md "wikilink")
[W](../Category/紀錄片.md "wikilink")
[W](../Category/公視外購電視節目.md "wikilink")
[W](../Category/英國廣播公司電視節目.md "wikilink")

1.  龍應台文化基金會，[〈【龍應台文化基金會採訪通知】「思沙龍」剖析東西文化拉扯下的「俄國式民主」〉](http://www.coolloud.org.tw/node/21773)，2008年6月4日