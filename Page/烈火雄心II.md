《**烈火雄心II**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司製作的時裝](../Page/電視廣播有限公司.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，全劇共35集，由[王喜](../Page/王喜.md "wikilink")、[方中信](../Page/方中信.md "wikilink")、[張可頤](../Page/張可頤.md "wikilink")、[蒙嘉慧](../Page/蒙嘉慧.md "wikilink")、[歐錦棠及](../Page/歐錦棠.md "wikilink")[文頌嫻領銜主演](../Page/文頌嫻.md "wikilink")，由[陳慧珊](../Page/陳慧珊.md "wikilink")、[宣萱特別演出](../Page/宣萱.md "wikilink")，並由[鄭嘉穎聯合主演](../Page/鄭嘉穎.md "wikilink")，監製[王心慰](../Page/王心慰.md "wikilink")。此劇為《[烈火雄心](../Page/烈火雄心.md "wikilink")》系列作，但在劇情上與上輯完全沒有關連。

## 劇情大綱

紀德田（[王喜飾](../Page/王喜.md "wikilink")）加入消防員多年，也不曾升職，表面上，消防員對紀德田來說，只是一份工作，平日的紀德田，凡事不認真，常愛打侃嬉笑渡日，其實的紀德田毅力非凡，應變能力極高，只是從不外露，也不爭功，這與他的背景有極的大關係。

紀德田有一弟弟紀興田（[鄭嘉穎飾](../Page/鄭嘉穎.md "wikilink")），性格與紀德田大相逕庭。紀興田天生好勝，自小無論學業、運動均成績彪炳，大學畢業後，考進消防隊當起消防隊長，但是，當一個消防員對紀興田來說，只能夠滿足自己的英雄感，因此紀興田甫出班便力求表現，要成為消防隊中的明星。

紀德田早婚，妻子卻在數年前因一起交通意外去世，留下女兒紀瑤（[成珈瑩飾](../Page/成珈瑩.md "wikilink")），紀德田獨力撫育紀瑩，故不想在此工作上有任何的損傷，一向只求盡了職責便交差了事，從來不冒不必要的險，就連一直鍾愛的賽車也放棄了，只求無風無浪，獨自撫養紀瑤成人。

在一次遊輪大火之中，紀德田負責搜索的範圍內出現意外，因此受到上司唐明（[方中信飾](../Page/方中信.md "wikilink")）的責難，紀德田雖知此乃意外，但心中有愧，自覺拯救生命壓力極大，頓時起了逃避之心，主動要求調往潛水隊，可是世事往往難以預料，紀德田在潛水隊中過了一段短時期，又被調回陸上消防局，又再次重遇要求嚴格的唐明，紀德田面對唐明的嚴苛，以為唐明處處針對自己，不料在一次救火行動中，唐明居然甘願冒著生命危險，救了紀德田，由此令紀德田終於明白唐明一向以來，對下屬的嚴格要求，也是唐明對自己的要求，從這一刻開始，紀德田的工作態度轉趨積極，令周遭的人也同時另眼相看
。

紀德田對於紀興田的出色表現，沒有妒忌，反而引以為傲，而紀興田一直覺得大哥紀德田是個得過且過的人，然而，當紀德田受到唐明的啟發後，態度和表現大有進步，更贏得同袍和上司的讚賞；紀興田遂對這個大哥產生從未有過的妒忌，更決意在工作裏努力向上，爭功求名
。

## 演員表

### 紀家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/王喜.md" title="wikilink">王　喜</a></strong></p></td>
<td><p><strong>紀德田</strong></p></td>
<td><p><strong>紀德、Peter</strong><br />
消防員，後為消防隊目<br />
蔡少玲之夫<br />
紀興田之兄<br />
紀瑤之父<br />
蔡南豐之妹夫<br />
鍾茵怡之男友<br />
梁孟功、林聚賢之好友<br />
自從老婆死後，為了照顧瑤瑤做事變的馬馬虎虎、得過且過、沒有上進心，常因嘴賤惹禍，後遇到了鍾茵怡，開始奮發圖強<br />
於第35集因救火而命喪火場</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宣萱.md" title="wikilink">宣　萱</a></p></td>
<td><p>蔡少玲</p></td>
<td><p>蔡南豐之妹<br />
紀德田之妻<br />
紀瑤之母<br />
交通意外不幸離世<br />
（特別客串）</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>紀興田</strong></p></td>
<td><p><strong>Mark</strong><br />
消防隊長<br />
紀德田之弟<br />
紀瑤之二叔<br />
鍾茵怡之前男友<br />
做事急功近利、記算利益，一心只想快速成高官<br />
為了巴結上司而和陳小蘭交往<br />
腳踏兩條船，同時跟陳小蘭和鍾茵怡交往<br />
於第10集從英國回來，成為蝴蝶灣滅火輪消防局隊長<br />
於第32集和鍾茵怡分手<br />
於第35集和陳小蘭分手，因紀德田的死開始想瞭解當消防員真正的意義</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/成珈瑩.md" title="wikilink">成珈瑩</a></strong></p></td>
<td><p><strong>紀　瑤</strong></p></td>
<td><p><strong>瑤瑤</strong><br />
紀德田、蔡少玲之女<br />
紀興田之姪女<br />
蔡南豐之外甥女</p></td>
</tr>
</tbody>
</table>

### 唐家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林韋辰.md" title="wikilink">林韋辰</a></p></td>
<td><p>唐　偉</p></td>
<td><p><strong>Kelvin</strong><br />
唐明之兄<br />
江逸雅之大伯<br />
<a href="../Page/同性戀.md" title="wikilink">同性戀者</a><br />
於第9集因愛滋病身亡<br />
（特別客串）</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/方中信.md" title="wikilink">方中信</a></strong></p></td>
<td><p><strong>唐　明</strong></p></td>
<td><p><strong>Wilson</strong><br />
高級消防隊長<br />
唐偉之弟<br />
江逸雅之夫<br />
何寶琳之前男友<br />
於第24集和江逸雅離婚<br />
於第35集和江逸雅復合</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>江逸雅</strong></p></td>
<td><p><strong>Yan</strong><br />
家庭主婦 / 公關公司員工<br />
唐明之妻<br />
唐偉之弟婦<br />
葉向陽之好友<br />
何寶琳之下屬兼好友<br />
於第24集和唐明離婚<br />
於第30集知道何寶琳就是自己婚姻的第三者，後去英國半工半讀<br />
於第35集和唐明復合</p></td>
</tr>
</tbody>
</table>

### 鰂魚涌消防局

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/方中信.md" title="wikilink">方中信</a></strong></p></td>
<td><p><strong>唐　明</strong></p></td>
<td><p><strong>Wilson、唐Sir</strong><br />
高級消防隊長<br />
參見<strong><a href="../Page/#唐家.md" title="wikilink">唐家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐瑞偉.md" title="wikilink">歐瑞偉</a></p></td>
<td><p>鄭志光</p></td>
<td><p>消防總隊目</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林敬剛.md" title="wikilink">林敬剛</a></p></td>
<td><p>亞　飛</p></td>
<td><p>消防隊長</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬國明.md" title="wikilink">馬國明</a></p></td>
<td><p>Billy</p></td>
<td><p>消防隊目</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡諾言.md" title="wikilink">胡諾言</a></p></td>
<td><p>周兆輝</p></td>
<td><p>狗　仔<br />
消防員<br />
唐明下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杜港.md" title="wikilink">杜　港</a></p></td>
<td><p>亞　港</p></td>
<td><p>消防員<br />
唐明下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張國威.md" title="wikilink">張國威</a></p></td>
<td><p>亞　威</p></td>
<td></td>
</tr>
</tbody>
</table>

### 碧瑤消防局

[HK_Pok_Fu_Lam_Divisional_Fire_Station.JPG](https://zh.wikipedia.org/wiki/File:HK_Pok_Fu_Lam_Divisional_Fire_Station.JPG "fig:HK_Pok_Fu_Lam_Divisional_Fire_Station.JPG")約有一公里距離。\]\]

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/方中信.md" title="wikilink">方中信</a></strong></p></td>
<td><p><strong>唐　明</strong></p></td>
<td><p><strong>Wilson、唐Sir</strong><br />
高級消防隊長<br />
參見<strong><a href="../Page/#唐家.md" title="wikilink">唐家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林敬剛.md" title="wikilink">林敬剛</a></p></td>
<td><p>亞　飛</p></td>
<td><p>消防隊長</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/元華.md" title="wikilink">元　華</a></strong></p></td>
<td><p><strong>關安蔭</strong></p></td>
<td><p><strong>老鬼、老油條</strong><br />
消防總隊目<br />
唐明之好友<br />
康珊之前夫<br />
陳小蘭之父</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/王喜.md" title="wikilink">王　喜</a></strong></p></td>
<td><p><strong>紀德田</strong></p></td>
<td><p><strong>紀德</strong><br />
消防隊目<br />
唐明之下屬<br />
參見<strong><a href="../Page/#紀家.md" title="wikilink">紀家</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬國明.md" title="wikilink">馬國明</a></p></td>
<td><p>Billy</p></td>
<td><p>消防隊目</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐錦棠.md" title="wikilink">歐錦棠</a></strong></p></td>
<td><p><strong>蔡南豐</strong></p></td>
<td><p>消防員<br />
唐明之下屬<br />
參見<a href="../Page/#雷國彪拳術館.md" title="wikilink"><strong>雷國彪拳術館</strong></a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/蔣志光.md" title="wikilink">蔣志光</a></strong></p></td>
<td><p><strong>梁孟功</strong></p></td>
<td><p>消防員<br />
紀德田、林聚賢之好友<br />
唐明之下屬</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蔡子健.md" title="wikilink">蔡子健</a></strong></p></td>
<td><p><strong>林聚賢</strong></p></td>
<td><p><strong>賢仔</strong><br />
消防員<br />
紀德田、梁孟功之好友<br />
唐明之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡諾言.md" title="wikilink">胡諾言</a></p></td>
<td><p>周兆輝</p></td>
<td><p><strong>狗仔</strong><br />
消防員<br />
唐明之下屬</p></td>
</tr>
</tbody>
</table>

### 蝴蝶灣滅火輪消防局

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>紀興田</strong></p></td>
<td><p>消防隊長<br />
參見<strong><a href="../Page/#紀家.md" title="wikilink">紀家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/元華.md" title="wikilink">元　華</a></strong></p></td>
<td><p><strong>關安蔭</strong></p></td>
<td><p>消防總隊目<br />
參見<strong><a href="../Page/#碧瑤消防局.md" title="wikilink">碧瑤消防局</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凌子軒.md" title="wikilink">凌子軒</a></p></td>
<td><p>孫世英</p></td>
<td><p><strong>老鼠</strong><br />
消防員<br />
紀興田下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭祖.md" title="wikilink">鄭　祖</a></p></td>
<td><p>何漢坤</p></td>
<td><p><strong>大舊</strong><br />
消防隊目<br />
紀興田下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賀文傑.md" title="wikilink">賀文傑</a></p></td>
<td><p>馬　仔</p></td>
<td><p>消防員<br />
紀興田下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱志平.md" title="wikilink">朱志平</a></p></td>
<td><p>超　人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/唐寧_(香港).md" title="wikilink">唐　寧</a></strong></p></td>
<td><p><strong>陳小蘭</strong></p></td>
<td><p><strong>Joey</strong><br />
原名<strong>關穎恩</strong><br />
一級文員<br />
關安蔭、康珊之幼女<br />
因關安蔭小時候沒有從火場先救出哥哥東東，令其傷重不治，後被康珊帶去日本<br />
為了讓關安蔭和康珊復合，從日本回港進入與關安蔭同間消防局工作</p></td>
</tr>
</tbody>
</table>

### 火炭消防局

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俊棠.md" title="wikilink">王俊棠</a></p></td>
<td><p>張偉波</p></td>
<td><p><strong>波Sir</strong><br />
高級消防隊長<br />
唐明之前下屬</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>紀興田</strong></p></td>
<td><p>消防隊長<br />
於第26集調往此局工作<br />
參見<strong><a href="../Page/#紀家.md" title="wikilink">紀家</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭冠中.md" title="wikilink">彭冠中</a></p></td>
<td><p>王耀強</p></td>
<td><p>消防總隊目<br />
張偉波之下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊證樺.md" title="wikilink">楊證樺</a></p></td>
<td><p>亞　超</p></td>
<td><p>消防隊目<br />
張偉波之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凌子軒.md" title="wikilink">凌子軒</a></p></td>
<td><p>孫世英</p></td>
<td><p><strong>老鼠</strong><br />
消防員<br />
紀興田之下屬<br />
於第26集調往此局工作</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭祖.md" title="wikilink">鄭　祖</a></p></td>
<td><p>何漢坤</p></td>
<td><p><strong>大舊</strong><br />
消防員<br />
紀興田之下屬<br />
於第26集調往此局工作</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡烱龍.md" title="wikilink">胡烱龍</a></p></td>
<td><p>李威龍</p></td>
<td><p>消防員<br />
張偉波之下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張智軒.md" title="wikilink">張智軒</a></p></td>
<td><p>亞　浦</p></td>
<td><p>消防員<br />
張偉波之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/船員.md" title="wikilink">船　員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭世豪.md" title="wikilink">鄭世豪</a></p></td>
<td><p>亞　維</p></td>
<td><p>消防員<br />
張偉波之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙敏通.md" title="wikilink">趙敏通</a></p></td>
<td></td>
<td><p>消防員<br />
張偉波之下屬</p></td>
</tr>
</tbody>
</table>

### 消防潛水拯救隊（）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魏惠文.md" title="wikilink">魏惠文</a></p></td>
<td><p>關　標</p></td>
<td><p><strong>關Sir、標Sir</strong><br />
消防隊長</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/元華.md" title="wikilink">元　華</a></strong></p></td>
<td><p><strong>關安蔭</strong></p></td>
<td><p><strong>老鬼、蔭叔</strong><br />
消防總隊目<br />
參見<strong><a href="../Page/#碧瑤消防局.md" title="wikilink">碧瑤消防局</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/程小龍.md" title="wikilink">程小龍</a></p></td>
<td><p>陳德立</p></td>
<td><p><strong>立叔</strong><br />
消防總隊目</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a></p></td>
<td><p>張利喜</p></td>
<td><p>消防隊目</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙永洪.md" title="wikilink">趙永洪</a></p></td>
<td><p>亞　安</p></td>
<td><p>隊員</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 消防救護員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/張可頤.md" title="wikilink">張可頤</a></strong></p></td>
<td><p><strong>鍾茵怡</strong></p></td>
<td><p><strong>Chris</strong><br />
救護隊目，後為救護總隊目<br />
鍾廣成之女<br />
紀德田之女友<br />
紀興田之前女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李成昌.md" title="wikilink">李成昌</a></p></td>
<td><p>岑國昌</p></td>
<td><p><strong>岑哥、老總</strong><br />
救護總隊目</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阮德鏘.md" title="wikilink">阮德鏘</a></p></td>
<td><p>譚水和</p></td>
<td><p><strong>大泡和</strong><br />
救護員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳狄克.md" title="wikilink">陳狄克</a></p></td>
<td><p>歐陽克</p></td>
<td><p>救護員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/甘子正.md" title="wikilink">甘子正</a></p></td>
<td><p>亞　正</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁志達.md" title="wikilink">梁志達</a></p></td>
<td><p>亞　達</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕭徽勇.md" title="wikilink">蕭徽勇</a></p></td>
<td><p>救護員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>楊主任</p></td>
<td><p>達通貿易主任<br />
蔡南豐七姨之子 （第26集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭世豪.md" title="wikilink">鄭世豪</a></p></td>
<td><p>救護員</p></td>
<td><p>（第6集）</p></td>
</tr>
<tr class="odd">
<td><p>談判專家</p></td>
<td><p>（第14集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>亞　維</p></td>
<td><p>消防員<br />
參見<strong><a href="../Page/#火炭消防局.md" title="wikilink">火炭消防局</a></strong><br />
（第26-27集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁輝宗.md" title="wikilink">梁輝宗</a></p></td>
<td><p>救護員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁智達.md" title="wikilink">梁智達</a></p></td>
<td><p>（第30集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/嚴文軒.md" title="wikilink">嚴文軒</a></p></td>
<td><p>（第31集）</p></td>
<td></td>
</tr>
</tbody>
</table>

### 消防控制中心（）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/文頌嫻.md" title="wikilink">文頌嫻</a></strong></p></td>
<td><p><strong>葉向陽</strong></p></td>
<td><p><strong>Sunnie、師母、陽陽</strong><br />
消防隊目（控制）<br />
雷國彪之前未婚妻<br />
蔡南豐之女友<br />
江逸雅之好友<br />
於第11集和蔡南豐發生關係<br />
於第35集為蔡南豐生下3名女兒</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳楚翹.md" title="wikilink">陳楚翹</a></p></td>
<td><p>Amy</p></td>
<td><p>葉向陽之同事</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏竹欣.md" title="wikilink">夏竹欣</a></p></td>
<td><p>Zoe</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁珈詠.md" title="wikilink">梁珈詠</a></p></td>
<td><p>Cat</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>肥　仔</p></td>
<td></td>
</tr>
</tbody>
</table>

### 雷國彪拳術館

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李煒棋.md" title="wikilink">李煒棋</a></p></td>
<td><p>雷國彪</p></td>
<td><p>葉向陽之前未婚夫<br />
武館教頭兼創辦人<br />
蔡南豐、譚水和、歐思武之師父<br />
雷國彪將武館創辦人<br />
婚前因患感冒突然猝死</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/文頌嫻.md" title="wikilink">文頌嫻</a></strong></p></td>
<td><p><strong>葉向陽</strong></p></td>
<td><p><strong>Sunnie、師母、陽陽、消防隊目（控制）<br />
參見</strong><a href="../Page/#消防控制中心（Fire_Control_Centre）.md" title="wikilink">消防控制中心</a>'''</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐錦棠.md" title="wikilink">歐錦棠</a></strong></p></td>
<td><p><strong>蔡南豐</strong></p></td>
<td><p><strong>大師兄、功夫佬</strong><br />
消防員<br />
雷國彪拳術館之武師<br />
蔡少玲之兄<br />
紀瑤之舅父<br />
葉向陽之男友<br />
歐思武、譚水和、亞標、亞華、亞良之師兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵傳勇.md" title="wikilink">邵傳勇</a></p></td>
<td><p>歐思武</p></td>
<td><p><strong>亞武</strong><br />
蔡南豐之同門師弟</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阮德鏘.md" title="wikilink">阮德鏘</a></p></td>
<td><p>譚水和</p></td>
<td><p><strong>大泡和</strong><br />
救護員<br />
蔡南豐之同門師弟<br />
參見<strong><a href="../Page/#消防救護隊.md" title="wikilink">消防救護隊</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳少邦.md" title="wikilink">陳少邦</a></p></td>
<td><p>亞　標</p></td>
<td><p>蔡南豐之同門師弟</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳卓羲.md" title="wikilink">吳卓羲</a></p></td>
<td><p>亞　華</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葉暐.md" title="wikilink">葉　暐</a></p></td>
<td><p>亞　良</p></td>
<td></td>
</tr>
</tbody>
</table>

### 公關公司

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳慧珊.md" title="wikilink">陳慧珊</a></strong></p></td>
<td><p><strong>何寶琳</strong></p></td>
<td><p><strong>Michelle</strong><br />
公關公司老闆娘<br />
唐明前女友<br />
江逸雅之上司兼好友<br />
Gary之前女友<br />
於第29集知道自己是江逸雅婚姻的第三者<br />
於第30集把公司轉移到加拿大，並且到加拿大生活</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>江逸雅</strong></p></td>
<td><p><strong>Yan</strong><br />
何寶琳之下屬兼好友<br />
於第20集加入<br />
參見<strong><a href="../Page/#唐家.md" title="wikilink">唐家</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/簡慕華.md" title="wikilink">簡慕華</a></p></td>
<td><p>Ada</p></td>
<td><p>員工</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宋芝齡.md" title="wikilink">宋芝齡</a></p></td>
<td><p>Dora Sung</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周暘.md" title="wikilink">周　暘</a></p></td>
<td><p>Andrew</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李明麗.md" title="wikilink">李明麗</a></p></td>
<td><p>Alice</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭安彤.md" title="wikilink">鄭安彤</a></p></td>
<td><p>Anne</p></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱／關係</strong></p></td>
<td><p>'''備註</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/朱咪咪.md" title="wikilink">朱咪咪</a></strong></p></td>
<td><p><strong>王碧霞</strong></p></td>
<td><p>鍾茵怡母親之金蘭姊妹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉家輝.md" title="wikilink">劉家輝</a></p></td>
<td><p>鍾廣成</p></td>
<td><p>鍾茵怡之父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳德文.md" title="wikilink">陳德文</a></p></td>
<td><p>James</p></td>
<td><p>何寶琳之同事</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾曉瑩.md" title="wikilink">鍾曉瑩</a></p></td>
<td><p>Mandy</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃梓瑋.md" title="wikilink">黃梓瑋</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁健平.md" title="wikilink">梁健平</a></p></td>
<td><p>羅成傑</p></td>
<td><p><strong>羅Sir</strong><br />
高級消防區長</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>劉　Sir</p></td>
<td><p>高級消防區長</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊瑞麟.md" title="wikilink">楊瑞麟</a></p></td>
<td><p>何　Sir</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張雷.md" title="wikilink">張　雷</a></p></td>
<td><p>李Sir</p></td>
<td><p>消防總長（Chief Fire Officer）<br />
康珊之多年好友<br />
陳小蘭之乾爹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/官仲銘.md" title="wikilink">官仲銘</a></p></td>
<td><p><a href="../Page/消防員.md" title="wikilink">消防員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藥房.md" title="wikilink">藥房職員</a></p></td>
<td></td>
<td><p>（第21集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周景輝.md" title="wikilink">周景輝</a></p></td>
<td><p><a href="../Page/消防員.md" title="wikilink">消防員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/呂沛霖.md" title="wikilink">呂沛霖</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃子衡.md" title="wikilink">黃子衡</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃俊紳.md" title="wikilink">黃俊紳</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾慧雲.md" title="wikilink">曾慧雲</a></p></td>
<td><p>何太太</p></td>
<td><p>何家信之母<br />
紀德田及紀瑤之鄰居</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/薛立賢.md" title="wikilink">薛立賢</a></p></td>
<td><p>何家信</p></td>
<td><p>何太太之子<br />
紀瑤之鄰居兼好友</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丁主惠.md" title="wikilink">丁主惠</a></p></td>
<td><p>邦仔之母</p></td>
<td></td>
<td><p>（第1集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宋紫盈.md" title="wikilink">宋紫盈</a></p></td>
<td><p>黃主任</p></td>
<td><p>紀瑤之<a href="../Page/小學.md" title="wikilink">小學</a><a href="../Page/老師.md" title="wikilink">老師</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張鴻昌.md" title="wikilink">張鴻昌</a></p></td>
<td><p><a href="../Page/船長.md" title="wikilink">船　長</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td><p>遊輪上女童之母<br />
困在304室</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱婉儀.md" title="wikilink">朱婉儀</a></p></td>
<td><p>May</p></td>
<td><p>蔡南豐之前女友</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虞天偉.md" title="wikilink">虞天偉</a></p></td>
<td><p><a href="../Page/消防訓練學校.md" title="wikilink">消防訓練學校導師</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>考　官</p></td>
<td></td>
<td><p>（第26集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何仲偉.md" title="wikilink">何仲偉</a></p></td>
<td><p><a href="../Page/#潛水拯救隊.md" title="wikilink">潛水拯救隊學員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅振鵬.md" title="wikilink">羅振鵬</a></p></td>
<td><p><a href="../Page/#潛水拯救隊.md" title="wikilink">潛水拯救隊學員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Gary公司職員</p></td>
<td></td>
<td><p>（第13集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凌禮文.md" title="wikilink">凌禮文</a></p></td>
<td><p>七　叔</p></td>
<td><p><a href="../Page/茶餐廳.md" title="wikilink">茶餐廳</a><a href="../Page/老闆.md" title="wikilink">老闆</a><br />
紀德田之遠房親戚</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎秀英.md" title="wikilink">黎秀英</a></p></td>
<td><p>七　嬸</p></td>
<td><p>茶餐廳老闆娘<br />
紀德田之遠房親戚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黎宣.md" title="wikilink">黎　宣</a></p></td>
<td><p>六　姐</p></td>
<td><p>何寶琳之多年家傭<br />
第26集中風入院<br />
第30集出院<br />
居於置富道260號樂雲閣4樓A座</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊浚廷.md" title="wikilink">楊浚廷</a></p></td>
<td><p>速遞員</p></td>
<td></td>
<td><p>（第9集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/章志文.md" title="wikilink">章志文</a></p></td>
<td><p>亞　夏</p></td>
<td><p>蔡南豐之同屆消防學員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麻醉師.md" title="wikilink">麻醉師</a></p></td>
<td></td>
<td><p>（第33集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何偉業.md" title="wikilink">何偉業</a></p></td>
<td><p>亞　寶</p></td>
<td><p>蔡南豐之同屆消防學員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周凱珊.md" title="wikilink">周凱珊</a></p></td>
<td><p>Cat</p></td>
<td><p>紀興田之友</p></td>
<td><p>（第10集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李啟傑.md" title="wikilink">李啟傑</a></p></td>
<td><p>武師柴</p></td>
<td><p>蔡南豐之友</p></td>
<td><p>（第11集）</p></td>
</tr>
<tr class="even">
<td><p>警隊便衣<a href="../Page/探員.md" title="wikilink">探員</a></p></td>
<td></td>
<td><p>（第22集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盧永匡.md" title="wikilink">盧永匡</a></p></td>
<td><p>武師忠</p></td>
<td><p>蔡南豐之友</p></td>
<td><p>（第11集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鍾建文.md" title="wikilink">鍾建文</a></p></td>
<td><p><a href="../Page/汽車.md" title="wikilink">汽車經紀</a></p></td>
<td></td>
<td><p>（第12集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張貝茜.md" title="wikilink">張貝茜</a></p></td>
<td><p>全景大廈被困在電梯死者</p></td>
<td></td>
<td><p>碧瑤小路二號<br />
（第12集）</p></td>
</tr>
<tr class="even">
<td><p>梁孟功之表妹</p></td>
<td><p>紀德田之相親對象</p></td>
<td><p>（第29集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧泰和.md" title="wikilink">鄧泰和</a></p></td>
<td><p>持雙程證之賊</p></td>
<td></td>
<td><p>打傷紀德田頭部<br />
（第12集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭耀明.md" title="wikilink">郭耀明</a></p></td>
<td><p>Gary</p></td>
<td><p>經理<br />
已婚<br />
何寶琳之前男友</p></td>
<td><p>（第13-14集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅泳嫻.md" title="wikilink">羅泳嫻</a></p></td>
<td><p>Gary之妻</p></td>
<td></td>
<td><p>（第14集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅天池.md" title="wikilink">羅天池</a></p></td>
<td><p>Gary之同事</p></td>
<td></td>
<td><p>（第14集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧浩光.md" title="wikilink">鄧浩光</a></p></td>
<td><p>亞　峰</p></td>
<td><p>唐明之<a href="../Page/大學.md" title="wikilink">大學同學兼好友</a><br />
<a href="../Page/南丫島.md" title="wikilink">南丫島</a><a href="../Page/餐館.md" title="wikilink">餐廳</a><a href="../Page/老闆.md" title="wikilink">老闆</a><br />
前<a href="../Page/消防員.md" title="wikilink">消防員</a></p></td>
<td><p>（第15-16集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傅楚卉.md" title="wikilink">傅楚卉</a></p></td>
<td><p>琪　琪</p></td>
<td><p>亞峰之妻<br />
<a href="../Page/南丫島.md" title="wikilink">南丫島</a><a href="../Page/餐館.md" title="wikilink">餐廳</a><a href="../Page/老闆.md" title="wikilink">老闆娘</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/呂珊.md" title="wikilink">呂　珊</a></p></td>
<td><p>康　珊</p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本名牌代理商人</a><br />
關安蔭之前妻<br />
東東及陳小蘭之母<br />
李Sir之多年好友</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳念君.md" title="wikilink">陳念君</a></p></td>
<td><p>叉燒母（第7集）<br />
似琳女子（第17集）</p></td>
<td></td>
<td><p>竹棚受傷小童的母親<br />
被唐明誤認為何寶琳</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃可惠.md" title="wikilink">黃可惠</a></p></td>
<td><p>鄭志光之妻</p></td>
<td></td>
<td><p>（第18集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王冠忠.md" title="wikilink">王冠忠</a></p></td>
<td><p>何寶琳之友</p></td>
<td></td>
<td><p>（第18集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭玉京.md" title="wikilink">鄭玉京</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃煒林.md" title="wikilink">黃煒林</a></p></td>
<td><p>消防隊長</p></td>
<td><p>15年前關安蔭之上司</p></td>
<td><p>（第18集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃天鐸.md" title="wikilink">黃天鐸</a></p></td>
<td><p><a href="../Page/傢俬.md" title="wikilink">傢俬店員</a></p></td>
<td></td>
<td><p>（第19集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅蘭.md" title="wikilink">羅　蘭</a></p></td>
<td><p>婆　婆<br />
公公之妻</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孫季卿.md" title="wikilink">孫季卿</a></p></td>
<td><p>公　公<br />
婆婆之夫<br />
死者</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃振威.md" title="wikilink">黃振威</a></p></td>
<td><p>護衛員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張漢斌.md" title="wikilink">張漢斌</a></p></td>
<td><p><a href="../Page/卡拉OK.md" title="wikilink">卡拉OK經理</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳榮峻.md" title="wikilink">陳榮峻</a></p></td>
<td><p>亞　達</p></td>
<td><p><strong>達叔</strong><br />
<a href="../Page/便利店.md" title="wikilink">便利店員工</a><br />
梁愛芬之父<br />
約12年前被鍾茵怡打傷<br />
<a href="../Page/心臟病.md" title="wikilink">心臟病病逝</a>（第31集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岑寶兒.md" title="wikilink">岑寶兒</a></p></td>
<td><p>梁愛芬</p></td>
<td><p><strong>Fanny</strong><br />
亞達及亞嫻之女<br />
約12年前的傷人案目擊者，於12年後，為此事不停要脅鍾茵怡</p></td>
<td><p>（第19集出現）<br />
（第34集）失血過多而死</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇恩磁.md" title="wikilink">蘇恩磁</a></p></td>
<td><p>亞　嫻</p></td>
<td><p>亞達之妻<br />
梁愛芬之母</p></td>
<td><p>（第23-24、31集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/-.md" title="wikilink">-</a></p></td>
<td><p>東　東</p></td>
<td><p>關安蔭及康珊之長子<br />
（約15年前已去世）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李永豪.md" title="wikilink">李永豪</a></p></td>
<td><p>大頭B</p></td>
<td><p>東東兒時之好友</p></td>
<td><p>（第20集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何志祥.md" title="wikilink">何志祥</a></p></td>
<td><p>青頭華</p></td>
<td><p>梁愛芬之友</p></td>
<td><p>（第20集出現）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜大偉.md" title="wikilink">杜大偉</a></p></td>
<td><p>金毛強</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚權輝.md" title="wikilink">譚權輝</a></p></td>
<td><p>白毛昌</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林遠迎.md" title="wikilink">林遠迎</a></p></td>
<td><p>紀興田之舊同學</p></td>
<td></td>
<td><p>（第21集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林佩君.md" title="wikilink">林佩君</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/顏頌詩.md" title="wikilink">顏頌詩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岳峰.md" title="wikilink">岳　峰</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湯俊明.md" title="wikilink">湯俊明</a></p></td>
<td><p>Paul</p></td>
<td><p><a href="../Page/律師.md" title="wikilink">律師</a><br />
紀興田之舊同學<br />
鍾茵怡之代表<a href="../Page/律師.md" title="wikilink">律師</a></p></td>
<td><p>（第21-23集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江暉.md" title="wikilink">江　暉</a></p></td>
<td><p><a href="../Page/侍應生.md" title="wikilink">侍應生</a></p></td>
<td></td>
<td><p>（第21集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李鴻傑.md" title="wikilink">李鴻傑</a></p></td>
<td><p>林老闆</p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣華期公司之</a><a href="../Page/老闆.md" title="wikilink">老闆</a><br />
何寶琳公司之客戶</p></td>
<td><p>（第21集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾健明.md" title="wikilink">曾健明</a></p></td>
<td><p>陳老闆</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/招石文.md" title="wikilink">招石文</a></p></td>
<td><p>李老闆</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃鳳琼.md" title="wikilink">黃鳳-{琼}-</a></p></td>
<td><p><a href="../Page/孕婦.md" title="wikilink">孕　婦</a></p></td>
<td><p>在士美菲路士美邨後樓梯產子</p></td>
<td><p>（第22、26集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>老　陳</p></td>
<td><p>受傷工人</p></td>
<td><p>（第22集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李海生.md" title="wikilink">李海生</a></p></td>
<td><p>管　工</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡啟光.md" title="wikilink">胡啟光</a></p></td>
<td><p><a href="../Page/保安員.md" title="wikilink">保安員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何家俊.md" title="wikilink">何家俊</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伍慧珊.md" title="wikilink">伍慧珊</a></p></td>
<td><p>警隊便衣<a href="../Page/探員.md" title="wikilink">探員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳中堅.md" title="wikilink">陳中堅</a></p></td>
<td><p>徐　伯</p></td>
<td><p>報案其妻失蹤</p></td>
<td><p>（第24集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉志清.md" title="wikilink">劉志清</a></p></td>
<td><p>Gilbert</p></td>
<td><p><a href="../Page/直銷.md" title="wikilink">傳銷員</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/-.md" title="wikilink">-</a></p></td>
<td><p>辦理離婚<a href="../Page/律師.md" title="wikilink">律師</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃嘉樂.md" title="wikilink">黃嘉樂</a></p></td>
<td><p><a href="../Page/伴郎.md" title="wikilink">伴　郎</a></p></td>
<td><p>唐明及江逸雅的<a href="../Page/伴郎.md" title="wikilink">伴郎</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張雪芹.md" title="wikilink">張雪芹</a></p></td>
<td><p><a href="../Page/伴娘.md" title="wikilink">伴　娘</a></p></td>
<td><p>唐明及江逸雅的<a href="../Page/伴娘.md" title="wikilink">伴娘</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/影院.md" title="wikilink">影院售票員</a></p></td>
<td></td>
<td><p>（第31集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凌漢_(演員).md" title="wikilink">凌　漢</a></p></td>
<td><p>管理員</p></td>
<td></td>
<td><p>（第26集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳文靜.md" title="wikilink">陳文靜</a></p></td>
<td><p>達通貿易女職員</p></td>
<td></td>
<td><p>（第26集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳丹丹.md" title="wikilink">陳丹丹</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘冠霖.md" title="wikilink">潘冠霖</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/-.md" title="wikilink">-</a></p></td>
<td><p>打遊戲機之青年</p></td>
<td></td>
<td><p>（第26集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳亦謙.md" title="wikilink">吳亦謙</a></p></td>
<td><p><a href="../Page/醫生.md" title="wikilink">醫　生</a></p></td>
<td></td>
<td><p>（第26集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄺佐輝.md" title="wikilink">鄺佐輝</a></p></td>
<td><p>陳醫生</p></td>
<td><p>唐明之主診<a href="../Page/醫生.md" title="wikilink">醫生</a></p></td>
<td><p>（第32-33集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卓兒.md" title="wikilink">卓　兒</a></p></td>
<td><p><a href="../Page/護士.md" title="wikilink">護　士</a></p></td>
<td></td>
<td><p>（第1、27集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇麗明.md" title="wikilink">蘇麗明</a></p></td>
<td></td>
<td><p>（第32-33集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧英敏.md" title="wikilink">鄧英敏</a></p></td>
<td><p>新聞<a href="../Page/記者.md" title="wikilink">記者</a></p></td>
<td></td>
<td><p>（第30集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何綺雲.md" title="wikilink">何綺雲</a></p></td>
<td><p><a href="../Page/新聞報導員.md" title="wikilink">新聞報導員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>方洪武</p></td>
<td><p>美都科技集團<a href="../Page/董事長.md" title="wikilink">主席</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王少萍.md" title="wikilink">王少萍</a></p></td>
<td><p><a href="../Page/電視台.md" title="wikilink">電視台</a><a href="../Page/記者.md" title="wikilink">記者</a></p></td>
<td></td>
<td><p>（第31集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余慕蓮.md" title="wikilink">余慕蓮</a></p></td>
<td><p>梁愛芬之女房東</p></td>
<td></td>
<td><p>（第34集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李思蓓.md" title="wikilink">李思蓓</a></p></td>
<td><p><a href="../Page/唱片騎師.md" title="wikilink">唱片騎師</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧汝超.md" title="wikilink">鄧汝超</a></p></td>
<td><p>陳先生</p></td>
<td><p>家居火鍋氣體包裝工場之戶主</p></td>
<td><p>（第35集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尤程.md" title="wikilink">尤　程</a></p></td>
<td><p>陳太太</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/-.md" title="wikilink">-</a></p></td>
<td><p><a href="../Page/談判專家.md" title="wikilink">談判專家</a></p></td>
<td><p>|</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 小趣事

本劇中奇怪地亦同上集《[烈火雄心](../Page/烈火雄心.md "wikilink")》一樣近半
角色的名字均取自香港的公共屋邨、居屋及私人屋苑的名稱。較明顯的例子包括紀德田－[德田邨](../Page/德田邨.md "wikilink")、紀興田－[興田邨](../Page/興田邨.md "wikilink")、唐明－[唐明苑](../Page/唐明苑.md "wikilink")、
鍾茵怡－
[茵怡花園](../Page/茵怡花園.md "wikilink")、關安蔭－[安蔭邨](../Page/安蔭邨.md "wikilink")、蔡南豐－[南豐新邨等等](../Page/南豐新邨.md "wikilink")。

此外，本劇的消防局《鰂魚涌消防局》、《火炭消防局》、《碧瑤消防局》、及《蝴蝶灣滅火輪消防局》全是虛構出來，劇中的「碧瑤消防局」現實名稱為「薄扶林消防局」，與碧瑤灣約有一公里距離。而劇中早期部份取景就用「北角消防局」去替代「鰂魚涌消防局」。「火炭消防局」現實名稱為「沙田消防局」。而蝴蝶灣滅火輪消防局則取景於屯門滅火輪消防局。

## 參考資料

## 外部連結

  - [無綫電視官方網頁 -
    烈火雄心II](https://web.archive.org/web/20110804223839/http://tvcity.tvb.com/drama/burn_flame2/)
  - [無綫電視官方網頁（TVBI） -
    烈火雄心II](http://www.tvb.com/tvbi/program/flameII/index.html)
  - [《烈火雄心II》 GOTV
    第1集重溫](https://web.archive.org/web/20140222162413/http://gotv.tvb.com/programme/102437/151492/)

## 電視節目的變遷

[Category:烈火雄心系列](../Category/烈火雄心系列.md "wikilink")
[Category:2002年無綫電視劇集](../Category/2002年無綫電視劇集.md "wikilink")
[Category:無綫電視2000年代背景劇集](../Category/無綫電視2000年代背景劇集.md "wikilink")