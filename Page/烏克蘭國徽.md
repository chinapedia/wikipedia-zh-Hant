**烏克蘭國徽**為藍色的盾徽，中間有金色的[三叉戟](../Page/三叉戟.md "wikilink")。這個圖案最初是[留里克王朝的](../Page/留里克王朝.md "wikilink")[印章圖案](../Page/印章.md "wikilink")，1918年3月22日曾被立為[烏克蘭人民共和國國徽](../Page/烏克蘭人民共和國.md "wikilink")。1992年2月19日經[烏克蘭最高拉達采納作為國徽](../Page/烏克蘭最高拉達.md "wikilink")。

## 歷史上的國徽

<center>

<File:Seal> of Sviatoslav I of Kiev-
Avers.png|[斯维亚托斯拉夫一世的徽章](../Page/斯维亚托斯拉夫·伊戈列维奇.md "wikilink")
<File:Coin_of_Vladimir_the_Great.jpg>|[弗拉基米尔一世时期硬币上的图案](../Page/弗拉基米尔一世.md "wikilink")
<File:Yarthewise.png>|[智者雅罗斯拉夫的徽章](../Page/智者雅罗斯拉夫.md "wikilink")
<File:Alex> K Halych-Volhynia.svg|罗斯王国（西乌克兰）国徽 <File:Alex> K Ukrainska
Derzhava.svg|乌克兰国国徽

<File:Organization> of Ukrainian Nationalists-M.svg|乌克兰民族主义者组织

</center>

### 乌克兰国国徽

<center>

<File:Alex> K Ukrainska
Derzhava.svg|[乌克兰国国徽](../Page/乌克兰国.md "wikilink")（1918年）

</center>

### 西乌克兰人民共和国国徽

西乌克兰人民共和国国徽启用于其独立时，与今日国徽略有不同，中间為金黃色的獅子。

<center>

<File:ZUNR>
coa.svg|[西乌克兰人民共和国国徽](../Page/西乌克兰人民共和国.md "wikilink")（1918年－1919年）

</center>

### 烏克蘭人民共和國國徽

烏克蘭人民共和國國徽啟用於其獨立時，與今國徽相似。

<center>

<File:Coat> of Arms of
UNR.svg|[烏克蘭人民共和國國徽](../Page/烏克蘭人民共和國.md "wikilink")（1918）
<File:Coat> of Arms of UNR-2.svg|烏克蘭人民共和國大國徽（1918）

</center>

### 喀尔巴阡卢森尼亚徽章

<center>

<File:Karptska> Ukraina
COA.svg|[喀尔巴阡卢森尼亚徽章](../Page/喀尔巴阡卢森尼亚.md "wikilink")
<File:Karpatska> Ukraina-2 COA.svg|外喀尔巴阡州徽章

</center>

### 烏克蘭蘇維埃社會主義共和國國徽

<center>

| 乌克兰苏维埃社会主义共和国国徽                                                                                        |
| ------------------------------------------------------------------------------------------------------ |
| [Герб_УССР_1919.jpg](https://zh.wikipedia.org/wiki/File:Герб_УССР_1919.jpg "fig:Герб_УССР_1919.jpg") |
| 1919年-1929年                                                                                            |

</center>

### 其他版本

Image:USS kokarda.svg|徽章上的[步枪兵徽章](../Page/步枪兵.md "wikilink")
Image:Organization_of_Ukrainian_Nationalists-M.svg|[乌克兰民族主义者组织](../Page/乌克兰民族主义者组织.md "wikilink")
1929 - 2010
Image:OUN-B-01.svg|[乌克兰民族主义者组织](../Page/乌克兰民族主义者组织.md "wikilink")
1940年至1958年 Image:Dyvizia
Galychyna.svg|[党卫队第14武装掷弹兵师（第1乌克兰人）](../Page/党卫队第14武装掷弹兵师（第1乌克兰人）.md "wikilink")
Image:Lviv coat
sov.png|[苏联](../Page/苏联.md "wikilink")[利沃夫](../Page/利沃夫.md "wikilink")（1967
- 1990年） <File:POL> Kamieniec Podolski COA.svg|（1374-1796）的卡缅涅茨 -
波多利斯基（Kamianets-Podilskyi）市的波格罗斯卡（Pogon
Ruska）的变化直到\[俄罗斯帝国\]占领波多利亚
[File:Nizhyn-COA.jpg|无髯拉斯克的变化为涅任城市](File:Nizhyn-COA.jpg%7C无髯拉斯克的变化为涅任城市)
Image:Coat_of_arms_of_the_January_Uprising.svg|[1月起义](../Page/1月起义.md "wikilink")
[波兰](../Page/波兰.md "wikilink") - [立陶宛](../Page/立陶宛.md "wikilink")
-[鲁塞尼亚](../Page/鲁塞尼亚.md "wikilink")[联邦的徽章](../Page/联邦.md "wikilink")
<File:Lesser> Coat of Arms of Ukraine
(1992).svg|[烏克蘭小國徽](../Page/烏克蘭.md "wikilink")（1992年）

### 大国徽

Image:Bytynsʹkyĭ Coat of Arms of Ukraine Project 03.png|迈科拉·比提斯克伊设计的徽章
Image:Bytynsʹkyĭ Coat of Arms of Ukraine Project 01.png|迈科拉·比提斯克伊设计的徽章
Image:Bytynsʹkyĭ Coat of Arms of Ukraine Project 02.png|迈科拉·比提斯克伊设计的徽章
Image:UNR coa projects.svg|赫鲁舍夫斯基的设计 Image:Big emblem of
Ukraine.svg|1996年的主张

[烏克蘭憲法曾提及要設計大國徽](../Page/烏克蘭.md "wikilink"),\[1\]但從來沒有被採納。然而，大國徽又被刊載在不同文獻之中。在这个变体中，盾牌由左侧的[加利西亚徽章和身着传统服装的哥萨克支持](../Page/加利西亚（中欧）.md "wikilink")，手持一个[滑膛枪](../Page/滑膛枪.md "wikilink")，是科扎克的象征
[哥萨克将军的职权在右边](../Page/哥萨克.md "wikilink")。
徽章上加有[弗拉基米尔大帝的冠冕](../Page/弗拉基米尔大帝.md "wikilink")，象征着乌克兰[主权](../Page/主权.md "wikilink")，底部装饰着[荚和小麦](../Page/荚.md "wikilink")。

乌克兰议会必须通过大国徽的正式通过，得到乌克兰议会三分之二多数的赞成。

在1917年[迈克哈尔洛·拉什维斯基提出武器大大衣通过与橄榄枝鸽子淋上单个屏蔽件的形式](../Page/迈克哈尔洛·拉什维斯基.md "wikilink")。盾牌分成五种方式。在它的中心有一个小盾牌描绘\[犁\]作为由老乌克兰的国家象征包围着富有成效的和平工作的一个符号：弗拉基米尔伟大[沃洛大王侯武器](../Page/沃洛大.md "wikilink")（tryzub）
[利特无髯带有金色狮子](../Page/利特温驱动器.md "wikilink")，[带着步枪的哥萨克](../Page/带着步枪的哥萨克.md "wikilink")，基辅的十字弓和利沃夫的狮子。

[U](../Category/國徽.md "wikilink")
[Category:乌克兰国家象征](../Category/乌克兰国家象征.md "wikilink")

1.  ，[第20条](../Page/乌克兰宪法第20条.md "wikilink")。