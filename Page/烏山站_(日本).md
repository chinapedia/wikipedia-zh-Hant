**烏山站**（）是位於[日本](../Page/日本.md "wikilink")[栃木縣](../Page/栃木縣.md "wikilink")[那須烏山市南二丁目的](../Page/那須烏山市.md "wikilink")[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）[烏山線鐵路車站](../Page/烏山線.md "wikilink")。

## 車站構造

[EV-E300-1_V1_at_karasuyama_station.JPG](https://zh.wikipedia.org/wiki/File:EV-E300-1_V1_at_karasuyama_station.JPG "fig:EV-E300-1_V1_at_karasuyama_station.JPG")後，車頂的集電弓升起以利用站內設置的剛性懸掛高架供電線進行充電。EV-E301系V1編組，2014年攝於烏山車站內。\]\]

  - 設有[綠色窗口](../Page/綠色窗口.md "wikilink")。

原為[側式月台](../Page/側式月台.md "wikilink")2面2線設計，但基本上只使用站房側的軌道。其後2012年2月因烏山線被用作測試以蓄電池驅動的電聯車，外側的軌道被移除，用作安裝充電裝置。測試完成後，JR東日本決定將此系統實用化，並在烏山線引入[JR東日本EV-E301系電聯車](../Page/JR東日本EV-E301系電聯車.md "wikilink")。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1</strong></p></td>
<td><p>烏山線</p></td>
<td><p><a href="../Page/大金站.md" title="wikilink">大金</a>、<a href="../Page/寶積寺站.md" title="wikilink">寶積寺</a>、<a href="../Page/宇都宮站.md" title="wikilink">宇都宮方向</a></p></td>
</tr>
</tbody>
</table>

## 利用狀況

  - 1日平均乘車人員 670人（2006年度）

## 車站周邊

  - 會館

  - [國道294號](../Page/國道294號.md "wikilink")

  - 栃木縣立烏山高等學校

  - 栃木縣立烏山女子高等學校

  - 那須烏山市政府 烏山廳舍（舊、[烏山町公所](../Page/烏山町.md "wikilink")）

  - 烏山郵局

  - 島崎酒造（日本酒「東力士」的釀造家）

## 巴士路線

  - 那須烏山市營巴士
      - 高部車庫行
      - 瀧見谷循環線
      - 國見蕨莊、四斗蒔行
      - [市塙站](../Page/市塙站.md "wikilink")、市貝溫泉行
  - [JR巴士關東](../Page/JR巴士關東.md "wikilink")
      - 馬頭、藤澤行
  - [櫻市營巴士](../Page/櫻市.md "wikilink")
      - 喜連川溫泉、[片岡站行](../Page/片岡站.md "wikilink")
  - [市貝町營巴士](../Page/市貝町.md "wikilink")
      - 【市塙黑田烏山線】市塙站、市塙温泉行(經由市貝町公所)

## 历史

  - 1923年4月15日 - 開業。

## 相鄰車站

  - 東日本旅客鐵道

    烏山線

      -
        [瀧](../Page/瀧站_\(栃木縣\).md "wikilink")－**烏山**

## 參考文獻

## 相關條目

  - [日本鐵路車站列表 Ka](../Page/日本鐵路車站列表_Ka.md "wikilink")

## 外部連結

  - [JR東日本－烏山站](http://www.jreast.co.jp/estation/station/info.aspx?StationCd=513)

[Rasuyama](../Category/日本鐵路車站_Ka.md "wikilink")
[Category:烏山線車站](../Category/烏山線車站.md "wikilink")
[Category:栃木縣鐵路車站](../Category/栃木縣鐵路車站.md "wikilink")
[Category:1923年啟用的鐵路車站](../Category/1923年啟用的鐵路車站.md "wikilink")
[Category:那須烏山市](../Category/那須烏山市.md "wikilink")