{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Lake Silver 201707.jpg | caption1 = 車站外觀 }}
**烏溪沙站**（），是[港鐵](../Page/港鐵.md "wikilink")[馬鞍山綫的上行終點站](../Page/馬鞍山綫.md "wikilink")，位於[烏溪沙](../Page/烏溪沙.md "wikilink")[西沙路旁側](../Page/西沙路.md "wikilink")，是該線其中一個架空車站，取名自鄰近的[烏溪沙和](../Page/烏溪沙.md "wikilink")[烏溪沙村](../Page/烏溪沙村.md "wikilink")，於2004年12月21日啟用。

## 結構

### 樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>P</strong></p></td>
<td></td>
<td><p>馬鞍山綫往<a href="../Page/大圍站.md" title="wikilink">大圍</a><small>（<a href="../Page/馬鞍山站_(香港).md" title="wikilink">馬鞍山</a>）</small></p></td>
</tr>
<tr class="even">
<td><p>島式月台，開左/右邊門</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>馬鞍山綫往大圍<small>（馬鞍山）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>C</strong></p></td>
<td><p>大堂</p></td>
<td><p>A1、B出入口、客務中心、商店、自助服務、洗手間</p></td>
</tr>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>地面</p></td>
<td><p>A2出入口</p></td>
</tr>
</tbody>
</table>

### 大堂

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Wu Kai Sha Station 2017 11 part6.jpg | caption1 = 大堂

}}
大堂入付費區設有洗手間供乘客使用。車站出入口設有客務中心、增值機和售票機，另外亦有為盲人指導的錄音提示器。此外，出入口亦設有特闊閘機，方便有需要人士出入閘。

因站內原先預計人流不多，烏溪沙站只有少量商店，包括[便利店](../Page/便利商店.md "wikilink")、[銀行](../Page/銀行.md "wikilink")、[唐記包點](../Page/唐記包點.md "wikilink")、勝豐壽司及兩間地產代理，站內亦設有兩部[自動櫃員機及](../Page/自動櫃員機.md "wikilink")4個報紙櫃供乘客於早上免費取閱。\[1\]。

[Wu_Kai_Sha_Station_2017_11_part5.jpg](https://zh.wikipedia.org/wiki/File:Wu_Kai_Sha_Station_2017_11_part5.jpg "fig:Wu_Kai_Sha_Station_2017_11_part5.jpg")

### 月台

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Wu Kai Sha Station 2018 06 part4.jpg | caption1 = 1號月台 | image2
= Wu Kai Sha Station 2018 06 part6.jpg | caption2 = 月台佈置 }}
烏溪沙站設有兩個月台，以一個島式月台設計排列。月台可容納8卡列車，

[Wu_Kai_Sha_Station_2018_06_part1.jpg](https://zh.wikipedia.org/wiki/File:Wu_Kai_Sha_Station_2018_06_part1.jpg "fig:Wu_Kai_Sha_Station_2018_06_part1.jpg")
[Wu_Kai_Sha_Station_2018_06_part2.jpg](https://zh.wikipedia.org/wiki/File:Wu_Kai_Sha_Station_2018_06_part2.jpg "fig:Wu_Kai_Sha_Station_2018_06_part2.jpg")

### 月台閘門

Wu Kai Sha Station platform extension.jpg|1號月台擴展部份及月台閘門工程 Wu Kai Sha
Station platform extension (2).jpg|2號月台擴展部份及月台閘門工程

烏溪沙站的月台已在2017年安裝了[月台閘門](../Page/月台閘門.md "wikilink")。

### 出入口

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = MTR WKS (4).JPG | caption1 = A1出入口 | image2 = Wu Kai Sha
Station Exit B 201108.jpg | caption2 = B出入口 }}
烏溪沙站設有三個出入口，車站的北面是通往[西沙路的](../Page/西沙路.md "wikilink")[行人天橋](../Page/行人天橋.md "wikilink")，西面為[利安邨和](../Page/利安邨.md "wikilink")[錦龍苑](../Page/錦龍苑.md "wikilink")，其上蓋是私人屋苑[銀湖·天峰](../Page/銀湖·天峰.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>指示</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/迎海.md" title="wikilink">迎海</a></p></td>
<td><p>迎海、<a href="../Page/西沙路.md" title="wikilink">西沙路</a>、樟木頭老人度假中心、<a href="../Page/香港浸信會神學院.md" title="wikilink">香港浸信會神學院</a>、<a href="../Page/香港李寶椿聯合世界書院.md" title="wikilink">香港李寶椿聯合世界書院</a>、<a href="../Page/落禾沙.md" title="wikilink">落禾沙</a>、<a href="../Page/帝琴灣.md" title="wikilink">帝琴灣</a>、<a href="../Page/凱弦居.md" title="wikilink">凱弦居</a>、<a href="../Page/凱琴居.md" title="wikilink">凱琴居</a>、烏溪沙新村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>西沙路</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/銀湖·天峰.md" title="wikilink">銀湖·天峰</a></p></td>
<td><p>銀湖·天峰、公共運輸交匯處、<a href="../Page/明愛馬鞍山中學.md" title="wikilink">明愛馬鞍山中學</a>、<a href="../Page/英基國際幼稚園.md" title="wikilink">英基國際幼稚園</a>、<a href="../Page/錦龍苑.md" title="wikilink">錦龍苑</a>、<a href="../Page/利安邨.md" title="wikilink">利安邨</a>、<a href="../Page/馬鞍山靈糧小學.md" title="wikilink">馬鞍山靈糧小學</a>、<a href="../Page/翠擁華庭.md" title="wikilink">翠擁華庭</a>、<a href="../Page/富寶花園.md" title="wikilink">富寶花園</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰接車站

烏溪沙站是馬鞍山綫的下行起點，乘客可在此乘坐往[大圍列車](../Page/大圍站.md "wikilink")，往返新界東北或九龍各區。

[Wu_Kai_Sha_Station_2018_06_part5.jpg](https://zh.wikipedia.org/wiki/File:Wu_Kai_Sha_Station_2018_06_part5.jpg "fig:Wu_Kai_Sha_Station_2018_06_part5.jpg")

## 接駁交通

烏溪沙站南面設有[烏溪沙站公共運輸交匯處](../Page/烏溪沙站公共運輸交匯處.md "wikilink")，位於B出入口。

<table>
<thead>
<tr class="header">
<th><p>接駁交通列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 歷史

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Wu Kai Sha Station.jpg | caption1 = 兩鐵合併前的月台（2005年） }}
馬鞍山綫通車前曾命名為**利安站**（），因為車站位於利安邨的東面\[2\]。

## 未來發展

當沙中綫東西走廊工程完成後，新建路段將會與西鐵綫和馬鞍山綫合併，並易名為屯馬綫，烏溪沙站將成為屯馬綫的總站，日後在本站前往大圍至九龍城以至紅磡以西或尖沙咀、西九龍、深水埗、荃灣、元朗、天水圍及屯門一帶，將毋須轉乘其他路綫就可以直接到達。

### 未來樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>P</strong></p></td>
<td></td>
<td><p>屯馬綫往<a href="../Page/屯門站_(西鐵綫).md" title="wikilink">屯門</a><small>（<a href="../Page/馬鞍山站_(香港).md" title="wikilink">馬鞍山</a>）</small></p></td>
</tr>
<tr class="even">
<td><p>島式月台，開左/右邊門</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>屯馬綫往屯門<small>（馬鞍山）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>C</strong></p></td>
<td><p>大堂</p></td>
<td><p>A1、B出入口、客務中心、商店、自助服務、洗手間</p></td>
</tr>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>地面</p></td>
<td><p>A2出入口</p></td>
</tr>
</tbody>
</table>

## 相關條目

  - 上蓋物業——[銀湖·天峰](../Page/銀湖·天峰.md "wikilink")

## 參考資料

## 外部連結

  - [港鐵公司－烏溪沙站街道圖](http://www.mtr.com.hk/archive/ch/services/maps/wks.pdf)
  - [港鐵公司－烏溪沙站位置圖](http://www.mtr.com.hk/archive/ch/services/layouts/wks.pdf)
  - [港鐵公司－烏溪沙站列車服務時間](http://www.mtr.com.hk/archive/ch/services/timetables/wks.pdf)

[Category:烏溪沙](../Category/烏溪沙.md "wikilink")
[Category:2004年啟用的鐵路車站](../Category/2004年啟用的鐵路車站.md "wikilink")
[Category:前馬鞍山鐵路車站](../Category/前馬鞍山鐵路車站.md "wikilink")
[Category:馬鞍山綫車站](../Category/馬鞍山綫車站.md "wikilink")
[Category:沙田區鐵路車站](../Category/沙田區鐵路車站.md "wikilink")

1.
2.