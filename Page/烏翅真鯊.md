**烏翅真鯊**（[學名](../Page/學名.md "wikilink")：**），又名**污翅白眼鮫**、**黑翼鯊**、**伯爵鯊**、**黑鰭鯊**或**黑鰭礁鯊**，是一種生活在熱帶及溫和[海洋的](../Page/海洋.md "wikilink")[鯊魚](../Page/鯊魚.md "wikilink")。牠經常被誤會為[黑邊鰭真鯊](../Page/黑邊鰭真鯊.md "wikilink")（學名*Carcharhinus
limbatus*）。

## 分佈

烏翅真鯊是現今在印度、太平洋及[加勒比海](../Page/加勒比海.md "wikilink")[珊瑚礁附近淺水區](../Page/珊瑚礁.md "wikilink")（有時會在30厘米深的淺水區）最常見的[鯊魚之一](../Page/鯊魚.md "wikilink")。牠們生活在20℃至27℃水溫的[海洋](../Page/海洋.md "wikilink")，並且不會冒險進入一些遠離海洋的熱帶湖或河流中。

## 外表

[Carcharhinus_melanopterus_Luc_Viatour.jpg](https://zh.wikipedia.org/wiki/File:Carcharhinus_melanopterus_Luc_Viatour.jpg "fig:Carcharhinus_melanopterus_Luc_Viatour.jpg")
就如牠的名字所言，烏翅真鯊的胸鰭及背鰭的頂端是[黑色的](../Page/黑色.md "wikilink")，而下部是[白色](../Page/白色.md "wikilink")。牠們身體上半部的皮是呈[褐色](../Page/褐色.md "wikilink")。最大的烏翅真鯊可達3.5米長。牠的前端是呈圓鈍的。一種稱為[灰珊瑚鯊的鯊魚外形與牠很相似](../Page/灰珊瑚鯊.md "wikilink")，但從其較粗狀及[灰色的身體及沒有黑端的背鰭](../Page/灰色.md "wikilink")，就能分辨牠們。由於烏翅真鯊體型較小，所以並不認為是[人類的威脅](../Page/人類.md "wikilink")。

## 攝食

烏翅真鯊主要獵食珊瑚魚，但牠們也會捕獵[鱘魚及](../Page/鱘魚.md "wikilink")[烏魚](../Page/烏魚.md "wikilink")。

## 生殖、行為及與人類的接觸

[Snorkeler_with_blacktip_reef_shark.jpg](https://zh.wikipedia.org/wiki/File:Snorkeler_with_blacktip_reef_shark.jpg "fig:Snorkeler_with_blacktip_reef_shark.jpg")
烏翅真鯊是胎生的，每胎約有2至4頭小鯊。母鯊的懷胎期為16個月。出生的小鯊約為33至52厘米長。

烏翅真鯊並非群居的，但卻有時會一小群出沒。一般烏翅真鯊都是較害羞的，但卻會對潛水員及[水肺潛水員感到好奇](../Page/水肺潛水.md "wikilink")。與其他[鯊魚無異](../Page/鯊魚.md "wikilink")，當牠們感覺到危險時，會把身體捲曲成S型。除非是感到憤怒，否則牠們是無害的。一些情況如用手餵食或以矛捕魚，加上低視野，都會令烏翅真鯊感到憤怒。

烏翅真鯊是少有能跳躍出水面的鯊魚，這種行為稱為「[躍身擊浪](../Page/躍身擊浪.md "wikilink")」。牠們亦會在海面徘徊。

[Blacktip_reef_shark.jpg](https://zh.wikipedia.org/wiki/File:Blacktip_reef_shark.jpg "fig:Blacktip_reef_shark.jpg")

## 數量下降

烏翅真鯊有時會被漁業混獲及丟棄。牠們的數量，與其他[鯊魚一樣亦在下降](../Page/鯊魚.md "wikilink")。由於牠們的鰭會作為珍貴的[魚翅](../Page/魚翅.md "wikilink")，牠們的鰭在被捕獲時會被切去，而鯊魚本身會被丟回海中等待死亡。這可能是烏翅真鯊數量減少的原因。

## 参考文献

  -
  -
## 外部連結

  - [新聞：香港發現黑翼鯊屍體](http://www.zblive.com/zatan/bjzt/200609/6853.html)
  - [MarineBio: Blacktip reef shark, Carcharhinus
    melanopterus](http://marinebio.org/species.asp?id=90)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[melanopterus](../Category/真鯊屬.md "wikilink")