**烏臺詩案**，發生於[宋神宗](../Page/宋神宗.md "wikilink")[元豐二年](../Page/元丰_\(北宋\).md "wikilink")（1079年），為[蘇軾](../Page/蘇軾.md "wikilink")[政治生涯重大轉折](../Page/政治.md "wikilink")，幾至於死亡，後經力保，改謫[黃州](../Page/黃州.md "wikilink")[團練副使安置](../Page/團練副使.md "wikilink")。所謂「烏臺」，即[御史臺](../Page/御史臺.md "wikilink")，因官署內遍植[柏樹](../Page/柏樹.md "wikilink")，又稱「[柏臺](../Page/柏臺.md "wikilink")」。柏樹上常有[烏鴉棲息築巢](../Page/烏鴉.md "wikilink")，乃稱[烏臺](../Page/烏臺.md "wikilink")。前代文網寬鬆，[白居易敢以當朝天子的韻事入](../Page/白居易.md "wikilink")[長恨歌](../Page/長恨歌.md "wikilink")，宋初沿襲其風。烏臺詩案實為見諸歷史記載的最初成規模的[文字獄](../Page/文字獄.md "wikilink")。

## 過程

蘇軾於當年移知[湖州](../Page/湖州.md "wikilink")，到任後上表謝恩，朝臣以其上表中用語，暗藏譏刺。[御史何正臣上表](../Page/御史.md "wikilink")[彈劾蘇軾](../Page/彈劾.md "wikilink")，指其“愚弄朝廷，妄自尊大”，又以蘇軾動輒歸咎[新法](../Page/王安石變法.md "wikilink")，要求朝廷明正刑賞。御史[李定曾因不](../Page/李定_\(北宋\).md "wikilink")[服](../Page/五服.md "wikilink")[母孝](../Page/丁憂.md "wikilink")，受蘇軾譏諷，於此案中也指蘇軾有“悛終不悔，其惡已著”、“傲悖之語，日聞中外”、“言偽而辯，行偽而堅”、“怨己不用”等四大可廢之罪。

御史[舒亶尋摘蘇軾詩句](../Page/舒亶.md "wikilink")，指其心懷不軌，譏諷神宗[青苗法](../Page/青苗法.md "wikilink")、[助役法](../Page/助役法.md "wikilink")、[明法科](../Page/明法.md "wikilink")、興水利、鹽禁等政策。神宗下令拘捕，[太常博士](../Page/太常博士.md "wikilink")[皇甫遵奉令前往逮人](../Page/皇甫遵.md "wikilink")。[蘇轍時在](../Page/蘇轍.md "wikilink")[商丘已預知消息](../Page/商丘.md "wikilink")，託[王適協助安置蘇軾家屬](../Page/王適.md "wikilink")，並上書神宗陳情，願以官職贖兄長之罪。

蘇軾在9月被捕後，寫信给蘇轍交代身後之事，長子[蘇邁則隨途照顧](../Page/蘇邁.md "wikilink")。押解至[太湖](../Page/太湖.md "wikilink")，蘇軾曾意圖自盡，幾經掙扎，終未成舉。捕至[御史臺獄下](../Page/御史臺.md "wikilink")，御史臺依平日書信詩文往來，構陷牽連七十餘人。蘇軾自料必死，暗藏[金丹](../Page/金丹.md "wikilink")，預備自盡。

押解途上，蘇軾與蘇邁約定，如有不測，則單送魚至牢中。親戚送飯時，一時不察，蘇軾以為將死，遂寫下二詩與蘇轍訣別：

## 結果

後因[太皇太后曹氏](../Page/慈聖皇后.md "wikilink")\[1\]、[王安禮等人出面力挽](../Page/王安禮.md "wikilink")，曾任宋神宗[宰相的](../Page/宰相.md "wikilink")[王安石也说](../Page/王安石.md "wikilink")：“岂有圣世而杀才士者乎？”蘇軾終免一死\[2\]，貶謫為“檢校尚書水部員外郎黃州團練副使本州安置”，前往[黃州](../Page/黃州.md "wikilink")，為其文學創作生涯的重要階段。蘇轍被貶[江西](../Page/江西.md "wikilink")[筠州任酒监](../Page/筠州.md "wikilink")，平日與蘇軾往來者，如[曾鞏](../Page/曾鞏.md "wikilink")、[李清臣](../Page/李清臣.md "wikilink")、[張方平](../Page/張方平.md "wikilink")、[黃庭堅](../Page/黃庭堅.md "wikilink")、[范镇](../Page/范镇.md "wikilink")、[司馬光等](../Page/司馬光.md "wikilink")29人亦遭處分。张方平、司马光和范镇罚红铜三十斤，其余各罚红铜二十斤。烏臺詩案於12月結束。

## 注釋

[Category:北宋政治事件](../Category/北宋政治事件.md "wikilink")
[Category:文字狱](../Category/文字狱.md "wikilink")
[Category:1070年代中国政治](../Category/1070年代中国政治.md "wikilink")
[Category:1079年](../Category/1079年.md "wikilink")

1.  曹太后告訴神宗，當日蘇軾兄弟中式，仁宗皇帝在後宮言，“朕又為子孫得兩個太平宰相”。神宗有所動
2.  [周紫芝](../Page/周紫芝.md "wikilink")《读诗谳》：“旧传元丰间，朝廷以群言论公，独神庙惜其才，不忍。大丞相王文公曰：‘岂有圣世而杀才士者乎？’当时议，以公一言而决。”《太仓稊米集》卷四九