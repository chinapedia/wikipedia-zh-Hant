**烏茲別克蘇維埃社會主義共和國國徽**啟用於1937年3月2日，參照[蘇聯](../Page/蘇聯.md "wikilink")[國徽設計而成](../Page/蘇聯國徽.md "wikilink")。

呈圓形，下為旭日初升之象，前為地球。上方有紅色五角星，中為[鎚子與鐮刀](../Page/鎚子與鐮刀.md "wikilink")，象徵[社會主義照亮全球](../Page/社會主義.md "wikilink")。外圍由麥和[棉花裝飾](../Page/棉花.md "wikilink")。飾帶以[俄語和](../Page/俄語.md "wikilink")[烏茲別克語書有](../Page/烏茲別克語.md "wikilink")[國家格言](../Page/國家格言.md "wikilink")「[全世界無產者，聯合起來！](../Page/全世界無產者，聯合起來！.md "wikilink")」和烏茲別克語「烏茲別克蘇維埃社會主義共和國」的首字母。

[卡拉卡爾帕克蘇維埃社會主義自治共和國有自己的國徽](../Page/卡拉卡爾帕克斯坦自治共和國.md "wikilink")，使用烏茲別克語和[卡拉卡爾帕克語](../Page/卡拉卡爾帕克語.md "wikilink")。

1992年，[烏茲別克斯坦國徽成為新國家的國徽](../Page/烏茲別克斯坦國徽.md "wikilink")，但與原來的國徽相似。

## 历史

<File:Emblem> of the Uzbek SSR (1931).svg|1925 - 1931 <File:Emblem> of
the Uzbek SSR (1929-1937).svg|1931 - 1937 <File:Emblem> of the Uzbek SSR
(1947).svg|1937 - 1947 <File:Emblem> of the Uzbek SSR.svg|1947 - 1992

{{-}}

[Category:乌兹别克苏维埃社会主义共和国国家象征](../Category/乌兹别克苏维埃社会主义共和国国家象征.md "wikilink")
[U](../Category/苏联加盟共和国国徽.md "wikilink")