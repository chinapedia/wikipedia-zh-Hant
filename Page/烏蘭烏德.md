**烏蘭烏德**（[俄語](../Page/俄語.md "wikilink")：：[布里亞特語](../Page/布里亞特語.md "wikilink")：），[俄羅斯](../Page/俄羅斯.md "wikilink")[布里亞特共和國首府](../Page/布里亞特共和國.md "wikilink")（經緯座標：51°50′N
107°36′E），人口404,357（2010年）。是東[西伯利亞第三大城市](../Page/西伯利亞.md "wikilink")。1666年由[哥薩克人建立](../Page/哥薩克.md "wikilink")，旧名上乌金斯克，是苏俄内战时期远东共和国的第一座首都。

## 交通

[西伯利亞鐵路與往](../Page/西伯利亞鐵路.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")[烏蘭巴托以至](../Page/烏蘭巴托.md "wikilink")[中國](../Page/中國.md "wikilink")[北京鐵路](../Page/北京.md "wikilink")（[K3/4次列車](../Page/K3/4次列車.md "wikilink")）的交界，2013年開通與北京的直飛航班。2016年开通至[天津港的公路货运专线](../Page/天津港.md "wikilink")。

[Lenin's_head_in_Ulan_Ude.jpg](https://zh.wikipedia.org/wiki/File:Lenin's_head_in_Ulan_Ude.jpg "fig:Lenin's_head_in_Ulan_Ude.jpg")领导人[列宁的最大的头部塑像建在烏蘭烏德](../Page/列宁.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Ethnographic_museum_Gate.JPG "fig:缩略图")

## 气候数据

## 姐妹城市

  - [美国](../Page/美国.md "wikilink")[柏克萊](../Page/柏克萊.md "wikilink") \[1\]

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[呼和浩特](../Page/呼和浩特.md "wikilink")
    \[2\]

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[长春](../Page/长春.md "wikilink")
    \[3\]

  - [中华民国](../Page/中华民国.md "wikilink")[台北](../Page/台北.md "wikilink")\[4\]

## 参考

[У](../Category/布里亞特共和國城市.md "wikilink")
[Category:包含俄羅斯城邦模板錯誤的條目](../Category/包含俄羅斯城邦模板錯誤的條目.md "wikilink")

1.
2.
3.
4.  [Sister cities
    list](http://www.edunet.taipei.gov.tw/attach/The%2045%20Sister%20Cities%20list.doc)