**烏古廼**（，即遼[太平元年](../Page/太平_\(辽\).md "wikilink")—遼[咸雍十年](../Page/咸雍.md "wikilink")），又名**胡來**，[完顏石魯之長子](../Page/完顏石魯.md "wikilink")。

[金熙宗即位後](../Page/金熙宗.md "wikilink")，追上[諡號為惠桓皇帝](../Page/諡號.md "wikilink")，[廟號為](../Page/廟號.md "wikilink")**景祖**，皇統四年，稱他的埋葬地為[定陵](../Page/定陵.md "wikilink")，皇統五年，增諡為景祖英烈**惠桓皇帝**。

## 生平

烏古廼是女真完顏部的酋長，從他起完顏部開始在女真諸部落里逐漸占支配地位。其出身非常有傳奇色彩，其父昭祖完顏石魯開始久不育，求巫師占卜，預言了長男兩女再一次男的順序，并說次男“不良”，不要為好。結果果然按此次序得子女四人。次男完顏烏骨出成人后酗酒成性，并常常頂撞其母徒单氏。后來烏骨廼和徒单氏以巫師的預言為依据合謀殺了烏骨出。<ref>《金史》：初昭祖久无子，有巫者能道神语，甚验，乃往祷焉。巫良久曰：“男子之魂至矣。此子厚有福德，子孙昌盛；可拜而受之。若生，则名之曰烏骨廼。”是为景祖。又良久曰：“女子之魂至矣，可名曰五鵶忍。”又良久曰：“女子之兆复见，可名曰斡都拔。”又久之，复曰：“男子之兆复见，然性不驯良，长则残忍，无亲亲之恩，必行非义，不可受也。”昭祖方念后嗣未立，乃曰：“虽不良，亦愿受之。”巫者曰：“当名之曰烏古出。”既而生二男二女，其次弟先后皆如巫者之言，遂以巫所命名名之。

景祖初立，烏古出酗酒，屡悖威顺皇后。后曰：“巫言验矣，悖乱之人终不可留。”遂与景祖谋而杀之。部人怒曰：“此子性如此，在国俗当主父母之业，奈何杀之？”欲杀景祖。后乃匿景祖，出谓众曰：“为子而悖其母，率是而行，将焉用之？吾割爱而杀之，烏骨廼不知也，汝辈宁杀我乎？”众乃罢去。</ref>

烏古廼喜愛二儿子[完顏劾里鉢](../Page/完顏劾里鉢.md "wikilink")（金世祖）的胆勇材略，所以劾里鉢的后代在金國的政治中占統治地位。當儿子們都長大成人時，按女真習俗應當各自搬到不同的宮邸中生活，但烏古廼讓長子完顏劾者与次子完顏劾里鉢同邸，由劾者专治家务，劾里鉢主外事。這是以后劾者之子孫[完顏撒改](../Page/完顏撒改.md "wikilink")，[完顏宗翰長期擔任國論](../Page/完顏宗翰.md "wikilink")[勃极烈](../Page/勃极烈.md "wikilink")（相當于國相）一職，而劾里鉢之子[完顏阿骨打](../Page/完顏阿骨打.md "wikilink")，[完顏吳乞買成為皇帝的一個直接原因](../Page/完顏吳乞買.md "wikilink")。

## 家庭

  - 昭肃皇后[唐括氏，名多保真](../Page/唐括多保真.md "wikilink")<ref>《金史》載：帅水隈鸦村唐括部人，讳多保真。父石批德撒骨只，巫者也。后有识度，在父母家好待宾客，父母出，则多置酒馔享邻里，迨于行旅。景祖饮食过人，时人名之“活罗”，解在《景祖纪》。昭祖曰：“俭啬之女吝惜酒食，不可以配。”乌古乃闻后性度如是，乃娶焉。
    辽使同干来伐五国浦聂部，景祖使后与劾孙为质于拔乙门，而与同干袭取之，辽主以景祖为节度使。
    后虽喜宾客，而自不饮酒。景祖与客饮，后专听之。翌日，枚数其人所为，无一不中其启肯。有醉而喧呶者，辄自歌以释其忿争。军中有被笞罚者，每以酒食慰谕之。景祖行部，辄与偕行，政事狱讼皆与决焉。
    景祖没后，世祖兄弟凡用兵，皆禀于后而后行，胜负皆有惩劝。农月，亲课耕耘刈获，远则乘马，近则策杖，勤于事者勉之，晏出早休者训励之。
    后往邑屯村，世祖、肃宗皆从。会桓赧、散达偕来，是时已有隙，被酒，语相侵不能平，遂举刃相向。后起，两执其手，谓桓赧、散达曰：“汝等皆吾夫时旧人，奈何一旦遽忘吾夫之恩，与小兒子辈忿争乎。”因自作歌，桓赧、散达怒乃解。其后桓赧兄弟起兵来攻，当是时，肃宗先已再失利矣，世祖已退乌春兵，与桓赧战于北隘甸。部人失束宽逃归，袒甲而至，告曰：“军败矣。”后方忧懑，会康宗来报捷，后乃喜。既而桓赧、散达皆降。
    后不妒忌，阔略女工，能辑睦宗族，当时以为有丈夫之度云。天会十五年追谥。</ref>
      - 韩国公[完顏劾者](../Page/完顏劾者.md "wikilink")
      - [金世祖完顏劾里鉢](../Page/金世祖.md "wikilink")
      - 沂国公[完顏劾孫](../Page/完顏劾孫.md "wikilink")
      - [金肃宗完顏頗剌淑](../Page/金肃宗.md "wikilink")
      - [金穆宗完顏盈歌](../Page/金穆宗.md "wikilink")

<!-- end list -->

  - 次室注思灰（[契丹人](../Page/契丹人.md "wikilink")）
      - 代国公完顏劾真保

<!-- end list -->

  - 次室温迪痕氏，名敌本
      - 虞国公[完顏麻頗](../Page/完顏麻頗.md "wikilink")
      - 隋国公[完顏阿離合懣](../Page/完顏阿離合懣.md "wikilink")
      - 郑国公[完顏謾都訶](../Page/完顏謾都訶.md "wikilink")

## 注釋

<references/>

## 參考資料

  - 《金史》列傳第一第三第八

  - 《[大金國志](../Page/大金國志.md "wikilink")》金國世系之圖

[W](../Category/完顏氏.md "wikilink")
[Category:金朝追尊皇帝](../Category/金朝追尊皇帝.md "wikilink")