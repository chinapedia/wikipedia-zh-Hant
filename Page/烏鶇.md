**烏鶇**（[學名](../Page/學名.md "wikilink")：**）
是[鶇屬的一種](../Page/鶇屬.md "wikilink")[鸟类](../Page/鸟类.md "wikilink")。\[1\]
其下有多個[亞種](../Page/亞種.md "wikilink")，分佈與歐、亞、非和北美洲，是[瑞典的國鳥](../Page/瑞典.md "wikilink")。\[2\]
後來又被引入[澳大利亞和](../Page/澳大利亞.md "wikilink")[新西蘭](../Page/新西蘭.md "wikilink")，并成為了澳大利亞當地的[入侵物種](../Page/入侵物種.md "wikilink")。烏鶇是一種[留鳥](../Page/留鳥.md "wikilink")，但有些地方的亞種也會進行[遷徙](../Page/候鳥.md "wikilink")。

烏鶇是一種[雜食動物](../Page/雜食動物.md "wikilink")，以昆蟲和漿果等為食。它們常栖于林区外围、小镇和乡村边缘。该物种的[模式種廣泛分佈於歐洲各地](../Page/模式種.md "wikilink")。
雄性模式種通體黑色，有黃色的眼環和喙，而雌性荷和幼體的翎羽則呈黑褐色。兩性對繁殖地均具有領地意識，會威脅對侵犯領地的同類，但在遷徙時群居。

## 命名與分類

[Turdus_merula_(juvenile)_-lawn-8.jpg](https://zh.wikipedia.org/wiki/File:Turdus_merula_\(juvenile\)_-lawn-8.jpg "fig:Turdus_merula_(juvenile)_-lawn-8.jpg")
烏鶇由[卡爾·林奈在](../Page/卡爾·林奈.md "wikilink")1758年出版的《[自然系統](../Page/自然系統.md "wikilink")》[第10版中首次描述](../Page/自然系統第10版.md "wikilink")，當時學名為*Turdus
merula*\[3\]。這個學名的[兩部分都來自](../Page/二名法.md "wikilink")[拉丁語](../Page/拉丁語.md "wikilink")，其意思大致都是指[鶇科的鳥類](../Page/鶇科.md "wikilink")。其[種加詞更是烏鶇法語名](../Page/種加詞.md "wikilink")“merle”\[4\]
和[低地蘇格蘭語名](../Page/低地蘇格蘭語.md "wikilink")“merl”的起源\[5\]。在鶇科[鶇屬的](../Page/鶇屬.md "wikilink")60多種鳥類中，烏鶇與東南亞和西南太平洋地區的[島鶇](../Page/島鶇.md "wikilink")（*T. poliocephalus*）在進化過程上有較近的關係。\[6\]

### 亚种

作為分佈極為廣泛的一種鳥類，烏鶇有許多的亞種，它們包括：\[7\]
[Blackbird_2.jpg](https://zh.wikipedia.org/wiki/File:Blackbird_2.jpg "fig:Blackbird_2.jpg")
[Amsel_Weibchen_aufgeplustert_edit2.jpg](https://zh.wikipedia.org/wiki/File:Amsel_Weibchen_aufgeplustert_edit2.jpg "fig:Amsel_Weibchen_aufgeplustert_edit2.jpg")
[Turdus_merula_-Gran_Canaria,_Canary_Islands,_Spain-8_(2).jpg](https://zh.wikipedia.org/wiki/File:Turdus_merula_-Gran_Canaria,_Canary_Islands,_Spain-8_\(2\).jpg "fig:Turdus_merula_-Gran_Canaria,_Canary_Islands,_Spain-8_(2).jpg")的*T.
m. cabrerae*\]\]

  - *T. m.
    merula*，指名亞種：分佈於從[冰島和](../Page/冰島.md "wikilink")[英倫諸島到](../Page/英倫諸島.md "wikilink")[烏拉爾山脈的廣大歐洲地區](../Page/烏拉爾山脈.md "wikilink")，分佈範圍最北則可達北緯70度，但實際並不常見。[尼羅河河谷地區也有少量的此亞種](../Page/尼羅河.md "wikilink")。它也被引入了澳大利亞和新西蘭。\[8\]
  - *T m.
    aterrimus*：分佈於[匈牙利及](../Page/匈牙利.md "wikilink")[希臘南部](../Page/希臘.md "wikilink")、[克里特島](../Page/克里特島.md "wikilink")、[土耳其北部和](../Page/土耳其.md "wikilink")[伊朗北部](../Page/伊朗.md "wikilink")。此亞種為候鳥，會遷徙到土耳其南部、[埃及北部](../Page/埃及.md "wikilink")、[伊拉克及伊朗南部過冬](../Page/伊拉克.md "wikilink")。其體型較指名亞種更小，兩性的翎羽的顏色都較黯淡。\[9\]
  - *T. m.
    azorensis*：[亞速爾群島的小型族群](../Page/亞速爾群島.md "wikilink")。雄性羽毛顏色比指名亞種更黑更亮。\[10\]
  - *T. m.
    bourdilloni*：其名稱是為了紀念印度[特拉凡科森林管理員](../Page/特拉凡科.md "wikilink")[托馬斯·富爾頓·鮑迪倫](../Page/托馬斯·富爾頓·鮑迪倫.md "wikilink")，此亞種生活在特拉凡科海拔以上的山嶺中,
    與另一亞種*simillimus*較像，在某些地區生活在一起\[11\]，其雄性都是棕褐色的。\[12\]
  - *T. m.
    cabrerae*：以西班牙博物學家[安赫爾·卡布雷拉之名命名](../Page/安赫爾·卡布雷拉_\(博物學家\).md "wikilink")，分佈於[馬德拉群島大部和](../Page/馬德拉群島.md "wikilink")[加那利群島西部](../Page/加那利群島.md "wikilink")。\[13\]
  - 乌鸫新疆亚种（*T. m.
    intermedia*）：分布于阿富汗、伊拉克、巴基斯坦以及[中国大陆的](../Page/中国大陆.md "wikilink")[新疆](../Page/新疆.md "wikilink")、[青海等地](../Page/青海.md "wikilink")。该物种的模式产地在新疆阿克苏。\[14\]\[15\]
    此亞種體型較大。\[16\]
  - *T. m.
    kinnisii*：其名稱是為了紀念英國醫生[約翰·金尼斯](../Page/約翰·金尼斯.md "wikilink")，此人曾在錫蘭的英國軍隊中服役。這個亞種就生活在[斯里蘭卡海拔](../Page/斯里蘭卡.md "wikilink")以上的山地中。雄性為藍灰色。雌性與其相似，但較顯褐色\[17\]。其體型與*nigropileus*一般，但眼環呈橘紅色。
  - 乌鸫普通亚种（*T. m.
    mandarinus*）：是中国的特有物种。分布于[台灣以及中国除東北以外的大部分地區](../Page/台灣.md "wikilink")。该物种的模式产地在中国南部。\[18\]\[19\]
    其某些族群會遷往[香港及](../Page/香港.md "wikilink")[老撾](../Page/老撾.md "wikilink")、[越南等地過冬](../Page/越南.md "wikilink")。雄性為黑色，雌性與雄性較為相似，但稍顯褐色，身體後部的羽毛顏色較淡\[20\]，這是一個大型的亞種。\[21\]此亞種已獨立為[中國黑鶇新種](../Page/中國黑鶇.md "wikilink")。
  - *T. m.
    mauretanicus*：分佈於[摩洛哥中北部](../Page/摩洛哥.md "wikilink")、[阿爾及利亞沿海和](../Page/阿爾及利亞.md "wikilink")[突尼斯北部地區](../Page/突尼斯.md "wikilink")，是一種小型烏鶇。\[22\]
  - 乌鸫西藏亚种（*T. m.
    maximus*）：分布于巴基斯坦、印度、不丹以及中国大陆的西藏等地，多见于海拔2000-4000米高处的林缘、灌丛或山坡。该物种的模式产地在克什米尔。\[23\]
  - *T. m.
    nigropileus*：生活在[西高止山脈中北部海拔](../Page/西高止山脈.md "wikilink")以上的地區，一些族群在冬季向南遷徙\[24\]。雄性羽毛為灰褐色，頭部為黑色。雌性則是正常的褐色。\[25\]
    這是一種小型的烏鶇，有較大的黃色的眼環。\[26\]
  - 乌鸫四川亚种（*T. m.
    sowerbyi*）：其名稱是為了紀念英國博物學家[詹姆斯·索爾比](../Page/詹姆斯·索爾比.md "wikilink")，是中国的特有物种。分布于四川等地，主要栖息于平原田野或庭园的乔木上。该物种的模式产地在四川乐山\[27\]，它與指名亞種肖似，但更小更黑。\[28\]
  - *T. m.
    spencei*：其名稱是為了紀念英國昆蟲學家[威廉·斯彭思](../Page/威廉·斯彭思_\(昆蟲學家\).md "wikilink")。它與*nigropileus*較像，但頭部顏色較淡。生活於[東高止山脈](../Page/東高止山脈.md "wikilink")\[29\]。實際上其分類有爭議，有時會被劃分到*nigropileus*之中\[30\]\[31\]。
  - *T. m.
    syriacus*：分佈於地中海沿海及南至[西奈半島北部的中東地區](../Page/西奈半島.md "wikilink")。此亞種的部分族群會進行遷徙，它們在冬天會前往[約旦河及](../Page/約旦河.md "wikilink")[尼羅河三角洲地區](../Page/尼羅河三角洲.md "wikilink")。兩性的翎羽的顏色都比指名亞種更深。\[32\]

烏鶇的幾個印度、中國亞種（又稱**反舌**、**黑鸟**、**中国黑鸫**、**百舌**、**乌吸**\[33\]）有時會單獨列出，形成*T.
simillimus*、*T. maximus*、*T. mandarinus*\[34\]\[35\]\[36\]\[37\]。

## 描述

烏鶇的指名亞種身體長度在之間，體重則在之間。成年雄性翅膀為光亮的黑色，腿部則為黑褐色，有黃色的眼環和橘黃色的喙。在冬季喙的顏色會變深。\[38\]
常年雌性則通體碳褐色，喙的顏色較為黯淡，有灰白色的喉部，胸部有不分明的斑點。幼體特征一律與成年雌性相同，唯獨上半身多出一些白斑，斑點也會出現在特別年幼的個體的喙上。在幼體中，顏色較深者比較可能為雄性。\[39\]
剛成年不到十二個月的雄性有深色的喙和較淡的眼環，翅膀也還是褐色的，只有身體其他部位上的羽毛變成黑色。\[40\]

## 分佈與棲息地

烏鶇的亞種廣泛分佈于歐、亞、北美等大陸上，在[加那利群島等島嶼上也可以見到其蹤跡](../Page/加那利群島.md "wikilink")。新西蘭和澳大利亞的烏鶇則是後來人為引入的。\[41\]
其西部和南部的亞種種群數量都很穩定，但是北部的烏鶇因為會進行遷徙而使得其數量在每年不同季節間發生變化。\[42\]
不過生活在都市區的烏鶇也有可能就留在溫暖的、食物豐富的城市裡過冬，而不進行遷徙，它們的繁殖季也較靠前。\[43\]

大多數的烏鶇都生活在林地中，它們喜好在落葉樹上築巢。不過在一些人類的花園裡面，其密度比林地中的最高的10公頃一對還要密集，達到平均7.5公頃一對。\[44\]
緯度更高的地區，其棲息地則主要由[環頸鶇佔據](../Page/環頸鶇.md "wikilink")。\[45\]

在歐洲，烏鶇生活在海拔以下的地區，北美洲在海拔以下，印度半島和斯里蘭卡為在海拔。只有喜馬拉雅地區的亞種會生活在更高的地區，例如*T. m.
maximus*的活動範圍高達海拔，在冬季也還高於在海拔。\[46\]

它們有時也會漂泊到本來不生活的地區，例如一些從飼養環境中逃出的烏鶇在1971年出現在了[魁北克](../Page/魁北克.md "wikilink")，當時這並非學界認可的烏鶇分佈範圍。\[47\]
後來1994年在[紐芬蘭](../Page/紐芬蘭.md "wikilink")[博納維斯塔發現了野生種](../Page/博納維斯塔_\(紐芬蘭\).md "wikilink")，\[48\]
其分佈區才向北更進一步。\[49\]

## 文化

[SingSong6dcaldecott.jpg](https://zh.wikipedia.org/wiki/File:SingSong6dcaldecott.jpg "fig:SingSong6dcaldecott.jpg")》繪本封面\]\]
在古希臘傳說中，烏鶇象征著[珀尔塞福涅](../Page/珀尔塞福涅.md "wikilink")，與因吃了[哈德斯的](../Page/哈德斯.md "wikilink")[石榴而墮入地獄的冥后一樣](../Page/石榴.md "wikilink")，傳說烏鶇吃了石榴就會死\[50\]。此外，烏鶇也經常出現在許多的歐洲民間文學中，《[六便士之歌](../Page/六便士之歌.md "wikilink")》即其中之一\[51\]。英語[聖誕頌歌](../Page/聖誕頌歌.md "wikilink")《[聖誕節的十二天](../Page/聖誕節的十二天.md "wikilink")》中的歌詞“four
calling birds”（四隻唱歌的鳥）在18世紀時作“four colly birds”（四隻黑色的鳥），即指烏鶇\[52\]。

與其他一些黑色的鳥如[烏鴉不同的是](../Page/烏鴉.md "wikilink")，烏鶇在歐洲並不被認為是厄運的象征\[53\]。在悲劇《[馬爾菲公爵夫人](../Page/馬爾菲公爵夫人.md "wikilink")》中，它的叫聲反而被當做對災難的提前警示\[54\]。

烏鶇是瑞典的國鳥\[55\]，曾被印在1970年20[歐爾的郵票上](../Page/歐爾_\(瑞典貨幣\).md "wikilink")\[56\]。此外英國、愛爾蘭也有發行印有烏鶇的郵票\[57\]。據說[塞爾維亞語中的](../Page/塞爾維亞語.md "wikilink")[科索沃一詞也來自烏鶇](../Page/科索沃.md "wikilink")（kos），[科索沃波爾耶的意思就是](../Page/科索沃波爾耶.md "wikilink")“有烏鶇的地方”\[58\]。

## 參考文獻

## 外部連結

{{ Commons|Turdus merula|Turdus merula }}

  - ARKive -
    [烏鶇的圖片和影片](http://www.arkive.org/species/ARK/birds/Turdus_merula/)
  - [RSPB
    烏鶇首頁](http://www.rspb.org.uk/birds/guide/b/blackbird/index.asp)
  - [BBC
    烏鶇首頁](http://www.bbc.co.uk/nature/wildfacts/factfiles/247.shtml)
  - [Birds of Britain
    烏鶇頁面](https://web.archive.org/web/20070202072641/http://www.birdsofbritain.co.uk/bird-guide/blackbird.htm)

在這裡聆聽烏鶇的聲音：

  - [烏鶇的視頻](http://ibc.hbw.com/ibc/phtml/especie.phtml?idEspecie=5942)

[Category:鶇屬](../Category/鶇屬.md "wikilink")
[Category:亞洲鳥類](../Category/亞洲鳥類.md "wikilink")
[Category:歐洲鳥類](../Category/歐洲鳥類.md "wikilink")
[Category:北美洲鳥類](../Category/北美洲鳥類.md "wikilink")
[Category:澳大利亞鳥類](../Category/澳大利亞鳥類.md "wikilink")
[Category:新西蘭鳥類](../Category/新西蘭鳥類.md "wikilink")

1.

2.
3.

4.

5.

6.
7.
8.
9.
10.
11.
12.
13.
14.

15.
16.
17.  p222–228

18.

19. MacKinnon, J., & Phillipps, K. (2000). *A Field Guide to the Birds
    of China.* Oxford University Press. Oxford. ISBN 978-0-19-854940-6

20.  p228

21.
22.  p1215–1218

23.

24.
25.
26. Collar, N. J. (2005). Indian Blackbird (*Turdus simillimus*). p. 646
    in: del Hoyo, J., Elliott, A., & Christie, D. A. eds. (2005)
    *[Handbook of the Birds of the
    World](../Page/Handbook_of_the_Birds_of_the_World.md "wikilink").*
    Vol. 10. Cuckoo-shrikes to Thrushes. Lynx Edicions, Barcelona. ISBN
    978-84-87334-72-6

27.

28.
29.
30.

31.
32.
33.
34. Collar, N. J. (2005). Common Blackbird (*Turdus merula*). p. 645 in:
    del Hoyo, J., Elliott, A., & Christie, D. A. eds. (2005) *[Handbook
    of the Birds of the
    World](../Page/Handbook_of_the_Birds_of_the_World.md "wikilink").*
    Vol. 10. Cuckoo-shrikes to Thrushes. Lynx Edicions, Barcelona. ISBN
    978-84-87334-72-6

35.

36.

37.
38. Mullarney, Killian; Svensson, Lars, Zetterstrom, Dan; Grant, Peter.
    (2001). *Birds of Europe.* Princeton University Press. p 304–306
    ISBN 978-0-691-05054-6

39.
40.
41.
42.
43.

44.

45.

46.
47.

48.
49.

50.

51.

52.

53.
54.

55.

56.

57.

58.