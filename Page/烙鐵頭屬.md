**烙鐵頭屬**（[學名](../Page/學名.md "wikilink")：*Ovophis*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[蝰蛇科](../Page/蝰蛇科.md "wikilink")[蝮亞科下的一個](../Page/蝮亞科.md "wikilink")[有毒蛇](../Page/毒蛇.md "wikilink")[屬](../Page/屬.md "wikilink")，主要分布於[亞洲](../Page/亞洲.md "wikilink")，目前共有3個種已被確認。\[1\]\[2\]

## 地理分布

烙鐵頭蛇主要分布於[亞洲](../Page/亞洲.md "wikilink")，包括[中國](../Page/中國.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[印度](../Page/印度.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[日本](../Page/日本.md "wikilink")[沖繩](../Page/沖繩.md "wikilink")、[印尼等地](../Page/印尼.md "wikilink")。\[3\]

## 品種

| 品種\[4\]                                                                    | 學名及命名者\[5\]                                       | 亞種數\[6\] | 異稱 | 地理分布\[7\]                                                                                                                                                                                                                                                                                                                                                                                                                       |
| -------------------------------------------------------------------------- | ------------------------------------------------- | -------- | -- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **[京那巴魯烙鐵頭](../Page/京那巴魯烙鐵頭.md "wikilink")**                               | Ovophis chaseni，<small>M.A. Smith，1931</small>    | 0        |    | [印尼](../Page/印尼.md "wikilink")[婆羅洲的](../Page/婆羅洲.md "wikilink")[京那巴魯山](../Page/京那巴魯山.md "wikilink")                                                                                                                                                                                                                                                                                                                             |
| **[山烙鐵頭](../Page/山烙鐵頭蛇.md "wikilink")**<font size="-1"><sup>T</sup></font> | Ovophis monticola，<small>Günther，1864</small>     | 4        |    | [尼泊爾](../Page/尼泊爾.md "wikilink")、[印度](../Page/印度.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[中國](../Page/中國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[印尼](../Page/印尼.md "wikilink")[蘇門答臘](../Page/蘇門答臘.md "wikilink")、[台灣](../Page/台灣.md "wikilink") |
| **[沖繩烙鐵頭](../Page/沖繩烙鐵頭.md "wikilink")**                                   | Ovophis okinavensis，<small>Boulenger，1892</small> | 0        |    | [日本](../Page/日本.md "wikilink")[琉球群島](../Page/琉球群島.md "wikilink")：[沖繩及](../Page/沖繩.md "wikilink")[奄美群島](../Page/奄美群島.md "wikilink")                                                                                                                                                                                                                                                                                              |
|                                                                            |                                                   |          |    |                                                                                                                                                                                                                                                                                                                                                                                                                                 |

## 分類學

烙鐵頭屬曾經被归入[竹葉青屬](../Page/竹葉青屬.md "wikilink")（*Trimeresurus*）。另外，1934年有人在[中國及](../Page/中國.md "wikilink")[越南](../Page/越南.md "wikilink")[東京地區發現另一種烙鐵頭蛇](../Page/東京_\(越南\).md "wikilink")，稱為「東京烙鐵頭（*O.
tonkinensis*）」。\[8\]

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：烙鐵頭屬](http://reptile-database.reptarium.cz/search.php?&genus=Ovophis&submit=Search)

[Category:蝰蛇科](../Category/蝰蛇科.md "wikilink")

1.
2.

3.
4.
5.
6.
7.
8.