[Barbecued_lamb_sticks.jpg](https://zh.wikipedia.org/wiki/File:Barbecued_lamb_sticks.jpg "fig:Barbecued_lamb_sticks.jpg")[羊肉串](../Page/羊肉.md "wikilink")\]\]

**烤串**，或称**烤串儿**、**串燒**，指将[肉类](../Page/肉类.md "wikilink")、[蔬菜等食材用签子穿成串后](../Page/蔬菜.md "wikilink")[炙烤的烹饪方法](../Page/炙烤.md "wikilink")，也指通过该方法产生的食物，属于[烧烤的一种](../Page/烧烤.md "wikilink")。烤串在[中东](../Page/中东.md "wikilink")、[南亚](../Page/南亚.md "wikilink")、[中亚](../Page/中亚.md "wikilink")、[地中海](../Page/地中海.md "wikilink")、[东亚等多地均非常流行](../Page/东亚.md "wikilink")。

一種起源于[中东](../Page/中东.md "wikilink")，後來流傳至[南亚及地中海地區](../Page/南亚.md "wikilink")，後至全世界的料理。卡博（Kebabı）的意思是在中東一帶燒、煎和烤的肉類料理總稱（不一定是串起來的肉類，主要為[雞肉](../Page/雞肉.md "wikilink")、[羊肉](../Page/羊肉.md "wikilink")、[牛肉](../Page/牛肉.md "wikilink")，沒有[豬肉](../Page/豬肉.md "wikilink")）。這個詞源於[波斯語的](../Page/波斯語.md "wikilink")“”（**），意思原指「[烤肉](../Page/烤.md "wikilink")」\[1\]。

## 概要

卡博最典型的調理方式是，將切成四角形的肉串起來。在[土耳其](../Page/土耳其.md "wikilink")，除了最單純的[串燒形式](../Page/串燒.md "wikilink")，也有搭配[酸奶食用的İskender](../Page/酸奶.md "wikilink")
kebap，
還有將各種骨頭上的殘肉屑堆疊固定在棍棒上旋轉燒烤，要吃的時候一片一片薄薄的切下來的[沙威瑪](../Page/沙威瑪.md "wikilink")（Döner
Kebabi）等各種變種版的卡博串。另外也有不用燒烤的形式改用長時間[燉煮](../Page/燉煮.md "wikilink")、[炸](../Page/炸.md "wikilink")、[蒸的肉類料理被稱作卡博](../Page/蒸.md "wikilink")（Kebabı）。

## 世界的普及化

### 東南亞

在19世紀初，大量[印度](../Page/印度.md "wikilink")[坦米爾人和](../Page/坦米爾人.md "wikilink")[阿拉伯移民湧入](../Page/阿拉伯.md "wikilink")[荷屬東印度](../Page/荷屬東印度.md "wikilink")，這些移民帶來的飲食習慣，結合[爪哇和](../Page/爪哇.md "wikilink")[蘇門答臘當地的食材](../Page/蘇門答臘.md "wikilink")，促使沙嗲在[印尼出現](../Page/印尼.md "wikilink")。

沙嗲從爪哇傳遍荷屬東印度的各個島嶼後，在不同島嶼的居民，都製作出具有當地特色的沙嗲食品。到19世紀末期，沙嗲已經跨過海峽傳到鄰近的[馬來半島](../Page/馬來半島.md "wikilink")，遍及[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")，其後更流傳到[泰國](../Page/泰國.md "wikilink")。

最後，在印尼殖民的[荷蘭人也將沙嗲和許多其他的印尼當地美食帶回](../Page/荷蘭人.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")，沙嗲因此被融入荷蘭的菜式，常見於荷蘭酒館的下酒菜。

### 中国大陸

[Mutton_bbq.jpg](https://zh.wikipedia.org/wiki/File:Mutton_bbq.jpg "fig:Mutton_bbq.jpg")[乌鲁木齐街头的烤羊肉串摊位](../Page/乌鲁木齐.md "wikilink")\]\]
在中国大陆，烤串（[汉语拼音](../Page/汉语拼音.md "wikilink")：kǎochuàn；[维吾尔语](../Page/维吾尔语.md "wikilink")：كاۋاپ,*kawap*），或称烤串儿、串烧，有着悠久的历史，2012年4月至7月，宁夏文物考古研究所在对中卫常乐汉代墓地进行第四次考古发掘时，曾在M17墓发现肉串\[2\]，但1980年代以来，烤串的流行是从新疆传播到各地的\[3\]。在中国，烤串业者常同时售卖其它类型的烧烤食品，因此烤串常被泛称为烧烤，而羊肉串作为最常见的烤串品种常被作为烤串的代名词。食用烤串的行为被称为吃烧烤、撸串等。由于各地、各民族的饮食习惯不同，烤串在中国各地区之间的风格也有所不同。

中式烤串一般是将食材用签子串好后在[木炭烤炉上](../Page/木炭.md "wikilink")[炙烤](../Page/炙烤.md "wikilink")，所用的食材非常丰富，除常见的[羊肉](../Page/羊肉.md "wikilink")、[牛肉](../Page/牛肉.md "wikilink")、[猪肉](../Page/猪肉.md "wikilink")、[鸡肉](../Page/鸡肉.md "wikilink")、鱼虾和蔬菜等之外，昆虫、玉米、[面筋等也被用于烤串](../Page/面筋.md "wikilink")。牛羊肉是烤串的最主要食材，尤其是在中国北方\[4\]，除了普通肉串，筋类也常被用于烤串。猪肉串在新疆等穆斯林聚居的地区较为少见。禽类除常见的鸡翅、鸡腿之外，鸡胗、鸭胗、鸡心、鸡脖、鸡爪、鸡脆骨等也都是常见的烤串食材。水产品类的烤串，除了烤鱼、烤对虾，还有烤鱿鱼、烤海馬\[5\]、烤皮皮虾等。一些常见的火锅食材也有被用于烧烤的，如蟹棒、鱼丸、牛丸、鱼豆腐等。素食类的食材包括蔬菜、水果、面食等，常见的有土豆片、茄子、辣椒、金针菇、玉米、馒头片、烤饼、豆腐、豆皮等等。

### 日本

### 美国

大多流行于喜好小吃美食者。一般为家庭聚会的焦点。好友聚在一起，边烤边吃卡博串，有时会搭配冰啤酒。

### 南美洲

### 台灣

目前在台灣各地都能吃到地道口味的卡博串。

## 參考資料

[Category:新疆食品](../Category/新疆食品.md "wikilink")
[Category:烤肉料理](../Category/烤肉料理.md "wikilink")
[Category:燒烤](../Category/燒烤.md "wikilink")
[Category:串燒](../Category/串燒.md "wikilink")
[Category:中東食品](../Category/中東食品.md "wikilink")
[Category:街頭小吃](../Category/街頭小吃.md "wikilink")

1.

2.

3.

4.
5.