**熱地**（，），男，[藏族](../Page/藏族.md "wikilink")，[西藏](../Page/西藏自治区.md "wikilink")[比如县人](../Page/比如县.md "wikilink")，1959年7月參加工作，1961年10月加入[中國共產黨](../Page/中國共產黨.md "wikilink")，[中央黨校大專學歷](../Page/中共中央黨校.md "wikilink")。

曾先後歷任[那曲地區黨委書記](../Page/那曲地區.md "wikilink")、[西藏自治区黨委副書記](../Page/西藏自治区.md "wikilink")、自治區紀委書記、自治區革委會副主任，自治區人大常委會副主任，自治區政協主席、自治區黨校校長。1993年至1994年任西藏自治區黨委副書記、自治區人大常委會主任、黨組書記。1994年至2003年任西藏自治區黨委常務副書記、自治區人大常委會主任、黨組書記。2003年3月至2008年3月任[全國人大常委會副委員長](../Page/全國人大常委會副委員長.md "wikilink")。

[中共第十一屆中央候補委員](../Page/中国共产党第十一届中央委员会候补委员.md "wikilink")，[第十二](../Page/中国共产党第十二届中央委员会委员.md "wikilink")、[十三](../Page/中国共产党第十三届中央委员会委员.md "wikilink")、[十四](../Page/中国共产党第十四届中央委员会委员.md "wikilink")、[十五](../Page/中国共产党第十五届中央委员会委员.md "wikilink")、[十六屆中央委員](../Page/中国共产党第十六届中央委员会委员.md "wikilink")。[第十屆全國人大常委會副委員長](../Page/第十屆全國人大常委會.md "wikilink")。

{{-}}

[Category:藏族全国人大常委会副委员长](../Category/藏族全国人大常委会副委员长.md "wikilink")
[Category:第十届全国人大常委会副委员长](../Category/第十届全国人大常委会副委员长.md "wikilink")
[Category:第九屆全國人大代表](../Category/第九屆全國人大代表.md "wikilink")
[Category:第八屆全國人大代表](../Category/第八屆全國人大代表.md "wikilink")
[Category:西藏自治區全國人民代表大會代表](../Category/西藏自治區全國人民代表大會代表.md "wikilink")
[Category:藏族全國人大代表](../Category/藏族全國人大代表.md "wikilink")
[Category:西藏自治区人大常委会主任](../Category/西藏自治区人大常委会主任.md "wikilink")
[Category:西藏自治區政協主席](../Category/西藏自治區政協主席.md "wikilink")
[Category:中共西藏自治區黨委副書記](../Category/中共西藏自治區黨委副書記.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十四届中央委员会委员](../Category/中国共产党第十四届中央委员会委员.md "wikilink")
[Category:中国共产党第十三届中央委员会委员](../Category/中国共产党第十三届中央委员会委员.md "wikilink")
[Category:中国共产党第十二届中央委员会委员](../Category/中国共产党第十二届中央委员会委员.md "wikilink")
[Category:中国共产党第十一届中央委员会候补委员](../Category/中国共产党第十一届中央委员会候补委员.md "wikilink")
[Category:中华人民共和国时期藏族中国共产党党员](../Category/中华人民共和国时期藏族中国共产党党员.md "wikilink")
[Category:中共那曲地委书记](../Category/中共那曲地委书记.md "wikilink")
[Category:中国共产党党员
(1961年入党)](../Category/中国共产党党员_\(1961年入党\).md "wikilink")
[Category:比如人](../Category/比如人.md "wikilink")
[Category:中国登山协会名誉主席](../Category/中国登山协会名誉主席.md "wikilink")
[Category:中国高尔夫球协会名誉主席](../Category/中国高尔夫球协会名誉主席.md "wikilink")