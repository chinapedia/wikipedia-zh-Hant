**熱寂**（[英语](../Page/英语.md "wikilink")：Heat death of the
universe）是猜想[宇宙終極命運的一種假說](../Page/宇宙.md "wikilink")。根據[熱力學第二定律](../Page/熱力學第二定律.md "wikilink")，作為一個「孤立」的系統，宇宙的[熵會隨著時間的流異而增加](../Page/熵.md "wikilink")，由有序向無序，當[宇宙的](../Page/宇宙.md "wikilink")[熵達到最大值時](../Page/熵.md "wikilink")，[宇宙中的其他有效能量已經全數轉化為熱能](../Page/宇宙.md "wikilink")，所有物質溫度達到熱平衡。這種狀態稱為熱寂。這樣的宇宙中再也沒有任何可以維持[運動或是](../Page/運動.md "wikilink")[生命的能量存在](../Page/生命.md "wikilink")。熱寂理論最早由[威廉·湯姆森](../Page/威廉·湯姆森，第一代開爾文男爵.md "wikilink")（[英语](../Page/英语.md "wikilink")：William
Thomson）於1850年根據[自然界中](../Page/自然界.md "wikilink")[機械能損失的](../Page/機械能.md "wikilink")[熱力學原理推導出的](../Page/熱力學.md "wikilink")。

## 发展历史

熱寂理論起源於十九世紀物理學家對[熱力學第一定律和](../Page/熱力學第一定律.md "wikilink")[熱力學第二定律對宇宙進程的影響的研究](../Page/熱力學第二定律.md "wikilink")，特別是威廉·湯姆森在1851年對當時的一個動態熱力學理論（Theory
of
Heat）實驗做出了如下描述：「熱量並非一種物質，然而是機械作用的一種動態形式，我們認識到機械[功與熱量之間必須是相關的](../Page/功.md "wikilink")，就如同因與果」\[1\]隨後，熱寂理論又由[赫爾曼·馮·亥姆霍茲和](../Page/赫爾曼·馮·亥姆霍茲.md "wikilink")[威廉·兰金](../Page/威廉·兰金.md "wikilink")\[2\]加以發展。
[Lord_Kelvin_photograph.jpg](https://zh.wikipedia.org/wiki/File:Lord_Kelvin_photograph.jpg "fig:Lord_Kelvin_photograph.jpg")（William
Thomson）——熱寂理論的創立者\]\]

## 理论不足

热寂说单纯地考虑热力学第二定律，而没有考虑[引力效应](../Page/引力.md "wikilink")。引力系统是负比热容系统，不存在稳定的平衡态。\[3\]

## 熱寂時間表

由於宇宙熱寂說僅僅是一種可能的猜想，並沒有任何事實證據支持該學說的正確性，所以以下內容僅為在假設該學說成立基礎下的假說。

### 退化時代：從10<sup>14</sup>年到10<sup>40</sup>年

#### 星系和恆星停止產生：10<sup>14</sup>年

在這段時間裡，星系和恆星的形成逐漸減緩並完全停止，至於那些仍然存在的恆星，由於自身[核燃料的逐漸枯竭](../Page/核燃料.md "wikilink")，恆星的[溫度和](../Page/溫度.md "wikilink")[光度逐漸下降](../Page/光度.md "wikilink")，直到[核燃料完全耗盡](../Page/核燃料.md "wikilink")，恆星死亡為止。當宇宙中所有的恆星都熄滅之後，只有[行星](../Page/行星.md "wikilink")、[小行星](../Page/小行星.md "wikilink")（包括[彗星](../Page/彗星.md "wikilink")、[隕石和](../Page/隕石.md "wikilink")[棕矮星](../Page/棕矮星.md "wikilink")）、[白矮星](../Page/白矮星.md "wikilink")、[黑矮星](../Page/黑矮星.md "wikilink")、[中子星](../Page/中子星.md "wikilink")、[奇異星和](../Page/奇異星.md "wikilink")[黑洞能夠繼續存在](../Page/黑洞.md "wikilink")。偶爾，[棕矮星之間的相互撞擊會形成新的](../Page/棕矮星.md "wikilink")[紅矮星](../Page/紅矮星.md "wikilink")。這些[紅矮星會在宇宙中繼續存在數十億年](../Page/紅矮星.md "wikilink")，成為宇宙中为数不多的可見光源。

#### 行星開始脫離軌道：10<sup>15</sup>年

隨著時間的流逝，由於[引力波和引力擾動的影響](../Page/引力波.md "wikilink")，行星逐漸脫離它們的原始軌道。

#### 恆星開始脫離軌道：10<sup>16</sup>年

同樣是因為[引力波和引力擾動的影響](../Page/引力波.md "wikilink")，星系中的恆星和恆星殘骸也開始離開它們的原始軌道，只留下分散的恆星殘骸以及[超大質量黑洞](../Page/超大質量黑洞.md "wikilink")。

[BlackHole.jpg](https://zh.wikipedia.org/wiki/File:BlackHole.jpg "fig:BlackHole.jpg")\]\]

### 可能的分歧之一

第一個可能性是以某些[大一統理論中](../Page/大一統理論.md "wikilink")，是以質子壽命極長但有限為前提推測的。

參見[質子和](../Page/質子.md "wikilink")[質子衰變條目](../Page/質子衰變.md "wikilink")。

#### 一半質子完成衰變：10<sup>36</sup>年

根據某些理論認為[質子會衰變](../Page/質子.md "wikilink")，而[半衰期](../Page/半衰期.md "wikilink")（10<sup>36</sup>年）的估計是正確的話，屆時，宇宙中大約一半的物質已經通過[質子衰變形式轉化為](../Page/質子衰變.md "wikilink")[伽馬射線和](../Page/伽馬射線.md "wikilink")[輕子](../Page/輕子.md "wikilink")。

#### 全部質子完成衰變：10<sup>40</sup>年

這時，所有的質子都已完成衰變。事實上在這種情況下，宇宙中所有的物質只能兩種形式存在：黑洞或是[輕子](../Page/輕子.md "wikilink")。

#### 黑洞時代：從10<sup>40</sup>年到1[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>100</sup>年

##### 黑洞佔主導地位：10<sup>40</sup>年

黑洞繼續通過[霍金輻射的形式緩慢的蒸發](../Page/霍金輻射.md "wikilink")

##### 黑洞崩潰：1[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>100</sup>年

除了少量黑洞以外，絕大部分的黑洞都已蒸發完畢。現在，所有組成恆星和星系的物質都衰變為[光子和](../Page/光子.md "wikilink")[輕子](../Page/輕子.md "wikilink")

#### [宇宙的終極命運](../Page/宇宙的終極命運.md "wikilink")

##### 黑暗時代：從1[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>100</sup>年到1.5[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>150</sup>年

**最後的黑洞蒸發完畢：1.5[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>150</sup>年**

所有殘餘的黑洞都已完全蒸發：首先是低質量的黑洞，最後是[超大質量黑洞](../Page/超大質量黑洞.md "wikilink")，現在宇宙中所有的物質都已衰變為[光子和](../Page/光子.md "wikilink")[輕子](../Page/輕子.md "wikilink")。

##### 光子時代：1.5[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>150</sup>年到10<sup>100<sup>056</sup></sup>年

**宇宙達到最低能量狀態：10[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>1000</sup>年到10<sup>100<sup>056</sup></sup>年**

現在，宇宙已經進入低能狀態，目前尚不清楚在這之後會發生什麼。一種假設是宇宙很可能會永遠停留在這種狀態，進入真正意義上的熱寂狀態。意味著[量子事件將會取代其他的微觀活動成為宇宙的主宰](../Page/量子.md "wikilink")。

<timeline>

1.  ImageSize = width:1100 height:370 \# too wide

ImageSize = width:1000 height:370 PlotArea = left:40 right:235 bottom:75
top:75

Colors =

` id:period1  value:rgb(1,1,0.7) # light yellow`
` id:period2  value:rgb(0.7,0.7,1) # light blue`
` id:events  value:rgb(1,0.7,1) # light purple`
` id:era2  value:lightorange`
` id:era1  Value:yellowgreen`

DateFormat = yyyy Period = from:-138 till:1200 TimeAxis = format:yyyy
orientation:horizontal ScaleMajor = unit:year increment:100 start:-100
ScaleMinor = unit:year increment:10 start:-130

AlignBars = justify

BarData =

` bar:Era`
` bar:Events`

TextData =

` fontsize:M`
` pos:(0,260)`
` text:"Big"`
` text:"Bang"`

PlotData=

` textcolor:black  fontsize:M`

` width:110`
` bar:Era  mark:(line,white)  align:left  shift:(0,0)`
` from:-138  till:8  shift:(0,35)  color:era1  text:"The Primordial Era" # or end at one million? stelliferous without stars?`
` from:8  till:14  shift:(0,15)  color:era2  text:"The Stelliferous Era"`
` from:14  till:40  shift:(0,-5)  color:era1  text:"The Degenerate Era"`
` from:40  till:100  shift:(0,-25)  color:era2  text:"The Black Hole Era"`
` from:100  till:150  shift:(0,-45)  color:era1  text:"The Dark Era"`
` from:150  till:1000  shift:(0,0)  color:era2  text:"The Photon Era"`

` from:1000  till:1200  shift:(0,0)  color:era1  text:"The Heat Death"`

` width:110`
` bar:Events  `
` color:events  align:left  shift:(43,3)  mark:(line,teal)`
` at:-8  shift:(0,35)  text:"One second"`
` at:8  shift:(-2,15)  text:"First star began to shine"`
` at:10  shift:(-2,-5)  text:"13.7 billion years, this present day"`
` at:14  shift:(0,-25)  text:"The last star has died down"`
` at:150  shift:(0,-45)  text:"The last supermassive black holes have evaporated"`

</timeline>

### 可能分歧之二

假如宇宙是平面的（即Ω=1）且沒有質子衰變；則隨著[量子穿隧效應加上長時間的機率](../Page/量子穿隧效應.md "wikilink")，最終所有小於鐵的物質都會因此而發生核融合，最終變為鐵。（而鐵的[結合能最小](../Page/鐵峰頂.md "wikilink")，因此熵值最大。）之後在很長一段時間內，自發[熵值減少最終會通過](../Page/熵.md "wikilink")[龐加萊定理](../Page/龐加萊－本迪克松定理.md "wikilink")、[熱漲落和](../Page/熱漲落.md "wikilink")[漲落定理](../Page/漲落定理.md "wikilink")，[量子穿隧效應也應該將大物體變成](../Page/量子穿隧效應.md "wikilink")[黑洞](../Page/黑洞.md "wikilink")。根據所做的假設，這種情況發生的時間可以從10<sup>100<sup>056</sup>000</sup>年
×
10<sup>21</sup>年（一千萬五千六百億億億億億億億億億億億年）到10<sup>131<sup>076</sup>000</sup>年
×
10<sup>21</sup>年（一萬三千一百七十六兆兆兆兆兆兆兆兆兆年）計算。[量子穿隧效應也可能使](../Page/量子穿隧效應.md "wikilink")[鐵星在大約年內坍縮成](../Page/鐵星.md "wikilink")[中子星](../Page/中子星.md "wikilink")。

#### 全部物質變為鐵：15[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>1500</sup>年到10<sup>100<sup>056</sup></sup>年

假如宇宙是平面的（即Ω=1），在15[古高爾年](../Page/古戈爾.md "wikilink")—10<sup>1500</sup>年（十五萬億億億億億億億億億億億年）到年到10<sup>100<sup>056</sup></sup>年（一千萬五千六百億億億億億億億億億億億年），量子穿梭效應所導致的核聚變應使小於鐵物質融合成鐵-56，而自發裂變和核衰變也應使大於鐵的物質衰變成鐵。\[4\]

#### 量子穿隧效應至量子漲落：10<sup>100<sup>056</sup></sup>年到10<sup>131<sup>076</sup></sup>年

另一個宇宙可以是通過隨機的[量子穿隧效應或](../Page/量子穿隧效應.md "wikilink")[量子漲落粗略地創建的](../Page/量子漲落.md "wikilink")10<sup>100<sup>056</sup></sup>年（一千萬五千六百億億億億億億億億億億億年）到10<sup>100<sup>076</sup></sup>年（一千萬七千六百億億億億億億億億億億億年）在很長一段時間內，自發[熵減少最終會通過](../Page/熵.md "wikilink")[龐加萊定理](../Page/龐加萊－本迪克松定理.md "wikilink")、[熱漲落和](../Page/熱漲落.md "wikilink")[漲落定理](../Page/漲落定理.md "wikilink")。然而，這種情況被描述為：“高度投機，可能錯誤的，並且完全地不可驗證”。

#### 鐵星坍縮成中子星：10<sup>100<sup>076</sup></sup>年年到10<sup>131<sup>076</sup>000</sup>年 × 10<sup>21</sup>年

10<sup>100<sup>076</sup></sup>年（一千萬五千六百億億億億億億億億億億億年）到10<sup>131<sup>076</sup>000</sup>年
×
10<sup>21</sup>年（一萬三千一百七十六兆兆兆兆兆兆兆兆兆年）[量子穿隧效應也應該將大物體變成](../Page/量子穿隧效應.md "wikilink")[黑洞](../Page/黑洞.md "wikilink")。根據所做的假設，這種情況發生的時間可以從10<sup>100<sup>056</sup>000</sup>年
×
10<sup>21</sup>年（一千萬五千六百億億億億億億億億億億億年）到10<sup>131<sup>076</sup>000</sup>年
×
10<sup>21</sup>年（一萬三千一百七十六兆兆兆兆兆兆兆兆兆年）計算。[量子穿隧效應也可能使](../Page/量子穿隧效應.md "wikilink")[鐵星在大約年內坍縮成](../Page/鐵星.md "wikilink")[中子星](../Page/中子星.md "wikilink")。

### 可能分歧之三

以較新的觀測所知[宇宙常數](../Page/宇宙常數.md "wikilink")，和以質子壽命無限為前提假設，有強大的[暗能量不斷加速](../Page/暗能量.md "wikilink")，這樣宇宙的整體[熵極大](../Page/熵.md "wikilink")，所以微觀物質很少改變，但長遠來說宏觀物體難以保持現狀。這仍然屬於熱寂的結論之一。這樣的宇宙的預期壽命遠較其他假設為短，估計約26[古高爾年](../Page/古高爾.md "wikilink")—10<sup>2600</sup>年（二十六萬億億億億億億億億億億億年）（某些人認為幾百億到幾百兆年）便會崩潰而不適合維持生物的存活。

#### 宇宙加速膨脹

[宇宙加速膨脹使星系間距離擴大](../Page/宇宙加速膨脹.md "wikilink")。10<sup>10</sup>年內，除了一些有引力聯繫的星系，其它星系都會因为離開[可觀測宇宙外而變成不可見](../Page/可觀測宇宙.md "wikilink")。

#### [宇宙的終極命運](../Page/宇宙的終極命運.md "wikilink")

因為發光體不存在，只有高質量的[暗物质](../Page/暗物质.md "wikilink")，除人為光源外完全黑暗。

但此情形下很難一直持續下去，到10<sup>2600</sup>年（二十六萬億億億億億億億億億億億年），比上一種模型更可能因為[零点能作用產生](../Page/零点能量.md "wikilink")[大撕裂](../Page/大撕裂.md "wikilink")（[英语](../Page/英语.md "wikilink")：Big
Rip），甚至產生新的[宇宙](../Page/宇宙.md "wikilink")[大爆炸](../Page/大爆炸.md "wikilink")。

## 参考文献

{{-}}

[R](../Category/熱力學.md "wikilink") [R](../Category/天文學.md "wikilink")
[Category:热力学熵](../Category/热力学熵.md "wikilink")
[Category:1851年科学](../Category/1851年科学.md "wikilink")

1.  Thomson, William.（1951）. "[On the Dynamical Theory of
    Heat](http://zapatopi.net/kelvin/papers/on_the_dynamical_theory_of_heat.html),
    with numerical results deduced from Mr Joule's equivalent of a
    Thermal Unit, and M. Regnault's Observations on Steam." Excerpts.
    \[§§1-14 & §§99-100\], Transactions of the Royal Society of
    Edinburgh, March, 1851; and Philosophical Magazine IV. 1852, \[from
    Mathematical and Physical Papers, vol. i, art. XLVIII, pp. 174\]
2.  [Physics Timeline](http://webplaza.pt.lu/fklaess/html/HISTORIA.HTML)
    （Helmholtz and Heat Death, 1854）
3.  程龙。热力学观点下的引力理论及其相关研究\[D\].南昌：南昌大学理学院物理系，2012.
4.  Time without end: Physics and biology in an open universe, Freeman
    J. Dyson, *Reviews of Modern Physics* **51**（1979）, pp. 447–460, .