**热雷米·图拉朗**（****，）乃一名[法國職業足球員](../Page/法國.md "wikilink")，司職[中場和](../Page/中場.md "wikilink")[后卫](../Page/后卫.md "wikilink")，目前效力[法甲球會](../Page/法甲.md "wikilink")[摩纳哥](../Page/摩纳哥足球俱乐部.md "wikilink")。

## 生平

### 球會

#### 南特

杜拿蘭出身於法甲另一支球會南特，早在2001-02年球季已出過場\[1\]，曾入選過法國國家青年隊出戰歐洲青年賽。然而在南特早期並沒太多上陣機會。直至2004-05年球季他在一場聯賽中取得入球助球隊取勝，逐漸為南特所重視。儘管嶄露頭角，南特卻在2005-06年球季在法甲聯賽僅排第17位，需要降級。

#### 里昂

[2006年世界杯後他被法甲里昂看中](../Page/2006年世界杯.md "wikilink")，正式七百萬歐元費用轉會至該隊\[2\]。杜拿蘭在第一季已佔主力位置，雖然多數時間打中場中路位置，但偏向於攻擊型，长传球精确，在青年錦標賽時更司職左翼。在里昂成為聯賽七連霸之時，杜拿蘭就在第六個、第七個聯賽冠軍之中成為[法甲冠軍成員](../Page/法甲.md "wikilink")。

#### 马拉加

2011-2012赛季前，图拉朗以1000万欧元的身价转会至有着“西甲曼城”之称的马拉加足球俱乐部。

### 國家隊

他在2006年10月11日對[法羅群島的](../Page/法羅群島.md "wikilink")[歐洲國家盃外圍賽首度代表國家隊上陣](../Page/歐洲國家盃.md "wikilink")，當時法國贏5-0。之後亦入選過國家隊，出戰[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")。由於在球會和國家隊均具表現，故被外界認為是法國元老級球員[馬基利尼的接班人](../Page/馬基利尼.md "wikilink")\[3\]。

## 榮譽

  - 里昂

<!-- end list -->

  - [法甲聯賽冠軍](../Page/法甲.md "wikilink")：2006/07年、2007/08年；
  - [法國盃冠軍](../Page/法國盃.md "wikilink")：2008年；
  - [法國超級盃冠軍](../Page/法國超級盃.md "wikilink")：2006年、2007年；

## 參考資料

## 外部連結

  - [FFF Appearance
    record](http://www.fff.fr/servfff/historique/selec_joueurs.php?id=TOULALAN%20Jérémy)
  - [ESPNsoccernet
    Profile](http://soccernet.espn.go.com/players/stats?id=19909&cc=5901)
  - [Football Database
    Profile](http://www.footballdatabase.com/index.php?page=player&Id=6552&pn=J%C3%A9r%C3%A9my_Toulalan)
  - [Olympique Lyonnais Official Website
    Profile](http://www.olweb.fr/index.php?lng=fr&j=1463&pid=201002)

[Category:法國足球運動員](../Category/法國足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:南特球員](../Category/南特球員.md "wikilink")
[Category:里昂球員](../Category/里昂球員.md "wikilink")
[Category:馬拉加球員](../Category/馬拉加球員.md "wikilink")
[Category:摩納哥球員](../Category/摩納哥球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")

1.
2.
3.