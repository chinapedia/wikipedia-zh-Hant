**無我**（，，），佛教術語，指對於[我的否定](../Page/我_\(佛教\).md "wikilink")，為佛教根本思想之一。這個名詞有二方面的意思，一方面，它可以解釋為，沒有[我](../Page/我_\(佛教\).md "wikilink")，[我不存在](../Page/我_\(佛教\).md "wikilink")，大乘佛教又稱**我空**；另一方面，則可解釋為，這不是[我](../Page/我_\(佛教\).md "wikilink")，也稱為[非我](../Page/非我.md "wikilink")（）。這兩種含義間的爭論，成為佛教各宗派間的重要課題。\[1\]\[2\]

否定世界上有物质性的实在自体（即所谓“[我](../Page/我_\(佛教\).md "wikilink")”）的存在，有两类：

1.  人无我，是说人身不外是色（形质）、受（[感觉](../Page/感官.md "wikilink")）、想（观念）、行（行动）、识（意识）五类，即[五蕴结合而成](../Page/五蕴.md "wikilink")，没有常恒自在的主体。
2.  法无我，[大乘佛教还认为色](../Page/大乘佛教.md "wikilink")、受等五类都由种种因缘和合而生，不断变迁，也无常恒坚实的自体。

## 類似的說法

[德謨克利特所主張的原子說](../Page/德谟克利特.md "wikilink")，在古希臘時代，認為萬物皆由原子組成，而世界是冰冷的，並無東西聯繫。

[理察·道金斯所出版的](../Page/理查德·道金斯.md "wikilink")《自私的基因》說明基因是自私的行為，而有機物皆由基因構成，所有的感覺都是由基因為了存活所顯示的感覺，不論恐懼、愛情、傷心，各種情緒，而人本是無我，就如同萬物本身就不存在一樣，這點與悉達多所主張的並無異。

## 参考文献

## 外部連結

  - [*Nirvana Sutra*](http://www.nirvanasutra.net/), Kosho Yamamoto's
    English translation of the *[Mahāyāna Mahāparinirvāṇa
    Sūtra](../Page/Mahāyāna_Mahāparinirvāṇa_Sūtra.md "wikilink")*

[category:佛教術語](../Page/category:佛教術語.md "wikilink")

[Category:佛教阿毘達摩術語](../Category/佛教阿毘達摩術語.md "wikilink")

1.  [Anatta Buddhism](http://www.britannica.com/topic/anatta),
    *Encyclopædia Britannica* (2013)
2.  \[a\]
    \[b\] , Quote: "...anatta is the doctrine of non-self, and is an
    extreme empiricist doctrine that holds that the notion of an
    unchanging permanent self is a fiction and has no reality. According
    to Buddhist doctrine, the individual person consists of [five
    skandhas](../Page/Skandha.md "wikilink") or heaps—the body,
    feelings, perceptions, impulses and consciousness. The belief in a
    self or soul, over these five skandhas, is illusory and the cause of
    suffering."
    \[c\] , Quote: "...Buddha's teaching that beings have no soul, no
    abiding essence. This 'no-soul doctrine' (anatta-vada) he expounded
    in his second sermon."