**無斑柱頜針魚**，又稱**台灣圓尾鶴鱵**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鶴鱵目](../Page/鶴鱵目.md "wikilink")[鶴鱵亞目](../Page/鶴鱵亞目.md "wikilink")[鶴鱵科的一個](../Page/鶴鱵科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於西[太平洋區](../Page/太平洋.md "wikilink")，包括[南海](../Page/南海.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新幾內亞等海域](../Page/新幾內亞.md "wikilink")。

## 深度

水深0至10公尺。

## 特徵

本魚口裂大，上下頷等長，牙齒呈針狀。魚體背部[綠色](../Page/綠色.md "wikilink")，體側[銀色](../Page/銀色.md "wikilink")，腹部則[白色](../Page/白色.md "wikilink")；在主鰓蓋骨與前鰓蓋骨間有一黑帶；胸鰭[黑色但末端](../Page/黑色.md "wikilink")[黃色](../Page/黃色.md "wikilink")。體略側扁且延長，上下頷細長如喙，具細齒。側線低，近腹緣，體被細鱗，背前鱗數130至180枚，胸鰭、腹鰭短小，尾鰭截平，背鰭位於臀鰭第7至10軟條基底之上方，具軟條17至21枚，臀鰭軟條23至25枚。體長可達100公分。另外，其牙齒銳利需小心咬傷。

## 生態

為熱帶海域表層性的肉食性魚類，常成群出現在河口及紅樹林地區。本種魚可洄溯至河川下游，有時還會躍出水面以尾鰭擊水前進，以[沙丁魚及](../Page/沙丁魚.md "wikilink")[銀漢魚為食](../Page/銀漢魚.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，以炭火慢烤，待熟後擠[檸檬汁於魚體上](../Page/檸檬汁.md "wikilink")，鮮美可口或煮薑絲清湯，味更美。因口中常有[寄生蟲寄生](../Page/寄生蟲.md "wikilink")，在處理時，應將頭部去除。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

## 扩展阅读

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[leiura](../Category/柱頜針魚屬.md "wikilink")