**無極**一詞源於[道德經](../Page/道德經.md "wikilink")、[莊子](../Page/莊子.md "wikilink")。

## 出处

### 道德經

### 莊子

## 和[太極的關係](../Page/太極.md "wikilink")

無極而太極 , 有些說 無極 = [炁](../Page/炁.md "wikilink")，目的是解析太極
的物質化或狀態化的來源.物理學對終極的來源是不知道的 (
沒有嚴密的實驗證明 ).太極 的詮釋可能是 :
[數學結構](../Page/數學結構.md "wikilink")
[等價](../Page/等價.md "wikilink") 某些
[物理意義](../Page/物理意義.md "wikilink").因為物理上沒有嚴密的實驗證明
終極的來源是甚麼，所以 太極 (數學結構 等價 某些 物理意義) 的來源是沒有人知道的.若要用 無極 = 炁 是太極
的來源, [邏輯經驗論者是不接受的](../Page/邏輯經驗論.md "wikilink") ; 因無極 = 炁 是非經驗 ,
但太極的意義是可[經驗的](../Page/經驗.md "wikilink").
有些看法亦將陰陽[物質化](../Page/物質.md "wikilink"),或是[狀態化](../Page/狀態化.md "wikilink").

## 参见

  - [太极](../Page/太极.md "wikilink")
  - [本体论 (哲学)](../Page/本体论_\(哲学\).md "wikilink")

## 参考文献

## 外部链接

[Category:道家](../Category/道家.md "wikilink")