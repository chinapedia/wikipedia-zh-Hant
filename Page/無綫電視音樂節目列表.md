本條目列出[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")（無綫電視）製作的**音樂節目**。
<small>註：節目名稱**前**的為該節目於[無綫電視的首播日期及頻道](../Page/電視廣播有限公司.md "wikilink")，節目名稱**後**的為該節目的結束播放日期（如有）。</small>

## 常規音樂節目

  - 1968年6月22日[翡翠台](../Page/翡翠台.md "wikilink")：[星光晚會](../Page/星光晚會.md "wikilink")（SING
    SING
    SING，歷任主持：[詹小屏](../Page/詹小屏.md "wikilink")、[鍾玲玲](../Page/鍾玲玲.md "wikilink")）
  - 1969年4月3日[翡翠台](../Page/翡翠台.md "wikilink")：[可樂晚唱](../Page/可樂晚唱.md "wikilink")（主持：[方逸華](../Page/方逸華.md "wikilink")）
  - 197_年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[弦韻寄心聲](../Page/弦韻寄心聲.md "wikilink")（主持：[鄭少秋](../Page/鄭少秋.md "wikilink")）
  - 197_年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[歌舞良宵](../Page/歌舞良宵.md "wikilink")（主持：[森森](../Page/森森.md "wikilink")）
  - 1974年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[載歌載舞](../Page/載歌載舞.md "wikilink")（主持：[森森](../Page/森森.md "wikilink")、[斑斑](../Page/斑斑.md "wikilink")、[櫻櫻](../Page/櫻櫻.md "wikilink")、[菁菁](../Page/菁菁.md "wikilink")）
  - 197_年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[節奏](../Page/節奏.md "wikilink")（主持：[容茱迪](../Page/容茱迪.md "wikilink")）
  - 1981年10月10日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")：[勁歌金曲](../Page/勁歌金曲.md "wikilink")
      - 2006年1月8日[翡翠台](../Page/翡翠台.md "wikilink")：[勁歌總選經典大件事](../Page/勁歌總選經典大件事.md "wikilink")（主持：[林敏聰](../Page/林敏聰.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")）
      - 2006年7月5日[翡翠台](../Page/翡翠台.md "wikilink")：勁歌金曲熱唱世界盃
        ([東港城特約](../Page/東港城.md "wikilink"))
      - 2007年1月6日[翡翠台](../Page/翡翠台.md "wikilink")：[翻轉勁歌25年](../Page/翻轉勁歌25年.md "wikilink")（呈獻）
      - 2008年1月5日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：志偉哈林講勁歌（主持：[曾志偉](../Page/曾志偉.md "wikilink")、[庾澄慶](../Page/庾澄慶.md "wikilink")）
      - 2011年1月1日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：玩轉勁歌30年（主持：崔建邦、羅敏莊）
  - 1984年1月1日[翡翠台](../Page/翡翠台.md "wikilink")：[新地任你點](../Page/新地任你點.md "wikilink")（主持：[林憶蓮](../Page/林憶蓮.md "wikilink")、[王喜](../Page/王喜.md "wikilink")、[王賢誌](../Page/王賢誌.md "wikilink")、[袁彩雲](../Page/袁彩雲.md "wikilink")、[吳家樂](../Page/吳家樂.md "wikilink")、[黃宗澤](../Page/黃宗澤.md "wikilink")；第一代主持：[胡渭康](../Page/胡渭康.md "wikilink")、[盧敏儀](../Page/盧敏儀.md "wikilink")、[區瑞強](../Page/區瑞強.md "wikilink")，1989至1997年間易名為《週末任你點》)<small>至1999年7月4日</small>
  - 1984年__月__日[明珠台](../Page/明珠台.md "wikilink")：[週一金榜流行曲](../Page/週一金榜流行曲.md "wikilink")<small>至1988年__月__日</small>
  - 1984年__月__日[明珠台](../Page/明珠台.md "wikilink")：[週五金榜流行曲](../Page/週五金榜流行曲.md "wikilink")<small>至1988年__月__日</small>
  - 1987年3月__日[明珠台](../Page/明珠台.md "wikilink")：[沙龍流行音樂轉播站](../Page/沙龍流行音樂轉播站.md "wikilink")
  - 1988年7月18日[翡翠台](../Page/翡翠台.md "wikilink")：[金曲挑戰站](../Page/金曲挑戰站.md "wikilink")<small>至2015年4月24日</small>
  - 1989年1月__日[翡翠台](../Page/翡翠台.md "wikilink")：[萬事發音樂看世界](../Page/萬事發音樂看世界.md "wikilink")
  - 1990年1月7日[翡翠台](../Page/翡翠台.md "wikilink")：[週末新地帶](../Page/週末新地帶.md "wikilink")（主持：[黃凱芹](../Page/黃凱芹.md "wikilink")、[梁榮忠](../Page/梁榮忠.md "wikilink")、[梁漢文](../Page/梁漢文.md "wikilink")、[阮兆祥](../Page/阮兆祥.md "wikilink")、[莫文蔚](../Page/莫文蔚.md "wikilink")，1997年前稱為《新地帶》)<small>至1999年3月27日</small>
  - 1990年7月__日[翡翠台](../Page/翡翠台.md "wikilink")：[萬事發音樂看世界](../Page/萬事發音樂看世界.md "wikilink")
  - 1990年__月__日[明珠台](../Page/明珠台.md "wikilink")：[MAXELL音樂動感](../Page/MAXELL音樂動感.md "wikilink")
  - 1990年10月11日[翡翠台](../Page/翡翠台.md "wikilink")：[靚歌穿梭伴我行](../Page/靚歌穿梭伴我行.md "wikilink")（主持：[陳啟泰](../Page/陳啟泰.md "wikilink")）
  - 1991年1月10日[翡翠台](../Page/翡翠台.md "wikilink")：[逝影流聲](../Page/逝影流聲.md "wikilink")（主持：[蔡楓華](../Page/蔡楓華.md "wikilink")）
  - 1991年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[樂韻夜風情](../Page/樂韻夜風情.md "wikilink")
  - 1991年10月5日[翡翠台](../Page/翡翠台.md "wikilink")：[翡翠音樂幹線](../Page/翡翠音樂幹線.md "wikilink")<small>至2004年7月24日</small>
  - 1993年3月6日[明珠台](../Page/明珠台.md "wikilink")：[明珠音樂幹線](../Page/明珠音樂幹線.md "wikilink")<small>至1993年6月26日</small>
  - 1995年2月__日[翡翠台](../Page/翡翠台.md "wikilink")：[樂壇新勢力](../Page/樂壇新勢力.md "wikilink")
  - 1995年__月__日[明珠台](../Page/明珠台.md "wikilink")：[亞洲音樂大放送](../Page/亞洲音樂大放送.md "wikilink")
  - 1995年9月18日[翡翠台](../Page/翡翠台.md "wikilink")：[好歌三點播](../Page/好歌三點播.md "wikilink")（主持：[李浩林](../Page/李浩林.md "wikilink")、[劉倩怡](../Page/劉倩怡.md "wikilink")）
  - 1997年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[深夜宣言](../Page/深夜宣言.md "wikilink")
  - 1999年7月11日[翡翠台](../Page/翡翠台.md "wikilink")：[非常音樂空間](../Page/非常音樂空間.md "wikilink")
  - 1999年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[星SING傳聲筒](../Page/星SING傳聲筒.md "wikilink")
  - 1999年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[勁歌超星座](../Page/勁歌超星座.md "wikilink")<small>至2004年7月17日</small>
  - 2001年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[熱唱K一族](../Page/熱唱K一族.md "wikilink")
  - 2002年9月23日[翡翠台](../Page/翡翠台.md "wikilink")：[SMS話點就點](../Page/SMS話點就點.md "wikilink")<small>至2004年7月17日</small>
  - 2004年7月18日[翡翠台](../Page/翡翠台.md "wikilink")：[重量Plug](../Page/重量Plug.md "wikilink")<small>至2015年4月19日</small>
  - 2004年7月24日[翡翠台](../Page/翡翠台.md "wikilink")：[音樂潮@giv](../Page/音樂潮@giv.md "wikilink")<small>至2005年7月9日</small>
  - 2004年7月27日[翡翠台](../Page/翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")：[無間音樂](../Page/無間音樂.md "wikilink")
  - 2005年7月16日[翡翠台](../Page/翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")：[360º音樂無邊](../Page/360º音樂無邊.md "wikilink")<small>至2011年9月24日</small>
  - 2010年2月28日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：我的故事．我的歌<small>至2010年4月25日</small>
  - 2010年5月11日[J2](../Page/J2.md "wikilink")：[巨聲工房](../Page/巨聲工房.md "wikilink")
  - 2010年12月12日[J2](../Page/J2.md "wikilink")、[音樂台](../Page/無綫音樂台.md "wikilink")：[Music
    Café](../Page/Music_Café.md "wikilink")
  - 2011年10月3日[翡翠台](../Page/翡翠台.md "wikilink")：[360°](../Page/360°.md "wikilink")<small>至2015年4月19日</small>
  - 2015年4月25日[翡翠台](../Page/翡翠台.md "wikilink")：[樂勢力](../Page/樂勢力.md "wikilink")
  - 2015年4月26日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")：[超級勁歌推介](../Page/超級勁歌推介.md "wikilink")

[TVB8](../Page/TVB8.md "wikilink")：

  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[華語金曲速遞](../Page/華語金曲速遞.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[音樂共同體](../Page/音樂共同體.md "wikilink")
  - 2005年11月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[火熱推介](../Page/火熱推介.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[型人型樂館](../Page/型人型樂館.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[寰宇音樂放送](../Page/寰宇音樂放送.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[搜搜金曲](../Page/搜搜金曲.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[音樂復刻](../Page/音樂復刻.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[K歌新登陸](../Page/K歌新登陸.md "wikilink")
  - 200_年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[音樂潮拜](../Page/音樂潮拜.md "wikilink")
  - 2006年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[眾星熱唱CONCENT
    BLOG](../Page/眾星熱唱CONCENT_BLOG.md "wikilink")

## 懷舊金曲/粤劇

  - 1968年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[廣東歌樂](../Page/廣東歌樂.md "wikilink")（主持：[胡章釗](../Page/胡章釗.md "wikilink")；編導：[鍾景輝](../Page/鍾景輝.md "wikilink")）
  - ____年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[曲藝大全](../Page/曲藝大全.md "wikilink")
  - 1976年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[家寶之聲](../Page/家寶之聲.md "wikilink")（主持/演出：[羅家英](../Page/羅家英.md "wikilink")、[李寶瑩](../Page/李寶瑩.md "wikilink")；編導：[温燦華](../Page/温燦華.md "wikilink")）
  - 2001年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[名曲滿天星](../Page/名曲滿天星.md "wikilink")（主持：[汪明荃](../Page/汪明荃.md "wikilink")）
  - 2003年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[紅星追聲名歌SING](../Page/紅星追聲名歌SING.md "wikilink")
  - 2005年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[金曲迴響香港情](../Page/金曲迴響香港情.md "wikilink")
  - 2006年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[超級靚聲演鬥廳](../Page/超級靚聲演鬥廳.md "wikilink")（；主持：[伍衞國](../Page/伍衞國.md "wikilink")、[關菊英](../Page/關菊英.md "wikilink")）
  - 2006年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[我們的世紀金曲](../Page/我們的世紀金曲.md "wikilink")（[晶苑地產特約](../Page/晶苑地產.md "wikilink")）
  - 2006年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[聽得到的回憶](../Page/聽得到的回憶.md "wikilink")
  - 2010年10月10日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[金曲擂台](../Page/金曲擂台.md "wikilink")
  - 2015年2月15日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[Sunday靚聲王](../Page/Sunday靚聲王.md "wikilink")（主持：[鄭少秋](../Page/鄭少秋.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")）

## 流行音樂頒獎典禮

  - 1977年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[金唱片頒獎典禮](../Page/金唱片頒獎典禮.md "wikilink")
  - 1984年1月28日[翡翠台](../Page/翡翠台.md "wikilink")：[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")
      - 1983年_月__日[翡翠台](../Page/翡翠台.md "wikilink")：[勁歌金曲季選](../Page/勁歌金曲季選.md "wikilink")<small>至2003年1_月__日</small>
      - 2004年6月5日[翡翠台](../Page/翡翠台.md "wikilink")：[勁歌金曲優秀選](../Page/勁歌金曲優秀選.md "wikilink")
  - 1999年__月__日：[TVB8金曲榜頒獎典禮](../Page/TVB8金曲榜頒獎典禮.md "wikilink")

## 歌唱/音樂比賽

  - 1968年7月10日[翡翠台](../Page/翡翠台.md "wikilink")：[聲寶之夜](../Page/聲寶之夜.md "wikilink")（歷任主持：[譚炳文](../Page/譚炳文.md "wikilink")，[鍾曉薇](../Page/鍾曉薇.md "wikilink")，[王愛明](../Page/王愛明.md "wikilink")，[歐嘉慧](../Page/歐嘉慧.md "wikilink")，[何守信](../Page/何守信.md "wikilink")，[李道洪](../Page/李道洪.md "wikilink")，[呂有慧等](../Page/呂有慧.md "wikilink")）
  - 1977年9月4日[翡翠台](../Page/翡翠台.md "wikilink")：1977業餘音樂天才比賽（主持：[周潤發](../Page/周潤發.md "wikilink")，[繆騫人](../Page/繆騫人.md "wikilink")，[林旭華](../Page/林旭華.md "wikilink"))
    (編導：[梁家樹](../Page/梁家樹.md "wikilink")；監製：[吳慧萍](../Page/吳慧萍.md "wikilink"))
  - 19__年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[銀河聲星Sing](../Page/銀河聲星Sing.md "wikilink")
  - 1982年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[TVB全球華人新秀歌唱大賽](../Page/TVB全球華人新秀歌唱大賽.md "wikilink")(前稱《**TVB8全球華人新秀歌唱大賽**》/《**英皇新秀歌唱大賽**》/《**全球華人新秀歌唱大賽**》/《**新秀歌唱大賽**》)
      - 1997年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[英皇新秀歌唱大賽](../Page/英皇新秀歌唱大賽.md "wikilink")（前稱《[全球華人新秀歌唱大賽香港區選拔賽](../Page/全球華人新秀歌唱大賽.md "wikilink")》）
  - 1991年2月3日[翡翠台](../Page/翡翠台.md "wikilink")：第一屆亞洲鐳射卡拉OK大賽香港區決賽（主持：[陳敏兒](../Page/陳敏兒.md "wikilink")，[廖啟智](../Page/廖啟智.md "wikilink"))
  - ____年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[CASH流行曲創作大賽](../Page/CASH流行曲創作大賽.md "wikilink")
  - 2006年__月__日[音樂台](../Page/無綫音樂台.md "wikilink")：[Umc音樂節](../Page/Umc音樂節.md "wikilink")（[南豐集團特約](../Page/南豐集團.md "wikilink")）
  - 2007年12月1日[翡翠台](../Page/翡翠台.md "wikilink")：[東亞澳門新星選拔大賽](../Page/東亞澳門新星選拔大賽.md "wikilink")
  - 2007年12月1日[翡翠台](../Page/翡翠台.md "wikilink")：[18區粵曲粵唱越開心](../Page/18區粵曲粵唱越開心.md "wikilink")
  - 2009年7月19日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[超級巨聲](../Page/超級巨聲.md "wikilink")
      - 2010年2月27日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：超級巨聲畢業演唱會
  - 2010年5月9日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[超級巨聲2](../Page/超級巨聲2.md "wikilink")
      - 2010年9月26日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：巨聲同學會
      - 2010年10月9日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：超級巨聲2畢業演唱會
  - 2011年8月14日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[超級巨聲3](../Page/超級巨聲3.md "wikilink")
  - 2014年10月25日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[超級巨聲4](../Page/超級巨聲4.md "wikilink")
      - 2015年2月7日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：超級巨聲4畢業演唱會

## 音樂會

  - 2006年9月16日[翡翠台](../Page/翡翠台.md "wikilink")：聽側田唱音樂會（[屈臣氏蒸餾水特約](../Page/屈臣氏集團.md "wikilink")）
  - 2006年10月28日[翡翠台](../Page/翡翠台.md "wikilink")：往事只能回味劉家昌音樂會（[四洲集團特約](../Page/四洲集團.md "wikilink")）
  - 2006年12月16日[翡翠台](../Page/翡翠台.md "wikilink")：10 X 10我至愛演唱會
  - 2007年1月1日[翡翠台](../Page/翡翠台.md "wikilink")：聽側田唱廣州音樂會（[屈臣氏蒸餾水特約](../Page/屈臣氏集團.md "wikilink")）
  - 2007年2月18日[翡翠台](../Page/翡翠台.md "wikilink")：我們的世紀金曲演唱會
  - 2007年12月15日[翡翠台](../Page/翡翠台.md "wikilink")：你想成真廿載情（信用卡20周年特約）
  - 2010年9月18日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：超級巨聲SUPER
    VOICE演唱會
  - 2010年12月19日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：荃加福祿壽宇宙最長演唱會
    長做長有版
  - 2012年11月17日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：那些年
    爆笑不離3兄弟演唱會
  - 2013年3月31日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：繼續寵愛·十年·音樂會
  - 2013年6月1日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：開心果永遠欣想妳演唱會
  - 2013年7月1日[翡翠台](../Page/翡翠台.md "wikilink")：[香港巨蛋音樂節](../Page/香港巨蛋音樂節.md "wikilink")
  - 2013年10月20日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：梅艷芳·十年回憶音樂會
  - 2014年7月1日[翡翠台](../Page/翡翠台.md "wikilink")：香港巨蛋音樂節2014
  - 2015年2月27日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：菀之歌‧菀之論演唱會

## 音樂特輯

  - 1969年6月19日[翡翠台](../Page/翡翠台.md "wikilink")：[芳芳的旋律](../Page/芳芳的旋律.md "wikilink")
    (主持/演出：[蕭芳芳嘉賓](../Page/蕭芳芳.md "wikilink")：[許冠傑](../Page/許冠傑.md "wikilink")、[許冠文](../Page/許冠文.md "wikilink")、[鄧光榮](../Page/鄧光榮.md "wikilink")、[黃霑](../Page/黃霑.md "wikilink")、[王愛明等](../Page/王愛明.md "wikilink"))至1969年11月2日
  - 1970年6月8日[翡翠台](../Page/翡翠台.md "wikilink")：[芳芳的旋律](../Page/芳芳的旋律.md "wikilink")
    (主持/演出：[蕭芳芳](../Page/蕭芳芳.md "wikilink"))（端午節特備節目）
  - 1970年9月19日[翡翠台](../Page/翡翠台.md "wikilink")：[姚蘇蓉之歌](../Page/姚蘇蓉之歌.md "wikilink")（演出：[姚蘇蓉](../Page/姚蘇蓉.md "wikilink")）
  - 1970年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[筷子之歌](../Page/筷子之歌.md "wikilink")（演出：[筷子姊妺花](../Page/筷子姊妺花.md "wikilink")、[桃麗絲](../Page/桃麗絲.md "wikilink")，編導：[蔡和平](../Page/蔡和平.md "wikilink")）（日立牌家庭電器及瑞興公司聯合特約）
  - 1970年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[羣星之歌](../Page/羣星之歌.md "wikilink")（演出：[姚蘇蓉](../Page/姚蘇蓉.md "wikilink")）
  - 1971年5月29日[翡翠台](../Page/翡翠台.md "wikilink")：[陳芬蘭之歌](../Page/陳芬蘭之歌.md "wikilink")（演出：[陳芬蘭](../Page/陳芬蘭.md "wikilink")）
  - 1974年1月25日[翡翠台](../Page/翡翠台.md "wikilink")：[四朵金花特輯](../Page/四朵金花特輯.md "wikilink")（演出：[王愛明](../Page/王愛明.md "wikilink")、[張德蘭](../Page/張德蘭.md "wikilink")、[沈殿霞](../Page/沈殿霞.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")）
  - 1974年1月25日[翡翠台](../Page/翡翠台.md "wikilink")：[葉麗儀特輯](../Page/葉麗儀特輯.md "wikilink")（演出：[葉麗儀](../Page/葉麗儀.md "wikilink")）
  - 1974年9月26日[翡翠台](../Page/翡翠台.md "wikilink")：[仙杜拉之歌](../Page/仙杜拉之歌.md "wikilink")（演出：[仙杜拉](../Page/仙杜拉.md "wikilink")）
  - 1975年6月12日[翡翠台](../Page/翡翠台.md "wikilink")：[關菊英特輯](../Page/關菊英特輯.md "wikilink")（演出：[關菊英](../Page/關菊英.md "wikilink")）
  - 1975年6月19日[翡翠台](../Page/翡翠台.md "wikilink")：[鍾玲玲特輯](../Page/鍾玲玲特輯.md "wikilink")（演出：[鍾玲玲](../Page/鍾玲玲.md "wikilink")，編導：[馮吉隆](../Page/馮吉隆.md "wikilink")）
  - 1976年5月3日[翡翠台](../Page/翡翠台.md "wikilink")：[杜麗莎之歌](../Page/杜麗莎之歌.md "wikilink")（演出：[杜麗莎](../Page/杜麗莎.md "wikilink")，編導：[劉韻姿](../Page/劉韻姿.md "wikilink")）
  - 1976年5月__日[翡翠台](../Page/翡翠台.md "wikilink")：[四樂士特輯](../Page/四樂士特輯.md "wikilink")（演出：[森森](../Page/森森.md "wikilink")、[斑斑](../Page/斑斑.md "wikilink")、[賈思樂](../Page/賈思樂.md "wikilink")、[李振輝](../Page/李振輝.md "wikilink")）
  - 1977年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[甄妮特輯77](../Page/甄妮特輯77.md "wikilink")（演出：[甄妮](../Page/甄妮.md "wikilink")）
  - 1977年8月2日[翡翠台](../Page/翡翠台.md "wikilink")：[歐陽菲菲特輯](../Page/歐陽菲菲特輯.md "wikilink")（演出：[歐陽菲菲](../Page/歐陽菲菲.md "wikilink")，編導：[李鼎倫](../Page/李鼎倫.md "wikilink"))
    (監製：[吳慧萍](../Page/吳慧萍.md "wikilink")）
  - 1977年9月11日[翡翠台](../Page/翡翠台.md "wikilink")：[鄧麗君特輯](../Page/鄧麗君特輯.md "wikilink")（演出：[鄧麗君](../Page/鄧麗君.md "wikilink")）
  - 1978年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[甄妮特輯78](../Page/甄妮特輯78.md "wikilink")（演出：[甄妮](../Page/甄妮.md "wikilink")）
  - 1978年2月4日[翡翠台](../Page/翡翠台.md "wikilink")：[樂在其中](../Page/樂在其中.md "wikilink")（演出：[賈思樂](../Page/賈思樂.md "wikilink")、[露雲娜](../Page/露雲娜.md "wikilink")，共15輯）
  - 1980年2月8日[明珠台](../Page/明珠台.md "wikilink")：歌聲滿香江
  - 1980年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：甄妮之夏日戀情
  - 1981年1月3日[翡翠台](../Page/翡翠台.md "wikilink")：群星拱照顧嘉輝（主持：[黃霑](../Page/黃霑.md "wikilink")、[葉麗儀](../Page/葉麗儀.md "wikilink"))
    (監製：[馮吉隆](../Page/馮吉隆.md "wikilink"))
  - 1982年7月__日[翡翠台](../Page/翡翠台.md "wikilink")：十彩新地
  - 1983年12月18日[明珠台](../Page/明珠台.md "wikilink")：弦樂飄飄同競技
  - 1984年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：甄妮特輯之不再孤獨
  - 1988年3月27日[翡翠台](../Page/翡翠台.md "wikilink")：譚詠麟之友情一線牽（演出：[譚詠麟](../Page/譚詠麟.md "wikilink")）
  - 1988年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：金光燦爛徐小鳳（演出：[徐小鳳](../Page/徐小鳳.md "wikilink")）
  - 1988年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：梅艷芳之夢裏風情（演出：[梅艷芳](../Page/梅艷芳.md "wikilink")）
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：[浪奔浪流十九年](../Page/浪奔浪流十九年.md "wikilink")（演出：[葉麗儀](../Page/葉麗儀.md "wikilink")）
  - 1989年4月23日[翡翠台](../Page/翡翠台.md "wikilink")：日落巴黎（演出：[張國榮](../Page/張國榮.md "wikilink")、[鍾楚紅](../Page/鍾楚紅.md "wikilink")、[張曼玉](../Page/張曼玉.md "wikilink")，導演：[吳宇森](../Page/吳宇森.md "wikilink")）
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：陳百強感情寫真
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：梅艷芳巴西熱浪嘉年華（演出：[梅艷芳](../Page/梅艷芳.md "wikilink")、[江欣燕](../Page/江欣燕.md "wikilink")）
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：羅文冷暖激流音樂特輯
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：張學友特輯之我在栢斯的日子（演出：[張學友](../Page/張學友.md "wikilink")、[陳法蓉](../Page/陳法蓉.md "wikilink")、[吳大維](../Page/吳大維.md "wikilink")，編導：[小美](../Page/小美.md "wikilink")）
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：林憶蓮今夕何夕音樂特輯（演出：[林憶蓮](../Page/林憶蓮.md "wikilink")、[王敏德](../Page/王敏德.md "wikilink")、[曾江](../Page/曾江.md "wikilink")、[黃秋生](../Page/黃秋生.md "wikilink")）
  - 1989年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：甄妮香港情未了
  - 1990年1月__日[翡翠台](../Page/翡翠台.md "wikilink")：純美接觸-{意大利}-（演出：[鄺美雲](../Page/鄺美雲.md "wikilink")）
  - 1990年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：李克勤熱浪迷情（演出：[李克勤](../Page/李克勤.md "wikilink")、[劉小慧](../Page/劉小慧.md "wikilink")、[林穎嫺](../Page/林穎嫺.md "wikilink")、[關淑怡](../Page/關淑怡.md "wikilink")）
  - 1990年9月2日[翡翠台](../Page/翡翠台.md "wikilink")：譚詠麟
    夢幻之旅（演出：[譚詠麟與當地女演員](../Page/譚詠麟.md "wikilink")，編導：[小美](../Page/小美.md "wikilink")）
  - 1990年9月22日[翡翠台](../Page/翡翠台.md "wikilink")：徐小鳳
    情懷維也納（演出：[徐小鳳](../Page/徐小鳳.md "wikilink")）
  - 1990年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：劉美君音樂特輯之四度誘惑（演出：[劉美君](../Page/劉美君.md "wikilink")、[張學友](../Page/張學友.md "wikilink")、[劉德華](../Page/劉德華.md "wikilink")、[黎明](../Page/黎明.md "wikilink")、[梁家輝](../Page/梁家輝.md "wikilink")）
  - 1991年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：穿梭夢裏人(音樂單元劇)（演出：[張學友](../Page/張學友.md "wikilink")、[張曼玉](../Page/張曼玉.md "wikilink")、[吳大維](../Page/吳大維.md "wikilink")、[何婉盈](../Page/何婉盈.md "wikilink")）
  - 1991年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：林子祥之情懷洛杉磯
  - 1991年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：舞台巨星許冠傑 靚歌迴響曲中情
  - 1991年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：黎明音樂特輯之我的感覺
  - 1991年12月__日[翡翠台](../Page/翡翠台.md "wikilink")：日立劉德華音樂電影之一起走過紅場的日子（）（演出：[劉德華](../Page/劉德華.md "wikilink")、[關之琳](../Page/關之琳.md "wikilink")）
  - 1992年1月__日[翡翠台](../Page/翡翠台.md "wikilink")：林憶蓮音樂特輯之情傾黃浦江
  - 1992年3月__日[翡翠台](../Page/翡翠台.md "wikilink")：現代愛情戀曲
  - 1992年7月12日[翡翠台](../Page/翡翠台.md "wikilink")：葉蒨文音樂特輯之瀟灑闖紅塵（演出：[葉蒨文](../Page/葉蒨文.md "wikilink")、[鞏俐](../Page/鞏俐.md "wikilink")、[成龍](../Page/成龍.md "wikilink")、[林子祥](../Page/林子祥.md "wikilink")、[杜德偉](../Page/杜德偉.md "wikilink")、[陳玉蓮](../Page/陳玉蓮.md "wikilink")）
  - 1992年7月26日[翡翠台](../Page/翡翠台.md "wikilink")：劉德華音樂特輯之情路狂奔（演出：[劉德華](../Page/劉德華.md "wikilink")、[郭藹明](../Page/郭藹明.md "wikilink")、[苗僑偉](../Page/苗僑偉.md "wikilink")）
  - 1992年8月__日[翡翠台](../Page/翡翠台.md "wikilink")：700愛情專線（）
  - 1992年9月20日[翡翠台](../Page/翡翠台.md "wikilink")：黎明音樂特輯之一夜傾情（演出：[黎明](../Page/黎明.md "wikilink")）
  - 1993年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：周慧敏音樂特輯之迷情戀界（演出：[周慧敏](../Page/周慧敏.md "wikilink")、[蔡一傑](../Page/蔡一傑.md "wikilink")、[蔡一智](../Page/蔡一智.md "wikilink")、[蘇志威](../Page/蘇志威.md "wikilink")、[劉青雲](../Page/劉青雲.md "wikilink")、[鍾鎮濤](../Page/鍾鎮濤.md "wikilink")、[林俊賢](../Page/林俊賢.md "wikilink")）
  - 1993年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：葉玉卿音樂特輯之碧波盪漾昆士蘭（演出：[葉玉卿](../Page/葉玉卿.md "wikilink")、[鄭浩南](../Page/鄭浩南.md "wikilink")、[陳淑蘭](../Page/陳淑蘭.md "wikilink")）
  - 1993年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：李克勤音樂特輯之驀然回首（演出：[李克勤](../Page/李克勤.md "wikilink")、[袁詠儀](../Page/袁詠儀.md "wikilink")，編導：[小美](../Page/小美.md "wikilink")）
  - 1993年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：梁朝偉,王靖雯音樂特輯之栢斯思我愛你（演出：[梁朝偉](../Page/梁朝偉.md "wikilink")、[王靖雯](../Page/王靖雯.md "wikilink")、[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")）
  - 1993年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：郭富城音樂特輯之希臘驚情（演出：[郭富城](../Page/郭富城.md "wikilink")、[周文健](../Page/周文健.md "wikilink")、[鍾麗緹](../Page/鍾麗緹.md "wikilink")、[劉殷伶](../Page/劉殷伶.md "wikilink")）
  - 1993年10月10日[翡翠台](../Page/翡翠台.md "wikilink")：陳慧嫻音樂特輯93之[紐約嫻情](../Page/紐約嫻情.md "wikilink")（演出：[陳慧嫻](../Page/陳慧嫻.md "wikilink")）
  - 1993年12月__日[翡翠台](../Page/翡翠台.md "wikilink")：劉德華音樂電影之波多黎各的童話（）（演出：[劉德華](../Page/劉德華.md "wikilink")、[翁杏蘭](../Page/翁杏蘭.md "wikilink")、[楚原](../Page/楚原.md "wikilink")、[尹揚明](../Page/尹揚明.md "wikilink")）
  - 1994年2月20日[翡翠台](../Page/翡翠台.md "wikilink")：葉玉卿音樂特輯之卿卿的禁區
  - 1994年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：梅艷芳音樂特輯之情歸何處
  - 1994年11月__日[翡翠台](../Page/翡翠台.md "wikilink")：鄭秀文音樂特輯之潮流的誘惑
  - 1994年12月11日[翡翠台](../Page/翡翠台.md "wikilink")：黎明音樂特輯之北京的黎明（演出：[黎明](../Page/黎明.md "wikilink")、[梁詠琪](../Page/梁詠琪.md "wikilink")）
  - 1995年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：張學友音樂特輯之洛杉磯戀曲（演出：[張學友](../Page/張學友.md "wikilink")、[陳慧琳](../Page/陳慧琳.md "wikilink")、[葛民輝](../Page/葛民輝.md "wikilink")）
  - 1995年9月__日[翡翠台](../Page/翡翠台.md "wikilink")：鄭伊健奇趣東遊記（演出：[鄭伊健](../Page/鄭伊健.md "wikilink")、[蔡安蕎](../Page/蔡安蕎.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")）
  - 1995年11月__日[翡翠台](../Page/翡翠台.md "wikilink")：彭羚音樂特輯之羚的特集
  - 1996年1月5日[翡翠台](../Page/翡翠台.md "wikilink")：郭富城昆士蘭特輯之失憶情緣（演出：[郭富城](../Page/郭富城.md "wikilink")、[朱茵](../Page/朱茵.md "wikilink")、[成奎安](../Page/成奎安.md "wikilink")、[歐陽震華](../Page/歐陽震華.md "wikilink")）
  - 1996年12月__日[翡翠台](../Page/翡翠台.md "wikilink")：第一太平銀行特約：陳慧嫻英倫奇緣（演出：[陳慧嫻](../Page/陳慧嫻.md "wikilink")）
  - 1997年2月__日[翡翠台](../Page/翡翠台.md "wikilink")：黎明情深說話盡情講
  - 1997年3月__日[翡翠台](../Page/翡翠台.md "wikilink")：鄭秀文音樂特輯：97男歡女愛（演出：[鄭秀文](../Page/鄭秀文.md "wikilink")、[王喜](../Page/王喜_\(香港\).md "wikilink")、[梅小惠](../Page/梅小惠.md "wikilink")、[朱永棠](../Page/朱永棠.md "wikilink")、[李珊珊](../Page/李珊珊.md "wikilink")）
  - 1997年3月__日[翡翠台](../Page/翡翠台.md "wikilink")：梅艷芳音樂特輯之三誓盟（演出：[梅艷芳](../Page/梅艷芳.md "wikilink")、[盧慶輝](../Page/盧慶輝.md "wikilink")、[陳子聰](../Page/陳子聰.md "wikilink")）
  - 1998年4月__日[翡翠台](../Page/翡翠台.md "wikilink")：許志安音樂特輯之友緣情感覺
  - 1998年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：蘇永康澳洲音樂特輯：我為你傾心（演出：[蘇永康](../Page/蘇永康.md "wikilink")、[林保怡](../Page/林保怡.md "wikilink")）
  - 1998年7月__日[翡翠台](../Page/翡翠台.md "wikilink")：羅文弦曲情繫俄羅斯
  - 1998年10月__日[翡翠台](../Page/翡翠台.md "wikilink")：陳慧琳愛情一卡拉南非（演出：[陳慧琳](../Page/陳慧琳.md "wikilink")、[郭耀明](../Page/郭耀明.md "wikilink")）
  - 1998年11月__日[翡翠台](../Page/翡翠台.md "wikilink")：梁詠琪音樂特輯之加州琪遇記
  - 1998年12月__日[翡翠台](../Page/翡翠台.md "wikilink")：王菲音樂特輯之菲菲依然係我（演出：[王菲](../Page/王菲.md "wikilink")）
  - 1999年10月10日[翡翠台](../Page/翡翠台.md "wikilink")：張國榮音樂電影之左右情緣（演出：[張國榮](../Page/張國榮.md "wikilink")、[邱淑貞](../Page/邱淑貞.md "wikilink")、[張柏芝](../Page/張柏芝.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")、[曾志偉](../Page/曾志偉.md "wikilink")、[吳君如](../Page/吳君如.md "wikilink")、[梅艷芳](../Page/梅艷芳.md "wikilink")，導演：張國榮）
  - 2001年4月15日[翡翠台](../Page/翡翠台.md "wikilink")：譚詠麟 愛的新體驗
  - 2001年9月22日、29日[翡翠台](../Page/翡翠台.md "wikilink")：甄妮：生命的插曲
  - 2001年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：陳奕迅音樂電影：藍寶石的夜空（演出：[陳奕迅](../Page/陳奕迅.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")）
  - 2002年3月10日[翡翠台](../Page/翡翠台.md "wikilink")：梅艷芳芳華絕代傾情夜（主持：[曾志偉](../Page/曾志偉.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink"))
  - 2002年12月__日[翡翠台](../Page/翡翠台.md "wikilink")：楊千嬅音樂特輯：當飄雪戀上聖誕樹（演出：[楊千嬅](../Page/楊千嬅.md "wikilink")、[古天樂](../Page/古天樂.md "wikilink")）
  - 2003年5月__日[翡翠台](../Page/翡翠台.md "wikilink")：三色糖的情式（演出：[盧巧音](../Page/盧巧音.md "wikilink")、[盧海鵬](../Page/盧海鵬.md "wikilink")、[黎明](../Page/黎明.md "wikilink")、[林嘉欣](../Page/林嘉欣.md "wikilink")、[曾志偉](../Page/曾志偉.md "wikilink")、[陳小春](../Page/陳小春.md "wikilink")、[李燦森](../Page/李燦森.md "wikilink")、[鄭中基](../Page/鄭中基.md "wikilink")）
  - 2003年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：古天樂音樂特輯之緣份咖啡室（演出：[古天樂](../Page/古天樂.md "wikilink")、[李心潔](../Page/李心潔.md "wikilink")、[林雪](../Page/林雪.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")、[梁家輝](../Page/梁家輝.md "wikilink")、[陳奕迅](../Page/陳奕迅.md "wikilink")）
  - 2004年__月__日[翡翠台](../Page/翡翠台.md "wikilink")：樂壇教父Uncle Ray
  - 2005年10月1日[翡翠台](../Page/翡翠台.md "wikilink")：由世界中心開始（歌星：[楊千嬅](../Page/楊千嬅.md "wikilink")、[古巨基](../Page/古巨基.md "wikilink")、[雷頌德](../Page/雷頌德.md "wikilink")、[側田](../Page/側田.md "wikilink")）
  - 2006年12月30日[翡翠台](../Page/翡翠台.md "wikilink")：溫拿狂想33
  - 2007年1月20日[翡翠台](../Page/翡翠台.md "wikilink")：好歌來自呂方
  - 2007年5月12日[翡翠台](../Page/翡翠台.md "wikilink")： 2007
  - 2007年11月25日[翡翠台](../Page/翡翠台.md "wikilink")：人生多麼好<small>至2007年12月2日</small>
  - 2008年1月6日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[克勤高清演藝廳](../Page/克勤高清演藝廳.md "wikilink")（主持：[李克勤](../Page/李克勤.md "wikilink")）
  - 2008年3月29日音樂台：[情迷一夜劉美君](../Page/情迷一夜劉美君.md "wikilink")
  - 2008年11月29日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[藍兒本色](../Page/藍兒本色.md "wikilink")
  - 2009年3月21日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[謝安琪玩謝安琪](../Page/謝安琪玩謝安琪.md "wikilink")
  - 2009年5月16日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[峯從那裡來](../Page/峯從那裡來.md "wikilink")
  - 2011年1月23日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[鄭伊健Beautiful
    Life](../Page/鄭伊健Beautiful_Life.md "wikilink")
  - 2011年1月30日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[周慧敏．最愛V25](../Page/周慧敏．最愛V25.md "wikilink")（主持：[黃宇詩](../Page/黃宇詩.md "wikilink")）
  - 2011年2月6日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[呂方．唱出味道來](../Page/呂方．唱出味道來.md "wikilink")
  - 2011年2月13日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[明秋喜相逢](../Page/明秋喜相逢.md "wikilink")（主持：[張繼聰](../Page/張繼聰.md "wikilink")）
  - 2011年2月20日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[梁詠琪︰高妹G夜](../Page/梁詠琪︰高妹G夜.md "wikilink")（主持：[黎國輝](../Page/黎國輝.md "wikilink")）
  - 2011年5月1日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：[許志安一直相愛25年](../Page/許志安一直相愛25年.md "wikilink")
  - 2013年1月5日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：得閒講
    德嫻唱
  - 2013年1月12日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：十分熹祥十分吋（主持：[林子祥](../Page/林子祥.md "wikilink")、[趙增熹](../Page/趙增熹.md "wikilink")）
  - 2014年1月26日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：小鳳·姐·有約
  - 2014年6月28日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：巫啟賢･迷･音樂
  - 2014年7月5日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：杜德偉･巨星同學會
  - 2014年7月25日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：一生一世容袓兒
  - 2014年11月29日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：譚詠麟笑唱人生40年
  - 2014年12月13日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：遇上千嬅
    ･ 遇見愛
  - 2015年3月21日[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")：樂壇教父顧嘉煇（主持：[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")）
  - 2018年4月7日[翡翠台](../Page/翡翠台.md "wikilink")：古巨基初心再體驗
  - 2019年1月6日[翡翠台](../Page/翡翠台.md "wikilink")：軒仔與他的產地（主持：[陸浩明](../Page/陸浩明.md "wikilink")）
  - 2019年2月24日[翡翠台](../Page/翡翠台.md "wikilink")：尋找阿Lam的那些年（主持：[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")）

## 節目待查

  - 溫拿狂想曲（）
  - [李蕙敏音樂特輯之複製情緣](../Page/李蕙敏.md "wikilink")

[無綫電視音樂節目](../Category/無綫電視音樂節目.md "wikilink")
[音](../Category/無綫電視節目列表.md "wikilink")