[Cisco_Aironet_1131AG_-_Close.jpg](https://zh.wikipedia.org/wiki/File:Cisco_Aironet_1131AG_-_Close.jpg "fig:Cisco_Aironet_1131AG_-_Close.jpg")
Aironet 无线接入点\]\]

**無線存取點**（英語：Wireless Access
Point，縮寫**WAP**）是[電腦網絡中一种連接](../Page/電腦網絡.md "wikilink")[無線網絡至有線網絡](../Page/無線網絡.md "wikilink")（[乙太網](../Page/乙太網.md "wikilink")）的裝置，又稱為**無線基地台**。它通常作為一個單獨裝置，並透過有線網路連接到[路由器](../Page/路由器.md "wikilink")，也能與路由器整合在一起。無線接入點和[熱點不同](../Page/热点_\(Wi-Fi\).md "wikilink")，後者是指稱能夠連接[無線區域網的物理地點](../Page/無線區域網.md "wikilink")。[漫游則是數個無線接入點運行](../Page/漫游.md "wikilink")，將傳送的[數據由一個接入點連至到另一接入點使該無線網絡擴大](../Page/數據.md "wikilink")；反之整個無線網路若沒有任何接入點就會成為一個點對點的[ad-hoc網路](../Page/Ad_Hoc网络.md "wikilink")。無線接入點亦具有[DHCP之動態配置IP位址功能](../Page/DHCP.md "wikilink")。

## 介紹

2010年之後，便宜、容易安裝的無線接收盒迅速地流行。這些裝置避免有線的乙太網絡纜線的纏結混亂。在[商業](../Page/商業.md "wikilink")、[家居或是](../Page/家居.md "wikilink")[學校如要鋪設有線網路經常需要纜線通過](../Page/學校.md "wikilink")[牆壁和](../Page/牆壁.md "wikilink")[天花板](../Page/天花板.md "wikilink")，無線網路可以大幅減少這些纜線，甚至可以不用。無線網路給用戶有更大的流動性，不用被網纜縛住。今天的无线接取器通常支持使用无线电波接受发送数据的标准，这些标准和它们使用的频率是由[IEEE定义的](../Page/IEEE.md "wikilink")。大部分无线接取器使用[IEEE
802.11标准](../Page/IEEE_802.11.md "wikilink")。

## 常见应用

一个典型的企业级应用，包括附加几个无线接入点到有线网络，然后提供无线接入办公[局域网](../Page/局域网.md "wikilink")。无线接入点的管理是由[无线局域网控制器负责处理自动调节射频功率](../Page/无线局域网控制器.md "wikilink")，通道，身份验证和安全性。此外，控制器可以组合成一个无线移动集团，允许跨控制器漫游。该控制器可以是流动性域的一部分，能够让客户在整个大的或地区级办公室地点的访问。这样可以节省客户的时间和管理开销，因为它可以自动重新关联或重新验证。此外，多个控制器和所有连接到这些控制器的数百个接入点都可以通过一个叫思科无线控制系统的软件来管理。这个软件可以处理一个和控制器相同的功能还增加了用户的映射或RFID地点的奖金功能上载的地图，提升控制器和接入点的固件，和流氓检测/处理。在这种情况下，无线接入点的功能是作为客户的无线网关来访问有线网络。

[熱點是一个常见的公用的无线接入点](../Page/热点_\(Wi-Fi\).md "wikilink")，其中无线客户端可以连接到互联网。这一现象已经很普遍了，在大城市的咖啡馆，图书馆，目前，在台灣已相當普及。
[Planet_WAP-4000.JPG](https://zh.wikipedia.org/wiki/File:Planet_WAP-4000.JPG "fig:Planet_WAP-4000.JPG")
在[工商業方面](../Page/工商業.md "wikilink")，無線網路對日常運作有重要影響，企業員工經常需操作附有[條碼掃描器及無線連接的可攜式資料終端機](../Page/條碼掃描器.md "wikilink")，以便讓他們即時更新工作進度和存貨。

一個符合[IEEE
802.11协议的无线接入点可與大約](../Page/IEEE_802.11.md "wikilink")30個位於[半徑](../Page/半徑.md "wikilink")[100米之內使用者端聯絡](../Page/100米.md "wikilink")。可是，无线接入点受到室內或室外設置、距離地面高度、附近的阻礙物、使用相同頻率電子裝置、[天線](../Page/天線.md "wikilink")、[天氣](../Page/天氣.md "wikilink")、使用[頻道和裝置的功率等因素影響](../Page/頻道.md "wikilink")，致使通訊範圍變化很大。網路設計工程師藉由[中繼器和](../Page/中繼器.md "wikilink")[反射器去放大和反射無線信號](../Page/反射器.md "wikilink")，擴大無線接收盒的接收範圍。在測試中，無線網路操作範圍可達數[公里](../Page/公里.md "wikilink")。

一個典型的企業應用，就是在有線網路上安裝數個无线接入点，提供辦公室區域網路的無線存取。在无线接入点的接收範圍內，無線用戶端既有移动性的好處，又能充分地與網路連接。在這種場合，无线接入点成為使用者端接入有線網路的一個接口。另外一個用途則是不予許使用網纜連接情況：例如，製造商使用無線網路連接辦公室和貨倉之間的網路連線。

## 安全性

[Network-wireless-encrypted.svg](https://zh.wikipedia.org/wiki/File:Network-wireless-encrypted.svg "fig:Network-wireless-encrypted.svg")
无线接入点在信息安全上有特別的顧慮。許多無線接收盒的安全性僅依靠實體的存取控制，並預設區域網路的所有使用者都是可信任的；然而無線接收盒若被連接到網路上，則其涵蓋範圍內的電子裝置均可能連線到此無線接收盒。目前最常見的解決方案為無線傳輸加密。先進的無線接收盒均有內建加密機制。第一代的資料加密機制[WEP容易被破解](../Page/WEP.md "wikilink")；相較之下，第二代和第三代的[WPA及](../Page/WPA.md "wikilink")[WPA2通常被認為是較安全的加密機制](../Page/WPA2.md "wikilink")（具有較安全的[密码或](../Page/密碼_\(認證\).md "wikilink")（passphrase）的情形下）。\[1\]

基於每個人都可享有隨時隨地連上網路的好處，一些人主張无线接入点对大众开放其使用权。

## 主要厂商

### 美国

  -
  - [Cisco Systems](../Page/Cisco_Systems.md "wikilink")

  - [3Com](../Page/3Com.md "wikilink")

  - [Netgear](../Page/Netgear.md "wikilink")

  -
  -
### 中国

  - [H3C](../Page/H3C.md "wikilink")
  - [Huawei](../Page/Huawei.md "wikilink")
  - [TP-Link](../Page/TP-Link.md "wikilink")
  - [OUTENGDA](../Page/OUTENGDA.md "wikilink")

### 台灣

  - [D-Link](../Page/D-Link.md "wikilink")
  - [ZyXEL](../Page/ZyXEL.md "wikilink")
  - [ASUS](../Page/ASUS.md "wikilink")
  - [EDIMAX](../Page/訊舟科技.md "wikilink")
  - [居易科技](../Page/居易科技.md "wikilink")

## 参考条目

  - [中继器](../Page/中继器.md "wikilink")
  - [网络地址转换](../Page/网络地址转换.md "wikilink")（NAT）
  - [桥接器](../Page/桥接器.md "wikilink")
  - [集线器](../Page/集线器.md "wikilink")
  - [路由器](../Page/路由器.md "wikilink")
  - [網路交換器](../Page/網路交換器.md "wikilink")
  - [调制解调器](../Page/调制解调器.md "wikilink")
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")
  - [热点 (Wi-Fi)](../Page/热点_\(Wi-Fi\).md "wikilink")
  - [IEEE 802.11](../Page/IEEE_802.11.md "wikilink")

## 參考資料

## 外部連結

  - [IBM: Building a wireless access point on
    Linux](http://www.ibm.com/developerworks/library/l-wap.html?ca=dnt-429)
  - [RouterGuide.org](https://web.archive.org/web/20130624172634/http://routerguide.org/)

[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")

1.  <http://www.wi-fi.org/faqs.php>