**無聊龍屬**（[學名](../Page/學名.md "wikilink")︰*Borogovia*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，是靈活的[肉食性](../Page/肉食性.md "wikilink")[傷齒龍科恐龍](../Page/傷齒龍科.md "wikilink")，生存於[上白堊紀的](../Page/白堊紀.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")。
[Jabberwocky_creatures.jpg](https://zh.wikipedia.org/wiki/File:Jabberwocky_creatures.jpg "fig:Jabberwocky_creatures.jpg")的奇特生物「Borogoves」，是本屬的命名來源\]\]
在1971年，一個[波蘭與](../Page/波蘭人民共和國.md "wikilink")[蒙古的聯合挖掘團隊](../Page/蒙古人民共和國.md "wikilink")，在[南戈壁省的](../Page/南戈壁省.md "wikilink")[耐梅蓋吐盆地發現一個小型獸腳類化石](../Page/耐梅蓋吐盆地.md "wikilink")。隔年，Halszka
Osmólska發表這個發現，並推測這個化石可能屬於[蜥鳥龍](../Page/蜥鳥龍.md "wikilink")\[1\]。之後她改變意見，認為個化石是個新物種。

在1987年，Halszka
Osmólska將這個化石進行敘述、命名，[模式種是](../Page/模式種.md "wikilink")**細腳無聊龍**（*Borogovia
gracilicrus*），並歸類於[傷齒龍科](../Page/傷齒龍科.md "wikilink")\[2\]。無聊龍的屬名是來自於[路易斯·卡羅在](../Page/路易斯·卡羅.md "wikilink")《[愛麗絲鏡中奇遇](../Page/愛麗絲鏡中奇遇.md "wikilink")》中創造的[詩歌](../Page/詩歌.md "wikilink")《杰伯沃基》（*Jabberwocky*），這首詩歌所提到的奇異生物「Borogoves」：種名在[拉丁文意為](../Page/拉丁文.md "wikilink")「纖細的小腿」，意指無聊龍的瘦長小腿\[3\]。

無聊龍的[正模標本](../Page/正模標本.md "wikilink")（編號ZPAL
MgD-I/174）只是兩塊部份的後肢骨頭，來自於同一個體，包含：左右[脛跗骨](../Page/脛跗骨.md "wikilink")、左右[蹠骨](../Page/蹠骨.md "wikilink")、以及第二到第四腳趾\[4\]。化石發現於[耐梅蓋特組](../Page/耐梅蓋特組.md "wikilink")（Nemegt
Formation），地質年代相當於[馬斯垂克階早期](../Page/馬斯垂克階.md "wikilink")。

脛跗骨的長度為28公分，完整身長被估計約有2公尺長，體重估計約20公斤。脛跗骨非常延長，第三腳趾狹窄，第二腳趾的第二[趾骨短](../Page/趾骨.md "wikilink")，第二腳趾的趾爪短而平坦。Osmólska推論無聊龍的第二趾爪無法大幅後縮，應該還保有支撐重量的功能；而第三腳趾過於狹窄，第二腳趾有部分支撐重量的功能\[5\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [*Borogovia* at
    *Thescelosaurus*\!](https://web.archive.org/web/20090710170842/http://www.thescelosaurus.com/troodontidae.htm)
  - [*Borogovia* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/de_4/5a671b2.htm), at Dino
    Russ' Lair

## 外部連結

  - [恐龍博物館—無聊龍](https://web.archive.org/web/20070713234545/http://www.dinosaur.net.cn/museum/Borogovia.htm)

[Category:傷齒龍科](../Category/傷齒龍科.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")

1.  Osmólska, H., 1982, "*Hulsanpes perlei* n.g.n.sp. (Deinonychosauria,
    Saurisichia, Dinosauria) from the Upper Cretaceous Barun Goyot
    Formation of Mongolia", *Neues Jahrbuch für Geologie und
    Paläontologie, Monatschefte* **1982**(7): 440-448

2.
3.  Osmólska, H., 1987, "*Borogovia gracilicrus* gen. et sp. n., a new
    troodontid dinosaur from the Late Cretaceous of Mongolia", *Acta
    Palaeontologica Polonica* **32**: 133-150

4.
5.