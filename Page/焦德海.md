**焦德海**（1878年－1937年\[1\]）是[中国](../Page/中国.md "wikilink")[相声](../Page/相声.md "wikilink")[第四代演员](../Page/相声演员师承关系.md "wikilink")，[北京天桥著名的相声艺人](../Page/北京天桥.md "wikilink")[相声八德之一](../Page/相声八德.md "wikilink")。

生于[北京](../Page/北京.md "wikilink")。曾学竹板书，后改学相声，拜[徐长福为师](../Page/徐长福.md "wikilink")。最初在北京天桥撂地说相声，后来到青云阁茶社等处献艺，还经常应邀参加堂会演出。他以[说](../Page/说.md "wikilink")、[学见长](../Page/学.md "wikilink")，口齿清晰，语言幽默，台風稳健，表情细腻。

## 著名作品

  - [珍珠翡翠白玉汤](../Page/珍珠翡翠白玉汤.md "wikilink")
  - [山东斗法](../Page/山东斗法.md "wikilink")
  - 《[五人义](../Page/五人义.md "wikilink")》
  - 《[假行家](../Page/假行家.md "wikilink")》
  - 《[吃饺子](../Page/吃饺子.md "wikilink")》
  - 《[财迷回家](../Page/财迷回家.md "wikilink")》
  - 《[坟头子](../Page/坟头子.md "wikilink")》
  - 《[开粥厂](../Page/开粥厂.md "wikilink")》
  - 《[洋药方](../Page/洋药方.md "wikilink")》
  - 《[对对子](../Page/对对子.md "wikilink")》
  - 《[打白朗](../Page/打白朗.md "wikilink")》
  - 《[老老年](../Page/老老年.md "wikilink")》

## 參考資料

`|width=30% align=center|`**`从师`**
`|width=40% align=center|`**`辈分`**
`|width=30% align=center|`**`收徒`**
`|-`

|width=30% align=center|[徐长福](../Page/徐长福.md "wikilink")

`|width=40% align=center|`**`德`**
` |width=30% align=center |`[`张寿臣`](../Page/张寿臣.md "wikilink")`、`[`李寿增`](../Page/李寿增.md "wikilink")`、`[`富寿严`](../Page/富寿严.md "wikilink")`、`[`李寿清`](../Page/李寿清.md "wikilink")`、`[`叶寿亭`](../Page/叶寿亭.md "wikilink")`、`[`于俊波`](../Page/于俊波.md "wikilink")`、`[`常连安`](../Page/常连安.md "wikilink")`、`[`朱阔泉`](../Page/朱阔泉.md "wikilink")`、`[`汤金澄`](../Page/汤金澄.md "wikilink")`、`[`于堃江`](../Page/于堃江.md "wikilink")`、`[`尹凤岐`](../Page/尹凤岐.md "wikilink")`、`[`彦授辰`](../Page/彦授辰.md "wikilink")`、`[`路彩祥`](../Page/路彩祥.md "wikilink")`、`[`白葆亭`](../Page/白葆亭.md "wikilink")

|-

[4](../Category/相声演员.md "wikilink")

1.  <http://www.godpp.gov.cn/wmzh/2008-01/28/content_12338229.htm>