[Carducci.jpg](https://zh.wikipedia.org/wiki/File:Carducci.jpg "fig:Carducci.jpg")
**焦苏埃·卡尔杜奇**（[意大利语](../Page/意大利语.md "wikilink")：****，），[意大利诗人](../Page/意大利.md "wikilink")、教师，1906年获得[诺贝尔文学奖](../Page/诺贝尔文学奖.md "wikilink")，是首个获得该奖项的意大利人，被称为19世纪意大利诗歌的顶峰。\[1\]

## 生平

焦苏埃·卡尔杜奇在[意大利](../Page/意大利.md "wikilink")[托斯卡纳西北角近](../Page/托斯卡纳.md "wikilink")[比萨的一个小镇](../Page/比萨.md "wikilink")[皮耶特拉桑塔出生](../Page/皮耶特拉桑塔.md "wikilink")，父亲是[意大利统一的支持者](../Page/意大利统一.md "wikilink")，因为父亲的政治取向，卡尔杜奇童年时曾被迫数度搬家，最终在[佛罗伦萨安顿下来](../Page/佛罗伦萨.md "wikilink")，持续数年。

卡尔杜奇自童年起开始写作诗歌，受[古希腊](../Page/古希腊.md "wikilink")、[古罗马以至意大利诗人如](../Page/古罗马.md "wikilink")[但丁等的风格影响](../Page/但丁.md "wikilink")，\[2\]诗风典雅，常以[贺拉斯和](../Page/贺拉斯.md "wikilink")[维吉尔等拉丁诗人的格律写作](../Page/维吉尔.md "wikilink")。他曾翻译[荷马史诗](../Page/荷马.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》第九卷至意大利语。1856年，卡尔杜奇在[比萨高等师范学校](../Page/比萨高等师范学校.md "wikilink")（）得到博士学位，开始教学。翌年，他出版了第一部诗集。

1859年，卡尔杜奇与埃尔维拉·梅尼库奇（）结婚，育有四名子女。卡尔杜奇曾在[皮斯托亚短暂教授](../Page/皮斯托亚.md "wikilink")[希腊语](../Page/希腊语.md "wikilink")，随后被派往[博洛尼亚大学教授意大利语](../Page/博洛尼亚大学.md "wikilink")。他是一个受欢迎的讲师，也是激烈的文学和社会批评家。他的政治观点往往与[基督教教义相悖](../Page/基督教.md "wikilink")。卡尔杜奇晚年曾说：“我不会相信上帝的真实性，也不会与梵帝冈或任何神职人员和睦共处，因为他们就是意大利最坚定不移的敌人。”\[3\]卡尔杜奇反宗教的取向在他的诗作《[撒旦颂](../Page/撒旦颂.md "wikilink")》（）中表露无遗。该诗作在1863年写成，1865年出版，1869年被[博洛尼亚一份激进报章再版](../Page/博洛尼亚.md "wikilink")，作为对[梵帝冈的挑衅](../Page/梵帝冈.md "wikilink")。\[4\]

此外，卡尔杜奇亦著有《》和《[野蛮颂歌](../Page/野蛮颂歌.md "wikilink")》（）等诗集。1906年，他成为历来第一位获得[诺贝尔文学奖的意大利人](../Page/诺贝尔文学奖.md "wikilink")。他也获选为意大利参议员。\[5\]虽然卡尔杜奇主要以他的诗作闻名，但他亦创作了许多散文作品，例如文学批评、传记、演讲辞等20余册，\[6\]包括《意大利民族文学史》。\[7\]

卡尔杜奇1907年在博洛尼亚逝世，享年71岁。

## 作品的中譯

  - 陳映真/主編，李魁賢/譯，《撒旦頌》，台北市：遠景，1981年再版。
  - 諾貝爾文學獎全集編譯委員會/編譯，《卡杜其(1906)吉卜齡(1907)》，台北市：九華出版，1981年。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [诺贝尔奖网站对卡尔杜奇的介绍](http://nobelprize.org/literature/laureates/1906/carducci-bio.html)

  - [焦苏埃·卡尔杜奇的诗作](http://www.0web.it/poesia/giosue-carducci)

[C](../Category/诺贝尔文学奖获得者.md "wikilink")
[C](../Category/義大利諾貝爾獎獲得者.md "wikilink")
[C](../Category/意大利诗人.md "wikilink")
[C](../Category/比薩大學校友.md "wikilink")

1.

2.

3.  Carelle, A., *Naturalismo Italiano*, Draghi, Padova 1911, cited at
    <http://www.infidels.org/library/historical/joseph_mccabe/dictionary.html>

4.  *Carducci, Giosuè, Selected Verse/ Giosuè Carducci: edited with a
    translation, introduction and commentary by David H. Higgins*, (Aris
    & Phillips; Warminster, England), 1994. See also: Bailey, John Cann,
    "Carducci - The Taylorian Lecture," (Clarendon Press, Oxford) 1926.

5.

6.

7.