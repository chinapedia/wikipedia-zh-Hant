[Bidboland_gas_refinery.jpg](https://zh.wikipedia.org/wiki/File:Bidboland_gas_refinery.jpg "fig:Bidboland_gas_refinery.jpg")
[RefineryFlow.png](https://zh.wikipedia.org/wiki/File:RefineryFlow.png "fig:RefineryFlow.png")
[Crude_Oil_Distillation.png](https://zh.wikipedia.org/wiki/File:Crude_Oil_Distillation.png "fig:Crude_Oil_Distillation.png")
**煉油廠**是一個處理提煉[石油的](../Page/石油.md "wikilink")[工廠](../Page/工廠.md "wikilink")，將[原油](../Page/原油.md "wikilink")[精煉過後分為許多各有用途的石油產物](../Page/精煉.md "wikilink")，例如[汽油](../Page/汽油.md "wikilink")、[柴油等](../Page/柴油.md "wikilink")[燃料和化工产品](../Page/燃料.md "wikilink")。煉油廠中根据不同的油品有不同的装置进行加工，其中有[蒸餾装置](../Page/蒸餾.md "wikilink")，催化装置，重整装置，加氢裂化装置等。蒸餾装置中，[蒸餾塔是装置中重要的一种塔](../Page/蒸餾塔.md "wikilink")，形式可以是板式塔與填料式塔，主要作用是将经过[预热并](../Page/预热.md "wikilink")[脱盐的原油](../Page/脱盐.md "wikilink")，利用[沸點的差異以及蒸餾塔底部較高溫的特點將物質分離](../Page/沸點.md "wikilink")。

## 過程

[石油通過管道或](../Page/石油.md "wikilink")[油輪運到煉油廠](../Page/油輪.md "wikilink")。在那裡，用[分餾法將其分離成具有相近](../Page/分餾.md "wikilink")[沸點的產品之混合物](../Page/沸點.md "wikilink")。這些混合物被進一步處理後，生產出一系列[燃料和大量的](../Page/燃料.md "wikilink")[化學工業原材料](../Page/化學工業.md "wikilink")。

## 主要石油產品

石油產品通常被分為三類：輕餾分油（液化石油氣、汽油、石腦油），中間餾分油（煤油、柴油），重餾分油與渣油（重燃料油、潤滑油、蠟、瀝青）。這種分類是根據蒸餾原油的製程。

  - [液化石油氣](../Page/液化石油氣.md "wikilink")
    (液化石油氣的成分大抵為丙烷與丁烷，藉由壓力液化之後加以儲存與運送)
  - [汽油](../Page/汽油.md "wikilink")
  - [石腦油](../Page/石腦油.md "wikilink")
  - [煤油與](../Page/煤油.md "wikilink")[航空燃油](../Page/航空燃油.md "wikilink")
  - [柴油](../Page/柴油.md "wikilink")
  - [燃油](../Page/燃油.md "wikilink")
  - [潤滑油](../Page/潤滑油.md "wikilink")
  - [石蠟](../Page/石蠟.md "wikilink")
  - [沥青與](../Page/沥青.md "wikilink")[焦油](../Page/焦油.md "wikilink")
  - [石油焦](../Page/石油焦.md "wikilink")
  - [硫磺](../Page/硫磺.md "wikilink")

煉油廠也生產許多中層產品，如氫、輕質碳氫化合物、重整油、高溫[裂解油](../Page/裂解.md "wikilink")。這些產品通常不經長途運輸，而是直接當場進一步處理，因此化學廠時常設在煉油廠旁。例如：輕質碳氫化合物在[乙烯廠蒸汽裂解](../Page/乙烯.md "wikilink")，將產出的乙烯聚化產生[聚乙烯](../Page/聚乙烯.md "wikilink")。

## 分类

炼油厂按主要炼油产品可分为：

  - 燃料油型炼厂
  - 燃料润滑油型炼厂
  - 燃料化工型炼厂
  - 综合型炼厂。

## 工业生产

2012年，中国原油一次加工能力达到5.75亿吨；新增炼油能力3500万吨，比上年增加6.5%。中国全年累计加工原油4.67亿吨，比上年增长3.5%，增速较上年下降4.9个百分点；全年累计生产成品油2.81亿吨，比上年增长5.2%，增速与上年相比下降0.3个百分点。

## 外部連結

  - [Searchable United States Refinery
    Map](http://www.energysupplylogistics.com/refineries)
  - [Interactive map of UK
    refineries](https://web.archive.org/web/20071212173402/http://www.energyinst.org.uk/education/refineries/map.htm)
  - [Complete, detailed refinery
    description](http://www.osha.gov/dts/osta/otm/otm_iv/otm_iv_2.html)
  - [Petroleum Refinery Planning and Optimization Using Linear
    Programming](http://www.cheresources.com/refinery_planning_optimization.shtml)
  - [Student's Guide to
    Refining](http://www.cheresources.com/refining.shtml)
  - [Global refinery shortage shifts power
    balance](http://news.bbc.co.uk/2/hi/business/4296812.stm)
  - [Ecomuseum
    Bergslagen](https://web.archive.org/web/20060815000718/http://www.ekomuseum.se/english/besoksmal/oljeon.html)
    - history of Oljeön, [Sweden](../Page/Sweden.md "wikilink")
  - [Detailed refinery
    description](https://web.archive.org/web/20070626023751/http://www.setlaboratories.com/overview.htm#Basics%20of%20Crude%20Oil)
  - [Fueling Profits: Report on Industry
    Consolidation](http://www.consumerfed.org/pdfs/oilprofits.pdf)
    (publication of the Consumer Federation of America)
  - [Price Spikes, Excess Profits and
    Excuses](http://www.consumerfed.org/pdfs/gasoline1003.pdf)
    (publication of the Consumer Federation of America)
  - [Refinery
    study](http://www.barnesandclick.com/PROSiE/OKRefReport.pdf)
  - [Basics of Oil
    Refining](http://www.petrostrategies.org/Learning_Center/refining.htm)
    Overview of crude oil refining process
  - [Basics of Oil
    Refining](http://www.petro-canada.ca/pdfs/investors/Refing101-EdmMiniIRDay-TD_Aug23-ir-e-f.pdf)
    Overview of crude oil refining process with focus on Canadian crude
    oil
  - [Refinery Animations &
    Videos](http://www.nzrc.co.nz/visitors--learning/classroom--learning-resources/learning-centre.aspx)
    Oil Refinery Process Animations,Videos & 360 Degree Views

[炼油厂](../Category/炼油厂.md "wikilink")