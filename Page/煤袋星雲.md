**煤袋星雲**（或簡稱為**煤袋**）是天空中非常著名的[暗星雲](../Page/暗星雲.md "wikilink")，在南半球的[銀河襯托下](../Page/銀河.md "wikilink")，很容易用裸眼看見的一片深色斑塊。這個斑塊位於[南十字座](../Page/南十字座.md "wikilink")，最接近[地球的部分](../Page/地球.md "wikilink")，距離大約是600[光年](../Page/光年.md "wikilink")。

## 一般資訊

煤袋星雲覆蓋的天空大約7[°長](../Page/度.md "wikilink")5°寬，並且涵蓋到[半人馬座和](../Page/半人馬座.md "wikilink")[蒼蠅座](../Page/蒼蠅座.md "wikilink")\[1\]。西班牙航海家[平松在](../Page/文森特·亞涅斯·平松.md "wikilink")1499年最早確認它的存在，並稱它為“*il
Canopo
fosco*”（[亞美利哥·維斯普奇稱它為黑暗的](../Page/亞美利哥·維斯普奇.md "wikilink")[老人星](../Page/老人星.md "wikilink")），也稱為“*Macula
Magellani*”（麥哲倫的斑點）或“*黑麥哲倫雲*”以與[麥哲倫雲相對應](../Page/麥哲倫雲.md "wikilink")。

在1970年，[K.
Mattila證明煤炭袋並非全黑](../Page/K._Mattila.md "wikilink")，它只是非常的暗淡（亮度僅有相鄰銀河的一成），光亮則是來自被它遮蔽的星光反射。

煤炭袋並未登錄在[NGC天體表內](../Page/NGC天體表.md "wikilink")，事實上也沒有可供辨識的編號（除了在[科德韋爾深空天體表中編號為](../Page/科德韋爾深空天體表.md "wikilink")**C099**）。它在[拉里·尼薇和](../Page/拉里·尼薇.md "wikilink")[傑瑞·普內爾的](../Page/傑瑞·普內爾.md "wikilink")[科幻小說中被突顯為](../Page/科幻.md "wikilink")*上帝眼中的塵粒*。

[Emu_public.jpg](https://zh.wikipedia.org/wiki/File:Emu_public.jpg "fig:Emu_public.jpg")，左邊是[天蝎座](../Page/天蝎座.md "wikilink")。\]\]

在中，煤袋星雲是很重要的。在幾個中，它形成*澳大利亞原住民天文學*。原住民的認為它是監看人民的*執法官*的頭與肩，觀看著人民確保不會違反傳統的法律。依據哈尼（W.E.
Harney）的報導，這是被稱為*Utdjungon*和唯一僅存的部落法律，可以讓遵守的成員阻止世界於被火熱的恆星毀滅\[2\]。還有Gaiarbau
(1880)認為在地球上的波拉環是參考煤袋星雲的複製品。由於波拉場地在羅盤上是位於北/南，在南天的煤袋被認為是啟動/儀式環。這些天文遺址允許神靈與地球上的人類夥伴繼續進行儀式。

在[印加天文學](../Page/印加.md "wikilink")，這個星雲被稱為*Yutu*，意思是*鷓鴣，像南方的鳥*\[3\]或（Tinamou，南美產的一種鳥）\[4\]。

## 在小說

[An_Emu_in_the_Sky_over_Paranal.jpg](https://zh.wikipedia.org/wiki/File:An_Emu_in_the_Sky_over_Paranal.jpg "fig:An_Emu_in_the_Sky_over_Paranal.jpg")的汙斑\[5\]。\]\]
煤袋星雲在[星際爭霸戰的劇情中被設定為](../Page/星際爭霸戰.md "wikilink")[免疫症侯群](../Page/免疫症侯群_\(星際爭霸戰\).md "wikilink")，以及在亞瑟·克拉克主演的[2001太空奧德賽中是](../Page/2001太空奧德賽.md "wikilink")[讓它成為你最後的戰場](../Page/讓它成為你最後的戰場.md "wikilink")。

在[拉瑞·尼文和](../Page/拉瑞·尼文.md "wikilink")[Jerry
Pournelle的](../Page/Jerry_Pournelle.md "wikilink")[科學幻想小說](../Page/科學幻想.md "wikilink")*[上帝眼中的塵埃和它的續集](../Page/上帝眼中的塵埃.md "wikilink")，**和*[穹頂](../Page/穹頂_\(小說\).md "wikilink")''中，煤袋星雲的圖片都很顯著\[6\]。

此外，[亨利·德·維爾·斯塔克普爾在他的小說](../Page/亨利·德·維爾·斯塔克普爾.md "wikilink")[藍色珊瑚礁](../Page/藍色珊瑚礁_\(小說\).md "wikilink")（1908年）中描述煤袋星雲，就如萊斯特蘭奇在*諾森伯蘭*的甲板上觀察的：*在銀河系中，靠近南十字座附近，發生可怕的圓形深淵，大煤袋。如此嚴酷的定義它，暗示它是無底和無止盡的深淵，對著它沉思是折磨富於想像力的頭腦。用肉眼看到它是黑暗和死亡的悲慘世界，但即使是最小的望遠鏡也能揭示它有著眾多和美麗的星辰。萊斯特蘭奇的眼神從這個神奇處遊走至燃燒的十字，並且……*\[7\]。

煤袋星雲出現在[飛出個未來系列的](../Page/飛出個未來.md "wikilink")*地獄是另一個機器人*的劇情中\[8\]。

在*[銀河鐵道之夜](../Page/銀河鐵道之夜.md "wikilink")*，康帕內拉在結束時去到了煤袋星雲。

在影集，*[超世紀戰警](../Page/超世紀戰警.md "wikilink")*，預言者伊瑪說服戰警協助他的人民反抗Necromongers時說："煤袋行星一去不回了"\[9\]。

## 圖集

<File:Coalsack> and Dark Doodad Dark Nebulae.jpg|煤袋星雲是在照片頂部附近，看似黑暗的大片區域。

## 參考資料

## 外部連結

  - [Starry Night Photography: Coalsack Dark
    Nebula](http://www.starrynightphotos.com/southern_sky/coal_sack.htm)
  - [Starry Night Photography: The
    Emu](http://www.starrynightphotos.com/southern_sky/emu.htm)
  - [SIMBAD: Coal Sack
    Nebula](http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=NAME+COAL+SACK&submit=SIMBAD+search)

[Category:南十字座](../Category/南十字座.md "wikilink")
[Category:暗星雲](../Category/暗星雲.md "wikilink")
[099b](../Category/科德韦尔天体.md "wikilink")

1.
2.  Songs of the Songmen, 28-30.
3.  p. 5, [*The Hundred Greatest
    Stars*](https://books.google.com/books?id=Aq24O7B4ehoC), James B.
    Kaler, New York, Copernicus Books, 2002.
4.
5.
6.  <http://www.amazon.com/Outies-Mote-Gods-Eye-3/dp/0615434142/ref=sr_1_cc_1?s=movies-tv&ie=UTF8&qid=1318736760&sr=1-1-catcorr>
7.  [H. De Vere
    Stacpoole](../Page/Henry_De_Vere_Stacpoole.md "wikilink"): ‘[The
    Blue Lagoon](../Page/The_Blue_Lagoon_\(novel\).md "wikilink")’,
    London: Adelphi Terrace, 1908, T. Fisher Unwin Ltd., quote taken
    from 28th Impression (1923), p. 12, from pdf p. 28 at
    <https://archive.org/details/bluelagoonromanc00stacrich>
8.  <http://theinfosphere.org/Hell_Is_Other_Robots>
9.  <http://www.script-o-rama.com/movie_scripts/c/chronicles-of-riddick-script-transcript.html>