**熊庆来**（），[字迪之](../Page/表字.md "wikilink")，[中国](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")[弥勒人](../Page/弥勒县.md "wikilink")。数学家，中国近代[数学的先驱](../Page/数学.md "wikilink")，并被誉为“中国数学界的[伯乐](../Page/伯乐.md "wikilink")”。

## 生平

1907年考入[云南方言学堂](../Page/云南方言学堂.md "wikilink")（同年学校改名[云南高等学堂](../Page/云南高等学堂.md "wikilink")）。1913年赴[欧洲留学](../Page/欧洲.md "wikilink")，曾就读于[巴黎大学](../Page/巴黎大学.md "wikilink")、[蒙彼利埃大学](../Page/蒙彼利埃大学.md "wikilink")、[马赛大学等校](../Page/马赛大学.md "wikilink")，取得高等普通数学、高等数学分析、[力学](../Page/力学.md "wikilink")、[天文学](../Page/天文学.md "wikilink")、普通[物理学证书](../Page/物理学.md "wikilink")，并获[蒙彼利埃大学理科硕士学位](../Page/蒙彼利埃大学.md "wikilink")。

1921年初，离欧返回[昆明](../Page/昆明.md "wikilink")，任[云南甲种工业学校及](../Page/云南甲种工业学校.md "wikilink")[云南路政学校教员](../Page/云南路政学校.md "wikilink")。

1921年秋，[国立东南大学](../Page/国立东南大学.md "wikilink")（后更名[国立中央大学](../Page/国立中央大学.md "wikilink")、[南京大学](../Page/南京大学.md "wikilink")）成立[算学系](../Page/南京大学数学系.md "wikilink")，熊受[郭秉文之邀聘担任教授兼系主任](../Page/郭秉文.md "wikilink")。这是中国近代史上第一个高等数学系，以往中国大学数学系的水平相当于当时欧美国家中学数学的程度，在此五年间自编讲义，计有《[平面三角](../Page/平面三角.md "wikilink")》、《[球面三角](../Page/球面三角.md "wikilink")》、《方程式论》、《[微积分](../Page/微积分.md "wikilink")》、《解析函数》、《[微分几何](../Page/微分几何.md "wikilink")》、《力学》、《微分方程》、《偏微分方程》、《高等算学分析》等十余种大学教材，这是第一次用中文写成的近代数学教科书。期间和[何鲁](../Page/何鲁.md "wikilink")、[胡刚复资助学生](../Page/胡刚复.md "wikilink")[严济慈到](../Page/严济慈.md "wikilink")[巴黎攻读博士](../Page/巴黎.md "wikilink")，由于严济慈在数学、物理等方面的杰出表现，法国从此开始承认中国大学的文凭。

1926年，转到[清华学校](../Page/清华学校.md "wikilink")（1928年改名[清华大学](../Page/清华大学.md "wikilink")）任教，担任算学系教授兼主任。1929年主持成立清华大学算学研究部，这是中国近代史上第一个近代数学研究机构。次年招收第一届硕士研究生，[陈省身便在这一年入读](../Page/陈省身.md "wikilink")。1931年推荐中学数学教员[华罗庚担任助理员](../Page/华罗庚.md "wikilink")，对华罗庚一生影响甚远。

1932年，代表中国赴[瑞士](../Page/瑞士.md "wikilink")[苏黎世参加](../Page/苏黎世.md "wikilink")[国际数学家大会](../Page/国际数学家大会.md "wikilink")，这是中国数学家第一次出席国际数学家大会。会后在巴黎从事研究工作，1934年以《关于无穷级整函数与亚纯函数》论文获法国国家科学博士学位。该[函数论方面的研究定义了一个](../Page/函数论.md "wikilink")“[无穷级函数](../Page/无穷级函数.md "wikilink")”，国际上称为[熊氏无穷数](../Page/熊氏无穷数.md "wikilink")。

1934年，从巴黎返回国内后继续在清华大学担任数学教授兼系主任。

1937年，应云南省主席[龙云之邀](../Page/龙云.md "wikilink")，担任[云南大学校长至](../Page/云南大学.md "wikilink")1949年。其间该校在极其艰难的条件下惨淡经营获得重大发展，成为中国著名大学之一。1945年[国民党六大召开前夕](../Page/国民党.md "wikilink")，列入[朱家骅与](../Page/朱家骅.md "wikilink")[陈立夫联名向](../Page/陈立夫.md "wikilink")[蒋介石推荐的](../Page/蒋介石.md "wikilink")98名“最优秀教授党员”之一。\[1\]1947年他在原籍[雲南省](../Page/雲南省.md "wikilink")[彌勒縣當選為](../Page/彌勒縣.md "wikilink")[第一屆國民大會代表](../Page/第一屆國民大會代表.md "wikilink")。

1949年6月，前往法国。同年9月，熊庆来随[梅贻琦团长赴巴黎出席](../Page/梅贻琦.md "wikilink")[联合国教科文组织第四次大会](../Page/联合国教科文组织.md "wikilink")，会议结束后暂留巴黎作研究工作。1951年其患[中风](../Page/中风.md "wikilink")，右手瘫痪，遂练习用左手写字，并继续在法国从事数学研究工作至1957年。

1957年回国，在[中国科学院数学研究所进行研究](../Page/中国科学院.md "wikilink")，[杨乐](../Page/杨乐.md "wikilink")、[张广厚即是这一时期的学生](../Page/张广厚.md "wikilink")。

1969年2月3日深夜，在[文革期间被迫害](../Page/文革.md "wikilink")，在北京逝世。\[2\]

## 纪念

### 熊庆来故居

云南大学至公堂东侧保存有熊庆来故居。始建于1937年，占地面积308平方米，两层小楼，一楼接待来访宾客，二楼为卧室和办公室。目前开放公众参观。

### 庆来中学

  - 云南省弥勒县有以他名字命名的“庆来中学”。庆来中学是一所由红河卷烟厂发起并控股，云南烟草红河州公司和红河雄风印业有限责任公司共同出资创办的一所股份制、经营性民办学校。

## 参考来源

## 外部链接

  - [清华大学数学系的创建人——熊庆来
    （1893—1969）](http://www.tsinghua.edu.cn/publish/thunews/9817/2011/20110225231836125570420/20110225231836125570420_.html)

[Category:中国数学家](../Category/中国数学家.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:中国教育家](../Category/中国教育家.md "wikilink")
[Category:國立中央大學教席學者](../Category/國立中央大學教席學者.md "wikilink")
[Category:南京大學教席學者](../Category/南京大學教席學者.md "wikilink")
[Q庆](../Category/熊姓.md "wikilink")
[Category:弥勒人](../Category/弥勒人.md "wikilink")
[Category:文革受难者](../Category/文革受难者.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:中国国民党党员](../Category/中国国民党党员.md "wikilink")
[Category:云南大学校长](../Category/云南大学校长.md "wikilink")

1.  [45年国民党“最优秀教授党员”：华罗庚陈寅恪冯友兰](http://news.ifeng.com/history/zhongguojindaishi/detail_2013_04/16/24258575_0.shtml)
2.