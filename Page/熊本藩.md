**熊本藩**（）是日本[江戶時代的一個藩](../Page/江戶時代.md "wikilink")。藩廳位於[熊本城](../Page/熊本城.md "wikilink")（[熊本市](../Page/熊本市.md "wikilink")），領地包括[肥後國](../Page/肥後國.md "wikilink")（[熊本县](../Page/熊本县.md "wikilink")）除去[球磨郡](../Page/球磨郡.md "wikilink")、[天草郡的地区以及](../Page/天草郡.md "wikilink")[豐後國](../Page/豐後國.md "wikilink")（[大分县](../Page/大分县.md "wikilink")）的一部分（[鶴崎](../Page/鶴崎市.md "wikilink")、[佐賀関等](../Page/佐賀関.md "wikilink")）。也被称作**肥后藩**。藩主是[加藤氏及](../Page/加藤氏.md "wikilink")[細川氏](../Page/細川氏.md "wikilink")。自加藤氏失勢後，由細川氏管理到[明治時代](../Page/明治時代.md "wikilink")。

## 歷史

[Kumamotojo.jpg](https://zh.wikipedia.org/wiki/File:Kumamotojo.jpg "fig:Kumamotojo.jpg")，由初代藩主[加藤清正主持興建](../Page/加藤清正.md "wikilink")，圖片為熊本城的大小[天守](../Page/天守.md "wikilink")（重建）\]\]
在江戶時代以前，熊本藩的範圍大概包括加藤清正及[小西行長管治的領地](../Page/小西行長.md "wikilink")。後者在[關原之戰參加西軍](../Page/關原之戰.md "wikilink")，因戰敗後被德川軍處死，其領土分配給加藤清正，成為52萬石大名，清正時代建造[熊本城](../Page/熊本城.md "wikilink")，在清正管治期間，風評良好，後世稱他為清正公。1632年，第二代藩主[忠廣因](../Page/加藤忠廣.md "wikilink")[德川忠長事件而被處分](../Page/德川忠長.md "wikilink")，分配到[出羽國](../Page/出羽國.md "wikilink")[羽岡藩](../Page/羽岡藩.md "wikilink")，並由新領主細川氏管治，直到廢藩置縣為止，由細川氏所管治。

在管治初期，召回不少加藤氏家臣及肥後國人。第二代藩主[光尚早年死亡](../Page/細川光尚.md "wikilink")。第三代藩主[綱利](../Page/細川綱利.md "wikilink")7歲就任。1747年第五代藩主[忠孝在](../Page/細川忠孝.md "wikilink")[江戶被](../Page/江戶.md "wikilink")7000石旗本[板倉勝該斬死](../Page/板倉勝該.md "wikilink")，自此將家紋改變。

1755年開設[藩校時習館](../Page/藩校時習館.md "wikilink")，[重賢進行改革](../Page/細川重賢.md "wikilink")，成功將熊本藩中興。幕末時代藩內分為勤王黨、時習館黨、實學黨等。

1870年末代藩主[護久](../Page/細川護久.md "wikilink")，支持實學黨，控制了藩政。1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，原有領地成為了[熊本縣的行政區劃](../Page/熊本縣.md "wikilink")。

細川護久直系後代，第18代細川家當主[細川護熙是第](../Page/細川護熙.md "wikilink")79任日本首相。

## 藩主

### 加藤家

[Kato_Kiyomasa.jpg](https://zh.wikipedia.org/wiki/File:Kato_Kiyomasa.jpg "fig:Kato_Kiyomasa.jpg")\]\]

外樣 52萬石

1.  [清正](../Page/加藤清正.md "wikilink")（1587年-1611年）
2.  [忠廣](../Page/加藤忠廣.md "wikilink")（1611年-1632年）

### 細川家

外樣 54萬石

1.  [忠利](../Page/細川忠利.md "wikilink")（1633年-1641年）
2.  [光尚](../Page/細川光尚.md "wikilink")（1641年-1649年）
3.  [綱利](../Page/細川綱利.md "wikilink")（1649年-1712年）
4.  [宣紀](../Page/細川宣紀.md "wikilink")（1712年-1732年）
5.  [宗孝](../Page/細川宗孝.md "wikilink")（1732年-1747年）
6.  [重賢](../Page/細川重賢.md "wikilink")（1747年-1785年）
7.  [治年](../Page/細川治年.md "wikilink")（1785年-1787年）
8.  [齊茲](../Page/細川諂茲.md "wikilink")（1787年-1810年）
9.  [齊樹](../Page/細川齊樹.md "wikilink")（1810年-1826年）
10. [齊護](../Page/細川齊護.md "wikilink")（1826年-1860年）
11. [韶邦](../Page/細川韶邦.md "wikilink")（1860年-1870年）
12. [護久](../Page/細川護久.md "wikilink")（1870年-1871年）

## 支藩

熊本藩有兩個支藩，分別是宇土藩及熊本新田藩（後改稱高瀨藩）。

### 宇土藩

1546年設立。藩廳位於宇土，藩廳位於[宇土城的遺址](../Page/宇土城.md "wikilink")，細川光松的從弟[行孝被分為](../Page/細川行孝.md "wikilink")3萬石，六代藩主立禮及立政曾繼承熊本藩成為藩主。1870年與熊本藩合併而廢藩。

細川氏、外樣3萬石

1.  [行孝](../Page/細川行孝.md "wikilink")（1646年-1690年）
2.  [有孝](../Page/細川有孝.md "wikilink")（1690年-1703年）
3.  [興生](../Page/細川興生.md "wikilink")（1703年-1735年）
4.  [興里](../Page/細川興里.md "wikilink")（1735年-1745年）
5.  [興文](../Page/細川興文.md "wikilink")（1745年-1772年）
6.  [立禮](../Page/細川齊茲.md "wikilink")（1772年-1787年）後繼承熊本藩改名為齊茲
7.  [立之](../Page/細川立之.md "wikilink")（1787年-1818年）
8.  [立政](../Page/細川齊護.md "wikilink")（1818年-1826年）後繼承熊本藩改名為齊護
9.  [行芬](../Page/細川行芬.md "wikilink")（1826年-1851年）
10. [立則](../Page/細川立則.md "wikilink")（1851年-1862年）
11. [行真](../Page/細川行真.md "wikilink")（1862年-1870年）

### 肥後新田藩/高瀨藩

1666年第三代藩主[細川綱利的弟弟](../Page/細川綱利.md "wikilink")[利重被封於熊本藩藏米](../Page/細川利重.md "wikilink")3萬5千石。1870年與熊本藩合併而被廢藩。

細川家、外樣3萬5千石

1.  [利重](../Page/細川利重.md "wikilink")（1666年-1687年）
2.  [利昌](../Page/細川利昌.md "wikilink")（1687年-1715年）
3.  [利恭](../Page/細川利恭.md "wikilink")（1715年-1742年）
4.  [利寛](../Page/細川利寛.md "wikilink")（1742年-1767年）
5.  [利致](../Page/細川利致.md "wikilink")（1767年-1781年）
6.  [利庸](../Page/細川利庸.md "wikilink")（1781年-1805年）
7.  [利國](../Page/細川利國.md "wikilink")（1805年-1810年）
8.  [利愛](../Page/細川利愛.md "wikilink")（1810年-1833年）
9.  [利用](../Page/細川利用.md "wikilink")（1833年-1856年）
10. [利永](../Page/細川利永.md "wikilink")（1856年-1870年）

## 主要家臣

## 參考資料

  - [熊本藩](http://www.asahi-net.or.jp/~me4k-skri/han/kyushu/kumamoto.html)

[Category:熊本藩](../Category/熊本藩.md "wikilink")
[Category:肥後細川氏](../Category/肥後細川氏.md "wikilink")
[Category:尾張加藤氏](../Category/尾張加藤氏.md "wikilink")