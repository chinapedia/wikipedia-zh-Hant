**熱河龍屬**（[屬名](../Page/屬.md "wikilink")：）是[鳥腳下目](../Page/鳥腳下目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於[白堊紀早期的](../Page/白堊紀.md "wikilink")[中國](../Page/中國.md "wikilink")。熱河龍是小型、原始的鳥腳類恐龍，屬於[熱河龍科](../Page/熱河龍科.md "wikilink")。

目前已經發現兩個標本，[化石發現於](../Page/化石.md "wikilink")[遼寧省](../Page/遼寧省.md "wikilink")[北票市](../Page/北票市.md "wikilink")[上園鎮的陸家屯](../Page/上園鎮.md "wikilink")，該地屬於[義縣組](../Page/義縣組.md "wikilink")，地質年代約1億2500萬年前，相當於[阿普第階早期](../Page/阿普第階.md "wikilink")。這些化石位於[凝灰岩層](../Page/凝灰岩.md "wikilink")，被推測遭到火山爆發的火山灰所掩埋，火山灰覆蓋半徑50到60公里的範圍。[模式種是](../Page/模式種.md "wikilink")**上園熱河龍**（*J.
shangyuanensis*），在2000年由[徐星](../Page/徐星.md "wikilink")、[汪筱林](../Page/汪筱林.md "wikilink")、[尤海魯等人根據兩個部份骨骼來敘述](../Page/尤海魯.md "wikilink")、命名。屬名意為「熱河蜥蜴」，以中國的前行政區劃[熱河省為名](../Page/熱河省.md "wikilink")，化石發現處過去曾經被劃分於熱河省。種名則是以化石發現處所在的上園鎮為名\[1\]。

## 特徵

[Jeholosaurus_shangyuanensis.png](https://zh.wikipedia.org/wiki/File:Jeholosaurus_shangyuanensis.png "fig:Jeholosaurus_shangyuanensis.png")
熱河目前已經發現兩個標本，都屬於幼年個體，其中一個可能已達亞成年個體\[2\]。[正模標本](../Page/正模標本.md "wikilink")（編號
IVPP V
12529）曾遭到擠壓，體型較大，包含：一個不完整顱骨、下頜、[頸椎](../Page/頸椎.md "wikilink")、[尾椎](../Page/尾椎.md "wikilink")、[股骨](../Page/股骨.md "wikilink")、[脛骨](../Page/脛骨.md "wikilink")、[腓骨](../Page/腓骨.md "wikilink")、第一到第四[蹠骨](../Page/蹠骨.md "wikilink")、以及趾骨。第二個標本（編號
IVPP V 12530）是一個不完整顱骨與頸椎，保存狀態較好，體型可能較小。

科學家根據這些骨頭，推測正模標本在生前的身長為71.1公分，其中尾巴佔了35.6公分，不完整的顱骨長6.3公分，下頜則為5.9公分，前肢長25.4公分，後肢長33公分，[股骨長](../Page/股骨.md "wikilink")9公分，[脛骨長](../Page/脛骨.md "wikilink")10.7公分\[3\]。

這兩個頭顱骨相當不完整。這些標本的眼眶/顱骨比例相當大，眼眶長度約是頭顱骨長度的40%；口鼻部短，約是頭顱骨長度的40%，顯示這些標本可能是幼年個體。[前上頜骨有](../Page/前上頜骨.md "wikilink")6顆牙齒，[上頜骨有至少](../Page/上頜骨.md "wikilink")13顆牙齒。上頜骨的頰齒略為平坦，如同其他[草食性動物](../Page/草食性.md "wikilink")；前上頜骨的牙齒長而狹窄，較類似[肉食性動物](../Page/肉食性.md "wikilink")。綜合以上兩點，有些古生物學家認為熱河龍是[雜食性動物](../Page/雜食性.md "wikilink")，同時以[植物](../Page/植物.md "wikilink")、[昆蟲](../Page/昆蟲.md "wikilink")、小型動物為食。上頜骨的下緣位於內側深處，顯示熱河龍在生前可能具有肉質的頰部，位於嘴部的兩側。[鼻骨的後背側有大型鼻骨孔](../Page/鼻骨.md "wikilink")（Nasal
foramina）。沒有發現[眼瞼骨](../Page/眼瞼骨.md "wikilink")。[方顴骨接鄰一個大型洞孔](../Page/方顴骨.md "wikilink")，直徑約有方顴骨的1/4長度。方顴骨的高度少於顱骨高度的30%。[齒骨的長度是](../Page/齒骨.md "wikilink")[前齒骨的](../Page/前齒骨.md "wikilink")1.5倍。齒骨向後延伸，後緣幾乎與[隅骨後緣平行](../Page/隅骨.md "wikilink")\[4\]。

股骨呈弓形，前粗隆部的位置略低於大粗隆部，第三粗隆部與大粗隆部同寬，第四粗隆部向外突出。股骨髁與股骨之間沒有明顯溝痕。目前已發現四個蹠骨。第一蹠骨呈條狀，位置較後。第三蹠骨最長，長約5.5公分，位置較前。雖然[頸椎](../Page/頸椎.md "wikilink")、[尾椎被保存下來](../Page/尾椎.md "wikilink")，但無法得知詳細數量\[5\]。

## 分類

熱河龍是種[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")，具有典型的鳥臀目[骨盆](../Page/骨盆.md "wikilink")，[恥骨與](../Page/恥骨.md "wikilink")[腸骨向前方平行延伸](../Page/腸骨.md "wikilink")，恥骨與[坐骨向後方平行延伸](../Page/坐骨.md "wikilink")，呈現一個四端分岔的結構\[6\]。

徐星等人命名熱河龍時，並沒有將牠們歸類於任何一科，只歸類於鳥臀目的分類未定屬。熱河龍常被歸類於[稜齒龍科](../Page/稜齒龍科.md "wikilink")（或稱稜齒龍類），這是一群原始鳥腳類恐龍組合而成的[並系群](../Page/並系群.md "wikilink")，根據以下特徵：

1.  前上頜骨相當長，往鼻骨方向有個圓錐狀骨突，[稜齒龍也有類似的特徵](../Page/稜齒龍.md "wikilink")。
2.  前上頜骨有6顆牙齒，與更原始的[賴索托龍相同](../Page/賴索托龍.md "wikilink")。但是熱河龍的前上頜齒較修長、彎曲，沒有邊緣小齒，與稜齒龍類的[西風龍](../Page/西風龍.md "wikilink")、[奇異龍相同](../Page/奇異龍.md "wikilink")。
3.  上頜有個眶前的小孔，長度是顱骨的14%，後方另有一個洞孔，類似稜齒龍類。
4.  [顴骨的後突長](../Page/顴骨.md "wikilink")，類似[鹽都龍](../Page/鹽都龍.md "wikilink")。
5.  [眶後骨](../Page/眶後骨.md "wikilink")、[額骨具有典型的原始稜齒龍類特徵](../Page/額骨.md "wikilink")。

熱河龍的[前上頜骨沒有接觸](../Page/前上頜骨.md "wikilink")[淚骨](../Page/淚骨.md "wikilink")、頜部關節位置高、前上頜骨與上頜骨位於水平位置，這些則是原始特徵。但是，熱河龍也具有進階型鳥腳類的特徵，例如股骨大粗隆部與前粗隆部的形狀。熱河龍的下頜聯合處癒合，顯示牠們可能與[頭飾龍類有關係](../Page/頭飾龍類.md "wikilink")\[7\]。

以下[演化樹取自於](../Page/演化樹.md "wikilink")[尤海魯等人在](../Page/尤海魯.md "wikilink")2009年的[畸齒龍科研究](../Page/畸齒龍科.md "wikilink")\[8\]。

以下[演化樹來自於](../Page/演化樹.md "wikilink")[彼得·馬克維奇的鳥腳類研究](../Page/彼得·馬克維奇.md "wikilink")\[9\]：

在2012年，長春龍被歸類於新建立的[熱河龍科](../Page/熱河龍科.md "wikilink")，這個[演化支還包含](../Page/演化支.md "wikilink")[何耶龍](../Page/何耶龍.md "wikilink")、[長春龍](../Page/長春龍.md "wikilink")，可能還有[韓國龍](../Page/韓國龍.md "wikilink")、[越龍](../Page/越龍.md "wikilink")\[10\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [鳥腳下目的各屬簡介、演化樹](https://web.archive.org/web/20130927064547/http://www.thescelosaurus.com/ornithopoda.htm)
    Thescelosaurus

  - [熱河龍的詳細骨骼資料、分類關係](http://www.dinodata.org/index.php?option=com_content&task=view&id=6733&Itemid=67)
    Dinodata

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:鳥腳下目](../Category/鳥腳下目.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:热河生物群](../Category/热河生物群.md "wikilink")

1.
2.  Paul, G.S., 2010, *The Princeton Field Guide to Dinosaurs*,
    Princeton University Press p. 275

3.
4.  Xu, Wang and You, 2000. A primitive ornithopod from the Early
    Cretaceous Yixian Formation of Liaoning. Vertebrata PalAsiatica
    38(4)318-325.

5.
6.
7.
8.  Zheng, X-.T., You, H.-L., Xu, X. and Dong, Z.-M. (2009). "An Early
    Cretaceous heterodontosaurid dinosaur with filamentous integumentary
    structures." *Nature*, **458**(19): 333-336.

9.

10.