**熱海市**（）位於[日本](../Page/日本.md "wikilink")[靜岡縣東部](../Page/靜岡縣.md "wikilink")，與[神奈川縣接壤](../Page/神奈川縣.md "wikilink")，於1937年4月10日設市。此市以[溫泉而出名](../Page/溫泉.md "wikilink")，也是東京圈重要的觀光都市。

## 地理

位于[伊豆半島的北部](../Page/伊豆半島.md "wikilink")，市區大部分是丘陵。

市區分成三個部分。

  - [熱海站與](../Page/熱海站.md "wikilink")[来宮站地區](../Page/来宮站.md "wikilink")
    - 擁有車站和市政府等，市中心。
  - 湯河原站地區 - 和神奈川縣[湯河原町構成伊豆湯河原温泉](../Page/湯河原町.md "wikilink")。
  - 伊豆多賀站與網代站地區 -
    近[伊東市](../Page/伊東市.md "wikilink")，小規模[旅館很多](../Page/酒店.md "wikilink")。

## 历史

  - 1604年 [德川家康爲洗溫泉而造訪當地](../Page/德川家康.md "wikilink")。
  - 1925年3月25日 [鐵道省熱海線](../Page/鐵道省.md "wikilink")（現在的東海道本線的一部分）
    [湯河原站](../Page/湯河原站.md "wikilink")～[熱海站開業](../Page/熱海站.md "wikilink")。
  - 1937年4月10日 熱海町和多賀村合併，設市。
  - 1950年 成為「[國際觀光文化都市](../Page/國際觀光文化都市.md "wikilink")」。

## 交通

### 鐵道

  - 中央車站：[熱海站](../Page/熱海站.md "wikilink")
  - [東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")（JR東海）
      - [東海道新幹線](../Page/東海道新幹線.md "wikilink")：熱海站
  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）
      - [東海道本線](../Page/東海道本線.md "wikilink")：熱海站
      - [伊東線](../Page/伊東線.md "wikilink")：熱海站 -
        [来宮站](../Page/来宮站.md "wikilink") -
        [伊豆多賀站](../Page/伊豆多賀站.md "wikilink") -
        [網代站](../Page/網代站.md "wikilink")

## 姊妹或友好城市

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>國家</p></th>
<th><p>締結日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/別府市.md" title="wikilink">別府市</a>（<a href="../Page/大分縣.md" title="wikilink">大分縣</a>）</p></td>
<td></td>
<td><p>1966年8月5日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聖雷莫.md" title="wikilink">聖雷莫</a>（<a href="../Page/利古里亞.md" title="wikilink">利古里亞</a>）</p></td>
<td></td>
<td><p>1976年11月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卡斯凱什.md" title="wikilink">卡斯凱什</a>（<a href="../Page/里斯本大區.md" title="wikilink">里斯本大區</a>）</p></td>
<td></td>
<td><p>1989年7月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/珠海市.md" title="wikilink">珠海市</a>（<a href="../Page/廣東省.md" title="wikilink">廣東省</a>）</p></td>
<td></td>
<td><p>2004年7月25日</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [熱海市政府（日語）](http://www.city.atami.shizuoka.jp/www/toppage/0000000000000/APM03000.html)

  - [热海温泉（静冈县热海市）](https://web.archive.org/web/20120513050328/http://www.jnto.go.jp/tourism/ch_kan/s048.html)

  - [日本国家旅游局 |
    热海·伊东](http://www.welcome2japan.cn/location/regional/shizuoka/atami_ito.html)

  -