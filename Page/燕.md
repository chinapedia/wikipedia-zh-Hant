## [中國古代](../Page/中國古代.md "wikilink")[地方政權](../Page/地方政府.md "wikilink")

  - **[燕京戎](../Page/燕京戎.md "wikilink")**，简称“燕”。[商朝末期](../Page/商朝.md "wikilink")[山西](../Page/山西省.md "wikilink")[汾河流域的游牧民族](../Page/汾河.md "wikilink")。
  - **[南燕国](../Page/南燕国.md "wikilink")**，[周朝姞姓诸侯小国](../Page/周朝.md "wikilink")，位于现[河南省](../Page/河南省.md "wikilink")。
  - **[燕国](../Page/燕国.md "wikilink")**，[周朝](../Page/周朝.md "wikilink")[召公建立的政權](../Page/召公.md "wikilink")，位于[冀](../Page/河北省.md "wikilink")[辽一带的](../Page/辽宁省.md "wikilink")[姬姓燕国](../Page/姬姓.md "wikilink")，一稱**北燕國**，战国七雄之一。
  - **[燕国
    (楚汉战争)](../Page/燕国_\(楚汉战争\).md "wikilink")**（前208年—前202年），[秦末](../Page/秦朝.md "wikilink")[漢初](../Page/汉朝.md "wikilink")[韩广](../Page/韩广.md "wikilink")、[臧荼建立地方政權](../Page/臧荼.md "wikilink")。
  - **[燕国](../Page/广阳郡.md "wikilink")**（前202年—前195年），[漢初高祖](../Page/汉朝.md "wikilink")[刘邦册封](../Page/刘邦.md "wikilink")[卢绾建立诸侯国](../Page/卢绾.md "wikilink")。
  - **[广阳郡](../Page/广阳郡.md "wikilink")**，[汉朝](../Page/汉朝.md "wikilink")[幽州地区的一个郡](../Page/幽州.md "wikilink")，建立时称**燕国**，[汉昭帝时改名广阳郡](../Page/汉昭帝.md "wikilink")，[三国及](../Page/三国.md "wikilink")[晋朝复称燕国](../Page/晋朝.md "wikilink")。
  - **[燕国
    (公孙渊)](../Page/燕国_\(公孙渊\).md "wikilink")**，[三国时期](../Page/三国.md "wikilink")**[公孙渊](../Page/公孫淵.md "wikilink")**自称燕王建立的政權，位于[辽东郡](../Page/辽东郡.md "wikilink")，后被[曹魏消灭](../Page/曹魏.md "wikilink")。
  - **[慕容氏諸燕](../Page/慕容氏諸燕.md "wikilink")**，指[十六國時代](../Page/五胡十六国.md "wikilink")，由[鲜卑](../Page/鲜卑.md "wikilink")[慕容氏成員及其養子等](../Page/慕容姓.md "wikilink")，所建立的數個政權。
      - **[前燕](../Page/前燕.md "wikilink")**（337年-370年），[鲜卑人](../Page/鲜卑.md "wikilink")[慕容皝建立的政權](../Page/慕容皝.md "wikilink")。
      - **[西燕](../Page/西燕.md "wikilink")**（384年-394年），[鲜卑人](../Page/鲜卑.md "wikilink")[慕容泓建立的政權](../Page/慕容泓.md "wikilink")。
      - **[後燕](../Page/後燕.md "wikilink")**（384年—407年），[鲜卑人](../Page/鲜卑.md "wikilink")[慕容垂建立的政權](../Page/慕容垂.md "wikilink")。
      - **[南燕
        (十六國)](../Page/南燕_\(十六國\).md "wikilink")**（398年—410年），[鲜卑人](../Page/鲜卑.md "wikilink")[慕容德建立的政權](../Page/慕容德.md "wikilink")。
      - **[北燕](../Page/北燕.md "wikilink")**（407年—436年），[慕容垂的養子](../Page/慕容垂.md "wikilink")[高雲所建之政權](../Page/高雲.md "wikilink")。
  - **燕**（615年—616年），[隋末](../Page/隋朝.md "wikilink")[王須拔](../Page/王須拔.md "wikilink")、[魏刀兒起事自立之国号](../Page/魏刀兒.md "wikilink")，后改魏。
  - **燕国**（618年—624年），[隋末](../Page/隋朝.md "wikilink")[唐初](../Page/唐朝.md "wikilink")[高开道建立地方政權](../Page/高开道.md "wikilink")。
  - **[大燕国](../Page/燕_\(安史之乱\).md "wikilink")**（756年—763年），[唐朝](../Page/唐朝.md "wikilink")[安史之亂时期叛軍將領](../Page/安史之亂.md "wikilink")[安祿山建立的政權](../Page/安祿山.md "wikilink")。
  - **[燕
    (五代)](../Page/燕_\(五代\).md "wikilink")**（911年—913年），[五代十國時期](../Page/五代十国.md "wikilink")[劉守光建立的政權](../Page/劉守光.md "wikilink")。

## 地理名称

  - [河北省北部一帶](../Page/河北省.md "wikilink")，「**燕地**」是該地的別稱。
  - [北京](../Page/北京市.md "wikilink")、[保定一帶](../Page/保定市.md "wikilink")，「**[燕京](../Page/北京市.md "wikilink")**」是該地的雅稱。
  - **[燕市](../Page/燕市.md "wikilink")**，[日本城市名稱](../Page/日本.md "wikilink")，在[新潟县](../Page/新潟县.md "wikilink")。

## 动物

  - **[燕科](../Page/燕科.md "wikilink")**，一種[鸟纲](../Page/鸟.md "wikilink")[雀形目中的一](../Page/雀形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")
  - **[燕鳥](../Page/燕鳥.md "wikilink")**，一種[下白堊紀的古](../Page/白垩纪.md "wikilink")[鳥類](../Page/鸟.md "wikilink")

## 姓氏

  - **[燕姓](../Page/燕姓.md "wikilink")**，一個[中國姓氏](../Page/中國姓氏.md "wikilink")

## 參見

  -