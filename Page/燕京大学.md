**燕京大学**（），简称**燕大**，是20世纪初由四間[美國及](../Page/美國.md "wikilink")[英國](../Page/英國.md "wikilink")[基督教](../Page/基督教.md "wikilink")[教会聯合於](../Page/教会.md "wikilink")[北京开办的](../Page/北京.md "wikilink")[綜合大學](../Page/綜合大學.md "wikilink")，為當時[中國教學品質](../Page/中國.md "wikilink")、校園環境優秀的大學之一，也被認為是[中國教會學校之首](../Page/中國教會學校列表.md "wikilink")\[1\]\[2\]。校名源自於[北京昔名](../Page/北京.md "wikilink")——**燕京**。現今美國[哈佛大學的](../Page/哈佛大學.md "wikilink")[哈佛燕京學社即以該校為名](../Page/哈佛燕京學社.md "wikilink")。

1949年[中共建政後](../Page/中共建政.md "wikilink")，燕京大學資產由[中华人民共和国政府接管](../Page/中华人民共和国政府.md "wikilink")，隨後遭到整併而解體：民族学系、社会学系、语文系（民族语文系）、历史系并入中央民族学院（今[中央民族大学](../Page/中央民族大学.md "wikilink")），法学院并入[北京政法学院](../Page/北京政法学院.md "wikilink")（今[中国政法大学](../Page/中国政法大学.md "wikilink")）、[中國人民大學](../Page/中國人民大學.md "wikilink")，经济学系并入中央财经学院（今[中央财经大学](../Page/中央财经大学.md "wikilink")），其他文科、理科多併入[北京大學](../Page/北京大學.md "wikilink")，工学院併入[清華大學](../Page/清華大學.md "wikilink")，醫學系併入[中国协和医学院](../Page/北京协和医学院.md "wikilink")。其校舍由北京大學接收，成為北京大學的主校區，現已列為古蹟保護。此外，其部分人員南遷[香港](../Page/香港.md "wikilink")，參與[崇基學院的創建](../Page/崇基學院.md "wikilink")，為[香港中文大學創始的成員](../Page/香港中文大學.md "wikilink")[書院之一](../Page/書院.md "wikilink")。

## 历史

[Yenching_University_campus.jpg](https://zh.wikipedia.org/wiki/File:Yenching_University_campus.jpg "fig:Yenching_University_campus.jpg")

### 起源

燕京大学建立于1919年，其前身是[美国教会在](../Page/美国.md "wikilink")[北京一带办的三所](../Page/北京.md "wikilink")[教会](../Page/教会.md "wikilink")[学校](../Page/学校.md "wikilink")：

  - 汇文大学（Peking Huiwen
    University，美国[美以美会](../Page/美以美会.md "wikilink")）。前身是1870年创办的崇内怀理书院，1889年改名汇文大学。校长[刘海澜博士](../Page/刘海澜.md "wikilink")，在[崇文门](../Page/崇文门.md "wikilink")[船板胡同的校址后来改办](../Page/船板胡同.md "wikilink")[汇文中学](../Page/汇文中学.md "wikilink")。

<!-- end list -->

  - [华北协和女子大学](../Page/华北协和女子大学.md "wikilink")（前身是美国[公理会](../Page/公理会.md "wikilink")1864年创办的贝满女塾），位于灯市口东口的佟府夹道胡同。校址后来改办[贝满女中](../Page/贝满女中.md "wikilink")

<!-- end list -->

  - [通州协和大学](../Page/通州协和大学.md "wikilink")（Tongzhou Harmony
    University）。前身是1867年创办的“[公理会潞河书院](../Page/公理会.md "wikilink")”。[谢卫楼](../Page/谢卫楼.md "wikilink")（Davelle
    Sheffield）校长。

除了[美以美会和](../Page/美以美会.md "wikilink")[公理会外](../Page/公理会.md "wikilink")，美国[美北长老会和英国](../Page/美北长老会.md "wikilink")[伦敦会也参与燕京大学的创建](../Page/伦敦会.md "wikilink")。

### 变迁

1920年，汇文大学、华北协和女子大学和通州协和大学合并，在[南京](../Page/南京.md "wikilink")[金陵神学院任教的](../Page/金陵神学院.md "wikilink")[司徒雷登被任命为新成立的](../Page/司徒雷登.md "wikilink")**燕京大学**的校长。

1921年，校长[司徒雷登获得](../Page/司徒雷登.md "wikilink")[美国著名出版商](../Page/美国.md "wikilink")[亨利·卢斯及](../Page/亨利·卢斯.md "wikilink")[美铝公司创办人](../Page/美国铝业公司.md "wikilink")[查尔斯·马丁·霍尔的捐款](../Page/查尔斯·马丁·霍尔.md "wikilink")，在北京西郊购买了数处前[清](../Page/清.md "wikilink")[亲王赐园](../Page/亲王.md "wikilink")，聘请建筑设计师[亨利·墨菲进行总体规划](../Page/亨利·墨菲.md "wikilink")，建造了近代中国规模最大、质量最高、环境最优美的一所校园。1926年，正式迁址。全校共轄有[神学院](../Page/神学院.md "wikilink")、[法学院](../Page/法学院.md "wikilink")、[医学院](../Page/医学院.md "wikilink")（又称医预院，学-{制}-为三年预科），以及[文科和](../Page/文科.md "wikilink")[理科相关专业学系](../Page/理科.md "wikilink")。

[Communists_taking_over_Yenching_University.jpg](https://zh.wikipedia.org/wiki/File:Communists_taking_over_Yenching_University.jpg "fig:Communists_taking_over_Yenching_University.jpg")等进入校园\]\]

1929年，燕京大学和哈佛大学开展国际学术合作，成立[哈佛燕京学社](../Page/哈佛燕京学社.md "wikilink")，提高了燕大学术地位，也扩充燕大的经济来源。\[3\]\[4\]

1952年，[中国大陆实行院系调整](../Page/中国大陆.md "wikilink")，燕京大学被拆分，学校民族学系、社会学系、语文系（民族语文系）、历史系并入中央民族学院（今[中央民族大学](../Page/中央民族大学.md "wikilink")），法学院并入[北京政法学院](../Page/北京政法学院.md "wikilink")（今[中国政法大学](../Page/中国政法大学.md "wikilink")），经济学系并入中央财经学院（今[中央财经大学](../Page/中央财经大学.md "wikilink")），其他文科、理科多併入[北京大學](../Page/北京大學.md "wikilink")、工学院併入[清華大學](../Page/清華大學.md "wikilink")。**燕京大学**校址“[燕园](../Page/燕园.md "wikilink")”校舍由北京大學接收。

### 传承

[國民政府遷台後](../Page/國民政府.md "wikilink")，燕京大學校董會聯合其他十三所基督教大學在[香港成立](../Page/香港.md "wikilink")[崇基學院](../Page/崇基學院.md "wikilink")，崇基學院隨後被併入[香港中文大學](../Page/香港中文大學.md "wikilink")。

燕京大学校園在[中华人民共和国成立之后由](../Page/中华人民共和国.md "wikilink")[中共政府接管](../Page/中共政府.md "wikilink")，燕京大学就此终结，1952年全国高校院系调整，北大迁入燕大的校址。由于当时“院系调整”规模较大，除院系外人事亦有很大调整，涉及全国几乎所有高等院校，其中一些调整在1949年以后已经开始，所有原教会背景学校于此时停办，院系及人员分拆进入其他大学。严格意义上说燕京大学与其他中国教会学校的历史在1949年正式终止。\[5\]

另外，[香港地區的香港](../Page/香港.md "wikilink")[校友會則成立](../Page/校友會.md "wikilink")[中華基督教會燕京書院](../Page/中華基督教會燕京書院.md "wikilink")，以繼承及發揚燕京大學之辦學精神和傳統。學校沿用「因真理，得自由，以服務」為校訓。\[6\]

## 校园

燕京大学原址即今天的[北京大学校园](../Page/北京大学.md "wikilink")。校园最初的基址是[清朝](../Page/清朝.md "wikilink")[三山五园的附属园林](../Page/三山五园.md "wikilink")，包括漱春园、弘雅园（墨尔根园）（漱春园和弘雅园为[明朝](../Page/明朝.md "wikilink")[勺园的一部分](../Page/勺园.md "wikilink")），1921年自军阀[陈树藩手中买入](../Page/陈树藩.md "wikilink")。后又陆续从[载沣等人手中购入朗润园](../Page/载沣.md "wikilink")、蔚秀园、承泽园等园林（1949年后，原由[徐世昌家族租用的镜春园](../Page/徐世昌.md "wikilink")、鸣鹤园并入燕京大学园区）。1921年—1926年，曾为多座在华教会大学进行过设计的[美国建筑师](../Page/美国.md "wikilink")[亨利·墨菲接受聘请](../Page/亨利·墨菲.md "wikilink")，为燕京大学进行了总体规划和建筑设计，建筑群全部都采用了中国古典宫殿的式样。

燕京大学的正门位于校园西边，坐西向东，以[玉泉山玉峰塔为对景](../Page/玉泉山.md "wikilink")，门内为东西轴线。从校友门经石拱桥、华表（取自[圆明园安佑宫废墟](../Page/圆明园.md "wikilink")），方院两侧是九开间的庑殿顶建筑——穆楼和民主楼，正面是歇山顶的贝公楼（行政楼），两侧是宗教楼和[图书馆](../Page/图书馆.md "wikilink")，沿中轴线继续向东，一直到[未名湖中的思义亭](../Page/未名湖.md "wikilink")，湖畔还有[博雅塔](../Page/博雅塔.md "wikilink")、临湖轩。东部以未名湖为界，分为北部的男院和南部的女院。男院包括德、才、兼、备4幢男生宿舍以及华氏体育馆。女院沿一条南北轴线，分布适楼、南北阁、女生宿舍和鲍氏体育馆。

燕京大学建筑群在外部尽量模仿中国古典建筑，在内部使用功能方面则尽量采用当时最先进的设备：[暖气](../Page/暖气.md "wikilink")、[热水](../Page/热水.md "wikilink")、[抽水马桶](../Page/抽水马桶.md "wikilink")、[浴缸](../Page/浴缸.md "wikilink")、[饮水喷泉等等](../Page/饮水喷泉.md "wikilink")。

## 传统

### 校训

  - 1937年至1945年改为：因自由 得真理 以服务\[7\]

### 校歌

## 校长

  - [司徒雷登](../Page/司徒雷登.md "wikilink")：1919年1月-1945年，后长期担任校务长
  - [吳雷川](../Page/吳雷川.md "wikilink")：1929年-1934年为代理校长
  - [陸志韋](../Page/陸志韋.md "wikilink")：1934年-1941年为代理校长
  - [梅貽寶](../Page/梅貽寶.md "wikilink")：1942年-1945年为代理校长
  - [陸志韋](../Page/陸志韋.md "wikilink")：1945年-1949年为代理校长
  - [翁独健](../Page/翁独健.md "wikilink")：1949年-1952年为代理校长

## 著名校友

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/鄭念.md" title="wikilink">鄭念</a>（<a href="../Page/上海生與死.md" title="wikilink">上海生與死</a>）</li>
<li><a href="../Page/謝婉瑩.md" title="wikilink">謝婉瑩</a>（<a href="../Page/冰心.md" title="wikilink">冰心</a>）</li>
<li><a href="../Page/周南_(新华社社长).md" title="wikilink">周南</a>，<a href="../Page/新華社.md" title="wikilink">新華社社長</a></li>
<li><a href="../Page/趙蘭坤.md" title="wikilink">趙蘭坤</a></li>
<li><a href="../Page/杜聯喆.md" title="wikilink">杜聯喆</a></li>
<li><a href="../Page/房兆楹.md" title="wikilink">房兆楹</a></li>
<li><a href="../Page/黃華_(政治人物).md" title="wikilink">黃華——原名王汝梅</a>，曾經任外交部部長、國務院副總理。</li>
<li><a href="../Page/沈昌煥.md" title="wikilink">沈昌煥</a></li>
<li><a href="../Page/俞啟威.md" title="wikilink">俞啟威</a></li>
<li><a href="../Page/沈劍虹.md" title="wikilink">沈劍虹</a></li>
<li><a href="../Page/余夢燕.md" title="wikilink">余夢燕</a></li>
<li><a href="../Page/魏景蒙.md" title="wikilink">魏景蒙</a></li>
<li><a href="../Page/侯祥麟.md" title="wikilink">侯祥麟</a></li>
<li><a href="../Page/谭其骧.md" title="wikilink">谭其骧</a></li>
<li><a href="../Page/罗香林.md" title="wikilink">罗香林</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/翁心植.md" title="wikilink">翁心植</a></li>
<li><a href="../Page/王夔.md" title="wikilink">王夔</a></li>
<li><a href="../Page/翁獨健.md" title="wikilink">翁獨健</a></li>
<li><a href="../Page/周一良.md" title="wikilink">周一良</a></li>
<li><a href="../Page/李慎之.md" title="wikilink">李慎之</a></li>
<li><a href="../Page/蔣彥永.md" title="wikilink">蔣彥永</a></li>
<li><a href="../Page/褚圣麟.md" title="wikilink">褚圣麟</a></li>
<li><a href="../Page/張文裕.md" title="wikilink">張文裕</a></li>
<li><a href="../Page/王承书.md" title="wikilink">王承书</a></li>
<li><a href="../Page/黄昆.md" title="wikilink">黄昆</a></li>
<li><a href="../Page/吴兴华.md" title="wikilink">吴兴华</a></li>
<li><a href="../Page/王世襄.md" title="wikilink">王世襄</a></li>
<li><a href="../Page/阮銘.md" title="wikilink">阮銘</a></li>
<li><a href="../Page/侯仁之.md" title="wikilink">侯仁之</a></li>
<li><a href="../Page/周汝昌.md" title="wikilink">周汝昌</a></li>
<li><a href="../Page/连士升.md" title="wikilink">连士升</a></li>
<li><a href="../Page/薛寿生.md" title="wikilink">薛寿生</a></li>
<li><a href="../Page/司徒喬.md" title="wikilink">司徒喬</a></li>
<li><a href="../Page/江平_(學者).md" title="wikilink">江平 (學者)</a></li>
<li><a href="../Page/孫道臨.md" title="wikilink">孫道臨</a></li>
<li><a href="../Page/鄭德坤.md" title="wikilink">鄭德坤</a>（1907-2001），<a href="../Page/香港中文大學.md" title="wikilink">香港中文大學中國考古藝術研究中心首任主任</a>，1977至1979年中文大學副校長。[8]</li>
</ul></td>
</tr>
</tbody>
</table>

## 軼事

民國十九年（1930年），[燕京大學](../Page/燕京大學.md "wikilink")[大講堂落成](../Page/大講堂.md "wikilink")，社會名流各國代表參加落成儀式。時任[閻錫山北平駐軍](../Page/閻錫山.md "wikilink")[統帥](../Page/統帥.md "wikilink")[商震在落成會上致辭](../Page/商震.md "wikilink")，先稱讚大講堂的結構，後話鋒一轉，喊道：「教育我們中國人的講堂——如此壯觀的講堂，不是由我們中國人手建造，卻是由外國人手建成，對我們中國人來說，難道不是莫大的恥辱！」。頃刻間，擠滿在後面座位的學生們響起了暴風雨般的鼓掌和怒號。\[9\]

## 参见

  - [中国教会学校列表](../Page/中国教会学校列表.md "wikilink")
  - [哈佛燕京学社](../Page/哈佛燕京学社.md "wikilink")
  - [中華基督教會燕京書院](../Page/中華基督教會燕京書院.md "wikilink")
  - [中國人民大學](../Page/中國人民大學.md "wikilink")
  - [北京大學](../Page/北京大學.md "wikilink")
  - [清華大學](../Page/清華大學.md "wikilink")
  - [香港中文大學](../Page/香港中文大學.md "wikilink")
  - [19世纪中国大学列表](../Page/19世纪中国大学列表.md "wikilink")

## 外部链接

  - [哈佛燕京學社](http://www.harvard-yenching.org/)
  - [《亲历与见闻——黄华回忆录》 作者： 黄华
    出版社：世界知识出版社。](https://web.archive.org/web/20151117020026/http://lz.book.sohu.com/serialize-id-8054.html)

## 參考資料

{{-}}

[燕京大学](../Category/燕京大学.md "wikilink")
[Category:中華民國大陸時期大專院校](../Category/中華民國大陸時期大專院校.md "wikilink")
[Category:中華人民共和國已不存在的大學](../Category/中華人民共和國已不存在的大學.md "wikilink")
[Category:北平四大名校](../Category/北平四大名校.md "wikilink")
[Category:1952年廢除](../Category/1952年廢除.md "wikilink")
[Category:1919年創建的教育機構](../Category/1919年創建的教育機構.md "wikilink")
[Category:中國傳統建築](../Category/中國傳統建築.md "wikilink")
[Category:1919年中國建立](../Category/1919年中國建立.md "wikilink")

1.
2.
3.
4.
5.  关于燕京大学1945-1949年变化可参阅：。
6.
7.
8.
9.