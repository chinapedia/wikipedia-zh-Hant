[Yanshan_Petrochemical_Headquarters_(20150105093445).JPG](https://zh.wikipedia.org/wiki/File:Yanshan_Petrochemical_Headquarters_\(20150105093445\).JPG "fig:Yanshan_Petrochemical_Headquarters_(20150105093445).JPG")
**中国石油化工股份有限公司北京燕山分公司**（简称**燕山石化**，原稱**北京燕山石油化工公司、北京燕山石油化工股份有限公司**）（除牌當時為0325.HK）--[中國石化旗下公司](../Page/中國石化.md "wikilink")，前[香港上市公司](../Page/香港.md "wikilink")。

創辦於1970年7月20日[北京](../Page/北京.md "wikilink")[房山区燕山](../Page/房山区.md "wikilink")，主要業務為製造及銷售三類主要[石油化工產品](../Page/石油化工.md "wikilink")；即(i)[樹脂及](../Page/樹脂.md "wikilink")[塑料](../Page/塑料.md "wikilink")；(ii)[合成橡膠](../Page/合成橡膠.md "wikilink")；及(iii)基本有機化工產品。本公司的主要生產設施包括71萬噸[乙烯裝置](../Page/乙烯.md "wikilink")（年設計能力）、高壓聚乙烯裝置、[聚丙烯裝置](../Page/聚丙烯.md "wikilink")、低壓聚乙烯裝置、[苯酚丙酮裝置](../Page/苯酚丙酮.md "wikilink")、順丁橡膠裝置及丁基橡膠裝置。全部來自中國收入\[1\]。

1997年4月23日成立股份有限公司，並在香港及[紐約上市](../Page/紐約.md "wikilink")，1998年7月27日，[中國石化進行大改組](../Page/中國石化.md "wikilink")，轉為[中國石化控股公司](../Page/中國石化.md "wikilink")。2005年1月17日，本公司已被[中國石化控股公司以按照中國公司法第](../Page/中國石化.md "wikilink")184條，透過北京飛天以吸收合併方式，以註銷本公司H股3.8港元私有化通過，正式2005年10月5日退出[香港和](../Page/香港.md "wikilink")[紐約股市](../Page/紐約.md "wikilink")。同時註銷[美國存託股份股票](../Page/ADR.md "wikilink")\[2\]。

## 图集

## 參考

## 外部連結

  - [北京燕山石油化工網頁](http://bypc.sinopec.com/)

[Category:总部位于北京市的中华人民共和国国有企业](../Category/总部位于北京市的中华人民共和国国有企业.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:中国石化](../Category/中国石化.md "wikilink")
[Category:前恒生中國企業指數成份股](../Category/前恒生中國企業指數成份股.md "wikilink")
[Category:1970年成立的公司](../Category/1970年成立的公司.md "wikilink")
[Category:房山区](../Category/房山区.md "wikilink")

1.  [中国石油化工股份有限公司北京燕山分公司](http://bypc.sinopec.com/bypc/about_us/instro/)
2.  [私有化通告](http://www.hkexnews.hk/listedco/listconews/SEHK/2005/0117/LTN20050117072_C.pdf)