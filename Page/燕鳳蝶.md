**燕鳳蝶**（[學名](../Page/學名.md "wikilink")：**）是[鳳蝶科下](../Page/鳳蝶科.md "wikilink")[燕鳳蝶屬的一個](../Page/燕鳳蝶屬.md "wikilink")[種](../Page/種.md "wikilink")，生活於[南亞及](../Page/南亞.md "wikilink")[東南亞](../Page/東南亞.md "wikilink")。

## 描述

[VB_013_White_Dragontail.jpg](https://zh.wikipedia.org/wiki/File:VB_013_White_Dragontail.jpg "fig:VB_013_White_Dragontail.jpg")
燕鳳蝶的上表面呈深黑褐色。前翅上有白色半透明的斜汶，由前緣伸展至背部。前翅的外半部份有[三角形的透明部份](../Page/三角形.md "wikilink")。在半透明斜汶及透明的位置中間，是隱約呈圓環的黑邊。黑邊伸延至前緣及末端，向頂端加寬。纖毛呈黑色。

前翅上的白紋在後翅上延續並於第3翅脈完結，而外圍並不是半透明的。後翅的後半部呈深黑褐色，伸延至幼長尾巴於第4翅脈白色鱗片的末端。纖毛同樣是黑色的，但在第5翅脈及沿尾巴外圍底部的纖毛是白色的。

底部相似，但是呈不透明的黑褐色。一條不明確灰色斑紋沿翅膀的底部伸延至後翅的背緣及沿前翅的前緣。後翅上的不透明白帶從背緣至頂端與迂迴的白線相連。之後在臀部有不規則的白色小圓點。

觸角、[頭部及](../Page/頭部.md "wikilink")[胸部都是黑色的](../Page/胸部.md "wikilink")，而下腹是深黑褐色的；觸角、胸部及下腹的底部呈灰色。跗節的爪分為兩半的。雄性有性別的標記。\[1\]

## 分佈

燕鳳蝶在[印度的](../Page/印度.md "wikilink")[阿薩姆邦至](../Page/阿薩姆邦.md "wikilink")[緬甸都可以發現](../Page/緬甸.md "wikilink")，亦可以在[泰國](../Page/泰國.md "wikilink")、[老撾](../Page/老撾.md "wikilink")、[越南](../Page/越南.md "wikilink")、南[中國](../Page/中國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[汶萊](../Page/汶萊.md "wikilink")、[菲律賓及](../Page/菲律賓.md "wikilink")[香港都有牠們的蹤跡](../Page/香港.md "wikilink")。
[VB_015_White_Dragontai.jpg](https://zh.wikipedia.org/wiki/File:VB_015_White_Dragontai.jpg "fig:VB_015_White_Dragontai.jpg")

## 保育狀況

現時仍未有任何報告指燕鳳蝶受到危害，但在[馬來西亞半島將燕鳳蝶定為易危及需要接受保護](../Page/馬來西亞.md "wikilink")\[2\]。在香港燕鳳蝶也未受保護，但其寄主植物寬藥青藤則受到香港法例第96章《林區及郊區條例》附屬法例《林務規例》內的保護\[3\]。

## 生命週期

燕鳳蝶的生命週期約為6星期。

### 食物

燕鳳蝶幼蟲的寄主[植物為](../Page/植物.md "wikilink")[寬藥青藤](../Page/寬藥青藤.md "wikilink")\[4\]\[5\]\[6\]。

### 幼蟲

早期[幼蟲的胸部面是呈黑色的](../Page/幼蟲.md "wikilink")，胸部較窄，尾巴較寬。幼蟲光滑。兩則、頭部的幼環及[肛門部份都是黃赭色的](../Page/肛門.md "wikilink")。開始時頭部是黑色的，在較後的時期會變為[蘋果綠色](../Page/蘋果.md "wikilink")，背部較深，在第7至10節有3條幼長的黃帶，在腳上由頭至尾有一條淺黃色線。頭部是綠色的，在頂部有四個黑圓點，和在口頜部份有兩個較小的黑圓點。\[7\]

### 蛹

在人工培養下發現，燕鳳蝶的在[樹葉下結](../Page/樹葉.md "wikilink")[蛹](../Page/蛹.md "wikilink")。蛹是由在懸絲上的白色墊子所連結，而蛹本身則有圍邊。蛹只有約22毫米長，及於初期的黃綠色轉至成熟的[翡翠色](../Page/翡翠.md "wikilink")。\[8\]

## 參考

[Category:燕鳳蝶屬](../Category/燕鳳蝶屬.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國蝴蝶](../Category/中國蝴蝶.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")

1.  Bingham, C. T. 1907. Fauna of British India. Butterflies. Volume 2

2.  Collins, N.M., Morris, M.G. (1985) *Threatened Swallowtail
    Butterflies of the World*. IUCN. ISBN 2-88032-603-6

3.  [漁農自然護理署蝴蝶介紹](http://www.afcd.gov.hk/tc_chi/conservation/hkbiodiversity/speciesgroup/speciesgroup_butterflies.html)，載[漁農自然護理署網頁](../Page/漁農自然護理署.md "wikilink")，2008年9月9日下載

4.  Gaden S. Robinson, Phillip R. Ackery, Ian J. Kitching, George W.
    Beccaloni and Luis M. Hernández HOSTS - a Database of the World's
    Lepidopteran Hostplants.
    [1](http://www.nhm.ac.uk/research-curation/projects/hostplants/)
    Accessed December 2006

5.  [Burkhardt, Col. V.R. 1964. Hongkong Naturalist.
    pages 97-104](http://sunzi1.lib.hku.hk/hkjo/view/44/4401182.pdf)

6.

7.
8.