**爛番茄**（[英文](../Page/英文.md "wikilink")：**Rotten
Tomatoes**）是一個[網站](../Page/網站.md "wikilink")，以提供[電影](../Page/電影.md "wikilink")、[電子遊戲及](../Page/電子遊戲.md "wikilink")[电视节目的相關評論](../Page/电视节目.md "wikilink")、資訊和新聞為主。1998年8月，网站由[加州大学伯克利分校](../Page/加利福尼亞大學柏克萊分校.md "wikilink")（UC
Berkeley）的三位亚裔本科生创建：Senh Duong, Patrick Y. Lee and Stephen
Wang。\[1\]\[2\]\[3\]\[4\]網站的名稱是因[歌舞雜耍表演](../Page/歌舞雜耍表演.md "wikilink")（vaudeville）時期觀眾在演出不佳時會往舞台上扔擲[番茄和其他蔬果而來](../Page/番茄.md "wikilink")。

爛番茄的工作人員會搜尋各網站上刊載的特定電影或遊戲評論，不論是業餘或專業的。一旦搜尋到之後，工作人員使用[整合資料](../Page/整合資料.md "wikilink")（aggregate
data）來決定評論是正面（「新鮮」（fresh），以一個鮮紅的番茄作為標記）或負面（「腐爛」（rotten），以一個綠色被砸爛的番茄作為標記）。

網站會追蹤所有的評論內容（一些主要的大型電影約能達到250篇）以及正面評價的比例。若正面的評論超過60%以上，該部作品將會被認為是「新鮮」（fresh）。相反的，若一部作品的正面評價低於60%，則該作品會被標示為「腐爛」（rotten）。此外，知名的影評人如[羅傑·艾伯特](../Page/羅傑·艾伯特.md "wikilink")、狄森·湯瑪森（Desson
Thomson）和史帝芬·杭特（Stephen
Hunter，[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")）等人會被列在網頁上一個名為「Cream
of the Crop」的專區，將他們的影評獨立呈現，同時也依然將這些知名作者的影評列入電影的整體評分計算中。

許多想看電影的民眾，都會參考這個網站，例如蘋果[iTunes
Store和](../Page/iTunes_Store.md "wikilink")[Google Play
電影也會加以連結評價](../Page/Google_Play_Store.md "wikilink")，以便民眾參考。

## 参考

## 外部連結

  - [RottenTomatoes.com](http://www.rottentomatoes.com)

  -
  -
  -
  -
[Category:线上电影数据库](../Category/线上电影数据库.md "wikilink")
[Category:电影奖项](../Category/电影奖项.md "wikilink")
[Category:电影主题网站](../Category/电影主题网站.md "wikilink")
[Category:电影评论网站](../Category/电影评论网站.md "wikilink")
[Category:推荐系统](../Category/推荐系统.md "wikilink")
[Category:1998年建立的网站](../Category/1998年建立的网站.md "wikilink")

1.
2.
3.
4.