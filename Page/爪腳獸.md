**爪腳獸**（*Ancylotherium*）是[爪獸科最後生存的一類](../Page/爪獸科.md "wikilink")，生存於650-200萬年前的[中新世晚期及大部份](../Page/中新世.md "wikilink")[上新世](../Page/上新世.md "wikilink")。

## 描述

爪腳獸相對較大，肩高達2米，外觀較像[山羊](../Page/山羊.md "wikilink")。爪腳獸有長的前肢及短的後肢，但是牠們並非趾行的。牠們像[北美洲的](../Page/北美洲.md "wikilink")[石爪獸](../Page/石爪獸.md "wikilink")。

爪腳獸生活在[東非及](../Page/東非.md "wikilink")[南非的](../Page/南非.md "wikilink")[大草原](../Page/大草原.md "wikilink")。牠們是[草食性的](../Page/草食性.md "wikilink")，適合吃大草原樹上的植物。

爪腳獸的近親是其他的[奇蹄目](../Page/奇蹄目.md "wikilink")，包括已[滅絕的](../Page/滅絕.md "wikilink")[雷獸及現今](../Page/雷獸.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")，如[馬](../Page/馬.md "wikilink")、[貘及](../Page/貘.md "wikilink")[犀牛](../Page/犀牛.md "wikilink")。

## 化石

爪腳獸的[化石遺骸在很多](../Page/化石.md "wikilink")[東非及](../Page/東非.md "wikilink")[南非的](../Page/南非.md "wikilink")[人科化石位址中發現](../Page/人科.md "wikilink")，如[拉多里](../Page/拉多里.md "wikilink")、[奧杜威及](../Page/奧杜威.md "wikilink")[奧莫河](../Page/奧莫河.md "wikilink")。

## 參考

  - Haines, Tim, and Paul Chambers. The Complete Guide to Prehistoric
    Life. Canada: Firefly Books Ltd., 2006.

## 外部連結

  - [BBC Science and
    News](http://www.bbc.co.uk/nature/wildfacts/factfiles/451.shtml)
  - [Fact File
    description](https://web.archive.org/web/20080520174918/http://www.abc.net.au/beasts/factfiles/factfiles/ancylotherium.htm)

[Category:爪獸科](../Category/爪獸科.md "wikilink")