**爱尔兰根纲领**（；）是[菲利克斯·克莱因于](../Page/菲利克斯·克莱因.md "wikilink")1872年发表一个深具影响的研究[纲领](../Page/纲领.md "wikilink")，题为《新几何研究上比较的观点》（*Vergleichende
Betrachtungen über neuere geometrische
Forschungen*），由于克莱因那个时候在[爱尔兰根而得名](../Page/爱尔兰根.md "wikilink")。该纲领建议了对于那个时候的[几何问题的一种新的解决办法](../Page/几何.md "wikilink")。

## 19世纪几何中的问题

有一个“几何”还是很多个？自[欧几里得以来](../Page/欧几里得.md "wikilink")，几何意味着二维（[平面几何](../Page/平面几何.md "wikilink")）或者三维（[立体几何](../Page/立体几何.md "wikilink")）[欧几里得空间的几何](../Page/欧几里得空间.md "wikilink")。在19世纪上半叶，有了一些发展使得这个景象变得复杂了。数学应用要求有四维或者更高维的几何；对传统欧几里得几何的基础的审视已经揭示出平行公理和其他公理的独立性，而且[非欧几里得几何已经诞生](../Page/非欧几里得几何.md "wikilink")；而在[射影几何中](../Page/射影几何.md "wikilink")，新的“点”（[无穷远点](../Page/无穷远线.md "wikilink")，有[复数坐标的点](../Page/复数.md "wikilink")）已经被引入。

用抽象术语来说，这个解决办法是使用[对称性作为根本的原则](../Page/对称性.md "wikilink")，并且从一开始就陈述不同的几何可以共存，因为它们处理不同类型的命题和不同类型的对称性和变换下的不变量。[仿射几何和](../Page/仿射几何.md "wikilink")[射影几何的区别就在于诸如平行这种仿射不变量的概念是前者的恰当主题](../Page/射影几何.md "wikilink")，而对后者来说却不是主要概念。然后，通过从各个几何中抽象出基础的对称[群](../Page/群.md "wikilink")，它们之间的关系可以在群的级别重新建立。因为仿射几何的群是射影几何的群的[子群](../Page/子群.md "wikilink")，所有射影几何的概念不变量“先验的”在仿射几何中有意义；但是反过来不行。如果你包含更多对称性进来，你就有一个更强的理论，但更少的概念和定理（但会更深刻和一般化）。

## 齐性空间

换而言之，各种“传统空间”是[齐性空间](../Page/齐性空间.md "wikilink")；但是不是对于一个唯一确定的群。改变群就改变了相应的几何语言。

在今天的语言中，经典几何中考虑的群都是很著名的[李群](../Page/李群.md "wikilink")。特定的关系用技术化的语言很容易描述。

## 例子：仿射几何

例如*n*维[射影几何的群就是](../Page/射影几何.md "wikilink")*n*维[射影空间的对称群](../Page/射影空间.md "wikilink")（*n+1*阶矩阵群，取和标量矩阵的商）。该[仿射群是保持所选的](../Page/仿射群.md "wikilink")**[无穷远超平面](../Page/无穷远超平面.md "wikilink")**不变（映射集合到自身，不是固定每一点）的子群。这个子群有一个已知的结构（*n*阶矩阵群和[平移子群的](../Page/平移.md "wikilink")[准直积](../Page/准直积.md "wikilink")）。这个表述告诉我们什么性质是“仿射的”。用欧几里得平面几何术语，平行就是：仿射变换总是将一个平行四边形变成另一个平行四边形。而圆不是仿射的，因为仿射剪切可以把圆变成椭圆。

要精确的解释仿射和欧几里得几何之间的关系，我们现在要在仿射群中点出欧几里得几何的群。[欧几里得群实际上是](../Page/欧几里得群.md "wikilink")（采用前面仿射群的表述）正交（旋转和反射）群和平移群的准直积。

## 在后来的工作上的影响

爱尔兰根纲领的长期效应可以在纯数学的很多方面显现出来（例如，参看[相似中隐含的使用](../Page/相似_\(几何\).md "wikilink")）；而变换的思想和用对称群综合的思想现在当然也已成为[物理学中的标准做法](../Page/物理学.md "wikilink")。

当[拓扑照例使用](../Page/拓扑.md "wikilink")[同胚下的](../Page/同胚.md "wikilink")[不变量的术语来表述时](../Page/不变量.md "wikilink")，我们可以看到操作背后的基础思想。所涉及到的群在几乎所有情况下——除了[李群](../Page/李群.md "wikilink")——都是无穷维的，但其方法是一样。当然这只是说克莱因的影响启发。诸如[哈罗德·斯科特·麦克唐纳·考克斯特所写的书例行的采用爱尔兰根纲领的方法来帮助](../Page/哈罗德·斯科特·麦克唐纳·考克斯特.md "wikilink")'定位'几何。用说教的术语，该纲领成了[变换几何](../Page/变换几何.md "wikilink")，这是一个有一些不良影响的好事，它比[欧几里得的风格建立在更强的直觉上](../Page/欧几里得.md "wikilink")，但是也更难转换成为逻辑体系。

对于一个几何和它的群，群的一个元素有时叫做该几何的一个**[作用](../Page/群作用.md "wikilink")**。例如，可以通过基于[双曲运动的一个发展来学习](../Page/双曲运动.md "wikilink")[双曲几何的](../Page/双曲几何.md "wikilink")[庞加莱半平面模型](../Page/庞加莱半平面模型.md "wikilink")。

## 从爱尔兰根纲领的抽象的回归

经常，两个或者更多的不同的[几何有](../Page/几何.md "wikilink")[同构的](../Page/同构.md "wikilink")[自同构群](../Page/自同构群.md "wikilink")。这就产生了从爱尔兰根纲领的*抽象*群解读出具体的几何的问题。

一个例子：[可定向](../Page/可定向.md "wikilink")（也就是说，[反射是除外的](../Page/反射_\(数学\).md "wikilink")）[椭圆几何](../Page/椭圆几何.md "wikilink")（也就是，把[n维球面相对点等同的曲面](../Page/n维球面.md "wikilink")）和[可定向](../Page/可定向.md "wikilink")[球面几何](../Page/球面几何.md "wikilink")（同样的[非欧几里得几何](../Page/非欧几里得几何.md "wikilink")，但是相对的点没有等同起来）有[同构的](../Page/同构.md "wikilink")[自同构群](../Page/自同构群.md "wikilink")，偶数*n*的[*SO*(*n*+1)](../Page/特殊正交群.md "wikilink")｡这两个看起来不同。但是事实上，这两个几何紧密相关，以一种可以精确描述的方式。

在举一例，不同[曲率半径的](../Page/曲率半径.md "wikilink")[椭圆几何有](../Page/椭圆几何.md "wikilink")[同构的](../Page/同构.md "wikilink")[自同构群](../Page/自同构群.md "wikilink")。这其实不能算作一个评价，因为所有这种几何[同构](../Page/同构.md "wikilink")。一般的[黎曼几何在这个纲领所能包括的边界之外](../Page/黎曼几何.md "wikilink")。

更多值得注意的例子产生于物理学中。

首先，*n*维[双曲几何](../Page/双曲几何.md "wikilink")，*n*维[de
Sitter空间和](../Page/de_Sitter空间.md "wikilink")（*n*−1）维[逆几何](../Page/逆几何.md "wikilink")（inversive
geometry）都有同构的[自同构群](../Page/自同构群.md "wikilink")，

\[O(n,1)/\mathbb{Z}_2\],

[正确时间的](../Page/正确时间.md "wikilink")[洛伦兹群](../Page/洛伦兹群.md "wikilink")，对于*n*
≥
3的情况。但是这些显然是不同的几何。这里，有些有趣的结果从物理学中进来。已经证明这三个几何中的任何一个中的物理模型是对于某些模型[对偶的](../Page/波粒二象性.md "wikilink")。

还有，*n*维[反de
Sitter空间和有](../Page/反de_Sitter空间.md "wikilink")“洛伦兹”特征数的(*n*−1)维[共形空间](../Page/共形空间.md "wikilink")（conformal
space，和有“欧几里得”特征数的共形空间不同，那种和[逆几何相同](../Page/逆几何.md "wikilink")，对于3维以上情况）有[同构的](../Page/同构.md "wikilink")[自同构群](../Page/自同构群.md "wikilink")，但是不同的几何。再次，在物理中有一些在两个[空间中](../Page/几何.md "wikilink")[对偶的模型](../Page/波粒二象性.md "wikilink")。更多的细节参看[AdS/CFT](../Page/AdS/CFT.md "wikilink")。

所以，在和物理中的对偶性的关系中，爱尔兰根纲领还是可以视为相当丰富的。

## 克莱因几何的推广

克莱因的观点实际上将几何视为两个群（通常是[李群](../Page/李群.md "wikilink")）的商G/H，上节提到相关的各种几何的[同构的](../Page/同构.md "wikilink")[自同构群也就是同构的H](../Page/自同构群.md "wikilink")。因而，上述的爱尔兰根纲领的局限性可以通过引入附加的结构来弥补。换而言之，没有考虑空间的不均匀性，因而只有[齐性空间得到了处理](../Page/齐性空间.md "wikilink")。如果在此基础上引入[联络](../Page/联络.md "wikilink")，则就像引入[度量将](../Page/度量.md "wikilink")[欧几里得几何推广为](../Page/欧几里得几何.md "wikilink")[黎曼几何一样](../Page/黎曼几何.md "wikilink")，我们将推广到了[嘉当几何](../Page/嘉当几何.md "wikilink")。细节可以参看[嘉当联络](../Page/嘉当联络.md "wikilink")。

## 参看

  - [齐性空间](../Page/齐性空间.md "wikilink")

  -
  - [希尔伯特计划](../Page/希尔伯特计划.md "wikilink")

  - [朗蘭茲綱領](../Page/朗蘭茲綱領.md "wikilink")

## 参考

  -
这是一本便宜而且不太难的书，里面有大量引用[索菲斯·李](../Page/索菲斯·李.md "wikilink")、克莱因和[埃利·嘉当的话](../Page/埃利·嘉当.md "wikilink")。

  -
*Mathematische Annalen*, 43 (1893) pp. 63-100（Also: Gesammelte Abh. Vol.
1, Springer, 1921, pp. 460-497).由Mellen Haskell翻译的一个英译本可见于*Bull. N. Y.
Math. Soc* 2 (1892-1893): 215--249.
本纲领的德文版可在美国[密歇根大学的在线收集站上阅读](../Page/密歇根大学.md "wikilink")：

  - "

      -
[Category:古典幾何學](../Category/古典幾何學.md "wikilink")
[Category:群论](../Category/群论.md "wikilink")
[Category:对称](../Category/对称.md "wikilink")
[Category:齐性空间](../Category/齐性空间.md "wikilink")