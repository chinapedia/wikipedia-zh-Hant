**爱德华·威滕**（，姓氏亦译为-{zh-hans:**维腾**;
zh-hant:**威滕**;}-、**维敦**或**惠滕**，），[美国](../Page/美国.md "wikilink")[犹太裔](../Page/犹太.md "wikilink")[数学物理学家](../Page/数学物理.md "wikilink")、[菲尔兹奖得主](../Page/菲尔兹奖.md "wikilink")，也是[普林斯顿高等研究院教授](../Page/普林斯顿高等研究院.md "wikilink")，畢業於[布蘭代斯大學](../Page/布蘭代斯大學.md "wikilink")[歷史系](../Page/歷史系.md "wikilink")、[普林斯頓大學](../Page/普林斯頓大學.md "wikilink")[應用數學博士](../Page/應用數學.md "wikilink")，他是[弦理论和](../Page/弦理论.md "wikilink")[量子场论的顶尖专家](../Page/量子场论.md "wikilink")，创立了[M理论](../Page/M理论.md "wikilink")。爱德华·威滕被视为当代最伟大的物理学家之一，他的一些同行甚至认为他是[爱因斯坦的后继者之一](../Page/爱因斯坦.md "wikilink")\[1\]。[国际数学联盟于](../Page/国际数学联盟.md "wikilink")1990年授予他[菲尔兹奖](../Page/菲尔兹奖.md "wikilink")。爱德华·威滕也是唯一获得这项荣誉的物理学家。

## 生平

威滕生于美国[马里兰州](../Page/马里兰州.md "wikilink")[巴尔的摩](../Page/巴尔的摩_\(马里兰州\).md "wikilink")。父亲[路易斯·威滕](../Page/路易斯·威滕.md "wikilink")（Louis
Witten）是研究[广义相对论的理论物理学家](../Page/广义相对论.md "wikilink")，母亲是洛兰·沃拉克·威滕（Lorraine
W.
Witten）。威滕原先就读于[约翰·霍普金斯大学](../Page/约翰·霍普金斯大学.md "wikilink")，在转学[布兰代斯大学获得](../Page/布兰代斯大学.md "wikilink")[历史系学士学位后](../Page/历史系.md "wikilink")（辅修[语言学](../Page/语言学.md "wikilink")），曾参与[民主党候选人](../Page/民主党.md "wikilink")[乔治·麦戈文的](../Page/乔治·麦戈文.md "wikilink")[总统竞选工作一段短时间](../Page/总统.md "wikilink")，后来乔治·麦戈文败给[理查德·尼克松](../Page/理查德·尼克松.md "wikilink")。

他于1976年获得[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")[应用数学博士学位](../Page/应用数学.md "wikilink")，导师是[戴维·格娄斯](../Page/戴维·格娄斯.md "wikilink")。在此之后，威滕先后任职于[哈佛大学初级研究员](../Page/哈佛大学.md "wikilink")（Junior
Fellow）和普林斯顿大学教授。他现在是[普林斯顿高等研究院的](../Page/普林斯顿高等研究院.md "wikilink")[查尔斯·希莫尼数学物理学教授](../Page/查尔斯·希莫尼.md "wikilink")。

## 研究

[Gross_Witten_Hawking_TIFR_2001.jpg](https://zh.wikipedia.org/wiki/File:Gross_Witten_Hawking_TIFR_2001.jpg "fig:Gross_Witten_Hawking_TIFR_2001.jpg")及[斯蒂芬·霍金](../Page/斯蒂芬·霍金.md "wikilink")\]\]

威滕对[理论物理学做出广泛的贡献](../Page/理论物理学.md "wikilink")，也促进大量重要的数学进展。他已经出版超过350部著作，主要是[量子场论](../Page/量子场论.md "wikilink")、弦理论、拓扑和几何形状的相关领域。威滕被他的同行们广泛认为是20世纪最重要的理论物理学家之一。虽然在某些方面很难将他的深刻贡献加以归类，威滕于物理方面的贡献侧重于[超对称](../Page/超对称.md "wikilink")，于数学方面则是拓扑结构。

威滕在物理学的早期贡献之一是所谓[级列问题](../Page/级列问题.md "wikilink")（hierarchy
problem）的解决方案。粒子物理学的标淮模型预言一种被称为[希格斯玻色子的存在](../Page/希格斯玻色子.md "wikilink")。然而它的质量似乎比模型预测的数字还要轻得多。威滕认为，超对称破坏的机制提供了级列问题一种的自然解释。在超对称理论中，[威滕指数](../Page/威滕指数.md "wikilink")（Witten
index）用来判断超对称是否遭到破坏。威滕在超对称规范理论仍然继续做出开创性的贡献。他与[普林斯顿高等研究院](../Page/普林斯顿高等研究院.md "wikilink")[内森·塞伯格](../Page/内森·塞伯格.md "wikilink")（Nathan
Seiberg）发展出[塞伯格-维腾理论](../Page/塞伯格-维腾理论.md "wikilink")。

威滕显然是弦理论的代表人物，这是一个用来说明所有自然力的理论。甚至早在1984年，威滕为[重力异常做出重要贡献](../Page/重力异常.md "wikilink")，为第一次弦理论革命铺平了道路。1990年代中期在南加州大学弦理论会议中，威滕推测存在一个统一的理论还没有被发现，称之为M理论。M理论可能是宇宙物理理论最根本的理论。[英国物理学家](../Page/英国.md "wikilink")[斯蒂芬·霍金在他的著作](../Page/斯蒂芬·霍金.md "wikilink")《[大设计](../Page/大设计.md "wikilink")》（The
Grand Design）中，认为M理论可能是宇宙的终极理论。

爱德华·威滕其他重要物理学的贡献包括規範/重力對偶。1997年，阿根廷[理论物理学家](../Page/理论物理学.md "wikilink")[胡安·马尔达西那首先提出了在](../Page/胡安·马尔达西那.md "wikilink")[反德西特空间背景下某些](../Page/反德西特空间.md "wikilink")[超引力理论和边界上](../Page/超引力理论.md "wikilink")[共形场论的对偶关系](../Page/共形场论.md "wikilink")，即[AdS/CFT对偶猜想](../Page/AdS/CFT对偶.md "wikilink")。这一革命性的发现为过去15年中佔据了理论物理的主导地位，对于威滕的研究产生非常重要的影响。

国际数学联盟在1990年授予威滕[菲尔兹奖](../Page/菲尔兹奖.md "wikilink")\[2\]\[3\]，成为第一位获得该奖项的物理学家（也是唯一一位）。他对纯数学方面的研究影响深远，例如他使用[琼斯多项式](../Page/琼斯多项式.md "wikilink")（Jones
Polynomial）来解释[陈-西蒙斯理论](../Page/陈-西蒙斯理论.md "wikilink")」（Chern-Simons
theory）。这项研究对于[低维拓扑结构有深远影响](../Page/低维拓扑结构.md "wikilink")，并推导出[量子不变量](../Page/量子不变量.md "wikilink")。

威滕被认为是他的世代中最优秀的物理学家\[4\]，也被认为是世界上最伟大的物理学家之一，也许甚至是爱因斯坦的后继者\[5\]。在1995年，他在[南加州大学会议中提出](../Page/南加州大学.md "wikilink")[M理论](../Page/M理论.md "wikilink")，并用它来解释一些以前观察到的现象，在弦论中引发所谓的[第二次超弦革命](../Page/第二次超弦革命.md "wikilink")。

## 评价

威滕具有深刻的物理直觉和高超的数学能力。他专长量子场论、弦理论和[拓扑与](../Page/拓扑.md "wikilink")[几何相关的范围](../Page/几何.md "wikilink")。他的主要贡献包括广义相对论的[正能量定理证明](../Page/正能量定理.md "wikilink")、[超对称和](../Page/超对称.md "wikilink")[莫尔斯理论](../Page/莫尔斯理论.md "wikilink")、[拓扑量子场论](../Page/拓扑量子场论.md "wikilink")、[超弦紧化](../Page/超弦紧化.md "wikilink")、[镜像对称](../Page/镜像对称.md "wikilink")、[超对称规范场论和对](../Page/超对称规范场论.md "wikilink")[M理论存在性的猜想](../Page/M理论.md "wikilink")。

威滕受到许多同行的广泛赞赏，数学家[迈克尔·阿蒂亚曾说](../Page/迈克尔·阿蒂亚.md "wikilink")\[6\]：

威滕获得很多奖项，包括[麦克阿瑟基金](../Page/麦克阿瑟基金会.md "wikilink")（1982年）、[菲尔兹奖](../Page/菲尔兹奖.md "wikilink")（1990年）、Nemmers数学奖、毕达哥拉斯奖\[7\]、[庞加莱奖](../Page/庞加莱奖.md "wikilink")（2006年）、[克拉福德奖](../Page/克拉福德奖.md "wikilink")（2008年）、[洛伦兹奖章](../Page/洛伦兹奖章.md "wikilink")（2010年）、[艾萨克·牛顿奖章](../Page/艾萨克·牛顿奖章.md "wikilink")（2010年）和[美国国家科学奖章](../Page/美国国家科学奖章.md "wikilink")（2002年）\[8\]。他被选入《[时代杂志](../Page/时代杂志.md "wikilink")》2004年影响最大的100位人士中。爱德华·威滕是当代的物理学家中[H指数最高的一位](../Page/H指数.md "wikilink")\[9\]\[10\]。

## 参考资料

## 外部链接

  - [威滕在研究院的网页](http://www.sns.ias.edu/~witten/)

  - [Publications on
    ArXiv](http://arxiv.org/find/hep-th/1/au:+Witten_E/0/1/0/all/0/1)

  - [Witten theme tree on
    arxiv.org](http://xstructure.inr.ac.ru/x-bin/search_e3.py?archive=all&search_year=all&au=Witten_E&per_page=100&skip=0)

  - [Futurama episode
    information](http://www.gotfuturama.com/Information/Capsules/1ACV11.txt)

  -
  -
  - [Institute of Physics
    profile](http://www.iop.org/about/awards/international/page_43947.html)

[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:21世纪数学家](../Category/21世纪数学家.md "wikilink")
[Category:20世纪物理学家](../Category/20世纪物理学家.md "wikilink")
[Category:21世纪物理学家](../Category/21世纪物理学家.md "wikilink")
[Category:菲尔兹奖获得者](../Category/菲尔兹奖获得者.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:英国皇家学会院士](../Category/英国皇家学会院士.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:理论物理学家](../Category/理论物理学家.md "wikilink")
[Category:犹太科学家](../Category/犹太科学家.md "wikilink")
[Category:普林斯顿高等研究院教职员](../Category/普林斯顿高等研究院教职员.md "wikilink")
[Category:哈佛大学教师](../Category/哈佛大学教师.md "wikilink")
[Category:普林斯顿大学校友](../Category/普林斯顿大学校友.md "wikilink")
[Category:约翰·霍普金斯大学校友](../Category/约翰·霍普金斯大学校友.md "wikilink")
[Category:布兰戴斯大学校友](../Category/布兰戴斯大学校友.md "wikilink")
[Category:美国犹太人](../Category/美国犹太人.md "wikilink")
[Category:马里兰州人](../Category/马里兰州人.md "wikilink")
[Category:量子重力研究者](../Category/量子重力研究者.md "wikilink")
[Category:亨利·庞加莱奖获得者](../Category/亨利·庞加莱奖获得者.md "wikilink")
[Category:突破奖获得者](../Category/突破奖获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦奖章获得者](../Category/阿尔伯特·爱因斯坦奖章获得者.md "wikilink")
[Category:洛伦兹奖章获得者](../Category/洛伦兹奖章获得者.md "wikilink")
[Category:丹尼·海涅曼数学物理奖获得者](../Category/丹尼·海涅曼数学物理奖获得者.md "wikilink")
[Category:克拉福德奖获得者](../Category/克拉福德奖获得者.md "wikilink")
[Category:ICTP狄拉克奖章获得者](../Category/ICTP狄拉克奖章获得者.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦世界科学奖获得者](../Category/阿尔伯特·爱因斯坦世界科学奖获得者.md "wikilink")
[Category:奥斯卡·克莱因纪念讲座](../Category/奥斯卡·克莱因纪念讲座.md "wikilink")
[Category:弗雷德里克·埃瑟·内默斯数学奖获得者](../Category/弗雷德里克·埃瑟·内默斯数学奖获得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")

1.
2.  ["On the work of Edward
    Witten"](http://www.emis.ams.org/mirror/IMU/medals/Fields/1990/Witten/page1.html)
    (when being awarded the Field's medal)
3.  [National Medal of Science Awarded to Institute for Advanced Study
    Physicist Edward
    Witten](http://www.ias.edu/newsroom/announcements/view/witten.html)
    , Institute for Advanced Study announcement, 22 October 2003
4.  [The Man Who Led the Second Superstring
    Revolution](http://discovermagazine.com/2008/dec/13-the-man-who-led-the-second-superstring-revolution),
    *Discover Magazine*, 13 November 2008
5.  [The Elegant Universe: Welcome to the 11th
    Dimension](http://www.pbs.org/wgbh/nova/transcripts/3014_elegant.html),
    PBS NOVA transcript
6.
7.
8.  ["Edward
    Witten"](http://www.nsf.gov/od/nms/recip_details.cfm?recip_id=5000000000422),
    The President's National Medal of Science: Recipient Details.
9.  Philip Ball, [Index aims for fair ranking of
    scientists](http://www.nature.com/nature/journal/v436/n7053/full/436900a.html),
    *Nature* 436, 900 (18 August 2005)
10. [The H-Index: The Hot Topic in Information
    Science](http://www.timeshighereducation.co.uk/story.asp?storyCode=401175&sectioncode=26),
    *Times Higher Education*, 13 March 2008