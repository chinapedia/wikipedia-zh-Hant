**爱德华·哈盖鲁普·格里格**（，），[挪威作曲家](../Page/挪威.md "wikilink")，浪漫主义音乐时期的重要作曲家之一。

爱德华·格里格出生于[卑尔根](../Page/卑尔根.md "wikilink")，祖先是[蘇格蘭人](../Page/蘇格蘭.md "wikilink")，具有民主主义、爱国主义思想倾向，作品大多以风俗生活、北欧民间传说、文学著作或自然景物为题材，具有鲜明的民族风格，是挪威民族乐派的人物。代表作有《》、《[培尔·金特组曲](../Page/培尔·金特组曲.md "wikilink")》、《霍爾堡組曲》、《钢琴抒情组曲》等等。

## 作品节选

  - 《在秋天》（Concert Overture In Autumn）
  - 《悲歌旋律》（Two Elegiac Melodies）
  - 《[皮爾金組曲](../Page/皮爾金組曲.md "wikilink")》
  - 《十字军战士西古尔德》（Sigurd Jorsalfar）
  - 《》
  - 《霍爾堡組曲》（Holberg Suite）
  - 《天鵝》（A Swan）
  - 《朝露》（Morning Dew）

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

## 外部链接

  - [葛利格曲目列表](http://www.classicalmusicdb.com/composers/view/10)

  -
  - [格里格介绍及作品介绍专页](http://www.sin80.com/artist/grieg)

[Category:挪威作曲家](../Category/挪威作曲家.md "wikilink")
[Category:浪漫主义作曲家](../Category/浪漫主义作曲家.md "wikilink")
[Category:卑爾根人](../Category/卑爾根人.md "wikilink")