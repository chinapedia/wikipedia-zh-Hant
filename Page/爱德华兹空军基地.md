**爱德华兹空军基地**（；），[美国著名](../Page/美国.md "wikilink")[空军基地之一](../Page/空军基地.md "wikilink")，以降落[航天飞机而闻名](../Page/航天飞机.md "wikilink")。

爱德华兹空军基地位于美国的[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")，离[洛杉矶约](../Page/洛杉矶.md "wikilink")150公里。基地创建于1930年代，曾经是[美国陸军航空隊](../Page/美国陸军航空隊.md "wikilink")（USAAC）及[美國陸軍航空軍的训练中心之一](../Page/美國陸軍航空軍.md "wikilink")。现基地内设有[美国空军飞行试验中心](../Page/美国空军飞行试验中心.md "wikilink")（Air
Force Flight Test Center）、[-{zh-hans:美国宇航局;
zh-hant:太空總署;}-旗下的德莱顿飞行研究中心等机构](../Page/NASA.md "wikilink")。

爱德华兹空军基地作为NASA航天飞机第一备降机场（首选是位于[佛罗里达州的](../Page/佛罗里达州.md "wikilink")[肯尼迪航天中心](../Page/肯尼迪航天中心.md "wikilink")），自1981年[哥倫比亞號太空梭首降以来到](../Page/哥倫比亞號太空梭.md "wikilink")2005年夏的[发现号](../Page/发现号航天飞机.md "wikilink")[STS-114任务](../Page/STS-114.md "wikilink")，在NASA的111次航天飞机降落中，已有49次降落在此。同时，爱德华兹空军基地也是美国空军重要的试飞基地之一，包括美国最新的“X”系列飞机都在此试飞。由于涉及国防机密，所以基地的保密程度相当高。
[Edwards_afm.jpg](https://zh.wikipedia.org/wiki/File:Edwards_afm.jpg "fig:Edwards_afm.jpg")
[Edwards_AFB_control_tower.jpg](https://zh.wikipedia.org/wiki/File:Edwards_AFB_control_tower.jpg "fig:Edwards_AFB_control_tower.jpg")
{{-}}

## 外部链接

  - [爱德华兹空军基地官方頁面](http://www.edwards.af.mil/)

[Category:美国空军基地](../Category/美国空军基地.md "wikilink")
[Category:加利福尼亞州机场](../Category/加利福尼亞州机场.md "wikilink")
[Category:加利福尼亚州军事](../Category/加利福尼亚州军事.md "wikilink")
[Category:1933年启用的机场](../Category/1933年启用的机场.md "wikilink")
[愛德華空軍基地](../Category/愛德華空軍基地.md "wikilink")