**爱德华湖**（；）位于[刚果民主共和国和](../Page/刚果民主共和国.md "wikilink")[乌干达边境](../Page/乌干达.md "wikilink")，东非大裂谷的西部分支-艾伯丁裂谷里，其北岸距离[赤道仅数公里](../Page/赤道.md "wikilink")，是[非洲](../Page/非洲.md "wikilink")[大湖地区七大湖中面积最小的湖泊](../Page/大湖地区.md "wikilink")。

爱德华湖由断层陷落而成；湖面海拔912米，南北长76公里，东西宽51公里，面积2150平方公里；最深117米；有许多山溪注入，湖水北经[塞姆利基河注入](../Page/塞姆利基河.md "wikilink")[艾伯特湖](../Page/艾伯特湖.md "wikilink")。

爱德华湖由[英国探险家](../Page/英国.md "wikilink")[亨利·莫顿·斯坦利命名](../Page/亨利·莫顿·斯坦利.md "wikilink")，以表达对[威尔士亲王](../Page/威尔士亲王.md "wikilink")，王子阿尔伯特爱德华的敬意，也就是后来的[英国国王](../Page/英国国王.md "wikilink")[爱德华七世](../Page/爱德华七世_\(英国\).md "wikilink")。

[Category:非洲跨國湖泊](../Category/非洲跨國湖泊.md "wikilink")
[Category:東非大裂谷湖泊](../Category/東非大裂谷湖泊.md "wikilink")
[Category:非洲大湖地區](../Category/非洲大湖地區.md "wikilink")
[Category:尼羅河](../Category/尼羅河.md "wikilink")
[Category:刚果民主共和国湖泊](../Category/刚果民主共和国湖泊.md "wikilink")
[Category:乌干达湖泊](../Category/乌干达湖泊.md "wikilink")
[Category:剛果民主共和國-烏干達邊界](../Category/剛果民主共和國-烏干達邊界.md "wikilink")