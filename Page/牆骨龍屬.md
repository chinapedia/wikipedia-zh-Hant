**牆骨龍屬**（[學名](../Page/學名.md "wikilink")：*Tichosteus*）是[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，是種[草食性恐龍](../Page/草食性.md "wikilink")。目前只有發現[脊椎](../Page/脊椎.md "wikilink")[化石發現於](../Page/化石.md "wikilink")[美國](../Page/美國.md "wikilink")[科羅拉多州的](../Page/科羅拉多州.md "wikilink")[莫里遜組](../Page/莫里遜組.md "wikilink")，地質年代為[侏羅紀晚期的](../Page/侏羅紀.md "wikilink")[啟莫里階](../Page/啟莫里階.md "wikilink")。

在1877年，[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward Drinker
Cope）命名[模式種](../Page/模式種.md "wikilink")*T.
lucasanus*\[1\]。屬名在[古希臘文意為](../Page/古希臘文.md "wikilink")「牆壁骨頭」，意指脊椎內部中空，而兩側骨璧沒有通往氣囊的穿孔；種名則是以發現化石的當地學校院長Oramel
W.
Lucas為名。他在[阿肯色河附近發現這兩個背部脊椎](../Page/阿肯色河.md "wikilink")（編號AMNH 5770）。這個脊椎長約2.3公分，科普估計其完整身長接近[短吻鱷](../Page/短吻鱷.md "wikilink")。之後，Charles
Craig
Mook估計其身長相當於[狼](../Page/狼.md "wikilink")。這些脊椎來自於亞成年個體，椎體與[神經弓之間可辨識出未癒合骨縫](../Page/神經弓.md "wikilink")。

隔年，愛德華·德林克·科普命名牆骨龍的第二個[物種](../Page/物種.md "wikilink")，*T.
aequifacies*\[2\]，種名在[拉丁文意為](../Page/拉丁文.md "wikilink")「平衡的面孔」，意指與模式種相比，這些新脊椎較具對稱性。[正模標本](../Page/正模標本.md "wikilink")（編號AMNH 5771）也是包含兩節脊椎。

科普本人對於牆骨龍的分類非常困惑，當時僅歸類於[爬行動物](../Page/爬行動物.md "wikilink")，而沒有做出更詳細的分類。之後研究多認為牆骨龍是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。在20世紀末期，有研究人員提出牆骨龍有可能是屬於原始[禽龍類](../Page/禽龍類.md "wikilink")\[3\]，但兩個種則有可能是[疑名](../Page/疑名.md "wikilink")，或是[鳥腳下目的分類未定屬](../Page/鳥腳下目.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [DinoRuss](http://web.me.com/dinoruss/de_4/5a943cb.htm)
  - [AndroidWorld.com](http://www.androidworld.com/prod87.htm)

[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:禽龍類](../Category/禽龍類.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  E.D. Cope, 1877, "On reptilian remains from the Dakota Beds of
    Colorado", *Proceedings of the American Philosophical Society*
    **17**(100): 193-196
2.  E.D. Cope, 1878, "Descriptions of new extinct Vertebrata from the
    Upper Tertiary and Dakota Formations", *Bulletin of the United
    States Geological and Geographical Survey of the Territories*,
    **4**(2): 379-396
3.  Weishampel, D. B., Dodson P., and Osmolska H., 1990, *The
    Dinosauria*, California University Press, p. 531