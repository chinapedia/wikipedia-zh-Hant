**版纳鱼螈（*Ichthyophis
bannanicus*）**是[两栖纲](../Page/两栖纲.md "wikilink")[鱼螈科的一种动物](../Page/鱼螈科.md "wikilink")，为[中國特有的一种](../Page/中國.md "wikilink")[蚓螈](../Page/蚓螈.md "wikilink")。

## 特征

身体呈[蠕虫状](../Page/蠕虫.md "wikilink")，长约40厘米；头部扁平如铲状，眼呈点状，隐藏于皮膜下；皮肤光滑，多黏液；深棕色背部，有蜡光，体侧从眼后至肛侧各有一条棕黄色纵带，浅棕色腹部；颈部腹面有3条颈沟；躯干部有环褶328～381个，尾巴很短，仅有4个环褶；身体前半部的环褶之间有细鳞两行，后部三或四行。

## 分布

已知分布於[雲南](../Page/雲南.md "wikilink")、[廣西和](../Page/廣西.md "wikilink")[廣東](../Page/廣東.md "wikilink")，並可能分布於[寮國](../Page/寮國.md "wikilink")、[緬甸和](../Page/緬甸.md "wikilink")[越南](../Page/越南.md "wikilink")。

版纳鱼螈棲息於[亞熱帶或](../Page/亞熱帶.md "wikilink")[熱帶的潮濕低地森林](../Page/熱帶.md "wikilink")、河流、間歇河、農場、鄉村花園、嚴重砍伐的前森林、灌溉地及季節氾濫農地。棲息地的减少是其面临的主要威脅。

## 參考

  - Zhigang, Y., Wenhao, C., Wilkinson, M. & Gower, D. 2004.
    [Ichthyophis
    bannanicus](http://www.iucnredlist.org/search/details.php/59609/all).
    [2006 IUCN Red List of Threatened
    Species.](http://www.iucnredlist.org) Downloaded on 22 July 2007.

[IB](../Category/IUCN無危物種.md "wikilink")
[bannanicus](../Category/魚螈屬.md "wikilink")
[IB](../Category/云南动物.md "wikilink")