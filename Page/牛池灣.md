[Ngau_Chi_Wan_Tsuen_201704.jpg](https://zh.wikipedia.org/wiki/File:Ngau_Chi_Wan_Tsuen_201704.jpg "fig:Ngau_Chi_Wan_Tsuen_201704.jpg")
[香港牛池灣82_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:香港牛池灣82_-_panoramio.jpg "fig:香港牛池灣82_-_panoramio.jpg")
[St._Joseph's_Home_for_the_Aged_outlook_2012.JPG](https://zh.wikipedia.org/wiki/File:St._Joseph's_Home_for_the_Aged_outlook_2012.JPG "fig:St._Joseph's_Home_for_the_Aged_outlook_2012.JPG")計劃於[聖若瑟安老院一帶興建住宅](../Page/聖若瑟安老院.md "wikilink")，其中安老院主樓、中庭別墅、宿舍及門樓均獲得保留，不過項目目前仍在策劃中\]\]
**牛池灣**（）為一[香港地名](../Page/香港.md "wikilink")、常被謬稱為**彩虹**。由於區內[港鐵車站以](../Page/港鐵車站.md "wikilink")「[彩虹](../Page/彩虹站.md "wikilink")」（）命名，故「彩虹」被一些[香港人廣泛作地名使用](../Page/香港人.md "wikilink")，惟其並非正式地名。牛池灣乃昔日[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[黃大仙區](../Page/黃大仙區.md "wikilink")[斧山下的一個](../Page/斧山.md "wikilink")[海灣](../Page/海灣.md "wikilink")，附近地區因而以此為名，泛指[斧山道及](../Page/斧山道.md "wikilink")[清水灣道之間的地方及](../Page/清水灣道.md "wikilink")[彩虹邨](../Page/彩虹邨.md "wikilink")，彩虹邨位於牛池灣西面部分，毗鄰的[坪石邨則位於](../Page/坪石邨.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")[佐敦谷西北面](../Page/佐敦谷.md "wikilink")。在[都市規劃當中](../Page/都市規劃.md "wikilink")，原先的[牛池灣鄉沒有重建](../Page/牛池灣鄉.md "wikilink")。現時牛池灣內的[聖若瑟安老院正在重建](../Page/聖若瑟安老院.md "wikilink")。

## 歷史

清代[嘉慶](../Page/嘉慶.md "wikilink")24年（公元1819年）刊行的「新安縣誌〉，已經清楚標示牛池灣。至於地名來源有2種說法，據說牛池灣古稱「**牛尿灣**」或「**牛屎灣**」，因為以前該地是放[牛的](../Page/牛.md "wikilink")。由於其名字難聽，所以後來就雅化為牛池灣，但此說法較不確實。另一說法，因[牛池灣村的西北面有一大水池](../Page/牛池灣村.md "wikilink")，因水池形狀像一隻牛睡於水中，水池名為牛池而村莊亦位於牛頭角海灣之北，故命名為牛池灣村\[1\]\[2\]。

在1970年代初期，[東華三院曾計劃在牛池灣興建](../Page/東華三院.md "wikilink")[非牟利的](../Page/非牟利.md "wikilink")[殯儀館](../Page/殯儀館.md "wikilink")（即[鑽石山殯儀館](../Page/鑽石山殯儀館.md "wikilink")）\[3\]。

## 地點

<File:Ngau> Chi Wan Civic Centre
201408.jpg|[牛池灣文娛中心及市政大廈](../Page/牛池灣文娛中心.md "wikilink")
<File:Ngau> Chi Wan Fire Station - 11-12-2006 4-50-06.jpg|牛池灣消防局 HK
NgauChiWanVillage Archway.JPG|牛池灣鄉牌匾 <File:Da> Wang Gong Temple, Ngau
Chi Wan - 11-12-2006 4-43-18.jpg|牛池灣村大王宮 <File:Lung> Cheung Road near
Ngau Chi Wan Village Refuse Collection Point (Hong
Kong).jpg|牛池灣村垃圾收集站，從[龍翔道向東望後方樓宇](../Page/龍翔道.md "wikilink")，從左至右分別為威豪花園、怡發花園及怡富花園。

## 彩雲邨交通

<div class="NavFrame collapsed" style="background: #FFFFFF; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

**接駁交通列表**

</div>

<div class="NavContent" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[彩虹站B出入口](../Page/彩虹站.md "wikilink")
    (步行約15至20分鐘)

<!-- end list -->

  - [彩雲巴士總站](../Page/彩雲巴士總站.md "wikilink")

<!-- end list -->

  - [清水灣道](../Page/清水灣道.md "wikilink")

<!-- end list -->

  - [新清水灣道](../Page/新清水灣道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - [西貢至](../Page/西貢.md "wikilink")[旺角線](../Page/旺角.md "wikilink")\[4\]
  - 西貢至[觀塘線](../Page/觀塘.md "wikilink")\[5\]
  - 西貢至[銅鑼灣線](../Page/銅鑼灣.md "wikilink") (下午至晚上服務)\[6\]
  - 荃灣荃灣街市街　＞　坪石、彩雲及順利順安 下午繁忙時段直通車

<!-- end list -->

  - [彩雲（豐盛街）巴士總站](../Page/彩雲（豐盛街）巴士總站.md "wikilink")/[豐盛街](../Page/豐盛街.md "wikilink")

<!-- end list -->

  - [牛池灣街](../Page/牛池灣街.md "wikilink")

<!-- end list -->

  - 鄰近[彩德邨](../Page/彩德邨.md "wikilink")/[彩興苑](../Page/彩興苑.md "wikilink")

[13X](../Page/九龍巴士13X線.md "wikilink") 特別班次

  - 往來彩德邨及[彩盈邨的行人隧道](../Page/彩盈邨.md "wikilink")

</div>

</div>

## 坪石邨交通

<div class="NavFrame collapsed" style="background: #FFFFFF; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

**接駁交通列表**

</div>

<div class="NavContent" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[彩虹站](../Page/彩虹站.md "wikilink")（A出入口）

<!-- end list -->

  - [清水灣道](../Page/清水灣道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - [西貢至](../Page/西貢.md "wikilink")[旺角線](../Page/旺角.md "wikilink")\[7\]
  - 西貢至[觀塘線](../Page/觀塘.md "wikilink")\[8\]
  - 西貢至[銅鑼灣線](../Page/銅鑼灣.md "wikilink") (下午至晚上服務)\[9\]

<!-- end list -->

  - [坪石巴士總站](../Page/坪石巴士總站.md "wikilink")

<!-- end list -->

  - 坪石邨專線小巴總站

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至坪石線\[10\]
  - 觀塘至西貢線\[11\]

</div>

</div>

## 彩虹邨交通

<div class="NavFrame collapsed" style="background: #FFFFFF; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

**接駁交通列表**

</div>

<div class="NavContent" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[彩虹站](../Page/彩虹站.md "wikilink")（C出入口）

<!-- end list -->

  - [太子道東](../Page/太子道東.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink") (大部分只在往觀塘方向停站)

<!-- end list -->

  - 觀塘至[青山道線](../Page/青山道.md "wikilink")\[12\]
  - 觀塘至青山道線\[13\] (通宵線)
  - 觀塘至[美孚線](../Page/美孚.md "wikilink")\[14\]
  - 觀塘至[佐敦道](../Page/佐敦道.md "wikilink")/[土瓜灣線](../Page/土瓜灣.md "wikilink")\[15\]
    (24小時線)
  - 牛頭角至旺角線\[16\]
  - 牛頭角至佐敦道線\[17\] (上午線)
  - 九龍灣至旺角線\[18\]
  - 西貢至旺角線\[19\]
  - 香港仔至觀塘線\[20\] (黃昏線)
  - 觀塘至西環線\[21\] (上午線)
  - 觀塘至銅鑼灣線\[22\] (黃昏至通宵線)
  - 中環至觀塘線\[23\] (晚上至通宵線)
  - [香港仔至觀塘線](../Page/香港仔.md "wikilink")\[24\] (黃昏線)
  - [紅磡至觀塘線](../Page/紅磡.md "wikilink") (24小時服務)\[25\]

<!-- end list -->

  - [龍翔道](../Page/龍翔道.md "wikilink")

<!-- end list -->

  - [彩虹巴士總站](../Page/彩虹巴士總站.md "wikilink")

<!-- end list -->

  - [彩虹道](../Page/彩虹道.md "wikilink")（彩虹邨通道一段）

<!-- end list -->

  - [斧山道](../Page/斧山道.md "wikilink")

</div>

</div>

## 區議會議席分佈

為方便比較，以下列表以[大老山隧道以東](../Page/大老山隧道.md "wikilink")、[大老山以南](../Page/大老山.md "wikilink")、[飛鵝山以西](../Page/飛鵝山.md "wikilink")、[太子道東](../Page/太子道東.md "wikilink")、[清水灣道以北為範圍](../Page/清水灣道.md "wikilink")(包括[彩雲邨](../Page/彩雲邨.md "wikilink"))。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/蒲崗村道.md" title="wikilink">蒲崗村道以東</a>、<a href="../Page/豐盛街.md" title="wikilink">豐盛街以北</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大老山隧道.md" title="wikilink">大老山隧道以東</a>、<a href="../Page/蒲崗村道.md" title="wikilink">蒲崗村道</a>、<a href="../Page/斧山道.md" title="wikilink">斧山道</a>、<a href="../Page/鳳德道.md" title="wikilink">鳳德道以北</a></p></td>
<td><p><a href="../Page/龍星.md" title="wikilink">鑽石山選區的一部分</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒲崗村道.md" title="wikilink">蒲崗村道以東</a>、<a href="../Page/斧山道.md" title="wikilink">斧山道以東</a>、<a href="../Page/豐盛街.md" title="wikilink">豐盛街以南</a>、<a href="../Page/龍翔道.md" title="wikilink">龍翔道以北</a>、<a href="../Page/清水灣道.md" title="wikilink">清水灣道以北</a>、<a href="../Page/斧山.md" title="wikilink">斧山以西</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斧山道.md" title="wikilink">斧山道</a>、<a href="../Page/龍翔道.md" title="wikilink">龍翔道</a>、<a href="../Page/太子道.md" title="wikilink">太子道東</a>、<a href="../Page/觀塘繞道.md" title="wikilink">觀塘繞道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斧山.md" title="wikilink">斧山以東</a>、<a href="../Page/大老山.md" title="wikilink">大老山以南</a>、<a href="../Page/飛鵝山.md" title="wikilink">飛鵝山以西</a>、<a href="../Page/清水灣道.md" title="wikilink">清水灣道</a>、<a href="../Page/新清水灣道.md" title="wikilink">新清水灣道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參考資料

{{-}}

{{-}}  {{-}}

[Category:香港已消失海灣](../Category/香港已消失海灣.md "wikilink")
[牛池灣](../Category/牛池灣.md "wikilink")
[Category:黃大仙區](../Category/黃大仙區.md "wikilink")
[Category:清水灣道](../Category/清水灣道.md "wikilink")
[Category:九龍十三鄉](../Category/九龍十三鄉.md "wikilink")
[Category:新九龍](../Category/新九龍.md "wikilink")

1.  《九龍街道命名考源》梁濤 著，第90頁，市政局出版，1993年
2.  《香港歷史文化小百科16－趣談九龍街道》 爾東 著，第52-53頁，明報出版社，2004年11月，ISBN 962-8871-46-3
3.
4.  [西貢市中心　—　旺角登打士街](http://www.16seats.net/chi/rmb/r_kn96.html)
5.  [西貢市中心　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kn92.html)
6.  [西貢市中心　—　銅鑼灣登龍街](http://www.16seats.net/chi/rmb/r_hn98.html)
7.  [西貢市中心　—　旺角登打士街](http://www.16seats.net/chi/rmb/r_kn96.html)
8.  [西貢市中心　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kn92.html)
9.  [西貢市中心　—　銅鑼灣登龍街](http://www.16seats.net/chi/rmb/r_hn98.html)
10. [荃灣荃灣街市街　—　新蒲崗及坪石](http://www.16seats.net/chi/rmb/r_kn42.html)
11. [西貢市中心　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kn92.html)
12. [觀塘協和街　—　青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
13. [觀塘及黃大仙　—　青山道](http://www.16seats.net/chi/rmb/r_k22.html)
14. [觀塘協和街　—　美孚](http://www.16seats.net/chi/rmb/r_k64.html)
15. [觀塘同仁街　—　佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
16. [牛頭角站　—　旺角登打士街](http://www.16seats.net/chi/rmb/r_k24.html)
17. [牛頭角站　—　旺角登打士街](http://www.16seats.net/chi/rmb/r_k24.html)
18. [旺角 \>
    九龍灣(Megabox)](http://www.i-busnet.com/minibus/minibus_red/mok_megabox.php)
19. [西貢市中心　—　旺角登打士街](http://www.16seats.net/chi/rmb/r_kn96.html)
20. [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
21. [觀塘宜安街　＞　西環卑路乍街,
    西環修打蘭街　＞　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
22. [銅鑼灣鵝頸橋　—　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
23. [中環／灣仔　＞　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)
24. [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
25. [紅磡差館里　＞　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_k15.html)