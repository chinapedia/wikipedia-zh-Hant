**青铜鼻学院**是[英國](../Page/英國.md "wikilink")[牛津大學的一個學院](../Page/牛津大學.md "wikilink")。名字青銅鼻被認為起源於一個鼻子形狀的青銅門把，垂懸在學院大廳的[高桌上](../Page/高桌.md "wikilink")。

## 参考文献

## 外部連結

  - [Official HCR website](http://hcr.bnc.ox.ac.uk/)

[Category:1500年代創建的教育機構](../Category/1500年代創建的教育機構.md "wikilink")
[Category:牛津大学布雷齐诺斯学院](../Category/牛津大学布雷齐诺斯学院.md "wikilink")