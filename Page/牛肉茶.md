**牛肉茶**是[香港一種的](../Page/香港.md "wikilink")[飲料](../Page/飲料.md "wikilink")，在過去的[茶餐廳常見](../Page/茶餐廳.md "wikilink")\[1\]，近年已少見\[2\]。

正宗的做法大概如下：
先將[牛肉用清](../Page/牛肉.md "wikilink")[水洗乾淨切片](../Page/水.md "wikilink")，加入清水一杯，浸二小時，將浸著水的牛肉放入[雪櫃](../Page/雪櫃.md "wikilink")。
將牛肉及浸牛肉的水倒入煲內，加[薑](../Page/薑.md "wikilink")、[杞子](../Page/杞子.md "wikilink")、三碗滾水用慢火燉一小時半，加少許[鹽調味](../Page/鹽.md "wikilink")，再燉約半小時，即可飲用。

但是在香港的茶餐廳提供的牛肉茶是以一種名稱為「」的調味料沖熱開水而成。這種「保衞爾牛肉汁」在[英國](../Page/英國.md "wikilink")[狂牛症流行後](../Page/狂牛症.md "wikilink")，由於香港政府禁止入口英國牛肉產品，而2004年保衛爾牛肉汁推出不含牛肉、成分為[酵母萃取物](../Page/酵母萃取物.md "wikilink")、可供[素食者食用的版本](../Page/素食.md "wikilink")，此後於香港出售的，而不再是牛肉製品了。但在英國本地及世界大部份國家有出售保衛爾牛肉汁的國家和地區仍然可以買到含有牛肉成分的保衛爾。

[普法戰爭期間](../Page/普法戰爭.md "wikilink")，當時[法國國王](../Page/法國國王.md "wikilink")[拿破崙三世](../Page/拿破崙三世.md "wikilink")（Napoleon
III）為了給[法國軍隊營養品](../Page/法國.md "wikilink")，向[英國購買大量的](../Page/英國.md "wikilink")[牛肉](../Page/牛肉.md "wikilink")[罐頭](../Page/罐頭.md "wikilink")，可是數量實在太多，[英國一時之間沒辦法供應](../Page/英國.md "wikilink")，只好研發將[牛肉](../Page/牛肉.md "wikilink")[罐頭提煉出精華](../Page/罐頭.md "wikilink")，製作成為牛肉精，即為最早期的「」，此舉解決了[法國軍隊的營養補給](../Page/法國.md "wikilink")，最後[法國雖然戰敗](../Page/法國.md "wikilink")，但是「」開始變成當代流行的營養品。[20世紀的兩次](../Page/20世紀.md "wikilink")[世界大戰](../Page/世界大戰.md "wikilink")，「」成為英軍最佳營養品，戰後在[亞太地區的流行](../Page/亞太地區.md "wikilink")，[香港的](../Page/香港.md "wikilink")[茶餐廳將](../Page/茶餐廳.md "wikilink")「」稱作「保衛爾牛肉茶」。

## 參考資料

{{-}}

[Category:香港飲料](../Category/香港飲料.md "wikilink")
[Category:茶外茶](../Category/茶外茶.md "wikilink")
[Category:牛肉料理](../Category/牛肉料理.md "wikilink")

1.  [茶記飲品知多啲](http://orientaldaily.on.cc/cnt/lifestyle/20150614/00321_003.html)，東方日報，2015年6月14日
2.  [現代牛肉汁
    加胡椒粉辟腥](http://hk.apple.nextmedia.com/supplement/food/art/20141230/18984895)，蘋果日報，2014年12月30日