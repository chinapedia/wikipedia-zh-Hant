**牛至**（[學名](../Page/學名.md "wikilink")：**）（英文：、）又名**滇香薷**、**奧勒岡葉**、**披薩草**、**香芹酚**、**野馬鬱蘭**，是[唇形科](../Page/唇形科.md "wikilink")[牛至属中的一種植物](../Page/牛至属.md "wikilink")。

## 形态

[多年生](../Page/多年生植物.md "wikilink")[草本植物](../Page/草本.md "wikilink")，高达60厘米，全株被有微柔毛，有芳香；方形茎；卵形或矩圆状卵形的叶子对生，有腺点和柔毛；夏季开紫红色至白色的唇形花，成伞房状圆锥花序；卵圆形小[坚果](../Page/坚果.md "wikilink")。

## 分布

原生於歐洲[地中海沿岸地區](../Page/地中海.md "wikilink")；生于山坡草地、路旁。栽培需要排水良好的[土壤及充足的日照](../Page/土壤.md "wikilink")。

## 用途

牛至全草可提取芳香油，也作藥用，可紆緩傷風感冒病徵，但味道極不好喝\[1\]；做[烹調用時](../Page/烹調.md "wikilink")，常與[番茄](../Page/番茄.md "wikilink")、[乳酪搭配](../Page/乳酪.md "wikilink")；牛至與[羅勒是給予](../Page/羅勒.md "wikilink")[義大利菜獨特香味的兩大用料](../Page/義大利菜.md "wikilink")。因時常撒在[披薩餅上](../Page/披薩餅.md "wikilink")，所以又名披薩草。

## 參考資料

## 外部連結

  - [牛至
    Niuzhi](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00290)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)
  - [牛至 Niu
    Zhi](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00866)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

<!-- end list -->

  - [How to Grow
    Oregano](http://growingherbs.org.uk/herbs/how_to_grow_oregano.htm)
    Information about planting, propagating and growing oregano.
  - [Flora Europaea: *Origanum
    vulgare*](http://rbg-web2.rbge.org.uk/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=Origanum+&SPECIES_XREF=vulgare&TAXON_NAME_XREF=&RANK=)
  - [Germplasm Resources Information Network: *Origanum
    vulgare*](http://www.ars-grin.gov/cgi-bin/npgs/html/taxon.pl?25913)
  - [Gernot Katzer's Spice Pages: *Oregano (Origanum vulgare
    L.)*](http://gernot-katzers-spice-pages.com/engl/Orig_vul.html)
  - [Inhibition of enteric parasites by emulsified oil of
    oregano](http://www3.interscience.wiley.com/journal/72501130/abstract)
  - [Oregano Herb Profile:
    *vulgare*](http://www.mountainroseherbs.com/learn/oregano.html)

[Category:香草](../Category/香草.md "wikilink")
[Category:唇形科](../Category/唇形科.md "wikilink")
[Category:牛至属](../Category/牛至属.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  《真情康翠瑩》，2018-04-11 13:55，am1430