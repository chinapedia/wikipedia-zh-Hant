[Pagoda_Street,_Chinatown_Heritage_Centre,_Dec_05.JPG](https://zh.wikipedia.org/wiki/File:Pagoda_Street,_Chinatown_Heritage_Centre,_Dec_05.JPG "fig:Pagoda_Street,_Chinatown_Heritage_Centre,_Dec_05.JPG")
**牛車水**（）是[新加坡的](../Page/新加坡.md "wikilink")[唐人街](../Page/唐人街.md "wikilink")，是新加坡历史上重要的华人聚集地，为[新加坡的著名旅遊景點之一](../Page/新加坡旅遊景點列表.md "wikilink")，位于新加坡欧南园區。

## 歷史

1330年，中国元代民间商人、航海家[汪大渊到访](../Page/汪大渊.md "wikilink")[淡马锡](../Page/新加坡历史.md "wikilink")，记录了此地已存在华人社区。这是新加坡最古老的唐人街之一，也是最大的一个。

1819年，在英國人[斯坦福·莱佛士登陸新加坡之前](../Page/斯坦福·莱佛士.md "wikilink")，中國南下的勞工已在这片区域從事[檳榔與](../Page/檳榔.md "wikilink")[胡椒的種植](../Page/胡椒.md "wikilink")。此后，從中國南來的華人越來越多，莱佛士索性把新加坡河西南部沿駁船碼頭（Boat
Quay）一帶的地區劃為華人居住區。那一时期，新加坡還沒有自來水設備，全岛所需要的水都得用牛車自安祥山（Ann Siang
Hill）和史必靈街（Spring Street）的水井汲水載到此，於是這個以牛車載水供應用水的地區就稱為牛車水。

1822年，斯坦福·莱佛士制定了新加坡第一个城市规划，其中就将牛车水定为华人移民的居住区。此后随着新加坡的发展和人口的繁荣，牛车水变得过度拥挤，华人移民开始向新加坡的其他地区定居，1960年代[組屋的出现才缓解了这一状况](../Page/組屋.md "wikilink")。

1927年3月12日，由國民黨左派帶領的遊行隊伍在牛車水警局前與警方發生衝突，造成六死14傷，史稱「[牛車水事件](../Page/牛車水事件.md "wikilink")」。

## 鄰近

  - [芳林公园](../Page/芳林公园.md "wikilink")（Hong Lim Park）
  - [丹戎巴葛](../Page/丹戎巴葛.md "wikilink")（Tanjong Pagar），新加坡一個歷史區域。
  - [克拉碼頭](../Page/克拉碼頭.md "wikilink")（Clarke Quay），著名不夜區，有不少酒吧餐廳及夜總會。
  - [萊佛士坊](../Page/萊佛士坊.md "wikilink")（Raffles Place），新加坡商業地帶。

## 主要街道

  - Amoy Street（厦门街），取自福建省港口城市[厦门](../Page/厦门.md "wikilink")。
  - Ann Siang Road（安祥路），取自马六甲商人谢安祥（Chia Ann Siang）。
  - Boon Tat Street（文达街）, 取自新加坡商人黄文达（Ong Boon Tat）。
  - Club Street（客纳街），取自19世紀末至20世紀初的多間中式會館。
  - Dickenson Hill Road（狄更生山路），取自殖民時期一位傳教士J. T. Dickenson。
  - Erskine Road（厄士金路），取自19世紀一位英国殖民地商人Samuel Erskine。
  - Eu Tong Sen Street（余东旋街），取自著名马来亚华商余东旋。
  - Hokien
    Street（福建街），取自大部分[新加坡华人祖籍的中国](../Page/新加坡华人.md "wikilink")[福建省](../Page/福建省.md "wikilink")。
  - Jiak Chuan Road（若泉路）, 取自新加坡商人[陳金聲](../Page/陳金聲.md "wikilink")（Tan
    Kim Seng）的孫子陳若泉。
  - Keong Saik Road（恭錫路），取自马来亚商人陳恭錫（Tan Keong Saik）。
  - McCallum Street（麦卡南街），取自殖民地官员Henry Edward McCallum少校。
  - Mosque Street（摩士街），取自同区的[詹美回教堂](../Page/詹美回教堂.md "wikilink")（Masjid
    Jamae）。
  - Pagoda Street（宝塔街），取自詹美回教堂。
  - Sago Street（碩莪街），取自當時的西米（Sago）粉廠。
  - Smith
    Street（史密斯街），取自海峽殖民地總督[史密斯爵士](../Page/史密斯_\(海峽殖民地總督\).md "wikilink")（Sir
    Cecil Clementi Smith）。
  - Teck Lim Road（德霖路），取自华商黄德霖（Ong Teck Lim）。
  - Telok Ayer Street（直落亚逸街），取自已消失的直落亞逸灣。
  - Temple Street（登婆街），取自同區的印度教[馬里安曼廟](../Page/馬里安曼廟.md "wikilink")（Sri
    Mariamman Temple）。

## 交通

  - [牛車水地鐵站](../Page/牛車水地鐵站.md "wikilink")

## 参考文献

## 参见

  - [唐人街](../Page/唐人街.md "wikilink")
  - [新加坡华人](../Page/新加坡华人.md "wikilink")
  - [牛車水事件](../Page/牛車水事件.md "wikilink")

{{-}}

[Category:新加坡旅遊景點](../Category/新加坡旅遊景點.md "wikilink")
[Category:亞洲唐人街](../Category/亞洲唐人街.md "wikilink")
[Category:新加坡行政区划](../Category/新加坡行政区划.md "wikilink")