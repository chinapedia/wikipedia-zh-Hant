**牛頓縣**（**Newton County,
Mississippi**）是[美國](../Page/美國.md "wikilink")[密西西比州中部偏東的一個縣](../Page/密西西比州.md "wikilink")。面積1,501平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口21,838人。縣治[第開特](../Page/第開特_\(密西西比州\).md "wikilink")
(Decatur)。

目前的縣成立於1836年2月25日。縣名紀念紀念著名科學家[伊薩克·牛頓](../Page/伊薩克·牛頓.md "wikilink") （Sir
Issac Newton）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[N](../Category/密西西比州行政區劃.md "wikilink")

1.  <http://www.rootsweb.com/~msgenweb/county-hist/newton.htm>