[Newtons_cradle_animation_book.gif](https://zh.wikipedia.org/wiki/File:Newtons_cradle_animation_book.gif "fig:Newtons_cradle_animation_book.gif")
**牛顿摆**是一个1960年代发明的桌面演示装置，五个[质量相同的](../Page/质量.md "wikilink")[球体由吊绳固定](../Page/球体.md "wikilink")，彼此紧密排列。

牛顿摆最早是由法国物理学家[埃德姆·马略特](../Page/埃德姆·马略特.md "wikilink")（Edme
Mariotte）于1676年提出的。当摆动最右侧的球并在回摆时碰撞紧密排列的另外四个球，最左边的球将被弹出，并*仅有*最左边的球被弹出。

当然此过程也是可逆的，当摆动最左侧的球撞击其它球时，最右侧的球会被弹出。当最右侧的两个球同时摆动并撞击其他球时，最左侧的两个球会被弹出。同理相反方向同样可行，并适用于更多的球，三个，四个，五个甚至六个。

## 原理

[Kugelstoßpendel.jpg](https://zh.wikipedia.org/wiki/File:Kugelstoßpendel.jpg "fig:Kugelstoßpendel.jpg")

旁边的图示中最左边的球得到[动量并通过](../Page/动量.md "wikilink")[碰撞传递到右侧并排悬挂的球上](../Page/碰撞.md "wikilink")，动量在四个球中向右传递。当最右面的球无法将动量继续传递的时候，被弹出。

这是一系列[弹性碰撞](../Page/弹性碰撞.md "wikilink")，其中并包含[非弹性碰撞和](../Page/非弹性碰撞.md "wikilink")[动量](../Page/动量.md "wikilink")。由于在碰撞中不存在其它力的影响，左侧质量\(m\)速度\(v_l\)的\(l\)球动量必须传递给右侧静止的球。右侧质量\(m\)具有的\(r\)球被[碰撞后具有相同的动量](../Page/碰撞.md "wikilink")。被碰撞的球都具有向右的速度\(v_r\)并有向右移动的[趋势](../Page/趋势.md "wikilink")，称作[动量守恒](../Page/动量守恒.md "wikilink")。

\[l\,m\,v_l = r\,m\,v_r\,.\]
碰撞前后的[能量必须一致](../Page/能量.md "wikilink")，此处忽略球的[振动运动](../Page/振动.md "wikilink")，

\[l\,m\,\frac{v_l^2}{2} = r\,m\,\frac{v_r^2}{2}\,.\] 写作

\[l\,m\,v_l\,\frac{v_l}{2} = r\,m\,v_r\,\frac{v_r}{2}\,,\]
对于第一个公式，由于\(l\,m\,v_l\)不等于零，所以速度为\(v_l=v_r\,.\)。第一个公式\(l=r:\)说明碰撞时有数个球被碰撞后弹出。

在这里，被碰撞的球以同样的速度移动，而剩余的球不动。当多于两个球时，则不能按照[能量守恒和](../Page/能量守恒.md "wikilink")[动量守恒考虑](../Page/动量守恒.md "wikilink")。

在[重力系统中](../Page/重力.md "wikilink")，左侧的\(l\)球以速度\(v_l\)[碰撞右侧速度为](../Page/碰撞.md "wikilink")\(v_r\)的\(r\)球，遵守[能量守恒和](../Page/能量守恒.md "wikilink")[动量守恒](../Page/动量守恒.md "wikilink")，碰撞后\(l\)球以速度\(v_l\)向右，\(r\)球以速度\(v_r\)相左继续运动。相反的，\(l\)球可以以相反的速度\(-v_l\)，\(r\)球有相反的速度\(-v_r\)。

要解释球串的表现，必须更进一步思考，[撞击波是如何在球串中传递的](../Page/撞击波.md "wikilink")。

## 参阅

  - [摆](../Page/摆.md "wikilink")
  - [埃德姆·马略特](../Page/埃德姆·马略特.md "wikilink")

## 参考书籍

  - F. Herrmann, P. Schmälzle: *A simple explanation of a well-known
    collision experiment*, Am. J. Phys. 49, 761 (1981)（德语）
  - F. Herrmann, M. Seitz: *How does the ball-chain work?*, Am. J. Phys.
    50, 977 (1982)（德语）

## 链接

[Category:教学玩具](../Category/教学玩具.md "wikilink")
[Category:办公室玩具](../Category/办公室玩具.md "wikilink")
[Category:新奇物品](../Category/新奇物品.md "wikilink")
[Category:金属玩具](../Category/金属玩具.md "wikilink")
[Category:物理学教育](../Category/物理学教育.md "wikilink")
[Category:科学演示实验](../Category/科学演示实验.md "wikilink")