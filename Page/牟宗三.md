**牟宗三**（）（），字**離中**，現代思想家、哲學家、教育家，是新儒家學派代表人物。

## 生平

牟宗三1909年生於[山東省](../Page/山東省.md "wikilink")[棲霞縣牟家疃](../Page/棲霞縣.md "wikilink")，祖籍[湖北省](../Page/湖北省.md "wikilink")[公安縣](../Page/公安縣.md "wikilink")。牟氏9歲入私塾讀書，11歲入新制小學，15歲入棲霞縣立中學。1928年，考入[北京大學預科](../Page/北京大學.md "wikilink")，兩年後升哲學系，1933年畢業。大學時代先後受學於[張申府](../Page/張申府.md "wikilink")、[金岳霖](../Page/金岳霖.md "wikilink")，受[熊十力影響最大](../Page/熊十力.md "wikilink")，受其《新唯識論》義理之震撼，並維持幾十年的師生情誼。\[1\]

北大哲學系畢業後，牟氏先後任教於山東[壽張縣鄉村師範](../Page/壽張縣.md "wikilink")、廣州學海書院、山東鄒平村治學院、廣西梧州中學、南寧中學、[華西大學](../Page/華西大學.md "wikilink")、[中央大學](../Page/中央大學.md "wikilink")、[金陵大學](../Page/金陵大學.md "wikilink")、[浙江大學](../Page/浙江大學.md "wikilink")，以講授邏輯與西方文化為主。三十年代，曾主編《歷史與文化》、《再生》雜誌等。1949年，牟氏到台灣[國立臺灣師範大學與](../Page/國立臺灣師範大學.md "wikilink")[東海大學任教](../Page/東海大學_\(台灣\).md "wikilink")。1954年受聘為台灣教育部學術審議委員。\[2\]\[3\]

1960年應聘至[香港大學主講中國哲學](../Page/香港大學.md "wikilink")。1968年，由香港大學轉任[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院哲學系主任](../Page/新亞書院.md "wikilink")。1974年自香港中文大學退休，任教[新亞研究所](../Page/新亞研究所.md "wikilink")。其後又任教[國立臺灣大學](../Page/國立臺灣大學.md "wikilink")、[國立臺灣師範大學](../Page/國立臺灣師範大學.md "wikilink")、[東海大學](../Page/東海大學_\(台灣\).md "wikilink")、[國立中央大學](../Page/國立中央大學.md "wikilink")。\[4\]

1995年4月12日下午三時，牟氏因[器官衰竭逝於](../Page/器官衰竭.md "wikilink")[國立台灣大學醫學院附設醫院](../Page/國立台灣大學醫學院附設醫院.md "wikilink")，享壽86歲。

## 學術

牟宗三的思想受[熊十力的影響很大](../Page/熊十力.md "wikilink")，他不僅繼承而且發展了熊十力的哲學思想。牟氏較多地著力於哲學理論方面的專研，謀求儒家哲學與康德哲學的融通，並力圖重建儒家的「道德的形上學」。代表作包括：《心體與性體》《才性與玄理》《中國哲學十九講》《中西哲學之匯通》《現象與物自身》《佛性與般若》等。

牟氏曾獨力翻譯康德的《三大批判》，融合康德哲學與[孔](../Page/孔子.md "wikilink")、[孟](../Page/孟子.md "wikilink")、[陸](../Page/陸九淵.md "wikilink")、[王的](../Page/王陽明.md "wikilink")[心學](../Page/心學.md "wikilink")，以中國哲學與康德哲學互相詮解。也曾參與編寫雜誌，主編《再生》、創辦《歷史與文化》、編輯《理想歷史文化》。作為[新儒家代表人物](../Page/新儒家.md "wikilink")，牟氏認為當代[新儒學的任務為](../Page/新儒學.md "wikilink")「道統之肯定，即肯定道德宗教之價值，護住孔孟所開闢之人生宇宙之本源。」以「反省中華民族的文化生命，以重開中國哲學的途徑。」為己任。
他的著作，都是針對時代或學術問題，而提供一解決之道。\[5\]

牟氏的學術研究範圍包括：

  - [邏輯學](../Page/邏輯學.md "wikilink")
  - [康德哲學](../Page/伊曼努尔·康德.md "wikilink")
  - [宋明理學](../Page/宋明理學.md "wikilink")
  - [魏晉玄學](../Page/魏晉玄學.md "wikilink")
  - [佛學](../Page/佛學.md "wikilink")

## 榮譽

  - 1983年，獲授中華民國政府頒發[行政院文化獎](../Page/行政院文化獎.md "wikilink")。\[6\]
  - 1987年，獲[香港大學頒授榮譽文學博士](../Page/香港大學.md "wikilink")。\[7\]

## 評價

  - 牟宗三治喪委員會的評價：“綜觀先生一生，無論講學論道，著書抒義，莫不念念以光暢中國哲學之傳統、昭蘇民族文化之生命為宗趣。其學思之精敏，慧識之弘卓，與夫文化意識之綿穆強烈，較之時流之內失宗主而博雜歧出者，敻乎尚矣。”\[8\]

## 著作概覽

  - 《[周易的自然哲學與道德涵義](../Page/周易的自然哲學與道德涵義.md "wikilink")》
  - 《邏輯典範》
  - 《[認識心之批判](../Page/認識心之批判.md "wikilink")》
  - 《理則學》
  - 《[道德的理想主義](../Page/道德的理想主義.md "wikilink")》
  - 《歷史哲學》
  - 《中國哲學的特質》
  - 《[名家與荀子](../Page/名家與荀子.md "wikilink")》
  - 《生命的學問》
  - 《五十自述》

<!-- end list -->

  - 《時代與感受》
  - 《中國文化的省察》
  - 《[心體與性體](../Page/心體與性體.md "wikilink")》
  - 《從陸象山到劉蕺山》
  - 《[才性與玄理](../Page/才性與玄理.md "wikilink")》
  - 《[佛性與般若](../Page/佛性與般若.md "wikilink")》
  - 《[智的直覺與中國哲學](../Page/智的直覺與中國哲學.md "wikilink")》
  - 《[現象與物自身](../Page/現象與物自身.md "wikilink")》
  - 《[圓善論](../Page/圓善論.md "wikilink")》
  - 《名理論》

<!-- end list -->

  - 《[歷史哲學](../Page/歷史哲學.md "wikilink")》
  - 《[政道與治道](../Page/政道與治道.md "wikilink")》
  - 《康德的道德哲學》
  - 譯作：《康德「純粹理性之批判」》
  - 譯作：《康德「判斷力之批判」》
  - 《[中國哲學十九講](../Page/中國哲學十九講.md "wikilink")》
  - 《中西哲學之會通十四講》
  - 《人文講習錄》

## 弟子

  - [蔡仁厚](../Page/蔡仁厚.md "wikilink")
  - [戴璉璋](../Page/戴璉璋.md "wikilink")
  - [陳癸淼](../Page/陳癸淼.md "wikilink")
  - [王邦雄](../Page/王邦雄.md "wikilink")
  - [曾昭旭](../Page/曾昭旭.md "wikilink")
  - [盧雪崑](../Page/盧雪崑.md "wikilink")
  - [鄺錦倫](../Page/鄺錦倫.md "wikilink")
  - [謝仲明](../Page/謝仲明.md "wikilink")
  - [關永中](../Page/關永中.md "wikilink")
  - [韋政通](../Page/韋政通.md "wikilink")
  - [何淑靜](../Page/何淑靜.md "wikilink")
  - [蔡美麗](../Page/蔡美麗.md "wikilink")

<!-- end list -->

  - [朱建民](../Page/朱建民.md "wikilink")
  - [范良光](../Page/范良光.md "wikilink")
  - [王財貴](../Page/王財貴.md "wikilink")
  - [李天命](../Page/李天命.md "wikilink")
  - [冼景炬](../Page/冼景炬.md "wikilink")
  - [楊祖漢](../Page/楊祖漢.md "wikilink")
  - [李瑞全](../Page/李瑞全.md "wikilink")
  - [蕭振邦](../Page/蕭振邦.md "wikilink")
  - [顏國明](../Page/顏國明.md "wikilink")
  - [鄧立光](../Page/鄧立光.md "wikilink")

<!-- end list -->

  - [林月惠](../Page/林月惠.md "wikilink")
  - [李明輝](../Page/李明輝.md "wikilink")
  - [莊耀郎](../Page/莊耀郎.md "wikilink")
  - [劉錦賢](../Page/劉錦賢.md "wikilink")
  - [高柏園](../Page/高柏園.md "wikilink")
  - [謝大寧](../Page/謝大寧.md "wikilink")
  - [林安梧](../Page/林安梧.md "wikilink")
  - [樊克偉](../Page/樊克偉.md "wikilink")
  - [吳明](../Page/吳明.md "wikilink")
  - [鄭志明](../Page/鄭志明.md "wikilink")

<!-- end list -->

  - [霍韜晦](../Page/霍韜晦.md "wikilink")
  - [王淮](../Page/王淮.md "wikilink")
  - [唐亦男](../Page/唐亦男.md "wikilink")
  - [郭漢揚](../Page/郭漢揚.md "wikilink")
  - [劉述先](../Page/劉述先.md "wikilink")
  - [黃子程](../Page/黃子程.md "wikilink")
  - [陶國璋](../Page/陶國璋.md "wikilink")
  - [岑逸飛](../Page/岑逸飛.md "wikilink")
  - [賴光朋](../Page/賴光朋.md "wikilink") (自稱最後入室弟子)
  - [李祖原](../Page/李祖原.md "wikilink")
  - [信廣來](../Page/信廣來.md "wikilink")

## 參見

  - [儒學](../Page/儒學.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [余英時](../Page/余英時.md "wikilink")：〈[追憶牟宗三先生](http://www.chinainperspective.com/ArtShow.aspx?AID=17172)〉。
  - [劉述先](../Page/劉述先.md "wikilink")：〈[牟宗三先生在當代中國哲學上的貢獻](http://140.109.24.171/home/publish/PDF/Newsletter/18/18-172-174.pdf)〉。
  - 李明辉：〈[牟宗三与“生命的学问”](http://www.nssd.org/articles/article_read.aspx?id=664853201)〉。
  - 李明輝：〈[牟宗三哲學中的「物自身」概念](http://fscpc.org/mouzongsan/pdf/Ming-HueiSTCC022.pdf)〉。
  - 李明輝：〈[略論牟宗三先生的康德學](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/18/18-184-193.pdf)〉。
  - 李明輝：[〈牟宗三先生的哲學詮釋中之方法論問題〉](http://www.aisixiang.com/data/18559.html)（1996）
  - 李明辉：〈[牟宗三误解了康德的“道德情感”概念吗？](http://www.nssd.org/articles/article_read.aspx?id=668435356)〉。
  - 曾國祥：〈[牟宗三與儒家民主：一個黑格爾式的再詮釋](https://www.tpsr.tw/node/1422)〉。
  - 林維杰：〈[牟宗三先生論儒教](https://web.archive.org/web/20141022194917/http://203.72.2.115/EJournal/4012000703.pdf)〉。
  - 林維杰：〈[牟宗三倫理美學中的人物想像](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/75/75-89-108.pdf)〉。
  - 林維杰：〈[牟宗三哲學中的理論與實踐](http://www.litphil.sinica.edu.tw/public/publications/newsletter/59/99-126.pdf)〉。
  - 黃冠閔：〈[牟宗三的感通論：一個概念脈絡的梳理](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/75/75-65-87.pdf)〉。
  - 黃冠閔：〈[寂寞的獨體與記憶共同體：牟宗三《五十自述》中的生命修辭](http://homepage.ntu.edu.tw/~bcla/e_book/87/8704.pdf)〉。
  - 陳士誠：〈[牟宗三先生論道德惡與自由決意](http://libwri.nhu.edu.tw:8081/Ejournal/4012001602.pdf)〉。
  - 陳士誠：〈[康德之自由決意與告子之生之謂性－－以牟宗三哲學發展論其對惡問題之理解](http://www.ntpu.edu.tw/dcll/96102728PDF/010.pdf)〉。
  - 李瑞全：〈[龍溪四無句與儒家之圓教義之證成──兼論牟宗三先生對龍溪評價之發展](http://in.ncu.edu.tw/phi/teachers/lsc/docs/Wang%20Lun-Chi's%20Doctrine%20of%20Ssu-Wu%20and%20The%20Justification%20of%20Confucian%20Roung%20Teaching%20on%20the%20Evolution%20of%20Professor%20Nou%20Tsung-san's%20Evaluation%20of%20Wang%20Lun-chi.pdf)〉。
  - 倪梁康：[〈牟宗三與現象學〉](http://www.aisixiang.com/data/26983.html)（2002）
  - 史偉民：〈[道德感情與動機：康德、席勒與牟宗三](http://thjcs.web.nthu.edu.tw/ezfiles/662/1662/img/2351/165524173.pdf)〉。
  - 何乏筆：〈[何為「兼體無累」的工夫——論牟宗三與創造性的問題化](https://web.archive.org/web/20141105061338/http://www.litphil.sinica.edu.tw/heubel/publications/4-7.pdf)〉。
  - 施益堅：〈[中西哲學之會通？論雷奧福對牟宗三的詮釋和批判](http://www.litphil.sinica.edu.tw/public/publications/newsletter/66/43-52.pdf)〉。

[Category:20世紀哲學家](../Category/20世紀哲學家.md "wikilink")
[Category:中華民國哲學家](../Category/中華民國哲學家.md "wikilink")
[Category:民國儒學學者](../Category/民國儒學學者.md "wikilink")
[Category:新儒家學者](../Category/新儒家學者.md "wikilink")
[Category:新儒學八大家](../Category/新儒學八大家.md "wikilink")
[文](../Category/國立中央大學教授.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:國立臺灣師範大學教授](../Category/國立臺灣師範大學教授.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:東海大學教授](../Category/東海大學教授.md "wikilink")
[Category:香港大學教授](../Category/香港大學教授.md "wikilink")
[Category:香港中文大學教授](../Category/香港中文大學教授.md "wikilink")
[Category:北京大學校友](../Category/北京大學校友.md "wikilink")
[Category:台灣戰後山東移民](../Category/台灣戰後山東移民.md "wikilink")
[Category:公安人](../Category/公安人.md "wikilink")
[Category:栖霞人](../Category/栖霞人.md "wikilink")
[Z宗](../Category/牟姓.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:中華民國反共主義者](../Category/中華民國反共主義者.md "wikilink")
[Category:行政院文化獎得主](../Category/行政院文化獎得主.md "wikilink")

1.  {{ cite web
    |url=<https://www4.hku.hk/hongrads/index.php/chi/archive/graduate_detail/176>
    |title=第130屆頒授典禮 (1987)牟宗三讚詞 |date= |publisher=香港大學 |author= }}

2.  {{ cite web
    |url=<http://cultural-award.moc.gov.tw/index03_03_1.html>
    |title=行政院文化獎第3屆得獎人介紹 |access-date=2018-02-09
    |archive-url=<https://web.archive.org/web/20180210002137/http://cultural-award.moc.gov.tw/index03_03_1.html>
    |archive-date=2018-02-10 |dead-url=yes }}

3.  {{ cite web
    |url=<http://www.cuhk.edu.hk/ugallery/tc/story/mou_tsung_san.htm>
    |title=大師身影：牟宗三先生 |date= |publisher= |author= }}

4.
5.
6.
7.
8.  [牟宗三先生學行事略](http://groups.msn.com/sgip55u2n4ipcjm9146n91lu30/page99.msnw)