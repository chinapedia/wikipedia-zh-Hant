**牡丹江**位于[中国东北地区](../Page/中国东北地区.md "wikilink")，是[松花江右岸支流](../Page/松花江.md "wikilink")，河名系[满语](../Page/满语.md "wikilink")“穆丹乌拉”（）的转译音，意为“弯曲的江”。发源于[吉林省](../Page/吉林省.md "wikilink")[敦化市](../Page/敦化市.md "wikilink")，于[黑龙江省](../Page/黑龙江省.md "wikilink")[依兰县注入松花江](../Page/依兰县.md "wikilink")，全长725千米。\[1\]現今牡丹江取代[長城](../Page/長城.md "wikilink")，成為[冬小麥種植的北界](../Page/小麥.md "wikilink")。

## 历史

牡丹江流域历史悠久，从[商朝到](../Page/商朝.md "wikilink")[隋朝的](../Page/隋朝.md "wikilink")2300多年间，这里是[满族祖先](../Page/满族.md "wikilink")[肃慎](../Page/肃慎.md "wikilink")、[挹娄](../Page/挹娄.md "wikilink")、[勿吉等部族的居住地](../Page/勿吉.md "wikilink")。牡丹江在[唐代称](../Page/唐代.md "wikilink")**忽汉河**，[粟末靺鞨在此兴起](../Page/粟末靺鞨.md "wikilink")，建立[渤海国](../Page/渤海国.md "wikilink")，首府[上京龙泉府位于今牡丹江右岸黑龙江宁安市](../Page/上京龙泉府.md "wikilink")[渤海镇境内](../Page/渤海镇_\(宁安市\).md "wikilink")。牡丹江《[辽史](../Page/辽史.md "wikilink")》称**斡朗改**，《[金史](../Page/金史.md "wikilink")》作**呼里改江**，[元代称](../Page/元代.md "wikilink")**忽尔哈河**，《[明一统志](../Page/明一统志.md "wikilink")》称**胡里改江**，又称**呼拉哈河**、**虎尔哈河**、**胡尔哈河**，[清代在](../Page/清代.md "wikilink")[镜泊湖以下河段称](../Page/镜泊湖.md "wikilink")**虎尔哈河**或**瑚尔哈河**（），湖以上河段称**勒富善河**（）。\[2\]

## 干流概况

牡丹江发源于吉林敦化西南部[江源镇马五店村西南](../Page/江源镇_\(敦化市\).md "wikilink")[牡丹岭北麓](../Page/牡丹岭.md "wikilink")，蜿蜒向北流经敦化市区、黑龙江[镜泊湖](../Page/镜泊湖.md "wikilink")、[宁安市](../Page/宁安市.md "wikilink")、[牡丹江市](../Page/牡丹江市.md "wikilink")、[海林市](../Page/海林市.md "wikilink")[莲花水库](../Page/莲花水库.md "wikilink")、[林口县](../Page/林口县.md "wikilink")[三道通镇等地](../Page/三道通镇.md "wikilink")，最后于依兰县注入松花江。全长725千米，河道平均比降1.39%，流域面积38909平方千米。牡丹江干流穿行于[老爷岭和](../Page/老爷岭.md "wikilink")[张广才岭之间条形谷底中](../Page/张广才岭.md "wikilink")，上游河道狭小，下游河道宽度一般在400～500米。年均径流量89.5亿立方米。\[3\]

## 流域概况

牡丹江流域地处中温带大陆性季风气候区，夏季炎热多雨，冬季严寒漫长。年均气温3.5℃，极端低温-43.1℃，极端高温37.5℃。流域封冻期从11月上旬至第二年3月下旬。年均降水量540～610毫米，主要集中在6－9月。年均径流量89.5亿立方米。流域植被较好，森林覆盖率46%，河流含沙量很少。\[4\]

## 参考资料

[M](../Category/松花江水系.md "wikilink") [M](../Category/吉林河流.md "wikilink")
[M](../Category/黑龍江河流.md "wikilink")

1.

2.  [牡丹江流域满语地名之翻译考证](http://www.doc88.com/p-3037524896521.html)，王岸英，《民族翻译》，2008年第一期

3.
4.