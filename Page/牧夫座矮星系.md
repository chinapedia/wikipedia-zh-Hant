{{ Galaxy | | image = | name = 牧夫座矮星系 | epoch =
[J2000](../Page/J2000.md "wikilink") | type = dSph\[1\] | ra = \[2\] |
dec =  ± 15″\[3\] | dist_ly = 197 ± 18[kly](../Page/光年.md "wikilink")
(60 ± 6 [kpc](../Page/秒差距.md "wikilink"))\[4\] | z = | appmag_v = 13.1
| size_v = 26′.0 ± 1′.4\[5\] | constellation name =
[牧夫座](../Page/牧夫座.md "wikilink") | notes = | names = Boo
dSph, Boötes Satellite,\[6\]
Boötes Dwarf Spheroidal Galaxy,\[7\]
Boötes dSph galaxy\[8\] }}

**牧夫座矮星系**（*Boo
dSph*）是迄2006年所發現最黯淡的星系，總光度只相當於100,000[太陽](../Page/太陽.md "wikilink")，絕對星等只有-5.8等，位於[牧夫座的方向上](../Page/牧夫座.md "wikilink")，距離197,000光年。這個[矮橢球星系似乎受到](../Page/矮橢球星系.md "wikilink")[銀河系的潮汐力破壞](../Page/銀河系.md "wikilink")\[9\]，有兩串星跡在軌道上交叉成十字狀，但一般被潮汐力破懷的星系通常只有一條星跡。

這個星系比另一個已知的黯淡星系[大熊座矮星系](../Page/大熊座矮星系.md "wikilink")（絕對星等-6.75等）還要黯淡，甚至比[參宿七](../Page/參宿七.md "wikilink")（絕對星等-6.8等）還黯淡。扣除[暗星系](../Page/暗星系.md "wikilink")，像是[室女座星系團的](../Page/室女座星系團.md "wikilink")[室女座HI21星系](../Page/室女座HI21.md "wikilink")，這是已知最黯的星系。

## 外部連結

  - [The Universe within 500,000 light-years The Satellite
    Galaxies](http://www.atlasoftheuniverse.com/sattelit.html) (Atlas of
    the Universe)
  - [Two New Galaxies Orbiting the Milky
    Way](http://kencroswell.com/BootesCanesVenaticiDwarfs.html) (Ken
    Croswell) April 19, 2006
  - [Strange satellite galaxies revealed around Milky
    Way](http://www.newscientistspace.com/article/dn9043-strange-satellite-galaxies-revealed-around-milky-way.html)
    Kimm Groshong (New Scientist) 17:00 24 April 2006
  - [New Milky Way companions found: SDSS-II first to view two dim dwarf
    galaxies](http://www.sdss.org/news/releases/20060508.companions.html)
    (SDSS) May 8, 2006
  - [Astronomers Find Two New Milky Way
    Companions](http://www.spacedaily.com/reports/Astronomers_Find_Two_New_Milky_Way_Companions.html)
    (SpaceDaily) May 10, 2006

## 註

<div class="references-small">

1.  [視星等](../Page/視星等.md "wikilink") =
    [絕對星等](../Page/絕對星等.md "wikilink") +
    [距離模數](../Page/距離模數.md "wikilink") = -5.8\[10\] +
    18.9\[11\] = 13.1

</div>

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮球狀星系](../Category/矮球狀星系.md "wikilink")
[Category:銀河系群](../Category/銀河系群.md "wikilink")
[Category:牧夫座](../Category/牧夫座.md "wikilink")

1.

2.
3.
4.
5.
6.

7.
8.
9.
10.
11.