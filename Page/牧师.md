[Oratorio-pastor.jpg](https://zh.wikipedia.org/wiki/File:Oratorio-pastor.jpg "fig:Oratorio-pastor.jpg")[加的斯Oratorio](../Page/加的斯.md "wikilink")
de la Santa Cueva地下教堂的[耶稣善牧雕像](../Page/耶稣善牧.md "wikilink")\]\]

**牧師**是指[基督教的](../Page/基督教.md "wikilink")[團體中](../Page/教會.md "wikilink")，專職負責帶領及照顧其他[基督徒的](../Page/基督徒.md "wikilink")[神职人员](../Page/神职人员.md "wikilink")。[聖經原文的用字就是](../Page/聖經.md "wikilink")[牧羊人之意](../Page/牧羊人.md "wikilink")。雖然[天主教會也有類似的職位](../Page/天主教會.md "wikilink")，但「牧師」一词在中文语境中特指[新教的基層神職人員](../Page/新教.md "wikilink")。

## 詞源

牧師的一詞源於[拉丁語的](../Page/拉丁語.md "wikilink")，而[希臘文為](../Page/希臘文.md "wikilink")，與《[新約聖經](../Page/新約聖經.md "wikilink")·[以弗所書](../Page/以弗所書.md "wikilink")》4:11有關。《[聖經和合本](../Page/聖經和合本.md "wikilink")》譯「他所賜的有使徒、有先知、有傳福音的、有**牧師**和[教師](../Page/教師.md "wikilink")」，但此經文在其他[聖經譯本卻有不同譯法](../Page/聖經譯本.md "wikilink")，如牧者（《[聖經和合本修訂版](../Page/聖經和合本修訂版.md "wikilink")》）、牧人（《[呂振中譯本](../Page/聖經_\(呂振中譯本\).md "wikilink")》和《[聖經恢復本](../Page/聖經恢復本.md "wikilink")》）、司牧（《[思高譯本](../Page/聖經_\(思高譯本\).md "wikilink")》）、牧養（《[聖經新譯本](../Page/聖經新譯本.md "wikilink")》）。

## 歷史演化

許多學者認為，基督宗教在第一世紀後受到周圍不同地方的文化和習俗的影響，逐漸地將[長老和](../Page/長老.md "wikilink")[監督的職務自一般信徒分離出來](../Page/監督.md "wikilink")，然後將**監督**提升到高於長老的地位，稱之為[主教](../Page/主教.md "wikilink")(Bishop)，而將長老置在主教之下，在英文稱為祭司（priest），在中文稱為“[司鐸](../Page/司鐸.md "wikilink")”（神父）。在這個時候開始，一位主教可以監督很多城或[教區的諸多](../Page/教區.md "wikilink")[司鐸](../Page/祭司.md "wikilink")、[教會](../Page/教會.md "wikilink")、信徒。[宗教改革後](../Page/宗教改革.md "wikilink")，[加爾文](../Page/加爾文.md "wikilink")、[慈運理和其他改革者以牧師](../Page/慈運理.md "wikilink")（pastor）取代[天主教的](../Page/天主教.md "wikilink")[神父](../Page/神父.md "wikilink")（priest）。不同的是：新教教會由牧師和獨立的長老管理，牧師並可以結婚，其妻子在某些教會被稱為「**牧師娘**」、「**先生媽**」是因為受到[臺灣閩南語的影響](../Page/臺灣閩南語.md "wikilink")，亦有「師母」等源自中國傳統文化師父師母的稱呼。

## 職位概況

### 新教

在新教中，特别是在[浸信会中](../Page/浸信会.md "wikilink")，认为“牧师”（pastor，以弗所书4:11）、“**长老**”（elder，使徒行传20:17）、“**监督**”（bishop或overseer，提摩太前书3:1）指的是同一个职位，牧师强调的是此职位的牧养工作，长老强调的是这人在属灵上是成熟的（不一定是年龄很老的），监督强调的是他的治理教会的工作。保罗在[提摩太前书第三章规定了监督的条件](../Page/提摩太前书.md "wikilink")，在[提多书第一章制定了长老的条件](../Page/提多书.md "wikilink")，比较一下发现两者是基本一致的；而且保罗在提多书第一章里交替使用长老和监督（1:5,7）。此外，保罗在使徒行传20章见到了以弗所的长老，嘱咐他们牧养神的教会，这也是牧师的工作。\[1\]

由二十世紀開始，[女性亦可以成為牧師](../Page/女性.md "wikilink")。然而，在某些教會中（如[浸信會](../Page/浸信會.md "wikilink")），女性擔任牧師還不普遍；在另一些教會（如[信義會](../Page/信義會.md "wikilink")），女性出任牧師是很普遍的。在圣公会的[三級聖品制裡](../Page/三級聖品制.md "wikilink")，牧師上一級是[主教](../Page/主教.md "wikilink")，低一級是[會吏](../Page/會吏.md "wikilink")。而在[会众制的新教教會中](../Page/会众制.md "wikilink")，[主任牧師則成了一間教會裡最高的決策者](../Page/主任牧師.md "wikilink")。

[Priest_of_Woman.jpg](https://zh.wikipedia.org/wiki/File:Priest_of_Woman.jpg "fig:Priest_of_Woman.jpg")
[英國或其他國家的](../Page/英國.md "wikilink")[國教教會的牧師](../Page/國教.md "wikilink")，尤是[聖公會和](../Page/聖公會.md "wikilink")[衛理會的牧師](../Page/衛理會.md "wikilink")，英文稱之為Minister，中文譯「會長」；如果是指非國教教會的牧師，尤是[美國的教會](../Page/美國.md "wikilink")，英文稱之為Pastor；頭銜為Reverend。

一些[新教教徒](../Page/新教.md "wikilink")（例如[召會](../Page/召會.md "wikilink")）認為，牧師的稱謂違背了「[信徒皆祭司](../Page/信徒皆祭司.md "wikilink")」的教義，因此拒絕使用這個詞和牧師聘用制度，其中包括[門諾會和一些受](../Page/門諾會.md "wikilink")[弟兄運動影響的教會](../Page/弟兄運動.md "wikilink")。

### 天主教會

天主教會中的基层司牧一职，稱為「**本堂神父**」或「**主任司鐸**」，由具有神品的[神父擔任](../Page/神父.md "wikilink")，主要工作為管理[堂區并牧养堂区内的羊群](../Page/堂區.md "wikilink")。在英语中稱為「parish
priest」。

## 圣经对牧师之资格的要求

  - [提摩太前书第三章说](../Page/提摩太前书.md "wikilink")：“作监督的，必须无可指责，只作一个妇人的丈夫，有节制，自守，端正，乐意接待远人，善於教导；不因酒滋事，不打人，只要温和，不争竞，不贪财；好好管理自己的家，使儿女凡事端庄顺服（或作：端端庄庄的使儿女顺服）。人若不知道管理自己的家，焉能照管神的教会呢？初入教的不可作监督，恐怕他自高自大，就落在魔鬼所受的刑罚里。监督也必须在教外有好名声，恐怕被人毁谤，落在魔鬼的网罗里。
  - [提多书第一章说](../Page/提多书.md "wikilink")：”若有无可指责的人，只作一个妇人的丈夫，儿女也是信主的，没有人告他们是放荡不服约束的，就可以设立。
    监督既是神的管家，必须无可指责，不任性，不暴躁，不因酒滋事，不打人，不贪无义之财。乐意接待远人，好善，庄重，公平，圣洁自持。坚守所教真实的道理，就能将纯正的教训劝化人，又能把争辩的人驳倒了。
    ”

## 某些教会对牧师之资格的要求

牧師的資格一般最少要有一個[神學學士學位](../Page/神學.md "wikilink")。

一般來說，每一間新教教會最少有一個牧師。在中国大陆，情况比较特殊，有些教会没有牧师，而只有一个或几个经验丰富的信徒组织其他信徒活动，被称为“带领人”。而另有些缺少牧师的教会就會由[傳道人帶領教會](../Page/傳道人.md "wikilink")；如果連傳道人也沒有的話，那就由不受薪的幾個執事/[長老共同管理](../Page/長老.md "wikilink")。這些情況常見於一些信徒不多的小型教會，在有些基督徒看来是教会发展不成熟的表现，也不太符合圣经的要求，因为将长老、传道人、执事的定义与职责搞得混乱不清。

## 圣公会職掌分級

  - [座堂主任牧師](../Page/座堂主任牧師.md "wikilink")（[普世聖公宗](../Page/普世聖公宗.md "wikilink")）(Dean)
  - [牧區主任牧師](../Page/牧區主任牧師.md "wikilink")（普世聖公宗）（[基督教](../Page/基督教.md "wikilink")）(Vicar)
  - [牧區助理聖品](../Page/牧區助理聖品.md "wikilink")/[助理牧師](../Page/助理牧師.md "wikilink")（普世聖公宗）
    (Curate)
  - [傳道區主理聖品](../Page/傳道區主理聖品.md "wikilink")(priest-in-charge)
  - [法政牧師](../Page/法政牧師.md "wikilink")（普世聖公宗） (Canon)
  - [教憲牧師](../Page/教憲牧師.md "wikilink")（普世聖公宗）(Canon Theologian)
  - [會吏長](../Page/會吏長.md "wikilink")（普世聖公宗） (Archdeacon)

另外，部份基督教教會又另設[顧問牧師和](../Page/顧問牧師.md "wikilink")[義務牧師](../Page/義務牧師.md "wikilink")。

## 薪資

基督教萌芽初期，傳道人的生活費用來源主要有：

1.  會友奉獻
2.  母會或其他堂會的奉獻（羅馬書15：25-27）
3.  傳道人兼職（例如保羅以織帳棚為業\[2\]）

在台灣，牧師薪資通常是交由教會資深會友組成的長執會決定；小規模的教會（例如50人以內）的牧師，教會日常運作及薪水即可由宣教機構或母會承擔，但如果是不屬於任何堂會或宣教機構的獨立教會，薪水通常是由牧師/傳道人自籌（如仰賴會友稀少的奉獻度日、仰賴配偶供應或在外另有兼職謀生）。在教會，牧師薪資通常是個敏感的議題，因為部分人士誤以為可以透過薪資來控制牧師，或以為牧師工時很少，不需要撥付相當的工資。\[3\]

## 其他解釋

現今許多[線上遊戲及文學作品中](../Page/線上遊戲.md "wikilink")，常出現「牧師」一職業，如[魔獸世界](../Page/魔獸世界.md "wikilink")。通常線上遊戲中的牧師是擔任治療者的角色，藉由神聖的咒語和[魔法達到補血](../Page/魔法.md "wikilink")、驅魔、復活等功能。相較於其他職業多半是為敵方帶來損傷，牧師則是為友方提供增益、治療，因此在一個團隊裡是不可或缺的角色。

## 参见

  - [长老](../Page/长老.md "wikilink")
  - [主教](../Page/主教.md "wikilink")
  - [祭司](../Page/祭司.md "wikilink")

## 参考

<references/>

[Category:基督教頭銜](../Category/基督教頭銜.md "wikilink")
[Category:基督教聖職者](../Category/基督教聖職者.md "wikilink")

1.  Jeff Brown: Form and Freedom, What the New Testament Teaches about
    Church Government and Church Leadership, 2004, ISBN 3-937965-06-8.
2.
3.