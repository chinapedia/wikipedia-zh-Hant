**物部守屋**（）是[日本](../Page/日本.md "wikilink")[飛鳥時代的政治家](../Page/飛鳥時代.md "wikilink")，官職為[大連](../Page/大連_\(官職\).md "wikilink")（[伴造出身的有力](../Page/伴造.md "wikilink")[豪族](../Page/豪族.md "wikilink")）。物部守屋出身於當時朝廷中的有力氏族[物部氏](../Page/物部氏.md "wikilink")，其父親為[物部尾輿](../Page/物部尾輿.md "wikilink")，母親為[弓削氏之女](../Page/弓削氏.md "wikilink")[阿佐姬](../Page/阿佐姬.md "wikilink")。

物部守屋同其父一樣，對剛傳入日本的[佛教持反對態度](../Page/佛教.md "wikilink")，主張推崇日本本土宗教[神道教](../Page/神道教.md "wikilink")，強硬地反對佛教。572年，[敏達天皇即位](../Page/敏達天皇.md "wikilink")。物部守屋被任命為[大連](../Page/大連_\(官職\).md "wikilink")。而另一位支持佛教的貴族[蘇我馬子則被任命為](../Page/蘇我馬子.md "wikilink")[大臣](../Page/大臣.md "wikilink")。

根據《[日本書紀](../Page/日本書紀.md "wikilink")》記載，敏達14年（585年）2月，蘇我馬子得病，上奏敏達天皇，提出建立[佛寺](../Page/佛寺.md "wikilink")、崇祀[佛像的請求](../Page/佛像.md "wikilink")，並得到天皇的許可。但不久日本卻發生了疫病，大量人口死亡。3月，物部守屋便連同[中臣勝海上奏](../Page/中臣勝海.md "wikilink")，認為由於崇信「蕃神」導致了這場疫病，要求廢止佛法。天皇隨即准奏，守屋親自前往[蘇我氏所建的佛寺](../Page/蘇我氏.md "wikilink")，破壞佛塔，燒毀佛殿，將[佛像投入海中](../Page/佛像.md "wikilink")，當面唾駡[蘇我馬子](../Page/蘇我馬子.md "wikilink")、[司馬達等等一些信佛者](../Page/司馬達等.md "wikilink")，將和尚、尼姑拖到鬧市鞭打。從此物部守屋與蘇我馬子二人結下大怨。然而疫情更為嚴重，敏達天皇也病重，但蘇我馬子卻病癒。於是天皇再次批准馬子建立佛寺。

不久以後，[敏達天皇駕崩](../Page/敏達天皇.md "wikilink")。天皇的駕崩加劇了崇佛派和排佛派的對立。在[殯宮舉行天皇](../Page/殯宮.md "wikilink")[葬禮時](../Page/葬禮.md "wikilink")，物部守屋和[蘇我馬子互相譏諷和嘲笑](../Page/蘇我馬子.md "wikilink")。在蘇我馬子的推舉下，馬子的外甥池邊皇子即位，是為[用明天皇](../Page/用明天皇.md "wikilink")。物部守屋則與敏達天皇的異母弟[穴穗部皇子相結交](../Page/穴穗部皇子.md "wikilink")，並奉穴穗部皇子的命令攻殺了先皇的寵臣[三輪逆](../Page/三輪逆.md "wikilink")。

587年，用明天皇得病，欲重新信奉佛法，召集群臣商議。物部守屋、[中臣勝海二人強烈地反對](../Page/中臣勝海.md "wikilink")，但[蘇我馬子卻引](../Page/蘇我馬子.md "wikilink")[豐國法師入宮講解佛法](../Page/豐國法師.md "wikilink")，守屋怒視豐國法師。[押坂部毛屎將群臣將要在路上謀害物部守屋的事秘告於物部](../Page/押坂部毛屎.md "wikilink")，於是守屋逃出宮殿，在阿都（[河內國](../Page/河內國.md "wikilink")）聚兵自衛。[中臣勝海舉兵響應](../Page/中臣勝海.md "wikilink")，製作了崇佛派的[彥人皇子和](../Page/彥人皇子.md "wikilink")[竹田皇子的像對其進行](../Page/竹田皇子.md "wikilink")[詛咒](../Page/詛咒.md "wikilink")。但不久自度形勢不利，前往彥人皇子的住宅並發誓效忠於皇子。歸途中，中臣勝海為[舍人](../Page/舍人.md "wikilink")[迹見赤檮所殺](../Page/迹見赤檮.md "wikilink")。物部守屋隨後遣使告知蘇我馬子，稱群臣欲謀害自己，不得不聚兵自衛。馬子雖然公開表示要討伐守屋，但卻招兵買馬，日夜警備。

同年4月，用明天皇駕崩，蘇我馬子推舉泊瀨部皇子即位，是為[崇峻天皇](../Page/崇峻天皇.md "wikilink")。物部守屋欲舉兵擁立[穴穗部皇子即位](../Page/穴穗部皇子.md "wikilink")。蘇我馬子得知了此事，假借敏達天皇皇后[炊屋姬](../Page/推古天皇.md "wikilink")（推古）的詔命，發兵誅殺了排佛派的[穴穗部皇子和](../Page/穴穗部皇子.md "wikilink")[宅部皇子](../Page/宅部皇子.md "wikilink")。蘇我馬子決定殲滅物部守屋，聯合[泊瀨部](../Page/崇峻天皇.md "wikilink")、[竹田](../Page/竹田皇子.md "wikilink")、[豐聰耳](../Page/聖德太子.md "wikilink")（聖德太子）等皇子與朝廷各公卿大臣，率軍攻打物部氏，史稱[丁未之亂](../Page/丁未之亂.md "wikilink")。

物部氏據[稻城抵抗](../Page/稻城.md "wikilink")，一度擊退[蘇我氏的軍隊](../Page/蘇我氏.md "wikilink")。豐聰耳皇子向[四天王像祈禱](../Page/四天王.md "wikilink")，若能戰勝物部氏，必廣建佛塔，弘揚佛法。於是蘇我軍大勝，迹見赤檮射殺物部守屋，其子也全部被殺，物部氏滅亡。豐聰耳皇子於是在[難波建立了](../Page/難波.md "wikilink")[四天王寺以](../Page/四天王寺.md "wikilink")[還願](../Page/還願.md "wikilink")。

## 參考資料

  - [大日本史
    物部守屋](http://miko.org/~uraki/kuon/furu/text/dainihonsi/dns110.htm#05)

## 相關條目

  - [四天王寺](../Page/四天王寺.md "wikilink")
  - [大聖勝軍寺](../Page/大聖勝軍寺.md "wikilink")

[Category:飛鳥時代人物](../Category/飛鳥時代人物.md "wikilink")
[Category:日本战争身亡者](../Category/日本战争身亡者.md "wikilink")
[Category:日本戰爭身亡者](../Category/日本戰爭身亡者.md "wikilink")
[Moriya](../Category/物部氏.md "wikilink")