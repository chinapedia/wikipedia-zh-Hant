**牯嶺街小劇場**位於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")，建於1906年。它一共有三層：一樓設有服務台、文宣品區和實驗劇場；二樓設有會議室、視聽室、藝文空間；三樓設有排練室。它是臺灣唯一規劃定位為[前衛劇場的表演公共場所](../Page/先锋派.md "wikilink")。也是台北市第一個推動閒置空間再利用，轉化成藝文空間，並委由民間經營之場地。

從2002年小劇場正式開放至今，已經策劃100場以上的活動，參與使用者超過8000人次。

## 歷史

### 建築

本體建於1906年，[巴洛克風格建築](../Page/巴洛克建筑.md "wikilink")，[日治時期為](../Page/台灣日治時期.md "wikilink")[日本憲兵分隊所](../Page/日本憲兵.md "wikilink")，[戰後改成](../Page/中華民國領台時期.md "wikilink")[台北市政府警察局刑事組](../Page/台北市政府警察局.md "wikilink")。1958年改建為日式磚造建築，成為台北市政府警察局中正第二分局。1995年中正第二分局遷離，建物移交[台北市政府新聞處管理](../Page/台北市政府新聞處.md "wikilink")。2014年，[台北市政府公告登錄為歷史建築](../Page/台北市政府.md "wikilink")\[1\]。

### 小劇場

[陳梅毛](../Page/陳梅毛.md "wikilink")、[江世芳](../Page/江世芳.md "wikilink")、[鴻鴻等人成立](../Page/鴻鴻.md "wikilink")「小劇場聯盟」，爭取該地做為小劇場之用，1997年由台北市政府新聞處負責規劃成小劇場展演空間。

  - 1999年[台北市政府文化局成立](../Page/台北市政府文化局.md "wikilink")，小劇場移交文化局管理，於2001年設立「中正二分局小劇場」，並正式對外公開徵選委託營運管理單位。
  - 2001年～2005年6月，由[趙自強負責的](../Page/趙自強.md "wikilink")[如果兒童劇團經營管理](../Page/如果兒童劇團.md "wikilink")，「中正二分局小劇場」更名為「牯嶺街小劇場」。
  - 2002年3月，牯嶺街小劇場正式對外開放。
  - 2005年7月起，由[王墨林負責的](../Page/王墨林.md "wikilink")[身體氣象館統籌](../Page/身體氣象館.md "wikilink")，策劃及營運則由該劇場的經營管理處執行。

## 参见

  - [實驗劇場](../Page/實驗劇場.md "wikilink")
  - [身體氣象館](../Page/身體氣象館.md "wikilink")
  - [如果兒童劇團](../Page/如果兒童劇團.md "wikilink")
  - [臺北市政府文化局](../Page/臺北市政府文化局.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [牯嶺街小劇場官方網站](http://www.glt.org.tw/)
  - [牯嶺街小劇場
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/38)

[Category:台北市文化](../Category/台北市文化.md "wikilink")
[Category:臺北市中正區的藝文場館](../Category/臺北市中正區的藝文場館.md "wikilink")
[Category:台灣表演場館](../Category/台灣表演場館.md "wikilink")
[Category:1906年完工建築物](../Category/1906年完工建築物.md "wikilink")
[Category:臺北市文化局](../Category/臺北市文化局.md "wikilink")
[Category:臺北市歷史建築](../Category/臺北市歷史建築.md "wikilink")

1.