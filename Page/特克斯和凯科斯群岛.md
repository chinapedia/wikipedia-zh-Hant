|population_density_sq_mi = |population_density_rank = n/a
|GDP_PPP = |GDP_PPP_rank = |GDP_PPP_year = |GDP_PPP_per_capita =
|GDP_PPP_per_capita_rank = |sovereignty_type = |sovereignty_note =
|established_event1 = |established_date1 = |established_event2 =
|established_date2 = |HDI = 0.930 |HDI_rank = n/a |HDI_year = n/a
|currency =
[美元](../Page/美元.md "wikilink")（[USD](../Page/ISO_4217.md "wikilink")）
|currency_code = BSD |country_code = |time_zone =
[EST](../Page/北美东部时区.md "wikilink") |utc_offset =-5
|time_zone_DST = EDT |utc_offset_DST =-4 |iso3166code = TC |cctld =
[.tc](../Page/.tc.md "wikilink")
[英國郵區編號](../Page/英國郵區編號.md "wikilink")：TKCA 1ZZ |date_format = dd mm
yyyy（[AD](../Page/基督纪年.md "wikilink")） |drives_on = [-{zh-cn:左;
zh-tw:右駕;zh-hk:右軚;}-](../Page/道路通行方向.md "wikilink") |calling_code =
+1-649 |footnote1 = }} **特克斯和凯科斯群岛**（，和 /  /
)），位于[中美洲](../Page/中美洲.md "wikilink")[巴哈马群岛东南的](../Page/巴哈马群岛.md "wikilink")[英国属地](../Page/英国.md "wikilink")，屬[西印度群島內](../Page/西印度群島.md "wikilink")[盧卡亞群島的一部份](../Page/盧卡亞群島.md "wikilink")。由[特克斯群岛](../Page/特克斯群岛.md "wikilink")\[1\]和[凯科斯群岛](../Page/凯科斯群岛.md "wikilink")30多个岛屿组成，其中8个岛屿常年有人定居，面积430[平方千米](../Page/平方千米.md "wikilink")，[歐洲聯盟資料為](../Page/歐洲聯盟.md "wikilink")\[2\]。

## 歷史

本地原住民是來自[伊斯帕尼奧拉島的](../Page/伊斯帕尼奧拉島.md "wikilink")[泰諾族](../Page/泰諾族.md "wikilink")。[西班牙人於](../Page/西班牙.md "wikilink")1512年進駐群島，並捉拿島上的泰諾人作奴隸。自此至17世紀，各島上的人口不斷減少。

由16至18世紀，西班牙、法國和英國先後佔領群島，但都未有建立聚居地。直至1783年，在[美國獨立戰爭後](../Page/美國獨立戰爭.md "wikilink")，英國人移居群島，並開始進行[棉花種植](../Page/棉花.md "wikilink")，但未幾被製鹽業取代。

在19世紀，群島因幾番引入在鄰近海域沉船事故中生還的[黑奴而補充了當地人口和勞動力](../Page/黑奴.md "wikilink")。

群島於1848年成為英國[殖民地](../Page/殖民地.md "wikilink")，但又於1873年成為隸屬[牙買加的殖民地](../Page/牙買加.md "wikilink")。群島曾於1917年被時仼[加拿大總理](../Page/加拿大總理.md "wikilink")[羅伯特·萊爾德·博登提議將群島併入加國](../Page/羅伯特·萊爾德·博登.md "wikilink")，但被時仼[英國首相](../Page/英國首相.md "wikilink")[大衛·勞合·喬治拒絕](../Page/大衛·勞合·喬治.md "wikilink")，群島繼續成為牙買加屬地\[3\]。

群島於1959年再度分離成為單獨一個殖民地，但隸屬剛成立的[西印度群島聯邦](../Page/西印度群島聯邦.md "wikilink")，總督仍由[牙買加總督兼仼](../Page/牙買加總督.md "wikilink")，不久牙買加獨立，總督改由[巴哈馬總督兼仼](../Page/巴哈馬總督.md "wikilink")，直至巴哈馬於1973年獨立。隨後群島獲單獨委派總督。

隨着21世紀初期島上的政治危機，群島於2006年獲頒憲法，但於2009年因政府貪污醜聞而被英國取消自治權\[4\]，直至新憲法於2012年被頒佈，新政府於同年11月被選出。

## 經濟

群島以旅遊業和離岸金融業為主要收入來源。遊客主要來自[加拿大](../Page/加拿大.md "wikilink")，加拿大因此對其有重大影響力，基於現時和潛在經濟好處，近年甚至有呼聲要求群島加入加拿大，成為其第十一省。

## 地理

群島分為兩部分：於東面、人口較多但面積較小的特克斯群島，和於西面、人口較少但面積較大的凱科斯群島。兩個群島共有三百餘島。

### 地質

以[石灰岩](../Page/石灰岩.md "wikilink")、珊瑚碎屑和砂粒為主。

### 生態

屬生態熱點，有大量[沼澤和](../Page/沼澤.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")，海中有[龍蝦和貝殼類棲息](../Page/龍蝦.md "wikilink")。

### 氣候

全年溫暖乾燥，但偶有[颱風吹襲](../Page/颱風.md "wikilink")。

## 人口

2008年人口36,000人，多为[黑人](../Page/黑人.md "wikilink")\[5\]。通用[英语](../Page/英语.md "wikilink")。经济以[渔业及旅遊業为主](../Page/渔业.md "wikilink")，有鱼产品加工业和制盐业。首府為大特克斯島上之[科伯恩城](../Page/科伯恩城.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p>{{Historical populations</p></td>
<td><p>align= left</p></td>
<td><p>type =</p></td>
<td><p>footnote = Source[6]</p></td>
<td><p>title =戶籍人口普查和年均增長率</p></td>
<td><p>percentages =</p></td>
<td><p>1911|5615</p></td>
<td><p>1921|5522</p></td>
<td><p>1943|6138</p></td>
<td><p>1960|5668</p></td>
<td><p>1970|5558</p></td>
<td><p>1980|7413</p></td>
<td><p>1990|11465</p></td>
<td><p>2000|20014</p></td>
<td><p>2012|31458</p>
<p>}}</p></td>
</tr>
</tbody>
</table>

## 交通

[普羅維登西亞萊斯島設有國際機場和港口](../Page/普羅維登西亞萊斯島.md "wikilink")。

## 参考资料

## 延伸閱讀

  -
  -
[Category:加勒比地区](../Category/加勒比地区.md "wikilink")
[Category:英國海外領土及皇室屬土](../Category/英國海外領土及皇室屬土.md "wikilink")
[特克斯和凱科斯群島](../Category/特克斯和凱科斯群島.md "wikilink")

1.
2.

3.

4.

5.

6.  [1](http://www.depstc.org/census/c_background.html)，[archive , 2
    Jan., 2014](https://archive.is/wwwdepstcorg/census/c_background.html)
    [archive](http://*/www.depstc.org/census/c_background.html)